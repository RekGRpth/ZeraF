.class public Lcom/android/gallery3d/filtershow/imageshow/ImageShow;
.super Landroid/view/View;
.source "ImageShow.java"

# interfaces
.implements Landroid/view/GestureDetector$OnDoubleTapListener;
.implements Landroid/view/GestureDetector$OnGestureListener;
.implements Landroid/widget/SeekBar$OnSeekBarChangeListener;
.implements Lcom/android/gallery3d/filtershow/ui/SliderListener;


# static fields
.field private static final LOGTAG:Ljava/lang/String; = "Gallery2/ImageShow"

.field private static UNVEIL_HORIZONTAL:I

.field private static UNVEIL_VERTICAL:I

.field private static mBackgroundColor:I

.field private static mOriginalText:Ljava/lang/String;

.field private static mOriginalTextMargin:I

.field private static mOriginalTextSize:I

.field protected static mTextPadding:I

.field protected static mTextSize:I


# instance fields
.field private final USE_BACKGROUND_IMAGE:Z

.field private final USE_SLIDER_GESTURE:Z

.field protected backUpImage:Landroid/graphics/Bitmap;

.field private mActivity:Lcom/android/gallery3d/filtershow/FilterShowActivity;

.field private mBackgroundImage:Landroid/graphics/Bitmap;

.field private mController:Lcom/android/gallery3d/filtershow/PanelController;

.field private mCurrentFilter:Lcom/android/gallery3d/filtershow/filters/ImageFilter;

.field private mDirtyGeometry:Z

.field private mFilteredImage:Landroid/graphics/Bitmap;

.field private mFiltersOnlyImage:Landroid/graphics/Bitmap;

.field private mGeometryOnlyImage:Landroid/graphics/Bitmap;

.field private mGestureDetector:Landroid/view/GestureDetector;

.field private final mHandler:Landroid/os/Handler;

.field private mHistoryAdapter:Lcom/android/gallery3d/filtershow/HistoryAdapter;

.field private mImageBounds:Landroid/graphics/Rect;

.field protected mImageFiltersOnlyPreset:Lcom/android/gallery3d/filtershow/presets/ImagePreset;

.field protected mImageGeometryOnlyPreset:Lcom/android/gallery3d/filtershow/presets/ImagePreset;

.field protected mImageLoader:Lcom/android/gallery3d/filtershow/cache/ImageLoader;

.field protected mImagePreset:Lcom/android/gallery3d/filtershow/presets/ImagePreset;

.field private mImageStateAdapter:Lcom/android/gallery3d/filtershow/ImageStateAdapter;

.field private mImportantToast:Z

.field protected mPaint:Landroid/graphics/Paint;

.field private mSeekBar:Landroid/widget/SeekBar;

.field private mShowControls:Z

.field private mShowOriginal:Z

.field private mShowOriginalDirection:I

.field private mShowToast:Z

.field protected mSliderController:Lcom/android/gallery3d/filtershow/ui/SliderController;

.field private mToast:Ljava/lang/String;

.field private mTouchDownX:I

.field private mTouchDownY:I

.field private mTouchShowOriginal:Z

.field private mTouchShowOriginalDate:J

.field private final mTouchShowOriginalDelayMax:J

.field private final mTouchShowOriginalDelayMin:J

.field protected mTouchX:F

.field protected mTouchY:F


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const/16 v0, 0x18

    sput v0, Lcom/android/gallery3d/filtershow/imageshow/ImageShow;->mTextSize:I

    const/16 v0, 0x14

    sput v0, Lcom/android/gallery3d/filtershow/imageshow/ImageShow;->mTextPadding:I

    const/high16 v0, -0x10000

    sput v0, Lcom/android/gallery3d/filtershow/imageshow/ImageShow;->mBackgroundColor:I

    const/4 v0, 0x1

    sput v0, Lcom/android/gallery3d/filtershow/imageshow/ImageShow;->UNVEIL_HORIZONTAL:I

    const/4 v0, 0x2

    sput v0, Lcom/android/gallery3d/filtershow/imageshow/ImageShow;->UNVEIL_VERTICAL:I

    const/16 v0, 0x8

    sput v0, Lcom/android/gallery3d/filtershow/imageshow/ImageShow;->mOriginalTextMargin:I

    const/16 v0, 0x1a

    sput v0, Lcom/android/gallery3d/filtershow/imageshow/ImageShow;->mOriginalTextSize:I

    const-string v0, "Original"

    sput-object v0, Lcom/android/gallery3d/filtershow/imageshow/ImageShow;->mOriginalText:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 5
    .param p1    # Landroid/content/Context;

    const/4 v4, 0x0

    const/4 v3, 0x0

    const/4 v2, 0x0

    invoke-direct {p0, p1}, Landroid/view/View;-><init>(Landroid/content/Context;)V

    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0}, Landroid/graphics/Paint;-><init>()V

    iput-object v0, p0, Lcom/android/gallery3d/filtershow/imageshow/ImageShow;->mPaint:Landroid/graphics/Paint;

    iput-object v2, p0, Lcom/android/gallery3d/filtershow/imageshow/ImageShow;->mImagePreset:Lcom/android/gallery3d/filtershow/presets/ImagePreset;

    iput-object v2, p0, Lcom/android/gallery3d/filtershow/imageshow/ImageShow;->mImageGeometryOnlyPreset:Lcom/android/gallery3d/filtershow/presets/ImagePreset;

    iput-object v2, p0, Lcom/android/gallery3d/filtershow/imageshow/ImageShow;->mImageFiltersOnlyPreset:Lcom/android/gallery3d/filtershow/presets/ImagePreset;

    iput-object v2, p0, Lcom/android/gallery3d/filtershow/imageshow/ImageShow;->mImageLoader:Lcom/android/gallery3d/filtershow/cache/ImageLoader;

    iput-object v2, p0, Lcom/android/gallery3d/filtershow/imageshow/ImageShow;->mCurrentFilter:Lcom/android/gallery3d/filtershow/filters/ImageFilter;

    iput-boolean v3, p0, Lcom/android/gallery3d/filtershow/imageshow/ImageShow;->mDirtyGeometry:Z

    iput-object v2, p0, Lcom/android/gallery3d/filtershow/imageshow/ImageShow;->mBackgroundImage:Landroid/graphics/Bitmap;

    iput-boolean v3, p0, Lcom/android/gallery3d/filtershow/imageshow/ImageShow;->USE_BACKGROUND_IMAGE:Z

    iput-object v2, p0, Lcom/android/gallery3d/filtershow/imageshow/ImageShow;->mGeometryOnlyImage:Landroid/graphics/Bitmap;

    iput-object v2, p0, Lcom/android/gallery3d/filtershow/imageshow/ImageShow;->mFiltersOnlyImage:Landroid/graphics/Bitmap;

    iput-object v2, p0, Lcom/android/gallery3d/filtershow/imageshow/ImageShow;->mFilteredImage:Landroid/graphics/Bitmap;

    iput-boolean v3, p0, Lcom/android/gallery3d/filtershow/imageshow/ImageShow;->USE_SLIDER_GESTURE:Z

    new-instance v0, Lcom/android/gallery3d/filtershow/ui/SliderController;

    invoke-direct {v0}, Lcom/android/gallery3d/filtershow/ui/SliderController;-><init>()V

    iput-object v0, p0, Lcom/android/gallery3d/filtershow/imageshow/ImageShow;->mSliderController:Lcom/android/gallery3d/filtershow/ui/SliderController;

    iput-object v2, p0, Lcom/android/gallery3d/filtershow/imageshow/ImageShow;->mGestureDetector:Landroid/view/GestureDetector;

    iput-object v2, p0, Lcom/android/gallery3d/filtershow/imageshow/ImageShow;->mHistoryAdapter:Lcom/android/gallery3d/filtershow/HistoryAdapter;

    iput-object v2, p0, Lcom/android/gallery3d/filtershow/imageshow/ImageShow;->mImageStateAdapter:Lcom/android/gallery3d/filtershow/ImageStateAdapter;

    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    iput-object v0, p0, Lcom/android/gallery3d/filtershow/imageshow/ImageShow;->mImageBounds:Landroid/graphics/Rect;

    iput-boolean v3, p0, Lcom/android/gallery3d/filtershow/imageshow/ImageShow;->mTouchShowOriginal:Z

    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/android/gallery3d/filtershow/imageshow/ImageShow;->mTouchShowOriginalDate:J

    const-wide/16 v0, 0xc8

    iput-wide v0, p0, Lcom/android/gallery3d/filtershow/imageshow/ImageShow;->mTouchShowOriginalDelayMin:J

    const-wide/16 v0, 0x12c

    iput-wide v0, p0, Lcom/android/gallery3d/filtershow/imageshow/ImageShow;->mTouchShowOriginalDelayMax:J

    iput v3, p0, Lcom/android/gallery3d/filtershow/imageshow/ImageShow;->mShowOriginalDirection:I

    iput v3, p0, Lcom/android/gallery3d/filtershow/imageshow/ImageShow;->mTouchDownX:I

    iput v3, p0, Lcom/android/gallery3d/filtershow/imageshow/ImageShow;->mTouchDownY:I

    iput v4, p0, Lcom/android/gallery3d/filtershow/imageshow/ImageShow;->mTouchX:F

    iput v4, p0, Lcom/android/gallery3d/filtershow/imageshow/ImageShow;->mTouchY:F

    iput-object v2, p0, Lcom/android/gallery3d/filtershow/imageshow/ImageShow;->backUpImage:Landroid/graphics/Bitmap;

    iput-boolean v3, p0, Lcom/android/gallery3d/filtershow/imageshow/ImageShow;->mShowControls:Z

    iput-boolean v3, p0, Lcom/android/gallery3d/filtershow/imageshow/ImageShow;->mShowOriginal:Z

    iput-object v2, p0, Lcom/android/gallery3d/filtershow/imageshow/ImageShow;->mToast:Ljava/lang/String;

    iput-boolean v3, p0, Lcom/android/gallery3d/filtershow/imageshow/ImageShow;->mShowToast:Z

    iput-boolean v3, p0, Lcom/android/gallery3d/filtershow/imageshow/ImageShow;->mImportantToast:Z

    iput-object v2, p0, Lcom/android/gallery3d/filtershow/imageshow/ImageShow;->mSeekBar:Landroid/widget/SeekBar;

    iput-object v2, p0, Lcom/android/gallery3d/filtershow/imageshow/ImageShow;->mController:Lcom/android/gallery3d/filtershow/PanelController;

    iput-object v2, p0, Lcom/android/gallery3d/filtershow/imageshow/ImageShow;->mActivity:Lcom/android/gallery3d/filtershow/FilterShowActivity;

    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    iput-object v0, p0, Lcom/android/gallery3d/filtershow/imageshow/ImageShow;->mHandler:Landroid/os/Handler;

    new-instance v0, Lcom/android/gallery3d/filtershow/HistoryAdapter;

    const v1, 0x7f040019

    const v2, 0x7f0b0059

    invoke-direct {v0, p1, v1, v2}, Lcom/android/gallery3d/filtershow/HistoryAdapter;-><init>(Landroid/content/Context;II)V

    iput-object v0, p0, Lcom/android/gallery3d/filtershow/imageshow/ImageShow;->mHistoryAdapter:Lcom/android/gallery3d/filtershow/HistoryAdapter;

    invoke-virtual {p0, p1}, Lcom/android/gallery3d/filtershow/imageshow/ImageShow;->setupGestureDetector(Landroid/content/Context;)V

    check-cast p1, Lcom/android/gallery3d/filtershow/FilterShowActivity;

    iput-object p1, p0, Lcom/android/gallery3d/filtershow/imageshow/ImageShow;->mActivity:Lcom/android/gallery3d/filtershow/FilterShowActivity;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 5
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/util/AttributeSet;

    const/4 v4, 0x0

    const/4 v3, 0x0

    const/4 v2, 0x0

    invoke-direct {p0, p1, p2}, Landroid/view/View;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0}, Landroid/graphics/Paint;-><init>()V

    iput-object v0, p0, Lcom/android/gallery3d/filtershow/imageshow/ImageShow;->mPaint:Landroid/graphics/Paint;

    iput-object v2, p0, Lcom/android/gallery3d/filtershow/imageshow/ImageShow;->mImagePreset:Lcom/android/gallery3d/filtershow/presets/ImagePreset;

    iput-object v2, p0, Lcom/android/gallery3d/filtershow/imageshow/ImageShow;->mImageGeometryOnlyPreset:Lcom/android/gallery3d/filtershow/presets/ImagePreset;

    iput-object v2, p0, Lcom/android/gallery3d/filtershow/imageshow/ImageShow;->mImageFiltersOnlyPreset:Lcom/android/gallery3d/filtershow/presets/ImagePreset;

    iput-object v2, p0, Lcom/android/gallery3d/filtershow/imageshow/ImageShow;->mImageLoader:Lcom/android/gallery3d/filtershow/cache/ImageLoader;

    iput-object v2, p0, Lcom/android/gallery3d/filtershow/imageshow/ImageShow;->mCurrentFilter:Lcom/android/gallery3d/filtershow/filters/ImageFilter;

    iput-boolean v3, p0, Lcom/android/gallery3d/filtershow/imageshow/ImageShow;->mDirtyGeometry:Z

    iput-object v2, p0, Lcom/android/gallery3d/filtershow/imageshow/ImageShow;->mBackgroundImage:Landroid/graphics/Bitmap;

    iput-boolean v3, p0, Lcom/android/gallery3d/filtershow/imageshow/ImageShow;->USE_BACKGROUND_IMAGE:Z

    iput-object v2, p0, Lcom/android/gallery3d/filtershow/imageshow/ImageShow;->mGeometryOnlyImage:Landroid/graphics/Bitmap;

    iput-object v2, p0, Lcom/android/gallery3d/filtershow/imageshow/ImageShow;->mFiltersOnlyImage:Landroid/graphics/Bitmap;

    iput-object v2, p0, Lcom/android/gallery3d/filtershow/imageshow/ImageShow;->mFilteredImage:Landroid/graphics/Bitmap;

    iput-boolean v3, p0, Lcom/android/gallery3d/filtershow/imageshow/ImageShow;->USE_SLIDER_GESTURE:Z

    new-instance v0, Lcom/android/gallery3d/filtershow/ui/SliderController;

    invoke-direct {v0}, Lcom/android/gallery3d/filtershow/ui/SliderController;-><init>()V

    iput-object v0, p0, Lcom/android/gallery3d/filtershow/imageshow/ImageShow;->mSliderController:Lcom/android/gallery3d/filtershow/ui/SliderController;

    iput-object v2, p0, Lcom/android/gallery3d/filtershow/imageshow/ImageShow;->mGestureDetector:Landroid/view/GestureDetector;

    iput-object v2, p0, Lcom/android/gallery3d/filtershow/imageshow/ImageShow;->mHistoryAdapter:Lcom/android/gallery3d/filtershow/HistoryAdapter;

    iput-object v2, p0, Lcom/android/gallery3d/filtershow/imageshow/ImageShow;->mImageStateAdapter:Lcom/android/gallery3d/filtershow/ImageStateAdapter;

    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    iput-object v0, p0, Lcom/android/gallery3d/filtershow/imageshow/ImageShow;->mImageBounds:Landroid/graphics/Rect;

    iput-boolean v3, p0, Lcom/android/gallery3d/filtershow/imageshow/ImageShow;->mTouchShowOriginal:Z

    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/android/gallery3d/filtershow/imageshow/ImageShow;->mTouchShowOriginalDate:J

    const-wide/16 v0, 0xc8

    iput-wide v0, p0, Lcom/android/gallery3d/filtershow/imageshow/ImageShow;->mTouchShowOriginalDelayMin:J

    const-wide/16 v0, 0x12c

    iput-wide v0, p0, Lcom/android/gallery3d/filtershow/imageshow/ImageShow;->mTouchShowOriginalDelayMax:J

    iput v3, p0, Lcom/android/gallery3d/filtershow/imageshow/ImageShow;->mShowOriginalDirection:I

    iput v3, p0, Lcom/android/gallery3d/filtershow/imageshow/ImageShow;->mTouchDownX:I

    iput v3, p0, Lcom/android/gallery3d/filtershow/imageshow/ImageShow;->mTouchDownY:I

    iput v4, p0, Lcom/android/gallery3d/filtershow/imageshow/ImageShow;->mTouchX:F

    iput v4, p0, Lcom/android/gallery3d/filtershow/imageshow/ImageShow;->mTouchY:F

    iput-object v2, p0, Lcom/android/gallery3d/filtershow/imageshow/ImageShow;->backUpImage:Landroid/graphics/Bitmap;

    iput-boolean v3, p0, Lcom/android/gallery3d/filtershow/imageshow/ImageShow;->mShowControls:Z

    iput-boolean v3, p0, Lcom/android/gallery3d/filtershow/imageshow/ImageShow;->mShowOriginal:Z

    iput-object v2, p0, Lcom/android/gallery3d/filtershow/imageshow/ImageShow;->mToast:Ljava/lang/String;

    iput-boolean v3, p0, Lcom/android/gallery3d/filtershow/imageshow/ImageShow;->mShowToast:Z

    iput-boolean v3, p0, Lcom/android/gallery3d/filtershow/imageshow/ImageShow;->mImportantToast:Z

    iput-object v2, p0, Lcom/android/gallery3d/filtershow/imageshow/ImageShow;->mSeekBar:Landroid/widget/SeekBar;

    iput-object v2, p0, Lcom/android/gallery3d/filtershow/imageshow/ImageShow;->mController:Lcom/android/gallery3d/filtershow/PanelController;

    iput-object v2, p0, Lcom/android/gallery3d/filtershow/imageshow/ImageShow;->mActivity:Lcom/android/gallery3d/filtershow/FilterShowActivity;

    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    iput-object v0, p0, Lcom/android/gallery3d/filtershow/imageshow/ImageShow;->mHandler:Landroid/os/Handler;

    new-instance v0, Lcom/android/gallery3d/filtershow/HistoryAdapter;

    const v1, 0x7f040019

    const v2, 0x7f0b0059

    invoke-direct {v0, p1, v1, v2}, Lcom/android/gallery3d/filtershow/HistoryAdapter;-><init>(Landroid/content/Context;II)V

    iput-object v0, p0, Lcom/android/gallery3d/filtershow/imageshow/ImageShow;->mHistoryAdapter:Lcom/android/gallery3d/filtershow/HistoryAdapter;

    new-instance v0, Lcom/android/gallery3d/filtershow/ImageStateAdapter;

    const v1, 0x7f04001a

    invoke-direct {v0, p1, v1}, Lcom/android/gallery3d/filtershow/ImageStateAdapter;-><init>(Landroid/content/Context;I)V

    iput-object v0, p0, Lcom/android/gallery3d/filtershow/imageshow/ImageShow;->mImageStateAdapter:Lcom/android/gallery3d/filtershow/ImageStateAdapter;

    invoke-virtual {p0, p1}, Lcom/android/gallery3d/filtershow/imageshow/ImageShow;->setupGestureDetector(Landroid/content/Context;)V

    check-cast p1, Lcom/android/gallery3d/filtershow/FilterShowActivity;

    iput-object p1, p0, Lcom/android/gallery3d/filtershow/imageshow/ImageShow;->mActivity:Lcom/android/gallery3d/filtershow/FilterShowActivity;

    return-void
.end method

.method static synthetic access$002(Lcom/android/gallery3d/filtershow/imageshow/ImageShow;Z)Z
    .locals 0
    .param p0    # Lcom/android/gallery3d/filtershow/imageshow/ImageShow;
    .param p1    # Z

    iput-boolean p1, p0, Lcom/android/gallery3d/filtershow/imageshow/ImageShow;->mShowToast:Z

    return p1
.end method

.method private imageSizeChanged(Landroid/graphics/Bitmap;)V
    .locals 7
    .param p1    # Landroid/graphics/Bitmap;

    const/4 v6, 0x0

    if-eqz p1, :cond_0

    invoke-virtual {p0}, Lcom/android/gallery3d/filtershow/imageshow/ImageShow;->getImagePreset()Lcom/android/gallery3d/filtershow/presets/ImagePreset;

    move-result-object v5

    if-nez v5, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    invoke-virtual {p1}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v5

    int-to-float v4, v5

    invoke-virtual {p1}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v5

    int-to-float v1, v5

    invoke-virtual {p0}, Lcom/android/gallery3d/filtershow/imageshow/ImageShow;->getImagePreset()Lcom/android/gallery3d/filtershow/presets/ImagePreset;

    move-result-object v5

    iget-object v0, v5, Lcom/android/gallery3d/filtershow/presets/ImagePreset;->mGeoData:Lcom/android/gallery3d/filtershow/imageshow/GeometryMetadata;

    invoke-virtual {v0}, Lcom/android/gallery3d/filtershow/imageshow/GeometryMetadata;->getPhotoBounds()Landroid/graphics/RectF;

    move-result-object v2

    invoke-virtual {v2}, Landroid/graphics/RectF;->width()F

    move-result v5

    cmpl-float v5, v4, v5

    if-nez v5, :cond_2

    invoke-virtual {v2}, Landroid/graphics/RectF;->height()F

    move-result v5

    cmpl-float v5, v1, v5

    if-eqz v5, :cond_0

    :cond_2
    new-instance v3, Landroid/graphics/RectF;

    invoke-direct {v3, v6, v6, v4, v1}, Landroid/graphics/RectF;-><init>(FFFF)V

    invoke-virtual {p0}, Lcom/android/gallery3d/filtershow/imageshow/ImageShow;->getImagePreset()Lcom/android/gallery3d/filtershow/presets/ImagePreset;

    move-result-object v5

    iget-object v5, v5, Lcom/android/gallery3d/filtershow/presets/ImagePreset;->mGeoData:Lcom/android/gallery3d/filtershow/imageshow/GeometryMetadata;

    invoke-virtual {v5, v3}, Lcom/android/gallery3d/filtershow/imageshow/GeometryMetadata;->setPhotoBounds(Landroid/graphics/RectF;)V

    invoke-virtual {p0}, Lcom/android/gallery3d/filtershow/imageshow/ImageShow;->getImagePreset()Lcom/android/gallery3d/filtershow/presets/ImagePreset;

    move-result-object v5

    iget-object v5, v5, Lcom/android/gallery3d/filtershow/presets/ImagePreset;->mGeoData:Lcom/android/gallery3d/filtershow/imageshow/GeometryMetadata;

    invoke-virtual {v5, v3}, Lcom/android/gallery3d/filtershow/imageshow/GeometryMetadata;->setCropBounds(Landroid/graphics/RectF;)V

    invoke-direct {p0}, Lcom/android/gallery3d/filtershow/imageshow/ImageShow;->setDirtyGeometryFlag()V

    goto :goto_0
.end method

.method private parameterToUI(IIII)I
    .locals 2
    .param p1    # I
    .param p2    # I
    .param p3    # I
    .param p4    # I

    sub-int v0, p1, p2

    mul-int/2addr v0, p4

    sub-int v1, p3, p2

    div-int/2addr v0, v1

    return v0
.end method

.method public static setDefaultBackgroundColor(I)V
    .locals 0
    .param p0    # I

    sput p0, Lcom/android/gallery3d/filtershow/imageshow/ImageShow;->mBackgroundColor:I

    return-void
.end method

.method private setDirtyGeometryFlag()V
    .locals 1

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/android/gallery3d/filtershow/imageshow/ImageShow;->mDirtyGeometry:Z

    return-void
.end method

.method public static setOriginalText(Ljava/lang/String;)V
    .locals 0
    .param p0    # Ljava/lang/String;

    sput-object p0, Lcom/android/gallery3d/filtershow/imageshow/ImageShow;->mOriginalText:Ljava/lang/String;

    return-void
.end method

.method public static setOriginalTextMargin(I)V
    .locals 0
    .param p0    # I

    sput p0, Lcom/android/gallery3d/filtershow/imageshow/ImageShow;->mOriginalTextMargin:I

    return-void
.end method

.method public static setOriginalTextSize(I)V
    .locals 0
    .param p0    # I

    sput p0, Lcom/android/gallery3d/filtershow/imageshow/ImageShow;->mOriginalTextSize:I

    return-void
.end method

.method public static setTextPadding(I)V
    .locals 0
    .param p0    # I

    sput p0, Lcom/android/gallery3d/filtershow/imageshow/ImageShow;->mTextPadding:I

    return-void
.end method

.method public static setTextSize(I)V
    .locals 0
    .param p0    # I

    sput p0, Lcom/android/gallery3d/filtershow/imageshow/ImageShow;->mTextSize:I

    return-void
.end method

.method private uiToParameter(IIII)I
    .locals 1
    .param p1    # I
    .param p2    # I
    .param p3    # I
    .param p4    # I

    sub-int v0, p3, p2

    mul-int/2addr v0, p1

    div-int/2addr v0, p4

    add-int/2addr v0, p2

    return v0
.end method


# virtual methods
.method protected clearDirtyGeometryFlag()V
    .locals 1

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/android/gallery3d/filtershow/imageshow/ImageShow;->mDirtyGeometry:Z

    return-void
.end method

.method public defaultDrawImage(Landroid/graphics/Canvas;)V
    .locals 1
    .param p1    # Landroid/graphics/Canvas;

    invoke-virtual {p0}, Lcom/android/gallery3d/filtershow/imageshow/ImageShow;->getFilteredImage()Landroid/graphics/Bitmap;

    move-result-object v0

    invoke-virtual {p0, p1, v0}, Lcom/android/gallery3d/filtershow/imageshow/ImageShow;->drawImage(Landroid/graphics/Canvas;Landroid/graphics/Bitmap;)V

    invoke-virtual {p0}, Lcom/android/gallery3d/filtershow/imageshow/ImageShow;->getGeometryOnlyImage()Landroid/graphics/Bitmap;

    move-result-object v0

    invoke-virtual {p0, p1, v0}, Lcom/android/gallery3d/filtershow/imageshow/ImageShow;->drawPartialImage(Landroid/graphics/Canvas;Landroid/graphics/Bitmap;)V

    return-void
.end method

.method public drawBackground(Landroid/graphics/Canvas;)V
    .locals 1
    .param p1    # Landroid/graphics/Canvas;

    sget v0, Lcom/android/gallery3d/filtershow/imageshow/ImageShow;->mBackgroundColor:I

    invoke-virtual {p1, v0}, Landroid/graphics/Canvas;->drawColor(I)V

    return-void
.end method

.method public drawImage(Landroid/graphics/Canvas;Landroid/graphics/Bitmap;)V
    .locals 12
    .param p1    # Landroid/graphics/Canvas;
    .param p2    # Landroid/graphics/Bitmap;

    const/4 v9, 0x0

    const/high16 v11, 0x40000000

    if-eqz p2, :cond_0

    new-instance v2, Landroid/graphics/Rect;

    invoke-virtual {p2}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v7

    invoke-virtual {p2}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v8

    invoke-direct {v2, v9, v9, v7, v8}, Landroid/graphics/Rect;-><init>(IIII)V

    invoke-virtual {p2}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v7

    int-to-float v7, v7

    invoke-virtual {p2}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v8

    int-to-float v8, v8

    invoke-virtual {p0}, Landroid/view/View;->getWidth()I

    move-result v9

    int-to-float v9, v9

    invoke-virtual {p0}, Landroid/view/View;->getHeight()I

    move-result v10

    int-to-float v10, v10

    invoke-static {v7, v8, v9, v10}, Lcom/android/gallery3d/filtershow/imageshow/GeometryMath;->scale(FFFF)F

    move-result v3

    invoke-virtual {p2}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v7

    int-to-float v7, v7

    mul-float v6, v7, v3

    invoke-virtual {p2}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v7

    int-to-float v7, v7

    mul-float v1, v7, v3

    invoke-virtual {p0}, Landroid/view/View;->getHeight()I

    move-result v7

    int-to-float v7, v7

    sub-float/2addr v7, v1

    div-float v5, v7, v11

    invoke-virtual {p0}, Landroid/view/View;->getWidth()I

    move-result v7

    int-to-float v7, v7

    sub-float/2addr v7, v6

    div-float v4, v7, v11

    new-instance v0, Landroid/graphics/Rect;

    float-to-int v7, v4

    float-to-int v8, v5

    add-float v9, v6, v4

    float-to-int v9, v9

    add-float v10, v1, v5

    float-to-int v10, v10

    invoke-direct {v0, v7, v8, v9, v10}, Landroid/graphics/Rect;-><init>(IIII)V

    iput-object v0, p0, Lcom/android/gallery3d/filtershow/imageshow/ImageShow;->mImageBounds:Landroid/graphics/Rect;

    iget-object v7, p0, Lcom/android/gallery3d/filtershow/imageshow/ImageShow;->mPaint:Landroid/graphics/Paint;

    invoke-virtual {p1, p2, v2, v0, v7}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;Landroid/graphics/Rect;Landroid/graphics/Rect;Landroid/graphics/Paint;)V

    :cond_0
    return-void
.end method

.method public drawPartialImage(Landroid/graphics/Canvas;Landroid/graphics/Bitmap;)V
    .locals 11
    .param p1    # Landroid/graphics/Canvas;
    .param p2    # Landroid/graphics/Bitmap;

    const/high16 v10, -0x1000000

    const/high16 v4, 0x3f800000

    iget-boolean v0, p0, Lcom/android/gallery3d/filtershow/imageshow/ImageShow;->mTouchShowOriginal:Z

    if-nez v0, :cond_0

    :goto_0
    return-void

    :cond_0
    invoke-virtual {p1}, Landroid/graphics/Canvas;->save()I

    if-eqz p2, :cond_2

    iget v0, p0, Lcom/android/gallery3d/filtershow/imageshow/ImageShow;->mShowOriginalDirection:I

    if-nez v0, :cond_1

    iget v0, p0, Lcom/android/gallery3d/filtershow/imageshow/ImageShow;->mTouchY:F

    iget v1, p0, Lcom/android/gallery3d/filtershow/imageshow/ImageShow;->mTouchDownY:I

    int-to-float v1, v1

    sub-float/2addr v0, v1

    iget v1, p0, Lcom/android/gallery3d/filtershow/imageshow/ImageShow;->mTouchX:F

    iget v2, p0, Lcom/android/gallery3d/filtershow/imageshow/ImageShow;->mTouchDownX:I

    int-to-float v2, v2

    sub-float/2addr v1, v2

    cmpl-float v0, v0, v1

    if-lez v0, :cond_3

    sget v0, Lcom/android/gallery3d/filtershow/imageshow/ImageShow;->UNVEIL_VERTICAL:I

    iput v0, p0, Lcom/android/gallery3d/filtershow/imageshow/ImageShow;->mShowOriginalDirection:I

    :cond_1
    :goto_1
    const/4 v8, 0x0

    const/4 v9, 0x0

    iget v0, p0, Lcom/android/gallery3d/filtershow/imageshow/ImageShow;->mShowOriginalDirection:I

    sget v1, Lcom/android/gallery3d/filtershow/imageshow/ImageShow;->UNVEIL_VERTICAL:I

    if-ne v0, v1, :cond_4

    iget-object v0, p0, Lcom/android/gallery3d/filtershow/imageshow/ImageShow;->mImageBounds:Landroid/graphics/Rect;

    invoke-virtual {v0}, Landroid/graphics/Rect;->width()I

    move-result v8

    iget v0, p0, Lcom/android/gallery3d/filtershow/imageshow/ImageShow;->mTouchY:F

    iget-object v1, p0, Lcom/android/gallery3d/filtershow/imageshow/ImageShow;->mImageBounds:Landroid/graphics/Rect;

    iget v1, v1, Landroid/graphics/Rect;->top:I

    int-to-float v1, v1

    sub-float/2addr v0, v1

    float-to-int v9, v0

    :goto_2
    new-instance v7, Landroid/graphics/Rect;

    iget-object v0, p0, Lcom/android/gallery3d/filtershow/imageshow/ImageShow;->mImageBounds:Landroid/graphics/Rect;

    iget v0, v0, Landroid/graphics/Rect;->left:I

    iget-object v1, p0, Lcom/android/gallery3d/filtershow/imageshow/ImageShow;->mImageBounds:Landroid/graphics/Rect;

    iget v1, v1, Landroid/graphics/Rect;->top:I

    iget-object v2, p0, Lcom/android/gallery3d/filtershow/imageshow/ImageShow;->mImageBounds:Landroid/graphics/Rect;

    iget v2, v2, Landroid/graphics/Rect;->left:I

    add-int/2addr v2, v8

    iget-object v3, p0, Lcom/android/gallery3d/filtershow/imageshow/ImageShow;->mImageBounds:Landroid/graphics/Rect;

    iget v3, v3, Landroid/graphics/Rect;->top:I

    add-int/2addr v3, v9

    invoke-direct {v7, v0, v1, v2, v3}, Landroid/graphics/Rect;-><init>(IIII)V

    invoke-virtual {p1, v7}, Landroid/graphics/Canvas;->clipRect(Landroid/graphics/Rect;)Z

    invoke-virtual {p0, p1, p2}, Lcom/android/gallery3d/filtershow/imageshow/ImageShow;->drawImage(Landroid/graphics/Canvas;Landroid/graphics/Bitmap;)V

    new-instance v5, Landroid/graphics/Paint;

    invoke-direct {v5}, Landroid/graphics/Paint;-><init>()V

    invoke-virtual {v5, v10}, Landroid/graphics/Paint;->setColor(I)V

    iget v0, p0, Lcom/android/gallery3d/filtershow/imageshow/ImageShow;->mShowOriginalDirection:I

    sget v1, Lcom/android/gallery3d/filtershow/imageshow/ImageShow;->UNVEIL_VERTICAL:I

    if-ne v0, v1, :cond_5

    iget-object v0, p0, Lcom/android/gallery3d/filtershow/imageshow/ImageShow;->mImageBounds:Landroid/graphics/Rect;

    iget v0, v0, Landroid/graphics/Rect;->left:I

    int-to-float v1, v0

    iget v0, p0, Lcom/android/gallery3d/filtershow/imageshow/ImageShow;->mTouchY:F

    sub-float v2, v0, v4

    iget-object v0, p0, Lcom/android/gallery3d/filtershow/imageshow/ImageShow;->mImageBounds:Landroid/graphics/Rect;

    iget v0, v0, Landroid/graphics/Rect;->right:I

    int-to-float v3, v0

    iget v0, p0, Lcom/android/gallery3d/filtershow/imageshow/ImageShow;->mTouchY:F

    sub-float v4, v0, v4

    move-object v0, p1

    invoke-virtual/range {v0 .. v5}, Landroid/graphics/Canvas;->drawLine(FFFFLandroid/graphics/Paint;)V

    :goto_3
    new-instance v6, Landroid/graphics/Rect;

    invoke-direct {v6}, Landroid/graphics/Rect;-><init>()V

    sget v0, Lcom/android/gallery3d/filtershow/imageshow/ImageShow;->mOriginalTextSize:I

    int-to-float v0, v0

    invoke-virtual {v5, v0}, Landroid/graphics/Paint;->setTextSize(F)V

    sget-object v0, Lcom/android/gallery3d/filtershow/imageshow/ImageShow;->mOriginalText:Ljava/lang/String;

    const/4 v1, 0x0

    sget-object v2, Lcom/android/gallery3d/filtershow/imageshow/ImageShow;->mOriginalText:Ljava/lang/String;

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v2

    invoke-virtual {v5, v0, v1, v2, v6}, Landroid/graphics/Paint;->getTextBounds(Ljava/lang/String;IILandroid/graphics/Rect;)V

    invoke-virtual {v5, v10}, Landroid/graphics/Paint;->setColor(I)V

    sget-object v0, Lcom/android/gallery3d/filtershow/imageshow/ImageShow;->mOriginalText:Ljava/lang/String;

    iget-object v1, p0, Lcom/android/gallery3d/filtershow/imageshow/ImageShow;->mImageBounds:Landroid/graphics/Rect;

    iget v1, v1, Landroid/graphics/Rect;->left:I

    sget v2, Lcom/android/gallery3d/filtershow/imageshow/ImageShow;->mOriginalTextMargin:I

    add-int/2addr v1, v2

    add-int/lit8 v1, v1, 0x1

    int-to-float v1, v1

    iget-object v2, p0, Lcom/android/gallery3d/filtershow/imageshow/ImageShow;->mImageBounds:Landroid/graphics/Rect;

    iget v2, v2, Landroid/graphics/Rect;->top:I

    invoke-virtual {v6}, Landroid/graphics/Rect;->height()I

    move-result v3

    add-int/2addr v2, v3

    sget v3, Lcom/android/gallery3d/filtershow/imageshow/ImageShow;->mOriginalTextMargin:I

    add-int/2addr v2, v3

    add-int/lit8 v2, v2, 0x1

    int-to-float v2, v2

    invoke-virtual {p1, v0, v1, v2, v5}, Landroid/graphics/Canvas;->drawText(Ljava/lang/String;FFLandroid/graphics/Paint;)V

    const/4 v0, -0x1

    invoke-virtual {v5, v0}, Landroid/graphics/Paint;->setColor(I)V

    sget-object v0, Lcom/android/gallery3d/filtershow/imageshow/ImageShow;->mOriginalText:Ljava/lang/String;

    iget-object v1, p0, Lcom/android/gallery3d/filtershow/imageshow/ImageShow;->mImageBounds:Landroid/graphics/Rect;

    iget v1, v1, Landroid/graphics/Rect;->left:I

    sget v2, Lcom/android/gallery3d/filtershow/imageshow/ImageShow;->mOriginalTextMargin:I

    add-int/2addr v1, v2

    int-to-float v1, v1

    iget-object v2, p0, Lcom/android/gallery3d/filtershow/imageshow/ImageShow;->mImageBounds:Landroid/graphics/Rect;

    iget v2, v2, Landroid/graphics/Rect;->top:I

    invoke-virtual {v6}, Landroid/graphics/Rect;->height()I

    move-result v3

    add-int/2addr v2, v3

    sget v3, Lcom/android/gallery3d/filtershow/imageshow/ImageShow;->mOriginalTextMargin:I

    add-int/2addr v2, v3

    int-to-float v2, v2

    invoke-virtual {p1, v0, v1, v2, v5}, Landroid/graphics/Canvas;->drawText(Ljava/lang/String;FFLandroid/graphics/Paint;)V

    :cond_2
    invoke-virtual {p1}, Landroid/graphics/Canvas;->restore()V

    goto/16 :goto_0

    :cond_3
    sget v0, Lcom/android/gallery3d/filtershow/imageshow/ImageShow;->UNVEIL_HORIZONTAL:I

    iput v0, p0, Lcom/android/gallery3d/filtershow/imageshow/ImageShow;->mShowOriginalDirection:I

    goto/16 :goto_1

    :cond_4
    iget v0, p0, Lcom/android/gallery3d/filtershow/imageshow/ImageShow;->mTouchX:F

    iget-object v1, p0, Lcom/android/gallery3d/filtershow/imageshow/ImageShow;->mImageBounds:Landroid/graphics/Rect;

    iget v1, v1, Landroid/graphics/Rect;->left:I

    int-to-float v1, v1

    sub-float/2addr v0, v1

    float-to-int v8, v0

    iget-object v0, p0, Lcom/android/gallery3d/filtershow/imageshow/ImageShow;->mImageBounds:Landroid/graphics/Rect;

    invoke-virtual {v0}, Landroid/graphics/Rect;->height()I

    move-result v9

    goto/16 :goto_2

    :cond_5
    iget v0, p0, Lcom/android/gallery3d/filtershow/imageshow/ImageShow;->mTouchX:F

    sub-float v1, v0, v4

    iget-object v0, p0, Lcom/android/gallery3d/filtershow/imageshow/ImageShow;->mImageBounds:Landroid/graphics/Rect;

    iget v0, v0, Landroid/graphics/Rect;->top:I

    int-to-float v2, v0

    iget v0, p0, Lcom/android/gallery3d/filtershow/imageshow/ImageShow;->mTouchX:F

    sub-float v3, v0, v4

    iget-object v0, p0, Lcom/android/gallery3d/filtershow/imageshow/ImageShow;->mImageBounds:Landroid/graphics/Rect;

    iget v0, v0, Landroid/graphics/Rect;->bottom:I

    int-to-float v4, v0

    move-object v0, p1

    invoke-virtual/range {v0 .. v5}, Landroid/graphics/Canvas;->drawLine(FFFFLandroid/graphics/Paint;)V

    goto/16 :goto_3
.end method

.method public drawToast(Landroid/graphics/Canvas;)V
    .locals 9
    .param p1    # Landroid/graphics/Canvas;

    const/4 v8, 0x0

    const/16 v7, 0xff

    iget-boolean v4, p0, Lcom/android/gallery3d/filtershow/imageshow/ImageShow;->mShowToast:Z

    if-eqz v4, :cond_0

    iget-object v4, p0, Lcom/android/gallery3d/filtershow/imageshow/ImageShow;->mToast:Ljava/lang/String;

    if-eqz v4, :cond_0

    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0}, Landroid/graphics/Paint;-><init>()V

    const/high16 v4, 0x43000000

    invoke-virtual {v0, v4}, Landroid/graphics/Paint;->setTextSize(F)V

    iget-object v4, p0, Lcom/android/gallery3d/filtershow/imageshow/ImageShow;->mToast:Ljava/lang/String;

    invoke-virtual {v0, v4}, Landroid/graphics/Paint;->measureText(Ljava/lang/String;)F

    move-result v1

    invoke-virtual {p0}, Landroid/view/View;->getWidth()I

    move-result v4

    int-to-float v4, v4

    sub-float/2addr v4, v1

    const/high16 v5, 0x40000000

    div-float/2addr v4, v5

    float-to-int v2, v4

    invoke-virtual {p0}, Landroid/view/View;->getHeight()I

    move-result v4

    int-to-float v4, v4

    const/high16 v5, 0x40400000

    div-float/2addr v4, v5

    float-to-int v3, v4

    invoke-virtual {v0, v7, v8, v8, v8}, Landroid/graphics/Paint;->setARGB(IIII)V

    iget-object v4, p0, Lcom/android/gallery3d/filtershow/imageshow/ImageShow;->mToast:Ljava/lang/String;

    add-int/lit8 v5, v2, -0x2

    int-to-float v5, v5

    add-int/lit8 v6, v3, -0x2

    int-to-float v6, v6

    invoke-virtual {p1, v4, v5, v6, v0}, Landroid/graphics/Canvas;->drawText(Ljava/lang/String;FFLandroid/graphics/Paint;)V

    iget-object v4, p0, Lcom/android/gallery3d/filtershow/imageshow/ImageShow;->mToast:Ljava/lang/String;

    add-int/lit8 v5, v2, -0x2

    int-to-float v5, v5

    int-to-float v6, v3

    invoke-virtual {p1, v4, v5, v6, v0}, Landroid/graphics/Canvas;->drawText(Ljava/lang/String;FFLandroid/graphics/Paint;)V

    iget-object v4, p0, Lcom/android/gallery3d/filtershow/imageshow/ImageShow;->mToast:Ljava/lang/String;

    int-to-float v5, v2

    add-int/lit8 v6, v3, -0x2

    int-to-float v6, v6

    invoke-virtual {p1, v4, v5, v6, v0}, Landroid/graphics/Canvas;->drawText(Ljava/lang/String;FFLandroid/graphics/Paint;)V

    iget-object v4, p0, Lcom/android/gallery3d/filtershow/imageshow/ImageShow;->mToast:Ljava/lang/String;

    add-int/lit8 v5, v2, 0x2

    int-to-float v5, v5

    add-int/lit8 v6, v3, 0x2

    int-to-float v6, v6

    invoke-virtual {p1, v4, v5, v6, v0}, Landroid/graphics/Canvas;->drawText(Ljava/lang/String;FFLandroid/graphics/Paint;)V

    iget-object v4, p0, Lcom/android/gallery3d/filtershow/imageshow/ImageShow;->mToast:Ljava/lang/String;

    add-int/lit8 v5, v2, 0x2

    int-to-float v5, v5

    int-to-float v6, v3

    invoke-virtual {p1, v4, v5, v6, v0}, Landroid/graphics/Canvas;->drawText(Ljava/lang/String;FFLandroid/graphics/Paint;)V

    iget-object v4, p0, Lcom/android/gallery3d/filtershow/imageshow/ImageShow;->mToast:Ljava/lang/String;

    int-to-float v5, v2

    add-int/lit8 v6, v3, 0x2

    int-to-float v6, v6

    invoke-virtual {p1, v4, v5, v6, v0}, Landroid/graphics/Canvas;->drawText(Ljava/lang/String;FFLandroid/graphics/Paint;)V

    iget-boolean v4, p0, Lcom/android/gallery3d/filtershow/imageshow/ImageShow;->mImportantToast:Z

    if-eqz v4, :cond_1

    const/16 v4, 0xc8

    invoke-virtual {v0, v7, v4, v8, v8}, Landroid/graphics/Paint;->setARGB(IIII)V

    :goto_0
    iget-object v4, p0, Lcom/android/gallery3d/filtershow/imageshow/ImageShow;->mToast:Ljava/lang/String;

    int-to-float v5, v2

    int-to-float v6, v3

    invoke-virtual {p1, v4, v5, v6, v0}, Landroid/graphics/Canvas;->drawText(Ljava/lang/String;FFLandroid/graphics/Paint;)V

    :cond_0
    return-void

    :cond_1
    invoke-virtual {v0, v7, v7, v7, v7}, Landroid/graphics/Paint;->setARGB(IIII)V

    goto :goto_0
.end method

.method public getCurrentFilter()Lcom/android/gallery3d/filtershow/filters/ImageFilter;
    .locals 1

    iget-object v0, p0, Lcom/android/gallery3d/filtershow/imageshow/ImageShow;->mCurrentFilter:Lcom/android/gallery3d/filtershow/filters/ImageFilter;

    return-object v0
.end method

.method public getDefaultBackgroundColor()I
    .locals 1

    sget v0, Lcom/android/gallery3d/filtershow/imageshow/ImageShow;->mBackgroundColor:I

    return v0
.end method

.method protected getDirtyGeometryFlag()Z
    .locals 1

    iget-boolean v0, p0, Lcom/android/gallery3d/filtershow/imageshow/ImageShow;->mDirtyGeometry:Z

    return v0
.end method

.method public getDisplayedImageBounds()Landroid/graphics/Rect;
    .locals 1

    iget-object v0, p0, Lcom/android/gallery3d/filtershow/imageshow/ImageShow;->mImageBounds:Landroid/graphics/Rect;

    return-object v0
.end method

.method public getFilteredImage()Landroid/graphics/Bitmap;
    .locals 1

    iget-object v0, p0, Lcom/android/gallery3d/filtershow/imageshow/ImageShow;->mFilteredImage:Landroid/graphics/Bitmap;

    return-object v0
.end method

.method public getFiltersOnlyImage()Landroid/graphics/Bitmap;
    .locals 1

    iget-object v0, p0, Lcom/android/gallery3d/filtershow/imageshow/ImageShow;->mFiltersOnlyImage:Landroid/graphics/Bitmap;

    return-object v0
.end method

.method protected getGeometry()Lcom/android/gallery3d/filtershow/imageshow/GeometryMetadata;
    .locals 2

    new-instance v0, Lcom/android/gallery3d/filtershow/imageshow/GeometryMetadata;

    invoke-virtual {p0}, Lcom/android/gallery3d/filtershow/imageshow/ImageShow;->getImagePreset()Lcom/android/gallery3d/filtershow/presets/ImagePreset;

    move-result-object v1

    iget-object v1, v1, Lcom/android/gallery3d/filtershow/presets/ImagePreset;->mGeoData:Lcom/android/gallery3d/filtershow/imageshow/GeometryMetadata;

    invoke-direct {v0, v1}, Lcom/android/gallery3d/filtershow/imageshow/GeometryMetadata;-><init>(Lcom/android/gallery3d/filtershow/imageshow/GeometryMetadata;)V

    return-object v0
.end method

.method public getGeometryOnlyImage()Landroid/graphics/Bitmap;
    .locals 1

    iget-object v0, p0, Lcom/android/gallery3d/filtershow/imageshow/ImageShow;->mGeometryOnlyImage:Landroid/graphics/Bitmap;

    return-object v0
.end method

.method public getHistory()Lcom/android/gallery3d/filtershow/HistoryAdapter;
    .locals 1

    iget-object v0, p0, Lcom/android/gallery3d/filtershow/imageshow/ImageShow;->mHistoryAdapter:Lcom/android/gallery3d/filtershow/HistoryAdapter;

    return-object v0
.end method

.method public getImageBounds()Landroid/graphics/Rect;
    .locals 2

    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    invoke-virtual {p0}, Lcom/android/gallery3d/filtershow/imageshow/ImageShow;->getImagePreset()Lcom/android/gallery3d/filtershow/presets/ImagePreset;

    move-result-object v1

    iget-object v1, v1, Lcom/android/gallery3d/filtershow/presets/ImagePreset;->mGeoData:Lcom/android/gallery3d/filtershow/imageshow/GeometryMetadata;

    invoke-virtual {v1}, Lcom/android/gallery3d/filtershow/imageshow/GeometryMetadata;->getPhotoBounds()Landroid/graphics/RectF;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/graphics/RectF;->roundOut(Landroid/graphics/Rect;)V

    return-object v0
.end method

.method public getImagePreset()Lcom/android/gallery3d/filtershow/presets/ImagePreset;
    .locals 1

    iget-object v0, p0, Lcom/android/gallery3d/filtershow/imageshow/ImageShow;->mImagePreset:Lcom/android/gallery3d/filtershow/presets/ImagePreset;

    return-object v0
.end method

.method public getImageRotation()F
    .locals 1

    invoke-virtual {p0}, Lcom/android/gallery3d/filtershow/imageshow/ImageShow;->getImagePreset()Lcom/android/gallery3d/filtershow/presets/ImagePreset;

    move-result-object v0

    iget-object v0, v0, Lcom/android/gallery3d/filtershow/presets/ImagePreset;->mGeoData:Lcom/android/gallery3d/filtershow/imageshow/GeometryMetadata;

    invoke-virtual {v0}, Lcom/android/gallery3d/filtershow/imageshow/GeometryMetadata;->getRotation()F

    move-result v0

    return v0
.end method

.method public getImageRotationZoomFactor()F
    .locals 1

    invoke-virtual {p0}, Lcom/android/gallery3d/filtershow/imageshow/ImageShow;->getImagePreset()Lcom/android/gallery3d/filtershow/presets/ImagePreset;

    move-result-object v0

    iget-object v0, v0, Lcom/android/gallery3d/filtershow/presets/ImagePreset;->mGeoData:Lcom/android/gallery3d/filtershow/imageshow/GeometryMetadata;

    invoke-virtual {v0}, Lcom/android/gallery3d/filtershow/imageshow/GeometryMetadata;->getScaleFactor()F

    move-result v0

    return v0
.end method

.method public getImageStateAdapter()Landroid/widget/ArrayAdapter;
    .locals 1

    iget-object v0, p0, Lcom/android/gallery3d/filtershow/imageshow/ImageShow;->mImageStateAdapter:Lcom/android/gallery3d/filtershow/ImageStateAdapter;

    return-object v0
.end method

.method public getPanelController()Lcom/android/gallery3d/filtershow/PanelController;
    .locals 1

    iget-object v0, p0, Lcom/android/gallery3d/filtershow/imageshow/ImageShow;->mController:Lcom/android/gallery3d/filtershow/PanelController;

    return-object v0
.end method

.method public hasModifications()Z
    .locals 1

    invoke-virtual {p0}, Lcom/android/gallery3d/filtershow/imageshow/ImageShow;->getImagePreset()Lcom/android/gallery3d/filtershow/presets/ImagePreset;

    move-result-object v0

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_0
    invoke-virtual {p0}, Lcom/android/gallery3d/filtershow/imageshow/ImageShow;->getImagePreset()Lcom/android/gallery3d/filtershow/presets/ImagePreset;

    move-result-object v0

    invoke-virtual {v0}, Lcom/android/gallery3d/filtershow/presets/ImagePreset;->hasModifications()Z

    move-result v0

    goto :goto_0
.end method

.method public imageLoaded()V
    .locals 0

    invoke-virtual {p0}, Lcom/android/gallery3d/filtershow/imageshow/ImageShow;->updateImage()V

    invoke-virtual {p0}, Landroid/view/View;->invalidate()V

    return-void
.end method

.method public onDoubleTap(Landroid/view/MotionEvent;)Z
    .locals 1
    .param p1    # Landroid/view/MotionEvent;

    const/4 v0, 0x0

    return v0
.end method

.method public onDoubleTapEvent(Landroid/view/MotionEvent;)Z
    .locals 1
    .param p1    # Landroid/view/MotionEvent;

    const/4 v0, 0x0

    return v0
.end method

.method public onDown(Landroid/view/MotionEvent;)Z
    .locals 1
    .param p1    # Landroid/view/MotionEvent;

    const/4 v0, 0x0

    return v0
.end method

.method public onDraw(Landroid/graphics/Canvas;)V
    .locals 6
    .param p1    # Landroid/graphics/Canvas;

    const/16 v5, 0xc8

    const/4 v4, 0x0

    invoke-virtual {p0, p1}, Lcom/android/gallery3d/filtershow/imageshow/ImageShow;->drawBackground(Landroid/graphics/Canvas;)V

    invoke-virtual {p0}, Lcom/android/gallery3d/filtershow/imageshow/ImageShow;->requestFilteredImages()V

    invoke-virtual {p0, p1}, Lcom/android/gallery3d/filtershow/imageshow/ImageShow;->defaultDrawImage(Landroid/graphics/Canvas;)V

    invoke-virtual {p0}, Lcom/android/gallery3d/filtershow/imageshow/ImageShow;->showTitle()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-virtual {p0}, Lcom/android/gallery3d/filtershow/imageshow/ImageShow;->getImagePreset()Lcom/android/gallery3d/filtershow/presets/ImagePreset;

    move-result-object v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/android/gallery3d/filtershow/imageshow/ImageShow;->mPaint:Landroid/graphics/Paint;

    invoke-virtual {v1, v5, v4, v4, v4}, Landroid/graphics/Paint;->setARGB(IIII)V

    iget-object v1, p0, Lcom/android/gallery3d/filtershow/imageshow/ImageShow;->mPaint:Landroid/graphics/Paint;

    sget v2, Lcom/android/gallery3d/filtershow/imageshow/ImageShow;->mTextSize:I

    int-to-float v2, v2

    invoke-virtual {v1, v2}, Landroid/graphics/Paint;->setTextSize(F)V

    new-instance v0, Landroid/graphics/Rect;

    invoke-virtual {p0}, Landroid/view/View;->getWidth()I

    move-result v1

    sget v2, Lcom/android/gallery3d/filtershow/imageshow/ImageShow;->mTextSize:I

    sget v3, Lcom/android/gallery3d/filtershow/imageshow/ImageShow;->mTextPadding:I

    add-int/2addr v2, v3

    invoke-direct {v0, v4, v4, v1, v2}, Landroid/graphics/Rect;-><init>(IIII)V

    iget-object v1, p0, Lcom/android/gallery3d/filtershow/imageshow/ImageShow;->mPaint:Landroid/graphics/Paint;

    invoke-virtual {p1, v0, v1}, Landroid/graphics/Canvas;->drawRect(Landroid/graphics/Rect;Landroid/graphics/Paint;)V

    iget-object v1, p0, Lcom/android/gallery3d/filtershow/imageshow/ImageShow;->mPaint:Landroid/graphics/Paint;

    const/16 v2, 0xff

    invoke-virtual {v1, v2, v5, v5, v5}, Landroid/graphics/Paint;->setARGB(IIII)V

    invoke-virtual {p0}, Lcom/android/gallery3d/filtershow/imageshow/ImageShow;->getImagePreset()Lcom/android/gallery3d/filtershow/presets/ImagePreset;

    move-result-object v1

    invoke-virtual {v1}, Lcom/android/gallery3d/filtershow/presets/ImagePreset;->name()Ljava/lang/String;

    move-result-object v1

    sget v2, Lcom/android/gallery3d/filtershow/imageshow/ImageShow;->mTextPadding:I

    int-to-float v2, v2

    const/high16 v3, 0x3fc00000

    sget v4, Lcom/android/gallery3d/filtershow/imageshow/ImageShow;->mTextPadding:I

    int-to-float v4, v4

    mul-float/2addr v3, v4

    iget-object v4, p0, Lcom/android/gallery3d/filtershow/imageshow/ImageShow;->mPaint:Landroid/graphics/Paint;

    invoke-virtual {p1, v1, v2, v3, v4}, Landroid/graphics/Canvas;->drawText(Ljava/lang/String;FFLandroid/graphics/Paint;)V

    :cond_0
    invoke-virtual {p0}, Lcom/android/gallery3d/filtershow/imageshow/ImageShow;->showControls()Z

    move-result v1

    if-eqz v1, :cond_1

    :cond_1
    invoke-virtual {p0, p1}, Lcom/android/gallery3d/filtershow/imageshow/ImageShow;->drawToast(Landroid/graphics/Canvas;)V

    return-void
.end method

.method public onFling(Landroid/view/MotionEvent;Landroid/view/MotionEvent;FF)Z
    .locals 4
    .param p1    # Landroid/view/MotionEvent;
    .param p2    # Landroid/view/MotionEvent;
    .param p3    # F
    .param p4    # F

    iget-object v0, p0, Lcom/android/gallery3d/filtershow/imageshow/ImageShow;->mActivity:Lcom/android/gallery3d/filtershow/FilterShowActivity;

    invoke-virtual {v0}, Lcom/android/gallery3d/filtershow/FilterShowActivity;->isShowingHistoryPanel()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v0

    invoke-virtual {p2}, Landroid/view/MotionEvent;->getX()F

    move-result v1

    cmpl-float v0, v0, v1

    if-gtz v0, :cond_1

    :cond_0
    iget-object v0, p0, Lcom/android/gallery3d/filtershow/imageshow/ImageShow;->mActivity:Lcom/android/gallery3d/filtershow/FilterShowActivity;

    invoke-virtual {v0}, Lcom/android/gallery3d/filtershow/FilterShowActivity;->isShowingHistoryPanel()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-virtual {p2}, Landroid/view/MotionEvent;->getX()F

    move-result v0

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v1

    cmpl-float v0, v0, v1

    if-lez v0, :cond_3

    :cond_1
    iget-boolean v0, p0, Lcom/android/gallery3d/filtershow/imageshow/ImageShow;->mTouchShowOriginal:Z

    if-eqz v0, :cond_2

    iget-boolean v0, p0, Lcom/android/gallery3d/filtershow/imageshow/ImageShow;->mTouchShowOriginal:Z

    if-eqz v0, :cond_3

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    iget-wide v2, p0, Lcom/android/gallery3d/filtershow/imageshow/ImageShow;->mTouchShowOriginalDate:J

    sub-long/2addr v0, v2

    const-wide/16 v2, 0x12c

    cmp-long v0, v0, v2

    if-gez v0, :cond_3

    :cond_2
    iget-object v0, p0, Lcom/android/gallery3d/filtershow/imageshow/ImageShow;->mActivity:Lcom/android/gallery3d/filtershow/FilterShowActivity;

    invoke-virtual {v0}, Lcom/android/gallery3d/filtershow/FilterShowActivity;->toggleHistoryPanel()V

    :cond_3
    const/4 v0, 0x1

    return v0
.end method

.method public onItemClick(I)V
    .locals 2
    .param p1    # I

    new-instance v1, Lcom/android/gallery3d/filtershow/presets/ImagePreset;

    iget-object v0, p0, Lcom/android/gallery3d/filtershow/imageshow/ImageShow;->mHistoryAdapter:Lcom/android/gallery3d/filtershow/HistoryAdapter;

    invoke-virtual {v0, p1}, Landroid/widget/ArrayAdapter;->getItem(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/gallery3d/filtershow/presets/ImagePreset;

    invoke-direct {v1, v0}, Lcom/android/gallery3d/filtershow/presets/ImagePreset;-><init>(Lcom/android/gallery3d/filtershow/presets/ImagePreset;)V

    const/4 v0, 0x0

    invoke-virtual {p0, v1, v0}, Lcom/android/gallery3d/filtershow/imageshow/ImageShow;->setImagePreset(Lcom/android/gallery3d/filtershow/presets/ImagePreset;Z)V

    iget-object v0, p0, Lcom/android/gallery3d/filtershow/imageshow/ImageShow;->mHistoryAdapter:Lcom/android/gallery3d/filtershow/HistoryAdapter;

    invoke-virtual {v0, p1}, Lcom/android/gallery3d/filtershow/HistoryAdapter;->setCurrentPreset(I)V

    return-void
.end method

.method public onLongPress(Landroid/view/MotionEvent;)V
    .locals 0
    .param p1    # Landroid/view/MotionEvent;

    return-void
.end method

.method protected onMeasure(II)V
    .locals 2
    .param p1    # I
    .param p2    # I

    invoke-static {p1}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v1

    invoke-static {p2}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v0

    invoke-virtual {p0, v1, v0}, Landroid/view/View;->setMeasuredDimension(II)V

    return-void
.end method

.method public onNewValue(I)V
    .locals 4
    .param p1    # I

    const/16 v0, 0x64

    const/16 v1, -0x64

    invoke-virtual {p0}, Lcom/android/gallery3d/filtershow/imageshow/ImageShow;->getCurrentFilter()Lcom/android/gallery3d/filtershow/filters/ImageFilter;

    move-result-object v2

    if-eqz v2, :cond_0

    invoke-virtual {p0}, Lcom/android/gallery3d/filtershow/imageshow/ImageShow;->getCurrentFilter()Lcom/android/gallery3d/filtershow/filters/ImageFilter;

    move-result-object v2

    invoke-virtual {v2, p1}, Lcom/android/gallery3d/filtershow/filters/ImageFilter;->setParameter(I)V

    invoke-virtual {p0}, Lcom/android/gallery3d/filtershow/imageshow/ImageShow;->getCurrentFilter()Lcom/android/gallery3d/filtershow/filters/ImageFilter;

    move-result-object v2

    invoke-virtual {v2}, Lcom/android/gallery3d/filtershow/filters/ImageFilter;->getMaxParameter()I

    move-result v0

    invoke-virtual {p0}, Lcom/android/gallery3d/filtershow/imageshow/ImageShow;->getCurrentFilter()Lcom/android/gallery3d/filtershow/filters/ImageFilter;

    move-result-object v2

    invoke-virtual {v2}, Lcom/android/gallery3d/filtershow/filters/ImageFilter;->getMinParameter()I

    move-result v1

    :cond_0
    invoke-virtual {p0}, Lcom/android/gallery3d/filtershow/imageshow/ImageShow;->getImagePreset()Lcom/android/gallery3d/filtershow/presets/ImagePreset;

    move-result-object v2

    if-eqz v2, :cond_1

    iget-object v2, p0, Lcom/android/gallery3d/filtershow/imageshow/ImageShow;->mImageLoader:Lcom/android/gallery3d/filtershow/cache/ImageLoader;

    invoke-virtual {p0}, Lcom/android/gallery3d/filtershow/imageshow/ImageShow;->getImagePreset()Lcom/android/gallery3d/filtershow/presets/ImagePreset;

    move-result-object v3

    invoke-virtual {v2, v3, p0}, Lcom/android/gallery3d/filtershow/cache/ImageLoader;->resetImageForPreset(Lcom/android/gallery3d/filtershow/presets/ImagePreset;Lcom/android/gallery3d/filtershow/imageshow/ImageShow;)V

    invoke-virtual {p0}, Lcom/android/gallery3d/filtershow/imageshow/ImageShow;->getImagePreset()Lcom/android/gallery3d/filtershow/presets/ImagePreset;

    move-result-object v2

    iget-object v3, p0, Lcom/android/gallery3d/filtershow/imageshow/ImageShow;->mImageStateAdapter:Lcom/android/gallery3d/filtershow/ImageStateAdapter;

    invoke-virtual {v2, v3}, Lcom/android/gallery3d/filtershow/presets/ImagePreset;->fillImageStateAdapter(Lcom/android/gallery3d/filtershow/ImageStateAdapter;)V

    :cond_1
    invoke-virtual {p0}, Lcom/android/gallery3d/filtershow/imageshow/ImageShow;->getPanelController()Lcom/android/gallery3d/filtershow/PanelController;

    move-result-object v2

    if-eqz v2, :cond_2

    invoke-virtual {p0}, Lcom/android/gallery3d/filtershow/imageshow/ImageShow;->getPanelController()Lcom/android/gallery3d/filtershow/PanelController;

    move-result-object v2

    invoke-virtual {v2, p1}, Lcom/android/gallery3d/filtershow/PanelController;->onNewValue(I)V

    :cond_2
    invoke-virtual {p0, p1, v1, v0}, Lcom/android/gallery3d/filtershow/imageshow/ImageShow;->updateSeekBar(III)V

    invoke-virtual {p0}, Landroid/view/View;->invalidate()V

    return-void
.end method

.method public onProgressChanged(Landroid/widget/SeekBar;IZ)V
    .locals 4
    .param p1    # Landroid/widget/SeekBar;
    .param p2    # I
    .param p3    # Z

    move v2, p2

    invoke-virtual {p0}, Lcom/android/gallery3d/filtershow/imageshow/ImageShow;->getCurrentFilter()Lcom/android/gallery3d/filtershow/filters/ImageFilter;

    move-result-object v3

    if-eqz v3, :cond_0

    invoke-virtual {p0}, Lcom/android/gallery3d/filtershow/imageshow/ImageShow;->getCurrentFilter()Lcom/android/gallery3d/filtershow/filters/ImageFilter;

    move-result-object v3

    invoke-virtual {v3}, Lcom/android/gallery3d/filtershow/filters/ImageFilter;->getMaxParameter()I

    move-result v0

    invoke-virtual {p0}, Lcom/android/gallery3d/filtershow/imageshow/ImageShow;->getCurrentFilter()Lcom/android/gallery3d/filtershow/filters/ImageFilter;

    move-result-object v3

    invoke-virtual {v3}, Lcom/android/gallery3d/filtershow/filters/ImageFilter;->getMinParameter()I

    move-result v1

    invoke-virtual {p1}, Landroid/widget/ProgressBar;->getMax()I

    move-result v3

    invoke-direct {p0, p2, v1, v0, v3}, Lcom/android/gallery3d/filtershow/imageshow/ImageShow;->uiToParameter(IIII)I

    move-result v2

    :cond_0
    invoke-virtual {p0, v2}, Lcom/android/gallery3d/filtershow/imageshow/ImageShow;->onNewValue(I)V

    return-void
.end method

.method public onScroll(Landroid/view/MotionEvent;Landroid/view/MotionEvent;FF)Z
    .locals 1
    .param p1    # Landroid/view/MotionEvent;
    .param p2    # Landroid/view/MotionEvent;
    .param p3    # F
    .param p4    # F

    const/4 v0, 0x0

    return v0
.end method

.method public onShowPress(Landroid/view/MotionEvent;)V
    .locals 0
    .param p1    # Landroid/view/MotionEvent;

    return-void
.end method

.method public onSingleTapConfirmed(Landroid/view/MotionEvent;)Z
    .locals 1
    .param p1    # Landroid/view/MotionEvent;

    const/4 v0, 0x0

    return v0
.end method

.method public onSingleTapUp(Landroid/view/MotionEvent;)Z
    .locals 1
    .param p1    # Landroid/view/MotionEvent;

    const/4 v0, 0x0

    return v0
.end method

.method public onStartTrackingTouch(Landroid/widget/SeekBar;)V
    .locals 0
    .param p1    # Landroid/widget/SeekBar;

    return-void
.end method

.method public onStopTrackingTouch(Landroid/widget/SeekBar;)V
    .locals 0
    .param p1    # Landroid/widget/SeekBar;

    return-void
.end method

.method public onTouchDown(FF)V
    .locals 0
    .param p1    # F
    .param p2    # F

    iput p1, p0, Lcom/android/gallery3d/filtershow/imageshow/ImageShow;->mTouchX:F

    iput p2, p0, Lcom/android/gallery3d/filtershow/imageshow/ImageShow;->mTouchY:F

    invoke-virtual {p0}, Landroid/view/View;->invalidate()V

    return-void
.end method

.method public onTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 9
    .param p1    # Landroid/view/MotionEvent;

    const/4 v8, 0x0

    const/4 v7, 0x1

    const/4 v6, 0x0

    invoke-super {p0, p1}, Landroid/view/View;->onTouchEvent(Landroid/view/MotionEvent;)Z

    iget-object v2, p0, Lcom/android/gallery3d/filtershow/imageshow/ImageShow;->mGestureDetector:Landroid/view/GestureDetector;

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/android/gallery3d/filtershow/imageshow/ImageShow;->mGestureDetector:Landroid/view/GestureDetector;

    invoke-virtual {v2, p1}, Landroid/view/GestureDetector;->onTouchEvent(Landroid/view/MotionEvent;)Z

    :cond_0
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v2

    float-to-int v0, v2

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v2

    float-to-int v1, v2

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v2

    if-nez v2, :cond_1

    iput v0, p0, Lcom/android/gallery3d/filtershow/imageshow/ImageShow;->mTouchDownX:I

    iput v1, p0, Lcom/android/gallery3d/filtershow/imageshow/ImageShow;->mTouchDownY:I

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    iput-wide v2, p0, Lcom/android/gallery3d/filtershow/imageshow/ImageShow;->mTouchShowOriginalDate:J

    iput v6, p0, Lcom/android/gallery3d/filtershow/imageshow/ImageShow;->mShowOriginalDirection:I

    :cond_1
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v2

    const/4 v3, 0x2

    if-ne v2, v3, :cond_2

    int-to-float v2, v0

    iput v2, p0, Lcom/android/gallery3d/filtershow/imageshow/ImageShow;->mTouchX:F

    int-to-float v2, v1

    iput v2, p0, Lcom/android/gallery3d/filtershow/imageshow/ImageShow;->mTouchY:F

    iget-object v2, p0, Lcom/android/gallery3d/filtershow/imageshow/ImageShow;->mActivity:Lcom/android/gallery3d/filtershow/FilterShowActivity;

    invoke-virtual {v2}, Lcom/android/gallery3d/filtershow/FilterShowActivity;->isShowingHistoryPanel()Z

    move-result v2

    if-nez v2, :cond_2

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    iget-wide v4, p0, Lcom/android/gallery3d/filtershow/imageshow/ImageShow;->mTouchShowOriginalDate:J

    sub-long/2addr v2, v4

    const-wide/16 v4, 0xc8

    cmp-long v2, v2, v4

    if-lez v2, :cond_2

    iput-boolean v7, p0, Lcom/android/gallery3d/filtershow/imageshow/ImageShow;->mTouchShowOriginal:Z

    :cond_2
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v2

    if-ne v2, v7, :cond_3

    iput-boolean v6, p0, Lcom/android/gallery3d/filtershow/imageshow/ImageShow;->mTouchShowOriginal:Z

    iput v6, p0, Lcom/android/gallery3d/filtershow/imageshow/ImageShow;->mTouchDownX:I

    iput v6, p0, Lcom/android/gallery3d/filtershow/imageshow/ImageShow;->mTouchDownY:I

    iput v8, p0, Lcom/android/gallery3d/filtershow/imageshow/ImageShow;->mTouchX:F

    iput v8, p0, Lcom/android/gallery3d/filtershow/imageshow/ImageShow;->mTouchY:F

    :cond_3
    invoke-virtual {p0}, Landroid/view/View;->invalidate()V

    return v7
.end method

.method public onTouchUp()V
    .locals 0

    return-void
.end method

.method public requestFilteredImages()V
    .locals 4

    iget-object v1, p0, Lcom/android/gallery3d/filtershow/imageshow/ImageShow;->mImageLoader:Lcom/android/gallery3d/filtershow/cache/ImageLoader;

    if-eqz v1, :cond_3

    iget-object v1, p0, Lcom/android/gallery3d/filtershow/imageshow/ImageShow;->mImageLoader:Lcom/android/gallery3d/filtershow/cache/ImageLoader;

    invoke-virtual {p0}, Lcom/android/gallery3d/filtershow/imageshow/ImageShow;->getImagePreset()Lcom/android/gallery3d/filtershow/presets/ImagePreset;

    move-result-object v2

    invoke-virtual {p0}, Lcom/android/gallery3d/filtershow/imageshow/ImageShow;->showHires()Z

    move-result v3

    invoke-virtual {v1, p0, v2, v3}, Lcom/android/gallery3d/filtershow/cache/ImageLoader;->getImageForPreset(Lcom/android/gallery3d/filtershow/imageshow/ImageShow;Lcom/android/gallery3d/filtershow/presets/ImagePreset;Z)Landroid/graphics/Bitmap;

    move-result-object v0

    if-eqz v0, :cond_1

    iget-object v1, p0, Lcom/android/gallery3d/filtershow/imageshow/ImageShow;->mFilteredImage:Landroid/graphics/Bitmap;

    if-nez v1, :cond_0

    invoke-virtual {p0}, Landroid/view/View;->invalidate()V

    :cond_0
    iput-object v0, p0, Lcom/android/gallery3d/filtershow/imageshow/ImageShow;->mFilteredImage:Landroid/graphics/Bitmap;

    :cond_1
    const/4 v1, 0x0

    invoke-virtual {p0, v1}, Lcom/android/gallery3d/filtershow/imageshow/ImageShow;->updateImagePresets(Z)V

    iget-object v1, p0, Lcom/android/gallery3d/filtershow/imageshow/ImageShow;->mImageGeometryOnlyPreset:Lcom/android/gallery3d/filtershow/presets/ImagePreset;

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/android/gallery3d/filtershow/imageshow/ImageShow;->mImageLoader:Lcom/android/gallery3d/filtershow/cache/ImageLoader;

    iget-object v2, p0, Lcom/android/gallery3d/filtershow/imageshow/ImageShow;->mImageGeometryOnlyPreset:Lcom/android/gallery3d/filtershow/presets/ImagePreset;

    invoke-virtual {p0}, Lcom/android/gallery3d/filtershow/imageshow/ImageShow;->showHires()Z

    move-result v3

    invoke-virtual {v1, p0, v2, v3}, Lcom/android/gallery3d/filtershow/cache/ImageLoader;->getImageForPreset(Lcom/android/gallery3d/filtershow/imageshow/ImageShow;Lcom/android/gallery3d/filtershow/presets/ImagePreset;Z)Landroid/graphics/Bitmap;

    move-result-object v0

    if-eqz v0, :cond_2

    iput-object v0, p0, Lcom/android/gallery3d/filtershow/imageshow/ImageShow;->mGeometryOnlyImage:Landroid/graphics/Bitmap;

    :cond_2
    iget-object v1, p0, Lcom/android/gallery3d/filtershow/imageshow/ImageShow;->mImageFiltersOnlyPreset:Lcom/android/gallery3d/filtershow/presets/ImagePreset;

    if-eqz v1, :cond_3

    iget-object v1, p0, Lcom/android/gallery3d/filtershow/imageshow/ImageShow;->mImageLoader:Lcom/android/gallery3d/filtershow/cache/ImageLoader;

    iget-object v2, p0, Lcom/android/gallery3d/filtershow/imageshow/ImageShow;->mImageFiltersOnlyPreset:Lcom/android/gallery3d/filtershow/presets/ImagePreset;

    invoke-virtual {p0}, Lcom/android/gallery3d/filtershow/imageshow/ImageShow;->showHires()Z

    move-result v3

    invoke-virtual {v1, p0, v2, v3}, Lcom/android/gallery3d/filtershow/cache/ImageLoader;->getImageForPreset(Lcom/android/gallery3d/filtershow/imageshow/ImageShow;Lcom/android/gallery3d/filtershow/presets/ImagePreset;Z)Landroid/graphics/Bitmap;

    move-result-object v0

    if-eqz v0, :cond_3

    iput-object v0, p0, Lcom/android/gallery3d/filtershow/imageshow/ImageShow;->mFiltersOnlyImage:Landroid/graphics/Bitmap;

    :cond_3
    iget-boolean v1, p0, Lcom/android/gallery3d/filtershow/imageshow/ImageShow;->mShowOriginal:Z

    if-eqz v1, :cond_4

    iget-object v1, p0, Lcom/android/gallery3d/filtershow/imageshow/ImageShow;->mGeometryOnlyImage:Landroid/graphics/Bitmap;

    iput-object v1, p0, Lcom/android/gallery3d/filtershow/imageshow/ImageShow;->mFilteredImage:Landroid/graphics/Bitmap;

    :cond_4
    return-void
.end method

.method public resetImageCaches(Lcom/android/gallery3d/filtershow/imageshow/ImageShow;)V
    .locals 1
    .param p1    # Lcom/android/gallery3d/filtershow/imageshow/ImageShow;

    iget-object v0, p0, Lcom/android/gallery3d/filtershow/imageshow/ImageShow;->mImageLoader:Lcom/android/gallery3d/filtershow/cache/ImageLoader;

    if-nez v0, :cond_0

    :goto_0
    return-void

    :cond_0
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/android/gallery3d/filtershow/imageshow/ImageShow;->updateImagePresets(Z)V

    goto :goto_0
.end method

.method public resetParameter()V
    .locals 2

    invoke-virtual {p0}, Lcom/android/gallery3d/filtershow/imageshow/ImageShow;->getCurrentFilter()Lcom/android/gallery3d/filtershow/filters/ImageFilter;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/android/gallery3d/filtershow/filters/ImageFilter;->getDefaultParameter()I

    move-result v1

    invoke-virtual {p0, v1}, Lcom/android/gallery3d/filtershow/imageshow/ImageShow;->onNewValue(I)V

    :cond_0
    return-void
.end method

.method public saveImage(Lcom/android/gallery3d/filtershow/FilterShowActivity;Ljava/io/File;)V
    .locals 2
    .param p1    # Lcom/android/gallery3d/filtershow/FilterShowActivity;
    .param p2    # Ljava/io/File;

    iget-object v0, p0, Lcom/android/gallery3d/filtershow/imageshow/ImageShow;->mImageLoader:Lcom/android/gallery3d/filtershow/cache/ImageLoader;

    invoke-virtual {p0}, Lcom/android/gallery3d/filtershow/imageshow/ImageShow;->getImagePreset()Lcom/android/gallery3d/filtershow/presets/ImagePreset;

    move-result-object v1

    invoke-virtual {v0, v1, p1, p2}, Lcom/android/gallery3d/filtershow/cache/ImageLoader;->saveImage(Lcom/android/gallery3d/filtershow/presets/ImagePreset;Lcom/android/gallery3d/filtershow/FilterShowActivity;Ljava/io/File;)V

    return-void
.end method

.method public select()V
    .locals 4

    invoke-virtual {p0}, Lcom/android/gallery3d/filtershow/imageshow/ImageShow;->getCurrentFilter()Lcom/android/gallery3d/filtershow/filters/ImageFilter;

    move-result-object v3

    if-eqz v3, :cond_0

    invoke-virtual {p0}, Lcom/android/gallery3d/filtershow/imageshow/ImageShow;->getCurrentFilter()Lcom/android/gallery3d/filtershow/filters/ImageFilter;

    move-result-object v3

    invoke-virtual {v3}, Lcom/android/gallery3d/filtershow/filters/ImageFilter;->getParameter()I

    move-result v2

    invoke-virtual {p0}, Lcom/android/gallery3d/filtershow/imageshow/ImageShow;->getCurrentFilter()Lcom/android/gallery3d/filtershow/filters/ImageFilter;

    move-result-object v3

    invoke-virtual {v3}, Lcom/android/gallery3d/filtershow/filters/ImageFilter;->getMaxParameter()I

    move-result v0

    invoke-virtual {p0}, Lcom/android/gallery3d/filtershow/imageshow/ImageShow;->getCurrentFilter()Lcom/android/gallery3d/filtershow/filters/ImageFilter;

    move-result-object v3

    invoke-virtual {v3}, Lcom/android/gallery3d/filtershow/filters/ImageFilter;->getMinParameter()I

    move-result v1

    invoke-virtual {p0, v2, v1, v0}, Lcom/android/gallery3d/filtershow/imageshow/ImageShow;->updateSeekBar(III)V

    :cond_0
    iget-object v3, p0, Lcom/android/gallery3d/filtershow/imageshow/ImageShow;->mSeekBar:Landroid/widget/SeekBar;

    if-eqz v3, :cond_1

    iget-object v3, p0, Lcom/android/gallery3d/filtershow/imageshow/ImageShow;->mSeekBar:Landroid/widget/SeekBar;

    invoke-virtual {v3, p0}, Landroid/widget/SeekBar;->setOnSeekBarChangeListener(Landroid/widget/SeekBar$OnSeekBarChangeListener;)V

    :cond_1
    return-void
.end method

.method public setAdapter(Lcom/android/gallery3d/filtershow/HistoryAdapter;)V
    .locals 0
    .param p1    # Lcom/android/gallery3d/filtershow/HistoryAdapter;

    iput-object p1, p0, Lcom/android/gallery3d/filtershow/imageshow/ImageShow;->mHistoryAdapter:Lcom/android/gallery3d/filtershow/HistoryAdapter;

    return-void
.end method

.method public setCurrentFilter(Lcom/android/gallery3d/filtershow/filters/ImageFilter;)V
    .locals 0
    .param p1    # Lcom/android/gallery3d/filtershow/filters/ImageFilter;

    iput-object p1, p0, Lcom/android/gallery3d/filtershow/imageshow/ImageShow;->mCurrentFilter:Lcom/android/gallery3d/filtershow/filters/ImageFilter;

    return-void
.end method

.method public setGeometry(Lcom/android/gallery3d/filtershow/imageshow/GeometryMetadata;)V
    .locals 1
    .param p1    # Lcom/android/gallery3d/filtershow/imageshow/GeometryMetadata;

    invoke-virtual {p0}, Lcom/android/gallery3d/filtershow/imageshow/ImageShow;->getImagePreset()Lcom/android/gallery3d/filtershow/presets/ImagePreset;

    move-result-object v0

    iget-object v0, v0, Lcom/android/gallery3d/filtershow/presets/ImagePreset;->mGeoData:Lcom/android/gallery3d/filtershow/imageshow/GeometryMetadata;

    invoke-virtual {v0, p1}, Lcom/android/gallery3d/filtershow/imageshow/GeometryMetadata;->set(Lcom/android/gallery3d/filtershow/imageshow/GeometryMetadata;)V

    return-void
.end method

.method public setImageLoader(Lcom/android/gallery3d/filtershow/cache/ImageLoader;)V
    .locals 2
    .param p1    # Lcom/android/gallery3d/filtershow/cache/ImageLoader;

    iput-object p1, p0, Lcom/android/gallery3d/filtershow/imageshow/ImageShow;->mImageLoader:Lcom/android/gallery3d/filtershow/cache/ImageLoader;

    iget-object v0, p0, Lcom/android/gallery3d/filtershow/imageshow/ImageShow;->mImageLoader:Lcom/android/gallery3d/filtershow/cache/ImageLoader;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/gallery3d/filtershow/imageshow/ImageShow;->mImageLoader:Lcom/android/gallery3d/filtershow/cache/ImageLoader;

    invoke-virtual {v0, p0}, Lcom/android/gallery3d/filtershow/cache/ImageLoader;->addListener(Lcom/android/gallery3d/filtershow/imageshow/ImageShow;)V

    iget-object v0, p0, Lcom/android/gallery3d/filtershow/imageshow/ImageShow;->mImagePreset:Lcom/android/gallery3d/filtershow/presets/ImagePreset;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/gallery3d/filtershow/imageshow/ImageShow;->mImagePreset:Lcom/android/gallery3d/filtershow/presets/ImagePreset;

    iget-object v1, p0, Lcom/android/gallery3d/filtershow/imageshow/ImageShow;->mImageLoader:Lcom/android/gallery3d/filtershow/cache/ImageLoader;

    invoke-virtual {v0, v1}, Lcom/android/gallery3d/filtershow/presets/ImagePreset;->setImageLoader(Lcom/android/gallery3d/filtershow/cache/ImageLoader;)V

    :cond_0
    return-void
.end method

.method public setImagePreset(Lcom/android/gallery3d/filtershow/presets/ImagePreset;)V
    .locals 1
    .param p1    # Lcom/android/gallery3d/filtershow/presets/ImagePreset;

    const/4 v0, 0x1

    invoke-virtual {p0, p1, v0}, Lcom/android/gallery3d/filtershow/imageshow/ImageShow;->setImagePreset(Lcom/android/gallery3d/filtershow/presets/ImagePreset;Z)V

    return-void
.end method

.method public setImagePreset(Lcom/android/gallery3d/filtershow/presets/ImagePreset;Z)V
    .locals 2
    .param p1    # Lcom/android/gallery3d/filtershow/presets/ImagePreset;
    .param p2    # Z

    if-nez p1, :cond_0

    :goto_0
    return-void

    :cond_0
    iput-object p1, p0, Lcom/android/gallery3d/filtershow/imageshow/ImageShow;->mImagePreset:Lcom/android/gallery3d/filtershow/presets/ImagePreset;

    invoke-virtual {p0}, Lcom/android/gallery3d/filtershow/imageshow/ImageShow;->getImagePreset()Lcom/android/gallery3d/filtershow/presets/ImagePreset;

    move-result-object v0

    iget-object v1, p0, Lcom/android/gallery3d/filtershow/imageshow/ImageShow;->mImageLoader:Lcom/android/gallery3d/filtershow/cache/ImageLoader;

    invoke-virtual {v0, v1}, Lcom/android/gallery3d/filtershow/presets/ImagePreset;->setImageLoader(Lcom/android/gallery3d/filtershow/cache/ImageLoader;)V

    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/android/gallery3d/filtershow/imageshow/ImageShow;->updateImagePresets(Z)V

    if-eqz p2, :cond_1

    iget-object v0, p0, Lcom/android/gallery3d/filtershow/imageshow/ImageShow;->mHistoryAdapter:Lcom/android/gallery3d/filtershow/HistoryAdapter;

    invoke-virtual {p0}, Lcom/android/gallery3d/filtershow/imageshow/ImageShow;->getImagePreset()Lcom/android/gallery3d/filtershow/presets/ImagePreset;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/android/gallery3d/filtershow/HistoryAdapter;->addHistoryItem(Lcom/android/gallery3d/filtershow/presets/ImagePreset;)V

    :cond_1
    invoke-virtual {p0}, Lcom/android/gallery3d/filtershow/imageshow/ImageShow;->getImagePreset()Lcom/android/gallery3d/filtershow/presets/ImagePreset;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/android/gallery3d/filtershow/presets/ImagePreset;->setEndpoint(Lcom/android/gallery3d/filtershow/imageshow/ImageShow;)V

    invoke-virtual {p0}, Lcom/android/gallery3d/filtershow/imageshow/ImageShow;->updateImage()V

    iget-object v0, p0, Lcom/android/gallery3d/filtershow/imageshow/ImageShow;->mImagePreset:Lcom/android/gallery3d/filtershow/presets/ImagePreset;

    iget-object v1, p0, Lcom/android/gallery3d/filtershow/imageshow/ImageShow;->mImageStateAdapter:Lcom/android/gallery3d/filtershow/ImageStateAdapter;

    invoke-virtual {v0, v1}, Lcom/android/gallery3d/filtershow/presets/ImagePreset;->fillImageStateAdapter(Lcom/android/gallery3d/filtershow/ImageStateAdapter;)V

    invoke-virtual {p0}, Landroid/view/View;->invalidate()V

    goto :goto_0
.end method

.method public setImageRotation(F)V
    .locals 1
    .param p1    # F

    invoke-virtual {p0}, Lcom/android/gallery3d/filtershow/imageshow/ImageShow;->getImagePreset()Lcom/android/gallery3d/filtershow/presets/ImagePreset;

    move-result-object v0

    iget-object v0, v0, Lcom/android/gallery3d/filtershow/presets/ImagePreset;->mGeoData:Lcom/android/gallery3d/filtershow/imageshow/GeometryMetadata;

    invoke-virtual {v0, p1}, Lcom/android/gallery3d/filtershow/imageshow/GeometryMetadata;->setRotation(F)V

    return-void
.end method

.method public setImageRotation(FF)V
    .locals 2
    .param p1    # F
    .param p2    # F

    invoke-virtual {p0}, Lcom/android/gallery3d/filtershow/imageshow/ImageShow;->getImageRotation()F

    move-result v0

    cmpl-float v1, p1, v0

    if-eqz v1, :cond_0

    invoke-virtual {p0}, Landroid/view/View;->invalidate()V

    :cond_0
    invoke-virtual {p0, p1}, Lcom/android/gallery3d/filtershow/imageshow/ImageShow;->setImageRotation(F)V

    invoke-virtual {p0, p2}, Lcom/android/gallery3d/filtershow/imageshow/ImageShow;->setImageRotationZoomFactor(F)V

    return-void
.end method

.method public setImageRotationZoomFactor(F)V
    .locals 1
    .param p1    # F

    invoke-virtual {p0}, Lcom/android/gallery3d/filtershow/imageshow/ImageShow;->getImagePreset()Lcom/android/gallery3d/filtershow/presets/ImagePreset;

    move-result-object v0

    iget-object v0, v0, Lcom/android/gallery3d/filtershow/presets/ImagePreset;->mGeoData:Lcom/android/gallery3d/filtershow/imageshow/GeometryMetadata;

    invoke-virtual {v0, p1}, Lcom/android/gallery3d/filtershow/imageshow/GeometryMetadata;->setScaleFactor(F)V

    return-void
.end method

.method public setPanelController(Lcom/android/gallery3d/filtershow/PanelController;)V
    .locals 0
    .param p1    # Lcom/android/gallery3d/filtershow/PanelController;

    iput-object p1, p0, Lcom/android/gallery3d/filtershow/imageshow/ImageShow;->mController:Lcom/android/gallery3d/filtershow/PanelController;

    return-void
.end method

.method public setSeekBar(Landroid/widget/SeekBar;)V
    .locals 0
    .param p1    # Landroid/widget/SeekBar;

    iput-object p1, p0, Lcom/android/gallery3d/filtershow/imageshow/ImageShow;->mSeekBar:Landroid/widget/SeekBar;

    return-void
.end method

.method public setShowControls(Z)Lcom/android/gallery3d/filtershow/imageshow/ImageShow;
    .locals 2
    .param p1    # Z

    iput-boolean p1, p0, Lcom/android/gallery3d/filtershow/imageshow/ImageShow;->mShowControls:Z

    iget-boolean v0, p0, Lcom/android/gallery3d/filtershow/imageshow/ImageShow;->mShowControls:Z

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/android/gallery3d/filtershow/imageshow/ImageShow;->mSeekBar:Landroid/widget/SeekBar;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/gallery3d/filtershow/imageshow/ImageShow;->mSeekBar:Landroid/widget/SeekBar;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/ProgressBar;->setVisibility(I)V

    :cond_0
    :goto_0
    return-object p0

    :cond_1
    iget-object v0, p0, Lcom/android/gallery3d/filtershow/imageshow/ImageShow;->mSeekBar:Landroid/widget/SeekBar;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/gallery3d/filtershow/imageshow/ImageShow;->mSeekBar:Landroid/widget/SeekBar;

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Landroid/widget/ProgressBar;->setVisibility(I)V

    goto :goto_0
.end method

.method public setupGestureDetector(Landroid/content/Context;)V
    .locals 1
    .param p1    # Landroid/content/Context;

    new-instance v0, Landroid/view/GestureDetector;

    invoke-direct {v0, p1, p0}, Landroid/view/GestureDetector;-><init>(Landroid/content/Context;Landroid/view/GestureDetector$OnGestureListener;)V

    iput-object v0, p0, Lcom/android/gallery3d/filtershow/imageshow/ImageShow;->mGestureDetector:Landroid/view/GestureDetector;

    return-void
.end method

.method public showControls()Z
    .locals 1

    iget-boolean v0, p0, Lcom/android/gallery3d/filtershow/imageshow/ImageShow;->mShowControls:Z

    return v0
.end method

.method public showHires()Z
    .locals 1

    const/4 v0, 0x1

    return v0
.end method

.method public showOriginal(Z)V
    .locals 0
    .param p1    # Z

    iput-boolean p1, p0, Lcom/android/gallery3d/filtershow/imageshow/ImageShow;->mShowOriginal:Z

    invoke-virtual {p0}, Landroid/view/View;->invalidate()V

    return-void
.end method

.method public showTitle()Z
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public showToast(Ljava/lang/String;)V
    .locals 1
    .param p1    # Ljava/lang/String;

    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, Lcom/android/gallery3d/filtershow/imageshow/ImageShow;->showToast(Ljava/lang/String;Z)V

    return-void
.end method

.method public showToast(Ljava/lang/String;Z)V
    .locals 4
    .param p1    # Ljava/lang/String;
    .param p2    # Z

    iput-object p1, p0, Lcom/android/gallery3d/filtershow/imageshow/ImageShow;->mToast:Ljava/lang/String;

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/android/gallery3d/filtershow/imageshow/ImageShow;->mShowToast:Z

    iput-boolean p2, p0, Lcom/android/gallery3d/filtershow/imageshow/ImageShow;->mImportantToast:Z

    invoke-virtual {p0}, Landroid/view/View;->invalidate()V

    iget-object v0, p0, Lcom/android/gallery3d/filtershow/imageshow/ImageShow;->mHandler:Landroid/os/Handler;

    new-instance v1, Lcom/android/gallery3d/filtershow/imageshow/ImageShow$1;

    invoke-direct {v1, p0}, Lcom/android/gallery3d/filtershow/imageshow/ImageShow$1;-><init>(Lcom/android/gallery3d/filtershow/imageshow/ImageShow;)V

    const-wide/16 v2, 0x190

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    return-void
.end method

.method public unselect()V
    .locals 0

    return-void
.end method

.method public updateFilteredImage(Landroid/graphics/Bitmap;)V
    .locals 0
    .param p1    # Landroid/graphics/Bitmap;

    iput-object p1, p0, Lcom/android/gallery3d/filtershow/imageshow/ImageShow;->mFilteredImage:Landroid/graphics/Bitmap;

    return-void
.end method

.method public updateGeometryFlags()Z
    .locals 1

    const/4 v0, 0x1

    return v0
.end method

.method public updateImage()V
    .locals 2

    invoke-virtual {p0}, Lcom/android/gallery3d/filtershow/imageshow/ImageShow;->updateGeometryFlags()Z

    move-result v1

    if-nez v1, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v1, p0, Lcom/android/gallery3d/filtershow/imageshow/ImageShow;->mImageLoader:Lcom/android/gallery3d/filtershow/cache/ImageLoader;

    invoke-virtual {v1}, Lcom/android/gallery3d/filtershow/cache/ImageLoader;->getOriginalBitmapLarge()Landroid/graphics/Bitmap;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-direct {p0, v0}, Lcom/android/gallery3d/filtershow/imageshow/ImageShow;->imageSizeChanged(Landroid/graphics/Bitmap;)V

    invoke-virtual {p0}, Landroid/view/View;->invalidate()V

    goto :goto_0
.end method

.method public updateImagePresets(Z)V
    .locals 6
    .param p1    # Z

    const/4 v5, 0x0

    const/4 v4, 0x0

    invoke-virtual {p0}, Lcom/android/gallery3d/filtershow/imageshow/ImageShow;->getImagePreset()Lcom/android/gallery3d/filtershow/presets/ImagePreset;

    move-result-object v1

    if-nez v1, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    if-eqz p1, :cond_2

    iget-object v2, p0, Lcom/android/gallery3d/filtershow/imageshow/ImageShow;->mImageLoader:Lcom/android/gallery3d/filtershow/cache/ImageLoader;

    invoke-virtual {p0}, Lcom/android/gallery3d/filtershow/imageshow/ImageShow;->getImagePreset()Lcom/android/gallery3d/filtershow/presets/ImagePreset;

    move-result-object v3

    invoke-virtual {v2, v3, p0}, Lcom/android/gallery3d/filtershow/cache/ImageLoader;->resetImageForPreset(Lcom/android/gallery3d/filtershow/presets/ImagePreset;Lcom/android/gallery3d/filtershow/imageshow/ImageShow;)V

    :cond_2
    if-nez p1, :cond_3

    iget-object v2, p0, Lcom/android/gallery3d/filtershow/imageshow/ImageShow;->mImageGeometryOnlyPreset:Lcom/android/gallery3d/filtershow/presets/ImagePreset;

    if-nez v2, :cond_5

    :cond_3
    new-instance v0, Lcom/android/gallery3d/filtershow/presets/ImagePreset;

    invoke-direct {v0, v1}, Lcom/android/gallery3d/filtershow/presets/ImagePreset;-><init>(Lcom/android/gallery3d/filtershow/presets/ImagePreset;)V

    invoke-virtual {v0, v4}, Lcom/android/gallery3d/filtershow/presets/ImagePreset;->setDoApplyFilters(Z)V

    iget-object v2, p0, Lcom/android/gallery3d/filtershow/imageshow/ImageShow;->mImageGeometryOnlyPreset:Lcom/android/gallery3d/filtershow/presets/ImagePreset;

    if-eqz v2, :cond_4

    iget-object v2, p0, Lcom/android/gallery3d/filtershow/imageshow/ImageShow;->mImageGeometryOnlyPreset:Lcom/android/gallery3d/filtershow/presets/ImagePreset;

    invoke-virtual {v0, v2}, Lcom/android/gallery3d/filtershow/presets/ImagePreset;->same(Lcom/android/gallery3d/filtershow/presets/ImagePreset;)Z

    move-result v2

    if-nez v2, :cond_5

    :cond_4
    iput-object v0, p0, Lcom/android/gallery3d/filtershow/imageshow/ImageShow;->mImageGeometryOnlyPreset:Lcom/android/gallery3d/filtershow/presets/ImagePreset;

    iput-object v5, p0, Lcom/android/gallery3d/filtershow/imageshow/ImageShow;->mGeometryOnlyImage:Landroid/graphics/Bitmap;

    :cond_5
    if-nez p1, :cond_6

    iget-object v2, p0, Lcom/android/gallery3d/filtershow/imageshow/ImageShow;->mImageFiltersOnlyPreset:Lcom/android/gallery3d/filtershow/presets/ImagePreset;

    if-nez v2, :cond_0

    :cond_6
    new-instance v0, Lcom/android/gallery3d/filtershow/presets/ImagePreset;

    invoke-direct {v0, v1}, Lcom/android/gallery3d/filtershow/presets/ImagePreset;-><init>(Lcom/android/gallery3d/filtershow/presets/ImagePreset;)V

    invoke-virtual {v0, v4}, Lcom/android/gallery3d/filtershow/presets/ImagePreset;->setDoApplyGeometry(Z)V

    iget-object v2, p0, Lcom/android/gallery3d/filtershow/imageshow/ImageShow;->mImageFiltersOnlyPreset:Lcom/android/gallery3d/filtershow/presets/ImagePreset;

    if-eqz v2, :cond_7

    iget-object v2, p0, Lcom/android/gallery3d/filtershow/imageshow/ImageShow;->mImageFiltersOnlyPreset:Lcom/android/gallery3d/filtershow/presets/ImagePreset;

    invoke-virtual {v0, v2}, Lcom/android/gallery3d/filtershow/presets/ImagePreset;->same(Lcom/android/gallery3d/filtershow/presets/ImagePreset;)Z

    move-result v2

    if-nez v2, :cond_0

    :cond_7
    iput-object v0, p0, Lcom/android/gallery3d/filtershow/imageshow/ImageShow;->mImageFiltersOnlyPreset:Lcom/android/gallery3d/filtershow/presets/ImagePreset;

    iput-object v5, p0, Lcom/android/gallery3d/filtershow/imageshow/ImageShow;->mFiltersOnlyImage:Landroid/graphics/Bitmap;

    goto :goto_0
.end method

.method public updateSeekBar(III)V
    .locals 3
    .param p1    # I
    .param p2    # I
    .param p3    # I

    iget-object v2, p0, Lcom/android/gallery3d/filtershow/imageshow/ImageShow;->mSeekBar:Landroid/widget/SeekBar;

    if-nez v2, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v2, p0, Lcom/android/gallery3d/filtershow/imageshow/ImageShow;->mSeekBar:Landroid/widget/SeekBar;

    invoke-virtual {v2}, Landroid/widget/ProgressBar;->getMax()I

    move-result v1

    invoke-direct {p0, p1, p2, p3, v1}, Lcom/android/gallery3d/filtershow/imageshow/ImageShow;->parameterToUI(IIII)I

    move-result v0

    iget-object v2, p0, Lcom/android/gallery3d/filtershow/imageshow/ImageShow;->mSeekBar:Landroid/widget/SeekBar;

    invoke-virtual {v2, v0}, Landroid/widget/ProgressBar;->setProgress(I)V

    invoke-virtual {p0}, Lcom/android/gallery3d/filtershow/imageshow/ImageShow;->getPanelController()Lcom/android/gallery3d/filtershow/PanelController;

    move-result-object v2

    if-eqz v2, :cond_0

    invoke-virtual {p0}, Lcom/android/gallery3d/filtershow/imageshow/ImageShow;->getPanelController()Lcom/android/gallery3d/filtershow/PanelController;

    move-result-object v2

    invoke-virtual {v2, p1}, Lcom/android/gallery3d/filtershow/PanelController;->onNewValue(I)V

    goto :goto_0
.end method
