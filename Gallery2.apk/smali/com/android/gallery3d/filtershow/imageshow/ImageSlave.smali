.class public Lcom/android/gallery3d/filtershow/imageshow/ImageSlave;
.super Lcom/android/gallery3d/filtershow/imageshow/ImageShow;
.source "ImageSlave.java"


# instance fields
.field private mMasterImageShow:Lcom/android/gallery3d/filtershow/imageshow/ImageShow;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1    # Landroid/content/Context;

    invoke-direct {p0, p1}, Lcom/android/gallery3d/filtershow/imageshow/ImageShow;-><init>(Landroid/content/Context;)V

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/android/gallery3d/filtershow/imageshow/ImageSlave;->mMasterImageShow:Lcom/android/gallery3d/filtershow/imageshow/ImageShow;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/util/AttributeSet;

    invoke-direct {p0, p1, p2}, Lcom/android/gallery3d/filtershow/imageshow/ImageShow;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/android/gallery3d/filtershow/imageshow/ImageSlave;->mMasterImageShow:Lcom/android/gallery3d/filtershow/imageshow/ImageShow;

    return-void
.end method


# virtual methods
.method public getCurrentFilter()Lcom/android/gallery3d/filtershow/filters/ImageFilter;
    .locals 1

    iget-object v0, p0, Lcom/android/gallery3d/filtershow/imageshow/ImageSlave;->mMasterImageShow:Lcom/android/gallery3d/filtershow/imageshow/ImageShow;

    invoke-virtual {v0}, Lcom/android/gallery3d/filtershow/imageshow/ImageShow;->getCurrentFilter()Lcom/android/gallery3d/filtershow/filters/ImageFilter;

    move-result-object v0

    return-object v0
.end method

.method public getDisplayedImageBounds()Landroid/graphics/Rect;
    .locals 1

    iget-object v0, p0, Lcom/android/gallery3d/filtershow/imageshow/ImageSlave;->mMasterImageShow:Lcom/android/gallery3d/filtershow/imageshow/ImageShow;

    invoke-virtual {v0}, Lcom/android/gallery3d/filtershow/imageshow/ImageShow;->getDisplayedImageBounds()Landroid/graphics/Rect;

    move-result-object v0

    return-object v0
.end method

.method public getFilteredImage()Landroid/graphics/Bitmap;
    .locals 1

    iget-object v0, p0, Lcom/android/gallery3d/filtershow/imageshow/ImageSlave;->mMasterImageShow:Lcom/android/gallery3d/filtershow/imageshow/ImageShow;

    invoke-virtual {v0}, Lcom/android/gallery3d/filtershow/imageshow/ImageShow;->getFilteredImage()Landroid/graphics/Bitmap;

    move-result-object v0

    return-object v0
.end method

.method public getHistory()Lcom/android/gallery3d/filtershow/HistoryAdapter;
    .locals 1

    iget-object v0, p0, Lcom/android/gallery3d/filtershow/imageshow/ImageSlave;->mMasterImageShow:Lcom/android/gallery3d/filtershow/imageshow/ImageShow;

    invoke-virtual {v0}, Lcom/android/gallery3d/filtershow/imageshow/ImageShow;->getHistory()Lcom/android/gallery3d/filtershow/HistoryAdapter;

    move-result-object v0

    return-object v0
.end method

.method public getImagePreset()Lcom/android/gallery3d/filtershow/presets/ImagePreset;
    .locals 1

    iget-object v0, p0, Lcom/android/gallery3d/filtershow/imageshow/ImageSlave;->mMasterImageShow:Lcom/android/gallery3d/filtershow/imageshow/ImageShow;

    invoke-virtual {v0}, Lcom/android/gallery3d/filtershow/imageshow/ImageShow;->getImagePreset()Lcom/android/gallery3d/filtershow/presets/ImagePreset;

    move-result-object v0

    return-object v0
.end method

.method public getImageRotation()F
    .locals 1

    iget-object v0, p0, Lcom/android/gallery3d/filtershow/imageshow/ImageSlave;->mMasterImageShow:Lcom/android/gallery3d/filtershow/imageshow/ImageShow;

    invoke-virtual {v0}, Lcom/android/gallery3d/filtershow/imageshow/ImageShow;->getImageRotation()F

    move-result v0

    return v0
.end method

.method public getImageRotationZoomFactor()F
    .locals 1

    iget-object v0, p0, Lcom/android/gallery3d/filtershow/imageshow/ImageSlave;->mMasterImageShow:Lcom/android/gallery3d/filtershow/imageshow/ImageShow;

    invoke-virtual {v0}, Lcom/android/gallery3d/filtershow/imageshow/ImageShow;->getImageRotationZoomFactor()F

    move-result v0

    return v0
.end method

.method public getMaster()Lcom/android/gallery3d/filtershow/imageshow/ImageShow;
    .locals 1

    iget-object v0, p0, Lcom/android/gallery3d/filtershow/imageshow/ImageSlave;->mMasterImageShow:Lcom/android/gallery3d/filtershow/imageshow/ImageShow;

    return-object v0
.end method

.method public getPanelController()Lcom/android/gallery3d/filtershow/PanelController;
    .locals 1

    iget-object v0, p0, Lcom/android/gallery3d/filtershow/imageshow/ImageSlave;->mMasterImageShow:Lcom/android/gallery3d/filtershow/imageshow/ImageShow;

    invoke-virtual {v0}, Lcom/android/gallery3d/filtershow/imageshow/ImageShow;->getPanelController()Lcom/android/gallery3d/filtershow/PanelController;

    move-result-object v0

    return-object v0
.end method

.method public onDraw(Landroid/graphics/Canvas;)V
    .locals 0
    .param p1    # Landroid/graphics/Canvas;

    invoke-super {p0, p1}, Lcom/android/gallery3d/filtershow/imageshow/ImageShow;->onDraw(Landroid/graphics/Canvas;)V

    return-void
.end method

.method public requestFilteredImages()V
    .locals 1

    iget-object v0, p0, Lcom/android/gallery3d/filtershow/imageshow/ImageSlave;->mMasterImageShow:Lcom/android/gallery3d/filtershow/imageshow/ImageShow;

    invoke-virtual {v0}, Lcom/android/gallery3d/filtershow/imageshow/ImageShow;->requestFilteredImages()V

    return-void
.end method

.method public resetImageCaches(Lcom/android/gallery3d/filtershow/imageshow/ImageShow;)V
    .locals 1
    .param p1    # Lcom/android/gallery3d/filtershow/imageshow/ImageShow;

    iget-object v0, p0, Lcom/android/gallery3d/filtershow/imageshow/ImageSlave;->mMasterImageShow:Lcom/android/gallery3d/filtershow/imageshow/ImageShow;

    invoke-virtual {v0, p1}, Lcom/android/gallery3d/filtershow/imageshow/ImageShow;->resetImageCaches(Lcom/android/gallery3d/filtershow/imageshow/ImageShow;)V

    return-void
.end method

.method public setCurrentFilter(Lcom/android/gallery3d/filtershow/filters/ImageFilter;)V
    .locals 1
    .param p1    # Lcom/android/gallery3d/filtershow/filters/ImageFilter;

    iget-object v0, p0, Lcom/android/gallery3d/filtershow/imageshow/ImageSlave;->mMasterImageShow:Lcom/android/gallery3d/filtershow/imageshow/ImageShow;

    invoke-virtual {v0, p1}, Lcom/android/gallery3d/filtershow/imageshow/ImageShow;->setCurrentFilter(Lcom/android/gallery3d/filtershow/filters/ImageFilter;)V

    return-void
.end method

.method public setImagePreset(Lcom/android/gallery3d/filtershow/presets/ImagePreset;Z)V
    .locals 1
    .param p1    # Lcom/android/gallery3d/filtershow/presets/ImagePreset;
    .param p2    # Z

    iget-object v0, p0, Lcom/android/gallery3d/filtershow/imageshow/ImageSlave;->mMasterImageShow:Lcom/android/gallery3d/filtershow/imageshow/ImageShow;

    invoke-virtual {v0, p1, p2}, Lcom/android/gallery3d/filtershow/imageshow/ImageShow;->setImagePreset(Lcom/android/gallery3d/filtershow/presets/ImagePreset;Z)V

    return-void
.end method

.method public setMaster(Lcom/android/gallery3d/filtershow/imageshow/ImageShow;)V
    .locals 0
    .param p1    # Lcom/android/gallery3d/filtershow/imageshow/ImageShow;

    iput-object p1, p0, Lcom/android/gallery3d/filtershow/imageshow/ImageSlave;->mMasterImageShow:Lcom/android/gallery3d/filtershow/imageshow/ImageShow;

    return-void
.end method

.method public setPanelController(Lcom/android/gallery3d/filtershow/PanelController;)V
    .locals 1
    .param p1    # Lcom/android/gallery3d/filtershow/PanelController;

    iget-object v0, p0, Lcom/android/gallery3d/filtershow/imageshow/ImageSlave;->mMasterImageShow:Lcom/android/gallery3d/filtershow/imageshow/ImageShow;

    invoke-virtual {v0, p1}, Lcom/android/gallery3d/filtershow/imageshow/ImageShow;->setPanelController(Lcom/android/gallery3d/filtershow/PanelController;)V

    return-void
.end method

.method public showTitle()Z
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public updateImage()V
    .locals 1

    iget-object v0, p0, Lcom/android/gallery3d/filtershow/imageshow/ImageSlave;->mMasterImageShow:Lcom/android/gallery3d/filtershow/imageshow/ImageShow;

    invoke-virtual {v0}, Lcom/android/gallery3d/filtershow/imageshow/ImageShow;->updateImage()V

    return-void
.end method

.method public updateImagePresets(Z)V
    .locals 1
    .param p1    # Z

    iget-object v0, p0, Lcom/android/gallery3d/filtershow/imageshow/ImageSlave;->mMasterImageShow:Lcom/android/gallery3d/filtershow/imageshow/ImageShow;

    invoke-virtual {v0, p1}, Lcom/android/gallery3d/filtershow/imageshow/ImageShow;->updateImagePresets(Z)V

    return-void
.end method
