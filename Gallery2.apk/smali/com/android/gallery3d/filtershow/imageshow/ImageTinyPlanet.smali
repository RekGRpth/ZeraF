.class public Lcom/android/gallery3d/filtershow/imageshow/ImageTinyPlanet;
.super Lcom/android/gallery3d/filtershow/imageshow/ImageSlave;
.source "ImageTinyPlanet.java"


# instance fields
.field private mCenterX:F

.field private mCenterY:F

.field private mCurrentX:F

.field private mCurrentY:F

.field private mStartAngle:F

.field private mTouchCenterX:F

.field private mTouchCenterY:F


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1    # Landroid/content/Context;

    const/4 v0, 0x0

    invoke-direct {p0, p1}, Lcom/android/gallery3d/filtershow/imageshow/ImageSlave;-><init>(Landroid/content/Context;)V

    iput v0, p0, Lcom/android/gallery3d/filtershow/imageshow/ImageTinyPlanet;->mTouchCenterX:F

    iput v0, p0, Lcom/android/gallery3d/filtershow/imageshow/ImageTinyPlanet;->mTouchCenterY:F

    iput v0, p0, Lcom/android/gallery3d/filtershow/imageshow/ImageTinyPlanet;->mCurrentX:F

    iput v0, p0, Lcom/android/gallery3d/filtershow/imageshow/ImageTinyPlanet;->mCurrentY:F

    iput v0, p0, Lcom/android/gallery3d/filtershow/imageshow/ImageTinyPlanet;->mCenterX:F

    iput v0, p0, Lcom/android/gallery3d/filtershow/imageshow/ImageTinyPlanet;->mCenterY:F

    iput v0, p0, Lcom/android/gallery3d/filtershow/imageshow/ImageTinyPlanet;->mStartAngle:F

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/util/AttributeSet;

    const/4 v0, 0x0

    invoke-direct {p0, p1, p2}, Lcom/android/gallery3d/filtershow/imageshow/ImageSlave;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    iput v0, p0, Lcom/android/gallery3d/filtershow/imageshow/ImageTinyPlanet;->mTouchCenterX:F

    iput v0, p0, Lcom/android/gallery3d/filtershow/imageshow/ImageTinyPlanet;->mTouchCenterY:F

    iput v0, p0, Lcom/android/gallery3d/filtershow/imageshow/ImageTinyPlanet;->mCurrentX:F

    iput v0, p0, Lcom/android/gallery3d/filtershow/imageshow/ImageTinyPlanet;->mCurrentY:F

    iput v0, p0, Lcom/android/gallery3d/filtershow/imageshow/ImageTinyPlanet;->mCenterX:F

    iput v0, p0, Lcom/android/gallery3d/filtershow/imageshow/ImageTinyPlanet;->mCenterY:F

    iput v0, p0, Lcom/android/gallery3d/filtershow/imageshow/ImageTinyPlanet;->mStartAngle:F

    return-void
.end method

.method protected static angleFor(FF)F
    .locals 4
    .param p0    # F
    .param p1    # F

    float-to-double v0, p0

    float-to-double v2, p1

    invoke-static {v0, v1, v2, v3}, Ljava/lang/Math;->atan2(DD)D

    move-result-wide v0

    const-wide v2, 0x4066800000000000L

    mul-double/2addr v0, v2

    const-wide v2, 0x400921fb54442d18L

    div-double/2addr v0, v2

    double-to-float v0, v0

    return v0
.end method


# virtual methods
.method protected getCurrentTouchAngle()F
    .locals 10

    iget v6, p0, Lcom/android/gallery3d/filtershow/imageshow/ImageTinyPlanet;->mCurrentX:F

    iget v7, p0, Lcom/android/gallery3d/filtershow/imageshow/ImageTinyPlanet;->mTouchCenterX:F

    cmpl-float v6, v6, v7

    if-nez v6, :cond_0

    iget v6, p0, Lcom/android/gallery3d/filtershow/imageshow/ImageTinyPlanet;->mCurrentY:F

    iget v7, p0, Lcom/android/gallery3d/filtershow/imageshow/ImageTinyPlanet;->mTouchCenterY:F

    cmpl-float v6, v6, v7

    if-nez v6, :cond_0

    const/4 v6, 0x0

    :goto_0
    return v6

    :cond_0
    iget v6, p0, Lcom/android/gallery3d/filtershow/imageshow/ImageTinyPlanet;->mTouchCenterX:F

    iget v7, p0, Lcom/android/gallery3d/filtershow/imageshow/ImageTinyPlanet;->mCenterX:F

    sub-float v2, v6, v7

    iget v6, p0, Lcom/android/gallery3d/filtershow/imageshow/ImageTinyPlanet;->mTouchCenterY:F

    iget v7, p0, Lcom/android/gallery3d/filtershow/imageshow/ImageTinyPlanet;->mCenterY:F

    sub-float v4, v6, v7

    iget v6, p0, Lcom/android/gallery3d/filtershow/imageshow/ImageTinyPlanet;->mCurrentX:F

    iget v7, p0, Lcom/android/gallery3d/filtershow/imageshow/ImageTinyPlanet;->mCenterX:F

    sub-float v3, v6, v7

    iget v6, p0, Lcom/android/gallery3d/filtershow/imageshow/ImageTinyPlanet;->mCurrentY:F

    iget v7, p0, Lcom/android/gallery3d/filtershow/imageshow/ImageTinyPlanet;->mCenterY:F

    sub-float v5, v6, v7

    invoke-static {v2, v4}, Lcom/android/gallery3d/filtershow/imageshow/ImageTinyPlanet;->angleFor(FF)F

    move-result v0

    invoke-static {v3, v5}, Lcom/android/gallery3d/filtershow/imageshow/ImageTinyPlanet;->angleFor(FF)F

    move-result v1

    sub-float v6, v1, v0

    const/high16 v7, 0x43b40000

    rem-float/2addr v6, v7

    float-to-double v6, v6

    const-wide v8, 0x400921fb54442d18L

    mul-double/2addr v6, v8

    const-wide v8, 0x4066800000000000L

    div-double/2addr v6, v8

    double-to-float v6, v6

    goto :goto_0
.end method

.method public onTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 5
    .param p1    # Landroid/view/MotionEvent;

    invoke-virtual {p0}, Lcom/android/gallery3d/filtershow/imageshow/ImageSlave;->getCurrentFilter()Lcom/android/gallery3d/filtershow/filters/ImageFilter;

    move-result-object v0

    check-cast v0, Lcom/android/gallery3d/filtershow/filters/ImageFilterTinyPlanet;

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v1

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v2

    iput v1, p0, Lcom/android/gallery3d/filtershow/imageshow/ImageTinyPlanet;->mCurrentX:F

    iput v2, p0, Lcom/android/gallery3d/filtershow/imageshow/ImageTinyPlanet;->mCurrentY:F

    invoke-virtual {p0}, Landroid/view/View;->getWidth()I

    move-result v3

    div-int/lit8 v3, v3, 0x2

    int-to-float v3, v3

    iput v3, p0, Lcom/android/gallery3d/filtershow/imageshow/ImageTinyPlanet;->mCenterX:F

    invoke-virtual {p0}, Landroid/view/View;->getHeight()I

    move-result v3

    div-int/lit8 v3, v3, 0x2

    int-to-float v3, v3

    iput v3, p0, Lcom/android/gallery3d/filtershow/imageshow/ImageTinyPlanet;->mCenterY:F

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getActionMasked()I

    move-result v3

    packed-switch v3, :pswitch_data_0

    :goto_0
    invoke-virtual {p0, p0}, Lcom/android/gallery3d/filtershow/imageshow/ImageSlave;->resetImageCaches(Lcom/android/gallery3d/filtershow/imageshow/ImageShow;)V

    invoke-virtual {p0}, Landroid/view/View;->invalidate()V

    const/4 v3, 0x1

    return v3

    :pswitch_0
    iput v1, p0, Lcom/android/gallery3d/filtershow/imageshow/ImageTinyPlanet;->mTouchCenterX:F

    iput v2, p0, Lcom/android/gallery3d/filtershow/imageshow/ImageTinyPlanet;->mTouchCenterY:F

    invoke-virtual {v0}, Lcom/android/gallery3d/filtershow/filters/ImageFilterTinyPlanet;->getAngle()F

    move-result v3

    iput v3, p0, Lcom/android/gallery3d/filtershow/imageshow/ImageTinyPlanet;->mStartAngle:F

    goto :goto_0

    :pswitch_1
    iget v3, p0, Lcom/android/gallery3d/filtershow/imageshow/ImageTinyPlanet;->mStartAngle:F

    invoke-virtual {p0}, Lcom/android/gallery3d/filtershow/imageshow/ImageTinyPlanet;->getCurrentTouchAngle()F

    move-result v4

    add-float/2addr v3, v4

    invoke-virtual {v0, v3}, Lcom/android/gallery3d/filtershow/filters/ImageFilterTinyPlanet;->setAngle(F)V

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_1
    .end packed-switch
.end method
