.class Lcom/android/gallery3d/filtershow/PanelController$UtilityPanel;
.super Ljava/lang/Object;
.source "PanelController.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/gallery3d/filtershow/PanelController;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = "UtilityPanel"
.end annotation


# instance fields
.field firstTimeCropDisplayed:Z

.field private mAspectButton:Landroid/view/View;

.field private final mContext:Landroid/content/Context;

.field private mCurvesButton:Landroid/view/View;

.field private mEffectName:Ljava/lang/String;

.field private mParameterValue:I

.field private mSelected:Z

.field private mShowParameterValue:Z

.field private final mTextView:Landroid/widget/TextView;

.field private final mView:Landroid/view/View;

.field final synthetic this$0:Lcom/android/gallery3d/filtershow/PanelController;


# direct methods
.method public constructor <init>(Lcom/android/gallery3d/filtershow/PanelController;Landroid/content/Context;Landroid/view/View;Landroid/view/View;Landroid/view/View;Landroid/view/View;)V
    .locals 2
    .param p2    # Landroid/content/Context;
    .param p3    # Landroid/view/View;
    .param p4    # Landroid/view/View;
    .param p5    # Landroid/view/View;
    .param p6    # Landroid/view/View;

    const/4 v1, 0x0

    const/4 v0, 0x0

    iput-object p1, p0, Lcom/android/gallery3d/filtershow/PanelController$UtilityPanel;->this$0:Lcom/android/gallery3d/filtershow/PanelController;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-boolean v0, p0, Lcom/android/gallery3d/filtershow/PanelController$UtilityPanel;->mSelected:Z

    iput-object v1, p0, Lcom/android/gallery3d/filtershow/PanelController$UtilityPanel;->mEffectName:Ljava/lang/String;

    iput v0, p0, Lcom/android/gallery3d/filtershow/PanelController$UtilityPanel;->mParameterValue:I

    iput-boolean v0, p0, Lcom/android/gallery3d/filtershow/PanelController$UtilityPanel;->mShowParameterValue:Z

    iput-object v1, p0, Lcom/android/gallery3d/filtershow/PanelController$UtilityPanel;->mAspectButton:Landroid/view/View;

    iput-object v1, p0, Lcom/android/gallery3d/filtershow/PanelController$UtilityPanel;->mCurvesButton:Landroid/view/View;

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/android/gallery3d/filtershow/PanelController$UtilityPanel;->firstTimeCropDisplayed:Z

    iput-object p2, p0, Lcom/android/gallery3d/filtershow/PanelController$UtilityPanel;->mContext:Landroid/content/Context;

    iput-object p3, p0, Lcom/android/gallery3d/filtershow/PanelController$UtilityPanel;->mView:Landroid/view/View;

    check-cast p4, Landroid/widget/TextView;

    iput-object p4, p0, Lcom/android/gallery3d/filtershow/PanelController$UtilityPanel;->mTextView:Landroid/widget/TextView;

    iput-object p5, p0, Lcom/android/gallery3d/filtershow/PanelController$UtilityPanel;->mAspectButton:Landroid/view/View;

    iput-object p6, p0, Lcom/android/gallery3d/filtershow/PanelController$UtilityPanel;->mCurvesButton:Landroid/view/View;

    return-void
.end method

.method static synthetic access$500(Lcom/android/gallery3d/filtershow/PanelController$UtilityPanel;)Landroid/view/View;
    .locals 1
    .param p0    # Lcom/android/gallery3d/filtershow/PanelController$UtilityPanel;

    iget-object v0, p0, Lcom/android/gallery3d/filtershow/PanelController$UtilityPanel;->mView:Landroid/view/View;

    return-object v0
.end method


# virtual methods
.method public hideAspectButtons()V
    .locals 2

    iget-object v0, p0, Lcom/android/gallery3d/filtershow/PanelController$UtilityPanel;->mAspectButton:Landroid/view/View;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/gallery3d/filtershow/PanelController$UtilityPanel;->mAspectButton:Landroid/view/View;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    :cond_0
    return-void
.end method

.method public hideCurvesButtons()V
    .locals 2

    iget-object v0, p0, Lcom/android/gallery3d/filtershow/PanelController$UtilityPanel;->mCurvesButton:Landroid/view/View;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/gallery3d/filtershow/PanelController$UtilityPanel;->mCurvesButton:Landroid/view/View;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    :cond_0
    return-void
.end method

.method public onNewValue(I)V
    .locals 0
    .param p1    # I

    iput p1, p0, Lcom/android/gallery3d/filtershow/PanelController$UtilityPanel;->mParameterValue:I

    invoke-virtual {p0}, Lcom/android/gallery3d/filtershow/PanelController$UtilityPanel;->updateText()V

    return-void
.end method

.method public select()Landroid/view/ViewPropertyAnimator;
    .locals 5

    const/4 v4, 0x0

    iget-object v2, p0, Lcom/android/gallery3d/filtershow/PanelController$UtilityPanel;->mView:Landroid/view/View;

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Landroid/view/View;->setVisibility(I)V

    iget-object v2, p0, Lcom/android/gallery3d/filtershow/PanelController$UtilityPanel;->this$0:Lcom/android/gallery3d/filtershow/PanelController;

    invoke-static {v2}, Lcom/android/gallery3d/filtershow/PanelController;->access$000(Lcom/android/gallery3d/filtershow/PanelController;)Landroid/view/View;

    move-result-object v2

    invoke-virtual {v2}, Landroid/view/View;->getHeight()I

    move-result v1

    iget-object v2, p0, Lcom/android/gallery3d/filtershow/PanelController$UtilityPanel;->mView:Landroid/view/View;

    invoke-virtual {v2, v4}, Landroid/view/View;->setX(F)V

    iget-object v2, p0, Lcom/android/gallery3d/filtershow/PanelController$UtilityPanel;->mView:Landroid/view/View;

    neg-int v3, v1

    int-to-float v3, v3

    invoke-virtual {v2, v3}, Landroid/view/View;->setY(F)V

    invoke-virtual {p0}, Lcom/android/gallery3d/filtershow/PanelController$UtilityPanel;->updateText()V

    iget-object v2, p0, Lcom/android/gallery3d/filtershow/PanelController$UtilityPanel;->mView:Landroid/view/View;

    invoke-virtual {v2}, Landroid/view/View;->animate()Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    invoke-virtual {v0, v4}, Landroid/view/ViewPropertyAnimator;->y(F)Landroid/view/ViewPropertyAnimator;

    const-wide/16 v2, 0xc8

    invoke-virtual {v0, v2, v3}, Landroid/view/ViewPropertyAnimator;->setDuration(J)Landroid/view/ViewPropertyAnimator;

    move-result-object v2

    invoke-virtual {v2}, Landroid/view/ViewPropertyAnimator;->withLayer()Landroid/view/ViewPropertyAnimator;

    const/4 v2, 0x1

    iput-boolean v2, p0, Lcom/android/gallery3d/filtershow/PanelController$UtilityPanel;->mSelected:Z

    return-object v0
.end method

.method public selected()Z
    .locals 1

    iget-boolean v0, p0, Lcom/android/gallery3d/filtershow/PanelController$UtilityPanel;->mSelected:Z

    return v0
.end method

.method public setAspectButton(Lcom/android/gallery3d/filtershow/ui/FramedTextButton;I)V
    .locals 9
    .param p1    # Lcom/android/gallery3d/filtershow/ui/FramedTextButton;
    .param p2    # I

    const/high16 v8, 0x40e00000

    const/high16 v7, 0x40a00000

    const/high16 v6, 0x40800000

    const/high16 v5, 0x40400000

    const/high16 v4, 0x3f800000

    iget-object v2, p0, Lcom/android/gallery3d/filtershow/PanelController$UtilityPanel;->this$0:Lcom/android/gallery3d/filtershow/PanelController;

    invoke-static {v2}, Lcom/android/gallery3d/filtershow/PanelController;->access$400(Lcom/android/gallery3d/filtershow/PanelController;)Lcom/android/gallery3d/filtershow/imageshow/ImageShow;

    move-result-object v0

    check-cast v0, Lcom/android/gallery3d/filtershow/imageshow/ImageCrop;

    packed-switch p2, :pswitch_data_0

    :goto_0
    :pswitch_0
    invoke-virtual {v0}, Landroid/view/View;->invalidate()V

    return-void

    :pswitch_1
    iget-object v2, p0, Lcom/android/gallery3d/filtershow/PanelController$UtilityPanel;->mContext:Landroid/content/Context;

    const v3, 0x7f0c0182

    invoke-virtual {v2, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v1}, Lcom/android/gallery3d/filtershow/ui/FramedTextButton;->setText(Ljava/lang/String;)V

    invoke-virtual {v0, v4, v4}, Lcom/android/gallery3d/filtershow/imageshow/ImageCrop;->apply(FF)V

    invoke-virtual {v0, v1}, Lcom/android/gallery3d/filtershow/imageshow/ImageCrop;->setAspectString(Ljava/lang/String;)V

    goto :goto_0

    :pswitch_2
    iget-object v2, p0, Lcom/android/gallery3d/filtershow/PanelController$UtilityPanel;->mContext:Landroid/content/Context;

    const v3, 0x7f0c0183

    invoke-virtual {v2, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v1}, Lcom/android/gallery3d/filtershow/ui/FramedTextButton;->setText(Ljava/lang/String;)V

    invoke-virtual {v0, v6, v5}, Lcom/android/gallery3d/filtershow/imageshow/ImageCrop;->apply(FF)V

    invoke-virtual {v0, v1}, Lcom/android/gallery3d/filtershow/imageshow/ImageCrop;->setAspectString(Ljava/lang/String;)V

    goto :goto_0

    :pswitch_3
    iget-object v2, p0, Lcom/android/gallery3d/filtershow/PanelController$UtilityPanel;->mContext:Landroid/content/Context;

    const v3, 0x7f0c0184

    invoke-virtual {v2, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v1}, Lcom/android/gallery3d/filtershow/ui/FramedTextButton;->setText(Ljava/lang/String;)V

    invoke-virtual {v0, v5, v6}, Lcom/android/gallery3d/filtershow/imageshow/ImageCrop;->apply(FF)V

    invoke-virtual {v0, v1}, Lcom/android/gallery3d/filtershow/imageshow/ImageCrop;->setAspectString(Ljava/lang/String;)V

    goto :goto_0

    :pswitch_4
    iget-object v2, p0, Lcom/android/gallery3d/filtershow/PanelController$UtilityPanel;->mContext:Landroid/content/Context;

    const v3, 0x7f0c0186

    invoke-virtual {v2, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v1}, Lcom/android/gallery3d/filtershow/ui/FramedTextButton;->setText(Ljava/lang/String;)V

    invoke-virtual {v0, v7, v8}, Lcom/android/gallery3d/filtershow/imageshow/ImageCrop;->apply(FF)V

    invoke-virtual {v0, v1}, Lcom/android/gallery3d/filtershow/imageshow/ImageCrop;->setAspectString(Ljava/lang/String;)V

    goto :goto_0

    :pswitch_5
    iget-object v2, p0, Lcom/android/gallery3d/filtershow/PanelController$UtilityPanel;->mContext:Landroid/content/Context;

    const v3, 0x7f0c0187

    invoke-virtual {v2, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v1}, Lcom/android/gallery3d/filtershow/ui/FramedTextButton;->setText(Ljava/lang/String;)V

    invoke-virtual {v0, v8, v7}, Lcom/android/gallery3d/filtershow/imageshow/ImageCrop;->apply(FF)V

    invoke-virtual {v0, v1}, Lcom/android/gallery3d/filtershow/imageshow/ImageCrop;->setAspectString(Ljava/lang/String;)V

    goto :goto_0

    :pswitch_6
    iget-object v2, p0, Lcom/android/gallery3d/filtershow/PanelController$UtilityPanel;->mContext:Landroid/content/Context;

    const v3, 0x7f0c0189

    invoke-virtual {v2, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v1}, Lcom/android/gallery3d/filtershow/ui/FramedTextButton;->setText(Ljava/lang/String;)V

    invoke-virtual {v0}, Lcom/android/gallery3d/filtershow/imageshow/ImageCrop;->applyClear()V

    invoke-virtual {v0, v1}, Lcom/android/gallery3d/filtershow/imageshow/ImageCrop;->setAspectString(Ljava/lang/String;)V

    goto :goto_0

    :pswitch_7
    iget-object v2, p0, Lcom/android/gallery3d/filtershow/PanelController$UtilityPanel;->mContext:Landroid/content/Context;

    const v3, 0x7f0c018a

    invoke-virtual {v2, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v1}, Lcom/android/gallery3d/filtershow/ui/FramedTextButton;->setText(Ljava/lang/String;)V

    invoke-virtual {v0}, Lcom/android/gallery3d/filtershow/imageshow/ImageCrop;->applyOriginal()V

    invoke-virtual {v0, v1}, Lcom/android/gallery3d/filtershow/imageshow/ImageCrop;->setAspectString(Ljava/lang/String;)V

    goto/16 :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x7f0b0158
        :pswitch_1
        :pswitch_0
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_0
        :pswitch_6
        :pswitch_7
    .end packed-switch
.end method

.method public setEffectName(Ljava/lang/String;)V
    .locals 1
    .param p1    # Ljava/lang/String;

    iput-object p1, p0, Lcom/android/gallery3d/filtershow/PanelController$UtilityPanel;->mEffectName:Ljava/lang/String;

    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/android/gallery3d/filtershow/PanelController$UtilityPanel;->setShowParameter(Z)V

    return-void
.end method

.method public setShowParameter(Z)V
    .locals 0
    .param p1    # Z

    iput-boolean p1, p0, Lcom/android/gallery3d/filtershow/PanelController$UtilityPanel;->mShowParameterValue:Z

    invoke-virtual {p0}, Lcom/android/gallery3d/filtershow/PanelController$UtilityPanel;->updateText()V

    return-void
.end method

.method public showAspectButtons()V
    .locals 2

    iget-object v0, p0, Lcom/android/gallery3d/filtershow/PanelController$UtilityPanel;->mAspectButton:Landroid/view/View;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/gallery3d/filtershow/PanelController$UtilityPanel;->mAspectButton:Landroid/view/View;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    :cond_0
    return-void
.end method

.method public showCurvesButtons()V
    .locals 2

    iget-object v0, p0, Lcom/android/gallery3d/filtershow/PanelController$UtilityPanel;->mCurvesButton:Landroid/view/View;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/gallery3d/filtershow/PanelController$UtilityPanel;->mCurvesButton:Landroid/view/View;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    :cond_0
    return-void
.end method

.method public unselect()Landroid/view/ViewPropertyAnimator;
    .locals 4

    const/4 v3, 0x0

    iget-object v2, p0, Lcom/android/gallery3d/filtershow/PanelController$UtilityPanel;->mView:Landroid/view/View;

    invoke-virtual {v2}, Landroid/view/View;->animate()Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    iget-object v2, p0, Lcom/android/gallery3d/filtershow/PanelController$UtilityPanel;->mView:Landroid/view/View;

    invoke-virtual {v2, v3}, Landroid/view/View;->setX(F)V

    iget-object v2, p0, Lcom/android/gallery3d/filtershow/PanelController$UtilityPanel;->mView:Landroid/view/View;

    invoke-virtual {v2, v3}, Landroid/view/View;->setY(F)V

    iget-object v2, p0, Lcom/android/gallery3d/filtershow/PanelController$UtilityPanel;->this$0:Lcom/android/gallery3d/filtershow/PanelController;

    invoke-static {v2}, Lcom/android/gallery3d/filtershow/PanelController;->access$000(Lcom/android/gallery3d/filtershow/PanelController;)Landroid/view/View;

    move-result-object v2

    invoke-virtual {v2}, Landroid/view/View;->getHeight()I

    move-result v1

    neg-int v2, v1

    int-to-float v2, v2

    invoke-virtual {v0, v2}, Landroid/view/ViewPropertyAnimator;->y(F)Landroid/view/ViewPropertyAnimator;

    const-wide/16 v2, 0xc8

    invoke-virtual {v0, v2, v3}, Landroid/view/ViewPropertyAnimator;->setDuration(J)Landroid/view/ViewPropertyAnimator;

    move-result-object v2

    invoke-virtual {v2}, Landroid/view/ViewPropertyAnimator;->withLayer()Landroid/view/ViewPropertyAnimator;

    move-result-object v2

    new-instance v3, Lcom/android/gallery3d/filtershow/PanelController$UtilityPanel$1;

    invoke-direct {v3, p0}, Lcom/android/gallery3d/filtershow/PanelController$UtilityPanel$1;-><init>(Lcom/android/gallery3d/filtershow/PanelController$UtilityPanel;)V

    invoke-virtual {v2, v3}, Landroid/view/ViewPropertyAnimator;->withEndAction(Ljava/lang/Runnable;)Landroid/view/ViewPropertyAnimator;

    const/4 v2, 0x0

    iput-boolean v2, p0, Lcom/android/gallery3d/filtershow/PanelController$UtilityPanel;->mSelected:Z

    return-object v0
.end method

.method public updateText()V
    .locals 4

    iget-object v1, p0, Lcom/android/gallery3d/filtershow/PanelController$UtilityPanel;->mContext:Landroid/content/Context;

    const v2, 0x7f0c017f

    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    iget-boolean v1, p0, Lcom/android/gallery3d/filtershow/PanelController$UtilityPanel;->mShowParameterValue:Z

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/android/gallery3d/filtershow/PanelController$UtilityPanel;->mTextView:Landroid/widget/TextView;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/android/gallery3d/filtershow/PanelController$UtilityPanel;->mEffectName:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget v3, p0, Lcom/android/gallery3d/filtershow/PanelController$UtilityPanel;->mParameterValue:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Landroid/text/Html;->fromHtml(Ljava/lang/String;)Landroid/text/Spanned;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    :goto_0
    return-void

    :cond_0
    iget-object v1, p0, Lcom/android/gallery3d/filtershow/PanelController$UtilityPanel;->mTextView:Landroid/widget/TextView;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/android/gallery3d/filtershow/PanelController$UtilityPanel;->mEffectName:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Landroid/text/Html;->fromHtml(Ljava/lang/String;)Landroid/text/Spanned;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0
.end method
