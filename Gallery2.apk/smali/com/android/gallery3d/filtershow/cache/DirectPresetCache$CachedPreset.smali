.class public Lcom/android/gallery3d/filtershow/cache/DirectPresetCache$CachedPreset;
.super Ljava/lang/Object;
.source "DirectPresetCache.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/gallery3d/filtershow/cache/DirectPresetCache;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4
    name = "CachedPreset"
.end annotation


# instance fields
.field private mAge:J

.field private mBitmap:Landroid/graphics/Bitmap;

.field private mBusy:Z

.field private mPreset:Lcom/android/gallery3d/filtershow/presets/ImagePreset;

.field final synthetic this$0:Lcom/android/gallery3d/filtershow/cache/DirectPresetCache;


# direct methods
.method protected constructor <init>(Lcom/android/gallery3d/filtershow/cache/DirectPresetCache;)V
    .locals 2

    const/4 v0, 0x0

    iput-object p1, p0, Lcom/android/gallery3d/filtershow/cache/DirectPresetCache$CachedPreset;->this$0:Lcom/android/gallery3d/filtershow/cache/DirectPresetCache;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lcom/android/gallery3d/filtershow/cache/DirectPresetCache$CachedPreset;->mBitmap:Landroid/graphics/Bitmap;

    iput-object v0, p0, Lcom/android/gallery3d/filtershow/cache/DirectPresetCache$CachedPreset;->mPreset:Lcom/android/gallery3d/filtershow/presets/ImagePreset;

    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/android/gallery3d/filtershow/cache/DirectPresetCache$CachedPreset;->mAge:J

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/android/gallery3d/filtershow/cache/DirectPresetCache$CachedPreset;->mBusy:Z

    return-void
.end method

.method static synthetic access$100(Lcom/android/gallery3d/filtershow/cache/DirectPresetCache$CachedPreset;)Lcom/android/gallery3d/filtershow/presets/ImagePreset;
    .locals 1
    .param p0    # Lcom/android/gallery3d/filtershow/cache/DirectPresetCache$CachedPreset;

    iget-object v0, p0, Lcom/android/gallery3d/filtershow/cache/DirectPresetCache$CachedPreset;->mPreset:Lcom/android/gallery3d/filtershow/presets/ImagePreset;

    return-object v0
.end method

.method static synthetic access$102(Lcom/android/gallery3d/filtershow/cache/DirectPresetCache$CachedPreset;Lcom/android/gallery3d/filtershow/presets/ImagePreset;)Lcom/android/gallery3d/filtershow/presets/ImagePreset;
    .locals 0
    .param p0    # Lcom/android/gallery3d/filtershow/cache/DirectPresetCache$CachedPreset;
    .param p1    # Lcom/android/gallery3d/filtershow/presets/ImagePreset;

    iput-object p1, p0, Lcom/android/gallery3d/filtershow/cache/DirectPresetCache$CachedPreset;->mPreset:Lcom/android/gallery3d/filtershow/presets/ImagePreset;

    return-object p1
.end method

.method static synthetic access$200(Lcom/android/gallery3d/filtershow/cache/DirectPresetCache$CachedPreset;)Z
    .locals 1
    .param p0    # Lcom/android/gallery3d/filtershow/cache/DirectPresetCache$CachedPreset;

    iget-boolean v0, p0, Lcom/android/gallery3d/filtershow/cache/DirectPresetCache$CachedPreset;->mBusy:Z

    return v0
.end method

.method static synthetic access$202(Lcom/android/gallery3d/filtershow/cache/DirectPresetCache$CachedPreset;Z)Z
    .locals 0
    .param p0    # Lcom/android/gallery3d/filtershow/cache/DirectPresetCache$CachedPreset;
    .param p1    # Z

    iput-boolean p1, p0, Lcom/android/gallery3d/filtershow/cache/DirectPresetCache$CachedPreset;->mBusy:Z

    return p1
.end method

.method static synthetic access$300(Lcom/android/gallery3d/filtershow/cache/DirectPresetCache$CachedPreset;)Landroid/graphics/Bitmap;
    .locals 1
    .param p0    # Lcom/android/gallery3d/filtershow/cache/DirectPresetCache$CachedPreset;

    iget-object v0, p0, Lcom/android/gallery3d/filtershow/cache/DirectPresetCache$CachedPreset;->mBitmap:Landroid/graphics/Bitmap;

    return-object v0
.end method

.method static synthetic access$302(Lcom/android/gallery3d/filtershow/cache/DirectPresetCache$CachedPreset;Landroid/graphics/Bitmap;)Landroid/graphics/Bitmap;
    .locals 0
    .param p0    # Lcom/android/gallery3d/filtershow/cache/DirectPresetCache$CachedPreset;
    .param p1    # Landroid/graphics/Bitmap;

    iput-object p1, p0, Lcom/android/gallery3d/filtershow/cache/DirectPresetCache$CachedPreset;->mBitmap:Landroid/graphics/Bitmap;

    return-object p1
.end method

.method static synthetic access$400(Lcom/android/gallery3d/filtershow/cache/DirectPresetCache$CachedPreset;)J
    .locals 2
    .param p0    # Lcom/android/gallery3d/filtershow/cache/DirectPresetCache$CachedPreset;

    iget-wide v0, p0, Lcom/android/gallery3d/filtershow/cache/DirectPresetCache$CachedPreset;->mAge:J

    return-wide v0
.end method

.method static synthetic access$402(Lcom/android/gallery3d/filtershow/cache/DirectPresetCache$CachedPreset;J)J
    .locals 0
    .param p0    # Lcom/android/gallery3d/filtershow/cache/DirectPresetCache$CachedPreset;
    .param p1    # J

    iput-wide p1, p0, Lcom/android/gallery3d/filtershow/cache/DirectPresetCache$CachedPreset;->mAge:J

    return-wide p1
.end method


# virtual methods
.method public busy()Z
    .locals 1

    iget-boolean v0, p0, Lcom/android/gallery3d/filtershow/cache/DirectPresetCache$CachedPreset;->mBusy:Z

    return v0
.end method

.method public setBusy(Z)V
    .locals 0
    .param p1    # Z

    iput-boolean p1, p0, Lcom/android/gallery3d/filtershow/cache/DirectPresetCache$CachedPreset;->mBusy:Z

    return-void
.end method
