.class Lcom/android/gallery3d/filtershow/PanelController$Panel;
.super Ljava/lang/Object;
.source "PanelController.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/gallery3d/filtershow/PanelController;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = "Panel"
.end annotation


# instance fields
.field private final mContainer:Landroid/view/View;

.field private mPosition:I

.field private final mSubviews:Ljava/util/Vector;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Vector",
            "<",
            "Landroid/view/View;",
            ">;"
        }
    .end annotation
.end field

.field private final mView:Landroid/view/View;

.field final synthetic this$0:Lcom/android/gallery3d/filtershow/PanelController;


# direct methods
.method public constructor <init>(Lcom/android/gallery3d/filtershow/PanelController;Landroid/view/View;Landroid/view/View;I)V
    .locals 1
    .param p2    # Landroid/view/View;
    .param p3    # Landroid/view/View;
    .param p4    # I

    iput-object p1, p0, Lcom/android/gallery3d/filtershow/PanelController$Panel;->this$0:Lcom/android/gallery3d/filtershow/PanelController;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x0

    iput v0, p0, Lcom/android/gallery3d/filtershow/PanelController$Panel;->mPosition:I

    new-instance v0, Ljava/util/Vector;

    invoke-direct {v0}, Ljava/util/Vector;-><init>()V

    iput-object v0, p0, Lcom/android/gallery3d/filtershow/PanelController$Panel;->mSubviews:Ljava/util/Vector;

    iput-object p2, p0, Lcom/android/gallery3d/filtershow/PanelController$Panel;->mView:Landroid/view/View;

    iput-object p3, p0, Lcom/android/gallery3d/filtershow/PanelController$Panel;->mContainer:Landroid/view/View;

    iput p4, p0, Lcom/android/gallery3d/filtershow/PanelController$Panel;->mPosition:I

    return-void
.end method

.method static synthetic access$300(Lcom/android/gallery3d/filtershow/PanelController$Panel;)Landroid/view/View;
    .locals 1
    .param p0    # Lcom/android/gallery3d/filtershow/PanelController$Panel;

    iget-object v0, p0, Lcom/android/gallery3d/filtershow/PanelController$Panel;->mContainer:Landroid/view/View;

    return-object v0
.end method


# virtual methods
.method public addView(Landroid/view/View;)V
    .locals 1
    .param p1    # Landroid/view/View;

    iget-object v0, p0, Lcom/android/gallery3d/filtershow/PanelController$Panel;->mSubviews:Ljava/util/Vector;

    invoke-virtual {v0, p1}, Ljava/util/Vector;->add(Ljava/lang/Object;)Z

    return-void
.end method

.method public getPosition()I
    .locals 1

    iget v0, p0, Lcom/android/gallery3d/filtershow/PanelController$Panel;->mPosition:I

    return v0
.end method

.method public select(II)Landroid/view/ViewPropertyAnimator;
    .locals 6
    .param p1    # I
    .param p2    # I

    const/4 v5, 0x0

    iget-object v3, p0, Lcom/android/gallery3d/filtershow/PanelController$Panel;->mView:Landroid/view/View;

    const/4 v4, 0x1

    invoke-virtual {v3, v4}, Landroid/view/View;->setSelected(Z)V

    iget-object v3, p0, Lcom/android/gallery3d/filtershow/PanelController$Panel;->mContainer:Landroid/view/View;

    const/4 v4, 0x0

    invoke-virtual {v3, v4}, Landroid/view/View;->setVisibility(I)V

    iget-object v3, p0, Lcom/android/gallery3d/filtershow/PanelController$Panel;->mContainer:Landroid/view/View;

    invoke-virtual {v3, v5}, Landroid/view/View;->setX(F)V

    iget-object v3, p0, Lcom/android/gallery3d/filtershow/PanelController$Panel;->mContainer:Landroid/view/View;

    invoke-virtual {v3, v5}, Landroid/view/View;->setY(F)V

    iget-object v3, p0, Lcom/android/gallery3d/filtershow/PanelController$Panel;->mContainer:Landroid/view/View;

    invoke-virtual {v3}, Landroid/view/View;->animate()Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    iget-object v3, p0, Lcom/android/gallery3d/filtershow/PanelController$Panel;->this$0:Lcom/android/gallery3d/filtershow/PanelController;

    invoke-static {v3}, Lcom/android/gallery3d/filtershow/PanelController;->access$000(Lcom/android/gallery3d/filtershow/PanelController;)Landroid/view/View;

    move-result-object v3

    invoke-virtual {v3}, Landroid/view/View;->getWidth()I

    move-result v2

    iget-object v3, p0, Lcom/android/gallery3d/filtershow/PanelController$Panel;->this$0:Lcom/android/gallery3d/filtershow/PanelController;

    invoke-static {v3}, Lcom/android/gallery3d/filtershow/PanelController;->access$000(Lcom/android/gallery3d/filtershow/PanelController;)Landroid/view/View;

    move-result-object v3

    invoke-virtual {v3}, Landroid/view/View;->getHeight()I

    move-result v1

    invoke-static {}, Lcom/android/gallery3d/filtershow/PanelController;->access$100()I

    move-result v3

    if-ne p2, v3, :cond_2

    iget v3, p0, Lcom/android/gallery3d/filtershow/PanelController$Panel;->mPosition:I

    if-ge p1, v3, :cond_1

    iget-object v3, p0, Lcom/android/gallery3d/filtershow/PanelController$Panel;->mContainer:Landroid/view/View;

    int-to-float v4, v2

    invoke-virtual {v3, v4}, Landroid/view/View;->setX(F)V

    :goto_0
    invoke-virtual {v0, v5}, Landroid/view/ViewPropertyAnimator;->x(F)Landroid/view/ViewPropertyAnimator;

    :cond_0
    :goto_1
    const-wide/16 v3, 0xc8

    invoke-virtual {v0, v3, v4}, Landroid/view/ViewPropertyAnimator;->setDuration(J)Landroid/view/ViewPropertyAnimator;

    move-result-object v3

    invoke-virtual {v3}, Landroid/view/ViewPropertyAnimator;->withLayer()Landroid/view/ViewPropertyAnimator;

    return-object v0

    :cond_1
    iget-object v3, p0, Lcom/android/gallery3d/filtershow/PanelController$Panel;->mContainer:Landroid/view/View;

    neg-int v4, v2

    int-to-float v4, v4

    invoke-virtual {v3, v4}, Landroid/view/View;->setX(F)V

    goto :goto_0

    :cond_2
    invoke-static {}, Lcom/android/gallery3d/filtershow/PanelController;->access$200()I

    move-result v3

    if-ne p2, v3, :cond_0

    iget-object v3, p0, Lcom/android/gallery3d/filtershow/PanelController$Panel;->mContainer:Landroid/view/View;

    int-to-float v4, v1

    invoke-virtual {v3, v4}, Landroid/view/View;->setY(F)V

    invoke-virtual {v0, v5}, Landroid/view/ViewPropertyAnimator;->y(F)Landroid/view/ViewPropertyAnimator;

    goto :goto_1
.end method

.method public unselect(II)Landroid/view/ViewPropertyAnimator;
    .locals 7
    .param p1    # I
    .param p2    # I

    const/4 v6, 0x0

    iget-object v4, p0, Lcom/android/gallery3d/filtershow/PanelController$Panel;->mContainer:Landroid/view/View;

    invoke-virtual {v4}, Landroid/view/View;->animate()Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    iget-object v4, p0, Lcom/android/gallery3d/filtershow/PanelController$Panel;->mView:Landroid/view/View;

    const/4 v5, 0x0

    invoke-virtual {v4, v5}, Landroid/view/View;->setSelected(Z)V

    iget-object v4, p0, Lcom/android/gallery3d/filtershow/PanelController$Panel;->mContainer:Landroid/view/View;

    invoke-virtual {v4, v6}, Landroid/view/View;->setX(F)V

    iget-object v4, p0, Lcom/android/gallery3d/filtershow/PanelController$Panel;->mContainer:Landroid/view/View;

    invoke-virtual {v4, v6}, Landroid/view/View;->setY(F)V

    const/4 v1, 0x0

    iget-object v4, p0, Lcom/android/gallery3d/filtershow/PanelController$Panel;->this$0:Lcom/android/gallery3d/filtershow/PanelController;

    invoke-static {v4}, Lcom/android/gallery3d/filtershow/PanelController;->access$000(Lcom/android/gallery3d/filtershow/PanelController;)Landroid/view/View;

    move-result-object v4

    invoke-virtual {v4}, Landroid/view/View;->getWidth()I

    move-result v3

    iget-object v4, p0, Lcom/android/gallery3d/filtershow/PanelController$Panel;->this$0:Lcom/android/gallery3d/filtershow/PanelController;

    invoke-static {v4}, Lcom/android/gallery3d/filtershow/PanelController;->access$000(Lcom/android/gallery3d/filtershow/PanelController;)Landroid/view/View;

    move-result-object v4

    invoke-virtual {v4}, Landroid/view/View;->getHeight()I

    move-result v2

    invoke-static {}, Lcom/android/gallery3d/filtershow/PanelController;->access$100()I

    move-result v4

    if-ne p2, v4, :cond_2

    iget v4, p0, Lcom/android/gallery3d/filtershow/PanelController$Panel;->mPosition:I

    if-le p1, v4, :cond_1

    neg-int v1, v3

    :goto_0
    int-to-float v4, v1

    invoke-virtual {v0, v4}, Landroid/view/ViewPropertyAnimator;->x(F)Landroid/view/ViewPropertyAnimator;

    :cond_0
    :goto_1
    const-wide/16 v4, 0xc8

    invoke-virtual {v0, v4, v5}, Landroid/view/ViewPropertyAnimator;->setDuration(J)Landroid/view/ViewPropertyAnimator;

    move-result-object v4

    invoke-virtual {v4}, Landroid/view/ViewPropertyAnimator;->withLayer()Landroid/view/ViewPropertyAnimator;

    move-result-object v4

    new-instance v5, Lcom/android/gallery3d/filtershow/PanelController$Panel$1;

    invoke-direct {v5, p0}, Lcom/android/gallery3d/filtershow/PanelController$Panel$1;-><init>(Lcom/android/gallery3d/filtershow/PanelController$Panel;)V

    invoke-virtual {v4, v5}, Landroid/view/ViewPropertyAnimator;->withEndAction(Ljava/lang/Runnable;)Landroid/view/ViewPropertyAnimator;

    return-object v0

    :cond_1
    move v1, v3

    goto :goto_0

    :cond_2
    invoke-static {}, Lcom/android/gallery3d/filtershow/PanelController;->access$200()I

    move-result v4

    if-ne p2, v4, :cond_0

    int-to-float v4, v2

    invoke-virtual {v0, v4}, Landroid/view/ViewPropertyAnimator;->y(F)Landroid/view/ViewPropertyAnimator;

    goto :goto_1
.end method
