.class public Lcom/android/gallery3d/filtershow/filters/ImageFilterStraighten;
.super Lcom/android/gallery3d/filtershow/filters/ImageFilter;
.source "ImageFilterStraighten.java"


# instance fields
.field private final mConfig:Landroid/graphics/Bitmap$Config;

.field private mRotation:F

.field private mZoomFactor:F


# direct methods
.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Lcom/android/gallery3d/filtershow/filters/ImageFilter;-><init>()V

    sget-object v0, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    iput-object v0, p0, Lcom/android/gallery3d/filtershow/filters/ImageFilterStraighten;->mConfig:Landroid/graphics/Bitmap$Config;

    const-string v0, "Straighten"

    iput-object v0, p0, Lcom/android/gallery3d/filtershow/filters/ImageFilter;->mName:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(FF)V
    .locals 1
    .param p1    # F
    .param p2    # F

    invoke-direct {p0}, Lcom/android/gallery3d/filtershow/filters/ImageFilter;-><init>()V

    sget-object v0, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    iput-object v0, p0, Lcom/android/gallery3d/filtershow/filters/ImageFilterStraighten;->mConfig:Landroid/graphics/Bitmap$Config;

    iput p1, p0, Lcom/android/gallery3d/filtershow/filters/ImageFilterStraighten;->mRotation:F

    iput p2, p0, Lcom/android/gallery3d/filtershow/filters/ImageFilterStraighten;->mZoomFactor:F

    return-void
.end method


# virtual methods
.method public apply(Landroid/graphics/Bitmap;FZ)Landroid/graphics/Bitmap;
    .locals 17
    .param p1    # Landroid/graphics/Bitmap;
    .param p2    # F
    .param p3    # Z

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/android/gallery3d/filtershow/filters/ImageFilterStraighten;->mConfig:Landroid/graphics/Bitmap$Config;

    const/4 v4, 0x1

    move-object/from16 v0, p1

    invoke-virtual {v0, v3, v4}, Landroid/graphics/Bitmap;->copy(Landroid/graphics/Bitmap$Config;Z)Landroid/graphics/Bitmap;

    move-result-object v1

    new-instance v12, Landroid/graphics/Canvas;

    invoke-direct {v12, v1}, Landroid/graphics/Canvas;-><init>(Landroid/graphics/Bitmap;)V

    invoke-virtual {v12}, Landroid/graphics/Canvas;->save()I

    new-instance v11, Landroid/graphics/Rect;

    const/4 v3, 0x0

    const/4 v4, 0x0

    invoke-virtual/range {p1 .. p1}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v5

    invoke-virtual/range {p1 .. p1}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v6

    invoke-direct {v11, v3, v4, v5, v6}, Landroid/graphics/Rect;-><init>(IIII)V

    invoke-virtual {v1}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v3

    int-to-float v0, v3

    move/from16 v16, v0

    invoke-virtual {v1}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v3

    int-to-float v13, v3

    invoke-virtual {v1}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v3

    int-to-float v3, v3

    const/high16 v4, 0x40000000

    div-float v15, v3, v4

    invoke-virtual {v1}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v3

    int-to-float v3, v3

    const/high16 v4, 0x40000000

    div-float v14, v3, v4

    move-object/from16 v0, p0

    iget v3, v0, Lcom/android/gallery3d/filtershow/filters/ImageFilterStraighten;->mZoomFactor:F

    move-object/from16 v0, p0

    iget v4, v0, Lcom/android/gallery3d/filtershow/filters/ImageFilterStraighten;->mZoomFactor:F

    invoke-virtual {v12, v3, v4, v15, v14}, Landroid/graphics/Canvas;->scale(FFFF)V

    move-object/from16 v0, p0

    iget v3, v0, Lcom/android/gallery3d/filtershow/filters/ImageFilterStraighten;->mRotation:F

    invoke-virtual {v12, v3, v15, v14}, Landroid/graphics/Canvas;->rotate(FFF)V

    new-instance v3, Landroid/graphics/Paint;

    invoke-direct {v3}, Landroid/graphics/Paint;-><init>()V

    move-object/from16 v0, p1

    invoke-virtual {v12, v0, v11, v11, v3}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;Landroid/graphics/Rect;Landroid/graphics/Rect;Landroid/graphics/Paint;)V

    invoke-virtual {v12}, Landroid/graphics/Canvas;->restore()V

    mul-float v3, v16, v13

    float-to-int v3, v3

    new-array v2, v3, [I

    const/4 v3, 0x0

    move/from16 v0, v16

    float-to-int v4, v0

    const/4 v5, 0x0

    const/4 v6, 0x0

    move/from16 v0, v16

    float-to-int v7, v0

    float-to-int v8, v13

    invoke-virtual/range {v1 .. v8}, Landroid/graphics/Bitmap;->getPixels([IIIIIII)V

    const/4 v5, 0x0

    move/from16 v0, v16

    float-to-int v6, v0

    const/4 v7, 0x0

    const/4 v8, 0x0

    move/from16 v0, v16

    float-to-int v9, v0

    float-to-int v10, v13

    move-object/from16 v3, p1

    move-object v4, v2

    invoke-virtual/range {v3 .. v10}, Landroid/graphics/Bitmap;->setPixels([IIIIIII)V

    invoke-virtual {v1}, Landroid/graphics/Bitmap;->recycle()V

    const/4 v1, 0x0

    const/4 v2, 0x0

    return-object p1
.end method

.method public clone()Lcom/android/gallery3d/filtershow/filters/ImageFilter;
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/CloneNotSupportedException;
        }
    .end annotation

    invoke-super {p0}, Lcom/android/gallery3d/filtershow/filters/ImageFilter;->clone()Lcom/android/gallery3d/filtershow/filters/ImageFilter;

    move-result-object v0

    check-cast v0, Lcom/android/gallery3d/filtershow/filters/ImageFilterStraighten;

    iget v1, p0, Lcom/android/gallery3d/filtershow/filters/ImageFilterStraighten;->mRotation:F

    iput v1, v0, Lcom/android/gallery3d/filtershow/filters/ImageFilterStraighten;->mRotation:F

    iget v1, p0, Lcom/android/gallery3d/filtershow/filters/ImageFilterStraighten;->mZoomFactor:F

    iput v1, v0, Lcom/android/gallery3d/filtershow/filters/ImageFilterStraighten;->mZoomFactor:F

    return-object v0
.end method

.method public bridge synthetic clone()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/CloneNotSupportedException;
        }
    .end annotation

    invoke-virtual {p0}, Lcom/android/gallery3d/filtershow/filters/ImageFilterStraighten;->clone()Lcom/android/gallery3d/filtershow/filters/ImageFilter;

    move-result-object v0

    return-object v0
.end method

.method public setRotation(F)V
    .locals 0
    .param p1    # F

    iput p1, p0, Lcom/android/gallery3d/filtershow/filters/ImageFilterStraighten;->mRotation:F

    return-void
.end method

.method public setRotationZoomFactor(F)V
    .locals 0
    .param p1    # F

    iput p1, p0, Lcom/android/gallery3d/filtershow/filters/ImageFilterStraighten;->mZoomFactor:F

    return-void
.end method
