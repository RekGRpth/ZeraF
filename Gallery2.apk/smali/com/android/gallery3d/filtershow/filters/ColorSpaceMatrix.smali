.class public Lcom/android/gallery3d/filtershow/filters/ColorSpaceMatrix;
.super Ljava/lang/Object;
.source "ColorSpaceMatrix.java"


# static fields
.field private static final BLUM:F = 0.082f

.field private static final GLUM:F = 0.6094f

.field private static final RLUM:F = 0.3086f


# instance fields
.field private final mMatrix:[F


# direct methods
.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/16 v0, 0x10

    new-array v0, v0, [F

    iput-object v0, p0, Lcom/android/gallery3d/filtershow/filters/ColorSpaceMatrix;->mMatrix:[F

    invoke-virtual {p0}, Lcom/android/gallery3d/filtershow/filters/ColorSpaceMatrix;->identity()V

    return-void
.end method

.method public constructor <init>(Lcom/android/gallery3d/filtershow/filters/ColorSpaceMatrix;)V
    .locals 4
    .param p1    # Lcom/android/gallery3d/filtershow/filters/ColorSpaceMatrix;

    const/4 v3, 0x0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/16 v0, 0x10

    new-array v0, v0, [F

    iput-object v0, p0, Lcom/android/gallery3d/filtershow/filters/ColorSpaceMatrix;->mMatrix:[F

    iget-object v0, p1, Lcom/android/gallery3d/filtershow/filters/ColorSpaceMatrix;->mMatrix:[F

    iget-object v1, p0, Lcom/android/gallery3d/filtershow/filters/ColorSpaceMatrix;->mMatrix:[F

    iget-object v2, p1, Lcom/android/gallery3d/filtershow/filters/ColorSpaceMatrix;->mMatrix:[F

    array-length v2, v2

    invoke-static {v0, v3, v1, v3, v2}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    return-void
.end method

.method private getBluef(FFF)F
    .locals 3
    .param p1    # F
    .param p2    # F
    .param p3    # F

    iget-object v0, p0, Lcom/android/gallery3d/filtershow/filters/ColorSpaceMatrix;->mMatrix:[F

    const/4 v1, 0x2

    aget v0, v0, v1

    mul-float/2addr v0, p1

    iget-object v1, p0, Lcom/android/gallery3d/filtershow/filters/ColorSpaceMatrix;->mMatrix:[F

    const/4 v2, 0x6

    aget v1, v1, v2

    mul-float/2addr v1, p2

    add-float/2addr v0, v1

    iget-object v1, p0, Lcom/android/gallery3d/filtershow/filters/ColorSpaceMatrix;->mMatrix:[F

    const/16 v2, 0xa

    aget v1, v1, v2

    mul-float/2addr v1, p3

    add-float/2addr v0, v1

    iget-object v1, p0, Lcom/android/gallery3d/filtershow/filters/ColorSpaceMatrix;->mMatrix:[F

    const/16 v2, 0xe

    aget v1, v1, v2

    add-float/2addr v0, v1

    return v0
.end method

.method private getGreenf(FFF)F
    .locals 3
    .param p1    # F
    .param p2    # F
    .param p3    # F

    iget-object v0, p0, Lcom/android/gallery3d/filtershow/filters/ColorSpaceMatrix;->mMatrix:[F

    const/4 v1, 0x1

    aget v0, v0, v1

    mul-float/2addr v0, p1

    iget-object v1, p0, Lcom/android/gallery3d/filtershow/filters/ColorSpaceMatrix;->mMatrix:[F

    const/4 v2, 0x5

    aget v1, v1, v2

    mul-float/2addr v1, p2

    add-float/2addr v0, v1

    iget-object v1, p0, Lcom/android/gallery3d/filtershow/filters/ColorSpaceMatrix;->mMatrix:[F

    const/16 v2, 0x9

    aget v1, v1, v2

    mul-float/2addr v1, p3

    add-float/2addr v0, v1

    iget-object v1, p0, Lcom/android/gallery3d/filtershow/filters/ColorSpaceMatrix;->mMatrix:[F

    const/16 v2, 0xd

    aget v1, v1, v2

    add-float/2addr v0, v1

    return v0
.end method

.method private getRedf(FFF)F
    .locals 3
    .param p1    # F
    .param p2    # F
    .param p3    # F

    iget-object v0, p0, Lcom/android/gallery3d/filtershow/filters/ColorSpaceMatrix;->mMatrix:[F

    const/4 v1, 0x0

    aget v0, v0, v1

    mul-float/2addr v0, p1

    iget-object v1, p0, Lcom/android/gallery3d/filtershow/filters/ColorSpaceMatrix;->mMatrix:[F

    const/4 v2, 0x4

    aget v1, v1, v2

    mul-float/2addr v1, p2

    add-float/2addr v0, v1

    iget-object v1, p0, Lcom/android/gallery3d/filtershow/filters/ColorSpaceMatrix;->mMatrix:[F

    const/16 v2, 0x8

    aget v1, v1, v2

    mul-float/2addr v1, p3

    add-float/2addr v0, v1

    iget-object v1, p0, Lcom/android/gallery3d/filtershow/filters/ColorSpaceMatrix;->mMatrix:[F

    const/16 v2, 0xc

    aget v1, v1, v2

    add-float/2addr v0, v1

    return v0
.end method

.method private multiply([F)V
    .locals 11
    .param p1    # [F

    const/16 v10, 0x10

    const/4 v9, 0x4

    new-array v1, v10, [F

    const/4 v3, 0x0

    :goto_0
    if-ge v3, v9, :cond_1

    mul-int/lit8 v4, v3, 0x4

    const/4 v2, 0x0

    :goto_1
    if-ge v2, v9, :cond_0

    add-int v5, v4, v2

    iget-object v6, p0, Lcom/android/gallery3d/filtershow/filters/ColorSpaceMatrix;->mMatrix:[F

    add-int/lit8 v7, v4, 0x0

    aget v6, v6, v7

    aget v7, p1, v2

    mul-float/2addr v6, v7

    iget-object v7, p0, Lcom/android/gallery3d/filtershow/filters/ColorSpaceMatrix;->mMatrix:[F

    add-int/lit8 v8, v4, 0x1

    aget v7, v7, v8

    add-int/lit8 v8, v2, 0x4

    aget v8, p1, v8

    mul-float/2addr v7, v8

    add-float/2addr v6, v7

    iget-object v7, p0, Lcom/android/gallery3d/filtershow/filters/ColorSpaceMatrix;->mMatrix:[F

    add-int/lit8 v8, v4, 0x2

    aget v7, v7, v8

    add-int/lit8 v8, v2, 0x8

    aget v8, p1, v8

    mul-float/2addr v7, v8

    add-float/2addr v6, v7

    iget-object v7, p0, Lcom/android/gallery3d/filtershow/filters/ColorSpaceMatrix;->mMatrix:[F

    add-int/lit8 v8, v4, 0x3

    aget v7, v7, v8

    add-int/lit8 v8, v2, 0xc

    aget v8, p1, v8

    mul-float/2addr v7, v8

    add-float/2addr v6, v7

    aput v6, v1, v5

    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    :cond_0
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    :cond_1
    const/4 v0, 0x0

    :goto_2
    if-ge v0, v10, :cond_2

    iget-object v5, p0, Lcom/android/gallery3d/filtershow/filters/ColorSpaceMatrix;->mMatrix:[F

    aget v6, v1, v0

    aput v6, v5, v0

    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    :cond_2
    return-void
.end method

.method private xRotateMatrix(FF)V
    .locals 4
    .param p1    # F
    .param p2    # F

    new-instance v0, Lcom/android/gallery3d/filtershow/filters/ColorSpaceMatrix;

    invoke-direct {v0}, Lcom/android/gallery3d/filtershow/filters/ColorSpaceMatrix;-><init>()V

    iget-object v1, v0, Lcom/android/gallery3d/filtershow/filters/ColorSpaceMatrix;->mMatrix:[F

    const/4 v2, 0x5

    aput p2, v1, v2

    const/4 v2, 0x6

    aput p1, v1, v2

    const/16 v2, 0x9

    neg-float v3, p1

    aput v3, v1, v2

    const/16 v2, 0xa

    aput p2, v1, v2

    invoke-direct {p0, v1}, Lcom/android/gallery3d/filtershow/filters/ColorSpaceMatrix;->multiply([F)V

    return-void
.end method

.method private yRotateMatrix(FF)V
    .locals 4
    .param p1    # F
    .param p2    # F

    new-instance v0, Lcom/android/gallery3d/filtershow/filters/ColorSpaceMatrix;

    invoke-direct {v0}, Lcom/android/gallery3d/filtershow/filters/ColorSpaceMatrix;-><init>()V

    iget-object v1, v0, Lcom/android/gallery3d/filtershow/filters/ColorSpaceMatrix;->mMatrix:[F

    const/4 v2, 0x0

    aput p2, v1, v2

    const/4 v2, 0x2

    neg-float v3, p1

    aput v3, v1, v2

    const/16 v2, 0x8

    aput p1, v1, v2

    const/16 v2, 0xa

    aput p2, v1, v2

    invoke-direct {p0, v1}, Lcom/android/gallery3d/filtershow/filters/ColorSpaceMatrix;->multiply([F)V

    return-void
.end method

.method private zRotateMatrix(FF)V
    .locals 4
    .param p1    # F
    .param p2    # F

    new-instance v0, Lcom/android/gallery3d/filtershow/filters/ColorSpaceMatrix;

    invoke-direct {v0}, Lcom/android/gallery3d/filtershow/filters/ColorSpaceMatrix;-><init>()V

    iget-object v1, v0, Lcom/android/gallery3d/filtershow/filters/ColorSpaceMatrix;->mMatrix:[F

    const/4 v2, 0x0

    aput p2, v1, v2

    const/4 v2, 0x1

    aput p1, v1, v2

    const/4 v2, 0x4

    neg-float v3, p1

    aput v3, v1, v2

    const/4 v2, 0x5

    aput p2, v1, v2

    invoke-direct {p0, v1}, Lcom/android/gallery3d/filtershow/filters/ColorSpaceMatrix;->multiply([F)V

    return-void
.end method

.method private zShearMatrix(FF)V
    .locals 3
    .param p1    # F
    .param p2    # F

    new-instance v0, Lcom/android/gallery3d/filtershow/filters/ColorSpaceMatrix;

    invoke-direct {v0}, Lcom/android/gallery3d/filtershow/filters/ColorSpaceMatrix;-><init>()V

    iget-object v1, v0, Lcom/android/gallery3d/filtershow/filters/ColorSpaceMatrix;->mMatrix:[F

    const/4 v2, 0x2

    aput p1, v1, v2

    const/4 v2, 0x6

    aput p2, v1, v2

    invoke-direct {p0, v1}, Lcom/android/gallery3d/filtershow/filters/ColorSpaceMatrix;->multiply([F)V

    return-void
.end method


# virtual methods
.method public changeSaturation(F)V
    .locals 7
    .param p1    # F

    const v6, 0x3f1c01a3

    const v5, 0x3e9e00d2

    const v4, 0x3da7ef9e

    const/high16 v3, 0x3f800000

    iget-object v0, p0, Lcom/android/gallery3d/filtershow/filters/ColorSpaceMatrix;->mMatrix:[F

    const/4 v1, 0x0

    sub-float v2, v3, p1

    mul-float/2addr v2, v5

    add-float/2addr v2, p1

    aput v2, v0, v1

    iget-object v0, p0, Lcom/android/gallery3d/filtershow/filters/ColorSpaceMatrix;->mMatrix:[F

    const/4 v1, 0x1

    sub-float v2, v3, p1

    mul-float/2addr v2, v5

    aput v2, v0, v1

    iget-object v0, p0, Lcom/android/gallery3d/filtershow/filters/ColorSpaceMatrix;->mMatrix:[F

    const/4 v1, 0x2

    sub-float v2, v3, p1

    mul-float/2addr v2, v5

    aput v2, v0, v1

    iget-object v0, p0, Lcom/android/gallery3d/filtershow/filters/ColorSpaceMatrix;->mMatrix:[F

    const/4 v1, 0x4

    sub-float v2, v3, p1

    mul-float/2addr v2, v6

    aput v2, v0, v1

    iget-object v0, p0, Lcom/android/gallery3d/filtershow/filters/ColorSpaceMatrix;->mMatrix:[F

    const/4 v1, 0x5

    sub-float v2, v3, p1

    mul-float/2addr v2, v6

    add-float/2addr v2, p1

    aput v2, v0, v1

    iget-object v0, p0, Lcom/android/gallery3d/filtershow/filters/ColorSpaceMatrix;->mMatrix:[F

    const/4 v1, 0x6

    sub-float v2, v3, p1

    mul-float/2addr v2, v6

    aput v2, v0, v1

    iget-object v0, p0, Lcom/android/gallery3d/filtershow/filters/ColorSpaceMatrix;->mMatrix:[F

    const/16 v1, 0x8

    sub-float v2, v3, p1

    mul-float/2addr v2, v4

    aput v2, v0, v1

    iget-object v0, p0, Lcom/android/gallery3d/filtershow/filters/ColorSpaceMatrix;->mMatrix:[F

    const/16 v1, 0x9

    sub-float v2, v3, p1

    mul-float/2addr v2, v4

    aput v2, v0, v1

    iget-object v0, p0, Lcom/android/gallery3d/filtershow/filters/ColorSpaceMatrix;->mMatrix:[F

    const/16 v1, 0xa

    sub-float v2, v3, p1

    mul-float/2addr v2, v4

    add-float/2addr v2, p1

    aput v2, v0, v1

    return-void
.end method

.method public convertToLuminance()V
    .locals 7

    iget-object v0, p0, Lcom/android/gallery3d/filtershow/filters/ColorSpaceMatrix;->mMatrix:[F

    const/4 v1, 0x0

    iget-object v2, p0, Lcom/android/gallery3d/filtershow/filters/ColorSpaceMatrix;->mMatrix:[F

    const/4 v3, 0x1

    iget-object v4, p0, Lcom/android/gallery3d/filtershow/filters/ColorSpaceMatrix;->mMatrix:[F

    const/4 v5, 0x2

    const v6, 0x3e9e00d2

    aput v6, v4, v5

    aput v6, v2, v3

    aput v6, v0, v1

    iget-object v0, p0, Lcom/android/gallery3d/filtershow/filters/ColorSpaceMatrix;->mMatrix:[F

    const/4 v1, 0x4

    iget-object v2, p0, Lcom/android/gallery3d/filtershow/filters/ColorSpaceMatrix;->mMatrix:[F

    const/4 v3, 0x5

    iget-object v4, p0, Lcom/android/gallery3d/filtershow/filters/ColorSpaceMatrix;->mMatrix:[F

    const/4 v5, 0x6

    const v6, 0x3f1c01a3

    aput v6, v4, v5

    aput v6, v2, v3

    aput v6, v0, v1

    iget-object v0, p0, Lcom/android/gallery3d/filtershow/filters/ColorSpaceMatrix;->mMatrix:[F

    const/16 v1, 0x8

    iget-object v2, p0, Lcom/android/gallery3d/filtershow/filters/ColorSpaceMatrix;->mMatrix:[F

    const/16 v3, 0x9

    iget-object v4, p0, Lcom/android/gallery3d/filtershow/filters/ColorSpaceMatrix;->mMatrix:[F

    const/16 v5, 0xa

    const v6, 0x3da7ef9e

    aput v6, v4, v5

    aput v6, v2, v3

    aput v6, v0, v1

    return-void
.end method

.method public getBlue(III)F
    .locals 4
    .param p1    # I
    .param p2    # I
    .param p3    # I

    int-to-float v0, p1

    iget-object v1, p0, Lcom/android/gallery3d/filtershow/filters/ColorSpaceMatrix;->mMatrix:[F

    const/4 v2, 0x2

    aget v1, v1, v2

    mul-float/2addr v0, v1

    int-to-float v1, p2

    iget-object v2, p0, Lcom/android/gallery3d/filtershow/filters/ColorSpaceMatrix;->mMatrix:[F

    const/4 v3, 0x6

    aget v2, v2, v3

    mul-float/2addr v1, v2

    add-float/2addr v0, v1

    int-to-float v1, p3

    iget-object v2, p0, Lcom/android/gallery3d/filtershow/filters/ColorSpaceMatrix;->mMatrix:[F

    const/16 v3, 0xa

    aget v2, v2, v3

    mul-float/2addr v1, v2

    add-float/2addr v0, v1

    iget-object v1, p0, Lcom/android/gallery3d/filtershow/filters/ColorSpaceMatrix;->mMatrix:[F

    const/16 v2, 0xe

    aget v1, v1, v2

    add-float/2addr v0, v1

    return v0
.end method

.method public getGreen(III)F
    .locals 4
    .param p1    # I
    .param p2    # I
    .param p3    # I

    int-to-float v0, p1

    iget-object v1, p0, Lcom/android/gallery3d/filtershow/filters/ColorSpaceMatrix;->mMatrix:[F

    const/4 v2, 0x1

    aget v1, v1, v2

    mul-float/2addr v0, v1

    int-to-float v1, p2

    iget-object v2, p0, Lcom/android/gallery3d/filtershow/filters/ColorSpaceMatrix;->mMatrix:[F

    const/4 v3, 0x5

    aget v2, v2, v3

    mul-float/2addr v1, v2

    add-float/2addr v0, v1

    int-to-float v1, p3

    iget-object v2, p0, Lcom/android/gallery3d/filtershow/filters/ColorSpaceMatrix;->mMatrix:[F

    const/16 v3, 0x9

    aget v2, v2, v3

    mul-float/2addr v1, v2

    add-float/2addr v0, v1

    iget-object v1, p0, Lcom/android/gallery3d/filtershow/filters/ColorSpaceMatrix;->mMatrix:[F

    const/16 v2, 0xd

    aget v1, v1, v2

    add-float/2addr v0, v1

    return v0
.end method

.method public getMatrix()[F
    .locals 1

    iget-object v0, p0, Lcom/android/gallery3d/filtershow/filters/ColorSpaceMatrix;->mMatrix:[F

    return-object v0
.end method

.method public getRed(III)F
    .locals 4
    .param p1    # I
    .param p2    # I
    .param p3    # I

    int-to-float v0, p1

    iget-object v1, p0, Lcom/android/gallery3d/filtershow/filters/ColorSpaceMatrix;->mMatrix:[F

    const/4 v2, 0x0

    aget v1, v1, v2

    mul-float/2addr v0, v1

    int-to-float v1, p2

    iget-object v2, p0, Lcom/android/gallery3d/filtershow/filters/ColorSpaceMatrix;->mMatrix:[F

    const/4 v3, 0x4

    aget v2, v2, v3

    mul-float/2addr v1, v2

    add-float/2addr v0, v1

    int-to-float v1, p3

    iget-object v2, p0, Lcom/android/gallery3d/filtershow/filters/ColorSpaceMatrix;->mMatrix:[F

    const/16 v3, 0x8

    aget v2, v2, v3

    mul-float/2addr v1, v2

    add-float/2addr v0, v1

    iget-object v1, p0, Lcom/android/gallery3d/filtershow/filters/ColorSpaceMatrix;->mMatrix:[F

    const/16 v2, 0xc

    aget v1, v1, v2

    add-float/2addr v0, v1

    return v0
.end method

.method public identity()V
    .locals 9

    iget-object v0, p0, Lcom/android/gallery3d/filtershow/filters/ColorSpaceMatrix;->mMatrix:[F

    const/4 v1, 0x0

    invoke-static {v0, v1}, Ljava/util/Arrays;->fill([FF)V

    iget-object v0, p0, Lcom/android/gallery3d/filtershow/filters/ColorSpaceMatrix;->mMatrix:[F

    const/4 v1, 0x0

    iget-object v2, p0, Lcom/android/gallery3d/filtershow/filters/ColorSpaceMatrix;->mMatrix:[F

    const/4 v3, 0x5

    iget-object v4, p0, Lcom/android/gallery3d/filtershow/filters/ColorSpaceMatrix;->mMatrix:[F

    const/16 v5, 0xa

    iget-object v6, p0, Lcom/android/gallery3d/filtershow/filters/ColorSpaceMatrix;->mMatrix:[F

    const/16 v7, 0xf

    const/high16 v8, 0x3f800000

    aput v8, v6, v7

    aput v8, v4, v5

    aput v8, v2, v3

    aput v8, v0, v1

    return-void
.end method

.method public setHue(F)V
    .locals 17
    .param p1    # F

    const-wide/high16 v13, 0x4000000000000000L

    invoke-static {v13, v14}, Ljava/lang/Math;->sqrt(D)D

    move-result-wide v13

    double-to-float v4, v13

    const/high16 v13, 0x3f800000

    div-float v6, v13, v4

    const/high16 v13, 0x3f800000

    div-float v5, v13, v4

    move-object/from16 v0, p0

    invoke-direct {v0, v6, v5}, Lcom/android/gallery3d/filtershow/filters/ColorSpaceMatrix;->xRotateMatrix(FF)V

    const-wide/high16 v13, 0x4008000000000000L

    invoke-static {v13, v14}, Ljava/lang/Math;->sqrt(D)D

    move-result-wide v13

    double-to-float v4, v13

    const/high16 v13, -0x40800000

    div-float v8, v13, v4

    const-wide/high16 v13, 0x4000000000000000L

    invoke-static {v13, v14}, Ljava/lang/Math;->sqrt(D)D

    move-result-wide v13

    double-to-float v13, v13

    div-float v7, v13, v4

    move-object/from16 v0, p0

    invoke-direct {v0, v8, v7}, Lcom/android/gallery3d/filtershow/filters/ColorSpaceMatrix;->yRotateMatrix(FF)V

    const v13, 0x3e9e00d2

    const v14, 0x3f1c01a3

    const v15, 0x3da7ef9e

    move-object/from16 v0, p0

    invoke-direct {v0, v13, v14, v15}, Lcom/android/gallery3d/filtershow/filters/ColorSpaceMatrix;->getRedf(FFF)F

    move-result v1

    const v13, 0x3e9e00d2

    const v14, 0x3f1c01a3

    const v15, 0x3da7ef9e

    move-object/from16 v0, p0

    invoke-direct {v0, v13, v14, v15}, Lcom/android/gallery3d/filtershow/filters/ColorSpaceMatrix;->getGreenf(FFF)F

    move-result v2

    const v13, 0x3e9e00d2

    const v14, 0x3f1c01a3

    const v15, 0x3da7ef9e

    move-object/from16 v0, p0

    invoke-direct {v0, v13, v14, v15}, Lcom/android/gallery3d/filtershow/filters/ColorSpaceMatrix;->getBluef(FFF)F

    move-result v3

    div-float v11, v1, v3

    div-float v12, v2, v3

    move-object/from16 v0, p0

    invoke-direct {v0, v11, v12}, Lcom/android/gallery3d/filtershow/filters/ColorSpaceMatrix;->zShearMatrix(FF)V

    move/from16 v0, p1

    float-to-double v13, v0

    const-wide v15, 0x400921fb54442d18L

    mul-double/2addr v13, v15

    const-wide v15, 0x4066800000000000L

    div-double/2addr v13, v15

    invoke-static {v13, v14}, Ljava/lang/Math;->sin(D)D

    move-result-wide v13

    double-to-float v10, v13

    move/from16 v0, p1

    float-to-double v13, v0

    const-wide v15, 0x400921fb54442d18L

    mul-double/2addr v13, v15

    const-wide v15, 0x4066800000000000L

    div-double/2addr v13, v15

    invoke-static {v13, v14}, Ljava/lang/Math;->cos(D)D

    move-result-wide v13

    double-to-float v9, v13

    move-object/from16 v0, p0

    invoke-direct {v0, v10, v9}, Lcom/android/gallery3d/filtershow/filters/ColorSpaceMatrix;->zRotateMatrix(FF)V

    neg-float v13, v11

    neg-float v14, v12

    move-object/from16 v0, p0

    invoke-direct {v0, v13, v14}, Lcom/android/gallery3d/filtershow/filters/ColorSpaceMatrix;->zShearMatrix(FF)V

    neg-float v13, v8

    move-object/from16 v0, p0

    invoke-direct {v0, v13, v7}, Lcom/android/gallery3d/filtershow/filters/ColorSpaceMatrix;->yRotateMatrix(FF)V

    neg-float v13, v6

    move-object/from16 v0, p0

    invoke-direct {v0, v13, v5}, Lcom/android/gallery3d/filtershow/filters/ColorSpaceMatrix;->xRotateMatrix(FF)V

    return-void
.end method
