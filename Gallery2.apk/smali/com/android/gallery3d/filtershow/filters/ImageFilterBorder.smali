.class public Lcom/android/gallery3d/filtershow/filters/ImageFilterBorder;
.super Lcom/android/gallery3d/filtershow/filters/ImageFilter;
.source "ImageFilterBorder.java"


# instance fields
.field mNinePatch:Landroid/graphics/drawable/Drawable;


# direct methods
.method public constructor <init>(Landroid/graphics/drawable/Drawable;)V
    .locals 1
    .param p1    # Landroid/graphics/drawable/Drawable;

    invoke-direct {p0}, Lcom/android/gallery3d/filtershow/filters/ImageFilter;-><init>()V

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/android/gallery3d/filtershow/filters/ImageFilterBorder;->mNinePatch:Landroid/graphics/drawable/Drawable;

    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/android/gallery3d/filtershow/filters/ImageFilter;->setFilterType(B)V

    const-string v0, "Border"

    iput-object v0, p0, Lcom/android/gallery3d/filtershow/filters/ImageFilter;->mName:Ljava/lang/String;

    iput-object p1, p0, Lcom/android/gallery3d/filtershow/filters/ImageFilterBorder;->mNinePatch:Landroid/graphics/drawable/Drawable;

    return-void
.end method


# virtual methods
.method public apply(Landroid/graphics/Bitmap;FZ)Landroid/graphics/Bitmap;
    .locals 8
    .param p1    # Landroid/graphics/Bitmap;
    .param p2    # F
    .param p3    # Z

    const/4 v7, 0x0

    iget-object v5, p0, Lcom/android/gallery3d/filtershow/filters/ImageFilterBorder;->mNinePatch:Landroid/graphics/drawable/Drawable;

    if-nez v5, :cond_0

    :goto_0
    return-object p1

    :cond_0
    invoke-virtual {p1}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v4

    invoke-virtual {p1}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v2

    const/high16 v5, 0x40000000

    mul-float v3, p2, v5

    new-instance v0, Landroid/graphics/Rect;

    int-to-float v5, v4

    div-float/2addr v5, v3

    float-to-int v5, v5

    int-to-float v6, v2

    div-float/2addr v6, v3

    float-to-int v6, v6

    invoke-direct {v0, v7, v7, v5, v6}, Landroid/graphics/Rect;-><init>(IIII)V

    new-instance v1, Landroid/graphics/Canvas;

    invoke-direct {v1, p1}, Landroid/graphics/Canvas;-><init>(Landroid/graphics/Bitmap;)V

    invoke-virtual {v1, v3, v3}, Landroid/graphics/Canvas;->scale(FF)V

    iget-object v5, p0, Lcom/android/gallery3d/filtershow/filters/ImageFilterBorder;->mNinePatch:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v5, v0}, Landroid/graphics/drawable/Drawable;->setBounds(Landroid/graphics/Rect;)V

    iget-object v5, p0, Lcom/android/gallery3d/filtershow/filters/ImageFilterBorder;->mNinePatch:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v5, v1}, Landroid/graphics/drawable/Drawable;->draw(Landroid/graphics/Canvas;)V

    goto :goto_0
.end method

.method public clone()Lcom/android/gallery3d/filtershow/filters/ImageFilter;
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/CloneNotSupportedException;
        }
    .end annotation

    invoke-super {p0}, Lcom/android/gallery3d/filtershow/filters/ImageFilter;->clone()Lcom/android/gallery3d/filtershow/filters/ImageFilter;

    move-result-object v0

    check-cast v0, Lcom/android/gallery3d/filtershow/filters/ImageFilterBorder;

    iget-object v1, p0, Lcom/android/gallery3d/filtershow/filters/ImageFilterBorder;->mNinePatch:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v0, v1}, Lcom/android/gallery3d/filtershow/filters/ImageFilterBorder;->setDrawable(Landroid/graphics/drawable/Drawable;)V

    return-object v0
.end method

.method public bridge synthetic clone()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/CloneNotSupportedException;
        }
    .end annotation

    invoke-virtual {p0}, Lcom/android/gallery3d/filtershow/filters/ImageFilterBorder;->clone()Lcom/android/gallery3d/filtershow/filters/ImageFilter;

    move-result-object v0

    return-object v0
.end method

.method public isNil()Z
    .locals 1

    iget-object v0, p0, Lcom/android/gallery3d/filtershow/filters/ImageFilterBorder;->mNinePatch:Landroid/graphics/drawable/Drawable;

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public same(Lcom/android/gallery3d/filtershow/filters/ImageFilter;)Z
    .locals 5
    .param p1    # Lcom/android/gallery3d/filtershow/filters/ImageFilter;

    const/4 v2, 0x0

    invoke-super {p0, p1}, Lcom/android/gallery3d/filtershow/filters/ImageFilter;->same(Lcom/android/gallery3d/filtershow/filters/ImageFilter;)Z

    move-result v1

    if-nez v1, :cond_1

    :cond_0
    :goto_0
    return v2

    :cond_1
    instance-of v3, p1, Lcom/android/gallery3d/filtershow/filters/ImageFilterBorder;

    if-eqz v3, :cond_0

    move-object v0, p1

    check-cast v0, Lcom/android/gallery3d/filtershow/filters/ImageFilterBorder;

    iget-object v3, p0, Lcom/android/gallery3d/filtershow/filters/ImageFilterBorder;->mNinePatch:Landroid/graphics/drawable/Drawable;

    iget-object v4, v0, Lcom/android/gallery3d/filtershow/filters/ImageFilterBorder;->mNinePatch:Landroid/graphics/drawable/Drawable;

    if-ne v3, v4, :cond_0

    const/4 v2, 0x1

    goto :goto_0
.end method

.method public setDrawable(Landroid/graphics/drawable/Drawable;)V
    .locals 0
    .param p1    # Landroid/graphics/drawable/Drawable;

    iput-object p1, p0, Lcom/android/gallery3d/filtershow/filters/ImageFilterBorder;->mNinePatch:Landroid/graphics/drawable/Drawable;

    return-void
.end method
