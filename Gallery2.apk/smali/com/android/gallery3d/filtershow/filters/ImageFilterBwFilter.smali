.class public Lcom/android/gallery3d/filtershow/filters/ImageFilterBwFilter;
.super Lcom/android/gallery3d/filtershow/filters/ImageFilter;
.source "ImageFilterBwFilter.java"


# direct methods
.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Lcom/android/gallery3d/filtershow/filters/ImageFilter;-><init>()V

    const-string v0, "BW Filter"

    iput-object v0, p0, Lcom/android/gallery3d/filtershow/filters/ImageFilter;->mName:Ljava/lang/String;

    const/16 v0, 0xb4

    iput v0, p0, Lcom/android/gallery3d/filtershow/filters/ImageFilter;->mMaxParameter:I

    const/16 v0, -0xb4

    iput v0, p0, Lcom/android/gallery3d/filtershow/filters/ImageFilter;->mMinParameter:I

    return-void
.end method


# virtual methods
.method public apply(Landroid/graphics/Bitmap;FZ)Landroid/graphics/Bitmap;
    .locals 10
    .param p1    # Landroid/graphics/Bitmap;
    .param p2    # F
    .param p3    # Z

    const/high16 v9, 0x3f800000

    invoke-virtual {p1}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v2

    invoke-virtual {p1}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v3

    const/4 v0, 0x3

    new-array v7, v0, [F

    const/4 v0, 0x0

    iget v1, p0, Lcom/android/gallery3d/filtershow/filters/ImageFilter;->mParameter:I

    add-int/lit16 v1, v1, 0xb4

    int-to-float v1, v1

    aput v1, v7, v0

    const/4 v0, 0x1

    aput v9, v7, v0

    const/4 v0, 0x2

    aput v9, v7, v0

    invoke-static {v7}, Landroid/graphics/Color;->HSVToColor([F)I

    move-result v8

    shr-int/lit8 v0, v8, 0x10

    and-int/lit16 v4, v0, 0xff

    shr-int/lit8 v0, v8, 0x8

    and-int/lit16 v5, v0, 0xff

    shr-int/lit8 v0, v8, 0x0

    and-int/lit16 v6, v0, 0xff

    move-object v0, p0

    move-object v1, p1

    invoke-virtual/range {v0 .. v6}, Lcom/android/gallery3d/filtershow/filters/ImageFilterBwFilter;->nativeApplyFilter(Landroid/graphics/Bitmap;IIIII)V

    return-object p1
.end method

.method public clone()Lcom/android/gallery3d/filtershow/filters/ImageFilter;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/CloneNotSupportedException;
        }
    .end annotation

    invoke-super {p0}, Lcom/android/gallery3d/filtershow/filters/ImageFilter;->clone()Lcom/android/gallery3d/filtershow/filters/ImageFilter;

    move-result-object v0

    check-cast v0, Lcom/android/gallery3d/filtershow/filters/ImageFilterBwFilter;

    return-object v0
.end method

.method public bridge synthetic clone()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/CloneNotSupportedException;
        }
    .end annotation

    invoke-virtual {p0}, Lcom/android/gallery3d/filtershow/filters/ImageFilterBwFilter;->clone()Lcom/android/gallery3d/filtershow/filters/ImageFilter;

    move-result-object v0

    return-object v0
.end method

.method protected native nativeApplyFilter(Landroid/graphics/Bitmap;IIIII)V
.end method
