.class public Lcom/android/gallery3d/filtershow/filters/ImageFilterWBalance;
.super Lcom/android/gallery3d/filtershow/filters/ImageFilter;
.source "ImageFilterWBalance.java"


# static fields
.field private static final TAG:Ljava/lang/String; = "Gallery2/ImageFilterWBalance"


# direct methods
.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Lcom/android/gallery3d/filtershow/filters/ImageFilter;-><init>()V

    const/4 v0, 0x3

    invoke-virtual {p0, v0}, Lcom/android/gallery3d/filtershow/filters/ImageFilter;->setFilterType(B)V

    const-string v0, "WBalance"

    iput-object v0, p0, Lcom/android/gallery3d/filtershow/filters/ImageFilter;->mName:Ljava/lang/String;

    return-void
.end method


# virtual methods
.method public apply(Landroid/graphics/Bitmap;FZ)Landroid/graphics/Bitmap;
    .locals 6
    .param p1    # Landroid/graphics/Bitmap;
    .param p2    # F
    .param p3    # Z

    const/4 v4, -0x1

    invoke-virtual {p1}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v2

    invoke-virtual {p1}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v3

    move-object v0, p0

    move-object v1, p1

    move v5, v4

    invoke-virtual/range {v0 .. v5}, Lcom/android/gallery3d/filtershow/filters/ImageFilterWBalance;->nativeApplyFilter(Landroid/graphics/Bitmap;IIII)V

    return-object p1
.end method

.method protected native nativeApplyFilter(Landroid/graphics/Bitmap;IIII)V
.end method
