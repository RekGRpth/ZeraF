.class public Lcom/android/gallery3d/filtershow/filters/ImageFilterSharpen;
.super Lcom/android/gallery3d/filtershow/filters/ImageFilterRS;
.source "ImageFilterSharpen.java"


# static fields
.field private static final LOGTAG:Ljava/lang/String; = "Gallery2/ImageFilterSharpen"


# instance fields
.field private mScript:Lcom/android/gallery3d/filtershow/filters/ScriptC_convolve3x3;


# direct methods
.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Lcom/android/gallery3d/filtershow/filters/ImageFilterRS;-><init>()V

    const-string v0, "Sharpen"

    iput-object v0, p0, Lcom/android/gallery3d/filtershow/filters/ImageFilter;->mName:Ljava/lang/String;

    return-void
.end method


# virtual methods
.method public createFilter(Landroid/content/res/Resources;FZ)V
    .locals 9
    .param p1    # Landroid/content/res/Resources;
    .param p2    # F
    .param p3    # Z

    sget-object v6, Lcom/android/gallery3d/filtershow/filters/ImageFilterSharpen;->mInPixelsAllocation:Landroid/renderscript/Allocation;

    invoke-virtual {v6}, Landroid/renderscript/Allocation;->getType()Landroid/renderscript/Type;

    move-result-object v6

    invoke-virtual {v6}, Landroid/renderscript/Type;->getX()I

    move-result v5

    sget-object v6, Lcom/android/gallery3d/filtershow/filters/ImageFilterSharpen;->mInPixelsAllocation:Landroid/renderscript/Allocation;

    invoke-virtual {v6}, Landroid/renderscript/Allocation;->getType()Landroid/renderscript/Type;

    move-result-object v6

    invoke-virtual {v6}, Landroid/renderscript/Type;->getY()I

    move-result v1

    iget v6, p0, Lcom/android/gallery3d/filtershow/filters/ImageFilter;->mParameter:I

    int-to-float v6, v6

    mul-float v3, v6, p2

    const/high16 v6, 0x42c80000

    div-float v4, v3, v6

    const/16 v6, 0x9

    new-array v0, v6, [F

    move v2, v4

    const/4 v6, 0x0

    neg-float v7, v2

    aput v7, v0, v6

    const/4 v6, 0x1

    neg-float v7, v2

    aput v7, v0, v6

    const/4 v6, 0x2

    neg-float v7, v2

    aput v7, v0, v6

    const/4 v6, 0x3

    neg-float v7, v2

    aput v7, v0, v6

    const/4 v6, 0x4

    const/high16 v7, 0x41000000

    mul-float/2addr v7, v2

    const/high16 v8, 0x3f800000

    add-float/2addr v7, v8

    aput v7, v0, v6

    const/4 v6, 0x5

    neg-float v7, v2

    aput v7, v0, v6

    const/4 v6, 0x6

    neg-float v7, v2

    aput v7, v0, v6

    const/4 v6, 0x7

    neg-float v7, v2

    aput v7, v0, v6

    const/16 v6, 0x8

    neg-float v7, v2

    aput v7, v0, v6

    iget-object v6, p0, Lcom/android/gallery3d/filtershow/filters/ImageFilterSharpen;->mScript:Lcom/android/gallery3d/filtershow/filters/ScriptC_convolve3x3;

    if-nez v6, :cond_0

    new-instance v6, Lcom/android/gallery3d/filtershow/filters/ScriptC_convolve3x3;

    invoke-static {}, Lcom/android/gallery3d/filtershow/filters/ImageFilterSharpen;->getRenderScriptContext()Landroid/renderscript/RenderScript;

    move-result-object v7

    const v8, 0x7f070002

    invoke-direct {v6, v7, p1, v8}, Lcom/android/gallery3d/filtershow/filters/ScriptC_convolve3x3;-><init>(Landroid/renderscript/RenderScript;Landroid/content/res/Resources;I)V

    iput-object v6, p0, Lcom/android/gallery3d/filtershow/filters/ImageFilterSharpen;->mScript:Lcom/android/gallery3d/filtershow/filters/ScriptC_convolve3x3;

    :cond_0
    iget-object v6, p0, Lcom/android/gallery3d/filtershow/filters/ImageFilterSharpen;->mScript:Lcom/android/gallery3d/filtershow/filters/ScriptC_convolve3x3;

    invoke-virtual {v6, v0}, Lcom/android/gallery3d/filtershow/filters/ScriptC_convolve3x3;->set_gCoeffs([F)V

    iget-object v6, p0, Lcom/android/gallery3d/filtershow/filters/ImageFilterSharpen;->mScript:Lcom/android/gallery3d/filtershow/filters/ScriptC_convolve3x3;

    invoke-virtual {v6, v5}, Lcom/android/gallery3d/filtershow/filters/ScriptC_convolve3x3;->set_gWidth(I)V

    iget-object v6, p0, Lcom/android/gallery3d/filtershow/filters/ImageFilterSharpen;->mScript:Lcom/android/gallery3d/filtershow/filters/ScriptC_convolve3x3;

    invoke-virtual {v6, v1}, Lcom/android/gallery3d/filtershow/filters/ScriptC_convolve3x3;->set_gHeight(I)V

    return-void
.end method

.method public runFilter()V
    .locals 3

    iget-object v0, p0, Lcom/android/gallery3d/filtershow/filters/ImageFilterSharpen;->mScript:Lcom/android/gallery3d/filtershow/filters/ScriptC_convolve3x3;

    sget-object v1, Lcom/android/gallery3d/filtershow/filters/ImageFilterSharpen;->mInPixelsAllocation:Landroid/renderscript/Allocation;

    invoke-virtual {v0, v1}, Lcom/android/gallery3d/filtershow/filters/ScriptC_convolve3x3;->set_gIn(Landroid/renderscript/Allocation;)V

    iget-object v0, p0, Lcom/android/gallery3d/filtershow/filters/ImageFilterSharpen;->mScript:Lcom/android/gallery3d/filtershow/filters/ScriptC_convolve3x3;

    sget-object v1, Lcom/android/gallery3d/filtershow/filters/ImageFilterSharpen;->mInPixelsAllocation:Landroid/renderscript/Allocation;

    invoke-virtual {v0, v1}, Lcom/android/gallery3d/filtershow/filters/ScriptC_convolve3x3;->bind_gPixels(Landroid/renderscript/Allocation;)V

    iget-object v0, p0, Lcom/android/gallery3d/filtershow/filters/ImageFilterSharpen;->mScript:Lcom/android/gallery3d/filtershow/filters/ScriptC_convolve3x3;

    sget-object v1, Lcom/android/gallery3d/filtershow/filters/ImageFilterSharpen;->mInPixelsAllocation:Landroid/renderscript/Allocation;

    sget-object v2, Lcom/android/gallery3d/filtershow/filters/ImageFilterSharpen;->mOutPixelsAllocation:Landroid/renderscript/Allocation;

    invoke-virtual {v0, v1, v2}, Lcom/android/gallery3d/filtershow/filters/ScriptC_convolve3x3;->forEach_root(Landroid/renderscript/Allocation;Landroid/renderscript/Allocation;)V

    return-void
.end method
