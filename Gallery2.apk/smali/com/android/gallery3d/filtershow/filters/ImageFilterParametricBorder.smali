.class public Lcom/android/gallery3d/filtershow/filters/ImageFilterParametricBorder;
.super Lcom/android/gallery3d/filtershow/filters/ImageFilter;
.source "ImageFilterParametricBorder.java"


# instance fields
.field private mBorderColor:I

.field private mBorderCornerRadius:I

.field private mBorderSize:I


# direct methods
.method public constructor <init>()V
    .locals 2

    const/16 v1, 0xa

    invoke-direct {p0}, Lcom/android/gallery3d/filtershow/filters/ImageFilter;-><init>()V

    const/4 v0, -0x1

    iput v0, p0, Lcom/android/gallery3d/filtershow/filters/ImageFilterParametricBorder;->mBorderColor:I

    iput v1, p0, Lcom/android/gallery3d/filtershow/filters/ImageFilterParametricBorder;->mBorderSize:I

    iput v1, p0, Lcom/android/gallery3d/filtershow/filters/ImageFilterParametricBorder;->mBorderCornerRadius:I

    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/android/gallery3d/filtershow/filters/ImageFilter;->setFilterType(B)V

    const-string v0, "Border"

    iput-object v0, p0, Lcom/android/gallery3d/filtershow/filters/ImageFilter;->mName:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(III)V
    .locals 2
    .param p1    # I
    .param p2    # I
    .param p3    # I

    const/16 v1, 0xa

    invoke-direct {p0}, Lcom/android/gallery3d/filtershow/filters/ImageFilter;-><init>()V

    const/4 v0, -0x1

    iput v0, p0, Lcom/android/gallery3d/filtershow/filters/ImageFilterParametricBorder;->mBorderColor:I

    iput v1, p0, Lcom/android/gallery3d/filtershow/filters/ImageFilterParametricBorder;->mBorderSize:I

    iput v1, p0, Lcom/android/gallery3d/filtershow/filters/ImageFilterParametricBorder;->mBorderCornerRadius:I

    invoke-virtual {p0, p1, p2, p3}, Lcom/android/gallery3d/filtershow/filters/ImageFilterParametricBorder;->setBorder(III)V

    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/android/gallery3d/filtershow/filters/ImageFilter;->setFilterType(B)V

    return-void
.end method


# virtual methods
.method public apply(Landroid/graphics/Bitmap;FZ)Landroid/graphics/Bitmap;
    .locals 11
    .param p1    # Landroid/graphics/Bitmap;
    .param p2    # F
    .param p3    # Z

    const/high16 v10, 0x42c80000

    const/4 v9, 0x0

    new-instance v2, Landroid/graphics/Canvas;

    invoke-direct {v2, p1}, Landroid/graphics/Canvas;-><init>(Landroid/graphics/Bitmap;)V

    new-instance v0, Landroid/graphics/Path;

    invoke-direct {v0}, Landroid/graphics/Path;-><init>()V

    invoke-virtual {v0, v9, v9}, Landroid/graphics/Path;->moveTo(FF)V

    invoke-virtual {p1}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v6

    invoke-virtual {p1}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v3

    iget v7, p0, Lcom/android/gallery3d/filtershow/filters/ImageFilterParametricBorder;->mBorderSize:I

    int-to-float v7, v7

    div-float/2addr v7, v10

    invoke-virtual {p1}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v8

    int-to-float v8, v8

    mul-float v1, v7, v8

    iget v7, p0, Lcom/android/gallery3d/filtershow/filters/ImageFilterParametricBorder;->mBorderCornerRadius:I

    int-to-float v7, v7

    div-float/2addr v7, v10

    invoke-virtual {p1}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v8

    int-to-float v8, v8

    mul-float v5, v7, v8

    int-to-float v7, v3

    invoke-virtual {v0, v9, v7}, Landroid/graphics/Path;->lineTo(FF)V

    int-to-float v7, v6

    int-to-float v8, v3

    invoke-virtual {v0, v7, v8}, Landroid/graphics/Path;->lineTo(FF)V

    int-to-float v7, v6

    invoke-virtual {v0, v7, v9}, Landroid/graphics/Path;->lineTo(FF)V

    invoke-virtual {v0, v9, v9}, Landroid/graphics/Path;->lineTo(FF)V

    new-instance v7, Landroid/graphics/RectF;

    int-to-float v8, v6

    sub-float/2addr v8, v1

    int-to-float v9, v3

    sub-float/2addr v9, v1

    invoke-direct {v7, v1, v1, v8, v9}, Landroid/graphics/RectF;-><init>(FFFF)V

    sget-object v8, Landroid/graphics/Path$Direction;->CW:Landroid/graphics/Path$Direction;

    invoke-virtual {v0, v7, v5, v5, v8}, Landroid/graphics/Path;->addRoundRect(Landroid/graphics/RectF;FFLandroid/graphics/Path$Direction;)V

    new-instance v4, Landroid/graphics/Paint;

    invoke-direct {v4}, Landroid/graphics/Paint;-><init>()V

    const/4 v7, 0x1

    invoke-virtual {v4, v7}, Landroid/graphics/Paint;->setAntiAlias(Z)V

    iget v7, p0, Lcom/android/gallery3d/filtershow/filters/ImageFilterParametricBorder;->mBorderColor:I

    invoke-virtual {v4, v7}, Landroid/graphics/Paint;->setColor(I)V

    invoke-virtual {v2, v0, v4}, Landroid/graphics/Canvas;->drawPath(Landroid/graphics/Path;Landroid/graphics/Paint;)V

    return-object p1
.end method

.method public clone()Lcom/android/gallery3d/filtershow/filters/ImageFilter;
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/CloneNotSupportedException;
        }
    .end annotation

    invoke-super {p0}, Lcom/android/gallery3d/filtershow/filters/ImageFilter;->clone()Lcom/android/gallery3d/filtershow/filters/ImageFilter;

    move-result-object v0

    check-cast v0, Lcom/android/gallery3d/filtershow/filters/ImageFilterParametricBorder;

    iget v1, p0, Lcom/android/gallery3d/filtershow/filters/ImageFilterParametricBorder;->mBorderColor:I

    iget v2, p0, Lcom/android/gallery3d/filtershow/filters/ImageFilterParametricBorder;->mBorderSize:I

    iget v3, p0, Lcom/android/gallery3d/filtershow/filters/ImageFilterParametricBorder;->mBorderCornerRadius:I

    invoke-virtual {v0, v1, v2, v3}, Lcom/android/gallery3d/filtershow/filters/ImageFilterParametricBorder;->setBorder(III)V

    return-object v0
.end method

.method public bridge synthetic clone()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/CloneNotSupportedException;
        }
    .end annotation

    invoke-virtual {p0}, Lcom/android/gallery3d/filtershow/filters/ImageFilterParametricBorder;->clone()Lcom/android/gallery3d/filtershow/filters/ImageFilter;

    move-result-object v0

    return-object v0
.end method

.method public isNil()Z
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public same(Lcom/android/gallery3d/filtershow/filters/ImageFilter;)Z
    .locals 5
    .param p1    # Lcom/android/gallery3d/filtershow/filters/ImageFilter;

    const/4 v2, 0x0

    invoke-super {p0, p1}, Lcom/android/gallery3d/filtershow/filters/ImageFilter;->same(Lcom/android/gallery3d/filtershow/filters/ImageFilter;)Z

    move-result v1

    if-nez v1, :cond_1

    :cond_0
    :goto_0
    return v2

    :cond_1
    instance-of v3, p1, Lcom/android/gallery3d/filtershow/filters/ImageFilterParametricBorder;

    if-eqz v3, :cond_0

    move-object v0, p1

    check-cast v0, Lcom/android/gallery3d/filtershow/filters/ImageFilterParametricBorder;

    iget v3, v0, Lcom/android/gallery3d/filtershow/filters/ImageFilterParametricBorder;->mBorderColor:I

    iget v4, p0, Lcom/android/gallery3d/filtershow/filters/ImageFilterParametricBorder;->mBorderColor:I

    if-ne v3, v4, :cond_0

    iget v3, v0, Lcom/android/gallery3d/filtershow/filters/ImageFilterParametricBorder;->mBorderSize:I

    iget v4, p0, Lcom/android/gallery3d/filtershow/filters/ImageFilterParametricBorder;->mBorderSize:I

    if-ne v3, v4, :cond_0

    iget v3, v0, Lcom/android/gallery3d/filtershow/filters/ImageFilterParametricBorder;->mBorderCornerRadius:I

    iget v4, p0, Lcom/android/gallery3d/filtershow/filters/ImageFilterParametricBorder;->mBorderCornerRadius:I

    if-ne v3, v4, :cond_0

    const/4 v2, 0x1

    goto :goto_0
.end method

.method public setBorder(III)V
    .locals 0
    .param p1    # I
    .param p2    # I
    .param p3    # I

    iput p1, p0, Lcom/android/gallery3d/filtershow/filters/ImageFilterParametricBorder;->mBorderColor:I

    iput p2, p0, Lcom/android/gallery3d/filtershow/filters/ImageFilterParametricBorder;->mBorderSize:I

    iput p3, p0, Lcom/android/gallery3d/filtershow/filters/ImageFilterParametricBorder;->mBorderCornerRadius:I

    return-void
.end method
