.class public Lcom/android/gallery3d/filtershow/filters/ImageFilterBWGreen;
.super Lcom/android/gallery3d/filtershow/filters/ImageFilter;
.source "ImageFilterBWGreen.java"


# direct methods
.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Lcom/android/gallery3d/filtershow/filters/ImageFilter;-><init>()V

    const-string v0, "B&W - Green"

    iput-object v0, p0, Lcom/android/gallery3d/filtershow/filters/ImageFilter;->mName:Ljava/lang/String;

    return-void
.end method


# virtual methods
.method public apply(Landroid/graphics/Bitmap;FZ)Landroid/graphics/Bitmap;
    .locals 2
    .param p1    # Landroid/graphics/Bitmap;
    .param p2    # F
    .param p3    # Z

    invoke-virtual {p1}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v1

    invoke-virtual {p1}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v0

    invoke-virtual {p0, p1, v1, v0}, Lcom/android/gallery3d/filtershow/filters/ImageFilterBWGreen;->nativeApplyFilter(Landroid/graphics/Bitmap;II)V

    return-object p1
.end method

.method protected native nativeApplyFilter(Landroid/graphics/Bitmap;II)V
.end method
