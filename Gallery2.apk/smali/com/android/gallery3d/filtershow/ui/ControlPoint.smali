.class public Lcom/android/gallery3d/filtershow/ui/ControlPoint;
.super Ljava/lang/Object;
.source "ControlPoint.java"

# interfaces
.implements Ljava/lang/Comparable;


# instance fields
.field public x:F

.field public y:F


# direct methods
.method public constructor <init>(FF)V
    .locals 0
    .param p1    # F
    .param p2    # F

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput p1, p0, Lcom/android/gallery3d/filtershow/ui/ControlPoint;->x:F

    iput p2, p0, Lcom/android/gallery3d/filtershow/ui/ControlPoint;->y:F

    return-void
.end method

.method public constructor <init>(Lcom/android/gallery3d/filtershow/ui/ControlPoint;)V
    .locals 1
    .param p1    # Lcom/android/gallery3d/filtershow/ui/ControlPoint;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iget v0, p1, Lcom/android/gallery3d/filtershow/ui/ControlPoint;->x:F

    iput v0, p0, Lcom/android/gallery3d/filtershow/ui/ControlPoint;->x:F

    iget v0, p1, Lcom/android/gallery3d/filtershow/ui/ControlPoint;->y:F

    iput v0, p0, Lcom/android/gallery3d/filtershow/ui/ControlPoint;->y:F

    return-void
.end method


# virtual methods
.method public compareTo(Ljava/lang/Object;)I
    .locals 3
    .param p1    # Ljava/lang/Object;

    move-object v0, p1

    check-cast v0, Lcom/android/gallery3d/filtershow/ui/ControlPoint;

    iget v1, v0, Lcom/android/gallery3d/filtershow/ui/ControlPoint;->x:F

    iget v2, p0, Lcom/android/gallery3d/filtershow/ui/ControlPoint;->x:F

    cmpg-float v1, v1, v2

    if-gez v1, :cond_0

    const/4 v1, 0x1

    :goto_0
    return v1

    :cond_0
    iget v1, v0, Lcom/android/gallery3d/filtershow/ui/ControlPoint;->x:F

    iget v2, p0, Lcom/android/gallery3d/filtershow/ui/ControlPoint;->x:F

    cmpl-float v1, v1, v2

    if-lez v1, :cond_1

    const/4 v1, -0x1

    goto :goto_0

    :cond_1
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public copy()Lcom/android/gallery3d/filtershow/ui/ControlPoint;
    .locals 3

    new-instance v0, Lcom/android/gallery3d/filtershow/ui/ControlPoint;

    iget v1, p0, Lcom/android/gallery3d/filtershow/ui/ControlPoint;->x:F

    iget v2, p0, Lcom/android/gallery3d/filtershow/ui/ControlPoint;->y:F

    invoke-direct {v0, v1, v2}, Lcom/android/gallery3d/filtershow/ui/ControlPoint;-><init>(FF)V

    return-object v0
.end method
