.class public Lcom/android/gallery3d/data/EmptyAlbumImage;
.super Lcom/android/gallery3d/data/ActionImage;
.source "EmptyAlbumImage.java"


# static fields
.field private static final TAG:Ljava/lang/String; = "Gallery2/EmptyAlbumImage"


# direct methods
.method public constructor <init>(Lcom/android/gallery3d/data/Path;Lcom/android/gallery3d/app/GalleryApp;)V
    .locals 1
    .param p1    # Lcom/android/gallery3d/data/Path;
    .param p2    # Lcom/android/gallery3d/app/GalleryApp;

    const v0, 0x7f02016c

    invoke-direct {p0, p1, p2, v0}, Lcom/android/gallery3d/data/ActionImage;-><init>(Lcom/android/gallery3d/data/Path;Lcom/android/gallery3d/app/GalleryApp;I)V

    return-void
.end method


# virtual methods
.method public getSupportedOperations()I
    .locals 2

    invoke-super {p0}, Lcom/android/gallery3d/data/ActionImage;->getSupportedOperations()I

    move-result v0

    const/high16 v1, 0x400000

    or-int/2addr v0, v1

    return v0
.end method
