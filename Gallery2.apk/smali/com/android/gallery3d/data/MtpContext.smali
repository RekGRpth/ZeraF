.class public Lcom/android/gallery3d/data/MtpContext;
.super Ljava/lang/Object;
.source "MtpContext.java"

# interfaces
.implements Lcom/android/gallery3d/data/MtpClient$Listener;


# annotations
.annotation build Landroid/annotation/TargetApi;
    value = 0xc
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/gallery3d/data/MtpContext$ScannerClient;
    }
.end annotation


# static fields
.field private static final TAG:Ljava/lang/String; = "Gallery2/MtpContext"


# instance fields
.field private mClient:Lcom/android/gallery3d/data/MtpClient;

.field private mContext:Landroid/content/Context;

.field private mScannerClient:Lcom/android/gallery3d/data/MtpContext$ScannerClient;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 2
    .param p1    # Landroid/content/Context;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/android/gallery3d/data/MtpContext;->mContext:Landroid/content/Context;

    new-instance v0, Lcom/android/gallery3d/data/MtpContext$ScannerClient;

    invoke-direct {v0, p1}, Lcom/android/gallery3d/data/MtpContext$ScannerClient;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/android/gallery3d/data/MtpContext;->mScannerClient:Lcom/android/gallery3d/data/MtpContext$ScannerClient;

    new-instance v0, Lcom/android/gallery3d/data/MtpClient;

    iget-object v1, p0, Lcom/android/gallery3d/data/MtpContext;->mContext:Landroid/content/Context;

    invoke-direct {v0, v1}, Lcom/android/gallery3d/data/MtpClient;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/android/gallery3d/data/MtpContext;->mClient:Lcom/android/gallery3d/data/MtpClient;

    return-void
.end method

.method private notifyDirty()V
    .locals 3

    iget-object v0, p0, Lcom/android/gallery3d/data/MtpContext;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const-string v1, "mtp://"

    invoke-static {v1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentResolver;->notifyChange(Landroid/net/Uri;Landroid/database/ContentObserver;)V

    return-void
.end method

.method private showToast(I)V
    .locals 2
    .param p1    # I

    iget-object v0, p0, Lcom/android/gallery3d/data/MtpContext;->mContext:Landroid/content/Context;

    const/4 v1, 0x0

    invoke-static {v0, p1, v1}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    return-void
.end method


# virtual methods
.method public copyAlbum(Ljava/lang/String;Ljava/lang/String;Ljava/util/List;)Z
    .locals 9
    .param p1    # Ljava/lang/String;
    .param p2    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/util/List",
            "<",
            "Landroid/mtp/MtpObjectInfo;",
            ">;)Z"
        }
    .end annotation

    invoke-static {}, Landroid/os/Environment;->getExternalStorageDirectory()Ljava/io/File;

    move-result-object v1

    new-instance v2, Ljava/io/File;

    invoke-direct {v2, v1, p2}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    invoke-virtual {v2}, Ljava/io/File;->mkdirs()Z

    const/4 v6, 0x0

    invoke-interface {p3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_0
    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v7

    if-eqz v7, :cond_1

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/mtp/MtpObjectInfo;

    invoke-virtual {v0}, Landroid/mtp/MtpObjectInfo;->getCompressedSize()I

    move-result v7

    int-to-long v7, v7

    invoke-static {v7, v8}, Lcom/android/gallery3d/util/GalleryUtils;->hasSpaceForSize(J)Z

    move-result v7

    if-eqz v7, :cond_0

    new-instance v4, Ljava/io/File;

    invoke-virtual {v0}, Landroid/mtp/MtpObjectInfo;->getName()Ljava/lang/String;

    move-result-object v7

    invoke-direct {v4, v2, v7}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    invoke-virtual {v4}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v5

    iget-object v7, p0, Lcom/android/gallery3d/data/MtpContext;->mClient:Lcom/android/gallery3d/data/MtpClient;

    invoke-virtual {v0}, Landroid/mtp/MtpObjectInfo;->getObjectHandle()I

    move-result v8

    invoke-virtual {v7, p1, v8, v5}, Lcom/android/gallery3d/data/MtpClient;->importFile(Ljava/lang/String;ILjava/lang/String;)Z

    move-result v7

    if-eqz v7, :cond_0

    iget-object v7, p0, Lcom/android/gallery3d/data/MtpContext;->mScannerClient:Lcom/android/gallery3d/data/MtpContext$ScannerClient;

    invoke-virtual {v7, v5}, Lcom/android/gallery3d/data/MtpContext$ScannerClient;->scanPath(Ljava/lang/String;)V

    add-int/lit8 v6, v6, 0x1

    goto :goto_0

    :cond_1
    invoke-interface {p3}, Ljava/util/List;->size()I

    move-result v7

    if-ne v6, v7, :cond_2

    const/4 v7, 0x1

    :goto_1
    return v7

    :cond_2
    const/4 v7, 0x0

    goto :goto_1
.end method

.method public copyFile(Ljava/lang/String;Landroid/mtp/MtpObjectInfo;)Z
    .locals 7
    .param p1    # Ljava/lang/String;
    .param p2    # Landroid/mtp/MtpObjectInfo;

    invoke-virtual {p2}, Landroid/mtp/MtpObjectInfo;->getCompressedSize()I

    move-result v4

    int-to-long v4, v4

    invoke-static {v4, v5}, Lcom/android/gallery3d/util/GalleryUtils;->hasSpaceForSize(J)Z

    move-result v4

    if-eqz v4, :cond_0

    invoke-static {}, Landroid/os/Environment;->getExternalStorageDirectory()Ljava/io/File;

    move-result-object v0

    new-instance v1, Ljava/io/File;

    const-string v4, "Imported"

    invoke-direct {v1, v0, v4}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    invoke-virtual {v1}, Ljava/io/File;->mkdirs()Z

    new-instance v4, Ljava/io/File;

    invoke-virtual {p2}, Landroid/mtp/MtpObjectInfo;->getName()Ljava/lang/String;

    move-result-object v5

    invoke-direct {v4, v1, v5}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    invoke-virtual {v4}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p2}, Landroid/mtp/MtpObjectInfo;->getObjectHandle()I

    move-result v3

    iget-object v4, p0, Lcom/android/gallery3d/data/MtpContext;->mClient:Lcom/android/gallery3d/data/MtpClient;

    invoke-virtual {v4, p1, v3, v2}, Lcom/android/gallery3d/data/MtpClient;->importFile(Ljava/lang/String;ILjava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_1

    iget-object v4, p0, Lcom/android/gallery3d/data/MtpContext;->mScannerClient:Lcom/android/gallery3d/data/MtpContext$ScannerClient;

    invoke-virtual {v4, v2}, Lcom/android/gallery3d/data/MtpContext$ScannerClient;->scanPath(Ljava/lang/String;)V

    const/4 v4, 0x1

    :goto_0
    return v4

    :cond_0
    const-string v4, "Gallery2/MtpContext"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "No space to import "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {p2}, Landroid/mtp/MtpObjectInfo;->getName()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, " whose size = "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {p2}, Landroid/mtp/MtpObjectInfo;->getCompressedSize()I

    move-result v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    :cond_1
    const/4 v4, 0x0

    goto :goto_0
.end method

.method public deviceAdded(Landroid/mtp/MtpDevice;)V
    .locals 1
    .param p1    # Landroid/mtp/MtpDevice;

    invoke-direct {p0}, Lcom/android/gallery3d/data/MtpContext;->notifyDirty()V

    const v0, 0x7f0c02b0

    invoke-direct {p0, v0}, Lcom/android/gallery3d/data/MtpContext;->showToast(I)V

    return-void
.end method

.method public deviceRemoved(Landroid/mtp/MtpDevice;)V
    .locals 1
    .param p1    # Landroid/mtp/MtpDevice;

    invoke-direct {p0}, Lcom/android/gallery3d/data/MtpContext;->notifyDirty()V

    const v0, 0x7f0c02b1

    invoke-direct {p0, v0}, Lcom/android/gallery3d/data/MtpContext;->showToast(I)V

    return-void
.end method

.method public getMtpClient()Lcom/android/gallery3d/data/MtpClient;
    .locals 1

    iget-object v0, p0, Lcom/android/gallery3d/data/MtpContext;->mClient:Lcom/android/gallery3d/data/MtpClient;

    return-object v0
.end method

.method public pause()V
    .locals 1

    iget-object v0, p0, Lcom/android/gallery3d/data/MtpContext;->mClient:Lcom/android/gallery3d/data/MtpClient;

    invoke-virtual {v0, p0}, Lcom/android/gallery3d/data/MtpClient;->removeListener(Lcom/android/gallery3d/data/MtpClient$Listener;)V

    return-void
.end method

.method public resume()V
    .locals 1

    iget-object v0, p0, Lcom/android/gallery3d/data/MtpContext;->mClient:Lcom/android/gallery3d/data/MtpClient;

    invoke-virtual {v0, p0}, Lcom/android/gallery3d/data/MtpClient;->addListener(Lcom/android/gallery3d/data/MtpClient$Listener;)V

    invoke-direct {p0}, Lcom/android/gallery3d/data/MtpContext;->notifyDirty()V

    return-void
.end method
