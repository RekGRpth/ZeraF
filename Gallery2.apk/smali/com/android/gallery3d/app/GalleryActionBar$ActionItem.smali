.class Lcom/android/gallery3d/app/GalleryActionBar$ActionItem;
.super Ljava/lang/Object;
.source "GalleryActionBar.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/gallery3d/app/GalleryActionBar;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "ActionItem"
.end annotation


# instance fields
.field public action:I

.field public clusterBy:I

.field public dialogTitle:I

.field public enabled:Z

.field public spinnerTitle:I

.field public visible:Z


# direct methods
.method public constructor <init>(IZZII)V
    .locals 7
    .param p1    # I
    .param p2    # Z
    .param p3    # Z
    .param p4    # I
    .param p5    # I

    move-object v0, p0

    move v1, p1

    move v2, p2

    move v3, p3

    move v4, p4

    move v5, p4

    move v6, p5

    invoke-direct/range {v0 .. v6}, Lcom/android/gallery3d/app/GalleryActionBar$ActionItem;-><init>(IZZIII)V

    return-void
.end method

.method public constructor <init>(IZZIII)V
    .locals 1
    .param p1    # I
    .param p2    # Z
    .param p3    # Z
    .param p4    # I
    .param p5    # I
    .param p6    # I

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput p1, p0, Lcom/android/gallery3d/app/GalleryActionBar$ActionItem;->action:I

    iput-boolean p3, p0, Lcom/android/gallery3d/app/GalleryActionBar$ActionItem;->enabled:Z

    iput p4, p0, Lcom/android/gallery3d/app/GalleryActionBar$ActionItem;->spinnerTitle:I

    iput p5, p0, Lcom/android/gallery3d/app/GalleryActionBar$ActionItem;->dialogTitle:I

    iput p6, p0, Lcom/android/gallery3d/app/GalleryActionBar$ActionItem;->clusterBy:I

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/android/gallery3d/app/GalleryActionBar$ActionItem;->visible:Z

    return-void
.end method
