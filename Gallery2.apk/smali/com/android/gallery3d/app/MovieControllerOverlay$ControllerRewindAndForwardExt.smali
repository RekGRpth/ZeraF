.class Lcom/android/gallery3d/app/MovieControllerOverlay$ControllerRewindAndForwardExt;
.super Ljava/lang/Object;
.source "MovieControllerOverlay.java"

# interfaces
.implements Landroid/view/View$OnClickListener;
.implements Lcom/mediatek/gallery3d/video/IControllerRewindAndForward;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/gallery3d/app/MovieControllerOverlay;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = "ControllerRewindAndForwardExt"
.end annotation


# static fields
.field private static final BUTTON_PADDING:I = 0x28


# instance fields
.field private mButtonWidth:I

.field private mContollerButtons:Landroid/widget/LinearLayout;

.field private mForward:Landroid/widget/ImageView;

.field private mListenerForRewind:Lcom/mediatek/gallery3d/video/IControllerRewindAndForward$IRewindAndForwardListener;

.field private mRewind:Landroid/widget/ImageView;

.field private mStop:Landroid/widget/ImageView;

.field private mTimeBarHeight:I

.field final synthetic this$0:Lcom/android/gallery3d/app/MovieControllerOverlay;


# direct methods
.method constructor <init>(Lcom/android/gallery3d/app/MovieControllerOverlay;)V
    .locals 1

    iput-object p1, p0, Lcom/android/gallery3d/app/MovieControllerOverlay$ControllerRewindAndForwardExt;->this$0:Lcom/android/gallery3d/app/MovieControllerOverlay;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x0

    iput v0, p0, Lcom/android/gallery3d/app/MovieControllerOverlay$ControllerRewindAndForwardExt;->mTimeBarHeight:I

    return-void
.end method


# virtual methods
.method public getAddedRightPadding()I
    .locals 1

    iget v0, p0, Lcom/android/gallery3d/app/MovieControllerOverlay$ControllerRewindAndForwardExt;->mTimeBarHeight:I

    mul-int/lit8 v0, v0, 0x3

    add-int/lit8 v0, v0, 0x50

    return v0
.end method

.method public getPlayPauseEanbled()Z
    .locals 1

    iget-object v0, p0, Lcom/android/gallery3d/app/MovieControllerOverlay$ControllerRewindAndForwardExt;->this$0:Lcom/android/gallery3d/app/MovieControllerOverlay;

    iget-object v0, v0, Lcom/android/gallery3d/app/CommonControllerOverlay;->mPlayPauseReplayView:Landroid/widget/ImageView;

    invoke-virtual {v0}, Landroid/view/View;->isEnabled()Z

    move-result v0

    return v0
.end method

.method public getTimeBarEanbled()Z
    .locals 1

    iget-object v0, p0, Lcom/android/gallery3d/app/MovieControllerOverlay$ControllerRewindAndForwardExt;->this$0:Lcom/android/gallery3d/app/MovieControllerOverlay;

    iget-object v0, v0, Lcom/android/gallery3d/app/CommonControllerOverlay;->mTimeBar:Lcom/android/gallery3d/app/TimeBar;

    invoke-virtual {v0}, Lcom/android/gallery3d/app/TimeBar;->getScrubbing()Z

    move-result v0

    return v0
.end method

.method public getView()Landroid/view/View;
    .locals 1

    iget-object v0, p0, Lcom/android/gallery3d/app/MovieControllerOverlay$ControllerRewindAndForwardExt;->mContollerButtons:Landroid/widget/LinearLayout;

    return-object v0
.end method

.method init(Landroid/content/Context;)V
    .locals 9
    .param p1    # Landroid/content/Context;

    const/16 v8, 0x28

    const/4 v7, 0x0

    const/4 v6, 0x1

    const-string v4, "Gallery2/MovieControllerOverlay"

    const-string v5, "ControllerRewindAndForwardExt init"

    invoke-static {v4, v5}, Lcom/mediatek/gallery3d/util/MtkLog;->v(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v4, p0, Lcom/android/gallery3d/app/MovieControllerOverlay$ControllerRewindAndForwardExt;->this$0:Lcom/android/gallery3d/app/MovieControllerOverlay;

    iget-object v4, v4, Lcom/android/gallery3d/app/CommonControllerOverlay;->mTimeBar:Lcom/android/gallery3d/app/TimeBar;

    invoke-virtual {v4}, Lcom/android/gallery3d/app/TimeBar;->getPreferredHeight()I

    move-result v4

    iput v4, p0, Lcom/android/gallery3d/app/MovieControllerOverlay$ControllerRewindAndForwardExt;->mTimeBarHeight:I

    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v5, 0x7f0200c3

    invoke-static {v4, v5}, Landroid/graphics/BitmapFactory;->decodeResource(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;

    move-result-object v0

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v4

    iput v4, p0, Lcom/android/gallery3d/app/MovieControllerOverlay$ControllerRewindAndForwardExt;->mButtonWidth:I

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->recycle()V

    new-instance v4, Landroid/widget/LinearLayout;

    invoke-direct {v4, p1}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;)V

    iput-object v4, p0, Lcom/android/gallery3d/app/MovieControllerOverlay$ControllerRewindAndForwardExt;->mContollerButtons:Landroid/widget/LinearLayout;

    new-instance v3, Landroid/widget/LinearLayout$LayoutParams;

    invoke-virtual {p0}, Lcom/android/gallery3d/app/MovieControllerOverlay$ControllerRewindAndForwardExt;->getAddedRightPadding()I

    move-result v4

    iget v5, p0, Lcom/android/gallery3d/app/MovieControllerOverlay$ControllerRewindAndForwardExt;->mTimeBarHeight:I

    invoke-direct {v3, v4, v5}, Landroid/widget/LinearLayout$LayoutParams;-><init>(II)V

    iget-object v4, p0, Lcom/android/gallery3d/app/MovieControllerOverlay$ControllerRewindAndForwardExt;->mContollerButtons:Landroid/widget/LinearLayout;

    invoke-virtual {v4, v7}, Landroid/widget/LinearLayout;->setHorizontalGravity(I)V

    iget-object v4, p0, Lcom/android/gallery3d/app/MovieControllerOverlay$ControllerRewindAndForwardExt;->mContollerButtons:Landroid/widget/LinearLayout;

    invoke-virtual {v4, v7}, Landroid/view/View;->setVisibility(I)V

    iget-object v4, p0, Lcom/android/gallery3d/app/MovieControllerOverlay$ControllerRewindAndForwardExt;->mContollerButtons:Landroid/widget/LinearLayout;

    const/16 v5, 0x11

    invoke-virtual {v4, v5}, Landroid/widget/LinearLayout;->setGravity(I)V

    new-instance v1, Landroid/widget/LinearLayout$LayoutParams;

    iget v4, p0, Lcom/android/gallery3d/app/MovieControllerOverlay$ControllerRewindAndForwardExt;->mTimeBarHeight:I

    iget v5, p0, Lcom/android/gallery3d/app/MovieControllerOverlay$ControllerRewindAndForwardExt;->mTimeBarHeight:I

    invoke-direct {v1, v4, v5}, Landroid/widget/LinearLayout$LayoutParams;-><init>(II)V

    new-instance v4, Landroid/widget/ImageView;

    invoke-direct {v4, p1}, Landroid/widget/ImageView;-><init>(Landroid/content/Context;)V

    iput-object v4, p0, Lcom/android/gallery3d/app/MovieControllerOverlay$ControllerRewindAndForwardExt;->mRewind:Landroid/widget/ImageView;

    iget-object v4, p0, Lcom/android/gallery3d/app/MovieControllerOverlay$ControllerRewindAndForwardExt;->mRewind:Landroid/widget/ImageView;

    const v5, 0x7f020131

    invoke-virtual {v4, v5}, Landroid/widget/ImageView;->setImageResource(I)V

    iget-object v4, p0, Lcom/android/gallery3d/app/MovieControllerOverlay$ControllerRewindAndForwardExt;->mRewind:Landroid/widget/ImageView;

    sget-object v5, Landroid/widget/ImageView$ScaleType;->CENTER:Landroid/widget/ImageView$ScaleType;

    invoke-virtual {v4, v5}, Landroid/widget/ImageView;->setScaleType(Landroid/widget/ImageView$ScaleType;)V

    iget-object v4, p0, Lcom/android/gallery3d/app/MovieControllerOverlay$ControllerRewindAndForwardExt;->mRewind:Landroid/widget/ImageView;

    invoke-virtual {v4, v6}, Landroid/view/View;->setFocusable(Z)V

    iget-object v4, p0, Lcom/android/gallery3d/app/MovieControllerOverlay$ControllerRewindAndForwardExt;->mRewind:Landroid/widget/ImageView;

    invoke-virtual {v4, v6}, Landroid/view/View;->setClickable(Z)V

    iget-object v4, p0, Lcom/android/gallery3d/app/MovieControllerOverlay$ControllerRewindAndForwardExt;->mRewind:Landroid/widget/ImageView;

    invoke-virtual {v4, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v4, p0, Lcom/android/gallery3d/app/MovieControllerOverlay$ControllerRewindAndForwardExt;->mContollerButtons:Landroid/widget/LinearLayout;

    iget-object v5, p0, Lcom/android/gallery3d/app/MovieControllerOverlay$ControllerRewindAndForwardExt;->mRewind:Landroid/widget/ImageView;

    invoke-virtual {v4, v5, v1}, Landroid/view/ViewGroup;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    new-instance v4, Landroid/widget/ImageView;

    invoke-direct {v4, p1}, Landroid/widget/ImageView;-><init>(Landroid/content/Context;)V

    iput-object v4, p0, Lcom/android/gallery3d/app/MovieControllerOverlay$ControllerRewindAndForwardExt;->mStop:Landroid/widget/ImageView;

    iget-object v4, p0, Lcom/android/gallery3d/app/MovieControllerOverlay$ControllerRewindAndForwardExt;->mStop:Landroid/widget/ImageView;

    const v5, 0x7f020132

    invoke-virtual {v4, v5}, Landroid/widget/ImageView;->setImageResource(I)V

    iget-object v4, p0, Lcom/android/gallery3d/app/MovieControllerOverlay$ControllerRewindAndForwardExt;->mStop:Landroid/widget/ImageView;

    sget-object v5, Landroid/widget/ImageView$ScaleType;->CENTER:Landroid/widget/ImageView$ScaleType;

    invoke-virtual {v4, v5}, Landroid/widget/ImageView;->setScaleType(Landroid/widget/ImageView$ScaleType;)V

    iget-object v4, p0, Lcom/android/gallery3d/app/MovieControllerOverlay$ControllerRewindAndForwardExt;->mStop:Landroid/widget/ImageView;

    invoke-virtual {v4, v6}, Landroid/view/View;->setFocusable(Z)V

    iget-object v4, p0, Lcom/android/gallery3d/app/MovieControllerOverlay$ControllerRewindAndForwardExt;->mStop:Landroid/widget/ImageView;

    invoke-virtual {v4, v6}, Landroid/view/View;->setClickable(Z)V

    iget-object v4, p0, Lcom/android/gallery3d/app/MovieControllerOverlay$ControllerRewindAndForwardExt;->mStop:Landroid/widget/ImageView;

    invoke-virtual {v4, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    new-instance v2, Landroid/widget/LinearLayout$LayoutParams;

    iget v4, p0, Lcom/android/gallery3d/app/MovieControllerOverlay$ControllerRewindAndForwardExt;->mTimeBarHeight:I

    iget v5, p0, Lcom/android/gallery3d/app/MovieControllerOverlay$ControllerRewindAndForwardExt;->mTimeBarHeight:I

    invoke-direct {v2, v4, v5}, Landroid/widget/LinearLayout$LayoutParams;-><init>(II)V

    invoke-virtual {v2, v8, v7, v8, v7}, Landroid/view/ViewGroup$MarginLayoutParams;->setMargins(IIII)V

    iget-object v4, p0, Lcom/android/gallery3d/app/MovieControllerOverlay$ControllerRewindAndForwardExt;->mContollerButtons:Landroid/widget/LinearLayout;

    iget-object v5, p0, Lcom/android/gallery3d/app/MovieControllerOverlay$ControllerRewindAndForwardExt;->mStop:Landroid/widget/ImageView;

    invoke-virtual {v4, v5, v2}, Landroid/view/ViewGroup;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    new-instance v4, Landroid/widget/ImageView;

    invoke-direct {v4, p1}, Landroid/widget/ImageView;-><init>(Landroid/content/Context;)V

    iput-object v4, p0, Lcom/android/gallery3d/app/MovieControllerOverlay$ControllerRewindAndForwardExt;->mForward:Landroid/widget/ImageView;

    iget-object v4, p0, Lcom/android/gallery3d/app/MovieControllerOverlay$ControllerRewindAndForwardExt;->mForward:Landroid/widget/ImageView;

    const v5, 0x7f020128

    invoke-virtual {v4, v5}, Landroid/widget/ImageView;->setImageResource(I)V

    iget-object v4, p0, Lcom/android/gallery3d/app/MovieControllerOverlay$ControllerRewindAndForwardExt;->mForward:Landroid/widget/ImageView;

    sget-object v5, Landroid/widget/ImageView$ScaleType;->CENTER:Landroid/widget/ImageView$ScaleType;

    invoke-virtual {v4, v5}, Landroid/widget/ImageView;->setScaleType(Landroid/widget/ImageView$ScaleType;)V

    iget-object v4, p0, Lcom/android/gallery3d/app/MovieControllerOverlay$ControllerRewindAndForwardExt;->mForward:Landroid/widget/ImageView;

    invoke-virtual {v4, v6}, Landroid/view/View;->setFocusable(Z)V

    iget-object v4, p0, Lcom/android/gallery3d/app/MovieControllerOverlay$ControllerRewindAndForwardExt;->mForward:Landroid/widget/ImageView;

    invoke-virtual {v4, v6}, Landroid/view/View;->setClickable(Z)V

    iget-object v4, p0, Lcom/android/gallery3d/app/MovieControllerOverlay$ControllerRewindAndForwardExt;->mForward:Landroid/widget/ImageView;

    invoke-virtual {v4, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v4, p0, Lcom/android/gallery3d/app/MovieControllerOverlay$ControllerRewindAndForwardExt;->mContollerButtons:Landroid/widget/LinearLayout;

    iget-object v5, p0, Lcom/android/gallery3d/app/MovieControllerOverlay$ControllerRewindAndForwardExt;->mForward:Landroid/widget/ImageView;

    invoke-virtual {v4, v5, v1}, Landroid/view/ViewGroup;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    iget-object v4, p0, Lcom/android/gallery3d/app/MovieControllerOverlay$ControllerRewindAndForwardExt;->this$0:Lcom/android/gallery3d/app/MovieControllerOverlay;

    iget-object v5, p0, Lcom/android/gallery3d/app/MovieControllerOverlay$ControllerRewindAndForwardExt;->mContollerButtons:Landroid/widget/LinearLayout;

    invoke-virtual {v4, v5, v3}, Landroid/view/ViewGroup;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    return-void
.end method

.method public onCancelHiding()V
    .locals 2

    const-string v0, "Gallery2/MovieControllerOverlay"

    const-string v1, "ControllerRewindAndForwardExt onCancelHiding"

    invoke-static {v0, v1}, Lcom/mediatek/gallery3d/util/MtkLog;->v(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v0, p0, Lcom/android/gallery3d/app/MovieControllerOverlay$ControllerRewindAndForwardExt;->mContollerButtons:Landroid/widget/LinearLayout;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setAnimation(Landroid/view/animation/Animation;)V

    return-void
.end method

.method public onClick(Landroid/view/View;)V
    .locals 2
    .param p1    # Landroid/view/View;

    iget-object v0, p0, Lcom/android/gallery3d/app/MovieControllerOverlay$ControllerRewindAndForwardExt;->mStop:Landroid/widget/ImageView;

    if-ne p1, v0, :cond_1

    const-string v0, "Gallery2/MovieControllerOverlay"

    const-string v1, "ControllerRewindAndForwardExt onClick mStop"

    invoke-static {v0, v1}, Lcom/mediatek/gallery3d/util/MtkLog;->v(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v0, p0, Lcom/android/gallery3d/app/MovieControllerOverlay$ControllerRewindAndForwardExt;->mListenerForRewind:Lcom/mediatek/gallery3d/video/IControllerRewindAndForward$IRewindAndForwardListener;

    invoke-interface {v0}, Lcom/mediatek/gallery3d/video/IControllerRewindAndForward$IRewindAndForwardListener;->onStopVideo()V

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v0, p0, Lcom/android/gallery3d/app/MovieControllerOverlay$ControllerRewindAndForwardExt;->mRewind:Landroid/widget/ImageView;

    if-ne p1, v0, :cond_2

    const-string v0, "Gallery2/MovieControllerOverlay"

    const-string v1, "ControllerRewindAndForwardExt onClick mRewind"

    invoke-static {v0, v1}, Lcom/mediatek/gallery3d/util/MtkLog;->v(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v0, p0, Lcom/android/gallery3d/app/MovieControllerOverlay$ControllerRewindAndForwardExt;->mListenerForRewind:Lcom/mediatek/gallery3d/video/IControllerRewindAndForward$IRewindAndForwardListener;

    invoke-interface {v0}, Lcom/mediatek/gallery3d/video/IControllerRewindAndForward$IRewindAndForwardListener;->onRewind()V

    goto :goto_0

    :cond_2
    iget-object v0, p0, Lcom/android/gallery3d/app/MovieControllerOverlay$ControllerRewindAndForwardExt;->mForward:Landroid/widget/ImageView;

    if-ne p1, v0, :cond_0

    const-string v0, "Gallery2/MovieControllerOverlay"

    const-string v1, "ControllerRewindAndForwardExt onClick mForward"

    invoke-static {v0, v1}, Lcom/mediatek/gallery3d/util/MtkLog;->v(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v0, p0, Lcom/android/gallery3d/app/MovieControllerOverlay$ControllerRewindAndForwardExt;->mListenerForRewind:Lcom/mediatek/gallery3d/video/IControllerRewindAndForward$IRewindAndForwardListener;

    invoke-interface {v0}, Lcom/mediatek/gallery3d/video/IControllerRewindAndForward$IRewindAndForwardListener;->onForward()V

    goto :goto_0
.end method

.method public onHide()V
    .locals 2

    const-string v0, "Gallery2/MovieControllerOverlay"

    const-string v1, "ControllerRewindAndForwardExt onHide"

    invoke-static {v0, v1}, Lcom/mediatek/gallery3d/util/MtkLog;->v(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v0, p0, Lcom/android/gallery3d/app/MovieControllerOverlay$ControllerRewindAndForwardExt;->mContollerButtons:Landroid/widget/LinearLayout;

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    return-void
.end method

.method public onLayout(IIII)V
    .locals 6
    .param p1    # I
    .param p2    # I
    .param p3    # I
    .param p4    # I

    const-string v2, "Gallery2/MovieControllerOverlay"

    const-string v3, "ControllerRewindAndForwardExt onLayout"

    invoke-static {v2, v3}, Lcom/mediatek/gallery3d/util/MtkLog;->v(Ljava/lang/String;Ljava/lang/String;)I

    sub-int v2, p2, p1

    invoke-virtual {p0}, Lcom/android/gallery3d/app/MovieControllerOverlay$ControllerRewindAndForwardExt;->getAddedRightPadding()I

    move-result v3

    sub-int/2addr v2, v3

    div-int/lit8 v0, v2, 0x2

    invoke-virtual {p0}, Lcom/android/gallery3d/app/MovieControllerOverlay$ControllerRewindAndForwardExt;->getAddedRightPadding()I

    move-result v2

    add-int v1, v0, v2

    iget-object v2, p0, Lcom/android/gallery3d/app/MovieControllerOverlay$ControllerRewindAndForwardExt;->this$0:Lcom/android/gallery3d/app/MovieControllerOverlay;

    add-int v3, v1, p4

    invoke-static {v2, v3}, Lcom/android/gallery3d/app/MovieControllerOverlay;->access$702(Lcom/android/gallery3d/app/MovieControllerOverlay;I)I

    iget-object v2, p0, Lcom/android/gallery3d/app/MovieControllerOverlay$ControllerRewindAndForwardExt;->mContollerButtons:Landroid/widget/LinearLayout;

    add-int v3, v0, p4

    iget-object v4, p0, Lcom/android/gallery3d/app/MovieControllerOverlay$ControllerRewindAndForwardExt;->this$0:Lcom/android/gallery3d/app/MovieControllerOverlay;

    iget-object v4, v4, Lcom/android/gallery3d/app/CommonControllerOverlay;->mTimeBar:Lcom/android/gallery3d/app/TimeBar;

    invoke-virtual {v4}, Lcom/android/gallery3d/app/TimeBar;->getBarHeight()I

    move-result v4

    sub-int v4, p3, v4

    add-int v5, v1, p4

    invoke-virtual {v2, v3, v4, v5, p3}, Landroid/view/ViewGroup;->layout(IIII)V

    return-void
.end method

.method public onShow()V
    .locals 2

    const-string v0, "Gallery2/MovieControllerOverlay"

    const-string v1, "ControllerRewindAndForwardExt onShow"

    invoke-static {v0, v1}, Lcom/mediatek/gallery3d/util/MtkLog;->v(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v0, p0, Lcom/android/gallery3d/app/MovieControllerOverlay$ControllerRewindAndForwardExt;->mContollerButtons:Landroid/widget/LinearLayout;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    return-void
.end method

.method public onStartHiding()V
    .locals 2

    const-string v0, "Gallery2/MovieControllerOverlay"

    const-string v1, "ControllerRewindAndForwardExt onStartHiding"

    invoke-static {v0, v1}, Lcom/mediatek/gallery3d/util/MtkLog;->v(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v0, p0, Lcom/android/gallery3d/app/MovieControllerOverlay$ControllerRewindAndForwardExt;->this$0:Lcom/android/gallery3d/app/MovieControllerOverlay;

    iget-object v1, p0, Lcom/android/gallery3d/app/MovieControllerOverlay$ControllerRewindAndForwardExt;->mContollerButtons:Landroid/widget/LinearLayout;

    invoke-static {v0, v1}, Lcom/android/gallery3d/app/MovieControllerOverlay;->access$600(Lcom/android/gallery3d/app/MovieControllerOverlay;Landroid/view/View;)V

    return-void
.end method

.method public setCanReplay(Z)V
    .locals 0
    .param p1    # Z

    invoke-virtual {p0, p1}, Lcom/android/gallery3d/app/MovieControllerOverlay$ControllerRewindAndForwardExt;->setCanReplay(Z)V

    return-void
.end method

.method public setIListener(Lcom/mediatek/gallery3d/video/IControllerRewindAndForward$IRewindAndForwardListener;)V
    .locals 3
    .param p1    # Lcom/mediatek/gallery3d/video/IControllerRewindAndForward$IRewindAndForwardListener;

    const-string v0, "Gallery2/MovieControllerOverlay"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "ControllerRewindAndForwardExt setIListener "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/mediatek/gallery3d/util/MtkLog;->v(Ljava/lang/String;Ljava/lang/String;)I

    iput-object p1, p0, Lcom/android/gallery3d/app/MovieControllerOverlay$ControllerRewindAndForwardExt;->mListenerForRewind:Lcom/mediatek/gallery3d/video/IControllerRewindAndForward$IRewindAndForwardListener;

    return-void
.end method

.method public setListener(Lcom/android/gallery3d/app/ControllerOverlay$Listener;)V
    .locals 0
    .param p1    # Lcom/android/gallery3d/app/ControllerOverlay$Listener;

    invoke-virtual {p0, p1}, Lcom/android/gallery3d/app/MovieControllerOverlay$ControllerRewindAndForwardExt;->setListener(Lcom/android/gallery3d/app/ControllerOverlay$Listener;)V

    return-void
.end method

.method public setPlayPauseReplayResume()V
    .locals 0

    return-void
.end method

.method public setTimes(IIII)V
    .locals 1
    .param p1    # I
    .param p2    # I
    .param p3    # I
    .param p4    # I

    const/4 v0, 0x0

    invoke-virtual {p0, p1, p2, v0, v0}, Lcom/android/gallery3d/app/MovieControllerOverlay$ControllerRewindAndForwardExt;->setTimes(IIII)V

    return-void
.end method

.method public setViewEnabled(Z)V
    .locals 3
    .param p1    # Z

    const-string v0, "Gallery2/MovieControllerOverlay"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "ControllerRewindAndForwardExt setViewEnabled is "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/mediatek/gallery3d/util/MtkLog;->v(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v0, p0, Lcom/android/gallery3d/app/MovieControllerOverlay$ControllerRewindAndForwardExt;->mRewind:Landroid/widget/ImageView;

    invoke-virtual {v0, p1}, Landroid/view/View;->setEnabled(Z)V

    iget-object v0, p0, Lcom/android/gallery3d/app/MovieControllerOverlay$ControllerRewindAndForwardExt;->mForward:Landroid/widget/ImageView;

    invoke-virtual {v0, p1}, Landroid/view/View;->setEnabled(Z)V

    return-void
.end method

.method public show()V
    .locals 0

    invoke-virtual {p0}, Lcom/android/gallery3d/app/MovieControllerOverlay$ControllerRewindAndForwardExt;->show()V

    return-void
.end method

.method public showControllerButtonsView(ZZZ)V
    .locals 3
    .param p1    # Z
    .param p2    # Z
    .param p3    # Z

    const-string v0, "Gallery2/MovieControllerOverlay"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "ControllerRewindAndForwardExt showControllerButtonsView "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/mediatek/gallery3d/util/MtkLog;->v(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v0, p0, Lcom/android/gallery3d/app/MovieControllerOverlay$ControllerRewindAndForwardExt;->mStop:Landroid/widget/ImageView;

    invoke-virtual {v0, p1}, Landroid/view/View;->setEnabled(Z)V

    iget-object v0, p0, Lcom/android/gallery3d/app/MovieControllerOverlay$ControllerRewindAndForwardExt;->mRewind:Landroid/widget/ImageView;

    invoke-virtual {v0, p2}, Landroid/view/View;->setEnabled(Z)V

    iget-object v0, p0, Lcom/android/gallery3d/app/MovieControllerOverlay$ControllerRewindAndForwardExt;->mForward:Landroid/widget/ImageView;

    invoke-virtual {v0, p3}, Landroid/view/View;->setEnabled(Z)V

    return-void
.end method

.method public showEnded()V
    .locals 0

    invoke-virtual {p0}, Lcom/android/gallery3d/app/MovieControllerOverlay$ControllerRewindAndForwardExt;->showEnded()V

    return-void
.end method

.method public showErrorMessage(Ljava/lang/String;)V
    .locals 0
    .param p1    # Ljava/lang/String;

    invoke-virtual {p0, p1}, Lcom/android/gallery3d/app/MovieControllerOverlay$ControllerRewindAndForwardExt;->showErrorMessage(Ljava/lang/String;)V

    return-void
.end method

.method public showLoading()V
    .locals 0

    invoke-virtual {p0}, Lcom/android/gallery3d/app/MovieControllerOverlay$ControllerRewindAndForwardExt;->showLoading()V

    return-void
.end method

.method public showPaused()V
    .locals 0

    invoke-virtual {p0}, Lcom/android/gallery3d/app/MovieControllerOverlay$ControllerRewindAndForwardExt;->showPaused()V

    return-void
.end method

.method public showPlaying()V
    .locals 0

    invoke-virtual {p0}, Lcom/android/gallery3d/app/MovieControllerOverlay$ControllerRewindAndForwardExt;->showPlaying()V

    return-void
.end method
