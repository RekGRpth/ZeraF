.class Lcom/android/gallery3d/app/PhotoDataAdapter$GifAnimationRunnable;
.super Ljava/lang/Object;
.source "PhotoDataAdapter.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/gallery3d/app/PhotoDataAdapter;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "GifAnimationRunnable"
.end annotation


# instance fields
.field private mGifAnimation:Lcom/android/gallery3d/app/PhotoDataAdapter$GifAnimation;

.field private mPath:Lcom/android/gallery3d/data/Path;

.field final synthetic this$0:Lcom/android/gallery3d/app/PhotoDataAdapter;


# direct methods
.method public constructor <init>(Lcom/android/gallery3d/app/PhotoDataAdapter;Lcom/android/gallery3d/data/Path;Lcom/android/gallery3d/app/PhotoDataAdapter$GifAnimation;)V
    .locals 7
    .param p2    # Lcom/android/gallery3d/data/Path;
    .param p3    # Lcom/android/gallery3d/app/PhotoDataAdapter$GifAnimation;

    const/4 v3, 0x1

    const/4 v4, 0x0

    iput-object p1, p0, Lcom/android/gallery3d/app/PhotoDataAdapter$GifAnimationRunnable;->this$0:Lcom/android/gallery3d/app/PhotoDataAdapter;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p2, p0, Lcom/android/gallery3d/app/PhotoDataAdapter$GifAnimationRunnable;->mPath:Lcom/android/gallery3d/data/Path;

    iput-object p3, p0, Lcom/android/gallery3d/app/PhotoDataAdapter$GifAnimationRunnable;->mGifAnimation:Lcom/android/gallery3d/app/PhotoDataAdapter$GifAnimation;

    iget-object v5, p0, Lcom/android/gallery3d/app/PhotoDataAdapter$GifAnimationRunnable;->mGifAnimation:Lcom/android/gallery3d/app/PhotoDataAdapter$GifAnimation;

    if-eqz v5, :cond_0

    iget-object v5, p0, Lcom/android/gallery3d/app/PhotoDataAdapter$GifAnimationRunnable;->mGifAnimation:Lcom/android/gallery3d/app/PhotoDataAdapter$GifAnimation;

    iget-object v5, v5, Lcom/android/gallery3d/app/PhotoDataAdapter$GifAnimation;->gifDecoder:Lcom/mediatek/gallery3d/gif/GifDecoderWrapper;

    if-nez v5, :cond_2

    :cond_0
    const-string v3, "Gallery2/PhotoDataAdapter"

    const-string v4, "GifAnimationRunnable:invalid GifDecoder"

    invoke-static {v3, v4}, Lcom/android/gallery3d/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    invoke-direct {p0}, Lcom/android/gallery3d/app/PhotoDataAdapter$GifAnimationRunnable;->releaseGifResource()V

    :cond_1
    :goto_0
    return-void

    :cond_2
    iget-object v5, p0, Lcom/android/gallery3d/app/PhotoDataAdapter$GifAnimationRunnable;->mGifAnimation:Lcom/android/gallery3d/app/PhotoDataAdapter$GifAnimation;

    iget v5, v5, Lcom/android/gallery3d/app/PhotoDataAdapter$GifAnimation;->animatedIndex:I

    invoke-static {p1}, Lcom/android/gallery3d/app/PhotoDataAdapter;->access$1500(Lcom/android/gallery3d/app/PhotoDataAdapter;)I

    move-result v6

    if-eq v5, v6, :cond_3

    move v1, v3

    :goto_1
    invoke-virtual {p1, v4}, Lcom/android/gallery3d/app/PhotoDataAdapter;->getMediaItem(I)Lcom/android/gallery3d/data/MediaItem;

    move-result-object v2

    if-eqz v2, :cond_4

    invoke-virtual {v2}, Lcom/android/gallery3d/data/MediaObject;->getPath()Lcom/android/gallery3d/data/Path;

    move-result-object v0

    :goto_2
    if-eq p2, v0, :cond_5

    move v5, v3

    :goto_3
    or-int/2addr v1, v5

    if-eqz v1, :cond_6

    const-string v3, "Gallery2/PhotoDataAdapter"

    const-string v4, "GifAnimationRunnable:image changed"

    invoke-static {v3, v4}, Lcom/android/gallery3d/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    invoke-direct {p0}, Lcom/android/gallery3d/app/PhotoDataAdapter$GifAnimationRunnable;->releaseGifResource()V

    goto :goto_0

    :cond_3
    move v1, v4

    goto :goto_1

    :cond_4
    const/4 v0, 0x0

    goto :goto_2

    :cond_5
    move v5, v4

    goto :goto_3

    :cond_6
    iget-object v5, p0, Lcom/android/gallery3d/app/PhotoDataAdapter$GifAnimationRunnable;->mGifAnimation:Lcom/android/gallery3d/app/PhotoDataAdapter$GifAnimation;

    iput v4, v5, Lcom/android/gallery3d/app/PhotoDataAdapter$GifAnimation;->currentFrame:I

    iget-object v4, p0, Lcom/android/gallery3d/app/PhotoDataAdapter$GifAnimationRunnable;->mGifAnimation:Lcom/android/gallery3d/app/PhotoDataAdapter$GifAnimation;

    iget-object v5, p0, Lcom/android/gallery3d/app/PhotoDataAdapter$GifAnimationRunnable;->mGifAnimation:Lcom/android/gallery3d/app/PhotoDataAdapter$GifAnimation;

    iget-object v5, v5, Lcom/android/gallery3d/app/PhotoDataAdapter$GifAnimation;->gifDecoder:Lcom/mediatek/gallery3d/gif/GifDecoderWrapper;

    invoke-virtual {v5}, Lcom/mediatek/gallery3d/gif/GifDecoderWrapper;->getTotalFrameCount()I

    move-result v5

    iput v5, v4, Lcom/android/gallery3d/app/PhotoDataAdapter$GifAnimation;->totalFrameCount:I

    iget-object v4, p0, Lcom/android/gallery3d/app/PhotoDataAdapter$GifAnimationRunnable;->mGifAnimation:Lcom/android/gallery3d/app/PhotoDataAdapter$GifAnimation;

    iget v4, v4, Lcom/android/gallery3d/app/PhotoDataAdapter$GifAnimation;->totalFrameCount:I

    if-gt v4, v3, :cond_1

    const-string v3, "Gallery2/PhotoDataAdapter"

    const-string v4, "GifAnimationRunnable:invalid frame count, NO animation!"

    invoke-static {v3, v4}, Lcom/android/gallery3d/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    invoke-direct {p0}, Lcom/android/gallery3d/app/PhotoDataAdapter$GifAnimationRunnable;->releaseGifResource()V

    goto :goto_0
.end method

.method private releaseGifResource()V
    .locals 2

    const/4 v1, 0x0

    iget-object v0, p0, Lcom/android/gallery3d/app/PhotoDataAdapter$GifAnimationRunnable;->mGifAnimation:Lcom/android/gallery3d/app/PhotoDataAdapter$GifAnimation;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/gallery3d/app/PhotoDataAdapter$GifAnimationRunnable;->mGifAnimation:Lcom/android/gallery3d/app/PhotoDataAdapter$GifAnimation;

    iput-object v1, v0, Lcom/android/gallery3d/app/PhotoDataAdapter$GifAnimation;->gifDecoder:Lcom/mediatek/gallery3d/gif/GifDecoderWrapper;

    iput-object v1, p0, Lcom/android/gallery3d/app/PhotoDataAdapter$GifAnimationRunnable;->mGifAnimation:Lcom/android/gallery3d/app/PhotoDataAdapter$GifAnimation;

    :cond_0
    return-void
.end method


# virtual methods
.method public run()V
    .locals 13

    const/4 v10, 0x0

    const/4 v8, 0x1

    const/4 v9, 0x0

    iget-object v11, p0, Lcom/android/gallery3d/app/PhotoDataAdapter$GifAnimationRunnable;->this$0:Lcom/android/gallery3d/app/PhotoDataAdapter;

    invoke-static {v11}, Lcom/android/gallery3d/app/PhotoDataAdapter;->access$3800(Lcom/android/gallery3d/app/PhotoDataAdapter;)Z

    move-result v11

    if-nez v11, :cond_0

    const-string v8, "Gallery2/PhotoDataAdapter"

    const-string v9, "GifAnimationRunnable:run:already paused"

    invoke-static {v8, v9}, Lcom/android/gallery3d/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    invoke-direct {p0}, Lcom/android/gallery3d/app/PhotoDataAdapter$GifAnimationRunnable;->releaseGifResource()V

    :goto_0
    return-void

    :cond_0
    iget-object v11, p0, Lcom/android/gallery3d/app/PhotoDataAdapter$GifAnimationRunnable;->mGifAnimation:Lcom/android/gallery3d/app/PhotoDataAdapter$GifAnimation;

    if-eqz v11, :cond_1

    iget-object v11, p0, Lcom/android/gallery3d/app/PhotoDataAdapter$GifAnimationRunnable;->mGifAnimation:Lcom/android/gallery3d/app/PhotoDataAdapter$GifAnimation;

    iget-object v11, v11, Lcom/android/gallery3d/app/PhotoDataAdapter$GifAnimation;->gifDecoder:Lcom/mediatek/gallery3d/gif/GifDecoderWrapper;

    if-nez v11, :cond_2

    :cond_1
    const-string v8, "Gallery2/PhotoDataAdapter"

    const-string v9, "GifAnimationRunnable:run:invalid GifDecoder"

    invoke-static {v8, v9}, Lcom/android/gallery3d/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    invoke-direct {p0}, Lcom/android/gallery3d/app/PhotoDataAdapter$GifAnimationRunnable;->releaseGifResource()V

    goto :goto_0

    :cond_2
    iget-object v11, p0, Lcom/android/gallery3d/app/PhotoDataAdapter$GifAnimationRunnable;->mGifAnimation:Lcom/android/gallery3d/app/PhotoDataAdapter$GifAnimation;

    iget v11, v11, Lcom/android/gallery3d/app/PhotoDataAdapter$GifAnimation;->animatedIndex:I

    iget-object v12, p0, Lcom/android/gallery3d/app/PhotoDataAdapter$GifAnimationRunnable;->this$0:Lcom/android/gallery3d/app/PhotoDataAdapter;

    invoke-static {v12}, Lcom/android/gallery3d/app/PhotoDataAdapter;->access$1500(Lcom/android/gallery3d/app/PhotoDataAdapter;)I

    move-result v12

    if-eq v11, v12, :cond_3

    move v5, v8

    :goto_1
    iget-object v11, p0, Lcom/android/gallery3d/app/PhotoDataAdapter$GifAnimationRunnable;->this$0:Lcom/android/gallery3d/app/PhotoDataAdapter;

    invoke-virtual {v11, v9}, Lcom/android/gallery3d/app/PhotoDataAdapter;->getMediaItem(I)Lcom/android/gallery3d/data/MediaItem;

    move-result-object v6

    if-eqz v6, :cond_4

    invoke-virtual {v6}, Lcom/android/gallery3d/data/MediaObject;->getPath()Lcom/android/gallery3d/data/Path;

    move-result-object v3

    :goto_2
    iget-object v11, p0, Lcom/android/gallery3d/app/PhotoDataAdapter$GifAnimationRunnable;->mPath:Lcom/android/gallery3d/data/Path;

    if-eq v11, v3, :cond_5

    move v11, v8

    :goto_3
    or-int/2addr v5, v11

    if-eqz v5, :cond_6

    const-string v8, "Gallery2/PhotoDataAdapter"

    const-string v9, " GifAnimationRunnable:run:image changed"

    invoke-static {v8, v9}, Lcom/android/gallery3d/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    invoke-direct {p0}, Lcom/android/gallery3d/app/PhotoDataAdapter$GifAnimationRunnable;->releaseGifResource()V

    goto :goto_0

    :cond_3
    move v5, v9

    goto :goto_1

    :cond_4
    move-object v3, v10

    goto :goto_2

    :cond_5
    move v11, v9

    goto :goto_3

    :cond_6
    iget-object v11, p0, Lcom/android/gallery3d/app/PhotoDataAdapter$GifAnimationRunnable;->mGifAnimation:Lcom/android/gallery3d/app/PhotoDataAdapter$GifAnimation;

    iget-object v11, v11, Lcom/android/gallery3d/app/PhotoDataAdapter$GifAnimation;->gifDecoder:Lcom/mediatek/gallery3d/gif/GifDecoderWrapper;

    iget-object v12, p0, Lcom/android/gallery3d/app/PhotoDataAdapter$GifAnimationRunnable;->mGifAnimation:Lcom/android/gallery3d/app/PhotoDataAdapter$GifAnimation;

    iget v12, v12, Lcom/android/gallery3d/app/PhotoDataAdapter$GifAnimation;->currentFrame:I

    invoke-virtual {v11, v12}, Lcom/mediatek/gallery3d/gif/GifDecoderWrapper;->getFrameBitmap(I)Landroid/graphics/Bitmap;

    move-result-object v0

    if-nez v0, :cond_7

    const-string v8, "Gallery2/PhotoDataAdapter"

    const-string v9, "GifAnimationRunnable:run:got null frame!"

    invoke-static {v8, v9}, Lcom/android/gallery3d/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    invoke-direct {p0}, Lcom/android/gallery3d/app/PhotoDataAdapter$GifAnimationRunnable;->releaseGifResource()V

    goto :goto_0

    :cond_7
    const/16 v11, 0x800

    invoke-static {v0, v11, v8}, Lcom/android/gallery3d/common/BitmapUtils;->resizeDownBySideLength(Landroid/graphics/Bitmap;IZ)Landroid/graphics/Bitmap;

    move-result-object v0

    invoke-static {v0}, Lcom/mediatek/gallery3d/util/MediatekFeature;->replaceGifBackground(Landroid/graphics/Bitmap;)Landroid/graphics/Bitmap;

    move-result-object v0

    const-string v8, "Gallery2/PhotoDataAdapter"

    new-instance v11, Ljava/lang/StringBuilder;

    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    const-string v12, "GifAnimationRunnable:run:update frame["

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    iget-object v12, p0, Lcom/android/gallery3d/app/PhotoDataAdapter$GifAnimationRunnable;->mGifAnimation:Lcom/android/gallery3d/app/PhotoDataAdapter$GifAnimation;

    iget v12, v12, Lcom/android/gallery3d/app/PhotoDataAdapter$GifAnimation;->currentFrame:I

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v11

    const-string v12, "]"

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-static {v8, v11}, Lcom/android/gallery3d/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v8, p0, Lcom/android/gallery3d/app/PhotoDataAdapter$GifAnimationRunnable;->mGifAnimation:Lcom/android/gallery3d/app/PhotoDataAdapter$GifAnimation;

    iget-object v8, v8, Lcom/android/gallery3d/app/PhotoDataAdapter$GifAnimation;->gifDecoder:Lcom/mediatek/gallery3d/gif/GifDecoderWrapper;

    iget-object v11, p0, Lcom/android/gallery3d/app/PhotoDataAdapter$GifAnimationRunnable;->mGifAnimation:Lcom/android/gallery3d/app/PhotoDataAdapter$GifAnimation;

    iget v11, v11, Lcom/android/gallery3d/app/PhotoDataAdapter$GifAnimation;->currentFrame:I

    invoke-virtual {v8, v11}, Lcom/mediatek/gallery3d/gif/GifDecoderWrapper;->getFrameDuration(I)I

    move-result v8

    int-to-long v1, v8

    iget-object v8, p0, Lcom/android/gallery3d/app/PhotoDataAdapter$GifAnimationRunnable;->mGifAnimation:Lcom/android/gallery3d/app/PhotoDataAdapter$GifAnimation;

    iget-object v11, p0, Lcom/android/gallery3d/app/PhotoDataAdapter$GifAnimationRunnable;->mGifAnimation:Lcom/android/gallery3d/app/PhotoDataAdapter$GifAnimation;

    iget v11, v11, Lcom/android/gallery3d/app/PhotoDataAdapter$GifAnimation;->currentFrame:I

    add-int/lit8 v11, v11, 0x1

    iget-object v12, p0, Lcom/android/gallery3d/app/PhotoDataAdapter$GifAnimationRunnable;->mGifAnimation:Lcom/android/gallery3d/app/PhotoDataAdapter$GifAnimation;

    iget v12, v12, Lcom/android/gallery3d/app/PhotoDataAdapter$GifAnimation;->totalFrameCount:I

    rem-int/2addr v11, v12

    iput v11, v8, Lcom/android/gallery3d/app/PhotoDataAdapter$GifAnimation;->currentFrame:I

    const/4 v4, 0x0

    iget-object v8, p0, Lcom/android/gallery3d/app/PhotoDataAdapter$GifAnimationRunnable;->this$0:Lcom/android/gallery3d/app/PhotoDataAdapter;

    invoke-virtual {v8, v9}, Lcom/android/gallery3d/app/PhotoDataAdapter;->getMediaItem(I)Lcom/android/gallery3d/data/MediaItem;

    move-result-object v8

    invoke-static {v8, v0}, Lcom/mediatek/gallery3d/util/MediatekFeature;->getMtkScreenNail(Lcom/android/gallery3d/data/MediaItem;Landroid/graphics/Bitmap;)Lcom/android/gallery3d/ui/ScreenNail;

    move-result-object v7

    if-nez v7, :cond_8

    new-instance v7, Lcom/android/gallery3d/ui/BitmapScreenNail;

    invoke-direct {v7, v0}, Lcom/android/gallery3d/ui/BitmapScreenNail;-><init>(Landroid/graphics/Bitmap;)V

    :cond_8
    move-object v4, v7

    iget-object v8, p0, Lcom/android/gallery3d/app/PhotoDataAdapter$GifAnimationRunnable;->mGifAnimation:Lcom/android/gallery3d/app/PhotoDataAdapter$GifAnimation;

    iget-object v8, v8, Lcom/android/gallery3d/app/PhotoDataAdapter$GifAnimation;->entry:Lcom/android/gallery3d/app/PhotoDataAdapter$ImageEntry;

    iget-object v8, v8, Lcom/android/gallery3d/app/PhotoDataAdapter$ImageEntry;->currentGifFrame:Lcom/android/gallery3d/ui/ScreenNail;

    if-eqz v8, :cond_9

    iget-object v8, p0, Lcom/android/gallery3d/app/PhotoDataAdapter$GifAnimationRunnable;->mGifAnimation:Lcom/android/gallery3d/app/PhotoDataAdapter$GifAnimation;

    iget-object v8, v8, Lcom/android/gallery3d/app/PhotoDataAdapter$GifAnimation;->entry:Lcom/android/gallery3d/app/PhotoDataAdapter$ImageEntry;

    iget-object v8, v8, Lcom/android/gallery3d/app/PhotoDataAdapter$ImageEntry;->currentGifFrame:Lcom/android/gallery3d/ui/ScreenNail;

    invoke-interface {v8}, Lcom/android/gallery3d/ui/ScreenNail;->recycle()V

    iget-object v8, p0, Lcom/android/gallery3d/app/PhotoDataAdapter$GifAnimationRunnable;->mGifAnimation:Lcom/android/gallery3d/app/PhotoDataAdapter$GifAnimation;

    iget-object v8, v8, Lcom/android/gallery3d/app/PhotoDataAdapter$GifAnimation;->entry:Lcom/android/gallery3d/app/PhotoDataAdapter$ImageEntry;

    iput-object v10, v8, Lcom/android/gallery3d/app/PhotoDataAdapter$ImageEntry;->currentGifFrame:Lcom/android/gallery3d/ui/ScreenNail;

    :cond_9
    iget-object v8, p0, Lcom/android/gallery3d/app/PhotoDataAdapter$GifAnimationRunnable;->mGifAnimation:Lcom/android/gallery3d/app/PhotoDataAdapter$GifAnimation;

    iget-object v8, v8, Lcom/android/gallery3d/app/PhotoDataAdapter$GifAnimation;->entry:Lcom/android/gallery3d/app/PhotoDataAdapter$ImageEntry;

    iput-object v4, v8, Lcom/android/gallery3d/app/PhotoDataAdapter$ImageEntry;->currentGifFrame:Lcom/android/gallery3d/ui/ScreenNail;

    iget-object v8, p0, Lcom/android/gallery3d/app/PhotoDataAdapter$GifAnimationRunnable;->this$0:Lcom/android/gallery3d/app/PhotoDataAdapter;

    iget-object v10, p0, Lcom/android/gallery3d/app/PhotoDataAdapter$GifAnimationRunnable;->mGifAnimation:Lcom/android/gallery3d/app/PhotoDataAdapter$GifAnimation;

    iget-object v10, v10, Lcom/android/gallery3d/app/PhotoDataAdapter$GifAnimation;->entry:Lcom/android/gallery3d/app/PhotoDataAdapter$ImageEntry;

    invoke-static {v8, v10}, Lcom/android/gallery3d/app/PhotoDataAdapter;->access$3900(Lcom/android/gallery3d/app/PhotoDataAdapter;Lcom/android/gallery3d/app/PhotoDataAdapter$ImageEntry;)V

    iget-object v8, p0, Lcom/android/gallery3d/app/PhotoDataAdapter$GifAnimationRunnable;->this$0:Lcom/android/gallery3d/app/PhotoDataAdapter;

    invoke-static {v8}, Lcom/android/gallery3d/app/PhotoDataAdapter;->access$4000(Lcom/android/gallery3d/app/PhotoDataAdapter;)Lcom/android/gallery3d/ui/PhotoView;

    move-result-object v8

    invoke-virtual {v8, v9}, Lcom/android/gallery3d/ui/PhotoView;->notifyImageChange(I)V

    iget-object v8, p0, Lcom/android/gallery3d/app/PhotoDataAdapter$GifAnimationRunnable;->this$0:Lcom/android/gallery3d/app/PhotoDataAdapter;

    invoke-static {v8}, Lcom/android/gallery3d/app/PhotoDataAdapter;->access$800(Lcom/android/gallery3d/app/PhotoDataAdapter;)Landroid/os/Handler;

    move-result-object v8

    iget-object v9, p0, Lcom/android/gallery3d/app/PhotoDataAdapter$GifAnimationRunnable;->this$0:Lcom/android/gallery3d/app/PhotoDataAdapter;

    invoke-static {v9}, Lcom/android/gallery3d/app/PhotoDataAdapter;->access$800(Lcom/android/gallery3d/app/PhotoDataAdapter;)Landroid/os/Handler;

    move-result-object v9

    const/4 v10, 0x3

    invoke-virtual {v9, v10, p0}, Landroid/os/Handler;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v9

    invoke-virtual {v8, v9, v1, v2}, Landroid/os/Handler;->sendMessageDelayed(Landroid/os/Message;J)Z

    goto/16 :goto_0
.end method
