.class Lcom/android/gallery3d/app/MoviePlayer$RetryExtension;
.super Ljava/lang/Object;
.source "MoviePlayer.java"

# interfaces
.implements Landroid/media/MediaPlayer$OnErrorListener;
.implements Landroid/media/MediaPlayer$OnInfoListener;
.implements Lcom/android/gallery3d/app/MoviePlayer$Restorable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/gallery3d/app/MoviePlayer;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "RetryExtension"
.end annotation


# static fields
.field private static final KEY_VIDEO_RETRY_COUNT:Ljava/lang/String; = "video_retry_count"


# instance fields
.field private mRetryCount:I

.field private mRetryDuration:I

.field private mRetryPosition:I

.field final synthetic this$0:Lcom/android/gallery3d/app/MoviePlayer;


# direct methods
.method private constructor <init>(Lcom/android/gallery3d/app/MoviePlayer;)V
    .locals 0

    iput-object p1, p0, Lcom/android/gallery3d/app/MoviePlayer$RetryExtension;->this$0:Lcom/android/gallery3d/app/MoviePlayer;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/android/gallery3d/app/MoviePlayer;Lcom/android/gallery3d/app/MoviePlayer$1;)V
    .locals 0
    .param p1    # Lcom/android/gallery3d/app/MoviePlayer;
    .param p2    # Lcom/android/gallery3d/app/MoviePlayer$1;

    invoke-direct {p0, p1}, Lcom/android/gallery3d/app/MoviePlayer$RetryExtension;-><init>(Lcom/android/gallery3d/app/MoviePlayer;)V

    return-void
.end method


# virtual methods
.method public clearRetry()V
    .locals 3

    const-string v0, "Gallery2/MoviePlayer"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "clearRetry() mRetryCount="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/android/gallery3d/app/MoviePlayer$RetryExtension;->mRetryCount:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/mediatek/gallery3d/ext/MtkLog;->v(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v0, 0x0

    iput v0, p0, Lcom/android/gallery3d/app/MoviePlayer$RetryExtension;->mRetryCount:I

    return-void
.end method

.method public getRetryCount()I
    .locals 3

    const-string v0, "Gallery2/MoviePlayer"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "getRetryCount() return "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/android/gallery3d/app/MoviePlayer$RetryExtension;->mRetryCount:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/mediatek/gallery3d/ext/MtkLog;->v(Ljava/lang/String;Ljava/lang/String;)I

    iget v0, p0, Lcom/android/gallery3d/app/MoviePlayer$RetryExtension;->mRetryCount:I

    return v0
.end method

.method public handleOnReplay()Z
    .locals 6

    const/4 v3, 0x1

    const/4 v2, 0x0

    invoke-virtual {p0}, Lcom/android/gallery3d/app/MoviePlayer$RetryExtension;->isRetrying()Z

    move-result v4

    if-eqz v4, :cond_1

    invoke-virtual {p0}, Lcom/android/gallery3d/app/MoviePlayer$RetryExtension;->clearRetry()V

    iget-object v4, p0, Lcom/android/gallery3d/app/MoviePlayer$RetryExtension;->this$0:Lcom/android/gallery3d/app/MoviePlayer;

    invoke-static {v4}, Lcom/android/gallery3d/app/MoviePlayer;->access$000(Lcom/android/gallery3d/app/MoviePlayer;)Lcom/mediatek/gallery3d/video/MTKVideoView;

    move-result-object v4

    invoke-virtual {v4}, Lcom/mediatek/gallery3d/video/MTKVideoView;->getCurrentPosition()I

    move-result v1

    iget-object v4, p0, Lcom/android/gallery3d/app/MoviePlayer$RetryExtension;->this$0:Lcom/android/gallery3d/app/MoviePlayer;

    invoke-static {v4}, Lcom/android/gallery3d/app/MoviePlayer;->access$000(Lcom/android/gallery3d/app/MoviePlayer;)Lcom/mediatek/gallery3d/video/MTKVideoView;

    move-result-object v4

    invoke-virtual {v4}, Lcom/mediatek/gallery3d/video/MTKVideoView;->getDuration()I

    move-result v0

    iget-object v4, p0, Lcom/android/gallery3d/app/MoviePlayer$RetryExtension;->this$0:Lcom/android/gallery3d/app/MoviePlayer;

    if-lez v1, :cond_0

    move v2, v3

    :cond_0
    invoke-static {v4, v2, v1, v0}, Lcom/android/gallery3d/app/MoviePlayer;->access$1200(Lcom/android/gallery3d/app/MoviePlayer;ZII)V

    const-string v2, "Gallery2/MoviePlayer"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "onReplay() errorPosition="

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ", errorDuration="

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v2, v4}, Lcom/mediatek/gallery3d/ext/MtkLog;->v(Ljava/lang/String;Ljava/lang/String;)I

    :goto_0
    return v3

    :cond_1
    move v3, v2

    goto :goto_0
.end method

.method public isRetrying()Z
    .locals 4

    const/4 v0, 0x0

    iget v1, p0, Lcom/android/gallery3d/app/MoviePlayer$RetryExtension;->mRetryCount:I

    if-lez v1, :cond_0

    const/4 v0, 0x1

    :cond_0
    const-string v1, "Gallery2/MoviePlayer"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "isRetrying() mRetryCount="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget v3, p0, Lcom/android/gallery3d/app/MoviePlayer$RetryExtension;->mRetryCount:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/mediatek/gallery3d/ext/MtkLog;->v(Ljava/lang/String;Ljava/lang/String;)I

    return v0
.end method

.method public onError(Landroid/media/MediaPlayer;II)Z
    .locals 3
    .param p1    # Landroid/media/MediaPlayer;
    .param p2    # I
    .param p3    # I

    const/4 v0, 0x1

    const/16 v1, 0x105

    if-ne p2, v1, :cond_1

    iget-object v1, p0, Lcom/android/gallery3d/app/MoviePlayer$RetryExtension;->this$0:Lcom/android/gallery3d/app/MoviePlayer;

    invoke-static {v1}, Lcom/android/gallery3d/app/MoviePlayer;->access$000(Lcom/android/gallery3d/app/MoviePlayer;)Lcom/mediatek/gallery3d/video/MTKVideoView;

    move-result-object v1

    invoke-virtual {v1}, Lcom/mediatek/gallery3d/video/MTKVideoView;->getCurrentPosition()I

    move-result v1

    iput v1, p0, Lcom/android/gallery3d/app/MoviePlayer$RetryExtension;->mRetryPosition:I

    iget-object v1, p0, Lcom/android/gallery3d/app/MoviePlayer$RetryExtension;->this$0:Lcom/android/gallery3d/app/MoviePlayer;

    invoke-static {v1}, Lcom/android/gallery3d/app/MoviePlayer;->access$000(Lcom/android/gallery3d/app/MoviePlayer;)Lcom/mediatek/gallery3d/video/MTKVideoView;

    move-result-object v1

    invoke-virtual {v1}, Lcom/mediatek/gallery3d/video/MTKVideoView;->getDuration()I

    move-result v1

    iput v1, p0, Lcom/android/gallery3d/app/MoviePlayer$RetryExtension;->mRetryDuration:I

    iget v1, p0, Lcom/android/gallery3d/app/MoviePlayer$RetryExtension;->mRetryCount:I

    add-int/lit8 v1, v1, 0x1

    iput v1, p0, Lcom/android/gallery3d/app/MoviePlayer$RetryExtension;->mRetryCount:I

    invoke-virtual {p0}, Lcom/android/gallery3d/app/MoviePlayer$RetryExtension;->reachRetryCount()Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/android/gallery3d/app/MoviePlayer$RetryExtension;->this$0:Lcom/android/gallery3d/app/MoviePlayer;

    sget-object v2, Lcom/android/gallery3d/app/MoviePlayer$TState;->RETRY_ERROR:Lcom/android/gallery3d/app/MoviePlayer$TState;

    invoke-static {v1, v2}, Lcom/android/gallery3d/app/MoviePlayer;->access$2702(Lcom/android/gallery3d/app/MoviePlayer;Lcom/android/gallery3d/app/MoviePlayer$TState;)Lcom/android/gallery3d/app/MoviePlayer$TState;

    iget-object v1, p0, Lcom/android/gallery3d/app/MoviePlayer$RetryExtension;->this$0:Lcom/android/gallery3d/app/MoviePlayer;

    invoke-static {v1}, Lcom/android/gallery3d/app/MoviePlayer;->access$2800(Lcom/android/gallery3d/app/MoviePlayer;)Lcom/mediatek/gallery3d/ext/IContrllerOverlayExt;

    move-result-object v1

    invoke-interface {v1}, Lcom/mediatek/gallery3d/ext/IContrllerOverlayExt;->showReconnectingError()V

    iget-object v1, p0, Lcom/android/gallery3d/app/MoviePlayer$RetryExtension;->this$0:Lcom/android/gallery3d/app/MoviePlayer;

    invoke-static {v1}, Lcom/android/gallery3d/app/MoviePlayer;->access$100(Lcom/android/gallery3d/app/MoviePlayer;)Lcom/android/gallery3d/app/MovieControllerOverlay;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/android/gallery3d/app/CommonControllerOverlay;->setCanReplay(Z)V

    :goto_0
    return v0

    :cond_0
    iget-object v1, p0, Lcom/android/gallery3d/app/MoviePlayer$RetryExtension;->this$0:Lcom/android/gallery3d/app/MoviePlayer;

    invoke-static {v1}, Lcom/android/gallery3d/app/MoviePlayer;->access$2800(Lcom/android/gallery3d/app/MoviePlayer;)Lcom/mediatek/gallery3d/ext/IContrllerOverlayExt;

    move-result-object v1

    iget v2, p0, Lcom/android/gallery3d/app/MoviePlayer$RetryExtension;->mRetryCount:I

    invoke-interface {v1, v2}, Lcom/mediatek/gallery3d/ext/IContrllerOverlayExt;->showReconnecting(I)V

    invoke-virtual {p0}, Lcom/android/gallery3d/app/MoviePlayer$RetryExtension;->retry()V

    goto :goto_0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public onInfo(Landroid/media/MediaPlayer;II)Z
    .locals 3
    .param p1    # Landroid/media/MediaPlayer;
    .param p2    # I
    .param p3    # I

    const/4 v0, 0x1

    const/16 v1, 0x35d

    if-ne p2, v1, :cond_0

    invoke-virtual {p0}, Lcom/android/gallery3d/app/MoviePlayer$RetryExtension;->clearRetry()V

    :goto_0
    return v0

    :cond_0
    const/16 v1, 0x35a

    if-eq p2, v1, :cond_1

    const/16 v1, 0x35b

    if-ne p2, v1, :cond_2

    :cond_1
    const-string v1, "Gallery2/MoviePlayer"

    const-string v2, "onInfo is PAUSE PLAY COMPLETED"

    invoke-static {v1, v2}, Lcom/mediatek/gallery3d/ext/MtkLog;->v(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v1, p0, Lcom/android/gallery3d/app/MoviePlayer$RetryExtension;->this$0:Lcom/android/gallery3d/app/MoviePlayer;

    invoke-static {v1}, Lcom/android/gallery3d/app/MoviePlayer;->access$000(Lcom/android/gallery3d/app/MoviePlayer;)Lcom/mediatek/gallery3d/video/MTKVideoView;

    move-result-object v1

    invoke-virtual {v1}, Landroid/widget/VideoView;->canPause()Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/android/gallery3d/app/MoviePlayer$RetryExtension;->this$0:Lcom/android/gallery3d/app/MoviePlayer;

    invoke-static {v1}, Lcom/android/gallery3d/app/MoviePlayer;->access$100(Lcom/android/gallery3d/app/MoviePlayer;)Lcom/android/gallery3d/app/MovieControllerOverlay;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/android/gallery3d/app/MovieControllerOverlay;->setViewEnabled(Z)V

    iget-object v0, p0, Lcom/android/gallery3d/app/MoviePlayer$RetryExtension;->this$0:Lcom/android/gallery3d/app/MoviePlayer;

    invoke-virtual {v0}, Lcom/android/gallery3d/app/MoviePlayer;->updateRewindAndForwardUI()V

    :cond_2
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public onRestoreInstanceState(Landroid/os/Bundle;)V
    .locals 1
    .param p1    # Landroid/os/Bundle;

    const-string v0, "video_retry_count"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lcom/android/gallery3d/app/MoviePlayer$RetryExtension;->mRetryCount:I

    return-void
.end method

.method public onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 2
    .param p1    # Landroid/os/Bundle;

    const-string v0, "video_retry_count"

    iget v1, p0, Lcom/android/gallery3d/app/MoviePlayer$RetryExtension;->mRetryCount:I

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    return-void
.end method

.method public reachRetryCount()Z
    .locals 3

    const-string v0, "Gallery2/MoviePlayer"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "reachRetryCount() mRetryCount="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/android/gallery3d/app/MoviePlayer$RetryExtension;->mRetryCount:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/mediatek/gallery3d/ext/MtkLog;->v(Ljava/lang/String;Ljava/lang/String;)I

    iget v0, p0, Lcom/android/gallery3d/app/MoviePlayer$RetryExtension;->mRetryCount:I

    const/4 v1, 0x3

    if-le v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public retry()V
    .locals 4

    iget-object v0, p0, Lcom/android/gallery3d/app/MoviePlayer$RetryExtension;->this$0:Lcom/android/gallery3d/app/MoviePlayer;

    const/4 v1, 0x1

    iget v2, p0, Lcom/android/gallery3d/app/MoviePlayer$RetryExtension;->mRetryPosition:I

    iget v3, p0, Lcom/android/gallery3d/app/MoviePlayer$RetryExtension;->mRetryDuration:I

    invoke-static {v0, v1, v2, v3}, Lcom/android/gallery3d/app/MoviePlayer;->access$1200(Lcom/android/gallery3d/app/MoviePlayer;ZII)V

    const-string v0, "Gallery2/MoviePlayer"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "retry() mRetryCount="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/android/gallery3d/app/MoviePlayer$RetryExtension;->mRetryCount:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", mRetryPosition="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/android/gallery3d/app/MoviePlayer$RetryExtension;->mRetryPosition:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/mediatek/gallery3d/ext/MtkLog;->v(Ljava/lang/String;Ljava/lang/String;)I

    return-void
.end method

.method public showRetry()V
    .locals 2

    iget-object v0, p0, Lcom/android/gallery3d/app/MoviePlayer$RetryExtension;->this$0:Lcom/android/gallery3d/app/MoviePlayer;

    invoke-static {v0}, Lcom/android/gallery3d/app/MoviePlayer;->access$2800(Lcom/android/gallery3d/app/MoviePlayer;)Lcom/mediatek/gallery3d/ext/IContrllerOverlayExt;

    move-result-object v0

    invoke-interface {v0}, Lcom/mediatek/gallery3d/ext/IContrllerOverlayExt;->showReconnectingError()V

    iget-object v0, p0, Lcom/android/gallery3d/app/MoviePlayer$RetryExtension;->this$0:Lcom/android/gallery3d/app/MoviePlayer;

    invoke-static {v0}, Lcom/android/gallery3d/app/MoviePlayer;->access$1100(Lcom/android/gallery3d/app/MoviePlayer;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/android/gallery3d/app/MoviePlayer$RetryExtension;->this$0:Lcom/android/gallery3d/app/MoviePlayer;

    invoke-static {v0}, Lcom/android/gallery3d/app/MoviePlayer;->access$000(Lcom/android/gallery3d/app/MoviePlayer;)Lcom/mediatek/gallery3d/video/MTKVideoView;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/VideoView;->canSeekForward()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    iget-object v0, p0, Lcom/android/gallery3d/app/MoviePlayer$RetryExtension;->this$0:Lcom/android/gallery3d/app/MoviePlayer;

    invoke-static {v0}, Lcom/android/gallery3d/app/MoviePlayer;->access$000(Lcom/android/gallery3d/app/MoviePlayer;)Lcom/mediatek/gallery3d/video/MTKVideoView;

    move-result-object v0

    iget-object v1, p0, Lcom/android/gallery3d/app/MoviePlayer$RetryExtension;->this$0:Lcom/android/gallery3d/app/MoviePlayer;

    invoke-static {v1}, Lcom/android/gallery3d/app/MoviePlayer;->access$2900(Lcom/android/gallery3d/app/MoviePlayer;)I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/mediatek/gallery3d/video/MTKVideoView;->seekTo(I)V

    :cond_1
    iget-object v0, p0, Lcom/android/gallery3d/app/MoviePlayer$RetryExtension;->this$0:Lcom/android/gallery3d/app/MoviePlayer;

    invoke-static {v0}, Lcom/android/gallery3d/app/MoviePlayer;->access$000(Lcom/android/gallery3d/app/MoviePlayer;)Lcom/mediatek/gallery3d/video/MTKVideoView;

    move-result-object v0

    iget-object v1, p0, Lcom/android/gallery3d/app/MoviePlayer$RetryExtension;->this$0:Lcom/android/gallery3d/app/MoviePlayer;

    invoke-static {v1}, Lcom/android/gallery3d/app/MoviePlayer;->access$3000(Lcom/android/gallery3d/app/MoviePlayer;)I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/mediatek/gallery3d/video/MTKVideoView;->setDuration(I)V

    iget-object v0, p0, Lcom/android/gallery3d/app/MoviePlayer$RetryExtension;->this$0:Lcom/android/gallery3d/app/MoviePlayer;

    invoke-static {v0}, Lcom/android/gallery3d/app/MoviePlayer;->access$2900(Lcom/android/gallery3d/app/MoviePlayer;)I

    move-result v0

    iput v0, p0, Lcom/android/gallery3d/app/MoviePlayer$RetryExtension;->mRetryPosition:I

    iget-object v0, p0, Lcom/android/gallery3d/app/MoviePlayer$RetryExtension;->this$0:Lcom/android/gallery3d/app/MoviePlayer;

    invoke-static {v0}, Lcom/android/gallery3d/app/MoviePlayer;->access$3000(Lcom/android/gallery3d/app/MoviePlayer;)I

    move-result v0

    iput v0, p0, Lcom/android/gallery3d/app/MoviePlayer$RetryExtension;->mRetryDuration:I

    return-void
.end method
