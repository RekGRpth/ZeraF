.class public Lcom/android/gallery3d/app/StitchingProgressManager;
.super Ljava/lang/Object;
.source "StitchingProgressManager.java"


# direct methods
.method public constructor <init>(Lcom/android/gallery3d/app/GalleryApp;)V
    .locals 0
    .param p1    # Lcom/android/gallery3d/app/GalleryApp;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public addChangeListener(Lcom/android/gallery3d/app/StitchingChangeListener;)V
    .locals 0
    .param p1    # Lcom/android/gallery3d/app/StitchingChangeListener;

    return-void
.end method

.method public getProgress(Landroid/net/Uri;)Ljava/lang/Integer;
    .locals 1
    .param p1    # Landroid/net/Uri;

    const/4 v0, 0x0

    return-object v0
.end method

.method public removeChangeListener(Lcom/android/gallery3d/app/StitchingChangeListener;)V
    .locals 0
    .param p1    # Lcom/android/gallery3d/app/StitchingChangeListener;

    return-void
.end method
