.class public Lcom/android/gallery3d/app/SinglePhotoDataAdapter;
.super Lcom/android/gallery3d/ui/TileImageViewAdapter;
.source "SinglePhotoDataAdapter.java"

# interfaces
.implements Lcom/android/gallery3d/app/PhotoPage$Model;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/gallery3d/app/SinglePhotoDataAdapter$MavRenderThread;,
        Lcom/android/gallery3d/app/SinglePhotoDataAdapter$MavDecoderListener;,
        Lcom/android/gallery3d/app/SinglePhotoDataAdapter$GifAnimationRunnable;,
        Lcom/android/gallery3d/app/SinglePhotoDataAdapter$GifDecoderListener;,
        Lcom/android/gallery3d/app/SinglePhotoDataAdapter$ImageBundle;
    }
.end annotation


# static fields
.field private static final MSG_RUN_OBJECT:I = 0x2

.field private static final MSG_UPDATE_IMAGE:I = 0x1

.field private static final MSG_UPDATE_LARGE_IMAGE:I = 0x4

.field private static final MSG_UPDATE_SECOND_IMAGE:I = 0x3

.field private static final SIZE_BACKUP:I = 0x400

.field private static final TAG:Ljava/lang/String; = "Gallery2/SinglePhotoDataAdapter"

.field private static final TYPE_LOAD_FRAME:I = 0x1

.field private static final TYPE_LOAD_TOTAL_COUNT:I = 0x0

.field private static final mIsGifAnimationSupported:Z

.field private static final mIsMavSupported:Z

.field private static final mIsStereoDisplaySupported:Z

.field private static final mShowThumbFirst:Z = true


# instance fields
.field private final mActivity:Lcom/android/gallery3d/app/AbstractGalleryActivity;

.field private mAnimateGif:Z

.field private mBitmapScreenNail:Lcom/android/gallery3d/ui/BitmapScreenNail;

.field private mCurrentFrameNum:I

.field private mCurrentGifFrame:Landroid/graphics/Bitmap;

.field private mCurrentMpoIndex:I

.field private mCurrentScreenNail:Lcom/android/gallery3d/ui/ScreenNail;

.field private mFirstScreenNail:Lcom/android/gallery3d/ui/ScreenNail;

.field private mGifDecoder:Lcom/mediatek/gallery3d/gif/GifDecoderWrapper;

.field private mGifTask:Lcom/android/gallery3d/util/Future;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/android/gallery3d/util/Future",
            "<*>;"
        }
    .end annotation
.end field

.field private mHandler:Landroid/os/Handler;

.field private mHasFullImage:Z

.field private mIsActive:Z

.field private mIsMavLoadingFinished:Z

.field private mIsMavStereoMode:Z

.field private mItem:Lcom/android/gallery3d/data/MediaItem;

.field private mLargeListener:Lcom/android/gallery3d/util/FutureListener;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/android/gallery3d/util/FutureListener",
            "<",
            "Landroid/graphics/BitmapRegionDecoder;",
            ">;"
        }
    .end annotation
.end field

.field private mLoadingState:I

.field private mMavListener:Lcom/android/gallery3d/app/PhotoDataAdapter$MavListener;

.field private mMavPlayback:Z

.field private mMpoDecoderTask:Lcom/android/gallery3d/util/Future;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/android/gallery3d/util/Future",
            "<",
            "Lcom/mediatek/gallery3d/util/MediatekFeature$DataBundle;",
            ">;"
        }
    .end annotation
.end field

.field private mNextMpoIndex:I

.field private mOldCurrentScreenNail:Lcom/android/gallery3d/ui/ScreenNail;

.field private mOldFirstScreenNail:Lcom/android/gallery3d/ui/ScreenNail;

.field private mOldSecondScreenNail:Lcom/android/gallery3d/ui/ScreenNail;

.field private mPhotoView:Lcom/android/gallery3d/ui/PhotoView;

.field private mRenderLock:Ljava/lang/Object;

.field public mRenderRequested:Z

.field private mSecondScreenNail:Lcom/android/gallery3d/ui/ScreenNail;

.field private mSecondThumbListener:Lcom/android/gallery3d/util/FutureListener;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/android/gallery3d/util/FutureListener",
            "<",
            "Lcom/mediatek/gallery3d/util/MediatekFeature$DataBundle;",
            ">;"
        }
    .end annotation
.end field

.field private mShowStereoImage:Z

.field private mShowedThumb:Z

.field private mTask:Lcom/android/gallery3d/util/Future;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/android/gallery3d/util/Future",
            "<*>;"
        }
    .end annotation
.end field

.field private mThreadPool:Lcom/android/gallery3d/util/ThreadPool;

.field private mThumbListener:Lcom/android/gallery3d/util/FutureListener;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/android/gallery3d/util/FutureListener",
            "<",
            "Landroid/graphics/Bitmap;",
            ">;"
        }
    .end annotation
.end field

.field private mTotalFrameCount:I

.field private mpoFrames:[Lcom/android/gallery3d/ui/ScreenNail;

.field private mpoTotalCount:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    invoke-static {}, Lcom/mediatek/gallery3d/util/MediatekFeature;->isGifAnimationSupported()Z

    move-result v0

    sput-boolean v0, Lcom/android/gallery3d/app/SinglePhotoDataAdapter;->mIsGifAnimationSupported:Z

    invoke-static {}, Lcom/mediatek/gallery3d/util/MediatekFeature;->isStereoDisplaySupported()Z

    move-result v0

    sput-boolean v0, Lcom/android/gallery3d/app/SinglePhotoDataAdapter;->mIsStereoDisplaySupported:Z

    invoke-static {}, Lcom/mediatek/gallery3d/util/MediatekFeature;->isMAVSupported()Z

    move-result v0

    sput-boolean v0, Lcom/android/gallery3d/app/SinglePhotoDataAdapter;->mIsMavSupported:Z

    return-void
.end method

.method public constructor <init>(Lcom/android/gallery3d/app/AbstractGalleryActivity;Lcom/android/gallery3d/ui/PhotoView;Lcom/android/gallery3d/data/MediaItem;)V
    .locals 4
    .param p1    # Lcom/android/gallery3d/app/AbstractGalleryActivity;
    .param p2    # Lcom/android/gallery3d/ui/PhotoView;
    .param p3    # Lcom/android/gallery3d/data/MediaItem;

    const/4 v3, -0x1

    const/4 v1, 0x1

    const/4 v2, 0x0

    invoke-direct {p0}, Lcom/android/gallery3d/ui/TileImageViewAdapter;-><init>()V

    iput-boolean v2, p0, Lcom/android/gallery3d/app/SinglePhotoDataAdapter;->mIsActive:Z

    iput v2, p0, Lcom/android/gallery3d/app/SinglePhotoDataAdapter;->mLoadingState:I

    new-instance v0, Lcom/android/gallery3d/app/SinglePhotoDataAdapter$2;

    invoke-direct {v0, p0}, Lcom/android/gallery3d/app/SinglePhotoDataAdapter$2;-><init>(Lcom/android/gallery3d/app/SinglePhotoDataAdapter;)V

    iput-object v0, p0, Lcom/android/gallery3d/app/SinglePhotoDataAdapter;->mLargeListener:Lcom/android/gallery3d/util/FutureListener;

    new-instance v0, Lcom/android/gallery3d/app/SinglePhotoDataAdapter$3;

    invoke-direct {v0, p0}, Lcom/android/gallery3d/app/SinglePhotoDataAdapter$3;-><init>(Lcom/android/gallery3d/app/SinglePhotoDataAdapter;)V

    iput-object v0, p0, Lcom/android/gallery3d/app/SinglePhotoDataAdapter;->mThumbListener:Lcom/android/gallery3d/util/FutureListener;

    new-instance v0, Lcom/android/gallery3d/app/SinglePhotoDataAdapter$4;

    invoke-direct {v0, p0}, Lcom/android/gallery3d/app/SinglePhotoDataAdapter$4;-><init>(Lcom/android/gallery3d/app/SinglePhotoDataAdapter;)V

    iput-object v0, p0, Lcom/android/gallery3d/app/SinglePhotoDataAdapter;->mSecondThumbListener:Lcom/android/gallery3d/util/FutureListener;

    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lcom/android/gallery3d/app/SinglePhotoDataAdapter;->mRenderLock:Ljava/lang/Object;

    iput-boolean v2, p0, Lcom/android/gallery3d/app/SinglePhotoDataAdapter;->mRenderRequested:Z

    iput-boolean v2, p0, Lcom/android/gallery3d/app/SinglePhotoDataAdapter;->mIsMavStereoMode:Z

    iput v3, p0, Lcom/android/gallery3d/app/SinglePhotoDataAdapter;->mCurrentMpoIndex:I

    iput v3, p0, Lcom/android/gallery3d/app/SinglePhotoDataAdapter;->mNextMpoIndex:I

    invoke-static {p3}, Lcom/android/gallery3d/common/Utils;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/gallery3d/data/MediaItem;

    iput-object v0, p0, Lcom/android/gallery3d/app/SinglePhotoDataAdapter;->mItem:Lcom/android/gallery3d/data/MediaItem;

    iput-object p1, p0, Lcom/android/gallery3d/app/SinglePhotoDataAdapter;->mActivity:Lcom/android/gallery3d/app/AbstractGalleryActivity;

    invoke-virtual {p3}, Lcom/android/gallery3d/data/MediaObject;->getSupportedOperations()I

    move-result v0

    and-int/lit8 v0, v0, 0x40

    if-eqz v0, :cond_0

    move v0, v1

    :goto_0
    iput-boolean v0, p0, Lcom/android/gallery3d/app/SinglePhotoDataAdapter;->mHasFullImage:Z

    sget-boolean v0, Lcom/android/gallery3d/app/SinglePhotoDataAdapter;->mIsGifAnimationSupported:Z

    if-eqz v0, :cond_1

    invoke-virtual {p3}, Lcom/android/gallery3d/data/MediaObject;->getSupportedOperations()I

    move-result v0

    and-int/lit16 v0, v0, 0x1000

    if-eqz v0, :cond_1

    iput-boolean v1, p0, Lcom/android/gallery3d/app/SinglePhotoDataAdapter;->mAnimateGif:Z

    :goto_1
    sget-boolean v0, Lcom/android/gallery3d/app/SinglePhotoDataAdapter;->mIsMavSupported:Z

    if-eqz v0, :cond_2

    invoke-virtual {p3}, Lcom/android/gallery3d/data/MediaObject;->getSupportedOperations()I

    move-result v0

    const/high16 v3, 0x2000000

    and-int/2addr v0, v3

    if-eqz v0, :cond_2

    iput-boolean v1, p0, Lcom/android/gallery3d/app/SinglePhotoDataAdapter;->mMavPlayback:Z

    :goto_2
    sget-boolean v0, Lcom/android/gallery3d/app/SinglePhotoDataAdapter;->mIsStereoDisplaySupported:Z

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/android/gallery3d/app/SinglePhotoDataAdapter;->mItem:Lcom/android/gallery3d/data/MediaItem;

    invoke-virtual {v0}, Lcom/android/gallery3d/data/MediaObject;->getSupportedOperations()I

    move-result v0

    const/high16 v3, 0x10000

    and-int/2addr v0, v3

    if-eqz v0, :cond_3

    :goto_3
    iput-boolean v1, p0, Lcom/android/gallery3d/app/SinglePhotoDataAdapter;->mShowStereoImage:Z

    const-string v0, "Gallery2/SinglePhotoDataAdapter"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Gallery2/SinglePhotoDataAdapter:mShowStereoImage="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-boolean v2, p0, Lcom/android/gallery3d/app/SinglePhotoDataAdapter;->mShowStereoImage:Z

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/android/gallery3d/app/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    invoke-static {p2}, Lcom/android/gallery3d/common/Utils;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/gallery3d/ui/PhotoView;

    iput-object v0, p0, Lcom/android/gallery3d/app/SinglePhotoDataAdapter;->mPhotoView:Lcom/android/gallery3d/ui/PhotoView;

    new-instance v0, Lcom/android/gallery3d/app/SinglePhotoDataAdapter$1;

    invoke-virtual {p1}, Lcom/android/gallery3d/app/AbstractGalleryActivity;->getGLRoot()Lcom/android/gallery3d/ui/GLRoot;

    move-result-object v1

    invoke-direct {v0, p0, v1}, Lcom/android/gallery3d/app/SinglePhotoDataAdapter$1;-><init>(Lcom/android/gallery3d/app/SinglePhotoDataAdapter;Lcom/android/gallery3d/ui/GLRoot;)V

    iput-object v0, p0, Lcom/android/gallery3d/app/SinglePhotoDataAdapter;->mHandler:Landroid/os/Handler;

    invoke-virtual {p1}, Lcom/android/gallery3d/app/AbstractGalleryActivity;->getThreadPool()Lcom/android/gallery3d/util/ThreadPool;

    move-result-object v0

    iput-object v0, p0, Lcom/android/gallery3d/app/SinglePhotoDataAdapter;->mThreadPool:Lcom/android/gallery3d/util/ThreadPool;

    return-void

    :cond_0
    move v0, v2

    goto :goto_0

    :cond_1
    iput-boolean v2, p0, Lcom/android/gallery3d/app/SinglePhotoDataAdapter;->mAnimateGif:Z

    goto :goto_1

    :cond_2
    iput-boolean v2, p0, Lcom/android/gallery3d/app/SinglePhotoDataAdapter;->mMavPlayback:Z

    goto :goto_2

    :cond_3
    move v1, v2

    goto :goto_3
.end method

.method static synthetic access$000(Lcom/android/gallery3d/app/SinglePhotoDataAdapter;Lcom/android/gallery3d/util/Future;)V
    .locals 0
    .param p0    # Lcom/android/gallery3d/app/SinglePhotoDataAdapter;
    .param p1    # Lcom/android/gallery3d/util/Future;

    invoke-direct {p0, p1}, Lcom/android/gallery3d/app/SinglePhotoDataAdapter;->onDecodeThumbComplete(Lcom/android/gallery3d/util/Future;)V

    return-void
.end method

.method static synthetic access$100(Lcom/android/gallery3d/app/SinglePhotoDataAdapter;Lcom/android/gallery3d/util/Future;)V
    .locals 0
    .param p0    # Lcom/android/gallery3d/app/SinglePhotoDataAdapter;
    .param p1    # Lcom/android/gallery3d/util/Future;

    invoke-direct {p0, p1}, Lcom/android/gallery3d/app/SinglePhotoDataAdapter;->onDecodeSecondThumbComplete(Lcom/android/gallery3d/util/Future;)V

    return-void
.end method

.method static synthetic access$1000(Lcom/android/gallery3d/app/SinglePhotoDataAdapter;)I
    .locals 1
    .param p0    # Lcom/android/gallery3d/app/SinglePhotoDataAdapter;

    iget v0, p0, Lcom/android/gallery3d/app/SinglePhotoDataAdapter;->mTotalFrameCount:I

    return v0
.end method

.method static synthetic access$1100(Lcom/android/gallery3d/app/SinglePhotoDataAdapter;)Landroid/graphics/Bitmap;
    .locals 1
    .param p0    # Lcom/android/gallery3d/app/SinglePhotoDataAdapter;

    iget-object v0, p0, Lcom/android/gallery3d/app/SinglePhotoDataAdapter;->mCurrentGifFrame:Landroid/graphics/Bitmap;

    return-object v0
.end method

.method static synthetic access$1200(Lcom/android/gallery3d/app/SinglePhotoDataAdapter;Landroid/graphics/Bitmap;)V
    .locals 0
    .param p0    # Lcom/android/gallery3d/app/SinglePhotoDataAdapter;
    .param p1    # Landroid/graphics/Bitmap;

    invoke-direct {p0, p1}, Lcom/android/gallery3d/app/SinglePhotoDataAdapter;->updateGifFrame(Landroid/graphics/Bitmap;)V

    return-void
.end method

.method static synthetic access$1300(Lcom/android/gallery3d/app/SinglePhotoDataAdapter;Lcom/android/gallery3d/util/Future;Lcom/android/gallery3d/data/MediaItem;I)V
    .locals 0
    .param p0    # Lcom/android/gallery3d/app/SinglePhotoDataAdapter;
    .param p1    # Lcom/android/gallery3d/util/Future;
    .param p2    # Lcom/android/gallery3d/data/MediaItem;
    .param p3    # I

    invoke-direct {p0, p1, p2, p3}, Lcom/android/gallery3d/app/SinglePhotoDataAdapter;->updateMavDecoder(Lcom/android/gallery3d/util/Future;Lcom/android/gallery3d/data/MediaItem;I)V

    return-void
.end method

.method static synthetic access$1400(Lcom/android/gallery3d/app/SinglePhotoDataAdapter;)Z
    .locals 1
    .param p0    # Lcom/android/gallery3d/app/SinglePhotoDataAdapter;

    iget-boolean v0, p0, Lcom/android/gallery3d/app/SinglePhotoDataAdapter;->mIsMavStereoMode:Z

    return v0
.end method

.method static synthetic access$1402(Lcom/android/gallery3d/app/SinglePhotoDataAdapter;Z)Z
    .locals 0
    .param p0    # Lcom/android/gallery3d/app/SinglePhotoDataAdapter;
    .param p1    # Z

    iput-boolean p1, p0, Lcom/android/gallery3d/app/SinglePhotoDataAdapter;->mIsMavStereoMode:Z

    return p1
.end method

.method static synthetic access$1500()Z
    .locals 1

    sget-boolean v0, Lcom/android/gallery3d/app/SinglePhotoDataAdapter;->mIsStereoDisplaySupported:Z

    return v0
.end method

.method static synthetic access$1600(Lcom/android/gallery3d/app/SinglePhotoDataAdapter;)I
    .locals 1
    .param p0    # Lcom/android/gallery3d/app/SinglePhotoDataAdapter;

    iget v0, p0, Lcom/android/gallery3d/app/SinglePhotoDataAdapter;->mCurrentMpoIndex:I

    return v0
.end method

.method static synthetic access$1700(Lcom/android/gallery3d/app/SinglePhotoDataAdapter;)[Lcom/android/gallery3d/ui/ScreenNail;
    .locals 1
    .param p0    # Lcom/android/gallery3d/app/SinglePhotoDataAdapter;

    iget-object v0, p0, Lcom/android/gallery3d/app/SinglePhotoDataAdapter;->mpoFrames:[Lcom/android/gallery3d/ui/ScreenNail;

    return-object v0
.end method

.method static synthetic access$1800(Lcom/android/gallery3d/app/SinglePhotoDataAdapter;)Lcom/android/gallery3d/ui/ScreenNail;
    .locals 1
    .param p0    # Lcom/android/gallery3d/app/SinglePhotoDataAdapter;

    iget-object v0, p0, Lcom/android/gallery3d/app/SinglePhotoDataAdapter;->mCurrentScreenNail:Lcom/android/gallery3d/ui/ScreenNail;

    return-object v0
.end method

.method static synthetic access$1802(Lcom/android/gallery3d/app/SinglePhotoDataAdapter;Lcom/android/gallery3d/ui/ScreenNail;)Lcom/android/gallery3d/ui/ScreenNail;
    .locals 0
    .param p0    # Lcom/android/gallery3d/app/SinglePhotoDataAdapter;
    .param p1    # Lcom/android/gallery3d/ui/ScreenNail;

    iput-object p1, p0, Lcom/android/gallery3d/app/SinglePhotoDataAdapter;->mCurrentScreenNail:Lcom/android/gallery3d/ui/ScreenNail;

    return-object p1
.end method

.method static synthetic access$1900(Lcom/android/gallery3d/app/SinglePhotoDataAdapter;)Lcom/android/gallery3d/ui/PhotoView;
    .locals 1
    .param p0    # Lcom/android/gallery3d/app/SinglePhotoDataAdapter;

    iget-object v0, p0, Lcom/android/gallery3d/app/SinglePhotoDataAdapter;->mPhotoView:Lcom/android/gallery3d/ui/PhotoView;

    return-object v0
.end method

.method static synthetic access$200(Lcom/android/gallery3d/app/SinglePhotoDataAdapter;Lcom/android/gallery3d/app/SinglePhotoDataAdapter$ImageBundle;)V
    .locals 0
    .param p0    # Lcom/android/gallery3d/app/SinglePhotoDataAdapter;
    .param p1    # Lcom/android/gallery3d/app/SinglePhotoDataAdapter$ImageBundle;

    invoke-direct {p0, p1}, Lcom/android/gallery3d/app/SinglePhotoDataAdapter;->onDecodeLargeComplete(Lcom/android/gallery3d/app/SinglePhotoDataAdapter$ImageBundle;)V

    return-void
.end method

.method static synthetic access$2000(Lcom/android/gallery3d/app/SinglePhotoDataAdapter;)Ljava/lang/Object;
    .locals 1
    .param p0    # Lcom/android/gallery3d/app/SinglePhotoDataAdapter;

    iget-object v0, p0, Lcom/android/gallery3d/app/SinglePhotoDataAdapter;->mRenderLock:Ljava/lang/Object;

    return-object v0
.end method

.method static synthetic access$300(Lcom/android/gallery3d/app/SinglePhotoDataAdapter;)I
    .locals 1
    .param p0    # Lcom/android/gallery3d/app/SinglePhotoDataAdapter;

    iget v0, p0, Lcom/android/gallery3d/app/SinglePhotoDataAdapter;->mLoadingState:I

    return v0
.end method

.method static synthetic access$400(Lcom/android/gallery3d/app/SinglePhotoDataAdapter;)Lcom/android/gallery3d/data/MediaItem;
    .locals 1
    .param p0    # Lcom/android/gallery3d/app/SinglePhotoDataAdapter;

    iget-object v0, p0, Lcom/android/gallery3d/app/SinglePhotoDataAdapter;->mItem:Lcom/android/gallery3d/data/MediaItem;

    return-object v0
.end method

.method static synthetic access$500(Lcom/android/gallery3d/app/SinglePhotoDataAdapter;)Landroid/os/Handler;
    .locals 1
    .param p0    # Lcom/android/gallery3d/app/SinglePhotoDataAdapter;

    iget-object v0, p0, Lcom/android/gallery3d/app/SinglePhotoDataAdapter;->mHandler:Landroid/os/Handler;

    return-object v0
.end method

.method static synthetic access$600(Lcom/android/gallery3d/app/SinglePhotoDataAdapter;Lcom/android/gallery3d/util/Future;)V
    .locals 0
    .param p0    # Lcom/android/gallery3d/app/SinglePhotoDataAdapter;
    .param p1    # Lcom/android/gallery3d/util/Future;

    invoke-direct {p0, p1}, Lcom/android/gallery3d/app/SinglePhotoDataAdapter;->startGifAnimation(Lcom/android/gallery3d/util/Future;)V

    return-void
.end method

.method static synthetic access$700(Lcom/android/gallery3d/app/SinglePhotoDataAdapter;)Z
    .locals 1
    .param p0    # Lcom/android/gallery3d/app/SinglePhotoDataAdapter;

    iget-boolean v0, p0, Lcom/android/gallery3d/app/SinglePhotoDataAdapter;->mIsActive:Z

    return v0
.end method

.method static synthetic access$800(Lcom/android/gallery3d/app/SinglePhotoDataAdapter;)Lcom/mediatek/gallery3d/gif/GifDecoderWrapper;
    .locals 1
    .param p0    # Lcom/android/gallery3d/app/SinglePhotoDataAdapter;

    iget-object v0, p0, Lcom/android/gallery3d/app/SinglePhotoDataAdapter;->mGifDecoder:Lcom/mediatek/gallery3d/gif/GifDecoderWrapper;

    return-object v0
.end method

.method static synthetic access$900(Lcom/android/gallery3d/app/SinglePhotoDataAdapter;)I
    .locals 1
    .param p0    # Lcom/android/gallery3d/app/SinglePhotoDataAdapter;

    iget v0, p0, Lcom/android/gallery3d/app/SinglePhotoDataAdapter;->mCurrentFrameNum:I

    return v0
.end method

.method static synthetic access$902(Lcom/android/gallery3d/app/SinglePhotoDataAdapter;I)I
    .locals 0
    .param p0    # Lcom/android/gallery3d/app/SinglePhotoDataAdapter;
    .param p1    # I

    iput p1, p0, Lcom/android/gallery3d/app/SinglePhotoDataAdapter;->mCurrentFrameNum:I

    return p1
.end method

.method private getScreenNails([Landroid/graphics/Bitmap;)[Lcom/android/gallery3d/ui/ScreenNail;
    .locals 7
    .param p1    # [Landroid/graphics/Bitmap;

    if-nez p1, :cond_1

    const/4 v3, 0x0

    :cond_0
    return-object v3

    :cond_1
    array-length v1, p1

    new-array v3, v1, [Lcom/android/gallery3d/ui/ScreenNail;

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v1, :cond_0

    aget-object v4, p1, v0

    if-nez v4, :cond_2

    const-string v4, "Gallery2/SinglePhotoDataAdapter"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "getScreenNails: bmps["

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "] is null"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/mediatek/gallery3d/util/MtkLog;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_2
    const/4 v4, 0x0

    invoke-virtual {p0, v4}, Lcom/android/gallery3d/app/SinglePhotoDataAdapter;->getMediaItem(I)Lcom/android/gallery3d/data/MediaItem;

    move-result-object v4

    aget-object v5, p1, v0

    invoke-static {v4, v5}, Lcom/mediatek/gallery3d/util/MediatekFeature;->getMtkScreenNail(Lcom/android/gallery3d/data/MediaItem;Landroid/graphics/Bitmap;)Lcom/android/gallery3d/ui/ScreenNail;

    move-result-object v2

    if-nez v2, :cond_3

    new-instance v2, Lcom/android/gallery3d/ui/BitmapScreenNail;

    aget-object v4, p1, v0

    invoke-direct {v2, v4}, Lcom/android/gallery3d/ui/BitmapScreenNail;-><init>(Landroid/graphics/Bitmap;)V

    :cond_3
    aput-object v2, v3, v0

    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method

.method private onDecodeLargeComplete(Lcom/android/gallery3d/app/SinglePhotoDataAdapter$ImageBundle;)V
    .locals 6
    .param p1    # Lcom/android/gallery3d/app/SinglePhotoDataAdapter$ImageBundle;

    :try_start_0
    iget-object v4, p1, Lcom/android/gallery3d/app/SinglePhotoDataAdapter$ImageBundle;->decoder:Landroid/graphics/BitmapRegionDecoder;

    invoke-virtual {v4}, Landroid/graphics/BitmapRegionDecoder;->getWidth()I

    move-result v1

    iget-object v4, p1, Lcom/android/gallery3d/app/SinglePhotoDataAdapter$ImageBundle;->decoder:Landroid/graphics/BitmapRegionDecoder;

    invoke-virtual {v4}, Landroid/graphics/BitmapRegionDecoder;->getHeight()I

    move-result v0

    const/4 v4, 0x1

    iget-object v5, p0, Lcom/android/gallery3d/app/SinglePhotoDataAdapter;->mItem:Lcom/android/gallery3d/data/MediaItem;

    invoke-virtual {v5}, Lcom/android/gallery3d/data/MediaItem;->getStereoLayout()I

    move-result v5

    invoke-static {v4, v5, v1}, Lcom/mediatek/gallery3d/stereo/StereoHelper;->adjustDim(ZII)I

    move-result v1

    const/4 v4, 0x0

    iget-object v5, p0, Lcom/android/gallery3d/app/SinglePhotoDataAdapter;->mItem:Lcom/android/gallery3d/data/MediaItem;

    invoke-virtual {v5}, Lcom/android/gallery3d/data/MediaItem;->getStereoLayout()I

    move-result v5

    invoke-static {v4, v5, v0}, Lcom/mediatek/gallery3d/stereo/StereoHelper;->adjustDim(ZII)I

    move-result v0

    iget-object v4, p0, Lcom/android/gallery3d/app/SinglePhotoDataAdapter;->mItem:Lcom/android/gallery3d/data/MediaItem;

    iget-object v5, p1, Lcom/android/gallery3d/app/SinglePhotoDataAdapter$ImageBundle;->backupImage:Landroid/graphics/Bitmap;

    invoke-static {v4, v5}, Lcom/mediatek/gallery3d/util/MediatekFeature;->getMtkScreenNail(Lcom/android/gallery3d/data/MediaItem;Landroid/graphics/Bitmap;)Lcom/android/gallery3d/ui/ScreenNail;

    move-result-object v2

    if-eqz v2, :cond_0

    iget-object v4, p1, Lcom/android/gallery3d/app/SinglePhotoDataAdapter$ImageBundle;->decoder:Landroid/graphics/BitmapRegionDecoder;

    invoke-virtual {p0, v4, v2, v1, v0}, Lcom/android/gallery3d/ui/TileImageViewAdapter;->setRegionDecoder(Landroid/graphics/BitmapRegionDecoder;Lcom/android/gallery3d/ui/ScreenNail;II)V

    :goto_0
    iget-object v4, p0, Lcom/android/gallery3d/app/SinglePhotoDataAdapter;->mPhotoView:Lcom/android/gallery3d/ui/PhotoView;

    const/4 v5, 0x0

    invoke-virtual {v4, v5}, Lcom/android/gallery3d/ui/PhotoView;->notifyImageChange(I)V

    :goto_1
    return-void

    :cond_0
    iget-object v4, p1, Lcom/android/gallery3d/app/SinglePhotoDataAdapter$ImageBundle;->decoder:Landroid/graphics/BitmapRegionDecoder;

    iget-object v5, p1, Lcom/android/gallery3d/app/SinglePhotoDataAdapter$ImageBundle;->backupImage:Landroid/graphics/Bitmap;

    invoke-virtual {p0, v4, v5, v1, v0}, Lcom/android/gallery3d/ui/TileImageViewAdapter;->setRegionDecoder(Landroid/graphics/BitmapRegionDecoder;Landroid/graphics/Bitmap;II)V
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v3

    const-string v4, "Gallery2/SinglePhotoDataAdapter"

    const-string v5, "fail to decode large"

    invoke-static {v4, v5, v3}, Lcom/android/gallery3d/app/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_1
.end method

.method private onDecodeSecondThumbComplete(Lcom/android/gallery3d/util/Future;)V
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/android/gallery3d/util/Future",
            "<",
            "Lcom/mediatek/gallery3d/util/MediatekFeature$DataBundle;",
            ">;)V"
        }
    .end annotation

    const/4 v6, 0x2

    const/4 v5, 0x1

    :try_start_0
    invoke-interface {p1}, Lcom/android/gallery3d/util/Future;->get()Ljava/lang/Object;

    move-result-object v2

    if-eqz v2, :cond_1

    invoke-interface {p1}, Lcom/android/gallery3d/util/Future;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/mediatek/gallery3d/util/MediatekFeature$DataBundle;

    iget-object v0, v2, Lcom/mediatek/gallery3d/util/MediatekFeature$DataBundle;->secondFrame:Landroid/graphics/Bitmap;

    :goto_0
    const-string v2, "Gallery2/SinglePhotoDataAdapter"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "onDecodeSecondThumbComplete:second="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/android/gallery3d/app/Log;->i(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-nez v0, :cond_2

    iget-boolean v2, p0, Lcom/android/gallery3d/app/SinglePhotoDataAdapter;->mHasFullImage:Z

    if-eqz v2, :cond_0

    iget v2, p0, Lcom/android/gallery3d/app/SinglePhotoDataAdapter;->mLoadingState:I

    if-eq v2, v6, :cond_0

    iput-boolean v5, p0, Lcom/android/gallery3d/app/SinglePhotoDataAdapter;->mShowedThumb:Z

    iget-object v2, p0, Lcom/android/gallery3d/app/SinglePhotoDataAdapter;->mThreadPool:Lcom/android/gallery3d/util/ThreadPool;

    iget-object v3, p0, Lcom/android/gallery3d/app/SinglePhotoDataAdapter;->mItem:Lcom/android/gallery3d/data/MediaItem;

    invoke-virtual {v3}, Lcom/android/gallery3d/data/MediaItem;->requestLargeImage()Lcom/android/gallery3d/util/ThreadPool$Job;

    move-result-object v3

    iget-object v4, p0, Lcom/android/gallery3d/app/SinglePhotoDataAdapter;->mLargeListener:Lcom/android/gallery3d/util/FutureListener;

    invoke-virtual {v2, v3, v4}, Lcom/android/gallery3d/util/ThreadPool;->submit(Lcom/android/gallery3d/util/ThreadPool$Job;Lcom/android/gallery3d/util/FutureListener;)Lcom/android/gallery3d/util/Future;

    move-result-object v2

    :goto_1
    iput-object v2, p0, Lcom/android/gallery3d/app/SinglePhotoDataAdapter;->mTask:Lcom/android/gallery3d/util/Future;

    :cond_0
    return-void

    :cond_1
    const/4 v0, 0x0

    goto :goto_0

    :cond_2
    const/4 v3, 0x1

    :try_start_1
    invoke-interface {p1}, Lcom/android/gallery3d/util/Future;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/mediatek/gallery3d/util/MediatekFeature$DataBundle;

    iget-object v2, v2, Lcom/mediatek/gallery3d/util/MediatekFeature$DataBundle;->firstFrame:Landroid/graphics/Bitmap;

    invoke-virtual {p0, v3, v2}, Lcom/android/gallery3d/ui/TileImageViewAdapter;->setStereoScreenNail(ILandroid/graphics/Bitmap;)V

    const/4 v2, 0x2

    invoke-virtual {p0, v2, v0}, Lcom/android/gallery3d/ui/TileImageViewAdapter;->setStereoScreenNail(ILandroid/graphics/Bitmap;)V

    iget-object v2, p0, Lcom/android/gallery3d/app/SinglePhotoDataAdapter;->mPhotoView:Lcom/android/gallery3d/ui/PhotoView;

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Lcom/android/gallery3d/ui/PhotoView;->notifyImageChange(I)V

    invoke-direct {p0}, Lcom/android/gallery3d/app/SinglePhotoDataAdapter;->resetStereoMode()V
    :try_end_1
    .catch Ljava/lang/Throwable; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    iget-boolean v2, p0, Lcom/android/gallery3d/app/SinglePhotoDataAdapter;->mHasFullImage:Z

    if-eqz v2, :cond_0

    iget v2, p0, Lcom/android/gallery3d/app/SinglePhotoDataAdapter;->mLoadingState:I

    if-eq v2, v6, :cond_0

    iput-boolean v5, p0, Lcom/android/gallery3d/app/SinglePhotoDataAdapter;->mShowedThumb:Z

    iget-object v2, p0, Lcom/android/gallery3d/app/SinglePhotoDataAdapter;->mThreadPool:Lcom/android/gallery3d/util/ThreadPool;

    iget-object v3, p0, Lcom/android/gallery3d/app/SinglePhotoDataAdapter;->mItem:Lcom/android/gallery3d/data/MediaItem;

    invoke-virtual {v3}, Lcom/android/gallery3d/data/MediaItem;->requestLargeImage()Lcom/android/gallery3d/util/ThreadPool$Job;

    move-result-object v3

    iget-object v4, p0, Lcom/android/gallery3d/app/SinglePhotoDataAdapter;->mLargeListener:Lcom/android/gallery3d/util/FutureListener;

    invoke-virtual {v2, v3, v4}, Lcom/android/gallery3d/util/ThreadPool;->submit(Lcom/android/gallery3d/util/ThreadPool$Job;Lcom/android/gallery3d/util/FutureListener;)Lcom/android/gallery3d/util/Future;

    move-result-object v2

    goto :goto_1

    :catch_0
    move-exception v1

    :try_start_2
    const-string v2, "Gallery2/SinglePhotoDataAdapter"

    const-string v3, "fail to decode thumb"

    invoke-static {v2, v3, v1}, Lcom/android/gallery3d/app/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    iget-boolean v2, p0, Lcom/android/gallery3d/app/SinglePhotoDataAdapter;->mHasFullImage:Z

    if-eqz v2, :cond_0

    iget v2, p0, Lcom/android/gallery3d/app/SinglePhotoDataAdapter;->mLoadingState:I

    if-eq v2, v6, :cond_0

    iput-boolean v5, p0, Lcom/android/gallery3d/app/SinglePhotoDataAdapter;->mShowedThumb:Z

    iget-object v2, p0, Lcom/android/gallery3d/app/SinglePhotoDataAdapter;->mThreadPool:Lcom/android/gallery3d/util/ThreadPool;

    iget-object v3, p0, Lcom/android/gallery3d/app/SinglePhotoDataAdapter;->mItem:Lcom/android/gallery3d/data/MediaItem;

    invoke-virtual {v3}, Lcom/android/gallery3d/data/MediaItem;->requestLargeImage()Lcom/android/gallery3d/util/ThreadPool$Job;

    move-result-object v3

    iget-object v4, p0, Lcom/android/gallery3d/app/SinglePhotoDataAdapter;->mLargeListener:Lcom/android/gallery3d/util/FutureListener;

    invoke-virtual {v2, v3, v4}, Lcom/android/gallery3d/util/ThreadPool;->submit(Lcom/android/gallery3d/util/ThreadPool$Job;Lcom/android/gallery3d/util/FutureListener;)Lcom/android/gallery3d/util/Future;

    move-result-object v2

    goto :goto_1

    :catchall_0
    move-exception v2

    iget-boolean v3, p0, Lcom/android/gallery3d/app/SinglePhotoDataAdapter;->mHasFullImage:Z

    if-eqz v3, :cond_3

    iget v3, p0, Lcom/android/gallery3d/app/SinglePhotoDataAdapter;->mLoadingState:I

    if-eq v3, v6, :cond_3

    iput-boolean v5, p0, Lcom/android/gallery3d/app/SinglePhotoDataAdapter;->mShowedThumb:Z

    iget-object v3, p0, Lcom/android/gallery3d/app/SinglePhotoDataAdapter;->mThreadPool:Lcom/android/gallery3d/util/ThreadPool;

    iget-object v4, p0, Lcom/android/gallery3d/app/SinglePhotoDataAdapter;->mItem:Lcom/android/gallery3d/data/MediaItem;

    invoke-virtual {v4}, Lcom/android/gallery3d/data/MediaItem;->requestLargeImage()Lcom/android/gallery3d/util/ThreadPool$Job;

    move-result-object v4

    iget-object v5, p0, Lcom/android/gallery3d/app/SinglePhotoDataAdapter;->mLargeListener:Lcom/android/gallery3d/util/FutureListener;

    invoke-virtual {v3, v4, v5}, Lcom/android/gallery3d/util/ThreadPool;->submit(Lcom/android/gallery3d/util/ThreadPool$Job;Lcom/android/gallery3d/util/FutureListener;)Lcom/android/gallery3d/util/Future;

    move-result-object v3

    iput-object v3, p0, Lcom/android/gallery3d/app/SinglePhotoDataAdapter;->mTask:Lcom/android/gallery3d/util/Future;

    :cond_3
    throw v2
.end method

.method private onDecodeThumbComplete(Lcom/android/gallery3d/util/Future;)V
    .locals 10
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/android/gallery3d/util/Future",
            "<",
            "Landroid/graphics/Bitmap;",
            ">;)V"
        }
    .end annotation

    const/4 v9, 0x2

    :try_start_0
    invoke-interface {p1}, Lcom/android/gallery3d/util/Future;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/graphics/Bitmap;

    if-nez v0, :cond_1

    const/4 v7, 0x2

    iput v7, p0, Lcom/android/gallery3d/app/SinglePhotoDataAdapter;->mLoadingState:I

    :cond_0
    :goto_0
    return-void

    :cond_1
    const/4 v7, 0x1

    iput v7, p0, Lcom/android/gallery3d/app/SinglePhotoDataAdapter;->mLoadingState:I

    iget-object v7, p0, Lcom/android/gallery3d/app/SinglePhotoDataAdapter;->mItem:Lcom/android/gallery3d/data/MediaItem;

    invoke-static {v7, v0}, Lcom/mediatek/gallery3d/util/MediatekFeature;->getMtkScreenNail(Lcom/android/gallery3d/data/MediaItem;Landroid/graphics/Bitmap;)Lcom/android/gallery3d/ui/ScreenNail;

    move-result-object v3

    if-eqz v3, :cond_3

    const/4 v6, 0x0

    const/4 v1, 0x0

    invoke-static {v3}, Lcom/mediatek/gallery3d/util/MediatekFeature;->getSizeForSubtype(Lcom/android/gallery3d/ui/ScreenNail;)Lcom/android/gallery3d/ui/PhotoView$Size;

    move-result-object v4

    if-eqz v4, :cond_2

    iget v6, v4, Lcom/android/gallery3d/ui/PhotoView$Size;->width:I

    iget v1, v4, Lcom/android/gallery3d/ui/PhotoView$Size;->height:I

    :goto_1
    invoke-virtual {p0, v3, v6, v1}, Lcom/android/gallery3d/ui/TileImageViewAdapter;->setScreenNail(Lcom/android/gallery3d/ui/ScreenNail;II)V

    :goto_2
    iget-object v7, p0, Lcom/android/gallery3d/app/SinglePhotoDataAdapter;->mPhotoView:Lcom/android/gallery3d/ui/PhotoView;

    const/4 v8, 0x0

    invoke-virtual {v7, v8}, Lcom/android/gallery3d/ui/PhotoView;->notifyImageChange(I)V

    iget-boolean v7, p0, Lcom/android/gallery3d/app/SinglePhotoDataAdapter;->mShowStereoImage:Z

    if-eqz v7, :cond_4

    new-instance v2, Lcom/mediatek/gallery3d/util/MediatekFeature$Params;

    invoke-direct {v2}, Lcom/mediatek/gallery3d/util/MediatekFeature$Params;-><init>()V

    const/4 v7, 0x0

    iput-boolean v7, v2, Lcom/mediatek/gallery3d/util/MediatekFeature$Params;->inOriginalFrame:Z

    const/4 v7, 0x1

    iput-boolean v7, v2, Lcom/mediatek/gallery3d/util/MediatekFeature$Params;->inFirstFrame:Z

    const/4 v7, 0x1

    iput-boolean v7, v2, Lcom/mediatek/gallery3d/util/MediatekFeature$Params;->inSecondFrame:Z

    const/4 v7, 0x1

    invoke-static {v2, v7}, Lcom/mediatek/gallery3d/util/MediatekFeature;->enablePictureQualityEnhance(Lcom/mediatek/gallery3d/util/MediatekFeature$Params;Z)V

    const-string v7, "Gallery2/SinglePhotoDataAdapter"

    const-string v8, "onDecodeThumbComplete:start second image task"

    invoke-static {v7, v8}, Lcom/android/gallery3d/app/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v7, p0, Lcom/android/gallery3d/app/SinglePhotoDataAdapter;->mThreadPool:Lcom/android/gallery3d/util/ThreadPool;

    iget-object v8, p0, Lcom/android/gallery3d/app/SinglePhotoDataAdapter;->mItem:Lcom/android/gallery3d/data/MediaItem;

    const/4 v9, 0x1

    invoke-virtual {v8, v9, v2}, Lcom/android/gallery3d/data/MediaItem;->requestImage(ILcom/mediatek/gallery3d/util/MediatekFeature$Params;)Lcom/android/gallery3d/util/ThreadPool$Job;

    move-result-object v8

    iget-object v9, p0, Lcom/android/gallery3d/app/SinglePhotoDataAdapter;->mSecondThumbListener:Lcom/android/gallery3d/util/FutureListener;

    invoke-virtual {v7, v8, v9}, Lcom/android/gallery3d/util/ThreadPool;->submit(Lcom/android/gallery3d/util/ThreadPool$Job;Lcom/android/gallery3d/util/FutureListener;)Lcom/android/gallery3d/util/Future;

    move-result-object v7

    iput-object v7, p0, Lcom/android/gallery3d/app/SinglePhotoDataAdapter;->mTask:Lcom/android/gallery3d/util/Future;
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v5

    const-string v7, "Gallery2/SinglePhotoDataAdapter"

    const-string v8, "fail to decode thumb"

    invoke-static {v7, v8, v5}, Lcom/android/gallery3d/app/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0

    :cond_2
    :try_start_1
    invoke-interface {v3}, Lcom/android/gallery3d/ui/ScreenNail;->getWidth()I

    move-result v6

    invoke-interface {v3}, Lcom/android/gallery3d/ui/ScreenNail;->getHeight()I

    move-result v1

    goto :goto_1

    :cond_3
    new-instance v3, Lcom/android/gallery3d/ui/BitmapScreenNail;

    invoke-direct {v3, v0}, Lcom/android/gallery3d/ui/BitmapScreenNail;-><init>(Landroid/graphics/Bitmap;)V

    invoke-interface {v3}, Lcom/android/gallery3d/ui/ScreenNail;->getWidth()I

    move-result v7

    invoke-interface {v3}, Lcom/android/gallery3d/ui/ScreenNail;->getHeight()I

    move-result v8

    invoke-virtual {p0, v3, v7, v8}, Lcom/android/gallery3d/ui/TileImageViewAdapter;->setScreenNail(Lcom/android/gallery3d/ui/ScreenNail;II)V

    goto :goto_2

    :cond_4
    iget-boolean v7, p0, Lcom/android/gallery3d/app/SinglePhotoDataAdapter;->mHasFullImage:Z

    if-eqz v7, :cond_0

    iget v7, p0, Lcom/android/gallery3d/app/SinglePhotoDataAdapter;->mLoadingState:I

    if-eq v7, v9, :cond_0

    const/4 v7, 0x1

    iput-boolean v7, p0, Lcom/android/gallery3d/app/SinglePhotoDataAdapter;->mShowedThumb:Z

    iget-object v7, p0, Lcom/android/gallery3d/app/SinglePhotoDataAdapter;->mThreadPool:Lcom/android/gallery3d/util/ThreadPool;

    iget-object v8, p0, Lcom/android/gallery3d/app/SinglePhotoDataAdapter;->mItem:Lcom/android/gallery3d/data/MediaItem;

    invoke-virtual {v8}, Lcom/android/gallery3d/data/MediaItem;->requestLargeImage()Lcom/android/gallery3d/util/ThreadPool$Job;

    move-result-object v8

    iget-object v9, p0, Lcom/android/gallery3d/app/SinglePhotoDataAdapter;->mLargeListener:Lcom/android/gallery3d/util/FutureListener;

    invoke-virtual {v7, v8, v9}, Lcom/android/gallery3d/util/ThreadPool;->submit(Lcom/android/gallery3d/util/ThreadPool$Job;Lcom/android/gallery3d/util/FutureListener;)Lcom/android/gallery3d/util/Future;

    move-result-object v7

    iput-object v7, p0, Lcom/android/gallery3d/app/SinglePhotoDataAdapter;->mTask:Lcom/android/gallery3d/util/Future;
    :try_end_1
    .catch Ljava/lang/Throwable; {:try_start_1 .. :try_end_1} :catch_0

    goto/16 :goto_0
.end method

.method private releaseGifResource()V
    .locals 2

    const/4 v1, 0x0

    iput-object v1, p0, Lcom/android/gallery3d/app/SinglePhotoDataAdapter;->mGifDecoder:Lcom/mediatek/gallery3d/gif/GifDecoderWrapper;

    iget-object v0, p0, Lcom/android/gallery3d/app/SinglePhotoDataAdapter;->mCurrentGifFrame:Landroid/graphics/Bitmap;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/gallery3d/app/SinglePhotoDataAdapter;->mCurrentGifFrame:Landroid/graphics/Bitmap;

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->isRecycled()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/android/gallery3d/app/SinglePhotoDataAdapter;->mCurrentGifFrame:Landroid/graphics/Bitmap;

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->recycle()V

    iput-object v1, p0, Lcom/android/gallery3d/app/SinglePhotoDataAdapter;->mCurrentGifFrame:Landroid/graphics/Bitmap;

    :cond_0
    return-void
.end method

.method private requestRender()V
    .locals 5

    const-string v2, "Gallery2/SinglePhotoDataAdapter"

    const-string v3, "requestRender"

    invoke-static {v2, v3}, Lcom/mediatek/gallery3d/util/MtkLog;->d(Ljava/lang/String;Ljava/lang/String;)I

    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v0

    iget-object v3, p0, Lcom/android/gallery3d/app/SinglePhotoDataAdapter;->mRenderLock:Ljava/lang/Object;

    monitor-enter v3

    const/4 v2, 0x1

    :try_start_0
    iput-boolean v2, p0, Lcom/android/gallery3d/app/SinglePhotoDataAdapter;->mRenderRequested:Z

    iget-object v2, p0, Lcom/android/gallery3d/app/SinglePhotoDataAdapter;->mRenderLock:Ljava/lang/Object;

    invoke-virtual {v2}, Ljava/lang/Object;->notifyAll()V

    monitor-exit v3
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v2

    sub-long v0, v2, v0

    const-string v2, "Gallery2/SinglePhotoDataAdapter"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "request render consumed "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "ms"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/android/gallery3d/app/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    return-void

    :catchall_0
    move-exception v2

    :try_start_1
    monitor-exit v3
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v2
.end method

.method private resetStereoMode()V
    .locals 4

    iget-boolean v1, p0, Lcom/android/gallery3d/app/SinglePhotoDataAdapter;->mShowStereoImage:Z

    if-nez v1, :cond_0

    :goto_0
    return-void

    :cond_0
    const/4 v0, 0x0

    const/4 v1, 0x2

    invoke-virtual {p0, v1}, Lcom/android/gallery3d/ui/TileImageViewAdapter;->getStereoScreenNail(I)Lcom/android/gallery3d/ui/ScreenNail;

    move-result-object v1

    if-eqz v1, :cond_1

    const/4 v0, 0x1

    :cond_1
    if-eqz v0, :cond_2

    iget-object v1, p0, Lcom/android/gallery3d/app/SinglePhotoDataAdapter;->mItem:Lcom/android/gallery3d/data/MediaItem;

    invoke-virtual {v1}, Lcom/android/gallery3d/data/MediaObject;->getSupportedOperations()I

    move-result v1

    const/high16 v2, 0x80000

    and-int/2addr v1, v2

    if-eqz v1, :cond_2

    const/4 v0, 0x0

    :cond_2
    const-string v1, "Gallery2/SinglePhotoDataAdapter"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "resetStereoMode:stereoMode="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/android/gallery3d/app/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v1, p0, Lcom/android/gallery3d/app/SinglePhotoDataAdapter;->mPhotoView:Lcom/android/gallery3d/ui/PhotoView;

    invoke-virtual {v1, v0}, Lcom/android/gallery3d/ui/PhotoView;->allowStereoMode(Z)V

    iget-object v1, p0, Lcom/android/gallery3d/app/SinglePhotoDataAdapter;->mPhotoView:Lcom/android/gallery3d/ui/PhotoView;

    invoke-virtual {v1, v0}, Lcom/android/gallery3d/ui/PhotoView;->setStereoMode(Z)V

    goto :goto_0
.end method

.method private startGifAnimation(Lcom/android/gallery3d/util/Future;)V
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/android/gallery3d/util/Future",
            "<",
            "Lcom/mediatek/gallery3d/util/MediatekFeature$DataBundle;",
            ">;)V"
        }
    .end annotation

    const/4 v1, 0x1

    const/4 v2, 0x0

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/android/gallery3d/app/SinglePhotoDataAdapter;->mGifTask:Lcom/android/gallery3d/util/Future;

    invoke-interface {p1}, Lcom/android/gallery3d/util/Future;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/mediatek/gallery3d/util/MediatekFeature$DataBundle;

    iget-object v0, v0, Lcom/mediatek/gallery3d/util/MediatekFeature$DataBundle;->gifDecoder:Lcom/mediatek/gallery3d/gif/GifDecoderWrapper;

    iput-object v0, p0, Lcom/android/gallery3d/app/SinglePhotoDataAdapter;->mGifDecoder:Lcom/mediatek/gallery3d/gif/GifDecoderWrapper;

    iget-object v0, p0, Lcom/android/gallery3d/app/SinglePhotoDataAdapter;->mGifDecoder:Lcom/mediatek/gallery3d/gif/GifDecoderWrapper;

    if-eqz v0, :cond_0

    iput v2, p0, Lcom/android/gallery3d/app/SinglePhotoDataAdapter;->mCurrentFrameNum:I

    iget-object v0, p0, Lcom/android/gallery3d/app/SinglePhotoDataAdapter;->mGifDecoder:Lcom/mediatek/gallery3d/gif/GifDecoderWrapper;

    invoke-virtual {v0}, Lcom/mediatek/gallery3d/gif/GifDecoderWrapper;->getTotalFrameCount()I

    move-result v0

    iput v0, p0, Lcom/android/gallery3d/app/SinglePhotoDataAdapter;->mTotalFrameCount:I

    iget v0, p0, Lcom/android/gallery3d/app/SinglePhotoDataAdapter;->mTotalFrameCount:I

    if-gt v0, v1, :cond_1

    const-string v0, "SinglePhotoDataAdapter"

    const-string v1, "invalid frame count, NO animation!"

    invoke-static {v0, v1}, Lcom/android/gallery3d/app/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v0, p0, Lcom/android/gallery3d/app/SinglePhotoDataAdapter;->mGifDecoder:Lcom/mediatek/gallery3d/gif/GifDecoderWrapper;

    invoke-virtual {v0}, Lcom/mediatek/gallery3d/gif/GifDecoderWrapper;->getWidth()I

    move-result v0

    iget-object v3, p0, Lcom/android/gallery3d/app/SinglePhotoDataAdapter;->mGifDecoder:Lcom/mediatek/gallery3d/gif/GifDecoderWrapper;

    invoke-virtual {v3}, Lcom/mediatek/gallery3d/gif/GifDecoderWrapper;->getHeight()I

    move-result v3

    sget-object v4, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    invoke-static {v0, v3, v4}, Landroid/graphics/Bitmap;->createBitmap(IILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;

    move-result-object v0

    iput-object v0, p0, Lcom/android/gallery3d/app/SinglePhotoDataAdapter;->mCurrentGifFrame:Landroid/graphics/Bitmap;

    iget-object v0, p0, Lcom/android/gallery3d/app/SinglePhotoDataAdapter;->mCurrentGifFrame:Landroid/graphics/Bitmap;

    if-eqz v0, :cond_2

    move v0, v1

    :goto_1
    invoke-static {v0}, Lcom/android/gallery3d/common/Utils;->assertTrue(Z)V

    iget-object v0, p0, Lcom/android/gallery3d/app/SinglePhotoDataAdapter;->mHandler:Landroid/os/Handler;

    iget-object v1, p0, Lcom/android/gallery3d/app/SinglePhotoDataAdapter;->mHandler:Landroid/os/Handler;

    const/4 v2, 0x2

    new-instance v3, Lcom/android/gallery3d/app/SinglePhotoDataAdapter$GifAnimationRunnable;

    invoke-direct {v3, p0}, Lcom/android/gallery3d/app/SinglePhotoDataAdapter$GifAnimationRunnable;-><init>(Lcom/android/gallery3d/app/SinglePhotoDataAdapter;)V

    invoke-virtual {v1, v2, v3}, Landroid/os/Handler;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    goto :goto_0

    :cond_2
    move v0, v2

    goto :goto_1
.end method

.method private startMavPlayback()V
    .locals 4

    const-string v1, "Gallery2/SinglePhotoDataAdapter"

    const-string v2, "startMavPlayback"

    invoke-static {v1, v2}, Lcom/mediatek/gallery3d/util/MtkLog;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget v1, p0, Lcom/android/gallery3d/app/SinglePhotoDataAdapter;->mpoTotalCount:I

    if-lez v1, :cond_1

    iget v1, p0, Lcom/android/gallery3d/app/SinglePhotoDataAdapter;->mpoTotalCount:I

    div-int/lit8 v0, v1, 0x2

    const-string v1, "Gallery2/SinglePhotoDataAdapter"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "the middle frame is "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/mediatek/gallery3d/util/MtkLog;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v1, p0, Lcom/android/gallery3d/app/SinglePhotoDataAdapter;->mMavListener:Lcom/android/gallery3d/app/PhotoDataAdapter$MavListener;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/android/gallery3d/app/SinglePhotoDataAdapter;->mMavListener:Lcom/android/gallery3d/app/PhotoDataAdapter$MavListener;

    iget v2, p0, Lcom/android/gallery3d/app/SinglePhotoDataAdapter;->mpoTotalCount:I

    add-int/lit8 v2, v2, -0x1

    invoke-interface {v1, v2, v0}, Lcom/android/gallery3d/app/PhotoDataAdapter$MavListener;->setSeekBar(II)V

    iget-object v1, p0, Lcom/android/gallery3d/app/SinglePhotoDataAdapter;->mMavListener:Lcom/android/gallery3d/app/PhotoDataAdapter$MavListener;

    const/4 v2, 0x1

    invoke-interface {v1, v2}, Lcom/android/gallery3d/app/PhotoDataAdapter$MavListener;->setStatus(Z)V

    :cond_0
    new-instance v1, Lcom/android/gallery3d/app/SinglePhotoDataAdapter$MavRenderThread;

    invoke-direct {v1, p0}, Lcom/android/gallery3d/app/SinglePhotoDataAdapter$MavRenderThread;-><init>(Lcom/android/gallery3d/app/SinglePhotoDataAdapter;)V

    invoke-virtual {v1}, Ljava/lang/Thread;->start()V

    :goto_0
    return-void

    :cond_1
    const-string v1, "Gallery2/SinglePhotoDataAdapter"

    const-string v2, "mpoTotalCount <= 0"

    invoke-static {v1, v2}, Lcom/mediatek/gallery3d/util/MtkLog;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method private updateGifFrame(Landroid/graphics/Bitmap;)V
    .locals 3
    .param p1    # Landroid/graphics/Bitmap;

    if-nez p1, :cond_0

    :goto_0
    return-void

    :cond_0
    iget-object v1, p0, Lcom/android/gallery3d/app/SinglePhotoDataAdapter;->mItem:Lcom/android/gallery3d/data/MediaItem;

    invoke-static {v1, p1}, Lcom/mediatek/gallery3d/util/MediatekFeature;->getMtkScreenNail(Lcom/android/gallery3d/data/MediaItem;Landroid/graphics/Bitmap;)Lcom/android/gallery3d/ui/ScreenNail;

    move-result-object v0

    if-eqz v0, :cond_1

    invoke-interface {v0}, Lcom/android/gallery3d/ui/ScreenNail;->getWidth()I

    move-result v1

    invoke-interface {v0}, Lcom/android/gallery3d/ui/ScreenNail;->getHeight()I

    move-result v2

    invoke-virtual {p0, v0, v1, v2}, Lcom/android/gallery3d/ui/TileImageViewAdapter;->setScreenNail(Lcom/android/gallery3d/ui/ScreenNail;II)V

    :goto_1
    iget-object v1, p0, Lcom/android/gallery3d/app/SinglePhotoDataAdapter;->mPhotoView:Lcom/android/gallery3d/ui/PhotoView;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Lcom/android/gallery3d/ui/PhotoView;->notifyImageChange(I)V

    goto :goto_0

    :cond_1
    invoke-virtual {p1}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v1

    invoke-virtual {p1}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v2

    invoke-virtual {p0, p1, v1, v2}, Lcom/android/gallery3d/ui/TileImageViewAdapter;->setScreenNail(Landroid/graphics/Bitmap;II)V

    goto :goto_1
.end method

.method private updateMavDecoder(Lcom/android/gallery3d/util/Future;Lcom/android/gallery3d/data/MediaItem;I)V
    .locals 6
    .param p2    # Lcom/android/gallery3d/data/MediaItem;
    .param p3    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/android/gallery3d/util/Future",
            "<",
            "Lcom/mediatek/gallery3d/util/MediatekFeature$DataBundle;",
            ">;",
            "Lcom/android/gallery3d/data/MediaItem;",
            "I)V"
        }
    .end annotation

    const/4 v5, 0x1

    const-string v2, "Gallery2/SinglePhotoDataAdapter"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, ">> updateMavDecoder, type: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/android/gallery3d/app/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    if-nez p3, :cond_2

    invoke-interface {p1}, Lcom/android/gallery3d/util/Future;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/mediatek/gallery3d/util/MediatekFeature$DataBundle;

    iget v2, v2, Lcom/mediatek/gallery3d/util/MediatekFeature$DataBundle;->mpoTotalCount:I

    iput v2, p0, Lcom/android/gallery3d/app/SinglePhotoDataAdapter;->mpoTotalCount:I

    const-string v2, "Gallery2/SinglePhotoDataAdapter"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "the mav total count is "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget v4, p0, Lcom/android/gallery3d/app/SinglePhotoDataAdapter;->mpoTotalCount:I

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/mediatek/gallery3d/util/MtkLog;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v2, p0, Lcom/android/gallery3d/app/SinglePhotoDataAdapter;->mMavListener:Lcom/android/gallery3d/app/PhotoDataAdapter$MavListener;

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/android/gallery3d/app/SinglePhotoDataAdapter;->mMavListener:Lcom/android/gallery3d/app/PhotoDataAdapter$MavListener;

    iget v3, p0, Lcom/android/gallery3d/app/SinglePhotoDataAdapter;->mpoTotalCount:I

    add-int/lit8 v3, v3, -0x1

    const/4 v4, 0x0

    invoke-interface {v2, v3, v4}, Lcom/android/gallery3d/app/PhotoDataAdapter$MavListener;->setSeekBar(II)V

    :cond_0
    new-instance v1, Lcom/mediatek/gallery3d/util/MediatekFeature$Params;

    invoke-direct {v1}, Lcom/mediatek/gallery3d/util/MediatekFeature$Params;-><init>()V

    iput-boolean v5, v1, Lcom/mediatek/gallery3d/util/MediatekFeature$Params;->inMpoFrames:Z

    iget-object v2, p0, Lcom/android/gallery3d/app/SinglePhotoDataAdapter;->mActivity:Lcom/android/gallery3d/app/AbstractGalleryActivity;

    invoke-virtual {v2}, Landroid/app/Activity;->getWindowManager()Landroid/view/WindowManager;

    move-result-object v2

    invoke-interface {v2}, Landroid/view/WindowManager;->getDefaultDisplay()Landroid/view/Display;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/Display;->getHeight()I

    move-result v2

    iput v2, v1, Lcom/mediatek/gallery3d/util/MediatekFeature$Params;->inTargetDisplayHeight:I

    invoke-virtual {v0}, Landroid/view/Display;->getWidth()I

    move-result v2

    iput v2, v1, Lcom/mediatek/gallery3d/util/MediatekFeature$Params;->inTargetDisplayWidth:I

    iput-boolean v5, v1, Lcom/mediatek/gallery3d/util/MediatekFeature$Params;->inPQEnhance:Z

    const-string v2, "Gallery2/SinglePhotoDataAdapter"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "display width: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v0}, Landroid/view/Display;->getWidth()I

    move-result v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ", height: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v0}, Landroid/view/Display;->getHeight()I

    move-result v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/mediatek/gallery3d/util/MtkLog;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v2, p0, Lcom/android/gallery3d/app/SinglePhotoDataAdapter;->mMavListener:Lcom/android/gallery3d/app/PhotoDataAdapter$MavListener;

    invoke-virtual {p2, v2}, Lcom/android/gallery3d/data/MediaItem;->setMavListener(Lcom/android/gallery3d/app/PhotoDataAdapter$MavListener;)V

    const-string v2, "Gallery2/SinglePhotoDataAdapter"

    const-string v3, "start load all mav frames"

    invoke-static {v2, v3}, Lcom/mediatek/gallery3d/util/MtkLog;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v2, p0, Lcom/android/gallery3d/app/SinglePhotoDataAdapter;->mThreadPool:Lcom/android/gallery3d/util/ThreadPool;

    invoke-virtual {p2, v5, v1}, Lcom/android/gallery3d/data/MediaItem;->requestImage(ILcom/mediatek/gallery3d/util/MediatekFeature$Params;)Lcom/android/gallery3d/util/ThreadPool$Job;

    move-result-object v3

    new-instance v4, Lcom/android/gallery3d/app/SinglePhotoDataAdapter$MavDecoderListener;

    invoke-direct {v4, p0, p2, v5}, Lcom/android/gallery3d/app/SinglePhotoDataAdapter$MavDecoderListener;-><init>(Lcom/android/gallery3d/app/SinglePhotoDataAdapter;Lcom/android/gallery3d/data/MediaItem;I)V

    invoke-virtual {v2, v3, v4}, Lcom/android/gallery3d/util/ThreadPool;->submit(Lcom/android/gallery3d/util/ThreadPool$Job;Lcom/android/gallery3d/util/FutureListener;)Lcom/android/gallery3d/util/Future;

    move-result-object v2

    iput-object v2, p0, Lcom/android/gallery3d/app/SinglePhotoDataAdapter;->mMpoDecoderTask:Lcom/android/gallery3d/util/Future;

    :cond_1
    :goto_0
    const-string v2, "Gallery2/SinglePhotoDataAdapter"

    const-string v3, "<< updateMavDecoder"

    invoke-static {v2, v3}, Lcom/android/gallery3d/app/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    return-void

    :cond_2
    if-ne p3, v5, :cond_1

    invoke-interface {p1}, Lcom/android/gallery3d/util/Future;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/mediatek/gallery3d/util/MediatekFeature$DataBundle;

    iget-object v2, v2, Lcom/mediatek/gallery3d/util/MediatekFeature$DataBundle;->mpoFrames:[Landroid/graphics/Bitmap;

    invoke-direct {p0, v2}, Lcom/android/gallery3d/app/SinglePhotoDataAdapter;->getScreenNails([Landroid/graphics/Bitmap;)[Lcom/android/gallery3d/ui/ScreenNail;

    move-result-object v2

    iput-object v2, p0, Lcom/android/gallery3d/app/SinglePhotoDataAdapter;->mpoFrames:[Lcom/android/gallery3d/ui/ScreenNail;

    iput-boolean v5, p0, Lcom/android/gallery3d/app/SinglePhotoDataAdapter;->mIsMavLoadingFinished:Z

    iget-object v2, p0, Lcom/android/gallery3d/app/SinglePhotoDataAdapter;->mPhotoView:Lcom/android/gallery3d/ui/PhotoView;

    iget-boolean v3, p0, Lcom/android/gallery3d/app/SinglePhotoDataAdapter;->mIsMavLoadingFinished:Z

    invoke-virtual {v2, v3}, Lcom/android/gallery3d/ui/PhotoView;->setMavLoadingFinished(Z)V

    invoke-direct {p0}, Lcom/android/gallery3d/app/SinglePhotoDataAdapter;->startMavPlayback()V

    goto :goto_0
.end method


# virtual methods
.method public cancelCurrentMavDecodeTask()V
    .locals 2

    const/4 v1, 0x0

    sget-boolean v0, Lcom/android/gallery3d/app/SinglePhotoDataAdapter;->mIsMavSupported:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/gallery3d/app/SinglePhotoDataAdapter;->mMpoDecoderTask:Lcom/android/gallery3d/util/Future;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/gallery3d/app/SinglePhotoDataAdapter;->mMpoDecoderTask:Lcom/android/gallery3d/util/Future;

    invoke-interface {v0}, Lcom/android/gallery3d/util/Future;->cancel()V

    iput-boolean v1, p0, Lcom/android/gallery3d/app/SinglePhotoDataAdapter;->mIsMavLoadingFinished:Z

    iput v1, p0, Lcom/android/gallery3d/app/SinglePhotoDataAdapter;->mpoTotalCount:I

    :cond_0
    return-void
.end method

.method public enterConsumeMode()V
    .locals 0

    return-void
.end method

.method public enteredConsumeMode()Z
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public getCurrentIndex()I
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public getImageRotation(I)I
    .locals 1
    .param p1    # I

    if-nez p1, :cond_0

    iget-object v0, p0, Lcom/android/gallery3d/app/SinglePhotoDataAdapter;->mItem:Lcom/android/gallery3d/data/MediaItem;

    invoke-virtual {v0}, Lcom/android/gallery3d/data/MediaItem;->getFullImageRotation()I

    move-result v0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getImageSize(ILcom/android/gallery3d/ui/PhotoView$Size;)V
    .locals 1
    .param p1    # I
    .param p2    # Lcom/android/gallery3d/ui/PhotoView$Size;

    const/4 v0, 0x0

    if-nez p1, :cond_0

    iget-object v0, p0, Lcom/android/gallery3d/app/SinglePhotoDataAdapter;->mItem:Lcom/android/gallery3d/data/MediaItem;

    invoke-virtual {v0}, Lcom/android/gallery3d/data/MediaItem;->getWidth()I

    move-result v0

    iput v0, p2, Lcom/android/gallery3d/ui/PhotoView$Size;->width:I

    iget-object v0, p0, Lcom/android/gallery3d/app/SinglePhotoDataAdapter;->mItem:Lcom/android/gallery3d/data/MediaItem;

    invoke-virtual {v0}, Lcom/android/gallery3d/data/MediaItem;->getHeight()I

    move-result v0

    iput v0, p2, Lcom/android/gallery3d/ui/PhotoView$Size;->height:I

    :goto_0
    return-void

    :cond_0
    iput v0, p2, Lcom/android/gallery3d/ui/PhotoView$Size;->width:I

    iput v0, p2, Lcom/android/gallery3d/ui/PhotoView$Size;->height:I

    goto :goto_0
.end method

.method public getLoadingState(I)I
    .locals 1
    .param p1    # I

    iget v0, p0, Lcom/android/gallery3d/app/SinglePhotoDataAdapter;->mLoadingState:I

    return v0
.end method

.method public getMediaItem(I)Lcom/android/gallery3d/data/MediaItem;
    .locals 1
    .param p1    # I

    if-nez p1, :cond_0

    iget-object v0, p0, Lcom/android/gallery3d/app/SinglePhotoDataAdapter;->mItem:Lcom/android/gallery3d/data/MediaItem;

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getScreenNail(I)Lcom/android/gallery3d/ui/ScreenNail;
    .locals 1
    .param p1    # I

    if-nez p1, :cond_0

    invoke-virtual {p0}, Lcom/android/gallery3d/ui/TileImageViewAdapter;->getScreenNail()Lcom/android/gallery3d/ui/ScreenNail;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getTotalFrameCount()I
    .locals 1

    iget v0, p0, Lcom/android/gallery3d/app/SinglePhotoDataAdapter;->mpoTotalCount:I

    return v0
.end method

.method public isCamera(I)Z
    .locals 1
    .param p1    # I

    const/4 v0, 0x0

    return v0
.end method

.method public isDeletable(I)Z
    .locals 1
    .param p1    # I

    const/4 v0, 0x0

    return v0
.end method

.method public isEmpty()Z
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public isMav(I)Z
    .locals 3
    .param p1    # I

    const-string v1, "image/mpo"

    iget-object v2, p0, Lcom/android/gallery3d/app/SinglePhotoDataAdapter;->mItem:Lcom/android/gallery3d/data/MediaItem;

    invoke-virtual {v2}, Lcom/android/gallery3d/data/MediaItem;->getMimeType()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    iget-object v1, p0, Lcom/android/gallery3d/app/SinglePhotoDataAdapter;->mItem:Lcom/android/gallery3d/data/MediaItem;

    invoke-virtual {v1}, Lcom/android/gallery3d/data/MediaItem;->getSubType()I

    move-result v1

    and-int/lit8 v1, v1, 0x2

    if-eqz v1, :cond_0

    const/4 v1, 0x1

    :goto_0
    and-int/2addr v0, v1

    return v0

    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public isMavLoadingFinished()Z
    .locals 1

    iget-boolean v0, p0, Lcom/android/gallery3d/app/SinglePhotoDataAdapter;->mIsMavLoadingFinished:Z

    return v0
.end method

.method public isPanorama(I)Z
    .locals 1
    .param p1    # I

    const/4 v0, 0x0

    return v0
.end method

.method public isStaticCamera(I)Z
    .locals 1
    .param p1    # I

    const/4 v0, 0x0

    return v0
.end method

.method public isVideo(I)Z
    .locals 2
    .param p1    # I

    iget-object v0, p0, Lcom/android/gallery3d/app/SinglePhotoDataAdapter;->mItem:Lcom/android/gallery3d/data/MediaItem;

    invoke-virtual {v0}, Lcom/android/gallery3d/data/MediaObject;->getMediaType()I

    move-result v0

    const/4 v1, 0x4

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public moveTo(I)V
    .locals 1
    .param p1    # I

    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public pause()V
    .locals 5

    const/4 v4, 0x0

    const/4 v3, 0x0

    iput-boolean v3, p0, Lcom/android/gallery3d/app/SinglePhotoDataAdapter;->mIsActive:Z

    iget-object v2, p0, Lcom/android/gallery3d/app/SinglePhotoDataAdapter;->mTask:Lcom/android/gallery3d/util/Future;

    invoke-interface {v2}, Lcom/android/gallery3d/util/Future;->cancel()V

    invoke-interface {v2}, Lcom/android/gallery3d/util/Future;->waitDone()V

    invoke-interface {v2}, Lcom/android/gallery3d/util/Future;->get()Ljava/lang/Object;

    move-result-object v3

    if-nez v3, :cond_0

    iput-object v4, p0, Lcom/android/gallery3d/app/SinglePhotoDataAdapter;->mTask:Lcom/android/gallery3d/util/Future;

    :cond_0
    iget-object v3, p0, Lcom/android/gallery3d/app/SinglePhotoDataAdapter;->mBitmapScreenNail:Lcom/android/gallery3d/ui/BitmapScreenNail;

    if-eqz v3, :cond_1

    iget-object v3, p0, Lcom/android/gallery3d/app/SinglePhotoDataAdapter;->mBitmapScreenNail:Lcom/android/gallery3d/ui/BitmapScreenNail;

    invoke-virtual {v3}, Lcom/android/gallery3d/ui/BitmapScreenNail;->recycle()V

    iput-object v4, p0, Lcom/android/gallery3d/app/SinglePhotoDataAdapter;->mBitmapScreenNail:Lcom/android/gallery3d/ui/BitmapScreenNail;

    :cond_1
    iget-object v2, p0, Lcom/android/gallery3d/app/SinglePhotoDataAdapter;->mGifTask:Lcom/android/gallery3d/util/Future;

    if-eqz v2, :cond_2

    invoke-interface {v2}, Lcom/android/gallery3d/util/Future;->cancel()V

    invoke-interface {v2}, Lcom/android/gallery3d/util/Future;->waitDone()V

    :cond_2
    iput-object v4, p0, Lcom/android/gallery3d/app/SinglePhotoDataAdapter;->mGifTask:Lcom/android/gallery3d/util/Future;

    iget-object v3, p0, Lcom/android/gallery3d/app/SinglePhotoDataAdapter;->mMpoDecoderTask:Lcom/android/gallery3d/util/Future;

    if-eqz v3, :cond_3

    iget-object v3, p0, Lcom/android/gallery3d/app/SinglePhotoDataAdapter;->mMpoDecoderTask:Lcom/android/gallery3d/util/Future;

    invoke-interface {v3}, Lcom/android/gallery3d/util/Future;->cancel()V

    iget-object v3, p0, Lcom/android/gallery3d/app/SinglePhotoDataAdapter;->mMpoDecoderTask:Lcom/android/gallery3d/util/Future;

    invoke-interface {v3}, Lcom/android/gallery3d/util/Future;->waitDone()V

    :cond_3
    iget-object v3, p0, Lcom/android/gallery3d/app/SinglePhotoDataAdapter;->mpoFrames:[Lcom/android/gallery3d/ui/ScreenNail;

    if-eqz v3, :cond_6

    iget-object v3, p0, Lcom/android/gallery3d/app/SinglePhotoDataAdapter;->mpoFrames:[Lcom/android/gallery3d/ui/ScreenNail;

    array-length v1, v3

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v1, :cond_5

    iget-object v3, p0, Lcom/android/gallery3d/app/SinglePhotoDataAdapter;->mpoFrames:[Lcom/android/gallery3d/ui/ScreenNail;

    aget-object v3, v3, v0

    if-eqz v3, :cond_4

    iget-object v3, p0, Lcom/android/gallery3d/app/SinglePhotoDataAdapter;->mpoFrames:[Lcom/android/gallery3d/ui/ScreenNail;

    aget-object v3, v3, v0

    invoke-interface {v3}, Lcom/android/gallery3d/ui/ScreenNail;->recycle()V

    iget-object v3, p0, Lcom/android/gallery3d/app/SinglePhotoDataAdapter;->mpoFrames:[Lcom/android/gallery3d/ui/ScreenNail;

    aput-object v4, v3, v0

    :cond_4
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_5
    iput-object v4, p0, Lcom/android/gallery3d/app/SinglePhotoDataAdapter;->mpoFrames:[Lcom/android/gallery3d/ui/ScreenNail;

    :cond_6
    return-void
.end method

.method public resume()V
    .locals 6

    const/4 v5, 0x0

    const/4 v4, 0x1

    iput-boolean v4, p0, Lcom/android/gallery3d/app/SinglePhotoDataAdapter;->mIsActive:Z

    iget-object v1, p0, Lcom/android/gallery3d/app/SinglePhotoDataAdapter;->mTask:Lcom/android/gallery3d/util/Future;

    if-eqz v1, :cond_0

    iget-boolean v1, p0, Lcom/android/gallery3d/app/SinglePhotoDataAdapter;->mMavPlayback:Z

    if-eqz v1, :cond_1

    :cond_0
    iput-boolean v5, p0, Lcom/android/gallery3d/app/SinglePhotoDataAdapter;->mShowedThumb:Z

    iget-object v1, p0, Lcom/android/gallery3d/app/SinglePhotoDataAdapter;->mThreadPool:Lcom/android/gallery3d/util/ThreadPool;

    iget-object v2, p0, Lcom/android/gallery3d/app/SinglePhotoDataAdapter;->mItem:Lcom/android/gallery3d/data/MediaItem;

    invoke-virtual {v2, v4}, Lcom/android/gallery3d/data/MediaItem;->requestImage(I)Lcom/android/gallery3d/util/ThreadPool$Job;

    move-result-object v2

    iget-object v3, p0, Lcom/android/gallery3d/app/SinglePhotoDataAdapter;->mThumbListener:Lcom/android/gallery3d/util/FutureListener;

    invoke-virtual {v1, v2, v3}, Lcom/android/gallery3d/util/ThreadPool;->submit(Lcom/android/gallery3d/util/ThreadPool$Job;Lcom/android/gallery3d/util/FutureListener;)Lcom/android/gallery3d/util/Future;

    move-result-object v1

    iput-object v1, p0, Lcom/android/gallery3d/app/SinglePhotoDataAdapter;->mTask:Lcom/android/gallery3d/util/Future;

    :cond_1
    sget-boolean v1, Lcom/android/gallery3d/app/SinglePhotoDataAdapter;->mIsGifAnimationSupported:Z

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/android/gallery3d/app/SinglePhotoDataAdapter;->mGifTask:Lcom/android/gallery3d/util/Future;

    if-nez v1, :cond_2

    iget-boolean v1, p0, Lcom/android/gallery3d/app/SinglePhotoDataAdapter;->mAnimateGif:Z

    if-eqz v1, :cond_2

    new-instance v0, Lcom/mediatek/gallery3d/util/MediatekFeature$Params;

    invoke-direct {v0}, Lcom/mediatek/gallery3d/util/MediatekFeature$Params;-><init>()V

    iput-boolean v5, v0, Lcom/mediatek/gallery3d/util/MediatekFeature$Params;->inOriginalFrame:Z

    iput-boolean v4, v0, Lcom/mediatek/gallery3d/util/MediatekFeature$Params;->inGifDecoder:Z

    const-string v1, "Gallery2/SinglePhotoDataAdapter"

    const-string v2, "resume:start GifDecoder task"

    invoke-static {v1, v2}, Lcom/android/gallery3d/app/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v1, p0, Lcom/android/gallery3d/app/SinglePhotoDataAdapter;->mThreadPool:Lcom/android/gallery3d/util/ThreadPool;

    iget-object v2, p0, Lcom/android/gallery3d/app/SinglePhotoDataAdapter;->mItem:Lcom/android/gallery3d/data/MediaItem;

    invoke-virtual {v2, v4, v0}, Lcom/android/gallery3d/data/MediaItem;->requestImage(ILcom/mediatek/gallery3d/util/MediatekFeature$Params;)Lcom/android/gallery3d/util/ThreadPool$Job;

    move-result-object v2

    new-instance v3, Lcom/android/gallery3d/app/SinglePhotoDataAdapter$GifDecoderListener;

    invoke-direct {v3, p0}, Lcom/android/gallery3d/app/SinglePhotoDataAdapter$GifDecoderListener;-><init>(Lcom/android/gallery3d/app/SinglePhotoDataAdapter;)V

    invoke-virtual {v1, v2, v3}, Lcom/android/gallery3d/util/ThreadPool;->submit(Lcom/android/gallery3d/util/ThreadPool$Job;Lcom/android/gallery3d/util/FutureListener;)Lcom/android/gallery3d/util/Future;

    move-result-object v1

    iput-object v1, p0, Lcom/android/gallery3d/app/SinglePhotoDataAdapter;->mGifTask:Lcom/android/gallery3d/util/Future;

    :cond_2
    sget-boolean v1, Lcom/android/gallery3d/app/SinglePhotoDataAdapter;->mIsMavSupported:Z

    if-eqz v1, :cond_3

    iget-boolean v1, p0, Lcom/android/gallery3d/app/SinglePhotoDataAdapter;->mMavPlayback:Z

    if-eqz v1, :cond_3

    const-string v1, "Gallery2/SinglePhotoDataAdapter"

    const-string v2, "create mav decoder task"

    invoke-static {v1, v2}, Lcom/mediatek/gallery3d/util/MtkLog;->d(Ljava/lang/String;Ljava/lang/String;)I

    new-instance v0, Lcom/mediatek/gallery3d/util/MediatekFeature$Params;

    invoke-direct {v0}, Lcom/mediatek/gallery3d/util/MediatekFeature$Params;-><init>()V

    iput-boolean v4, v0, Lcom/mediatek/gallery3d/util/MediatekFeature$Params;->inMpoTotalCount:Z

    const-string v1, "Gallery2/SinglePhotoDataAdapter"

    const-string v2, "get mav total count"

    invoke-static {v1, v2}, Lcom/mediatek/gallery3d/util/MtkLog;->d(Ljava/lang/String;Ljava/lang/String;)I

    iput-boolean v5, p0, Lcom/android/gallery3d/app/SinglePhotoDataAdapter;->mIsMavLoadingFinished:Z

    iget-object v1, p0, Lcom/android/gallery3d/app/SinglePhotoDataAdapter;->mPhotoView:Lcom/android/gallery3d/ui/PhotoView;

    iget-boolean v2, p0, Lcom/android/gallery3d/app/SinglePhotoDataAdapter;->mIsMavLoadingFinished:Z

    invoke-virtual {v1, v2}, Lcom/android/gallery3d/ui/PhotoView;->setMavLoadingFinished(Z)V

    iget-object v1, p0, Lcom/android/gallery3d/app/SinglePhotoDataAdapter;->mThreadPool:Lcom/android/gallery3d/util/ThreadPool;

    iget-object v2, p0, Lcom/android/gallery3d/app/SinglePhotoDataAdapter;->mItem:Lcom/android/gallery3d/data/MediaItem;

    invoke-virtual {v2, v4, v0}, Lcom/android/gallery3d/data/MediaItem;->requestImage(ILcom/mediatek/gallery3d/util/MediatekFeature$Params;)Lcom/android/gallery3d/util/ThreadPool$Job;

    move-result-object v2

    new-instance v3, Lcom/android/gallery3d/app/SinglePhotoDataAdapter$MavDecoderListener;

    iget-object v4, p0, Lcom/android/gallery3d/app/SinglePhotoDataAdapter;->mItem:Lcom/android/gallery3d/data/MediaItem;

    invoke-direct {v3, p0, v4, v5}, Lcom/android/gallery3d/app/SinglePhotoDataAdapter$MavDecoderListener;-><init>(Lcom/android/gallery3d/app/SinglePhotoDataAdapter;Lcom/android/gallery3d/data/MediaItem;I)V

    invoke-virtual {v1, v2, v3}, Lcom/android/gallery3d/util/ThreadPool;->submit(Lcom/android/gallery3d/util/ThreadPool$Job;Lcom/android/gallery3d/util/FutureListener;)Lcom/android/gallery3d/util/Future;

    move-result-object v1

    iput-object v1, p0, Lcom/android/gallery3d/app/SinglePhotoDataAdapter;->mMpoDecoderTask:Lcom/android/gallery3d/util/Future;

    :cond_3
    invoke-direct {p0}, Lcom/android/gallery3d/app/SinglePhotoDataAdapter;->resetStereoMode()V

    return-void
.end method

.method public setCurrentPhoto(Lcom/android/gallery3d/data/Path;I)V
    .locals 0
    .param p1    # Lcom/android/gallery3d/data/Path;
    .param p2    # I

    return-void
.end method

.method public setFocusHintDirection(I)V
    .locals 0
    .param p1    # I

    return-void
.end method

.method public setFocusHintPath(Lcom/android/gallery3d/data/Path;)V
    .locals 0
    .param p1    # Lcom/android/gallery3d/data/Path;

    return-void
.end method

.method public setImageBitmap(I)V
    .locals 5
    .param p1    # I

    iget-object v2, p0, Lcom/android/gallery3d/app/SinglePhotoDataAdapter;->mpoFrames:[Lcom/android/gallery3d/ui/ScreenNail;

    if-nez v2, :cond_0

    const-string v2, "Gallery2/SinglePhotoDataAdapter"

    const-string v3, "setImageBitmap: the mpoFrames of current entry is null"

    invoke-static {v2, v3}, Lcom/mediatek/gallery3d/util/MtkLog;->v(Ljava/lang/String;Ljava/lang/String;)I

    :goto_0
    return-void

    :cond_0
    const/4 v1, 0x0

    const/4 v0, 0x0

    iget-object v2, p0, Lcom/android/gallery3d/app/SinglePhotoDataAdapter;->mpoFrames:[Lcom/android/gallery3d/ui/ScreenNail;

    array-length v0, v2

    if-ltz p1, :cond_1

    if-ge p1, v0, :cond_1

    iput p1, p0, Lcom/android/gallery3d/app/SinglePhotoDataAdapter;->mCurrentMpoIndex:I

    const-string v2, "Gallery2/SinglePhotoDataAdapter"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "get current mpo frame, index: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/mediatek/gallery3d/util/MtkLog;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_1
    iget-boolean v2, p0, Lcom/android/gallery3d/app/SinglePhotoDataAdapter;->mIsMavStereoMode:Z

    if-eqz v2, :cond_4

    const/4 v2, 0x1

    if-le v0, v2, :cond_2

    add-int/lit8 v1, p1, 0x1

    if-gez v1, :cond_3

    move v1, p1

    :cond_2
    :goto_1
    iput v1, p0, Lcom/android/gallery3d/app/SinglePhotoDataAdapter;->mNextMpoIndex:I

    const-string v2, "Gallery2/SinglePhotoDataAdapter"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "get next mpo frame, index: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/mediatek/gallery3d/util/MtkLog;->d(Ljava/lang/String;Ljava/lang/String;)I

    invoke-direct {p0}, Lcom/android/gallery3d/app/SinglePhotoDataAdapter;->requestRender()V

    goto :goto_0

    :cond_3
    add-int/lit8 v2, v0, -0x1

    if-le v1, v2, :cond_2

    add-int/lit8 v1, v0, -0x1

    goto :goto_1

    :cond_4
    move v1, p1

    goto :goto_1
.end method

.method public setMavListener(Lcom/android/gallery3d/app/PhotoDataAdapter$MavListener;)V
    .locals 0
    .param p1    # Lcom/android/gallery3d/app/PhotoDataAdapter$MavListener;

    iput-object p1, p0, Lcom/android/gallery3d/app/SinglePhotoDataAdapter;->mMavListener:Lcom/android/gallery3d/app/PhotoDataAdapter$MavListener;

    return-void
.end method

.method public setNeedFullImage(Z)V
    .locals 0
    .param p1    # Z

    return-void
.end method

.method public updateMavStereoMode(Z)V
    .locals 0
    .param p1    # Z

    iput-boolean p1, p0, Lcom/android/gallery3d/app/SinglePhotoDataAdapter;->mIsMavStereoMode:Z

    return-void
.end method
