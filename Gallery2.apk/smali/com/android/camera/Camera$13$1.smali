.class Lcom/android/camera/Camera$13$1;
.super Ljava/lang/Object;
.source "Camera.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/android/camera/Camera$13;->onRestorePreferencesClicked()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$1:Lcom/android/camera/Camera$13;


# direct methods
.method constructor <init>(Lcom/android/camera/Camera$13;)V
    .locals 0

    iput-object p1, p0, Lcom/android/camera/Camera$13$1;->this$1:Lcom/android/camera/Camera$13;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 4

    const/4 v3, 0x0

    iget-object v0, p0, Lcom/android/camera/Camera$13$1;->this$1:Lcom/android/camera/Camera$13;

    iget-object v0, v0, Lcom/android/camera/Camera$13;->this$0:Lcom/android/camera/Camera;

    invoke-static {v0, v3}, Lcom/android/camera/Camera;->access$2002(Lcom/android/camera/Camera;I)I

    iget-object v0, p0, Lcom/android/camera/Camera$13$1;->this$1:Lcom/android/camera/Camera$13;

    iget-object v0, v0, Lcom/android/camera/Camera$13;->this$0:Lcom/android/camera/Camera;

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lcom/android/camera/Camera;->access$2102(Lcom/android/camera/Camera;Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v0, p0, Lcom/android/camera/Camera$13$1;->this$1:Lcom/android/camera/Camera$13;

    iget-object v0, v0, Lcom/android/camera/Camera$13;->this$0:Lcom/android/camera/Camera;

    invoke-static {v0}, Lcom/android/camera/Camera;->access$800(Lcom/android/camera/Camera;)Lcom/android/camera/actor/CameraActor;

    move-result-object v0

    invoke-virtual {v0}, Lcom/android/camera/actor/CameraActor;->onRestoreSettings()V

    iget-object v0, p0, Lcom/android/camera/Camera$13$1;->this$1:Lcom/android/camera/Camera$13;

    iget-object v0, v0, Lcom/android/camera/Camera$13;->this$0:Lcom/android/camera/Camera;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/android/camera/Camera;->collapseViewManager(Z)Z

    iget-object v0, p0, Lcom/android/camera/Camera$13$1;->this$1:Lcom/android/camera/Camera$13;

    iget-object v0, v0, Lcom/android/camera/Camera$13;->this$0:Lcom/android/camera/Camera;

    iget-object v1, p0, Lcom/android/camera/Camera$13$1;->this$1:Lcom/android/camera/Camera$13;

    iget-object v1, v1, Lcom/android/camera/Camera$13;->this$0:Lcom/android/camera/Camera;

    invoke-static {v1}, Lcom/android/camera/Camera;->access$1700(Lcom/android/camera/Camera;)Lcom/android/camera/ComboPreferences;

    move-result-object v1

    iget-object v2, p0, Lcom/android/camera/Camera$13$1;->this$1:Lcom/android/camera/Camera$13;

    iget-object v2, v2, Lcom/android/camera/Camera$13;->this$0:Lcom/android/camera/Camera;

    invoke-static {v2}, Lcom/android/camera/Camera;->access$2600(Lcom/android/camera/Camera;)Landroid/hardware/Camera$Parameters;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lcom/android/camera/CameraSettings;->restorePreferences(Landroid/content/Context;Lcom/android/camera/ComboPreferences;Landroid/hardware/Camera$Parameters;)V

    iget-object v0, p0, Lcom/android/camera/Camera$13$1;->this$1:Lcom/android/camera/Camera$13;

    iget-object v0, v0, Lcom/android/camera/Camera$13;->this$0:Lcom/android/camera/Camera;

    invoke-static {v0}, Lcom/android/camera/Camera;->access$4500(Lcom/android/camera/Camera;)Lcom/android/camera/manager/ZoomManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/android/camera/manager/ZoomManager;->resetZoom()V

    iget-object v0, p0, Lcom/android/camera/Camera$13$1;->this$1:Lcom/android/camera/Camera$13;

    iget-object v0, v0, Lcom/android/camera/Camera$13;->this$0:Lcom/android/camera/Camera;

    invoke-static {v0}, Lcom/android/camera/Camera;->access$800(Lcom/android/camera/Camera;)Lcom/android/camera/actor/CameraActor;

    move-result-object v0

    invoke-virtual {v0}, Lcom/android/camera/actor/CameraActor;->getMode()I

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/camera/Camera$13$1;->this$1:Lcom/android/camera/Camera$13;

    iget-object v0, v0, Lcom/android/camera/Camera$13;->this$0:Lcom/android/camera/Camera;

    invoke-virtual {v0}, Lcom/android/camera/Camera;->isNonePickIntent()Z

    move-result v0

    if-nez v0, :cond_1

    :cond_0
    iget-object v0, p0, Lcom/android/camera/Camera$13$1;->this$1:Lcom/android/camera/Camera$13;

    iget-object v0, v0, Lcom/android/camera/Camera$13;->this$0:Lcom/android/camera/Camera;

    invoke-static {v0, v3}, Lcom/android/camera/Camera;->access$1400(Lcom/android/camera/Camera;Z)V

    iget-object v0, p0, Lcom/android/camera/Camera$13$1;->this$1:Lcom/android/camera/Camera$13;

    iget-object v0, v0, Lcom/android/camera/Camera$13;->this$0:Lcom/android/camera/Camera;

    invoke-static {v0}, Lcom/android/camera/Camera;->access$3400(Lcom/android/camera/Camera;)Lcom/android/camera/SettingChecker;

    move-result-object v0

    invoke-virtual {v0}, Lcom/android/camera/SettingChecker;->applyParametersToUIImmediately()V

    :goto_0
    return-void

    :cond_1
    iget-object v0, p0, Lcom/android/camera/Camera$13$1;->this$1:Lcom/android/camera/Camera$13;

    iget-object v0, v0, Lcom/android/camera/Camera$13;->this$0:Lcom/android/camera/Camera;

    invoke-static {v0}, Lcom/android/camera/Camera;->access$4100(Lcom/android/camera/Camera;)Lcom/android/camera/manager/ModePicker;

    move-result-object v0

    invoke-virtual {v0, v3}, Lcom/android/camera/manager/ModePicker;->setCurrentMode(I)V

    goto :goto_0
.end method
