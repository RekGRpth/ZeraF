.class public final Lcom/android/camera/R$color;
.super Ljava/lang/Object;
.source "R.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/camera/R;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "color"
.end annotation


# static fields
.field public static final album_background:I = 0x7f09000c

.field public static final album_placeholder:I = 0x7f09000d

.field public static final albumset_background:I = 0x7f090007

.field public static final albumset_label_background:I = 0x7f090009

.field public static final albumset_label_count:I = 0x7f09000b

.field public static final albumset_label_title:I = 0x7f09000a

.field public static final albumset_placeholder:I = 0x7f090008

.field public static final background_main_toolbar:I = 0x7f09001c

.field public static final background_screen:I = 0x7f09001a

.field public static final background_toolbar:I = 0x7f09001b

.field public static final bitmap_screennail_placeholder:I = 0x7f090012

.field public static final blue:I = 0x7f090018

.field public static final button_dark_transparent_background:I = 0x7f090014

.field public static final cache_background:I = 0x7f090010

.field public static final cache_placeholder:I = 0x7f090011

.field public static final color_picker_preset_color1:I = 0x7f090022

.field public static final color_picker_preset_color10:I = 0x7f09002b

.field public static final color_picker_preset_color11:I = 0x7f09002c

.field public static final color_picker_preset_color12:I = 0x7f09002d

.field public static final color_picker_preset_color13:I = 0x7f09002e

.field public static final color_picker_preset_color14:I = 0x7f09002f

.field public static final color_picker_preset_color15:I = 0x7f090030

.field public static final color_picker_preset_color16:I = 0x7f090031

.field public static final color_picker_preset_color17:I = 0x7f090032

.field public static final color_picker_preset_color18:I = 0x7f090033

.field public static final color_picker_preset_color19:I = 0x7f090034

.field public static final color_picker_preset_color2:I = 0x7f090023

.field public static final color_picker_preset_color20:I = 0x7f090035

.field public static final color_picker_preset_color21:I = 0x7f090036

.field public static final color_picker_preset_color22:I = 0x7f090037

.field public static final color_picker_preset_color23:I = 0x7f090038

.field public static final color_picker_preset_color24:I = 0x7f090039

.field public static final color_picker_preset_color3:I = 0x7f090024

.field public static final color_picker_preset_color4:I = 0x7f090025

.field public static final color_picker_preset_color5:I = 0x7f090026

.field public static final color_picker_preset_color6:I = 0x7f090027

.field public static final color_picker_preset_color7:I = 0x7f090028

.field public static final color_picker_preset_color8:I = 0x7f090029

.field public static final color_picker_preset_color9:I = 0x7f09002a

.field public static final darker_transparent:I = 0x7f09003a

.field public static final default_background:I = 0x7f090006

.field public static final green:I = 0x7f090016

.field public static final opaque_cyan:I = 0x7f090021

.field public static final photo_background:I = 0x7f09000e

.field public static final photo_placeholder:I = 0x7f09000f

.field public static final popup_background:I = 0x7f090001

.field public static final popup_title_color:I = 0x7f090000

.field public static final red:I = 0x7f090017

.field public static final seletct_image_background:I = 0x7f090005

.field public static final setting_item_text_color_highlight:I = 0x7f090002

.field public static final setting_item_text_color_normal:I = 0x7f090003

.field public static final setting_item_text_color_normal_text:I = 0x7f090004

.field public static final slideshow_background:I = 0x7f090013

.field public static final text_toolbar:I = 0x7f090019

.field public static final toolbar_separation_line:I = 0x7f09001d

.field public static final translucent_black:I = 0x7f09001e

.field public static final translucent_cyan:I = 0x7f090020

.field public static final translucent_white:I = 0x7f09001f

.field public static final yellow:I = 0x7f090015


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
