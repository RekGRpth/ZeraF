.class public Lcom/android/camera/VoiceManager;
.super Ljava/lang/Object;
.source "VoiceManager.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/camera/VoiceManager$MyVoiceCommandListener;,
        Lcom/android/camera/VoiceManager$Listener;
    }
.end annotation


# static fields
.field private static final LOG:Z

.field private static final TAG:Ljava/lang/String; = "VoiceManager"

.field private static final UNKNOWN:I = -0x1

.field public static final VOICE_OFF:Ljava/lang/String; = "off"

.field public static final VOICE_ON:Ljava/lang/String; = "on"

.field private static final VOICE_SERVICE:Ljava/lang/String; = "voicecommand"


# instance fields
.field private mContext:Landroid/content/Context;

.field private mKeywords:[Ljava/lang/String;

.field private mListeners:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/android/camera/VoiceManager$Listener;",
            ">;"
        }
    .end annotation
.end field

.field private mRegistered:Z

.field private mStartUpdate:Z

.field private mVoiceListener:Lcom/android/camera/VoiceManager$MyVoiceCommandListener;

.field private mVoiceManager:Lcom/mediatek/common/voicecommand/IVoiceCommandManager;

.field private mVoiceValue:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    sget-boolean v0, Lcom/android/camera/Log;->LOGV:Z

    sput-boolean v0, Lcom/android/camera/VoiceManager;->LOG:Z

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1    # Landroid/content/Context;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-direct {v0}, Ljava/util/concurrent/CopyOnWriteArrayList;-><init>()V

    iput-object v0, p0, Lcom/android/camera/VoiceManager;->mListeners:Ljava/util/List;

    iput-object p1, p0, Lcom/android/camera/VoiceManager;->mContext:Landroid/content/Context;

    return-void
.end method

.method static synthetic access$000()Z
    .locals 1

    sget-boolean v0, Lcom/android/camera/VoiceManager;->LOG:Z

    return v0
.end method

.method static synthetic access$100(Lcom/android/camera/VoiceManager;I)V
    .locals 0
    .param p0    # Lcom/android/camera/VoiceManager;
    .param p1    # I

    invoke-direct {p0, p1}, Lcom/android/camera/VoiceManager;->notifyCommandIfNeed(I)V

    return-void
.end method

.method static synthetic access$202(Lcom/android/camera/VoiceManager;[Ljava/lang/String;)[Ljava/lang/String;
    .locals 0
    .param p0    # Lcom/android/camera/VoiceManager;
    .param p1    # [Ljava/lang/String;

    iput-object p1, p0, Lcom/android/camera/VoiceManager;->mKeywords:[Ljava/lang/String;

    return-object p1
.end method

.method static synthetic access$300(Lcom/android/camera/VoiceManager;)V
    .locals 0
    .param p0    # Lcom/android/camera/VoiceManager;

    invoke-direct {p0}, Lcom/android/camera/VoiceManager;->notifyUserGuideIfNeed()V

    return-void
.end method

.method static synthetic access$402(Lcom/android/camera/VoiceManager;Ljava/lang/String;)Ljava/lang/String;
    .locals 0
    .param p0    # Lcom/android/camera/VoiceManager;
    .param p1    # Ljava/lang/String;

    iput-object p1, p0, Lcom/android/camera/VoiceManager;->mVoiceValue:Ljava/lang/String;

    return-object p1
.end method

.method static synthetic access$500(Lcom/android/camera/VoiceManager;)V
    .locals 0
    .param p0    # Lcom/android/camera/VoiceManager;

    invoke-direct {p0}, Lcom/android/camera/VoiceManager;->notifyStateChangedIfNeed()V

    return-void
.end method

.method private disableVoice()V
    .locals 3

    sget-boolean v0, Lcom/android/camera/VoiceManager;->LOG:Z

    if-eqz v0, :cond_0

    const-string v0, "VoiceManager"

    const-string v1, "disableVoice()"

    invoke-static {v0, v1}, Lcom/android/camera/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    invoke-direct {p0}, Lcom/android/camera/VoiceManager;->ensureManager()V

    const/4 v0, 0x2

    const/4 v1, 0x4

    const/4 v2, 0x0

    invoke-direct {p0, v0, v1, v2}, Lcom/android/camera/VoiceManager;->startVoiceCommand(IILandroid/os/Bundle;)V

    invoke-direct {p0}, Lcom/android/camera/VoiceManager;->unRegisterManagerListener()V

    return-void
.end method

.method private ensureManager()V
    .locals 2

    iget-object v0, p0, Lcom/android/camera/VoiceManager;->mVoiceManager:Lcom/mediatek/common/voicecommand/IVoiceCommandManager;

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/android/camera/VoiceManager;->mContext:Landroid/content/Context;

    const-string v1, "voicecommand"

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/mediatek/common/voicecommand/IVoiceCommandManager;

    iput-object v0, p0, Lcom/android/camera/VoiceManager;->mVoiceManager:Lcom/mediatek/common/voicecommand/IVoiceCommandManager;

    new-instance v0, Lcom/android/camera/VoiceManager$MyVoiceCommandListener;

    iget-object v1, p0, Lcom/android/camera/VoiceManager;->mContext:Landroid/content/Context;

    invoke-direct {v0, p0, v1}, Lcom/android/camera/VoiceManager$MyVoiceCommandListener;-><init>(Lcom/android/camera/VoiceManager;Landroid/content/Context;)V

    iput-object v0, p0, Lcom/android/camera/VoiceManager;->mVoiceListener:Lcom/android/camera/VoiceManager$MyVoiceCommandListener;

    :cond_0
    return-void
.end method

.method private getUserVoiceGuide([Ljava/lang/String;)Ljava/lang/String;
    .locals 7
    .param p1    # [Ljava/lang/String;

    const/4 v3, 0x2

    const/4 v6, 0x1

    const/4 v5, 0x0

    const/4 v0, 0x0

    if-eqz p1, :cond_0

    array-length v1, p1

    if-lt v1, v3, :cond_0

    iget-object v1, p0, Lcom/android/camera/VoiceManager;->mContext:Landroid/content/Context;

    const v2, 0x7f0c00ae

    new-array v3, v3, [Ljava/lang/Object;

    aget-object v4, p1, v5

    aput-object v4, v3, v5

    aget-object v4, p1, v6

    aput-object v4, v3, v6

    invoke-virtual {v1, v2, v3}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    :cond_0
    sget-boolean v1, Lcom/android/camera/VoiceManager;->LOG:Z

    if-eqz v1, :cond_1

    const-string v1, "VoiceManager"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "getUserVoiceGuide("

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ") return "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/android/camera/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    :cond_1
    return-object v0
.end method

.method private notifyCommandIfNeed(I)V
    .locals 5
    .param p1    # I

    sget-boolean v2, Lcom/android/camera/VoiceManager;->LOG:Z

    if-eqz v2, :cond_0

    const-string v2, "VoiceManager"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "notifyCommandIfNeed("

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ")"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/android/camera/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    iget-object v2, p0, Lcom/android/camera/VoiceManager;->mListeners:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/android/camera/VoiceManager$Listener;

    invoke-interface {v1, p1}, Lcom/android/camera/VoiceManager$Listener;->onVoiceCommandReceive(I)V

    goto :goto_0

    :cond_1
    return-void
.end method

.method private notifyStateChangedIfNeed()V
    .locals 5

    sget-boolean v2, Lcom/android/camera/VoiceManager;->LOG:Z

    if-eqz v2, :cond_0

    const-string v2, "VoiceManager"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "notifyStateChangedIfNeed() mVoiceValue="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lcom/android/camera/VoiceManager;->mVoiceValue:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/android/camera/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    iget-object v2, p0, Lcom/android/camera/VoiceManager;->mListeners:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/android/camera/VoiceManager$Listener;

    iget-object v2, p0, Lcom/android/camera/VoiceManager;->mVoiceValue:Ljava/lang/String;

    invoke-interface {v1, v2}, Lcom/android/camera/VoiceManager$Listener;->onVoiceValueUpdated(Ljava/lang/String;)V

    goto :goto_0

    :cond_1
    return-void
.end method

.method private notifyUserGuideIfNeed()V
    .locals 6

    sget-boolean v3, Lcom/android/camera/VoiceManager;->LOG:Z

    if-eqz v3, :cond_0

    const-string v3, "VoiceManager"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "notifyUserGuideIfNeed() mKeywords="

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-object v5, p0, Lcom/android/camera/VoiceManager;->mKeywords:[Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/android/camera/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    iget-object v3, p0, Lcom/android/camera/VoiceManager;->mKeywords:[Ljava/lang/String;

    if-eqz v3, :cond_1

    iget-object v3, p0, Lcom/android/camera/VoiceManager;->mKeywords:[Ljava/lang/String;

    invoke-direct {p0, v3}, Lcom/android/camera/VoiceManager;->getUserVoiceGuide([Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    if-eqz v2, :cond_1

    iget-object v3, p0, Lcom/android/camera/VoiceManager;->mListeners:Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/android/camera/VoiceManager$Listener;

    invoke-interface {v1, v2}, Lcom/android/camera/VoiceManager$Listener;->onUserGuideUpdated(Ljava/lang/String;)V

    goto :goto_0

    :cond_1
    return-void
.end method

.method private registerManagerListener()V
    .locals 4

    sget-boolean v1, Lcom/android/camera/VoiceManager;->LOG:Z

    if-eqz v1, :cond_0

    const-string v1, "VoiceManager"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "registerManagerListener() mRegistered="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-boolean v3, p0, Lcom/android/camera/VoiceManager;->mRegistered:Z

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/android/camera/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    iget-object v1, p0, Lcom/android/camera/VoiceManager;->mVoiceManager:Lcom/mediatek/common/voicecommand/IVoiceCommandManager;

    if-eqz v1, :cond_1

    iget-boolean v1, p0, Lcom/android/camera/VoiceManager;->mRegistered:Z

    if-nez v1, :cond_1

    :try_start_0
    iget-object v1, p0, Lcom/android/camera/VoiceManager;->mVoiceManager:Lcom/mediatek/common/voicecommand/IVoiceCommandManager;

    iget-object v2, p0, Lcom/android/camera/VoiceManager;->mVoiceListener:Lcom/android/camera/VoiceManager$MyVoiceCommandListener;

    invoke-interface {v1, v2}, Lcom/mediatek/common/voicecommand/IVoiceCommandManager;->registerListener(Lcom/mediatek/common/voicecommand/VoiceCommandListener;)V

    const/4 v1, 0x1

    iput-boolean v1, p0, Lcom/android/camera/VoiceManager;->mRegistered:Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/IllegalAccessException; {:try_start_0 .. :try_end_0} :catch_1

    :cond_1
    :goto_0
    return-void

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Ljava/lang/Throwable;->printStackTrace()V

    goto :goto_0

    :catch_1
    move-exception v0

    invoke-virtual {v0}, Ljava/lang/Throwable;->printStackTrace()V

    goto :goto_0
.end method

.method private startGetVoiceState()V
    .locals 3

    sget-boolean v0, Lcom/android/camera/VoiceManager;->LOG:Z

    if-eqz v0, :cond_0

    const-string v0, "VoiceManager"

    const-string v1, "startGetVoiceState()"

    invoke-static {v0, v1}, Lcom/android/camera/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    invoke-direct {p0}, Lcom/android/camera/VoiceManager;->ensureManager()V

    invoke-direct {p0}, Lcom/android/camera/VoiceManager;->registerManagerListener()V

    const/4 v0, 0x1

    const/4 v1, 0x2

    const/4 v2, 0x0

    invoke-direct {p0, v0, v1, v2}, Lcom/android/camera/VoiceManager;->startVoiceCommand(IILandroid/os/Bundle;)V

    return-void
.end method

.method private startVoiceCommand(IILandroid/os/Bundle;)V
    .locals 4
    .param p1    # I
    .param p2    # I
    .param p3    # Landroid/os/Bundle;

    sget-boolean v1, Lcom/android/camera/VoiceManager;->LOG:Z

    if-eqz v1, :cond_0

    const-string v1, "VoiceManager"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "startVoiceCommand("

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ", "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ", "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ")"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/android/camera/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    iget-object v1, p0, Lcom/android/camera/VoiceManager;->mVoiceManager:Lcom/mediatek/common/voicecommand/IVoiceCommandManager;

    if-eqz v1, :cond_1

    :try_start_0
    iget-object v1, p0, Lcom/android/camera/VoiceManager;->mVoiceManager:Lcom/mediatek/common/voicecommand/IVoiceCommandManager;

    iget-object v2, p0, Lcom/android/camera/VoiceManager;->mContext:Landroid/content/Context;

    invoke-interface {v1, v2, p1, p2, p3}, Lcom/mediatek/common/voicecommand/IVoiceCommandManager;->sendCommand(Landroid/content/Context;IILandroid/os/Bundle;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/IllegalAccessException; {:try_start_0 .. :try_end_0} :catch_1

    :cond_1
    :goto_0
    sget-boolean v1, Lcom/android/camera/VoiceManager;->LOG:Z

    if-eqz v1, :cond_2

    const-string v1, "VoiceManager"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "startVoiceCommand() mVoiceManager="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/android/camera/VoiceManager;->mVoiceManager:Lcom/mediatek/common/voicecommand/IVoiceCommandManager;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/android/camera/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    :cond_2
    return-void

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Ljava/lang/Throwable;->printStackTrace()V

    goto :goto_0

    :catch_1
    move-exception v0

    invoke-virtual {v0}, Ljava/lang/Throwable;->printStackTrace()V

    goto :goto_0
.end method

.method private stopVoice()V
    .locals 3

    const/4 v2, 0x2

    sget-boolean v0, Lcom/android/camera/VoiceManager;->LOG:Z

    if-eqz v0, :cond_0

    const-string v0, "VoiceManager"

    const-string v1, "stopVoice()"

    invoke-static {v0, v1}, Lcom/android/camera/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    invoke-direct {p0}, Lcom/android/camera/VoiceManager;->ensureManager()V

    const/4 v0, 0x0

    invoke-direct {p0, v2, v2, v0}, Lcom/android/camera/VoiceManager;->startVoiceCommand(IILandroid/os/Bundle;)V

    invoke-direct {p0}, Lcom/android/camera/VoiceManager;->unRegisterManagerListener()V

    return-void
.end method

.method private unRegisterManagerListener()V
    .locals 4

    sget-boolean v1, Lcom/android/camera/VoiceManager;->LOG:Z

    if-eqz v1, :cond_0

    const-string v1, "VoiceManager"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "unRegisterManagerListener() mRegistered="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-boolean v3, p0, Lcom/android/camera/VoiceManager;->mRegistered:Z

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/android/camera/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    iget-object v1, p0, Lcom/android/camera/VoiceManager;->mVoiceManager:Lcom/mediatek/common/voicecommand/IVoiceCommandManager;

    if-eqz v1, :cond_1

    iget-boolean v1, p0, Lcom/android/camera/VoiceManager;->mRegistered:Z

    if-eqz v1, :cond_1

    :try_start_0
    iget-object v1, p0, Lcom/android/camera/VoiceManager;->mVoiceManager:Lcom/mediatek/common/voicecommand/IVoiceCommandManager;

    iget-object v2, p0, Lcom/android/camera/VoiceManager;->mVoiceListener:Lcom/android/camera/VoiceManager$MyVoiceCommandListener;

    invoke-interface {v1, v2}, Lcom/mediatek/common/voicecommand/IVoiceCommandManager;->unRegisterListener(Lcom/mediatek/common/voicecommand/VoiceCommandListener;)V

    const/4 v1, 0x0

    iput-boolean v1, p0, Lcom/android/camera/VoiceManager;->mRegistered:Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/IllegalAccessException; {:try_start_0 .. :try_end_0} :catch_1

    :cond_1
    :goto_0
    return-void

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Ljava/lang/Throwable;->printStackTrace()V

    goto :goto_0

    :catch_1
    move-exception v0

    invoke-virtual {v0}, Ljava/lang/Throwable;->printStackTrace()V

    goto :goto_0
.end method


# virtual methods
.method public addListener(Lcom/android/camera/VoiceManager$Listener;)Z
    .locals 4
    .param p1    # Lcom/android/camera/VoiceManager$Listener;

    sget-boolean v1, Lcom/android/camera/VoiceManager;->LOG:Z

    if-eqz v1, :cond_0

    const-string v1, "VoiceManager"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "addListener("

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ")"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/android/camera/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    const/4 v0, 0x0

    iget-object v1, p0, Lcom/android/camera/VoiceManager;->mListeners:Ljava/util/List;

    invoke-interface {v1, p1}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_1

    iget-object v1, p0, Lcom/android/camera/VoiceManager;->mListeners:Ljava/util/List;

    invoke-interface {v1, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    move-result v0

    :cond_1
    invoke-direct {p0}, Lcom/android/camera/VoiceManager;->notifyUserGuideIfNeed()V

    return v0
.end method

.method public enableVoice()V
    .locals 4

    const/4 v3, 0x0

    const/4 v2, 0x1

    sget-boolean v0, Lcom/android/camera/VoiceManager;->LOG:Z

    if-eqz v0, :cond_0

    const-string v0, "VoiceManager"

    const-string v1, "enableVoice()"

    invoke-static {v0, v1}, Lcom/android/camera/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    invoke-direct {p0}, Lcom/android/camera/VoiceManager;->ensureManager()V

    invoke-direct {p0}, Lcom/android/camera/VoiceManager;->registerManagerListener()V

    const/4 v0, 0x2

    const/4 v1, 0x3

    invoke-direct {p0, v0, v1, v3}, Lcom/android/camera/VoiceManager;->startVoiceCommand(IILandroid/os/Bundle;)V

    invoke-direct {p0, v2, v2, v3}, Lcom/android/camera/VoiceManager;->startVoiceCommand(IILandroid/os/Bundle;)V

    return-void
.end method

.method public getVoiceValue()Ljava/lang/String;
    .locals 3

    sget-boolean v0, Lcom/android/camera/VoiceManager;->LOG:Z

    if-eqz v0, :cond_0

    const-string v0, "VoiceManager"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "getVoiceValue() return "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/android/camera/VoiceManager;->mVoiceValue:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/android/camera/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    iget-object v0, p0, Lcom/android/camera/VoiceManager;->mVoiceValue:Ljava/lang/String;

    return-object v0
.end method

.method public removeListener(Lcom/android/camera/VoiceManager$Listener;)Z
    .locals 3
    .param p1    # Lcom/android/camera/VoiceManager$Listener;

    sget-boolean v0, Lcom/android/camera/VoiceManager;->LOG:Z

    if-eqz v0, :cond_0

    const-string v0, "VoiceManager"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "removeListener("

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ")"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/android/camera/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    iget-object v0, p0, Lcom/android/camera/VoiceManager;->mListeners:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->remove(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public setVoiceValue(Ljava/lang/String;)V
    .locals 3
    .param p1    # Ljava/lang/String;

    sget-boolean v0, Lcom/android/camera/VoiceManager;->LOG:Z

    if-eqz v0, :cond_0

    const-string v0, "VoiceManager"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "setVoiceValue("

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ") mVoiceValue="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/android/camera/VoiceManager;->mVoiceValue:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/android/camera/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    iget-object v0, p0, Lcom/android/camera/VoiceManager;->mVoiceValue:Ljava/lang/String;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/android/camera/VoiceManager;->mVoiceValue:Ljava/lang/String;

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_2

    :cond_1
    iput-object p1, p0, Lcom/android/camera/VoiceManager;->mVoiceValue:Ljava/lang/String;

    const-string v0, "on"

    iget-object v1, p0, Lcom/android/camera/VoiceManager;->mVoiceValue:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-virtual {p0}, Lcom/android/camera/VoiceManager;->enableVoice()V

    :cond_2
    :goto_0
    return-void

    :cond_3
    invoke-direct {p0}, Lcom/android/camera/VoiceManager;->disableVoice()V

    goto :goto_0
.end method

.method public startUpdateVoiceState()V
    .locals 3

    sget-boolean v0, Lcom/android/camera/VoiceManager;->LOG:Z

    if-eqz v0, :cond_0

    const-string v0, "VoiceManager"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "startUpdateVoiceState() mStartUpdate="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-boolean v2, p0, Lcom/android/camera/VoiceManager;->mStartUpdate:Z

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/android/camera/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    invoke-static {}, Lcom/android/camera/FeatureSwitcher;->isVoiceEnabled()Z

    move-result v0

    if-eqz v0, :cond_1

    iget-boolean v0, p0, Lcom/android/camera/VoiceManager;->mStartUpdate:Z

    if-nez v0, :cond_1

    invoke-direct {p0}, Lcom/android/camera/VoiceManager;->startGetVoiceState()V

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/android/camera/VoiceManager;->mStartUpdate:Z

    :cond_1
    return-void
.end method

.method public stopUpdateVoiceState()V
    .locals 3

    sget-boolean v0, Lcom/android/camera/VoiceManager;->LOG:Z

    if-eqz v0, :cond_0

    const-string v0, "VoiceManager"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "stopUpdateVoiceState() mStartUpdate="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-boolean v2, p0, Lcom/android/camera/VoiceManager;->mStartUpdate:Z

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/android/camera/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    invoke-static {}, Lcom/android/camera/FeatureSwitcher;->isVoiceEnabled()Z

    move-result v0

    if-eqz v0, :cond_1

    iget-boolean v0, p0, Lcom/android/camera/VoiceManager;->mStartUpdate:Z

    if-eqz v0, :cond_1

    invoke-direct {p0}, Lcom/android/camera/VoiceManager;->stopVoice()V

    const-string v0, "off"

    iput-object v0, p0, Lcom/android/camera/VoiceManager;->mVoiceValue:Ljava/lang/String;

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/android/camera/VoiceManager;->mStartUpdate:Z

    :cond_1
    return-void
.end method
