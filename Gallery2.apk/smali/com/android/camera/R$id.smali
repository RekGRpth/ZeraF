.class public final Lcom/android/camera/R$id;
.super Ljava/lang/Object;
.source "R.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/camera/R;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "id"
.end annotation


# static fields
.field public static final ADVmode:I = 0x7f0b017e

.field public static final PQSwitch:I = 0x7f0b017d

.field public static final action_bar:I = 0x7f0b00cb

.field public static final action_bar_back:I = 0x7f0b00b9

.field public static final action_bar_title:I = 0x7f0b00ba

.field public static final action_camera:I = 0x7f0b0148

.field public static final action_cancel:I = 0x7f0b017c

.field public static final action_cluster_album:I = 0x7f0b0166

.field public static final action_cluster_faces:I = 0x7f0b016b

.field public static final action_cluster_location:I = 0x7f0b0168

.field public static final action_cluster_size:I = 0x7f0b016a

.field public static final action_cluster_tags:I = 0x7f0b0169

.field public static final action_cluster_time:I = 0x7f0b0167

.field public static final action_crop:I = 0x7f0b0174

.field public static final action_delete:I = 0x7f0b016f

.field public static final action_details:I = 0x7f0b0176

.field public static final action_edit:I = 0x7f0b0170

.field public static final action_filter_all:I = 0x7f0b014e

.field public static final action_filter_image:I = 0x7f0b014f

.field public static final action_filter_video:I = 0x7f0b0150

.field public static final action_general_help:I = 0x7f0b014c

.field public static final action_group_by:I = 0x7f0b014b

.field public static final action_import:I = 0x7f0b016d

.field public static final action_picture_quality:I = 0x7f0b017b

.field public static final action_print:I = 0x7f0b0171

.field public static final action_protect_info:I = 0x7f0b0177

.field public static final action_rotate_ccw:I = 0x7f0b0172

.field public static final action_rotate_cw:I = 0x7f0b0173

.field public static final action_select:I = 0x7f0b014a

.field public static final action_select_all:I = 0x7f0b0004

.field public static final action_setas:I = 0x7f0b0175

.field public static final action_share:I = 0x7f0b016c

.field public static final action_share_panorama:I = 0x7f0b016e

.field public static final action_show_on_map:I = 0x7f0b0178

.field public static final action_slideshow:I = 0x7f0b0149

.field public static final action_switch_stereo_mode:I = 0x7f0b0179

.field public static final action_toggle_full_caching:I = 0x7f0b0003

.field public static final action_trim:I = 0x7f0b017a

.field public static final add_account:I = 0x7f0b017f

.field public static final applyEffect:I = 0x7f0b0036

.field public static final appwidget_empty_view:I = 0x7f0b0008

.field public static final appwidget_loading_item:I = 0x7f0b0007

.field public static final appwidget_photo_item:I = 0x7f0b0009

.field public static final appwidget_stack_view:I = 0x7f0b000a

.field public static final arrows_dot_1:I = 0x7f0b0099

.field public static final arrows_dot_10:I = 0x7f0b00a2

.field public static final arrows_dot_2:I = 0x7f0b009a

.field public static final arrows_dot_3:I = 0x7f0b009b

.field public static final arrows_dot_4:I = 0x7f0b009c

.field public static final arrows_dot_5:I = 0x7f0b009d

.field public static final arrows_dot_6:I = 0x7f0b009e

.field public static final arrows_dot_7:I = 0x7f0b009f

.field public static final arrows_dot_8:I = 0x7f0b00a0

.field public static final arrows_dot_9:I = 0x7f0b00a1

.field public static final arrows_indicator:I = 0x7f0b0098

.field public static final arrows_indicator_arrow:I = 0x7f0b00a3

.field public static final artistic_button:I = 0x7f0b00c4

.field public static final aspect:I = 0x7f0b0034

.field public static final author:I = 0x7f0b001d

.field public static final bg_replace_cancel_button:I = 0x7f0b000d

.field public static final bg_replace_message:I = 0x7f0b000c

.field public static final bg_replace_message_frame:I = 0x7f0b000b

.field public static final borderButton:I = 0x7f0b0051

.field public static final bordersList:I = 0x7f0b0039

.field public static final btn_cancel:I = 0x7f0b0014

.field public static final btn_done:I = 0x7f0b0016

.field public static final btn_firstrun_dismiss:I = 0x7f0b0072

.field public static final btn_pause_resume:I = 0x7f0b0112

.field public static final btn_play:I = 0x7f0b011b

.field public static final btn_retake:I = 0x7f0b011a

.field public static final buffer_size:I = 0x7f0b0069

.field public static final buffer_tip:I = 0x7f0b0068

.field public static final bwfilterButton:I = 0x7f0b004f

.field public static final camera_app_root:I = 0x7f0b0012

.field public static final camera_picker:I = 0x7f0b0002

.field public static final camera_root:I = 0x7f0b0011

.field public static final camera_shutter:I = 0x7f0b0013

.field public static final cancel:I = 0x7f0b001c

.field public static final center_indicator:I = 0x7f0b0097

.field public static final checkBoxEv0:I = 0x7f0b0128

.field public static final checkBoxEvMinus:I = 0x7f0b0129

.field public static final checkBoxEvPlus:I = 0x7f0b012a

.field public static final color_button:I = 0x7f0b00c5

.field public static final colorsButton:I = 0x7f0b0053

.field public static final colorsFxList:I = 0x7f0b0042

.field public static final container:I = 0x7f0b012b

.field public static final content:I = 0x7f0b0063

.field public static final contrastButton:I = 0x7f0b0048

.field public static final copyright:I = 0x7f0b001e

.field public static final cropButton:I = 0x7f0b003e

.field public static final crop_menu_1to1:I = 0x7f0b0158

.field public static final crop_menu_3to4:I = 0x7f0b015b

.field public static final crop_menu_4to3:I = 0x7f0b015a

.field public static final crop_menu_4to6:I = 0x7f0b0159

.field public static final crop_menu_5to7:I = 0x7f0b015c

.field public static final crop_menu_7to5:I = 0x7f0b015d

.field public static final crop_menu_9to16:I = 0x7f0b015e

.field public static final crop_menu_none:I = 0x7f0b015f

.field public static final crop_menu_original:I = 0x7f0b0160

.field public static final crop_popupmenu:I = 0x7f0b0157

.field public static final current_setting:I = 0x7f0b0064

.field public static final curve_menu_blue:I = 0x7f0b0165

.field public static final curve_menu_green:I = 0x7f0b0164

.field public static final curve_menu_red:I = 0x7f0b0163

.field public static final curve_menu_rgb:I = 0x7f0b0162

.field public static final curvesButtonRGB:I = 0x7f0b004c

.field public static final curves_popupmenu:I = 0x7f0b0161

.field public static final data:I = 0x7f0b000f

.field public static final divider:I = 0x7f0b0020

.field public static final done:I = 0x7f0b006f

.field public static final effectImage1:I = 0x7f0b0133

.field public static final effectImage2:I = 0x7f0b0136

.field public static final effectImage3:I = 0x7f0b0139

.field public static final effect_label:I = 0x7f0b00c0

.field public static final effects_bar:I = 0x7f0b00c9

.field public static final effects_menu:I = 0x7f0b00ca

.field public static final exposureButton:I = 0x7f0b0046

.field public static final exposure_button:I = 0x7f0b00c3

.field public static final face_view:I = 0x7f0b00fb

.field public static final filterButtonsList:I = 0x7f0b0033

.field public static final filterSeekBar:I = 0x7f0b002f

.field public static final filtersPanel:I = 0x7f0b0031

.field public static final filtershow_done:I = 0x7f0b0021

.field public static final firstrun:I = 0x7f0b0075

.field public static final fix_button:I = 0x7f0b00c6

.field public static final flipButton:I = 0x7f0b0040

.field public static final focus_indicator:I = 0x7f0b005e

.field public static final focus_indicator_rotate_layout:I = 0x7f0b005d

.field public static final footer:I = 0x7f0b006c

.field public static final frame:I = 0x7f0b00f9

.field public static final frame_layout:I = 0x7f0b0118

.field public static final fxButton:I = 0x7f0b0050

.field public static final fxList:I = 0x7f0b0037

.field public static final gallery_root:I = 0x7f0b006a

.field public static final geometryButton:I = 0x7f0b0052

.field public static final geometryList:I = 0x7f0b003b

.field public static final gl_root_cover:I = 0x7f0b0060

.field public static final gl_root_view:I = 0x7f0b005f

.field public static final header:I = 0x7f0b006b

.field public static final historyPanel:I = 0x7f0b0054

.field public static final hueButton:I = 0x7f0b004d

.field public static final image:I = 0x7f0b0065

.field public static final imageBorder:I = 0x7f0b002c

.field public static final imageCrop:I = 0x7f0b0028

.field public static final imageCurves:I = 0x7f0b002b

.field public static final imageFlip:I = 0x7f0b002a

.field public static final imageRotate:I = 0x7f0b0029

.field public static final imageShow:I = 0x7f0b0026

.field public static final imageStateList:I = 0x7f0b0024

.field public static final imageStatePanel:I = 0x7f0b0023

.field public static final imageStraighten:I = 0x7f0b0027

.field public static final imageTinyPlanet:I = 0x7f0b002e

.field public static final imageView:I = 0x7f0b00d4

.field public static final imageView_adv:I = 0x7f0b00f4

.field public static final imageZoom:I = 0x7f0b002d

.field public static final imagestate_label:I = 0x7f0b005b

.field public static final imagestate_parameter:I = 0x7f0b005c

.field public static final info_view:I = 0x7f0b008e

.field public static final linearLayoutMain:I = 0x7f0b00d5

.field public static final listBorders:I = 0x7f0b003a

.field public static final listColorsFx:I = 0x7f0b0043

.field public static final listFilters:I = 0x7f0b0038

.field public static final listGeometry:I = 0x7f0b003c

.field public static final loading:I = 0x7f0b0030

.field public static final mainPanel:I = 0x7f0b0025

.field public static final mainView:I = 0x7f0b0022

.field public static final mavview:I = 0x7f0b0073

.field public static final mavviewer_progressbar:I = 0x7f0b0074

.field public static final max_port:I = 0x7f0b00f8

.field public static final max_port_err_msg:I = 0x7f0b00f7

.field public static final menu_share:I = 0x7f0b0151

.field public static final message:I = 0x7f0b0084

.field public static final min_port:I = 0x7f0b00f6

.field public static final min_port_err_msg:I = 0x7f0b00f5

.field public static final mode_asd:I = 0x7f0b007e

.field public static final mode_best:I = 0x7f0b0080

.field public static final mode_ev:I = 0x7f0b0081

.field public static final mode_face_beauty:I = 0x7f0b007b

.field public static final mode_hdr:I = 0x7f0b007a

.field public static final mode_mav:I = 0x7f0b007d

.field public static final mode_panorama:I = 0x7f0b007c

.field public static final mode_photo:I = 0x7f0b0079

.field public static final mode_picker:I = 0x7f0b0078

.field public static final mode_picker_background:I = 0x7f0b0076

.field public static final mode_picker_scroller:I = 0x7f0b0077

.field public static final mode_smile:I = 0x7f0b007f

.field public static final movie_view_root:I = 0x7f0b0082

.field public static final navi_line:I = 0x7f0b00b7

.field public static final navigation_bar:I = 0x7f0b0005

.field public static final on_screen_group:I = 0x7f0b0085

.field public static final on_screen_info:I = 0x7f0b008d

.field public static final on_screen_pickers:I = 0x7f0b0090

.field public static final on_screen_progress:I = 0x7f0b0095

.field public static final onscreen_asd_indicator:I = 0x7f0b008b

.field public static final onscreen_camera_picker:I = 0x7f0b0093

.field public static final onscreen_exposure_indicator:I = 0x7f0b0088

.field public static final onscreen_flash_picker:I = 0x7f0b0092

.field public static final onscreen_scene_indicator:I = 0x7f0b0086

.field public static final onscreen_selftimer_indicator:I = 0x7f0b008a

.field public static final onscreen_stereo3d_picker:I = 0x7f0b0091

.field public static final onscreen_timelapse_indicator:I = 0x7f0b0089

.field public static final onscreen_toast_layout:I = 0x7f0b008f

.field public static final onscreen_voice_indicator:I = 0x7f0b008c

.field public static final onscreen_white_balance_indicator:I = 0x7f0b0087

.field public static final operationsButton:I = 0x7f0b0155

.field public static final operationsList:I = 0x7f0b0055

.field public static final other_setting_indicator:I = 0x7f0b0001

.field public static final pager:I = 0x7f0b012d

.field public static final panel:I = 0x7f0b0010

.field public static final pano_arrows_bottom:I = 0x7f0b00b6

.field public static final pano_arrows_left:I = 0x7f0b00a6

.field public static final pano_arrows_right:I = 0x7f0b00ae

.field public static final pano_arrows_up:I = 0x7f0b00b0

.field public static final pano_bottom_dot_1:I = 0x7f0b00b3

.field public static final pano_bottom_dot_2:I = 0x7f0b00b4

.field public static final pano_bottom_dot_3:I = 0x7f0b00b5

.field public static final pano_down:I = 0x7f0b00b2

.field public static final pano_frame_layout:I = 0x7f0b0094

.field public static final pano_left:I = 0x7f0b00a5

.field public static final pano_left_dot_1:I = 0x7f0b00a7

.field public static final pano_left_dot_2:I = 0x7f0b00a8

.field public static final pano_left_dot_3:I = 0x7f0b00a9

.field public static final pano_right:I = 0x7f0b00aa

.field public static final pano_right_dot_1:I = 0x7f0b00ab

.field public static final pano_right_dot_2:I = 0x7f0b00ac

.field public static final pano_right_dot_3:I = 0x7f0b00ad

.field public static final pano_up:I = 0x7f0b00af

.field public static final pano_up_dot_1:I = 0x7f0b00b1

.field public static final pano_view:I = 0x7f0b0096

.field public static final photo:I = 0x7f0b00b8

.field public static final photo_view:I = 0x7f0b00c8

.field public static final photopage_bottom_control_edit:I = 0x7f0b00cd

.field public static final photopage_bottom_control_panorama:I = 0x7f0b00ce

.field public static final photopage_bottom_control_tiny_planet:I = 0x7f0b00cf

.field public static final photopage_bottom_controls:I = 0x7f0b00cc

.field public static final photopage_progress_background:I = 0x7f0b00d1

.field public static final photopage_progress_bar:I = 0x7f0b00d0

.field public static final photopage_progress_bar_text:I = 0x7f0b00d3

.field public static final photopage_progress_foreground:I = 0x7f0b00d2

.field public static final pickCurvesChannel:I = 0x7f0b0035

.field public static final preview_border:I = 0x7f0b00fa

.field public static final progress:I = 0x7f0b006e

.field public static final progress_bars:I = 0x7f0b00fd

.field public static final progress_indicator:I = 0x7f0b00fc

.field public static final proxy_host:I = 0x7f0b00ff

.field public static final proxy_host_err_msg:I = 0x7f0b00fe

.field public static final proxy_port:I = 0x7f0b0101

.field public static final proxy_port_err_msg:I = 0x7f0b0100

.field public static final radio:I = 0x7f0b0131

.field public static final radio1:I = 0x7f0b0134

.field public static final radio2:I = 0x7f0b0137

.field public static final radio3:I = 0x7f0b013a

.field public static final recording_current:I = 0x7f0b0114

.field public static final recording_progress:I = 0x7f0b0115

.field public static final recording_size_group:I = 0x7f0b0113

.field public static final recording_time:I = 0x7f0b0111

.field public static final recording_time_group:I = 0x7f0b0110

.field public static final recording_total:I = 0x7f0b0116

.field public static final redEyeButton:I = 0x7f0b0041

.field public static final redoButton:I = 0x7f0b0153

.field public static final redo_button:I = 0x7f0b00bc

.field public static final remaining_view:I = 0x7f0b0117

.field public static final resetHistoryButton:I = 0x7f0b0154

.field public static final resetOperationsButton:I = 0x7f0b0056

.field public static final restore_default:I = 0x7f0b0000

.field public static final review_image:I = 0x7f0b0119

.field public static final rlMavSeekBar:I = 0x7f0b0061

.field public static final rotateButton:I = 0x7f0b003f

.field public static final rotate_dialog_button1:I = 0x7f0b0124

.field public static final rotate_dialog_button2:I = 0x7f0b0123

.field public static final rotate_dialog_button_layout:I = 0x7f0b0122

.field public static final rotate_dialog_layout:I = 0x7f0b011d

.field public static final rotate_dialog_root_layout:I = 0x7f0b011c

.field public static final rotate_dialog_spinner:I = 0x7f0b0125

.field public static final rotate_dialog_text:I = 0x7f0b0121

.field public static final rotate_dialog_title:I = 0x7f0b011f

.field public static final rotate_dialog_title_divider:I = 0x7f0b0120

.field public static final rotate_dialog_title_layout:I = 0x7f0b011e

.field public static final rowTextView:I = 0x7f0b0059

.field public static final saturationButton:I = 0x7f0b004e

.field public static final save:I = 0x7f0b014d

.field public static final saveOperationsButton:I = 0x7f0b0057

.field public static final save_button:I = 0x7f0b00be

.field public static final save_share_buttons:I = 0x7f0b00bd

.field public static final scroll_view:I = 0x7f0b00c1

.field public static final secondRowPanel:I = 0x7f0b0032

.field public static final seekBar_GrassSat:I = 0x7f0b00ea

.field public static final seekBar_color:I = 0x7f0b00d9

.field public static final seekBar_grassTone:I = 0x7f0b00e6

.field public static final seekBar_hue:I = 0x7f0b0102

.field public static final seekBar_saturation:I = 0x7f0b0106

.field public static final seekBar_sharpness:I = 0x7f0b00d6

.field public static final seekBar_skinSat:I = 0x7f0b00e1

.field public static final seekBar_skinTone:I = 0x7f0b00dd

.field public static final seekBar_skySat:I = 0x7f0b00f1

.field public static final seekBar_skyTone:I = 0x7f0b00ed

.field public static final seekbar_mav:I = 0x7f0b0062

.field public static final selectedMark:I = 0x7f0b0058

.field public static final selection_menu:I = 0x7f0b0006

.field public static final selectroot:I = 0x7f0b0126

.field public static final settingList:I = 0x7f0b0130

.field public static final setting_indicator:I = 0x7f0b012e

.field public static final setting_item:I = 0x7f0b0067

.field public static final setting_switch:I = 0x7f0b0066

.field public static final settings_list:I = 0x7f0b012f

.field public static final shadowRecoveryButton:I = 0x7f0b0049

.field public static final share_button:I = 0x7f0b00bf

.field public static final sharpenButton:I = 0x7f0b004b

.field public static final showImageStateButton:I = 0x7f0b0156

.field public static final shutter_button_photo:I = 0x7f0b0017

.field public static final shutter_button_video:I = 0x7f0b0015

.field public static final start_trim:I = 0x7f0b013c

.field public static final static_center_indicator:I = 0x7f0b00a4

.field public static final status:I = 0x7f0b006d

.field public static final step:I = 0x7f0b013f

.field public static final step_desc:I = 0x7f0b0140

.field public static final step_option_item:I = 0x7f0b013e

.field public static final straightenButton:I = 0x7f0b003d

.field public static final surface_view:I = 0x7f0b0083

.field public static final tab_title:I = 0x7f0b012c

.field public static final textView1:I = 0x7f0b0070

.field public static final textView1_GrassSat:I = 0x7f0b00e9

.field public static final textView1_skinSat:I = 0x7f0b00e2

.field public static final textView1_skySat:I = 0x7f0b00f0

.field public static final textView2:I = 0x7f0b0071

.field public static final textView2_hue:I = 0x7f0b010a

.field public static final textView2_hue_old:I = 0x7f0b0103

.field public static final textView3:I = 0x7f0b00da

.field public static final textView3_saturation:I = 0x7f0b010d

.field public static final textView3_saturation_old:I = 0x7f0b0107

.field public static final textView4:I = 0x7f0b00de

.field public static final textView5:I = 0x7f0b00e5

.field public static final textView_GrassSat:I = 0x7f0b00eb

.field public static final textView_GrassSat_progress:I = 0x7f0b00ec

.field public static final textView_color:I = 0x7f0b00dc

.field public static final textView_color_progress:I = 0x7f0b00db

.field public static final textView_grassTone:I = 0x7f0b00e7

.field public static final textView_grassTone_progress:I = 0x7f0b00e8

.field public static final textView_hue:I = 0x7f0b010b

.field public static final textView_hue_old:I = 0x7f0b0104

.field public static final textView_hue_progress:I = 0x7f0b010c

.field public static final textView_hue_progress_old:I = 0x7f0b0105

.field public static final textView_saturation:I = 0x7f0b010e

.field public static final textView_saturation_old:I = 0x7f0b0108

.field public static final textView_saturation_progress:I = 0x7f0b010f

.field public static final textView_saturation_progress_old:I = 0x7f0b0109

.field public static final textView_sharpness:I = 0x7f0b00d8

.field public static final textView_sharpness_progress:I = 0x7f0b00d7

.field public static final textView_skinSat:I = 0x7f0b00e4

.field public static final textView_skinSat_progress:I = 0x7f0b00e3

.field public static final textView_skinTone:I = 0x7f0b00e0

.field public static final textView_skinTone_progress:I = 0x7f0b00df

.field public static final textView_skySat:I = 0x7f0b00f2

.field public static final textView_skySat_progress:I = 0x7f0b00f3

.field public static final textView_skyTone:I = 0x7f0b00ee

.field public static final textView_skyTone_progress:I = 0x7f0b00ef

.field public static final thumbnail:I = 0x7f0b013b

.field public static final thumbnails:I = 0x7f0b0127

.field public static final tinyplanetButton:I = 0x7f0b0044

.field public static final title:I = 0x7f0b000e

.field public static final title1:I = 0x7f0b0132

.field public static final title2:I = 0x7f0b0135

.field public static final title3:I = 0x7f0b0138

.field public static final toggles:I = 0x7f0b00c2

.field public static final toolbar:I = 0x7f0b00c7

.field public static final trim_view_root:I = 0x7f0b013d

.field public static final tv_empty_albumset:I = 0x7f0b001f

.field public static final typeMark:I = 0x7f0b005a

.field public static final undoButton:I = 0x7f0b0152

.field public static final undo_button:I = 0x7f0b00bb

.field public static final vibranceButton:I = 0x7f0b004a

.field public static final view_layer_bottom:I = 0x7f0b0142

.field public static final view_layer_normal:I = 0x7f0b0143

.field public static final view_layer_overlay:I = 0x7f0b0147

.field public static final view_layer_root:I = 0x7f0b0141

.field public static final view_layer_setting:I = 0x7f0b0146

.field public static final view_layer_shutter:I = 0x7f0b0145

.field public static final view_layer_top:I = 0x7f0b0144

.field public static final vignetteButton:I = 0x7f0b0047

.field public static final wbalanceButton:I = 0x7f0b0045

.field public static final widget_type:I = 0x7f0b0018

.field public static final widget_type_album:I = 0x7f0b0019

.field public static final widget_type_photo:I = 0x7f0b001a

.field public static final widget_type_shuffle:I = 0x7f0b001b


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
