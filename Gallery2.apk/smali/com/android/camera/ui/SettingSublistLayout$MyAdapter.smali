.class Lcom/android/camera/ui/SettingSublistLayout$MyAdapter;
.super Landroid/widget/BaseAdapter;
.source "SettingSublistLayout.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/camera/ui/SettingSublistLayout;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "MyAdapter"
.end annotation


# instance fields
.field private mSelectedIndex:I

.field final synthetic this$0:Lcom/android/camera/ui/SettingSublistLayout;


# direct methods
.method public constructor <init>(Lcom/android/camera/ui/SettingSublistLayout;)V
    .locals 0

    iput-object p1, p0, Lcom/android/camera/ui/SettingSublistLayout$MyAdapter;->this$0:Lcom/android/camera/ui/SettingSublistLayout;

    invoke-direct {p0}, Landroid/widget/BaseAdapter;-><init>()V

    return-void
.end method


# virtual methods
.method public getCount()I
    .locals 1

    iget-object v0, p0, Lcom/android/camera/ui/SettingSublistLayout$MyAdapter;->this$0:Lcom/android/camera/ui/SettingSublistLayout;

    invoke-static {v0}, Lcom/android/camera/ui/SettingSublistLayout;->access$000(Lcom/android/camera/ui/SettingSublistLayout;)Lcom/android/camera/ListPreference;

    move-result-object v0

    invoke-virtual {v0}, Lcom/android/camera/ListPreference;->getEntries()[Ljava/lang/CharSequence;

    move-result-object v0

    array-length v0, v0

    return v0
.end method

.method public getItem(I)Ljava/lang/Object;
    .locals 1
    .param p1    # I

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    return-object v0
.end method

.method public getItemId(I)J
    .locals 2
    .param p1    # I

    int-to-long v0, p1

    return-wide v0
.end method

.method public getSelectedIndex()I
    .locals 1

    iget v0, p0, Lcom/android/camera/ui/SettingSublistLayout$MyAdapter;->mSelectedIndex:I

    return v0
.end method

.method public getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 6
    .param p1    # I
    .param p2    # Landroid/view/View;
    .param p3    # Landroid/view/ViewGroup;

    const/4 v5, 0x0

    const/4 v3, 0x0

    const/4 v0, 0x0

    if-nez p2, :cond_0

    iget-object v2, p0, Lcom/android/camera/ui/SettingSublistLayout$MyAdapter;->this$0:Lcom/android/camera/ui/SettingSublistLayout;

    invoke-static {v2}, Lcom/android/camera/ui/SettingSublistLayout;->access$100(Lcom/android/camera/ui/SettingSublistLayout;)Landroid/view/LayoutInflater;

    move-result-object v2

    const v4, 0x7f040057

    invoke-virtual {v2, v4, v5}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object p2

    new-instance v0, Lcom/android/camera/ui/SettingSublistLayout$ViewHolder;

    iget-object v2, p0, Lcom/android/camera/ui/SettingSublistLayout$MyAdapter;->this$0:Lcom/android/camera/ui/SettingSublistLayout;

    invoke-direct {v0, v2, v5}, Lcom/android/camera/ui/SettingSublistLayout$ViewHolder;-><init>(Lcom/android/camera/ui/SettingSublistLayout;Lcom/android/camera/ui/SettingSublistLayout$1;)V

    const v2, 0x7f0b0065

    invoke-virtual {p2, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/ImageView;

    iput-object v2, v0, Lcom/android/camera/ui/SettingSublistLayout$ViewHolder;->mImageView:Landroid/widget/ImageView;

    const v2, 0x7f0b000e

    invoke-virtual {p2, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    iput-object v2, v0, Lcom/android/camera/ui/SettingSublistLayout$ViewHolder;->mTextView:Landroid/widget/TextView;

    const v2, 0x7f0b0131

    invoke-virtual {p2, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/RadioButton;

    iput-object v2, v0, Lcom/android/camera/ui/SettingSublistLayout$ViewHolder;->mRadioButton:Landroid/widget/RadioButton;

    invoke-virtual {p2, v0}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    :goto_0
    iget-object v2, p0, Lcom/android/camera/ui/SettingSublistLayout$MyAdapter;->this$0:Lcom/android/camera/ui/SettingSublistLayout;

    invoke-static {v2}, Lcom/android/camera/ui/SettingSublistLayout;->access$000(Lcom/android/camera/ui/SettingSublistLayout;)Lcom/android/camera/ListPreference;

    move-result-object v2

    invoke-virtual {v2, p1}, Lcom/android/camera/ListPreference;->getIconId(I)I

    move-result v1

    iget-object v2, p0, Lcom/android/camera/ui/SettingSublistLayout$MyAdapter;->this$0:Lcom/android/camera/ui/SettingSublistLayout;

    invoke-static {v2}, Lcom/android/camera/ui/SettingSublistLayout;->access$000(Lcom/android/camera/ui/SettingSublistLayout;)Lcom/android/camera/ListPreference;

    move-result-object v2

    invoke-virtual {v2, p1}, Lcom/android/camera/ListPreference;->getIconId(I)I

    move-result v2

    const/4 v4, -0x1

    if-ne v2, v4, :cond_1

    iget-object v2, v0, Lcom/android/camera/ui/SettingSublistLayout$ViewHolder;->mImageView:Landroid/widget/ImageView;

    const/16 v4, 0x8

    invoke-virtual {v2, v4}, Landroid/widget/ImageView;->setVisibility(I)V

    :goto_1
    iget-object v2, v0, Lcom/android/camera/ui/SettingSublistLayout$ViewHolder;->mTextView:Landroid/widget/TextView;

    iget-object v4, p0, Lcom/android/camera/ui/SettingSublistLayout$MyAdapter;->this$0:Lcom/android/camera/ui/SettingSublistLayout;

    invoke-static {v4}, Lcom/android/camera/ui/SettingSublistLayout;->access$000(Lcom/android/camera/ui/SettingSublistLayout;)Lcom/android/camera/ListPreference;

    move-result-object v4

    invoke-virtual {v4}, Lcom/android/camera/ListPreference;->getEntries()[Ljava/lang/CharSequence;

    move-result-object v4

    aget-object v4, v4, p1

    invoke-virtual {v2, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v4, v0, Lcom/android/camera/ui/SettingSublistLayout$ViewHolder;->mRadioButton:Landroid/widget/RadioButton;

    iget v2, p0, Lcom/android/camera/ui/SettingSublistLayout$MyAdapter;->mSelectedIndex:I

    if-ne p1, v2, :cond_2

    const/4 v2, 0x1

    :goto_2
    invoke-virtual {v4, v2}, Landroid/widget/CompoundButton;->setChecked(Z)V

    return-object p2

    :cond_0
    invoke-virtual {p2}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/camera/ui/SettingSublistLayout$ViewHolder;

    goto :goto_0

    :cond_1
    iget-object v2, v0, Lcom/android/camera/ui/SettingSublistLayout$ViewHolder;->mImageView:Landroid/widget/ImageView;

    invoke-virtual {v2, v3}, Landroid/widget/ImageView;->setVisibility(I)V

    iget-object v2, v0, Lcom/android/camera/ui/SettingSublistLayout$ViewHolder;->mImageView:Landroid/widget/ImageView;

    invoke-virtual {v2, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    goto :goto_1

    :cond_2
    move v2, v3

    goto :goto_2
.end method

.method public setSelectedIndex(I)V
    .locals 0
    .param p1    # I

    iput p1, p0, Lcom/android/camera/ui/SettingSublistLayout$MyAdapter;->mSelectedIndex:I

    return-void
.end method
