.class public Lcom/android/camera/ui/ShutterButton;
.super Lcom/android/camera/ui/RotateImageView;
.source "ShutterButton.java"

# interfaces
.implements Landroid/view/View$OnLongClickListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/camera/ui/ShutterButton$OnShutterButtonListener;
    }
.end annotation


# instance fields
.field private mListener:Lcom/android/camera/ui/ShutterButton$OnShutterButtonListener;

.field private mOldPressed:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/util/AttributeSet;

    invoke-direct {p0, p1, p2}, Lcom/android/camera/ui/RotateImageView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    invoke-virtual {p0, p0}, Landroid/view/View;->setOnLongClickListener(Landroid/view/View$OnLongClickListener;)V

    return-void
.end method

.method static synthetic access$000(Lcom/android/camera/ui/ShutterButton;Z)V
    .locals 0
    .param p0    # Lcom/android/camera/ui/ShutterButton;
    .param p1    # Z

    invoke-direct {p0, p1}, Lcom/android/camera/ui/ShutterButton;->callShutterButtonFocus(Z)V

    return-void
.end method

.method private callShutterButtonFocus(Z)V
    .locals 1
    .param p1    # Z

    iget-object v0, p0, Lcom/android/camera/ui/ShutterButton;->mListener:Lcom/android/camera/ui/ShutterButton$OnShutterButtonListener;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/camera/ui/ShutterButton;->mListener:Lcom/android/camera/ui/ShutterButton$OnShutterButtonListener;

    invoke-interface {v0, p0, p1}, Lcom/android/camera/ui/ShutterButton$OnShutterButtonListener;->onShutterButtonFocus(Lcom/android/camera/ui/ShutterButton;Z)V

    :cond_0
    return-void
.end method


# virtual methods
.method protected drawableStateChanged()V
    .locals 2

    invoke-super {p0}, Landroid/widget/ImageView;->drawableStateChanged()V

    invoke-virtual {p0}, Landroid/view/View;->isPressed()Z

    move-result v0

    iget-boolean v1, p0, Lcom/android/camera/ui/ShutterButton;->mOldPressed:Z

    if-eq v0, v1, :cond_0

    if-nez v0, :cond_1

    new-instance v1, Lcom/android/camera/ui/ShutterButton$1;

    invoke-direct {v1, p0, v0}, Lcom/android/camera/ui/ShutterButton$1;-><init>(Lcom/android/camera/ui/ShutterButton;Z)V

    invoke-virtual {p0, v1}, Landroid/view/View;->post(Ljava/lang/Runnable;)Z

    :goto_0
    iput-boolean v0, p0, Lcom/android/camera/ui/ShutterButton;->mOldPressed:Z

    :cond_0
    return-void

    :cond_1
    invoke-direct {p0, v0}, Lcom/android/camera/ui/ShutterButton;->callShutterButtonFocus(Z)V

    goto :goto_0
.end method

.method public onLongClick(Landroid/view/View;)Z
    .locals 1
    .param p1    # Landroid/view/View;

    iget-object v0, p0, Lcom/android/camera/ui/ShutterButton;->mListener:Lcom/android/camera/ui/ShutterButton$OnShutterButtonListener;

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Landroid/view/View;->isEnabled()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Landroid/view/View;->isClickable()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/camera/ui/ShutterButton;->mListener:Lcom/android/camera/ui/ShutterButton$OnShutterButtonListener;

    invoke-interface {v0, p0}, Lcom/android/camera/ui/ShutterButton$OnShutterButtonListener;->onShutterButtonLongPressed(Lcom/android/camera/ui/ShutterButton;)V

    :cond_0
    const/4 v0, 0x0

    return v0
.end method

.method public performClick()Z
    .locals 2

    invoke-super {p0}, Landroid/view/View;->performClick()Z

    move-result v0

    iget-object v1, p0, Lcom/android/camera/ui/ShutterButton;->mListener:Lcom/android/camera/ui/ShutterButton$OnShutterButtonListener;

    if-eqz v1, :cond_0

    invoke-virtual {p0}, Landroid/view/View;->isEnabled()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-virtual {p0}, Landroid/view/View;->isClickable()Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/android/camera/ui/ShutterButton;->mListener:Lcom/android/camera/ui/ShutterButton$OnShutterButtonListener;

    invoke-interface {v1, p0}, Lcom/android/camera/ui/ShutterButton$OnShutterButtonListener;->onShutterButtonClick(Lcom/android/camera/ui/ShutterButton;)V

    :cond_0
    return v0
.end method

.method public setOnShutterButtonListener(Lcom/android/camera/ui/ShutterButton$OnShutterButtonListener;)V
    .locals 0
    .param p1    # Lcom/android/camera/ui/ShutterButton$OnShutterButtonListener;

    iput-object p1, p0, Lcom/android/camera/ui/ShutterButton;->mListener:Lcom/android/camera/ui/ShutterButton$OnShutterButtonListener;

    return-void
.end method
