.class Lcom/android/camera/Camera$CameraStartUpThread;
.super Ljava/lang/Thread;
.source "Camera.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/camera/Camera;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "CameraStartUpThread"
.end annotation


# instance fields
.field private mCancel:Z

.field final synthetic this$0:Lcom/android/camera/Camera;


# direct methods
.method private constructor <init>(Lcom/android/camera/Camera;)V
    .locals 1

    iput-object p1, p0, Lcom/android/camera/Camera$CameraStartUpThread;->this$0:Lcom/android/camera/Camera;

    invoke-direct {p0}, Ljava/lang/Thread;-><init>()V

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/android/camera/Camera$CameraStartUpThread;->mCancel:Z

    return-void
.end method

.method synthetic constructor <init>(Lcom/android/camera/Camera;Lcom/android/camera/Camera$1;)V
    .locals 0
    .param p1    # Lcom/android/camera/Camera;
    .param p2    # Lcom/android/camera/Camera$1;

    invoke-direct {p0, p1}, Lcom/android/camera/Camera$CameraStartUpThread;-><init>(Lcom/android/camera/Camera;)V

    return-void
.end method


# virtual methods
.method public cancel()V
    .locals 1

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/android/camera/Camera$CameraStartUpThread;->mCancel:Z

    return-void
.end method

.method public isCanceled()Z
    .locals 3

    invoke-static {}, Lcom/android/camera/Camera;->access$000()Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v0, "Camera"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "isCanceled() return "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-boolean v2, p0, Lcom/android/camera/Camera$CameraStartUpThread;->mCancel:Z

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/android/camera/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    iget-boolean v0, p0, Lcom/android/camera/Camera$CameraStartUpThread;->mCancel:Z

    return v0
.end method

.method public run()V
    .locals 11

    const/16 v10, 0xa

    const/16 v9, 0x9

    const/4 v8, 0x4

    const/4 v7, 0x1

    invoke-static {}, Lcom/android/camera/manager/MMProfileManager;->startProfileCameraStartUp()V

    iget-object v4, p0, Lcom/android/camera/Camera$CameraStartUpThread;->this$0:Lcom/android/camera/Camera;

    iget-object v5, p0, Lcom/android/camera/Camera$CameraStartUpThread;->this$0:Lcom/android/camera/Camera;

    invoke-static {v5}, Lcom/android/camera/Camera;->access$1700(Lcom/android/camera/Camera;)Lcom/android/camera/ComboPreferences;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/android/camera/Camera;->access$1800(Lcom/android/camera/Camera;Lcom/android/camera/ComboPreferences;)I

    move-result v0

    iget-object v4, p0, Lcom/android/camera/Camera$CameraStartUpThread;->this$0:Lcom/android/camera/Camera;

    invoke-static {v4}, Lcom/android/camera/Camera;->access$1900(Lcom/android/camera/Camera;)I

    move-result v4

    if-eq v4, v0, :cond_0

    const-string v4, "Camera"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "CameraStartUpThread.run() camera id preference="

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, ", memory="

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget-object v6, p0, Lcom/android/camera/Camera$CameraStartUpThread;->this$0:Lcom/android/camera/Camera;

    invoke-static {v6}, Lcom/android/camera/Camera;->access$1900(Lcom/android/camera/Camera;)I

    move-result v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/android/camera/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v4, p0, Lcom/android/camera/Camera$CameraStartUpThread;->this$0:Lcom/android/camera/Camera;

    invoke-static {v4}, Lcom/android/camera/Camera;->access$1700(Lcom/android/camera/Camera;)Lcom/android/camera/ComboPreferences;

    move-result-object v4

    iget-object v5, p0, Lcom/android/camera/Camera$CameraStartUpThread;->this$0:Lcom/android/camera/Camera;

    invoke-static {v5}, Lcom/android/camera/Camera;->access$1900(Lcom/android/camera/Camera;)I

    move-result v5

    invoke-static {v4, v5}, Lcom/android/camera/CameraSettings;->writePreferredCameraId(Landroid/content/SharedPreferences;I)V

    :cond_0
    iget-object v4, p0, Lcom/android/camera/Camera$CameraStartUpThread;->this$0:Lcom/android/camera/Camera;

    invoke-static {v4}, Lcom/android/camera/Camera;->access$1700(Lcom/android/camera/Camera;)Lcom/android/camera/ComboPreferences;

    move-result-object v4

    invoke-static {v4}, Lcom/android/camera/CameraSettings;->readEffectType(Landroid/content/SharedPreferences;)I

    move-result v3

    iget-object v4, p0, Lcom/android/camera/Camera$CameraStartUpThread;->this$0:Lcom/android/camera/Camera;

    invoke-static {v4}, Lcom/android/camera/Camera;->access$1700(Lcom/android/camera/Camera;)Lcom/android/camera/ComboPreferences;

    move-result-object v4

    invoke-static {v4}, Lcom/android/camera/CameraSettings;->readEffectParameter(Landroid/content/SharedPreferences;)Ljava/lang/Object;

    move-result-object v2

    iget-object v4, p0, Lcom/android/camera/Camera$CameraStartUpThread;->this$0:Lcom/android/camera/Camera;

    invoke-static {v4}, Lcom/android/camera/Camera;->access$2000(Lcom/android/camera/Camera;)I

    move-result v4

    if-ne v3, v4, :cond_2

    if-nez v2, :cond_1

    iget-object v4, p0, Lcom/android/camera/Camera$CameraStartUpThread;->this$0:Lcom/android/camera/Camera;

    invoke-static {v4}, Lcom/android/camera/Camera;->access$2100(Lcom/android/camera/Camera;)Ljava/lang/Object;

    move-result-object v4

    if-nez v4, :cond_2

    :cond_1
    if-eqz v2, :cond_3

    iget-object v4, p0, Lcom/android/camera/Camera$CameraStartUpThread;->this$0:Lcom/android/camera/Camera;

    invoke-static {v4}, Lcom/android/camera/Camera;->access$2100(Lcom/android/camera/Camera;)Ljava/lang/Object;

    move-result-object v4

    invoke-virtual {v2, v4}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_3

    :cond_2
    const-string v4, "Camera"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "CameraStartUpThread.run() preference=("

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, ", "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, ") memory=("

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget-object v6, p0, Lcom/android/camera/Camera$CameraStartUpThread;->this$0:Lcom/android/camera/Camera;

    invoke-static {v6}, Lcom/android/camera/Camera;->access$2000(Lcom/android/camera/Camera;)I

    move-result v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, ", "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget-object v6, p0, Lcom/android/camera/Camera$CameraStartUpThread;->this$0:Lcom/android/camera/Camera;

    invoke-static {v6}, Lcom/android/camera/Camera;->access$2100(Lcom/android/camera/Camera;)Ljava/lang/Object;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, ")"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/android/camera/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v4, p0, Lcom/android/camera/Camera$CameraStartUpThread;->this$0:Lcom/android/camera/Camera;

    invoke-static {v4}, Lcom/android/camera/Camera;->access$1700(Lcom/android/camera/Camera;)Lcom/android/camera/ComboPreferences;

    move-result-object v4

    iget-object v5, p0, Lcom/android/camera/Camera$CameraStartUpThread;->this$0:Lcom/android/camera/Camera;

    invoke-static {v5}, Lcom/android/camera/Camera;->access$2000(Lcom/android/camera/Camera;)I

    move-result v5

    iget-object v6, p0, Lcom/android/camera/Camera$CameraStartUpThread;->this$0:Lcom/android/camera/Camera;

    invoke-static {v6}, Lcom/android/camera/Camera;->access$2100(Lcom/android/camera/Camera;)Ljava/lang/Object;

    move-result-object v6

    invoke-static {v4, v5, v6}, Lcom/android/camera/CameraSettings;->writePreferredVideoEffect(Landroid/content/SharedPreferences;ILjava/lang/Object;)V

    :cond_3
    :try_start_0
    iget-object v4, p0, Lcom/android/camera/Camera$CameraStartUpThread;->this$0:Lcom/android/camera/Camera;

    iget-object v5, p0, Lcom/android/camera/Camera$CameraStartUpThread;->this$0:Lcom/android/camera/Camera;

    iget-object v6, p0, Lcom/android/camera/Camera$CameraStartUpThread;->this$0:Lcom/android/camera/Camera;

    invoke-static {v6}, Lcom/android/camera/Camera;->access$1900(Lcom/android/camera/Camera;)I

    move-result v6

    invoke-static {v5, v6}, Lcom/android/camera/Util;->openCamera(Landroid/app/Activity;I)Lcom/android/camera/CameraManager$CameraProxy;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/android/camera/Camera;->access$2202(Lcom/android/camera/Camera;Lcom/android/camera/CameraManager$CameraProxy;)Lcom/android/camera/CameraManager$CameraProxy;

    iget-object v4, p0, Lcom/android/camera/Camera$CameraStartUpThread;->this$0:Lcom/android/camera/Camera;

    invoke-static {v4}, Lcom/android/camera/Camera;->access$2300(Lcom/android/camera/Camera;)V

    iget-object v4, p0, Lcom/android/camera/Camera$CameraStartUpThread;->this$0:Lcom/android/camera/Camera;

    const/4 v5, 0x1

    invoke-static {v4, v5}, Lcom/android/camera/Camera;->access$2402(Lcom/android/camera/Camera;Z)Z
    :try_end_0
    .catch Lcom/android/camera/CameraHardwareException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Lcom/android/camera/CameraDisabledException; {:try_start_0 .. :try_end_0} :catch_1

    iget-object v4, p0, Lcom/android/camera/Camera$CameraStartUpThread;->this$0:Lcom/android/camera/Camera;

    invoke-static {}, Lcom/android/camera/CameraHolder;->instance()Lcom/android/camera/CameraHolder;

    move-result-object v5

    invoke-virtual {v5}, Lcom/android/camera/CameraHolder;->getOriginalParameters()Landroid/hardware/Camera$Parameters;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/android/camera/Camera;->access$2502(Lcom/android/camera/Camera;Landroid/hardware/Camera$Parameters;)Landroid/hardware/Camera$Parameters;

    invoke-static {}, Lcom/android/camera/manager/MMProfileManager;->startProfileCameraParameterCopy()V

    iget-object v4, p0, Lcom/android/camera/Camera$CameraStartUpThread;->this$0:Lcom/android/camera/Camera;

    iget-object v5, p0, Lcom/android/camera/Camera$CameraStartUpThread;->this$0:Lcom/android/camera/Camera;

    invoke-static {v5}, Lcom/android/camera/Camera;->access$2500(Lcom/android/camera/Camera;)Landroid/hardware/Camera$Parameters;

    move-result-object v5

    invoke-virtual {v5}, Landroid/hardware/Camera$Parameters;->copy()Landroid/hardware/Camera$Parameters;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/android/camera/Camera;->access$2602(Lcom/android/camera/Camera;Landroid/hardware/Camera$Parameters;)Landroid/hardware/Camera$Parameters;

    iget-object v4, p0, Lcom/android/camera/Camera$CameraStartUpThread;->this$0:Lcom/android/camera/Camera;

    iget-object v5, p0, Lcom/android/camera/Camera$CameraStartUpThread;->this$0:Lcom/android/camera/Camera;

    invoke-static {v5}, Lcom/android/camera/Camera;->access$1900(Lcom/android/camera/Camera;)I

    move-result v5

    invoke-static {v4, v5}, Lcom/android/camera/ModeChecker;->updateModeMatrix(Lcom/android/camera/Camera;I)V

    invoke-static {}, Lcom/android/camera/manager/MMProfileManager;->stopProfileCameraParameterCopy()V

    iget-object v4, p0, Lcom/android/camera/Camera$CameraStartUpThread;->this$0:Lcom/android/camera/Camera;

    invoke-static {v4}, Lcom/android/camera/Camera;->access$800(Lcom/android/camera/Camera;)Lcom/android/camera/actor/CameraActor;

    move-result-object v4

    invoke-virtual {v4}, Lcom/android/camera/actor/CameraActor;->onCameraOpenDone()V

    invoke-static {}, Lcom/android/camera/manager/MMProfileManager;->startProfileCameraPreviewPreReadyBlock()V

    iget-object v4, p0, Lcom/android/camera/Camera$CameraStartUpThread;->this$0:Lcom/android/camera/Camera;

    invoke-static {v4}, Lcom/android/camera/Camera;->access$2700(Lcom/android/camera/Camera;)Landroid/os/ConditionVariable;

    move-result-object v4

    invoke-virtual {v4}, Landroid/os/ConditionVariable;->block()V

    invoke-static {}, Lcom/android/camera/manager/MMProfileManager;->stopProfileCameraPreviewPreReadyBlock()V

    iget-boolean v4, p0, Lcom/android/camera/Camera$CameraStartUpThread;->mCancel:Z

    if-eqz v4, :cond_6

    invoke-static {}, Lcom/android/camera/manager/MMProfileManager;->stopProfileCameraStartUp()V

    :goto_0
    return-void

    :catch_0
    move-exception v1

    iget-object v4, p0, Lcom/android/camera/Camera$CameraStartUpThread;->this$0:Lcom/android/camera/Camera;

    invoke-static {v4}, Lcom/android/camera/Camera;->access$800(Lcom/android/camera/Camera;)Lcom/android/camera/actor/CameraActor;

    move-result-object v4

    invoke-virtual {v4}, Lcom/android/camera/actor/CameraActor;->onCameraOpenFailed()V

    iget-object v4, p0, Lcom/android/camera/Camera$CameraStartUpThread;->this$0:Lcom/android/camera/Camera;

    invoke-static {v4, v7}, Lcom/android/camera/Camera;->access$1502(Lcom/android/camera/Camera;Z)Z

    iget-object v4, p0, Lcom/android/camera/Camera$CameraStartUpThread;->this$0:Lcom/android/camera/Camera;

    invoke-static {v4}, Lcom/android/camera/Camera;->access$1000(Lcom/android/camera/Camera;)Landroid/os/Handler;

    move-result-object v4

    invoke-virtual {v4, v9}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    invoke-static {}, Lcom/android/camera/Camera;->access$000()Z

    move-result v4

    if-eqz v4, :cond_4

    invoke-static {v9}, Lcom/android/camera/Camera;->access$100(I)Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Lcom/android/camera/manager/MMProfileManager;->triggersSendMessage(Ljava/lang/String;)V

    :cond_4
    invoke-static {}, Lcom/android/camera/manager/MMProfileManager;->stopProfileCameraStartUp()V

    goto :goto_0

    :catch_1
    move-exception v1

    iget-object v4, p0, Lcom/android/camera/Camera$CameraStartUpThread;->this$0:Lcom/android/camera/Camera;

    invoke-static {v4}, Lcom/android/camera/Camera;->access$800(Lcom/android/camera/Camera;)Lcom/android/camera/actor/CameraActor;

    move-result-object v4

    invoke-virtual {v4}, Lcom/android/camera/actor/CameraActor;->onCameraDisabled()V

    iget-object v4, p0, Lcom/android/camera/Camera$CameraStartUpThread;->this$0:Lcom/android/camera/Camera;

    invoke-static {v4, v7}, Lcom/android/camera/Camera;->access$1602(Lcom/android/camera/Camera;Z)Z

    iget-object v4, p0, Lcom/android/camera/Camera$CameraStartUpThread;->this$0:Lcom/android/camera/Camera;

    invoke-static {v4}, Lcom/android/camera/Camera;->access$1000(Lcom/android/camera/Camera;)Landroid/os/Handler;

    move-result-object v4

    invoke-virtual {v4, v10}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    invoke-static {}, Lcom/android/camera/Camera;->access$000()Z

    move-result v4

    if-eqz v4, :cond_5

    invoke-static {v10}, Lcom/android/camera/Camera;->access$100(I)Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Lcom/android/camera/manager/MMProfileManager;->triggersSendMessage(Ljava/lang/String;)V

    :cond_5
    invoke-static {}, Lcom/android/camera/manager/MMProfileManager;->stopProfileCameraStartUp()V

    goto :goto_0

    :cond_6
    iget-object v4, p0, Lcom/android/camera/Camera$CameraStartUpThread;->this$0:Lcom/android/camera/Camera;

    invoke-static {v4}, Lcom/android/camera/Camera;->access$2800(Lcom/android/camera/Camera;)V

    iget-object v4, p0, Lcom/android/camera/Camera$CameraStartUpThread;->this$0:Lcom/android/camera/Camera;

    invoke-static {v4}, Lcom/android/camera/Camera;->access$600(Lcom/android/camera/Camera;)V

    iget-object v4, p0, Lcom/android/camera/Camera$CameraStartUpThread;->this$0:Lcom/android/camera/Camera;

    invoke-static {v4}, Lcom/android/camera/Camera;->access$2900(Lcom/android/camera/Camera;)V

    iget-object v4, p0, Lcom/android/camera/Camera$CameraStartUpThread;->this$0:Lcom/android/camera/Camera;

    invoke-static {v4}, Lcom/android/camera/Camera;->access$3000(Lcom/android/camera/Camera;)V

    iget-object v4, p0, Lcom/android/camera/Camera$CameraStartUpThread;->this$0:Lcom/android/camera/Camera;

    invoke-static {v4}, Lcom/android/camera/Camera;->access$3100(Lcom/android/camera/Camera;)V

    iget-object v4, p0, Lcom/android/camera/Camera$CameraStartUpThread;->this$0:Lcom/android/camera/Camera;

    invoke-static {v4}, Lcom/android/camera/Camera;->access$3200(Lcom/android/camera/Camera;)V

    iget-object v4, p0, Lcom/android/camera/Camera$CameraStartUpThread;->this$0:Lcom/android/camera/Camera;

    invoke-static {v4}, Lcom/android/camera/Camera;->access$3300(Lcom/android/camera/Camera;)V

    iget-object v4, p0, Lcom/android/camera/Camera$CameraStartUpThread;->this$0:Lcom/android/camera/Camera;

    invoke-static {v4, v7}, Lcom/android/camera/Camera;->access$1400(Lcom/android/camera/Camera;Z)V

    iget-object v4, p0, Lcom/android/camera/Camera$CameraStartUpThread;->this$0:Lcom/android/camera/Camera;

    invoke-static {v4}, Lcom/android/camera/Camera;->access$3400(Lcom/android/camera/Camera;)Lcom/android/camera/SettingChecker;

    move-result-object v4

    invoke-virtual {v4}, Lcom/android/camera/SettingChecker;->applyParametersToUIImmediately()V

    iget-object v4, p0, Lcom/android/camera/Camera$CameraStartUpThread;->this$0:Lcom/android/camera/Camera;

    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v5

    invoke-static {v4, v5, v6}, Lcom/android/camera/Camera;->access$902(Lcom/android/camera/Camera;J)J

    iget-object v4, p0, Lcom/android/camera/Camera$CameraStartUpThread;->this$0:Lcom/android/camera/Camera;

    invoke-static {v4}, Lcom/android/camera/Camera;->access$1000(Lcom/android/camera/Camera;)Landroid/os/Handler;

    move-result-object v4

    invoke-virtual {v4, v8}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    invoke-static {}, Lcom/android/camera/Camera;->access$000()Z

    move-result v4

    if-eqz v4, :cond_7

    invoke-static {v8}, Lcom/android/camera/Camera;->access$100(I)Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Lcom/android/camera/manager/MMProfileManager;->triggersSendMessage(Ljava/lang/String;)V

    :cond_7
    iget-object v4, p0, Lcom/android/camera/Camera$CameraStartUpThread;->this$0:Lcom/android/camera/Camera;

    invoke-static {v4}, Lcom/android/camera/Camera;->access$1000(Lcom/android/camera/Camera;)Landroid/os/Handler;

    move-result-object v4

    invoke-virtual {v4, v7}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    invoke-static {}, Lcom/android/camera/Camera;->access$000()Z

    move-result v4

    if-eqz v4, :cond_8

    invoke-static {v7}, Lcom/android/camera/Camera;->access$100(I)Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Lcom/android/camera/manager/MMProfileManager;->triggersSendMessage(Ljava/lang/String;)V

    :cond_8
    invoke-static {}, Lcom/android/camera/manager/MMProfileManager;->stopProfileCameraStartUp()V

    goto/16 :goto_0
.end method
