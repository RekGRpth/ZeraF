.class public Lcom/android/camera/CameraSettings;
.super Ljava/lang/Object;
.source "CameraSettings.java"


# static fields
.field public static final COLOR_EFFECT_NONE:Ljava/lang/String; = "none"

.field public static final CURRENT_LOCAL_VERSION:I = 0x2

.field public static final CURRENT_VERSION:I = 0x5

.field public static final DEFAULT_CAPTURE_NUM:Ljava/lang/String; = "40"

.field public static final DEFAULT_CAPTURE_NUM_MAX:Ljava/lang/String; = "99"

.field public static final DEFAULT_CONINUOUS_CAPTURE_NUM:Ljava/lang/String; = "20"

.field public static final DEFAULT_VIDEO_DURATION:I = 0x0

.field public static final DIP_HIGH:Ljava/lang/String; = "high"

.field public static final DIP_LOW:Ljava/lang/String; = "low"

.field public static final DIP_MEDIUM:Ljava/lang/String; = "middle"

.field public static final EXPOSURE_DEFAULT_VALUE:Ljava/lang/String; = "0"

.field public static final FACE_DETECTION_DEFAULT:Ljava/lang/String; = "on"

.field public static final FOCUS_METER_SPOT:Ljava/lang/String; = "spot"

.field public static final IMG_SIZE_FOR_HIGH_ISO:Ljava/lang/String; = "1280x960"

.field public static final IMG_SIZE_FOR_HIGH_ISO_ARRAYS:[Ljava/lang/String; = null

.field public static final IMG_SIZE_FOR_PANORAMA:Ljava/lang/String; = "1600x1200"

.field public static final ISO_AUTO:Ljava/lang/String; = "auto"

.field public static final ISO_SPEED_1600:Ljava/lang/String; = "1600"

.field public static final ISO_SPEED_800:Ljava/lang/String; = "800"

.field public static final KEY_ANDROID_INSPACE:Ljava/lang/String; = "backdropper/file:///system/media/video/AndroidInSpace.480p.mp4"

.field public static final KEY_ANTI_BANDING:Ljava/lang/String; = "pref_camera_antibanding_key"

.field public static final KEY_BRIGHTNESS:Ljava/lang/String; = "pref_camera_brightness_key"

.field public static final KEY_CAMERA_FACE_DETECT:Ljava/lang/String; = "pref_face_detect_key"

.field public static final KEY_CAMERA_ID:Ljava/lang/String; = "pref_camera_id_key"

.field public static final KEY_CAMERA_ZSD:Ljava/lang/String; = "pref_camera_zsd_key"

.field public static final KEY_COLOR_EFFECT:Ljava/lang/String; = "pref_camera_coloreffect_key"

.field public static final KEY_CONTINUOUS_NUMBER:Ljava/lang/String; = "pref_camera_shot_number"

.field public static final KEY_CONTRAST:Ljava/lang/String; = "pref_camera_contrast_key"

.field public static final KEY_EDGE:Ljava/lang/String; = "pref_camera_edge_key"

.field public static final KEY_EXPOSURE:Ljava/lang/String; = "pref_camera_exposure_key"

.field public static final KEY_FACE_BEAUTY_PROPERTIES:Ljava/lang/String; = "pref_camera_facebeauty_properties_key"

.field public static final KEY_FACE_BEAUTY_SHARP:Ljava/lang/String; = "pref_facebeauty_sharp_key"

.field public static final KEY_FACE_BEAUTY_SKIN_COLOR:Ljava/lang/String; = "pref_facebeauty_skin_color_key"

.field public static final KEY_FACE_BEAUTY_SMOOTH:Ljava/lang/String; = "pref_facebeauty_smooth_key"

.field public static final KEY_FD_MODE:Ljava/lang/String; = "pref_camera_fd_key"

.field public static final KEY_FLASH_MODE:Ljava/lang/String; = "pref_camera_flashmode_key"

.field public static final KEY_HUE:Ljava/lang/String; = "pref_camera_hue_key"

.field public static final KEY_IMAGE_PROPERTIES:Ljava/lang/String; = "pref_camera_image_properties_key"

.field public static final KEY_ISO:Ljava/lang/String; = "pref_camera_iso_key"

.field public static final KEY_JPEG_QUALITY:Ljava/lang/String; = "pref_camera_jpegquality_key"

.field public static final KEY_LOCAL_VERSION:Ljava/lang/String; = "pref_local_version_key"

.field public static final KEY_NORMAL_CAPTURE_KEY:Ljava/lang/String; = "pref_camera_normal_capture_key"

.field public static final KEY_PICTURE_RATIO:Ljava/lang/String; = "pref_camera_picturesize_ratio_key"

.field public static final KEY_PICTURE_SIZE:Ljava/lang/String; = "pref_camera_picturesize_key"

.field public static final KEY_RECORD_LOCATION:Ljava/lang/String; = "pref_camera_recordlocation_key"

.field public static final KEY_SATURATION:Ljava/lang/String; = "pref_camera_saturation_key"

.field public static final KEY_SCENE_MODE:Ljava/lang/String; = "pref_camera_scenemode_key"

.field public static final KEY_SELF_TIMER:Ljava/lang/String; = "pref_camera_self_timer_key"

.field public static final KEY_STEREO3D_MODE:Ljava/lang/String; = "pref_stereo3d_mode_key"

.field public static final KEY_STEREO3D_PICTURE_FORMAT:Ljava/lang/String; = "pref_camera_pictureformat_key"

.field public static final KEY_STEREO3D_PICTURE_SIZE:Ljava/lang/String; = "pref_camera_picturesize_stereo3d_key"

.field public static final KEY_SUN_SET:Ljava/lang/String; = "backdropper/file:///system/media/video/Sunset.480p.mp4"

.field public static final KEY_VERSION:Ljava/lang/String; = "pref_version_key"

.field public static final KEY_VIDEO_EFFECT:Ljava/lang/String; = "pref_video_effect_key"

.field public static final KEY_VIDEO_EIS:Ljava/lang/String; = "pref_video_eis_key"

.field public static final KEY_VIDEO_HDR:Ljava/lang/String; = "pref_video_hdr_key"

.field public static final KEY_VIDEO_HD_AUDIO_RECORDING:Ljava/lang/String; = "pref_camera_video_hd_recording_key"

.field public static final KEY_VIDEO_QUALITY:Ljava/lang/String; = "pref_video_quality_key"

.field public static final KEY_VIDEO_RECORD_AUDIO:Ljava/lang/String; = "pref_camera_recordaudio_key"

.field public static final KEY_VIDEO_TIME_LAPSE_FRAME_INTERVAL:Ljava/lang/String; = "pref_video_time_lapse_frame_interval_key"

.field public static final KEY_VOICE:Ljava/lang/String; = "pref_voice_key"

.field public static final KEY_WHITE_BALANCE:Ljava/lang/String; = "pref_camera_whitebalance_key"

.field private static final LOG:Z

.field public static final MAX_ISO_SPEED:Ljava/lang/String; = "1600"

.field private static final NOT_FOUND:I = -0x1

.field public static final PICTURE_RATIO_16_9:Ljava/lang/String; = "1.7778"

.field public static final PICTURE_RATIO_3_2:Ljava/lang/String; = "1.5"

.field public static final PICTURE_RATIO_4_3:Ljava/lang/String; = "1.3333"

.field public static final PICTURE_RATIO_5_3:Ljava/lang/String; = "1.6667"

.field public static final PICTURE_SIZE_16_9:[Ljava/lang/String;

.field public static final PICTURE_SIZE_3_2:[Ljava/lang/String;

.field public static final PICTURE_SIZE_4_3:[Ljava/lang/String;

.field public static final PICTURE_SIZE_5_3:[Ljava/lang/String;

.field public static final RATIOS:[D

.field public static final SELF_TIMER_OFF:Ljava/lang/String; = "0"

.field public static final STEREO3D_DISABLE:Ljava/lang/String; = "0"

.field public static final STEREO3D_ENABLE:Ljava/lang/String; = "1"

.field public static final SUPPORTED_SHOW_CONINUOUS_SHOT_NUMBER:Z

.field private static final TAG:Ljava/lang/String; = "CameraSettings"

.field public static final VIDEO_MICRIPHONE_ON:Ljava/lang/String; = "on"

.field public static final WHITE_BALANCE_AUTO:Ljava/lang/String; = "auto"


# instance fields
.field private final mCameraId:I

.field private final mCameraInfo:[Landroid/hardware/Camera$CameraInfo;

.field private final mContext:Lcom/android/camera/Camera;

.field private final mParameters:Landroid/hardware/Camera$Parameters;


# direct methods
.method static constructor <clinit>()V
    .locals 8

    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v1, 0x1

    const/4 v2, 0x0

    invoke-static {}, Lcom/android/camera/FeatureSwitcher;->isLcaRAM()Z

    move-result v0

    if-nez v0, :cond_0

    move v0, v1

    :goto_0
    sput-boolean v0, Lcom/android/camera/CameraSettings;->SUPPORTED_SHOW_CONINUOUS_SHOT_NUMBER:Z

#    new-array v0, v6, [Ljava/lang/String;

#    const-string v3, "1280x960"

#    aput-object v3, v0, v2

#    const-string v3, "1280x720"

#    aput-object v3, v0, v1

#    const-string v3, "1280x768"

#    aput-object v3, v0, v5

#    sput-object v0, Lcom/android/camera/CameraSettings;->IMG_SIZE_FOR_HIGH_ISO_ARRAYS:[Ljava/lang/String;

    const/16 v0, 0x9

    new-array v0, v0, [Ljava/lang/String;

    const-string v3, "320x240"

    aput-object v3, v0, v2

    const-string v3, "640x480"

    aput-object v3, v0, v1

    const-string v3, "1024x768"

    aput-object v3, v0, v5

    const-string v3, "1280x960"

    aput-object v3, v0, v6

    const-string v3, "1600x1200"

    aput-object v3, v0, v7

    const/4 v3, 0x5

    const-string v4, "2048x1536"

    aput-object v4, v0, v3

    const/4 v3, 0x6

    const-string v4, "2560x1920"

    aput-object v4, v0, v3

    const/4 v3, 0x7

    const-string v4, "3264x2448"

    aput-object v4, v0, v3

    const/16 v3, 0x8

    const-string v4, "4096x3072"

    aput-object v4, v0, v3

    sput-object v0, Lcom/android/camera/CameraSettings;->PICTURE_SIZE_4_3:[Ljava/lang/String;

    new-array v0, v7, [Ljava/lang/String;

    const-string v3, "1280x720"

    aput-object v3, v0, v2

    const-string v3, "2048x1152"

    aput-object v3, v0, v1

    const-string v3, "2560x1440"

    aput-object v3, v0, v5

    const-string v3, "3328x1872"

    aput-object v3, v0, v6

    sput-object v0, Lcom/android/camera/CameraSettings;->PICTURE_SIZE_16_9:[Ljava/lang/String;

    new-array v0, v7, [Ljava/lang/String;

    const-string v3, "1280x768"

    aput-object v3, v0, v2

    const-string v3, "1600x960"

    aput-object v3, v0, v1

    const-string v3, "2880x1728"

    aput-object v3, v0, v5

    const-string v3, "3600x2160"

    aput-object v3, v0, v6

    sput-object v0, Lcom/android/camera/CameraSettings;->PICTURE_SIZE_5_3:[Ljava/lang/String;

    new-array v0, v6, [Ljava/lang/String;

    const-string v3, "1440x960"

    aput-object v3, v0, v2

    const-string v2, "2048x1360"

    aput-object v2, v0, v1

    const-string v1, "2560x1712"

    aput-object v1, v0, v5

    sput-object v0, Lcom/android/camera/CameraSettings;->PICTURE_SIZE_3_2:[Ljava/lang/String;

    new-array v0, v7, [D

    fill-array-data v0, :array_0

    sput-object v0, Lcom/android/camera/CameraSettings;->RATIOS:[D

    sget-boolean v0, Lcom/android/camera/Log;->LOGV:Z

    sput-boolean v0, Lcom/android/camera/CameraSettings;->LOG:Z

    return-void

    :cond_0
    move v0, v2

    goto/16 :goto_0

    :array_0
    .array-data 8
        0x3ff55532617c1bdaL
        0x3ff8000000000000L
        0x3ffaaacd9e83e426L
        0x3ffc71de69ad42c4L
    .end array-data
.end method

.method public constructor <init>(Lcom/android/camera/Camera;Landroid/hardware/Camera$Parameters;I[Landroid/hardware/Camera$CameraInfo;)V
    .locals 0
    .param p1    # Lcom/android/camera/Camera;
    .param p2    # Landroid/hardware/Camera$Parameters;
    .param p3    # I
    .param p4    # [Landroid/hardware/Camera$CameraInfo;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/android/camera/CameraSettings;->mContext:Lcom/android/camera/Camera;

    iput-object p2, p0, Lcom/android/camera/CameraSettings;->mParameters:Landroid/hardware/Camera$Parameters;

    iput p3, p0, Lcom/android/camera/CameraSettings;->mCameraId:I

    iput-object p4, p0, Lcom/android/camera/CameraSettings;->mCameraInfo:[Landroid/hardware/Camera$CameraInfo;

    return-void
.end method

.method private buildCameraId(Lcom/android/camera/PreferenceGroup;Lcom/android/camera/IconListPreference;I)V
    .locals 8
    .param p1    # Lcom/android/camera/PreferenceGroup;
    .param p2    # Lcom/android/camera/IconListPreference;
    .param p3    # I

    const/4 v7, 0x2

    const/4 v5, 0x0

    const/4 v4, 0x1

    iget-object v6, p0, Lcom/android/camera/CameraSettings;->mCameraInfo:[Landroid/hardware/Camera$CameraInfo;

    array-length v3, v6

    if-ge v3, v7, :cond_0

    invoke-virtual {p2}, Lcom/android/camera/ListPreference;->getKey()Ljava/lang/String;

    move-result-object v4

    invoke-direct {p0, p1, v4, p3}, Lcom/android/camera/CameraSettings;->removePreference(Lcom/android/camera/PreferenceGroup;Ljava/lang/String;I)Z

    :goto_0
    return-void

    :cond_0
    new-array v0, v7, [Ljava/lang/CharSequence;

    const/4 v1, 0x0

    :goto_1
    iget-object v6, p0, Lcom/android/camera/CameraSettings;->mCameraInfo:[Landroid/hardware/Camera$CameraInfo;

    array-length v6, v6

    if-ge v1, v6, :cond_1

    iget-object v6, p0, Lcom/android/camera/CameraSettings;->mCameraInfo:[Landroid/hardware/Camera$CameraInfo;

    aget-object v6, v6, v1

    iget v6, v6, Landroid/hardware/Camera$CameraInfo;->facing:I

    if-ne v6, v4, :cond_2

    move v2, v4

    :goto_2
    aget-object v6, v0, v2

    if-nez v6, :cond_4

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, ""

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    aput-object v6, v0, v2

    if-ne v2, v4, :cond_3

    move v6, v5

    :goto_3
    aget-object v6, v0, v6

    if-eqz v6, :cond_4

    :cond_1
    invoke-virtual {p2, v0}, Lcom/android/camera/ListPreference;->setEntryValues([Ljava/lang/CharSequence;)V

    invoke-direct {p0, p3, p2}, Lcom/android/camera/CameraSettings;->setListPreference(ILcom/android/camera/ListPreference;)V

    goto :goto_0

    :cond_2
    move v2, v5

    goto :goto_2

    :cond_3
    move v6, v4

    goto :goto_3

    :cond_4
    add-int/lit8 v1, v1, 0x1

    goto :goto_1
.end method

.method private buildExposureCompensation(Lcom/android/camera/PreferenceGroup;Lcom/android/camera/ListPreference;I)V
    .locals 9
    .param p1    # Lcom/android/camera/PreferenceGroup;
    .param p2    # Lcom/android/camera/ListPreference;
    .param p3    # I

    iget-object v8, p0, Lcom/android/camera/CameraSettings;->mParameters:Landroid/hardware/Camera$Parameters;

    invoke-virtual {v8}, Landroid/hardware/Camera$Parameters;->getMaxExposureCompensation()I

    move-result v2

    iget-object v8, p0, Lcom/android/camera/CameraSettings;->mParameters:Landroid/hardware/Camera$Parameters;

    invoke-virtual {v8}, Landroid/hardware/Camera$Parameters;->getMinExposureCompensation()I

    move-result v4

    if-nez v2, :cond_0

    if-nez v4, :cond_0

    invoke-virtual {p2}, Lcom/android/camera/ListPreference;->getKey()Ljava/lang/String;

    move-result-object v8

    invoke-direct {p0, p1, v8, p3}, Lcom/android/camera/CameraSettings;->removePreference(Lcom/android/camera/PreferenceGroup;Ljava/lang/String;I)Z

    :goto_0
    return-void

    :cond_0
    iget-object v8, p0, Lcom/android/camera/CameraSettings;->mParameters:Landroid/hardware/Camera$Parameters;

    invoke-virtual {v8}, Landroid/hardware/Camera$Parameters;->getExposureCompensationStep()F

    move-result v6

    int-to-float v8, v2

    mul-float/2addr v8, v6

    invoke-static {v8}, Landroid/util/FloatMath;->floor(F)F

    move-result v8

    float-to-int v3, v8

    int-to-float v8, v4

    mul-float/2addr v8, v6

    invoke-static {v8}, Landroid/util/FloatMath;->ceil(F)F

    move-result v8

    float-to-int v5, v8

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    move v1, v5

    :goto_1
    if-gt v1, v3, :cond_1

    int-to-float v8, v1

    div-float/2addr v8, v6

    invoke-static {v8}, Ljava/lang/Math;->round(F)I

    move-result v8

    invoke-static {v8}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v7

    invoke-static {v7}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v0, v8}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    :cond_1
    invoke-virtual {p2, v0}, Lcom/android/camera/ListPreference;->filterUnsupported(Ljava/util/List;)V

    invoke-direct {p0, p3, p2}, Lcom/android/camera/CameraSettings;->setListPreference(ILcom/android/camera/ListPreference;)V

    goto :goto_0
.end method

.method private buildFaceBeautyPreference(Lcom/android/camera/PreferenceGroup;ILcom/android/camera/ListPreference;I)V
    .locals 5
    .param p1    # Lcom/android/camera/PreferenceGroup;
    .param p2    # I
    .param p3    # Lcom/android/camera/ListPreference;
    .param p4    # I

    iget-object v4, p0, Lcom/android/camera/CameraSettings;->mParameters:Landroid/hardware/Camera$Parameters;

    invoke-static {v4, p2}, Lcom/android/camera/ParametersHelper;->getMaxLevel(Landroid/hardware/Camera$Parameters;I)I

    move-result v2

    iget-object v4, p0, Lcom/android/camera/CameraSettings;->mParameters:Landroid/hardware/Camera$Parameters;

    invoke-static {v4, p2}, Lcom/android/camera/ParametersHelper;->getMinLevel(Landroid/hardware/Camera$Parameters;I)I

    move-result v3

    if-nez v2, :cond_0

    if-nez v3, :cond_0

    invoke-virtual {p3}, Lcom/android/camera/ListPreference;->getKey()Ljava/lang/String;

    move-result-object v4

    invoke-direct {p0, p1, v4, p4}, Lcom/android/camera/CameraSettings;->removePreference(Lcom/android/camera/PreferenceGroup;Ljava/lang/String;I)Z

    :goto_0
    return-void

    :cond_0
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    move v1, v3

    :goto_1
    if-gt v1, v2, :cond_1

    invoke-static {v1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    :cond_1
    invoke-virtual {p3, v0}, Lcom/android/camera/ListPreference;->filterUnsupported(Ljava/util/List;)V

    invoke-direct {p0, p4, p3}, Lcom/android/camera/CameraSettings;->setListPreference(ILcom/android/camera/ListPreference;)V

    goto :goto_0
.end method

.method private static buildPreviewRatios(Landroid/content/Context;Landroid/hardware/Camera$Parameters;)Ljava/util/List;
    .locals 7
    .param p0    # Landroid/content/Context;
    .param p1    # Landroid/hardware/Camera$Parameters;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Landroid/hardware/Camera$Parameters;",
            ")",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    const/4 v2, 0x0

    if-eqz p0, :cond_0

    if-eqz p1, :cond_0

    invoke-static {p0, p1}, Lcom/android/camera/CameraSettings;->findFullscreenRatio(Landroid/content/Context;Landroid/hardware/Camera$Parameters;)D

    move-result-wide v0

    const-wide v4, 0x3ff5555555555555L

    invoke-static {v4, v5}, Lcom/android/camera/SettingUtils;->getRatioString(D)Ljava/lang/String;

    move-result-object v4

    invoke-interface {v3, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    invoke-static {v0, v1}, Lcom/android/camera/SettingUtils;->getRatioString(D)Ljava/lang/String;

    move-result-object v2

    invoke-interface {v3, v2}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_0

    invoke-interface {v3, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :cond_0
    sget-boolean v4, Lcom/android/camera/CameraSettings;->LOG:Z

    if-eqz v4, :cond_1

    const-string v4, "CameraSettings"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "buildPreviewRatios("

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, ") add supportedRatio "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/android/camera/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    :cond_1
    return-object v3
.end method

.method private buildSupportedListperference(Ljava/util/List;I)V
    .locals 2
    .param p2    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;I)V"
        }
    .end annotation

    iget-object v1, p0, Lcom/android/camera/CameraSettings;->mContext:Lcom/android/camera/Camera;

    invoke-virtual {v1}, Lcom/android/camera/Camera;->getSettingChecker()Lcom/android/camera/SettingChecker;

    move-result-object v1

    invoke-virtual {v1, p2}, Lcom/android/camera/SettingChecker;->getListPreference(I)Lcom/android/camera/ListPreference;

    move-result-object v0

    if-eqz v0, :cond_0

    if-eqz p1, :cond_0

    invoke-virtual {v0}, Lcom/android/camera/ListPreference;->getKey()Ljava/lang/String;

    move-result-object v1

    invoke-interface {p1, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :cond_0
    return-void
.end method

.method public static buildSupportedPictureSize(Landroid/content/Context;Landroid/hardware/Camera$Parameters;Ljava/lang/String;)Ljava/util/List;
    .locals 12
    .param p0    # Landroid/content/Context;
    .param p1    # Landroid/hardware/Camera$Parameters;
    .param p2    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Landroid/hardware/Camera$Parameters;",
            "Ljava/lang/String;",
            ")",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    const-wide/16 v4, 0x0

    if-nez p2, :cond_1

    invoke-static {p0, p1}, Lcom/android/camera/CameraSettings;->findFullscreenRatio(Landroid/content/Context;Landroid/hardware/Camera$Parameters;)D

    move-result-wide v4

    :goto_0
    invoke-virtual {p1}, Landroid/hardware/Camera$Parameters;->getSupportedPictureSizes()Ljava/util/List;

    move-result-object v7

    if-eqz v7, :cond_2

    invoke-interface {v7}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_0
    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v8

    if-eqz v8, :cond_2

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Landroid/hardware/Camera$Size;

    iget v8, v6, Landroid/hardware/Camera$Size;->width:I

    int-to-double v8, v8

    iget v10, v6, Landroid/hardware/Camera$Size;->height:I

    int-to-double v10, v10

    div-double/2addr v8, v10

    invoke-static {v4, v5, v8, v9}, Lcom/android/camera/CameraSettings;->toleranceRatio(DD)Z

    move-result v8

    if-eqz v8, :cond_0

    iget v8, v6, Landroid/hardware/Camera$Size;->width:I

    iget v9, v6, Landroid/hardware/Camera$Size;->height:I

    invoke-static {v8, v9}, Lcom/android/camera/SettingUtils;->buildSize(II)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v3, v8}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_1

    :cond_1
    :try_start_0
    invoke-static {p2}, Ljava/lang/Double;->parseDouble(Ljava/lang/String;)D
    :try_end_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-wide v4

    goto :goto_0

    :catch_0
    move-exception v1

    const-string v8, "CameraSettings"

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "buildSupportedPictureSize() bad ratio: "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v8, v9, v1}, Lcom/android/camera/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    invoke-static {p0, p1}, Lcom/android/camera/CameraSettings;->findFullscreenRatio(Landroid/content/Context;Landroid/hardware/Camera$Parameters;)D

    move-result-wide v4

    goto :goto_0

    :cond_2
    sget-boolean v8, Lcom/android/camera/CameraSettings;->LOG:Z

    if-eqz v8, :cond_3

    const-string v8, "CameraSettings"

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "buildSupportedPictureSize("

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, ", "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, ")"

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    move-result v10

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v8, v9}, Lcom/android/camera/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    invoke-virtual {v3}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_2
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v8

    if-eqz v8, :cond_3

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    const-string v8, "CameraSettings"

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "buildSupportedPictureSize() add "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v8, v9}, Lcom/android/camera/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_2

    :cond_3
    return-object v3
.end method

.method public static clearEffects(Landroid/content/SharedPreferences;Ljava/lang/String;)V
    .locals 3
    .param p0    # Landroid/content/SharedPreferences;
    .param p1    # Ljava/lang/String;

    const-string v1, "1"

    invoke-virtual {v1, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {p0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string v1, "pref_video_effect_key"

    const-string v2, "none"

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->apply()V

    :cond_0
    return-void
.end method

.method private filterDisabledOptions(Lcom/android/camera/PreferenceGroup;Lcom/android/camera/ListPreference;Ljava/util/List;ZI)V
    .locals 2
    .param p1    # Lcom/android/camera/PreferenceGroup;
    .param p2    # Lcom/android/camera/ListPreference;
    .param p4    # Z
    .param p5    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/android/camera/PreferenceGroup;",
            "Lcom/android/camera/ListPreference;",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;ZI)V"
        }
    .end annotation

    const/4 v1, 0x1

    if-eqz p3, :cond_0

    invoke-interface {p3}, Ljava/util/List;->size()I

    move-result v0

    if-ge v0, v1, :cond_1

    :cond_0
    invoke-virtual {p2}, Lcom/android/camera/ListPreference;->getKey()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, p1, v0, p5}, Lcom/android/camera/CameraSettings;->removePreference(Lcom/android/camera/PreferenceGroup;Ljava/lang/String;I)Z

    :goto_0
    return-void

    :cond_1
    invoke-virtual {p2, p3}, Lcom/android/camera/ListPreference;->filterDisabled(Ljava/util/List;)V

    invoke-virtual {p2}, Lcom/android/camera/ListPreference;->getEntries()[Ljava/lang/CharSequence;

    move-result-object v0

    array-length v0, v0

    if-ge v0, v1, :cond_2

    invoke-virtual {p2}, Lcom/android/camera/ListPreference;->getKey()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, p1, v0, p5}, Lcom/android/camera/CameraSettings;->removePreference(Lcom/android/camera/PreferenceGroup;Ljava/lang/String;I)Z

    goto :goto_0

    :cond_2
    invoke-direct {p0, p2, p4}, Lcom/android/camera/CameraSettings;->resetIfInvalid(Lcom/android/camera/ListPreference;Z)V

    invoke-direct {p0, p5, p2}, Lcom/android/camera/CameraSettings;->setListPreference(ILcom/android/camera/ListPreference;)V

    goto :goto_0
.end method

.method private filterUnsupportedEntries(Lcom/android/camera/PreferenceGroup;Lcom/android/camera/ListPreference;Ljava/util/List;ZI)V
    .locals 1
    .param p1    # Lcom/android/camera/PreferenceGroup;
    .param p2    # Lcom/android/camera/ListPreference;
    .param p4    # Z
    .param p5    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/android/camera/PreferenceGroup;",
            "Lcom/android/camera/ListPreference;",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;ZI)V"
        }
    .end annotation

    if-eqz p3, :cond_0

    invoke-interface {p3}, Ljava/util/List;->size()I

    move-result v0

    if-gtz v0, :cond_1

    :cond_0
    invoke-virtual {p2}, Lcom/android/camera/ListPreference;->getKey()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, p1, v0, p5}, Lcom/android/camera/CameraSettings;->removePreference(Lcom/android/camera/PreferenceGroup;Ljava/lang/String;I)Z

    :goto_0
    return-void

    :cond_1
    invoke-virtual {p2, p3}, Lcom/android/camera/ListPreference;->filterUnsupportedEntries(Ljava/util/List;)V

    invoke-virtual {p2}, Lcom/android/camera/ListPreference;->getEntries()[Ljava/lang/CharSequence;

    move-result-object v0

    array-length v0, v0

    if-gtz v0, :cond_2

    invoke-virtual {p2}, Lcom/android/camera/ListPreference;->getKey()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, p1, v0, p5}, Lcom/android/camera/CameraSettings;->removePreference(Lcom/android/camera/PreferenceGroup;Ljava/lang/String;I)Z

    goto :goto_0

    :cond_2
    invoke-direct {p0, p2, p4}, Lcom/android/camera/CameraSettings;->resetIfInvalid(Lcom/android/camera/ListPreference;Z)V

    invoke-direct {p0, p5, p2}, Lcom/android/camera/CameraSettings;->setListPreference(ILcom/android/camera/ListPreference;)V

    goto :goto_0
.end method

.method private filterUnsupportedOptions(Lcom/android/camera/PreferenceGroup;Lcom/android/camera/ListPreference;Ljava/util/List;I)V
    .locals 6
    .param p1    # Lcom/android/camera/PreferenceGroup;
    .param p2    # Lcom/android/camera/ListPreference;
    .param p4    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/android/camera/PreferenceGroup;",
            "Lcom/android/camera/ListPreference;",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;I)V"
        }
    .end annotation

    const/4 v4, 0x1

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move v5, p4

    invoke-direct/range {v0 .. v5}, Lcom/android/camera/CameraSettings;->filterUnsupportedOptions(Lcom/android/camera/PreferenceGroup;Lcom/android/camera/ListPreference;Ljava/util/List;ZI)V

    return-void
.end method

.method private filterUnsupportedOptions(Lcom/android/camera/PreferenceGroup;Lcom/android/camera/ListPreference;Ljava/util/List;ZI)V
    .locals 2
    .param p1    # Lcom/android/camera/PreferenceGroup;
    .param p2    # Lcom/android/camera/ListPreference;
    .param p4    # Z
    .param p5    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/android/camera/PreferenceGroup;",
            "Lcom/android/camera/ListPreference;",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;ZI)V"
        }
    .end annotation

    const/4 v1, 0x1

    if-eqz p3, :cond_0

    invoke-interface {p3}, Ljava/util/List;->size()I

    move-result v0

    if-gt v0, v1, :cond_1

    :cond_0
    invoke-virtual {p2}, Lcom/android/camera/ListPreference;->getKey()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, p1, v0, p5}, Lcom/android/camera/CameraSettings;->removePreference(Lcom/android/camera/PreferenceGroup;Ljava/lang/String;I)Z

    :goto_0
    return-void

    :cond_1
    invoke-virtual {p2, p3}, Lcom/android/camera/ListPreference;->filterUnsupported(Ljava/util/List;)V

    invoke-virtual {p2}, Lcom/android/camera/ListPreference;->getEntries()[Ljava/lang/CharSequence;

    move-result-object v0

    array-length v0, v0

    if-gt v0, v1, :cond_2

    invoke-virtual {p2}, Lcom/android/camera/ListPreference;->getKey()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, p1, v0, p5}, Lcom/android/camera/CameraSettings;->removePreference(Lcom/android/camera/PreferenceGroup;Ljava/lang/String;I)Z

    goto :goto_0

    :cond_2
    invoke-direct {p0, p2, p4}, Lcom/android/camera/CameraSettings;->resetIfInvalid(Lcom/android/camera/ListPreference;Z)V

    invoke-direct {p0, p5, p2}, Lcom/android/camera/CameraSettings;->setListPreference(ILcom/android/camera/ListPreference;)V

    goto :goto_0
.end method

.method private filterUnsupportedOptionsForPictureSize(Lcom/android/camera/PreferenceGroup;Lcom/android/camera/ListPreference;Ljava/util/List;ZI)V
    .locals 2
    .param p1    # Lcom/android/camera/PreferenceGroup;
    .param p2    # Lcom/android/camera/ListPreference;
    .param p4    # Z
    .param p5    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/android/camera/PreferenceGroup;",
            "Lcom/android/camera/ListPreference;",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;ZI)V"
        }
    .end annotation

    const/4 v1, 0x1

    if-eqz p3, :cond_0

    invoke-interface {p3}, Ljava/util/List;->size()I

    move-result v0

    if-ge v0, v1, :cond_1

    :cond_0
    invoke-virtual {p2}, Lcom/android/camera/ListPreference;->getKey()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, p1, v0, p5}, Lcom/android/camera/CameraSettings;->removePreference(Lcom/android/camera/PreferenceGroup;Ljava/lang/String;I)Z

    :goto_0
    return-void

    :cond_1
    invoke-virtual {p2, p3}, Lcom/android/camera/ListPreference;->filterUnsupported(Ljava/util/List;)V

    invoke-virtual {p2}, Lcom/android/camera/ListPreference;->getEntries()[Ljava/lang/CharSequence;

    move-result-object v0

    array-length v0, v0

    if-ge v0, v1, :cond_2

    invoke-virtual {p2}, Lcom/android/camera/ListPreference;->getKey()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, p1, v0, p5}, Lcom/android/camera/CameraSettings;->removePreference(Lcom/android/camera/PreferenceGroup;Ljava/lang/String;I)Z

    goto :goto_0

    :cond_2
    invoke-direct {p0, p2, p4}, Lcom/android/camera/CameraSettings;->resetIfInvalid(Lcom/android/camera/ListPreference;Z)V

    invoke-direct {p0, p5, p2}, Lcom/android/camera/CameraSettings;->setListPreference(ILcom/android/camera/ListPreference;)V

    goto :goto_0
.end method

.method public static findFullscreenRatio(Landroid/content/Context;Landroid/hardware/Camera$Parameters;)D
    .locals 16
    .param p0    # Landroid/content/Context;
    .param p1    # Landroid/hardware/Camera$Parameters;

    const-wide v1, 0x3ff5555555555555L

    if-eqz p0, :cond_2

    if-eqz p1, :cond_2

    invoke-virtual/range {p0 .. p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v12

    invoke-virtual {v12}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v9

    iget v12, v9, Landroid/util/DisplayMetrics;->widthPixels:I

    iget v13, v9, Landroid/util/DisplayMetrics;->heightPixels:I

    if-le v12, v13, :cond_1

    iget v12, v9, Landroid/util/DisplayMetrics;->widthPixels:I

    int-to-double v12, v12

    iget v14, v9, Landroid/util/DisplayMetrics;->heightPixels:I

    int-to-double v14, v14

    div-double v5, v12, v14

    :goto_0
    const/4 v7, 0x0

    :goto_1
    sget-object v12, Lcom/android/camera/CameraSettings;->RATIOS:[D

    array-length v12, v12

    if-ge v7, v12, :cond_2

    sget-object v12, Lcom/android/camera/CameraSettings;->RATIOS:[D

    aget-wide v12, v12, v7

    sub-double/2addr v12, v5

    invoke-static {v12, v13}, Ljava/lang/Math;->abs(D)D

    move-result-wide v12

    sub-double v14, v5, v1

    invoke-static {v14, v15}, Ljava/lang/Math;->abs(D)D

    move-result-wide v14

    cmpg-double v12, v12, v14

    if-gez v12, :cond_0

    sget-object v12, Lcom/android/camera/CameraSettings;->RATIOS:[D

    aget-wide v1, v12, v7

    :cond_0
    add-int/lit8 v7, v7, 0x1

    goto :goto_1

    :cond_1
    iget v12, v9, Landroid/util/DisplayMetrics;->heightPixels:I

    int-to-double v12, v12

    iget v14, v9, Landroid/util/DisplayMetrics;->widthPixels:I

    int-to-double v14, v14

    div-double v5, v12, v14

    goto :goto_0

    :cond_2
    invoke-virtual/range {p1 .. p1}, Landroid/hardware/Camera$Parameters;->getSupportedPictureSizes()Ljava/util/List;

    move-result-object v11

    if-eqz v11, :cond_5

    invoke-interface {v11}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v8

    :cond_3
    invoke-interface {v8}, Ljava/util/Iterator;->hasNext()Z

    move-result v12

    if-eqz v12, :cond_4

    invoke-interface {v8}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v10

    check-cast v10, Landroid/hardware/Camera$Size;

    iget v12, v10, Landroid/hardware/Camera$Size;->width:I

    int-to-double v12, v12

    iget v14, v10, Landroid/hardware/Camera$Size;->height:I

    int-to-double v14, v14

    div-double/2addr v12, v14

    invoke-static {v1, v2, v12, v13}, Lcom/android/camera/CameraSettings;->toleranceRatio(DD)Z

    move-result v12

    if-eqz v12, :cond_3

    const-string v12, "CameraSettings"

    new-instance v13, Ljava/lang/StringBuilder;

    invoke-direct {v13}, Ljava/lang/StringBuilder;-><init>()V

    const-string v14, "findFullscreenRatio("

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    move-object/from16 v0, p1

    invoke-virtual {v13, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v13

    const-string v14, ") return "

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v13, v1, v2}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v13}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v13

    invoke-static {v12, v13}, Lcom/android/camera/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    move-wide v3, v1

    :goto_2
    return-wide v3

    :cond_4
    const-wide v1, 0x3ff5555555555555L

    :cond_5
    sget-boolean v12, Lcom/android/camera/CameraSettings;->LOG:Z

    if-eqz v12, :cond_6

    const-string v12, "CameraSettings"

    new-instance v13, Ljava/lang/StringBuilder;

    invoke-direct {v13}, Ljava/lang/StringBuilder;-><init>()V

    const-string v14, "findFullscreenRatio("

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    move-object/from16 v0, p1

    invoke-virtual {v13, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v13

    const-string v14, ") return "

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v13, v1, v2}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v13}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v13

    invoke-static {v12, v13}, Lcom/android/camera/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    :cond_6
    move-wide v3, v1

    goto :goto_2
.end method

.method public static getDefaultVideoQuality(ILjava/lang/String;)Ljava/lang/String;
    .locals 2
    .param p0    # I
    .param p1    # Ljava/lang/String;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-static {p0, v0}, Landroid/media/CamcorderProfile;->hasProfile(II)Z

    move-result v1

    if-eqz v1, :cond_0

    :goto_0
    return-object p1

    :cond_0
    const/16 v1, 0xa

    invoke-static {v1}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object p1

    goto :goto_0
.end method

.method private getMTKSupportedVideoQuality()Ljava/util/ArrayList;
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    const/16 v6, 0x12

    const/16 v5, 0xb

    const/16 v4, 0xa

    const/16 v3, 0x9

    const/16 v2, 0x8

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iget v1, p0, Lcom/android/camera/CameraSettings;->mCameraId:I

    invoke-static {v1, v2}, Landroid/media/CamcorderProfile;->hasProfile(II)Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-static {v2}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :cond_0
    iget v1, p0, Lcom/android/camera/CameraSettings;->mCameraId:I

    invoke-static {v1, v3}, Landroid/media/CamcorderProfile;->hasProfile(II)Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-static {v3}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :cond_1
    iget v1, p0, Lcom/android/camera/CameraSettings;->mCameraId:I

    invoke-static {v1, v4}, Landroid/media/CamcorderProfile;->hasProfile(II)Z

    move-result v1

    if-eqz v1, :cond_2

    invoke-static {v4}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :cond_2
    iget v1, p0, Lcom/android/camera/CameraSettings;->mCameraId:I

    invoke-static {v1, v5}, Landroid/media/CamcorderProfile;->hasProfile(II)Z

    move-result v1

    if-eqz v1, :cond_3

    invoke-static {v5}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :cond_3
    iget v1, p0, Lcom/android/camera/CameraSettings;->mCameraId:I

    invoke-static {v1, v6}, Landroid/media/CamcorderProfile;->hasProfile(II)Z

    move-result v1

    if-eqz v1, :cond_4

    invoke-static {v6}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :cond_4
    return-object v0
.end method

.method private getSupportedVideoQuality()Ljava/util/ArrayList;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    const/4 v4, 0x6

    const/4 v3, 0x5

    const/4 v2, 0x4

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iget v1, p0, Lcom/android/camera/CameraSettings;->mCameraId:I

    invoke-static {v1, v4}, Landroid/media/CamcorderProfile;->hasProfile(II)Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-static {v4}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :cond_0
    iget v1, p0, Lcom/android/camera/CameraSettings;->mCameraId:I

    invoke-static {v1, v3}, Landroid/media/CamcorderProfile;->hasProfile(II)Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-static {v3}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :cond_1
    iget v1, p0, Lcom/android/camera/CameraSettings;->mCameraId:I

    invoke-static {v1, v2}, Landroid/media/CamcorderProfile;->hasProfile(II)Z

    move-result v1

    if-eqz v1, :cond_2

    invoke-static {v2}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :cond_2
    return-object v0
.end method

.method private initPreference(Lcom/android/camera/PreferenceGroup;)V
    .locals 53
    .param p1    # Lcom/android/camera/PreferenceGroup;

    const-string v4, "pref_video_quality_key"

    move-object/from16 v0, p1

    invoke-virtual {v0, v4}, Lcom/android/camera/PreferenceGroup;->findPreference(Ljava/lang/String;)Lcom/android/camera/ListPreference;

    move-result-object v49

    const-string v4, "pref_video_time_lapse_frame_interval_key"

    move-object/from16 v0, p1

    invoke-virtual {v0, v4}, Lcom/android/camera/PreferenceGroup;->findPreference(Ljava/lang/String;)Lcom/android/camera/ListPreference;

    move-result-object v46

    const-string v4, "pref_camera_picturesize_key"

    move-object/from16 v0, p1

    invoke-virtual {v0, v4}, Lcom/android/camera/PreferenceGroup;->findPreference(Ljava/lang/String;)Lcom/android/camera/ListPreference;

    move-result-object v10

    const-string v4, "pref_camera_whitebalance_key"

    move-object/from16 v0, p1

    invoke-virtual {v0, v4}, Lcom/android/camera/PreferenceGroup;->findPreference(Ljava/lang/String;)Lcom/android/camera/ListPreference;

    move-result-object v51

    const-string v4, "pref_camera_scenemode_key"

    move-object/from16 v0, p1

    invoke-virtual {v0, v4}, Lcom/android/camera/PreferenceGroup;->findPreference(Ljava/lang/String;)Lcom/android/camera/ListPreference;

    move-result-object v37

    const-string v4, "pref_camera_flashmode_key"

    move-object/from16 v0, p1

    invoke-virtual {v0, v4}, Lcom/android/camera/PreferenceGroup;->findPreference(Ljava/lang/String;)Lcom/android/camera/ListPreference;

    move-result-object v27

    const-string v4, "pref_camera_exposure_key"

    move-object/from16 v0, p1

    invoke-virtual {v0, v4}, Lcom/android/camera/PreferenceGroup;->findPreference(Ljava/lang/String;)Lcom/android/camera/ListPreference;

    move-result-object v24

    const-string v4, "pref_camera_id_key"

    move-object/from16 v0, p1

    invoke-virtual {v0, v4}, Lcom/android/camera/PreferenceGroup;->findPreference(Ljava/lang/String;)Lcom/android/camera/ListPreference;

    move-result-object v16

    check-cast v16, Lcom/android/camera/IconListPreference;

    const-string v4, "pref_video_effect_key"

    move-object/from16 v0, p1

    invoke-virtual {v0, v4}, Lcom/android/camera/PreferenceGroup;->findPreference(Ljava/lang/String;)Lcom/android/camera/ListPreference;

    move-result-object v48

    const-string v4, "pref_camera_iso_key"

    move-object/from16 v0, p1

    invoke-virtual {v0, v4}, Lcom/android/camera/PreferenceGroup;->findPreference(Ljava/lang/String;)Lcom/android/camera/ListPreference;

    move-result-object v30

    const-string v4, "pref_camera_antibanding_key"

    move-object/from16 v0, p1

    invoke-virtual {v0, v4}, Lcom/android/camera/PreferenceGroup;->findPreference(Ljava/lang/String;)Lcom/android/camera/ListPreference;

    move-result-object v14

    const-string v4, "pref_camera_coloreffect_key"

    move-object/from16 v0, p1

    invoke-virtual {v0, v4}, Lcom/android/camera/PreferenceGroup;->findPreference(Ljava/lang/String;)Lcom/android/camera/ListPreference;

    move-result-object v17

    const-string v4, "pref_camera_edge_key"

    move-object/from16 v0, p1

    invoke-virtual {v0, v4}, Lcom/android/camera/PreferenceGroup;->findPreference(Ljava/lang/String;)Lcom/android/camera/ListPreference;

    move-result-object v20

    const-string v4, "pref_camera_hue_key"

    move-object/from16 v0, p1

    invoke-virtual {v0, v4}, Lcom/android/camera/PreferenceGroup;->findPreference(Ljava/lang/String;)Lcom/android/camera/ListPreference;

    move-result-object v28

    const-string v4, "pref_camera_saturation_key"

    move-object/from16 v0, p1

    invoke-virtual {v0, v4}, Lcom/android/camera/PreferenceGroup;->findPreference(Ljava/lang/String;)Lcom/android/camera/ListPreference;

    move-result-object v36

    const-string v4, "pref_camera_brightness_key"

    move-object/from16 v0, p1

    invoke-virtual {v0, v4}, Lcom/android/camera/PreferenceGroup;->findPreference(Ljava/lang/String;)Lcom/android/camera/ListPreference;

    move-result-object v15

    const-string v4, "pref_camera_contrast_key"

    move-object/from16 v0, p1

    invoke-virtual {v0, v4}, Lcom/android/camera/PreferenceGroup;->findPreference(Ljava/lang/String;)Lcom/android/camera/ListPreference;

    move-result-object v19

    const-string v4, "pref_video_eis_key"

    move-object/from16 v0, p1

    invoke-virtual {v0, v4}, Lcom/android/camera/PreferenceGroup;->findPreference(Ljava/lang/String;)Lcom/android/camera/ListPreference;

    move-result-object v21

    const-string v4, "pref_camera_picturesize_stereo3d_key"

    move-object/from16 v0, p1

    invoke-virtual {v0, v4}, Lcom/android/camera/PreferenceGroup;->findPreference(Ljava/lang/String;)Lcom/android/camera/ListPreference;

    move-result-object v43

    const-string v4, "pref_camera_shot_number"

    move-object/from16 v0, p1

    invoke-virtual {v0, v4}, Lcom/android/camera/PreferenceGroup;->findPreference(Ljava/lang/String;)Lcom/android/camera/ListPreference;

    move-result-object v18

    const-string v4, "pref_camera_image_properties_key"

    move-object/from16 v0, p1

    invoke-virtual {v0, v4}, Lcom/android/camera/PreferenceGroup;->findPreference(Ljava/lang/String;)Lcom/android/camera/ListPreference;

    move-result-object v6

    const-string v4, "pref_camera_picturesize_ratio_key"

    move-object/from16 v0, p1

    invoke-virtual {v0, v4}, Lcom/android/camera/PreferenceGroup;->findPreference(Ljava/lang/String;)Lcom/android/camera/ListPreference;

    move-result-object v35

    const-string v4, "pref_camera_facebeauty_properties_key"

    move-object/from16 v0, p1

    invoke-virtual {v0, v4}, Lcom/android/camera/PreferenceGroup;->findPreference(Ljava/lang/String;)Lcom/android/camera/ListPreference;

    move-result-object v26

    const-string v4, "pref_facebeauty_smooth_key"

    move-object/from16 v0, p1

    invoke-virtual {v0, v4}, Lcom/android/camera/PreferenceGroup;->findPreference(Ljava/lang/String;)Lcom/android/camera/ListPreference;

    move-result-object v42

    const-string v4, "pref_facebeauty_skin_color_key"

    move-object/from16 v0, p1

    invoke-virtual {v0, v4}, Lcom/android/camera/PreferenceGroup;->findPreference(Ljava/lang/String;)Lcom/android/camera/ListPreference;

    move-result-object v41

    const-string v4, "pref_facebeauty_sharp_key"

    move-object/from16 v0, p1

    invoke-virtual {v0, v4}, Lcom/android/camera/PreferenceGroup;->findPreference(Ljava/lang/String;)Lcom/android/camera/ListPreference;

    move-result-object v40

    const-string v4, "pref_voice_key"

    move-object/from16 v0, p1

    invoke-virtual {v0, v4}, Lcom/android/camera/PreferenceGroup;->findPreference(Ljava/lang/String;)Lcom/android/camera/ListPreference;

    move-result-object v50

    const-string v4, "pref_camera_zsd_key"

    move-object/from16 v0, p1

    invoke-virtual {v0, v4}, Lcom/android/camera/PreferenceGroup;->findPreference(Ljava/lang/String;)Lcom/android/camera/ListPreference;

    move-result-object v52

    const-string v4, "pref_camera_self_timer_key"

    move-object/from16 v0, p1

    invoke-virtual {v0, v4}, Lcom/android/camera/PreferenceGroup;->findPreference(Ljava/lang/String;)Lcom/android/camera/ListPreference;

    move-result-object v38

    const-string v4, "pref_camera_recordaudio_key"

    move-object/from16 v0, p1

    invoke-virtual {v0, v4}, Lcom/android/camera/PreferenceGroup;->findPreference(Ljava/lang/String;)Lcom/android/camera/ListPreference;

    move-result-object v32

    const-string v4, "pref_camera_recordlocation_key"

    move-object/from16 v0, p1

    invoke-virtual {v0, v4}, Lcom/android/camera/PreferenceGroup;->findPreference(Ljava/lang/String;)Lcom/android/camera/ListPreference;

    move-result-object v44

    const-string v4, "pref_face_detect_key"

    move-object/from16 v0, p1

    invoke-virtual {v0, v4}, Lcom/android/camera/PreferenceGroup;->findPreference(Ljava/lang/String;)Lcom/android/camera/ListPreference;

    move-result-object v25

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/camera/CameraSettings;->mContext:Lcom/android/camera/Camera;

    invoke-virtual {v4}, Lcom/android/camera/Camera;->getSettingChecker()Lcom/android/camera/SettingChecker;

    move-result-object v39

    monitor-enter v39

    :try_start_0
    invoke-virtual/range {v39 .. v39}, Lcom/android/camera/SettingChecker;->clearListPreference()V

    const/4 v4, 0x7

    move-object/from16 v0, v39

    move-object/from16 v1, v38

    invoke-virtual {v0, v4, v1}, Lcom/android/camera/SettingChecker;->setListPreference(ILcom/android/camera/ListPreference;)V

    sget-boolean v4, Lcom/android/camera/CameraSettings;->SUPPORTED_SHOW_CONINUOUS_SHOT_NUMBER:Z

    if-eqz v4, :cond_0

    const/16 v4, 0x9

    move-object/from16 v0, v39

    move-object/from16 v1, v18

    invoke-virtual {v0, v4, v1}, Lcom/android/camera/SettingChecker;->setListPreference(ILcom/android/camera/ListPreference;)V

    :cond_0
    const/16 v4, 0xa

    move-object/from16 v0, v39

    move-object/from16 v1, v44

    invoke-virtual {v0, v4, v1}, Lcom/android/camera/SettingChecker;->setListPreference(ILcom/android/camera/ListPreference;)V

    const/16 v4, 0x10

    move-object/from16 v0, v39

    move-object/from16 v1, v32

    invoke-virtual {v0, v4, v1}, Lcom/android/camera/SettingChecker;->setListPreference(ILcom/android/camera/ListPreference;)V

    const/16 v4, 0x12

    move-object/from16 v0, v39

    move-object/from16 v1, v46

    invoke-virtual {v0, v4, v1}, Lcom/android/camera/SettingChecker;->setListPreference(ILcom/android/camera/ListPreference;)V

    const/16 v4, 0x32

    move-object/from16 v0, v39

    move-object/from16 v1, v25

    invoke-virtual {v0, v4, v1}, Lcom/android/camera/SettingChecker;->setListPreference(ILcom/android/camera/ListPreference;)V

    if-eqz v26, :cond_13

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/camera/CameraSettings;->mParameters:Landroid/hardware/Camera$Parameters;

    invoke-static {v4}, Lcom/android/camera/ParametersHelper;->isFaceBeautySupported(Landroid/hardware/Camera$Parameters;)Z

    move-result v4

    if-nez v4, :cond_13

    invoke-virtual/range {v26 .. v26}, Lcom/android/camera/ListPreference;->getKey()Ljava/lang/String;

    move-result-object v4

    const/16 v5, 0x2e

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    invoke-direct {v0, v1, v4, v5}, Lcom/android/camera/CameraSettings;->removePreference(Lcom/android/camera/PreferenceGroup;Ljava/lang/String;I)Z

    :goto_0
    if-eqz v30, :cond_1

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/camera/CameraSettings;->mParameters:Landroid/hardware/Camera$Parameters;

    invoke-virtual {v4}, Landroid/hardware/Camera$Parameters;->getSupportedISOSpeed()Ljava/util/List;

    move-result-object v4

    const/16 v5, 0xc

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    move-object/from16 v2, v30

    invoke-direct {v0, v1, v2, v4, v5}, Lcom/android/camera/CameraSettings;->filterUnsupportedOptions(Lcom/android/camera/PreferenceGroup;Lcom/android/camera/ListPreference;Ljava/util/List;I)V

    :cond_1
    if-eqz v42, :cond_2

    const/4 v4, 0x0

    const/16 v5, 0x2f

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    move-object/from16 v2, v42

    invoke-direct {v0, v1, v4, v2, v5}, Lcom/android/camera/CameraSettings;->buildFaceBeautyPreference(Lcom/android/camera/PreferenceGroup;ILcom/android/camera/ListPreference;I)V

    :cond_2
    if-eqz v41, :cond_3

    const/4 v4, 0x1

    const/16 v5, 0x30

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    move-object/from16 v2, v41

    invoke-direct {v0, v1, v4, v2, v5}, Lcom/android/camera/CameraSettings;->buildFaceBeautyPreference(Lcom/android/camera/PreferenceGroup;ILcom/android/camera/ListPreference;I)V

    :cond_3
    if-eqz v40, :cond_4

    const/4 v4, 0x2

    const/16 v5, 0x31

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    move-object/from16 v2, v40

    invoke-direct {v0, v1, v4, v2, v5}, Lcom/android/camera/CameraSettings;->buildFaceBeautyPreference(Lcom/android/camera/PreferenceGroup;ILcom/android/camera/ListPreference;I)V

    :cond_4
    if-eqz v17, :cond_5

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/camera/CameraSettings;->mParameters:Landroid/hardware/Camera$Parameters;

    invoke-virtual {v4}, Landroid/hardware/Camera$Parameters;->getSupportedColorEffects()Ljava/util/List;

    move-result-object v4

    const/4 v5, 0x6

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    move-object/from16 v2, v17

    invoke-direct {v0, v1, v2, v4, v5}, Lcom/android/camera/CameraSettings;->filterUnsupportedOptions(Lcom/android/camera/PreferenceGroup;Lcom/android/camera/ListPreference;Ljava/util/List;I)V

    :cond_5
    if-eqz v14, :cond_6

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/camera/CameraSettings;->mParameters:Landroid/hardware/Camera$Parameters;

    invoke-virtual {v4}, Landroid/hardware/Camera$Parameters;->getSupportedAntibanding()Ljava/util/List;

    move-result-object v4

    const/16 v5, 0xe

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    invoke-direct {v0, v1, v14, v4, v5}, Lcom/android/camera/CameraSettings;->filterUnsupportedOptions(Lcom/android/camera/PreferenceGroup;Lcom/android/camera/ListPreference;Ljava/util/List;I)V

    :cond_6
    new-instance v7, Ljava/util/ArrayList;

    invoke-direct {v7}, Ljava/util/ArrayList;-><init>()V

    if-eqz v20, :cond_7

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/camera/CameraSettings;->mParameters:Landroid/hardware/Camera$Parameters;

    invoke-virtual {v4}, Landroid/hardware/Camera$Parameters;->getSupportedEdgeMode()Ljava/util/List;

    move-result-object v4

    const/16 v5, 0x1e

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    move-object/from16 v2, v20

    invoke-direct {v0, v1, v2, v4, v5}, Lcom/android/camera/CameraSettings;->filterUnsupportedOptions(Lcom/android/camera/PreferenceGroup;Lcom/android/camera/ListPreference;Ljava/util/List;I)V

    const/16 v4, 0x1e

    move-object/from16 v0, p0

    invoke-direct {v0, v7, v4}, Lcom/android/camera/CameraSettings;->buildSupportedListperference(Ljava/util/List;I)V

    :cond_7
    if-eqz v28, :cond_8

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/camera/CameraSettings;->mParameters:Landroid/hardware/Camera$Parameters;

    invoke-virtual {v4}, Landroid/hardware/Camera$Parameters;->getSupportedHueMode()Ljava/util/List;

    move-result-object v4

    const/16 v5, 0x1f

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    move-object/from16 v2, v28

    invoke-direct {v0, v1, v2, v4, v5}, Lcom/android/camera/CameraSettings;->filterUnsupportedOptions(Lcom/android/camera/PreferenceGroup;Lcom/android/camera/ListPreference;Ljava/util/List;I)V

    const/16 v4, 0x1f

    move-object/from16 v0, p0

    invoke-direct {v0, v7, v4}, Lcom/android/camera/CameraSettings;->buildSupportedListperference(Ljava/util/List;I)V

    :cond_8
    if-eqz v36, :cond_9

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/camera/CameraSettings;->mParameters:Landroid/hardware/Camera$Parameters;

    invoke-virtual {v4}, Landroid/hardware/Camera$Parameters;->getSupportedSaturationMode()Ljava/util/List;

    move-result-object v4

    const/16 v5, 0x20

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    move-object/from16 v2, v36

    invoke-direct {v0, v1, v2, v4, v5}, Lcom/android/camera/CameraSettings;->filterUnsupportedOptions(Lcom/android/camera/PreferenceGroup;Lcom/android/camera/ListPreference;Ljava/util/List;I)V

    const/16 v4, 0x20

    move-object/from16 v0, p0

    invoke-direct {v0, v7, v4}, Lcom/android/camera/CameraSettings;->buildSupportedListperference(Ljava/util/List;I)V

    :cond_9
    if-eqz v15, :cond_a

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/camera/CameraSettings;->mParameters:Landroid/hardware/Camera$Parameters;

    invoke-virtual {v4}, Landroid/hardware/Camera$Parameters;->getSupportedBrightnessMode()Ljava/util/List;

    move-result-object v4

    const/16 v5, 0x21

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    invoke-direct {v0, v1, v15, v4, v5}, Lcom/android/camera/CameraSettings;->filterUnsupportedOptions(Lcom/android/camera/PreferenceGroup;Lcom/android/camera/ListPreference;Ljava/util/List;I)V

    const/16 v4, 0x21

    move-object/from16 v0, p0

    invoke-direct {v0, v7, v4}, Lcom/android/camera/CameraSettings;->buildSupportedListperference(Ljava/util/List;I)V

    :cond_a
    if-eqz v19, :cond_b

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/camera/CameraSettings;->mParameters:Landroid/hardware/Camera$Parameters;

    invoke-virtual {v4}, Landroid/hardware/Camera$Parameters;->getSupportedContrastMode()Ljava/util/List;

    move-result-object v4

    const/16 v5, 0x22

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    move-object/from16 v2, v19

    invoke-direct {v0, v1, v2, v4, v5}, Lcom/android/camera/CameraSettings;->filterUnsupportedOptions(Lcom/android/camera/PreferenceGroup;Lcom/android/camera/ListPreference;Ljava/util/List;I)V

    const/16 v4, 0x22

    move-object/from16 v0, p0

    invoke-direct {v0, v7, v4}, Lcom/android/camera/CameraSettings;->buildSupportedListperference(Ljava/util/List;I)V

    :cond_b
    if-eqz v6, :cond_c

    const/4 v8, 0x1

    const/4 v9, 0x5

    move-object/from16 v4, p0

    move-object/from16 v5, p1

    invoke-direct/range {v4 .. v9}, Lcom/android/camera/CameraSettings;->filterUnsupportedEntries(Lcom/android/camera/PreferenceGroup;Lcom/android/camera/ListPreference;Ljava/util/List;ZI)V

    :cond_c
    if-eqz v21, :cond_14

    const-string v4, "true"

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/android/camera/CameraSettings;->mParameters:Landroid/hardware/Camera$Parameters;

    const-string v8, "video-stabilization-supported"

    invoke-virtual {v5, v8}, Landroid/hardware/Camera$Parameters;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_14

    const/4 v4, 0x0

    const/16 v5, 0xf

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    move-object/from16 v2, v21

    invoke-direct {v0, v1, v2, v4, v5}, Lcom/android/camera/CameraSettings;->filterUnsupportedOptions(Lcom/android/camera/PreferenceGroup;Lcom/android/camera/ListPreference;Ljava/util/List;I)V

    :cond_d
    :goto_1
    invoke-static {}, Lcom/android/camera/FeatureSwitcher;->isHdRecordingEnabled()Z

    move-result v4

    if-nez v4, :cond_15

    const-string v4, "pref_camera_video_hd_recording_key"

    const/16 v5, 0x11

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    invoke-direct {v0, v1, v4, v5}, Lcom/android/camera/CameraSettings;->removePreference(Lcom/android/camera/PreferenceGroup;Ljava/lang/String;I)Z

    :goto_2
    invoke-static {}, Lcom/android/camera/FeatureSwitcher;->isLcaRAM()Z

    move-result v4

    if-eqz v4, :cond_16

    const-string v4, "pref_camera_zsd_key"

    const/16 v5, 0x8

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    invoke-direct {v0, v1, v4, v5}, Lcom/android/camera/CameraSettings;->removePreference(Lcom/android/camera/PreferenceGroup;Ljava/lang/String;I)Z

    :cond_e
    :goto_3
    if-eqz v49, :cond_f

    invoke-direct/range {p0 .. p0}, Lcom/android/camera/CameraSettings;->getMTKSupportedVideoQuality()Ljava/util/ArrayList;

    move-result-object v4

    const/16 v5, 0x14

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    move-object/from16 v2, v49

    invoke-direct {v0, v1, v2, v4, v5}, Lcom/android/camera/CameraSettings;->filterUnsupportedOptions(Lcom/android/camera/PreferenceGroup;Lcom/android/camera/ListPreference;Ljava/util/List;I)V

    :cond_f
    if-eqz v35, :cond_10

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/camera/CameraSettings;->mContext:Lcom/android/camera/Camera;

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/android/camera/CameraSettings;->mParameters:Landroid/hardware/Camera$Parameters;

    invoke-static {v4, v5}, Lcom/android/camera/CameraSettings;->buildPreviewRatios(Landroid/content/Context;Landroid/hardware/Camera$Parameters;)Ljava/util/List;

    move-result-object v45

    const/16 v4, 0x15

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    move-object/from16 v2, v35

    move-object/from16 v3, v45

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/android/camera/CameraSettings;->filterUnsupportedOptions(Lcom/android/camera/PreferenceGroup;Lcom/android/camera/ListPreference;Ljava/util/List;I)V

    :cond_10
    if-eqz v10, :cond_11

    if-eqz v35, :cond_11

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/camera/CameraSettings;->mParameters:Landroid/hardware/Camera$Parameters;

    invoke-virtual {v4}, Landroid/hardware/Camera$Parameters;->getSupportedPictureSizes()Ljava/util/List;

    move-result-object v4

    invoke-static {v4}, Lcom/android/camera/CameraSettings;->sizeListToStringList(Ljava/util/List;)Ljava/util/List;

    move-result-object v11

    const/4 v12, 0x0

    const/16 v13, 0xb

    move-object/from16 v8, p0

    move-object/from16 v9, p1

    invoke-direct/range {v8 .. v13}, Lcom/android/camera/CameraSettings;->filterUnsupportedOptionsForPictureSize(Lcom/android/camera/PreferenceGroup;Lcom/android/camera/ListPreference;Ljava/util/List;ZI)V

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/camera/CameraSettings;->mContext:Lcom/android/camera/Camera;

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/android/camera/CameraSettings;->mParameters:Landroid/hardware/Camera$Parameters;

    invoke-virtual/range {v35 .. v35}, Lcom/android/camera/ListPreference;->getValue()Ljava/lang/String;

    move-result-object v8

    invoke-static {v4, v5, v8}, Lcom/android/camera/CameraSettings;->buildSupportedPictureSize(Landroid/content/Context;Landroid/hardware/Camera$Parameters;Ljava/lang/String;)Ljava/util/List;

    move-result-object v11

    const/4 v12, 0x0

    const/16 v13, 0xb

    move-object/from16 v8, p0

    move-object/from16 v9, p1

    invoke-direct/range {v8 .. v13}, Lcom/android/camera/CameraSettings;->filterDisabledOptions(Lcom/android/camera/PreferenceGroup;Lcom/android/camera/ListPreference;Ljava/util/List;ZI)V

    :cond_11
    invoke-static {}, Lcom/mediatek/camera/ext/ExtensionHelper;->getFeatureExtension()Lcom/mediatek/camera/ext/IFeatureExtension;

    move-result-object v47

    if-eqz v51, :cond_12

    invoke-virtual/range {v51 .. v51}, Lcom/android/camera/ListPreference;->getEntries()[Ljava/lang/CharSequence;

    move-result-object v4

    move-object/from16 v0, v47

    invoke-interface {v0, v4}, Lcom/mediatek/camera/ext/IFeatureExtension;->updateWBStrings([Ljava/lang/CharSequence;)V

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/camera/CameraSettings;->mParameters:Landroid/hardware/Camera$Parameters;

    invoke-virtual {v4}, Landroid/hardware/Camera$Parameters;->getSupportedWhiteBalance()Ljava/util/List;

    move-result-object v4

    const/4 v5, 0x4

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    move-object/from16 v2, v51

    invoke-direct {v0, v1, v2, v4, v5}, Lcom/android/camera/CameraSettings;->filterUnsupportedOptions(Lcom/android/camera/PreferenceGroup;Lcom/android/camera/ListPreference;Ljava/util/List;I)V

    :cond_12
    if-eqz v37, :cond_18

    invoke-virtual/range {v37 .. v37}, Lcom/android/camera/ListPreference;->getEntries()[Ljava/lang/CharSequence;

    move-result-object v22

    invoke-virtual/range {v37 .. v37}, Lcom/android/camera/ListPreference;->getEntryValues()[Ljava/lang/CharSequence;

    move-result-object v23

    move-object/from16 v0, v22

    array-length v0, v0

    move/from16 v31, v0

    new-instance v33, Ljava/util/ArrayList;

    invoke-direct/range {v33 .. v33}, Ljava/util/ArrayList;-><init>()V

    new-instance v34, Ljava/util/ArrayList;

    invoke-direct/range {v34 .. v34}, Ljava/util/ArrayList;-><init>()V

    const/16 v29, 0x0

    :goto_4
    move/from16 v0, v29

    move/from16 v1, v31

    if-ge v0, v1, :cond_17

    aget-object v4, v22, v29

    move-object/from16 v0, v33

    invoke-virtual {v0, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    aget-object v4, v23, v29

    move-object/from16 v0, v34

    invoke-virtual {v0, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    add-int/lit8 v29, v29, 0x1

    goto :goto_4

    :cond_13
    const/16 v4, 0x2e

    move-object/from16 v0, p0

    move-object/from16 v1, v26

    invoke-direct {v0, v4, v1}, Lcom/android/camera/CameraSettings;->setListPreference(ILcom/android/camera/ListPreference;)V

    goto/16 :goto_0

    :catchall_0
    move-exception v4

    monitor-exit v39
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v4

    :cond_14
    if-eqz v21, :cond_d

    :try_start_1
    const-string v4, "true"

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/android/camera/CameraSettings;->mParameters:Landroid/hardware/Camera$Parameters;

    const-string v8, "video-stabilization-supported"

    invoke-virtual {v5, v8}, Landroid/hardware/Camera$Parameters;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_d

    const/16 v4, 0xf

    move-object/from16 v0, p0

    move-object/from16 v1, v21

    invoke-direct {v0, v4, v1}, Lcom/android/camera/CameraSettings;->setListPreference(ILcom/android/camera/ListPreference;)V

    goto/16 :goto_1

    :cond_15
    const/16 v4, 0x11

    const-string v5, "pref_camera_video_hd_recording_key"

    move-object/from16 v0, p1

    invoke-virtual {v0, v5}, Lcom/android/camera/PreferenceGroup;->findPreference(Ljava/lang/String;)Lcom/android/camera/ListPreference;

    move-result-object v5

    move-object/from16 v0, p0

    invoke-direct {v0, v4, v5}, Lcom/android/camera/CameraSettings;->setListPreference(ILcom/android/camera/ListPreference;)V

    goto/16 :goto_2

    :cond_16
    if-eqz v52, :cond_e

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/camera/CameraSettings;->mParameters:Landroid/hardware/Camera$Parameters;

    invoke-virtual {v4}, Landroid/hardware/Camera$Parameters;->getSupportedZSDMode()Ljava/util/List;

    move-result-object v4

    const/16 v5, 0x8

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    move-object/from16 v2, v52

    invoke-direct {v0, v1, v2, v4, v5}, Lcom/android/camera/CameraSettings;->filterUnsupportedOptions(Lcom/android/camera/PreferenceGroup;Lcom/android/camera/ListPreference;Ljava/util/List;I)V

    goto/16 :goto_3

    :cond_17
    move-object/from16 v0, v47

    move-object/from16 v1, v33

    move-object/from16 v2, v34

    invoke-interface {v0, v1, v2}, Lcom/mediatek/camera/ext/IFeatureExtension;->updateSceneStrings(Ljava/util/ArrayList;Ljava/util/ArrayList;)V

    invoke-virtual/range {v34 .. v34}, Ljava/util/ArrayList;->size()I

    move-result v31

    move/from16 v0, v31

    new-array v4, v0, [Ljava/lang/CharSequence;

    move-object/from16 v0, v33

    invoke-virtual {v0, v4}, Ljava/util/ArrayList;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v4

    check-cast v4, [Ljava/lang/CharSequence;

    move-object/from16 v0, v37

    invoke-virtual {v0, v4}, Lcom/android/camera/ListPreference;->setEntries([Ljava/lang/CharSequence;)V

    move/from16 v0, v31

    new-array v4, v0, [Ljava/lang/CharSequence;

    move-object/from16 v0, v34

    invoke-virtual {v0, v4}, Ljava/util/ArrayList;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v4

    check-cast v4, [Ljava/lang/CharSequence;

    move-object/from16 v0, v37

    invoke-virtual {v0, v4}, Lcom/android/camera/ListPreference;->setEntryValues([Ljava/lang/CharSequence;)V

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/camera/CameraSettings;->mParameters:Landroid/hardware/Camera$Parameters;

    invoke-virtual {v4}, Landroid/hardware/Camera$Parameters;->getSupportedSceneModes()Ljava/util/List;

    move-result-object v4

    const/4 v5, 0x3

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    move-object/from16 v2, v37

    invoke-direct {v0, v1, v2, v4, v5}, Lcom/android/camera/CameraSettings;->filterUnsupportedOptions(Lcom/android/camera/PreferenceGroup;Lcom/android/camera/ListPreference;Ljava/util/List;I)V

    :cond_18
    if-eqz v27, :cond_19

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/camera/CameraSettings;->mParameters:Landroid/hardware/Camera$Parameters;

    invoke-virtual {v4}, Landroid/hardware/Camera$Parameters;->getSupportedFlashModes()Ljava/util/List;

    move-result-object v4

    const/4 v5, 0x0

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    move-object/from16 v2, v27

    invoke-direct {v0, v1, v2, v4, v5}, Lcom/android/camera/CameraSettings;->filterUnsupportedOptions(Lcom/android/camera/PreferenceGroup;Lcom/android/camera/ListPreference;Ljava/util/List;I)V

    :cond_19
    if-eqz v24, :cond_1a

    const/4 v4, 0x2

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    move-object/from16 v2, v24

    invoke-direct {v0, v1, v2, v4}, Lcom/android/camera/CameraSettings;->buildExposureCompensation(Lcom/android/camera/PreferenceGroup;Lcom/android/camera/ListPreference;I)V

    :cond_1a
    if-eqz v16, :cond_1b

    const/4 v4, 0x1

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    move-object/from16 v2, v16

    invoke-direct {v0, v1, v2, v4}, Lcom/android/camera/CameraSettings;->buildCameraId(Lcom/android/camera/PreferenceGroup;Lcom/android/camera/IconListPreference;I)V

    :cond_1b
    if-eqz v46, :cond_1c

    move-object/from16 v0, p0

    move-object/from16 v1, v46

    invoke-direct {v0, v1}, Lcom/android/camera/CameraSettings;->resetIfInvalid(Lcom/android/camera/ListPreference;)V

    :cond_1c
    if-eqz v48, :cond_1d

    invoke-static {}, Lcom/android/camera/FeatureSwitcher;->isVideoLiveEffectEnabled()Z

    move-result v4

    if-nez v4, :cond_1e

    invoke-virtual/range {v48 .. v48}, Lcom/android/camera/ListPreference;->getKey()Ljava/lang/String;

    move-result-object v4

    const/16 v5, 0x13

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    invoke-direct {v0, v1, v4, v5}, Lcom/android/camera/CameraSettings;->removePreference(Lcom/android/camera/PreferenceGroup;Ljava/lang/String;I)Z

    :cond_1d
    :goto_5
    invoke-static {}, Lcom/android/camera/FeatureSwitcher;->isVoiceEnabled()Z

    move-result v4

    if-nez v4, :cond_1f

    const-string v4, "pref_voice_key"

    const/16 v5, 0x16

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    invoke-direct {v0, v1, v4, v5}, Lcom/android/camera/CameraSettings;->removePreference(Lcom/android/camera/PreferenceGroup;Ljava/lang/String;I)Z

    :goto_6
    monitor-exit v39

    return-void

    :cond_1e
    const/16 v4, 0x13

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    move-object/from16 v2, v48

    invoke-direct {v0, v1, v2, v4}, Lcom/android/camera/CameraSettings;->initVideoEffect(Lcom/android/camera/PreferenceGroup;Lcom/android/camera/ListPreference;I)V

    move-object/from16 v0, p0

    move-object/from16 v1, v48

    invoke-direct {v0, v1}, Lcom/android/camera/CameraSettings;->resetIfInvalid(Lcom/android/camera/ListPreference;)V

    goto :goto_5

    :cond_1f
    move-object/from16 v0, v50

    check-cast v0, Lcom/android/camera/VoiceListPreference;

    move-object v4, v0

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/android/camera/CameraSettings;->mContext:Lcom/android/camera/Camera;

    invoke-virtual {v5}, Lcom/android/camera/Camera;->getVoiceManager()Lcom/android/camera/VoiceManager;

    move-result-object v5

    invoke-virtual {v4, v5}, Lcom/android/camera/VoiceListPreference;->setVoiceManager(Lcom/android/camera/VoiceManager;)V

    const/16 v4, 0x16

    move-object/from16 v0, p0

    move-object/from16 v1, v50

    invoke-direct {v0, v4, v1}, Lcom/android/camera/CameraSettings;->setListPreference(ILcom/android/camera/ListPreference;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_6
.end method

.method private initVideoEffect(Lcom/android/camera/PreferenceGroup;Lcom/android/camera/ListPreference;I)V
    .locals 10
    .param p1    # Lcom/android/camera/PreferenceGroup;
    .param p2    # Lcom/android/camera/ListPreference;
    .param p3    # I

    const/4 v1, 0x1

    invoke-virtual {p2}, Lcom/android/camera/ListPreference;->getEntryValues()[Ljava/lang/CharSequence;

    move-result-object v8

    invoke-static {v1}, Lcom/android/camera/actor/EffectsRecorder;->isEffectSupported(I)Z

    move-result v3

    const/4 v9, 0x2

    invoke-static {v9}, Lcom/android/camera/actor/EffectsRecorder;->isEffectSupported(I)Z

    move-result v9

    if-eqz v9, :cond_1

    iget-object v9, p0, Lcom/android/camera/CameraSettings;->mParameters:Landroid/hardware/Camera$Parameters;

    invoke-virtual {v9}, Landroid/hardware/Camera$Parameters;->isAutoExposureLockSupported()Z

    move-result v9

    if-eqz v9, :cond_1

    iget-object v9, p0, Lcom/android/camera/CameraSettings;->mParameters:Landroid/hardware/Camera$Parameters;

    invoke-virtual {v9}, Landroid/hardware/Camera$Parameters;->isAutoWhiteBalanceLockSupported()Z

    move-result v9

    if-eqz v9, :cond_1

    :goto_0
    new-instance v6, Ljava/util/ArrayList;

    invoke-direct {v6}, Ljava/util/ArrayList;-><init>()V

    move-object v0, v8

    array-length v5, v0

    const/4 v4, 0x0

    :goto_1
    if-ge v4, v5, :cond_5

    aget-object v7, v0, v4

    invoke-virtual {v7}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v2

    if-nez v3, :cond_2

    const-string v9, "goofy_face"

    invoke-virtual {v2, v9}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v9

    if-eqz v9, :cond_2

    :cond_0
    :goto_2
    add-int/lit8 v4, v4, 0x1

    goto :goto_1

    :cond_1
    const/4 v1, 0x0

    goto :goto_0

    :cond_2
    if-nez v1, :cond_3

    const-string v9, "backdropper"

    invoke-virtual {v2, v9}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v9

    if-nez v9, :cond_0

    :cond_3
    invoke-static {}, Lcom/android/camera/FeatureSwitcher;->isLcaROM()Z

    move-result v9

    if-eqz v9, :cond_4

    const-string v9, "backdropper/file:///system/media/video/Sunset.480p.mp4"

    invoke-virtual {v2, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-nez v9, :cond_0

    const-string v9, "backdropper/file:///system/media/video/AndroidInSpace.480p.mp4"

    invoke-virtual {v2, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-nez v9, :cond_0

    :cond_4
    invoke-virtual {v6, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_2

    :cond_5
    invoke-direct {p0, p1, p2, v6, p3}, Lcom/android/camera/CameraSettings;->filterUnsupportedOptions(Lcom/android/camera/PreferenceGroup;Lcom/android/camera/ListPreference;Ljava/util/List;I)V

    return-void
.end method

.method public static initialCameraPictureSize(Landroid/content/Context;Landroid/hardware/Camera$Parameters;)V
    .locals 8
    .param p0    # Landroid/content/Context;
    .param p1    # Landroid/hardware/Camera$Parameters;

    invoke-static {p0, p1}, Lcom/android/camera/CameraSettings;->buildPreviewRatios(Landroid/content/Context;Landroid/hardware/Camera$Parameters;)Ljava/util/List;

    move-result-object v4

    const/4 v3, 0x0

    if-eqz v4, :cond_0

    invoke-interface {v4}, Ljava/util/List;->size()I

    move-result v6

    if-lez v6, :cond_0

    invoke-static {p0}, Lcom/android/camera/ComboPreferences;->get(Landroid/content/Context;)Lcom/android/camera/ComboPreferences;

    move-result-object v6

    invoke-virtual {v6}, Lcom/android/camera/ComboPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-interface {v4}, Ljava/util/List;->size()I

    move-result v6

    add-int/lit8 v6, v6, -0x1

    invoke-interface {v4, v6}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    const-string v6, "pref_camera_picturesize_ratio_key"

    invoke-interface {v0, v6, v3}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->apply()V

    :cond_0
    invoke-static {p0, p1, v3}, Lcom/android/camera/CameraSettings;->buildSupportedPictureSize(Landroid/content/Context;Landroid/hardware/Camera$Parameters;Ljava/lang/String;)Ljava/util/List;

    move-result-object v5

    if-eqz v5, :cond_1

    invoke-interface {v5}, Ljava/util/List;->size()I

    move-result v6

    if-lez v6, :cond_1

    invoke-interface {v5}, Ljava/util/List;->size()I

    move-result v6

    add-int/lit8 v6, v6, -0x1

    invoke-interface {v5, v6}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    invoke-static {p0}, Lcom/android/camera/ComboPreferences;->get(Landroid/content/Context;)Lcom/android/camera/ComboPreferences;

    move-result-object v6

    invoke-virtual {v6}, Lcom/android/camera/ComboPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string v6, "pref_camera_picturesize_key"

    invoke-interface {v0, v6, v1}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->apply()V

    invoke-static {v1}, Lcom/android/camera/SettingUtils;->getSize(Ljava/lang/String;)Landroid/graphics/Point;

    move-result-object v2

    iget v6, v2, Landroid/graphics/Point;->x:I

    iget v7, v2, Landroid/graphics/Point;->y:I

    invoke-virtual {p1, v6, v7}, Landroid/hardware/Camera$Parameters;->setPictureSize(II)V

    :cond_1
    return-void
.end method

.method public static readEffectParameter(Landroid/content/SharedPreferences;)Ljava/lang/Object;
    .locals 7
    .param p0    # Landroid/content/SharedPreferences;

    const/4 v3, 0x0

    const-string v4, "pref_video_effect_key"

    const-string v5, "none"

    invoke-interface {p0, v4, v5}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    const-string v4, "none"

    invoke-virtual {v1, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_1

    move-object v0, v3

    :cond_0
    :goto_0
    return-object v0

    :cond_1
    const/16 v4, 0x2f

    invoke-virtual {v1, v4}, Ljava/lang/String;->indexOf(I)I

    move-result v2

    add-int/lit8 v4, v2, 0x1

    invoke-virtual {v1, v4}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v0

    const-string v4, "goofy_face"

    invoke-virtual {v1, v4}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_7

    const-string v4, "squeeze"

    invoke-virtual {v0, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_2

    const/4 v3, 0x0

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    goto :goto_0

    :cond_2
    const-string v4, "big_eyes"

    invoke-virtual {v0, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_3

    const/4 v3, 0x1

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    goto :goto_0

    :cond_3
    const-string v4, "big_mouth"

    invoke-virtual {v0, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_4

    const/4 v3, 0x2

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    goto :goto_0

    :cond_4
    const-string v4, "small_mouth"

    invoke-virtual {v0, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_5

    const/4 v3, 0x3

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    goto :goto_0

    :cond_5
    const-string v4, "big_nose"

    invoke-virtual {v0, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_6

    const/4 v3, 0x4

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    goto :goto_0

    :cond_6
    const-string v4, "small_eyes"

    invoke-virtual {v0, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_8

    const/4 v3, 0x5

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    goto :goto_0

    :cond_7
    const-string v4, "backdropper"

    invoke-virtual {v1, v4}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v4

    if-nez v4, :cond_0

    :cond_8
    const-string v4, "CameraSettings"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "Invalid effect selection: "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/android/camera/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    move-object v0, v3

    goto/16 :goto_0
.end method

.method public static readEffectType(Landroid/content/SharedPreferences;)I
    .locals 5
    .param p0    # Landroid/content/SharedPreferences;

    const/4 v1, 0x0

    const-string v2, "pref_video_effect_key"

    const-string v3, "none"

    invoke-interface {p0, v2, v3}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    const-string v2, "none"

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    :goto_0
    return v1

    :cond_0
    const-string v2, "goofy_face"

    invoke-virtual {v0, v2}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_1

    const/4 v1, 0x1

    goto :goto_0

    :cond_1
    const-string v2, "backdropper"

    invoke-virtual {v0, v2}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_2

    const/4 v1, 0x2

    goto :goto_0

    :cond_2
    const-string v2, "CameraSettings"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Invalid effect selection: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/android/camera/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method public static readExposure(Lcom/android/camera/ComboPreferences;)I
    .locals 5
    .param p0    # Lcom/android/camera/ComboPreferences;

    const-string v2, "pref_camera_exposure_key"

    const-string v3, "0"

    invoke-virtual {p0, v2, v3}, Lcom/android/camera/ComboPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    :try_start_0
    invoke-static {v1}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result v2

    :goto_0
    return v2

    :catch_0
    move-exception v0

    const-string v2, "CameraSettings"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Invalid exposure: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/android/camera/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v2, 0x0

    goto :goto_0
.end method

.method public static readPreferredCamera3DMode(Landroid/content/SharedPreferences;)Ljava/lang/String;
    .locals 2
    .param p0    # Landroid/content/SharedPreferences;

    const-string v0, "pref_stereo3d_mode_key"

    const-string v1, "0"

    invoke-interface {p0, v0, v1}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public static readPreferredCameraId(Landroid/content/SharedPreferences;)I
    .locals 2
    .param p0    # Landroid/content/SharedPreferences;

    const-string v0, "pref_camera_id_key"

    const-string v1, "0"

    invoke-interface {p0, v0, v1}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v0

    return v0
.end method

.method private removePreference(I)V
    .locals 2
    .param p1    # I

    iget-object v0, p0, Lcom/android/camera/CameraSettings;->mContext:Lcom/android/camera/Camera;

    invoke-virtual {v0}, Lcom/android/camera/Camera;->getSettingChecker()Lcom/android/camera/SettingChecker;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, p1, v1}, Lcom/android/camera/SettingChecker;->setListPreference(ILcom/android/camera/ListPreference;)V

    return-void
.end method

.method private removePreference(Lcom/android/camera/PreferenceGroup;Ljava/lang/String;I)Z
    .locals 5
    .param p1    # Lcom/android/camera/PreferenceGroup;
    .param p2    # Ljava/lang/String;
    .param p3    # I

    const/4 v4, 0x1

    const/4 v1, 0x0

    invoke-virtual {p1}, Lcom/android/camera/PreferenceGroup;->size()I

    move-result v2

    :goto_0
    if-ge v1, v2, :cond_2

    invoke-virtual {p1, v1}, Lcom/android/camera/PreferenceGroup;->get(I)Lcom/android/camera/CameraPreference;

    move-result-object v0

    instance-of v3, v0, Lcom/android/camera/PreferenceGroup;

    if-eqz v3, :cond_0

    move-object v3, v0

    check-cast v3, Lcom/android/camera/PreferenceGroup;

    invoke-direct {p0, v3, p2, p3}, Lcom/android/camera/CameraSettings;->removePreference(Lcom/android/camera/PreferenceGroup;Ljava/lang/String;I)Z

    move-result v3

    if-eqz v3, :cond_0

    move v3, v4

    :goto_1
    return v3

    :cond_0
    instance-of v3, v0, Lcom/android/camera/ListPreference;

    if-eqz v3, :cond_1

    check-cast v0, Lcom/android/camera/ListPreference;

    invoke-virtual {v0}, Lcom/android/camera/ListPreference;->getKey()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_1

    invoke-virtual {p1, v1}, Lcom/android/camera/PreferenceGroup;->removePreference(I)V

    invoke-direct {p0, p3}, Lcom/android/camera/CameraSettings;->removePreference(I)V

    move v3, v4

    goto :goto_1

    :cond_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_2
    const/4 v3, 0x0

    goto :goto_1
.end method

.method private resetIfInvalid(Lcom/android/camera/ListPreference;)V
    .locals 1
    .param p1    # Lcom/android/camera/ListPreference;

    const/4 v0, 0x1

    invoke-direct {p0, p1, v0}, Lcom/android/camera/CameraSettings;->resetIfInvalid(Lcom/android/camera/ListPreference;Z)V

    return-void
.end method

.method private resetIfInvalid(Lcom/android/camera/ListPreference;Z)V
    .locals 3
    .param p1    # Lcom/android/camera/ListPreference;
    .param p2    # Z

    invoke-virtual {p1}, Lcom/android/camera/ListPreference;->getValue()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Lcom/android/camera/ListPreference;->findIndexOfValue(Ljava/lang/String;)I

    move-result v1

    const/4 v2, -0x1

    if-ne v1, v2, :cond_0

    if-eqz p2, :cond_1

    const/4 v1, 0x0

    invoke-virtual {p1, v1}, Lcom/android/camera/ListPreference;->setValueIndex(I)V

    :cond_0
    :goto_0
    return-void

    :cond_1
    invoke-virtual {p1}, Lcom/android/camera/ListPreference;->getEntryValues()[Ljava/lang/CharSequence;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-virtual {p1}, Lcom/android/camera/ListPreference;->getEntryValues()[Ljava/lang/CharSequence;

    move-result-object v1

    array-length v1, v1

    if-lez v1, :cond_0

    invoke-virtual {p1}, Lcom/android/camera/ListPreference;->getEntryValues()[Ljava/lang/CharSequence;

    move-result-object v1

    array-length v1, v1

    add-int/lit8 v1, v1, -0x1

    invoke-virtual {p1, v1}, Lcom/android/camera/ListPreference;->setValueIndex(I)V

    goto :goto_0
.end method

.method public static restorePreferences(Landroid/content/Context;Lcom/android/camera/ComboPreferences;Landroid/hardware/Camera$Parameters;)V
    .locals 6
    .param p0    # Landroid/content/Context;
    .param p1    # Lcom/android/camera/ComboPreferences;
    .param p2    # Landroid/hardware/Camera$Parameters;

    const/4 v5, -0x1

    invoke-static {p1}, Lcom/android/camera/CameraSettings;->readPreferredCameraId(Landroid/content/SharedPreferences;)I

    move-result v1

    invoke-static {}, Lcom/android/camera/CameraHolder;->instance()Lcom/android/camera/CameraHolder;

    move-result-object v4

    invoke-virtual {v4}, Lcom/android/camera/CameraHolder;->getBackCameraId()I

    move-result v0

    if-eq v0, v5, :cond_0

    invoke-virtual {p1, p0, v0}, Lcom/android/camera/ComboPreferences;->setLocalId(Landroid/content/Context;I)V

    invoke-virtual {p1}, Lcom/android/camera/ComboPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v2

    invoke-interface {v2}, Landroid/content/SharedPreferences$Editor;->clear()Landroid/content/SharedPreferences$Editor;

    invoke-interface {v2}, Landroid/content/SharedPreferences$Editor;->apply()V

    :cond_0
    invoke-static {}, Lcom/android/camera/CameraHolder;->instance()Lcom/android/camera/CameraHolder;

    move-result-object v4

    invoke-virtual {v4}, Lcom/android/camera/CameraHolder;->getFrontCameraId()I

    move-result v3

    if-eq v3, v5, :cond_1

    invoke-virtual {p1, p0, v3}, Lcom/android/camera/ComboPreferences;->setLocalId(Landroid/content/Context;I)V

    invoke-virtual {p1}, Lcom/android/camera/ComboPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v2

    invoke-interface {v2}, Landroid/content/SharedPreferences$Editor;->clear()Landroid/content/SharedPreferences$Editor;

    invoke-interface {v2}, Landroid/content/SharedPreferences$Editor;->apply()V

    :cond_1
    invoke-virtual {p1, p0, v1}, Lcom/android/camera/ComboPreferences;->setLocalId(Landroid/content/Context;I)V

    invoke-virtual {p1}, Lcom/android/camera/ComboPreferences;->getGlobal()Landroid/content/SharedPreferences;

    move-result-object v4

    invoke-static {v4}, Lcom/android/camera/CameraSettings;->upgradeGlobalPreferences(Landroid/content/SharedPreferences;)V

    invoke-virtual {p1}, Lcom/android/camera/ComboPreferences;->getLocal()Landroid/content/SharedPreferences;

    move-result-object v4

    invoke-static {v4}, Lcom/android/camera/CameraSettings;->upgradeLocalPreferences(Landroid/content/SharedPreferences;)V

    invoke-static {p0, p2}, Lcom/android/camera/CameraSettings;->initialCameraPictureSize(Landroid/content/Context;Landroid/hardware/Camera$Parameters;)V

    invoke-static {p1, v1}, Lcom/android/camera/CameraSettings;->writePreferredCameraId(Landroid/content/SharedPreferences;I)V

    return-void
.end method

.method public static setCameraPictureSize(Ljava/lang/String;Ljava/util/List;Landroid/hardware/Camera$Parameters;Ljava/lang/String;Landroid/content/Context;)Z
    .locals 10
    .param p0    # Ljava/lang/String;
    .param p2    # Landroid/hardware/Camera$Parameters;
    .param p3    # Ljava/lang/String;
    .param p4    # Landroid/content/Context;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/util/List",
            "<",
            "Landroid/hardware/Camera$Size;",
            ">;",
            "Landroid/hardware/Camera$Parameters;",
            "Ljava/lang/String;",
            "Landroid/content/Context;",
            ")Z"
        }
    .end annotation

    const/16 v9, 0x78

    const/4 v5, 0x0

    const/4 v4, -0x1

    sget-boolean v6, Lcom/android/camera/CameraSettings;->LOG:Z

    if-eqz v6, :cond_0

    const-string v6, "CameraSettings"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "setCameraPictureSize("

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, ")"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Lcom/android/camera/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    invoke-virtual {p0, v9}, Ljava/lang/String;->indexOf(I)I

    move-result v1

    if-ne v1, v4, :cond_1

    move v4, v5

    :goto_0
    return v4

    :cond_1
    invoke-static {p4, p2, p3}, Lcom/android/camera/CameraSettings;->buildSupportedPictureSize(Landroid/content/Context;Landroid/hardware/Camera$Parameters;Ljava/lang/String;)Ljava/util/List;

    move-result-object v2

    sget-object v6, Lcom/android/camera/SettingChecker;->MAPPING_FINDER_PICTURE_SIZE:Lcom/android/camera/Restriction$MappingFinder;

    invoke-interface {v6, p0, v2}, Lcom/android/camera/Restriction$MappingFinder;->find(Ljava/lang/String;Ljava/util/List;)Ljava/lang/String;

    move-result-object p0

    if-nez p0, :cond_2

    move v1, v4

    :goto_1
    invoke-virtual {p0, v5, v1}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v3

    add-int/lit8 v4, v1, 0x1

    invoke-virtual {p0, v4}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v0

    invoke-virtual {p2, v3, v0}, Landroid/hardware/Camera$Parameters;->setPictureSize(II)V

    const/4 v4, 0x1

    goto :goto_0

    :cond_2
    invoke-virtual {p0, v9}, Ljava/lang/String;->indexOf(I)I

    move-result v1

    goto :goto_1
.end method

.method private setListPreference(ILcom/android/camera/ListPreference;)V
    .locals 1
    .param p1    # I
    .param p2    # Lcom/android/camera/ListPreference;

    iget-object v0, p0, Lcom/android/camera/CameraSettings;->mContext:Lcom/android/camera/Camera;

    invoke-virtual {v0}, Lcom/android/camera/Camera;->getSettingChecker()Lcom/android/camera/SettingChecker;

    move-result-object v0

    invoke-virtual {v0, p1, p2}, Lcom/android/camera/SettingChecker;->setListPreference(ILcom/android/camera/ListPreference;)V

    return-void
.end method

.method private static sizeListToStringList(Ljava/util/List;)Ljava/util/List;
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Landroid/hardware/Camera$Size;",
            ">;)",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    invoke-interface {p0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/hardware/Camera$Size;

    sget-object v3, Ljava/util/Locale;->ENGLISH:Ljava/util/Locale;

    const-string v4, "%dx%d"

    const/4 v5, 0x2

    new-array v5, v5, [Ljava/lang/Object;

    const/4 v6, 0x0

    iget v7, v2, Landroid/hardware/Camera$Size;->width:I

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    aput-object v7, v5, v6

    const/4 v6, 0x1

    iget v7, v2, Landroid/hardware/Camera$Size;->height:I

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    aput-object v7, v5, v6

    invoke-static {v3, v4, v5}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_0
    return-object v1
.end method

.method private static toleranceRatio(DD)Z
    .locals 5
    .param p0    # D
    .param p2    # D

    const/4 v0, 0x1

    const-wide/16 v1, 0x0

    cmpl-double v1, p2, v1

    if-lez v1, :cond_0

    sub-double v1, p0, p2

    invoke-static {v1, v2}, Ljava/lang/Math;->abs(D)D

    move-result-wide v1

    const-wide v3, 0x3fb1eb851eb851ecL

    cmpg-double v1, v1, v3

    if-gtz v1, :cond_2

    const/4 v0, 0x1

    :cond_0
    :goto_0
    sget-boolean v1, Lcom/android/camera/CameraSettings;->LOG:Z

    if-eqz v1, :cond_1

    const-string v1, "CameraSettings"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "toleranceRatio("

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p0, p1}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ", "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p2, p3}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ") return "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/android/camera/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    :cond_1
    return v0

    :cond_2
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private static upgradeCameraId(Landroid/content/SharedPreferences;)V
    .locals 3
    .param p0    # Landroid/content/SharedPreferences;

    invoke-static {p0}, Lcom/android/camera/CameraSettings;->readPreferredCameraId(Landroid/content/SharedPreferences;)I

    move-result v0

    if-nez v0, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    invoke-static {}, Lcom/android/camera/CameraHolder;->instance()Lcom/android/camera/CameraHolder;

    move-result-object v2

    invoke-virtual {v2}, Lcom/android/camera/CameraHolder;->getNumberOfCameras()I

    move-result v1

    if-ltz v0, :cond_2

    if-lt v0, v1, :cond_0

    :cond_2
    const/4 v2, 0x0

    invoke-static {p0, v2}, Lcom/android/camera/CameraSettings;->writePreferredCameraId(Landroid/content/SharedPreferences;I)V

    goto :goto_0
.end method

.method public static upgradeGlobalPreferences(Landroid/content/SharedPreferences;)V
    .locals 0
    .param p0    # Landroid/content/SharedPreferences;

    invoke-static {p0}, Lcom/android/camera/CameraSettings;->upgradeOldVersion(Landroid/content/SharedPreferences;)V

    invoke-static {p0}, Lcom/android/camera/CameraSettings;->upgradeCameraId(Landroid/content/SharedPreferences;)V

    return-void
.end method

.method public static upgradeLocalPreferences(Landroid/content/SharedPreferences;)V
    .locals 6
    .param p0    # Landroid/content/SharedPreferences;

    const/4 v5, 0x2

    :try_start_0
    const-string v3, "pref_local_version_key"

    const/4 v4, 0x0

    invoke-interface {p0, v3, v4}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result v2

    :goto_0
    if-ne v2, v5, :cond_0

    :goto_1
    return-void

    :catch_0
    move-exception v1

    const/4 v2, 0x0

    goto :goto_0

    :cond_0
    invoke-interface {p0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const/4 v3, 0x1

    if-ne v2, v3, :cond_1

    const-string v3, "pref_video_quality_key"

    invoke-interface {v0, v3}, Landroid/content/SharedPreferences$Editor;->remove(Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    :cond_1
    const-string v3, "pref_local_version_key"

    invoke-interface {v0, v3, v5}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->apply()V

    goto :goto_1
.end method

.method private static upgradeOldVersion(Landroid/content/SharedPreferences;)V
    .locals 8
    .param p0    # Landroid/content/SharedPreferences;

    const/4 v7, 0x5

    const/4 v6, 0x0

    :try_start_0
    const-string v4, "pref_version_key"

    const/4 v5, 0x0

    invoke-interface {p0, v4, v5}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result v3

    :goto_0
    if-ne v3, v7, :cond_0

    :goto_1
    return-void

    :catch_0
    move-exception v1

    const/4 v3, 0x0

    goto :goto_0

    :cond_0
    invoke-interface {p0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    if-nez v3, :cond_1

    const/4 v3, 0x1

    :cond_1
    const/4 v4, 0x1

    if-ne v3, v4, :cond_2

    const-string v4, "pref_camera_jpegquality_key"

    const-string v5, "85"

    invoke-interface {p0, v4, v5}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    const-string v4, "65"

    invoke-virtual {v2, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_5

    const-string v2, "normal"

    :goto_2
    const-string v4, "pref_camera_jpegquality_key"

    invoke-interface {v0, v4, v2}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    const/4 v3, 0x2

    :cond_2
    const/4 v4, 0x2

    if-ne v3, v4, :cond_3

    const-string v5, "pref_camera_recordlocation_key"

    const-string v4, "pref_camera_recordlocation_key"

    invoke-interface {p0, v4, v6}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v4

    if-eqz v4, :cond_7

    const-string v4, "on"

    :goto_3
    invoke-interface {v0, v5, v4}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    const/4 v3, 0x3

    :cond_3
    const/4 v4, 0x3

    if-ne v3, v4, :cond_4

    const-string v4, "pref_camera_videoquality_key"

    invoke-interface {v0, v4}, Landroid/content/SharedPreferences$Editor;->remove(Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    const-string v4, "pref_camera_video_duration_key"

    invoke-interface {v0, v4}, Landroid/content/SharedPreferences$Editor;->remove(Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    :cond_4
    const-string v4, "pref_version_key"

    invoke-interface {v0, v4, v7}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->apply()V

    goto :goto_1

    :cond_5
    const-string v4, "75"

    invoke-virtual {v2, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_6

    const-string v2, "fine"

    goto :goto_2

    :cond_6
    const-string v2, "superfine"

    goto :goto_2

    :cond_7
    const-string v4, "none"

    goto :goto_3
.end method

.method public static writePreferredCamera3DMode(Landroid/content/SharedPreferences;Ljava/lang/String;)V
    .locals 2
    .param p0    # Landroid/content/SharedPreferences;
    .param p1    # Ljava/lang/String;

    invoke-interface {p0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string v1, "pref_stereo3d_mode_key"

    invoke-interface {v0, v1, p1}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->apply()V

    return-void
.end method

.method public static writePreferredCameraId(Landroid/content/SharedPreferences;I)V
    .locals 3
    .param p0    # Landroid/content/SharedPreferences;
    .param p1    # I

    invoke-interface {p0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string v1, "pref_camera_id_key"

    invoke-static {p1}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->apply()V

    return-void
.end method

.method public static writePreferredVideoEffect(Landroid/content/SharedPreferences;ILjava/lang/Object;)V
    .locals 9
    .param p0    # Landroid/content/SharedPreferences;
    .param p1    # I
    .param p2    # Ljava/lang/Object;

    const-string v2, "none"

    const/4 v1, 0x0

    packed-switch p1, :pswitch_data_0

    :goto_0
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v6, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    if-nez v1, :cond_1

    const-string v6, ""

    :goto_1
    invoke-virtual {v7, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-interface {p0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string v6, "pref_video_effect_key"

    invoke-interface {v0, v6, v4}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->apply()V

    sget-boolean v6, Lcom/android/camera/CameraSettings;->LOG:Z

    if-eqz v6, :cond_0

    const-string v6, "CameraSettings"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "writePreferredVideoEffect("

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, ", "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, ") write "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Lcom/android/camera/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    return-void

    :pswitch_0
    const-string v2, "none"

    goto :goto_0

    :pswitch_1
    const-string v2, "goofy_face"

    const-string v1, "squeeze"

    const/4 v5, 0x0

    :try_start_0
    invoke-static {p2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v6

    invoke-static {v6}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v5

    :goto_2
    packed-switch v5, :pswitch_data_1

    goto :goto_0

    :pswitch_2
    const-string v1, "squeeze"

    goto :goto_0

    :catch_0
    move-exception v3

    invoke-virtual {v3}, Ljava/lang/Throwable;->printStackTrace()V

    goto :goto_2

    :pswitch_3
    const-string v1, "big_eyes"

    goto :goto_0

    :pswitch_4
    const-string v1, "big_mouth"

    goto :goto_0

    :pswitch_5
    const-string v1, "small_mouth"

    goto :goto_0

    :pswitch_6
    const-string v1, "big_nose"

    goto :goto_0

    :pswitch_7
    const-string v1, "small_eyes"

    goto :goto_0

    :pswitch_8
    const-string v2, "backdropper"

    invoke-static {p2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    goto/16 :goto_0

    :cond_1
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "/"

    invoke-virtual {v6, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    goto/16 :goto_1

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_8
    .end packed-switch

    :pswitch_data_1
    .packed-switch 0x0
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_7
    .end packed-switch
.end method


# virtual methods
.method public getPreferenceGroup(I)Lcom/android/camera/PreferenceGroup;
    .locals 3
    .param p1    # I

    new-instance v1, Lcom/android/camera/PreferenceInflater;

    iget-object v2, p0, Lcom/android/camera/CameraSettings;->mContext:Lcom/android/camera/Camera;

    invoke-direct {v1, v2}, Lcom/android/camera/PreferenceInflater;-><init>(Landroid/content/Context;)V

    invoke-virtual {v1, p1}, Lcom/android/camera/PreferenceInflater;->inflate(I)Lcom/android/camera/CameraPreference;

    move-result-object v0

    check-cast v0, Lcom/android/camera/PreferenceGroup;

    invoke-direct {p0, v0}, Lcom/android/camera/CameraSettings;->initPreference(Lcom/android/camera/PreferenceGroup;)V

    return-object v0
.end method

.method public removePreferenceFromScreen(Lcom/android/camera/PreferenceGroup;Ljava/lang/String;I)V
    .locals 0
    .param p1    # Lcom/android/camera/PreferenceGroup;
    .param p2    # Ljava/lang/String;
    .param p3    # I

    invoke-direct {p0, p1, p2, p3}, Lcom/android/camera/CameraSettings;->removePreference(Lcom/android/camera/PreferenceGroup;Ljava/lang/String;I)Z

    return-void
.end method
