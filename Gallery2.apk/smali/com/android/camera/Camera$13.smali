.class Lcom/android/camera/Camera$13;
.super Ljava/lang/Object;
.source "Camera.java"

# interfaces
.implements Lcom/android/camera/manager/SettingManager$SettingListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/camera/Camera;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/camera/Camera;


# direct methods
.method constructor <init>(Lcom/android/camera/Camera;)V
    .locals 0

    iput-object p1, p0, Lcom/android/camera/Camera$13;->this$0:Lcom/android/camera/Camera;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onRestorePreferencesClicked()V
    .locals 7

    const/4 v1, 0x0

    iget-object v0, p0, Lcom/android/camera/Camera$13;->this$0:Lcom/android/camera/Camera;

    invoke-static {v0}, Lcom/android/camera/Camera;->access$4000(Lcom/android/camera/Camera;)Lcom/android/camera/manager/SettingManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/android/camera/manager/ViewManager;->isEnabled()Z

    move-result v0

    if-nez v0, :cond_0

    :goto_0
    return-void

    :cond_0
    new-instance v4, Lcom/android/camera/Camera$13$1;

    invoke-direct {v4, p0}, Lcom/android/camera/Camera$13$1;-><init>(Lcom/android/camera/Camera$13;)V

    iget-object v0, p0, Lcom/android/camera/Camera$13;->this$0:Lcom/android/camera/Camera;

    iget-object v2, p0, Lcom/android/camera/Camera$13;->this$0:Lcom/android/camera/Camera;

    const v3, 0x7f0c00ff

    invoke-virtual {v2, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, Lcom/android/camera/Camera$13;->this$0:Lcom/android/camera/Camera;

    const v5, 0x104000a

    invoke-virtual {v3, v5}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v3

    iget-object v5, p0, Lcom/android/camera/Camera$13;->this$0:Lcom/android/camera/Camera;

    const/high16 v6, 0x1040000

    invoke-virtual {v5, v6}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v5

    move-object v6, v1

    invoke-virtual/range {v0 .. v6}, Lcom/android/camera/Camera;->showAlertDialog(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Runnable;Ljava/lang/String;Ljava/lang/Runnable;)V

    goto :goto_0
.end method

.method public onSharedPreferenceChanged()V
    .locals 10

    const/4 v6, 0x1

    const/4 v5, 0x0

    iget-object v7, p0, Lcom/android/camera/Camera$13;->this$0:Lcom/android/camera/Camera;

    invoke-static {v7}, Lcom/android/camera/Camera;->access$1700(Lcom/android/camera/Camera;)Lcom/android/camera/ComboPreferences;

    move-result-object v7

    invoke-static {v7}, Lcom/android/camera/CameraSettings;->readEffectType(Landroid/content/SharedPreferences;)I

    move-result v2

    iget-object v7, p0, Lcom/android/camera/Camera$13;->this$0:Lcom/android/camera/Camera;

    invoke-static {v7}, Lcom/android/camera/Camera;->access$1700(Lcom/android/camera/Camera;)Lcom/android/camera/ComboPreferences;

    move-result-object v7

    invoke-static {v7}, Lcom/android/camera/CameraSettings;->readEffectParameter(Landroid/content/SharedPreferences;)Ljava/lang/Object;

    move-result-object v1

    iget-object v7, p0, Lcom/android/camera/Camera$13;->this$0:Lcom/android/camera/Camera;

    invoke-static {v7}, Lcom/android/camera/Camera;->access$2000(Lcom/android/camera/Camera;)I

    move-result v7

    if-eqz v7, :cond_3

    move v4, v6

    :goto_0
    if-eqz v2, :cond_4

    move v3, v6

    :goto_1
    if-nez v1, :cond_0

    iget-object v7, p0, Lcom/android/camera/Camera$13;->this$0:Lcom/android/camera/Camera;

    invoke-static {v7}, Lcom/android/camera/Camera;->access$2100(Lcom/android/camera/Camera;)Ljava/lang/Object;

    move-result-object v7

    if-eqz v7, :cond_5

    :cond_0
    if-eqz v1, :cond_1

    iget-object v7, p0, Lcom/android/camera/Camera$13;->this$0:Lcom/android/camera/Camera;

    invoke-static {v7}, Lcom/android/camera/Camera;->access$2100(Lcom/android/camera/Camera;)Ljava/lang/Object;

    move-result-object v7

    invoke-virtual {v1, v7}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-nez v7, :cond_5

    :cond_1
    move v0, v6

    :goto_2
    invoke-static {}, Lcom/android/camera/Camera;->access$000()Z

    move-result v7

    if-eqz v7, :cond_2

    const-string v7, "Camera"

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "onSharedPreferenceChanged() effectType="

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, ", effectParameter="

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, ", oldIsEffects="

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v4}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, ", newIsEffects="

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, ", mLastEffectType="

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    iget-object v9, p0, Lcom/android/camera/Camera$13;->this$0:Lcom/android/camera/Camera;

    invoke-static {v9}, Lcom/android/camera/Camera;->access$2000(Lcom/android/camera/Camera;)I

    move-result v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, ", mLastEffectParameter="

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    iget-object v9, p0, Lcom/android/camera/Camera$13;->this$0:Lcom/android/camera/Camera;

    invoke-static {v9}, Lcom/android/camera/Camera;->access$2100(Lcom/android/camera/Camera;)Ljava/lang/Object;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8}, Lcom/android/camera/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    :cond_2
    iget-object v7, p0, Lcom/android/camera/Camera$13;->this$0:Lcom/android/camera/Camera;

    invoke-static {v7, v2}, Lcom/android/camera/Camera;->access$2002(Lcom/android/camera/Camera;I)I

    iget-object v7, p0, Lcom/android/camera/Camera$13;->this$0:Lcom/android/camera/Camera;

    invoke-static {v7, v1}, Lcom/android/camera/Camera;->access$2102(Lcom/android/camera/Camera;Ljava/lang/Object;)Ljava/lang/Object;

    if-eq v4, v3, :cond_9

    iget-object v7, p0, Lcom/android/camera/Camera$13;->this$0:Lcom/android/camera/Camera;

    invoke-static {v7}, Lcom/android/camera/Camera;->access$4000(Lcom/android/camera/Camera;)Lcom/android/camera/manager/SettingManager;

    move-result-object v7

    invoke-virtual {v7, v6}, Lcom/android/camera/manager/SettingManager;->collapse(Z)Z

    if-eqz v3, :cond_7

    iget-object v6, p0, Lcom/android/camera/Camera$13;->this$0:Lcom/android/camera/Camera;

    invoke-virtual {v6}, Lcom/android/camera/Camera;->isNonePickIntent()Z

    move-result v6

    if-eqz v6, :cond_6

    iget-object v5, p0, Lcom/android/camera/Camera$13;->this$0:Lcom/android/camera/Camera;

    invoke-static {v5}, Lcom/android/camera/Camera;->access$4100(Lcom/android/camera/Camera;)Lcom/android/camera/manager/ModePicker;

    move-result-object v5

    const/16 v6, 0x9

    invoke-virtual {v5, v6}, Lcom/android/camera/manager/ModePicker;->setCurrentMode(I)V

    :goto_3
    return-void

    :cond_3
    move v4, v5

    goto/16 :goto_0

    :cond_4
    move v3, v5

    goto/16 :goto_1

    :cond_5
    move v0, v5

    goto/16 :goto_2

    :cond_6
    iget-object v6, p0, Lcom/android/camera/Camera$13;->this$0:Lcom/android/camera/Camera;

    invoke-static {v6, v5}, Lcom/android/camera/Camera;->access$1400(Lcom/android/camera/Camera;Z)V

    goto :goto_3

    :cond_7
    iget-object v6, p0, Lcom/android/camera/Camera$13;->this$0:Lcom/android/camera/Camera;

    invoke-virtual {v6}, Lcom/android/camera/Camera;->isNonePickIntent()Z

    move-result v6

    if-eqz v6, :cond_8

    iget-object v5, p0, Lcom/android/camera/Camera$13;->this$0:Lcom/android/camera/Camera;

    invoke-static {v5}, Lcom/android/camera/Camera;->access$800(Lcom/android/camera/Camera;)Lcom/android/camera/actor/CameraActor;

    move-result-object v5

    invoke-virtual {v5}, Lcom/android/camera/actor/CameraActor;->onEffectsDeactive()V

    iget-object v5, p0, Lcom/android/camera/Camera$13;->this$0:Lcom/android/camera/Camera;

    invoke-virtual {v5}, Lcom/android/camera/Camera;->backToLastMode()V

    goto :goto_3

    :cond_8
    iget-object v6, p0, Lcom/android/camera/Camera$13;->this$0:Lcom/android/camera/Camera;

    invoke-static {v6}, Lcom/android/camera/Camera;->access$800(Lcom/android/camera/Camera;)Lcom/android/camera/actor/CameraActor;

    move-result-object v6

    invoke-virtual {v6}, Lcom/android/camera/actor/CameraActor;->onEffectsDeactive()V

    iget-object v6, p0, Lcom/android/camera/Camera$13;->this$0:Lcom/android/camera/Camera;

    invoke-static {v6, v5}, Lcom/android/camera/Camera;->access$1400(Lcom/android/camera/Camera;Z)V

    goto :goto_3

    :cond_9
    if-eqz v0, :cond_a

    iget-object v7, p0, Lcom/android/camera/Camera$13;->this$0:Lcom/android/camera/Camera;

    invoke-static {v7}, Lcom/android/camera/Camera;->access$4000(Lcom/android/camera/Camera;)Lcom/android/camera/manager/SettingManager;

    move-result-object v7

    invoke-virtual {v7, v6}, Lcom/android/camera/manager/SettingManager;->collapse(Z)Z

    :cond_a
    iget-object v6, p0, Lcom/android/camera/Camera$13;->this$0:Lcom/android/camera/Camera;

    invoke-static {v6, v5}, Lcom/android/camera/Camera;->access$1400(Lcom/android/camera/Camera;Z)V

    goto :goto_3
.end method
