.class public Lcom/android/camera/Camera;
.super Lcom/android/camera/ActivityBase;
.source "Camera.java"

# interfaces
.implements Lcom/android/camera/CameraScreenNail$FrameListener;
.implements Lcom/android/camera/VoiceManager$Listener;
.implements Lcom/android/camera/ui/PreviewFrameLayout$OnSizeChangedListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/camera/Camera$MyOrientationEventListener;,
        Lcom/android/camera/Camera$CameraStartUpThread;,
        Lcom/android/camera/Camera$OnSingleTapUpListener;,
        Lcom/android/camera/Camera$OnFullScreenChangedListener;,
        Lcom/android/camera/Camera$Resumable;,
        Lcom/android/camera/Camera$OnPreferenceReadyListener;,
        Lcom/android/camera/Camera$OnParametersReadyListener;,
        Lcom/android/camera/Camera$OnOrientationListener;
    }
.end annotation


# static fields
.field private static final DELAY_MSG_SCREEN_SWITCH:I = 0x1d4c0

.field private static final DELAY_MSG_SHOW_ONSCREEN_VIEW:I = 0xbb8

.field private static final EXTRA_PHOTO_CROP_VALUE:Ljava/lang/String; = "crop"

.field private static final EXTRA_QUICK_CAPTURE:Ljava/lang/String; = "android.intent.extra.quickCapture"

.field private static final EXTRA_VIDEO_WALLPAPER_IDENTIFY:Ljava/lang/String; = "identity"

.field private static final EXTRA_VIDEO_WALLPAPER_IDENTIFY_VALUE:Ljava/lang/String; = "com.mediatek.vlw"

.field private static final EXTRA_VIDEO_WALLPAPER_RATION:Ljava/lang/String; = "ratio"

.field private static final LOG:Z

.field private static final MSG_APPLY_PARAMETERS_WHEN_IDEL:I = 0xc

.field private static final MSG_CAMERA_OPEN_DONE:I = 0x1

.field private static final MSG_CAMERA_PARAMETERS_READY:I = 0x2

.field private static final MSG_CAMERA_PREFERENCE_READY:I = 0x3

.field private static final MSG_CHECK_DISPLAY_ROTATION:I = 0x4

.field private static final MSG_CLEAR_SCREEN_DELAY:I = 0x7

.field private static final MSG_OPEN_CAMERA_DISABLED:I = 0xa

.field private static final MSG_OPEN_CAMERA_FAIL:I = 0x9

.field private static final MSG_SHOW_ONSCREEN_INDICATOR:I = 0x8

.field private static final MSG_SWITCH_CAMERA:I = 0x5

.field private static final MSG_SWITCH_CAMERA_START_ANIMATION:I = 0x6

.field private static final PICK_TYPE_NORMAL:I = 0x0

.field private static final PICK_TYPE_PHOTO:I = 0x1

.field private static final PICK_TYPE_VIDEO:I = 0x2

.field private static final PICK_TYPE_WALLPAPER:I = 0x3

.field public static final SHOW_INFO_LENGTH_LONG:I = 0x1388

.field public static final STATE_FOCUSING:I = 0x2

.field public static final STATE_IDLE:I = 0x1

.field public static final STATE_PREVIEW_STOPPED:I = 0x0

.field public static final STATE_RECORDING_IN_PROGRESS:I = 0x3

.field public static final STATE_SNAPSHOT_IN_PROGRESS:I = 0x3

.field public static final STATE_SWITCHING_CAMERA:I = 0x4

.field private static final TAG:Ljava/lang/String; = "Camera"

.field public static final UNKNOWN:I = -0x1

.field public static final VIEW_STATE_CAPTURE:I = 0x0

.field public static final VIEW_STATE_CONTINIUOUS:I = 0x2

.field public static final VIEW_STATE_FOCUSING:I = 0x4

.field public static final VIEW_STATE_LEARNING_VIDEO_EFFECTS:I = 0xa

.field private static final VIEW_STATE_NORMAL:I = -0x1

.field public static final VIEW_STATE_PANORAMA_CAPTURE:I = 0x9

.field public static final VIEW_STATE_PICKING:I = 0x8

.field public static final VIEW_STATE_RECORDING:I = 0x1

.field public static final VIEW_STATE_REVIEW:I = 0x6

.field public static final VIEW_STATE_SAVING:I = 0x5

.field public static final VIEW_STATE_SETTING:I = 0x3

.field private static final VIEW_STATE_SWITCHING:I = 0x7

.field private static final WALLPAPER_DEFAULT_ASPECTIO:F = 1.2f

.field private static final WALLPAPER_MIN_WIDTH:I = 0x12c


# instance fields
.field private mAcceptFloatingInfo:Z

.field private mCameraActor:Lcom/android/camera/actor/CameraActor;

.field private mCameraDevice:Lcom/android/camera/CameraManager$CameraProxy;

.field private mCameraDisabled:Z

.field private mCameraDisplayOrientation:I

.field private mCameraId:I

.field private mCameraOpened:Z

.field private mCameraSettings:Lcom/android/camera/CameraSettings;

.field private mCameraStartUpThread:Lcom/android/camera/Camera$CameraStartUpThread;

.field private mCameraState:I

.field private mCommonManagerCount:I

.field private mCropValue:Ljava/lang/String;

.field private mCurrentViewState:I

.field private mDisplayOrientation:I

.field private mDisplayRotation:I

.field private mFaceView:Lcom/android/camera/ui/FaceView;

.field private mFileSaver:Lcom/android/camera/FileSaver;

.field private mFlashMode:Ljava/lang/String;

.field private mFocusAreaIndicator:Lcom/android/camera/ui/RotateLayout;

.field private mFocusManager:Lcom/android/camera/FocusManager;

.field private mForceFinishing:Z

.field private mFullScreenListeners:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/android/camera/Camera$OnFullScreenChangedListener;",
            ">;"
        }
    .end annotation
.end field

.field private mIndicatorManager:Lcom/android/camera/manager/IndicatorManager;

.field private mInflater:Landroid/view/LayoutInflater;

.field private mInfoManager:Lcom/android/camera/manager/InfoManager;

.field private mInitialParams:Landroid/hardware/Camera$Parameters;

.field private mLastAudioBitRate:I

.field private mLastEffectParameter:Ljava/lang/Object;

.field private mLastEffectType:I

.field private mLastManagers:[Lcom/android/camera/manager/ViewManager;

.field private mLastMode:I

.field private mLastPictureSize:Landroid/hardware/Camera$Size;

.field private mLastPreviewSize:Landroid/hardware/Camera$Size;

.field private mLastVideoBitRate:I

.field private mLastVisibles:[Z

.field private mLastZsdMode:Ljava/lang/String;

.field private mLimitedDuration:I

.field private mLimitedSize:J

.field private mLocationManager:Lcom/android/camera/LocationManager;

.field private mMainHandler:Landroid/os/Handler;

.field private mModeChangedListener:Lcom/android/camera/manager/ModePicker$OnModeChangedListener;

.field private mModePicker:Lcom/android/camera/manager/ModePicker;

.field private mNumberOfCameras:I

.field private mOnResumeTime:J

.field private mOpenCameraFail:Z

.field private mOrientation:I

.field private mOrientationCompensation:I

.field private mOrientationListener:Lcom/android/camera/Camera$MyOrientationEventListener;

.field private mOrientationListeners:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/android/camera/Camera$OnOrientationListener;",
            ">;"
        }
    .end annotation
.end field

.field private mParameters:Landroid/hardware/Camera$Parameters;

.field private mParametersListeners:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/android/camera/Camera$OnParametersReadyListener;",
            ">;"
        }
    .end annotation
.end field

.field private mPendingSwitchCameraId:I

.field private mPhotoShutterListener:Lcom/android/camera/ui/ShutterButton$OnShutterButtonListener;

.field private mPickType:I

.field private mPickerListener:Lcom/android/camera/manager/PickerManager$PickerListener;

.field private mPickerManager:Lcom/android/camera/manager/PickerManager;

.field private mPreferenceGroup:Lcom/android/camera/PreferenceGroup;

.field private mPreferenceListeners:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/android/camera/Camera$OnPreferenceReadyListener;",
            ">;"
        }
    .end annotation
.end field

.field private mPreferences:Lcom/android/camera/ComboPreferences;

.field private mPreviewFrameHeight:I

.field private mPreviewFrameLayout:Lcom/android/camera/ui/PreviewFrameLayout;

.field private mPreviewFrameWidth:I

.field private mProfile:Landroid/media/CamcorderProfile;

.field private mQualityId:I

.field private mQuickCapture:Z

.field private final mReceiver:Landroid/content/BroadcastReceiver;

.field private mRemainingManager:Lcom/android/camera/manager/RemainingManager;

.field private mResumables:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/android/camera/Camera$Resumable;",
            ">;"
        }
    .end annotation
.end field

.field private mReviewManager:Lcom/android/camera/manager/ReviewManager;

.field private mRotateDialog:Lcom/android/camera/manager/RotateDialog;

.field private mRotateProgress:Lcom/android/camera/manager/RotateProgress;

.field private mRotateToast:Lcom/android/camera/manager/OnScreenHint;

.field private mSaveUri:Landroid/net/Uri;

.field private mSettingChecker:Lcom/android/camera/SettingChecker;

.field private mSettingListener:Lcom/android/camera/manager/SettingManager$SettingListener;

.field private mSettingManager:Lcom/android/camera/manager/SettingManager;

.field private mShutterManager:Lcom/android/camera/manager/ShutterManager;

.field private mStartPreviewPrerequisiteReady:Landroid/os/ConditionVariable;

.field private mStereoMode:Z

.field private mSurfaceTexture:Landroid/graphics/SurfaceTexture;

.field private mSurfaceTextureReady:Z

.field private mThumbnailManager:Lcom/android/camera/manager/ThumbnailManager;

.field private mTimelapseMs:I

.field private mVideoShutterListener:Lcom/android/camera/ui/ShutterButton$OnShutterButtonListener;

.field private mViewLayerBottom:Landroid/view/ViewGroup;

.field private mViewLayerNormal:Landroid/view/ViewGroup;

.field private mViewLayerOverlay:Landroid/view/ViewGroup;

.field private mViewLayerSetting:Landroid/view/ViewGroup;

.field private mViewLayerShutter:Landroid/view/ViewGroup;

.field private mViewLayerTop:Landroid/view/ViewGroup;

.field private mViewManagers:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/android/camera/manager/ViewManager;",
            ">;"
        }
    .end annotation
.end field

.field private mVoiceManager:Lcom/android/camera/VoiceManager;

.field private mWallpaperAspectio:F

.field private mWfdListener:Lcom/android/camera/WfdManagerLocal$Listener;

.field private mWfdLocal:Lcom/android/camera/WfdManagerLocal;

.field private mZoomManager:Lcom/android/camera/manager/ZoomManager;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    sget-boolean v0, Lcom/android/camera/Log;->LOGV:Z

    sput-boolean v0, Lcom/android/camera/Camera;->LOG:Z

    return-void
.end method

.method public constructor <init>()V
    .locals 4

    const/4 v3, 0x1

    const/4 v2, 0x0

    const/4 v1, -0x1

    invoke-direct {p0}, Lcom/android/camera/ActivityBase;-><init>()V

    iput v2, p0, Lcom/android/camera/Camera;->mCameraState:I

    new-instance v0, Landroid/os/ConditionVariable;

    invoke-direct {v0}, Landroid/os/ConditionVariable;-><init>()V

    iput-object v0, p0, Lcom/android/camera/Camera;->mStartPreviewPrerequisiteReady:Landroid/os/ConditionVariable;

    iput v1, p0, Lcom/android/camera/Camera;->mPendingSwitchCameraId:I

    iput v1, p0, Lcom/android/camera/Camera;->mOrientation:I

    iput v2, p0, Lcom/android/camera/Camera;->mOrientationCompensation:I

    new-instance v0, Lcom/android/camera/Camera$1;

    invoke-direct {v0, p0}, Lcom/android/camera/Camera$1;-><init>(Lcom/android/camera/Camera;)V

    iput-object v0, p0, Lcom/android/camera/Camera;->mMainHandler:Landroid/os/Handler;

    iput v1, p0, Lcom/android/camera/Camera;->mLastAudioBitRate:I

    iput v1, p0, Lcom/android/camera/Camera;->mLastVideoBitRate:I

    iput-boolean v3, p0, Lcom/android/camera/Camera;->mSurfaceTextureReady:Z

    new-instance v0, Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-direct {v0}, Ljava/util/concurrent/CopyOnWriteArrayList;-><init>()V

    iput-object v0, p0, Lcom/android/camera/Camera;->mFullScreenListeners:Ljava/util/List;

    new-instance v0, Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-direct {v0}, Ljava/util/concurrent/CopyOnWriteArrayList;-><init>()V

    iput-object v0, p0, Lcom/android/camera/Camera;->mPreferenceListeners:Ljava/util/List;

    new-instance v0, Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-direct {v0}, Ljava/util/concurrent/CopyOnWriteArrayList;-><init>()V

    iput-object v0, p0, Lcom/android/camera/Camera;->mParametersListeners:Ljava/util/List;

    new-instance v0, Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-direct {v0}, Ljava/util/concurrent/CopyOnWriteArrayList;-><init>()V

    iput-object v0, p0, Lcom/android/camera/Camera;->mViewManagers:Ljava/util/List;

    new-instance v0, Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-direct {v0}, Ljava/util/concurrent/CopyOnWriteArrayList;-><init>()V

    iput-object v0, p0, Lcom/android/camera/Camera;->mResumables:Ljava/util/List;

    iput v2, p0, Lcom/android/camera/Camera;->mLastEffectType:I

    new-instance v0, Lcom/android/camera/Camera$5;

    invoke-direct {v0, p0}, Lcom/android/camera/Camera$5;-><init>(Lcom/android/camera/Camera;)V

    iput-object v0, p0, Lcom/android/camera/Camera;->mModeChangedListener:Lcom/android/camera/manager/ModePicker$OnModeChangedListener;

    new-instance v0, Lcom/android/camera/Camera$6;

    invoke-direct {v0, p0}, Lcom/android/camera/Camera$6;-><init>(Lcom/android/camera/Camera;)V

    iput-object v0, p0, Lcom/android/camera/Camera;->mVideoShutterListener:Lcom/android/camera/ui/ShutterButton$OnShutterButtonListener;

    new-instance v0, Lcom/android/camera/Camera$7;

    invoke-direct {v0, p0}, Lcom/android/camera/Camera$7;-><init>(Lcom/android/camera/Camera;)V

    iput-object v0, p0, Lcom/android/camera/Camera;->mPhotoShutterListener:Lcom/android/camera/ui/ShutterButton$OnShutterButtonListener;

    new-instance v0, Lcom/android/camera/Camera$13;

    invoke-direct {v0, p0}, Lcom/android/camera/Camera$13;-><init>(Lcom/android/camera/Camera;)V

    iput-object v0, p0, Lcom/android/camera/Camera;->mSettingListener:Lcom/android/camera/manager/SettingManager$SettingListener;

    new-instance v0, Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-direct {v0}, Ljava/util/concurrent/CopyOnWriteArrayList;-><init>()V

    iput-object v0, p0, Lcom/android/camera/Camera;->mOrientationListeners:Ljava/util/List;

    new-instance v0, Lcom/android/camera/Camera$14;

    invoke-direct {v0, p0}, Lcom/android/camera/Camera$14;-><init>(Lcom/android/camera/Camera;)V

    iput-object v0, p0, Lcom/android/camera/Camera;->mPickerListener:Lcom/android/camera/manager/PickerManager$PickerListener;

    new-instance v0, Lcom/android/camera/Camera$15;

    invoke-direct {v0, p0}, Lcom/android/camera/Camera$15;-><init>(Lcom/android/camera/Camera;)V

    iput-object v0, p0, Lcom/android/camera/Camera;->mReceiver:Landroid/content/BroadcastReceiver;

    iput v1, p0, Lcom/android/camera/Camera;->mCurrentViewState:I

    iput-boolean v3, p0, Lcom/android/camera/Camera;->mAcceptFloatingInfo:Z

    new-instance v0, Lcom/android/camera/Camera$23;

    invoke-direct {v0, p0}, Lcom/android/camera/Camera$23;-><init>(Lcom/android/camera/Camera;)V

    iput-object v0, p0, Lcom/android/camera/Camera;->mWfdListener:Lcom/android/camera/WfdManagerLocal$Listener;

    return-void
.end method

.method static synthetic access$000()Z
    .locals 1

    sget-boolean v0, Lcom/android/camera/Camera;->LOG:Z

    return v0
.end method

.method static synthetic access$100(I)Ljava/lang/String;
    .locals 1
    .param p0    # I

    invoke-static {p0}, Lcom/android/camera/Camera;->getMsgLabel(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$1000(Lcom/android/camera/Camera;)Landroid/os/Handler;
    .locals 1
    .param p0    # Lcom/android/camera/Camera;

    iget-object v0, p0, Lcom/android/camera/Camera;->mMainHandler:Landroid/os/Handler;

    return-object v0
.end method

.method static synthetic access$1100(Lcom/android/camera/Camera;)V
    .locals 0
    .param p0    # Lcom/android/camera/Camera;

    invoke-direct {p0}, Lcom/android/camera/Camera;->notifyOrientationChanged()V

    return-void
.end method

.method static synthetic access$1200(Lcom/android/camera/Camera;)V
    .locals 0
    .param p0    # Lcom/android/camera/Camera;

    invoke-direct {p0}, Lcom/android/camera/Camera;->switchCamera()V

    return-void
.end method

.method static synthetic access$1300(Lcom/android/camera/Camera;)V
    .locals 0
    .param p0    # Lcom/android/camera/Camera;

    invoke-direct {p0}, Lcom/android/camera/Camera;->doShowIndicator()V

    return-void
.end method

.method static synthetic access$1400(Lcom/android/camera/Camera;Z)V
    .locals 0
    .param p0    # Lcom/android/camera/Camera;
    .param p1    # Z

    invoke-direct {p0, p1}, Lcom/android/camera/Camera;->applyParameters(Z)V

    return-void
.end method

.method static synthetic access$1502(Lcom/android/camera/Camera;Z)Z
    .locals 0
    .param p0    # Lcom/android/camera/Camera;
    .param p1    # Z

    iput-boolean p1, p0, Lcom/android/camera/Camera;->mOpenCameraFail:Z

    return p1
.end method

.method static synthetic access$1602(Lcom/android/camera/Camera;Z)Z
    .locals 0
    .param p0    # Lcom/android/camera/Camera;
    .param p1    # Z

    iput-boolean p1, p0, Lcom/android/camera/Camera;->mCameraDisabled:Z

    return p1
.end method

.method static synthetic access$1700(Lcom/android/camera/Camera;)Lcom/android/camera/ComboPreferences;
    .locals 1
    .param p0    # Lcom/android/camera/Camera;

    iget-object v0, p0, Lcom/android/camera/Camera;->mPreferences:Lcom/android/camera/ComboPreferences;

    return-object v0
.end method

.method static synthetic access$1800(Lcom/android/camera/Camera;Lcom/android/camera/ComboPreferences;)I
    .locals 1
    .param p0    # Lcom/android/camera/Camera;
    .param p1    # Lcom/android/camera/ComboPreferences;

    invoke-direct {p0, p1}, Lcom/android/camera/Camera;->getPreferredCameraId(Lcom/android/camera/ComboPreferences;)I

    move-result v0

    return v0
.end method

.method static synthetic access$1900(Lcom/android/camera/Camera;)I
    .locals 1
    .param p0    # Lcom/android/camera/Camera;

    iget v0, p0, Lcom/android/camera/Camera;->mCameraId:I

    return v0
.end method

.method static synthetic access$200(Lcom/android/camera/Camera;)Lcom/android/camera/Camera$CameraStartUpThread;
    .locals 1
    .param p0    # Lcom/android/camera/Camera;

    iget-object v0, p0, Lcom/android/camera/Camera;->mCameraStartUpThread:Lcom/android/camera/Camera$CameraStartUpThread;

    return-object v0
.end method

.method static synthetic access$2000(Lcom/android/camera/Camera;)I
    .locals 1
    .param p0    # Lcom/android/camera/Camera;

    iget v0, p0, Lcom/android/camera/Camera;->mLastEffectType:I

    return v0
.end method

.method static synthetic access$2002(Lcom/android/camera/Camera;I)I
    .locals 0
    .param p0    # Lcom/android/camera/Camera;
    .param p1    # I

    iput p1, p0, Lcom/android/camera/Camera;->mLastEffectType:I

    return p1
.end method

.method static synthetic access$202(Lcom/android/camera/Camera;Lcom/android/camera/Camera$CameraStartUpThread;)Lcom/android/camera/Camera$CameraStartUpThread;
    .locals 0
    .param p0    # Lcom/android/camera/Camera;
    .param p1    # Lcom/android/camera/Camera$CameraStartUpThread;

    iput-object p1, p0, Lcom/android/camera/Camera;->mCameraStartUpThread:Lcom/android/camera/Camera$CameraStartUpThread;

    return-object p1
.end method

.method static synthetic access$2100(Lcom/android/camera/Camera;)Ljava/lang/Object;
    .locals 1
    .param p0    # Lcom/android/camera/Camera;

    iget-object v0, p0, Lcom/android/camera/Camera;->mLastEffectParameter:Ljava/lang/Object;

    return-object v0
.end method

.method static synthetic access$2102(Lcom/android/camera/Camera;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0
    .param p0    # Lcom/android/camera/Camera;
    .param p1    # Ljava/lang/Object;

    iput-object p1, p0, Lcom/android/camera/Camera;->mLastEffectParameter:Ljava/lang/Object;

    return-object p1
.end method

.method static synthetic access$2202(Lcom/android/camera/Camera;Lcom/android/camera/CameraManager$CameraProxy;)Lcom/android/camera/CameraManager$CameraProxy;
    .locals 0
    .param p0    # Lcom/android/camera/Camera;
    .param p1    # Lcom/android/camera/CameraManager$CameraProxy;

    iput-object p1, p0, Lcom/android/camera/Camera;->mCameraDevice:Lcom/android/camera/CameraManager$CameraProxy;

    return-object p1
.end method

.method static synthetic access$2300(Lcom/android/camera/Camera;)V
    .locals 0
    .param p0    # Lcom/android/camera/Camera;

    invoke-direct {p0}, Lcom/android/camera/Camera;->prepareMockCamera()V

    return-void
.end method

.method static synthetic access$2400(Lcom/android/camera/Camera;)Z
    .locals 1
    .param p0    # Lcom/android/camera/Camera;

    iget-boolean v0, p0, Lcom/android/camera/Camera;->mCameraOpened:Z

    return v0
.end method

.method static synthetic access$2402(Lcom/android/camera/Camera;Z)Z
    .locals 0
    .param p0    # Lcom/android/camera/Camera;
    .param p1    # Z

    iput-boolean p1, p0, Lcom/android/camera/Camera;->mCameraOpened:Z

    return p1
.end method

.method static synthetic access$2500(Lcom/android/camera/Camera;)Landroid/hardware/Camera$Parameters;
    .locals 1
    .param p0    # Lcom/android/camera/Camera;

    iget-object v0, p0, Lcom/android/camera/Camera;->mInitialParams:Landroid/hardware/Camera$Parameters;

    return-object v0
.end method

.method static synthetic access$2502(Lcom/android/camera/Camera;Landroid/hardware/Camera$Parameters;)Landroid/hardware/Camera$Parameters;
    .locals 0
    .param p0    # Lcom/android/camera/Camera;
    .param p1    # Landroid/hardware/Camera$Parameters;

    iput-object p1, p0, Lcom/android/camera/Camera;->mInitialParams:Landroid/hardware/Camera$Parameters;

    return-object p1
.end method

.method static synthetic access$2600(Lcom/android/camera/Camera;)Landroid/hardware/Camera$Parameters;
    .locals 1
    .param p0    # Lcom/android/camera/Camera;

    iget-object v0, p0, Lcom/android/camera/Camera;->mParameters:Landroid/hardware/Camera$Parameters;

    return-object v0
.end method

.method static synthetic access$2602(Lcom/android/camera/Camera;Landroid/hardware/Camera$Parameters;)Landroid/hardware/Camera$Parameters;
    .locals 0
    .param p0    # Lcom/android/camera/Camera;
    .param p1    # Landroid/hardware/Camera$Parameters;

    iput-object p1, p0, Lcom/android/camera/Camera;->mParameters:Landroid/hardware/Camera$Parameters;

    return-object p1
.end method

.method static synthetic access$2700(Lcom/android/camera/Camera;)Landroid/os/ConditionVariable;
    .locals 1
    .param p0    # Lcom/android/camera/Camera;

    iget-object v0, p0, Lcom/android/camera/Camera;->mStartPreviewPrerequisiteReady:Landroid/os/ConditionVariable;

    return-object v0
.end method

.method static synthetic access$2800(Lcom/android/camera/Camera;)V
    .locals 0
    .param p0    # Lcom/android/camera/Camera;

    invoke-direct {p0}, Lcom/android/camera/Camera;->initializeFocusManager()V

    return-void
.end method

.method static synthetic access$2900(Lcom/android/camera/Camera;)V
    .locals 0
    .param p0    # Lcom/android/camera/Camera;

    invoke-direct {p0}, Lcom/android/camera/Camera;->initializeCameraPreferences()V

    return-void
.end method

.method static synthetic access$300(Lcom/android/camera/Camera;)V
    .locals 0
    .param p0    # Lcom/android/camera/Camera;

    invoke-direct {p0}, Lcom/android/camera/Camera;->notifyParametersReady()V

    return-void
.end method

.method static synthetic access$3000(Lcom/android/camera/Camera;)V
    .locals 0
    .param p0    # Lcom/android/camera/Camera;

    invoke-direct {p0}, Lcom/android/camera/Camera;->clearDeviceCallbacks()V

    return-void
.end method

.method static synthetic access$3100(Lcom/android/camera/Camera;)V
    .locals 0
    .param p0    # Lcom/android/camera/Camera;

    invoke-direct {p0}, Lcom/android/camera/Camera;->applyDeviceCallbacks()V

    return-void
.end method

.method static synthetic access$3200(Lcom/android/camera/Camera;)V
    .locals 0
    .param p0    # Lcom/android/camera/Camera;

    invoke-direct {p0}, Lcom/android/camera/Camera;->clearViewCallbacks()V

    return-void
.end method

.method static synthetic access$3300(Lcom/android/camera/Camera;)V
    .locals 0
    .param p0    # Lcom/android/camera/Camera;

    invoke-direct {p0}, Lcom/android/camera/Camera;->applayViewCallbacks()V

    return-void
.end method

.method static synthetic access$3400(Lcom/android/camera/Camera;)Lcom/android/camera/SettingChecker;
    .locals 1
    .param p0    # Lcom/android/camera/Camera;

    iget-object v0, p0, Lcom/android/camera/Camera;->mSettingChecker:Lcom/android/camera/SettingChecker;

    return-object v0
.end method

.method static synthetic access$3500(Lcom/android/camera/Camera;)V
    .locals 0
    .param p0    # Lcom/android/camera/Camera;

    invoke-direct {p0}, Lcom/android/camera/Camera;->setZoomParameter()V

    return-void
.end method

.method static synthetic access$3700(Lcom/android/camera/Camera;)I
    .locals 1
    .param p0    # Lcom/android/camera/Camera;

    iget v0, p0, Lcom/android/camera/Camera;->mCameraState:I

    return v0
.end method

.method static synthetic access$3800(Lcom/android/camera/Camera;)V
    .locals 0
    .param p0    # Lcom/android/camera/Camera;

    invoke-direct {p0}, Lcom/android/camera/Camera;->releaseCameraActor()V

    return-void
.end method

.method static synthetic access$3900(Lcom/android/camera/Camera;)I
    .locals 1
    .param p0    # Lcom/android/camera/Camera;

    iget v0, p0, Lcom/android/camera/Camera;->mLastMode:I

    return v0
.end method

.method static synthetic access$400(Lcom/android/camera/Camera;)V
    .locals 0
    .param p0    # Lcom/android/camera/Camera;

    invoke-direct {p0}, Lcom/android/camera/Camera;->notifyPreferenceReady()V

    return-void
.end method

.method static synthetic access$4000(Lcom/android/camera/Camera;)Lcom/android/camera/manager/SettingManager;
    .locals 1
    .param p0    # Lcom/android/camera/Camera;

    iget-object v0, p0, Lcom/android/camera/Camera;->mSettingManager:Lcom/android/camera/manager/SettingManager;

    return-object v0
.end method

.method static synthetic access$4100(Lcom/android/camera/Camera;)Lcom/android/camera/manager/ModePicker;
    .locals 1
    .param p0    # Lcom/android/camera/Camera;

    iget-object v0, p0, Lcom/android/camera/Camera;->mModePicker:Lcom/android/camera/manager/ModePicker;

    return-object v0
.end method

.method static synthetic access$4200(Lcom/android/camera/Camera;)V
    .locals 0
    .param p0    # Lcom/android/camera/Camera;

    invoke-direct {p0}, Lcom/android/camera/Camera;->setPreviewFrameLayoutAspectRatio()V

    return-void
.end method

.method static synthetic access$4300(Lcom/android/camera/Camera;)Lcom/android/camera/FocusManager;
    .locals 1
    .param p0    # Lcom/android/camera/Camera;

    iget-object v0, p0, Lcom/android/camera/Camera;->mFocusManager:Lcom/android/camera/FocusManager;

    return-object v0
.end method

.method static synthetic access$4400(Lcom/android/camera/Camera;)Lcom/android/camera/LocationManager;
    .locals 1
    .param p0    # Lcom/android/camera/Camera;

    iget-object v0, p0, Lcom/android/camera/Camera;->mLocationManager:Lcom/android/camera/LocationManager;

    return-object v0
.end method

.method static synthetic access$4500(Lcom/android/camera/Camera;)Lcom/android/camera/manager/ZoomManager;
    .locals 1
    .param p0    # Lcom/android/camera/Camera;

    iget-object v0, p0, Lcom/android/camera/Camera;->mZoomManager:Lcom/android/camera/manager/ZoomManager;

    return-object v0
.end method

.method static synthetic access$4600(Lcom/android/camera/Camera;)I
    .locals 1
    .param p0    # Lcom/android/camera/Camera;

    iget v0, p0, Lcom/android/camera/Camera;->mOrientationCompensation:I

    return v0
.end method

.method static synthetic access$4602(Lcom/android/camera/Camera;I)I
    .locals 0
    .param p0    # Lcom/android/camera/Camera;
    .param p1    # I

    iput p1, p0, Lcom/android/camera/Camera;->mOrientationCompensation:I

    return p1
.end method

.method static synthetic access$4700(Lcom/android/camera/Camera;)Lcom/android/camera/manager/ReviewManager;
    .locals 1
    .param p0    # Lcom/android/camera/Camera;

    iget-object v0, p0, Lcom/android/camera/Camera;->mReviewManager:Lcom/android/camera/manager/ReviewManager;

    return-object v0
.end method

.method static synthetic access$4800(Lcom/android/camera/Camera;)I
    .locals 1
    .param p0    # Lcom/android/camera/Camera;

    iget v0, p0, Lcom/android/camera/Camera;->mPendingSwitchCameraId:I

    return v0
.end method

.method static synthetic access$4802(Lcom/android/camera/Camera;I)I
    .locals 0
    .param p0    # Lcom/android/camera/Camera;
    .param p1    # I

    iput p1, p0, Lcom/android/camera/Camera;->mPendingSwitchCameraId:I

    return p1
.end method

.method static synthetic access$4902(Lcom/android/camera/Camera;Ljava/lang/String;)Ljava/lang/String;
    .locals 0
    .param p0    # Lcom/android/camera/Camera;
    .param p1    # Ljava/lang/String;

    iput-object p1, p0, Lcom/android/camera/Camera;->mFlashMode:Ljava/lang/String;

    return-object p1
.end method

.method static synthetic access$500(Lcom/android/camera/Camera;)I
    .locals 1
    .param p0    # Lcom/android/camera/Camera;

    iget v0, p0, Lcom/android/camera/Camera;->mDisplayRotation:I

    return v0
.end method

.method static synthetic access$5002(Lcom/android/camera/Camera;Z)Z
    .locals 0
    .param p0    # Lcom/android/camera/Camera;
    .param p1    # Z

    iput-boolean p1, p0, Lcom/android/camera/Camera;->mStereoMode:Z

    return p1
.end method

.method static synthetic access$5100(Lcom/android/camera/Camera;)V
    .locals 0
    .param p0    # Lcom/android/camera/Camera;

    invoke-direct {p0}, Lcom/android/camera/Camera;->refreshModeRelated()V

    return-void
.end method

.method static synthetic access$5200(Lcom/android/camera/Camera;Landroid/content/Intent;)Z
    .locals 1
    .param p0    # Lcom/android/camera/Camera;
    .param p1    # Landroid/content/Intent;

    invoke-direct {p0, p1}, Lcom/android/camera/Camera;->isSameStorage(Landroid/content/Intent;)Z

    move-result v0

    return v0
.end method

.method static synthetic access$5300(Lcom/android/camera/Camera;)Lcom/android/camera/manager/RemainingManager;
    .locals 1
    .param p0    # Lcom/android/camera/Camera;

    iget-object v0, p0, Lcom/android/camera/Camera;->mRemainingManager:Lcom/android/camera/manager/RemainingManager;

    return-object v0
.end method

.method static synthetic access$5400(Lcom/android/camera/Camera;Landroid/net/Uri;)Z
    .locals 1
    .param p0    # Lcom/android/camera/Camera;
    .param p1    # Landroid/net/Uri;

    invoke-direct {p0, p1}, Lcom/android/camera/Camera;->isSameStorage(Landroid/net/Uri;)Z

    move-result v0

    return v0
.end method

.method static synthetic access$5500(Lcom/android/camera/Camera;)Lcom/android/camera/manager/ThumbnailManager;
    .locals 1
    .param p0    # Lcom/android/camera/Camera;

    iget-object v0, p0, Lcom/android/camera/Camera;->mThumbnailManager:Lcom/android/camera/manager/ThumbnailManager;

    return-object v0
.end method

.method static synthetic access$5600(Lcom/android/camera/Camera;)Lcom/android/camera/manager/IndicatorManager;
    .locals 1
    .param p0    # Lcom/android/camera/Camera;

    iget-object v0, p0, Lcom/android/camera/Camera;->mIndicatorManager:Lcom/android/camera/manager/IndicatorManager;

    return-object v0
.end method

.method static synthetic access$5700(Lcom/android/camera/Camera;)Lcom/android/camera/manager/PickerManager;
    .locals 1
    .param p0    # Lcom/android/camera/Camera;

    iget-object v0, p0, Lcom/android/camera/Camera;->mPickerManager:Lcom/android/camera/manager/PickerManager;

    return-object v0
.end method

.method static synthetic access$5800(Lcom/android/camera/Camera;)Lcom/android/camera/manager/InfoManager;
    .locals 1
    .param p0    # Lcom/android/camera/Camera;

    iget-object v0, p0, Lcom/android/camera/Camera;->mInfoManager:Lcom/android/camera/manager/InfoManager;

    return-object v0
.end method

.method static synthetic access$5900(Lcom/android/camera/Camera;I)V
    .locals 0
    .param p0    # Lcom/android/camera/Camera;
    .param p1    # I

    invoke-direct {p0, p1}, Lcom/android/camera/Camera;->showIndicator(I)V

    return-void
.end method

.method static synthetic access$600(Lcom/android/camera/Camera;)V
    .locals 0
    .param p0    # Lcom/android/camera/Camera;

    invoke-direct {p0}, Lcom/android/camera/Camera;->setDisplayOrientation()V

    return-void
.end method

.method static synthetic access$6000(Lcom/android/camera/Camera;)I
    .locals 1
    .param p0    # Lcom/android/camera/Camera;

    iget v0, p0, Lcom/android/camera/Camera;->mCurrentViewState:I

    return v0
.end method

.method static synthetic access$6100(Lcom/android/camera/Camera;)Z
    .locals 1
    .param p0    # Lcom/android/camera/Camera;

    invoke-direct {p0}, Lcom/android/camera/Camera;->isVoiceEnabled()Z

    move-result v0

    return v0
.end method

.method static synthetic access$6200(Lcom/android/camera/Camera;)Lcom/android/camera/manager/ShutterManager;
    .locals 1
    .param p0    # Lcom/android/camera/Camera;

    iget-object v0, p0, Lcom/android/camera/Camera;->mShutterManager:Lcom/android/camera/manager/ShutterManager;

    return-object v0
.end method

.method static synthetic access$700(Lcom/android/camera/Camera;)I
    .locals 1
    .param p0    # Lcom/android/camera/Camera;

    iget v0, p0, Lcom/android/camera/Camera;->mOrientation:I

    return v0
.end method

.method static synthetic access$702(Lcom/android/camera/Camera;I)I
    .locals 0
    .param p0    # Lcom/android/camera/Camera;
    .param p1    # I

    iput p1, p0, Lcom/android/camera/Camera;->mOrientation:I

    return p1
.end method

.method static synthetic access$800(Lcom/android/camera/Camera;)Lcom/android/camera/actor/CameraActor;
    .locals 1
    .param p0    # Lcom/android/camera/Camera;

    iget-object v0, p0, Lcom/android/camera/Camera;->mCameraActor:Lcom/android/camera/actor/CameraActor;

    return-object v0
.end method

.method static synthetic access$802(Lcom/android/camera/Camera;Lcom/android/camera/actor/CameraActor;)Lcom/android/camera/actor/CameraActor;
    .locals 0
    .param p0    # Lcom/android/camera/Camera;
    .param p1    # Lcom/android/camera/actor/CameraActor;

    iput-object p1, p0, Lcom/android/camera/Camera;->mCameraActor:Lcom/android/camera/actor/CameraActor;

    return-object p1
.end method

.method static synthetic access$900(Lcom/android/camera/Camera;)J
    .locals 2
    .param p0    # Lcom/android/camera/Camera;

    iget-wide v0, p0, Lcom/android/camera/Camera;->mOnResumeTime:J

    return-wide v0
.end method

.method static synthetic access$902(Lcom/android/camera/Camera;J)J
    .locals 0
    .param p0    # Lcom/android/camera/Camera;
    .param p1    # J

    iput-wide p1, p0, Lcom/android/camera/Camera;->mOnResumeTime:J

    return-wide p1
.end method

.method private addIdleHandler()V
    .locals 2

    invoke-static {}, Landroid/os/Looper;->myQueue()Landroid/os/MessageQueue;

    move-result-object v0

    new-instance v1, Lcom/android/camera/Camera$19;

    invoke-direct {v1, p0}, Lcom/android/camera/Camera$19;-><init>(Lcom/android/camera/Camera;)V

    invoke-virtual {v0, v1}, Landroid/os/MessageQueue;->addIdleHandler(Landroid/os/MessageQueue$IdleHandler;)V

    return-void
.end method

.method private applayViewCallbacks()V
    .locals 5

    iget-object v0, p0, Lcom/android/camera/Camera;->mShutterManager:Lcom/android/camera/manager/ShutterManager;

    iget-object v1, p0, Lcom/android/camera/Camera;->mPhotoShutterListener:Lcom/android/camera/ui/ShutterButton$OnShutterButtonListener;

    iget-object v2, p0, Lcom/android/camera/Camera;->mVideoShutterListener:Lcom/android/camera/ui/ShutterButton$OnShutterButtonListener;

    iget-object v3, p0, Lcom/android/camera/Camera;->mCameraActor:Lcom/android/camera/actor/CameraActor;

    invoke-virtual {v3}, Lcom/android/camera/actor/CameraActor;->getOkListener()Landroid/view/View$OnClickListener;

    move-result-object v3

    iget-object v4, p0, Lcom/android/camera/Camera;->mCameraActor:Lcom/android/camera/actor/CameraActor;

    invoke-virtual {v4}, Lcom/android/camera/actor/CameraActor;->getCancelListener()Landroid/view/View$OnClickListener;

    move-result-object v4

    invoke-virtual {v0, v1, v2, v3, v4}, Lcom/android/camera/manager/ShutterManager;->setShutterListener(Lcom/android/camera/ui/ShutterButton$OnShutterButtonListener;Lcom/android/camera/ui/ShutterButton$OnShutterButtonListener;Landroid/view/View$OnClickListener;Landroid/view/View$OnClickListener;)V

    return-void
.end method

.method private applyDeviceCallbacks()V
    .locals 2

    iget-object v0, p0, Lcom/android/camera/Camera;->mCameraDevice:Lcom/android/camera/CameraManager$CameraProxy;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/camera/Camera;->mCameraDevice:Lcom/android/camera/CameraManager$CameraProxy;

    iget-object v1, p0, Lcom/android/camera/Camera;->mCameraActor:Lcom/android/camera/actor/CameraActor;

    invoke-virtual {v1}, Lcom/android/camera/actor/CameraActor;->getASDCallback()Landroid/hardware/Camera$ASDCallback;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/android/camera/CameraManager$CameraProxy;->setASDCallback(Landroid/hardware/Camera$ASDCallback;)V

    iget-object v0, p0, Lcom/android/camera/Camera;->mCameraDevice:Lcom/android/camera/CameraManager$CameraProxy;

    iget-object v1, p0, Lcom/android/camera/Camera;->mCameraActor:Lcom/android/camera/actor/CameraActor;

    invoke-virtual {v1}, Lcom/android/camera/actor/CameraActor;->getContinuousShotDone()Landroid/hardware/Camera$ContinuousShotDone;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/android/camera/CameraManager$CameraProxy;->setCShotDoneCallback(Landroid/hardware/Camera$ContinuousShotDone;)V

    iget-object v0, p0, Lcom/android/camera/Camera;->mCameraDevice:Lcom/android/camera/CameraManager$CameraProxy;

    iget-object v1, p0, Lcom/android/camera/Camera;->mCameraActor:Lcom/android/camera/actor/CameraActor;

    invoke-virtual {v1}, Lcom/android/camera/actor/CameraActor;->getErrorCallback()Landroid/hardware/Camera$ErrorCallback;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/android/camera/CameraManager$CameraProxy;->setErrorCallback(Landroid/hardware/Camera$ErrorCallback;)V

    iget-object v0, p0, Lcom/android/camera/Camera;->mCameraDevice:Lcom/android/camera/CameraManager$CameraProxy;

    iget-object v1, p0, Lcom/android/camera/Camera;->mCameraActor:Lcom/android/camera/actor/CameraActor;

    invoke-virtual {v1}, Lcom/android/camera/actor/CameraActor;->getFaceDetectionListener()Landroid/hardware/Camera$FaceDetectionListener;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/android/camera/CameraManager$CameraProxy;->setFaceDetectionListener(Landroid/hardware/Camera$FaceDetectionListener;)V

    iget-object v0, p0, Lcom/android/camera/Camera;->mCameraDevice:Lcom/android/camera/CameraManager$CameraProxy;

    iget-object v1, p0, Lcom/android/camera/Camera;->mCameraActor:Lcom/android/camera/actor/CameraActor;

    invoke-virtual {v1}, Lcom/android/camera/actor/CameraActor;->getMAVCallback()Landroid/hardware/Camera$MAVCallback;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/android/camera/CameraManager$CameraProxy;->setMAVCallback(Landroid/hardware/Camera$MAVCallback;)V

    iget-object v0, p0, Lcom/android/camera/Camera;->mCameraDevice:Lcom/android/camera/CameraManager$CameraProxy;

    iget-object v1, p0, Lcom/android/camera/Camera;->mCameraActor:Lcom/android/camera/actor/CameraActor;

    invoke-virtual {v1}, Lcom/android/camera/actor/CameraActor;->getSmileCallback()Landroid/hardware/Camera$SmileCallback;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/android/camera/CameraManager$CameraProxy;->setSmileCallback(Landroid/hardware/Camera$SmileCallback;)V

    :cond_0
    return-void
.end method

.method private applyParameterForCapture(Lcom/android/camera/SaveRequest;)V
    .locals 1
    .param p1    # Lcom/android/camera/SaveRequest;

    new-instance v0, Lcom/android/camera/Camera$9;

    invoke-direct {v0, p0, p1}, Lcom/android/camera/Camera$9;-><init>(Lcom/android/camera/Camera;Lcom/android/camera/SaveRequest;)V

    invoke-virtual {p0, v0}, Lcom/android/camera/Camera;->lockRun(Ljava/lang/Runnable;)V

    return-void
.end method

.method private applyParameters(Z)V
    .locals 12
    .param p1    # Z

    invoke-direct {p0}, Lcom/android/camera/Camera;->cancelApplyParameters()Z

    move-result v9

    if-eqz v9, :cond_0

    :goto_0
    return-void

    :cond_0
    invoke-static {}, Lcom/android/camera/manager/MMProfileManager;->startProfileApplyParameters()V

    new-instance v9, Lcom/android/camera/Camera$2;

    invoke-direct {v9, p0}, Lcom/android/camera/Camera$2;-><init>(Lcom/android/camera/Camera;)V

    invoke-virtual {p0, v9}, Lcom/android/camera/Camera;->lockRun(Ljava/lang/Runnable;)V

    iget-object v9, p0, Lcom/android/camera/Camera;->mParameters:Landroid/hardware/Camera$Parameters;

    invoke-virtual {v9}, Landroid/hardware/Camera$Parameters;->getPictureSize()Landroid/hardware/Camera$Size;

    move-result-object v4

    iget-object v9, p0, Lcom/android/camera/Camera;->mParameters:Landroid/hardware/Camera$Parameters;

    invoke-virtual {v9}, Landroid/hardware/Camera$Parameters;->getPreviewSize()Landroid/hardware/Camera$Size;

    move-result-object v5

    iget-object v9, p0, Lcom/android/camera/Camera;->mParameters:Landroid/hardware/Camera$Parameters;

    invoke-virtual {v9}, Landroid/hardware/Camera$Parameters;->getZSDMode()Ljava/lang/String;

    move-result-object v6

    iget-object v9, p0, Lcom/android/camera/Camera;->mLastPreviewSize:Landroid/hardware/Camera$Size;

    invoke-virtual {v5, v9}, Landroid/hardware/Camera$Size;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-nez v9, :cond_3

    const/4 v2, 0x1

    :goto_1
    iget-object v9, p0, Lcom/android/camera/Camera;->mLastPictureSize:Landroid/hardware/Camera$Size;

    invoke-virtual {v4, v9}, Landroid/hardware/Camera$Size;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-nez v9, :cond_4

    const/4 v1, 0x1

    :goto_2
    if-nez v6, :cond_6

    iget-object v9, p0, Lcom/android/camera/Camera;->mLastZsdMode:Ljava/lang/String;

    if-eqz v9, :cond_5

    const/4 v3, 0x1

    :goto_3
    if-nez v3, :cond_1

    if-nez v2, :cond_1

    if-eqz p1, :cond_8

    :cond_1
    const/4 v7, 0x1

    :goto_4
    const/4 v8, 0x0

    const/4 v0, 0x0

    iget-object v9, p0, Lcom/android/camera/Camera;->mProfile:Landroid/media/CamcorderProfile;

    if-eqz v9, :cond_2

    iget v9, p0, Lcom/android/camera/Camera;->mLastVideoBitRate:I

    const/4 v10, -0x1

    if-ne v9, v10, :cond_9

    const/4 v8, 0x1

    :goto_5
    iget v9, p0, Lcom/android/camera/Camera;->mLastAudioBitRate:I

    const/4 v10, -0x1

    if-ne v9, v10, :cond_b

    const/4 v0, 0x1

    :cond_2
    :goto_6
    invoke-direct {p0}, Lcom/android/camera/Camera;->cancelApplyParameters()Z

    move-result v9

    if-eqz v9, :cond_d

    invoke-static {}, Lcom/android/camera/manager/MMProfileManager;->stopProfileApplyParameters()V

    goto :goto_0

    :cond_3
    const/4 v2, 0x0

    goto :goto_1

    :cond_4
    const/4 v1, 0x0

    goto :goto_2

    :cond_5
    const/4 v3, 0x0

    goto :goto_3

    :cond_6
    iget-object v9, p0, Lcom/android/camera/Camera;->mLastZsdMode:Ljava/lang/String;

    invoke-virtual {v6, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-nez v9, :cond_7

    const/4 v3, 0x1

    goto :goto_3

    :cond_7
    const/4 v3, 0x0

    goto :goto_3

    :cond_8
    const/4 v7, 0x0

    goto :goto_4

    :cond_9
    iget v9, p0, Lcom/android/camera/Camera;->mLastVideoBitRate:I

    iget-object v10, p0, Lcom/android/camera/Camera;->mProfile:Landroid/media/CamcorderProfile;

    iget v10, v10, Landroid/media/CamcorderProfile;->videoBitRate:I

    if-eq v9, v10, :cond_a

    const/4 v8, 0x1

    goto :goto_5

    :cond_a
    const/4 v8, 0x0

    goto :goto_5

    :cond_b
    iget v9, p0, Lcom/android/camera/Camera;->mLastAudioBitRate:I

    iget-object v10, p0, Lcom/android/camera/Camera;->mProfile:Landroid/media/CamcorderProfile;

    iget v10, v10, Landroid/media/CamcorderProfile;->audioBitRate:I

    if-eq v9, v10, :cond_c

    const/4 v0, 0x1

    goto :goto_6

    :cond_c
    const/4 v0, 0x0

    goto :goto_6

    :cond_d
    iget-object v9, p0, Lcom/android/camera/Camera;->mCameraDevice:Lcom/android/camera/CameraManager$CameraProxy;

    iget v10, p0, Lcom/android/camera/Camera;->mCameraDisplayOrientation:I

    invoke-virtual {v9, v10}, Lcom/android/camera/CameraManager$CameraProxy;->setDisplayOrientation(I)V

    if-eqz v7, :cond_e

    iget-object v9, p0, Lcom/android/camera/Camera;->mCameraActor:Lcom/android/camera/actor/CameraActor;

    invoke-virtual {v9}, Lcom/android/camera/actor/CameraActor;->stopPreview()V

    invoke-virtual {p0}, Lcom/android/camera/Camera;->isSwitchingCamera()Z

    move-result v9

    if-nez v9, :cond_f

    if-eqz v2, :cond_f

    iget-object v9, p0, Lcom/android/camera/Camera;->mSurfaceTexture:Landroid/graphics/SurfaceTexture;

    if-eqz v9, :cond_f

    invoke-virtual {p0}, Lcom/android/camera/Camera;->isVideoWallPaperIntent()Z

    move-result v9

    if-nez v9, :cond_f

    iget-object v9, p0, Lcom/android/camera/ActivityBase;->mCameraScreenNail:Lcom/android/camera/CameraScreenNail;

    invoke-virtual {v9}, Lcom/android/camera/CameraScreenNail;->copyOriginSizeTexture()V

    :goto_7
    iget-object v9, p0, Lcom/android/camera/ActivityBase;->mCameraScreenNail:Lcom/android/camera/CameraScreenNail;

    const/4 v10, 0x0

    invoke-virtual {v9, v10}, Lcom/android/camera/CameraScreenNail;->setDrawable(Z)V

    invoke-direct {p0}, Lcom/android/camera/Camera;->updateSurfaceTexture()V

    invoke-virtual {p0}, Lcom/android/camera/Camera;->setPreviewTextureAsync()V

    :cond_e
    invoke-direct {p0}, Lcom/android/camera/Camera;->cancelApplyParameters()Z

    move-result v9

    if-eqz v9, :cond_10

    invoke-static {}, Lcom/android/camera/manager/MMProfileManager;->stopProfileApplyParameters()V

    goto/16 :goto_0

    :cond_f
    iget-object v9, p0, Lcom/android/camera/ActivityBase;->mCameraScreenNail:Lcom/android/camera/CameraScreenNail;

    invoke-virtual {v9}, Lcom/android/camera/CameraScreenNail;->stopSwitchActorAnimation()V

    goto :goto_7

    :cond_10
    new-instance v9, Lcom/android/camera/Camera$3;

    invoke-direct {v9, p0, v2}, Lcom/android/camera/Camera$3;-><init>(Lcom/android/camera/Camera;Z)V

    invoke-virtual {p0, v9}, Lcom/android/camera/Camera;->lockRun(Ljava/lang/Runnable;)V

    invoke-direct {p0}, Lcom/android/camera/Camera;->cancelApplyParameters()Z

    move-result v9

    if-eqz v9, :cond_11

    invoke-static {}, Lcom/android/camera/manager/MMProfileManager;->stopProfileApplyParameters()V

    goto/16 :goto_0

    :cond_11
    iget-object v9, p0, Lcom/android/camera/Camera;->mMainHandler:Landroid/os/Handler;

    const/4 v10, 0x2

    invoke-virtual {v9, v10}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    sget-boolean v9, Lcom/android/camera/Camera;->LOG:Z

    if-eqz v9, :cond_12

    const/4 v9, 0x2

    invoke-static {v9}, Lcom/android/camera/Camera;->getMsgLabel(I)Ljava/lang/String;

    move-result-object v9

    invoke-static {v9}, Lcom/android/camera/manager/MMProfileManager;->triggersSendMessage(Ljava/lang/String;)V

    :cond_12
    if-nez v1, :cond_13

    if-nez v2, :cond_13

    if-nez p1, :cond_13

    if-nez v8, :cond_13

    if-eqz v0, :cond_14

    :cond_13
    invoke-direct {p0}, Lcom/android/camera/Camera;->showRemainingAways()V

    :cond_14
    iget-object v9, p0, Lcom/android/camera/Camera;->mCameraActor:Lcom/android/camera/actor/CameraActor;

    invoke-virtual {v9, v7}, Lcom/android/camera/actor/CameraActor;->onCameraParameterReady(Z)V

    sget-boolean v9, Lcom/android/camera/Camera;->LOG:Z

    if-eqz v9, :cond_15

    const-string v9, "Camera"

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string v11, "applyParameters("

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v10

    const-string v11, ") picturesize="

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-static {v4}, Lcom/android/camera/SettingUtils;->buildSize(Landroid/hardware/Camera$Size;)Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    const-string v11, " previewsize="

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-static {v5}, Lcom/android/camera/SettingUtils;->buildSize(Landroid/hardware/Camera$Size;)Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    const-string v11, " oldPictureSize="

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    iget-object v11, p0, Lcom/android/camera/Camera;->mLastPictureSize:Landroid/hardware/Camera$Size;

    invoke-static {v11}, Lcom/android/camera/SettingUtils;->buildSize(Landroid/hardware/Camera$Size;)Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    const-string v11, " oldPreviewSize="

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    iget-object v11, p0, Lcom/android/camera/Camera;->mLastPreviewSize:Landroid/hardware/Camera$Size;

    invoke-static {v11}, Lcom/android/camera/SettingUtils;->buildSize(Landroid/hardware/Camera$Size;)Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    const-string v11, " changedPreviewSize="

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v10

    const-string v11, ", changedPictureSize="

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v10

    const-string v11, " oldZsd="

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    iget-object v11, p0, Lcom/android/camera/Camera;->mLastZsdMode:Ljava/lang/String;

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    const-string v11, ", curZsd="

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    const-string v11, ", changedZsd="

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10, v3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v10

    const-string v11, " vBRateChanged="

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10, v8}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v10

    const-string v11, ", aBRateChanged="

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-static {v9, v10}, Lcom/android/camera/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    :cond_15
    iput-object v4, p0, Lcom/android/camera/Camera;->mLastPictureSize:Landroid/hardware/Camera$Size;

    iput-object v5, p0, Lcom/android/camera/Camera;->mLastPreviewSize:Landroid/hardware/Camera$Size;

    iput-object v6, p0, Lcom/android/camera/Camera;->mLastZsdMode:Ljava/lang/String;

    iget-object v9, p0, Lcom/android/camera/Camera;->mProfile:Landroid/media/CamcorderProfile;

    if-eqz v9, :cond_16

    iget-object v9, p0, Lcom/android/camera/Camera;->mProfile:Landroid/media/CamcorderProfile;

    iget v9, v9, Landroid/media/CamcorderProfile;->videoBitRate:I

    iput v9, p0, Lcom/android/camera/Camera;->mLastVideoBitRate:I

    iget-object v9, p0, Lcom/android/camera/Camera;->mProfile:Landroid/media/CamcorderProfile;

    iget v9, v9, Landroid/media/CamcorderProfile;->audioBitRate:I

    iput v9, p0, Lcom/android/camera/Camera;->mLastAudioBitRate:I

    :cond_16
    invoke-static {}, Lcom/android/camera/manager/MMProfileManager;->stopProfileApplyParameters()V

    goto/16 :goto_0
.end method

.method private callResumableBegin()V
    .locals 3

    iget-object v2, p0, Lcom/android/camera/Camera;->mResumables:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/android/camera/Camera$Resumable;

    invoke-interface {v1}, Lcom/android/camera/Camera$Resumable;->begin()V

    goto :goto_0

    :cond_0
    return-void
.end method

.method private callResumableFinish()V
    .locals 3

    iget-object v2, p0, Lcom/android/camera/Camera;->mResumables:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/android/camera/Camera$Resumable;

    invoke-interface {v1}, Lcom/android/camera/Camera$Resumable;->finish()V

    goto :goto_0

    :cond_0
    return-void
.end method

.method private callResumablePause()V
    .locals 3

    iget-object v2, p0, Lcom/android/camera/Camera;->mResumables:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/android/camera/Camera$Resumable;

    invoke-interface {v1}, Lcom/android/camera/Camera$Resumable;->pause()V

    goto :goto_0

    :cond_0
    return-void
.end method

.method private callResumableResume()V
    .locals 3

    iget-object v2, p0, Lcom/android/camera/Camera;->mResumables:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/android/camera/Camera$Resumable;

    invoke-interface {v1}, Lcom/android/camera/Camera$Resumable;->resume()V

    goto :goto_0

    :cond_0
    return-void
.end method

.method private cancelApplyParameters()Z
    .locals 4

    iget-object v1, p0, Lcom/android/camera/Camera;->mCameraDevice:Lcom/android/camera/CameraManager$CameraProxy;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/android/camera/Camera;->mParameters:Landroid/hardware/Camera$Parameters;

    if-eqz v1, :cond_0

    iget-boolean v1, p0, Lcom/android/camera/ActivityBase;->mPaused:Z

    if-nez v1, :cond_0

    iget-object v1, p0, Lcom/android/camera/Camera;->mCameraStartUpThread:Lcom/android/camera/Camera$CameraStartUpThread;

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/android/camera/Camera;->mCameraStartUpThread:Lcom/android/camera/Camera$CameraStartUpThread;

    invoke-virtual {v1}, Lcom/android/camera/Camera$CameraStartUpThread;->isCanceled()Z

    move-result v1

    if-eqz v1, :cond_2

    :cond_0
    const/4 v0, 0x1

    :goto_0
    sget-boolean v1, Lcom/android/camera/Camera;->LOG:Z

    if-eqz v1, :cond_1

    if-eqz v0, :cond_1

    const-string v1, "Camera"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "cancelApplyParameters() mCameraDevice="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/android/camera/Camera;->mCameraDevice:Lcom/android/camera/CameraManager$CameraProxy;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ", "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "mParameters="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/android/camera/Camera;->mParameters:Landroid/hardware/Camera$Parameters;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ", mPaused="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-boolean v3, p0, Lcom/android/camera/ActivityBase;->mPaused:Z

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " return "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/android/camera/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    :cond_1
    return v0

    :cond_2
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private checkViewManagerConfiguration()V
    .locals 3

    iget-object v2, p0, Lcom/android/camera/Camera;->mViewManagers:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/android/camera/manager/ViewManager;

    invoke-virtual {v1}, Lcom/android/camera/manager/ViewManager;->checkConfiguration()V

    goto :goto_0

    :cond_0
    return-void
.end method

.method private clearDeviceCallbacks()V
    .locals 2

    const/4 v1, 0x0

    iget-object v0, p0, Lcom/android/camera/Camera;->mCameraDevice:Lcom/android/camera/CameraManager$CameraProxy;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/camera/Camera;->mCameraDevice:Lcom/android/camera/CameraManager$CameraProxy;

    invoke-virtual {v0, v1}, Lcom/android/camera/CameraManager$CameraProxy;->setASDCallback(Landroid/hardware/Camera$ASDCallback;)V

    iget-object v0, p0, Lcom/android/camera/Camera;->mCameraDevice:Lcom/android/camera/CameraManager$CameraProxy;

    invoke-virtual {v0, v1}, Lcom/android/camera/CameraManager$CameraProxy;->setAutoFocusMoveCallback(Landroid/hardware/Camera$AutoFocusMoveCallback;)V

    iget-object v0, p0, Lcom/android/camera/Camera;->mCameraDevice:Lcom/android/camera/CameraManager$CameraProxy;

    invoke-virtual {v0, v1}, Lcom/android/camera/CameraManager$CameraProxy;->setCShotDoneCallback(Landroid/hardware/Camera$ContinuousShotDone;)V

    iget-object v0, p0, Lcom/android/camera/Camera;->mCameraDevice:Lcom/android/camera/CameraManager$CameraProxy;

    invoke-virtual {v0, v1}, Lcom/android/camera/CameraManager$CameraProxy;->setErrorCallback(Landroid/hardware/Camera$ErrorCallback;)V

    iget-object v0, p0, Lcom/android/camera/Camera;->mCameraDevice:Lcom/android/camera/CameraManager$CameraProxy;

    invoke-virtual {v0, v1}, Lcom/android/camera/CameraManager$CameraProxy;->setFaceDetectionListener(Landroid/hardware/Camera$FaceDetectionListener;)V

    iget-object v0, p0, Lcom/android/camera/Camera;->mCameraDevice:Lcom/android/camera/CameraManager$CameraProxy;

    invoke-virtual {v0, v1}, Lcom/android/camera/CameraManager$CameraProxy;->setMAVCallback(Landroid/hardware/Camera$MAVCallback;)V

    iget-object v0, p0, Lcom/android/camera/Camera;->mCameraDevice:Lcom/android/camera/CameraManager$CameraProxy;

    invoke-virtual {v0, v1}, Lcom/android/camera/CameraManager$CameraProxy;->setPreviewDoneCallback(Landroid/hardware/Camera$ZSDPreviewDone;)V

    iget-object v0, p0, Lcom/android/camera/Camera;->mCameraDevice:Lcom/android/camera/CameraManager$CameraProxy;

    invoke-virtual {v0, v1}, Lcom/android/camera/CameraManager$CameraProxy;->setSmileCallback(Landroid/hardware/Camera$SmileCallback;)V

    iget-object v0, p0, Lcom/android/camera/Camera;->mCameraDevice:Lcom/android/camera/CameraManager$CameraProxy;

    invoke-virtual {v0, v1}, Lcom/android/camera/CameraManager$CameraProxy;->setZoomChangeListener(Landroid/hardware/Camera$OnZoomChangeListener;)V

    :cond_0
    return-void
.end method

.method private clearFocusAndFace()V
    .locals 1

    iget-object v0, p0, Lcom/android/camera/Camera;->mFaceView:Lcom/android/camera/ui/FaceView;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/camera/Camera;->mFaceView:Lcom/android/camera/ui/FaceView;

    invoke-virtual {v0}, Lcom/android/camera/ui/FaceView;->clear()V

    :cond_0
    iget-object v0, p0, Lcom/android/camera/Camera;->mFocusManager:Lcom/android/camera/FocusManager;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/android/camera/Camera;->mFocusManager:Lcom/android/camera/FocusManager;

    invoke-virtual {v0}, Lcom/android/camera/FocusManager;->removeMessages()V

    :cond_1
    return-void
.end method

.method private clearUserSettings()V
    .locals 3

    sget-boolean v0, Lcom/android/camera/Camera;->LOG:Z

    if-eqz v0, :cond_0

    const-string v0, "Camera"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "clearUserSettings() isFinishing()="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Landroid/app/Activity;->isFinishing()Z

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/android/camera/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    iget-object v0, p0, Lcom/android/camera/Camera;->mSettingChecker:Lcom/android/camera/SettingChecker;

    if-eqz v0, :cond_1

    invoke-virtual {p0}, Landroid/app/Activity;->isFinishing()Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/android/camera/Camera;->mSettingChecker:Lcom/android/camera/SettingChecker;

    invoke-virtual {v0}, Lcom/android/camera/SettingChecker;->resetSettings()V

    :cond_1
    return-void
.end method

.method private clearViewCallbacks()V
    .locals 2

    const/4 v1, 0x0

    iget-object v0, p0, Lcom/android/camera/Camera;->mShutterManager:Lcom/android/camera/manager/ShutterManager;

    invoke-virtual {v0, v1, v1, v1, v1}, Lcom/android/camera/manager/ShutterManager;->setShutterListener(Lcom/android/camera/ui/ShutterButton$OnShutterButtonListener;Lcom/android/camera/ui/ShutterButton$OnShutterButtonListener;Landroid/view/View$OnClickListener;Landroid/view/View$OnClickListener;)V

    return-void
.end method

.method private closeCamera()V
    .locals 4

    const/4 v3, 0x0

    sget-boolean v0, Lcom/android/camera/Camera;->LOG:Z

    if-eqz v0, :cond_0

    const-string v0, "Camera"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "closeCamera() mCameraDevice="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/android/camera/Camera;->mCameraDevice:Lcom/android/camera/CameraManager$CameraProxy;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/android/camera/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    iget-object v0, p0, Lcom/android/camera/Camera;->mCameraDevice:Lcom/android/camera/CameraManager$CameraProxy;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/android/camera/Camera;->mCameraActor:Lcom/android/camera/actor/CameraActor;

    invoke-virtual {v0}, Lcom/android/camera/actor/CameraActor;->onCameraClose()V

    invoke-direct {p0}, Lcom/android/camera/Camera;->clearDeviceCallbacks()V

    invoke-static {}, Lcom/android/camera/CameraHolder;->instance()Lcom/android/camera/CameraHolder;

    move-result-object v0

    invoke-virtual {v0}, Lcom/android/camera/CameraHolder;->release()V

    iget-object v0, p0, Lcom/android/camera/Camera;->mFocusManager:Lcom/android/camera/FocusManager;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/android/camera/Camera;->mFocusManager:Lcom/android/camera/FocusManager;

    invoke-virtual {v0}, Lcom/android/camera/FocusManager;->onCameraReleased()V

    :cond_1
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/android/camera/Camera;->mCameraDevice:Lcom/android/camera/CameraManager$CameraProxy;

    invoke-virtual {p0, v3}, Lcom/android/camera/Camera;->setCameraState(I)V

    iput-boolean v3, p0, Lcom/android/camera/Camera;->mCameraOpened:Z

    :cond_2
    return-void
.end method

.method private doOnFirstFrameArrived()V
    .locals 0

    return-void
.end method

.method private doOnResume()V
    .locals 8

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    iget-object v4, p0, Lcom/android/camera/Camera;->mOrientationListener:Lcom/android/camera/Camera$MyOrientationEventListener;

    invoke-virtual {v4}, Landroid/view/OrientationEventListener;->enable()V

    invoke-virtual {p0}, Lcom/android/camera/Camera;->isNonePickIntent()Z

    move-result v4

    if-nez v4, :cond_0

    invoke-virtual {p0}, Lcom/android/camera/Camera;->isImageCaptureIntent()Z

    move-result v4

    if-eqz v4, :cond_1

    :cond_0
    iget-object v4, p0, Lcom/android/camera/Camera;->mVoiceManager:Lcom/android/camera/VoiceManager;

    invoke-virtual {v4}, Lcom/android/camera/VoiceManager;->startUpdateVoiceState()V

    :cond_1
    invoke-direct {p0}, Lcom/android/camera/Camera;->installIntentFilter()V

    invoke-direct {p0}, Lcom/android/camera/Camera;->callResumableResume()V

    invoke-direct {p0}, Lcom/android/camera/Camera;->checkViewManagerConfiguration()V

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    sget-boolean v4, Lcom/android/camera/Camera;->LOG:Z

    if-eqz v4, :cond_2

    const-string v4, "Camera"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "doOnResume() consume:"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    sub-long v6, v2, v0

    invoke-virtual {v5, v6, v7}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/android/camera/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    :cond_2
    return-void
.end method

.method private doShowIndicator()V
    .locals 2

    sget-boolean v0, Lcom/android/camera/Camera;->LOG:Z

    if-eqz v0, :cond_0

    const-string v0, "Camera"

    const-string v1, "doShowIndicator()"

    invoke-static {v0, v1}, Lcom/android/camera/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    new-instance v0, Lcom/android/camera/Camera$18;

    invoke-direct {v0, p0}, Lcom/android/camera/Camera$18;-><init>(Lcom/android/camera/Camera;)V

    invoke-virtual {p0, v0}, Landroid/app/Activity;->runOnUiThread(Ljava/lang/Runnable;)V

    return-void
.end method

.method private doShowInfo(Ljava/lang/String;I)V
    .locals 3
    .param p1    # Ljava/lang/String;
    .param p2    # I

    sget-boolean v0, Lcom/android/camera/Camera;->LOG:Z

    if-eqz v0, :cond_0

    const-string v0, "Camera"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "doShowInfo("

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ")"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/android/camera/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    new-instance v0, Lcom/android/camera/Camera$16;

    invoke-direct {v0, p0, p1, p2}, Lcom/android/camera/Camera$16;-><init>(Lcom/android/camera/Camera;Ljava/lang/String;I)V

    invoke-virtual {p0, v0}, Landroid/app/Activity;->runOnUiThread(Ljava/lang/Runnable;)V

    return-void
.end method

.method private doShowRemaining(Z)V
    .locals 3
    .param p1    # Z

    sget-boolean v0, Lcom/android/camera/Camera;->LOG:Z

    if-eqz v0, :cond_0

    const-string v0, "Camera"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "doShowRemaining("

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ")"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/android/camera/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    new-instance v0, Lcom/android/camera/Camera$17;

    invoke-direct {v0, p0, p1}, Lcom/android/camera/Camera$17;-><init>(Lcom/android/camera/Camera;Z)V

    invoke-virtual {p0, v0}, Landroid/app/Activity;->runOnUiThread(Ljava/lang/Runnable;)V

    return-void
.end method

.method private static getMsgLabel(I)Ljava/lang/String;
    .locals 1
    .param p0    # I

    packed-switch p0, :pswitch_data_0

    :pswitch_0
    const-string v0, "unknown message"

    :goto_0
    return-object v0

    :pswitch_1
    const-string v0, "MSG_CAMERA_OPEN_DONE"

    goto :goto_0

    :pswitch_2
    const-string v0, "MSG_CAMERA_PARAMETERS_READY"

    goto :goto_0

    :pswitch_3
    const-string v0, "MSG_CAMERA_PREFERENCE_READY"

    goto :goto_0

    :pswitch_4
    const-string v0, "MSG_CHECK_DISPLAY_ROTATION"

    goto :goto_0

    :pswitch_5
    const-string v0, "MSG_SWITCH_CAMERA"

    goto :goto_0

    :pswitch_6
    const-string v0, "MSG_SWITCH_CAMERA_START_ANIMATION"

    goto :goto_0

    :pswitch_7
    const-string v0, "MSG_CLEAR_SCREEN_DELAY"

    goto :goto_0

    :pswitch_8
    const-string v0, "MSG_SHOW_ONSCREEN_INDICATOR"

    goto :goto_0

    :pswitch_9
    const-string v0, "MSG_OPEN_CAMERA_FAIL"

    goto :goto_0

    :pswitch_a
    const-string v0, "MSG_OPEN_CAMERA_DISABLED"

    goto :goto_0

    :pswitch_b
    const-string v0, "MSG_APPLY_PARAMETERS_WHEN_IDEL"

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_7
        :pswitch_8
        :pswitch_9
        :pswitch_a
        :pswitch_0
        :pswitch_b
    .end packed-switch
.end method

.method private getPreferredCameraId(Lcom/android/camera/ComboPreferences;)I
    .locals 2
    .param p1    # Lcom/android/camera/ComboPreferences;

    invoke-static {p0}, Lcom/android/camera/Util;->getCameraFacingIntentExtras(Landroid/app/Activity;)I

    move-result v0

    const/4 v1, -0x1

    if-eq v0, v1, :cond_0

    :goto_0
    return v0

    :cond_0
    invoke-static {p1}, Lcom/android/camera/CameraSettings;->readPreferredCameraId(Landroid/content/SharedPreferences;)I

    move-result v0

    goto :goto_0
.end method

.method private getViewLayer(I)Landroid/view/ViewGroup;
    .locals 4
    .param p1    # I

    const/4 v0, 0x0

    packed-switch p1, :pswitch_data_0

    new-instance v1, Ljava/lang/RuntimeException;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Wrong layer:"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v1

    :pswitch_0
    iget-object v0, p0, Lcom/android/camera/Camera;->mViewLayerBottom:Landroid/view/ViewGroup;

    :goto_0
    sget-boolean v1, Lcom/android/camera/Camera;->LOG:Z

    if-eqz v1, :cond_0

    const-string v1, "Camera"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "getViewLayer("

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ") return "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/android/camera/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    return-object v0

    :pswitch_1
    iget-object v0, p0, Lcom/android/camera/Camera;->mViewLayerNormal:Landroid/view/ViewGroup;

    goto :goto_0

    :pswitch_2
    iget-object v0, p0, Lcom/android/camera/Camera;->mViewLayerTop:Landroid/view/ViewGroup;

    goto :goto_0

    :pswitch_3
    iget-object v0, p0, Lcom/android/camera/Camera;->mViewLayerShutter:Landroid/view/ViewGroup;

    goto :goto_0

    :pswitch_4
    iget-object v0, p0, Lcom/android/camera/Camera;->mViewLayerSetting:Landroid/view/ViewGroup;

    goto :goto_0

    :pswitch_5
    iget-object v0, p0, Lcom/android/camera/Camera;->mViewLayerOverlay:Landroid/view/ViewGroup;

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch -0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
    .end packed-switch
.end method

.method private hideActorViews()V
    .locals 6

    iget-object v3, p0, Lcom/android/camera/Camera;->mViewManagers:Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v1

    new-array v3, v1, [Z

    iput-object v3, p0, Lcom/android/camera/Camera;->mLastVisibles:[Z

    new-array v3, v1, [Lcom/android/camera/manager/ViewManager;

    iput-object v3, p0, Lcom/android/camera/Camera;->mLastManagers:[Lcom/android/camera/manager/ViewManager;

    iget v3, p0, Lcom/android/camera/Camera;->mCommonManagerCount:I

    add-int/lit8 v0, v3, -0x1

    :goto_0
    if-ge v0, v1, :cond_1

    iget-object v3, p0, Lcom/android/camera/Camera;->mViewManagers:Ljava/util/List;

    invoke-interface {v3, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/android/camera/manager/ViewManager;

    if-eqz v2, :cond_0

    iget-object v3, p0, Lcom/android/camera/Camera;->mLastManagers:[Lcom/android/camera/manager/ViewManager;

    aput-object v2, v3, v0

    iget-object v3, p0, Lcom/android/camera/Camera;->mLastVisibles:[Z

    invoke-virtual {v2}, Lcom/android/camera/manager/ViewManager;->isShowing()Z

    move-result v4

    aput-boolean v4, v3, v0

    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_1
    sget-boolean v3, Lcom/android/camera/Camera;->LOG:Z

    if-eqz v3, :cond_2

    const-string v3, "Camera"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "hideActorViews() size="

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ", mCommonManagerCount="

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget v5, p0, Lcom/android/camera/Camera;->mCommonManagerCount:I

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/android/camera/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    :cond_2
    return-void
.end method

.method private initializeAfterPreview()V
    .locals 8

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    invoke-direct {p0}, Lcom/android/camera/Camera;->callResumableBegin()V

    iget-object v4, p0, Lcom/android/camera/Camera;->mModePicker:Lcom/android/camera/manager/ModePicker;

    iget-object v5, p0, Lcom/android/camera/Camera;->mCameraActor:Lcom/android/camera/actor/CameraActor;

    invoke-virtual {v5}, Lcom/android/camera/actor/CameraActor;->getMode()I

    move-result v5

    invoke-virtual {v4, v5}, Lcom/android/camera/manager/ModePicker;->setCurrentMode(I)V

    iget-object v4, p0, Lcom/android/camera/Camera;->mShutterManager:Lcom/android/camera/manager/ShutterManager;

    invoke-virtual {v4}, Lcom/android/camera/manager/ViewManager;->show()V

    iget-object v4, p0, Lcom/android/camera/Camera;->mSettingManager:Lcom/android/camera/manager/SettingManager;

    invoke-virtual {v4}, Lcom/android/camera/manager/ViewManager;->show()V

    invoke-virtual {p0}, Lcom/android/camera/Camera;->isNonePickIntent()Z

    move-result v4

    if-eqz v4, :cond_0

    iget-object v4, p0, Lcom/android/camera/Camera;->mThumbnailManager:Lcom/android/camera/manager/ThumbnailManager;

    invoke-virtual {v4}, Lcom/android/camera/manager/ViewManager;->show()V

    :cond_0
    invoke-direct {p0}, Lcom/android/camera/Camera;->addIdleHandler()V

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    const-string v4, "Camera"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "initializeAfterPreview() consume:"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    sub-long v6, v2, v0

    invoke-virtual {v5, v6, v7}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/android/camera/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    return-void
.end method

.method private initializeCameraPreferences()V
    .locals 5

    const/4 v4, 0x3

    sget-boolean v0, Lcom/android/camera/Camera;->LOG:Z

    if-eqz v0, :cond_0

    const-string v0, "Camera"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "initializeCameraPreferences() mPreferenceGroup="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/android/camera/Camera;->mPreferenceGroup:Lcom/android/camera/PreferenceGroup;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/android/camera/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    invoke-static {}, Lcom/android/camera/manager/MMProfileManager;->startProfileInitPref()V

    iget-object v0, p0, Lcom/android/camera/Camera;->mPreferenceGroup:Lcom/android/camera/PreferenceGroup;

    if-nez v0, :cond_1

    new-instance v0, Lcom/android/camera/CameraSettings;

    iget-object v1, p0, Lcom/android/camera/Camera;->mInitialParams:Landroid/hardware/Camera$Parameters;

    iget v2, p0, Lcom/android/camera/Camera;->mCameraId:I

    invoke-static {}, Lcom/android/camera/CameraHolder;->instance()Lcom/android/camera/CameraHolder;

    move-result-object v3

    invoke-virtual {v3}, Lcom/android/camera/CameraHolder;->getCameraInfo()[Landroid/hardware/Camera$CameraInfo;

    move-result-object v3

    invoke-direct {v0, p0, v1, v2, v3}, Lcom/android/camera/CameraSettings;-><init>(Lcom/android/camera/Camera;Landroid/hardware/Camera$Parameters;I[Landroid/hardware/Camera$CameraInfo;)V

    iput-object v0, p0, Lcom/android/camera/Camera;->mCameraSettings:Lcom/android/camera/CameraSettings;

    iget-object v0, p0, Lcom/android/camera/Camera;->mCameraSettings:Lcom/android/camera/CameraSettings;

    iget-object v1, p0, Lcom/android/camera/Camera;->mCameraSettings:Lcom/android/camera/CameraSettings;

    const/high16 v2, 0x7f060000

    invoke-virtual {v1, v2}, Lcom/android/camera/CameraSettings;->getPreferenceGroup(I)Lcom/android/camera/PreferenceGroup;

    move-result-object v1

    iget v2, p0, Lcom/android/camera/Camera;->mCameraId:I

    invoke-static {v0, v1, v2}, Lcom/android/camera/SettingChecker;->filterUnsuportedPreference(Lcom/android/camera/CameraSettings;Lcom/android/camera/PreferenceGroup;I)Lcom/android/camera/PreferenceGroup;

    move-result-object v0

    iput-object v0, p0, Lcom/android/camera/Camera;->mPreferenceGroup:Lcom/android/camera/PreferenceGroup;

    invoke-direct {p0}, Lcom/android/camera/Camera;->limitSettingsByIntent()V

    :cond_1
    iget-object v0, p0, Lcom/android/camera/Camera;->mMainHandler:Landroid/os/Handler;

    invoke-virtual {v0, v4}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    sget-boolean v0, Lcom/android/camera/Camera;->LOG:Z

    if-eqz v0, :cond_2

    invoke-static {v4}, Lcom/android/camera/Camera;->getMsgLabel(I)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/android/camera/manager/MMProfileManager;->triggersSendMessage(Ljava/lang/String;)V

    :cond_2
    invoke-static {}, Lcom/android/camera/manager/MMProfileManager;->stopProfileInitPref()V

    return-void
.end method

.method private initializeCommonManagers()V
    .locals 2

    new-instance v0, Lcom/android/camera/SettingChecker;

    invoke-direct {v0, p0}, Lcom/android/camera/SettingChecker;-><init>(Lcom/android/camera/Camera;)V

    iput-object v0, p0, Lcom/android/camera/Camera;->mSettingChecker:Lcom/android/camera/SettingChecker;

    new-instance v0, Lcom/android/camera/manager/ReviewManager;

    invoke-direct {v0, p0}, Lcom/android/camera/manager/ReviewManager;-><init>(Lcom/android/camera/Camera;)V

    iput-object v0, p0, Lcom/android/camera/Camera;->mReviewManager:Lcom/android/camera/manager/ReviewManager;

    new-instance v0, Lcom/android/camera/manager/ShutterManager;

    invoke-direct {v0, p0}, Lcom/android/camera/manager/ShutterManager;-><init>(Lcom/android/camera/Camera;)V

    iput-object v0, p0, Lcom/android/camera/Camera;->mShutterManager:Lcom/android/camera/manager/ShutterManager;

    new-instance v0, Lcom/android/camera/manager/ModePicker;

    invoke-direct {v0, p0}, Lcom/android/camera/manager/ModePicker;-><init>(Lcom/android/camera/Camera;)V

    iput-object v0, p0, Lcom/android/camera/Camera;->mModePicker:Lcom/android/camera/manager/ModePicker;

    new-instance v0, Lcom/android/camera/manager/SettingManager;

    invoke-direct {v0, p0}, Lcom/android/camera/manager/SettingManager;-><init>(Lcom/android/camera/Camera;)V

    iput-object v0, p0, Lcom/android/camera/Camera;->mSettingManager:Lcom/android/camera/manager/SettingManager;

    new-instance v0, Lcom/android/camera/manager/ThumbnailManager;

    invoke-direct {v0, p0}, Lcom/android/camera/manager/ThumbnailManager;-><init>(Lcom/android/camera/Camera;)V

    iput-object v0, p0, Lcom/android/camera/Camera;->mThumbnailManager:Lcom/android/camera/manager/ThumbnailManager;

    new-instance v0, Lcom/android/camera/manager/PickerManager;

    invoke-direct {v0, p0}, Lcom/android/camera/manager/PickerManager;-><init>(Lcom/android/camera/Camera;)V

    iput-object v0, p0, Lcom/android/camera/Camera;->mPickerManager:Lcom/android/camera/manager/PickerManager;

    new-instance v0, Lcom/android/camera/manager/IndicatorManager;

    invoke-direct {v0, p0}, Lcom/android/camera/manager/IndicatorManager;-><init>(Lcom/android/camera/Camera;)V

    iput-object v0, p0, Lcom/android/camera/Camera;->mIndicatorManager:Lcom/android/camera/manager/IndicatorManager;

    new-instance v0, Lcom/android/camera/manager/RemainingManager;

    invoke-direct {v0, p0}, Lcom/android/camera/manager/RemainingManager;-><init>(Lcom/android/camera/Camera;)V

    iput-object v0, p0, Lcom/android/camera/Camera;->mRemainingManager:Lcom/android/camera/manager/RemainingManager;

    new-instance v0, Lcom/android/camera/manager/InfoManager;

    invoke-direct {v0, p0}, Lcom/android/camera/manager/InfoManager;-><init>(Lcom/android/camera/Camera;)V

    iput-object v0, p0, Lcom/android/camera/Camera;->mInfoManager:Lcom/android/camera/manager/InfoManager;

    new-instance v0, Lcom/android/camera/manager/ZoomManager;

    invoke-direct {v0, p0}, Lcom/android/camera/manager/ZoomManager;-><init>(Lcom/android/camera/Camera;)V

    iput-object v0, p0, Lcom/android/camera/Camera;->mZoomManager:Lcom/android/camera/manager/ZoomManager;

    new-instance v0, Lcom/android/camera/FileSaver;

    invoke-direct {v0, p0}, Lcom/android/camera/FileSaver;-><init>(Lcom/android/camera/Camera;)V

    iput-object v0, p0, Lcom/android/camera/Camera;->mFileSaver:Lcom/android/camera/FileSaver;

    new-instance v0, Lcom/android/camera/manager/RotateDialog;

    invoke-direct {v0, p0}, Lcom/android/camera/manager/RotateDialog;-><init>(Lcom/android/camera/Camera;)V

    iput-object v0, p0, Lcom/android/camera/Camera;->mRotateDialog:Lcom/android/camera/manager/RotateDialog;

    new-instance v0, Lcom/android/camera/manager/RotateProgress;

    invoke-direct {v0, p0}, Lcom/android/camera/manager/RotateProgress;-><init>(Lcom/android/camera/Camera;)V

    iput-object v0, p0, Lcom/android/camera/Camera;->mRotateProgress:Lcom/android/camera/manager/RotateProgress;

    new-instance v0, Lcom/android/camera/VoiceManager;

    invoke-direct {v0, p0}, Lcom/android/camera/VoiceManager;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/android/camera/Camera;->mVoiceManager:Lcom/android/camera/VoiceManager;

    new-instance v0, Lcom/android/camera/WfdManagerLocal;

    invoke-direct {v0, p0}, Lcom/android/camera/WfdManagerLocal;-><init>(Lcom/android/camera/Camera;)V

    iput-object v0, p0, Lcom/android/camera/Camera;->mWfdLocal:Lcom/android/camera/WfdManagerLocal;

    invoke-direct {p0}, Lcom/android/camera/Camera;->recordCommonManagers()V

    iget-object v0, p0, Lcom/android/camera/Camera;->mModePicker:Lcom/android/camera/manager/ModePicker;

    iget-object v1, p0, Lcom/android/camera/Camera;->mModeChangedListener:Lcom/android/camera/manager/ModePicker$OnModeChangedListener;

    invoke-virtual {v0, v1}, Lcom/android/camera/manager/ModePicker;->setListener(Lcom/android/camera/manager/ModePicker$OnModeChangedListener;)V

    iget-object v0, p0, Lcom/android/camera/Camera;->mSettingManager:Lcom/android/camera/manager/SettingManager;

    iget-object v1, p0, Lcom/android/camera/Camera;->mSettingListener:Lcom/android/camera/manager/SettingManager$SettingListener;

    invoke-virtual {v0, v1}, Lcom/android/camera/manager/SettingManager;->setListener(Lcom/android/camera/manager/SettingManager$SettingListener;)V

    iget-object v0, p0, Lcom/android/camera/Camera;->mPickerManager:Lcom/android/camera/manager/PickerManager;

    iget-object v1, p0, Lcom/android/camera/Camera;->mPickerListener:Lcom/android/camera/manager/PickerManager$PickerListener;

    invoke-virtual {v0, v1}, Lcom/android/camera/manager/PickerManager;->setListener(Lcom/android/camera/manager/PickerManager$PickerListener;)V

    iget-object v0, p0, Lcom/android/camera/Camera;->mThumbnailManager:Lcom/android/camera/manager/ThumbnailManager;

    iget-object v1, p0, Lcom/android/camera/Camera;->mFileSaver:Lcom/android/camera/FileSaver;

    invoke-virtual {v0, v1}, Lcom/android/camera/manager/ThumbnailManager;->setFileSaver(Lcom/android/camera/FileSaver;)V

    iget-object v0, p0, Lcom/android/camera/Camera;->mVoiceManager:Lcom/android/camera/VoiceManager;

    invoke-virtual {v0, p0}, Lcom/android/camera/VoiceManager;->addListener(Lcom/android/camera/VoiceManager$Listener;)Z

    iget-object v0, p0, Lcom/android/camera/Camera;->mWfdLocal:Lcom/android/camera/WfdManagerLocal;

    iget-object v1, p0, Lcom/android/camera/Camera;->mWfdListener:Lcom/android/camera/WfdManagerLocal$Listener;

    invoke-virtual {v0, v1}, Lcom/android/camera/WfdManagerLocal;->addListener(Lcom/android/camera/WfdManagerLocal$Listener;)Z

    return-void
.end method

.method private initializeFocusManager()V
    .locals 11

    const/4 v7, 0x1

    invoke-static {}, Lcom/android/camera/manager/MMProfileManager;->startProfileInitFocusManager()V

    iget-object v0, p0, Lcom/android/camera/Camera;->mFocusManager:Lcom/android/camera/FocusManager;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/camera/Camera;->mFocusManager:Lcom/android/camera/FocusManager;

    invoke-virtual {v0}, Lcom/android/camera/FocusManager;->removeMessages()V

    new-instance v0, Lcom/android/camera/Camera$8;

    invoke-direct {v0, p0}, Lcom/android/camera/Camera$8;-><init>(Lcom/android/camera/Camera;)V

    invoke-virtual {p0, v0}, Landroid/app/Activity;->runOnUiThread(Ljava/lang/Runnable;)V

    iget-object v0, p0, Lcom/android/camera/Camera;->mFocusManager:Lcom/android/camera/FocusManager;

    invoke-virtual {v0}, Lcom/android/camera/FocusManager;->release()V

    :cond_0
    invoke-static {}, Lcom/android/camera/CameraHolder;->instance()Lcom/android/camera/CameraHolder;

    move-result-object v0

    invoke-virtual {v0}, Lcom/android/camera/CameraHolder;->getCameraInfo()[Landroid/hardware/Camera$CameraInfo;

    move-result-object v0

    iget v1, p0, Lcom/android/camera/Camera;->mCameraId:I

    aget-object v10, v0, v1

    iget v0, v10, Landroid/hardware/Camera$CameraInfo;->facing:I

    if-ne v0, v7, :cond_1

    :goto_0
    iget-object v0, p0, Lcom/android/camera/Camera;->mCameraActor:Lcom/android/camera/actor/CameraActor;

    invoke-virtual {v0}, Lcom/android/camera/actor/CameraActor;->getMode()I

    move-result v0

    invoke-static {p0, v0}, Lcom/android/camera/SettingChecker;->getModeDefaultFocusModes(Landroid/content/Context;I)[Ljava/lang/String;

    move-result-object v3

    new-instance v0, Lcom/android/camera/FocusManager;

    iget-object v2, p0, Lcom/android/camera/Camera;->mPreferences:Lcom/android/camera/ComboPreferences;

    iget-object v4, p0, Lcom/android/camera/Camera;->mFocusAreaIndicator:Lcom/android/camera/ui/RotateLayout;

    iget-object v5, p0, Lcom/android/camera/Camera;->mInitialParams:Landroid/hardware/Camera$Parameters;

    iget-object v1, p0, Lcom/android/camera/Camera;->mCameraActor:Lcom/android/camera/actor/CameraActor;

    invoke-virtual {v1}, Lcom/android/camera/actor/CameraActor;->getFocusManagerListener()Lcom/android/camera/FocusManager$Listener;

    move-result-object v6

    invoke-virtual {p0}, Landroid/content/ContextWrapper;->getMainLooper()Landroid/os/Looper;

    move-result-object v8

    iget-object v1, p0, Lcom/android/camera/Camera;->mCameraActor:Lcom/android/camera/actor/CameraActor;

    invoke-virtual {v1}, Lcom/android/camera/actor/CameraActor;->getMode()I

    move-result v1

    invoke-static {v1}, Lcom/android/camera/SettingChecker;->getModeContinousFocusMode(I)Ljava/lang/String;

    move-result-object v9

    move-object v1, p0

    invoke-direct/range {v0 .. v9}, Lcom/android/camera/FocusManager;-><init>(Lcom/android/camera/Camera;Lcom/android/camera/ComboPreferences;[Ljava/lang/String;Landroid/view/View;Landroid/hardware/Camera$Parameters;Lcom/android/camera/FocusManager$Listener;ZLandroid/os/Looper;Ljava/lang/String;)V

    iput-object v0, p0, Lcom/android/camera/Camera;->mFocusManager:Lcom/android/camera/FocusManager;

    iget-object v0, p0, Lcom/android/camera/Camera;->mFocusManager:Lcom/android/camera/FocusManager;

    invoke-virtual {p0}, Lcom/android/camera/Camera;->getPreviewFrameWidth()I

    move-result v1

    invoke-virtual {p0}, Lcom/android/camera/Camera;->getPreviewFrameHeight()I

    move-result v2

    invoke-virtual {v0, v1, v2}, Lcom/android/camera/FocusManager;->setPreviewSize(II)V

    iget-object v0, p0, Lcom/android/camera/Camera;->mFocusManager:Lcom/android/camera/FocusManager;

    iget v1, p0, Lcom/android/camera/Camera;->mDisplayOrientation:I

    invoke-virtual {v0, v1}, Lcom/android/camera/FocusManager;->setDisplayOrientation(I)V

    invoke-static {}, Lcom/android/camera/manager/MMProfileManager;->stopProfileInitFocusManager()V

    return-void

    :cond_1
    const/4 v7, 0x0

    goto :goto_0
.end method

.method private initializeForOpeningProcess()V
    .locals 3

    invoke-static {}, Lcom/android/camera/manager/MMProfileManager;->startProfileInitOpeningProcess()V

    invoke-static {}, Lcom/android/camera/CameraHolder;->instance()Lcom/android/camera/CameraHolder;

    move-result-object v0

    invoke-virtual {v0}, Lcom/android/camera/CameraHolder;->getNumberOfCameras()I

    move-result v0

    iput v0, p0, Lcom/android/camera/Camera;->mNumberOfCameras:I

    invoke-static {}, Lcom/android/camera/manager/MMProfileManager;->startProfileCameraViewOperation()V

    const v0, 0x7f0b0142

    invoke-virtual {p0, v0}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    iput-object v0, p0, Lcom/android/camera/Camera;->mViewLayerBottom:Landroid/view/ViewGroup;

    const v0, 0x7f0b0143

    invoke-virtual {p0, v0}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    iput-object v0, p0, Lcom/android/camera/Camera;->mViewLayerNormal:Landroid/view/ViewGroup;

    const v0, 0x7f0b0144

    invoke-virtual {p0, v0}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    iput-object v0, p0, Lcom/android/camera/Camera;->mViewLayerTop:Landroid/view/ViewGroup;

    const v0, 0x7f0b0145

    invoke-virtual {p0, v0}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    iput-object v0, p0, Lcom/android/camera/Camera;->mViewLayerShutter:Landroid/view/ViewGroup;

    const v0, 0x7f0b0146

    invoke-virtual {p0, v0}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    iput-object v0, p0, Lcom/android/camera/Camera;->mViewLayerSetting:Landroid/view/ViewGroup;

    const v0, 0x7f0b0147

    invoke-virtual {p0, v0}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    iput-object v0, p0, Lcom/android/camera/Camera;->mViewLayerOverlay:Landroid/view/ViewGroup;

    const v0, 0x7f0b005d

    invoke-virtual {p0, v0}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/android/camera/ui/RotateLayout;

    iput-object v0, p0, Lcom/android/camera/Camera;->mFocusAreaIndicator:Lcom/android/camera/ui/RotateLayout;

    iget-object v0, p0, Lcom/android/camera/Camera;->mFaceView:Lcom/android/camera/ui/FaceView;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/camera/Camera;->mFaceView:Lcom/android/camera/ui/FaceView;

    invoke-virtual {v0}, Lcom/android/camera/ui/FaceView;->release()V

    :cond_0
    const v0, 0x7f0b00fb

    invoke-virtual {p0, v0}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/android/camera/ui/FaceView;

    iput-object v0, p0, Lcom/android/camera/Camera;->mFaceView:Lcom/android/camera/ui/FaceView;

    const v0, 0x7f0b00f9

    invoke-virtual {p0, v0}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/android/camera/ui/PreviewFrameLayout;

    iput-object v0, p0, Lcom/android/camera/Camera;->mPreviewFrameLayout:Lcom/android/camera/ui/PreviewFrameLayout;

    iget-object v0, p0, Lcom/android/camera/Camera;->mPreviewFrameLayout:Lcom/android/camera/ui/PreviewFrameLayout;

    invoke-virtual {p0, v0}, Lcom/android/camera/ActivityBase;->setSingleTapUpListener(Landroid/view/View;)V

    iget-object v0, p0, Lcom/android/camera/Camera;->mPreviewFrameLayout:Lcom/android/camera/ui/PreviewFrameLayout;

    invoke-virtual {v0, p0}, Landroid/view/View;->addOnLayoutChangeListener(Landroid/view/View$OnLayoutChangeListener;)V

    iget-object v0, p0, Lcom/android/camera/Camera;->mPreviewFrameLayout:Lcom/android/camera/ui/PreviewFrameLayout;

    invoke-virtual {v0, p0}, Lcom/android/camera/ui/PreviewFrameLayout;->setOnSizeChangedListener(Lcom/android/camera/ui/PreviewFrameLayout$OnSizeChangedListener;)V

    invoke-static {}, Lcom/mediatek/camera/FrameworksClassFactory;->isMockCamera()Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/android/camera/Camera;->mPreviewFrameLayout:Lcom/android/camera/ui/PreviewFrameLayout;

    const v1, 0x7f02013b

    invoke-virtual {v0, v1}, Landroid/view/View;->setBackgroundResource(I)V

    :cond_1
    invoke-static {}, Lcom/android/camera/manager/MMProfileManager;->stopProfileCameraViewOperation()V

    iget-object v0, p0, Lcom/android/camera/Camera;->mLocationManager:Lcom/android/camera/LocationManager;

    if-nez v0, :cond_2

    new-instance v0, Lcom/android/camera/LocationManager;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/android/camera/LocationManager;-><init>(Landroid/content/Context;Lcom/android/camera/LocationManager$Listener;)V

    iput-object v0, p0, Lcom/android/camera/Camera;->mLocationManager:Lcom/android/camera/LocationManager;

    :cond_2
    iget-object v0, p0, Lcom/android/camera/Camera;->mOrientationListener:Lcom/android/camera/Camera$MyOrientationEventListener;

    if-nez v0, :cond_3

    new-instance v0, Lcom/android/camera/Camera$MyOrientationEventListener;

    invoke-direct {v0, p0, p0}, Lcom/android/camera/Camera$MyOrientationEventListener;-><init>(Lcom/android/camera/Camera;Landroid/content/Context;)V

    iput-object v0, p0, Lcom/android/camera/Camera;->mOrientationListener:Lcom/android/camera/Camera$MyOrientationEventListener;

    :cond_3
    sget-boolean v0, Lcom/android/camera/Camera;->LOG:Z

    if-eqz v0, :cond_4

    const-string v0, "Camera"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "initializeForOpeningProcess() mNumberOfCameras="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/android/camera/Camera;->mNumberOfCameras:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/android/camera/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    :cond_4
    invoke-static {}, Lcom/android/camera/manager/MMProfileManager;->stopProfileInitOpeningProcess()V

    return-void
.end method

.method private installIntentFilter()V
    .locals 2

    new-instance v0, Landroid/content/IntentFilter;

    const-string v1, "android.intent.action.MEDIA_MOUNTED"

    invoke-direct {v0, v1}, Landroid/content/IntentFilter;-><init>(Ljava/lang/String;)V

    const-string v1, "android.intent.action.MEDIA_EJECT"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    const-string v1, "android.intent.action.MEDIA_UNMOUNTED"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    const-string v1, "android.intent.action.MEDIA_SCANNER_STARTED"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    const-string v1, "android.intent.action.MEDIA_SCANNER_FINISHED"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    const-string v1, "android.intent.action.MEDIA_CHECKING"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    const-string v1, "file"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addDataScheme(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/android/camera/Camera;->mReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {p0, v1, v0}, Landroid/content/ContextWrapper;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    return-void
.end method

.method private isMountPointChanged()Z
    .locals 5

    const/4 v0, 0x0

    invoke-static {}, Lcom/android/camera/Storage;->getMountPoint()Ljava/lang/String;

    move-result-object v1

    invoke-static {}, Lcom/android/camera/Storage;->updateDefaultDirectory()Z

    invoke-static {}, Lcom/android/camera/Storage;->getMountPoint()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    const/4 v0, 0x1

    :cond_0
    sget-boolean v2, Lcom/android/camera/Camera;->LOG:Z

    if-eqz v2, :cond_1

    const-string v2, "Camera"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "isMountPointChanged() old="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ", new="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-static {}, Lcom/android/camera/Storage;->getMountPoint()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ", return "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/android/camera/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    :cond_1
    return v0
.end method

.method private isSameStorage(Landroid/content/Intent;)Z
    .locals 7
    .param p1    # Landroid/content/Intent;

    const-string v4, "storage_volume"

    invoke-virtual {p1, v4}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v3

    check-cast v3, Landroid/os/storage/StorageVolume;

    const/4 v2, 0x0

    const/4 v1, 0x0

    const/4 v0, 0x0

    if-eqz v3, :cond_0

    invoke-static {}, Lcom/android/camera/Storage;->getMountPoint()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v3}, Landroid/os/storage/StorageVolume;->getPath()Ljava/lang/String;

    move-result-object v0

    if-eqz v1, :cond_0

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_0

    const/4 v2, 0x1

    :cond_0
    sget-boolean v4, Lcom/android/camera/Camera;->LOG:Z

    if-eqz v4, :cond_1

    const-string v4, "Camera"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "isSameStorage() mountPoint="

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, ", intentPath="

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, ", return "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/android/camera/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    :cond_1
    return v2
.end method

.method private isSameStorage(Landroid/net/Uri;)Z
    .locals 6
    .param p1    # Landroid/net/Uri;

    const/4 v2, 0x0

    const/4 v1, 0x0

    const/4 v0, 0x0

    if-eqz p1, :cond_0

    invoke-static {}, Lcom/android/camera/Storage;->getMountPoint()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1}, Landroid/net/Uri;->getPath()Ljava/lang/String;

    move-result-object v0

    if-eqz v1, :cond_0

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    const/4 v2, 0x1

    :cond_0
    sget-boolean v3, Lcom/android/camera/Camera;->LOG:Z

    if-eqz v3, :cond_1

    const-string v3, "Camera"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "isSameStorage("

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ") mountPoint="

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ", intentPath="

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ", return "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/android/camera/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    :cond_1
    return v2
.end method

.method private isVoiceEnabled()Z
    .locals 4

    const/4 v0, 0x0

    iget-object v1, p0, Lcom/android/camera/Camera;->mSettingChecker:Lcom/android/camera/SettingChecker;

    if-eqz v1, :cond_0

    const-string v1, "on"

    iget-object v2, p0, Lcom/android/camera/Camera;->mSettingChecker:Lcom/android/camera/SettingChecker;

    const/16 v3, 0x16

    invoke-virtual {v2, v3}, Lcom/android/camera/SettingChecker;->getSettingCurrentValue(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v0, 0x1

    :cond_0
    return v0
.end method

.method private limitPreviewByIntent(Landroid/media/CamcorderProfile;)V
    .locals 11
    .param p1    # Landroid/media/CamcorderProfile;

    const/16 v10, 0x12c

    sget-boolean v7, Lcom/android/camera/Camera;->LOG:Z

    if-eqz v7, :cond_0

    const-string v7, "Camera"

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "limitPreviewByIntent() videoFrameWidth="

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    iget v9, p1, Landroid/media/CamcorderProfile;->videoFrameWidth:I

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, ", videoFrameHeight="

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    iget v9, p1, Landroid/media/CamcorderProfile;->videoFrameHeight:I

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, ", mDisplayRotation="

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    iget v9, p0, Lcom/android/camera/Camera;->mDisplayRotation:I

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, ", mOrientationCompensation="

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    iget v9, p0, Lcom/android/camera/Camera;->mOrientationCompensation:I

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8}, Lcom/android/camera/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    invoke-virtual {p0}, Lcom/android/camera/Camera;->isVideoWallPaperIntent()Z

    move-result v7

    if-eqz v7, :cond_3

    iget-object v7, p0, Lcom/android/camera/Camera;->mParameters:Landroid/hardware/Camera$Parameters;

    if-eqz v7, :cond_3

    new-instance v5, Ljava/util/ArrayList;

    iget-object v7, p0, Lcom/android/camera/Camera;->mParameters:Landroid/hardware/Camera$Parameters;

    invoke-virtual {v7}, Landroid/hardware/Camera$Parameters;->getSupportedPreviewSizes()Ljava/util/List;

    move-result-object v7

    invoke-direct {v5, v7}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    iget v4, p0, Lcom/android/camera/Camera;->mOrientationCompensation:I

    invoke-interface {v5}, Ljava/util/List;->size()I

    move-result v7

    add-int/lit8 v7, v7, -0x1

    invoke-interface {v5, v7}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/hardware/Camera$Size;

    iget v7, v3, Landroid/hardware/Camera$Size;->width:I

    int-to-float v7, v7

    iget v8, v3, Landroid/hardware/Camera$Size;->height:I

    int-to-float v8, v8

    div-float v2, v7, v8

    invoke-interface {v5}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_1
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v7

    if-eqz v7, :cond_2

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Landroid/hardware/Camera$Size;

    iget v7, v6, Landroid/hardware/Camera$Size;->width:I

    if-lt v7, v10, :cond_1

    iget v7, v6, Landroid/hardware/Camera$Size;->height:I

    if-lt v7, v10, :cond_1

    iget v7, v6, Landroid/hardware/Camera$Size;->width:I

    int-to-float v7, v7

    iget v8, v6, Landroid/hardware/Camera$Size;->height:I

    int-to-float v8, v8

    div-float v0, v7, v8

    iget v7, p0, Lcom/android/camera/Camera;->mWallpaperAspectio:F

    sub-float v7, v0, v7

    invoke-static {v7}, Ljava/lang/Math;->abs(F)F

    move-result v7

    iget v8, p0, Lcom/android/camera/Camera;->mWallpaperAspectio:F

    sub-float v8, v2, v8

    invoke-static {v8}, Ljava/lang/Math;->abs(F)F

    move-result v8

    cmpg-float v7, v7, v8

    if-gtz v7, :cond_1

    move v2, v0

    move-object v3, v6

    goto :goto_0

    :cond_2
    iget v7, p0, Lcom/android/camera/Camera;->mDisplayRotation:I

    if-nez v7, :cond_6

    rem-int/lit16 v7, v4, 0xb4

    if-nez v7, :cond_5

    iget v7, v3, Landroid/hardware/Camera$Size;->height:I

    iput v7, p1, Landroid/media/CamcorderProfile;->videoFrameWidth:I

    iget v7, v3, Landroid/hardware/Camera$Size;->width:I

    iput v7, p1, Landroid/media/CamcorderProfile;->videoFrameHeight:I

    :goto_1
    sget-boolean v7, Lcom/android/camera/Camera;->LOG:Z

    if-eqz v7, :cond_3

    const-string v7, "Camera"

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "limitPreviewByIntent() findSize="

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-static {v3}, Lcom/android/camera/SettingUtils;->buildSize(Landroid/hardware/Camera$Size;)Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8}, Lcom/android/camera/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    :cond_3
    sget-boolean v7, Lcom/android/camera/Camera;->LOG:Z

    if-eqz v7, :cond_4

    const-string v7, "Camera"

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "limitPreviewByIntent() videoFrameWidth="

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    iget v9, p1, Landroid/media/CamcorderProfile;->videoFrameWidth:I

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, " videoFrameHeight="

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    iget v9, p1, Landroid/media/CamcorderProfile;->videoFrameHeight:I

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, ", mDisplayRotation="

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    iget v9, p0, Lcom/android/camera/Camera;->mDisplayRotation:I

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, ", orientation="

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    iget v9, p0, Lcom/android/camera/Camera;->mOrientationCompensation:I

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8}, Lcom/android/camera/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    :cond_4
    return-void

    :cond_5
    iget v7, v3, Landroid/hardware/Camera$Size;->width:I

    iput v7, p1, Landroid/media/CamcorderProfile;->videoFrameWidth:I

    iget v7, v3, Landroid/hardware/Camera$Size;->height:I

    iput v7, p1, Landroid/media/CamcorderProfile;->videoFrameHeight:I

    goto :goto_1

    :cond_6
    rem-int/lit16 v7, v4, 0xb4

    if-nez v7, :cond_7

    iget v7, v3, Landroid/hardware/Camera$Size;->width:I

    iput v7, p1, Landroid/media/CamcorderProfile;->videoFrameWidth:I

    iget v7, v3, Landroid/hardware/Camera$Size;->height:I

    iput v7, p1, Landroid/media/CamcorderProfile;->videoFrameHeight:I

    goto :goto_1

    :cond_7
    iget v7, v3, Landroid/hardware/Camera$Size;->height:I

    iput v7, p1, Landroid/media/CamcorderProfile;->videoFrameWidth:I

    iget v7, v3, Landroid/hardware/Camera$Size;->width:I

    iput v7, p1, Landroid/media/CamcorderProfile;->videoFrameHeight:I

    goto/16 :goto_1
.end method

.method private limitSettingsByIntent()V
    .locals 5

    const/16 v4, 0x14

    invoke-virtual {p0}, Lcom/android/camera/Camera;->isImageCaptureIntent()Z

    move-result v1

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/android/camera/Camera;->mCameraSettings:Lcom/android/camera/CameraSettings;

    iget-object v2, p0, Lcom/android/camera/Camera;->mPreferenceGroup:Lcom/android/camera/PreferenceGroup;

    const-string v3, "pref_camera_shot_number"

    const/16 v4, 0x9

    invoke-virtual {v1, v2, v3, v4}, Lcom/android/camera/CameraSettings;->removePreferenceFromScreen(Lcom/android/camera/PreferenceGroup;Ljava/lang/String;I)V

    :cond_0
    :goto_0
    return-void

    :cond_1
    invoke-virtual {p0}, Landroid/app/Activity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v1, "android.intent.extra.videoQuality"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/android/camera/Camera;->mCameraSettings:Lcom/android/camera/CameraSettings;

    iget-object v2, p0, Lcom/android/camera/Camera;->mPreferenceGroup:Lcom/android/camera/PreferenceGroup;

    const-string v3, "pref_video_quality_key"

    invoke-virtual {v1, v2, v3, v4}, Lcom/android/camera/CameraSettings;->removePreferenceFromScreen(Lcom/android/camera/PreferenceGroup;Ljava/lang/String;I)V

    :cond_2
    invoke-virtual {p0}, Lcom/android/camera/Camera;->isVideoWallPaperIntent()Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/android/camera/Camera;->mCameraSettings:Lcom/android/camera/CameraSettings;

    iget-object v2, p0, Lcom/android/camera/Camera;->mPreferenceGroup:Lcom/android/camera/PreferenceGroup;

    const-string v3, "pref_video_quality_key"

    invoke-virtual {v1, v2, v3, v4}, Lcom/android/camera/CameraSettings;->removePreferenceFromScreen(Lcom/android/camera/PreferenceGroup;Ljava/lang/String;I)V

    iget-object v1, p0, Lcom/android/camera/Camera;->mCameraSettings:Lcom/android/camera/CameraSettings;

    iget-object v2, p0, Lcom/android/camera/Camera;->mPreferenceGroup:Lcom/android/camera/PreferenceGroup;

    const-string v3, "pref_video_effect_key"

    const/16 v4, 0x13

    invoke-virtual {v1, v2, v3, v4}, Lcom/android/camera/CameraSettings;->removePreferenceFromScreen(Lcom/android/camera/PreferenceGroup;Ljava/lang/String;I)V

    goto :goto_0
.end method

.method private notifyOnFullScreenChanged(Z)V
    .locals 3
    .param p1    # Z

    iget-object v2, p0, Lcom/android/camera/Camera;->mFullScreenListeners:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :cond_0
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/android/camera/Camera$OnFullScreenChangedListener;

    if-eqz v1, :cond_0

    invoke-interface {v1, p1}, Lcom/android/camera/Camera$OnFullScreenChangedListener;->onFullScreenChanged(Z)V

    goto :goto_0

    :cond_1
    return-void
.end method

.method private notifyOrientationChanged()V
    .locals 5

    sget-boolean v2, Lcom/android/camera/Camera;->LOG:Z

    if-eqz v2, :cond_0

    const-string v2, "Camera"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "notifyOrientationChanged() mOrientationCompensation="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget v4, p0, Lcom/android/camera/Camera;->mOrientationCompensation:I

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ", mDisplayRotation="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget v4, p0, Lcom/android/camera/Camera;->mDisplayRotation:I

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/android/camera/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    invoke-static {}, Lcom/android/camera/manager/MMProfileManager;->startProfileNotifyOrientChanged()V

    iget-object v2, p0, Lcom/android/camera/Camera;->mOrientationListeners:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :cond_1
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_2

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/android/camera/Camera$OnOrientationListener;

    if-eqz v1, :cond_1

    iget v2, p0, Lcom/android/camera/Camera;->mOrientationCompensation:I

    invoke-interface {v1, v2}, Lcom/android/camera/Camera$OnOrientationListener;->onOrientationChanged(I)V

    goto :goto_0

    :cond_2
    invoke-static {}, Lcom/android/camera/manager/MMProfileManager;->stopProfileNotifyOrientChanged()V

    return-void
.end method

.method private notifyParametersReady()V
    .locals 5

    invoke-virtual {p0}, Lcom/android/camera/Camera;->isNonePickIntent()Z

    move-result v2

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/android/camera/Camera;->mModePicker:Lcom/android/camera/manager/ModePicker;

    invoke-virtual {v2}, Lcom/android/camera/manager/ViewManager;->show()V

    :cond_0
    invoke-direct {p0}, Lcom/android/camera/Camera;->setPreviewFrameLayoutAspectRatio()V

    iget-object v2, p0, Lcom/android/camera/Camera;->mLocationManager:Lcom/android/camera/LocationManager;

    iget-object v3, p0, Lcom/android/camera/Camera;->mPreferences:Lcom/android/camera/ComboPreferences;

    invoke-virtual {p0}, Landroid/content/ContextWrapper;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/android/camera/RecordLocationPreference;->get(Landroid/content/SharedPreferences;Landroid/content/ContentResolver;)Z

    move-result v3

    invoke-virtual {v2, v3}, Lcom/android/camera/LocationManager;->recordLocation(Z)V

    iget-object v2, p0, Lcom/android/camera/Camera;->mParametersListeners:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :cond_1
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_2

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/android/camera/Camera$OnParametersReadyListener;

    if-eqz v1, :cond_1

    invoke-interface {v1}, Lcom/android/camera/Camera$OnParametersReadyListener;->onCameraParameterReady()V

    goto :goto_0

    :cond_2
    return-void
.end method

.method private notifyPreferenceReady()V
    .locals 3

    iget-object v2, p0, Lcom/android/camera/Camera;->mPreferenceListeners:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :cond_0
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/android/camera/Camera$OnPreferenceReadyListener;

    if-eqz v1, :cond_0

    invoke-interface {v1}, Lcom/android/camera/Camera$OnPreferenceReadyListener;->onPreferenceReady()V

    goto :goto_0

    :cond_1
    return-void
.end method

.method private parseIntent()V
    .locals 6

    const/4 v4, 0x1

    const/4 v5, 0x0

    invoke-virtual {p0}, Landroid/app/Activity;->getIntent()Landroid/content/Intent;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    const-string v2, "android.media.action.IMAGE_CAPTURE"

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    iput v4, p0, Lcom/android/camera/Camera;->mPickType:I

    :goto_0
    iget v2, p0, Lcom/android/camera/Camera;->mPickType:I

    if-eqz v2, :cond_0

    const-string v2, "android.intent.extra.quickCapture"

    invoke-virtual {v1, v2, v5}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v2

    iput-boolean v2, p0, Lcom/android/camera/Camera;->mQuickCapture:Z

    const-string v2, "output"

    invoke-virtual {v1, v2}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v2

    check-cast v2, Landroid/net/Uri;

    iput-object v2, p0, Lcom/android/camera/Camera;->mSaveUri:Landroid/net/Uri;

    const-string v2, "android.intent.extra.sizeLimit"

    const-wide/16 v3, 0x0

    invoke-virtual {v1, v2, v3, v4}, Landroid/content/Intent;->getLongExtra(Ljava/lang/String;J)J

    move-result-wide v2

    iput-wide v2, p0, Lcom/android/camera/Camera;->mLimitedSize:J

    const-string v2, "crop"

    invoke-virtual {v1, v2}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/android/camera/Camera;->mCropValue:Ljava/lang/String;

    const-string v2, "android.intent.extra.durationLimit"

    invoke-virtual {v1, v2, v5}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v2

    iput v2, p0, Lcom/android/camera/Camera;->mLimitedDuration:I

    :cond_0
    sget-boolean v2, Lcom/android/camera/Camera;->LOG:Z

    if-eqz v2, :cond_1

    const-string v2, "Camera"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "parseIntent() mPickType="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget v4, p0, Lcom/android/camera/Camera;->mPickType:I

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ", mQuickCapture="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-boolean v4, p0, Lcom/android/camera/Camera;->mQuickCapture:Z

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ", mSaveUri="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lcom/android/camera/Camera;->mSaveUri:Landroid/net/Uri;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ", mLimitedSize="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-wide v4, p0, Lcom/android/camera/Camera;->mLimitedSize:J

    invoke-virtual {v3, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ", mCropValue="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lcom/android/camera/Camera;->mCropValue:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ", mLimitedDuration="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget v4, p0, Lcom/android/camera/Camera;->mLimitedDuration:I

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/android/camera/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    :cond_1
    return-void

    :cond_2
    const-string v2, "com.mediatek.vlw"

    const-string v3, "identity"

    invoke-virtual {v1, v3}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    const-string v2, "ratio"

    const v3, 0x3f99999a

    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->getFloatExtra(Ljava/lang/String;F)F

    move-result v2

    iput v2, p0, Lcom/android/camera/Camera;->mWallpaperAspectio:F

    const-string v2, "android.intent.extra.quickCapture"

    invoke-virtual {v1, v2, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    const/4 v2, 0x3

    iput v2, p0, Lcom/android/camera/Camera;->mPickType:I

    goto/16 :goto_0

    :cond_3
    const-string v2, "android.media.action.VIDEO_CAPTURE"

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_4

    const/4 v2, 0x2

    iput v2, p0, Lcom/android/camera/Camera;->mPickType:I

    goto/16 :goto_0

    :cond_4
    iput v5, p0, Lcom/android/camera/Camera;->mPickType:I

    goto/16 :goto_0
.end method

.method private prepareMockCamera()V
    .locals 1

    iget-object v0, p0, Lcom/android/camera/Camera;->mCameraDevice:Lcom/android/camera/CameraManager$CameraProxy;

    if-nez v0, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v0, p0, Lcom/android/camera/Camera;->mCameraDevice:Lcom/android/camera/CameraManager$CameraProxy;

    invoke-virtual {v0}, Lcom/android/camera/CameraManager$CameraProxy;->getCamera()Lcom/mediatek/camera/ICamera;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-static {}, Lcom/mediatek/camera/FrameworksClassFactory;->isMockCamera()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/camera/Camera;->mCameraDevice:Lcom/android/camera/CameraManager$CameraProxy;

    invoke-virtual {v0}, Lcom/android/camera/CameraManager$CameraProxy;->getCamera()Lcom/mediatek/camera/ICamera;

    move-result-object v0

    invoke-interface {v0, p0}, Lcom/mediatek/camera/ICamera;->setContext(Landroid/content/Context;)V

    goto :goto_0
.end method

.method private reInflateViewManager()V
    .locals 3

    invoke-static {}, Lcom/android/camera/manager/MMProfileManager;->startProfileReInflateViewManager()V

    iget-object v2, p0, Lcom/android/camera/Camera;->mViewManagers:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/android/camera/manager/ViewManager;

    invoke-virtual {v1}, Lcom/android/camera/manager/ViewManager;->reInflate()V

    goto :goto_0

    :cond_0
    invoke-static {}, Lcom/android/camera/manager/MMProfileManager;->stopProfileReInflateViewManager()V

    return-void
.end method

.method private recordCommonManagers()V
    .locals 1

    iget-object v0, p0, Lcom/android/camera/Camera;->mViewManagers:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    iput v0, p0, Lcom/android/camera/Camera;->mCommonManagerCount:I

    return-void
.end method

.method private refreshModeRelated()V
    .locals 1

    iget-object v0, p0, Lcom/android/camera/Camera;->mModePicker:Lcom/android/camera/manager/ModePicker;

    invoke-virtual {v0}, Lcom/android/camera/manager/ViewManager;->refresh()V

    iget-object v0, p0, Lcom/android/camera/Camera;->mPickerManager:Lcom/android/camera/manager/PickerManager;

    invoke-virtual {v0}, Lcom/android/camera/manager/ViewManager;->refresh()V

    iget-object v0, p0, Lcom/android/camera/Camera;->mShutterManager:Lcom/android/camera/manager/ShutterManager;

    invoke-virtual {v0}, Lcom/android/camera/manager/ViewManager;->refresh()V

    return-void
.end method

.method private releaseCameraActor()V
    .locals 3

    sget-boolean v0, Lcom/android/camera/Camera;->LOG:Z

    if-eqz v0, :cond_0

    const-string v0, "Camera"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "releaseCameraActor() mode="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/android/camera/Camera;->mCameraActor:Lcom/android/camera/actor/CameraActor;

    invoke-virtual {v2}, Lcom/android/camera/actor/CameraActor;->getMode()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/android/camera/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/android/camera/Camera;->collapseViewManager(Z)Z

    iget-object v0, p0, Lcom/android/camera/Camera;->mCameraActor:Lcom/android/camera/actor/CameraActor;

    invoke-virtual {v0}, Lcom/android/camera/actor/CameraActor;->release()V

    iget-object v0, p0, Lcom/android/camera/Camera;->mCameraActor:Lcom/android/camera/actor/CameraActor;

    invoke-virtual {v0}, Lcom/android/camera/actor/CameraActor;->getMode()I

    move-result v0

    iput v0, p0, Lcom/android/camera/Camera;->mLastMode:I

    return-void
.end method

.method private releaseSurface()V
    .locals 1

    iget-object v0, p0, Lcom/android/camera/Camera;->mSurfaceTexture:Landroid/graphics/SurfaceTexture;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/camera/ActivityBase;->mCameraScreenNail:Lcom/android/camera/CameraScreenNail;

    invoke-virtual {v0}, Lcom/android/camera/CameraScreenNail;->releaseSurfaceTexture()V

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/android/camera/Camera;->mSurfaceTexture:Landroid/graphics/SurfaceTexture;

    :cond_0
    return-void
.end method

.method private restoreActorViews()V
    .locals 4

    iget-object v3, p0, Lcom/android/camera/Camera;->mLastManagers:[Lcom/android/camera/manager/ViewManager;

    if-nez v3, :cond_0

    :goto_0
    return-void

    :cond_0
    iget-object v3, p0, Lcom/android/camera/Camera;->mLastManagers:[Lcom/android/camera/manager/ViewManager;

    array-length v1, v3

    iget v3, p0, Lcom/android/camera/Camera;->mCommonManagerCount:I

    add-int/lit8 v0, v3, -0x1

    :goto_1
    if-ge v0, v1, :cond_2

    iget-object v3, p0, Lcom/android/camera/Camera;->mLastManagers:[Lcom/android/camera/manager/ViewManager;

    aget-object v2, v3, v0

    if-eqz v2, :cond_1

    iget-object v3, p0, Lcom/android/camera/Camera;->mViewManagers:Ljava/util/List;

    invoke-interface {v3, v2}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_1

    invoke-virtual {v2}, Lcom/android/camera/manager/ViewManager;->isShowing()Z

    move-result v3

    if-nez v3, :cond_1

    iget-object v3, p0, Lcom/android/camera/Camera;->mLastVisibles:[Z

    aget-boolean v3, v3, v0

    if-eqz v3, :cond_1

    invoke-virtual {v2}, Lcom/android/camera/manager/ViewManager;->show()V

    :cond_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    :cond_2
    const/4 v3, 0x0

    iput-object v3, p0, Lcom/android/camera/Camera;->mLastManagers:[Lcom/android/camera/manager/ViewManager;

    goto :goto_0
.end method

.method private setDisplayOrientation()V
    .locals 3

    invoke-static {}, Lcom/android/camera/manager/MMProfileManager;->startProfileSetDisplayOrientation()V

    invoke-static {p0}, Lcom/android/camera/Util;->getDisplayRotation(Landroid/app/Activity;)I

    move-result v0

    iput v0, p0, Lcom/android/camera/Camera;->mDisplayRotation:I

    iget v0, p0, Lcom/android/camera/Camera;->mDisplayRotation:I

    iget v1, p0, Lcom/android/camera/Camera;->mCameraId:I

    invoke-static {v0, v1}, Lcom/android/camera/Util;->getDisplayOrientation(II)I

    move-result v0

    iput v0, p0, Lcom/android/camera/Camera;->mDisplayOrientation:I

    const/4 v0, 0x0

    iget v1, p0, Lcom/android/camera/Camera;->mCameraId:I

    invoke-static {v0, v1}, Lcom/android/camera/Util;->getDisplayOrientation(II)I

    move-result v0

    iput v0, p0, Lcom/android/camera/Camera;->mCameraDisplayOrientation:I

    iget-object v0, p0, Lcom/android/camera/Camera;->mFaceView:Lcom/android/camera/ui/FaceView;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/camera/Camera;->mFaceView:Lcom/android/camera/ui/FaceView;

    iget v1, p0, Lcom/android/camera/Camera;->mDisplayOrientation:I

    invoke-virtual {v0, v1}, Lcom/android/camera/ui/FaceView;->setDisplayOrientation(I)V

    :cond_0
    iget-object v0, p0, Lcom/android/camera/Camera;->mFocusManager:Lcom/android/camera/FocusManager;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/android/camera/Camera;->mFocusManager:Lcom/android/camera/FocusManager;

    iget v1, p0, Lcom/android/camera/Camera;->mDisplayOrientation:I

    invoke-virtual {v0, v1}, Lcom/android/camera/FocusManager;->setDisplayOrientation(I)V

    :cond_1
    invoke-virtual {p0}, Lcom/android/gallery3d/app/AbstractGalleryActivity;->getGLRoot()Lcom/android/gallery3d/ui/GLRoot;

    move-result-object v0

    if-eqz v0, :cond_2

    invoke-virtual {p0}, Lcom/android/gallery3d/app/AbstractGalleryActivity;->getGLRoot()Lcom/android/gallery3d/ui/GLRoot;

    move-result-object v0

    invoke-interface {v0}, Lcom/android/gallery3d/ui/GLRoot;->requestLayoutContentPane()V

    :cond_2
    sget-boolean v0, Lcom/android/camera/Camera;->LOG:Z

    if-eqz v0, :cond_3

    const-string v0, "Camera"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "setDisplayOrientation() mDisplayRotation="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/android/camera/Camera;->mDisplayRotation:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", mCameraDisplayOrientation="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/android/camera/Camera;->mCameraDisplayOrientation:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", mDisplayOrientation="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/android/camera/Camera;->mDisplayOrientation:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/android/camera/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    :cond_3
    invoke-static {}, Lcom/android/camera/manager/MMProfileManager;->stopProfileSetDisplayOrientation()V

    return-void
.end method

.method private setLoadingAnimationVisible(Z)V
    .locals 3
    .param p1    # Z

    sget-boolean v0, Lcom/android/camera/Camera;->LOG:Z

    if-eqz v0, :cond_0

    const-string v0, "Camera"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "setLoadingAnimationVisible("

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ")"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/android/camera/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    return-void
.end method

.method private setPreviewFrameLayoutAspectRatio()V
    .locals 8

    iget-object v3, p0, Lcom/android/camera/Camera;->mPreviewFrameLayout:Lcom/android/camera/ui/PreviewFrameLayout;

    if-eqz v3, :cond_0

    iget-object v3, p0, Lcom/android/camera/Camera;->mParameters:Landroid/hardware/Camera$Parameters;

    if-eqz v3, :cond_0

    invoke-static {}, Lcom/android/camera/manager/MMProfileManager;->triggerSetPreviewAspectRatio()V

    const/4 v2, 0x1

    const/4 v0, 0x1

    invoke-virtual {p0}, Lcom/android/camera/Camera;->isVideoMode()Z

    move-result v3

    if-eqz v3, :cond_1

    iget-object v3, p0, Lcom/android/camera/Camera;->mProfile:Landroid/media/CamcorderProfile;

    if-eqz v3, :cond_1

    iget-object v3, p0, Lcom/android/camera/Camera;->mProfile:Landroid/media/CamcorderProfile;

    iget v2, v3, Landroid/media/CamcorderProfile;->videoFrameWidth:I

    iget-object v3, p0, Lcom/android/camera/Camera;->mProfile:Landroid/media/CamcorderProfile;

    iget v0, v3, Landroid/media/CamcorderProfile;->videoFrameHeight:I

    :goto_0
    iget-object v3, p0, Lcom/android/camera/Camera;->mPreviewFrameLayout:Lcom/android/camera/ui/PreviewFrameLayout;

    int-to-double v4, v2

    int-to-double v6, v0

    div-double/2addr v4, v6

    invoke-virtual {v3, v4, v5}, Lcom/android/camera/ui/PreviewFrameLayout;->setAspectRatio(D)V

    sget-boolean v3, Lcom/android/camera/Camera;->LOG:Z

    if-eqz v3, :cond_0

    const-string v3, "Camera"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "setPreviewFrameLayoutAspectRatio() width="

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ", height="

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/android/camera/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    return-void

    :cond_1
    iget-object v3, p0, Lcom/android/camera/Camera;->mParameters:Landroid/hardware/Camera$Parameters;

    invoke-virtual {v3}, Landroid/hardware/Camera$Parameters;->getPreviewSize()Landroid/hardware/Camera$Size;

    move-result-object v1

    iget v2, v1, Landroid/hardware/Camera$Size;->width:I

    iget v0, v1, Landroid/hardware/Camera$Size;->height:I

    goto :goto_0
.end method

.method private setViewManagerEnable(Z)V
    .locals 1
    .param p1    # Z

    invoke-virtual {p0}, Lcom/android/camera/Camera;->isNonePickIntent()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/camera/Camera;->mModePicker:Lcom/android/camera/manager/ModePicker;

    invoke-virtual {v0, p1}, Lcom/android/camera/manager/ModePicker;->setEnabled(Z)V

    iget-object v0, p0, Lcom/android/camera/Camera;->mThumbnailManager:Lcom/android/camera/manager/ThumbnailManager;

    invoke-virtual {v0, p1}, Lcom/android/camera/manager/ThumbnailManager;->setEnabled(Z)V

    :cond_0
    iget-object v0, p0, Lcom/android/camera/Camera;->mSettingManager:Lcom/android/camera/manager/SettingManager;

    invoke-virtual {v0, p1}, Lcom/android/camera/manager/ViewManager;->setEnabled(Z)V

    iget-object v0, p0, Lcom/android/camera/Camera;->mPickerManager:Lcom/android/camera/manager/PickerManager;

    invoke-virtual {v0, p1}, Lcom/android/camera/manager/PickerManager;->setEnabled(Z)V

    iget-object v0, p0, Lcom/android/camera/Camera;->mZoomManager:Lcom/android/camera/manager/ZoomManager;

    invoke-virtual {v0, p1}, Lcom/android/camera/manager/ViewManager;->setEnabled(Z)V

    return-void
.end method

.method private setViewManagerVisible(Z)V
    .locals 1
    .param p1    # Z

    if-eqz p1, :cond_1

    invoke-virtual {p0}, Lcom/android/camera/Camera;->isNonePickIntent()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/camera/Camera;->mModePicker:Lcom/android/camera/manager/ModePicker;

    invoke-virtual {v0}, Lcom/android/camera/manager/ViewManager;->show()V

    iget-object v0, p0, Lcom/android/camera/Camera;->mThumbnailManager:Lcom/android/camera/manager/ThumbnailManager;

    invoke-virtual {v0}, Lcom/android/camera/manager/ViewManager;->show()V

    :cond_0
    iget-object v0, p0, Lcom/android/camera/Camera;->mShutterManager:Lcom/android/camera/manager/ShutterManager;

    invoke-virtual {v0}, Lcom/android/camera/manager/ViewManager;->show()V

    iget-object v0, p0, Lcom/android/camera/Camera;->mSettingManager:Lcom/android/camera/manager/SettingManager;

    invoke-virtual {v0}, Lcom/android/camera/manager/ViewManager;->show()V

    :goto_0
    return-void

    :cond_1
    iget-object v0, p0, Lcom/android/camera/Camera;->mModePicker:Lcom/android/camera/manager/ModePicker;

    invoke-virtual {v0}, Lcom/android/camera/manager/ViewManager;->hide()V

    iget-object v0, p0, Lcom/android/camera/Camera;->mPickerManager:Lcom/android/camera/manager/PickerManager;

    invoke-virtual {v0}, Lcom/android/camera/manager/ViewManager;->hide()V

    iget-object v0, p0, Lcom/android/camera/Camera;->mSettingManager:Lcom/android/camera/manager/SettingManager;

    invoke-virtual {v0}, Lcom/android/camera/manager/SettingManager;->hide()V

    goto :goto_0
.end method

.method private setZoomParameter()V
    .locals 1

    iget-object v0, p0, Lcom/android/camera/Camera;->mZoomManager:Lcom/android/camera/manager/ZoomManager;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/camera/Camera;->mZoomManager:Lcom/android/camera/manager/ZoomManager;

    invoke-virtual {v0}, Lcom/android/camera/manager/ZoomManager;->setZoomParameter()V

    :cond_0
    return-void
.end method

.method private showIndicator(I)V
    .locals 4
    .param p1    # I

    const/16 v3, 0x8

    sget-boolean v0, Lcom/android/camera/Camera;->LOG:Z

    if-eqz v0, :cond_0

    const-string v0, "Camera"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "showIndicator("

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ")"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/android/camera/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    iget-object v0, p0, Lcom/android/camera/Camera;->mMainHandler:Landroid/os/Handler;

    invoke-virtual {v0, v3}, Landroid/os/Handler;->removeMessages(I)V

    if-lez p1, :cond_1

    iget-object v0, p0, Lcom/android/camera/Camera;->mMainHandler:Landroid/os/Handler;

    int-to-long v1, p1

    invoke-virtual {v0, v3, v1, v2}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    :goto_0
    return-void

    :cond_1
    iget-object v0, p0, Lcom/android/camera/Camera;->mMainHandler:Landroid/os/Handler;

    invoke-virtual {v0, v3}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    goto :goto_0
.end method

.method private showRemainingAways()V
    .locals 2

    sget-boolean v0, Lcom/android/camera/Camera;->LOG:Z

    if-eqz v0, :cond_0

    const-string v0, "Camera"

    const-string v1, "showRemainingAways()"

    invoke-static {v0, v1}, Lcom/android/camera/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    iget-object v0, p0, Lcom/android/camera/Camera;->mMainHandler:Landroid/os/Handler;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeMessages(I)V

    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/android/camera/Camera;->doShowRemaining(Z)V

    return-void
.end method

.method private switchCamera()V
    .locals 8

    const/4 v7, 0x6

    const/4 v6, -0x1

    const/4 v5, 0x0

    sget-boolean v2, Lcom/android/camera/Camera;->LOG:Z

    if-eqz v2, :cond_0

    const-string v2, "Camera"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "switchCamera() begin id="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget v4, p0, Lcom/android/camera/Camera;->mPendingSwitchCameraId:I

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ", mPaused="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-boolean v4, p0, Lcom/android/camera/ActivityBase;->mPaused:Z

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/android/camera/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    iget-boolean v2, p0, Lcom/android/camera/ActivityBase;->mPaused:Z

    if-nez v2, :cond_1

    iget-boolean v2, p0, Lcom/android/camera/Camera;->mOpenCameraFail:Z

    if-nez v2, :cond_1

    iget-boolean v2, p0, Lcom/android/camera/Camera;->mCameraDisabled:Z

    if-nez v2, :cond_1

    iget v2, p0, Lcom/android/camera/Camera;->mPendingSwitchCameraId:I

    if-ne v2, v6, :cond_2

    :cond_1
    :goto_0
    return-void

    :cond_2
    invoke-static {}, Lcom/android/camera/manager/MMProfileManager;->startProfileSwitchCamera()V

    iget v2, p0, Lcom/android/camera/Camera;->mPendingSwitchCameraId:I

    iput v2, p0, Lcom/android/camera/Camera;->mCameraId:I

    iget-object v2, p0, Lcom/android/camera/Camera;->mPickerManager:Lcom/android/camera/manager/PickerManager;

    iget v3, p0, Lcom/android/camera/Camera;->mCameraId:I

    invoke-virtual {v2, v3}, Lcom/android/camera/manager/PickerManager;->setCameraId(I)V

    invoke-direct {p0}, Lcom/android/camera/Camera;->closeCamera()V

    const/4 v2, 0x1

    invoke-virtual {p0, v2}, Lcom/android/camera/Camera;->collapseViewManager(Z)Z

    invoke-direct {p0}, Lcom/android/camera/Camera;->clearFocusAndFace()V

    iget-object v2, p0, Lcom/android/camera/Camera;->mPreferences:Lcom/android/camera/ComboPreferences;

    iget v3, p0, Lcom/android/camera/Camera;->mCameraId:I

    invoke-virtual {v2, p0, v3}, Lcom/android/camera/ComboPreferences;->setLocalId(Landroid/content/Context;I)V

    iget-object v2, p0, Lcom/android/camera/Camera;->mPreferences:Lcom/android/camera/ComboPreferences;

    invoke-virtual {v2}, Lcom/android/camera/ComboPreferences;->getLocal()Landroid/content/SharedPreferences;

    move-result-object v2

    invoke-static {v2}, Lcom/android/camera/CameraSettings;->upgradeLocalPreferences(Landroid/content/SharedPreferences;)V

    iput-object v5, p0, Lcom/android/camera/Camera;->mPreferenceGroup:Lcom/android/camera/PreferenceGroup;

    iget-object v2, p0, Lcom/android/camera/Camera;->mFocusManager:Lcom/android/camera/FocusManager;

    if-eqz v2, :cond_3

    iget-object v2, p0, Lcom/android/camera/Camera;->mFocusManager:Lcom/android/camera/FocusManager;

    invoke-virtual {v2}, Lcom/android/camera/FocusManager;->removeMessages()V

    iget-object v2, p0, Lcom/android/camera/Camera;->mFocusManager:Lcom/android/camera/FocusManager;

    invoke-virtual {v2}, Lcom/android/camera/FocusManager;->release()V

    iput-object v5, p0, Lcom/android/camera/Camera;->mFocusManager:Lcom/android/camera/FocusManager;

    :cond_3
    new-instance v0, Lcom/android/camera/Camera$CameraStartUpThread;

    invoke-direct {v0, p0, v5}, Lcom/android/camera/Camera$CameraStartUpThread;-><init>(Lcom/android/camera/Camera;Lcom/android/camera/Camera$1;)V

    invoke-virtual {v0}, Ljava/lang/Thread;->start()V

    :try_start_0
    invoke-virtual {v0}, Ljava/lang/Thread;->join()V
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_1
    iget-object v2, p0, Lcom/android/camera/Camera;->mMainHandler:Landroid/os/Handler;

    invoke-virtual {v2, v7}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    sget-boolean v2, Lcom/android/camera/Camera;->LOG:Z

    if-eqz v2, :cond_4

    invoke-static {v7}, Lcom/android/camera/Camera;->getMsgLabel(I)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/android/camera/manager/MMProfileManager;->triggersSendMessage(Ljava/lang/String;)V

    :cond_4
    iput v6, p0, Lcom/android/camera/Camera;->mPendingSwitchCameraId:I

    invoke-direct {p0}, Lcom/android/camera/Camera;->refreshModeRelated()V

    invoke-virtual {p0}, Lcom/android/camera/Camera;->restoreViewState()V

    sget-boolean v2, Lcom/android/camera/Camera;->LOG:Z

    if-eqz v2, :cond_5

    const-string v2, "Camera"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "switchCamera() end camera id="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget v4, p0, Lcom/android/camera/Camera;->mCameraId:I

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/android/camera/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    :cond_5
    invoke-static {}, Lcom/android/camera/manager/MMProfileManager;->stopProfileSwitchCamera()V

    goto :goto_0

    :catch_0
    move-exception v1

    invoke-virtual {v1}, Ljava/lang/Throwable;->printStackTrace()V

    goto :goto_1
.end method

.method private uninstallIntentFilter()V
    .locals 1

    iget-object v0, p0, Lcom/android/camera/Camera;->mReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {p0, v0}, Landroid/content/ContextWrapper;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    return-void
.end method

.method private updateCameraScreenNailSize(IILandroid/hardware/Camera$Size;)V
    .locals 6
    .param p1    # I
    .param p2    # I
    .param p3    # Landroid/hardware/Camera$Size;

    iget v2, p3, Landroid/hardware/Camera$Size;->width:I

    iget v0, p3, Landroid/hardware/Camera$Size;->height:I

    iget v3, p0, Lcom/android/camera/Camera;->mCameraDisplayOrientation:I

    rem-int/lit16 v3, v3, 0xb4

    if-eqz v3, :cond_0

    move v1, v2

    move v2, v0

    move v0, v1

    :cond_0
    if-ne p1, v2, :cond_1

    if-eq p2, v0, :cond_2

    :cond_1
    iget-object v3, p0, Lcom/android/camera/ActivityBase;->mCameraScreenNail:Lcom/android/camera/CameraScreenNail;

    invoke-virtual {v3, v2, v0}, Lcom/android/gallery3d/ui/SurfaceTextureScreenNail;->setSize(II)V

    invoke-virtual {p0}, Lcom/android/camera/ActivityBase;->notifyScreenNailChanged()V

    :cond_2
    sget-boolean v3, Lcom/android/camera/Camera;->LOG:Z

    if-eqz v3, :cond_3

    const-string v3, "Camera"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "updateCameraScreenNailSize("

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ", "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ", "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-static {p3}, Lcom/android/camera/SettingUtils;->buildSize(Landroid/hardware/Camera$Size;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ")"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/android/camera/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    :cond_3
    return-void
.end method

.method private updateFocusAndFace()V
    .locals 5

    const/4 v1, 0x1

    const/4 v2, 0x0

    iget-object v3, p0, Lcom/android/camera/Camera;->mFaceView:Lcom/android/camera/ui/FaceView;

    if-eqz v3, :cond_0

    iget-object v3, p0, Lcom/android/camera/Camera;->mFaceView:Lcom/android/camera/ui/FaceView;

    invoke-virtual {v3}, Lcom/android/camera/ui/FaceView;->clear()V

    iget-object v3, p0, Lcom/android/camera/Camera;->mFaceView:Lcom/android/camera/ui/FaceView;

    invoke-virtual {v3, v2}, Landroid/view/View;->setVisibility(I)V

    iget-object v3, p0, Lcom/android/camera/Camera;->mFaceView:Lcom/android/camera/ui/FaceView;

    iget v4, p0, Lcom/android/camera/Camera;->mDisplayOrientation:I

    invoke-virtual {v3, v4}, Lcom/android/camera/ui/FaceView;->setDisplayOrientation(I)V

    invoke-static {}, Lcom/android/camera/CameraHolder;->instance()Lcom/android/camera/CameraHolder;

    move-result-object v3

    invoke-virtual {v3}, Lcom/android/camera/CameraHolder;->getCameraInfo()[Landroid/hardware/Camera$CameraInfo;

    move-result-object v3

    iget v4, p0, Lcom/android/camera/Camera;->mCameraId:I

    aget-object v0, v3, v4

    iget-object v3, p0, Lcom/android/camera/Camera;->mFaceView:Lcom/android/camera/ui/FaceView;

    iget v4, v0, Landroid/hardware/Camera$CameraInfo;->facing:I

    if-ne v4, v1, :cond_3

    :goto_0
    invoke-virtual {v3, v1}, Lcom/android/camera/ui/FaceView;->setMirror(Z)V

    iget-object v1, p0, Lcom/android/camera/Camera;->mFaceView:Lcom/android/camera/ui/FaceView;

    invoke-virtual {v1}, Lcom/android/camera/ui/FaceView;->resume()V

    :cond_0
    iget-object v1, p0, Lcom/android/camera/Camera;->mFocusManager:Lcom/android/camera/FocusManager;

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/android/camera/Camera;->mFocusManager:Lcom/android/camera/FocusManager;

    iget-object v2, p0, Lcom/android/camera/Camera;->mFocusAreaIndicator:Lcom/android/camera/ui/RotateLayout;

    invoke-virtual {v1, v2}, Lcom/android/camera/FocusManager;->setFocusAreaIndicator(Landroid/view/View;)V

    :cond_1
    iget-object v1, p0, Lcom/android/camera/Camera;->mFocusManager:Lcom/android/camera/FocusManager;

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/android/camera/Camera;->mFaceView:Lcom/android/camera/ui/FaceView;

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/android/camera/Camera;->mFocusManager:Lcom/android/camera/FocusManager;

    iget-object v2, p0, Lcom/android/camera/Camera;->mFaceView:Lcom/android/camera/ui/FaceView;

    invoke-virtual {v1, v2}, Lcom/android/camera/FocusManager;->setFaceView(Lcom/android/camera/ui/FaceView;)V

    :cond_2
    return-void

    :cond_3
    move v1, v2

    goto :goto_0
.end method

.method private updateSurfaceTexture()V
    .locals 5

    const/4 v4, -0x1

    invoke-static {}, Lcom/android/camera/manager/MMProfileManager;->startProfileUpdateSurfaceTexture()V

    iget-object v3, p0, Lcom/android/camera/Camera;->mParameters:Landroid/hardware/Camera$Parameters;

    invoke-virtual {v3}, Landroid/hardware/Camera$Parameters;->getPreviewSize()Landroid/hardware/Camera$Size;

    move-result-object v2

    iget-object v3, p0, Lcom/android/camera/Camera;->mSurfaceTexture:Landroid/graphics/SurfaceTexture;

    if-nez v3, :cond_0

    invoke-direct {p0, v4, v4, v2}, Lcom/android/camera/Camera;->updateCameraScreenNailSize(IILandroid/hardware/Camera$Size;)V

    iget-object v3, p0, Lcom/android/camera/ActivityBase;->mCameraScreenNail:Lcom/android/camera/CameraScreenNail;

    invoke-virtual {v3}, Lcom/android/camera/CameraScreenNail;->acquireSurfaceTexture()V

    iget-object v3, p0, Lcom/android/camera/ActivityBase;->mCameraScreenNail:Lcom/android/camera/CameraScreenNail;

    invoke-virtual {v3}, Lcom/android/gallery3d/ui/SurfaceTextureScreenNail;->getSurfaceTexture()Landroid/graphics/SurfaceTexture;

    move-result-object v3

    iput-object v3, p0, Lcom/android/camera/Camera;->mSurfaceTexture:Landroid/graphics/SurfaceTexture;

    :goto_0
    invoke-static {}, Lcom/android/camera/manager/MMProfileManager;->stopProfileUpdateSurfaceTexture()V

    return-void

    :cond_0
    iget-object v3, p0, Lcom/android/camera/ActivityBase;->mCameraScreenNail:Lcom/android/camera/CameraScreenNail;

    invoke-virtual {v3}, Lcom/android/gallery3d/ui/SurfaceTextureScreenNail;->getWidth()I

    move-result v1

    iget-object v3, p0, Lcom/android/camera/ActivityBase;->mCameraScreenNail:Lcom/android/camera/CameraScreenNail;

    invoke-virtual {v3}, Lcom/android/gallery3d/ui/SurfaceTextureScreenNail;->getHeight()I

    move-result v0

    invoke-direct {p0, v1, v0, v2}, Lcom/android/camera/Camera;->updateCameraScreenNailSize(IILandroid/hardware/Camera$Size;)V

    goto :goto_0
.end method

.method private waitCameraStartUpThread(Z)V
    .locals 4
    .param p1    # Z

    sget-boolean v1, Lcom/android/camera/Camera;->LOG:Z

    if-eqz v1, :cond_0

    const-string v1, "Camera"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "waitCameraStartUpThread("

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ") begin mCameraStartUpThread="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/android/camera/Camera;->mCameraStartUpThread:Lcom/android/camera/Camera$CameraStartUpThread;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/android/camera/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    :try_start_0
    iget-object v1, p0, Lcom/android/camera/Camera;->mCameraStartUpThread:Lcom/android/camera/Camera$CameraStartUpThread;

    if-eqz v1, :cond_2

    if-eqz p1, :cond_1

    iget-object v1, p0, Lcom/android/camera/Camera;->mCameraStartUpThread:Lcom/android/camera/Camera$CameraStartUpThread;

    invoke-virtual {v1}, Lcom/android/camera/Camera$CameraStartUpThread;->cancel()V

    :cond_1
    iget-object v1, p0, Lcom/android/camera/Camera;->mCameraStartUpThread:Lcom/android/camera/Camera$CameraStartUpThread;

    invoke-virtual {v1}, Ljava/lang/Thread;->join()V

    const/4 v1, 0x0

    iput-object v1, p0, Lcom/android/camera/Camera;->mCameraStartUpThread:Lcom/android/camera/Camera$CameraStartUpThread;
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0

    :cond_2
    :goto_0
    sget-boolean v1, Lcom/android/camera/Camera;->LOG:Z

    if-eqz v1, :cond_3

    const-string v1, "Camera"

    const-string v2, "waitCameraStartUpThread() end"

    invoke-static {v1, v2}, Lcom/android/camera/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    :cond_3
    return-void

    :catch_0
    move-exception v0

    const-string v1, "Camera"

    const-string v2, "waitCameraStartUpThread()"

    invoke-static {v1, v2, v0}, Lcom/android/camera/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0
.end method


# virtual methods
.method public addOnFullScreenChangedListener(Lcom/android/camera/Camera$OnFullScreenChangedListener;)Z
    .locals 1
    .param p1    # Lcom/android/camera/Camera$OnFullScreenChangedListener;

    iget-object v0, p0, Lcom/android/camera/Camera;->mFullScreenListeners:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/android/camera/Camera;->mFullScreenListeners:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    move-result v0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public addOnOrientationListener(Lcom/android/camera/Camera$OnOrientationListener;)Z
    .locals 1
    .param p1    # Lcom/android/camera/Camera$OnOrientationListener;

    iget-object v0, p0, Lcom/android/camera/Camera;->mOrientationListeners:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/android/camera/Camera;->mOrientationListeners:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    move-result v0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public addOnParametersReadyListener(Lcom/android/camera/Camera$OnParametersReadyListener;)Z
    .locals 1
    .param p1    # Lcom/android/camera/Camera$OnParametersReadyListener;

    iget-object v0, p0, Lcom/android/camera/Camera;->mParametersListeners:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/android/camera/Camera;->mParametersListeners:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    move-result v0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public addOnPreferenceReadyListener(Lcom/android/camera/Camera$OnPreferenceReadyListener;)Z
    .locals 1
    .param p1    # Lcom/android/camera/Camera$OnPreferenceReadyListener;

    iget-object v0, p0, Lcom/android/camera/Camera;->mPreferenceListeners:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/android/camera/Camera;->mPreferenceListeners:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    move-result v0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public addResumable(Lcom/android/camera/Camera$Resumable;)Z
    .locals 1
    .param p1    # Lcom/android/camera/Camera$Resumable;

    iget-object v0, p0, Lcom/android/camera/Camera;->mResumables:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/android/camera/Camera;->mResumables:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    move-result v0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public addView(Landroid/view/View;)V
    .locals 1
    .param p1    # Landroid/view/View;

    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, Lcom/android/camera/Camera;->addView(Landroid/view/View;I)V

    return-void
.end method

.method public addView(Landroid/view/View;I)V
    .locals 1
    .param p1    # Landroid/view/View;
    .param p2    # I

    invoke-direct {p0, p2}, Lcom/android/camera/Camera;->getViewLayer(I)Landroid/view/ViewGroup;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {v0, p1}, Landroid/view/ViewGroup;->addView(Landroid/view/View;)V

    :cond_0
    return-void
.end method

.method public addViewManager(Lcom/android/camera/manager/ViewManager;)Z
    .locals 1
    .param p1    # Lcom/android/camera/manager/ViewManager;

    iget-object v0, p0, Lcom/android/camera/Camera;->mViewManagers:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/android/camera/Camera;->mViewManagers:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    move-result v0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public animateCapture()V
    .locals 5

    const/4 v1, 0x1

    const/4 v2, 0x0

    iget-object v3, p0, Lcom/android/camera/Camera;->mModePicker:Lcom/android/camera/manager/ModePicker;

    iget-object v4, p0, Lcom/android/camera/Camera;->mCameraActor:Lcom/android/camera/actor/CameraActor;

    invoke-virtual {v4}, Lcom/android/camera/actor/CameraActor;->getMode()I

    move-result v4

    invoke-virtual {v3, v4}, Lcom/android/camera/manager/ModePicker;->getModeIndex(I)I

    move-result v3

    const/16 v4, 0x9

    if-ne v3, v4, :cond_0

    iget-object v3, p0, Lcom/android/camera/ActivityBase;->mCameraScreenNail:Lcom/android/camera/CameraScreenNail;

    invoke-virtual {v3}, Lcom/android/camera/CameraScreenNail;->copyOriginSizeTexture()V

    :cond_0
    invoke-virtual {p0}, Landroid/content/ContextWrapper;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v3

    const-string v4, "accelerometer_rotation"

    invoke-static {v3, v4, v2}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v3

    if-eq v1, v3, :cond_2

    :goto_0
    const/4 v0, 0x0

    if-eqz v1, :cond_3

    iget-object v2, p0, Lcom/android/camera/ActivityBase;->mCameraScreenNail:Lcom/android/camera/CameraScreenNail;

    invoke-virtual {v2, v0}, Lcom/android/camera/CameraScreenNail;->animateCapture(I)V

    :goto_1
    sget-boolean v2, Lcom/android/camera/Camera;->LOG:Z

    if-eqz v2, :cond_1

    const-string v2, "Camera"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "animateCapture() rotation="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ", mDisplayRotation="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget v4, p0, Lcom/android/camera/Camera;->mDisplayRotation:I

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ", mOrientationCompensation="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget v4, p0, Lcom/android/camera/Camera;->mOrientationCompensation:I

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ", rotationLocked="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/android/camera/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    :cond_1
    return-void

    :cond_2
    move v1, v2

    goto :goto_0

    :cond_3
    iget v2, p0, Lcom/android/camera/Camera;->mOrientationCompensation:I

    iget v3, p0, Lcom/android/camera/Camera;->mDisplayRotation:I

    sub-int/2addr v2, v3

    add-int/lit16 v2, v2, 0x168

    rem-int/lit16 v0, v2, 0x168

    iget-object v2, p0, Lcom/android/camera/ActivityBase;->mCameraScreenNail:Lcom/android/camera/CameraScreenNail;

    invoke-virtual {v2, v0}, Lcom/android/camera/CameraScreenNail;->animateCapture(I)V

    goto :goto_1
.end method

.method public applyContinousCallback()V
    .locals 3

    sget-boolean v0, Lcom/android/camera/Camera;->LOG:Z

    if-eqz v0, :cond_0

    const-string v0, "Camera"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "applyContinousCallback() mCameraDevice="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/android/camera/Camera;->mCameraDevice:Lcom/android/camera/CameraManager$CameraProxy;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/android/camera/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    iget-object v0, p0, Lcom/android/camera/Camera;->mCameraDevice:Lcom/android/camera/CameraManager$CameraProxy;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/android/camera/Camera;->mFocusManager:Lcom/android/camera/FocusManager;

    invoke-virtual {v0}, Lcom/android/camera/FocusManager;->getContinousFocusSupported()Z

    move-result v0

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/android/camera/Camera;->mCameraDevice:Lcom/android/camera/CameraManager$CameraProxy;

    iget-object v1, p0, Lcom/android/camera/Camera;->mCameraActor:Lcom/android/camera/actor/CameraActor;

    invoke-virtual {v1}, Lcom/android/camera/actor/CameraActor;->getAutoFocusMoveCallback()Landroid/hardware/Camera$AutoFocusMoveCallback;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/android/camera/CameraManager$CameraProxy;->setAutoFocusMoveCallback(Landroid/hardware/Camera$AutoFocusMoveCallback;)V

    :cond_1
    :goto_0
    return-void

    :cond_2
    iget-object v0, p0, Lcom/android/camera/Camera;->mCameraDevice:Lcom/android/camera/CameraManager$CameraProxy;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/android/camera/CameraManager$CameraProxy;->setAutoFocusMoveCallback(Landroid/hardware/Camera$AutoFocusMoveCallback;)V

    goto :goto_0
.end method

.method public applyContinousShot()V
    .locals 1

    new-instance v0, Lcom/android/camera/Camera$10;

    invoke-direct {v0, p0}, Lcom/android/camera/Camera$10;-><init>(Lcom/android/camera/Camera;)V

    invoke-virtual {p0, v0}, Lcom/android/camera/Camera;->lockRun(Ljava/lang/Runnable;)V

    return-void
.end method

.method public applyParameterForFocus(Z)V
    .locals 1
    .param p1    # Z

    new-instance v0, Lcom/android/camera/Camera$12;

    invoke-direct {v0, p0, p1}, Lcom/android/camera/Camera$12;-><init>(Lcom/android/camera/Camera;Z)V

    invoke-virtual {p0, v0}, Lcom/android/camera/Camera;->lockRun(Ljava/lang/Runnable;)V

    return-void
.end method

.method public applyParametersToServer()V
    .locals 3

    iget-object v0, p0, Lcom/android/camera/Camera;->mCameraDevice:Lcom/android/camera/CameraManager$CameraProxy;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/camera/Camera;->mParameters:Landroid/hardware/Camera$Parameters;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/camera/Camera;->mCameraDevice:Lcom/android/camera/CameraManager$CameraProxy;

    iget-object v1, p0, Lcom/android/camera/Camera;->mParameters:Landroid/hardware/Camera$Parameters;

    invoke-virtual {v0, v1}, Lcom/android/camera/CameraManager$CameraProxy;->setParameters(Landroid/hardware/Camera$Parameters;)V

    :cond_0
    sget-boolean v0, Lcom/android/camera/Camera;->LOG:Z

    if-eqz v0, :cond_1

    const-string v0, "Camera"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "applyParameterToServer() mParameters="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/android/camera/Camera;->mParameters:Landroid/hardware/Camera$Parameters;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", mCameraDevice="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/android/camera/Camera;->mCameraDevice:Lcom/android/camera/CameraManager$CameraProxy;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/android/camera/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    :cond_1
    return-void
.end method

.method public backToLastMode()V
    .locals 5

    const/4 v4, 0x0

    sget-boolean v1, Lcom/android/camera/Camera;->LOG:Z

    if-eqz v1, :cond_0

    const-string v1, "Camera"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "backToLastMode() mLastMode="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget v3, p0, Lcom/android/camera/Camera;->mLastMode:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/android/camera/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    invoke-virtual {p0}, Lcom/android/camera/Camera;->getCameraId()I

    move-result v1

    iget v2, p0, Lcom/android/camera/Camera;->mLastMode:I

    invoke-static {p0, v1, v2}, Lcom/android/camera/ModeChecker;->getModePickerVisible(Lcom/android/camera/Camera;II)Z

    move-result v0

    if-nez v0, :cond_1

    iput v4, p0, Lcom/android/camera/Camera;->mLastMode:I

    :cond_1
    invoke-direct {p0, v4}, Lcom/android/camera/Camera;->waitCameraStartUpThread(Z)V

    iget-object v1, p0, Lcom/android/camera/Camera;->mModePicker:Lcom/android/camera/manager/ModePicker;

    iget v2, p0, Lcom/android/camera/Camera;->mLastMode:I

    invoke-virtual {v1, v2}, Lcom/android/camera/manager/ModePicker;->setCurrentMode(I)V

    return-void
.end method

.method public cancelContinuousShot()V
    .locals 1

    new-instance v0, Lcom/android/camera/Camera$11;

    invoke-direct {v0, p0}, Lcom/android/camera/Camera$11;-><init>(Lcom/android/camera/Camera;)V

    invoke-virtual {p0, v0}, Lcom/android/camera/Camera;->lockRun(Ljava/lang/Runnable;)V

    return-void
.end method

.method public collapseViewManager(Z)Z
    .locals 6
    .param p1    # Z

    const/4 v0, 0x0

    iget-object v3, p0, Lcom/android/camera/Camera;->mRotateDialog:Lcom/android/camera/manager/RotateDialog;

    invoke-virtual {v3}, Lcom/android/camera/manager/ViewManager;->isShowing()Z

    move-result v3

    if-eqz v3, :cond_2

    if-nez p1, :cond_2

    iget-object v3, p0, Lcom/android/camera/Camera;->mRotateDialog:Lcom/android/camera/manager/RotateDialog;

    invoke-virtual {v3}, Lcom/android/camera/manager/ViewManager;->hide()V

    const/4 v0, 0x1

    :cond_0
    :goto_0
    sget-boolean v3, Lcom/android/camera/Camera;->LOG:Z

    if-eqz v3, :cond_1

    const-string v3, "Camera"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "collapseViewManager("

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ") return "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/android/camera/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    :cond_1
    return v0

    :cond_2
    iget-object v3, p0, Lcom/android/camera/Camera;->mViewManagers:Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_3
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/android/camera/manager/ViewManager;

    invoke-virtual {v2, p1}, Lcom/android/camera/manager/ViewManager;->collapse(Z)Z

    move-result v3

    if-nez v3, :cond_4

    if-eqz v0, :cond_5

    :cond_4
    const/4 v0, 0x1

    :goto_1
    if-nez p1, :cond_3

    if-eqz v0, :cond_3

    goto :goto_0

    :cond_5
    const/4 v0, 0x0

    goto :goto_1
.end method

.method public dismissAlertDialog()V
    .locals 1

    iget-object v0, p0, Lcom/android/camera/Camera;->mRotateDialog:Lcom/android/camera/manager/RotateDialog;

    invoke-virtual {v0}, Lcom/android/camera/manager/ViewManager;->hide()V

    return-void
.end method

.method public dismissInfo()V
    .locals 2

    sget-boolean v0, Lcom/android/camera/Camera;->LOG:Z

    if-eqz v0, :cond_0

    const-string v0, "Camera"

    const-string v1, "dismissInfo()"

    invoke-static {v0, v1}, Lcom/android/camera/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    iget-object v0, p0, Lcom/android/camera/Camera;->mMainHandler:Landroid/os/Handler;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeMessages(I)V

    invoke-direct {p0}, Lcom/android/camera/Camera;->doShowIndicator()V

    return-void
.end method

.method public dismissProgress()V
    .locals 1

    iget-object v0, p0, Lcom/android/camera/Camera;->mRotateProgress:Lcom/android/camera/manager/RotateProgress;

    invoke-virtual {v0}, Lcom/android/camera/manager/ViewManager;->hide()V

    invoke-virtual {p0}, Lcom/android/camera/Camera;->restoreViewState()V

    return-void
.end method

.method public effectsActive()Z
    .locals 4

    iget v1, p0, Lcom/android/camera/Camera;->mLastEffectType:I

    if-eqz v1, :cond_1

    const/4 v0, 0x1

    :goto_0
    sget-boolean v1, Lcom/android/camera/Camera;->LOG:Z

    if-eqz v1, :cond_0

    const-string v1, "Camera"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "effectsActive() mLastEffectType="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget v3, p0, Lcom/android/camera/Camera;->mLastEffectType:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ", return "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/android/camera/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public fetchParametersFromServer()Landroid/hardware/Camera$Parameters;
    .locals 3

    sget-boolean v0, Lcom/android/camera/Camera;->LOG:Z

    if-eqz v0, :cond_0

    const-string v0, "Camera"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "fetchParameterFromServer() mParameters="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/android/camera/Camera;->mParameters:Landroid/hardware/Camera$Parameters;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", mCameraDevice="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/android/camera/Camera;->mCameraDevice:Lcom/android/camera/CameraManager$CameraProxy;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/android/camera/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    iget-object v0, p0, Lcom/android/camera/Camera;->mCameraDevice:Lcom/android/camera/CameraManager$CameraProxy;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/android/camera/Camera;->mCameraDevice:Lcom/android/camera/CameraManager$CameraProxy;

    invoke-virtual {v0}, Lcom/android/camera/CameraManager$CameraProxy;->getParameters()Landroid/hardware/Camera$Parameters;

    move-result-object v0

    iput-object v0, p0, Lcom/android/camera/Camera;->mParameters:Landroid/hardware/Camera$Parameters;

    :cond_1
    sget-boolean v0, Lcom/android/camera/Camera;->LOG:Z

    if-eqz v0, :cond_2

    const-string v0, "Camera"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "fetchParameterFromServer() new mParameters="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/android/camera/Camera;->mParameters:Landroid/hardware/Camera$Parameters;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/android/camera/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    :cond_2
    iget-object v0, p0, Lcom/android/camera/Camera;->mParameters:Landroid/hardware/Camera$Parameters;

    return-object v0
.end method

.method public fetchProfile(II)Landroid/media/CamcorderProfile;
    .locals 3
    .param p1    # I
    .param p2    # I

    sget-boolean v0, Lcom/android/camera/Camera;->LOG:Z

    if-eqz v0, :cond_0

    const-string v0, "Camera"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "fetchProfile("

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ")"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/android/camera/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    iput p2, p0, Lcom/android/camera/Camera;->mTimelapseMs:I

    iget v0, p0, Lcom/android/camera/Camera;->mTimelapseMs:I

    if-eqz v0, :cond_1

    add-int/lit16 p1, p1, 0x3e8

    :cond_1
    iput p1, p0, Lcom/android/camera/Camera;->mQualityId:I

    iget v0, p0, Lcom/android/camera/Camera;->mCameraId:I

    invoke-static {v0, p1}, Lcom/mediatek/camera/FrameworksClassFactory;->getMtkCamcorderProfile(II)Landroid/media/CamcorderProfile;

    move-result-object v0

    iput-object v0, p0, Lcom/android/camera/Camera;->mProfile:Landroid/media/CamcorderProfile;

    invoke-static {}, Lcom/mediatek/camera/ext/ExtensionHelper;->getFeatureExtension()Lcom/mediatek/camera/ext/IFeatureExtension;

    move-result-object v0

    iget-object v1, p0, Lcom/android/camera/Camera;->mProfile:Landroid/media/CamcorderProfile;

    invoke-interface {v0, p1, v1}, Lcom/mediatek/camera/ext/IFeatureExtension;->checkMMSVideoCodec(ILandroid/media/CamcorderProfile;)V

    sget-boolean v0, Lcom/android/camera/Camera;->LOG:Z

    if-eqz v0, :cond_2

    const-string v0, "Camera"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "fetchProfile() mProfile.videoFrameRate="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/android/camera/Camera;->mProfile:Landroid/media/CamcorderProfile;

    iget v2, v2, Landroid/media/CamcorderProfile;->videoFrameRate:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", mProfile.videoFrameWidth="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/android/camera/Camera;->mProfile:Landroid/media/CamcorderProfile;

    iget v2, v2, Landroid/media/CamcorderProfile;->videoFrameWidth:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", mProfile.videoFrameHeight="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/android/camera/Camera;->mProfile:Landroid/media/CamcorderProfile;

    iget v2, v2, Landroid/media/CamcorderProfile;->videoFrameHeight:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", mProfile.audioBitRate="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/android/camera/Camera;->mProfile:Landroid/media/CamcorderProfile;

    iget v2, v2, Landroid/media/CamcorderProfile;->audioBitRate:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", mProfile.videoBitRate="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/android/camera/Camera;->mProfile:Landroid/media/CamcorderProfile;

    iget v2, v2, Landroid/media/CamcorderProfile;->videoBitRate:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", mProfile.quality="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/android/camera/Camera;->mProfile:Landroid/media/CamcorderProfile;

    iget v2, v2, Landroid/media/CamcorderProfile;->quality:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", mProfile.duration="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/android/camera/Camera;->mProfile:Landroid/media/CamcorderProfile;

    iget v2, v2, Landroid/media/CamcorderProfile;->duration:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/android/camera/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    :cond_2
    iget-object v0, p0, Lcom/android/camera/Camera;->mProfile:Landroid/media/CamcorderProfile;

    invoke-direct {p0, v0}, Lcom/android/camera/Camera;->limitPreviewByIntent(Landroid/media/CamcorderProfile;)V

    iget-object v0, p0, Lcom/android/camera/Camera;->mProfile:Landroid/media/CamcorderProfile;

    return-object v0
.end method

.method public getAudioMode()Ljava/lang/String;
    .locals 2

    iget-object v0, p0, Lcom/android/camera/Camera;->mSettingChecker:Lcom/android/camera/SettingChecker;

    const/16 v1, 0x11

    invoke-virtual {v0, v1}, Lcom/android/camera/SettingChecker;->getPreferenceValue(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getCameraActor()Lcom/android/camera/actor/CameraActor;
    .locals 1

    iget-object v0, p0, Lcom/android/camera/Camera;->mCameraActor:Lcom/android/camera/actor/CameraActor;

    return-object v0
.end method

.method public getCameraCount()I
    .locals 1

    iget v0, p0, Lcom/android/camera/Camera;->mNumberOfCameras:I

    return v0
.end method

.method public getCameraDevice()Lcom/android/camera/CameraManager$CameraProxy;
    .locals 1

    iget-object v0, p0, Lcom/android/camera/Camera;->mCameraDevice:Lcom/android/camera/CameraManager$CameraProxy;

    return-object v0
.end method

.method public getCameraDisplayOrientation()I
    .locals 1

    iget v0, p0, Lcom/android/camera/Camera;->mCameraDisplayOrientation:I

    return v0
.end method

.method public getCameraId()I
    .locals 1

    iget v0, p0, Lcom/android/camera/Camera;->mCameraId:I

    return v0
.end method

.method public getCameraScreenNailHeight()I
    .locals 1

    iget-object v0, p0, Lcom/android/camera/ActivityBase;->mCameraScreenNail:Lcom/android/camera/CameraScreenNail;

    invoke-virtual {v0}, Lcom/android/gallery3d/ui/SurfaceTextureScreenNail;->getHeight()I

    move-result v0

    return v0
.end method

.method public getCameraScreenNailWidth()I
    .locals 1

    iget-object v0, p0, Lcom/android/camera/ActivityBase;->mCameraScreenNail:Lcom/android/camera/CameraScreenNail;

    invoke-virtual {v0}, Lcom/android/gallery3d/ui/SurfaceTextureScreenNail;->getWidth()I

    move-result v0

    return v0
.end method

.method public getCameraSettings()Lcom/android/camera/CameraSettings;
    .locals 1

    iget-object v0, p0, Lcom/android/camera/Camera;->mCameraSettings:Lcom/android/camera/CameraSettings;

    return-object v0
.end method

.method public getCameraState()I
    .locals 1

    iget v0, p0, Lcom/android/camera/Camera;->mCameraState:I

    return v0
.end method

.method public getCropValue()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/android/camera/Camera;->mCropValue:Ljava/lang/String;

    return-object v0
.end method

.method public getCurrentMode()I
    .locals 1

    iget-object v0, p0, Lcom/android/camera/Camera;->mCameraActor:Lcom/android/camera/actor/CameraActor;

    invoke-virtual {v0}, Lcom/android/camera/actor/CameraActor;->getMode()I

    move-result v0

    return v0
.end method

.method public getDisplayOrientation()I
    .locals 1

    iget v0, p0, Lcom/android/camera/Camera;->mDisplayOrientation:I

    return v0
.end method

.method public getDisplayRotation()I
    .locals 1

    iget v0, p0, Lcom/android/camera/Camera;->mDisplayRotation:I

    return v0
.end method

.method public getEffectParameter()Ljava/lang/Object;
    .locals 1

    iget-object v0, p0, Lcom/android/camera/Camera;->mLastEffectParameter:Ljava/lang/Object;

    return-object v0
.end method

.method public getEffectType()I
    .locals 1

    iget v0, p0, Lcom/android/camera/Camera;->mLastEffectType:I

    return v0
.end method

.method public getFaceView()Lcom/android/camera/ui/FaceView;
    .locals 1

    iget-object v0, p0, Lcom/android/camera/Camera;->mFaceView:Lcom/android/camera/ui/FaceView;

    return-object v0
.end method

.method public getFileSaver()Lcom/android/camera/FileSaver;
    .locals 1

    iget-object v0, p0, Lcom/android/camera/Camera;->mFileSaver:Lcom/android/camera/FileSaver;

    return-object v0
.end method

.method public getFocusManager()Lcom/android/camera/FocusManager;
    .locals 1

    iget-object v0, p0, Lcom/android/camera/Camera;->mFocusManager:Lcom/android/camera/FocusManager;

    return-object v0
.end method

.method public getIndicatorManager()Lcom/android/camera/manager/IndicatorManager;
    .locals 1

    iget-object v0, p0, Lcom/android/camera/Camera;->mIndicatorManager:Lcom/android/camera/manager/IndicatorManager;

    return-object v0
.end method

.method public getLimitedDuration()I
    .locals 1

    iget v0, p0, Lcom/android/camera/Camera;->mLimitedDuration:I

    return v0
.end method

.method public getLimitedSize()J
    .locals 2

    iget-wide v0, p0, Lcom/android/camera/Camera;->mLimitedSize:J

    return-wide v0
.end method

.method public getListPreference(I)Lcom/android/camera/ListPreference;
    .locals 1
    .param p1    # I

    iget-object v0, p0, Lcom/android/camera/Camera;->mSettingChecker:Lcom/android/camera/SettingChecker;

    invoke-virtual {v0, p1}, Lcom/android/camera/SettingChecker;->getListPreference(I)Lcom/android/camera/ListPreference;

    move-result-object v0

    return-object v0
.end method

.method public getListPreference(Ljava/lang/String;)Lcom/android/camera/ListPreference;
    .locals 2
    .param p1    # Ljava/lang/String;

    sget-object v1, Lcom/android/camera/SettingChecker;->KEYS_FOR_SETTING:[Ljava/lang/String;

    invoke-static {v1, p1}, Lcom/android/camera/SettingUtils;->index([Ljava/lang/String;Ljava/lang/String;)I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/android/camera/Camera;->getListPreference(I)Lcom/android/camera/ListPreference;

    move-result-object v1

    return-object v1
.end method

.method public getLocationManager()Lcom/android/camera/LocationManager;
    .locals 1

    iget-object v0, p0, Lcom/android/camera/Camera;->mLocationManager:Lcom/android/camera/LocationManager;

    return-object v0
.end method

.method public getMicrophone()Z
    .locals 3

    const-string v0, "on"

    iget-object v1, p0, Lcom/android/camera/Camera;->mSettingChecker:Lcom/android/camera/SettingChecker;

    const/16 v2, 0x10

    invoke-virtual {v1, v2}, Lcom/android/camera/SettingChecker;->getPreferenceValue(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public getOrientationCompensation()I
    .locals 1

    iget v0, p0, Lcom/android/camera/Camera;->mOrientationCompensation:I

    return v0
.end method

.method public getOrietation()I
    .locals 1

    iget v0, p0, Lcom/android/camera/Camera;->mOrientation:I

    return v0
.end method

.method public getParameters()Landroid/hardware/Camera$Parameters;
    .locals 3

    sget-boolean v0, Lcom/android/camera/Camera;->LOG:Z

    if-eqz v0, :cond_0

    const-string v0, "Camera"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "getParameters() return "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/android/camera/Camera;->mParameters:Landroid/hardware/Camera$Parameters;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/android/camera/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    iget-object v0, p0, Lcom/android/camera/Camera;->mParameters:Landroid/hardware/Camera$Parameters;

    return-object v0
.end method

.method public getPickerManager()Lcom/android/camera/manager/PickerManager;
    .locals 1

    iget-object v0, p0, Lcom/android/camera/Camera;->mPickerManager:Lcom/android/camera/manager/PickerManager;

    return-object v0
.end method

.method public getPreferenceGroup()Lcom/android/camera/PreferenceGroup;
    .locals 1

    iget-object v0, p0, Lcom/android/camera/Camera;->mPreferenceGroup:Lcom/android/camera/PreferenceGroup;

    return-object v0
.end method

.method public getPreferences()Lcom/android/camera/ComboPreferences;
    .locals 1

    iget-object v0, p0, Lcom/android/camera/Camera;->mPreferences:Lcom/android/camera/ComboPreferences;

    return-object v0
.end method

.method public getPreviewFrameHeight()I
    .locals 3

    sget-boolean v0, Lcom/android/camera/Camera;->LOG:Z

    if-eqz v0, :cond_0

    const-string v0, "Camera"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "getPreviewFrameHeight() return , real="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/android/camera/Camera;->mPreviewFrameLayout:Lcom/android/camera/ui/PreviewFrameLayout;

    invoke-virtual {v2}, Landroid/view/View;->getHeight()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/android/camera/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    iget v0, p0, Lcom/android/camera/Camera;->mPreviewFrameHeight:I

    return v0
.end method

.method public getPreviewFrameWidth()I
    .locals 3

    sget-boolean v0, Lcom/android/camera/Camera;->LOG:Z

    if-eqz v0, :cond_0

    const-string v0, "Camera"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "getPreviewFrameWidth() return "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/android/camera/Camera;->mPreviewFrameHeight:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", real="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/android/camera/Camera;->mPreviewFrameLayout:Lcom/android/camera/ui/PreviewFrameLayout;

    invoke-virtual {v2}, Landroid/view/View;->getWidth()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/android/camera/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    iget v0, p0, Lcom/android/camera/Camera;->mPreviewFrameWidth:I

    return v0
.end method

.method public getProfile()Landroid/media/CamcorderProfile;
    .locals 1

    iget-object v0, p0, Lcom/android/camera/Camera;->mProfile:Landroid/media/CamcorderProfile;

    return-object v0
.end method

.method public getQualityId()I
    .locals 1

    iget v0, p0, Lcom/android/camera/Camera;->mQualityId:I

    return v0
.end method

.method public getRemainingManager()Lcom/android/camera/manager/RemainingManager;
    .locals 1

    iget-object v0, p0, Lcom/android/camera/Camera;->mRemainingManager:Lcom/android/camera/manager/RemainingManager;

    return-object v0
.end method

.method public getReviewManager()Lcom/android/camera/manager/ReviewManager;
    .locals 1

    iget-object v0, p0, Lcom/android/camera/Camera;->mReviewManager:Lcom/android/camera/manager/ReviewManager;

    return-object v0
.end method

.method public getSaveUri()Landroid/net/Uri;
    .locals 1

    iget-object v0, p0, Lcom/android/camera/Camera;->mSaveUri:Landroid/net/Uri;

    return-object v0
.end method

.method public getSelfTimer()Ljava/lang/String;
    .locals 4

    iget-object v1, p0, Lcom/android/camera/Camera;->mSettingChecker:Lcom/android/camera/SettingChecker;

    const/4 v2, 0x7

    invoke-virtual {v1, v2}, Lcom/android/camera/SettingChecker;->getSettingCurrentValue(I)Ljava/lang/String;

    move-result-object v0

    sget-boolean v1, Lcom/android/camera/Camera;->LOG:Z

    if-eqz v1, :cond_0

    const-string v1, "Camera"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "getSelfTimer() return "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/android/camera/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    return-object v0
.end method

.method public getSettingChecker()Lcom/android/camera/SettingChecker;
    .locals 1

    iget-object v0, p0, Lcom/android/camera/Camera;->mSettingChecker:Lcom/android/camera/SettingChecker;

    return-object v0
.end method

.method public getSettingManager()Lcom/android/camera/manager/SettingManager;
    .locals 1

    iget-object v0, p0, Lcom/android/camera/Camera;->mSettingManager:Lcom/android/camera/manager/SettingManager;

    return-object v0
.end method

.method public getShutterManager()Lcom/android/camera/manager/ShutterManager;
    .locals 1

    iget-object v0, p0, Lcom/android/camera/Camera;->mShutterManager:Lcom/android/camera/manager/ShutterManager;

    return-object v0
.end method

.method public getSurfaceTexture()Landroid/graphics/SurfaceTexture;
    .locals 1

    iget-object v0, p0, Lcom/android/camera/Camera;->mSurfaceTexture:Landroid/graphics/SurfaceTexture;

    return-object v0
.end method

.method public getSurfaceTextureReady()Z
    .locals 1

    iget-boolean v0, p0, Lcom/android/camera/Camera;->mSurfaceTextureReady:Z

    return v0
.end method

.method public getThumbnailManager()Lcom/android/camera/manager/ThumbnailManager;
    .locals 1

    iget-object v0, p0, Lcom/android/camera/Camera;->mThumbnailManager:Lcom/android/camera/manager/ThumbnailManager;

    return-object v0
.end method

.method public getTimelapseMs()I
    .locals 1

    iget v0, p0, Lcom/android/camera/Camera;->mTimelapseMs:I

    return v0
.end method

.method public getViewState()I
    .locals 1

    iget v0, p0, Lcom/android/camera/Camera;->mCurrentViewState:I

    return v0
.end method

.method public getVoiceManager()Lcom/android/camera/VoiceManager;
    .locals 1

    iget-object v0, p0, Lcom/android/camera/Camera;->mVoiceManager:Lcom/android/camera/VoiceManager;

    return-object v0
.end method

.method public getWallpaperPickAspectio()F
    .locals 1

    iget v0, p0, Lcom/android/camera/Camera;->mWallpaperAspectio:F

    return v0
.end method

.method public getWfdManagerLocal()Lcom/android/camera/WfdManagerLocal;
    .locals 1

    iget-object v0, p0, Lcom/android/camera/Camera;->mWfdLocal:Lcom/android/camera/WfdManagerLocal;

    return-object v0
.end method

.method public getZoomManager()Lcom/android/camera/manager/ZoomManager;
    .locals 1

    iget-object v0, p0, Lcom/android/camera/Camera;->mZoomManager:Lcom/android/camera/manager/ZoomManager;

    return-object v0
.end method

.method public hideReview()V
    .locals 1

    iget-object v0, p0, Lcom/android/camera/Camera;->mReviewManager:Lcom/android/camera/manager/ReviewManager;

    invoke-virtual {v0}, Lcom/android/camera/manager/ViewManager;->hide()V

    invoke-virtual {p0}, Lcom/android/camera/Camera;->restoreViewState()V

    return-void
.end method

.method public hideToast()V
    .locals 2

    sget-boolean v0, Lcom/android/camera/Camera;->LOG:Z

    if-eqz v0, :cond_0

    const-string v0, "Camera"

    const-string v1, "hideToast()"

    invoke-static {v0, v1}, Lcom/android/camera/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    iget-object v0, p0, Lcom/android/camera/Camera;->mRotateToast:Lcom/android/camera/manager/OnScreenHint;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/android/camera/Camera;->mRotateToast:Lcom/android/camera/manager/OnScreenHint;

    invoke-virtual {v0}, Lcom/android/camera/manager/OnScreenHint;->cancel()V

    :cond_1
    return-void
.end method

.method public inflate(II)Landroid/view/View;
    .locals 3
    .param p1    # I
    .param p2    # I

    invoke-virtual {p0}, Landroid/app/Activity;->getLayoutInflater()Landroid/view/LayoutInflater;

    move-result-object v0

    invoke-direct {p0, p2}, Lcom/android/camera/Camera;->getViewLayer(I)Landroid/view/ViewGroup;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v0, p1, v1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    return-object v0
.end method

.method public isAcceptFloatingInfo()Z
    .locals 4

    invoke-virtual {p0}, Lcom/android/camera/ActivityBase;->isFullScreen()Z

    move-result v1

    if-eqz v1, :cond_1

    iget-boolean v1, p0, Lcom/android/camera/Camera;->mAcceptFloatingInfo:Z

    if-eqz v1, :cond_1

    const/4 v0, 0x1

    :goto_0
    sget-boolean v1, Lcom/android/camera/Camera;->LOG:Z

    if-eqz v1, :cond_0

    const-string v1, "Camera"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "isAcceptFloatingInfo() isFullScreen()="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {p0}, Lcom/android/camera/ActivityBase;->isFullScreen()Z

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ", mAcceptFloatingInfo="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-boolean v3, p0, Lcom/android/camera/Camera;->mAcceptFloatingInfo:Z

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " return "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/android/camera/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isCameraClosed()Z
    .locals 1

    iget-boolean v0, p0, Lcom/android/camera/Camera;->mCameraOpened:Z

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isCameraIdle()Z
    .locals 4

    const/4 v0, 0x1

    iget v1, p0, Lcom/android/camera/Camera;->mCameraState:I

    if-eq v1, v0, :cond_0

    iget-object v1, p0, Lcom/android/camera/Camera;->mFocusManager:Lcom/android/camera/FocusManager;

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/android/camera/Camera;->mFocusManager:Lcom/android/camera/FocusManager;

    invoke-virtual {v1}, Lcom/android/camera/FocusManager;->isFocusCompleted()Z

    move-result v1

    if-eqz v1, :cond_2

    iget v1, p0, Lcom/android/camera/Camera;->mCameraState:I

    const/4 v2, 0x4

    if-eq v1, v2, :cond_2

    :cond_0
    :goto_0
    sget-boolean v1, Lcom/android/camera/Camera;->LOG:Z

    if-eqz v1, :cond_1

    const-string v1, "Camera"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "isCameraIdle() mCameraState="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget v3, p0, Lcom/android/camera/Camera;->mCameraState:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ", return "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/android/camera/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    :cond_1
    return v0

    :cond_2
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isImageCaptureIntent()Z
    .locals 2

    const/4 v0, 0x1

    iget v1, p0, Lcom/android/camera/Camera;->mPickType:I

    if-ne v0, v1, :cond_0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isNonePickIntent()Z
    .locals 1

    iget v0, p0, Lcom/android/camera/Camera;->mPickType:I

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isNormalViewState()Z
    .locals 3

    sget-boolean v0, Lcom/android/camera/Camera;->LOG:Z

    if-eqz v0, :cond_0

    const-string v0, "Camera"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "isNormalViewState() mCurrentViewState="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/android/camera/Camera;->mCurrentViewState:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/android/camera/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    iget v0, p0, Lcom/android/camera/Camera;->mCurrentViewState:I

    const/4 v1, -0x1

    if-ne v0, v1, :cond_1

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isQuickCapture()Z
    .locals 1

    iget-boolean v0, p0, Lcom/android/camera/Camera;->mQuickCapture:Z

    return v0
.end method

.method public isShowingProgress()Z
    .locals 1

    iget-object v0, p0, Lcom/android/camera/Camera;->mRotateProgress:Lcom/android/camera/manager/RotateProgress;

    invoke-virtual {v0}, Lcom/android/camera/manager/ViewManager;->isShowing()Z

    move-result v0

    return v0
.end method

.method public isStereoMode()Z
    .locals 1

    iget-boolean v0, p0, Lcom/android/camera/Camera;->mStereoMode:Z

    return v0
.end method

.method public isSwitchingCamera()Z
    .locals 3

    sget-boolean v0, Lcom/android/camera/Camera;->LOG:Z

    if-eqz v0, :cond_0

    const-string v0, "Camera"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "isSwitchingCamera() mCurrentViewState="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/android/camera/Camera;->mCurrentViewState:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", mPendingSwitchCameraId="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/android/camera/Camera;->mPendingSwitchCameraId:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/android/camera/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    iget v0, p0, Lcom/android/camera/Camera;->mPendingSwitchCameraId:I

    const/4 v1, -0x1

    if-eq v0, v1, :cond_1

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isVideoCaptureIntent()Z
    .locals 2

    const/4 v0, 0x2

    iget v1, p0, Lcom/android/camera/Camera;->mPickType:I

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isVideoMode()Z
    .locals 3

    sget-boolean v0, Lcom/android/camera/Camera;->LOG:Z

    if-eqz v0, :cond_0

    const-string v0, "Camera"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "isVideoMode() getCurrentMode()="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lcom/android/camera/Camera;->getCurrentMode()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/android/camera/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    const/16 v0, 0x9

    invoke-virtual {p0}, Lcom/android/camera/Camera;->getCurrentMode()I

    move-result v1

    if-eq v0, v1, :cond_1

    const/16 v0, 0x6d

    invoke-virtual {p0}, Lcom/android/camera/Camera;->getCurrentMode()I

    move-result v1

    if-ne v0, v1, :cond_2

    :cond_1
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_2
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isVideoWallPaperIntent()Z
    .locals 2

    const/4 v0, 0x3

    iget v1, p0, Lcom/android/camera/Camera;->mPickType:I

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public keepScreenOn()V
    .locals 2

    sget-boolean v0, Lcom/android/camera/Camera;->LOG:Z

    if-eqz v0, :cond_0

    const-string v0, "Camera"

    const-string v1, "keepScreenOn()"

    invoke-static {v0, v1}, Lcom/android/camera/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    iget-object v0, p0, Lcom/android/camera/Camera;->mMainHandler:Landroid/os/Handler;

    const/4 v1, 0x7

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeMessages(I)V

    invoke-virtual {p0}, Landroid/app/Activity;->getWindow()Landroid/view/Window;

    move-result-object v0

    const/16 v1, 0x80

    invoke-virtual {v0, v1}, Landroid/view/Window;->addFlags(I)V

    return-void
.end method

.method public keepScreenOnAwhile()V
    .locals 4

    const/4 v3, 0x7

    sget-boolean v0, Lcom/android/camera/Camera;->LOG:Z

    if-eqz v0, :cond_0

    const-string v0, "Camera"

    const-string v1, "keepScreenOnAwhile()"

    invoke-static {v0, v1}, Lcom/android/camera/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    iget-object v0, p0, Lcom/android/camera/Camera;->mMainHandler:Landroid/os/Handler;

    invoke-virtual {v0, v3}, Landroid/os/Handler;->removeMessages(I)V

    invoke-virtual {p0}, Landroid/app/Activity;->getWindow()Landroid/view/Window;

    move-result-object v0

    const/16 v1, 0x80

    invoke-virtual {v0, v1}, Landroid/view/Window;->addFlags(I)V

    iget-object v0, p0, Lcom/android/camera/Camera;->mMainHandler:Landroid/os/Handler;

    const-wide/32 v1, 0x1d4c0

    invoke-virtual {v0, v3, v1, v2}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    sget-boolean v0, Lcom/android/camera/Camera;->LOG:Z

    if-eqz v0, :cond_1

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const/16 v1, 0xc

    invoke-static {v1}, Lcom/android/camera/Camera;->getMsgLabel(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", delayed for "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const v1, 0x1d4c0

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/android/camera/manager/MMProfileManager;->triggersSendMessage(Ljava/lang/String;)V

    :cond_1
    return-void
.end method

.method public lockRun(Ljava/lang/Runnable;)V
    .locals 3
    .param p1    # Ljava/lang/Runnable;

    sget-boolean v0, Lcom/android/camera/Camera;->LOG:Z

    if-eqz v0, :cond_0

    const-string v0, "Camera"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "lockRun("

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ") mCameraDevice="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/android/camera/Camera;->mCameraDevice:Lcom/android/camera/CameraManager$CameraProxy;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/android/camera/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    iget-object v0, p0, Lcom/android/camera/Camera;->mCameraDevice:Lcom/android/camera/CameraManager$CameraProxy;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/android/camera/Camera;->mCameraDevice:Lcom/android/camera/CameraManager$CameraProxy;

    invoke-virtual {v0, p1}, Lcom/android/camera/CameraManager$CameraProxy;->lockParametersRun(Ljava/lang/Runnable;)V

    :cond_1
    return-void
.end method

.method public notifyPreferenceChanged()V
    .locals 1

    iget-object v0, p0, Lcom/android/camera/Camera;->mSettingListener:Lcom/android/camera/manager/SettingManager$SettingListener;

    invoke-interface {v0}, Lcom/android/camera/manager/SettingManager$SettingListener;->onSharedPreferenceChanged()V

    iget-object v0, p0, Lcom/android/camera/Camera;->mSettingManager:Lcom/android/camera/manager/SettingManager;

    invoke-virtual {v0}, Lcom/android/camera/manager/ViewManager;->refresh()V

    return-void
.end method

.method protected onActivityResult(IILandroid/content/Intent;)V
    .locals 1
    .param p1    # I
    .param p2    # I
    .param p3    # Landroid/content/Intent;

    invoke-super {p0, p1, p2, p3}, Lcom/android/gallery3d/app/AbstractGalleryActivity;->onActivityResult(IILandroid/content/Intent;)V

    iget-object v0, p0, Lcom/android/camera/Camera;->mCameraActor:Lcom/android/camera/actor/CameraActor;

    invoke-virtual {v0, p1, p2, p3}, Lcom/android/camera/actor/CameraActor;->onActivityResult(IILandroid/content/Intent;)V

    return-void
.end method

.method protected onAfterFullScreeChanged(Z)V
    .locals 5
    .param p1    # Z

    const/4 v4, 0x4

    sget-boolean v0, Lcom/android/camera/Camera;->LOG:Z

    if-eqz v0, :cond_0

    const-string v0, "Camera"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "onAfterFullScreeChanged("

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ")"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/android/camera/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    if-eqz p1, :cond_4

    iget-wide v0, p0, Lcom/android/camera/Camera;->mOnResumeTime:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/android/camera/Camera;->mMainHandler:Landroid/os/Handler;

    invoke-virtual {v0, v4}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    :cond_1
    sget-boolean v0, Lcom/android/camera/Camera;->LOG:Z

    if-eqz v0, :cond_2

    invoke-static {v4}, Lcom/android/camera/Camera;->getMsgLabel(I)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/android/camera/manager/MMProfileManager;->triggersSendMessage(Ljava/lang/String;)V

    :cond_2
    :goto_0
    iget-object v0, p0, Lcom/android/camera/Camera;->mSettingChecker:Lcom/android/camera/SettingChecker;

    if-eqz v0, :cond_3

    new-instance v0, Lcom/android/camera/Camera$4;

    invoke-direct {v0, p0, p1}, Lcom/android/camera/Camera$4;-><init>(Lcom/android/camera/Camera;Z)V

    invoke-virtual {p0, v0}, Lcom/android/camera/Camera;->lockRun(Ljava/lang/Runnable;)V

    :cond_3
    invoke-direct {p0, p1}, Lcom/android/camera/Camera;->notifyOnFullScreenChanged(Z)V

    return-void

    :cond_4
    invoke-virtual {p0}, Lcom/android/camera/Camera;->hideToast()V

    goto :goto_0
.end method

.method public onBackPressed()V
    .locals 4

    sget-boolean v1, Lcom/android/camera/Camera;->LOG:Z

    if-eqz v1, :cond_0

    const-string v1, "Camera"

    const-string v2, "onBackPressed()"

    invoke-static {v1, v2}, Lcom/android/camera/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    iget-boolean v1, p0, Lcom/android/camera/ActivityBase;->mPaused:Z

    if-nez v1, :cond_1

    iget-boolean v1, p0, Lcom/android/camera/Camera;->mForceFinishing:Z

    if-eqz v1, :cond_2

    :cond_1
    :goto_0
    return-void

    :cond_2
    iget-boolean v1, p0, Lcom/android/camera/Camera;->mOpenCameraFail:Z

    if-nez v1, :cond_3

    iget-boolean v1, p0, Lcom/android/camera/Camera;->mCameraDisabled:Z

    if-eqz v1, :cond_4

    :cond_3
    invoke-super {p0}, Lcom/android/gallery3d/app/AbstractGalleryActivity;->onBackPressed()V

    goto :goto_0

    :cond_4
    const/4 v0, 0x0

    iget-object v1, p0, Lcom/android/camera/Camera;->mCameraActor:Lcom/android/camera/actor/CameraActor;

    invoke-virtual {v1}, Lcom/android/camera/actor/CameraActor;->onBackPressed()Z

    move-result v1

    if-nez v1, :cond_5

    const/4 v1, 0x0

    invoke-virtual {p0, v1}, Lcom/android/camera/Camera;->collapseViewManager(Z)Z

    move-result v1

    if-nez v1, :cond_5

    invoke-super {p0}, Lcom/android/gallery3d/app/AbstractGalleryActivity;->onBackPressed()V

    invoke-direct {p0}, Lcom/android/camera/Camera;->clearUserSettings()V

    :cond_5
    const-string v1, "Camera"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "onBackPressed() handle="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/android/camera/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method public onConfigurationChanged(Landroid/content/res/Configuration;)V
    .locals 6
    .param p1    # Landroid/content/res/Configuration;

    const/4 v5, 0x1

    const/4 v4, 0x0

    invoke-static {}, Lcom/android/camera/manager/MMProfileManager;->startProfileCameraOnConfigChange()V

    invoke-super {p0, p1}, Lcom/android/gallery3d/app/AbstractGalleryActivity;->onConfigurationChanged(Landroid/content/res/Configuration;)V

    sget-boolean v1, Lcom/android/camera/Camera;->LOG:Z

    if-eqz v1, :cond_0

    const-string v1, "Camera"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "onConfigurationChanged("

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ")"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/android/camera/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    invoke-direct {p0}, Lcom/android/camera/Camera;->clearFocusAndFace()V

    const/4 v1, 0x0

    iput-object v1, p0, Lcom/android/camera/Camera;->mRotateToast:Lcom/android/camera/manager/OnScreenHint;

    invoke-static {}, Lcom/android/camera/manager/MMProfileManager;->startProfileCameraViewOperation()V

    const v1, 0x7f0b0012

    invoke-virtual {p0, v1}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    invoke-virtual {v0}, Landroid/view/ViewGroup;->removeAllViews()V

    invoke-virtual {p0}, Landroid/app/Activity;->getLayoutInflater()Landroid/view/LayoutInflater;

    move-result-object v1

    const v2, 0x7f040047

    invoke-virtual {v1, v2, v0, v5}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    invoke-virtual {p0}, Landroid/app/Activity;->getLayoutInflater()Landroid/view/LayoutInflater;

    move-result-object v1

    const v2, 0x7f04005f

    invoke-virtual {v1, v2, v0, v5}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    iget-object v1, p0, Lcom/android/camera/Camera;->mViewLayerBottom:Landroid/view/ViewGroup;

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/android/camera/Camera;->mViewLayerBottom:Landroid/view/ViewGroup;

    invoke-virtual {v1}, Landroid/view/ViewGroup;->removeAllViews()V

    :cond_1
    iget-object v1, p0, Lcom/android/camera/Camera;->mViewLayerNormal:Landroid/view/ViewGroup;

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/android/camera/Camera;->mViewLayerNormal:Landroid/view/ViewGroup;

    invoke-virtual {v1}, Landroid/view/ViewGroup;->removeAllViews()V

    :cond_2
    iget-object v1, p0, Lcom/android/camera/Camera;->mViewLayerShutter:Landroid/view/ViewGroup;

    if-eqz v1, :cond_3

    iget-object v1, p0, Lcom/android/camera/Camera;->mViewLayerShutter:Landroid/view/ViewGroup;

    invoke-virtual {v1}, Landroid/view/ViewGroup;->removeAllViews()V

    :cond_3
    iget-object v1, p0, Lcom/android/camera/Camera;->mViewLayerSetting:Landroid/view/ViewGroup;

    if-eqz v1, :cond_4

    iget-object v1, p0, Lcom/android/camera/Camera;->mViewLayerSetting:Landroid/view/ViewGroup;

    invoke-virtual {v1}, Landroid/view/ViewGroup;->removeAllViews()V

    :cond_4
    iget-object v1, p0, Lcom/android/camera/Camera;->mViewLayerOverlay:Landroid/view/ViewGroup;

    if-eqz v1, :cond_5

    iget-object v1, p0, Lcom/android/camera/Camera;->mViewLayerOverlay:Landroid/view/ViewGroup;

    invoke-virtual {v1}, Landroid/view/ViewGroup;->removeAllViews()V

    :cond_5
    invoke-static {}, Lcom/android/camera/manager/MMProfileManager;->stopProfileCameraViewOperation()V

    const/4 v1, -0x1

    invoke-virtual {p0, v4, v1}, Lcom/android/camera/Camera;->setOrientation(ZI)V

    invoke-direct {p0}, Lcom/android/camera/Camera;->setDisplayOrientation()V

    invoke-direct {p0}, Lcom/android/camera/Camera;->initializeForOpeningProcess()V

    invoke-direct {p0}, Lcom/android/camera/Camera;->setPreviewFrameLayoutAspectRatio()V

    invoke-direct {p0, v4}, Lcom/android/camera/Camera;->setLoadingAnimationVisible(Z)V

    invoke-direct {p0}, Lcom/android/camera/Camera;->updateFocusAndFace()V

    invoke-direct {p0}, Lcom/android/camera/Camera;->reInflateViewManager()V

    invoke-direct {p0}, Lcom/android/camera/Camera;->notifyOrientationChanged()V

    invoke-static {}, Lcom/android/camera/manager/MMProfileManager;->stopProfileCameraOnConfigChange()V

    return-void
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 2
    .param p1    # Landroid/os/Bundle;

    invoke-super {p0, p1}, Lcom/android/camera/ActivityBase;->onCreate(Landroid/os/Bundle;)V

    invoke-static {}, Lcom/android/camera/manager/MMProfileManager;->startProfileCameraOnCreate()V

    invoke-direct {p0}, Lcom/android/camera/Camera;->parseIntent()V

    new-instance v0, Lcom/android/camera/ComboPreferences;

    invoke-direct {v0, p0}, Lcom/android/camera/ComboPreferences;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/android/camera/Camera;->mPreferences:Lcom/android/camera/ComboPreferences;

    iget-object v0, p0, Lcom/android/camera/Camera;->mPreferences:Lcom/android/camera/ComboPreferences;

    invoke-virtual {v0}, Lcom/android/camera/ComboPreferences;->getGlobal()Landroid/content/SharedPreferences;

    move-result-object v0

    invoke-static {v0}, Lcom/android/camera/CameraSettings;->upgradeGlobalPreferences(Landroid/content/SharedPreferences;)V

    iget-object v0, p0, Lcom/android/camera/Camera;->mPreferences:Lcom/android/camera/ComboPreferences;

    invoke-direct {p0, v0}, Lcom/android/camera/Camera;->getPreferredCameraId(Lcom/android/camera/ComboPreferences;)I

    move-result v0

    iput v0, p0, Lcom/android/camera/Camera;->mCameraId:I

    iget-object v0, p0, Lcom/android/camera/Camera;->mPreferences:Lcom/android/camera/ComboPreferences;

    iget v1, p0, Lcom/android/camera/Camera;->mCameraId:I

    invoke-virtual {v0, p0, v1}, Lcom/android/camera/ComboPreferences;->setLocalId(Landroid/content/Context;I)V

    iget-object v0, p0, Lcom/android/camera/Camera;->mPreferences:Lcom/android/camera/ComboPreferences;

    invoke-virtual {v0}, Lcom/android/camera/ComboPreferences;->getLocal()Landroid/content/SharedPreferences;

    move-result-object v0

    invoke-static {v0}, Lcom/android/camera/CameraSettings;->upgradeLocalPreferences(Landroid/content/SharedPreferences;)V

    invoke-direct {p0}, Lcom/android/camera/Camera;->initializeCommonManagers()V

    invoke-virtual {p0}, Lcom/android/camera/Camera;->isVideoCaptureIntent()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-virtual {p0}, Lcom/android/camera/Camera;->isVideoWallPaperIntent()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    new-instance v0, Lcom/android/camera/actor/VideoActor;

    invoke-direct {v0, p0}, Lcom/android/camera/actor/VideoActor;-><init>(Lcom/android/camera/Camera;)V

    iput-object v0, p0, Lcom/android/camera/Camera;->mCameraActor:Lcom/android/camera/actor/CameraActor;

    :goto_0
    new-instance v0, Lcom/android/camera/Camera$CameraStartUpThread;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/android/camera/Camera$CameraStartUpThread;-><init>(Lcom/android/camera/Camera;Lcom/android/camera/Camera$1;)V

    iput-object v0, p0, Lcom/android/camera/Camera;->mCameraStartUpThread:Lcom/android/camera/Camera$CameraStartUpThread;

    iget-object v0, p0, Lcom/android/camera/Camera;->mCameraStartUpThread:Lcom/android/camera/Camera$CameraStartUpThread;

    invoke-virtual {v0}, Ljava/lang/Thread;->start()V

    invoke-static {p0}, Lcom/mediatek/camera/ext/ExtensionHelper;->ensureCameraExtension(Landroid/content/Context;)V

    invoke-static {}, Lcom/android/camera/Storage;->updateDefaultDirectory()Z

    invoke-static {}, Lcom/android/camera/manager/MMProfileManager;->startProfileCameraViewOperation()V

    const v0, 0x7f04000a

    invoke-virtual {p0, v0}, Lcom/android/camera/ActivityBase;->setContentView(I)V

    invoke-static {}, Lcom/android/camera/manager/MMProfileManager;->stopProfileCameraViewOperation()V

    invoke-virtual {p0}, Lcom/android/camera/Camera;->isNonePickIntent()Z

    move-result v0

    invoke-virtual {p0, v0}, Lcom/android/camera/ActivityBase;->createCameraScreenNail(Z)V

    iget-object v0, p0, Lcom/android/camera/ActivityBase;->mCameraScreenNail:Lcom/android/camera/CameraScreenNail;

    invoke-virtual {v0, p0}, Lcom/android/camera/CameraScreenNail;->setFrameListener(Lcom/android/camera/CameraScreenNail$FrameListener;)V

    invoke-direct {p0}, Lcom/android/camera/Camera;->initializeForOpeningProcess()V

    invoke-static {}, Lcom/android/camera/manager/MMProfileManager;->startProfileCameraPreviewPreReadyOpen()V

    iget-object v0, p0, Lcom/android/camera/Camera;->mStartPreviewPrerequisiteReady:Landroid/os/ConditionVariable;

    invoke-virtual {v0}, Landroid/os/ConditionVariable;->open()V

    invoke-static {}, Lcom/android/camera/manager/MMProfileManager;->stopProfileCameraPreviewPreReadyOpen()V

    invoke-direct {p0}, Lcom/android/camera/Camera;->initializeAfterPreview()V

    invoke-static {}, Lcom/android/camera/manager/MMProfileManager;->stopProfileCameraOnCreate()V

    return-void

    :cond_1
    new-instance v0, Lcom/android/camera/actor/PhotoActor;

    invoke-direct {v0, p0}, Lcom/android/camera/actor/PhotoActor;-><init>(Lcom/android/camera/Camera;)V

    iput-object v0, p0, Lcom/android/camera/Camera;->mCameraActor:Lcom/android/camera/actor/CameraActor;

    goto :goto_0
.end method

.method protected onDestroy()V
    .locals 3

    invoke-super {p0}, Lcom/android/camera/ActivityBase;->onDestroy()V

    invoke-static {}, Lcom/android/camera/manager/MMProfileManager;->startProfileCameraOnDestroy()V

    invoke-direct {p0}, Lcom/android/camera/Camera;->callResumableFinish()V

    iget-object v0, p0, Lcom/android/camera/Camera;->mFileSaver:Lcom/android/camera/FileSaver;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/camera/Camera;->mFileSaver:Lcom/android/camera/FileSaver;

    invoke-virtual {v0}, Lcom/android/camera/FileSaver;->finishAfterSaved()V

    :cond_0
    invoke-static {}, Lcom/android/camera/manager/SelfTimerManager;->release()V

    invoke-static {}, Lcom/android/camera/manager/MMProfileManager;->stopProfileCameraOnDestroy()V

    sget-boolean v0, Lcom/android/camera/Camera;->LOG:Z

    if-eqz v0, :cond_1

    const-string v0, "Camera"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "onDestroy() isChangingConfigurations()="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Landroid/app/Activity;->isChangingConfigurations()Z

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", mForceFinishing="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-boolean v2, p0, Lcom/android/camera/Camera;->mForceFinishing:Z

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/android/camera/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    :cond_1
    return-void
.end method

.method public onFirstFrameArrived()V
    .locals 2

    sget-boolean v0, Lcom/android/camera/Camera;->LOG:Z

    if-eqz v0, :cond_0

    const-string v0, "Camera"

    const-string v1, "onFirstFrameArrived()"

    invoke-static {v0, v1}, Lcom/android/camera/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    return-void
.end method

.method public onKeyDown(ILandroid/view/KeyEvent;)Z
    .locals 2
    .param p1    # I
    .param p2    # Landroid/view/KeyEvent;

    const/4 v0, 0x1

    iget-boolean v1, p0, Lcom/android/camera/ActivityBase;->mPaused:Z

    if-eqz v1, :cond_1

    :cond_0
    :goto_0
    return v0

    :cond_1
    invoke-virtual {p0}, Lcom/android/camera/ActivityBase;->isFullScreen()Z

    move-result v1

    if-eqz v1, :cond_2

    const/16 v1, 0x52

    if-ne v1, p1, :cond_2

    invoke-virtual {p2}, Landroid/view/KeyEvent;->getRepeatCount()I

    move-result v1

    if-nez v1, :cond_2

    iget-object v1, p0, Lcom/android/camera/Camera;->mSettingManager:Lcom/android/camera/manager/SettingManager;

    invoke-virtual {v1}, Lcom/android/camera/manager/SettingManager;->handleMenuEvent()Z

    move-result v1

    if-nez v1, :cond_0

    :cond_2
    iget-object v1, p0, Lcom/android/camera/Camera;->mCameraActor:Lcom/android/camera/actor/CameraActor;

    invoke-virtual {v1, p1, p2}, Lcom/android/camera/actor/CameraActor;->onKeyDown(ILandroid/view/KeyEvent;)Z

    move-result v1

    if-nez v1, :cond_0

    invoke-super {p0, p1, p2}, Lcom/android/camera/ActivityBase;->onKeyDown(ILandroid/view/KeyEvent;)Z

    move-result v0

    goto :goto_0
.end method

.method public onKeyUp(ILandroid/view/KeyEvent;)Z
    .locals 2
    .param p1    # I
    .param p2    # Landroid/view/KeyEvent;

    const/4 v0, 0x1

    iget-boolean v1, p0, Lcom/android/camera/ActivityBase;->mPaused:Z

    if-eqz v1, :cond_1

    :cond_0
    :goto_0
    return v0

    :cond_1
    iget-object v1, p0, Lcom/android/camera/Camera;->mCameraActor:Lcom/android/camera/actor/CameraActor;

    invoke-virtual {v1, p1, p2}, Lcom/android/camera/actor/CameraActor;->onKeyUp(ILandroid/view/KeyEvent;)Z

    move-result v1

    if-nez v1, :cond_0

    invoke-super {p0, p1, p2}, Landroid/app/Activity;->onKeyUp(ILandroid/view/KeyEvent;)Z

    move-result v0

    goto :goto_0
.end method

.method protected onPause()V
    .locals 5

    const/4 v4, 0x1

    const/4 v3, -0x1

    sget-boolean v0, Lcom/android/camera/Camera;->LOG:Z

    if-eqz v0, :cond_0

    const-string v0, "Camera"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "onPause() mForceFinishing="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-boolean v2, p0, Lcom/android/camera/Camera;->mForceFinishing:Z

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", mOpenCameraFail="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-boolean v2, p0, Lcom/android/camera/Camera;->mOpenCameraFail:Z

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", mCameraDisabled="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-boolean v2, p0, Lcom/android/camera/Camera;->mCameraDisabled:Z

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/android/camera/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    invoke-static {}, Lcom/android/camera/manager/MMProfileManager;->startProfileCameraOnPause()V

    invoke-super {p0}, Lcom/android/camera/ActivityBase;->onPause()V

    invoke-static {}, Lcom/android/camera/manager/MMProfileManager;->stopProfileCameraOnPause()V

    iget v0, p0, Lcom/android/camera/Camera;->mPendingSwitchCameraId:I

    if-eq v0, v3, :cond_1

    iput v3, p0, Lcom/android/camera/Camera;->mPendingSwitchCameraId:I

    :cond_1
    invoke-static {}, Lcom/android/camera/manager/MMProfileManager;->startProfileCameraOnPause()V

    iget-boolean v0, p0, Lcom/android/camera/Camera;->mForceFinishing:Z

    if-nez v0, :cond_2

    iget-boolean v0, p0, Lcom/android/camera/Camera;->mOpenCameraFail:Z

    if-nez v0, :cond_2

    iget-boolean v0, p0, Lcom/android/camera/Camera;->mCameraDisabled:Z

    if-eqz v0, :cond_3

    :cond_2
    invoke-static {}, Lcom/android/camera/Util;->exitCameraPQMode()V

    :goto_0
    return-void

    :cond_3
    invoke-direct {p0, v4}, Lcom/android/camera/Camera;->waitCameraStartUpThread(Z)V

    invoke-direct {p0}, Lcom/android/camera/Camera;->closeCamera()V

    invoke-direct {p0}, Lcom/android/camera/Camera;->releaseSurface()V

    invoke-direct {p0}, Lcom/android/camera/Camera;->clearFocusAndFace()V

    invoke-direct {p0}, Lcom/android/camera/Camera;->uninstallIntentFilter()V

    invoke-direct {p0}, Lcom/android/camera/Camera;->callResumablePause()V

    invoke-virtual {p0, v4}, Lcom/android/camera/Camera;->collapseViewManager(Z)Z

    iget-object v0, p0, Lcom/android/camera/Camera;->mOrientationListener:Lcom/android/camera/Camera$MyOrientationEventListener;

    invoke-virtual {v0}, Landroid/view/OrientationEventListener;->disable()V

    iget-object v0, p0, Lcom/android/camera/Camera;->mVoiceManager:Lcom/android/camera/VoiceManager;

    invoke-virtual {v0}, Lcom/android/camera/VoiceManager;->stopUpdateVoiceState()V

    iget-object v0, p0, Lcom/android/camera/Camera;->mLocationManager:Lcom/android/camera/LocationManager;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/android/camera/LocationManager;->recordLocation(Z)V

    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/android/camera/Camera;->mOnResumeTime:J

    iget-object v0, p0, Lcom/android/camera/Camera;->mMainHandler:Landroid/os/Handler;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacksAndMessages(Ljava/lang/Object;)V

    invoke-virtual {p0}, Lcom/android/camera/Camera;->resetScreenOn()V

    invoke-static {}, Lcom/android/camera/Util;->exitCameraPQMode()V

    invoke-static {}, Lcom/android/camera/manager/MMProfileManager;->stopProfileCameraOnPause()V

    goto :goto_0
.end method

.method protected onPreviewTextureCopied()V
    .locals 3

    const/4 v2, 0x5

    sget-boolean v0, Lcom/android/camera/Camera;->LOG:Z

    if-eqz v0, :cond_0

    const-string v0, "Camera"

    const-string v1, "onPreviewTextureCopied()"

    invoke-static {v0, v1}, Lcom/android/camera/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    iget-object v0, p0, Lcom/android/camera/Camera;->mMainHandler:Landroid/os/Handler;

    invoke-virtual {v0, v2}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    sget-boolean v0, Lcom/android/camera/Camera;->LOG:Z

    if-eqz v0, :cond_1

    invoke-static {v2}, Lcom/android/camera/Camera;->getMsgLabel(I)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/android/camera/manager/MMProfileManager;->triggersSendMessage(Ljava/lang/String;)V

    :cond_1
    return-void
.end method

.method protected onRestart()V
    .locals 3

    invoke-super {p0}, Landroid/app/Activity;->onRestart()V

    invoke-virtual {p0}, Lcom/android/camera/Camera;->isNonePickIntent()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-direct {p0}, Lcom/android/camera/Camera;->isMountPointChanged()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Landroid/app/Activity;->finish()V

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/android/camera/Camera;->mForceFinishing:Z

    invoke-virtual {p0}, Landroid/app/Activity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {p0, v0}, Landroid/app/Activity;->startActivity(Landroid/content/Intent;)V

    :cond_0
    sget-boolean v0, Lcom/android/camera/Camera;->LOG:Z

    if-eqz v0, :cond_1

    const-string v0, "Camera"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "onRestart() mForceFinishing="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-boolean v2, p0, Lcom/android/camera/Camera;->mForceFinishing:Z

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/android/camera/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    :cond_1
    return-void
.end method

.method protected onResume()V
    .locals 3

    sget-boolean v0, Lcom/android/camera/Camera;->LOG:Z

    if-eqz v0, :cond_0

    const-string v0, "Camera"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "onResume() mForceFinishing="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-boolean v2, p0, Lcom/android/camera/Camera;->mForceFinishing:Z

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", mOpenCameraFail="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-boolean v2, p0, Lcom/android/camera/Camera;->mOpenCameraFail:Z

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", mCameraDisabled="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-boolean v2, p0, Lcom/android/camera/Camera;->mCameraDisabled:Z

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", mCameraState="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/android/camera/Camera;->mCameraState:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", mCameraStartUpThread="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/android/camera/Camera;->mCameraStartUpThread:Lcom/android/camera/Camera$CameraStartUpThread;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/android/camera/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    invoke-super {p0}, Lcom/android/camera/ActivityBase;->onResume()V

    invoke-static {}, Lcom/android/camera/manager/MMProfileManager;->startProfileCameraOnResume()V

    iget-boolean v0, p0, Lcom/android/camera/Camera;->mForceFinishing:Z

    if-nez v0, :cond_1

    iget-boolean v0, p0, Lcom/android/camera/Camera;->mOpenCameraFail:Z

    if-nez v0, :cond_1

    iget-boolean v0, p0, Lcom/android/camera/Camera;->mCameraDisabled:Z

    if-eqz v0, :cond_2

    :cond_1
    :goto_0
    return-void

    :cond_2
    invoke-virtual {p0}, Lcom/android/camera/ActivityBase;->updateCameraAppViewIfNeed()V

    iget v0, p0, Lcom/android/camera/Camera;->mCameraState:I

    if-nez v0, :cond_3

    iget-object v0, p0, Lcom/android/camera/Camera;->mCameraStartUpThread:Lcom/android/camera/Camera$CameraStartUpThread;

    if-nez v0, :cond_3

    new-instance v0, Lcom/android/camera/Camera$CameraStartUpThread;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/android/camera/Camera$CameraStartUpThread;-><init>(Lcom/android/camera/Camera;Lcom/android/camera/Camera$1;)V

    iput-object v0, p0, Lcom/android/camera/Camera;->mCameraStartUpThread:Lcom/android/camera/Camera$CameraStartUpThread;

    iget-object v0, p0, Lcom/android/camera/Camera;->mCameraStartUpThread:Lcom/android/camera/Camera$CameraStartUpThread;

    invoke-virtual {v0}, Ljava/lang/Thread;->start()V

    :cond_3
    invoke-direct {p0}, Lcom/android/camera/Camera;->doOnResume()V

    invoke-static {}, Lcom/android/camera/Util;->enterCameraPQMode()V

    invoke-static {}, Lcom/android/camera/manager/MMProfileManager;->stopProfileCameraOnResume()V

    goto :goto_0
.end method

.method protected onSingleTapUp(Landroid/view/View;II)V
    .locals 3
    .param p1    # Landroid/view/View;
    .param p2    # I
    .param p3    # I

    sget-boolean v0, Lcom/android/camera/Camera;->LOG:Z

    if-eqz v0, :cond_0

    const-string v0, "Camera"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "onSingleTapUp("

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ")"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/android/camera/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    iget-object v0, p0, Lcom/android/camera/Camera;->mRotateDialog:Lcom/android/camera/manager/RotateDialog;

    invoke-virtual {v0}, Lcom/android/camera/manager/ViewManager;->isShowing()Z

    move-result v0

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/android/camera/Camera;->mRotateProgress:Lcom/android/camera/manager/RotateProgress;

    invoke-virtual {v0}, Lcom/android/camera/manager/ViewManager;->isShowing()Z

    move-result v0

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/android/camera/Camera;->mSettingManager:Lcom/android/camera/manager/SettingManager;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/android/camera/manager/SettingManager;->collapse(Z)Z

    move-result v0

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/android/camera/Camera;->mCameraActor:Lcom/android/camera/actor/CameraActor;

    invoke-virtual {v0}, Lcom/android/camera/actor/CameraActor;->getonSingleTapUpListener()Lcom/android/camera/Camera$OnSingleTapUpListener;

    move-result-object v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/android/camera/Camera;->mCameraActor:Lcom/android/camera/actor/CameraActor;

    invoke-virtual {v0}, Lcom/android/camera/actor/CameraActor;->getonSingleTapUpListener()Lcom/android/camera/Camera$OnSingleTapUpListener;

    move-result-object v0

    invoke-interface {v0, p1, p2, p3}, Lcom/android/camera/Camera$OnSingleTapUpListener;->onSingleTapUp(Landroid/view/View;II)V

    :cond_1
    return-void
.end method

.method protected onSingleTapUpBorder(Landroid/view/View;II)V
    .locals 2
    .param p1    # Landroid/view/View;
    .param p2    # I
    .param p3    # I

    iget-object v0, p0, Lcom/android/camera/Camera;->mRotateDialog:Lcom/android/camera/manager/RotateDialog;

    invoke-virtual {v0}, Lcom/android/camera/manager/ViewManager;->isShowing()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/android/camera/Camera;->mRotateProgress:Lcom/android/camera/manager/RotateProgress;

    invoke-virtual {v0}, Lcom/android/camera/manager/ViewManager;->isShowing()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/android/camera/Camera;->mSettingManager:Lcom/android/camera/manager/SettingManager;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/android/camera/manager/SettingManager;->collapse(Z)Z

    :cond_0
    return-void
.end method

.method public onSizeChanged(II)V
    .locals 1
    .param p1    # I
    .param p2    # I

    iget-object v0, p0, Lcom/android/camera/Camera;->mFocusManager:Lcom/android/camera/FocusManager;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/camera/Camera;->mFocusManager:Lcom/android/camera/FocusManager;

    invoke-virtual {v0, p1, p2}, Lcom/android/camera/FocusManager;->setPreviewSize(II)V

    :cond_0
    iput p1, p0, Lcom/android/camera/Camera;->mPreviewFrameWidth:I

    iput p2, p0, Lcom/android/camera/Camera;->mPreviewFrameHeight:I

    return-void
.end method

.method public onUserGuideUpdated(Ljava/lang/String;)V
    .locals 3
    .param p1    # Ljava/lang/String;

    sget-boolean v0, Lcom/android/camera/Camera;->LOG:Z

    if-eqz v0, :cond_0

    const-string v0, "Camera"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "onUserGuideUpdated("

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ")"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/android/camera/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    new-instance v0, Lcom/android/camera/Camera$20;

    invoke-direct {v0, p0, p1}, Lcom/android/camera/Camera$20;-><init>(Lcom/android/camera/Camera;Ljava/lang/String;)V

    invoke-virtual {p0, v0}, Landroid/app/Activity;->runOnUiThread(Ljava/lang/Runnable;)V

    return-void
.end method

.method public onUserInteraction()V
    .locals 1

    iget-object v0, p0, Lcom/android/camera/Camera;->mCameraActor:Lcom/android/camera/actor/CameraActor;

    invoke-virtual {v0}, Lcom/android/camera/actor/CameraActor;->onUserInteraction()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-super {p0}, Landroid/app/Activity;->onUserInteraction()V

    :cond_0
    return-void
.end method

.method public onVoiceCommandReceive(I)V
    .locals 3
    .param p1    # I

    sget-boolean v0, Lcom/android/camera/Camera;->LOG:Z

    if-eqz v0, :cond_0

    const-string v0, "Camera"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "onVoiceCommandReceive("

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ")"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/android/camera/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    new-instance v0, Lcom/android/camera/Camera$22;

    invoke-direct {v0, p0, p1}, Lcom/android/camera/Camera$22;-><init>(Lcom/android/camera/Camera;I)V

    invoke-virtual {p0, v0}, Landroid/app/Activity;->runOnUiThread(Ljava/lang/Runnable;)V

    return-void
.end method

.method public onVoiceValueUpdated(Ljava/lang/String;)V
    .locals 3
    .param p1    # Ljava/lang/String;

    sget-boolean v0, Lcom/android/camera/Camera;->LOG:Z

    if-eqz v0, :cond_0

    const-string v0, "Camera"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "onVoiceValueUpdated("

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ")"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/android/camera/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    const-string v0, "on"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/android/camera/Camera;->mVoiceManager:Lcom/android/camera/VoiceManager;

    invoke-virtual {v0}, Lcom/android/camera/VoiceManager;->enableVoice()V

    :cond_1
    new-instance v0, Lcom/android/camera/Camera$21;

    invoke-direct {v0, p0}, Lcom/android/camera/Camera$21;-><init>(Lcom/android/camera/Camera;)V

    invoke-virtual {p0, v0}, Landroid/app/Activity;->runOnUiThread(Ljava/lang/Runnable;)V

    return-void
.end method

.method public preparePhotoRequest()Lcom/android/camera/SaveRequest;
    .locals 1

    const/4 v0, 0x0

    invoke-virtual {p0, v0, v0}, Lcom/android/camera/Camera;->preparePhotoRequest(II)Lcom/android/camera/SaveRequest;

    move-result-object v0

    return-object v0
.end method

.method public preparePhotoRequest(II)Lcom/android/camera/SaveRequest;
    .locals 2
    .param p1    # I
    .param p2    # I

    iget-object v1, p0, Lcom/android/camera/Camera;->mFileSaver:Lcom/android/camera/FileSaver;

    invoke-virtual {v1, p1, p2}, Lcom/android/camera/FileSaver;->preparePhotoRequest(II)Lcom/android/camera/SaveRequest;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/android/camera/Camera;->applyParameterForCapture(Lcom/android/camera/SaveRequest;)V

    return-object v0
.end method

.method public prepareVideoRequest(ILjava/lang/String;)Lcom/android/camera/SaveRequest;
    .locals 2
    .param p1    # I
    .param p2    # Ljava/lang/String;

    iget-object v1, p0, Lcom/android/camera/Camera;->mFileSaver:Lcom/android/camera/FileSaver;

    invoke-virtual {v1, p1, p2}, Lcom/android/camera/FileSaver;->prepareVideoRequest(ILjava/lang/String;)Lcom/android/camera/SaveRequest;

    move-result-object v0

    return-object v0
.end method

.method public removeOnFullScreenChangedListener(Lcom/android/camera/Camera$OnFullScreenChangedListener;)Z
    .locals 1
    .param p1    # Lcom/android/camera/Camera$OnFullScreenChangedListener;

    iget-object v0, p0, Lcom/android/camera/Camera;->mFullScreenListeners:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->remove(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public removeOnOrientationListener(Lcom/android/camera/Camera$OnOrientationListener;)Z
    .locals 1
    .param p1    # Lcom/android/camera/Camera$OnOrientationListener;

    iget-object v0, p0, Lcom/android/camera/Camera;->mOrientationListeners:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->remove(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public removeResumable(Lcom/android/camera/Camera$Resumable;)Z
    .locals 1
    .param p1    # Lcom/android/camera/Camera$Resumable;

    iget-object v0, p0, Lcom/android/camera/Camera;->mResumables:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->remove(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public removeView(Landroid/view/View;)V
    .locals 1
    .param p1    # Landroid/view/View;

    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, Lcom/android/camera/Camera;->removeView(Landroid/view/View;I)V

    return-void
.end method

.method public removeView(Landroid/view/View;I)V
    .locals 1
    .param p1    # Landroid/view/View;
    .param p2    # I

    invoke-direct {p0, p2}, Lcom/android/camera/Camera;->getViewLayer(I)Landroid/view/ViewGroup;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {v0, p1}, Landroid/view/ViewGroup;->removeView(Landroid/view/View;)V

    :cond_0
    return-void
.end method

.method public removeViewManager(Lcom/android/camera/manager/ViewManager;)Z
    .locals 1
    .param p1    # Lcom/android/camera/manager/ViewManager;

    iget-object v0, p0, Lcom/android/camera/Camera;->mViewManagers:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->remove(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public requestRender()V
    .locals 1

    iget-object v0, p0, Lcom/android/camera/ActivityBase;->mAppBridge:Lcom/android/camera/ActivityBase$MyAppBridge;

    invoke-virtual {v0}, Lcom/android/camera/ActivityBase$MyAppBridge;->requestRender()V

    return-void
.end method

.method public resetLiveEffect(Z)V
    .locals 4
    .param p1    # Z

    sget-boolean v1, Lcom/android/camera/Camera;->LOG:Z

    if-eqz v1, :cond_0

    const-string v1, "Camera"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "resetLiveEffect("

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ")"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/android/camera/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    iget-object v1, p0, Lcom/android/camera/Camera;->mPreferences:Lcom/android/camera/ComboPreferences;

    invoke-virtual {v1}, Lcom/android/camera/ComboPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string v1, "pref_video_effect_key"

    const v2, 0x7f0c0150

    invoke-virtual {p0, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->apply()V

    if-eqz p1, :cond_1

    const/4 v1, 0x0

    iput v1, p0, Lcom/android/camera/Camera;->mLastEffectType:I

    const/4 v1, 0x0

    iput-object v1, p0, Lcom/android/camera/Camera;->mLastEffectParameter:Ljava/lang/Object;

    :cond_1
    iget-object v1, p0, Lcom/android/camera/Camera;->mSettingManager:Lcom/android/camera/manager/SettingManager;

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/android/camera/Camera;->mSettingManager:Lcom/android/camera/manager/SettingManager;

    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Lcom/android/camera/manager/SettingManager;->collapse(Z)Z

    :cond_2
    return-void
.end method

.method public resetScreenOn()V
    .locals 2

    sget-boolean v0, Lcom/android/camera/Camera;->LOG:Z

    if-eqz v0, :cond_0

    const-string v0, "Camera"

    const-string v1, "resetScreenOn()"

    invoke-static {v0, v1}, Lcom/android/camera/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    iget-object v0, p0, Lcom/android/camera/Camera;->mMainHandler:Landroid/os/Handler;

    const/4 v1, 0x7

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeMessages(I)V

    invoke-virtual {p0}, Landroid/app/Activity;->getWindow()Landroid/view/Window;

    move-result-object v0

    const/16 v1, 0x80

    invoke-virtual {v0, v1}, Landroid/view/Window;->clearFlags(I)V

    return-void
.end method

.method public restoreViewState()V
    .locals 2

    sget-boolean v0, Lcom/android/camera/Camera;->LOG:Z

    if-eqz v0, :cond_0

    const-string v0, "Camera"

    const-string v1, "restoreViewState()"

    invoke-static {v0, v1}, Lcom/android/camera/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    const/4 v0, -0x1

    invoke-virtual {p0, v0}, Lcom/android/camera/Camera;->setViewState(I)V

    return-void
.end method

.method public setAcceptFloatingInfo(Z)V
    .locals 3
    .param p1    # Z

    sget-boolean v0, Lcom/android/camera/Camera;->LOG:Z

    if-eqz v0, :cond_0

    const-string v0, "Camera"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "setAcceptFloatingInfo("

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ")"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/android/camera/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    iput-boolean p1, p0, Lcom/android/camera/Camera;->mAcceptFloatingInfo:Z

    iget-boolean v0, p0, Lcom/android/camera/Camera;->mAcceptFloatingInfo:Z

    if-eqz v0, :cond_1

    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/android/camera/Camera;->showIndicator(I)V

    :goto_0
    iget-object v0, p0, Lcom/android/camera/Camera;->mRemainingManager:Lcom/android/camera/manager/RemainingManager;

    invoke-virtual {v0}, Lcom/android/camera/manager/RemainingManager;->showHint()V

    return-void

    :cond_1
    iget-object v0, p0, Lcom/android/camera/Camera;->mIndicatorManager:Lcom/android/camera/manager/IndicatorManager;

    invoke-virtual {v0}, Lcom/android/camera/manager/ViewManager;->hide()V

    iget-object v0, p0, Lcom/android/camera/Camera;->mRemainingManager:Lcom/android/camera/manager/RemainingManager;

    invoke-virtual {v0}, Lcom/android/camera/manager/ViewManager;->hide()V

    iget-object v0, p0, Lcom/android/camera/Camera;->mInfoManager:Lcom/android/camera/manager/InfoManager;

    invoke-virtual {v0}, Lcom/android/camera/manager/ViewManager;->hide()V

    iget-object v0, p0, Lcom/android/camera/Camera;->mPickerManager:Lcom/android/camera/manager/PickerManager;

    invoke-virtual {v0}, Lcom/android/camera/manager/ViewManager;->hide()V

    goto :goto_0
.end method

.method public setCameraState(I)V
    .locals 3
    .param p1    # I

    sget-boolean v0, Lcom/android/camera/Camera;->LOG:Z

    if-eqz v0, :cond_0

    const-string v0, "Camera"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "setCameraState("

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ")"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/android/camera/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    iput p1, p0, Lcom/android/camera/Camera;->mCameraState:I

    return-void
.end method

.method public setOrientation(ZI)V
    .locals 3
    .param p1    # Z
    .param p2    # I

    sget-boolean v0, Lcom/android/camera/Camera;->LOG:Z

    if-eqz v0, :cond_0

    const-string v0, "Camera"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "setOrientation orientation="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " mOrientationListener.getLock()="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/android/camera/Camera;->mOrientationListener:Lcom/android/camera/Camera$MyOrientationEventListener;

    invoke-virtual {v2}, Lcom/android/camera/Camera$MyOrientationEventListener;->getLock()Z

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " lock = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/android/camera/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    iget-object v0, p0, Lcom/android/camera/Camera;->mOrientationListener:Lcom/android/camera/Camera$MyOrientationEventListener;

    invoke-virtual {v0}, Lcom/android/camera/Camera$MyOrientationEventListener;->getLock()Z

    move-result v0

    if-ne v0, p1, :cond_1

    :goto_0
    return-void

    :cond_1
    const/4 v0, -0x1

    if-eq p2, v0, :cond_2

    iget-object v0, p0, Lcom/android/camera/Camera;->mOrientationListener:Lcom/android/camera/Camera$MyOrientationEventListener;

    invoke-virtual {v0, p2}, Lcom/android/camera/Camera$MyOrientationEventListener;->onOrientationChanged(I)V

    :cond_2
    iget-object v0, p0, Lcom/android/camera/Camera;->mOrientationListener:Lcom/android/camera/Camera$MyOrientationEventListener;

    invoke-virtual {v0, p1}, Lcom/android/camera/Camera$MyOrientationEventListener;->setLock(Z)V

    goto :goto_0
.end method

.method public setPreviewTextureAsync()V
    .locals 3

    sget-boolean v0, Lcom/android/camera/Camera;->LOG:Z

    if-eqz v0, :cond_0

    const-string v0, "Camera"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "setPreviewTextureAsync() mSurfaceTextureReady="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-boolean v2, p0, Lcom/android/camera/Camera;->mSurfaceTextureReady:Z

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", mSurfaceTexture="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/android/camera/Camera;->mSurfaceTexture:Landroid/graphics/SurfaceTexture;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/android/camera/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    iget-object v0, p0, Lcom/android/camera/Camera;->mCameraDevice:Lcom/android/camera/CameraManager$CameraProxy;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/android/camera/Camera;->mSurfaceTexture:Landroid/graphics/SurfaceTexture;

    if-eqz v0, :cond_1

    invoke-virtual {p0}, Lcom/android/camera/Camera;->effectsActive()Z

    move-result v0

    if-nez v0, :cond_1

    iget-boolean v0, p0, Lcom/android/camera/Camera;->mSurfaceTextureReady:Z

    if-eqz v0, :cond_1

    invoke-static {}, Lcom/android/camera/manager/MMProfileManager;->triggerSetPreviewTexture()V

    iget-object v0, p0, Lcom/android/camera/Camera;->mCameraDevice:Lcom/android/camera/CameraManager$CameraProxy;

    iget-object v1, p0, Lcom/android/camera/Camera;->mSurfaceTexture:Landroid/graphics/SurfaceTexture;

    invoke-virtual {v0, v1}, Lcom/android/camera/CameraManager$CameraProxy;->setPreviewTextureAsync(Landroid/graphics/SurfaceTexture;)V

    :cond_1
    return-void
.end method

.method public setResultExAndFinish(I)V
    .locals 0
    .param p1    # I

    invoke-virtual {p0, p1}, Lcom/android/camera/ActivityBase;->setResultEx(I)V

    invoke-virtual {p0}, Landroid/app/Activity;->finish()V

    invoke-direct {p0}, Lcom/android/camera/Camera;->clearUserSettings()V

    return-void
.end method

.method public setResultExAndFinish(ILandroid/content/Intent;)V
    .locals 0
    .param p1    # I
    .param p2    # Landroid/content/Intent;

    invoke-virtual {p0, p1, p2}, Lcom/android/camera/ActivityBase;->setResultEx(ILandroid/content/Intent;)V

    invoke-virtual {p0}, Landroid/app/Activity;->finish()V

    invoke-direct {p0}, Lcom/android/camera/Camera;->clearUserSettings()V

    return-void
.end method

.method public setReviewOrientationCompensation(I)V
    .locals 1
    .param p1    # I

    iget-object v0, p0, Lcom/android/camera/Camera;->mReviewManager:Lcom/android/camera/manager/ReviewManager;

    invoke-virtual {v0, p1}, Lcom/android/camera/manager/ReviewManager;->setOrientationCompensation(I)V

    return-void
.end method

.method public setRotationToParameters()V
    .locals 4

    const/4 v0, -0x1

    iget-object v1, p0, Lcom/android/camera/Camera;->mParameters:Landroid/hardware/Camera$Parameters;

    if-eqz v1, :cond_0

    iget v1, p0, Lcom/android/camera/Camera;->mCameraId:I

    iget v2, p0, Lcom/android/camera/Camera;->mOrientation:I

    invoke-static {v1, v2}, Lcom/android/camera/Util;->getJpegRotation(II)I

    move-result v0

    iget-object v1, p0, Lcom/android/camera/Camera;->mParameters:Landroid/hardware/Camera$Parameters;

    invoke-virtual {v1, v0}, Landroid/hardware/Camera$Parameters;->setRotation(I)V

    :cond_0
    sget-boolean v1, Lcom/android/camera/Camera;->LOG:Z

    if-eqz v1, :cond_1

    const-string v1, "Camera"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "setRotationToParameters() mCameraId="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget v3, p0, Lcom/android/camera/Camera;->mCameraId:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ", mOrientation="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget v3, p0, Lcom/android/camera/Camera;->mOrientation:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ", jpegRotation="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/android/camera/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    :cond_1
    return-void
.end method

.method public setSurfaceTextureReady(Z)V
    .locals 3
    .param p1    # Z

    sget-boolean v0, Lcom/android/camera/Camera;->LOG:Z

    if-eqz v0, :cond_0

    const-string v0, "Camera"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "setSurfaceTextureReady("

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ") mSurfaceTextureReady="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-boolean v2, p0, Lcom/android/camera/Camera;->mSurfaceTextureReady:Z

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/android/camera/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    iput-boolean p1, p0, Lcom/android/camera/Camera;->mSurfaceTextureReady:Z

    return-void
.end method

.method public setSwipingEnabled(Z)V
    .locals 0
    .param p1    # Z

    invoke-super {p0, p1}, Lcom/android/camera/ActivityBase;->setSwipingEnabled(Z)V

    return-void
.end method

.method public setViewState(I)V
    .locals 5
    .param p1    # I

    const/4 v4, 0x1

    const/4 v3, 0x0

    sget-boolean v0, Lcom/android/camera/Camera;->LOG:Z

    if-eqz v0, :cond_0

    const-string v0, "Camera"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "setViewState("

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ") mCurrentViewState="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/android/camera/Camera;->mCurrentViewState:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", mPendingSwitchCameraId="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/android/camera/Camera;->mPendingSwitchCameraId:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/android/camera/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    invoke-virtual {p0}, Lcom/android/camera/Camera;->isSwitchingCamera()Z

    move-result v0

    if-eqz v0, :cond_2

    :cond_1
    :goto_0
    return-void

    :cond_2
    iput p1, p0, Lcom/android/camera/Camera;->mCurrentViewState:I

    packed-switch p1, :pswitch_data_0

    goto :goto_0

    :pswitch_0
    invoke-direct {p0, v4}, Lcom/android/camera/Camera;->setViewManagerVisible(Z)V

    invoke-direct {p0, v4}, Lcom/android/camera/Camera;->setViewManagerEnable(Z)V

    iget-object v0, p0, Lcom/android/camera/Camera;->mShutterManager:Lcom/android/camera/manager/ShutterManager;

    invoke-virtual {v0, v4}, Lcom/android/camera/manager/ShutterManager;->setVideoShutterEnabled(Z)V

    iget-object v0, p0, Lcom/android/camera/Camera;->mShutterManager:Lcom/android/camera/manager/ShutterManager;

    invoke-virtual {v0, v4}, Lcom/android/camera/manager/ShutterManager;->setEnabled(Z)V

    iget-object v0, p0, Lcom/android/camera/Camera;->mSettingManager:Lcom/android/camera/manager/SettingManager;

    invoke-virtual {v0, v4}, Lcom/android/camera/manager/ViewManager;->setFileter(Z)V

    iget-object v0, p0, Lcom/android/camera/Camera;->mSettingManager:Lcom/android/camera/manager/SettingManager;

    invoke-virtual {v0, v4, v4}, Lcom/android/camera/manager/ViewManager;->setAnimationEnabled(ZZ)V

    iget-object v0, p0, Lcom/android/camera/Camera;->mMainHandler:Landroid/os/Handler;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/os/Handler;->hasMessages(I)Z

    move-result v0

    if-nez v0, :cond_1

    invoke-direct {p0, v3}, Lcom/android/camera/Camera;->showIndicator(I)V

    goto :goto_0

    :pswitch_1
    iget-object v0, p0, Lcom/android/camera/Camera;->mSettingManager:Lcom/android/camera/manager/SettingManager;

    invoke-virtual {v0, v4}, Lcom/android/camera/manager/SettingManager;->collapse(Z)Z

    invoke-direct {p0, v3}, Lcom/android/camera/Camera;->setViewManagerEnable(Z)V

    iget-object v0, p0, Lcom/android/camera/Camera;->mShutterManager:Lcom/android/camera/manager/ShutterManager;

    invoke-virtual {v0, v3}, Lcom/android/camera/manager/ShutterManager;->setEnabled(Z)V

    goto :goto_0

    :pswitch_2
    iget-object v0, p0, Lcom/android/camera/Camera;->mSettingManager:Lcom/android/camera/manager/SettingManager;

    invoke-virtual {v0, v4}, Lcom/android/camera/manager/SettingManager;->collapse(Z)Z

    invoke-direct {p0, v3}, Lcom/android/camera/Camera;->setViewManagerVisible(Z)V

    invoke-direct {p0, v3}, Lcom/android/camera/Camera;->setViewManagerEnable(Z)V

    iget-object v0, p0, Lcom/android/camera/Camera;->mShutterManager:Lcom/android/camera/manager/ShutterManager;

    invoke-virtual {v0, v3}, Lcom/android/camera/manager/ShutterManager;->setVideoShutterEnabled(Z)V

    goto :goto_0

    :pswitch_3
    iget-object v0, p0, Lcom/android/camera/Camera;->mSettingManager:Lcom/android/camera/manager/SettingManager;

    invoke-virtual {v0, v4}, Lcom/android/camera/manager/SettingManager;->collapse(Z)Z

    invoke-direct {p0, v3}, Lcom/android/camera/Camera;->setViewManagerVisible(Z)V

    invoke-direct {p0, v3}, Lcom/android/camera/Camera;->setViewManagerEnable(Z)V

    iget-object v0, p0, Lcom/android/camera/Camera;->mZoomManager:Lcom/android/camera/manager/ZoomManager;

    invoke-virtual {v0, v4}, Lcom/android/camera/manager/ViewManager;->setEnabled(Z)V

    goto :goto_0

    :pswitch_4
    iget-object v0, p0, Lcom/android/camera/Camera;->mModePicker:Lcom/android/camera/manager/ModePicker;

    invoke-virtual {v0}, Lcom/android/camera/manager/ViewManager;->hide()V

    iget-object v0, p0, Lcom/android/camera/Camera;->mThumbnailManager:Lcom/android/camera/manager/ThumbnailManager;

    invoke-virtual {v0}, Lcom/android/camera/manager/ViewManager;->hide()V

    iget-object v0, p0, Lcom/android/camera/Camera;->mPickerManager:Lcom/android/camera/manager/PickerManager;

    invoke-virtual {v0}, Lcom/android/camera/manager/ViewManager;->hide()V

    invoke-direct {p0, v3}, Lcom/android/camera/Camera;->setViewManagerEnable(Z)V

    iget-object v0, p0, Lcom/android/camera/Camera;->mSettingManager:Lcom/android/camera/manager/SettingManager;

    invoke-virtual {v0, v4}, Lcom/android/camera/manager/ViewManager;->setEnabled(Z)V

    goto :goto_0

    :pswitch_5
    invoke-direct {p0, v3}, Lcom/android/camera/Camera;->setViewManagerEnable(Z)V

    goto :goto_0

    :pswitch_6
    iget-object v0, p0, Lcom/android/camera/Camera;->mShutterManager:Lcom/android/camera/manager/ShutterManager;

    invoke-virtual {v0, v3}, Lcom/android/camera/manager/ShutterManager;->setEnabled(Z)V

    invoke-direct {p0, v3}, Lcom/android/camera/Camera;->setViewManagerVisible(Z)V

    invoke-direct {p0, v3}, Lcom/android/camera/Camera;->setViewManagerEnable(Z)V

    goto/16 :goto_0

    :pswitch_7
    invoke-direct {p0, v3}, Lcom/android/camera/Camera;->setViewManagerVisible(Z)V

    invoke-direct {p0, v3}, Lcom/android/camera/Camera;->setViewManagerEnable(Z)V

    goto/16 :goto_0

    :pswitch_8
    invoke-direct {p0, v3}, Lcom/android/camera/Camera;->setViewManagerEnable(Z)V

    iget-object v0, p0, Lcom/android/camera/Camera;->mShutterManager:Lcom/android/camera/manager/ShutterManager;

    invoke-virtual {v0, v3}, Lcom/android/camera/manager/ShutterManager;->setEnabled(Z)V

    iget-object v0, p0, Lcom/android/camera/Camera;->mIndicatorManager:Lcom/android/camera/manager/IndicatorManager;

    invoke-virtual {v0}, Lcom/android/camera/manager/ViewManager;->hide()V

    goto/16 :goto_0

    :pswitch_9
    iget-object v0, p0, Lcom/android/camera/Camera;->mShutterManager:Lcom/android/camera/manager/ShutterManager;

    invoke-virtual {v0, v4}, Lcom/android/camera/manager/ShutterManager;->setEnabled(Z)V

    invoke-direct {p0, v3}, Lcom/android/camera/Camera;->setViewManagerVisible(Z)V

    invoke-direct {p0, v3}, Lcom/android/camera/Camera;->setViewManagerEnable(Z)V

    goto/16 :goto_0

    :pswitch_a
    iget-object v0, p0, Lcom/android/camera/Camera;->mSettingManager:Lcom/android/camera/manager/SettingManager;

    invoke-virtual {v0, v4}, Lcom/android/camera/manager/SettingManager;->collapse(Z)Z

    iget-object v0, p0, Lcom/android/camera/Camera;->mShutterManager:Lcom/android/camera/manager/ShutterManager;

    invoke-virtual {v0, v4}, Lcom/android/camera/manager/ShutterManager;->setEnabled(Z)V

    invoke-direct {p0, v3}, Lcom/android/camera/Camera;->setViewManagerVisible(Z)V

    invoke-direct {p0, v3}, Lcom/android/camera/Camera;->setViewManagerEnable(Z)V

    iget-object v0, p0, Lcom/android/camera/Camera;->mThumbnailManager:Lcom/android/camera/manager/ThumbnailManager;

    invoke-virtual {v0}, Lcom/android/camera/manager/ViewManager;->hide()V

    goto/16 :goto_0

    :pswitch_b
    iget-object v0, p0, Lcom/android/camera/Camera;->mSettingManager:Lcom/android/camera/manager/SettingManager;

    invoke-virtual {v0, v4}, Lcom/android/camera/manager/SettingManager;->collapse(Z)Z

    invoke-direct {p0, v3}, Lcom/android/camera/Camera;->setViewManagerVisible(Z)V

    invoke-direct {p0, v3}, Lcom/android/camera/Camera;->setViewManagerEnable(Z)V

    iget-object v0, p0, Lcom/android/camera/Camera;->mZoomManager:Lcom/android/camera/manager/ZoomManager;

    invoke-virtual {v0, v3}, Lcom/android/camera/manager/ViewManager;->setEnabled(Z)V

    goto/16 :goto_0

    nop

    :pswitch_data_0
    .packed-switch -0x1
        :pswitch_0
        :pswitch_1
        :pswitch_3
        :pswitch_2
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_7
        :pswitch_8
        :pswitch_9
        :pswitch_a
        :pswitch_b
    .end packed-switch
.end method

.method public showAlertDialog(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Runnable;Ljava/lang/String;Ljava/lang/Runnable;)V
    .locals 7
    .param p1    # Ljava/lang/String;
    .param p2    # Ljava/lang/String;
    .param p3    # Ljava/lang/String;
    .param p4    # Ljava/lang/Runnable;
    .param p5    # Ljava/lang/String;
    .param p6    # Ljava/lang/Runnable;

    iget-object v0, p0, Lcom/android/camera/Camera;->mRotateDialog:Lcom/android/camera/manager/RotateDialog;

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    move-object v5, p5

    move-object v6, p6

    invoke-virtual/range {v0 .. v6}, Lcom/android/camera/manager/RotateDialog;->showAlertDialog(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Runnable;Ljava/lang/String;Ljava/lang/Runnable;)V

    return-void
.end method

.method public showBorder(Z)V
    .locals 1
    .param p1    # Z

    iget-object v0, p0, Lcom/android/camera/Camera;->mPreviewFrameLayout:Lcom/android/camera/ui/PreviewFrameLayout;

    invoke-virtual {v0, p1}, Lcom/android/camera/ui/PreviewFrameLayout;->showBorder(Z)V

    return-void
.end method

.method public showInfo(Ljava/lang/String;)V
    .locals 3
    .param p1    # Ljava/lang/String;

    sget-boolean v0, Lcom/android/camera/Camera;->LOG:Z

    if-eqz v0, :cond_0

    const-string v0, "Camera"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "showInfo("

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ")"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/android/camera/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    const/16 v0, 0xbb8

    invoke-virtual {p0, p1, v0}, Lcom/android/camera/Camera;->showInfo(Ljava/lang/String;I)V

    return-void
.end method

.method public showInfo(Ljava/lang/String;I)V
    .locals 3
    .param p1    # Ljava/lang/String;
    .param p2    # I

    sget-boolean v0, Lcom/android/camera/Camera;->LOG:Z

    if-eqz v0, :cond_0

    const-string v0, "Camera"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "showInfo("

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ")"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/android/camera/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    iget-object v0, p0, Lcom/android/camera/Camera;->mMainHandler:Landroid/os/Handler;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeMessages(I)V

    invoke-direct {p0, p1, p2}, Lcom/android/camera/Camera;->doShowInfo(Ljava/lang/String;I)V

    return-void
.end method

.method public showProgress(Ljava/lang/String;)V
    .locals 1
    .param p1    # Ljava/lang/String;

    const/4 v0, 0x5

    invoke-virtual {p0, v0}, Lcom/android/camera/Camera;->setViewState(I)V

    iget-object v0, p0, Lcom/android/camera/Camera;->mRotateProgress:Lcom/android/camera/manager/RotateProgress;

    invoke-virtual {v0, p1}, Lcom/android/camera/manager/RotateProgress;->showProgress(Ljava/lang/String;)V

    return-void
.end method

.method public showRemaining()V
    .locals 2

    sget-boolean v0, Lcom/android/camera/Camera;->LOG:Z

    if-eqz v0, :cond_0

    const-string v0, "Camera"

    const-string v1, "showRemaining()"

    invoke-static {v0, v1}, Lcom/android/camera/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    iget-object v0, p0, Lcom/android/camera/Camera;->mMainHandler:Landroid/os/Handler;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeMessages(I)V

    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/android/camera/Camera;->doShowRemaining(Z)V

    return-void
.end method

.method public showReview()V
    .locals 1

    const/4 v0, 0x6

    invoke-virtual {p0, v0}, Lcom/android/camera/Camera;->setViewState(I)V

    iget-object v0, p0, Lcom/android/camera/Camera;->mReviewManager:Lcom/android/camera/manager/ReviewManager;

    invoke-virtual {v0}, Lcom/android/camera/manager/ViewManager;->show()V

    return-void
.end method

.method public showReview(Ljava/io/FileDescriptor;)V
    .locals 3
    .param p1    # Ljava/io/FileDescriptor;

    sget-boolean v0, Lcom/android/camera/Camera;->LOG:Z

    if-eqz v0, :cond_0

    const-string v0, "Camera"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "showReview("

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ")"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/android/camera/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    const/4 v0, 0x6

    invoke-virtual {p0, v0}, Lcom/android/camera/Camera;->setViewState(I)V

    iget-object v0, p0, Lcom/android/camera/Camera;->mReviewManager:Lcom/android/camera/manager/ReviewManager;

    invoke-virtual {v0, p1}, Lcom/android/camera/manager/ReviewManager;->show(Ljava/io/FileDescriptor;)V

    return-void
.end method

.method public showReview(Ljava/lang/String;)V
    .locals 3
    .param p1    # Ljava/lang/String;

    sget-boolean v0, Lcom/android/camera/Camera;->LOG:Z

    if-eqz v0, :cond_0

    const-string v0, "Camera"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "showReview("

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ")"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/android/camera/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    const/4 v0, 0x6

    invoke-virtual {p0, v0}, Lcom/android/camera/Camera;->setViewState(I)V

    iget-object v0, p0, Lcom/android/camera/Camera;->mReviewManager:Lcom/android/camera/manager/ReviewManager;

    invoke-virtual {v0, p1}, Lcom/android/camera/manager/ReviewManager;->show(Ljava/lang/String;)V

    return-void
.end method

.method public showToast(I)V
    .locals 1
    .param p1    # I

    invoke-virtual {p0, p1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/android/camera/Camera;->showToast(Ljava/lang/String;)V

    return-void
.end method

.method public showToast(Ljava/lang/String;)V
    .locals 3
    .param p1    # Ljava/lang/String;

    sget-boolean v0, Lcom/android/camera/Camera;->LOG:Z

    if-eqz v0, :cond_0

    const-string v0, "Camera"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "showToast("

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ")"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/android/camera/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    if-eqz p1, :cond_1

    invoke-virtual {p0}, Lcom/android/camera/Camera;->isAcceptFloatingInfo()Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/android/camera/Camera;->mRotateToast:Lcom/android/camera/manager/OnScreenHint;

    if-nez v0, :cond_2

    invoke-static {p0, p1}, Lcom/android/camera/manager/OnScreenHint;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;)Lcom/android/camera/manager/OnScreenHint;

    move-result-object v0

    iput-object v0, p0, Lcom/android/camera/Camera;->mRotateToast:Lcom/android/camera/manager/OnScreenHint;

    :goto_0
    iget-object v0, p0, Lcom/android/camera/Camera;->mRotateToast:Lcom/android/camera/manager/OnScreenHint;

    invoke-virtual {v0}, Lcom/android/camera/manager/OnScreenHint;->showToast()V

    :cond_1
    return-void

    :cond_2
    iget-object v0, p0, Lcom/android/camera/Camera;->mRotateToast:Lcom/android/camera/manager/OnScreenHint;

    invoke-virtual {v0, p1}, Lcom/android/camera/manager/OnScreenHint;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0
.end method

.method public switchShutter(I)V
    .locals 1
    .param p1    # I

    iget-object v0, p0, Lcom/android/camera/Camera;->mShutterManager:Lcom/android/camera/manager/ShutterManager;

    invoke-virtual {v0, p1}, Lcom/android/camera/manager/ShutterManager;->switchShutter(I)V

    return-void
.end method
