.class Lcom/android/camera/actor/VideoActor$15;
.super Ljava/lang/Object;
.source "VideoActor.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/camera/actor/VideoActor;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/camera/actor/VideoActor;


# direct methods
.method constructor <init>(Lcom/android/camera/actor/VideoActor;)V
    .locals 0

    iput-object p1, p0, Lcom/android/camera/actor/VideoActor$15;->this$0:Lcom/android/camera/actor/VideoActor;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 3
    .param p1    # Landroid/view/View;

    const/4 v2, 0x1

    iget-object v0, p0, Lcom/android/camera/actor/VideoActor$15;->this$0:Lcom/android/camera/actor/VideoActor;

    invoke-static {v0}, Lcom/android/camera/actor/VideoActor;->access$5000(Lcom/android/camera/actor/VideoActor;)V

    iget-object v0, p0, Lcom/android/camera/actor/VideoActor$15;->this$0:Lcom/android/camera/actor/VideoActor;

    invoke-static {v0}, Lcom/android/camera/actor/VideoActor;->access$400(Lcom/android/camera/actor/VideoActor;)Lcom/android/camera/Camera;

    move-result-object v0

    invoke-virtual {v0}, Lcom/android/camera/Camera;->hideReview()V

    iget-object v0, p0, Lcom/android/camera/actor/VideoActor$15;->this$0:Lcom/android/camera/actor/VideoActor;

    invoke-static {v0}, Lcom/android/camera/actor/VideoActor;->access$2100(Lcom/android/camera/actor/VideoActor;)Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/android/camera/actor/VideoActor$15;->this$0:Lcom/android/camera/actor/VideoActor;

    invoke-static {v0}, Lcom/android/camera/actor/VideoActor;->access$400(Lcom/android/camera/actor/VideoActor;)Lcom/android/camera/Camera;

    move-result-object v0

    const/4 v1, 0x5

    invoke-virtual {v0, v1}, Lcom/android/camera/Camera;->switchShutter(I)V

    iget-object v0, p0, Lcom/android/camera/actor/VideoActor$15;->this$0:Lcom/android/camera/actor/VideoActor;

    invoke-static {v0}, Lcom/android/camera/actor/VideoActor;->access$400(Lcom/android/camera/actor/VideoActor;)Lcom/android/camera/Camera;

    move-result-object v0

    invoke-virtual {v0}, Lcom/android/camera/Camera;->getShutterManager()Lcom/android/camera/manager/ShutterManager;

    move-result-object v0

    invoke-virtual {v0, v2}, Lcom/android/camera/manager/ShutterManager;->setCancelButtonEnabled(Z)V

    iget-object v0, p0, Lcom/android/camera/actor/VideoActor$15;->this$0:Lcom/android/camera/actor/VideoActor;

    invoke-static {v0}, Lcom/android/camera/actor/VideoActor;->access$400(Lcom/android/camera/actor/VideoActor;)Lcom/android/camera/Camera;

    move-result-object v0

    invoke-virtual {v0}, Lcom/android/camera/Camera;->getShutterManager()Lcom/android/camera/manager/ShutterManager;

    move-result-object v0

    invoke-virtual {v0, v2}, Lcom/android/camera/manager/ShutterManager;->setVideoShutterEnabled(Z)V

    iget-object v0, p0, Lcom/android/camera/actor/VideoActor$15;->this$0:Lcom/android/camera/actor/VideoActor;

    invoke-static {v0}, Lcom/android/camera/actor/VideoActor;->access$5100(Lcom/android/camera/actor/VideoActor;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/camera/actor/VideoActor$15;->this$0:Lcom/android/camera/actor/VideoActor;

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lcom/android/camera/actor/VideoActor;->access$5102(Lcom/android/camera/actor/VideoActor;Z)Z

    iget-object v0, p0, Lcom/android/camera/actor/VideoActor$15;->this$0:Lcom/android/camera/actor/VideoActor;

    invoke-static {v0}, Lcom/android/camera/actor/VideoActor;->access$5200(Lcom/android/camera/actor/VideoActor;)V

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v0, p0, Lcom/android/camera/actor/VideoActor$15;->this$0:Lcom/android/camera/actor/VideoActor;

    invoke-static {v0}, Lcom/android/camera/actor/VideoActor;->access$400(Lcom/android/camera/actor/VideoActor;)Lcom/android/camera/Camera;

    move-result-object v0

    invoke-virtual {v0}, Lcom/android/camera/Camera;->getShutterManager()Lcom/android/camera/manager/ShutterManager;

    move-result-object v0

    invoke-virtual {v0, v2}, Lcom/android/camera/manager/ShutterManager;->setVideoShutterEnabled(Z)V

    iget-object v0, p0, Lcom/android/camera/actor/VideoActor$15;->this$0:Lcom/android/camera/actor/VideoActor;

    invoke-static {v0}, Lcom/android/camera/actor/VideoActor;->access$400(Lcom/android/camera/actor/VideoActor;)Lcom/android/camera/Camera;

    move-result-object v0

    const/4 v1, 0x2

    invoke-virtual {v0, v1}, Lcom/android/camera/Camera;->switchShutter(I)V

    goto :goto_0
.end method
