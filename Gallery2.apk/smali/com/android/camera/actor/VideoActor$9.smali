.class Lcom/android/camera/actor/VideoActor$9;
.super Ljava/lang/Object;
.source "VideoActor.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/camera/actor/VideoActor;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/camera/actor/VideoActor;


# direct methods
.method constructor <init>(Lcom/android/camera/actor/VideoActor;)V
    .locals 0

    iput-object p1, p0, Lcom/android/camera/actor/VideoActor$9;->this$0:Lcom/android/camera/actor/VideoActor;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 6

    const/4 v5, 0x0

    const/4 v4, 0x1

    invoke-static {}, Lcom/android/camera/actor/VideoActor;->access$300()Z

    move-result v1

    if-eqz v1, :cond_0

    const-string v1, "VideoActor"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "mVideoSavedRunnable.run() begin mVideoCameraClosed="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/android/camera/actor/VideoActor$9;->this$0:Lcom/android/camera/actor/VideoActor;

    invoke-static {v3}, Lcom/android/camera/actor/VideoActor;->access$700(Lcom/android/camera/actor/VideoActor;)Z

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ", mStoppingAction="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/android/camera/actor/VideoActor$9;->this$0:Lcom/android/camera/actor/VideoActor;

    invoke-static {v3}, Lcom/android/camera/actor/VideoActor;->access$2700(Lcom/android/camera/actor/VideoActor;)I

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ", mEffectsDisplayResult="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/android/camera/actor/VideoActor$9;->this$0:Lcom/android/camera/actor/VideoActor;

    invoke-static {v3}, Lcom/android/camera/actor/VideoActor;->access$3400(Lcom/android/camera/actor/VideoActor;)Z

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ", mFocusState="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/android/camera/actor/VideoActor$9;->this$0:Lcom/android/camera/actor/VideoActor;

    invoke-static {v3}, Lcom/android/camera/actor/VideoActor;->access$3500(Lcom/android/camera/actor/VideoActor;)I

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ", mSingleAutoModeSupported="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/android/camera/actor/VideoActor$9;->this$0:Lcom/android/camera/actor/VideoActor;

    invoke-static {v3}, Lcom/android/camera/actor/VideoActor;->access$3600(Lcom/android/camera/actor/VideoActor;)Z

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ", mRecorderBusy="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/android/camera/actor/VideoActor$9;->this$0:Lcom/android/camera/actor/VideoActor;

    invoke-static {v3}, Lcom/android/camera/actor/VideoActor;->access$2200(Lcom/android/camera/actor/VideoActor;)Z

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/android/camera/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    iget-object v1, p0, Lcom/android/camera/actor/VideoActor$9;->this$0:Lcom/android/camera/actor/VideoActor;

    invoke-static {v1, v5}, Lcom/android/camera/actor/VideoActor;->access$3700(Lcom/android/camera/actor/VideoActor;Z)V

    iget-object v1, p0, Lcom/android/camera/actor/VideoActor$9;->this$0:Lcom/android/camera/actor/VideoActor;

    invoke-static {v1}, Lcom/android/camera/actor/VideoActor;->access$400(Lcom/android/camera/actor/VideoActor;)Lcom/android/camera/Camera;

    move-result-object v1

    invoke-virtual {v1}, Lcom/android/camera/Camera;->dismissProgress()V

    iget-object v1, p0, Lcom/android/camera/actor/VideoActor$9;->this$0:Lcom/android/camera/actor/VideoActor;

    invoke-static {v1}, Lcom/android/camera/actor/VideoActor;->access$2100(Lcom/android/camera/actor/VideoActor;)Z

    move-result v1

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/android/camera/actor/VideoActor$9;->this$0:Lcom/android/camera/actor/VideoActor;

    invoke-static {v1}, Lcom/android/camera/actor/VideoActor;->access$400(Lcom/android/camera/actor/VideoActor;)Lcom/android/camera/Camera;

    move-result-object v1

    invoke-virtual {v1}, Lcom/android/camera/Camera;->getShutterManager()Lcom/android/camera/manager/ShutterManager;

    move-result-object v1

    invoke-virtual {v1, v4}, Lcom/android/camera/manager/ShutterManager;->setCancelButtonEnabled(Z)V

    :cond_1
    iget-object v1, p0, Lcom/android/camera/actor/VideoActor$9;->this$0:Lcom/android/camera/actor/VideoActor;

    invoke-static {v1}, Lcom/android/camera/actor/VideoActor;->access$700(Lcom/android/camera/actor/VideoActor;)Z

    move-result v1

    if-nez v1, :cond_2

    iget-object v1, p0, Lcom/android/camera/actor/VideoActor$9;->this$0:Lcom/android/camera/actor/VideoActor;

    invoke-static {v1}, Lcom/android/camera/actor/VideoActor;->access$400(Lcom/android/camera/actor/VideoActor;)Lcom/android/camera/Camera;

    move-result-object v1

    invoke-virtual {v1}, Lcom/android/camera/Camera;->keepScreenOnAwhile()V

    :cond_2
    iget-object v1, p0, Lcom/android/camera/actor/VideoActor$9;->this$0:Lcom/android/camera/actor/VideoActor;

    invoke-static {v1}, Lcom/android/camera/actor/VideoActor;->access$400(Lcom/android/camera/actor/VideoActor;)Lcom/android/camera/Camera;

    move-result-object v1

    invoke-virtual {v1}, Lcom/android/camera/Camera;->getShutterManager()Lcom/android/camera/manager/ShutterManager;

    move-result-object v1

    invoke-virtual {v1, v4}, Lcom/android/camera/manager/ShutterManager;->setVideoShutterEnabled(Z)V

    iget-object v1, p0, Lcom/android/camera/actor/VideoActor$9;->this$0:Lcom/android/camera/actor/VideoActor;

    invoke-static {v1}, Lcom/android/camera/actor/VideoActor;->access$700(Lcom/android/camera/actor/VideoActor;)Z

    move-result v1

    if-eqz v1, :cond_8

    iget-object v1, p0, Lcom/android/camera/actor/VideoActor$9;->this$0:Lcom/android/camera/actor/VideoActor;

    invoke-static {v1}, Lcom/android/camera/actor/VideoActor;->access$2700(Lcom/android/camera/actor/VideoActor;)I

    move-result v1

    if-eq v1, v4, :cond_8

    iget-object v1, p0, Lcom/android/camera/actor/VideoActor$9;->this$0:Lcom/android/camera/actor/VideoActor;

    invoke-static {v1}, Lcom/android/camera/actor/VideoActor;->access$2700(Lcom/android/camera/actor/VideoActor;)I

    move-result v1

    const/4 v2, 0x5

    if-eq v1, v2, :cond_8

    const/4 v0, 0x4

    :goto_0
    packed-switch v0, :pswitch_data_0

    :cond_3
    :goto_1
    iget-object v1, p0, Lcom/android/camera/actor/VideoActor$9;->this$0:Lcom/android/camera/actor/VideoActor;

    invoke-static {v1}, Lcom/android/camera/actor/VideoActor;->access$700(Lcom/android/camera/actor/VideoActor;)Z

    move-result v1

    if-eqz v1, :cond_4

    iget-object v1, p0, Lcom/android/camera/actor/VideoActor$9;->this$0:Lcom/android/camera/actor/VideoActor;

    invoke-static {v1}, Lcom/android/camera/actor/VideoActor;->access$3400(Lcom/android/camera/actor/VideoActor;)Z

    move-result v1

    if-nez v1, :cond_4

    iget-object v1, p0, Lcom/android/camera/actor/VideoActor$9;->this$0:Lcom/android/camera/actor/VideoActor;

    invoke-static {v1}, Lcom/android/camera/actor/VideoActor;->access$4000(Lcom/android/camera/actor/VideoActor;)V

    :cond_4
    iget-object v1, p0, Lcom/android/camera/actor/VideoActor$9;->this$0:Lcom/android/camera/actor/VideoActor;

    invoke-static {v1}, Lcom/android/camera/actor/VideoActor;->access$700(Lcom/android/camera/actor/VideoActor;)Z

    move-result v1

    if-nez v1, :cond_6

    iget-object v1, p0, Lcom/android/camera/actor/VideoActor$9;->this$0:Lcom/android/camera/actor/VideoActor;

    invoke-static {v1}, Lcom/android/camera/actor/VideoActor;->access$3500(Lcom/android/camera/actor/VideoActor;)I

    move-result v1

    const/4 v2, -0x1

    if-eq v1, v2, :cond_5

    iget-object v1, p0, Lcom/android/camera/actor/VideoActor$9;->this$0:Lcom/android/camera/actor/VideoActor;

    invoke-static {v1}, Lcom/android/camera/actor/VideoActor;->access$3500(Lcom/android/camera/actor/VideoActor;)I

    move-result v1

    if-eq v1, v4, :cond_5

    iget-object v1, p0, Lcom/android/camera/actor/VideoActor$9;->this$0:Lcom/android/camera/actor/VideoActor;

    invoke-static {v1}, Lcom/android/camera/actor/VideoActor;->access$3600(Lcom/android/camera/actor/VideoActor;)Z

    move-result v1

    if-eqz v1, :cond_6

    :cond_5
    iget-object v1, p0, Lcom/android/camera/actor/VideoActor$9;->this$0:Lcom/android/camera/actor/VideoActor;

    invoke-static {v1}, Lcom/android/camera/actor/VideoActor;->access$4100(Lcom/android/camera/actor/VideoActor;)V

    :cond_6
    iget-object v1, p0, Lcom/android/camera/actor/VideoActor$9;->this$0:Lcom/android/camera/actor/VideoActor;

    invoke-static {v1}, Lcom/android/camera/actor/VideoActor;->access$600(Lcom/android/camera/actor/VideoActor;)Z

    iget-object v1, p0, Lcom/android/camera/actor/VideoActor$9;->this$0:Lcom/android/camera/actor/VideoActor;

    invoke-static {v1, v5}, Lcom/android/camera/actor/VideoActor;->access$2202(Lcom/android/camera/actor/VideoActor;Z)Z

    invoke-static {}, Lcom/android/camera/actor/VideoActor;->access$300()Z

    move-result v1

    if-eqz v1, :cond_7

    const-string v1, "VideoActor"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "mVideoSavedRunnable.run() end mRecorderBusy="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/android/camera/actor/VideoActor$9;->this$0:Lcom/android/camera/actor/VideoActor;

    invoke-static {v3}, Lcom/android/camera/actor/VideoActor;->access$2200(Lcom/android/camera/actor/VideoActor;)Z

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/android/camera/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    :cond_7
    return-void

    :cond_8
    iget-object v1, p0, Lcom/android/camera/actor/VideoActor$9;->this$0:Lcom/android/camera/actor/VideoActor;

    invoke-static {v1}, Lcom/android/camera/actor/VideoActor;->access$2700(Lcom/android/camera/actor/VideoActor;)I

    move-result v0

    goto :goto_0

    :pswitch_0
    iget-object v1, p0, Lcom/android/camera/actor/VideoActor$9;->this$0:Lcom/android/camera/actor/VideoActor;

    invoke-static {v1}, Lcom/android/camera/actor/VideoActor;->access$700(Lcom/android/camera/actor/VideoActor;)Z

    move-result v1

    if-nez v1, :cond_3

    iget-object v1, p0, Lcom/android/camera/actor/VideoActor$9;->this$0:Lcom/android/camera/actor/VideoActor;

    invoke-static {v1}, Lcom/android/camera/actor/VideoActor;->access$2100(Lcom/android/camera/actor/VideoActor;)Z

    move-result v1

    if-nez v1, :cond_3

    iget-object v1, p0, Lcom/android/camera/actor/VideoActor$9;->this$0:Lcom/android/camera/actor/VideoActor;

    invoke-static {v1}, Lcom/android/camera/actor/VideoActor;->access$400(Lcom/android/camera/actor/VideoActor;)Lcom/android/camera/Camera;

    move-result-object v1

    invoke-virtual {v1}, Lcom/android/camera/Camera;->animateCapture()V

    goto/16 :goto_1

    :pswitch_1
    iget-object v1, p0, Lcom/android/camera/actor/VideoActor$9;->this$0:Lcom/android/camera/actor/VideoActor;

    invoke-static {v1}, Lcom/android/camera/actor/VideoActor;->access$3800(Lcom/android/camera/actor/VideoActor;)V

    goto/16 :goto_1

    :pswitch_2
    iget-object v1, p0, Lcom/android/camera/actor/VideoActor$9;->this$0:Lcom/android/camera/actor/VideoActor;

    invoke-static {v1, v5}, Lcom/android/camera/actor/VideoActor;->access$3900(Lcom/android/camera/actor/VideoActor;Z)V

    goto/16 :goto_1

    :pswitch_3
    iget-object v1, p0, Lcom/android/camera/actor/VideoActor$9;->this$0:Lcom/android/camera/actor/VideoActor;

    invoke-static {v1, v4}, Lcom/android/camera/actor/VideoActor;->access$3900(Lcom/android/camera/actor/VideoActor;Z)V

    goto/16 :goto_1

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_3
        :pswitch_2
        :pswitch_1
    .end packed-switch
.end method
