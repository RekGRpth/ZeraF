.class final Lcom/android/camera/actor/VideoActor$JpegPictureCallback;
.super Ljava/lang/Object;
.source "VideoActor.java"

# interfaces
.implements Landroid/hardware/Camera$PictureCallback;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/camera/actor/VideoActor;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x12
    name = "JpegPictureCallback"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/camera/actor/VideoActor;


# direct methods
.method public constructor <init>(Lcom/android/camera/actor/VideoActor;Landroid/location/Location;)V
    .locals 0
    .param p2    # Landroid/location/Location;

    iput-object p1, p0, Lcom/android/camera/actor/VideoActor$JpegPictureCallback;->this$0:Lcom/android/camera/actor/VideoActor;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onPictureTaken([BLandroid/hardware/Camera;)V
    .locals 2
    .param p1    # [B
    .param p2    # Landroid/hardware/Camera;

    invoke-static {}, Lcom/android/camera/actor/VideoActor;->access$300()Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v0, "VideoActor"

    const-string v1, "onPictureTaken,storeImage"

    invoke-static {v0, v1}, Lcom/android/camera/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    iget-object v0, p0, Lcom/android/camera/actor/VideoActor$JpegPictureCallback;->this$0:Lcom/android/camera/actor/VideoActor;

    invoke-static {v0}, Lcom/android/camera/actor/VideoActor;->access$1200(Lcom/android/camera/actor/VideoActor;)Lcom/android/camera/SaveRequest;

    move-result-object v0

    invoke-interface {v0, p1}, Lcom/android/camera/SaveRequest;->setData([B)V

    iget-object v0, p0, Lcom/android/camera/actor/VideoActor$JpegPictureCallback;->this$0:Lcom/android/camera/actor/VideoActor;

    invoke-static {v0}, Lcom/android/camera/actor/VideoActor;->access$1200(Lcom/android/camera/actor/VideoActor;)Lcom/android/camera/SaveRequest;

    move-result-object v0

    invoke-interface {v0}, Lcom/android/camera/SaveRequest;->addRequest()V

    iget-object v0, p0, Lcom/android/camera/actor/VideoActor$JpegPictureCallback;->this$0:Lcom/android/camera/actor/VideoActor;

    invoke-static {v0}, Lcom/android/camera/actor/VideoActor;->access$3200(Lcom/android/camera/actor/VideoActor;)Landroid/os/Handler;

    move-result-object v0

    const/16 v1, 0xf

    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    return-void
.end method
