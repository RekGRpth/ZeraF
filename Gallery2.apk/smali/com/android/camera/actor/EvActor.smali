.class public Lcom/android/camera/actor/EvActor;
.super Lcom/android/camera/actor/PhotoActor;
.source "EvActor.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/camera/actor/EvActor$EvCameraCategory;
    }
.end annotation


# static fields
.field private static final LOG:Z

.field private static final MAX_EV_NUM:I = 0x3

.field private static final SLEEP_TIME_FOR_SHUTTER_SOUND:I = 0x8c

.field private static final TAG:Ljava/lang/String; = "EvActor"


# instance fields
.field private mCancelListener:Landroid/view/View$OnClickListener;

.field private mCurrentEVNum:I

.field private mEVJpegPictureMultiCallback:Landroid/hardware/Camera$PictureCallback;

.field private mOkListener:Landroid/view/View$OnClickListener;

.field private mPickImageViewManager:Lcom/android/camera/manager/PickImageViewManager;

.field private mResumable:Lcom/android/camera/Camera$Resumable;

.field private mSaveRequest:Lcom/android/camera/SaveRequest;

.field private mSaveRequests:[Lcom/android/camera/SaveRequest;

.field private mSelectedChangedListener:Lcom/android/camera/manager/PickImageViewManager$SelectedChangedListener;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    sget-boolean v0, Lcom/android/camera/Log;->LOGV:Z

    sput-boolean v0, Lcom/android/camera/actor/EvActor;->LOG:Z

    return-void
.end method

.method public constructor <init>(Lcom/android/camera/Camera;)V
    .locals 2
    .param p1    # Lcom/android/camera/Camera;

    invoke-direct {p0, p1}, Lcom/android/camera/actor/PhotoActor;-><init>(Lcom/android/camera/Camera;)V

    const/4 v0, 0x0

    iput v0, p0, Lcom/android/camera/actor/EvActor;->mCurrentEVNum:I

    new-instance v0, Lcom/android/camera/actor/EvActor$1;

    invoke-direct {v0, p0}, Lcom/android/camera/actor/EvActor$1;-><init>(Lcom/android/camera/actor/EvActor;)V

    iput-object v0, p0, Lcom/android/camera/actor/EvActor;->mEVJpegPictureMultiCallback:Landroid/hardware/Camera$PictureCallback;

    new-instance v0, Lcom/android/camera/actor/EvActor$2;

    invoke-direct {v0, p0}, Lcom/android/camera/actor/EvActor$2;-><init>(Lcom/android/camera/actor/EvActor;)V

    iput-object v0, p0, Lcom/android/camera/actor/EvActor;->mSelectedChangedListener:Lcom/android/camera/manager/PickImageViewManager$SelectedChangedListener;

    new-instance v0, Lcom/android/camera/actor/EvActor$3;

    invoke-direct {v0, p0}, Lcom/android/camera/actor/EvActor$3;-><init>(Lcom/android/camera/actor/EvActor;)V

    iput-object v0, p0, Lcom/android/camera/actor/EvActor;->mOkListener:Landroid/view/View$OnClickListener;

    new-instance v0, Lcom/android/camera/actor/EvActor$4;

    invoke-direct {v0, p0}, Lcom/android/camera/actor/EvActor$4;-><init>(Lcom/android/camera/actor/EvActor;)V

    iput-object v0, p0, Lcom/android/camera/actor/EvActor;->mCancelListener:Landroid/view/View$OnClickListener;

    new-instance v0, Lcom/android/camera/actor/EvActor$5;

    invoke-direct {v0, p0}, Lcom/android/camera/actor/EvActor$5;-><init>(Lcom/android/camera/actor/EvActor;)V

    iput-object v0, p0, Lcom/android/camera/actor/EvActor;->mResumable:Lcom/android/camera/Camera$Resumable;

    sget-boolean v0, Lcom/android/camera/actor/EvActor;->LOG:Z

    if-eqz v0, :cond_0

    const-string v0, "EvActor"

    const-string v1, "EvActor initialize"

    invoke-static {v0, v1}, Lcom/android/camera/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    new-instance v0, Lcom/android/camera/actor/EvActor$EvCameraCategory;

    invoke-direct {v0, p0}, Lcom/android/camera/actor/EvActor$EvCameraCategory;-><init>(Lcom/android/camera/actor/EvActor;)V

    iput-object v0, p0, Lcom/android/camera/actor/PhotoActor;->mCameraCategory:Lcom/android/camera/actor/PhotoActor$CameraCategory;

    iget-object v0, p0, Lcom/android/camera/actor/PhotoActor;->mCamera:Lcom/android/camera/Camera;

    iget-object v1, p0, Lcom/android/camera/actor/EvActor;->mResumable:Lcom/android/camera/Camera$Resumable;

    invoke-virtual {v0, v1}, Lcom/android/camera/Camera;->addResumable(Lcom/android/camera/Camera$Resumable;)Z

    return-void
.end method

.method static synthetic access$000(Lcom/android/camera/actor/EvActor;)I
    .locals 1
    .param p0    # Lcom/android/camera/actor/EvActor;

    iget v0, p0, Lcom/android/camera/actor/EvActor;->mCurrentEVNum:I

    return v0
.end method

.method static synthetic access$002(Lcom/android/camera/actor/EvActor;I)I
    .locals 0
    .param p0    # Lcom/android/camera/actor/EvActor;
    .param p1    # I

    iput p1, p0, Lcom/android/camera/actor/EvActor;->mCurrentEVNum:I

    return p1
.end method

.method static synthetic access$008(Lcom/android/camera/actor/EvActor;)I
    .locals 2
    .param p0    # Lcom/android/camera/actor/EvActor;

    iget v0, p0, Lcom/android/camera/actor/EvActor;->mCurrentEVNum:I

    add-int/lit8 v1, v0, 0x1

    iput v1, p0, Lcom/android/camera/actor/EvActor;->mCurrentEVNum:I

    return v0
.end method

.method static synthetic access$100(Lcom/android/camera/actor/EvActor;[BI)V
    .locals 0
    .param p0    # Lcom/android/camera/actor/EvActor;
    .param p1    # [B
    .param p2    # I

    invoke-direct {p0, p1, p2}, Lcom/android/camera/actor/EvActor;->saveEvPictureForMultiCallBack([BI)V

    return-void
.end method

.method static synthetic access$1002(Lcom/android/camera/actor/EvActor;Lcom/android/camera/SaveRequest;)Lcom/android/camera/SaveRequest;
    .locals 0
    .param p0    # Lcom/android/camera/actor/EvActor;
    .param p1    # Lcom/android/camera/SaveRequest;

    iput-object p1, p0, Lcom/android/camera/actor/EvActor;->mSaveRequest:Lcom/android/camera/SaveRequest;

    return-object p1
.end method

.method static synthetic access$1100(Lcom/android/camera/actor/EvActor;)Landroid/hardware/Camera$PictureCallback;
    .locals 1
    .param p0    # Lcom/android/camera/actor/EvActor;

    iget-object v0, p0, Lcom/android/camera/actor/EvActor;->mEVJpegPictureMultiCallback:Landroid/hardware/Camera$PictureCallback;

    return-object v0
.end method

.method static synthetic access$200(Lcom/android/camera/actor/EvActor;)V
    .locals 0
    .param p0    # Lcom/android/camera/actor/EvActor;

    invoke-direct {p0}, Lcom/android/camera/actor/EvActor;->multiCallBackfireEvSelector()V

    return-void
.end method

.method static synthetic access$300()Z
    .locals 1

    sget-boolean v0, Lcom/android/camera/actor/EvActor;->LOG:Z

    return v0
.end method

.method static synthetic access$400(Lcom/android/camera/actor/EvActor;I)Z
    .locals 1
    .param p0    # Lcom/android/camera/actor/EvActor;
    .param p1    # I

    invoke-direct {p0, p1}, Lcom/android/camera/actor/EvActor;->saveOrDelete(I)Z

    move-result v0

    return v0
.end method

.method static synthetic access$500(Lcom/android/camera/actor/EvActor;)[Lcom/android/camera/SaveRequest;
    .locals 1
    .param p0    # Lcom/android/camera/actor/EvActor;

    iget-object v0, p0, Lcom/android/camera/actor/EvActor;->mSaveRequests:[Lcom/android/camera/SaveRequest;

    return-object v0
.end method

.method static synthetic access$502(Lcom/android/camera/actor/EvActor;[Lcom/android/camera/SaveRequest;)[Lcom/android/camera/SaveRequest;
    .locals 0
    .param p0    # Lcom/android/camera/actor/EvActor;
    .param p1    # [Lcom/android/camera/SaveRequest;

    iput-object p1, p0, Lcom/android/camera/actor/EvActor;->mSaveRequests:[Lcom/android/camera/SaveRequest;

    return-object p1
.end method

.method static synthetic access$600(Lcom/android/camera/actor/EvActor;)V
    .locals 0
    .param p0    # Lcom/android/camera/actor/EvActor;

    invoke-direct {p0}, Lcom/android/camera/actor/EvActor;->clearRequest()V

    return-void
.end method

.method static synthetic access$700(Lcom/android/camera/actor/EvActor;)Lcom/android/camera/manager/PickImageViewManager;
    .locals 1
    .param p0    # Lcom/android/camera/actor/EvActor;

    iget-object v0, p0, Lcom/android/camera/actor/EvActor;->mPickImageViewManager:Lcom/android/camera/manager/PickImageViewManager;

    return-object v0
.end method

.method static synthetic access$702(Lcom/android/camera/actor/EvActor;Lcom/android/camera/manager/PickImageViewManager;)Lcom/android/camera/manager/PickImageViewManager;
    .locals 0
    .param p0    # Lcom/android/camera/actor/EvActor;
    .param p1    # Lcom/android/camera/manager/PickImageViewManager;

    iput-object p1, p0, Lcom/android/camera/actor/EvActor;->mPickImageViewManager:Lcom/android/camera/manager/PickImageViewManager;

    return-object p1
.end method

.method static synthetic access$800(Lcom/android/camera/actor/EvActor;)V
    .locals 0
    .param p0    # Lcom/android/camera/actor/EvActor;

    invoke-direct {p0}, Lcom/android/camera/actor/EvActor;->setAspectRatio()V

    return-void
.end method

.method static synthetic access$900(Lcom/android/camera/actor/EvActor;)Lcom/android/camera/manager/PickImageViewManager$SelectedChangedListener;
    .locals 1
    .param p0    # Lcom/android/camera/actor/EvActor;

    iget-object v0, p0, Lcom/android/camera/actor/EvActor;->mSelectedChangedListener:Lcom/android/camera/manager/PickImageViewManager$SelectedChangedListener;

    return-object v0
.end method

.method private clearRequest()V
    .locals 4

    sget-boolean v1, Lcom/android/camera/actor/EvActor;->LOG:Z

    if-eqz v1, :cond_0

    const-string v1, "EvActor"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "clearRequest mSaveRequests="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/android/camera/actor/EvActor;->mSaveRequests:[Lcom/android/camera/SaveRequest;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/android/camera/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    iget-object v1, p0, Lcom/android/camera/actor/EvActor;->mSaveRequests:[Lcom/android/camera/SaveRequest;

    if-eqz v1, :cond_3

    const/4 v0, 0x0

    :goto_0
    iget-object v1, p0, Lcom/android/camera/actor/EvActor;->mSaveRequests:[Lcom/android/camera/SaveRequest;

    array-length v1, v1

    if-ge v0, v1, :cond_2

    iget-object v1, p0, Lcom/android/camera/actor/EvActor;->mSaveRequests:[Lcom/android/camera/SaveRequest;

    aget-object v1, v1, v0

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/android/camera/actor/EvActor;->mSaveRequests:[Lcom/android/camera/SaveRequest;

    aget-object v1, v1, v0

    invoke-interface {v1}, Lcom/android/camera/SaveRequest;->getTempFilePath()Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_1

    new-instance v1, Ljava/io/File;

    iget-object v2, p0, Lcom/android/camera/actor/EvActor;->mSaveRequests:[Lcom/android/camera/SaveRequest;

    aget-object v2, v2, v0

    invoke-interface {v2}, Lcom/android/camera/SaveRequest;->getTempFilePath()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1}, Ljava/io/File;->delete()Z

    :cond_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_2
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/android/camera/actor/EvActor;->mSaveRequests:[Lcom/android/camera/SaveRequest;

    :cond_3
    return-void
.end method

.method private multiCallBackfireEvSelector()V
    .locals 4

    iget-boolean v1, p0, Lcom/android/camera/actor/PhotoActor;->mCameraClosed:Z

    if-eqz v1, :cond_0

    :goto_0
    return-void

    :cond_0
    iget-object v1, p0, Lcom/android/camera/actor/EvActor;->mPickImageViewManager:Lcom/android/camera/manager/PickImageViewManager;

    invoke-virtual {v1}, Lcom/android/camera/manager/PickImageViewManager;->show()V

    iget-object v1, p0, Lcom/android/camera/actor/EvActor;->mPickImageViewManager:Lcom/android/camera/manager/PickImageViewManager;

    iget-object v2, p0, Lcom/android/camera/actor/EvActor;->mSaveRequests:[Lcom/android/camera/SaveRequest;

    invoke-virtual {v1, v2}, Lcom/android/camera/manager/PickImageViewManager;->setSaveRequests([Lcom/android/camera/SaveRequest;)V

    iget-object v1, p0, Lcom/android/camera/actor/EvActor;->mPickImageViewManager:Lcom/android/camera/manager/PickImageViewManager;

    invoke-virtual {v1}, Lcom/android/camera/manager/PickImageViewManager;->displayImages()V

    iget-object v1, p0, Lcom/android/camera/actor/PhotoActor;->mCamera:Lcom/android/camera/Camera;

    const/4 v2, 0x3

    invoke-virtual {v1, v2}, Lcom/android/camera/Camera;->switchShutter(I)V

    iget-object v1, p0, Lcom/android/camera/actor/PhotoActor;->mCamera:Lcom/android/camera/Camera;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Lcom/android/camera/Camera;->setCameraState(I)V

    iget-object v1, p0, Lcom/android/camera/actor/PhotoActor;->mCamera:Lcom/android/camera/Camera;

    const/16 v2, 0x8

    invoke-virtual {v1, v2}, Lcom/android/camera/Camera;->setViewState(I)V

    iget-object v1, p0, Lcom/android/camera/actor/PhotoActor;->mCamera:Lcom/android/camera/Camera;

    const/4 v2, 0x1

    const/4 v3, -0x1

    invoke-virtual {v1, v2, v3}, Lcom/android/camera/Camera;->setOrientation(ZI)V

    :try_start_0
    const-string v1, "EvActor"

    const-string v2, "sleep 140ms for shuttersound"

    invoke-static {v1, v2}, Lcom/android/camera/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    const-wide/16 v1, 0x8c

    invoke-static {v1, v2}, Ljava/lang/Thread;->sleep(J)V
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Ljava/lang/Throwable;->printStackTrace()V

    goto :goto_0
.end method

.method private saveEvPictureForMultiCallBack([BI)V
    .locals 4
    .param p1    # [B
    .param p2    # I

    const/4 v1, 0x0

    iget-boolean v0, p0, Lcom/android/camera/actor/PhotoActor;->mCameraClosed:Z

    if-eqz v0, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v0, p0, Lcom/android/camera/actor/EvActor;->mSaveRequest:Lcom/android/camera/SaveRequest;

    invoke-interface {v0, p1}, Lcom/android/camera/SaveRequest;->setData([B)V

    iget-object v2, p0, Lcom/android/camera/actor/EvActor;->mSaveRequest:Lcom/android/camera/SaveRequest;

    iget-object v0, p0, Lcom/android/camera/actor/PhotoActor;->mCamera:Lcom/android/camera/Camera;

    invoke-virtual {v0}, Lcom/android/camera/Camera;->isNonePickIntent()Z

    move-result v0

    if-nez v0, :cond_2

    const/4 v0, 0x1

    :goto_1
    invoke-interface {v2, v0}, Lcom/android/camera/SaveRequest;->setIgnoreThumbnail(Z)V

    iget-object v0, p0, Lcom/android/camera/actor/EvActor;->mSaveRequest:Lcom/android/camera/SaveRequest;

    invoke-interface {v0}, Lcom/android/camera/SaveRequest;->saveSync()V

    iget-object v0, p0, Lcom/android/camera/actor/EvActor;->mSaveRequests:[Lcom/android/camera/SaveRequest;

    add-int/lit8 v2, p2, -0x1

    iget-object v3, p0, Lcom/android/camera/actor/EvActor;->mSaveRequest:Lcom/android/camera/SaveRequest;

    aput-object v3, v0, v2

    const/4 v0, 0x3

    if-ge p2, v0, :cond_0

    iget-object v0, p0, Lcom/android/camera/actor/PhotoActor;->mCamera:Lcom/android/camera/Camera;

    const/4 v2, 0x2

    invoke-virtual {v0, v2, v1}, Lcom/android/camera/Camera;->preparePhotoRequest(II)Lcom/android/camera/SaveRequest;

    move-result-object v0

    iput-object v0, p0, Lcom/android/camera/actor/EvActor;->mSaveRequest:Lcom/android/camera/SaveRequest;

    goto :goto_0

    :cond_2
    move v0, v1

    goto :goto_1
.end method

.method private saveOrDelete(I)Z
    .locals 4
    .param p1    # I

    sget-boolean v1, Lcom/android/camera/actor/EvActor;->LOG:Z

    if-eqz v1, :cond_0

    const-string v1, "EvActor"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "saveOrDelete idx="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/android/camera/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    const/4 v0, 0x1

    iget-object v1, p0, Lcom/android/camera/actor/EvActor;->mPickImageViewManager:Lcom/android/camera/manager/PickImageViewManager;

    invoke-virtual {v1, p1}, Lcom/android/camera/manager/PickImageViewManager;->isSelected(I)Z

    move-result v1

    if-nez v1, :cond_1

    iget-object v1, p0, Lcom/android/camera/actor/EvActor;->mSaveRequests:[Lcom/android/camera/SaveRequest;

    aget-object v1, v1, p1

    invoke-interface {v1}, Lcom/android/camera/SaveRequest;->getTempFilePath()Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_1

    new-instance v1, Ljava/io/File;

    iget-object v2, p0, Lcom/android/camera/actor/EvActor;->mSaveRequests:[Lcom/android/camera/SaveRequest;

    aget-object v2, v2, p1

    invoke-interface {v2}, Lcom/android/camera/SaveRequest;->getTempFilePath()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1}, Ljava/io/File;->delete()Z

    const/4 v0, 0x0

    :cond_1
    return v0
.end method

.method private setAspectRatio()V
    .locals 8

    iget-object v3, p0, Lcom/android/camera/actor/EvActor;->mPickImageViewManager:Lcom/android/camera/manager/PickImageViewManager;

    if-eqz v3, :cond_0

    iget-object v3, p0, Lcom/android/camera/actor/PhotoActor;->mCamera:Lcom/android/camera/Camera;

    invoke-virtual {v3}, Lcom/android/camera/Camera;->getParameters()Landroid/hardware/Camera$Parameters;

    move-result-object v3

    if-eqz v3, :cond_0

    iget-object v3, p0, Lcom/android/camera/actor/PhotoActor;->mCamera:Lcom/android/camera/Camera;

    invoke-virtual {v3}, Lcom/android/camera/Camera;->getParameters()Landroid/hardware/Camera$Parameters;

    move-result-object v3

    invoke-virtual {v3}, Landroid/hardware/Camera$Parameters;->getPictureSize()Landroid/hardware/Camera$Size;

    move-result-object v1

    iget v3, v1, Landroid/hardware/Camera$Size;->width:I

    iget v4, v1, Landroid/hardware/Camera$Size;->height:I

    invoke-static {v3, v4}, Ljava/lang/Math;->max(II)I

    move-result v2

    iget v3, v1, Landroid/hardware/Camera$Size;->width:I

    iget v4, v1, Landroid/hardware/Camera$Size;->height:I

    invoke-static {v3, v4}, Ljava/lang/Math;->min(II)I

    move-result v0

    iget-object v3, p0, Lcom/android/camera/actor/EvActor;->mPickImageViewManager:Lcom/android/camera/manager/PickImageViewManager;

    int-to-double v4, v2

    int-to-double v6, v0

    div-double/2addr v4, v6

    invoke-virtual {v3, v4, v5}, Lcom/android/camera/manager/PickImageViewManager;->setAspectRatio(D)V

    :cond_0
    return-void
.end method


# virtual methods
.method public getCancelListener()Landroid/view/View$OnClickListener;
    .locals 1

    iget-object v0, p0, Lcom/android/camera/actor/EvActor;->mCancelListener:Landroid/view/View$OnClickListener;

    return-object v0
.end method

.method public getMode()I
    .locals 1

    const/16 v0, 0x8

    return v0
.end method

.method public getOkListener()Landroid/view/View$OnClickListener;
    .locals 1

    iget-object v0, p0, Lcom/android/camera/actor/EvActor;->mOkListener:Landroid/view/View$OnClickListener;

    return-object v0
.end method

.method public getPhotoShutterButtonListener()Lcom/android/camera/ui/ShutterButton$OnShutterButtonListener;
    .locals 0

    return-object p0
.end method

.method public onBackPressed()Z
    .locals 3

    sget-boolean v0, Lcom/android/camera/actor/EvActor;->LOG:Z

    if-eqz v0, :cond_0

    const-string v0, "EvActor"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "onBackPressed() mPickImageViewManager.isShowing()="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/android/camera/actor/EvActor;->mPickImageViewManager:Lcom/android/camera/manager/PickImageViewManager;

    invoke-virtual {v2}, Lcom/android/camera/manager/ViewManager;->isShowing()Z

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/android/camera/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    iget-object v0, p0, Lcom/android/camera/actor/EvActor;->mPickImageViewManager:Lcom/android/camera/manager/PickImageViewManager;

    invoke-virtual {v0}, Lcom/android/camera/manager/ViewManager;->isShowing()Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/android/camera/actor/EvActor;->mCancelListener:Landroid/view/View$OnClickListener;

    const/4 v1, 0x0

    invoke-interface {v0, v1}, Landroid/view/View$OnClickListener;->onClick(Landroid/view/View;)V

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    invoke-super {p0}, Lcom/android/camera/actor/PhotoActor;->onBackPressed()Z

    move-result v0

    goto :goto_0
.end method

.method public onCameraParameterReady(Z)V
    .locals 2
    .param p1    # Z

    invoke-super {p0, p1}, Lcom/android/camera/actor/PhotoActor;->onCameraParameterReady(Z)V

    sget-boolean v0, Lcom/android/camera/actor/EvActor;->LOG:Z

    if-eqz v0, :cond_0

    const-string v0, "EvActor"

    const-string v1, "EvActor onCameraParameterReady"

    invoke-static {v0, v1}, Lcom/android/camera/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    invoke-direct {p0}, Lcom/android/camera/actor/EvActor;->setAspectRatio()V

    return-void
.end method

.method public onShutterButtonLongPressed(Lcom/android/camera/ui/ShutterButton;)V
    .locals 4
    .param p1    # Lcom/android/camera/ui/ShutterButton;

    sget-boolean v0, Lcom/android/camera/actor/EvActor;->LOG:Z

    if-eqz v0, :cond_0

    const-string v0, "EvActor"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Ev.onShutterButtonLongPressed("

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ")"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/android/camera/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    iget-object v0, p0, Lcom/android/camera/actor/PhotoActor;->mCamera:Lcom/android/camera/Camera;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v2, p0, Lcom/android/camera/actor/PhotoActor;->mCamera:Lcom/android/camera/Camera;

    const v3, 0x7f0c0027

    invoke-virtual {v2, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/android/camera/actor/PhotoActor;->mCamera:Lcom/android/camera/Camera;

    const v3, 0x7f0c000d

    invoke-virtual {v2, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/android/camera/Camera;->showInfo(Ljava/lang/String;)V

    return-void
.end method

.method public release()V
    .locals 2

    sget-boolean v0, Lcom/android/camera/actor/EvActor;->LOG:Z

    if-eqz v0, :cond_0

    const-string v0, "EvActor"

    const-string v1, "EV.release()"

    invoke-static {v0, v1}, Lcom/android/camera/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    invoke-super {p0}, Lcom/android/camera/actor/PhotoActor;->release()V

    iget-object v0, p0, Lcom/android/camera/actor/EvActor;->mPickImageViewManager:Lcom/android/camera/manager/PickImageViewManager;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/android/camera/actor/EvActor;->mPickImageViewManager:Lcom/android/camera/manager/PickImageViewManager;

    invoke-virtual {v0}, Lcom/android/camera/manager/ViewManager;->release()V

    :cond_1
    iget-object v0, p0, Lcom/android/camera/actor/PhotoActor;->mCamera:Lcom/android/camera/Camera;

    iget-object v1, p0, Lcom/android/camera/actor/EvActor;->mResumable:Lcom/android/camera/Camera$Resumable;

    invoke-virtual {v0, v1}, Lcom/android/camera/Camera;->removeResumable(Lcom/android/camera/Camera$Resumable;)Z

    return-void
.end method

.method protected restartPreview(Z)V
    .locals 4
    .param p1    # Z

    const/4 v3, 0x0

    sget-boolean v0, Lcom/android/camera/actor/EvActor;->LOG:Z

    if-eqz v0, :cond_0

    const-string v0, "EvActor"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "restartPreview camerastate="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/android/camera/actor/PhotoActor;->mCamera:Lcom/android/camera/Camera;

    invoke-virtual {v2}, Lcom/android/camera/Camera;->getCameraState()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " needStop="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/android/camera/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    iget-object v0, p0, Lcom/android/camera/actor/EvActor;->mPickImageViewManager:Lcom/android/camera/manager/PickImageViewManager;

    invoke-virtual {v0}, Lcom/android/camera/manager/ViewManager;->hide()V

    iget-object v0, p0, Lcom/android/camera/actor/EvActor;->mPickImageViewManager:Lcom/android/camera/manager/PickImageViewManager;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/android/camera/manager/PickImageViewManager;->setSaveRequests([Lcom/android/camera/SaveRequest;)V

    iget-object v0, p0, Lcom/android/camera/actor/PhotoActor;->mCamera:Lcom/android/camera/Camera;

    invoke-virtual {v0, v3}, Lcom/android/camera/Camera;->switchShutter(I)V

    iget-object v0, p0, Lcom/android/camera/actor/PhotoActor;->mCamera:Lcom/android/camera/Camera;

    invoke-virtual {v0}, Lcom/android/camera/Camera;->restoreViewState()V

    iget-object v0, p0, Lcom/android/camera/actor/PhotoActor;->mCamera:Lcom/android/camera/Camera;

    const/4 v1, -0x1

    invoke-virtual {v0, v3, v1}, Lcom/android/camera/Camera;->setOrientation(ZI)V

    iget-object v0, p0, Lcom/android/camera/actor/PhotoActor;->mCamera:Lcom/android/camera/Camera;

    invoke-virtual {v0}, Lcom/android/camera/Camera;->getCameraState()I

    move-result v0

    if-nez v0, :cond_1

    const/4 v0, 0x1

    invoke-super {p0, v0}, Lcom/android/camera/actor/PhotoActor;->restartPreview(Z)V

    :cond_1
    return-void
.end method
