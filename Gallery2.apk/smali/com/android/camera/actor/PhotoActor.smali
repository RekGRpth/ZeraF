.class public Lcom/android/camera/actor/PhotoActor;
.super Lcom/android/camera/actor/CameraActor;
.source "PhotoActor.java"

# interfaces
.implements Landroid/hardware/Camera$ContinuousShotDone;
.implements Lcom/android/camera/FocusManager$Listener;
.implements Lcom/android/camera/ui/ShutterButton$OnShutterButtonListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/camera/actor/PhotoActor$MemoryManager;,
        Lcom/android/camera/actor/PhotoActor$CameraCategory;,
        Lcom/android/camera/actor/PhotoActor$AutoFocusMoveCallback;,
        Lcom/android/camera/actor/PhotoActor$AutoFocusCallback;,
        Lcom/android/camera/actor/PhotoActor$MainHandler;,
        Lcom/android/camera/actor/PhotoActor$WaitSavingDoneThread;,
        Lcom/android/camera/actor/PhotoActor$RenderInCapture;
    }
.end annotation


# static fields
.field private static final BURST_SAVING_DONE:I = 0x65

.field private static final IMAGE_DISPLAY_DURATION:I = 0x4b0

.field private static final IMAGE_PICK_SAVING_DONE:I = 0x66

.field private static final LOG:Z

.field private static final PARAMETER_CHANGE_DONE:I = 0x67

.field private static final REQUEST_CROP:I = 0x3e8

.field private static final SKIP_FOCUS_ON_CAPTURE:Z = true

.field private static final START_PREVIEW_DONE:I = 0x64

.field private static final TAG:Ljava/lang/String; = "PhotoActor"

.field private static final TEMP_CROP_FILE_NAME:Ljava/lang/String; = "crop-temp"

.field private static final THUMBNAIL_REFRESH_CONTINUOUS:I = 0x1f4

.field private static final THUMBNAIL_REFRESH_NORMAL:I

.field protected static sFaceDetectionStarted:Z

.field private static sFaceDetectionSync:Ljava/lang/Object;

.field private static sIsAutoFocusCallback:Z


# instance fields
.field private final mAutoFocusCallback:Lcom/android/camera/actor/PhotoActor$AutoFocusCallback;

.field private final mAutoFocusMoveCallback:Lcom/android/camera/actor/PhotoActor$AutoFocusMoveCallback;

.field public mAutoFocusTime:J

.field private mBurstSound:Landroid/media/SoundPool;

.field protected mCamera:Lcom/android/camera/Camera;

.field protected mCameraCategory:Lcom/android/camera/actor/PhotoActor$CameraCategory;

.field protected mCameraClosed:Z

.field private mCameraKeyLongPressed:Z

.field private mCameraSound:Landroid/media/MediaActionSound;

.field private mCancelListener:Landroid/view/View$OnClickListener;

.field public mCaptureStartTime:J

.field protected mCapturing:Z

.field private mContinuousJpegPictureCallback:Landroid/hardware/Camera$PictureCallback;

.field private mContinuousShotPerformed:Z

.field private mContinuousShotStartTime:J

.field private mCurrentShotsNum:I

.field private mDoSnapRunnable:Ljava/lang/Runnable;

.field private mFaceDetectionListener:Landroid/hardware/Camera$FaceDetectionListener;

.field private mFocusStartTime:J

.field protected final mHandler:Landroid/os/Handler;

.field private mIgnoreClick:Z

.field protected mInitialized:Z

.field public mJpegCallbackFinishTime:J

.field protected mJpegImageData:[B

.field private mJpegPictureCallback:Landroid/hardware/Camera$PictureCallback;

.field private mJpegPictureCallbackTime:J

.field private mKeyHalfPressed:Z

.field private mMaxCaptureNum:I

.field private mMemoryManager:Lcom/android/camera/actor/PhotoActor$MemoryManager;

.field private mOkListener:Landroid/view/View$OnClickListener;

.field private mOnSingleTapListener:Lcom/android/camera/Camera$OnSingleTapUpListener;

.field public mPictureDisplayedToJpegCallbackTime:J

.field private mPostViewPictureCallback:Landroid/hardware/Camera$PictureCallback;

.field protected mPostViewPictureCallbackTime:J

.field private mRawPictureCallback:Landroid/hardware/Camera$PictureCallback;

.field private mRawPictureCallbackTime:J

.field private mRenderThread:Lcom/android/camera/actor/PhotoActor$RenderInCapture;

.field private mRetakeListener:Landroid/view/View$OnClickListener;

.field protected mSaveRequest:Lcom/android/camera/SaveRequest;

.field private mSavingPictures:Z

.field private mSelfTimerListener:Lcom/android/camera/manager/SelfTimerManager$SelfTimerListener;

.field private mSelftimerCounting:Z

.field private mShutterCallback:Landroid/hardware/Camera$ShutterCallback;

.field private mShutterCallbackTime:J

.field public mShutterLag:J

.field public mShutterToPictureDisplayedTime:J

.field protected mSnapshotOnIdle:Z

.field private mSoundID:I

.field private mStreamID:I

.field private mSupportContinuous:Z

.field private mWaitSavingDoneThread:Ljava/lang/Thread;

.field private mZSDEnabled:Z

.field private mZSDPreviewDone:Landroid/hardware/Camera$ZSDPreviewDone;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    const/4 v1, 0x0

    sget-boolean v0, Lcom/android/camera/Log;->LOGV:Z

    sput-boolean v0, Lcom/android/camera/actor/PhotoActor;->LOG:Z

    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    sput-object v0, Lcom/android/camera/actor/PhotoActor;->sFaceDetectionSync:Ljava/lang/Object;

    sput-boolean v1, Lcom/android/camera/actor/PhotoActor;->sFaceDetectionStarted:Z

    sput-boolean v1, Lcom/android/camera/actor/PhotoActor;->sIsAutoFocusCallback:Z

    return-void
.end method

.method public constructor <init>(Lcom/android/camera/Camera;)V
    .locals 3
    .param p1    # Lcom/android/camera/Camera;

    const/4 v2, 0x0

    const/4 v1, 0x0

    invoke-direct {p0, p1}, Lcom/android/camera/actor/CameraActor;-><init>(Lcom/android/camera/Camera;)V

    sget-boolean v0, Lcom/android/camera/CameraSettings;->SUPPORTED_SHOW_CONINUOUS_SHOT_NUMBER:Z

    if-eqz v0, :cond_0

    const-string v0, "40"

    invoke-static {v0}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v0

    :goto_0
    iput v0, p0, Lcom/android/camera/actor/PhotoActor;->mMaxCaptureNum:I

    iput-boolean v1, p0, Lcom/android/camera/actor/PhotoActor;->mCapturing:Z

    new-instance v0, Lcom/android/camera/actor/PhotoActor$AutoFocusCallback;

    invoke-direct {v0, p0, v2}, Lcom/android/camera/actor/PhotoActor$AutoFocusCallback;-><init>(Lcom/android/camera/actor/PhotoActor;Lcom/android/camera/actor/PhotoActor$1;)V

    iput-object v0, p0, Lcom/android/camera/actor/PhotoActor;->mAutoFocusCallback:Lcom/android/camera/actor/PhotoActor$AutoFocusCallback;

    new-instance v0, Lcom/android/camera/actor/PhotoActor$AutoFocusMoveCallback;

    invoke-direct {v0, p0, v2}, Lcom/android/camera/actor/PhotoActor$AutoFocusMoveCallback;-><init>(Lcom/android/camera/actor/PhotoActor;Lcom/android/camera/actor/PhotoActor$1;)V

    iput-object v0, p0, Lcom/android/camera/actor/PhotoActor;->mAutoFocusMoveCallback:Lcom/android/camera/actor/PhotoActor$AutoFocusMoveCallback;

    new-instance v0, Lcom/android/camera/actor/PhotoActor$MainHandler;

    invoke-direct {v0, p0, v2}, Lcom/android/camera/actor/PhotoActor$MainHandler;-><init>(Lcom/android/camera/actor/PhotoActor;Lcom/android/camera/actor/PhotoActor$1;)V

    iput-object v0, p0, Lcom/android/camera/actor/PhotoActor;->mHandler:Landroid/os/Handler;

    iput-boolean v1, p0, Lcom/android/camera/actor/PhotoActor;->mCameraClosed:Z

    iput-boolean v1, p0, Lcom/android/camera/actor/PhotoActor;->mSelftimerCounting:Z

    iput-boolean v1, p0, Lcom/android/camera/actor/PhotoActor;->mSavingPictures:Z

    iput-boolean v1, p0, Lcom/android/camera/actor/PhotoActor;->mSnapshotOnIdle:Z

    new-instance v0, Lcom/android/camera/actor/PhotoActor$MemoryManager;

    invoke-direct {v0, p0, v2}, Lcom/android/camera/actor/PhotoActor$MemoryManager;-><init>(Lcom/android/camera/actor/PhotoActor;Lcom/android/camera/actor/PhotoActor$1;)V

    iput-object v0, p0, Lcom/android/camera/actor/PhotoActor;->mMemoryManager:Lcom/android/camera/actor/PhotoActor$MemoryManager;

    iput-boolean v1, p0, Lcom/android/camera/actor/PhotoActor;->mContinuousShotPerformed:Z

    iput-boolean v1, p0, Lcom/android/camera/actor/PhotoActor;->mIgnoreClick:Z

    iput v1, p0, Lcom/android/camera/actor/PhotoActor;->mCurrentShotsNum:I

    iput-boolean v1, p0, Lcom/android/camera/actor/PhotoActor;->mSupportContinuous:Z

    iput-boolean v1, p0, Lcom/android/camera/actor/PhotoActor;->mInitialized:Z

    iput-boolean v1, p0, Lcom/android/camera/actor/PhotoActor;->mKeyHalfPressed:Z

    iput-boolean v1, p0, Lcom/android/camera/actor/PhotoActor;->mCameraKeyLongPressed:Z

    new-instance v0, Lcom/android/camera/actor/PhotoActor$1;

    invoke-direct {v0, p0}, Lcom/android/camera/actor/PhotoActor$1;-><init>(Lcom/android/camera/actor/PhotoActor;)V

    iput-object v0, p0, Lcom/android/camera/actor/PhotoActor;->mDoSnapRunnable:Ljava/lang/Runnable;

    new-instance v0, Lcom/android/camera/actor/PhotoActor$2;

    invoke-direct {v0, p0}, Lcom/android/camera/actor/PhotoActor$2;-><init>(Lcom/android/camera/actor/PhotoActor;)V

    iput-object v0, p0, Lcom/android/camera/actor/PhotoActor;->mOkListener:Landroid/view/View$OnClickListener;

    new-instance v0, Lcom/android/camera/actor/PhotoActor$3;

    invoke-direct {v0, p0}, Lcom/android/camera/actor/PhotoActor$3;-><init>(Lcom/android/camera/actor/PhotoActor;)V

    iput-object v0, p0, Lcom/android/camera/actor/PhotoActor;->mCancelListener:Landroid/view/View$OnClickListener;

    new-instance v0, Lcom/android/camera/actor/PhotoActor$4;

    invoke-direct {v0, p0}, Lcom/android/camera/actor/PhotoActor$4;-><init>(Lcom/android/camera/actor/PhotoActor;)V

    iput-object v0, p0, Lcom/android/camera/actor/PhotoActor;->mRetakeListener:Landroid/view/View$OnClickListener;

    new-instance v0, Lcom/android/camera/actor/PhotoActor$5;

    invoke-direct {v0, p0}, Lcom/android/camera/actor/PhotoActor$5;-><init>(Lcom/android/camera/actor/PhotoActor;)V

    iput-object v0, p0, Lcom/android/camera/actor/PhotoActor;->mZSDPreviewDone:Landroid/hardware/Camera$ZSDPreviewDone;

    new-instance v0, Lcom/android/camera/actor/PhotoActor$6;

    invoke-direct {v0, p0}, Lcom/android/camera/actor/PhotoActor$6;-><init>(Lcom/android/camera/actor/PhotoActor;)V

    iput-object v0, p0, Lcom/android/camera/actor/PhotoActor;->mOnSingleTapListener:Lcom/android/camera/Camera$OnSingleTapUpListener;

    new-instance v0, Lcom/android/camera/actor/PhotoActor$7;

    invoke-direct {v0, p0}, Lcom/android/camera/actor/PhotoActor$7;-><init>(Lcom/android/camera/actor/PhotoActor;)V

    iput-object v0, p0, Lcom/android/camera/actor/PhotoActor;->mFaceDetectionListener:Landroid/hardware/Camera$FaceDetectionListener;

    new-instance v0, Lcom/android/camera/actor/PhotoActor$8;

    invoke-direct {v0, p0}, Lcom/android/camera/actor/PhotoActor$8;-><init>(Lcom/android/camera/actor/PhotoActor;)V

    iput-object v0, p0, Lcom/android/camera/actor/PhotoActor;->mSelfTimerListener:Lcom/android/camera/manager/SelfTimerManager$SelfTimerListener;

    new-instance v0, Lcom/android/camera/actor/PhotoActor$9;

    invoke-direct {v0, p0}, Lcom/android/camera/actor/PhotoActor$9;-><init>(Lcom/android/camera/actor/PhotoActor;)V

    iput-object v0, p0, Lcom/android/camera/actor/PhotoActor;->mShutterCallback:Landroid/hardware/Camera$ShutterCallback;

    new-instance v0, Lcom/android/camera/actor/PhotoActor$10;

    invoke-direct {v0, p0}, Lcom/android/camera/actor/PhotoActor$10;-><init>(Lcom/android/camera/actor/PhotoActor;)V

    iput-object v0, p0, Lcom/android/camera/actor/PhotoActor;->mPostViewPictureCallback:Landroid/hardware/Camera$PictureCallback;

    new-instance v0, Lcom/android/camera/actor/PhotoActor$11;

    invoke-direct {v0, p0}, Lcom/android/camera/actor/PhotoActor$11;-><init>(Lcom/android/camera/actor/PhotoActor;)V

    iput-object v0, p0, Lcom/android/camera/actor/PhotoActor;->mRawPictureCallback:Landroid/hardware/Camera$PictureCallback;

    new-instance v0, Lcom/android/camera/actor/PhotoActor$12;

    invoke-direct {v0, p0}, Lcom/android/camera/actor/PhotoActor$12;-><init>(Lcom/android/camera/actor/PhotoActor;)V

    iput-object v0, p0, Lcom/android/camera/actor/PhotoActor;->mContinuousJpegPictureCallback:Landroid/hardware/Camera$PictureCallback;

    new-instance v0, Lcom/android/camera/actor/PhotoActor$13;

    invoke-direct {v0, p0}, Lcom/android/camera/actor/PhotoActor$13;-><init>(Lcom/android/camera/actor/PhotoActor;)V

    iput-object v0, p0, Lcom/android/camera/actor/PhotoActor;->mJpegPictureCallback:Landroid/hardware/Camera$PictureCallback;

    invoke-virtual {p0}, Lcom/android/camera/actor/CameraActor;->getContext()Lcom/android/camera/Camera;

    move-result-object v0

    iput-object v0, p0, Lcom/android/camera/actor/PhotoActor;->mCamera:Lcom/android/camera/Camera;

    new-instance v0, Lcom/android/camera/actor/PhotoActor$CameraCategory;

    invoke-direct {v0, p0}, Lcom/android/camera/actor/PhotoActor$CameraCategory;-><init>(Lcom/android/camera/actor/PhotoActor;)V

    iput-object v0, p0, Lcom/android/camera/actor/PhotoActor;->mCameraCategory:Lcom/android/camera/actor/PhotoActor$CameraCategory;

    iget-object v0, p0, Lcom/android/camera/actor/PhotoActor;->mCamera:Lcom/android/camera/Camera;

    invoke-virtual {v0}, Lcom/android/camera/Camera;->isImageCaptureIntent()Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/android/camera/actor/PhotoActor;->mCamera:Lcom/android/camera/Camera;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/android/camera/Camera;->switchShutter(I)V

    :goto_1
    return-void

    :cond_0
    const-string v0, "20"

    invoke-static {v0}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v0

    goto/16 :goto_0

    :cond_1
    iget-object v0, p0, Lcom/android/camera/actor/PhotoActor;->mCamera:Lcom/android/camera/Camera;

    invoke-virtual {v0, v1}, Lcom/android/camera/Camera;->switchShutter(I)V

    goto :goto_1
.end method

.method static synthetic access$1000(Lcom/android/camera/actor/PhotoActor;)J
    .locals 2
    .param p0    # Lcom/android/camera/actor/PhotoActor;

    iget-wide v0, p0, Lcom/android/camera/actor/PhotoActor;->mContinuousShotStartTime:J

    return-wide v0
.end method

.method static synthetic access$1002(Lcom/android/camera/actor/PhotoActor;J)J
    .locals 0
    .param p0    # Lcom/android/camera/actor/PhotoActor;
    .param p1    # J

    iput-wide p1, p0, Lcom/android/camera/actor/PhotoActor;->mContinuousShotStartTime:J

    return-wide p1
.end method

.method static synthetic access$1100(Lcom/android/camera/actor/PhotoActor;)I
    .locals 1
    .param p0    # Lcom/android/camera/actor/PhotoActor;

    iget v0, p0, Lcom/android/camera/actor/PhotoActor;->mSoundID:I

    return v0
.end method

.method static synthetic access$1102(Lcom/android/camera/actor/PhotoActor;I)I
    .locals 0
    .param p0    # Lcom/android/camera/actor/PhotoActor;
    .param p1    # I

    iput p1, p0, Lcom/android/camera/actor/PhotoActor;->mSoundID:I

    return p1
.end method

.method static synthetic access$1200(Lcom/android/camera/actor/PhotoActor;)Landroid/media/SoundPool;
    .locals 1
    .param p0    # Lcom/android/camera/actor/PhotoActor;

    iget-object v0, p0, Lcom/android/camera/actor/PhotoActor;->mBurstSound:Landroid/media/SoundPool;

    return-object v0
.end method

.method static synthetic access$1202(Lcom/android/camera/actor/PhotoActor;Landroid/media/SoundPool;)Landroid/media/SoundPool;
    .locals 0
    .param p0    # Lcom/android/camera/actor/PhotoActor;
    .param p1    # Landroid/media/SoundPool;

    iput-object p1, p0, Lcom/android/camera/actor/PhotoActor;->mBurstSound:Landroid/media/SoundPool;

    return-object p1
.end method

.method static synthetic access$1300(Lcom/android/camera/actor/PhotoActor;)J
    .locals 2
    .param p0    # Lcom/android/camera/actor/PhotoActor;

    iget-wide v0, p0, Lcom/android/camera/actor/PhotoActor;->mShutterCallbackTime:J

    return-wide v0
.end method

.method static synthetic access$1400(Lcom/android/camera/actor/PhotoActor;)J
    .locals 2
    .param p0    # Lcom/android/camera/actor/PhotoActor;

    iget-wide v0, p0, Lcom/android/camera/actor/PhotoActor;->mRawPictureCallbackTime:J

    return-wide v0
.end method

.method static synthetic access$1402(Lcom/android/camera/actor/PhotoActor;J)J
    .locals 0
    .param p0    # Lcom/android/camera/actor/PhotoActor;
    .param p1    # J

    iput-wide p1, p0, Lcom/android/camera/actor/PhotoActor;->mRawPictureCallbackTime:J

    return-wide p1
.end method

.method static synthetic access$1502(Lcom/android/camera/actor/PhotoActor;Z)Z
    .locals 0
    .param p0    # Lcom/android/camera/actor/PhotoActor;
    .param p1    # Z

    iput-boolean p1, p0, Lcom/android/camera/actor/PhotoActor;->mIgnoreClick:Z

    return p1
.end method

.method static synthetic access$1600(Lcom/android/camera/actor/PhotoActor;[B)V
    .locals 0
    .param p0    # Lcom/android/camera/actor/PhotoActor;
    .param p1    # [B

    invoke-direct {p0, p1}, Lcom/android/camera/actor/PhotoActor;->setSaveRequest([B)V

    return-void
.end method

.method static synthetic access$1700(Lcom/android/camera/actor/PhotoActor;)I
    .locals 1
    .param p0    # Lcom/android/camera/actor/PhotoActor;

    iget v0, p0, Lcom/android/camera/actor/PhotoActor;->mCurrentShotsNum:I

    return v0
.end method

.method static synthetic access$1708(Lcom/android/camera/actor/PhotoActor;)I
    .locals 2
    .param p0    # Lcom/android/camera/actor/PhotoActor;

    iget v0, p0, Lcom/android/camera/actor/PhotoActor;->mCurrentShotsNum:I

    add-int/lit8 v1, v0, 0x1

    iput v1, p0, Lcom/android/camera/actor/PhotoActor;->mCurrentShotsNum:I

    return v0
.end method

.method static synthetic access$1800(Lcom/android/camera/actor/PhotoActor;)Lcom/android/camera/actor/PhotoActor$MemoryManager;
    .locals 1
    .param p0    # Lcom/android/camera/actor/PhotoActor;

    iget-object v0, p0, Lcom/android/camera/actor/PhotoActor;->mMemoryManager:Lcom/android/camera/actor/PhotoActor$MemoryManager;

    return-object v0
.end method

.method static synthetic access$1900(Lcom/android/camera/actor/PhotoActor;)I
    .locals 1
    .param p0    # Lcom/android/camera/actor/PhotoActor;

    iget v0, p0, Lcom/android/camera/actor/PhotoActor;->mMaxCaptureNum:I

    return v0
.end method

.method static synthetic access$2002(Lcom/android/camera/actor/PhotoActor;Z)Z
    .locals 0
    .param p0    # Lcom/android/camera/actor/PhotoActor;
    .param p1    # Z

    iput-boolean p1, p0, Lcom/android/camera/actor/PhotoActor;->mKeyHalfPressed:Z

    return p1
.end method

.method static synthetic access$2100(Lcom/android/camera/actor/PhotoActor;)J
    .locals 2
    .param p0    # Lcom/android/camera/actor/PhotoActor;

    iget-wide v0, p0, Lcom/android/camera/actor/PhotoActor;->mJpegPictureCallbackTime:J

    return-wide v0
.end method

.method static synthetic access$2102(Lcom/android/camera/actor/PhotoActor;J)J
    .locals 0
    .param p0    # Lcom/android/camera/actor/PhotoActor;
    .param p1    # J

    iput-wide p1, p0, Lcom/android/camera/actor/PhotoActor;->mJpegPictureCallbackTime:J

    return-wide p1
.end method

.method static synthetic access$2200(Lcom/android/camera/actor/PhotoActor;)J
    .locals 2
    .param p0    # Lcom/android/camera/actor/PhotoActor;

    iget-wide v0, p0, Lcom/android/camera/actor/PhotoActor;->mFocusStartTime:J

    return-wide v0
.end method

.method static synthetic access$2300(Lcom/android/camera/actor/PhotoActor;)Z
    .locals 1
    .param p0    # Lcom/android/camera/actor/PhotoActor;

    iget-boolean v0, p0, Lcom/android/camera/actor/PhotoActor;->mSelftimerCounting:Z

    return v0
.end method

.method static synthetic access$2402(Z)Z
    .locals 0
    .param p0    # Z

    sput-boolean p0, Lcom/android/camera/actor/PhotoActor;->sIsAutoFocusCallback:Z

    return p0
.end method

.method static synthetic access$2600(Lcom/android/camera/actor/PhotoActor;)Z
    .locals 1
    .param p0    # Lcom/android/camera/actor/PhotoActor;

    iget-boolean v0, p0, Lcom/android/camera/actor/PhotoActor;->mSupportContinuous:Z

    return v0
.end method

.method static synthetic access$2700(Lcom/android/camera/actor/PhotoActor;)Lcom/android/camera/actor/PhotoActor$RenderInCapture;
    .locals 1
    .param p0    # Lcom/android/camera/actor/PhotoActor;

    iget-object v0, p0, Lcom/android/camera/actor/PhotoActor;->mRenderThread:Lcom/android/camera/actor/PhotoActor$RenderInCapture;

    return-object v0
.end method

.method static synthetic access$2702(Lcom/android/camera/actor/PhotoActor;Lcom/android/camera/actor/PhotoActor$RenderInCapture;)Lcom/android/camera/actor/PhotoActor$RenderInCapture;
    .locals 0
    .param p0    # Lcom/android/camera/actor/PhotoActor;
    .param p1    # Lcom/android/camera/actor/PhotoActor$RenderInCapture;

    iput-object p1, p0, Lcom/android/camera/actor/PhotoActor;->mRenderThread:Lcom/android/camera/actor/PhotoActor$RenderInCapture;

    return-object p1
.end method

.method static synthetic access$2800(Lcom/android/camera/actor/PhotoActor;)Landroid/hardware/Camera$ZSDPreviewDone;
    .locals 1
    .param p0    # Lcom/android/camera/actor/PhotoActor;

    iget-object v0, p0, Lcom/android/camera/actor/PhotoActor;->mZSDPreviewDone:Landroid/hardware/Camera$ZSDPreviewDone;

    return-object v0
.end method

.method static synthetic access$2900(Lcom/android/camera/actor/PhotoActor;)Landroid/hardware/Camera$PictureCallback;
    .locals 1
    .param p0    # Lcom/android/camera/actor/PhotoActor;

    iget-object v0, p0, Lcom/android/camera/actor/PhotoActor;->mContinuousJpegPictureCallback:Landroid/hardware/Camera$PictureCallback;

    return-object v0
.end method

.method static synthetic access$3000(Lcom/android/camera/actor/PhotoActor;)Landroid/hardware/Camera$PictureCallback;
    .locals 1
    .param p0    # Lcom/android/camera/actor/PhotoActor;

    iget-object v0, p0, Lcom/android/camera/actor/PhotoActor;->mJpegPictureCallback:Landroid/hardware/Camera$PictureCallback;

    return-object v0
.end method

.method static synthetic access$3100(Lcom/android/camera/actor/PhotoActor;)Z
    .locals 1
    .param p0    # Lcom/android/camera/actor/PhotoActor;

    iget-boolean v0, p0, Lcom/android/camera/actor/PhotoActor;->mSavingPictures:Z

    return v0
.end method

.method static synthetic access$3102(Lcom/android/camera/actor/PhotoActor;Z)Z
    .locals 0
    .param p0    # Lcom/android/camera/actor/PhotoActor;
    .param p1    # Z

    iput-boolean p1, p0, Lcom/android/camera/actor/PhotoActor;->mSavingPictures:Z

    return p1
.end method

.method static synthetic access$3200(Lcom/android/camera/actor/PhotoActor;)V
    .locals 0
    .param p0    # Lcom/android/camera/actor/PhotoActor;

    invoke-direct {p0}, Lcom/android/camera/actor/PhotoActor;->cancelContinuousShot()V

    return-void
.end method

.method static synthetic access$3300(Lcom/android/camera/actor/PhotoActor;)Ljava/lang/Thread;
    .locals 1
    .param p0    # Lcom/android/camera/actor/PhotoActor;

    iget-object v0, p0, Lcom/android/camera/actor/PhotoActor;->mWaitSavingDoneThread:Ljava/lang/Thread;

    return-object v0
.end method

.method static synthetic access$3302(Lcom/android/camera/actor/PhotoActor;Ljava/lang/Thread;)Ljava/lang/Thread;
    .locals 0
    .param p0    # Lcom/android/camera/actor/PhotoActor;
    .param p1    # Ljava/lang/Thread;

    iput-object p1, p0, Lcom/android/camera/actor/PhotoActor;->mWaitSavingDoneThread:Ljava/lang/Thread;

    return-object p1
.end method

.method static synthetic access$400()Z
    .locals 1

    sget-boolean v0, Lcom/android/camera/actor/PhotoActor;->LOG:Z

    return v0
.end method

.method static synthetic access$500(Lcom/android/camera/actor/PhotoActor;)V
    .locals 0
    .param p0    # Lcom/android/camera/actor/PhotoActor;

    invoke-direct {p0}, Lcom/android/camera/actor/PhotoActor;->doAttach()V

    return-void
.end method

.method static synthetic access$600(Lcom/android/camera/actor/PhotoActor;)V
    .locals 0
    .param p0    # Lcom/android/camera/actor/PhotoActor;

    invoke-direct {p0}, Lcom/android/camera/actor/PhotoActor;->doCancel()V

    return-void
.end method

.method static synthetic access$700(Lcom/android/camera/actor/PhotoActor;)Z
    .locals 1
    .param p0    # Lcom/android/camera/actor/PhotoActor;

    iget-boolean v0, p0, Lcom/android/camera/actor/PhotoActor;->mZSDEnabled:Z

    return v0
.end method

.method static synthetic access$800(Lcom/android/camera/actor/PhotoActor;)Z
    .locals 1
    .param p0    # Lcom/android/camera/actor/PhotoActor;

    iget-boolean v0, p0, Lcom/android/camera/actor/PhotoActor;->mContinuousShotPerformed:Z

    return v0
.end method

.method static synthetic access$802(Lcom/android/camera/actor/PhotoActor;Z)Z
    .locals 0
    .param p0    # Lcom/android/camera/actor/PhotoActor;
    .param p1    # Z

    iput-boolean p1, p0, Lcom/android/camera/actor/PhotoActor;->mContinuousShotPerformed:Z

    return p1
.end method

.method static synthetic access$900(Lcom/android/camera/actor/PhotoActor;)I
    .locals 1
    .param p0    # Lcom/android/camera/actor/PhotoActor;

    iget v0, p0, Lcom/android/camera/actor/PhotoActor;->mStreamID:I

    return v0
.end method

.method static synthetic access$902(Lcom/android/camera/actor/PhotoActor;I)I
    .locals 0
    .param p0    # Lcom/android/camera/actor/PhotoActor;
    .param p1    # I

    iput p1, p0, Lcom/android/camera/actor/PhotoActor;->mStreamID:I

    return p1
.end method

.method private canTakePicture()Z
    .locals 1

    invoke-direct {p0}, Lcom/android/camera/actor/PhotoActor;->isCameraIdle()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/camera/actor/PhotoActor;->mCameraCategory:Lcom/android/camera/actor/PhotoActor$CameraCategory;

    invoke-virtual {v0}, Lcom/android/camera/actor/PhotoActor$CameraCategory;->canshot()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private cancelContinuousShot()V
    .locals 2

    iget-object v0, p0, Lcom/android/camera/actor/PhotoActor;->mCamera:Lcom/android/camera/Camera;

    invoke-virtual {v0}, Lcom/android/camera/Camera;->getCameraDevice()Lcom/android/camera/CameraManager$CameraProxy;

    move-result-object v0

    invoke-virtual {v0}, Lcom/android/camera/CameraManager$CameraProxy;->cancelContinuousShot()V

    iget-object v0, p0, Lcom/android/camera/actor/PhotoActor;->mBurstSound:Landroid/media/SoundPool;

    iget v1, p0, Lcom/android/camera/actor/PhotoActor;->mStreamID:I

    invoke-virtual {v0, v1}, Landroid/media/SoundPool;->stop(I)V

    return-void
.end method

.method private doAttach()V
    .locals 17

    move-object/from16 v0, p0

    iget-boolean v13, v0, Lcom/android/camera/actor/PhotoActor;->mCameraClosed:Z

    if-eqz v13, :cond_0

    :goto_0
    return-void

    :cond_0
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/camera/actor/PhotoActor;->mJpegImageData:[B

    move-object/from16 v0, p0

    invoke-direct {v0, v4}, Lcom/android/camera/actor/PhotoActor;->setSaveRequest([B)V

    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/android/camera/actor/PhotoActor;->mCamera:Lcom/android/camera/Camera;

    invoke-virtual {v13}, Lcom/android/camera/Camera;->getSaveUri()Landroid/net/Uri;

    move-result-object v10

    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/android/camera/actor/PhotoActor;->mCamera:Lcom/android/camera/Camera;

    invoke-virtual {v13}, Lcom/android/camera/Camera;->getCropValue()Ljava/lang/String;

    move-result-object v3

    if-nez v3, :cond_4

    if-eqz v10, :cond_3

    const/4 v8, 0x0

    :try_start_0
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/android/camera/actor/PhotoActor;->mCamera:Lcom/android/camera/Camera;

    invoke-virtual {v13}, Landroid/content/ContextWrapper;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v13

    invoke-virtual {v13, v10}, Landroid/content/ContentResolver;->openOutputStream(Landroid/net/Uri;)Ljava/io/OutputStream;

    move-result-object v8

    if-eqz v8, :cond_1

    invoke-virtual {v8, v4}, Ljava/io/OutputStream;->write([B)V

    invoke-virtual {v8}, Ljava/io/OutputStream;->close()V

    :cond_1
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/android/camera/actor/PhotoActor;->mCamera:Lcom/android/camera/Camera;

    const/4 v14, -0x1

    invoke-virtual {v13, v14}, Lcom/android/camera/Camera;->setResultExAndFinish(I)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :cond_2
    :goto_1
    invoke-static {v8}, Lcom/android/camera/Util;->closeSilently(Ljava/io/Closeable;)V

    goto :goto_0

    :catch_0
    move-exception v5

    :try_start_1
    sget-boolean v13, Lcom/android/camera/actor/PhotoActor;->LOG:Z

    if-eqz v13, :cond_2

    const-string v13, "PhotoActor"

    const-string v14, "IOException, when doAttach"

    invoke-static {v13, v14}, Lcom/android/camera/Log;->v(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_1

    :catchall_0
    move-exception v13

    invoke-static {v8}, Lcom/android/camera/Util;->closeSilently(Ljava/io/Closeable;)V

    throw v13

    :cond_3
    invoke-static {v4}, Lcom/android/camera/Exif;->getOrientation([B)I

    move-result v7

    const v13, 0xc800

    invoke-static {v4, v13}, Lcom/android/camera/Util;->makeBitmap([BI)Landroid/graphics/Bitmap;

    move-result-object v1

    invoke-static {v1, v7}, Lcom/android/camera/Util;->rotate(Landroid/graphics/Bitmap;I)Landroid/graphics/Bitmap;

    move-result-object v1

    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/android/camera/actor/PhotoActor;->mCamera:Lcom/android/camera/Camera;

    const/4 v14, -0x1

    new-instance v15, Landroid/content/Intent;

    const-string v16, "inline-data"

    invoke-direct/range {v15 .. v16}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    const-string v16, "data"

    move-object/from16 v0, v16

    invoke-virtual {v15, v0, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    move-result-object v15

    invoke-virtual {v13, v14, v15}, Lcom/android/camera/Camera;->setResultExAndFinish(ILandroid/content/Intent;)V

    goto :goto_0

    :cond_4
    const/4 v12, 0x0

    const/4 v11, 0x0

    :try_start_2
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/android/camera/actor/PhotoActor;->mCamera:Lcom/android/camera/Camera;

    const-string v14, "crop-temp"

    invoke-virtual {v13, v14}, Landroid/content/ContextWrapper;->getFileStreamPath(Ljava/lang/String;)Ljava/io/File;

    move-result-object v9

    invoke-virtual {v9}, Ljava/io/File;->delete()Z

    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/android/camera/actor/PhotoActor;->mCamera:Lcom/android/camera/Camera;

    const-string v14, "crop-temp"

    const/4 v15, 0x0

    invoke-virtual {v13, v14, v15}, Landroid/content/ContextWrapper;->openFileOutput(Ljava/lang/String;I)Ljava/io/FileOutputStream;

    move-result-object v11

    invoke-virtual {v11, v4}, Ljava/io/OutputStream;->write([B)V

    invoke-virtual {v11}, Ljava/io/FileOutputStream;->close()V

    invoke-static {v9}, Landroid/net/Uri;->fromFile(Ljava/io/File;)Landroid/net/Uri;
    :try_end_2
    .catch Ljava/io/FileNotFoundException; {:try_start_2 .. :try_end_2} :catch_1
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    move-result-object v12

    invoke-static {v11}, Lcom/android/camera/Util;->closeSilently(Ljava/io/Closeable;)V

    new-instance v6, Landroid/os/Bundle;

    invoke-direct {v6}, Landroid/os/Bundle;-><init>()V

    const-string v13, "circle"

    invoke-virtual {v3, v13}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v13

    if-eqz v13, :cond_5

    const-string v13, "circleCrop"

    const-string v14, "true"

    invoke-virtual {v6, v13, v14}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    :cond_5
    if-eqz v10, :cond_6

    const-string v13, "output"

    invoke-virtual {v6, v13, v10}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    :goto_2
    new-instance v2, Landroid/content/Intent;

    const-string v13, "com.android.camera.action.CROP"

    invoke-direct {v2, v13}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v12}, Landroid/content/Intent;->setData(Landroid/net/Uri;)Landroid/content/Intent;

    invoke-virtual {v2, v6}, Landroid/content/Intent;->putExtras(Landroid/os/Bundle;)Landroid/content/Intent;

    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/android/camera/actor/PhotoActor;->mCamera:Lcom/android/camera/Camera;

    const/16 v14, 0x3e8

    invoke-virtual {v13, v2, v14}, Landroid/app/Activity;->startActivityForResult(Landroid/content/Intent;I)V

    goto/16 :goto_0

    :catch_1
    move-exception v5

    :try_start_3
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/android/camera/actor/PhotoActor;->mCamera:Lcom/android/camera/Camera;

    const/4 v14, 0x0

    invoke-virtual {v13, v14}, Lcom/android/camera/Camera;->setResultExAndFinish(I)V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    :goto_3
    invoke-static {v11}, Lcom/android/camera/Util;->closeSilently(Ljava/io/Closeable;)V

    goto/16 :goto_0

    :catch_2
    move-exception v5

    :try_start_4
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/android/camera/actor/PhotoActor;->mCamera:Lcom/android/camera/Camera;

    const/4 v14, 0x0

    invoke-virtual {v13, v14}, Lcom/android/camera/Camera;->setResultExAndFinish(I)V
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    goto :goto_3

    :catchall_1
    move-exception v13

    invoke-static {v11}, Lcom/android/camera/Util;->closeSilently(Ljava/io/Closeable;)V

    throw v13

    :cond_6
    const-string v13, "return-data"

    const/4 v14, 0x1

    invoke-virtual {v6, v13, v14}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    goto :goto_2
.end method

.method private doCancel()V
    .locals 3

    iget-object v0, p0, Lcom/android/camera/actor/PhotoActor;->mCamera:Lcom/android/camera/Camera;

    const/4 v1, 0x0

    new-instance v2, Landroid/content/Intent;

    invoke-direct {v2}, Landroid/content/Intent;-><init>()V

    invoke-virtual {v0, v1, v2}, Lcom/android/camera/Camera;->setResultExAndFinish(ILandroid/content/Intent;)V

    return-void
.end method

.method private isBusy()Z
    .locals 2

    iget-object v0, p0, Lcom/android/camera/actor/PhotoActor;->mCamera:Lcom/android/camera/Camera;

    invoke-virtual {v0}, Lcom/android/camera/Camera;->getFileSaver()Lcom/android/camera/FileSaver;

    move-result-object v0

    invoke-virtual {v0}, Lcom/android/camera/FileSaver;->getWaitingCount()I

    move-result v0

    const/4 v1, 0x3

    if-le v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private isCameraIdle()Z
    .locals 4

    const/4 v0, 0x1

    sget-boolean v1, Lcom/android/camera/actor/PhotoActor;->LOG:Z

    if-eqz v1, :cond_0

    const-string v1, "PhotoActor"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "mCamera.getCameraState()="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/android/camera/actor/PhotoActor;->mCamera:Lcom/android/camera/Camera;

    invoke-virtual {v3}, Lcom/android/camera/Camera;->getCameraState()I

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " mCamera.getFocusManager()="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/android/camera/actor/PhotoActor;->mCamera:Lcom/android/camera/Camera;

    invoke-virtual {v3}, Lcom/android/camera/Camera;->getFocusManager()Lcom/android/camera/FocusManager;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/android/camera/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    iget-boolean v1, p0, Lcom/android/camera/actor/PhotoActor;->mSelftimerCounting:Z

    if-nez v1, :cond_2

    iget-object v1, p0, Lcom/android/camera/actor/PhotoActor;->mCamera:Lcom/android/camera/Camera;

    invoke-virtual {v1}, Lcom/android/camera/Camera;->getCameraState()I

    move-result v1

    if-eq v1, v0, :cond_1

    iget-object v1, p0, Lcom/android/camera/actor/PhotoActor;->mCamera:Lcom/android/camera/Camera;

    invoke-virtual {v1}, Lcom/android/camera/Camera;->getFocusManager()Lcom/android/camera/FocusManager;

    move-result-object v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/android/camera/actor/PhotoActor;->mCamera:Lcom/android/camera/Camera;

    invoke-virtual {v1}, Lcom/android/camera/Camera;->getFocusManager()Lcom/android/camera/FocusManager;

    move-result-object v1

    invoke-virtual {v1}, Lcom/android/camera/FocusManager;->isFocusCompleted()Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/android/camera/actor/PhotoActor;->mCamera:Lcom/android/camera/Camera;

    invoke-virtual {v1}, Lcom/android/camera/Camera;->getCameraState()I

    move-result v1

    const/4 v2, 0x4

    if-eq v1, v2, :cond_2

    :cond_1
    :goto_0
    return v0

    :cond_2
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private isImageCaptureIntent()Z
    .locals 2

    iget-object v1, p0, Lcom/android/camera/actor/CameraActor;->mContext:Lcom/android/camera/Camera;

    invoke-virtual {v1}, Landroid/app/Activity;->getIntent()Landroid/content/Intent;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    const-string v1, "android.media.action.IMAGE_CAPTURE"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    return v1
.end method

.method private isSupportContinuousShot()Z
    .locals 3

    const/4 v1, 0x0

    iget-object v2, p0, Lcom/android/camera/actor/PhotoActor;->mCamera:Lcom/android/camera/Camera;

    invoke-virtual {v2}, Lcom/android/camera/Camera;->getParameters()Landroid/hardware/Camera$Parameters;

    move-result-object v2

    invoke-virtual {v2}, Landroid/hardware/Camera$Parameters;->getSupportedCaptureMode()Ljava/util/List;

    move-result-object v0

    if-nez v0, :cond_1

    :cond_0
    :goto_0
    return v1

    :cond_1
    const-string v2, "continuousshot"

    invoke-interface {v0, v2}, Ljava/util/List;->indexOf(Ljava/lang/Object;)I

    move-result v2

    if-ltz v2, :cond_0

    const/4 v1, 0x1

    goto :goto_0
.end method

.method private isSupportFaceDetect()Z
    .locals 4

    iget-object v1, p0, Lcom/android/camera/actor/PhotoActor;->mCamera:Lcom/android/camera/Camera;

    invoke-virtual {v1}, Lcom/android/camera/Camera;->getSettingChecker()Lcom/android/camera/SettingChecker;

    move-result-object v1

    const/16 v2, 0x32

    invoke-virtual {v1, v2}, Lcom/android/camera/SettingChecker;->getSettingCurrentValue(I)Ljava/lang/String;

    move-result-object v0

    sget-boolean v1, Lcom/android/camera/actor/PhotoActor;->LOG:Z

    if-eqz v1, :cond_0

    const-string v1, "PhotoActor"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "isSupportFaceDetect faceDetection="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/android/camera/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    const-string v1, "on"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    return v1
.end method

.method private setSaveRequest([B)V
    .locals 1
    .param p1    # [B

    iget-object v0, p0, Lcom/android/camera/actor/PhotoActor;->mSaveRequest:Lcom/android/camera/SaveRequest;

    invoke-interface {v0, p1}, Lcom/android/camera/SaveRequest;->setData([B)V

    iget-object v0, p0, Lcom/android/camera/actor/PhotoActor;->mSaveRequest:Lcom/android/camera/SaveRequest;

    invoke-interface {v0}, Lcom/android/camera/SaveRequest;->addRequest()V

    return-void
.end method


# virtual methods
.method public autoFocus()V
    .locals 2

    sget-boolean v0, Lcom/android/camera/actor/PhotoActor;->LOG:Z

    if-eqz v0, :cond_0

    const-string v0, "PhotoActor"

    const-string v1, "autoFocus"

    invoke-static {v0, v1}, Lcom/android/camera/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/android/camera/actor/PhotoActor;->mFocusStartTime:J

    iget-object v0, p0, Lcom/android/camera/actor/PhotoActor;->mCamera:Lcom/android/camera/Camera;

    invoke-virtual {v0}, Lcom/android/camera/Camera;->getCameraDevice()Lcom/android/camera/CameraManager$CameraProxy;

    move-result-object v0

    iget-object v1, p0, Lcom/android/camera/actor/PhotoActor;->mAutoFocusCallback:Lcom/android/camera/actor/PhotoActor$AutoFocusCallback;

    invoke-virtual {v0, v1}, Lcom/android/camera/CameraManager$CameraProxy;->autoFocus(Landroid/hardware/Camera$AutoFocusCallback;)V

    iget-object v0, p0, Lcom/android/camera/actor/PhotoActor;->mCamera:Lcom/android/camera/Camera;

    const/4 v1, 0x2

    invoke-virtual {v0, v1}, Lcom/android/camera/Camera;->setCameraState(I)V

    iget-object v0, p0, Lcom/android/camera/actor/PhotoActor;->mCamera:Lcom/android/camera/Camera;

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Lcom/android/camera/Camera;->setViewState(I)V

    return-void
.end method

.method protected calculateShutterTime()V
    .locals 4

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/android/camera/actor/PhotoActor;->mShutterCallbackTime:J

    iget-wide v0, p0, Lcom/android/camera/actor/PhotoActor;->mShutterCallbackTime:J

    iget-wide v2, p0, Lcom/android/camera/actor/PhotoActor;->mCaptureStartTime:J

    sub-long/2addr v0, v2

    iput-wide v0, p0, Lcom/android/camera/actor/PhotoActor;->mShutterLag:J

    sget-boolean v0, Lcom/android/camera/actor/PhotoActor;->LOG:Z

    if-eqz v0, :cond_0

    const-string v0, "PhotoActor"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "mShutterLag = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-wide v2, p0, Lcom/android/camera/actor/PhotoActor;->mShutterLag:J

    invoke-virtual {v1, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "ms"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/android/camera/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    return-void
.end method

.method public cancelAutoFocus()V
    .locals 2

    sget-boolean v0, Lcom/android/camera/actor/PhotoActor;->LOG:Z

    if-eqz v0, :cond_0

    const-string v0, "PhotoActor"

    const-string v1, "cancelAutoFocus"

    invoke-static {v0, v1}, Lcom/android/camera/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    iget-object v0, p0, Lcom/android/camera/actor/PhotoActor;->mCamera:Lcom/android/camera/Camera;

    invoke-virtual {v0}, Lcom/android/camera/Camera;->getCameraDevice()Lcom/android/camera/CameraManager$CameraProxy;

    move-result-object v0

    invoke-virtual {v0}, Lcom/android/camera/CameraManager$CameraProxy;->cancelAutoFocus()V

    iget-boolean v0, p0, Lcom/android/camera/actor/PhotoActor;->mSelftimerCounting:Z

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/android/camera/actor/PhotoActor;->mCamera:Lcom/android/camera/Camera;

    invoke-virtual {v0}, Lcom/android/camera/Camera;->getCameraState()I

    move-result v0

    const/4 v1, 0x3

    if-eq v0, v1, :cond_1

    iget-object v0, p0, Lcom/android/camera/actor/PhotoActor;->mCamera:Lcom/android/camera/Camera;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/android/camera/Camera;->setCameraState(I)V

    iget-object v0, p0, Lcom/android/camera/actor/PhotoActor;->mCamera:Lcom/android/camera/Camera;

    invoke-virtual {v0}, Lcom/android/camera/Camera;->restoreViewState()V

    :cond_1
    invoke-virtual {p0}, Lcom/android/camera/actor/PhotoActor;->setFocusParameters()V

    return-void
.end method

.method public capture()Z
    .locals 14

    const/4 v13, 0x3

    const/4 v6, 0x1

    const/4 v5, 0x0

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v3

    sget-boolean v7, Lcom/android/camera/actor/PhotoActor;->LOG:Z

    if-eqz v7, :cond_0

    const-string v7, "PhotoActor"

    const-string v8, "capture begin"

    invoke-static {v7, v8}, Lcom/android/camera/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    iget-object v7, p0, Lcom/android/camera/actor/PhotoActor;->mCamera:Lcom/android/camera/Camera;

    invoke-virtual {v7}, Lcom/android/camera/Camera;->getCameraDevice()Lcom/android/camera/CameraManager$CameraProxy;

    move-result-object v7

    if-eqz v7, :cond_1

    iget-object v7, p0, Lcom/android/camera/actor/PhotoActor;->mCamera:Lcom/android/camera/Camera;

    invoke-virtual {v7}, Lcom/android/camera/Camera;->getCameraState()I

    move-result v7

    if-eq v7, v13, :cond_1

    iget-object v7, p0, Lcom/android/camera/actor/PhotoActor;->mCamera:Lcom/android/camera/Camera;

    invoke-virtual {v7}, Lcom/android/camera/Camera;->getCameraState()I

    move-result v7

    const/4 v8, 0x4

    if-ne v7, v8, :cond_3

    :cond_1
    iget-object v6, p0, Lcom/android/camera/actor/PhotoActor;->mCamera:Lcom/android/camera/Camera;

    invoke-virtual {v6}, Lcom/android/camera/Camera;->getCameraDevice()Lcom/android/camera/CameraManager$CameraProxy;

    move-result-object v6

    if-nez v6, :cond_2

    iget-object v6, p0, Lcom/android/camera/actor/PhotoActor;->mCamera:Lcom/android/camera/Camera;

    invoke-virtual {v6}, Lcom/android/camera/Camera;->restoreViewState()V

    :cond_2
    :goto_0
    return v5

    :cond_3
    iget-object v7, p0, Lcom/android/camera/actor/PhotoActor;->mCamera:Lcom/android/camera/Camera;

    invoke-virtual {v7}, Lcom/android/camera/Camera;->getParameters()Landroid/hardware/Camera$Parameters;

    move-result-object v2

    invoke-virtual {p0}, Lcom/android/camera/actor/CameraActor;->getContext()Lcom/android/camera/Camera;

    move-result-object v7

    const-string v8, "flashstate"

    invoke-virtual {v7, v8, v5}, Landroid/content/ContextWrapper;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v1

    invoke-interface {v1}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string v7, "flash"

    invoke-virtual {v2}, Landroid/hardware/Camera$Parameters;->getFlashMode()Ljava/lang/String;

    move-result-object v8

    invoke-interface {v0, v7, v8}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    const-string v7, "PhotoActor"

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "FlashMode"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v2}, Landroid/hardware/Camera$Parameters;->getFlashMode()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8}, Lcom/android/camera/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    invoke-static {}, Lcom/android/camera/manager/MMProfileManager;->startProfileTakePicture()V

    iput-boolean v6, p0, Lcom/android/camera/actor/PhotoActor;->mCapturing:Z

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v7

    iput-wide v7, p0, Lcom/android/camera/actor/PhotoActor;->mCaptureStartTime:J

    const-wide/16 v7, 0x0

    iput-wide v7, p0, Lcom/android/camera/actor/PhotoActor;->mPostViewPictureCallbackTime:J

    const/4 v7, 0x0

    iput-object v7, p0, Lcom/android/camera/actor/PhotoActor;->mJpegImageData:[B

    iget-object v7, p0, Lcom/android/camera/actor/CameraActor;->mContext:Lcom/android/camera/Camera;

    invoke-virtual {v7}, Lcom/android/camera/Camera;->preparePhotoRequest()Lcom/android/camera/SaveRequest;

    move-result-object v7

    iput-object v7, p0, Lcom/android/camera/actor/PhotoActor;->mSaveRequest:Lcom/android/camera/SaveRequest;

    iget-boolean v7, p0, Lcom/android/camera/actor/PhotoActor;->mContinuousShotPerformed:Z

    if-eqz v7, :cond_4

    iput v5, p0, Lcom/android/camera/actor/PhotoActor;->mStreamID:I

    :cond_4
    iget-object v7, p0, Lcom/android/camera/actor/PhotoActor;->mCameraCategory:Lcom/android/camera/actor/PhotoActor$CameraCategory;

    invoke-virtual {v7}, Lcom/android/camera/actor/PhotoActor$CameraCategory;->ensureCaptureTempPath()V

    iget-boolean v7, p0, Lcom/android/camera/actor/PhotoActor;->mZSDEnabled:Z

    if-eqz v7, :cond_5

    iget-object v7, p0, Lcom/android/camera/actor/PhotoActor;->mCameraCategory:Lcom/android/camera/actor/PhotoActor$CameraCategory;

    iget-object v8, p0, Lcom/android/camera/actor/PhotoActor;->mCamera:Lcom/android/camera/Camera;

    invoke-virtual {v7, v8}, Lcom/android/camera/actor/PhotoActor$CameraCategory;->animateCapture(Lcom/android/camera/Camera;)V

    :cond_5
    sget-object v7, Lcom/android/camera/actor/PhotoActor;->sFaceDetectionSync:Ljava/lang/Object;

    monitor-enter v7

    :try_start_0
    iget-object v8, p0, Lcom/android/camera/actor/PhotoActor;->mCameraCategory:Lcom/android/camera/actor/PhotoActor$CameraCategory;

    invoke-virtual {v8}, Lcom/android/camera/actor/PhotoActor$CameraCategory;->applySpecialCapture()Z

    move-result v8

    if-nez v8, :cond_6

    iget-object v8, p0, Lcom/android/camera/actor/CameraActor;->mContext:Lcom/android/camera/Camera;

    invoke-virtual {v8}, Lcom/android/camera/Camera;->getCameraDevice()Lcom/android/camera/CameraManager$CameraProxy;

    move-result-object v8

    invoke-virtual {p0}, Lcom/android/camera/actor/PhotoActor;->getShutterCallback()Landroid/hardware/Camera$ShutterCallback;

    move-result-object v9

    iget-object v10, p0, Lcom/android/camera/actor/PhotoActor;->mRawPictureCallback:Landroid/hardware/Camera$PictureCallback;

    iget-object v11, p0, Lcom/android/camera/actor/PhotoActor;->mPostViewPictureCallback:Landroid/hardware/Camera$PictureCallback;

    iget-object v12, p0, Lcom/android/camera/actor/PhotoActor;->mCameraCategory:Lcom/android/camera/actor/PhotoActor$CameraCategory;

    invoke-virtual {v12}, Lcom/android/camera/actor/PhotoActor$CameraCategory;->getJpegPictureCallback()Landroid/hardware/Camera$PictureCallback;

    move-result-object v12

    invoke-virtual {v8, v9, v10, v11, v12}, Lcom/android/camera/CameraManager$CameraProxy;->takePicture(Landroid/hardware/Camera$ShutterCallback;Landroid/hardware/Camera$PictureCallback;Landroid/hardware/Camera$PictureCallback;Landroid/hardware/Camera$PictureCallback;)V

    :cond_6
    const/4 v8, 0x0

    sput-boolean v8, Lcom/android/camera/actor/PhotoActor;->sFaceDetectionStarted:Z

    monitor-exit v7
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    iget-boolean v7, p0, Lcom/android/camera/actor/PhotoActor;->mZSDEnabled:Z

    if-nez v7, :cond_7

    iget-object v7, p0, Lcom/android/camera/actor/PhotoActor;->mCameraCategory:Lcom/android/camera/actor/PhotoActor$CameraCategory;

    iget-object v8, p0, Lcom/android/camera/actor/PhotoActor;->mCamera:Lcom/android/camera/Camera;

    invoke-virtual {v7, v8}, Lcom/android/camera/actor/PhotoActor$CameraCategory;->animateCapture(Lcom/android/camera/Camera;)V

    :cond_7
    iget-object v7, p0, Lcom/android/camera/actor/PhotoActor;->mCamera:Lcom/android/camera/Camera;

    invoke-virtual {v7, v13}, Lcom/android/camera/Camera;->setCameraState(I)V

    iget-object v7, p0, Lcom/android/camera/actor/PhotoActor;->mCamera:Lcom/android/camera/Camera;

    invoke-virtual {v7, v5}, Lcom/android/camera/Camera;->setSwipingEnabled(Z)V

    iget-object v7, p0, Lcom/android/camera/actor/PhotoActor;->mCamera:Lcom/android/camera/Camera;

    invoke-virtual {v7}, Lcom/android/camera/Camera;->showRemaining()V

    iget-object v7, p0, Lcom/android/camera/actor/PhotoActor;->mCamera:Lcom/android/camera/Camera;

    iget-boolean v8, p0, Lcom/android/camera/actor/PhotoActor;->mContinuousShotPerformed:Z

    if-eqz v8, :cond_8

    const/4 v5, 0x2

    :cond_8
    invoke-virtual {v7, v5}, Lcom/android/camera/Camera;->setViewState(I)V

    sget-boolean v5, Lcom/android/camera/actor/PhotoActor;->LOG:Z

    if-eqz v5, :cond_9

    const-string v5, "PhotoActor"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "Capture time = "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v8

    sub-long/2addr v8, v3

    invoke-virtual {v7, v8, v9}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v5, v7}, Lcom/android/camera/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    :cond_9
    invoke-static {}, Lcom/android/camera/manager/MMProfileManager;->stopProfileTakePicture()V

    move v5, v6

    goto/16 :goto_0

    :catchall_0
    move-exception v5

    :try_start_1
    monitor-exit v7
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v5
.end method

.method public doSmileShutter()Z
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public enableCameraControls(Z)V
    .locals 0
    .param p1    # Z

    return-void
.end method

.method public getAutoFocusMoveCallback()Landroid/hardware/Camera$AutoFocusMoveCallback;
    .locals 2

    sget-boolean v0, Lcom/android/camera/actor/PhotoActor;->LOG:Z

    if-eqz v0, :cond_0

    const-string v0, "PhotoActor"

    const-string v1, "PhotoActor.getAutoFocusMoveCallback"

    invoke-static {v0, v1}, Lcom/android/camera/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    iget-object v0, p0, Lcom/android/camera/actor/PhotoActor;->mAutoFocusMoveCallback:Lcom/android/camera/actor/PhotoActor$AutoFocusMoveCallback;

    return-object v0
.end method

.method public getCancelListener()Landroid/view/View$OnClickListener;
    .locals 1

    iget-object v0, p0, Lcom/android/camera/actor/PhotoActor;->mCancelListener:Landroid/view/View$OnClickListener;

    return-object v0
.end method

.method public getContinuousShotDone()Landroid/hardware/Camera$ContinuousShotDone;
    .locals 0

    return-object p0
.end method

.method public getErrorCallback()Landroid/hardware/Camera$ErrorCallback;
    .locals 1

    new-instance v0, Lcom/android/camera/CameraErrorCallback;

    invoke-direct {v0}, Lcom/android/camera/CameraErrorCallback;-><init>()V

    return-object v0
.end method

.method public getFaceDetectionListener()Landroid/hardware/Camera$FaceDetectionListener;
    .locals 1

    iget-object v0, p0, Lcom/android/camera/actor/PhotoActor;->mFaceDetectionListener:Landroid/hardware/Camera$FaceDetectionListener;

    return-object v0
.end method

.method public getFocusManagerListener()Lcom/android/camera/FocusManager$Listener;
    .locals 0

    return-object p0
.end method

.method public getMode()I
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public getOkListener()Landroid/view/View$OnClickListener;
    .locals 1

    iget-object v0, p0, Lcom/android/camera/actor/PhotoActor;->mOkListener:Landroid/view/View$OnClickListener;

    return-object v0
.end method

.method public getPhotoShutterButtonListener()Lcom/android/camera/ui/ShutterButton$OnShutterButtonListener;
    .locals 0

    return-object p0
.end method

.method public getRetakeListener()Landroid/view/View$OnClickListener;
    .locals 1

    iget-object v0, p0, Lcom/android/camera/actor/PhotoActor;->mRetakeListener:Landroid/view/View$OnClickListener;

    return-object v0
.end method

.method public getShutterCallback()Landroid/hardware/Camera$ShutterCallback;
    .locals 1

    iget-object v0, p0, Lcom/android/camera/actor/PhotoActor;->mShutterCallback:Landroid/hardware/Camera$ShutterCallback;

    return-object v0
.end method

.method public getonSingleTapUpListener()Lcom/android/camera/Camera$OnSingleTapUpListener;
    .locals 1

    iget-object v0, p0, Lcom/android/camera/actor/PhotoActor;->mOnSingleTapListener:Lcom/android/camera/Camera$OnSingleTapUpListener;

    return-object v0
.end method

.method public handleFocus()Z
    .locals 1

    iget-boolean v0, p0, Lcom/android/camera/actor/PhotoActor;->mKeyHalfPressed:Z

    if-eqz v0, :cond_0

    const-string v0, "auto"

    invoke-virtual {p0, v0}, Lcom/android/camera/actor/PhotoActor;->overrideFocusMode(Ljava/lang/String;)V

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/android/camera/actor/PhotoActor;->overrideFocusMode(Ljava/lang/String;)V

    const/4 v0, 0x0

    goto :goto_0
.end method

.method public initializeAfterPreview()V
    .locals 5

    const/4 v4, 0x1

    sget-boolean v1, Lcom/android/camera/actor/PhotoActor;->LOG:Z

    if-eqz v1, :cond_0

    const-string v1, "PhotoActor"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "initializeAfterPreview mCamera.getCameraDevice()="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/android/camera/actor/PhotoActor;->mCamera:Lcom/android/camera/Camera;

    invoke-virtual {v3}, Lcom/android/camera/Camera;->getCameraDevice()Lcom/android/camera/CameraManager$CameraProxy;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/android/camera/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    iget-boolean v1, p0, Lcom/android/camera/actor/PhotoActor;->mCameraClosed:Z

    if-nez v1, :cond_1

    iget-object v1, p0, Lcom/android/camera/actor/PhotoActor;->mCamera:Lcom/android/camera/Camera;

    invoke-virtual {v1}, Lcom/android/camera/Camera;->getCameraDevice()Lcom/android/camera/CameraManager$CameraProxy;

    move-result-object v1

    if-nez v1, :cond_2

    :cond_1
    :goto_0
    return-void

    :cond_2
    const/4 v1, 0x0

    sput-boolean v1, Lcom/android/camera/actor/PhotoActor;->sIsAutoFocusCallback:Z

    iget-object v1, p0, Lcom/android/camera/actor/PhotoActor;->mCamera:Lcom/android/camera/Camera;

    invoke-virtual {v1}, Lcom/android/camera/Camera;->getSelfTimer()Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/android/camera/actor/PhotoActor;->mCamera:Lcom/android/camera/Camera;

    invoke-virtual {v1}, Landroid/content/ContextWrapper;->getMainLooper()Landroid/os/Looper;

    move-result-object v1

    invoke-static {v1}, Lcom/android/camera/manager/SelfTimerManager;->getInstance(Landroid/os/Looper;)Lcom/android/camera/manager/SelfTimerManager;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/android/camera/manager/SelfTimerManager;->setSelfTimerDuration(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/android/camera/actor/PhotoActor;->mCamera:Lcom/android/camera/Camera;

    invoke-virtual {v1}, Landroid/content/ContextWrapper;->getMainLooper()Landroid/os/Looper;

    move-result-object v1

    invoke-static {v1}, Lcom/android/camera/manager/SelfTimerManager;->getInstance(Landroid/os/Looper;)Lcom/android/camera/manager/SelfTimerManager;

    move-result-object v1

    iget-object v2, p0, Lcom/android/camera/actor/PhotoActor;->mSelfTimerListener:Lcom/android/camera/manager/SelfTimerManager$SelfTimerListener;

    invoke-virtual {v1, v2}, Lcom/android/camera/manager/SelfTimerManager;->setTimerListener(Lcom/android/camera/manager/SelfTimerManager$SelfTimerListener;)V

    const-string v1, "on"

    iget-object v2, p0, Lcom/android/camera/actor/PhotoActor;->mCamera:Lcom/android/camera/Camera;

    invoke-virtual {v2}, Lcom/android/camera/Camera;->getSettingChecker()Lcom/android/camera/SettingChecker;

    move-result-object v2

    const/16 v3, 0x8

    invoke-virtual {v2, v3}, Lcom/android/camera/SettingChecker;->getParameterValue(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    iput-boolean v1, p0, Lcom/android/camera/actor/PhotoActor;->mZSDEnabled:Z

    invoke-virtual {p0}, Lcom/android/camera/actor/PhotoActor;->getMode()I

    move-result v1

    if-nez v1, :cond_3

    iget-object v1, p0, Lcom/android/camera/actor/PhotoActor;->mCamera:Lcom/android/camera/Camera;

    invoke-virtual {v1}, Lcom/android/camera/Camera;->getSettingChecker()Lcom/android/camera/SettingChecker;

    move-result-object v1

    const/16 v2, 0x9

    invoke-virtual {v1, v2}, Lcom/android/camera/SettingChecker;->getPreferenceValue(I)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    iput v1, p0, Lcom/android/camera/actor/PhotoActor;->mMaxCaptureNum:I

    :cond_3
    invoke-direct {p0}, Lcom/android/camera/actor/PhotoActor;->isSupportContinuousShot()Z

    move-result v1

    iput-boolean v1, p0, Lcom/android/camera/actor/PhotoActor;->mSupportContinuous:Z

    iget-object v1, p0, Lcom/android/camera/actor/PhotoActor;->mCamera:Lcom/android/camera/Camera;

    invoke-virtual {v1}, Lcom/android/camera/Camera;->keepScreenOnAwhile()V

    iget-object v1, p0, Lcom/android/camera/actor/PhotoActor;->mCameraCategory:Lcom/android/camera/actor/PhotoActor$CameraCategory;

    invoke-virtual {v1}, Lcom/android/camera/actor/PhotoActor$CameraCategory;->switchShutterButton()V

    invoke-direct {p0}, Lcom/android/camera/actor/PhotoActor;->isSupportFaceDetect()Z

    move-result v1

    if-eqz v1, :cond_6

    invoke-virtual {p0}, Lcom/android/camera/actor/PhotoActor;->startFaceDetection()V

    :goto_1
    sget-boolean v1, Lcom/android/camera/actor/PhotoActor;->LOG:Z

    if-eqz v1, :cond_4

    const-string v1, "PhotoActor"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "selfTimer="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/android/camera/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    :cond_4
    iget-boolean v1, p0, Lcom/android/camera/actor/PhotoActor;->mInitialized:Z

    if-nez v1, :cond_1

    invoke-virtual {p0}, Lcom/android/camera/actor/PhotoActor;->initializeFaceView()V

    iget-object v1, p0, Lcom/android/camera/actor/PhotoActor;->mCameraCategory:Lcom/android/camera/actor/PhotoActor$CameraCategory;

    invoke-virtual {v1}, Lcom/android/camera/actor/PhotoActor$CameraCategory;->initializeFirstTime()V

    iget-object v1, p0, Lcom/android/camera/actor/PhotoActor;->mCameraSound:Landroid/media/MediaActionSound;

    if-nez v1, :cond_5

    new-instance v1, Landroid/media/MediaActionSound;

    invoke-direct {v1}, Landroid/media/MediaActionSound;-><init>()V

    iput-object v1, p0, Lcom/android/camera/actor/PhotoActor;->mCameraSound:Landroid/media/MediaActionSound;

    iget-object v1, p0, Lcom/android/camera/actor/PhotoActor;->mCameraSound:Landroid/media/MediaActionSound;

    invoke-virtual {v1, v4}, Landroid/media/MediaActionSound;->load(I)V

    :cond_5
    iput-boolean v4, p0, Lcom/android/camera/actor/PhotoActor;->mInitialized:Z

    goto/16 :goto_0

    :cond_6
    invoke-virtual {p0}, Lcom/android/camera/actor/PhotoActor;->stopFaceDetection()V

    goto :goto_1
.end method

.method public initializeFaceView()V
    .locals 7

    const/4 v2, 0x1

    const/4 v3, 0x0

    sget-boolean v4, Lcom/android/camera/actor/PhotoActor;->LOG:Z

    if-eqz v4, :cond_0

    const-string v4, "PhotoActor"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "initializeFaceView sFaceDetectionStarted="

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    sget-boolean v6, Lcom/android/camera/actor/PhotoActor;->sFaceDetectionStarted:Z

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/android/camera/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    sget-boolean v4, Lcom/android/camera/actor/PhotoActor;->sFaceDetectionStarted:Z

    if-nez v4, :cond_1

    :goto_0
    return-void

    :cond_1
    iget-object v4, p0, Lcom/android/camera/actor/PhotoActor;->mCamera:Lcom/android/camera/Camera;

    invoke-virtual {v4}, Lcom/android/camera/Camera;->getFaceView()Lcom/android/camera/ui/FaceView;

    move-result-object v0

    invoke-virtual {v0}, Lcom/android/camera/ui/FaceView;->clear()V

    invoke-virtual {v0, v3}, Landroid/view/View;->setVisibility(I)V

    iget-object v4, p0, Lcom/android/camera/actor/PhotoActor;->mCamera:Lcom/android/camera/Camera;

    invoke-virtual {v4}, Lcom/android/camera/Camera;->getDisplayOrientation()I

    move-result v4

    invoke-virtual {v0, v4}, Lcom/android/camera/ui/FaceView;->setDisplayOrientation(I)V

    invoke-static {}, Lcom/android/camera/CameraHolder;->instance()Lcom/android/camera/CameraHolder;

    move-result-object v4

    invoke-virtual {v4}, Lcom/android/camera/CameraHolder;->getCameraInfo()[Landroid/hardware/Camera$CameraInfo;

    move-result-object v4

    iget-object v5, p0, Lcom/android/camera/actor/PhotoActor;->mCamera:Lcom/android/camera/Camera;

    invoke-virtual {v5}, Lcom/android/camera/Camera;->getCameraId()I

    move-result v5

    aget-object v1, v4, v5

    iget v4, v1, Landroid/hardware/Camera$CameraInfo;->facing:I

    if-ne v4, v2, :cond_2

    :goto_1
    invoke-virtual {v0, v2}, Lcom/android/camera/ui/FaceView;->setMirror(Z)V

    invoke-virtual {v0}, Lcom/android/camera/ui/FaceView;->resume()V

    iget-object v2, p0, Lcom/android/camera/actor/PhotoActor;->mCamera:Lcom/android/camera/Camera;

    invoke-virtual {v2}, Lcom/android/camera/Camera;->getFocusManager()Lcom/android/camera/FocusManager;

    move-result-object v2

    invoke-virtual {v2, v0}, Lcom/android/camera/FocusManager;->setFaceView(Lcom/android/camera/ui/FaceView;)V

    goto :goto_0

    :cond_2
    move v2, v3

    goto :goto_1
.end method

.method public interruptRenderThread()V
    .locals 1

    iget-object v0, p0, Lcom/android/camera/actor/PhotoActor;->mRenderThread:Lcom/android/camera/actor/PhotoActor$RenderInCapture;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/camera/actor/PhotoActor;->mRenderThread:Lcom/android/camera/actor/PhotoActor$RenderInCapture;

    invoke-virtual {v0}, Ljava/lang/Thread;->interrupt()V

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/android/camera/actor/PhotoActor;->mRenderThread:Lcom/android/camera/actor/PhotoActor$RenderInCapture;

    :cond_0
    return-void
.end method

.method public isCameraPrepareDone()Z
    .locals 6

    const/4 v2, 0x1

    const/4 v1, 0x0

    sget-boolean v3, Lcom/android/camera/actor/PhotoActor;->LOG:Z

    if-eqz v3, :cond_0

    const-string v3, "PhotoActor"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Check camera state in ModeActor, mCameraState="

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-object v5, p0, Lcom/android/camera/actor/PhotoActor;->mCamera:Lcom/android/camera/Camera;

    invoke-virtual {v5}, Lcom/android/camera/Camera;->getCameraState()I

    move-result v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " mCameraClosed="

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-boolean v5, p0, Lcom/android/camera/actor/PhotoActor;->mCameraClosed:Z

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/android/camera/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    iget-object v3, p0, Lcom/android/camera/actor/PhotoActor;->mCamera:Lcom/android/camera/Camera;

    invoke-virtual {v3}, Lcom/android/camera/Camera;->getCameraState()I

    move-result v0

    iget-boolean v3, p0, Lcom/android/camera/actor/PhotoActor;->mCameraClosed:Z

    if-nez v3, :cond_1

    const/4 v3, 0x4

    if-eq v0, v3, :cond_1

    if-nez v0, :cond_2

    :cond_1
    :goto_0
    return v1

    :cond_2
    iget-object v3, p0, Lcom/android/camera/actor/PhotoActor;->mCameraCategory:Lcom/android/camera/actor/PhotoActor$CameraCategory;

    invoke-virtual {v3}, Lcom/android/camera/actor/PhotoActor$CameraCategory;->canshot()Z

    move-result v3

    if-nez v3, :cond_3

    sget-boolean v2, Lcom/android/camera/actor/PhotoActor;->LOG:Z

    if-eqz v2, :cond_1

    const-string v2, "PhotoActor"

    const-string v3, "Not enough space or storage not ready."

    invoke-static {v2, v3}, Lcom/android/camera/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    :cond_3
    iget-object v3, p0, Lcom/android/camera/actor/PhotoActor;->mCamera:Lcom/android/camera/Camera;

    invoke-virtual {v3}, Landroid/content/ContextWrapper;->getMainLooper()Landroid/os/Looper;

    move-result-object v3

    invoke-static {v3}, Lcom/android/camera/manager/SelfTimerManager;->getInstance(Landroid/os/Looper;)Lcom/android/camera/manager/SelfTimerManager;

    move-result-object v3

    invoke-virtual {v3}, Lcom/android/camera/manager/SelfTimerManager;->isSelfTimerCounting()Z

    move-result v3

    if-nez v3, :cond_1

    iget-object v3, p0, Lcom/android/camera/actor/PhotoActor;->mCamera:Lcom/android/camera/Camera;

    invoke-virtual {v3}, Lcom/android/camera/Camera;->getFocusManager()Lcom/android/camera/FocusManager;

    move-result-object v3

    invoke-virtual {v3}, Lcom/android/camera/FocusManager;->isFocusingSnapOnFinish()Z

    move-result v3

    if-nez v3, :cond_4

    const/4 v3, 0x3

    if-eq v0, v3, :cond_4

    iget-boolean v3, p0, Lcom/android/camera/actor/PhotoActor;->mSavingPictures:Z

    if-eqz v3, :cond_5

    :cond_4
    iget-object v3, p0, Lcom/android/camera/actor/PhotoActor;->mCamera:Lcom/android/camera/Camera;

    invoke-virtual {v3}, Lcom/android/camera/Camera;->isImageCaptureIntent()Z

    move-result v3

    if-nez v3, :cond_5

    iput-boolean v2, p0, Lcom/android/camera/actor/PhotoActor;->mSnapshotOnIdle:Z

    goto :goto_0

    :cond_5
    invoke-direct {p0}, Lcom/android/camera/actor/PhotoActor;->isBusy()Z

    move-result v3

    if-eqz v3, :cond_6

    iget-object v2, p0, Lcom/android/camera/actor/PhotoActor;->mCamera:Lcom/android/camera/Camera;

    iget-object v3, p0, Lcom/android/camera/actor/PhotoActor;->mCamera:Lcom/android/camera/Camera;

    const v4, 0x7f0c000e

    invoke-virtual {v3, v4}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/android/camera/Camera;->showInfo(Ljava/lang/String;)V

    goto :goto_0

    :cond_6
    iput-boolean v1, p0, Lcom/android/camera/actor/PhotoActor;->mSnapshotOnIdle:Z

    move v1, v2

    goto :goto_0
.end method

.method public onBackPressed()Z
    .locals 5

    const/4 v1, 0x0

    const/4 v0, 0x1

    sget-boolean v2, Lcom/android/camera/actor/PhotoActor;->LOG:Z

    if-eqz v2, :cond_0

    const-string v2, "PhotoActor"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "onBackPressed() isFinishing()="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lcom/android/camera/actor/PhotoActor;->mCamera:Lcom/android/camera/Camera;

    invoke-virtual {v4}, Landroid/app/Activity;->isFinishing()Z

    move-result v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/android/camera/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    invoke-direct {p0}, Lcom/android/camera/actor/PhotoActor;->isCameraIdle()Z

    move-result v2

    if-nez v2, :cond_3

    iget-boolean v2, p0, Lcom/android/camera/actor/PhotoActor;->mSelftimerCounting:Z

    if-eqz v2, :cond_1

    iget-object v2, p0, Lcom/android/camera/actor/PhotoActor;->mCamera:Lcom/android/camera/Camera;

    invoke-virtual {v2}, Landroid/content/ContextWrapper;->getMainLooper()Landroid/os/Looper;

    move-result-object v2

    invoke-static {v2}, Lcom/android/camera/manager/SelfTimerManager;->getInstance(Landroid/os/Looper;)Lcom/android/camera/manager/SelfTimerManager;

    move-result-object v2

    invoke-virtual {v2}, Lcom/android/camera/manager/SelfTimerManager;->breakTimer()V

    iput-boolean v1, p0, Lcom/android/camera/actor/PhotoActor;->mSelftimerCounting:Z

    iget-object v1, p0, Lcom/android/camera/actor/PhotoActor;->mCamera:Lcom/android/camera/Camera;

    invoke-virtual {v1, v0}, Lcom/android/camera/Camera;->setCameraState(I)V

    iget-object v1, p0, Lcom/android/camera/actor/PhotoActor;->mCamera:Lcom/android/camera/Camera;

    invoke-virtual {v1, v0}, Lcom/android/camera/Camera;->setSwipingEnabled(Z)V

    iget-object v1, p0, Lcom/android/camera/actor/PhotoActor;->mCamera:Lcom/android/camera/Camera;

    invoke-virtual {v1}, Lcom/android/camera/Camera;->restoreViewState()V

    :cond_1
    iget-object v1, p0, Lcom/android/camera/actor/PhotoActor;->mCamera:Lcom/android/camera/Camera;

    invoke-virtual {v1}, Lcom/android/camera/Camera;->isImageCaptureIntent()Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/android/camera/actor/PhotoActor;->mCamera:Lcom/android/camera/Camera;

    invoke-virtual {v1}, Lcom/android/camera/Camera;->getShutterManager()Lcom/android/camera/manager/ShutterManager;

    move-result-object v1

    invoke-virtual {v1}, Lcom/android/camera/manager/ShutterManager;->getShutterType()I

    move-result v1

    const/4 v2, 0x3

    if-ne v1, v2, :cond_2

    iget-object v1, p0, Lcom/android/camera/actor/PhotoActor;->mCancelListener:Landroid/view/View$OnClickListener;

    const/4 v2, 0x0

    invoke-interface {v1, v2}, Landroid/view/View$OnClickListener;->onClick(Landroid/view/View;)V

    :cond_2
    :goto_0
    return v0

    :cond_3
    iget-object v2, p0, Lcom/android/camera/actor/PhotoActor;->mCameraCategory:Lcom/android/camera/actor/PhotoActor$CameraCategory;

    invoke-virtual {v2}, Lcom/android/camera/actor/PhotoActor$CameraCategory;->doCancelCapture()Z

    move-result v2

    if-nez v2, :cond_2

    move v0, v1

    goto :goto_0
.end method

.method public onBurstSaveDone()V
    .locals 2

    const/4 v1, 0x0

    iget-boolean v0, p0, Lcom/android/camera/actor/PhotoActor;->mCameraClosed:Z

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/android/camera/actor/PhotoActor;->mCamera:Lcom/android/camera/Camera;

    invoke-virtual {v0}, Lcom/android/camera/Camera;->isImageCaptureIntent()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/android/camera/actor/PhotoActor;->mCamera:Lcom/android/camera/Camera;

    invoke-virtual {v0}, Lcom/android/camera/Camera;->cancelContinuousShot()V

    invoke-virtual {p0, v1}, Lcom/android/camera/actor/PhotoActor;->restartPreview(Z)V

    :cond_0
    iput-boolean v1, p0, Lcom/android/camera/actor/PhotoActor;->mSavingPictures:Z

    return-void
.end method

.method public onCameraClose()V
    .locals 3

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/android/camera/actor/PhotoActor;->mCameraClosed:Z

    sget-boolean v0, Lcom/android/camera/actor/PhotoActor;->LOG:Z

    if-eqz v0, :cond_0

    const-string v0, "PhotoActor"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "onCameraClose mCameraClosed ="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-boolean v2, p0, Lcom/android/camera/actor/PhotoActor;->mCameraClosed:Z

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/android/camera/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    iget-object v0, p0, Lcom/android/camera/actor/PhotoActor;->mHandler:Landroid/os/Handler;

    const/16 v1, 0x64

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeMessages(I)V

    iget-object v0, p0, Lcom/android/camera/actor/PhotoActor;->mHandler:Landroid/os/Handler;

    const/16 v1, 0x67

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeMessages(I)V

    invoke-virtual {p0}, Lcom/android/camera/actor/PhotoActor;->resetPhotoActor()V

    iget-object v0, p0, Lcom/android/camera/actor/PhotoActor;->mCameraCategory:Lcom/android/camera/actor/PhotoActor$CameraCategory;

    invoke-virtual {v0}, Lcom/android/camera/actor/PhotoActor$CameraCategory;->onLeaveActor()V

    invoke-virtual {p0}, Lcom/android/camera/actor/PhotoActor;->stopPreview()V

    return-void
.end method

.method public onCameraOpenDone()V
    .locals 3

    const/4 v2, 0x0

    sget-boolean v0, Lcom/android/camera/actor/PhotoActor;->LOG:Z

    if-eqz v0, :cond_0

    const-string v0, "PhotoActor"

    const-string v1, "onCameraOpenDone"

    invoke-static {v0, v1}, Lcom/android/camera/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    iput-boolean v2, p0, Lcom/android/camera/actor/PhotoActor;->mCameraClosed:Z

    sput-boolean v2, Lcom/android/camera/actor/PhotoActor;->sFaceDetectionStarted:Z

    return-void
.end method

.method public onCameraParameterReady(Z)V
    .locals 3
    .param p1    # Z

    invoke-super {p0, p1}, Lcom/android/camera/actor/CameraActor;->onCameraParameterReady(Z)V

    sget-boolean v0, Lcom/android/camera/actor/PhotoActor;->LOG:Z

    if-eqz v0, :cond_0

    const-string v0, "PhotoActor"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "onCameraParameterReady startPreview="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/android/camera/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    if-eqz p1, :cond_1

    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/android/camera/actor/PhotoActor;->startPreview(Z)V

    iget-object v0, p0, Lcom/android/camera/actor/PhotoActor;->mHandler:Landroid/os/Handler;

    const/16 v1, 0x64

    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    :goto_0
    return-void

    :cond_1
    iget-object v0, p0, Lcom/android/camera/actor/PhotoActor;->mHandler:Landroid/os/Handler;

    const/16 v1, 0x67

    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    goto :goto_0
.end method

.method public onConinuousShotDone(I)V
    .locals 4
    .param p1    # I

    const/4 v3, 0x1

    sget-boolean v0, Lcom/android/camera/actor/PhotoActor;->LOG:Z

    if-eqz v0, :cond_0

    const-string v0, "PhotoActor"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "onContinuousShotDone, pictures saved = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/android/camera/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    new-instance v0, Lcom/android/camera/actor/PhotoActor$WaitSavingDoneThread;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/android/camera/actor/PhotoActor$WaitSavingDoneThread;-><init>(Lcom/android/camera/actor/PhotoActor;Lcom/android/camera/actor/PhotoActor$1;)V

    iput-object v0, p0, Lcom/android/camera/actor/PhotoActor;->mWaitSavingDoneThread:Ljava/lang/Thread;

    iget-object v0, p0, Lcom/android/camera/actor/PhotoActor;->mWaitSavingDoneThread:Ljava/lang/Thread;

    invoke-virtual {v0}, Ljava/lang/Thread;->start()V

    invoke-virtual {p0, v3, v3}, Lcom/android/camera/actor/PhotoActor;->updateSavingHint(ZZ)V

    return-void
.end method

.method public onImagePickSaveDone()V
    .locals 3

    iget-boolean v1, p0, Lcom/android/camera/actor/PhotoActor;->mCameraClosed:Z

    if-nez v1, :cond_0

    iget-object v1, p0, Lcom/android/camera/actor/PhotoActor;->mCamera:Lcom/android/camera/Camera;

    invoke-virtual {v1}, Landroid/app/Activity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    iget-object v1, p0, Lcom/android/camera/actor/PhotoActor;->mSaveRequest:Lcom/android/camera/SaveRequest;

    invoke-interface {v1}, Lcom/android/camera/SaveRequest;->getUri()Landroid/net/Uri;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setData(Landroid/net/Uri;)Landroid/content/Intent;

    iget-object v1, p0, Lcom/android/camera/actor/PhotoActor;->mCamera:Lcom/android/camera/Camera;

    const/4 v2, -0x1

    invoke-virtual {v1, v2, v0}, Lcom/android/camera/Camera;->setResultExAndFinish(ILandroid/content/Intent;)V

    :cond_0
    return-void
.end method

.method public onKeyDown(ILandroid/view/KeyEvent;)Z
    .locals 6
    .param p1    # I
    .param p2    # Landroid/view/KeyEvent;

    const/4 v5, 0x0

    const/4 v1, 0x0

    const/4 v0, 0x1

    sget-boolean v2, Lcom/android/camera/actor/PhotoActor;->LOG:Z

    if-eqz v2, :cond_0

    const-string v2, "PhotoActor"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "onKeyDown keyCode="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/android/camera/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    sparse-switch p1, :sswitch_data_0

    move v0, v1

    :cond_1
    :goto_0
    return v0

    :sswitch_0
    iget-boolean v1, p0, Lcom/android/camera/actor/PhotoActor;->mInitialized:Z

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/android/camera/actor/PhotoActor;->mCamera:Lcom/android/camera/Camera;

    invoke-virtual {v1}, Lcom/android/camera/ActivityBase;->isFullScreen()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-virtual {p2}, Landroid/view/KeyEvent;->getRepeatCount()I

    move-result v1

    if-nez v1, :cond_1

    iget-object v1, p0, Lcom/android/camera/actor/PhotoActor;->mCamera:Lcom/android/camera/Camera;

    invoke-virtual {v1, v0}, Lcom/android/camera/Camera;->collapseViewManager(Z)Z

    iget-object v1, p0, Lcom/android/camera/actor/PhotoActor;->mCamera:Lcom/android/camera/Camera;

    invoke-virtual {v1}, Lcom/android/camera/Camera;->getCameraState()I

    move-result v1

    const/4 v2, 0x3

    if-eq v1, v2, :cond_1

    iget-object v1, p0, Lcom/android/camera/actor/PhotoActor;->mCamera:Lcom/android/camera/Camera;

    invoke-virtual {v1}, Landroid/content/ContextWrapper;->getMainLooper()Landroid/os/Looper;

    move-result-object v1

    invoke-static {v1}, Lcom/android/camera/manager/SelfTimerManager;->getInstance(Landroid/os/Looper;)Lcom/android/camera/manager/SelfTimerManager;

    move-result-object v1

    invoke-virtual {v1}, Lcom/android/camera/manager/SelfTimerManager;->isSelfTimerEnabled()Z

    move-result v1

    if-nez v1, :cond_1

    invoke-direct {p0}, Lcom/android/camera/actor/PhotoActor;->canTakePicture()Z

    move-result v1

    if-eqz v1, :cond_1

    iput-boolean v0, p0, Lcom/android/camera/actor/PhotoActor;->mKeyHalfPressed:Z

    iget-object v1, p0, Lcom/android/camera/actor/PhotoActor;->mCamera:Lcom/android/camera/Camera;

    invoke-virtual {v1}, Lcom/android/camera/Camera;->getFocusManager()Lcom/android/camera/FocusManager;

    move-result-object v1

    invoke-virtual {v1}, Lcom/android/camera/FocusManager;->onShutterDown()V

    goto :goto_0

    :sswitch_1
    iget-boolean v2, p0, Lcom/android/camera/actor/PhotoActor;->mInitialized:Z

    if-eqz v2, :cond_1

    iget-object v2, p0, Lcom/android/camera/actor/PhotoActor;->mCamera:Lcom/android/camera/Camera;

    invoke-virtual {v2}, Lcom/android/camera/ActivityBase;->isFullScreen()Z

    move-result v2

    if-eqz v2, :cond_1

    iget-boolean v2, p0, Lcom/android/camera/actor/PhotoActor;->mCameraKeyLongPressed:Z

    if-nez v2, :cond_1

    invoke-virtual {p2}, Landroid/view/KeyEvent;->getRepeatCount()I

    move-result v2

    if-lez v2, :cond_1

    iget-object v2, p0, Lcom/android/camera/actor/PhotoActor;->mCamera:Lcom/android/camera/Camera;

    invoke-virtual {v2}, Lcom/android/camera/Camera;->getOrietation()I

    move-result v2

    const/4 v3, -0x1

    if-ne v2, v3, :cond_2

    move v0, v1

    goto :goto_0

    :cond_2
    invoke-virtual {p0, v5}, Lcom/android/camera/actor/PhotoActor;->onShutterButtonLongPressed(Lcom/android/camera/ui/ShutterButton;)V

    iput-boolean v0, p0, Lcom/android/camera/actor/PhotoActor;->mCameraKeyLongPressed:Z

    goto :goto_0

    :sswitch_2
    iget-object v1, p0, Lcom/android/camera/actor/PhotoActor;->mCamera:Lcom/android/camera/Camera;

    invoke-virtual {v1, v0}, Lcom/android/camera/Camera;->collapseViewManager(Z)Z

    iget-boolean v1, p0, Lcom/android/camera/actor/PhotoActor;->mInitialized:Z

    if-eqz v1, :cond_1

    invoke-virtual {p2}, Landroid/view/KeyEvent;->getRepeatCount()I

    move-result v1

    if-nez v1, :cond_1

    invoke-virtual {p0, v5, v0}, Lcom/android/camera/actor/PhotoActor;->onShutterButtonFocus(Lcom/android/camera/ui/ShutterButton;Z)V

    iget-object v1, p0, Lcom/android/camera/actor/PhotoActor;->mCamera:Lcom/android/camera/Camera;

    invoke-virtual {v1}, Lcom/android/camera/Camera;->getShutterManager()Lcom/android/camera/manager/ShutterManager;

    move-result-object v1

    invoke-virtual {v1}, Lcom/android/camera/manager/ShutterManager;->getPhotoShutter()Lcom/android/camera/ui/ShutterButton;

    move-result-object v1

    invoke-virtual {v1}, Landroid/view/View;->isInTouchMode()Z

    move-result v1

    if-eqz v1, :cond_3

    iget-object v1, p0, Lcom/android/camera/actor/PhotoActor;->mCamera:Lcom/android/camera/Camera;

    invoke-virtual {v1}, Lcom/android/camera/Camera;->getShutterManager()Lcom/android/camera/manager/ShutterManager;

    move-result-object v1

    invoke-virtual {v1}, Lcom/android/camera/manager/ShutterManager;->getPhotoShutter()Lcom/android/camera/ui/ShutterButton;

    move-result-object v1

    invoke-virtual {v1}, Landroid/view/View;->requestFocusFromTouch()Z

    :goto_1
    iget-object v1, p0, Lcom/android/camera/actor/PhotoActor;->mCamera:Lcom/android/camera/Camera;

    invoke-virtual {v1}, Lcom/android/camera/Camera;->getShutterManager()Lcom/android/camera/manager/ShutterManager;

    move-result-object v1

    invoke-virtual {v1}, Lcom/android/camera/manager/ShutterManager;->getPhotoShutter()Lcom/android/camera/ui/ShutterButton;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/view/View;->setPressed(Z)V

    goto/16 :goto_0

    :cond_3
    iget-object v1, p0, Lcom/android/camera/actor/PhotoActor;->mCamera:Lcom/android/camera/Camera;

    invoke-virtual {v1}, Lcom/android/camera/Camera;->getShutterManager()Lcom/android/camera/manager/ShutterManager;

    move-result-object v1

    invoke-virtual {v1}, Lcom/android/camera/manager/ShutterManager;->getPhotoShutter()Lcom/android/camera/ui/ShutterButton;

    move-result-object v1

    invoke-virtual {v1}, Landroid/view/View;->requestFocus()Z

    goto :goto_1

    nop

    :sswitch_data_0
    .sparse-switch
        0x17 -> :sswitch_2
        0x1b -> :sswitch_1
        0x50 -> :sswitch_0
    .end sparse-switch
.end method

.method public onKeyUp(ILandroid/view/KeyEvent;)Z
    .locals 6
    .param p1    # I
    .param p2    # Landroid/view/KeyEvent;

    const/4 v5, 0x0

    const/4 v0, 0x1

    const/4 v1, 0x0

    sget-boolean v2, Lcom/android/camera/actor/PhotoActor;->LOG:Z

    if-eqz v2, :cond_0

    const-string v2, "PhotoActor"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "onKeyUp keyCode="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/android/camera/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    sparse-switch p1, :sswitch_data_0

    move v0, v1

    :cond_1
    :goto_0
    return v0

    :sswitch_0
    iget-boolean v2, p0, Lcom/android/camera/actor/PhotoActor;->mInitialized:Z

    if-eqz v2, :cond_1

    invoke-virtual {p0, v5, v1}, Lcom/android/camera/actor/PhotoActor;->onShutterButtonFocus(Lcom/android/camera/ui/ShutterButton;Z)V

    iget-object v2, p0, Lcom/android/camera/actor/PhotoActor;->mCamera:Lcom/android/camera/Camera;

    invoke-virtual {v2}, Lcom/android/camera/Camera;->getCameraState()I

    move-result v2

    const/4 v3, 0x3

    if-eq v2, v3, :cond_1

    iget-object v2, p0, Lcom/android/camera/actor/PhotoActor;->mCamera:Lcom/android/camera/Camera;

    invoke-virtual {v2}, Landroid/content/ContextWrapper;->getMainLooper()Landroid/os/Looper;

    move-result-object v2

    invoke-static {v2}, Lcom/android/camera/manager/SelfTimerManager;->getInstance(Landroid/os/Looper;)Lcom/android/camera/manager/SelfTimerManager;

    move-result-object v2

    invoke-virtual {v2}, Lcom/android/camera/manager/SelfTimerManager;->isSelfTimerEnabled()Z

    move-result v2

    if-nez v2, :cond_1

    iput-boolean v1, p0, Lcom/android/camera/actor/PhotoActor;->mKeyHalfPressed:Z

    iget-object v1, p0, Lcom/android/camera/actor/PhotoActor;->mCamera:Lcom/android/camera/Camera;

    invoke-virtual {v1}, Lcom/android/camera/Camera;->getFocusManager()Lcom/android/camera/FocusManager;

    move-result-object v1

    invoke-virtual {v1}, Lcom/android/camera/FocusManager;->onShutterUp()V

    goto :goto_0

    :sswitch_1
    iget-boolean v2, p0, Lcom/android/camera/actor/PhotoActor;->mInitialized:Z

    if-eqz v2, :cond_3

    iget-boolean v2, p0, Lcom/android/camera/actor/PhotoActor;->mCameraKeyLongPressed:Z

    if-nez v2, :cond_3

    invoke-virtual {p2}, Landroid/view/KeyEvent;->getRepeatCount()I

    move-result v2

    if-nez v2, :cond_3

    iget-object v2, p0, Lcom/android/camera/actor/PhotoActor;->mCamera:Lcom/android/camera/Camera;

    invoke-virtual {v2}, Lcom/android/camera/ActivityBase;->isFullScreen()Z

    move-result v2

    if-eqz v2, :cond_3

    iget-object v2, p0, Lcom/android/camera/actor/PhotoActor;->mCamera:Lcom/android/camera/Camera;

    invoke-virtual {v2}, Lcom/android/camera/Camera;->getOrietation()I

    move-result v2

    const/4 v3, -0x1

    if-ne v2, v3, :cond_2

    move v0, v1

    goto :goto_0

    :cond_2
    invoke-virtual {p0, v5}, Lcom/android/camera/actor/PhotoActor;->onShutterButtonClick(Lcom/android/camera/ui/ShutterButton;)V

    :cond_3
    iput-boolean v1, p0, Lcom/android/camera/actor/PhotoActor;->mCameraKeyLongPressed:Z

    goto :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x1b -> :sswitch_1
        0x50 -> :sswitch_0
    .end sparse-switch
.end method

.method protected onPreviewStartDone()V
    .locals 2

    const/4 v1, 0x1

    iget-boolean v0, p0, Lcom/android/camera/actor/PhotoActor;->mCameraClosed:Z

    if-eqz v0, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v0, p0, Lcom/android/camera/actor/PhotoActor;->mCamera:Lcom/android/camera/Camera;

    invoke-virtual {v0, v1}, Lcom/android/camera/Camera;->setCameraState(I)V

    iget-object v0, p0, Lcom/android/camera/actor/PhotoActor;->mCamera:Lcom/android/camera/Camera;

    invoke-virtual {v0}, Lcom/android/camera/Camera;->isImageCaptureIntent()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/android/camera/actor/PhotoActor;->mWaitSavingDoneThread:Ljava/lang/Thread;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/camera/actor/PhotoActor;->mWaitSavingDoneThread:Ljava/lang/Thread;

    invoke-virtual {v0}, Ljava/lang/Thread;->isAlive()Z

    move-result v0

    if-eqz v0, :cond_0

    iput-boolean v1, p0, Lcom/android/camera/actor/PhotoActor;->mSavingPictures:Z

    goto :goto_0
.end method

.method public onShutterButtonClick(Lcom/android/camera/ui/ShutterButton;)V
    .locals 4
    .param p1    # Lcom/android/camera/ui/ShutterButton;

    const/4 v3, 0x0

    sget-boolean v0, Lcom/android/camera/actor/PhotoActor;->LOG:Z

    if-eqz v0, :cond_0

    const-string v0, "PhotoActor"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Photo.onShutterButtonClick("

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ")"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/android/camera/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    invoke-static {}, Lcom/android/camera/manager/MMProfileManager;->triggerPhotoShutterClick()V

    iget-boolean v0, p0, Lcom/android/camera/actor/PhotoActor;->mContinuousShotPerformed:Z

    if-nez v0, :cond_1

    iget-boolean v0, p0, Lcom/android/camera/actor/PhotoActor;->mIgnoreClick:Z

    if-nez v0, :cond_1

    invoke-virtual {p0}, Lcom/android/camera/actor/PhotoActor;->isCameraPrepareDone()Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/android/camera/actor/PhotoActor;->mCamera:Lcom/android/camera/Camera;

    invoke-virtual {v0}, Landroid/content/ContextWrapper;->getMainLooper()Landroid/os/Looper;

    move-result-object v0

    invoke-static {v0}, Lcom/android/camera/manager/SelfTimerManager;->getInstance(Landroid/os/Looper;)Lcom/android/camera/manager/SelfTimerManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/android/camera/manager/SelfTimerManager;->checkSelfTimerMode()Z

    move-result v0

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/android/camera/actor/PhotoActor;->mCamera:Lcom/android/camera/Camera;

    invoke-virtual {v0, v3}, Lcom/android/camera/Camera;->setSwipingEnabled(Z)V

    iget-object v0, p0, Lcom/android/camera/actor/PhotoActor;->mCamera:Lcom/android/camera/Camera;

    invoke-virtual {v0, v3}, Lcom/android/camera/Camera;->setViewState(I)V

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/android/camera/actor/PhotoActor;->mSelftimerCounting:Z

    :cond_1
    :goto_0
    return-void

    :cond_2
    iput-boolean v3, p0, Lcom/android/camera/actor/PhotoActor;->mSelftimerCounting:Z

    iget-object v0, p0, Lcom/android/camera/actor/PhotoActor;->mCamera:Lcom/android/camera/Camera;

    invoke-virtual {v0}, Lcom/android/camera/Camera;->getFocusManager()Lcom/android/camera/FocusManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/android/camera/FocusManager;->doSnap()V

    iget-object v0, p0, Lcom/android/camera/actor/PhotoActor;->mCameraCategory:Lcom/android/camera/actor/PhotoActor$CameraCategory;

    invoke-virtual {v0}, Lcom/android/camera/actor/PhotoActor$CameraCategory;->doShutter()V

    goto :goto_0
.end method

.method public onShutterButtonFocus(Lcom/android/camera/ui/ShutterButton;Z)V
    .locals 6
    .param p1    # Lcom/android/camera/ui/ShutterButton;
    .param p2    # Z

    const/4 v5, 0x3

    const/4 v4, 0x1

    const/4 v3, 0x0

    sget-boolean v0, Lcom/android/camera/actor/PhotoActor;->LOG:Z

    if-eqz v0, :cond_0

    const-string v0, "PhotoActor"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Photo.onShutterButtonFocus("

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ")"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " mContinuousShotPerformed="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-boolean v2, p0, Lcom/android/camera/actor/PhotoActor;->mContinuousShotPerformed:Z

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " mCameraClosed="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-boolean v2, p0, Lcom/android/camera/actor/PhotoActor;->mCameraClosed:Z

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " camera.state="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/android/camera/actor/PhotoActor;->mCamera:Lcom/android/camera/Camera;

    invoke-virtual {v2}, Lcom/android/camera/Camera;->getCameraState()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " mCameraCategory.supportContinuousShot()="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/android/camera/actor/PhotoActor;->mCameraCategory:Lcom/android/camera/actor/PhotoActor$CameraCategory;

    invoke-virtual {v2}, Lcom/android/camera/actor/PhotoActor$CameraCategory;->supportContinuousShot()Z

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " mCamera.isImageCaptureIntent()="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/android/camera/actor/PhotoActor;->mCamera:Lcom/android/camera/Camera;

    invoke-virtual {v2}, Lcom/android/camera/Camera;->isImageCaptureIntent()Z

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/android/camera/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    iget-object v0, p0, Lcom/android/camera/actor/PhotoActor;->mCamera:Lcom/android/camera/Camera;

    invoke-virtual {v0, v4}, Lcom/android/camera/Camera;->collapseViewManager(Z)Z

    iget-object v0, p0, Lcom/android/camera/actor/PhotoActor;->mCamera:Lcom/android/camera/Camera;

    invoke-virtual {v0}, Lcom/android/camera/Camera;->isImageCaptureIntent()Z

    move-result v0

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/android/camera/actor/PhotoActor;->mCameraCategory:Lcom/android/camera/actor/PhotoActor$CameraCategory;

    invoke-virtual {v0}, Lcom/android/camera/actor/PhotoActor$CameraCategory;->supportContinuousShot()Z

    move-result v0

    if-nez v0, :cond_6

    :cond_1
    iget-boolean v0, p0, Lcom/android/camera/actor/PhotoActor;->mCameraClosed:Z

    if-nez v0, :cond_2

    iget-object v0, p0, Lcom/android/camera/actor/PhotoActor;->mCameraCategory:Lcom/android/camera/actor/PhotoActor$CameraCategory;

    invoke-virtual {v0}, Lcom/android/camera/actor/PhotoActor$CameraCategory;->skipFocus()Z

    move-result v0

    if-nez v0, :cond_2

    iget-object v0, p0, Lcom/android/camera/actor/PhotoActor;->mCamera:Lcom/android/camera/Camera;

    invoke-virtual {v0}, Lcom/android/camera/Camera;->getCameraState()I

    move-result v0

    if-eq v0, v5, :cond_2

    iget-object v0, p0, Lcom/android/camera/actor/PhotoActor;->mCamera:Lcom/android/camera/Camera;

    invoke-virtual {v0}, Lcom/android/camera/Camera;->getCameraState()I

    move-result v0

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/android/camera/actor/PhotoActor;->mCamera:Lcom/android/camera/Camera;

    invoke-virtual {v0}, Landroid/content/ContextWrapper;->getMainLooper()Landroid/os/Looper;

    move-result-object v0

    invoke-static {v0}, Lcom/android/camera/manager/SelfTimerManager;->getInstance(Landroid/os/Looper;)Lcom/android/camera/manager/SelfTimerManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/android/camera/manager/SelfTimerManager;->isSelfTimerEnabled()Z

    move-result v0

    if-eqz v0, :cond_3

    :cond_2
    :goto_0
    return-void

    :cond_3
    if-eqz p2, :cond_4

    invoke-direct {p0}, Lcom/android/camera/actor/PhotoActor;->canTakePicture()Z

    move-result v0

    if-eqz v0, :cond_2

    :cond_4
    if-eqz p2, :cond_5

    iget-object v0, p0, Lcom/android/camera/actor/PhotoActor;->mCameraCategory:Lcom/android/camera/actor/PhotoActor$CameraCategory;

    invoke-virtual {v0}, Lcom/android/camera/actor/PhotoActor$CameraCategory;->shutterPressed()V

    goto :goto_0

    :cond_5
    iget-object v0, p0, Lcom/android/camera/actor/PhotoActor;->mCameraCategory:Lcom/android/camera/actor/PhotoActor$CameraCategory;

    invoke-virtual {v0}, Lcom/android/camera/actor/PhotoActor$CameraCategory;->shutterUp()V

    goto :goto_0

    :cond_6
    if-nez p2, :cond_9

    iget-boolean v0, p0, Lcom/android/camera/actor/PhotoActor;->mContinuousShotPerformed:Z

    if-eqz v0, :cond_9

    iget-boolean v0, p0, Lcom/android/camera/actor/PhotoActor;->mCameraClosed:Z

    if-nez v0, :cond_9

    sget-boolean v0, Lcom/android/camera/actor/PhotoActor;->LOG:Z

    if-eqz v0, :cond_7

    const-string v0, "PhotoActor"

    const-string v1, "Button up Msg received, start to Cancel continuous shot"

    invoke-static {v0, v1}, Lcom/android/camera/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    :cond_7
    iput-boolean v3, p0, Lcom/android/camera/actor/PhotoActor;->mContinuousShotPerformed:Z

    iget-object v0, p0, Lcom/android/camera/actor/PhotoActor;->mCamera:Lcom/android/camera/Camera;

    invoke-virtual {v0}, Lcom/android/camera/Camera;->getCameraState()I

    move-result v0

    if-ne v0, v5, :cond_a

    invoke-direct {p0}, Lcom/android/camera/actor/PhotoActor;->cancelContinuousShot()V

    iget v0, p0, Lcom/android/camera/actor/PhotoActor;->mCurrentShotsNum:I

    if-eqz v0, :cond_8

    invoke-virtual {p0, v4, v3}, Lcom/android/camera/actor/PhotoActor;->updateSavingHint(ZZ)V

    :cond_8
    iput-boolean v4, p0, Lcom/android/camera/actor/PhotoActor;->mSavingPictures:Z

    :cond_9
    :goto_1
    iput-boolean v3, p0, Lcom/android/camera/actor/PhotoActor;->mIgnoreClick:Z

    goto :goto_0

    :cond_a
    iget-object v0, p0, Lcom/android/camera/actor/PhotoActor;->mCamera:Lcom/android/camera/Camera;

    invoke-virtual {v0}, Lcom/android/camera/Camera;->cancelContinuousShot()V

    goto :goto_1
.end method

.method public onShutterButtonLongPressed(Lcom/android/camera/ui/ShutterButton;)V
    .locals 3
    .param p1    # Lcom/android/camera/ui/ShutterButton;

    sget-boolean v0, Lcom/android/camera/actor/PhotoActor;->LOG:Z

    if-eqz v0, :cond_0

    const-string v0, "PhotoActor"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Photo.onShutterButtonLongPressed("

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ")"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/android/camera/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    iget-object v0, p0, Lcom/android/camera/actor/PhotoActor;->mCamera:Lcom/android/camera/Camera;

    invoke-virtual {v0}, Lcom/android/camera/Camera;->isImageCaptureIntent()Z

    move-result v0

    if-nez v0, :cond_1

    iget-boolean v0, p0, Lcom/android/camera/actor/PhotoActor;->mSupportContinuous:Z

    if-nez v0, :cond_3

    :cond_1
    iget-object v0, p0, Lcom/android/camera/actor/PhotoActor;->mCamera:Lcom/android/camera/Camera;

    iget-object v1, p0, Lcom/android/camera/actor/PhotoActor;->mCamera:Lcom/android/camera/Camera;

    const v2, 0x7f0c0015

    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/android/camera/Camera;->showInfo(Ljava/lang/String;)V

    :cond_2
    :goto_0
    return-void

    :cond_3
    invoke-virtual {p0}, Lcom/android/camera/actor/PhotoActor;->isCameraPrepareDone()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-static {}, Lcom/android/camera/Util;->clearMemoryLimit()V

    iget-object v0, p0, Lcom/android/camera/actor/PhotoActor;->mMemoryManager:Lcom/android/camera/actor/PhotoActor$MemoryManager;

    invoke-virtual {v0}, Lcom/android/camera/actor/PhotoActor$MemoryManager;->initMemory()V

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/android/camera/actor/PhotoActor;->mContinuousShotPerformed:Z

    const/4 v0, 0x0

    iput v0, p0, Lcom/android/camera/actor/PhotoActor;->mCurrentShotsNum:I

    iget-object v0, p0, Lcom/android/camera/actor/PhotoActor;->mCamera:Lcom/android/camera/Camera;

    invoke-virtual {v0}, Lcom/android/camera/Camera;->applyContinousShot()V

    iget-object v0, p0, Lcom/android/camera/actor/PhotoActor;->mCamera:Lcom/android/camera/Camera;

    invoke-virtual {v0}, Lcom/android/camera/Camera;->getThumbnailManager()Lcom/android/camera/manager/ThumbnailManager;

    move-result-object v0

    const/16 v1, 0x1f4

    invoke-virtual {v0, v1}, Lcom/android/camera/manager/ThumbnailManager;->setRefreshInterval(I)V

    iget-object v0, p0, Lcom/android/camera/actor/PhotoActor;->mCamera:Lcom/android/camera/Camera;

    invoke-virtual {v0}, Lcom/android/camera/Camera;->getFocusManager()Lcom/android/camera/FocusManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/android/camera/FocusManager;->clearFocusOnContinuous()V

    iget-object v0, p0, Lcom/android/camera/actor/PhotoActor;->mCamera:Lcom/android/camera/Camera;

    invoke-virtual {v0}, Lcom/android/camera/Camera;->getFocusManager()Lcom/android/camera/FocusManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/android/camera/FocusManager;->doSnap()V

    goto :goto_0
.end method

.method public onUserInteraction()Z
    .locals 1

    iget-object v0, p0, Lcom/android/camera/actor/PhotoActor;->mCamera:Lcom/android/camera/Camera;

    invoke-virtual {v0}, Lcom/android/camera/Camera;->keepScreenOnAwhile()V

    const/4 v0, 0x1

    return v0
.end method

.method protected overrideFocusMode(Ljava/lang/String;)V
    .locals 1
    .param p1    # Ljava/lang/String;

    if-eqz p1, :cond_0

    iget-object v0, p0, Lcom/android/camera/actor/PhotoActor;->mCamera:Lcom/android/camera/Camera;

    invoke-virtual {v0}, Lcom/android/camera/Camera;->getParameters()Landroid/hardware/Camera$Parameters;

    move-result-object v0

    invoke-virtual {v0}, Landroid/hardware/Camera$Parameters;->getSupportedFocusModes()Ljava/util/List;

    move-result-object v0

    invoke-static {p1, v0}, Lcom/android/camera/SettingChecker;->isSupported(Ljava/lang/Object;Ljava/util/List;)Z

    move-result v0

    if-nez v0, :cond_0

    const-string p1, "infinity"

    :cond_0
    iget-boolean v0, p0, Lcom/android/camera/actor/PhotoActor;->mCameraClosed:Z

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/android/camera/actor/PhotoActor;->mCamera:Lcom/android/camera/Camera;

    invoke-virtual {v0}, Lcom/android/camera/Camera;->getFocusManager()Lcom/android/camera/FocusManager;

    move-result-object v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/android/camera/actor/PhotoActor;->mCamera:Lcom/android/camera/Camera;

    invoke-virtual {v0}, Lcom/android/camera/Camera;->getFocusManager()Lcom/android/camera/FocusManager;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/android/camera/FocusManager;->overrideFocusMode(Ljava/lang/String;)V

    :cond_1
    return-void
.end method

.method public playSound(I)V
    .locals 1
    .param p1    # I

    iget-object v0, p0, Lcom/android/camera/actor/PhotoActor;->mCameraSound:Landroid/media/MediaActionSound;

    invoke-virtual {v0, p1}, Landroid/media/MediaActionSound;->play(I)V

    return-void
.end method

.method public readyToCapture()Z
    .locals 1

    const/4 v0, 0x1

    return v0
.end method

.method public release()V
    .locals 2

    iget-object v0, p0, Lcom/android/camera/actor/PhotoActor;->mHandler:Landroid/os/Handler;

    const/16 v1, 0x64

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeMessages(I)V

    iget-object v0, p0, Lcom/android/camera/actor/PhotoActor;->mHandler:Landroid/os/Handler;

    const/16 v1, 0x67

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeMessages(I)V

    iget-object v0, p0, Lcom/android/camera/actor/PhotoActor;->mBurstSound:Landroid/media/SoundPool;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/camera/actor/PhotoActor;->mBurstSound:Landroid/media/SoundPool;

    iget v1, p0, Lcom/android/camera/actor/PhotoActor;->mSoundID:I

    invoke-virtual {v0, v1}, Landroid/media/SoundPool;->unload(I)Z

    :cond_0
    invoke-virtual {p0}, Lcom/android/camera/actor/PhotoActor;->resetPhotoActor()V

    iget-object v0, p0, Lcom/android/camera/actor/PhotoActor;->mCameraCategory:Lcom/android/camera/actor/PhotoActor$CameraCategory;

    invoke-virtual {v0}, Lcom/android/camera/actor/PhotoActor$CameraCategory;->onLeaveActor()V

    return-void
.end method

.method protected resetPhotoActor()V
    .locals 2

    const/4 v1, 0x0

    sput-boolean v1, Lcom/android/camera/actor/PhotoActor;->sIsAutoFocusCallback:Z

    iget-boolean v0, p0, Lcom/android/camera/actor/PhotoActor;->mSelftimerCounting:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/camera/actor/PhotoActor;->mCamera:Lcom/android/camera/Camera;

    invoke-virtual {v0}, Landroid/content/ContextWrapper;->getMainLooper()Landroid/os/Looper;

    move-result-object v0

    invoke-static {v0}, Lcom/android/camera/manager/SelfTimerManager;->getInstance(Landroid/os/Looper;)Lcom/android/camera/manager/SelfTimerManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/android/camera/manager/SelfTimerManager;->breakTimer()V

    iput-boolean v1, p0, Lcom/android/camera/actor/PhotoActor;->mSelftimerCounting:Z

    :cond_0
    iget-object v0, p0, Lcom/android/camera/actor/PhotoActor;->mCamera:Lcom/android/camera/Camera;

    invoke-virtual {v0}, Lcom/android/camera/Camera;->dismissInfo()V

    iget-object v0, p0, Lcom/android/camera/actor/PhotoActor;->mCamera:Lcom/android/camera/Camera;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/android/camera/Camera;->setSwipingEnabled(Z)V

    return-void
.end method

.method protected restartPreview(Z)V
    .locals 2
    .param p1    # Z

    const/4 v1, 0x1

    const/4 v0, 0x0

    sput-boolean v0, Lcom/android/camera/actor/PhotoActor;->sIsAutoFocusCallback:Z

    invoke-virtual {p0, p1}, Lcom/android/camera/actor/PhotoActor;->startPreview(Z)V

    iget-object v0, p0, Lcom/android/camera/actor/PhotoActor;->mCamera:Lcom/android/camera/Camera;

    invoke-virtual {v0, v1}, Lcom/android/camera/Camera;->setCameraState(I)V

    iget-object v0, p0, Lcom/android/camera/actor/PhotoActor;->mCamera:Lcom/android/camera/Camera;

    invoke-virtual {v0}, Lcom/android/camera/Camera;->restoreViewState()V

    iget-object v0, p0, Lcom/android/camera/actor/PhotoActor;->mCamera:Lcom/android/camera/Camera;

    invoke-virtual {v0, v1}, Lcom/android/camera/Camera;->setSwipingEnabled(Z)V

    invoke-virtual {p0}, Lcom/android/camera/actor/PhotoActor;->startFaceDetection()V

    return-void
.end method

.method public setFocusParameters()V
    .locals 4

    const/4 v1, 0x0

    sget-boolean v0, Lcom/android/camera/actor/PhotoActor;->LOG:Z

    if-eqz v0, :cond_0

    const-string v0, "PhotoActor"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "setFocusParameters sIsAutoFocusCallback ="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    sget-boolean v3, Lcom/android/camera/actor/PhotoActor;->sIsAutoFocusCallback:Z

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Lcom/android/camera/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    iget-object v2, p0, Lcom/android/camera/actor/PhotoActor;->mCamera:Lcom/android/camera/Camera;

    sget-boolean v0, Lcom/android/camera/actor/PhotoActor;->sIsAutoFocusCallback:Z

    if-nez v0, :cond_1

    const/4 v0, 0x1

    :goto_0
    invoke-virtual {v2, v0}, Lcom/android/camera/Camera;->applyParameterForFocus(Z)V

    sput-boolean v1, Lcom/android/camera/actor/PhotoActor;->sIsAutoFocusCallback:Z

    return-void

    :cond_1
    move v0, v1

    goto :goto_0
.end method

.method public startFaceDetection()V
    .locals 3

    sget-boolean v0, Lcom/android/camera/actor/PhotoActor;->LOG:Z

    if-eqz v0, :cond_0

    const-string v0, "PhotoActor"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "startFaceDetection sFaceDetectionStarted="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    sget-boolean v2, Lcom/android/camera/actor/PhotoActor;->sFaceDetectionStarted:Z

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/android/camera/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    sget-object v1, Lcom/android/camera/actor/PhotoActor;->sFaceDetectionSync:Ljava/lang/Object;

    monitor-enter v1

    :try_start_0
    invoke-direct {p0}, Lcom/android/camera/actor/PhotoActor;->isSupportFaceDetect()Z

    move-result v0

    if-eqz v0, :cond_1

    sget-boolean v0, Lcom/android/camera/actor/PhotoActor;->sFaceDetectionStarted:Z

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/android/camera/actor/PhotoActor;->mCameraCategory:Lcom/android/camera/actor/PhotoActor$CameraCategory;

    iget-object v2, p0, Lcom/android/camera/actor/PhotoActor;->mCamera:Lcom/android/camera/Camera;

    invoke-virtual {v0, v2}, Lcom/android/camera/actor/PhotoActor$CameraCategory;->enableFD(Lcom/android/camera/Camera;)Z

    move-result v0

    if-nez v0, :cond_2

    :cond_1
    monitor-exit v1

    :goto_0
    return-void

    :cond_2
    iget-object v0, p0, Lcom/android/camera/actor/PhotoActor;->mCamera:Lcom/android/camera/Camera;

    invoke-virtual {v0}, Lcom/android/camera/Camera;->getCameraDevice()Lcom/android/camera/CameraManager$CameraProxy;

    move-result-object v0

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/android/camera/actor/PhotoActor;->mCamera:Lcom/android/camera/Camera;

    invoke-virtual {v0}, Lcom/android/camera/Camera;->getParameters()Landroid/hardware/Camera$Parameters;

    move-result-object v0

    invoke-virtual {v0}, Landroid/hardware/Camera$Parameters;->getMaxNumDetectedFaces()I

    move-result v0

    if-lez v0, :cond_3

    const/4 v0, 0x1

    sput-boolean v0, Lcom/android/camera/actor/PhotoActor;->sFaceDetectionStarted:Z

    invoke-virtual {p0}, Lcom/android/camera/actor/PhotoActor;->initializeFaceView()V

    iget-object v0, p0, Lcom/android/camera/actor/PhotoActor;->mCamera:Lcom/android/camera/Camera;

    invoke-virtual {v0}, Lcom/android/camera/Camera;->getCameraDevice()Lcom/android/camera/CameraManager$CameraProxy;

    move-result-object v0

    invoke-virtual {v0}, Lcom/android/camera/CameraManager$CameraProxy;->startFaceDetection()V

    :cond_3
    monitor-exit v1

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public startPreview(Z)V
    .locals 3
    .param p1    # Z

    const/4 v2, 0x0

    sget-boolean v0, Lcom/android/camera/actor/PhotoActor;->LOG:Z

    if-eqz v0, :cond_0

    const-string v0, "PhotoActor"

    const-string v1, "PhotoActor.startPreview"

    invoke-static {v0, v1}, Lcom/android/camera/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    iget-object v0, p0, Lcom/android/camera/actor/PhotoActor;->mCamera:Lcom/android/camera/Camera;

    new-instance v1, Lcom/android/camera/actor/PhotoActor$14;

    invoke-direct {v1, p0}, Lcom/android/camera/actor/PhotoActor$14;-><init>(Lcom/android/camera/actor/PhotoActor;)V

    invoke-virtual {v0, v1}, Landroid/app/Activity;->runOnUiThread(Ljava/lang/Runnable;)V

    if-eqz p1, :cond_1

    invoke-virtual {p0}, Lcom/android/camera/actor/PhotoActor;->stopPreview()V

    :cond_1
    iget-boolean v0, p0, Lcom/android/camera/actor/PhotoActor;->mSnapshotOnIdle:Z

    if-nez v0, :cond_3

    const-string v0, "continuous-picture"

    iget-object v1, p0, Lcom/android/camera/actor/PhotoActor;->mCamera:Lcom/android/camera/Camera;

    invoke-virtual {v1}, Lcom/android/camera/Camera;->getFocusManager()Lcom/android/camera/FocusManager;

    move-result-object v1

    invoke-virtual {v1}, Lcom/android/camera/FocusManager;->getFocusMode()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/android/camera/actor/PhotoActor;->mCamera:Lcom/android/camera/Camera;

    invoke-virtual {v0}, Lcom/android/camera/Camera;->getCameraDevice()Lcom/android/camera/CameraManager$CameraProxy;

    move-result-object v0

    invoke-virtual {v0}, Lcom/android/camera/CameraManager$CameraProxy;->cancelAutoFocus()V

    :cond_2
    iget-object v0, p0, Lcom/android/camera/actor/PhotoActor;->mCamera:Lcom/android/camera/Camera;

    invoke-virtual {v0}, Lcom/android/camera/Camera;->getFocusManager()Lcom/android/camera/FocusManager;

    move-result-object v0

    invoke-virtual {v0, v2}, Lcom/android/camera/FocusManager;->setAeLock(Z)V

    iget-object v0, p0, Lcom/android/camera/actor/PhotoActor;->mCamera:Lcom/android/camera/Camera;

    invoke-virtual {v0}, Lcom/android/camera/Camera;->getFocusManager()Lcom/android/camera/FocusManager;

    move-result-object v0

    invoke-virtual {v0, v2}, Lcom/android/camera/FocusManager;->setAwbLock(Z)V

    :cond_3
    invoke-virtual {p0}, Lcom/android/camera/actor/PhotoActor;->setFocusParameters()V

    iget-object v0, p0, Lcom/android/camera/actor/PhotoActor;->mCamera:Lcom/android/camera/Camera;

    invoke-virtual {v0}, Lcom/android/camera/Camera;->getCameraDevice()Lcom/android/camera/CameraManager$CameraProxy;

    move-result-object v0

    invoke-virtual {v0}, Lcom/android/camera/CameraManager$CameraProxy;->startPreviewAsync()V

    iget-object v0, p0, Lcom/android/camera/actor/PhotoActor;->mCamera:Lcom/android/camera/Camera;

    invoke-virtual {v0}, Lcom/android/camera/Camera;->getFocusManager()Lcom/android/camera/FocusManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/android/camera/FocusManager;->onPreviewStarted()V

    iget-boolean v0, p0, Lcom/android/camera/actor/PhotoActor;->mSnapshotOnIdle:Z

    if-eqz v0, :cond_4

    iget-object v0, p0, Lcom/android/camera/actor/PhotoActor;->mHandler:Landroid/os/Handler;

    iget-object v1, p0, Lcom/android/camera/actor/PhotoActor;->mDoSnapRunnable:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    :cond_4
    return-void
.end method

.method public stopFaceDetection()V
    .locals 3

    sget-boolean v0, Lcom/android/camera/actor/PhotoActor;->LOG:Z

    if-eqz v0, :cond_0

    const-string v0, "PhotoActor"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "stopFaceDetection sFaceDetectionStarted="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    sget-boolean v2, Lcom/android/camera/actor/PhotoActor;->sFaceDetectionStarted:Z

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/android/camera/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    sget-object v1, Lcom/android/camera/actor/PhotoActor;->sFaceDetectionSync:Ljava/lang/Object;

    monitor-enter v1

    :try_start_0
    sget-boolean v0, Lcom/android/camera/actor/PhotoActor;->sFaceDetectionStarted:Z

    if-nez v0, :cond_1

    monitor-exit v1

    :goto_0
    return-void

    :cond_1
    iget-object v0, p0, Lcom/android/camera/actor/PhotoActor;->mCamera:Lcom/android/camera/Camera;

    invoke-virtual {v0}, Lcom/android/camera/Camera;->getCameraDevice()Lcom/android/camera/CameraManager$CameraProxy;

    move-result-object v0

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/android/camera/actor/PhotoActor;->mCamera:Lcom/android/camera/Camera;

    invoke-virtual {v0}, Lcom/android/camera/Camera;->getParameters()Landroid/hardware/Camera$Parameters;

    move-result-object v0

    invoke-virtual {v0}, Landroid/hardware/Camera$Parameters;->getMaxNumDetectedFaces()I

    move-result v0

    if-lez v0, :cond_2

    const/4 v0, 0x0

    sput-boolean v0, Lcom/android/camera/actor/PhotoActor;->sFaceDetectionStarted:Z

    iget-object v0, p0, Lcom/android/camera/actor/PhotoActor;->mCamera:Lcom/android/camera/Camera;

    invoke-virtual {v0}, Lcom/android/camera/Camera;->getCameraDevice()Lcom/android/camera/CameraManager$CameraProxy;

    move-result-object v0

    invoke-virtual {v0}, Lcom/android/camera/CameraManager$CameraProxy;->stopFaceDetection()V

    iget-object v0, p0, Lcom/android/camera/actor/PhotoActor;->mCamera:Lcom/android/camera/Camera;

    invoke-virtual {v0}, Lcom/android/camera/Camera;->getFaceView()Lcom/android/camera/ui/FaceView;

    move-result-object v0

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/android/camera/actor/PhotoActor;->mCamera:Lcom/android/camera/Camera;

    invoke-virtual {v0}, Lcom/android/camera/Camera;->getFaceView()Lcom/android/camera/ui/FaceView;

    move-result-object v0

    invoke-virtual {v0}, Lcom/android/camera/ui/FaceView;->clear()V

    :cond_2
    monitor-exit v1

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public stopPreview()V
    .locals 4

    const/4 v3, 0x0

    sget-boolean v0, Lcom/android/camera/actor/PhotoActor;->LOG:Z

    if-eqz v0, :cond_0

    const-string v0, "PhotoActor"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "stopPreview() mCamera.getCameraState()="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/android/camera/actor/PhotoActor;->mCamera:Lcom/android/camera/Camera;

    invoke-virtual {v2}, Lcom/android/camera/Camera;->getCameraState()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/android/camera/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    iget-object v0, p0, Lcom/android/camera/actor/PhotoActor;->mCamera:Lcom/android/camera/Camera;

    invoke-virtual {v0}, Lcom/android/camera/Camera;->getCameraState()I

    move-result v0

    if-eqz v0, :cond_3

    iget-boolean v0, p0, Lcom/android/camera/actor/PhotoActor;->mZSDEnabled:Z

    if-eqz v0, :cond_1

    iget-boolean v0, p0, Lcom/android/camera/actor/PhotoActor;->mZSDEnabled:Z

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/android/camera/actor/PhotoActor;->mCamera:Lcom/android/camera/Camera;

    invoke-virtual {v0}, Lcom/android/camera/Camera;->getCameraState()I

    move-result v0

    const/4 v1, 0x3

    if-eq v0, v1, :cond_3

    :cond_1
    sget-object v1, Lcom/android/camera/actor/PhotoActor;->sFaceDetectionSync:Ljava/lang/Object;

    monitor-enter v1

    :try_start_0
    iget-object v0, p0, Lcom/android/camera/actor/PhotoActor;->mCamera:Lcom/android/camera/Camera;

    invoke-virtual {v0}, Lcom/android/camera/Camera;->getCameraDevice()Lcom/android/camera/CameraManager$CameraProxy;

    move-result-object v0

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/android/camera/actor/PhotoActor;->mCamera:Lcom/android/camera/Camera;

    invoke-virtual {v0}, Lcom/android/camera/Camera;->getCameraDevice()Lcom/android/camera/CameraManager$CameraProxy;

    move-result-object v0

    invoke-virtual {v0}, Lcom/android/camera/CameraManager$CameraProxy;->cancelAutoFocus()V

    iget-object v0, p0, Lcom/android/camera/actor/PhotoActor;->mCamera:Lcom/android/camera/Camera;

    invoke-virtual {v0}, Lcom/android/camera/Camera;->getCameraDevice()Lcom/android/camera/CameraManager$CameraProxy;

    move-result-object v0

    invoke-virtual {v0}, Lcom/android/camera/CameraManager$CameraProxy;->stopPreview()V

    :cond_2
    const/4 v0, 0x0

    sput-boolean v0, Lcom/android/camera/actor/PhotoActor;->sFaceDetectionStarted:Z

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    iget-object v0, p0, Lcom/android/camera/actor/PhotoActor;->mCamera:Lcom/android/camera/Camera;

    invoke-virtual {v0, v3}, Lcom/android/camera/Camera;->setCameraState(I)V

    iget-object v0, p0, Lcom/android/camera/actor/PhotoActor;->mCamera:Lcom/android/camera/Camera;

    invoke-virtual {v0}, Lcom/android/camera/Camera;->getFocusManager()Lcom/android/camera/FocusManager;

    move-result-object v0

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/android/camera/actor/PhotoActor;->mCamera:Lcom/android/camera/Camera;

    invoke-virtual {v0}, Lcom/android/camera/Camera;->getFocusManager()Lcom/android/camera/FocusManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/android/camera/FocusManager;->onPreviewStopped()V

    :cond_3
    return-void

    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method

.method protected updateSavingHint(ZZ)V
    .locals 6
    .param p1    # Z
    .param p2    # Z

    const/4 v4, 0x1

    sget-boolean v0, Lcom/android/camera/actor/PhotoActor;->LOG:Z

    if-eqz v0, :cond_0

    const-string v0, "PhotoActor"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "updateSavingHint, saving = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " shotDone = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/android/camera/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    if-eqz p1, :cond_3

    if-nez p2, :cond_2

    iget-object v0, p0, Lcom/android/camera/actor/PhotoActor;->mCamera:Lcom/android/camera/Camera;

    iget-object v1, p0, Lcom/android/camera/actor/PhotoActor;->mCamera:Lcom/android/camera/Camera;

    const v2, 0x7f0c00a7

    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/android/camera/Camera;->showProgress(Ljava/lang/String;)V

    :cond_1
    :goto_0
    return-void

    :cond_2
    iget v0, p0, Lcom/android/camera/actor/PhotoActor;->mCurrentShotsNum:I

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/android/camera/actor/PhotoActor;->mCamera:Lcom/android/camera/Camera;

    sget-object v1, Ljava/util/Locale;->ENGLISH:Ljava/util/Locale;

    iget-object v2, p0, Lcom/android/camera/actor/PhotoActor;->mCamera:Lcom/android/camera/Camera;

    const v3, 0x7f0c000f

    invoke-virtual {v2, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    new-array v3, v4, [Ljava/lang/Object;

    const/4 v4, 0x0

    iget v5, p0, Lcom/android/camera/actor/PhotoActor;->mCurrentShotsNum:I

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v3, v4

    invoke-static {v1, v2, v3}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/android/camera/Camera;->showProgress(Ljava/lang/String;)V

    goto :goto_0

    :cond_3
    iget-object v0, p0, Lcom/android/camera/actor/PhotoActor;->mCamera:Lcom/android/camera/Camera;

    invoke-virtual {v0}, Lcom/android/camera/Camera;->dismissProgress()V

    iget-object v0, p0, Lcom/android/camera/actor/PhotoActor;->mCamera:Lcom/android/camera/Camera;

    invoke-virtual {v0, v4}, Lcom/android/camera/Camera;->setSwipingEnabled(Z)V

    goto :goto_0
.end method
