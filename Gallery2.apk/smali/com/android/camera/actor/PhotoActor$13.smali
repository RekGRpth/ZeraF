.class Lcom/android/camera/actor/PhotoActor$13;
.super Ljava/lang/Object;
.source "PhotoActor.java"

# interfaces
.implements Landroid/hardware/Camera$PictureCallback;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/camera/actor/PhotoActor;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/camera/actor/PhotoActor;


# direct methods
.method constructor <init>(Lcom/android/camera/actor/PhotoActor;)V
    .locals 0

    iput-object p1, p0, Lcom/android/camera/actor/PhotoActor$13;->this$0:Lcom/android/camera/actor/PhotoActor;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onPictureTaken([BLandroid/hardware/Camera;)V
    .locals 10
    .param p1    # [B
    .param p2    # Landroid/hardware/Camera;

    invoke-static {}, Lcom/android/camera/actor/PhotoActor;->access$400()Z

    move-result v5

    if-eqz v5, :cond_0

    const-string v5, "PhotoActor"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "JpegPictureCallback onPictureTaken jpegData="

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, " mCameraClosed="

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    iget-object v7, p0, Lcom/android/camera/actor/PhotoActor$13;->this$0:Lcom/android/camera/actor/PhotoActor;

    iget-boolean v7, v7, Lcom/android/camera/actor/PhotoActor;->mCameraClosed:Z

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Lcom/android/camera/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    iget-object v5, p0, Lcom/android/camera/actor/PhotoActor$13;->this$0:Lcom/android/camera/actor/PhotoActor;

    const/4 v6, 0x0

    invoke-static {v5, v6}, Lcom/android/camera/actor/PhotoActor;->access$2002(Lcom/android/camera/actor/PhotoActor;Z)Z

    iget-object v5, p0, Lcom/android/camera/actor/PhotoActor$13;->this$0:Lcom/android/camera/actor/PhotoActor;

    iget-boolean v5, v5, Lcom/android/camera/actor/PhotoActor;->mCameraClosed:Z

    if-nez v5, :cond_1

    if-nez p1, :cond_3

    :cond_1
    iget-object v5, p0, Lcom/android/camera/actor/PhotoActor$13;->this$0:Lcom/android/camera/actor/PhotoActor;

    iget-object v5, v5, Lcom/android/camera/actor/PhotoActor;->mCamera:Lcom/android/camera/Camera;

    invoke-virtual {v5}, Lcom/android/camera/Camera;->restoreViewState()V

    iget-object v5, p0, Lcom/android/camera/actor/PhotoActor$13;->this$0:Lcom/android/camera/actor/PhotoActor;

    iget-object v5, v5, Lcom/android/camera/actor/PhotoActor;->mCamera:Lcom/android/camera/Camera;

    const/4 v6, 0x1

    invoke-virtual {v5, v6}, Lcom/android/camera/Camera;->setSwipingEnabled(Z)V

    iget-object v5, p0, Lcom/android/camera/actor/PhotoActor$13;->this$0:Lcom/android/camera/actor/PhotoActor;

    iget-boolean v5, v5, Lcom/android/camera/actor/PhotoActor;->mCameraClosed:Z

    if-nez v5, :cond_2

    iget-object v5, p0, Lcom/android/camera/actor/PhotoActor$13;->this$0:Lcom/android/camera/actor/PhotoActor;

    const/4 v6, 0x0

    invoke-virtual {v5, v6}, Lcom/android/camera/actor/PhotoActor;->restartPreview(Z)V

    :cond_2
    :goto_0
    return-void

    :cond_3
    iget-object v5, p0, Lcom/android/camera/actor/PhotoActor$13;->this$0:Lcom/android/camera/actor/PhotoActor;

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v6

    invoke-static {v5, v6, v7}, Lcom/android/camera/actor/PhotoActor;->access$2102(Lcom/android/camera/actor/PhotoActor;J)J

    iget-object v5, p0, Lcom/android/camera/actor/PhotoActor$13;->this$0:Lcom/android/camera/actor/PhotoActor;

    iget-wide v5, v5, Lcom/android/camera/actor/PhotoActor;->mPostViewPictureCallbackTime:J

    const-wide/16 v7, 0x0

    cmp-long v5, v5, v7

    if-eqz v5, :cond_7

    iget-object v5, p0, Lcom/android/camera/actor/PhotoActor$13;->this$0:Lcom/android/camera/actor/PhotoActor;

    iget-object v6, p0, Lcom/android/camera/actor/PhotoActor$13;->this$0:Lcom/android/camera/actor/PhotoActor;

    iget-wide v6, v6, Lcom/android/camera/actor/PhotoActor;->mPostViewPictureCallbackTime:J

    iget-object v8, p0, Lcom/android/camera/actor/PhotoActor$13;->this$0:Lcom/android/camera/actor/PhotoActor;

    invoke-static {v8}, Lcom/android/camera/actor/PhotoActor;->access$1300(Lcom/android/camera/actor/PhotoActor;)J

    move-result-wide v8

    sub-long/2addr v6, v8

    iput-wide v6, v5, Lcom/android/camera/actor/PhotoActor;->mShutterToPictureDisplayedTime:J

    iget-object v5, p0, Lcom/android/camera/actor/PhotoActor$13;->this$0:Lcom/android/camera/actor/PhotoActor;

    iget-object v6, p0, Lcom/android/camera/actor/PhotoActor$13;->this$0:Lcom/android/camera/actor/PhotoActor;

    invoke-static {v6}, Lcom/android/camera/actor/PhotoActor;->access$2100(Lcom/android/camera/actor/PhotoActor;)J

    move-result-wide v6

    iget-object v8, p0, Lcom/android/camera/actor/PhotoActor$13;->this$0:Lcom/android/camera/actor/PhotoActor;

    iget-wide v8, v8, Lcom/android/camera/actor/PhotoActor;->mPostViewPictureCallbackTime:J

    sub-long/2addr v6, v8

    iput-wide v6, v5, Lcom/android/camera/actor/PhotoActor;->mPictureDisplayedToJpegCallbackTime:J

    :goto_1
    invoke-static {}, Lcom/android/camera/actor/PhotoActor;->access$400()Z

    move-result v5

    if-eqz v5, :cond_4

    const-string v5, "PhotoActor"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "mPictureDisplayedToJpegCallbackTime = "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    iget-object v7, p0, Lcom/android/camera/actor/PhotoActor$13;->this$0:Lcom/android/camera/actor/PhotoActor;

    iget-wide v7, v7, Lcom/android/camera/actor/PhotoActor;->mPictureDisplayedToJpegCallbackTime:J

    invoke-virtual {v6, v7, v8}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, "ms"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Lcom/android/camera/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    :cond_4
    invoke-static {}, Lcom/android/camera/manager/MMProfileManager;->startProfileStorePicture()V

    iget-object v5, p0, Lcom/android/camera/actor/PhotoActor$13;->this$0:Lcom/android/camera/actor/PhotoActor;

    iget-object v5, v5, Lcom/android/camera/actor/PhotoActor;->mCamera:Lcom/android/camera/Camera;

    invoke-virtual {v5}, Lcom/android/camera/Camera;->getFocusManager()Lcom/android/camera/FocusManager;

    move-result-object v5

    invoke-virtual {v5}, Lcom/android/camera/FocusManager;->updateFocusUI()V

    iget-object v5, p0, Lcom/android/camera/actor/PhotoActor$13;->this$0:Lcom/android/camera/actor/PhotoActor;

    iget-object v5, v5, Lcom/android/camera/actor/PhotoActor;->mCamera:Lcom/android/camera/Camera;

    invoke-virtual {v5}, Lcom/android/camera/Camera;->isImageCaptureIntent()Z

    move-result v5

    if-nez v5, :cond_5

    invoke-static {}, Lcom/mediatek/camera/ext/ExtensionHelper;->getFeatureExtension()Lcom/mediatek/camera/ext/IFeatureExtension;

    move-result-object v4

    invoke-interface {v4}, Lcom/mediatek/camera/ext/IFeatureExtension;->isDelayRestartPreview()Z

    move-result v5

    if-eqz v5, :cond_9

    const-wide/16 v5, 0x4b0

    iget-object v7, p0, Lcom/android/camera/actor/PhotoActor$13;->this$0:Lcom/android/camera/actor/PhotoActor;

    iget-wide v7, v7, Lcom/android/camera/actor/PhotoActor;->mPictureDisplayedToJpegCallbackTime:J

    sub-long v0, v5, v7

    const-wide/16 v5, 0x0

    cmp-long v5, v0, v5

    if-gtz v5, :cond_8

    iget-object v5, p0, Lcom/android/camera/actor/PhotoActor$13;->this$0:Lcom/android/camera/actor/PhotoActor;

    const/4 v6, 0x1

    invoke-virtual {v5, v6}, Lcom/android/camera/actor/PhotoActor;->restartPreview(Z)V

    :cond_5
    :goto_2
    iget-object v5, p0, Lcom/android/camera/actor/PhotoActor$13;->this$0:Lcom/android/camera/actor/PhotoActor;

    const/4 v6, 0x0

    iput-boolean v6, v5, Lcom/android/camera/actor/PhotoActor;->mCapturing:Z

    iget-object v5, p0, Lcom/android/camera/actor/PhotoActor$13;->this$0:Lcom/android/camera/actor/PhotoActor;

    iget-object v5, v5, Lcom/android/camera/actor/PhotoActor;->mCamera:Lcom/android/camera/Camera;

    invoke-virtual {v5}, Lcom/android/camera/Camera;->isImageCaptureIntent()Z

    move-result v5

    if-nez v5, :cond_a

    iget-object v5, p0, Lcom/android/camera/actor/PhotoActor$13;->this$0:Lcom/android/camera/actor/PhotoActor;

    invoke-static {v5, p1}, Lcom/android/camera/actor/PhotoActor;->access$1600(Lcom/android/camera/actor/PhotoActor;[B)V

    iget-object v5, p0, Lcom/android/camera/actor/PhotoActor$13;->this$0:Lcom/android/camera/actor/PhotoActor;

    iget-object v5, v5, Lcom/android/camera/actor/PhotoActor;->mCameraCategory:Lcom/android/camera/actor/PhotoActor$CameraCategory;

    invoke-virtual {v5}, Lcom/android/camera/actor/PhotoActor$CameraCategory;->doOnPictureTaken()V

    :goto_3
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    iget-object v5, p0, Lcom/android/camera/actor/PhotoActor$13;->this$0:Lcom/android/camera/actor/PhotoActor;

    iget-object v6, p0, Lcom/android/camera/actor/PhotoActor$13;->this$0:Lcom/android/camera/actor/PhotoActor;

    invoke-static {v6}, Lcom/android/camera/actor/PhotoActor;->access$2100(Lcom/android/camera/actor/PhotoActor;)J

    move-result-wide v6

    sub-long v6, v2, v6

    iput-wide v6, v5, Lcom/android/camera/actor/PhotoActor;->mJpegCallbackFinishTime:J

    invoke-static {}, Lcom/android/camera/actor/PhotoActor;->access$400()Z

    move-result v5

    if-eqz v5, :cond_6

    const-string v5, "PhotoActor"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "mJpegCallbackFinishTime = "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    iget-object v7, p0, Lcom/android/camera/actor/PhotoActor$13;->this$0:Lcom/android/camera/actor/PhotoActor;

    iget-wide v7, v7, Lcom/android/camera/actor/PhotoActor;->mJpegCallbackFinishTime:J

    invoke-virtual {v6, v7, v8}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, "ms"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Lcom/android/camera/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    :cond_6
    invoke-static {}, Lcom/android/camera/manager/MMProfileManager;->stopProfileStorePicture()V

    iget-object v5, p0, Lcom/android/camera/actor/PhotoActor$13;->this$0:Lcom/android/camera/actor/PhotoActor;

    const-wide/16 v6, 0x0

    invoke-static {v5, v6, v7}, Lcom/android/camera/actor/PhotoActor;->access$2102(Lcom/android/camera/actor/PhotoActor;J)J

    goto/16 :goto_0

    :cond_7
    iget-object v5, p0, Lcom/android/camera/actor/PhotoActor$13;->this$0:Lcom/android/camera/actor/PhotoActor;

    iget-object v6, p0, Lcom/android/camera/actor/PhotoActor$13;->this$0:Lcom/android/camera/actor/PhotoActor;

    invoke-static {v6}, Lcom/android/camera/actor/PhotoActor;->access$1400(Lcom/android/camera/actor/PhotoActor;)J

    move-result-wide v6

    iget-object v8, p0, Lcom/android/camera/actor/PhotoActor$13;->this$0:Lcom/android/camera/actor/PhotoActor;

    invoke-static {v8}, Lcom/android/camera/actor/PhotoActor;->access$1300(Lcom/android/camera/actor/PhotoActor;)J

    move-result-wide v8

    sub-long/2addr v6, v8

    iput-wide v6, v5, Lcom/android/camera/actor/PhotoActor;->mShutterToPictureDisplayedTime:J

    iget-object v5, p0, Lcom/android/camera/actor/PhotoActor$13;->this$0:Lcom/android/camera/actor/PhotoActor;

    iget-object v6, p0, Lcom/android/camera/actor/PhotoActor$13;->this$0:Lcom/android/camera/actor/PhotoActor;

    invoke-static {v6}, Lcom/android/camera/actor/PhotoActor;->access$2100(Lcom/android/camera/actor/PhotoActor;)J

    move-result-wide v6

    iget-object v8, p0, Lcom/android/camera/actor/PhotoActor$13;->this$0:Lcom/android/camera/actor/PhotoActor;

    invoke-static {v8}, Lcom/android/camera/actor/PhotoActor;->access$1400(Lcom/android/camera/actor/PhotoActor;)J

    move-result-wide v8

    sub-long/2addr v6, v8

    iput-wide v6, v5, Lcom/android/camera/actor/PhotoActor;->mPictureDisplayedToJpegCallbackTime:J

    goto/16 :goto_1

    :cond_8
    iget-object v5, p0, Lcom/android/camera/actor/PhotoActor$13;->this$0:Lcom/android/camera/actor/PhotoActor;

    iget-object v5, v5, Lcom/android/camera/actor/PhotoActor;->mHandler:Landroid/os/Handler;

    new-instance v6, Lcom/android/camera/actor/PhotoActor$13$1;

    invoke-direct {v6, p0}, Lcom/android/camera/actor/PhotoActor$13$1;-><init>(Lcom/android/camera/actor/PhotoActor$13;)V

    invoke-virtual {v5, v6, v0, v1}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    goto/16 :goto_2

    :cond_9
    iget-object v5, p0, Lcom/android/camera/actor/PhotoActor$13;->this$0:Lcom/android/camera/actor/PhotoActor;

    const/4 v6, 0x1

    invoke-virtual {v5, v6}, Lcom/android/camera/actor/PhotoActor;->restartPreview(Z)V

    goto/16 :goto_2

    :cond_a
    iget-object v5, p0, Lcom/android/camera/actor/PhotoActor$13;->this$0:Lcom/android/camera/actor/PhotoActor;

    iput-object p1, v5, Lcom/android/camera/actor/PhotoActor;->mJpegImageData:[B

    iget-object v5, p0, Lcom/android/camera/actor/PhotoActor$13;->this$0:Lcom/android/camera/actor/PhotoActor;

    iget-object v5, v5, Lcom/android/camera/actor/PhotoActor;->mCamera:Lcom/android/camera/Camera;

    invoke-virtual {v5}, Lcom/android/camera/Camera;->isQuickCapture()Z

    move-result v5

    if-nez v5, :cond_b

    iget-object v5, p0, Lcom/android/camera/actor/PhotoActor$13;->this$0:Lcom/android/camera/actor/PhotoActor;

    iget-object v5, v5, Lcom/android/camera/actor/PhotoActor;->mCamera:Lcom/android/camera/Camera;

    invoke-virtual {v5}, Lcom/android/camera/Camera;->showReview()V

    iget-object v5, p0, Lcom/android/camera/actor/PhotoActor$13;->this$0:Lcom/android/camera/actor/PhotoActor;

    iget-object v5, v5, Lcom/android/camera/actor/PhotoActor;->mCamera:Lcom/android/camera/Camera;

    const/4 v6, 0x3

    invoke-virtual {v5, v6}, Lcom/android/camera/Camera;->switchShutter(I)V

    iget-object v5, p0, Lcom/android/camera/actor/PhotoActor$13;->this$0:Lcom/android/camera/actor/PhotoActor;

    iget-object v5, v5, Lcom/android/camera/actor/PhotoActor;->mCamera:Lcom/android/camera/Camera;

    const/16 v6, 0x8

    invoke-virtual {v5, v6}, Lcom/android/camera/Camera;->setViewState(I)V

    goto/16 :goto_3

    :cond_b
    iget-object v5, p0, Lcom/android/camera/actor/PhotoActor$13;->this$0:Lcom/android/camera/actor/PhotoActor;

    invoke-static {v5}, Lcom/android/camera/actor/PhotoActor;->access$500(Lcom/android/camera/actor/PhotoActor;)V

    goto/16 :goto_3
.end method
