.class Lcom/android/camera/actor/VideoActor$10;
.super Ljava/lang/Object;
.source "VideoActor.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/camera/actor/VideoActor;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/camera/actor/VideoActor;


# direct methods
.method constructor <init>(Lcom/android/camera/actor/VideoActor;)V
    .locals 0

    iput-object p1, p0, Lcom/android/camera/actor/VideoActor$10;->this$0:Lcom/android/camera/actor/VideoActor;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 7
    .param p1    # Landroid/view/View;

    const/4 v4, 0x1

    const/4 v6, 0x0

    invoke-static {}, Lcom/android/camera/actor/VideoActor;->access$300()Z

    move-result v1

    if-eqz v1, :cond_0

    const-string v1, "VideoActor"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "mVideoPauseResumeListner.onClick() mMediaRecoderRecordingPaused="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/android/camera/actor/VideoActor$10;->this$0:Lcom/android/camera/actor/VideoActor;

    invoke-static {v3}, Lcom/android/camera/actor/VideoActor;->access$4200(Lcom/android/camera/actor/VideoActor;)Z

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ",mRecorderBusy = "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/android/camera/actor/VideoActor$10;->this$0:Lcom/android/camera/actor/VideoActor;

    invoke-static {v3}, Lcom/android/camera/actor/VideoActor;->access$2200(Lcom/android/camera/actor/VideoActor;)Z

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ",mMediaRecorderRecording = "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/android/camera/actor/VideoActor$10;->this$0:Lcom/android/camera/actor/VideoActor;

    invoke-static {v3}, Lcom/android/camera/actor/VideoActor;->access$500(Lcom/android/camera/actor/VideoActor;)Z

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/android/camera/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    iget-object v1, p0, Lcom/android/camera/actor/VideoActor$10;->this$0:Lcom/android/camera/actor/VideoActor;

    invoke-static {v1}, Lcom/android/camera/actor/VideoActor;->access$2200(Lcom/android/camera/actor/VideoActor;)Z

    move-result v1

    if-nez v1, :cond_1

    iget-object v1, p0, Lcom/android/camera/actor/VideoActor$10;->this$0:Lcom/android/camera/actor/VideoActor;

    invoke-static {v1}, Lcom/android/camera/actor/VideoActor;->access$500(Lcom/android/camera/actor/VideoActor;)Z

    move-result v1

    if-nez v1, :cond_2

    :cond_1
    :goto_0
    return-void

    :cond_2
    iget-object v1, p0, Lcom/android/camera/actor/VideoActor$10;->this$0:Lcom/android/camera/actor/VideoActor;

    invoke-static {v1, v4}, Lcom/android/camera/actor/VideoActor;->access$2202(Lcom/android/camera/actor/VideoActor;Z)Z

    iget-object v1, p0, Lcom/android/camera/actor/VideoActor$10;->this$0:Lcom/android/camera/actor/VideoActor;

    invoke-static {v1}, Lcom/android/camera/actor/VideoActor;->access$4200(Lcom/android/camera/actor/VideoActor;)Z

    move-result v1

    if-eqz v1, :cond_3

    iget-object v1, p0, Lcom/android/camera/actor/VideoActor$10;->this$0:Lcom/android/camera/actor/VideoActor;

    invoke-static {v1}, Lcom/android/camera/actor/VideoActor;->access$4300(Lcom/android/camera/actor/VideoActor;)Lcom/android/camera/manager/RecordingView;

    move-result-object v1

    invoke-virtual {v1, v4}, Lcom/android/camera/manager/RecordingView;->setRecordingIndicator(Z)V

    :try_start_0
    iget-object v1, p0, Lcom/android/camera/actor/VideoActor$10;->this$0:Lcom/android/camera/actor/VideoActor;

    invoke-static {v1}, Lcom/android/camera/actor/VideoActor;->access$2400(Lcom/android/camera/actor/VideoActor;)Landroid/media/MediaRecorder;

    move-result-object v1

    invoke-virtual {v1}, Landroid/media/MediaRecorder;->start()V

    iget-object v1, p0, Lcom/android/camera/actor/VideoActor$10;->this$0:Lcom/android/camera/actor/VideoActor;

    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v2

    iget-object v4, p0, Lcom/android/camera/actor/VideoActor$10;->this$0:Lcom/android/camera/actor/VideoActor;

    invoke-static {v4}, Lcom/android/camera/actor/VideoActor;->access$4500(Lcom/android/camera/actor/VideoActor;)J

    move-result-wide v4

    sub-long/2addr v2, v4

    invoke-static {v1, v2, v3}, Lcom/android/camera/actor/VideoActor;->access$4402(Lcom/android/camera/actor/VideoActor;J)J

    iget-object v1, p0, Lcom/android/camera/actor/VideoActor$10;->this$0:Lcom/android/camera/actor/VideoActor;

    const-wide/16 v2, 0x0

    invoke-static {v1, v2, v3}, Lcom/android/camera/actor/VideoActor;->access$4502(Lcom/android/camera/actor/VideoActor;J)J

    iget-object v1, p0, Lcom/android/camera/actor/VideoActor$10;->this$0:Lcom/android/camera/actor/VideoActor;

    const/4 v2, 0x0

    invoke-static {v1, v2}, Lcom/android/camera/actor/VideoActor;->access$4202(Lcom/android/camera/actor/VideoActor;Z)Z
    :try_end_0
    .catch Ljava/lang/IllegalStateException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_1
    iget-object v1, p0, Lcom/android/camera/actor/VideoActor$10;->this$0:Lcom/android/camera/actor/VideoActor;

    invoke-static {v1, v6}, Lcom/android/camera/actor/VideoActor;->access$2202(Lcom/android/camera/actor/VideoActor;Z)Z

    invoke-static {}, Lcom/android/camera/actor/VideoActor;->access$300()Z

    move-result v1

    if-eqz v1, :cond_1

    const-string v1, "VideoActor"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "mVideoPauseResumeListner.onClick() end. mRecorderBusy="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/android/camera/actor/VideoActor$10;->this$0:Lcom/android/camera/actor/VideoActor;

    invoke-static {v3}, Lcom/android/camera/actor/VideoActor;->access$2200(Lcom/android/camera/actor/VideoActor;)Z

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/android/camera/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    :catch_0
    move-exception v0

    const-string v1, "VideoActor"

    const-string v2, "Could not start media recorder. "

    invoke-static {v1, v2, v0}, Lcom/android/camera/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    iget-object v1, p0, Lcom/android/camera/actor/VideoActor$10;->this$0:Lcom/android/camera/actor/VideoActor;

    invoke-static {v1}, Lcom/android/camera/actor/VideoActor;->access$400(Lcom/android/camera/actor/VideoActor;)Lcom/android/camera/Camera;

    move-result-object v1

    const v2, 0x7f0c00a9

    invoke-virtual {v1, v2}, Lcom/android/camera/Camera;->showToast(I)V

    iget-object v1, p0, Lcom/android/camera/actor/VideoActor$10;->this$0:Lcom/android/camera/actor/VideoActor;

    invoke-static {v1}, Lcom/android/camera/actor/VideoActor;->access$2800(Lcom/android/camera/actor/VideoActor;)V

    goto :goto_1

    :cond_3
    iget-object v1, p0, Lcom/android/camera/actor/VideoActor$10;->this$0:Lcom/android/camera/actor/VideoActor;

    invoke-static {v1}, Lcom/android/camera/actor/VideoActor;->access$4600(Lcom/android/camera/actor/VideoActor;)V

    goto :goto_1
.end method
