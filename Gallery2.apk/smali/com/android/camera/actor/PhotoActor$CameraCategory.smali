.class public Lcom/android/camera/actor/PhotoActor$CameraCategory;
.super Ljava/lang/Object;
.source "PhotoActor.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/camera/actor/PhotoActor;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4
    name = "CameraCategory"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/camera/actor/PhotoActor;


# direct methods
.method protected constructor <init>(Lcom/android/camera/actor/PhotoActor;)V
    .locals 0

    iput-object p1, p0, Lcom/android/camera/actor/PhotoActor$CameraCategory;->this$0:Lcom/android/camera/actor/PhotoActor;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public animateCapture(Lcom/android/camera/Camera;)V
    .locals 1
    .param p1    # Lcom/android/camera/Camera;

    iget-object v0, p0, Lcom/android/camera/actor/PhotoActor$CameraCategory;->this$0:Lcom/android/camera/actor/PhotoActor;

    invoke-static {v0}, Lcom/android/camera/actor/PhotoActor;->access$800(Lcom/android/camera/actor/PhotoActor;)Z

    move-result v0

    if-nez v0, :cond_0

    invoke-virtual {p1}, Lcom/android/camera/Camera;->isImageCaptureIntent()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-virtual {p1}, Lcom/android/camera/Camera;->animateCapture()V

    :cond_0
    return-void
.end method

.method public applySpecialCapture()Z
    .locals 3

    iget-object v0, p0, Lcom/android/camera/actor/PhotoActor$CameraCategory;->this$0:Lcom/android/camera/actor/PhotoActor;

    invoke-static {v0}, Lcom/android/camera/actor/PhotoActor;->access$800(Lcom/android/camera/actor/PhotoActor;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/android/camera/actor/PhotoActor$CameraCategory;->this$0:Lcom/android/camera/actor/PhotoActor;

    invoke-static {v0}, Lcom/android/camera/actor/PhotoActor;->access$700(Lcom/android/camera/actor/PhotoActor;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/camera/actor/PhotoActor$CameraCategory;->this$0:Lcom/android/camera/actor/PhotoActor;

    iget-object v0, v0, Lcom/android/camera/actor/PhotoActor;->mCamera:Lcom/android/camera/Camera;

    invoke-virtual {v0}, Lcom/android/camera/Camera;->isImageCaptureIntent()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/android/camera/actor/PhotoActor$CameraCategory;->this$0:Lcom/android/camera/actor/PhotoActor;

    new-instance v1, Lcom/android/camera/actor/PhotoActor$RenderInCapture;

    iget-object v2, p0, Lcom/android/camera/actor/PhotoActor$CameraCategory;->this$0:Lcom/android/camera/actor/PhotoActor;

    invoke-direct {v1, v2}, Lcom/android/camera/actor/PhotoActor$RenderInCapture;-><init>(Lcom/android/camera/actor/PhotoActor;)V

    invoke-static {v0, v1}, Lcom/android/camera/actor/PhotoActor;->access$2702(Lcom/android/camera/actor/PhotoActor;Lcom/android/camera/actor/PhotoActor$RenderInCapture;)Lcom/android/camera/actor/PhotoActor$RenderInCapture;

    iget-object v0, p0, Lcom/android/camera/actor/PhotoActor$CameraCategory;->this$0:Lcom/android/camera/actor/PhotoActor;

    invoke-static {v0}, Lcom/android/camera/actor/PhotoActor;->access$2700(Lcom/android/camera/actor/PhotoActor;)Lcom/android/camera/actor/PhotoActor$RenderInCapture;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Thread;->start()V

    iget-object v0, p0, Lcom/android/camera/actor/PhotoActor$CameraCategory;->this$0:Lcom/android/camera/actor/PhotoActor;

    iget-object v0, v0, Lcom/android/camera/actor/PhotoActor;->mCamera:Lcom/android/camera/Camera;

    invoke-virtual {v0}, Lcom/android/camera/Camera;->getCameraDevice()Lcom/android/camera/CameraManager$CameraProxy;

    move-result-object v0

    iget-object v1, p0, Lcom/android/camera/actor/PhotoActor$CameraCategory;->this$0:Lcom/android/camera/actor/PhotoActor;

    invoke-static {v1}, Lcom/android/camera/actor/PhotoActor;->access$2800(Lcom/android/camera/actor/PhotoActor;)Landroid/hardware/Camera$ZSDPreviewDone;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/android/camera/CameraManager$CameraProxy;->setPreviewDoneCallback(Landroid/hardware/Camera$ZSDPreviewDone;)V

    :goto_0
    const/4 v0, 0x0

    return v0

    :cond_0
    iget-object v0, p0, Lcom/android/camera/actor/PhotoActor$CameraCategory;->this$0:Lcom/android/camera/actor/PhotoActor;

    iget-object v0, v0, Lcom/android/camera/actor/PhotoActor;->mCamera:Lcom/android/camera/Camera;

    invoke-virtual {v0}, Lcom/android/camera/Camera;->getCameraDevice()Lcom/android/camera/CameraManager$CameraProxy;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/android/camera/CameraManager$CameraProxy;->setPreviewDoneCallback(Landroid/hardware/Camera$ZSDPreviewDone;)V

    goto :goto_0
.end method

.method public canshot()Z
    .locals 4

    const-wide/16 v0, 0x1

    invoke-static {}, Lcom/android/camera/Storage;->getLeftSpace()J

    move-result-wide v2

    cmp-long v0, v0, v2

    if-gtz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public doCancelCapture()Z
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public doOnPictureTaken()V
    .locals 1

    iget-object v0, p0, Lcom/android/camera/actor/PhotoActor$CameraCategory;->this$0:Lcom/android/camera/actor/PhotoActor;

    invoke-virtual {v0}, Lcom/android/camera/actor/PhotoActor;->interruptRenderThread()V

    return-void
.end method

.method public doShutter()V
    .locals 0

    return-void
.end method

.method public enableFD(Lcom/android/camera/Camera;)Z
    .locals 3
    .param p1    # Lcom/android/camera/Camera;

    invoke-static {}, Lcom/android/camera/CameraHolder;->instance()Lcom/android/camera/CameraHolder;

    move-result-object v1

    invoke-virtual {v1}, Lcom/android/camera/CameraHolder;->getCameraInfo()[Landroid/hardware/Camera$CameraInfo;

    move-result-object v1

    invoke-virtual {p1}, Lcom/android/camera/Camera;->getCameraId()I

    move-result v2

    aget-object v0, v1, v2

    const/4 v1, 0x1

    return v1
.end method

.method public ensureCaptureTempPath()V
    .locals 0

    return-void
.end method

.method public getJpegPictureCallback()Landroid/hardware/Camera$PictureCallback;
    .locals 1

    iget-object v0, p0, Lcom/android/camera/actor/PhotoActor$CameraCategory;->this$0:Lcom/android/camera/actor/PhotoActor;

    invoke-static {v0}, Lcom/android/camera/actor/PhotoActor;->access$800(Lcom/android/camera/actor/PhotoActor;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/camera/actor/PhotoActor$CameraCategory;->this$0:Lcom/android/camera/actor/PhotoActor;

    invoke-static {v0}, Lcom/android/camera/actor/PhotoActor;->access$2900(Lcom/android/camera/actor/PhotoActor;)Landroid/hardware/Camera$PictureCallback;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/android/camera/actor/PhotoActor$CameraCategory;->this$0:Lcom/android/camera/actor/PhotoActor;

    invoke-static {v0}, Lcom/android/camera/actor/PhotoActor;->access$3000(Lcom/android/camera/actor/PhotoActor;)Landroid/hardware/Camera$PictureCallback;

    move-result-object v0

    goto :goto_0
.end method

.method public initializeFirstTime()V
    .locals 5

    iget-object v0, p0, Lcom/android/camera/actor/PhotoActor$CameraCategory;->this$0:Lcom/android/camera/actor/PhotoActor;

    new-instance v1, Landroid/media/SoundPool;

    const/16 v2, 0xa

    const/4 v3, 0x7

    const/4 v4, 0x0

    invoke-direct {v1, v2, v3, v4}, Landroid/media/SoundPool;-><init>(III)V

    invoke-static {v0, v1}, Lcom/android/camera/actor/PhotoActor;->access$1202(Lcom/android/camera/actor/PhotoActor;Landroid/media/SoundPool;)Landroid/media/SoundPool;

    iget-object v0, p0, Lcom/android/camera/actor/PhotoActor$CameraCategory;->this$0:Lcom/android/camera/actor/PhotoActor;

    iget-object v1, p0, Lcom/android/camera/actor/PhotoActor$CameraCategory;->this$0:Lcom/android/camera/actor/PhotoActor;

    invoke-static {v1}, Lcom/android/camera/actor/PhotoActor;->access$1200(Lcom/android/camera/actor/PhotoActor;)Landroid/media/SoundPool;

    move-result-object v1

    const-string v2, "/system/media/audio/ui/camera_shutter.ogg"

    const/4 v3, 0x1

    invoke-virtual {v1, v2, v3}, Landroid/media/SoundPool;->load(Ljava/lang/String;I)I

    move-result v1

    invoke-static {v0, v1}, Lcom/android/camera/actor/PhotoActor;->access$1102(Lcom/android/camera/actor/PhotoActor;I)I

    return-void
.end method

.method public onLeaveActor()V
    .locals 5

    const/4 v4, 0x1

    invoke-static {}, Lcom/android/camera/actor/PhotoActor;->access$400()Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v0, "PhotoActor"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "onLeaveActor mContinuousShotPerformed="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/android/camera/actor/PhotoActor$CameraCategory;->this$0:Lcom/android/camera/actor/PhotoActor;

    invoke-static {v2}, Lcom/android/camera/actor/PhotoActor;->access$800(Lcom/android/camera/actor/PhotoActor;)Z

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " mSavingPictures="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/android/camera/actor/PhotoActor$CameraCategory;->this$0:Lcom/android/camera/actor/PhotoActor;

    invoke-static {v2}, Lcom/android/camera/actor/PhotoActor;->access$3100(Lcom/android/camera/actor/PhotoActor;)Z

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/android/camera/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    iget-object v0, p0, Lcom/android/camera/actor/PhotoActor$CameraCategory;->this$0:Lcom/android/camera/actor/PhotoActor;

    invoke-virtual {v0}, Lcom/android/camera/actor/PhotoActor;->interruptRenderThread()V

    iget-object v0, p0, Lcom/android/camera/actor/PhotoActor$CameraCategory;->this$0:Lcom/android/camera/actor/PhotoActor;

    invoke-static {v0}, Lcom/android/camera/actor/PhotoActor;->access$800(Lcom/android/camera/actor/PhotoActor;)Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/android/camera/actor/PhotoActor$CameraCategory;->this$0:Lcom/android/camera/actor/PhotoActor;

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lcom/android/camera/actor/PhotoActor;->access$802(Lcom/android/camera/actor/PhotoActor;Z)Z

    iget-object v0, p0, Lcom/android/camera/actor/PhotoActor$CameraCategory;->this$0:Lcom/android/camera/actor/PhotoActor;

    invoke-static {v0, v4}, Lcom/android/camera/actor/PhotoActor;->access$3102(Lcom/android/camera/actor/PhotoActor;Z)Z

    iget-object v0, p0, Lcom/android/camera/actor/PhotoActor$CameraCategory;->this$0:Lcom/android/camera/actor/PhotoActor;

    invoke-static {v0}, Lcom/android/camera/actor/PhotoActor;->access$3200(Lcom/android/camera/actor/PhotoActor;)V

    :cond_1
    iget-object v0, p0, Lcom/android/camera/actor/PhotoActor$CameraCategory;->this$0:Lcom/android/camera/actor/PhotoActor;

    invoke-static {v0}, Lcom/android/camera/actor/PhotoActor;->access$3100(Lcom/android/camera/actor/PhotoActor;)Z

    move-result v0

    if-eqz v0, :cond_5

    iget-object v0, p0, Lcom/android/camera/actor/PhotoActor$CameraCategory;->this$0:Lcom/android/camera/actor/PhotoActor;

    invoke-static {v0}, Lcom/android/camera/actor/PhotoActor;->access$3300(Lcom/android/camera/actor/PhotoActor;)Ljava/lang/Thread;

    move-result-object v0

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/android/camera/actor/PhotoActor$CameraCategory;->this$0:Lcom/android/camera/actor/PhotoActor;

    invoke-static {v0}, Lcom/android/camera/actor/PhotoActor;->access$3300(Lcom/android/camera/actor/PhotoActor;)Ljava/lang/Thread;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Thread;->isAlive()Z

    move-result v0

    if-nez v0, :cond_3

    :cond_2
    iget-object v0, p0, Lcom/android/camera/actor/PhotoActor$CameraCategory;->this$0:Lcom/android/camera/actor/PhotoActor;

    new-instance v1, Lcom/android/camera/actor/PhotoActor$WaitSavingDoneThread;

    iget-object v2, p0, Lcom/android/camera/actor/PhotoActor$CameraCategory;->this$0:Lcom/android/camera/actor/PhotoActor;

    const/4 v3, 0x0

    invoke-direct {v1, v2, v3}, Lcom/android/camera/actor/PhotoActor$WaitSavingDoneThread;-><init>(Lcom/android/camera/actor/PhotoActor;Lcom/android/camera/actor/PhotoActor$1;)V

    invoke-static {v0, v1}, Lcom/android/camera/actor/PhotoActor;->access$3302(Lcom/android/camera/actor/PhotoActor;Ljava/lang/Thread;)Ljava/lang/Thread;

    iget-object v0, p0, Lcom/android/camera/actor/PhotoActor$CameraCategory;->this$0:Lcom/android/camera/actor/PhotoActor;

    invoke-static {v0}, Lcom/android/camera/actor/PhotoActor;->access$3300(Lcom/android/camera/actor/PhotoActor;)Ljava/lang/Thread;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Thread;->start()V

    iget-object v0, p0, Lcom/android/camera/actor/PhotoActor$CameraCategory;->this$0:Lcom/android/camera/actor/PhotoActor;

    invoke-virtual {v0, v4, v4}, Lcom/android/camera/actor/PhotoActor;->updateSavingHint(ZZ)V

    :cond_3
    :goto_0
    iget-object v0, p0, Lcom/android/camera/actor/PhotoActor$CameraCategory;->this$0:Lcom/android/camera/actor/PhotoActor;

    iget-object v0, v0, Lcom/android/camera/actor/PhotoActor;->mCamera:Lcom/android/camera/Camera;

    invoke-virtual {v0}, Lcom/android/camera/Camera;->isImageCaptureIntent()Z

    move-result v0

    if-eqz v0, :cond_4

    iget-object v0, p0, Lcom/android/camera/actor/PhotoActor$CameraCategory;->this$0:Lcom/android/camera/actor/PhotoActor;

    iget-object v0, v0, Lcom/android/camera/actor/PhotoActor;->mCamera:Lcom/android/camera/Camera;

    invoke-virtual {v0}, Lcom/android/camera/Camera;->hideReview()V

    :cond_4
    return-void

    :cond_5
    iget-object v0, p0, Lcom/android/camera/actor/PhotoActor$CameraCategory;->this$0:Lcom/android/camera/actor/PhotoActor;

    iget-object v0, v0, Lcom/android/camera/actor/PhotoActor;->mCamera:Lcom/android/camera/Camera;

    invoke-virtual {v0}, Lcom/android/camera/Camera;->restoreViewState()V

    goto :goto_0
.end method

.method public shutterPressed()V
    .locals 1

    iget-object v0, p0, Lcom/android/camera/actor/PhotoActor$CameraCategory;->this$0:Lcom/android/camera/actor/PhotoActor;

    iget-object v0, v0, Lcom/android/camera/actor/PhotoActor;->mCamera:Lcom/android/camera/Camera;

    invoke-virtual {v0}, Lcom/android/camera/Camera;->getFocusManager()Lcom/android/camera/FocusManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/android/camera/FocusManager;->onShutterDown()V

    return-void
.end method

.method public shutterUp()V
    .locals 1

    iget-object v0, p0, Lcom/android/camera/actor/PhotoActor$CameraCategory;->this$0:Lcom/android/camera/actor/PhotoActor;

    iget-object v0, v0, Lcom/android/camera/actor/PhotoActor;->mCamera:Lcom/android/camera/Camera;

    invoke-virtual {v0}, Lcom/android/camera/Camera;->getFocusManager()Lcom/android/camera/FocusManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/android/camera/FocusManager;->onShutterUp()V

    return-void
.end method

.method public skipFocus()Z
    .locals 1

    const/4 v0, 0x1

    return v0
.end method

.method public supportContinuousShot()Z
    .locals 1

    iget-object v0, p0, Lcom/android/camera/actor/PhotoActor$CameraCategory;->this$0:Lcom/android/camera/actor/PhotoActor;

    invoke-static {v0}, Lcom/android/camera/actor/PhotoActor;->access$2600(Lcom/android/camera/actor/PhotoActor;)Z

    move-result v0

    return v0
.end method

.method public switchShutterButton()V
    .locals 2

    iget-object v0, p0, Lcom/android/camera/actor/PhotoActor$CameraCategory;->this$0:Lcom/android/camera/actor/PhotoActor;

    iget-object v0, v0, Lcom/android/camera/actor/PhotoActor;->mCamera:Lcom/android/camera/Camera;

    invoke-virtual {v0}, Lcom/android/camera/Camera;->isImageCaptureIntent()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/camera/actor/PhotoActor$CameraCategory;->this$0:Lcom/android/camera/actor/PhotoActor;

    iget-object v0, v0, Lcom/android/camera/actor/PhotoActor;->mCamera:Lcom/android/camera/Camera;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/android/camera/Camera;->switchShutter(I)V

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/android/camera/actor/PhotoActor$CameraCategory;->this$0:Lcom/android/camera/actor/PhotoActor;

    iget-object v0, v0, Lcom/android/camera/actor/PhotoActor;->mCamera:Lcom/android/camera/Camera;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/android/camera/Camera;->switchShutter(I)V

    goto :goto_0
.end method
