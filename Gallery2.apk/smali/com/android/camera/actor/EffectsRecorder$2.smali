.class Lcom/android/camera/actor/EffectsRecorder$2;
.super Ljava/lang/Object;
.source "EffectsRecorder.java"

# interfaces
.implements Landroid/filterpacks/videoproc/BackDropperFilter$LearningDoneListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/camera/actor/EffectsRecorder;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/camera/actor/EffectsRecorder;


# direct methods
.method constructor <init>(Lcom/android/camera/actor/EffectsRecorder;)V
    .locals 0

    iput-object p1, p0, Lcom/android/camera/actor/EffectsRecorder$2;->this$0:Lcom/android/camera/actor/EffectsRecorder;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onLearningDone(Landroid/filterpacks/videoproc/BackDropperFilter;)V
    .locals 3
    .param p1    # Landroid/filterpacks/videoproc/BackDropperFilter;

    const/4 v2, 0x1

    invoke-static {}, Lcom/android/camera/actor/EffectsRecorder;->access$000()Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v0, "EffectsRecorder"

    const-string v1, "Learning done callback triggered"

    invoke-static {v0, v1}, Lcom/android/camera/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    iget-object v0, p0, Lcom/android/camera/actor/EffectsRecorder$2;->this$0:Lcom/android/camera/actor/EffectsRecorder;

    const/4 v1, 0x2

    invoke-static {v0, v1, v2}, Lcom/android/camera/actor/EffectsRecorder;->access$500(Lcom/android/camera/actor/EffectsRecorder;II)V

    iget-object v0, p0, Lcom/android/camera/actor/EffectsRecorder$2;->this$0:Lcom/android/camera/actor/EffectsRecorder;

    invoke-virtual {v0, v2}, Lcom/android/camera/actor/EffectsRecorder;->enable3ALocks(Z)V

    return-void
.end method
