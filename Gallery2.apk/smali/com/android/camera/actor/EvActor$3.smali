.class Lcom/android/camera/actor/EvActor$3;
.super Ljava/lang/Object;
.source "EvActor.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/camera/actor/EvActor;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/camera/actor/EvActor;


# direct methods
.method constructor <init>(Lcom/android/camera/actor/EvActor;)V
    .locals 0

    iput-object p1, p0, Lcom/android/camera/actor/EvActor$3;->this$0:Lcom/android/camera/actor/EvActor;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 3
    .param p1    # Landroid/view/View;

    invoke-static {}, Lcom/android/camera/actor/EvActor;->access$300()Z

    move-result v1

    if-eqz v1, :cond_0

    const-string v1, "EvActor"

    const-string v2, "mOkListener.onClick()"

    invoke-static {v1, v2}, Lcom/android/camera/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    iget-object v1, p0, Lcom/android/camera/actor/EvActor$3;->this$0:Lcom/android/camera/actor/EvActor;

    iget-boolean v1, v1, Lcom/android/camera/actor/PhotoActor;->mCameraClosed:Z

    if-eqz v1, :cond_1

    :goto_0
    return-void

    :cond_1
    const/4 v0, 0x0

    :goto_1
    const/4 v1, 0x3

    if-ge v0, v1, :cond_3

    iget-object v1, p0, Lcom/android/camera/actor/EvActor$3;->this$0:Lcom/android/camera/actor/EvActor;

    invoke-static {v1, v0}, Lcom/android/camera/actor/EvActor;->access$400(Lcom/android/camera/actor/EvActor;I)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/android/camera/actor/EvActor$3;->this$0:Lcom/android/camera/actor/EvActor;

    invoke-static {v1}, Lcom/android/camera/actor/EvActor;->access$500(Lcom/android/camera/actor/EvActor;)[Lcom/android/camera/SaveRequest;

    move-result-object v1

    aget-object v1, v1, v0

    invoke-interface {v1}, Lcom/android/camera/SaveRequest;->addRequest()V

    :cond_2
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    :cond_3
    iget-object v1, p0, Lcom/android/camera/actor/EvActor$3;->this$0:Lcom/android/camera/actor/EvActor;

    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Lcom/android/camera/actor/EvActor;->restartPreview(Z)V

    goto :goto_0
.end method
