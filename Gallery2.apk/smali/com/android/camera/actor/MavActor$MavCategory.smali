.class Lcom/android/camera/actor/MavActor$MavCategory;
.super Lcom/android/camera/actor/PhotoActor$CameraCategory;
.source "MavActor.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/camera/actor/MavActor;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = "MavCategory"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/camera/actor/MavActor;


# direct methods
.method constructor <init>(Lcom/android/camera/actor/MavActor;)V
    .locals 0

    iput-object p1, p0, Lcom/android/camera/actor/MavActor$MavCategory;->this$0:Lcom/android/camera/actor/MavActor;

    invoke-direct {p0, p1}, Lcom/android/camera/actor/PhotoActor$CameraCategory;-><init>(Lcom/android/camera/actor/PhotoActor;)V

    return-void
.end method


# virtual methods
.method public applySpecialCapture()Z
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public doOnPictureTaken()V
    .locals 0

    return-void
.end method

.method public doShutter()V
    .locals 2

    iget-object v0, p0, Lcom/android/camera/actor/MavActor$MavCategory;->this$0:Lcom/android/camera/actor/MavActor;

    const/4 v1, 0x2

    invoke-virtual {v0, v1}, Lcom/android/camera/actor/PhotoActor;->playSound(I)V

    iget-object v0, p0, Lcom/android/camera/actor/MavActor$MavCategory;->this$0:Lcom/android/camera/actor/MavActor;

    iget-object v0, v0, Lcom/android/camera/actor/PhotoActor;->mCamera:Lcom/android/camera/Camera;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/android/camera/Camera;->setSwipingEnabled(Z)V

    return-void
.end method

.method public enableFD(Lcom/android/camera/Camera;)Z
    .locals 1
    .param p1    # Lcom/android/camera/Camera;

    const/4 v0, 0x0

    return v0
.end method

.method public initializeFirstTime()V
    .locals 4

    iget-object v0, p0, Lcom/android/camera/actor/MavActor$MavCategory;->this$0:Lcom/android/camera/actor/MavActor;

    iget-object v0, v0, Lcom/android/camera/actor/PhotoActor;->mCamera:Lcom/android/camera/Camera;

    iget-object v1, p0, Lcom/android/camera/actor/MavActor$MavCategory;->this$0:Lcom/android/camera/actor/MavActor;

    invoke-static {v1}, Lcom/android/camera/actor/MavActor;->access$1400(Lcom/android/camera/actor/MavActor;)Lcom/android/camera/Camera$OnFullScreenChangedListener;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/android/camera/Camera;->addOnFullScreenChangedListener(Lcom/android/camera/Camera$OnFullScreenChangedListener;)Z

    iget-object v0, p0, Lcom/android/camera/actor/MavActor$MavCategory;->this$0:Lcom/android/camera/actor/MavActor;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/android/camera/actor/MavActor;->showGuideString(I)V

    iget-object v0, p0, Lcom/android/camera/actor/MavActor$MavCategory;->this$0:Lcom/android/camera/actor/MavActor;

    new-instance v1, Lcom/android/camera/manager/PanoramaViewManager;

    iget-object v2, p0, Lcom/android/camera/actor/MavActor$MavCategory;->this$0:Lcom/android/camera/actor/MavActor;

    iget-object v2, v2, Lcom/android/camera/actor/PhotoActor;->mCamera:Lcom/android/camera/Camera;

    const/4 v3, 0x1

    invoke-direct {v1, v2, v3}, Lcom/android/camera/manager/PanoramaViewManager;-><init>(Lcom/android/camera/Camera;I)V

    invoke-static {v0, v1}, Lcom/android/camera/actor/MavActor;->access$1302(Lcom/android/camera/actor/MavActor;Lcom/android/camera/manager/PanoramaViewManager;)Lcom/android/camera/manager/PanoramaViewManager;

    return-void
.end method

.method public onLeaveActor()V
    .locals 3

    const/4 v2, 0x0

    iget-object v0, p0, Lcom/android/camera/actor/MavActor$MavCategory;->this$0:Lcom/android/camera/actor/MavActor;

    invoke-static {v0, v2}, Lcom/android/camera/actor/MavActor;->access$1502(Lcom/android/camera/actor/MavActor;Z)Z

    iget-object v0, p0, Lcom/android/camera/actor/MavActor$MavCategory;->this$0:Lcom/android/camera/actor/MavActor;

    iget-object v0, v0, Lcom/android/camera/actor/PhotoActor;->mCamera:Lcom/android/camera/Camera;

    const/4 v1, -0x1

    invoke-virtual {v0, v2, v1}, Lcom/android/camera/Camera;->setOrientation(ZI)V

    iget-object v0, p0, Lcom/android/camera/actor/MavActor$MavCategory;->this$0:Lcom/android/camera/actor/MavActor;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/android/camera/actor/PhotoActor;->overrideFocusMode(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/android/camera/actor/MavActor$MavCategory;->this$0:Lcom/android/camera/actor/MavActor;

    iget-object v0, v0, Lcom/android/camera/actor/PhotoActor;->mCamera:Lcom/android/camera/Camera;

    invoke-virtual {v0}, Lcom/android/camera/Camera;->restoreViewState()V

    return-void
.end method

.method public shutterPressed()V
    .locals 2

    const-string v0, "MavActor"

    const-string v1, "MavCategory.shutterPressed"

    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v0, p0, Lcom/android/camera/actor/MavActor$MavCategory;->this$0:Lcom/android/camera/actor/MavActor;

    const-string v1, "auto"

    invoke-virtual {v0, v1}, Lcom/android/camera/actor/PhotoActor;->overrideFocusMode(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/android/camera/actor/MavActor$MavCategory;->this$0:Lcom/android/camera/actor/MavActor;

    const/4 v1, 0x1

    invoke-static {v0, v1}, Lcom/android/camera/actor/MavActor;->access$1502(Lcom/android/camera/actor/MavActor;Z)Z

    iget-object v0, p0, Lcom/android/camera/actor/MavActor$MavCategory;->this$0:Lcom/android/camera/actor/MavActor;

    iget-object v0, v0, Lcom/android/camera/actor/PhotoActor;->mCamera:Lcom/android/camera/Camera;

    invoke-virtual {v0}, Lcom/android/camera/Camera;->getFocusManager()Lcom/android/camera/FocusManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/android/camera/FocusManager;->onShutterDown()V

    return-void
.end method

.method public shutterUp()V
    .locals 2

    const-string v0, "MavActor"

    const-string v1, "MavCategory.shutterUp"

    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v0, p0, Lcom/android/camera/actor/MavActor$MavCategory;->this$0:Lcom/android/camera/actor/MavActor;

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lcom/android/camera/actor/MavActor;->access$1502(Lcom/android/camera/actor/MavActor;Z)Z

    iget-object v0, p0, Lcom/android/camera/actor/MavActor$MavCategory;->this$0:Lcom/android/camera/actor/MavActor;

    iget-object v0, v0, Lcom/android/camera/actor/PhotoActor;->mCamera:Lcom/android/camera/Camera;

    invoke-virtual {v0}, Lcom/android/camera/Camera;->getFocusManager()Lcom/android/camera/FocusManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/android/camera/FocusManager;->onShutterUp()V

    return-void
.end method

.method public skipFocus()Z
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public supportContinuousShot()Z
    .locals 1

    const/4 v0, 0x0

    return v0
.end method
