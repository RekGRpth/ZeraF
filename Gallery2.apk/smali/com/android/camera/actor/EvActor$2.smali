.class Lcom/android/camera/actor/EvActor$2;
.super Ljava/lang/Object;
.source "EvActor.java"

# interfaces
.implements Lcom/android/camera/manager/PickImageViewManager$SelectedChangedListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/camera/actor/EvActor;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/camera/actor/EvActor;


# direct methods
.method constructor <init>(Lcom/android/camera/actor/EvActor;)V
    .locals 0

    iput-object p1, p0, Lcom/android/camera/actor/EvActor$2;->this$0:Lcom/android/camera/actor/EvActor;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onDisplayFail()V
    .locals 2

    invoke-static {}, Lcom/android/camera/actor/EvActor;->access$300()Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v0, "EvActor"

    const-string v1, "onDisplayFail, we don\'t delete the file for analyzing issue."

    invoke-static {v0, v1}, Lcom/android/camera/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    iget-object v0, p0, Lcom/android/camera/actor/EvActor$2;->this$0:Lcom/android/camera/actor/EvActor;

    iget-boolean v0, v0, Lcom/android/camera/actor/PhotoActor;->mCameraClosed:Z

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/android/camera/actor/EvActor$2;->this$0:Lcom/android/camera/actor/EvActor;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/android/camera/actor/EvActor;->restartPreview(Z)V

    :cond_1
    return-void
.end method

.method public onSelectedChanged(Z)V
    .locals 4
    .param p1    # Z

    invoke-static {}, Lcom/android/camera/actor/EvActor;->access$300()Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v0, "EvActor"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "onSelectedChanged selected="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/android/camera/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    iget-object v0, p0, Lcom/android/camera/actor/EvActor$2;->this$0:Lcom/android/camera/actor/EvActor;

    iget-boolean v0, v0, Lcom/android/camera/actor/PhotoActor;->mCameraClosed:Z

    if-eqz v0, :cond_2

    :cond_1
    :goto_0
    return-void

    :cond_2
    const-wide/16 v0, 0x0

    invoke-static {}, Lcom/android/camera/Storage;->getLeftSpace()J

    move-result-wide v2

    cmp-long v0, v0, v2

    if-gez v0, :cond_1

    if-eqz p1, :cond_3

    iget-object v0, p0, Lcom/android/camera/actor/EvActor$2;->this$0:Lcom/android/camera/actor/EvActor;

    iget-object v0, v0, Lcom/android/camera/actor/PhotoActor;->mCamera:Lcom/android/camera/Camera;

    const/4 v1, 0x3

    invoke-virtual {v0, v1}, Lcom/android/camera/Camera;->switchShutter(I)V

    goto :goto_0

    :cond_3
    iget-object v0, p0, Lcom/android/camera/actor/EvActor$2;->this$0:Lcom/android/camera/actor/EvActor;

    iget-object v0, v0, Lcom/android/camera/actor/PhotoActor;->mCamera:Lcom/android/camera/Camera;

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Lcom/android/camera/Camera;->switchShutter(I)V

    goto :goto_0
.end method
