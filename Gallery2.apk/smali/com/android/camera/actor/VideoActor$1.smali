.class Lcom/android/camera/actor/VideoActor$1;
.super Ljava/lang/Object;
.source "VideoActor.java"

# interfaces
.implements Lcom/android/camera/ui/ShutterButton$OnShutterButtonListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/camera/actor/VideoActor;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/camera/actor/VideoActor;


# direct methods
.method constructor <init>(Lcom/android/camera/actor/VideoActor;)V
    .locals 0

    iput-object p1, p0, Lcom/android/camera/actor/VideoActor$1;->this$0:Lcom/android/camera/actor/VideoActor;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onShutterButtonClick(Lcom/android/camera/ui/ShutterButton;)V
    .locals 4
    .param p1    # Lcom/android/camera/ui/ShutterButton;

    invoke-static {}, Lcom/android/camera/actor/VideoActor;->access$300()Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v0, "VideoActor"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Video.onShutterButtonClick("

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ") mMediaRecorderRecording="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/android/camera/actor/VideoActor$1;->this$0:Lcom/android/camera/actor/VideoActor;

    invoke-static {v2}, Lcom/android/camera/actor/VideoActor;->access$500(Lcom/android/camera/actor/VideoActor;)Z

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/android/camera/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    invoke-static {}, Lcom/android/camera/manager/MMProfileManager;->triggerVideoShutterClick()V

    invoke-static {}, Lcom/android/camera/Storage;->getLeftSpace()J

    move-result-wide v0

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-gtz v0, :cond_2

    iget-object v0, p0, Lcom/android/camera/actor/VideoActor$1;->this$0:Lcom/android/camera/actor/VideoActor;

    invoke-static {v0}, Lcom/android/camera/actor/VideoActor;->access$600(Lcom/android/camera/actor/VideoActor;)Z

    :cond_1
    :goto_0
    return-void

    :cond_2
    iget-object v0, p0, Lcom/android/camera/actor/VideoActor$1;->this$0:Lcom/android/camera/actor/VideoActor;

    invoke-static {v0}, Lcom/android/camera/actor/VideoActor;->access$700(Lcom/android/camera/actor/VideoActor;)Z

    move-result v0

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/android/camera/actor/VideoActor$1;->this$0:Lcom/android/camera/actor/VideoActor;

    invoke-static {v0}, Lcom/android/camera/actor/VideoActor;->access$500(Lcom/android/camera/actor/VideoActor;)Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-static {}, Lcom/android/camera/manager/MMProfileManager;->startProfileStopVideoRecording()V

    iget-object v0, p0, Lcom/android/camera/actor/VideoActor$1;->this$0:Lcom/android/camera/actor/VideoActor;

    invoke-static {v0}, Lcom/android/camera/actor/VideoActor;->access$800(Lcom/android/camera/actor/VideoActor;)V

    invoke-static {}, Lcom/android/camera/manager/MMProfileManager;->stopProfileStopVideoRecording()V

    goto :goto_0

    :cond_3
    invoke-static {}, Lcom/android/camera/manager/MMProfileManager;->startProfileStartVideoRecording()V

    invoke-static {}, Lcom/android/camera/FeatureSwitcher;->isContinuousFocusEnabledWhenTouch()Z

    move-result v0

    if-eqz v0, :cond_4

    iget-object v0, p0, Lcom/android/camera/actor/VideoActor$1;->this$0:Lcom/android/camera/actor/VideoActor;

    invoke-static {v0}, Lcom/android/camera/actor/VideoActor;->access$900(Lcom/android/camera/actor/VideoActor;)V

    :cond_4
    iget-object v0, p0, Lcom/android/camera/actor/VideoActor$1;->this$0:Lcom/android/camera/actor/VideoActor;

    invoke-static {v0}, Lcom/android/camera/actor/VideoActor;->access$1000(Lcom/android/camera/actor/VideoActor;)V

    invoke-static {}, Lcom/android/camera/manager/MMProfileManager;->stopProfileStartVideoRecording()V

    iget-object v0, p0, Lcom/android/camera/actor/VideoActor$1;->this$0:Lcom/android/camera/actor/VideoActor;

    invoke-static {v0}, Lcom/android/camera/actor/VideoActor;->access$500(Lcom/android/camera/actor/VideoActor;)Z

    move-result v0

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/android/camera/actor/VideoActor$1;->this$0:Lcom/android/camera/actor/VideoActor;

    invoke-static {v0}, Lcom/android/camera/actor/VideoActor;->access$400(Lcom/android/camera/actor/VideoActor;)Lcom/android/camera/Camera;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/android/camera/Camera;->setSwipingEnabled(Z)V

    goto :goto_0
.end method

.method public onShutterButtonFocus(Lcom/android/camera/ui/ShutterButton;Z)V
    .locals 3
    .param p1    # Lcom/android/camera/ui/ShutterButton;
    .param p2    # Z

    invoke-static {}, Lcom/android/camera/actor/VideoActor;->access$300()Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v0, "VideoActor"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Video.onShutterButtonFocus("

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ")"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/android/camera/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    invoke-static {}, Lcom/android/camera/FeatureSwitcher;->isContinuousFocusEnabledWhenTouch()Z

    move-result v0

    if-eqz v0, :cond_1

    if-nez p2, :cond_1

    iget-object v0, p0, Lcom/android/camera/actor/VideoActor$1;->this$0:Lcom/android/camera/actor/VideoActor;

    invoke-static {v0}, Lcom/android/camera/actor/VideoActor;->access$400(Lcom/android/camera/actor/VideoActor;)Lcom/android/camera/Camera;

    move-result-object v0

    invoke-virtual {v0}, Lcom/android/camera/Camera;->getFocusManager()Lcom/android/camera/FocusManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/android/camera/FocusManager;->onShutterUp()V

    :cond_1
    return-void
.end method

.method public onShutterButtonLongPressed(Lcom/android/camera/ui/ShutterButton;)V
    .locals 3
    .param p1    # Lcom/android/camera/ui/ShutterButton;

    invoke-static {}, Lcom/android/camera/actor/VideoActor;->access$300()Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v0, "VideoActor"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Video.onShutterButtonLongPressed("

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ")"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/android/camera/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    return-void
.end method
