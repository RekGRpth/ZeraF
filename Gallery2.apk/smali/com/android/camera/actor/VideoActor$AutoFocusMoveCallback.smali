.class final Lcom/android/camera/actor/VideoActor$AutoFocusMoveCallback;
.super Ljava/lang/Object;
.source "VideoActor.java"

# interfaces
.implements Landroid/hardware/Camera$AutoFocusMoveCallback;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/camera/actor/VideoActor;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x12
    name = "AutoFocusMoveCallback"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/camera/actor/VideoActor;


# direct methods
.method private constructor <init>(Lcom/android/camera/actor/VideoActor;)V
    .locals 0

    iput-object p1, p0, Lcom/android/camera/actor/VideoActor$AutoFocusMoveCallback;->this$0:Lcom/android/camera/actor/VideoActor;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/android/camera/actor/VideoActor;Lcom/android/camera/actor/VideoActor$1;)V
    .locals 0
    .param p1    # Lcom/android/camera/actor/VideoActor;
    .param p2    # Lcom/android/camera/actor/VideoActor$1;

    invoke-direct {p0, p1}, Lcom/android/camera/actor/VideoActor$AutoFocusMoveCallback;-><init>(Lcom/android/camera/actor/VideoActor;)V

    return-void
.end method


# virtual methods
.method public onAutoFocusMoving(ZLandroid/hardware/Camera;)V
    .locals 1
    .param p1    # Z
    .param p2    # Landroid/hardware/Camera;

    iget-object v0, p0, Lcom/android/camera/actor/VideoActor$AutoFocusMoveCallback;->this$0:Lcom/android/camera/actor/VideoActor;

    invoke-static {v0}, Lcom/android/camera/actor/VideoActor;->access$400(Lcom/android/camera/actor/VideoActor;)Lcom/android/camera/Camera;

    move-result-object v0

    invoke-virtual {v0}, Lcom/android/camera/Camera;->getFocusManager()Lcom/android/camera/FocusManager;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/android/camera/FocusManager;->onAutoFocusMoving(Z)V

    return-void
.end method
