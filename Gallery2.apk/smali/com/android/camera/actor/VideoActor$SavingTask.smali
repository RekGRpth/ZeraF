.class Lcom/android/camera/actor/VideoActor$SavingTask;
.super Ljava/lang/Thread;
.source "VideoActor.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/camera/actor/VideoActor;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "SavingTask"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/camera/actor/VideoActor;


# direct methods
.method private constructor <init>(Lcom/android/camera/actor/VideoActor;)V
    .locals 0

    iput-object p1, p0, Lcom/android/camera/actor/VideoActor$SavingTask;->this$0:Lcom/android/camera/actor/VideoActor;

    invoke-direct {p0}, Ljava/lang/Thread;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/android/camera/actor/VideoActor;Lcom/android/camera/actor/VideoActor$1;)V
    .locals 0
    .param p1    # Lcom/android/camera/actor/VideoActor;
    .param p2    # Lcom/android/camera/actor/VideoActor$1;

    invoke-direct {p0, p1}, Lcom/android/camera/actor/VideoActor$SavingTask;-><init>(Lcom/android/camera/actor/VideoActor;)V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 7

    const/4 v6, 0x0

    invoke-static {}, Lcom/android/camera/actor/VideoActor;->access$300()Z

    move-result v3

    if-eqz v3, :cond_0

    const-string v3, "VideoActor"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "SavingTask.run() begin "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ", mEffectsActive="

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-object v5, p0, Lcom/android/camera/actor/VideoActor$SavingTask;->this$0:Lcom/android/camera/actor/VideoActor;

    invoke-static {v5}, Lcom/android/camera/actor/VideoActor;->access$2100(Lcom/android/camera/actor/VideoActor;)Z

    move-result v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ", mMediaRecorderRecording="

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-object v5, p0, Lcom/android/camera/actor/VideoActor$SavingTask;->this$0:Lcom/android/camera/actor/VideoActor;

    invoke-static {v5}, Lcom/android/camera/actor/VideoActor;->access$500(Lcom/android/camera/actor/VideoActor;)Z

    move-result v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ", mRecorderBusy="

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-object v5, p0, Lcom/android/camera/actor/VideoActor$SavingTask;->this$0:Lcom/android/camera/actor/VideoActor;

    invoke-static {v5}, Lcom/android/camera/actor/VideoActor;->access$2200(Lcom/android/camera/actor/VideoActor;)Z

    move-result v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/android/camera/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    invoke-static {}, Lcom/android/camera/manager/MMProfileManager;->startProfileStoreVideo()V

    const/4 v1, 0x0

    const/4 v2, 0x0

    iget-object v3, p0, Lcom/android/camera/actor/VideoActor$SavingTask;->this$0:Lcom/android/camera/actor/VideoActor;

    invoke-static {v3}, Lcom/android/camera/actor/VideoActor;->access$500(Lcom/android/camera/actor/VideoActor;)Z

    move-result v3

    if-eqz v3, :cond_1

    :try_start_0
    iget-object v3, p0, Lcom/android/camera/actor/VideoActor$SavingTask;->this$0:Lcom/android/camera/actor/VideoActor;

    invoke-static {v3}, Lcom/android/camera/actor/VideoActor;->access$2100(Lcom/android/camera/actor/VideoActor;)Z

    move-result v3

    if-eqz v3, :cond_7

    iget-object v3, p0, Lcom/android/camera/actor/VideoActor$SavingTask;->this$0:Lcom/android/camera/actor/VideoActor;

    invoke-static {v3}, Lcom/android/camera/actor/VideoActor;->access$2300(Lcom/android/camera/actor/VideoActor;)Lcom/android/camera/actor/EffectsRecorder;

    move-result-object v3

    invoke-virtual {v3}, Lcom/android/camera/actor/EffectsRecorder;->stopRecording()V

    :goto_0
    iget-object v3, p0, Lcom/android/camera/actor/VideoActor$SavingTask;->this$0:Lcom/android/camera/actor/VideoActor;

    iget-object v4, p0, Lcom/android/camera/actor/VideoActor$SavingTask;->this$0:Lcom/android/camera/actor/VideoActor;

    invoke-static {v4}, Lcom/android/camera/actor/VideoActor;->access$2500(Lcom/android/camera/actor/VideoActor;)Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/android/camera/actor/VideoActor;->access$1702(Lcom/android/camera/actor/VideoActor;Ljava/lang/String;)Ljava/lang/String;

    invoke-static {}, Lcom/android/camera/actor/VideoActor;->access$300()Z

    move-result v3

    if-eqz v3, :cond_1

    const-string v3, "VideoActor"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Setting current video filename: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-object v5, p0, Lcom/android/camera/actor/VideoActor$SavingTask;->this$0:Lcom/android/camera/actor/VideoActor;

    invoke-static {v5}, Lcom/android/camera/actor/VideoActor;->access$1700(Lcom/android/camera/actor/VideoActor;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/android/camera/Log;->v(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/lang/RuntimeException; {:try_start_0 .. :try_end_0} :catch_0

    :cond_1
    :goto_1
    iget-object v3, p0, Lcom/android/camera/actor/VideoActor$SavingTask;->this$0:Lcom/android/camera/actor/VideoActor;

    invoke-static {v3, v6}, Lcom/android/camera/actor/VideoActor;->access$502(Lcom/android/camera/actor/VideoActor;Z)Z

    iget-object v3, p0, Lcom/android/camera/actor/VideoActor$SavingTask;->this$0:Lcom/android/camera/actor/VideoActor;

    invoke-static {v3}, Lcom/android/camera/actor/VideoActor;->access$700(Lcom/android/camera/actor/VideoActor;)Z

    move-result v3

    if-nez v3, :cond_2

    iget-object v3, p0, Lcom/android/camera/actor/VideoActor$SavingTask;->this$0:Lcom/android/camera/actor/VideoActor;

    invoke-static {v3}, Lcom/android/camera/actor/VideoActor;->access$400(Lcom/android/camera/actor/VideoActor;)Lcom/android/camera/Camera;

    move-result-object v3

    const/4 v4, 0x1

    invoke-virtual {v3, v4}, Lcom/android/camera/Camera;->setCameraState(I)V

    :cond_2
    iget-object v3, p0, Lcom/android/camera/actor/VideoActor$SavingTask;->this$0:Lcom/android/camera/actor/VideoActor;

    invoke-static {v3}, Lcom/android/camera/actor/VideoActor;->access$400(Lcom/android/camera/actor/VideoActor;)Lcom/android/camera/Camera;

    move-result-object v3

    invoke-virtual {v3}, Lcom/android/camera/Camera;->isNonePickIntent()Z

    move-result v3

    if-nez v3, :cond_9

    iget-object v3, p0, Lcom/android/camera/actor/VideoActor$SavingTask;->this$0:Lcom/android/camera/actor/VideoActor;

    invoke-static {v3}, Lcom/android/camera/actor/VideoActor;->access$2100(Lcom/android/camera/actor/VideoActor;)Z

    move-result v3

    if-nez v3, :cond_3

    if-nez v1, :cond_3

    iget-object v3, p0, Lcom/android/camera/actor/VideoActor$SavingTask;->this$0:Lcom/android/camera/actor/VideoActor;

    invoke-static {v3}, Lcom/android/camera/actor/VideoActor;->access$2700(Lcom/android/camera/actor/VideoActor;)I

    move-result v3

    const/4 v4, 0x3

    if-eq v3, v4, :cond_3

    iget-object v3, p0, Lcom/android/camera/actor/VideoActor$SavingTask;->this$0:Lcom/android/camera/actor/VideoActor;

    invoke-static {v3}, Lcom/android/camera/actor/VideoActor;->access$400(Lcom/android/camera/actor/VideoActor;)Lcom/android/camera/Camera;

    move-result-object v3

    invoke-virtual {v3}, Lcom/android/camera/Camera;->isQuickCapture()Z

    move-result v3

    if-eqz v3, :cond_8

    iget-object v3, p0, Lcom/android/camera/actor/VideoActor$SavingTask;->this$0:Lcom/android/camera/actor/VideoActor;

    const/4 v4, 0x2

    invoke-static {v3, v4}, Lcom/android/camera/actor/VideoActor;->access$2702(Lcom/android/camera/actor/VideoActor;I)I

    :cond_3
    :goto_2
    iget-object v3, p0, Lcom/android/camera/actor/VideoActor$SavingTask;->this$0:Lcom/android/camera/actor/VideoActor;

    invoke-static {v3}, Lcom/android/camera/actor/VideoActor;->access$2100(Lcom/android/camera/actor/VideoActor;)Z

    move-result v3

    if-nez v3, :cond_4

    iget-object v3, p0, Lcom/android/camera/actor/VideoActor$SavingTask;->this$0:Lcom/android/camera/actor/VideoActor;

    invoke-static {v3}, Lcom/android/camera/actor/VideoActor;->access$2800(Lcom/android/camera/actor/VideoActor;)V

    :cond_4
    if-eqz v2, :cond_5

    iget-object v3, p0, Lcom/android/camera/actor/VideoActor$SavingTask;->this$0:Lcom/android/camera/actor/VideoActor;

    invoke-static {v3}, Lcom/android/camera/actor/VideoActor;->access$2900(Lcom/android/camera/actor/VideoActor;)V

    :cond_5
    iget-object v3, p0, Lcom/android/camera/actor/VideoActor$SavingTask;->this$0:Lcom/android/camera/actor/VideoActor;

    invoke-static {v3}, Lcom/android/camera/actor/VideoActor;->access$3000(Lcom/android/camera/actor/VideoActor;)Ljava/lang/Thread;

    move-result-object v4

    monitor-enter v4

    :try_start_1
    iget-object v3, p0, Lcom/android/camera/actor/VideoActor$SavingTask;->this$0:Lcom/android/camera/actor/VideoActor;

    invoke-static {v3}, Lcom/android/camera/actor/VideoActor;->access$2100(Lcom/android/camera/actor/VideoActor;)Z

    move-result v3

    if-nez v3, :cond_a

    iget-object v3, p0, Lcom/android/camera/actor/VideoActor$SavingTask;->this$0:Lcom/android/camera/actor/VideoActor;

    invoke-static {v3}, Lcom/android/camera/actor/VideoActor;->access$3000(Lcom/android/camera/actor/VideoActor;)Ljava/lang/Thread;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Object;->notifyAll()V

    iget-object v3, p0, Lcom/android/camera/actor/VideoActor$SavingTask;->this$0:Lcom/android/camera/actor/VideoActor;

    invoke-static {v3}, Lcom/android/camera/actor/VideoActor;->access$3200(Lcom/android/camera/actor/VideoActor;)Landroid/os/Handler;

    move-result-object v3

    iget-object v5, p0, Lcom/android/camera/actor/VideoActor$SavingTask;->this$0:Lcom/android/camera/actor/VideoActor;

    invoke-static {v5}, Lcom/android/camera/actor/VideoActor;->access$3100(Lcom/android/camera/actor/VideoActor;)Ljava/lang/Runnable;

    move-result-object v5

    invoke-virtual {v3, v5}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    iget-object v3, p0, Lcom/android/camera/actor/VideoActor$SavingTask;->this$0:Lcom/android/camera/actor/VideoActor;

    invoke-static {v3}, Lcom/android/camera/actor/VideoActor;->access$3200(Lcom/android/camera/actor/VideoActor;)Landroid/os/Handler;

    move-result-object v3

    iget-object v5, p0, Lcom/android/camera/actor/VideoActor$SavingTask;->this$0:Lcom/android/camera/actor/VideoActor;

    invoke-static {v5}, Lcom/android/camera/actor/VideoActor;->access$3100(Lcom/android/camera/actor/VideoActor;)Ljava/lang/Runnable;

    move-result-object v5

    invoke-virtual {v3, v5}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    :goto_3
    monitor-exit v4
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    invoke-static {}, Lcom/android/camera/actor/VideoActor;->access$300()Z

    move-result v3

    if-eqz v3, :cond_6

    const-string v3, "VideoActor"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "SavingTask.run() end "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ", mCurrentVideoUri="

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-object v5, p0, Lcom/android/camera/actor/VideoActor$SavingTask;->this$0:Lcom/android/camera/actor/VideoActor;

    invoke-static {v5}, Lcom/android/camera/actor/VideoActor;->access$3300(Lcom/android/camera/actor/VideoActor;)Landroid/net/Uri;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ", mRecorderBusy="

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-object v5, p0, Lcom/android/camera/actor/VideoActor$SavingTask;->this$0:Lcom/android/camera/actor/VideoActor;

    invoke-static {v5}, Lcom/android/camera/actor/VideoActor;->access$2200(Lcom/android/camera/actor/VideoActor;)Z

    move-result v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/android/camera/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    :cond_6
    invoke-static {}, Lcom/android/camera/manager/MMProfileManager;->stopProfileStoreVideo()V

    return-void

    :cond_7
    :try_start_2
    iget-object v3, p0, Lcom/android/camera/actor/VideoActor$SavingTask;->this$0:Lcom/android/camera/actor/VideoActor;

    invoke-static {v3}, Lcom/android/camera/actor/VideoActor;->access$2400(Lcom/android/camera/actor/VideoActor;)Landroid/media/MediaRecorder;

    move-result-object v3

    const/4 v4, 0x0

    invoke-virtual {v3, v4}, Landroid/media/MediaRecorder;->setOnErrorListener(Landroid/media/MediaRecorder$OnErrorListener;)V

    iget-object v3, p0, Lcom/android/camera/actor/VideoActor$SavingTask;->this$0:Lcom/android/camera/actor/VideoActor;

    invoke-static {v3}, Lcom/android/camera/actor/VideoActor;->access$2400(Lcom/android/camera/actor/VideoActor;)Landroid/media/MediaRecorder;

    move-result-object v3

    const/4 v4, 0x0

    invoke-virtual {v3, v4}, Landroid/media/MediaRecorder;->setOnInfoListener(Landroid/media/MediaRecorder$OnInfoListener;)V

    iget-object v3, p0, Lcom/android/camera/actor/VideoActor$SavingTask;->this$0:Lcom/android/camera/actor/VideoActor;

    invoke-static {v3}, Lcom/android/camera/actor/VideoActor;->access$2400(Lcom/android/camera/actor/VideoActor;)Landroid/media/MediaRecorder;

    move-result-object v3

    invoke-virtual {v3}, Landroid/media/MediaRecorder;->stop()V

    iget-object v3, p0, Lcom/android/camera/actor/VideoActor$SavingTask;->this$0:Lcom/android/camera/actor/VideoActor;

    invoke-static {v3}, Lcom/android/camera/actor/VideoActor;->access$2400(Lcom/android/camera/actor/VideoActor;)Landroid/media/MediaRecorder;

    move-result-object v3

    const/4 v4, 0x0

    invoke-virtual {v3, v4}, Landroid/media/MediaRecorder;->setOnCameraReleasedListener(Landroid/media/MediaRecorder$OnInfoListener;)V
    :try_end_2
    .catch Ljava/lang/RuntimeException; {:try_start_2 .. :try_end_2} :catch_0

    const/4 v2, 0x1

    goto/16 :goto_0

    :catch_0
    move-exception v0

    const-string v3, "VideoActor"

    const-string v4, "stop fail"

    invoke-static {v3, v4, v0}, Lcom/android/camera/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    const/4 v1, 0x1

    iget-object v3, p0, Lcom/android/camera/actor/VideoActor$SavingTask;->this$0:Lcom/android/camera/actor/VideoActor;

    invoke-static {v3}, Lcom/android/camera/actor/VideoActor;->access$2500(Lcom/android/camera/actor/VideoActor;)Ljava/lang/String;

    move-result-object v3

    if-eqz v3, :cond_1

    iget-object v3, p0, Lcom/android/camera/actor/VideoActor$SavingTask;->this$0:Lcom/android/camera/actor/VideoActor;

    iget-object v4, p0, Lcom/android/camera/actor/VideoActor$SavingTask;->this$0:Lcom/android/camera/actor/VideoActor;

    invoke-static {v4}, Lcom/android/camera/actor/VideoActor;->access$2500(Lcom/android/camera/actor/VideoActor;)Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/android/camera/actor/VideoActor;->access$2600(Lcom/android/camera/actor/VideoActor;Ljava/lang/String;)V

    goto/16 :goto_1

    :cond_8
    iget-object v3, p0, Lcom/android/camera/actor/VideoActor$SavingTask;->this$0:Lcom/android/camera/actor/VideoActor;

    const/4 v4, 0x4

    invoke-static {v3, v4}, Lcom/android/camera/actor/VideoActor;->access$2702(Lcom/android/camera/actor/VideoActor;I)I

    goto/16 :goto_2

    :cond_9
    if-eqz v1, :cond_3

    iget-object v3, p0, Lcom/android/camera/actor/VideoActor$SavingTask;->this$0:Lcom/android/camera/actor/VideoActor;

    const/4 v4, 0x5

    invoke-static {v3, v4}, Lcom/android/camera/actor/VideoActor;->access$2702(Lcom/android/camera/actor/VideoActor;I)I

    goto/16 :goto_2

    :cond_a
    :try_start_3
    iget-object v3, p0, Lcom/android/camera/actor/VideoActor$SavingTask;->this$0:Lcom/android/camera/actor/VideoActor;

    const/4 v5, 0x0

    invoke-static {v3, v5}, Lcom/android/camera/actor/VideoActor;->access$2202(Lcom/android/camera/actor/VideoActor;Z)Z

    goto/16 :goto_3

    :catchall_0
    move-exception v3

    monitor-exit v4
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    throw v3
.end method
