.class Lcom/android/camera/actor/EvActor$4;
.super Ljava/lang/Object;
.source "EvActor.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/camera/actor/EvActor;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/camera/actor/EvActor;


# direct methods
.method constructor <init>(Lcom/android/camera/actor/EvActor;)V
    .locals 0

    iput-object p1, p0, Lcom/android/camera/actor/EvActor$4;->this$0:Lcom/android/camera/actor/EvActor;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 2
    .param p1    # Landroid/view/View;

    invoke-static {}, Lcom/android/camera/actor/EvActor;->access$300()Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v0, "EvActor"

    const-string v1, "mCancelListener.onClick()"

    invoke-static {v0, v1}, Lcom/android/camera/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    iget-object v0, p0, Lcom/android/camera/actor/EvActor$4;->this$0:Lcom/android/camera/actor/EvActor;

    iget-boolean v0, v0, Lcom/android/camera/actor/PhotoActor;->mCameraClosed:Z

    if-eqz v0, :cond_1

    :goto_0
    return-void

    :cond_1
    iget-object v0, p0, Lcom/android/camera/actor/EvActor$4;->this$0:Lcom/android/camera/actor/EvActor;

    invoke-static {v0}, Lcom/android/camera/actor/EvActor;->access$600(Lcom/android/camera/actor/EvActor;)V

    iget-object v0, p0, Lcom/android/camera/actor/EvActor$4;->this$0:Lcom/android/camera/actor/EvActor;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/android/camera/actor/EvActor;->restartPreview(Z)V

    goto :goto_0
.end method
