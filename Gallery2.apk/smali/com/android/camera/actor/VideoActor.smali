.class public Lcom/android/camera/actor/VideoActor;
.super Lcom/android/camera/actor/CameraActor;
.source "VideoActor.java"

# interfaces
.implements Landroid/media/MediaRecorder$OnErrorListener;
.implements Landroid/media/MediaRecorder$OnInfoListener;
.implements Lcom/android/camera/FocusManager$Listener;
.implements Lcom/android/camera/actor/EffectsRecorder$EffectsListener;
.implements Lcom/android/camera/actor/EffectsRecorder$OnSurfaceStateChangeListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/camera/actor/VideoActor$JpegPictureCallback;,
        Lcom/android/camera/actor/VideoActor$AutoFocusMoveCallback;,
        Lcom/android/camera/actor/VideoActor$AutoFocusCallback;,
        Lcom/android/camera/actor/VideoActor$SavingTask;,
        Lcom/android/camera/actor/VideoActor$MainHandler;
    }
.end annotation


# static fields
.field private static final EFFECT_BG_FROM_GALLERY:Ljava/lang/String; = "gallery"

.field private static final FILE_ERROR:J = -0xcL

.field private static final FOCUSED:I = 0x2

.field private static final FOCUSING:I = 0x1

.field private static final FOCUS_IDLE:I = 0x3

.field private static final INVALID_DURATION:J = -0x1L

.field private static final LOG:Z

.field private static final MEDIA_RECORDER_INFO_RECORDING_SIZE:I = 0x37f

.field private static final PREF_CAMERA_VIDEO_HD_RECORDING_ENTRYVALUES:[Ljava/lang/String;

.field private static final REQUEST_EFFECT_BACKDROPPER:I = 0x3e8

.field private static final START_FOCUSING:I = -0x1

.field private static final STOP_FAIL:I = 0x5

.field private static final STOP_NORMAL:I = 0x1

.field private static final STOP_RETURN:I = 0x2

.field private static final STOP_RETURN_UNVALID:I = 0x3

.field private static final STOP_SHOW_ALERT:I = 0x4

.field private static final TAG:Ljava/lang/String; = "VideoActor"

.field private static final UPDATE_RECORD_TIME:I = 0x5

.field private static final UPDATE_SNAP_UI:I = 0xf


# instance fields
.field private final mAutoFocusCallback:Lcom/android/camera/actor/VideoActor$AutoFocusCallback;

.field private final mAutoFocusMoveCallback:Lcom/android/camera/actor/VideoActor$AutoFocusMoveCallback;

.field public mCancelListener:Landroid/view/View$OnClickListener;

.field private mCaptureTimeLapse:Z

.field private mConnectApiReady:Z

.field private mContentResolver:Landroid/content/ContentResolver;

.field private mCurrentShowIndicator:I

.field private mCurrentVideoFilename:Ljava/lang/String;

.field private mCurrentVideoUri:Landroid/net/Uri;

.field private mEffectApplyTime:I

.field private mEffectParameter:Ljava/lang/Object;

.field private mEffectType:I

.field private mEffectUriFromGallery:Ljava/lang/String;

.field private mEffectsDisplayResult:Z

.field private mEffectsError:Z

.field private mEffectsRecorder:Lcom/android/camera/actor/EffectsRecorder;

.field private mErrorCallback:Lcom/android/camera/CameraErrorCallback;

.field private mFocusStartTime:J

.field private mFocusState:I

.field private final mHandler:Landroid/os/Handler;

.field private mIsAutoFocusCallback:Z

.field private mIsContinousFocusMode:Z

.field private mMaxVideoDurationInMs:I

.field private mMediaRecoderRecordingPaused:Z

.field private mMediaRecorder:Landroid/media/MediaRecorder;

.field private mMediaRecorderRecording:Z

.field private mNeedReLearningEffect:Z

.field public mOkListener:Landroid/view/View$OnClickListener;

.field private mOrientation:I

.field private mPhotoSaveRequest:Lcom/android/camera/SaveRequest;

.field private mPhotoShutterListener:Lcom/android/camera/ui/ShutterButton$OnShutterButtonListener;

.field private mProfile:Landroid/media/CamcorderProfile;

.field private mRecordAudio:Z

.field private volatile mRecorderBusy:Z

.field private mRecorderCameraReleased:Z

.field private mRecordingStartTime:J

.field private mRecordingView:Lcom/android/camera/manager/RecordingView;

.field private mRequestedSizeLimit:J

.field private mResetEffect:Z

.field public mRetakeListener:Landroid/view/View$OnClickListener;

.field public mReviewPlay:Landroid/view/View$OnClickListener;

.field private mSingleAutoModeSupported:Z

.field private mSingleStartRecording:Z

.field private mSnapUri:Landroid/net/Uri;

.field private mStartLocation:Landroid/location/Location;

.field private mStartRecordingFailed:Z

.field private mStopVideoRecording:Z

.field private mStoppingAction:I

.field private mTapupListener:Lcom/android/camera/Camera$OnSingleTapUpListener;

.field private mTimeBetweenTimeLapseFrameCaptureMs:I

.field private mTotalSize:J

.field private mVideoCameraClosed:Z

.field private mVideoContext:Lcom/android/camera/Camera;

.field private mVideoFileDescriptor:Landroid/os/ParcelFileDescriptor;

.field private mVideoFilename:Ljava/lang/String;

.field public mVideoPauseResumeListner:Landroid/view/View$OnClickListener;

.field private mVideoRecordedDuration:J

.field private mVideoSaveRequest:Lcom/android/camera/SaveRequest;

.field private mVideoSavedRunnable:Ljava/lang/Runnable;

.field private mVideoSavingTask:Ljava/lang/Thread;

.field private mVideoShutterListener:Lcom/android/camera/ui/ShutterButton$OnShutterButtonListener;

.field private mVideoTempPath:Ljava/lang/String;

.field private mWfdListener:Lcom/android/camera/WfdManagerLocal$Listener;

.field private mWfdListenerEnabled:Z

.field private mWfdManager:Lcom/android/camera/WfdManagerLocal;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    sget-boolean v0, Lcom/android/camera/Log;->LOGV:Z

    sput-boolean v0, Lcom/android/camera/actor/VideoActor;->LOG:Z

    const/4 v0, 0x2

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v2, "normal"

    aput-object v2, v0, v1

    const/4 v1, 0x1

    const-string v2, "indoor"

    aput-object v2, v0, v1

    sput-object v0, Lcom/android/camera/actor/VideoActor;->PREF_CAMERA_VIDEO_HD_RECORDING_ENTRYVALUES:[Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Lcom/android/camera/Camera;)V
    .locals 7
    .param p1    # Lcom/android/camera/Camera;

    const-wide/16 v5, 0x0

    const/4 v4, 0x1

    const/4 v2, 0x0

    const/4 v3, 0x0

    invoke-direct {p0, p1}, Lcom/android/camera/actor/CameraActor;-><init>(Lcom/android/camera/Camera;)V

    iput-object v2, p0, Lcom/android/camera/actor/VideoActor;->mVideoFilename:Ljava/lang/String;

    new-instance v1, Lcom/android/camera/CameraErrorCallback;

    invoke-direct {v1}, Lcom/android/camera/CameraErrorCallback;-><init>()V

    iput-object v1, p0, Lcom/android/camera/actor/VideoActor;->mErrorCallback:Lcom/android/camera/CameraErrorCallback;

    iput-boolean v3, p0, Lcom/android/camera/actor/VideoActor;->mMediaRecorderRecording:Z

    iput-boolean v3, p0, Lcom/android/camera/actor/VideoActor;->mMediaRecoderRecordingPaused:Z

    iput-boolean v4, p0, Lcom/android/camera/actor/VideoActor;->mRecorderCameraReleased:Z

    iput-boolean v3, p0, Lcom/android/camera/actor/VideoActor;->mVideoCameraClosed:Z

    iput-boolean v3, p0, Lcom/android/camera/actor/VideoActor;->mCaptureTimeLapse:Z

    iput-boolean v3, p0, Lcom/android/camera/actor/VideoActor;->mRecordAudio:Z

    iput-boolean v4, p0, Lcom/android/camera/actor/VideoActor;->mConnectApiReady:Z

    iput-boolean v3, p0, Lcom/android/camera/actor/VideoActor;->mStartRecordingFailed:Z

    iput-wide v5, p0, Lcom/android/camera/actor/VideoActor;->mVideoRecordedDuration:J

    iput v3, p0, Lcom/android/camera/actor/VideoActor;->mCurrentShowIndicator:I

    iput v4, p0, Lcom/android/camera/actor/VideoActor;->mStoppingAction:I

    iput v3, p0, Lcom/android/camera/actor/VideoActor;->mTimeBetweenTimeLapseFrameCaptureMs:I

    const/4 v1, -0x1

    iput v1, p0, Lcom/android/camera/actor/VideoActor;->mOrientation:I

    new-instance v1, Lcom/android/camera/actor/VideoActor$AutoFocusCallback;

    invoke-direct {v1, p0, v2}, Lcom/android/camera/actor/VideoActor$AutoFocusCallback;-><init>(Lcom/android/camera/actor/VideoActor;Lcom/android/camera/actor/VideoActor$1;)V

    iput-object v1, p0, Lcom/android/camera/actor/VideoActor;->mAutoFocusCallback:Lcom/android/camera/actor/VideoActor$AutoFocusCallback;

    new-instance v1, Lcom/android/camera/actor/VideoActor$MainHandler;

    invoke-direct {v1, p0, v2}, Lcom/android/camera/actor/VideoActor$MainHandler;-><init>(Lcom/android/camera/actor/VideoActor;Lcom/android/camera/actor/VideoActor$1;)V

    iput-object v1, p0, Lcom/android/camera/actor/VideoActor;->mHandler:Landroid/os/Handler;

    iput-boolean v3, p0, Lcom/android/camera/actor/VideoActor;->mIsAutoFocusCallback:Z

    iput-boolean v3, p0, Lcom/android/camera/actor/VideoActor;->mSingleStartRecording:Z

    iput v3, p0, Lcom/android/camera/actor/VideoActor;->mFocusState:I

    new-instance v1, Lcom/android/camera/actor/VideoActor$AutoFocusMoveCallback;

    invoke-direct {v1, p0, v2}, Lcom/android/camera/actor/VideoActor$AutoFocusMoveCallback;-><init>(Lcom/android/camera/actor/VideoActor;Lcom/android/camera/actor/VideoActor$1;)V

    iput-object v1, p0, Lcom/android/camera/actor/VideoActor;->mAutoFocusMoveCallback:Lcom/android/camera/actor/VideoActor$AutoFocusMoveCallback;

    iput-object v2, p0, Lcom/android/camera/actor/VideoActor;->mEffectParameter:Ljava/lang/Object;

    iput-object v2, p0, Lcom/android/camera/actor/VideoActor;->mEffectUriFromGallery:Ljava/lang/String;

    iput v3, p0, Lcom/android/camera/actor/VideoActor;->mEffectType:I

    const v1, 0x7fffffff

    iput v1, p0, Lcom/android/camera/actor/VideoActor;->mEffectApplyTime:I

    iput-wide v5, p0, Lcom/android/camera/actor/VideoActor;->mRequestedSizeLimit:J

    iput-boolean v4, p0, Lcom/android/camera/actor/VideoActor;->mResetEffect:Z

    iput-boolean v3, p0, Lcom/android/camera/actor/VideoActor;->mEffectsError:Z

    iput-boolean v3, p0, Lcom/android/camera/actor/VideoActor;->mNeedReLearningEffect:Z

    iput-boolean v3, p0, Lcom/android/camera/actor/VideoActor;->mStopVideoRecording:Z

    iput-wide v5, p0, Lcom/android/camera/actor/VideoActor;->mTotalSize:J

    iput-boolean v3, p0, Lcom/android/camera/actor/VideoActor;->mWfdListenerEnabled:Z

    new-instance v1, Lcom/android/camera/actor/VideoActor$1;

    invoke-direct {v1, p0}, Lcom/android/camera/actor/VideoActor$1;-><init>(Lcom/android/camera/actor/VideoActor;)V

    iput-object v1, p0, Lcom/android/camera/actor/VideoActor;->mVideoShutterListener:Lcom/android/camera/ui/ShutterButton$OnShutterButtonListener;

    new-instance v1, Lcom/android/camera/actor/VideoActor$2;

    invoke-direct {v1, p0}, Lcom/android/camera/actor/VideoActor$2;-><init>(Lcom/android/camera/actor/VideoActor;)V

    iput-object v1, p0, Lcom/android/camera/actor/VideoActor;->mPhotoShutterListener:Lcom/android/camera/ui/ShutterButton$OnShutterButtonListener;

    new-instance v1, Lcom/android/camera/actor/VideoActor$3;

    invoke-direct {v1, p0}, Lcom/android/camera/actor/VideoActor$3;-><init>(Lcom/android/camera/actor/VideoActor;)V

    iput-object v1, p0, Lcom/android/camera/actor/VideoActor;->mWfdListener:Lcom/android/camera/WfdManagerLocal$Listener;

    new-instance v1, Lcom/android/camera/actor/VideoActor$4;

    invoke-direct {v1, p0}, Lcom/android/camera/actor/VideoActor$4;-><init>(Lcom/android/camera/actor/VideoActor;)V

    iput-object v1, p0, Lcom/android/camera/actor/VideoActor;->mTapupListener:Lcom/android/camera/Camera$OnSingleTapUpListener;

    iput-boolean v3, p0, Lcom/android/camera/actor/VideoActor;->mRecorderBusy:Z

    new-instance v1, Lcom/android/camera/actor/VideoActor$9;

    invoke-direct {v1, p0}, Lcom/android/camera/actor/VideoActor$9;-><init>(Lcom/android/camera/actor/VideoActor;)V

    iput-object v1, p0, Lcom/android/camera/actor/VideoActor;->mVideoSavedRunnable:Ljava/lang/Runnable;

    new-instance v1, Lcom/android/camera/actor/VideoActor$10;

    invoke-direct {v1, p0}, Lcom/android/camera/actor/VideoActor$10;-><init>(Lcom/android/camera/actor/VideoActor;)V

    iput-object v1, p0, Lcom/android/camera/actor/VideoActor;->mVideoPauseResumeListner:Landroid/view/View$OnClickListener;

    new-instance v1, Lcom/android/camera/actor/VideoActor$14;

    invoke-direct {v1, p0}, Lcom/android/camera/actor/VideoActor$14;-><init>(Lcom/android/camera/actor/VideoActor;)V

    iput-object v1, p0, Lcom/android/camera/actor/VideoActor;->mReviewPlay:Landroid/view/View$OnClickListener;

    new-instance v1, Lcom/android/camera/actor/VideoActor$15;

    invoke-direct {v1, p0}, Lcom/android/camera/actor/VideoActor$15;-><init>(Lcom/android/camera/actor/VideoActor;)V

    iput-object v1, p0, Lcom/android/camera/actor/VideoActor;->mRetakeListener:Landroid/view/View$OnClickListener;

    new-instance v1, Lcom/android/camera/actor/VideoActor$16;

    invoke-direct {v1, p0}, Lcom/android/camera/actor/VideoActor$16;-><init>(Lcom/android/camera/actor/VideoActor;)V

    iput-object v1, p0, Lcom/android/camera/actor/VideoActor;->mOkListener:Landroid/view/View$OnClickListener;

    new-instance v1, Lcom/android/camera/actor/VideoActor$17;

    invoke-direct {v1, p0}, Lcom/android/camera/actor/VideoActor$17;-><init>(Lcom/android/camera/actor/VideoActor;)V

    iput-object v1, p0, Lcom/android/camera/actor/VideoActor;->mCancelListener:Landroid/view/View$OnClickListener;

    invoke-virtual {p0}, Lcom/android/camera/actor/CameraActor;->getContext()Lcom/android/camera/Camera;

    move-result-object v1

    iput-object v1, p0, Lcom/android/camera/actor/VideoActor;->mVideoContext:Lcom/android/camera/Camera;

    iget-object v1, p0, Lcom/android/camera/actor/VideoActor;->mVideoContext:Lcom/android/camera/Camera;

    invoke-virtual {v1}, Lcom/android/camera/Camera;->isNonePickIntent()Z

    move-result v1

    if-nez v1, :cond_2

    iget-object v1, p0, Lcom/android/camera/actor/VideoActor;->mVideoContext:Lcom/android/camera/Camera;

    const/4 v2, 0x2

    invoke-virtual {v1, v2}, Lcom/android/camera/Camera;->switchShutter(I)V

    :goto_0
    new-instance v1, Lcom/android/camera/manager/RecordingView;

    iget-object v2, p0, Lcom/android/camera/actor/VideoActor;->mVideoContext:Lcom/android/camera/Camera;

    invoke-direct {v1, v2}, Lcom/android/camera/manager/RecordingView;-><init>(Lcom/android/camera/Camera;)V

    iput-object v1, p0, Lcom/android/camera/actor/VideoActor;->mRecordingView:Lcom/android/camera/manager/RecordingView;

    iget-object v1, p0, Lcom/android/camera/actor/VideoActor;->mRecordingView:Lcom/android/camera/manager/RecordingView;

    iget-object v2, p0, Lcom/android/camera/actor/VideoActor;->mVideoPauseResumeListner:Landroid/view/View$OnClickListener;

    invoke-virtual {v1, v2}, Lcom/android/camera/manager/RecordingView;->setListener(Landroid/view/View$OnClickListener;)V

    invoke-static {}, Lcom/android/camera/CameraHolder;->instance()Lcom/android/camera/CameraHolder;

    move-result-object v1

    invoke-virtual {v1}, Lcom/android/camera/CameraHolder;->getFrontCameraId()I

    move-result v0

    iget-object v1, p0, Lcom/android/camera/actor/VideoActor;->mVideoContext:Lcom/android/camera/Camera;

    invoke-virtual {v1}, Lcom/android/camera/Camera;->getCameraId()I

    move-result v1

    if-eq v1, v0, :cond_3

    invoke-direct {p0}, Lcom/android/camera/actor/VideoActor;->effectActive()Z

    move-result v1

    if-nez v1, :cond_3

    iget-object v1, p0, Lcom/android/camera/actor/VideoActor;->mVideoContext:Lcom/android/camera/Camera;

    invoke-virtual {v1}, Lcom/android/camera/Camera;->getShutterManager()Lcom/android/camera/manager/ShutterManager;

    move-result-object v1

    invoke-static {}, Lcom/android/camera/FeatureSwitcher;->isVssEnabled()Z

    move-result v2

    invoke-virtual {v1, v2}, Lcom/android/camera/manager/ShutterManager;->setPhotoShutterEnabled(Z)V

    sget-boolean v1, Lcom/android/camera/actor/VideoActor;->LOG:Z

    if-eqz v1, :cond_0

    const-string v1, "VideoActor"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "FeatureSwitcher.isVssEnabled()= "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-static {}, Lcom/android/camera/FeatureSwitcher;->isVssEnabled()Z

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/android/camera/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    :goto_1
    iget-object v1, p0, Lcom/android/camera/actor/VideoActor;->mVideoContext:Lcom/android/camera/Camera;

    invoke-virtual {v1}, Lcom/android/camera/Camera;->getWfdManagerLocal()Lcom/android/camera/WfdManagerLocal;

    move-result-object v1

    iput-object v1, p0, Lcom/android/camera/actor/VideoActor;->mWfdManager:Lcom/android/camera/WfdManagerLocal;

    iget-object v1, p0, Lcom/android/camera/actor/VideoActor;->mWfdManager:Lcom/android/camera/WfdManagerLocal;

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/android/camera/actor/VideoActor;->mWfdManager:Lcom/android/camera/WfdManagerLocal;

    iget-object v2, p0, Lcom/android/camera/actor/VideoActor;->mWfdListener:Lcom/android/camera/WfdManagerLocal$Listener;

    invoke-virtual {v1, v2}, Lcom/android/camera/WfdManagerLocal;->addListener(Lcom/android/camera/WfdManagerLocal$Listener;)Z

    :cond_1
    return-void

    :cond_2
    iget-object v1, p0, Lcom/android/camera/actor/VideoActor;->mVideoContext:Lcom/android/camera/Camera;

    invoke-virtual {v1, v3}, Lcom/android/camera/Camera;->switchShutter(I)V

    goto :goto_0

    :cond_3
    iget-object v1, p0, Lcom/android/camera/actor/VideoActor;->mVideoContext:Lcom/android/camera/Camera;

    invoke-virtual {v1}, Lcom/android/camera/Camera;->getShutterManager()Lcom/android/camera/manager/ShutterManager;

    move-result-object v1

    invoke-virtual {v1, v3}, Lcom/android/camera/manager/ShutterManager;->setPhotoShutterEnabled(Z)V

    sget-boolean v1, Lcom/android/camera/actor/VideoActor;->LOG:Z

    if-eqz v1, :cond_0

    const-string v1, "VideoActor"

    const-string v2, "mVideoContext.getShutterManager().setPhotoShutterEnabled(false)"

    invoke-static {v1, v2}, Lcom/android/camera/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1
.end method

.method static synthetic access$1000(Lcom/android/camera/actor/VideoActor;)V
    .locals 0
    .param p0    # Lcom/android/camera/actor/VideoActor;

    invoke-direct {p0}, Lcom/android/camera/actor/VideoActor;->startVideoRecording()V

    return-void
.end method

.method static synthetic access$1100(Lcom/android/camera/actor/VideoActor;)Z
    .locals 1
    .param p0    # Lcom/android/camera/actor/VideoActor;

    iget-boolean v0, p0, Lcom/android/camera/actor/VideoActor;->mStopVideoRecording:Z

    return v0
.end method

.method static synthetic access$1200(Lcom/android/camera/actor/VideoActor;)Lcom/android/camera/SaveRequest;
    .locals 1
    .param p0    # Lcom/android/camera/actor/VideoActor;

    iget-object v0, p0, Lcom/android/camera/actor/VideoActor;->mPhotoSaveRequest:Lcom/android/camera/SaveRequest;

    return-object v0
.end method

.method static synthetic access$1202(Lcom/android/camera/actor/VideoActor;Lcom/android/camera/SaveRequest;)Lcom/android/camera/SaveRequest;
    .locals 0
    .param p0    # Lcom/android/camera/actor/VideoActor;
    .param p1    # Lcom/android/camera/SaveRequest;

    iput-object p1, p0, Lcom/android/camera/actor/VideoActor;->mPhotoSaveRequest:Lcom/android/camera/SaveRequest;

    return-object p1
.end method

.method static synthetic access$1300(Lcom/android/camera/actor/VideoActor;Z)V
    .locals 0
    .param p0    # Lcom/android/camera/actor/VideoActor;
    .param p1    # Z

    invoke-direct {p0, p1}, Lcom/android/camera/actor/VideoActor;->showVideoSnapshotUI(Z)V

    return-void
.end method

.method static synthetic access$1402(Lcom/android/camera/actor/VideoActor;Z)Z
    .locals 0
    .param p0    # Lcom/android/camera/actor/VideoActor;
    .param p1    # Z

    iput-boolean p1, p0, Lcom/android/camera/actor/VideoActor;->mWfdListenerEnabled:Z

    return p1
.end method

.method static synthetic access$1500(Lcom/android/camera/actor/VideoActor;I)V
    .locals 0
    .param p0    # Lcom/android/camera/actor/VideoActor;
    .param p1    # I

    invoke-direct {p0, p1}, Lcom/android/camera/actor/VideoActor;->setFocusState(I)V

    return-void
.end method

.method static synthetic access$1600(Lcom/android/camera/actor/VideoActor;)Landroid/os/ParcelFileDescriptor;
    .locals 1
    .param p0    # Lcom/android/camera/actor/VideoActor;

    iget-object v0, p0, Lcom/android/camera/actor/VideoActor;->mVideoFileDescriptor:Landroid/os/ParcelFileDescriptor;

    return-object v0
.end method

.method static synthetic access$1700(Lcom/android/camera/actor/VideoActor;)Ljava/lang/String;
    .locals 1
    .param p0    # Lcom/android/camera/actor/VideoActor;

    iget-object v0, p0, Lcom/android/camera/actor/VideoActor;->mCurrentVideoFilename:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$1702(Lcom/android/camera/actor/VideoActor;Ljava/lang/String;)Ljava/lang/String;
    .locals 0
    .param p0    # Lcom/android/camera/actor/VideoActor;
    .param p1    # Ljava/lang/String;

    iput-object p1, p0, Lcom/android/camera/actor/VideoActor;->mCurrentVideoFilename:Ljava/lang/String;

    return-object p1
.end method

.method static synthetic access$1800(Lcom/android/camera/actor/VideoActor;)V
    .locals 0
    .param p0    # Lcom/android/camera/actor/VideoActor;

    invoke-direct {p0}, Lcom/android/camera/actor/VideoActor;->updateRecordingTime()V

    return-void
.end method

.method static synthetic access$1900(Lcom/android/camera/actor/VideoActor;)Landroid/net/Uri;
    .locals 1
    .param p0    # Lcom/android/camera/actor/VideoActor;

    iget-object v0, p0, Lcom/android/camera/actor/VideoActor;->mSnapUri:Landroid/net/Uri;

    return-object v0
.end method

.method static synthetic access$2100(Lcom/android/camera/actor/VideoActor;)Z
    .locals 1
    .param p0    # Lcom/android/camera/actor/VideoActor;

    invoke-direct {p0}, Lcom/android/camera/actor/VideoActor;->effectActive()Z

    move-result v0

    return v0
.end method

.method static synthetic access$2200(Lcom/android/camera/actor/VideoActor;)Z
    .locals 1
    .param p0    # Lcom/android/camera/actor/VideoActor;

    iget-boolean v0, p0, Lcom/android/camera/actor/VideoActor;->mRecorderBusy:Z

    return v0
.end method

.method static synthetic access$2202(Lcom/android/camera/actor/VideoActor;Z)Z
    .locals 0
    .param p0    # Lcom/android/camera/actor/VideoActor;
    .param p1    # Z

    iput-boolean p1, p0, Lcom/android/camera/actor/VideoActor;->mRecorderBusy:Z

    return p1
.end method

.method static synthetic access$2300(Lcom/android/camera/actor/VideoActor;)Lcom/android/camera/actor/EffectsRecorder;
    .locals 1
    .param p0    # Lcom/android/camera/actor/VideoActor;

    iget-object v0, p0, Lcom/android/camera/actor/VideoActor;->mEffectsRecorder:Lcom/android/camera/actor/EffectsRecorder;

    return-object v0
.end method

.method static synthetic access$2400(Lcom/android/camera/actor/VideoActor;)Landroid/media/MediaRecorder;
    .locals 1
    .param p0    # Lcom/android/camera/actor/VideoActor;

    iget-object v0, p0, Lcom/android/camera/actor/VideoActor;->mMediaRecorder:Landroid/media/MediaRecorder;

    return-object v0
.end method

.method static synthetic access$2500(Lcom/android/camera/actor/VideoActor;)Ljava/lang/String;
    .locals 1
    .param p0    # Lcom/android/camera/actor/VideoActor;

    iget-object v0, p0, Lcom/android/camera/actor/VideoActor;->mVideoFilename:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$2600(Lcom/android/camera/actor/VideoActor;Ljava/lang/String;)V
    .locals 0
    .param p0    # Lcom/android/camera/actor/VideoActor;
    .param p1    # Ljava/lang/String;

    invoke-direct {p0, p1}, Lcom/android/camera/actor/VideoActor;->deleteVideoFile(Ljava/lang/String;)V

    return-void
.end method

.method static synthetic access$2700(Lcom/android/camera/actor/VideoActor;)I
    .locals 1
    .param p0    # Lcom/android/camera/actor/VideoActor;

    iget v0, p0, Lcom/android/camera/actor/VideoActor;->mStoppingAction:I

    return v0
.end method

.method static synthetic access$2702(Lcom/android/camera/actor/VideoActor;I)I
    .locals 0
    .param p0    # Lcom/android/camera/actor/VideoActor;
    .param p1    # I

    iput p1, p0, Lcom/android/camera/actor/VideoActor;->mStoppingAction:I

    return p1
.end method

.method static synthetic access$2800(Lcom/android/camera/actor/VideoActor;)V
    .locals 0
    .param p0    # Lcom/android/camera/actor/VideoActor;

    invoke-direct {p0}, Lcom/android/camera/actor/VideoActor;->releaseMediaRecorder()V

    return-void
.end method

.method static synthetic access$2900(Lcom/android/camera/actor/VideoActor;)V
    .locals 0
    .param p0    # Lcom/android/camera/actor/VideoActor;

    invoke-direct {p0}, Lcom/android/camera/actor/VideoActor;->addVideoToMediaStore()V

    return-void
.end method

.method static synthetic access$300()Z
    .locals 1

    sget-boolean v0, Lcom/android/camera/actor/VideoActor;->LOG:Z

    return v0
.end method

.method static synthetic access$3000(Lcom/android/camera/actor/VideoActor;)Ljava/lang/Thread;
    .locals 1
    .param p0    # Lcom/android/camera/actor/VideoActor;

    iget-object v0, p0, Lcom/android/camera/actor/VideoActor;->mVideoSavingTask:Ljava/lang/Thread;

    return-object v0
.end method

.method static synthetic access$3100(Lcom/android/camera/actor/VideoActor;)Ljava/lang/Runnable;
    .locals 1
    .param p0    # Lcom/android/camera/actor/VideoActor;

    iget-object v0, p0, Lcom/android/camera/actor/VideoActor;->mVideoSavedRunnable:Ljava/lang/Runnable;

    return-object v0
.end method

.method static synthetic access$3200(Lcom/android/camera/actor/VideoActor;)Landroid/os/Handler;
    .locals 1
    .param p0    # Lcom/android/camera/actor/VideoActor;

    iget-object v0, p0, Lcom/android/camera/actor/VideoActor;->mHandler:Landroid/os/Handler;

    return-object v0
.end method

.method static synthetic access$3300(Lcom/android/camera/actor/VideoActor;)Landroid/net/Uri;
    .locals 1
    .param p0    # Lcom/android/camera/actor/VideoActor;

    iget-object v0, p0, Lcom/android/camera/actor/VideoActor;->mCurrentVideoUri:Landroid/net/Uri;

    return-object v0
.end method

.method static synthetic access$3400(Lcom/android/camera/actor/VideoActor;)Z
    .locals 1
    .param p0    # Lcom/android/camera/actor/VideoActor;

    iget-boolean v0, p0, Lcom/android/camera/actor/VideoActor;->mEffectsDisplayResult:Z

    return v0
.end method

.method static synthetic access$3500(Lcom/android/camera/actor/VideoActor;)I
    .locals 1
    .param p0    # Lcom/android/camera/actor/VideoActor;

    iget v0, p0, Lcom/android/camera/actor/VideoActor;->mFocusState:I

    return v0
.end method

.method static synthetic access$3600(Lcom/android/camera/actor/VideoActor;)Z
    .locals 1
    .param p0    # Lcom/android/camera/actor/VideoActor;

    iget-boolean v0, p0, Lcom/android/camera/actor/VideoActor;->mSingleAutoModeSupported:Z

    return v0
.end method

.method static synthetic access$3700(Lcom/android/camera/actor/VideoActor;Z)V
    .locals 0
    .param p0    # Lcom/android/camera/actor/VideoActor;
    .param p1    # Z

    invoke-direct {p0, p1}, Lcom/android/camera/actor/VideoActor;->hideOtherSettings(Z)V

    return-void
.end method

.method static synthetic access$3800(Lcom/android/camera/actor/VideoActor;)V
    .locals 0
    .param p0    # Lcom/android/camera/actor/VideoActor;

    invoke-direct {p0}, Lcom/android/camera/actor/VideoActor;->showAlert()V

    return-void
.end method

.method static synthetic access$3900(Lcom/android/camera/actor/VideoActor;Z)V
    .locals 0
    .param p0    # Lcom/android/camera/actor/VideoActor;
    .param p1    # Z

    invoke-direct {p0, p1}, Lcom/android/camera/actor/VideoActor;->doReturnToCaller(Z)V

    return-void
.end method

.method static synthetic access$400(Lcom/android/camera/actor/VideoActor;)Lcom/android/camera/Camera;
    .locals 1
    .param p0    # Lcom/android/camera/actor/VideoActor;

    iget-object v0, p0, Lcom/android/camera/actor/VideoActor;->mVideoContext:Lcom/android/camera/Camera;

    return-object v0
.end method

.method static synthetic access$4000(Lcom/android/camera/actor/VideoActor;)V
    .locals 0
    .param p0    # Lcom/android/camera/actor/VideoActor;

    invoke-direct {p0}, Lcom/android/camera/actor/VideoActor;->closeVideoFileDescriptor()V

    return-void
.end method

.method static synthetic access$4100(Lcom/android/camera/actor/VideoActor;)V
    .locals 0
    .param p0    # Lcom/android/camera/actor/VideoActor;

    invoke-direct {p0}, Lcom/android/camera/actor/VideoActor;->changeFocusState()V

    return-void
.end method

.method static synthetic access$4200(Lcom/android/camera/actor/VideoActor;)Z
    .locals 1
    .param p0    # Lcom/android/camera/actor/VideoActor;

    iget-boolean v0, p0, Lcom/android/camera/actor/VideoActor;->mMediaRecoderRecordingPaused:Z

    return v0
.end method

.method static synthetic access$4202(Lcom/android/camera/actor/VideoActor;Z)Z
    .locals 0
    .param p0    # Lcom/android/camera/actor/VideoActor;
    .param p1    # Z

    iput-boolean p1, p0, Lcom/android/camera/actor/VideoActor;->mMediaRecoderRecordingPaused:Z

    return p1
.end method

.method static synthetic access$4300(Lcom/android/camera/actor/VideoActor;)Lcom/android/camera/manager/RecordingView;
    .locals 1
    .param p0    # Lcom/android/camera/actor/VideoActor;

    iget-object v0, p0, Lcom/android/camera/actor/VideoActor;->mRecordingView:Lcom/android/camera/manager/RecordingView;

    return-object v0
.end method

.method static synthetic access$4402(Lcom/android/camera/actor/VideoActor;J)J
    .locals 0
    .param p0    # Lcom/android/camera/actor/VideoActor;
    .param p1    # J

    iput-wide p1, p0, Lcom/android/camera/actor/VideoActor;->mRecordingStartTime:J

    return-wide p1
.end method

.method static synthetic access$4500(Lcom/android/camera/actor/VideoActor;)J
    .locals 2
    .param p0    # Lcom/android/camera/actor/VideoActor;

    iget-wide v0, p0, Lcom/android/camera/actor/VideoActor;->mVideoRecordedDuration:J

    return-wide v0
.end method

.method static synthetic access$4502(Lcom/android/camera/actor/VideoActor;J)J
    .locals 0
    .param p0    # Lcom/android/camera/actor/VideoActor;
    .param p1    # J

    iput-wide p1, p0, Lcom/android/camera/actor/VideoActor;->mVideoRecordedDuration:J

    return-wide p1
.end method

.method static synthetic access$4600(Lcom/android/camera/actor/VideoActor;)V
    .locals 0
    .param p0    # Lcom/android/camera/actor/VideoActor;

    invoke-direct {p0}, Lcom/android/camera/actor/VideoActor;->pauseVideoRecording()V

    return-void
.end method

.method static synthetic access$4700(Lcom/android/camera/actor/VideoActor;)J
    .locals 2
    .param p0    # Lcom/android/camera/actor/VideoActor;

    iget-wide v0, p0, Lcom/android/camera/actor/VideoActor;->mFocusStartTime:J

    return-wide v0
.end method

.method static synthetic access$4802(Lcom/android/camera/actor/VideoActor;Z)Z
    .locals 0
    .param p0    # Lcom/android/camera/actor/VideoActor;
    .param p1    # Z

    iput-boolean p1, p0, Lcom/android/camera/actor/VideoActor;->mIsAutoFocusCallback:Z

    return p1
.end method

.method static synthetic access$4900(Lcom/android/camera/actor/VideoActor;)V
    .locals 0
    .param p0    # Lcom/android/camera/actor/VideoActor;

    invoke-direct {p0}, Lcom/android/camera/actor/VideoActor;->startPlayVideoActivity()V

    return-void
.end method

.method static synthetic access$500(Lcom/android/camera/actor/VideoActor;)Z
    .locals 1
    .param p0    # Lcom/android/camera/actor/VideoActor;

    iget-boolean v0, p0, Lcom/android/camera/actor/VideoActor;->mMediaRecorderRecording:Z

    return v0
.end method

.method static synthetic access$5000(Lcom/android/camera/actor/VideoActor;)V
    .locals 0
    .param p0    # Lcom/android/camera/actor/VideoActor;

    invoke-direct {p0}, Lcom/android/camera/actor/VideoActor;->deleteCurrentVideo()V

    return-void
.end method

.method static synthetic access$502(Lcom/android/camera/actor/VideoActor;Z)Z
    .locals 0
    .param p0    # Lcom/android/camera/actor/VideoActor;
    .param p1    # Z

    iput-boolean p1, p0, Lcom/android/camera/actor/VideoActor;->mMediaRecorderRecording:Z

    return p1
.end method

.method static synthetic access$5100(Lcom/android/camera/actor/VideoActor;)Z
    .locals 1
    .param p0    # Lcom/android/camera/actor/VideoActor;

    iget-boolean v0, p0, Lcom/android/camera/actor/VideoActor;->mNeedReLearningEffect:Z

    return v0
.end method

.method static synthetic access$5102(Lcom/android/camera/actor/VideoActor;Z)Z
    .locals 0
    .param p0    # Lcom/android/camera/actor/VideoActor;
    .param p1    # Z

    iput-boolean p1, p0, Lcom/android/camera/actor/VideoActor;->mNeedReLearningEffect:Z

    return p1
.end method

.method static synthetic access$5200(Lcom/android/camera/actor/VideoActor;)V
    .locals 0
    .param p0    # Lcom/android/camera/actor/VideoActor;

    invoke-direct {p0}, Lcom/android/camera/actor/VideoActor;->startPreview()V

    return-void
.end method

.method static synthetic access$5300(Lcom/android/camera/actor/VideoActor;)V
    .locals 0
    .param p0    # Lcom/android/camera/actor/VideoActor;

    invoke-direct {p0}, Lcom/android/camera/actor/VideoActor;->stopVideoRecordingAsync()V

    return-void
.end method

.method static synthetic access$5400(Lcom/android/camera/actor/VideoActor;)V
    .locals 0
    .param p0    # Lcom/android/camera/actor/VideoActor;

    invoke-direct {p0}, Lcom/android/camera/actor/VideoActor;->releaseEffects()V

    return-void
.end method

.method static synthetic access$5500(Lcom/android/camera/actor/VideoActor;)Lcom/android/camera/SaveRequest;
    .locals 1
    .param p0    # Lcom/android/camera/actor/VideoActor;

    iget-object v0, p0, Lcom/android/camera/actor/VideoActor;->mVideoSaveRequest:Lcom/android/camera/SaveRequest;

    return-object v0
.end method

.method static synthetic access$600(Lcom/android/camera/actor/VideoActor;)Z
    .locals 1
    .param p0    # Lcom/android/camera/actor/VideoActor;

    invoke-direct {p0}, Lcom/android/camera/actor/VideoActor;->backToLastModeIfNeed()Z

    move-result v0

    return v0
.end method

.method static synthetic access$700(Lcom/android/camera/actor/VideoActor;)Z
    .locals 1
    .param p0    # Lcom/android/camera/actor/VideoActor;

    iget-boolean v0, p0, Lcom/android/camera/actor/VideoActor;->mVideoCameraClosed:Z

    return v0
.end method

.method static synthetic access$800(Lcom/android/camera/actor/VideoActor;)V
    .locals 0
    .param p0    # Lcom/android/camera/actor/VideoActor;

    invoke-direct {p0}, Lcom/android/camera/actor/VideoActor;->onStopVideoRecordingAsync()V

    return-void
.end method

.method static synthetic access$900(Lcom/android/camera/actor/VideoActor;)V
    .locals 0
    .param p0    # Lcom/android/camera/actor/VideoActor;

    invoke-direct {p0}, Lcom/android/camera/actor/VideoActor;->shutterPressed()V

    return-void
.end method

.method private addVideoToMediaStore()V
    .locals 5

    iget-object v2, p0, Lcom/android/camera/actor/VideoActor;->mVideoFileDescriptor:Landroid/os/ParcelFileDescriptor;

    if-nez v2, :cond_2

    iget-object v2, p0, Lcom/android/camera/actor/VideoActor;->mVideoContext:Lcom/android/camera/Camera;

    invoke-virtual {v2}, Lcom/android/camera/Camera;->getFileSaver()Lcom/android/camera/FileSaver;

    move-result-object v1

    iget-object v2, p0, Lcom/android/camera/actor/VideoActor;->mProfile:Landroid/media/CamcorderProfile;

    iget v2, v2, Landroid/media/CamcorderProfile;->fileFormat:I

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v4, p0, Lcom/android/camera/actor/VideoActor;->mProfile:Landroid/media/CamcorderProfile;

    iget v4, v4, Landroid/media/CamcorderProfile;->videoFrameWidth:I

    invoke-static {v4}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "x"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lcom/android/camera/actor/VideoActor;->mProfile:Landroid/media/CamcorderProfile;

    iget v4, v4, Landroid/media/CamcorderProfile;->videoFrameHeight:I

    invoke-static {v4}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Lcom/android/camera/FileSaver;->prepareVideoRequest(ILjava/lang/String;)Lcom/android/camera/SaveRequest;

    move-result-object v2

    iput-object v2, p0, Lcom/android/camera/actor/VideoActor;->mVideoSaveRequest:Lcom/android/camera/SaveRequest;

    iget-object v2, p0, Lcom/android/camera/actor/VideoActor;->mVideoSaveRequest:Lcom/android/camera/SaveRequest;

    iget-object v3, p0, Lcom/android/camera/actor/VideoActor;->mStartLocation:Landroid/location/Location;

    invoke-interface {v2, v3}, Lcom/android/camera/SaveRequest;->setLocation(Landroid/location/Location;)V

    iget-object v2, p0, Lcom/android/camera/actor/VideoActor;->mVideoSaveRequest:Lcom/android/camera/SaveRequest;

    iget-object v3, p0, Lcom/android/camera/actor/VideoActor;->mVideoTempPath:Ljava/lang/String;

    invoke-interface {v2, v3}, Lcom/android/camera/SaveRequest;->setTempPath(Ljava/lang/String;)V

    invoke-static {}, Lcom/android/camera/Storage;->isStorageReady()Z

    move-result v2

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/android/camera/actor/VideoActor;->mVideoSaveRequest:Lcom/android/camera/SaveRequest;

    invoke-direct {p0}, Lcom/android/camera/actor/VideoActor;->computeDuration()J

    move-result-wide v3

    invoke-interface {v2, v3, v4}, Lcom/android/camera/SaveRequest;->setDuration(J)V

    :cond_0
    iget-object v3, p0, Lcom/android/camera/actor/VideoActor;->mVideoSaveRequest:Lcom/android/camera/SaveRequest;

    iget-object v2, p0, Lcom/android/camera/actor/VideoActor;->mVideoContext:Lcom/android/camera/Camera;

    invoke-virtual {v2}, Lcom/android/camera/Camera;->isNonePickIntent()Z

    move-result v2

    if-nez v2, :cond_3

    const/4 v2, 0x1

    :goto_0
    invoke-interface {v3, v2}, Lcom/android/camera/SaveRequest;->setIgnoreThumbnail(Z)V

    iget-object v2, p0, Lcom/android/camera/actor/VideoActor;->mVideoSaveRequest:Lcom/android/camera/SaveRequest;

    new-instance v3, Lcom/android/camera/actor/VideoActor$19;

    invoke-direct {v3, p0}, Lcom/android/camera/actor/VideoActor$19;-><init>(Lcom/android/camera/actor/VideoActor;)V

    invoke-interface {v2, v3}, Lcom/android/camera/SaveRequest;->setListener(Lcom/android/camera/FileSaver$FileSaverListener;)V

    iget-object v2, p0, Lcom/android/camera/actor/VideoActor;->mVideoSaveRequest:Lcom/android/camera/SaveRequest;

    invoke-interface {v2}, Lcom/android/camera/SaveRequest;->addRequest()V

    :try_start_0
    sget-boolean v2, Lcom/android/camera/actor/VideoActor;->LOG:Z

    if-eqz v2, :cond_1

    const-string v2, "VideoActor"

    const-string v3, "Wait for URI when saving video done"

    invoke-static {v2, v3}, Lcom/android/camera/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    :cond_1
    iget-object v3, p0, Lcom/android/camera/actor/VideoActor;->mVideoSaveRequest:Lcom/android/camera/SaveRequest;

    monitor-enter v3
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0

    :try_start_1
    iget-object v2, p0, Lcom/android/camera/actor/VideoActor;->mVideoSaveRequest:Lcom/android/camera/SaveRequest;

    invoke-virtual {v2}, Ljava/lang/Object;->wait()V

    monitor-exit v3
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    :goto_1
    iget-object v2, p0, Lcom/android/camera/actor/VideoActor;->mVideoSaveRequest:Lcom/android/camera/SaveRequest;

    invoke-interface {v2}, Lcom/android/camera/SaveRequest;->getUri()Landroid/net/Uri;

    move-result-object v2

    iput-object v2, p0, Lcom/android/camera/actor/VideoActor;->mCurrentVideoUri:Landroid/net/Uri;

    iget-object v2, p0, Lcom/android/camera/actor/VideoActor;->mVideoSaveRequest:Lcom/android/camera/SaveRequest;

    invoke-interface {v2}, Lcom/android/camera/SaveRequest;->getFilePath()Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/android/camera/actor/VideoActor;->mCurrentVideoFilename:Ljava/lang/String;

    sget-boolean v2, Lcom/android/camera/actor/VideoActor;->LOG:Z

    if-eqz v2, :cond_2

    const-string v2, "VideoActor"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Saving video,mCurrentVideoUri=="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lcom/android/camera/actor/VideoActor;->mCurrentVideoUri:Landroid/net/Uri;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ",mCurrentVideoFilename="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lcom/android/camera/actor/VideoActor;->mCurrentVideoFilename:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/android/camera/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    :cond_2
    return-void

    :cond_3
    const/4 v2, 0x0

    goto :goto_0

    :catchall_0
    move-exception v2

    :try_start_2
    monitor-exit v3
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    :try_start_3
    throw v2
    :try_end_3
    .catch Ljava/lang/InterruptedException; {:try_start_3 .. :try_end_3} :catch_0

    :catch_0
    move-exception v0

    const-string v2, "VideoActor"

    const-string v3, "Got notify from onFileSaved"

    invoke-static {v2, v3, v0}, Lcom/android/camera/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_1
.end method

.method private backToLastModeIfNeed()Z
    .locals 4

    sget-boolean v1, Lcom/android/camera/actor/VideoActor;->LOG:Z

    if-eqz v1, :cond_0

    const-string v1, "VideoActor"

    const-string v2, "backToLastModeIfNeed()"

    invoke-static {v1, v2}, Lcom/android/camera/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    const/4 v0, 0x0

    iget-object v1, p0, Lcom/android/camera/actor/VideoActor;->mVideoContext:Lcom/android/camera/Camera;

    invoke-virtual {v1}, Lcom/android/camera/Camera;->isNonePickIntent()Z

    move-result v1

    if-eqz v1, :cond_4

    invoke-direct {p0}, Lcom/android/camera/actor/VideoActor;->effectActive()Z

    move-result v1

    if-nez v1, :cond_4

    invoke-direct {p0}, Lcom/android/camera/actor/VideoActor;->releaseVideoActor()V

    iget-object v1, p0, Lcom/android/camera/actor/VideoActor;->mWfdManager:Lcom/android/camera/WfdManagerLocal;

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/android/camera/actor/VideoActor;->mWfdManager:Lcom/android/camera/WfdManagerLocal;

    iget-object v2, p0, Lcom/android/camera/actor/VideoActor;->mWfdListener:Lcom/android/camera/WfdManagerLocal$Listener;

    invoke-virtual {v1, v2}, Lcom/android/camera/WfdManagerLocal;->removeListener(Lcom/android/camera/WfdManagerLocal$Listener;)Z

    :cond_1
    iget-object v1, p0, Lcom/android/camera/actor/VideoActor;->mVideoContext:Lcom/android/camera/Camera;

    new-instance v2, Lcom/android/camera/actor/VideoActor$11;

    invoke-direct {v2, p0}, Lcom/android/camera/actor/VideoActor$11;-><init>(Lcom/android/camera/actor/VideoActor;)V

    invoke-virtual {v1, v2}, Landroid/app/Activity;->runOnUiThread(Ljava/lang/Runnable;)V

    const/4 v0, 0x1

    :cond_2
    :goto_0
    sget-boolean v1, Lcom/android/camera/actor/VideoActor;->LOG:Z

    if-eqz v1, :cond_3

    const-string v1, "VideoActor"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "backToLastModeIfNeed() return "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/android/camera/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    :cond_3
    return v0

    :cond_4
    iget-object v1, p0, Lcom/android/camera/actor/VideoActor;->mVideoContext:Lcom/android/camera/Camera;

    invoke-virtual {v1}, Lcom/android/camera/Camera;->isVideoCaptureIntent()Z

    move-result v1

    if-nez v1, :cond_5

    iget-object v1, p0, Lcom/android/camera/actor/VideoActor;->mVideoContext:Lcom/android/camera/Camera;

    invoke-virtual {v1}, Lcom/android/camera/Camera;->isVideoWallPaperIntent()Z

    move-result v1

    if-eqz v1, :cond_2

    :cond_5
    iget-object v1, p0, Lcom/android/camera/actor/VideoActor;->mVideoContext:Lcom/android/camera/Camera;

    new-instance v2, Lcom/android/camera/actor/VideoActor$12;

    invoke-direct {v2, p0}, Lcom/android/camera/actor/VideoActor$12;-><init>(Lcom/android/camera/actor/VideoActor;)V

    invoke-virtual {v1, v2}, Landroid/app/Activity;->runOnUiThread(Ljava/lang/Runnable;)V

    goto :goto_0
.end method

.method private backToLastTheseCase()V
    .locals 1

    iget-object v0, p0, Lcom/android/camera/actor/VideoActor;->mRecordingView:Lcom/android/camera/manager/RecordingView;

    invoke-virtual {v0}, Lcom/android/camera/manager/RecordingView;->hide()V

    iget-object v0, p0, Lcom/android/camera/actor/VideoActor;->mVideoContext:Lcom/android/camera/Camera;

    invoke-virtual {v0}, Lcom/android/camera/Camera;->restoreViewState()V

    invoke-direct {p0}, Lcom/android/camera/actor/VideoActor;->backToLastModeIfNeed()Z

    return-void
.end method

.method private changeFocusState()V
    .locals 3

    const/4 v2, 0x0

    sget-boolean v0, Lcom/android/camera/actor/VideoActor;->LOG:Z

    if-eqz v0, :cond_0

    const-string v0, "VideoActor"

    const-string v1, "changeFocusState()"

    invoke-static {v0, v1}, Lcom/android/camera/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    iget-object v0, p0, Lcom/android/camera/actor/VideoActor;->mVideoContext:Lcom/android/camera/Camera;

    invoke-virtual {v0}, Lcom/android/camera/Camera;->getCameraDevice()Lcom/android/camera/CameraManager$CameraProxy;

    move-result-object v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/android/camera/actor/VideoActor;->mVideoContext:Lcom/android/camera/Camera;

    invoke-virtual {v0}, Lcom/android/camera/Camera;->getCameraDevice()Lcom/android/camera/CameraManager$CameraProxy;

    move-result-object v0

    invoke-virtual {v0}, Lcom/android/camera/CameraManager$CameraProxy;->cancelAutoFocus()V

    :cond_1
    iput-boolean v2, p0, Lcom/android/camera/actor/VideoActor;->mSingleStartRecording:Z

    iput-boolean v2, p0, Lcom/android/camera/actor/VideoActor;->mIsAutoFocusCallback:Z

    iget-object v0, p0, Lcom/android/camera/actor/VideoActor;->mVideoContext:Lcom/android/camera/Camera;

    invoke-virtual {v0}, Lcom/android/camera/Camera;->getFocusManager()Lcom/android/camera/FocusManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/android/camera/FocusManager;->resetTouchFocus()V

    invoke-virtual {p0}, Lcom/android/camera/actor/VideoActor;->setFocusParameters()V

    iget-object v0, p0, Lcom/android/camera/actor/VideoActor;->mVideoContext:Lcom/android/camera/Camera;

    invoke-virtual {v0}, Lcom/android/camera/Camera;->getFocusManager()Lcom/android/camera/FocusManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/android/camera/FocusManager;->updateFocusUI()V

    return-void
.end method

.method private cleanupEmptyFile()V
    .locals 5

    iget-object v1, p0, Lcom/android/camera/actor/VideoActor;->mVideoFilename:Ljava/lang/String;

    if-eqz v1, :cond_1

    new-instance v0, Ljava/io/File;

    iget-object v1, p0, Lcom/android/camera/actor/VideoActor;->mVideoFilename:Ljava/lang/String;

    invoke-direct {v0, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/io/File;->length()J

    move-result-wide v1

    const-wide/16 v3, 0x0

    cmp-long v1, v1, v3

    if-nez v1, :cond_1

    invoke-virtual {v0}, Ljava/io/File;->delete()Z

    move-result v1

    if-eqz v1, :cond_1

    sget-boolean v1, Lcom/android/camera/actor/VideoActor;->LOG:Z

    if-eqz v1, :cond_0

    const-string v1, "VideoActor"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Empty video file deleted: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/android/camera/actor/VideoActor;->mVideoFilename:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/android/camera/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/android/camera/actor/VideoActor;->mVideoFilename:Ljava/lang/String;

    :cond_1
    return-void
.end method

.method private closeEffects(Z)V
    .locals 3
    .param p1    # Z

    if-eqz p1, :cond_1

    sget-boolean v0, Lcom/android/camera/actor/VideoActor;->LOG:Z

    if-eqz v0, :cond_0

    const-string v0, "VideoActor"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Closing effects,mEffectsRecorder = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/android/camera/actor/VideoActor;->mEffectsRecorder:Lcom/android/camera/actor/EffectsRecorder;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/android/camera/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    const/4 v0, 0x0

    iput v0, p0, Lcom/android/camera/actor/VideoActor;->mEffectType:I

    iget-object v0, p0, Lcom/android/camera/actor/VideoActor;->mEffectsRecorder:Lcom/android/camera/actor/EffectsRecorder;

    if-nez v0, :cond_2

    :cond_1
    :goto_0
    return-void

    :cond_2
    iget-object v0, p0, Lcom/android/camera/actor/VideoActor;->mEffectsRecorder:Lcom/android/camera/actor/EffectsRecorder;

    invoke-virtual {v0}, Lcom/android/camera/actor/EffectsRecorder;->release()V

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/android/camera/actor/VideoActor;->mEffectsRecorder:Lcom/android/camera/actor/EffectsRecorder;

    goto :goto_0
.end method

.method private closeVideoFileDescriptor()V
    .locals 3

    iget-object v1, p0, Lcom/android/camera/actor/VideoActor;->mVideoFileDescriptor:Landroid/os/ParcelFileDescriptor;

    if-eqz v1, :cond_0

    :try_start_0
    iget-object v1, p0, Lcom/android/camera/actor/VideoActor;->mVideoFileDescriptor:Landroid/os/ParcelFileDescriptor;

    invoke-virtual {v1}, Landroid/os/ParcelFileDescriptor;->close()V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/android/camera/actor/VideoActor;->mVideoFileDescriptor:Landroid/os/ParcelFileDescriptor;

    :cond_0
    return-void

    :catch_0
    move-exception v0

    const-string v1, "VideoActor"

    const-string v2, "Fail to close fd"

    invoke-static {v1, v2, v0}, Lcom/android/camera/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0
.end method

.method private computeDuration()J
    .locals 5

    invoke-direct {p0}, Lcom/android/camera/actor/VideoActor;->getDuration()J

    move-result-wide v0

    sget-boolean v2, Lcom/android/camera/actor/VideoActor;->LOG:Z

    if-eqz v2, :cond_0

    const-string v2, "VideoActor"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "computeDuration() return "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/android/camera/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    return-wide v0
.end method

.method private convertOutputFormatToFileExt(I)Ljava/lang/String;
    .locals 1
    .param p1    # I

    const/4 v0, 0x2

    if-ne p1, v0, :cond_0

    const-string v0, ".mp4"

    :goto_0
    return-object v0

    :cond_0
    const-string v0, ".3gp"

    goto :goto_0
.end method

.method private convertOutputFormatToMimeType(I)Ljava/lang/String;
    .locals 1
    .param p1    # I

    const/4 v0, 0x2

    if-ne p1, v0, :cond_0

    const-string v0, "video/mp4"

    :goto_0
    return-object v0

    :cond_0
    const-string v0, "video/3gpp"

    goto :goto_0
.end method

.method private deleteCurrentVideo()V
    .locals 4

    const/4 v3, 0x0

    sget-boolean v0, Lcom/android/camera/actor/VideoActor;->LOG:Z

    if-eqz v0, :cond_0

    const-string v0, "VideoActor"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "deleteCurrentVideo() mCurrentVideoFilename="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/android/camera/actor/VideoActor;->mCurrentVideoFilename:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/android/camera/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    iget-object v0, p0, Lcom/android/camera/actor/VideoActor;->mCurrentVideoFilename:Ljava/lang/String;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/android/camera/actor/VideoActor;->mCurrentVideoFilename:Ljava/lang/String;

    invoke-direct {p0, v0}, Lcom/android/camera/actor/VideoActor;->deleteVideoFile(Ljava/lang/String;)V

    iput-object v3, p0, Lcom/android/camera/actor/VideoActor;->mCurrentVideoFilename:Ljava/lang/String;

    iget-object v0, p0, Lcom/android/camera/actor/VideoActor;->mCurrentVideoUri:Landroid/net/Uri;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/android/camera/actor/VideoActor;->mContentResolver:Landroid/content/ContentResolver;

    iget-object v1, p0, Lcom/android/camera/actor/VideoActor;->mCurrentVideoUri:Landroid/net/Uri;

    invoke-virtual {v0, v1, v3, v3}, Landroid/content/ContentResolver;->delete(Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)I

    iput-object v3, p0, Lcom/android/camera/actor/VideoActor;->mCurrentVideoUri:Landroid/net/Uri;

    :cond_1
    return-void
.end method

.method private deleteVideoFile(Ljava/lang/String;)V
    .locals 4
    .param p1    # Ljava/lang/String;

    new-instance v0, Ljava/io/File;

    invoke-direct {v0, p1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/io/File;->delete()Z

    move-result v1

    if-nez v1, :cond_0

    sget-boolean v1, Lcom/android/camera/actor/VideoActor;->LOG:Z

    if-eqz v1, :cond_0

    const-string v1, "VideoActor"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Could not delete "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/android/camera/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    return-void
.end method

.method private doReturnToCaller(Z)V
    .locals 5
    .param p1    # Z

    sget-boolean v2, Lcom/android/camera/actor/VideoActor;->LOG:Z

    if-eqz v2, :cond_0

    const-string v2, "VideoActor"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "doReturnToCaller("

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ")"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/android/camera/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    new-instance v1, Landroid/content/Intent;

    invoke-direct {v1}, Landroid/content/Intent;-><init>()V

    if-eqz p1, :cond_2

    const/4 v0, -0x1

    iget-object v2, p0, Lcom/android/camera/actor/VideoActor;->mCurrentVideoUri:Landroid/net/Uri;

    invoke-virtual {v1, v2}, Landroid/content/Intent;->setData(Landroid/net/Uri;)Landroid/content/Intent;

    iget-object v2, p0, Lcom/android/camera/actor/VideoActor;->mVideoContext:Lcom/android/camera/Camera;

    invoke-virtual {v2}, Lcom/android/camera/Camera;->isVideoWallPaperIntent()Z

    move-result v2

    if-eqz v2, :cond_1

    iget-object v2, p0, Lcom/android/camera/actor/VideoActor;->mCurrentVideoUri:Landroid/net/Uri;

    invoke-static {v2}, Lcom/android/camera/Util;->setLastUri(Landroid/net/Uri;)V

    :cond_1
    :goto_0
    iget-object v2, p0, Lcom/android/camera/actor/VideoActor;->mVideoContext:Lcom/android/camera/Camera;

    invoke-virtual {v2, v0, v1}, Lcom/android/camera/Camera;->setResultExAndFinish(ILandroid/content/Intent;)V

    return-void

    :cond_2
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private effectActive()Z
    .locals 1

    iget-object v0, p0, Lcom/android/camera/actor/VideoActor;->mVideoContext:Lcom/android/camera/Camera;

    invoke-virtual {v0}, Lcom/android/camera/Camera;->getEffectType()I

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private generateVideoFilename(I)V
    .locals 5
    .param p1    # I

    const-string v1, "videorecorder"

    const/4 v2, 0x1

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-direct {p0, p1}, Lcom/android/camera/actor/VideoActor;->convertOutputFormatToFileExt(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static {}, Lcom/android/camera/Storage;->getFileDirectory()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const/16 v4, 0x2f

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ".tmp"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    iput-object v3, p0, Lcom/android/camera/actor/VideoActor;->mVideoTempPath:Ljava/lang/String;

    iget-object v3, p0, Lcom/android/camera/actor/VideoActor;->mVideoTempPath:Ljava/lang/String;

    iput-object v3, p0, Lcom/android/camera/actor/VideoActor;->mVideoFilename:Ljava/lang/String;

    return-void
.end method

.method private getDuration()J
    .locals 4

    new-instance v1, Landroid/media/MediaMetadataRetriever;

    invoke-direct {v1}, Landroid/media/MediaMetadataRetriever;-><init>()V

    :try_start_0
    iget-object v2, p0, Lcom/android/camera/actor/VideoActor;->mCurrentVideoFilename:Ljava/lang/String;

    invoke-virtual {v1, v2}, Landroid/media/MediaMetadataRetriever;->setDataSource(Ljava/lang/String;)V

    const/16 v2, 0x9

    invoke-virtual {v1, v2}, Landroid/media/MediaMetadataRetriever;->extractMetadata(I)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Ljava/lang/Long;->valueOf(Ljava/lang/String;)Ljava/lang/Long;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Long;->longValue()J
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/RuntimeException; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-wide v2

    :goto_0
    invoke-virtual {v1}, Landroid/media/MediaMetadataRetriever;->release()V

    return-wide v2

    :catch_0
    move-exception v0

    const-wide/16 v2, -0x1

    goto :goto_0

    :catch_1
    move-exception v0

    const-wide/16 v2, -0xc

    goto :goto_0

    :catchall_0
    move-exception v2

    invoke-virtual {v1}, Landroid/media/MediaMetadataRetriever;->release()V

    throw v2
.end method

.method private getRecordMode(Ljava/lang/String;)I
    .locals 4
    .param p1    # Ljava/lang/String;

    const/4 v0, 0x0

    iget-boolean v1, p0, Lcom/android/camera/actor/VideoActor;->mRecordAudio:Z

    if-eqz v1, :cond_3

    sget-object v1, Lcom/android/camera/actor/VideoActor;->PREF_CAMERA_VIDEO_HD_RECORDING_ENTRYVALUES:[Ljava/lang/String;

    const/4 v2, 0x0

    aget-object v1, v1, v2

    invoke-virtual {p1, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    const/4 v0, 0x0

    :goto_0
    sget-boolean v1, Lcom/android/camera/actor/VideoActor;->LOG:Z

    if-eqz v1, :cond_0

    const-string v1, "VideoActor"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "getRecordMode("

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ") return "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/android/camera/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    return v0

    :cond_1
    sget-object v1, Lcom/android/camera/actor/VideoActor;->PREF_CAMERA_VIDEO_HD_RECORDING_ENTRYVALUES:[Ljava/lang/String;

    const/4 v2, 0x1

    aget-object v1, v1, v2

    invoke-virtual {p1, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    const/4 v0, 0x1

    goto :goto_0

    :cond_2
    const/4 v0, 0x2

    goto :goto_0

    :cond_3
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private getRequestedSizeLimit()V
    .locals 4

    invoke-direct {p0}, Lcom/android/camera/actor/VideoActor;->closeVideoFileDescriptor()V

    iget-object v2, p0, Lcom/android/camera/actor/VideoActor;->mVideoContext:Lcom/android/camera/Camera;

    invoke-virtual {v2}, Lcom/android/camera/Camera;->isNonePickIntent()Z

    move-result v2

    if-nez v2, :cond_1

    iget-object v2, p0, Lcom/android/camera/actor/VideoActor;->mVideoContext:Lcom/android/camera/Camera;

    invoke-virtual {v2}, Lcom/android/camera/Camera;->getSaveUri()Landroid/net/Uri;

    move-result-object v1

    if-eqz v1, :cond_0

    :try_start_0
    iget-object v2, p0, Lcom/android/camera/actor/VideoActor;->mContentResolver:Landroid/content/ContentResolver;

    const-string v3, "rw"

    invoke-virtual {v2, v1, v3}, Landroid/content/ContentResolver;->openFileDescriptor(Landroid/net/Uri;Ljava/lang/String;)Landroid/os/ParcelFileDescriptor;

    move-result-object v2

    iput-object v2, p0, Lcom/android/camera/actor/VideoActor;->mVideoFileDescriptor:Landroid/os/ParcelFileDescriptor;

    iput-object v1, p0, Lcom/android/camera/actor/VideoActor;->mCurrentVideoUri:Landroid/net/Uri;
    :try_end_0
    .catch Ljava/io/FileNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    :cond_0
    :goto_0
    iget-object v2, p0, Lcom/android/camera/actor/VideoActor;->mVideoContext:Lcom/android/camera/Camera;

    invoke-virtual {v2}, Lcom/android/camera/Camera;->getLimitedSize()J

    move-result-wide v2

    iput-wide v2, p0, Lcom/android/camera/actor/VideoActor;->mRequestedSizeLimit:J

    :cond_1
    return-void

    :catch_0
    move-exception v0

    const-string v2, "VideoActor"

    const-string v3, "initializeNormalRecorder()"

    invoke-static {v2, v3, v0}, Lcom/android/camera/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0
.end method

.method private getTimeLapseVideoLength(J)J
    .locals 6
    .param p1    # J

    long-to-double v2, p1

    iget v4, p0, Lcom/android/camera/actor/VideoActor;->mTimeBetweenTimeLapseFrameCaptureMs:I

    int-to-double v4, v4

    div-double v0, v2, v4

    iget-object v2, p0, Lcom/android/camera/actor/VideoActor;->mProfile:Landroid/media/CamcorderProfile;

    iget v2, v2, Landroid/media/CamcorderProfile;->videoFrameRate:I

    int-to-double v2, v2

    div-double v2, v0, v2

    const-wide v4, 0x408f400000000000L

    mul-double/2addr v2, v4

    double-to-long v2, v2

    return-wide v2
.end method

.method private hideOtherSettings(Z)V
    .locals 2
    .param p1    # Z

    if-eqz p1, :cond_0

    iget-object v0, p0, Lcom/android/camera/actor/VideoActor;->mVideoContext:Lcom/android/camera/Camera;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/android/camera/Camera;->setViewState(I)V

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/android/camera/actor/VideoActor;->mVideoContext:Lcom/android/camera/Camera;

    invoke-virtual {v0}, Lcom/android/camera/Camera;->restoreViewState()V

    goto :goto_0
.end method

.method private initVideoRecordingFirst()V
    .locals 4

    iget-object v1, p0, Lcom/android/camera/actor/VideoActor;->mVideoContext:Lcom/android/camera/Camera;

    invoke-virtual {v1}, Landroid/content/ContextWrapper;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    iput-object v1, p0, Lcom/android/camera/actor/VideoActor;->mContentResolver:Landroid/content/ContentResolver;

    iget-object v1, p0, Lcom/android/camera/actor/VideoActor;->mVideoContext:Lcom/android/camera/Camera;

    invoke-virtual {v1}, Lcom/android/camera/Camera;->getFocusManager()Lcom/android/camera/FocusManager;

    move-result-object v1

    invoke-virtual {v1}, Lcom/android/camera/FocusManager;->getContinousFocusSupported()Z

    move-result v1

    iput-boolean v1, p0, Lcom/android/camera/actor/VideoActor;->mIsContinousFocusMode:Z

    iget-object v1, p0, Lcom/android/camera/actor/VideoActor;->mVideoContext:Lcom/android/camera/Camera;

    invoke-virtual {v1}, Lcom/android/camera/Camera;->getTimelapseMs()I

    move-result v1

    iput v1, p0, Lcom/android/camera/actor/VideoActor;->mTimeBetweenTimeLapseFrameCaptureMs:I

    iget v1, p0, Lcom/android/camera/actor/VideoActor;->mTimeBetweenTimeLapseFrameCaptureMs:I

    if-eqz v1, :cond_1

    const/4 v1, 0x1

    :goto_0
    iput-boolean v1, p0, Lcom/android/camera/actor/VideoActor;->mCaptureTimeLapse:Z

    iget-object v1, p0, Lcom/android/camera/actor/VideoActor;->mVideoContext:Lcom/android/camera/Camera;

    invoke-virtual {v1}, Lcom/android/camera/Camera;->getMicrophone()Z

    move-result v1

    iput-boolean v1, p0, Lcom/android/camera/actor/VideoActor;->mRecordAudio:Z

    invoke-static {}, Lcom/android/camera/FeatureSwitcher;->isContinuousFocusEnabledWhenTouch()Z

    move-result v1

    iput-boolean v1, p0, Lcom/android/camera/actor/VideoActor;->mSingleAutoModeSupported:Z

    iget-object v1, p0, Lcom/android/camera/actor/VideoActor;->mVideoContext:Lcom/android/camera/Camera;

    invoke-virtual {v1}, Lcom/android/camera/Camera;->getLimitedDuration()I

    move-result v0

    mul-int/lit16 v1, v0, 0x3e8

    iput v1, p0, Lcom/android/camera/actor/VideoActor;->mMaxVideoDurationInMs:I

    iget-object v1, p0, Lcom/android/camera/actor/VideoActor;->mVideoContext:Lcom/android/camera/Camera;

    new-instance v2, Lcom/android/camera/actor/VideoActor$7;

    invoke-direct {v2, p0}, Lcom/android/camera/actor/VideoActor$7;-><init>(Lcom/android/camera/actor/VideoActor;)V

    invoke-virtual {v1, v2}, Landroid/app/Activity;->runOnUiThread(Ljava/lang/Runnable;)V

    sget-boolean v1, Lcom/android/camera/actor/VideoActor;->LOG:Z

    if-eqz v1, :cond_0

    const-string v1, "VideoActor"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "initVideoRecordingFirst,mIsContinousFocusMode ="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-boolean v3, p0, Lcom/android/camera/actor/VideoActor;->mIsContinousFocusMode:Z

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ",mTimeBetweenTimeLapseFrameCaptureMs ="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget v3, p0, Lcom/android/camera/actor/VideoActor;->mTimeBetweenTimeLapseFrameCaptureMs:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ",mRecordAudio = "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-boolean v3, p0, Lcom/android/camera/actor/VideoActor;->mRecordAudio:Z

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ",mSingleAutoModeSupported = "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-boolean v3, p0, Lcom/android/camera/actor/VideoActor;->mSingleAutoModeSupported:Z

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ",mMaxVideoDurationInMs ="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget v3, p0, Lcom/android/camera/actor/VideoActor;->mMaxVideoDurationInMs:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/android/camera/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    return-void

    :cond_1
    const/4 v1, 0x0

    goto :goto_0
.end method

.method private initializeEffectsPreview()V
    .locals 6

    sget-boolean v2, Lcom/android/camera/actor/VideoActor;->LOG:Z

    if-eqz v2, :cond_0

    const-string v2, "VideoActor"

    const-string v3, "initializeEffectsPreview"

    invoke-static {v2, v3}, Lcom/android/camera/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    iget-object v2, p0, Lcom/android/camera/actor/VideoActor;->mVideoContext:Lcom/android/camera/Camera;

    invoke-virtual {v2}, Lcom/android/camera/Camera;->getCameraDevice()Lcom/android/camera/CameraManager$CameraProxy;

    move-result-object v2

    if-nez v2, :cond_1

    :goto_0
    return-void

    :cond_1
    invoke-static {}, Lcom/android/camera/CameraHolder;->instance()Lcom/android/camera/CameraHolder;

    move-result-object v2

    invoke-virtual {v2}, Lcom/android/camera/CameraHolder;->getCameraInfo()[Landroid/hardware/Camera$CameraInfo;

    move-result-object v2

    iget-object v3, p0, Lcom/android/camera/actor/VideoActor;->mVideoContext:Lcom/android/camera/Camera;

    invoke-virtual {v3}, Lcom/android/camera/Camera;->getCameraId()I

    move-result v3

    aget-object v0, v2, v3

    const/4 v2, 0x0

    iput-boolean v2, p0, Lcom/android/camera/actor/VideoActor;->mEffectsDisplayResult:Z

    new-instance v2, Lcom/android/camera/actor/EffectsRecorder;

    iget-object v3, p0, Lcom/android/camera/actor/VideoActor;->mVideoContext:Lcom/android/camera/Camera;

    invoke-direct {v2, v3}, Lcom/android/camera/actor/EffectsRecorder;-><init>(Landroid/content/Context;)V

    iput-object v2, p0, Lcom/android/camera/actor/VideoActor;->mEffectsRecorder:Lcom/android/camera/actor/EffectsRecorder;

    iget-object v2, p0, Lcom/android/camera/actor/VideoActor;->mEffectsRecorder:Lcom/android/camera/actor/EffectsRecorder;

    iget-object v3, p0, Lcom/android/camera/actor/VideoActor;->mVideoContext:Lcom/android/camera/Camera;

    invoke-virtual {v3}, Lcom/android/camera/Camera;->getCameraDisplayOrientation()I

    move-result v3

    invoke-virtual {v2, v3}, Lcom/android/camera/actor/EffectsRecorder;->setCameraDisplayOrientation(I)V

    invoke-static {}, Lcom/mediatek/camera/FrameworksClassFactory;->isMockCamera()Z

    move-result v2

    if-nez v2, :cond_2

    iget-object v2, p0, Lcom/android/camera/actor/VideoActor;->mEffectsRecorder:Lcom/android/camera/actor/EffectsRecorder;

    iget-object v3, p0, Lcom/android/camera/actor/VideoActor;->mVideoContext:Lcom/android/camera/Camera;

    invoke-virtual {v3}, Lcom/android/camera/Camera;->getCameraDevice()Lcom/android/camera/CameraManager$CameraProxy;

    move-result-object v3

    invoke-virtual {v3}, Lcom/android/camera/CameraManager$CameraProxy;->getCamera()Lcom/mediatek/camera/ICamera;

    move-result-object v3

    invoke-interface {v3}, Lcom/mediatek/camera/ICamera;->getInstance()Landroid/hardware/Camera;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/android/camera/actor/EffectsRecorder;->setCamera(Landroid/hardware/Camera;)V

    :cond_2
    iget-object v2, p0, Lcom/android/camera/actor/VideoActor;->mEffectsRecorder:Lcom/android/camera/actor/EffectsRecorder;

    iget v3, v0, Landroid/hardware/Camera$CameraInfo;->facing:I

    invoke-virtual {v2, v3}, Lcom/android/camera/actor/EffectsRecorder;->setCameraFacing(I)V

    iget-object v2, p0, Lcom/android/camera/actor/VideoActor;->mEffectsRecorder:Lcom/android/camera/actor/EffectsRecorder;

    iget-object v3, p0, Lcom/android/camera/actor/VideoActor;->mProfile:Landroid/media/CamcorderProfile;

    invoke-virtual {v2, v3}, Lcom/android/camera/actor/EffectsRecorder;->setProfile(Landroid/media/CamcorderProfile;)V

    iget-object v2, p0, Lcom/android/camera/actor/VideoActor;->mEffectsRecorder:Lcom/android/camera/actor/EffectsRecorder;

    invoke-virtual {v2, p0}, Lcom/android/camera/actor/EffectsRecorder;->setEffectsListener(Lcom/android/camera/actor/EffectsRecorder$EffectsListener;)V

    iget-object v2, p0, Lcom/android/camera/actor/VideoActor;->mEffectsRecorder:Lcom/android/camera/actor/EffectsRecorder;

    invoke-virtual {v2, p0}, Lcom/android/camera/actor/EffectsRecorder;->setOnInfoListener(Landroid/media/MediaRecorder$OnInfoListener;)V

    iget-object v2, p0, Lcom/android/camera/actor/VideoActor;->mEffectsRecorder:Lcom/android/camera/actor/EffectsRecorder;

    invoke-virtual {v2, p0}, Lcom/android/camera/actor/EffectsRecorder;->setOnCameraReleasedListener(Landroid/media/MediaRecorder$OnInfoListener;)V

    iget-object v2, p0, Lcom/android/camera/actor/VideoActor;->mEffectsRecorder:Lcom/android/camera/actor/EffectsRecorder;

    invoke-virtual {v2, p0}, Lcom/android/camera/actor/EffectsRecorder;->setOnErrorListener(Landroid/media/MediaRecorder$OnErrorListener;)V

    iget-object v2, p0, Lcom/android/camera/actor/VideoActor;->mEffectsRecorder:Lcom/android/camera/actor/EffectsRecorder;

    invoke-virtual {v2, p0}, Lcom/android/camera/actor/EffectsRecorder;->setSurfaceStateListener(Lcom/android/camera/actor/EffectsRecorder$OnSurfaceStateChangeListener;)V

    const/4 v1, 0x0

    iget-object v2, p0, Lcom/android/camera/actor/VideoActor;->mVideoContext:Lcom/android/camera/Camera;

    invoke-virtual {v2}, Lcom/android/camera/Camera;->getOrietation()I

    move-result v2

    iput v2, p0, Lcom/android/camera/actor/VideoActor;->mOrientation:I

    iget v2, p0, Lcom/android/camera/actor/VideoActor;->mOrientation:I

    const/4 v3, -0x1

    if-eq v2, v3, :cond_3

    iget v1, p0, Lcom/android/camera/actor/VideoActor;->mOrientation:I

    :cond_3
    iget-object v2, p0, Lcom/android/camera/actor/VideoActor;->mEffectsRecorder:Lcom/android/camera/actor/EffectsRecorder;

    invoke-virtual {v2, v1}, Lcom/android/camera/actor/EffectsRecorder;->setOrientationHint(I)V

    iget-object v2, p0, Lcom/android/camera/actor/VideoActor;->mVideoContext:Lcom/android/camera/Camera;

    iget-object v3, p0, Lcom/android/camera/actor/VideoActor;->mVideoContext:Lcom/android/camera/Camera;

    invoke-virtual {v3}, Lcom/android/camera/Camera;->getOrientationCompensation()I

    move-result v3

    invoke-virtual {v2, v3}, Lcom/android/camera/Camera;->setReviewOrientationCompensation(I)V

    iget-object v2, p0, Lcom/android/camera/actor/VideoActor;->mEffectsRecorder:Lcom/android/camera/actor/EffectsRecorder;

    iget-object v3, p0, Lcom/android/camera/actor/VideoActor;->mVideoContext:Lcom/android/camera/Camera;

    invoke-virtual {v3}, Lcom/android/camera/Camera;->getSurfaceTexture()Landroid/graphics/SurfaceTexture;

    move-result-object v3

    iget-object v4, p0, Lcom/android/camera/actor/VideoActor;->mVideoContext:Lcom/android/camera/Camera;

    invoke-virtual {v4}, Lcom/android/camera/Camera;->getCameraScreenNailWidth()I

    move-result v4

    iget-object v5, p0, Lcom/android/camera/actor/VideoActor;->mVideoContext:Lcom/android/camera/Camera;

    invoke-virtual {v5}, Lcom/android/camera/Camera;->getCameraScreenNailHeight()I

    move-result v5

    invoke-virtual {v2, v3, v4, v5}, Lcom/android/camera/actor/EffectsRecorder;->setPreviewSurfaceTexture(Landroid/graphics/SurfaceTexture;II)V

    iget v2, p0, Lcom/android/camera/actor/VideoActor;->mEffectType:I

    const/4 v3, 0x2

    if-ne v2, v3, :cond_4

    iget-object v2, p0, Lcom/android/camera/actor/VideoActor;->mEffectParameter:Ljava/lang/Object;

    check-cast v2, Ljava/lang/String;

    const-string v3, "gallery"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_4

    iget-object v2, p0, Lcom/android/camera/actor/VideoActor;->mEffectsRecorder:Lcom/android/camera/actor/EffectsRecorder;

    iget v3, p0, Lcom/android/camera/actor/VideoActor;->mEffectType:I

    iget-object v4, p0, Lcom/android/camera/actor/VideoActor;->mEffectUriFromGallery:Ljava/lang/String;

    invoke-virtual {v2, v3, v4}, Lcom/android/camera/actor/EffectsRecorder;->setEffect(ILjava/lang/Object;)V

    goto/16 :goto_0

    :cond_4
    iget-object v2, p0, Lcom/android/camera/actor/VideoActor;->mEffectsRecorder:Lcom/android/camera/actor/EffectsRecorder;

    iget v3, p0, Lcom/android/camera/actor/VideoActor;->mEffectType:I

    iget-object v4, p0, Lcom/android/camera/actor/VideoActor;->mEffectParameter:Ljava/lang/Object;

    invoke-virtual {v2, v3, v4}, Lcom/android/camera/actor/EffectsRecorder;->setEffect(ILjava/lang/Object;)V

    goto/16 :goto_0
.end method

.method private initializeEffectsRecording()V
    .locals 7

    sget-boolean v2, Lcom/android/camera/actor/VideoActor;->LOG:Z

    if-eqz v2, :cond_0

    const-string v2, "VideoActor"

    const-string v3, "initializeEffectsRecording"

    invoke-static {v2, v3}, Lcom/android/camera/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    invoke-direct {p0}, Lcom/android/camera/actor/VideoActor;->getRequestedSizeLimit()V

    iget-object v2, p0, Lcom/android/camera/actor/VideoActor;->mEffectsRecorder:Lcom/android/camera/actor/EffectsRecorder;

    iget-object v3, p0, Lcom/android/camera/actor/VideoActor;->mProfile:Landroid/media/CamcorderProfile;

    invoke-virtual {v2, v3}, Lcom/android/camera/actor/EffectsRecorder;->setProfile(Landroid/media/CamcorderProfile;)V

    iget-object v3, p0, Lcom/android/camera/actor/VideoActor;->mEffectsRecorder:Lcom/android/camera/actor/EffectsRecorder;

    iget-boolean v2, p0, Lcom/android/camera/actor/VideoActor;->mRecordAudio:Z

    if-nez v2, :cond_2

    const/4 v2, 0x1

    :goto_0
    invoke-virtual {v3, v2}, Lcom/android/camera/actor/EffectsRecorder;->setMuteAudio(Z)V

    iget-boolean v2, p0, Lcom/android/camera/actor/VideoActor;->mCaptureTimeLapse:Z

    if-eqz v2, :cond_3

    iget-object v2, p0, Lcom/android/camera/actor/VideoActor;->mEffectsRecorder:Lcom/android/camera/actor/EffectsRecorder;

    const-wide v3, 0x408f400000000000L

    iget v5, p0, Lcom/android/camera/actor/VideoActor;->mTimeBetweenTimeLapseFrameCaptureMs:I

    int-to-double v5, v5

    div-double/2addr v3, v5

    invoke-virtual {v2, v3, v4}, Lcom/android/camera/actor/EffectsRecorder;->setCaptureRate(D)V

    :goto_1
    iget-object v2, p0, Lcom/android/camera/actor/VideoActor;->mVideoFileDescriptor:Landroid/os/ParcelFileDescriptor;

    if-eqz v2, :cond_4

    iget-object v2, p0, Lcom/android/camera/actor/VideoActor;->mEffectsRecorder:Lcom/android/camera/actor/EffectsRecorder;

    iget-object v3, p0, Lcom/android/camera/actor/VideoActor;->mVideoFileDescriptor:Landroid/os/ParcelFileDescriptor;

    invoke-virtual {v3}, Landroid/os/ParcelFileDescriptor;->getFileDescriptor()Ljava/io/FileDescriptor;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/android/camera/actor/EffectsRecorder;->setOutputFile(Ljava/io/FileDescriptor;)V

    :goto_2
    invoke-static {}, Lcom/android/camera/Storage;->getAvailableSpace()J

    move-result-wide v2

    sget-wide v4, Lcom/android/camera/Storage;->LOW_STORAGE_THRESHOLD:J

    sub-long v0, v2, v4

    iget-wide v2, p0, Lcom/android/camera/actor/VideoActor;->mRequestedSizeLimit:J

    const-wide/16 v4, 0x0

    cmp-long v2, v2, v4

    if-lez v2, :cond_1

    iget-wide v2, p0, Lcom/android/camera/actor/VideoActor;->mRequestedSizeLimit:J

    cmp-long v2, v2, v0

    if-gez v2, :cond_1

    iget-wide v0, p0, Lcom/android/camera/actor/VideoActor;->mRequestedSizeLimit:J

    :cond_1
    iget-object v2, p0, Lcom/android/camera/actor/VideoActor;->mEffectsRecorder:Lcom/android/camera/actor/EffectsRecorder;

    invoke-virtual {v2, v0, v1}, Lcom/android/camera/actor/EffectsRecorder;->setMaxFileSize(J)V

    iget-object v2, p0, Lcom/android/camera/actor/VideoActor;->mEffectsRecorder:Lcom/android/camera/actor/EffectsRecorder;

    iget v3, p0, Lcom/android/camera/actor/VideoActor;->mMaxVideoDurationInMs:I

    invoke-virtual {v2, v3}, Lcom/android/camera/actor/EffectsRecorder;->setMaxDuration(I)V

    return-void

    :cond_2
    const/4 v2, 0x0

    goto :goto_0

    :cond_3
    iget-object v2, p0, Lcom/android/camera/actor/VideoActor;->mEffectsRecorder:Lcom/android/camera/actor/EffectsRecorder;

    const-wide/16 v3, 0x0

    invoke-virtual {v2, v3, v4}, Lcom/android/camera/actor/EffectsRecorder;->setCaptureRate(D)V

    goto :goto_1

    :cond_4
    iget-object v2, p0, Lcom/android/camera/actor/VideoActor;->mProfile:Landroid/media/CamcorderProfile;

    iget v2, v2, Landroid/media/CamcorderProfile;->fileFormat:I

    invoke-direct {p0, v2}, Lcom/android/camera/actor/VideoActor;->generateVideoFilename(I)V

    iget-object v2, p0, Lcom/android/camera/actor/VideoActor;->mEffectsRecorder:Lcom/android/camera/actor/EffectsRecorder;

    iget-object v3, p0, Lcom/android/camera/actor/VideoActor;->mVideoFilename:Ljava/lang/String;

    invoke-virtual {v2, v3}, Lcom/android/camera/actor/EffectsRecorder;->setOutputFile(Ljava/lang/String;)V

    goto :goto_2
.end method

.method private initializeNormalRecorder()V
    .locals 15

    sget-boolean v10, Lcom/android/camera/actor/VideoActor;->LOG:Z

    if-eqz v10, :cond_0

    const-string v10, "VideoActor"

    const-string v11, "initializeNormalRecorder()"

    invoke-static {v10, v11}, Lcom/android/camera/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    invoke-direct {p0}, Lcom/android/camera/actor/VideoActor;->getRequestedSizeLimit()V

    invoke-static {}, Lcom/mediatek/camera/FrameworksClassFactory;->getMediaRecorder()Landroid/media/MediaRecorder;

    move-result-object v10

    iput-object v10, p0, Lcom/android/camera/actor/VideoActor;->mMediaRecorder:Landroid/media/MediaRecorder;

    iget-object v10, p0, Lcom/android/camera/actor/VideoActor;->mVideoContext:Lcom/android/camera/Camera;

    invoke-virtual {v10}, Lcom/android/camera/Camera;->getCameraDevice()Lcom/android/camera/CameraManager$CameraProxy;

    move-result-object v10

    invoke-virtual {v10}, Lcom/android/camera/CameraManager$CameraProxy;->unlock()V

    invoke-static {}, Lcom/mediatek/camera/FrameworksClassFactory;->isMockCamera()Z

    move-result v10

    if-nez v10, :cond_7

    iget-object v10, p0, Lcom/android/camera/actor/VideoActor;->mMediaRecorder:Landroid/media/MediaRecorder;

    iget-object v11, p0, Lcom/android/camera/actor/VideoActor;->mVideoContext:Lcom/android/camera/Camera;

    invoke-virtual {v11}, Lcom/android/camera/Camera;->getCameraDevice()Lcom/android/camera/CameraManager$CameraProxy;

    move-result-object v11

    invoke-virtual {v11}, Lcom/android/camera/CameraManager$CameraProxy;->getCamera()Lcom/mediatek/camera/ICamera;

    move-result-object v11

    invoke-interface {v11}, Lcom/mediatek/camera/ICamera;->getInstance()Landroid/hardware/Camera;

    move-result-object v11

    invoke-virtual {v10, v11}, Landroid/media/MediaRecorder;->setCamera(Landroid/hardware/Camera;)V

    :goto_0
    iget-boolean v10, p0, Lcom/android/camera/actor/VideoActor;->mCaptureTimeLapse:Z

    if-nez v10, :cond_1

    iget-boolean v10, p0, Lcom/android/camera/actor/VideoActor;->mRecordAudio:Z

    if-eqz v10, :cond_1

    iget-object v10, p0, Lcom/android/camera/actor/VideoActor;->mMediaRecorder:Landroid/media/MediaRecorder;

    const/4 v11, 0x5

    invoke-virtual {v10, v11}, Landroid/media/MediaRecorder;->setAudioSource(I)V

    :cond_1
    iget-object v10, p0, Lcom/android/camera/actor/VideoActor;->mMediaRecorder:Landroid/media/MediaRecorder;

    const/4 v11, 0x1

    invoke-virtual {v10, v11}, Landroid/media/MediaRecorder;->setVideoSource(I)V

    iget-object v10, p0, Lcom/android/camera/actor/VideoActor;->mMediaRecorder:Landroid/media/MediaRecorder;

    iget-object v11, p0, Lcom/android/camera/actor/VideoActor;->mProfile:Landroid/media/CamcorderProfile;

    iget v11, v11, Landroid/media/CamcorderProfile;->fileFormat:I

    invoke-virtual {v10, v11}, Landroid/media/MediaRecorder;->setOutputFormat(I)V

    iget-object v10, p0, Lcom/android/camera/actor/VideoActor;->mMediaRecorder:Landroid/media/MediaRecorder;

    iget-object v11, p0, Lcom/android/camera/actor/VideoActor;->mProfile:Landroid/media/CamcorderProfile;

    iget v11, v11, Landroid/media/CamcorderProfile;->videoFrameRate:I

    invoke-virtual {v10, v11}, Landroid/media/MediaRecorder;->setVideoFrameRate(I)V

    iget-object v10, p0, Lcom/android/camera/actor/VideoActor;->mMediaRecorder:Landroid/media/MediaRecorder;

    iget-object v11, p0, Lcom/android/camera/actor/VideoActor;->mProfile:Landroid/media/CamcorderProfile;

    iget v11, v11, Landroid/media/CamcorderProfile;->videoFrameWidth:I

    iget-object v12, p0, Lcom/android/camera/actor/VideoActor;->mProfile:Landroid/media/CamcorderProfile;

    iget v12, v12, Landroid/media/CamcorderProfile;->videoFrameHeight:I

    invoke-virtual {v10, v11, v12}, Landroid/media/MediaRecorder;->setVideoSize(II)V

    iget-object v10, p0, Lcom/android/camera/actor/VideoActor;->mMediaRecorder:Landroid/media/MediaRecorder;

    iget-object v11, p0, Lcom/android/camera/actor/VideoActor;->mProfile:Landroid/media/CamcorderProfile;

    iget v11, v11, Landroid/media/CamcorderProfile;->videoBitRate:I

    invoke-virtual {v10, v11}, Landroid/media/MediaRecorder;->setVideoEncodingBitRate(I)V

    iget-object v10, p0, Lcom/android/camera/actor/VideoActor;->mMediaRecorder:Landroid/media/MediaRecorder;

    iget-object v11, p0, Lcom/android/camera/actor/VideoActor;->mProfile:Landroid/media/CamcorderProfile;

    iget v11, v11, Landroid/media/CamcorderProfile;->videoCodec:I

    invoke-virtual {v10, v11}, Landroid/media/MediaRecorder;->setVideoEncoder(I)V

    iget-boolean v10, p0, Lcom/android/camera/actor/VideoActor;->mCaptureTimeLapse:Z

    if-nez v10, :cond_2

    iget-boolean v10, p0, Lcom/android/camera/actor/VideoActor;->mRecordAudio:Z

    if-eqz v10, :cond_2

    iget-object v10, p0, Lcom/android/camera/actor/VideoActor;->mMediaRecorder:Landroid/media/MediaRecorder;

    iget-object v11, p0, Lcom/android/camera/actor/VideoActor;->mProfile:Landroid/media/CamcorderProfile;

    iget v11, v11, Landroid/media/CamcorderProfile;->audioBitRate:I

    invoke-virtual {v10, v11}, Landroid/media/MediaRecorder;->setAudioEncodingBitRate(I)V

    iget-object v10, p0, Lcom/android/camera/actor/VideoActor;->mMediaRecorder:Landroid/media/MediaRecorder;

    iget-object v11, p0, Lcom/android/camera/actor/VideoActor;->mProfile:Landroid/media/CamcorderProfile;

    iget v11, v11, Landroid/media/CamcorderProfile;->audioChannels:I

    invoke-virtual {v10, v11}, Landroid/media/MediaRecorder;->setAudioChannels(I)V

    iget-object v10, p0, Lcom/android/camera/actor/VideoActor;->mMediaRecorder:Landroid/media/MediaRecorder;

    iget-object v11, p0, Lcom/android/camera/actor/VideoActor;->mProfile:Landroid/media/CamcorderProfile;

    iget v11, v11, Landroid/media/CamcorderProfile;->audioSampleRate:I

    invoke-virtual {v10, v11}, Landroid/media/MediaRecorder;->setAudioSamplingRate(I)V

    iget-object v10, p0, Lcom/android/camera/actor/VideoActor;->mMediaRecorder:Landroid/media/MediaRecorder;

    iget-object v11, p0, Lcom/android/camera/actor/VideoActor;->mProfile:Landroid/media/CamcorderProfile;

    iget v11, v11, Landroid/media/CamcorderProfile;->audioCodec:I

    invoke-virtual {v10, v11}, Landroid/media/MediaRecorder;->setAudioEncoder(I)V

    invoke-static {}, Lcom/android/camera/FeatureSwitcher;->isHdRecordingEnabled()Z

    move-result v10

    if-eqz v10, :cond_2

    iget-object v10, p0, Lcom/android/camera/actor/VideoActor;->mMediaRecorder:Landroid/media/MediaRecorder;

    iget-object v11, p0, Lcom/android/camera/actor/VideoActor;->mVideoContext:Lcom/android/camera/Camera;

    invoke-virtual {v11}, Lcom/android/camera/Camera;->getAudioMode()Ljava/lang/String;

    move-result-object v11

    invoke-direct {p0, v11}, Lcom/android/camera/actor/VideoActor;->getRecordMode(Ljava/lang/String;)I

    move-result v11

    const/4 v12, 0x1

    invoke-static {v10, v11, v12}, Lcom/mediatek/media/MediaRecorderEx;->setHDRecordMode(Landroid/media/MediaRecorder;IZ)V

    :cond_2
    iget-object v10, p0, Lcom/android/camera/actor/VideoActor;->mMediaRecorder:Landroid/media/MediaRecorder;

    iget v11, p0, Lcom/android/camera/actor/VideoActor;->mMaxVideoDurationInMs:I

    invoke-virtual {v10, v11}, Landroid/media/MediaRecorder;->setMaxDuration(I)V

    iget-boolean v10, p0, Lcom/android/camera/actor/VideoActor;->mCaptureTimeLapse:Z

    if-eqz v10, :cond_3

    iget-object v10, p0, Lcom/android/camera/actor/VideoActor;->mMediaRecorder:Landroid/media/MediaRecorder;

    const-wide v11, 0x408f400000000000L

    iget v13, p0, Lcom/android/camera/actor/VideoActor;->mTimeBetweenTimeLapseFrameCaptureMs:I

    int-to-double v13, v13

    div-double/2addr v11, v13

    invoke-virtual {v10, v11, v12}, Landroid/media/MediaRecorder;->setCaptureRate(D)V

    :cond_3
    iget-object v10, p0, Lcom/android/camera/actor/VideoActor;->mVideoFileDescriptor:Landroid/os/ParcelFileDescriptor;

    if-eqz v10, :cond_8

    iget-object v10, p0, Lcom/android/camera/actor/VideoActor;->mMediaRecorder:Landroid/media/MediaRecorder;

    iget-object v11, p0, Lcom/android/camera/actor/VideoActor;->mVideoFileDescriptor:Landroid/os/ParcelFileDescriptor;

    invoke-virtual {v11}, Landroid/os/ParcelFileDescriptor;->getFileDescriptor()Ljava/io/FileDescriptor;

    move-result-object v11

    invoke-virtual {v10, v11}, Landroid/media/MediaRecorder;->setOutputFile(Ljava/io/FileDescriptor;)V

    :goto_1
    const/4 v5, 0x0

    iget-object v10, p0, Lcom/android/camera/actor/VideoActor;->mVideoSaveRequest:Lcom/android/camera/SaveRequest;

    if-eqz v10, :cond_9

    iget-object v10, p0, Lcom/android/camera/actor/VideoActor;->mVideoSaveRequest:Lcom/android/camera/SaveRequest;

    invoke-interface {v10}, Lcom/android/camera/SaveRequest;->getLocation()Landroid/location/Location;

    move-result-object v5

    :goto_2
    if-eqz v5, :cond_4

    iget-object v10, p0, Lcom/android/camera/actor/VideoActor;->mMediaRecorder:Landroid/media/MediaRecorder;

    invoke-virtual {v5}, Landroid/location/Location;->getLatitude()D

    move-result-wide v11

    double-to-float v11, v11

    invoke-virtual {v5}, Landroid/location/Location;->getLongitude()D

    move-result-wide v12

    double-to-float v12, v12

    invoke-virtual {v10, v11, v12}, Landroid/media/MediaRecorder;->setLocation(FF)V

    :cond_4
    iget-object v10, p0, Lcom/android/camera/actor/VideoActor;->mVideoContext:Lcom/android/camera/Camera;

    invoke-virtual {v10}, Lcom/android/camera/Camera;->getLocationManager()Lcom/android/camera/LocationManager;

    move-result-object v10

    invoke-virtual {v10}, Lcom/android/camera/LocationManager;->getCurrentLocation()Landroid/location/Location;

    move-result-object v10

    iput-object v10, p0, Lcom/android/camera/actor/VideoActor;->mStartLocation:Landroid/location/Location;

    iget-object v10, p0, Lcom/android/camera/actor/VideoActor;->mStartLocation:Landroid/location/Location;

    if-eqz v10, :cond_5

    iget-object v10, p0, Lcom/android/camera/actor/VideoActor;->mStartLocation:Landroid/location/Location;

    invoke-virtual {v10}, Landroid/location/Location;->getLatitude()D

    move-result-wide v10

    double-to-float v4, v10

    iget-object v10, p0, Lcom/android/camera/actor/VideoActor;->mStartLocation:Landroid/location/Location;

    invoke-virtual {v10}, Landroid/location/Location;->getLongitude()D

    move-result-wide v10

    double-to-float v6, v10

    iget-object v10, p0, Lcom/android/camera/actor/VideoActor;->mMediaRecorder:Landroid/media/MediaRecorder;

    invoke-virtual {v10, v4, v6}, Landroid/media/MediaRecorder;->setLocation(FF)V

    :cond_5
    invoke-static {}, Lcom/android/camera/Storage;->getAvailableSpace()J

    move-result-wide v10

    sget-wide v12, Lcom/android/camera/Storage;->RECORD_LOW_STORAGE_THRESHOLD:J

    sub-long v7, v10, v12

    iget-wide v10, p0, Lcom/android/camera/actor/VideoActor;->mRequestedSizeLimit:J

    const-wide/16 v12, 0x0

    cmp-long v10, v10, v12

    if-lez v10, :cond_6

    iget-wide v10, p0, Lcom/android/camera/actor/VideoActor;->mRequestedSizeLimit:J

    cmp-long v10, v10, v7

    if-gez v10, :cond_6

    iget-wide v7, p0, Lcom/android/camera/actor/VideoActor;->mRequestedSizeLimit:J

    :cond_6
    :try_start_0
    iget-object v10, p0, Lcom/android/camera/actor/VideoActor;->mMediaRecorder:Landroid/media/MediaRecorder;

    invoke-virtual {v10, v7, v8}, Landroid/media/MediaRecorder;->setMaxFileSize(J)V
    :try_end_0
    .catch Ljava/lang/RuntimeException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_3
    iget-object v10, p0, Lcom/android/camera/actor/VideoActor;->mVideoFileDescriptor:Landroid/os/ParcelFileDescriptor;

    if-eqz v10, :cond_a

    iget-object v10, p0, Lcom/android/camera/actor/VideoActor;->mMediaRecorder:Landroid/media/MediaRecorder;

    iget-object v11, p0, Lcom/android/camera/actor/VideoActor;->mVideoFileDescriptor:Landroid/os/ParcelFileDescriptor;

    invoke-virtual {v11}, Landroid/os/ParcelFileDescriptor;->getFileDescriptor()Ljava/io/FileDescriptor;

    move-result-object v11

    invoke-virtual {v10, v11}, Landroid/media/MediaRecorder;->setOutputFile(Ljava/io/FileDescriptor;)V

    :goto_4
    const/4 v9, 0x0

    iget-object v10, p0, Lcom/android/camera/actor/VideoActor;->mVideoContext:Lcom/android/camera/Camera;

    invoke-virtual {v10}, Lcom/android/camera/Camera;->getOrietation()I

    move-result v10

    iput v10, p0, Lcom/android/camera/actor/VideoActor;->mOrientation:I

    iget v10, p0, Lcom/android/camera/actor/VideoActor;->mOrientation:I

    const/4 v11, -0x1

    if-eq v10, v11, :cond_c

    invoke-static {}, Lcom/android/camera/CameraHolder;->instance()Lcom/android/camera/CameraHolder;

    move-result-object v10

    invoke-virtual {v10}, Lcom/android/camera/CameraHolder;->getCameraInfo()[Landroid/hardware/Camera$CameraInfo;

    move-result-object v10

    iget-object v11, p0, Lcom/android/camera/actor/VideoActor;->mVideoContext:Lcom/android/camera/Camera;

    invoke-virtual {v11}, Lcom/android/camera/Camera;->getCameraId()I

    move-result v11

    aget-object v3, v10, v11

    iget v10, v3, Landroid/hardware/Camera$CameraInfo;->facing:I

    const/4 v11, 0x1

    if-ne v10, v11, :cond_b

    iget v10, v3, Landroid/hardware/Camera$CameraInfo;->orientation:I

    iget v11, p0, Lcom/android/camera/actor/VideoActor;->mOrientation:I

    sub-int/2addr v10, v11

    add-int/lit16 v10, v10, 0x168

    rem-int/lit16 v9, v10, 0x168

    :goto_5
    iget-object v10, p0, Lcom/android/camera/actor/VideoActor;->mMediaRecorder:Landroid/media/MediaRecorder;

    invoke-virtual {v10, v9}, Landroid/media/MediaRecorder;->setOrientationHint(I)V

    iget-object v10, p0, Lcom/android/camera/actor/VideoActor;->mVideoContext:Lcom/android/camera/Camera;

    iget-object v11, p0, Lcom/android/camera/actor/VideoActor;->mVideoContext:Lcom/android/camera/Camera;

    invoke-virtual {v11}, Lcom/android/camera/Camera;->getOrientationCompensation()I

    move-result v11

    invoke-virtual {v10, v11}, Lcom/android/camera/Camera;->setReviewOrientationCompensation(I)V

    :try_start_1
    iget-object v10, p0, Lcom/android/camera/actor/VideoActor;->mMediaRecorder:Landroid/media/MediaRecorder;

    invoke-virtual {v10}, Landroid/media/MediaRecorder;->prepare()V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_1

    iget-object v10, p0, Lcom/android/camera/actor/VideoActor;->mMediaRecorder:Landroid/media/MediaRecorder;

    invoke-virtual {v10, p0}, Landroid/media/MediaRecorder;->setOnErrorListener(Landroid/media/MediaRecorder$OnErrorListener;)V

    iget-object v10, p0, Lcom/android/camera/actor/VideoActor;->mMediaRecorder:Landroid/media/MediaRecorder;

    invoke-virtual {v10, p0}, Landroid/media/MediaRecorder;->setOnInfoListener(Landroid/media/MediaRecorder$OnInfoListener;)V

    iget-object v10, p0, Lcom/android/camera/actor/VideoActor;->mMediaRecorder:Landroid/media/MediaRecorder;

    invoke-virtual {v10, p0}, Landroid/media/MediaRecorder;->setOnCameraReleasedListener(Landroid/media/MediaRecorder$OnInfoListener;)V

    return-void

    :cond_7
    iget-object v10, p0, Lcom/android/camera/actor/VideoActor;->mMediaRecorder:Landroid/media/MediaRecorder;

    check-cast v10, Lcom/mediatek/mock/media/MockMediaRecorder;

    iget-object v11, p0, Lcom/android/camera/actor/VideoActor;->mVideoContext:Lcom/android/camera/Camera;

    invoke-virtual {v10, v11}, Lcom/mediatek/mock/media/MockMediaRecorder;->setContext(Landroid/content/Context;)V

    goto/16 :goto_0

    :cond_8
    iget-object v10, p0, Lcom/android/camera/actor/VideoActor;->mVideoContext:Lcom/android/camera/Camera;

    invoke-virtual {v10}, Lcom/android/camera/Camera;->getFileSaver()Lcom/android/camera/FileSaver;

    move-result-object v2

    iget-object v10, p0, Lcom/android/camera/actor/VideoActor;->mProfile:Landroid/media/CamcorderProfile;

    iget v10, v10, Landroid/media/CamcorderProfile;->fileFormat:I

    new-instance v11, Ljava/lang/StringBuilder;

    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v12, p0, Lcom/android/camera/actor/VideoActor;->mProfile:Landroid/media/CamcorderProfile;

    iget v12, v12, Landroid/media/CamcorderProfile;->videoFrameWidth:I

    invoke-static {v12}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v12

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    const-string v12, "x"

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    iget-object v12, p0, Lcom/android/camera/actor/VideoActor;->mProfile:Landroid/media/CamcorderProfile;

    iget v12, v12, Landroid/media/CamcorderProfile;->videoFrameHeight:I

    invoke-static {v12}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v12

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v2, v10, v11}, Lcom/android/camera/FileSaver;->prepareVideoRequest(ILjava/lang/String;)Lcom/android/camera/SaveRequest;

    move-result-object v10

    iput-object v10, p0, Lcom/android/camera/actor/VideoActor;->mVideoSaveRequest:Lcom/android/camera/SaveRequest;

    iget-object v10, p0, Lcom/android/camera/actor/VideoActor;->mMediaRecorder:Landroid/media/MediaRecorder;

    iget-object v11, p0, Lcom/android/camera/actor/VideoActor;->mVideoSaveRequest:Lcom/android/camera/SaveRequest;

    invoke-interface {v11}, Lcom/android/camera/SaveRequest;->getTempFilePath()Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v10, v11}, Landroid/media/MediaRecorder;->setOutputFile(Ljava/lang/String;)V

    iget-object v10, p0, Lcom/android/camera/actor/VideoActor;->mVideoSaveRequest:Lcom/android/camera/SaveRequest;

    invoke-interface {v10}, Lcom/android/camera/SaveRequest;->getTempFilePath()Ljava/lang/String;

    move-result-object v10

    iput-object v10, p0, Lcom/android/camera/actor/VideoActor;->mVideoFilename:Ljava/lang/String;

    goto/16 :goto_1

    :cond_9
    iget-object v10, p0, Lcom/android/camera/actor/VideoActor;->mVideoContext:Lcom/android/camera/Camera;

    invoke-virtual {v10}, Lcom/android/camera/Camera;->getLocationManager()Lcom/android/camera/LocationManager;

    move-result-object v10

    invoke-virtual {v10}, Lcom/android/camera/LocationManager;->getCurrentLocation()Landroid/location/Location;

    move-result-object v5

    goto/16 :goto_2

    :catch_0
    move-exception v1

    const-string v10, "VideoActor"

    const-string v11, "initializeNormalRecorder()"

    invoke-static {v10, v11, v1}, Lcom/android/camera/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto/16 :goto_3

    :cond_a
    iget-object v10, p0, Lcom/android/camera/actor/VideoActor;->mProfile:Landroid/media/CamcorderProfile;

    iget v10, v10, Landroid/media/CamcorderProfile;->fileFormat:I

    invoke-direct {p0, v10}, Lcom/android/camera/actor/VideoActor;->generateVideoFilename(I)V

    iget-object v10, p0, Lcom/android/camera/actor/VideoActor;->mMediaRecorder:Landroid/media/MediaRecorder;

    iget-object v11, p0, Lcom/android/camera/actor/VideoActor;->mVideoFilename:Ljava/lang/String;

    invoke-virtual {v10, v11}, Landroid/media/MediaRecorder;->setOutputFile(Ljava/lang/String;)V

    goto/16 :goto_4

    :cond_b
    iget v10, v3, Landroid/hardware/Camera$CameraInfo;->orientation:I

    iget v11, p0, Lcom/android/camera/actor/VideoActor;->mOrientation:I

    add-int/2addr v10, v11

    rem-int/lit16 v9, v10, 0x168

    goto/16 :goto_5

    :cond_c
    invoke-static {}, Lcom/android/camera/CameraHolder;->instance()Lcom/android/camera/CameraHolder;

    move-result-object v10

    invoke-virtual {v10}, Lcom/android/camera/CameraHolder;->getCameraInfo()[Landroid/hardware/Camera$CameraInfo;

    move-result-object v10

    iget-object v11, p0, Lcom/android/camera/actor/VideoActor;->mVideoContext:Lcom/android/camera/Camera;

    invoke-virtual {v11}, Lcom/android/camera/Camera;->getCameraId()I

    move-result v11

    aget-object v3, v10, v11

    iget v9, v3, Landroid/hardware/Camera$CameraInfo;->orientation:I

    goto/16 :goto_5

    :catch_1
    move-exception v0

    const-string v10, "VideoActor"

    const-string v11, "prepare failed"

    invoke-static {v10, v11, v0}, Lcom/android/camera/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    invoke-direct {p0}, Lcom/android/camera/actor/VideoActor;->releaseMediaRecorder()V

    new-instance v10, Ljava/lang/RuntimeException;

    invoke-direct {v10, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/Throwable;)V

    throw v10
.end method

.method private static isSupported(Ljava/lang/Object;Ljava/util/List;)Z
    .locals 2
    .param p0    # Ljava/lang/Object;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Object;",
            "Ljava/util/List",
            "<*>;)Z"
        }
    .end annotation

    const/4 v0, 0x0

    if-nez p1, :cond_1

    :cond_0
    :goto_0
    return v0

    :cond_1
    invoke-interface {p1, p0}, Ljava/util/List;->indexOf(Ljava/lang/Object;)I

    move-result v1

    if-ltz v1, :cond_0

    const/4 v0, 0x1

    goto :goto_0
.end method

.method private isVideoProcessing()Z
    .locals 1

    iget-object v0, p0, Lcom/android/camera/actor/VideoActor;->mVideoSavingTask:Ljava/lang/Thread;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/camera/actor/VideoActor;->mVideoSavingTask:Ljava/lang/Thread;

    invoke-virtual {v0}, Ljava/lang/Thread;->isAlive()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private onStopVideoRecordingAsync()V
    .locals 3

    const/4 v2, 0x1

    sget-boolean v0, Lcom/android/camera/actor/VideoActor;->LOG:Z

    if-eqz v0, :cond_0

    const-string v0, "VideoActor"

    const-string v1, "onStopVideoRecordingAsync"

    invoke-static {v0, v1}, Lcom/android/camera/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    iput-boolean v2, p0, Lcom/android/camera/actor/VideoActor;->mEffectsDisplayResult:Z

    iput-boolean v2, p0, Lcom/android/camera/actor/VideoActor;->mStopVideoRecording:Z

    invoke-direct {p0}, Lcom/android/camera/actor/VideoActor;->stopVideoRecordingAsync()V

    return-void
.end method

.method private pauseAudioPlayback()V
    .locals 3

    sget-boolean v1, Lcom/android/camera/actor/VideoActor;->LOG:Z

    if-eqz v1, :cond_0

    const-string v1, "VideoActor"

    const-string v2, "pauseAudioPlayback()"

    invoke-static {v1, v2}, Lcom/android/camera/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    new-instance v0, Landroid/content/Intent;

    const-string v1, "com.android.music.musicservicecommand"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    const-string v1, "command"

    const-string v2, "pause"

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    iget-object v1, p0, Lcom/android/camera/actor/VideoActor;->mVideoContext:Lcom/android/camera/Camera;

    invoke-virtual {v1, v0}, Landroid/content/ContextWrapper;->sendBroadcast(Landroid/content/Intent;)V

    return-void
.end method

.method private pauseVideoRecording()V
    .locals 5

    sget-boolean v1, Lcom/android/camera/actor/VideoActor;->LOG:Z

    if-eqz v1, :cond_0

    const-string v1, "VideoActor"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "pauseVideoRecording() mRecorderBusy="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-boolean v3, p0, Lcom/android/camera/actor/VideoActor;->mRecorderBusy:Z

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/android/camera/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    iget-object v1, p0, Lcom/android/camera/actor/VideoActor;->mRecordingView:Lcom/android/camera/manager/RecordingView;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Lcom/android/camera/manager/RecordingView;->setRecordingIndicator(Z)V

    iget-boolean v1, p0, Lcom/android/camera/actor/VideoActor;->mMediaRecorderRecording:Z

    if-eqz v1, :cond_1

    iget-boolean v1, p0, Lcom/android/camera/actor/VideoActor;->mMediaRecoderRecordingPaused:Z

    if-nez v1, :cond_1

    :try_start_0
    iget-object v1, p0, Lcom/android/camera/actor/VideoActor;->mMediaRecorder:Landroid/media/MediaRecorder;

    invoke-static {v1}, Lcom/mediatek/media/MediaRecorderEx;->pause(Landroid/media/MediaRecorder;)V
    :try_end_0
    .catch Ljava/lang/IllegalStateException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v1

    iget-wide v3, p0, Lcom/android/camera/actor/VideoActor;->mRecordingStartTime:J

    sub-long/2addr v1, v3

    iput-wide v1, p0, Lcom/android/camera/actor/VideoActor;->mVideoRecordedDuration:J

    const/4 v1, 0x1

    iput-boolean v1, p0, Lcom/android/camera/actor/VideoActor;->mMediaRecoderRecordingPaused:Z

    :cond_1
    return-void

    :catch_0
    move-exception v0

    const-string v1, "Camera"

    const-string v2, "Could not pause media recorder. "

    invoke-static {v1, v2}, Lcom/android/camera/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method private releaseEffects()V
    .locals 3

    const/4 v0, 0x1

    sget-boolean v1, Lcom/android/camera/actor/VideoActor;->LOG:Z

    if-eqz v1, :cond_0

    const-string v1, "VideoActor"

    const-string v2, "releaseEffects()"

    invoke-static {v1, v2}, Lcom/android/camera/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    iget-object v1, p0, Lcom/android/camera/actor/VideoActor;->mVideoContext:Lcom/android/camera/Camera;

    invoke-virtual {v1}, Lcom/android/camera/Camera;->dismissInfo()V

    iget-boolean v1, p0, Lcom/android/camera/actor/VideoActor;->mVideoCameraClosed:Z

    if-nez v1, :cond_1

    iget-object v1, p0, Lcom/android/camera/actor/VideoActor;->mVideoContext:Lcom/android/camera/Camera;

    invoke-virtual {v1}, Lcom/android/camera/Camera;->isNonePickIntent()Z

    move-result v1

    if-eqz v1, :cond_8

    :cond_1
    iget-object v1, p0, Lcom/android/camera/actor/VideoActor;->mEffectsRecorder:Lcom/android/camera/actor/EffectsRecorder;

    if-eqz v1, :cond_3

    sget-boolean v1, Lcom/android/camera/actor/VideoActor;->LOG:Z

    if-eqz v1, :cond_2

    const-string v1, "VideoActor"

    const-string v2, "mEffectsRecorder.disconnectCamera()"

    invoke-static {v1, v2}, Lcom/android/camera/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    :cond_2
    iget-object v1, p0, Lcom/android/camera/actor/VideoActor;->mEffectsRecorder:Lcom/android/camera/actor/EffectsRecorder;

    invoke-virtual {v1}, Lcom/android/camera/actor/EffectsRecorder;->disconnectCamera()V

    :cond_3
    iget-boolean v1, p0, Lcom/android/camera/actor/VideoActor;->mMediaRecorderRecording:Z

    if-eqz v1, :cond_7

    invoke-direct {p0}, Lcom/android/camera/actor/VideoActor;->effectActive()Z

    move-result v1

    if-nez v1, :cond_6

    :goto_0
    invoke-direct {p0, v0}, Lcom/android/camera/actor/VideoActor;->closeEffects(Z)V

    :goto_1
    invoke-direct {p0}, Lcom/android/camera/actor/VideoActor;->effectActive()Z

    move-result v0

    if-eqz v0, :cond_5

    iget-object v0, p0, Lcom/android/camera/actor/VideoActor;->mEffectsRecorder:Lcom/android/camera/actor/EffectsRecorder;

    if-eqz v0, :cond_5

    sget-boolean v0, Lcom/android/camera/actor/VideoActor;->LOG:Z

    if-eqz v0, :cond_4

    const-string v0, "VideoActor"

    const-string v1, "mEffectsRecorder.disconnectDisplay()"

    invoke-static {v0, v1}, Lcom/android/camera/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    :cond_4
    iget-object v0, p0, Lcom/android/camera/actor/VideoActor;->mEffectsRecorder:Lcom/android/camera/actor/EffectsRecorder;

    invoke-virtual {v0}, Lcom/android/camera/actor/EffectsRecorder;->disconnectDisplay()V

    :cond_5
    :goto_2
    return-void

    :cond_6
    const/4 v0, 0x0

    goto :goto_0

    :cond_7
    invoke-direct {p0, v0}, Lcom/android/camera/actor/VideoActor;->closeEffects(Z)V

    goto :goto_1

    :cond_8
    iget-object v0, p0, Lcom/android/camera/actor/VideoActor;->mEffectsRecorder:Lcom/android/camera/actor/EffectsRecorder;

    if-eqz v0, :cond_5

    iget-object v0, p0, Lcom/android/camera/actor/VideoActor;->mEffectsRecorder:Lcom/android/camera/actor/EffectsRecorder;

    invoke-virtual {v0}, Lcom/android/camera/actor/EffectsRecorder;->stopPreview()V

    goto :goto_2
.end method

.method private releaseEffectsRecorder()V
    .locals 3

    const/4 v2, 0x0

    sget-boolean v0, Lcom/android/camera/actor/VideoActor;->LOG:Z

    if-eqz v0, :cond_0

    const-string v0, "VideoActor"

    const-string v1, "Releasing effects recorder."

    invoke-static {v0, v1}, Lcom/android/camera/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    iget-object v0, p0, Lcom/android/camera/actor/VideoActor;->mEffectsRecorder:Lcom/android/camera/actor/EffectsRecorder;

    if-eqz v0, :cond_1

    invoke-direct {p0}, Lcom/android/camera/actor/VideoActor;->cleanupEmptyFile()V

    iget-object v0, p0, Lcom/android/camera/actor/VideoActor;->mEffectsRecorder:Lcom/android/camera/actor/EffectsRecorder;

    invoke-virtual {v0}, Lcom/android/camera/actor/EffectsRecorder;->release()V

    iput-object v2, p0, Lcom/android/camera/actor/VideoActor;->mEffectsRecorder:Lcom/android/camera/actor/EffectsRecorder;

    :cond_1
    const/4 v0, 0x0

    iput v0, p0, Lcom/android/camera/actor/VideoActor;->mEffectType:I

    iput-object v2, p0, Lcom/android/camera/actor/VideoActor;->mVideoFilename:Ljava/lang/String;

    return-void
.end method

.method private releaseMediaRecorder()V
    .locals 4

    const/4 v3, 0x0

    sget-boolean v0, Lcom/android/camera/actor/VideoActor;->LOG:Z

    if-eqz v0, :cond_0

    const-string v0, "VideoActor"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "releaseMediaRecorder() mMediaRecorder="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/android/camera/actor/VideoActor;->mMediaRecorder:Landroid/media/MediaRecorder;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/android/camera/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    iget-object v0, p0, Lcom/android/camera/actor/VideoActor;->mMediaRecorder:Landroid/media/MediaRecorder;

    if-eqz v0, :cond_1

    invoke-direct {p0}, Lcom/android/camera/actor/VideoActor;->cleanupEmptyFile()V

    iget-object v0, p0, Lcom/android/camera/actor/VideoActor;->mMediaRecorder:Landroid/media/MediaRecorder;

    invoke-virtual {v0}, Landroid/media/MediaRecorder;->reset()V

    iget-object v0, p0, Lcom/android/camera/actor/VideoActor;->mMediaRecorder:Landroid/media/MediaRecorder;

    invoke-virtual {v0}, Landroid/media/MediaRecorder;->release()V

    iput-object v3, p0, Lcom/android/camera/actor/VideoActor;->mMediaRecorder:Landroid/media/MediaRecorder;

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/android/camera/actor/VideoActor;->mRecorderCameraReleased:Z

    :cond_1
    iput-object v3, p0, Lcom/android/camera/actor/VideoActor;->mVideoFilename:Ljava/lang/String;

    return-void
.end method

.method private releaseVideoActor()V
    .locals 4

    const/4 v3, 0x0

    const/4 v2, 0x0

    sget-boolean v0, Lcom/android/camera/actor/VideoActor;->LOG:Z

    if-eqz v0, :cond_0

    const-string v0, "VideoActor"

    const-string v1, "releaseVideoActor"

    invoke-static {v0, v1}, Lcom/android/camera/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    iput-object v3, p0, Lcom/android/camera/actor/VideoActor;->mVideoShutterListener:Lcom/android/camera/ui/ShutterButton$OnShutterButtonListener;

    iget-object v0, p0, Lcom/android/camera/actor/VideoActor;->mVideoContext:Lcom/android/camera/Camera;

    invoke-virtual {v0}, Lcom/android/camera/Camera;->getFocusManager()Lcom/android/camera/FocusManager;

    move-result-object v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/android/camera/actor/VideoActor;->mVideoContext:Lcom/android/camera/Camera;

    invoke-virtual {v0}, Lcom/android/camera/Camera;->getFocusManager()Lcom/android/camera/FocusManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/android/camera/FocusManager;->removeMessages()V

    :cond_1
    iput-boolean v2, p0, Lcom/android/camera/actor/VideoActor;->mSingleStartRecording:Z

    iput-boolean v2, p0, Lcom/android/camera/actor/VideoActor;->mIsAutoFocusCallback:Z

    iput-object v3, p0, Lcom/android/camera/actor/CameraActor;->mFocusManager:Lcom/android/camera/FocusManager;

    iput-boolean v2, p0, Lcom/android/camera/actor/VideoActor;->mMediaRecoderRecordingPaused:Z

    return-void
.end method

.method private restoreReviewIfNeed()V
    .locals 5

    iget-object v2, p0, Lcom/android/camera/actor/VideoActor;->mVideoContext:Lcom/android/camera/Camera;

    invoke-virtual {v2}, Lcom/android/camera/Camera;->getReviewManager()Lcom/android/camera/manager/ReviewManager;

    move-result-object v2

    invoke-virtual {v2}, Lcom/android/camera/manager/ViewManager;->isShowing()Z

    move-result v2

    if-eqz v2, :cond_1

    iget-object v2, p0, Lcom/android/camera/actor/VideoActor;->mVideoContext:Lcom/android/camera/Camera;

    invoke-virtual {v2}, Lcom/android/camera/Camera;->isNonePickIntent()Z

    move-result v2

    if-nez v2, :cond_0

    iget-object v2, p0, Lcom/android/camera/actor/VideoActor;->mVideoFileDescriptor:Landroid/os/ParcelFileDescriptor;

    if-nez v2, :cond_0

    iget-object v2, p0, Lcom/android/camera/actor/VideoActor;->mVideoContext:Lcom/android/camera/Camera;

    invoke-virtual {v2}, Lcom/android/camera/Camera;->getSaveUri()Landroid/net/Uri;

    move-result-object v1

    if-eqz v1, :cond_0

    :try_start_0
    iget-object v2, p0, Lcom/android/camera/actor/VideoActor;->mContentResolver:Landroid/content/ContentResolver;

    const-string v3, "rw"

    invoke-virtual {v2, v1, v3}, Landroid/content/ContentResolver;->openFileDescriptor(Landroid/net/Uri;Ljava/lang/String;)Landroid/os/ParcelFileDescriptor;

    move-result-object v2

    iput-object v2, p0, Lcom/android/camera/actor/VideoActor;->mVideoFileDescriptor:Landroid/os/ParcelFileDescriptor;
    :try_end_0
    .catch Ljava/io/FileNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    :cond_0
    :goto_0
    iget-object v2, p0, Lcom/android/camera/actor/VideoActor;->mVideoContext:Lcom/android/camera/Camera;

    new-instance v3, Lcom/android/camera/actor/VideoActor$5;

    invoke-direct {v3, p0}, Lcom/android/camera/actor/VideoActor$5;-><init>(Lcom/android/camera/actor/VideoActor;)V

    invoke-virtual {v2, v3}, Landroid/app/Activity;->runOnUiThread(Ljava/lang/Runnable;)V

    :cond_1
    sget-boolean v2, Lcom/android/camera/actor/VideoActor;->LOG:Z

    if-eqz v2, :cond_2

    const-string v2, "VideoActor"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "restoreReviewIfNeed() review show="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lcom/android/camera/actor/VideoActor;->mVideoContext:Lcom/android/camera/Camera;

    invoke-virtual {v4}, Lcom/android/camera/Camera;->getReviewManager()Lcom/android/camera/manager/ReviewManager;

    move-result-object v4

    invoke-virtual {v4}, Lcom/android/camera/manager/ViewManager;->isShowing()Z

    move-result v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " mVideoFileDescriptor="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lcom/android/camera/actor/VideoActor;->mVideoFileDescriptor:Landroid/os/ParcelFileDescriptor;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ", mCurrentVideoFilename="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lcom/android/camera/actor/VideoActor;->mCurrentVideoFilename:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/android/camera/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    :cond_2
    return-void

    :catch_0
    move-exception v0

    const-string v2, "VideoActor"

    const-string v3, "initializeNormalRecorder()"

    invoke-static {v2, v3, v0}, Lcom/android/camera/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0
.end method

.method private setAutoFocusMode()V
    .locals 3

    const-string v1, "auto"

    iget-object v2, p0, Lcom/android/camera/actor/VideoActor;->mVideoContext:Lcom/android/camera/Camera;

    invoke-virtual {v2}, Lcom/android/camera/Camera;->getParameters()Landroid/hardware/Camera$Parameters;

    move-result-object v2

    invoke-virtual {v2}, Landroid/hardware/Camera$Parameters;->getSupportedFocusModes()Ljava/util/List;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/android/camera/actor/VideoActor;->isSupported(Ljava/lang/Object;Ljava/util/List;)Z

    move-result v1

    if-eqz v1, :cond_0

    const-string v0, "auto"

    iget-object v1, p0, Lcom/android/camera/actor/VideoActor;->mVideoContext:Lcom/android/camera/Camera;

    new-instance v2, Lcom/android/camera/actor/VideoActor$13;

    invoke-direct {v2, p0}, Lcom/android/camera/actor/VideoActor$13;-><init>(Lcom/android/camera/actor/VideoActor;)V

    invoke-virtual {v1, v2}, Lcom/android/camera/Camera;->lockRun(Ljava/lang/Runnable;)V

    :cond_0
    sget-boolean v1, Lcom/android/camera/actor/VideoActor;->LOG:Z

    if-eqz v1, :cond_1

    const-string v1, "VideoActor"

    const-string v2, "set focus mode is auto"

    invoke-static {v1, v2}, Lcom/android/camera/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    :cond_1
    return-void
.end method

.method private setFocusState(I)V
    .locals 3
    .param p1    # I

    sget-boolean v0, Lcom/android/camera/actor/VideoActor;->LOG:Z

    if-eqz v0, :cond_0

    const-string v0, "VideoActor"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "setFocusState("

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ") mMediaRecorderRecording="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-boolean v2, p0, Lcom/android/camera/actor/VideoActor;->mMediaRecorderRecording:Z

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", mVideoCameraClosed="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-boolean v2, p0, Lcom/android/camera/actor/VideoActor;->mVideoCameraClosed:Z

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ",mVideoContext.getViewState() = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/android/camera/actor/VideoActor;->mVideoContext:Lcom/android/camera/Camera;

    invoke-virtual {v2}, Lcom/android/camera/Camera;->getViewState()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/android/camera/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    iput p1, p0, Lcom/android/camera/actor/VideoActor;->mFocusState:I

    iget-boolean v0, p0, Lcom/android/camera/actor/VideoActor;->mMediaRecorderRecording:Z

    if-nez v0, :cond_1

    iget-boolean v0, p0, Lcom/android/camera/actor/VideoActor;->mVideoCameraClosed:Z

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/android/camera/actor/VideoActor;->mVideoContext:Lcom/android/camera/Camera;

    invoke-virtual {v0}, Lcom/android/camera/Camera;->getViewState()I

    move-result v0

    const/4 v1, 0x6

    if-ne v0, v1, :cond_2

    :cond_1
    :goto_0
    return-void

    :cond_2
    packed-switch p1, :pswitch_data_0

    goto :goto_0

    :pswitch_0
    iget-object v0, p0, Lcom/android/camera/actor/VideoActor;->mVideoContext:Lcom/android/camera/Camera;

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Lcom/android/camera/Camera;->setViewState(I)V

    goto :goto_0

    :pswitch_1
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/android/camera/actor/VideoActor;->hideOtherSettings(Z)V

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_1
    .end packed-switch
.end method

.method private showAlert()V
    .locals 3

    sget-boolean v1, Lcom/android/camera/actor/VideoActor;->LOG:Z

    if-eqz v1, :cond_0

    const-string v1, "VideoActor"

    const-string v2, "showAlert()"

    invoke-static {v1, v2}, Lcom/android/camera/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    invoke-static {}, Lcom/android/camera/Storage;->isStorageReady()Z

    move-result v1

    if-eqz v1, :cond_2

    const/4 v0, 0x0

    iget-object v1, p0, Lcom/android/camera/actor/VideoActor;->mVideoFileDescriptor:Landroid/os/ParcelFileDescriptor;

    if-eqz v1, :cond_3

    iget-object v1, p0, Lcom/android/camera/actor/VideoActor;->mVideoFileDescriptor:Landroid/os/ParcelFileDescriptor;

    invoke-virtual {v1}, Landroid/os/ParcelFileDescriptor;->getFileDescriptor()Ljava/io/FileDescriptor;

    move-result-object v0

    iget-object v1, p0, Lcom/android/camera/actor/VideoActor;->mVideoContext:Lcom/android/camera/Camera;

    iget-object v2, p0, Lcom/android/camera/actor/VideoActor;->mVideoFileDescriptor:Landroid/os/ParcelFileDescriptor;

    invoke-virtual {v2}, Landroid/os/ParcelFileDescriptor;->getFileDescriptor()Ljava/io/FileDescriptor;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/android/camera/Camera;->showReview(Ljava/io/FileDescriptor;)V

    :cond_1
    :goto_0
    iget-object v1, p0, Lcom/android/camera/actor/VideoActor;->mVideoContext:Lcom/android/camera/Camera;

    const/4 v2, 0x3

    invoke-virtual {v1, v2}, Lcom/android/camera/Camera;->switchShutter(I)V

    :cond_2
    return-void

    :cond_3
    iget-object v1, p0, Lcom/android/camera/actor/VideoActor;->mCurrentVideoFilename:Ljava/lang/String;

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/android/camera/actor/VideoActor;->mVideoContext:Lcom/android/camera/Camera;

    iget-object v2, p0, Lcom/android/camera/actor/VideoActor;->mCurrentVideoFilename:Ljava/lang/String;

    invoke-virtual {v1, v2}, Lcom/android/camera/Camera;->showReview(Ljava/lang/String;)V

    goto :goto_0
.end method

.method private showVideoSnapshotUI(Z)V
    .locals 4
    .param p1    # Z

    const/4 v1, 0x1

    const/4 v2, 0x0

    iget-object v0, p0, Lcom/android/camera/actor/VideoActor;->mVideoContext:Lcom/android/camera/Camera;

    invoke-virtual {v0}, Lcom/android/camera/Camera;->isVideoCaptureIntent()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/android/camera/actor/VideoActor;->mVideoContext:Lcom/android/camera/Camera;

    invoke-virtual {v0, p1}, Lcom/android/camera/Camera;->showBorder(Z)V

    iget-object v0, p0, Lcom/android/camera/actor/VideoActor;->mVideoContext:Lcom/android/camera/Camera;

    invoke-virtual {v0}, Lcom/android/camera/Camera;->getZoomManager()Lcom/android/camera/manager/ZoomManager;

    move-result-object v3

    if-nez p1, :cond_1

    move v0, v1

    :goto_0
    invoke-virtual {v3, v0}, Lcom/android/camera/manager/ViewManager;->setEnabled(Z)V

    iget-object v0, p0, Lcom/android/camera/actor/VideoActor;->mVideoContext:Lcom/android/camera/Camera;

    invoke-virtual {v0}, Lcom/android/camera/Camera;->getShutterManager()Lcom/android/camera/manager/ShutterManager;

    move-result-object v0

    if-nez p1, :cond_2

    :goto_1
    invoke-virtual {v0, v1}, Lcom/android/camera/manager/ShutterManager;->setPhotoShutterEnabled(Z)V

    sget-boolean v0, Lcom/android/camera/actor/VideoActor;->LOG:Z

    if-eqz v0, :cond_0

    const-string v0, "VideoActor"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "showVideoSnapshotUI,enable shutter,enabled is "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/android/camera/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    return-void

    :cond_1
    move v0, v2

    goto :goto_0

    :cond_2
    move v1, v2

    goto :goto_1
.end method

.method private shutterPressed()V
    .locals 2

    iget-object v0, p0, Lcom/android/camera/actor/VideoActor;->mVideoContext:Lcom/android/camera/Camera;

    invoke-virtual {v0}, Lcom/android/camera/Camera;->getParameters()Landroid/hardware/Camera$Parameters;

    move-result-object v0

    if-eqz v0, :cond_0

    const-string v0, "auto"

    iget-object v1, p0, Lcom/android/camera/actor/VideoActor;->mVideoContext:Lcom/android/camera/Camera;

    invoke-virtual {v1}, Lcom/android/camera/Camera;->getParameters()Landroid/hardware/Camera$Parameters;

    move-result-object v1

    invoke-virtual {v1}, Landroid/hardware/Camera$Parameters;->getSupportedFocusModes()Ljava/util/List;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/android/camera/actor/VideoActor;->isSupported(Ljava/lang/Object;Ljava/util/List;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/camera/actor/VideoActor;->mVideoContext:Lcom/android/camera/Camera;

    invoke-virtual {v0}, Lcom/android/camera/Camera;->getFocusManager()Lcom/android/camera/FocusManager;

    move-result-object v0

    const-string v1, "auto"

    invoke-virtual {v0, v1}, Lcom/android/camera/FocusManager;->overrideFocusMode(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/android/camera/actor/VideoActor;->mVideoContext:Lcom/android/camera/Camera;

    invoke-virtual {v0}, Lcom/android/camera/Camera;->getFocusManager()Lcom/android/camera/FocusManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/android/camera/FocusManager;->onShutterDown()V

    :cond_0
    return-void
.end method

.method private startEffectRecording()V
    .locals 3

    sget-boolean v1, Lcom/android/camera/actor/VideoActor;->LOG:Z

    if-eqz v1, :cond_0

    const-string v1, "VideoActor"

    const-string v2, "startEffectRecording()"

    invoke-static {v1, v2}, Lcom/android/camera/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    :try_start_0
    iget-object v1, p0, Lcom/android/camera/actor/VideoActor;->mEffectsRecorder:Lcom/android/camera/actor/EffectsRecorder;

    invoke-virtual {v1}, Lcom/android/camera/actor/EffectsRecorder;->startRecording()V
    :try_end_0
    .catch Ljava/lang/RuntimeException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    return-void

    :catch_0
    move-exception v0

    const-string v1, "VideoActor"

    const-string v2, "Could not start effects recorder. "

    invoke-static {v1, v2, v0}, Lcom/android/camera/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    const/4 v1, 0x1

    iput-boolean v1, p0, Lcom/android/camera/actor/VideoActor;->mStartRecordingFailed:Z

    invoke-direct {p0}, Lcom/android/camera/actor/VideoActor;->releaseEffectsRecorder()V

    goto :goto_0
.end method

.method private startNormalRecording()V
    .locals 3

    sget-boolean v1, Lcom/android/camera/actor/VideoActor;->LOG:Z

    if-eqz v1, :cond_0

    const-string v1, "VideoActor"

    const-string v2, "startNormalRecording()"

    invoke-static {v1, v2}, Lcom/android/camera/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    :try_start_0
    iget-object v1, p0, Lcom/android/camera/actor/VideoActor;->mMediaRecorder:Landroid/media/MediaRecorder;

    invoke-virtual {v1}, Landroid/media/MediaRecorder;->start()V
    :try_end_0
    .catch Ljava/lang/RuntimeException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    return-void

    :catch_0
    move-exception v0

    const-string v1, "VideoActor"

    const-string v2, "Could not start media recorder. "

    invoke-static {v1, v2, v0}, Lcom/android/camera/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    const/4 v1, 0x1

    iput-boolean v1, p0, Lcom/android/camera/actor/VideoActor;->mStartRecordingFailed:Z

    invoke-direct {p0}, Lcom/android/camera/actor/VideoActor;->releaseMediaRecorder()V

    iget-object v1, p0, Lcom/android/camera/actor/VideoActor;->mVideoContext:Lcom/android/camera/Camera;

    invoke-virtual {v1}, Lcom/android/camera/Camera;->getCameraDevice()Lcom/android/camera/CameraManager$CameraProxy;

    move-result-object v1

    invoke-virtual {v1}, Lcom/android/camera/CameraManager$CameraProxy;->lock()V

    goto :goto_0
.end method

.method private startPlayVideoActivity()V
    .locals 5

    new-instance v1, Landroid/content/Intent;

    const-string v2, "android.intent.action.VIEW"

    invoke-direct {v1, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Lcom/android/camera/actor/VideoActor;->mCurrentVideoUri:Landroid/net/Uri;

    iget-object v3, p0, Lcom/android/camera/actor/VideoActor;->mProfile:Landroid/media/CamcorderProfile;

    iget v3, v3, Landroid/media/CamcorderProfile;->fileFormat:I

    invoke-direct {p0, v3}, Lcom/android/camera/actor/VideoActor;->convertOutputFormatToMimeType(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->setDataAndType(Landroid/net/Uri;Ljava/lang/String;)Landroid/content/Intent;

    :try_start_0
    iget-object v2, p0, Lcom/android/camera/actor/VideoActor;->mVideoContext:Lcom/android/camera/Camera;

    invoke-virtual {v2, v1}, Landroid/app/Activity;->startActivity(Landroid/content/Intent;)V
    :try_end_0
    .catch Landroid/content/ActivityNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    return-void

    :catch_0
    move-exception v0

    const-string v2, "VideoActor"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Couldn\'t view video "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lcom/android/camera/actor/VideoActor;->mCurrentVideoUri:Landroid/net/Uri;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3, v0}, Lcom/android/camera/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0
.end method

.method private startPreview()V
    .locals 3

    iget-object v1, p0, Lcom/android/camera/actor/VideoActor;->mVideoContext:Lcom/android/camera/Camera;

    new-instance v2, Lcom/android/camera/actor/VideoActor$6;

    invoke-direct {v2, p0}, Lcom/android/camera/actor/VideoActor$6;-><init>(Lcom/android/camera/actor/VideoActor;)V

    invoke-virtual {v1, v2}, Landroid/app/Activity;->runOnUiThread(Ljava/lang/Runnable;)V

    invoke-virtual {p0}, Lcom/android/camera/actor/VideoActor;->stopPreview()V

    iget-object v1, p0, Lcom/android/camera/actor/VideoActor;->mVideoContext:Lcom/android/camera/Camera;

    invoke-virtual {v1}, Lcom/android/camera/Camera;->getCameraState()I

    move-result v1

    if-eqz v1, :cond_0

    invoke-direct {p0}, Lcom/android/camera/actor/VideoActor;->effectActive()Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/android/camera/actor/VideoActor;->mEffectsRecorder:Lcom/android/camera/actor/EffectsRecorder;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/android/camera/actor/VideoActor;->mEffectsRecorder:Lcom/android/camera/actor/EffectsRecorder;

    invoke-virtual {v1}, Lcom/android/camera/actor/EffectsRecorder;->release()V

    const/4 v1, 0x0

    iput-object v1, p0, Lcom/android/camera/actor/VideoActor;->mEffectsRecorder:Lcom/android/camera/actor/EffectsRecorder;

    :cond_0
    iget-object v1, p0, Lcom/android/camera/actor/VideoActor;->mVideoContext:Lcom/android/camera/Camera;

    invoke-virtual {v1}, Lcom/android/camera/Camera;->isNonePickIntent()Z

    move-result v1

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/android/camera/actor/VideoActor;->mVideoContext:Lcom/android/camera/Camera;

    invoke-virtual {v1}, Lcom/android/camera/Camera;->applyContinousCallback()V

    :cond_1
    sget-boolean v1, Lcom/android/camera/actor/VideoActor;->LOG:Z

    if-eqz v1, :cond_2

    const-string v1, "VideoActor"

    const-string v2, "startPreview()"

    invoke-static {v1, v2}, Lcom/android/camera/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    :cond_2
    :try_start_0
    invoke-direct {p0}, Lcom/android/camera/actor/VideoActor;->effectActive()Z

    move-result v1

    if-nez v1, :cond_3

    iget-object v1, p0, Lcom/android/camera/actor/VideoActor;->mVideoContext:Lcom/android/camera/Camera;

    invoke-virtual {v1}, Lcom/android/camera/Camera;->getCameraDevice()Lcom/android/camera/CameraManager$CameraProxy;

    move-result-object v1

    invoke-virtual {v1}, Lcom/android/camera/CameraManager$CameraProxy;->startPreviewAsync()V

    iget-object v1, p0, Lcom/android/camera/actor/VideoActor;->mVideoContext:Lcom/android/camera/Camera;

    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Lcom/android/camera/Camera;->setCameraState(I)V

    iget-object v1, p0, Lcom/android/camera/actor/VideoActor;->mVideoContext:Lcom/android/camera/Camera;

    invoke-virtual {v1}, Lcom/android/camera/Camera;->getFocusManager()Lcom/android/camera/FocusManager;

    move-result-object v1

    invoke-virtual {v1}, Lcom/android/camera/FocusManager;->onPreviewStarted()V

    :goto_0
    return-void

    :cond_3
    invoke-direct {p0}, Lcom/android/camera/actor/VideoActor;->initializeEffectsPreview()V

    iget-object v1, p0, Lcom/android/camera/actor/VideoActor;->mEffectsRecorder:Lcom/android/camera/actor/EffectsRecorder;

    invoke-virtual {v1}, Lcom/android/camera/actor/EffectsRecorder;->startPreview()V
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v0

    invoke-direct {p0}, Lcom/android/camera/actor/VideoActor;->releaseVideoActor()V

    const-string v1, "VideoActor"

    const-string v2, "startPreview()"

    invoke-static {v1, v2, v0}, Lcom/android/camera/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0
.end method

.method private startVideoRecording()V
    .locals 7

    const-wide/16 v5, 0x0

    const/4 v2, 0x0

    const/4 v1, 0x1

    sget-boolean v0, Lcom/android/camera/actor/VideoActor;->LOG:Z

    if-eqz v0, :cond_0

    const-string v0, "VideoActor"

    const-string v3, "startVideoRecording()"

    invoke-static {v0, v3}, Lcom/android/camera/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    iget-boolean v0, p0, Lcom/android/camera/actor/VideoActor;->mSingleAutoModeSupported:Z

    if-eqz v0, :cond_1

    iget-boolean v0, p0, Lcom/android/camera/actor/VideoActor;->mIsContinousFocusMode:Z

    if-eqz v0, :cond_1

    iput-boolean v1, p0, Lcom/android/camera/actor/VideoActor;->mSingleStartRecording:Z

    invoke-direct {p0}, Lcom/android/camera/actor/VideoActor;->setAutoFocusMode()V

    :cond_1
    iget-object v0, p0, Lcom/android/camera/actor/VideoActor;->mVideoContext:Lcom/android/camera/Camera;

    invoke-virtual {v0, v2}, Lcom/android/camera/Camera;->setSwipingEnabled(Z)V

    invoke-direct {p0, v1}, Lcom/android/camera/actor/VideoActor;->hideOtherSettings(Z)V

    iget-object v0, p0, Lcom/android/camera/actor/VideoActor;->mVideoContext:Lcom/android/camera/Camera;

    invoke-virtual {v0}, Lcom/android/camera/Camera;->getLimitedSize()J

    move-result-wide v3

    cmp-long v0, v3, v5

    if-lez v0, :cond_3

    invoke-direct {p0}, Lcom/android/camera/actor/VideoActor;->effectActive()Z

    move-result v0

    if-nez v0, :cond_2

    iget-object v0, p0, Lcom/android/camera/actor/VideoActor;->mVideoContext:Lcom/android/camera/Camera;

    invoke-virtual {v0}, Lcom/android/camera/Camera;->getLimitedSize()J

    move-result-wide v3

    iput-wide v3, p0, Lcom/android/camera/actor/VideoActor;->mTotalSize:J

    iget-object v0, p0, Lcom/android/camera/actor/VideoActor;->mRecordingView:Lcom/android/camera/manager/RecordingView;

    iget-wide v3, p0, Lcom/android/camera/actor/VideoActor;->mTotalSize:J

    invoke-virtual {v0, v3, v4}, Lcom/android/camera/manager/RecordingView;->setTotalSize(J)V

    iget-object v0, p0, Lcom/android/camera/actor/VideoActor;->mRecordingView:Lcom/android/camera/manager/RecordingView;

    invoke-virtual {v0, v5, v6}, Lcom/android/camera/manager/RecordingView;->setCurrentSize(J)V

    :cond_2
    iget-object v3, p0, Lcom/android/camera/actor/VideoActor;->mRecordingView:Lcom/android/camera/manager/RecordingView;

    invoke-direct {p0}, Lcom/android/camera/actor/VideoActor;->effectActive()Z

    move-result v0

    if-nez v0, :cond_4

    move v0, v1

    :goto_0
    invoke-virtual {v3, v0}, Lcom/android/camera/manager/RecordingView;->setRecordingSizeVisible(Z)V

    :cond_3
    iget-object v0, p0, Lcom/android/camera/actor/VideoActor;->mRecordingView:Lcom/android/camera/manager/RecordingView;

    invoke-virtual {v0, v1}, Lcom/android/camera/manager/RecordingView;->setRecordingIndicator(Z)V

    iget-object v3, p0, Lcom/android/camera/actor/VideoActor;->mRecordingView:Lcom/android/camera/manager/RecordingView;

    invoke-direct {p0}, Lcom/android/camera/actor/VideoActor;->effectActive()Z

    move-result v0

    if-nez v0, :cond_5

    move v0, v1

    :goto_1
    invoke-virtual {v3, v0}, Lcom/android/camera/manager/RecordingView;->setPauseResumeVisible(Z)V

    iget-object v0, p0, Lcom/android/camera/actor/VideoActor;->mRecordingView:Lcom/android/camera/manager/RecordingView;

    invoke-virtual {v0}, Lcom/android/camera/manager/ViewManager;->show()V

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/android/camera/actor/VideoActor;->mCurrentVideoUri:Landroid/net/Uri;

    invoke-direct {p0}, Lcom/android/camera/actor/VideoActor;->effectActive()Z

    move-result v0

    if-eqz v0, :cond_6

    invoke-direct {p0}, Lcom/android/camera/actor/VideoActor;->initializeEffectsRecording()V

    iget-object v0, p0, Lcom/android/camera/actor/VideoActor;->mEffectsRecorder:Lcom/android/camera/actor/EffectsRecorder;

    if-nez v0, :cond_7

    const-string v0, "VideoActor"

    const-string v1, "Fail to initialize effect recorder."

    new-instance v2, Ljava/lang/Throwable;

    invoke-direct {v2}, Ljava/lang/Throwable;-><init>()V

    invoke-static {v0, v1, v2}, Lcom/android/camera/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    :goto_2
    return-void

    :cond_4
    move v0, v2

    goto :goto_0

    :cond_5
    move v0, v2

    goto :goto_1

    :cond_6
    invoke-direct {p0}, Lcom/android/camera/actor/VideoActor;->initializeNormalRecorder()V

    iget-object v0, p0, Lcom/android/camera/actor/VideoActor;->mMediaRecorder:Landroid/media/MediaRecorder;

    if-nez v0, :cond_7

    const-string v0, "VideoActor"

    const-string v1, "Fail to initialize media recorder."

    new-instance v2, Ljava/lang/Throwable;

    invoke-direct {v2}, Ljava/lang/Throwable;-><init>()V

    invoke-static {v0, v1, v2}, Lcom/android/camera/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_2

    :cond_7
    invoke-direct {p0}, Lcom/android/camera/actor/VideoActor;->pauseAudioPlayback()V

    invoke-direct {p0}, Lcom/android/camera/actor/VideoActor;->effectActive()Z

    move-result v0

    if-eqz v0, :cond_c

    iget-object v0, p0, Lcom/android/camera/actor/VideoActor;->mVideoContext:Lcom/android/camera/Camera;

    invoke-virtual {v0}, Lcom/android/camera/Camera;->getShutterManager()Lcom/android/camera/manager/ShutterManager;

    move-result-object v0

    invoke-virtual {v0, v2}, Lcom/android/camera/manager/ShutterManager;->setCancelButtonEnabled(Z)V

    invoke-direct {p0}, Lcom/android/camera/actor/VideoActor;->startEffectRecording()V

    :goto_3
    iget-boolean v0, p0, Lcom/android/camera/actor/VideoActor;->mStartRecordingFailed:Z

    if-eqz v0, :cond_9

    sget-boolean v0, Lcom/android/camera/actor/VideoActor;->LOG:Z

    if-eqz v0, :cond_8

    const-string v0, "VideoActor"

    const-string v3, "mStartRecordingFailed."

    invoke-static {v0, v3}, Lcom/android/camera/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    :cond_8
    iput-boolean v2, p0, Lcom/android/camera/actor/VideoActor;->mStartRecordingFailed:Z

    iget-object v0, p0, Lcom/android/camera/actor/VideoActor;->mVideoContext:Lcom/android/camera/Camera;

    const v3, 0x7f0c00ad

    invoke-virtual {v0, v3}, Lcom/android/camera/Camera;->showToast(I)V

    invoke-direct {p0}, Lcom/android/camera/actor/VideoActor;->backToLastTheseCase()V

    :cond_9
    iget-object v0, p0, Lcom/android/camera/actor/VideoActor;->mVideoContext:Lcom/android/camera/Camera;

    new-instance v3, Lcom/android/camera/actor/VideoActor$8;

    invoke-direct {v3, p0}, Lcom/android/camera/actor/VideoActor$8;-><init>(Lcom/android/camera/actor/VideoActor;)V

    invoke-virtual {v0, v3}, Lcom/android/camera/Camera;->lockRun(Ljava/lang/Runnable;)V

    iput-boolean v2, p0, Lcom/android/camera/actor/VideoActor;->mMediaRecoderRecordingPaused:Z

    iput-wide v5, p0, Lcom/android/camera/actor/VideoActor;->mVideoRecordedDuration:J

    iput-boolean v2, p0, Lcom/android/camera/actor/VideoActor;->mRecorderCameraReleased:Z

    iput v1, p0, Lcom/android/camera/actor/VideoActor;->mStoppingAction:I

    iget-boolean v0, p0, Lcom/android/camera/actor/VideoActor;->mWfdListenerEnabled:Z

    if-eqz v0, :cond_a

    invoke-direct {p0}, Lcom/android/camera/actor/VideoActor;->effectActive()Z

    move-result v0

    if-nez v0, :cond_d

    invoke-direct {p0}, Lcom/android/camera/actor/VideoActor;->releaseMediaRecorder()V

    :goto_4
    invoke-direct {p0}, Lcom/android/camera/actor/VideoActor;->backToLastTheseCase()V

    :cond_a
    iput-boolean v1, p0, Lcom/android/camera/actor/VideoActor;->mMediaRecorderRecording:Z

    iget-object v0, p0, Lcom/android/camera/actor/VideoActor;->mVideoContext:Lcom/android/camera/Camera;

    invoke-virtual {v0}, Lcom/android/camera/Camera;->getShutterManager()Lcom/android/camera/manager/ShutterManager;

    move-result-object v0

    invoke-virtual {v0, v1}, Lcom/android/camera/manager/ShutterManager;->setVideoShutterMask(Z)V

    iget-object v0, p0, Lcom/android/camera/actor/VideoActor;->mVideoContext:Lcom/android/camera/Camera;

    const/4 v1, 0x3

    invoke-virtual {v0, v1}, Lcom/android/camera/Camera;->setCameraState(I)V

    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/android/camera/actor/VideoActor;->mRecordingStartTime:J

    invoke-direct {p0}, Lcom/android/camera/actor/VideoActor;->updateRecordingTime()V

    iget-boolean v0, p0, Lcom/android/camera/actor/VideoActor;->mCaptureTimeLapse:Z

    if-nez v0, :cond_b

    iget-object v0, p0, Lcom/android/camera/actor/VideoActor;->mHandler:Landroid/os/Handler;

    const/4 v1, 0x5

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeMessages(I)V

    :cond_b
    iget-object v0, p0, Lcom/android/camera/actor/VideoActor;->mVideoContext:Lcom/android/camera/Camera;

    invoke-virtual {v0}, Lcom/android/camera/Camera;->keepScreenOn()V

    goto/16 :goto_2

    :cond_c
    invoke-direct {p0}, Lcom/android/camera/actor/VideoActor;->startNormalRecording()V

    goto :goto_3

    :cond_d
    invoke-direct {p0}, Lcom/android/camera/actor/VideoActor;->releaseEffectsRecorder()V

    goto :goto_4
.end method

.method private stopEffectVideoRecording()V
    .locals 2

    sget-boolean v0, Lcom/android/camera/actor/VideoActor;->LOG:Z

    if-eqz v0, :cond_0

    const-string v0, "VideoActor"

    const-string v1, "stopEffectVideoRecording"

    invoke-static {v0, v1}, Lcom/android/camera/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    iget-object v0, p0, Lcom/android/camera/actor/VideoActor;->mEffectsRecorder:Lcom/android/camera/actor/EffectsRecorder;

    invoke-virtual {v0}, Lcom/android/camera/actor/EffectsRecorder;->stopRecording()V

    return-void
.end method

.method private stopVideoRecordingAsync()V
    .locals 6

    const/4 v5, 0x3

    const/4 v4, 0x1

    const/4 v3, 0x0

    sget-boolean v0, Lcom/android/camera/actor/VideoActor;->LOG:Z

    if-eqz v0, :cond_0

    const-string v0, "VideoActor"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "stopVideoRecordingAsync() mMediaRecorderRecording="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-boolean v2, p0, Lcom/android/camera/actor/VideoActor;->mMediaRecorderRecording:Z

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", mRecorderBusy="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-boolean v2, p0, Lcom/android/camera/actor/VideoActor;->mRecorderBusy:Z

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/android/camera/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    iget-object v0, p0, Lcom/android/camera/actor/VideoActor;->mVideoContext:Lcom/android/camera/Camera;

    invoke-virtual {v0, v4}, Lcom/android/camera/Camera;->setSwipingEnabled(Z)V

    iget-object v0, p0, Lcom/android/camera/actor/VideoActor;->mHandler:Landroid/os/Handler;

    const/4 v1, 0x5

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeMessages(I)V

    iget-object v0, p0, Lcom/android/camera/actor/VideoActor;->mVideoContext:Lcom/android/camera/Camera;

    invoke-virtual {v0}, Lcom/android/camera/Camera;->getShutterManager()Lcom/android/camera/manager/ShutterManager;

    move-result-object v0

    invoke-virtual {v0, v3}, Lcom/android/camera/manager/ShutterManager;->setVideoShutterMask(Z)V

    invoke-direct {p0}, Lcom/android/camera/actor/VideoActor;->isVideoProcessing()Z

    move-result v0

    if-eqz v0, :cond_2

    :cond_1
    :goto_0
    return-void

    :cond_2
    iget-boolean v0, p0, Lcom/android/camera/actor/VideoActor;->mRecorderBusy:Z

    if-nez v0, :cond_1

    iput-boolean v4, p0, Lcom/android/camera/actor/VideoActor;->mRecorderBusy:Z

    iget-object v0, p0, Lcom/android/camera/actor/VideoActor;->mRecordingView:Lcom/android/camera/manager/RecordingView;

    invoke-virtual {v0}, Lcom/android/camera/manager/RecordingView;->hide()V

    iget-boolean v0, p0, Lcom/android/camera/actor/VideoActor;->mMediaRecorderRecording:Z

    if-eqz v0, :cond_4

    iget-object v0, p0, Lcom/android/camera/actor/VideoActor;->mVideoContext:Lcom/android/camera/Camera;

    invoke-virtual {v0}, Lcom/android/camera/Camera;->getShutterManager()Lcom/android/camera/manager/ShutterManager;

    move-result-object v0

    invoke-virtual {v0, v3}, Lcom/android/camera/manager/ShutterManager;->setVideoShutterEnabled(Z)V

    iget v0, p0, Lcom/android/camera/actor/VideoActor;->mStoppingAction:I

    if-eq v0, v5, :cond_3

    iget-object v0, p0, Lcom/android/camera/actor/VideoActor;->mVideoContext:Lcom/android/camera/Camera;

    iget-object v1, p0, Lcom/android/camera/actor/VideoActor;->mVideoContext:Lcom/android/camera/Camera;

    invoke-virtual {v1}, Landroid/view/ContextThemeWrapper;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0c00a7

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/android/camera/Camera;->showProgress(Ljava/lang/String;)V

    :cond_3
    new-instance v0, Lcom/android/camera/actor/VideoActor$SavingTask;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/android/camera/actor/VideoActor$SavingTask;-><init>(Lcom/android/camera/actor/VideoActor;Lcom/android/camera/actor/VideoActor$1;)V

    iput-object v0, p0, Lcom/android/camera/actor/VideoActor;->mVideoSavingTask:Ljava/lang/Thread;

    iget-object v0, p0, Lcom/android/camera/actor/VideoActor;->mVideoSavingTask:Ljava/lang/Thread;

    invoke-virtual {v0}, Ljava/lang/Thread;->start()V

    goto :goto_0

    :cond_4
    iput-boolean v3, p0, Lcom/android/camera/actor/VideoActor;->mRecorderBusy:Z

    invoke-direct {p0}, Lcom/android/camera/actor/VideoActor;->effectActive()Z

    move-result v0

    if-nez v0, :cond_5

    invoke-direct {p0}, Lcom/android/camera/actor/VideoActor;->releaseMediaRecorder()V

    :cond_5
    iget v0, p0, Lcom/android/camera/actor/VideoActor;->mStoppingAction:I

    if-ne v0, v5, :cond_1

    invoke-direct {p0, v3}, Lcom/android/camera/actor/VideoActor;->doReturnToCaller(Z)V

    goto :goto_0
.end method

.method private updateEffectSelection()Z
    .locals 9

    const/4 v8, 0x2

    const/4 v3, 0x0

    const/4 v4, 0x1

    iget v2, p0, Lcom/android/camera/actor/VideoActor;->mEffectType:I

    iget-object v1, p0, Lcom/android/camera/actor/VideoActor;->mEffectParameter:Ljava/lang/Object;

    iget-object v5, p0, Lcom/android/camera/actor/VideoActor;->mVideoContext:Lcom/android/camera/Camera;

    invoke-virtual {v5}, Lcom/android/camera/Camera;->getEffectType()I

    move-result v5

    iput v5, p0, Lcom/android/camera/actor/VideoActor;->mEffectType:I

    iget-object v5, p0, Lcom/android/camera/actor/VideoActor;->mVideoContext:Lcom/android/camera/Camera;

    invoke-virtual {v5}, Lcom/android/camera/Camera;->getEffectParameter()Ljava/lang/Object;

    move-result-object v5

    iput-object v5, p0, Lcom/android/camera/actor/VideoActor;->mEffectParameter:Ljava/lang/Object;

    sget-boolean v5, Lcom/android/camera/actor/VideoActor;->LOG:Z

    if-eqz v5, :cond_0

    const-string v5, "VideoActor"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "updateEffectSelection,mEffectType = "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    iget v7, p0, Lcom/android/camera/actor/VideoActor;->mEffectType:I

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, ",mEffectParameter = "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    iget-object v7, p0, Lcom/android/camera/actor/VideoActor;->mEffectParameter:Ljava/lang/Object;

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, ",previousEffectType = "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, ",previousEffectParameter = "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Lcom/android/camera/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    iget v5, p0, Lcom/android/camera/actor/VideoActor;->mEffectType:I

    if-ne v5, v2, :cond_4

    iget v5, p0, Lcom/android/camera/actor/VideoActor;->mEffectType:I

    if-nez v5, :cond_3

    if-eqz v1, :cond_1

    const-string v5, "gallery"

    invoke-virtual {v1, v5}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_1

    iget-object v5, p0, Lcom/android/camera/actor/VideoActor;->mEffectParameter:Ljava/lang/Object;

    if-nez v5, :cond_1

    iget-object v5, p0, Lcom/android/camera/actor/VideoActor;->mVideoContext:Lcom/android/camera/Camera;

    invoke-virtual {v5}, Lcom/android/camera/Camera;->isNonePickIntent()Z

    move-result v5

    if-eqz v5, :cond_2

    invoke-direct {p0}, Lcom/android/camera/actor/VideoActor;->releaseEffects()V

    invoke-direct {p0}, Lcom/android/camera/actor/VideoActor;->backToLastModeIfNeed()Z

    move v3, v4

    :cond_1
    :goto_0
    return v3

    :cond_2
    iget-object v4, p0, Lcom/android/camera/actor/VideoActor;->mVideoContext:Lcom/android/camera/Camera;

    new-instance v5, Lcom/android/camera/actor/VideoActor$18;

    invoke-direct {v5, p0}, Lcom/android/camera/actor/VideoActor$18;-><init>(Lcom/android/camera/actor/VideoActor;)V

    invoke-virtual {v4, v5}, Landroid/app/Activity;->runOnUiThread(Ljava/lang/Runnable;)V

    goto :goto_0

    :cond_3
    iget-object v5, p0, Lcom/android/camera/actor/VideoActor;->mEffectParameter:Ljava/lang/Object;

    if-eqz v5, :cond_4

    iget-object v5, p0, Lcom/android/camera/actor/VideoActor;->mEffectParameter:Ljava/lang/Object;

    invoke-virtual {v5, v1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_4

    sget-boolean v4, Lcom/android/camera/actor/VideoActor;->LOG:Z

    if-eqz v4, :cond_1

    const-string v4, "VideoActor"

    const-string v5, "mEffectParameter.equals(previousEffectParameter)"

    invoke-static {v4, v5}, Lcom/android/camera/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    :cond_4
    iget v5, p0, Lcom/android/camera/actor/VideoActor;->mEffectType:I

    if-nez v5, :cond_5

    iget-object v4, p0, Lcom/android/camera/actor/VideoActor;->mVideoContext:Lcom/android/camera/Camera;

    invoke-virtual {v4, v8}, Lcom/android/camera/Camera;->switchShutter(I)V

    goto :goto_0

    :cond_5
    iget v3, p0, Lcom/android/camera/actor/VideoActor;->mEffectType:I

    if-ne v3, v8, :cond_7

    iget-object v3, p0, Lcom/android/camera/actor/VideoActor;->mEffectParameter:Ljava/lang/Object;

    check-cast v3, Ljava/lang/String;

    const-string v5, "gallery"

    invoke-virtual {v3, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_7

    if-eqz v1, :cond_6

    const-string v3, "gallery"

    invoke-virtual {v1, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_7

    :cond_6
    new-instance v0, Landroid/content/Intent;

    const-string v3, "android.intent.action.PICK"

    invoke-direct {v0, v3}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    sget-object v3, Landroid/provider/MediaStore$Video$Media;->EXTERNAL_CONTENT_URI:Landroid/net/Uri;

    const-string v5, "video/*"

    invoke-virtual {v0, v3, v5}, Landroid/content/Intent;->setDataAndType(Landroid/net/Uri;Ljava/lang/String;)Landroid/content/Intent;

    const-string v3, "android.intent.extra.LOCAL_ONLY"

    invoke-virtual {v0, v3, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    iget-object v3, p0, Lcom/android/camera/actor/VideoActor;->mVideoContext:Lcom/android/camera/Camera;

    const/16 v5, 0x3e8

    invoke-virtual {v3, v0, v5}, Landroid/app/Activity;->startActivityForResult(Landroid/content/Intent;I)V

    move v3, v4

    goto :goto_0

    :cond_7
    if-nez v2, :cond_b

    sget-boolean v3, Lcom/android/camera/actor/VideoActor;->LOG:Z

    if-eqz v3, :cond_8

    const-string v3, "VideoActor"

    const-string v5, "previousEffectType == EffectsRecorder.EFFECT_NONE"

    invoke-static {v3, v5}, Lcom/android/camera/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    :cond_8
    iget-object v3, p0, Lcom/android/camera/actor/VideoActor;->mVideoContext:Lcom/android/camera/Camera;

    invoke-virtual {v3}, Lcom/android/camera/Camera;->getReviewManager()Lcom/android/camera/manager/ReviewManager;

    move-result-object v3

    invoke-virtual {v3}, Lcom/android/camera/manager/ViewManager;->isShowing()Z

    move-result v3

    if-eqz v3, :cond_9

    invoke-direct {p0}, Lcom/android/camera/actor/VideoActor;->effectActive()Z

    move-result v3

    if-eqz v3, :cond_9

    iput-boolean v4, p0, Lcom/android/camera/actor/VideoActor;->mNeedReLearningEffect:Z

    move v3, v4

    goto/16 :goto_0

    :cond_9
    invoke-direct {p0}, Lcom/android/camera/actor/VideoActor;->startPreview()V

    :cond_a
    :goto_1
    move v3, v4

    goto/16 :goto_0

    :cond_b
    sget-boolean v3, Lcom/android/camera/actor/VideoActor;->LOG:Z

    if-eqz v3, :cond_c

    const-string v3, "VideoActor"

    const-string v5, "mEffectsRecorder.setEffect(mEffectType, mEffectParameter"

    invoke-static {v3, v5}, Lcom/android/camera/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    :cond_c
    iget-object v3, p0, Lcom/android/camera/actor/VideoActor;->mEffectsRecorder:Lcom/android/camera/actor/EffectsRecorder;

    iget v5, p0, Lcom/android/camera/actor/VideoActor;->mEffectType:I

    iget-object v6, p0, Lcom/android/camera/actor/VideoActor;->mEffectParameter:Ljava/lang/Object;

    invoke-virtual {v3, v5, v6}, Lcom/android/camera/actor/EffectsRecorder;->setEffect(ILjava/lang/Object;)V

    iget v3, p0, Lcom/android/camera/actor/VideoActor;->mEffectType:I

    if-ne v3, v4, :cond_a

    iget-object v3, p0, Lcom/android/camera/actor/VideoActor;->mVideoContext:Lcom/android/camera/Camera;

    const/4 v5, 0x5

    invoke-virtual {v3, v5}, Lcom/android/camera/Camera;->switchShutter(I)V

    iget-object v3, p0, Lcom/android/camera/actor/VideoActor;->mVideoContext:Lcom/android/camera/Camera;

    invoke-virtual {v3}, Lcom/android/camera/Camera;->getShutterManager()Lcom/android/camera/manager/ShutterManager;

    move-result-object v3

    invoke-virtual {v3, v4}, Lcom/android/camera/manager/ShutterManager;->setCancelButtonEnabled(Z)V

    iget-object v3, p0, Lcom/android/camera/actor/VideoActor;->mVideoContext:Lcom/android/camera/Camera;

    invoke-virtual {v3}, Lcom/android/camera/Camera;->getShutterManager()Lcom/android/camera/manager/ShutterManager;

    move-result-object v3

    invoke-virtual {v3, v4}, Lcom/android/camera/manager/ShutterManager;->setVideoShutterEnabled(Z)V

    goto :goto_1
.end method

.method private updateRecordingTime()V
    .locals 15

    const/4 v14, 0x0

    const/4 v13, 0x1

    iget-boolean v10, p0, Lcom/android/camera/actor/VideoActor;->mMediaRecorderRecording:Z

    if-nez v10, :cond_0

    :goto_0
    return-void

    :cond_0
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v6

    iget-wide v10, p0, Lcom/android/camera/actor/VideoActor;->mRecordingStartTime:J

    sub-long v2, v6, v10

    iget-boolean v10, p0, Lcom/android/camera/actor/VideoActor;->mMediaRecoderRecordingPaused:Z

    if-eqz v10, :cond_1

    iget-wide v2, p0, Lcom/android/camera/actor/VideoActor;->mVideoRecordedDuration:J

    :cond_1
    move-wide v4, v2

    iget-boolean v10, p0, Lcom/android/camera/actor/VideoActor;->mCaptureTimeLapse:Z

    if-nez v10, :cond_4

    iget-object v10, p0, Lcom/android/camera/actor/VideoActor;->mRecordingView:Lcom/android/camera/manager/RecordingView;

    invoke-virtual {v10, v4, v5, v14}, Lcom/android/camera/manager/RecordingView;->showTime(JZ)V

    const-wide/16 v8, 0x3e8

    :goto_1
    iget v10, p0, Lcom/android/camera/actor/VideoActor;->mCurrentShowIndicator:I

    rsub-int/lit8 v10, v10, 0x1

    iput v10, p0, Lcom/android/camera/actor/VideoActor;->mCurrentShowIndicator:I

    iget-boolean v10, p0, Lcom/android/camera/actor/VideoActor;->mMediaRecoderRecordingPaused:Z

    if-eqz v10, :cond_5

    iget v10, p0, Lcom/android/camera/actor/VideoActor;->mCurrentShowIndicator:I

    if-ne v13, v10, :cond_5

    iget-object v10, p0, Lcom/android/camera/actor/VideoActor;->mRecordingView:Lcom/android/camera/manager/RecordingView;

    invoke-virtual {v10, v14}, Lcom/android/camera/manager/RecordingView;->setTimeVisible(Z)V

    :goto_2
    const-wide/16 v0, 0x1f4

    iget-boolean v10, p0, Lcom/android/camera/actor/VideoActor;->mMediaRecoderRecordingPaused:Z

    if-nez v10, :cond_2

    rem-long v10, v2, v8

    sub-long v0, v8, v10

    :cond_2
    sget-boolean v10, Lcom/android/camera/actor/VideoActor;->LOG:Z

    if-eqz v10, :cond_3

    const-string v10, "VideoActor"

    new-instance v11, Ljava/lang/StringBuilder;

    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    const-string v12, "updateRecordingTime(),actualNextUpdateDelay=="

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-static {v10, v11}, Lcom/android/camera/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    :cond_3
    iget-object v10, p0, Lcom/android/camera/actor/VideoActor;->mHandler:Landroid/os/Handler;

    const/4 v11, 0x5

    invoke-virtual {v10, v11, v0, v1}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    goto :goto_0

    :cond_4
    iget-object v10, p0, Lcom/android/camera/actor/VideoActor;->mRecordingView:Lcom/android/camera/manager/RecordingView;

    invoke-direct {p0, v2, v3}, Lcom/android/camera/actor/VideoActor;->getTimeLapseVideoLength(J)J

    move-result-wide v11

    invoke-virtual {v10, v11, v12, v13}, Lcom/android/camera/manager/RecordingView;->showTime(JZ)V

    iget v10, p0, Lcom/android/camera/actor/VideoActor;->mTimeBetweenTimeLapseFrameCaptureMs:I

    int-to-long v8, v10

    goto :goto_1

    :cond_5
    iget-object v10, p0, Lcom/android/camera/actor/VideoActor;->mRecordingView:Lcom/android/camera/manager/RecordingView;

    invoke-virtual {v10, v13}, Lcom/android/camera/manager/RecordingView;->setTimeVisible(Z)V

    goto :goto_2
.end method

.method private waitForRecorder()V
    .locals 4

    iget-object v2, p0, Lcom/android/camera/actor/VideoActor;->mVideoSavingTask:Ljava/lang/Thread;

    monitor-enter v2

    :try_start_0
    iget-boolean v1, p0, Lcom/android/camera/actor/VideoActor;->mRecorderCameraReleased:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-nez v1, :cond_1

    :try_start_1
    sget-boolean v1, Lcom/android/camera/actor/VideoActor;->LOG:Z

    if-eqz v1, :cond_0

    const-string v1, "VideoActor"

    const-string v3, "Wait for releasing camera done in MediaRecorder"

    invoke-static {v1, v3}, Lcom/android/camera/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    iget-object v1, p0, Lcom/android/camera/actor/VideoActor;->mVideoSavingTask:Ljava/lang/Thread;

    invoke-virtual {v1}, Ljava/lang/Object;->wait()V
    :try_end_1
    .catch Ljava/lang/InterruptedException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    :cond_1
    :goto_0
    :try_start_2
    monitor-exit v2

    return-void

    :catch_0
    move-exception v0

    const-string v1, "VideoActor"

    const-string v3, "Got notify from Media recorder()"

    invoke-static {v1, v3, v0}, Lcom/android/camera/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0

    :catchall_0
    move-exception v1

    monitor-exit v2
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    throw v1
.end method


# virtual methods
.method public autoFocus()V
    .locals 2

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/android/camera/actor/VideoActor;->mFocusStartTime:J

    sget-boolean v0, Lcom/android/camera/actor/VideoActor;->LOG:Z

    if-eqz v0, :cond_0

    const-string v0, "VideoActor"

    const-string v1, "autoFocus"

    invoke-static {v0, v1}, Lcom/android/camera/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    iget-object v0, p0, Lcom/android/camera/actor/VideoActor;->mVideoContext:Lcom/android/camera/Camera;

    invoke-virtual {v0}, Lcom/android/camera/Camera;->getCameraDevice()Lcom/android/camera/CameraManager$CameraProxy;

    move-result-object v0

    iget-object v1, p0, Lcom/android/camera/actor/VideoActor;->mAutoFocusCallback:Lcom/android/camera/actor/VideoActor$AutoFocusCallback;

    invoke-virtual {v0, v1}, Lcom/android/camera/CameraManager$CameraProxy;->autoFocus(Landroid/hardware/Camera$AutoFocusCallback;)V

    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/android/camera/actor/VideoActor;->setFocusState(I)V

    return-void
.end method

.method public cancelAutoFocus()V
    .locals 2

    sget-boolean v0, Lcom/android/camera/actor/VideoActor;->LOG:Z

    if-eqz v0, :cond_0

    const-string v0, "VideoActor"

    const-string v1, "cancelAutoFocus"

    invoke-static {v0, v1}, Lcom/android/camera/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    iget-object v0, p0, Lcom/android/camera/actor/VideoActor;->mVideoContext:Lcom/android/camera/Camera;

    invoke-virtual {v0}, Lcom/android/camera/Camera;->getCameraDevice()Lcom/android/camera/CameraManager$CameraProxy;

    move-result-object v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/android/camera/actor/VideoActor;->mVideoContext:Lcom/android/camera/Camera;

    invoke-virtual {v0}, Lcom/android/camera/Camera;->getCameraDevice()Lcom/android/camera/CameraManager$CameraProxy;

    move-result-object v0

    invoke-virtual {v0}, Lcom/android/camera/CameraManager$CameraProxy;->cancelAutoFocus()V

    :cond_1
    const/4 v0, 0x3

    invoke-direct {p0, v0}, Lcom/android/camera/actor/VideoActor;->setFocusState(I)V

    iget-boolean v0, p0, Lcom/android/camera/actor/VideoActor;->mSingleStartRecording:Z

    if-eqz v0, :cond_2

    iget-boolean v0, p0, Lcom/android/camera/actor/VideoActor;->mSingleAutoModeSupported:Z

    if-eqz v0, :cond_2

    iget-boolean v0, p0, Lcom/android/camera/actor/VideoActor;->mIsAutoFocusCallback:Z

    if-nez v0, :cond_3

    :cond_2
    invoke-virtual {p0}, Lcom/android/camera/actor/VideoActor;->setFocusParameters()V

    :cond_3
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/android/camera/actor/VideoActor;->mIsAutoFocusCallback:Z

    return-void
.end method

.method public capture()Z
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public doSmileShutter()Z
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public bridge synthetic getAutoFocusMoveCallback()Landroid/hardware/Camera$AutoFocusMoveCallback;
    .locals 1

    invoke-virtual {p0}, Lcom/android/camera/actor/VideoActor;->getAutoFocusMoveCallback()Lcom/android/camera/actor/VideoActor$AutoFocusMoveCallback;

    move-result-object v0

    return-object v0
.end method

.method public getAutoFocusMoveCallback()Lcom/android/camera/actor/VideoActor$AutoFocusMoveCallback;
    .locals 1

    iget-object v0, p0, Lcom/android/camera/actor/VideoActor;->mAutoFocusMoveCallback:Lcom/android/camera/actor/VideoActor$AutoFocusMoveCallback;

    return-object v0
.end method

.method public getCancelListener()Landroid/view/View$OnClickListener;
    .locals 1

    iget-object v0, p0, Lcom/android/camera/actor/VideoActor;->mCancelListener:Landroid/view/View$OnClickListener;

    return-object v0
.end method

.method public getErrorCallback()Landroid/hardware/Camera$ErrorCallback;
    .locals 1

    iget-object v0, p0, Lcom/android/camera/actor/VideoActor;->mErrorCallback:Lcom/android/camera/CameraErrorCallback;

    return-object v0
.end method

.method public getFocusManagerListener()Lcom/android/camera/FocusManager$Listener;
    .locals 0

    return-object p0
.end method

.method public getMode()I
    .locals 1

    const/16 v0, 0x9

    return v0
.end method

.method public getOkListener()Landroid/view/View$OnClickListener;
    .locals 1

    iget-object v0, p0, Lcom/android/camera/actor/VideoActor;->mOkListener:Landroid/view/View$OnClickListener;

    return-object v0
.end method

.method public getPhotoShutterButtonListener()Lcom/android/camera/ui/ShutterButton$OnShutterButtonListener;
    .locals 3

    sget-boolean v0, Lcom/android/camera/actor/VideoActor;->LOG:Z

    if-eqz v0, :cond_0

    const-string v0, "VideoActor"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "getPhotoShutterButtonListener"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/android/camera/actor/VideoActor;->mPhotoShutterListener:Lcom/android/camera/ui/ShutterButton$OnShutterButtonListener;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/android/camera/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    iget-object v0, p0, Lcom/android/camera/actor/VideoActor;->mPhotoShutterListener:Lcom/android/camera/ui/ShutterButton$OnShutterButtonListener;

    return-object v0
.end method

.method public getPlayListener()Landroid/view/View$OnClickListener;
    .locals 1

    iget-object v0, p0, Lcom/android/camera/actor/VideoActor;->mReviewPlay:Landroid/view/View$OnClickListener;

    return-object v0
.end method

.method public getRetakeListener()Landroid/view/View$OnClickListener;
    .locals 1

    iget-object v0, p0, Lcom/android/camera/actor/VideoActor;->mRetakeListener:Landroid/view/View$OnClickListener;

    return-object v0
.end method

.method public getVideoShutterButtonListener()Lcom/android/camera/ui/ShutterButton$OnShutterButtonListener;
    .locals 3

    sget-boolean v0, Lcom/android/camera/actor/VideoActor;->LOG:Z

    if-eqz v0, :cond_0

    const-string v0, "VideoActor"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "getVideoShutterButtonListener"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/android/camera/actor/VideoActor;->mVideoShutterListener:Lcom/android/camera/ui/ShutterButton$OnShutterButtonListener;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/android/camera/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    iget-object v0, p0, Lcom/android/camera/actor/VideoActor;->mVideoShutterListener:Lcom/android/camera/ui/ShutterButton$OnShutterButtonListener;

    return-object v0
.end method

.method public getonSingleTapUpListener()Lcom/android/camera/Camera$OnSingleTapUpListener;
    .locals 1

    iget-object v0, p0, Lcom/android/camera/actor/VideoActor;->mTapupListener:Lcom/android/camera/Camera$OnSingleTapUpListener;

    return-object v0
.end method

.method public onActivityResult(IILandroid/content/Intent;)V
    .locals 4
    .param p1    # I
    .param p2    # I
    .param p3    # Landroid/content/Intent;

    const/4 v3, 0x0

    packed-switch p1, :pswitch_data_0

    const-string v0, "VideoActor"

    const-string v1, "Unknown activity result sent to Camera!"

    invoke-static {v0, v1}, Lcom/android/camera/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    :goto_0
    invoke-super {p0, p1, p2, p3}, Lcom/android/camera/actor/CameraActor;->onActivityResult(IILandroid/content/Intent;)V

    return-void

    :pswitch_0
    const/4 v0, -0x1

    if-ne p2, v0, :cond_1

    invoke-virtual {p3}, Landroid/content/Intent;->getData()Landroid/net/Uri;

    move-result-object v0

    invoke-virtual {v0}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/android/camera/actor/VideoActor;->mEffectUriFromGallery:Ljava/lang/String;

    sget-boolean v0, Lcom/android/camera/actor/VideoActor;->LOG:Z

    if-eqz v0, :cond_0

    const-string v0, "VideoActor"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Received URI from gallery: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/android/camera/actor/VideoActor;->mEffectUriFromGallery:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/android/camera/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    iput-boolean v3, p0, Lcom/android/camera/actor/VideoActor;->mResetEffect:Z

    goto :goto_0

    :cond_1
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/android/camera/actor/VideoActor;->mEffectUriFromGallery:Ljava/lang/String;

    sget-boolean v0, Lcom/android/camera/actor/VideoActor;->LOG:Z

    if-eqz v0, :cond_2

    const-string v0, "VideoActor"

    const-string v1, "No URI from gallery"

    invoke-static {v0, v1}, Lcom/android/camera/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    :cond_2
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/android/camera/actor/VideoActor;->mResetEffect:Z

    invoke-direct {p0}, Lcom/android/camera/actor/VideoActor;->releaseEffects()V

    iget-object v0, p0, Lcom/android/camera/actor/VideoActor;->mVideoContext:Lcom/android/camera/Camera;

    invoke-virtual {v0, v3}, Lcom/android/camera/Camera;->resetLiveEffect(Z)V

    iget-object v0, p0, Lcom/android/camera/actor/VideoActor;->mVideoContext:Lcom/android/camera/Camera;

    invoke-virtual {v0}, Lcom/android/camera/Camera;->notifyPreferenceChanged()V

    invoke-direct {p0, v3}, Lcom/android/camera/actor/VideoActor;->hideOtherSettings(Z)V

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x3e8
        :pswitch_0
    .end packed-switch
.end method

.method public onBackPressed()Z
    .locals 4

    const/4 v0, 0x1

    sget-boolean v1, Lcom/android/camera/actor/VideoActor;->LOG:Z

    if-eqz v1, :cond_0

    const-string v1, "VideoActor"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "onBackPressed() isFinishing()="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/android/camera/actor/VideoActor;->mVideoContext:Lcom/android/camera/Camera;

    invoke-virtual {v3}, Landroid/app/Activity;->isFinishing()Z

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ", mVideoCameraClosed="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-boolean v3, p0, Lcom/android/camera/actor/VideoActor;->mVideoCameraClosed:Z

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ", isVideoProcessing()="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-direct {p0}, Lcom/android/camera/actor/VideoActor;->isVideoProcessing()Z

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ",mVideoContext.isShowingProgress() = "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/android/camera/actor/VideoActor;->mVideoContext:Lcom/android/camera/Camera;

    invoke-virtual {v3}, Lcom/android/camera/Camera;->isShowingProgress()Z

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/android/camera/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    iget-boolean v1, p0, Lcom/android/camera/actor/VideoActor;->mVideoCameraClosed:Z

    if-nez v1, :cond_1

    iget-object v1, p0, Lcom/android/camera/actor/VideoActor;->mVideoContext:Lcom/android/camera/Camera;

    invoke-virtual {v1}, Lcom/android/camera/Camera;->isShowingProgress()Z

    move-result v1

    if-nez v1, :cond_1

    invoke-direct {p0}, Lcom/android/camera/actor/VideoActor;->isVideoProcessing()Z

    move-result v1

    if-eqz v1, :cond_2

    :cond_1
    :goto_0
    return v0

    :cond_2
    iget-boolean v1, p0, Lcom/android/camera/actor/VideoActor;->mMediaRecorderRecording:Z

    if-eqz v1, :cond_3

    invoke-direct {p0}, Lcom/android/camera/actor/VideoActor;->onStopVideoRecordingAsync()V

    goto :goto_0

    :cond_3
    invoke-super {p0}, Lcom/android/camera/actor/CameraActor;->onBackPressed()Z

    move-result v0

    goto :goto_0
.end method

.method public onCameraClose()V
    .locals 3

    const/4 v2, 0x0

    sget-boolean v0, Lcom/android/camera/actor/VideoActor;->LOG:Z

    if-eqz v0, :cond_0

    const-string v0, "VideoActor"

    const-string v1, "onCameraClose()"

    invoke-static {v0, v1}, Lcom/android/camera/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    iget-object v0, p0, Lcom/android/camera/actor/VideoActor;->mHandler:Landroid/os/Handler;

    const/4 v1, 0x5

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeMessages(I)V

    iget-object v0, p0, Lcom/android/camera/actor/VideoActor;->mHandler:Landroid/os/Handler;

    const/16 v1, 0xf

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeMessages(I)V

    invoke-direct {p0, v2}, Lcom/android/camera/actor/VideoActor;->showVideoSnapshotUI(Z)V

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/android/camera/actor/VideoActor;->mVideoCameraClosed:Z

    iput-boolean v2, p0, Lcom/android/camera/actor/VideoActor;->mSingleStartRecording:Z

    iput-boolean v2, p0, Lcom/android/camera/actor/VideoActor;->mIsAutoFocusCallback:Z

    iput-boolean v2, p0, Lcom/android/camera/actor/VideoActor;->mNeedReLearningEffect:Z

    iput-boolean v2, p0, Lcom/android/camera/actor/VideoActor;->mEffectsError:Z

    iget-object v0, p0, Lcom/android/camera/actor/VideoActor;->mVideoContext:Lcom/android/camera/Camera;

    invoke-virtual {v0}, Lcom/android/camera/Camera;->getCameraDevice()Lcom/android/camera/CameraManager$CameraProxy;

    move-result-object v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/android/camera/actor/VideoActor;->mVideoContext:Lcom/android/camera/Camera;

    invoke-virtual {v0}, Lcom/android/camera/Camera;->getCameraDevice()Lcom/android/camera/CameraManager$CameraProxy;

    move-result-object v0

    invoke-virtual {v0}, Lcom/android/camera/CameraManager$CameraProxy;->cancelAutoFocus()V

    :cond_1
    iget-object v0, p0, Lcom/android/camera/actor/VideoActor;->mVideoContext:Lcom/android/camera/Camera;

    invoke-virtual {v0}, Lcom/android/camera/Camera;->getFocusManager()Lcom/android/camera/FocusManager;

    move-result-object v0

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/android/camera/actor/VideoActor;->mVideoContext:Lcom/android/camera/Camera;

    invoke-virtual {v0}, Lcom/android/camera/Camera;->getFocusManager()Lcom/android/camera/FocusManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/android/camera/FocusManager;->onPreviewStopped()V

    :cond_2
    invoke-virtual {p0}, Lcom/android/camera/actor/VideoActor;->stopVideoOnPause()V

    iget-object v0, p0, Lcom/android/camera/actor/VideoActor;->mVideoContext:Lcom/android/camera/Camera;

    invoke-virtual {v0}, Lcom/android/camera/Camera;->getCameraDevice()Lcom/android/camera/CameraManager$CameraProxy;

    move-result-object v0

    if-nez v0, :cond_3

    :goto_0
    return-void

    :cond_3
    invoke-direct {p0}, Lcom/android/camera/actor/VideoActor;->releaseEffects()V

    iget-object v0, p0, Lcom/android/camera/actor/VideoActor;->mVideoContext:Lcom/android/camera/Camera;

    invoke-virtual {v0}, Lcom/android/camera/Camera;->resetScreenOn()V

    goto :goto_0
.end method

.method public onCameraParameterReady(Z)V
    .locals 3
    .param p1    # Z

    sget-boolean v0, Lcom/android/camera/actor/VideoActor;->LOG:Z

    if-eqz v0, :cond_0

    const-string v0, "VideoActor"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "onCameraParameterReady("

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ") getCameraState()="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/android/camera/actor/VideoActor;->mVideoContext:Lcom/android/camera/Camera;

    invoke-virtual {v2}, Lcom/android/camera/Camera;->getCameraState()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/android/camera/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/android/camera/actor/VideoActor;->mVideoCameraClosed:Z

    iget-object v0, p0, Lcom/android/camera/actor/VideoActor;->mVideoContext:Lcom/android/camera/Camera;

    invoke-virtual {v0}, Lcom/android/camera/Camera;->getProfile()Landroid/media/CamcorderProfile;

    move-result-object v0

    iput-object v0, p0, Lcom/android/camera/actor/VideoActor;->mProfile:Landroid/media/CamcorderProfile;

    iget-object v0, p0, Lcom/android/camera/actor/VideoActor;->mProfile:Landroid/media/CamcorderProfile;

    invoke-static {v0}, Lcom/android/camera/Util;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    invoke-direct {p0}, Lcom/android/camera/actor/VideoActor;->initVideoRecordingFirst()V

    invoke-direct {p0}, Lcom/android/camera/actor/VideoActor;->updateEffectSelection()Z

    move-result v0

    if-nez v0, :cond_1

    if-eqz p1, :cond_1

    invoke-direct {p0}, Lcom/android/camera/actor/VideoActor;->startPreview()V

    :cond_1
    invoke-direct {p0}, Lcom/android/camera/actor/VideoActor;->restoreReviewIfNeed()V

    return-void
.end method

.method public onEffectsDeactive()V
    .locals 2

    invoke-direct {p0}, Lcom/android/camera/actor/VideoActor;->releaseEffects()V

    iget-object v0, p0, Lcom/android/camera/actor/VideoActor;->mVideoContext:Lcom/android/camera/Camera;

    invoke-virtual {v0}, Lcom/android/camera/Camera;->getShutterManager()Lcom/android/camera/manager/ShutterManager;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/android/camera/manager/ShutterManager;->setPhotoShutterEnabled(Z)V

    return-void
.end method

.method public declared-synchronized onEffectsError(Ljava/lang/Exception;Ljava/lang/String;)V
    .locals 2
    .param p1    # Ljava/lang/Exception;
    .param p2    # Ljava/lang/String;

    monitor-enter p0

    :try_start_0
    sget-boolean v0, Lcom/android/camera/actor/VideoActor;->LOG:Z

    if-eqz v0, :cond_0

    const-string v0, "VideoActor"

    const-string v1, "onEffectsError"

    invoke-static {v0, v1, p1}, Lcom/android/camera/Log;->v(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    :cond_0
    if-eqz p2, :cond_1

    new-instance v0, Ljava/io/File;

    invoke-direct {v0, p2}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-direct {p0, p2}, Lcom/android/camera/actor/VideoActor;->deleteVideoFile(Ljava/lang/String;)V

    :cond_1
    instance-of v0, p1, Landroid/filterpacks/videosink/MediaRecorderStopException;

    if-eqz v0, :cond_3

    const-string v0, "VideoActor"

    const-string v1, "Problem recoding video file. Removing incomplete file."

    invoke-static {v0, v1}, Lcom/android/camera/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    invoke-virtual {p0}, Lcom/android/camera/actor/VideoActor;->updateEffectRecordingUI()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :cond_2
    :goto_0
    monitor-exit p0

    return-void

    :cond_3
    :try_start_1
    iget-boolean v0, p0, Lcom/android/camera/actor/VideoActor;->mEffectsError:Z

    if-nez v0, :cond_2

    iget-boolean v0, p0, Lcom/android/camera/actor/VideoActor;->mVideoCameraClosed:Z

    if-nez v0, :cond_2

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/android/camera/actor/VideoActor;->mEffectsError:Z

    iget-object v0, p0, Lcom/android/camera/actor/VideoActor;->mVideoContext:Lcom/android/camera/Camera;

    const v1, 0x7f0c00af

    invoke-static {v0, v1}, Lcom/android/camera/Util;->showErrorAndFinish(Landroid/app/Activity;I)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public onEffectsUpdate(II)V
    .locals 6
    .param p1    # I
    .param p2    # I

    const/4 v5, 0x5

    const/4 v4, 0x0

    const/4 v3, 0x1

    sget-boolean v0, Lcom/android/camera/actor/VideoActor;->LOG:Z

    if-eqz v0, :cond_0

    const-string v0, "VideoActor"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "onEffectsUpdate. Effect Message = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/android/camera/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    const/4 v0, 0x3

    if-ne p2, v0, :cond_6

    iget-boolean v0, p0, Lcom/android/camera/actor/VideoActor;->mMediaRecorderRecording:Z

    if-eqz v0, :cond_2

    invoke-direct {p0}, Lcom/android/camera/actor/VideoActor;->stopVideoRecordingAsync()V

    invoke-virtual {p0}, Lcom/android/camera/actor/VideoActor;->updateEffectRecordingUI()V

    iget-object v0, p0, Lcom/android/camera/actor/VideoActor;->mVideoSavingTask:Ljava/lang/Thread;

    if-eqz v0, :cond_1

    iget-object v1, p0, Lcom/android/camera/actor/VideoActor;->mVideoSavingTask:Ljava/lang/Thread;

    monitor-enter v1

    const/4 v0, 0x1

    :try_start_0
    iput-boolean v0, p0, Lcom/android/camera/actor/VideoActor;->mRecorderCameraReleased:Z

    iget-object v0, p0, Lcom/android/camera/actor/VideoActor;->mVideoSavingTask:Ljava/lang/Thread;

    invoke-virtual {v0}, Ljava/lang/Object;->notifyAll()V

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :cond_1
    sget-boolean v0, Lcom/android/camera/actor/VideoActor;->LOG:Z

    if-eqz v0, :cond_2

    const-string v0, "VideoActor"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "onEffectsUpdate() release mVideoSavingTask="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/android/camera/actor/VideoActor;->mVideoSavingTask:Ljava/lang/Thread;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/android/camera/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    :cond_2
    iput-boolean v3, p0, Lcom/android/camera/actor/VideoActor;->mRecorderCameraReleased:Z

    iget-object v0, p0, Lcom/android/camera/actor/VideoActor;->mVideoContext:Lcom/android/camera/Camera;

    invoke-virtual {v0}, Lcom/android/camera/Camera;->dismissInfo()V

    invoke-direct {p0, v4}, Lcom/android/camera/actor/VideoActor;->hideOtherSettings(Z)V

    invoke-direct {p0}, Lcom/android/camera/actor/VideoActor;->startPreview()V

    :cond_3
    :goto_0
    iget-boolean v0, p0, Lcom/android/camera/actor/VideoActor;->mVideoCameraClosed:Z

    if-eqz v0, :cond_5

    sget-boolean v0, Lcom/android/camera/actor/VideoActor;->LOG:Z

    if-eqz v0, :cond_4

    const-string v0, "VideoActor"

    const-string v1, "OnEffectsUpdate: closing effects if activity paused"

    invoke-static {v0, v1}, Lcom/android/camera/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    :cond_4
    invoke-direct {p0, v3}, Lcom/android/camera/actor/VideoActor;->closeEffects(Z)V

    :cond_5
    return-void

    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0

    :cond_6
    const/4 v0, 0x4

    if-ne p2, v0, :cond_9

    invoke-virtual {p0}, Lcom/android/camera/actor/VideoActor;->updateEffectRecordingUI()V

    iget-boolean v0, p0, Lcom/android/camera/actor/VideoActor;->mEffectsDisplayResult:Z

    if-eqz v0, :cond_7

    invoke-direct {p0}, Lcom/android/camera/actor/VideoActor;->addVideoToMediaStore()V

    iget-object v0, p0, Lcom/android/camera/actor/VideoActor;->mVideoContext:Lcom/android/camera/Camera;

    invoke-virtual {v0}, Lcom/android/camera/Camera;->dismissProgress()V

    iget-object v0, p0, Lcom/android/camera/actor/VideoActor;->mVideoContext:Lcom/android/camera/Camera;

    invoke-virtual {v0}, Lcom/android/camera/Camera;->isVideoCaptureIntent()Z

    move-result v0

    if-eqz v0, :cond_7

    iget-object v0, p0, Lcom/android/camera/actor/VideoActor;->mVideoContext:Lcom/android/camera/Camera;

    invoke-virtual {v0}, Lcom/android/camera/Camera;->isQuickCapture()Z

    move-result v0

    if-eqz v0, :cond_8

    invoke-direct {p0, v3}, Lcom/android/camera/actor/VideoActor;->doReturnToCaller(Z)V

    :cond_7
    :goto_1
    iput-boolean v4, p0, Lcom/android/camera/actor/VideoActor;->mEffectsDisplayResult:Z

    iget-boolean v0, p0, Lcom/android/camera/actor/VideoActor;->mVideoCameraClosed:Z

    if-eqz v0, :cond_3

    invoke-direct {p0}, Lcom/android/camera/actor/VideoActor;->closeVideoFileDescriptor()V

    goto :goto_0

    :cond_8
    invoke-direct {p0}, Lcom/android/camera/actor/VideoActor;->showAlert()V

    goto :goto_1

    :cond_9
    if-ne p2, v5, :cond_c

    sget-boolean v0, Lcom/android/camera/actor/VideoActor;->LOG:Z

    if-eqz v0, :cond_a

    const-string v0, "VideoActor"

    const-string v1, "effectMsg == EffectsRecorder.EFFECT_MSG_PREVIEW_RUNNING"

    invoke-static {v0, v1}, Lcom/android/camera/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    :cond_a
    iget-object v0, p0, Lcom/android/camera/actor/VideoActor;->mVideoContext:Lcom/android/camera/Camera;

    invoke-virtual {v0}, Lcom/android/camera/Camera;->getCameraState()I

    move-result v0

    if-nez v0, :cond_b

    iget-object v0, p0, Lcom/android/camera/actor/VideoActor;->mVideoContext:Lcom/android/camera/Camera;

    invoke-virtual {v0}, Lcom/android/camera/Camera;->isCameraClosed()Z

    move-result v0

    if-nez v0, :cond_b

    iget-object v0, p0, Lcom/android/camera/actor/VideoActor;->mVideoContext:Lcom/android/camera/Camera;

    invoke-virtual {v0, v3}, Lcom/android/camera/Camera;->setCameraState(I)V

    iget-object v0, p0, Lcom/android/camera/actor/VideoActor;->mVideoContext:Lcom/android/camera/Camera;

    invoke-virtual {v0}, Lcom/android/camera/Camera;->getFocusManager()Lcom/android/camera/FocusManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/android/camera/FocusManager;->onPreviewStarted()V

    :cond_b
    iget v0, p0, Lcom/android/camera/actor/VideoActor;->mEffectType:I

    if-ne v0, v3, :cond_3

    iget-object v0, p0, Lcom/android/camera/actor/VideoActor;->mVideoContext:Lcom/android/camera/Camera;

    invoke-virtual {v0, v5}, Lcom/android/camera/Camera;->switchShutter(I)V

    iget-object v0, p0, Lcom/android/camera/actor/VideoActor;->mVideoContext:Lcom/android/camera/Camera;

    invoke-virtual {v0}, Lcom/android/camera/Camera;->getShutterManager()Lcom/android/camera/manager/ShutterManager;

    move-result-object v0

    invoke-virtual {v0, v3}, Lcom/android/camera/manager/ShutterManager;->setCancelButtonEnabled(Z)V

    iget-object v0, p0, Lcom/android/camera/actor/VideoActor;->mVideoContext:Lcom/android/camera/Camera;

    invoke-virtual {v0}, Lcom/android/camera/Camera;->getShutterManager()Lcom/android/camera/manager/ShutterManager;

    move-result-object v0

    invoke-virtual {v0, v3}, Lcom/android/camera/manager/ShutterManager;->setVideoShutterEnabled(Z)V

    goto/16 :goto_0

    :cond_c
    const/4 v0, 0x2

    if-ne p1, v0, :cond_3

    packed-switch p2, :pswitch_data_0

    goto/16 :goto_0

    :pswitch_0
    sget-boolean v0, Lcom/android/camera/actor/VideoActor;->LOG:Z

    if-eqz v0, :cond_d

    const-string v0, "VideoActor"

    const-string v1, "msg is EffectsRecorder.EFFECT_MSG_STARTED_LEARNING"

    invoke-static {v0, v1}, Lcom/android/camera/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    :cond_d
    iget-object v0, p0, Lcom/android/camera/actor/VideoActor;->mVideoContext:Lcom/android/camera/Camera;

    iget-object v1, p0, Lcom/android/camera/actor/VideoActor;->mVideoContext:Lcom/android/camera/Camera;

    const v2, 0x7f0c000b

    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    iget v2, p0, Lcom/android/camera/actor/VideoActor;->mEffectApplyTime:I

    invoke-virtual {v0, v1, v2}, Lcom/android/camera/Camera;->showInfo(Ljava/lang/String;I)V

    iget-object v0, p0, Lcom/android/camera/actor/VideoActor;->mVideoContext:Lcom/android/camera/Camera;

    invoke-virtual {v0, v5}, Lcom/android/camera/Camera;->switchShutter(I)V

    iget-object v0, p0, Lcom/android/camera/actor/VideoActor;->mVideoContext:Lcom/android/camera/Camera;

    invoke-virtual {v0}, Lcom/android/camera/Camera;->getShutterManager()Lcom/android/camera/manager/ShutterManager;

    move-result-object v0

    invoke-virtual {v0, v3}, Lcom/android/camera/manager/ShutterManager;->setCancelButtonEnabled(Z)V

    iget-object v0, p0, Lcom/android/camera/actor/VideoActor;->mVideoContext:Lcom/android/camera/Camera;

    invoke-virtual {v0}, Lcom/android/camera/Camera;->getShutterManager()Lcom/android/camera/manager/ShutterManager;

    move-result-object v0

    invoke-virtual {v0, v4}, Lcom/android/camera/manager/ShutterManager;->setVideoShutterEnabled(Z)V

    iget-object v0, p0, Lcom/android/camera/actor/VideoActor;->mVideoContext:Lcom/android/camera/Camera;

    const/16 v1, 0xa

    invoke-virtual {v0, v1}, Lcom/android/camera/Camera;->setViewState(I)V

    goto/16 :goto_0

    :pswitch_1
    invoke-virtual {p0}, Lcom/android/camera/actor/VideoActor;->updateEffectRecordingUI()V

    iget-object v0, p0, Lcom/android/camera/actor/VideoActor;->mVideoContext:Lcom/android/camera/Camera;

    invoke-virtual {v0}, Lcom/android/camera/Camera;->getShutterManager()Lcom/android/camera/manager/ShutterManager;

    move-result-object v0

    invoke-virtual {v0, v3}, Lcom/android/camera/manager/ShutterManager;->setVideoShutterEnabled(Z)V

    :pswitch_2
    iget-object v0, p0, Lcom/android/camera/actor/VideoActor;->mVideoContext:Lcom/android/camera/Camera;

    invoke-virtual {v0}, Lcom/android/camera/Camera;->dismissInfo()V

    invoke-direct {p0, v4}, Lcom/android/camera/actor/VideoActor;->hideOtherSettings(Z)V

    goto/16 :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public onError(Landroid/media/MediaRecorder;II)V
    .locals 7
    .param p1    # Landroid/media/MediaRecorder;
    .param p2    # I
    .param p3    # I

    const/4 v4, 0x0

    const-string v0, "VideoActor"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "MediaRecorder error. what="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ". extra="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/android/camera/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v0, 0x1

    if-ne p2, v0, :cond_1

    invoke-direct {p0}, Lcom/android/camera/actor/VideoActor;->stopVideoRecordingAsync()V

    :cond_0
    :goto_0
    return-void

    :cond_1
    const/16 v0, -0x44f

    if-ne p3, v0, :cond_0

    invoke-direct {p0}, Lcom/android/camera/actor/VideoActor;->onStopVideoRecordingAsync()V

    iget-object v0, p0, Lcom/android/camera/actor/VideoActor;->mVideoContext:Lcom/android/camera/Camera;

    iget-object v1, p0, Lcom/android/camera/actor/VideoActor;->mVideoContext:Lcom/android/camera/Camera;

    const v2, 0x7f0c00f3

    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/android/camera/actor/VideoActor;->mVideoContext:Lcom/android/camera/Camera;

    const v3, 0x7f0c00ac

    invoke-virtual {v2, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, Lcom/android/camera/actor/VideoActor;->mVideoContext:Lcom/android/camera/Camera;

    const v5, 0x7f0c013d

    invoke-virtual {v3, v5}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v3

    move-object v5, v4

    move-object v6, v4

    invoke-virtual/range {v0 .. v6}, Lcom/android/camera/Camera;->showAlertDialog(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Runnable;Ljava/lang/String;Ljava/lang/Runnable;)V

    goto :goto_0
.end method

.method public onInfo(Landroid/media/MediaRecorder;II)V
    .locals 5
    .param p1    # Landroid/media/MediaRecorder;
    .param p2    # I
    .param p3    # I

    const/16 v1, 0x320

    if-ne p2, v1, :cond_1

    iget-boolean v1, p0, Lcom/android/camera/actor/VideoActor;->mMediaRecorderRecording:Z

    if-eqz v1, :cond_0

    invoke-direct {p0}, Lcom/android/camera/actor/VideoActor;->onStopVideoRecordingAsync()V

    :cond_0
    :goto_0
    return-void

    :cond_1
    const/16 v1, 0x321

    if-ne p2, v1, :cond_2

    iget-boolean v1, p0, Lcom/android/camera/actor/VideoActor;->mMediaRecorderRecording:Z

    if-eqz v1, :cond_0

    invoke-direct {p0}, Lcom/android/camera/actor/VideoActor;->onStopVideoRecordingAsync()V

    iget-object v1, p0, Lcom/android/camera/actor/VideoActor;->mVideoContext:Lcom/android/camera/Camera;

    const v2, 0x7f0c0141

    invoke-virtual {v1, v2}, Lcom/android/camera/Camera;->showToast(I)V

    goto :goto_0

    :cond_2
    const/16 v1, 0x7cf

    if-ne p2, v1, :cond_4

    iget-object v1, p0, Lcom/android/camera/actor/VideoActor;->mVideoSavingTask:Ljava/lang/Thread;

    if-eqz v1, :cond_0

    iget-object v2, p0, Lcom/android/camera/actor/VideoActor;->mVideoSavingTask:Ljava/lang/Thread;

    monitor-enter v2

    :try_start_0
    sget-boolean v1, Lcom/android/camera/actor/VideoActor;->LOG:Z

    if-eqz v1, :cond_3

    const-string v1, "VideoActor"

    const-string v3, "MediaRecorder camera released, notify job wait for camera release"

    invoke-static {v1, v3}, Lcom/android/camera/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    :cond_3
    const/4 v1, 0x1

    iput-boolean v1, p0, Lcom/android/camera/actor/VideoActor;->mRecorderCameraReleased:Z

    iget-object v1, p0, Lcom/android/camera/actor/VideoActor;->mVideoSavingTask:Ljava/lang/Thread;

    invoke-virtual {v1}, Ljava/lang/Object;->notifyAll()V

    monitor-exit v2

    goto :goto_0

    :catchall_0
    move-exception v1

    monitor-exit v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1

    :cond_4
    const/16 v1, 0x7ce

    if-ne p2, v1, :cond_5

    iget-boolean v1, p0, Lcom/android/camera/actor/VideoActor;->mCaptureTimeLapse:Z

    if-nez v1, :cond_0

    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v1

    iput-wide v1, p0, Lcom/android/camera/actor/VideoActor;->mRecordingStartTime:J

    invoke-direct {p0}, Lcom/android/camera/actor/VideoActor;->updateRecordingTime()V

    goto :goto_0

    :cond_5
    const/16 v1, 0x381

    if-eq p2, v1, :cond_6

    const/16 v1, 0x382

    if-ne p2, v1, :cond_7

    :cond_6
    iget-object v1, p0, Lcom/android/camera/actor/VideoActor;->mVideoContext:Lcom/android/camera/Camera;

    const v2, 0x7f0c00ab

    invoke-virtual {v1, v2}, Lcom/android/camera/Camera;->showToast(I)V

    goto :goto_0

    :cond_7
    const/16 v1, 0x383

    if-ne p2, v1, :cond_8

    iget-object v1, p0, Lcom/android/camera/actor/VideoActor;->mVideoContext:Lcom/android/camera/Camera;

    const v2, 0x7f0c00aa

    invoke-virtual {v1, v2}, Lcom/android/camera/Camera;->showToast(I)V

    invoke-direct {p0}, Lcom/android/camera/actor/VideoActor;->stopVideoRecordingAsync()V

    goto :goto_0

    :cond_8
    const/16 v1, 0x37f

    if-ne p2, v1, :cond_0

    iget-wide v1, p0, Lcom/android/camera/actor/VideoActor;->mTotalSize:J

    const-wide/16 v3, 0x0

    cmp-long v1, v1, v3

    if-lez v1, :cond_0

    invoke-direct {p0}, Lcom/android/camera/actor/VideoActor;->effectActive()Z

    move-result v1

    if-nez v1, :cond_0

    mul-int/lit8 v1, p3, 0x64

    int-to-long v1, v1

    iget-wide v3, p0, Lcom/android/camera/actor/VideoActor;->mTotalSize:J

    div-long/2addr v1, v3

    long-to-int v0, v1

    const/16 v1, 0x64

    if-gt v0, v1, :cond_0

    sget-boolean v1, Lcom/android/camera/actor/VideoActor;->LOG:Z

    if-eqz v1, :cond_9

    const-string v1, "VideoActor"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "MEDIA_RECORDER_INFO_RECORDING_SIZE,extra= "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " progress= "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/android/camera/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    :cond_9
    iget-object v1, p0, Lcom/android/camera/actor/VideoActor;->mRecordingView:Lcom/android/camera/manager/RecordingView;

    int-to-long v2, p3

    invoke-virtual {v1, v2, v3}, Lcom/android/camera/manager/RecordingView;->setCurrentSize(J)V

    iget-object v1, p0, Lcom/android/camera/actor/VideoActor;->mRecordingView:Lcom/android/camera/manager/RecordingView;

    invoke-virtual {v1, v0}, Lcom/android/camera/manager/RecordingView;->setSizeProgress(I)V

    goto/16 :goto_0
.end method

.method public onKeyDown(ILandroid/view/KeyEvent;)Z
    .locals 3
    .param p1    # I
    .param p2    # Landroid/view/KeyEvent;

    const/4 v2, 0x0

    const/4 v0, 0x1

    iget-boolean v1, p0, Lcom/android/camera/actor/VideoActor;->mVideoCameraClosed:Z

    if-eqz v1, :cond_1

    :cond_0
    :goto_0
    return v0

    :cond_1
    sparse-switch p1, :sswitch_data_0

    :cond_2
    invoke-super {p0, p1, p2}, Lcom/android/camera/actor/CameraActor;->onKeyDown(ILandroid/view/KeyEvent;)Z

    move-result v0

    goto :goto_0

    :sswitch_0
    invoke-virtual {p2}, Landroid/view/KeyEvent;->getRepeatCount()I

    move-result v1

    if-nez v1, :cond_2

    iget-object v1, p0, Lcom/android/camera/actor/VideoActor;->mVideoContext:Lcom/android/camera/Camera;

    invoke-virtual {v1}, Lcom/android/camera/Camera;->getReviewManager()Lcom/android/camera/manager/ReviewManager;

    move-result-object v1

    invoke-virtual {v1}, Lcom/android/camera/manager/ViewManager;->isShowing()Z

    move-result v1

    if-nez v1, :cond_0

    iget-object v1, p0, Lcom/android/camera/actor/VideoActor;->mVideoShutterListener:Lcom/android/camera/ui/ShutterButton$OnShutterButtonListener;

    invoke-interface {v1, v2}, Lcom/android/camera/ui/ShutterButton$OnShutterButtonListener;->onShutterButtonClick(Lcom/android/camera/ui/ShutterButton;)V

    goto :goto_0

    :sswitch_1
    invoke-virtual {p2}, Landroid/view/KeyEvent;->getRepeatCount()I

    move-result v1

    if-nez v1, :cond_2

    iget-object v1, p0, Lcom/android/camera/actor/VideoActor;->mVideoContext:Lcom/android/camera/Camera;

    invoke-virtual {v1}, Lcom/android/camera/Camera;->getReviewManager()Lcom/android/camera/manager/ReviewManager;

    move-result-object v1

    invoke-virtual {v1}, Lcom/android/camera/manager/ViewManager;->isShowing()Z

    move-result v1

    if-nez v1, :cond_0

    iget-object v1, p0, Lcom/android/camera/actor/VideoActor;->mVideoShutterListener:Lcom/android/camera/ui/ShutterButton$OnShutterButtonListener;

    invoke-interface {v1, v2}, Lcom/android/camera/ui/ShutterButton$OnShutterButtonListener;->onShutterButtonClick(Lcom/android/camera/ui/ShutterButton;)V

    goto :goto_0

    :sswitch_2
    iget-boolean v1, p0, Lcom/android/camera/actor/VideoActor;->mMediaRecorderRecording:Z

    if-eqz v1, :cond_2

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        0x17 -> :sswitch_1
        0x1b -> :sswitch_0
        0x52 -> :sswitch_2
    .end sparse-switch
.end method

.method public onMediaEject()V
    .locals 0

    invoke-direct {p0}, Lcom/android/camera/actor/VideoActor;->stopVideoRecordingAsync()V

    return-void
.end method

.method public onOrientationChanged(I)V
    .locals 1
    .param p1    # I

    invoke-direct {p0}, Lcom/android/camera/actor/VideoActor;->effectActive()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/camera/actor/VideoActor;->mEffectsRecorder:Lcom/android/camera/actor/EffectsRecorder;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/camera/actor/VideoActor;->mEffectsRecorder:Lcom/android/camera/actor/EffectsRecorder;

    invoke-virtual {v0, p1}, Lcom/android/camera/actor/EffectsRecorder;->setOrientationHint(I)V

    :cond_0
    return-void
.end method

.method public onRestoreSettings()V
    .locals 2

    invoke-direct {p0}, Lcom/android/camera/actor/VideoActor;->releaseEffects()V

    iget-object v0, p0, Lcom/android/camera/actor/VideoActor;->mVideoContext:Lcom/android/camera/Camera;

    invoke-virtual {v0}, Lcom/android/camera/Camera;->isNonePickIntent()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/android/camera/actor/VideoActor;->hideOtherSettings(Z)V

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/android/camera/actor/VideoActor;->mVideoContext:Lcom/android/camera/Camera;

    invoke-virtual {v0}, Lcom/android/camera/Camera;->getShutterManager()Lcom/android/camera/manager/ShutterManager;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/android/camera/manager/ShutterManager;->setPhotoShutterEnabled(Z)V

    goto :goto_0
.end method

.method public onStateChange(Z)V
    .locals 3
    .param p1    # Z

    sget-boolean v0, Lcom/android/camera/actor/VideoActor;->LOG:Z

    if-eqz v0, :cond_0

    const-string v0, "VideoActor"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Effects report, surfaceTexture ready for camera = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "effect is "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-direct {p0}, Lcom/android/camera/actor/VideoActor;->effectActive()Z

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ",mVideoCameraClosed = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-boolean v2, p0, Lcom/android/camera/actor/VideoActor;->mVideoCameraClosed:Z

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/android/camera/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    iput-boolean p1, p0, Lcom/android/camera/actor/VideoActor;->mConnectApiReady:Z

    iget-object v0, p0, Lcom/android/camera/actor/VideoActor;->mVideoContext:Lcom/android/camera/Camera;

    invoke-virtual {v0, p1}, Lcom/android/camera/Camera;->setSurfaceTextureReady(Z)V

    iget-boolean v0, p0, Lcom/android/camera/actor/VideoActor;->mConnectApiReady:Z

    if-eqz v0, :cond_1

    invoke-direct {p0}, Lcom/android/camera/actor/VideoActor;->effectActive()Z

    move-result v0

    if-nez v0, :cond_1

    iget-boolean v0, p0, Lcom/android/camera/actor/VideoActor;->mVideoCameraClosed:Z

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/android/camera/actor/VideoActor;->mVideoContext:Lcom/android/camera/Camera;

    invoke-virtual {v0}, Lcom/android/camera/Camera;->getCameraDevice()Lcom/android/camera/CameraManager$CameraProxy;

    move-result-object v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/android/camera/actor/VideoActor;->mVideoContext:Lcom/android/camera/Camera;

    invoke-virtual {v0}, Lcom/android/camera/Camera;->setPreviewTextureAsync()V

    :cond_1
    return-void
.end method

.method public onUserInteraction()Z
    .locals 1

    iget-boolean v0, p0, Lcom/android/camera/actor/VideoActor;->mMediaRecorderRecording:Z

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/android/camera/actor/VideoActor;->mVideoContext:Lcom/android/camera/Camera;

    invoke-virtual {v0}, Lcom/android/camera/Camera;->keepScreenOnAwhile()V

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public playSound(I)V
    .locals 0
    .param p1    # I

    return-void
.end method

.method public readyToCapture()Z
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public setFocusParameters()V
    .locals 2

    iget-object v1, p0, Lcom/android/camera/actor/VideoActor;->mVideoContext:Lcom/android/camera/Camera;

    iget-boolean v0, p0, Lcom/android/camera/actor/VideoActor;->mIsAutoFocusCallback:Z

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-virtual {v1, v0}, Lcom/android/camera/Camera;->applyParameterForFocus(Z)V

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public startFaceDetection()V
    .locals 0

    return-void
.end method

.method public stopFaceDetection()V
    .locals 0

    return-void
.end method

.method public stopPreview()V
    .locals 3

    sget-boolean v0, Lcom/android/camera/actor/VideoActor;->LOG:Z

    if-eqz v0, :cond_0

    const-string v0, "VideoActor"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "stopPreview() mVideoContext.getCameraState()="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/android/camera/actor/VideoActor;->mVideoContext:Lcom/android/camera/Camera;

    invoke-virtual {v2}, Lcom/android/camera/Camera;->getCameraState()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/android/camera/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    iget-object v0, p0, Lcom/android/camera/actor/VideoActor;->mVideoContext:Lcom/android/camera/Camera;

    invoke-virtual {v0}, Lcom/android/camera/Camera;->getCameraState()I

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/android/camera/actor/VideoActor;->mVideoContext:Lcom/android/camera/Camera;

    invoke-virtual {v0}, Lcom/android/camera/Camera;->getCameraDevice()Lcom/android/camera/CameraManager$CameraProxy;

    move-result-object v0

    invoke-virtual {v0}, Lcom/android/camera/CameraManager$CameraProxy;->stopPreview()V

    iget-object v0, p0, Lcom/android/camera/actor/VideoActor;->mVideoContext:Lcom/android/camera/Camera;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/android/camera/Camera;->setCameraState(I)V

    :cond_1
    return-void
.end method

.method public stopVideoOnPause()V
    .locals 5

    sget-boolean v2, Lcom/android/camera/actor/VideoActor;->LOG:Z

    if-eqz v2, :cond_0

    const-string v2, "VideoActor"

    const-string v3, "stopVideoOnPause()"

    invoke-static {v2, v3}, Lcom/android/camera/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    invoke-direct {p0}, Lcom/android/camera/actor/VideoActor;->effectActive()Z

    move-result v0

    const/4 v1, 0x0

    iget-boolean v2, p0, Lcom/android/camera/actor/VideoActor;->mMediaRecorderRecording:Z

    if-eqz v2, :cond_5

    const/4 v2, 0x1

    iput-boolean v2, p0, Lcom/android/camera/actor/VideoActor;->mEffectsDisplayResult:Z

    iget-object v2, p0, Lcom/android/camera/actor/VideoActor;->mVideoContext:Lcom/android/camera/Camera;

    invoke-virtual {v2}, Lcom/android/camera/Camera;->isNonePickIntent()Z

    move-result v2

    if-nez v2, :cond_1

    if-nez v0, :cond_1

    const/4 v2, 0x4

    iput v2, p0, Lcom/android/camera/actor/VideoActor;->mStoppingAction:I

    :cond_1
    invoke-direct {p0}, Lcom/android/camera/actor/VideoActor;->stopVideoRecordingAsync()V

    invoke-direct {p0}, Lcom/android/camera/actor/VideoActor;->isVideoProcessing()Z

    move-result v1

    :cond_2
    :goto_0
    if-eqz v0, :cond_6

    iget-object v2, p0, Lcom/android/camera/actor/VideoActor;->mVideoSavingTask:Ljava/lang/Thread;

    if-eqz v2, :cond_3

    invoke-direct {p0}, Lcom/android/camera/actor/VideoActor;->waitForRecorder()V

    :cond_3
    :goto_1
    sget-boolean v2, Lcom/android/camera/actor/VideoActor;->LOG:Z

    if-eqz v2, :cond_4

    const-string v2, "VideoActor"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "stopVideoOnPause() effectsActive="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ", videoSaving="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ", mVideoSavingTask="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lcom/android/camera/actor/VideoActor;->mVideoSavingTask:Ljava/lang/Thread;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ", mMediaRecorderRecording="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-boolean v4, p0, Lcom/android/camera/actor/VideoActor;->mMediaRecorderRecording:Z

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/android/camera/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    :cond_4
    return-void

    :cond_5
    if-nez v0, :cond_2

    invoke-direct {p0}, Lcom/android/camera/actor/VideoActor;->releaseMediaRecorder()V

    goto :goto_0

    :cond_6
    if-eqz v1, :cond_7

    invoke-direct {p0}, Lcom/android/camera/actor/VideoActor;->waitForRecorder()V

    goto :goto_1

    :cond_7
    invoke-direct {p0}, Lcom/android/camera/actor/VideoActor;->closeVideoFileDescriptor()V

    goto :goto_1
.end method

.method public updateEffectRecordingUI()V
    .locals 3

    const/4 v2, 0x1

    sget-boolean v0, Lcom/android/camera/actor/VideoActor;->LOG:Z

    if-eqz v0, :cond_0

    const-string v0, "VideoActor"

    const-string v1, "updateEffectRecordingUI()"

    invoke-static {v0, v1}, Lcom/android/camera/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    iget-boolean v0, p0, Lcom/android/camera/actor/VideoActor;->mVideoCameraClosed:Z

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/android/camera/actor/VideoActor;->mVideoContext:Lcom/android/camera/Camera;

    invoke-virtual {v0}, Lcom/android/camera/Camera;->keepScreenOnAwhile()V

    iget-object v0, p0, Lcom/android/camera/actor/VideoActor;->mVideoContext:Lcom/android/camera/Camera;

    invoke-virtual {v0}, Lcom/android/camera/Camera;->dismissProgress()V

    :cond_1
    iget-object v0, p0, Lcom/android/camera/actor/VideoActor;->mVideoContext:Lcom/android/camera/Camera;

    invoke-virtual {v0}, Lcom/android/camera/Camera;->getShutterManager()Lcom/android/camera/manager/ShutterManager;

    move-result-object v0

    invoke-virtual {v0, v2}, Lcom/android/camera/manager/ShutterManager;->setVideoShutterEnabled(Z)V

    iget-object v0, p0, Lcom/android/camera/actor/VideoActor;->mVideoContext:Lcom/android/camera/Camera;

    invoke-virtual {v0}, Lcom/android/camera/Camera;->getShutterManager()Lcom/android/camera/manager/ShutterManager;

    move-result-object v0

    invoke-virtual {v0, v2}, Lcom/android/camera/manager/ShutterManager;->setCancelButtonEnabled(Z)V

    return-void
.end method
