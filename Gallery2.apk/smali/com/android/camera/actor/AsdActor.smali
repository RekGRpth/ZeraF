.class public Lcom/android/camera/actor/AsdActor;
.super Lcom/android/camera/actor/PhotoActor;
.source "AsdActor.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/camera/actor/AsdActor$AsdCameraCategory;
    }
.end annotation


# static fields
.field private static final LOG:Z

.field private static final SAVE_ORIGINAL_PICTURE:Z = true

.field private static final TAG:Ljava/lang/String; = "AsdActor"


# instance fields
.field private final mASDCaptureCallback:Landroid/hardware/Camera$ASDCallback;

.field private mOriginalSaveRequest:Lcom/android/camera/SaveRequest;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    sget-boolean v0, Lcom/android/camera/Log;->LOGV:Z

    sput-boolean v0, Lcom/android/camera/actor/AsdActor;->LOG:Z

    return-void
.end method

.method public constructor <init>(Lcom/android/camera/Camera;)V
    .locals 2
    .param p1    # Lcom/android/camera/Camera;

    invoke-direct {p0, p1}, Lcom/android/camera/actor/PhotoActor;-><init>(Lcom/android/camera/Camera;)V

    new-instance v0, Lcom/android/camera/actor/AsdActor$1;

    invoke-direct {v0, p0}, Lcom/android/camera/actor/AsdActor$1;-><init>(Lcom/android/camera/actor/AsdActor;)V

    iput-object v0, p0, Lcom/android/camera/actor/AsdActor;->mASDCaptureCallback:Landroid/hardware/Camera$ASDCallback;

    sget-boolean v0, Lcom/android/camera/actor/AsdActor;->LOG:Z

    if-eqz v0, :cond_0

    const-string v0, "AsdActor"

    const-string v1, "AsdActor initialize"

    invoke-static {v0, v1}, Lcom/android/camera/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    new-instance v0, Lcom/android/camera/actor/AsdActor$AsdCameraCategory;

    invoke-direct {v0, p0}, Lcom/android/camera/actor/AsdActor$AsdCameraCategory;-><init>(Lcom/android/camera/actor/AsdActor;)V

    iput-object v0, p0, Lcom/android/camera/actor/PhotoActor;->mCameraCategory:Lcom/android/camera/actor/PhotoActor$CameraCategory;

    iget-object v0, p0, Lcom/android/camera/actor/PhotoActor;->mCamera:Lcom/android/camera/Camera;

    invoke-virtual {v0}, Lcom/android/camera/Camera;->getIndicatorManager()Lcom/android/camera/manager/IndicatorManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/android/camera/manager/IndicatorManager;->saveSceneMode()V

    return-void
.end method

.method static synthetic access$000()Z
    .locals 1

    sget-boolean v0, Lcom/android/camera/actor/AsdActor;->LOG:Z

    return v0
.end method


# virtual methods
.method public getASDCallback()Landroid/hardware/Camera$ASDCallback;
    .locals 1

    iget-object v0, p0, Lcom/android/camera/actor/AsdActor;->mASDCaptureCallback:Landroid/hardware/Camera$ASDCallback;

    return-object v0
.end method

.method public getMode()I
    .locals 1

    const/4 v0, 0x5

    return v0
.end method

.method public getPhotoShutterButtonListener()Lcom/android/camera/ui/ShutterButton$OnShutterButtonListener;
    .locals 0

    return-object p0
.end method

.method public onCameraParameterReady(Z)V
    .locals 2
    .param p1    # Z

    invoke-super {p0, p1}, Lcom/android/camera/actor/PhotoActor;->onCameraParameterReady(Z)V

    sget-boolean v0, Lcom/android/camera/actor/AsdActor;->LOG:Z

    if-eqz v0, :cond_0

    const-string v0, "AsdActor"

    const-string v1, "AsdActor onCameraParameterReady"

    invoke-static {v0, v1}, Lcom/android/camera/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    return-void
.end method

.method public onShutterButtonLongPressed(Lcom/android/camera/ui/ShutterButton;)V
    .locals 4
    .param p1    # Lcom/android/camera/ui/ShutterButton;

    sget-boolean v0, Lcom/android/camera/actor/AsdActor;->LOG:Z

    if-eqz v0, :cond_0

    const-string v0, "AsdActor"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Asd.onShutterButtonLongPressed("

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ")"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/android/camera/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    iget-object v0, p0, Lcom/android/camera/actor/PhotoActor;->mCamera:Lcom/android/camera/Camera;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v2, p0, Lcom/android/camera/actor/PhotoActor;->mCamera:Lcom/android/camera/Camera;

    const v3, 0x7f0c005b

    invoke-virtual {v2, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/android/camera/actor/PhotoActor;->mCamera:Lcom/android/camera/Camera;

    const v3, 0x7f0c000d

    invoke-virtual {v2, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/android/camera/Camera;->showInfo(Ljava/lang/String;)V

    return-void
.end method
