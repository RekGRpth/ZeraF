.class Lcom/android/camera/manager/MMProfileManager$MMProfileWrapper;
.super Ljava/lang/Object;
.source "MMProfileManager.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/camera/manager/MMProfileManager;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "MMProfileWrapper"
.end annotation


# static fields
.field private static final MMPROFILE_FLAG_END:I = 0x2

.field private static final MMPROFILE_FLAG_EVENT_SEPARATOR:I = 0x8

.field private static final MMPROFILE_FLAG_PULSE:I = 0x4

.field private static final MMPROFILE_FLAG_START:I = 0x1

.field private static final MMP_ROOT_EVENT:I = 0x1


# direct methods
.method private constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static doMMProfileEnableEvent(II)V
    .locals 0
    .param p0    # I
    .param p1    # I

    invoke-static {p0, p1}, Lcom/mediatek/mmprofile/MMProfile;->MMProfileEnableEvent(II)V

    return-void
.end method

.method public static doMMProfileEnableEventRecursive(II)V
    .locals 0
    .param p0    # I
    .param p1    # I

    invoke-static {p0, p1}, Lcom/mediatek/mmprofile/MMProfile;->MMProfileEnableEventRecursive(II)V

    return-void
.end method

.method public static doMMProfileFindEvent(ILjava/lang/String;)I
    .locals 1
    .param p0    # I
    .param p1    # Ljava/lang/String;

    invoke-static {p0, p1}, Lcom/mediatek/mmprofile/MMProfile;->MMProfileFindEvent(ILjava/lang/String;)I

    move-result v0

    return v0
.end method

.method public static doMMProfileLog(II)V
    .locals 0
    .param p0    # I
    .param p1    # I

    invoke-static {p0, p1}, Lcom/mediatek/mmprofile/MMProfile;->MMProfileLog(II)V

    return-void
.end method

.method public static doMMProfileLogEx(IIII)V
    .locals 0
    .param p0    # I
    .param p1    # I
    .param p2    # I
    .param p3    # I

    invoke-static {p0, p1, p2, p3}, Lcom/mediatek/mmprofile/MMProfile;->MMProfileLogEx(IIII)V

    return-void
.end method

.method public static doMMProfileLogMetaString(IILjava/lang/String;)I
    .locals 1
    .param p0    # I
    .param p1    # I
    .param p2    # Ljava/lang/String;

    invoke-static {p0, p1, p2}, Lcom/mediatek/mmprofile/MMProfile;->MMProfileLogMetaString(IILjava/lang/String;)I

    move-result v0

    return v0
.end method

.method public static doMMProfileLogMetaStringEx(IIIILjava/lang/String;)I
    .locals 1
    .param p0    # I
    .param p1    # I
    .param p2    # I
    .param p3    # I
    .param p4    # Ljava/lang/String;

    invoke-static {p0, p1, p2, p3, p4}, Lcom/mediatek/mmprofile/MMProfile;->MMProfileLogMetaStringEx(IIIILjava/lang/String;)I

    move-result v0

    return v0
.end method

.method public static doMMProfileQueryEnable(I)I
    .locals 1
    .param p0    # I

    invoke-static {p0}, Lcom/mediatek/mmprofile/MMProfile;->MMProfileQueryEnable(I)I

    move-result v0

    return v0
.end method

.method public static doMMProfileRegisterEvent(ILjava/lang/String;)I
    .locals 1
    .param p0    # I
    .param p1    # Ljava/lang/String;

    invoke-static {p0, p1}, Lcom/mediatek/mmprofile/MMProfile;->MMProfileRegisterEvent(ILjava/lang/String;)I

    move-result v0

    return v0
.end method
