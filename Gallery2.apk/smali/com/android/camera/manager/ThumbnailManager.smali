.class public Lcom/android/camera/manager/ThumbnailManager;
.super Lcom/android/camera/manager/ViewManager;
.source "ThumbnailManager.java"

# interfaces
.implements Landroid/view/View$OnClickListener;
.implements Lcom/android/camera/Camera$OnFullScreenChangedListener;
.implements Lcom/android/camera/Camera$Resumable;
.implements Lcom/android/camera/FileSaver$FileSaverListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/camera/manager/ThumbnailManager$WorkerHandler;,
        Lcom/android/camera/manager/ThumbnailManager$SaveThumbnailTask;,
        Lcom/android/camera/manager/ThumbnailManager$LoadThumbnailTask;
    }
.end annotation


# static fields
.field private static final ACTION_DELETE_PICTURE:Ljava/lang/String; = "com.android.gallery3d.action.DELETE_PICTURE"

.field private static final ACTION_IPO_SHUTDOWN:Ljava/lang/String; = "android.intent.action.ACTION_SHUTDOWN_IPO"

.field private static final LOG:Z

.field private static final MSG_CHECK_THUMBNAIL:I = 0x2

.field private static final MSG_SAVE_THUMBNAIL:I = 0x0

.field private static final MSG_UPDATE_THUMBNAIL:I = 0x1

.field private static final TAG:Ljava/lang/String; = "ThumbnailManager"


# instance fields
.field private mCurrentSaveRequest:Lcom/android/camera/SaveRequest;

.field private mDeletePictureFilter:Landroid/content/IntentFilter;

.field private mDeletePictureReceiver:Landroid/content/BroadcastReceiver;

.field private mIpoShutdownFilter:Landroid/content/IntentFilter;

.field private mIpoShutdownReceiver:Landroid/content/BroadcastReceiver;

.field private mLastRefreshTime:J

.field private mLastSaveRequest:Lcom/android/camera/SaveRequest;

.field private mLoadThumbnailTask:Landroid/os/AsyncTask;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/AsyncTask",
            "<",
            "Ljava/lang/Void;",
            "Ljava/lang/Void;",
            "Lcom/android/camera/Thumbnail;",
            ">;"
        }
    .end annotation
.end field

.field private mMainHandler:Landroid/os/Handler;

.field private mRefreshInterval:J

.field private mResumed:Z

.field private mThumbnail:Lcom/android/camera/Thumbnail;

.field private mThumbnailView:Lcom/android/camera/ui/RotateImageView;

.field private mUpdateThumbnailDelayed:Z

.field private mWorkerHandler:Lcom/android/camera/manager/ThumbnailManager$WorkerHandler;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    sget-boolean v0, Lcom/android/camera/Log;->LOGV:Z

    sput-boolean v0, Lcom/android/camera/manager/ThumbnailManager;->LOG:Z

    return-void
.end method

.method public constructor <init>(Lcom/android/camera/Camera;)V
    .locals 2
    .param p1    # Lcom/android/camera/Camera;

    invoke-direct {p0, p1}, Lcom/android/camera/manager/ViewManager;-><init>(Lcom/android/camera/Camera;)V

    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/android/camera/manager/ThumbnailManager;->mRefreshInterval:J

    new-instance v0, Landroid/content/IntentFilter;

    const-string v1, "com.android.gallery3d.action.DELETE_PICTURE"

    invoke-direct {v0, v1}, Landroid/content/IntentFilter;-><init>(Ljava/lang/String;)V

    iput-object v0, p0, Lcom/android/camera/manager/ThumbnailManager;->mDeletePictureFilter:Landroid/content/IntentFilter;

    new-instance v0, Lcom/android/camera/manager/ThumbnailManager$1;

    invoke-direct {v0, p0}, Lcom/android/camera/manager/ThumbnailManager$1;-><init>(Lcom/android/camera/manager/ThumbnailManager;)V

    iput-object v0, p0, Lcom/android/camera/manager/ThumbnailManager;->mDeletePictureReceiver:Landroid/content/BroadcastReceiver;

    new-instance v0, Landroid/content/IntentFilter;

    const-string v1, "android.intent.action.ACTION_SHUTDOWN_IPO"

    invoke-direct {v0, v1}, Landroid/content/IntentFilter;-><init>(Ljava/lang/String;)V

    iput-object v0, p0, Lcom/android/camera/manager/ThumbnailManager;->mIpoShutdownFilter:Landroid/content/IntentFilter;

    new-instance v0, Lcom/android/camera/manager/ThumbnailManager$2;

    invoke-direct {v0, p0}, Lcom/android/camera/manager/ThumbnailManager$2;-><init>(Lcom/android/camera/manager/ThumbnailManager;)V

    iput-object v0, p0, Lcom/android/camera/manager/ThumbnailManager;->mIpoShutdownReceiver:Landroid/content/BroadcastReceiver;

    new-instance v0, Lcom/android/camera/manager/ThumbnailManager$3;

    invoke-direct {v0, p0}, Lcom/android/camera/manager/ThumbnailManager$3;-><init>(Lcom/android/camera/manager/ThumbnailManager;)V

    iput-object v0, p0, Lcom/android/camera/manager/ThumbnailManager;->mMainHandler:Landroid/os/Handler;

    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/android/camera/manager/ViewManager;->setFileter(Z)V

    invoke-virtual {p1, p0}, Lcom/android/camera/Camera;->addResumable(Lcom/android/camera/Camera$Resumable;)Z

    invoke-virtual {p1, p0}, Lcom/android/camera/Camera;->addOnFullScreenChangedListener(Lcom/android/camera/Camera$OnFullScreenChangedListener;)Z

    return-void
.end method

.method static synthetic access$000()Z
    .locals 1

    sget-boolean v0, Lcom/android/camera/manager/ThumbnailManager;->LOG:Z

    return v0
.end method

.method static synthetic access$100(Lcom/android/camera/manager/ThumbnailManager;)V
    .locals 0
    .param p0    # Lcom/android/camera/manager/ThumbnailManager;

    invoke-direct {p0}, Lcom/android/camera/manager/ThumbnailManager;->getLastThumbnailUncached()V

    return-void
.end method

.method static synthetic access$1000(Lcom/android/camera/manager/ThumbnailManager;)J
    .locals 2
    .param p0    # Lcom/android/camera/manager/ThumbnailManager;

    iget-wide v0, p0, Lcom/android/camera/manager/ThumbnailManager;->mRefreshInterval:J

    return-wide v0
.end method

.method static synthetic access$1100(Lcom/android/camera/manager/ThumbnailManager;)J
    .locals 2
    .param p0    # Lcom/android/camera/manager/ThumbnailManager;

    iget-wide v0, p0, Lcom/android/camera/manager/ThumbnailManager;->mLastRefreshTime:J

    return-wide v0
.end method

.method static synthetic access$1102(Lcom/android/camera/manager/ThumbnailManager;J)J
    .locals 0
    .param p0    # Lcom/android/camera/manager/ThumbnailManager;
    .param p1    # J

    iput-wide p1, p0, Lcom/android/camera/manager/ThumbnailManager;->mLastRefreshTime:J

    return-wide p1
.end method

.method static synthetic access$1200(Lcom/android/camera/manager/ThumbnailManager;)Z
    .locals 1
    .param p0    # Lcom/android/camera/manager/ThumbnailManager;

    iget-boolean v0, p0, Lcom/android/camera/manager/ThumbnailManager;->mResumed:Z

    return v0
.end method

.method static synthetic access$1300(Lcom/android/camera/manager/ThumbnailManager;)V
    .locals 0
    .param p0    # Lcom/android/camera/manager/ThumbnailManager;

    invoke-direct {p0}, Lcom/android/camera/manager/ThumbnailManager;->sendUpdateThumbnail()V

    return-void
.end method

.method static synthetic access$202(Lcom/android/camera/manager/ThumbnailManager;Z)Z
    .locals 0
    .param p0    # Lcom/android/camera/manager/ThumbnailManager;
    .param p1    # Z

    iput-boolean p1, p0, Lcom/android/camera/manager/ThumbnailManager;->mUpdateThumbnailDelayed:Z

    return p1
.end method

.method static synthetic access$300(Lcom/android/camera/manager/ThumbnailManager;)V
    .locals 0
    .param p0    # Lcom/android/camera/manager/ThumbnailManager;

    invoke-direct {p0}, Lcom/android/camera/manager/ThumbnailManager;->saveThumbnailToFile()V

    return-void
.end method

.method static synthetic access$400(Lcom/android/camera/manager/ThumbnailManager;)V
    .locals 0
    .param p0    # Lcom/android/camera/manager/ThumbnailManager;

    invoke-direct {p0}, Lcom/android/camera/manager/ThumbnailManager;->updateThumbnailView()V

    return-void
.end method

.method static synthetic access$500(Lcom/android/camera/manager/ThumbnailManager;)Lcom/android/camera/Thumbnail;
    .locals 1
    .param p0    # Lcom/android/camera/manager/ThumbnailManager;

    iget-object v0, p0, Lcom/android/camera/manager/ThumbnailManager;->mThumbnail:Lcom/android/camera/Thumbnail;

    return-object v0
.end method

.method static synthetic access$502(Lcom/android/camera/manager/ThumbnailManager;Lcom/android/camera/Thumbnail;)Lcom/android/camera/Thumbnail;
    .locals 0
    .param p0    # Lcom/android/camera/manager/ThumbnailManager;
    .param p1    # Lcom/android/camera/Thumbnail;

    iput-object p1, p0, Lcom/android/camera/manager/ThumbnailManager;->mThumbnail:Lcom/android/camera/Thumbnail;

    return-object p1
.end method

.method static synthetic access$700(Lcom/android/camera/manager/ThumbnailManager;)Lcom/android/camera/SaveRequest;
    .locals 1
    .param p0    # Lcom/android/camera/manager/ThumbnailManager;

    iget-object v0, p0, Lcom/android/camera/manager/ThumbnailManager;->mCurrentSaveRequest:Lcom/android/camera/SaveRequest;

    return-object v0
.end method

.method static synthetic access$800(Lcom/android/camera/manager/ThumbnailManager;)Lcom/android/camera/ui/RotateImageView;
    .locals 1
    .param p0    # Lcom/android/camera/manager/ThumbnailManager;

    iget-object v0, p0, Lcom/android/camera/manager/ThumbnailManager;->mThumbnailView:Lcom/android/camera/ui/RotateImageView;

    return-object v0
.end method

.method static synthetic access$900(Lcom/android/camera/manager/ThumbnailManager;)Lcom/android/camera/SaveRequest;
    .locals 1
    .param p0    # Lcom/android/camera/manager/ThumbnailManager;

    iget-object v0, p0, Lcom/android/camera/manager/ThumbnailManager;->mLastSaveRequest:Lcom/android/camera/SaveRequest;

    return-object v0
.end method

.method private getLastThumbnail()V
    .locals 3

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/android/camera/manager/ThumbnailManager;->mThumbnail:Lcom/android/camera/Thumbnail;

    invoke-direct {p0}, Lcom/android/camera/manager/ThumbnailManager;->updateThumbnailView()V

    new-instance v0, Lcom/android/camera/manager/ThumbnailManager$LoadThumbnailTask;

    const/4 v1, 0x1

    invoke-direct {v0, p0, v1}, Lcom/android/camera/manager/ThumbnailManager$LoadThumbnailTask;-><init>(Lcom/android/camera/manager/ThumbnailManager;Z)V

    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/Void;

    invoke-virtual {v0, v1}, Landroid/os/AsyncTask;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    move-result-object v0

    iput-object v0, p0, Lcom/android/camera/manager/ThumbnailManager;->mLoadThumbnailTask:Landroid/os/AsyncTask;

    sget-boolean v0, Lcom/android/camera/manager/ThumbnailManager;->LOG:Z

    if-eqz v0, :cond_0

    const-string v0, "ThumbnailManager"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "getLastThumbnail() mThumbnail="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/android/camera/manager/ThumbnailManager;->mThumbnail:Lcom/android/camera/Thumbnail;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/android/camera/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    return-void
.end method

.method private getLastThumbnailUncached()V
    .locals 3

    const/4 v2, 0x0

    iget-object v0, p0, Lcom/android/camera/manager/ThumbnailManager;->mLoadThumbnailTask:Landroid/os/AsyncTask;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/camera/manager/ThumbnailManager;->mLoadThumbnailTask:Landroid/os/AsyncTask;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/os/AsyncTask;->cancel(Z)Z

    :cond_0
    new-instance v0, Lcom/android/camera/manager/ThumbnailManager$LoadThumbnailTask;

    invoke-direct {v0, p0, v2}, Lcom/android/camera/manager/ThumbnailManager$LoadThumbnailTask;-><init>(Lcom/android/camera/manager/ThumbnailManager;Z)V

    new-array v1, v2, [Ljava/lang/Void;

    invoke-virtual {v0, v1}, Landroid/os/AsyncTask;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    move-result-object v0

    iput-object v0, p0, Lcom/android/camera/manager/ThumbnailManager;->mLoadThumbnailTask:Landroid/os/AsyncTask;

    return-void
.end method

.method private saveThumbnailToFile()V
    .locals 4

    sget-boolean v0, Lcom/android/camera/manager/ThumbnailManager;->LOG:Z

    if-eqz v0, :cond_0

    const-string v0, "ThumbnailManager"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "saveThumbnailToFile() mThumbnail="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/android/camera/manager/ThumbnailManager;->mThumbnail:Lcom/android/camera/Thumbnail;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/android/camera/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    iget-object v0, p0, Lcom/android/camera/manager/ThumbnailManager;->mThumbnail:Lcom/android/camera/Thumbnail;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/android/camera/manager/ThumbnailManager;->mThumbnail:Lcom/android/camera/Thumbnail;

    invoke-virtual {v0}, Lcom/android/camera/Thumbnail;->fromFile()Z

    move-result v0

    if-nez v0, :cond_1

    new-instance v0, Lcom/android/camera/manager/ThumbnailManager$SaveThumbnailTask;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/android/camera/manager/ThumbnailManager$SaveThumbnailTask;-><init>(Lcom/android/camera/manager/ThumbnailManager;Lcom/android/camera/manager/ThumbnailManager$1;)V

    const/4 v1, 0x1

    new-array v1, v1, [Lcom/android/camera/Thumbnail;

    const/4 v2, 0x0

    iget-object v3, p0, Lcom/android/camera/manager/ThumbnailManager;->mThumbnail:Lcom/android/camera/Thumbnail;

    aput-object v3, v1, v2

    invoke-virtual {v0, v1}, Landroid/os/AsyncTask;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    :cond_1
    return-void
.end method

.method private sendUpdateThumbnail()V
    .locals 4

    const/4 v3, 0x1

    iget-object v1, p0, Lcom/android/camera/manager/ThumbnailManager;->mMainHandler:Landroid/os/Handler;

    invoke-virtual {v1, v3}, Landroid/os/Handler;->removeMessages(I)V

    iget-object v1, p0, Lcom/android/camera/manager/ThumbnailManager;->mMainHandler:Landroid/os/Handler;

    iget-object v2, p0, Lcom/android/camera/manager/ThumbnailManager;->mThumbnail:Lcom/android/camera/Thumbnail;

    invoke-virtual {v1, v3, v2}, Landroid/os/Handler;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v0

    invoke-virtual {v0}, Landroid/os/Message;->sendToTarget()V

    return-void
.end method

.method private updateThumbnailView()V
    .locals 4

    const/4 v3, 0x4

    sget-boolean v0, Lcom/android/camera/manager/ThumbnailManager;->LOG:Z

    if-eqz v0, :cond_0

    const-string v0, "ThumbnailManager"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "updateThumbnailView() mThumbnailView="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/android/camera/manager/ThumbnailManager;->mThumbnailView:Lcom/android/camera/ui/RotateImageView;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", mThumbnail="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/android/camera/manager/ThumbnailManager;->mThumbnail:Lcom/android/camera/Thumbnail;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", isShowing="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lcom/android/camera/manager/ViewManager;->isShowing()Z

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/android/camera/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    iget-object v0, p0, Lcom/android/camera/manager/ThumbnailManager;->mThumbnailView:Lcom/android/camera/ui/RotateImageView;

    if-eqz v0, :cond_1

    invoke-virtual {p0}, Lcom/android/camera/manager/ViewManager;->isShowing()Z

    move-result v0

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/android/camera/manager/ThumbnailManager;->mThumbnail:Lcom/android/camera/Thumbnail;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/android/camera/manager/ThumbnailManager;->mThumbnail:Lcom/android/camera/Thumbnail;

    invoke-virtual {v0}, Lcom/android/camera/Thumbnail;->getBitmap()Landroid/graphics/Bitmap;

    move-result-object v0

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/android/camera/manager/ThumbnailManager;->mThumbnailView:Lcom/android/camera/ui/RotateImageView;

    iget-object v1, p0, Lcom/android/camera/manager/ThumbnailManager;->mThumbnail:Lcom/android/camera/Thumbnail;

    invoke-virtual {v1}, Lcom/android/camera/Thumbnail;->getBitmap()Landroid/graphics/Bitmap;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/android/camera/ui/RotateImageView;->setBitmap(Landroid/graphics/Bitmap;)V

    iget-object v0, p0, Lcom/android/camera/manager/ThumbnailManager;->mThumbnailView:Lcom/android/camera/ui/RotateImageView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    :goto_0
    sget-boolean v0, Lcom/android/camera/manager/ThumbnailManager;->LOG:Z

    if-eqz v0, :cond_1

    const-string v0, "ThumbnailManager"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "updateThumbnailView() "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/android/camera/manager/ThumbnailManager;->mThumbnailView:Lcom/android/camera/ui/RotateImageView;

    invoke-virtual {v2}, Landroid/view/View;->getVisibility()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/android/camera/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    :cond_1
    return-void

    :cond_2
    iget-object v0, p0, Lcom/android/camera/manager/ThumbnailManager;->mThumbnailView:Lcom/android/camera/ui/RotateImageView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/android/camera/ui/RotateImageView;->setBitmap(Landroid/graphics/Bitmap;)V

    iget-object v0, p0, Lcom/android/camera/manager/ThumbnailManager;->mThumbnailView:Lcom/android/camera/ui/RotateImageView;

    invoke-virtual {v0, v3}, Landroid/widget/ImageView;->setVisibility(I)V

    goto :goto_0

    :cond_3
    iget-object v0, p0, Lcom/android/camera/manager/ThumbnailManager;->mThumbnailView:Lcom/android/camera/ui/RotateImageView;

    invoke-virtual {v0, v3}, Landroid/widget/ImageView;->setVisibility(I)V

    goto :goto_0
.end method


# virtual methods
.method public begin()V
    .locals 3

    iget-object v1, p0, Lcom/android/camera/manager/ThumbnailManager;->mWorkerHandler:Lcom/android/camera/manager/ThumbnailManager$WorkerHandler;

    if-nez v1, :cond_0

    new-instance v0, Landroid/os/HandlerThread;

    const-string v1, "thumbnail-creation-thread"

    invoke-direct {v0, v1}, Landroid/os/HandlerThread;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/lang/Thread;->start()V

    new-instance v1, Lcom/android/camera/manager/ThumbnailManager$WorkerHandler;

    invoke-virtual {v0}, Landroid/os/HandlerThread;->getLooper()Landroid/os/Looper;

    move-result-object v2

    invoke-direct {v1, p0, v2}, Lcom/android/camera/manager/ThumbnailManager$WorkerHandler;-><init>(Lcom/android/camera/manager/ThumbnailManager;Landroid/os/Looper;)V

    iput-object v1, p0, Lcom/android/camera/manager/ThumbnailManager;->mWorkerHandler:Lcom/android/camera/manager/ThumbnailManager$WorkerHandler;

    :cond_0
    return-void
.end method

.method public finish()V
    .locals 1

    iget-object v0, p0, Lcom/android/camera/manager/ThumbnailManager;->mWorkerHandler:Lcom/android/camera/manager/ThumbnailManager$WorkerHandler;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/camera/manager/ThumbnailManager;->mWorkerHandler:Lcom/android/camera/manager/ThumbnailManager$WorkerHandler;

    invoke-virtual {v0}, Landroid/os/Handler;->getLooper()Landroid/os/Looper;

    move-result-object v0

    invoke-virtual {v0}, Landroid/os/Looper;->quit()V

    :cond_0
    return-void
.end method

.method public forceUpdate()V
    .locals 0

    invoke-direct {p0}, Lcom/android/camera/manager/ThumbnailManager;->getLastThumbnail()V

    return-void
.end method

.method protected getView()Landroid/view/View;
    .locals 2

    const v1, 0x7f04005b

    invoke-virtual {p0, v1}, Lcom/android/camera/manager/ViewManager;->inflate(I)Landroid/view/View;

    move-result-object v0

    const v1, 0x7f0b013b

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/android/camera/ui/RotateImageView;

    iput-object v1, p0, Lcom/android/camera/manager/ThumbnailManager;->mThumbnailView:Lcom/android/camera/ui/RotateImageView;

    iget-object v1, p0, Lcom/android/camera/manager/ThumbnailManager;->mThumbnailView:Lcom/android/camera/ui/RotateImageView;

    invoke-virtual {v1, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    return-object v0
.end method

.method public onClick(Landroid/view/View;)V
    .locals 3
    .param p1    # Landroid/view/View;

    sget-boolean v0, Lcom/android/camera/manager/ThumbnailManager;->LOG:Z

    if-eqz v0, :cond_0

    const-string v0, "ThumbnailManager"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "onClick() mThumbnail="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/android/camera/manager/ThumbnailManager;->mThumbnail:Lcom/android/camera/Thumbnail;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " really="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lcom/android/camera/manager/ViewManager;->getContext()Lcom/android/camera/Camera;

    move-result-object v2

    invoke-virtual {v2}, Lcom/android/camera/ActivityBase;->isFullScreen()Z

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/android/camera/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    invoke-virtual {p0}, Lcom/android/camera/manager/ViewManager;->getContext()Lcom/android/camera/Camera;

    move-result-object v0

    invoke-virtual {v0}, Lcom/android/camera/ActivityBase;->isFullScreen()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-virtual {p0}, Lcom/android/camera/manager/ViewManager;->getContext()Lcom/android/camera/Camera;

    move-result-object v0

    invoke-virtual {v0}, Lcom/android/camera/Camera;->isCameraIdle()Z

    move-result v0

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/android/camera/manager/ThumbnailManager;->mThumbnail:Lcom/android/camera/Thumbnail;

    if-eqz v0, :cond_2

    invoke-virtual {p0}, Lcom/android/camera/manager/ViewManager;->getContext()Lcom/android/camera/Camera;

    move-result-object v0

    invoke-virtual {v0}, Lcom/android/camera/Camera;->getFileSaver()Lcom/android/camera/FileSaver;

    move-result-object v0

    if-eqz v0, :cond_1

    invoke-virtual {p0}, Lcom/android/camera/manager/ViewManager;->getContext()Lcom/android/camera/Camera;

    move-result-object v0

    invoke-virtual {v0}, Lcom/android/camera/Camera;->getFileSaver()Lcom/android/camera/FileSaver;

    move-result-object v0

    invoke-virtual {v0}, Lcom/android/camera/FileSaver;->waitDone()V

    :cond_1
    invoke-virtual {p0}, Lcom/android/camera/manager/ViewManager;->getContext()Lcom/android/camera/Camera;

    move-result-object v0

    invoke-virtual {v0}, Lcom/android/camera/ActivityBase;->gotoGallery()V

    :cond_2
    return-void
.end method

.method public onFileSaved(Lcom/android/camera/SaveRequest;)V
    .locals 4
    .param p1    # Lcom/android/camera/SaveRequest;

    const/4 v3, 0x0

    sget-boolean v0, Lcom/android/camera/manager/ThumbnailManager;->LOG:Z

    if-eqz v0, :cond_0

    const-string v0, "ThumbnailManager"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "onFileSaved("

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ") ignore="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-interface {p1}, Lcom/android/camera/SaveRequest;->isIgnoreThumbnail()Z

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/android/camera/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    invoke-interface {p1}, Lcom/android/camera/SaveRequest;->isIgnoreThumbnail()Z

    move-result v0

    if-nez v0, :cond_1

    invoke-interface {p1}, Lcom/android/camera/SaveRequest;->getUri()Landroid/net/Uri;

    move-result-object v0

    if-eqz v0, :cond_1

    iput-object p1, p0, Lcom/android/camera/manager/ThumbnailManager;->mCurrentSaveRequest:Lcom/android/camera/SaveRequest;

    iget-object v0, p0, Lcom/android/camera/manager/ThumbnailManager;->mWorkerHandler:Lcom/android/camera/manager/ThumbnailManager$WorkerHandler;

    invoke-virtual {v0, v3}, Landroid/os/Handler;->removeMessages(I)V

    iget-object v0, p0, Lcom/android/camera/manager/ThumbnailManager;->mWorkerHandler:Lcom/android/camera/manager/ThumbnailManager$WorkerHandler;

    invoke-virtual {v0, v3}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    :cond_1
    return-void
.end method

.method public onFullScreenChanged(Z)V
    .locals 2
    .param p1    # Z

    if-eqz p1, :cond_1

    iget-boolean v0, p0, Lcom/android/camera/manager/ThumbnailManager;->mUpdateThumbnailDelayed:Z

    if-eqz v0, :cond_1

    invoke-direct {p0}, Lcom/android/camera/manager/ThumbnailManager;->getLastThumbnailUncached()V

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/android/camera/manager/ThumbnailManager;->mUpdateThumbnailDelayed:Z

    :cond_0
    :goto_0
    return-void

    :cond_1
    invoke-direct {p0}, Lcom/android/camera/manager/ThumbnailManager;->updateThumbnailView()V

    iget-object v0, p0, Lcom/android/camera/manager/ThumbnailManager;->mWorkerHandler:Lcom/android/camera/manager/ThumbnailManager$WorkerHandler;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/camera/manager/ThumbnailManager;->mWorkerHandler:Lcom/android/camera/manager/ThumbnailManager$WorkerHandler;

    const/4 v1, 0x2

    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    goto :goto_0
.end method

.method protected onRefresh()V
    .locals 0

    invoke-direct {p0}, Lcom/android/camera/manager/ThumbnailManager;->updateThumbnailView()V

    return-void
.end method

.method public pause()V
    .locals 4

    sget-boolean v1, Lcom/android/camera/manager/ThumbnailManager;->LOG:Z

    if-eqz v1, :cond_0

    const-string v1, "ThumbnailManager"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "pause() mResumed="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-boolean v3, p0, Lcom/android/camera/manager/ThumbnailManager;->mResumed:Z

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/android/camera/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    iget-boolean v1, p0, Lcom/android/camera/manager/ThumbnailManager;->mResumed:Z

    if-eqz v1, :cond_2

    invoke-virtual {p0}, Lcom/android/camera/manager/ViewManager;->getContext()Lcom/android/camera/Camera;

    move-result-object v1

    invoke-static {v1}, Lvedroid/support/v4/content/LocalBroadcastManager;->getInstance(Landroid/content/Context;)Lvedroid/support/v4/content/LocalBroadcastManager;

    move-result-object v0

    iget-object v1, p0, Lcom/android/camera/manager/ThumbnailManager;->mDeletePictureReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {v0, v1}, Lvedroid/support/v4/content/LocalBroadcastManager;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    invoke-virtual {p0}, Lcom/android/camera/manager/ViewManager;->getContext()Lcom/android/camera/Camera;

    move-result-object v1

    iget-object v2, p0, Lcom/android/camera/manager/ThumbnailManager;->mIpoShutdownReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {v1, v2}, Landroid/content/ContextWrapper;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    iget-object v1, p0, Lcom/android/camera/manager/ThumbnailManager;->mLoadThumbnailTask:Landroid/os/AsyncTask;

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/android/camera/manager/ThumbnailManager;->mLoadThumbnailTask:Landroid/os/AsyncTask;

    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Landroid/os/AsyncTask;->cancel(Z)Z

    const/4 v1, 0x0

    iput-object v1, p0, Lcom/android/camera/manager/ThumbnailManager;->mLoadThumbnailTask:Landroid/os/AsyncTask;

    :cond_1
    invoke-direct {p0}, Lcom/android/camera/manager/ThumbnailManager;->saveThumbnailToFile()V

    const/4 v1, 0x0

    iput-boolean v1, p0, Lcom/android/camera/manager/ThumbnailManager;->mResumed:Z

    :cond_2
    return-void
.end method

.method public resume()V
    .locals 4

    sget-boolean v1, Lcom/android/camera/manager/ThumbnailManager;->LOG:Z

    if-eqz v1, :cond_0

    const-string v1, "ThumbnailManager"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "resume() mResumed="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-boolean v3, p0, Lcom/android/camera/manager/ThumbnailManager;->mResumed:Z

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/android/camera/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    iget-boolean v1, p0, Lcom/android/camera/manager/ThumbnailManager;->mResumed:Z

    if-nez v1, :cond_2

    invoke-virtual {p0}, Lcom/android/camera/manager/ViewManager;->getContext()Lcom/android/camera/Camera;

    move-result-object v1

    invoke-static {v1}, Lvedroid/support/v4/content/LocalBroadcastManager;->getInstance(Landroid/content/Context;)Lvedroid/support/v4/content/LocalBroadcastManager;

    move-result-object v0

    iget-object v1, p0, Lcom/android/camera/manager/ThumbnailManager;->mDeletePictureReceiver:Landroid/content/BroadcastReceiver;

    iget-object v2, p0, Lcom/android/camera/manager/ThumbnailManager;->mDeletePictureFilter:Landroid/content/IntentFilter;

    invoke-virtual {v0, v1, v2}, Lvedroid/support/v4/content/LocalBroadcastManager;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)V

    invoke-virtual {p0}, Lcom/android/camera/manager/ViewManager;->getContext()Lcom/android/camera/Camera;

    move-result-object v1

    iget-object v2, p0, Lcom/android/camera/manager/ThumbnailManager;->mIpoShutdownReceiver:Landroid/content/BroadcastReceiver;

    iget-object v3, p0, Lcom/android/camera/manager/ThumbnailManager;->mIpoShutdownFilter:Landroid/content/IntentFilter;

    invoke-virtual {v1, v2, v3}, Landroid/content/ContextWrapper;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    invoke-virtual {p0}, Lcom/android/camera/manager/ViewManager;->isShowing()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-direct {p0}, Lcom/android/camera/manager/ThumbnailManager;->getLastThumbnail()V

    :cond_1
    const/4 v1, 0x1

    iput-boolean v1, p0, Lcom/android/camera/manager/ThumbnailManager;->mResumed:Z

    :cond_2
    return-void
.end method

.method public setEnabled(Z)V
    .locals 3
    .param p1    # Z

    sget-boolean v0, Lcom/android/camera/manager/ThumbnailManager;->LOG:Z

    if-eqz v0, :cond_0

    const-string v0, "ThumbnailManager"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "setEnabled "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " isenable="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lcom/android/camera/manager/ViewManager;->isEnabled()Z

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/android/camera/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    invoke-super {p0, p1}, Lcom/android/camera/manager/ViewManager;->setEnabled(Z)V

    iget-object v0, p0, Lcom/android/camera/manager/ThumbnailManager;->mThumbnailView:Lcom/android/camera/ui/RotateImageView;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/android/camera/manager/ThumbnailManager;->mThumbnailView:Lcom/android/camera/ui/RotateImageView;

    invoke-virtual {v0, p1}, Lcom/android/camera/ui/TwoStateImageView;->setEnabled(Z)V

    iget-object v0, p0, Lcom/android/camera/manager/ThumbnailManager;->mThumbnailView:Lcom/android/camera/ui/RotateImageView;

    invoke-virtual {v0, p1}, Landroid/view/View;->setClickable(Z)V

    :cond_1
    return-void
.end method

.method public setFileSaver(Lcom/android/camera/FileSaver;)V
    .locals 0
    .param p1    # Lcom/android/camera/FileSaver;

    if-eqz p1, :cond_0

    invoke-virtual {p1, p0}, Lcom/android/camera/FileSaver;->addListener(Lcom/android/camera/FileSaver$FileSaverListener;)Z

    :cond_0
    return-void
.end method

.method public setRefreshInterval(I)V
    .locals 3
    .param p1    # I

    sget-boolean v0, Lcom/android/camera/manager/ThumbnailManager;->LOG:Z

    if-eqz v0, :cond_0

    const-string v0, "ThumbnailManager"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "setRefreshInterval("

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ")"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/android/camera/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    int-to-long v0, p1

    iput-wide v0, p0, Lcom/android/camera/manager/ThumbnailManager;->mRefreshInterval:J

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/android/camera/manager/ThumbnailManager;->mLastRefreshTime:J

    return-void
.end method
