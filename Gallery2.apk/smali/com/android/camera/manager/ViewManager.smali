.class public abstract Lcom/android/camera/manager/ViewManager;
.super Ljava/lang/Object;
.source "ViewManager.java"

# interfaces
.implements Lcom/android/camera/Camera$OnOrientationListener;


# static fields
.field private static final LOG:Z

.field private static final TAG:Ljava/lang/String; = "ViewManager"

.field public static final UNKNOWN:I = -0x1

.field public static final VIEW_LAYER_BOTTOM:I = -0x1

.field public static final VIEW_LAYER_NORMAL:I = 0x0

.field public static final VIEW_LAYER_OVERLAY:I = 0x4

.field public static final VIEW_LAYER_SETTING:I = 0x3

.field public static final VIEW_LAYER_SHUTTER:I = 0x2

.field public static final VIEW_LAYER_TOP:I = 0x1


# instance fields
.field private mConfigOrientation:I

.field private mContext:Lcom/android/camera/Camera;

.field private mEnabled:Z

.field private mFadeIn:Landroid/view/animation/Animation;

.field private mFadeOut:Landroid/view/animation/Animation;

.field private mFilter:Z

.field private mHideAnimationEnabled:Z

.field private mOrientation:I

.field private mShowAnimationEnabled:Z

.field private mShowing:Z

.field private mView:Landroid/view/View;

.field private final mViewLayer:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    sget-boolean v0, Lcom/android/camera/Log;->LOGV:Z

    sput-boolean v0, Lcom/android/camera/manager/ViewManager;->LOG:Z

    return-void
.end method

.method public constructor <init>(Lcom/android/camera/Camera;)V
    .locals 1
    .param p1    # Lcom/android/camera/Camera;

    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/android/camera/manager/ViewManager;-><init>(Lcom/android/camera/Camera;I)V

    return-void
.end method

.method public constructor <init>(Lcom/android/camera/Camera;I)V
    .locals 1
    .param p1    # Lcom/android/camera/Camera;
    .param p2    # I

    const/4 v0, 0x1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-boolean v0, p0, Lcom/android/camera/manager/ViewManager;->mEnabled:Z

    iput-boolean v0, p0, Lcom/android/camera/manager/ViewManager;->mFilter:Z

    iput-boolean v0, p0, Lcom/android/camera/manager/ViewManager;->mShowAnimationEnabled:Z

    iput-boolean v0, p0, Lcom/android/camera/manager/ViewManager;->mHideAnimationEnabled:Z

    const/4 v0, -0x1

    iput v0, p0, Lcom/android/camera/manager/ViewManager;->mConfigOrientation:I

    iput-object p1, p0, Lcom/android/camera/manager/ViewManager;->mContext:Lcom/android/camera/Camera;

    iget-object v0, p0, Lcom/android/camera/manager/ViewManager;->mContext:Lcom/android/camera/Camera;

    invoke-virtual {v0, p0}, Lcom/android/camera/Camera;->addViewManager(Lcom/android/camera/manager/ViewManager;)Z

    iget-object v0, p0, Lcom/android/camera/manager/ViewManager;->mContext:Lcom/android/camera/Camera;

    invoke-virtual {v0, p0}, Lcom/android/camera/Camera;->addOnOrientationListener(Lcom/android/camera/Camera$OnOrientationListener;)Z

    iget-object v0, p0, Lcom/android/camera/manager/ViewManager;->mContext:Lcom/android/camera/Camera;

    invoke-virtual {v0}, Lcom/android/camera/Camera;->getOrientationCompensation()I

    move-result v0

    iput v0, p0, Lcom/android/camera/manager/ViewManager;->mOrientation:I

    iput p2, p0, Lcom/android/camera/manager/ViewManager;->mViewLayer:I

    return-void
.end method


# virtual methods
.method public checkConfiguration()V
    .locals 4

    iget-object v1, p0, Lcom/android/camera/manager/ViewManager;->mContext:Lcom/android/camera/Camera;

    invoke-virtual {v1}, Landroid/view/ContextThemeWrapper;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v1

    iget v0, v1, Landroid/content/res/Configuration;->orientation:I

    sget-boolean v1, Lcom/android/camera/manager/ViewManager;->LOG:Z

    if-eqz v1, :cond_0

    const-string v1, "ViewManager"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "checkConfiguration() mConfigOrientation="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget v3, p0, Lcom/android/camera/manager/ViewManager;->mConfigOrientation:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ", newConfigOrientation="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ", this="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/android/camera/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    iget v1, p0, Lcom/android/camera/manager/ViewManager;->mConfigOrientation:I

    const/4 v2, -0x1

    if-eq v1, v2, :cond_1

    iget v1, p0, Lcom/android/camera/manager/ViewManager;->mConfigOrientation:I

    if-eq v0, v1, :cond_1

    invoke-virtual {p0}, Lcom/android/camera/manager/ViewManager;->reInflate()V

    :cond_1
    return-void
.end method

.method public collapse(Z)Z
    .locals 1
    .param p1    # Z

    const/4 v0, 0x0

    return v0
.end method

.method protected fadeIn()V
    .locals 2

    iget-boolean v0, p0, Lcom/android/camera/manager/ViewManager;->mShowAnimationEnabled:Z

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/android/camera/manager/ViewManager;->mFadeIn:Landroid/view/animation/Animation;

    if-nez v0, :cond_0

    invoke-virtual {p0}, Lcom/android/camera/manager/ViewManager;->getFadeInAnimation()Landroid/view/animation/Animation;

    move-result-object v0

    iput-object v0, p0, Lcom/android/camera/manager/ViewManager;->mFadeIn:Landroid/view/animation/Animation;

    :cond_0
    iget-object v0, p0, Lcom/android/camera/manager/ViewManager;->mFadeIn:Landroid/view/animation/Animation;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/android/camera/manager/ViewManager;->mView:Landroid/view/View;

    iget-object v1, p0, Lcom/android/camera/manager/ViewManager;->mFadeIn:Landroid/view/animation/Animation;

    invoke-virtual {v0, v1}, Landroid/view/View;->startAnimation(Landroid/view/animation/Animation;)V

    :cond_1
    :goto_0
    return-void

    :cond_2
    iget-object v0, p0, Lcom/android/camera/manager/ViewManager;->mView:Landroid/view/View;

    invoke-static {v0}, Lcom/android/camera/Util;->fadeIn(Landroid/view/View;)V

    goto :goto_0
.end method

.method protected fadeOut()V
    .locals 2

    iget-boolean v0, p0, Lcom/android/camera/manager/ViewManager;->mHideAnimationEnabled:Z

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/android/camera/manager/ViewManager;->mFadeOut:Landroid/view/animation/Animation;

    if-nez v0, :cond_0

    invoke-virtual {p0}, Lcom/android/camera/manager/ViewManager;->getFadeOutAnimation()Landroid/view/animation/Animation;

    move-result-object v0

    iput-object v0, p0, Lcom/android/camera/manager/ViewManager;->mFadeOut:Landroid/view/animation/Animation;

    :cond_0
    iget-object v0, p0, Lcom/android/camera/manager/ViewManager;->mFadeOut:Landroid/view/animation/Animation;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/android/camera/manager/ViewManager;->mView:Landroid/view/View;

    iget-object v1, p0, Lcom/android/camera/manager/ViewManager;->mFadeOut:Landroid/view/animation/Animation;

    invoke-virtual {v0, v1}, Landroid/view/View;->startAnimation(Landroid/view/animation/Animation;)V

    :cond_1
    :goto_0
    return-void

    :cond_2
    iget-object v0, p0, Lcom/android/camera/manager/ViewManager;->mView:Landroid/view/View;

    invoke-static {v0}, Lcom/android/camera/Util;->fadeOut(Landroid/view/View;)V

    goto :goto_0
.end method

.method public final getContext()Lcom/android/camera/Camera;
    .locals 1

    iget-object v0, p0, Lcom/android/camera/manager/ViewManager;->mContext:Lcom/android/camera/Camera;

    return-object v0
.end method

.method protected getFadeInAnimation()Landroid/view/animation/Animation;
    .locals 1

    const/4 v0, 0x0

    return-object v0
.end method

.method protected getFadeOutAnimation()Landroid/view/animation/Animation;
    .locals 1

    const/4 v0, 0x0

    return-object v0
.end method

.method public getHideAnimationEnabled()Z
    .locals 1

    iget-boolean v0, p0, Lcom/android/camera/manager/ViewManager;->mHideAnimationEnabled:Z

    return v0
.end method

.method public getOrientation()I
    .locals 1

    iget v0, p0, Lcom/android/camera/manager/ViewManager;->mOrientation:I

    return v0
.end method

.method public getShowAnimationEnabled()Z
    .locals 1

    iget-boolean v0, p0, Lcom/android/camera/manager/ViewManager;->mShowAnimationEnabled:Z

    return v0
.end method

.method protected abstract getView()Landroid/view/View;
.end method

.method public getViewLayer()I
    .locals 1

    iget v0, p0, Lcom/android/camera/manager/ViewManager;->mViewLayer:I

    return v0
.end method

.method public hide()V
    .locals 3

    sget-boolean v0, Lcom/android/camera/manager/ViewManager;->LOG:Z

    if-eqz v0, :cond_0

    const-string v0, "ViewManager"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "hide() "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/android/camera/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    iget-object v0, p0, Lcom/android/camera/manager/ViewManager;->mView:Landroid/view/View;

    if-eqz v0, :cond_1

    iget-boolean v0, p0, Lcom/android/camera/manager/ViewManager;->mShowing:Z

    if-eqz v0, :cond_1

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/android/camera/manager/ViewManager;->mShowing:Z

    invoke-virtual {p0}, Lcom/android/camera/manager/ViewManager;->fadeOut()V

    iget-object v0, p0, Lcom/android/camera/manager/ViewManager;->mView:Landroid/view/View;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    :cond_1
    return-void
.end method

.method public final inflate(I)Landroid/view/View;
    .locals 2
    .param p1    # I

    invoke-virtual {p0}, Lcom/android/camera/manager/ViewManager;->getContext()Lcom/android/camera/Camera;

    move-result-object v0

    iget v1, p0, Lcom/android/camera/manager/ViewManager;->mViewLayer:I

    invoke-virtual {v0, p1, v1}, Lcom/android/camera/Camera;->inflate(II)Landroid/view/View;

    move-result-object v0

    return-object v0
.end method

.method public isEnabled()Z
    .locals 1

    iget-boolean v0, p0, Lcom/android/camera/manager/ViewManager;->mEnabled:Z

    return v0
.end method

.method public isShowing()Z
    .locals 1

    iget-boolean v0, p0, Lcom/android/camera/manager/ViewManager;->mShowing:Z

    return v0
.end method

.method public onOrientationChanged(I)V
    .locals 3
    .param p1    # I

    iget v0, p0, Lcom/android/camera/manager/ViewManager;->mOrientation:I

    if-eq v0, p1, :cond_0

    iput p1, p0, Lcom/android/camera/manager/ViewManager;->mOrientation:I

    iget-object v0, p0, Lcom/android/camera/manager/ViewManager;->mView:Landroid/view/View;

    iget v1, p0, Lcom/android/camera/manager/ViewManager;->mOrientation:I

    const/4 v2, 0x1

    invoke-static {v0, v1, v2}, Lcom/android/camera/Util;->setOrientation(Landroid/view/View;IZ)V

    :cond_0
    return-void
.end method

.method protected onRefresh()V
    .locals 0

    return-void
.end method

.method protected onRelease()V
    .locals 0

    return-void
.end method

.method public final reInflate()V
    .locals 4

    iget-boolean v0, p0, Lcom/android/camera/manager/ViewManager;->mShowing:Z

    invoke-virtual {p0}, Lcom/android/camera/manager/ViewManager;->hide()V

    iget-object v1, p0, Lcom/android/camera/manager/ViewManager;->mView:Landroid/view/View;

    if-eqz v1, :cond_0

    invoke-virtual {p0}, Lcom/android/camera/manager/ViewManager;->getContext()Lcom/android/camera/Camera;

    move-result-object v1

    iget-object v2, p0, Lcom/android/camera/manager/ViewManager;->mView:Landroid/view/View;

    iget v3, p0, Lcom/android/camera/manager/ViewManager;->mViewLayer:I

    invoke-virtual {v1, v2, v3}, Lcom/android/camera/Camera;->removeView(Landroid/view/View;I)V

    :cond_0
    invoke-virtual {p0}, Lcom/android/camera/manager/ViewManager;->onRelease()V

    const/4 v1, 0x0

    iput-object v1, p0, Lcom/android/camera/manager/ViewManager;->mView:Landroid/view/View;

    if-eqz v0, :cond_1

    invoke-virtual {p0}, Lcom/android/camera/manager/ViewManager;->show()V

    :cond_1
    return-void
.end method

.method public final refresh()V
    .locals 1

    iget-boolean v0, p0, Lcom/android/camera/manager/ViewManager;->mShowing:Z

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/android/camera/manager/ViewManager;->onRefresh()V

    :cond_0
    return-void
.end method

.method public final release()V
    .locals 3

    invoke-virtual {p0}, Lcom/android/camera/manager/ViewManager;->hide()V

    iget-object v0, p0, Lcom/android/camera/manager/ViewManager;->mView:Landroid/view/View;

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/android/camera/manager/ViewManager;->getContext()Lcom/android/camera/Camera;

    move-result-object v0

    iget-object v1, p0, Lcom/android/camera/manager/ViewManager;->mView:Landroid/view/View;

    iget v2, p0, Lcom/android/camera/manager/ViewManager;->mViewLayer:I

    invoke-virtual {v0, v1, v2}, Lcom/android/camera/Camera;->removeView(Landroid/view/View;I)V

    :cond_0
    invoke-virtual {p0}, Lcom/android/camera/manager/ViewManager;->onRelease()V

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/android/camera/manager/ViewManager;->mView:Landroid/view/View;

    iget-object v0, p0, Lcom/android/camera/manager/ViewManager;->mContext:Lcom/android/camera/Camera;

    invoke-virtual {v0, p0}, Lcom/android/camera/Camera;->removeViewManager(Lcom/android/camera/manager/ViewManager;)Z

    iget-object v0, p0, Lcom/android/camera/manager/ViewManager;->mContext:Lcom/android/camera/Camera;

    invoke-virtual {v0, p0}, Lcom/android/camera/Camera;->removeOnOrientationListener(Lcom/android/camera/Camera$OnOrientationListener;)Z

    return-void
.end method

.method public setAnimationEnabled(ZZ)V
    .locals 0
    .param p1    # Z
    .param p2    # Z

    iput-boolean p1, p0, Lcom/android/camera/manager/ViewManager;->mShowAnimationEnabled:Z

    iput-boolean p2, p0, Lcom/android/camera/manager/ViewManager;->mHideAnimationEnabled:Z

    return-void
.end method

.method public setEnabled(Z)V
    .locals 2
    .param p1    # Z

    iput-boolean p1, p0, Lcom/android/camera/manager/ViewManager;->mEnabled:Z

    iget-object v0, p0, Lcom/android/camera/manager/ViewManager;->mView:Landroid/view/View;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/camera/manager/ViewManager;->mView:Landroid/view/View;

    iget-boolean v1, p0, Lcom/android/camera/manager/ViewManager;->mEnabled:Z

    invoke-virtual {v0, v1}, Landroid/view/View;->setEnabled(Z)V

    iget-boolean v0, p0, Lcom/android/camera/manager/ViewManager;->mFilter:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/camera/manager/ViewManager;->mView:Landroid/view/View;

    iget-boolean v1, p0, Lcom/android/camera/manager/ViewManager;->mEnabled:Z

    invoke-static {v0, v1}, Lcom/android/camera/SettingUtils;->setEnabledState(Landroid/view/View;Z)V

    :cond_0
    return-void
.end method

.method public setFileter(Z)V
    .locals 0
    .param p1    # Z

    iput-boolean p1, p0, Lcom/android/camera/manager/ViewManager;->mFilter:Z

    return-void
.end method

.method public show()V
    .locals 4

    const/4 v3, 0x0

    sget-boolean v0, Lcom/android/camera/manager/ViewManager;->LOG:Z

    if-eqz v0, :cond_0

    const-string v0, "ViewManager"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "show() "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/android/camera/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    iget-object v0, p0, Lcom/android/camera/manager/ViewManager;->mView:Landroid/view/View;

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/android/camera/manager/ViewManager;->mContext:Lcom/android/camera/Camera;

    invoke-virtual {v0}, Landroid/view/ContextThemeWrapper;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v0

    iget v0, v0, Landroid/content/res/Configuration;->orientation:I

    iput v0, p0, Lcom/android/camera/manager/ViewManager;->mConfigOrientation:I

    invoke-virtual {p0}, Lcom/android/camera/manager/ViewManager;->getView()Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/android/camera/manager/ViewManager;->mView:Landroid/view/View;

    invoke-virtual {p0}, Lcom/android/camera/manager/ViewManager;->getContext()Lcom/android/camera/Camera;

    move-result-object v0

    iget-object v1, p0, Lcom/android/camera/manager/ViewManager;->mView:Landroid/view/View;

    iget v2, p0, Lcom/android/camera/manager/ViewManager;->mViewLayer:I

    invoke-virtual {v0, v1, v2}, Lcom/android/camera/Camera;->addView(Landroid/view/View;I)V

    iget-object v0, p0, Lcom/android/camera/manager/ViewManager;->mView:Landroid/view/View;

    iget v1, p0, Lcom/android/camera/manager/ViewManager;->mOrientation:I

    invoke-static {v0, v1, v3}, Lcom/android/camera/Util;->setOrientation(Landroid/view/View;IZ)V

    :cond_1
    iget-object v0, p0, Lcom/android/camera/manager/ViewManager;->mView:Landroid/view/View;

    if-eqz v0, :cond_3

    iget-boolean v0, p0, Lcom/android/camera/manager/ViewManager;->mShowing:Z

    if-nez v0, :cond_3

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/android/camera/manager/ViewManager;->mShowing:Z

    iget-boolean v0, p0, Lcom/android/camera/manager/ViewManager;->mEnabled:Z

    invoke-virtual {p0, v0}, Lcom/android/camera/manager/ViewManager;->setEnabled(Z)V

    invoke-virtual {p0}, Lcom/android/camera/manager/ViewManager;->refresh()V

    invoke-virtual {p0}, Lcom/android/camera/manager/ViewManager;->fadeIn()V

    iget-object v0, p0, Lcom/android/camera/manager/ViewManager;->mView:Landroid/view/View;

    invoke-virtual {v0, v3}, Landroid/view/View;->setVisibility(I)V

    :cond_2
    :goto_0
    return-void

    :cond_3
    iget-boolean v0, p0, Lcom/android/camera/manager/ViewManager;->mShowing:Z

    if-eqz v0, :cond_2

    invoke-virtual {p0}, Lcom/android/camera/manager/ViewManager;->refresh()V

    goto :goto_0
.end method
