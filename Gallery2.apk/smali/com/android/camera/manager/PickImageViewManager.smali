.class public Lcom/android/camera/manager/PickImageViewManager;
.super Lcom/android/camera/manager/ViewManager;
.source "PickImageViewManager.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/camera/manager/PickImageViewManager$SelectedChangedListener;
    }
.end annotation


# static fields
.field static final ANIMATION:Z = true

.field private static final HEIGHT_PADDING:I = 0x28

.field private static final LOG:Z

.field private static final TAG:Ljava/lang/String; = "PickImageViewManager"

.field private static final WIDTH_PADDING:I = 0xc8


# instance fields
.field private mAspectRatio:D

.field private mDisplayOrientaion:I

.field private mDisplayRotation:I

.field private mEv0:Lcom/android/camera/ui/EVPickerItem;

.field private mEvPickers:[Lcom/android/camera/ui/EVPickerItem;

.field private mEvm:Lcom/android/camera/ui/EVPickerItem;

.field private mEvp:Lcom/android/camera/ui/EVPickerItem;

.field private mFrameHeight:I

.field private mFrameWidth:I

.field private mNeedInitialize:Z

.field private mOnClickListener:Landroid/view/View$OnClickListener;

.field private mPictures2Pick:I

.field private mRootView:Landroid/view/View;

.field private mS3DMode:Z

.field private mSaveRequests:[Lcom/android/camera/SaveRequest;

.field private mSelectedChangedListener:Lcom/android/camera/manager/PickImageViewManager$SelectedChangedListener;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    sget-boolean v0, Lcom/android/camera/Log;->LOGV:Z

    sput-boolean v0, Lcom/android/camera/manager/PickImageViewManager;->LOG:Z

    return-void
.end method

.method public constructor <init>(Lcom/android/camera/Camera;I)V
    .locals 2
    .param p1    # Lcom/android/camera/Camera;
    .param p2    # I

    const/4 v1, 0x1

    const/4 v0, 0x0

    invoke-direct {p0, p1, v1}, Lcom/android/camera/manager/ViewManager;-><init>(Lcom/android/camera/Camera;I)V

    iput-boolean v0, p0, Lcom/android/camera/manager/PickImageViewManager;->mS3DMode:Z

    iput-boolean v1, p0, Lcom/android/camera/manager/PickImageViewManager;->mNeedInitialize:Z

    iput v0, p0, Lcom/android/camera/manager/PickImageViewManager;->mPictures2Pick:I

    new-instance v0, Lcom/android/camera/manager/PickImageViewManager$1;

    invoke-direct {v0, p0}, Lcom/android/camera/manager/PickImageViewManager$1;-><init>(Lcom/android/camera/manager/PickImageViewManager;)V

    iput-object v0, p0, Lcom/android/camera/manager/PickImageViewManager;->mOnClickListener:Landroid/view/View$OnClickListener;

    iput p2, p0, Lcom/android/camera/manager/PickImageViewManager;->mPictures2Pick:I

    return-void
.end method

.method static synthetic access$000()Z
    .locals 1

    sget-boolean v0, Lcom/android/camera/manager/PickImageViewManager;->LOG:Z

    return v0
.end method

.method static synthetic access$100(Lcom/android/camera/manager/PickImageViewManager;)I
    .locals 1
    .param p0    # Lcom/android/camera/manager/PickImageViewManager;

    iget v0, p0, Lcom/android/camera/manager/PickImageViewManager;->mPictures2Pick:I

    return v0
.end method

.method static synthetic access$200(Lcom/android/camera/manager/PickImageViewManager;)Landroid/view/View;
    .locals 1
    .param p0    # Lcom/android/camera/manager/PickImageViewManager;

    iget-object v0, p0, Lcom/android/camera/manager/PickImageViewManager;->mRootView:Landroid/view/View;

    return-object v0
.end method

.method static synthetic access$300(Lcom/android/camera/manager/PickImageViewManager;)Z
    .locals 1
    .param p0    # Lcom/android/camera/manager/PickImageViewManager;

    invoke-direct {p0}, Lcom/android/camera/manager/PickImageViewManager;->isAnyImgSelected()Z

    move-result v0

    return v0
.end method

.method static synthetic access$400(Lcom/android/camera/manager/PickImageViewManager;)Lcom/android/camera/manager/PickImageViewManager$SelectedChangedListener;
    .locals 1
    .param p0    # Lcom/android/camera/manager/PickImageViewManager;

    iget-object v0, p0, Lcom/android/camera/manager/PickImageViewManager;->mSelectedChangedListener:Lcom/android/camera/manager/PickImageViewManager$SelectedChangedListener;

    return-object v0
.end method

.method private configLayoutOrientation()V
    .locals 3

    sget-boolean v0, Lcom/android/camera/manager/PickImageViewManager;->LOG:Z

    if-eqz v0, :cond_0

    const-string v0, "PickImageViewManager"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "configLayoutOrientation RequestedOrientation="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lcom/android/camera/manager/ViewManager;->getContext()Lcom/android/camera/Camera;

    move-result-object v2

    invoke-virtual {v2}, Landroid/app/Activity;->getRequestedOrientation()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/android/camera/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    invoke-virtual {p0}, Lcom/android/camera/manager/ViewManager;->getContext()Lcom/android/camera/Camera;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/Activity;->getRequestedOrientation()I

    move-result v0

    const/4 v1, 0x1

    if-ne v0, v1, :cond_1

    const/16 v0, 0x10e

    invoke-super {p0, v0}, Lcom/android/camera/manager/ViewManager;->onOrientationChanged(I)V

    :cond_1
    return-void
.end method

.method private initializeViewManager()V
    .locals 6

    iget-object v3, p0, Lcom/android/camera/manager/PickImageViewManager;->mRootView:Landroid/view/View;

    const v4, 0x7f0b0128

    invoke-virtual {v3, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Lcom/android/camera/ui/EVPickerItem;

    iput-object v3, p0, Lcom/android/camera/manager/PickImageViewManager;->mEv0:Lcom/android/camera/ui/EVPickerItem;

    iget-object v3, p0, Lcom/android/camera/manager/PickImageViewManager;->mRootView:Landroid/view/View;

    const v4, 0x7f0b012a

    invoke-virtual {v3, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Lcom/android/camera/ui/EVPickerItem;

    iput-object v3, p0, Lcom/android/camera/manager/PickImageViewManager;->mEvp:Lcom/android/camera/ui/EVPickerItem;

    iget-object v3, p0, Lcom/android/camera/manager/PickImageViewManager;->mRootView:Landroid/view/View;

    const v4, 0x7f0b0129

    invoke-virtual {v3, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Lcom/android/camera/ui/EVPickerItem;

    iput-object v3, p0, Lcom/android/camera/manager/PickImageViewManager;->mEvm:Lcom/android/camera/ui/EVPickerItem;

    const/4 v3, 0x3

    new-array v3, v3, [Lcom/android/camera/ui/EVPickerItem;

    const/4 v4, 0x0

    iget-object v5, p0, Lcom/android/camera/manager/PickImageViewManager;->mEvm:Lcom/android/camera/ui/EVPickerItem;

    aput-object v5, v3, v4

    const/4 v4, 0x1

    iget-object v5, p0, Lcom/android/camera/manager/PickImageViewManager;->mEv0:Lcom/android/camera/ui/EVPickerItem;

    aput-object v5, v3, v4

    const/4 v4, 0x2

    iget-object v5, p0, Lcom/android/camera/manager/PickImageViewManager;->mEvp:Lcom/android/camera/ui/EVPickerItem;

    aput-object v5, v3, v4

    iput-object v3, p0, Lcom/android/camera/manager/PickImageViewManager;->mEvPickers:[Lcom/android/camera/ui/EVPickerItem;

    iget-object v3, p0, Lcom/android/camera/manager/PickImageViewManager;->mEv0:Lcom/android/camera/ui/EVPickerItem;

    iget-object v4, p0, Lcom/android/camera/manager/PickImageViewManager;->mOnClickListener:Landroid/view/View$OnClickListener;

    invoke-virtual {v3, v4}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v3, p0, Lcom/android/camera/manager/PickImageViewManager;->mEvp:Lcom/android/camera/ui/EVPickerItem;

    iget-object v4, p0, Lcom/android/camera/manager/PickImageViewManager;->mOnClickListener:Landroid/view/View$OnClickListener;

    invoke-virtual {v3, v4}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v3, p0, Lcom/android/camera/manager/PickImageViewManager;->mEvm:Lcom/android/camera/ui/EVPickerItem;

    iget-object v4, p0, Lcom/android/camera/manager/PickImageViewManager;->mOnClickListener:Landroid/view/View$OnClickListener;

    invoke-virtual {v3, v4}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    invoke-virtual {p0}, Lcom/android/camera/manager/ViewManager;->getContext()Lcom/android/camera/Camera;

    move-result-object v3

    invoke-virtual {v3}, Lcom/android/camera/Camera;->getDisplayOrientation()I

    move-result v3

    iput v3, p0, Lcom/android/camera/manager/PickImageViewManager;->mDisplayOrientaion:I

    invoke-virtual {p0}, Lcom/android/camera/manager/ViewManager;->getContext()Lcom/android/camera/Camera;

    move-result-object v3

    invoke-virtual {v3}, Lcom/android/camera/Camera;->getDisplayRotation()I

    move-result v3

    iput v3, p0, Lcom/android/camera/manager/PickImageViewManager;->mDisplayRotation:I

    invoke-virtual {p0}, Lcom/android/camera/manager/ViewManager;->getContext()Lcom/android/camera/Camera;

    move-result-object v3

    const-string v4, "window"

    invoke-virtual {v3, v4}, Landroid/app/Activity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/view/WindowManager;

    invoke-interface {v2}, Landroid/view/WindowManager;->getDefaultDisplay()Landroid/view/Display;

    move-result-object v3

    invoke-virtual {v3}, Landroid/view/Display;->getHeight()I

    move-result v0

    invoke-interface {v2}, Landroid/view/WindowManager;->getDefaultDisplay()Landroid/view/Display;

    move-result-object v3

    invoke-virtual {v3}, Landroid/view/Display;->getWidth()I

    move-result v1

    invoke-static {v0, v1}, Ljava/lang/Math;->max(II)I

    move-result v3

    iput v3, p0, Lcom/android/camera/manager/PickImageViewManager;->mFrameWidth:I

    invoke-static {v0, v1}, Ljava/lang/Math;->min(II)I

    move-result v3

    iput v3, p0, Lcom/android/camera/manager/PickImageViewManager;->mFrameHeight:I

    return-void
.end method

.method private isAnyImgSelected()Z
    .locals 1

    iget-object v0, p0, Lcom/android/camera/manager/PickImageViewManager;->mEv0:Lcom/android/camera/ui/EVPickerItem;

    invoke-virtual {v0}, Landroid/view/View;->isSelected()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/android/camera/manager/PickImageViewManager;->mEvp:Lcom/android/camera/ui/EVPickerItem;

    invoke-virtual {v0}, Landroid/view/View;->isSelected()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/android/camera/manager/PickImageViewManager;->mEvm:Lcom/android/camera/ui/EVPickerItem;

    invoke-virtual {v0}, Landroid/view/View;->isSelected()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public displayImages()V
    .locals 21

    sget-boolean v3, Lcom/android/camera/manager/PickImageViewManager;->LOG:Z

    if-eqz v3, :cond_0

    const-string v3, "PickImageViewManager"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "displayImages mSaveRequests="

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/android/camera/manager/PickImageViewManager;->mSaveRequests:[Lcom/android/camera/SaveRequest;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/android/camera/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/android/camera/manager/PickImageViewManager;->mSaveRequests:[Lcom/android/camera/SaveRequest;

    if-nez v3, :cond_2

    :cond_1
    :goto_0
    return-void

    :cond_2
    move-object/from16 v0, p0

    iget v3, v0, Lcom/android/camera/manager/PickImageViewManager;->mFrameHeight:I

    div-int/lit8 v3, v3, 0x2

    add-int/lit8 v17, v3, -0x28

    move/from16 v0, v17

    int-to-double v3, v0

    move-object/from16 v0, p0

    iget-wide v5, v0, Lcom/android/camera/manager/PickImageViewManager;->mAspectRatio:D

    mul-double/2addr v3, v5

    double-to-int v0, v3

    move/from16 v18, v0

    move-object/from16 v0, p0

    iget v3, v0, Lcom/android/camera/manager/PickImageViewManager;->mFrameWidth:I

    add-int/lit16 v14, v3, -0xc8

    move/from16 v0, v18

    if-le v0, v14, :cond_3

    move/from16 v18, v14

    move/from16 v0, v18

    int-to-double v3, v0

    move-object/from16 v0, p0

    iget-wide v5, v0, Lcom/android/camera/manager/PickImageViewManager;->mAspectRatio:D

    div-double/2addr v3, v5

    double-to-int v0, v3

    move/from16 v17, v0

    :cond_3
    const/4 v3, 0x3

    new-array v0, v3, [I

    move-object/from16 v20, v0

    fill-array-data v20, :array_0

    const/4 v7, 0x0

    const/16 v19, 0x0

    const/4 v15, 0x0

    const/4 v13, 0x0

    :goto_1
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/android/camera/manager/PickImageViewManager;->mSaveRequests:[Lcom/android/camera/SaveRequest;

    array-length v3, v3

    if-ge v13, v3, :cond_1

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/android/camera/manager/PickImageViewManager;->mSaveRequests:[Lcom/android/camera/SaveRequest;

    aget-object v3, v3, v13

    invoke-interface {v3}, Lcom/android/camera/SaveRequest;->getTempFilePath()Ljava/lang/String;

    move-result-object v19

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/android/camera/manager/PickImageViewManager;->mRootView:Landroid/view/View;

    aget v4, v20, v13

    invoke-virtual {v3, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v16

    check-cast v16, Lcom/android/camera/ui/EVPickerItem;

    invoke-virtual/range {v16 .. v16}, Landroid/view/View;->isSelected()Z

    move-result v3

    if-eqz v3, :cond_4

    invoke-virtual/range {v16 .. v16}, Lcom/android/camera/ui/EVPickerItem;->performClick()Z

    :cond_4
    mul-int v3, v18, v17

    move-object/from16 v0, v19

    move/from16 v1, v18

    invoke-static {v0, v1, v3}, Lcom/android/camera/Util;->makeBitmap(Ljava/lang/String;II)Landroid/graphics/Bitmap;

    move-result-object v2

    if-nez v2, :cond_6

    sget-boolean v3, Lcom/android/camera/manager/PickImageViewManager;->LOG:Z

    if-eqz v3, :cond_5

    const-string v3, "PickImageViewManager"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "File is gone or damaged:"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/android/camera/manager/PickImageViewManager;->mSaveRequests:[Lcom/android/camera/SaveRequest;

    aget-object v5, v5, v13

    invoke-interface {v5}, Lcom/android/camera/SaveRequest;->getTempFilePath()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/android/camera/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    :cond_5
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/android/camera/manager/PickImageViewManager;->mSelectedChangedListener:Lcom/android/camera/manager/PickImageViewManager$SelectedChangedListener;

    invoke-interface {v3}, Lcom/android/camera/manager/PickImageViewManager$SelectedChangedListener;->onDisplayFail()V

    goto/16 :goto_0

    :cond_6
    :try_start_0
    new-instance v12, Landroid/media/ExifInterface;

    move-object/from16 v0, v19

    invoke-direct {v12, v0}, Landroid/media/ExifInterface;-><init>(Ljava/lang/String;)V

    invoke-static {v12}, Lcom/android/camera/Util;->getExifOrientation(Landroid/media/ExifInterface;)I
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v15

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/android/camera/manager/PickImageViewManager;->mSaveRequests:[Lcom/android/camera/SaveRequest;

    aget-object v3, v3, v13

    invoke-interface {v3}, Lcom/android/camera/SaveRequest;->getJpegRotation()I

    move-result v3

    add-int/2addr v15, v3

    invoke-virtual {v2}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v3

    int-to-float v3, v3

    invoke-virtual {v2}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v4

    int-to-float v4, v4

    invoke-static {v3, v4}, Ljava/lang/Math;->max(FF)F

    move-result v10

    invoke-virtual {v2}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v3

    int-to-float v3, v3

    invoke-virtual {v2}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v4

    int-to-float v4, v4

    invoke-static {v3, v4}, Ljava/lang/Math;->min(FF)F

    move-result v9

    if-nez v7, :cond_7

    new-instance v7, Landroid/graphics/Matrix;

    invoke-direct {v7}, Landroid/graphics/Matrix;-><init>()V

    neg-int v3, v15

    int-to-float v3, v3

    invoke-virtual {v7, v3}, Landroid/graphics/Matrix;->postRotate(F)Z

    invoke-virtual/range {p0 .. p0}, Lcom/android/camera/manager/ViewManager;->getContext()Lcom/android/camera/Camera;

    move-result-object v3

    invoke-virtual {v3}, Landroid/app/Activity;->getRequestedOrientation()I

    move-result v3

    const/4 v4, 0x1

    if-ne v3, v4, :cond_a

    const/high16 v3, 0x42b40000

    invoke-virtual {v7, v3}, Landroid/graphics/Matrix;->postRotate(F)Z

    move/from16 v0, v17

    int-to-float v3, v0

    div-float/2addr v3, v9

    move/from16 v0, v18

    int-to-float v4, v0

    div-float/2addr v4, v10

    invoke-virtual {v7, v3, v4}, Landroid/graphics/Matrix;->postScale(FF)Z

    :cond_7
    :goto_2
    const/4 v3, 0x0

    const/4 v4, 0x0

    invoke-virtual {v2}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v5

    invoke-virtual {v2}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v6

    const/4 v8, 0x1

    invoke-static/range {v2 .. v8}, Landroid/graphics/Bitmap;->createBitmap(Landroid/graphics/Bitmap;IIIILandroid/graphics/Matrix;Z)Landroid/graphics/Bitmap;

    move-result-object v2

    move-object/from16 v0, v16

    invoke-virtual {v0, v2}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    aget v3, v20, v13

    const v4, 0x7f0b0128

    if-ne v3, v4, :cond_8

    invoke-virtual/range {v16 .. v16}, Lcom/android/camera/ui/EVPickerItem;->performClick()Z

    :cond_8
    sget-boolean v3, Lcom/android/camera/manager/PickImageViewManager;->LOG:Z

    if-eqz v3, :cond_9

    const-string v3, "PickImageViewManager"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v4, v13}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " orientation="

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v15}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " bmpWidth="

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v10}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " bmpHeight="

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v9}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " mSaveRequests[i].getJpegRotation()="

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/android/camera/manager/PickImageViewManager;->mSaveRequests:[Lcom/android/camera/SaveRequest;

    aget-object v5, v5, v13

    invoke-interface {v5}, Lcom/android/camera/SaveRequest;->getJpegRotation()I

    move-result v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " thumb: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    move/from16 v0, v18

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "x"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    move/from16 v0, v17

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " bmp="

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v2}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "x"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v2}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " mAspectRatio="

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    move-object/from16 v0, p0

    iget-wide v5, v0, Lcom/android/camera/manager/PickImageViewManager;->mAspectRatio:D

    invoke-virtual {v4, v5, v6}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/android/camera/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    :cond_9
    add-int/lit8 v13, v13, 0x1

    goto/16 :goto_1

    :catch_0
    move-exception v11

    const-string v3, "PickImageViewManager"

    const-string v4, "cannot read exif"

    invoke-static {v3, v4, v11}, Lcom/android/camera/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/android/camera/manager/PickImageViewManager;->mSelectedChangedListener:Lcom/android/camera/manager/PickImageViewManager$SelectedChangedListener;

    invoke-interface {v3}, Lcom/android/camera/manager/PickImageViewManager$SelectedChangedListener;->onDisplayFail()V

    goto/16 :goto_0

    :cond_a
    move/from16 v0, v18

    int-to-float v3, v0

    div-float/2addr v3, v10

    move/from16 v0, v17

    int-to-float v4, v0

    div-float/2addr v4, v9

    invoke-virtual {v7, v3, v4}, Landroid/graphics/Matrix;->postScale(FF)Z

    goto/16 :goto_2

    :array_0
    .array-data 4
        0x7f0b0129
        0x7f0b0128
        0x7f0b012a
    .end array-data
.end method

.method protected getView()Landroid/view/View;
    .locals 2

    const v1, 0x7f040052

    invoke-virtual {p0, v1}, Lcom/android/camera/manager/ViewManager;->inflate(I)Landroid/view/View;

    move-result-object v0

    const v1, 0x7f0b0126

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    iput-object v1, p0, Lcom/android/camera/manager/PickImageViewManager;->mRootView:Landroid/view/View;

    return-object v0
.end method

.method public isSelected(I)Z
    .locals 1
    .param p1    # I

    iget-object v0, p0, Lcom/android/camera/manager/PickImageViewManager;->mEvPickers:[Lcom/android/camera/ui/EVPickerItem;

    aget-object v0, v0, p1

    invoke-virtual {v0}, Landroid/view/View;->isSelected()Z

    move-result v0

    return v0
.end method

.method protected onRelease()V
    .locals 1

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/android/camera/manager/PickImageViewManager;->mNeedInitialize:Z

    return-void
.end method

.method public setAspectRatio(D)V
    .locals 0
    .param p1    # D

    iput-wide p1, p0, Lcom/android/camera/manager/PickImageViewManager;->mAspectRatio:D

    return-void
.end method

.method public setSaveRequests([Lcom/android/camera/SaveRequest;)V
    .locals 0
    .param p1    # [Lcom/android/camera/SaveRequest;

    iput-object p1, p0, Lcom/android/camera/manager/PickImageViewManager;->mSaveRequests:[Lcom/android/camera/SaveRequest;

    return-void
.end method

.method public setSelectedChangedListener(Lcom/android/camera/manager/PickImageViewManager$SelectedChangedListener;)V
    .locals 0
    .param p1    # Lcom/android/camera/manager/PickImageViewManager$SelectedChangedListener;

    iput-object p1, p0, Lcom/android/camera/manager/PickImageViewManager;->mSelectedChangedListener:Lcom/android/camera/manager/PickImageViewManager$SelectedChangedListener;

    return-void
.end method

.method public show()V
    .locals 1

    invoke-super {p0}, Lcom/android/camera/manager/ViewManager;->show()V

    iget-boolean v0, p0, Lcom/android/camera/manager/PickImageViewManager;->mNeedInitialize:Z

    if-eqz v0, :cond_0

    invoke-direct {p0}, Lcom/android/camera/manager/PickImageViewManager;->initializeViewManager()V

    invoke-direct {p0}, Lcom/android/camera/manager/PickImageViewManager;->configLayoutOrientation()V

    invoke-virtual {p0}, Lcom/android/camera/manager/PickImageViewManager;->displayImages()V

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/android/camera/manager/PickImageViewManager;->mNeedInitialize:Z

    :cond_0
    return-void
.end method
