.class public Lcom/android/camera/manager/SettingManager;
.super Lcom/android/camera/manager/ViewManager;
.source "SettingManager.java"

# interfaces
.implements Landroid/view/View$OnClickListener;
.implements Landroid/widget/TabHost$OnTabChangeListener;
.implements Lcom/android/camera/Camera$OnPreferenceReadyListener;
.implements Lcom/android/camera/ui/SettingListLayout$Listener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/camera/manager/SettingManager$MyPagerAdapter;,
        Lcom/android/camera/manager/SettingManager$Holder;,
        Lcom/android/camera/manager/SettingManager$SettingListener;
    }
.end annotation


# static fields
.field private static final DELAY_MSG_REMOVE_SETTING_MS:I = 0xbb8

.field private static final LOG:Z

.field private static final MSG_REMOVE_SETTING:I = 0x0

.field private static final SETTING_PAGE_LAYER:I = 0x3

.field private static final TAB_INDICATOR_KEY_CAMERA:Ljava/lang/String; = "camera"

.field private static final TAB_INDICATOR_KEY_COMMON:Ljava/lang/String; = "common"

.field private static final TAB_INDICATOR_KEY_PREVIEW:Ljava/lang/String; = "preview"

.field private static final TAB_INDICATOR_KEY_VIDEO:Ljava/lang/String; = "video"

.field private static final TAG:Ljava/lang/String; = "SettingManager"


# instance fields
.field private mAdapter:Lcom/android/camera/manager/SettingManager$MyPagerAdapter;

.field private mFadeIn:Landroid/view/animation/Animation;

.field private mFadeOut:Landroid/view/animation/Animation;

.field private mIndicator:Lcom/android/camera/ui/RotateImageView;

.field private mListener:Lcom/android/camera/manager/SettingManager$SettingListener;

.field private mMainHandler:Landroid/os/Handler;

.field private mPager:Lvedroid/support/v4/view/ViewPager;

.field private mSettingLayout:Landroid/view/ViewGroup;

.field private mShowingContainer:Z

.field private mTabHost:Landroid/widget/TabHost;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    sget-boolean v0, Lcom/android/camera/Log;->LOGV:Z

    sput-boolean v0, Lcom/android/camera/manager/SettingManager;->LOG:Z

    return-void
.end method

.method public constructor <init>(Lcom/android/camera/Camera;)V
    .locals 1
    .param p1    # Lcom/android/camera/Camera;

    invoke-direct {p0, p1}, Lcom/android/camera/manager/ViewManager;-><init>(Lcom/android/camera/Camera;)V

    new-instance v0, Lcom/android/camera/manager/SettingManager$1;

    invoke-direct {v0, p0}, Lcom/android/camera/manager/SettingManager$1;-><init>(Lcom/android/camera/manager/SettingManager;)V

    iput-object v0, p0, Lcom/android/camera/manager/SettingManager;->mMainHandler:Landroid/os/Handler;

    invoke-virtual {p1, p0}, Lcom/android/camera/Camera;->addOnPreferenceReadyListener(Lcom/android/camera/Camera$OnPreferenceReadyListener;)Z

    return-void
.end method

.method static synthetic access$000()Z
    .locals 1

    sget-boolean v0, Lcom/android/camera/manager/SettingManager;->LOG:Z

    return v0
.end method

.method static synthetic access$100(Lcom/android/camera/manager/SettingManager;)Landroid/view/ViewGroup;
    .locals 1
    .param p0    # Lcom/android/camera/manager/SettingManager;

    iget-object v0, p0, Lcom/android/camera/manager/SettingManager;->mSettingLayout:Landroid/view/ViewGroup;

    return-object v0
.end method

.method static synthetic access$200(Lcom/android/camera/manager/SettingManager;I)V
    .locals 0
    .param p0    # Lcom/android/camera/manager/SettingManager;
    .param p1    # I

    invoke-direct {p0, p1}, Lcom/android/camera/manager/SettingManager;->highlightCurrentSetting(I)V

    return-void
.end method

.method private highlightCurrentSetting(I)V
    .locals 1
    .param p1    # I

    iget-object v0, p0, Lcom/android/camera/manager/SettingManager;->mTabHost:Landroid/widget/TabHost;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/camera/manager/SettingManager;->mTabHost:Landroid/widget/TabHost;

    invoke-virtual {v0, p1}, Landroid/widget/TabHost;->setCurrentTab(I)V

    :cond_0
    return-void
.end method

.method private initializeSettings()V
    .locals 15

    const/4 v14, 0x3

    const/4 v8, 0x0

    const v13, 0x7f020103

    const v12, 0x7f020100

    const v11, 0x7f020101

    iget-object v7, p0, Lcom/android/camera/manager/SettingManager;->mSettingLayout:Landroid/view/ViewGroup;

    if-nez v7, :cond_7

    invoke-virtual {p0}, Lcom/android/camera/manager/ViewManager;->getContext()Lcom/android/camera/Camera;

    move-result-object v7

    invoke-virtual {v7}, Lcom/android/camera/Camera;->getPreferenceGroup()Lcom/android/camera/PreferenceGroup;

    move-result-object v7

    if-eqz v7, :cond_7

    invoke-virtual {p0}, Lcom/android/camera/manager/ViewManager;->getContext()Lcom/android/camera/Camera;

    move-result-object v7

    const v9, 0x7f040053

    invoke-virtual {v7, v9, v14}, Lcom/android/camera/Camera;->inflate(II)Landroid/view/View;

    move-result-object v7

    check-cast v7, Landroid/view/ViewGroup;

    iput-object v7, p0, Lcom/android/camera/manager/SettingManager;->mSettingLayout:Landroid/view/ViewGroup;

    iget-object v7, p0, Lcom/android/camera/manager/SettingManager;->mSettingLayout:Landroid/view/ViewGroup;

    const v9, 0x7f0b012c

    invoke-virtual {v7, v9}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v7

    check-cast v7, Landroid/widget/TabHost;

    iput-object v7, p0, Lcom/android/camera/manager/SettingManager;->mTabHost:Landroid/widget/TabHost;

    iget-object v7, p0, Lcom/android/camera/manager/SettingManager;->mTabHost:Landroid/widget/TabHost;

    invoke-virtual {v7}, Landroid/widget/TabHost;->setup()V

    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    invoke-virtual {p0}, Lcom/android/camera/manager/ViewManager;->getContext()Lcom/android/camera/Camera;

    move-result-object v7

    invoke-virtual {v7}, Lcom/android/camera/Camera;->isNonePickIntent()Z

    move-result v7

    if-eqz v7, :cond_1

    invoke-static {}, Lcom/mediatek/camera/ext/ExtensionHelper;->getFeatureExtension()Lcom/mediatek/camera/ext/IFeatureExtension;

    move-result-object v7

    invoke-interface {v7}, Lcom/mediatek/camera/ext/IFeatureExtension;->isPrioritizePreviewSize()Z

    move-result v7

    if-eqz v7, :cond_0

    new-instance v7, Lcom/android/camera/manager/SettingManager$Holder;

    const-string v9, "preview"

    sget-object v10, Lcom/android/camera/SettingChecker;->SETTING_GROUP_COMMON_FOR_TAB_PREVIEW:[I

    invoke-direct {v7, p0, v9, v11, v10}, Lcom/android/camera/manager/SettingManager$Holder;-><init>(Lcom/android/camera/manager/SettingManager;Ljava/lang/String;I[I)V

    invoke-interface {v3, v7}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    new-instance v7, Lcom/android/camera/manager/SettingManager$Holder;

    const-string v9, "common"

    sget-object v10, Lcom/android/camera/SettingChecker;->SETTING_GROUP_COMMON_FOR_TAB:[I

    invoke-direct {v7, p0, v9, v11, v10}, Lcom/android/camera/manager/SettingManager$Holder;-><init>(Lcom/android/camera/manager/SettingManager;Ljava/lang/String;I[I)V

    invoke-interface {v3, v7}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    new-instance v7, Lcom/android/camera/manager/SettingManager$Holder;

    const-string v9, "camera"

    sget-object v10, Lcom/android/camera/SettingChecker;->SETTING_GROUP_CAMERA_FOR_TAB_NO_PREVIEW:[I

    invoke-direct {v7, p0, v9, v12, v10}, Lcom/android/camera/manager/SettingManager$Holder;-><init>(Lcom/android/camera/manager/SettingManager;Ljava/lang/String;I[I)V

    invoke-interface {v3, v7}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    new-instance v7, Lcom/android/camera/manager/SettingManager$Holder;

    const-string v9, "video"

    sget-object v10, Lcom/android/camera/SettingChecker;->SETTING_GROUP_VIDEO_FOR_TAB_NO_PREVIEW:[I

    invoke-direct {v7, p0, v9, v13, v10}, Lcom/android/camera/manager/SettingManager$Holder;-><init>(Lcom/android/camera/manager/SettingManager;Ljava/lang/String;I[I)V

    invoke-interface {v3, v7}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :goto_0
    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v6

    new-instance v5, Ljava/util/ArrayList;

    invoke-direct {v5}, Ljava/util/ArrayList;-><init>()V

    const/4 v1, 0x0

    :goto_1
    if-ge v1, v6, :cond_6

    invoke-interface {v3, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/camera/manager/SettingManager$Holder;

    invoke-virtual {p0}, Lcom/android/camera/manager/ViewManager;->getContext()Lcom/android/camera/Camera;

    move-result-object v7

    const v9, 0x7f040056

    invoke-virtual {v7, v9, v14}, Lcom/android/camera/Camera;->inflate(II)Landroid/view/View;

    move-result-object v4

    check-cast v4, Lcom/android/camera/ui/SettingListLayout;

    iget-object v7, v0, Lcom/android/camera/manager/SettingManager$Holder;->mSettingKeys:[I

    invoke-static {v7}, Lcom/android/camera/SettingChecker;->getSettingKeys([I)[Ljava/lang/String;

    move-result-object v9

    if-nez v1, :cond_5

    const/4 v7, 0x1

    :goto_2
    invoke-virtual {v4, v9, v7}, Lcom/android/camera/ui/SettingListLayout;->initialize([Ljava/lang/String;Z)V

    invoke-interface {v5, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    new-instance v2, Landroid/widget/ImageView;

    invoke-virtual {p0}, Lcom/android/camera/manager/ViewManager;->getContext()Lcom/android/camera/Camera;

    move-result-object v7

    invoke-direct {v2, v7}, Landroid/widget/ImageView;-><init>(Landroid/content/Context;)V

    const v7, 0x7f020011

    invoke-virtual {v2, v7}, Landroid/view/View;->setBackgroundResource(I)V

    iget v7, v0, Lcom/android/camera/manager/SettingManager$Holder;->mIndicatorIconRes:I

    invoke-virtual {v2, v7}, Landroid/widget/ImageView;->setImageResource(I)V

    sget-object v7, Landroid/widget/ImageView$ScaleType;->CENTER:Landroid/widget/ImageView$ScaleType;

    invoke-virtual {v2, v7}, Landroid/widget/ImageView;->setScaleType(Landroid/widget/ImageView$ScaleType;)V

    iget-object v7, p0, Lcom/android/camera/manager/SettingManager;->mTabHost:Landroid/widget/TabHost;

    iget-object v9, p0, Lcom/android/camera/manager/SettingManager;->mTabHost:Landroid/widget/TabHost;

    iget-object v10, v0, Lcom/android/camera/manager/SettingManager$Holder;->mIndicatorKey:Ljava/lang/String;

    invoke-virtual {v9, v10}, Landroid/widget/TabHost;->newTabSpec(Ljava/lang/String;)Landroid/widget/TabHost$TabSpec;

    move-result-object v9

    invoke-virtual {v9, v2}, Landroid/widget/TabHost$TabSpec;->setIndicator(Landroid/view/View;)Landroid/widget/TabHost$TabSpec;

    move-result-object v9

    const v10, 0x1020011

    invoke-virtual {v9, v10}, Landroid/widget/TabHost$TabSpec;->setContent(I)Landroid/widget/TabHost$TabSpec;

    move-result-object v9

    invoke-virtual {v7, v9}, Landroid/widget/TabHost;->addTab(Landroid/widget/TabHost$TabSpec;)V

    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    :cond_0
    new-instance v7, Lcom/android/camera/manager/SettingManager$Holder;

    const-string v9, "common"

    sget-object v10, Lcom/android/camera/SettingChecker;->SETTING_GROUP_COMMON_FOR_TAB:[I

    invoke-direct {v7, p0, v9, v11, v10}, Lcom/android/camera/manager/SettingManager$Holder;-><init>(Lcom/android/camera/manager/SettingManager;Ljava/lang/String;I[I)V

    invoke-interface {v3, v7}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    new-instance v7, Lcom/android/camera/manager/SettingManager$Holder;

    const-string v9, "camera"

    sget-object v10, Lcom/android/camera/SettingChecker;->SETTING_GROUP_CAMERA_FOR_TAB:[I

    invoke-direct {v7, p0, v9, v12, v10}, Lcom/android/camera/manager/SettingManager$Holder;-><init>(Lcom/android/camera/manager/SettingManager;Ljava/lang/String;I[I)V

    invoke-interface {v3, v7}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    new-instance v7, Lcom/android/camera/manager/SettingManager$Holder;

    const-string v9, "video"

    sget-object v10, Lcom/android/camera/SettingChecker;->SETTING_GROUP_VIDEO_FOR_TAB:[I

    invoke-direct {v7, p0, v9, v13, v10}, Lcom/android/camera/manager/SettingManager$Holder;-><init>(Lcom/android/camera/manager/SettingManager;Ljava/lang/String;I[I)V

    invoke-interface {v3, v7}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto/16 :goto_0

    :cond_1
    invoke-static {}, Lcom/mediatek/camera/ext/ExtensionHelper;->getFeatureExtension()Lcom/mediatek/camera/ext/IFeatureExtension;

    move-result-object v7

    invoke-interface {v7}, Lcom/mediatek/camera/ext/IFeatureExtension;->isPrioritizePreviewSize()Z

    move-result v7

    if-eqz v7, :cond_3

    invoke-virtual {p0}, Lcom/android/camera/manager/ViewManager;->getContext()Lcom/android/camera/Camera;

    move-result-object v7

    invoke-virtual {v7}, Lcom/android/camera/Camera;->isImageCaptureIntent()Z

    move-result v7

    if-eqz v7, :cond_2

    new-instance v7, Lcom/android/camera/manager/SettingManager$Holder;

    const-string v9, "preview"

    sget-object v10, Lcom/android/camera/SettingChecker;->SETTING_GROUP_COMMON_FOR_TAB_PREVIEW:[I

    invoke-direct {v7, p0, v9, v11, v10}, Lcom/android/camera/manager/SettingManager$Holder;-><init>(Lcom/android/camera/manager/SettingManager;Ljava/lang/String;I[I)V

    invoke-interface {v3, v7}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    new-instance v7, Lcom/android/camera/manager/SettingManager$Holder;

    const-string v9, "common"

    sget-object v10, Lcom/android/camera/SettingChecker;->SETTING_GROUP_COMMON_FOR_TAB:[I

    invoke-direct {v7, p0, v9, v11, v10}, Lcom/android/camera/manager/SettingManager$Holder;-><init>(Lcom/android/camera/manager/SettingManager;Ljava/lang/String;I[I)V

    invoke-interface {v3, v7}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    new-instance v7, Lcom/android/camera/manager/SettingManager$Holder;

    const-string v9, "camera"

    sget-object v10, Lcom/android/camera/SettingChecker;->SETTING_GROUP_CAMERA_FOR_TAB_NO_PREVIEW:[I

    invoke-direct {v7, p0, v9, v12, v10}, Lcom/android/camera/manager/SettingManager$Holder;-><init>(Lcom/android/camera/manager/SettingManager;Ljava/lang/String;I[I)V

    invoke-interface {v3, v7}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto/16 :goto_0

    :cond_2
    new-instance v7, Lcom/android/camera/manager/SettingManager$Holder;

    const-string v9, "common"

    sget-object v10, Lcom/android/camera/SettingChecker;->SETTING_GROUP_COMMON_FOR_TAB:[I

    invoke-direct {v7, p0, v9, v11, v10}, Lcom/android/camera/manager/SettingManager$Holder;-><init>(Lcom/android/camera/manager/SettingManager;Ljava/lang/String;I[I)V

    invoke-interface {v3, v7}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    new-instance v7, Lcom/android/camera/manager/SettingManager$Holder;

    const-string v9, "video"

    sget-object v10, Lcom/android/camera/SettingChecker;->SETTING_GROUP_VIDEO_FOR_TAB_NO_PREVIEW:[I

    invoke-direct {v7, p0, v9, v13, v10}, Lcom/android/camera/manager/SettingManager$Holder;-><init>(Lcom/android/camera/manager/SettingManager;Ljava/lang/String;I[I)V

    invoke-interface {v3, v7}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto/16 :goto_0

    :cond_3
    new-instance v7, Lcom/android/camera/manager/SettingManager$Holder;

    const-string v9, "common"

    sget-object v10, Lcom/android/camera/SettingChecker;->SETTING_GROUP_COMMON_FOR_TAB:[I

    invoke-direct {v7, p0, v9, v11, v10}, Lcom/android/camera/manager/SettingManager$Holder;-><init>(Lcom/android/camera/manager/SettingManager;Ljava/lang/String;I[I)V

    invoke-interface {v3, v7}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    invoke-virtual {p0}, Lcom/android/camera/manager/ViewManager;->getContext()Lcom/android/camera/Camera;

    move-result-object v7

    invoke-virtual {v7}, Lcom/android/camera/Camera;->isImageCaptureIntent()Z

    move-result v7

    if-eqz v7, :cond_4

    new-instance v7, Lcom/android/camera/manager/SettingManager$Holder;

    const-string v9, "camera"

    sget-object v10, Lcom/android/camera/SettingChecker;->SETTING_GROUP_CAMERA_FOR_TAB:[I

    invoke-direct {v7, p0, v9, v12, v10}, Lcom/android/camera/manager/SettingManager$Holder;-><init>(Lcom/android/camera/manager/SettingManager;Ljava/lang/String;I[I)V

    invoke-interface {v3, v7}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto/16 :goto_0

    :cond_4
    new-instance v7, Lcom/android/camera/manager/SettingManager$Holder;

    const-string v9, "video"

    sget-object v10, Lcom/android/camera/SettingChecker;->SETTING_GROUP_VIDEO_FOR_TAB:[I

    invoke-direct {v7, p0, v9, v13, v10}, Lcom/android/camera/manager/SettingManager$Holder;-><init>(Lcom/android/camera/manager/SettingManager;Ljava/lang/String;I[I)V

    invoke-interface {v3, v7}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto/16 :goto_0

    :cond_5
    move v7, v8

    goto/16 :goto_2

    :cond_6
    new-instance v7, Lcom/android/camera/manager/SettingManager$MyPagerAdapter;

    invoke-direct {v7, p0, v5}, Lcom/android/camera/manager/SettingManager$MyPagerAdapter;-><init>(Lcom/android/camera/manager/SettingManager;Ljava/util/List;)V

    iput-object v7, p0, Lcom/android/camera/manager/SettingManager;->mAdapter:Lcom/android/camera/manager/SettingManager$MyPagerAdapter;

    iget-object v7, p0, Lcom/android/camera/manager/SettingManager;->mSettingLayout:Landroid/view/ViewGroup;

    const v9, 0x7f0b012d

    invoke-virtual {v7, v9}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v7

    check-cast v7, Lvedroid/support/v4/view/ViewPager;

    iput-object v7, p0, Lcom/android/camera/manager/SettingManager;->mPager:Lvedroid/support/v4/view/ViewPager;

    iget-object v7, p0, Lcom/android/camera/manager/SettingManager;->mPager:Lvedroid/support/v4/view/ViewPager;

    iget-object v9, p0, Lcom/android/camera/manager/SettingManager;->mAdapter:Lcom/android/camera/manager/SettingManager$MyPagerAdapter;

    invoke-virtual {v7, v9}, Lvedroid/support/v4/view/ViewPager;->setAdapter(Lvedroid/support/v4/view/PagerAdapter;)V

    iget-object v7, p0, Lcom/android/camera/manager/SettingManager;->mPager:Lvedroid/support/v4/view/ViewPager;

    iget-object v9, p0, Lcom/android/camera/manager/SettingManager;->mAdapter:Lcom/android/camera/manager/SettingManager$MyPagerAdapter;

    invoke-virtual {v7, v9}, Lvedroid/support/v4/view/ViewPager;->setOnPageChangeListener(Lvedroid/support/v4/view/ViewPager$OnPageChangeListener;)V

    iget-object v7, p0, Lcom/android/camera/manager/SettingManager;->mTabHost:Landroid/widget/TabHost;

    invoke-virtual {v7, p0}, Landroid/widget/TabHost;->setOnTabChangedListener(Landroid/widget/TabHost$OnTabChangeListener;)V

    :cond_7
    iget-object v7, p0, Lcom/android/camera/manager/SettingManager;->mSettingLayout:Landroid/view/ViewGroup;

    invoke-virtual {p0}, Lcom/android/camera/manager/ViewManager;->getOrientation()I

    move-result v9

    invoke-static {v7, v9, v8}, Lcom/android/camera/Util;->setOrientation(Landroid/view/View;IZ)V

    return-void
.end method

.method private releaseSettingResource()V
    .locals 3

    const/4 v2, 0x0

    sget-boolean v0, Lcom/android/camera/manager/SettingManager;->LOG:Z

    if-eqz v0, :cond_0

    const-string v0, "SettingManager"

    const-string v1, "releaseSettingResource()"

    invoke-static {v0, v1}, Lcom/android/camera/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/android/camera/manager/SettingManager;->collapse(Z)Z

    iget-object v0, p0, Lcom/android/camera/manager/SettingManager;->mSettingLayout:Landroid/view/ViewGroup;

    if-eqz v0, :cond_1

    iput-object v2, p0, Lcom/android/camera/manager/SettingManager;->mAdapter:Lcom/android/camera/manager/SettingManager$MyPagerAdapter;

    iput-object v2, p0, Lcom/android/camera/manager/SettingManager;->mPager:Lvedroid/support/v4/view/ViewPager;

    iput-object v2, p0, Lcom/android/camera/manager/SettingManager;->mSettingLayout:Landroid/view/ViewGroup;

    :cond_1
    return-void
.end method

.method private setChildrenClickable(Z)V
    .locals 7
    .param p1    # Z

    sget-boolean v4, Lcom/android/camera/manager/SettingManager;->LOG:Z

    if-eqz v4, :cond_0

    const-string v4, "SettingManager"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "setChildrenClickable("

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, ") "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/android/camera/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    invoke-virtual {p0}, Lcom/android/camera/manager/ViewManager;->getContext()Lcom/android/camera/Camera;

    move-result-object v4

    invoke-virtual {v4}, Lcom/android/camera/Camera;->getPreferenceGroup()Lcom/android/camera/PreferenceGroup;

    move-result-object v0

    if-eqz v0, :cond_2

    invoke-virtual {v0}, Lcom/android/camera/PreferenceGroup;->size()I

    move-result v2

    const/4 v1, 0x0

    :goto_0
    if-ge v1, v2, :cond_2

    invoke-virtual {v0, v1}, Lcom/android/camera/PreferenceGroup;->get(I)Lcom/android/camera/CameraPreference;

    move-result-object v3

    instance-of v4, v3, Lcom/android/camera/ListPreference;

    if-eqz v4, :cond_1

    check-cast v3, Lcom/android/camera/ListPreference;

    invoke-virtual {v3, p1}, Lcom/android/camera/ListPreference;->setClickable(Z)V

    :cond_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_2
    return-void
.end method

.method private showSetting()V
    .locals 7

    const/4 v6, 0x3

    const/4 v5, 0x1

    const/4 v4, 0x0

    sget-boolean v1, Lcom/android/camera/manager/SettingManager;->LOG:Z

    if-eqz v1, :cond_0

    const-string v1, "SettingManager"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "showSetting() mShowingContainer="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-boolean v3, p0, Lcom/android/camera/manager/SettingManager;->mShowingContainer:Z

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ", getContext().isFullScreen()="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {p0}, Lcom/android/camera/manager/ViewManager;->getContext()Lcom/android/camera/Camera;

    move-result-object v3

    invoke-virtual {v3}, Lcom/android/camera/ActivityBase;->isFullScreen()Z

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/android/camera/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    invoke-virtual {p0}, Lcom/android/camera/manager/ViewManager;->getContext()Lcom/android/camera/Camera;

    move-result-object v1

    invoke-virtual {v1}, Lcom/android/camera/ActivityBase;->isFullScreen()Z

    move-result v1

    if-eqz v1, :cond_3

    invoke-virtual {p0}, Lcom/android/camera/manager/ViewManager;->getContext()Lcom/android/camera/Camera;

    move-result-object v1

    invoke-virtual {v1}, Lcom/android/camera/Camera;->getPreferenceGroup()Lcom/android/camera/PreferenceGroup;

    move-result-object v0

    iget-boolean v1, p0, Lcom/android/camera/manager/SettingManager;->mShowingContainer:Z

    if-nez v1, :cond_2

    if-eqz v0, :cond_2

    invoke-virtual {p0}, Lcom/android/camera/manager/ViewManager;->getContext()Lcom/android/camera/Camera;

    move-result-object v1

    invoke-virtual {v1}, Lcom/android/camera/Camera;->isNormalViewState()Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/android/camera/manager/SettingManager;->mMainHandler:Landroid/os/Handler;

    invoke-virtual {v1, v4}, Landroid/os/Handler;->removeMessages(I)V

    iput-boolean v5, p0, Lcom/android/camera/manager/SettingManager;->mShowingContainer:Z

    invoke-direct {p0}, Lcom/android/camera/manager/SettingManager;->initializeSettings()V

    invoke-virtual {p0}, Lcom/android/camera/manager/ViewManager;->refresh()V

    iget-object v1, p0, Lcom/android/camera/manager/SettingManager;->mPager:Lvedroid/support/v4/view/ViewPager;

    invoke-virtual {v1}, Lvedroid/support/v4/view/ViewPager;->getCurrentItem()I

    move-result v1

    invoke-direct {p0, v1}, Lcom/android/camera/manager/SettingManager;->highlightCurrentSetting(I)V

    iget-object v1, p0, Lcom/android/camera/manager/SettingManager;->mSettingLayout:Landroid/view/ViewGroup;

    invoke-virtual {v1, v4}, Landroid/view/View;->setVisibility(I)V

    iget-object v1, p0, Lcom/android/camera/manager/SettingManager;->mSettingLayout:Landroid/view/ViewGroup;

    invoke-virtual {v1}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    move-result-object v1

    if-nez v1, :cond_1

    invoke-virtual {p0}, Lcom/android/camera/manager/ViewManager;->getContext()Lcom/android/camera/Camera;

    move-result-object v1

    iget-object v2, p0, Lcom/android/camera/manager/SettingManager;->mSettingLayout:Landroid/view/ViewGroup;

    invoke-virtual {v1, v2, v6}, Lcom/android/camera/Camera;->addView(Landroid/view/View;I)V

    :cond_1
    invoke-virtual {p0}, Lcom/android/camera/manager/ViewManager;->getContext()Lcom/android/camera/Camera;

    move-result-object v1

    invoke-virtual {v1, v6}, Lcom/android/camera/Camera;->setViewState(I)V

    invoke-virtual {p0}, Lcom/android/camera/manager/ViewManager;->getContext()Lcom/android/camera/Camera;

    move-result-object v1

    invoke-virtual {v1, v4}, Lcom/android/camera/Camera;->setSwipingEnabled(Z)V

    iget-object v1, p0, Lcom/android/camera/manager/SettingManager;->mSettingLayout:Landroid/view/ViewGroup;

    invoke-direct {p0, v1}, Lcom/android/camera/manager/SettingManager;->startFadeInAnimation(Landroid/view/View;)V

    iget-object v1, p0, Lcom/android/camera/manager/SettingManager;->mIndicator:Lcom/android/camera/ui/RotateImageView;

    const v2, 0x7f0200f8

    invoke-virtual {v1, v2}, Landroid/widget/ImageView;->setImageResource(I)V

    :cond_2
    invoke-direct {p0, v5}, Lcom/android/camera/manager/SettingManager;->setChildrenClickable(Z)V

    :cond_3
    return-void
.end method

.method private startFadeInAnimation(Landroid/view/View;)V
    .locals 2
    .param p1    # Landroid/view/View;

    iget-object v0, p0, Lcom/android/camera/manager/SettingManager;->mFadeIn:Landroid/view/animation/Animation;

    if-nez v0, :cond_0

    invoke-virtual {p0}, Lcom/android/camera/manager/ViewManager;->getContext()Lcom/android/camera/Camera;

    move-result-object v0

    const v1, 0x7f050005

    invoke-static {v0, v1}, Landroid/view/animation/AnimationUtils;->loadAnimation(Landroid/content/Context;I)Landroid/view/animation/Animation;

    move-result-object v0

    iput-object v0, p0, Lcom/android/camera/manager/SettingManager;->mFadeIn:Landroid/view/animation/Animation;

    :cond_0
    if-eqz p1, :cond_1

    iget-object v0, p0, Lcom/android/camera/manager/SettingManager;->mFadeIn:Landroid/view/animation/Animation;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/android/camera/manager/SettingManager;->mFadeIn:Landroid/view/animation/Animation;

    invoke-virtual {p1, v0}, Landroid/view/View;->startAnimation(Landroid/view/animation/Animation;)V

    :cond_1
    return-void
.end method

.method private startFadeOutAnimation(Landroid/view/View;)V
    .locals 2
    .param p1    # Landroid/view/View;

    iget-object v0, p0, Lcom/android/camera/manager/SettingManager;->mFadeOut:Landroid/view/animation/Animation;

    if-nez v0, :cond_0

    invoke-virtual {p0}, Lcom/android/camera/manager/ViewManager;->getContext()Lcom/android/camera/Camera;

    move-result-object v0

    const v1, 0x7f050006

    invoke-static {v0, v1}, Landroid/view/animation/AnimationUtils;->loadAnimation(Landroid/content/Context;I)Landroid/view/animation/Animation;

    move-result-object v0

    iput-object v0, p0, Lcom/android/camera/manager/SettingManager;->mFadeOut:Landroid/view/animation/Animation;

    :cond_0
    if-eqz p1, :cond_1

    iget-object v0, p0, Lcom/android/camera/manager/SettingManager;->mFadeOut:Landroid/view/animation/Animation;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/android/camera/manager/SettingManager;->mFadeOut:Landroid/view/animation/Animation;

    invoke-virtual {p1, v0}, Landroid/view/View;->startAnimation(Landroid/view/animation/Animation;)V

    :cond_1
    return-void
.end method


# virtual methods
.method public collapse(Z)Z
    .locals 4
    .param p1    # Z

    const/4 v0, 0x0

    iget-boolean v1, p0, Lcom/android/camera/manager/SettingManager;->mShowingContainer:Z

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/android/camera/manager/SettingManager;->mAdapter:Lcom/android/camera/manager/SettingManager$MyPagerAdapter;

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/android/camera/manager/SettingManager;->mAdapter:Lcom/android/camera/manager/SettingManager$MyPagerAdapter;

    invoke-virtual {v1, p1}, Lcom/android/camera/manager/SettingManager$MyPagerAdapter;->collapse(Z)Z

    move-result v1

    if-nez v1, :cond_0

    invoke-virtual {p0}, Lcom/android/camera/manager/SettingManager;->hideSetting()V

    :cond_0
    const/4 v0, 0x1

    :cond_1
    sget-boolean v1, Lcom/android/camera/manager/SettingManager;->LOG:Z

    if-eqz v1, :cond_2

    const-string v1, "SettingManager"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "collapse("

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ") mShowingContainer="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-boolean v3, p0, Lcom/android/camera/manager/SettingManager;->mShowingContainer:Z

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ", return "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/android/camera/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    :cond_2
    return v0
.end method

.method protected getView()Landroid/view/View;
    .locals 2

    const v1, 0x7f040054

    invoke-virtual {p0, v1}, Lcom/android/camera/manager/ViewManager;->inflate(I)Landroid/view/View;

    move-result-object v0

    const v1, 0x7f0b012e

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/android/camera/ui/RotateImageView;

    iput-object v1, p0, Lcom/android/camera/manager/SettingManager;->mIndicator:Lcom/android/camera/ui/RotateImageView;

    iget-object v1, p0, Lcom/android/camera/manager/SettingManager;->mIndicator:Lcom/android/camera/ui/RotateImageView;

    invoke-virtual {v1, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    return-object v0
.end method

.method public handleMenuEvent()Z
    .locals 4

    const/4 v0, 0x0

    invoke-virtual {p0}, Lcom/android/camera/manager/ViewManager;->isEnabled()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-virtual {p0}, Lcom/android/camera/manager/ViewManager;->isShowing()Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/android/camera/manager/SettingManager;->mIndicator:Lcom/android/camera/ui/RotateImageView;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/android/camera/manager/SettingManager;->mIndicator:Lcom/android/camera/ui/RotateImageView;

    invoke-virtual {v1}, Landroid/view/View;->performClick()Z

    const/4 v0, 0x1

    :cond_0
    sget-boolean v1, Lcom/android/camera/manager/SettingManager;->LOG:Z

    if-eqz v1, :cond_1

    const-string v1, "SettingManager"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "handleMenuEvent() isEnabled()="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {p0}, Lcom/android/camera/manager/ViewManager;->isEnabled()Z

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ", isShowing()="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {p0}, Lcom/android/camera/manager/ViewManager;->isShowing()Z

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ", mIndicator="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/android/camera/manager/SettingManager;->mIndicator:Lcom/android/camera/ui/RotateImageView;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ", return "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/android/camera/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    :cond_1
    return v0
.end method

.method public hide()V
    .locals 1

    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/android/camera/manager/SettingManager;->collapse(Z)Z

    invoke-super {p0}, Lcom/android/camera/manager/ViewManager;->hide()V

    return-void
.end method

.method public hideSetting()V
    .locals 4

    const/4 v3, 0x0

    sget-boolean v0, Lcom/android/camera/manager/SettingManager;->LOG:Z

    if-eqz v0, :cond_0

    const-string v0, "SettingManager"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "hideSetting() mShowingContainer="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-boolean v2, p0, Lcom/android/camera/manager/SettingManager;->mShowingContainer:Z

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", mSettingLayout="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/android/camera/manager/SettingManager;->mSettingLayout:Landroid/view/ViewGroup;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/android/camera/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    iget-boolean v0, p0, Lcom/android/camera/manager/SettingManager;->mShowingContainer:Z

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/android/camera/manager/SettingManager;->mSettingLayout:Landroid/view/ViewGroup;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/android/camera/manager/SettingManager;->mMainHandler:Landroid/os/Handler;

    invoke-virtual {v0, v3}, Landroid/os/Handler;->removeMessages(I)V

    iget-object v0, p0, Lcom/android/camera/manager/SettingManager;->mSettingLayout:Landroid/view/ViewGroup;

    invoke-direct {p0, v0}, Lcom/android/camera/manager/SettingManager;->startFadeOutAnimation(Landroid/view/View;)V

    iget-object v0, p0, Lcom/android/camera/manager/SettingManager;->mSettingLayout:Landroid/view/ViewGroup;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    iput-boolean v3, p0, Lcom/android/camera/manager/SettingManager;->mShowingContainer:Z

    invoke-virtual {p0}, Lcom/android/camera/manager/ViewManager;->getContext()Lcom/android/camera/Camera;

    move-result-object v0

    invoke-virtual {v0}, Lcom/android/camera/Camera;->getViewState()I

    move-result v0

    const/4 v1, 0x3

    if-ne v0, v1, :cond_1

    invoke-virtual {p0}, Lcom/android/camera/manager/ViewManager;->getContext()Lcom/android/camera/Camera;

    move-result-object v0

    invoke-virtual {v0}, Lcom/android/camera/Camera;->restoreViewState()V

    invoke-virtual {p0}, Lcom/android/camera/manager/ViewManager;->getContext()Lcom/android/camera/Camera;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/android/camera/Camera;->setSwipingEnabled(Z)V

    :cond_1
    iget-object v0, p0, Lcom/android/camera/manager/SettingManager;->mIndicator:Lcom/android/camera/ui/RotateImageView;

    const v1, 0x7f0200f9

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    iget-object v0, p0, Lcom/android/camera/manager/SettingManager;->mMainHandler:Landroid/os/Handler;

    const-wide/16 v1, 0xbb8

    invoke-virtual {v0, v3, v1, v2}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    :cond_2
    invoke-direct {p0, v3}, Lcom/android/camera/manager/SettingManager;->setChildrenClickable(Z)V

    return-void
.end method

.method public onClick(Landroid/view/View;)V
    .locals 1
    .param p1    # Landroid/view/View;

    iget-object v0, p0, Lcom/android/camera/manager/SettingManager;->mIndicator:Lcom/android/camera/ui/RotateImageView;

    if-ne p1, v0, :cond_0

    iget-boolean v0, p0, Lcom/android/camera/manager/SettingManager;->mShowingContainer:Z

    if-nez v0, :cond_1

    invoke-direct {p0}, Lcom/android/camera/manager/SettingManager;->showSetting()V

    :cond_0
    :goto_0
    return-void

    :cond_1
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/android/camera/manager/SettingManager;->collapse(Z)Z

    goto :goto_0
.end method

.method public onOrientationChanged(I)V
    .locals 2
    .param p1    # I

    invoke-super {p0, p1}, Lcom/android/camera/manager/ViewManager;->onOrientationChanged(I)V

    iget-object v0, p0, Lcom/android/camera/manager/SettingManager;->mSettingLayout:Landroid/view/ViewGroup;

    const/4 v1, 0x1

    invoke-static {v0, p1, v1}, Lcom/android/camera/Util;->setOrientation(Landroid/view/View;IZ)V

    return-void
.end method

.method public onPreferenceReady()V
    .locals 0

    invoke-direct {p0}, Lcom/android/camera/manager/SettingManager;->releaseSettingResource()V

    return-void
.end method

.method public onRefresh()V
    .locals 3

    sget-boolean v0, Lcom/android/camera/manager/SettingManager;->LOG:Z

    if-eqz v0, :cond_0

    const-string v0, "SettingManager"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "onRefresh() isShowing()="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lcom/android/camera/manager/ViewManager;->isShowing()Z

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", mShowingContainer="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-boolean v2, p0, Lcom/android/camera/manager/SettingManager;->mShowingContainer:Z

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/android/camera/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    iget-boolean v0, p0, Lcom/android/camera/manager/SettingManager;->mShowingContainer:Z

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/android/camera/manager/SettingManager;->mAdapter:Lcom/android/camera/manager/SettingManager$MyPagerAdapter;

    if-eqz v0, :cond_1

    invoke-virtual {p0}, Lcom/android/camera/manager/ViewManager;->getContext()Lcom/android/camera/Camera;

    move-result-object v0

    invoke-virtual {v0}, Lcom/android/camera/Camera;->getSettingChecker()Lcom/android/camera/SettingChecker;

    move-result-object v0

    invoke-virtual {v0}, Lcom/android/camera/SettingChecker;->applyParametersToUI()V

    iget-object v0, p0, Lcom/android/camera/manager/SettingManager;->mAdapter:Lcom/android/camera/manager/SettingManager$MyPagerAdapter;

    invoke-virtual {v0}, Lcom/android/camera/manager/SettingManager$MyPagerAdapter;->notifyDataSetChanged()V

    :cond_1
    return-void
.end method

.method protected onRelease()V
    .locals 0

    invoke-super {p0}, Lcom/android/camera/manager/ViewManager;->onRelease()V

    invoke-direct {p0}, Lcom/android/camera/manager/SettingManager;->releaseSettingResource()V

    return-void
.end method

.method public onRestorePreferencesClicked()V
    .locals 3

    sget-boolean v0, Lcom/android/camera/manager/SettingManager;->LOG:Z

    if-eqz v0, :cond_0

    const-string v0, "SettingManager"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "onRestorePreferencesClicked() mShowingContainer="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-boolean v2, p0, Lcom/android/camera/manager/SettingManager;->mShowingContainer:Z

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/android/camera/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    iget-object v0, p0, Lcom/android/camera/manager/SettingManager;->mListener:Lcom/android/camera/manager/SettingManager$SettingListener;

    if-eqz v0, :cond_1

    iget-boolean v0, p0, Lcom/android/camera/manager/SettingManager;->mShowingContainer:Z

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/android/camera/manager/SettingManager;->mListener:Lcom/android/camera/manager/SettingManager$SettingListener;

    invoke-interface {v0}, Lcom/android/camera/manager/SettingManager$SettingListener;->onRestorePreferencesClicked()V

    :cond_1
    return-void
.end method

.method public onSettingChanged(Lcom/android/camera/ui/SettingListLayout;)V
    .locals 3
    .param p1    # Lcom/android/camera/ui/SettingListLayout;

    sget-boolean v0, Lcom/android/camera/manager/SettingManager;->LOG:Z

    if-eqz v0, :cond_0

    const-string v0, "SettingManager"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "onSettingChanged("

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ")"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/android/camera/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    iget-object v0, p0, Lcom/android/camera/manager/SettingManager;->mListener:Lcom/android/camera/manager/SettingManager$SettingListener;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/android/camera/manager/SettingManager;->mListener:Lcom/android/camera/manager/SettingManager$SettingListener;

    invoke-interface {v0}, Lcom/android/camera/manager/SettingManager$SettingListener;->onSharedPreferenceChanged()V

    :cond_1
    invoke-virtual {p0}, Lcom/android/camera/manager/ViewManager;->refresh()V

    return-void
.end method

.method public onTabChanged(Ljava/lang/String;)V
    .locals 4
    .param p1    # Ljava/lang/String;

    const/4 v0, -0x1

    iget-object v1, p0, Lcom/android/camera/manager/SettingManager;->mTabHost:Landroid/widget/TabHost;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/android/camera/manager/SettingManager;->mPager:Lvedroid/support/v4/view/ViewPager;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/android/camera/manager/SettingManager;->mTabHost:Landroid/widget/TabHost;

    invoke-virtual {v1}, Landroid/widget/TabHost;->getCurrentTab()I

    move-result v0

    iget-object v1, p0, Lcom/android/camera/manager/SettingManager;->mPager:Lvedroid/support/v4/view/ViewPager;

    invoke-virtual {v1, v0}, Lvedroid/support/v4/view/ViewPager;->setCurrentItem(I)V

    :cond_0
    sget-boolean v1, Lcom/android/camera/manager/SettingManager;->LOG:Z

    if-eqz v1, :cond_1

    const-string v1, "SettingManager"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "onTabChanged("

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ") currentIndex="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/android/camera/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    :cond_1
    return-void
.end method

.method public setListener(Lcom/android/camera/manager/SettingManager$SettingListener;)V
    .locals 0
    .param p1    # Lcom/android/camera/manager/SettingManager$SettingListener;

    iput-object p1, p0, Lcom/android/camera/manager/SettingManager;->mListener:Lcom/android/camera/manager/SettingManager$SettingListener;

    return-void
.end method
