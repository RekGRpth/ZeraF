.class public Lcom/android/camera/manager/PanoramaViewManager;
.super Lcom/android/camera/manager/ViewManager;
.source "PanoramaViewManager.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/camera/manager/PanoramaViewManager$ViewChangeListener;
    }
.end annotation


# static fields
.field static final ANIMATION:Z = true

.field private static final DIRECTION_DOWN:I = 0x3

.field private static final DIRECTION_LEFT:I = 0x1

.field private static final DIRECTION_RIGHT:I = 0x0

.field private static final DIRECTION_UNKNOWN:I = 0x4

.field private static final DIRECTION_UP:I = 0x2

.field private static final LOG:Z

.field public static final MAV_VIEW:I = 0x1

.field private static final NONE_ORIENTATION:I = -0x1

.field public static final PANORAMA_VIEW:I = 0x0

.field private static final PANO_3D_OVERLAP_DISTANCE:I = 0x20

.field private static final TAG:Ljava/lang/String; = "PanoramaViewManager"

.field private static final TARGET_DISTANCE_HORIZONTAL:I = 0xa0

.field private static final TARGET_DISTANCE_VERTICAL:I = 0x78


# instance fields
.field private mAnimation:Lcom/android/camera/AnimationController;

.field private mCenterIndicator:Landroid/view/ViewGroup;

.field private mCollimatedArrowsDrawable:Landroid/view/ViewGroup;

.field private mDirectionSigns:[Landroid/view/ViewGroup;

.field private mDisplayDirection:I

.field private mDisplayMatrix:Landroid/graphics/Matrix;

.field private mDisplayOrientaion:I

.field private mDisplayRotation:I

.field private mHalfArrowHeight:I

.field private mHalfArrowLength:I

.field private mHoldOrientation:I

.field private mNaviLine:Lcom/android/camera/ui/NaviLineImageView;

.field private mNeedInitialize:Z

.field private mOnSizeChangedListener:Lcom/android/camera/ui/RotateLayout$OnSizeChangedListener;

.field private mPanoView:Landroid/view/View;

.field private mPreviewHeight:I

.field private mPreviewWidth:I

.field private mProgressIndicator:Lcom/android/camera/ui/ProgressIndicator;

.field private mRootView:Landroid/view/View;

.field private mS3DMode:Z

.field private mScreenProgressLayout:Lcom/android/camera/ui/RotateLayout;

.field private mSensorDirection:I

.field private mSensorMatrix:[Landroid/graphics/Matrix;

.field private mViewCategory:I

.field private mViewChangedListener:Lcom/android/camera/manager/PanoramaViewManager$ViewChangeListener;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    sget-boolean v0, Lcom/android/camera/Log;->LOGV:Z

    sput-boolean v0, Lcom/android/camera/manager/PanoramaViewManager;->LOG:Z

    return-void
.end method

.method public constructor <init>(Lcom/android/camera/Camera;I)V
    .locals 3
    .param p1    # Lcom/android/camera/Camera;
    .param p2    # I

    const/4 v2, 0x4

    const/4 v1, 0x0

    invoke-direct {p0, p1}, Lcom/android/camera/manager/ViewManager;-><init>(Lcom/android/camera/Camera;)V

    new-array v0, v2, [Landroid/view/ViewGroup;

    iput-object v0, p0, Lcom/android/camera/manager/PanoramaViewManager;->mDirectionSigns:[Landroid/view/ViewGroup;

    iput-boolean v1, p0, Lcom/android/camera/manager/PanoramaViewManager;->mS3DMode:Z

    new-instance v0, Landroid/graphics/Matrix;

    invoke-direct {v0}, Landroid/graphics/Matrix;-><init>()V

    iput-object v0, p0, Lcom/android/camera/manager/PanoramaViewManager;->mDisplayMatrix:Landroid/graphics/Matrix;

    iput v2, p0, Lcom/android/camera/manager/PanoramaViewManager;->mSensorDirection:I

    iput v2, p0, Lcom/android/camera/manager/PanoramaViewManager;->mDisplayDirection:I

    iput v1, p0, Lcom/android/camera/manager/PanoramaViewManager;->mHalfArrowHeight:I

    iput v1, p0, Lcom/android/camera/manager/PanoramaViewManager;->mHalfArrowLength:I

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/android/camera/manager/PanoramaViewManager;->mNeedInitialize:Z

    const/4 v0, -0x1

    iput v0, p0, Lcom/android/camera/manager/PanoramaViewManager;->mHoldOrientation:I

    new-instance v0, Lcom/android/camera/manager/PanoramaViewManager$1;

    invoke-direct {v0, p0}, Lcom/android/camera/manager/PanoramaViewManager$1;-><init>(Lcom/android/camera/manager/PanoramaViewManager;)V

    iput-object v0, p0, Lcom/android/camera/manager/PanoramaViewManager;->mOnSizeChangedListener:Lcom/android/camera/ui/RotateLayout$OnSizeChangedListener;

    iput p2, p0, Lcom/android/camera/manager/PanoramaViewManager;->mViewCategory:I

    return-void
.end method

.method static synthetic access$000()Z
    .locals 1

    sget-boolean v0, Lcom/android/camera/manager/PanoramaViewManager;->LOG:Z

    return v0
.end method

.method static synthetic access$102(Lcom/android/camera/manager/PanoramaViewManager;I)I
    .locals 0
    .param p0    # Lcom/android/camera/manager/PanoramaViewManager;
    .param p1    # I

    iput p1, p0, Lcom/android/camera/manager/PanoramaViewManager;->mPreviewWidth:I

    return p1
.end method

.method static synthetic access$202(Lcom/android/camera/manager/PanoramaViewManager;I)I
    .locals 0
    .param p0    # Lcom/android/camera/manager/PanoramaViewManager;
    .param p1    # I

    iput p1, p0, Lcom/android/camera/manager/PanoramaViewManager;->mPreviewHeight:I

    return p1
.end method

.method private filterViewCategory(I)Z
    .locals 3
    .param p1    # I

    iget v0, p0, Lcom/android/camera/manager/PanoramaViewManager;->mViewCategory:I

    if-eq v0, p1, :cond_1

    sget-boolean v0, Lcom/android/camera/manager/PanoramaViewManager;->LOG:Z

    if-eqz v0, :cond_0

    const-string v0, "PanoramaViewManager"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Only panorama could call this method. mViewCategory="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/android/camera/manager/PanoramaViewManager;->mViewCategory:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/android/camera/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x1

    goto :goto_0
.end method

.method private getArrowHL()V
    .locals 3

    iget v2, p0, Lcom/android/camera/manager/PanoramaViewManager;->mHalfArrowHeight:I

    if-nez v2, :cond_0

    iget-object v2, p0, Lcom/android/camera/manager/PanoramaViewManager;->mNaviLine:Lcom/android/camera/ui/NaviLineImageView;

    invoke-virtual {v2}, Landroid/view/View;->getWidth()I

    move-result v1

    iget-object v2, p0, Lcom/android/camera/manager/PanoramaViewManager;->mNaviLine:Lcom/android/camera/ui/NaviLineImageView;

    invoke-virtual {v2}, Landroid/view/View;->getHeight()I

    move-result v0

    if-le v1, v0, :cond_1

    shr-int/lit8 v2, v1, 0x1

    iput v2, p0, Lcom/android/camera/manager/PanoramaViewManager;->mHalfArrowLength:I

    shr-int/lit8 v2, v0, 0x1

    iput v2, p0, Lcom/android/camera/manager/PanoramaViewManager;->mHalfArrowHeight:I

    :cond_0
    :goto_0
    return-void

    :cond_1
    shr-int/lit8 v2, v1, 0x1

    iput v2, p0, Lcom/android/camera/manager/PanoramaViewManager;->mHalfArrowHeight:I

    shr-int/lit8 v2, v0, 0x1

    iput v2, p0, Lcom/android/camera/manager/PanoramaViewManager;->mHalfArrowLength:I

    goto :goto_0
.end method

.method private initializeViewManager()V
    .locals 8

    const/16 v7, 0x8

    const/4 v6, 0x2

    const/4 v5, 0x0

    const/4 v4, 0x1

    iget-object v0, p0, Lcom/android/camera/manager/PanoramaViewManager;->mRootView:Landroid/view/View;

    const v1, 0x7f0b0096

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/android/camera/manager/PanoramaViewManager;->mPanoView:Landroid/view/View;

    iget-object v0, p0, Lcom/android/camera/manager/PanoramaViewManager;->mRootView:Landroid/view/View;

    const v1, 0x7f0b0095

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/android/camera/ui/RotateLayout;

    iput-object v0, p0, Lcom/android/camera/manager/PanoramaViewManager;->mScreenProgressLayout:Lcom/android/camera/ui/RotateLayout;

    iget-object v0, p0, Lcom/android/camera/manager/PanoramaViewManager;->mRootView:Landroid/view/View;

    const v1, 0x7f0b0097

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    iput-object v0, p0, Lcom/android/camera/manager/PanoramaViewManager;->mCenterIndicator:Landroid/view/ViewGroup;

    iget-object v1, p0, Lcom/android/camera/manager/PanoramaViewManager;->mDirectionSigns:[Landroid/view/ViewGroup;

    iget-object v0, p0, Lcom/android/camera/manager/PanoramaViewManager;->mRootView:Landroid/view/View;

    const v2, 0x7f0b00aa

    invoke-virtual {v0, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    aput-object v0, v1, v5

    iget-object v1, p0, Lcom/android/camera/manager/PanoramaViewManager;->mDirectionSigns:[Landroid/view/ViewGroup;

    iget-object v0, p0, Lcom/android/camera/manager/PanoramaViewManager;->mRootView:Landroid/view/View;

    const v2, 0x7f0b00a5

    invoke-virtual {v0, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    aput-object v0, v1, v4

    iget-object v1, p0, Lcom/android/camera/manager/PanoramaViewManager;->mDirectionSigns:[Landroid/view/ViewGroup;

    iget-object v0, p0, Lcom/android/camera/manager/PanoramaViewManager;->mRootView:Landroid/view/View;

    const v2, 0x7f0b00af

    invoke-virtual {v0, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    aput-object v0, v1, v6

    iget-object v1, p0, Lcom/android/camera/manager/PanoramaViewManager;->mDirectionSigns:[Landroid/view/ViewGroup;

    const/4 v2, 0x3

    iget-object v0, p0, Lcom/android/camera/manager/PanoramaViewManager;->mRootView:Landroid/view/View;

    const v3, 0x7f0b00b2

    invoke-virtual {v0, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    aput-object v0, v1, v2

    new-instance v1, Lcom/android/camera/AnimationController;

    iget-object v2, p0, Lcom/android/camera/manager/PanoramaViewManager;->mDirectionSigns:[Landroid/view/ViewGroup;

    iget-object v0, p0, Lcom/android/camera/manager/PanoramaViewManager;->mCenterIndicator:Landroid/view/ViewGroup;

    invoke-virtual {v0, v5}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    invoke-direct {v1, v2, v0}, Lcom/android/camera/AnimationController;-><init>([Landroid/view/ViewGroup;Landroid/view/ViewGroup;)V

    iput-object v1, p0, Lcom/android/camera/manager/PanoramaViewManager;->mAnimation:Lcom/android/camera/AnimationController;

    iget v0, p0, Lcom/android/camera/manager/PanoramaViewManager;->mViewCategory:I

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/android/camera/manager/PanoramaViewManager;->mRootView:Landroid/view/View;

    const v1, 0x7f0b00b7

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/android/camera/ui/NaviLineImageView;

    iput-object v0, p0, Lcom/android/camera/manager/PanoramaViewManager;->mNaviLine:Lcom/android/camera/ui/NaviLineImageView;

    iget-object v0, p0, Lcom/android/camera/manager/PanoramaViewManager;->mRootView:Landroid/view/View;

    const v1, 0x7f0b00a4

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    iput-object v0, p0, Lcom/android/camera/manager/PanoramaViewManager;->mCollimatedArrowsDrawable:Landroid/view/ViewGroup;

    new-instance v0, Lcom/android/camera/ui/ProgressIndicator;

    invoke-virtual {p0}, Lcom/android/camera/manager/ViewManager;->getContext()Lcom/android/camera/Camera;

    move-result-object v1

    invoke-direct {v0, v1, v6}, Lcom/android/camera/ui/ProgressIndicator;-><init>(Landroid/app/Activity;I)V

    iput-object v0, p0, Lcom/android/camera/manager/PanoramaViewManager;->mProgressIndicator:Lcom/android/camera/ui/ProgressIndicator;

    iget-object v0, p0, Lcom/android/camera/manager/PanoramaViewManager;->mProgressIndicator:Lcom/android/camera/ui/ProgressIndicator;

    invoke-virtual {v0, v7}, Lcom/android/camera/ui/ProgressIndicator;->setVisibility(I)V

    iget-object v0, p0, Lcom/android/camera/manager/PanoramaViewManager;->mProgressIndicator:Lcom/android/camera/ui/ProgressIndicator;

    invoke-virtual {p0}, Lcom/android/camera/manager/ViewManager;->getOrientation()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/android/camera/ui/ProgressIndicator;->setOrientation(I)V

    invoke-direct {p0}, Lcom/android/camera/manager/PanoramaViewManager;->prepareSensorMatrix()V

    :cond_0
    :goto_0
    invoke-virtual {p0}, Lcom/android/camera/manager/ViewManager;->getContext()Lcom/android/camera/Camera;

    move-result-object v0

    invoke-virtual {v0}, Lcom/android/camera/Camera;->getDisplayOrientation()I

    move-result v0

    iput v0, p0, Lcom/android/camera/manager/PanoramaViewManager;->mDisplayOrientaion:I

    invoke-virtual {p0}, Lcom/android/camera/manager/ViewManager;->getContext()Lcom/android/camera/Camera;

    move-result-object v0

    invoke-virtual {v0}, Lcom/android/camera/Camera;->getDisplayRotation()I

    move-result v0

    iput v0, p0, Lcom/android/camera/manager/PanoramaViewManager;->mDisplayRotation:I

    iget-object v0, p0, Lcom/android/camera/manager/PanoramaViewManager;->mScreenProgressLayout:Lcom/android/camera/ui/RotateLayout;

    iget-object v1, p0, Lcom/android/camera/manager/PanoramaViewManager;->mOnSizeChangedListener:Lcom/android/camera/ui/RotateLayout$OnSizeChangedListener;

    invoke-virtual {v0, v1}, Lcom/android/camera/ui/RotateLayout;->setOnSizeChangedListener(Lcom/android/camera/ui/RotateLayout$OnSizeChangedListener;)V

    return-void

    :cond_1
    iget v0, p0, Lcom/android/camera/manager/PanoramaViewManager;->mViewCategory:I

    if-ne v0, v4, :cond_0

    new-instance v0, Lcom/android/camera/ui/ProgressIndicator;

    invoke-virtual {p0}, Lcom/android/camera/manager/ViewManager;->getContext()Lcom/android/camera/Camera;

    move-result-object v1

    invoke-direct {v0, v1, v4}, Lcom/android/camera/ui/ProgressIndicator;-><init>(Landroid/app/Activity;I)V

    iput-object v0, p0, Lcom/android/camera/manager/PanoramaViewManager;->mProgressIndicator:Lcom/android/camera/ui/ProgressIndicator;

    iget-object v0, p0, Lcom/android/camera/manager/PanoramaViewManager;->mProgressIndicator:Lcom/android/camera/ui/ProgressIndicator;

    invoke-virtual {v0, v7}, Lcom/android/camera/ui/ProgressIndicator;->setVisibility(I)V

    iget-object v0, p0, Lcom/android/camera/manager/PanoramaViewManager;->mProgressIndicator:Lcom/android/camera/ui/ProgressIndicator;

    invoke-virtual {p0}, Lcom/android/camera/manager/ViewManager;->getOrientation()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/android/camera/ui/ProgressIndicator;->setOrientation(I)V

    goto :goto_0
.end method

.method private prepareSensorMatrix()V
    .locals 7

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v2, 0x1

    const/4 v4, 0x0

    const/high16 v3, -0x40800000

    const/4 v0, 0x4

    new-array v0, v0, [Landroid/graphics/Matrix;

    iput-object v0, p0, Lcom/android/camera/manager/PanoramaViewManager;->mSensorMatrix:[Landroid/graphics/Matrix;

    iget-object v0, p0, Lcom/android/camera/manager/PanoramaViewManager;->mSensorMatrix:[Landroid/graphics/Matrix;

    new-instance v1, Landroid/graphics/Matrix;

    invoke-direct {v1}, Landroid/graphics/Matrix;-><init>()V

    aput-object v1, v0, v2

    iget-object v0, p0, Lcom/android/camera/manager/PanoramaViewManager;->mSensorMatrix:[Landroid/graphics/Matrix;

    aget-object v0, v0, v2

    invoke-virtual {v0, v3, v3}, Landroid/graphics/Matrix;->setScale(FF)V

    iget-object v0, p0, Lcom/android/camera/manager/PanoramaViewManager;->mSensorMatrix:[Landroid/graphics/Matrix;

    aget-object v0, v0, v2

    const/4 v1, 0x0

    const/high16 v2, 0x42f00000

    invoke-virtual {v0, v1, v2}, Landroid/graphics/Matrix;->postTranslate(FF)Z

    iget-object v0, p0, Lcom/android/camera/manager/PanoramaViewManager;->mSensorMatrix:[Landroid/graphics/Matrix;

    new-instance v1, Landroid/graphics/Matrix;

    invoke-direct {v1}, Landroid/graphics/Matrix;-><init>()V

    aput-object v1, v0, v4

    iget-object v0, p0, Lcom/android/camera/manager/PanoramaViewManager;->mSensorMatrix:[Landroid/graphics/Matrix;

    aget-object v0, v0, v4

    invoke-virtual {v0, v3, v3}, Landroid/graphics/Matrix;->setScale(FF)V

    iget-object v0, p0, Lcom/android/camera/manager/PanoramaViewManager;->mSensorMatrix:[Landroid/graphics/Matrix;

    aget-object v0, v0, v4

    const/high16 v1, 0x43a00000

    const/high16 v2, 0x42f00000

    invoke-virtual {v0, v1, v2}, Landroid/graphics/Matrix;->postTranslate(FF)Z

    iget-object v0, p0, Lcom/android/camera/manager/PanoramaViewManager;->mSensorMatrix:[Landroid/graphics/Matrix;

    new-instance v1, Landroid/graphics/Matrix;

    invoke-direct {v1}, Landroid/graphics/Matrix;-><init>()V

    aput-object v1, v0, v5

    iget-object v0, p0, Lcom/android/camera/manager/PanoramaViewManager;->mSensorMatrix:[Landroid/graphics/Matrix;

    aget-object v0, v0, v5

    invoke-virtual {v0, v3, v3}, Landroid/graphics/Matrix;->setScale(FF)V

    iget-object v0, p0, Lcom/android/camera/manager/PanoramaViewManager;->mSensorMatrix:[Landroid/graphics/Matrix;

    aget-object v0, v0, v5

    const/high16 v1, 0x43200000

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/graphics/Matrix;->postTranslate(FF)Z

    iget-object v0, p0, Lcom/android/camera/manager/PanoramaViewManager;->mSensorMatrix:[Landroid/graphics/Matrix;

    new-instance v1, Landroid/graphics/Matrix;

    invoke-direct {v1}, Landroid/graphics/Matrix;-><init>()V

    aput-object v1, v0, v6

    iget-object v0, p0, Lcom/android/camera/manager/PanoramaViewManager;->mSensorMatrix:[Landroid/graphics/Matrix;

    aget-object v0, v0, v6

    invoke-virtual {v0, v3, v3}, Landroid/graphics/Matrix;->setScale(FF)V

    iget-object v0, p0, Lcom/android/camera/manager/PanoramaViewManager;->mSensorMatrix:[Landroid/graphics/Matrix;

    aget-object v0, v0, v6

    const/high16 v1, 0x43200000

    const/high16 v2, 0x43700000

    invoke-virtual {v0, v1, v2}, Landroid/graphics/Matrix;->postTranslate(FF)Z

    return-void
.end method

.method private prepareTransformMatrix(I)V
    .locals 9
    .param p1    # I

    const/4 v8, 0x0

    const/high16 v7, 0x40000000

    iget-object v4, p0, Lcom/android/camera/manager/PanoramaViewManager;->mDisplayMatrix:Landroid/graphics/Matrix;

    invoke-virtual {v4}, Landroid/graphics/Matrix;->reset()V

    iget v4, p0, Lcom/android/camera/manager/PanoramaViewManager;->mPreviewWidth:I

    shr-int/lit8 v1, v4, 0x1

    iget v4, p0, Lcom/android/camera/manager/PanoramaViewManager;->mPreviewHeight:I

    shr-int/lit8 v0, v4, 0x1

    invoke-direct {p0}, Lcom/android/camera/manager/PanoramaViewManager;->getArrowHL()V

    int-to-float v4, v1

    iget v5, p0, Lcom/android/camera/manager/PanoramaViewManager;->mHalfArrowLength:I

    int-to-float v5, v5

    sub-float v3, v4, v5

    int-to-float v4, v0

    iget v5, p0, Lcom/android/camera/manager/PanoramaViewManager;->mHalfArrowLength:I

    int-to-float v5, v5

    sub-float v2, v4, v5

    iget-object v4, p0, Lcom/android/camera/manager/PanoramaViewManager;->mDisplayMatrix:Landroid/graphics/Matrix;

    const/high16 v5, 0x43200000

    div-float v5, v3, v5

    const/high16 v6, 0x42f00000

    div-float v6, v2, v6

    invoke-virtual {v4, v5, v6}, Landroid/graphics/Matrix;->postScale(FF)Z

    iget v4, p0, Lcom/android/camera/manager/PanoramaViewManager;->mDisplayOrientaion:I

    sparse-switch v4, :sswitch_data_0

    :goto_0
    :sswitch_0
    iget-object v4, p0, Lcom/android/camera/manager/PanoramaViewManager;->mDisplayMatrix:Landroid/graphics/Matrix;

    iget v5, p0, Lcom/android/camera/manager/PanoramaViewManager;->mHalfArrowLength:I

    int-to-float v5, v5

    iget v6, p0, Lcom/android/camera/manager/PanoramaViewManager;->mHalfArrowLength:I

    int-to-float v6, v6

    invoke-virtual {v4, v5, v6}, Landroid/graphics/Matrix;->postTranslate(FF)Z

    return-void

    :sswitch_1
    iget-object v4, p0, Lcom/android/camera/manager/PanoramaViewManager;->mDisplayMatrix:Landroid/graphics/Matrix;

    neg-float v5, v2

    mul-float/2addr v5, v7

    invoke-virtual {v4, v8, v5}, Landroid/graphics/Matrix;->postTranslate(FF)Z

    iget-object v4, p0, Lcom/android/camera/manager/PanoramaViewManager;->mDisplayMatrix:Landroid/graphics/Matrix;

    const/high16 v5, 0x42b40000

    invoke-virtual {v4, v5}, Landroid/graphics/Matrix;->postRotate(F)Z

    goto :goto_0

    :sswitch_2
    iget-object v4, p0, Lcom/android/camera/manager/PanoramaViewManager;->mDisplayMatrix:Landroid/graphics/Matrix;

    neg-float v5, v3

    mul-float/2addr v5, v7

    neg-float v6, v2

    mul-float/2addr v6, v7

    invoke-virtual {v4, v5, v6}, Landroid/graphics/Matrix;->postTranslate(FF)Z

    iget-object v4, p0, Lcom/android/camera/manager/PanoramaViewManager;->mDisplayMatrix:Landroid/graphics/Matrix;

    const/high16 v5, 0x43340000

    invoke-virtual {v4, v5}, Landroid/graphics/Matrix;->postRotate(F)Z

    goto :goto_0

    :sswitch_3
    iget-object v4, p0, Lcom/android/camera/manager/PanoramaViewManager;->mDisplayMatrix:Landroid/graphics/Matrix;

    neg-float v5, v2

    mul-float/2addr v5, v7

    invoke-virtual {v4, v5, v8}, Landroid/graphics/Matrix;->postTranslate(FF)Z

    iget-object v4, p0, Lcom/android/camera/manager/PanoramaViewManager;->mDisplayMatrix:Landroid/graphics/Matrix;

    const/high16 v5, -0x3d4c0000

    invoke-virtual {v4, v5}, Landroid/graphics/Matrix;->postRotate(F)Z

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x5a -> :sswitch_1
        0xb4 -> :sswitch_2
        0x10e -> :sswitch_3
    .end sparse-switch
.end method

.method private setOrientationIndicator(I)V
    .locals 6
    .param p1    # I

    const/16 v5, 0x10e

    const/16 v4, 0xb4

    const/16 v3, 0x5a

    const/4 v2, 0x0

    const/4 v1, 0x1

    if-nez p1, :cond_1

    iget-object v0, p0, Lcom/android/camera/manager/PanoramaViewManager;->mCollimatedArrowsDrawable:Landroid/view/ViewGroup;

    check-cast v0, Lcom/android/camera/ui/Rotatable;

    invoke-interface {v0, v2, v1}, Lcom/android/camera/ui/Rotatable;->setOrientation(IZ)V

    iget-object v0, p0, Lcom/android/camera/manager/PanoramaViewManager;->mCenterIndicator:Landroid/view/ViewGroup;

    check-cast v0, Lcom/android/camera/ui/Rotatable;

    invoke-interface {v0, v2, v1}, Lcom/android/camera/ui/Rotatable;->setOrientation(IZ)V

    iget-object v0, p0, Lcom/android/camera/manager/PanoramaViewManager;->mNaviLine:Lcom/android/camera/ui/NaviLineImageView;

    const/high16 v1, -0x3d4c0000

    invoke-virtual {v0, v1}, Landroid/view/View;->setRotation(F)V

    :cond_0
    :goto_0
    return-void

    :cond_1
    if-ne p1, v1, :cond_2

    iget-object v0, p0, Lcom/android/camera/manager/PanoramaViewManager;->mCollimatedArrowsDrawable:Landroid/view/ViewGroup;

    check-cast v0, Lcom/android/camera/ui/Rotatable;

    invoke-interface {v0, v4, v1}, Lcom/android/camera/ui/Rotatable;->setOrientation(IZ)V

    iget-object v0, p0, Lcom/android/camera/manager/PanoramaViewManager;->mCenterIndicator:Landroid/view/ViewGroup;

    check-cast v0, Lcom/android/camera/ui/Rotatable;

    invoke-interface {v0, v4, v1}, Lcom/android/camera/ui/Rotatable;->setOrientation(IZ)V

    iget-object v0, p0, Lcom/android/camera/manager/PanoramaViewManager;->mNaviLine:Lcom/android/camera/ui/NaviLineImageView;

    const/high16 v1, 0x42b40000

    invoke-virtual {v0, v1}, Landroid/view/View;->setRotation(F)V

    goto :goto_0

    :cond_2
    const/4 v0, 0x2

    if-ne p1, v0, :cond_3

    iget-object v0, p0, Lcom/android/camera/manager/PanoramaViewManager;->mCollimatedArrowsDrawable:Landroid/view/ViewGroup;

    check-cast v0, Lcom/android/camera/ui/Rotatable;

    invoke-interface {v0, v3, v1}, Lcom/android/camera/ui/Rotatable;->setOrientation(IZ)V

    iget-object v0, p0, Lcom/android/camera/manager/PanoramaViewManager;->mCenterIndicator:Landroid/view/ViewGroup;

    check-cast v0, Lcom/android/camera/ui/Rotatable;

    invoke-interface {v0, v3, v1}, Lcom/android/camera/ui/Rotatable;->setOrientation(IZ)V

    iget-object v0, p0, Lcom/android/camera/manager/PanoramaViewManager;->mNaviLine:Lcom/android/camera/ui/NaviLineImageView;

    const/high16 v1, 0x43340000

    invoke-virtual {v0, v1}, Landroid/view/View;->setRotation(F)V

    goto :goto_0

    :cond_3
    const/4 v0, 0x3

    if-ne p1, v0, :cond_0

    iget-object v0, p0, Lcom/android/camera/manager/PanoramaViewManager;->mCollimatedArrowsDrawable:Landroid/view/ViewGroup;

    check-cast v0, Lcom/android/camera/ui/Rotatable;

    invoke-interface {v0, v5, v1}, Lcom/android/camera/ui/Rotatable;->setOrientation(IZ)V

    iget-object v0, p0, Lcom/android/camera/manager/PanoramaViewManager;->mCenterIndicator:Landroid/view/ViewGroup;

    check-cast v0, Lcom/android/camera/ui/Rotatable;

    invoke-interface {v0, v5, v1}, Lcom/android/camera/ui/Rotatable;->setOrientation(IZ)V

    iget-object v0, p0, Lcom/android/camera/manager/PanoramaViewManager;->mNaviLine:Lcom/android/camera/ui/NaviLineImageView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setRotation(F)V

    goto :goto_0
.end method

.method private updateDirection(I)V
    .locals 5
    .param p1    # I

    const/4 v4, 0x4

    iget v1, p0, Lcom/android/camera/manager/PanoramaViewManager;->mDisplayOrientaion:I

    const/16 v2, 0x5a

    if-ne v1, v2, :cond_1

    const/4 v1, 0x1

    if-eq p1, v1, :cond_0

    if-nez p1, :cond_3

    :cond_0
    rsub-int/lit8 p1, p1, 0x3

    :cond_1
    :goto_0
    sget-boolean v1, Lcom/android/camera/manager/PanoramaViewManager;->LOG:Z

    if-eqz v1, :cond_2

    const-string v1, "PanoramaViewManager"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "updateDirection mDirection: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget v3, p0, Lcom/android/camera/manager/PanoramaViewManager;->mSensorDirection:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " direction: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/android/camera/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    :cond_2
    iget v1, p0, Lcom/android/camera/manager/PanoramaViewManager;->mSensorDirection:I

    if-eq v1, p1, :cond_5

    iput p1, p0, Lcom/android/camera/manager/PanoramaViewManager;->mSensorDirection:I

    iget v1, p0, Lcom/android/camera/manager/PanoramaViewManager;->mSensorDirection:I

    if-eq v1, v4, :cond_4

    iget-object v1, p0, Lcom/android/camera/manager/PanoramaViewManager;->mViewChangedListener:Lcom/android/camera/manager/PanoramaViewManager$ViewChangeListener;

    invoke-interface {v1}, Lcom/android/camera/manager/PanoramaViewManager$ViewChangeListener;->onCaptureBegin()V

    invoke-direct {p0, p1}, Lcom/android/camera/manager/PanoramaViewManager;->setOrientationIndicator(I)V

    iget-object v1, p0, Lcom/android/camera/manager/PanoramaViewManager;->mCenterIndicator:Landroid/view/ViewGroup;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Landroid/view/View;->setVisibility(I)V

    iget-object v1, p0, Lcom/android/camera/manager/PanoramaViewManager;->mAnimation:Lcom/android/camera/AnimationController;

    invoke-virtual {v1}, Lcom/android/camera/AnimationController;->startCenterAnimation()V

    const/4 v0, 0x0

    :goto_1
    if-ge v0, v4, :cond_5

    iget-object v1, p0, Lcom/android/camera/manager/PanoramaViewManager;->mDirectionSigns:[Landroid/view/ViewGroup;

    aget-object v1, v1, v0

    invoke-virtual {v1, v4}, Landroid/view/View;->setVisibility(I)V

    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    :cond_3
    add-int/lit8 p1, p1, -0x2

    goto :goto_0

    :cond_4
    iget-object v1, p0, Lcom/android/camera/manager/PanoramaViewManager;->mCenterIndicator:Landroid/view/ViewGroup;

    invoke-virtual {v1, v4}, Landroid/view/View;->setVisibility(I)V

    :cond_5
    return-void
.end method

.method private updateUIShowingMatrix(III)V
    .locals 9
    .param p1    # I
    .param p2    # I
    .param p3    # I

    const/4 v6, 0x1

    const/4 v8, 0x0

    const/4 v3, 0x2

    new-array v2, v3, [F

    int-to-float v3, p1

    aput v3, v2, v8

    int-to-float v3, p2

    aput v3, v2, v6

    iget-object v3, p0, Lcom/android/camera/manager/PanoramaViewManager;->mSensorMatrix:[Landroid/graphics/Matrix;

    aget-object v3, v3, p3

    invoke-virtual {v3, v2}, Landroid/graphics/Matrix;->mapPoints([F)V

    sget-boolean v3, Lcom/android/camera/manager/PanoramaViewManager;->LOG:Z

    if-eqz v3, :cond_0

    const-string v3, "PanoramaViewManager"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Matrix x = "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    aget v5, v2, v8

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " y = "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    aget v5, v2, v6

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/android/camera/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    invoke-direct {p0, p3}, Lcom/android/camera/manager/PanoramaViewManager;->prepareTransformMatrix(I)V

    iget-object v3, p0, Lcom/android/camera/manager/PanoramaViewManager;->mDisplayMatrix:Landroid/graphics/Matrix;

    invoke-virtual {v3, v2}, Landroid/graphics/Matrix;->mapPoints([F)V

    sget-boolean v3, Lcom/android/camera/manager/PanoramaViewManager;->LOG:Z

    if-eqz v3, :cond_1

    const-string v3, "PanoramaViewManager"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "DisplayMatrix x = "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    aget v5, v2, v8

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " y = "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    aget v5, v2, v6

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/android/camera/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    :cond_1
    aget v3, v2, v8

    float-to-int v0, v3

    aget v3, v2, v6

    float-to-int v1, v3

    iget-object v3, p0, Lcom/android/camera/manager/PanoramaViewManager;->mNaviLine:Lcom/android/camera/ui/NaviLineImageView;

    iget v4, p0, Lcom/android/camera/manager/PanoramaViewManager;->mHalfArrowHeight:I

    sub-int v4, v0, v4

    iget v5, p0, Lcom/android/camera/manager/PanoramaViewManager;->mHalfArrowLength:I

    sub-int v5, v1, v5

    iget v6, p0, Lcom/android/camera/manager/PanoramaViewManager;->mHalfArrowHeight:I

    add-int/2addr v6, v0

    iget v7, p0, Lcom/android/camera/manager/PanoramaViewManager;->mHalfArrowLength:I

    add-int/2addr v7, v1

    invoke-virtual {v3, v4, v5, v6, v7}, Lcom/android/camera/ui/NaviLineImageView;->setLayoutPosition(IIII)V

    invoke-direct {p0, p3}, Lcom/android/camera/manager/PanoramaViewManager;->updateDirection(I)V

    iget-object v3, p0, Lcom/android/camera/manager/PanoramaViewManager;->mNaviLine:Lcom/android/camera/ui/NaviLineImageView;

    invoke-virtual {v3, v8}, Landroid/widget/ImageView;->setVisibility(I)V

    return-void
.end method


# virtual methods
.method protected getView()Landroid/view/View;
    .locals 2

    const v1, 0x7f04002d

    invoke-virtual {p0, v1}, Lcom/android/camera/manager/ViewManager;->inflate(I)Landroid/view/View;

    move-result-object v0

    const v1, 0x7f0b0094

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    iput-object v1, p0, Lcom/android/camera/manager/PanoramaViewManager;->mRootView:Landroid/view/View;

    return-object v0
.end method

.method public onOrientationChanged(I)V
    .locals 3
    .param p1    # I

    sget-boolean v0, Lcom/android/camera/manager/PanoramaViewManager;->LOG:Z

    if-eqz v0, :cond_0

    const-string v0, "PanoramaViewManager"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "onOrientationChanged mContext.getCameraState()="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lcom/android/camera/manager/ViewManager;->getContext()Lcom/android/camera/Camera;

    move-result-object v2

    invoke-virtual {v2}, Lcom/android/camera/Camera;->getCameraState()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " orientation="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/android/camera/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    invoke-virtual {p0}, Lcom/android/camera/manager/ViewManager;->getContext()Lcom/android/camera/Camera;

    move-result-object v0

    invoke-virtual {v0}, Lcom/android/camera/Camera;->getCameraState()I

    move-result v0

    const/4 v1, 0x3

    if-eq v0, v1, :cond_2

    invoke-super {p0, p1}, Lcom/android/camera/manager/ViewManager;->onOrientationChanged(I)V

    iget-object v0, p0, Lcom/android/camera/manager/PanoramaViewManager;->mProgressIndicator:Lcom/android/camera/ui/ProgressIndicator;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/android/camera/manager/PanoramaViewManager;->mProgressIndicator:Lcom/android/camera/ui/ProgressIndicator;

    invoke-virtual {v0, p1}, Lcom/android/camera/ui/ProgressIndicator;->setOrientation(I)V

    :cond_1
    const/4 v0, -0x1

    iput v0, p0, Lcom/android/camera/manager/PanoramaViewManager;->mHoldOrientation:I

    :goto_0
    return-void

    :cond_2
    iput p1, p0, Lcom/android/camera/manager/PanoramaViewManager;->mHoldOrientation:I

    goto :goto_0
.end method

.method protected onRelease()V
    .locals 1

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/android/camera/manager/PanoramaViewManager;->mNeedInitialize:Z

    return-void
.end method

.method public resetController()V
    .locals 7

    const/4 v6, 0x4

    const/4 v5, 0x0

    const/16 v4, 0x8

    sget-boolean v1, Lcom/android/camera/manager/PanoramaViewManager;->LOG:Z

    if-eqz v1, :cond_0

    const-string v1, "PanoramaViewManager"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "resetController mViewCategory="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget v3, p0, Lcom/android/camera/manager/PanoramaViewManager;->mViewCategory:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/android/camera/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    iget-object v1, p0, Lcom/android/camera/manager/PanoramaViewManager;->mPanoView:Landroid/view/View;

    invoke-virtual {v1, v4}, Landroid/view/View;->setVisibility(I)V

    iget-object v1, p0, Lcom/android/camera/manager/PanoramaViewManager;->mProgressIndicator:Lcom/android/camera/ui/ProgressIndicator;

    invoke-virtual {v1, v5}, Lcom/android/camera/ui/ProgressIndicator;->setProgress(I)V

    iget-object v1, p0, Lcom/android/camera/manager/PanoramaViewManager;->mProgressIndicator:Lcom/android/camera/ui/ProgressIndicator;

    invoke-virtual {v1, v4}, Lcom/android/camera/ui/ProgressIndicator;->setVisibility(I)V

    iget-object v1, p0, Lcom/android/camera/manager/PanoramaViewManager;->mAnimation:Lcom/android/camera/AnimationController;

    invoke-virtual {v1}, Lcom/android/camera/AnimationController;->stopCenterAnimation()V

    iget-object v1, p0, Lcom/android/camera/manager/PanoramaViewManager;->mCenterIndicator:Landroid/view/ViewGroup;

    invoke-virtual {v1, v4}, Landroid/view/View;->setVisibility(I)V

    iget v1, p0, Lcom/android/camera/manager/PanoramaViewManager;->mViewCategory:I

    if-nez v1, :cond_1

    iput v6, p0, Lcom/android/camera/manager/PanoramaViewManager;->mSensorDirection:I

    iget-object v1, p0, Lcom/android/camera/manager/PanoramaViewManager;->mNaviLine:Lcom/android/camera/ui/NaviLineImageView;

    invoke-virtual {v1, v4}, Landroid/widget/ImageView;->setVisibility(I)V

    iget-object v1, p0, Lcom/android/camera/manager/PanoramaViewManager;->mCollimatedArrowsDrawable:Landroid/view/ViewGroup;

    invoke-virtual {v1, v4}, Landroid/view/View;->setVisibility(I)V

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v6, :cond_1

    iget-object v1, p0, Lcom/android/camera/manager/PanoramaViewManager;->mDirectionSigns:[Landroid/view/ViewGroup;

    aget-object v1, v1, v0

    invoke-virtual {v1, v5}, Landroid/view/View;->setSelected(Z)V

    iget-object v1, p0, Lcom/android/camera/manager/PanoramaViewManager;->mDirectionSigns:[Landroid/view/ViewGroup;

    aget-object v1, v1, v0

    invoke-virtual {v1, v5}, Landroid/view/View;->setVisibility(I)V

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_1
    return-void
.end method

.method public set3DMode(Z)V
    .locals 0
    .param p1    # Z

    iput-boolean p1, p0, Lcom/android/camera/manager/PanoramaViewManager;->mS3DMode:Z

    return-void
.end method

.method public setProgress(I)V
    .locals 1
    .param p1    # I

    iget-object v0, p0, Lcom/android/camera/manager/PanoramaViewManager;->mProgressIndicator:Lcom/android/camera/ui/ProgressIndicator;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/camera/manager/PanoramaViewManager;->mProgressIndicator:Lcom/android/camera/ui/ProgressIndicator;

    invoke-virtual {v0, p1}, Lcom/android/camera/ui/ProgressIndicator;->setProgress(I)V

    :cond_0
    return-void
.end method

.method public setViewChangedListener(Lcom/android/camera/manager/PanoramaViewManager$ViewChangeListener;)V
    .locals 0
    .param p1    # Lcom/android/camera/manager/PanoramaViewManager$ViewChangeListener;

    iput-object p1, p0, Lcom/android/camera/manager/PanoramaViewManager;->mViewChangedListener:Lcom/android/camera/manager/PanoramaViewManager$ViewChangeListener;

    return-void
.end method

.method public setViewsForNext(I)V
    .locals 3
    .param p1    # I

    const/4 v2, 0x0

    invoke-direct {p0, v2}, Lcom/android/camera/manager/PanoramaViewManager;->filterViewCategory(I)Z

    move-result v0

    if-nez v0, :cond_0

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/android/camera/manager/PanoramaViewManager;->mProgressIndicator:Lcom/android/camera/ui/ProgressIndicator;

    add-int/lit8 v1, p1, 0x1

    invoke-virtual {v0, v1}, Lcom/android/camera/ui/ProgressIndicator;->setProgress(I)V

    if-nez p1, :cond_1

    iget-object v0, p0, Lcom/android/camera/manager/PanoramaViewManager;->mAnimation:Lcom/android/camera/AnimationController;

    invoke-virtual {v0}, Lcom/android/camera/AnimationController;->startDirectionAnimation()V

    goto :goto_0

    :cond_1
    iget-object v0, p0, Lcom/android/camera/manager/PanoramaViewManager;->mNaviLine:Lcom/android/camera/ui/NaviLineImageView;

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    iget-object v0, p0, Lcom/android/camera/manager/PanoramaViewManager;->mAnimation:Lcom/android/camera/AnimationController;

    invoke-virtual {v0}, Lcom/android/camera/AnimationController;->stopCenterAnimation()V

    iget-object v0, p0, Lcom/android/camera/manager/PanoramaViewManager;->mCenterIndicator:Landroid/view/ViewGroup;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    iget-object v0, p0, Lcom/android/camera/manager/PanoramaViewManager;->mCollimatedArrowsDrawable:Landroid/view/ViewGroup;

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    goto :goto_0
.end method

.method public show()V
    .locals 1

    invoke-super {p0}, Lcom/android/camera/manager/ViewManager;->show()V

    iget-boolean v0, p0, Lcom/android/camera/manager/PanoramaViewManager;->mNeedInitialize:Z

    if-eqz v0, :cond_0

    invoke-direct {p0}, Lcom/android/camera/manager/PanoramaViewManager;->initializeViewManager()V

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/android/camera/manager/PanoramaViewManager;->mNeedInitialize:Z

    :cond_0
    invoke-virtual {p0}, Lcom/android/camera/manager/PanoramaViewManager;->showCaptureView()V

    return-void
.end method

.method public showCaptureView()V
    .locals 5

    const/4 v4, 0x4

    const/4 v3, 0x0

    iget v1, p0, Lcom/android/camera/manager/PanoramaViewManager;->mHoldOrientation:I

    const/4 v2, -0x1

    if-eq v1, v2, :cond_0

    iget v1, p0, Lcom/android/camera/manager/PanoramaViewManager;->mHoldOrientation:I

    invoke-virtual {p0, v1}, Lcom/android/camera/manager/PanoramaViewManager;->onOrientationChanged(I)V

    :cond_0
    iget v1, p0, Lcom/android/camera/manager/PanoramaViewManager;->mViewCategory:I

    const/4 v2, 0x1

    if-ne v1, v2, :cond_2

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v4, :cond_1

    iget-object v1, p0, Lcom/android/camera/manager/PanoramaViewManager;->mDirectionSigns:[Landroid/view/ViewGroup;

    aget-object v1, v1, v0

    invoke-virtual {v1, v4}, Landroid/view/View;->setVisibility(I)V

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_1
    iget-object v1, p0, Lcom/android/camera/manager/PanoramaViewManager;->mCenterIndicator:Landroid/view/ViewGroup;

    invoke-virtual {v1, v3}, Landroid/view/View;->setVisibility(I)V

    iget-object v1, p0, Lcom/android/camera/manager/PanoramaViewManager;->mAnimation:Lcom/android/camera/AnimationController;

    invoke-virtual {v1}, Lcom/android/camera/AnimationController;->startCenterAnimation()V

    :goto_1
    iget-object v1, p0, Lcom/android/camera/manager/PanoramaViewManager;->mPanoView:Landroid/view/View;

    invoke-virtual {v1, v3}, Landroid/view/View;->setVisibility(I)V

    iget-object v1, p0, Lcom/android/camera/manager/PanoramaViewManager;->mProgressIndicator:Lcom/android/camera/ui/ProgressIndicator;

    invoke-virtual {v1, v3}, Lcom/android/camera/ui/ProgressIndicator;->setVisibility(I)V

    return-void

    :cond_2
    iget-object v1, p0, Lcom/android/camera/manager/PanoramaViewManager;->mCenterIndicator:Landroid/view/ViewGroup;

    const/16 v2, 0x8

    invoke-virtual {v1, v2}, Landroid/view/View;->setVisibility(I)V

    goto :goto_1
.end method

.method public startCenterAnimation()V
    .locals 2

    iget-object v0, p0, Lcom/android/camera/manager/PanoramaViewManager;->mCollimatedArrowsDrawable:Landroid/view/ViewGroup;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    iget-object v0, p0, Lcom/android/camera/manager/PanoramaViewManager;->mAnimation:Lcom/android/camera/AnimationController;

    invoke-virtual {v0}, Lcom/android/camera/AnimationController;->startCenterAnimation()V

    iget-object v0, p0, Lcom/android/camera/manager/PanoramaViewManager;->mCenterIndicator:Landroid/view/ViewGroup;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    return-void
.end method

.method public updateMovingUI(IIZ)V
    .locals 4
    .param p1    # I
    .param p2    # I
    .param p3    # Z

    const/4 v3, 0x4

    const/4 v2, 0x0

    invoke-direct {p0, v2}, Lcom/android/camera/manager/PanoramaViewManager;->filterViewCategory(I)Z

    move-result v2

    if-nez v2, :cond_0

    :goto_0
    return-void

    :cond_0
    if-eq p2, v3, :cond_1

    if-nez p3, :cond_1

    iget-object v2, p0, Lcom/android/camera/manager/PanoramaViewManager;->mNaviLine:Lcom/android/camera/ui/NaviLineImageView;

    invoke-virtual {v2}, Landroid/view/View;->getWidth()I

    move-result v2

    if-eqz v2, :cond_1

    iget-object v2, p0, Lcom/android/camera/manager/PanoramaViewManager;->mNaviLine:Lcom/android/camera/ui/NaviLineImageView;

    invoke-virtual {v2}, Landroid/view/View;->getHeight()I

    move-result v2

    if-nez v2, :cond_2

    :cond_1
    iget-object v2, p0, Lcom/android/camera/manager/PanoramaViewManager;->mNaviLine:Lcom/android/camera/ui/NaviLineImageView;

    invoke-virtual {v2, v3}, Landroid/widget/ImageView;->setVisibility(I)V

    goto :goto_0

    :cond_2
    const/high16 v2, -0x10000

    and-int/2addr v2, p1

    shr-int/lit8 v2, v2, 0x10

    int-to-short v0, v2

    const v2, 0xffff

    and-int/2addr v2, p1

    int-to-short v1, v2

    invoke-direct {p0, v0, v1, p2}, Lcom/android/camera/manager/PanoramaViewManager;->updateUIShowingMatrix(III)V

    goto :goto_0
.end method
