.class public Lcom/android/camera/manager/ShutterManager;
.super Lcom/android/camera/manager/ViewManager;
.source "ShutterManager.java"

# interfaces
.implements Lcom/android/camera/Camera$OnFullScreenChangedListener;


# static fields
.field private static final LOG:Z

.field public static final SHUTTER_TYPE_CANCEL:I = 0x4

.field public static final SHUTTER_TYPE_CANCEL_VIDEO:I = 0x5

.field public static final SHUTTER_TYPE_OK_CANCEL:I = 0x3

.field public static final SHUTTER_TYPE_PHOTO:I = 0x1

.field public static final SHUTTER_TYPE_PHOTO_VIDEO:I = 0x0

.field public static final SHUTTER_TYPE_VIDEO:I = 0x2

.field private static final TAG:Ljava/lang/String; = "ShutterManager"


# instance fields
.field private mCancelButton:Landroid/view/View;

.field private mCancelButtonEnabled:Z

.field private mCancelListener:Landroid/view/View$OnClickListener;

.field private mFullScreen:Z

.field private mOkButton:Landroid/view/View;

.field private mOklistener:Landroid/view/View$OnClickListener;

.field private mPhotoListener:Lcom/android/camera/ui/ShutterButton$OnShutterButtonListener;

.field private mPhotoShutter:Lcom/android/camera/ui/ShutterButton;

.field private mPhotoShutterEnabled:Z

.field private mShutterType:I

.field private mVideoListener:Lcom/android/camera/ui/ShutterButton$OnShutterButtonListener;

.field private mVideoShutter:Lcom/android/camera/ui/ShutterButton;

.field private mVideoShutterEnabled:Z

.field private mVideoShutterMasked:Z


# direct methods
.method static constructor <clinit>()V
    .locals 1

    sget-boolean v0, Lcom/android/camera/Log;->LOGV:Z

    sput-boolean v0, Lcom/android/camera/manager/ShutterManager;->LOG:Z

    return-void
.end method

.method public constructor <init>(Lcom/android/camera/Camera;)V
    .locals 3
    .param p1    # Lcom/android/camera/Camera;

    const/4 v2, 0x0

    const/4 v1, 0x1

    const/4 v0, 0x2

    invoke-direct {p0, p1, v0}, Lcom/android/camera/manager/ViewManager;-><init>(Lcom/android/camera/Camera;I)V

    iput v2, p0, Lcom/android/camera/manager/ShutterManager;->mShutterType:I

    iput-boolean v1, p0, Lcom/android/camera/manager/ShutterManager;->mPhotoShutterEnabled:Z

    iput-boolean v1, p0, Lcom/android/camera/manager/ShutterManager;->mVideoShutterEnabled:Z

    iput-boolean v1, p0, Lcom/android/camera/manager/ShutterManager;->mCancelButtonEnabled:Z

    iput-boolean v1, p0, Lcom/android/camera/manager/ShutterManager;->mFullScreen:Z

    invoke-virtual {p0, v2}, Lcom/android/camera/manager/ViewManager;->setFileter(Z)V

    invoke-virtual {p1, p0}, Lcom/android/camera/Camera;->addOnFullScreenChangedListener(Lcom/android/camera/Camera$OnFullScreenChangedListener;)Z

    return-void
.end method

.method private applyListener()V
    .locals 3

    iget-object v0, p0, Lcom/android/camera/manager/ShutterManager;->mPhotoShutter:Lcom/android/camera/ui/ShutterButton;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/camera/manager/ShutterManager;->mPhotoShutter:Lcom/android/camera/ui/ShutterButton;

    iget-object v1, p0, Lcom/android/camera/manager/ShutterManager;->mPhotoListener:Lcom/android/camera/ui/ShutterButton$OnShutterButtonListener;

    invoke-virtual {v0, v1}, Lcom/android/camera/ui/ShutterButton;->setOnShutterButtonListener(Lcom/android/camera/ui/ShutterButton$OnShutterButtonListener;)V

    :cond_0
    iget-object v0, p0, Lcom/android/camera/manager/ShutterManager;->mVideoShutter:Lcom/android/camera/ui/ShutterButton;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/android/camera/manager/ShutterManager;->mVideoShutter:Lcom/android/camera/ui/ShutterButton;

    iget-object v1, p0, Lcom/android/camera/manager/ShutterManager;->mVideoListener:Lcom/android/camera/ui/ShutterButton$OnShutterButtonListener;

    invoke-virtual {v0, v1}, Lcom/android/camera/ui/ShutterButton;->setOnShutterButtonListener(Lcom/android/camera/ui/ShutterButton$OnShutterButtonListener;)V

    :cond_1
    iget-object v0, p0, Lcom/android/camera/manager/ShutterManager;->mOkButton:Landroid/view/View;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/android/camera/manager/ShutterManager;->mOkButton:Landroid/view/View;

    iget-object v1, p0, Lcom/android/camera/manager/ShutterManager;->mOklistener:Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    :cond_2
    iget-object v0, p0, Lcom/android/camera/manager/ShutterManager;->mCancelButton:Landroid/view/View;

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/android/camera/manager/ShutterManager;->mCancelButton:Landroid/view/View;

    iget-object v1, p0, Lcom/android/camera/manager/ShutterManager;->mCancelListener:Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    :cond_3
    sget-boolean v0, Lcom/android/camera/manager/ShutterManager;->LOG:Z

    if-eqz v0, :cond_4

    const-string v0, "ShutterManager"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "applyListener() mPhotoShutter=("

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/android/camera/manager/ShutterManager;->mPhotoShutter:Lcom/android/camera/ui/ShutterButton;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/android/camera/manager/ShutterManager;->mPhotoListener:Lcom/android/camera/ui/ShutterButton$OnShutterButtonListener;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "), mVideoShutter=("

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/android/camera/manager/ShutterManager;->mVideoShutter:Lcom/android/camera/ui/ShutterButton;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/android/camera/manager/ShutterManager;->mVideoListener:Lcom/android/camera/ui/ShutterButton$OnShutterButtonListener;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "), mOkButton=("

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/android/camera/manager/ShutterManager;->mOkButton:Landroid/view/View;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/android/camera/manager/ShutterManager;->mOklistener:Landroid/view/View$OnClickListener;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "), mCancelButton=("

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/android/camera/manager/ShutterManager;->mCancelButton:Landroid/view/View;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/android/camera/manager/ShutterManager;->mCancelListener:Landroid/view/View$OnClickListener;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ")"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/android/camera/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    :cond_4
    return-void
.end method


# virtual methods
.method public getPhotoShutter()Lcom/android/camera/ui/ShutterButton;
    .locals 1

    iget-object v0, p0, Lcom/android/camera/manager/ShutterManager;->mPhotoShutter:Lcom/android/camera/ui/ShutterButton;

    return-object v0
.end method

.method public getShutterType()I
    .locals 1

    iget v0, p0, Lcom/android/camera/manager/ShutterManager;->mShutterType:I

    return v0
.end method

.method public getVideoShutter()Lcom/android/camera/ui/ShutterButton;
    .locals 1

    iget-object v0, p0, Lcom/android/camera/manager/ShutterManager;->mVideoShutter:Lcom/android/camera/ui/ShutterButton;

    return-object v0
.end method

.method protected getView()Landroid/view/View;
    .locals 3

    const/4 v1, 0x0

    const v0, 0x7f04000f

    iget v2, p0, Lcom/android/camera/manager/ShutterManager;->mShutterType:I

    packed-switch v2, :pswitch_data_0

    :goto_0
    invoke-virtual {p0, v0}, Lcom/android/camera/manager/ViewManager;->inflate(I)Landroid/view/View;

    move-result-object v1

    const v2, 0x7f0b0017

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Lcom/android/camera/ui/ShutterButton;

    iput-object v2, p0, Lcom/android/camera/manager/ShutterManager;->mPhotoShutter:Lcom/android/camera/ui/ShutterButton;

    const v2, 0x7f0b0015

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Lcom/android/camera/ui/ShutterButton;

    iput-object v2, p0, Lcom/android/camera/manager/ShutterManager;->mVideoShutter:Lcom/android/camera/ui/ShutterButton;

    const v2, 0x7f0b0016

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    iput-object v2, p0, Lcom/android/camera/manager/ShutterManager;->mOkButton:Landroid/view/View;

    const v2, 0x7f0b0014

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    iput-object v2, p0, Lcom/android/camera/manager/ShutterManager;->mCancelButton:Landroid/view/View;

    invoke-direct {p0}, Lcom/android/camera/manager/ShutterManager;->applyListener()V

    return-object v1

    :pswitch_0
    const v0, 0x7f04000f

    goto :goto_0

    :pswitch_1
    const v0, 0x7f04000e

    goto :goto_0

    :pswitch_2
    const v0, 0x7f040010

    goto :goto_0

    :pswitch_3
    const v0, 0x7f04000d

    goto :goto_0

    :pswitch_4
    const v0, 0x7f04000b

    goto :goto_0

    :pswitch_5
    const v0, 0x7f04000c

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
    .end packed-switch
.end method

.method public isCancelButtonEnabled()Z
    .locals 3

    sget-boolean v0, Lcom/android/camera/manager/ShutterManager;->LOG:Z

    if-eqz v0, :cond_0

    const-string v0, "ShutterManager"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "isCancelButtonEnabled() return "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-boolean v2, p0, Lcom/android/camera/manager/ShutterManager;->mCancelButtonEnabled:Z

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/android/camera/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    iget-boolean v0, p0, Lcom/android/camera/manager/ShutterManager;->mCancelButtonEnabled:Z

    return v0
.end method

.method public isPhotoShutterEnabled()Z
    .locals 3

    sget-boolean v0, Lcom/android/camera/manager/ShutterManager;->LOG:Z

    if-eqz v0, :cond_0

    const-string v0, "ShutterManager"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "isPhotoShutterEnabled() return "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-boolean v2, p0, Lcom/android/camera/manager/ShutterManager;->mPhotoShutterEnabled:Z

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/android/camera/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    iget-boolean v0, p0, Lcom/android/camera/manager/ShutterManager;->mPhotoShutterEnabled:Z

    return v0
.end method

.method public isVideoShutterEnabled()Z
    .locals 3

    sget-boolean v0, Lcom/android/camera/manager/ShutterManager;->LOG:Z

    if-eqz v0, :cond_0

    const-string v0, "ShutterManager"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "isVideoShutterEnabled() return "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-boolean v2, p0, Lcom/android/camera/manager/ShutterManager;->mVideoShutterEnabled:Z

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/android/camera/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    iget-boolean v0, p0, Lcom/android/camera/manager/ShutterManager;->mVideoShutterEnabled:Z

    return v0
.end method

.method public onFullScreenChanged(Z)V
    .locals 0
    .param p1    # Z

    iput-boolean p1, p0, Lcom/android/camera/manager/ShutterManager;->mFullScreen:Z

    invoke-virtual {p0}, Lcom/android/camera/manager/ViewManager;->refresh()V

    return-void
.end method

.method public onRefresh()V
    .locals 7

    const/4 v2, 0x1

    const/4 v3, 0x0

    sget-boolean v4, Lcom/android/camera/manager/ShutterManager;->LOG:Z

    if-eqz v4, :cond_0

    const-string v4, "ShutterManager"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "onRefresh() mPhotoShutterEnabled="

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget-boolean v6, p0, Lcom/android/camera/manager/ShutterManager;->mPhotoShutterEnabled:Z

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, ", mFullScreen="

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget-boolean v6, p0, Lcom/android/camera/manager/ShutterManager;->mFullScreen:Z

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, ", isEnabled()="

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {p0}, Lcom/android/camera/manager/ViewManager;->isEnabled()Z

    move-result v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/android/camera/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    iget-object v4, p0, Lcom/android/camera/manager/ShutterManager;->mVideoShutter:Lcom/android/camera/ui/ShutterButton;

    if-eqz v4, :cond_1

    invoke-virtual {p0}, Lcom/android/camera/manager/ViewManager;->getContext()Lcom/android/camera/Camera;

    move-result-object v4

    invoke-virtual {p0}, Lcom/android/camera/manager/ViewManager;->getContext()Lcom/android/camera/Camera;

    move-result-object v5

    invoke-virtual {v5}, Lcom/android/camera/Camera;->getCameraId()I

    move-result v5

    const/16 v6, 0x9

    invoke-static {v4, v5, v6}, Lcom/android/camera/ModeChecker;->getModePickerVisible(Lcom/android/camera/Camera;II)Z

    move-result v1

    iget-boolean v4, p0, Lcom/android/camera/manager/ShutterManager;->mVideoShutterEnabled:Z

    if-eqz v4, :cond_5

    invoke-virtual {p0}, Lcom/android/camera/manager/ViewManager;->isEnabled()Z

    move-result v4

    if-eqz v4, :cond_5

    iget-boolean v4, p0, Lcom/android/camera/manager/ShutterManager;->mFullScreen:Z

    if-eqz v4, :cond_5

    if-eqz v1, :cond_5

    invoke-virtual {p0}, Lcom/android/camera/manager/ViewManager;->getContext()Lcom/android/camera/Camera;

    move-result-object v4

    invoke-virtual {v4}, Lcom/android/camera/Camera;->getWfdManagerLocal()Lcom/android/camera/WfdManagerLocal;

    move-result-object v4

    invoke-virtual {v4}, Lcom/android/camera/WfdManagerLocal;->isWfdEnabled()Z

    move-result v4

    if-nez v4, :cond_5

    move v0, v2

    :goto_0
    iget-object v4, p0, Lcom/android/camera/manager/ShutterManager;->mVideoShutter:Lcom/android/camera/ui/ShutterButton;

    invoke-virtual {v4, v0}, Lcom/android/camera/ui/TwoStateImageView;->setEnabled(Z)V

    iget-object v4, p0, Lcom/android/camera/manager/ShutterManager;->mVideoShutter:Lcom/android/camera/ui/ShutterButton;

    invoke-virtual {v4, v0}, Landroid/view/View;->setClickable(Z)V

    iget-boolean v4, p0, Lcom/android/camera/manager/ShutterManager;->mVideoShutterMasked:Z

    if-eqz v4, :cond_6

    iget-object v4, p0, Lcom/android/camera/manager/ShutterManager;->mVideoShutter:Lcom/android/camera/ui/ShutterButton;

    const v5, 0x7f02002f

    invoke-virtual {v4, v5}, Landroid/widget/ImageView;->setImageResource(I)V

    :cond_1
    :goto_1
    iget-object v4, p0, Lcom/android/camera/manager/ShutterManager;->mPhotoShutter:Lcom/android/camera/ui/ShutterButton;

    if-eqz v4, :cond_2

    iget-boolean v4, p0, Lcom/android/camera/manager/ShutterManager;->mPhotoShutterEnabled:Z

    if-eqz v4, :cond_7

    invoke-virtual {p0}, Lcom/android/camera/manager/ViewManager;->isEnabled()Z

    move-result v4

    if-eqz v4, :cond_7

    iget-boolean v4, p0, Lcom/android/camera/manager/ShutterManager;->mFullScreen:Z

    if-eqz v4, :cond_7

    move v0, v2

    :goto_2
    iget-object v4, p0, Lcom/android/camera/manager/ShutterManager;->mPhotoShutter:Lcom/android/camera/ui/ShutterButton;

    invoke-virtual {v4, v0}, Lcom/android/camera/ui/TwoStateImageView;->setEnabled(Z)V

    iget-object v4, p0, Lcom/android/camera/manager/ShutterManager;->mPhotoShutter:Lcom/android/camera/ui/ShutterButton;

    invoke-virtual {v4, v0}, Landroid/view/View;->setClickable(Z)V

    :cond_2
    iget-object v4, p0, Lcom/android/camera/manager/ShutterManager;->mOkButton:Landroid/view/View;

    if-eqz v4, :cond_3

    invoke-virtual {p0}, Lcom/android/camera/manager/ViewManager;->isEnabled()Z

    move-result v4

    if-eqz v4, :cond_8

    iget-boolean v4, p0, Lcom/android/camera/manager/ShutterManager;->mFullScreen:Z

    if-eqz v4, :cond_8

    move v0, v2

    :goto_3
    iget-object v4, p0, Lcom/android/camera/manager/ShutterManager;->mOkButton:Landroid/view/View;

    invoke-virtual {v4, v0}, Landroid/view/View;->setEnabled(Z)V

    iget-object v4, p0, Lcom/android/camera/manager/ShutterManager;->mOkButton:Landroid/view/View;

    invoke-virtual {v4, v0}, Landroid/view/View;->setClickable(Z)V

    :cond_3
    iget-object v4, p0, Lcom/android/camera/manager/ShutterManager;->mCancelButton:Landroid/view/View;

    if-eqz v4, :cond_4

    iget-boolean v4, p0, Lcom/android/camera/manager/ShutterManager;->mCancelButtonEnabled:Z

    if-eqz v4, :cond_9

    invoke-virtual {p0}, Lcom/android/camera/manager/ViewManager;->isEnabled()Z

    move-result v4

    if-eqz v4, :cond_9

    iget-boolean v4, p0, Lcom/android/camera/manager/ShutterManager;->mFullScreen:Z

    if-eqz v4, :cond_9

    move v0, v2

    :goto_4
    iget-object v2, p0, Lcom/android/camera/manager/ShutterManager;->mCancelButton:Landroid/view/View;

    invoke-virtual {v2, v0}, Landroid/view/View;->setEnabled(Z)V

    iget-object v2, p0, Lcom/android/camera/manager/ShutterManager;->mCancelButton:Landroid/view/View;

    invoke-virtual {v2, v0}, Landroid/view/View;->setClickable(Z)V

    :cond_4
    return-void

    :cond_5
    move v0, v3

    goto :goto_0

    :cond_6
    iget-object v4, p0, Lcom/android/camera/manager/ShutterManager;->mVideoShutter:Lcom/android/camera/ui/ShutterButton;

    const v5, 0x7f02002e

    invoke-virtual {v4, v5}, Landroid/widget/ImageView;->setImageResource(I)V

    goto :goto_1

    :cond_7
    move v0, v3

    goto :goto_2

    :cond_8
    move v0, v3

    goto :goto_3

    :cond_9
    move v0, v3

    goto :goto_4
.end method

.method protected onRelease()V
    .locals 2

    const/4 v1, 0x0

    iget-object v0, p0, Lcom/android/camera/manager/ShutterManager;->mPhotoShutter:Lcom/android/camera/ui/ShutterButton;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/camera/manager/ShutterManager;->mPhotoShutter:Lcom/android/camera/ui/ShutterButton;

    invoke-virtual {v0, v1}, Lcom/android/camera/ui/ShutterButton;->setOnShutterButtonListener(Lcom/android/camera/ui/ShutterButton$OnShutterButtonListener;)V

    :cond_0
    iget-object v0, p0, Lcom/android/camera/manager/ShutterManager;->mVideoShutter:Lcom/android/camera/ui/ShutterButton;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/android/camera/manager/ShutterManager;->mVideoShutter:Lcom/android/camera/ui/ShutterButton;

    invoke-virtual {v0, v1}, Lcom/android/camera/ui/ShutterButton;->setOnShutterButtonListener(Lcom/android/camera/ui/ShutterButton$OnShutterButtonListener;)V

    :cond_1
    iget-object v0, p0, Lcom/android/camera/manager/ShutterManager;->mOkButton:Landroid/view/View;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/android/camera/manager/ShutterManager;->mOkButton:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    :cond_2
    iget-object v0, p0, Lcom/android/camera/manager/ShutterManager;->mCancelButton:Landroid/view/View;

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/android/camera/manager/ShutterManager;->mCancelButton:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    :cond_3
    iput-object v1, p0, Lcom/android/camera/manager/ShutterManager;->mPhotoShutter:Lcom/android/camera/ui/ShutterButton;

    iput-object v1, p0, Lcom/android/camera/manager/ShutterManager;->mVideoShutter:Lcom/android/camera/ui/ShutterButton;

    iput-object v1, p0, Lcom/android/camera/manager/ShutterManager;->mOkButton:Landroid/view/View;

    iput-object v1, p0, Lcom/android/camera/manager/ShutterManager;->mCancelButton:Landroid/view/View;

    return-void
.end method

.method public performPhotoShutter()Z
    .locals 4

    const/4 v0, 0x0

    iget-object v1, p0, Lcom/android/camera/manager/ShutterManager;->mPhotoShutter:Lcom/android/camera/ui/ShutterButton;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/android/camera/manager/ShutterManager;->mPhotoShutter:Lcom/android/camera/ui/ShutterButton;

    invoke-virtual {v1}, Landroid/view/View;->isEnabled()Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/android/camera/manager/ShutterManager;->mPhotoShutter:Lcom/android/camera/ui/ShutterButton;

    invoke-virtual {v1}, Lcom/android/camera/ui/ShutterButton;->performClick()Z

    const/4 v0, 0x1

    :cond_0
    sget-boolean v1, Lcom/android/camera/manager/ShutterManager;->LOG:Z

    if-eqz v1, :cond_1

    const-string v1, "ShutterManager"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "performPhotoShutter() mPhotoShutter="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/android/camera/manager/ShutterManager;->mPhotoShutter:Lcom/android/camera/ui/ShutterButton;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ", return "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/android/camera/manager/ShutterManager;->mPhotoShutter:Lcom/android/camera/ui/ShutterButton;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/android/camera/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    :cond_1
    return v0
.end method

.method public setCancelButtonEnabled(Z)V
    .locals 3
    .param p1    # Z

    sget-boolean v0, Lcom/android/camera/manager/ShutterManager;->LOG:Z

    if-eqz v0, :cond_0

    const-string v0, "ShutterManager"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "setCancelButtonEnabled("

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ")"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/android/camera/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    iput-boolean p1, p0, Lcom/android/camera/manager/ShutterManager;->mCancelButtonEnabled:Z

    invoke-virtual {p0}, Lcom/android/camera/manager/ViewManager;->refresh()V

    return-void
.end method

.method public setEnabled(Z)V
    .locals 0
    .param p1    # Z

    invoke-super {p0, p1}, Lcom/android/camera/manager/ViewManager;->setEnabled(Z)V

    invoke-virtual {p0}, Lcom/android/camera/manager/ViewManager;->refresh()V

    return-void
.end method

.method public setPhotoShutterEnabled(Z)V
    .locals 3
    .param p1    # Z

    sget-boolean v0, Lcom/android/camera/manager/ShutterManager;->LOG:Z

    if-eqz v0, :cond_0

    const-string v0, "ShutterManager"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "setPhotoShutterEnabled("

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ")"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/android/camera/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    iput-boolean p1, p0, Lcom/android/camera/manager/ShutterManager;->mPhotoShutterEnabled:Z

    invoke-virtual {p0}, Lcom/android/camera/manager/ViewManager;->refresh()V

    return-void
.end method

.method public setShutterListener(Lcom/android/camera/ui/ShutterButton$OnShutterButtonListener;Lcom/android/camera/ui/ShutterButton$OnShutterButtonListener;Landroid/view/View$OnClickListener;Landroid/view/View$OnClickListener;)V
    .locals 0
    .param p1    # Lcom/android/camera/ui/ShutterButton$OnShutterButtonListener;
    .param p2    # Lcom/android/camera/ui/ShutterButton$OnShutterButtonListener;
    .param p3    # Landroid/view/View$OnClickListener;
    .param p4    # Landroid/view/View$OnClickListener;

    iput-object p1, p0, Lcom/android/camera/manager/ShutterManager;->mPhotoListener:Lcom/android/camera/ui/ShutterButton$OnShutterButtonListener;

    iput-object p2, p0, Lcom/android/camera/manager/ShutterManager;->mVideoListener:Lcom/android/camera/ui/ShutterButton$OnShutterButtonListener;

    iput-object p3, p0, Lcom/android/camera/manager/ShutterManager;->mOklistener:Landroid/view/View$OnClickListener;

    iput-object p4, p0, Lcom/android/camera/manager/ShutterManager;->mCancelListener:Landroid/view/View$OnClickListener;

    invoke-direct {p0}, Lcom/android/camera/manager/ShutterManager;->applyListener()V

    return-void
.end method

.method public setVideoShutterEnabled(Z)V
    .locals 3
    .param p1    # Z

    sget-boolean v0, Lcom/android/camera/manager/ShutterManager;->LOG:Z

    if-eqz v0, :cond_0

    const-string v0, "ShutterManager"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "setVideoShutterEnabled("

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ")"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/android/camera/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    iput-boolean p1, p0, Lcom/android/camera/manager/ShutterManager;->mVideoShutterEnabled:Z

    invoke-virtual {p0}, Lcom/android/camera/manager/ViewManager;->refresh()V

    return-void
.end method

.method public setVideoShutterMask(Z)V
    .locals 3
    .param p1    # Z

    sget-boolean v0, Lcom/android/camera/manager/ShutterManager;->LOG:Z

    if-eqz v0, :cond_0

    const-string v0, "ShutterManager"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "setVideoShutterMask("

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ")"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/android/camera/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    iput-boolean p1, p0, Lcom/android/camera/manager/ShutterManager;->mVideoShutterMasked:Z

    invoke-virtual {p0}, Lcom/android/camera/manager/ViewManager;->refresh()V

    return-void
.end method

.method public switchShutter(I)V
    .locals 3
    .param p1    # I

    sget-boolean v0, Lcom/android/camera/manager/ShutterManager;->LOG:Z

    if-eqz v0, :cond_0

    const-string v0, "ShutterManager"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "switchShutterType("

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ") mShutterType="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/android/camera/manager/ShutterManager;->mShutterType:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/android/camera/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    iget v0, p0, Lcom/android/camera/manager/ShutterManager;->mShutterType:I

    if-eq v0, p1, :cond_1

    iput p1, p0, Lcom/android/camera/manager/ShutterManager;->mShutterType:I

    invoke-virtual {p0}, Lcom/android/camera/manager/ViewManager;->reInflate()V

    :cond_1
    return-void
.end method
