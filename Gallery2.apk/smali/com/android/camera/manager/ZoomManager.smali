.class public Lcom/android/camera/manager/ZoomManager;
.super Lcom/android/camera/manager/ViewManager;
.source "ZoomManager.java"

# interfaces
.implements Lcom/android/camera/Camera$OnFullScreenChangedListener;
.implements Lcom/android/camera/Camera$Resumable;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/camera/manager/ZoomManager$MyListener;
    }
.end annotation


# static fields
.field private static final FORMATOR:Ljava/text/DecimalFormat;

.field private static final LOG:Z

.field private static final RATIO_FACTOR_RATE:I = 0x64

.field private static final TAG:Ljava/lang/String; = "ZoomManager"

.field private static final UNKNOWN:I = -0x1

.field private static final ZERO:F = 1.0f

.field private static mIgoreDistance:F


# instance fields
.field private mDeviceSupport:Z

.field private mGalleryGestureListener:Lcom/android/gallery3d/ui/GestureRecognizer$Listener;

.field private mGestureListener:Lcom/android/camera/manager/ZoomManager$MyListener;

.field private mIgnorGestureForZooming:Z

.field private mLastZoomRatio:I

.field private mResumed:Z

.field private mZoomIndexFactor:F

.field private mZoomRatios:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 2

    sget-boolean v0, Lcom/android/camera/Log;->LOGV:Z

    sput-boolean v0, Lcom/android/camera/manager/ZoomManager;->LOG:Z

    new-instance v0, Ljava/text/DecimalFormat;

    const-string v1, "#.0"

    invoke-direct {v0, v1}, Ljava/text/DecimalFormat;-><init>(Ljava/lang/String;)V

    sput-object v0, Lcom/android/camera/manager/ZoomManager;->FORMATOR:Ljava/text/DecimalFormat;

    return-void
.end method

.method public constructor <init>(Lcom/android/camera/Camera;)V
    .locals 2
    .param p1    # Lcom/android/camera/Camera;

    invoke-direct {p0, p1}, Lcom/android/camera/manager/ViewManager;-><init>(Lcom/android/camera/Camera;)V

    new-instance v0, Lcom/android/camera/manager/ZoomManager$MyListener;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/android/camera/manager/ZoomManager$MyListener;-><init>(Lcom/android/camera/manager/ZoomManager;Lcom/android/camera/manager/ZoomManager$1;)V

    iput-object v0, p0, Lcom/android/camera/manager/ZoomManager;->mGestureListener:Lcom/android/camera/manager/ZoomManager$MyListener;

    const/4 v0, -0x1

    iput v0, p0, Lcom/android/camera/manager/ZoomManager;->mLastZoomRatio:I

    const/high16 v0, 0x3f800000

    iput v0, p0, Lcom/android/camera/manager/ZoomManager;->mZoomIndexFactor:F

    invoke-virtual {p1, p0}, Lcom/android/camera/Camera;->addResumable(Lcom/android/camera/Camera$Resumable;)Z

    invoke-virtual {p1, p0}, Lcom/android/camera/Camera;->addOnFullScreenChangedListener(Lcom/android/camera/Camera$OnFullScreenChangedListener;)Z

    return-void
.end method

.method static synthetic access$100()Z
    .locals 1

    sget-boolean v0, Lcom/android/camera/manager/ZoomManager;->LOG:Z

    return v0
.end method

.method static synthetic access$1000(Lcom/android/camera/manager/ZoomManager;)I
    .locals 1
    .param p0    # Lcom/android/camera/manager/ZoomManager;

    invoke-direct {p0}, Lcom/android/camera/manager/ZoomManager;->getMaxZoomIndex()I

    move-result v0

    return v0
.end method

.method static synthetic access$1100(Lcom/android/camera/manager/ZoomManager;)F
    .locals 1
    .param p0    # Lcom/android/camera/manager/ZoomManager;

    invoke-direct {p0}, Lcom/android/camera/manager/ZoomManager;->getMaxZoomIndexFactor()F

    move-result v0

    return v0
.end method

.method static synthetic access$1200(Lcom/android/camera/manager/ZoomManager;IZ)V
    .locals 0
    .param p0    # Lcom/android/camera/manager/ZoomManager;
    .param p1    # I
    .param p2    # Z

    invoke-direct {p0, p1, p2}, Lcom/android/camera/manager/ZoomManager;->performZoom(IZ)V

    return-void
.end method

.method static synthetic access$200(Lcom/android/camera/manager/ZoomManager;)Lcom/android/gallery3d/ui/GestureRecognizer$Listener;
    .locals 1
    .param p0    # Lcom/android/camera/manager/ZoomManager;

    iget-object v0, p0, Lcom/android/camera/manager/ZoomManager;->mGalleryGestureListener:Lcom/android/gallery3d/ui/GestureRecognizer$Listener;

    return-object v0
.end method

.method static synthetic access$302(Lcom/android/camera/manager/ZoomManager;Z)Z
    .locals 0
    .param p0    # Lcom/android/camera/manager/ZoomManager;
    .param p1    # Z

    iput-boolean p1, p0, Lcom/android/camera/manager/ZoomManager;->mIgnorGestureForZooming:Z

    return p1
.end method

.method static synthetic access$400(Lcom/android/camera/manager/ZoomManager;)Z
    .locals 1
    .param p0    # Lcom/android/camera/manager/ZoomManager;

    invoke-direct {p0}, Lcom/android/camera/manager/ZoomManager;->shouldIgnoreCurrentGesture()Z

    move-result v0

    return v0
.end method

.method static synthetic access$500(Lcom/android/camera/manager/ZoomManager;FF)Z
    .locals 1
    .param p0    # Lcom/android/camera/manager/ZoomManager;
    .param p1    # F
    .param p2    # F

    invoke-direct {p0, p1, p2}, Lcom/android/camera/manager/ZoomManager;->shouldIgnoreScrollGesture(FF)Z

    move-result v0

    return v0
.end method

.method static synthetic access$600(Lcom/android/camera/manager/ZoomManager;)F
    .locals 1
    .param p0    # Lcom/android/camera/manager/ZoomManager;

    iget v0, p0, Lcom/android/camera/manager/ZoomManager;->mZoomIndexFactor:F

    return v0
.end method

.method static synthetic access$602(Lcom/android/camera/manager/ZoomManager;F)F
    .locals 0
    .param p0    # Lcom/android/camera/manager/ZoomManager;
    .param p1    # F

    iput p1, p0, Lcom/android/camera/manager/ZoomManager;->mZoomIndexFactor:F

    return p1
.end method

.method static synthetic access$632(Lcom/android/camera/manager/ZoomManager;F)F
    .locals 1
    .param p0    # Lcom/android/camera/manager/ZoomManager;
    .param p1    # F

    iget v0, p0, Lcom/android/camera/manager/ZoomManager;->mZoomIndexFactor:F

    mul-float/2addr v0, p1

    iput v0, p0, Lcom/android/camera/manager/ZoomManager;->mZoomIndexFactor:F

    return v0
.end method

.method static synthetic access$700(Lcom/android/camera/manager/ZoomManager;)Z
    .locals 1
    .param p0    # Lcom/android/camera/manager/ZoomManager;

    invoke-direct {p0}, Lcom/android/camera/manager/ZoomManager;->isAppSupported()Z

    move-result v0

    return v0
.end method

.method static synthetic access$800(Lcom/android/camera/manager/ZoomManager;)I
    .locals 1
    .param p0    # Lcom/android/camera/manager/ZoomManager;

    iget v0, p0, Lcom/android/camera/manager/ZoomManager;->mLastZoomRatio:I

    return v0
.end method

.method static synthetic access$900(Lcom/android/camera/manager/ZoomManager;I)I
    .locals 1
    .param p0    # Lcom/android/camera/manager/ZoomManager;
    .param p1    # I

    invoke-direct {p0, p1}, Lcom/android/camera/manager/ZoomManager;->findZoomIndex(I)I

    move-result v0

    return v0
.end method

.method private findZoomIndex(I)I
    .locals 9
    .param p1    # I

    const/4 v1, 0x0

    iget-object v7, p0, Lcom/android/camera/manager/ZoomManager;->mZoomRatios:Ljava/util/List;

    if-eqz v7, :cond_0

    iget-object v7, p0, Lcom/android/camera/manager/ZoomManager;->mZoomRatios:Ljava/util/List;

    invoke-interface {v7}, Ljava/util/List;->size()I

    move-result v3

    const/4 v7, 0x1

    if-ne v3, v7, :cond_1

    const/4 v1, 0x0

    :cond_0
    :goto_0
    return v1

    :cond_1
    iget-object v7, p0, Lcom/android/camera/manager/ZoomManager;->mZoomRatios:Ljava/util/List;

    add-int/lit8 v8, v3, -0x1

    invoke-interface {v7, v8}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Ljava/lang/Integer;

    invoke-virtual {v7}, Ljava/lang/Integer;->intValue()I

    move-result v4

    iget-object v7, p0, Lcom/android/camera/manager/ZoomManager;->mZoomRatios:Ljava/util/List;

    const/4 v8, 0x0

    invoke-interface {v7, v8}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Ljava/lang/Integer;

    invoke-virtual {v7}, Ljava/lang/Integer;->intValue()I

    move-result v5

    if-gt p1, v5, :cond_2

    const/4 v1, 0x0

    goto :goto_0

    :cond_2
    if-lt p1, v4, :cond_3

    add-int/lit8 v1, v3, -0x1

    goto :goto_0

    :cond_3
    const/4 v2, 0x0

    :goto_1
    add-int/lit8 v7, v3, -0x1

    if-ge v2, v7, :cond_0

    iget-object v7, p0, Lcom/android/camera/manager/ZoomManager;->mZoomRatios:Ljava/util/List;

    invoke-interface {v7, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Ljava/lang/Integer;

    invoke-virtual {v7}, Ljava/lang/Integer;->intValue()I

    move-result v0

    iget-object v7, p0, Lcom/android/camera/manager/ZoomManager;->mZoomRatios:Ljava/util/List;

    add-int/lit8 v8, v2, 0x1

    invoke-interface {v7, v8}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Ljava/lang/Integer;

    invoke-virtual {v7}, Ljava/lang/Integer;->intValue()I

    move-result v6

    if-lt p1, v0, :cond_4

    if-ge p1, v6, :cond_4

    move v1, v2

    goto :goto_0

    :cond_4
    add-int/lit8 v2, v2, 0x1

    goto :goto_1
.end method

.method private getMaxZoomIndex()I
    .locals 2

    const/4 v0, -0x1

    iget-object v1, p0, Lcom/android/camera/manager/ZoomManager;->mZoomRatios:Ljava/util/List;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/android/camera/manager/ZoomManager;->mZoomRatios:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    add-int/lit8 v0, v1, -0x1

    :cond_0
    return v0
.end method

.method private getMaxZoomIndexFactor()F
    .locals 2

    invoke-direct {p0}, Lcom/android/camera/manager/ZoomManager;->getMaxZoomRatio()I

    move-result v0

    int-to-float v0, v0

    const/high16 v1, 0x42c80000

    div-float/2addr v0, v1

    return v0
.end method

.method private getMaxZoomRatio()I
    .locals 3

    const/4 v0, -0x1

    iget-object v1, p0, Lcom/android/camera/manager/ZoomManager;->mZoomRatios:Ljava/util/List;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/android/camera/manager/ZoomManager;->mZoomRatios:Ljava/util/List;

    iget-object v2, p0, Lcom/android/camera/manager/ZoomManager;->mZoomRatios:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    add-int/lit8 v2, v2, -0x1

    invoke-interface {v1, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v0

    :cond_0
    return v0
.end method

.method private isAppSupported()Z
    .locals 4

    invoke-virtual {p0}, Lcom/android/camera/manager/ViewManager;->getContext()Lcom/android/camera/Camera;

    move-result-object v1

    invoke-virtual {v1}, Lcom/android/camera/Camera;->getCurrentMode()I

    move-result v1

    invoke-static {v1}, Lcom/android/camera/SettingChecker;->isZoomEnable(I)Z

    move-result v0

    sget-boolean v1, Lcom/android/camera/manager/ZoomManager;->LOG:Z

    if-eqz v1, :cond_0

    const-string v1, "ZoomManager"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "isAppSupported() return "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/android/camera/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    return v0
.end method

.method private isValidZoomIndex(I)Z
    .locals 4
    .param p1    # I

    const/4 v0, 0x0

    iget-object v1, p0, Lcom/android/camera/manager/ZoomManager;->mZoomRatios:Ljava/util/List;

    if-eqz v1, :cond_0

    if-ltz p1, :cond_0

    iget-object v1, p0, Lcom/android/camera/manager/ZoomManager;->mZoomRatios:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    if-ge p1, v1, :cond_0

    const/4 v0, 0x1

    :cond_0
    sget-boolean v1, Lcom/android/camera/manager/ZoomManager;->LOG:Z

    if-eqz v1, :cond_1

    const-string v1, "ZoomManager"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "isValidZoomIndex("

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ") return "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/android/camera/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    :cond_1
    return v0
.end method

.method private performZoom(IZ)V
    .locals 7
    .param p1    # I
    .param p2    # Z

    sget-boolean v2, Lcom/android/camera/manager/ZoomManager;->LOG:Z

    if-eqz v2, :cond_0

    const-string v2, "ZoomManager"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "performZoom("

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ", "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ") mResumed="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-boolean v4, p0, Lcom/android/camera/manager/ZoomManager;->mResumed:Z

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ", mDeviceSupport="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-boolean v4, p0, Lcom/android/camera/manager/ZoomManager;->mDeviceSupport:Z

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/android/camera/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    invoke-virtual {p0}, Lcom/android/camera/manager/ViewManager;->getContext()Lcom/android/camera/Camera;

    move-result-object v2

    invoke-virtual {v2}, Lcom/android/camera/Camera;->getCameraDevice()Lcom/android/camera/CameraManager$CameraProxy;

    move-result-object v2

    if-eqz v2, :cond_1

    iget-boolean v2, p0, Lcom/android/camera/manager/ZoomManager;->mDeviceSupport:Z

    if-eqz v2, :cond_1

    invoke-direct {p0, p1}, Lcom/android/camera/manager/ZoomManager;->isValidZoomIndex(I)Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-direct {p0, p1}, Lcom/android/camera/manager/ZoomManager;->startAsyncZoom(I)V

    iget-object v2, p0, Lcom/android/camera/manager/ZoomManager;->mZoomRatios:Ljava/util/List;

    invoke-interface {v2, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v0

    iget v2, p0, Lcom/android/camera/manager/ZoomManager;->mLastZoomRatio:I

    if-eq v2, v0, :cond_1

    iput v0, p0, Lcom/android/camera/manager/ZoomManager;->mLastZoomRatio:I

    :cond_1
    if-eqz p2, :cond_2

    iget v2, p0, Lcom/android/camera/manager/ZoomManager;->mLastZoomRatio:I

    int-to-float v2, v2

    const/high16 v3, 0x42c80000

    div-float v1, v2, v3

    invoke-virtual {p0}, Lcom/android/camera/manager/ViewManager;->getContext()Lcom/android/camera/Camera;

    move-result-object v2

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "x"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    sget-object v4, Lcom/android/camera/manager/ZoomManager;->FORMATOR:Ljava/text/DecimalFormat;

    float-to-double v5, v1

    invoke-virtual {v4, v5, v6}, Ljava/text/NumberFormat;->format(D)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/android/camera/Camera;->showInfo(Ljava/lang/String;)V

    :cond_2
    return-void
.end method

.method private shouldIgnoreCurrentGesture()Z
    .locals 1

    invoke-direct {p0}, Lcom/android/camera/manager/ZoomManager;->isAppSupported()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/android/camera/manager/ViewManager;->isEnabled()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/android/camera/manager/ZoomManager;->mIgnorGestureForZooming:Z

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private shouldIgnoreScrollGesture(FF)Z
    .locals 3
    .param p1    # F
    .param p2    # F

    sget-boolean v0, Lcom/android/camera/manager/ZoomManager;->LOG:Z

    if-eqz v0, :cond_0

    const-string v0, "ZoomManager"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "shouldIgnoreScrollGesture("

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ") mIgoreDistance = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    sget v2, Lcom/android/camera/manager/ZoomManager;->mIgoreDistance:F

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/android/camera/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    sget v0, Lcom/android/camera/manager/ZoomManager;->mIgoreDistance:F

    const/4 v1, 0x0

    cmpl-float v0, v0, v1

    if-nez v0, :cond_1

    invoke-virtual {p0}, Lcom/android/camera/manager/ViewManager;->getContext()Lcom/android/camera/Camera;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/ContextThemeWrapper;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0a0054

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    int-to-float v0, v0

    sput v0, Lcom/android/camera/manager/ZoomManager;->mIgoreDistance:F

    :cond_1
    invoke-static {p1}, Ljava/lang/Math;->abs(F)F

    move-result v0

    sget v1, Lcom/android/camera/manager/ZoomManager;->mIgoreDistance:F

    cmpg-float v0, v0, v1

    if-gez v0, :cond_2

    invoke-static {p2}, Ljava/lang/Math;->abs(F)F

    move-result v0

    sget v1, Lcom/android/camera/manager/ZoomManager;->mIgoreDistance:F

    cmpg-float v0, v0, v1

    if-gez v0, :cond_2

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_2
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private startAsyncZoom(I)V
    .locals 3
    .param p1    # I

    sget-boolean v0, Lcom/android/camera/manager/ZoomManager;->LOG:Z

    if-eqz v0, :cond_0

    const-string v0, "ZoomManager"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "startAsyncZoom("

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ")"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/android/camera/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    invoke-virtual {p0}, Lcom/android/camera/manager/ViewManager;->getContext()Lcom/android/camera/Camera;

    move-result-object v0

    new-instance v1, Lcom/android/camera/manager/ZoomManager$1;

    invoke-direct {v1, p0, p1}, Lcom/android/camera/manager/ZoomManager$1;-><init>(Lcom/android/camera/manager/ZoomManager;I)V

    invoke-virtual {v0, v1}, Lcom/android/camera/Camera;->lockRun(Ljava/lang/Runnable;)V

    return-void
.end method


# virtual methods
.method public begin()V
    .locals 0

    return-void
.end method

.method public finish()V
    .locals 0

    return-void
.end method

.method protected getView()Landroid/view/View;
    .locals 1

    const/4 v0, 0x0

    return-object v0
.end method

.method public onFullScreenChanged(Z)V
    .locals 0
    .param p1    # Z

    if-eqz p1, :cond_0

    invoke-virtual {p0}, Lcom/android/camera/manager/ZoomManager;->resume()V

    :goto_0
    return-void

    :cond_0
    invoke-virtual {p0}, Lcom/android/camera/manager/ZoomManager;->pause()V

    goto :goto_0
.end method

.method public pause()V
    .locals 3

    sget-boolean v0, Lcom/android/camera/manager/ZoomManager;->LOG:Z

    if-eqz v0, :cond_0

    const-string v0, "ZoomManager"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "pause() mGalleryGestureListener="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/android/camera/manager/ZoomManager;->mGalleryGestureListener:Lcom/android/gallery3d/ui/GestureRecognizer$Listener;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", mResumed="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-boolean v2, p0, Lcom/android/camera/manager/ZoomManager;->mResumed:Z

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/android/camera/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    iget-boolean v0, p0, Lcom/android/camera/manager/ZoomManager;->mResumed:Z

    if-eqz v0, :cond_1

    invoke-virtual {p0}, Lcom/android/camera/manager/ViewManager;->getContext()Lcom/android/camera/Camera;

    move-result-object v0

    iget-object v1, p0, Lcom/android/camera/manager/ZoomManager;->mGalleryGestureListener:Lcom/android/gallery3d/ui/GestureRecognizer$Listener;

    invoke-virtual {v0, v1}, Lcom/android/camera/ActivityBase;->setGestureListener(Lcom/android/gallery3d/ui/GestureRecognizer$Listener;)Lcom/android/gallery3d/ui/GestureRecognizer$Listener;

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/android/camera/manager/ZoomManager;->mGalleryGestureListener:Lcom/android/gallery3d/ui/GestureRecognizer$Listener;

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/android/camera/manager/ZoomManager;->mResumed:Z

    :cond_1
    return-void
.end method

.method public resetZoom()V
    .locals 4

    const/4 v3, 0x0

    sget-boolean v0, Lcom/android/camera/manager/ZoomManager;->LOG:Z

    if-eqz v0, :cond_0

    const-string v0, "ZoomManager"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "resetZoom() mZoomRatios="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/android/camera/manager/ZoomManager;->mZoomRatios:Ljava/util/List;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", mLastZoomRatio="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/android/camera/manager/ZoomManager;->mLastZoomRatio:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/android/camera/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    const/high16 v0, 0x3f800000

    iput v0, p0, Lcom/android/camera/manager/ZoomManager;->mZoomIndexFactor:F

    invoke-direct {p0, v3}, Lcom/android/camera/manager/ZoomManager;->isValidZoomIndex(I)Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/android/camera/manager/ZoomManager;->mZoomRatios:Ljava/util/List;

    invoke-interface {v0, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    iput v0, p0, Lcom/android/camera/manager/ZoomManager;->mLastZoomRatio:I

    :cond_1
    return-void
.end method

.method public resume()V
    .locals 4

    iget-boolean v1, p0, Lcom/android/camera/manager/ZoomManager;->mResumed:Z

    if-nez v1, :cond_1

    invoke-virtual {p0}, Lcom/android/camera/manager/ViewManager;->getContext()Lcom/android/camera/Camera;

    move-result-object v1

    invoke-virtual {v1}, Lcom/android/camera/ActivityBase;->isFullScreen()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-virtual {p0}, Lcom/android/camera/manager/ViewManager;->getContext()Lcom/android/camera/Camera;

    move-result-object v1

    iget-object v2, p0, Lcom/android/camera/manager/ZoomManager;->mGestureListener:Lcom/android/camera/manager/ZoomManager$MyListener;

    invoke-virtual {v1, v2}, Lcom/android/camera/ActivityBase;->setGestureListener(Lcom/android/gallery3d/ui/GestureRecognizer$Listener;)Lcom/android/gallery3d/ui/GestureRecognizer$Listener;

    move-result-object v0

    iget-object v1, p0, Lcom/android/camera/manager/ZoomManager;->mGestureListener:Lcom/android/camera/manager/ZoomManager$MyListener;

    if-eq v0, v1, :cond_0

    iput-object v0, p0, Lcom/android/camera/manager/ZoomManager;->mGalleryGestureListener:Lcom/android/gallery3d/ui/GestureRecognizer$Listener;

    :cond_0
    const/4 v1, 0x1

    iput-boolean v1, p0, Lcom/android/camera/manager/ZoomManager;->mResumed:Z

    :cond_1
    sget-boolean v1, Lcom/android/camera/manager/ZoomManager;->LOG:Z

    if-eqz v1, :cond_2

    const-string v1, "ZoomManager"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "resume() mGalleryGestureListener="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/android/camera/manager/ZoomManager;->mGalleryGestureListener:Lcom/android/gallery3d/ui/GestureRecognizer$Listener;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/android/camera/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    :cond_2
    return-void
.end method

.method public setZoomParameter()V
    .locals 10

    const/4 v8, 0x0

    invoke-direct {p0}, Lcom/android/camera/manager/ZoomManager;->isAppSupported()Z

    move-result v7

    if-eqz v7, :cond_4

    invoke-virtual {p0}, Lcom/android/camera/manager/ViewManager;->getContext()Lcom/android/camera/Camera;

    move-result-object v7

    invoke-virtual {v7}, Lcom/android/camera/Camera;->getParameters()Landroid/hardware/Camera$Parameters;

    move-result-object v7

    invoke-virtual {v7}, Landroid/hardware/Camera$Parameters;->isZoomSupported()Z

    move-result v7

    iput-boolean v7, p0, Lcom/android/camera/manager/ZoomManager;->mDeviceSupport:Z

    invoke-virtual {p0}, Lcom/android/camera/manager/ViewManager;->getContext()Lcom/android/camera/Camera;

    move-result-object v7

    invoke-virtual {v7}, Lcom/android/camera/Camera;->getParameters()Landroid/hardware/Camera$Parameters;

    move-result-object v7

    invoke-virtual {v7}, Landroid/hardware/Camera$Parameters;->getZoomRatios()Ljava/util/List;

    move-result-object v7

    iput-object v7, p0, Lcom/android/camera/manager/ZoomManager;->mZoomRatios:Ljava/util/List;

    invoke-virtual {p0}, Lcom/android/camera/manager/ViewManager;->getContext()Lcom/android/camera/Camera;

    move-result-object v7

    invoke-virtual {v7}, Lcom/android/camera/Camera;->getParameters()Landroid/hardware/Camera$Parameters;

    move-result-object v7

    invoke-virtual {v7}, Landroid/hardware/Camera$Parameters;->getZoom()I

    move-result v2

    iget-object v7, p0, Lcom/android/camera/manager/ZoomManager;->mZoomRatios:Ljava/util/List;

    invoke-interface {v7}, Ljava/util/List;->size()I

    move-result v3

    iget-object v7, p0, Lcom/android/camera/manager/ZoomManager;->mZoomRatios:Ljava/util/List;

    invoke-interface {v7, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Ljava/lang/Integer;

    invoke-virtual {v7}, Ljava/lang/Integer;->intValue()I

    move-result v0

    iget-object v7, p0, Lcom/android/camera/manager/ZoomManager;->mZoomRatios:Ljava/util/List;

    add-int/lit8 v9, v3, -0x1

    invoke-interface {v7, v9}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Ljava/lang/Integer;

    invoke-virtual {v7}, Ljava/lang/Integer;->intValue()I

    move-result v4

    iget-object v7, p0, Lcom/android/camera/manager/ZoomManager;->mZoomRatios:Ljava/util/List;

    invoke-interface {v7, v8}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Ljava/lang/Integer;

    invoke-virtual {v7}, Ljava/lang/Integer;->intValue()I

    move-result v5

    move v1, v2

    iget v7, p0, Lcom/android/camera/manager/ZoomManager;->mLastZoomRatio:I

    const/4 v9, -0x1

    if-eq v7, v9, :cond_0

    iget v7, p0, Lcom/android/camera/manager/ZoomManager;->mLastZoomRatio:I

    if-ne v7, v0, :cond_2

    :cond_0
    iput v0, p0, Lcom/android/camera/manager/ZoomManager;->mLastZoomRatio:I

    move v1, v2

    :goto_0
    iget-object v7, p0, Lcom/android/camera/manager/ZoomManager;->mZoomRatios:Ljava/util/List;

    invoke-interface {v7, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Ljava/lang/Integer;

    invoke-virtual {v7}, Ljava/lang/Integer;->intValue()I

    move-result v6

    iget v7, p0, Lcom/android/camera/manager/ZoomManager;->mLastZoomRatio:I

    if-eq v6, v7, :cond_3

    const/4 v7, 0x1

    :goto_1
    invoke-direct {p0, v1, v7}, Lcom/android/camera/manager/ZoomManager;->performZoom(IZ)V

    sget-boolean v7, Lcom/android/camera/manager/ZoomManager;->LOG:Z

    if-eqz v7, :cond_1

    const-string v7, "ZoomManager"

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "onCameraParameterReady() index="

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, ", len="

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, ", maxRatio="

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, ", minRatio="

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, ", curRatio="

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, ", finalIndex="

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, ", newRatio="

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, ", mSupportZoom="

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    iget-boolean v9, p0, Lcom/android/camera/manager/ZoomManager;->mDeviceSupport:Z

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, ", mLastZoomRatio="

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    iget v9, p0, Lcom/android/camera/manager/ZoomManager;->mLastZoomRatio:I

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8}, Lcom/android/camera/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    :cond_1
    :goto_2
    return-void

    :cond_2
    iget v7, p0, Lcom/android/camera/manager/ZoomManager;->mLastZoomRatio:I

    invoke-direct {p0, v7}, Lcom/android/camera/manager/ZoomManager;->findZoomIndex(I)I

    move-result v1

    goto/16 :goto_0

    :cond_3
    move v7, v8

    goto :goto_1

    :cond_4
    invoke-virtual {p0}, Lcom/android/camera/manager/ZoomManager;->resetZoom()V

    invoke-direct {p0, v8, v8}, Lcom/android/camera/manager/ZoomManager;->performZoom(IZ)V

    goto :goto_2
.end method
