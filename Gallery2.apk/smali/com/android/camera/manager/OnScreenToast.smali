.class public Lcom/android/camera/manager/OnScreenToast;
.super Ljava/lang/Object;
.source "OnScreenToast.java"

# interfaces
.implements Lcom/android/camera/Camera$OnOrientationListener;


# static fields
.field private static final LOG:Z

.field static final TAG:Ljava/lang/String; = "OnScreenToast"

.field private static final TOAST_DURATION:I = 0x7d0


# instance fields
.field private mContext:Landroid/content/Context;

.field private final mHandler:Landroid/os/Handler;

.field private final mHide:Ljava/lang/Runnable;

.field private mLayout:Landroid/widget/RelativeLayout;

.field private mNextView:Landroid/view/View;

.field private mOrientation:I

.field private final mParams:Landroid/view/WindowManager$LayoutParams;

.field private final mShow:Ljava/lang/Runnable;

.field private mText:Landroid/widget/TextView;

.field private mView:Landroid/view/View;

.field private final mWM:Landroid/view/WindowManager;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    sget-boolean v0, Lcom/android/camera/Log;->LOGV:Z

    sput-boolean v0, Lcom/android/camera/manager/OnScreenToast;->LOG:Z

    return-void
.end method

.method private constructor <init>(Landroid/content/Context;)V
    .locals 2
    .param p1    # Landroid/content/Context;

    const/4 v1, -0x1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Landroid/view/WindowManager$LayoutParams;

    invoke-direct {v0}, Landroid/view/WindowManager$LayoutParams;-><init>()V

    iput-object v0, p0, Lcom/android/camera/manager/OnScreenToast;->mParams:Landroid/view/WindowManager$LayoutParams;

    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    iput-object v0, p0, Lcom/android/camera/manager/OnScreenToast;->mHandler:Landroid/os/Handler;

    new-instance v0, Lcom/android/camera/manager/OnScreenToast$1;

    invoke-direct {v0, p0}, Lcom/android/camera/manager/OnScreenToast$1;-><init>(Lcom/android/camera/manager/OnScreenToast;)V

    iput-object v0, p0, Lcom/android/camera/manager/OnScreenToast;->mShow:Ljava/lang/Runnable;

    new-instance v0, Lcom/android/camera/manager/OnScreenToast$2;

    invoke-direct {v0, p0}, Lcom/android/camera/manager/OnScreenToast$2;-><init>(Lcom/android/camera/manager/OnScreenToast;)V

    iput-object v0, p0, Lcom/android/camera/manager/OnScreenToast;->mHide:Ljava/lang/Runnable;

    const-string v0, "window"

    invoke-virtual {p1, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/WindowManager;

    iput-object v0, p0, Lcom/android/camera/manager/OnScreenToast;->mWM:Landroid/view/WindowManager;

    iget-object v0, p0, Lcom/android/camera/manager/OnScreenToast;->mParams:Landroid/view/WindowManager$LayoutParams;

    iput v1, v0, Landroid/view/ViewGroup$LayoutParams;->height:I

    iget-object v0, p0, Lcom/android/camera/manager/OnScreenToast;->mParams:Landroid/view/WindowManager$LayoutParams;

    iput v1, v0, Landroid/view/ViewGroup$LayoutParams;->width:I

    iget-object v0, p0, Lcom/android/camera/manager/OnScreenToast;->mParams:Landroid/view/WindowManager$LayoutParams;

    const/16 v1, 0x18

    iput v1, v0, Landroid/view/WindowManager$LayoutParams;->flags:I

    iget-object v0, p0, Lcom/android/camera/manager/OnScreenToast;->mParams:Landroid/view/WindowManager$LayoutParams;

    const/4 v1, -0x3

    iput v1, v0, Landroid/view/WindowManager$LayoutParams;->format:I

    iget-object v0, p0, Lcom/android/camera/manager/OnScreenToast;->mParams:Landroid/view/WindowManager$LayoutParams;

    const/16 v1, 0x3e8

    iput v1, v0, Landroid/view/WindowManager$LayoutParams;->type:I

    iget-object v0, p0, Lcom/android/camera/manager/OnScreenToast;->mParams:Landroid/view/WindowManager$LayoutParams;

    const-string v1, "OnScreenHint"

    invoke-virtual {v0, v1}, Landroid/view/WindowManager$LayoutParams;->setTitle(Ljava/lang/CharSequence;)V

    iput-object p1, p0, Lcom/android/camera/manager/OnScreenToast;->mContext:Landroid/content/Context;

    return-void
.end method

.method static synthetic access$000(Lcom/android/camera/manager/OnScreenToast;)V
    .locals 0
    .param p0    # Lcom/android/camera/manager/OnScreenToast;

    invoke-direct {p0}, Lcom/android/camera/manager/OnScreenToast;->handleShow()V

    return-void
.end method

.method static synthetic access$100(Lcom/android/camera/manager/OnScreenToast;)Landroid/content/Context;
    .locals 1
    .param p0    # Lcom/android/camera/manager/OnScreenToast;

    iget-object v0, p0, Lcom/android/camera/manager/OnScreenToast;->mContext:Landroid/content/Context;

    return-object v0
.end method

.method static synthetic access$200(Lcom/android/camera/manager/OnScreenToast;)V
    .locals 0
    .param p0    # Lcom/android/camera/manager/OnScreenToast;

    invoke-direct {p0}, Lcom/android/camera/manager/OnScreenToast;->handleHide()V

    return-void
.end method

.method private declared-synchronized handleHide()V
    .locals 3

    monitor-enter p0

    :try_start_0
    iget-object v1, p0, Lcom/android/camera/manager/OnScreenToast;->mView:Landroid/view/View;

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/android/camera/manager/OnScreenToast;->mView:Landroid/view/View;

    invoke-static {v1}, Lcom/android/camera/Util;->fadeOut(Landroid/view/View;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :try_start_1
    iget-object v1, p0, Lcom/android/camera/manager/OnScreenToast;->mView:Landroid/view/View;

    invoke-virtual {v1}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    move-result-object v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/android/camera/manager/OnScreenToast;->mWM:Landroid/view/WindowManager;

    iget-object v2, p0, Lcom/android/camera/manager/OnScreenToast;->mView:Landroid/view/View;

    invoke-interface {v1, v2}, Landroid/view/WindowManager;->removeView(Landroid/view/View;)V
    :try_end_1
    .catch Landroid/view/WindowManager$BadTokenException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    :cond_0
    :goto_0
    const/4 v1, 0x0

    :try_start_2
    iput-object v1, p0, Lcom/android/camera/manager/OnScreenToast;->mView:Landroid/view/View;
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    :cond_1
    monitor-exit p0

    return-void

    :catch_0
    move-exception v0

    :try_start_3
    invoke-virtual {v0}, Ljava/lang/Throwable;->printStackTrace()V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v1

    monitor-exit p0

    throw v1
.end method

.method private declared-synchronized handleShow()V
    .locals 4

    monitor-enter p0

    :try_start_0
    iget-object v1, p0, Lcom/android/camera/manager/OnScreenToast;->mView:Landroid/view/View;

    iget-object v2, p0, Lcom/android/camera/manager/OnScreenToast;->mNextView:Landroid/view/View;

    if-eq v1, v2, :cond_1

    invoke-direct {p0}, Lcom/android/camera/manager/OnScreenToast;->handleHide()V

    iget-object v1, p0, Lcom/android/camera/manager/OnScreenToast;->mNextView:Landroid/view/View;

    iput-object v1, p0, Lcom/android/camera/manager/OnScreenToast;->mView:Landroid/view/View;

    iget-object v1, p0, Lcom/android/camera/manager/OnScreenToast;->mParams:Landroid/view/WindowManager$LayoutParams;

    const/4 v2, 0x0

    iput v2, v1, Landroid/view/WindowManager$LayoutParams;->x:I

    iget-object v1, p0, Lcom/android/camera/manager/OnScreenToast;->mParams:Landroid/view/WindowManager$LayoutParams;

    const/4 v2, 0x0

    iput v2, v1, Landroid/view/WindowManager$LayoutParams;->y:I

    iget-object v1, p0, Lcom/android/camera/manager/OnScreenToast;->mParams:Landroid/view/WindowManager$LayoutParams;

    const/4 v2, -0x1

    iput v2, v1, Landroid/view/ViewGroup$LayoutParams;->height:I

    iget-object v1, p0, Lcom/android/camera/manager/OnScreenToast;->mParams:Landroid/view/WindowManager$LayoutParams;

    const/4 v2, -0x1

    iput v2, v1, Landroid/view/ViewGroup$LayoutParams;->width:I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :try_start_1
    iget-object v1, p0, Lcom/android/camera/manager/OnScreenToast;->mView:Landroid/view/View;

    invoke-virtual {v1}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    move-result-object v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/android/camera/manager/OnScreenToast;->mWM:Landroid/view/WindowManager;

    iget-object v2, p0, Lcom/android/camera/manager/OnScreenToast;->mView:Landroid/view/View;

    invoke-interface {v1, v2}, Landroid/view/WindowManager;->removeView(Landroid/view/View;)V

    :cond_0
    iget-object v1, p0, Lcom/android/camera/manager/OnScreenToast;->mWM:Landroid/view/WindowManager;

    iget-object v2, p0, Lcom/android/camera/manager/OnScreenToast;->mView:Landroid/view/View;

    iget-object v3, p0, Lcom/android/camera/manager/OnScreenToast;->mParams:Landroid/view/WindowManager$LayoutParams;

    invoke-interface {v1, v2, v3}, Landroid/view/WindowManager;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V
    :try_end_1
    .catch Landroid/view/WindowManager$BadTokenException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    :goto_0
    :try_start_2
    iget-object v1, p0, Lcom/android/camera/manager/OnScreenToast;->mView:Landroid/view/View;

    invoke-static {v1}, Lcom/android/camera/Util;->fadeIn(Landroid/view/View;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    :cond_1
    monitor-exit p0

    return-void

    :catch_0
    move-exception v0

    :try_start_3
    invoke-virtual {v0}, Ljava/lang/Throwable;->printStackTrace()V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v1

    monitor-exit p0

    throw v1
.end method

.method private isLandcape()Z
    .locals 5

    iget-object v2, p0, Lcom/android/camera/manager/OnScreenToast;->mContext:Landroid/content/Context;

    check-cast v2, Lcom/android/camera/Camera;

    invoke-virtual {v2}, Lcom/android/camera/Camera;->getOrietation()I

    move-result v1

    const/16 v2, 0x5a

    if-eq v1, v2, :cond_0

    const/16 v2, 0x10e

    if-ne v1, v2, :cond_2

    :cond_0
    const/4 v0, 0x1

    :goto_0
    sget-boolean v2, Lcom/android/camera/manager/OnScreenToast;->LOG:Z

    if-eqz v2, :cond_1

    const-string v2, "OnScreenToast"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "isLandcape() orientation="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ", return "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/android/camera/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    :cond_1
    return v0

    :cond_2
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static makeText(Landroid/content/Context;Ljava/lang/CharSequence;)Lcom/android/camera/manager/OnScreenToast;
    .locals 6
    .param p0    # Landroid/content/Context;
    .param p1    # Ljava/lang/CharSequence;

    new-instance v1, Lcom/android/camera/manager/OnScreenToast;

    invoke-direct {v1, p0}, Lcom/android/camera/manager/OnScreenToast;-><init>(Landroid/content/Context;)V

    const-string v4, "layout_inflater"

    invoke-virtual {p0, v4}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/LayoutInflater;

    const v4, 0x7f04002b

    const/4 v5, 0x0

    invoke-virtual {v0, v4, v5}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v3

    const v4, 0x7f0b0084

    invoke-virtual {v3, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    invoke-virtual {v2, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iput-object v3, v1, Lcom/android/camera/manager/OnScreenToast;->mNextView:Landroid/view/View;

    const v4, 0x7f0b008f

    invoke-virtual {v3, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/RelativeLayout;

    iput-object v4, v1, Lcom/android/camera/manager/OnScreenToast;->mLayout:Landroid/widget/RelativeLayout;

    iput-object v2, v1, Lcom/android/camera/manager/OnScreenToast;->mText:Landroid/widget/TextView;

    return-object v1
.end method


# virtual methods
.method public cancel()V
    .locals 2

    iget-object v0, p0, Lcom/android/camera/manager/OnScreenToast;->mHandler:Landroid/os/Handler;

    iget-object v1, p0, Lcom/android/camera/manager/OnScreenToast;->mHide:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    return-void
.end method

.method public onOrientationChanged(I)V
    .locals 7
    .param p1    # I

    const/4 v6, -0x1

    const/4 v5, -0x2

    iget v2, p0, Lcom/android/camera/manager/OnScreenToast;->mOrientation:I

    if-eq v2, p1, :cond_0

    iput p1, p0, Lcom/android/camera/manager/OnScreenToast;->mOrientation:I

    iget-object v2, p0, Lcom/android/camera/manager/OnScreenToast;->mView:Landroid/view/View;

    iget v3, p0, Lcom/android/camera/manager/OnScreenToast;->mOrientation:I

    const/4 v4, 0x1

    invoke-static {v2, v3, v4}, Lcom/android/camera/Util;->setOrientation(Landroid/view/View;IZ)V

    :cond_0
    iget-object v2, p0, Lcom/android/camera/manager/OnScreenToast;->mView:Landroid/view/View;

    if-eqz v2, :cond_1

    invoke-direct {p0}, Lcom/android/camera/manager/OnScreenToast;->isLandcape()Z

    move-result v2

    if-eqz v2, :cond_2

    iget-object v2, p0, Lcom/android/camera/manager/OnScreenToast;->mLayout:Landroid/widget/RelativeLayout;

    invoke-virtual {v2}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v1

    iput v6, v1, Landroid/view/ViewGroup$LayoutParams;->height:I

    iput v5, v1, Landroid/view/ViewGroup$LayoutParams;->width:I

    iget-object v2, p0, Lcom/android/camera/manager/OnScreenToast;->mLayout:Landroid/widget/RelativeLayout;

    invoke-virtual {v2, v1}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    new-instance v0, Landroid/widget/RelativeLayout$LayoutParams;

    invoke-direct {v0, v5, v5}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    const/16 v2, 0xf

    invoke-virtual {v0, v2}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(I)V

    const/16 v2, 0x9

    invoke-virtual {v0, v2}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(I)V

    iget-object v2, p0, Lcom/android/camera/manager/OnScreenToast;->mText:Landroid/widget/TextView;

    invoke-virtual {v2, v0}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    :goto_0
    iget-object v2, p0, Lcom/android/camera/manager/OnScreenToast;->mView:Landroid/view/View;

    invoke-virtual {v2}, Landroid/view/View;->requestLayout()V

    :cond_1
    return-void

    :cond_2
    iget-object v2, p0, Lcom/android/camera/manager/OnScreenToast;->mLayout:Landroid/widget/RelativeLayout;

    invoke-virtual {v2}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v1

    iput v6, v1, Landroid/view/ViewGroup$LayoutParams;->width:I

    iput v5, v1, Landroid/view/ViewGroup$LayoutParams;->height:I

    iget-object v2, p0, Lcom/android/camera/manager/OnScreenToast;->mLayout:Landroid/widget/RelativeLayout;

    invoke-virtual {v2, v1}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    new-instance v0, Landroid/widget/RelativeLayout$LayoutParams;

    invoke-direct {v0, v5, v5}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    const/16 v2, 0xe

    invoke-virtual {v0, v2}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(I)V

    iget-object v2, p0, Lcom/android/camera/manager/OnScreenToast;->mText:Landroid/widget/TextView;

    invoke-virtual {v2, v0}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    goto :goto_0
.end method

.method public setText(Ljava/lang/CharSequence;)V
    .locals 3
    .param p1    # Ljava/lang/CharSequence;

    iget-object v1, p0, Lcom/android/camera/manager/OnScreenToast;->mNextView:Landroid/view/View;

    if-nez v1, :cond_0

    new-instance v1, Ljava/lang/RuntimeException;

    const-string v2, "This OnScreenHint was not created with OnScreenHint.makeText()"

    invoke-direct {v1, v2}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v1

    :cond_0
    iget-object v1, p0, Lcom/android/camera/manager/OnScreenToast;->mNextView:Landroid/view/View;

    const v2, 0x7f0b0084

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    if-nez v0, :cond_1

    new-instance v1, Ljava/lang/RuntimeException;

    const-string v2, "This OnScreenHint was not created with OnScreenHint.makeText()"

    invoke-direct {v1, v2}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v1

    :cond_1
    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    return-void
.end method

.method public show()V
    .locals 2

    iget-object v0, p0, Lcom/android/camera/manager/OnScreenToast;->mNextView:Landroid/view/View;

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/RuntimeException;

    const-string v1, "View is not initialized"

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    iget-object v0, p0, Lcom/android/camera/manager/OnScreenToast;->mHandler:Landroid/os/Handler;

    iget-object v1, p0, Lcom/android/camera/manager/OnScreenToast;->mShow:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    return-void
.end method

.method public showToast()V
    .locals 4

    iget-object v0, p0, Lcom/android/camera/manager/OnScreenToast;->mNextView:Landroid/view/View;

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/RuntimeException;

    const-string v1, "View is not initialized"

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    iget-object v0, p0, Lcom/android/camera/manager/OnScreenToast;->mHandler:Landroid/os/Handler;

    iget-object v1, p0, Lcom/android/camera/manager/OnScreenToast;->mShow:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    iget-object v0, p0, Lcom/android/camera/manager/OnScreenToast;->mHandler:Landroid/os/Handler;

    iget-object v1, p0, Lcom/android/camera/manager/OnScreenToast;->mHide:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    iget-object v0, p0, Lcom/android/camera/manager/OnScreenToast;->mHandler:Landroid/os/Handler;

    iget-object v1, p0, Lcom/android/camera/manager/OnScreenToast;->mShow:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    iget-object v0, p0, Lcom/android/camera/manager/OnScreenToast;->mHandler:Landroid/os/Handler;

    iget-object v1, p0, Lcom/android/camera/manager/OnScreenToast;->mHide:Ljava/lang/Runnable;

    const-wide/16 v2, 0x7d0

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    return-void
.end method
