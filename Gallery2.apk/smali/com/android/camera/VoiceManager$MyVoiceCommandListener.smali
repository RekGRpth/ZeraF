.class Lcom/android/camera/VoiceManager$MyVoiceCommandListener;
.super Lcom/mediatek/common/voicecommand/VoiceCommandListener;
.source "VoiceManager.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/camera/VoiceManager;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "MyVoiceCommandListener"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/camera/VoiceManager;


# direct methods
.method public constructor <init>(Lcom/android/camera/VoiceManager;Landroid/content/Context;)V
    .locals 0
    .param p2    # Landroid/content/Context;

    iput-object p1, p0, Lcom/android/camera/VoiceManager$MyVoiceCommandListener;->this$0:Lcom/android/camera/VoiceManager;

    invoke-direct {p0, p2}, Lcom/mediatek/common/voicecommand/VoiceCommandListener;-><init>(Landroid/content/Context;)V

    return-void
.end method

.method private printExtraData(Landroid/os/Bundle;)V
    .locals 6
    .param p1    # Landroid/os/Bundle;

    invoke-static {}, Lcom/android/camera/VoiceManager;->access$000()Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-virtual {p1}, Landroid/os/Bundle;->keySet()Ljava/util/Set;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    const-string v3, "VoiceManager"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "printExtraData() extraData["

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "]="

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {p1, v1}, Landroid/os/Bundle;->get(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/android/camera/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    :cond_0
    return-void
.end method


# virtual methods
.method public onVoiceCommandNotified(IILandroid/os/Bundle;)V
    .locals 8
    .param p1    # I
    .param p2    # I
    .param p3    # Landroid/os/Bundle;

    const/4 v7, 0x1

    const/4 v6, -0x1

    invoke-static {}, Lcom/android/camera/VoiceManager;->access$000()Z

    move-result v3

    if-eqz v3, :cond_0

    const-string v3, "VoiceManager"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "onVoiceCommandNotified("

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ", "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ", "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ")"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/android/camera/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    const/4 v2, -0x1

    packed-switch p1, :pswitch_data_0

    :cond_1
    :goto_0
    :pswitch_0
    return-void

    :pswitch_1
    packed-switch p2, :pswitch_data_1

    goto :goto_0

    :pswitch_2
    if-eqz p3, :cond_1

    invoke-direct {p0, p3}, Lcom/android/camera/VoiceManager$MyVoiceCommandListener;->printExtraData(Landroid/os/Bundle;)V

    const-string v3, "Result"

    invoke-virtual {p3, v3, v6}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v2

    if-ne v2, v7, :cond_1

    const-string v3, "Result_Info"

    invoke-virtual {p3, v3, v6}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v0

    iget-object v3, p0, Lcom/android/camera/VoiceManager$MyVoiceCommandListener;->this$0:Lcom/android/camera/VoiceManager;

    invoke-static {v3, v0}, Lcom/android/camera/VoiceManager;->access$100(Lcom/android/camera/VoiceManager;I)V

    goto :goto_0

    :pswitch_3
    packed-switch p2, :pswitch_data_2

    goto :goto_0

    :pswitch_4
    if-eqz p3, :cond_1

    invoke-direct {p0, p3}, Lcom/android/camera/VoiceManager$MyVoiceCommandListener;->printExtraData(Landroid/os/Bundle;)V

    const-string v3, "Result"

    invoke-virtual {p3, v3, v6}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v2

    if-ne v2, v7, :cond_1

    iget-object v3, p0, Lcom/android/camera/VoiceManager$MyVoiceCommandListener;->this$0:Lcom/android/camera/VoiceManager;

    const-string v4, "Result_Info"

    invoke-virtual {p3, v4}, Landroid/os/Bundle;->getStringArray(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/android/camera/VoiceManager;->access$202(Lcom/android/camera/VoiceManager;[Ljava/lang/String;)[Ljava/lang/String;

    iget-object v3, p0, Lcom/android/camera/VoiceManager$MyVoiceCommandListener;->this$0:Lcom/android/camera/VoiceManager;

    invoke-static {v3}, Lcom/android/camera/VoiceManager;->access$300(Lcom/android/camera/VoiceManager;)V

    goto :goto_0

    :pswitch_5
    if-eqz p3, :cond_1

    invoke-direct {p0, p3}, Lcom/android/camera/VoiceManager$MyVoiceCommandListener;->printExtraData(Landroid/os/Bundle;)V

    const-string v3, "Result"

    invoke-virtual {p3, v3, v6}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v2

    if-ne v2, v7, :cond_1

    const-string v3, "Result_Info"

    const/4 v4, 0x0

    invoke-virtual {p3, v3, v4}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v1

    iget-object v4, p0, Lcom/android/camera/VoiceManager$MyVoiceCommandListener;->this$0:Lcom/android/camera/VoiceManager;

    if-eqz v1, :cond_2

    const-string v3, "on"

    :goto_1
    invoke-static {v4, v3}, Lcom/android/camera/VoiceManager;->access$402(Lcom/android/camera/VoiceManager;Ljava/lang/String;)Ljava/lang/String;

    iget-object v3, p0, Lcom/android/camera/VoiceManager$MyVoiceCommandListener;->this$0:Lcom/android/camera/VoiceManager;

    invoke-static {v3}, Lcom/android/camera/VoiceManager;->access$500(Lcom/android/camera/VoiceManager;)V

    goto :goto_0

    :cond_2
    const-string v3, "off"

    goto :goto_1

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_3
        :pswitch_1
    .end packed-switch

    :pswitch_data_1
    .packed-switch 0x1
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_2
    .end packed-switch

    :pswitch_data_2
    .packed-switch 0x1
        :pswitch_4
        :pswitch_5
    .end packed-switch
.end method
