.class public Lcom/mediatek/mtklogger/MainActivity;
.super Landroid/app/Activity;
.source "MainActivity.java"


# static fields
.field private static final ALPHA_FULL:I = 0xff

.field private static final ALPHA_GRAY:I = 0x4b

.field private static final IS_LOG_START:I = 0x1

.field private static final IS_LOG_STOP:I = 0x0

.field private static final MESSAGE_DELAY_TIME:I = 0x190

.field private static final MSG_MONITOR_SDCARD_BAR:I = 0x3

.field private static final MSG_SET_AUTO_START:I = 0x4

.field private static final MSG_TIMER:I = 0x1

.field private static final MSG_WAITING_DIALOG_TIMER:I = 0x2

.field private static final SDCARD_RATIO_BAR_TIMER:I = 0x7530

.field private static final START_STOP_TIMER:I = 0xea60

.field private static final TAG:Ljava/lang/String; = "MTKLogger/MainActivity"

.field private static final TIMER_PERIOD:I = 0x3e8


# instance fields
.field private mClearLogImageButton:Landroid/widget/ImageButton;

.field private mDefaultSharedPreferences:Landroid/content/SharedPreferences;

.field private mFreeStorageSize:F

.field private mFreeStorageText:Landroid/widget/TextView;

.field private mLogImageViews:Landroid/util/SparseArray;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/util/SparseArray",
            "<",
            "Landroid/widget/ImageView;",
            ">;"
        }
    .end annotation
.end field

.field private mLogTextViews:Landroid/util/SparseArray;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/util/SparseArray",
            "<",
            "Landroid/widget/TextView;",
            ">;"
        }
    .end annotation
.end field

.field private mManager:Lcom/mediatek/mtklogger/framework/MTKLoggerManager;

.field private mMessageHandler:Landroid/os/Handler;

.field private mMobileLogImage:Landroid/widget/ImageView;

.field private mMobileLogText:Landroid/widget/TextView;

.field private mModemLogImage:Landroid/widget/ImageView;

.field private mModemLogText:Landroid/widget/TextView;

.field private mMonitorTimer:Ljava/util/Timer;

.field private mNetworkLogImage:Landroid/widget/ImageView;

.field private mNetworkLogText:Landroid/widget/TextView;

.field private mSDCardPathStr:Ljava/lang/String;

.field private mSavePathStr:Ljava/lang/String;

.field private mSavePathText:Landroid/widget/TextView;

.field private mSdcardRatioBar:Lcom/mediatek/mtklogger/LinearColorBar;

.field private mServiceReceiver:Landroid/content/BroadcastReceiver;

.field private mSettingsImageButton:Landroid/widget/ImageButton;

.field private mSettingsMenuItem:Landroid/view/MenuItem;

.field private mSharedPreferences:Landroid/content/SharedPreferences;

.field private mStartStopToggleButton:Landroid/widget/ToggleButton;

.field private mStorageChartLabelText:Landroid/widget/TextView;

.field private mStorageStatusReceiver:Landroid/content/BroadcastReceiver;

.field private mTagImageButton:Landroid/widget/ImageButton;

.field private mTimeMillisecond:J

.field private mTimeText:Landroid/widget/TextView;

.field private mTimer:Ljava/util/Timer;

.field private mUsedStorageSize:F

.field private mUsedStorageText:Landroid/widget/TextView;

.field private mWaitingDialog:Landroid/app/ProgressDialog;

.field private mWaitingDialogTimer:Ljava/util/Timer;


# direct methods
.method public constructor <init>()V
    .locals 2

    invoke-direct {p0}, Landroid/app/Activity;-><init>()V

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/mediatek/mtklogger/MainActivity;->mManager:Lcom/mediatek/mtklogger/framework/MTKLoggerManager;

    new-instance v0, Landroid/util/SparseArray;

    invoke-direct {v0}, Landroid/util/SparseArray;-><init>()V

    iput-object v0, p0, Lcom/mediatek/mtklogger/MainActivity;->mLogImageViews:Landroid/util/SparseArray;

    new-instance v0, Landroid/util/SparseArray;

    invoke-direct {v0}, Landroid/util/SparseArray;-><init>()V

    iput-object v0, p0, Lcom/mediatek/mtklogger/MainActivity;->mLogTextViews:Landroid/util/SparseArray;

    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/mediatek/mtklogger/MainActivity;->mTimeMillisecond:J

    new-instance v0, Lcom/mediatek/mtklogger/MainActivity$1;

    invoke-direct {v0, p0}, Lcom/mediatek/mtklogger/MainActivity$1;-><init>(Lcom/mediatek/mtklogger/MainActivity;)V

    iput-object v0, p0, Lcom/mediatek/mtklogger/MainActivity;->mServiceReceiver:Landroid/content/BroadcastReceiver;

    new-instance v0, Lcom/mediatek/mtklogger/MainActivity$2;

    invoke-direct {v0, p0}, Lcom/mediatek/mtklogger/MainActivity$2;-><init>(Lcom/mediatek/mtklogger/MainActivity;)V

    iput-object v0, p0, Lcom/mediatek/mtklogger/MainActivity;->mStorageStatusReceiver:Landroid/content/BroadcastReceiver;

    new-instance v0, Lcom/mediatek/mtklogger/MainActivity$4;

    invoke-direct {v0, p0}, Lcom/mediatek/mtklogger/MainActivity$4;-><init>(Lcom/mediatek/mtklogger/MainActivity;)V

    iput-object v0, p0, Lcom/mediatek/mtklogger/MainActivity;->mMessageHandler:Landroid/os/Handler;

    return-void
.end method

.method static synthetic access$000(Lcom/mediatek/mtklogger/MainActivity;)V
    .locals 0
    .param p0    # Lcom/mediatek/mtklogger/MainActivity;

    invoke-direct {p0}, Lcom/mediatek/mtklogger/MainActivity;->stopWaitingDialog()V

    return-void
.end method

.method static synthetic access$100(Lcom/mediatek/mtklogger/MainActivity;Ljava/lang/String;)Ljava/lang/String;
    .locals 1
    .param p0    # Lcom/mediatek/mtklogger/MainActivity;
    .param p1    # Ljava/lang/String;

    invoke-direct {p0, p1}, Lcom/mediatek/mtklogger/MainActivity;->analyzeReason(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$1000(Lcom/mediatek/mtklogger/MainActivity;)Landroid/content/SharedPreferences;
    .locals 1
    .param p0    # Lcom/mediatek/mtklogger/MainActivity;

    iget-object v0, p0, Lcom/mediatek/mtklogger/MainActivity;->mDefaultSharedPreferences:Landroid/content/SharedPreferences;

    return-object v0
.end method

.method static synthetic access$1100(Lcom/mediatek/mtklogger/MainActivity;)Lcom/mediatek/mtklogger/framework/MTKLoggerManager;
    .locals 1
    .param p0    # Lcom/mediatek/mtklogger/MainActivity;

    iget-object v0, p0, Lcom/mediatek/mtklogger/MainActivity;->mManager:Lcom/mediatek/mtklogger/framework/MTKLoggerManager;

    return-object v0
.end method

.method static synthetic access$1200(Lcom/mediatek/mtklogger/MainActivity;)Landroid/content/SharedPreferences;
    .locals 1
    .param p0    # Lcom/mediatek/mtklogger/MainActivity;

    iget-object v0, p0, Lcom/mediatek/mtklogger/MainActivity;->mSharedPreferences:Landroid/content/SharedPreferences;

    return-object v0
.end method

.method static synthetic access$1300(Lcom/mediatek/mtklogger/MainActivity;)V
    .locals 0
    .param p0    # Lcom/mediatek/mtklogger/MainActivity;

    invoke-direct {p0}, Lcom/mediatek/mtklogger/MainActivity;->clearLogs()V

    return-void
.end method

.method static synthetic access$1400(Lcom/mediatek/mtklogger/MainActivity;)V
    .locals 0
    .param p0    # Lcom/mediatek/mtklogger/MainActivity;

    invoke-direct {p0}, Lcom/mediatek/mtklogger/MainActivity;->tagLogs()V

    return-void
.end method

.method static synthetic access$200(Lcom/mediatek/mtklogger/MainActivity;)V
    .locals 0
    .param p0    # Lcom/mediatek/mtklogger/MainActivity;

    invoke-direct {p0}, Lcom/mediatek/mtklogger/MainActivity;->updateUI()V

    return-void
.end method

.method static synthetic access$300(Lcom/mediatek/mtklogger/MainActivity;)V
    .locals 0
    .param p0    # Lcom/mediatek/mtklogger/MainActivity;

    invoke-direct {p0}, Lcom/mediatek/mtklogger/MainActivity;->changeWaitingDialog()V

    return-void
.end method

.method static synthetic access$400(Lcom/mediatek/mtklogger/MainActivity;)Landroid/os/Handler;
    .locals 1
    .param p0    # Lcom/mediatek/mtklogger/MainActivity;

    iget-object v0, p0, Lcom/mediatek/mtklogger/MainActivity;->mMessageHandler:Landroid/os/Handler;

    return-object v0
.end method

.method static synthetic access$514(Lcom/mediatek/mtklogger/MainActivity;J)J
    .locals 2
    .param p0    # Lcom/mediatek/mtklogger/MainActivity;
    .param p1    # J

    iget-wide v0, p0, Lcom/mediatek/mtklogger/MainActivity;->mTimeMillisecond:J

    add-long/2addr v0, p1

    iput-wide v0, p0, Lcom/mediatek/mtklogger/MainActivity;->mTimeMillisecond:J

    return-wide v0
.end method

.method static synthetic access$600(Lcom/mediatek/mtklogger/MainActivity;)Ljava/lang/String;
    .locals 1
    .param p0    # Lcom/mediatek/mtklogger/MainActivity;

    invoke-direct {p0}, Lcom/mediatek/mtklogger/MainActivity;->calculateTimer()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$700(Lcom/mediatek/mtklogger/MainActivity;)Landroid/widget/TextView;
    .locals 1
    .param p0    # Lcom/mediatek/mtklogger/MainActivity;

    iget-object v0, p0, Lcom/mediatek/mtklogger/MainActivity;->mTimeText:Landroid/widget/TextView;

    return-object v0
.end method

.method static synthetic access$800(Lcom/mediatek/mtklogger/MainActivity;)V
    .locals 0
    .param p0    # Lcom/mediatek/mtklogger/MainActivity;

    invoke-direct {p0}, Lcom/mediatek/mtklogger/MainActivity;->refreshSdcardBar()V

    return-void
.end method

.method static synthetic access$900(Lcom/mediatek/mtklogger/MainActivity;Z)V
    .locals 0
    .param p0    # Lcom/mediatek/mtklogger/MainActivity;
    .param p1    # Z

    invoke-direct {p0, p1}, Lcom/mediatek/mtklogger/MainActivity;->updateLogAutoStart(Z)V

    return-void
.end method

.method private alertLowStorageDialog()V
    .locals 5

    new-instance v0, Landroid/app/AlertDialog$Builder;

    invoke-direct {v0, p0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    const v1, 0x7f07001d

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    const v1, 0x7f07001e

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    const/16 v4, 0x1e

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-virtual {p0, v1, v2}, Lcom/mediatek/mtklogger/MainActivity;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setMessage(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    const v1, 0x104000a

    new-instance v2, Lcom/mediatek/mtklogger/MainActivity$5;

    invoke-direct {v2, p0}, Lcom/mediatek/mtklogger/MainActivity$5;-><init>(Lcom/mediatek/mtklogger/MainActivity;)V

    invoke-virtual {v0, v1, v2}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->show()Landroid/app/AlertDialog;

    return-void
.end method

.method private analyzeReason(Ljava/lang/String;)Ljava/lang/String;
    .locals 10
    .param p1    # Ljava/lang/String;

    const/4 v9, -0x1

    const-string v6, ""

    const/4 v1, 0x0

    :goto_0
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v7

    if-ge v1, v7, :cond_0

    const-string v7, ":"

    invoke-virtual {p1, v7, v1}, Ljava/lang/String;->indexOf(Ljava/lang/String;I)I

    move-result v2

    if-ne v2, v9, :cond_1

    :cond_0
    :goto_1
    return-object v6

    :cond_1
    const/4 v3, -0x1

    :try_start_0
    invoke-virtual {p1, v1, v2}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v7

    invoke-static {v7}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v3

    :goto_2
    sget-object v7, Lcom/mediatek/mtklogger/utils/Utils;->LOG_TYPE_SET:Ljava/util/Set;

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    invoke-interface {v7, v8}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_0

    add-int/lit8 v1, v2, 0x1

    const-string v7, ";"

    invoke-virtual {p1, v7, v1}, Ljava/lang/String;->indexOf(Ljava/lang/String;I)I

    move-result v2

    if-eq v2, v9, :cond_0

    invoke-virtual {p1, v1, v2}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v5

    const/4 v4, 0x0

    :try_start_1
    sget-object v7, Lcom/mediatek/mtklogger/utils/Utils;->FAIL_REASON_DETAIL_MAP:Ljava/util/Map;

    invoke-interface {v7, v5}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Ljava/lang/Integer;

    invoke-virtual {v7}, Ljava/lang/Integer;->intValue()I
    :try_end_1
    .catch Ljava/lang/NullPointerException; {:try_start_1 .. :try_end_1} :catch_1

    move-result v4

    :goto_3
    if-ne v4, v9, :cond_2

    add-int/lit8 v1, v2, 0x1

    goto :goto_1

    :catch_0
    move-exception v0

    const/4 v3, -0x1

    goto :goto_2

    :catch_1
    move-exception v0

    const/4 v4, -0x1

    goto :goto_3

    :cond_2
    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v7, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    sget-object v7, Lcom/mediatek/mtklogger/utils/Utils;->LOG_NAME_MAP:Landroid/util/SparseArray;

    invoke-virtual {v7, v3}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Ljava/lang/Integer;

    invoke-virtual {v7}, Ljava/lang/Integer;->intValue()I

    move-result v7

    invoke-virtual {p0, v7}, Lcom/mediatek/mtklogger/MainActivity;->getString(I)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v8, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, " : "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {p0, v4}, Lcom/mediatek/mtklogger/MainActivity;->getString(I)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, "\n"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    add-int/lit8 v1, v2, 0x1

    goto/16 :goto_0
.end method

.method private calculateTimer()Ljava/lang/String;
    .locals 10

    const/16 v9, 0xa

    iget-wide v5, p0, Lcom/mediatek/mtklogger/MainActivity;->mTimeMillisecond:J

    long-to-int v5, v5

    div-int/lit16 v0, v5, 0xe10

    const/16 v5, 0x270f

    if-le v0, v5, :cond_0

    const-string v5, "MTKLogger/MainActivity"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "There is something wrong with time record! The hour is "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Lcom/mediatek/mtklogger/utils/Utils;->loge(Ljava/lang/String;Ljava/lang/String;)V

    const-wide/16 v5, 0x0

    iput-wide v5, p0, Lcom/mediatek/mtklogger/MainActivity;->mTimeMillisecond:J

    const-string v5, "MTKLogger/MainActivity"

    const-string v6, "There is something wrong with time record!"

    invoke-static {v5, v6}, Lcom/mediatek/mtklogger/utils/Utils;->loge(Ljava/lang/String;Ljava/lang/String;)V

    :cond_0
    if-lt v0, v9, :cond_1

    const/16 v5, 0x63

    if-gt v0, v5, :cond_1

    iget-object v5, p0, Lcom/mediatek/mtklogger/MainActivity;->mTimeText:Landroid/widget/TextView;

    const/high16 v6, 0x428c0000

    invoke-virtual {v5, v6}, Landroid/widget/TextView;->setTextSize(F)V

    :goto_0
    iget-wide v5, p0, Lcom/mediatek/mtklogger/MainActivity;->mTimeMillisecond:J

    long-to-int v5, v5

    div-int/lit8 v5, v5, 0x3c

    rem-int/lit8 v1, v5, 0x3c

    iget-wide v5, p0, Lcom/mediatek/mtklogger/MainActivity;->mTimeMillisecond:J

    const-wide/16 v7, 0x3c

    rem-long v2, v5, v7

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, ""

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, ":"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    if-ge v1, v9, :cond_4

    const-string v5, "0"

    :goto_1
    invoke-virtual {v6, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, ":"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-wide/16 v7, 0xa

    cmp-long v5, v2, v7

    if-gez v5, :cond_5

    const-string v5, "0"

    :goto_2
    invoke-virtual {v6, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    return-object v4

    :cond_1
    const/16 v5, 0x64

    if-lt v0, v5, :cond_2

    const/16 v5, 0x3e7

    if-gt v0, v5, :cond_2

    iget-object v5, p0, Lcom/mediatek/mtklogger/MainActivity;->mTimeText:Landroid/widget/TextView;

    const/high16 v6, 0x42700000

    invoke-virtual {v5, v6}, Landroid/widget/TextView;->setTextSize(F)V

    goto :goto_0

    :cond_2
    const/16 v5, 0x3e8

    if-lt v0, v5, :cond_3

    iget-object v5, p0, Lcom/mediatek/mtklogger/MainActivity;->mTimeText:Landroid/widget/TextView;

    const/high16 v6, 0x42480000

    invoke-virtual {v5, v6}, Landroid/widget/TextView;->setTextSize(F)V

    goto :goto_0

    :cond_3
    iget-object v5, p0, Lcom/mediatek/mtklogger/MainActivity;->mTimeText:Landroid/widget/TextView;

    const/high16 v6, 0x42a00000

    invoke-virtual {v5, v6}, Landroid/widget/TextView;->setTextSize(F)V

    goto :goto_0

    :cond_4
    const-string v5, ""

    goto :goto_1

    :cond_5
    const-string v5, ""

    goto :goto_2
.end method

.method private changeWaitingDialog()V
    .locals 9

    const-wide/32 v2, 0xea60

    const/4 v5, 0x1

    iget-object v0, p0, Lcom/mediatek/mtklogger/MainActivity;->mManager:Lcom/mediatek/mtklogger/framework/MTKLoggerManager;

    invoke-virtual {v0}, Lcom/mediatek/mtklogger/framework/MTKLoggerManager;->getCurrentRunningStage()I

    move-result v6

    const-string v0, "MTKLogger/MainActivity"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "changeWaitingDialog() -> currentRunningStage is "

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/mediatek/mtklogger/utils/Utils;->logd(Ljava/lang/String;Ljava/lang/String;)V

    if-nez v6, :cond_1

    invoke-direct {p0}, Lcom/mediatek/mtklogger/MainActivity;->stopWaitingDialog()V

    :cond_0
    :goto_0
    return-void

    :cond_1
    const-string v8, ""

    const-string v7, ""

    if-ne v6, v5, :cond_4

    const v0, 0x7f07001f

    invoke-virtual {p0, v0}, Lcom/mediatek/mtklogger/MainActivity;->getString(I)Ljava/lang/String;

    move-result-object v8

    const v0, 0x7f070020

    invoke-virtual {p0, v0}, Lcom/mediatek/mtklogger/MainActivity;->getString(I)Ljava/lang/String;

    move-result-object v7

    :cond_2
    :goto_1
    iget-object v0, p0, Lcom/mediatek/mtklogger/MainActivity;->mWaitingDialog:Landroid/app/ProgressDialog;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    invoke-static {p0, v8, v7, v5, v0}, Landroid/app/ProgressDialog;->show(Landroid/content/Context;Ljava/lang/CharSequence;Ljava/lang/CharSequence;ZZ)Landroid/app/ProgressDialog;

    move-result-object v0

    iput-object v0, p0, Lcom/mediatek/mtklogger/MainActivity;->mWaitingDialog:Landroid/app/ProgressDialog;

    iget-object v0, p0, Lcom/mediatek/mtklogger/MainActivity;->mWaitingDialogTimer:Ljava/util/Timer;

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/mediatek/mtklogger/MainActivity;->mWaitingDialogTimer:Ljava/util/Timer;

    invoke-virtual {v0}, Ljava/util/Timer;->cancel()V

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/mediatek/mtklogger/MainActivity;->mWaitingDialogTimer:Ljava/util/Timer;

    :cond_3
    new-instance v0, Ljava/util/Timer;

    invoke-direct {v0, v5}, Ljava/util/Timer;-><init>(Z)V

    iput-object v0, p0, Lcom/mediatek/mtklogger/MainActivity;->mWaitingDialogTimer:Ljava/util/Timer;

    iget-object v0, p0, Lcom/mediatek/mtklogger/MainActivity;->mWaitingDialogTimer:Ljava/util/Timer;

    new-instance v1, Lcom/mediatek/mtklogger/MainActivity$3;

    invoke-direct {v1, p0}, Lcom/mediatek/mtklogger/MainActivity$3;-><init>(Lcom/mediatek/mtklogger/MainActivity;)V

    move-wide v4, v2

    invoke-virtual/range {v0 .. v5}, Ljava/util/Timer;->schedule(Ljava/util/TimerTask;JJ)V

    goto :goto_0

    :cond_4
    const/4 v0, 0x2

    if-ne v6, v0, :cond_5

    const v0, 0x7f070021

    invoke-virtual {p0, v0}, Lcom/mediatek/mtklogger/MainActivity;->getString(I)Ljava/lang/String;

    move-result-object v8

    const v0, 0x7f070022

    invoke-virtual {p0, v0}, Lcom/mediatek/mtklogger/MainActivity;->getString(I)Ljava/lang/String;

    move-result-object v7

    goto :goto_1

    :cond_5
    const/4 v0, 0x3

    if-ne v6, v0, :cond_2

    const v0, 0x7f070023

    invoke-virtual {p0, v0}, Lcom/mediatek/mtklogger/MainActivity;->getString(I)Ljava/lang/String;

    move-result-object v8

    const v0, 0x7f070024

    invoke-virtual {p0, v0}, Lcom/mediatek/mtklogger/MainActivity;->getString(I)Ljava/lang/String;

    move-result-object v7

    goto :goto_1
.end method

.method private checkPath()Z
    .locals 5

    const/4 v4, 0x0

    const/4 v0, 0x1

    new-instance v1, Ljava/io/File;

    iget-object v2, p0, Lcom/mediatek/mtklogger/MainActivity;->mSDCardPathStr:Ljava/lang/String;

    invoke-direct {v1, v2}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1}, Ljava/io/File;->exists()Z

    move-result v1

    if-nez v1, :cond_0

    const v1, 0x7f07001c

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    iget-object v3, p0, Lcom/mediatek/mtklogger/MainActivity;->mSDCardPathStr:Ljava/lang/String;

    aput-object v3, v2, v4

    invoke-virtual {p0, v1, v2}, Lcom/mediatek/mtklogger/MainActivity;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-static {p0, v1, v4}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v1

    invoke-virtual {v1}, Landroid/widget/Toast;->show()V

    const/4 v0, 0x0

    :cond_0
    return v0
.end method

.method private checkTaglogCompressing()V
    .locals 4

    iget-object v1, p0, Lcom/mediatek/mtklogger/MainActivity;->mSharedPreferences:Landroid/content/SharedPreferences;

    const-string v2, "tag_log_compressing"

    const/4 v3, 0x0

    invoke-interface {v1, v2, v3}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v1

    if-eqz v1, :cond_0

    const-string v1, "MTKLogger/MainActivity"

    const-string v2, "checkTaglogCompressing() ? true"

    invoke-static {v1, v2}, Lcom/mediatek/mtklogger/utils/Utils;->logd(Ljava/lang/String;Ljava/lang/String;)V

    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    const-string v1, "com.mediatek.log2server.EXCEPTION_HAPPEND"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    const-string v1, "extra_key_from_main_activity"

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    invoke-virtual {p0, v0}, Lcom/mediatek/mtklogger/MainActivity;->sendBroadcast(Landroid/content/Intent;)V

    :cond_0
    return-void
.end method

.method private clearLogs()V
    .locals 15

    const/4 v14, 0x2

    const/4 v13, 0x3

    const/4 v9, 0x0

    const/4 v10, 0x1

    invoke-direct {p0}, Lcom/mediatek/mtklogger/MainActivity;->checkPath()Z

    move-result v8

    if-nez v8, :cond_0

    :goto_0
    return-void

    :cond_0
    new-instance v2, Landroid/content/Intent;

    const-class v8, Lcom/mediatek/mtklogger/LogFolderListActivity;

    invoke-direct {v2, p0, v8}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const-string v8, "rootpath"

    iget-object v11, p0, Lcom/mediatek/mtklogger/MainActivity;->mSavePathStr:Ljava/lang/String;

    invoke-virtual {v2, v8, v11}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    iget-object v8, p0, Lcom/mediatek/mtklogger/MainActivity;->mStartStopToggleButton:Landroid/widget/ToggleButton;

    invoke-virtual {v8}, Landroid/widget/ToggleButton;->isChecked()Z

    move-result v8

    if-eqz v8, :cond_9

    sget-object v8, Lcom/mediatek/mtklogger/utils/Utils;->LOG_TYPE_SET:Ljava/util/Set;

    invoke-interface {v8}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_1
    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v8

    if-eqz v8, :cond_9

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Ljava/lang/Integer;

    iget-object v11, p0, Lcom/mediatek/mtklogger/MainActivity;->mSharedPreferences:Landroid/content/SharedPreferences;

    sget-object v8, Lcom/mediatek/mtklogger/utils/Utils;->KEY_STATUS_MAP:Landroid/util/SparseArray;

    invoke-virtual {v7}, Ljava/lang/Integer;->intValue()I

    move-result v12

    invoke-virtual {v8, v12}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Ljava/lang/String;

    invoke-interface {v11, v8, v9}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v8

    if-ne v10, v8, :cond_5

    move v3, v10

    :goto_2
    if-eqz v3, :cond_1

    invoke-virtual {v7}, Ljava/lang/Integer;->intValue()I

    move-result v8

    if-ne v8, v14, :cond_8

    iget-object v8, p0, Lcom/mediatek/mtklogger/MainActivity;->mManager:Lcom/mediatek/mtklogger/framework/MTKLoggerManager;

    invoke-virtual {v8}, Lcom/mediatek/mtklogger/framework/MTKLoggerManager;->getModemLogRunningDetailStatus()I

    move-result v0

    const-string v8, "MTKLogger/MainActivity"

    new-instance v11, Ljava/lang/StringBuilder;

    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    const-string v12, "modemLogRunningDetailStatus : "

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-static {v8, v11}, Lcom/mediatek/mtklogger/utils/Utils;->logd(Ljava/lang/String;Ljava/lang/String;)V

    if-eq v0, v10, :cond_2

    if-ne v0, v13, :cond_6

    :cond_2
    move v4, v10

    :goto_3
    if-eqz v4, :cond_3

    const-string v8, "MTKLogger/MainActivity"

    const-string v11, "Modem one is running!"

    invoke-static {v8, v11}, Lcom/mediatek/mtklogger/utils/Utils;->logd(Ljava/lang/String;Ljava/lang/String;)V

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v11, p0, Lcom/mediatek/mtklogger/MainActivity;->mSavePathStr:Ljava/lang/String;

    invoke-virtual {v8, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    sget-object v8, Lcom/mediatek/mtklogger/utils/Utils;->LOG_PATH_MAP:Landroid/util/SparseArray;

    invoke-virtual {v7}, Ljava/lang/Integer;->intValue()I

    move-result v12

    invoke-virtual {v8, v12}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Ljava/lang/String;

    invoke-virtual {v11, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v8}, Lcom/mediatek/mtklogger/taglog/TagLogUtils;->getCurrentLogFolderFromPath(Ljava/lang/String;)Ljava/io/File;

    move-result-object v6

    if-eqz v6, :cond_3

    invoke-virtual {v6}, Ljava/io/File;->exists()Z

    move-result v8

    if-eqz v8, :cond_3

    sget-object v8, Lcom/mediatek/mtklogger/LogFolderListActivity;->EXTRA_FILTER_FILES_KEY:Landroid/util/SparseArray;

    invoke-virtual {v7}, Ljava/lang/Integer;->intValue()I

    move-result v11

    invoke-virtual {v8, v11}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Ljava/lang/String;

    invoke-virtual {v6}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v2, v8, v11}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    :cond_3
    if-eq v0, v14, :cond_4

    if-ne v0, v13, :cond_7

    :cond_4
    move v5, v10

    :goto_4
    if-eqz v5, :cond_1

    const-string v8, "MTKLogger/MainActivity"

    const-string v11, "DualModem is running!"

    invoke-static {v8, v11}, Lcom/mediatek/mtklogger/utils/Utils;->logd(Ljava/lang/String;Ljava/lang/String;)V

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v11, p0, Lcom/mediatek/mtklogger/MainActivity;->mSavePathStr:Ljava/lang/String;

    invoke-virtual {v8, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v11, "dualmdlog"

    invoke-virtual {v8, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v8}, Lcom/mediatek/mtklogger/taglog/TagLogUtils;->getCurrentLogFolderFromPath(Ljava/lang/String;)Ljava/io/File;

    move-result-object v6

    if-eqz v6, :cond_1

    invoke-virtual {v6}, Ljava/io/File;->exists()Z

    move-result v8

    if-eqz v8, :cond_1

    const-string v8, "filterDualModemFile"

    invoke-virtual {v6}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v2, v8, v11}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    goto/16 :goto_1

    :cond_5
    move v3, v9

    goto/16 :goto_2

    :cond_6
    move v4, v9

    goto/16 :goto_3

    :cond_7
    move v5, v9

    goto :goto_4

    :cond_8
    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v11, p0, Lcom/mediatek/mtklogger/MainActivity;->mSavePathStr:Ljava/lang/String;

    invoke-virtual {v8, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    sget-object v8, Lcom/mediatek/mtklogger/utils/Utils;->LOG_PATH_MAP:Landroid/util/SparseArray;

    invoke-virtual {v7}, Ljava/lang/Integer;->intValue()I

    move-result v12

    invoke-virtual {v8, v12}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Ljava/lang/String;

    invoke-virtual {v11, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v8}, Lcom/mediatek/mtklogger/taglog/TagLogUtils;->getCurrentLogFolderFromPath(Ljava/lang/String;)Ljava/io/File;

    move-result-object v6

    if-eqz v6, :cond_1

    invoke-virtual {v6}, Ljava/io/File;->exists()Z

    move-result v8

    if-eqz v8, :cond_1

    sget-object v8, Lcom/mediatek/mtklogger/LogFolderListActivity;->EXTRA_FILTER_FILES_KEY:Landroid/util/SparseArray;

    invoke-virtual {v7}, Ljava/lang/Integer;->intValue()I

    move-result v11

    invoke-virtual {v8, v11}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Ljava/lang/String;

    invoke-virtual {v6}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v2, v8, v11}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    goto/16 :goto_1

    :cond_9
    invoke-virtual {p0, v2}, Lcom/mediatek/mtklogger/MainActivity;->startActivity(Landroid/content/Intent;)V

    iget-object v8, p0, Lcom/mediatek/mtklogger/MainActivity;->mMessageHandler:Landroid/os/Handler;

    invoke-virtual {v8, v13}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    goto/16 :goto_0
.end method

.method private findViews()V
    .locals 5

    const/4 v4, 0x4

    const/4 v3, 0x2

    const/4 v2, 0x1

    const v0, 0x7f090021

    invoke-virtual {p0, v0}, Lcom/mediatek/mtklogger/MainActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageButton;

    iput-object v0, p0, Lcom/mediatek/mtklogger/MainActivity;->mSettingsImageButton:Landroid/widget/ImageButton;

    const v0, 0x7f090026

    invoke-virtual {p0, v0}, Lcom/mediatek/mtklogger/MainActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/mediatek/mtklogger/MainActivity;->mModemLogImage:Landroid/widget/ImageView;

    const v0, 0x7f090027

    invoke-virtual {p0, v0}, Lcom/mediatek/mtklogger/MainActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/mediatek/mtklogger/MainActivity;->mModemLogText:Landroid/widget/TextView;

    iget-object v0, p0, Lcom/mediatek/mtklogger/MainActivity;->mLogImageViews:Landroid/util/SparseArray;

    iget-object v1, p0, Lcom/mediatek/mtklogger/MainActivity;->mModemLogImage:Landroid/widget/ImageView;

    invoke-virtual {v0, v3, v1}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    iget-object v0, p0, Lcom/mediatek/mtklogger/MainActivity;->mLogTextViews:Landroid/util/SparseArray;

    iget-object v1, p0, Lcom/mediatek/mtklogger/MainActivity;->mModemLogText:Landroid/widget/TextView;

    invoke-virtual {v0, v3, v1}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    const v0, 0x7f090024

    invoke-virtual {p0, v0}, Lcom/mediatek/mtklogger/MainActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/mediatek/mtklogger/MainActivity;->mMobileLogImage:Landroid/widget/ImageView;

    const v0, 0x7f090025

    invoke-virtual {p0, v0}, Lcom/mediatek/mtklogger/MainActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/mediatek/mtklogger/MainActivity;->mMobileLogText:Landroid/widget/TextView;

    iget-object v0, p0, Lcom/mediatek/mtklogger/MainActivity;->mLogImageViews:Landroid/util/SparseArray;

    iget-object v1, p0, Lcom/mediatek/mtklogger/MainActivity;->mMobileLogImage:Landroid/widget/ImageView;

    invoke-virtual {v0, v2, v1}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    iget-object v0, p0, Lcom/mediatek/mtklogger/MainActivity;->mLogTextViews:Landroid/util/SparseArray;

    iget-object v1, p0, Lcom/mediatek/mtklogger/MainActivity;->mMobileLogText:Landroid/widget/TextView;

    invoke-virtual {v0, v2, v1}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    const v0, 0x7f090028

    invoke-virtual {p0, v0}, Lcom/mediatek/mtklogger/MainActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/mediatek/mtklogger/MainActivity;->mNetworkLogImage:Landroid/widget/ImageView;

    const v0, 0x7f090029

    invoke-virtual {p0, v0}, Lcom/mediatek/mtklogger/MainActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/mediatek/mtklogger/MainActivity;->mNetworkLogText:Landroid/widget/TextView;

    iget-object v0, p0, Lcom/mediatek/mtklogger/MainActivity;->mLogImageViews:Landroid/util/SparseArray;

    iget-object v1, p0, Lcom/mediatek/mtklogger/MainActivity;->mNetworkLogImage:Landroid/widget/ImageView;

    invoke-virtual {v0, v4, v1}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    iget-object v0, p0, Lcom/mediatek/mtklogger/MainActivity;->mLogTextViews:Landroid/util/SparseArray;

    iget-object v1, p0, Lcom/mediatek/mtklogger/MainActivity;->mNetworkLogText:Landroid/widget/TextView;

    invoke-virtual {v0, v4, v1}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    const v0, 0x7f090023

    invoke-virtual {p0, v0}, Lcom/mediatek/mtklogger/MainActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/mediatek/mtklogger/MainActivity;->mTimeText:Landroid/widget/TextView;

    const v0, 0x7f09002a

    invoke-virtual {p0, v0}, Lcom/mediatek/mtklogger/MainActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/mediatek/mtklogger/MainActivity;->mSavePathText:Landroid/widget/TextView;

    const v0, 0x7f09002c

    invoke-virtual {p0, v0}, Lcom/mediatek/mtklogger/MainActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/mediatek/mtklogger/LinearColorBar;

    iput-object v0, p0, Lcom/mediatek/mtklogger/MainActivity;->mSdcardRatioBar:Lcom/mediatek/mtklogger/LinearColorBar;

    const v0, 0x7f09002e

    invoke-virtual {p0, v0}, Lcom/mediatek/mtklogger/MainActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/mediatek/mtklogger/MainActivity;->mStorageChartLabelText:Landroid/widget/TextView;

    const v0, 0x7f09002d

    invoke-virtual {p0, v0}, Lcom/mediatek/mtklogger/MainActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/mediatek/mtklogger/MainActivity;->mUsedStorageText:Landroid/widget/TextView;

    const v0, 0x7f09002f

    invoke-virtual {p0, v0}, Lcom/mediatek/mtklogger/MainActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/mediatek/mtklogger/MainActivity;->mFreeStorageText:Landroid/widget/TextView;

    const v0, 0x7f090031

    invoke-virtual {p0, v0}, Lcom/mediatek/mtklogger/MainActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageButton;

    iput-object v0, p0, Lcom/mediatek/mtklogger/MainActivity;->mTagImageButton:Landroid/widget/ImageButton;

    const v0, 0x7f090033

    invoke-virtual {p0, v0}, Lcom/mediatek/mtklogger/MainActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ToggleButton;

    iput-object v0, p0, Lcom/mediatek/mtklogger/MainActivity;->mStartStopToggleButton:Landroid/widget/ToggleButton;

    const v0, 0x7f090032

    invoke-virtual {p0, v0}, Lcom/mediatek/mtklogger/MainActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageButton;

    iput-object v0, p0, Lcom/mediatek/mtklogger/MainActivity;->mClearLogImageButton:Landroid/widget/ImageButton;

    return-void
.end method

.method private initViews()V
    .locals 9

    const/4 v8, 0x0

    new-instance v3, Lcom/mediatek/mtklogger/framework/MTKLoggerManager;

    invoke-direct {v3, p0}, Lcom/mediatek/mtklogger/framework/MTKLoggerManager;-><init>(Landroid/content/Context;)V

    iput-object v3, p0, Lcom/mediatek/mtklogger/MainActivity;->mManager:Lcom/mediatek/mtklogger/framework/MTKLoggerManager;

    invoke-static {p0}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v3

    iput-object v3, p0, Lcom/mediatek/mtklogger/MainActivity;->mDefaultSharedPreferences:Landroid/content/SharedPreferences;

    const-string v3, "log_settings"

    invoke-virtual {p0, v3, v8}, Lcom/mediatek/mtklogger/MainActivity;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v3

    iput-object v3, p0, Lcom/mediatek/mtklogger/MainActivity;->mSharedPreferences:Landroid/content/SharedPreferences;

    sget-object v3, Lcom/mediatek/mtklogger/utils/Utils;->LOG_TYPE_SET:Ljava/util/Set;

    invoke-interface {v3}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Integer;

    iget-object v3, p0, Lcom/mediatek/mtklogger/MainActivity;->mLogTextViews:Landroid/util/SparseArray;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v4

    invoke-virtual {v3, v4}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/widget/TextView;

    const v5, 0x7f070017

    const/4 v4, 0x1

    new-array v6, v4, [Ljava/lang/Object;

    sget-object v4, Lcom/mediatek/mtklogger/utils/Utils;->LOG_NAME_MAP:Landroid/util/SparseArray;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v7

    invoke-virtual {v4, v7}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/Integer;

    invoke-virtual {v4}, Ljava/lang/Integer;->intValue()I

    move-result v4

    invoke-virtual {p0, v4}, Lcom/mediatek/mtklogger/MainActivity;->getString(I)Ljava/lang/String;

    move-result-object v4

    aput-object v4, v6, v8

    invoke-virtual {p0, v5, v6}, Lcom/mediatek/mtklogger/MainActivity;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0

    :cond_0
    invoke-virtual {p0}, Lcom/mediatek/mtklogger/MainActivity;->getIntent()Landroid/content/Intent;

    move-result-object v3

    const-string v4, "reason_start"

    invoke-virtual {v3, v4}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    if-eqz v2, :cond_1

    const-string v3, "low_storage"

    invoke-virtual {v3, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_1

    iget-object v3, p0, Lcom/mediatek/mtklogger/MainActivity;->mSharedPreferences:Landroid/content/SharedPreferences;

    invoke-static {v3}, Lcom/mediatek/mtklogger/utils/Utils;->checkLogStarted(Landroid/content/SharedPreferences;)Z

    move-result v3

    if-eqz v3, :cond_1

    invoke-direct {p0}, Lcom/mediatek/mtklogger/MainActivity;->alertLowStorageDialog()V

    :cond_1
    return-void
.end method

.method private isAutoTest()Z
    .locals 4

    const-string v1, "ro.monkey"

    const-string v2, "false"

    invoke-static {v1, v2}, Landroid/os/SystemProperties;->get(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    const-string v1, "MTKLogger/MainActivity"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "isAutoTest()-> Monkey running status is "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/mediatek/mtklogger/utils/Utils;->logi(Ljava/lang/String;Ljava/lang/String;)V

    const-string v1, "true"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    return v1
.end method

.method private monitorSdcardRatioBar()V
    .locals 6

    iget-object v0, p0, Lcom/mediatek/mtklogger/MainActivity;->mMonitorTimer:Ljava/util/Timer;

    if-eqz v0, :cond_0

    :goto_0
    return-void

    :cond_0
    new-instance v0, Ljava/util/Timer;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Ljava/util/Timer;-><init>(Z)V

    iput-object v0, p0, Lcom/mediatek/mtklogger/MainActivity;->mMonitorTimer:Ljava/util/Timer;

    iget-object v0, p0, Lcom/mediatek/mtklogger/MainActivity;->mMonitorTimer:Ljava/util/Timer;

    new-instance v1, Lcom/mediatek/mtklogger/MainActivity$11;

    invoke-direct {v1, p0}, Lcom/mediatek/mtklogger/MainActivity$11;-><init>(Lcom/mediatek/mtklogger/MainActivity;)V

    const-wide/16 v2, 0x0

    const-wide/16 v4, 0x7530

    invoke-virtual/range {v0 .. v5}, Ljava/util/Timer;->schedule(Ljava/util/TimerTask;JJ)V

    goto :goto_0
.end method

.method private refreshSdcardBar()V
    .locals 15

    const-wide/16 v13, 0x400

    const v12, 0x7f070015

    const v11, 0x7f070014

    const/high16 v10, 0x44800000

    const/4 v9, 0x0

    invoke-direct {p0}, Lcom/mediatek/mtklogger/MainActivity;->checkPath()Z

    move-result v5

    if-nez v5, :cond_0

    const-string v5, "MTKLogger/MainActivity"

    const-string v6, "Selected log path is unavailable, reset storage info"

    invoke-static {v5, v6}, Lcom/mediatek/mtklogger/utils/Utils;->logd(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v5, p0, Lcom/mediatek/mtklogger/MainActivity;->mSdcardRatioBar:Lcom/mediatek/mtklogger/LinearColorBar;

    invoke-virtual {v5, v9, v9}, Lcom/mediatek/mtklogger/LinearColorBar;->setGradientPaint(FF)V

    iget-object v5, p0, Lcom/mediatek/mtklogger/MainActivity;->mSdcardRatioBar:Lcom/mediatek/mtklogger/LinearColorBar;

    invoke-virtual {v5, v9, v9, v9}, Lcom/mediatek/mtklogger/LinearColorBar;->setRatios(FFF)V

    iget-object v5, p0, Lcom/mediatek/mtklogger/MainActivity;->mUsedStorageText:Landroid/widget/TextView;

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "0M "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {p0, v11}, Lcom/mediatek/mtklogger/MainActivity;->getString(I)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v5, p0, Lcom/mediatek/mtklogger/MainActivity;->mFreeStorageText:Landroid/widget/TextView;

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "0M "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {p0, v12}, Lcom/mediatek/mtklogger/MainActivity;->getString(I)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    :goto_0
    return-void

    :cond_0
    new-instance v4, Landroid/os/StatFs;

    iget-object v5, p0, Lcom/mediatek/mtklogger/MainActivity;->mSDCardPathStr:Ljava/lang/String;

    invoke-direct {v4, v5}, Landroid/os/StatFs;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4}, Landroid/os/StatFs;->getBlockSize()I

    move-result v5

    div-int/lit16 v0, v5, 0x400

    invoke-virtual {v4}, Landroid/os/StatFs;->getAvailableBlocks()I

    move-result v5

    mul-int/2addr v5, v0

    int-to-float v5, v5

    iput v5, p0, Lcom/mediatek/mtklogger/MainActivity;->mFreeStorageSize:F

    invoke-virtual {v4}, Landroid/os/StatFs;->getBlockCount()I

    move-result v5

    mul-int/2addr v5, v0

    int-to-float v5, v5

    iget v6, p0, Lcom/mediatek/mtklogger/MainActivity;->mFreeStorageSize:F

    sub-float/2addr v5, v6

    iput v5, p0, Lcom/mediatek/mtklogger/MainActivity;->mUsedStorageSize:F

    iget v5, p0, Lcom/mediatek/mtklogger/MainActivity;->mUsedStorageSize:F

    iget v6, p0, Lcom/mediatek/mtklogger/MainActivity;->mFreeStorageSize:F

    iget v7, p0, Lcom/mediatek/mtklogger/MainActivity;->mUsedStorageSize:F

    add-float/2addr v6, v7

    div-float v3, v5, v6

    iget-object v5, p0, Lcom/mediatek/mtklogger/MainActivity;->mSdcardRatioBar:Lcom/mediatek/mtklogger/LinearColorBar;

    const/4 v6, 0x0

    invoke-virtual {v5, v6}, Lcom/mediatek/mtklogger/LinearColorBar;->setShowingGreen(Z)V

    iget-object v5, p0, Lcom/mediatek/mtklogger/MainActivity;->mSavePathStr:Ljava/lang/String;

    invoke-static {v5}, Lcom/mediatek/mtklogger/utils/Utils;->getFileSize(Ljava/lang/String;)J

    move-result-wide v1

    cmp-long v5, v1, v13

    if-gtz v5, :cond_1

    const-wide/16 v1, 0x400

    :cond_1
    iget-object v5, p0, Lcom/mediatek/mtklogger/MainActivity;->mSdcardRatioBar:Lcom/mediatek/mtklogger/LinearColorBar;

    div-long v6, v1, v13

    long-to-float v6, v6

    iget v7, p0, Lcom/mediatek/mtklogger/MainActivity;->mFreeStorageSize:F

    iget v8, p0, Lcom/mediatek/mtklogger/MainActivity;->mUsedStorageSize:F

    add-float/2addr v7, v8

    div-float/2addr v6, v7

    sub-float v6, v3, v6

    invoke-virtual {v5, v6, v3}, Lcom/mediatek/mtklogger/LinearColorBar;->setGradientPaint(FF)V

    iget-object v5, p0, Lcom/mediatek/mtklogger/MainActivity;->mSdcardRatioBar:Lcom/mediatek/mtklogger/LinearColorBar;

    const/high16 v6, 0x3f800000

    sub-float/2addr v6, v3

    invoke-virtual {v5, v9, v3, v6}, Lcom/mediatek/mtklogger/LinearColorBar;->setRatios(FFF)V

    iget-object v5, p0, Lcom/mediatek/mtklogger/MainActivity;->mUsedStorageText:Landroid/widget/TextView;

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    iget v7, p0, Lcom/mediatek/mtklogger/MainActivity;->mUsedStorageSize:F

    div-float/2addr v7, v10

    float-to-int v7, v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, "M "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {p0, v11}, Lcom/mediatek/mtklogger/MainActivity;->getString(I)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v5, p0, Lcom/mediatek/mtklogger/MainActivity;->mFreeStorageText:Landroid/widget/TextView;

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    iget v7, p0, Lcom/mediatek/mtklogger/MainActivity;->mFreeStorageSize:F

    div-float/2addr v7, v10

    float-to-int v7, v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, "M "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {p0, v12}, Lcom/mediatek/mtklogger/MainActivity;->getString(I)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_0
.end method

.method private removeManualTitle()V
    .locals 2

    const v1, 0x7f09001d

    invoke-virtual {p0, v1}, Lcom/mediatek/mtklogger/MainActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    const v1, 0x7f09001e

    invoke-virtual {p0, v1}, Lcom/mediatek/mtklogger/MainActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->removeView(Landroid/view/View;)V

    return-void
.end method

.method private setButtonStatus(Z)V
    .locals 1
    .param p1    # Z

    iget-object v0, p0, Lcom/mediatek/mtklogger/MainActivity;->mSettingsMenuItem:Landroid/view/MenuItem;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/mediatek/mtklogger/MainActivity;->mSettingsMenuItem:Landroid/view/MenuItem;

    invoke-interface {v0, p1}, Landroid/view/MenuItem;->setEnabled(Z)Landroid/view/MenuItem;

    :cond_0
    iget-object v0, p0, Lcom/mediatek/mtklogger/MainActivity;->mTagImageButton:Landroid/widget/ImageButton;

    invoke-virtual {v0, p1}, Landroid/widget/ImageButton;->setEnabled(Z)V

    iget-object v0, p0, Lcom/mediatek/mtklogger/MainActivity;->mStartStopToggleButton:Landroid/widget/ToggleButton;

    invoke-virtual {v0, p1}, Landroid/widget/ToggleButton;->setEnabled(Z)V

    iget-object v0, p0, Lcom/mediatek/mtklogger/MainActivity;->mClearLogImageButton:Landroid/widget/ImageButton;

    invoke-virtual {v0, p1}, Landroid/widget/ImageButton;->setEnabled(Z)V

    return-void
.end method

.method private setListeners()V
    .locals 2

    iget-object v0, p0, Lcom/mediatek/mtklogger/MainActivity;->mSettingsImageButton:Landroid/widget/ImageButton;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/mediatek/mtklogger/MainActivity;->mSettingsImageButton:Landroid/widget/ImageButton;

    new-instance v1, Lcom/mediatek/mtklogger/MainActivity$6;

    invoke-direct {v1, p0}, Lcom/mediatek/mtklogger/MainActivity$6;-><init>(Lcom/mediatek/mtklogger/MainActivity;)V

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    :cond_0
    iget-object v0, p0, Lcom/mediatek/mtklogger/MainActivity;->mStartStopToggleButton:Landroid/widget/ToggleButton;

    new-instance v1, Lcom/mediatek/mtklogger/MainActivity$7;

    invoke-direct {v1, p0}, Lcom/mediatek/mtklogger/MainActivity$7;-><init>(Lcom/mediatek/mtklogger/MainActivity;)V

    invoke-virtual {v0, v1}, Landroid/widget/ToggleButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v0, p0, Lcom/mediatek/mtklogger/MainActivity;->mClearLogImageButton:Landroid/widget/ImageButton;

    new-instance v1, Lcom/mediatek/mtklogger/MainActivity$8;

    invoke-direct {v1, p0}, Lcom/mediatek/mtklogger/MainActivity$8;-><init>(Lcom/mediatek/mtklogger/MainActivity;)V

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v0, p0, Lcom/mediatek/mtklogger/MainActivity;->mTagImageButton:Landroid/widget/ImageButton;

    new-instance v1, Lcom/mediatek/mtklogger/MainActivity$9;

    invoke-direct {v1, p0}, Lcom/mediatek/mtklogger/MainActivity$9;-><init>(Lcom/mediatek/mtklogger/MainActivity;)V

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    return-void
.end method

.method private startTimer()V
    .locals 11

    const/4 v10, 0x1

    const/4 v9, 0x0

    const-wide/16 v2, 0x3e8

    const-wide/16 v4, 0x0

    iget-object v0, p0, Lcom/mediatek/mtklogger/MainActivity;->mSharedPreferences:Landroid/content/SharedPreferences;

    const-string v1, "begin_recording_time"

    invoke-interface {v0, v1, v4, v5}, Landroid/content/SharedPreferences;->getLong(Ljava/lang/String;J)J

    move-result-wide v6

    cmp-long v0, v6, v4

    if-nez v0, :cond_1

    iput-wide v4, p0, Lcom/mediatek/mtklogger/MainActivity;->mTimeMillisecond:J

    :goto_0
    iget-object v0, p0, Lcom/mediatek/mtklogger/MainActivity;->mSharedPreferences:Landroid/content/SharedPreferences;

    const-string v1, "system_time_reset"

    invoke-interface {v0, v1, v9}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v8

    if-eqz v8, :cond_0

    iget-object v0, p0, Lcom/mediatek/mtklogger/MainActivity;->mSharedPreferences:Landroid/content/SharedPreferences;

    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string v1, "system_time_reset"

    invoke-interface {v0, v1, v9}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    const v0, 0x7f07008c

    invoke-static {p0, v0, v10}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    :cond_0
    const-string v0, "MTKLogger/MainActivity"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "-->startTimer(), startTime="

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v6, v7}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v4, ", timeResetFlag="

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/mediatek/mtklogger/utils/Utils;->logv(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/mediatek/mtklogger/MainActivity;->mTimeText:Landroid/widget/TextView;

    invoke-direct {p0}, Lcom/mediatek/mtklogger/MainActivity;->calculateTimer()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    invoke-direct {p0}, Lcom/mediatek/mtklogger/MainActivity;->stopTimer()V

    new-instance v0, Ljava/util/Timer;

    invoke-direct {v0, v10}, Ljava/util/Timer;-><init>(Z)V

    iput-object v0, p0, Lcom/mediatek/mtklogger/MainActivity;->mTimer:Ljava/util/Timer;

    iget-object v0, p0, Lcom/mediatek/mtklogger/MainActivity;->mTimer:Ljava/util/Timer;

    new-instance v1, Lcom/mediatek/mtklogger/MainActivity$10;

    invoke-direct {v1, p0}, Lcom/mediatek/mtklogger/MainActivity$10;-><init>(Lcom/mediatek/mtklogger/MainActivity;)V

    move-wide v4, v2

    invoke-virtual/range {v0 .. v5}, Ljava/util/Timer;->schedule(Ljava/util/TimerTask;JJ)V

    return-void

    :cond_1
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    sub-long/2addr v0, v6

    div-long/2addr v0, v2

    iput-wide v0, p0, Lcom/mediatek/mtklogger/MainActivity;->mTimeMillisecond:J

    goto :goto_0
.end method

.method private stopTimer()V
    .locals 1

    iget-object v0, p0, Lcom/mediatek/mtklogger/MainActivity;->mTimer:Ljava/util/Timer;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/mediatek/mtklogger/MainActivity;->mTimer:Ljava/util/Timer;

    invoke-virtual {v0}, Ljava/util/Timer;->cancel()V

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/mediatek/mtklogger/MainActivity;->mTimer:Ljava/util/Timer;

    :cond_0
    return-void
.end method

.method private stopWaitingDialog()V
    .locals 2

    const/4 v1, 0x0

    iget-object v0, p0, Lcom/mediatek/mtklogger/MainActivity;->mWaitingDialog:Landroid/app/ProgressDialog;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/mediatek/mtklogger/MainActivity;->mWaitingDialog:Landroid/app/ProgressDialog;

    invoke-virtual {v0}, Landroid/app/ProgressDialog;->cancel()V

    iput-object v1, p0, Lcom/mediatek/mtklogger/MainActivity;->mWaitingDialog:Landroid/app/ProgressDialog;

    :cond_0
    iget-object v0, p0, Lcom/mediatek/mtklogger/MainActivity;->mWaitingDialogTimer:Ljava/util/Timer;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/mediatek/mtklogger/MainActivity;->mWaitingDialogTimer:Ljava/util/Timer;

    invoke-virtual {v0}, Ljava/util/Timer;->cancel()V

    iput-object v1, p0, Lcom/mediatek/mtklogger/MainActivity;->mWaitingDialogTimer:Ljava/util/Timer;

    :cond_1
    return-void
.end method

.method private tagLogs()V
    .locals 1

    iget-object v0, p0, Lcom/mediatek/mtklogger/MainActivity;->mStartStopToggleButton:Landroid/widget/ToggleButton;

    invoke-virtual {v0}, Landroid/widget/ToggleButton;->isChecked()Z

    move-result v0

    if-nez v0, :cond_0

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/mediatek/mtklogger/MainActivity;->mManager:Lcom/mediatek/mtklogger/framework/MTKLoggerManager;

    invoke-virtual {v0}, Lcom/mediatek/mtklogger/framework/MTKLoggerManager;->tagLog()Z

    goto :goto_0
.end method

.method private updateLogAutoStart(Z)V
    .locals 5
    .param p1    # Z

    sget-object v2, Lcom/mediatek/mtklogger/utils/Utils;->LOG_TYPE_SET:Ljava/util/Set;

    invoke-interface {v2}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :cond_0
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Integer;

    iget-object v3, p0, Lcom/mediatek/mtklogger/MainActivity;->mDefaultSharedPreferences:Landroid/content/SharedPreferences;

    sget-object v2, Lcom/mediatek/mtklogger/settings/SettingsActivity;->KEY_LOG_SWITCH_MAP:Landroid/util/SparseArray;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v4

    invoke-virtual {v2, v4}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    const/4 v4, 0x1

    invoke-interface {v3, v2, v4}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v2

    if-eqz v2, :cond_0

    const-string v2, "MTKLogger/MainActivity"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Log("

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ") is setAutoStart ? "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/mediatek/mtklogger/utils/Utils;->logd(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v2, p0, Lcom/mediatek/mtklogger/MainActivity;->mManager:Lcom/mediatek/mtklogger/framework/MTKLoggerManager;

    if-nez v2, :cond_2

    :cond_1
    return-void

    :cond_2
    iget-object v2, p0, Lcom/mediatek/mtklogger/MainActivity;->mManager:Lcom/mediatek/mtklogger/framework/MTKLoggerManager;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v3

    invoke-virtual {v2, v3, p1}, Lcom/mediatek/mtklogger/framework/MTKLoggerManager;->setAutoStart(IZ)Z

    iget-object v2, p0, Lcom/mediatek/mtklogger/MainActivity;->mDefaultSharedPreferences:Landroid/content/SharedPreferences;

    invoke-interface {v2}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v3

    sget-object v2, Lcom/mediatek/mtklogger/settings/SettingsActivity;->KEY_LOG_AUTOSTART_MAP:Landroid/util/SparseArray;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v4

    invoke-virtual {v2, v4}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    invoke-interface {v3, v2, p1}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    move-result-object v2

    invoke-interface {v2}, Landroid/content/SharedPreferences$Editor;->commit()Z

    goto :goto_0
.end method

.method private updateUI()V
    .locals 18

    const-string v12, "MTKLogger/MainActivity"

    const-string v13, "Update UI Start"

    invoke-static {v12, v13}, Lcom/mediatek/mtklogger/utils/Utils;->logd(Ljava/lang/String;Ljava/lang/String;)V

    invoke-static/range {p0 .. p0}, Lcom/mediatek/mtklogger/utils/Utils;->getCurrentLogPath(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v12

    move-object/from16 v0, p0

    iput-object v12, v0, Lcom/mediatek/mtklogger/MainActivity;->mSDCardPathStr:Ljava/lang/String;

    new-instance v12, Ljava/lang/StringBuilder;

    invoke-direct {v12}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/mediatek/mtklogger/MainActivity;->mSDCardPathStr:Ljava/lang/String;

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    const-string v13, "/mtklog/"

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v12

    move-object/from16 v0, p0

    iput-object v12, v0, Lcom/mediatek/mtklogger/MainActivity;->mSavePathStr:Ljava/lang/String;

    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/mediatek/mtklogger/MainActivity;->mSavePathText:Landroid/widget/TextView;

    new-instance v13, Ljava/lang/StringBuilder;

    invoke-direct {v13}, Ljava/lang/StringBuilder;-><init>()V

    const v14, 0x7f070013

    move-object/from16 v0, p0

    invoke-virtual {v0, v14}, Lcom/mediatek/mtklogger/MainActivity;->getString(I)Ljava/lang/String;

    move-result-object v14

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    const-string v14, ": "

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/mediatek/mtklogger/MainActivity;->mSavePathStr:Ljava/lang/String;

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v13}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v13

    invoke-virtual {v12, v13}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/mediatek/mtklogger/MainActivity;->mStorageChartLabelText:Landroid/widget/TextView;

    const-string v12, "/mnt/sdcard2"

    invoke-static {}, Lcom/mediatek/mtklogger/utils/Utils;->getLogPathType()Ljava/lang/String;

    move-result-object v14

    invoke-virtual {v12, v14}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v12

    if-eqz v12, :cond_0

    const v12, 0x7f07001b

    :goto_0
    invoke-virtual {v13, v12}, Landroid/widget/TextView;->setText(I)V

    invoke-direct/range {p0 .. p0}, Lcom/mediatek/mtklogger/MainActivity;->isAutoTest()Z

    move-result v12

    if-eqz v12, :cond_1

    const/4 v12, 0x0

    move-object/from16 v0, p0

    invoke-direct {v0, v12}, Lcom/mediatek/mtklogger/MainActivity;->setButtonStatus(Z)V

    :goto_1
    return-void

    :cond_0
    const v12, 0x7f07001a

    goto :goto_0

    :cond_1
    const/4 v12, 0x1

    move-object/from16 v0, p0

    invoke-direct {v0, v12}, Lcom/mediatek/mtklogger/MainActivity;->setButtonStatus(Z)V

    invoke-direct/range {p0 .. p0}, Lcom/mediatek/mtklogger/MainActivity;->changeWaitingDialog()V

    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/mediatek/mtklogger/MainActivity;->mSharedPreferences:Landroid/content/SharedPreferences;

    invoke-static {v12}, Lcom/mediatek/mtklogger/utils/Utils;->checkLogStarted(Landroid/content/SharedPreferences;)Z

    move-result v7

    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/mediatek/mtklogger/MainActivity;->mStartStopToggleButton:Landroid/widget/ToggleButton;

    invoke-virtual {v12, v7}, Landroid/widget/ToggleButton;->setChecked(Z)V

    if-eqz v7, :cond_2

    invoke-direct/range {p0 .. p0}, Lcom/mediatek/mtklogger/MainActivity;->startTimer()V

    :goto_2
    sget-object v12, Lcom/mediatek/mtklogger/utils/Utils;->LOG_TYPE_SET:Ljava/util/Set;

    invoke-interface {v12}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_3
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v12

    if-eqz v12, :cond_8

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v10

    check-cast v10, Ljava/lang/Integer;

    const/4 v13, 0x1

    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/mediatek/mtklogger/MainActivity;->mSharedPreferences:Landroid/content/SharedPreferences;

    sget-object v12, Lcom/mediatek/mtklogger/utils/Utils;->KEY_STATUS_MAP:Landroid/util/SparseArray;

    invoke-virtual {v10}, Ljava/lang/Integer;->intValue()I

    move-result v15

    invoke-virtual {v12, v15}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v12

    check-cast v12, Ljava/lang/String;

    const/4 v15, 0x0

    invoke-interface {v14, v12, v15}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v12

    if-ne v13, v12, :cond_3

    const/4 v6, 0x1

    :goto_4
    if-eqz v7, :cond_4

    if-eqz v6, :cond_4

    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/mediatek/mtklogger/MainActivity;->mLogImageViews:Landroid/util/SparseArray;

    invoke-virtual {v10}, Ljava/lang/Integer;->intValue()I

    move-result v13

    invoke-virtual {v12, v13}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v12

    check-cast v12, Landroid/widget/ImageView;

    const v13, 0x7f02000c

    invoke-virtual {v12, v13}, Landroid/widget/ImageView;->setImageResource(I)V

    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/mediatek/mtklogger/MainActivity;->mLogTextViews:Landroid/util/SparseArray;

    invoke-virtual {v10}, Ljava/lang/Integer;->intValue()I

    move-result v13

    invoke-virtual {v12, v13}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v12

    check-cast v12, Landroid/widget/TextView;

    const v14, 0x7f070016

    const/4 v13, 0x1

    new-array v15, v13, [Ljava/lang/Object;

    const/16 v16, 0x0

    sget-object v13, Lcom/mediatek/mtklogger/utils/Utils;->LOG_NAME_MAP:Landroid/util/SparseArray;

    invoke-virtual {v10}, Ljava/lang/Integer;->intValue()I

    move-result v17

    move/from16 v0, v17

    invoke-virtual {v13, v0}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v13

    check-cast v13, Ljava/lang/Integer;

    invoke-virtual {v13}, Ljava/lang/Integer;->intValue()I

    move-result v13

    move-object/from16 v0, p0

    invoke-virtual {v0, v13}, Lcom/mediatek/mtklogger/MainActivity;->getString(I)Ljava/lang/String;

    move-result-object v13

    aput-object v13, v15, v16

    move-object/from16 v0, p0

    invoke-virtual {v0, v14, v15}, Lcom/mediatek/mtklogger/MainActivity;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v13

    invoke-virtual {v12, v13}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/mediatek/mtklogger/MainActivity;->mLogImageViews:Landroid/util/SparseArray;

    invoke-virtual {v10}, Ljava/lang/Integer;->intValue()I

    move-result v13

    invoke-virtual {v12, v13}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v12

    check-cast v12, Landroid/widget/ImageView;

    const/16 v13, 0xff

    invoke-virtual {v12, v13}, Landroid/widget/ImageView;->setAlpha(I)V

    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/mediatek/mtklogger/MainActivity;->mLogTextViews:Landroid/util/SparseArray;

    invoke-virtual {v10}, Ljava/lang/Integer;->intValue()I

    move-result v13

    invoke-virtual {v12, v13}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v12

    check-cast v12, Landroid/widget/TextView;

    const/high16 v13, 0x437f0000

    invoke-virtual {v12, v13}, Landroid/widget/TextView;->setAlpha(F)V

    goto/16 :goto_3

    :cond_2
    invoke-direct/range {p0 .. p0}, Lcom/mediatek/mtklogger/MainActivity;->stopTimer()V

    goto/16 :goto_2

    :cond_3
    const/4 v6, 0x0

    goto :goto_4

    :cond_4
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/mediatek/mtklogger/MainActivity;->mDefaultSharedPreferences:Landroid/content/SharedPreferences;

    sget-object v12, Lcom/mediatek/mtklogger/settings/SettingsActivity;->KEY_LOG_SWITCH_MAP:Landroid/util/SparseArray;

    invoke-virtual {v10}, Ljava/lang/Integer;->intValue()I

    move-result v14

    invoke-virtual {v12, v14}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v12

    check-cast v12, Ljava/lang/String;

    const/4 v14, 0x1

    invoke-interface {v13, v12, v14}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v5

    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/mediatek/mtklogger/MainActivity;->mLogImageViews:Landroid/util/SparseArray;

    invoke-virtual {v10}, Ljava/lang/Integer;->intValue()I

    move-result v13

    invoke-virtual {v12, v13}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v12

    check-cast v12, Landroid/widget/ImageView;

    const v13, 0x7f02000b

    invoke-virtual {v12, v13}, Landroid/widget/ImageView;->setImageResource(I)V

    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/mediatek/mtklogger/MainActivity;->mLogTextViews:Landroid/util/SparseArray;

    invoke-virtual {v10}, Ljava/lang/Integer;->intValue()I

    move-result v13

    invoke-virtual {v12, v13}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v12

    check-cast v12, Landroid/widget/TextView;

    if-eqz v5, :cond_5

    const v13, 0x7f070017

    move v14, v13

    :goto_5
    const/4 v13, 0x1

    new-array v15, v13, [Ljava/lang/Object;

    const/16 v16, 0x0

    sget-object v13, Lcom/mediatek/mtklogger/utils/Utils;->LOG_NAME_MAP:Landroid/util/SparseArray;

    invoke-virtual {v10}, Ljava/lang/Integer;->intValue()I

    move-result v17

    move/from16 v0, v17

    invoke-virtual {v13, v0}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v13

    check-cast v13, Ljava/lang/Integer;

    invoke-virtual {v13}, Ljava/lang/Integer;->intValue()I

    move-result v13

    move-object/from16 v0, p0

    invoke-virtual {v0, v13}, Lcom/mediatek/mtklogger/MainActivity;->getString(I)Ljava/lang/String;

    move-result-object v13

    aput-object v13, v15, v16

    move-object/from16 v0, p0

    invoke-virtual {v0, v14, v15}, Lcom/mediatek/mtklogger/MainActivity;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v13

    invoke-virtual {v12, v13}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/mediatek/mtklogger/MainActivity;->mLogImageViews:Landroid/util/SparseArray;

    invoke-virtual {v10}, Ljava/lang/Integer;->intValue()I

    move-result v13

    invoke-virtual {v12, v13}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v12

    check-cast v12, Landroid/widget/ImageView;

    if-eqz v5, :cond_6

    const/16 v13, 0xff

    :goto_6
    invoke-virtual {v12, v13}, Landroid/widget/ImageView;->setAlpha(I)V

    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/mediatek/mtklogger/MainActivity;->mLogTextViews:Landroid/util/SparseArray;

    invoke-virtual {v10}, Ljava/lang/Integer;->intValue()I

    move-result v13

    invoke-virtual {v12, v13}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v12

    check-cast v12, Landroid/widget/TextView;

    if-eqz v5, :cond_7

    const/high16 v13, 0x437f0000

    :goto_7
    invoke-virtual {v12, v13}, Landroid/widget/TextView;->setAlpha(F)V

    goto/16 :goto_3

    :cond_5
    const v13, 0x7f070018

    move v14, v13

    goto :goto_5

    :cond_6
    const/16 v13, 0x4b

    goto :goto_6

    :cond_7
    const/high16 v13, 0x42960000

    goto :goto_7

    :cond_8
    const/4 v8, 0x0

    sget-object v1, Landroid/os/Build;->TYPE:Ljava/lang/String;

    if-eqz v1, :cond_9

    const-string v12, "MTKLogger/MainActivity"

    new-instance v13, Ljava/lang/StringBuilder;

    invoke-direct {v13}, Ljava/lang/StringBuilder;-><init>()V

    const-string v14, "Build type :"

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v13, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v13}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v13

    invoke-static {v12, v13}, Lcom/mediatek/mtklogger/utils/Utils;->logd(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v1}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v12

    const-string v13, "user"

    invoke-virtual {v12, v13}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v12

    if-eqz v12, :cond_e

    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/mediatek/mtklogger/MainActivity;->mSharedPreferences:Landroid/content/SharedPreferences;

    const-string v13, "tagLogEnable"

    const/4 v14, 0x0

    invoke-interface {v12, v13, v14}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v8

    :cond_9
    :goto_8
    if-eqz v7, :cond_f

    if-eqz v8, :cond_f

    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/mediatek/mtklogger/MainActivity;->mTagImageButton:Landroid/widget/ImageButton;

    const/4 v13, 0x1

    invoke-virtual {v12, v13}, Landroid/widget/ImageButton;->setEnabled(Z)V

    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/mediatek/mtklogger/MainActivity;->mTagImageButton:Landroid/widget/ImageButton;

    const/16 v13, 0xff

    invoke-virtual {v12, v13}, Landroid/widget/ImageButton;->setAlpha(I)V

    :goto_9
    const/4 v4, 0x0

    sget-object v12, Lcom/mediatek/mtklogger/utils/Utils;->LOG_TYPE_SET:Ljava/util/Set;

    invoke-interface {v12}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_a
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v12

    if-eqz v12, :cond_b

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v10

    check-cast v10, Ljava/lang/Integer;

    new-instance v9, Ljava/io/File;

    new-instance v12, Ljava/lang/StringBuilder;

    invoke-direct {v12}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/mediatek/mtklogger/MainActivity;->mSavePathStr:Ljava/lang/String;

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    sget-object v12, Lcom/mediatek/mtklogger/utils/Utils;->LOG_PATH_MAP:Landroid/util/SparseArray;

    invoke-virtual {v10}, Ljava/lang/Integer;->intValue()I

    move-result v14

    invoke-virtual {v12, v14}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v12

    check-cast v12, Ljava/lang/String;

    invoke-virtual {v13, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v12

    invoke-direct {v9, v12}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v9}, Ljava/io/File;->exists()Z

    move-result v12

    if-eqz v12, :cond_a

    invoke-virtual {v9}, Ljava/io/File;->listFiles()[Ljava/io/File;

    move-result-object v12

    array-length v12, v12

    if-lez v12, :cond_a

    const/4 v4, 0x1

    :cond_b
    new-instance v2, Ljava/io/File;

    new-instance v12, Ljava/lang/StringBuilder;

    invoke-direct {v12}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/mediatek/mtklogger/MainActivity;->mSavePathStr:Ljava/lang/String;

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    const-string v13, "dualmdlog"

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v12

    invoke-direct {v2, v12}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2}, Ljava/io/File;->exists()Z

    move-result v12

    if-eqz v12, :cond_c

    invoke-virtual {v2}, Ljava/io/File;->listFiles()[Ljava/io/File;

    move-result-object v12

    array-length v12, v12

    if-lez v12, :cond_c

    const/4 v4, 0x1

    :cond_c
    new-instance v11, Ljava/io/File;

    new-instance v12, Ljava/lang/StringBuilder;

    invoke-direct {v12}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/mediatek/mtklogger/MainActivity;->mSavePathStr:Ljava/lang/String;

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    const-string v13, "taglog"

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v12

    invoke-direct {v11, v12}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v11}, Ljava/io/File;->exists()Z

    move-result v12

    if-eqz v12, :cond_d

    invoke-virtual {v11}, Ljava/io/File;->listFiles()[Ljava/io/File;

    move-result-object v12

    array-length v12, v12

    if-lez v12, :cond_d

    const/4 v4, 0x1

    :cond_d
    if-eqz v4, :cond_10

    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/mediatek/mtklogger/MainActivity;->mClearLogImageButton:Landroid/widget/ImageButton;

    const/4 v13, 0x1

    invoke-virtual {v12, v13}, Landroid/widget/ImageButton;->setEnabled(Z)V

    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/mediatek/mtklogger/MainActivity;->mClearLogImageButton:Landroid/widget/ImageButton;

    const/16 v13, 0xff

    invoke-virtual {v12, v13}, Landroid/widget/ImageButton;->setAlpha(I)V

    goto/16 :goto_1

    :cond_e
    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/mediatek/mtklogger/MainActivity;->mSharedPreferences:Landroid/content/SharedPreferences;

    const-string v13, "tagLogEnable"

    const/4 v14, 0x1

    invoke-interface {v12, v13, v14}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v8

    goto/16 :goto_8

    :cond_f
    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/mediatek/mtklogger/MainActivity;->mTagImageButton:Landroid/widget/ImageButton;

    const/4 v13, 0x0

    invoke-virtual {v12, v13}, Landroid/widget/ImageButton;->setEnabled(Z)V

    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/mediatek/mtklogger/MainActivity;->mTagImageButton:Landroid/widget/ImageButton;

    const/16 v13, 0x4b

    invoke-virtual {v12, v13}, Landroid/widget/ImageButton;->setAlpha(I)V

    goto/16 :goto_9

    :cond_10
    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/mediatek/mtklogger/MainActivity;->mClearLogImageButton:Landroid/widget/ImageButton;

    const/4 v13, 0x0

    invoke-virtual {v12, v13}, Landroid/widget/ImageButton;->setEnabled(Z)V

    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/mediatek/mtklogger/MainActivity;->mClearLogImageButton:Landroid/widget/ImageButton;

    const/16 v13, 0x4b

    invoke-virtual {v12, v13}, Landroid/widget/ImageButton;->setAlpha(I)V

    goto/16 :goto_1
.end method


# virtual methods
.method public onCreate(Landroid/os/Bundle;)V
    .locals 4
    .param p1    # Landroid/os/Bundle;

    const-string v0, "MTKLogger/MainActivity"

    const-string v1, "onCreate"

    invoke-static {v0, v1}, Lcom/mediatek/mtklogger/utils/Utils;->logd(Ljava/lang/String;Ljava/lang/String;)V

    invoke-super {p0, p1}, Landroid/app/Activity;->onCreate(Landroid/os/Bundle;)V

    const v0, 0x7f030008

    invoke-virtual {p0, v0}, Lcom/mediatek/mtklogger/MainActivity;->setContentView(I)V

    sget v0, Lcom/mediatek/mtklogger/utils/Utils;->ANDROID_VERSION_NUMBER:F

    float-to-double v0, v0

    const-wide v2, 0x400ffdf3b645a1cbL

    cmpl-double v0, v0, v2

    if-lez v0, :cond_0

    invoke-direct {p0}, Lcom/mediatek/mtklogger/MainActivity;->removeManualTitle()V

    :cond_0
    invoke-direct {p0}, Lcom/mediatek/mtklogger/MainActivity;->findViews()V

    invoke-direct {p0}, Lcom/mediatek/mtklogger/MainActivity;->initViews()V

    invoke-direct {p0}, Lcom/mediatek/mtklogger/MainActivity;->setListeners()V

    invoke-direct {p0}, Lcom/mediatek/mtklogger/MainActivity;->updateUI()V

    return-void
.end method

.method public onCreateOptionsMenu(Landroid/view/Menu;)Z
    .locals 5
    .param p1    # Landroid/view/Menu;

    const/4 v4, 0x1

    sget v0, Lcom/mediatek/mtklogger/utils/Utils;->ANDROID_VERSION_NUMBER:F

    float-to-double v0, v0

    const-wide v2, 0x400ffdf3b645a1cbL

    cmpg-double v0, v0, v2

    if-gez v0, :cond_0

    :goto_0
    return v4

    :cond_0
    const v0, 0x7f070010

    invoke-virtual {p0, v0}, Lcom/mediatek/mtklogger/MainActivity;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-interface {p1, v0}, Landroid/view/Menu;->add(Ljava/lang/CharSequence;)Landroid/view/MenuItem;

    move-result-object v0

    iput-object v0, p0, Lcom/mediatek/mtklogger/MainActivity;->mSettingsMenuItem:Landroid/view/MenuItem;

    iget-object v0, p0, Lcom/mediatek/mtklogger/MainActivity;->mSettingsMenuItem:Landroid/view/MenuItem;

    const/4 v1, 0x2

    invoke-interface {v0, v1}, Landroid/view/MenuItem;->setShowAsAction(I)V

    iget-object v0, p0, Lcom/mediatek/mtklogger/MainActivity;->mSettingsMenuItem:Landroid/view/MenuItem;

    const v1, 0x7f020008

    invoke-interface {v0, v1}, Landroid/view/MenuItem;->setIcon(I)Landroid/view/MenuItem;

    invoke-direct {p0}, Lcom/mediatek/mtklogger/MainActivity;->isAutoTest()Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/mediatek/mtklogger/MainActivity;->mSettingsMenuItem:Landroid/view/MenuItem;

    const/4 v1, 0x0

    invoke-interface {v0, v1}, Landroid/view/MenuItem;->setEnabled(Z)Landroid/view/MenuItem;

    goto :goto_0

    :cond_1
    iget-object v0, p0, Lcom/mediatek/mtklogger/MainActivity;->mSettingsMenuItem:Landroid/view/MenuItem;

    invoke-interface {v0, v4}, Landroid/view/MenuItem;->setEnabled(Z)Landroid/view/MenuItem;

    goto :goto_0
.end method

.method protected onDestroy()V
    .locals 2

    const-string v0, "MTKLogger/MainActivity"

    const-string v1, "onDestroy"

    invoke-static {v0, v1}, Lcom/mediatek/mtklogger/utils/Utils;->logd(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/mediatek/mtklogger/MainActivity;->mManager:Lcom/mediatek/mtklogger/framework/MTKLoggerManager;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/mediatek/mtklogger/MainActivity;->mManager:Lcom/mediatek/mtklogger/framework/MTKLoggerManager;

    invoke-virtual {v0}, Lcom/mediatek/mtklogger/framework/MTKLoggerManager;->free()V

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/mediatek/mtklogger/MainActivity;->mManager:Lcom/mediatek/mtklogger/framework/MTKLoggerManager;

    :cond_0
    invoke-direct {p0}, Lcom/mediatek/mtklogger/MainActivity;->stopWaitingDialog()V

    invoke-super {p0}, Landroid/app/Activity;->onDestroy()V

    return-void
.end method

.method public onOptionsItemSelected(Landroid/view/MenuItem;)Z
    .locals 6
    .param p1    # Landroid/view/MenuItem;

    const/4 v5, 0x1

    sget v1, Lcom/mediatek/mtklogger/utils/Utils;->ANDROID_VERSION_NUMBER:F

    float-to-double v1, v1

    const-wide v3, 0x400ffdf3b645a1cbL

    cmpg-double v1, v1, v3

    if-gez v1, :cond_0

    :goto_0
    return v5

    :cond_0
    const-string v1, "MTKLogger/MainActivity"

    const-string v2, "SettingsActivity open!"

    invoke-static {v1, v2}, Lcom/mediatek/mtklogger/utils/Utils;->logd(Ljava/lang/String;Ljava/lang/String;)V

    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/mediatek/mtklogger/settings/SettingsActivity;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    invoke-virtual {p0, v0}, Lcom/mediatek/mtklogger/MainActivity;->startActivity(Landroid/content/Intent;)V

    goto :goto_0
.end method

.method protected onPause()V
    .locals 2

    const-string v0, "MTKLogger/MainActivity"

    const-string v1, "onPause"

    invoke-static {v0, v1}, Lcom/mediatek/mtklogger/utils/Utils;->logd(Ljava/lang/String;Ljava/lang/String;)V

    invoke-super {p0}, Landroid/app/Activity;->onPause()V

    iget-object v0, p0, Lcom/mediatek/mtklogger/MainActivity;->mMonitorTimer:Ljava/util/Timer;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/mediatek/mtklogger/MainActivity;->mMonitorTimer:Ljava/util/Timer;

    invoke-virtual {v0}, Ljava/util/Timer;->cancel()V

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/mediatek/mtklogger/MainActivity;->mMonitorTimer:Ljava/util/Timer;

    :cond_0
    invoke-direct {p0}, Lcom/mediatek/mtklogger/MainActivity;->stopTimer()V

    iget-object v0, p0, Lcom/mediatek/mtklogger/MainActivity;->mServiceReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {p0, v0}, Lcom/mediatek/mtklogger/MainActivity;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    iget-object v0, p0, Lcom/mediatek/mtklogger/MainActivity;->mStorageStatusReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {p0, v0}, Lcom/mediatek/mtklogger/MainActivity;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    return-void
.end method

.method protected onResume()V
    .locals 4

    const-string v2, "MTKLogger/MainActivity"

    const-string v3, "onResume"

    invoke-static {v2, v3}, Lcom/mediatek/mtklogger/utils/Utils;->logd(Ljava/lang/String;Ljava/lang/String;)V

    new-instance v1, Landroid/content/IntentFilter;

    invoke-direct {v1}, Landroid/content/IntentFilter;-><init>()V

    const-string v2, "com.mediatek.mtklogger.intent.action.LOG_STATE_CHANGED"

    invoke-virtual {v1, v2}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    const-string v2, "stage_event"

    invoke-virtual {v1, v2}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    const-string v2, "extra_key_from_taglog"

    invoke-virtual {v1, v2}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    iget-object v2, p0, Lcom/mediatek/mtklogger/MainActivity;->mServiceReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {p0, v2, v1}, Lcom/mediatek/mtklogger/MainActivity;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    new-instance v0, Landroid/content/IntentFilter;

    invoke-direct {v0}, Landroid/content/IntentFilter;-><init>()V

    const-string v2, "android.intent.action.MEDIA_BAD_REMOVAL"

    invoke-virtual {v0, v2}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    const-string v2, "android.intent.action.MEDIA_EJECT"

    invoke-virtual {v0, v2}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    const-string v2, "android.intent.action.MEDIA_REMOVED"

    invoke-virtual {v0, v2}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    const-string v2, "android.intent.action.MEDIA_UNMOUNTED"

    invoke-virtual {v0, v2}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    const-string v2, "android.intent.action.MEDIA_MOUNTED"

    invoke-virtual {v0, v2}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    const-string v2, "file"

    invoke-virtual {v0, v2}, Landroid/content/IntentFilter;->addDataScheme(Ljava/lang/String;)V

    iget-object v2, p0, Lcom/mediatek/mtklogger/MainActivity;->mStorageStatusReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {p0, v2, v0}, Lcom/mediatek/mtklogger/MainActivity;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    invoke-direct {p0}, Lcom/mediatek/mtklogger/MainActivity;->checkTaglogCompressing()V

    invoke-direct {p0}, Lcom/mediatek/mtklogger/MainActivity;->updateUI()V

    invoke-direct {p0}, Lcom/mediatek/mtklogger/MainActivity;->monitorSdcardRatioBar()V

    invoke-super {p0}, Landroid/app/Activity;->onResume()V

    return-void
.end method

.method protected onStop()V
    .locals 2

    const-string v0, "MTKLogger/MainActivity"

    const-string v1, "onStop"

    invoke-static {v0, v1}, Lcom/mediatek/mtklogger/utils/Utils;->logd(Ljava/lang/String;Ljava/lang/String;)V

    invoke-super {p0}, Landroid/app/Activity;->onStop()V

    return-void
.end method
