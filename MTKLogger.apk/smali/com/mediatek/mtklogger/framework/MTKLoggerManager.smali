.class public Lcom/mediatek/mtklogger/framework/MTKLoggerManager;
.super Ljava/lang/Object;
.source "MTKLoggerManager.java"


# static fields
.field private static final TAG:Ljava/lang/String; = "MTKLogger/MTKLoggerManager"


# instance fields
.field private mContext:Landroid/content/Context;

.field private mService:Lcom/mediatek/mtklogger/IMTKLoggerManager;

.field mServiceConnection:Landroid/content/ServiceConnection;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1    # Landroid/content/Context;

    const/4 v0, 0x0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lcom/mediatek/mtklogger/framework/MTKLoggerManager;->mService:Lcom/mediatek/mtklogger/IMTKLoggerManager;

    iput-object v0, p0, Lcom/mediatek/mtklogger/framework/MTKLoggerManager;->mContext:Landroid/content/Context;

    new-instance v0, Lcom/mediatek/mtklogger/framework/MTKLoggerManager$1;

    invoke-direct {v0, p0}, Lcom/mediatek/mtklogger/framework/MTKLoggerManager$1;-><init>(Lcom/mediatek/mtklogger/framework/MTKLoggerManager;)V

    iput-object v0, p0, Lcom/mediatek/mtklogger/framework/MTKLoggerManager;->mServiceConnection:Landroid/content/ServiceConnection;

    iput-object p1, p0, Lcom/mediatek/mtklogger/framework/MTKLoggerManager;->mContext:Landroid/content/Context;

    invoke-direct {p0}, Lcom/mediatek/mtklogger/framework/MTKLoggerManager;->initService()V

    return-void
.end method

.method static synthetic access$002(Lcom/mediatek/mtklogger/framework/MTKLoggerManager;Lcom/mediatek/mtklogger/IMTKLoggerManager;)Lcom/mediatek/mtklogger/IMTKLoggerManager;
    .locals 0
    .param p0    # Lcom/mediatek/mtklogger/framework/MTKLoggerManager;
    .param p1    # Lcom/mediatek/mtklogger/IMTKLoggerManager;

    iput-object p1, p0, Lcom/mediatek/mtklogger/framework/MTKLoggerManager;->mService:Lcom/mediatek/mtklogger/IMTKLoggerManager;

    return-object p1
.end method

.method private initService()V
    .locals 4

    const-string v1, "MTKLogger/MTKLoggerManager"

    const-string v2, "-->initService()"

    invoke-static {v1, v2}, Lcom/mediatek/mtklogger/utils/Utils;->logd(Ljava/lang/String;Ljava/lang/String;)V

    new-instance v0, Landroid/content/Intent;

    const-string v1, "com.mediatek.mtklogger.MTKLoggerService"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/mediatek/mtklogger/framework/MTKLoggerManager;->mContext:Landroid/content/Context;

    invoke-virtual {v1, v0}, Landroid/content/Context;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;

    iget-object v1, p0, Lcom/mediatek/mtklogger/framework/MTKLoggerManager;->mContext:Landroid/content/Context;

    iget-object v2, p0, Lcom/mediatek/mtklogger/framework/MTKLoggerManager;->mServiceConnection:Landroid/content/ServiceConnection;

    const/4 v3, 0x1

    invoke-virtual {v1, v0, v2, v3}, Landroid/content/Context;->bindService(Landroid/content/Intent;Landroid/content/ServiceConnection;I)Z

    move-result v1

    if-nez v1, :cond_0

    const-string v1, "MTKLogger/MTKLoggerManager"

    const-string v2, "Fail to bind to MTKLoggerService"

    invoke-static {v1, v2}, Lcom/mediatek/mtklogger/utils/Utils;->loge(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v1, p0, Lcom/mediatek/mtklogger/framework/MTKLoggerManager;->mContext:Landroid/content/Context;

    iget-object v2, p0, Lcom/mediatek/mtklogger/framework/MTKLoggerManager;->mServiceConnection:Landroid/content/ServiceConnection;

    invoke-virtual {v1, v2}, Landroid/content/Context;->unbindService(Landroid/content/ServiceConnection;)V

    iget-object v1, p0, Lcom/mediatek/mtklogger/framework/MTKLoggerManager;->mContext:Landroid/content/Context;

    invoke-virtual {v1, v0}, Landroid/content/Context;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;

    :cond_0
    return-void
.end method


# virtual methods
.method public clearLog()Z
    .locals 4

    const/4 v1, 0x0

    iget-object v2, p0, Lcom/mediatek/mtklogger/framework/MTKLoggerManager;->mService:Lcom/mediatek/mtklogger/IMTKLoggerManager;

    if-nez v2, :cond_0

    const-string v2, "MTKLogger/MTKLoggerManager"

    const-string v3, "Service has not been bind to yet."

    invoke-static {v2, v3}, Lcom/mediatek/mtklogger/utils/Utils;->loge(Ljava/lang/String;Ljava/lang/String;)V

    :goto_0
    return v1

    :cond_0
    :try_start_0
    iget-object v2, p0, Lcom/mediatek/mtklogger/framework/MTKLoggerManager;->mService:Lcom/mediatek/mtklogger/IMTKLoggerManager;

    invoke-interface {v2}, Lcom/mediatek/mtklogger/IMTKLoggerManager;->clearLog()Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    goto :goto_0

    :catch_0
    move-exception v0

    const-string v2, "MTKLogger/MTKLoggerManager"

    const-string v3, "Fail to call service API."

    invoke-static {v2, v3, v0}, Lcom/mediatek/mtklogger/utils/Utils;->loge(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0
.end method

.method public free()V
    .locals 2

    iget-object v0, p0, Lcom/mediatek/mtklogger/framework/MTKLoggerManager;->mContext:Landroid/content/Context;

    iget-object v1, p0, Lcom/mediatek/mtklogger/framework/MTKLoggerManager;->mServiceConnection:Landroid/content/ServiceConnection;

    invoke-virtual {v0, v1}, Landroid/content/Context;->unbindService(Landroid/content/ServiceConnection;)V

    return-void
.end method

.method public getCurrentRunningStage()I
    .locals 4

    const/4 v1, 0x0

    iget-object v2, p0, Lcom/mediatek/mtklogger/framework/MTKLoggerManager;->mService:Lcom/mediatek/mtklogger/IMTKLoggerManager;

    if-nez v2, :cond_0

    const-string v2, "MTKLogger/MTKLoggerManager"

    const-string v3, "Service has not been bind to yet."

    invoke-static {v2, v3}, Lcom/mediatek/mtklogger/utils/Utils;->loge(Ljava/lang/String;Ljava/lang/String;)V

    :goto_0
    return v1

    :cond_0
    :try_start_0
    iget-object v2, p0, Lcom/mediatek/mtklogger/framework/MTKLoggerManager;->mService:Lcom/mediatek/mtklogger/IMTKLoggerManager;

    invoke-interface {v2}, Lcom/mediatek/mtklogger/IMTKLoggerManager;->getCurrentRunningStage()I
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    goto :goto_0

    :catch_0
    move-exception v0

    const-string v2, "MTKLogger/MTKLoggerManager"

    const-string v3, "Fail to call service API."

    invoke-static {v2, v3, v0}, Lcom/mediatek/mtklogger/utils/Utils;->loge(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0
.end method

.method public getModemLogRunningDetailStatus()I
    .locals 3

    iget-object v1, p0, Lcom/mediatek/mtklogger/framework/MTKLoggerManager;->mService:Lcom/mediatek/mtklogger/IMTKLoggerManager;

    if-nez v1, :cond_0

    const-string v1, "MTKLogger/MTKLoggerManager"

    const-string v2, "Service has not been bind to yet."

    invoke-static {v1, v2}, Lcom/mediatek/mtklogger/utils/Utils;->loge(Ljava/lang/String;Ljava/lang/String;)V

    const/4 v1, -0x1

    :goto_0
    return v1

    :cond_0
    :try_start_0
    iget-object v1, p0, Lcom/mediatek/mtklogger/framework/MTKLoggerManager;->mService:Lcom/mediatek/mtklogger/IMTKLoggerManager;

    const/4 v2, 0x2

    invoke-interface {v1, v2}, Lcom/mediatek/mtklogger/IMTKLoggerManager;->getLogRunningStatus(I)I
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    goto :goto_0

    :catch_0
    move-exception v0

    const-string v1, "MTKLogger/MTKLoggerManager"

    const-string v2, "Fail to call service API."

    invoke-static {v1, v2, v0}, Lcom/mediatek/mtklogger/utils/Utils;->loge(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    const/4 v1, 0x0

    goto :goto_0
.end method

.method public runCommand(Ljava/lang/String;)Z
    .locals 4
    .param p1    # Ljava/lang/String;

    const/4 v1, 0x0

    iget-object v2, p0, Lcom/mediatek/mtklogger/framework/MTKLoggerManager;->mService:Lcom/mediatek/mtklogger/IMTKLoggerManager;

    if-nez v2, :cond_0

    const-string v2, "MTKLogger/MTKLoggerManager"

    const-string v3, "Service has not been bind to yet."

    invoke-static {v2, v3}, Lcom/mediatek/mtklogger/utils/Utils;->loge(Ljava/lang/String;Ljava/lang/String;)V

    :goto_0
    return v1

    :cond_0
    :try_start_0
    iget-object v2, p0, Lcom/mediatek/mtklogger/framework/MTKLoggerManager;->mService:Lcom/mediatek/mtklogger/IMTKLoggerManager;

    invoke-interface {v2, p1}, Lcom/mediatek/mtklogger/IMTKLoggerManager;->runCommand(Ljava/lang/String;)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    goto :goto_0

    :catch_0
    move-exception v0

    const-string v2, "MTKLogger/MTKLoggerManager"

    const-string v3, "Fail to call service API."

    invoke-static {v2, v3, v0}, Lcom/mediatek/mtklogger/utils/Utils;->loge(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0
.end method

.method public setAutoStart(IZ)Z
    .locals 4
    .param p1    # I
    .param p2    # Z

    const/4 v1, 0x0

    iget-object v2, p0, Lcom/mediatek/mtklogger/framework/MTKLoggerManager;->mService:Lcom/mediatek/mtklogger/IMTKLoggerManager;

    if-nez v2, :cond_0

    const-string v2, "MTKLogger/MTKLoggerManager"

    const-string v3, "Service has not been bind to yet."

    invoke-static {v2, v3}, Lcom/mediatek/mtklogger/utils/Utils;->loge(Ljava/lang/String;Ljava/lang/String;)V

    :goto_0
    return v1

    :cond_0
    :try_start_0
    iget-object v2, p0, Lcom/mediatek/mtklogger/framework/MTKLoggerManager;->mService:Lcom/mediatek/mtklogger/IMTKLoggerManager;

    invoke-interface {v2, p1, p2}, Lcom/mediatek/mtklogger/IMTKLoggerManager;->setAutoStart(IZ)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    goto :goto_0

    :catch_0
    move-exception v0

    const-string v2, "MTKLogger/MTKLoggerManager"

    const-string v3, "Fail to call service API."

    invoke-static {v2, v3, v0}, Lcom/mediatek/mtklogger/utils/Utils;->loge(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0
.end method

.method public setLogSize(II)Z
    .locals 4
    .param p1    # I
    .param p2    # I

    const/4 v1, 0x0

    iget-object v2, p0, Lcom/mediatek/mtklogger/framework/MTKLoggerManager;->mService:Lcom/mediatek/mtklogger/IMTKLoggerManager;

    if-nez v2, :cond_0

    const-string v2, "MTKLogger/MTKLoggerManager"

    const-string v3, "Service has not been bind to yet."

    invoke-static {v2, v3}, Lcom/mediatek/mtklogger/utils/Utils;->loge(Ljava/lang/String;Ljava/lang/String;)V

    :goto_0
    return v1

    :cond_0
    :try_start_0
    iget-object v2, p0, Lcom/mediatek/mtklogger/framework/MTKLoggerManager;->mService:Lcom/mediatek/mtklogger/IMTKLoggerManager;

    invoke-interface {v2, p1, p2}, Lcom/mediatek/mtklogger/IMTKLoggerManager;->setLogSize(II)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    goto :goto_0

    :catch_0
    move-exception v0

    const-string v2, "MTKLogger/MTKLoggerManager"

    const-string v3, "Fail to call service API."

    invoke-static {v2, v3, v0}, Lcom/mediatek/mtklogger/utils/Utils;->loge(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0
.end method

.method public setMobileSubLogEnableState(IZ)Z
    .locals 4
    .param p1    # I
    .param p2    # Z

    const/4 v1, 0x0

    iget-object v2, p0, Lcom/mediatek/mtklogger/framework/MTKLoggerManager;->mService:Lcom/mediatek/mtklogger/IMTKLoggerManager;

    if-nez v2, :cond_0

    const-string v2, "MTKLogger/MTKLoggerManager"

    const-string v3, "Service has not been bind to yet."

    invoke-static {v2, v3}, Lcom/mediatek/mtklogger/utils/Utils;->loge(Ljava/lang/String;Ljava/lang/String;)V

    :goto_0
    return v1

    :cond_0
    :try_start_0
    iget-object v2, p0, Lcom/mediatek/mtklogger/framework/MTKLoggerManager;->mService:Lcom/mediatek/mtklogger/IMTKLoggerManager;

    const/4 v3, 0x1

    invoke-interface {v2, v3, p1, p2}, Lcom/mediatek/mtklogger/IMTKLoggerManager;->setSubLogEnableState(IIZ)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    goto :goto_0

    :catch_0
    move-exception v0

    const-string v2, "MTKLogger/MTKLoggerManager"

    const-string v3, "Fail to call service API."

    invoke-static {v2, v3, v0}, Lcom/mediatek/mtklogger/utils/Utils;->loge(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0
.end method

.method public startLog(I)Z
    .locals 4
    .param p1    # I

    const/4 v1, 0x0

    iget-object v2, p0, Lcom/mediatek/mtklogger/framework/MTKLoggerManager;->mService:Lcom/mediatek/mtklogger/IMTKLoggerManager;

    if-nez v2, :cond_0

    const-string v2, "MTKLogger/MTKLoggerManager"

    const-string v3, "Service has not been bind to yet."

    invoke-static {v2, v3}, Lcom/mediatek/mtklogger/utils/Utils;->loge(Ljava/lang/String;Ljava/lang/String;)V

    :goto_0
    return v1

    :cond_0
    :try_start_0
    iget-object v2, p0, Lcom/mediatek/mtklogger/framework/MTKLoggerManager;->mService:Lcom/mediatek/mtklogger/IMTKLoggerManager;

    invoke-interface {v2, p1}, Lcom/mediatek/mtklogger/IMTKLoggerManager;->startLog(I)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    goto :goto_0

    :catch_0
    move-exception v0

    const-string v2, "MTKLogger/MTKLoggerManager"

    const-string v3, "Fail to call service API."

    invoke-static {v2, v3, v0}, Lcom/mediatek/mtklogger/utils/Utils;->loge(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0
.end method

.method public stopCommand(Ljava/lang/String;)Z
    .locals 4
    .param p1    # Ljava/lang/String;

    const/4 v1, 0x0

    iget-object v2, p0, Lcom/mediatek/mtklogger/framework/MTKLoggerManager;->mService:Lcom/mediatek/mtklogger/IMTKLoggerManager;

    if-nez v2, :cond_0

    const-string v2, "MTKLogger/MTKLoggerManager"

    const-string v3, "Service has not been bind to yet."

    invoke-static {v2, v3}, Lcom/mediatek/mtklogger/utils/Utils;->loge(Ljava/lang/String;Ljava/lang/String;)V

    :goto_0
    return v1

    :cond_0
    :try_start_0
    iget-object v2, p0, Lcom/mediatek/mtklogger/framework/MTKLoggerManager;->mService:Lcom/mediatek/mtklogger/IMTKLoggerManager;

    invoke-interface {v2}, Lcom/mediatek/mtklogger/IMTKLoggerManager;->stopCommand()Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    goto :goto_0

    :catch_0
    move-exception v0

    const-string v2, "MTKLogger/MTKLoggerManager"

    const-string v3, "Fail to call service API."

    invoke-static {v2, v3, v0}, Lcom/mediatek/mtklogger/utils/Utils;->loge(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0
.end method

.method public stopLog(I)Z
    .locals 4
    .param p1    # I

    const/4 v1, 0x0

    iget-object v2, p0, Lcom/mediatek/mtklogger/framework/MTKLoggerManager;->mService:Lcom/mediatek/mtklogger/IMTKLoggerManager;

    if-nez v2, :cond_0

    const-string v2, "MTKLogger/MTKLoggerManager"

    const-string v3, "Service has not been bind to yet."

    invoke-static {v2, v3}, Lcom/mediatek/mtklogger/utils/Utils;->loge(Ljava/lang/String;Ljava/lang/String;)V

    :goto_0
    return v1

    :cond_0
    :try_start_0
    iget-object v2, p0, Lcom/mediatek/mtklogger/framework/MTKLoggerManager;->mService:Lcom/mediatek/mtklogger/IMTKLoggerManager;

    invoke-interface {v2, p1}, Lcom/mediatek/mtklogger/IMTKLoggerManager;->stopLog(I)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    goto :goto_0

    :catch_0
    move-exception v0

    const-string v2, "MTKLogger/MTKLoggerManager"

    const-string v3, "Fail to call service API."

    invoke-static {v2, v3, v0}, Lcom/mediatek/mtklogger/utils/Utils;->loge(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0
.end method

.method public tagLog()Z
    .locals 4

    const/4 v1, 0x0

    iget-object v2, p0, Lcom/mediatek/mtklogger/framework/MTKLoggerManager;->mService:Lcom/mediatek/mtklogger/IMTKLoggerManager;

    if-nez v2, :cond_0

    const-string v2, "MTKLogger/MTKLoggerManager"

    const-string v3, "Service has not been bind to yet."

    invoke-static {v2, v3}, Lcom/mediatek/mtklogger/utils/Utils;->loge(Ljava/lang/String;Ljava/lang/String;)V

    :goto_0
    return v1

    :cond_0
    :try_start_0
    iget-object v2, p0, Lcom/mediatek/mtklogger/framework/MTKLoggerManager;->mService:Lcom/mediatek/mtklogger/IMTKLoggerManager;

    invoke-interface {v2}, Lcom/mediatek/mtklogger/IMTKLoggerManager;->tagLog()Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    goto :goto_0

    :catch_0
    move-exception v0

    const-string v2, "MTKLogger/MTKLoggerManager"

    const-string v3, "Fail to call service API."

    invoke-static {v2, v3, v0}, Lcom/mediatek/mtklogger/utils/Utils;->loge(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0
.end method
