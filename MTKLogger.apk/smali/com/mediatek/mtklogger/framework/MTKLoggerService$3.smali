.class Lcom/mediatek/mtklogger/framework/MTKLoggerService$3;
.super Landroid/os/Handler;
.source "MTKLoggerService.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/mediatek/mtklogger/framework/MTKLoggerService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/mediatek/mtklogger/framework/MTKLoggerService;


# direct methods
.method constructor <init>(Lcom/mediatek/mtklogger/framework/MTKLoggerService;Landroid/os/Looper;)V
    .locals 0
    .param p2    # Landroid/os/Looper;

    iput-object p1, p0, Lcom/mediatek/mtklogger/framework/MTKLoggerService$3;->this$0:Lcom/mediatek/mtklogger/framework/MTKLoggerService;

    invoke-direct {p0, p2}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 6
    .param p1    # Landroid/os/Message;

    const/4 v5, 0x0

    iget v1, p1, Landroid/os/Message;->what:I

    const-string v2, "MTKLogger/MTKLoggerService"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, " mStartStopRespHandler receive message, what="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ", arg1="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget v4, p1, Landroid/os/Message;->arg1:I

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ", arg2="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget v4, p1, Landroid/os/Message;->arg2:I

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/mediatek/mtklogger/utils/Utils;->logd(Ljava/lang/String;Ljava/lang/String;)V

    const/4 v2, 0x2

    if-ne v1, v2, :cond_0

    const-string v2, "MTKLogger/MTKLoggerService"

    const-string v3, "At boot up/IPO time, waiting SD card time out."

    invoke-static {v2, v3}, Lcom/mediatek/mtklogger/utils/Utils;->logw(Ljava/lang/String;Ljava/lang/String;)V

    # setter for: Lcom/mediatek/mtklogger/framework/MTKLoggerService;->isWaitingSDReady:Z
    invoke-static {v5}, Lcom/mediatek/mtklogger/framework/MTKLoggerService;->access$502(Z)Z

    iget-object v2, p0, Lcom/mediatek/mtklogger/framework/MTKLoggerService$3;->this$0:Lcom/mediatek/mtklogger/framework/MTKLoggerService;

    const-string v3, "sd_timeout"

    # invokes: Lcom/mediatek/mtklogger/framework/MTKLoggerService;->changeLogRunningStatus(ZLjava/lang/String;)V
    invoke-static {v2, v5, v3}, Lcom/mediatek/mtklogger/framework/MTKLoggerService;->access$300(Lcom/mediatek/mtklogger/framework/MTKLoggerService;ZLjava/lang/String;)V

    :goto_0
    return-void

    :cond_0
    const/16 v2, 0x3e8

    if-gt v1, v2, :cond_1

    const/4 v2, 0x1

    if-ne v1, v2, :cond_2

    :cond_1
    iget-object v2, p0, Lcom/mediatek/mtklogger/framework/MTKLoggerService$3;->this$0:Lcom/mediatek/mtklogger/framework/MTKLoggerService;

    # invokes: Lcom/mediatek/mtklogger/framework/MTKLoggerService;->handleLogStateChangeMsg(Landroid/os/Handler;Landroid/os/Message;)V
    invoke-static {v2, p0, p1}, Lcom/mediatek/mtklogger/framework/MTKLoggerService;->access$1200(Lcom/mediatek/mtklogger/framework/MTKLoggerService;Landroid/os/Handler;Landroid/os/Message;)V

    goto :goto_0

    :cond_2
    const/4 v2, 0x3

    if-ne v1, v2, :cond_3

    iget v0, p1, Landroid/os/Message;->arg1:I

    iget-object v2, p0, Lcom/mediatek/mtklogger/framework/MTKLoggerService$3;->this$0:Lcom/mediatek/mtklogger/framework/MTKLoggerService;

    # invokes: Lcom/mediatek/mtklogger/framework/MTKLoggerService;->handleGlobalRunningStageChange(I)V
    invoke-static {v2, v0}, Lcom/mediatek/mtklogger/framework/MTKLoggerService;->access$1300(Lcom/mediatek/mtklogger/framework/MTKLoggerService;I)V

    goto :goto_0

    :cond_3
    const-string v2, "MTKLogger/MTKLoggerService"

    const-string v3, "Unknown message"

    invoke-static {v2, v3}, Lcom/mediatek/mtklogger/utils/Utils;->loge(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method
