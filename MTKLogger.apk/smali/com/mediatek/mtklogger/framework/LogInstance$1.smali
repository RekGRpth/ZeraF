.class final Lcom/mediatek/mtklogger/framework/LogInstance$1;
.super Landroid/os/Handler;
.source "LogInstance.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/mediatek/mtklogger/framework/LogInstance;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = null
.end annotation


# direct methods
.method constructor <init>(Landroid/os/Looper;)V
    .locals 0
    .param p1    # Landroid/os/Looper;

    invoke-direct {p0, p1}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 11
    .param p1    # Landroid/os/Message;

    if-eqz p1, :cond_0

    sget-object v7, Lcom/mediatek/mtklogger/framework/LogInstance;->RUNNING_NOTIFICATION_MAP:Ljava/util/HashMap;

    monitor-enter v7

    :try_start_0
    iget v0, p1, Landroid/os/Message;->what:I

    sget-object v6, Lcom/mediatek/mtklogger/framework/LogInstance;->RUNNING_NOTIFICATION_MAP:Ljava/util/HashMap;

    invoke-virtual {v6}, Ljava/util/HashMap;->size()I

    move-result v6

    if-gtz v6, :cond_1

    const/4 v6, 0x0

    const/4 v8, 0x0

    const/4 v9, 0x0

    const/4 v10, 0x0

    invoke-static {v6, v8, v9, v0, v10}, Lcom/mediatek/mtklogger/framework/LogInstance;->showLogStatusInNotificationBar(ZLjava/lang/String;Ljava/lang/String;ILjava/lang/String;)V

    :goto_0
    # getter for: Lcom/mediatek/mtklogger/framework/LogInstance;->PENDING_ON_NOTIFICATION_MAP:Ljava/util/HashMap;
    invoke-static {}, Lcom/mediatek/mtklogger/framework/LogInstance;->access$100()Ljava/util/HashMap;

    move-result-object v6

    invoke-virtual {v6}, Ljava/util/HashMap;->clear()V

    # getter for: Lcom/mediatek/mtklogger/framework/LogInstance;->PENDING_OFF_NOTIFICATION_MAP:Ljava/util/HashMap;
    invoke-static {}, Lcom/mediatek/mtklogger/framework/LogInstance;->access$200()Ljava/util/HashMap;

    move-result-object v6

    invoke-virtual {v6}, Ljava/util/HashMap;->clear()V

    monitor-exit v7

    :cond_0
    return-void

    :cond_1
    sget-object v6, Lcom/mediatek/mtklogger/framework/LogInstance;->mContext:Landroid/content/Context;

    const v8, 0x7f070004

    invoke-virtual {v6, v8}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    sget-object v6, Lcom/mediatek/mtklogger/framework/LogInstance;->RUNNING_NOTIFICATION_MAP:Ljava/util/HashMap;

    const v8, 0x7f070005

    const v9, 0x7f070006

    # invokes: Lcom/mediatek/mtklogger/framework/LogInstance;->getLogStateDescStr(Ljava/util/Map;II)Ljava/lang/String;
    invoke-static {v6, v8, v9}, Lcom/mediatek/mtklogger/framework/LogInstance;->access$000(Ljava/util/Map;II)Ljava/lang/String;

    move-result-object v1

    # getter for: Lcom/mediatek/mtklogger/framework/LogInstance;->PENDING_ON_NOTIFICATION_MAP:Ljava/util/HashMap;
    invoke-static {}, Lcom/mediatek/mtklogger/framework/LogInstance;->access$100()Ljava/util/HashMap;

    move-result-object v6

    const v8, 0x7f070005

    const v9, 0x7f070006

    # invokes: Lcom/mediatek/mtklogger/framework/LogInstance;->getLogStateDescStr(Ljava/util/Map;II)Ljava/lang/String;
    invoke-static {v6, v8, v9}, Lcom/mediatek/mtklogger/framework/LogInstance;->access$000(Ljava/util/Map;II)Ljava/lang/String;

    move-result-object v4

    # getter for: Lcom/mediatek/mtklogger/framework/LogInstance;->PENDING_OFF_NOTIFICATION_MAP:Ljava/util/HashMap;
    invoke-static {}, Lcom/mediatek/mtklogger/framework/LogInstance;->access$200()Ljava/util/HashMap;

    move-result-object v6

    const v8, 0x7f070007

    const v9, 0x7f070008

    # invokes: Lcom/mediatek/mtklogger/framework/LogInstance;->getLogStateDescStr(Ljava/util/Map;II)Ljava/lang/String;
    invoke-static {v6, v8, v9}, Lcom/mediatek/mtklogger/framework/LogInstance;->access$000(Ljava/util/Map;II)Ljava/lang/String;

    move-result-object v3

    const-string v5, ""

    invoke-static {v4}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v6

    if-nez v6, :cond_2

    invoke-static {v3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v6

    if-nez v6, :cond_2

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v6, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v8, ", "

    invoke-virtual {v6, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    :goto_1
    const/4 v6, 0x1

    invoke-static {v6, v2, v1, v0, v5}, Lcom/mediatek/mtklogger/framework/LogInstance;->showLogStatusInNotificationBar(ZLjava/lang/String;Ljava/lang/String;ILjava/lang/String;)V

    goto :goto_0

    :catchall_0
    move-exception v6

    monitor-exit v7
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v6

    :cond_2
    :try_start_1
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v6, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result-object v5

    goto :goto_1
.end method
