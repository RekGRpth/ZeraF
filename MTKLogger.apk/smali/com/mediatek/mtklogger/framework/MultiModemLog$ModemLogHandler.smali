.class Lcom/mediatek/mtklogger/framework/MultiModemLog$ModemLogHandler;
.super Lcom/mediatek/mtklogger/framework/LogInstance$LogHandler;
.source "MultiModemLog.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/mediatek/mtklogger/framework/MultiModemLog;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = "ModemLogHandler"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/mediatek/mtklogger/framework/MultiModemLog;


# direct methods
.method constructor <init>(Lcom/mediatek/mtklogger/framework/MultiModemLog;)V
    .locals 0

    iput-object p1, p0, Lcom/mediatek/mtklogger/framework/MultiModemLog$ModemLogHandler;->this$0:Lcom/mediatek/mtklogger/framework/MultiModemLog;

    invoke-direct {p0, p1}, Lcom/mediatek/mtklogger/framework/LogInstance$LogHandler;-><init>(Lcom/mediatek/mtklogger/framework/LogInstance;)V

    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 22
    .param p1    # Landroid/os/Message;

    const-string v18, "MTKLogger/MultiModemLog"

    new-instance v19, Ljava/lang/StringBuilder;

    invoke-direct/range {v19 .. v19}, Ljava/lang/StringBuilder;-><init>()V

    const-string v20, "Handle receive message, what="

    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    move-object/from16 v0, p1

    iget v0, v0, Landroid/os/Message;->what:I

    move/from16 v20, v0

    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v19

    const-string v20, ", msg.arg1=["

    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    move-object/from16 v0, p1

    iget v0, v0, Landroid/os/Message;->arg1:I

    move/from16 v20, v0

    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v19

    const-string v20, "]"

    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    invoke-virtual/range {v19 .. v19}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v19

    invoke-static/range {v18 .. v19}, Lcom/mediatek/mtklogger/utils/Utils;->logi(Ljava/lang/String;Ljava/lang/String;)V

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/mediatek/mtklogger/framework/MultiModemLog$ModemLogHandler;->this$0:Lcom/mediatek/mtklogger/framework/MultiModemLog;

    move-object/from16 v18, v0

    # getter for: Lcom/mediatek/mtklogger/framework/MultiModemLog;->bConnected:Z
    invoke-static/range {v18 .. v18}, Lcom/mediatek/mtklogger/framework/MultiModemLog;->access$100(Lcom/mediatek/mtklogger/framework/MultiModemLog;)Z

    move-result v18

    if-nez v18, :cond_1

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/mediatek/mtklogger/framework/MultiModemLog$ModemLogHandler;->this$0:Lcom/mediatek/mtklogger/framework/MultiModemLog;

    move-object/from16 v18, v0

    # getter for: Lcom/mediatek/mtklogger/framework/MultiModemLog;->mModemManager:Lcom/mediatek/mtklogger/framework/MultiModemLog$ModemManager;
    invoke-static/range {v18 .. v18}, Lcom/mediatek/mtklogger/framework/MultiModemLog;->access$000(Lcom/mediatek/mtklogger/framework/MultiModemLog;)Lcom/mediatek/mtklogger/framework/MultiModemLog$ModemManager;

    move-result-object v18

    if-nez v18, :cond_0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/mediatek/mtklogger/framework/MultiModemLog$ModemLogHandler;->this$0:Lcom/mediatek/mtklogger/framework/MultiModemLog;

    move-object/from16 v18, v0

    new-instance v19, Lcom/mediatek/mtklogger/framework/MultiModemLog$ModemManager;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/mediatek/mtklogger/framework/MultiModemLog$ModemLogHandler;->this$0:Lcom/mediatek/mtklogger/framework/MultiModemLog;

    move-object/from16 v20, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/mediatek/mtklogger/framework/MultiModemLog$ModemLogHandler;->this$0:Lcom/mediatek/mtklogger/framework/MultiModemLog;

    move-object/from16 v21, v0

    move-object/from16 v0, v21

    iget-object v0, v0, Lcom/mediatek/mtklogger/framework/MultiModemLog;->mHandler:Lcom/mediatek/mtklogger/framework/LogInstance$LogHandler;

    move-object/from16 v21, v0

    invoke-direct/range {v19 .. v21}, Lcom/mediatek/mtklogger/framework/MultiModemLog$ModemManager;-><init>(Lcom/mediatek/mtklogger/framework/MultiModemLog;Landroid/os/Handler;)V

    # setter for: Lcom/mediatek/mtklogger/framework/MultiModemLog;->mModemManager:Lcom/mediatek/mtklogger/framework/MultiModemLog$ModemManager;
    invoke-static/range {v18 .. v19}, Lcom/mediatek/mtklogger/framework/MultiModemLog;->access$002(Lcom/mediatek/mtklogger/framework/MultiModemLog;Lcom/mediatek/mtklogger/framework/MultiModemLog$ModemManager;)Lcom/mediatek/mtklogger/framework/MultiModemLog$ModemManager;

    :cond_0
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/mediatek/mtklogger/framework/MultiModemLog$ModemLogHandler;->this$0:Lcom/mediatek/mtklogger/framework/MultiModemLog;

    move-object/from16 v18, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/mediatek/mtklogger/framework/MultiModemLog$ModemLogHandler;->this$0:Lcom/mediatek/mtklogger/framework/MultiModemLog;

    move-object/from16 v19, v0

    # getter for: Lcom/mediatek/mtklogger/framework/MultiModemLog;->mModemManager:Lcom/mediatek/mtklogger/framework/MultiModemLog$ModemManager;
    invoke-static/range {v19 .. v19}, Lcom/mediatek/mtklogger/framework/MultiModemLog;->access$000(Lcom/mediatek/mtklogger/framework/MultiModemLog;)Lcom/mediatek/mtklogger/framework/MultiModemLog$ModemManager;

    move-result-object v19

    # invokes: Lcom/mediatek/mtklogger/framework/MultiModemLog$ModemManager;->initLogConnection()Z
    invoke-static/range {v19 .. v19}, Lcom/mediatek/mtklogger/framework/MultiModemLog$ModemManager;->access$200(Lcom/mediatek/mtklogger/framework/MultiModemLog$ModemManager;)Z

    move-result v19

    # setter for: Lcom/mediatek/mtklogger/framework/MultiModemLog;->bConnected:Z
    invoke-static/range {v18 .. v19}, Lcom/mediatek/mtklogger/framework/MultiModemLog;->access$102(Lcom/mediatek/mtklogger/framework/MultiModemLog;Z)Z

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/mediatek/mtklogger/framework/MultiModemLog$ModemLogHandler;->this$0:Lcom/mediatek/mtklogger/framework/MultiModemLog;

    move-object/from16 v18, v0

    const/16 v19, -0x1

    # setter for: Lcom/mediatek/mtklogger/framework/MultiModemLog;->mCurrentStatus:I
    invoke-static/range {v18 .. v19}, Lcom/mediatek/mtklogger/framework/MultiModemLog;->access$302(Lcom/mediatek/mtklogger/framework/MultiModemLog;I)I

    :cond_1
    move-object/from16 v0, p1

    iget-object v5, v0, Landroid/os/Message;->obj:Ljava/lang/Object;

    const/4 v4, 0x0

    if-eqz v5, :cond_2

    instance-of v0, v5, Ljava/lang/String;

    move/from16 v18, v0

    if-eqz v18, :cond_2

    move-object v4, v5

    check-cast v4, Ljava/lang/String;

    :cond_2
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/mediatek/mtklogger/framework/MultiModemLog$ModemLogHandler;->this$0:Lcom/mediatek/mtklogger/framework/MultiModemLog;

    move-object/from16 v18, v0

    # getter for: Lcom/mediatek/mtklogger/framework/MultiModemLog;->bConnected:Z
    invoke-static/range {v18 .. v18}, Lcom/mediatek/mtklogger/framework/MultiModemLog;->access$100(Lcom/mediatek/mtklogger/framework/MultiModemLog;)Z

    move-result v18

    if-eqz v18, :cond_3

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/mediatek/mtklogger/framework/MultiModemLog$ModemLogHandler;->this$0:Lcom/mediatek/mtklogger/framework/MultiModemLog;

    move-object/from16 v18, v0

    move-object/from16 v0, v18

    iget-object v0, v0, Lcom/mediatek/mtklogger/framework/MultiModemLog;->mSharedPreferences:Landroid/content/SharedPreferences;

    move-object/from16 v18, v0

    const-string v19, "md_assert_file_path"

    const-string v20, ""

    invoke-interface/range {v18 .. v20}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v18

    if-nez v18, :cond_3

    const-string v18, ";"

    move-object/from16 v0, v18

    invoke-virtual {v2, v0}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v17

    move-object/from16 v0, v17

    array-length v0, v0

    move/from16 v18, v0

    const/16 v19, 0x2

    move/from16 v0, v18

    move/from16 v1, v19

    if-ne v0, v1, :cond_3

    const-string v18, "MTKLogger/MultiModemLog"

    new-instance v19, Ljava/lang/StringBuilder;

    invoke-direct/range {v19 .. v19}, Ljava/lang/StringBuilder;-><init>()V

    const-string v20, "The former modem reset dialog was killed abnormally, re-show it, assert file path="

    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    move-object/from16 v0, v19

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    invoke-virtual/range {v19 .. v19}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v19

    invoke-static/range {v18 .. v19}, Lcom/mediatek/mtklogger/utils/Utils;->logi(Ljava/lang/String;Ljava/lang/String;)V

    :try_start_0
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/mediatek/mtklogger/framework/MultiModemLog$ModemLogHandler;->this$0:Lcom/mediatek/mtklogger/framework/MultiModemLog;

    move-object/from16 v18, v0

    const/16 v19, 0x0

    aget-object v19, v17, v19

    invoke-static/range {v19 .. v19}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v19

    const/16 v20, 0x1

    aget-object v20, v17, v20

    # invokes: Lcom/mediatek/mtklogger/framework/MultiModemLog;->showMemoryDumpDoneDialog(ILjava/lang/String;)V
    invoke-static/range {v18 .. v20}, Lcom/mediatek/mtklogger/framework/MultiModemLog;->access$400(Lcom/mediatek/mtklogger/framework/MultiModemLog;ILjava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_0

    :cond_3
    :goto_0
    move-object/from16 v0, p1

    iget v0, v0, Landroid/os/Message;->what:I

    move/from16 v18, v0

    sparse-switch v18, :sswitch_data_0

    const-string v18, "MTKLogger/MultiModemLog"

    new-instance v19, Ljava/lang/StringBuilder;

    invoke-direct/range {v19 .. v19}, Ljava/lang/StringBuilder;-><init>()V

    const-string v20, "Not supported message: "

    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    move-object/from16 v0, p1

    iget v0, v0, Landroid/os/Message;->what:I

    move/from16 v20, v0

    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v19

    invoke-virtual/range {v19 .. v19}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v19

    invoke-static/range {v18 .. v19}, Lcom/mediatek/mtklogger/utils/Utils;->logw(Ljava/lang/String;Ljava/lang/String;)V

    :cond_4
    :goto_1
    return-void

    :catch_0
    move-exception v9

    const-string v18, "MTKLogger/MultiModemLog"

    const-string v19, "Cached modem assert file format invalid"

    invoke-static/range {v18 .. v19}, Lcom/mediatek/mtklogger/utils/Utils;->loge(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    :sswitch_0
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/mediatek/mtklogger/framework/MultiModemLog$ModemLogHandler;->this$0:Lcom/mediatek/mtklogger/framework/MultiModemLog;

    move-object/from16 v18, v0

    # getter for: Lcom/mediatek/mtklogger/framework/MultiModemLog;->bConnected:Z
    invoke-static/range {v18 .. v18}, Lcom/mediatek/mtklogger/framework/MultiModemLog;->access$100(Lcom/mediatek/mtklogger/framework/MultiModemLog;)Z

    move-result v18

    if-nez v18, :cond_6

    const-string v18, "MTKLogger/MultiModemLog"

    const-string v19, "Fail to establish connection to native layer."

    invoke-static/range {v18 .. v19}, Lcom/mediatek/mtklogger/utils/Utils;->loge(Ljava/lang/String;Ljava/lang/String;)V

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/mediatek/mtklogger/framework/MultiModemLog$ModemLogHandler;->this$0:Lcom/mediatek/mtklogger/framework/MultiModemLog;

    move-object/from16 v18, v0

    move-object/from16 v0, v18

    iget-object v0, v0, Lcom/mediatek/mtklogger/framework/MultiModemLog;->mSharedPreferences:Landroid/content/SharedPreferences;

    move-object/from16 v18, v0

    if-eqz v18, :cond_5

    const/16 v18, 0x1

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/mediatek/mtklogger/framework/MultiModemLog$ModemLogHandler;->this$0:Lcom/mediatek/mtklogger/framework/MultiModemLog;

    move-object/from16 v19, v0

    move-object/from16 v0, v19

    iget-object v0, v0, Lcom/mediatek/mtklogger/framework/MultiModemLog;->mSharedPreferences:Landroid/content/SharedPreferences;

    move-object/from16 v19, v0

    const-string v20, "modemlog_enable"

    const/16 v21, 0x1

    invoke-interface/range {v19 .. v21}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v19

    move/from16 v0, v18

    move/from16 v1, v19

    if-ne v0, v1, :cond_5

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/mediatek/mtklogger/framework/MultiModemLog$ModemLogHandler;->this$0:Lcom/mediatek/mtklogger/framework/MultiModemLog;

    move-object/from16 v18, v0

    move-object/from16 v0, v18

    iget-object v0, v0, Lcom/mediatek/mtklogger/framework/MultiModemLog;->mSharedPreferences:Landroid/content/SharedPreferences;

    move-object/from16 v18, v0

    invoke-interface/range {v18 .. v18}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v18

    const-string v19, "modemlog_enable"

    const/16 v20, 0x0

    invoke-interface/range {v18 .. v20}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    move-result-object v18

    invoke-interface/range {v18 .. v18}, Landroid/content/SharedPreferences$Editor;->commit()Z

    :cond_5
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/mediatek/mtklogger/framework/MultiModemLog$ModemLogHandler;->this$0:Lcom/mediatek/mtklogger/framework/MultiModemLog;

    move-object/from16 v18, v0

    const/16 v19, 0x0

    const-string v20, "1"

    # invokes: Lcom/mediatek/mtklogger/framework/MultiModemLog;->notifyServiceStatus(ILjava/lang/String;)V
    invoke-static/range {v18 .. v20}, Lcom/mediatek/mtklogger/framework/MultiModemLog;->access$500(Lcom/mediatek/mtklogger/framework/MultiModemLog;ILjava/lang/String;)V

    goto :goto_1

    :cond_6
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/mediatek/mtklogger/framework/MultiModemLog$ModemLogHandler;->this$0:Lcom/mediatek/mtklogger/framework/MultiModemLog;

    move-object/from16 v18, v0

    invoke-virtual/range {v18 .. v18}, Lcom/mediatek/mtklogger/framework/MultiModemLog;->getLogStorageState()I

    move-result v11

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/mediatek/mtklogger/framework/MultiModemLog$ModemLogHandler;->this$0:Lcom/mediatek/mtklogger/framework/MultiModemLog;

    move-object/from16 v18, v0

    # invokes: Lcom/mediatek/mtklogger/framework/MultiModemLog;->getCurrentMode()Ljava/lang/String;
    invoke-static/range {v18 .. v18}, Lcom/mediatek/mtklogger/framework/MultiModemLog;->access$600(Lcom/mediatek/mtklogger/framework/MultiModemLog;)Ljava/lang/String;

    move-result-object v7

    const-string v18, "1"

    move-object/from16 v0, v18

    invoke-virtual {v0, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v18

    if-nez v18, :cond_9

    const/16 v18, 0x1

    move/from16 v0, v18

    if-eq v11, v0, :cond_9

    const-string v18, "MTKLogger/MultiModemLog"

    const-string v19, "Log storage is not ready yet."

    invoke-static/range {v18 .. v19}, Lcom/mediatek/mtklogger/utils/Utils;->loge(Ljava/lang/String;Ljava/lang/String;)V

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/mediatek/mtklogger/framework/MultiModemLog$ModemLogHandler;->this$0:Lcom/mediatek/mtklogger/framework/MultiModemLog;

    move-object/from16 v18, v0

    move-object/from16 v0, v18

    iget-object v0, v0, Lcom/mediatek/mtklogger/framework/MultiModemLog;->mSharedPreferences:Landroid/content/SharedPreferences;

    move-object/from16 v18, v0

    if-eqz v18, :cond_7

    const/16 v18, 0x1

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/mediatek/mtklogger/framework/MultiModemLog$ModemLogHandler;->this$0:Lcom/mediatek/mtklogger/framework/MultiModemLog;

    move-object/from16 v19, v0

    move-object/from16 v0, v19

    iget-object v0, v0, Lcom/mediatek/mtklogger/framework/MultiModemLog;->mSharedPreferences:Landroid/content/SharedPreferences;

    move-object/from16 v19, v0

    const-string v20, "modemlog_enable"

    const/16 v21, 0x1

    invoke-interface/range {v19 .. v21}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v19

    move/from16 v0, v18

    move/from16 v1, v19

    if-ne v0, v1, :cond_7

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/mediatek/mtklogger/framework/MultiModemLog$ModemLogHandler;->this$0:Lcom/mediatek/mtklogger/framework/MultiModemLog;

    move-object/from16 v18, v0

    move-object/from16 v0, v18

    iget-object v0, v0, Lcom/mediatek/mtklogger/framework/MultiModemLog;->mSharedPreferences:Landroid/content/SharedPreferences;

    move-object/from16 v18, v0

    invoke-interface/range {v18 .. v18}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v18

    const-string v19, "modemlog_enable"

    const/16 v20, 0x0

    invoke-interface/range {v18 .. v20}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    move-result-object v18

    invoke-interface/range {v18 .. v18}, Landroid/content/SharedPreferences$Editor;->commit()Z

    :cond_7
    const/16 v18, -0x1

    move/from16 v0, v18

    if-ne v11, v0, :cond_8

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/mediatek/mtklogger/framework/MultiModemLog$ModemLogHandler;->this$0:Lcom/mediatek/mtklogger/framework/MultiModemLog;

    move-object/from16 v18, v0

    const/16 v19, 0x0

    const-string v20, "2"

    # invokes: Lcom/mediatek/mtklogger/framework/MultiModemLog;->notifyServiceStatus(ILjava/lang/String;)V
    invoke-static/range {v18 .. v20}, Lcom/mediatek/mtklogger/framework/MultiModemLog;->access$500(Lcom/mediatek/mtklogger/framework/MultiModemLog;ILjava/lang/String;)V

    goto/16 :goto_1

    :cond_8
    const/16 v18, -0x2

    move/from16 v0, v18

    if-ne v11, v0, :cond_4

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/mediatek/mtklogger/framework/MultiModemLog$ModemLogHandler;->this$0:Lcom/mediatek/mtklogger/framework/MultiModemLog;

    move-object/from16 v18, v0

    const/16 v19, 0x0

    const-string v20, "3"

    # invokes: Lcom/mediatek/mtklogger/framework/MultiModemLog;->notifyServiceStatus(ILjava/lang/String;)V
    invoke-static/range {v18 .. v20}, Lcom/mediatek/mtklogger/framework/MultiModemLog;->access$500(Lcom/mediatek/mtklogger/framework/MultiModemLog;ILjava/lang/String;)V

    goto/16 :goto_1

    :cond_9
    const-wide/16 v18, 0xc8

    :try_start_1
    invoke-static/range {v18 .. v19}, Ljava/lang/Thread;->sleep(J)V

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/mediatek/mtklogger/framework/MultiModemLog$ModemLogHandler;->this$0:Lcom/mediatek/mtklogger/framework/MultiModemLog;

    move-object/from16 v18, v0

    # getter for: Lcom/mediatek/mtklogger/framework/MultiModemLog;->mModemManager:Lcom/mediatek/mtklogger/framework/MultiModemLog$ModemManager;
    invoke-static/range {v18 .. v18}, Lcom/mediatek/mtklogger/framework/MultiModemLog;->access$000(Lcom/mediatek/mtklogger/framework/MultiModemLog;)Lcom/mediatek/mtklogger/framework/MultiModemLog$ModemManager;

    move-result-object v18

    new-instance v19, Ljava/lang/StringBuilder;

    invoke-direct/range {v19 .. v19}, Ljava/lang/StringBuilder;-><init>()V

    const-string v20, "start,"

    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    move-object/from16 v0, v19

    invoke-virtual {v0, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    invoke-virtual/range {v19 .. v19}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v19

    # invokes: Lcom/mediatek/mtklogger/framework/MultiModemLog$ModemManager;->sendCmd(Ljava/lang/String;)Z
    invoke-static/range {v18 .. v19}, Lcom/mediatek/mtklogger/framework/MultiModemLog$ModemManager;->access$700(Lcom/mediatek/mtklogger/framework/MultiModemLog$ModemManager;Ljava/lang/String;)Z

    const-wide/16 v18, 0x1f4

    invoke-static/range {v18 .. v19}, Ljava/lang/Thread;->sleep(J)V
    :try_end_1
    .catch Ljava/lang/InterruptedException; {:try_start_1 .. :try_end_1} :catch_1

    :goto_2
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/mediatek/mtklogger/framework/MultiModemLog$ModemLogHandler;->this$0:Lcom/mediatek/mtklogger/framework/MultiModemLog;

    move-object/from16 v18, v0

    # getter for: Lcom/mediatek/mtklogger/framework/MultiModemLog;->mModemManager:Lcom/mediatek/mtklogger/framework/MultiModemLog$ModemManager;
    invoke-static/range {v18 .. v18}, Lcom/mediatek/mtklogger/framework/MultiModemLog;->access$000(Lcom/mediatek/mtklogger/framework/MultiModemLog;)Lcom/mediatek/mtklogger/framework/MultiModemLog$ModemManager;

    move-result-object v18

    const-string v19, "resume"

    # invokes: Lcom/mediatek/mtklogger/framework/MultiModemLog$ModemManager;->sendCmd(Ljava/lang/String;)Z
    invoke-static/range {v18 .. v19}, Lcom/mediatek/mtklogger/framework/MultiModemLog$ModemManager;->access$700(Lcom/mediatek/mtklogger/framework/MultiModemLog$ModemManager;Ljava/lang/String;)Z

    move-result v18

    if-eqz v18, :cond_4

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/mediatek/mtklogger/framework/MultiModemLog$ModemLogHandler;->this$0:Lcom/mediatek/mtklogger/framework/MultiModemLog;

    move-object/from16 v18, v0

    move-object/from16 v0, v18

    iget-object v0, v0, Lcom/mediatek/mtklogger/framework/MultiModemLog;->mSharedPreferences:Landroid/content/SharedPreferences;

    move-object/from16 v18, v0

    invoke-interface/range {v18 .. v18}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v18

    const-string v19, "modemlog_enable"

    const/16 v20, 0x1

    invoke-interface/range {v18 .. v20}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    move-result-object v18

    invoke-interface/range {v18 .. v18}, Landroid/content/SharedPreferences$Editor;->commit()Z

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/mediatek/mtklogger/framework/MultiModemLog$ModemLogHandler;->this$0:Lcom/mediatek/mtklogger/framework/MultiModemLog;

    move-object/from16 v18, v0

    const/16 v19, 0x1

    const-string v20, ""

    # invokes: Lcom/mediatek/mtklogger/framework/MultiModemLog;->notifyServiceStatus(ILjava/lang/String;)V
    invoke-static/range {v18 .. v20}, Lcom/mediatek/mtklogger/framework/MultiModemLog;->access$500(Lcom/mediatek/mtklogger/framework/MultiModemLog;ILjava/lang/String;)V

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/mediatek/mtklogger/framework/MultiModemLog$ModemLogHandler;->this$0:Lcom/mediatek/mtklogger/framework/MultiModemLog;

    move-object/from16 v18, v0

    # invokes: Lcom/mediatek/mtklogger/framework/MultiModemLog;->runMonitoringLogSizeThread()V
    invoke-static/range {v18 .. v18}, Lcom/mediatek/mtklogger/framework/MultiModemLog;->access$800(Lcom/mediatek/mtklogger/framework/MultiModemLog;)V

    goto/16 :goto_1

    :catch_1
    move-exception v9

    invoke-virtual {v9}, Ljava/lang/InterruptedException;->printStackTrace()V

    goto :goto_2

    :sswitch_1
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/mediatek/mtklogger/framework/MultiModemLog$ModemLogHandler;->this$0:Lcom/mediatek/mtklogger/framework/MultiModemLog;

    move-object/from16 v18, v0

    # getter for: Lcom/mediatek/mtklogger/framework/MultiModemLog;->bConnected:Z
    invoke-static/range {v18 .. v18}, Lcom/mediatek/mtklogger/framework/MultiModemLog;->access$100(Lcom/mediatek/mtklogger/framework/MultiModemLog;)Z

    move-result v18

    if-eqz v18, :cond_d

    const/4 v6, 0x0

    const-string v18, "MTKLogger/MultiModemLog"

    new-instance v19, Ljava/lang/StringBuilder;

    invoke-direct/range {v19 .. v19}, Ljava/lang/StringBuilder;-><init>()V

    const-string v20, "Receive stop command, stop reason="

    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    move-object/from16 v0, v19

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    invoke-virtual/range {v19 .. v19}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v19

    invoke-static/range {v18 .. v19}, Lcom/mediatek/mtklogger/utils/Utils;->logd(Ljava/lang/String;Ljava/lang/String;)V

    const-string v18, "ipo_shutdown"

    move-object/from16 v0, v18

    invoke-virtual {v0, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v18

    if-eqz v18, :cond_b

    const-string v18, "sys.ipo.pwrdncap"

    const-string v19, "-1"

    invoke-static/range {v18 .. v19}, Landroid/os/SystemProperties;->get(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v10

    const-string v18, "MTKLogger/MultiModemLog"

    new-instance v19, Ljava/lang/StringBuilder;

    invoke-direct/range {v19 .. v19}, Ljava/lang/StringBuilder;-><init>()V

    const-string v20, "IPO flag for modem log is "

    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    move-object/from16 v0, v19

    invoke-virtual {v0, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    invoke-virtual/range {v19 .. v19}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v19

    invoke-static/range {v18 .. v19}, Lcom/mediatek/mtklogger/utils/Utils;->logv(Ljava/lang/String;Ljava/lang/String;)V

    const-string v18, "1"

    move-object/from16 v0, v18

    invoke-virtual {v0, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v18

    if-nez v18, :cond_a

    const-string v18, "3"

    move-object/from16 v0, v18

    invoke-virtual {v0, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v18

    if-eqz v18, :cond_b

    :cond_a
    const-string v18, "MTKLogger/MultiModemLog"

    const-string v19, "According to IPO flag, do not need to stop modem log when IPO shutdown"

    invoke-static/range {v18 .. v19}, Lcom/mediatek/mtklogger/utils/Utils;->logi(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_1

    :cond_b
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/mediatek/mtklogger/framework/MultiModemLog$ModemLogHandler;->this$0:Lcom/mediatek/mtklogger/framework/MultiModemLog;

    move-object/from16 v18, v0

    # getter for: Lcom/mediatek/mtklogger/framework/MultiModemLog;->mModemManager:Lcom/mediatek/mtklogger/framework/MultiModemLog$ModemManager;
    invoke-static/range {v18 .. v18}, Lcom/mediatek/mtklogger/framework/MultiModemLog;->access$000(Lcom/mediatek/mtklogger/framework/MultiModemLog;)Lcom/mediatek/mtklogger/framework/MultiModemLog$ModemManager;

    move-result-object v18

    const-string v19, "pause"

    # invokes: Lcom/mediatek/mtklogger/framework/MultiModemLog$ModemManager;->sendCmd(Ljava/lang/String;)Z
    invoke-static/range {v18 .. v19}, Lcom/mediatek/mtklogger/framework/MultiModemLog$ModemManager;->access$700(Lcom/mediatek/mtklogger/framework/MultiModemLog$ModemManager;Ljava/lang/String;)Z

    move-result v6

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/mediatek/mtklogger/framework/MultiModemLog$ModemLogHandler;->this$0:Lcom/mediatek/mtklogger/framework/MultiModemLog;

    move-object/from16 v18, v0

    const/16 v19, 0x1

    # setter for: Lcom/mediatek/mtklogger/framework/MultiModemLog;->mMonitorThreadStop:Z
    invoke-static/range {v18 .. v19}, Lcom/mediatek/mtklogger/framework/MultiModemLog;->access$902(Lcom/mediatek/mtklogger/framework/MultiModemLog;Z)Z

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/mediatek/mtklogger/framework/MultiModemLog$ModemLogHandler;->this$0:Lcom/mediatek/mtklogger/framework/MultiModemLog;

    move-object/from16 v18, v0

    move-object/from16 v0, v18

    iget-object v0, v0, Lcom/mediatek/mtklogger/framework/MultiModemLog;->waitNextClearLogCheck:Ljava/lang/Object;

    move-object/from16 v19, v0

    monitor-enter v19

    :try_start_2
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/mediatek/mtklogger/framework/MultiModemLog$ModemLogHandler;->this$0:Lcom/mediatek/mtklogger/framework/MultiModemLog;

    move-object/from16 v18, v0

    move-object/from16 v0, v18

    iget-object v0, v0, Lcom/mediatek/mtklogger/framework/MultiModemLog;->waitNextClearLogCheck:Ljava/lang/Object;

    move-object/from16 v18, v0

    invoke-virtual/range {v18 .. v18}, Ljava/lang/Object;->notifyAll()V

    monitor-exit v19
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    if-nez v6, :cond_c

    const-string v18, "MTKLogger/MultiModemLog"

    const-string v19, "Send stop command to native layer fail, maybe connection has already be lost."

    invoke-static/range {v18 .. v19}, Lcom/mediatek/mtklogger/utils/Utils;->loge(Ljava/lang/String;Ljava/lang/String;)V

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/mediatek/mtklogger/framework/MultiModemLog$ModemLogHandler;->this$0:Lcom/mediatek/mtklogger/framework/MultiModemLog;

    move-object/from16 v18, v0

    const/16 v19, 0x0

    const-string v20, "4"

    # invokes: Lcom/mediatek/mtklogger/framework/MultiModemLog;->notifyServiceStatus(ILjava/lang/String;)V
    invoke-static/range {v18 .. v20}, Lcom/mediatek/mtklogger/framework/MultiModemLog;->access$500(Lcom/mediatek/mtklogger/framework/MultiModemLog;ILjava/lang/String;)V

    goto/16 :goto_1

    :catchall_0
    move-exception v18

    :try_start_3
    monitor-exit v19
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    throw v18

    :cond_c
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/mediatek/mtklogger/framework/MultiModemLog$ModemLogHandler;->this$0:Lcom/mediatek/mtklogger/framework/MultiModemLog;

    move-object/from16 v18, v0

    move-object/from16 v0, v18

    iget-object v0, v0, Lcom/mediatek/mtklogger/framework/MultiModemLog;->mSharedPreferences:Landroid/content/SharedPreferences;

    move-object/from16 v18, v0

    invoke-interface/range {v18 .. v18}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v18

    const-string v19, "modemlog_enable"

    const/16 v20, 0x0

    invoke-interface/range {v18 .. v20}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    move-result-object v18

    invoke-interface/range {v18 .. v18}, Landroid/content/SharedPreferences$Editor;->commit()Z

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/mediatek/mtklogger/framework/MultiModemLog$ModemLogHandler;->this$0:Lcom/mediatek/mtklogger/framework/MultiModemLog;

    move-object/from16 v18, v0

    const/16 v19, 0x0

    const-string v20, ""

    # invokes: Lcom/mediatek/mtklogger/framework/MultiModemLog;->notifyServiceStatus(ILjava/lang/String;)V
    invoke-static/range {v18 .. v20}, Lcom/mediatek/mtklogger/framework/MultiModemLog;->access$500(Lcom/mediatek/mtklogger/framework/MultiModemLog;ILjava/lang/String;)V

    goto/16 :goto_1

    :cond_d
    const-string v18, "MTKLogger/MultiModemLog"

    const-string v19, "Have not connected to native layer, just ignore this command"

    invoke-static/range {v18 .. v19}, Lcom/mediatek/mtklogger/utils/Utils;->logw(Ljava/lang/String;Ljava/lang/String;)V

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/mediatek/mtklogger/framework/MultiModemLog$ModemLogHandler;->this$0:Lcom/mediatek/mtklogger/framework/MultiModemLog;

    move-object/from16 v18, v0

    const/16 v19, 0x0

    const-string v20, "1"

    # invokes: Lcom/mediatek/mtklogger/framework/MultiModemLog;->notifyServiceStatus(ILjava/lang/String;)V
    invoke-static/range {v18 .. v20}, Lcom/mediatek/mtklogger/framework/MultiModemLog;->access$500(Lcom/mediatek/mtklogger/framework/MultiModemLog;ILjava/lang/String;)V

    goto/16 :goto_1

    :sswitch_2
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/mediatek/mtklogger/framework/MultiModemLog$ModemLogHandler;->this$0:Lcom/mediatek/mtklogger/framework/MultiModemLog;

    move-object/from16 v18, v0

    # getter for: Lcom/mediatek/mtklogger/framework/MultiModemLog;->bConnected:Z
    invoke-static/range {v18 .. v18}, Lcom/mediatek/mtklogger/framework/MultiModemLog;->access$100(Lcom/mediatek/mtklogger/framework/MultiModemLog;)Z

    move-result v18

    if-eqz v18, :cond_11

    invoke-static {v4}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v18

    if-nez v18, :cond_10

    const-string v18, "MTKLogger/MultiModemLog"

    new-instance v19, Ljava/lang/StringBuilder;

    invoke-direct/range {v19 .. v19}, Ljava/lang/StringBuilder;-><init>()V

    const-string v20, "Receive config command, parameter="

    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    move-object/from16 v0, v19

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    invoke-virtual/range {v19 .. v19}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v19

    invoke-static/range {v18 .. v19}, Lcom/mediatek/mtklogger/utils/Utils;->logd(Ljava/lang/String;Ljava/lang/String;)V

    const-string v18, "autostart="

    move-object/from16 v0, v18

    invoke-virtual {v4, v0}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v18

    if-eqz v18, :cond_f

    const-string v18, "1"

    move-object/from16 v0, v18

    invoke-virtual {v4, v0}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v18

    if-eqz v18, :cond_e

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/mediatek/mtklogger/framework/MultiModemLog$ModemLogHandler;->this$0:Lcom/mediatek/mtklogger/framework/MultiModemLog;

    move-object/from16 v18, v0

    # getter for: Lcom/mediatek/mtklogger/framework/MultiModemLog;->mModemManager:Lcom/mediatek/mtklogger/framework/MultiModemLog$ModemManager;
    invoke-static/range {v18 .. v18}, Lcom/mediatek/mtklogger/framework/MultiModemLog;->access$000(Lcom/mediatek/mtklogger/framework/MultiModemLog;)Lcom/mediatek/mtklogger/framework/MultiModemLog$ModemManager;

    move-result-object v18

    new-instance v19, Ljava/lang/StringBuilder;

    invoke-direct/range {v19 .. v19}, Ljava/lang/StringBuilder;-><init>()V

    const-string v20, "setauto,"

    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/mediatek/mtklogger/framework/MultiModemLog$ModemLogHandler;->this$0:Lcom/mediatek/mtklogger/framework/MultiModemLog;

    move-object/from16 v20, v0

    # invokes: Lcom/mediatek/mtklogger/framework/MultiModemLog;->getCurrentMode()Ljava/lang/String;
    invoke-static/range {v20 .. v20}, Lcom/mediatek/mtklogger/framework/MultiModemLog;->access$600(Lcom/mediatek/mtklogger/framework/MultiModemLog;)Ljava/lang/String;

    move-result-object v20

    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    invoke-virtual/range {v19 .. v19}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v19

    # invokes: Lcom/mediatek/mtklogger/framework/MultiModemLog$ModemManager;->sendCmd(Ljava/lang/String;)Z
    invoke-static/range {v18 .. v19}, Lcom/mediatek/mtklogger/framework/MultiModemLog$ModemManager;->access$700(Lcom/mediatek/mtklogger/framework/MultiModemLog$ModemManager;Ljava/lang/String;)Z

    goto/16 :goto_1

    :cond_e
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/mediatek/mtklogger/framework/MultiModemLog$ModemLogHandler;->this$0:Lcom/mediatek/mtklogger/framework/MultiModemLog;

    move-object/from16 v18, v0

    # getter for: Lcom/mediatek/mtklogger/framework/MultiModemLog;->mModemManager:Lcom/mediatek/mtklogger/framework/MultiModemLog$ModemManager;
    invoke-static/range {v18 .. v18}, Lcom/mediatek/mtklogger/framework/MultiModemLog;->access$000(Lcom/mediatek/mtklogger/framework/MultiModemLog;)Lcom/mediatek/mtklogger/framework/MultiModemLog$ModemManager;

    move-result-object v18

    const-string v19, "setauto,0"

    # invokes: Lcom/mediatek/mtklogger/framework/MultiModemLog$ModemManager;->sendCmd(Ljava/lang/String;)Z
    invoke-static/range {v18 .. v19}, Lcom/mediatek/mtklogger/framework/MultiModemLog$ModemManager;->access$700(Lcom/mediatek/mtklogger/framework/MultiModemLog$ModemManager;Ljava/lang/String;)Z

    goto/16 :goto_1

    :cond_f
    const-string v18, "MTKLogger/MultiModemLog"

    const-string v19, "Unsupported config command"

    invoke-static/range {v18 .. v19}, Lcom/mediatek/mtklogger/utils/Utils;->logw(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_1

    :cond_10
    const-string v18, "MTKLogger/MultiModemLog"

    const-string v19, "Receive config command, but parameter is null"

    invoke-static/range {v18 .. v19}, Lcom/mediatek/mtklogger/utils/Utils;->loge(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_1

    :cond_11
    const-string v18, "MTKLogger/MultiModemLog"

    const-string v19, "Fail to config native parameter because of lost connection."

    invoke-static/range {v18 .. v19}, Lcom/mediatek/mtklogger/utils/Utils;->logw(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_1

    :sswitch_3
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/mediatek/mtklogger/framework/MultiModemLog$ModemLogHandler;->this$0:Lcom/mediatek/mtklogger/framework/MultiModemLog;

    move-object/from16 v18, v0

    # getter for: Lcom/mediatek/mtklogger/framework/MultiModemLog;->bConnected:Z
    invoke-static/range {v18 .. v18}, Lcom/mediatek/mtklogger/framework/MultiModemLog;->access$100(Lcom/mediatek/mtklogger/framework/MultiModemLog;)Z

    move-result v18

    if-nez v18, :cond_12

    const-string v18, "MTKLogger/MultiModemLog"

    const-string v19, "Reconnect to native layer fail! Mark log status as stopped."

    invoke-static/range {v18 .. v19}, Lcom/mediatek/mtklogger/utils/Utils;->logw(Ljava/lang/String;Ljava/lang/String;)V

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/mediatek/mtklogger/framework/MultiModemLog$ModemLogHandler;->this$0:Lcom/mediatek/mtklogger/framework/MultiModemLog;

    move-object/from16 v18, v0

    const/16 v19, 0x0

    const-string v20, "1"

    # invokes: Lcom/mediatek/mtklogger/framework/MultiModemLog;->notifyServiceStatus(ILjava/lang/String;)V
    invoke-static/range {v18 .. v20}, Lcom/mediatek/mtklogger/framework/MultiModemLog;->access$500(Lcom/mediatek/mtklogger/framework/MultiModemLog;ILjava/lang/String;)V

    goto/16 :goto_1

    :cond_12
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/mediatek/mtklogger/framework/MultiModemLog$ModemLogHandler;->this$0:Lcom/mediatek/mtklogger/framework/MultiModemLog;

    move-object/from16 v18, v0

    # invokes: Lcom/mediatek/mtklogger/framework/MultiModemLog;->runMonitoringLogSizeThread()V
    invoke-static/range {v18 .. v18}, Lcom/mediatek/mtklogger/framework/MultiModemLog;->access$800(Lcom/mediatek/mtklogger/framework/MultiModemLog;)V

    const-string v18, "MTKLogger/MultiModemLog"

    const-string v19, "Send query pause status command to modem."

    invoke-static/range {v18 .. v19}, Lcom/mediatek/mtklogger/utils/Utils;->logd(Ljava/lang/String;Ljava/lang/String;)V

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/mediatek/mtklogger/framework/MultiModemLog$ModemLogHandler;->this$0:Lcom/mediatek/mtklogger/framework/MultiModemLog;

    move-object/from16 v18, v0

    # getter for: Lcom/mediatek/mtklogger/framework/MultiModemLog;->mModemManager:Lcom/mediatek/mtklogger/framework/MultiModemLog$ModemManager;
    invoke-static/range {v18 .. v18}, Lcom/mediatek/mtklogger/framework/MultiModemLog;->access$000(Lcom/mediatek/mtklogger/framework/MultiModemLog;)Lcom/mediatek/mtklogger/framework/MultiModemLog$ModemManager;

    move-result-object v18

    const-string v19, "ispaused"

    # invokes: Lcom/mediatek/mtklogger/framework/MultiModemLog$ModemManager;->sendCmd(Ljava/lang/String;)Z
    invoke-static/range {v18 .. v19}, Lcom/mediatek/mtklogger/framework/MultiModemLog$ModemManager;->access$700(Lcom/mediatek/mtklogger/framework/MultiModemLog$ModemManager;Ljava/lang/String;)Z

    goto/16 :goto_1

    :sswitch_4
    const-string v18, "MTKLogger/MultiModemLog"

    new-instance v19, Ljava/lang/StringBuilder;

    invoke-direct/range {v19 .. v19}, Ljava/lang/StringBuilder;-><init>()V

    const-string v20, "Receive adb command["

    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    move-object/from16 v0, v19

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    const-string v20, "]"

    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    invoke-virtual/range {v19 .. v19}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v19

    invoke-static/range {v18 .. v19}, Lcom/mediatek/mtklogger/utils/Utils;->logd(Ljava/lang/String;Ljava/lang/String;)V

    invoke-static {v4}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v18

    if-nez v18, :cond_4

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/mediatek/mtklogger/framework/MultiModemLog$ModemLogHandler;->this$0:Lcom/mediatek/mtklogger/framework/MultiModemLog;

    move-object/from16 v18, v0

    move-object/from16 v0, v18

    # invokes: Lcom/mediatek/mtklogger/framework/MultiModemLog;->dealWithADBCommand(Ljava/lang/String;)V
    invoke-static {v0, v4}, Lcom/mediatek/mtklogger/framework/MultiModemLog;->access$1000(Lcom/mediatek/mtklogger/framework/MultiModemLog;Ljava/lang/String;)V

    goto/16 :goto_1

    :sswitch_5
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/mediatek/mtklogger/framework/MultiModemLog$ModemLogHandler;->this$0:Lcom/mediatek/mtklogger/framework/MultiModemLog;

    move-object/from16 v18, v0

    const/16 v19, 0x0

    # setter for: Lcom/mediatek/mtklogger/framework/MultiModemLog;->bConnected:Z
    invoke-static/range {v18 .. v19}, Lcom/mediatek/mtklogger/framework/MultiModemLog;->access$102(Lcom/mediatek/mtklogger/framework/MultiModemLog;Z)Z

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/mediatek/mtklogger/framework/MultiModemLog$ModemLogHandler;->this$0:Lcom/mediatek/mtklogger/framework/MultiModemLog;

    move-object/from16 v18, v0

    const/16 v19, 0x1

    # setter for: Lcom/mediatek/mtklogger/framework/MultiModemLog;->mMonitorThreadStop:Z
    invoke-static/range {v18 .. v19}, Lcom/mediatek/mtklogger/framework/MultiModemLog;->access$902(Lcom/mediatek/mtklogger/framework/MultiModemLog;Z)Z

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/mediatek/mtklogger/framework/MultiModemLog$ModemLogHandler;->this$0:Lcom/mediatek/mtklogger/framework/MultiModemLog;

    move-object/from16 v18, v0

    move-object/from16 v0, v18

    iget-object v0, v0, Lcom/mediatek/mtklogger/framework/MultiModemLog;->waitNextClearLogCheck:Ljava/lang/Object;

    move-object/from16 v19, v0

    monitor-enter v19

    :try_start_4
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/mediatek/mtklogger/framework/MultiModemLog$ModemLogHandler;->this$0:Lcom/mediatek/mtklogger/framework/MultiModemLog;

    move-object/from16 v18, v0

    move-object/from16 v0, v18

    iget-object v0, v0, Lcom/mediatek/mtklogger/framework/MultiModemLog;->waitNextClearLogCheck:Ljava/lang/Object;

    move-object/from16 v18, v0

    invoke-virtual/range {v18 .. v18}, Ljava/lang/Object;->notifyAll()V

    monitor-exit v19
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    const-string v18, "MTKLogger/MultiModemLog"

    new-instance v19, Ljava/lang/StringBuilder;

    invoke-direct/range {v19 .. v19}, Ljava/lang/StringBuilder;-><init>()V

    const-string v20, "Modemlog ["

    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    move-object/from16 v0, p1

    iget v0, v0, Landroid/os/Message;->arg1:I

    move/from16 v20, v0

    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v19

    const-string v20, "] lost, do not stop other instance, just update status as stopped"

    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    invoke-virtual/range {v19 .. v19}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v19

    invoke-static/range {v18 .. v19}, Lcom/mediatek/mtklogger/utils/Utils;->logw(Ljava/lang/String;Ljava/lang/String;)V

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/mediatek/mtklogger/framework/MultiModemLog$ModemLogHandler;->this$0:Lcom/mediatek/mtklogger/framework/MultiModemLog;

    move-object/from16 v18, v0

    # getter for: Lcom/mediatek/mtklogger/framework/MultiModemLog;->mModemManager:Lcom/mediatek/mtklogger/framework/MultiModemLog$ModemManager;
    invoke-static/range {v18 .. v18}, Lcom/mediatek/mtklogger/framework/MultiModemLog;->access$000(Lcom/mediatek/mtklogger/framework/MultiModemLog;)Lcom/mediatek/mtklogger/framework/MultiModemLog$ModemManager;

    move-result-object v18

    if-eqz v18, :cond_13

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/mediatek/mtklogger/framework/MultiModemLog$ModemLogHandler;->this$0:Lcom/mediatek/mtklogger/framework/MultiModemLog;

    move-object/from16 v18, v0

    # getter for: Lcom/mediatek/mtklogger/framework/MultiModemLog;->mModemManager:Lcom/mediatek/mtklogger/framework/MultiModemLog$ModemManager;
    invoke-static/range {v18 .. v18}, Lcom/mediatek/mtklogger/framework/MultiModemLog;->access$000(Lcom/mediatek/mtklogger/framework/MultiModemLog;)Lcom/mediatek/mtklogger/framework/MultiModemLog$ModemManager;

    move-result-object v18

    # invokes: Lcom/mediatek/mtklogger/framework/MultiModemLog$ModemManager;->stop()V
    invoke-static/range {v18 .. v18}, Lcom/mediatek/mtklogger/framework/MultiModemLog$ModemManager;->access$1100(Lcom/mediatek/mtklogger/framework/MultiModemLog$ModemManager;)V

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/mediatek/mtklogger/framework/MultiModemLog$ModemLogHandler;->this$0:Lcom/mediatek/mtklogger/framework/MultiModemLog;

    move-object/from16 v18, v0

    const/16 v19, 0x0

    # setter for: Lcom/mediatek/mtklogger/framework/MultiModemLog;->mModemManager:Lcom/mediatek/mtklogger/framework/MultiModemLog$ModemManager;
    invoke-static/range {v18 .. v19}, Lcom/mediatek/mtklogger/framework/MultiModemLog;->access$002(Lcom/mediatek/mtklogger/framework/MultiModemLog;Lcom/mediatek/mtklogger/framework/MultiModemLog$ModemManager;)Lcom/mediatek/mtklogger/framework/MultiModemLog$ModemManager;

    :cond_13
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/mediatek/mtklogger/framework/MultiModemLog$ModemLogHandler;->this$0:Lcom/mediatek/mtklogger/framework/MultiModemLog;

    move-object/from16 v18, v0

    const/16 v19, 0x0

    const-string v20, "5"

    # invokes: Lcom/mediatek/mtklogger/framework/MultiModemLog;->notifyServiceStatus(ILjava/lang/String;)V
    invoke-static/range {v18 .. v20}, Lcom/mediatek/mtklogger/framework/MultiModemLog;->access$500(Lcom/mediatek/mtklogger/framework/MultiModemLog;ILjava/lang/String;)V

    goto/16 :goto_1

    :catchall_1
    move-exception v18

    :try_start_5
    monitor-exit v19
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_1

    throw v18

    :sswitch_6
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/mediatek/mtklogger/framework/MultiModemLog$ModemLogHandler;->this$0:Lcom/mediatek/mtklogger/framework/MultiModemLog;

    move-object/from16 v18, v0

    # getter for: Lcom/mediatek/mtklogger/framework/MultiModemLog;->bConnected:Z
    invoke-static/range {v18 .. v18}, Lcom/mediatek/mtklogger/framework/MultiModemLog;->access$100(Lcom/mediatek/mtklogger/framework/MultiModemLog;)Z

    move-result v18

    if-eqz v18, :cond_15

    const/16 v18, 0x1

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/mediatek/mtklogger/framework/MultiModemLog$ModemLogHandler;->this$0:Lcom/mediatek/mtklogger/framework/MultiModemLog;

    move-object/from16 v19, v0

    move-object/from16 v0, v19

    iget-object v0, v0, Lcom/mediatek/mtklogger/framework/MultiModemLog;->mSharedPreferences:Landroid/content/SharedPreferences;

    move-object/from16 v19, v0

    const-string v20, "modemlog_enable"

    const/16 v21, 0x1

    invoke-interface/range {v19 .. v21}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v19

    move/from16 v0, v18

    move/from16 v1, v19

    if-ne v0, v1, :cond_14

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/mediatek/mtklogger/framework/MultiModemLog$ModemLogHandler;->this$0:Lcom/mediatek/mtklogger/framework/MultiModemLog;

    move-object/from16 v18, v0

    # getter for: Lcom/mediatek/mtklogger/framework/MultiModemLog;->mModemManager:Lcom/mediatek/mtklogger/framework/MultiModemLog$ModemManager;
    invoke-static/range {v18 .. v18}, Lcom/mediatek/mtklogger/framework/MultiModemLog;->access$000(Lcom/mediatek/mtklogger/framework/MultiModemLog;)Lcom/mediatek/mtklogger/framework/MultiModemLog$ModemManager;

    move-result-object v18

    const-string v19, "polling"

    # invokes: Lcom/mediatek/mtklogger/framework/MultiModemLog$ModemManager;->sendCmdToOneInstance(Ljava/lang/String;)Z
    invoke-static/range {v18 .. v19}, Lcom/mediatek/mtklogger/framework/MultiModemLog$ModemManager;->access$1200(Lcom/mediatek/mtklogger/framework/MultiModemLog$ModemManager;Ljava/lang/String;)Z

    goto/16 :goto_1

    :cond_14
    const-string v18, "MTKLogger/MultiModemLog"

    const-string v19, "Modem log is stopped, no dumping is allowed."

    invoke-static/range {v18 .. v19}, Lcom/mediatek/mtklogger/utils/Utils;->logw(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_1

    :cond_15
    const-string v18, "MTKLogger/MultiModemLog"

    const-string v19, "Lost connection to native, so ignore polling command."

    invoke-static/range {v18 .. v19}, Lcom/mediatek/mtklogger/utils/Utils;->loge(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_1

    :sswitch_7
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/mediatek/mtklogger/framework/MultiModemLog$ModemLogHandler;->this$0:Lcom/mediatek/mtklogger/framework/MultiModemLog;

    move-object/from16 v18, v0

    # getter for: Lcom/mediatek/mtklogger/framework/MultiModemLog;->bConnected:Z
    invoke-static/range {v18 .. v18}, Lcom/mediatek/mtklogger/framework/MultiModemLog;->access$100(Lcom/mediatek/mtklogger/framework/MultiModemLog;)Z

    move-result v18

    if-eqz v18, :cond_16

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/mediatek/mtklogger/framework/MultiModemLog$ModemLogHandler;->this$0:Lcom/mediatek/mtklogger/framework/MultiModemLog;

    move-object/from16 v18, v0

    # getter for: Lcom/mediatek/mtklogger/framework/MultiModemLog;->mModemManager:Lcom/mediatek/mtklogger/framework/MultiModemLog$ModemManager;
    invoke-static/range {v18 .. v18}, Lcom/mediatek/mtklogger/framework/MultiModemLog;->access$000(Lcom/mediatek/mtklogger/framework/MultiModemLog;)Lcom/mediatek/mtklogger/framework/MultiModemLog$ModemManager;

    move-result-object v18

    move-object/from16 v0, p1

    iget v0, v0, Landroid/os/Message;->arg1:I

    move/from16 v19, v0

    const-string v20, "resetmd"

    # invokes: Lcom/mediatek/mtklogger/framework/MultiModemLog$ModemManager;->sendCmd(ILjava/lang/String;)Z
    invoke-static/range {v18 .. v20}, Lcom/mediatek/mtklogger/framework/MultiModemLog$ModemManager;->access$1300(Lcom/mediatek/mtklogger/framework/MultiModemLog$ModemManager;ILjava/lang/String;)Z

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/mediatek/mtklogger/framework/MultiModemLog$ModemLogHandler;->this$0:Lcom/mediatek/mtklogger/framework/MultiModemLog;

    move-object/from16 v18, v0

    # invokes: Lcom/mediatek/mtklogger/framework/MultiModemLog;->showResetModemDialog()V
    invoke-static/range {v18 .. v18}, Lcom/mediatek/mtklogger/framework/MultiModemLog;->access$1400(Lcom/mediatek/mtklogger/framework/MultiModemLog;)V

    goto/16 :goto_1

    :cond_16
    const-string v18, "MTKLogger/MultiModemLog"

    const-string v19, "Lost connection to native, reset command will have no effect."

    invoke-static/range {v18 .. v19}, Lcom/mediatek/mtklogger/utils/Utils;->loge(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_1

    :sswitch_8
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/mediatek/mtklogger/framework/MultiModemLog$ModemLogHandler;->this$0:Lcom/mediatek/mtklogger/framework/MultiModemLog;

    move-object/from16 v18, v0

    const/16 v19, 0x3

    # setter for: Lcom/mediatek/mtklogger/framework/MultiModemLog;->mCurrentStage:I
    invoke-static/range {v18 .. v19}, Lcom/mediatek/mtklogger/framework/MultiModemLog;->access$1502(Lcom/mediatek/mtklogger/framework/MultiModemLog;I)I

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/mediatek/mtklogger/framework/MultiModemLog$ModemLogHandler;->this$0:Lcom/mediatek/mtklogger/framework/MultiModemLog;

    move-object/from16 v18, v0

    # getter for: Lcom/mediatek/mtklogger/framework/MultiModemLog;->mCmdResHandler:Landroid/os/Handler;
    invoke-static/range {v18 .. v18}, Lcom/mediatek/mtklogger/framework/MultiModemLog;->access$1600(Lcom/mediatek/mtklogger/framework/MultiModemLog;)Landroid/os/Handler;

    move-result-object v18

    const/16 v19, 0x3

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/mediatek/mtklogger/framework/MultiModemLog$ModemLogHandler;->this$0:Lcom/mediatek/mtklogger/framework/MultiModemLog;

    move-object/from16 v20, v0

    # getter for: Lcom/mediatek/mtklogger/framework/MultiModemLog;->mCurrentStage:I
    invoke-static/range {v20 .. v20}, Lcom/mediatek/mtklogger/framework/MultiModemLog;->access$1500(Lcom/mediatek/mtklogger/framework/MultiModemLog;)I

    move-result v20

    invoke-static/range {v20 .. v20}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v20

    invoke-virtual/range {v18 .. v20}, Landroid/os/Handler;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v18

    invoke-virtual/range {v18 .. v18}, Landroid/os/Message;->sendToTarget()V

    goto/16 :goto_1

    :sswitch_9
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/mediatek/mtklogger/framework/MultiModemLog$ModemLogHandler;->this$0:Lcom/mediatek/mtklogger/framework/MultiModemLog;

    move-object/from16 v18, v0

    const/16 v19, 0x0

    # setter for: Lcom/mediatek/mtklogger/framework/MultiModemLog;->mCurrentStage:I
    invoke-static/range {v18 .. v19}, Lcom/mediatek/mtklogger/framework/MultiModemLog;->access$1502(Lcom/mediatek/mtklogger/framework/MultiModemLog;I)I

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/mediatek/mtklogger/framework/MultiModemLog$ModemLogHandler;->this$0:Lcom/mediatek/mtklogger/framework/MultiModemLog;

    move-object/from16 v18, v0

    # getter for: Lcom/mediatek/mtklogger/framework/MultiModemLog;->mCmdResHandler:Landroid/os/Handler;
    invoke-static/range {v18 .. v18}, Lcom/mediatek/mtklogger/framework/MultiModemLog;->access$1600(Lcom/mediatek/mtklogger/framework/MultiModemLog;)Landroid/os/Handler;

    move-result-object v18

    const/16 v19, 0x3

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/mediatek/mtklogger/framework/MultiModemLog$ModemLogHandler;->this$0:Lcom/mediatek/mtklogger/framework/MultiModemLog;

    move-object/from16 v20, v0

    # getter for: Lcom/mediatek/mtklogger/framework/MultiModemLog;->mCurrentStage:I
    invoke-static/range {v20 .. v20}, Lcom/mediatek/mtklogger/framework/MultiModemLog;->access$1500(Lcom/mediatek/mtklogger/framework/MultiModemLog;)I

    move-result v20

    invoke-static/range {v20 .. v20}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v20

    invoke-virtual/range {v18 .. v20}, Landroid/os/Handler;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v18

    invoke-virtual/range {v18 .. v18}, Landroid/os/Message;->sendToTarget()V

    move-object/from16 v0, p1

    iget-object v14, v0, Landroid/os/Message;->obj:Ljava/lang/Object;

    const-string v15, ""

    if-eqz v14, :cond_17

    instance-of v0, v14, Ljava/lang/String;

    move/from16 v18, v0

    if-eqz v18, :cond_17

    move-object v15, v14

    check-cast v15, Ljava/lang/String;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/mediatek/mtklogger/framework/MultiModemLog$ModemLogHandler;->this$0:Lcom/mediatek/mtklogger/framework/MultiModemLog;

    move-object/from16 v18, v0

    move-object/from16 v0, v18

    iget-object v0, v0, Lcom/mediatek/mtklogger/framework/MultiModemLog;->mSharedPreferences:Landroid/content/SharedPreferences;

    move-object/from16 v18, v0

    invoke-interface/range {v18 .. v18}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v18

    const-string v19, "modem_exception_path"

    move-object/from16 v0, v18

    move-object/from16 v1, v19

    invoke-interface {v0, v1, v15}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    move-result-object v18

    invoke-interface/range {v18 .. v18}, Landroid/content/SharedPreferences$Editor;->commit()Z

    :cond_17
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/mediatek/mtklogger/framework/MultiModemLog$ModemLogHandler;->this$0:Lcom/mediatek/mtklogger/framework/MultiModemLog;

    move-object/from16 v18, v0

    move-object/from16 v0, p1

    iget v0, v0, Landroid/os/Message;->arg1:I

    move/from16 v19, v0

    move-object/from16 v0, v18

    move/from16 v1, v19

    # invokes: Lcom/mediatek/mtklogger/framework/MultiModemLog;->showMemoryDumpDoneDialog(ILjava/lang/String;)V
    invoke-static {v0, v1, v15}, Lcom/mediatek/mtklogger/framework/MultiModemLog;->access$400(Lcom/mediatek/mtklogger/framework/MultiModemLog;ILjava/lang/String;)V

    goto/16 :goto_1

    :sswitch_a
    sget-object v18, Lcom/mediatek/mtklogger/framework/LogInstance;->mContext:Landroid/content/Context;

    const-string v19, "Log file is lost"

    const/16 v20, 0x1

    invoke-static/range {v18 .. v20}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v18

    invoke-virtual/range {v18 .. v18}, Landroid/widget/Toast;->show()V

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/mediatek/mtklogger/framework/MultiModemLog$ModemLogHandler;->this$0:Lcom/mediatek/mtklogger/framework/MultiModemLog;

    move-object/from16 v18, v0

    # getter for: Lcom/mediatek/mtklogger/framework/MultiModemLog;->bConnected:Z
    invoke-static/range {v18 .. v18}, Lcom/mediatek/mtklogger/framework/MultiModemLog;->access$100(Lcom/mediatek/mtklogger/framework/MultiModemLog;)Z

    move-result v18

    if-eqz v18, :cond_4

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/mediatek/mtklogger/framework/MultiModemLog$ModemLogHandler;->this$0:Lcom/mediatek/mtklogger/framework/MultiModemLog;

    move-object/from16 v18, v0

    # getter for: Lcom/mediatek/mtklogger/framework/MultiModemLog;->mModemManager:Lcom/mediatek/mtklogger/framework/MultiModemLog$ModemManager;
    invoke-static/range {v18 .. v18}, Lcom/mediatek/mtklogger/framework/MultiModemLog;->access$000(Lcom/mediatek/mtklogger/framework/MultiModemLog;)Lcom/mediatek/mtklogger/framework/MultiModemLog$ModemManager;

    move-result-object v18

    const-string v19, "pause"

    # invokes: Lcom/mediatek/mtklogger/framework/MultiModemLog$ModemManager;->sendCmd(Ljava/lang/String;)Z
    invoke-static/range {v18 .. v19}, Lcom/mediatek/mtklogger/framework/MultiModemLog$ModemManager;->access$700(Lcom/mediatek/mtklogger/framework/MultiModemLog$ModemManager;Ljava/lang/String;)Z

    move-result v18

    if-eqz v18, :cond_4

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/mediatek/mtklogger/framework/MultiModemLog$ModemLogHandler;->this$0:Lcom/mediatek/mtklogger/framework/MultiModemLog;

    move-object/from16 v18, v0

    move-object/from16 v0, v18

    iget-object v0, v0, Lcom/mediatek/mtklogger/framework/MultiModemLog;->mSharedPreferences:Landroid/content/SharedPreferences;

    move-object/from16 v18, v0

    invoke-interface/range {v18 .. v18}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v18

    const-string v19, "modemlog_enable"

    const/16 v20, 0x0

    invoke-interface/range {v18 .. v20}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    move-result-object v18

    invoke-interface/range {v18 .. v18}, Landroid/content/SharedPreferences$Editor;->commit()Z

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/mediatek/mtklogger/framework/MultiModemLog$ModemLogHandler;->this$0:Lcom/mediatek/mtklogger/framework/MultiModemLog;

    move-object/from16 v18, v0

    const/16 v19, 0x0

    const-string v20, ""

    # invokes: Lcom/mediatek/mtklogger/framework/MultiModemLog;->notifyServiceStatus(ILjava/lang/String;)V
    invoke-static/range {v18 .. v20}, Lcom/mediatek/mtklogger/framework/MultiModemLog;->access$500(Lcom/mediatek/mtklogger/framework/MultiModemLog;ILjava/lang/String;)V

    goto/16 :goto_1

    :sswitch_b
    new-instance v3, Landroid/app/AlertDialog$Builder;

    sget-object v18, Lcom/mediatek/mtklogger/framework/LogInstance;->mContext:Landroid/content/Context;

    move-object/from16 v0, v18

    invoke-direct {v3, v0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    const v18, 0x7f070078

    move/from16 v0, v18

    invoke-virtual {v3, v0}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    move-result-object v18

    const v19, 0x7f070079

    invoke-virtual/range {v18 .. v19}, Landroid/app/AlertDialog$Builder;->setMessage(I)Landroid/app/AlertDialog$Builder;

    const v18, 0x1040013

    new-instance v19, Lcom/mediatek/mtklogger/framework/MultiModemLog$ModemLogHandler$1;

    move-object/from16 v0, v19

    move-object/from16 v1, p0

    invoke-direct {v0, v1}, Lcom/mediatek/mtklogger/framework/MultiModemLog$ModemLogHandler$1;-><init>(Lcom/mediatek/mtklogger/framework/MultiModemLog$ModemLogHandler;)V

    move/from16 v0, v18

    move-object/from16 v1, v19

    invoke-virtual {v3, v0, v1}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    invoke-virtual {v3}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v8

    invoke-virtual {v8}, Landroid/app/AlertDialog;->getWindow()Landroid/view/Window;

    move-result-object v18

    const/16 v19, 0x7d3

    invoke-virtual/range {v18 .. v19}, Landroid/view/Window;->setType(I)V

    invoke-virtual {v8}, Landroid/app/AlertDialog;->show()V

    goto/16 :goto_1

    :sswitch_c
    sget-object v18, Lcom/mediatek/mtklogger/framework/LogInstance;->mContext:Landroid/content/Context;

    const-string v19, "Failed to write log file"

    const/16 v20, 0x1

    invoke-static/range {v18 .. v20}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v18

    invoke-virtual/range {v18 .. v18}, Landroid/widget/Toast;->show()V

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/mediatek/mtklogger/framework/MultiModemLog$ModemLogHandler;->this$0:Lcom/mediatek/mtklogger/framework/MultiModemLog;

    move-object/from16 v18, v0

    # getter for: Lcom/mediatek/mtklogger/framework/MultiModemLog;->bConnected:Z
    invoke-static/range {v18 .. v18}, Lcom/mediatek/mtklogger/framework/MultiModemLog;->access$100(Lcom/mediatek/mtklogger/framework/MultiModemLog;)Z

    move-result v18

    if-eqz v18, :cond_4

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/mediatek/mtklogger/framework/MultiModemLog$ModemLogHandler;->this$0:Lcom/mediatek/mtklogger/framework/MultiModemLog;

    move-object/from16 v18, v0

    # getter for: Lcom/mediatek/mtklogger/framework/MultiModemLog;->mModemManager:Lcom/mediatek/mtklogger/framework/MultiModemLog$ModemManager;
    invoke-static/range {v18 .. v18}, Lcom/mediatek/mtklogger/framework/MultiModemLog;->access$000(Lcom/mediatek/mtklogger/framework/MultiModemLog;)Lcom/mediatek/mtklogger/framework/MultiModemLog$ModemManager;

    move-result-object v18

    const-string v19, "pause"

    # invokes: Lcom/mediatek/mtklogger/framework/MultiModemLog$ModemManager;->sendCmd(Ljava/lang/String;)Z
    invoke-static/range {v18 .. v19}, Lcom/mediatek/mtklogger/framework/MultiModemLog$ModemManager;->access$700(Lcom/mediatek/mtklogger/framework/MultiModemLog$ModemManager;Ljava/lang/String;)Z

    move-result v18

    if-eqz v18, :cond_4

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/mediatek/mtklogger/framework/MultiModemLog$ModemLogHandler;->this$0:Lcom/mediatek/mtklogger/framework/MultiModemLog;

    move-object/from16 v18, v0

    move-object/from16 v0, v18

    iget-object v0, v0, Lcom/mediatek/mtklogger/framework/MultiModemLog;->mSharedPreferences:Landroid/content/SharedPreferences;

    move-object/from16 v18, v0

    invoke-interface/range {v18 .. v18}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v18

    const-string v19, "modemlog_enable"

    const/16 v20, 0x0

    invoke-interface/range {v18 .. v20}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    move-result-object v18

    invoke-interface/range {v18 .. v18}, Landroid/content/SharedPreferences$Editor;->commit()Z

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/mediatek/mtklogger/framework/MultiModemLog$ModemLogHandler;->this$0:Lcom/mediatek/mtklogger/framework/MultiModemLog;

    move-object/from16 v18, v0

    const/16 v19, 0x0

    const-string v20, ""

    # invokes: Lcom/mediatek/mtklogger/framework/MultiModemLog;->notifyServiceStatus(ILjava/lang/String;)V
    invoke-static/range {v18 .. v20}, Lcom/mediatek/mtklogger/framework/MultiModemLog;->access$500(Lcom/mediatek/mtklogger/framework/MultiModemLog;ILjava/lang/String;)V

    goto/16 :goto_1

    :sswitch_d
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/mediatek/mtklogger/framework/MultiModemLog$ModemLogHandler;->this$0:Lcom/mediatek/mtklogger/framework/MultiModemLog;

    move-object/from16 v18, v0

    # getter for: Lcom/mediatek/mtklogger/framework/MultiModemLog;->bConnected:Z
    invoke-static/range {v18 .. v18}, Lcom/mediatek/mtklogger/framework/MultiModemLog;->access$100(Lcom/mediatek/mtklogger/framework/MultiModemLog;)Z

    move-result v18

    if-eqz v18, :cond_4

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/mediatek/mtklogger/framework/MultiModemLog$ModemLogHandler;->this$0:Lcom/mediatek/mtklogger/framework/MultiModemLog;

    move-object/from16 v18, v0

    # getter for: Lcom/mediatek/mtklogger/framework/MultiModemLog;->mModemManager:Lcom/mediatek/mtklogger/framework/MultiModemLog$ModemManager;
    invoke-static/range {v18 .. v18}, Lcom/mediatek/mtklogger/framework/MultiModemLog;->access$000(Lcom/mediatek/mtklogger/framework/MultiModemLog;)Lcom/mediatek/mtklogger/framework/MultiModemLog$ModemManager;

    move-result-object v18

    const-string v19, "pause"

    # invokes: Lcom/mediatek/mtklogger/framework/MultiModemLog$ModemManager;->sendCmd(Ljava/lang/String;)Z
    invoke-static/range {v18 .. v19}, Lcom/mediatek/mtklogger/framework/MultiModemLog$ModemManager;->access$700(Lcom/mediatek/mtklogger/framework/MultiModemLog$ModemManager;Ljava/lang/String;)Z

    move-result v18

    if-eqz v18, :cond_4

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/mediatek/mtklogger/framework/MultiModemLog$ModemLogHandler;->this$0:Lcom/mediatek/mtklogger/framework/MultiModemLog;

    move-object/from16 v18, v0

    move-object/from16 v0, v18

    iget-object v0, v0, Lcom/mediatek/mtklogger/framework/MultiModemLog;->mSharedPreferences:Landroid/content/SharedPreferences;

    move-object/from16 v18, v0

    invoke-interface/range {v18 .. v18}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v18

    const-string v19, "modemlog_enable"

    const/16 v20, 0x0

    invoke-interface/range {v18 .. v20}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    move-result-object v18

    invoke-interface/range {v18 .. v18}, Landroid/content/SharedPreferences$Editor;->commit()Z

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/mediatek/mtklogger/framework/MultiModemLog$ModemLogHandler;->this$0:Lcom/mediatek/mtklogger/framework/MultiModemLog;

    move-object/from16 v18, v0

    const/16 v19, 0x0

    const-string v20, "3"

    # invokes: Lcom/mediatek/mtklogger/framework/MultiModemLog;->notifyServiceStatus(ILjava/lang/String;)V
    invoke-static/range {v18 .. v20}, Lcom/mediatek/mtklogger/framework/MultiModemLog;->access$500(Lcom/mediatek/mtklogger/framework/MultiModemLog;ILjava/lang/String;)V

    goto/16 :goto_1

    :sswitch_e
    move-object/from16 v0, p1

    iget v12, v0, Landroid/os/Message;->arg1:I

    move-object/from16 v0, p1

    iget-object v0, v0, Landroid/os/Message;->obj:Ljava/lang/Object;

    move-object/from16 v16, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/mediatek/mtklogger/framework/MultiModemLog$ModemLogHandler;->this$0:Lcom/mediatek/mtklogger/framework/MultiModemLog;

    move-object/from16 v18, v0

    # getter for: Lcom/mediatek/mtklogger/framework/MultiModemLog;->mCurrentStatus:I
    invoke-static/range {v18 .. v18}, Lcom/mediatek/mtklogger/framework/MultiModemLog;->access$300(Lcom/mediatek/mtklogger/framework/MultiModemLog;)I

    move-result v18

    const/16 v19, -0x1

    move/from16 v0, v18

    move/from16 v1, v19

    if-ne v0, v1, :cond_18

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/mediatek/mtklogger/framework/MultiModemLog$ModemLogHandler;->this$0:Lcom/mediatek/mtklogger/framework/MultiModemLog;

    move-object/from16 v18, v0

    const/16 v19, 0x0

    # setter for: Lcom/mediatek/mtklogger/framework/MultiModemLog;->mCurrentStatus:I
    invoke-static/range {v18 .. v19}, Lcom/mediatek/mtklogger/framework/MultiModemLog;->access$302(Lcom/mediatek/mtklogger/framework/MultiModemLog;I)I

    :cond_18
    const-string v18, "0"

    move-object/from16 v0, v18

    move-object/from16 v1, v16

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v18

    if-eqz v18, :cond_19

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/mediatek/mtklogger/framework/MultiModemLog$ModemLogHandler;->this$0:Lcom/mediatek/mtklogger/framework/MultiModemLog;

    move-object/from16 v18, v0

    move-object/from16 v0, v18

    # |= operator for: Lcom/mediatek/mtklogger/framework/MultiModemLog;->mCurrentStatus:I
    invoke-static {v0, v12}, Lcom/mediatek/mtklogger/framework/MultiModemLog;->access$376(Lcom/mediatek/mtklogger/framework/MultiModemLog;I)I

    :goto_3
    const-string v18, "MTKLogger/MultiModemLog"

    new-instance v19, Ljava/lang/StringBuilder;

    invoke-direct/range {v19 .. v19}, Ljava/lang/StringBuilder;-><init>()V

    const-string v20, "Query MD pause status return, modemIndex="

    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    move-object/from16 v0, v19

    invoke-virtual {v0, v12}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v19

    const-string v20, ", new pause status="

    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    move-object/from16 v0, v19

    move-object/from16 v1, v16

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v19

    const-string v20, ", mCurrentStatus="

    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/mediatek/mtklogger/framework/MultiModemLog$ModemLogHandler;->this$0:Lcom/mediatek/mtklogger/framework/MultiModemLog;

    move-object/from16 v20, v0

    # getter for: Lcom/mediatek/mtklogger/framework/MultiModemLog;->mCurrentStatus:I
    invoke-static/range {v20 .. v20}, Lcom/mediatek/mtklogger/framework/MultiModemLog;->access$300(Lcom/mediatek/mtklogger/framework/MultiModemLog;)I

    move-result v20

    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v19

    invoke-virtual/range {v19 .. v19}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v19

    invoke-static/range {v18 .. v19}, Lcom/mediatek/mtklogger/utils/Utils;->logd(Ljava/lang/String;Ljava/lang/String;)V

    sget-object v18, Lcom/mediatek/mtklogger/utils/Utils;->KEY_LOG_RUNNING_STATUS_IN_SYSPROP_MAP:Landroid/util/SparseArray;

    const/16 v19, 0x2

    invoke-virtual/range {v18 .. v19}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v18

    check-cast v18, Ljava/lang/String;

    const-string v19, "0"

    invoke-static/range {v18 .. v19}, Landroid/os/SystemProperties;->get(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v13

    const-string v18, "0"

    move-object/from16 v0, v18

    invoke-virtual {v0, v13}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v18

    if-eqz v18, :cond_1b

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/mediatek/mtklogger/framework/MultiModemLog$ModemLogHandler;->this$0:Lcom/mediatek/mtklogger/framework/MultiModemLog;

    move-object/from16 v18, v0

    # getter for: Lcom/mediatek/mtklogger/framework/MultiModemLog;->mCurrentStatus:I
    invoke-static/range {v18 .. v18}, Lcom/mediatek/mtklogger/framework/MultiModemLog;->access$300(Lcom/mediatek/mtklogger/framework/MultiModemLog;)I

    move-result v18

    if-lez v18, :cond_1b

    const-string v18, "MTKLogger/MultiModemLog"

    const-string v19, "Modem log runing status from system property is 0, but new query value is 1, update this to user."

    invoke-static/range {v18 .. v19}, Lcom/mediatek/mtklogger/utils/Utils;->logw(Ljava/lang/String;Ljava/lang/String;)V

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/mediatek/mtklogger/framework/MultiModemLog$ModemLogHandler;->this$0:Lcom/mediatek/mtklogger/framework/MultiModemLog;

    move-object/from16 v18, v0

    const/16 v19, 0x1

    const-string v20, ""

    # invokes: Lcom/mediatek/mtklogger/framework/MultiModemLog;->notifyServiceStatus(ILjava/lang/String;)V
    invoke-static/range {v18 .. v20}, Lcom/mediatek/mtklogger/framework/MultiModemLog;->access$500(Lcom/mediatek/mtklogger/framework/MultiModemLog;ILjava/lang/String;)V

    goto/16 :goto_1

    :cond_19
    const-string v18, "1"

    move-object/from16 v0, v18

    move-object/from16 v1, v16

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v18

    if-eqz v18, :cond_1a

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/mediatek/mtklogger/framework/MultiModemLog$ModemLogHandler;->this$0:Lcom/mediatek/mtklogger/framework/MultiModemLog;

    move-object/from16 v18, v0

    move-object/from16 v0, v18

    # |= operator for: Lcom/mediatek/mtklogger/framework/MultiModemLog;->mCurrentStatus:I
    invoke-static {v0, v12}, Lcom/mediatek/mtklogger/framework/MultiModemLog;->access$376(Lcom/mediatek/mtklogger/framework/MultiModemLog;I)I

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/mediatek/mtklogger/framework/MultiModemLog$ModemLogHandler;->this$0:Lcom/mediatek/mtklogger/framework/MultiModemLog;

    move-object/from16 v18, v0

    move-object/from16 v0, v18

    # ^= operator for: Lcom/mediatek/mtklogger/framework/MultiModemLog;->mCurrentStatus:I
    invoke-static {v0, v12}, Lcom/mediatek/mtklogger/framework/MultiModemLog;->access$380(Lcom/mediatek/mtklogger/framework/MultiModemLog;I)I

    goto/16 :goto_3

    :cond_1a
    const-string v18, "MTKLogger/MultiModemLog"

    const-string v19, "Invalid pause status value."

    invoke-static/range {v18 .. v19}, Lcom/mediatek/mtklogger/utils/Utils;->loge(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_3

    :cond_1b
    const-string v18, "1"

    move-object/from16 v0, v18

    invoke-virtual {v0, v13}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v18

    if-eqz v18, :cond_4

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/mediatek/mtklogger/framework/MultiModemLog$ModemLogHandler;->this$0:Lcom/mediatek/mtklogger/framework/MultiModemLog;

    move-object/from16 v18, v0

    # getter for: Lcom/mediatek/mtklogger/framework/MultiModemLog;->mCurrentStatus:I
    invoke-static/range {v18 .. v18}, Lcom/mediatek/mtklogger/framework/MultiModemLog;->access$300(Lcom/mediatek/mtklogger/framework/MultiModemLog;)I

    move-result v18

    if-nez v18, :cond_4

    const-string v18, "MTKLogger/MultiModemLog"

    const-string v19, "Modem log runing status from system property is 1, but new query value is 0, update this to user."

    invoke-static/range {v18 .. v19}, Lcom/mediatek/mtklogger/utils/Utils;->logw(Ljava/lang/String;Ljava/lang/String;)V

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/mediatek/mtklogger/framework/MultiModemLog$ModemLogHandler;->this$0:Lcom/mediatek/mtklogger/framework/MultiModemLog;

    move-object/from16 v18, v0

    const/16 v19, 0x0

    const-string v20, ""

    # invokes: Lcom/mediatek/mtklogger/framework/MultiModemLog;->notifyServiceStatus(ILjava/lang/String;)V
    invoke-static/range {v18 .. v20}, Lcom/mediatek/mtklogger/framework/MultiModemLog;->access$500(Lcom/mediatek/mtklogger/framework/MultiModemLog;ILjava/lang/String;)V

    goto/16 :goto_1

    :sswitch_data_0
    .sparse-switch
        0x1 -> :sswitch_0
        0x3 -> :sswitch_1
        0x7 -> :sswitch_2
        0x8 -> :sswitch_4
        0x9 -> :sswitch_3
        0x16 -> :sswitch_5
        0x33 -> :sswitch_6
        0x34 -> :sswitch_7
        0x46 -> :sswitch_8
        0x47 -> :sswitch_9
        0x48 -> :sswitch_a
        0x49 -> :sswitch_b
        0x4a -> :sswitch_c
        0x4b -> :sswitch_d
        0x4c -> :sswitch_e
    .end sparse-switch
.end method
