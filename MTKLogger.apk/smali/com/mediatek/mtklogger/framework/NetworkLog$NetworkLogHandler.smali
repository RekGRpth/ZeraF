.class Lcom/mediatek/mtklogger/framework/NetworkLog$NetworkLogHandler;
.super Lcom/mediatek/mtklogger/framework/LogInstance$LogHandler;
.source "NetworkLog.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/mediatek/mtklogger/framework/NetworkLog;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = "NetworkLogHandler"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/mediatek/mtklogger/framework/NetworkLog;


# direct methods
.method constructor <init>(Lcom/mediatek/mtklogger/framework/NetworkLog;)V
    .locals 0

    iput-object p1, p0, Lcom/mediatek/mtklogger/framework/NetworkLog$NetworkLogHandler;->this$0:Lcom/mediatek/mtklogger/framework/NetworkLog;

    invoke-direct {p0, p1}, Lcom/mediatek/mtklogger/framework/LogInstance$LogHandler;-><init>(Lcom/mediatek/mtklogger/framework/LogInstance;)V

    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 25
    .param p1    # Landroid/os/Message;

    const-string v20, "MTKLogger/NetworkLog"

    new-instance v21, Ljava/lang/StringBuilder;

    invoke-direct/range {v21 .. v21}, Ljava/lang/StringBuilder;-><init>()V

    const-string v22, "Handle receive message, what="

    invoke-virtual/range {v21 .. v22}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v21

    move-object/from16 v0, p1

    iget v0, v0, Landroid/os/Message;->what:I

    move/from16 v22, v0

    invoke-virtual/range {v21 .. v22}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v21

    invoke-virtual/range {v21 .. v21}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v21

    invoke-static/range {v20 .. v21}, Lcom/mediatek/mtklogger/utils/Utils;->logi(Ljava/lang/String;Ljava/lang/String;)V

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/mediatek/mtklogger/framework/NetworkLog$NetworkLogHandler;->this$0:Lcom/mediatek/mtklogger/framework/NetworkLog;

    move-object/from16 v20, v0

    # getter for: Lcom/mediatek/mtklogger/framework/NetworkLog;->bConnected:Z
    invoke-static/range {v20 .. v20}, Lcom/mediatek/mtklogger/framework/NetworkLog;->access$000(Lcom/mediatek/mtklogger/framework/NetworkLog;)Z

    move-result v20

    if-nez v20, :cond_1

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/mediatek/mtklogger/framework/NetworkLog$NetworkLogHandler;->this$0:Lcom/mediatek/mtklogger/framework/NetworkLog;

    move-object/from16 v20, v0

    move-object/from16 v0, v20

    iget-object v0, v0, Lcom/mediatek/mtklogger/framework/NetworkLog;->mLogConnection:Lcom/mediatek/mtklogger/framework/LogConnection;

    move-object/from16 v20, v0

    if-nez v20, :cond_0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/mediatek/mtklogger/framework/NetworkLog$NetworkLogHandler;->this$0:Lcom/mediatek/mtklogger/framework/NetworkLog;

    move-object/from16 v20, v0

    new-instance v21, Lcom/mediatek/mtklogger/framework/NetworkLog$NetworkLogConnection;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/mediatek/mtklogger/framework/NetworkLog$NetworkLogHandler;->this$0:Lcom/mediatek/mtklogger/framework/NetworkLog;

    move-object/from16 v22, v0

    const-string v23, "netdiag"

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/mediatek/mtklogger/framework/NetworkLog$NetworkLogHandler;->this$0:Lcom/mediatek/mtklogger/framework/NetworkLog;

    move-object/from16 v24, v0

    move-object/from16 v0, v24

    iget-object v0, v0, Lcom/mediatek/mtklogger/framework/NetworkLog;->mHandler:Lcom/mediatek/mtklogger/framework/LogInstance$LogHandler;

    move-object/from16 v24, v0

    invoke-direct/range {v21 .. v24}, Lcom/mediatek/mtklogger/framework/NetworkLog$NetworkLogConnection;-><init>(Lcom/mediatek/mtklogger/framework/NetworkLog;Ljava/lang/String;Landroid/os/Handler;)V

    move-object/from16 v0, v21

    move-object/from16 v1, v20

    iput-object v0, v1, Lcom/mediatek/mtklogger/framework/NetworkLog;->mLogConnection:Lcom/mediatek/mtklogger/framework/LogConnection;

    :cond_0
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/mediatek/mtklogger/framework/NetworkLog$NetworkLogHandler;->this$0:Lcom/mediatek/mtklogger/framework/NetworkLog;

    move-object/from16 v20, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/mediatek/mtklogger/framework/NetworkLog$NetworkLogHandler;->this$0:Lcom/mediatek/mtklogger/framework/NetworkLog;

    move-object/from16 v21, v0

    invoke-virtual/range {v21 .. v21}, Lcom/mediatek/mtklogger/framework/NetworkLog;->initLogConnection()Z

    move-result v21

    # setter for: Lcom/mediatek/mtklogger/framework/NetworkLog;->bConnected:Z
    invoke-static/range {v20 .. v21}, Lcom/mediatek/mtklogger/framework/NetworkLog;->access$002(Lcom/mediatek/mtklogger/framework/NetworkLog;Z)Z

    :cond_1
    move-object/from16 v0, p1

    iget-object v4, v0, Landroid/os/Message;->obj:Ljava/lang/Object;

    const/4 v3, 0x0

    if-eqz v4, :cond_2

    instance-of v0, v4, Ljava/lang/String;

    move/from16 v20, v0

    if-eqz v20, :cond_2

    move-object v3, v4

    check-cast v3, Ljava/lang/String;

    :cond_2
    move-object/from16 v0, p1

    iget v0, v0, Landroid/os/Message;->what:I

    move/from16 v20, v0

    sparse-switch v20, :sswitch_data_0

    move-object/from16 v0, p1

    iget v0, v0, Landroid/os/Message;->what:I

    move/from16 v20, v0

    const/16 v21, 0x20

    move/from16 v0, v20

    move/from16 v1, v21

    if-le v0, v1, :cond_1d

    move-object/from16 v0, p1

    iget v0, v0, Landroid/os/Message;->what:I

    move/from16 v20, v0

    add-int/lit8 v16, v20, -0x20

    const-string v9, ""

    move-object/from16 v0, p1

    iget-object v10, v0, Landroid/os/Message;->obj:Ljava/lang/Object;

    if-eqz v10, :cond_3

    instance-of v0, v10, Ljava/lang/String;

    move/from16 v20, v0

    if-eqz v20, :cond_3

    move-object v9, v10

    check-cast v9, Ljava/lang/String;

    :cond_3
    const-string v20, "MTKLogger/NetworkLog"

    new-instance v21, Ljava/lang/StringBuilder;

    invoke-direct/range {v21 .. v21}, Ljava/lang/StringBuilder;-><init>()V

    const-string v22, "Resp type: "

    invoke-virtual/range {v21 .. v22}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v21

    move-object/from16 v0, v21

    move/from16 v1, v16

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v21

    const-string v22, ", failReason string: ["

    invoke-virtual/range {v21 .. v22}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v21

    move-object/from16 v0, v21

    invoke-virtual {v0, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v21

    const-string v22, "]"

    invoke-virtual/range {v21 .. v22}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v21

    invoke-virtual/range {v21 .. v21}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v21

    invoke-static/range {v20 .. v21}, Lcom/mediatek/mtklogger/utils/Utils;->logi(Ljava/lang/String;Ljava/lang/String;)V

    const/16 v20, 0x1

    move/from16 v0, v16

    move/from16 v1, v20

    if-eq v0, v1, :cond_4

    const/16 v20, 0x2

    move/from16 v0, v16

    move/from16 v1, v20

    if-eq v0, v1, :cond_4

    const/16 v20, 0x4

    move/from16 v0, v16

    move/from16 v1, v20

    if-eq v0, v1, :cond_4

    const/16 v20, 0x7

    move/from16 v0, v16

    move/from16 v1, v20

    if-ne v0, v1, :cond_1c

    :cond_4
    invoke-static {v9}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v20

    if-nez v20, :cond_19

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/mediatek/mtklogger/framework/NetworkLog$NetworkLogHandler;->this$0:Lcom/mediatek/mtklogger/framework/NetworkLog;

    move-object/from16 v20, v0

    const/16 v21, 0x0

    move-object/from16 v0, v20

    move/from16 v1, v21

    # invokes: Lcom/mediatek/mtklogger/framework/NetworkLog;->notifyServiceStatus(ILjava/lang/String;)V
    invoke-static {v0, v1, v9}, Lcom/mediatek/mtklogger/framework/NetworkLog;->access$100(Lcom/mediatek/mtklogger/framework/NetworkLog;ILjava/lang/String;)V

    :cond_5
    :goto_0
    return-void

    :sswitch_0
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/mediatek/mtklogger/framework/NetworkLog$NetworkLogHandler;->this$0:Lcom/mediatek/mtklogger/framework/NetworkLog;

    move-object/from16 v20, v0

    # getter for: Lcom/mediatek/mtklogger/framework/NetworkLog;->bConnected:Z
    invoke-static/range {v20 .. v20}, Lcom/mediatek/mtklogger/framework/NetworkLog;->access$000(Lcom/mediatek/mtklogger/framework/NetworkLog;)Z

    move-result v20

    if-nez v20, :cond_7

    const-string v20, "MTKLogger/NetworkLog"

    const-string v21, "Fail to establish connection to native layer."

    invoke-static/range {v20 .. v21}, Lcom/mediatek/mtklogger/utils/Utils;->loge(Ljava/lang/String;Ljava/lang/String;)V

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/mediatek/mtklogger/framework/NetworkLog$NetworkLogHandler;->this$0:Lcom/mediatek/mtklogger/framework/NetworkLog;

    move-object/from16 v20, v0

    move-object/from16 v0, v20

    iget-object v0, v0, Lcom/mediatek/mtklogger/framework/NetworkLog;->mSharedPreferences:Landroid/content/SharedPreferences;

    move-object/from16 v20, v0

    if-eqz v20, :cond_6

    const/16 v20, 0x1

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/mediatek/mtklogger/framework/NetworkLog$NetworkLogHandler;->this$0:Lcom/mediatek/mtklogger/framework/NetworkLog;

    move-object/from16 v21, v0

    move-object/from16 v0, v21

    iget-object v0, v0, Lcom/mediatek/mtklogger/framework/NetworkLog;->mSharedPreferences:Landroid/content/SharedPreferences;

    move-object/from16 v21, v0

    const-string v22, "networklog_enable"

    const/16 v23, 0x1

    invoke-interface/range {v21 .. v23}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v21

    move/from16 v0, v20

    move/from16 v1, v21

    if-ne v0, v1, :cond_6

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/mediatek/mtklogger/framework/NetworkLog$NetworkLogHandler;->this$0:Lcom/mediatek/mtklogger/framework/NetworkLog;

    move-object/from16 v20, v0

    move-object/from16 v0, v20

    iget-object v0, v0, Lcom/mediatek/mtklogger/framework/NetworkLog;->mSharedPreferences:Landroid/content/SharedPreferences;

    move-object/from16 v20, v0

    invoke-interface/range {v20 .. v20}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v20

    const-string v21, "networklog_enable"

    const/16 v22, 0x0

    invoke-interface/range {v20 .. v22}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    move-result-object v20

    invoke-interface/range {v20 .. v20}, Landroid/content/SharedPreferences$Editor;->commit()Z

    :cond_6
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/mediatek/mtklogger/framework/NetworkLog$NetworkLogHandler;->this$0:Lcom/mediatek/mtklogger/framework/NetworkLog;

    move-object/from16 v20, v0

    const/16 v21, 0x0

    const-string v22, "1"

    # invokes: Lcom/mediatek/mtklogger/framework/NetworkLog;->notifyServiceStatus(ILjava/lang/String;)V
    invoke-static/range {v20 .. v22}, Lcom/mediatek/mtklogger/framework/NetworkLog;->access$100(Lcom/mediatek/mtklogger/framework/NetworkLog;ILjava/lang/String;)V

    goto :goto_0

    :cond_7
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/mediatek/mtklogger/framework/NetworkLog$NetworkLogHandler;->this$0:Lcom/mediatek/mtklogger/framework/NetworkLog;

    move-object/from16 v20, v0

    invoke-virtual/range {v20 .. v20}, Lcom/mediatek/mtklogger/framework/NetworkLog;->getLogStorageState()I

    move-result v14

    const/16 v20, 0x1

    move/from16 v0, v20

    if-eq v14, v0, :cond_a

    const-string v20, "MTKLogger/NetworkLog"

    const-string v21, "Log storage is not ready yet."

    invoke-static/range {v20 .. v21}, Lcom/mediatek/mtklogger/utils/Utils;->loge(Ljava/lang/String;Ljava/lang/String;)V

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/mediatek/mtklogger/framework/NetworkLog$NetworkLogHandler;->this$0:Lcom/mediatek/mtklogger/framework/NetworkLog;

    move-object/from16 v20, v0

    move-object/from16 v0, v20

    iget-object v0, v0, Lcom/mediatek/mtklogger/framework/NetworkLog;->mSharedPreferences:Landroid/content/SharedPreferences;

    move-object/from16 v20, v0

    if-eqz v20, :cond_8

    const/16 v20, 0x1

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/mediatek/mtklogger/framework/NetworkLog$NetworkLogHandler;->this$0:Lcom/mediatek/mtklogger/framework/NetworkLog;

    move-object/from16 v21, v0

    move-object/from16 v0, v21

    iget-object v0, v0, Lcom/mediatek/mtklogger/framework/NetworkLog;->mSharedPreferences:Landroid/content/SharedPreferences;

    move-object/from16 v21, v0

    const-string v22, "networklog_enable"

    const/16 v23, 0x1

    invoke-interface/range {v21 .. v23}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v21

    move/from16 v0, v20

    move/from16 v1, v21

    if-ne v0, v1, :cond_8

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/mediatek/mtklogger/framework/NetworkLog$NetworkLogHandler;->this$0:Lcom/mediatek/mtklogger/framework/NetworkLog;

    move-object/from16 v20, v0

    move-object/from16 v0, v20

    iget-object v0, v0, Lcom/mediatek/mtklogger/framework/NetworkLog;->mSharedPreferences:Landroid/content/SharedPreferences;

    move-object/from16 v20, v0

    invoke-interface/range {v20 .. v20}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v20

    const-string v21, "networklog_enable"

    const/16 v22, 0x0

    invoke-interface/range {v20 .. v22}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    move-result-object v20

    invoke-interface/range {v20 .. v20}, Landroid/content/SharedPreferences$Editor;->commit()Z

    :cond_8
    const/16 v20, -0x1

    move/from16 v0, v20

    if-ne v14, v0, :cond_9

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/mediatek/mtklogger/framework/NetworkLog$NetworkLogHandler;->this$0:Lcom/mediatek/mtklogger/framework/NetworkLog;

    move-object/from16 v20, v0

    const/16 v21, 0x0

    const-string v22, "2"

    # invokes: Lcom/mediatek/mtklogger/framework/NetworkLog;->notifyServiceStatus(ILjava/lang/String;)V
    invoke-static/range {v20 .. v22}, Lcom/mediatek/mtklogger/framework/NetworkLog;->access$100(Lcom/mediatek/mtklogger/framework/NetworkLog;ILjava/lang/String;)V

    goto/16 :goto_0

    :cond_9
    const/16 v20, -0x2

    move/from16 v0, v20

    if-ne v14, v0, :cond_5

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/mediatek/mtklogger/framework/NetworkLog$NetworkLogHandler;->this$0:Lcom/mediatek/mtklogger/framework/NetworkLog;

    move-object/from16 v20, v0

    const/16 v21, 0x0

    const-string v22, "3"

    # invokes: Lcom/mediatek/mtklogger/framework/NetworkLog;->notifyServiceStatus(ILjava/lang/String;)V
    invoke-static/range {v20 .. v22}, Lcom/mediatek/mtklogger/framework/NetworkLog;->access$100(Lcom/mediatek/mtklogger/framework/NetworkLog;ILjava/lang/String;)V

    goto/16 :goto_0

    :cond_a
    sget-object v20, Lcom/mediatek/mtklogger/framework/LogInstance;->mContext:Landroid/content/Context;

    invoke-static/range {v20 .. v20}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v7

    const-string v20, "networklog_logsize"

    const-string v21, ""

    move-object/from16 v0, v20

    move-object/from16 v1, v21

    invoke-interface {v7, v0, v1}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v13

    const/16 v12, 0xc8

    invoke-static {v13}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v20

    if-nez v20, :cond_b

    :try_start_0
    invoke-static {v13}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v19

    if-lez v19, :cond_b

    move/from16 v12, v19

    :cond_b
    :goto_1
    const-string v17, ""

    invoke-static {}, Lcom/mediatek/mtklogger/utils/Utils;->getLogPathType()Ljava/lang/String;

    move-result-object v11

    const-string v20, "/data"

    move-object/from16 v0, v20

    invoke-virtual {v0, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v20

    if-eqz v20, :cond_c

    new-instance v20, Ljava/lang/StringBuilder;

    invoke-direct/range {v20 .. v20}, Ljava/lang/StringBuilder;-><init>()V

    const-string v21, "tcpdump_data_start_"

    invoke-virtual/range {v20 .. v21}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    move-object/from16 v0, v20

    invoke-virtual {v0, v12}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v20

    invoke-virtual/range {v20 .. v20}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v17

    :goto_2
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/mediatek/mtklogger/framework/NetworkLog$NetworkLogHandler;->this$0:Lcom/mediatek/mtklogger/framework/NetworkLog;

    move-object/from16 v20, v0

    move-object/from16 v0, v20

    iget-object v0, v0, Lcom/mediatek/mtklogger/framework/NetworkLog;->mLogConnection:Lcom/mediatek/mtklogger/framework/LogConnection;

    move-object/from16 v20, v0

    move-object/from16 v0, v20

    move-object/from16 v1, v17

    invoke-virtual {v0, v1}, Lcom/mediatek/mtklogger/framework/LogConnection;->sendCmd(Ljava/lang/String;)Z

    move-result v6

    if-nez v6, :cond_5

    const-string v20, "MTKLogger/NetworkLog"

    const-string v21, "Send start command to native layer fail, maybe connection has already be lost."

    invoke-static/range {v20 .. v21}, Lcom/mediatek/mtklogger/utils/Utils;->loge(Ljava/lang/String;Ljava/lang/String;)V

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/mediatek/mtklogger/framework/NetworkLog$NetworkLogHandler;->this$0:Lcom/mediatek/mtklogger/framework/NetworkLog;

    move-object/from16 v20, v0

    const/16 v21, 0x0

    const-string v22, "4"

    # invokes: Lcom/mediatek/mtklogger/framework/NetworkLog;->notifyServiceStatus(ILjava/lang/String;)V
    invoke-static/range {v20 .. v22}, Lcom/mediatek/mtklogger/framework/NetworkLog;->access$100(Lcom/mediatek/mtklogger/framework/NetworkLog;ILjava/lang/String;)V

    goto/16 :goto_0

    :cond_c
    new-instance v20, Ljava/lang/StringBuilder;

    invoke-direct/range {v20 .. v20}, Ljava/lang/StringBuilder;-><init>()V

    const-string v21, "tcpdump_sdcard_start_"

    invoke-virtual/range {v20 .. v21}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    move-object/from16 v0, v20

    invoke-virtual {v0, v12}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v20

    invoke-virtual/range {v20 .. v20}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v17

    goto :goto_2

    :sswitch_1
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/mediatek/mtklogger/framework/NetworkLog$NetworkLogHandler;->this$0:Lcom/mediatek/mtklogger/framework/NetworkLog;

    move-object/from16 v20, v0

    # getter for: Lcom/mediatek/mtklogger/framework/NetworkLog;->bConnected:Z
    invoke-static/range {v20 .. v20}, Lcom/mediatek/mtklogger/framework/NetworkLog;->access$000(Lcom/mediatek/mtklogger/framework/NetworkLog;)Z

    move-result v20

    if-eqz v20, :cond_15

    const/4 v6, 0x0

    const-string v18, ""

    invoke-static {}, Lcom/mediatek/mtklogger/utils/Utils;->getLogPathType()Ljava/lang/String;

    move-result-object v11

    sget-object v20, Lcom/mediatek/mtklogger/framework/LogInstance;->mContext:Landroid/content/Context;

    invoke-static/range {v20 .. v20}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v7

    const-string v20, "networklog_ping_flag"

    const/16 v21, 0x0

    move-object/from16 v0, v20

    move/from16 v1, v21

    invoke-interface {v7, v0, v1}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v15

    const-string v20, "MTKLogger/NetworkLog"

    new-instance v21, Ljava/lang/StringBuilder;

    invoke-direct/range {v21 .. v21}, Ljava/lang/StringBuilder;-><init>()V

    const-string v22, "NetworkLog ping at stop time flag = "

    invoke-virtual/range {v21 .. v22}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v21

    move-object/from16 v0, v21

    invoke-virtual {v0, v15}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v21

    invoke-virtual/range {v21 .. v21}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v21

    invoke-static/range {v20 .. v21}, Lcom/mediatek/mtklogger/utils/Utils;->logi(Ljava/lang/String;Ljava/lang/String;)V

    const-string v20, "/data"

    move-object/from16 v0, v20

    invoke-virtual {v0, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v20

    if-eqz v20, :cond_11

    if-eqz v15, :cond_10

    const-string v18, "tcpdump_data_stop"

    :goto_3
    invoke-static {v3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v20

    if-nez v20, :cond_14

    const-string v20, "MTKLogger/NetworkLog"

    new-instance v21, Ljava/lang/StringBuilder;

    invoke-direct/range {v21 .. v21}, Ljava/lang/StringBuilder;-><init>()V

    const-string v22, "Network log stop reason = "

    invoke-virtual/range {v21 .. v22}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v21

    move-object/from16 v0, v21

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v21

    invoke-virtual/range {v21 .. v21}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v21

    invoke-static/range {v20 .. v21}, Lcom/mediatek/mtklogger/utils/Utils;->logi(Ljava/lang/String;Ljava/lang/String;)V

    const-string v20, "storage_full_or_lost"

    move-object/from16 v0, v20

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v20

    if-nez v20, :cond_d

    const-string v20, "sd_timeout"

    move-object/from16 v0, v20

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v20

    if-nez v20, :cond_d

    const-string v20, "folder_lost"

    move-object/from16 v0, v20

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v20

    if-eqz v20, :cond_13

    :cond_d
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/mediatek/mtklogger/framework/NetworkLog$NetworkLogHandler;->this$0:Lcom/mediatek/mtklogger/framework/NetworkLog;

    move-object/from16 v20, v0

    move-object/from16 v0, v20

    iget-object v0, v0, Lcom/mediatek/mtklogger/framework/NetworkLog;->mLogConnection:Lcom/mediatek/mtklogger/framework/LogConnection;

    move-object/from16 v20, v0

    const-string v21, "tcpdump_sdcard_check"

    invoke-virtual/range {v20 .. v21}, Lcom/mediatek/mtklogger/framework/LogConnection;->sendCmd(Ljava/lang/String;)Z

    move-result v6

    if-eqz v6, :cond_f

    const-string v20, "MTKLogger/NetworkLog"

    const-string v21, "Native will not response to check command, assum it to be successful."

    invoke-static/range {v20 .. v21}, Lcom/mediatek/mtklogger/utils/Utils;->logd(Ljava/lang/String;Ljava/lang/String;)V

    const-string v8, "7"

    const-string v20, "folder_lost"

    move-object/from16 v0, v20

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v20

    if-eqz v20, :cond_e

    const-string v8, "8"

    :cond_e
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/mediatek/mtklogger/framework/NetworkLog$NetworkLogHandler;->this$0:Lcom/mediatek/mtklogger/framework/NetworkLog;

    move-object/from16 v20, v0

    const/16 v21, 0x0

    move-object/from16 v0, v20

    move/from16 v1, v21

    # invokes: Lcom/mediatek/mtklogger/framework/NetworkLog;->notifyServiceStatus(ILjava/lang/String;)V
    invoke-static {v0, v1, v8}, Lcom/mediatek/mtklogger/framework/NetworkLog;->access$100(Lcom/mediatek/mtklogger/framework/NetworkLog;ILjava/lang/String;)V

    :cond_f
    :goto_4
    if-nez v6, :cond_5

    const-string v20, "MTKLogger/NetworkLog"

    const-string v21, "Send stop command to native layer fail, maybe connection has already be lost."

    invoke-static/range {v20 .. v21}, Lcom/mediatek/mtklogger/utils/Utils;->loge(Ljava/lang/String;Ljava/lang/String;)V

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/mediatek/mtklogger/framework/NetworkLog$NetworkLogHandler;->this$0:Lcom/mediatek/mtklogger/framework/NetworkLog;

    move-object/from16 v20, v0

    const/16 v21, 0x0

    const-string v22, "4"

    # invokes: Lcom/mediatek/mtklogger/framework/NetworkLog;->notifyServiceStatus(ILjava/lang/String;)V
    invoke-static/range {v20 .. v22}, Lcom/mediatek/mtklogger/framework/NetworkLog;->access$100(Lcom/mediatek/mtklogger/framework/NetworkLog;ILjava/lang/String;)V

    goto/16 :goto_0

    :cond_10
    const-string v18, "tcpdump_data_stop_noping"

    goto/16 :goto_3

    :cond_11
    if-eqz v15, :cond_12

    const-string v18, "tcpdump_sdcard_stop"

    goto/16 :goto_3

    :cond_12
    const-string v18, "tcpdump_sdcard_stop_noping"

    goto/16 :goto_3

    :cond_13
    const-string v20, "MTKLogger/NetworkLog"

    const-string v21, "Unsupported stop reason, ignore it."

    invoke-static/range {v20 .. v21}, Lcom/mediatek/mtklogger/utils/Utils;->logw(Ljava/lang/String;Ljava/lang/String;)V

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/mediatek/mtklogger/framework/NetworkLog$NetworkLogHandler;->this$0:Lcom/mediatek/mtklogger/framework/NetworkLog;

    move-object/from16 v20, v0

    move-object/from16 v0, v20

    iget-object v0, v0, Lcom/mediatek/mtklogger/framework/NetworkLog;->mLogConnection:Lcom/mediatek/mtklogger/framework/LogConnection;

    move-object/from16 v20, v0

    move-object/from16 v0, v20

    move-object/from16 v1, v18

    invoke-virtual {v0, v1}, Lcom/mediatek/mtklogger/framework/LogConnection;->sendCmd(Ljava/lang/String;)Z

    move-result v6

    goto :goto_4

    :cond_14
    const-string v20, "MTKLogger/NetworkLog"

    const-string v21, "Normally stop network log with stop command"

    invoke-static/range {v20 .. v21}, Lcom/mediatek/mtklogger/utils/Utils;->logd(Ljava/lang/String;Ljava/lang/String;)V

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/mediatek/mtklogger/framework/NetworkLog$NetworkLogHandler;->this$0:Lcom/mediatek/mtklogger/framework/NetworkLog;

    move-object/from16 v20, v0

    move-object/from16 v0, v20

    iget-object v0, v0, Lcom/mediatek/mtklogger/framework/NetworkLog;->mLogConnection:Lcom/mediatek/mtklogger/framework/LogConnection;

    move-object/from16 v20, v0

    move-object/from16 v0, v20

    move-object/from16 v1, v18

    invoke-virtual {v0, v1}, Lcom/mediatek/mtklogger/framework/LogConnection;->sendCmd(Ljava/lang/String;)Z

    move-result v6

    goto :goto_4

    :cond_15
    const-string v20, "MTKLogger/NetworkLog"

    const-string v21, "Have not connected to native layer, just ignore this command"

    invoke-static/range {v20 .. v21}, Lcom/mediatek/mtklogger/utils/Utils;->logw(Ljava/lang/String;Ljava/lang/String;)V

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/mediatek/mtklogger/framework/NetworkLog$NetworkLogHandler;->this$0:Lcom/mediatek/mtklogger/framework/NetworkLog;

    move-object/from16 v20, v0

    const/16 v21, 0x0

    const-string v22, "1"

    # invokes: Lcom/mediatek/mtklogger/framework/NetworkLog;->notifyServiceStatus(ILjava/lang/String;)V
    invoke-static/range {v20 .. v22}, Lcom/mediatek/mtklogger/framework/NetworkLog;->access$100(Lcom/mediatek/mtklogger/framework/NetworkLog;ILjava/lang/String;)V

    goto/16 :goto_0

    :sswitch_2
    move-object/from16 v0, p1

    iget-object v2, v0, Landroid/os/Message;->obj:Ljava/lang/Object;

    if-nez v2, :cond_16

    const-string v20, "MTKLogger/NetworkLog"

    const-string v21, "Fail to get start shell command."

    invoke-static/range {v20 .. v21}, Lcom/mediatek/mtklogger/utils/Utils;->loge(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    :cond_16
    move-object v5, v2

    check-cast v5, Ljava/lang/String;

    invoke-static {v5}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v20

    if-eqz v20, :cond_17

    const-string v20, "MTKLogger/NetworkLog"

    const-string v21, "Please give me a not null command to run in shell."

    invoke-static/range {v20 .. v21}, Lcom/mediatek/mtklogger/utils/Utils;->logw(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    :cond_17
    const-string v20, "MTKLogger/NetworkLog"

    new-instance v21, Ljava/lang/StringBuilder;

    invoke-direct/range {v21 .. v21}, Ljava/lang/StringBuilder;-><init>()V

    const-string v22, "Send command["

    invoke-virtual/range {v21 .. v22}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v21

    move-object/from16 v0, v21

    invoke-virtual {v0, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v21

    const-string v22, "] to shell now."

    invoke-virtual/range {v21 .. v22}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v21

    invoke-virtual/range {v21 .. v21}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v21

    invoke-static/range {v20 .. v21}, Lcom/mediatek/mtklogger/utils/Utils;->logd(Ljava/lang/String;Ljava/lang/String;)V

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/mediatek/mtklogger/framework/NetworkLog$NetworkLogHandler;->this$0:Lcom/mediatek/mtklogger/framework/NetworkLog;

    move-object/from16 v20, v0

    move-object/from16 v0, v20

    iget-object v0, v0, Lcom/mediatek/mtklogger/framework/NetworkLog;->mLogConnection:Lcom/mediatek/mtklogger/framework/LogConnection;

    move-object/from16 v20, v0

    new-instance v21, Ljava/lang/StringBuilder;

    invoke-direct/range {v21 .. v21}, Ljava/lang/StringBuilder;-><init>()V

    const-string v22, "runshell_command_start_"

    invoke-virtual/range {v21 .. v22}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v21

    move-object/from16 v0, v21

    invoke-virtual {v0, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v21

    invoke-virtual/range {v21 .. v21}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v21

    invoke-virtual/range {v20 .. v21}, Lcom/mediatek/mtklogger/framework/LogConnection;->sendCmd(Ljava/lang/String;)Z

    goto/16 :goto_0

    :sswitch_3
    const-string v20, "MTKLogger/NetworkLog"

    const-string v21, "Stop former sent shell command now."

    invoke-static/range {v20 .. v21}, Lcom/mediatek/mtklogger/utils/Utils;->logd(Ljava/lang/String;Ljava/lang/String;)V

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/mediatek/mtklogger/framework/NetworkLog$NetworkLogHandler;->this$0:Lcom/mediatek/mtklogger/framework/NetworkLog;

    move-object/from16 v20, v0

    move-object/from16 v0, v20

    iget-object v0, v0, Lcom/mediatek/mtklogger/framework/NetworkLog;->mLogConnection:Lcom/mediatek/mtklogger/framework/LogConnection;

    move-object/from16 v20, v0

    const-string v21, "runshell_command_stop_"

    invoke-virtual/range {v20 .. v21}, Lcom/mediatek/mtklogger/framework/LogConnection;->sendCmd(Ljava/lang/String;)Z

    goto/16 :goto_0

    :sswitch_4
    const-string v20, "MTKLogger/NetworkLog"

    new-instance v21, Ljava/lang/StringBuilder;

    invoke-direct/range {v21 .. v21}, Ljava/lang/StringBuilder;-><init>()V

    const-string v22, "Receive adb command["

    invoke-virtual/range {v21 .. v22}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v21

    move-object/from16 v0, v21

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v21

    const-string v22, "]"

    invoke-virtual/range {v21 .. v22}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v21

    invoke-virtual/range {v21 .. v21}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v21

    invoke-static/range {v20 .. v21}, Lcom/mediatek/mtklogger/utils/Utils;->logd(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    :sswitch_5
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/mediatek/mtklogger/framework/NetworkLog$NetworkLogHandler;->this$0:Lcom/mediatek/mtklogger/framework/NetworkLog;

    move-object/from16 v20, v0

    # getter for: Lcom/mediatek/mtklogger/framework/NetworkLog;->bConnected:Z
    invoke-static/range {v20 .. v20}, Lcom/mediatek/mtklogger/framework/NetworkLog;->access$000(Lcom/mediatek/mtklogger/framework/NetworkLog;)Z

    move-result v20

    if-nez v20, :cond_5

    const-string v20, "MTKLogger/NetworkLog"

    const-string v21, "Reconnect to native layer fail! Mark log status as stopped."

    invoke-static/range {v20 .. v21}, Lcom/mediatek/mtklogger/utils/Utils;->logw(Ljava/lang/String;Ljava/lang/String;)V

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/mediatek/mtklogger/framework/NetworkLog$NetworkLogHandler;->this$0:Lcom/mediatek/mtklogger/framework/NetworkLog;

    move-object/from16 v20, v0

    const/16 v21, 0x0

    const-string v22, "1"

    # invokes: Lcom/mediatek/mtklogger/framework/NetworkLog;->notifyServiceStatus(ILjava/lang/String;)V
    invoke-static/range {v20 .. v22}, Lcom/mediatek/mtklogger/framework/NetworkLog;->access$100(Lcom/mediatek/mtklogger/framework/NetworkLog;ILjava/lang/String;)V

    goto/16 :goto_0

    :sswitch_6
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/mediatek/mtklogger/framework/NetworkLog$NetworkLogHandler;->this$0:Lcom/mediatek/mtklogger/framework/NetworkLog;

    move-object/from16 v20, v0

    move-object/from16 v0, v20

    iget-object v0, v0, Lcom/mediatek/mtklogger/framework/NetworkLog;->mLogConnection:Lcom/mediatek/mtklogger/framework/LogConnection;

    move-object/from16 v20, v0

    if-eqz v20, :cond_18

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/mediatek/mtklogger/framework/NetworkLog$NetworkLogHandler;->this$0:Lcom/mediatek/mtklogger/framework/NetworkLog;

    move-object/from16 v20, v0

    move-object/from16 v0, v20

    iget-object v0, v0, Lcom/mediatek/mtklogger/framework/NetworkLog;->mLogConnection:Lcom/mediatek/mtklogger/framework/LogConnection;

    move-object/from16 v20, v0

    invoke-virtual/range {v20 .. v20}, Lcom/mediatek/mtklogger/framework/LogConnection;->stop()V

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/mediatek/mtklogger/framework/NetworkLog$NetworkLogHandler;->this$0:Lcom/mediatek/mtklogger/framework/NetworkLog;

    move-object/from16 v20, v0

    const/16 v21, 0x0

    move-object/from16 v0, v21

    move-object/from16 v1, v20

    iput-object v0, v1, Lcom/mediatek/mtklogger/framework/NetworkLog;->mLogConnection:Lcom/mediatek/mtklogger/framework/LogConnection;

    :cond_18
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/mediatek/mtklogger/framework/NetworkLog$NetworkLogHandler;->this$0:Lcom/mediatek/mtklogger/framework/NetworkLog;

    move-object/from16 v20, v0

    const/16 v21, 0x0

    # setter for: Lcom/mediatek/mtklogger/framework/NetworkLog;->bConnected:Z
    invoke-static/range {v20 .. v21}, Lcom/mediatek/mtklogger/framework/NetworkLog;->access$002(Lcom/mediatek/mtklogger/framework/NetworkLog;Z)Z

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/mediatek/mtklogger/framework/NetworkLog$NetworkLogHandler;->this$0:Lcom/mediatek/mtklogger/framework/NetworkLog;

    move-object/from16 v20, v0

    const/16 v21, 0x0

    const-string v22, "5"

    # invokes: Lcom/mediatek/mtklogger/framework/NetworkLog;->notifyServiceStatus(ILjava/lang/String;)V
    invoke-static/range {v20 .. v22}, Lcom/mediatek/mtklogger/framework/NetworkLog;->access$100(Lcom/mediatek/mtklogger/framework/NetworkLog;ILjava/lang/String;)V

    goto/16 :goto_0

    :cond_19
    const/16 v20, 0x1

    move/from16 v0, v16

    move/from16 v1, v20

    if-ne v0, v1, :cond_1a

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/mediatek/mtklogger/framework/NetworkLog$NetworkLogHandler;->this$0:Lcom/mediatek/mtklogger/framework/NetworkLog;

    move-object/from16 v20, v0

    const/16 v21, 0x1

    const-string v22, ""

    # invokes: Lcom/mediatek/mtklogger/framework/NetworkLog;->notifyServiceStatus(ILjava/lang/String;)V
    invoke-static/range {v20 .. v22}, Lcom/mediatek/mtklogger/framework/NetworkLog;->access$100(Lcom/mediatek/mtklogger/framework/NetworkLog;ILjava/lang/String;)V

    goto/16 :goto_0

    :cond_1a
    const/16 v20, 0x1

    move/from16 v0, v16

    move/from16 v1, v20

    if-eq v0, v1, :cond_1b

    const/16 v20, 0x2

    move/from16 v0, v16

    move/from16 v1, v20

    if-ne v0, v1, :cond_5

    :cond_1b
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/mediatek/mtklogger/framework/NetworkLog$NetworkLogHandler;->this$0:Lcom/mediatek/mtklogger/framework/NetworkLog;

    move-object/from16 v20, v0

    const/16 v21, 0x0

    const-string v22, ""

    # invokes: Lcom/mediatek/mtklogger/framework/NetworkLog;->notifyServiceStatus(ILjava/lang/String;)V
    invoke-static/range {v20 .. v22}, Lcom/mediatek/mtklogger/framework/NetworkLog;->access$100(Lcom/mediatek/mtklogger/framework/NetworkLog;ILjava/lang/String;)V

    goto/16 :goto_0

    :cond_1c
    const-string v20, "MTKLogger/NetworkLog"

    new-instance v21, Ljava/lang/StringBuilder;

    invoke-direct/range {v21 .. v21}, Ljava/lang/StringBuilder;-><init>()V

    const-string v22, "Ignore response whose type = "

    invoke-virtual/range {v21 .. v22}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v21

    move-object/from16 v0, v21

    move/from16 v1, v16

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v21

    invoke-virtual/range {v21 .. v21}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v21

    invoke-static/range {v20 .. v21}, Lcom/mediatek/mtklogger/utils/Utils;->logd(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    :cond_1d
    const-string v20, "MTKLogger/NetworkLog"

    const-string v21, "Current unsupported messge"

    invoke-static/range {v20 .. v21}, Lcom/mediatek/mtklogger/utils/Utils;->logw(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    :catch_0
    move-exception v20

    goto/16 :goto_1

    :sswitch_data_0
    .sparse-switch
        0x1 -> :sswitch_0
        0x3 -> :sswitch_1
        0x5 -> :sswitch_2
        0x6 -> :sswitch_3
        0x8 -> :sswitch_4
        0x9 -> :sswitch_5
        0x16 -> :sswitch_6
    .end sparse-switch
.end method
