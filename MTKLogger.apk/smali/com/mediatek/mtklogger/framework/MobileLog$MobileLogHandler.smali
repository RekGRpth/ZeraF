.class Lcom/mediatek/mtklogger/framework/MobileLog$MobileLogHandler;
.super Lcom/mediatek/mtklogger/framework/LogInstance$LogHandler;
.source "MobileLog.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/mediatek/mtklogger/framework/MobileLog;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = "MobileLogHandler"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/mediatek/mtklogger/framework/MobileLog;


# direct methods
.method constructor <init>(Lcom/mediatek/mtklogger/framework/MobileLog;)V
    .locals 0

    iput-object p1, p0, Lcom/mediatek/mtklogger/framework/MobileLog$MobileLogHandler;->this$0:Lcom/mediatek/mtklogger/framework/MobileLog;

    invoke-direct {p0, p1}, Lcom/mediatek/mtklogger/framework/LogInstance$LogHandler;-><init>(Lcom/mediatek/mtklogger/framework/LogInstance;)V

    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 13
    .param p1    # Landroid/os/Message;

    const-string v8, "MTKLogger/MobileLog"

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "Handle receive message, what="

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    iget v10, p1, Landroid/os/Message;->what:I

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v8, v9}, Lcom/mediatek/mtklogger/utils/Utils;->logi(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v8, p0, Lcom/mediatek/mtklogger/framework/MobileLog$MobileLogHandler;->this$0:Lcom/mediatek/mtklogger/framework/MobileLog;

    # getter for: Lcom/mediatek/mtklogger/framework/MobileLog;->bConnected:Z
    invoke-static {v8}, Lcom/mediatek/mtklogger/framework/MobileLog;->access$000(Lcom/mediatek/mtklogger/framework/MobileLog;)Z

    move-result v8

    if-nez v8, :cond_1

    iget-object v8, p0, Lcom/mediatek/mtklogger/framework/MobileLog$MobileLogHandler;->this$0:Lcom/mediatek/mtklogger/framework/MobileLog;

    iget-object v8, v8, Lcom/mediatek/mtklogger/framework/MobileLog;->mLogConnection:Lcom/mediatek/mtklogger/framework/LogConnection;

    if-nez v8, :cond_0

    iget-object v8, p0, Lcom/mediatek/mtklogger/framework/MobileLog$MobileLogHandler;->this$0:Lcom/mediatek/mtklogger/framework/MobileLog;

    new-instance v9, Lcom/mediatek/mtklogger/framework/MobileLog$MobileLogConnection;

    iget-object v10, p0, Lcom/mediatek/mtklogger/framework/MobileLog$MobileLogHandler;->this$0:Lcom/mediatek/mtklogger/framework/MobileLog;

    const-string v11, "mobilelogd"

    iget-object v12, p0, Lcom/mediatek/mtklogger/framework/MobileLog$MobileLogHandler;->this$0:Lcom/mediatek/mtklogger/framework/MobileLog;

    iget-object v12, v12, Lcom/mediatek/mtklogger/framework/MobileLog;->mHandler:Lcom/mediatek/mtklogger/framework/LogInstance$LogHandler;

    invoke-direct {v9, v10, v11, v12}, Lcom/mediatek/mtklogger/framework/MobileLog$MobileLogConnection;-><init>(Lcom/mediatek/mtklogger/framework/MobileLog;Ljava/lang/String;Landroid/os/Handler;)V

    iput-object v9, v8, Lcom/mediatek/mtklogger/framework/MobileLog;->mLogConnection:Lcom/mediatek/mtklogger/framework/LogConnection;

    :cond_0
    iget-object v8, p0, Lcom/mediatek/mtklogger/framework/MobileLog$MobileLogHandler;->this$0:Lcom/mediatek/mtklogger/framework/MobileLog;

    iget-object v9, p0, Lcom/mediatek/mtklogger/framework/MobileLog$MobileLogHandler;->this$0:Lcom/mediatek/mtklogger/framework/MobileLog;

    invoke-virtual {v9}, Lcom/mediatek/mtklogger/framework/MobileLog;->initLogConnection()Z

    move-result v9

    # setter for: Lcom/mediatek/mtklogger/framework/MobileLog;->bConnected:Z
    invoke-static {v8, v9}, Lcom/mediatek/mtklogger/framework/MobileLog;->access$002(Lcom/mediatek/mtklogger/framework/MobileLog;Z)Z

    :cond_1
    iget-object v1, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    const/4 v0, 0x0

    if-eqz v1, :cond_2

    instance-of v8, v1, Ljava/lang/String;

    if-eqz v8, :cond_2

    move-object v0, v1

    check-cast v0, Ljava/lang/String;

    :cond_2
    iget v8, p1, Landroid/os/Message;->what:I

    sparse-switch v8, :sswitch_data_0

    const-string v8, "MTKLogger/MobileLog"

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "Not supported message: "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    iget v10, p1, Landroid/os/Message;->what:I

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v8, v9}, Lcom/mediatek/mtklogger/utils/Utils;->logw(Ljava/lang/String;Ljava/lang/String;)V

    :cond_3
    :goto_0
    return-void

    :sswitch_0
    iget-object v8, p0, Lcom/mediatek/mtklogger/framework/MobileLog$MobileLogHandler;->this$0:Lcom/mediatek/mtklogger/framework/MobileLog;

    # getter for: Lcom/mediatek/mtklogger/framework/MobileLog;->bConnected:Z
    invoke-static {v8}, Lcom/mediatek/mtklogger/framework/MobileLog;->access$000(Lcom/mediatek/mtklogger/framework/MobileLog;)Z

    move-result v8

    if-nez v8, :cond_5

    const-string v8, "MTKLogger/MobileLog"

    const-string v9, "Fail to establish connection to native layer."

    invoke-static {v8, v9}, Lcom/mediatek/mtklogger/utils/Utils;->loge(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v8, p0, Lcom/mediatek/mtklogger/framework/MobileLog$MobileLogHandler;->this$0:Lcom/mediatek/mtklogger/framework/MobileLog;

    iget-object v8, v8, Lcom/mediatek/mtklogger/framework/MobileLog;->mSharedPreferences:Landroid/content/SharedPreferences;

    if-eqz v8, :cond_4

    const/4 v8, 0x1

    iget-object v9, p0, Lcom/mediatek/mtklogger/framework/MobileLog$MobileLogHandler;->this$0:Lcom/mediatek/mtklogger/framework/MobileLog;

    iget-object v9, v9, Lcom/mediatek/mtklogger/framework/MobileLog;->mSharedPreferences:Landroid/content/SharedPreferences;

    const-string v10, "mobilelog_enable"

    const/4 v11, 0x1

    invoke-interface {v9, v10, v11}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v9

    if-ne v8, v9, :cond_4

    iget-object v8, p0, Lcom/mediatek/mtklogger/framework/MobileLog$MobileLogHandler;->this$0:Lcom/mediatek/mtklogger/framework/MobileLog;

    iget-object v8, v8, Lcom/mediatek/mtklogger/framework/MobileLog;->mSharedPreferences:Landroid/content/SharedPreferences;

    invoke-interface {v8}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v8

    const-string v9, "mobilelog_enable"

    const/4 v10, 0x0

    invoke-interface {v8, v9, v10}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    move-result-object v8

    invoke-interface {v8}, Landroid/content/SharedPreferences$Editor;->commit()Z

    :cond_4
    iget-object v8, p0, Lcom/mediatek/mtklogger/framework/MobileLog$MobileLogHandler;->this$0:Lcom/mediatek/mtklogger/framework/MobileLog;

    const/4 v9, 0x0

    const-string v10, "1"

    # invokes: Lcom/mediatek/mtklogger/framework/MobileLog;->notifyServiceStatus(ILjava/lang/String;)V
    invoke-static {v8, v9, v10}, Lcom/mediatek/mtklogger/framework/MobileLog;->access$100(Lcom/mediatek/mtklogger/framework/MobileLog;ILjava/lang/String;)V

    goto :goto_0

    :cond_5
    iget-object v8, p0, Lcom/mediatek/mtklogger/framework/MobileLog$MobileLogHandler;->this$0:Lcom/mediatek/mtklogger/framework/MobileLog;

    invoke-virtual {v8}, Lcom/mediatek/mtklogger/framework/MobileLog;->getLogStorageState()I

    move-result v5

    const/4 v8, 0x1

    if-eq v5, v8, :cond_8

    const-string v8, "MTKLogger/MobileLog"

    const-string v9, "Log storage is not ready yet."

    invoke-static {v8, v9}, Lcom/mediatek/mtklogger/utils/Utils;->loge(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v8, p0, Lcom/mediatek/mtklogger/framework/MobileLog$MobileLogHandler;->this$0:Lcom/mediatek/mtklogger/framework/MobileLog;

    iget-object v8, v8, Lcom/mediatek/mtklogger/framework/MobileLog;->mSharedPreferences:Landroid/content/SharedPreferences;

    if-eqz v8, :cond_6

    const/4 v8, 0x1

    iget-object v9, p0, Lcom/mediatek/mtklogger/framework/MobileLog$MobileLogHandler;->this$0:Lcom/mediatek/mtklogger/framework/MobileLog;

    iget-object v9, v9, Lcom/mediatek/mtklogger/framework/MobileLog;->mSharedPreferences:Landroid/content/SharedPreferences;

    const-string v10, "mobilelog_enable"

    const/4 v11, 0x1

    invoke-interface {v9, v10, v11}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v9

    if-ne v8, v9, :cond_6

    iget-object v8, p0, Lcom/mediatek/mtklogger/framework/MobileLog$MobileLogHandler;->this$0:Lcom/mediatek/mtklogger/framework/MobileLog;

    iget-object v8, v8, Lcom/mediatek/mtklogger/framework/MobileLog;->mSharedPreferences:Landroid/content/SharedPreferences;

    invoke-interface {v8}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v8

    const-string v9, "mobilelog_enable"

    const/4 v10, 0x0

    invoke-interface {v8, v9, v10}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    move-result-object v8

    invoke-interface {v8}, Landroid/content/SharedPreferences$Editor;->commit()Z

    :cond_6
    const-string v8, "MTKLogger/MobileLog"

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "Going to start mobile log, but SD card not ready yet, status="

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, ", just send out a stop command to native."

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v8, v9}, Lcom/mediatek/mtklogger/utils/Utils;->logi(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v8, p0, Lcom/mediatek/mtklogger/framework/MobileLog$MobileLogHandler;->this$0:Lcom/mediatek/mtklogger/framework/MobileLog;

    iget-object v8, v8, Lcom/mediatek/mtklogger/framework/MobileLog;->mLogConnection:Lcom/mediatek/mtklogger/framework/LogConnection;

    const-string v9, "stop"

    invoke-virtual {v8, v9}, Lcom/mediatek/mtklogger/framework/LogConnection;->sendCmd(Ljava/lang/String;)Z

    const/4 v8, -0x1

    if-ne v5, v8, :cond_7

    iget-object v8, p0, Lcom/mediatek/mtklogger/framework/MobileLog$MobileLogHandler;->this$0:Lcom/mediatek/mtklogger/framework/MobileLog;

    const/4 v9, 0x0

    const-string v10, "2"

    # invokes: Lcom/mediatek/mtklogger/framework/MobileLog;->notifyServiceStatus(ILjava/lang/String;)V
    invoke-static {v8, v9, v10}, Lcom/mediatek/mtklogger/framework/MobileLog;->access$100(Lcom/mediatek/mtklogger/framework/MobileLog;ILjava/lang/String;)V

    goto/16 :goto_0

    :cond_7
    const/4 v8, -0x2

    if-ne v5, v8, :cond_3

    iget-object v8, p0, Lcom/mediatek/mtklogger/framework/MobileLog$MobileLogHandler;->this$0:Lcom/mediatek/mtklogger/framework/MobileLog;

    const/4 v9, 0x0

    const-string v10, "3"

    # invokes: Lcom/mediatek/mtklogger/framework/MobileLog;->notifyServiceStatus(ILjava/lang/String;)V
    invoke-static {v8, v9, v10}, Lcom/mediatek/mtklogger/framework/MobileLog;->access$100(Lcom/mediatek/mtklogger/framework/MobileLog;ILjava/lang/String;)V

    goto/16 :goto_0

    :cond_8
    const/4 v2, 0x0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v8

    if-nez v8, :cond_b

    const-string v8, "MTKLogger/MobileLog"

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "Mobile log initialization reason = "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v8, v9}, Lcom/mediatek/mtklogger/utils/Utils;->logi(Ljava/lang/String;Ljava/lang/String;)V

    const-string v8, "boot"

    invoke-virtual {v8, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_9

    iget-object v8, p0, Lcom/mediatek/mtklogger/framework/MobileLog$MobileLogHandler;->this$0:Lcom/mediatek/mtklogger/framework/MobileLog;

    iget-object v8, v8, Lcom/mediatek/mtklogger/framework/MobileLog;->mLogConnection:Lcom/mediatek/mtklogger/framework/LogConnection;

    const-string v9, "copy"

    invoke-virtual {v8, v9}, Lcom/mediatek/mtklogger/framework/LogConnection;->sendCmd(Ljava/lang/String;)Z

    move-result v2

    :goto_1
    if-eqz v2, :cond_c

    const/4 v8, 0x4

    const-wide/16 v9, 0x2710

    invoke-virtual {p0, v8, v9, v10}, Lcom/mediatek/mtklogger/framework/MobileLog$MobileLogHandler;->sendEmptyMessageDelayed(IJ)Z

    iget-object v8, p0, Lcom/mediatek/mtklogger/framework/MobileLog$MobileLogHandler;->this$0:Lcom/mediatek/mtklogger/framework/MobileLog;

    const/4 v9, 0x1

    const-string v10, ""

    # invokes: Lcom/mediatek/mtklogger/framework/MobileLog;->notifyServiceStatus(ILjava/lang/String;)V
    invoke-static {v8, v9, v10}, Lcom/mediatek/mtklogger/framework/MobileLog;->access$100(Lcom/mediatek/mtklogger/framework/MobileLog;ILjava/lang/String;)V

    goto/16 :goto_0

    :cond_9
    const-string v8, "ipo"

    invoke-virtual {v8, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_a

    iget-object v8, p0, Lcom/mediatek/mtklogger/framework/MobileLog$MobileLogHandler;->this$0:Lcom/mediatek/mtklogger/framework/MobileLog;

    iget-object v8, v8, Lcom/mediatek/mtklogger/framework/MobileLog;->mLogConnection:Lcom/mediatek/mtklogger/framework/LogConnection;

    const-string v9, "IPO_BOOT"

    invoke-virtual {v8, v9}, Lcom/mediatek/mtklogger/framework/LogConnection;->sendCmd(Ljava/lang/String;)Z

    move-result v2

    goto :goto_1

    :cond_a
    const-string v8, "MTKLogger/MobileLog"

    const-string v9, "Unsupported initialization reason, ignore it."

    invoke-static {v8, v9}, Lcom/mediatek/mtklogger/utils/Utils;->logw(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v8, p0, Lcom/mediatek/mtklogger/framework/MobileLog$MobileLogHandler;->this$0:Lcom/mediatek/mtklogger/framework/MobileLog;

    iget-object v8, v8, Lcom/mediatek/mtklogger/framework/MobileLog;->mLogConnection:Lcom/mediatek/mtklogger/framework/LogConnection;

    const-string v9, "start"

    invoke-virtual {v8, v9}, Lcom/mediatek/mtklogger/framework/LogConnection;->sendCmd(Ljava/lang/String;)Z

    move-result v2

    goto :goto_1

    :cond_b
    const-string v8, "MTKLogger/MobileLog"

    const-string v9, "No valid start up reason was found when init. Just start up."

    invoke-static {v8, v9}, Lcom/mediatek/mtklogger/utils/Utils;->logd(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v8, p0, Lcom/mediatek/mtklogger/framework/MobileLog$MobileLogHandler;->this$0:Lcom/mediatek/mtklogger/framework/MobileLog;

    iget-object v8, v8, Lcom/mediatek/mtklogger/framework/MobileLog;->mLogConnection:Lcom/mediatek/mtklogger/framework/LogConnection;

    const-string v9, "start"

    invoke-virtual {v8, v9}, Lcom/mediatek/mtklogger/framework/LogConnection;->sendCmd(Ljava/lang/String;)Z

    move-result v2

    goto :goto_1

    :cond_c
    const-string v8, "MTKLogger/MobileLog"

    const-string v9, "Send start command to native layer fail, maybe connection has already be lost."

    invoke-static {v8, v9}, Lcom/mediatek/mtklogger/utils/Utils;->loge(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v8, p0, Lcom/mediatek/mtklogger/framework/MobileLog$MobileLogHandler;->this$0:Lcom/mediatek/mtklogger/framework/MobileLog;

    const/4 v9, 0x0

    const-string v10, "4"

    # invokes: Lcom/mediatek/mtklogger/framework/MobileLog;->notifyServiceStatus(ILjava/lang/String;)V
    invoke-static {v8, v9, v10}, Lcom/mediatek/mtklogger/framework/MobileLog;->access$100(Lcom/mediatek/mtklogger/framework/MobileLog;ILjava/lang/String;)V

    goto/16 :goto_0

    :sswitch_1
    iget-object v8, p0, Lcom/mediatek/mtklogger/framework/MobileLog$MobileLogHandler;->this$0:Lcom/mediatek/mtklogger/framework/MobileLog;

    # getter for: Lcom/mediatek/mtklogger/framework/MobileLog;->bConnected:Z
    invoke-static {v8}, Lcom/mediatek/mtklogger/framework/MobileLog;->access$000(Lcom/mediatek/mtklogger/framework/MobileLog;)Z

    move-result v8

    if-eqz v8, :cond_10

    const/4 v2, 0x0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v8

    if-nez v8, :cond_e

    const-string v8, "MTKLogger/MobileLog"

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "Mobile log stop reason = "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v8, v9}, Lcom/mediatek/mtklogger/utils/Utils;->logi(Ljava/lang/String;Ljava/lang/String;)V

    const-string v8, "ipo_shutdown"

    invoke-virtual {v8, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_d

    iget-object v8, p0, Lcom/mediatek/mtklogger/framework/MobileLog$MobileLogHandler;->this$0:Lcom/mediatek/mtklogger/framework/MobileLog;

    iget-object v8, v8, Lcom/mediatek/mtklogger/framework/MobileLog;->mLogConnection:Lcom/mediatek/mtklogger/framework/LogConnection;

    const-string v9, "IPO_SHUTDOWN"

    invoke-virtual {v8, v9}, Lcom/mediatek/mtklogger/framework/LogConnection;->sendCmd(Ljava/lang/String;)Z

    move-result v2

    :goto_2
    iget-object v8, p0, Lcom/mediatek/mtklogger/framework/MobileLog$MobileLogHandler;->this$0:Lcom/mediatek/mtklogger/framework/MobileLog;

    iget-object v8, v8, Lcom/mediatek/mtklogger/framework/MobileLog;->mLogConnection:Lcom/mediatek/mtklogger/framework/LogConnection;

    invoke-virtual {v8}, Lcom/mediatek/mtklogger/framework/LogConnection;->stop()V

    iget-object v8, p0, Lcom/mediatek/mtklogger/framework/MobileLog$MobileLogHandler;->this$0:Lcom/mediatek/mtklogger/framework/MobileLog;

    const/4 v9, 0x0

    iput-object v9, v8, Lcom/mediatek/mtklogger/framework/MobileLog;->mLogConnection:Lcom/mediatek/mtklogger/framework/LogConnection;

    iget-object v8, p0, Lcom/mediatek/mtklogger/framework/MobileLog$MobileLogHandler;->this$0:Lcom/mediatek/mtklogger/framework/MobileLog;

    const/4 v9, 0x0

    # setter for: Lcom/mediatek/mtklogger/framework/MobileLog;->bConnected:Z
    invoke-static {v8, v9}, Lcom/mediatek/mtklogger/framework/MobileLog;->access$002(Lcom/mediatek/mtklogger/framework/MobileLog;Z)Z

    if-nez v2, :cond_f

    const-string v8, "MTKLogger/MobileLog"

    const-string v9, "Send stop command to native layer fail, maybe connection has already be lost."

    invoke-static {v8, v9}, Lcom/mediatek/mtklogger/utils/Utils;->loge(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v8, p0, Lcom/mediatek/mtklogger/framework/MobileLog$MobileLogHandler;->this$0:Lcom/mediatek/mtklogger/framework/MobileLog;

    const/4 v9, 0x0

    const-string v10, "4"

    # invokes: Lcom/mediatek/mtklogger/framework/MobileLog;->notifyServiceStatus(ILjava/lang/String;)V
    invoke-static {v8, v9, v10}, Lcom/mediatek/mtklogger/framework/MobileLog;->access$100(Lcom/mediatek/mtklogger/framework/MobileLog;ILjava/lang/String;)V

    :goto_3
    const/4 v8, 0x4

    invoke-virtual {p0, v8}, Lcom/mediatek/mtklogger/framework/MobileLog$MobileLogHandler;->removeMessages(I)V

    goto/16 :goto_0

    :cond_d
    const-string v8, "MTKLogger/MobileLog"

    const-string v9, "Unsupported stop reason, ignore it."

    invoke-static {v8, v9}, Lcom/mediatek/mtklogger/utils/Utils;->logw(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v8, p0, Lcom/mediatek/mtklogger/framework/MobileLog$MobileLogHandler;->this$0:Lcom/mediatek/mtklogger/framework/MobileLog;

    iget-object v8, v8, Lcom/mediatek/mtklogger/framework/MobileLog;->mLogConnection:Lcom/mediatek/mtklogger/framework/LogConnection;

    const-string v9, "stop"

    invoke-virtual {v8, v9}, Lcom/mediatek/mtklogger/framework/LogConnection;->sendCmd(Ljava/lang/String;)Z

    move-result v2

    goto :goto_2

    :cond_e
    const-string v8, "MTKLogger/MobileLog"

    const-string v9, "Normally stop mobile log with stop command"

    invoke-static {v8, v9}, Lcom/mediatek/mtklogger/utils/Utils;->logd(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v8, p0, Lcom/mediatek/mtklogger/framework/MobileLog$MobileLogHandler;->this$0:Lcom/mediatek/mtklogger/framework/MobileLog;

    iget-object v8, v8, Lcom/mediatek/mtklogger/framework/MobileLog;->mLogConnection:Lcom/mediatek/mtklogger/framework/LogConnection;

    const-string v9, "stop"

    invoke-virtual {v8, v9}, Lcom/mediatek/mtklogger/framework/LogConnection;->sendCmd(Ljava/lang/String;)Z

    move-result v2

    goto :goto_2

    :cond_f
    iget-object v8, p0, Lcom/mediatek/mtklogger/framework/MobileLog$MobileLogHandler;->this$0:Lcom/mediatek/mtklogger/framework/MobileLog;

    const/4 v9, 0x0

    const-string v10, ""

    # invokes: Lcom/mediatek/mtklogger/framework/MobileLog;->notifyServiceStatus(ILjava/lang/String;)V
    invoke-static {v8, v9, v10}, Lcom/mediatek/mtklogger/framework/MobileLog;->access$100(Lcom/mediatek/mtklogger/framework/MobileLog;ILjava/lang/String;)V

    goto :goto_3

    :cond_10
    const-string v8, "MTKLogger/MobileLog"

    const-string v9, "Have not connected to native layer, just ignore this command"

    invoke-static {v8, v9}, Lcom/mediatek/mtklogger/utils/Utils;->logw(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v8, p0, Lcom/mediatek/mtklogger/framework/MobileLog$MobileLogHandler;->this$0:Lcom/mediatek/mtklogger/framework/MobileLog;

    const/4 v9, 0x0

    const-string v10, "1"

    # invokes: Lcom/mediatek/mtklogger/framework/MobileLog;->notifyServiceStatus(ILjava/lang/String;)V
    invoke-static {v8, v9, v10}, Lcom/mediatek/mtklogger/framework/MobileLog;->access$100(Lcom/mediatek/mtklogger/framework/MobileLog;ILjava/lang/String;)V

    goto/16 :goto_0

    :sswitch_2
    iget-object v8, p0, Lcom/mediatek/mtklogger/framework/MobileLog$MobileLogHandler;->this$0:Lcom/mediatek/mtklogger/framework/MobileLog;

    # getter for: Lcom/mediatek/mtklogger/framework/MobileLog;->bConnected:Z
    invoke-static {v8}, Lcom/mediatek/mtklogger/framework/MobileLog;->access$000(Lcom/mediatek/mtklogger/framework/MobileLog;)Z

    move-result v8

    if-eqz v8, :cond_11

    const-string v8, "MTKLogger/MobileLog"

    const-string v9, "Receive a check command. Ignore it."

    invoke-static {v8, v9}, Lcom/mediatek/mtklogger/utils/Utils;->logd(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    :cond_11
    const-string v8, "MTKLogger/MobileLog"

    const-string v9, "lost connection to native layer, stop check."

    invoke-static {v8, v9}, Lcom/mediatek/mtklogger/utils/Utils;->logw(Ljava/lang/String;Ljava/lang/String;)V

    const/4 v8, 0x4

    invoke-virtual {p0, v8}, Lcom/mediatek/mtklogger/framework/MobileLog$MobileLogHandler;->removeMessages(I)V

    goto/16 :goto_0

    :sswitch_3
    iget-object v8, p0, Lcom/mediatek/mtklogger/framework/MobileLog$MobileLogHandler;->this$0:Lcom/mediatek/mtklogger/framework/MobileLog;

    # getter for: Lcom/mediatek/mtklogger/framework/MobileLog;->bConnected:Z
    invoke-static {v8}, Lcom/mediatek/mtklogger/framework/MobileLog;->access$000(Lcom/mediatek/mtklogger/framework/MobileLog;)Z

    move-result v8

    if-eqz v8, :cond_17

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v8

    if-nez v8, :cond_16

    const-string v8, "MTKLogger/MobileLog"

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "Receive config command, parameter="

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v8, v9}, Lcom/mediatek/mtklogger/utils/Utils;->logd(Ljava/lang/String;Ljava/lang/String;)V

    const-string v8, "logsize="

    invoke-virtual {v0, v8}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v8

    if-nez v8, :cond_12

    const-string v8, "autostart="

    invoke-virtual {v0, v8}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v8

    if-eqz v8, :cond_13

    :cond_12
    iget-object v8, p0, Lcom/mediatek/mtklogger/framework/MobileLog$MobileLogHandler;->this$0:Lcom/mediatek/mtklogger/framework/MobileLog;

    iget-object v8, v8, Lcom/mediatek/mtklogger/framework/MobileLog;->mLogConnection:Lcom/mediatek/mtklogger/framework/LogConnection;

    invoke-virtual {v8, v0}, Lcom/mediatek/mtklogger/framework/LogConnection;->sendCmd(Ljava/lang/String;)Z

    goto/16 :goto_0

    :cond_13
    const-string v8, "sublog_"

    invoke-virtual {v0, v8}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v8

    if-eqz v8, :cond_15

    iget v7, p1, Landroid/os/Message;->arg1:I

    iget v3, p1, Landroid/os/Message;->arg2:I

    const-string v8, "MTKLogger/MobileLog"

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "Try to set mobile sub log enable state, subType="

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, ", enable?"

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v8, v9}, Lcom/mediatek/mtklogger/utils/Utils;->logv(Ljava/lang/String;Ljava/lang/String;)V

    sget-object v8, Lcom/mediatek/mtklogger/framework/MobileLog;->KEY_SUB_LOG_TYPE_MAP:Landroid/util/SparseArray;

    invoke-virtual {v8, v7}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Ljava/lang/String;

    if-nez v6, :cond_14

    const-string v8, "MTKLogger/MobileLog"

    const-string v9, "Unsupported sub mobile log type"

    invoke-static {v8, v9}, Lcom/mediatek/mtklogger/utils/Utils;->loge(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    :cond_14
    iget-object v8, p0, Lcom/mediatek/mtklogger/framework/MobileLog$MobileLogHandler;->this$0:Lcom/mediatek/mtklogger/framework/MobileLog;

    iget-object v8, v8, Lcom/mediatek/mtklogger/framework/MobileLog;->mLogConnection:Lcom/mediatek/mtklogger/framework/LogConnection;

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "sublog_"

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, "="

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Lcom/mediatek/mtklogger/framework/LogConnection;->sendCmd(Ljava/lang/String;)Z

    goto/16 :goto_0

    :cond_15
    const-string v8, "MTKLogger/MobileLog"

    const-string v9, "Unsupported config command"

    invoke-static {v8, v9}, Lcom/mediatek/mtklogger/utils/Utils;->logw(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    :cond_16
    const-string v8, "MTKLogger/MobileLog"

    const-string v9, "Receive config command, but parameter is null"

    invoke-static {v8, v9}, Lcom/mediatek/mtklogger/utils/Utils;->loge(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    :cond_17
    const-string v8, "MTKLogger/MobileLog"

    const-string v9, "Fail to config native parameter because of lost connection."

    invoke-static {v8, v9}, Lcom/mediatek/mtklogger/utils/Utils;->logw(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    :sswitch_4
    const-string v8, "MTKLogger/MobileLog"

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "Receive adb command["

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, "]"

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v8, v9}, Lcom/mediatek/mtklogger/utils/Utils;->logd(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    :sswitch_5
    iget-object v8, p0, Lcom/mediatek/mtklogger/framework/MobileLog$MobileLogHandler;->this$0:Lcom/mediatek/mtklogger/framework/MobileLog;

    # getter for: Lcom/mediatek/mtklogger/framework/MobileLog;->bConnected:Z
    invoke-static {v8}, Lcom/mediatek/mtklogger/framework/MobileLog;->access$000(Lcom/mediatek/mtklogger/framework/MobileLog;)Z

    move-result v8

    if-nez v8, :cond_3

    const-string v8, "MTKLogger/MobileLog"

    const-string v9, "Reconnect to native layer fail! Mark log status as stopped."

    invoke-static {v8, v9}, Lcom/mediatek/mtklogger/utils/Utils;->logw(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v8, p0, Lcom/mediatek/mtklogger/framework/MobileLog$MobileLogHandler;->this$0:Lcom/mediatek/mtklogger/framework/MobileLog;

    const/4 v9, 0x0

    const-string v10, "1"

    # invokes: Lcom/mediatek/mtklogger/framework/MobileLog;->notifyServiceStatus(ILjava/lang/String;)V
    invoke-static {v8, v9, v10}, Lcom/mediatek/mtklogger/framework/MobileLog;->access$100(Lcom/mediatek/mtklogger/framework/MobileLog;ILjava/lang/String;)V

    goto/16 :goto_0

    :sswitch_6
    iget-object v8, p0, Lcom/mediatek/mtklogger/framework/MobileLog$MobileLogHandler;->this$0:Lcom/mediatek/mtklogger/framework/MobileLog;

    iget-object v8, v8, Lcom/mediatek/mtklogger/framework/MobileLog;->mLogConnection:Lcom/mediatek/mtklogger/framework/LogConnection;

    if-eqz v8, :cond_18

    iget-object v8, p0, Lcom/mediatek/mtklogger/framework/MobileLog$MobileLogHandler;->this$0:Lcom/mediatek/mtklogger/framework/MobileLog;

    iget-object v8, v8, Lcom/mediatek/mtklogger/framework/MobileLog;->mLogConnection:Lcom/mediatek/mtklogger/framework/LogConnection;

    invoke-virtual {v8}, Lcom/mediatek/mtklogger/framework/LogConnection;->stop()V

    iget-object v8, p0, Lcom/mediatek/mtklogger/framework/MobileLog$MobileLogHandler;->this$0:Lcom/mediatek/mtklogger/framework/MobileLog;

    const/4 v9, 0x0

    iput-object v9, v8, Lcom/mediatek/mtklogger/framework/MobileLog;->mLogConnection:Lcom/mediatek/mtklogger/framework/LogConnection;

    :cond_18
    const/4 v8, 0x4

    invoke-virtual {p0, v8}, Lcom/mediatek/mtklogger/framework/MobileLog$MobileLogHandler;->removeMessages(I)V

    iget-object v8, p0, Lcom/mediatek/mtklogger/framework/MobileLog$MobileLogHandler;->this$0:Lcom/mediatek/mtklogger/framework/MobileLog;

    const/4 v9, 0x0

    # setter for: Lcom/mediatek/mtklogger/framework/MobileLog;->bConnected:Z
    invoke-static {v8, v9}, Lcom/mediatek/mtklogger/framework/MobileLog;->access$002(Lcom/mediatek/mtklogger/framework/MobileLog;Z)Z

    iget-object v8, p0, Lcom/mediatek/mtklogger/framework/MobileLog$MobileLogHandler;->this$0:Lcom/mediatek/mtklogger/framework/MobileLog;

    const/4 v9, 0x0

    const-string v10, "5"

    # invokes: Lcom/mediatek/mtklogger/framework/MobileLog;->notifyServiceStatus(ILjava/lang/String;)V
    invoke-static {v8, v9, v10}, Lcom/mediatek/mtklogger/framework/MobileLog;->access$100(Lcom/mediatek/mtklogger/framework/MobileLog;ILjava/lang/String;)V

    goto/16 :goto_0

    :sswitch_7
    iget-object v8, p0, Lcom/mediatek/mtklogger/framework/MobileLog$MobileLogHandler;->this$0:Lcom/mediatek/mtklogger/framework/MobileLog;

    iget-object v8, v8, Lcom/mediatek/mtklogger/framework/MobileLog;->mSharedPreferences:Landroid/content/SharedPreferences;

    const-string v9, "mobilelog_enable"

    const/4 v10, 0x0

    invoke-interface {v8, v9, v10}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v4

    const/4 v8, 0x1

    if-eq v4, v8, :cond_19

    iget-object v8, p0, Lcom/mediatek/mtklogger/framework/MobileLog$MobileLogHandler;->this$0:Lcom/mediatek/mtklogger/framework/MobileLog;

    const/4 v9, 0x1

    const-string v10, ""

    # invokes: Lcom/mediatek/mtklogger/framework/MobileLog;->notifyServiceStatus(ILjava/lang/String;)V
    invoke-static {v8, v9, v10}, Lcom/mediatek/mtklogger/framework/MobileLog;->access$100(Lcom/mediatek/mtklogger/framework/MobileLog;ILjava/lang/String;)V

    goto/16 :goto_0

    :cond_19
    const-string v8, "MTKLogger/MobileLog"

    const-string v9, "Still running, ignore alive message."

    invoke-static {v8, v9}, Lcom/mediatek/mtklogger/utils/Utils;->logv(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x1 -> :sswitch_0
        0x3 -> :sswitch_1
        0x4 -> :sswitch_2
        0x7 -> :sswitch_3
        0x8 -> :sswitch_4
        0x9 -> :sswitch_5
        0x16 -> :sswitch_6
        0x17 -> :sswitch_7
    .end sparse-switch
.end method
