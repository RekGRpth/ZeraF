.class public Lcom/mediatek/mtklogger/taglog/TagLogManager;
.super Ljava/lang/Object;
.source "TagLogManager.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/mediatek/mtklogger/taglog/TagLogManager$ResumeTagThread;,
        Lcom/mediatek/mtklogger/taglog/TagLogManager$TagLogThread;,
        Lcom/mediatek/mtklogger/taglog/TagLogManager$UIHandler;
    }
.end annotation


# static fields
.field private static final COMPRESS_SPEED:I = 0x1

.field private static final DIALOG_ALL_LOGTOOL_STOPED:I = 0x12e

.field private static final DIALOG_INPUT:I = 0x12d

.field private static final DIALOG_LACK_OF_SDSPACE:I = 0x12f

.field private static final DIALOG_START_PROGRESS:I = 0x132

.field private static final DIALOG_ZIP_LOG_FAIL:I = 0x131

.field private static final EVENT_ALL_LOGTOOL_STOPED:I = 0xcd

.field private static final EVENT_CHECK_INPUTDIALOG_TIMEOUT:I = 0xd1

.field private static final EVENT_CREATE_INPUTDIALOG:I = 0xcb

.field private static final EVENT_ZIP_LOG_FAIL:I = 0xcf

.field private static final EVENT_ZIP_LOG_SUCCESS:I = 0xce

.field private static final INPUT_TIMEOUT:I = 0x1d4c0

.field private static final LOGPATHKEY:Landroid/util/SparseArray;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/util/SparseArray",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private static final MONITOR_TIMER:I = 0xc8

.field private static final STOPPED_TIMEOUT:I = 0x3e80

.field private static final TAG:Ljava/lang/String; = "MTKLogger/TagLogManager"

.field private static final WAIT_MODEM_INTENT:I = 0x3e8

.field private static instance:Lcom/mediatek/mtklogger/taglog/TagLogManager;

.field private static isTagingLog:Z


# instance fields
.field private mContext:Landroid/content/Context;

.field private mCurrentLogFolderList:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/mediatek/mtklogger/taglog/LogInformation;",
            ">;"
        }
    .end annotation
.end field

.field private mDataFromExtras:Landroid/os/Bundle;

.field private mDbPathFromAee:Ljava/lang/String;

.field private mDefaultSharedPreferences:Landroid/content/SharedPreferences;

.field private mDialog:Landroid/app/AlertDialog;

.field private mIgnoreMdLog:Z

.field private mIsFromMainActivity:Z

.field private mIsFromReboot:Z

.field private mIsInputDialogClicked:Z

.field private mIsModemExp:Z

.field private mIsTagInputNull:Z

.field private mIsTaglogClicked:Z

.field private mIsWaitingLogStateChange:Z

.field private mLogPathInTagLog:Landroid/util/SparseArray;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/util/SparseArray",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private mLogService:Lcom/mediatek/mtklogger/framework/MTKLoggerService;

.field private mLogStateReceiver:Landroid/content/BroadcastReceiver;

.field private mLogToolStatus:Landroid/util/SparseArray;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/util/SparseArray",
            "<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field

.field private mManualSaveLog:Z

.field private mMonitorTimer:Ljava/util/Timer;

.field mNeedMoreSpace:J

.field private mProgressDialog:Landroid/app/ProgressDialog;

.field private mSharedPreferences:Landroid/content/SharedPreferences;

.field private mTag:Ljava/lang/String;

.field private mTagLogResult:Ljava/lang/String;

.field private mTotalFilesCount:I

.field private mUIHandler:Lcom/mediatek/mtklogger/taglog/TagLogManager$UIHandler;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    const/4 v0, 0x0

    sput-object v0, Lcom/mediatek/mtklogger/taglog/TagLogManager;->instance:Lcom/mediatek/mtklogger/taglog/TagLogManager;

    const/4 v0, 0x0

    sput-boolean v0, Lcom/mediatek/mtklogger/taglog/TagLogManager;->isTagingLog:Z

    new-instance v0, Landroid/util/SparseArray;

    invoke-direct {v0}, Landroid/util/SparseArray;-><init>()V

    sput-object v0, Lcom/mediatek/mtklogger/taglog/TagLogManager;->LOGPATHKEY:Landroid/util/SparseArray;

    sget-object v0, Lcom/mediatek/mtklogger/taglog/TagLogManager;->LOGPATHKEY:Landroid/util/SparseArray;

    const/4 v1, 0x2

    const-string v2, "ModemLogPath"

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    sget-object v0, Lcom/mediatek/mtklogger/taglog/TagLogManager;->LOGPATHKEY:Landroid/util/SparseArray;

    const/4 v1, 0x1

    const-string v2, "MobileLogPath"

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    sget-object v0, Lcom/mediatek/mtklogger/taglog/TagLogManager;->LOGPATHKEY:Landroid/util/SparseArray;

    const/4 v1, 0x4

    const-string v2, "NetLogPath"

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    return-void
.end method

.method private constructor <init>(Landroid/content/Context;)V
    .locals 3
    .param p1    # Landroid/content/Context;

    const/4 v0, 0x0

    const/4 v2, 0x0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lcom/mediatek/mtklogger/taglog/TagLogManager;->mContext:Landroid/content/Context;

    iput-object v0, p0, Lcom/mediatek/mtklogger/taglog/TagLogManager;->mLogService:Lcom/mediatek/mtklogger/framework/MTKLoggerService;

    const-string v0, ""

    iput-object v0, p0, Lcom/mediatek/mtklogger/taglog/TagLogManager;->mTag:Ljava/lang/String;

    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/mediatek/mtklogger/taglog/TagLogManager;->mNeedMoreSpace:J

    new-instance v0, Landroid/util/SparseArray;

    invoke-direct {v0}, Landroid/util/SparseArray;-><init>()V

    iput-object v0, p0, Lcom/mediatek/mtklogger/taglog/TagLogManager;->mLogPathInTagLog:Landroid/util/SparseArray;

    new-instance v0, Landroid/util/SparseArray;

    invoke-direct {v0}, Landroid/util/SparseArray;-><init>()V

    iput-object v0, p0, Lcom/mediatek/mtklogger/taglog/TagLogManager;->mLogToolStatus:Landroid/util/SparseArray;

    iput-boolean v2, p0, Lcom/mediatek/mtklogger/taglog/TagLogManager;->mIsFromMainActivity:Z

    iput-boolean v2, p0, Lcom/mediatek/mtklogger/taglog/TagLogManager;->mManualSaveLog:Z

    iput-boolean v2, p0, Lcom/mediatek/mtklogger/taglog/TagLogManager;->mIsTagInputNull:Z

    iput-boolean v2, p0, Lcom/mediatek/mtklogger/taglog/TagLogManager;->mIsTaglogClicked:Z

    iput-boolean v2, p0, Lcom/mediatek/mtklogger/taglog/TagLogManager;->mIsFromReboot:Z

    iput v2, p0, Lcom/mediatek/mtklogger/taglog/TagLogManager;->mTotalFilesCount:I

    iput-boolean v2, p0, Lcom/mediatek/mtklogger/taglog/TagLogManager;->mIgnoreMdLog:Z

    iput-boolean v2, p0, Lcom/mediatek/mtklogger/taglog/TagLogManager;->mIsWaitingLogStateChange:Z

    new-instance v0, Lcom/mediatek/mtklogger/taglog/TagLogManager$1;

    invoke-direct {v0, p0}, Lcom/mediatek/mtklogger/taglog/TagLogManager$1;-><init>(Lcom/mediatek/mtklogger/taglog/TagLogManager;)V

    iput-object v0, p0, Lcom/mediatek/mtklogger/taglog/TagLogManager;->mLogStateReceiver:Landroid/content/BroadcastReceiver;

    const-string v0, "MTKLogger/TagLogManager"

    const-string v1, "<init>"

    invoke-static {v0, v1}, Lcom/mediatek/mtklogger/utils/Utils;->logv(Ljava/lang/String;Ljava/lang/String;)V

    iput-object p1, p0, Lcom/mediatek/mtklogger/taglog/TagLogManager;->mContext:Landroid/content/Context;

    invoke-direct {p0}, Lcom/mediatek/mtklogger/taglog/TagLogManager;->init()V

    return-void
.end method

.method static synthetic access$000(Lcom/mediatek/mtklogger/taglog/TagLogManager;)Z
    .locals 1
    .param p0    # Lcom/mediatek/mtklogger/taglog/TagLogManager;

    iget-boolean v0, p0, Lcom/mediatek/mtklogger/taglog/TagLogManager;->mIsWaitingLogStateChange:Z

    return v0
.end method

.method static synthetic access$002(Lcom/mediatek/mtklogger/taglog/TagLogManager;Z)Z
    .locals 0
    .param p0    # Lcom/mediatek/mtklogger/taglog/TagLogManager;
    .param p1    # Z

    iput-boolean p1, p0, Lcom/mediatek/mtklogger/taglog/TagLogManager;->mIsWaitingLogStateChange:Z

    return p1
.end method

.method static synthetic access$1000(Lcom/mediatek/mtklogger/taglog/TagLogManager;I)V
    .locals 0
    .param p0    # Lcom/mediatek/mtklogger/taglog/TagLogManager;
    .param p1    # I

    invoke-direct {p0, p1}, Lcom/mediatek/mtklogger/taglog/TagLogManager;->createDialog(I)V

    return-void
.end method

.method static synthetic access$1100(Lcom/mediatek/mtklogger/taglog/TagLogManager;)Lcom/mediatek/mtklogger/taglog/TagLogManager$UIHandler;
    .locals 1
    .param p0    # Lcom/mediatek/mtklogger/taglog/TagLogManager;

    iget-object v0, p0, Lcom/mediatek/mtklogger/taglog/TagLogManager;->mUIHandler:Lcom/mediatek/mtklogger/taglog/TagLogManager$UIHandler;

    return-object v0
.end method

.method static synthetic access$1300(Lcom/mediatek/mtklogger/taglog/TagLogManager;)Z
    .locals 1
    .param p0    # Lcom/mediatek/mtklogger/taglog/TagLogManager;

    iget-boolean v0, p0, Lcom/mediatek/mtklogger/taglog/TagLogManager;->mIsInputDialogClicked:Z

    return v0
.end method

.method static synthetic access$1302(Lcom/mediatek/mtklogger/taglog/TagLogManager;Z)Z
    .locals 0
    .param p0    # Lcom/mediatek/mtklogger/taglog/TagLogManager;
    .param p1    # Z

    iput-boolean p1, p0, Lcom/mediatek/mtklogger/taglog/TagLogManager;->mIsInputDialogClicked:Z

    return p1
.end method

.method static synthetic access$1402(Lcom/mediatek/mtklogger/taglog/TagLogManager;Ljava/lang/String;)Ljava/lang/String;
    .locals 0
    .param p0    # Lcom/mediatek/mtklogger/taglog/TagLogManager;
    .param p1    # Ljava/lang/String;

    iput-object p1, p0, Lcom/mediatek/mtklogger/taglog/TagLogManager;->mTagLogResult:Ljava/lang/String;

    return-object p1
.end method

.method static synthetic access$1500(Lcom/mediatek/mtklogger/taglog/TagLogManager;)V
    .locals 0
    .param p0    # Lcom/mediatek/mtklogger/taglog/TagLogManager;

    invoke-direct {p0}, Lcom/mediatek/mtklogger/taglog/TagLogManager;->dismissProgressDialog()V

    return-void
.end method

.method static synthetic access$1600(Lcom/mediatek/mtklogger/taglog/TagLogManager;)Landroid/content/Context;
    .locals 1
    .param p0    # Lcom/mediatek/mtklogger/taglog/TagLogManager;

    iget-object v0, p0, Lcom/mediatek/mtklogger/taglog/TagLogManager;->mContext:Landroid/content/Context;

    return-object v0
.end method

.method static synthetic access$1700(Lcom/mediatek/mtklogger/taglog/TagLogManager;)V
    .locals 0
    .param p0    # Lcom/mediatek/mtklogger/taglog/TagLogManager;

    invoke-direct {p0}, Lcom/mediatek/mtklogger/taglog/TagLogManager;->createProgressDialog()V

    return-void
.end method

.method static synthetic access$1800(Lcom/mediatek/mtklogger/taglog/TagLogManager;)Landroid/content/SharedPreferences;
    .locals 1
    .param p0    # Lcom/mediatek/mtklogger/taglog/TagLogManager;

    iget-object v0, p0, Lcom/mediatek/mtklogger/taglog/TagLogManager;->mSharedPreferences:Landroid/content/SharedPreferences;

    return-object v0
.end method

.method static synthetic access$1900(Lcom/mediatek/mtklogger/taglog/TagLogManager;)Landroid/util/SparseArray;
    .locals 1
    .param p0    # Lcom/mediatek/mtklogger/taglog/TagLogManager;

    invoke-direct {p0}, Lcom/mediatek/mtklogger/taglog/TagLogManager;->getLogPath()Landroid/util/SparseArray;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$200(Lcom/mediatek/mtklogger/taglog/TagLogManager;)Landroid/os/Bundle;
    .locals 1
    .param p0    # Lcom/mediatek/mtklogger/taglog/TagLogManager;

    iget-object v0, p0, Lcom/mediatek/mtklogger/taglog/TagLogManager;->mDataFromExtras:Landroid/os/Bundle;

    return-object v0
.end method

.method static synthetic access$2000(Lcom/mediatek/mtklogger/taglog/TagLogManager;)Ljava/util/List;
    .locals 1
    .param p0    # Lcom/mediatek/mtklogger/taglog/TagLogManager;

    iget-object v0, p0, Lcom/mediatek/mtklogger/taglog/TagLogManager;->mCurrentLogFolderList:Ljava/util/List;

    return-object v0
.end method

.method static synthetic access$2002(Lcom/mediatek/mtklogger/taglog/TagLogManager;Ljava/util/List;)Ljava/util/List;
    .locals 0
    .param p0    # Lcom/mediatek/mtklogger/taglog/TagLogManager;
    .param p1    # Ljava/util/List;

    iput-object p1, p0, Lcom/mediatek/mtklogger/taglog/TagLogManager;->mCurrentLogFolderList:Ljava/util/List;

    return-object p1
.end method

.method static synthetic access$2100(Lcom/mediatek/mtklogger/taglog/TagLogManager;)Z
    .locals 1
    .param p0    # Lcom/mediatek/mtklogger/taglog/TagLogManager;

    iget-boolean v0, p0, Lcom/mediatek/mtklogger/taglog/TagLogManager;->mIgnoreMdLog:Z

    return v0
.end method

.method static synthetic access$2102(Lcom/mediatek/mtklogger/taglog/TagLogManager;Z)Z
    .locals 0
    .param p0    # Lcom/mediatek/mtklogger/taglog/TagLogManager;
    .param p1    # Z

    iput-boolean p1, p0, Lcom/mediatek/mtklogger/taglog/TagLogManager;->mIgnoreMdLog:Z

    return p1
.end method

.method static synthetic access$2200(Lcom/mediatek/mtklogger/taglog/TagLogManager;)Lcom/mediatek/mtklogger/framework/MTKLoggerService;
    .locals 1
    .param p0    # Lcom/mediatek/mtklogger/taglog/TagLogManager;

    iget-object v0, p0, Lcom/mediatek/mtklogger/taglog/TagLogManager;->mLogService:Lcom/mediatek/mtklogger/framework/MTKLoggerService;

    return-object v0
.end method

.method static synthetic access$2300(Lcom/mediatek/mtklogger/taglog/TagLogManager;)I
    .locals 1
    .param p0    # Lcom/mediatek/mtklogger/taglog/TagLogManager;

    iget v0, p0, Lcom/mediatek/mtklogger/taglog/TagLogManager;->mTotalFilesCount:I

    return v0
.end method

.method static synthetic access$2302(Lcom/mediatek/mtklogger/taglog/TagLogManager;I)I
    .locals 0
    .param p0    # Lcom/mediatek/mtklogger/taglog/TagLogManager;
    .param p1    # I

    iput p1, p0, Lcom/mediatek/mtklogger/taglog/TagLogManager;->mTotalFilesCount:I

    return p1
.end method

.method static synthetic access$2312(Lcom/mediatek/mtklogger/taglog/TagLogManager;I)I
    .locals 1
    .param p0    # Lcom/mediatek/mtklogger/taglog/TagLogManager;
    .param p1    # I

    iget v0, p0, Lcom/mediatek/mtklogger/taglog/TagLogManager;->mTotalFilesCount:I

    add-int/2addr v0, p1

    iput v0, p0, Lcom/mediatek/mtklogger/taglog/TagLogManager;->mTotalFilesCount:I

    return v0
.end method

.method static synthetic access$2400(Lcom/mediatek/mtklogger/taglog/TagLogManager;)Landroid/app/ProgressDialog;
    .locals 1
    .param p0    # Lcom/mediatek/mtklogger/taglog/TagLogManager;

    iget-object v0, p0, Lcom/mediatek/mtklogger/taglog/TagLogManager;->mProgressDialog:Landroid/app/ProgressDialog;

    return-object v0
.end method

.method static synthetic access$2500(Lcom/mediatek/mtklogger/taglog/TagLogManager;Z)V
    .locals 0
    .param p0    # Lcom/mediatek/mtklogger/taglog/TagLogManager;
    .param p1    # Z

    invoke-direct {p0, p1}, Lcom/mediatek/mtklogger/taglog/TagLogManager;->startOrStopAllLogTool(Z)V

    return-void
.end method

.method static synthetic access$2600(Lcom/mediatek/mtklogger/taglog/TagLogManager;)Ljava/lang/String;
    .locals 1
    .param p0    # Lcom/mediatek/mtklogger/taglog/TagLogManager;

    iget-object v0, p0, Lcom/mediatek/mtklogger/taglog/TagLogManager;->mTag:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$2602(Lcom/mediatek/mtklogger/taglog/TagLogManager;Ljava/lang/String;)Ljava/lang/String;
    .locals 0
    .param p0    # Lcom/mediatek/mtklogger/taglog/TagLogManager;
    .param p1    # Ljava/lang/String;

    iput-object p1, p0, Lcom/mediatek/mtklogger/taglog/TagLogManager;->mTag:Ljava/lang/String;

    return-object p1
.end method

.method static synthetic access$2700(Lcom/mediatek/mtklogger/taglog/TagLogManager;Ljava/lang/String;)V
    .locals 0
    .param p0    # Lcom/mediatek/mtklogger/taglog/TagLogManager;
    .param p1    # Ljava/lang/String;

    invoke-direct {p0, p1}, Lcom/mediatek/mtklogger/taglog/TagLogManager;->rememberCurrentTaggingLogFolder(Ljava/lang/String;)V

    return-void
.end method

.method static synthetic access$2800(Lcom/mediatek/mtklogger/taglog/TagLogManager;Ljava/lang/String;)V
    .locals 0
    .param p0    # Lcom/mediatek/mtklogger/taglog/TagLogManager;
    .param p1    # Ljava/lang/String;

    invoke-direct {p0, p1}, Lcom/mediatek/mtklogger/taglog/TagLogManager;->tagSelectedLogFolder(Ljava/lang/String;)V

    return-void
.end method

.method static synthetic access$2900(Lcom/mediatek/mtklogger/taglog/TagLogManager;)Ljava/util/Timer;
    .locals 1
    .param p0    # Lcom/mediatek/mtklogger/taglog/TagLogManager;

    iget-object v0, p0, Lcom/mediatek/mtklogger/taglog/TagLogManager;->mMonitorTimer:Ljava/util/Timer;

    return-object v0
.end method

.method static synthetic access$2902(Lcom/mediatek/mtklogger/taglog/TagLogManager;Ljava/util/Timer;)Ljava/util/Timer;
    .locals 0
    .param p0    # Lcom/mediatek/mtklogger/taglog/TagLogManager;
    .param p1    # Ljava/util/Timer;

    iput-object p1, p0, Lcom/mediatek/mtklogger/taglog/TagLogManager;->mMonitorTimer:Ljava/util/Timer;

    return-object p1
.end method

.method static synthetic access$300(Lcom/mediatek/mtklogger/taglog/TagLogManager;Z)V
    .locals 0
    .param p0    # Lcom/mediatek/mtklogger/taglog/TagLogManager;
    .param p1    # Z

    invoke-direct {p0, p1}, Lcom/mediatek/mtklogger/taglog/TagLogManager;->deInit(Z)V

    return-void
.end method

.method static synthetic access$3000(Lcom/mediatek/mtklogger/taglog/TagLogManager;)Z
    .locals 1
    .param p0    # Lcom/mediatek/mtklogger/taglog/TagLogManager;

    iget-boolean v0, p0, Lcom/mediatek/mtklogger/taglog/TagLogManager;->mIsTaglogClicked:Z

    return v0
.end method

.method static synthetic access$3002(Lcom/mediatek/mtklogger/taglog/TagLogManager;Z)Z
    .locals 0
    .param p0    # Lcom/mediatek/mtklogger/taglog/TagLogManager;
    .param p1    # Z

    iput-boolean p1, p0, Lcom/mediatek/mtklogger/taglog/TagLogManager;->mIsTaglogClicked:Z

    return p1
.end method

.method static synthetic access$3100(Lcom/mediatek/mtklogger/taglog/TagLogManager;)Z
    .locals 1
    .param p0    # Lcom/mediatek/mtklogger/taglog/TagLogManager;

    iget-boolean v0, p0, Lcom/mediatek/mtklogger/taglog/TagLogManager;->mIsTagInputNull:Z

    return v0
.end method

.method static synthetic access$3102(Lcom/mediatek/mtklogger/taglog/TagLogManager;Z)Z
    .locals 0
    .param p0    # Lcom/mediatek/mtklogger/taglog/TagLogManager;
    .param p1    # Z

    iput-boolean p1, p0, Lcom/mediatek/mtklogger/taglog/TagLogManager;->mIsTagInputNull:Z

    return p1
.end method

.method static synthetic access$400(Lcom/mediatek/mtklogger/taglog/TagLogManager;)Z
    .locals 1
    .param p0    # Lcom/mediatek/mtklogger/taglog/TagLogManager;

    iget-boolean v0, p0, Lcom/mediatek/mtklogger/taglog/TagLogManager;->mManualSaveLog:Z

    return v0
.end method

.method static synthetic access$402(Lcom/mediatek/mtklogger/taglog/TagLogManager;Z)Z
    .locals 0
    .param p0    # Lcom/mediatek/mtklogger/taglog/TagLogManager;
    .param p1    # Z

    iput-boolean p1, p0, Lcom/mediatek/mtklogger/taglog/TagLogManager;->mManualSaveLog:Z

    return p1
.end method

.method static synthetic access$502(Lcom/mediatek/mtklogger/taglog/TagLogManager;Ljava/lang/String;)Ljava/lang/String;
    .locals 0
    .param p0    # Lcom/mediatek/mtklogger/taglog/TagLogManager;
    .param p1    # Ljava/lang/String;

    iput-object p1, p0, Lcom/mediatek/mtklogger/taglog/TagLogManager;->mDbPathFromAee:Ljava/lang/String;

    return-object p1
.end method

.method static synthetic access$600(Lcom/mediatek/mtklogger/taglog/TagLogManager;)Z
    .locals 1
    .param p0    # Lcom/mediatek/mtklogger/taglog/TagLogManager;

    iget-boolean v0, p0, Lcom/mediatek/mtklogger/taglog/TagLogManager;->mIsModemExp:Z

    return v0
.end method

.method static synthetic access$602(Lcom/mediatek/mtklogger/taglog/TagLogManager;Z)Z
    .locals 0
    .param p0    # Lcom/mediatek/mtklogger/taglog/TagLogManager;
    .param p1    # Z

    iput-boolean p1, p0, Lcom/mediatek/mtklogger/taglog/TagLogManager;->mIsModemExp:Z

    return p1
.end method

.method static synthetic access$700(Lcom/mediatek/mtklogger/taglog/TagLogManager;)Z
    .locals 1
    .param p0    # Lcom/mediatek/mtklogger/taglog/TagLogManager;

    iget-boolean v0, p0, Lcom/mediatek/mtklogger/taglog/TagLogManager;->mIsFromReboot:Z

    return v0
.end method

.method static synthetic access$702(Lcom/mediatek/mtklogger/taglog/TagLogManager;Z)Z
    .locals 0
    .param p0    # Lcom/mediatek/mtklogger/taglog/TagLogManager;
    .param p1    # Z

    iput-boolean p1, p0, Lcom/mediatek/mtklogger/taglog/TagLogManager;->mIsFromReboot:Z

    return p1
.end method

.method static synthetic access$800(Lcom/mediatek/mtklogger/taglog/TagLogManager;)V
    .locals 0
    .param p0    # Lcom/mediatek/mtklogger/taglog/TagLogManager;

    invoke-direct {p0}, Lcom/mediatek/mtklogger/taglog/TagLogManager;->initToolStatus()V

    return-void
.end method

.method static synthetic access$900(Lcom/mediatek/mtklogger/taglog/TagLogManager;)Landroid/util/SparseArray;
    .locals 1
    .param p0    # Lcom/mediatek/mtklogger/taglog/TagLogManager;

    iget-object v0, p0, Lcom/mediatek/mtklogger/taglog/TagLogManager;->mLogToolStatus:Landroid/util/SparseArray;

    return-object v0
.end method

.method private createDialog(I)V
    .locals 12
    .param p1    # I

    new-instance v0, Landroid/app/AlertDialog$Builder;

    iget-object v4, p0, Lcom/mediatek/mtklogger/taglog/TagLogManager;->mContext:Landroid/content/Context;

    invoke-direct {v0, v4}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    sparse-switch p1, :sswitch_data_0

    :goto_0
    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v4

    iput-object v4, p0, Lcom/mediatek/mtklogger/taglog/TagLogManager;->mDialog:Landroid/app/AlertDialog;

    iget-object v4, p0, Lcom/mediatek/mtklogger/taglog/TagLogManager;->mDialog:Landroid/app/AlertDialog;

    const/4 v5, 0x0

    invoke-virtual {v4, v5}, Landroid/app/AlertDialog;->setCancelable(Z)V

    iget-object v4, p0, Lcom/mediatek/mtklogger/taglog/TagLogManager;->mDialog:Landroid/app/AlertDialog;

    invoke-virtual {v4}, Landroid/app/AlertDialog;->getWindow()Landroid/view/Window;

    move-result-object v3

    const/16 v4, 0x7d3

    invoke-virtual {v3, v4}, Landroid/view/Window;->setType(I)V

    iget-object v4, p0, Lcom/mediatek/mtklogger/taglog/TagLogManager;->mDialog:Landroid/app/AlertDialog;

    invoke-virtual {v4}, Landroid/app/AlertDialog;->show()V

    return-void

    :sswitch_0
    const/4 v4, 0x0

    iput-boolean v4, p0, Lcom/mediatek/mtklogger/taglog/TagLogManager;->mIsInputDialogClicked:Z

    new-instance v2, Landroid/widget/EditText;

    iget-object v4, p0, Lcom/mediatek/mtklogger/taglog/TagLogManager;->mContext:Landroid/content/Context;

    invoke-direct {v2, v4}, Landroid/widget/EditText;-><init>(Landroid/content/Context;)V

    new-instance v4, Lcom/mediatek/mtklogger/taglog/TagLogManager$4;

    invoke-direct {v4, p0}, Lcom/mediatek/mtklogger/taglog/TagLogManager$4;-><init>(Lcom/mediatek/mtklogger/taglog/TagLogManager;)V

    invoke-virtual {v2, v4}, Landroid/widget/EditText;->setCustomSelectionActionModeCallback(Landroid/view/ActionMode$Callback;)V

    const/4 v4, 0x0

    invoke-virtual {v2, v4}, Landroid/widget/EditText;->setLongClickable(Z)V

    const/4 v4, 0x0

    invoke-virtual {v2, v4}, Landroid/widget/EditText;->setTextIsSelectable(Z)V

    new-instance v4, Lcom/mediatek/mtklogger/taglog/TagLogManager$5;

    invoke-direct {v4, p0}, Lcom/mediatek/mtklogger/taglog/TagLogManager$5;-><init>(Lcom/mediatek/mtklogger/taglog/TagLogManager;)V

    invoke-virtual {v2, v4}, Landroid/widget/EditText;->setKeyListener(Landroid/text/method/KeyListener;)V

    iget-object v4, p0, Lcom/mediatek/mtklogger/taglog/TagLogManager;->mDataFromExtras:Landroid/os/Bundle;

    const-string v5, "taglogInputName"

    invoke-virtual {v4, v5}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    if-nez v1, :cond_0

    const-string v1, ""

    :cond_0
    invoke-virtual {v2, v1}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    const v4, 0x7f070065

    invoke-virtual {v0, v4}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    move-result-object v4

    const v5, 0x7f070069

    invoke-virtual {v4, v5}, Landroid/app/AlertDialog$Builder;->setMessage(I)Landroid/app/AlertDialog$Builder;

    move-result-object v4

    invoke-virtual {v4, v2}, Landroid/app/AlertDialog$Builder;->setView(Landroid/view/View;)Landroid/app/AlertDialog$Builder;

    move-result-object v4

    const v5, 0x104000a

    new-instance v6, Lcom/mediatek/mtklogger/taglog/TagLogManager$7;

    invoke-direct {v6, p0, v2}, Lcom/mediatek/mtklogger/taglog/TagLogManager$7;-><init>(Lcom/mediatek/mtklogger/taglog/TagLogManager;Landroid/widget/EditText;)V

    invoke-virtual {v4, v5, v6}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v4

    const/high16 v5, 0x1040000

    new-instance v6, Lcom/mediatek/mtklogger/taglog/TagLogManager$6;

    invoke-direct {v6, p0}, Lcom/mediatek/mtklogger/taglog/TagLogManager$6;-><init>(Lcom/mediatek/mtklogger/taglog/TagLogManager;)V

    invoke-virtual {v4, v5, v6}, Landroid/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    goto :goto_0

    :sswitch_1
    const v4, 0x7f070065

    invoke-virtual {v0, v4}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    move-result-object v4

    const v5, 0x7f07006a

    invoke-virtual {v4, v5}, Landroid/app/AlertDialog$Builder;->setMessage(I)Landroid/app/AlertDialog$Builder;

    move-result-object v4

    const v5, 0x104000a

    new-instance v6, Lcom/mediatek/mtklogger/taglog/TagLogManager$9;

    invoke-direct {v6, p0}, Lcom/mediatek/mtklogger/taglog/TagLogManager$9;-><init>(Lcom/mediatek/mtklogger/taglog/TagLogManager;)V

    invoke-virtual {v4, v5, v6}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v4

    const/high16 v5, 0x1040000

    new-instance v6, Lcom/mediatek/mtklogger/taglog/TagLogManager$8;

    invoke-direct {v6, p0}, Lcom/mediatek/mtklogger/taglog/TagLogManager$8;-><init>(Lcom/mediatek/mtklogger/taglog/TagLogManager;)V

    invoke-virtual {v4, v5, v6}, Landroid/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    goto/16 :goto_0

    :sswitch_2
    const v4, 0x7f070067

    invoke-virtual {v0, v4}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    move-result-object v4

    iget-object v5, p0, Lcom/mediatek/mtklogger/taglog/TagLogManager;->mContext:Landroid/content/Context;

    const v6, 0x7f070068

    const/4 v7, 0x1

    new-array v7, v7, [Ljava/lang/Object;

    const/4 v8, 0x0

    new-instance v9, Ljava/text/DecimalFormat;

    const-string v10, ".00"

    invoke-direct {v9, v10}, Ljava/text/DecimalFormat;-><init>(Ljava/lang/String;)V

    iget-wide v10, p0, Lcom/mediatek/mtklogger/taglog/TagLogManager;->mNeedMoreSpace:J

    long-to-float v10, v10

    const/high16 v11, 0x44800000

    div-float/2addr v10, v11

    const/high16 v11, 0x44800000

    div-float/2addr v10, v11

    float-to-double v10, v10

    invoke-virtual {v9, v10, v11}, Ljava/text/DecimalFormat;->format(D)Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/String;->toString()Ljava/lang/String;

    move-result-object v9

    aput-object v9, v7, v8

    invoke-virtual {v5, v6, v7}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Landroid/app/AlertDialog$Builder;->setMessage(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    move-result-object v4

    const v5, 0x104000a

    new-instance v6, Lcom/mediatek/mtklogger/taglog/TagLogManager$11;

    invoke-direct {v6, p0}, Lcom/mediatek/mtklogger/taglog/TagLogManager$11;-><init>(Lcom/mediatek/mtklogger/taglog/TagLogManager;)V

    invoke-virtual {v4, v5, v6}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v4

    const/high16 v5, 0x1040000

    new-instance v6, Lcom/mediatek/mtklogger/taglog/TagLogManager$10;

    invoke-direct {v6, p0}, Lcom/mediatek/mtklogger/taglog/TagLogManager$10;-><init>(Lcom/mediatek/mtklogger/taglog/TagLogManager;)V

    invoke-virtual {v4, v5, v6}, Landroid/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    goto/16 :goto_0

    :sswitch_3
    const v4, 0x7f070066

    invoke-virtual {v0, v4}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    move-result-object v4

    const v5, 0x7f07006b

    invoke-virtual {v4, v5}, Landroid/app/AlertDialog$Builder;->setMessage(I)Landroid/app/AlertDialog$Builder;

    move-result-object v4

    const v5, 0x104000a

    new-instance v6, Lcom/mediatek/mtklogger/taglog/TagLogManager$12;

    invoke-direct {v6, p0}, Lcom/mediatek/mtklogger/taglog/TagLogManager$12;-><init>(Lcom/mediatek/mtklogger/taglog/TagLogManager;)V

    invoke-virtual {v4, v5, v6}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    goto/16 :goto_0

    :sswitch_4
    const v4, 0x7f070066

    invoke-virtual {v0, v4}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    move-result-object v4

    const v5, 0x7f07006c

    invoke-virtual {v4, v5}, Landroid/app/AlertDialog$Builder;->setMessage(I)Landroid/app/AlertDialog$Builder;

    move-result-object v4

    const v5, 0x104000a

    new-instance v6, Lcom/mediatek/mtklogger/taglog/TagLogManager$13;

    invoke-direct {v6, p0}, Lcom/mediatek/mtklogger/taglog/TagLogManager$13;-><init>(Lcom/mediatek/mtklogger/taglog/TagLogManager;)V

    invoke-virtual {v4, v5, v6}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    goto/16 :goto_0

    :sswitch_5
    const v4, 0x7f070066

    invoke-virtual {v0, v4}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    move-result-object v4

    const v5, 0x7f07006d

    invoke-virtual {v4, v5}, Landroid/app/AlertDialog$Builder;->setMessage(I)Landroid/app/AlertDialog$Builder;

    move-result-object v4

    const v5, 0x104000a

    new-instance v6, Lcom/mediatek/mtklogger/taglog/TagLogManager$14;

    invoke-direct {v6, p0}, Lcom/mediatek/mtklogger/taglog/TagLogManager$14;-><init>(Lcom/mediatek/mtklogger/taglog/TagLogManager;)V

    invoke-virtual {v4, v5, v6}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    goto/16 :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x12d -> :sswitch_0
        0x12e -> :sswitch_1
        0x12f -> :sswitch_2
        0x131 -> :sswitch_3
        0x193 -> :sswitch_4
        0x194 -> :sswitch_5
    .end sparse-switch
.end method

.method private createProgressDialog()V
    .locals 7

    const/4 v6, 0x0

    const/4 v1, 0x0

    :goto_0
    iget v3, p0, Lcom/mediatek/mtklogger/taglog/TagLogManager;->mTotalFilesCount:I

    if-nez v3, :cond_2

    add-int/lit8 v1, v1, 0x32

    const/16 v3, 0x2710

    if-lt v1, v3, :cond_1

    :try_start_0
    const-string v3, "MTKLogger/TagLogManager"

    const-string v4, "Create progress dialog failed! The total files count calculated error!"

    invoke-static {v3, v4}, Lcom/mediatek/mtklogger/utils/Utils;->loge(Ljava/lang/String;Ljava/lang/String;)V

    :cond_0
    :goto_1
    return-void

    :cond_1
    const-wide/16 v3, 0x32

    invoke-static {v3, v4}, Ljava/lang/Thread;->sleep(J)V
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Ljava/lang/InterruptedException;->printStackTrace()V

    goto :goto_0

    :cond_2
    iget-object v3, p0, Lcom/mediatek/mtklogger/taglog/TagLogManager;->mProgressDialog:Landroid/app/ProgressDialog;

    if-nez v3, :cond_5

    iget-object v3, p0, Lcom/mediatek/mtklogger/taglog/TagLogManager;->mUIHandler:Lcom/mediatek/mtklogger/taglog/TagLogManager$UIHandler;

    if-eqz v3, :cond_5

    new-instance v3, Landroid/app/ProgressDialog;

    iget-object v4, p0, Lcom/mediatek/mtklogger/taglog/TagLogManager;->mContext:Landroid/content/Context;

    invoke-direct {v3, v4}, Landroid/app/ProgressDialog;-><init>(Landroid/content/Context;)V

    iput-object v3, p0, Lcom/mediatek/mtklogger/taglog/TagLogManager;->mProgressDialog:Landroid/app/ProgressDialog;

    iget-object v3, p0, Lcom/mediatek/mtklogger/taglog/TagLogManager;->mProgressDialog:Landroid/app/ProgressDialog;

    if-eqz v3, :cond_6

    iget-object v3, p0, Lcom/mediatek/mtklogger/taglog/TagLogManager;->mProgressDialog:Landroid/app/ProgressDialog;

    invoke-virtual {v3, v6}, Landroid/app/ProgressDialog;->setProgressStyle(I)V

    iget-object v3, p0, Lcom/mediatek/mtklogger/taglog/TagLogManager;->mProgressDialog:Landroid/app/ProgressDialog;

    const v4, 0x7f070065

    invoke-virtual {v3, v4}, Landroid/app/ProgressDialog;->setTitle(I)V

    iget-object v3, p0, Lcom/mediatek/mtklogger/taglog/TagLogManager;->mProgressDialog:Landroid/app/ProgressDialog;

    iget-object v4, p0, Lcom/mediatek/mtklogger/taglog/TagLogManager;->mContext:Landroid/content/Context;

    invoke-virtual {v4}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v5, 0x7f07006e

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getText(I)Ljava/lang/CharSequence;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/app/ProgressDialog;->setMessage(Ljava/lang/CharSequence;)V

    iget-object v3, p0, Lcom/mediatek/mtklogger/taglog/TagLogManager;->mProgressDialog:Landroid/app/ProgressDialog;

    invoke-virtual {v3, v6}, Landroid/app/ProgressDialog;->setCancelable(Z)V

    iget-object v3, p0, Lcom/mediatek/mtklogger/taglog/TagLogManager;->mProgressDialog:Landroid/app/ProgressDialog;

    const/4 v4, 0x1

    invoke-virtual {v3, v4}, Landroid/app/ProgressDialog;->setProgressStyle(I)V

    iget-object v3, p0, Lcom/mediatek/mtklogger/taglog/TagLogManager;->mProgressDialog:Landroid/app/ProgressDialog;

    const/4 v4, -0x1

    iget-object v5, p0, Lcom/mediatek/mtklogger/taglog/TagLogManager;->mContext:Landroid/content/Context;

    invoke-virtual {v5}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    const v6, 0x7f07006f

    invoke-virtual {v5, v6}, Landroid/content/res/Resources;->getText(I)Ljava/lang/CharSequence;

    move-result-object v5

    new-instance v6, Lcom/mediatek/mtklogger/taglog/TagLogManager$2;

    invoke-direct {v6, p0}, Lcom/mediatek/mtklogger/taglog/TagLogManager$2;-><init>(Lcom/mediatek/mtklogger/taglog/TagLogManager;)V

    invoke-virtual {v3, v4, v5, v6}, Landroid/app/ProgressDialog;->setButton(ILjava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)V

    iget-object v3, p0, Lcom/mediatek/mtklogger/taglog/TagLogManager;->mProgressDialog:Landroid/app/ProgressDialog;

    invoke-virtual {v3}, Landroid/app/ProgressDialog;->getWindow()Landroid/view/Window;

    move-result-object v2

    const/16 v3, 0x7d3

    invoke-virtual {v2, v3}, Landroid/view/Window;->setType(I)V

    iget-boolean v3, p0, Lcom/mediatek/mtklogger/taglog/TagLogManager;->mManualSaveLog:Z

    if-nez v3, :cond_3

    iget-boolean v3, p0, Lcom/mediatek/mtklogger/taglog/TagLogManager;->mIsFromMainActivity:Z

    if-eqz v3, :cond_4

    :cond_3
    const-string v3, "MTKLogger/TagLogManager"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "mManualSaveLog?"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-boolean v5, p0, Lcom/mediatek/mtklogger/taglog/TagLogManager;->mManualSaveLog:Z

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "; mIsFromMainActivity?"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-boolean v5, p0, Lcom/mediatek/mtklogger/taglog/TagLogManager;->mIsFromMainActivity:Z

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/mediatek/mtklogger/utils/Utils;->logd(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v3, p0, Lcom/mediatek/mtklogger/taglog/TagLogManager;->mProgressDialog:Landroid/app/ProgressDialog;

    invoke-virtual {v3}, Landroid/app/ProgressDialog;->show()V

    :cond_4
    iget-object v3, p0, Lcom/mediatek/mtklogger/taglog/TagLogManager;->mProgressDialog:Landroid/app/ProgressDialog;

    iget v4, p0, Lcom/mediatek/mtklogger/taglog/TagLogManager;->mTotalFilesCount:I

    invoke-virtual {v3, v4}, Landroid/app/ProgressDialog;->setMax(I)V

    iget-object v3, p0, Lcom/mediatek/mtklogger/taglog/TagLogManager;->mProgressDialog:Landroid/app/ProgressDialog;

    invoke-static {}, Lcom/mediatek/mtklogger/taglog/ZipManager;->getZippedFilesCount()I

    move-result v4

    invoke-virtual {v3, v4}, Landroid/app/ProgressDialog;->setProgress(I)V

    invoke-direct {p0}, Lcom/mediatek/mtklogger/taglog/TagLogManager;->monitorProgressDialogBar()V

    :cond_5
    :goto_2
    iget-boolean v3, p0, Lcom/mediatek/mtklogger/taglog/TagLogManager;->mManualSaveLog:Z

    if-nez v3, :cond_0

    iget-boolean v3, p0, Lcom/mediatek/mtklogger/taglog/TagLogManager;->mIsFromMainActivity:Z

    if-nez v3, :cond_0

    invoke-direct {p0}, Lcom/mediatek/mtklogger/taglog/TagLogManager;->dismissProgressDialog()V

    goto/16 :goto_1

    :cond_6
    const-string v3, "MTKLogger/TagLogManager"

    const-string v4, "new mProgressDialog failed"

    invoke-static {v3, v4}, Lcom/mediatek/mtklogger/utils/Utils;->loge(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_2
.end method

.method private deInit(Z)V
    .locals 3
    .param p1    # Z

    const/4 v2, 0x0

    const-string v0, "MTKLogger/TagLogManager"

    const-string v1, "-->deInit()"

    invoke-static {v0, v1}, Lcom/mediatek/mtklogger/utils/Utils;->logd(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/mediatek/mtklogger/taglog/TagLogManager;->mContext:Landroid/content/Context;

    iget-object v1, p0, Lcom/mediatek/mtklogger/taglog/TagLogManager;->mLogStateReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {v0, v1}, Landroid/content/Context;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    iget-object v0, p0, Lcom/mediatek/mtklogger/taglog/TagLogManager;->mDialog:Landroid/app/AlertDialog;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/mediatek/mtklogger/taglog/TagLogManager;->mDialog:Landroid/app/AlertDialog;

    invoke-virtual {v0}, Landroid/app/AlertDialog;->isShowing()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/mediatek/mtklogger/taglog/TagLogManager;->mDialog:Landroid/app/AlertDialog;

    invoke-virtual {v0}, Landroid/app/AlertDialog;->dismiss()V

    iput-object v2, p0, Lcom/mediatek/mtklogger/taglog/TagLogManager;->mDialog:Landroid/app/AlertDialog;

    :cond_0
    invoke-direct {p0}, Lcom/mediatek/mtklogger/taglog/TagLogManager;->dismissProgressDialog()V

    sput-object v2, Lcom/mediatek/mtklogger/taglog/TagLogManager;->instance:Lcom/mediatek/mtklogger/taglog/TagLogManager;

    iput-object v2, p0, Lcom/mediatek/mtklogger/taglog/TagLogManager;->mUIHandler:Lcom/mediatek/mtklogger/taglog/TagLogManager$UIHandler;

    if-eqz p1, :cond_1

    iget-boolean v0, p0, Lcom/mediatek/mtklogger/taglog/TagLogManager;->mManualSaveLog:Z

    if-nez v0, :cond_1

    invoke-direct {p0}, Lcom/mediatek/mtklogger/taglog/TagLogManager;->notifyToLog2Server()V

    :goto_0
    return-void

    :cond_1
    const-string v0, "MTKLogger/TagLogManager"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "<-- Not need to notify Log2Server, mManualSaveLog="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-boolean v2, p0, Lcom/mediatek/mtklogger/taglog/TagLogManager;->mManualSaveLog:Z

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/mediatek/mtklogger/utils/Utils;->logd(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method private dismissProgressDialog()V
    .locals 1

    iget-object v0, p0, Lcom/mediatek/mtklogger/taglog/TagLogManager;->mProgressDialog:Landroid/app/ProgressDialog;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/mediatek/mtklogger/taglog/TagLogManager;->mProgressDialog:Landroid/app/ProgressDialog;

    invoke-virtual {v0}, Landroid/app/ProgressDialog;->dismiss()V

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/mediatek/mtklogger/taglog/TagLogManager;->mProgressDialog:Landroid/app/ProgressDialog;

    :cond_0
    return-void
.end method

.method private forgetCachedTaggingLogFolder()V
    .locals 5

    const-string v3, "MTKLogger/TagLogManager"

    const-string v4, "-->forgetCachedTaggingLogFolder()"

    invoke-static {v3, v4}, Lcom/mediatek/mtklogger/utils/Utils;->logd(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v3, p0, Lcom/mediatek/mtklogger/taglog/TagLogManager;->mSharedPreferences:Landroid/content/SharedPreferences;

    invoke-interface {v3}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    sget-object v3, Lcom/mediatek/mtklogger/utils/Utils;->LOG_TYPE_SET:Ljava/util/Set;

    invoke-interface {v3}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Integer;

    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    move-result v2

    sget-object v3, Lcom/mediatek/mtklogger/utils/Utils;->KEY_TAGGING_LOG_MAP:Landroid/util/SparseArray;

    invoke-virtual {v3, v2}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    invoke-interface {v0, v3}, Landroid/content/SharedPreferences$Editor;->remove(Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    goto :goto_0

    :cond_0
    const-string v3, "tagging_dest"

    invoke-interface {v0, v3}, Landroid/content/SharedPreferences$Editor;->remove(Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    move-result-object v3

    const-string v4, "tagging_db"

    invoke-interface {v3, v4}, Landroid/content/SharedPreferences$Editor;->remove(Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    move-result-object v3

    invoke-interface {v3}, Landroid/content/SharedPreferences$Editor;->commit()Z

    return-void
.end method

.method public static getInstance(Landroid/content/Context;)Lcom/mediatek/mtklogger/taglog/TagLogManager;
    .locals 1
    .param p0    # Landroid/content/Context;

    sget-object v0, Lcom/mediatek/mtklogger/taglog/TagLogManager;->instance:Lcom/mediatek/mtklogger/taglog/TagLogManager;

    if-nez v0, :cond_0

    new-instance v0, Lcom/mediatek/mtklogger/taglog/TagLogManager;

    invoke-direct {v0, p0}, Lcom/mediatek/mtklogger/taglog/TagLogManager;-><init>(Landroid/content/Context;)V

    sput-object v0, Lcom/mediatek/mtklogger/taglog/TagLogManager;->instance:Lcom/mediatek/mtklogger/taglog/TagLogManager;

    :cond_0
    sget-object v0, Lcom/mediatek/mtklogger/taglog/TagLogManager;->instance:Lcom/mediatek/mtklogger/taglog/TagLogManager;

    return-object v0
.end method

.method private getLogPath()Landroid/util/SparseArray;
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Landroid/util/SparseArray",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    new-instance v2, Landroid/util/SparseArray;

    invoke-direct {v2}, Landroid/util/SparseArray;-><init>()V

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v6, p0, Lcom/mediatek/mtklogger/taglog/TagLogManager;->mContext:Landroid/content/Context;

    invoke-static {v6}, Lcom/mediatek/mtklogger/utils/Utils;->getCurrentLogPath(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "/mtklog/"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    new-instance v5, Ljava/io/File;

    invoke-direct {v5, v4}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v5}, Ljava/io/File;->exists()Z

    move-result v5

    if-nez v5, :cond_1

    :cond_0
    return-object v2

    :cond_1
    const-string v5, "MTKLogger/TagLogManager"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "getMTKLogPath :"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Lcom/mediatek/mtklogger/utils/Utils;->logi(Ljava/lang/String;Ljava/lang/String;)V

    sget-object v5, Lcom/mediatek/mtklogger/utils/Utils;->LOG_TYPE_SET:Ljava/util/Set;

    invoke-interface {v5}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Integer;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v5, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    sget-object v5, Lcom/mediatek/mtklogger/utils/Utils;->LOG_PATH_MAP:Landroid/util/SparseArray;

    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    move-result v7

    invoke-virtual {v5, v7}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/lang/String;

    invoke-virtual {v6, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    move-result v5

    invoke-virtual {v2, v5, v0}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    goto :goto_0
.end method

.method private init()V
    .locals 7

    const/4 v4, 0x0

    const-string v2, "MTKLogger/TagLogManager"

    const-string v3, "-->init()"

    invoke-static {v2, v3}, Lcom/mediatek/mtklogger/utils/Utils;->logd(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v2, p0, Lcom/mediatek/mtklogger/taglog/TagLogManager;->mContext:Landroid/content/Context;

    const v3, 0x103006e

    invoke-virtual {v2, v3}, Landroid/content/Context;->setTheme(I)V

    sput-boolean v4, Lcom/mediatek/mtklogger/taglog/TagLogManager;->isTagingLog:Z

    new-instance v2, Lcom/mediatek/mtklogger/taglog/TagLogManager$UIHandler;

    const/4 v3, 0x0

    invoke-direct {v2, p0, v3}, Lcom/mediatek/mtklogger/taglog/TagLogManager$UIHandler;-><init>(Lcom/mediatek/mtklogger/taglog/TagLogManager;Lcom/mediatek/mtklogger/taglog/TagLogManager$1;)V

    iput-object v2, p0, Lcom/mediatek/mtklogger/taglog/TagLogManager;->mUIHandler:Lcom/mediatek/mtklogger/taglog/TagLogManager$UIHandler;

    iget-object v2, p0, Lcom/mediatek/mtklogger/taglog/TagLogManager;->mContext:Landroid/content/Context;

    const-string v3, "log_settings"

    invoke-virtual {v2, v3, v4}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v2

    iput-object v2, p0, Lcom/mediatek/mtklogger/taglog/TagLogManager;->mSharedPreferences:Landroid/content/SharedPreferences;

    iget-object v2, p0, Lcom/mediatek/mtklogger/taglog/TagLogManager;->mContext:Landroid/content/Context;

    invoke-static {v2}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v2

    iput-object v2, p0, Lcom/mediatek/mtklogger/taglog/TagLogManager;->mDefaultSharedPreferences:Landroid/content/SharedPreferences;

    iget-object v2, p0, Lcom/mediatek/mtklogger/taglog/TagLogManager;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f060007

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getStringArray(I)[Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_0

    array-length v2, v1

    if-lez v2, :cond_0

    aget-object v2, v1, v4

    iget-object v3, p0, Lcom/mediatek/mtklogger/taglog/TagLogManager;->mDefaultSharedPreferences:Landroid/content/SharedPreferences;

    const-string v4, "log_mode"

    iget-object v5, p0, Lcom/mediatek/mtklogger/taglog/TagLogManager;->mContext:Landroid/content/Context;

    const v6, 0x7f070073

    invoke-virtual {v5, v6}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v5

    invoke-interface {v3, v4, v5}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    iput-boolean v2, p0, Lcom/mediatek/mtklogger/taglog/TagLogManager;->mIgnoreMdLog:Z

    :cond_0
    const-string v2, "MTKLogger/TagLogManager"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "mIgnoreMdLog?"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-boolean v4, p0, Lcom/mediatek/mtklogger/taglog/TagLogManager;->mIgnoreMdLog:Z

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/mediatek/mtklogger/utils/Utils;->logd(Ljava/lang/String;Ljava/lang/String;)V

    new-instance v0, Landroid/content/IntentFilter;

    invoke-direct {v0}, Landroid/content/IntentFilter;-><init>()V

    const-string v2, "com.mediatek.mtklogger.intent.action.LOG_STATE_CHANGED"

    invoke-virtual {v0, v2}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    iget-object v2, p0, Lcom/mediatek/mtklogger/taglog/TagLogManager;->mContext:Landroid/content/Context;

    iget-object v3, p0, Lcom/mediatek/mtklogger/taglog/TagLogManager;->mLogStateReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {v2, v3, v0}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    iget-object v2, p0, Lcom/mediatek/mtklogger/taglog/TagLogManager;->mContext:Landroid/content/Context;

    instance-of v2, v2, Lcom/mediatek/mtklogger/framework/MTKLoggerService;

    if-eqz v2, :cond_1

    iget-object v2, p0, Lcom/mediatek/mtklogger/taglog/TagLogManager;->mContext:Landroid/content/Context;

    check-cast v2, Lcom/mediatek/mtklogger/framework/MTKLoggerService;

    iput-object v2, p0, Lcom/mediatek/mtklogger/taglog/TagLogManager;->mLogService:Lcom/mediatek/mtklogger/framework/MTKLoggerService;

    :goto_0
    return-void

    :cond_1
    const-string v2, "MTKLogger/TagLogManager"

    const-string v3, "Wrong parameter to contruct TagLogManager"

    invoke-static {v2, v3}, Lcom/mediatek/mtklogger/utils/Utils;->loge(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method private initToolStatus()V
    .locals 9

    const/4 v3, 0x1

    const/4 v4, 0x0

    sget-object v2, Lcom/mediatek/mtklogger/utils/Utils;->LOG_TYPE_SET:Ljava/util/Set;

    invoke-interface {v2}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Integer;

    iget-object v5, p0, Lcom/mediatek/mtklogger/taglog/TagLogManager;->mLogToolStatus:Landroid/util/SparseArray;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v6

    iget-object v7, p0, Lcom/mediatek/mtklogger/taglog/TagLogManager;->mSharedPreferences:Landroid/content/SharedPreferences;

    sget-object v2, Lcom/mediatek/mtklogger/utils/Utils;->KEY_STATUS_MAP:Landroid/util/SparseArray;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v8

    invoke-virtual {v2, v8}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    invoke-interface {v7, v2, v4}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v2

    if-ne v3, v2, :cond_0

    move v2, v3

    :goto_1
    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    invoke-virtual {v5, v6, v2}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    goto :goto_0

    :cond_0
    move v2, v4

    goto :goto_1

    :cond_1
    return-void
.end method

.method private monitorProgressDialogBar()V
    .locals 6

    iget-object v0, p0, Lcom/mediatek/mtklogger/taglog/TagLogManager;->mMonitorTimer:Ljava/util/Timer;

    if-eqz v0, :cond_0

    :goto_0
    return-void

    :cond_0
    new-instance v0, Ljava/util/Timer;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Ljava/util/Timer;-><init>(Z)V

    iput-object v0, p0, Lcom/mediatek/mtklogger/taglog/TagLogManager;->mMonitorTimer:Ljava/util/Timer;

    iget-object v0, p0, Lcom/mediatek/mtklogger/taglog/TagLogManager;->mMonitorTimer:Ljava/util/Timer;

    new-instance v1, Lcom/mediatek/mtklogger/taglog/TagLogManager$3;

    invoke-direct {v1, p0}, Lcom/mediatek/mtklogger/taglog/TagLogManager$3;-><init>(Lcom/mediatek/mtklogger/taglog/TagLogManager;)V

    const-wide/16 v2, 0x0

    const-wide/16 v4, 0xc8

    invoke-virtual/range {v0 .. v5}, Ljava/util/Timer;->schedule(Ljava/util/TimerTask;JJ)V

    goto :goto_0
.end method

.method private notifyToLog2Server()V
    .locals 7

    const-string v3, "MTKLogger/TagLogManager"

    const-string v4, "-->notifyToLog2Server()"

    invoke-static {v3, v4}, Lcom/mediatek/mtklogger/utils/Utils;->logd(Ljava/lang/String;Ljava/lang/String;)V

    new-instance v1, Landroid/content/Intent;

    invoke-direct {v1}, Landroid/content/Intent;-><init>()V

    const-string v3, "com.mediatek.syslogger.taglog"

    invoke-virtual {v1, v3}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    iget-object v3, p0, Lcom/mediatek/mtklogger/taglog/TagLogManager;->mTagLogResult:Ljava/lang/String;

    if-eqz v3, :cond_0

    const-string v3, "TaglogResult"

    iget-object v4, p0, Lcom/mediatek/mtklogger/taglog/TagLogManager;->mTagLogResult:Ljava/lang/String;

    invoke-virtual {v1, v3, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const-string v3, "MTKLogger/TagLogManager"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "TaglogResult = "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-object v5, p0, Lcom/mediatek/mtklogger/taglog/TagLogManager;->mTagLogResult:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/mediatek/mtklogger/utils/Utils;->logd(Ljava/lang/String;Ljava/lang/String;)V

    :goto_0
    iget-object v3, p0, Lcom/mediatek/mtklogger/taglog/TagLogManager;->mLogPathInTagLog:Landroid/util/SparseArray;

    if-eqz v3, :cond_2

    sget-object v3, Lcom/mediatek/mtklogger/utils/Utils;->LOG_TYPE_SET:Ljava/util/Set;

    invoke-interface {v3}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_1
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_3

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Integer;

    iget-object v3, p0, Lcom/mediatek/mtklogger/taglog/TagLogManager;->mLogPathInTagLog:Landroid/util/SparseArray;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v4

    invoke-virtual {v3, v4}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v3

    if-eqz v3, :cond_1

    sget-object v3, Lcom/mediatek/mtklogger/taglog/TagLogManager;->LOGPATHKEY:Landroid/util/SparseArray;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v4

    invoke-virtual {v3, v4}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    iget-object v4, p0, Lcom/mediatek/mtklogger/taglog/TagLogManager;->mLogPathInTagLog:Landroid/util/SparseArray;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v5

    invoke-virtual {v4, v5}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/String;

    invoke-virtual {v1, v3, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const-string v4, "MTKLogger/TagLogManager"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v3, Lcom/mediatek/mtklogger/taglog/TagLogManager;->LOGPATHKEY:Landroid/util/SparseArray;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v6

    invoke-virtual {v3, v6}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v5, " = "

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget-object v3, p0, Lcom/mediatek/mtklogger/taglog/TagLogManager;->mLogPathInTagLog:Landroid/util/SparseArray;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v6

    invoke-virtual {v3, v6}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v4, v3}, Lcom/mediatek/mtklogger/utils/Utils;->logd(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1

    :cond_0
    const-string v3, "MTKLogger/TagLogManager"

    const-string v4, "mTagLogResult is null!"

    invoke-static {v3, v4}, Lcom/mediatek/mtklogger/utils/Utils;->loge(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    :cond_1
    const-string v3, "MTKLogger/TagLogManager"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "mLogPathInTagLog["

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "]"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "= null!"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/mediatek/mtklogger/utils/Utils;->loge(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_1

    :cond_2
    const-string v3, "MTKLogger/TagLogManager"

    const-string v4, "mLogPathInTagLog is null"

    invoke-static {v3, v4}, Lcom/mediatek/mtklogger/utils/Utils;->loge(Ljava/lang/String;Ljava/lang/String;)V

    :cond_3
    iget-object v3, p0, Lcom/mediatek/mtklogger/taglog/TagLogManager;->mDataFromExtras:Landroid/os/Bundle;

    if-eqz v3, :cond_4

    iget-object v3, p0, Lcom/mediatek/mtklogger/taglog/TagLogManager;->mDataFromExtras:Landroid/os/Bundle;

    invoke-virtual {v1, v3}, Landroid/content/Intent;->putExtras(Landroid/os/Bundle;)Landroid/content/Intent;

    :goto_2
    iget-object v3, p0, Lcom/mediatek/mtklogger/taglog/TagLogManager;->mContext:Landroid/content/Context;

    invoke-static {v3}, Lcom/mediatek/mtklogger/exceptionreporter/ExceptionReportManager;->getInstance(Landroid/content/Context;)Lcom/mediatek/mtklogger/exceptionreporter/ExceptionReportManager;

    move-result-object v3

    invoke-virtual {v3, v1}, Lcom/mediatek/mtklogger/exceptionreporter/ExceptionReportManager;->beginException(Landroid/content/Intent;)Z

    const-string v3, "MTKLogger/TagLogManager"

    const-string v4, "send intent to Log2Server"

    invoke-static {v3, v4}, Lcom/mediatek/mtklogger/utils/Utils;->logd(Ljava/lang/String;Ljava/lang/String;)V

    return-void

    :cond_4
    const-string v3, "MTKLogger/TagLogManager"

    const-string v4, "Data From Aee is null"

    invoke-static {v3, v4}, Lcom/mediatek/mtklogger/utils/Utils;->loge(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_2
.end method

.method private rememberCurrentTaggingLogFolder(Ljava/lang/String;)V
    .locals 6
    .param p1    # Ljava/lang/String;

    const-string v3, "MTKLogger/TagLogManager"

    const-string v4, "-->rememberCurrentTaggingLogFolder()"

    invoke-static {v3, v4}, Lcom/mediatek/mtklogger/utils/Utils;->logd(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v3, p0, Lcom/mediatek/mtklogger/taglog/TagLogManager;->mSharedPreferences:Landroid/content/SharedPreferences;

    invoke-interface {v3}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    iget-object v3, p0, Lcom/mediatek/mtklogger/taglog/TagLogManager;->mCurrentLogFolderList:Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/mediatek/mtklogger/taglog/LogInformation;

    sget-object v3, Lcom/mediatek/mtklogger/utils/Utils;->KEY_TAGGING_LOG_MAP:Landroid/util/SparseArray;

    invoke-virtual {v2}, Lcom/mediatek/mtklogger/taglog/LogInformation;->getLogType()I

    move-result v4

    invoke-virtual {v3, v4}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    invoke-virtual {v2}, Lcom/mediatek/mtklogger/taglog/LogInformation;->getLogFile()Ljava/io/File;

    move-result-object v4

    invoke-virtual {v4}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v4

    invoke-interface {v0, v3, v4}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    goto :goto_0

    :cond_0
    const-string v3, "tagging_dest"

    invoke-interface {v0, v3, p1}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    move-result-object v3

    const-string v4, "tagging_db"

    iget-object v5, p0, Lcom/mediatek/mtklogger/taglog/TagLogManager;->mDbPathFromAee:Ljava/lang/String;

    invoke-interface {v3, v4, v5}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    move-result-object v3

    invoke-interface {v3}, Landroid/content/SharedPreferences$Editor;->commit()Z

    return-void
.end method

.method private startOrStopAllLogTool(Z)V
    .locals 9
    .param p1    # Z

    const/4 v8, 0x0

    const/4 v5, 0x0

    const-string v4, "MTKLogger/TagLogManager"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "startOrStopAllLogTool() -> isStart?"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v4, v6}, Lcom/mediatek/mtklogger/utils/Utils;->logd(Ljava/lang/String;Ljava/lang/String;)V

    const/4 v0, 0x0

    sget-object v4, Lcom/mediatek/mtklogger/utils/Utils;->LOG_TYPE_SET:Ljava/util/Set;

    invoke-interface {v4}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_3

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v4

    const/4 v6, 0x2

    if-ne v4, v6, :cond_1

    iget-boolean v4, p0, Lcom/mediatek/mtklogger/taglog/TagLogManager;->mIsModemExp:Z

    if-nez v4, :cond_0

    iget-boolean v4, p0, Lcom/mediatek/mtklogger/taglog/TagLogManager;->mIgnoreMdLog:Z

    if-nez v4, :cond_0

    :cond_1
    iget-object v4, p0, Lcom/mediatek/mtklogger/taglog/TagLogManager;->mLogToolStatus:Landroid/util/SparseArray;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v6

    invoke-static {v5}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v7

    invoke-virtual {v4, v6, v7}, Landroid/util/SparseArray;->get(ILjava/lang/Object;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/Boolean;

    invoke-virtual {v4}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v4

    if-eqz v4, :cond_2

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v4

    :goto_1
    or-int/2addr v0, v4

    goto :goto_0

    :cond_2
    move v4, v5

    goto :goto_1

    :cond_3
    if-nez v0, :cond_4

    :goto_2
    return-void

    :cond_4
    const/4 v3, 0x0

    if-eqz p1, :cond_5

    iget-object v4, p0, Lcom/mediatek/mtklogger/taglog/TagLogManager;->mLogService:Lcom/mediatek/mtklogger/framework/MTKLoggerService;

    invoke-virtual {v4, v0, v8}, Lcom/mediatek/mtklogger/framework/MTKLoggerService;->startRecording(ILjava/lang/String;)Z

    move-result v3

    :goto_3
    const-string v4, "MTKLogger/TagLogManager"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "startOrStopAllLogTool() -> result?"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/mediatek/mtklogger/utils/Utils;->logd(Ljava/lang/String;Ljava/lang/String;)V

    if-eqz v3, :cond_6

    const/4 v4, 0x1

    iput-boolean v4, p0, Lcom/mediatek/mtklogger/taglog/TagLogManager;->mIsWaitingLogStateChange:Z

    goto :goto_2

    :cond_5
    iget-object v4, p0, Lcom/mediatek/mtklogger/taglog/TagLogManager;->mLogService:Lcom/mediatek/mtklogger/framework/MTKLoggerService;

    invoke-virtual {v4, v0, v8}, Lcom/mediatek/mtklogger/framework/MTKLoggerService;->stopRecording(ILjava/lang/String;)Z

    move-result v3

    goto :goto_3

    :cond_6
    const-string v4, "MTKLogger/TagLogManager"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "-->startOrStopAllLogTool(), isStart="

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, ", operation fail!"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/mediatek/mtklogger/utils/Utils;->loge(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_2
.end method

.method private tagSelectedLogFolder(Ljava/lang/String;)V
    .locals 10
    .param p1    # Ljava/lang/String;

    const-string v6, "MTKLogger/TagLogManager"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "-->tagSelectedLogFolder(), targetFolderName="

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Lcom/mediatek/mtklogger/utils/Utils;->logd(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v6, p0, Lcom/mediatek/mtklogger/taglog/TagLogManager;->mSharedPreferences:Landroid/content/SharedPreferences;

    invoke-interface {v6}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v6

    const-string v7, "tag_log_compressing"

    const/4 v8, 0x1

    invoke-interface {v6, v7, v8}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    move-result-object v6

    invoke-interface {v6}, Landroid/content/SharedPreferences$Editor;->commit()Z

    iget-object v6, p0, Lcom/mediatek/mtklogger/taglog/TagLogManager;->mCurrentLogFolderList:Ljava/util/List;

    invoke-direct {p0, v6, p1}, Lcom/mediatek/mtklogger/taglog/TagLogManager;->zipAllLogAndDelete(Ljava/util/List;Ljava/lang/String;)Landroid/util/SparseArray;

    move-result-object v6

    iput-object v6, p0, Lcom/mediatek/mtklogger/taglog/TagLogManager;->mLogPathInTagLog:Landroid/util/SparseArray;

    sget-object v6, Lcom/mediatek/mtklogger/utils/Utils;->LOG_TYPE_SET:Ljava/util/Set;

    invoke-interface {v6}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v6

    if-eqz v6, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/lang/Integer;

    iget-object v6, p0, Lcom/mediatek/mtklogger/taglog/TagLogManager;->mLogPathInTagLog:Landroid/util/SparseArray;

    invoke-virtual {v5}, Ljava/lang/Integer;->intValue()I

    move-result v7

    invoke-virtual {v6, v7}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v6

    if-nez v6, :cond_0

    const-string v6, "MTKLogger/TagLogManager"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "mLogPathInTagLog["

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, "]"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, "= null"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Lcom/mediatek/mtklogger/utils/Utils;->logd(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    :cond_0
    const-string v7, "MTKLogger/TagLogManager"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "mLogPathInTagLog["

    invoke-virtual {v6, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v8, "]"

    invoke-virtual {v6, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v8, "="

    invoke-virtual {v6, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    iget-object v6, p0, Lcom/mediatek/mtklogger/taglog/TagLogManager;->mLogPathInTagLog:Landroid/util/SparseArray;

    invoke-virtual {v5}, Ljava/lang/Integer;->intValue()I

    move-result v9

    invoke-virtual {v6, v9}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Ljava/lang/String;

    invoke-virtual {v8, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v7, v6}, Lcom/mediatek/mtklogger/utils/Utils;->logd(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    :cond_1
    iget-boolean v6, p0, Lcom/mediatek/mtklogger/taglog/TagLogManager;->mManualSaveLog:Z

    if-nez v6, :cond_2

    iget-object v6, p0, Lcom/mediatek/mtklogger/taglog/TagLogManager;->mDbPathFromAee:Ljava/lang/String;

    invoke-static {v6, p1}, Lcom/mediatek/mtklogger/taglog/TagLogUtils;->writeFolderToTagFolder(Ljava/lang/String;Ljava/lang/String;)V

    :cond_2
    new-instance v0, Ljava/io/File;

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v6, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    sget-object v7, Ljava/io/File;->separator:Ljava/lang/String;

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, "checksop.txt"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-direct {v0, v6}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    if-eqz v0, :cond_4

    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v6

    if-eqz v6, :cond_3

    invoke-virtual {v0}, Ljava/io/File;->delete()Z

    :cond_3
    :try_start_0
    invoke-virtual {v0}, Ljava/io/File;->createNewFile()Z
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_1
    iget-object v6, p0, Lcom/mediatek/mtklogger/taglog/TagLogManager;->mContext:Landroid/content/Context;

    invoke-static {v6, v0}, Lcom/mediatek/mtklogger/taglog/TagLogUtils;->writeCheckSOPToFile(Landroid/content/Context;Ljava/io/File;)Z

    :cond_4
    const/4 v3, 0x1

    iget-object v6, p0, Lcom/mediatek/mtklogger/taglog/TagLogManager;->mCurrentLogFolderList:Ljava/util/List;

    invoke-interface {v6}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_5
    :goto_2
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v6

    if-eqz v6, :cond_6

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/mediatek/mtklogger/taglog/LogInformation;

    iget-object v6, p0, Lcom/mediatek/mtklogger/taglog/TagLogManager;->mLogPathInTagLog:Landroid/util/SparseArray;

    invoke-virtual {v4}, Lcom/mediatek/mtklogger/taglog/LogInformation;->getLogType()I

    move-result v7

    invoke-virtual {v6, v7}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v6

    if-nez v6, :cond_5

    const-string v6, "MTKLogger/TagLogManager"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "mLogPathInTagLog["

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v4}, Lcom/mediatek/mtklogger/taglog/LogInformation;->getLogType()I

    move-result v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, "]"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, "= null"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Lcom/mediatek/mtklogger/utils/Utils;->loge(Ljava/lang/String;Ljava/lang/String;)V

    const/4 v3, 0x0

    goto :goto_2

    :catch_0
    move-exception v1

    invoke-virtual {v1}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_1

    :cond_6
    if-eqz v3, :cond_7

    iget-object v6, p0, Lcom/mediatek/mtklogger/taglog/TagLogManager;->mUIHandler:Lcom/mediatek/mtklogger/taglog/TagLogManager$UIHandler;

    const/16 v7, 0xce

    invoke-virtual {v6, v7}, Lcom/mediatek/mtklogger/taglog/TagLogManager$UIHandler;->sendEmptyMessage(I)Z

    :goto_3
    iget-object v6, p0, Lcom/mediatek/mtklogger/taglog/TagLogManager;->mSharedPreferences:Landroid/content/SharedPreferences;

    invoke-interface {v6}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v6

    const-string v7, "tag_log_compressing"

    const/4 v8, 0x0

    invoke-interface {v6, v7, v8}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    move-result-object v6

    invoke-interface {v6}, Landroid/content/SharedPreferences$Editor;->commit()Z

    invoke-direct {p0}, Lcom/mediatek/mtklogger/taglog/TagLogManager;->forgetCachedTaggingLogFolder()V

    return-void

    :cond_7
    iget-object v6, p0, Lcom/mediatek/mtklogger/taglog/TagLogManager;->mUIHandler:Lcom/mediatek/mtklogger/taglog/TagLogManager$UIHandler;

    const/16 v7, 0xcf

    invoke-virtual {v6, v7}, Lcom/mediatek/mtklogger/taglog/TagLogManager$UIHandler;->sendEmptyMessage(I)Z

    goto :goto_3
.end method

.method private zipAllLogAndDelete(Ljava/util/List;Ljava/lang/String;)Landroid/util/SparseArray;
    .locals 7
    .param p2    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/mediatek/mtklogger/taglog/LogInformation;",
            ">;",
            "Ljava/lang/String;",
            ")",
            "Landroid/util/SparseArray",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    new-instance v2, Landroid/util/SparseArray;

    invoke-direct {v2}, Landroid/util/SparseArray;-><init>()V

    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_0
    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_1

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/mediatek/mtklogger/taglog/LogInformation;

    invoke-direct {p0, v0, p2}, Lcom/mediatek/mtklogger/taglog/TagLogManager;->zipLogAndDelete(Lcom/mediatek/mtklogger/taglog/LogInformation;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-virtual {v0}, Lcom/mediatek/mtklogger/taglog/LogInformation;->getLogType()I

    move-result v4

    invoke-virtual {v2, v4, v1}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    const-string v4, "MTKLogger/TagLogManager"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "currentTaglogPaths["

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v0}, Lcom/mediatek/mtklogger/taglog/LogInformation;->getLogType()I

    move-result v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "]"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "="

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/mediatek/mtklogger/utils/Utils;->logi(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    :cond_1
    return-object v2
.end method

.method private zipLogAndDelete(Lcom/mediatek/mtklogger/taglog/LogInformation;Ljava/lang/String;)Ljava/lang/String;
    .locals 11
    .param p1    # Lcom/mediatek/mtklogger/taglog/LogInformation;
    .param p2    # Ljava/lang/String;

    const/4 v8, 0x0

    invoke-virtual {p1}, Lcom/mediatek/mtklogger/taglog/LogInformation;->getLogFile()Ljava/io/File;

    move-result-object v1

    if-nez v1, :cond_0

    const-string v7, "MTKLogger/TagLogManager"

    const-string v9, "Needed log folder path is null!!"

    invoke-static {v7, v9}, Lcom/mediatek/mtklogger/utils/Utils;->loge(Ljava/lang/String;Ljava/lang/String;)V

    move-object v6, v8

    :goto_0
    return-object v6

    :cond_0
    const-string v7, "MTKLogger/TagLogManager"

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "-->zipLog() Folder : "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v1}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v7, v9}, Lcom/mediatek/mtklogger/utils/Utils;->logd(Ljava/lang/String;Ljava/lang/String;)V

    const/4 v6, 0x0

    if-eqz v1, :cond_6

    const-string v7, "dualmdlog"

    invoke-virtual {v1}, Ljava/io/File;->getParentFile()Ljava/io/File;

    move-result-object v9

    invoke-virtual {v9}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v7, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v7, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    sget-object v9, Ljava/io/File;->separator:Ljava/lang/String;

    invoke-virtual {v7, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    if-eqz v0, :cond_3

    const-string v7, "Dual"

    :goto_1
    invoke-virtual {v9, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v1}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v7, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v9, ".zip"

    invoke-virtual {v7, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    const-string v7, "MTKLogger/TagLogManager"

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "targetLogFileName :"

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v7, v9}, Lcom/mediatek/mtklogger/utils/Utils;->logi(Ljava/lang/String;Ljava/lang/String;)V

    new-instance v4, Ljava/io/File;

    invoke-direct {v4, v3}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    if-eqz v4, :cond_1

    invoke-virtual {v4}, Ljava/io/File;->exists()Z

    move-result v7

    if-eqz v7, :cond_1

    invoke-virtual {v4}, Ljava/io/File;->isFile()Z

    move-result v7

    if-eqz v7, :cond_1

    const-string v7, "MTKLogger/TagLogManager"

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "target taglog file ["

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, "] already exist, delete it first"

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v7, v9}, Lcom/mediatek/mtklogger/utils/Utils;->logw(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v4}, Ljava/io/File;->delete()Z

    :cond_1
    invoke-virtual {v1}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v7

    invoke-static {v7, v3}, Lcom/mediatek/mtklogger/taglog/ZipManager;->zipFileOrFolder(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v5

    if-nez v5, :cond_4

    const-string v7, "MTKLogger/TagLogManager"

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "Fail to zip log folder: "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v1}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v7, v9}, Lcom/mediatek/mtklogger/utils/Utils;->loge(Ljava/lang/String;Ljava/lang/String;)V

    new-instance v2, Ljava/io/File;

    invoke-direct {v2, v3}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2}, Ljava/io/File;->exists()Z

    move-result v7

    if-eqz v7, :cond_2

    invoke-virtual {v2}, Ljava/io/File;->delete()Z

    :cond_2
    move-object v6, v8

    goto/16 :goto_0

    :cond_3
    const-string v7, ""

    goto/16 :goto_1

    :cond_4
    const-string v7, "MTKLogger/TagLogManager"

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "Zip log success, target log file="

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8}, Lcom/mediatek/mtklogger/utils/Utils;->logi(Ljava/lang/String;Ljava/lang/String;)V

    move-object v6, v3

    iget-boolean v7, p0, Lcom/mediatek/mtklogger/taglog/TagLogManager;->mIsModemExp:Z

    if-eqz v7, :cond_5

    invoke-virtual {p1}, Lcom/mediatek/mtklogger/taglog/LogInformation;->getLogType()I

    move-result v7

    const/4 v8, 0x2

    if-ne v7, v8, :cond_5

    const-string v7, "MTKLogger/TagLogManager"

    const-string v8, "It is a ModemExp and not delete primary log folder"

    invoke-static {v7, v8}, Lcom/mediatek/mtklogger/utils/Utils;->logd(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    :cond_5
    invoke-static {v1}, Lcom/mediatek/mtklogger/utils/Utils;->deleteFolder(Ljava/io/File;)V

    :cond_6
    const-string v7, "MTKLogger/TagLogManager"

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "<--zipLog(), zipResultPath="

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8}, Lcom/mediatek/mtklogger/utils/Utils;->logd(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0
.end method


# virtual methods
.method public beginTag(Landroid/content/Intent;)V
    .locals 4
    .param p1    # Landroid/content/Intent;

    const/4 v3, 0x1

    const/4 v2, 0x0

    const-string v0, "MTKLogger/TagLogManager"

    const-string v1, "-->beginTag()"

    invoke-static {v0, v1}, Lcom/mediatek/mtklogger/utils/Utils;->logd(Ljava/lang/String;Ljava/lang/String;)V

    iput-boolean v2, p0, Lcom/mediatek/mtklogger/taglog/TagLogManager;->mIsFromMainActivity:Z

    const-string v0, "extra_key_from_main_activity"

    invoke-virtual {p1, v0, v2}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v0

    if-eqz v0, :cond_2

    sget-boolean v0, Lcom/mediatek/mtklogger/taglog/TagLogManager;->isTagingLog:Z

    if-eqz v0, :cond_1

    const-string v0, "MTKLogger/TagLogManager"

    const-string v1, "EXTRA_KEY_FROM_MAIN_ACTIVITY"

    invoke-static {v0, v1}, Lcom/mediatek/mtklogger/utils/Utils;->loge(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/mediatek/mtklogger/taglog/TagLogManager;->mUIHandler:Lcom/mediatek/mtklogger/taglog/TagLogManager$UIHandler;

    if-eqz v0, :cond_0

    iput-boolean v3, p0, Lcom/mediatek/mtklogger/taglog/TagLogManager;->mIsFromMainActivity:Z

    iget-object v0, p0, Lcom/mediatek/mtklogger/taglog/TagLogManager;->mUIHandler:Lcom/mediatek/mtklogger/taglog/TagLogManager$UIHandler;

    const/16 v1, 0x132

    invoke-virtual {v0, v1}, Lcom/mediatek/mtklogger/taglog/TagLogManager$UIHandler;->sendEmptyMessage(I)Z

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v0, p0, Lcom/mediatek/mtklogger/taglog/TagLogManager;->mSharedPreferences:Landroid/content/SharedPreferences;

    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string v1, "tag_log_compressing"

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    goto :goto_0

    :cond_2
    sget-boolean v0, Lcom/mediatek/mtklogger/taglog/TagLogManager;->isTagingLog:Z

    if-eqz v0, :cond_3

    const-string v0, "MTKLogger/TagLogManager"

    const-string v1, "TagLog is already on process, reject new request!"

    invoke-static {v0, v1}, Lcom/mediatek/mtklogger/utils/Utils;->logw(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/mediatek/mtklogger/taglog/TagLogManager;->mContext:Landroid/content/Context;

    const v1, 0x7f070070

    invoke-static {v0, v1, v2}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    goto :goto_0

    :cond_3
    sput-boolean v3, Lcom/mediatek/mtklogger/taglog/TagLogManager;->isTagingLog:Z

    invoke-virtual {p1}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v0

    iput-object v0, p0, Lcom/mediatek/mtklogger/taglog/TagLogManager;->mDataFromExtras:Landroid/os/Bundle;

    iget-object v0, p0, Lcom/mediatek/mtklogger/taglog/TagLogManager;->mUIHandler:Lcom/mediatek/mtklogger/taglog/TagLogManager$UIHandler;

    const/16 v1, 0xcb

    invoke-virtual {v0, v1}, Lcom/mediatek/mtklogger/taglog/TagLogManager$UIHandler;->sendEmptyMessage(I)Z

    goto :goto_0
.end method

.method public resumeTag()V
    .locals 12

    const/4 v11, 0x0

    iget-object v8, p0, Lcom/mediatek/mtklogger/taglog/TagLogManager;->mSharedPreferences:Landroid/content/SharedPreferences;

    const-string v9, "tag_log_compressing"

    invoke-interface {v8, v9, v11}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v2

    const-string v8, "MTKLogger/TagLogManager"

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "-->resumeTag(), isLastTaggingNotFinished="

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v8, v9}, Lcom/mediatek/mtklogger/utils/Utils;->logd(Ljava/lang/String;Ljava/lang/String;)V

    if-eqz v2, :cond_8

    iget-object v8, p0, Lcom/mediatek/mtklogger/taglog/TagLogManager;->mSharedPreferences:Landroid/content/SharedPreferences;

    const-string v9, "tagging_dest"

    const-string v10, ""

    invoke-interface {v8, v9, v10}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    iget-object v8, p0, Lcom/mediatek/mtklogger/taglog/TagLogManager;->mSharedPreferences:Landroid/content/SharedPreferences;

    const-string v9, "tagging_db"

    const-string v10, ""

    invoke-interface {v8, v9, v10}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v7}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v8

    if-eqz v8, :cond_0

    const-string v8, "MTKLogger/TagLogManager"

    const-string v9, "Last tagging flag is not null, but can not find target folder, ignore"

    invoke-static {v8, v9}, Lcom/mediatek/mtklogger/utils/Utils;->logw(Ljava/lang/String;Ljava/lang/String;)V

    :goto_0
    return-void

    :cond_0
    new-instance v6, Ljava/io/File;

    invoke-direct {v6, v7}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    if-eqz v6, :cond_1

    invoke-virtual {v6}, Ljava/io/File;->exists()Z

    move-result v8

    if-nez v8, :cond_2

    :cond_1
    const-string v8, "MTKLogger/TagLogManager"

    const-string v9, "Last tagging target folder does not exist, ignore"

    invoke-static {v8, v9}, Lcom/mediatek/mtklogger/utils/Utils;->logw(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    :cond_2
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v8

    if-eqz v8, :cond_3

    const-string v8, "MTKLogger/TagLogManager"

    const-string v9, "At resume time, find no db file, treat this as user trigger event"

    invoke-static {v8, v9}, Lcom/mediatek/mtklogger/utils/Utils;->logd(Ljava/lang/String;Ljava/lang/String;)V

    const/4 v8, 0x1

    iput-boolean v8, p0, Lcom/mediatek/mtklogger/taglog/TagLogManager;->mManualSaveLog:Z

    :goto_1
    new-instance v8, Ljava/util/ArrayList;

    invoke-direct {v8}, Ljava/util/ArrayList;-><init>()V

    iput-object v8, p0, Lcom/mediatek/mtklogger/taglog/TagLogManager;->mCurrentLogFolderList:Ljava/util/List;

    sget-object v8, Lcom/mediatek/mtklogger/utils/Utils;->LOG_TYPE_SET:Ljava/util/Set;

    invoke-interface {v8}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_2
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v8

    if-eqz v8, :cond_6

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Ljava/lang/Integer;

    invoke-virtual {v8}, Ljava/lang/Integer;->intValue()I

    move-result v5

    iget-object v9, p0, Lcom/mediatek/mtklogger/taglog/TagLogManager;->mSharedPreferences:Landroid/content/SharedPreferences;

    sget-object v8, Lcom/mediatek/mtklogger/utils/Utils;->KEY_TAGGING_LOG_MAP:Landroid/util/SparseArray;

    invoke-virtual {v8, v5}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Ljava/lang/String;

    const-string v10, ""

    invoke-interface {v9, v8, v10}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v8

    if-nez v8, :cond_5

    new-instance v3, Ljava/io/File;

    invoke-direct {v3, v4}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    if-eqz v3, :cond_4

    invoke-virtual {v3}, Ljava/io/File;->exists()Z

    move-result v8

    if-eqz v8, :cond_4

    const-string v8, "MTKLogger/TagLogManager"

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "Add not finished log folder: "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v8, v9}, Lcom/mediatek/mtklogger/utils/Utils;->logd(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v8, p0, Lcom/mediatek/mtklogger/taglog/TagLogManager;->mCurrentLogFolderList:Ljava/util/List;

    new-instance v9, Lcom/mediatek/mtklogger/taglog/LogInformation;

    invoke-direct {v9, v5, v3}, Lcom/mediatek/mtklogger/taglog/LogInformation;-><init>(ILjava/io/File;)V

    invoke-interface {v8, v9}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_2

    :cond_3
    iput-boolean v11, p0, Lcom/mediatek/mtklogger/taglog/TagLogManager;->mManualSaveLog:Z

    iput-object v0, p0, Lcom/mediatek/mtklogger/taglog/TagLogManager;->mDbPathFromAee:Ljava/lang/String;

    goto :goto_1

    :cond_4
    const-string v8, "MTKLogger/TagLogManager"

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "Log folder ["

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, "] not exist, maybe already tag finish."

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v8, v9}, Lcom/mediatek/mtklogger/utils/Utils;->logd(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_2

    :cond_5
    const-string v8, "MTKLogger/TagLogManager"

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "Log folder for type ["

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, "] is empty or null."

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v8, v9}, Lcom/mediatek/mtklogger/utils/Utils;->logd(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_2

    :cond_6
    iget-object v8, p0, Lcom/mediatek/mtklogger/taglog/TagLogManager;->mCurrentLogFolderList:Ljava/util/List;

    invoke-interface {v8}, Ljava/util/List;->size()I

    move-result v8

    if-lez v8, :cond_7

    new-instance v8, Lcom/mediatek/mtklogger/taglog/TagLogManager$ResumeTagThread;

    invoke-direct {v8, p0, v7}, Lcom/mediatek/mtklogger/taglog/TagLogManager$ResumeTagThread;-><init>(Lcom/mediatek/mtklogger/taglog/TagLogManager;Ljava/lang/String;)V

    invoke-virtual {v8}, Lcom/mediatek/mtklogger/taglog/TagLogManager$ResumeTagThread;->start()V

    goto/16 :goto_0

    :cond_7
    const-string v8, "MTKLogger/TagLogManager"

    const-string v9, "From taglog flag, need to resume tagging, but find no log folder that need tag, ignore"

    invoke-static {v8, v9}, Lcom/mediatek/mtklogger/utils/Utils;->logw(Ljava/lang/String;Ljava/lang/String;)V

    invoke-direct {p0}, Lcom/mediatek/mtklogger/taglog/TagLogManager;->forgetCachedTaggingLogFolder()V

    goto/16 :goto_0

    :cond_8
    invoke-direct {p0, v11}, Lcom/mediatek/mtklogger/taglog/TagLogManager;->deInit(Z)V

    goto/16 :goto_0
.end method
