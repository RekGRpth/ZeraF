.class public final Lcom/android/providers/downloads/ui/R$string;
.super Ljava/lang/Object;
.source "R.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/providers/downloads/ui/R;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "string"
.end annotation


# static fields
.field public static final action:I = 0x7f070008

.field public static final app_label:I = 0x7f070018

.field public static final authenticate_dialog_title:I = 0x7f070005

.field public static final button_sort_by_date:I = 0x7f07001e

.field public static final button_sort_by_size:I = 0x7f07001d

.field public static final cancel:I = 0x7f070009

.field public static final cancel_running_download:I = 0x7f070031

.field public static final confirm_oma_download_next_url:I = 0x7f070012

.field public static final confirm_oma_download_title:I = 0x7f07000b

.field public static final delete_download:I = 0x7f07002f

.field public static final delete_failed_with_SDcard:I = 0x7f070015

.field public static final deselect_all:I = 0x7f070033

.field public static final dialog_cannot_resume:I = 0x7f07002a

.field public static final dialog_failed_body:I = 0x7f070024

.field public static final dialog_file_already_exists:I = 0x7f07002b

.field public static final dialog_file_missing_body:I = 0x7f070027

.field public static final dialog_insufficient_space_on_cache:I = 0x7f070029

.field public static final dialog_insufficient_space_on_external:I = 0x7f070028

.field public static final dialog_media_not_found:I = 0x7f07002c

.field public static final dialog_queued_body:I = 0x7f070026

.field public static final dialog_title_not_available:I = 0x7f070023

.field public static final dialog_title_queued_body:I = 0x7f070025

.field public static final download_error:I = 0x7f070022

.field public static final download_error_attribute_mismatch:I = 0x7f070003

.field public static final download_error_insufficient_memory:I = 0x7f070000

.field public static final download_error_invalid_ddversion:I = 0x7f070002

.field public static final download_error_invalid_descriptor:I = 0x7f070001

.field public static final download_no_application_title:I = 0x7f07002d

.field public static final download_paused:I = 0x7f070004

.field public static final download_queued:I = 0x7f07001f

.field public static final download_running:I = 0x7f070020

.field public static final download_share_dialog:I = 0x7f070036

.field public static final download_success:I = 0x7f070021

.field public static final download_title_sorted_by_date:I = 0x7f070019

.field public static final download_title_sorted_by_size:I = 0x7f07001a

.field public static final keep_queued_download:I = 0x7f070030

.field public static final missing_title:I = 0x7f07001c

.field public static final no_downloads:I = 0x7f07001b

.field public static final ok:I = 0x7f07000a

.field public static final oma_download_content_not_supported:I = 0x7f070011

.field public static final oma_download_description:I = 0x7f070010

.field public static final oma_download_name:I = 0x7f07000c

.field public static final oma_download_size:I = 0x7f07000f

.field public static final oma_download_type:I = 0x7f07000e

.field public static final oma_download_vendor:I = 0x7f07000d

.field public static final password:I = 0x7f070007

.field public static final pause:I = 0x7f070013

.field public static final remove_download:I = 0x7f07002e

.field public static final resume:I = 0x7f070014

.field public static final retry_download:I = 0x7f070032

.field public static final select_all:I = 0x7f070034

.field public static final selected_count:I = 0x7f070035

.field public static final share_download:I = 0x7f070016

.field public static final share_failed_with_download_error:I = 0x7f070017

.field public static final username:I = 0x7f070006


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
