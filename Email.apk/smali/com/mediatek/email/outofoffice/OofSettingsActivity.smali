.class public Lcom/mediatek/email/outofoffice/OofSettingsActivity;
.super Landroid/app/Activity;
.source "OofSettingsActivity.java"

# interfaces
.implements Lcom/mediatek/email/outofoffice/AccountSettingsOutOfOfficeFragment$Callback;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/mediatek/email/outofoffice/OofSettingsActivity$AlertDialogFragment;
    }
.end annotation


# static fields
.field private static final ACCOUNT_ID:Ljava/lang/String; = "account_id"

.field public static final OOF_SETTING_ACTION:Ljava/lang/String; = "android.intent.action.OOF_SETTING_ACTION"


# instance fields
.field private mAccountId:J

.field private mActionBar:Landroid/app/ActionBar;

.field mCurrentFragment:Landroid/app/Fragment;


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Landroid/app/Activity;-><init>()V

    return-void
.end method

.method public static createIntent(Landroid/content/Context;J)Landroid/content/Intent;
    .locals 2
    .param p0    # Landroid/content/Context;
    .param p1    # J

    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    const-string v1, "android.intent.action.OOF_SETTING_ACTION"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    const-string v1, "account_id"

    invoke-virtual {v0, v1, p1, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;J)Landroid/content/Intent;

    return-object v0
.end method


# virtual methods
.method public onBackPressed()V
    .locals 6

    invoke-static {p0}, Lcom/android/email/Controller;->getInstance(Landroid/content/Context;)Lcom/android/email/Controller;

    move-result-object v2

    iget-wide v4, p0, Lcom/mediatek/email/outofoffice/OofSettingsActivity;->mAccountId:J

    invoke-virtual {v2, v4, v5}, Lcom/android/email/Controller;->stopOof(J)V

    const/4 v4, 0x0

    iput-object v4, p0, Lcom/mediatek/email/outofoffice/OofSettingsActivity;->mCurrentFragment:Landroid/app/Fragment;

    sget-object v1, Lcom/mediatek/email/outofoffice/AccountSettingsOutOfOfficeFragment;->sSavebutton:Landroid/view/View;

    check-cast v1, Landroid/widget/Button;

    invoke-virtual {v1}, Landroid/view/View;->isEnabled()Z

    move-result v4

    if-eqz v4, :cond_1

    invoke-virtual {p0}, Landroid/app/Activity;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v3

    const-string v4, "CancelAlert"

    invoke-virtual {v3, v4}, Landroid/app/FragmentManager;->findFragmentByTag(Ljava/lang/String;)Landroid/app/Fragment;

    move-result-object v0

    check-cast v0, Lcom/mediatek/email/outofoffice/OofSettingsActivity$AlertDialogFragment;

    if-nez v0, :cond_0

    const v4, 0x7f08005a

    const v5, 0x7f08005b

    invoke-static {v4, v5}, Lcom/mediatek/email/outofoffice/OofSettingsActivity$AlertDialogFragment;->newInstance(II)Lcom/mediatek/email/outofoffice/OofSettingsActivity$AlertDialogFragment;

    move-result-object v0

    invoke-virtual {p0}, Landroid/app/Activity;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v4

    const-string v5, "CancelAlert"

    invoke-virtual {v0, v4, v5}, Landroid/app/DialogFragment;->show(Landroid/app/FragmentManager;Ljava/lang/String;)V

    :cond_0
    :goto_0
    return-void

    :cond_1
    invoke-super {p0}, Landroid/app/Activity;->onBackPressed()V

    goto :goto_0
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 8
    .param p1    # Landroid/os/Bundle;

    const-wide/16 v6, -0x1

    const/4 v5, 0x4

    invoke-super {p0, p1}, Landroid/app/Activity;->onCreate(Landroid/os/Bundle;)V

    const v3, 0x7f040050

    invoke-virtual {p0, v3}, Landroid/app/Activity;->setContentView(I)V

    invoke-virtual {p0}, Landroid/app/Activity;->getIntent()Landroid/content/Intent;

    move-result-object v1

    const-string v3, "account_id"

    invoke-virtual {v1, v3, v6, v7}, Landroid/content/Intent;->getLongExtra(Ljava/lang/String;J)J

    move-result-wide v3

    iput-wide v3, p0, Lcom/mediatek/email/outofoffice/OofSettingsActivity;->mAccountId:J

    if-nez p1, :cond_0

    iget-wide v3, p0, Lcom/mediatek/email/outofoffice/OofSettingsActivity;->mAccountId:J

    cmp-long v3, v3, v6

    if-eqz v3, :cond_0

    invoke-virtual {p0}, Landroid/app/Activity;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v3

    invoke-virtual {v3}, Landroid/app/FragmentManager;->beginTransaction()Landroid/app/FragmentTransaction;

    move-result-object v0

    iget-wide v3, p0, Lcom/mediatek/email/outofoffice/OofSettingsActivity;->mAccountId:J

    invoke-static {v3, v4}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    invoke-static {v3}, Lcom/mediatek/email/outofoffice/AccountSettingsOutOfOfficeFragment;->newInstance(Ljava/lang/Long;)Lcom/mediatek/email/outofoffice/AccountSettingsOutOfOfficeFragment;

    move-result-object v2

    iput-object v2, p0, Lcom/mediatek/email/outofoffice/OofSettingsActivity;->mCurrentFragment:Landroid/app/Fragment;

    const v3, 0x7f0f00f1

    invoke-virtual {v0, v3, v2}, Landroid/app/FragmentTransaction;->add(ILandroid/app/Fragment;)Landroid/app/FragmentTransaction;

    invoke-virtual {v0}, Landroid/app/FragmentTransaction;->commit()I

    :cond_0
    invoke-virtual {p0}, Landroid/app/Activity;->getActionBar()Landroid/app/ActionBar;

    move-result-object v3

    iput-object v3, p0, Lcom/mediatek/email/outofoffice/OofSettingsActivity;->mActionBar:Landroid/app/ActionBar;

    iget-object v3, p0, Lcom/mediatek/email/outofoffice/OofSettingsActivity;->mActionBar:Landroid/app/ActionBar;

    invoke-virtual {v3, v5, v5}, Landroid/app/ActionBar;->setDisplayOptions(II)V

    iget-object v3, p0, Lcom/mediatek/email/outofoffice/OofSettingsActivity;->mActionBar:Landroid/app/ActionBar;

    const/4 v4, 0x1

    invoke-virtual {v3, v4}, Landroid/app/ActionBar;->setDisplayShowTitleEnabled(Z)V

    iget-object v3, p0, Lcom/mediatek/email/outofoffice/OofSettingsActivity;->mActionBar:Landroid/app/ActionBar;

    const v4, 0x7f08004e

    invoke-virtual {v3, v4}, Landroid/app/ActionBar;->setTitle(I)V

    return-void
.end method

.method public onOptionsItemSelected(Landroid/view/MenuItem;)Z
    .locals 1
    .param p1    # Landroid/view/MenuItem;

    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    invoke-super {p0, p1}, Landroid/app/Activity;->onOptionsItemSelected(Landroid/view/MenuItem;)Z

    move-result v0

    :goto_0
    return v0

    :pswitch_0
    invoke-virtual {p0}, Lcom/mediatek/email/outofoffice/OofSettingsActivity;->onBackPressed()V

    const/4 v0, 0x1

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x102002c
        :pswitch_0
    .end packed-switch
.end method

.method public onSettingFinished()V
    .locals 0

    invoke-virtual {p0}, Lcom/mediatek/email/outofoffice/OofSettingsActivity;->onBackPressed()V

    return-void
.end method
