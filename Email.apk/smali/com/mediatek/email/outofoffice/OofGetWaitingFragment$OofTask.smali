.class Lcom/mediatek/email/outofoffice/OofGetWaitingFragment$OofTask;
.super Lcom/android/emailcommon/utility/EmailAsyncTask;
.source "OofGetWaitingFragment.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/mediatek/email/outofoffice/OofGetWaitingFragment;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "OofTask"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/android/emailcommon/utility/EmailAsyncTask",
        "<",
        "Ljava/lang/Void;",
        "Ljava/lang/Void;",
        "Ljava/lang/Integer;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/mediatek/email/outofoffice/OofGetWaitingFragment;


# direct methods
.method public constructor <init>(Lcom/mediatek/email/outofoffice/OofGetWaitingFragment;)V
    .locals 1

    iput-object p1, p0, Lcom/mediatek/email/outofoffice/OofGetWaitingFragment$OofTask;->this$0:Lcom/mediatek/email/outofoffice/OofGetWaitingFragment;

    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/android/emailcommon/utility/EmailAsyncTask;-><init>(Lcom/android/emailcommon/utility/EmailAsyncTask$Tracker;)V

    return-void
.end method


# virtual methods
.method protected varargs doInBackground([Ljava/lang/Void;)Ljava/lang/Integer;
    .locals 5
    .param p1    # [Ljava/lang/Void;

    iget-object v1, p0, Lcom/mediatek/email/outofoffice/OofGetWaitingFragment$OofTask;->this$0:Lcom/mediatek/email/outofoffice/OofGetWaitingFragment;

    const/4 v2, -0x1

    invoke-static {v1, v2}, Lcom/mediatek/email/outofoffice/OofGetWaitingFragment;->access$000(Lcom/mediatek/email/outofoffice/OofGetWaitingFragment;I)V

    iget-object v1, p0, Lcom/mediatek/email/outofoffice/OofGetWaitingFragment$OofTask;->this$0:Lcom/mediatek/email/outofoffice/OofGetWaitingFragment;

    invoke-static {v1}, Lcom/mediatek/email/outofoffice/OofGetWaitingFragment;->access$100(Lcom/mediatek/email/outofoffice/OofGetWaitingFragment;)Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, Lcom/android/email/Controller;->getInstance(Landroid/content/Context;)Lcom/android/email/Controller;

    move-result-object v0

    iget-object v1, p0, Lcom/mediatek/email/outofoffice/OofGetWaitingFragment$OofTask;->this$0:Lcom/mediatek/email/outofoffice/OofGetWaitingFragment;

    invoke-static {v1}, Lcom/mediatek/email/outofoffice/OofGetWaitingFragment;->access$200(Lcom/mediatek/email/outofoffice/OofGetWaitingFragment;)Lcom/android/emailcommon/provider/Account;

    move-result-object v1

    iget-wide v1, v1, Lcom/android/emailcommon/provider/EmailContent;->mId:J

    const/4 v3, 0x0

    const/4 v4, 0x1

    invoke-virtual {v0, v1, v2, v3, v4}, Lcom/android/email/Controller;->syncOof(JLcom/android/emailcommon/service/OofParams;Z)Lcom/android/emailcommon/service/OofParams;

    move-result-object v1

    sput-object v1, Lcom/android/email/activity/setup/AccountSettingsFragment;->sOofParams:Lcom/android/emailcommon/service/OofParams;

    sget-object v1, Lcom/android/email/activity/setup/AccountSettingsFragment;->sOofParams:Lcom/android/emailcommon/service/OofParams;

    if-nez v1, :cond_0

    const-string v1, "OofGetWaitingFragment"

    const-string v2, "OofTask return oofParams is null."

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v1, 0x4

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    :goto_0
    return-object v1

    :cond_0
    sget-object v1, Lcom/android/email/activity/setup/AccountSettingsFragment;->sOofParams:Lcom/android/emailcommon/service/OofParams;

    invoke-virtual {v1}, Lcom/android/emailcommon/service/OofParams;->getmStatus()I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    goto :goto_0
.end method

.method protected bridge synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1
    .param p1    # [Ljava/lang/Object;

    check-cast p1, [Ljava/lang/Void;

    invoke-virtual {p0, p1}, Lcom/mediatek/email/outofoffice/OofGetWaitingFragment$OofTask;->doInBackground([Ljava/lang/Void;)Ljava/lang/Integer;

    move-result-object v0

    return-object v0
.end method

.method protected onSuccess(Ljava/lang/Integer;)V
    .locals 2
    .param p1    # Ljava/lang/Integer;

    iget-object v0, p0, Lcom/mediatek/email/outofoffice/OofGetWaitingFragment$OofTask;->this$0:Lcom/mediatek/email/outofoffice/OofGetWaitingFragment;

    invoke-virtual {p1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-static {v0, v1}, Lcom/mediatek/email/outofoffice/OofGetWaitingFragment;->access$302(Lcom/mediatek/email/outofoffice/OofGetWaitingFragment;I)I

    iget-object v0, p0, Lcom/mediatek/email/outofoffice/OofGetWaitingFragment$OofTask;->this$0:Lcom/mediatek/email/outofoffice/OofGetWaitingFragment;

    iget-boolean v0, v0, Lcom/mediatek/email/outofoffice/OofGetWaitingFragment;->mAttached:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/mediatek/email/outofoffice/OofGetWaitingFragment$OofTask;->this$0:Lcom/mediatek/email/outofoffice/OofGetWaitingFragment;

    invoke-virtual {p1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-static {v0, v1}, Lcom/mediatek/email/outofoffice/OofGetWaitingFragment;->access$000(Lcom/mediatek/email/outofoffice/OofGetWaitingFragment;I)V

    :cond_0
    return-void
.end method

.method protected bridge synthetic onSuccess(Ljava/lang/Object;)V
    .locals 0
    .param p1    # Ljava/lang/Object;

    check-cast p1, Ljava/lang/Integer;

    invoke-virtual {p0, p1}, Lcom/mediatek/email/outofoffice/OofGetWaitingFragment$OofTask;->onSuccess(Ljava/lang/Integer;)V

    return-void
.end method
