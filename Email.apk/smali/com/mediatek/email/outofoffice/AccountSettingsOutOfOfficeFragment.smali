.class public Lcom/mediatek/email/outofoffice/AccountSettingsOutOfOfficeFragment;
.super Landroid/app/Fragment;
.source "AccountSettingsOutOfOfficeFragment.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/mediatek/email/outofoffice/AccountSettingsOutOfOfficeFragment$TextChangeWatcher;,
        Lcom/mediatek/email/outofoffice/AccountSettingsOutOfOfficeFragment$WaitingFetchOofDialog;,
        Lcom/mediatek/email/outofoffice/AccountSettingsOutOfOfficeFragment$AlertDialogFragment;,
        Lcom/mediatek/email/outofoffice/AccountSettingsOutOfOfficeFragment$OofAsyncTask;,
        Lcom/mediatek/email/outofoffice/AccountSettingsOutOfOfficeFragment$Callback;
    }
.end annotation


# static fields
.field private static final ACCOUNT_ID:Ljava/lang/String; = "account_id"

.field private static final BUNDLE_KEY_ACTIVITY_TITLE:Ljava/lang/String; = "AccountSettingsOutOfOfficeFragment.title"

.field private static final BUNDLE_KEY_NEED_RESUME:Ljava/lang/String; = "ResumeSave"

.field private static final BUNDLE_KEY_SAVE:Ljava/lang/String; = "save"

.field public static final CANCEL_ALERT_TAG:Ljava/lang/String; = "CancelAlert"

.field private static final OOF_KEY_FROM:Ljava/lang/String; = "from"

.field private static final OOF_KEY_TO:Ljava/lang/String; = "to"

.field private static final OOF_PARAMS:Ljava/lang/String; = "Oof_params"

.field private static final OOF_REPLY:Ljava/lang/String; = "oof_reply"

.field private static final OOF_TO_EXTERNAL:Ljava/lang/String; = "oof_external"

.field private static final PICK_FROM_DATE:I = 0x0

.field private static final PICK_FROM_TIME:I = 0x2

.field private static final PICK_TO_DATE:I = 0x1

.field private static final PICK_TO_TIME:I = 0x3

.field private static final TAG:Ljava/lang/String; = "AccountSettingsOutOfOfficeFragment"

.field private static final TO_DAY:Ljava/lang/String; = "to_day"

.field private static final TO_DAY_TIME:Ljava/lang/String; = "to_daytime"

.field private static final TO_HOUR:Ljava/lang/String; = "to_hour"

.field private static final TO_MINUTE:Ljava/lang/String; = "to_minute"

.field private static final TO_MONTH:Ljava/lang/String; = "to_month"

.field private static final TO_YEAR:Ljava/lang/String; = "to_year"

.field static sSavebutton:Landroid/view/View;


# instance fields
.field private callBack1:Landroid/app/DatePickerDialog$OnDateSetListener;

.field private callBack2:Landroid/app/DatePickerDialog$OnDateSetListener;

.field private callBack3:Landroid/app/TimePickerDialog$OnTimeSetListener;

.field private callBack4:Landroid/app/TimePickerDialog$OnTimeSetListener;

.field private mAccount:Lcom/android/emailcommon/provider/Account;

.field private mAutoReply:Landroid/widget/TextView;

.field private mCallback:Lcom/mediatek/email/outofoffice/AccountSettingsOutOfOfficeFragment$Callback;

.field private mCancelButton:Landroid/widget/Button;

.field private mContext:Landroid/content/Context;

.field private mCurrentDay:I

.field private mCurrentDayTime:I

.field private mCurrentHour:I

.field private mCurrentMinute:I

.field private mCurrentMonth:I

.field private mCurrentWeekday:I

.field private mCurrentYear:I

.field private mDialog:Lcom/mediatek/email/outofoffice/AccountSettingsOutOfOfficeFragment$WaitingFetchOofDialog;

.field private mExternalView:Landroid/view/View;

.field private mFrom:Landroid/widget/TextView;

.field private mFromDateButton:Landroid/widget/Button;

.field private mFromDay:I

.field private mFromDayTime:I

.field private mFromHour:I

.field private mFromMinute:I

.field private mFromMonth:I

.field private mFromTimeButton:Landroid/widget/Button;

.field private mFromWeekday:I

.field private mFromYear:I

.field private mNeedToResume:Z

.field private mOofAsyncTask:Lcom/mediatek/email/outofoffice/AccountSettingsOutOfOfficeFragment$OofAsyncTask;

.field private mOofParams:Lcom/android/emailcommon/service/OofParams;

.field private mOutOffice:Landroid/widget/CheckBox;

.field private mPaused:Z

.field private mReplySumm:Landroid/widget/TextView;

.field private mReplyView:Landroid/view/View;

.field private mSaveButton:Landroid/widget/Button;

.field private mSeverCheck:Landroid/widget/CheckBox;

.field private mTitle:Landroid/widget/TextView;

.field private mTitleSumm:Landroid/widget/TextView;

.field private mTitleView:Landroid/view/View;

.field private mTo:Landroid/widget/TextView;

.field private mToDateButton:Landroid/widget/Button;

.field private mToDay:I

.field private mToDayTime:I

.field private mToHour:I

.field private mToMinute:I

.field private mToMonth:I

.field private mToServer:Landroid/widget/TextView;

.field private mToTimeButton:Landroid/widget/Button;

.field private mToWeekday:I

.field private mToYear:I


# direct methods
.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Landroid/app/Fragment;-><init>()V

    new-instance v0, Lcom/mediatek/email/outofoffice/AccountSettingsOutOfOfficeFragment$10;

    invoke-direct {v0, p0}, Lcom/mediatek/email/outofoffice/AccountSettingsOutOfOfficeFragment$10;-><init>(Lcom/mediatek/email/outofoffice/AccountSettingsOutOfOfficeFragment;)V

    iput-object v0, p0, Lcom/mediatek/email/outofoffice/AccountSettingsOutOfOfficeFragment;->callBack1:Landroid/app/DatePickerDialog$OnDateSetListener;

    new-instance v0, Lcom/mediatek/email/outofoffice/AccountSettingsOutOfOfficeFragment$11;

    invoke-direct {v0, p0}, Lcom/mediatek/email/outofoffice/AccountSettingsOutOfOfficeFragment$11;-><init>(Lcom/mediatek/email/outofoffice/AccountSettingsOutOfOfficeFragment;)V

    iput-object v0, p0, Lcom/mediatek/email/outofoffice/AccountSettingsOutOfOfficeFragment;->callBack2:Landroid/app/DatePickerDialog$OnDateSetListener;

    new-instance v0, Lcom/mediatek/email/outofoffice/AccountSettingsOutOfOfficeFragment$12;

    invoke-direct {v0, p0}, Lcom/mediatek/email/outofoffice/AccountSettingsOutOfOfficeFragment$12;-><init>(Lcom/mediatek/email/outofoffice/AccountSettingsOutOfOfficeFragment;)V

    iput-object v0, p0, Lcom/mediatek/email/outofoffice/AccountSettingsOutOfOfficeFragment;->callBack3:Landroid/app/TimePickerDialog$OnTimeSetListener;

    new-instance v0, Lcom/mediatek/email/outofoffice/AccountSettingsOutOfOfficeFragment$13;

    invoke-direct {v0, p0}, Lcom/mediatek/email/outofoffice/AccountSettingsOutOfOfficeFragment$13;-><init>(Lcom/mediatek/email/outofoffice/AccountSettingsOutOfOfficeFragment;)V

    iput-object v0, p0, Lcom/mediatek/email/outofoffice/AccountSettingsOutOfOfficeFragment;->callBack4:Landroid/app/TimePickerDialog$OnTimeSetListener;

    return-void
.end method

.method static synthetic access$000(Lcom/mediatek/email/outofoffice/AccountSettingsOutOfOfficeFragment;)Landroid/widget/CheckBox;
    .locals 1
    .param p0    # Lcom/mediatek/email/outofoffice/AccountSettingsOutOfOfficeFragment;

    iget-object v0, p0, Lcom/mediatek/email/outofoffice/AccountSettingsOutOfOfficeFragment;->mOutOffice:Landroid/widget/CheckBox;

    return-object v0
.end method

.method static synthetic access$100(Lcom/mediatek/email/outofoffice/AccountSettingsOutOfOfficeFragment;)Landroid/widget/CheckBox;
    .locals 1
    .param p0    # Lcom/mediatek/email/outofoffice/AccountSettingsOutOfOfficeFragment;

    iget-object v0, p0, Lcom/mediatek/email/outofoffice/AccountSettingsOutOfOfficeFragment;->mSeverCheck:Landroid/widget/CheckBox;

    return-object v0
.end method

.method static synthetic access$1000(Lcom/mediatek/email/outofoffice/AccountSettingsOutOfOfficeFragment;)I
    .locals 1
    .param p0    # Lcom/mediatek/email/outofoffice/AccountSettingsOutOfOfficeFragment;

    iget v0, p0, Lcom/mediatek/email/outofoffice/AccountSettingsOutOfOfficeFragment;->mFromDayTime:I

    return v0
.end method

.method static synthetic access$1002(Lcom/mediatek/email/outofoffice/AccountSettingsOutOfOfficeFragment;I)I
    .locals 0
    .param p0    # Lcom/mediatek/email/outofoffice/AccountSettingsOutOfOfficeFragment;
    .param p1    # I

    iput p1, p0, Lcom/mediatek/email/outofoffice/AccountSettingsOutOfOfficeFragment;->mFromDayTime:I

    return p1
.end method

.method static synthetic access$1100(Lcom/mediatek/email/outofoffice/AccountSettingsOutOfOfficeFragment;)I
    .locals 1
    .param p0    # Lcom/mediatek/email/outofoffice/AccountSettingsOutOfOfficeFragment;

    iget v0, p0, Lcom/mediatek/email/outofoffice/AccountSettingsOutOfOfficeFragment;->mFromHour:I

    return v0
.end method

.method static synthetic access$1102(Lcom/mediatek/email/outofoffice/AccountSettingsOutOfOfficeFragment;I)I
    .locals 0
    .param p0    # Lcom/mediatek/email/outofoffice/AccountSettingsOutOfOfficeFragment;
    .param p1    # I

    iput p1, p0, Lcom/mediatek/email/outofoffice/AccountSettingsOutOfOfficeFragment;->mFromHour:I

    return p1
.end method

.method static synthetic access$1120(Lcom/mediatek/email/outofoffice/AccountSettingsOutOfOfficeFragment;I)I
    .locals 1
    .param p0    # Lcom/mediatek/email/outofoffice/AccountSettingsOutOfOfficeFragment;
    .param p1    # I

    iget v0, p0, Lcom/mediatek/email/outofoffice/AccountSettingsOutOfOfficeFragment;->mFromHour:I

    sub-int/2addr v0, p1

    iput v0, p0, Lcom/mediatek/email/outofoffice/AccountSettingsOutOfOfficeFragment;->mFromHour:I

    return v0
.end method

.method static synthetic access$1200(Lcom/mediatek/email/outofoffice/AccountSettingsOutOfOfficeFragment;)I
    .locals 1
    .param p0    # Lcom/mediatek/email/outofoffice/AccountSettingsOutOfOfficeFragment;

    iget v0, p0, Lcom/mediatek/email/outofoffice/AccountSettingsOutOfOfficeFragment;->mFromMinute:I

    return v0
.end method

.method static synthetic access$1202(Lcom/mediatek/email/outofoffice/AccountSettingsOutOfOfficeFragment;I)I
    .locals 0
    .param p0    # Lcom/mediatek/email/outofoffice/AccountSettingsOutOfOfficeFragment;
    .param p1    # I

    iput p1, p0, Lcom/mediatek/email/outofoffice/AccountSettingsOutOfOfficeFragment;->mFromMinute:I

    return p1
.end method

.method static synthetic access$1300(Lcom/mediatek/email/outofoffice/AccountSettingsOutOfOfficeFragment;)I
    .locals 1
    .param p0    # Lcom/mediatek/email/outofoffice/AccountSettingsOutOfOfficeFragment;

    iget v0, p0, Lcom/mediatek/email/outofoffice/AccountSettingsOutOfOfficeFragment;->mToYear:I

    return v0
.end method

.method static synthetic access$1302(Lcom/mediatek/email/outofoffice/AccountSettingsOutOfOfficeFragment;I)I
    .locals 0
    .param p0    # Lcom/mediatek/email/outofoffice/AccountSettingsOutOfOfficeFragment;
    .param p1    # I

    iput p1, p0, Lcom/mediatek/email/outofoffice/AccountSettingsOutOfOfficeFragment;->mToYear:I

    return p1
.end method

.method static synthetic access$1400(Lcom/mediatek/email/outofoffice/AccountSettingsOutOfOfficeFragment;)I
    .locals 1
    .param p0    # Lcom/mediatek/email/outofoffice/AccountSettingsOutOfOfficeFragment;

    iget v0, p0, Lcom/mediatek/email/outofoffice/AccountSettingsOutOfOfficeFragment;->mToMonth:I

    return v0
.end method

.method static synthetic access$1402(Lcom/mediatek/email/outofoffice/AccountSettingsOutOfOfficeFragment;I)I
    .locals 0
    .param p0    # Lcom/mediatek/email/outofoffice/AccountSettingsOutOfOfficeFragment;
    .param p1    # I

    iput p1, p0, Lcom/mediatek/email/outofoffice/AccountSettingsOutOfOfficeFragment;->mToMonth:I

    return p1
.end method

.method static synthetic access$1500(Lcom/mediatek/email/outofoffice/AccountSettingsOutOfOfficeFragment;)I
    .locals 1
    .param p0    # Lcom/mediatek/email/outofoffice/AccountSettingsOutOfOfficeFragment;

    iget v0, p0, Lcom/mediatek/email/outofoffice/AccountSettingsOutOfOfficeFragment;->mToDay:I

    return v0
.end method

.method static synthetic access$1502(Lcom/mediatek/email/outofoffice/AccountSettingsOutOfOfficeFragment;I)I
    .locals 0
    .param p0    # Lcom/mediatek/email/outofoffice/AccountSettingsOutOfOfficeFragment;
    .param p1    # I

    iput p1, p0, Lcom/mediatek/email/outofoffice/AccountSettingsOutOfOfficeFragment;->mToDay:I

    return p1
.end method

.method static synthetic access$1600(Lcom/mediatek/email/outofoffice/AccountSettingsOutOfOfficeFragment;)I
    .locals 1
    .param p0    # Lcom/mediatek/email/outofoffice/AccountSettingsOutOfOfficeFragment;

    iget v0, p0, Lcom/mediatek/email/outofoffice/AccountSettingsOutOfOfficeFragment;->mToDayTime:I

    return v0
.end method

.method static synthetic access$1602(Lcom/mediatek/email/outofoffice/AccountSettingsOutOfOfficeFragment;I)I
    .locals 0
    .param p0    # Lcom/mediatek/email/outofoffice/AccountSettingsOutOfOfficeFragment;
    .param p1    # I

    iput p1, p0, Lcom/mediatek/email/outofoffice/AccountSettingsOutOfOfficeFragment;->mToDayTime:I

    return p1
.end method

.method static synthetic access$1700(Lcom/mediatek/email/outofoffice/AccountSettingsOutOfOfficeFragment;)I
    .locals 1
    .param p0    # Lcom/mediatek/email/outofoffice/AccountSettingsOutOfOfficeFragment;

    iget v0, p0, Lcom/mediatek/email/outofoffice/AccountSettingsOutOfOfficeFragment;->mToHour:I

    return v0
.end method

.method static synthetic access$1702(Lcom/mediatek/email/outofoffice/AccountSettingsOutOfOfficeFragment;I)I
    .locals 0
    .param p0    # Lcom/mediatek/email/outofoffice/AccountSettingsOutOfOfficeFragment;
    .param p1    # I

    iput p1, p0, Lcom/mediatek/email/outofoffice/AccountSettingsOutOfOfficeFragment;->mToHour:I

    return p1
.end method

.method static synthetic access$1720(Lcom/mediatek/email/outofoffice/AccountSettingsOutOfOfficeFragment;I)I
    .locals 1
    .param p0    # Lcom/mediatek/email/outofoffice/AccountSettingsOutOfOfficeFragment;
    .param p1    # I

    iget v0, p0, Lcom/mediatek/email/outofoffice/AccountSettingsOutOfOfficeFragment;->mToHour:I

    sub-int/2addr v0, p1

    iput v0, p0, Lcom/mediatek/email/outofoffice/AccountSettingsOutOfOfficeFragment;->mToHour:I

    return v0
.end method

.method static synthetic access$1800(Lcom/mediatek/email/outofoffice/AccountSettingsOutOfOfficeFragment;)I
    .locals 1
    .param p0    # Lcom/mediatek/email/outofoffice/AccountSettingsOutOfOfficeFragment;

    iget v0, p0, Lcom/mediatek/email/outofoffice/AccountSettingsOutOfOfficeFragment;->mToMinute:I

    return v0
.end method

.method static synthetic access$1802(Lcom/mediatek/email/outofoffice/AccountSettingsOutOfOfficeFragment;I)I
    .locals 0
    .param p0    # Lcom/mediatek/email/outofoffice/AccountSettingsOutOfOfficeFragment;
    .param p1    # I

    iput p1, p0, Lcom/mediatek/email/outofoffice/AccountSettingsOutOfOfficeFragment;->mToMinute:I

    return p1
.end method

.method static synthetic access$1900(Lcom/mediatek/email/outofoffice/AccountSettingsOutOfOfficeFragment;)Landroid/content/Context;
    .locals 1
    .param p0    # Lcom/mediatek/email/outofoffice/AccountSettingsOutOfOfficeFragment;

    iget-object v0, p0, Lcom/mediatek/email/outofoffice/AccountSettingsOutOfOfficeFragment;->mContext:Landroid/content/Context;

    return-object v0
.end method

.method static synthetic access$200(Lcom/mediatek/email/outofoffice/AccountSettingsOutOfOfficeFragment;)Landroid/widget/Button;
    .locals 1
    .param p0    # Lcom/mediatek/email/outofoffice/AccountSettingsOutOfOfficeFragment;

    iget-object v0, p0, Lcom/mediatek/email/outofoffice/AccountSettingsOutOfOfficeFragment;->mSaveButton:Landroid/widget/Button;

    return-object v0
.end method

.method static synthetic access$2000(Lcom/mediatek/email/outofoffice/AccountSettingsOutOfOfficeFragment;)Z
    .locals 1
    .param p0    # Lcom/mediatek/email/outofoffice/AccountSettingsOutOfOfficeFragment;

    iget-boolean v0, p0, Lcom/mediatek/email/outofoffice/AccountSettingsOutOfOfficeFragment;->mPaused:Z

    return v0
.end method

.method static synthetic access$2100(Lcom/mediatek/email/outofoffice/AccountSettingsOutOfOfficeFragment;I)V
    .locals 0
    .param p0    # Lcom/mediatek/email/outofoffice/AccountSettingsOutOfOfficeFragment;
    .param p1    # I

    invoke-direct {p0, p1}, Lcom/mediatek/email/outofoffice/AccountSettingsOutOfOfficeFragment;->reportProgress(I)V

    return-void
.end method

.method static synthetic access$2300(Lcom/mediatek/email/outofoffice/AccountSettingsOutOfOfficeFragment;)V
    .locals 0
    .param p0    # Lcom/mediatek/email/outofoffice/AccountSettingsOutOfOfficeFragment;

    invoke-direct {p0}, Lcom/mediatek/email/outofoffice/AccountSettingsOutOfOfficeFragment;->onCheckingDialogCancel()V

    return-void
.end method

.method static synthetic access$2400(Lcom/mediatek/email/outofoffice/AccountSettingsOutOfOfficeFragment;III)Ljava/lang/Long;
    .locals 1
    .param p0    # Lcom/mediatek/email/outofoffice/AccountSettingsOutOfOfficeFragment;
    .param p1    # I
    .param p2    # I
    .param p3    # I

    invoke-direct {p0, p1, p2, p3}, Lcom/mediatek/email/outofoffice/AccountSettingsOutOfOfficeFragment;->getDayNums(III)Ljava/lang/Long;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$2500(Lcom/mediatek/email/outofoffice/AccountSettingsOutOfOfficeFragment;Ljava/lang/Long;II)Ljava/lang/Long;
    .locals 1
    .param p0    # Lcom/mediatek/email/outofoffice/AccountSettingsOutOfOfficeFragment;
    .param p1    # Ljava/lang/Long;
    .param p2    # I
    .param p3    # I

    invoke-direct {p0, p1, p2, p3}, Lcom/mediatek/email/outofoffice/AccountSettingsOutOfOfficeFragment;->getMinuteNums(Ljava/lang/Long;II)Ljava/lang/Long;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$2600(Lcom/mediatek/email/outofoffice/AccountSettingsOutOfOfficeFragment;)I
    .locals 1
    .param p0    # Lcom/mediatek/email/outofoffice/AccountSettingsOutOfOfficeFragment;

    iget v0, p0, Lcom/mediatek/email/outofoffice/AccountSettingsOutOfOfficeFragment;->mFromWeekday:I

    return v0
.end method

.method static synthetic access$2602(Lcom/mediatek/email/outofoffice/AccountSettingsOutOfOfficeFragment;I)I
    .locals 0
    .param p0    # Lcom/mediatek/email/outofoffice/AccountSettingsOutOfOfficeFragment;
    .param p1    # I

    iput p1, p0, Lcom/mediatek/email/outofoffice/AccountSettingsOutOfOfficeFragment;->mFromWeekday:I

    return p1
.end method

.method static synthetic access$2700(Lcom/mediatek/email/outofoffice/AccountSettingsOutOfOfficeFragment;)Landroid/widget/Button;
    .locals 1
    .param p0    # Lcom/mediatek/email/outofoffice/AccountSettingsOutOfOfficeFragment;

    iget-object v0, p0, Lcom/mediatek/email/outofoffice/AccountSettingsOutOfOfficeFragment;->mFromDateButton:Landroid/widget/Button;

    return-object v0
.end method

.method static synthetic access$2800(Lcom/mediatek/email/outofoffice/AccountSettingsOutOfOfficeFragment;)I
    .locals 1
    .param p0    # Lcom/mediatek/email/outofoffice/AccountSettingsOutOfOfficeFragment;

    iget v0, p0, Lcom/mediatek/email/outofoffice/AccountSettingsOutOfOfficeFragment;->mToWeekday:I

    return v0
.end method

.method static synthetic access$2802(Lcom/mediatek/email/outofoffice/AccountSettingsOutOfOfficeFragment;I)I
    .locals 0
    .param p0    # Lcom/mediatek/email/outofoffice/AccountSettingsOutOfOfficeFragment;
    .param p1    # I

    iput p1, p0, Lcom/mediatek/email/outofoffice/AccountSettingsOutOfOfficeFragment;->mToWeekday:I

    return p1
.end method

.method static synthetic access$2900(Lcom/mediatek/email/outofoffice/AccountSettingsOutOfOfficeFragment;)Landroid/widget/Button;
    .locals 1
    .param p0    # Lcom/mediatek/email/outofoffice/AccountSettingsOutOfOfficeFragment;

    iget-object v0, p0, Lcom/mediatek/email/outofoffice/AccountSettingsOutOfOfficeFragment;->mToDateButton:Landroid/widget/Button;

    return-object v0
.end method

.method static synthetic access$300(Lcom/mediatek/email/outofoffice/AccountSettingsOutOfOfficeFragment;)Lcom/android/emailcommon/provider/Account;
    .locals 1
    .param p0    # Lcom/mediatek/email/outofoffice/AccountSettingsOutOfOfficeFragment;

    iget-object v0, p0, Lcom/mediatek/email/outofoffice/AccountSettingsOutOfOfficeFragment;->mAccount:Lcom/android/emailcommon/provider/Account;

    return-object v0
.end method

.method static synthetic access$3000(Lcom/mediatek/email/outofoffice/AccountSettingsOutOfOfficeFragment;)Landroid/widget/Button;
    .locals 1
    .param p0    # Lcom/mediatek/email/outofoffice/AccountSettingsOutOfOfficeFragment;

    iget-object v0, p0, Lcom/mediatek/email/outofoffice/AccountSettingsOutOfOfficeFragment;->mToTimeButton:Landroid/widget/Button;

    return-object v0
.end method

.method static synthetic access$3100(Lcom/mediatek/email/outofoffice/AccountSettingsOutOfOfficeFragment;)I
    .locals 1
    .param p0    # Lcom/mediatek/email/outofoffice/AccountSettingsOutOfOfficeFragment;

    iget v0, p0, Lcom/mediatek/email/outofoffice/AccountSettingsOutOfOfficeFragment;->mCurrentHour:I

    return v0
.end method

.method static synthetic access$3200(Lcom/mediatek/email/outofoffice/AccountSettingsOutOfOfficeFragment;)I
    .locals 1
    .param p0    # Lcom/mediatek/email/outofoffice/AccountSettingsOutOfOfficeFragment;

    iget v0, p0, Lcom/mediatek/email/outofoffice/AccountSettingsOutOfOfficeFragment;->mCurrentDayTime:I

    return v0
.end method

.method static synthetic access$3300(Lcom/mediatek/email/outofoffice/AccountSettingsOutOfOfficeFragment;)I
    .locals 1
    .param p0    # Lcom/mediatek/email/outofoffice/AccountSettingsOutOfOfficeFragment;

    iget v0, p0, Lcom/mediatek/email/outofoffice/AccountSettingsOutOfOfficeFragment;->mCurrentYear:I

    return v0
.end method

.method static synthetic access$3400(Lcom/mediatek/email/outofoffice/AccountSettingsOutOfOfficeFragment;)I
    .locals 1
    .param p0    # Lcom/mediatek/email/outofoffice/AccountSettingsOutOfOfficeFragment;

    iget v0, p0, Lcom/mediatek/email/outofoffice/AccountSettingsOutOfOfficeFragment;->mCurrentMonth:I

    return v0
.end method

.method static synthetic access$3500(Lcom/mediatek/email/outofoffice/AccountSettingsOutOfOfficeFragment;)I
    .locals 1
    .param p0    # Lcom/mediatek/email/outofoffice/AccountSettingsOutOfOfficeFragment;

    iget v0, p0, Lcom/mediatek/email/outofoffice/AccountSettingsOutOfOfficeFragment;->mCurrentDay:I

    return v0
.end method

.method static synthetic access$3600(Lcom/mediatek/email/outofoffice/AccountSettingsOutOfOfficeFragment;)I
    .locals 1
    .param p0    # Lcom/mediatek/email/outofoffice/AccountSettingsOutOfOfficeFragment;

    iget v0, p0, Lcom/mediatek/email/outofoffice/AccountSettingsOutOfOfficeFragment;->mCurrentMinute:I

    return v0
.end method

.method static synthetic access$3700(Lcom/mediatek/email/outofoffice/AccountSettingsOutOfOfficeFragment;)Landroid/widget/Button;
    .locals 1
    .param p0    # Lcom/mediatek/email/outofoffice/AccountSettingsOutOfOfficeFragment;

    iget-object v0, p0, Lcom/mediatek/email/outofoffice/AccountSettingsOutOfOfficeFragment;->mFromTimeButton:Landroid/widget/Button;

    return-object v0
.end method

.method static synthetic access$400(Lcom/mediatek/email/outofoffice/AccountSettingsOutOfOfficeFragment;)Lcom/mediatek/email/outofoffice/AccountSettingsOutOfOfficeFragment$OofAsyncTask;
    .locals 1
    .param p0    # Lcom/mediatek/email/outofoffice/AccountSettingsOutOfOfficeFragment;

    iget-object v0, p0, Lcom/mediatek/email/outofoffice/AccountSettingsOutOfOfficeFragment;->mOofAsyncTask:Lcom/mediatek/email/outofoffice/AccountSettingsOutOfOfficeFragment$OofAsyncTask;

    return-object v0
.end method

.method static synthetic access$402(Lcom/mediatek/email/outofoffice/AccountSettingsOutOfOfficeFragment;Lcom/mediatek/email/outofoffice/AccountSettingsOutOfOfficeFragment$OofAsyncTask;)Lcom/mediatek/email/outofoffice/AccountSettingsOutOfOfficeFragment$OofAsyncTask;
    .locals 0
    .param p0    # Lcom/mediatek/email/outofoffice/AccountSettingsOutOfOfficeFragment;
    .param p1    # Lcom/mediatek/email/outofoffice/AccountSettingsOutOfOfficeFragment$OofAsyncTask;

    iput-object p1, p0, Lcom/mediatek/email/outofoffice/AccountSettingsOutOfOfficeFragment;->mOofAsyncTask:Lcom/mediatek/email/outofoffice/AccountSettingsOutOfOfficeFragment$OofAsyncTask;

    return-object p1
.end method

.method static synthetic access$500(Lcom/mediatek/email/outofoffice/AccountSettingsOutOfOfficeFragment;)Lcom/mediatek/email/outofoffice/AccountSettingsOutOfOfficeFragment$WaitingFetchOofDialog;
    .locals 1
    .param p0    # Lcom/mediatek/email/outofoffice/AccountSettingsOutOfOfficeFragment;

    iget-object v0, p0, Lcom/mediatek/email/outofoffice/AccountSettingsOutOfOfficeFragment;->mDialog:Lcom/mediatek/email/outofoffice/AccountSettingsOutOfOfficeFragment$WaitingFetchOofDialog;

    return-object v0
.end method

.method static synthetic access$502(Lcom/mediatek/email/outofoffice/AccountSettingsOutOfOfficeFragment;Lcom/mediatek/email/outofoffice/AccountSettingsOutOfOfficeFragment$WaitingFetchOofDialog;)Lcom/mediatek/email/outofoffice/AccountSettingsOutOfOfficeFragment$WaitingFetchOofDialog;
    .locals 0
    .param p0    # Lcom/mediatek/email/outofoffice/AccountSettingsOutOfOfficeFragment;
    .param p1    # Lcom/mediatek/email/outofoffice/AccountSettingsOutOfOfficeFragment$WaitingFetchOofDialog;

    iput-object p1, p0, Lcom/mediatek/email/outofoffice/AccountSettingsOutOfOfficeFragment;->mDialog:Lcom/mediatek/email/outofoffice/AccountSettingsOutOfOfficeFragment$WaitingFetchOofDialog;

    return-object p1
.end method

.method static synthetic access$600(Lcom/mediatek/email/outofoffice/AccountSettingsOutOfOfficeFragment;)Lcom/mediatek/email/outofoffice/AccountSettingsOutOfOfficeFragment$Callback;
    .locals 1
    .param p0    # Lcom/mediatek/email/outofoffice/AccountSettingsOutOfOfficeFragment;

    iget-object v0, p0, Lcom/mediatek/email/outofoffice/AccountSettingsOutOfOfficeFragment;->mCallback:Lcom/mediatek/email/outofoffice/AccountSettingsOutOfOfficeFragment$Callback;

    return-object v0
.end method

.method static synthetic access$700(Lcom/mediatek/email/outofoffice/AccountSettingsOutOfOfficeFragment;)I
    .locals 1
    .param p0    # Lcom/mediatek/email/outofoffice/AccountSettingsOutOfOfficeFragment;

    iget v0, p0, Lcom/mediatek/email/outofoffice/AccountSettingsOutOfOfficeFragment;->mFromYear:I

    return v0
.end method

.method static synthetic access$702(Lcom/mediatek/email/outofoffice/AccountSettingsOutOfOfficeFragment;I)I
    .locals 0
    .param p0    # Lcom/mediatek/email/outofoffice/AccountSettingsOutOfOfficeFragment;
    .param p1    # I

    iput p1, p0, Lcom/mediatek/email/outofoffice/AccountSettingsOutOfOfficeFragment;->mFromYear:I

    return p1
.end method

.method static synthetic access$800(Lcom/mediatek/email/outofoffice/AccountSettingsOutOfOfficeFragment;)I
    .locals 1
    .param p0    # Lcom/mediatek/email/outofoffice/AccountSettingsOutOfOfficeFragment;

    iget v0, p0, Lcom/mediatek/email/outofoffice/AccountSettingsOutOfOfficeFragment;->mFromMonth:I

    return v0
.end method

.method static synthetic access$802(Lcom/mediatek/email/outofoffice/AccountSettingsOutOfOfficeFragment;I)I
    .locals 0
    .param p0    # Lcom/mediatek/email/outofoffice/AccountSettingsOutOfOfficeFragment;
    .param p1    # I

    iput p1, p0, Lcom/mediatek/email/outofoffice/AccountSettingsOutOfOfficeFragment;->mFromMonth:I

    return p1
.end method

.method static synthetic access$900(Lcom/mediatek/email/outofoffice/AccountSettingsOutOfOfficeFragment;)I
    .locals 1
    .param p0    # Lcom/mediatek/email/outofoffice/AccountSettingsOutOfOfficeFragment;

    iget v0, p0, Lcom/mediatek/email/outofoffice/AccountSettingsOutOfOfficeFragment;->mFromDay:I

    return v0
.end method

.method static synthetic access$902(Lcom/mediatek/email/outofoffice/AccountSettingsOutOfOfficeFragment;I)I
    .locals 0
    .param p0    # Lcom/mediatek/email/outofoffice/AccountSettingsOutOfOfficeFragment;
    .param p1    # I

    iput p1, p0, Lcom/mediatek/email/outofoffice/AccountSettingsOutOfOfficeFragment;->mFromDay:I

    return p1
.end method

.method private getDayNums(III)Ljava/lang/Long;
    .locals 4
    .param p1    # I
    .param p2    # I
    .param p3    # I

    const-wide/16 v0, 0x0

    add-int/lit16 v2, p1, -0x76b

    mul-int/lit16 v2, v2, 0x174

    int-to-long v2, v2

    add-long/2addr v0, v2

    mul-int/lit8 v2, p2, 0x1f

    int-to-long v2, v2

    add-long/2addr v0, v2

    int-to-long v2, p3

    add-long/2addr v0, v2

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    return-object v0
.end method

.method private getMinuteNums(Ljava/lang/Long;II)Ljava/lang/Long;
    .locals 4
    .param p1    # Ljava/lang/Long;
    .param p2    # I
    .param p3    # I

    invoke-virtual {p1}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    const-wide/32 v2, 0xa8c0

    mul-long/2addr v0, v2

    mul-int/lit8 v2, p2, 0x3c

    int-to-long v2, v2

    add-long/2addr v0, v2

    int-to-long v2, p3

    add-long/2addr v0, v2

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    return-object v0
.end method

.method private getSumMinute(IIIIII)J
    .locals 5
    .param p1    # I
    .param p2    # I
    .param p3    # I
    .param p4    # I
    .param p5    # I
    .param p6    # I

    invoke-direct {p0, p1, p2, p3}, Lcom/mediatek/email/outofoffice/AccountSettingsOutOfOfficeFragment;->getDayNums(III)Ljava/lang/Long;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Long;->longValue()J

    move-result-wide v1

    const/16 v3, 0xc

    if-eq p4, v3, :cond_1

    if-nez p6, :cond_0

    move v0, p4

    :goto_0
    invoke-static {v1, v2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    invoke-direct {p0, v3, v0, p5}, Lcom/mediatek/email/outofoffice/AccountSettingsOutOfOfficeFragment;->getMinuteNums(Ljava/lang/Long;II)Ljava/lang/Long;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Long;->longValue()J

    move-result-wide v3

    return-wide v3

    :cond_0
    add-int/lit8 v0, p4, 0xc

    goto :goto_0

    :cond_1
    move v0, p4

    goto :goto_0
.end method

.method public static newInstance(Ljava/lang/Long;)Lcom/mediatek/email/outofoffice/AccountSettingsOutOfOfficeFragment;
    .locals 5
    .param p0    # Ljava/lang/Long;

    new-instance v1, Lcom/mediatek/email/outofoffice/AccountSettingsOutOfOfficeFragment;

    invoke-direct {v1}, Lcom/mediatek/email/outofoffice/AccountSettingsOutOfOfficeFragment;-><init>()V

    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    const-string v2, "account_id"

    invoke-virtual {p0}, Ljava/lang/Long;->longValue()J

    move-result-wide v3

    invoke-virtual {v0, v2, v3, v4}, Landroid/os/Bundle;->putLong(Ljava/lang/String;J)V

    invoke-virtual {v1, v0}, Landroid/app/Fragment;->setArguments(Landroid/os/Bundle;)V

    return-object v1
.end method

.method private onCheckingDialogCancel()V
    .locals 1

    iget-object v0, p0, Lcom/mediatek/email/outofoffice/AccountSettingsOutOfOfficeFragment;->mOofAsyncTask:Lcom/mediatek/email/outofoffice/AccountSettingsOutOfOfficeFragment$OofAsyncTask;

    invoke-static {v0}, Lcom/android/emailcommon/utility/Utility;->cancelTaskInterrupt(Lcom/android/emailcommon/utility/EmailAsyncTask;)V

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/mediatek/email/outofoffice/AccountSettingsOutOfOfficeFragment;->mOofAsyncTask:Lcom/mediatek/email/outofoffice/AccountSettingsOutOfOfficeFragment$OofAsyncTask;

    return-void
.end method

.method private reportProgress(I)V
    .locals 4
    .param p1    # I

    const v3, 0x7f080012

    const/4 v2, 0x0

    iput-object v2, p0, Lcom/mediatek/email/outofoffice/AccountSettingsOutOfOfficeFragment;->mOofAsyncTask:Lcom/mediatek/email/outofoffice/AccountSettingsOutOfOfficeFragment$OofAsyncTask;

    iget-object v2, p0, Lcom/mediatek/email/outofoffice/AccountSettingsOutOfOfficeFragment;->mDialog:Lcom/mediatek/email/outofoffice/AccountSettingsOutOfOfficeFragment$WaitingFetchOofDialog;

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/mediatek/email/outofoffice/AccountSettingsOutOfOfficeFragment;->mDialog:Lcom/mediatek/email/outofoffice/AccountSettingsOutOfOfficeFragment$WaitingFetchOofDialog;

    invoke-virtual {v2}, Landroid/app/DialogFragment;->dismissAllowingStateLoss()V

    :cond_0
    const/4 v2, 0x1

    if-eq p1, v2, :cond_3

    const/4 v2, 0x3

    if-ne p1, v2, :cond_2

    invoke-virtual {p0}, Landroid/app/Fragment;->getActivity()Landroid/app/Activity;

    move-result-object v2

    invoke-static {v2}, Lcom/android/email/activity/UiUtilities;->isWifiOnly(Landroid/content/Context;)Z

    move-result v2

    if-eqz v2, :cond_1

    const v2, 0x7f080014

    invoke-static {v3, v2}, Lcom/mediatek/email/outofoffice/AccountSettingsOutOfOfficeFragment$AlertDialogFragment;->newInstance(II)Lcom/mediatek/email/outofoffice/AccountSettingsOutOfOfficeFragment$AlertDialogFragment;

    move-result-object v0

    :goto_0
    invoke-virtual {p0}, Landroid/app/Fragment;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v2

    const-string v3, "AlertDialogFragment"

    invoke-virtual {v0, v2, v3}, Landroid/app/DialogFragment;->show(Landroid/app/FragmentManager;Ljava/lang/String;)V

    :goto_1
    return-void

    :cond_1
    const v2, 0x7f080013

    invoke-static {v3, v2}, Lcom/mediatek/email/outofoffice/AccountSettingsOutOfOfficeFragment$AlertDialogFragment;->newInstance(II)Lcom/mediatek/email/outofoffice/AccountSettingsOutOfOfficeFragment$AlertDialogFragment;

    move-result-object v0

    goto :goto_0

    :cond_2
    const v2, 0x7f08005c

    const v3, 0x7f08005d

    invoke-static {v2, v3}, Lcom/mediatek/email/outofoffice/AccountSettingsOutOfOfficeFragment$AlertDialogFragment;->newInstance(II)Lcom/mediatek/email/outofoffice/AccountSettingsOutOfOfficeFragment$AlertDialogFragment;

    move-result-object v1

    invoke-virtual {p0}, Landroid/app/Fragment;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v2

    const-string v3, "AlertDialogFragment"

    invoke-virtual {v1, v2, v3}, Landroid/app/DialogFragment;->show(Landroid/app/FragmentManager;Ljava/lang/String;)V

    goto :goto_1

    :cond_3
    iget-object v2, p0, Lcom/mediatek/email/outofoffice/AccountSettingsOutOfOfficeFragment;->mSaveButton:Landroid/widget/Button;

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setEnabled(Z)V

    iget-object v2, p0, Lcom/mediatek/email/outofoffice/AccountSettingsOutOfOfficeFragment;->mCallback:Lcom/mediatek/email/outofoffice/AccountSettingsOutOfOfficeFragment$Callback;

    invoke-interface {v2}, Lcom/mediatek/email/outofoffice/AccountSettingsOutOfOfficeFragment$Callback;->onSettingFinished()V

    goto :goto_1
.end method


# virtual methods
.method public addChangeListener()V
    .locals 3

    const/4 v2, 0x0

    iget-object v0, p0, Lcom/mediatek/email/outofoffice/AccountSettingsOutOfOfficeFragment;->mTitleSumm:Landroid/widget/TextView;

    new-instance v1, Lcom/mediatek/email/outofoffice/AccountSettingsOutOfOfficeFragment$TextChangeWatcher;

    invoke-direct {v1, p0, v2}, Lcom/mediatek/email/outofoffice/AccountSettingsOutOfOfficeFragment$TextChangeWatcher;-><init>(Lcom/mediatek/email/outofoffice/AccountSettingsOutOfOfficeFragment;Lcom/mediatek/email/outofoffice/AccountSettingsOutOfOfficeFragment$1;)V

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->addTextChangedListener(Landroid/text/TextWatcher;)V

    iget-object v0, p0, Lcom/mediatek/email/outofoffice/AccountSettingsOutOfOfficeFragment;->mFromDateButton:Landroid/widget/Button;

    new-instance v1, Lcom/mediatek/email/outofoffice/AccountSettingsOutOfOfficeFragment$TextChangeWatcher;

    invoke-direct {v1, p0, v2}, Lcom/mediatek/email/outofoffice/AccountSettingsOutOfOfficeFragment$TextChangeWatcher;-><init>(Lcom/mediatek/email/outofoffice/AccountSettingsOutOfOfficeFragment;Lcom/mediatek/email/outofoffice/AccountSettingsOutOfOfficeFragment$1;)V

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->addTextChangedListener(Landroid/text/TextWatcher;)V

    iget-object v0, p0, Lcom/mediatek/email/outofoffice/AccountSettingsOutOfOfficeFragment;->mFromTimeButton:Landroid/widget/Button;

    new-instance v1, Lcom/mediatek/email/outofoffice/AccountSettingsOutOfOfficeFragment$TextChangeWatcher;

    invoke-direct {v1, p0, v2}, Lcom/mediatek/email/outofoffice/AccountSettingsOutOfOfficeFragment$TextChangeWatcher;-><init>(Lcom/mediatek/email/outofoffice/AccountSettingsOutOfOfficeFragment;Lcom/mediatek/email/outofoffice/AccountSettingsOutOfOfficeFragment$1;)V

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->addTextChangedListener(Landroid/text/TextWatcher;)V

    iget-object v0, p0, Lcom/mediatek/email/outofoffice/AccountSettingsOutOfOfficeFragment;->mToDateButton:Landroid/widget/Button;

    new-instance v1, Lcom/mediatek/email/outofoffice/AccountSettingsOutOfOfficeFragment$TextChangeWatcher;

    invoke-direct {v1, p0, v2}, Lcom/mediatek/email/outofoffice/AccountSettingsOutOfOfficeFragment$TextChangeWatcher;-><init>(Lcom/mediatek/email/outofoffice/AccountSettingsOutOfOfficeFragment;Lcom/mediatek/email/outofoffice/AccountSettingsOutOfOfficeFragment$1;)V

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->addTextChangedListener(Landroid/text/TextWatcher;)V

    iget-object v0, p0, Lcom/mediatek/email/outofoffice/AccountSettingsOutOfOfficeFragment;->mToTimeButton:Landroid/widget/Button;

    new-instance v1, Lcom/mediatek/email/outofoffice/AccountSettingsOutOfOfficeFragment$TextChangeWatcher;

    invoke-direct {v1, p0, v2}, Lcom/mediatek/email/outofoffice/AccountSettingsOutOfOfficeFragment$TextChangeWatcher;-><init>(Lcom/mediatek/email/outofoffice/AccountSettingsOutOfOfficeFragment;Lcom/mediatek/email/outofoffice/AccountSettingsOutOfOfficeFragment$1;)V

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->addTextChangedListener(Landroid/text/TextWatcher;)V

    iget-object v0, p0, Lcom/mediatek/email/outofoffice/AccountSettingsOutOfOfficeFragment;->mSeverCheck:Landroid/widget/CheckBox;

    new-instance v1, Lcom/mediatek/email/outofoffice/AccountSettingsOutOfOfficeFragment$14;

    invoke-direct {v1, p0}, Lcom/mediatek/email/outofoffice/AccountSettingsOutOfOfficeFragment$14;-><init>(Lcom/mediatek/email/outofoffice/AccountSettingsOutOfOfficeFragment;)V

    invoke-virtual {v0, v1}, Landroid/widget/CompoundButton;->setOnCheckedChangeListener(Landroid/widget/CompoundButton$OnCheckedChangeListener;)V

    iget-object v0, p0, Lcom/mediatek/email/outofoffice/AccountSettingsOutOfOfficeFragment;->mOutOffice:Landroid/widget/CheckBox;

    new-instance v1, Lcom/mediatek/email/outofoffice/AccountSettingsOutOfOfficeFragment$15;

    invoke-direct {v1, p0}, Lcom/mediatek/email/outofoffice/AccountSettingsOutOfOfficeFragment$15;-><init>(Lcom/mediatek/email/outofoffice/AccountSettingsOutOfOfficeFragment;)V

    invoke-virtual {v0, v1}, Landroid/widget/CompoundButton;->setOnCheckedChangeListener(Landroid/widget/CompoundButton$OnCheckedChangeListener;)V

    return-void
.end method

.method public disableOutOfficeOption()V
    .locals 2

    const/4 v1, 0x0

    iget-object v0, p0, Lcom/mediatek/email/outofoffice/AccountSettingsOutOfOfficeFragment;->mFrom:Landroid/widget/TextView;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setEnabled(Z)V

    iget-object v0, p0, Lcom/mediatek/email/outofoffice/AccountSettingsOutOfOfficeFragment;->mTo:Landroid/widget/TextView;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setEnabled(Z)V

    iget-object v0, p0, Lcom/mediatek/email/outofoffice/AccountSettingsOutOfOfficeFragment;->mAutoReply:Landroid/widget/TextView;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setEnabled(Z)V

    iget-object v0, p0, Lcom/mediatek/email/outofoffice/AccountSettingsOutOfOfficeFragment;->mReplySumm:Landroid/widget/TextView;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setEnabled(Z)V

    iget-object v0, p0, Lcom/mediatek/email/outofoffice/AccountSettingsOutOfOfficeFragment;->mToServer:Landroid/widget/TextView;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setEnabled(Z)V

    iget-object v0, p0, Lcom/mediatek/email/outofoffice/AccountSettingsOutOfOfficeFragment;->mFromDateButton:Landroid/widget/Button;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setEnabled(Z)V

    iget-object v0, p0, Lcom/mediatek/email/outofoffice/AccountSettingsOutOfOfficeFragment;->mFromTimeButton:Landroid/widget/Button;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setEnabled(Z)V

    iget-object v0, p0, Lcom/mediatek/email/outofoffice/AccountSettingsOutOfOfficeFragment;->mToDateButton:Landroid/widget/Button;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setEnabled(Z)V

    iget-object v0, p0, Lcom/mediatek/email/outofoffice/AccountSettingsOutOfOfficeFragment;->mToTimeButton:Landroid/widget/Button;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setEnabled(Z)V

    iget-object v0, p0, Lcom/mediatek/email/outofoffice/AccountSettingsOutOfOfficeFragment;->mSeverCheck:Landroid/widget/CheckBox;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setEnabled(Z)V

    iget-object v0, p0, Lcom/mediatek/email/outofoffice/AccountSettingsOutOfOfficeFragment;->mExternalView:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/view/View;->setEnabled(Z)V

    iget-object v0, p0, Lcom/mediatek/email/outofoffice/AccountSettingsOutOfOfficeFragment;->mReplyView:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/view/View;->setEnabled(Z)V

    iget-object v0, p0, Lcom/mediatek/email/outofoffice/AccountSettingsOutOfOfficeFragment;->mTitleSumm:Landroid/widget/TextView;

    const v1, 0x7f08004f

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    return-void
.end method

.method public enableOutOfficeOption()V
    .locals 2

    const/4 v1, 0x1

    iget-object v0, p0, Lcom/mediatek/email/outofoffice/AccountSettingsOutOfOfficeFragment;->mFrom:Landroid/widget/TextView;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setEnabled(Z)V

    iget-object v0, p0, Lcom/mediatek/email/outofoffice/AccountSettingsOutOfOfficeFragment;->mTo:Landroid/widget/TextView;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setEnabled(Z)V

    iget-object v0, p0, Lcom/mediatek/email/outofoffice/AccountSettingsOutOfOfficeFragment;->mAutoReply:Landroid/widget/TextView;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setEnabled(Z)V

    iget-object v0, p0, Lcom/mediatek/email/outofoffice/AccountSettingsOutOfOfficeFragment;->mReplySumm:Landroid/widget/TextView;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setEnabled(Z)V

    iget-object v0, p0, Lcom/mediatek/email/outofoffice/AccountSettingsOutOfOfficeFragment;->mToServer:Landroid/widget/TextView;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setEnabled(Z)V

    iget-object v0, p0, Lcom/mediatek/email/outofoffice/AccountSettingsOutOfOfficeFragment;->mFromDateButton:Landroid/widget/Button;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setEnabled(Z)V

    iget-object v0, p0, Lcom/mediatek/email/outofoffice/AccountSettingsOutOfOfficeFragment;->mFromTimeButton:Landroid/widget/Button;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setEnabled(Z)V

    iget-object v0, p0, Lcom/mediatek/email/outofoffice/AccountSettingsOutOfOfficeFragment;->mToDateButton:Landroid/widget/Button;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setEnabled(Z)V

    iget-object v0, p0, Lcom/mediatek/email/outofoffice/AccountSettingsOutOfOfficeFragment;->mToTimeButton:Landroid/widget/Button;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setEnabled(Z)V

    iget-object v0, p0, Lcom/mediatek/email/outofoffice/AccountSettingsOutOfOfficeFragment;->mSeverCheck:Landroid/widget/CheckBox;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setEnabled(Z)V

    iget-object v0, p0, Lcom/mediatek/email/outofoffice/AccountSettingsOutOfOfficeFragment;->mExternalView:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/view/View;->setEnabled(Z)V

    iget-object v0, p0, Lcom/mediatek/email/outofoffice/AccountSettingsOutOfOfficeFragment;->mReplyView:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/view/View;->setEnabled(Z)V

    iget-object v0, p0, Lcom/mediatek/email/outofoffice/AccountSettingsOutOfOfficeFragment;->mTitleSumm:Landroid/widget/TextView;

    const v1, 0x7f080050

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    return-void
.end method

.method public getCurrentTime()V
    .locals 2

    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Ljava/util/Calendar;->get(I)I

    move-result v1

    iput v1, p0, Lcom/mediatek/email/outofoffice/AccountSettingsOutOfOfficeFragment;->mCurrentYear:I

    const/4 v1, 0x2

    invoke-virtual {v0, v1}, Ljava/util/Calendar;->get(I)I

    move-result v1

    iput v1, p0, Lcom/mediatek/email/outofoffice/AccountSettingsOutOfOfficeFragment;->mCurrentMonth:I

    const/4 v1, 0x5

    invoke-virtual {v0, v1}, Ljava/util/Calendar;->get(I)I

    move-result v1

    iput v1, p0, Lcom/mediatek/email/outofoffice/AccountSettingsOutOfOfficeFragment;->mCurrentDay:I

    const/4 v1, 0x7

    invoke-virtual {v0, v1}, Ljava/util/Calendar;->get(I)I

    move-result v1

    iput v1, p0, Lcom/mediatek/email/outofoffice/AccountSettingsOutOfOfficeFragment;->mCurrentWeekday:I

    const/16 v1, 0xa

    invoke-virtual {v0, v1}, Ljava/util/Calendar;->get(I)I

    move-result v1

    iput v1, p0, Lcom/mediatek/email/outofoffice/AccountSettingsOutOfOfficeFragment;->mCurrentHour:I

    const/16 v1, 0xc

    invoke-virtual {v0, v1}, Ljava/util/Calendar;->get(I)I

    move-result v1

    iput v1, p0, Lcom/mediatek/email/outofoffice/AccountSettingsOutOfOfficeFragment;->mCurrentMinute:I

    const/16 v1, 0x9

    invoke-virtual {v0, v1}, Ljava/util/Calendar;->get(I)I

    move-result v1

    iput v1, p0, Lcom/mediatek/email/outofoffice/AccountSettingsOutOfOfficeFragment;->mCurrentDayTime:I

    return-void
.end method

.method public init()V
    .locals 11

    const/16 v10, 0x1e

    const/16 v9, 0x14

    const/4 v8, 0x0

    invoke-virtual {p0}, Lcom/mediatek/email/outofoffice/AccountSettingsOutOfOfficeFragment;->getCurrentTime()V

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    iget v6, p0, Lcom/mediatek/email/outofoffice/AccountSettingsOutOfOfficeFragment;->mCurrentWeekday:I

    invoke-static {v6, v9}, Landroid/text/format/DateUtils;->getDayOfWeekString(II)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, ", "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget v6, p0, Lcom/mediatek/email/outofoffice/AccountSettingsOutOfOfficeFragment;->mCurrentMonth:I

    invoke-static {v6, v10}, Landroid/text/format/DateUtils;->getMonthString(II)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, " "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget v6, p0, Lcom/mediatek/email/outofoffice/AccountSettingsOutOfOfficeFragment;->mCurrentDay:I

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, ", "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget v6, p0, Lcom/mediatek/email/outofoffice/AccountSettingsOutOfOfficeFragment;->mCurrentYear:I

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    iget v5, p0, Lcom/mediatek/email/outofoffice/AccountSettingsOutOfOfficeFragment;->mCurrentHour:I

    if-nez v5, :cond_0

    const/16 v5, 0xc

    :goto_0
    invoke-virtual {v6, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, ":00"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, " "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget v6, p0, Lcom/mediatek/email/outofoffice/AccountSettingsOutOfOfficeFragment;->mCurrentDayTime:I

    invoke-static {v6}, Landroid/text/format/DateUtils;->getAMPMString(I)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    iget v5, p0, Lcom/mediatek/email/outofoffice/AccountSettingsOutOfOfficeFragment;->mCurrentYear:I

    iput v5, p0, Lcom/mediatek/email/outofoffice/AccountSettingsOutOfOfficeFragment;->mFromYear:I

    iget v5, p0, Lcom/mediatek/email/outofoffice/AccountSettingsOutOfOfficeFragment;->mCurrentMonth:I

    iput v5, p0, Lcom/mediatek/email/outofoffice/AccountSettingsOutOfOfficeFragment;->mFromMonth:I

    iget v5, p0, Lcom/mediatek/email/outofoffice/AccountSettingsOutOfOfficeFragment;->mCurrentWeekday:I

    iput v5, p0, Lcom/mediatek/email/outofoffice/AccountSettingsOutOfOfficeFragment;->mFromWeekday:I

    iget v5, p0, Lcom/mediatek/email/outofoffice/AccountSettingsOutOfOfficeFragment;->mCurrentDay:I

    iput v5, p0, Lcom/mediatek/email/outofoffice/AccountSettingsOutOfOfficeFragment;->mFromDay:I

    iget v5, p0, Lcom/mediatek/email/outofoffice/AccountSettingsOutOfOfficeFragment;->mCurrentDayTime:I

    iput v5, p0, Lcom/mediatek/email/outofoffice/AccountSettingsOutOfOfficeFragment;->mFromDayTime:I

    iget v5, p0, Lcom/mediatek/email/outofoffice/AccountSettingsOutOfOfficeFragment;->mCurrentHour:I

    iput v5, p0, Lcom/mediatek/email/outofoffice/AccountSettingsOutOfOfficeFragment;->mFromHour:I

    iput v8, p0, Lcom/mediatek/email/outofoffice/AccountSettingsOutOfOfficeFragment;->mFromMinute:I

    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v0

    iget v5, p0, Lcom/mediatek/email/outofoffice/AccountSettingsOutOfOfficeFragment;->mCurrentYear:I

    iget v6, p0, Lcom/mediatek/email/outofoffice/AccountSettingsOutOfOfficeFragment;->mCurrentMonth:I

    iget v7, p0, Lcom/mediatek/email/outofoffice/AccountSettingsOutOfOfficeFragment;->mCurrentDay:I

    invoke-virtual {v0, v5, v6, v7}, Ljava/util/Calendar;->set(III)V

    invoke-virtual {v0}, Ljava/util/Calendar;->getTimeInMillis()J

    move-result-wide v3

    const-wide/32 v5, 0x5265c00

    add-long/2addr v3, v5

    invoke-virtual {v0, v3, v4}, Ljava/util/Calendar;->setTimeInMillis(J)V

    const/4 v5, 0x1

    invoke-virtual {v0, v5}, Ljava/util/Calendar;->get(I)I

    move-result v5

    iput v5, p0, Lcom/mediatek/email/outofoffice/AccountSettingsOutOfOfficeFragment;->mToYear:I

    const/4 v5, 0x2

    invoke-virtual {v0, v5}, Ljava/util/Calendar;->get(I)I

    move-result v5

    iput v5, p0, Lcom/mediatek/email/outofoffice/AccountSettingsOutOfOfficeFragment;->mToMonth:I

    const/4 v5, 0x5

    invoke-virtual {v0, v5}, Ljava/util/Calendar;->get(I)I

    move-result v5

    iput v5, p0, Lcom/mediatek/email/outofoffice/AccountSettingsOutOfOfficeFragment;->mToDay:I

    const/4 v5, 0x7

    invoke-virtual {v0, v5}, Ljava/util/Calendar;->get(I)I

    move-result v5

    iput v5, p0, Lcom/mediatek/email/outofoffice/AccountSettingsOutOfOfficeFragment;->mToWeekday:I

    iget v5, p0, Lcom/mediatek/email/outofoffice/AccountSettingsOutOfOfficeFragment;->mCurrentHour:I

    iput v5, p0, Lcom/mediatek/email/outofoffice/AccountSettingsOutOfOfficeFragment;->mToHour:I

    iput v8, p0, Lcom/mediatek/email/outofoffice/AccountSettingsOutOfOfficeFragment;->mToMinute:I

    iget v5, p0, Lcom/mediatek/email/outofoffice/AccountSettingsOutOfOfficeFragment;->mCurrentDayTime:I

    iput v5, p0, Lcom/mediatek/email/outofoffice/AccountSettingsOutOfOfficeFragment;->mToDayTime:I

    iget-object v5, p0, Lcom/mediatek/email/outofoffice/AccountSettingsOutOfOfficeFragment;->mFromDateButton:Landroid/widget/Button;

    invoke-virtual {v5, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v5, p0, Lcom/mediatek/email/outofoffice/AccountSettingsOutOfOfficeFragment;->mToTimeButton:Landroid/widget/Button;

    invoke-virtual {v5, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    iget v6, p0, Lcom/mediatek/email/outofoffice/AccountSettingsOutOfOfficeFragment;->mToWeekday:I

    invoke-static {v6, v9}, Landroid/text/format/DateUtils;->getDayOfWeekString(II)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, ", "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget v6, p0, Lcom/mediatek/email/outofoffice/AccountSettingsOutOfOfficeFragment;->mToMonth:I

    invoke-static {v6, v10}, Landroid/text/format/DateUtils;->getMonthString(II)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, " "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget v6, p0, Lcom/mediatek/email/outofoffice/AccountSettingsOutOfOfficeFragment;->mToDay:I

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, ", "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget v6, p0, Lcom/mediatek/email/outofoffice/AccountSettingsOutOfOfficeFragment;->mToYear:I

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    iget-object v5, p0, Lcom/mediatek/email/outofoffice/AccountSettingsOutOfOfficeFragment;->mToDateButton:Landroid/widget/Button;

    invoke-virtual {v5, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v5, p0, Lcom/mediatek/email/outofoffice/AccountSettingsOutOfOfficeFragment;->mFromTimeButton:Landroid/widget/Button;

    invoke-virtual {v5, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v5, p0, Lcom/mediatek/email/outofoffice/AccountSettingsOutOfOfficeFragment;->mOutOffice:Landroid/widget/CheckBox;

    invoke-virtual {v5}, Landroid/widget/CompoundButton;->isChecked()Z

    move-result v5

    if-nez v5, :cond_1

    invoke-virtual {p0}, Lcom/mediatek/email/outofoffice/AccountSettingsOutOfOfficeFragment;->disableOutOfficeOption()V

    :goto_1
    return-void

    :cond_0
    iget v5, p0, Lcom/mediatek/email/outofoffice/AccountSettingsOutOfOfficeFragment;->mCurrentHour:I

    goto/16 :goto_0

    :cond_1
    invoke-virtual {p0}, Lcom/mediatek/email/outofoffice/AccountSettingsOutOfOfficeFragment;->enableOutOfficeOption()V

    goto :goto_1
.end method

.method public initTimeDisplay(ZLandroid/os/Bundle;)V
    .locals 4
    .param p1    # Z
    .param p2    # Landroid/os/Bundle;

    if-nez p1, :cond_0

    if-eqz p2, :cond_2

    :cond_0
    if-eqz p1, :cond_1

    invoke-virtual {p0}, Lcom/mediatek/email/outofoffice/AccountSettingsOutOfOfficeFragment;->enableOutOfficeOption()V

    :goto_0
    iget-object v3, p0, Lcom/mediatek/email/outofoffice/AccountSettingsOutOfOfficeFragment;->mOofParams:Lcom/android/emailcommon/service/OofParams;

    invoke-virtual {v3}, Lcom/android/emailcommon/service/OofParams;->getStartTimeInMillis()J

    move-result-wide v1

    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v0

    invoke-virtual {v0, v1, v2}, Ljava/util/Calendar;->setTimeInMillis(J)V

    const-string v3, "from"

    invoke-virtual {p0, v0, v3}, Lcom/mediatek/email/outofoffice/AccountSettingsOutOfOfficeFragment;->setTimeByCalendar(Ljava/util/Calendar;Ljava/lang/String;)V

    iget-object v3, p0, Lcom/mediatek/email/outofoffice/AccountSettingsOutOfOfficeFragment;->mOofParams:Lcom/android/emailcommon/service/OofParams;

    invoke-virtual {v3}, Lcom/android/emailcommon/service/OofParams;->getEndTimeInMillis()J

    move-result-wide v1

    invoke-virtual {v0, v1, v2}, Ljava/util/Calendar;->setTimeInMillis(J)V

    const-string v3, "to"

    invoke-virtual {p0, v0, v3}, Lcom/mediatek/email/outofoffice/AccountSettingsOutOfOfficeFragment;->setTimeByCalendar(Ljava/util/Calendar;Ljava/lang/String;)V

    :goto_1
    return-void

    :cond_1
    invoke-virtual {p0}, Lcom/mediatek/email/outofoffice/AccountSettingsOutOfOfficeFragment;->disableOutOfficeOption()V

    goto :goto_0

    :cond_2
    invoke-virtual {p0}, Lcom/mediatek/email/outofoffice/AccountSettingsOutOfOfficeFragment;->init()V

    goto :goto_1
.end method

.method public loadSettings(Landroid/view/View;Landroid/os/Bundle;)V
    .locals 2
    .param p1    # Landroid/view/View;
    .param p2    # Landroid/os/Bundle;

    const v0, 0x7f0f0009

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/mediatek/email/outofoffice/AccountSettingsOutOfOfficeFragment;->mTitle:Landroid/widget/TextView;

    const v0, 0x7f0f000a

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/mediatek/email/outofoffice/AccountSettingsOutOfOfficeFragment;->mTitleSumm:Landroid/widget/TextView;

    const v0, 0x7f0f0008

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/mediatek/email/outofoffice/AccountSettingsOutOfOfficeFragment;->mTitleView:Landroid/view/View;

    const v0, 0x7f0f0014

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/mediatek/email/outofoffice/AccountSettingsOutOfOfficeFragment;->mReplyView:Landroid/view/View;

    const v0, 0x7f0f0017

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/mediatek/email/outofoffice/AccountSettingsOutOfOfficeFragment;->mExternalView:Landroid/view/View;

    const v0, 0x7f0f000d

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/mediatek/email/outofoffice/AccountSettingsOutOfOfficeFragment;->mFrom:Landroid/widget/TextView;

    const v0, 0x7f0f0011

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/mediatek/email/outofoffice/AccountSettingsOutOfOfficeFragment;->mTo:Landroid/widget/TextView;

    const v0, 0x7f0f0015

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/mediatek/email/outofoffice/AccountSettingsOutOfOfficeFragment;->mAutoReply:Landroid/widget/TextView;

    const v0, 0x7f0f0016

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/mediatek/email/outofoffice/AccountSettingsOutOfOfficeFragment;->mReplySumm:Landroid/widget/TextView;

    const v0, 0x7f0f0018

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/mediatek/email/outofoffice/AccountSettingsOutOfOfficeFragment;->mToServer:Landroid/widget/TextView;

    const v0, 0x7f0f000e

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lcom/mediatek/email/outofoffice/AccountSettingsOutOfOfficeFragment;->mFromDateButton:Landroid/widget/Button;

    const v0, 0x7f0f000f

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lcom/mediatek/email/outofoffice/AccountSettingsOutOfOfficeFragment;->mFromTimeButton:Landroid/widget/Button;

    const v0, 0x7f0f0012

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lcom/mediatek/email/outofoffice/AccountSettingsOutOfOfficeFragment;->mToDateButton:Landroid/widget/Button;

    const v0, 0x7f0f0013

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lcom/mediatek/email/outofoffice/AccountSettingsOutOfOfficeFragment;->mToTimeButton:Landroid/widget/Button;

    const v0, 0x7f0f0001

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lcom/mediatek/email/outofoffice/AccountSettingsOutOfOfficeFragment;->mCancelButton:Landroid/widget/Button;

    const v0, 0x7f0f001a

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lcom/mediatek/email/outofoffice/AccountSettingsOutOfOfficeFragment;->mSaveButton:Landroid/widget/Button;

    iget-object v0, p0, Lcom/mediatek/email/outofoffice/AccountSettingsOutOfOfficeFragment;->mSaveButton:Landroid/widget/Button;

    sput-object v0, Lcom/mediatek/email/outofoffice/AccountSettingsOutOfOfficeFragment;->sSavebutton:Landroid/view/View;

    iget-object v0, p0, Lcom/mediatek/email/outofoffice/AccountSettingsOutOfOfficeFragment;->mTitleView:Landroid/view/View;

    new-instance v1, Lcom/mediatek/email/outofoffice/AccountSettingsOutOfOfficeFragment$1;

    invoke-direct {v1, p0}, Lcom/mediatek/email/outofoffice/AccountSettingsOutOfOfficeFragment$1;-><init>(Lcom/mediatek/email/outofoffice/AccountSettingsOutOfOfficeFragment;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v0, p0, Lcom/mediatek/email/outofoffice/AccountSettingsOutOfOfficeFragment;->mExternalView:Landroid/view/View;

    new-instance v1, Lcom/mediatek/email/outofoffice/AccountSettingsOutOfOfficeFragment$2;

    invoke-direct {v1, p0}, Lcom/mediatek/email/outofoffice/AccountSettingsOutOfOfficeFragment$2;-><init>(Lcom/mediatek/email/outofoffice/AccountSettingsOutOfOfficeFragment;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v0, p0, Lcom/mediatek/email/outofoffice/AccountSettingsOutOfOfficeFragment;->mFromDateButton:Landroid/widget/Button;

    new-instance v1, Lcom/mediatek/email/outofoffice/AccountSettingsOutOfOfficeFragment$3;

    invoke-direct {v1, p0}, Lcom/mediatek/email/outofoffice/AccountSettingsOutOfOfficeFragment$3;-><init>(Lcom/mediatek/email/outofoffice/AccountSettingsOutOfOfficeFragment;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v0, p0, Lcom/mediatek/email/outofoffice/AccountSettingsOutOfOfficeFragment;->mToDateButton:Landroid/widget/Button;

    new-instance v1, Lcom/mediatek/email/outofoffice/AccountSettingsOutOfOfficeFragment$4;

    invoke-direct {v1, p0}, Lcom/mediatek/email/outofoffice/AccountSettingsOutOfOfficeFragment$4;-><init>(Lcom/mediatek/email/outofoffice/AccountSettingsOutOfOfficeFragment;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v0, p0, Lcom/mediatek/email/outofoffice/AccountSettingsOutOfOfficeFragment;->mFromTimeButton:Landroid/widget/Button;

    new-instance v1, Lcom/mediatek/email/outofoffice/AccountSettingsOutOfOfficeFragment$5;

    invoke-direct {v1, p0}, Lcom/mediatek/email/outofoffice/AccountSettingsOutOfOfficeFragment$5;-><init>(Lcom/mediatek/email/outofoffice/AccountSettingsOutOfOfficeFragment;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v0, p0, Lcom/mediatek/email/outofoffice/AccountSettingsOutOfOfficeFragment;->mToTimeButton:Landroid/widget/Button;

    new-instance v1, Lcom/mediatek/email/outofoffice/AccountSettingsOutOfOfficeFragment$6;

    invoke-direct {v1, p0}, Lcom/mediatek/email/outofoffice/AccountSettingsOutOfOfficeFragment$6;-><init>(Lcom/mediatek/email/outofoffice/AccountSettingsOutOfOfficeFragment;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v0, p0, Lcom/mediatek/email/outofoffice/AccountSettingsOutOfOfficeFragment;->mReplyView:Landroid/view/View;

    new-instance v1, Lcom/mediatek/email/outofoffice/AccountSettingsOutOfOfficeFragment$7;

    invoke-direct {v1, p0}, Lcom/mediatek/email/outofoffice/AccountSettingsOutOfOfficeFragment$7;-><init>(Lcom/mediatek/email/outofoffice/AccountSettingsOutOfOfficeFragment;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    const v0, 0x7f0f000b

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/CheckBox;

    iput-object v0, p0, Lcom/mediatek/email/outofoffice/AccountSettingsOutOfOfficeFragment;->mOutOffice:Landroid/widget/CheckBox;

    const v0, 0x7f0f0019

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/CheckBox;

    iput-object v0, p0, Lcom/mediatek/email/outofoffice/AccountSettingsOutOfOfficeFragment;->mSeverCheck:Landroid/widget/CheckBox;

    invoke-virtual {p0, p2}, Lcom/mediatek/email/outofoffice/AccountSettingsOutOfOfficeFragment;->setDisplay(Landroid/os/Bundle;)V

    invoke-virtual {p0}, Lcom/mediatek/email/outofoffice/AccountSettingsOutOfOfficeFragment;->addChangeListener()V

    iget-object v0, p0, Lcom/mediatek/email/outofoffice/AccountSettingsOutOfOfficeFragment;->mSaveButton:Landroid/widget/Button;

    new-instance v1, Lcom/mediatek/email/outofoffice/AccountSettingsOutOfOfficeFragment$8;

    invoke-direct {v1, p0}, Lcom/mediatek/email/outofoffice/AccountSettingsOutOfOfficeFragment$8;-><init>(Lcom/mediatek/email/outofoffice/AccountSettingsOutOfOfficeFragment;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v0, p0, Lcom/mediatek/email/outofoffice/AccountSettingsOutOfOfficeFragment;->mSaveButton:Landroid/widget/Button;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setEnabled(Z)V

    iget-object v0, p0, Lcom/mediatek/email/outofoffice/AccountSettingsOutOfOfficeFragment;->mCancelButton:Landroid/widget/Button;

    new-instance v1, Lcom/mediatek/email/outofoffice/AccountSettingsOutOfOfficeFragment$9;

    invoke-direct {v1, p0}, Lcom/mediatek/email/outofoffice/AccountSettingsOutOfOfficeFragment$9;-><init>(Lcom/mediatek/email/outofoffice/AccountSettingsOutOfOfficeFragment;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    return-void
.end method

.method public onActivityCreated(Landroid/os/Bundle;)V
    .locals 2
    .param p1    # Landroid/os/Bundle;

    invoke-super {p0, p1}, Landroid/app/Fragment;->onActivityCreated(Landroid/os/Bundle;)V

    if-eqz p1, :cond_0

    invoke-virtual {p0}, Landroid/app/Fragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    const-string v1, "AccountSettingsOutOfOfficeFragment.title"

    invoke-virtual {p1, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/app/Activity;->setTitle(Ljava/lang/CharSequence;)V

    const-string v0, "ResumeSave"

    const/4 v1, 0x0

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    iput-boolean v0, p0, Lcom/mediatek/email/outofoffice/AccountSettingsOutOfOfficeFragment;->mNeedToResume:Z

    :cond_0
    invoke-virtual {p0}, Landroid/app/Fragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    const v1, 0x7f08004e

    invoke-virtual {v0, v1}, Landroid/app/Activity;->setTitle(I)V

    return-void
.end method

.method public onAttach(Landroid/app/Activity;)V
    .locals 0
    .param p1    # Landroid/app/Activity;

    invoke-super {p0, p1}, Landroid/app/Fragment;->onAttach(Landroid/app/Activity;)V

    return-void
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 5
    .param p1    # Landroid/os/Bundle;

    sget-boolean v3, Lcom/android/emailcommon/Logging;->DEBUG_LIFECYCLE:Z

    if-eqz v3, :cond_0

    sget-boolean v3, Lcom/android/email/Email;->DEBUG:Z

    if-eqz v3, :cond_0

    const-string v3, "Email"

    const-string v4, "AccountSettingsOutOfOfficeFragment onCreate"

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    invoke-super {p0, p1}, Landroid/app/Fragment;->onCreate(Landroid/os/Bundle;)V

    invoke-virtual {p0}, Landroid/app/Fragment;->getArguments()Landroid/os/Bundle;

    move-result-object v2

    const-string v3, "account_id"

    invoke-virtual {v2, v3}, Landroid/os/Bundle;->getLong(Ljava/lang/String;)J

    move-result-wide v0

    invoke-virtual {p0}, Landroid/app/Fragment;->getActivity()Landroid/app/Activity;

    move-result-object v3

    invoke-static {v3, v0, v1}, Lcom/android/emailcommon/provider/Account;->restoreAccountWithId(Landroid/content/Context;J)Lcom/android/emailcommon/provider/Account;

    move-result-object v3

    iput-object v3, p0, Lcom/mediatek/email/outofoffice/AccountSettingsOutOfOfficeFragment;->mAccount:Lcom/android/emailcommon/provider/Account;

    if-eqz p1, :cond_1

    const-string v3, "Oof_params"

    invoke-virtual {p1, v3}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v3

    check-cast v3, Lcom/android/emailcommon/service/OofParams;

    iput-object v3, p0, Lcom/mediatek/email/outofoffice/AccountSettingsOutOfOfficeFragment;->mOofParams:Lcom/android/emailcommon/service/OofParams;

    const-string v3, "to_year"

    invoke-virtual {p1, v3}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v3

    iput v3, p0, Lcom/mediatek/email/outofoffice/AccountSettingsOutOfOfficeFragment;->mToYear:I

    const-string v3, "to_month"

    invoke-virtual {p1, v3}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v3

    iput v3, p0, Lcom/mediatek/email/outofoffice/AccountSettingsOutOfOfficeFragment;->mToMonth:I

    const-string v3, "to_day"

    invoke-virtual {p1, v3}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v3

    iput v3, p0, Lcom/mediatek/email/outofoffice/AccountSettingsOutOfOfficeFragment;->mToDay:I

    const-string v3, "to_hour"

    invoke-virtual {p1, v3}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v3

    iput v3, p0, Lcom/mediatek/email/outofoffice/AccountSettingsOutOfOfficeFragment;->mToHour:I

    const-string v3, "to_minute"

    invoke-virtual {p1, v3}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v3

    iput v3, p0, Lcom/mediatek/email/outofoffice/AccountSettingsOutOfOfficeFragment;->mToMinute:I

    const-string v3, "to_daytime"

    invoke-virtual {p1, v3}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v3

    iput v3, p0, Lcom/mediatek/email/outofoffice/AccountSettingsOutOfOfficeFragment;->mToDayTime:I

    :goto_0
    invoke-virtual {p0}, Landroid/app/Fragment;->getActivity()Landroid/app/Activity;

    move-result-object v3

    check-cast v3, Lcom/mediatek/email/outofoffice/AccountSettingsOutOfOfficeFragment$Callback;

    iput-object v3, p0, Lcom/mediatek/email/outofoffice/AccountSettingsOutOfOfficeFragment;->mCallback:Lcom/mediatek/email/outofoffice/AccountSettingsOutOfOfficeFragment$Callback;

    return-void

    :cond_1
    sget-object v3, Lcom/android/email/activity/setup/AccountSettingsFragment;->sOofParams:Lcom/android/emailcommon/service/OofParams;

    iput-object v3, p0, Lcom/mediatek/email/outofoffice/AccountSettingsOutOfOfficeFragment;->mOofParams:Lcom/android/emailcommon/service/OofParams;

    goto :goto_0
.end method

.method public onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 4
    .param p1    # Landroid/view/LayoutInflater;
    .param p2    # Landroid/view/ViewGroup;
    .param p3    # Landroid/os/Bundle;

    sget-boolean v2, Lcom/android/emailcommon/Logging;->DEBUG_LIFECYCLE:Z

    if-eqz v2, :cond_0

    sget-boolean v2, Lcom/android/email/Email;->DEBUG:Z

    if-eqz v2, :cond_0

    const-string v2, "Email"

    const-string v3, "AccountSettingsOutOfOfficeFragment onCreateView"

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    const v0, 0x7f040005

    const/4 v2, 0x0

    invoke-virtual {p1, v0, p2, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    invoke-virtual {p0}, Landroid/app/Fragment;->getActivity()Landroid/app/Activity;

    move-result-object v2

    iput-object v2, p0, Lcom/mediatek/email/outofoffice/AccountSettingsOutOfOfficeFragment;->mContext:Landroid/content/Context;

    invoke-virtual {p0, v1, p3}, Lcom/mediatek/email/outofoffice/AccountSettingsOutOfOfficeFragment;->loadSettings(Landroid/view/View;Landroid/os/Bundle;)V

    if-eqz p3, :cond_1

    const v2, 0x7f0f001a

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/Button;

    iput-object v2, p0, Lcom/mediatek/email/outofoffice/AccountSettingsOutOfOfficeFragment;->mSaveButton:Landroid/widget/Button;

    iget-object v2, p0, Lcom/mediatek/email/outofoffice/AccountSettingsOutOfOfficeFragment;->mSaveButton:Landroid/widget/Button;

    const-string v3, "save"

    invoke-virtual {p3, v3}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v3

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setEnabled(Z)V

    :cond_1
    return-object v1
.end method

.method public onDestroy()V
    .locals 0

    invoke-super {p0}, Landroid/app/Fragment;->onDestroy()V

    return-void
.end method

.method public onPause()V
    .locals 2

    const/4 v1, 0x1

    invoke-super {p0}, Landroid/app/Fragment;->onPause()V

    iput-boolean v1, p0, Lcom/mediatek/email/outofoffice/AccountSettingsOutOfOfficeFragment;->mPaused:Z

    iget-object v0, p0, Lcom/mediatek/email/outofoffice/AccountSettingsOutOfOfficeFragment;->mDialog:Lcom/mediatek/email/outofoffice/AccountSettingsOutOfOfficeFragment$WaitingFetchOofDialog;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/mediatek/email/outofoffice/AccountSettingsOutOfOfficeFragment;->mDialog:Lcom/mediatek/email/outofoffice/AccountSettingsOutOfOfficeFragment$WaitingFetchOofDialog;

    invoke-virtual {v0}, Landroid/app/DialogFragment;->dismissAllowingStateLoss()V

    :cond_0
    iget-object v0, p0, Lcom/mediatek/email/outofoffice/AccountSettingsOutOfOfficeFragment;->mOofAsyncTask:Lcom/mediatek/email/outofoffice/AccountSettingsOutOfOfficeFragment$OofAsyncTask;

    if-eqz v0, :cond_1

    iput-boolean v1, p0, Lcom/mediatek/email/outofoffice/AccountSettingsOutOfOfficeFragment;->mNeedToResume:Z

    invoke-direct {p0}, Lcom/mediatek/email/outofoffice/AccountSettingsOutOfOfficeFragment;->onCheckingDialogCancel()V

    :cond_1
    return-void
.end method

.method public onResume()V
    .locals 3

    const/4 v1, 0x0

    invoke-super {p0}, Landroid/app/Fragment;->onResume()V

    iput-boolean v1, p0, Lcom/mediatek/email/outofoffice/AccountSettingsOutOfOfficeFragment;->mPaused:Z

    iget-boolean v0, p0, Lcom/mediatek/email/outofoffice/AccountSettingsOutOfOfficeFragment;->mNeedToResume:Z

    if-eqz v0, :cond_0

    iput-boolean v1, p0, Lcom/mediatek/email/outofoffice/AccountSettingsOutOfOfficeFragment;->mNeedToResume:Z

    new-instance v0, Lcom/mediatek/email/outofoffice/AccountSettingsOutOfOfficeFragment$OofAsyncTask;

    invoke-direct {v0, p0}, Lcom/mediatek/email/outofoffice/AccountSettingsOutOfOfficeFragment$OofAsyncTask;-><init>(Lcom/mediatek/email/outofoffice/AccountSettingsOutOfOfficeFragment;)V

    new-array v1, v1, [Ljava/lang/Void;

    invoke-virtual {v0, v1}, Lcom/android/emailcommon/utility/EmailAsyncTask;->executeParallel([Ljava/lang/Object;)Lcom/android/emailcommon/utility/EmailAsyncTask;

    move-result-object v0

    check-cast v0, Lcom/mediatek/email/outofoffice/AccountSettingsOutOfOfficeFragment$OofAsyncTask;

    iput-object v0, p0, Lcom/mediatek/email/outofoffice/AccountSettingsOutOfOfficeFragment;->mOofAsyncTask:Lcom/mediatek/email/outofoffice/AccountSettingsOutOfOfficeFragment$OofAsyncTask;

    invoke-static {p0}, Lcom/mediatek/email/outofoffice/AccountSettingsOutOfOfficeFragment$WaitingFetchOofDialog;->newInstance(Lcom/mediatek/email/outofoffice/AccountSettingsOutOfOfficeFragment;)Lcom/mediatek/email/outofoffice/AccountSettingsOutOfOfficeFragment$WaitingFetchOofDialog;

    move-result-object v0

    iput-object v0, p0, Lcom/mediatek/email/outofoffice/AccountSettingsOutOfOfficeFragment;->mDialog:Lcom/mediatek/email/outofoffice/AccountSettingsOutOfOfficeFragment$WaitingFetchOofDialog;

    iget-object v0, p0, Lcom/mediatek/email/outofoffice/AccountSettingsOutOfOfficeFragment;->mDialog:Lcom/mediatek/email/outofoffice/AccountSettingsOutOfOfficeFragment$WaitingFetchOofDialog;

    invoke-virtual {p0}, Landroid/app/Fragment;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v1

    const-string v2, "WaitingFetchOofDialog"

    invoke-virtual {v0, v1, v2}, Landroid/app/DialogFragment;->show(Landroid/app/FragmentManager;Ljava/lang/String;)V

    :cond_0
    return-void
.end method

.method public onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 8
    .param p1    # Landroid/os/Bundle;

    const/16 v7, 0xc

    const/4 v2, 0x1

    const/4 v6, 0x0

    const-string v3, "AccountSettingsOutOfOfficeFragment.title"

    invoke-virtual {p0}, Landroid/app/Fragment;->getActivity()Landroid/app/Activity;

    move-result-object v1

    invoke-virtual {v1}, Landroid/app/Activity;->getTitle()Ljava/lang/CharSequence;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    invoke-virtual {p1, v3, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    const-string v1, "save"

    iget-object v3, p0, Lcom/mediatek/email/outofoffice/AccountSettingsOutOfOfficeFragment;->mSaveButton:Landroid/widget/Button;

    invoke-virtual {v3}, Landroid/view/View;->isEnabled()Z

    move-result v3

    invoke-virtual {p1, v1, v3}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    const-string v1, "ResumeSave"

    iget-boolean v3, p0, Lcom/mediatek/email/outofoffice/AccountSettingsOutOfOfficeFragment;->mNeedToResume:Z

    invoke-virtual {p1, v1, v3}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    iget-object v3, p0, Lcom/mediatek/email/outofoffice/AccountSettingsOutOfOfficeFragment;->mOofParams:Lcom/android/emailcommon/service/OofParams;

    iget-object v1, p0, Lcom/mediatek/email/outofoffice/AccountSettingsOutOfOfficeFragment;->mOutOffice:Landroid/widget/CheckBox;

    invoke-virtual {v1}, Landroid/widget/CompoundButton;->isChecked()Z

    move-result v1

    if-eqz v1, :cond_0

    move v1, v2

    :goto_0
    invoke-virtual {v3, v1}, Lcom/android/emailcommon/service/OofParams;->setOofState(I)V

    iget-object v1, p0, Lcom/mediatek/email/outofoffice/AccountSettingsOutOfOfficeFragment;->mOofParams:Lcom/android/emailcommon/service/OofParams;

    iget-object v3, p0, Lcom/mediatek/email/outofoffice/AccountSettingsOutOfOfficeFragment;->mSeverCheck:Landroid/widget/CheckBox;

    invoke-virtual {v3}, Landroid/widget/CompoundButton;->isChecked()Z

    move-result v3

    if-eqz v3, :cond_1

    :goto_1
    invoke-virtual {v1, v2}, Lcom/android/emailcommon/service/OofParams;->setIsExternal(I)V

    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v0

    iget v1, p0, Lcom/mediatek/email/outofoffice/AccountSettingsOutOfOfficeFragment;->mFromYear:I

    iget v2, p0, Lcom/mediatek/email/outofoffice/AccountSettingsOutOfOfficeFragment;->mFromMonth:I

    iget v3, p0, Lcom/mediatek/email/outofoffice/AccountSettingsOutOfOfficeFragment;->mFromDay:I

    iget v4, p0, Lcom/mediatek/email/outofoffice/AccountSettingsOutOfOfficeFragment;->mFromDayTime:I

    if-nez v4, :cond_2

    iget v4, p0, Lcom/mediatek/email/outofoffice/AccountSettingsOutOfOfficeFragment;->mFromHour:I

    :goto_2
    iget v5, p0, Lcom/mediatek/email/outofoffice/AccountSettingsOutOfOfficeFragment;->mFromMinute:I

    invoke-virtual/range {v0 .. v5}, Ljava/util/Calendar;->set(IIIII)V

    iget-object v1, p0, Lcom/mediatek/email/outofoffice/AccountSettingsOutOfOfficeFragment;->mOofParams:Lcom/android/emailcommon/service/OofParams;

    invoke-virtual {v0}, Ljava/util/Calendar;->getTimeInMillis()J

    move-result-wide v2

    invoke-virtual {v1, v2, v3}, Lcom/android/emailcommon/service/OofParams;->setStartTimeInMillis(J)V

    iget v1, p0, Lcom/mediatek/email/outofoffice/AccountSettingsOutOfOfficeFragment;->mToYear:I

    iget v2, p0, Lcom/mediatek/email/outofoffice/AccountSettingsOutOfOfficeFragment;->mToMonth:I

    iget v3, p0, Lcom/mediatek/email/outofoffice/AccountSettingsOutOfOfficeFragment;->mToDay:I

    iget v4, p0, Lcom/mediatek/email/outofoffice/AccountSettingsOutOfOfficeFragment;->mToDayTime:I

    if-nez v4, :cond_4

    iget v4, p0, Lcom/mediatek/email/outofoffice/AccountSettingsOutOfOfficeFragment;->mToHour:I

    :goto_3
    iget v5, p0, Lcom/mediatek/email/outofoffice/AccountSettingsOutOfOfficeFragment;->mToMinute:I

    invoke-virtual/range {v0 .. v5}, Ljava/util/Calendar;->set(IIIII)V

    iget-object v1, p0, Lcom/mediatek/email/outofoffice/AccountSettingsOutOfOfficeFragment;->mOofParams:Lcom/android/emailcommon/service/OofParams;

    invoke-virtual {v0}, Ljava/util/Calendar;->getTimeInMillis()J

    move-result-wide v2

    invoke-virtual {v1, v2, v3}, Lcom/android/emailcommon/service/OofParams;->setEndTimeInMillis(J)V

    iget-object v1, p0, Lcom/mediatek/email/outofoffice/AccountSettingsOutOfOfficeFragment;->mOofParams:Lcom/android/emailcommon/service/OofParams;

    invoke-virtual {p0}, Lcom/mediatek/email/outofoffice/AccountSettingsOutOfOfficeFragment;->onStarted()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/android/emailcommon/service/OofParams;->setReplyMessage(Ljava/lang/String;)V

    const-string v1, "Oof_params"

    iget-object v2, p0, Lcom/mediatek/email/outofoffice/AccountSettingsOutOfOfficeFragment;->mOofParams:Lcom/android/emailcommon/service/OofParams;

    invoke-virtual {p1, v1, v2}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    const-string v1, "to_year"

    iget v2, p0, Lcom/mediatek/email/outofoffice/AccountSettingsOutOfOfficeFragment;->mToYear:I

    invoke-virtual {p1, v1, v2}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    const-string v1, "to_month"

    iget v2, p0, Lcom/mediatek/email/outofoffice/AccountSettingsOutOfOfficeFragment;->mToMonth:I

    invoke-virtual {p1, v1, v2}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    const-string v1, "to_day"

    iget v2, p0, Lcom/mediatek/email/outofoffice/AccountSettingsOutOfOfficeFragment;->mToDay:I

    invoke-virtual {p1, v1, v2}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    const-string v1, "to_hour"

    iget v2, p0, Lcom/mediatek/email/outofoffice/AccountSettingsOutOfOfficeFragment;->mToHour:I

    invoke-virtual {p1, v1, v2}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    const-string v1, "to_minute"

    iget v2, p0, Lcom/mediatek/email/outofoffice/AccountSettingsOutOfOfficeFragment;->mToMinute:I

    invoke-virtual {p1, v1, v2}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    const-string v1, "to_daytime"

    iget v2, p0, Lcom/mediatek/email/outofoffice/AccountSettingsOutOfOfficeFragment;->mToDayTime:I

    invoke-virtual {p1, v1, v2}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    return-void

    :cond_0
    move v1, v6

    goto/16 :goto_0

    :cond_1
    move v2, v6

    goto :goto_1

    :cond_2
    iget v4, p0, Lcom/mediatek/email/outofoffice/AccountSettingsOutOfOfficeFragment;->mFromHour:I

    if-ne v4, v7, :cond_3

    move v4, v6

    :goto_4
    add-int/lit8 v4, v4, 0xc

    goto :goto_2

    :cond_3
    iget v4, p0, Lcom/mediatek/email/outofoffice/AccountSettingsOutOfOfficeFragment;->mFromHour:I

    goto :goto_4

    :cond_4
    iget v4, p0, Lcom/mediatek/email/outofoffice/AccountSettingsOutOfOfficeFragment;->mToHour:I

    if-ne v4, v7, :cond_5

    :goto_5
    add-int/lit8 v4, v6, 0xc

    goto :goto_3

    :cond_5
    iget v6, p0, Lcom/mediatek/email/outofoffice/AccountSettingsOutOfOfficeFragment;->mToHour:I

    goto :goto_5
.end method

.method public onStarted()Ljava/lang/String;
    .locals 4

    invoke-virtual {p0}, Landroid/app/Fragment;->getActivity()Landroid/app/Activity;

    move-result-object v1

    const-string v2, "oof_reply"

    const/4 v3, 0x1

    invoke-virtual {v1, v2, v3}, Landroid/content/ContextWrapper;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "oof_reply"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/mediatek/email/outofoffice/AccountSettingsOutOfOfficeFragment;->mAccount:Lcom/android/emailcommon/provider/Account;

    iget-wide v2, v2, Lcom/android/emailcommon/provider/EmailContent;->mId:J

    invoke-virtual {v1, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    const-string v2, ""

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    return-object v1
.end method

.method public setDisplay(Landroid/os/Bundle;)V
    .locals 9
    .param p1    # Landroid/os/Bundle;

    const/4 v5, 0x1

    const/4 v6, 0x0

    iget-object v7, p0, Lcom/mediatek/email/outofoffice/AccountSettingsOutOfOfficeFragment;->mOofParams:Lcom/android/emailcommon/service/OofParams;

    invoke-virtual {v7}, Lcom/android/emailcommon/service/OofParams;->getOofState()I

    move-result v7

    if-eqz v7, :cond_1

    move v2, v5

    :goto_0
    invoke-virtual {p0, v2, p1}, Lcom/mediatek/email/outofoffice/AccountSettingsOutOfOfficeFragment;->initTimeDisplay(ZLandroid/os/Bundle;)V

    iget-object v7, p0, Lcom/mediatek/email/outofoffice/AccountSettingsOutOfOfficeFragment;->mOutOffice:Landroid/widget/CheckBox;

    invoke-virtual {v7, v2}, Landroid/widget/CompoundButton;->setChecked(Z)V

    if-eqz v2, :cond_3

    iget-object v7, p0, Lcom/mediatek/email/outofoffice/AccountSettingsOutOfOfficeFragment;->mSeverCheck:Landroid/widget/CheckBox;

    iget-object v8, p0, Lcom/mediatek/email/outofoffice/AccountSettingsOutOfOfficeFragment;->mOofParams:Lcom/android/emailcommon/service/OofParams;

    invoke-virtual {v8}, Lcom/android/emailcommon/service/OofParams;->getIsExternal()I

    move-result v8

    if-eqz v8, :cond_2

    :goto_1
    invoke-virtual {v7, v5}, Landroid/widget/CompoundButton;->setChecked(Z)V

    iget-object v5, p0, Lcom/mediatek/email/outofoffice/AccountSettingsOutOfOfficeFragment;->mOofParams:Lcom/android/emailcommon/service/OofParams;

    invoke-virtual {v5}, Lcom/android/emailcommon/service/OofParams;->getReplyMessage()Ljava/lang/String;

    move-result-object v4

    if-eqz v4, :cond_0

    invoke-virtual {v4}, Ljava/lang/String;->isEmpty()Z

    move-result v5

    if-nez v5, :cond_0

    invoke-virtual {p0}, Landroid/app/Fragment;->getActivity()Landroid/app/Activity;

    move-result-object v5

    const-string v6, "oof_reply"

    const/4 v7, 0x2

    invoke-virtual {v5, v6, v7}, Landroid/content/ContextWrapper;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v5

    invoke-interface {v5}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "oof_reply"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget-object v6, p0, Lcom/mediatek/email/outofoffice/AccountSettingsOutOfOfficeFragment;->mAccount:Lcom/android/emailcommon/provider/Account;

    iget-wide v6, v6, Lcom/android/emailcommon/provider/EmailContent;->mId:J

    invoke-virtual {v5, v6, v7}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-interface {v0, v5, v4}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    :cond_0
    :goto_2
    return-void

    :cond_1
    move v2, v6

    goto :goto_0

    :cond_2
    move v5, v6

    goto :goto_1

    :cond_3
    invoke-virtual {p0}, Landroid/app/Fragment;->getActivity()Landroid/app/Activity;

    move-result-object v7

    const-string v8, "oof_reply"

    invoke-virtual {v7, v8, v5}, Landroid/content/ContextWrapper;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v3

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "oof_external"

    invoke-virtual {v5, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget-object v7, p0, Lcom/mediatek/email/outofoffice/AccountSettingsOutOfOfficeFragment;->mAccount:Lcom/android/emailcommon/provider/Account;

    iget-wide v7, v7, Lcom/android/emailcommon/provider/EmailContent;->mId:J

    invoke-virtual {v5, v7, v8}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-interface {v3, v5, v6}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v1

    iget-object v5, p0, Lcom/mediatek/email/outofoffice/AccountSettingsOutOfOfficeFragment;->mSeverCheck:Landroid/widget/CheckBox;

    invoke-virtual {v5, v1}, Landroid/widget/CompoundButton;->setChecked(Z)V

    goto :goto_2
.end method

.method public setTimeByCalendar(Ljava/util/Calendar;Ljava/lang/String;)V
    .locals 8
    .param p1    # Ljava/util/Calendar;
    .param p2    # Ljava/lang/String;

    const/4 v7, 0x5

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/16 v2, 0xc

    const/16 v6, 0xa

    const-string v3, "from"

    invoke-virtual {p2, v3}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_2

    invoke-virtual {p1, v4}, Ljava/util/Calendar;->get(I)I

    move-result v3

    iput v3, p0, Lcom/mediatek/email/outofoffice/AccountSettingsOutOfOfficeFragment;->mFromYear:I

    invoke-virtual {p1, v5}, Ljava/util/Calendar;->get(I)I

    move-result v3

    iput v3, p0, Lcom/mediatek/email/outofoffice/AccountSettingsOutOfOfficeFragment;->mFromMonth:I

    invoke-virtual {p1, v7}, Ljava/util/Calendar;->get(I)I

    move-result v3

    iput v3, p0, Lcom/mediatek/email/outofoffice/AccountSettingsOutOfOfficeFragment;->mFromDay:I

    const/4 v3, 0x7

    invoke-virtual {p1, v3}, Ljava/util/Calendar;->get(I)I

    move-result v3

    iput v3, p0, Lcom/mediatek/email/outofoffice/AccountSettingsOutOfOfficeFragment;->mFromWeekday:I

    invoke-virtual {p1, v6}, Ljava/util/Calendar;->get(I)I

    move-result v3

    iput v3, p0, Lcom/mediatek/email/outofoffice/AccountSettingsOutOfOfficeFragment;->mFromHour:I

    invoke-virtual {p1, v2}, Ljava/util/Calendar;->get(I)I

    move-result v3

    iput v3, p0, Lcom/mediatek/email/outofoffice/AccountSettingsOutOfOfficeFragment;->mFromMinute:I

    const/16 v3, 0x9

    invoke-virtual {p1, v3}, Ljava/util/Calendar;->get(I)I

    move-result v3

    iput v3, p0, Lcom/mediatek/email/outofoffice/AccountSettingsOutOfOfficeFragment;->mFromDayTime:I

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    iget v4, p0, Lcom/mediatek/email/outofoffice/AccountSettingsOutOfOfficeFragment;->mFromWeekday:I

    const/16 v5, 0x14

    invoke-static {v4, v5}, Landroid/text/format/DateUtils;->getDayOfWeekString(II)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ", "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget v4, p0, Lcom/mediatek/email/outofoffice/AccountSettingsOutOfOfficeFragment;->mFromMonth:I

    const/16 v5, 0x1e

    invoke-static {v4, v5}, Landroid/text/format/DateUtils;->getMonthString(II)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget v4, p0, Lcom/mediatek/email/outofoffice/AccountSettingsOutOfOfficeFragment;->mFromDay:I

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ", "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget v4, p0, Lcom/mediatek/email/outofoffice/AccountSettingsOutOfOfficeFragment;->mFromYear:I

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    iget v4, p0, Lcom/mediatek/email/outofoffice/AccountSettingsOutOfOfficeFragment;->mFromHour:I

    if-nez v4, :cond_0

    :goto_0
    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ":"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget v2, p0, Lcom/mediatek/email/outofoffice/AccountSettingsOutOfOfficeFragment;->mFromMinute:I

    if-ge v2, v6, :cond_1

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "0"

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget v4, p0, Lcom/mediatek/email/outofoffice/AccountSettingsOutOfOfficeFragment;->mFromMinute:I

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    :goto_1
    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget v3, p0, Lcom/mediatek/email/outofoffice/AccountSettingsOutOfOfficeFragment;->mFromDayTime:I

    invoke-static {v3}, Landroid/text/format/DateUtils;->getAMPMString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/mediatek/email/outofoffice/AccountSettingsOutOfOfficeFragment;->mFromDateButton:Landroid/widget/Button;

    invoke-virtual {v2, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v2, p0, Lcom/mediatek/email/outofoffice/AccountSettingsOutOfOfficeFragment;->mFromTimeButton:Landroid/widget/Button;

    invoke-virtual {v2, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    :goto_2
    return-void

    :cond_0
    iget v2, p0, Lcom/mediatek/email/outofoffice/AccountSettingsOutOfOfficeFragment;->mFromHour:I

    goto :goto_0

    :cond_1
    iget v2, p0, Lcom/mediatek/email/outofoffice/AccountSettingsOutOfOfficeFragment;->mFromMinute:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    goto :goto_1

    :cond_2
    invoke-virtual {p1, v4}, Ljava/util/Calendar;->get(I)I

    move-result v3

    iput v3, p0, Lcom/mediatek/email/outofoffice/AccountSettingsOutOfOfficeFragment;->mToYear:I

    invoke-virtual {p1, v5}, Ljava/util/Calendar;->get(I)I

    move-result v3

    iput v3, p0, Lcom/mediatek/email/outofoffice/AccountSettingsOutOfOfficeFragment;->mToMonth:I

    invoke-virtual {p1, v7}, Ljava/util/Calendar;->get(I)I

    move-result v3

    iput v3, p0, Lcom/mediatek/email/outofoffice/AccountSettingsOutOfOfficeFragment;->mToDay:I

    const/4 v3, 0x7

    invoke-virtual {p1, v3}, Ljava/util/Calendar;->get(I)I

    move-result v3

    iput v3, p0, Lcom/mediatek/email/outofoffice/AccountSettingsOutOfOfficeFragment;->mToWeekday:I

    invoke-virtual {p1, v6}, Ljava/util/Calendar;->get(I)I

    move-result v3

    iput v3, p0, Lcom/mediatek/email/outofoffice/AccountSettingsOutOfOfficeFragment;->mToHour:I

    invoke-virtual {p1, v2}, Ljava/util/Calendar;->get(I)I

    move-result v3

    iput v3, p0, Lcom/mediatek/email/outofoffice/AccountSettingsOutOfOfficeFragment;->mToMinute:I

    const/16 v3, 0x9

    invoke-virtual {p1, v3}, Ljava/util/Calendar;->get(I)I

    move-result v3

    iput v3, p0, Lcom/mediatek/email/outofoffice/AccountSettingsOutOfOfficeFragment;->mToDayTime:I

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    iget v4, p0, Lcom/mediatek/email/outofoffice/AccountSettingsOutOfOfficeFragment;->mToWeekday:I

    const/16 v5, 0x14

    invoke-static {v4, v5}, Landroid/text/format/DateUtils;->getDayOfWeekString(II)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ", "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget v4, p0, Lcom/mediatek/email/outofoffice/AccountSettingsOutOfOfficeFragment;->mToMonth:I

    const/16 v5, 0x1e

    invoke-static {v4, v5}, Landroid/text/format/DateUtils;->getMonthString(II)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget v4, p0, Lcom/mediatek/email/outofoffice/AccountSettingsOutOfOfficeFragment;->mToDay:I

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ", "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget v4, p0, Lcom/mediatek/email/outofoffice/AccountSettingsOutOfOfficeFragment;->mToYear:I

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    iget v4, p0, Lcom/mediatek/email/outofoffice/AccountSettingsOutOfOfficeFragment;->mToHour:I

    if-nez v4, :cond_3

    :goto_3
    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ":"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget v2, p0, Lcom/mediatek/email/outofoffice/AccountSettingsOutOfOfficeFragment;->mToMinute:I

    if-ge v2, v6, :cond_4

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "0"

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget v4, p0, Lcom/mediatek/email/outofoffice/AccountSettingsOutOfOfficeFragment;->mToMinute:I

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    :goto_4
    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget v3, p0, Lcom/mediatek/email/outofoffice/AccountSettingsOutOfOfficeFragment;->mToDayTime:I

    invoke-static {v3}, Landroid/text/format/DateUtils;->getAMPMString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/mediatek/email/outofoffice/AccountSettingsOutOfOfficeFragment;->mToDateButton:Landroid/widget/Button;

    invoke-virtual {v2, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v2, p0, Lcom/mediatek/email/outofoffice/AccountSettingsOutOfOfficeFragment;->mToTimeButton:Landroid/widget/Button;

    invoke-virtual {v2, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_2

    :cond_3
    iget v2, p0, Lcom/mediatek/email/outofoffice/AccountSettingsOutOfOfficeFragment;->mToHour:I

    goto :goto_3

    :cond_4
    iget v2, p0, Lcom/mediatek/email/outofoffice/AccountSettingsOutOfOfficeFragment;->mToMinute:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    goto :goto_4
.end method

.method protected showDialog(I)Landroid/app/Dialog;
    .locals 6
    .param p1    # I

    const/4 v5, 0x0

    packed-switch p1, :pswitch_data_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :pswitch_0
    new-instance v0, Landroid/app/DatePickerDialog;

    iget-object v1, p0, Lcom/mediatek/email/outofoffice/AccountSettingsOutOfOfficeFragment;->mContext:Landroid/content/Context;

    iget-object v2, p0, Lcom/mediatek/email/outofoffice/AccountSettingsOutOfOfficeFragment;->callBack1:Landroid/app/DatePickerDialog$OnDateSetListener;

    iget v3, p0, Lcom/mediatek/email/outofoffice/AccountSettingsOutOfOfficeFragment;->mFromYear:I

    iget v4, p0, Lcom/mediatek/email/outofoffice/AccountSettingsOutOfOfficeFragment;->mFromMonth:I

    iget v5, p0, Lcom/mediatek/email/outofoffice/AccountSettingsOutOfOfficeFragment;->mFromDay:I

    invoke-direct/range {v0 .. v5}, Landroid/app/DatePickerDialog;-><init>(Landroid/content/Context;Landroid/app/DatePickerDialog$OnDateSetListener;III)V

    goto :goto_0

    :pswitch_1
    new-instance v0, Landroid/app/DatePickerDialog;

    iget-object v1, p0, Lcom/mediatek/email/outofoffice/AccountSettingsOutOfOfficeFragment;->mContext:Landroid/content/Context;

    iget-object v2, p0, Lcom/mediatek/email/outofoffice/AccountSettingsOutOfOfficeFragment;->callBack2:Landroid/app/DatePickerDialog$OnDateSetListener;

    iget v3, p0, Lcom/mediatek/email/outofoffice/AccountSettingsOutOfOfficeFragment;->mToYear:I

    iget v4, p0, Lcom/mediatek/email/outofoffice/AccountSettingsOutOfOfficeFragment;->mToMonth:I

    iget v5, p0, Lcom/mediatek/email/outofoffice/AccountSettingsOutOfOfficeFragment;->mToDay:I

    invoke-direct/range {v0 .. v5}, Landroid/app/DatePickerDialog;-><init>(Landroid/content/Context;Landroid/app/DatePickerDialog$OnDateSetListener;III)V

    goto :goto_0

    :pswitch_2
    new-instance v0, Landroid/app/TimePickerDialog;

    iget-object v1, p0, Lcom/mediatek/email/outofoffice/AccountSettingsOutOfOfficeFragment;->mContext:Landroid/content/Context;

    iget-object v2, p0, Lcom/mediatek/email/outofoffice/AccountSettingsOutOfOfficeFragment;->callBack3:Landroid/app/TimePickerDialog$OnTimeSetListener;

    iget v3, p0, Lcom/mediatek/email/outofoffice/AccountSettingsOutOfOfficeFragment;->mFromDayTime:I

    if-nez v3, :cond_0

    iget v3, p0, Lcom/mediatek/email/outofoffice/AccountSettingsOutOfOfficeFragment;->mFromHour:I

    :goto_1
    iget v4, p0, Lcom/mediatek/email/outofoffice/AccountSettingsOutOfOfficeFragment;->mFromMinute:I

    invoke-direct/range {v0 .. v5}, Landroid/app/TimePickerDialog;-><init>(Landroid/content/Context;Landroid/app/TimePickerDialog$OnTimeSetListener;IIZ)V

    goto :goto_0

    :cond_0
    iget v3, p0, Lcom/mediatek/email/outofoffice/AccountSettingsOutOfOfficeFragment;->mFromHour:I

    add-int/lit8 v3, v3, 0xc

    goto :goto_1

    :pswitch_3
    new-instance v0, Landroid/app/TimePickerDialog;

    iget-object v1, p0, Lcom/mediatek/email/outofoffice/AccountSettingsOutOfOfficeFragment;->mContext:Landroid/content/Context;

    iget-object v2, p0, Lcom/mediatek/email/outofoffice/AccountSettingsOutOfOfficeFragment;->callBack4:Landroid/app/TimePickerDialog$OnTimeSetListener;

    iget v3, p0, Lcom/mediatek/email/outofoffice/AccountSettingsOutOfOfficeFragment;->mToDayTime:I

    if-nez v3, :cond_1

    iget v3, p0, Lcom/mediatek/email/outofoffice/AccountSettingsOutOfOfficeFragment;->mToHour:I

    :goto_2
    iget v4, p0, Lcom/mediatek/email/outofoffice/AccountSettingsOutOfOfficeFragment;->mToMinute:I

    invoke-direct/range {v0 .. v5}, Landroid/app/TimePickerDialog;-><init>(Landroid/content/Context;Landroid/app/TimePickerDialog$OnTimeSetListener;IIZ)V

    goto :goto_0

    :cond_1
    iget v3, p0, Lcom/mediatek/email/outofoffice/AccountSettingsOutOfOfficeFragment;->mToHour:I

    add-int/lit8 v3, v3, 0xc

    goto :goto_2

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method protected timeCheck()Z
    .locals 10

    iget-object v0, p0, Lcom/mediatek/email/outofoffice/AccountSettingsOutOfOfficeFragment;->mOutOffice:Landroid/widget/CheckBox;

    invoke-virtual {v0}, Landroid/widget/CompoundButton;->isChecked()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/mediatek/email/outofoffice/AccountSettingsOutOfOfficeFragment;->getCurrentTime()V

    iget v1, p0, Lcom/mediatek/email/outofoffice/AccountSettingsOutOfOfficeFragment;->mCurrentYear:I

    iget v2, p0, Lcom/mediatek/email/outofoffice/AccountSettingsOutOfOfficeFragment;->mCurrentMonth:I

    iget v3, p0, Lcom/mediatek/email/outofoffice/AccountSettingsOutOfOfficeFragment;->mCurrentDay:I

    iget v4, p0, Lcom/mediatek/email/outofoffice/AccountSettingsOutOfOfficeFragment;->mCurrentHour:I

    iget v5, p0, Lcom/mediatek/email/outofoffice/AccountSettingsOutOfOfficeFragment;->mCurrentMinute:I

    iget v6, p0, Lcom/mediatek/email/outofoffice/AccountSettingsOutOfOfficeFragment;->mCurrentDayTime:I

    move-object v0, p0

    invoke-direct/range {v0 .. v6}, Lcom/mediatek/email/outofoffice/AccountSettingsOutOfOfficeFragment;->getSumMinute(IIIIII)J

    move-result-wide v8

    iget v1, p0, Lcom/mediatek/email/outofoffice/AccountSettingsOutOfOfficeFragment;->mToYear:I

    iget v2, p0, Lcom/mediatek/email/outofoffice/AccountSettingsOutOfOfficeFragment;->mToMonth:I

    iget v3, p0, Lcom/mediatek/email/outofoffice/AccountSettingsOutOfOfficeFragment;->mToDay:I

    iget v4, p0, Lcom/mediatek/email/outofoffice/AccountSettingsOutOfOfficeFragment;->mToHour:I

    iget v5, p0, Lcom/mediatek/email/outofoffice/AccountSettingsOutOfOfficeFragment;->mToMinute:I

    iget v6, p0, Lcom/mediatek/email/outofoffice/AccountSettingsOutOfOfficeFragment;->mToDayTime:I

    move-object v0, p0

    invoke-direct/range {v0 .. v6}, Lcom/mediatek/email/outofoffice/AccountSettingsOutOfOfficeFragment;->getSumMinute(IIIIII)J

    move-result-wide v0

    cmp-long v0, v8, v0

    if-lez v0, :cond_0

    const v0, 0x7f08005c

    const v1, 0x7f08005d

    invoke-static {v0, v1}, Lcom/mediatek/email/outofoffice/AccountSettingsOutOfOfficeFragment$AlertDialogFragment;->newInstance(II)Lcom/mediatek/email/outofoffice/AccountSettingsOutOfOfficeFragment$AlertDialogFragment;

    move-result-object v7

    invoke-virtual {p0}, Landroid/app/Fragment;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v0

    const-string v1, "AlertDialogFragment"

    invoke-virtual {v7, v0, v1}, Landroid/app/DialogFragment;->show(Landroid/app/FragmentManager;Ljava/lang/String;)V

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
