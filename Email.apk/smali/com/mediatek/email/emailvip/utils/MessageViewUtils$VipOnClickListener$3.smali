.class Lcom/mediatek/email/emailvip/utils/MessageViewUtils$VipOnClickListener$3;
.super Ljava/lang/Object;
.source "MessageViewUtils.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/mediatek/email/emailvip/utils/MessageViewUtils$VipOnClickListener;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/mediatek/email/emailvip/utils/MessageViewUtils$VipOnClickListener;


# direct methods
.method constructor <init>(Lcom/mediatek/email/emailvip/utils/MessageViewUtils$VipOnClickListener;)V
    .locals 0

    iput-object p1, p0, Lcom/mediatek/email/emailvip/utils/MessageViewUtils$VipOnClickListener$3;->this$0:Lcom/mediatek/email/emailvip/utils/MessageViewUtils$VipOnClickListener;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 3
    .param p1    # Landroid/view/View;

    iget-object v1, p0, Lcom/mediatek/email/emailvip/utils/MessageViewUtils$VipOnClickListener$3;->this$0:Lcom/mediatek/email/emailvip/utils/MessageViewUtils$VipOnClickListener;

    invoke-virtual {v1}, Lcom/mediatek/email/emailvip/utils/MessageViewUtils$VipOnClickListener;->getTempAddress()Lcom/mediatek/email/emailvip/utils/MessageViewUtils$TempAddress;

    move-result-object v1

    iget-boolean v1, v1, Lcom/mediatek/email/emailvip/utils/MessageViewUtils$TempAddress;->mIsVipMember:Z

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/mediatek/email/emailvip/utils/MessageViewUtils$VipOnClickListener$3;->this$0:Lcom/mediatek/email/emailvip/utils/MessageViewUtils$VipOnClickListener;

    invoke-static {v1}, Lcom/mediatek/email/emailvip/utils/MessageViewUtils$VipOnClickListener;->access$000(Lcom/mediatek/email/emailvip/utils/MessageViewUtils$VipOnClickListener;)Ljava/lang/Runnable;

    move-result-object v1

    invoke-static {v1}, Lcom/android/emailcommon/utility/EmailAsyncTask;->runAsyncParallel(Ljava/lang/Runnable;)Lcom/android/emailcommon/utility/EmailAsyncTask;

    iget-object v1, p0, Lcom/mediatek/email/emailvip/utils/MessageViewUtils$VipOnClickListener$3;->this$0:Lcom/mediatek/email/emailvip/utils/MessageViewUtils$VipOnClickListener;

    invoke-virtual {v1}, Lcom/mediatek/email/emailvip/utils/MessageViewUtils$VipOnClickListener;->getTempAddress()Lcom/mediatek/email/emailvip/utils/MessageViewUtils$TempAddress;

    move-result-object v1

    const/4 v2, 0x0

    iput-boolean v2, v1, Lcom/mediatek/email/emailvip/utils/MessageViewUtils$TempAddress;->mIsVipMember:Z

    :goto_0
    iget-object v1, p0, Lcom/mediatek/email/emailvip/utils/MessageViewUtils$VipOnClickListener$3;->this$0:Lcom/mediatek/email/emailvip/utils/MessageViewUtils$VipOnClickListener;

    invoke-virtual {v1}, Lcom/mediatek/email/emailvip/utils/MessageViewUtils$VipOnClickListener;->getPopupWindow()Landroid/widget/PopupWindow;

    move-result-object v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/mediatek/email/emailvip/utils/MessageViewUtils$VipOnClickListener$3;->this$0:Lcom/mediatek/email/emailvip/utils/MessageViewUtils$VipOnClickListener;

    invoke-virtual {v1}, Lcom/mediatek/email/emailvip/utils/MessageViewUtils$VipOnClickListener;->getPopupWindow()Landroid/widget/PopupWindow;

    move-result-object v1

    invoke-virtual {v1}, Landroid/widget/PopupWindow;->dismiss()V

    :cond_0
    iget-object v1, p0, Lcom/mediatek/email/emailvip/utils/MessageViewUtils$VipOnClickListener$3;->this$0:Lcom/mediatek/email/emailvip/utils/MessageViewUtils$VipOnClickListener;

    iget-object v1, v1, Lcom/mediatek/email/emailvip/utils/MessageViewUtils$VipOnClickListener;->mFragment:Lcom/android/email/activity/MessageViewFragmentBase;

    iget-object v2, p0, Lcom/mediatek/email/emailvip/utils/MessageViewUtils$VipOnClickListener$3;->this$0:Lcom/mediatek/email/emailvip/utils/MessageViewUtils$VipOnClickListener;

    invoke-virtual {v2}, Lcom/mediatek/email/emailvip/utils/MessageViewUtils$VipOnClickListener;->getTempAddress()Lcom/mediatek/email/emailvip/utils/MessageViewUtils$TempAddress;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/android/email/activity/MessageViewFragmentBase;->needUpdateVipIcon(Lcom/android/emailcommon/mail/Address;)Z

    move-result v1

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/mediatek/email/emailvip/utils/MessageViewUtils$VipOnClickListener$3;->this$0:Lcom/mediatek/email/emailvip/utils/MessageViewUtils$VipOnClickListener;

    iget-object v1, v1, Lcom/mediatek/email/emailvip/utils/MessageViewUtils$VipOnClickListener;->mFragment:Lcom/android/email/activity/MessageViewFragmentBase;

    iget-object v2, p0, Lcom/mediatek/email/emailvip/utils/MessageViewUtils$VipOnClickListener$3;->this$0:Lcom/mediatek/email/emailvip/utils/MessageViewUtils$VipOnClickListener;

    invoke-virtual {v2}, Lcom/mediatek/email/emailvip/utils/MessageViewUtils$VipOnClickListener;->getTempAddress()Lcom/mediatek/email/emailvip/utils/MessageViewUtils$TempAddress;

    move-result-object v2

    iget-boolean v2, v2, Lcom/mediatek/email/emailvip/utils/MessageViewUtils$TempAddress;->mIsVipMember:Z

    invoke-virtual {v1, v2}, Lcom/android/email/activity/MessageViewFragmentBase;->updateSenderVipIcon(Z)V

    :cond_1
    iget-object v1, p0, Lcom/mediatek/email/emailvip/utils/MessageViewUtils$VipOnClickListener$3;->this$0:Lcom/mediatek/email/emailvip/utils/MessageViewUtils$VipOnClickListener;

    iget-object v1, v1, Lcom/mediatek/email/emailvip/utils/MessageViewUtils$VipOnClickListener;->mFragment:Lcom/android/email/activity/MessageViewFragmentBase;

    iget-object v2, p0, Lcom/mediatek/email/emailvip/utils/MessageViewUtils$VipOnClickListener$3;->this$0:Lcom/mediatek/email/emailvip/utils/MessageViewUtils$VipOnClickListener;

    invoke-virtual {v2}, Lcom/mediatek/email/emailvip/utils/MessageViewUtils$VipOnClickListener;->getTempAddress()Lcom/mediatek/email/emailvip/utils/MessageViewUtils$TempAddress;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/android/email/activity/MessageViewFragmentBase;->updateVipInformation(Lcom/mediatek/email/emailvip/utils/MessageViewUtils$TempAddress;)V

    return-void

    :cond_2
    invoke-static {}, Lcom/mediatek/email/emailvip/VipMemberCache;->getVipMembersCount()I

    move-result v0

    add-int/lit8 v1, v0, 0x1

    const/16 v2, 0x63

    if-le v1, v2, :cond_3

    iget-object v1, p0, Lcom/mediatek/email/emailvip/utils/MessageViewUtils$VipOnClickListener$3;->this$0:Lcom/mediatek/email/emailvip/utils/MessageViewUtils$VipOnClickListener;

    iget-object v1, v1, Lcom/mediatek/email/emailvip/utils/MessageViewUtils$VipOnClickListener;->mContext:Landroid/content/Context;

    const v2, 0x7f080041

    invoke-static {v1, v2}, Lcom/android/emailcommon/utility/Utility;->showToast(Landroid/content/Context;I)V

    goto :goto_0

    :cond_3
    iget-object v1, p0, Lcom/mediatek/email/emailvip/utils/MessageViewUtils$VipOnClickListener$3;->this$0:Lcom/mediatek/email/emailvip/utils/MessageViewUtils$VipOnClickListener;

    invoke-static {v1}, Lcom/mediatek/email/emailvip/utils/MessageViewUtils$VipOnClickListener;->access$100(Lcom/mediatek/email/emailvip/utils/MessageViewUtils$VipOnClickListener;)Ljava/lang/Runnable;

    move-result-object v1

    invoke-static {v1}, Lcom/android/emailcommon/utility/EmailAsyncTask;->runAsyncParallel(Ljava/lang/Runnable;)Lcom/android/emailcommon/utility/EmailAsyncTask;

    iget-object v1, p0, Lcom/mediatek/email/emailvip/utils/MessageViewUtils$VipOnClickListener$3;->this$0:Lcom/mediatek/email/emailvip/utils/MessageViewUtils$VipOnClickListener;

    invoke-virtual {v1}, Lcom/mediatek/email/emailvip/utils/MessageViewUtils$VipOnClickListener;->getTempAddress()Lcom/mediatek/email/emailvip/utils/MessageViewUtils$TempAddress;

    move-result-object v1

    const/4 v2, 0x1

    iput-boolean v2, v1, Lcom/mediatek/email/emailvip/utils/MessageViewUtils$TempAddress;->mIsVipMember:Z

    goto :goto_0
.end method
