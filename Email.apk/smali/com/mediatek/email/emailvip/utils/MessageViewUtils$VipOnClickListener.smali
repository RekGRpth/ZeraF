.class public Lcom/mediatek/email/emailvip/utils/MessageViewUtils$VipOnClickListener;
.super Ljava/lang/Object;
.source "MessageViewUtils.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/mediatek/email/emailvip/utils/MessageViewUtils;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "VipOnClickListener"
.end annotation


# instance fields
.field private final mAddVipIcon:Landroid/graphics/drawable/Drawable;

.field private mAddVipRunnable:Ljava/lang/Runnable;

.field public mBadge:Landroid/widget/ImageView;

.field private final mBlack:I

.field private mClicked:Z

.field public mContactStatusState:I

.field public mContext:Landroid/content/Context;

.field public mFragment:Lcom/android/email/activity/MessageViewFragmentBase;

.field public mPopup:Landroid/widget/PopupWindow;

.field private final mPopupHeight:I

.field public mQuickContactLookupUri:Landroid/net/Uri;

.field private final mRemoveVipIcon:Landroid/graphics/drawable/Drawable;

.field private mRemoveVipRunnable:Ljava/lang/Runnable;

.field private mSenderClickListener:Landroid/view/View$OnClickListener;

.field public mTempAddress:Lcom/mediatek/email/emailvip/utils/MessageViewUtils$TempAddress;

.field private mVipButtonClickListener:Landroid/view/View$OnClickListener;

.field private final mVipViewPadding:I

.field private final mWhite:I


# direct methods
.method public constructor <init>(Lcom/android/email/activity/MessageViewFragmentBase;Landroid/widget/PopupWindow;)V
    .locals 2
    .param p1    # Lcom/android/email/activity/MessageViewFragmentBase;
    .param p2    # Landroid/widget/PopupWindow;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v1, Lcom/mediatek/email/emailvip/utils/MessageViewUtils$VipOnClickListener$1;

    invoke-direct {v1, p0}, Lcom/mediatek/email/emailvip/utils/MessageViewUtils$VipOnClickListener$1;-><init>(Lcom/mediatek/email/emailvip/utils/MessageViewUtils$VipOnClickListener;)V

    iput-object v1, p0, Lcom/mediatek/email/emailvip/utils/MessageViewUtils$VipOnClickListener;->mAddVipRunnable:Ljava/lang/Runnable;

    new-instance v1, Lcom/mediatek/email/emailvip/utils/MessageViewUtils$VipOnClickListener$2;

    invoke-direct {v1, p0}, Lcom/mediatek/email/emailvip/utils/MessageViewUtils$VipOnClickListener$2;-><init>(Lcom/mediatek/email/emailvip/utils/MessageViewUtils$VipOnClickListener;)V

    iput-object v1, p0, Lcom/mediatek/email/emailvip/utils/MessageViewUtils$VipOnClickListener;->mRemoveVipRunnable:Ljava/lang/Runnable;

    new-instance v1, Lcom/mediatek/email/emailvip/utils/MessageViewUtils$VipOnClickListener$3;

    invoke-direct {v1, p0}, Lcom/mediatek/email/emailvip/utils/MessageViewUtils$VipOnClickListener$3;-><init>(Lcom/mediatek/email/emailvip/utils/MessageViewUtils$VipOnClickListener;)V

    iput-object v1, p0, Lcom/mediatek/email/emailvip/utils/MessageViewUtils$VipOnClickListener;->mVipButtonClickListener:Landroid/view/View$OnClickListener;

    new-instance v1, Lcom/mediatek/email/emailvip/utils/MessageViewUtils$VipOnClickListener$4;

    invoke-direct {v1, p0}, Lcom/mediatek/email/emailvip/utils/MessageViewUtils$VipOnClickListener$4;-><init>(Lcom/mediatek/email/emailvip/utils/MessageViewUtils$VipOnClickListener;)V

    iput-object v1, p0, Lcom/mediatek/email/emailvip/utils/MessageViewUtils$VipOnClickListener;->mSenderClickListener:Landroid/view/View$OnClickListener;

    iput-object p1, p0, Lcom/mediatek/email/emailvip/utils/MessageViewUtils$VipOnClickListener;->mFragment:Lcom/android/email/activity/MessageViewFragmentBase;

    invoke-virtual {p1}, Landroid/app/Fragment;->getActivity()Landroid/app/Activity;

    move-result-object v1

    iput-object v1, p0, Lcom/mediatek/email/emailvip/utils/MessageViewUtils$VipOnClickListener;->mContext:Landroid/content/Context;

    iput-object p2, p0, Lcom/mediatek/email/emailvip/utils/MessageViewUtils$VipOnClickListener;->mPopup:Landroid/widget/PopupWindow;

    iget-object v1, p0, Lcom/mediatek/email/emailvip/utils/MessageViewUtils$VipOnClickListener;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0a0035

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    iput v1, p0, Lcom/mediatek/email/emailvip/utils/MessageViewUtils$VipOnClickListener;->mVipViewPadding:I

    const v1, 0x7f0a0037

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    iput v1, p0, Lcom/mediatek/email/emailvip/utils/MessageViewUtils$VipOnClickListener;->mPopupHeight:I

    const v1, 0x106000b

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    iput v1, p0, Lcom/mediatek/email/emailvip/utils/MessageViewUtils$VipOnClickListener;->mWhite:I

    const v1, 0x106000c

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    iput v1, p0, Lcom/mediatek/email/emailvip/utils/MessageViewUtils$VipOnClickListener;->mBlack:I

    const v1, 0x7f020028

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    iput-object v1, p0, Lcom/mediatek/email/emailvip/utils/MessageViewUtils$VipOnClickListener;->mAddVipIcon:Landroid/graphics/drawable/Drawable;

    const v1, 0x7f02002a

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    iput-object v1, p0, Lcom/mediatek/email/emailvip/utils/MessageViewUtils$VipOnClickListener;->mRemoveVipIcon:Landroid/graphics/drawable/Drawable;

    return-void
.end method

.method static synthetic access$000(Lcom/mediatek/email/emailvip/utils/MessageViewUtils$VipOnClickListener;)Ljava/lang/Runnable;
    .locals 1
    .param p0    # Lcom/mediatek/email/emailvip/utils/MessageViewUtils$VipOnClickListener;

    iget-object v0, p0, Lcom/mediatek/email/emailvip/utils/MessageViewUtils$VipOnClickListener;->mRemoveVipRunnable:Ljava/lang/Runnable;

    return-object v0
.end method

.method static synthetic access$100(Lcom/mediatek/email/emailvip/utils/MessageViewUtils$VipOnClickListener;)Ljava/lang/Runnable;
    .locals 1
    .param p0    # Lcom/mediatek/email/emailvip/utils/MessageViewUtils$VipOnClickListener;

    iget-object v0, p0, Lcom/mediatek/email/emailvip/utils/MessageViewUtils$VipOnClickListener;->mAddVipRunnable:Ljava/lang/Runnable;

    return-object v0
.end method

.method static synthetic access$202(Lcom/mediatek/email/emailvip/utils/MessageViewUtils$VipOnClickListener;Z)Z
    .locals 0
    .param p0    # Lcom/mediatek/email/emailvip/utils/MessageViewUtils$VipOnClickListener;
    .param p1    # Z

    iput-boolean p1, p0, Lcom/mediatek/email/emailvip/utils/MessageViewUtils$VipOnClickListener;->mClicked:Z

    return p1
.end method

.method static synthetic access$300(Lcom/mediatek/email/emailvip/utils/MessageViewUtils$VipOnClickListener;)I
    .locals 1
    .param p0    # Lcom/mediatek/email/emailvip/utils/MessageViewUtils$VipOnClickListener;

    iget v0, p0, Lcom/mediatek/email/emailvip/utils/MessageViewUtils$VipOnClickListener;->mBlack:I

    return v0
.end method

.method static synthetic access$400(Lcom/mediatek/email/emailvip/utils/MessageViewUtils$VipOnClickListener;)I
    .locals 1
    .param p0    # Lcom/mediatek/email/emailvip/utils/MessageViewUtils$VipOnClickListener;

    iget v0, p0, Lcom/mediatek/email/emailvip/utils/MessageViewUtils$VipOnClickListener;->mVipViewPadding:I

    return v0
.end method

.method static synthetic access$500(Lcom/mediatek/email/emailvip/utils/MessageViewUtils$VipOnClickListener;Landroid/content/Context;Lcom/mediatek/email/emailvip/utils/MessageViewUtils$VipTextView;IZI)V
    .locals 0
    .param p0    # Lcom/mediatek/email/emailvip/utils/MessageViewUtils$VipOnClickListener;
    .param p1    # Landroid/content/Context;
    .param p2    # Lcom/mediatek/email/emailvip/utils/MessageViewUtils$VipTextView;
    .param p3    # I
    .param p4    # Z
    .param p5    # I

    invoke-direct/range {p0 .. p5}, Lcom/mediatek/email/emailvip/utils/MessageViewUtils$VipOnClickListener;->setVipViewState(Landroid/content/Context;Lcom/mediatek/email/emailvip/utils/MessageViewUtils$VipTextView;IZI)V

    return-void
.end method

.method private initContactStatusViews()V
    .locals 1

    const/4 v0, 0x0

    iput v0, p0, Lcom/mediatek/email/emailvip/utils/MessageViewUtils$VipOnClickListener;->mContactStatusState:I

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/mediatek/email/emailvip/utils/MessageViewUtils$VipOnClickListener;->mQuickContactLookupUri:Landroid/net/Uri;

    invoke-direct {p0}, Lcom/mediatek/email/emailvip/utils/MessageViewUtils$VipOnClickListener;->showDefaultQuickContactBadgeImage()V

    return-void
.end method

.method private setVipViewState(Landroid/content/Context;Lcom/mediatek/email/emailvip/utils/MessageViewUtils$VipTextView;IZI)V
    .locals 1
    .param p1    # Landroid/content/Context;
    .param p2    # Lcom/mediatek/email/emailvip/utils/MessageViewUtils$VipTextView;
    .param p3    # I
    .param p4    # Z
    .param p5    # I

    invoke-virtual {p2, p3}, Landroid/widget/TextView;->setTextColor(I)V

    invoke-virtual {p2}, Landroid/widget/TextView;->getPaint()Landroid/text/TextPaint;

    move-result-object v0

    invoke-virtual {v0, p4}, Landroid/graphics/Paint;->setFakeBoldText(Z)V

    invoke-virtual {p2}, Lcom/mediatek/email/emailvip/utils/MessageViewUtils$VipTextView;->getAddress()Lcom/mediatek/email/emailvip/utils/MessageViewUtils$TempAddress;

    move-result-object v0

    invoke-static {p1, v0, p4}, Lcom/mediatek/email/emailvip/utils/MessageViewUtils;->getSpannableString(Landroid/content/Context;Lcom/mediatek/email/emailvip/utils/MessageViewUtils$TempAddress;Z)Landroid/text/SpannableString;

    move-result-object v0

    invoke-virtual {p2, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    if-eqz p4, :cond_0

    const v0, 0x7f02000f

    :goto_0
    invoke-virtual {p2, v0}, Landroid/view/View;->setBackgroundResource(I)V

    invoke-virtual {p2, p5}, Lcom/mediatek/email/emailvip/utils/MessageViewUtils$VipTextView;->setPadding(I)V

    return-void

    :cond_0
    const v0, 0x7f02000d

    goto :goto_0
.end method

.method private showDefaultQuickContactBadgeImage()V
    .locals 2

    iget-object v0, p0, Lcom/mediatek/email/emailvip/utils/MessageViewUtils$VipOnClickListener;->mBadge:Landroid/widget/ImageView;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/mediatek/email/emailvip/utils/MessageViewUtils$VipOnClickListener;->mBadge:Landroid/widget/ImageView;

    const v1, 0x7f020025

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    :cond_0
    return-void
.end method


# virtual methods
.method public getPopupWindow()Landroid/widget/PopupWindow;
    .locals 1

    iget-object v0, p0, Lcom/mediatek/email/emailvip/utils/MessageViewUtils$VipOnClickListener;->mPopup:Landroid/widget/PopupWindow;

    return-object v0
.end method

.method public getTempAddress()Lcom/mediatek/email/emailvip/utils/MessageViewUtils$TempAddress;
    .locals 1

    iget-object v0, p0, Lcom/mediatek/email/emailvip/utils/MessageViewUtils$VipOnClickListener;->mTempAddress:Lcom/mediatek/email/emailvip/utils/MessageViewUtils$TempAddress;

    return-object v0
.end method

.method public onClick(Landroid/view/View;)V
    .locals 17
    .param p1    # Landroid/view/View;

    move-object/from16 v0, p0

    iget-boolean v1, v0, Lcom/mediatek/email/emailvip/utils/MessageViewUtils$VipOnClickListener;->mClicked:Z

    if-eqz v1, :cond_0

    :goto_0
    return-void

    :cond_0
    const/4 v1, 0x1

    move-object/from16 v0, p0

    iput-boolean v1, v0, Lcom/mediatek/email/emailvip/utils/MessageViewUtils$VipOnClickListener;->mClicked:Z

    move-object/from16 v3, p1

    check-cast v3, Lcom/mediatek/email/emailvip/utils/MessageViewUtils$VipTextView;

    invoke-virtual {v3}, Lcom/mediatek/email/emailvip/utils/MessageViewUtils$VipTextView;->getAddress()Lcom/mediatek/email/emailvip/utils/MessageViewUtils$TempAddress;

    move-result-object v1

    move-object/from16 v0, p0

    iput-object v1, v0, Lcom/mediatek/email/emailvip/utils/MessageViewUtils$VipOnClickListener;->mTempAddress:Lcom/mediatek/email/emailvip/utils/MessageViewUtils$TempAddress;

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/mediatek/email/emailvip/utils/MessageViewUtils$VipOnClickListener;->mTempAddress:Lcom/mediatek/email/emailvip/utils/MessageViewUtils$TempAddress;

    if-nez v1, :cond_1

    const/4 v1, 0x0

    move-object/from16 v0, p0

    iput-boolean v1, v0, Lcom/mediatek/email/emailvip/utils/MessageViewUtils$VipOnClickListener;->mClicked:Z

    goto :goto_0

    :cond_1
    invoke-virtual {v3}, Landroid/widget/TextView;->getTextColors()Landroid/content/res/ColorStateList;

    move-result-object v11

    invoke-virtual {v11}, Landroid/content/res/ColorStateList;->getDefaultColor()I

    move-result v1

    move-object/from16 v0, p0

    iget v2, v0, Lcom/mediatek/email/emailvip/utils/MessageViewUtils$VipOnClickListener;->mWhite:I

    if-ne v1, v2, :cond_4

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/mediatek/email/emailvip/utils/MessageViewUtils$VipOnClickListener;->mContext:Landroid/content/Context;

    move-object/from16 v0, p0

    iget v4, v0, Lcom/mediatek/email/emailvip/utils/MessageViewUtils$VipOnClickListener;->mBlack:I

    const/4 v5, 0x0

    move-object/from16 v0, p0

    iget v6, v0, Lcom/mediatek/email/emailvip/utils/MessageViewUtils$VipOnClickListener;->mVipViewPadding:I

    move-object/from16 v1, p0

    invoke-direct/range {v1 .. v6}, Lcom/mediatek/email/emailvip/utils/MessageViewUtils$VipOnClickListener;->setVipViewState(Landroid/content/Context;Lcom/mediatek/email/emailvip/utils/MessageViewUtils$VipTextView;IZI)V

    :goto_1
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/mediatek/email/emailvip/utils/MessageViewUtils$VipOnClickListener;->mPopup:Landroid/widget/PopupWindow;

    if-eqz v1, :cond_9

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/mediatek/email/emailvip/utils/MessageViewUtils$VipOnClickListener;->mPopup:Landroid/widget/PopupWindow;

    invoke-virtual {v1}, Landroid/widget/PopupWindow;->getContentView()Landroid/view/View;

    move-result-object v8

    const v1, 0x7f0f00ed

    invoke-virtual {v8, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v16

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/mediatek/email/emailvip/utils/MessageViewUtils$VipOnClickListener;->mSenderClickListener:Landroid/view/View$OnClickListener;

    move-object/from16 v0, v16

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    const v1, 0x7f0f00ec

    invoke-virtual {v8, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ImageView;

    move-object/from16 v0, p0

    iput-object v1, v0, Lcom/mediatek/email/emailvip/utils/MessageViewUtils$VipOnClickListener;->mBadge:Landroid/widget/ImageView;

    const v1, 0x7f0f00ee

    invoke-virtual {v8, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v15

    check-cast v15, Landroid/widget/TextView;

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/mediatek/email/emailvip/utils/MessageViewUtils$VipOnClickListener;->mTempAddress:Lcom/mediatek/email/emailvip/utils/MessageViewUtils$TempAddress;

    invoke-virtual {v1}, Lcom/android/emailcommon/mail/Address;->getPersonal()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v15, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    const v1, 0x7f0f00ef

    invoke-virtual {v8, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v7

    check-cast v7, Lcom/mediatek/email/emailvip/utils/EllipsizeTextView;

    new-instance v1, Lcom/mediatek/email/emailvip/utils/MessageViewUtils$VipOnClickListener$5;

    move-object/from16 v0, p0

    invoke-direct {v1, v0, v7}, Lcom/mediatek/email/emailvip/utils/MessageViewUtils$VipOnClickListener$5;-><init>(Lcom/mediatek/email/emailvip/utils/MessageViewUtils$VipOnClickListener;Lcom/mediatek/email/emailvip/utils/EllipsizeTextView;)V

    invoke-virtual {v7, v1}, Lcom/mediatek/email/emailvip/utils/EllipsizeTextView;->setOnDrawnListener(Lcom/mediatek/email/emailvip/utils/EllipsizeTextView$OnDrawnListener;)V

    const v1, 0x7f0f00f0

    invoke-virtual {v8, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v10

    check-cast v10, Landroid/widget/ImageButton;

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/mediatek/email/emailvip/utils/MessageViewUtils$VipOnClickListener;->mTempAddress:Lcom/mediatek/email/emailvip/utils/MessageViewUtils$TempAddress;

    iget-boolean v1, v1, Lcom/mediatek/email/emailvip/utils/MessageViewUtils$TempAddress;->mIsVipMember:Z

    if-eqz v1, :cond_5

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/mediatek/email/emailvip/utils/MessageViewUtils$VipOnClickListener;->mRemoveVipIcon:Landroid/graphics/drawable/Drawable;

    :goto_2
    invoke-virtual {v10, v1}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/mediatek/email/emailvip/utils/MessageViewUtils$VipOnClickListener;->mVipButtonClickListener:Landroid/view/View$OnClickListener;

    invoke-virtual {v10, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/mediatek/email/emailvip/utils/MessageViewUtils$VipOnClickListener;->mPopup:Landroid/widget/PopupWindow;

    new-instance v2, Lcom/mediatek/email/emailvip/utils/MessageViewUtils$VipOnClickListener$6;

    move-object/from16 v0, p0

    invoke-direct {v2, v0, v3}, Lcom/mediatek/email/emailvip/utils/MessageViewUtils$VipOnClickListener$6;-><init>(Lcom/mediatek/email/emailvip/utils/MessageViewUtils$VipOnClickListener;Lcom/mediatek/email/emailvip/utils/MessageViewUtils$VipTextView;)V

    invoke-virtual {v1, v2}, Landroid/widget/PopupWindow;->setOnDismissListener(Landroid/widget/PopupWindow$OnDismissListener;)V

    const/4 v1, 0x2

    new-array v14, v1, [I

    move-object/from16 v0, p1

    invoke-virtual {v0, v14}, Landroid/view/View;->getLocationOnScreen([I)V

    new-instance v9, Landroid/graphics/Rect;

    invoke-direct {v9}, Landroid/graphics/Rect;-><init>()V

    move-object/from16 v0, p1

    invoke-virtual {v0, v9}, Landroid/view/View;->getWindowVisibleDisplayFrame(Landroid/graphics/Rect;)V

    const/4 v1, 0x1

    aget v1, v14, v1

    iget v2, v9, Landroid/graphics/Rect;->bottom:I

    mul-int/lit8 v2, v2, 0x2

    div-int/lit8 v2, v2, 0x3

    if-gt v1, v2, :cond_7

    invoke-static {}, Lcom/mediatek/email/emailvip/utils/MessageViewUtils;->access$600()Z

    move-result v1

    if-eqz v1, :cond_6

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/mediatek/email/emailvip/utils/MessageViewUtils$VipOnClickListener;->mPopup:Landroid/widget/PopupWindow;

    invoke-static {}, Lcom/mediatek/email/emailvip/utils/MessageViewUtils;->access$700()I

    move-result v2

    const/4 v4, 0x0

    aget v4, v14, v4

    sub-int/2addr v2, v4

    invoke-virtual {v1, v2}, Landroid/widget/PopupWindow;->setWidth(I)V

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/mediatek/email/emailvip/utils/MessageViewUtils$VipOnClickListener;->mPopup:Landroid/widget/PopupWindow;

    const/4 v2, 0x0

    const/4 v4, 0x0

    aget v4, v14, v4

    const/4 v5, 0x1

    aget v5, v14, v5

    invoke-virtual/range {p1 .. p1}, Landroid/view/View;->getHeight()I

    move-result v6

    add-int/2addr v5, v6

    add-int/lit8 v5, v5, 0x2

    move-object/from16 v0, p1

    invoke-virtual {v1, v0, v2, v4, v5}, Landroid/widget/PopupWindow;->showAtLocation(Landroid/view/View;III)V

    :goto_3
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/mediatek/email/emailvip/utils/MessageViewUtils$VipOnClickListener;->mTempAddress:Lcom/mediatek/email/emailvip/utils/MessageViewUtils$TempAddress;

    if-eqz v1, :cond_3

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/mediatek/email/emailvip/utils/MessageViewUtils$VipOnClickListener;->mFragment:Lcom/android/email/activity/MessageViewFragmentBase;

    invoke-virtual {v1}, Landroid/app/Fragment;->getLoaderManager()Landroid/app/LoaderManager;

    move-result-object v13

    const/4 v1, 0x2

    invoke-virtual {v13, v1}, Landroid/app/LoaderManager;->getLoader(I)Landroid/content/Loader;

    move-result-object v12

    if-eqz v12, :cond_2

    invoke-virtual {v12}, Landroid/content/Loader;->cancelLoad()Z

    :cond_2
    const/4 v1, 0x2

    invoke-virtual {v3}, Lcom/mediatek/email/emailvip/utils/MessageViewUtils$VipTextView;->getAddress()Lcom/mediatek/email/emailvip/utils/MessageViewUtils$TempAddress;

    move-result-object v2

    invoke-virtual {v2}, Lcom/android/emailcommon/mail/Address;->getAddress()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/mediatek/email/emailvip/utils/MessageViewUtils$VipViewPhotoLoaderCallbacks;->createArguments(Ljava/lang/String;)Landroid/os/Bundle;

    move-result-object v2

    new-instance v4, Lcom/mediatek/email/emailvip/utils/MessageViewUtils$VipViewPhotoLoaderCallbacks;

    move-object/from16 v0, p0

    invoke-direct {v4, v0}, Lcom/mediatek/email/emailvip/utils/MessageViewUtils$VipViewPhotoLoaderCallbacks;-><init>(Lcom/mediatek/email/emailvip/utils/MessageViewUtils$VipOnClickListener;)V

    invoke-virtual {v13, v1, v2, v4}, Landroid/app/LoaderManager;->restartLoader(ILandroid/os/Bundle;Landroid/app/LoaderManager$LoaderCallbacks;)Landroid/content/Loader;

    :cond_3
    invoke-direct/range {p0 .. p0}, Lcom/mediatek/email/emailvip/utils/MessageViewUtils$VipOnClickListener;->initContactStatusViews()V

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/mediatek/email/emailvip/utils/MessageViewUtils$VipOnClickListener;->mBadge:Landroid/widget/ImageView;

    new-instance v2, Lcom/mediatek/email/emailvip/utils/MessageViewUtils$VipOnClickListener$7;

    move-object/from16 v0, p0

    invoke-direct {v2, v0}, Lcom/mediatek/email/emailvip/utils/MessageViewUtils$VipOnClickListener$7;-><init>(Lcom/mediatek/email/emailvip/utils/MessageViewUtils$VipOnClickListener;)V

    invoke-virtual {v1, v2}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    goto/16 :goto_0

    :cond_4
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/mediatek/email/emailvip/utils/MessageViewUtils$VipOnClickListener;->mContext:Landroid/content/Context;

    move-object/from16 v0, p0

    iget v4, v0, Lcom/mediatek/email/emailvip/utils/MessageViewUtils$VipOnClickListener;->mWhite:I

    const/4 v5, 0x1

    move-object/from16 v0, p0

    iget v6, v0, Lcom/mediatek/email/emailvip/utils/MessageViewUtils$VipOnClickListener;->mVipViewPadding:I

    move-object/from16 v1, p0

    invoke-direct/range {v1 .. v6}, Lcom/mediatek/email/emailvip/utils/MessageViewUtils$VipOnClickListener;->setVipViewState(Landroid/content/Context;Lcom/mediatek/email/emailvip/utils/MessageViewUtils$VipTextView;IZI)V

    goto/16 :goto_1

    :cond_5
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/mediatek/email/emailvip/utils/MessageViewUtils$VipOnClickListener;->mAddVipIcon:Landroid/graphics/drawable/Drawable;

    goto/16 :goto_2

    :cond_6
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/mediatek/email/emailvip/utils/MessageViewUtils$VipOnClickListener;->mPopup:Landroid/widget/PopupWindow;

    const/4 v2, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x1

    aget v5, v14, v5

    invoke-virtual/range {p1 .. p1}, Landroid/view/View;->getHeight()I

    move-result v6

    add-int/2addr v5, v6

    add-int/lit8 v5, v5, 0x2

    move-object/from16 v0, p1

    invoke-virtual {v1, v0, v2, v4, v5}, Landroid/widget/PopupWindow;->showAtLocation(Landroid/view/View;III)V

    goto :goto_3

    :cond_7
    invoke-static {}, Lcom/mediatek/email/emailvip/utils/MessageViewUtils;->access$600()Z

    move-result v1

    if-eqz v1, :cond_8

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/mediatek/email/emailvip/utils/MessageViewUtils$VipOnClickListener;->mPopup:Landroid/widget/PopupWindow;

    invoke-static {}, Lcom/mediatek/email/emailvip/utils/MessageViewUtils;->access$700()I

    move-result v2

    const/4 v4, 0x0

    aget v4, v14, v4

    sub-int/2addr v2, v4

    invoke-virtual {v1, v2}, Landroid/widget/PopupWindow;->setWidth(I)V

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/mediatek/email/emailvip/utils/MessageViewUtils$VipOnClickListener;->mPopup:Landroid/widget/PopupWindow;

    const/4 v2, 0x0

    const/4 v4, 0x0

    aget v4, v14, v4

    const/4 v5, 0x1

    aget v5, v14, v5

    invoke-virtual/range {p1 .. p1}, Landroid/view/View;->getHeight()I

    move-result v6

    add-int/2addr v5, v6

    add-int/lit8 v5, v5, 0x2

    move-object/from16 v0, p1

    invoke-virtual {v1, v0, v2, v4, v5}, Landroid/widget/PopupWindow;->showAtLocation(Landroid/view/View;III)V

    goto/16 :goto_3

    :cond_8
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/mediatek/email/emailvip/utils/MessageViewUtils$VipOnClickListener;->mPopup:Landroid/widget/PopupWindow;

    const/4 v2, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x1

    aget v5, v14, v5

    move-object/from16 v0, p0

    iget v6, v0, Lcom/mediatek/email/emailvip/utils/MessageViewUtils$VipOnClickListener;->mPopupHeight:I

    sub-int/2addr v5, v6

    add-int/lit8 v5, v5, -0x2

    move-object/from16 v0, p1

    invoke-virtual {v1, v0, v2, v4, v5}, Landroid/widget/PopupWindow;->showAtLocation(Landroid/view/View;III)V

    goto/16 :goto_3

    :cond_9
    const/4 v1, 0x0

    move-object/from16 v0, p0

    iput-boolean v1, v0, Lcom/mediatek/email/emailvip/utils/MessageViewUtils$VipOnClickListener;->mClicked:Z

    goto/16 :goto_0
.end method

.method public onClickBadge()V
    .locals 4

    const/4 v1, 0x1

    iget-object v0, p0, Lcom/mediatek/email/emailvip/utils/MessageViewUtils$VipOnClickListener;->mTempAddress:Lcom/mediatek/email/emailvip/utils/MessageViewUtils$TempAddress;

    if-nez v0, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget v0, p0, Lcom/mediatek/email/emailvip/utils/MessageViewUtils$VipOnClickListener;->mContactStatusState:I

    if-nez v0, :cond_2

    iput v1, p0, Lcom/mediatek/email/emailvip/utils/MessageViewUtils$VipOnClickListener;->mContactStatusState:I

    goto :goto_0

    :cond_2
    iget v0, p0, Lcom/mediatek/email/emailvip/utils/MessageViewUtils$VipOnClickListener;->mContactStatusState:I

    if-eq v0, v1, :cond_0

    iget-object v0, p0, Lcom/mediatek/email/emailvip/utils/MessageViewUtils$VipOnClickListener;->mContext:Landroid/content/Context;

    iget-object v1, p0, Lcom/mediatek/email/emailvip/utils/MessageViewUtils$VipOnClickListener;->mBadge:Landroid/widget/ImageView;

    iget-object v2, p0, Lcom/mediatek/email/emailvip/utils/MessageViewUtils$VipOnClickListener;->mQuickContactLookupUri:Landroid/net/Uri;

    iget-object v3, p0, Lcom/mediatek/email/emailvip/utils/MessageViewUtils$VipOnClickListener;->mTempAddress:Lcom/mediatek/email/emailvip/utils/MessageViewUtils$TempAddress;

    invoke-static {v0, v1, v2, v3}, Lcom/android/email/activity/UiUtilities;->showContacts(Landroid/content/Context;Landroid/widget/ImageView;Landroid/net/Uri;Lcom/android/emailcommon/mail/Address;)V

    goto :goto_0
.end method
