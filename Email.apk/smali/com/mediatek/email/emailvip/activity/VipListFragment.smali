.class public Lcom/mediatek/email/emailvip/activity/VipListFragment;
.super Landroid/app/ListFragment;
.source "VipListFragment.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/mediatek/email/emailvip/activity/VipListFragment$RecipientAdapter;,
        Lcom/mediatek/email/emailvip/activity/VipListFragment$VipListLoaderCallbacks;,
        Lcom/mediatek/email/emailvip/activity/VipListFragment$Callback;
    }
.end annotation


# static fields
.field private static final ARG_ACCOUNT_ID:Ljava/lang/String; = "accountId"

.field private static final VIP_LOADER_ID:I = 0x1


# instance fields
.field private mActivity:Landroid/app/Activity;

.field private mAddressAdapter:Lcom/android/ex/chips/AccountSpecifier;

.field private mCallback:Lcom/mediatek/email/emailvip/activity/VipListFragment$Callback;

.field private mImmutableAccountId:Ljava/lang/Long;

.field private mLastItemCount:I

.field private mListAdapter:Lcom/mediatek/email/emailvip/activity/VipListAdapter;

.field private mListView:Landroid/widget/ListView;

.field private mNewVipAddress:Ljava/lang/String;

.field private mRecordEmailAddress:Ljava/lang/String;

.field private mSearchContent:Landroid/view/View;

.field private mSearchVipView:Lcom/mediatek/email/emailvip/activity/VipAddressTextView;

.field private mVipNumber:I


# direct methods
.method public constructor <init>()V
    .locals 1

    const/4 v0, 0x0

    invoke-direct {p0}, Landroid/app/ListFragment;-><init>()V

    iput-object v0, p0, Lcom/mediatek/email/emailvip/activity/VipListFragment;->mNewVipAddress:Ljava/lang/String;

    iput-object v0, p0, Lcom/mediatek/email/emailvip/activity/VipListFragment;->mRecordEmailAddress:Ljava/lang/String;

    return-void
.end method

.method static synthetic access$000(Lcom/mediatek/email/emailvip/activity/VipListFragment;)Lcom/mediatek/email/emailvip/activity/VipListAdapter;
    .locals 1
    .param p0    # Lcom/mediatek/email/emailvip/activity/VipListFragment;

    iget-object v0, p0, Lcom/mediatek/email/emailvip/activity/VipListFragment;->mListAdapter:Lcom/mediatek/email/emailvip/activity/VipListAdapter;

    return-object v0
.end method

.method static synthetic access$100(Lcom/mediatek/email/emailvip/activity/VipListFragment;)Landroid/app/Activity;
    .locals 1
    .param p0    # Lcom/mediatek/email/emailvip/activity/VipListFragment;

    iget-object v0, p0, Lcom/mediatek/email/emailvip/activity/VipListFragment;->mActivity:Landroid/app/Activity;

    return-object v0
.end method

.method static synthetic access$1000(Lcom/mediatek/email/emailvip/activity/VipListFragment;)Lcom/mediatek/email/emailvip/activity/VipListFragment$Callback;
    .locals 1
    .param p0    # Lcom/mediatek/email/emailvip/activity/VipListFragment;

    iget-object v0, p0, Lcom/mediatek/email/emailvip/activity/VipListFragment;->mCallback:Lcom/mediatek/email/emailvip/activity/VipListFragment$Callback;

    return-object v0
.end method

.method static synthetic access$1100(Lcom/mediatek/email/emailvip/activity/VipListFragment;)I
    .locals 1
    .param p0    # Lcom/mediatek/email/emailvip/activity/VipListFragment;

    iget v0, p0, Lcom/mediatek/email/emailvip/activity/VipListFragment;->mVipNumber:I

    return v0
.end method

.method static synthetic access$1102(Lcom/mediatek/email/emailvip/activity/VipListFragment;I)I
    .locals 0
    .param p0    # Lcom/mediatek/email/emailvip/activity/VipListFragment;
    .param p1    # I

    iput p1, p0, Lcom/mediatek/email/emailvip/activity/VipListFragment;->mVipNumber:I

    return p1
.end method

.method static synthetic access$1202(Lcom/mediatek/email/emailvip/activity/VipListFragment;I)I
    .locals 0
    .param p0    # Lcom/mediatek/email/emailvip/activity/VipListFragment;
    .param p1    # I

    iput p1, p0, Lcom/mediatek/email/emailvip/activity/VipListFragment;->mLastItemCount:I

    return p1
.end method

.method static synthetic access$200(Lcom/mediatek/email/emailvip/activity/VipListFragment;)Ljava/lang/String;
    .locals 1
    .param p0    # Lcom/mediatek/email/emailvip/activity/VipListFragment;

    iget-object v0, p0, Lcom/mediatek/email/emailvip/activity/VipListFragment;->mRecordEmailAddress:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$300(Lcom/mediatek/email/emailvip/activity/VipListFragment;Landroid/content/Context;Ljava/lang/String;)V
    .locals 0
    .param p0    # Lcom/mediatek/email/emailvip/activity/VipListFragment;
    .param p1    # Landroid/content/Context;
    .param p2    # Ljava/lang/String;

    invoke-direct {p0, p1, p2}, Lcom/mediatek/email/emailvip/activity/VipListFragment;->updateVipUsername(Landroid/content/Context;Ljava/lang/String;)V

    return-void
.end method

.method static synthetic access$500(Lcom/mediatek/email/emailvip/activity/VipListFragment;[Lcom/android/emailcommon/mail/Address;)V
    .locals 0
    .param p0    # Lcom/mediatek/email/emailvip/activity/VipListFragment;
    .param p1    # [Lcom/android/emailcommon/mail/Address;

    invoke-direct {p0, p1}, Lcom/mediatek/email/emailvip/activity/VipListFragment;->saveAsVips([Lcom/android/emailcommon/mail/Address;)V

    return-void
.end method

.method static synthetic access$600(Lcom/mediatek/email/emailvip/activity/VipListFragment;)Ljava/lang/String;
    .locals 1
    .param p0    # Lcom/mediatek/email/emailvip/activity/VipListFragment;

    iget-object v0, p0, Lcom/mediatek/email/emailvip/activity/VipListFragment;->mNewVipAddress:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$602(Lcom/mediatek/email/emailvip/activity/VipListFragment;Ljava/lang/String;)Ljava/lang/String;
    .locals 0
    .param p0    # Lcom/mediatek/email/emailvip/activity/VipListFragment;
    .param p1    # Ljava/lang/String;

    iput-object p1, p0, Lcom/mediatek/email/emailvip/activity/VipListFragment;->mNewVipAddress:Ljava/lang/String;

    return-object p1
.end method

.method static synthetic access$700(Lcom/mediatek/email/emailvip/activity/VipListFragment;)Landroid/widget/ListView;
    .locals 1
    .param p0    # Lcom/mediatek/email/emailvip/activity/VipListFragment;

    iget-object v0, p0, Lcom/mediatek/email/emailvip/activity/VipListFragment;->mListView:Landroid/widget/ListView;

    return-object v0
.end method

.method static synthetic access$800(Lcom/mediatek/email/emailvip/activity/VipListFragment;)Ljava/lang/Long;
    .locals 1
    .param p0    # Lcom/mediatek/email/emailvip/activity/VipListFragment;

    iget-object v0, p0, Lcom/mediatek/email/emailvip/activity/VipListFragment;->mImmutableAccountId:Ljava/lang/Long;

    return-object v0
.end method

.method static synthetic access$900(Lcom/mediatek/email/emailvip/activity/VipListFragment;)Landroid/view/View;
    .locals 1
    .param p0    # Lcom/mediatek/email/emailvip/activity/VipListFragment;

    iget-object v0, p0, Lcom/mediatek/email/emailvip/activity/VipListFragment;->mSearchContent:Landroid/view/View;

    return-object v0
.end method

.method private getUserNameFromContacts(Ljava/lang/String;)Ljava/lang/String;
    .locals 10
    .param p1    # Ljava/lang/String;

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v0, "data1"

    invoke-virtual {v7, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v0, " = "

    invoke-virtual {v7, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v0, "\'"

    invoke-virtual {v7, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v7, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v0, "\'"

    invoke-virtual {v7, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const/4 v8, 0x0

    const/4 v6, 0x0

    :try_start_0
    invoke-virtual {p0}, Landroid/app/Fragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/ContextWrapper;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    sget-object v1, Landroid/provider/ContactsContract$CommonDataKinds$Email;->CONTENT_URI:Landroid/net/Uri;

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/String;

    const/4 v3, 0x0

    const-string v4, "display_name"

    aput-object v4, v2, v3

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x0

    const/4 v5, 0x0

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v6

    if-nez v6, :cond_1

    if-eqz v6, :cond_0

    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    :cond_0
    move-object v9, v8

    :goto_0
    return-object v9

    :cond_1
    :try_start_1
    invoke-interface {v6}, Landroid/database/Cursor;->moveToNext()Z

    move-result v0

    if-eqz v0, :cond_2

    const/4 v0, 0x0

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result-object v8

    :cond_2
    if-eqz v6, :cond_3

    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    :cond_3
    move-object v9, v8

    goto :goto_0

    :catchall_0
    move-exception v0

    if-eqz v6, :cond_4

    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    :cond_4
    throw v0
.end method

.method public static newInstance(Ljava/lang/Long;)Lcom/mediatek/email/emailvip/activity/VipListFragment;
    .locals 5
    .param p0    # Ljava/lang/Long;

    new-instance v1, Lcom/mediatek/email/emailvip/activity/VipListFragment;

    invoke-direct {v1}, Lcom/mediatek/email/emailvip/activity/VipListFragment;-><init>()V

    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    const-string v2, "accountId"

    invoke-virtual {p0}, Ljava/lang/Long;->longValue()J

    move-result-wide v3

    invoke-virtual {v0, v2, v3, v4}, Landroid/os/Bundle;->putLong(Ljava/lang/String;J)V

    invoke-virtual {v1, v0}, Landroid/app/Fragment;->setArguments(Landroid/os/Bundle;)V

    return-object v1
.end method

.method private saveAsVips([Lcom/android/emailcommon/mail/Address;)V
    .locals 9
    .param p1    # [Lcom/android/emailcommon/mail/Address;

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    move-object v2, p1

    array-length v4, v2

    const/4 v3, 0x0

    :goto_0
    if-ge v3, v4, :cond_0

    aget-object v0, v2, v3

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    :cond_0
    iget-object v5, p0, Lcom/mediatek/email/emailvip/activity/VipListFragment;->mActivity:Landroid/app/Activity;

    iget-object v6, p0, Lcom/mediatek/email/emailvip/activity/VipListFragment;->mImmutableAccountId:Ljava/lang/Long;

    invoke-virtual {v6}, Ljava/lang/Long;->longValue()J

    move-result-wide v6

    new-instance v8, Lcom/mediatek/email/emailvip/activity/VipListFragment$4;

    invoke-direct {v8, p0}, Lcom/mediatek/email/emailvip/activity/VipListFragment$4;-><init>(Lcom/mediatek/email/emailvip/activity/VipListFragment;)V

    invoke-static {v5, v6, v7, v1, v8}, Lcom/android/emailcommon/provider/VipMember;->addVIPs(Landroid/content/Context;JLjava/util/ArrayList;Lcom/android/emailcommon/provider/VipMember$AddVipsCallback;)V

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v5

    if-lez v5, :cond_1

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v5

    add-int/lit8 v5, v5, -0x1

    invoke-virtual {v1, v5}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/android/emailcommon/mail/Address;

    invoke-virtual {v5}, Lcom/android/emailcommon/mail/Address;->getAddress()Ljava/lang/String;

    move-result-object v5

    iput-object v5, p0, Lcom/mediatek/email/emailvip/activity/VipListFragment;->mNewVipAddress:Ljava/lang/String;

    iget-object v5, p0, Lcom/mediatek/email/emailvip/activity/VipListFragment;->mActivity:Landroid/app/Activity;

    new-instance v6, Lcom/mediatek/email/emailvip/activity/VipListFragment$5;

    invoke-direct {v6, p0}, Lcom/mediatek/email/emailvip/activity/VipListFragment$5;-><init>(Lcom/mediatek/email/emailvip/activity/VipListFragment;)V

    invoke-virtual {v5, v6}, Landroid/app/Activity;->runOnUiThread(Ljava/lang/Runnable;)V

    :cond_1
    return-void
.end method

.method private startLoading()V
    .locals 4

    const/4 v3, 0x0

    invoke-virtual {p0}, Landroid/app/Fragment;->getLoaderManager()Landroid/app/LoaderManager;

    move-result-object v0

    iget-object v1, p0, Lcom/mediatek/email/emailvip/activity/VipListFragment;->mCallback:Lcom/mediatek/email/emailvip/activity/VipListFragment$Callback;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/mediatek/email/emailvip/activity/VipListFragment;->mCallback:Lcom/mediatek/email/emailvip/activity/VipListFragment$Callback;

    iget v2, p0, Lcom/mediatek/email/emailvip/activity/VipListFragment;->mVipNumber:I

    invoke-interface {v1, v2}, Lcom/mediatek/email/emailvip/activity/VipListFragment$Callback;->onVipMemberChanged(I)V

    :cond_0
    const/4 v1, 0x1

    new-instance v2, Lcom/mediatek/email/emailvip/activity/VipListFragment$VipListLoaderCallbacks;

    invoke-direct {v2, p0, v3}, Lcom/mediatek/email/emailvip/activity/VipListFragment$VipListLoaderCallbacks;-><init>(Lcom/mediatek/email/emailvip/activity/VipListFragment;Lcom/mediatek/email/emailvip/activity/VipListFragment$1;)V

    invoke-virtual {v0, v1, v3, v2}, Landroid/app/LoaderManager;->initLoader(ILandroid/os/Bundle;Landroid/app/LoaderManager$LoaderCallbacks;)Landroid/content/Loader;

    return-void
.end method

.method private updateVipUsername(Landroid/content/Context;Ljava/lang/String;)V
    .locals 3
    .param p1    # Landroid/content/Context;
    .param p2    # Ljava/lang/String;

    invoke-direct {p0, p2}, Lcom/mediatek/email/emailvip/activity/VipListFragment;->getUserNameFromContacts(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/mediatek/email/emailvip/activity/VipListFragment;->mImmutableAccountId:Ljava/lang/Long;

    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v1

    invoke-static {p1, v1, v2, p2, v0}, Lcom/android/emailcommon/provider/VipMember;->updateVipDisplayName(Landroid/content/Context;JLjava/lang/String;Ljava/lang/String;)V

    const/4 v1, 0x0

    invoke-virtual {p0, v1}, Lcom/mediatek/email/emailvip/activity/VipListFragment;->setEmailAddress(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/mediatek/email/emailvip/activity/VipListFragment;->mListAdapter:Lcom/mediatek/email/emailvip/activity/VipListAdapter;

    invoke-virtual {v1, p2}, Lcom/mediatek/email/emailvip/activity/VipListAdapter;->updateAvatar(Ljava/lang/String;)V

    return-void
.end method


# virtual methods
.method public onActivityCreated(Landroid/os/Bundle;)V
    .locals 2
    .param p1    # Landroid/os/Bundle;

    invoke-super {p0, p1}, Landroid/app/Fragment;->onActivityCreated(Landroid/os/Bundle;)V

    invoke-virtual {p0}, Landroid/app/ListFragment;->getListView()Landroid/widget/ListView;

    move-result-object v0

    iput-object v0, p0, Lcom/mediatek/email/emailvip/activity/VipListFragment;->mListView:Landroid/widget/ListView;

    iget-object v0, p0, Lcom/mediatek/email/emailvip/activity/VipListFragment;->mListView:Landroid/widget/ListView;

    new-instance v1, Lcom/mediatek/email/emailvip/activity/VipListFragment$1;

    invoke-direct {v1, p0}, Lcom/mediatek/email/emailvip/activity/VipListFragment$1;-><init>(Lcom/mediatek/email/emailvip/activity/VipListFragment;)V

    invoke-virtual {v0, v1}, Landroid/widget/AbsListView;->setOnScrollListener(Landroid/widget/AbsListView$OnScrollListener;)V

    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Landroid/app/ListFragment;->setListShown(Z)V

    invoke-direct {p0}, Lcom/mediatek/email/emailvip/activity/VipListFragment;->startLoading()V

    return-void
.end method

.method public onAddVip([Lcom/android/emailcommon/mail/Address;)V
    .locals 1
    .param p1    # [Lcom/android/emailcommon/mail/Address;

    new-instance v0, Lcom/mediatek/email/emailvip/activity/VipListFragment$3;

    invoke-direct {v0, p0, p1}, Lcom/mediatek/email/emailvip/activity/VipListFragment$3;-><init>(Lcom/mediatek/email/emailvip/activity/VipListFragment;[Lcom/android/emailcommon/mail/Address;)V

    invoke-static {v0}, Lcom/android/emailcommon/utility/EmailAsyncTask;->runAsyncParallel(Ljava/lang/Runnable;)Lcom/android/emailcommon/utility/EmailAsyncTask;

    return-void
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 2
    .param p1    # Landroid/os/Bundle;

    invoke-super {p0, p1}, Landroid/app/Fragment;->onCreate(Landroid/os/Bundle;)V

    invoke-virtual {p0}, Landroid/app/Fragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    iput-object v0, p0, Lcom/mediatek/email/emailvip/activity/VipListFragment;->mActivity:Landroid/app/Activity;

    new-instance v0, Lcom/mediatek/email/emailvip/activity/VipListAdapter;

    iget-object v1, p0, Lcom/mediatek/email/emailvip/activity/VipListFragment;->mActivity:Landroid/app/Activity;

    invoke-direct {v0, v1}, Lcom/mediatek/email/emailvip/activity/VipListAdapter;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/mediatek/email/emailvip/activity/VipListFragment;->mListAdapter:Lcom/mediatek/email/emailvip/activity/VipListAdapter;

    iget-object v0, p0, Lcom/mediatek/email/emailvip/activity/VipListFragment;->mListAdapter:Lcom/mediatek/email/emailvip/activity/VipListAdapter;

    invoke-virtual {v0, p0}, Lcom/mediatek/email/emailvip/activity/VipListAdapter;->setFragment(Lcom/mediatek/email/emailvip/activity/VipListFragment;)V

    iget-object v0, p0, Lcom/mediatek/email/emailvip/activity/VipListFragment;->mListAdapter:Lcom/mediatek/email/emailvip/activity/VipListAdapter;

    invoke-virtual {p0, v0}, Landroid/app/ListFragment;->setListAdapter(Landroid/widget/ListAdapter;)V

    invoke-virtual {p0}, Landroid/app/Fragment;->getArguments()Landroid/os/Bundle;

    move-result-object v0

    const-string v1, "accountId"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getLong(Ljava/lang/String;)J

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    iput-object v0, p0, Lcom/mediatek/email/emailvip/activity/VipListFragment;->mImmutableAccountId:Ljava/lang/Long;

    iget-object v0, p0, Lcom/mediatek/email/emailvip/activity/VipListFragment;->mActivity:Landroid/app/Activity;

    check-cast v0, Lcom/mediatek/email/emailvip/activity/VipListFragment$Callback;

    iput-object v0, p0, Lcom/mediatek/email/emailvip/activity/VipListFragment;->mCallback:Lcom/mediatek/email/emailvip/activity/VipListFragment$Callback;

    iget-object v0, p0, Lcom/mediatek/email/emailvip/activity/VipListFragment;->mActivity:Landroid/app/Activity;

    invoke-static {v0}, Lcom/mediatek/email/emailvip/activity/ListPhotoManager;->getInstance(Landroid/content/Context;)Lcom/mediatek/email/emailvip/activity/ListPhotoManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/mediatek/email/emailvip/activity/ListPhotoManager;->refreshCache()V

    return-void
.end method

.method public onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 4
    .param p1    # Landroid/view/LayoutInflater;
    .param p2    # Landroid/view/ViewGroup;
    .param p3    # Landroid/os/Bundle;

    const v1, 0x7f040032

    const/4 v2, 0x0

    invoke-virtual {p1, v1, p2, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    const v1, 0x7f0f0091

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/mediatek/email/emailvip/activity/VipAddressTextView;

    iput-object v1, p0, Lcom/mediatek/email/emailvip/activity/VipListFragment;->mSearchVipView:Lcom/mediatek/email/emailvip/activity/VipAddressTextView;

    iget-object v1, p0, Lcom/mediatek/email/emailvip/activity/VipListFragment;->mSearchVipView:Lcom/mediatek/email/emailvip/activity/VipAddressTextView;

    invoke-virtual {v1, p0}, Lcom/mediatek/email/emailvip/activity/VipAddressTextView;->setTargetFragment(Lcom/mediatek/email/emailvip/activity/VipListFragment;)V

    new-instance v1, Lcom/mediatek/email/emailvip/activity/VipListFragment$RecipientAdapter;

    invoke-virtual {p0}, Landroid/app/Fragment;->getActivity()Landroid/app/Activity;

    move-result-object v2

    iget-object v3, p0, Lcom/mediatek/email/emailvip/activity/VipListFragment;->mSearchVipView:Lcom/mediatek/email/emailvip/activity/VipAddressTextView;

    invoke-direct {v1, p0, v2, v3}, Lcom/mediatek/email/emailvip/activity/VipListFragment$RecipientAdapter;-><init>(Lcom/mediatek/email/emailvip/activity/VipListFragment;Landroid/content/Context;Lcom/mediatek/email/emailvip/activity/VipAddressTextView;)V

    iput-object v1, p0, Lcom/mediatek/email/emailvip/activity/VipListFragment;->mAddressAdapter:Lcom/android/ex/chips/AccountSpecifier;

    iget-object v2, p0, Lcom/mediatek/email/emailvip/activity/VipListFragment;->mSearchVipView:Lcom/mediatek/email/emailvip/activity/VipAddressTextView;

    iget-object v1, p0, Lcom/mediatek/email/emailvip/activity/VipListFragment;->mAddressAdapter:Lcom/android/ex/chips/AccountSpecifier;

    check-cast v1, Lcom/mediatek/email/emailvip/activity/VipListFragment$RecipientAdapter;

    invoke-virtual {v2, v1}, Lcom/android/ex/chips/MTKRecipientEditTextView;->setAdapter(Landroid/widget/ListAdapter;)V

    iget-object v1, p0, Lcom/mediatek/email/emailvip/activity/VipListFragment;->mSearchVipView:Lcom/mediatek/email/emailvip/activity/VipAddressTextView;

    new-instance v2, Landroid/text/util/Rfc822Tokenizer;

    invoke-direct {v2}, Landroid/text/util/Rfc822Tokenizer;-><init>()V

    invoke-virtual {v1, v2}, Lcom/android/ex/chips/MTKRecipientEditTextView;->setTokenizer(Landroid/widget/MultiAutoCompleteTextView$Tokenizer;)V

    const v1, 0x7f0f0078

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    iput-object v1, p0, Lcom/mediatek/email/emailvip/activity/VipListFragment;->mSearchContent:Landroid/view/View;

    iget-object v1, p0, Lcom/mediatek/email/emailvip/activity/VipListFragment;->mSearchContent:Landroid/view/View;

    const/4 v2, 0x4

    invoke-virtual {v1, v2}, Landroid/view/View;->setVisibility(I)V

    return-object v0
.end method

.method public onDestroyView()V
    .locals 1

    invoke-super {p0}, Landroid/app/ListFragment;->onDestroyView()V

    iget-object v0, p0, Lcom/mediatek/email/emailvip/activity/VipListFragment;->mListAdapter:Lcom/mediatek/email/emailvip/activity/VipListAdapter;

    invoke-virtual {v0}, Lcom/mediatek/email/emailvip/activity/VipListAdapter;->stopLoadingAvatars()V

    return-void
.end method

.method public onResume()V
    .locals 1

    invoke-super {p0}, Landroid/app/Fragment;->onResume()V

    iget-object v0, p0, Lcom/mediatek/email/emailvip/activity/VipListFragment;->mRecordEmailAddress:Ljava/lang/String;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/mediatek/email/emailvip/activity/VipListFragment;->mActivity:Landroid/app/Activity;

    invoke-static {v0}, Lcom/mediatek/email/emailvip/activity/ListPhotoManager;->getInstance(Landroid/content/Context;)Lcom/mediatek/email/emailvip/activity/ListPhotoManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/mediatek/email/emailvip/activity/ListPhotoManager;->refreshCache()V

    new-instance v0, Lcom/mediatek/email/emailvip/activity/VipListFragment$2;

    invoke-direct {v0, p0}, Lcom/mediatek/email/emailvip/activity/VipListFragment$2;-><init>(Lcom/mediatek/email/emailvip/activity/VipListFragment;)V

    invoke-static {v0}, Lcom/android/emailcommon/utility/EmailAsyncTask;->runAsyncParallel(Ljava/lang/Runnable;)Lcom/android/emailcommon/utility/EmailAsyncTask;

    :cond_0
    return-void
.end method

.method public setEmailAddress(Ljava/lang/String;)V
    .locals 0
    .param p1    # Ljava/lang/String;

    iput-object p1, p0, Lcom/mediatek/email/emailvip/activity/VipListFragment;->mRecordEmailAddress:Ljava/lang/String;

    return-void
.end method
