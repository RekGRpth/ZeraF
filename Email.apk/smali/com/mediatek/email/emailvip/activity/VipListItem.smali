.class public Lcom/mediatek/email/emailvip/activity/VipListItem;
.super Landroid/widget/LinearLayout;
.source "VipListItem.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/mediatek/email/emailvip/activity/VipListItem$RemoveVipTask;
    }
.end annotation


# instance fields
.field private mActivity:Landroid/app/Activity;

.field private mContactStatusState:I

.field private mQuickContactLookupUri:Landroid/net/Uri;

.field private mRemoveButton:Landroid/widget/ImageView;

.field private final mTaskTracker:Lcom/android/emailcommon/utility/EmailAsyncTask$Tracker;

.field private mVipEmailAddress:Landroid/widget/TextView;

.field private mVipFragment:Lcom/mediatek/email/emailvip/activity/VipListFragment;

.field private mVipId:J

.field private mVipName:Landroid/widget/TextView;

.field private mVipPhoto:Landroid/widget/ImageView;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1    # Landroid/content/Context;

    invoke-direct {p0, p1}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;)V

    new-instance v0, Lcom/android/emailcommon/utility/EmailAsyncTask$Tracker;

    invoke-direct {v0}, Lcom/android/emailcommon/utility/EmailAsyncTask$Tracker;-><init>()V

    iput-object v0, p0, Lcom/mediatek/email/emailvip/activity/VipListItem;->mTaskTracker:Lcom/android/emailcommon/utility/EmailAsyncTask$Tracker;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/util/AttributeSet;

    invoke-direct {p0, p1, p2}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    new-instance v0, Lcom/android/emailcommon/utility/EmailAsyncTask$Tracker;

    invoke-direct {v0}, Lcom/android/emailcommon/utility/EmailAsyncTask$Tracker;-><init>()V

    iput-object v0, p0, Lcom/mediatek/email/emailvip/activity/VipListItem;->mTaskTracker:Lcom/android/emailcommon/utility/EmailAsyncTask$Tracker;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 1
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/util/AttributeSet;
    .param p3    # I

    invoke-direct {p0, p1, p2, p3}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    new-instance v0, Lcom/android/emailcommon/utility/EmailAsyncTask$Tracker;

    invoke-direct {v0}, Lcom/android/emailcommon/utility/EmailAsyncTask$Tracker;-><init>()V

    iput-object v0, p0, Lcom/mediatek/email/emailvip/activity/VipListItem;->mTaskTracker:Lcom/android/emailcommon/utility/EmailAsyncTask$Tracker;

    return-void
.end method

.method static synthetic access$000(Lcom/mediatek/email/emailvip/activity/VipListItem;)Lcom/android/emailcommon/utility/EmailAsyncTask$Tracker;
    .locals 1
    .param p0    # Lcom/mediatek/email/emailvip/activity/VipListItem;

    iget-object v0, p0, Lcom/mediatek/email/emailvip/activity/VipListItem;->mTaskTracker:Lcom/android/emailcommon/utility/EmailAsyncTask$Tracker;

    return-object v0
.end method

.method static synthetic access$100(Lcom/mediatek/email/emailvip/activity/VipListItem;)J
    .locals 2
    .param p0    # Lcom/mediatek/email/emailvip/activity/VipListItem;

    iget-wide v0, p0, Lcom/mediatek/email/emailvip/activity/VipListItem;->mVipId:J

    return-wide v0
.end method

.method private onClickPhoto()V
    .locals 5

    iget-object v2, p0, Lcom/mediatek/email/emailvip/activity/VipListItem;->mVipEmailAddress:Landroid/widget/TextView;

    invoke-virtual {v2}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_0

    :goto_0
    return-void

    :cond_0
    iget-object v2, p0, Lcom/mediatek/email/emailvip/activity/VipListItem;->mVipFragment:Lcom/mediatek/email/emailvip/activity/VipListFragment;

    invoke-virtual {v2, v1}, Lcom/mediatek/email/emailvip/activity/VipListFragment;->setEmailAddress(Ljava/lang/String;)V

    new-instance v0, Lcom/android/emailcommon/mail/Address;

    iget-object v2, p0, Lcom/mediatek/email/emailvip/activity/VipListItem;->mVipName:Landroid/widget/TextView;

    invoke-virtual {v2}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    invoke-direct {v0, v1, v2}, Lcom/android/emailcommon/mail/Address;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v2, p0, Lcom/mediatek/email/emailvip/activity/VipListItem;->mActivity:Landroid/app/Activity;

    iget-object v3, p0, Lcom/mediatek/email/emailvip/activity/VipListItem;->mVipPhoto:Landroid/widget/ImageView;

    iget-object v4, p0, Lcom/mediatek/email/emailvip/activity/VipListItem;->mQuickContactLookupUri:Landroid/net/Uri;

    invoke-static {v2, v3, v4, v0}, Lcom/android/email/activity/UiUtilities;->showContacts(Landroid/content/Context;Landroid/widget/ImageView;Landroid/net/Uri;Lcom/android/emailcommon/mail/Address;)V

    goto :goto_0
.end method

.method private onRemoveVip()V
    .locals 6

    iget-object v4, p0, Lcom/mediatek/email/emailvip/activity/VipListItem;->mActivity:Landroid/app/Activity;

    if-eqz v4, :cond_0

    iget-object v4, p0, Lcom/mediatek/email/emailvip/activity/VipListItem;->mVipName:Landroid/widget/TextView;

    invoke-virtual {v4}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    iget-object v4, p0, Lcom/mediatek/email/emailvip/activity/VipListItem;->mVipEmailAddress:Landroid/widget/TextView;

    invoke-virtual {v4}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "("

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ")"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    iget-object v4, p0, Lcom/mediatek/email/emailvip/activity/VipListItem;->mActivity:Landroid/app/Activity;

    invoke-virtual {v4}, Landroid/app/Activity;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v1

    const-string v4, "VipRemoveDialog"

    invoke-virtual {v1, v4}, Landroid/app/FragmentManager;->findFragmentByTag(Ljava/lang/String;)Landroid/app/Fragment;

    move-result-object v4

    if-nez v4, :cond_0

    new-instance v4, Lcom/mediatek/email/emailvip/activity/VipListItem$RemoveVipTask;

    invoke-virtual {p0}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v5

    invoke-direct {v4, p0, v5}, Lcom/mediatek/email/emailvip/activity/VipListItem$RemoveVipTask;-><init>(Lcom/mediatek/email/emailvip/activity/VipListItem;Landroid/content/Context;)V

    invoke-static {v4, v2}, Lcom/mediatek/email/emailvip/activity/VipRemoveDialog;->newInstance(Lcom/android/emailcommon/utility/EmailAsyncTask;Ljava/lang/String;)Lcom/mediatek/email/emailvip/activity/VipRemoveDialog;

    move-result-object v4

    const-string v5, "VipRemoveDialog"

    invoke-virtual {v4, v1, v5}, Landroid/app/DialogFragment;->show(Landroid/app/FragmentManager;Ljava/lang/String;)V

    :cond_0
    return-void
.end method


# virtual methods
.method public loadContactAvatar(Landroid/widget/ImageView;J)V
    .locals 2
    .param p1    # Landroid/widget/ImageView;
    .param p2    # J

    iget-object v0, p0, Landroid/view/View;->mContext:Landroid/content/Context;

    invoke-static {v0}, Lcom/mediatek/email/emailvip/activity/ListPhotoManager;->getInstance(Landroid/content/Context;)Lcom/mediatek/email/emailvip/activity/ListPhotoManager;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, p1, p2, p3, v1}, Lcom/mediatek/email/emailvip/activity/ListPhotoManager;->loadThumbnail(Landroid/widget/ImageView;JZ)V

    return-void
.end method

.method public onClick(Landroid/view/View;)V
    .locals 1
    .param p1    # Landroid/view/View;

    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    :goto_0
    return-void

    :pswitch_0
    invoke-direct {p0}, Lcom/mediatek/email/emailvip/activity/VipListItem;->onClickPhoto()V

    goto :goto_0

    :pswitch_1
    invoke-direct {p0}, Lcom/mediatek/email/emailvip/activity/VipListItem;->onRemoveVip()V

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x7f0f0093
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method protected onFinishInflate()V
    .locals 2

    const/4 v1, 0x1

    const v0, 0x7f0f0093

    invoke-virtual {p0, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/mediatek/email/emailvip/activity/VipListItem;->mVipPhoto:Landroid/widget/ImageView;

    iget-object v0, p0, Lcom/mediatek/email/emailvip/activity/VipListItem;->mVipPhoto:Landroid/widget/ImageView;

    invoke-virtual {v0, v1}, Landroid/view/View;->setClickable(Z)V

    iget-object v0, p0, Lcom/mediatek/email/emailvip/activity/VipListItem;->mVipPhoto:Landroid/widget/ImageView;

    invoke-virtual {v0, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    const v0, 0x7f0f0094

    invoke-virtual {p0, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/mediatek/email/emailvip/activity/VipListItem;->mRemoveButton:Landroid/widget/ImageView;

    iget-object v0, p0, Lcom/mediatek/email/emailvip/activity/VipListItem;->mRemoveButton:Landroid/widget/ImageView;

    invoke-virtual {v0, v1}, Landroid/view/View;->setClickable(Z)V

    iget-object v0, p0, Lcom/mediatek/email/emailvip/activity/VipListItem;->mRemoveButton:Landroid/widget/ImageView;

    invoke-virtual {v0, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    const v0, 0x7f0f0092

    invoke-virtual {p0, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/mediatek/email/emailvip/activity/VipListItem;->mVipName:Landroid/widget/TextView;

    const v0, 0x7f0f0065

    invoke-virtual {p0, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/mediatek/email/emailvip/activity/VipListItem;->mVipEmailAddress:Landroid/widget/TextView;

    return-void
.end method

.method public resetViews()V
    .locals 3

    const/4 v2, 0x1

    iget-object v0, p0, Lcom/mediatek/email/emailvip/activity/VipListItem;->mVipPhoto:Landroid/widget/ImageView;

    invoke-virtual {v0, v2}, Landroid/view/View;->setClickable(Z)V

    iget-object v0, p0, Landroid/view/View;->mContext:Landroid/content/Context;

    invoke-static {v0}, Lcom/mediatek/email/emailvip/activity/ListPhotoManager;->getInstance(Landroid/content/Context;)Lcom/mediatek/email/emailvip/activity/ListPhotoManager;

    move-result-object v0

    iget-object v1, p0, Lcom/mediatek/email/emailvip/activity/VipListItem;->mVipPhoto:Landroid/widget/ImageView;

    invoke-virtual {v0, v1}, Lcom/mediatek/email/emailvip/activity/ListPhotoManager;->removePhoto(Landroid/widget/ImageView;)V

    iget-object v0, p0, Lcom/mediatek/email/emailvip/activity/VipListItem;->mVipPhoto:Landroid/widget/ImageView;

    const v1, 0x7f020025

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    iget-object v0, p0, Lcom/mediatek/email/emailvip/activity/VipListItem;->mRemoveButton:Landroid/widget/ImageView;

    invoke-virtual {v0, v2}, Landroid/view/View;->setClickable(Z)V

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/mediatek/email/emailvip/activity/VipListItem;->mQuickContactLookupUri:Landroid/net/Uri;

    return-void
.end method

.method public setQuickContactLookupUri(Landroid/net/Uri;)V
    .locals 0
    .param p1    # Landroid/net/Uri;

    iput-object p1, p0, Lcom/mediatek/email/emailvip/activity/VipListItem;->mQuickContactLookupUri:Landroid/net/Uri;

    return-void
.end method

.method public setTargetActivity(Landroid/app/Activity;)V
    .locals 0
    .param p1    # Landroid/app/Activity;

    iput-object p1, p0, Lcom/mediatek/email/emailvip/activity/VipListItem;->mActivity:Landroid/app/Activity;

    return-void
.end method

.method public setVipEmailAddress(Ljava/lang/String;)V
    .locals 1
    .param p1    # Ljava/lang/String;

    iget-object v0, p0, Lcom/mediatek/email/emailvip/activity/VipListItem;->mVipEmailAddress:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    return-void
.end method

.method public setVipFragment(Lcom/mediatek/email/emailvip/activity/VipListFragment;)V
    .locals 0
    .param p1    # Lcom/mediatek/email/emailvip/activity/VipListFragment;

    iput-object p1, p0, Lcom/mediatek/email/emailvip/activity/VipListItem;->mVipFragment:Lcom/mediatek/email/emailvip/activity/VipListFragment;

    return-void
.end method

.method public setVipId(J)V
    .locals 0
    .param p1    # J

    iput-wide p1, p0, Lcom/mediatek/email/emailvip/activity/VipListItem;->mVipId:J

    return-void
.end method

.method public setVipName(Ljava/lang/String;)V
    .locals 1
    .param p1    # Ljava/lang/String;

    iget-object v0, p0, Lcom/mediatek/email/emailvip/activity/VipListItem;->mVipName:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    return-void
.end method
