.class public Lcom/mediatek/email/emailvip/VipMemberCache;
.super Ljava/lang/Object;
.source "VipMemberCache.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/mediatek/email/emailvip/VipMemberCache$1;,
        Lcom/mediatek/email/emailvip/VipMemberCache$VipContentObserver;,
        Lcom/mediatek/email/emailvip/VipMemberCache$UpdateRunnable;
    }
.end annotation


# static fields
.field public static final TAG:Ljava/lang/String; = "VIP_Settings"

.field private static sInstance:Lcom/mediatek/email/emailvip/VipMemberCache;

.field public static sVipAddresses:Ljava/util/HashSet;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashSet",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private mContentObserver:Lcom/mediatek/email/emailvip/VipMemberCache$VipContentObserver;

.field private mContext:Landroid/content/Context;

.field private mUpdateRunnable:Lcom/mediatek/email/emailvip/VipMemberCache$UpdateRunnable;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    sput-object v0, Lcom/mediatek/email/emailvip/VipMemberCache;->sVipAddresses:Ljava/util/HashSet;

    return-void
.end method

.method private constructor <init>(Landroid/content/Context;)V
    .locals 4
    .param p1    # Landroid/content/Context;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const-string v0, "VIP_Settings"

    const-string v1, "VipMemberCache init..."

    invoke-static {v0, v1}, Lcom/android/emailcommon/Logging;->d(Ljava/lang/String;Ljava/lang/String;)V

    iput-object p1, p0, Lcom/mediatek/email/emailvip/VipMemberCache;->mContext:Landroid/content/Context;

    new-instance v0, Lcom/mediatek/email/emailvip/VipMemberCache$UpdateRunnable;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/mediatek/email/emailvip/VipMemberCache$UpdateRunnable;-><init>(Lcom/mediatek/email/emailvip/VipMemberCache;Lcom/mediatek/email/emailvip/VipMemberCache$1;)V

    iput-object v0, p0, Lcom/mediatek/email/emailvip/VipMemberCache;->mUpdateRunnable:Lcom/mediatek/email/emailvip/VipMemberCache$UpdateRunnable;

    new-instance v0, Lcom/mediatek/email/emailvip/VipMemberCache$VipContentObserver;

    new-instance v1, Landroid/os/Handler;

    invoke-direct {v1}, Landroid/os/Handler;-><init>()V

    iget-object v2, p0, Lcom/mediatek/email/emailvip/VipMemberCache;->mContext:Landroid/content/Context;

    iget-object v3, p0, Lcom/mediatek/email/emailvip/VipMemberCache;->mUpdateRunnable:Lcom/mediatek/email/emailvip/VipMemberCache$UpdateRunnable;

    invoke-direct {v0, p0, v1, v2, v3}, Lcom/mediatek/email/emailvip/VipMemberCache$VipContentObserver;-><init>(Lcom/mediatek/email/emailvip/VipMemberCache;Landroid/os/Handler;Landroid/content/Context;Ljava/lang/Runnable;)V

    iput-object v0, p0, Lcom/mediatek/email/emailvip/VipMemberCache;->mContentObserver:Lcom/mediatek/email/emailvip/VipMemberCache$VipContentObserver;

    iget-object v0, p0, Lcom/mediatek/email/emailvip/VipMemberCache;->mContentObserver:Lcom/mediatek/email/emailvip/VipMemberCache$VipContentObserver;

    sget-object v1, Lcom/android/emailcommon/provider/VipMember;->CONTENT_URI:Landroid/net/Uri;

    invoke-virtual {v0, v1}, Lcom/mediatek/email/emailvip/VipMemberCache$VipContentObserver;->register(Landroid/net/Uri;)V

    iget-object v0, p0, Lcom/mediatek/email/emailvip/VipMemberCache;->mUpdateRunnable:Lcom/mediatek/email/emailvip/VipMemberCache$UpdateRunnable;

    invoke-static {v0}, Lcom/android/emailcommon/utility/EmailAsyncTask;->runAsyncParallel(Ljava/lang/Runnable;)Lcom/android/emailcommon/utility/EmailAsyncTask;

    return-void
.end method

.method static synthetic access$100(Lcom/mediatek/email/emailvip/VipMemberCache;)Landroid/content/Context;
    .locals 1
    .param p0    # Lcom/mediatek/email/emailvip/VipMemberCache;

    iget-object v0, p0, Lcom/mediatek/email/emailvip/VipMemberCache;->mContext:Landroid/content/Context;

    return-object v0
.end method

.method public static getVipMembersCount()I
    .locals 2

    sget-object v1, Lcom/mediatek/email/emailvip/VipMemberCache;->sVipAddresses:Ljava/util/HashSet;

    monitor-enter v1

    :try_start_0
    sget-object v0, Lcom/mediatek/email/emailvip/VipMemberCache;->sVipAddresses:Ljava/util/HashSet;

    invoke-virtual {v0}, Ljava/util/HashSet;->size()I

    move-result v0

    monitor-exit v1

    return v0

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public static getVipMessagesCount(Landroid/content/Context;J)I
    .locals 1
    .param p0    # Landroid/content/Context;
    .param p1    # J

    const/4 v0, 0x0

    invoke-static {p0, p1, p2, v0}, Lcom/mediatek/email/emailvip/VipMemberCache;->getVipMessagesCount(Landroid/content/Context;JZ)I

    move-result v0

    return v0
.end method

.method public static getVipMessagesCount(Landroid/content/Context;JZ)I
    .locals 2
    .param p0    # Landroid/content/Context;
    .param p1    # J
    .param p3    # Z

    invoke-static {p0, p1, p2, p3}, Lcom/mediatek/email/emailvip/VipMemberCache;->getVipMessagesIds(Landroid/content/Context;JZ)Landroid/database/Cursor;

    move-result-object v1

    if-nez v1, :cond_0

    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_0
    invoke-interface {v1}, Landroid/database/Cursor;->getCount()I

    move-result v0

    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    goto :goto_0
.end method

.method public static getVipMessagesIds(Landroid/content/Context;J)Landroid/database/Cursor;
    .locals 1
    .param p0    # Landroid/content/Context;
    .param p1    # J

    const/4 v0, 0x0

    invoke-static {p0, p1, p2, v0}, Lcom/mediatek/email/emailvip/VipMemberCache;->getVipMessagesIds(Landroid/content/Context;JZ)Landroid/database/Cursor;

    move-result-object v0

    return-object v0
.end method

.method public static getVipMessagesIds(Landroid/content/Context;JZ)Landroid/database/Cursor;
    .locals 12
    .param p0    # Landroid/content/Context;
    .param p1    # J
    .param p3    # Z

    const/4 v11, 0x1

    const/4 v10, 0x0

    invoke-static {}, Lcom/mediatek/email/emailvip/VipMemberCache;->hasVipMembers()Z

    move-result v0

    if-nez v0, :cond_1

    new-instance v8, Landroid/database/MatrixCursor;

    sget-object v0, Lcom/android/emailcommon/provider/EmailContent;->ID_PROJECTION:[Ljava/lang/String;

    invoke-direct {v8, v0}, Landroid/database/MatrixCursor;-><init>([Ljava/lang/String;)V

    :cond_0
    :goto_0
    return-object v8

    :cond_1
    const-string v3, "mailboxKey NOT IN (SELECT _id FROM Mailbox WHERE type = 6 OR type = 8) AND flagLoaded IN (2,1,4)"

    if-eqz p3, :cond_2

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "flagRead=0 AND "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    :cond_2
    const-wide/16 v0, 0x0

    cmp-long v0, p1, v0

    if-lez v0, :cond_3

    const-wide/high16 v0, 0x1000000000000000L

    cmp-long v0, p1, v0

    if-eqz v0, :cond_3

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "accountKey = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1, p2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " AND "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    :cond_3
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    sget-object v1, Lcom/android/emailcommon/provider/EmailContent$Message;->CONTENT_URI:Landroid/net/Uri;

    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/String;

    const-string v4, "_id"

    aput-object v4, v2, v10

    const-string v4, "fromList"

    aput-object v4, v2, v11

    const/4 v4, 0x0

    const-string v5, "timeStamp DESC"

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v6

    if-nez v6, :cond_4

    const-string v0, "VIP_Settings"

    const-string v1, "getVipMessagesIds return empty cursor because cursor is null"

    invoke-static {v0, v1}, Lcom/android/emailcommon/Logging;->e(Ljava/lang/String;Ljava/lang/String;)V

    new-instance v8, Landroid/database/MatrixCursor;

    sget-object v0, Lcom/android/emailcommon/provider/EmailContent;->ID_PROJECTION:[Ljava/lang/String;

    invoke-direct {v8, v0}, Landroid/database/MatrixCursor;-><init>([Ljava/lang/String;)V

    goto :goto_0

    :cond_4
    new-instance v8, Lcom/android/email/data/ClosingMatrixCursor;

    sget-object v0, Lcom/android/emailcommon/provider/EmailContent;->ID_PROJECTION:[Ljava/lang/String;

    invoke-direct {v8, v0, v6}, Lcom/android/email/data/ClosingMatrixCursor;-><init>([Ljava/lang/String;Landroid/database/Cursor;)V

    :cond_5
    :goto_1
    invoke-interface {v6}, Landroid/database/Cursor;->moveToNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v6, v11}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v7

    invoke-static {v7}, Lcom/mediatek/email/emailvip/VipMemberCache;->isVIP(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_5

    invoke-virtual {v8}, Landroid/database/MatrixCursor;->newRow()Landroid/database/MatrixCursor$RowBuilder;

    move-result-object v9

    invoke-interface {v6, v10}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    invoke-virtual {v9, v0}, Landroid/database/MatrixCursor$RowBuilder;->add(Ljava/lang/Object;)Landroid/database/MatrixCursor$RowBuilder;

    goto :goto_1
.end method

.method public static hasVipMembers()Z
    .locals 2

    sget-object v1, Lcom/mediatek/email/emailvip/VipMemberCache;->sVipAddresses:Ljava/util/HashSet;

    monitor-enter v1

    :try_start_0
    sget-object v0, Lcom/mediatek/email/emailvip/VipMemberCache;->sVipAddresses:Ljava/util/HashSet;

    invoke-virtual {v0}, Ljava/util/HashSet;->size()I

    move-result v0

    if-lez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    monitor-exit v1

    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public static init(Landroid/content/Context;)V
    .locals 1
    .param p0    # Landroid/content/Context;

    sget-object v0, Lcom/mediatek/email/emailvip/VipMemberCache;->sInstance:Lcom/mediatek/email/emailvip/VipMemberCache;

    if-nez v0, :cond_0

    new-instance v0, Lcom/mediatek/email/emailvip/VipMemberCache;

    invoke-direct {v0, p0}, Lcom/mediatek/email/emailvip/VipMemberCache;-><init>(Landroid/content/Context;)V

    sput-object v0, Lcom/mediatek/email/emailvip/VipMemberCache;->sInstance:Lcom/mediatek/email/emailvip/VipMemberCache;

    :cond_0
    return-void
.end method

.method public static isVIP(Ljava/lang/String;)Z
    .locals 3
    .param p0    # Ljava/lang/String;

    invoke-static {p0}, Lcom/android/emailcommon/mail/Address;->getFirstMailAddress(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v1, 0x0

    :goto_0
    return v1

    :cond_0
    sget-object v1, Lcom/mediatek/email/emailvip/VipMemberCache;->sVipAddresses:Ljava/util/HashSet;

    invoke-virtual {v0}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/util/HashSet;->contains(Ljava/lang/Object;)Z

    move-result v1

    goto :goto_0
.end method

.method public static updateVipMemberCache()V
    .locals 1

    sget-object v0, Lcom/mediatek/email/emailvip/VipMemberCache;->sInstance:Lcom/mediatek/email/emailvip/VipMemberCache;

    if-eqz v0, :cond_0

    sget-object v0, Lcom/mediatek/email/emailvip/VipMemberCache;->sInstance:Lcom/mediatek/email/emailvip/VipMemberCache;

    iget-object v0, v0, Lcom/mediatek/email/emailvip/VipMemberCache;->mUpdateRunnable:Lcom/mediatek/email/emailvip/VipMemberCache$UpdateRunnable;

    invoke-virtual {v0}, Lcom/mediatek/email/emailvip/VipMemberCache$UpdateRunnable;->run()V

    :cond_0
    return-void
.end method
