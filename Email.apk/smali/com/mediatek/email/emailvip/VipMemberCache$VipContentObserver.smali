.class Lcom/mediatek/email/emailvip/VipMemberCache$VipContentObserver;
.super Landroid/database/ContentObserver;
.source "VipMemberCache.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/mediatek/email/emailvip/VipMemberCache;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "VipContentObserver"
.end annotation


# instance fields
.field private mInnerContext:Landroid/content/Context;

.field private mInnerRunnable:Ljava/lang/Runnable;

.field private mRegistered:Z

.field private final mThrottle:Lcom/android/email/Throttle;

.field final synthetic this$0:Lcom/mediatek/email/emailvip/VipMemberCache;


# direct methods
.method public constructor <init>(Lcom/mediatek/email/emailvip/VipMemberCache;Landroid/os/Handler;Landroid/content/Context;Ljava/lang/Runnable;)V
    .locals 2
    .param p2    # Landroid/os/Handler;
    .param p3    # Landroid/content/Context;
    .param p4    # Ljava/lang/Runnable;

    iput-object p1, p0, Lcom/mediatek/email/emailvip/VipMemberCache$VipContentObserver;->this$0:Lcom/mediatek/email/emailvip/VipMemberCache;

    invoke-direct {p0, p2}, Landroid/database/ContentObserver;-><init>(Landroid/os/Handler;)V

    iput-object p3, p0, Lcom/mediatek/email/emailvip/VipMemberCache$VipContentObserver;->mInnerContext:Landroid/content/Context;

    new-instance v0, Lcom/android/email/Throttle;

    const-string v1, "VipContentObserver"

    invoke-direct {v0, v1, p0, p2}, Lcom/android/email/Throttle;-><init>(Ljava/lang/String;Ljava/lang/Runnable;Landroid/os/Handler;)V

    iput-object v0, p0, Lcom/mediatek/email/emailvip/VipMemberCache$VipContentObserver;->mThrottle:Lcom/android/email/Throttle;

    iput-object p4, p0, Lcom/mediatek/email/emailvip/VipMemberCache$VipContentObserver;->mInnerRunnable:Ljava/lang/Runnable;

    return-void
.end method


# virtual methods
.method public onChange(Z)V
    .locals 1
    .param p1    # Z

    iget-boolean v0, p0, Lcom/mediatek/email/emailvip/VipMemberCache$VipContentObserver;->mRegistered:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/mediatek/email/emailvip/VipMemberCache$VipContentObserver;->mThrottle:Lcom/android/email/Throttle;

    invoke-virtual {v0}, Lcom/android/email/Throttle;->onEvent()V

    :cond_0
    return-void
.end method

.method public register(Landroid/net/Uri;)V
    .locals 2
    .param p1    # Landroid/net/Uri;

    const/4 v1, 0x1

    invoke-virtual {p0}, Lcom/mediatek/email/emailvip/VipMemberCache$VipContentObserver;->unregister()V

    iget-object v0, p0, Lcom/mediatek/email/emailvip/VipMemberCache$VipContentObserver;->mInnerContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    invoke-virtual {v0, p1, v1, p0}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;)V

    iput-boolean v1, p0, Lcom/mediatek/email/emailvip/VipMemberCache$VipContentObserver;->mRegistered:Z

    const-string v0, "VIP_Settings"

    const-string v1, "VipContentObserver register"

    invoke-static {v0, v1}, Lcom/android/emailcommon/Logging;->d(Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method public run()V
    .locals 1

    iget-object v0, p0, Lcom/mediatek/email/emailvip/VipMemberCache$VipContentObserver;->mInnerRunnable:Ljava/lang/Runnable;

    invoke-static {v0}, Lcom/android/emailcommon/utility/EmailAsyncTask;->runAsyncParallel(Ljava/lang/Runnable;)Lcom/android/emailcommon/utility/EmailAsyncTask;

    return-void
.end method

.method public unregister()V
    .locals 1

    iget-boolean v0, p0, Lcom/mediatek/email/emailvip/VipMemberCache$VipContentObserver;->mRegistered:Z

    if-nez v0, :cond_0

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/mediatek/email/emailvip/VipMemberCache$VipContentObserver;->mThrottle:Lcom/android/email/Throttle;

    invoke-virtual {v0}, Lcom/android/email/Throttle;->cancelScheduledCallback()V

    iget-object v0, p0, Lcom/mediatek/email/emailvip/VipMemberCache$VipContentObserver;->mInnerContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    invoke-virtual {v0, p0}, Landroid/content/ContentResolver;->unregisterContentObserver(Landroid/database/ContentObserver;)V

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/mediatek/email/emailvip/VipMemberCache$VipContentObserver;->mRegistered:Z

    goto :goto_0
.end method
