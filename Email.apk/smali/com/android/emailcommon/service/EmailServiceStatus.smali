.class public interface abstract Lcom/android/emailcommon/service/EmailServiceStatus;
.super Ljava/lang/Object;
.source "EmailServiceStatus.java"


# static fields
.field public static final ACCESS_DENIED:I = 0x19

.field public static final ACCOUNT_UNINITIALIZED:I = 0x18

.field public static final ATTACHMENT_NOT_FOUND:I = 0x11

.field public static final CLIENT_CERTIFICATE_ERROR:I = 0x21

.field public static final CONNECTION_ERROR:I = 0x20

.field public static final FOLDER_NOT_CREATED:I = 0x14

.field public static final FOLDER_NOT_DELETED:I = 0x12

.field public static final FOLDER_NOT_RENAMED:I = 0x13

.field public static final IN_PROGRESS:I = 0x1

.field public static final LOGIN_FAILED:I = 0x16

.field public static final MESSAGE_NOT_FOUND:I = 0x10

.field public static final REMOTE_EXCEPTION:I = 0x15

.field public static final SECURITY_FAILURE:I = 0x17

.field public static final SUCCESS:I
