.class public final Lcom/android/ex/chips/R$color;
.super Ljava/lang/Object;
.source "R.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/ex/chips/R;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "color"
.end annotation


# static fields
.field public static final account_folder_list_item_separator:I = 0x7f090006

.field public static final account_setup_divider_color:I = 0x7f090003

.field public static final account_setup_headline_color:I = 0x7f090000

.field public static final account_setup_info_text_color:I = 0x7f090001

.field public static final account_setup_label_text_color:I = 0x7f090002

.field public static final actionbar_secondary:I = 0x7f090032

.field public static final button_text_color:I = 0x7f09000c

.field public static final button_text_disabled_color:I = 0x7f09000d

.field public static final buttontext:I = 0x7f090033

.field public static final combined_view_account_color_1:I = 0x7f090011

.field public static final combined_view_account_color_2:I = 0x7f090012

.field public static final combined_view_account_color_3:I = 0x7f090013

.field public static final combined_view_account_color_4:I = 0x7f090014

.field public static final combined_view_account_color_5:I = 0x7f090015

.field public static final combined_view_account_color_6:I = 0x7f090016

.field public static final combined_view_account_color_7:I = 0x7f090017

.field public static final combined_view_account_color_8:I = 0x7f090018

.field public static final combined_view_account_color_9:I = 0x7f090019

.field public static final compose_label_text:I = 0x7f090025

.field public static final connection_error_banner:I = 0x7f090007

.field public static final date_text_color_read:I = 0x7f090030

.field public static final date_text_color_unread:I = 0x7f09002f

.field public static final default_text_color:I = 0x7f090028

.field public static final divider_color:I = 0x7f090010

.field public static final error_bar_background:I = 0x7f090024

.field public static final label_list_heading_text_color:I = 0x7f090031

.field public static final mailbox_drop_available_bg_color:I = 0x7f09001b

.field public static final mailbox_drop_destructive_bg_color:I = 0x7f09001c

.field public static final mailbox_drop_unavailable_fg_color:I = 0x7f09001a

.field public static final message_activated_color:I = 0x7f090023

.field public static final message_list_item_background_read:I = 0x7f090005

.field public static final message_list_item_background_unread:I = 0x7f090004

.field public static final message_view_fogged_glass_color:I = 0x7f090027

.field public static final message_view_header_color:I = 0x7f090022

.field public static final message_view_info_back_color:I = 0x7f090021

.field public static final senders_text_color_read:I = 0x7f09002e

.field public static final senders_text_color_unread:I = 0x7f09002b

.field public static final snippet_text_color_read:I = 0x7f09002d

.field public static final snippet_text_color_unread:I = 0x7f09002a

.field public static final subject_text_color_read:I = 0x7f09002c

.field public static final subject_text_color_unread:I = 0x7f090029

.field public static final text_color_primary_invertible:I = 0x7f090034

.field public static final text_disabled_color:I = 0x7f09000b

.field public static final text_primary_color:I = 0x7f090008

.field public static final text_primary_color_inverse:I = 0x7f09000e

.field public static final text_secondary_color:I = 0x7f090009

.field public static final text_secondary_color_inverse:I = 0x7f09000f

.field public static final text_ternary_color:I = 0x7f09000a

.field public static final vip_recipient_area:I = 0x7f090026

.field public static final widget_default_text_color:I = 0x7f09001d

.field public static final widget_light_text_color:I = 0x7f09001e

.field public static final widget_title_color:I = 0x7f09001f

.field public static final widget_unread_color:I = 0x7f090020


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
