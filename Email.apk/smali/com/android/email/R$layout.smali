.class public final Lcom/android/email/R$layout;
.super Ljava/lang/Object;
.source "R.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/email/R;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "layout"
.end annotation


# static fields
.field public static final account_settings:I = 0x7f040000

.field public static final account_settings_buttons:I = 0x7f040001

.field public static final account_settings_edit_quick_responses_fragment:I = 0x7f040002

.field public static final account_settings_exchange_fragment:I = 0x7f040003

.field public static final account_settings_incoming_fragment:I = 0x7f040004

.field public static final account_settings_out_of_office_fragment:I = 0x7f040005

.field public static final account_settings_outgoing_fragment:I = 0x7f040006

.field public static final account_setup_account_type:I = 0x7f040007

.field public static final account_setup_basics:I = 0x7f040008

.field public static final account_setup_basics_common:I = 0x7f040009

.field public static final account_setup_buttons:I = 0x7f04000a

.field public static final account_setup_exchange:I = 0x7f04000b

.field public static final account_setup_exchange_fragment:I = 0x7f04000c

.field public static final account_setup_incoming:I = 0x7f04000d

.field public static final account_setup_incoming_fragment:I = 0x7f04000e

.field public static final account_setup_names:I = 0x7f04000f

.field public static final account_setup_names_common:I = 0x7f040010

.field public static final account_setup_options:I = 0x7f040011

.field public static final account_setup_options_common:I = 0x7f040012

.field public static final account_setup_outgoing:I = 0x7f040013

.field public static final account_setup_outgoing_fragment:I = 0x7f040014

.field public static final account_setup_popup_list:I = 0x7f040015

.field public static final account_shortcut_picker:I = 0x7f040016

.field public static final account_shortcutpicker_list_item1:I = 0x7f040017

.field public static final action_bar_custom_view:I = 0x7f040018

.field public static final action_bar_indeterminate_progress:I = 0x7f040019

.field public static final action_bar_remote_search:I = 0x7f04001a

.field public static final action_bar_search:I = 0x7f04001b

.field public static final action_bar_spinner:I = 0x7f04001c

.field public static final action_bar_spinner_dropdown:I = 0x7f04001d

.field public static final action_bar_spinner_dropdown_header:I = 0x7f04001e

.field public static final action_mode:I = 0x7f04001f

.field public static final address_text_view:I = 0x7f040020

.field public static final attachment:I = 0x7f040021

.field public static final attachment_file_rename:I = 0x7f040022

.field public static final auto_reply_dialog:I = 0x7f040023

.field public static final chips_alternate_item:I = 0x7f040024

.field public static final chips_recipient_dropdown_item:I = 0x7f040025

.field public static final client_certificate_selector:I = 0x7f040026

.field public static final compose_area_buttons:I = 0x7f040027

.field public static final compose_area_recipients:I = 0x7f040028

.field public static final compose_body:I = 0x7f040029

.field public static final compose_from:I = 0x7f04002a

.field public static final compose_subject:I = 0x7f04002b

.field public static final connection_error_banner:I = 0x7f04002c

.field public static final copy_chip_dialog_layout:I = 0x7f04002d

.field public static final debug:I = 0x7f04002e

.field public static final email_activity_one_pane:I = 0x7f04002f

.field public static final email_activity_two_pane:I = 0x7f040030

.field public static final email_vip_activity:I = 0x7f040031

.field public static final email_vip_fragment:I = 0x7f040032

.field public static final email_vip_item:I = 0x7f040033

.field public static final insert_quick_response:I = 0x7f040034

.field public static final mail_browser_link_context_header:I = 0x7f040035

.field public static final mailbox_list_fragment:I = 0x7f040036

.field public static final mailbox_list_header:I = 0x7f040037

.field public static final mailbox_list_item:I = 0x7f040038

.field public static final message_command_button_view:I = 0x7f040039

.field public static final message_compose:I = 0x7f04003a

.field public static final message_compose_attachment:I = 0x7f04003b

.field public static final message_compose_body:I = 0x7f04003c

.field public static final message_file_view:I = 0x7f04003d

.field public static final message_list_fragment:I = 0x7f04003e

.field public static final message_list_item_footer:I = 0x7f04003f

.field public static final message_list_item_normal:I = 0x7f040040

.field public static final message_list_item_wide:I = 0x7f040041

.field public static final message_list_search_header:I = 0x7f040042

.field public static final message_list_warning:I = 0x7f040043

.field public static final message_view_attachment:I = 0x7f040044

.field public static final message_view_details:I = 0x7f040045

.field public static final message_view_fragment:I = 0x7f040046

.field public static final message_view_header_actions:I = 0x7f040047

.field public static final message_view_header_upper:I = 0x7f040048

.field public static final message_view_invitation:I = 0x7f040049

.field public static final message_view_subheader:I = 0x7f04004a

.field public static final more_item:I = 0x7f04004b

.field public static final mtk_icon_list_item:I = 0x7f04004c

.field public static final mtk_message_compose_attachment:I = 0x7f04004d

.field public static final mtk_message_view_fragment_remain_btn:I = 0x7f04004e

.field public static final mtk_vip_pop_up_window:I = 0x7f04004f

.field public static final oof_settings_activity:I = 0x7f040050

.field public static final quick_response_item:I = 0x7f040051

.field public static final quoted_text:I = 0x7f040052

.field public static final recipient_dropdown_item:I = 0x7f040053

.field public static final recipient_dropdown_item_loading:I = 0x7f040054

.field public static final recipients_area:I = 0x7f040055

.field public static final three_pane:I = 0x7f040056

.field public static final vip_actionbar_custom_view:I = 0x7f040057

.field public static final vip_preference_widget_count:I = 0x7f040058

.field public static final waiting_for_sync_message:I = 0x7f040059

.field public static final widget:I = 0x7f04005a

.field public static final widget_list_item:I = 0x7f04005b

.field public static final widget_loading:I = 0x7f04005c


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
