.class interface abstract Lcom/android/email/service/EmailExternalOmacpService$EmailCapability;
.super Ljava/lang/Object;
.source "EmailExternalOmacpService.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/email/service/EmailExternalOmacpService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x608
    name = "EmailCapability"
.end annotation


# static fields
.field public static final APPID:Ljava/lang/String; = "appId"

.field public static final EMAIL:Ljava/lang/String; = "email"

.field public static final EMAIL_FROM:Ljava/lang/String; = "email_from"

.field public static final EMAIL_INBOUND_ADDR:Ljava/lang/String; = "email_inbound_addr"

.field public static final EMAIL_INBOUND_PASSWORD:Ljava/lang/String; = "email_inbound_password"

.field public static final EMAIL_INBOUND_PORT_NUMBER:Ljava/lang/String; = "email_inbound_port_number"

.field public static final EMAIL_INBOUND_SECURE:Ljava/lang/String; = "email_inbound_secure"

.field public static final EMAIL_INBOUND_USER_NAME:Ljava/lang/String; = "email_inbound_user_name"

.field public static final EMAIL_OUTBOUND_ADDR:Ljava/lang/String; = "email_outbound_addr"

.field public static final EMAIL_OUTBOUND_AUTH_TYPE:Ljava/lang/String; = "email_outbound_auth_type"

.field public static final EMAIL_OUTBOUND_PASSWORD:Ljava/lang/String; = "email_outbound_password"

.field public static final EMAIL_OUTBOUND_PORT_NUMBER:Ljava/lang/String; = "email_outbound_port_number"

.field public static final EMAIL_OUTBOUND_SECURE:Ljava/lang/String; = "email_outbound_secure"

.field public static final EMAIL_OUTBOUND_USER_NAME:Ljava/lang/String; = "email_outbound_user_name"

.field public static final EMAIL_PROVIDER_ID:Ljava/lang/String; = "email_provider_id"

.field public static final EMAIL_RT_ADDR:Ljava/lang/String; = "email_rt_addr"
