.class public abstract Lcom/android/email/mail/store/ServiceStore;
.super Lcom/android/email/mail/Store;
.source "ServiceStore.java"


# instance fields
.field protected mHostAuth:Lcom/android/emailcommon/provider/HostAuth;


# direct methods
.method public constructor <init>(Lcom/android/emailcommon/provider/Account;Landroid/content/Context;)V
    .locals 1
    .param p1    # Lcom/android/emailcommon/provider/Account;
    .param p2    # Landroid/content/Context;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/android/emailcommon/mail/MessagingException;
        }
    .end annotation

    invoke-direct {p0}, Lcom/android/email/mail/Store;-><init>()V

    iput-object p2, p0, Lcom/android/email/mail/Store;->mContext:Landroid/content/Context;

    iget-object v0, p0, Lcom/android/email/mail/Store;->mContext:Landroid/content/Context;

    invoke-virtual {p1, v0}, Lcom/android/emailcommon/provider/Account;->getOrCreateHostAuthRecv(Landroid/content/Context;)Lcom/android/emailcommon/provider/HostAuth;

    move-result-object v0

    iput-object v0, p0, Lcom/android/email/mail/store/ServiceStore;->mHostAuth:Lcom/android/emailcommon/provider/HostAuth;

    return-void
.end method


# virtual methods
.method public autoDiscover(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)Landroid/os/Bundle;
    .locals 2
    .param p1    # Landroid/content/Context;
    .param p2    # Ljava/lang/String;
    .param p3    # Ljava/lang/String;

    :try_start_0
    invoke-virtual {p0}, Lcom/android/email/mail/store/ServiceStore;->getService()Lcom/android/emailcommon/service/IEmailService;

    move-result-object v1

    invoke-interface {v1, p2, p3}, Lcom/android/emailcommon/service/IEmailService;->autoDiscover(Ljava/lang/String;Ljava/lang/String;)Landroid/os/Bundle;
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    :goto_0
    return-object v1

    :catch_0
    move-exception v0

    const/4 v1, 0x0

    goto :goto_0
.end method

.method public checkSettings()Landroid/os/Bundle;
    .locals 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/android/emailcommon/mail/MessagingException;
        }
    .end annotation

    :try_start_0
    invoke-virtual {p0}, Lcom/android/email/mail/store/ServiceStore;->getService()Lcom/android/emailcommon/service/IEmailService;

    move-result-object v2

    instance-of v3, v2, Lcom/android/emailcommon/service/EmailServiceProxy;

    if-eqz v3, :cond_0

    move-object v0, v2

    check-cast v0, Lcom/android/emailcommon/service/EmailServiceProxy;

    move-object v3, v0

    const/16 v4, 0x5a

    invoke-virtual {v3, v4}, Lcom/android/emailcommon/service/ServiceProxy;->setTimeout(I)Lcom/android/emailcommon/service/ServiceProxy;

    :cond_0
    iget-object v3, p0, Lcom/android/email/mail/store/ServiceStore;->mHostAuth:Lcom/android/emailcommon/provider/HostAuth;

    invoke-interface {v2, v3}, Lcom/android/emailcommon/service/IEmailService;->validate(Lcom/android/emailcommon/provider/HostAuth;)Landroid/os/Bundle;
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v3

    return-object v3

    :catch_0
    move-exception v1

    new-instance v3, Lcom/android/emailcommon/mail/MessagingException;

    const-string v4, "Call to validate generated an exception"

    invoke-direct {v3, v4, v1}, Lcom/android/emailcommon/mail/MessagingException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v3
.end method

.method protected abstract getService()Lcom/android/emailcommon/service/IEmailService;
.end method
