.class public final Lcom/android/email/R$drawable;
.super Ljava/lang/Object;
.source "R.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/email/R;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "drawable"
.end annotation


# static fields
.field public static final attachment_background:I = 0x7f020000

.field public static final attachment_bg_holo:I = 0x7f020001

.field public static final bg_separator:I = 0x7f020002

.field public static final bg_separator_inset:I = 0x7f020003

.field public static final btn_check_off_normal_holo_light:I = 0x7f020004

.field public static final btn_check_on_normal_holo_light:I = 0x7f020005

.field public static final btn_maybe_off:I = 0x7f020006

.field public static final btn_no_off:I = 0x7f020007

.field public static final btn_star_off_convo_holo_light:I = 0x7f020008

.field public static final btn_star_off_normal_email_holo_light:I = 0x7f020009

.field public static final btn_star_on_convo_holo_light:I = 0x7f02000a

.field public static final btn_star_on_normal_email_holo_light:I = 0x7f02000b

.field public static final btn_yes_off:I = 0x7f02000c

.field public static final chip_background:I = 0x7f02000d

.field public static final chip_background_invalid:I = 0x7f02000e

.field public static final chip_background_selected:I = 0x7f02000f

.field public static final chip_checkmark:I = 0x7f020010

.field public static final chip_delete:I = 0x7f020011

.field public static final conversation_read_selector:I = 0x7f020012

.field public static final conversation_unread_selector:I = 0x7f020013

.field public static final conversation_wide_read_selector:I = 0x7f020014

.field public static final conversation_wide_unread_selector:I = 0x7f020015

.field public static final dropdown_ic_arrow_normal_holo_dark:I = 0x7f020016

.field public static final email_widget_preview:I = 0x7f020017

.field public static final expander_close_holo_light:I = 0x7f020018

.field public static final expander_open_holo_light:I = 0x7f020019

.field public static final gradient_bg_email_widget_holo:I = 0x7f02001a

.field public static final header_bg_email_widget_holo:I = 0x7f02001b

.field public static final header_convo_view_sender_bg_holo:I = 0x7f02001c

.field public static final ic_add_contact_holo_light:I = 0x7f02001d

.field public static final ic_attachment_holo_light:I = 0x7f02001e

.field public static final ic_badge_attachment:I = 0x7f02001f

.field public static final ic_badge_forward_holo_light:I = 0x7f020020

.field public static final ic_badge_invite_holo_light:I = 0x7f020021

.field public static final ic_badge_reply_forward_holo_light:I = 0x7f020022

.field public static final ic_badge_reply_holo_light:I = 0x7f020023

.field public static final ic_cancel_holo_light:I = 0x7f020024

.field public static final ic_contact_picture:I = 0x7f020025

.field public static final ic_dialog_attach:I = 0x7f020026

.field public static final ic_email_add_vip_holo_dark:I = 0x7f020027

.field public static final ic_email_add_vip_holo_light:I = 0x7f020028

.field public static final ic_email_remove_vip_holo_dark:I = 0x7f020029

.field public static final ic_email_remove_vip_holo_light:I = 0x7f02002a

.field public static final ic_email_vip:I = 0x7f02002b

.field public static final ic_email_vip_holo_dark:I = 0x7f02002c

.field public static final ic_exchange_minitab_selected:I = 0x7f02002d

.field public static final ic_exchange_selected:I = 0x7f02002e

.field public static final ic_folder_drafts_holo_light:I = 0x7f02002f

.field public static final ic_folder_inbox_holo_light:I = 0x7f020030

.field public static final ic_folder_outbox_holo_light:I = 0x7f020031

.field public static final ic_folder_sent_holo_light:I = 0x7f020032

.field public static final ic_folder_vip_holo_light:I = 0x7f020033

.field public static final ic_forward_holo_dark:I = 0x7f020034

.field public static final ic_launcher_calendar:I = 0x7f020035

.field public static final ic_launcher_contacts:I = 0x7f020036

.field public static final ic_launcher_filemanager:I = 0x7f020037

.field public static final ic_launcher_gallery:I = 0x7f020038

.field public static final ic_launcher_musicplayer_2:I = 0x7f020039

.field public static final ic_launcher_video_player:I = 0x7f02003a

.field public static final ic_list_combined_inbox:I = 0x7f02003b

.field public static final ic_mailbox_collapsed_holo_light:I = 0x7f02003c

.field public static final ic_menu_compose_normal_holo_light:I = 0x7f02003d

.field public static final ic_menu_mark_read_holo_light:I = 0x7f02003e

.field public static final ic_menu_mark_unread_holo_light:I = 0x7f02003f

.field public static final ic_menu_move_to_holo_light:I = 0x7f020040

.field public static final ic_menu_refresh_holo_light:I = 0x7f020041

.field public static final ic_menu_search_holo_light:I = 0x7f020042

.field public static final ic_menu_send_holo_light:I = 0x7f020043

.field public static final ic_menu_star_holo_light:I = 0x7f020044

.field public static final ic_menu_star_off_holo_light:I = 0x7f020045

.field public static final ic_menu_trash_holo_light:I = 0x7f020046

.field public static final ic_newer_arrow_disabled_holo_light:I = 0x7f020047

.field public static final ic_newer_arrow_holo_light:I = 0x7f020048

.field public static final ic_notification_multiple_mail_holo_dark:I = 0x7f020049

.field public static final ic_notification_vip_holo_light:I = 0x7f02004a

.field public static final ic_older_arrow_disabled_holo_light:I = 0x7f02004b

.field public static final ic_older_arrow_holo_light:I = 0x7f02004c

.field public static final ic_remove_attachment_holo_light:I = 0x7f02004d

.field public static final ic_reply:I = 0x7f02004e

.field public static final ic_reply_all:I = 0x7f02004f

.field public static final ic_reply_all_holo_dark:I = 0x7f020050

.field public static final ic_reply_holo_dark:I = 0x7f020051

.field public static final ic_show_images_holo_light:I = 0x7f020052

.field public static final ic_spam_normal_holo_light:I = 0x7f020053

.field public static final ic_stereo_overlay:I = 0x7f020054

.field public static final ic_unread_label:I = 0x7f020055

.field public static final jog_tab_target_green:I = 0x7f020056

.field public static final jog_tab_target_red:I = 0x7f020057

.field public static final jog_tab_target_yellow:I = 0x7f020058

.field public static final list_activated_holo:I = 0x7f020059

.field public static final list_arrow_activated_holo:I = 0x7f02005a

.field public static final list_arrow_focused_holo:I = 0x7f02005b

.field public static final list_arrow_pressed_holo:I = 0x7f02005c

.field public static final list_arrow_selected_holo:I = 0x7f02005d

.field public static final list_conversation_wide_read_focused_holo:I = 0x7f02005e

.field public static final list_conversation_wide_read_normal_holo:I = 0x7f02005f

.field public static final list_conversation_wide_read_pressed_holo:I = 0x7f020060

.field public static final list_conversation_wide_read_selected_holo:I = 0x7f020061

.field public static final list_conversation_wide_unread_focused_holo:I = 0x7f020062

.field public static final list_conversation_wide_unread_normal_holo:I = 0x7f020063

.field public static final list_conversation_wide_unread_pressed_holo:I = 0x7f020064

.field public static final list_conversation_wide_unread_selected_holo:I = 0x7f020065

.field public static final list_div_top_btm_email_widget_holo:I = 0x7f020066

.field public static final list_focused_holo:I = 0x7f020067

.field public static final list_item_font_primary:I = 0x7f020068

.field public static final list_item_font_secondary:I = 0x7f020069

.field public static final list_pressed_holo:I = 0x7f02006a

.field public static final list_read_holo:I = 0x7f02006b

.field public static final list_selected_holo:I = 0x7f02006c

.field public static final list_unread_holo:I = 0x7f02006d

.field public static final menu_item_newer:I = 0x7f02006e

.field public static final menu_item_older:I = 0x7f02006f

.field public static final message_list_read_selector:I = 0x7f020070

.field public static final message_list_unread_selector:I = 0x7f020071

.field public static final message_list_wide_read_selector:I = 0x7f020072

.field public static final message_list_wide_unread_selector:I = 0x7f020073

.field public static final out_of_office_background:I = 0x7f020074

.field public static final stat_notify_email_generic:I = 0x7f020075

.field public static final stat_notify_email_vip:I = 0x7f020076


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
