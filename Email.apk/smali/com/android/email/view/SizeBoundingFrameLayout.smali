.class public Lcom/android/email/view/SizeBoundingFrameLayout;
.super Landroid/widget/FrameLayout;
.source "SizeBoundingFrameLayout.java"


# static fields
.field public static final DIMENSION_DEFAULT:I = -0x1


# instance fields
.field private mMaxHeight:I

.field private mMaxWidth:I


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1    # Landroid/content/Context;

    const/4 v0, -0x1

    invoke-direct {p0, p1}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;)V

    iput v0, p0, Lcom/android/email/view/SizeBoundingFrameLayout;->mMaxWidth:I

    iput v0, p0, Lcom/android/email/view/SizeBoundingFrameLayout;->mMaxHeight:I

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/util/AttributeSet;

    const/4 v0, -0x1

    invoke-direct {p0, p1, p2}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    iput v0, p0, Lcom/android/email/view/SizeBoundingFrameLayout;->mMaxWidth:I

    iput v0, p0, Lcom/android/email/view/SizeBoundingFrameLayout;->mMaxHeight:I

    invoke-direct {p0, p1, p2}, Lcom/android/email/view/SizeBoundingFrameLayout;->initFromAttributeSet(Landroid/content/Context;Landroid/util/AttributeSet;)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 1
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/util/AttributeSet;
    .param p3    # I

    const/4 v0, -0x1

    invoke-direct {p0, p1, p2, p3}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    iput v0, p0, Lcom/android/email/view/SizeBoundingFrameLayout;->mMaxWidth:I

    iput v0, p0, Lcom/android/email/view/SizeBoundingFrameLayout;->mMaxHeight:I

    invoke-direct {p0, p1, p2}, Lcom/android/email/view/SizeBoundingFrameLayout;->initFromAttributeSet(Landroid/content/Context;Landroid/util/AttributeSet;)V

    return-void
.end method

.method private initFromAttributeSet(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 3
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/util/AttributeSet;

    const/4 v2, -0x1

    sget-object v1, Lcom/android/email/R$styleable;->SizeBoundingFrameLayout_attributes:[I

    invoke-virtual {p1, p2, v1}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[I)Landroid/content/res/TypedArray;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/content/res/TypedArray;->getDimensionPixelSize(II)I

    move-result v1

    iput v1, p0, Lcom/android/email/view/SizeBoundingFrameLayout;->mMaxWidth:I

    const/4 v1, 0x1

    invoke-virtual {v0, v1, v2}, Landroid/content/res/TypedArray;->getDimensionPixelSize(II)I

    move-result v1

    iput v1, p0, Lcom/android/email/view/SizeBoundingFrameLayout;->mMaxHeight:I

    invoke-virtual {v0}, Landroid/content/res/TypedArray;->recycle()V

    return-void
.end method


# virtual methods
.method public getMaxHeight()I
    .locals 1

    iget v0, p0, Lcom/android/email/view/SizeBoundingFrameLayout;->mMaxHeight:I

    return v0
.end method

.method public getMaxWidth()I
    .locals 1

    iget v0, p0, Lcom/android/email/view/SizeBoundingFrameLayout;->mMaxWidth:I

    return v0
.end method

.method protected onMeasure(II)V
    .locals 6
    .param p1    # I
    .param p2    # I

    invoke-static {p1}, Landroid/view/View$MeasureSpec;->getMode(I)I

    move-result v2

    invoke-static {p2}, Landroid/view/View$MeasureSpec;->getMode(I)I

    move-result v0

    invoke-static {p1}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v3

    invoke-static {p2}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v1

    iget v4, p0, Lcom/android/email/view/SizeBoundingFrameLayout;->mMaxWidth:I

    if-ltz v4, :cond_0

    sparse-switch v2, :sswitch_data_0

    :cond_0
    :goto_0
    iget v4, p0, Lcom/android/email/view/SizeBoundingFrameLayout;->mMaxHeight:I

    if-ltz v4, :cond_1

    sparse-switch v0, :sswitch_data_1

    :cond_1
    :goto_1
    invoke-static {v3, v2}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v4

    invoke-static {v1, v0}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v5

    invoke-super {p0, v4, v5}, Landroid/widget/FrameLayout;->onMeasure(II)V

    return-void

    :sswitch_0
    iget v4, p0, Lcom/android/email/view/SizeBoundingFrameLayout;->mMaxWidth:I

    invoke-static {v3, v4}, Ljava/lang/Math;->min(II)I

    move-result v3

    goto :goto_0

    :sswitch_1
    const/high16 v2, -0x80000000

    iget v3, p0, Lcom/android/email/view/SizeBoundingFrameLayout;->mMaxWidth:I

    goto :goto_0

    :sswitch_2
    iget v4, p0, Lcom/android/email/view/SizeBoundingFrameLayout;->mMaxHeight:I

    invoke-static {v1, v4}, Ljava/lang/Math;->min(II)I

    move-result v1

    goto :goto_1

    :sswitch_3
    const/high16 v0, -0x80000000

    iget v1, p0, Lcom/android/email/view/SizeBoundingFrameLayout;->mMaxHeight:I

    goto :goto_1

    :sswitch_data_0
    .sparse-switch
        -0x80000000 -> :sswitch_0
        0x0 -> :sswitch_1
    .end sparse-switch

    :sswitch_data_1
    .sparse-switch
        -0x80000000 -> :sswitch_2
        0x0 -> :sswitch_3
    .end sparse-switch
.end method

.method public setMaxHeight(I)V
    .locals 0
    .param p1    # I

    iput p1, p0, Lcom/android/email/view/SizeBoundingFrameLayout;->mMaxHeight:I

    invoke-virtual {p0}, Landroid/view/View;->requestLayout()V

    invoke-virtual {p0}, Landroid/view/View;->invalidate()V

    return-void
.end method

.method public setMaxWidth(I)V
    .locals 0
    .param p1    # I

    iput p1, p0, Lcom/android/email/view/SizeBoundingFrameLayout;->mMaxWidth:I

    invoke-virtual {p0}, Landroid/view/View;->requestLayout()V

    invoke-virtual {p0}, Landroid/view/View;->invalidate()V

    return-void
.end method
