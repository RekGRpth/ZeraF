.class public Lcom/android/email/NotificationController;
.super Ljava/lang/Object;
.source "NotificationController.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/email/NotificationController$NotificationThread;,
        Lcom/android/email/NotificationController$AccountContentObserver;,
        Lcom/android/email/NotificationController$MessageContentObserver;,
        Lcom/android/email/NotificationController$UnreadMessagesCountObserver;
    }
.end annotation


# static fields
.field private static final MESSAGE_SELECTION:Ljava/lang/String; = "mailboxKey=? AND _id>? AND flagRead=0 AND flagLoaded IN (2,1,4)"

.field private static final MIN_SOUND_INTERVAL_MS:J = 0x3a98L

.field private static final NOTIFICATION_ID_ATTACHMENT_WARNING:I = 0x3

.field private static final NOTIFICATION_ID_BASE_LOGIN_WARNING:I = 0x20000000

.field private static final NOTIFICATION_ID_BASE_NEW_MESSAGES:I = 0x10000000

.field private static final NOTIFICATION_ID_BASE_SEND_WARNING:I = 0x30000000

.field private static final NOTIFICATION_ID_BASE_VIP_MESSAGES:I = 0x40000000

.field private static final NOTIFICATION_ID_EXCHANGE_CALENDAR_ADDED:I = 0x2

.field private static final NOTIFICATION_ID_PASSWORD_EXPIRED:I = 0x5

.field private static final NOTIFICATION_ID_PASSWORD_EXPIRING:I = 0x4

.field private static final NOTIFICATION_ID_SECURITY_NEEDED:I = 0x1

.field private static final TAG:Ljava/lang/String; = "NotificationController"

.field private static sInstance:Lcom/android/email/NotificationController;

.field private static sNotificationHandler:Landroid/os/Handler;

.field private static sNotificationThread:Lcom/android/email/NotificationController$NotificationThread;


# instance fields
.field private mAccountObserver:Landroid/database/ContentObserver;

.field private final mAudioManager:Landroid/media/AudioManager;

.field private final mClock:Lcom/android/email/Clock;

.field private mContext:Landroid/content/Context;

.field private final mGenericMultipleSenderIcon:Landroid/graphics/Bitmap;

.field private final mGenericSenderIcon:Landroid/graphics/Bitmap;

.field private mLastMessageNotifyTime:J

.field private mLastVipMessageNotifyTime:J

.field private final mNeedNotifyVIPMails:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/Long;",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/Long;",
            ">;>;"
        }
    .end annotation
.end field

.field private final mNotificationManager:Landroid/app/NotificationManager;

.field private final mNotificationMap:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/Long;",
            "Landroid/database/ContentObserver;",
            ">;"
        }
    .end annotation
.end field

.field private mSuspendAccountId:J

.field private mUnreadCountObserver:Landroid/database/ContentObserver;

.field private mVipNotifiedMessageCount:I

.field private mVipNotifiedMessageId:J


# direct methods
.method constructor <init>(Landroid/content/Context;Lcom/android/email/Clock;)V
    .locals 2
    .param p1    # Landroid/content/Context;
    .param p2    # Lcom/android/email/Clock;
    .annotation build Lcom/google/common/annotations/VisibleForTesting;
    .end annotation

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const-wide/16 v0, -0x1

    iput-wide v0, p0, Lcom/android/email/NotificationController;->mSuspendAccountId:J

    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/android/email/NotificationController;->mNeedNotifyVIPMails:Ljava/util/HashMap;

    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/android/email/NotificationController;->mLastVipMessageNotifyTime:J

    invoke-virtual {p1}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    iput-object v0, p0, Lcom/android/email/NotificationController;->mContext:Landroid/content/Context;

    const-string v0, "notification"

    invoke-virtual {p1, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/NotificationManager;

    iput-object v0, p0, Lcom/android/email/NotificationController;->mNotificationManager:Landroid/app/NotificationManager;

    const-string v0, "audio"

    invoke-virtual {p1, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/media/AudioManager;

    iput-object v0, p0, Lcom/android/email/NotificationController;->mAudioManager:Landroid/media/AudioManager;

    iget-object v0, p0, Lcom/android/email/NotificationController;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f020025

    invoke-static {v0, v1}, Landroid/graphics/BitmapFactory;->decodeResource(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;

    move-result-object v0

    iput-object v0, p0, Lcom/android/email/NotificationController;->mGenericSenderIcon:Landroid/graphics/Bitmap;

    iget-object v0, p0, Lcom/android/email/NotificationController;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f020049

    invoke-static {v0, v1}, Landroid/graphics/BitmapFactory;->decodeResource(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;

    move-result-object v0

    iput-object v0, p0, Lcom/android/email/NotificationController;->mGenericMultipleSenderIcon:Landroid/graphics/Bitmap;

    iput-object p2, p0, Lcom/android/email/NotificationController;->mClock:Lcom/android/email/Clock;

    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/android/email/NotificationController;->mNotificationMap:Ljava/util/HashMap;

    return-void
.end method

.method static synthetic access$000(Lcom/android/email/NotificationController;)Landroid/content/Context;
    .locals 1
    .param p0    # Lcom/android/email/NotificationController;

    iget-object v0, p0, Lcom/android/email/NotificationController;->mContext:Landroid/content/Context;

    return-object v0
.end method

.method static synthetic access$100(Lcom/android/email/NotificationController;)V
    .locals 0
    .param p0    # Lcom/android/email/NotificationController;

    invoke-direct {p0}, Lcom/android/email/NotificationController;->unregisterUnreadCountObserver()V

    return-void
.end method

.method static synthetic access$1000(Lcom/android/email/NotificationController;)Landroid/app/NotificationManager;
    .locals 1
    .param p0    # Lcom/android/email/NotificationController;

    iget-object v0, p0, Lcom/android/email/NotificationController;->mNotificationManager:Landroid/app/NotificationManager;

    return-object v0
.end method

.method static synthetic access$1100()Lcom/android/email/NotificationController;
    .locals 1

    sget-object v0, Lcom/android/email/NotificationController;->sInstance:Lcom/android/email/NotificationController;

    return-object v0
.end method

.method static synthetic access$1200(Lcom/android/email/NotificationController;)J
    .locals 2
    .param p0    # Lcom/android/email/NotificationController;

    iget-wide v0, p0, Lcom/android/email/NotificationController;->mSuspendAccountId:J

    return-wide v0
.end method

.method static synthetic access$1300(Lcom/android/email/NotificationController;Landroid/database/Cursor;J)Landroid/database/Cursor;
    .locals 1
    .param p0    # Lcom/android/email/NotificationController;
    .param p1    # Landroid/database/Cursor;
    .param p2    # J

    invoke-direct {p0, p1, p2, p3}, Lcom/android/email/NotificationController;->filterVipMessages(Landroid/database/Cursor;J)Landroid/database/Cursor;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$1400(Lcom/android/email/NotificationController;)Ljava/util/HashMap;
    .locals 1
    .param p0    # Lcom/android/email/NotificationController;

    iget-object v0, p0, Lcom/android/email/NotificationController;->mNeedNotifyVIPMails:Ljava/util/HashMap;

    return-object v0
.end method

.method static synthetic access$200(Lcom/android/email/NotificationController;J)V
    .locals 0
    .param p0    # Lcom/android/email/NotificationController;
    .param p1    # J

    invoke-direct {p0, p1, p2}, Lcom/android/email/NotificationController;->unregisterMessageNotification(J)V

    return-void
.end method

.method static synthetic access$300(Lcom/android/email/NotificationController;)Landroid/database/ContentObserver;
    .locals 1
    .param p0    # Lcom/android/email/NotificationController;

    iget-object v0, p0, Lcom/android/email/NotificationController;->mAccountObserver:Landroid/database/ContentObserver;

    return-object v0
.end method

.method static synthetic access$302(Lcom/android/email/NotificationController;Landroid/database/ContentObserver;)Landroid/database/ContentObserver;
    .locals 0
    .param p0    # Lcom/android/email/NotificationController;
    .param p1    # Landroid/database/ContentObserver;

    iput-object p1, p0, Lcom/android/email/NotificationController;->mAccountObserver:Landroid/database/ContentObserver;

    return-object p1
.end method

.method static synthetic access$400()Lcom/android/email/NotificationController$NotificationThread;
    .locals 1

    sget-object v0, Lcom/android/email/NotificationController;->sNotificationThread:Lcom/android/email/NotificationController$NotificationThread;

    return-object v0
.end method

.method static synthetic access$402(Lcom/android/email/NotificationController$NotificationThread;)Lcom/android/email/NotificationController$NotificationThread;
    .locals 0
    .param p0    # Lcom/android/email/NotificationController$NotificationThread;

    sput-object p0, Lcom/android/email/NotificationController;->sNotificationThread:Lcom/android/email/NotificationController$NotificationThread;

    return-object p0
.end method

.method static synthetic access$500(Lcom/android/email/NotificationController;)V
    .locals 0
    .param p0    # Lcom/android/email/NotificationController;

    invoke-direct {p0}, Lcom/android/email/NotificationController;->registerUnreadCountObserver()V

    return-void
.end method

.method static synthetic access$600(Lcom/android/email/NotificationController;J)V
    .locals 0
    .param p0    # Lcom/android/email/NotificationController;
    .param p1    # J

    invoke-direct {p0, p1, p2}, Lcom/android/email/NotificationController;->registerMessageNotification(J)V

    return-void
.end method

.method static synthetic access$700()Landroid/os/Handler;
    .locals 1

    sget-object v0, Lcom/android/email/NotificationController;->sNotificationHandler:Landroid/os/Handler;

    return-object v0
.end method

.method static synthetic access$800(Lcom/android/email/NotificationController;)Ljava/util/HashMap;
    .locals 1
    .param p0    # Lcom/android/email/NotificationController;

    iget-object v0, p0, Lcom/android/email/NotificationController;->mNotificationMap:Ljava/util/HashMap;

    return-object v0
.end method

.method static synthetic access$900(Lcom/android/email/NotificationController;J)I
    .locals 1
    .param p0    # Lcom/android/email/NotificationController;
    .param p1    # J

    invoke-direct {p0, p1, p2}, Lcom/android/email/NotificationController;->getNewMessageNotificationId(J)I

    move-result v0

    return v0
.end method

.method private createBaseAccountNotificationBuilder(Lcom/android/emailcommon/provider/Account;Ljava/lang/String;Ljava/lang/CharSequence;Ljava/lang/String;Landroid/content/Intent;Landroid/graphics/Bitmap;Ljava/lang/Integer;ZZ)Landroid/app/Notification$Builder;
    .locals 6
    .param p1    # Lcom/android/emailcommon/provider/Account;
    .param p2    # Ljava/lang/String;
    .param p3    # Ljava/lang/CharSequence;
    .param p4    # Ljava/lang/String;
    .param p5    # Landroid/content/Intent;
    .param p6    # Landroid/graphics/Bitmap;
    .param p7    # Ljava/lang/Integer;
    .param p8    # Z
    .param p9    # Z

    const/4 v2, 0x0

    const/4 v1, 0x0

    if-eqz p5, :cond_0

    iget-object v3, p0, Lcom/android/email/NotificationController;->mContext:Landroid/content/Context;

    const/high16 v4, 0x8000000

    invoke-static {v3, v2, p5, v4}, Landroid/app/PendingIntent;->getActivity(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v1

    :cond_0
    new-instance v3, Landroid/app/Notification$Builder;

    iget-object v4, p0, Lcom/android/email/NotificationController;->mContext:Landroid/content/Context;

    invoke-direct {v3, v4}, Landroid/app/Notification$Builder;-><init>(Landroid/content/Context;)V

    invoke-virtual {v3, p3}, Landroid/app/Notification$Builder;->setContentTitle(Ljava/lang/CharSequence;)Landroid/app/Notification$Builder;

    move-result-object v3

    invoke-virtual {v3, p4}, Landroid/app/Notification$Builder;->setContentText(Ljava/lang/CharSequence;)Landroid/app/Notification$Builder;

    move-result-object v3

    invoke-virtual {v3, v1}, Landroid/app/Notification$Builder;->setContentIntent(Landroid/app/PendingIntent;)Landroid/app/Notification$Builder;

    move-result-object v3

    invoke-virtual {v3, p6}, Landroid/app/Notification$Builder;->setLargeIcon(Landroid/graphics/Bitmap;)Landroid/app/Notification$Builder;

    move-result-object v3

    if-nez p7, :cond_2

    :goto_0
    invoke-virtual {v3, v2}, Landroid/app/Notification$Builder;->setNumber(I)Landroid/app/Notification$Builder;

    move-result-object v2

    const v3, 0x7f020075

    invoke-virtual {v2, v3}, Landroid/app/Notification$Builder;->setSmallIcon(I)Landroid/app/Notification$Builder;

    move-result-object v2

    iget-object v3, p0, Lcom/android/email/NotificationController;->mClock:Lcom/android/email/Clock;

    invoke-virtual {v3}, Lcom/android/email/Clock;->getTime()J

    move-result-wide v3

    invoke-virtual {v2, v3, v4}, Landroid/app/Notification$Builder;->setWhen(J)Landroid/app/Notification$Builder;

    move-result-object v2

    invoke-virtual {v2, p2}, Landroid/app/Notification$Builder;->setTicker(Ljava/lang/CharSequence;)Landroid/app/Notification$Builder;

    move-result-object v2

    invoke-virtual {v2, p9}, Landroid/app/Notification$Builder;->setOngoing(Z)Landroid/app/Notification$Builder;

    move-result-object v0

    if-eqz p8, :cond_1

    const-string v2, "Email"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "[Notification] createAccountNotification for account: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-wide v4, p1, Lcom/android/emailcommon/provider/EmailContent;->mId:J

    invoke-virtual {v3, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/android/emailcommon/Logging;->d(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {p0, v0, p1}, Lcom/android/email/NotificationController;->setupSoundAndVibration(Landroid/app/Notification$Builder;Lcom/android/emailcommon/provider/Account;)V

    :cond_1
    return-object v0

    :cond_2
    invoke-virtual {p7}, Ljava/lang/Integer;->intValue()I

    move-result v2

    goto :goto_0
.end method

.method private createNotification(Lcom/android/emailcommon/provider/Account;Landroid/database/Cursor;ZLcom/android/emailcommon/provider/EmailContent$Message;Ljava/lang/String;Ljava/lang/CharSequence;Ljava/lang/String;Landroid/content/Intent;Landroid/graphics/Bitmap;Ljava/lang/Integer;ZZ)Landroid/app/Notification;
    .locals 21
    .param p1    # Lcom/android/emailcommon/provider/Account;
    .param p2    # Landroid/database/Cursor;
    .param p3    # Z
    .param p4    # Lcom/android/emailcommon/provider/EmailContent$Message;
    .param p5    # Ljava/lang/String;
    .param p6    # Ljava/lang/CharSequence;
    .param p7    # Ljava/lang/String;
    .param p8    # Landroid/content/Intent;
    .param p9    # Landroid/graphics/Bitmap;
    .param p10    # Ljava/lang/Integer;
    .param p11    # Z
    .param p12    # Z

    const/4 v11, 0x0

    move-object/from16 v2, p0

    move-object/from16 v3, p1

    move-object/from16 v4, p5

    move-object/from16 v5, p6

    move-object/from16 v6, p7

    move-object/from16 v7, p8

    move-object/from16 v8, p9

    move-object/from16 v9, p10

    move/from16 v10, p11

    invoke-direct/range {v2 .. v11}, Lcom/android/email/NotificationController;->createBaseAccountNotificationBuilder(Lcom/android/emailcommon/provider/Account;Ljava/lang/String;Ljava/lang/CharSequence;Ljava/lang/String;Landroid/content/Intent;Landroid/graphics/Bitmap;Ljava/lang/Integer;ZZ)Landroid/app/Notification$Builder;

    move-result-object v13

    invoke-static {}, Lcom/android/email/NotificationController;->isRunningJellybeanOrLater()Z

    move-result v2

    if-eqz v2, :cond_3

    if-eqz p3, :cond_4

    if-eqz p2, :cond_3

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/email/NotificationController;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0b0002

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v17

    move-object/from16 v0, p7

    invoke-virtual {v13, v0}, Landroid/app/Notification$Builder;->setSubText(Ljava/lang/CharSequence;)Landroid/app/Notification$Builder;

    new-instance v14, Landroid/app/Notification$InboxStyle;

    invoke-direct {v14, v13}, Landroid/app/Notification$InboxStyle;-><init>(Landroid/app/Notification$Builder;)V

    move-object/from16 v0, p6

    invoke-virtual {v14, v0}, Landroid/app/Notification$InboxStyle;->setBigContentTitle(Ljava/lang/CharSequence;)Landroid/app/Notification$InboxStyle;

    const/16 v20, 0x0

    :cond_0
    const/4 v2, 0x0

    move-object/from16 v0, p2

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v18

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/email/NotificationController;->mContext:Landroid/content/Context;

    move-wide/from16 v0, v18

    invoke-static {v2, v0, v1}, Lcom/android/emailcommon/provider/EmailContent$Message;->restoreMessageWithId(Landroid/content/Context;J)Lcom/android/emailcommon/provider/EmailContent$Message;

    move-result-object v16

    if-eqz v16, :cond_1

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/email/NotificationController;->mContext:Landroid/content/Context;

    move-object/from16 v0, v16

    invoke-static {v2, v0}, Lcom/android/email/NotificationController;->getSingleMessageInboxLine(Landroid/content/Context;Lcom/android/emailcommon/provider/EmailContent$Message;)Ljava/lang/CharSequence;

    move-result-object v15

    invoke-virtual {v14, v15}, Landroid/app/Notification$InboxStyle;->addLine(Ljava/lang/CharSequence;)Landroid/app/Notification$InboxStyle;

    add-int/lit8 v20, v20, 0x1

    :cond_1
    move/from16 v0, v20

    move/from16 v1, v17

    if-gt v0, v1, :cond_2

    invoke-interface/range {p2 .. p2}, Landroid/database/Cursor;->moveToNext()Z

    move-result v2

    if-nez v2, :cond_0

    :cond_2
    const/4 v2, 0x0

    invoke-virtual {v13, v2}, Landroid/app/Notification$Builder;->setContentText(Ljava/lang/CharSequence;)Landroid/app/Notification$Builder;

    :cond_3
    :goto_0
    invoke-virtual {v13}, Landroid/app/Notification$Builder;->getNotification()Landroid/app/Notification;

    move-result-object v2

    return-object v2

    :cond_4
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/email/NotificationController;->mContext:Landroid/content/Context;

    move-object/from16 v0, p4

    iget-object v3, v0, Lcom/android/emailcommon/provider/EmailContent$Message;->mSubject:Ljava/lang/String;

    invoke-static {v2, v3}, Lcom/android/email/NotificationController;->getSingleMessageLittleText(Landroid/content/Context;Ljava/lang/String;)Ljava/lang/CharSequence;

    move-result-object v2

    invoke-virtual {v13, v2}, Landroid/app/Notification$Builder;->setContentText(Ljava/lang/CharSequence;)Landroid/app/Notification$Builder;

    move-object/from16 v0, p1

    iget-object v2, v0, Lcom/android/emailcommon/provider/Account;->mDisplayName:Ljava/lang/String;

    invoke-virtual {v13, v2}, Landroid/app/Notification$Builder;->setSubText(Ljava/lang/CharSequence;)Landroid/app/Notification$Builder;

    new-instance v12, Landroid/app/Notification$BigTextStyle;

    invoke-direct {v12, v13}, Landroid/app/Notification$BigTextStyle;-><init>(Landroid/app/Notification$Builder;)V

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/email/NotificationController;->mContext:Landroid/content/Context;

    move-object/from16 v0, p4

    invoke-static {v2, v0}, Lcom/android/email/NotificationController;->getSingleMessageBigText(Landroid/content/Context;Lcom/android/emailcommon/provider/EmailContent$Message;)Ljava/lang/CharSequence;

    move-result-object v2

    invoke-virtual {v12, v2}, Landroid/app/Notification$BigTextStyle;->bigText(Ljava/lang/CharSequence;)Landroid/app/Notification$BigTextStyle;

    goto :goto_0
.end method

.method private static declared-synchronized ensureHandlerExists()V
    .locals 3

    const-class v1, Lcom/android/email/NotificationController;

    monitor-enter v1

    :try_start_0
    sget-object v0, Lcom/android/email/NotificationController;->sNotificationThread:Lcom/android/email/NotificationController$NotificationThread;

    if-nez v0, :cond_0

    new-instance v0, Lcom/android/email/NotificationController$NotificationThread;

    invoke-direct {v0}, Lcom/android/email/NotificationController$NotificationThread;-><init>()V

    sput-object v0, Lcom/android/email/NotificationController;->sNotificationThread:Lcom/android/email/NotificationController$NotificationThread;

    new-instance v0, Landroid/os/Handler;

    sget-object v2, Lcom/android/email/NotificationController;->sNotificationThread:Lcom/android/email/NotificationController$NotificationThread;

    invoke-virtual {v2}, Lcom/android/email/NotificationController$NotificationThread;->getLooper()Landroid/os/Looper;

    move-result-object v2

    invoke-direct {v0, v2}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    sput-object v0, Lcom/android/email/NotificationController;->sNotificationHandler:Landroid/os/Handler;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :cond_0
    monitor-exit v1

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method private filterVipMessages(Landroid/database/Cursor;J)Landroid/database/Cursor;
    .locals 7
    .param p1    # Landroid/database/Cursor;
    .param p2    # J

    new-instance v3, Landroid/database/MatrixCursor;

    sget-object v5, Lcom/android/emailcommon/provider/EmailContent;->ID_PROJECTION:[Ljava/lang/String;

    invoke-direct {v3, v5}, Landroid/database/MatrixCursor;-><init>([Ljava/lang/String;)V

    sget-object v5, Lcom/android/email/NotificationController;->sInstance:Lcom/android/email/NotificationController;

    iget-object v5, v5, Lcom/android/email/NotificationController;->mNeedNotifyVIPMails:Ljava/util/HashMap;

    invoke-static {p2, p3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/util/HashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    const/4 v5, -0x1

    invoke-interface {p1, v5}, Landroid/database/Cursor;->moveToPosition(I)Z

    :goto_0
    invoke-interface {p1}, Landroid/database/Cursor;->moveToNext()Z

    move-result v5

    if-eqz v5, :cond_1

    const/4 v5, 0x0

    invoke-interface {p1, v5}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v1

    const/4 v5, 0x1

    invoke-interface {p1, v5}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/mediatek/email/emailvip/VipMemberCache;->isVIP(Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_0

    invoke-static {v1, v2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_0
    invoke-virtual {v3}, Landroid/database/MatrixCursor;->newRow()Landroid/database/MatrixCursor$RowBuilder;

    move-result-object v5

    invoke-static {v1, v2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v6

    invoke-virtual {v5, v6}, Landroid/database/MatrixCursor$RowBuilder;->add(Ljava/lang/Object;)Landroid/database/MatrixCursor$RowBuilder;

    goto :goto_0

    :cond_1
    invoke-virtual {v4}, Ljava/util/ArrayList;->size()I

    move-result v5

    if-lez v5, :cond_2

    sget-object v5, Lcom/android/email/NotificationController;->sInstance:Lcom/android/email/NotificationController;

    iget-object v5, v5, Lcom/android/email/NotificationController;->mNeedNotifyVIPMails:Ljava/util/HashMap;

    invoke-static {p2, p3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v6

    invoke-virtual {v5, v6, v4}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    :cond_2
    return-object v3
.end method

.method public static declared-synchronized getInstance(Landroid/content/Context;)Lcom/android/email/NotificationController;
    .locals 3
    .param p0    # Landroid/content/Context;

    const-class v1, Lcom/android/email/NotificationController;

    monitor-enter v1

    :try_start_0
    sget-object v0, Lcom/android/email/NotificationController;->sInstance:Lcom/android/email/NotificationController;

    if-nez v0, :cond_0

    new-instance v0, Lcom/android/email/NotificationController;

    sget-object v2, Lcom/android/email/Clock;->INSTANCE:Lcom/android/email/Clock;

    invoke-direct {v0, p0, v2}, Lcom/android/email/NotificationController;-><init>(Landroid/content/Context;Lcom/android/email/Clock;)V

    sput-object v0, Lcom/android/email/NotificationController;->sInstance:Lcom/android/email/NotificationController;

    :cond_0
    sget-object v0, Lcom/android/email/NotificationController;->sInstance:Lcom/android/email/NotificationController;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method private getLoginFailedNotificationId(J)I
    .locals 2
    .param p1    # J

    const/high16 v0, 0x20000000

    long-to-int v1, p1

    add-int/2addr v0, v1

    return v0
.end method

.method private getNewMessageNotificationId(J)I
    .locals 2
    .param p1    # J

    const-wide/32 v0, 0x10000000

    add-long/2addr v0, p1

    long-to-int v0, v0

    return v0
.end method

.method private getNewVipMessageTitle(Ljava/lang/String;I)Landroid/text/SpannableString;
    .locals 5
    .param p1    # Ljava/lang/String;
    .param p2    # I

    const/4 v3, 0x1

    if-le p2, v3, :cond_0

    iget-object v1, p0, Lcom/android/email/NotificationController;->mContext:Landroid/content/Context;

    const v2, 0x7f080065

    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    new-array v2, v3, [Ljava/lang/Object;

    const/4 v3, 0x0

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    :goto_0
    new-instance v1, Landroid/text/SpannableString;

    invoke-direct {v1, v0}, Landroid/text/SpannableString;-><init>(Ljava/lang/CharSequence;)V

    return-object v1

    :cond_0
    move-object v0, p1

    goto :goto_0
.end method

.method private getSendFailedNotificationId(J)I
    .locals 2
    .param p1    # J

    const/high16 v0, 0x30000000

    long-to-int v1, p1

    add-int/2addr v0, v1

    return v0
.end method

.method private getSenderPhoto(Lcom/android/emailcommon/provider/EmailContent$Message;)Landroid/graphics/Bitmap;
    .locals 7
    .param p1    # Lcom/android/emailcommon/provider/EmailContent$Message;

    const/4 v3, 0x0

    iget-object v6, p1, Lcom/android/emailcommon/provider/EmailContent$Message;->mFrom:Ljava/lang/String;

    invoke-static {v6}, Lcom/android/emailcommon/mail/Address;->unpackFirst(Ljava/lang/String;)Lcom/android/emailcommon/mail/Address;

    move-result-object v5

    if-nez v5, :cond_1

    :cond_0
    :goto_0
    return-object v3

    :cond_1
    invoke-virtual {v5}, Lcom/android/emailcommon/mail/Address;->getAddress()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v6

    if-nez v6, :cond_0

    iget-object v6, p0, Lcom/android/email/NotificationController;->mContext:Landroid/content/Context;

    invoke-static {v6, v0}, Lcom/android/email/activity/ContactStatusLoader;->getContactInfo(Landroid/content/Context;Ljava/lang/String;)Lcom/android/email/activity/ContactStatusLoader$Result;

    move-result-object v6

    iget-object v3, v6, Lcom/android/email/activity/ContactStatusLoader$Result;->mPhoto:Landroid/graphics/Bitmap;

    if-eqz v3, :cond_0

    iget-object v6, p0, Lcom/android/email/NotificationController;->mContext:Landroid/content/Context;

    invoke-virtual {v6}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v6, 0x1050006

    invoke-virtual {v4, v6}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    const v6, 0x1050005

    invoke-virtual {v4, v6}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v2

    invoke-virtual {v3}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v6

    if-ge v6, v1, :cond_0

    const/4 v6, 0x1

    invoke-static {v3, v2, v1, v6}, Landroid/graphics/Bitmap;->createScaledBitmap(Landroid/graphics/Bitmap;IIZ)Landroid/graphics/Bitmap;

    move-result-object v3

    goto :goto_0
.end method

.method private static getSingleMessageBigText(Landroid/content/Context;Lcom/android/emailcommon/provider/EmailContent$Message;)Ljava/lang/CharSequence;
    .locals 12
    .param p0    # Landroid/content/Context;
    .param p1    # Lcom/android/emailcommon/provider/EmailContent$Message;

    const/4 v8, 0x1

    const/4 v9, 0x0

    new-instance v3, Landroid/text/style/TextAppearanceSpan;

    const v10, 0x7f0c004b

    invoke-direct {v3, p0, v10}, Landroid/text/style/TextAppearanceSpan;-><init>(Landroid/content/Context;I)V

    iget-object v6, p1, Lcom/android/emailcommon/provider/EmailContent$Message;->mSubject:Ljava/lang/String;

    iget-object v4, p1, Lcom/android/emailcommon/provider/EmailContent$Message;->mSnippet:Ljava/lang/String;

    invoke-static {v6}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v10

    if-eqz v10, :cond_0

    move-object v5, v4

    :goto_0
    return-object v5

    :cond_0
    invoke-static {v4}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v10

    if-eqz v10, :cond_1

    new-instance v5, Landroid/text/SpannableString;

    invoke-direct {v5, v6}, Landroid/text/SpannableString;-><init>(Ljava/lang/CharSequence;)V

    invoke-virtual {v6}, Ljava/lang/String;->length()I

    move-result v8

    invoke-virtual {v5, v3, v9, v8, v9}, Landroid/text/SpannableString;->setSpan(Ljava/lang/Object;III)V

    goto :goto_0

    :cond_1
    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v10

    const v11, 0x7f0801fe

    invoke-virtual {v10, v11}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    const-string v10, "%2$s"

    invoke-virtual {v2, v10}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v10

    const-string v11, "%1$s"

    invoke-virtual {v2, v11}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v11

    if-le v10, v11, :cond_2

    move v1, v8

    :goto_1
    const/4 v10, 0x2

    new-array v10, v10, [Ljava/lang/Object;

    aput-object v6, v10, v9

    aput-object v4, v10, v8

    invoke-static {v2, v10}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    new-instance v5, Landroid/text/SpannableString;

    invoke-direct {v5, v0}, Landroid/text/SpannableString;-><init>(Ljava/lang/CharSequence;)V

    if-eqz v1, :cond_3

    invoke-virtual {v0, v6}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v7

    :goto_2
    invoke-virtual {v6}, Ljava/lang/String;->length()I

    move-result v8

    add-int/2addr v8, v7

    invoke-virtual {v5, v3, v7, v8, v9}, Landroid/text/SpannableString;->setSpan(Ljava/lang/Object;III)V

    goto :goto_0

    :cond_2
    move v1, v9

    goto :goto_1

    :cond_3
    invoke-virtual {v0, v6}, Ljava/lang/String;->lastIndexOf(Ljava/lang/String;)I

    move-result v7

    goto :goto_2
.end method

.method private static getSingleMessageInboxLine(Landroid/content/Context;Lcom/android/emailcommon/provider/EmailContent$Message;)Ljava/lang/CharSequence;
    .locals 16
    .param p0    # Landroid/content/Context;
    .param p1    # Lcom/android/emailcommon/provider/EmailContent$Message;

    move-object/from16 v0, p1

    iget-object v11, v0, Lcom/android/emailcommon/provider/EmailContent$Message;->mSubject:Ljava/lang/String;

    move-object/from16 v0, p1

    iget-object v9, v0, Lcom/android/emailcommon/provider/EmailContent$Message;->mSnippet:Ljava/lang/String;

    move-object/from16 v0, p1

    iget-object v13, v0, Lcom/android/emailcommon/provider/EmailContent$Message;->mFrom:Ljava/lang/String;

    invoke-static {v13}, Lcom/android/emailcommon/mail/Address;->unpack(Ljava/lang/String;)[Lcom/android/emailcommon/mail/Address;

    move-result-object v13

    invoke-static {v13}, Lcom/android/emailcommon/mail/Address;->toFriendly([Lcom/android/emailcommon/mail/Address;)Ljava/lang/String;

    move-result-object v8

    invoke-static {v11}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v13

    if-nez v13, :cond_0

    move-object v12, v11

    :goto_0
    new-instance v4, Landroid/text/style/TextAppearanceSpan;

    const v13, 0x7f0c004b

    move-object/from16 v0, p0

    invoke-direct {v4, v0, v13}, Landroid/text/style/TextAppearanceSpan;-><init>(Landroid/content/Context;I)V

    invoke-static {v8}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v13

    if-eqz v13, :cond_1

    move-object v10, v12

    :goto_1
    return-object v10

    :cond_0
    move-object v12, v9

    goto :goto_0

    :cond_1
    invoke-static {v12}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v13

    if-eqz v13, :cond_2

    new-instance v10, Landroid/text/SpannableString;

    invoke-direct {v10, v8}, Landroid/text/SpannableString;-><init>(Ljava/lang/CharSequence;)V

    const/4 v13, 0x0

    invoke-virtual {v8}, Ljava/lang/String;->length()I

    move-result v14

    const/4 v15, 0x0

    invoke-virtual {v10, v4, v13, v14, v15}, Landroid/text/SpannableString;->setSpan(Ljava/lang/Object;III)V

    goto :goto_1

    :cond_2
    invoke-virtual/range {p0 .. p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v13

    const v14, 0x7f0801fd

    invoke-virtual {v13, v14}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    new-instance v5, Landroid/text/style/TextAppearanceSpan;

    const v13, 0x7f0c004c

    move-object/from16 v0, p0

    invoke-direct {v5, v0, v13}, Landroid/text/style/TextAppearanceSpan;-><init>(Landroid/content/Context;I)V

    const/4 v13, 0x2

    new-array v13, v13, [Ljava/lang/Object;

    const/4 v14, 0x0

    aput-object v8, v13, v14

    const/4 v14, 0x1

    aput-object v12, v13, v14

    invoke-static {v1, v13}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    new-instance v10, Landroid/text/SpannableString;

    invoke-direct {v10, v2}, Landroid/text/SpannableString;-><init>(Ljava/lang/CharSequence;)V

    const-string v13, "%2$s"

    invoke-virtual {v1, v13}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v13

    const-string v14, "%1$s"

    invoke-virtual {v1, v14}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v14

    if-ge v13, v14, :cond_3

    const/4 v3, 0x1

    :goto_2
    if-eqz v3, :cond_4

    invoke-virtual {v2, v8}, Ljava/lang/String;->lastIndexOf(Ljava/lang/String;)I

    move-result v6

    :goto_3
    if-eqz v3, :cond_5

    invoke-virtual {v2, v12}, Ljava/lang/String;->lastIndexOf(Ljava/lang/String;)I

    move-result v7

    :goto_4
    invoke-virtual {v8}, Ljava/lang/String;->length()I

    move-result v13

    add-int/2addr v13, v6

    const/4 v14, 0x0

    invoke-virtual {v10, v4, v6, v13, v14}, Landroid/text/SpannableString;->setSpan(Ljava/lang/Object;III)V

    invoke-virtual {v12}, Ljava/lang/String;->length()I

    move-result v13

    add-int/2addr v13, v7

    const/4 v14, 0x0

    invoke-virtual {v10, v5, v7, v13, v14}, Landroid/text/SpannableString;->setSpan(Ljava/lang/Object;III)V

    goto :goto_1

    :cond_3
    const/4 v3, 0x0

    goto :goto_2

    :cond_4
    invoke-virtual {v2, v8}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v6

    goto :goto_3

    :cond_5
    invoke-virtual {v2, v12}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v7

    goto :goto_4
.end method

.method private static getSingleMessageLittleText(Landroid/content/Context;Ljava/lang/String;)Ljava/lang/CharSequence;
    .locals 4
    .param p0    # Landroid/content/Context;
    .param p1    # Ljava/lang/String;

    const/4 v3, 0x0

    if-nez p1, :cond_0

    const/4 v1, 0x0

    :goto_0
    return-object v1

    :cond_0
    new-instance v0, Landroid/text/style/TextAppearanceSpan;

    const v2, 0x7f0c004b

    invoke-direct {v0, p0, v2}, Landroid/text/style/TextAppearanceSpan;-><init>(Landroid/content/Context;I)V

    new-instance v1, Landroid/text/SpannableString;

    invoke-direct {v1, p1}, Landroid/text/SpannableString;-><init>(Ljava/lang/CharSequence;)V

    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v2

    invoke-virtual {v1, v0, v3, v2, v3}, Landroid/text/SpannableString;->setSpan(Ljava/lang/Object;III)V

    goto :goto_0
.end method

.method private getVipMessageCursor()Landroid/database/Cursor;
    .locals 8

    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    iget-object v6, p0, Lcom/android/email/NotificationController;->mNeedNotifyVIPMails:Ljava/util/HashMap;

    invoke-virtual {v6}, Ljava/util/HashMap;->values()Ljava/util/Collection;

    move-result-object v6

    invoke-interface {v6}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v6

    if-eqz v6, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/util/ArrayList;

    invoke-virtual {v3, v4}, Ljava/util/ArrayList;->addAll(Ljava/util/Collection;)Z

    goto :goto_0

    :cond_0
    invoke-static {}, Ljava/util/Collections;->reverseOrder()Ljava/util/Comparator;

    move-result-object v6

    invoke-static {v3, v6}, Ljava/util/Collections;->sort(Ljava/util/List;Ljava/util/Comparator;)V

    new-instance v5, Landroid/database/MatrixCursor;

    sget-object v6, Lcom/android/emailcommon/provider/EmailContent$Message;->ID_COLUMN_PROJECTION:[Ljava/lang/String;

    invoke-direct {v5, v6}, Landroid/database/MatrixCursor;-><init>([Ljava/lang/String;)V

    invoke-virtual {v3}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_1
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v6

    if-eqz v6, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Ljava/lang/Long;

    invoke-virtual {v6}, Ljava/lang/Long;->longValue()J

    move-result-wide v1

    invoke-virtual {v5}, Landroid/database/MatrixCursor;->newRow()Landroid/database/MatrixCursor$RowBuilder;

    move-result-object v6

    invoke-static {v1, v2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v7

    invoke-virtual {v6, v7}, Landroid/database/MatrixCursor$RowBuilder;->add(Ljava/lang/Object;)Landroid/database/MatrixCursor$RowBuilder;

    goto :goto_1

    :cond_1
    return-object v5
.end method

.method private static isRunningJellybeanOrLater()Z
    .locals 2

    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x10

    if-lt v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private needsOngoingNotification(I)Z
    .locals 1
    .param p1    # I

    const/4 v0, 0x1

    if-ne p1, v0, :cond_0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static notifyEmailUnreadNumber(Landroid/content/Context;I)V
    .locals 5
    .param p0    # Landroid/content/Context;
    .param p1    # I

    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    const-string v1, "com.mediatek.action.UNREAD_CHANGED"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    const-string v1, "com.mediatek.intent.extra.UNREAD_NUMBER"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    const-string v1, "com.mediatek.intent.extra.UNREAD_COMPONENT"

    new-instance v2, Landroid/content/ComponentName;

    const-string v3, "com.android.email"

    const-string v4, "com.android.email.activity.Welcome"

    invoke-direct {v2, v3, v4}, Landroid/content/ComponentName;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    invoke-virtual {p0, v0}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    const-string v1, "Email"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "!!!notifyEmailUnreadNumber: send broadcast to laucher : unreadCount = "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    const-string v2, "com_android_email_mtk_unread"

    invoke-static {v1, v2, p1}, Landroid/provider/Settings$System;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    return-void
.end method

.method public static notifyOnBootCompleted()V
    .locals 2

    invoke-static {}, Lcom/android/email/NotificationController;->ensureHandlerExists()V

    sget-object v0, Lcom/android/email/NotificationController;->sNotificationHandler:Landroid/os/Handler;

    new-instance v1, Lcom/android/email/NotificationController$3;

    invoke-direct {v1}, Lcom/android/email/NotificationController$3;-><init>()V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    return-void
.end method

.method private registerMessageNotification(J)V
    .locals 13
    .param p1    # J

    iget-object v2, p0, Lcom/android/email/NotificationController;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const-wide/high16 v2, 0x1000000000000000L

    cmp-long v2, p1, v2

    if-nez v2, :cond_2

    sget-object v1, Lcom/android/emailcommon/provider/Account;->CONTENT_URI:Landroid/net/Uri;

    sget-object v2, Lcom/android/emailcommon/provider/EmailContent;->ID_PROJECTION:[Ljava/lang/String;

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v8

    :goto_0
    :try_start_0
    invoke-interface {v8}, Landroid/database/Cursor;->moveToNext()Z

    move-result v2

    if-eqz v2, :cond_0

    const/4 v2, 0x0

    invoke-interface {v8, v2}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v9

    invoke-direct {p0, v9, v10}, Lcom/android/email/NotificationController;->registerMessageNotification(J)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v2

    invoke-interface {v8}, Landroid/database/Cursor;->close()V

    throw v2

    :cond_0
    invoke-interface {v8}, Landroid/database/Cursor;->close()V

    :cond_1
    :goto_1
    return-void

    :cond_2
    iget-object v2, p0, Lcom/android/email/NotificationController;->mNotificationMap:Ljava/util/HashMap;

    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v12

    check-cast v12, Landroid/database/ContentObserver;

    if-nez v12, :cond_1

    iget-object v2, p0, Lcom/android/email/NotificationController;->mContext:Landroid/content/Context;

    const/4 v3, 0x0

    invoke-static {v2, p1, p2, v3}, Lcom/android/emailcommon/provider/Mailbox;->restoreMailboxOfType(Landroid/content/Context;JI)Lcom/android/emailcommon/provider/Mailbox;

    move-result-object v11

    if-nez v11, :cond_3

    const-string v2, "Email"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Could not load INBOX for account id: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1, p2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1

    :cond_3
    sget-boolean v2, Lcom/android/email/Email;->DEBUG:Z

    if-eqz v2, :cond_4

    const-string v2, "Email"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Registering for notifications for account "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1, p2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    :cond_4
    new-instance v1, Lcom/android/email/NotificationController$MessageContentObserver;

    sget-object v2, Lcom/android/email/NotificationController;->sNotificationHandler:Landroid/os/Handler;

    iget-object v3, p0, Lcom/android/email/NotificationController;->mContext:Landroid/content/Context;

    iget-wide v4, v11, Lcom/android/emailcommon/provider/EmailContent;->mId:J

    move-wide v6, p1

    invoke-direct/range {v1 .. v7}, Lcom/android/email/NotificationController$MessageContentObserver;-><init>(Landroid/os/Handler;Landroid/content/Context;JJ)V

    sget-object v2, Lcom/android/emailcommon/provider/EmailContent$Message;->NOTIFIER_URI:Landroid/net/Uri;

    const/4 v3, 0x1

    invoke-virtual {v0, v2, v3, v1}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;)V

    iget-object v2, p0, Lcom/android/email/NotificationController;->mNotificationMap:Ljava/util/HashMap;

    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    invoke-virtual {v2, v3, v1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Lcom/android/email/NotificationController$MessageContentObserver;->onChange(Z)V

    goto :goto_1
.end method

.method private registerUnreadCountObserver()V
    .locals 5

    const/4 v4, 0x1

    iget-object v2, p0, Lcom/android/email/NotificationController;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    iget-object v2, p0, Lcom/android/email/NotificationController;->mUnreadCountObserver:Landroid/database/ContentObserver;

    if-eqz v2, :cond_0

    :goto_0
    return-void

    :cond_0
    sget-boolean v2, Lcom/android/email/Email;->DEBUG:Z

    if-eqz v2, :cond_1

    const-string v2, "Email"

    const-string v3, "Registering unread count observer for launcher "

    invoke-static {v2, v3}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    :cond_1
    new-instance v0, Lcom/android/email/NotificationController$UnreadMessagesCountObserver;

    sget-object v2, Lcom/android/email/NotificationController;->sNotificationHandler:Landroid/os/Handler;

    iget-object v3, p0, Lcom/android/email/NotificationController;->mContext:Landroid/content/Context;

    invoke-direct {v0, v2, v3}, Lcom/android/email/NotificationController$UnreadMessagesCountObserver;-><init>(Landroid/os/Handler;Landroid/content/Context;)V

    sget-object v2, Lcom/android/emailcommon/provider/EmailContent$Message;->NOTIFIER_URI:Landroid/net/Uri;

    invoke-virtual {v1, v2, v4, v0}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;)V

    iput-object v0, p0, Lcom/android/email/NotificationController;->mUnreadCountObserver:Landroid/database/ContentObserver;

    invoke-virtual {v0, v4}, Lcom/android/email/NotificationController$UnreadMessagesCountObserver;->onChange(Z)V

    goto :goto_0
.end method

.method private showAccountNotification(Lcom/android/emailcommon/provider/Account;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Landroid/content/Intent;I)V
    .locals 12
    .param p1    # Lcom/android/emailcommon/provider/Account;
    .param p2    # Ljava/lang/String;
    .param p3    # Ljava/lang/String;
    .param p4    # Ljava/lang/String;
    .param p5    # Landroid/content/Intent;
    .param p6    # I

    const/4 v7, 0x0

    const/4 v8, 0x0

    const/4 v9, 0x1

    move/from16 v0, p6

    invoke-direct {p0, v0}, Lcom/android/email/NotificationController;->needsOngoingNotification(I)Z

    move-result v10

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object/from16 v5, p4

    move-object/from16 v6, p5

    invoke-direct/range {v1 .. v10}, Lcom/android/email/NotificationController;->createBaseAccountNotificationBuilder(Lcom/android/emailcommon/provider/Account;Ljava/lang/String;Ljava/lang/CharSequence;Ljava/lang/String;Landroid/content/Intent;Landroid/graphics/Bitmap;Ljava/lang/Integer;ZZ)Landroid/app/Notification$Builder;

    move-result-object v11

    iget-object v1, p0, Lcom/android/email/NotificationController;->mNotificationManager:Landroid/app/NotificationManager;

    invoke-virtual {v11}, Landroid/app/Notification$Builder;->getNotification()Landroid/app/Notification;

    move-result-object v2

    move/from16 v0, p6

    invoke-virtual {v1, v0, v2}, Landroid/app/NotificationManager;->notify(ILandroid/app/Notification;)V

    return-void
.end method

.method private unregisterMessageNotification(J)V
    .locals 6
    .param p1    # J

    iget-object v3, p0, Lcom/android/email/NotificationController;->mContext:Landroid/content/Context;

    invoke-virtual {v3}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    const-wide/high16 v3, 0x1000000000000000L

    cmp-long v3, p1, v3

    if-nez v3, :cond_3

    sget-boolean v3, Lcom/android/email/Email;->DEBUG:Z

    if-eqz v3, :cond_0

    const-string v3, "Email"

    const-string v4, "Unregistering notifications for all accounts"

    invoke-static {v3, v4}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    iget-object v3, p0, Lcom/android/email/NotificationController;->mNotificationMap:Ljava/util/HashMap;

    invoke-virtual {v3}, Ljava/util/HashMap;->values()Ljava/util/Collection;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/database/ContentObserver;

    invoke-virtual {v2, v1}, Landroid/content/ContentResolver;->unregisterContentObserver(Landroid/database/ContentObserver;)V

    goto :goto_0

    :cond_1
    iget-object v3, p0, Lcom/android/email/NotificationController;->mNotificationMap:Ljava/util/HashMap;

    invoke-virtual {v3}, Ljava/util/HashMap;->clear()V

    :cond_2
    :goto_1
    return-void

    :cond_3
    sget-boolean v3, Lcom/android/email/Email;->DEBUG:Z

    if-eqz v3, :cond_4

    const-string v3, "Email"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Unregistering notifications for account "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p1, p2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    :cond_4
    iget-object v3, p0, Lcom/android/email/NotificationController;->mNotificationMap:Ljava/util/HashMap;

    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/util/HashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/database/ContentObserver;

    if-eqz v1, :cond_2

    invoke-virtual {v2, v1}, Landroid/content/ContentResolver;->unregisterContentObserver(Landroid/database/ContentObserver;)V

    goto :goto_1
.end method

.method private unregisterUnreadCountObserver()V
    .locals 3

    iget-object v1, p0, Lcom/android/email/NotificationController;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    sget-boolean v1, Lcom/android/email/Email;->DEBUG:Z

    if-eqz v1, :cond_0

    const-string v1, "Email"

    const-string v2, "Unregistering unread count observer for launcher "

    invoke-static {v1, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    iget-object v1, p0, Lcom/android/email/NotificationController;->mUnreadCountObserver:Landroid/database/ContentObserver;

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/android/email/NotificationController;->mUnreadCountObserver:Landroid/database/ContentObserver;

    invoke-virtual {v0, v1}, Landroid/content/ContentResolver;->unregisterContentObserver(Landroid/database/ContentObserver;)V

    const/4 v1, 0x0

    iput-object v1, p0, Lcom/android/email/NotificationController;->mUnreadCountObserver:Landroid/database/ContentObserver;

    :cond_1
    return-void
.end method


# virtual methods
.method public cancelLoginFailedNotification(J)V
    .locals 2
    .param p1    # J

    iget-object v0, p0, Lcom/android/email/NotificationController;->mNotificationManager:Landroid/app/NotificationManager;

    invoke-direct {p0, p1, p2}, Lcom/android/email/NotificationController;->getLoginFailedNotificationId(J)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/app/NotificationManager;->cancel(I)V

    return-void
.end method

.method public cancelPasswordExpirationNotifications()V
    .locals 2

    iget-object v0, p0, Lcom/android/email/NotificationController;->mNotificationManager:Landroid/app/NotificationManager;

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Landroid/app/NotificationManager;->cancel(I)V

    iget-object v0, p0, Lcom/android/email/NotificationController;->mNotificationManager:Landroid/app/NotificationManager;

    const/4 v1, 0x5

    invoke-virtual {v0, v1}, Landroid/app/NotificationManager;->cancel(I)V

    return-void
.end method

.method public cancelSecurityNeededNotification()V
    .locals 2

    iget-object v0, p0, Lcom/android/email/NotificationController;->mNotificationManager:Landroid/app/NotificationManager;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/app/NotificationManager;->cancel(I)V

    return-void
.end method

.method public cancelSendFailedNotification(J)V
    .locals 2
    .param p1    # J

    iget-object v0, p0, Lcom/android/email/NotificationController;->mNotificationManager:Landroid/app/NotificationManager;

    invoke-direct {p0, p1, p2}, Lcom/android/email/NotificationController;->getSendFailedNotificationId(J)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/app/NotificationManager;->cancel(I)V

    return-void
.end method

.method createNewMessageNotification(JJLandroid/database/Cursor;JIIZ)Landroid/app/Notification;
    .locals 26
    .param p1    # J
    .param p3    # J
    .param p5    # Landroid/database/Cursor;
    .param p6    # J
    .param p8    # I
    .param p9    # I
    .param p10    # Z
    .annotation build Lcom/google/common/annotations/VisibleForTesting;
    .end annotation

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/android/email/NotificationController;->mContext:Landroid/content/Context;

    move-wide/from16 v0, p1

    invoke-static {v3, v0, v1}, Lcom/android/emailcommon/provider/Account;->restoreAccountWithId(Landroid/content/Context;J)Lcom/android/emailcommon/provider/Account;

    move-result-object v16

    if-nez v16, :cond_0

    const/16 v19, 0x0

    :goto_0
    return-object v19

    :cond_0
    move-object/from16 v0, v16

    iget v3, v0, Lcom/android/emailcommon/provider/Account;->mFlags:I

    and-int/lit8 v3, v3, 0x1

    if-eqz v3, :cond_1

    const/16 v20, 0x1

    :goto_1
    if-nez v20, :cond_2

    const/16 v19, 0x0

    goto :goto_0

    :cond_1
    const/16 v20, 0x0

    goto :goto_1

    :cond_2
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/android/email/NotificationController;->mContext:Landroid/content/Context;

    move-wide/from16 v0, p6

    invoke-static {v3, v0, v1}, Lcom/android/emailcommon/provider/EmailContent$Message;->restoreMessageWithId(Landroid/content/Context;J)Lcom/android/emailcommon/provider/EmailContent$Message;

    move-result-object v17

    if-nez v17, :cond_3

    const/16 v19, 0x0

    goto :goto_0

    :cond_3
    move-object/from16 v0, v17

    iget-object v3, v0, Lcom/android/emailcommon/provider/EmailContent$Message;->mFrom:Ljava/lang/String;

    invoke-static {v3}, Lcom/android/emailcommon/mail/Address;->unpack(Ljava/lang/String;)[Lcom/android/emailcommon/mail/Address;

    move-result-object v3

    invoke-static {v3}, Lcom/android/emailcommon/mail/Address;->toFriendly([Lcom/android/emailcommon/mail/Address;)Ljava/lang/String;

    move-result-object v23

    if-nez v23, :cond_4

    const-string v23, ""

    :cond_4
    const/4 v3, 0x1

    move/from16 v0, p8

    if-le v0, v3, :cond_5

    const/16 v18, 0x1

    :goto_2
    if-eqz v18, :cond_6

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/email/NotificationController;->mGenericMultipleSenderIcon:Landroid/graphics/Bitmap;

    move-object/from16 v24, v0

    :goto_3
    move-object/from16 v0, p0

    move-object/from16 v1, v23

    move/from16 v2, p8

    invoke-virtual {v0, v1, v2}, Lcom/android/email/NotificationController;->getNewMessageTitle(Ljava/lang/String;I)Landroid/text/SpannableString;

    move-result-object v25

    if-eqz v18, :cond_7

    move-object/from16 v0, v16

    iget-object v10, v0, Lcom/android/emailcommon/provider/Account;->mDisplayName:Ljava/lang/String;

    :goto_4
    if-eqz v24, :cond_8

    move-object/from16 v12, v24

    :goto_5
    const/4 v3, 0x1

    move/from16 v0, p9

    if-le v0, v3, :cond_9

    invoke-static/range {p9 .. p9}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v13

    :goto_6
    const/4 v3, 0x1

    move/from16 v0, p8

    if-le v0, v3, :cond_a

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/android/email/NotificationController;->mContext:Landroid/content/Context;

    move-wide/from16 v0, p1

    invoke-static {v3, v0, v1}, Lcom/android/email/activity/Welcome;->createOpenAccountInboxIntent(Landroid/content/Context;J)Landroid/content/Intent;

    move-result-object v11

    :goto_7
    const v3, 0x1000c000

    invoke-virtual {v11, v3}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/android/email/NotificationController;->mClock:Lcom/android/email/Clock;

    invoke-virtual {v3}, Lcom/android/email/Clock;->getTime()J

    move-result-wide v21

    if-nez p10, :cond_b

    move-object/from16 v0, p0

    iget-wide v3, v0, Lcom/android/email/NotificationController;->mLastMessageNotifyTime:J

    sub-long v3, v21, v3

    const-wide/16 v5, 0x3a98

    cmp-long v3, v3, v5

    if-lez v3, :cond_b

    const/4 v14, 0x1

    :goto_8
    if-eqz p10, :cond_c

    const/4 v8, 0x0

    :goto_9
    const/4 v15, 0x0

    move-object/from16 v3, p0

    move-object/from16 v4, v16

    move-object/from16 v5, p5

    move/from16 v6, v18

    move-object/from16 v7, v17

    move-object/from16 v9, v25

    invoke-direct/range {v3 .. v15}, Lcom/android/email/NotificationController;->createNotification(Lcom/android/emailcommon/provider/Account;Landroid/database/Cursor;ZLcom/android/emailcommon/provider/EmailContent$Message;Ljava/lang/String;Ljava/lang/CharSequence;Ljava/lang/String;Landroid/content/Intent;Landroid/graphics/Bitmap;Ljava/lang/Integer;ZZ)Landroid/app/Notification;

    move-result-object v19

    move-wide/from16 v0, v21

    move-object/from16 v2, p0

    iput-wide v0, v2, Lcom/android/email/NotificationController;->mLastMessageNotifyTime:J

    goto/16 :goto_0

    :cond_5
    const/16 v18, 0x0

    goto :goto_2

    :cond_6
    move-object/from16 v0, p0

    move-object/from16 v1, v17

    invoke-direct {v0, v1}, Lcom/android/email/NotificationController;->getSenderPhoto(Lcom/android/emailcommon/provider/EmailContent$Message;)Landroid/graphics/Bitmap;

    move-result-object v24

    goto :goto_3

    :cond_7
    move-object/from16 v0, v17

    iget-object v10, v0, Lcom/android/emailcommon/provider/EmailContent$Message;->mSubject:Ljava/lang/String;

    goto :goto_4

    :cond_8
    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/android/email/NotificationController;->mGenericSenderIcon:Landroid/graphics/Bitmap;

    goto :goto_5

    :cond_9
    const/4 v13, 0x0

    goto :goto_6

    :cond_a
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/android/email/NotificationController;->mContext:Landroid/content/Context;

    move-wide/from16 v4, p1

    move-wide/from16 v6, p3

    move-wide/from16 v8, p6

    invoke-static/range {v3 .. v9}, Lcom/android/email/activity/Welcome;->createOpenMessageIntent(Landroid/content/Context;JJJ)Landroid/content/Intent;

    move-result-object v11

    goto :goto_7

    :cond_b
    const/4 v14, 0x0

    goto :goto_8

    :cond_c
    invoke-virtual/range {v25 .. v25}, Landroid/text/SpannableString;->toString()Ljava/lang/String;

    move-result-object v8

    goto :goto_9
.end method

.method createNewMessageNotificationPlus(JJJII)Lcom/mediatek/notification/NotificationPlus;
    .locals 17
    .param p1    # J
    .param p3    # J
    .param p5    # J
    .param p7    # I
    .param p8    # I

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/email/NotificationController;->mContext:Landroid/content/Context;

    move-wide/from16 v0, p1

    invoke-static {v2, v0, v1}, Lcom/android/emailcommon/provider/Account;->restoreAccountWithId(Landroid/content/Context;J)Lcom/android/emailcommon/provider/Account;

    move-result-object v9

    if-nez v9, :cond_0

    const/4 v12, 0x0

    :goto_0
    return-object v12

    :cond_0
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/email/NotificationController;->mContext:Landroid/content/Context;

    move-wide/from16 v0, p5

    invoke-static {v2, v0, v1}, Lcom/android/emailcommon/provider/EmailContent$Message;->restoreMessageWithId(Landroid/content/Context;J)Lcom/android/emailcommon/provider/EmailContent$Message;

    move-result-object v11

    if-nez v11, :cond_1

    const/4 v12, 0x0

    goto :goto_0

    :cond_1
    iget-object v2, v11, Lcom/android/emailcommon/provider/EmailContent$Message;->mFrom:Ljava/lang/String;

    invoke-static {v2}, Lcom/android/emailcommon/mail/Address;->unpack(Ljava/lang/String;)[Lcom/android/emailcommon/mail/Address;

    move-result-object v2

    invoke-static {v2}, Lcom/android/emailcommon/mail/Address;->toFriendly([Lcom/android/emailcommon/mail/Address;)Ljava/lang/String;

    move-result-object v14

    if-nez v14, :cond_2

    const-string v14, ""

    :cond_2
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/email/NotificationController;->mContext:Landroid/content/Context;

    const v3, 0x7f08009e

    invoke-virtual {v2, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    invoke-static/range {p7 .. p7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v3, v4

    invoke-static {v2, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v16

    iget-object v15, v11, Lcom/android/emailcommon/provider/EmailContent$Message;->mSubject:Ljava/lang/String;

    const/4 v2, 0x1

    move/from16 v0, p7

    if-le v0, v2, :cond_3

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/email/NotificationController;->mContext:Landroid/content/Context;

    move-wide/from16 v0, p1

    invoke-static {v2, v0, v1}, Lcom/android/email/activity/Welcome;->createOpenAccountInboxIntent(Landroid/content/Context;J)Landroid/content/Intent;

    move-result-object v10

    :goto_1
    const v2, 0x10008000

    invoke-virtual {v10, v2}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/email/NotificationController;->mContext:Landroid/content/Context;

    const/4 v3, 0x0

    const/high16 v4, 0x8000000

    invoke-static {v2, v3, v10, v4}, Landroid/app/PendingIntent;->getActivity(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v13

    new-instance v2, Lcom/mediatek/notification/NotificationPlus$Builder;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/android/email/NotificationController;->mContext:Landroid/content/Context;

    invoke-direct {v2, v3}, Lcom/mediatek/notification/NotificationPlus$Builder;-><init>(Landroid/content/Context;)V

    invoke-virtual/range {v16 .. v16}, Ljava/lang/String;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/mediatek/notification/NotificationPlus$Builder;->setTitle(Ljava/lang/String;)Lcom/mediatek/notification/NotificationPlus$Builder;

    move-result-object v2

    invoke-virtual {v2, v15}, Lcom/mediatek/notification/NotificationPlus$Builder;->setMessage(Ljava/lang/String;)Lcom/mediatek/notification/NotificationPlus$Builder;

    move-result-object v2

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/android/email/NotificationController;->mContext:Landroid/content/Context;

    const v4, 0x104000a

    invoke-virtual {v3, v4}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3, v13}, Lcom/mediatek/notification/NotificationPlus$Builder;->setPositiveButton(Ljava/lang/String;Landroid/app/PendingIntent;)Lcom/mediatek/notification/NotificationPlus$Builder;

    move-result-object v2

    invoke-virtual {v2}, Lcom/mediatek/notification/NotificationPlus$Builder;->create()Lcom/mediatek/notification/NotificationPlus;

    move-result-object v12

    const-string v2, "NotificationController"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Have created a new NotificationPlus for title : "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual/range {v16 .. v16}, Ljava/lang/String;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/android/emailcommon/Logging;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    :cond_3
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/email/NotificationController;->mContext:Landroid/content/Context;

    move-wide/from16 v3, p1

    move-wide/from16 v5, p3

    move-wide/from16 v7, p5

    invoke-static/range {v2 .. v8}, Lcom/android/email/activity/Welcome;->createOpenMessageIntent(Landroid/content/Context;JJJ)Landroid/content/Intent;

    move-result-object v10

    goto :goto_1
.end method

.method createVipMessageNotification()Landroid/app/Notification;
    .locals 36

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/android/email/NotificationController;->mNeedNotifyVIPMails:Ljava/util/HashMap;

    invoke-virtual {v3}, Ljava/util/HashMap;->size()I

    move-result v34

    if-gtz v34, :cond_0

    const-string v3, "NotificationController"

    const-string v4, "createVipMessageNotification no VIP mails, return null"

    invoke-static {v3, v4}, Lcom/android/emailcommon/Logging;->d(Ljava/lang/String;Ljava/lang/String;)V

    const/16 v25, 0x0

    :goto_0
    return-object v25

    :cond_0
    const/4 v11, 0x0

    const/4 v3, 0x1

    move/from16 v0, v34

    if-ne v0, v3, :cond_1

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/android/email/NotificationController;->mNeedNotifyVIPMails:Ljava/util/HashMap;

    invoke-virtual {v3}, Ljava/util/HashMap;->keySet()Ljava/util/Set;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/Set;->toArray()[Ljava/lang/Object;

    move-result-object v3

    const/4 v4, 0x0

    aget-object v3, v3, v4

    check-cast v3, Ljava/lang/Long;

    invoke-virtual {v3}, Ljava/lang/Long;->longValue()J

    move-result-wide v23

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/android/email/NotificationController;->mContext:Landroid/content/Context;

    move-wide/from16 v0, v23

    invoke-static {v3, v0, v1}, Lcom/android/emailcommon/provider/Account;->restoreAccountWithId(Landroid/content/Context;J)Lcom/android/emailcommon/provider/Account;

    move-result-object v11

    :goto_1
    if-nez v11, :cond_2

    const/16 v25, 0x0

    goto :goto_0

    :cond_1
    new-instance v11, Lcom/android/emailcommon/provider/Account;

    invoke-direct {v11}, Lcom/android/emailcommon/provider/Account;-><init>()V

    const-wide/high16 v3, 0x1000000000000000L

    iput-wide v3, v11, Lcom/android/emailcommon/provider/EmailContent;->mId:J

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/android/email/NotificationController;->mContext:Landroid/content/Context;

    const v4, 0x7f080064

    invoke-virtual {v3, v4}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    invoke-static/range {v34 .. v34}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    aput-object v6, v4, v5

    invoke-static {v3, v4}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    iput-object v3, v11, Lcom/android/emailcommon/provider/Account;->mDisplayName:Ljava/lang/String;

    goto :goto_1

    :cond_2
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/android/email/NotificationController;->mContext:Landroid/content/Context;

    invoke-static {v3, v11}, Lcom/mediatek/email/emailvip/VipNotificationStyle;->updateVipSoundAndVibration(Landroid/content/Context;Lcom/android/emailcommon/provider/Account;)V

    invoke-direct/range {p0 .. p0}, Lcom/android/email/NotificationController;->getVipMessageCursor()Landroid/database/Cursor;

    move-result-object v12

    invoke-interface {v12}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v3

    if-nez v3, :cond_3

    const/16 v25, 0x0

    goto :goto_0

    :cond_3
    const/4 v3, 0x0

    invoke-interface {v12, v3}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v8

    invoke-interface {v12}, Landroid/database/Cursor;->getCount()I

    move-result v33

    move-object/from16 v0, p0

    iget v3, v0, Lcom/android/email/NotificationController;->mVipNotifiedMessageCount:I

    move/from16 v0, v33

    if-ne v0, v3, :cond_5

    const-wide/16 v3, 0x0

    cmp-long v3, v8, v3

    if-eqz v3, :cond_4

    move-object/from16 v0, p0

    iget-wide v3, v0, Lcom/android/email/NotificationController;->mVipNotifiedMessageId:J

    cmp-long v3, v8, v3

    if-nez v3, :cond_5

    :cond_4
    const-string v3, "NotificationController"

    const-string v4, "createVipMessageNotification same as previous, do not notify again"

    invoke-static {v3, v4}, Lcom/android/emailcommon/Logging;->d(Ljava/lang/String;Ljava/lang/String;)V

    const/16 v25, 0x0

    goto/16 :goto_0

    :cond_5
    const/16 v29, 0x0

    move-object/from16 v0, p0

    iget v3, v0, Lcom/android/email/NotificationController;->mVipNotifiedMessageCount:I

    move/from16 v0, v33

    if-ge v0, v3, :cond_6

    const/16 v29, 0x1

    :cond_6
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/android/email/NotificationController;->mContext:Landroid/content/Context;

    invoke-static {v3}, Lcom/android/email/Preferences;->getPreferences(Landroid/content/Context;)Lcom/android/email/Preferences;

    move-result-object v3

    invoke-virtual {v3}, Lcom/android/email/Preferences;->getVipNotification()Z

    move-result v26

    if-nez v26, :cond_7

    if-nez v29, :cond_7

    const/16 v25, 0x0

    goto/16 :goto_0

    :cond_7
    move/from16 v0, v33

    move-object/from16 v1, p0

    iput v0, v1, Lcom/android/email/NotificationController;->mVipNotifiedMessageCount:I

    move-object/from16 v0, p0

    iput-wide v8, v0, Lcom/android/email/NotificationController;->mVipNotifiedMessageId:J

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/android/email/NotificationController;->mContext:Landroid/content/Context;

    invoke-static {v3, v8, v9}, Lcom/android/emailcommon/provider/EmailContent$Message;->restoreMessageWithId(Landroid/content/Context;J)Lcom/android/emailcommon/provider/EmailContent$Message;

    move-result-object v14

    if-nez v14, :cond_8

    const/16 v25, 0x0

    goto/16 :goto_0

    :cond_8
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/android/email/NotificationController;->mContext:Landroid/content/Context;

    iget-wide v4, v11, Lcom/android/emailcommon/provider/EmailContent;->mId:J

    const/4 v6, 0x1

    invoke-static {v3, v4, v5, v6}, Lcom/mediatek/email/emailvip/VipMemberCache;->getVipMessagesCount(Landroid/content/Context;JZ)I

    move-result v32

    iget-object v3, v14, Lcom/android/emailcommon/provider/EmailContent$Message;->mFrom:Ljava/lang/String;

    invoke-static {v3}, Lcom/android/emailcommon/mail/Address;->unpack(Ljava/lang/String;)[Lcom/android/emailcommon/mail/Address;

    move-result-object v3

    invoke-static {v3}, Lcom/android/emailcommon/mail/Address;->toFriendly([Lcom/android/emailcommon/mail/Address;)Ljava/lang/String;

    move-result-object v30

    if-nez v30, :cond_9

    const-string v30, ""

    :cond_9
    const/4 v3, 0x1

    move/from16 v0, v33

    if-le v0, v3, :cond_a

    const/4 v13, 0x1

    :goto_2
    if-eqz v13, :cond_b

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/email/NotificationController;->mGenericMultipleSenderIcon:Landroid/graphics/Bitmap;

    move-object/from16 v31, v0

    :goto_3
    move-object/from16 v0, p0

    move-object/from16 v1, v30

    move/from16 v2, v33

    invoke-direct {v0, v1, v2}, Lcom/android/email/NotificationController;->getNewVipMessageTitle(Ljava/lang/String;I)Landroid/text/SpannableString;

    move-result-object v16

    if-eqz v13, :cond_c

    iget-object v0, v11, Lcom/android/emailcommon/provider/Account;->mDisplayName:Ljava/lang/String;

    move-object/from16 v17, v0

    :goto_4
    if-eqz v31, :cond_d

    move-object/from16 v19, v31

    :goto_5
    const/4 v3, 0x1

    move/from16 v0, v32

    if-le v0, v3, :cond_e

    invoke-static/range {v32 .. v32}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v20

    :goto_6
    const/4 v3, 0x1

    move/from16 v0, v33

    if-le v0, v3, :cond_f

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/android/email/NotificationController;->mContext:Landroid/content/Context;

    iget-wide v4, v11, Lcom/android/emailcommon/provider/EmailContent;->mId:J

    const-wide/16 v6, -0x7

    const-wide/16 v8, -0x1

    invoke-static/range {v3 .. v9}, Lcom/android/email/activity/Welcome;->createOpenMessageIntent(Landroid/content/Context;JJJ)Landroid/content/Intent;

    move-result-object v18

    :goto_7
    const v3, 0x1000c000

    move-object/from16 v0, v18

    invoke-virtual {v0, v3}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/android/email/NotificationController;->mClock:Lcom/android/email/Clock;

    invoke-virtual {v3}, Lcom/android/email/Clock;->getTime()J

    move-result-wide v27

    if-nez v29, :cond_10

    move-object/from16 v0, p0

    iget-wide v3, v0, Lcom/android/email/NotificationController;->mLastVipMessageNotifyTime:J

    sub-long v3, v27, v3

    const-wide/16 v5, 0x3a98

    cmp-long v3, v3, v5

    if-lez v3, :cond_10

    const/16 v21, 0x1

    :goto_8
    if-eqz v29, :cond_11

    const/4 v15, 0x0

    :goto_9
    const/16 v22, 0x0

    move-object/from16 v10, p0

    invoke-direct/range {v10 .. v22}, Lcom/android/email/NotificationController;->createNotification(Lcom/android/emailcommon/provider/Account;Landroid/database/Cursor;ZLcom/android/emailcommon/provider/EmailContent$Message;Ljava/lang/String;Ljava/lang/CharSequence;Ljava/lang/String;Landroid/content/Intent;Landroid/graphics/Bitmap;Ljava/lang/Integer;ZZ)Landroid/app/Notification;

    move-result-object v25

    move-wide/from16 v0, v27

    move-object/from16 v2, p0

    iput-wide v0, v2, Lcom/android/email/NotificationController;->mLastVipMessageNotifyTime:J

    new-instance v35, Lcom/mediatek/email/emailvip/VipNotificationStyle;

    invoke-direct/range {v35 .. v35}, Lcom/mediatek/email/emailvip/VipNotificationStyle;-><init>()V

    move-object/from16 v0, v35

    move-object/from16 v1, v16

    invoke-virtual {v0, v1}, Lcom/mediatek/email/emailvip/VipNotificationStyle;->addVipTitle(Ljava/lang/CharSequence;)V

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/android/email/NotificationController;->mContext:Landroid/content/Context;

    move-object/from16 v0, v35

    move-object/from16 v1, v25

    invoke-virtual {v0, v3, v1}, Lcom/mediatek/email/emailvip/VipNotificationStyle;->updateVipIcon(Landroid/content/Context;Landroid/app/Notification;)V

    goto/16 :goto_0

    :cond_a
    const/4 v13, 0x0

    goto/16 :goto_2

    :cond_b
    move-object/from16 v0, p0

    invoke-direct {v0, v14}, Lcom/android/email/NotificationController;->getSenderPhoto(Lcom/android/emailcommon/provider/EmailContent$Message;)Landroid/graphics/Bitmap;

    move-result-object v31

    goto/16 :goto_3

    :cond_c
    iget-object v0, v14, Lcom/android/emailcommon/provider/EmailContent$Message;->mSubject:Ljava/lang/String;

    move-object/from16 v17, v0

    goto :goto_4

    :cond_d
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/email/NotificationController;->mGenericSenderIcon:Landroid/graphics/Bitmap;

    move-object/from16 v19, v0

    goto :goto_5

    :cond_e
    const/16 v20, 0x0

    goto :goto_6

    :cond_f
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/android/email/NotificationController;->mContext:Landroid/content/Context;

    iget-wide v4, v11, Lcom/android/emailcommon/provider/EmailContent;->mId:J

    iget-wide v6, v14, Lcom/android/emailcommon/provider/EmailContent$Message;->mMailboxKey:J

    invoke-static/range {v3 .. v9}, Lcom/android/email/activity/Welcome;->createOpenMessageIntent(Landroid/content/Context;JJJ)Landroid/content/Intent;

    move-result-object v18

    goto :goto_7

    :cond_10
    const/16 v21, 0x0

    goto :goto_8

    :cond_11
    invoke-virtual/range {v16 .. v16}, Landroid/text/SpannableString;->toString()Ljava/lang/String;

    move-result-object v15

    goto :goto_9
.end method

.method getNewMessageTitle(Ljava/lang/String;I)Landroid/text/SpannableString;
    .locals 5
    .param p1    # Ljava/lang/String;
    .param p2    # I
    .annotation build Lcom/google/common/annotations/VisibleForTesting;
    .end annotation

    const/4 v3, 0x1

    if-le p2, v3, :cond_0

    iget-object v1, p0, Lcom/android/email/NotificationController;->mContext:Landroid/content/Context;

    const v2, 0x7f08009e

    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    new-array v2, v3, [Ljava/lang/Object;

    const/4 v3, 0x0

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    :goto_0
    new-instance v1, Landroid/text/SpannableString;

    invoke-direct {v1, v0}, Landroid/text/SpannableString;-><init>(Ljava/lang/CharSequence;)V

    return-object v1

    :cond_0
    move-object v0, p1

    goto :goto_0
.end method

.method getRingerMode()I
    .locals 1
    .annotation build Lcom/google/common/annotations/VisibleForTesting;
    .end annotation

    iget-object v0, p0, Lcom/android/email/NotificationController;->mAudioManager:Landroid/media/AudioManager;

    invoke-virtual {v0}, Landroid/media/AudioManager;->getRingerMode()I

    move-result v0

    return v0
.end method

.method postNotificationForNewMessage(JJLandroid/database/Cursor;JIIZ)V
    .locals 13
    .param p1    # J
    .param p3    # J
    .param p5    # Landroid/database/Cursor;
    .param p6    # J
    .param p8    # I
    .param p9    # I
    .param p10    # Z

    sget-object v0, Lcom/android/email/NotificationController;->sInstance:Lcom/android/email/NotificationController;

    move-wide v1, p1

    move-wide/from16 v3, p3

    move-object/from16 v5, p5

    move-wide/from16 v6, p6

    move/from16 v8, p8

    move/from16 v9, p9

    move/from16 v10, p10

    invoke-virtual/range {v0 .. v10}, Lcom/android/email/NotificationController;->createNewMessageNotification(JJLandroid/database/Cursor;JIIZ)Landroid/app/Notification;

    move-result-object v11

    if-eqz v11, :cond_0

    const-string v0, "Email"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "[Notification] notify default flag "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, v11, Landroid/app/Notification;->defaults:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/android/emailcommon/Logging;->d(Ljava/lang/String;Ljava/lang/String;)V

    sget-object v0, Lcom/android/email/NotificationController;->sInstance:Lcom/android/email/NotificationController;

    iget-object v0, v0, Lcom/android/email/NotificationController;->mNotificationManager:Landroid/app/NotificationManager;

    sget-object v1, Lcom/android/email/NotificationController;->sInstance:Lcom/android/email/NotificationController;

    invoke-direct {v1, p1, p2}, Lcom/android/email/NotificationController;->getNewMessageNotificationId(J)I

    move-result v1

    invoke-virtual {v0, v1, v11}, Landroid/app/NotificationManager;->notify(ILandroid/app/Notification;)V

    :cond_0
    sget-object v0, Lcom/android/email/NotificationController;->sInstance:Lcom/android/email/NotificationController;

    move-wide v1, p1

    move-wide/from16 v3, p3

    move-wide/from16 v5, p6

    move/from16 v7, p8

    move/from16 v8, p9

    invoke-virtual/range {v0 .. v8}, Lcom/android/email/NotificationController;->createNewMessageNotificationPlus(JJJII)Lcom/mediatek/notification/NotificationPlus;

    move-result-object v12

    if-nez v12, :cond_1

    const-string v0, "NotificationController"

    const-string v1, " NotificationPlus create failed "

    invoke-static {v0, v1}, Lcom/android/emailcommon/Logging;->w(Ljava/lang/String;Ljava/lang/String;)V

    :goto_0
    return-void

    :cond_1
    const/4 v0, 0x1

    invoke-static {v0, v12}, Lcom/mediatek/notification/NotificationManagerPlus;->notify(ILcom/mediatek/notification/NotificationPlus;)V

    const-string v0, "NotificationController"

    const-string v1, " NotificationManagerPlus will popup NotificationPlus "

    invoke-static {v0, v1}, Lcom/android/emailcommon/Logging;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public postVipMailNotification(ZJ)V
    .locals 3
    .param p1    # Z
    .param p2    # J

    const-wide/16 v1, -0x1

    cmp-long v1, p2, v1

    if-nez v1, :cond_0

    :goto_0
    return-void

    :cond_0
    invoke-static {}, Lcom/android/email/NotificationController;->ensureHandlerExists()V

    const-wide/high16 v1, 0x1000000000000000L

    cmp-long v1, p2, v1

    if-nez v1, :cond_1

    new-instance v0, Lcom/android/email/NotificationController$4;

    invoke-direct {v0, p0, p1}, Lcom/android/email/NotificationController$4;-><init>(Lcom/android/email/NotificationController;Z)V

    :goto_1
    sget-object v1, Lcom/android/email/NotificationController;->sNotificationHandler:Landroid/os/Handler;

    invoke-virtual {v1, v0}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    goto :goto_0

    :cond_1
    new-instance v0, Lcom/android/email/NotificationController$5;

    invoke-direct {v0, p0, p1, p2, p3}, Lcom/android/email/NotificationController$5;-><init>(Lcom/android/email/NotificationController;ZJ)V

    goto :goto_1
.end method

.method sendOrCancelVipNotification()V
    .locals 5

    const/high16 v4, 0x40000000

    iget-object v1, p0, Lcom/android/email/NotificationController;->mNeedNotifyVIPMails:Ljava/util/HashMap;

    invoke-virtual {v1}, Ljava/util/HashMap;->size()I

    move-result v1

    if-lez v1, :cond_1

    sget-object v1, Lcom/android/email/NotificationController;->sInstance:Lcom/android/email/NotificationController;

    invoke-virtual {v1}, Lcom/android/email/NotificationController;->createVipMessageNotification()Landroid/app/Notification;

    move-result-object v0

    const-string v1, "NotificationController"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Post VIP Notification: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/android/emailcommon/Logging;->d(Ljava/lang/String;Ljava/lang/String;)V

    if-eqz v0, :cond_0

    sget-object v1, Lcom/android/email/NotificationController;->sInstance:Lcom/android/email/NotificationController;

    iget-object v1, v1, Lcom/android/email/NotificationController;->mNotificationManager:Landroid/app/NotificationManager;

    invoke-virtual {v1, v4, v0}, Landroid/app/NotificationManager;->notify(ILandroid/app/Notification;)V

    :cond_0
    :goto_0
    return-void

    :cond_1
    const-string v1, "NotificationController"

    const-string v2, "Cancel VIP Notification"

    invoke-static {v1, v2}, Lcom/android/emailcommon/Logging;->d(Ljava/lang/String;Ljava/lang/String;)V

    const/4 v1, 0x0

    iput v1, p0, Lcom/android/email/NotificationController;->mVipNotifiedMessageCount:I

    const-wide/16 v1, 0x0

    iput-wide v1, p0, Lcom/android/email/NotificationController;->mVipNotifiedMessageId:J

    sget-object v1, Lcom/android/email/NotificationController;->sInstance:Lcom/android/email/NotificationController;

    iget-object v1, v1, Lcom/android/email/NotificationController;->mNotificationManager:Landroid/app/NotificationManager;

    invoke-virtual {v1, v4}, Landroid/app/NotificationManager;->cancel(I)V

    goto :goto_0
.end method

.method setContext(Landroid/content/Context;)V
    .locals 0
    .param p1    # Landroid/content/Context;
    .annotation build Lcom/google/common/annotations/VisibleForTesting;
    .end annotation

    iput-object p1, p0, Lcom/android/email/NotificationController;->mContext:Landroid/content/Context;

    return-void
.end method

.method setupSoundAndVibration(Landroid/app/Notification$Builder;Lcom/android/emailcommon/provider/Account;)V
    .locals 10
    .param p1    # Landroid/app/Notification$Builder;
    .param p2    # Lcom/android/emailcommon/provider/Account;
    .annotation build Lcom/google/common/annotations/VisibleForTesting;
    .end annotation

    const/4 v6, 0x1

    const/4 v7, 0x0

    iget v1, p2, Lcom/android/emailcommon/provider/Account;->mFlags:I

    iget-object v3, p2, Lcom/android/emailcommon/provider/Account;->mRingtoneUri:Ljava/lang/String;

    and-int/lit8 v8, v1, 0x2

    if-eqz v8, :cond_2

    move v4, v6

    :goto_0
    and-int/lit8 v8, v1, 0x40

    if-eqz v8, :cond_3

    move v5, v6

    :goto_1
    invoke-virtual {p0}, Lcom/android/email/NotificationController;->getRingerMode()I

    move-result v8

    const/4 v9, 0x2

    if-eq v8, v9, :cond_4

    move v2, v6

    :goto_2
    const-string v6, "Email"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "[Notification] setupSoundAndVibration vibrate is "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v4}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Lcom/android/emailcommon/Logging;->d(Ljava/lang/String;Ljava/lang/String;)V

    const/4 v0, 0x4

    if-nez v4, :cond_0

    if-eqz v5, :cond_1

    if-eqz v2, :cond_1

    :cond_0
    or-int/lit8 v0, v0, 0x2

    :cond_1
    const-string v6, "Email"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "[Notification] setupSoundAndVibration default flag "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Lcom/android/emailcommon/Logging;->d(Ljava/lang/String;Ljava/lang/String;)V

    invoke-static {v3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v6

    if-eqz v6, :cond_5

    const/4 v6, 0x0

    :goto_3
    invoke-virtual {p1, v6}, Landroid/app/Notification$Builder;->setSound(Landroid/net/Uri;)Landroid/app/Notification$Builder;

    move-result-object v6

    invoke-virtual {v6, v0}, Landroid/app/Notification$Builder;->setDefaults(I)Landroid/app/Notification$Builder;

    return-void

    :cond_2
    move v4, v7

    goto :goto_0

    :cond_3
    move v5, v7

    goto :goto_1

    :cond_4
    move v2, v7

    goto :goto_2

    :cond_5
    invoke-static {v3}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v6

    goto :goto_3
.end method

.method public showDownloadForwardFailedNotification(Lcom/android/emailcommon/provider/EmailContent$Attachment;)V
    .locals 7
    .param p1    # Lcom/android/emailcommon/provider/EmailContent$Attachment;

    iget-object v0, p0, Lcom/android/email/NotificationController;->mContext:Landroid/content/Context;

    iget-wide v2, p1, Lcom/android/emailcommon/provider/EmailContent$Attachment;->mAccountKey:J

    invoke-static {v0, v2, v3}, Lcom/android/emailcommon/provider/Account;->restoreAccountWithId(Landroid/content/Context;J)Lcom/android/emailcommon/provider/Account;

    move-result-object v1

    if-nez v1, :cond_0

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/android/email/NotificationController;->mContext:Landroid/content/Context;

    const v2, 0x7f08010f

    invoke-virtual {v0, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    iget-object v0, p0, Lcom/android/email/NotificationController;->mContext:Landroid/content/Context;

    const v3, 0x7f080110

    invoke-virtual {v0, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v3

    iget-object v4, p1, Lcom/android/emailcommon/provider/EmailContent$Attachment;->mFileName:Ljava/lang/String;

    const/4 v5, 0x0

    const/4 v6, 0x3

    move-object v0, p0

    invoke-direct/range {v0 .. v6}, Lcom/android/email/NotificationController;->showAccountNotification(Lcom/android/emailcommon/provider/Account;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Landroid/content/Intent;I)V

    goto :goto_0
.end method

.method public showLoginFailedNotification(J)V
    .locals 7
    .param p1    # J

    iget-object v0, p0, Lcom/android/email/NotificationController;->mContext:Landroid/content/Context;

    invoke-static {v0, p1, p2}, Lcom/android/emailcommon/provider/Account;->restoreAccountWithId(Landroid/content/Context;J)Lcom/android/emailcommon/provider/Account;

    move-result-object v1

    if-nez v1, :cond_0

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/android/email/NotificationController;->mContext:Landroid/content/Context;

    const v2, 0x7f080111

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    iget-object v5, v1, Lcom/android/emailcommon/provider/Account;->mDisplayName:Ljava/lang/String;

    aput-object v5, v3, v4

    invoke-virtual {v0, v2, v3}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    iget-object v0, p0, Lcom/android/email/NotificationController;->mContext:Landroid/content/Context;

    const v3, 0x7f080112

    invoke-virtual {v0, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1}, Lcom/android/emailcommon/provider/Account;->getDisplayName()Ljava/lang/String;

    move-result-object v4

    iget-object v0, p0, Lcom/android/email/NotificationController;->mContext:Landroid/content/Context;

    iget-object v5, v1, Lcom/android/emailcommon/provider/Account;->mDisplayName:Ljava/lang/String;

    invoke-static {v0, p1, p2, v5}, Lcom/android/email/activity/setup/AccountSettings;->createAccountSettingsIntent(Landroid/content/Context;JLjava/lang/String;)Landroid/content/Intent;

    move-result-object v5

    invoke-direct {p0, p1, p2}, Lcom/android/email/NotificationController;->getLoginFailedNotificationId(J)I

    move-result v6

    move-object v0, p0

    invoke-direct/range {v0 .. v6}, Lcom/android/email/NotificationController;->showAccountNotification(Lcom/android/emailcommon/provider/Account;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Landroid/content/Intent;I)V

    goto :goto_0
.end method

.method public showPasswordExpiredNotification(J)V
    .locals 7
    .param p1    # J

    iget-object v0, p0, Lcom/android/email/NotificationController;->mContext:Landroid/content/Context;

    invoke-static {v0, p1, p2}, Lcom/android/emailcommon/provider/Account;->restoreAccountWithId(Landroid/content/Context;J)Lcom/android/emailcommon/provider/Account;

    move-result-object v1

    if-nez v1, :cond_0

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/android/email/NotificationController;->mContext:Landroid/content/Context;

    const/4 v6, 0x1

    invoke-static {v0, p1, p2, v6}, Lcom/android/email/activity/setup/AccountSecurity;->actionDevicePasswordExpirationIntent(Landroid/content/Context;JZ)Landroid/content/Intent;

    move-result-object v5

    invoke-virtual {v1}, Lcom/android/emailcommon/provider/Account;->getDisplayName()Ljava/lang/String;

    move-result-object v4

    iget-object v0, p0, Lcom/android/email/NotificationController;->mContext:Landroid/content/Context;

    const v6, 0x7f080191

    invoke-virtual {v0, v6}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    iget-object v0, p0, Lcom/android/email/NotificationController;->mContext:Landroid/content/Context;

    const v6, 0x7f080192

    invoke-virtual {v0, v6}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v3

    const/4 v6, 0x5

    move-object v0, p0

    invoke-direct/range {v0 .. v6}, Lcom/android/email/NotificationController;->showAccountNotification(Lcom/android/emailcommon/provider/Account;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Landroid/content/Intent;I)V

    goto :goto_0
.end method

.method public showPasswordExpiringNotification(J)V
    .locals 9
    .param p1    # J

    const/4 v8, 0x0

    iget-object v0, p0, Lcom/android/email/NotificationController;->mContext:Landroid/content/Context;

    invoke-static {v0, p1, p2}, Lcom/android/emailcommon/provider/Account;->restoreAccountWithId(Landroid/content/Context;J)Lcom/android/emailcommon/provider/Account;

    move-result-object v1

    if-nez v1, :cond_0

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/android/email/NotificationController;->mContext:Landroid/content/Context;

    invoke-static {v0, p1, p2, v8}, Lcom/android/email/activity/setup/AccountSecurity;->actionDevicePasswordExpirationIntent(Landroid/content/Context;JZ)Landroid/content/Intent;

    move-result-object v5

    invoke-virtual {v1}, Lcom/android/emailcommon/provider/Account;->getDisplayName()Ljava/lang/String;

    move-result-object v4

    iget-object v0, p0, Lcom/android/email/NotificationController;->mContext:Landroid/content/Context;

    const v6, 0x7f08018f

    const/4 v7, 0x1

    new-array v7, v7, [Ljava/lang/Object;

    aput-object v4, v7, v8

    invoke-virtual {v0, v6, v7}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    iget-object v0, p0, Lcom/android/email/NotificationController;->mContext:Landroid/content/Context;

    const v6, 0x7f080190

    invoke-virtual {v0, v6}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v3

    const/4 v6, 0x4

    move-object v0, p0

    invoke-direct/range {v0 .. v6}, Lcom/android/email/NotificationController;->showAccountNotification(Lcom/android/emailcommon/provider/Account;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Landroid/content/Intent;I)V

    goto :goto_0
.end method

.method public showSecurityNeededNotification(Lcom/android/emailcommon/provider/Account;)V
    .locals 9
    .param p1    # Lcom/android/emailcommon/provider/Account;

    const/4 v6, 0x1

    iget-object v0, p0, Lcom/android/email/NotificationController;->mContext:Landroid/content/Context;

    iget-wide v7, p1, Lcom/android/emailcommon/provider/EmailContent;->mId:J

    invoke-static {v0, v7, v8, v6}, Lcom/android/email/activity/setup/AccountSecurity;->actionUpdateSecurityIntent(Landroid/content/Context;JZ)Landroid/content/Intent;

    move-result-object v5

    invoke-virtual {p1}, Lcom/android/emailcommon/provider/Account;->getDisplayName()Ljava/lang/String;

    move-result-object v4

    iget-object v0, p0, Lcom/android/email/NotificationController;->mContext:Landroid/content/Context;

    const v1, 0x7f08018a

    new-array v7, v6, [Ljava/lang/Object;

    const/4 v8, 0x0

    aput-object v4, v7, v8

    invoke-virtual {v0, v1, v7}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    iget-object v0, p0, Lcom/android/email/NotificationController;->mContext:Landroid/content/Context;

    const v1, 0x7f08018b

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v3

    move-object v0, p0

    move-object v1, p1

    invoke-direct/range {v0 .. v6}, Lcom/android/email/NotificationController;->showAccountNotification(Lcom/android/emailcommon/provider/Account;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Landroid/content/Intent;I)V

    return-void
.end method

.method public showSendFailedNotification(J)V
    .locals 7
    .param p1    # J

    iget-object v0, p0, Lcom/android/email/NotificationController;->mContext:Landroid/content/Context;

    invoke-static {v0, p1, p2}, Lcom/android/emailcommon/provider/Account;->restoreAccountWithId(Landroid/content/Context;J)Lcom/android/emailcommon/provider/Account;

    move-result-object v1

    if-nez v1, :cond_0

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/android/email/NotificationController;->mContext:Landroid/content/Context;

    const v2, 0x7f080023

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    iget-object v5, v1, Lcom/android/emailcommon/provider/Account;->mDisplayName:Ljava/lang/String;

    aput-object v5, v3, v4

    invoke-virtual {v0, v2, v3}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    iget-object v0, p0, Lcom/android/email/NotificationController;->mContext:Landroid/content/Context;

    const v3, 0x7f080024

    invoke-virtual {v0, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1}, Lcom/android/emailcommon/provider/Account;->getDisplayName()Ljava/lang/String;

    move-result-object v4

    iget-object v0, p0, Lcom/android/email/NotificationController;->mContext:Landroid/content/Context;

    invoke-static {v0, p1, p2}, Lcom/android/email/activity/Welcome;->createOpenAccountInboxIntent(Landroid/content/Context;J)Landroid/content/Intent;

    move-result-object v5

    invoke-direct {p0, p1, p2}, Lcom/android/email/NotificationController;->getSendFailedNotificationId(J)I

    move-result v6

    move-object v0, p0

    invoke-direct/range {v0 .. v6}, Lcom/android/email/NotificationController;->showAccountNotification(Lcom/android/emailcommon/provider/Account;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Landroid/content/Intent;I)V

    goto :goto_0
.end method

.method public suspendMessageNotification(ZJ)V
    .locals 4
    .param p1    # Z
    .param p2    # J

    const-wide/16 v2, -0x1

    iget-wide v0, p0, Lcom/android/email/NotificationController;->mSuspendAccountId:J

    cmp-long v0, v0, v2

    if-eqz v0, :cond_0

    iput-wide v2, p0, Lcom/android/email/NotificationController;->mSuspendAccountId:J

    :cond_0
    if-eqz p1, :cond_1

    cmp-long v0, p2, v2

    if-eqz v0, :cond_1

    const-wide/16 v0, 0x0

    cmp-long v0, p2, v0

    if-lez v0, :cond_1

    iput-wide p2, p0, Lcom/android/email/NotificationController;->mSuspendAccountId:J

    const-wide/high16 v0, 0x1000000000000000L

    cmp-long v0, p2, v0

    if-nez v0, :cond_2

    invoke-static {}, Lcom/android/email/NotificationController;->ensureHandlerExists()V

    sget-object v0, Lcom/android/email/NotificationController;->sNotificationHandler:Landroid/os/Handler;

    new-instance v1, Lcom/android/email/NotificationController$2;

    invoke-direct {v1, p0}, Lcom/android/email/NotificationController$2;-><init>(Lcom/android/email/NotificationController;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    :goto_0
    const/4 v0, 0x1

    invoke-virtual {p0, v0, p2, p3}, Lcom/android/email/NotificationController;->postVipMailNotification(ZJ)V

    :cond_1
    return-void

    :cond_2
    iget-object v0, p0, Lcom/android/email/NotificationController;->mNotificationManager:Landroid/app/NotificationManager;

    invoke-direct {p0, p2, p3}, Lcom/android/email/NotificationController;->getNewMessageNotificationId(J)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/app/NotificationManager;->cancel(I)V

    goto :goto_0
.end method

.method public watchForMessages(Z)V
    .locals 3
    .param p1    # Z

    sget-boolean v0, Lcom/android/email/Email;->DEBUG:Z

    if-eqz v0, :cond_0

    const-string v0, "Email"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Notifications being toggled: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    if-nez p1, :cond_1

    sget-object v0, Lcom/android/email/NotificationController;->sNotificationThread:Lcom/android/email/NotificationController$NotificationThread;

    if-nez v0, :cond_1

    :goto_0
    return-void

    :cond_1
    invoke-static {}, Lcom/android/email/NotificationController;->ensureHandlerExists()V

    sget-object v0, Lcom/android/email/NotificationController;->sNotificationHandler:Landroid/os/Handler;

    new-instance v1, Lcom/android/email/NotificationController$1;

    invoke-direct {v1, p0, p1}, Lcom/android/email/NotificationController$1;-><init>(Lcom/android/email/NotificationController;Z)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    goto :goto_0
.end method
