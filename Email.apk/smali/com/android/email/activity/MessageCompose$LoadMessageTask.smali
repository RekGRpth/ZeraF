.class Lcom/android/email/activity/MessageCompose$LoadMessageTask;
.super Lcom/android/emailcommon/utility/EmailAsyncTask;
.source "MessageCompose.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/email/activity/MessageCompose;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "LoadMessageTask"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/android/emailcommon/utility/EmailAsyncTask",
        "<",
        "Ljava/lang/Void;",
        "Ljava/lang/Void;",
        "[",
        "Ljava/lang/Object;",
        ">;"
    }
.end annotation


# instance fields
.field private final mCallback:Lcom/android/email/activity/MessageCompose$OnMessageLoadHandler;

.field private mMessageId:J

.field private final mSaveTask:Lcom/android/email/activity/MessageCompose$SendOrSaveMessageTask;

.field final synthetic this$0:Lcom/android/email/activity/MessageCompose;


# direct methods
.method public constructor <init>(Lcom/android/email/activity/MessageCompose;JLcom/android/email/activity/MessageCompose$SendOrSaveMessageTask;Lcom/android/email/activity/MessageCompose$OnMessageLoadHandler;)V
    .locals 1
    .param p2    # J
    .param p4    # Lcom/android/email/activity/MessageCompose$SendOrSaveMessageTask;
    .param p5    # Lcom/android/email/activity/MessageCompose$OnMessageLoadHandler;

    iput-object p1, p0, Lcom/android/email/activity/MessageCompose$LoadMessageTask;->this$0:Lcom/android/email/activity/MessageCompose;

    invoke-static {p1}, Lcom/android/email/activity/MessageCompose;->access$2700(Lcom/android/email/activity/MessageCompose;)Lcom/android/emailcommon/utility/EmailAsyncTask$Tracker;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/android/emailcommon/utility/EmailAsyncTask;-><init>(Lcom/android/emailcommon/utility/EmailAsyncTask$Tracker;)V

    iput-wide p2, p0, Lcom/android/email/activity/MessageCompose$LoadMessageTask;->mMessageId:J

    iput-object p4, p0, Lcom/android/email/activity/MessageCompose$LoadMessageTask;->mSaveTask:Lcom/android/email/activity/MessageCompose$SendOrSaveMessageTask;

    iput-object p5, p0, Lcom/android/email/activity/MessageCompose$LoadMessageTask;->mCallback:Lcom/android/email/activity/MessageCompose$OnMessageLoadHandler;

    return-void
.end method

.method private getIdToLoad()J
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/InterruptedException;,
            Ljava/util/concurrent/ExecutionException;
        }
    .end annotation

    iget-wide v0, p0, Lcom/android/email/activity/MessageCompose$LoadMessageTask;->mMessageId:J

    const-wide/16 v2, -0x1

    cmp-long v0, v0, v2

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/android/email/activity/MessageCompose$LoadMessageTask;->mSaveTask:Lcom/android/email/activity/MessageCompose$SendOrSaveMessageTask;

    invoke-virtual {v0}, Lcom/android/emailcommon/utility/EmailAsyncTask;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/android/email/activity/MessageCompose$LoadMessageTask;->mMessageId:J

    :cond_0
    iget-wide v0, p0, Lcom/android/email/activity/MessageCompose$LoadMessageTask;->mMessageId:J

    return-wide v0
.end method


# virtual methods
.method protected bridge synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1
    .param p1    # [Ljava/lang/Object;

    check-cast p1, [Ljava/lang/Void;

    invoke-virtual {p0, p1}, Lcom/android/email/activity/MessageCompose$LoadMessageTask;->doInBackground([Ljava/lang/Void;)[Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method protected varargs doInBackground([Ljava/lang/Void;)[Ljava/lang/Object;
    .locals 12
    .param p1    # [Ljava/lang/Void;

    const/4 v8, 0x0

    const-string v9, ">>>> EmailAsyncTask#excuteParallel LoadMessageTask#doInBackground"

    invoke-static {v9}, Lcom/android/emailcommon/Logging;->d(Ljava/lang/String;)V

    :try_start_0
    invoke-direct {p0}, Lcom/android/email/activity/MessageCompose$LoadMessageTask;->getIdToLoad()J
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/util/concurrent/ExecutionException; {:try_start_0 .. :try_end_0} :catch_1

    move-result-wide v6

    iget-object v9, p0, Lcom/android/email/activity/MessageCompose$LoadMessageTask;->this$0:Lcom/android/email/activity/MessageCompose;

    invoke-static {v9, v6, v7}, Lcom/android/emailcommon/provider/EmailContent$Message;->restoreMessageWithId(Landroid/content/Context;J)Lcom/android/emailcommon/provider/EmailContent$Message;

    move-result-object v5

    if-nez v5, :cond_0

    :goto_0
    return-object v8

    :catch_0
    move-exception v4

    const-string v9, "MessageCompose"

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string v11, "Unable to load draft message since existing save task failed: "

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-static {v9, v10}, Lcom/android/emailcommon/Logging;->e(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    :catch_1
    move-exception v4

    const-string v9, "MessageCompose"

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string v11, "Unable to load draft message since existing save task failed: "

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-static {v9, v10}, Lcom/android/emailcommon/Logging;->e(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    :cond_0
    iget-wide v1, v5, Lcom/android/emailcommon/provider/EmailContent$Message;->mAccountKey:J

    iget-object v9, p0, Lcom/android/email/activity/MessageCompose$LoadMessageTask;->this$0:Lcom/android/email/activity/MessageCompose;

    invoke-static {v9, v1, v2}, Lcom/android/emailcommon/provider/Account;->restoreAccountWithId(Landroid/content/Context;J)Lcom/android/emailcommon/provider/Account;

    move-result-object v0

    :try_start_1
    iget-object v9, p0, Lcom/android/email/activity/MessageCompose$LoadMessageTask;->this$0:Lcom/android/email/activity/MessageCompose;

    iget-wide v10, v5, Lcom/android/emailcommon/provider/EmailContent;->mId:J

    invoke-static {v9, v10, v11}, Lcom/android/emailcommon/provider/EmailContent$Body;->restoreBodyWithMessageId(Landroid/content/Context;J)Lcom/android/emailcommon/provider/EmailContent$Body;
    :try_end_1
    .catch Ljava/lang/RuntimeException; {:try_start_1 .. :try_end_1} :catch_2

    move-result-object v3

    const-string v8, "<<<< EmailAsyncTask#excuteParallel LoadMessageTask#doInBackground"

    invoke-static {v8}, Lcom/android/emailcommon/Logging;->d(Ljava/lang/String;)V

    const/4 v8, 0x3

    new-array v8, v8, [Ljava/lang/Object;

    const/4 v9, 0x0

    aput-object v5, v8, v9

    const/4 v9, 0x1

    aput-object v3, v8, v9

    const/4 v9, 0x2

    aput-object v0, v8, v9

    goto :goto_0

    :catch_2
    move-exception v4

    const-string v9, "MessageCompose"

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string v11, "Exception while loading message body: "

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-static {v9, v10}, Lcom/android/emailcommon/Logging;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method protected bridge synthetic onSuccess(Ljava/lang/Object;)V
    .locals 0
    .param p1    # Ljava/lang/Object;

    check-cast p1, [Ljava/lang/Object;

    invoke-virtual {p0, p1}, Lcom/android/email/activity/MessageCompose$LoadMessageTask;->onSuccess([Ljava/lang/Object;)V

    return-void
.end method

.method protected onSuccess([Ljava/lang/Object;)V
    .locals 6
    .param p1    # [Ljava/lang/Object;

    const/4 v5, 0x1

    if-eqz p1, :cond_0

    array-length v3, p1

    const/4 v4, 0x3

    if-eq v3, v4, :cond_1

    :cond_0
    iget-object v3, p0, Lcom/android/email/activity/MessageCompose$LoadMessageTask;->mCallback:Lcom/android/email/activity/MessageCompose$OnMessageLoadHandler;

    invoke-interface {v3}, Lcom/android/email/activity/MessageCompose$OnMessageLoadHandler;->onLoadFailed()V

    :goto_0
    return-void

    :cond_1
    const/4 v3, 0x0

    aget-object v2, p1, v3

    check-cast v2, Lcom/android/emailcommon/provider/EmailContent$Message;

    aget-object v1, p1, v5

    check-cast v1, Lcom/android/emailcommon/provider/EmailContent$Body;

    const/4 v3, 0x2

    aget-object v0, p1, v3

    check-cast v0, Lcom/android/emailcommon/provider/Account;

    if-eqz v2, :cond_2

    if-eqz v1, :cond_2

    if-nez v0, :cond_3

    :cond_2
    iget-object v3, p0, Lcom/android/email/activity/MessageCompose$LoadMessageTask;->mCallback:Lcom/android/email/activity/MessageCompose$OnMessageLoadHandler;

    invoke-interface {v3}, Lcom/android/email/activity/MessageCompose$OnMessageLoadHandler;->onLoadFailed()V

    goto :goto_0

    :cond_3
    iget-object v3, p0, Lcom/android/email/activity/MessageCompose$LoadMessageTask;->this$0:Lcom/android/email/activity/MessageCompose;

    invoke-static {v3, v0}, Lcom/android/email/activity/MessageCompose;->access$2800(Lcom/android/email/activity/MessageCompose;Lcom/android/emailcommon/provider/Account;)V

    iget-object v3, p0, Lcom/android/email/activity/MessageCompose$LoadMessageTask;->mCallback:Lcom/android/email/activity/MessageCompose$OnMessageLoadHandler;

    invoke-interface {v3, v2, v1}, Lcom/android/email/activity/MessageCompose$OnMessageLoadHandler;->onMessageLoaded(Lcom/android/emailcommon/provider/EmailContent$Message;Lcom/android/emailcommon/provider/EmailContent$Body;)V

    iget-object v3, p0, Lcom/android/email/activity/MessageCompose$LoadMessageTask;->this$0:Lcom/android/email/activity/MessageCompose;

    invoke-static {v3, v5}, Lcom/android/email/activity/MessageCompose;->access$2900(Lcom/android/email/activity/MessageCompose;Z)V

    goto :goto_0
.end method
