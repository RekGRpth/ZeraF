.class public Lcom/android/email/activity/MessageFileViewFragment;
.super Lcom/android/email/activity/MessageViewFragmentBase;
.source "MessageFileViewFragment.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/email/activity/MessageFileViewFragment$1;,
        Lcom/android/email/activity/MessageFileViewFragment$MessageViewHandler;,
        Lcom/android/email/activity/MessageFileViewFragment$MessageFileViewCallback;,
        Lcom/android/email/activity/MessageFileViewFragment$OpenFileMessageCallback;,
        Lcom/android/email/activity/MessageFileViewFragment$LoadingFileMessageProgressDialog;
    }
.end annotation


# static fields
.field public static final MSG_PARSE_MESSAGE:I = 0x1

.field public static final MSG_START_LOADING:I = 0x0

.field public static final MSG_UPDATE_DATABASE:I = 0x2

.field public static final MSG_UPDATE_UI:I = 0x3

.field private static final TAG:Ljava/lang/String; = "MessageFileViewFragment"

.field private static sFragmentCount:I


# instance fields
.field private mCallBack:Lcom/android/email/activity/MessageFileViewFragment$OpenFileMessageCallback;

.field private mFileEmailUri:Landroid/net/Uri;

.field private mHandler:Lcom/android/email/activity/MessageFileViewFragment$MessageViewHandler;

.field private mIsLoadingFinished:Z

.field private mProgressDialog:Lcom/android/email/activity/MessageFileViewFragment$LoadingFileMessageProgressDialog;


# direct methods
.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Lcom/android/email/activity/MessageViewFragmentBase;-><init>()V

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/android/email/activity/MessageFileViewFragment;->mIsLoadingFinished:Z

    return-void
.end method

.method static synthetic access$100(Lcom/android/email/activity/MessageFileViewFragment;)Z
    .locals 1
    .param p0    # Lcom/android/email/activity/MessageFileViewFragment;

    iget-boolean v0, p0, Lcom/android/email/activity/MessageFileViewFragment;->mIsLoadingFinished:Z

    return v0
.end method

.method static synthetic access$200(Lcom/android/email/activity/MessageFileViewFragment;)Lcom/android/email/activity/MessageFileViewFragment$MessageViewHandler;
    .locals 1
    .param p0    # Lcom/android/email/activity/MessageFileViewFragment;

    iget-object v0, p0, Lcom/android/email/activity/MessageFileViewFragment;->mHandler:Lcom/android/email/activity/MessageFileViewFragment$MessageViewHandler;

    return-object v0
.end method

.method static synthetic access$300(Lcom/android/email/activity/MessageFileViewFragment;I)V
    .locals 0
    .param p0    # Lcom/android/email/activity/MessageFileViewFragment;
    .param p1    # I

    invoke-direct {p0, p1}, Lcom/android/email/activity/MessageFileViewFragment;->reportProgress(I)V

    return-void
.end method

.method private finish()V
    .locals 1

    invoke-direct {p0}, Lcom/android/email/activity/MessageFileViewFragment;->releaseProgressDialog()V

    invoke-super {p0}, Lcom/android/email/activity/MessageViewFragmentBase;->getCallback()Lcom/android/email/activity/MessageViewFragmentBase$Callback;

    move-result-object v0

    invoke-interface {v0}, Lcom/android/email/activity/MessageViewFragmentBase$Callback;->onMessageNotExists()V

    return-void
.end method

.method private releaseProgressDialog()V
    .locals 1

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/android/email/activity/MessageFileViewFragment;->mIsLoadingFinished:Z

    iget-object v0, p0, Lcom/android/email/activity/MessageFileViewFragment;->mProgressDialog:Lcom/android/email/activity/MessageFileViewFragment$LoadingFileMessageProgressDialog;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/email/activity/MessageFileViewFragment;->mProgressDialog:Lcom/android/email/activity/MessageFileViewFragment$LoadingFileMessageProgressDialog;

    invoke-virtual {v0}, Landroid/app/DialogFragment;->dismissAllowingStateLoss()V

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/android/email/activity/MessageFileViewFragment;->mProgressDialog:Lcom/android/email/activity/MessageFileViewFragment$LoadingFileMessageProgressDialog;

    :cond_0
    return-void
.end method

.method private reportProgress(I)V
    .locals 4
    .param p1    # I

    invoke-virtual {p0}, Landroid/app/Fragment;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v0

    if-eqz v0, :cond_1

    const-string v1, "LoadingFileMessageProgressDialog"

    invoke-virtual {v0, v1}, Landroid/app/FragmentManager;->findFragmentByTag(Ljava/lang/String;)Landroid/app/Fragment;

    move-result-object v1

    check-cast v1, Lcom/android/email/activity/MessageFileViewFragment$LoadingFileMessageProgressDialog;

    iput-object v1, p0, Lcom/android/email/activity/MessageFileViewFragment;->mProgressDialog:Lcom/android/email/activity/MessageFileViewFragment$LoadingFileMessageProgressDialog;

    iget-object v1, p0, Lcom/android/email/activity/MessageFileViewFragment;->mProgressDialog:Lcom/android/email/activity/MessageFileViewFragment$LoadingFileMessageProgressDialog;

    if-nez v1, :cond_0

    invoke-virtual {p0}, Landroid/app/Fragment;->getActivity()Landroid/app/Activity;

    move-result-object v1

    const/4 v2, 0x0

    invoke-static {v1, v2}, Lcom/android/email/activity/MessageFileViewFragment$LoadingFileMessageProgressDialog;->newInstance(Landroid/app/Activity;Landroid/app/Fragment;)Lcom/android/email/activity/MessageFileViewFragment$LoadingFileMessageProgressDialog;

    move-result-object v1

    iput-object v1, p0, Lcom/android/email/activity/MessageFileViewFragment;->mProgressDialog:Lcom/android/email/activity/MessageFileViewFragment$LoadingFileMessageProgressDialog;

    invoke-virtual {v0}, Landroid/app/FragmentManager;->beginTransaction()Landroid/app/FragmentTransaction;

    move-result-object v1

    iget-object v2, p0, Lcom/android/email/activity/MessageFileViewFragment;->mProgressDialog:Lcom/android/email/activity/MessageFileViewFragment$LoadingFileMessageProgressDialog;

    const-string v3, "LoadingFileMessageProgressDialog"

    invoke-virtual {v1, v2, v3}, Landroid/app/FragmentTransaction;->add(Landroid/app/Fragment;Ljava/lang/String;)Landroid/app/FragmentTransaction;

    move-result-object v1

    invoke-virtual {v1}, Landroid/app/FragmentTransaction;->commitAllowingStateLoss()I

    invoke-virtual {v0}, Landroid/app/FragmentManager;->executePendingTransactions()Z

    :goto_0
    return-void

    :cond_0
    iget-object v1, p0, Lcom/android/email/activity/MessageFileViewFragment;->mProgressDialog:Lcom/android/email/activity/MessageFileViewFragment$LoadingFileMessageProgressDialog;

    invoke-virtual {v1, p1}, Lcom/android/email/activity/MessageFileViewFragment$LoadingFileMessageProgressDialog;->updateProgress(I)V

    goto :goto_0

    :cond_1
    const-string v1, "MessageFileViewFragment"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "reportProgress failed : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/android/emailcommon/Logging;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method private showLoadAttachmentProgressDialog(Landroid/app/Activity;)V
    .locals 4
    .param p1    # Landroid/app/Activity;

    invoke-virtual {p0}, Landroid/app/Fragment;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v0

    invoke-static {p1, p0}, Lcom/android/email/activity/MessageFileViewFragment$LoadingFileMessageProgressDialog;->newInstance(Landroid/app/Activity;Landroid/app/Fragment;)Lcom/android/email/activity/MessageFileViewFragment$LoadingFileMessageProgressDialog;

    move-result-object v1

    iput-object v1, p0, Lcom/android/email/activity/MessageFileViewFragment;->mProgressDialog:Lcom/android/email/activity/MessageFileViewFragment$LoadingFileMessageProgressDialog;

    invoke-virtual {v0}, Landroid/app/FragmentManager;->beginTransaction()Landroid/app/FragmentTransaction;

    move-result-object v1

    iget-object v2, p0, Lcom/android/email/activity/MessageFileViewFragment;->mProgressDialog:Lcom/android/email/activity/MessageFileViewFragment$LoadingFileMessageProgressDialog;

    const-string v3, "LoadingFileMessageProgressDialog"

    invoke-virtual {v1, v2, v3}, Landroid/app/FragmentTransaction;->add(Landroid/app/Fragment;Ljava/lang/String;)Landroid/app/FragmentTransaction;

    move-result-object v1

    invoke-virtual {v1}, Landroid/app/FragmentTransaction;->commitAllowingStateLoss()I

    return-void
.end method


# virtual methods
.method public onActivityCreated(Landroid/os/Bundle;)V
    .locals 1
    .param p1    # Landroid/os/Bundle;

    invoke-super {p0, p1}, Lcom/android/email/activity/MessageViewFragmentBase;->onActivityCreated(Landroid/os/Bundle;)V

    iget-object v0, p0, Lcom/android/email/activity/MessageFileViewFragment;->mFileEmailUri:Landroid/net/Uri;

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/IllegalStateException;

    invoke-direct {v0}, Ljava/lang/IllegalStateException;-><init>()V

    throw v0

    :cond_0
    return-void
.end method

.method protected onCheckingDialogCancel()V
    .locals 2

    const-string v0, "MessageFileViewFragment"

    const-string v1, "User canceled the loading dialog ..."

    invoke-static {v0, v1}, Lcom/android/emailcommon/Logging;->d(Ljava/lang/String;Ljava/lang/String;)V

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/android/email/activity/MessageFileViewFragment;->mIsLoadingFinished:Z

    invoke-direct {p0}, Lcom/android/email/activity/MessageFileViewFragment;->finish()V

    return-void
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 2
    .param p1    # Landroid/os/Bundle;

    invoke-super {p0, p1}, Lcom/android/email/activity/MessageViewFragmentBase;->onCreate(Landroid/os/Bundle;)V

    sget v0, Lcom/android/email/activity/MessageFileViewFragment;->sFragmentCount:I

    add-int/lit8 v0, v0, 0x1

    sput v0, Lcom/android/email/activity/MessageFileViewFragment;->sFragmentCount:I

    new-instance v0, Lcom/android/email/activity/MessageFileViewFragment$MessageFileViewCallback;

    invoke-direct {v0, p0}, Lcom/android/email/activity/MessageFileViewFragment$MessageFileViewCallback;-><init>(Lcom/android/email/activity/MessageFileViewFragment;)V

    iput-object v0, p0, Lcom/android/email/activity/MessageFileViewFragment;->mCallBack:Lcom/android/email/activity/MessageFileViewFragment$OpenFileMessageCallback;

    new-instance v0, Lcom/android/email/activity/MessageFileViewFragment$MessageViewHandler;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/android/email/activity/MessageFileViewFragment$MessageViewHandler;-><init>(Lcom/android/email/activity/MessageFileViewFragment;Lcom/android/email/activity/MessageFileViewFragment$1;)V

    iput-object v0, p0, Lcom/android/email/activity/MessageFileViewFragment;->mHandler:Lcom/android/email/activity/MessageFileViewFragment$MessageViewHandler;

    return-void
.end method

.method public onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 3
    .param p1    # Landroid/view/LayoutInflater;
    .param p2    # Landroid/view/ViewGroup;
    .param p3    # Landroid/os/Bundle;

    const/16 v2, 0x8

    invoke-super {p0, p1, p2, p3}, Lcom/android/email/activity/MessageViewFragmentBase;->onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;

    move-result-object v0

    const v1, 0x7f0f00d4

    invoke-static {v0, v1, v2}, Lcom/android/email/activity/UiUtilities;->setVisibilitySafe(Landroid/view/View;II)V

    const v1, 0x7f0f0014

    invoke-static {v0, v1, v2}, Lcom/android/email/activity/UiUtilities;->setVisibilitySafe(Landroid/view/View;II)V

    const v1, 0x7f0f00d5

    invoke-static {v0, v1, v2}, Lcom/android/email/activity/UiUtilities;->setVisibilitySafe(Landroid/view/View;II)V

    const v1, 0x7f0f00d7

    invoke-static {v0, v1, v2}, Lcom/android/email/activity/UiUtilities;->setVisibilitySafe(Landroid/view/View;II)V

    const v1, 0x7f0f00d6

    invoke-static {v0, v1, v2}, Lcom/android/email/activity/UiUtilities;->setVisibilitySafe(Landroid/view/View;II)V

    return-object v0
.end method

.method public onDestroy()V
    .locals 2

    const/4 v1, 0x0

    invoke-super {p0}, Lcom/android/email/activity/MessageViewFragmentBase;->onDestroy()V

    sget v0, Lcom/android/email/activity/MessageFileViewFragment;->sFragmentCount:I

    add-int/lit8 v0, v0, -0x1

    sput v0, Lcom/android/email/activity/MessageFileViewFragment;->sFragmentCount:I

    sget v0, Lcom/android/email/activity/MessageFileViewFragment;->sFragmentCount:I

    if-nez v0, :cond_0

    invoke-virtual {p0}, Landroid/app/Fragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/Activity;->isChangingConfigurations()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-virtual {p0}, Lcom/android/email/activity/MessageViewFragmentBase;->getController()Lcom/android/email/Controller;

    move-result-object v0

    invoke-virtual {v0}, Lcom/android/email/Controller;->deleteAttachmentMessages()V

    :cond_0
    iput-object v1, p0, Lcom/android/email/activity/MessageFileViewFragment;->mCallBack:Lcom/android/email/activity/MessageFileViewFragment$OpenFileMessageCallback;

    iput-object v1, p0, Lcom/android/email/activity/MessageFileViewFragment;->mHandler:Lcom/android/email/activity/MessageFileViewFragment$MessageViewHandler;

    return-void
.end method

.method protected openMessageSync(Landroid/app/Activity;)Lcom/android/emailcommon/provider/EmailContent$Message;
    .locals 4
    .param p1    # Landroid/app/Activity;

    sget-boolean v1, Lcom/android/emailcommon/Logging;->DEBUG_LIFECYCLE:Z

    if-eqz v1, :cond_0

    sget-boolean v1, Lcom/android/email/Email;->DEBUG:Z

    if-eqz v1, :cond_0

    const-string v1, "Email"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " openMessageSync"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    invoke-direct {p0, p1}, Lcom/android/email/activity/MessageFileViewFragment;->showLoadAttachmentProgressDialog(Landroid/app/Activity;)V

    invoke-virtual {p0}, Lcom/android/email/activity/MessageViewFragmentBase;->getController()Lcom/android/email/Controller;

    move-result-object v1

    iget-object v2, p0, Lcom/android/email/activity/MessageFileViewFragment;->mFileEmailUri:Landroid/net/Uri;

    iget-object v3, p0, Lcom/android/email/activity/MessageFileViewFragment;->mCallBack:Lcom/android/email/activity/MessageFileViewFragment$OpenFileMessageCallback;

    invoke-virtual {v1, v2, v3}, Lcom/android/email/Controller;->loadMessageFromUri(Landroid/net/Uri;Lcom/android/email/activity/MessageFileViewFragment$OpenFileMessageCallback;)Lcom/android/emailcommon/provider/EmailContent$Message;

    move-result-object v0

    invoke-direct {p0}, Lcom/android/email/activity/MessageFileViewFragment;->releaseProgressDialog()V

    return-object v0
.end method

.method protected reloadMessageSync(Landroid/app/Activity;)Lcom/android/emailcommon/provider/EmailContent$Message;
    .locals 1
    .param p1    # Landroid/app/Activity;

    invoke-virtual {p0}, Lcom/android/email/activity/MessageViewFragmentBase;->getMessage()Lcom/android/emailcommon/provider/EmailContent$Message;

    move-result-object v0

    return-object v0
.end method

.method protected reloadUiFromMessage(Lcom/android/emailcommon/provider/EmailContent$Message;Z)V
    .locals 2
    .param p1    # Lcom/android/emailcommon/provider/EmailContent$Message;
    .param p2    # Z

    iget v0, p1, Lcom/android/emailcommon/provider/EmailContent$Message;->mFlagLoaded:I

    const/4 v1, 0x1

    if-eq v0, v1, :cond_0

    new-instance v0, Ljava/lang/IllegalStateException;

    invoke-direct {v0}, Ljava/lang/IllegalStateException;-><init>()V

    throw v0

    :cond_0
    invoke-super {p0, p1, p2}, Lcom/android/email/activity/MessageViewFragmentBase;->reloadUiFromMessage(Lcom/android/emailcommon/provider/EmailContent$Message;Z)V

    return-void
.end method

.method public setFileUri(Landroid/net/Uri;)V
    .locals 3
    .param p1    # Landroid/net/Uri;

    sget-boolean v0, Lcom/android/emailcommon/Logging;->DEBUG_LIFECYCLE:Z

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/android/email/Email;->DEBUG:Z

    if-eqz v0, :cond_0

    const-string v0, "Email"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " openMessage"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    iget-object v0, p0, Lcom/android/email/activity/MessageFileViewFragment;->mFileEmailUri:Landroid/net/Uri;

    if-eqz v0, :cond_1

    new-instance v0, Ljava/lang/IllegalStateException;

    invoke-direct {v0}, Ljava/lang/IllegalStateException;-><init>()V

    throw v0

    :cond_1
    if-nez p1, :cond_2

    new-instance v0, Ljava/lang/IllegalArgumentException;

    invoke-direct {v0}, Ljava/lang/IllegalArgumentException;-><init>()V

    throw v0

    :cond_2
    iput-object p1, p0, Lcom/android/email/activity/MessageFileViewFragment;->mFileEmailUri:Landroid/net/Uri;

    return-void
.end method
