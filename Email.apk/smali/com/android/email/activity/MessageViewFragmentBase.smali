.class public abstract Lcom/android/email/activity/MessageViewFragmentBase;
.super Landroid/app/Fragment;
.source "MessageViewFragmentBase.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/email/activity/MessageViewFragmentBase$UpdatePreviewIconTask;,
        Lcom/android/email/activity/MessageViewFragmentBase$MessageObserver;,
        Lcom/android/email/activity/MessageViewFragmentBase$ControllerResults;,
        Lcom/android/email/activity/MessageViewFragmentBase$CustomWebViewClient;,
        Lcom/android/email/activity/MessageViewFragmentBase$MessageViewAttachmentInfo;,
        Lcom/android/email/activity/MessageViewFragmentBase$LoadAttachmentsTask;,
        Lcom/android/email/activity/MessageViewFragmentBase$LoadBodyTask;,
        Lcom/android/email/activity/MessageViewFragmentBase$ReloadMessageTask;,
        Lcom/android/email/activity/MessageViewFragmentBase$LoadMessageTask;,
        Lcom/android/email/activity/MessageViewFragmentBase$ContactStatusLoaderCallbacks;,
        Lcom/android/email/activity/MessageViewFragmentBase$EmptyCallback;,
        Lcom/android/email/activity/MessageViewFragmentBase$Callback;
    }
.end annotation


# static fields
.field public static final ARG_MESSAGE_ID:Ljava/lang/String; = "messageId"

.field private static final BUNDLE_KEY_CURRENT_TAB:Ljava/lang/String; = "MessageViewFragmentBase.currentTab"

.field private static final BUNDLE_KEY_MESSAGE_KEY:Ljava/lang/String; = "MessageViewFragmentBase.messageKey"

.field private static final BUNDLE_KEY_PARTIAL_DOWNLOAD:Ljava/lang/String; = "MessageViewFragmentBase.particalDownload"

.field private static final BUNDLE_KEY_PICTURE_LOADED:Ljava/lang/String; = "MessageViewFragmentBase.pictureLoaded"

.field private static final CONTACT_STATUS_STATE_LOADED:I = 0x2

.field private static final CONTACT_STATUS_STATE_UNLOADED:I = 0x0

.field private static final CONTACT_STATUS_STATE_UNLOADED_TRIGGERED:I = 0x1

.field private static final DEFAULT_EMAIL_PREFIX:Ljava/lang/String; = "email://"

.field private static final HTTP_PREFIX:Ljava/lang/String; = "http://"

.field private static final IMG_TAG_START_REGEX:Ljava/util/regex/Pattern;

.field private static final PHOTO_LOADER_ID:I = 0x1

.field private static final PREVIEW_ICON_HEIGHT:I = 0x3e

.field private static final PREVIEW_ICON_WIDTH:I = 0x3e

.field protected static final TAB_ATTACHMENT:I = 0x67

.field protected static final TAB_FLAGS_HAS_ATTACHMENT:I = 0x1

.field protected static final TAB_FLAGS_HAS_INVITE:I = 0x2

.field protected static final TAB_FLAGS_HAS_PICTURES:I = 0x4

.field protected static final TAB_FLAGS_PICTURE_LOADED:I = 0x8

.field protected static final TAB_INVITE:I = 0x66

.field protected static final TAB_MESSAGE:I = 0x65

.field private static final TAB_NONE:I = 0x0

.field private static final TAG:Ljava/lang/String; = "MessageViewFragmentBase"

.field private static final VIEWS_NUMBER_TO_SHOW:I = 0x64

.field public static final VIP_VIEW_PHOTO_LOADER_ID:I = 0x2

.field private static sZoomSizes:[Ljava/lang/String;


# instance fields
.field private mAccountId:J

.field private mAddVipIcon:Landroid/graphics/drawable/Drawable;

.field private mAddressesView:Landroid/widget/TextView;

.field private mAlwaysShowPicturesButton:Landroid/view/View;

.field private mAttachmentCount:I

.field private mAttachmentTab:Landroid/widget/TextView;

.field private mAttachments:Landroid/widget/LinearLayout;

.field private mAttachmentsScroll:Landroid/view/View;

.field private mCallback:Lcom/android/email/activity/MessageViewFragmentBase$Callback;

.field private mContactStatusState:I

.field protected mContext:Landroid/content/Context;

.field private mController:Lcom/android/email/Controller;

.field private mControllerCallback:Lcom/android/email/ControllerResultUiThreadWrapper;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/android/email/ControllerResultUiThreadWrapper",
            "<",
            "Lcom/android/email/activity/MessageViewFragmentBase$ControllerResults;",
            ">;"
        }
    .end annotation
.end field

.field private mCurrentTab:I

.field private mDateTimeView:Landroid/widget/TextView;

.field private mDetailsCollapsed:Landroid/view/View;

.field private mDetailsExpanded:Landroid/view/View;

.field private mDetailsFilled:Z

.field private mDownloadAttachmentList:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation
.end field

.field private mFromAddressView:Landroid/widget/TextView;

.field private mFromBadge:Landroid/widget/ImageView;

.field protected mFromNameView:Landroid/widget/TextView;

.field private mHtmlTextRaw:Ljava/lang/String;

.field private mHtmlTextWebView:Ljava/lang/String;

.field private mInviteScroll:Landroid/view/View;

.field private mInviteTab:Landroid/widget/TextView;

.field private mIsMessageLoadedForTest:Z

.field private mLastZoomSize:I

.field private mLinkParserTask:Lcom/android/emailcommon/utility/LinkParserTask;

.field private mLoadAttachmentsTask:Lcom/android/email/activity/MessageViewFragmentBase$LoadAttachmentsTask;

.field private mLoadBodyTask:Lcom/android/email/activity/MessageViewFragmentBase$LoadBodyTask;

.field private mLoadMessageTask:Lcom/android/email/activity/MessageViewFragmentBase$LoadMessageTask;

.field private mLoadingProgress:Landroid/view/View;

.field private mMainView:Lcom/android/email/view/NonLockingScrollView;

.field private mMessage:Lcom/android/emailcommon/provider/EmailContent$Message;

.field private mMessageContentView:Landroid/webkit/WebView;

.field protected mMessageId:J

.field protected mMessageIsReload:Z

.field private mMessageLoading:Z

.field private mMessageObserver:Lcom/android/email/activity/MessageViewFragmentBase$MessageObserver;

.field private mMessageTab:Landroid/widget/TextView;

.field private mNeedReloadMessageContent:Z

.field private mPartialDownload:Z

.field private mQuickContactLookupUri:Landroid/net/Uri;

.field private mRemainBtnView:Landroid/widget/LinearLayout;

.field private mRemoveVipIcon:Landroid/graphics/drawable/Drawable;

.field private mRenderAttachmentTask:Landroid/os/AsyncTask;

.field private mRestoredPictureLoaded:Z

.field private mRestoredTab:I

.field private mSenderPresenceView:Landroid/widget/ImageView;

.field private mShowPicturesTab:Landroid/widget/TextView;

.field private mSubjectView:Landroid/widget/TextView;

.field private mTabFlags:I

.field private mTabSection:Landroid/view/View;

.field private final mTaskTracker:Lcom/android/emailcommon/utility/EmailAsyncTask$Tracker;

.field private mVipSwitcher:Landroid/widget/ImageView;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const-string v0, "<(?i)img\\s+"

    invoke-static {v0}, Ljava/util/regex/Pattern;->compile(Ljava/lang/String;)Ljava/util/regex/Pattern;

    move-result-object v0

    sput-object v0, Lcom/android/email/activity/MessageViewFragmentBase;->IMG_TAG_START_REGEX:Ljava/util/regex/Pattern;

    const/4 v0, 0x0

    sput-object v0, Lcom/android/email/activity/MessageViewFragmentBase;->sZoomSizes:[Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 4

    const-wide/16 v2, -0x1

    const/4 v1, 0x0

    invoke-direct {p0}, Landroid/app/Fragment;-><init>()V

    iput-wide v2, p0, Lcom/android/email/activity/MessageViewFragmentBase;->mAccountId:J

    iput-wide v2, p0, Lcom/android/email/activity/MessageViewFragmentBase;->mMessageId:J

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/android/email/activity/MessageViewFragmentBase;->mDownloadAttachmentList:Ljava/util/ArrayList;

    iput v1, p0, Lcom/android/email/activity/MessageViewFragmentBase;->mCurrentTab:I

    iput v1, p0, Lcom/android/email/activity/MessageViewFragmentBase;->mRestoredTab:I

    iput-boolean v1, p0, Lcom/android/email/activity/MessageViewFragmentBase;->mNeedReloadMessageContent:Z

    iput-boolean v1, p0, Lcom/android/email/activity/MessageViewFragmentBase;->mPartialDownload:Z

    new-instance v0, Lcom/android/emailcommon/utility/EmailAsyncTask$Tracker;

    invoke-direct {v0}, Lcom/android/emailcommon/utility/EmailAsyncTask$Tracker;-><init>()V

    iput-object v0, p0, Lcom/android/email/activity/MessageViewFragmentBase;->mTaskTracker:Lcom/android/emailcommon/utility/EmailAsyncTask$Tracker;

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/android/email/activity/MessageViewFragmentBase;->mRenderAttachmentTask:Landroid/os/AsyncTask;

    iput-boolean v1, p0, Lcom/android/email/activity/MessageViewFragmentBase;->mMessageLoading:Z

    sget-object v0, Lcom/android/email/activity/MessageViewFragmentBase$EmptyCallback;->INSTANCE:Lcom/android/email/activity/MessageViewFragmentBase$Callback;

    iput-object v0, p0, Lcom/android/email/activity/MessageViewFragmentBase;->mCallback:Lcom/android/email/activity/MessageViewFragmentBase$Callback;

    iput-boolean v1, p0, Lcom/android/email/activity/MessageViewFragmentBase;->mMessageIsReload:Z

    return-void
.end method

.method static synthetic access$1000(Lcom/android/email/activity/MessageViewFragmentBase;)V
    .locals 0
    .param p0    # Lcom/android/email/activity/MessageViewFragmentBase;

    invoke-direct {p0}, Lcom/android/email/activity/MessageViewFragmentBase;->onClickSender()V

    return-void
.end method

.method static synthetic access$1100(Lcom/android/email/activity/MessageViewFragmentBase;Lcom/android/email/activity/MessageViewFragmentBase$MessageViewAttachmentInfo;)V
    .locals 0
    .param p0    # Lcom/android/email/activity/MessageViewFragmentBase;
    .param p1    # Lcom/android/email/activity/MessageViewFragmentBase$MessageViewAttachmentInfo;

    invoke-direct {p0, p1}, Lcom/android/email/activity/MessageViewFragmentBase;->onSaveAttachment(Lcom/android/email/activity/MessageViewFragmentBase$MessageViewAttachmentInfo;)V

    return-void
.end method

.method static synthetic access$1700(Lcom/android/email/activity/MessageViewFragmentBase;)Lcom/android/emailcommon/utility/EmailAsyncTask$Tracker;
    .locals 1
    .param p0    # Lcom/android/email/activity/MessageViewFragmentBase;

    iget-object v0, p0, Lcom/android/email/activity/MessageViewFragmentBase;->mTaskTracker:Lcom/android/emailcommon/utility/EmailAsyncTask$Tracker;

    return-object v0
.end method

.method static synthetic access$1802(Lcom/android/email/activity/MessageViewFragmentBase;Z)Z
    .locals 0
    .param p0    # Lcom/android/email/activity/MessageViewFragmentBase;
    .param p1    # Z

    iput-boolean p1, p0, Lcom/android/email/activity/MessageViewFragmentBase;->mMessageLoading:Z

    return p1
.end method

.method static synthetic access$1902(Lcom/android/email/activity/MessageViewFragmentBase;Lcom/android/email/activity/MessageViewFragmentBase$LoadMessageTask;)Lcom/android/email/activity/MessageViewFragmentBase$LoadMessageTask;
    .locals 0
    .param p0    # Lcom/android/email/activity/MessageViewFragmentBase;
    .param p1    # Lcom/android/email/activity/MessageViewFragmentBase$LoadMessageTask;

    iput-object p1, p0, Lcom/android/email/activity/MessageViewFragmentBase;->mLoadMessageTask:Lcom/android/email/activity/MessageViewFragmentBase$LoadMessageTask;

    return-object p1
.end method

.method static synthetic access$200(Lcom/android/email/activity/MessageViewFragmentBase;)Lcom/android/emailcommon/provider/EmailContent$Message;
    .locals 1
    .param p0    # Lcom/android/email/activity/MessageViewFragmentBase;

    iget-object v0, p0, Lcom/android/email/activity/MessageViewFragmentBase;->mMessage:Lcom/android/emailcommon/provider/EmailContent$Message;

    return-object v0
.end method

.method static synthetic access$2000(Lcom/android/email/activity/MessageViewFragmentBase;)Lcom/android/email/activity/MessageViewFragmentBase$Callback;
    .locals 1
    .param p0    # Lcom/android/email/activity/MessageViewFragmentBase;

    iget-object v0, p0, Lcom/android/email/activity/MessageViewFragmentBase;->mCallback:Lcom/android/email/activity/MessageViewFragmentBase$Callback;

    return-object v0
.end method

.method static synthetic access$202(Lcom/android/email/activity/MessageViewFragmentBase;Lcom/android/emailcommon/provider/EmailContent$Message;)Lcom/android/emailcommon/provider/EmailContent$Message;
    .locals 0
    .param p0    # Lcom/android/email/activity/MessageViewFragmentBase;
    .param p1    # Lcom/android/emailcommon/provider/EmailContent$Message;

    iput-object p1, p0, Lcom/android/email/activity/MessageViewFragmentBase;->mMessage:Lcom/android/emailcommon/provider/EmailContent$Message;

    return-object p1
.end method

.method static synthetic access$2100(Lcom/android/email/activity/MessageViewFragmentBase;)V
    .locals 0
    .param p0    # Lcom/android/email/activity/MessageViewFragmentBase;

    invoke-direct {p0}, Lcom/android/email/activity/MessageViewFragmentBase;->queryContactStatus()V

    return-void
.end method

.method static synthetic access$2200(Lcom/android/email/activity/MessageViewFragmentBase;)J
    .locals 2
    .param p0    # Lcom/android/email/activity/MessageViewFragmentBase;

    iget-wide v0, p0, Lcom/android/email/activity/MessageViewFragmentBase;->mAccountId:J

    return-wide v0
.end method

.method static synthetic access$2302(Lcom/android/email/activity/MessageViewFragmentBase;Lcom/android/email/activity/MessageViewFragmentBase$LoadBodyTask;)Lcom/android/email/activity/MessageViewFragmentBase$LoadBodyTask;
    .locals 0
    .param p0    # Lcom/android/email/activity/MessageViewFragmentBase;
    .param p1    # Lcom/android/email/activity/MessageViewFragmentBase$LoadBodyTask;

    iput-object p1, p0, Lcom/android/email/activity/MessageViewFragmentBase;->mLoadBodyTask:Lcom/android/email/activity/MessageViewFragmentBase$LoadBodyTask;

    return-object p1
.end method

.method static synthetic access$2400(Lcom/android/email/activity/MessageViewFragmentBase;ZZ)V
    .locals 0
    .param p0    # Lcom/android/email/activity/MessageViewFragmentBase;
    .param p1    # Z
    .param p2    # Z

    invoke-direct {p0, p1, p2}, Lcom/android/email/activity/MessageViewFragmentBase;->showContent(ZZ)V

    return-void
.end method

.method static synthetic access$2500(Lcom/android/email/activity/MessageViewFragmentBase;Ljava/lang/String;Ljava/lang/String;Z)V
    .locals 0
    .param p0    # Lcom/android/email/activity/MessageViewFragmentBase;
    .param p1    # Ljava/lang/String;
    .param p2    # Ljava/lang/String;
    .param p3    # Z

    invoke-direct {p0, p1, p2, p3}, Lcom/android/email/activity/MessageViewFragmentBase;->reloadUiFromBody(Ljava/lang/String;Ljava/lang/String;Z)V

    return-void
.end method

.method static synthetic access$2600(Lcom/android/email/activity/MessageViewFragmentBase;Lcom/android/emailcommon/provider/EmailContent$Message;)V
    .locals 0
    .param p0    # Lcom/android/email/activity/MessageViewFragmentBase;
    .param p1    # Lcom/android/emailcommon/provider/EmailContent$Message;

    invoke-direct {p0, p1}, Lcom/android/email/activity/MessageViewFragmentBase;->updateRemainMsgInfo(Lcom/android/emailcommon/provider/EmailContent$Message;)V

    return-void
.end method

.method static synthetic access$2702(Lcom/android/email/activity/MessageViewFragmentBase;Lcom/android/email/activity/MessageViewFragmentBase$LoadAttachmentsTask;)Lcom/android/email/activity/MessageViewFragmentBase$LoadAttachmentsTask;
    .locals 0
    .param p0    # Lcom/android/email/activity/MessageViewFragmentBase;
    .param p1    # Lcom/android/email/activity/MessageViewFragmentBase$LoadAttachmentsTask;

    iput-object p1, p0, Lcom/android/email/activity/MessageViewFragmentBase;->mLoadAttachmentsTask:Lcom/android/email/activity/MessageViewFragmentBase$LoadAttachmentsTask;

    return-object p1
.end method

.method static synthetic access$2800(Lcom/android/email/activity/MessageViewFragmentBase;)Ljava/lang/String;
    .locals 1
    .param p0    # Lcom/android/email/activity/MessageViewFragmentBase;

    iget-object v0, p0, Lcom/android/email/activity/MessageViewFragmentBase;->mHtmlTextRaw:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$2802(Lcom/android/email/activity/MessageViewFragmentBase;Ljava/lang/String;)Ljava/lang/String;
    .locals 0
    .param p0    # Lcom/android/email/activity/MessageViewFragmentBase;
    .param p1    # Ljava/lang/String;

    iput-object p1, p0, Lcom/android/email/activity/MessageViewFragmentBase;->mHtmlTextRaw:Ljava/lang/String;

    return-object p1
.end method

.method static synthetic access$2900(Lcom/android/email/activity/MessageViewFragmentBase;Lcom/android/emailcommon/provider/EmailContent$Attachment;Lcom/android/emailcommon/provider/EmailContent$Message;Ljava/lang/String;)Z
    .locals 1
    .param p0    # Lcom/android/email/activity/MessageViewFragmentBase;
    .param p1    # Lcom/android/emailcommon/provider/EmailContent$Attachment;
    .param p2    # Lcom/android/emailcommon/provider/EmailContent$Message;
    .param p3    # Ljava/lang/String;

    invoke-direct {p0, p1, p2, p3}, Lcom/android/email/activity/MessageViewFragmentBase;->isInlineAttachment(Lcom/android/emailcommon/provider/EmailContent$Attachment;Lcom/android/emailcommon/provider/EmailContent$Message;Ljava/lang/String;)Z

    move-result v0

    return v0
.end method

.method static synthetic access$300(Lcom/android/email/activity/MessageViewFragmentBase;)Z
    .locals 1
    .param p0    # Lcom/android/email/activity/MessageViewFragmentBase;

    iget-boolean v0, p0, Lcom/android/email/activity/MessageViewFragmentBase;->mPartialDownload:Z

    return v0
.end method

.method static synthetic access$3000(Lcom/android/email/activity/MessageViewFragmentBase;J)V
    .locals 0
    .param p0    # Lcom/android/email/activity/MessageViewFragmentBase;
    .param p1    # J

    invoke-direct {p0, p1, p2}, Lcom/android/email/activity/MessageViewFragmentBase;->onDownloadAttachmentSilently(J)V

    return-void
.end method

.method static synthetic access$302(Lcom/android/email/activity/MessageViewFragmentBase;Z)Z
    .locals 0
    .param p0    # Lcom/android/email/activity/MessageViewFragmentBase;
    .param p1    # Z

    iput-boolean p1, p0, Lcom/android/email/activity/MessageViewFragmentBase;->mPartialDownload:Z

    return p1
.end method

.method static synthetic access$3100(Lcom/android/email/activity/MessageViewFragmentBase;J)Lcom/android/email/activity/MessageViewFragmentBase$MessageViewAttachmentInfo;
    .locals 1
    .param p0    # Lcom/android/email/activity/MessageViewFragmentBase;
    .param p1    # J

    invoke-direct {p0, p1, p2}, Lcom/android/email/activity/MessageViewFragmentBase;->findAttachmentInfoFromView(J)Lcom/android/email/activity/MessageViewFragmentBase$MessageViewAttachmentInfo;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$3200()Ljava/util/regex/Pattern;
    .locals 1

    sget-object v0, Lcom/android/email/activity/MessageViewFragmentBase;->IMG_TAG_START_REGEX:Ljava/util/regex/Pattern;

    return-object v0
.end method

.method static synthetic access$3300(Lcom/android/email/activity/MessageViewFragmentBase;)I
    .locals 1
    .param p0    # Lcom/android/email/activity/MessageViewFragmentBase;

    iget v0, p0, Lcom/android/email/activity/MessageViewFragmentBase;->mTabFlags:I

    return v0
.end method

.method static synthetic access$3400(Lcom/android/email/activity/MessageViewFragmentBase;I)V
    .locals 0
    .param p0    # Lcom/android/email/activity/MessageViewFragmentBase;
    .param p1    # I

    invoke-direct {p0, p1}, Lcom/android/email/activity/MessageViewFragmentBase;->updateTabs(I)V

    return-void
.end method

.method static synthetic access$3500(Lcom/android/email/activity/MessageViewFragmentBase;Ljava/util/List;)V
    .locals 0
    .param p0    # Lcom/android/email/activity/MessageViewFragmentBase;
    .param p1    # Ljava/util/List;

    invoke-direct {p0, p1}, Lcom/android/email/activity/MessageViewFragmentBase;->addAttachments(Ljava/util/List;)V

    return-void
.end method

.method static synthetic access$3600(Lcom/android/email/activity/MessageViewFragmentBase;I)V
    .locals 0
    .param p0    # Lcom/android/email/activity/MessageViewFragmentBase;
    .param p1    # I

    invoke-direct {p0, p1}, Lcom/android/email/activity/MessageViewFragmentBase;->setAttachmentCount(I)V

    return-void
.end method

.method static synthetic access$3700(Lcom/android/email/activity/MessageViewFragmentBase;)Ljava/lang/String;
    .locals 1
    .param p0    # Lcom/android/email/activity/MessageViewFragmentBase;

    iget-object v0, p0, Lcom/android/email/activity/MessageViewFragmentBase;->mHtmlTextWebView:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$3702(Lcom/android/email/activity/MessageViewFragmentBase;Ljava/lang/String;)Ljava/lang/String;
    .locals 0
    .param p0    # Lcom/android/email/activity/MessageViewFragmentBase;
    .param p1    # Ljava/lang/String;

    iput-object p1, p0, Lcom/android/email/activity/MessageViewFragmentBase;->mHtmlTextWebView:Ljava/lang/String;

    return-object p1
.end method

.method static synthetic access$3800(Lcom/android/email/activity/MessageViewFragmentBase;Ljava/lang/String;)V
    .locals 0
    .param p0    # Lcom/android/email/activity/MessageViewFragmentBase;
    .param p1    # Ljava/lang/String;

    invoke-direct {p0, p1}, Lcom/android/email/activity/MessageViewFragmentBase;->setMessageHtml(Ljava/lang/String;)V

    return-void
.end method

.method static synthetic access$400(Lcom/android/email/activity/MessageViewFragmentBase;J)V
    .locals 0
    .param p0    # Lcom/android/email/activity/MessageViewFragmentBase;
    .param p1    # J

    invoke-direct {p0, p1, p2}, Lcom/android/email/activity/MessageViewFragmentBase;->doLoadMsgBackground(J)V

    return-void
.end method

.method static synthetic access$4500(Lcom/android/email/activity/MessageViewFragmentBase;)V
    .locals 0
    .param p0    # Lcom/android/email/activity/MessageViewFragmentBase;

    invoke-direct {p0}, Lcom/android/email/activity/MessageViewFragmentBase;->cancelAllTasks()V

    return-void
.end method

.method static synthetic access$4600(Lcom/android/email/activity/MessageViewFragmentBase;J)V
    .locals 0
    .param p0    # Lcom/android/email/activity/MessageViewFragmentBase;
    .param p1    # J

    invoke-direct {p0, p1, p2}, Lcom/android/email/activity/MessageViewFragmentBase;->attachmentDownloaded(J)V

    return-void
.end method

.method static synthetic access$4700(Lcom/android/email/activity/MessageViewFragmentBase;J)Lcom/android/email/activity/MessageViewFragmentBase$MessageViewAttachmentInfo;
    .locals 1
    .param p0    # Lcom/android/email/activity/MessageViewFragmentBase;
    .param p1    # J

    invoke-direct {p0, p1, p2}, Lcom/android/email/activity/MessageViewFragmentBase;->findAttachmentInfo(J)Lcom/android/email/activity/MessageViewFragmentBase$MessageViewAttachmentInfo;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$4800(Landroid/content/Context;Lcom/android/email/AttachmentInfo;)Landroid/graphics/Bitmap;
    .locals 1
    .param p0    # Landroid/content/Context;
    .param p1    # Lcom/android/email/AttachmentInfo;

    invoke-static {p0, p1}, Lcom/android/email/activity/MessageViewFragmentBase;->getPreviewIcon(Landroid/content/Context;Lcom/android/email/AttachmentInfo;)Landroid/graphics/Bitmap;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$4900(Lcom/android/email/activity/MessageViewFragmentBase;Lcom/android/emailcommon/provider/EmailContent$Attachment;Z)Landroid/view/View;
    .locals 1
    .param p0    # Lcom/android/email/activity/MessageViewFragmentBase;
    .param p1    # Lcom/android/emailcommon/provider/EmailContent$Attachment;
    .param p2    # Z

    invoke-direct {p0, p1, p2}, Lcom/android/email/activity/MessageViewFragmentBase;->createAttachmentView(Lcom/android/emailcommon/provider/EmailContent$Attachment;Z)Landroid/view/View;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$500(Lcom/android/email/activity/MessageViewFragmentBase;)I
    .locals 1
    .param p0    # Lcom/android/email/activity/MessageViewFragmentBase;

    iget v0, p0, Lcom/android/email/activity/MessageViewFragmentBase;->mContactStatusState:I

    return v0
.end method

.method static synthetic access$5000(Lcom/android/email/activity/MessageViewFragmentBase;)Landroid/widget/LinearLayout;
    .locals 1
    .param p0    # Lcom/android/email/activity/MessageViewFragmentBase;

    iget-object v0, p0, Lcom/android/email/activity/MessageViewFragmentBase;->mAttachments:Landroid/widget/LinearLayout;

    return-object v0
.end method

.method static synthetic access$502(Lcom/android/email/activity/MessageViewFragmentBase;I)I
    .locals 0
    .param p0    # Lcom/android/email/activity/MessageViewFragmentBase;
    .param p1    # I

    iput p1, p0, Lcom/android/email/activity/MessageViewFragmentBase;->mContactStatusState:I

    return p1
.end method

.method static synthetic access$5100(Lcom/android/email/activity/MessageViewFragmentBase;Z)V
    .locals 0
    .param p0    # Lcom/android/email/activity/MessageViewFragmentBase;
    .param p1    # Z

    invoke-direct {p0, p1}, Lcom/android/email/activity/MessageViewFragmentBase;->updateVipIcon(Z)V

    return-void
.end method

.method static synthetic access$5200(Lcom/android/email/activity/MessageViewFragmentBase;)Landroid/widget/ImageView;
    .locals 1
    .param p0    # Lcom/android/email/activity/MessageViewFragmentBase;

    iget-object v0, p0, Lcom/android/email/activity/MessageViewFragmentBase;->mVipSwitcher:Landroid/widget/ImageView;

    return-object v0
.end method

.method static synthetic access$602(Lcom/android/email/activity/MessageViewFragmentBase;Landroid/net/Uri;)Landroid/net/Uri;
    .locals 0
    .param p0    # Lcom/android/email/activity/MessageViewFragmentBase;
    .param p1    # Landroid/net/Uri;

    iput-object p1, p0, Lcom/android/email/activity/MessageViewFragmentBase;->mQuickContactLookupUri:Landroid/net/Uri;

    return-object p1
.end method

.method static synthetic access$700(Lcom/android/email/activity/MessageViewFragmentBase;)Landroid/widget/ImageView;
    .locals 1
    .param p0    # Lcom/android/email/activity/MessageViewFragmentBase;

    iget-object v0, p0, Lcom/android/email/activity/MessageViewFragmentBase;->mSenderPresenceView:Landroid/widget/ImageView;

    return-object v0
.end method

.method static synthetic access$800(Lcom/android/email/activity/MessageViewFragmentBase;)Landroid/widget/ImageView;
    .locals 1
    .param p0    # Lcom/android/email/activity/MessageViewFragmentBase;

    iget-object v0, p0, Lcom/android/email/activity/MessageViewFragmentBase;->mFromBadge:Landroid/widget/ImageView;

    return-object v0
.end method

.method static synthetic access$900(Lcom/android/email/activity/MessageViewFragmentBase;)V
    .locals 0
    .param p0    # Lcom/android/email/activity/MessageViewFragmentBase;

    invoke-direct {p0}, Lcom/android/email/activity/MessageViewFragmentBase;->showDefaultQuickContactBadgeImage()V

    return-void
.end method

.method private addAttachment(Lcom/android/emailcommon/provider/EmailContent$Attachment;Z)V
    .locals 17
    .param p1    # Lcom/android/emailcommon/provider/EmailContent$Attachment;
    .param p2    # Z

    invoke-virtual/range {p0 .. p0}, Landroid/app/Fragment;->getActivity()Landroid/app/Activity;

    move-result-object v14

    invoke-virtual {v14}, Landroid/app/Activity;->getLayoutInflater()Landroid/view/LayoutInflater;

    move-result-object v8

    const v14, 0x7f040044

    const/4 v15, 0x0

    invoke-virtual {v8, v14, v15}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v13

    const v14, 0x7f0f006c

    invoke-static {v13, v14}, Lcom/android/email/activity/UiUtilities;->getView(Landroid/view/View;I)Landroid/view/View;

    move-result-object v5

    check-cast v5, Landroid/widget/TextView;

    const v14, 0x7f0f00b9

    invoke-static {v13, v14}, Lcom/android/email/activity/UiUtilities;->getView(Landroid/view/View;I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/TextView;

    const v14, 0x7f0f00b8

    invoke-static {v13, v14}, Lcom/android/email/activity/UiUtilities;->getView(Landroid/view/View;I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/ImageView;

    const v14, 0x7f0f00bc

    invoke-static {v13, v14}, Lcom/android/email/activity/UiUtilities;->getView(Landroid/view/View;I)Landroid/view/View;

    move-result-object v11

    check-cast v11, Landroid/widget/Button;

    const v14, 0x7f0f001a

    invoke-static {v13, v14}, Lcom/android/email/activity/UiUtilities;->getView(Landroid/view/View;I)Landroid/view/View;

    move-result-object v12

    check-cast v12, Landroid/widget/Button;

    const v14, 0x7f0f00ba

    invoke-static {v13, v14}, Lcom/android/email/activity/UiUtilities;->getView(Landroid/view/View;I)Landroid/view/View;

    move-result-object v10

    check-cast v10, Landroid/widget/Button;

    const v14, 0x7f0f00bb

    invoke-static {v13, v14}, Lcom/android/email/activity/UiUtilities;->getView(Landroid/view/View;I)Landroid/view/View;

    move-result-object v9

    check-cast v9, Landroid/widget/Button;

    const v14, 0x7f0f0001

    invoke-static {v13, v14}, Lcom/android/email/activity/UiUtilities;->getView(Landroid/view/View;I)Landroid/view/View;

    move-result-object v7

    check-cast v7, Landroid/widget/Button;

    const v14, 0x7f0f00a8

    invoke-static {v13, v14}, Lcom/android/email/activity/UiUtilities;->getView(Landroid/view/View;I)Landroid/view/View;

    move-result-object v6

    check-cast v6, Landroid/widget/ProgressBar;

    new-instance v3, Lcom/android/email/activity/MessageViewFragmentBase$MessageViewAttachmentInfo;

    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/android/email/activity/MessageViewFragmentBase;->mContext:Landroid/content/Context;

    const/4 v15, 0x0

    move-object/from16 v0, p1

    invoke-direct {v3, v14, v0, v6, v15}, Lcom/android/email/activity/MessageViewFragmentBase$MessageViewAttachmentInfo;-><init>(Landroid/content/Context;Lcom/android/emailcommon/provider/EmailContent$Attachment;Landroid/widget/ProgressBar;Lcom/android/email/activity/MessageViewFragmentBase$1;)V

    if-eqz p1, :cond_0

    move-object/from16 v0, p1

    iget-object v14, v0, Lcom/android/emailcommon/provider/EmailContent$Attachment;->mContentUri:Ljava/lang/String;

    invoke-static {v14}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v14

    if-nez v14, :cond_0

    move-object/from16 v0, p1

    iget-object v14, v0, Lcom/android/emailcommon/provider/EmailContent$Attachment;->mContentUri:Ljava/lang/String;

    const-string v15, "com.android.email.attachmentprovider"

    invoke-virtual {v14, v15}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v14

    if-eqz v14, :cond_1

    :cond_0
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/android/email/activity/MessageViewFragmentBase;->mContext:Landroid/content/Context;

    move-object/from16 v0, p1

    invoke-static {v14, v0}, Lcom/android/emailcommon/utility/Utility;->attachmentExists(Landroid/content/Context;Lcom/android/emailcommon/provider/EmailContent$Attachment;)Z

    move-result v14

    if-eqz v14, :cond_2

    :cond_1
    const/4 v14, 0x1

    invoke-static {v3, v14}, Lcom/android/email/activity/MessageViewFragmentBase$MessageViewAttachmentInfo;->access$1502(Lcom/android/email/activity/MessageViewFragmentBase$MessageViewAttachmentInfo;Z)Z

    :cond_2
    invoke-static {v3, v11}, Lcom/android/email/activity/MessageViewFragmentBase$MessageViewAttachmentInfo;->access$4102(Lcom/android/email/activity/MessageViewFragmentBase$MessageViewAttachmentInfo;Landroid/widget/Button;)Landroid/widget/Button;

    invoke-static {v3, v12}, Lcom/android/email/activity/MessageViewFragmentBase$MessageViewAttachmentInfo;->access$4202(Lcom/android/email/activity/MessageViewFragmentBase$MessageViewAttachmentInfo;Landroid/widget/Button;)Landroid/widget/Button;

    invoke-static {v3, v10}, Lcom/android/email/activity/MessageViewFragmentBase$MessageViewAttachmentInfo;->access$1302(Lcom/android/email/activity/MessageViewFragmentBase$MessageViewAttachmentInfo;Landroid/widget/Button;)Landroid/widget/Button;

    invoke-static {v3, v9}, Lcom/android/email/activity/MessageViewFragmentBase$MessageViewAttachmentInfo;->access$4302(Lcom/android/email/activity/MessageViewFragmentBase$MessageViewAttachmentInfo;Landroid/widget/Button;)Landroid/widget/Button;

    invoke-static {v3, v7}, Lcom/android/email/activity/MessageViewFragmentBase$MessageViewAttachmentInfo;->access$1402(Lcom/android/email/activity/MessageViewFragmentBase$MessageViewAttachmentInfo;Landroid/widget/Button;)Landroid/widget/Button;

    invoke-static {v3, v2}, Lcom/android/email/activity/MessageViewFragmentBase$MessageViewAttachmentInfo;->access$4002(Lcom/android/email/activity/MessageViewFragmentBase$MessageViewAttachmentInfo;Landroid/widget/ImageView;)Landroid/widget/ImageView;

    move-object/from16 v0, p0

    move/from16 v1, p2

    invoke-direct {v0, v3, v1}, Lcom/android/email/activity/MessageViewFragmentBase;->updateAttachmentButtons(Lcom/android/email/activity/MessageViewFragmentBase$MessageViewAttachmentInfo;Z)V

    invoke-virtual {v13, v3}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    move-object/from16 v0, p0

    invoke-virtual {v11, v0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    move-object/from16 v0, p0

    invoke-virtual {v12, v0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    move-object/from16 v0, p0

    invoke-virtual {v10, v0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    move-object/from16 v0, p0

    invoke-virtual {v9, v0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    move-object/from16 v0, p0

    invoke-virtual {v7, v0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v14, v3, Lcom/android/email/AttachmentInfo;->mName:Ljava/lang/String;

    move-object/from16 v0, p1

    iget-object v15, v0, Lcom/android/emailcommon/provider/EmailContent$Attachment;->mMimeType:Ljava/lang/String;

    invoke-static {v14, v15}, Lcom/android/emailcommon/internet/MimeUtility;->generateAttName(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v14

    invoke-virtual {v5, v14}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/android/email/activity/MessageViewFragmentBase;->mContext:Landroid/content/Context;

    iget-wide v15, v3, Lcom/android/email/AttachmentInfo;->mSize:J

    invoke-static/range {v14 .. v16}, Lcom/android/email/activity/UiUtilities;->formatSize(Landroid/content/Context;J)Ljava/lang/String;

    move-result-object v14

    invoke-virtual {v4, v14}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/android/email/activity/MessageViewFragmentBase;->mAttachments:Landroid/widget/LinearLayout;

    invoke-virtual {v14, v13}, Landroid/view/ViewGroup;->addView(Landroid/view/View;)V

    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/android/email/activity/MessageViewFragmentBase;->mAttachments:Landroid/widget/LinearLayout;

    const/4 v15, 0x0

    invoke-virtual {v14, v15}, Landroid/view/View;->setVisibility(I)V

    return-void
.end method

.method private addAttachments(Ljava/util/List;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/android/emailcommon/provider/EmailContent$Attachment;",
            ">;)V"
        }
    .end annotation

    move-object v0, p1

    if-eqz p1, :cond_0

    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v1

    if-nez v1, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v1, p0, Lcom/android/email/activity/MessageViewFragmentBase;->mRenderAttachmentTask:Landroid/os/AsyncTask;

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/android/email/activity/MessageViewFragmentBase;->mRenderAttachmentTask:Landroid/os/AsyncTask;

    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Landroid/os/AsyncTask;->cancel(Z)Z

    const/4 v1, 0x0

    iput-object v1, p0, Lcom/android/email/activity/MessageViewFragmentBase;->mRenderAttachmentTask:Landroid/os/AsyncTask;

    :cond_2
    new-instance v1, Lcom/android/email/activity/MessageViewFragmentBase$4;

    invoke-direct {v1, p0, v0}, Lcom/android/email/activity/MessageViewFragmentBase$4;-><init>(Lcom/android/email/activity/MessageViewFragmentBase;Ljava/util/List;)V

    sget-object v2, Landroid/os/AsyncTask;->THREAD_POOL_EXECUTOR:Ljava/util/concurrent/Executor;

    const/4 v3, 0x0

    new-array v3, v3, [Ljava/lang/Void;

    invoke-virtual {v1, v2, v3}, Landroid/os/AsyncTask;->executeOnExecutor(Ljava/util/concurrent/Executor;[Ljava/lang/Object;)Landroid/os/AsyncTask;

    move-result-object v1

    iput-object v1, p0, Lcom/android/email/activity/MessageViewFragmentBase;->mRenderAttachmentTask:Landroid/os/AsyncTask;

    goto :goto_0
.end method

.method private addVip(Lcom/android/emailcommon/mail/Address;)V
    .locals 3
    .param p1    # Lcom/android/emailcommon/mail/Address;

    const/4 v2, 0x0

    iget-object v0, p0, Lcom/android/email/activity/MessageViewFragmentBase;->mVipSwitcher:Landroid/widget/ImageView;

    invoke-virtual {v0, v2}, Landroid/view/View;->setClickable(Z)V

    new-instance v0, Lcom/android/email/activity/MessageViewFragmentBase$6;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/android/email/activity/MessageViewFragmentBase$6;-><init>(Lcom/android/email/activity/MessageViewFragmentBase;Lcom/android/emailcommon/utility/EmailAsyncTask$Tracker;)V

    const/4 v1, 0x1

    new-array v1, v1, [Lcom/android/emailcommon/mail/Address;

    aput-object p1, v1, v2

    invoke-virtual {v0, v1}, Lcom/android/emailcommon/utility/EmailAsyncTask;->executeParallel([Ljava/lang/Object;)Lcom/android/emailcommon/utility/EmailAsyncTask;

    return-void
.end method

.method private attachmentDownloaded(J)V
    .locals 9
    .param p1    # J

    invoke-direct {p0, p1, p2}, Lcom/android/email/activity/MessageViewFragmentBase;->doFinishLoadAttachment(J)V

    iget-object v0, p0, Lcom/android/email/activity/MessageViewFragmentBase;->mContext:Landroid/content/Context;

    invoke-static {v0, p1, p2}, Lcom/android/emailcommon/provider/EmailContent$Attachment;->restoreAttachmentWithId(Landroid/content/Context;J)Lcom/android/emailcommon/provider/EmailContent$Attachment;

    move-result-object v7

    if-nez v7, :cond_1

    const-string v0, "Email"

    const-string v1, "Attachment has been delete."

    invoke-static {v0, v1}, Lcom/android/emailcommon/Logging;->d(Ljava/lang/String;Ljava/lang/String;)V

    :cond_0
    :goto_0
    return-void

    :cond_1
    invoke-direct {p0, p1, p2}, Lcom/android/email/activity/MessageViewFragmentBase;->findAttachmentView(J)Landroid/view/View;

    move-result-object v6

    iget-object v0, v7, Lcom/android/emailcommon/provider/EmailContent$Attachment;->mContentId:Ljava/lang/String;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/android/email/activity/MessageViewFragmentBase;->mMessage:Lcom/android/emailcommon/provider/EmailContent$Message;

    iget-object v1, p0, Lcom/android/email/activity/MessageViewFragmentBase;->mHtmlTextWebView:Ljava/lang/String;

    invoke-direct {p0, v7, v0, v1}, Lcom/android/email/activity/MessageViewFragmentBase;->isInlineAttachment(Lcom/android/emailcommon/provider/EmailContent$Attachment;Lcom/android/emailcommon/provider/EmailContent$Message;Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_3

    :cond_2
    if-eqz v6, :cond_0

    invoke-virtual {v6}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Lcom/android/email/activity/MessageViewFragmentBase$MessageViewAttachmentInfo;

    invoke-direct {p0, v8}, Lcom/android/email/activity/MessageViewFragmentBase;->updatePreviewIcon(Lcom/android/email/activity/MessageViewFragmentBase$MessageViewAttachmentInfo;)V

    goto :goto_0

    :cond_3
    iget-object v0, p0, Lcom/android/email/activity/MessageViewFragmentBase;->mAttachments:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v6}, Landroid/view/ViewGroup;->removeView(Landroid/view/View;)V

    const-string v0, "Reloading UI for inline attachment loaded"

    invoke-static {v0}, Lcom/android/emailcommon/Logging;->d(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/android/email/activity/MessageViewFragmentBase;->mHtmlTextWebView:Ljava/lang/String;

    invoke-static {v0, v7}, Lcom/android/emailcommon/utility/AttachmentUtilities;->refactorHtmlTextRaw(Ljava/lang/String;Lcom/android/emailcommon/provider/EmailContent$Attachment;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/android/email/activity/MessageViewFragmentBase;->mHtmlTextWebView:Ljava/lang/String;

    iget-object v0, p0, Lcom/android/email/activity/MessageViewFragmentBase;->mMessageContentView:Landroid/webkit/WebView;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/email/activity/MessageViewFragmentBase;->mMessageContentView:Landroid/webkit/WebView;

    const-string v1, "email://"

    iget-object v2, p0, Lcom/android/email/activity/MessageViewFragmentBase;->mHtmlTextWebView:Ljava/lang/String;

    const-string v3, "text/html"

    const-string v4, "utf-8"

    const/4 v5, 0x0

    invoke-virtual/range {v0 .. v5}, Landroid/webkit/WebView;->loadDataWithBaseURL(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/android/email/activity/MessageViewFragmentBase;->mHtmlTextWebView:Ljava/lang/String;

    invoke-direct {p0, v0}, Lcom/android/email/activity/MessageViewFragmentBase;->doLinkParse(Ljava/lang/String;)V

    goto :goto_0
.end method

.method private blockNetworkLoads(Z)V
    .locals 1
    .param p1    # Z

    iget-object v0, p0, Lcom/android/email/activity/MessageViewFragmentBase;->mMessageContentView:Landroid/webkit/WebView;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/email/activity/MessageViewFragmentBase;->mMessageContentView:Landroid/webkit/WebView;

    invoke-virtual {v0}, Landroid/webkit/WebView;->getSettings()Landroid/webkit/WebSettings;

    move-result-object v0

    invoke-virtual {v0, p1}, Landroid/webkit/WebSettings;->setBlockNetworkLoads(Z)V

    :cond_0
    return-void
.end method

.method private cancelAllTasks()V
    .locals 1

    iget-object v0, p0, Lcom/android/email/activity/MessageViewFragmentBase;->mMessageObserver:Lcom/android/email/activity/MessageViewFragmentBase$MessageObserver;

    invoke-virtual {v0}, Lcom/android/email/activity/MessageViewFragmentBase$MessageObserver;->unregister()V

    iget-object v0, p0, Lcom/android/email/activity/MessageViewFragmentBase;->mTaskTracker:Lcom/android/emailcommon/utility/EmailAsyncTask$Tracker;

    invoke-virtual {v0}, Lcom/android/emailcommon/utility/EmailAsyncTask$Tracker;->cancellAllInterrupt()V

    return-void
.end method

.method private cleanupDetachedViews()V
    .locals 3

    const/4 v2, 0x0

    const-string v0, "MessageViewFragmentBase"

    const-string v1, "cleanupDetachedViews mLinkParserTask and webview will be destoryed."

    invoke-static {v0, v1}, Lcom/android/emailcommon/Logging;->d(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/android/email/activity/MessageViewFragmentBase;->mLinkParserTask:Lcom/android/emailcommon/utility/LinkParserTask;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/email/activity/MessageViewFragmentBase;->mLinkParserTask:Lcom/android/emailcommon/utility/LinkParserTask;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/os/AsyncTask;->cancel(Z)Z

    iget-object v0, p0, Lcom/android/email/activity/MessageViewFragmentBase;->mLinkParserTask:Lcom/android/emailcommon/utility/LinkParserTask;

    invoke-virtual {v0}, Lcom/android/emailcommon/utility/LinkParserTask;->stopWebView()V

    iput-object v2, p0, Lcom/android/email/activity/MessageViewFragmentBase;->mLinkParserTask:Lcom/android/emailcommon/utility/LinkParserTask;

    :cond_0
    iget-object v0, p0, Lcom/android/email/activity/MessageViewFragmentBase;->mMessageContentView:Landroid/webkit/WebView;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/android/email/activity/MessageViewFragmentBase;->mMessageContentView:Landroid/webkit/WebView;

    invoke-virtual {v0}, Landroid/webkit/WebView;->destroy()V

    iput-object v2, p0, Lcom/android/email/activity/MessageViewFragmentBase;->mMessageContentView:Landroid/webkit/WebView;

    :cond_1
    return-void
.end method

.method private final clearTabFlags(I)V
    .locals 2
    .param p1    # I

    iget v0, p0, Lcom/android/email/activity/MessageViewFragmentBase;->mTabFlags:I

    xor-int/lit8 v1, p1, -0x1

    and-int/2addr v0, v1

    invoke-direct {p0, v0}, Lcom/android/email/activity/MessageViewFragmentBase;->updateTabs(I)V

    return-void
.end method

.method private createAttachmentView(Lcom/android/emailcommon/provider/EmailContent$Attachment;Z)Landroid/view/View;
    .locals 17
    .param p1    # Lcom/android/emailcommon/provider/EmailContent$Attachment;
    .param p2    # Z

    invoke-virtual/range {p0 .. p0}, Landroid/app/Fragment;->getActivity()Landroid/app/Activity;

    move-result-object v14

    invoke-virtual {v14}, Landroid/app/Activity;->getLayoutInflater()Landroid/view/LayoutInflater;

    move-result-object v8

    const v14, 0x7f040044

    const/4 v15, 0x0

    invoke-virtual {v8, v14, v15}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v13

    const v14, 0x7f0f006c

    invoke-static {v13, v14}, Lcom/android/email/activity/UiUtilities;->getView(Landroid/view/View;I)Landroid/view/View;

    move-result-object v5

    check-cast v5, Landroid/widget/TextView;

    const v14, 0x7f0f00b9

    invoke-static {v13, v14}, Lcom/android/email/activity/UiUtilities;->getView(Landroid/view/View;I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/TextView;

    const v14, 0x7f0f00b8

    invoke-static {v13, v14}, Lcom/android/email/activity/UiUtilities;->getView(Landroid/view/View;I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/ImageView;

    const v14, 0x7f0f00bc

    invoke-static {v13, v14}, Lcom/android/email/activity/UiUtilities;->getView(Landroid/view/View;I)Landroid/view/View;

    move-result-object v11

    check-cast v11, Landroid/widget/Button;

    const v14, 0x7f0f001a

    invoke-static {v13, v14}, Lcom/android/email/activity/UiUtilities;->getView(Landroid/view/View;I)Landroid/view/View;

    move-result-object v12

    check-cast v12, Landroid/widget/Button;

    const v14, 0x7f0f00ba

    invoke-static {v13, v14}, Lcom/android/email/activity/UiUtilities;->getView(Landroid/view/View;I)Landroid/view/View;

    move-result-object v10

    check-cast v10, Landroid/widget/Button;

    const v14, 0x7f0f00bb

    invoke-static {v13, v14}, Lcom/android/email/activity/UiUtilities;->getView(Landroid/view/View;I)Landroid/view/View;

    move-result-object v9

    check-cast v9, Landroid/widget/Button;

    const v14, 0x7f0f0001

    invoke-static {v13, v14}, Lcom/android/email/activity/UiUtilities;->getView(Landroid/view/View;I)Landroid/view/View;

    move-result-object v7

    check-cast v7, Landroid/widget/Button;

    const v14, 0x7f0f00a8

    invoke-static {v13, v14}, Lcom/android/email/activity/UiUtilities;->getView(Landroid/view/View;I)Landroid/view/View;

    move-result-object v6

    check-cast v6, Landroid/widget/ProgressBar;

    new-instance v3, Lcom/android/email/activity/MessageViewFragmentBase$MessageViewAttachmentInfo;

    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/android/email/activity/MessageViewFragmentBase;->mContext:Landroid/content/Context;

    const/4 v15, 0x0

    move-object/from16 v0, p1

    invoke-direct {v3, v14, v0, v6, v15}, Lcom/android/email/activity/MessageViewFragmentBase$MessageViewAttachmentInfo;-><init>(Landroid/content/Context;Lcom/android/emailcommon/provider/EmailContent$Attachment;Landroid/widget/ProgressBar;Lcom/android/email/activity/MessageViewFragmentBase$1;)V

    if-eqz p1, :cond_0

    move-object/from16 v0, p1

    iget-object v14, v0, Lcom/android/emailcommon/provider/EmailContent$Attachment;->mContentUri:Ljava/lang/String;

    invoke-static {v14}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v14

    if-nez v14, :cond_0

    move-object/from16 v0, p1

    iget-object v14, v0, Lcom/android/emailcommon/provider/EmailContent$Attachment;->mContentUri:Ljava/lang/String;

    const-string v15, "com.android.email.attachmentprovider"

    invoke-virtual {v14, v15}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v14

    if-eqz v14, :cond_1

    :cond_0
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/android/email/activity/MessageViewFragmentBase;->mContext:Landroid/content/Context;

    move-object/from16 v0, p1

    invoke-static {v14, v0}, Lcom/android/emailcommon/utility/Utility;->attachmentExists(Landroid/content/Context;Lcom/android/emailcommon/provider/EmailContent$Attachment;)Z

    move-result v14

    if-eqz v14, :cond_2

    :cond_1
    const/4 v14, 0x1

    invoke-static {v3, v14}, Lcom/android/email/activity/MessageViewFragmentBase$MessageViewAttachmentInfo;->access$1502(Lcom/android/email/activity/MessageViewFragmentBase$MessageViewAttachmentInfo;Z)Z

    :cond_2
    invoke-static {v3, v11}, Lcom/android/email/activity/MessageViewFragmentBase$MessageViewAttachmentInfo;->access$4102(Lcom/android/email/activity/MessageViewFragmentBase$MessageViewAttachmentInfo;Landroid/widget/Button;)Landroid/widget/Button;

    invoke-static {v3, v12}, Lcom/android/email/activity/MessageViewFragmentBase$MessageViewAttachmentInfo;->access$4202(Lcom/android/email/activity/MessageViewFragmentBase$MessageViewAttachmentInfo;Landroid/widget/Button;)Landroid/widget/Button;

    invoke-static {v3, v10}, Lcom/android/email/activity/MessageViewFragmentBase$MessageViewAttachmentInfo;->access$1302(Lcom/android/email/activity/MessageViewFragmentBase$MessageViewAttachmentInfo;Landroid/widget/Button;)Landroid/widget/Button;

    invoke-static {v3, v9}, Lcom/android/email/activity/MessageViewFragmentBase$MessageViewAttachmentInfo;->access$4302(Lcom/android/email/activity/MessageViewFragmentBase$MessageViewAttachmentInfo;Landroid/widget/Button;)Landroid/widget/Button;

    invoke-static {v3, v7}, Lcom/android/email/activity/MessageViewFragmentBase$MessageViewAttachmentInfo;->access$1402(Lcom/android/email/activity/MessageViewFragmentBase$MessageViewAttachmentInfo;Landroid/widget/Button;)Landroid/widget/Button;

    invoke-static {v3, v2}, Lcom/android/email/activity/MessageViewFragmentBase$MessageViewAttachmentInfo;->access$4002(Lcom/android/email/activity/MessageViewFragmentBase$MessageViewAttachmentInfo;Landroid/widget/ImageView;)Landroid/widget/ImageView;

    move-object/from16 v0, p0

    move/from16 v1, p2

    invoke-direct {v0, v3, v1}, Lcom/android/email/activity/MessageViewFragmentBase;->updateAttachmentButtons(Lcom/android/email/activity/MessageViewFragmentBase$MessageViewAttachmentInfo;Z)V

    invoke-virtual {v13, v3}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    move-object/from16 v0, p0

    invoke-virtual {v11, v0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    move-object/from16 v0, p0

    invoke-virtual {v12, v0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    move-object/from16 v0, p0

    invoke-virtual {v10, v0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    move-object/from16 v0, p0

    invoke-virtual {v9, v0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    move-object/from16 v0, p0

    invoke-virtual {v7, v0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v14, v3, Lcom/android/email/AttachmentInfo;->mName:Ljava/lang/String;

    move-object/from16 v0, p1

    iget-object v15, v0, Lcom/android/emailcommon/provider/EmailContent$Attachment;->mMimeType:Ljava/lang/String;

    invoke-static {v14, v15}, Lcom/android/emailcommon/internet/MimeUtility;->generateAttName(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v14

    invoke-virtual {v5, v14}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/android/email/activity/MessageViewFragmentBase;->mContext:Landroid/content/Context;

    iget-wide v15, v3, Lcom/android/email/AttachmentInfo;->mSize:J

    invoke-static/range {v14 .. v16}, Lcom/android/email/activity/UiUtilities;->formatSize(Landroid/content/Context;J)Ljava/lang/String;

    move-result-object v14

    invoke-virtual {v4, v14}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    return-object v13
.end method

.method private createMockAttachment(Ljava/lang/String;)Landroid/net/Uri;
    .locals 4
    .param p1    # Ljava/lang/String;

    sget-object v3, Landroid/os/Environment;->DIRECTORY_DOWNLOADS:Ljava/lang/String;

    invoke-static {v3}, Landroid/os/Environment;->getExternalStoragePublicDirectory(Ljava/lang/String;)Ljava/io/File;

    move-result-object v0

    invoke-virtual {v0}, Ljava/io/File;->mkdirs()Z

    new-instance v1, Ljava/io/File;

    invoke-direct {v1, v0, p1}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    invoke-virtual {v1}, Ljava/io/File;->exists()Z

    move-result v3

    if-nez v3, :cond_0

    :try_start_0
    invoke-virtual {v1}, Ljava/io/File;->createNewFile()Z

    new-instance v2, Ljava/io/FileWriter;

    invoke-direct {v2, v1}, Ljava/io/FileWriter;-><init>(Ljava/io/File;)V

    const-string v3, "MessageComposeTest This is a attachment for email testcase"

    invoke-virtual {v2, v3}, Ljava/io/Writer;->write(Ljava/lang/String;)V

    invoke-virtual {v2}, Ljava/io/OutputStreamWriter;->flush()V

    invoke-virtual {v2}, Ljava/io/OutputStreamWriter;->close()V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    :cond_0
    :goto_0
    invoke-static {v1}, Landroid/net/Uri;->fromFile(Ljava/io/File;)Landroid/net/Uri;

    move-result-object v3

    return-object v3

    :catch_0
    move-exception v3

    goto :goto_0
.end method

.method private doFinishLoadAttachment(J)V
    .locals 2
    .param p1    # J

    const/4 v1, 0x1

    invoke-direct {p0, p1, p2}, Lcom/android/email/activity/MessageViewFragmentBase;->findAttachmentInfo(J)Lcom/android/email/activity/MessageViewFragmentBase$MessageViewAttachmentInfo;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-static {v0, v1}, Lcom/android/email/activity/MessageViewFragmentBase$MessageViewAttachmentInfo;->access$1502(Lcom/android/email/activity/MessageViewFragmentBase$MessageViewAttachmentInfo;Z)Z

    invoke-direct {p0, v0, v1}, Lcom/android/email/activity/MessageViewFragmentBase;->updateAttachmentButtons(Lcom/android/email/activity/MessageViewFragmentBase$MessageViewAttachmentInfo;Z)V

    :cond_0
    return-void
.end method

.method private declared-synchronized doLinkParse(Ljava/lang/String;)V
    .locals 4
    .param p1    # Ljava/lang/String;

    monitor-enter p0

    :try_start_0
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v0, "MessageViewFragmentBase"

    const-string v1, "text is null!"

    invoke-static {v0, v1}, Lcom/android/emailcommon/Logging;->d(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :goto_0
    monitor-exit p0

    return-void

    :cond_0
    :try_start_1
    iget-object v0, p0, Lcom/android/email/activity/MessageViewFragmentBase;->mLinkParserTask:Lcom/android/emailcommon/utility/LinkParserTask;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/android/email/activity/MessageViewFragmentBase;->mLinkParserTask:Lcom/android/emailcommon/utility/LinkParserTask;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/os/AsyncTask;->cancel(Z)Z

    iget-object v0, p0, Lcom/android/email/activity/MessageViewFragmentBase;->mLinkParserTask:Lcom/android/emailcommon/utility/LinkParserTask;

    invoke-virtual {v0}, Lcom/android/emailcommon/utility/LinkParserTask;->stopWebView()V

    const-string v0, "MessageViewFragmentBase"

    const-string v1, "doLinkParse canceled pre LinkParseTask"

    invoke-static {v0, v1}, Lcom/android/emailcommon/Logging;->d(Ljava/lang/String;Ljava/lang/String;)V

    :cond_1
    new-instance v0, Lcom/android/emailcommon/utility/LinkParserTask;

    iget-object v1, p0, Lcom/android/email/activity/MessageViewFragmentBase;->mMessageContentView:Landroid/webkit/WebView;

    invoke-direct {v0, v1}, Lcom/android/emailcommon/utility/LinkParserTask;-><init>(Landroid/webkit/WebView;)V

    iput-object v0, p0, Lcom/android/email/activity/MessageViewFragmentBase;->mLinkParserTask:Lcom/android/emailcommon/utility/LinkParserTask;

    iget-object v0, p0, Lcom/android/email/activity/MessageViewFragmentBase;->mLinkParserTask:Lcom/android/emailcommon/utility/LinkParserTask;

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/String;

    const/4 v2, 0x0

    invoke-virtual {p1}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v1, v2

    invoke-virtual {v0, v1}, Landroid/os/AsyncTask;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method private doLoadMsgBackground(J)V
    .locals 3
    .param p1    # J

    iget-object v0, p0, Lcom/android/email/activity/MessageViewFragmentBase;->mContext:Landroid/content/Context;

    invoke-static {v0}, Lcom/android/email/RefreshManager;->getInstance(Landroid/content/Context;)Lcom/android/email/RefreshManager;

    move-result-object v0

    iget-object v1, p0, Lcom/android/email/activity/MessageViewFragmentBase;->mContext:Landroid/content/Context;

    invoke-virtual {v0, v1}, Lcom/android/email/RefreshManager;->checkIsLowStorage(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_0

    :goto_0
    return-void

    :cond_0
    invoke-virtual {p0}, Landroid/app/Fragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    invoke-static {v0}, Lcom/android/emailcommon/utility/Utility;->hasConnectivity(Landroid/content/Context;)Z

    move-result v0

    if-nez v0, :cond_1

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/android/email/activity/MessageViewFragmentBase;->mPartialDownload:Z

    invoke-static {}, Lcom/android/email/activity/ConnectionAlertDialog;->newInstance()Lcom/android/email/activity/ConnectionAlertDialog;

    move-result-object v0

    invoke-virtual {p0}, Landroid/app/Fragment;->getActivity()Landroid/app/Activity;

    move-result-object v1

    invoke-virtual {v1}, Landroid/app/Activity;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v1

    const-string v2, "connectionalertdialog"

    invoke-virtual {v0, v1, v2}, Landroid/app/DialogFragment;->show(Landroid/app/FragmentManager;Ljava/lang/String;)V

    goto :goto_0

    :cond_1
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/android/email/activity/MessageViewFragmentBase;->mPartialDownload:Z

    invoke-direct {p0}, Lcom/android/email/activity/MessageViewFragmentBase;->updateRemainProgress()V

    const-string v0, "Email"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "+++ Load partial Message id = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1, p2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/android/emailcommon/Logging;->d(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/android/email/activity/MessageViewFragmentBase;->mControllerCallback:Lcom/android/email/ControllerResultUiThreadWrapper;

    invoke-virtual {v0}, Lcom/android/email/ControllerResultUiThreadWrapper;->getWrappee()Lcom/android/email/Controller$Result;

    move-result-object v0

    check-cast v0, Lcom/android/email/activity/MessageViewFragmentBase$ControllerResults;

    invoke-virtual {v0, p1, p2}, Lcom/android/email/activity/MessageViewFragmentBase$ControllerResults;->setWaitForLoadMessageId(J)V

    iget-object v0, p0, Lcom/android/email/activity/MessageViewFragmentBase;->mController:Lcom/android/email/Controller;

    invoke-virtual {v0, p1, p2}, Lcom/android/email/Controller;->loadMessageForView(J)V

    goto :goto_0
.end method

.method private findAttachmentInfo(J)Lcom/android/email/activity/MessageViewFragmentBase$MessageViewAttachmentInfo;
    .locals 2
    .param p1    # J

    invoke-direct {p0, p1, p2}, Lcom/android/email/activity/MessageViewFragmentBase;->findAttachmentView(J)Landroid/view/View;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/android/email/activity/MessageViewFragmentBase$MessageViewAttachmentInfo;

    :goto_0
    return-object v1

    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method private findAttachmentInfoFromView(J)Lcom/android/email/activity/MessageViewFragmentBase$MessageViewAttachmentInfo;
    .locals 5
    .param p1    # J

    const/4 v2, 0x0

    iget-object v3, p0, Lcom/android/email/activity/MessageViewFragmentBase;->mAttachments:Landroid/widget/LinearLayout;

    invoke-virtual {v3}, Landroid/view/ViewGroup;->getChildCount()I

    move-result v1

    :goto_0
    if-ge v2, v1, :cond_1

    iget-object v3, p0, Lcom/android/email/activity/MessageViewFragmentBase;->mAttachments:Landroid/widget/LinearLayout;

    invoke-virtual {v3, v2}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v3

    invoke-virtual {v3}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/email/activity/MessageViewFragmentBase$MessageViewAttachmentInfo;

    iget-wide v3, v0, Lcom/android/email/AttachmentInfo;->mId:J

    cmp-long v3, v3, p1

    if-nez v3, :cond_0

    :goto_1
    return-object v0

    :cond_0
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    :cond_1
    const/4 v0, 0x0

    goto :goto_1
.end method

.method private findAttachmentView(J)Landroid/view/View;
    .locals 6
    .param p1    # J

    const/4 v2, 0x0

    iget-object v4, p0, Lcom/android/email/activity/MessageViewFragmentBase;->mAttachments:Landroid/widget/LinearLayout;

    invoke-virtual {v4}, Landroid/view/ViewGroup;->getChildCount()I

    move-result v1

    :goto_0
    if-ge v2, v1, :cond_1

    iget-object v4, p0, Lcom/android/email/activity/MessageViewFragmentBase;->mAttachments:Landroid/widget/LinearLayout;

    invoke-virtual {v4, v2}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v3

    invoke-virtual {v3}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/email/activity/MessageViewFragmentBase$MessageViewAttachmentInfo;

    iget-wide v4, v0, Lcom/android/email/AttachmentInfo;->mId:J

    cmp-long v4, v4, p1

    if-nez v4, :cond_0

    :goto_1
    return-object v3

    :cond_0
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    :cond_1
    const/4 v3, 0x0

    goto :goto_1
.end method

.method private formatDate(JZ)Ljava/lang/String;
    .locals 8
    .param p1    # J
    .param p3    # Z

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    new-instance v1, Ljava/util/Formatter;

    invoke-direct {v1, v7}, Ljava/util/Formatter;-><init>(Ljava/lang/Appendable;)V

    iget-object v0, p0, Lcom/android/email/activity/MessageViewFragmentBase;->mContext:Landroid/content/Context;

    const v3, 0x80011

    if-eqz p3, :cond_0

    const/4 v2, 0x4

    :goto_0
    or-int v6, v3, v2

    move-wide v2, p1

    move-wide v4, p1

    invoke-static/range {v0 .. v6}, Landroid/text/format/DateUtils;->formatDateRange(Landroid/content/Context;Ljava/util/Formatter;JJI)Ljava/util/Formatter;

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0

    :cond_0
    const/16 v2, 0x8

    goto :goto_0
.end method

.method private static getPreviewIcon(Landroid/content/Context;Lcom/android/email/AttachmentInfo;)Landroid/graphics/Bitmap;
    .locals 8
    .param p0    # Landroid/content/Context;
    .param p1    # Lcom/android/email/AttachmentInfo;

    :try_start_0
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v7

    iget-wide v0, p1, Lcom/android/email/AttachmentInfo;->mAccountKey:J

    iget-wide v2, p1, Lcom/android/email/AttachmentInfo;->mId:J

    const/16 v4, 0x3e

    const/16 v5, 0x3e

    invoke-static/range {v0 .. v5}, Lcom/android/emailcommon/utility/AttachmentUtilities;->getAttachmentThumbnailUri(JJII)Landroid/net/Uri;

    move-result-object v0

    invoke-virtual {v7, v0}, Landroid/content/ContentResolver;->openInputStream(Landroid/net/Uri;)Ljava/io/InputStream;

    move-result-object v0

    invoke-static {v0}, Landroid/graphics/BitmapFactory;->decodeStream(Ljava/io/InputStream;)Landroid/graphics/Bitmap;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    :goto_0
    return-object v0

    :catch_0
    move-exception v6

    const-string v0, "Email"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Attachment preview failed with exception "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v6}, Ljava/lang/Throwable;->getMessage()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v0, 0x0

    goto :goto_0
.end method

.method private getTabContentViewForFlag(I)Landroid/view/View;
    .locals 1
    .param p1    # I

    packed-switch p1, :pswitch_data_0

    new-instance v0, Ljava/lang/IllegalArgumentException;

    invoke-direct {v0}, Ljava/lang/IllegalArgumentException;-><init>()V

    throw v0

    :pswitch_0
    iget-object v0, p0, Lcom/android/email/activity/MessageViewFragmentBase;->mMessageContentView:Landroid/webkit/WebView;

    :goto_0
    return-object v0

    :pswitch_1
    iget-object v0, p0, Lcom/android/email/activity/MessageViewFragmentBase;->mAttachmentsScroll:Landroid/view/View;

    goto :goto_0

    :pswitch_2
    iget-object v0, p0, Lcom/android/email/activity/MessageViewFragmentBase;->mInviteScroll:Landroid/view/View;

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x65
        :pswitch_0
        :pswitch_2
        :pswitch_1
    .end packed-switch
.end method

.method private getTabViewForFlag(I)Landroid/view/View;
    .locals 1
    .param p1    # I

    packed-switch p1, :pswitch_data_0

    new-instance v0, Ljava/lang/IllegalArgumentException;

    invoke-direct {v0}, Ljava/lang/IllegalArgumentException;-><init>()V

    throw v0

    :pswitch_0
    iget-object v0, p0, Lcom/android/email/activity/MessageViewFragmentBase;->mMessageTab:Landroid/widget/TextView;

    :goto_0
    return-object v0

    :pswitch_1
    iget-object v0, p0, Lcom/android/email/activity/MessageViewFragmentBase;->mAttachmentTab:Landroid/widget/TextView;

    goto :goto_0

    :pswitch_2
    iget-object v0, p0, Lcom/android/email/activity/MessageViewFragmentBase;->mInviteTab:Landroid/widget/TextView;

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x65
        :pswitch_0
        :pswitch_2
        :pswitch_1
    .end packed-switch
.end method

.method private getWebViewZoom()I
    .locals 3

    iget-object v1, p0, Lcom/android/email/activity/MessageViewFragmentBase;->mContext:Landroid/content/Context;

    invoke-static {v1}, Lcom/android/email/Preferences;->getPreferences(Landroid/content/Context;)Lcom/android/email/Preferences;

    move-result-object v1

    invoke-virtual {v1}, Lcom/android/email/Preferences;->getTextZoom()I

    move-result v0

    sget-object v1, Lcom/android/email/activity/MessageViewFragmentBase;->sZoomSizes:[Ljava/lang/String;

    if-nez v1, :cond_0

    iget-object v1, p0, Lcom/android/email/activity/MessageViewFragmentBase;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f060011

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getStringArray(I)[Ljava/lang/String;

    move-result-object v1

    sput-object v1, Lcom/android/email/activity/MessageViewFragmentBase;->sZoomSizes:[Ljava/lang/String;

    :cond_0
    sget-object v1, Lcom/android/email/activity/MessageViewFragmentBase;->sZoomSizes:[Ljava/lang/String;

    aget-object v1, v1, v0

    invoke-static {v1}, Ljava/lang/Float;->valueOf(Ljava/lang/String;)Ljava/lang/Float;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Float;->floatValue()F

    move-result v1

    const/high16 v2, 0x42c80000

    mul-float/2addr v1, v2

    float-to-int v1, v1

    return v1
.end method

.method private hideDetails()V
    .locals 3

    iget-object v0, p0, Lcom/android/email/activity/MessageViewFragmentBase;->mMessage:Lcom/android/emailcommon/provider/EmailContent$Message;

    iget-object v1, p0, Lcom/android/email/activity/MessageViewFragmentBase;->mContext:Landroid/content/Context;

    iget-object v2, p0, Lcom/android/email/activity/MessageViewFragmentBase;->mAddressesView:Landroid/widget/TextView;

    invoke-static {v0, v1, v2}, Lcom/mediatek/email/emailvip/utils/MessageViewUtils;->updateAddressesView(Lcom/android/emailcommon/provider/EmailContent$Message;Landroid/content/Context;Landroid/widget/TextView;)V

    iget-object v0, p0, Lcom/android/email/activity/MessageViewFragmentBase;->mDetailsCollapsed:Landroid/view/View;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    iget-object v0, p0, Lcom/android/email/activity/MessageViewFragmentBase;->mDetailsExpanded:Landroid/view/View;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    return-void
.end method

.method private initContactStatusViews()V
    .locals 1

    const/4 v0, 0x0

    iput v0, p0, Lcom/android/email/activity/MessageViewFragmentBase;->mContactStatusState:I

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/android/email/activity/MessageViewFragmentBase;->mQuickContactLookupUri:Landroid/net/Uri;

    invoke-direct {p0}, Lcom/android/email/activity/MessageViewFragmentBase;->showDefaultQuickContactBadgeImage()V

    return-void
.end method

.method private isInlineAttachment(Lcom/android/emailcommon/provider/EmailContent$Attachment;Lcom/android/emailcommon/provider/EmailContent$Message;Ljava/lang/String;)Z
    .locals 2
    .param p1    # Lcom/android/emailcommon/provider/EmailContent$Attachment;
    .param p2    # Lcom/android/emailcommon/provider/EmailContent$Message;
    .param p3    # Ljava/lang/String;

    const/4 v0, 0x1

    if-eqz p2, :cond_1

    iget v1, p2, Lcom/android/emailcommon/provider/EmailContent$Message;->mFlagLoaded:I

    if-ne v1, v0, :cond_1

    if-eqz p3, :cond_2

    invoke-static {p3, p1}, Lcom/android/emailcommon/utility/Utility;->isAttchmentInlineStrictly(Ljava/lang/String;Lcom/android/emailcommon/provider/EmailContent$Attachment;)Z

    move-result v1

    if-eqz v1, :cond_2

    :cond_0
    :goto_0
    return v0

    :cond_1
    if-eqz p3, :cond_2

    iget-object v1, p1, Lcom/android/emailcommon/provider/EmailContent$Attachment;->mContentId:Ljava/lang/String;

    if-nez v1, :cond_0

    :cond_2
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private static isVisible(Landroid/view/View;)Z
    .locals 1
    .param p0    # Landroid/view/View;

    if-eqz p0, :cond_0

    invoke-virtual {p0}, Landroid/view/View;->getVisibility()I

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private loadsImagesAutomatically(Z)V
    .locals 1
    .param p1    # Z

    iget-object v0, p0, Lcom/android/email/activity/MessageViewFragmentBase;->mMessageContentView:Landroid/webkit/WebView;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/email/activity/MessageViewFragmentBase;->mMessageContentView:Landroid/webkit/WebView;

    invoke-virtual {v0}, Landroid/webkit/WebView;->getSettings()Landroid/webkit/WebSettings;

    move-result-object v0

    invoke-virtual {v0, p1}, Landroid/webkit/WebSettings;->setLoadsImagesAutomatically(Z)V

    :cond_0
    return-void
.end method

.method private static makeVisible(Landroid/view/View;Z)V
    .locals 2
    .param p0    # Landroid/view/View;
    .param p1    # Z

    if-eqz p1, :cond_1

    const/4 v0, 0x0

    :goto_0
    if-eqz p0, :cond_0

    invoke-virtual {p0}, Landroid/view/View;->getVisibility()I

    move-result v1

    if-eq v1, v0, :cond_0

    invoke-virtual {p0, v0}, Landroid/view/View;->setVisibility(I)V

    :cond_0
    return-void

    :cond_1
    const/16 v0, 0x8

    goto :goto_0
.end method

.method private onCancelAttachment(Lcom/android/email/activity/MessageViewFragmentBase$MessageViewAttachmentInfo;)V
    .locals 3
    .param p1    # Lcom/android/email/activity/MessageViewFragmentBase$MessageViewAttachmentInfo;

    const/4 v0, 0x1

    invoke-static {p1, v0}, Lcom/android/email/activity/MessageViewFragmentBase$MessageViewAttachmentInfo;->access$1602(Lcom/android/email/activity/MessageViewFragmentBase$MessageViewAttachmentInfo;Z)Z

    invoke-static {p1}, Lcom/android/email/activity/MessageViewFragmentBase$MessageViewAttachmentInfo;->access$1300(Lcom/android/email/activity/MessageViewFragmentBase$MessageViewAttachmentInfo;)Landroid/widget/Button;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    invoke-static {p1}, Lcom/android/email/activity/MessageViewFragmentBase$MessageViewAttachmentInfo;->access$1400(Lcom/android/email/activity/MessageViewFragmentBase$MessageViewAttachmentInfo;)Landroid/widget/Button;

    move-result-object v0

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    invoke-virtual {p1}, Lcom/android/email/activity/MessageViewFragmentBase$MessageViewAttachmentInfo;->hideProgress()V

    iget-object v0, p0, Lcom/android/email/activity/MessageViewFragmentBase;->mController:Lcom/android/email/Controller;

    iget-wide v1, p1, Lcom/android/email/AttachmentInfo;->mId:J

    invoke-virtual {v0, v1, v2}, Lcom/android/email/Controller;->cancelLoadAttachment(J)V

    return-void
.end method

.method private onClickSender()V
    .locals 4

    const/4 v2, 0x1

    invoke-virtual {p0}, Lcom/android/email/activity/MessageViewFragmentBase;->isMessageOpen()Z

    move-result v1

    if-nez v1, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v1, p0, Lcom/android/email/activity/MessageViewFragmentBase;->mMessage:Lcom/android/emailcommon/provider/EmailContent$Message;

    iget-object v1, v1, Lcom/android/emailcommon/provider/EmailContent$Message;->mFrom:Ljava/lang/String;

    invoke-static {v1}, Lcom/android/emailcommon/mail/Address;->unpackFirst(Ljava/lang/String;)Lcom/android/emailcommon/mail/Address;

    move-result-object v0

    if-eqz v0, :cond_0

    iget v1, p0, Lcom/android/email/activity/MessageViewFragmentBase;->mContactStatusState:I

    if-nez v1, :cond_2

    iput v2, p0, Lcom/android/email/activity/MessageViewFragmentBase;->mContactStatusState:I

    goto :goto_0

    :cond_2
    iget v1, p0, Lcom/android/email/activity/MessageViewFragmentBase;->mContactStatusState:I

    if-eq v1, v2, :cond_0

    invoke-virtual {p0}, Landroid/app/Fragment;->getActivity()Landroid/app/Activity;

    move-result-object v1

    iget-object v2, p0, Lcom/android/email/activity/MessageViewFragmentBase;->mFromBadge:Landroid/widget/ImageView;

    iget-object v3, p0, Lcom/android/email/activity/MessageViewFragmentBase;->mQuickContactLookupUri:Landroid/net/Uri;

    invoke-static {v1, v2, v3, v0}, Lcom/android/email/activity/UiUtilities;->showContacts(Landroid/content/Context;Landroid/widget/ImageView;Landroid/net/Uri;Lcom/android/emailcommon/mail/Address;)V

    goto :goto_0
.end method

.method private onDownloadAttachmentSilently(J)V
    .locals 4
    .param p1    # J

    invoke-virtual {p0}, Landroid/app/Fragment;->getActivity()Landroid/app/Activity;

    move-result-object v1

    invoke-static {v1}, Lcom/android/email/Preferences;->getPreferences(Landroid/content/Context;)Lcom/android/email/Preferences;

    move-result-object v0

    invoke-virtual {v0}, Lcom/android/email/Preferences;->checkLowStorage()V

    invoke-virtual {v0}, Lcom/android/email/Preferences;->getLowStorage()Z

    move-result v1

    if-eqz v1, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    invoke-virtual {p0}, Landroid/app/Fragment;->getActivity()Landroid/app/Activity;

    move-result-object v1

    invoke-static {v1}, Lcom/android/emailcommon/utility/Utility;->hasConnectivity(Landroid/content/Context;)Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v2, p0, Lcom/android/email/activity/MessageViewFragmentBase;->mDownloadAttachmentList:Ljava/util/ArrayList;

    monitor-enter v2

    :try_start_0
    iget-object v1, p0, Lcom/android/email/activity/MessageViewFragmentBase;->mDownloadAttachmentList:Ljava/util/ArrayList;

    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    invoke-virtual {v1, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    monitor-exit v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    invoke-direct {p0, p1, p2}, Lcom/android/email/activity/MessageViewFragmentBase;->triggerDownload(J)V

    goto :goto_0

    :catchall_0
    move-exception v1

    :try_start_1
    monitor-exit v2
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v1
.end method

.method private onInfoAttachment(Lcom/android/email/activity/MessageViewFragmentBase$MessageViewAttachmentInfo;)V
    .locals 3
    .param p1    # Lcom/android/email/activity/MessageViewFragmentBase$MessageViewAttachmentInfo;

    invoke-virtual {p0}, Landroid/app/Fragment;->getActivity()Landroid/app/Activity;

    move-result-object v1

    iget v2, p1, Lcom/android/email/AttachmentInfo;->mDenyFlags:I

    invoke-static {v1, v2}, Lcom/android/email/activity/AttachmentInfoDialog;->newInstance(Landroid/content/Context;I)Lcom/android/email/activity/AttachmentInfoDialog;

    move-result-object v0

    invoke-virtual {p0}, Landroid/app/Fragment;->getActivity()Landroid/app/Activity;

    move-result-object v1

    invoke-virtual {v1}, Landroid/app/Activity;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/app/DialogFragment;->show(Landroid/app/FragmentManager;Ljava/lang/String;)V

    return-void
.end method

.method private onLoadAttachment(Lcom/android/email/activity/MessageViewFragmentBase$MessageViewAttachmentInfo;)V
    .locals 8
    .param p1    # Lcom/android/email/activity/MessageViewFragmentBase$MessageViewAttachmentInfo;

    const/16 v1, 0x8

    const/4 v3, 0x1

    const/4 v2, 0x0

    iget-object v0, p0, Lcom/android/email/activity/MessageViewFragmentBase;->mContext:Landroid/content/Context;

    invoke-static {v0}, Lcom/android/email/Preferences;->getPreferences(Landroid/content/Context;)Lcom/android/email/Preferences;

    move-result-object v7

    invoke-virtual {v7}, Lcom/android/email/Preferences;->getLowStorage()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/email/activity/MessageViewFragmentBase;->mContext:Landroid/content/Context;

    const v1, 0x7f08000e

    invoke-static {v0, v1, v3}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    iget-object v0, p0, Lcom/android/email/activity/MessageViewFragmentBase;->mContext:Landroid/content/Context;

    const v1, 0x7f08000c

    invoke-static {v0, v1, v3}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    const-string v0, "onLoadAttachment canceled due to low storage"

    invoke-static {v0}, Lcom/android/emailcommon/Logging;->d(Ljava/lang/String;)V

    :goto_0
    return-void

    :cond_0
    invoke-virtual {p0}, Landroid/app/Fragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    invoke-static {v0}, Lcom/android/emailcommon/utility/Utility;->hasConnectivity(Landroid/content/Context;)Z

    move-result v0

    if-nez v0, :cond_1

    invoke-static {}, Lcom/android/email/activity/ConnectionAlertDialog;->newInstance()Lcom/android/email/activity/ConnectionAlertDialog;

    move-result-object v0

    invoke-virtual {p0}, Landroid/app/Fragment;->getActivity()Landroid/app/Activity;

    move-result-object v1

    invoke-virtual {v1}, Landroid/app/Activity;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v1

    const-string v2, "connectionalertdialog"

    invoke-virtual {v0, v1, v2}, Landroid/app/DialogFragment;->show(Landroid/app/FragmentManager;Ljava/lang/String;)V

    goto :goto_0

    :cond_1
    invoke-static {p1}, Lcom/android/email/activity/MessageViewFragmentBase$MessageViewAttachmentInfo;->access$1300(Lcom/android/email/activity/MessageViewFragmentBase$MessageViewAttachmentInfo;)Landroid/widget/Button;

    move-result-object v0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    invoke-static {}, Lcom/android/email/service/AttachmentDownloadService;->getQueueSize()I

    move-result v0

    if-nez v0, :cond_2

    invoke-static {p1}, Lcom/android/email/activity/MessageViewFragmentBase$MessageViewAttachmentInfo;->access$1400(Lcom/android/email/activity/MessageViewFragmentBase$MessageViewAttachmentInfo;)Landroid/widget/Button;

    move-result-object v0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    new-instance v0, Lcom/android/email/activity/MessageViewFragmentBase$3;

    iget-object v1, p0, Lcom/android/email/activity/MessageViewFragmentBase;->mTaskTracker:Lcom/android/emailcommon/utility/EmailAsyncTask$Tracker;

    invoke-direct {v0, p0, v1, p1}, Lcom/android/email/activity/MessageViewFragmentBase$3;-><init>(Lcom/android/email/activity/MessageViewFragmentBase;Lcom/android/emailcommon/utility/EmailAsyncTask$Tracker;Lcom/android/email/activity/MessageViewFragmentBase$MessageViewAttachmentInfo;)V

    new-array v1, v2, [Ljava/lang/Void;

    invoke-virtual {v0, v1}, Lcom/android/emailcommon/utility/EmailAsyncTask;->executeParallel([Ljava/lang/Object;)Lcom/android/emailcommon/utility/EmailAsyncTask;

    :goto_1
    invoke-static {p1, v2}, Lcom/android/email/activity/MessageViewFragmentBase$MessageViewAttachmentInfo;->access$1602(Lcom/android/email/activity/MessageViewFragmentBase$MessageViewAttachmentInfo;Z)Z

    invoke-static {p1}, Lcom/android/email/activity/MessageViewFragmentBase$MessageViewAttachmentInfo;->access$1400(Lcom/android/email/activity/MessageViewFragmentBase$MessageViewAttachmentInfo;)Landroid/widget/Button;

    move-result-object v0

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setEnabled(Z)V

    invoke-virtual {p1}, Lcom/android/email/activity/MessageViewFragmentBase$MessageViewAttachmentInfo;->showProgressIndeterminate()V

    iget-object v0, p0, Lcom/android/email/activity/MessageViewFragmentBase;->mController:Lcom/android/email/Controller;

    iget-wide v1, p1, Lcom/android/email/AttachmentInfo;->mId:J

    iget-wide v3, p0, Lcom/android/email/activity/MessageViewFragmentBase;->mMessageId:J

    iget-wide v5, p0, Lcom/android/email/activity/MessageViewFragmentBase;->mAccountId:J

    invoke-virtual/range {v0 .. v6}, Lcom/android/email/Controller;->loadAttachment(JJJ)V

    goto :goto_0

    :cond_2
    invoke-static {p1}, Lcom/android/email/activity/MessageViewFragmentBase$MessageViewAttachmentInfo;->access$1400(Lcom/android/email/activity/MessageViewFragmentBase$MessageViewAttachmentInfo;)Landroid/widget/Button;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    goto :goto_1
.end method

.method private onOpenAttachment(Lcom/android/email/activity/MessageViewFragmentBase$MessageViewAttachmentInfo;)V
    .locals 9
    .param p1    # Lcom/android/email/activity/MessageViewFragmentBase$MessageViewAttachmentInfo;

    const v8, 0x7f08010c

    const v7, 0x7f0800e8

    iget-object v3, p0, Lcom/android/email/activity/MessageViewFragmentBase;->mContext:Landroid/content/Context;

    iget-wide v4, p1, Lcom/android/email/AttachmentInfo;->mId:J

    invoke-static {v3, v4, v5}, Lcom/android/emailcommon/provider/EmailContent$Attachment;->restoreAttachmentWithId(Landroid/content/Context;J)Lcom/android/emailcommon/provider/EmailContent$Attachment;

    move-result-object v0

    if-nez v0, :cond_0

    invoke-virtual {p0}, Landroid/app/Fragment;->getActivity()Landroid/app/Activity;

    move-result-object v3

    invoke-static {v3, v8}, Lcom/android/emailcommon/utility/Utility;->showToast(Landroid/content/Context;I)V

    invoke-direct {p0, p1}, Lcom/android/email/activity/MessageViewFragmentBase;->updateDeletedAttachment(Lcom/android/email/activity/MessageViewFragmentBase$MessageViewAttachmentInfo;)V

    :goto_0
    return-void

    :cond_0
    iget-wide v3, p1, Lcom/android/email/AttachmentInfo;->mSize:J

    const-wide/16 v5, 0x0

    cmp-long v3, v3, v5

    if-nez v3, :cond_1

    invoke-virtual {p0}, Landroid/app/Fragment;->getActivity()Landroid/app/Activity;

    move-result-object v3

    invoke-static {v3, v8}, Lcom/android/emailcommon/utility/Utility;->showToast(Landroid/content/Context;I)V

    goto :goto_0

    :cond_1
    iget-object v3, p0, Lcom/android/email/activity/MessageViewFragmentBase;->mContext:Landroid/content/Context;

    invoke-static {v3, v0}, Lcom/android/emailcommon/utility/Utility;->attachmentExists(Landroid/content/Context;Lcom/android/emailcommon/provider/EmailContent$Attachment;)Z

    move-result v3

    if-nez v3, :cond_2

    invoke-virtual {p0}, Landroid/app/Fragment;->getActivity()Landroid/app/Activity;

    move-result-object v3

    const v4, 0x7f080022

    invoke-static {v3, v4}, Lcom/android/emailcommon/utility/Utility;->showToast(Landroid/content/Context;I)V

    goto :goto_0

    :cond_2
    iget-boolean v3, p1, Lcom/android/email/AttachmentInfo;->mAllowInstall:Z

    if-eqz v3, :cond_6

    invoke-static {}, Lcom/android/emailcommon/utility/Utility;->isExternalStorageMounted()Z

    move-result v3

    if-nez v3, :cond_3

    invoke-virtual {p0}, Landroid/app/Fragment;->getActivity()Landroid/app/Activity;

    move-result-object v3

    invoke-static {v3, v7}, Lcom/android/emailcommon/utility/Utility;->showToast(Landroid/content/Context;I)V

    goto :goto_0

    :cond_3
    iget-boolean v3, p1, Lcom/android/email/AttachmentInfo;->mIsLocal:Z

    if-nez v3, :cond_6

    iget-boolean v3, p1, Lcom/android/email/AttachmentInfo;->mAllowSave:Z

    if-eqz v3, :cond_4

    invoke-static {}, Lcom/android/emailcommon/utility/Utility;->isExternalStorageMounted()Z

    move-result v3

    if-nez v3, :cond_5

    :cond_4
    invoke-virtual {p0}, Landroid/app/Fragment;->getActivity()Landroid/app/Activity;

    move-result-object v3

    invoke-static {v3, v7}, Lcom/android/emailcommon/utility/Utility;->showToast(Landroid/content/Context;I)V

    goto :goto_0

    :cond_5
    invoke-virtual {p1}, Lcom/android/email/activity/MessageViewFragmentBase$MessageViewAttachmentInfo;->isFileSaved()Z

    move-result v3

    if-nez v3, :cond_6

    invoke-direct {p0, p1}, Lcom/android/email/activity/MessageViewFragmentBase;->performAttachmentSave(Lcom/android/email/activity/MessageViewFragmentBase$MessageViewAttachmentInfo;)Ljava/io/File;

    move-result-object v3

    if-nez v3, :cond_6

    invoke-virtual {p0}, Landroid/app/Fragment;->getActivity()Landroid/app/Activity;

    move-result-object v3

    invoke-static {v3, v7}, Lcom/android/emailcommon/utility/Utility;->showToast(Landroid/content/Context;I)V

    goto :goto_0

    :cond_6
    :try_start_0
    iget-object v3, p0, Lcom/android/email/activity/MessageViewFragmentBase;->mContext:Landroid/content/Context;

    iget-wide v4, p0, Lcom/android/email/activity/MessageViewFragmentBase;->mAccountId:J

    invoke-virtual {p1, v3, v4, v5}, Lcom/android/email/AttachmentInfo;->getAttachmentIntent(Landroid/content/Context;J)Landroid/content/Intent;

    move-result-object v2

    invoke-virtual {p0, v2}, Landroid/app/Fragment;->startActivity(Landroid/content/Intent;)V
    :try_end_0
    .catch Landroid/content/ActivityNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v1

    invoke-virtual {p0}, Landroid/app/Fragment;->getActivity()Landroid/app/Activity;

    move-result-object v3

    invoke-static {v3, v8}, Lcom/android/emailcommon/utility/Utility;->showToast(Landroid/content/Context;I)V

    goto :goto_0
.end method

.method private onSaveAttachment(Lcom/android/email/activity/MessageViewFragmentBase$MessageViewAttachmentInfo;)V
    .locals 8
    .param p1    # Lcom/android/email/activity/MessageViewFragmentBase$MessageViewAttachmentInfo;

    const v5, 0x7f0800e8

    iget-object v3, p0, Lcom/android/email/activity/MessageViewFragmentBase;->mContext:Landroid/content/Context;

    invoke-static {v3}, Lcom/android/emailcommon/utility/AttachmentUtilities;->getExternalStorageAvailableSize(Landroid/content/Context;)J

    move-result-wide v1

    const-wide/16 v3, -0x1

    cmp-long v3, v1, v3

    if-nez v3, :cond_1

    invoke-virtual {p0}, Landroid/app/Fragment;->getActivity()Landroid/app/Activity;

    move-result-object v3

    invoke-static {v3, v5}, Lcom/android/emailcommon/utility/Utility;->showToast(Landroid/content/Context;I)V

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-wide v3, p1, Lcom/android/email/AttachmentInfo;->mSize:J

    cmp-long v3, v1, v3

    if-gez v3, :cond_2

    invoke-virtual {p0}, Landroid/app/Fragment;->getActivity()Landroid/app/Activity;

    move-result-object v3

    const v4, 0x7f080063

    invoke-static {v3, v4}, Lcom/android/emailcommon/utility/Utility;->showToast(Landroid/content/Context;I)V

    goto :goto_0

    :cond_2
    invoke-virtual {p1}, Lcom/android/email/activity/MessageViewFragmentBase$MessageViewAttachmentInfo;->isFileSaved()Z

    move-result v3

    if-nez v3, :cond_0

    invoke-direct {p0, p1}, Lcom/android/email/activity/MessageViewFragmentBase;->performAttachmentSave(Lcom/android/email/activity/MessageViewFragmentBase$MessageViewAttachmentInfo;)Ljava/io/File;

    move-result-object v0

    if-eqz v0, :cond_3

    invoke-virtual {p0}, Landroid/app/Fragment;->getActivity()Landroid/app/Activity;

    move-result-object v3

    iget-object v4, p0, Lcom/android/email/activity/MessageViewFragmentBase;->mContext:Landroid/content/Context;

    const v5, 0x7f0800e7

    invoke-virtual {v4, v5}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v4

    const/4 v5, 0x1

    new-array v5, v5, [Ljava/lang/Object;

    const/4 v6, 0x0

    invoke-virtual {v0}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v7

    aput-object v7, v5, v6

    invoke-static {v4, v5}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/android/emailcommon/utility/Utility;->showToast(Landroid/content/Context;Ljava/lang/String;)V

    goto :goto_0

    :cond_3
    invoke-virtual {p0}, Landroid/app/Fragment;->getActivity()Landroid/app/Activity;

    move-result-object v3

    invoke-static {v3, v5}, Lcom/android/emailcommon/utility/Utility;->showToast(Landroid/content/Context;I)V

    goto :goto_0
.end method

.method private onSaveClicked(Landroid/view/View;)V
    .locals 4
    .param p1    # Landroid/view/View;

    invoke-virtual {p1}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/email/activity/MessageViewFragmentBase$MessageViewAttachmentInfo;

    iget-object v1, v0, Lcom/android/email/AttachmentInfo;->mName:Ljava/lang/String;

    invoke-static {v1}, Lcom/android/email/activity/RenameAttachmentFileDialog;->isFileNameValid(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-direct {p0, v0}, Lcom/android/email/activity/MessageViewFragmentBase;->onSaveAttachment(Lcom/android/email/activity/MessageViewFragmentBase$MessageViewAttachmentInfo;)V

    :goto_0
    return-void

    :cond_0
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Open rename dialog, attachment original name: + "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, v0, Lcom/android/email/AttachmentInfo;->mName:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/android/emailcommon/Logging;->d(Ljava/lang/String;)V

    new-instance v1, Lcom/android/email/activity/MessageViewFragmentBase$2;

    invoke-direct {v1, p0}, Lcom/android/email/activity/MessageViewFragmentBase$2;-><init>(Lcom/android/email/activity/MessageViewFragmentBase;)V

    invoke-static {v0, v1}, Lcom/android/email/activity/RenameAttachmentFileDialog;->newInstance(Lcom/android/email/AttachmentInfo;Lcom/android/email/activity/RenameAttachmentFileDialog$Callback;)Lcom/android/email/activity/RenameAttachmentFileDialog;

    move-result-object v1

    invoke-virtual {p0}, Landroid/app/Fragment;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v2

    const-string v3, "RenameAttachmentFileDialog"

    invoke-virtual {v1, v2, v3}, Landroid/app/DialogFragment;->show(Landroid/app/FragmentManager;Ljava/lang/String;)V

    goto :goto_0
.end method

.method private onVipSwitcherClick()V
    .locals 3

    invoke-virtual {p0}, Lcom/android/email/activity/MessageViewFragmentBase;->isMessageOpen()Z

    move-result v2

    if-nez v2, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    invoke-virtual {p0}, Lcom/android/email/activity/MessageViewFragmentBase;->getMessage()Lcom/android/emailcommon/provider/EmailContent$Message;

    move-result-object v1

    iget-object v2, v1, Lcom/android/emailcommon/provider/EmailContent$Message;->mFrom:Ljava/lang/String;

    invoke-static {v2}, Lcom/android/emailcommon/mail/Address;->unpackFirst(Ljava/lang/String;)Lcom/android/emailcommon/mail/Address;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v2, v1, Lcom/android/emailcommon/provider/EmailContent$Message;->mFrom:Ljava/lang/String;

    invoke-static {v2}, Lcom/mediatek/email/emailvip/VipMemberCache;->isVIP(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_2

    invoke-direct {p0, v0}, Lcom/android/email/activity/MessageViewFragmentBase;->removeVip(Lcom/android/emailcommon/mail/Address;)V

    goto :goto_0

    :cond_2
    invoke-direct {p0, v0}, Lcom/android/email/activity/MessageViewFragmentBase;->addVip(Lcom/android/emailcommon/mail/Address;)V

    goto :goto_0
.end method

.method private performAttachmentSave(Lcom/android/email/activity/MessageViewFragmentBase$MessageViewAttachmentInfo;)Ljava/io/File;
    .locals 21
    .param p1    # Lcom/android/email/activity/MessageViewFragmentBase$MessageViewAttachmentInfo;

    move-object/from16 v0, p1

    iget-wide v3, v0, Lcom/android/email/AttachmentInfo;->mSize:J

    const-wide/16 v8, 0x0

    cmp-long v3, v3, v8

    if-eqz v3, :cond_0

    move-object/from16 v0, p1

    iget-object v3, v0, Lcom/android/email/AttachmentInfo;->mName:Ljava/lang/String;

    invoke-static {v3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_0

    move-object/from16 v0, p1

    iget-object v3, v0, Lcom/android/email/AttachmentInfo;->mContentType:Ljava/lang/String;

    invoke-static {v3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-eqz v3, :cond_1

    :cond_0
    const/16 v17, 0x0

    :goto_0
    return-object v17

    :cond_1
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/android/email/activity/MessageViewFragmentBase;->mContext:Landroid/content/Context;

    move-object/from16 v0, p1

    iget-wide v4, v0, Lcom/android/email/AttachmentInfo;->mId:J

    invoke-static {v3, v4, v5}, Lcom/android/emailcommon/provider/EmailContent$Attachment;->restoreAttachmentWithId(Landroid/content/Context;J)Lcom/android/emailcommon/provider/EmailContent$Attachment;

    move-result-object v11

    if-nez v11, :cond_2

    invoke-direct/range {p0 .. p1}, Lcom/android/email/activity/MessageViewFragmentBase;->updateDeletedAttachment(Lcom/android/email/activity/MessageViewFragmentBase$MessageViewAttachmentInfo;)V

    const/16 v17, 0x0

    goto :goto_0

    :cond_2
    move-object/from16 v0, p0

    iget-wide v3, v0, Lcom/android/email/activity/MessageViewFragmentBase;->mAccountId:J

    iget-wide v8, v11, Lcom/android/emailcommon/provider/EmailContent;->mId:J

    invoke-static {v3, v4, v8, v9}, Lcom/android/emailcommon/utility/AttachmentUtilities;->getAttachmentUri(JJ)Landroid/net/Uri;

    move-result-object v12

    const-string v13, "/storage/sdcard0"

    :try_start_0
    new-instance v16, Ljava/io/File;

    sget-object v3, Landroid/os/Environment;->DIRECTORY_DOWNLOADS:Ljava/lang/String;

    move-object/from16 v0, v16

    invoke-direct {v0, v13, v3}, Ljava/io/File;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual/range {v16 .. v16}, Ljava/io/File;->mkdirs()Z

    move-result v3

    if-nez v3, :cond_3

    const-string v3, "MessageViewFragmentBase performAttachmentSave mkdirs failed"

    invoke-static {v3}, Lcom/android/emailcommon/Logging;->w(Ljava/lang/String;)V

    :cond_3
    move-object/from16 v0, p1

    iget-object v3, v0, Lcom/android/email/AttachmentInfo;->mName:Ljava/lang/String;

    move-object/from16 v0, v16

    invoke-static {v0, v3}, Lcom/android/emailcommon/utility/Utility;->createUniqueFile(Ljava/io/File;Ljava/lang/String;)Ljava/io/File;

    move-result-object v17

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "MessageViewFragmentBase performAttachmentSave save attachment to external storage. attachment.mid="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-wide v4, v11, Lcom/android/emailcommon/provider/EmailContent;->mId:J

    invoke-virtual {v3, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ", attachment.mFileName="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, v11, Lcom/android/emailcommon/provider/EmailContent$Attachment;->mFileName:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lcom/android/emailcommon/Logging;->d(Ljava/lang/String;)V

    const/4 v14, 0x0

    sget-boolean v3, Lcom/android/emailcommon/Configuration;->mIsRunTestcase:Z

    if-nez v3, :cond_4

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/android/email/activity/MessageViewFragmentBase;->mContext:Landroid/content/Context;

    invoke-virtual {v3}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v3

    invoke-static {v3, v12}, Lcom/android/emailcommon/utility/AttachmentUtilities;->resolveAttachmentIdToContentUri(Landroid/content/ContentResolver;Landroid/net/Uri;)Landroid/net/Uri;

    move-result-object v14

    :goto_1
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/android/email/activity/MessageViewFragmentBase;->mContext:Landroid/content/Context;

    invoke-virtual {v3}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v3

    invoke-virtual {v3, v14}, Landroid/content/ContentResolver;->openInputStream(Landroid/net/Uri;)Ljava/io/InputStream;

    move-result-object v18

    new-instance v20, Ljava/io/FileOutputStream;

    move-object/from16 v0, v20

    move-object/from16 v1, v17

    invoke-direct {v0, v1}, Ljava/io/FileOutputStream;-><init>(Ljava/io/File;)V

    move-object/from16 v0, v18

    move-object/from16 v1, v20

    invoke-static {v0, v1}, Lorg/apache/commons/io/IOUtils;->copy(Ljava/io/InputStream;Ljava/io/OutputStream;)I

    move-result v15

    invoke-virtual/range {v20 .. v20}, Ljava/io/OutputStream;->flush()V

    invoke-virtual/range {v20 .. v20}, Ljava/io/FileOutputStream;->close()V

    invoke-virtual/range {v18 .. v18}, Ljava/io/InputStream;->close()V

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "MessageViewFragmentBase performAttachmentSave attachment saved as "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual/range {v17 .. v17}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " ,length="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v15}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lcom/android/emailcommon/Logging;->d(Ljava/lang/String;)V

    invoke-virtual/range {v17 .. v17}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v7

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/android/email/activity/MessageViewFragmentBase;->mContext:Landroid/content/Context;

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/String;

    const/4 v5, 0x0

    aput-object v7, v4, v5

    const/4 v5, 0x0

    const/4 v8, 0x0

    invoke-static {v3, v4, v5, v8}, Landroid/media/MediaScannerConnection;->scanFile(Landroid/content/Context;[Ljava/lang/String;[Ljava/lang/String;Landroid/media/MediaScannerConnection$OnScanCompletedListener;)V

    invoke-virtual/range {p0 .. p0}, Landroid/app/Fragment;->getActivity()Landroid/app/Activity;

    move-result-object v3

    const-string v4, "download"

    invoke-virtual {v3, v4}, Landroid/app/Activity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/app/DownloadManager;

    const-string v3, "application/dcf"

    move-object/from16 v0, p1

    iget-object v4, v0, Lcom/android/email/AttachmentInfo;->mContentType:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_5

    const-string v6, "application/vnd.oma.drm.content"

    :goto_2
    move-object/from16 v0, p1

    iget-object v3, v0, Lcom/android/email/AttachmentInfo;->mName:Ljava/lang/String;

    move-object/from16 v0, p1

    iget-object v4, v0, Lcom/android/email/AttachmentInfo;->mName:Ljava/lang/String;

    const/4 v5, 0x0

    move-object/from16 v0, p1

    iget-wide v8, v0, Lcom/android/email/AttachmentInfo;->mSize:J

    const/4 v10, 0x1

    invoke-virtual/range {v2 .. v10}, Landroid/app/DownloadManager;->addCompletedDownload(Ljava/lang/String;Ljava/lang/String;ZLjava/lang/String;Ljava/lang/String;JZ)J

    move-object/from16 v0, p1

    invoke-static {v0, v7}, Lcom/android/email/activity/MessageViewFragmentBase$MessageViewAttachmentInfo;->access$1200(Lcom/android/email/activity/MessageViewFragmentBase$MessageViewAttachmentInfo;Ljava/lang/String;)V

    const/4 v3, 0x1

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    invoke-direct {v0, v1, v3}, Lcom/android/email/activity/MessageViewFragmentBase;->updateAttachmentButtons(Lcom/android/email/activity/MessageViewFragmentBase$MessageViewAttachmentInfo;Z)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    goto/16 :goto_0

    :catch_0
    move-exception v19

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "MessageViewFragmentBase performAttachmentSave exception: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual/range {v19 .. v19}, Ljava/lang/Throwable;->getMessage()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lcom/android/emailcommon/Logging;->d(Ljava/lang/String;)V

    const/16 v17, 0x0

    goto/16 :goto_0

    :cond_4
    :try_start_1
    move-object/from16 v0, p1

    iget-object v3, v0, Lcom/android/email/AttachmentInfo;->mName:Ljava/lang/String;

    move-object/from16 v0, p0

    invoke-direct {v0, v3}, Lcom/android/email/activity/MessageViewFragmentBase;->createMockAttachment(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v14

    goto/16 :goto_1

    :cond_5
    move-object/from16 v0, p1

    iget-object v6, v0, Lcom/android/email/AttachmentInfo;->mContentType:Ljava/lang/String;
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_2
.end method

.method private queryContactStatus()V
    .locals 6

    invoke-virtual {p0}, Lcom/android/email/activity/MessageViewFragmentBase;->isMessageOpen()Z

    move-result v2

    if-nez v2, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    invoke-direct {p0}, Lcom/android/email/activity/MessageViewFragmentBase;->initContactStatusViews()V

    iget-object v2, p0, Lcom/android/email/activity/MessageViewFragmentBase;->mMessage:Lcom/android/emailcommon/provider/EmailContent$Message;

    iget-object v2, v2, Lcom/android/emailcommon/provider/EmailContent$Message;->mFrom:Ljava/lang/String;

    invoke-static {v2}, Lcom/android/emailcommon/mail/Address;->unpackFirst(Ljava/lang/String;)Lcom/android/emailcommon/mail/Address;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-virtual {v1}, Lcom/android/emailcommon/mail/Address;->getAddress()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Landroid/app/Fragment;->getLoaderManager()Landroid/app/LoaderManager;

    move-result-object v2

    const/4 v3, 0x1

    invoke-static {v0}, Lcom/android/email/activity/MessageViewFragmentBase$ContactStatusLoaderCallbacks;->createArguments(Ljava/lang/String;)Landroid/os/Bundle;

    move-result-object v4

    new-instance v5, Lcom/android/email/activity/MessageViewFragmentBase$ContactStatusLoaderCallbacks;

    invoke-direct {v5, p0}, Lcom/android/email/activity/MessageViewFragmentBase$ContactStatusLoaderCallbacks;-><init>(Lcom/android/email/activity/MessageViewFragmentBase;)V

    invoke-virtual {v2, v3, v4, v5}, Landroid/app/LoaderManager;->restartLoader(ILandroid/os/Bundle;Landroid/app/LoaderManager$LoaderCallbacks;)Landroid/content/Loader;

    goto :goto_0
.end method

.method private reloadUiFromBody(Ljava/lang/String;Ljava/lang/String;Z)V
    .locals 10
    .param p1    # Ljava/lang/String;
    .param p2    # Ljava/lang/String;
    .param p3    # Z

    const/4 v5, 0x1

    const/4 v6, 0x0

    const/4 v3, 0x0

    const/4 v4, 0x0

    iput-object v4, p0, Lcom/android/email/activity/MessageViewFragmentBase;->mHtmlTextRaw:Ljava/lang/String;

    const/4 v0, 0x0

    const/4 v1, 0x0

    iget-object v4, p0, Lcom/android/email/activity/MessageViewFragmentBase;->mCallback:Lcom/android/email/activity/MessageViewFragmentBase$Callback;

    invoke-interface {v4}, Lcom/android/email/activity/MessageViewFragmentBase$Callback;->onGetQueryTerm()Ljava/lang/String;

    move-result-object v1

    if-nez p2, :cond_6

    move-object v3, p1

    if-eqz v3, :cond_0

    invoke-static {v3}, Lcom/android/email/mail/internet/EmailHtmlUtil;->escapeCharacterToDisplay(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    :cond_0
    new-instance v2, Ljava/lang/StringBuffer;

    const-string v4, "<html><body>"

    invoke-direct {v2, v4}, Ljava/lang/StringBuffer;-><init>(Ljava/lang/String;)V

    if-eqz v3, :cond_1

    invoke-virtual {v2, v3}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    :cond_1
    const-string v4, "</body></html>"

    invoke-virtual {v2, v4}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    invoke-virtual {v2}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v3

    :goto_0
    if-eqz v1, :cond_2

    if-eqz v3, :cond_2

    invoke-static {v3, v1}, Lcom/android/emailcommon/utility/TextUtilities;->highlightTermsInHtml(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    iget-object v4, p0, Lcom/android/email/activity/MessageViewFragmentBase;->mHtmlTextRaw:Ljava/lang/String;

    if-eqz v4, :cond_2

    iput-object v3, p0, Lcom/android/email/activity/MessageViewFragmentBase;->mHtmlTextRaw:Ljava/lang/String;

    :cond_2
    iget-object v4, p0, Lcom/android/email/activity/MessageViewFragmentBase;->mAlwaysShowPicturesButton:Landroid/view/View;

    invoke-static {v4, v6}, Lcom/android/email/activity/MessageViewFragmentBase;->makeVisible(Landroid/view/View;Z)V

    if-eqz v0, :cond_4

    iget-boolean v4, p0, Lcom/android/email/activity/MessageViewFragmentBase;->mRestoredPictureLoaded:Z

    if-nez v4, :cond_3

    if-eqz p3, :cond_8

    :cond_3
    invoke-direct {p0, v6}, Lcom/android/email/activity/MessageViewFragmentBase;->blockNetworkLoads(Z)V

    const/16 v4, 0x8

    invoke-virtual {p0, v4}, Lcom/android/email/activity/MessageViewFragmentBase;->addTabFlags(I)V

    iget-object v7, p0, Lcom/android/email/activity/MessageViewFragmentBase;->mAlwaysShowPicturesButton:Landroid/view/View;

    if-nez p3, :cond_7

    move v4, v5

    :goto_1
    invoke-static {v7, v4}, Lcom/android/email/activity/MessageViewFragmentBase;->makeVisible(Landroid/view/View;Z)V

    iput-boolean v6, p0, Lcom/android/email/activity/MessageViewFragmentBase;->mRestoredPictureLoaded:Z

    :cond_4
    :goto_2
    iget-object v4, p0, Lcom/android/email/activity/MessageViewFragmentBase;->mMainView:Lcom/android/email/view/NonLockingScrollView;

    invoke-virtual {v4}, Lcom/android/email/view/NonLockingScrollView;->isScrollFreeze()Z

    move-result v4

    if-eqz v4, :cond_5

    iget-object v4, p0, Lcom/android/email/activity/MessageViewFragmentBase;->mMainView:Lcom/android/email/view/NonLockingScrollView;

    invoke-virtual {v4, v6, v6}, Lcom/android/email/view/NonLockingScrollView;->scrollTo(II)V

    :cond_5
    invoke-direct {p0, v3}, Lcom/android/email/activity/MessageViewFragmentBase;->setMessageHtml(Ljava/lang/String;)V

    new-instance v4, Lcom/android/email/activity/MessageViewFragmentBase$LoadAttachmentsTask;

    invoke-direct {v4, p0}, Lcom/android/email/activity/MessageViewFragmentBase$LoadAttachmentsTask;-><init>(Lcom/android/email/activity/MessageViewFragmentBase;)V

    iput-object v4, p0, Lcom/android/email/activity/MessageViewFragmentBase;->mLoadAttachmentsTask:Lcom/android/email/activity/MessageViewFragmentBase$LoadAttachmentsTask;

    iget-object v4, p0, Lcom/android/email/activity/MessageViewFragmentBase;->mLoadAttachmentsTask:Lcom/android/email/activity/MessageViewFragmentBase$LoadAttachmentsTask;

    new-array v7, v5, [Ljava/lang/Long;

    iget-object v8, p0, Lcom/android/email/activity/MessageViewFragmentBase;->mMessage:Lcom/android/emailcommon/provider/EmailContent$Message;

    iget-wide v8, v8, Lcom/android/emailcommon/provider/EmailContent;->mId:J

    invoke-static {v8, v9}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v8

    aput-object v8, v7, v6

    invoke-virtual {v4, v7}, Lcom/android/emailcommon/utility/EmailAsyncTask;->executeParallel([Ljava/lang/Object;)Lcom/android/emailcommon/utility/EmailAsyncTask;

    iput-boolean v5, p0, Lcom/android/email/activity/MessageViewFragmentBase;->mIsMessageLoadedForTest:Z

    return-void

    :cond_6
    move-object v3, p2

    iput-object p2, p0, Lcom/android/email/activity/MessageViewFragmentBase;->mHtmlTextRaw:Ljava/lang/String;

    sget-object v4, Lcom/android/email/activity/MessageViewFragmentBase;->IMG_TAG_START_REGEX:Ljava/util/regex/Pattern;

    invoke-virtual {v4, v3}, Ljava/util/regex/Pattern;->matcher(Ljava/lang/CharSequence;)Ljava/util/regex/Matcher;

    move-result-object v4

    invoke-virtual {v4}, Ljava/util/regex/Matcher;->find()Z

    move-result v0

    goto :goto_0

    :cond_7
    move v4, v6

    goto :goto_1

    :cond_8
    const/4 v4, 0x4

    invoke-virtual {p0, v4}, Lcom/android/email/activity/MessageViewFragmentBase;->addTabFlags(I)V

    goto :goto_2
.end method

.method private removeVip(Lcom/android/emailcommon/mail/Address;)V
    .locals 3
    .param p1    # Lcom/android/emailcommon/mail/Address;

    const/4 v2, 0x0

    iget-object v0, p0, Lcom/android/email/activity/MessageViewFragmentBase;->mVipSwitcher:Landroid/widget/ImageView;

    invoke-virtual {v0, v2}, Landroid/view/View;->setClickable(Z)V

    new-instance v0, Lcom/android/email/activity/MessageViewFragmentBase$5;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/android/email/activity/MessageViewFragmentBase$5;-><init>(Lcom/android/email/activity/MessageViewFragmentBase;Lcom/android/emailcommon/utility/EmailAsyncTask$Tracker;)V

    const/4 v1, 0x1

    new-array v1, v1, [Lcom/android/emailcommon/mail/Address;

    aput-object p1, v1, v2

    invoke-virtual {v0, v1}, Lcom/android/emailcommon/utility/EmailAsyncTask;->executeParallel([Ljava/lang/Object;)Lcom/android/emailcommon/utility/EmailAsyncTask;

    return-void
.end method

.method private restoreInstanceState(Landroid/os/Bundle;)V
    .locals 4
    .param p1    # Landroid/os/Bundle;

    sget-boolean v0, Lcom/android/emailcommon/Logging;->DEBUG_LIFECYCLE:Z

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/android/email/Email;->DEBUG:Z

    if-eqz v0, :cond_0

    const-string v0, "Email"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " restoreInstanceState"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    const-string v0, "MessageViewFragmentBase.currentTab"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lcom/android/email/activity/MessageViewFragmentBase;->mRestoredTab:I

    const-string v0, "MessageViewFragmentBase.pictureLoaded"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/android/email/activity/MessageViewFragmentBase;->mRestoredPictureLoaded:Z

    const-string v0, "MessageViewFragmentBase.particalDownload"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/android/email/activity/MessageViewFragmentBase;->mPartialDownload:Z

    const-string v0, "MessageViewFragmentBase.messageKey"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getLong(Ljava/lang/String;)J

    move-result-wide v0

    iput-wide v0, p0, Lcom/android/email/activity/MessageViewFragmentBase;->mMessageId:J

    invoke-virtual {p0}, Landroid/app/Fragment;->getArguments()Landroid/os/Bundle;

    move-result-object v0

    const-string v1, "messageId"

    iget-wide v2, p0, Lcom/android/email/activity/MessageViewFragmentBase;->mMessageId:J

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Bundle;->putLong(Ljava/lang/String;J)V

    const-string v0, "MessageViewFragmentBase"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "restoreInstanceState BUNDLE_KEY_PARTIAL_DOWNLOAD = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-boolean v2, p0, Lcom/android/email/activity/MessageViewFragmentBase;->mPartialDownload:Z

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "restoreInstanceState BUNDLE_KEY_mMessageId"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-wide v2, p0, Lcom/android/email/activity/MessageViewFragmentBase;->mMessageId:J

    invoke-virtual {v1, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/android/emailcommon/Logging;->d(Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method private setAttachmentCount(I)V
    .locals 4
    .param p1    # I

    const/16 v3, 0x65

    const/4 v2, 0x1

    iput p1, p0, Lcom/android/email/activity/MessageViewFragmentBase;->mAttachmentCount:I

    iget v0, p0, Lcom/android/email/activity/MessageViewFragmentBase;->mAttachmentCount:I

    if-lez v0, :cond_0

    invoke-virtual {p0, v2}, Lcom/android/email/activity/MessageViewFragmentBase;->addTabFlags(I)V

    :goto_0
    return-void

    :cond_0
    iget v0, p0, Lcom/android/email/activity/MessageViewFragmentBase;->mCurrentTab:I

    if-eq v0, v3, :cond_1

    iget v0, p0, Lcom/android/email/activity/MessageViewFragmentBase;->mCurrentTab:I

    const/16 v1, 0x66

    if-eq v0, v1, :cond_1

    const-string v0, "mAttachmentCount == 0 and switch to MessageTab"

    invoke-static {v0}, Lcom/android/emailcommon/Logging;->d(Ljava/lang/String;)V

    invoke-direct {p0, v3}, Lcom/android/email/activity/MessageViewFragmentBase;->setCurrentTab(I)V

    :cond_1
    invoke-direct {p0, v2}, Lcom/android/email/activity/MessageViewFragmentBase;->clearTabFlags(I)V

    goto :goto_0
.end method

.method private setCurrentTab(I)V
    .locals 6
    .param p1    # I

    const/16 v5, 0x67

    const/16 v3, 0x66

    const/4 v1, 0x1

    const/16 v4, 0x65

    const/4 v2, 0x0

    iput p1, p0, Lcom/android/email/activity/MessageViewFragmentBase;->mCurrentTab:I

    invoke-direct {p0, v4}, Lcom/android/email/activity/MessageViewFragmentBase;->getTabContentViewForFlag(I)Landroid/view/View;

    move-result-object v0

    invoke-static {v0, v2}, Lcom/android/email/activity/MessageViewFragmentBase;->makeVisible(Landroid/view/View;Z)V

    invoke-direct {p0, v5}, Lcom/android/email/activity/MessageViewFragmentBase;->getTabContentViewForFlag(I)Landroid/view/View;

    move-result-object v0

    invoke-static {v0, v2}, Lcom/android/email/activity/MessageViewFragmentBase;->makeVisible(Landroid/view/View;Z)V

    invoke-direct {p0, v3}, Lcom/android/email/activity/MessageViewFragmentBase;->getTabContentViewForFlag(I)Landroid/view/View;

    move-result-object v0

    invoke-static {v0, v2}, Lcom/android/email/activity/MessageViewFragmentBase;->makeVisible(Landroid/view/View;Z)V

    invoke-direct {p0, v4}, Lcom/android/email/activity/MessageViewFragmentBase;->getTabViewForFlag(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/view/View;->setSelected(Z)V

    invoke-direct {p0, v5}, Lcom/android/email/activity/MessageViewFragmentBase;->getTabViewForFlag(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/view/View;->setSelected(Z)V

    invoke-direct {p0, v3}, Lcom/android/email/activity/MessageViewFragmentBase;->getTabViewForFlag(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/view/View;->setSelected(Z)V

    iget-object v3, p0, Lcom/android/email/activity/MessageViewFragmentBase;->mRemainBtnView:Landroid/widget/LinearLayout;

    if-ne p1, v4, :cond_1

    move v0, v1

    :goto_0
    invoke-static {v3, v0}, Lcom/android/email/activity/MessageViewFragmentBase;->makeVisible(Landroid/view/View;Z)V

    iget v0, p0, Lcom/android/email/activity/MessageViewFragmentBase;->mCurrentTab:I

    invoke-direct {p0, v0}, Lcom/android/email/activity/MessageViewFragmentBase;->getTabContentViewForFlag(I)Landroid/view/View;

    move-result-object v0

    invoke-static {v0, v1}, Lcom/android/email/activity/MessageViewFragmentBase;->makeVisible(Landroid/view/View;Z)V

    iget v0, p0, Lcom/android/email/activity/MessageViewFragmentBase;->mCurrentTab:I

    invoke-direct {p0, v0}, Lcom/android/email/activity/MessageViewFragmentBase;->getTabViewForFlag(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v1}, Landroid/view/View;->setSelected(Z)V

    iget-boolean v0, p0, Lcom/android/email/activity/MessageViewFragmentBase;->mNeedReloadMessageContent:Z

    if-eqz v0, :cond_0

    if-ne p1, v4, :cond_0

    iget-object v0, p0, Lcom/android/email/activity/MessageViewFragmentBase;->mMessageContentView:Landroid/webkit/WebView;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/email/activity/MessageViewFragmentBase;->mHtmlTextWebView:Ljava/lang/String;

    invoke-direct {p0, v0}, Lcom/android/email/activity/MessageViewFragmentBase;->setMessageHtml(Ljava/lang/String;)V

    iput-boolean v2, p0, Lcom/android/email/activity/MessageViewFragmentBase;->mNeedReloadMessageContent:Z

    :cond_0
    return-void

    :cond_1
    move v0, v2

    goto :goto_0
.end method

.method private static setDetailsRow(Landroid/view/View;Ljava/lang/String;II)V
    .locals 2
    .param p0    # Landroid/view/View;
    .param p1    # Ljava/lang/String;
    .param p2    # I
    .param p3    # I

    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0, p3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    :goto_0
    return-void

    :cond_0
    invoke-virtual {p0, p3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    invoke-static {p0, p2}, Lcom/android/email/activity/UiUtilities;->getView(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0
.end method

.method private setMessageHtml(Ljava/lang/String;)V
    .locals 7
    .param p1    # Ljava/lang/String;

    const/4 v0, 0x1

    const/4 v1, 0x0

    if-nez p1, :cond_0

    const-string p1, ""

    :cond_0
    iget v2, p0, Lcom/android/email/activity/MessageViewFragmentBase;->mTabFlags:I

    and-int/lit8 v2, v2, 0x8

    if-eqz v2, :cond_2

    move v6, v0

    :goto_0
    if-eqz v6, :cond_3

    invoke-direct {p0, v0}, Lcom/android/email/activity/MessageViewFragmentBase;->loadsImagesAutomatically(Z)V

    :goto_1
    iget-object v0, p0, Lcom/android/email/activity/MessageViewFragmentBase;->mMessageContentView:Landroid/webkit/WebView;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/android/email/activity/MessageViewFragmentBase;->mMessageContentView:Landroid/webkit/WebView;

    invoke-virtual {v0}, Landroid/webkit/WebView;->clearView()V

    iget-object v0, p0, Lcom/android/email/activity/MessageViewFragmentBase;->mMessageContentView:Landroid/webkit/WebView;

    const-string v1, "email://"

    const-string v3, "text/html"

    const-string v4, "utf-8"

    const/4 v5, 0x0

    move-object v2, p1

    invoke-virtual/range {v0 .. v5}, Landroid/webkit/WebView;->loadDataWithBaseURL(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    invoke-direct {p0, p1}, Lcom/android/email/activity/MessageViewFragmentBase;->doLinkParse(Ljava/lang/String;)V

    :cond_1
    return-void

    :cond_2
    move v6, v1

    goto :goto_0

    :cond_3
    invoke-direct {p0, v1}, Lcom/android/email/activity/MessageViewFragmentBase;->loadsImagesAutomatically(Z)V

    iget-object v0, p0, Lcom/android/email/activity/MessageViewFragmentBase;->mMessageContentView:Landroid/webkit/WebView;

    invoke-virtual {v0, v1}, Landroid/webkit/WebView;->clearCache(Z)V

    goto :goto_1
.end method

.method private setShowImagesForSender()V
    .locals 9

    invoke-virtual {p0}, Landroid/app/Fragment;->getView()Landroid/view/View;

    move-result-object v7

    const v8, 0x7f0f00ca

    invoke-static {v7, v8}, Lcom/android/email/activity/UiUtilities;->getView(Landroid/view/View;I)Landroid/view/View;

    move-result-object v7

    const/4 v8, 0x0

    invoke-static {v7, v8}, Lcom/android/email/activity/MessageViewFragmentBase;->makeVisible(Landroid/view/View;Z)V

    invoke-virtual {p0}, Landroid/app/Fragment;->getActivity()Landroid/app/Activity;

    move-result-object v7

    const v8, 0x7f0800ef

    invoke-static {v7, v8}, Lcom/android/emailcommon/utility/Utility;->showToast(Landroid/content/Context;I)V

    iget v7, p0, Lcom/android/email/activity/MessageViewFragmentBase;->mTabFlags:I

    invoke-direct {p0, v7}, Lcom/android/email/activity/MessageViewFragmentBase;->updateTabs(I)V

    iget-object v7, p0, Lcom/android/email/activity/MessageViewFragmentBase;->mMessage:Lcom/android/emailcommon/provider/EmailContent$Message;

    iget-object v7, v7, Lcom/android/emailcommon/provider/EmailContent$Message;->mFrom:Ljava/lang/String;

    invoke-static {v7}, Lcom/android/emailcommon/mail/Address;->unpack(Ljava/lang/String;)[Lcom/android/emailcommon/mail/Address;

    move-result-object v2

    invoke-virtual {p0}, Landroid/app/Fragment;->getActivity()Landroid/app/Activity;

    move-result-object v7

    invoke-static {v7}, Lcom/android/email/Preferences;->getPreferences(Landroid/content/Context;)Lcom/android/email/Preferences;

    move-result-object v5

    move-object v0, v2

    array-length v4, v0

    const/4 v3, 0x0

    :goto_0
    if-ge v3, v4, :cond_0

    aget-object v6, v0, v3

    invoke-virtual {v6}, Lcom/android/emailcommon/mail/Address;->getAddress()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v5, v1}, Lcom/android/email/Preferences;->setSenderAsTrusted(Ljava/lang/String;)V

    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    :cond_0
    return-void
.end method

.method private shouldShowImagesFor(Ljava/lang/String;)Z
    .locals 1
    .param p1    # Ljava/lang/String;

    invoke-virtual {p0}, Landroid/app/Fragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    invoke-static {v0}, Lcom/android/email/Preferences;->getPreferences(Landroid/content/Context;)Lcom/android/email/Preferences;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/android/email/Preferences;->shouldShowImagesFor(Ljava/lang/String;)Z

    move-result v0

    return v0
.end method

.method private showContent(ZZ)V
    .locals 2
    .param p1    # Z
    .param p2    # Z

    iget-object v0, p0, Lcom/android/email/activity/MessageViewFragmentBase;->mMainView:Lcom/android/email/view/NonLockingScrollView;

    invoke-static {v0, p1}, Lcom/android/email/activity/MessageViewFragmentBase;->makeVisible(Landroid/view/View;Z)V

    iget-object v1, p0, Lcom/android/email/activity/MessageViewFragmentBase;->mLoadingProgress:Landroid/view/View;

    if-nez p1, :cond_0

    if-eqz p2, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-static {v1, v0}, Lcom/android/email/activity/MessageViewFragmentBase;->makeVisible(Landroid/view/View;Z)V

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private showDefaultQuickContactBadgeImage()V
    .locals 2

    iget-object v0, p0, Lcom/android/email/activity/MessageViewFragmentBase;->mFromBadge:Landroid/widget/ImageView;

    const v1, 0x7f020025

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    return-void
.end method

.method private showDetails()V
    .locals 12

    const/16 v11, 0x8

    const/4 v10, 0x2

    const/16 v9, 0x64

    const/4 v8, 0x1

    const/4 v7, 0x0

    invoke-virtual {p0}, Lcom/android/email/activity/MessageViewFragmentBase;->isMessageOpen()Z

    move-result v4

    if-nez v4, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    const/4 v2, 0x0

    iget-boolean v4, p0, Lcom/android/email/activity/MessageViewFragmentBase;->mDetailsFilled:Z

    if-nez v4, :cond_3

    iget-object v4, p0, Lcom/android/email/activity/MessageViewFragmentBase;->mMessage:Lcom/android/emailcommon/provider/EmailContent$Message;

    iget-wide v4, v4, Lcom/android/emailcommon/provider/EmailContent$Message;->mTimeStamp:J

    invoke-direct {p0, v4, v5, v8}, Lcom/android/email/activity/MessageViewFragmentBase;->formatDate(JZ)Ljava/lang/String;

    move-result-object v1

    iget-object v4, p0, Lcom/android/email/activity/MessageViewFragmentBase;->mDetailsExpanded:Landroid/view/View;

    const v5, 0x7f0f00af

    const v6, 0x7f0f00be

    invoke-static {v4, v1, v5, v6}, Lcom/android/email/activity/MessageViewFragmentBase;->setDetailsRow(Landroid/view/View;Ljava/lang/String;II)V

    iget-object v4, p0, Lcom/android/email/activity/MessageViewFragmentBase;->mMessage:Lcom/android/emailcommon/provider/EmailContent$Message;

    iget-object v4, v4, Lcom/android/emailcommon/provider/EmailContent$Message;->mTo:Ljava/lang/String;

    iget-object v5, p0, Lcom/android/email/activity/MessageViewFragmentBase;->mDetailsExpanded:Landroid/view/View;

    const v6, 0x7f0f0010

    invoke-static {v4, v5, v6}, Lcom/mediatek/email/emailvip/utils/MessageViewUtils;->setRowVisibility(Ljava/lang/String;Landroid/view/View;I)V

    iget-object v4, p0, Lcom/android/email/activity/MessageViewFragmentBase;->mMessage:Lcom/android/emailcommon/provider/EmailContent$Message;

    iget-object v4, v4, Lcom/android/emailcommon/provider/EmailContent$Message;->mCc:Ljava/lang/String;

    iget-object v5, p0, Lcom/android/email/activity/MessageViewFragmentBase;->mDetailsExpanded:Landroid/view/View;

    const v6, 0x7f0f00c0

    invoke-static {v4, v5, v6}, Lcom/mediatek/email/emailvip/utils/MessageViewUtils;->setRowVisibility(Ljava/lang/String;Landroid/view/View;I)V

    iget-object v4, p0, Lcom/android/email/activity/MessageViewFragmentBase;->mMessage:Lcom/android/emailcommon/provider/EmailContent$Message;

    iget-object v4, v4, Lcom/android/emailcommon/provider/EmailContent$Message;->mBcc:Ljava/lang/String;

    iget-object v5, p0, Lcom/android/email/activity/MessageViewFragmentBase;->mDetailsExpanded:Landroid/view/View;

    const v6, 0x7f0f00c2

    invoke-static {v4, v5, v6}, Lcom/mediatek/email/emailvip/utils/MessageViewUtils;->setRowVisibility(Ljava/lang/String;Landroid/view/View;I)V

    iget-object v4, p0, Lcom/android/email/activity/MessageViewFragmentBase;->mMessage:Lcom/android/emailcommon/provider/EmailContent$Message;

    invoke-static {v4}, Lcom/mediatek/email/emailvip/utils/MessageViewUtils;->getAllAddresses(Lcom/android/emailcommon/provider/EmailContent$Message;)[Lcom/mediatek/email/emailvip/utils/MessageViewUtils$TempAddress;

    move-result-object v0

    array-length v4, v0

    if-eqz v4, :cond_0

    const/4 v4, 0x3

    new-array v3, v4, [Landroid/widget/LinearLayout;

    iget-object v4, p0, Lcom/android/email/activity/MessageViewFragmentBase;->mDetailsExpanded:Landroid/view/View;

    const v5, 0x7f0f00bf

    invoke-virtual {v4, v5}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/LinearLayout;

    aput-object v4, v3, v7

    iget-object v4, p0, Lcom/android/email/activity/MessageViewFragmentBase;->mDetailsExpanded:Landroid/view/View;

    const v5, 0x7f0f00c1

    invoke-virtual {v4, v5}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/LinearLayout;

    aput-object v4, v3, v8

    iget-object v4, p0, Lcom/android/email/activity/MessageViewFragmentBase;->mDetailsExpanded:Landroid/view/View;

    const v5, 0x7f0f00c3

    invoke-virtual {v4, v5}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/LinearLayout;

    aput-object v4, v3, v10

    aget-object v4, v3, v7

    invoke-virtual {v4}, Landroid/view/ViewGroup;->removeAllViews()V

    aget-object v4, v3, v8

    invoke-virtual {v4}, Landroid/view/ViewGroup;->removeAllViews()V

    aget-object v4, v3, v10

    invoke-virtual {v4}, Landroid/view/ViewGroup;->removeAllViews()V

    array-length v4, v0

    if-gt v4, v9, :cond_2

    array-length v4, v0

    invoke-static {p0, v0, v3, v7, v4}, Lcom/mediatek/email/emailvip/utils/MessageViewUtils;->setDetailsLayout(Lcom/android/email/activity/MessageViewFragmentBase;[Lcom/mediatek/email/emailvip/utils/MessageViewUtils$TempAddress;[Landroid/widget/LinearLayout;II)V

    const/4 v2, 0x1

    :goto_1
    iput-boolean v8, p0, Lcom/android/email/activity/MessageViewFragmentBase;->mDetailsFilled:Z

    iget-object v4, p0, Lcom/android/email/activity/MessageViewFragmentBase;->mDetailsCollapsed:Landroid/view/View;

    invoke-virtual {v4, v11}, Landroid/view/View;->setVisibility(I)V

    iget-object v4, p0, Lcom/android/email/activity/MessageViewFragmentBase;->mDetailsExpanded:Landroid/view/View;

    invoke-virtual {v4, v7}, Landroid/view/View;->setVisibility(I)V

    if-nez v2, :cond_0

    array-length v4, v0

    invoke-static {p0, v0, v3, v9, v4}, Lcom/mediatek/email/emailvip/utils/MessageViewUtils;->setDetailsLayout(Lcom/android/email/activity/MessageViewFragmentBase;[Lcom/mediatek/email/emailvip/utils/MessageViewUtils$TempAddress;[Landroid/widget/LinearLayout;II)V

    goto/16 :goto_0

    :cond_2
    invoke-static {p0, v0, v3, v7, v9}, Lcom/mediatek/email/emailvip/utils/MessageViewUtils;->setDetailsLayout(Lcom/android/email/activity/MessageViewFragmentBase;[Lcom/mediatek/email/emailvip/utils/MessageViewUtils$TempAddress;[Landroid/widget/LinearLayout;II)V

    goto :goto_1

    :cond_3
    iget-object v4, p0, Lcom/android/email/activity/MessageViewFragmentBase;->mDetailsCollapsed:Landroid/view/View;

    invoke-virtual {v4, v11}, Landroid/view/View;->setVisibility(I)V

    iget-object v4, p0, Lcom/android/email/activity/MessageViewFragmentBase;->mDetailsExpanded:Landroid/view/View;

    invoke-virtual {v4, v7}, Landroid/view/View;->setVisibility(I)V

    iget-object v4, p0, Lcom/android/email/activity/MessageViewFragmentBase;->mContext:Landroid/content/Context;

    iget-object v5, p0, Lcom/android/email/activity/MessageViewFragmentBase;->mDetailsExpanded:Landroid/view/View;

    const/4 v6, 0x0

    invoke-static {v4, v5, v6}, Lcom/mediatek/email/emailvip/utils/MessageViewUtils;->updateDetailsExpanded(Landroid/content/Context;Landroid/view/View;Lcom/mediatek/email/emailvip/utils/MessageViewUtils$TempAddress;)V

    goto/16 :goto_0
.end method

.method private showPicturesInHtml()V
    .locals 4

    const/4 v1, 0x1

    const/4 v2, 0x0

    iget v3, p0, Lcom/android/email/activity/MessageViewFragmentBase;->mTabFlags:I

    and-int/lit8 v3, v3, 0x8

    if-eqz v3, :cond_1

    move v0, v1

    :goto_0
    iget-object v3, p0, Lcom/android/email/activity/MessageViewFragmentBase;->mMessageContentView:Landroid/webkit/WebView;

    if-eqz v3, :cond_0

    if-nez v0, :cond_0

    invoke-direct {p0, v2}, Lcom/android/email/activity/MessageViewFragmentBase;->blockNetworkLoads(Z)V

    invoke-virtual {p0}, Landroid/app/Fragment;->getView()Landroid/view/View;

    move-result-object v2

    const v3, 0x7f0f00ca

    invoke-static {v2, v3}, Lcom/android/email/activity/UiUtilities;->getView(Landroid/view/View;I)Landroid/view/View;

    move-result-object v2

    invoke-static {v2, v1}, Lcom/android/email/activity/MessageViewFragmentBase;->makeVisible(Landroid/view/View;Z)V

    const/16 v1, 0x8

    invoke-virtual {p0, v1}, Lcom/android/email/activity/MessageViewFragmentBase;->addTabFlags(I)V

    iget-object v1, p0, Lcom/android/email/activity/MessageViewFragmentBase;->mHtmlTextWebView:Ljava/lang/String;

    invoke-direct {p0, v1}, Lcom/android/email/activity/MessageViewFragmentBase;->setMessageHtml(Ljava/lang/String;)V

    :cond_0
    return-void

    :cond_1
    move v0, v2

    goto :goto_0
.end method

.method private triggerDownload(J)V
    .locals 8
    .param p1    # J

    iget-object v7, p0, Lcom/android/email/activity/MessageViewFragmentBase;->mDownloadAttachmentList:Ljava/util/ArrayList;

    monitor-enter v7

    :try_start_0
    iget-object v0, p0, Lcom/android/email/activity/MessageViewFragmentBase;->mDownloadAttachmentList:Ljava/util/ArrayList;

    const/4 v3, 0x0

    invoke-virtual {v0, v3}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v1

    const-string v0, "Email"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "++++++++ try to download attachmentId: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1, v2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " in background!!!"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v0, v3}, Lcom/android/emailcommon/Logging;->d(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/android/email/activity/MessageViewFragmentBase;->mController:Lcom/android/email/Controller;

    iget-wide v3, p0, Lcom/android/email/activity/MessageViewFragmentBase;->mMessageId:J

    iget-wide v5, p0, Lcom/android/email/activity/MessageViewFragmentBase;->mAccountId:J

    invoke-virtual/range {v0 .. v6}, Lcom/android/email/Controller;->loadAttachment(JJJ)V

    monitor-exit v7

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v7
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method private updateAttachmentButtons(Lcom/android/email/activity/MessageViewFragmentBase$MessageViewAttachmentInfo;Z)V
    .locals 12
    .param p1    # Lcom/android/email/activity/MessageViewFragmentBase$MessageViewAttachmentInfo;
    .param p2    # Z

    const/4 v8, 0x0

    const/16 v11, 0x8

    invoke-static {p1}, Lcom/android/email/activity/MessageViewFragmentBase$MessageViewAttachmentInfo;->access$4000(Lcom/android/email/activity/MessageViewFragmentBase$MessageViewAttachmentInfo;)Landroid/widget/ImageView;

    move-result-object v0

    invoke-static {p1}, Lcom/android/email/activity/MessageViewFragmentBase$MessageViewAttachmentInfo;->access$4100(Lcom/android/email/activity/MessageViewFragmentBase$MessageViewAttachmentInfo;)Landroid/widget/Button;

    move-result-object v5

    invoke-static {p1}, Lcom/android/email/activity/MessageViewFragmentBase$MessageViewAttachmentInfo;->access$4200(Lcom/android/email/activity/MessageViewFragmentBase$MessageViewAttachmentInfo;)Landroid/widget/Button;

    move-result-object v6

    invoke-static {p1}, Lcom/android/email/activity/MessageViewFragmentBase$MessageViewAttachmentInfo;->access$1300(Lcom/android/email/activity/MessageViewFragmentBase$MessageViewAttachmentInfo;)Landroid/widget/Button;

    move-result-object v4

    invoke-static {p1}, Lcom/android/email/activity/MessageViewFragmentBase$MessageViewAttachmentInfo;->access$4300(Lcom/android/email/activity/MessageViewFragmentBase$MessageViewAttachmentInfo;)Landroid/widget/Button;

    move-result-object v2

    invoke-static {p1}, Lcom/android/email/activity/MessageViewFragmentBase$MessageViewAttachmentInfo;->access$1400(Lcom/android/email/activity/MessageViewFragmentBase$MessageViewAttachmentInfo;)Landroid/widget/Button;

    move-result-object v1

    iget-boolean v7, p1, Lcom/android/email/AttachmentInfo;->mAllowView:Z

    if-nez v7, :cond_0

    invoke-virtual {v5, v11}, Landroid/view/View;->setVisibility(I)V

    :cond_0
    iget-boolean v7, p1, Lcom/android/email/AttachmentInfo;->mAllowSave:Z

    if-nez v7, :cond_1

    invoke-virtual {v6, v11}, Landroid/view/View;->setVisibility(I)V

    :cond_1
    iget-boolean v7, p1, Lcom/android/email/AttachmentInfo;->mAllowView:Z

    if-nez v7, :cond_3

    iget-boolean v7, p1, Lcom/android/email/AttachmentInfo;->mAllowSave:Z

    if-nez v7, :cond_3

    invoke-virtual {p1}, Lcom/android/email/activity/MessageViewFragmentBase$MessageViewAttachmentInfo;->hideProgress()V

    invoke-virtual {v5, v11}, Landroid/view/View;->setVisibility(I)V

    invoke-virtual {v6, v11}, Landroid/view/View;->setVisibility(I)V

    invoke-virtual {v4, v11}, Landroid/view/View;->setVisibility(I)V

    invoke-virtual {v1, v11}, Landroid/view/View;->setVisibility(I)V

    invoke-virtual {v2, v8}, Landroid/view/View;->setVisibility(I)V

    :cond_2
    :goto_0
    invoke-virtual {v5, p1}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    invoke-virtual {v6, p1}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    invoke-virtual {v4, p1}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    invoke-virtual {v2, p1}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    invoke-virtual {v1, p1}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    return-void

    :cond_3
    invoke-static {p1}, Lcom/android/email/activity/MessageViewFragmentBase$MessageViewAttachmentInfo;->access$1500(Lcom/android/email/activity/MessageViewFragmentBase$MessageViewAttachmentInfo;)Z

    move-result v7

    if-eqz v7, :cond_c

    const/16 v7, 0x64

    invoke-virtual {p1, v7}, Lcom/android/email/activity/MessageViewFragmentBase$MessageViewAttachmentInfo;->showProgress(I)V

    iget-boolean v7, p1, Lcom/android/email/AttachmentInfo;->mAllowSave:Z

    if-eqz v7, :cond_4

    invoke-virtual {v6, v8}, Landroid/view/View;->setVisibility(I)V

    invoke-virtual {p1}, Lcom/android/email/activity/MessageViewFragmentBase$MessageViewAttachmentInfo;->isFileSaved()Z

    move-result v3

    if-nez v3, :cond_7

    const/4 v7, 0x1

    :goto_1
    invoke-virtual {v6, v7}, Landroid/widget/TextView;->setEnabled(Z)V

    if-nez v3, :cond_8

    const v7, 0x7f0800e4

    invoke-virtual {v6, v7}, Landroid/widget/TextView;->setText(I)V

    :cond_4
    :goto_2
    iget-boolean v7, p1, Lcom/android/email/AttachmentInfo;->mAllowView:Z

    if-eqz v7, :cond_6

    iget-object v7, p1, Lcom/android/email/AttachmentInfo;->mContentType:Ljava/lang/String;

    const-string v9, "audio/"

    invoke-virtual {v7, v9}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v7

    if-nez v7, :cond_5

    iget-object v7, p1, Lcom/android/email/AttachmentInfo;->mContentType:Ljava/lang/String;

    const-string v9, "video/"

    invoke-virtual {v7, v9}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v7

    if-eqz v7, :cond_9

    :cond_5
    const v7, 0x7f0800e1

    invoke-virtual {v5, v7}, Landroid/widget/TextView;->setText(I)V

    :goto_3
    invoke-virtual {v5, v8}, Landroid/view/View;->setVisibility(I)V

    :cond_6
    iget v7, p1, Lcom/android/email/AttachmentInfo;->mDenyFlags:I

    if-nez v7, :cond_b

    invoke-virtual {v2, v11}, Landroid/view/View;->setVisibility(I)V

    :goto_4
    invoke-virtual {v4, v11}, Landroid/view/View;->setVisibility(I)V

    invoke-virtual {v1, v11}, Landroid/view/View;->setVisibility(I)V

    if-eqz p2, :cond_2

    invoke-direct {p0, p1}, Lcom/android/email/activity/MessageViewFragmentBase;->updatePreviewIcon(Lcom/android/email/activity/MessageViewFragmentBase$MessageViewAttachmentInfo;)V

    goto :goto_0

    :cond_7
    move v7, v8

    goto :goto_1

    :cond_8
    const v7, 0x7f0800e5

    invoke-virtual {v6, v7}, Landroid/widget/TextView;->setText(I)V

    goto :goto_2

    :cond_9
    iget-boolean v7, p1, Lcom/android/email/AttachmentInfo;->mAllowInstall:Z

    if-eqz v7, :cond_a

    const v7, 0x7f0800e0

    invoke-virtual {v5, v7}, Landroid/widget/TextView;->setText(I)V

    goto :goto_3

    :cond_a
    const v7, 0x7f0800df

    invoke-virtual {v5, v7}, Landroid/widget/TextView;->setText(I)V

    goto :goto_3

    :cond_b
    invoke-virtual {v2, v8}, Landroid/view/View;->setVisibility(I)V

    goto :goto_4

    :cond_c
    invoke-virtual {v6, v11}, Landroid/view/View;->setVisibility(I)V

    invoke-virtual {v5, v11}, Landroid/view/View;->setVisibility(I)V

    invoke-virtual {v2, v11}, Landroid/view/View;->setVisibility(I)V

    iget-wide v9, p1, Lcom/android/email/AttachmentInfo;->mId:J

    invoke-static {v9, v10}, Lcom/android/email/service/AttachmentDownloadService;->isAttachmentQueued(J)Z

    move-result v7

    if-eqz v7, :cond_d

    invoke-virtual {p1}, Lcom/android/email/activity/MessageViewFragmentBase$MessageViewAttachmentInfo;->showProgressIndeterminate()V

    invoke-virtual {v4, v11}, Landroid/view/View;->setVisibility(I)V

    invoke-virtual {v1, v8}, Landroid/view/View;->setVisibility(I)V

    goto/16 :goto_0

    :cond_d
    invoke-virtual {v4, v8}, Landroid/view/View;->setVisibility(I)V

    invoke-virtual {v1, v11}, Landroid/view/View;->setVisibility(I)V

    invoke-virtual {p1}, Lcom/android/email/activity/MessageViewFragmentBase$MessageViewAttachmentInfo;->hideProgress()V

    goto/16 :goto_0
.end method

.method private updateAttachmentTab()V
    .locals 7

    const/4 v1, 0x0

    iget-object v5, p0, Lcom/android/email/activity/MessageViewFragmentBase;->mAttachments:Landroid/widget/LinearLayout;

    invoke-virtual {v5}, Landroid/view/ViewGroup;->getChildCount()I

    move-result v0

    :goto_0
    if-ge v1, v0, :cond_0

    iget-object v5, p0, Lcom/android/email/activity/MessageViewFragmentBase;->mAttachments:Landroid/widget/LinearLayout;

    invoke-virtual {v5, v1}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v4

    invoke-virtual {v4}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/android/email/activity/MessageViewFragmentBase$MessageViewAttachmentInfo;

    new-instance v2, Lcom/android/email/activity/MessageViewFragmentBase$MessageViewAttachmentInfo;

    invoke-virtual {p0}, Landroid/app/Fragment;->getActivity()Landroid/app/Activity;

    move-result-object v5

    const/4 v6, 0x0

    invoke-direct {v2, v5, v3, v6}, Lcom/android/email/activity/MessageViewFragmentBase$MessageViewAttachmentInfo;-><init>(Landroid/content/Context;Lcom/android/email/activity/MessageViewFragmentBase$MessageViewAttachmentInfo;Lcom/android/email/activity/MessageViewFragmentBase$1;)V

    const/4 v5, 0x0

    invoke-direct {p0, v2, v5}, Lcom/android/email/activity/MessageViewFragmentBase;->updateAttachmentButtons(Lcom/android/email/activity/MessageViewFragmentBase$MessageViewAttachmentInfo;Z)V

    invoke-virtual {v4, v2}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_0
    return-void
.end method

.method private updateDeletedAttachment(Lcom/android/email/activity/MessageViewFragmentBase$MessageViewAttachmentInfo;)V
    .locals 4
    .param p1    # Lcom/android/email/activity/MessageViewFragmentBase$MessageViewAttachmentInfo;

    const/4 v1, 0x0

    const/16 v0, 0x40

    iput v0, p1, Lcom/android/email/AttachmentInfo;->mDenyFlags:I

    iput-boolean v1, p1, Lcom/android/email/AttachmentInfo;->mAllowView:Z

    iput-boolean v1, p1, Lcom/android/email/AttachmentInfo;->mAllowSave:Z

    iput-boolean v1, p1, Lcom/android/email/AttachmentInfo;->mAllowInstall:Z

    invoke-direct {p0, p1, v1}, Lcom/android/email/activity/MessageViewFragmentBase;->updateAttachmentButtons(Lcom/android/email/activity/MessageViewFragmentBase$MessageViewAttachmentInfo;Z)V

    const-string v0, "MessageViewFragmentBase"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Attachment "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-wide v2, p1, Lcom/android/email/AttachmentInfo;->mId:J

    invoke-virtual {v1, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " can\'t be found in DB, reset UI buttons"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/android/emailcommon/Logging;->d(Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method private updatePreviewIcon(Lcom/android/email/activity/MessageViewFragmentBase$MessageViewAttachmentInfo;)V
    .locals 2
    .param p1    # Lcom/android/email/activity/MessageViewFragmentBase$MessageViewAttachmentInfo;

    new-instance v0, Lcom/android/email/activity/MessageViewFragmentBase$UpdatePreviewIconTask;

    invoke-direct {v0, p0, p1}, Lcom/android/email/activity/MessageViewFragmentBase$UpdatePreviewIconTask;-><init>(Lcom/android/email/activity/MessageViewFragmentBase;Lcom/android/email/activity/MessageViewFragmentBase$MessageViewAttachmentInfo;)V

    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/Void;

    invoke-virtual {v0, v1}, Lcom/android/emailcommon/utility/EmailAsyncTask;->executeParallel([Ljava/lang/Object;)Lcom/android/emailcommon/utility/EmailAsyncTask;

    return-void
.end method

.method private updateRemainMsgInfo(Lcom/android/emailcommon/provider/EmailContent$Message;)V
    .locals 8
    .param p1    # Lcom/android/emailcommon/provider/EmailContent$Message;

    const/4 v7, 0x2

    iget-object v3, p0, Lcom/android/email/activity/MessageViewFragmentBase;->mRemainBtnView:Landroid/widget/LinearLayout;

    invoke-virtual {v3}, Landroid/view/ViewGroup;->removeAllViews()V

    if-nez p1, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget v3, p1, Lcom/android/emailcommon/provider/EmailContent$Message;->mFlagLoaded:I

    if-ne v3, v7, :cond_0

    invoke-virtual {p0}, Landroid/app/Fragment;->getActivity()Landroid/app/Activity;

    move-result-object v3

    invoke-virtual {v3}, Landroid/app/Activity;->getLayoutInflater()Landroid/view/LayoutInflater;

    move-result-object v0

    const v3, 0x7f04004e

    const/4 v4, 0x0

    invoke-virtual {v0, v3, v4}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v2

    const v3, 0x7f0f00e9

    invoke-virtual {v2, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    invoke-virtual {v1, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    const-string v3, "Email"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "++++ remaining message id is: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-wide v5, p0, Lcom/android/email/activity/MessageViewFragmentBase;->mMessageId:J

    invoke-virtual {v4, v5, v6}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget v3, p1, Lcom/android/emailcommon/provider/EmailContent$Message;->mFlagLoaded:I

    if-ne v3, v7, :cond_2

    const/4 v3, 0x1

    :goto_1
    invoke-static {v1, v3}, Lcom/android/email/activity/MessageViewFragmentBase;->makeVisible(Landroid/view/View;Z)V

    iget-object v3, p0, Lcom/android/email/activity/MessageViewFragmentBase;->mRemainBtnView:Landroid/widget/LinearLayout;

    invoke-virtual {v3, v2}, Landroid/view/ViewGroup;->addView(Landroid/view/View;)V

    goto :goto_0

    :cond_2
    const/4 v3, 0x0

    goto :goto_1
.end method

.method private updateRemainProgress()V
    .locals 8

    const/4 v7, 0x1

    iget-object v5, p0, Lcom/android/email/activity/MessageViewFragmentBase;->mRemainBtnView:Landroid/widget/LinearLayout;

    invoke-virtual {v5}, Landroid/view/ViewGroup;->removeAllViews()V

    invoke-virtual {p0}, Landroid/app/Fragment;->getActivity()Landroid/app/Activity;

    move-result-object v5

    invoke-virtual {v5}, Landroid/app/Activity;->getLayoutInflater()Landroid/view/LayoutInflater;

    move-result-object v0

    const v5, 0x7f04004e

    const/4 v6, 0x0

    invoke-virtual {v0, v5, v6}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v4

    const v5, 0x7f0f00e9

    invoke-virtual {v4, v5}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/TextView;

    const/16 v5, 0x8

    invoke-virtual {v3, v5}, Landroid/view/View;->setVisibility(I)V

    const v5, 0x7f0f00eb

    invoke-virtual {v4, v5}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/ProgressBar;

    const v5, 0x7f0f00ea

    invoke-virtual {v4, v5}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    invoke-static {v2, v7}, Lcom/android/email/activity/MessageViewFragmentBase;->makeVisible(Landroid/view/View;Z)V

    invoke-static {v1, v7}, Lcom/android/email/activity/MessageViewFragmentBase;->makeVisible(Landroid/view/View;Z)V

    iget-object v5, p0, Lcom/android/email/activity/MessageViewFragmentBase;->mRemainBtnView:Landroid/widget/LinearLayout;

    invoke-virtual {v5, v4}, Landroid/view/ViewGroup;->addView(Landroid/view/View;)V

    return-void
.end method

.method private updateTabs(I)V
    .locals 11
    .param p1    # I

    const/4 v3, 0x1

    const/4 v4, 0x0

    iput p1, p0, Lcom/android/email/activity/MessageViewFragmentBase;->mTabFlags:I

    invoke-virtual {p0}, Landroid/app/Fragment;->getView()Landroid/view/View;

    move-result-object v5

    if-nez v5, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    and-int/lit8 v5, p1, 0x3

    if-eqz v5, :cond_3

    move v1, v3

    :goto_1
    iget-object v5, p0, Lcom/android/email/activity/MessageViewFragmentBase;->mMessageTab:Landroid/widget/TextView;

    invoke-static {v5, v1}, Lcom/android/email/activity/MessageViewFragmentBase;->makeVisible(Landroid/view/View;Z)V

    iget-object v6, p0, Lcom/android/email/activity/MessageViewFragmentBase;->mInviteTab:Landroid/widget/TextView;

    and-int/lit8 v5, p1, 0x2

    if-eqz v5, :cond_4

    move v5, v3

    :goto_2
    invoke-static {v6, v5}, Lcom/android/email/activity/MessageViewFragmentBase;->makeVisible(Landroid/view/View;Z)V

    iget-object v6, p0, Lcom/android/email/activity/MessageViewFragmentBase;->mAttachmentTab:Landroid/widget/TextView;

    and-int/lit8 v5, p1, 0x1

    if-eqz v5, :cond_5

    move v5, v3

    :goto_3
    invoke-static {v6, v5}, Lcom/android/email/activity/MessageViewFragmentBase;->makeVisible(Landroid/view/View;Z)V

    and-int/lit8 v5, p1, 0x4

    if-eqz v5, :cond_6

    move v0, v3

    :goto_4
    and-int/lit8 v5, p1, 0x8

    if-eqz v5, :cond_7

    move v2, v3

    :goto_5
    iget-object v6, p0, Lcom/android/email/activity/MessageViewFragmentBase;->mShowPicturesTab:Landroid/widget/TextView;

    if-eqz v0, :cond_8

    if-nez v2, :cond_8

    move v5, v3

    :goto_6
    invoke-static {v6, v5}, Lcom/android/email/activity/MessageViewFragmentBase;->makeVisible(Landroid/view/View;Z)V

    iget-object v5, p0, Lcom/android/email/activity/MessageViewFragmentBase;->mAttachmentTab:Landroid/widget/TextView;

    iget-object v6, p0, Lcom/android/email/activity/MessageViewFragmentBase;->mContext:Landroid/content/Context;

    invoke-virtual {v6}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v6

    const v7, 0x7f0d0006

    iget v8, p0, Lcom/android/email/activity/MessageViewFragmentBase;->mAttachmentCount:I

    new-array v9, v3, [Ljava/lang/Object;

    iget v10, p0, Lcom/android/email/activity/MessageViewFragmentBase;->mAttachmentCount:I

    invoke-static {v10}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v10

    aput-object v10, v9, v4

    invoke-virtual {v6, v7, v8, v9}, Landroid/content/res/Resources;->getQuantityString(II[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v5, p0, Lcom/android/email/activity/MessageViewFragmentBase;->mTabSection:Landroid/view/View;

    iget-object v6, p0, Lcom/android/email/activity/MessageViewFragmentBase;->mMessageTab:Landroid/widget/TextView;

    invoke-static {v6}, Lcom/android/email/activity/MessageViewFragmentBase;->isVisible(Landroid/view/View;)Z

    move-result v6

    if-nez v6, :cond_2

    iget-object v6, p0, Lcom/android/email/activity/MessageViewFragmentBase;->mInviteTab:Landroid/widget/TextView;

    invoke-static {v6}, Lcom/android/email/activity/MessageViewFragmentBase;->isVisible(Landroid/view/View;)Z

    move-result v6

    if-nez v6, :cond_2

    iget-object v6, p0, Lcom/android/email/activity/MessageViewFragmentBase;->mAttachmentTab:Landroid/widget/TextView;

    invoke-static {v6}, Lcom/android/email/activity/MessageViewFragmentBase;->isVisible(Landroid/view/View;)Z

    move-result v6

    if-nez v6, :cond_2

    iget-object v6, p0, Lcom/android/email/activity/MessageViewFragmentBase;->mShowPicturesTab:Landroid/widget/TextView;

    invoke-static {v6}, Lcom/android/email/activity/MessageViewFragmentBase;->isVisible(Landroid/view/View;)Z

    move-result v6

    if-nez v6, :cond_2

    iget-object v6, p0, Lcom/android/email/activity/MessageViewFragmentBase;->mAlwaysShowPicturesButton:Landroid/view/View;

    invoke-static {v6}, Lcom/android/email/activity/MessageViewFragmentBase;->isVisible(Landroid/view/View;)Z

    move-result v6

    if-eqz v6, :cond_9

    :cond_2
    :goto_7
    invoke-static {v5, v3}, Lcom/android/email/activity/MessageViewFragmentBase;->makeVisible(Landroid/view/View;Z)V

    iget v3, p0, Lcom/android/email/activity/MessageViewFragmentBase;->mRestoredTab:I

    if-eqz v3, :cond_0

    iget v3, p0, Lcom/android/email/activity/MessageViewFragmentBase;->mRestoredTab:I

    invoke-direct {p0, v3}, Lcom/android/email/activity/MessageViewFragmentBase;->getTabViewForFlag(I)Landroid/view/View;

    move-result-object v3

    invoke-static {v3}, Lcom/android/email/activity/MessageViewFragmentBase;->isVisible(Landroid/view/View;)Z

    move-result v3

    if-eqz v3, :cond_0

    iget v3, p0, Lcom/android/email/activity/MessageViewFragmentBase;->mRestoredTab:I

    invoke-direct {p0, v3}, Lcom/android/email/activity/MessageViewFragmentBase;->setCurrentTab(I)V

    iput v4, p0, Lcom/android/email/activity/MessageViewFragmentBase;->mRestoredTab:I

    goto/16 :goto_0

    :cond_3
    move v1, v4

    goto/16 :goto_1

    :cond_4
    move v5, v4

    goto/16 :goto_2

    :cond_5
    move v5, v4

    goto/16 :goto_3

    :cond_6
    move v0, v4

    goto :goto_4

    :cond_7
    move v2, v4

    goto :goto_5

    :cond_8
    move v5, v4

    goto :goto_6

    :cond_9
    move v3, v4

    goto :goto_7
.end method

.method private updateVipIcon(Z)V
    .locals 3
    .param p1    # Z

    const/4 v2, 0x0

    if-eqz p1, :cond_0

    iget-object v0, p0, Lcom/android/email/activity/MessageViewFragmentBase;->mFromNameView:Landroid/widget/TextView;

    const v1, 0x7f02002c

    invoke-virtual {v0, v1, v2, v2, v2}, Landroid/widget/TextView;->setCompoundDrawablesWithIntrinsicBounds(IIII)V

    iget-object v0, p0, Lcom/android/email/activity/MessageViewFragmentBase;->mVipSwitcher:Landroid/widget/ImageView;

    iget-object v1, p0, Lcom/android/email/activity/MessageViewFragmentBase;->mRemoveVipIcon:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    iget-object v0, p0, Lcom/android/email/activity/MessageViewFragmentBase;->mVipSwitcher:Landroid/widget/ImageView;

    iget-object v1, p0, Lcom/android/email/activity/MessageViewFragmentBase;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f08003d

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/view/View;->setContentDescription(Ljava/lang/CharSequence;)V

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/android/email/activity/MessageViewFragmentBase;->mFromNameView:Landroid/widget/TextView;

    invoke-virtual {v0, v2, v2, v2, v2}, Landroid/widget/TextView;->setCompoundDrawablesWithIntrinsicBounds(IIII)V

    iget-object v0, p0, Lcom/android/email/activity/MessageViewFragmentBase;->mVipSwitcher:Landroid/widget/ImageView;

    iget-object v1, p0, Lcom/android/email/activity/MessageViewFragmentBase;->mAddVipIcon:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    iget-object v0, p0, Lcom/android/email/activity/MessageViewFragmentBase;->mVipSwitcher:Landroid/widget/ImageView;

    iget-object v1, p0, Lcom/android/email/activity/MessageViewFragmentBase;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f08003e

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/view/View;->setContentDescription(Ljava/lang/CharSequence;)V

    goto :goto_0
.end method


# virtual methods
.method protected final addTabFlags(I)V
    .locals 1
    .param p1    # I

    iget v0, p0, Lcom/android/email/activity/MessageViewFragmentBase;->mTabFlags:I

    or-int/2addr v0, p1

    invoke-direct {p0, v0}, Lcom/android/email/activity/MessageViewFragmentBase;->updateTabs(I)V

    return-void
.end method

.method public clearIsMessageLoadedForTest()V
    .locals 1

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/android/email/activity/MessageViewFragmentBase;->mIsMessageLoadedForTest:Z

    return-void
.end method

.method public getAccountId()J
    .locals 2

    iget-wide v0, p0, Lcom/android/email/activity/MessageViewFragmentBase;->mAccountId:J

    return-wide v0
.end method

.method public getAttachmentInfo(J)Lcom/android/email/activity/MessageViewFragmentBase$MessageViewAttachmentInfo;
    .locals 1
    .param p1    # J

    invoke-direct {p0, p1, p2}, Lcom/android/email/activity/MessageViewFragmentBase;->findAttachmentInfo(J)Lcom/android/email/activity/MessageViewFragmentBase$MessageViewAttachmentInfo;

    move-result-object v0

    return-object v0
.end method

.method public getAttachmentsView()Landroid/widget/LinearLayout;
    .locals 1

    iget-object v0, p0, Lcom/android/email/activity/MessageViewFragmentBase;->mAttachments:Landroid/widget/LinearLayout;

    return-object v0
.end method

.method protected final getCallback()Lcom/android/email/activity/MessageViewFragmentBase$Callback;
    .locals 1

    iget-object v0, p0, Lcom/android/email/activity/MessageViewFragmentBase;->mCallback:Lcom/android/email/activity/MessageViewFragmentBase$Callback;

    return-object v0
.end method

.method protected final getController()Lcom/android/email/Controller;
    .locals 1

    iget-object v0, p0, Lcom/android/email/activity/MessageViewFragmentBase;->mController:Lcom/android/email/Controller;

    return-object v0
.end method

.method public final getMessage()Lcom/android/emailcommon/provider/EmailContent$Message;
    .locals 1

    iget-object v0, p0, Lcom/android/email/activity/MessageViewFragmentBase;->mMessage:Lcom/android/emailcommon/provider/EmailContent$Message;

    return-object v0
.end method

.method public injectMockController(Lcom/android/email/Controller;)V
    .locals 2
    .param p1    # Lcom/android/email/Controller;

    iput-object p1, p0, Lcom/android/email/activity/MessageViewFragmentBase;->mController:Lcom/android/email/Controller;

    iget-object v0, p0, Lcom/android/email/activity/MessageViewFragmentBase;->mController:Lcom/android/email/Controller;

    iget-object v1, p0, Lcom/android/email/activity/MessageViewFragmentBase;->mControllerCallback:Lcom/android/email/ControllerResultUiThreadWrapper;

    invoke-virtual {v0, v1}, Lcom/android/email/Controller;->addResultCallback(Lcom/android/email/Controller$Result;)V

    return-void
.end method

.method public isMessageLoadedForTest()Z
    .locals 1

    iget-boolean v0, p0, Lcom/android/email/activity/MessageViewFragmentBase;->mIsMessageLoadedForTest:Z

    return v0
.end method

.method public isMessageLoading()Z
    .locals 1

    iget-boolean v0, p0, Lcom/android/email/activity/MessageViewFragmentBase;->mMessageLoading:Z

    return v0
.end method

.method protected final isMessageOpen()Z
    .locals 1

    iget-object v0, p0, Lcom/android/email/activity/MessageViewFragmentBase;->mMessage:Lcom/android/emailcommon/provider/EmailContent$Message;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public loadMessage()V
    .locals 3

    const/4 v2, 0x0

    iget-object v0, p0, Lcom/android/email/activity/MessageViewFragmentBase;->mMainView:Lcom/android/email/view/NonLockingScrollView;

    invoke-virtual {v0, v2}, Landroid/widget/ScrollView;->fling(I)V

    invoke-virtual {p0}, Lcom/android/email/activity/MessageViewFragmentBase;->resetView()V

    new-instance v0, Lcom/android/email/activity/MessageViewFragmentBase$LoadMessageTask;

    const/4 v1, 0x1

    invoke-direct {v0, p0, v1}, Lcom/android/email/activity/MessageViewFragmentBase$LoadMessageTask;-><init>(Lcom/android/email/activity/MessageViewFragmentBase;Z)V

    iput-object v0, p0, Lcom/android/email/activity/MessageViewFragmentBase;->mLoadMessageTask:Lcom/android/email/activity/MessageViewFragmentBase$LoadMessageTask;

    iget-object v0, p0, Lcom/android/email/activity/MessageViewFragmentBase;->mLoadMessageTask:Lcom/android/email/activity/MessageViewFragmentBase$LoadMessageTask;

    new-array v1, v2, [Ljava/lang/Void;

    invoke-virtual {v0, v1}, Lcom/android/emailcommon/utility/EmailAsyncTask;->executeParallel([Ljava/lang/Object;)Lcom/android/emailcommon/utility/EmailAsyncTask;

    return-void
.end method

.method public needUpdateVipIcon(Lcom/android/emailcommon/mail/Address;)Z
    .locals 4
    .param p1    # Lcom/android/emailcommon/mail/Address;

    const/4 v2, 0x0

    if-nez p1, :cond_1

    :cond_0
    :goto_0
    return v2

    :cond_1
    iget-object v3, p0, Lcom/android/email/activity/MessageViewFragmentBase;->mMessage:Lcom/android/emailcommon/provider/EmailContent$Message;

    iget-object v3, v3, Lcom/android/emailcommon/provider/EmailContent$Message;->mFrom:Ljava/lang/String;

    invoke-static {v3}, Lcom/android/emailcommon/mail/Address;->unpackFirst(Ljava/lang/String;)Lcom/android/emailcommon/mail/Address;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {p1}, Lcom/android/emailcommon/mail/Address;->getAddress()Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-virtual {v0}, Lcom/android/emailcommon/mail/Address;->getAddress()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    const/4 v2, 0x1

    goto :goto_0
.end method

.method public onActivityCreated(Landroid/os/Bundle;)V
    .locals 3
    .param p1    # Landroid/os/Bundle;

    sget-boolean v0, Lcom/android/emailcommon/Logging;->DEBUG_LIFECYCLE:Z

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/android/email/Email;->DEBUG:Z

    if-eqz v0, :cond_0

    const-string v0, "Email"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " onActivityCreated"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    invoke-super {p0, p1}, Landroid/app/Fragment;->onActivityCreated(Landroid/os/Bundle;)V

    iget-object v0, p0, Lcom/android/email/activity/MessageViewFragmentBase;->mController:Lcom/android/email/Controller;

    iget-object v1, p0, Lcom/android/email/activity/MessageViewFragmentBase;->mControllerCallback:Lcom/android/email/ControllerResultUiThreadWrapper;

    invoke-virtual {v0, v1}, Lcom/android/email/Controller;->addResultCallback(Lcom/android/email/Controller$Result;)V

    invoke-virtual {p0}, Lcom/android/email/activity/MessageViewFragmentBase;->loadMessage()V

    invoke-static {p0}, Lcom/android/email/activity/UiUtilities;->installFragment(Landroid/app/Fragment;)V

    return-void
.end method

.method public onAttach(Landroid/app/Activity;)V
    .locals 3
    .param p1    # Landroid/app/Activity;

    sget-boolean v0, Lcom/android/emailcommon/Logging;->DEBUG_LIFECYCLE:Z

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/android/email/Email;->DEBUG:Z

    if-eqz v0, :cond_0

    const-string v0, "Email"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " onAttach"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    invoke-super {p0, p1}, Landroid/app/Fragment;->onAttach(Landroid/app/Activity;)V

    return-void
.end method

.method public onClick(Landroid/view/View;)V
    .locals 2
    .param p1    # Landroid/view/View;

    invoke-virtual {p0}, Lcom/android/email/activity/MessageViewFragmentBase;->isMessageOpen()Z

    move-result v0

    if-nez v0, :cond_0

    :goto_0
    return-void

    :cond_0
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    goto :goto_0

    :sswitch_0
    invoke-virtual {p1}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/email/activity/MessageViewFragmentBase$MessageViewAttachmentInfo;

    invoke-direct {p0, v0}, Lcom/android/email/activity/MessageViewFragmentBase;->onCancelAttachment(Lcom/android/email/activity/MessageViewFragmentBase$MessageViewAttachmentInfo;)V

    goto :goto_0

    :sswitch_1
    invoke-direct {p0}, Lcom/android/email/activity/MessageViewFragmentBase;->onClickSender()V

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/email/activity/MessageViewFragmentBase$MessageViewAttachmentInfo;

    invoke-direct {p0, v0}, Lcom/android/email/activity/MessageViewFragmentBase;->onLoadAttachment(Lcom/android/email/activity/MessageViewFragmentBase$MessageViewAttachmentInfo;)V

    goto :goto_0

    :sswitch_3
    invoke-virtual {p1}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/email/activity/MessageViewFragmentBase$MessageViewAttachmentInfo;

    invoke-direct {p0, v0}, Lcom/android/email/activity/MessageViewFragmentBase;->onInfoAttachment(Lcom/android/email/activity/MessageViewFragmentBase$MessageViewAttachmentInfo;)V

    goto :goto_0

    :sswitch_4
    invoke-direct {p0, p1}, Lcom/android/email/activity/MessageViewFragmentBase;->onSaveClicked(Landroid/view/View;)V

    goto :goto_0

    :sswitch_5
    invoke-virtual {p1}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/email/activity/MessageViewFragmentBase$MessageViewAttachmentInfo;

    invoke-direct {p0, v0}, Lcom/android/email/activity/MessageViewFragmentBase;->onOpenAttachment(Lcom/android/email/activity/MessageViewFragmentBase$MessageViewAttachmentInfo;)V

    goto :goto_0

    :sswitch_6
    const/16 v0, 0x65

    invoke-direct {p0, v0}, Lcom/android/email/activity/MessageViewFragmentBase;->setCurrentTab(I)V

    goto :goto_0

    :sswitch_7
    const/16 v0, 0x66

    invoke-direct {p0, v0}, Lcom/android/email/activity/MessageViewFragmentBase;->setCurrentTab(I)V

    goto :goto_0

    :sswitch_8
    const/16 v0, 0x67

    invoke-direct {p0, v0}, Lcom/android/email/activity/MessageViewFragmentBase;->setCurrentTab(I)V

    goto :goto_0

    :sswitch_9
    invoke-direct {p0}, Lcom/android/email/activity/MessageViewFragmentBase;->showPicturesInHtml()V

    goto :goto_0

    :sswitch_a
    invoke-direct {p0}, Lcom/android/email/activity/MessageViewFragmentBase;->setShowImagesForSender()V

    goto :goto_0

    :sswitch_b
    invoke-direct {p0}, Lcom/android/email/activity/MessageViewFragmentBase;->showDetails()V

    goto :goto_0

    :sswitch_c
    invoke-direct {p0}, Lcom/android/email/activity/MessageViewFragmentBase;->hideDetails()V

    goto :goto_0

    :sswitch_d
    iget-wide v0, p0, Lcom/android/email/activity/MessageViewFragmentBase;->mMessageId:J

    invoke-direct {p0, v0, v1}, Lcom/android/email/activity/MessageViewFragmentBase;->doLoadMsgBackground(J)V

    goto :goto_0

    :sswitch_e
    invoke-direct {p0}, Lcom/android/email/activity/MessageViewFragmentBase;->onVipSwitcherClick()V

    goto :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x7f0f0001 -> :sswitch_0
        0x7f0f001a -> :sswitch_4
        0x7f0f00ba -> :sswitch_2
        0x7f0f00bb -> :sswitch_3
        0x7f0f00bc -> :sswitch_5
        0x7f0f00c9 -> :sswitch_9
        0x7f0f00ca -> :sswitch_a
        0x7f0f00cb -> :sswitch_6
        0x7f0f00cc -> :sswitch_7
        0x7f0f00cd -> :sswitch_8
        0x7f0f00d8 -> :sswitch_1
        0x7f0f00dc -> :sswitch_e
        0x7f0f00e3 -> :sswitch_b
        0x7f0f00e6 -> :sswitch_c
        0x7f0f00e9 -> :sswitch_d
    .end sparse-switch
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 5
    .param p1    # Landroid/os/Bundle;

    sget-boolean v1, Lcom/android/emailcommon/Logging;->DEBUG_LIFECYCLE:Z

    if-eqz v1, :cond_0

    sget-boolean v1, Lcom/android/email/Email;->DEBUG:Z

    if-eqz v1, :cond_0

    const-string v1, "Email"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " onCreate"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    invoke-super {p0, p1}, Landroid/app/Fragment;->onCreate(Landroid/os/Bundle;)V

    invoke-virtual {p0}, Landroid/app/Fragment;->getActivity()Landroid/app/Activity;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/ContextWrapper;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    iput-object v1, p0, Lcom/android/email/activity/MessageViewFragmentBase;->mContext:Landroid/content/Context;

    iget-object v1, p0, Lcom/android/email/activity/MessageViewFragmentBase;->mContext:Landroid/content/Context;

    invoke-static {v1}, Lcom/android/email/Controller;->getInstance(Landroid/content/Context;)Lcom/android/email/Controller;

    move-result-object v1

    iput-object v1, p0, Lcom/android/email/activity/MessageViewFragmentBase;->mController:Lcom/android/email/Controller;

    new-instance v1, Lcom/android/email/ControllerResultUiThreadWrapper;

    new-instance v2, Landroid/os/Handler;

    invoke-direct {v2}, Landroid/os/Handler;-><init>()V

    new-instance v3, Lcom/android/email/activity/MessageViewFragmentBase$ControllerResults;

    const/4 v4, 0x0

    invoke-direct {v3, p0, v4}, Lcom/android/email/activity/MessageViewFragmentBase$ControllerResults;-><init>(Lcom/android/email/activity/MessageViewFragmentBase;Lcom/android/email/activity/MessageViewFragmentBase$1;)V

    invoke-direct {v1, v2, v3}, Lcom/android/email/ControllerResultUiThreadWrapper;-><init>(Landroid/os/Handler;Lcom/android/email/Controller$Result;)V

    iput-object v1, p0, Lcom/android/email/activity/MessageViewFragmentBase;->mControllerCallback:Lcom/android/email/ControllerResultUiThreadWrapper;

    new-instance v1, Lcom/android/email/activity/MessageViewFragmentBase$MessageObserver;

    new-instance v2, Landroid/os/Handler;

    invoke-direct {v2}, Landroid/os/Handler;-><init>()V

    iget-object v3, p0, Lcom/android/email/activity/MessageViewFragmentBase;->mContext:Landroid/content/Context;

    invoke-direct {v1, p0, v2, v3}, Lcom/android/email/activity/MessageViewFragmentBase$MessageObserver;-><init>(Lcom/android/email/activity/MessageViewFragmentBase;Landroid/os/Handler;Landroid/content/Context;)V

    iput-object v1, p0, Lcom/android/email/activity/MessageViewFragmentBase;->mMessageObserver:Lcom/android/email/activity/MessageViewFragmentBase$MessageObserver;

    if-eqz p1, :cond_1

    invoke-direct {p0, p1}, Lcom/android/email/activity/MessageViewFragmentBase;->restoreInstanceState(Landroid/os/Bundle;)V

    :cond_1
    iget-object v1, p0, Lcom/android/email/activity/MessageViewFragmentBase;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f020029

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    iput-object v1, p0, Lcom/android/email/activity/MessageViewFragmentBase;->mRemoveVipIcon:Landroid/graphics/drawable/Drawable;

    const v1, 0x7f020027

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    iput-object v1, p0, Lcom/android/email/activity/MessageViewFragmentBase;->mAddVipIcon:Landroid/graphics/drawable/Drawable;

    return-void
.end method

.method public onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 8
    .param p1    # Landroid/view/LayoutInflater;
    .param p2    # Landroid/view/ViewGroup;
    .param p3    # Landroid/os/Bundle;

    const/4 v5, 0x0

    const/4 v4, 0x1

    sget-boolean v3, Lcom/android/emailcommon/Logging;->DEBUG_LIFECYCLE:Z

    if-eqz v3, :cond_0

    sget-boolean v3, Lcom/android/email/Email;->DEBUG:Z

    if-eqz v3, :cond_0

    const-string v3, "Email"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v6, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, " onCreateView"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v3, v6}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    const v3, 0x7f040046

    invoke-virtual {p1, v3, p2, v5}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    invoke-direct {p0}, Lcom/android/email/activity/MessageViewFragmentBase;->cleanupDetachedViews()V

    const v3, 0x7f0f0081

    invoke-static {v1, v3}, Lcom/android/email/activity/UiUtilities;->getView(Landroid/view/View;I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/TextView;

    iput-object v3, p0, Lcom/android/email/activity/MessageViewFragmentBase;->mSubjectView:Landroid/widget/TextView;

    const v3, 0x7f0f00d9

    invoke-static {v1, v3}, Lcom/android/email/activity/UiUtilities;->getView(Landroid/view/View;I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/TextView;

    iput-object v3, p0, Lcom/android/email/activity/MessageViewFragmentBase;->mFromNameView:Landroid/widget/TextView;

    const v3, 0x7f0f00db

    invoke-static {v1, v3}, Lcom/android/email/activity/UiUtilities;->getView(Landroid/view/View;I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/TextView;

    iput-object v3, p0, Lcom/android/email/activity/MessageViewFragmentBase;->mFromAddressView:Landroid/widget/TextView;

    const v3, 0x7f0f00dc

    invoke-static {v1, v3}, Lcom/android/email/activity/UiUtilities;->getView(Landroid/view/View;I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/ImageView;

    iput-object v3, p0, Lcom/android/email/activity/MessageViewFragmentBase;->mVipSwitcher:Landroid/widget/ImageView;

    const v3, 0x7f0f00e4

    invoke-static {v1, v3}, Lcom/android/email/activity/UiUtilities;->getView(Landroid/view/View;I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/TextView;

    iput-object v3, p0, Lcom/android/email/activity/MessageViewFragmentBase;->mAddressesView:Landroid/widget/TextView;

    const v3, 0x7f0f00e5

    invoke-static {v1, v3}, Lcom/android/email/activity/UiUtilities;->getView(Landroid/view/View;I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/TextView;

    iput-object v3, p0, Lcom/android/email/activity/MessageViewFragmentBase;->mDateTimeView:Landroid/widget/TextView;

    const v3, 0x7f0f00a5

    invoke-static {v1, v3}, Lcom/android/email/activity/UiUtilities;->getView(Landroid/view/View;I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/webkit/WebView;

    iput-object v3, p0, Lcom/android/email/activity/MessageViewFragmentBase;->mMessageContentView:Landroid/webkit/WebView;

    iget-object v3, p0, Lcom/android/email/activity/MessageViewFragmentBase;->mMessageContentView:Landroid/webkit/WebView;

    invoke-virtual {v3}, Landroid/webkit/WebView;->enableEmailUsingFlag()V

    const v3, 0x7f0f009e

    invoke-static {v1, v3}, Lcom/android/email/activity/UiUtilities;->getView(Landroid/view/View;I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/LinearLayout;

    iput-object v3, p0, Lcom/android/email/activity/MessageViewFragmentBase;->mAttachments:Landroid/widget/LinearLayout;

    const v3, 0x7f0f00ce

    invoke-static {v1, v3}, Lcom/android/email/activity/UiUtilities;->getView(Landroid/view/View;I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/LinearLayout;

    iput-object v3, p0, Lcom/android/email/activity/MessageViewFragmentBase;->mRemainBtnView:Landroid/widget/LinearLayout;

    const v3, 0x7f0f00c8

    invoke-static {v1, v3}, Lcom/android/email/activity/UiUtilities;->getView(Landroid/view/View;I)Landroid/view/View;

    move-result-object v3

    iput-object v3, p0, Lcom/android/email/activity/MessageViewFragmentBase;->mTabSection:Landroid/view/View;

    const v3, 0x7f0f00d8

    invoke-static {v1, v3}, Lcom/android/email/activity/UiUtilities;->getView(Landroid/view/View;I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/ImageView;

    iput-object v3, p0, Lcom/android/email/activity/MessageViewFragmentBase;->mFromBadge:Landroid/widget/ImageView;

    const v3, 0x7f0f00da

    invoke-static {v1, v3}, Lcom/android/email/activity/UiUtilities;->getView(Landroid/view/View;I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/ImageView;

    iput-object v3, p0, Lcom/android/email/activity/MessageViewFragmentBase;->mSenderPresenceView:Landroid/widget/ImageView;

    const v3, 0x7f0f00c5

    invoke-static {v1, v3}, Lcom/android/email/activity/UiUtilities;->getView(Landroid/view/View;I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Lcom/android/email/view/NonLockingScrollView;

    iput-object v3, p0, Lcom/android/email/activity/MessageViewFragmentBase;->mMainView:Lcom/android/email/view/NonLockingScrollView;

    const v3, 0x7f0f00c4

    invoke-static {v1, v3}, Lcom/android/email/activity/UiUtilities;->getView(Landroid/view/View;I)Landroid/view/View;

    move-result-object v3

    iput-object v3, p0, Lcom/android/email/activity/MessageViewFragmentBase;->mLoadingProgress:Landroid/view/View;

    const v3, 0x7f0f00e3

    invoke-static {v1, v3}, Lcom/android/email/activity/UiUtilities;->getView(Landroid/view/View;I)Landroid/view/View;

    move-result-object v3

    iput-object v3, p0, Lcom/android/email/activity/MessageViewFragmentBase;->mDetailsCollapsed:Landroid/view/View;

    const v3, 0x7f0f00e6

    invoke-static {v1, v3}, Lcom/android/email/activity/UiUtilities;->getView(Landroid/view/View;I)Landroid/view/View;

    move-result-object v3

    iput-object v3, p0, Lcom/android/email/activity/MessageViewFragmentBase;->mDetailsExpanded:Landroid/view/View;

    iget-object v3, p0, Lcom/android/email/activity/MessageViewFragmentBase;->mFromNameView:Landroid/widget/TextView;

    invoke-virtual {v3, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v3, p0, Lcom/android/email/activity/MessageViewFragmentBase;->mFromAddressView:Landroid/widget/TextView;

    invoke-virtual {v3, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v3, p0, Lcom/android/email/activity/MessageViewFragmentBase;->mFromBadge:Landroid/widget/ImageView;

    invoke-virtual {v3, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v3, p0, Lcom/android/email/activity/MessageViewFragmentBase;->mSenderPresenceView:Landroid/widget/ImageView;

    invoke-virtual {v3, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v3, p0, Lcom/android/email/activity/MessageViewFragmentBase;->mVipSwitcher:Landroid/widget/ImageView;

    invoke-virtual {v3, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    const v3, 0x7f0f00cb

    invoke-static {v1, v3}, Lcom/android/email/activity/UiUtilities;->getView(Landroid/view/View;I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/TextView;

    iput-object v3, p0, Lcom/android/email/activity/MessageViewFragmentBase;->mMessageTab:Landroid/widget/TextView;

    const v3, 0x7f0f00cd

    invoke-static {v1, v3}, Lcom/android/email/activity/UiUtilities;->getView(Landroid/view/View;I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/TextView;

    iput-object v3, p0, Lcom/android/email/activity/MessageViewFragmentBase;->mAttachmentTab:Landroid/widget/TextView;

    const v3, 0x7f0f00c9

    invoke-static {v1, v3}, Lcom/android/email/activity/UiUtilities;->getView(Landroid/view/View;I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/TextView;

    iput-object v3, p0, Lcom/android/email/activity/MessageViewFragmentBase;->mShowPicturesTab:Landroid/widget/TextView;

    const v3, 0x7f0f00ca

    invoke-static {v1, v3}, Lcom/android/email/activity/UiUtilities;->getView(Landroid/view/View;I)Landroid/view/View;

    move-result-object v3

    iput-object v3, p0, Lcom/android/email/activity/MessageViewFragmentBase;->mAlwaysShowPicturesButton:Landroid/view/View;

    const v3, 0x7f0f00cc

    invoke-static {v1, v3}, Lcom/android/email/activity/UiUtilities;->getView(Landroid/view/View;I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/TextView;

    iput-object v3, p0, Lcom/android/email/activity/MessageViewFragmentBase;->mInviteTab:Landroid/widget/TextView;

    iget-object v3, p0, Lcom/android/email/activity/MessageViewFragmentBase;->mMessageTab:Landroid/widget/TextView;

    invoke-virtual {v3, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v3, p0, Lcom/android/email/activity/MessageViewFragmentBase;->mAttachmentTab:Landroid/widget/TextView;

    invoke-virtual {v3, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v3, p0, Lcom/android/email/activity/MessageViewFragmentBase;->mShowPicturesTab:Landroid/widget/TextView;

    invoke-virtual {v3, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v3, p0, Lcom/android/email/activity/MessageViewFragmentBase;->mAlwaysShowPicturesButton:Landroid/view/View;

    invoke-virtual {v3, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v3, p0, Lcom/android/email/activity/MessageViewFragmentBase;->mInviteTab:Landroid/widget/TextView;

    invoke-virtual {v3, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v3, p0, Lcom/android/email/activity/MessageViewFragmentBase;->mDetailsCollapsed:Landroid/view/View;

    invoke-virtual {v3, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v3, p0, Lcom/android/email/activity/MessageViewFragmentBase;->mDetailsExpanded:Landroid/view/View;

    invoke-virtual {v3, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    const v3, 0x7f0f00d0

    invoke-static {v1, v3}, Lcom/android/email/activity/UiUtilities;->getView(Landroid/view/View;I)Landroid/view/View;

    move-result-object v3

    iput-object v3, p0, Lcom/android/email/activity/MessageViewFragmentBase;->mAttachmentsScroll:Landroid/view/View;

    const v3, 0x7f0f00cf

    invoke-static {v1, v3}, Lcom/android/email/activity/UiUtilities;->getView(Landroid/view/View;I)Landroid/view/View;

    move-result-object v3

    iput-object v3, p0, Lcom/android/email/activity/MessageViewFragmentBase;->mInviteScroll:Landroid/view/View;

    iget-object v3, p0, Lcom/android/email/activity/MessageViewFragmentBase;->mMessageContentView:Landroid/webkit/WebView;

    invoke-virtual {v3}, Landroid/webkit/WebView;->getSettings()Landroid/webkit/WebSettings;

    move-result-object v2

    iget-object v3, p0, Lcom/android/email/activity/MessageViewFragmentBase;->mContext:Landroid/content/Context;

    invoke-virtual {v3}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v3

    const-string v6, "android.hardware.touchscreen.multitouch"

    invoke-virtual {v3, v6}, Landroid/content/pm/PackageManager;->hasSystemFeature(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_1

    move v3, v4

    :goto_0
    invoke-virtual {v2, v3}, Landroid/webkit/WebSettings;->setDisplayZoomControls(Z)V

    invoke-virtual {v2, v4}, Landroid/webkit/WebSettings;->setSupportZoom(Z)V

    invoke-virtual {v2, v4}, Landroid/webkit/WebSettings;->setBuiltInZoomControls(Z)V

    iget-object v3, p0, Lcom/android/email/activity/MessageViewFragmentBase;->mMessageContentView:Landroid/webkit/WebView;

    const/4 v5, 0x2

    invoke-virtual {v3, v5}, Landroid/webkit/WebView;->setOverScrollMode(I)V

    iget-object v3, p0, Lcom/android/email/activity/MessageViewFragmentBase;->mMessageContentView:Landroid/webkit/WebView;

    new-instance v5, Lcom/android/email/activity/MessageViewFragmentBase$CustomWebViewClient;

    const/4 v6, 0x0

    invoke-direct {v5, p0, v6}, Lcom/android/email/activity/MessageViewFragmentBase$CustomWebViewClient;-><init>(Lcom/android/email/activity/MessageViewFragmentBase;Lcom/android/email/activity/MessageViewFragmentBase$1;)V

    invoke-virtual {v3, v5}, Landroid/webkit/WebView;->setWebViewClient(Landroid/webkit/WebViewClient;)V

    iget-object v3, p0, Lcom/android/email/activity/MessageViewFragmentBase;->mMainView:Lcom/android/email/view/NonLockingScrollView;

    invoke-virtual {v3, v4}, Landroid/view/View;->setScrollbarFadingEnabled(Z)V

    iget-object v3, p0, Lcom/android/email/activity/MessageViewFragmentBase;->mMainView:Lcom/android/email/view/NonLockingScrollView;

    const/high16 v4, 0x2000000

    invoke-virtual {v3, v4}, Landroid/view/View;->setScrollBarStyle(I)V

    return-object v1

    :cond_1
    move v3, v5

    goto :goto_0
.end method

.method public onDestroy()V
    .locals 4

    sget-boolean v1, Lcom/android/emailcommon/Logging;->DEBUG_LIFECYCLE:Z

    if-eqz v1, :cond_0

    sget-boolean v1, Lcom/android/email/Email;->DEBUG:Z

    if-eqz v1, :cond_0

    const-string v1, "Email"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " onDestroy"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    invoke-virtual {p0}, Lcom/android/email/activity/MessageViewFragmentBase;->stopLoading()V

    invoke-direct {p0}, Lcom/android/email/activity/MessageViewFragmentBase;->cleanupDetachedViews()V

    invoke-super {p0}, Landroid/app/Fragment;->onDestroy()V

    invoke-virtual {p0}, Landroid/app/Fragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    instance-of v1, v0, Lcom/android/email/activity/FragmentInstallable;

    if-eqz v1, :cond_1

    check-cast v0, Lcom/android/email/activity/FragmentInstallable;

    invoke-interface {v0, p0}, Lcom/android/email/activity/FragmentInstallable;->onRemoveFragment(Landroid/app/Fragment;)V

    :cond_1
    return-void
.end method

.method public onDestroyView()V
    .locals 3

    sget-boolean v0, Lcom/android/emailcommon/Logging;->DEBUG_LIFECYCLE:Z

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/android/email/Email;->DEBUG:Z

    if-eqz v0, :cond_0

    const-string v0, "Email"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " onDestroyView"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    invoke-static {p0}, Lcom/android/email/activity/UiUtilities;->uninstallFragment(Landroid/app/Fragment;)V

    iget-object v0, p0, Lcom/android/email/activity/MessageViewFragmentBase;->mController:Lcom/android/email/Controller;

    iget-object v1, p0, Lcom/android/email/activity/MessageViewFragmentBase;->mControllerCallback:Lcom/android/email/ControllerResultUiThreadWrapper;

    invoke-virtual {v0, v1}, Lcom/android/email/Controller;->removeResultCallback(Lcom/android/email/Controller$Result;)V

    invoke-direct {p0}, Lcom/android/email/activity/MessageViewFragmentBase;->cancelAllTasks()V

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/android/email/activity/MessageViewFragmentBase;->mPartialDownload:Z

    invoke-super {p0}, Landroid/app/Fragment;->onDestroyView()V

    return-void
.end method

.method public onDetach()V
    .locals 3

    sget-boolean v0, Lcom/android/emailcommon/Logging;->DEBUG_LIFECYCLE:Z

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/android/email/Email;->DEBUG:Z

    if-eqz v0, :cond_0

    const-string v0, "Email"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " onDetach"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    invoke-super {p0}, Landroid/app/Fragment;->onDetach()V

    return-void
.end method

.method protected onMessageShown(JLcom/android/emailcommon/provider/Mailbox;)V
    .locals 0
    .param p1    # J
    .param p3    # Lcom/android/emailcommon/provider/Mailbox;

    return-void
.end method

.method public onPause()V
    .locals 3

    sget-boolean v0, Lcom/android/emailcommon/Logging;->DEBUG_LIFECYCLE:Z

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/android/email/Email;->DEBUG:Z

    if-eqz v0, :cond_0

    const-string v0, "Email"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " onPause"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    invoke-direct {p0}, Lcom/android/email/activity/MessageViewFragmentBase;->getWebViewZoom()I

    move-result v0

    iput v0, p0, Lcom/android/email/activity/MessageViewFragmentBase;->mLastZoomSize:I

    invoke-super {p0}, Landroid/app/Fragment;->onPause()V

    return-void
.end method

.method protected onPostLoadBody()V
    .locals 0

    return-void
.end method

.method public onResume()V
    .locals 3

    sget-boolean v0, Lcom/android/emailcommon/Logging;->DEBUG_LIFECYCLE:Z

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/android/email/Email;->DEBUG:Z

    if-eqz v0, :cond_0

    const-string v0, "Email"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " onResume"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    invoke-super {p0}, Landroid/app/Fragment;->onResume()V

    iget-object v0, p0, Lcom/android/email/activity/MessageViewFragmentBase;->mMessageContentView:Landroid/webkit/WebView;

    if-eqz v0, :cond_1

    iget v0, p0, Lcom/android/email/activity/MessageViewFragmentBase;->mLastZoomSize:I

    if-lez v0, :cond_1

    iget v0, p0, Lcom/android/email/activity/MessageViewFragmentBase;->mLastZoomSize:I

    invoke-direct {p0}, Lcom/android/email/activity/MessageViewFragmentBase;->getWebViewZoom()I

    move-result v1

    if-eq v0, v1, :cond_1

    invoke-virtual {p0}, Lcom/android/email/activity/MessageViewFragmentBase;->loadMessage()V

    :cond_1
    invoke-direct {p0}, Lcom/android/email/activity/MessageViewFragmentBase;->updateAttachmentTab()V

    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/android/email/activity/MessageViewFragmentBase;->updateVipInformation(Lcom/mediatek/email/emailvip/utils/MessageViewUtils$TempAddress;)V

    return-void
.end method

.method public onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 4
    .param p1    # Landroid/os/Bundle;

    sget-boolean v0, Lcom/android/emailcommon/Logging;->DEBUG_LIFECYCLE:Z

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/android/email/Email;->DEBUG:Z

    if-eqz v0, :cond_0

    const-string v0, "Email"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " onSaveInstanceState"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    invoke-super {p0, p1}, Landroid/app/Fragment;->onSaveInstanceState(Landroid/os/Bundle;)V

    const-string v0, "MessageViewFragmentBase.currentTab"

    iget v1, p0, Lcom/android/email/activity/MessageViewFragmentBase;->mCurrentTab:I

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    const-string v1, "MessageViewFragmentBase.pictureLoaded"

    iget v0, p0, Lcom/android/email/activity/MessageViewFragmentBase;->mTabFlags:I

    and-int/lit8 v0, v0, 0x8

    if-eqz v0, :cond_1

    const/4 v0, 0x1

    :goto_0
    invoke-virtual {p1, v1, v0}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    const-string v0, "MessageViewFragmentBase.particalDownload"

    iget-boolean v1, p0, Lcom/android/email/activity/MessageViewFragmentBase;->mPartialDownload:Z

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    const-string v0, "MessageViewFragmentBase.messageKey"

    iget-wide v1, p0, Lcom/android/email/activity/MessageViewFragmentBase;->mMessageId:J

    invoke-virtual {p1, v0, v1, v2}, Landroid/os/Bundle;->putLong(Ljava/lang/String;J)V

    const-string v0, "MessageViewFragmentBase"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "onSaveInstanceState BUNDLE_KEY_PARTIAL_DOWNLOAD = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-boolean v2, p0, Lcom/android/email/activity/MessageViewFragmentBase;->mPartialDownload:Z

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " BUNDLE_KEY_MESSAGE_KEY"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-wide v2, p0, Lcom/android/email/activity/MessageViewFragmentBase;->mMessageId:J

    invoke-virtual {v1, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/android/emailcommon/Logging;->d(Ljava/lang/String;Ljava/lang/String;)V

    return-void

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public onStart()V
    .locals 3

    sget-boolean v0, Lcom/android/emailcommon/Logging;->DEBUG_LIFECYCLE:Z

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/android/email/Email;->DEBUG:Z

    if-eqz v0, :cond_0

    const-string v0, "Email"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " onStart"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    invoke-super {p0}, Landroid/app/Fragment;->onStart()V

    return-void
.end method

.method public onStop()V
    .locals 3

    sget-boolean v0, Lcom/android/emailcommon/Logging;->DEBUG_LIFECYCLE:Z

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/android/email/Email;->DEBUG:Z

    if-eqz v0, :cond_0

    const-string v0, "Email"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " onStop"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    invoke-super {p0}, Landroid/app/Fragment;->onStop()V

    return-void
.end method

.method protected abstract openMessageSync(Landroid/app/Activity;)Lcom/android/emailcommon/provider/EmailContent$Message;
.end method

.method protected reloadMessageSync(Landroid/app/Activity;)Lcom/android/emailcommon/provider/EmailContent$Message;
    .locals 1
    .param p1    # Landroid/app/Activity;

    invoke-virtual {p0, p1}, Lcom/android/email/activity/MessageViewFragmentBase;->openMessageSync(Landroid/app/Activity;)Lcom/android/emailcommon/provider/EmailContent$Message;

    move-result-object v0

    return-object v0
.end method

.method protected reloadUiFromMessage(Lcom/android/emailcommon/provider/EmailContent$Message;Z)V
    .locals 12
    .param p1    # Lcom/android/emailcommon/provider/EmailContent$Message;
    .param p2    # Z

    iput-object p1, p0, Lcom/android/email/activity/MessageViewFragmentBase;->mMessage:Lcom/android/emailcommon/provider/EmailContent$Message;

    iget-wide v8, p1, Lcom/android/emailcommon/provider/EmailContent$Message;->mAccountKey:J

    iput-wide v8, p0, Lcom/android/email/activity/MessageViewFragmentBase;->mAccountId:J

    iget-object v8, p0, Lcom/android/email/activity/MessageViewFragmentBase;->mMessageObserver:Lcom/android/email/activity/MessageViewFragmentBase$MessageObserver;

    sget-object v9, Lcom/android/emailcommon/provider/EmailContent$Message;->CONTENT_URI:Landroid/net/Uri;

    iget-object v10, p0, Lcom/android/email/activity/MessageViewFragmentBase;->mMessage:Lcom/android/emailcommon/provider/EmailContent$Message;

    iget-wide v10, v10, Lcom/android/emailcommon/provider/EmailContent;->mId:J

    invoke-static {v9, v10, v11}, Landroid/content/ContentUris;->withAppendedId(Landroid/net/Uri;J)Landroid/net/Uri;

    move-result-object v9

    invoke-virtual {v8, v9}, Lcom/android/email/activity/MessageViewFragmentBase$MessageObserver;->register(Landroid/net/Uri;)V

    const/4 v8, 0x1

    const/4 v9, 0x0

    invoke-direct {p0, v8, v9}, Lcom/android/email/activity/MessageViewFragmentBase;->showContent(ZZ)V

    iget-object v8, p0, Lcom/android/email/activity/MessageViewFragmentBase;->mMessage:Lcom/android/emailcommon/provider/EmailContent$Message;

    invoke-virtual {p0, v8}, Lcom/android/email/activity/MessageViewFragmentBase;->updateHeaderView(Lcom/android/emailcommon/provider/EmailContent$Message;)V

    iget v8, p1, Lcom/android/emailcommon/provider/EmailContent$Message;->mFlagLoaded:I

    const/4 v9, 0x4

    if-ne v8, v9, :cond_0

    iget-object v8, p0, Lcom/android/email/activity/MessageViewFragmentBase;->mMessageContentView:Landroid/webkit/WebView;

    if-eqz v8, :cond_0

    iget-object v8, p0, Lcom/android/email/activity/MessageViewFragmentBase;->mMessageContentView:Landroid/webkit/WebView;

    const-string v9, "file:///android_asset/loading.html"

    invoke-virtual {v8, v9}, Landroid/webkit/WebView;->loadUrl(Ljava/lang/String;)V

    :cond_0
    iget-object v8, p0, Lcom/android/email/activity/MessageViewFragmentBase;->mContext:Landroid/content/Context;

    invoke-static {v8}, Lcom/android/email/Preferences;->getPreferences(Landroid/content/Context;)Lcom/android/email/Preferences;

    move-result-object v6

    invoke-virtual {v6}, Lcom/android/email/Preferences;->getLowStorage()Z

    move-result v8

    if-nez v8, :cond_2

    iget v8, p1, Lcom/android/emailcommon/provider/EmailContent$Message;->mFlagLoaded:I

    const/4 v9, 0x4

    if-ne v8, v9, :cond_2

    iget-object v8, p0, Lcom/android/email/activity/MessageViewFragmentBase;->mMessageContentView:Landroid/webkit/WebView;

    if-eqz v8, :cond_1

    iget-object v8, p0, Lcom/android/email/activity/MessageViewFragmentBase;->mMessageContentView:Landroid/webkit/WebView;

    const-string v9, "file:///android_asset/loading.html"

    invoke-virtual {v8, v9}, Landroid/webkit/WebView;->loadUrl(Ljava/lang/String;)V

    :cond_1
    iget-object v8, p0, Lcom/android/email/activity/MessageViewFragmentBase;->mControllerCallback:Lcom/android/email/ControllerResultUiThreadWrapper;

    invoke-virtual {v8}, Lcom/android/email/ControllerResultUiThreadWrapper;->getWrappee()Lcom/android/email/Controller$Result;

    move-result-object v8

    check-cast v8, Lcom/android/email/activity/MessageViewFragmentBase$ControllerResults;

    iget-wide v9, p1, Lcom/android/emailcommon/provider/EmailContent;->mId:J

    invoke-virtual {v8, v9, v10}, Lcom/android/email/activity/MessageViewFragmentBase$ControllerResults;->setWaitForLoadMessageId(J)V

    iget-object v8, p0, Lcom/android/email/activity/MessageViewFragmentBase;->mController:Lcom/android/email/Controller;

    iget-wide v9, p1, Lcom/android/emailcommon/provider/EmailContent;->mId:J

    invoke-virtual {v8, v9, v10}, Lcom/android/email/Controller;->loadMessageForView(J)V

    const-string v8, "Email"

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "+++ download unloaded Message: "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    iget-wide v10, p1, Lcom/android/emailcommon/provider/EmailContent;->mId:J

    invoke-virtual {v9, v10, v11}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v8, v9}, Lcom/android/emailcommon/Logging;->d(Ljava/lang/String;Ljava/lang/String;)V

    :goto_0
    return-void

    :cond_2
    iget-object v8, p0, Lcom/android/email/activity/MessageViewFragmentBase;->mMessage:Lcom/android/emailcommon/provider/EmailContent$Message;

    iget-object v8, v8, Lcom/android/emailcommon/provider/EmailContent$Message;->mFrom:Ljava/lang/String;

    invoke-static {v8}, Lcom/android/emailcommon/mail/Address;->unpack(Ljava/lang/String;)[Lcom/android/emailcommon/mail/Address;

    move-result-object v3

    const/4 v1, 0x0

    move-object v0, v3

    array-length v5, v0

    const/4 v4, 0x0

    :goto_1
    if-ge v4, v5, :cond_3

    aget-object v7, v0, v4

    invoke-virtual {v7}, Lcom/android/emailcommon/mail/Address;->getAddress()Ljava/lang/String;

    move-result-object v2

    invoke-direct {p0, v2}, Lcom/android/email/activity/MessageViewFragmentBase;->shouldShowImagesFor(Ljava/lang/String;)Z

    move-result v8

    if-eqz v8, :cond_4

    const/4 v1, 0x1

    :cond_3
    iget-object v8, p0, Lcom/android/email/activity/MessageViewFragmentBase;->mControllerCallback:Lcom/android/email/ControllerResultUiThreadWrapper;

    invoke-virtual {v8}, Lcom/android/email/ControllerResultUiThreadWrapper;->getWrappee()Lcom/android/email/Controller$Result;

    move-result-object v8

    check-cast v8, Lcom/android/email/activity/MessageViewFragmentBase$ControllerResults;

    const-wide/16 v9, -0x1

    invoke-virtual {v8, v9, v10}, Lcom/android/email/activity/MessageViewFragmentBase$ControllerResults;->setWaitForLoadMessageId(J)V

    const-string v8, "Email"

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "+++ Load complete/partial Message flag: "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    iget v10, p1, Lcom/android/emailcommon/provider/EmailContent$Message;->mFlagLoaded:I

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v8, v9}, Lcom/android/emailcommon/Logging;->d(Ljava/lang/String;Ljava/lang/String;)V

    new-instance v8, Lcom/android/email/activity/MessageViewFragmentBase$LoadBodyTask;

    iget-wide v9, p1, Lcom/android/emailcommon/provider/EmailContent;->mId:J

    invoke-direct {v8, p0, v9, v10, v1}, Lcom/android/email/activity/MessageViewFragmentBase$LoadBodyTask;-><init>(Lcom/android/email/activity/MessageViewFragmentBase;JZ)V

    iput-object v8, p0, Lcom/android/email/activity/MessageViewFragmentBase;->mLoadBodyTask:Lcom/android/email/activity/MessageViewFragmentBase$LoadBodyTask;

    iget-object v8, p0, Lcom/android/email/activity/MessageViewFragmentBase;->mLoadBodyTask:Lcom/android/email/activity/MessageViewFragmentBase$LoadBodyTask;

    const/4 v9, 0x0

    new-array v9, v9, [Ljava/lang/Void;

    invoke-virtual {v8, v9}, Lcom/android/emailcommon/utility/EmailAsyncTask;->executeParallel([Ljava/lang/Object;)Lcom/android/emailcommon/utility/EmailAsyncTask;

    goto :goto_0

    :cond_4
    add-int/lit8 v4, v4, 0x1

    goto :goto_1
.end method

.method public resetPartialLoading()V
    .locals 1

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/android/email/activity/MessageViewFragmentBase;->mPartialDownload:Z

    return-void
.end method

.method protected resetView()V
    .locals 6

    const/16 v3, 0x65

    const/4 v2, 0x1

    const/4 v5, 0x0

    invoke-direct {p0, v5, v5}, Lcom/android/email/activity/MessageViewFragmentBase;->showContent(ZZ)V

    invoke-direct {p0}, Lcom/android/email/activity/MessageViewFragmentBase;->hideDetails()V

    iput-boolean v5, p0, Lcom/android/email/activity/MessageViewFragmentBase;->mDetailsFilled:Z

    invoke-direct {p0, v5}, Lcom/android/email/activity/MessageViewFragmentBase;->updateTabs(I)V

    iput-boolean v5, p0, Lcom/android/email/activity/MessageViewFragmentBase;->mNeedReloadMessageContent:Z

    iget v1, p0, Lcom/android/email/activity/MessageViewFragmentBase;->mRestoredTab:I

    if-eqz v1, :cond_2

    iget v1, p0, Lcom/android/email/activity/MessageViewFragmentBase;->mRestoredTab:I

    invoke-direct {p0, v1}, Lcom/android/email/activity/MessageViewFragmentBase;->setCurrentTab(I)V

    iget v1, p0, Lcom/android/email/activity/MessageViewFragmentBase;->mRestoredTab:I

    if-eq v1, v3, :cond_0

    iput-boolean v2, p0, Lcom/android/email/activity/MessageViewFragmentBase;->mNeedReloadMessageContent:Z

    :cond_0
    iput v5, p0, Lcom/android/email/activity/MessageViewFragmentBase;->mRestoredTab:I

    :goto_0
    iget-object v1, p0, Lcom/android/email/activity/MessageViewFragmentBase;->mMessageContentView:Landroid/webkit/WebView;

    if-eqz v1, :cond_1

    invoke-direct {p0, v2}, Lcom/android/email/activity/MessageViewFragmentBase;->blockNetworkLoads(Z)V

    iget-object v1, p0, Lcom/android/email/activity/MessageViewFragmentBase;->mMessageContentView:Landroid/webkit/WebView;

    invoke-virtual {v1, v5, v5}, Landroid/webkit/WebView;->flingScroll(II)V

    iget-object v1, p0, Lcom/android/email/activity/MessageViewFragmentBase;->mMessageContentView:Landroid/webkit/WebView;

    invoke-virtual {v1, v5, v5}, Landroid/view/View;->measure(II)V

    iget-object v1, p0, Lcom/android/email/activity/MessageViewFragmentBase;->mMessageContentView:Landroid/webkit/WebView;

    invoke-virtual {v1}, Landroid/view/ViewGroup;->removeAllViews()V

    iget-object v1, p0, Lcom/android/email/activity/MessageViewFragmentBase;->mMessageContentView:Landroid/webkit/WebView;

    invoke-virtual {v1}, Landroid/webkit/WebView;->clearView()V

    iget-object v1, p0, Lcom/android/email/activity/MessageViewFragmentBase;->mMessageContentView:Landroid/webkit/WebView;

    invoke-virtual {v1, v5, v5}, Landroid/view/View;->scrollTo(II)V

    iget-object v1, p0, Lcom/android/email/activity/MessageViewFragmentBase;->mMessageContentView:Landroid/webkit/WebView;

    invoke-virtual {v1, v5}, Landroid/view/View;->setFocusable(Z)V

    iget-object v1, p0, Lcom/android/email/activity/MessageViewFragmentBase;->mMessageContentView:Landroid/webkit/WebView;

    const-string v2, "<html><body bgcolor=\"white\"/></html>"

    const-string v3, "text/html"

    const-string v4, "utf-8"

    invoke-virtual {v1, v2, v3, v4}, Landroid/webkit/WebView;->loadData(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    iget-object v1, p0, Lcom/android/email/activity/MessageViewFragmentBase;->mMessageContentView:Landroid/webkit/WebView;

    invoke-virtual {v1}, Landroid/webkit/WebView;->getSettings()Landroid/webkit/WebSettings;

    move-result-object v0

    sget-object v1, Landroid/webkit/WebSettings$LayoutAlgorithm;->NORMAL:Landroid/webkit/WebSettings$LayoutAlgorithm;

    invoke-virtual {v0, v1}, Landroid/webkit/WebSettings;->setLayoutAlgorithm(Landroid/webkit/WebSettings$LayoutAlgorithm;)V

    invoke-direct {p0}, Lcom/android/email/activity/MessageViewFragmentBase;->getWebViewZoom()I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/webkit/WebSettings;->setTextZoom(I)V

    :cond_1
    iget-object v1, p0, Lcom/android/email/activity/MessageViewFragmentBase;->mAttachmentsScroll:Landroid/view/View;

    invoke-virtual {v1, v5, v5}, Landroid/view/View;->scrollTo(II)V

    iget-object v1, p0, Lcom/android/email/activity/MessageViewFragmentBase;->mInviteScroll:Landroid/view/View;

    invoke-virtual {v1, v5, v5}, Landroid/view/View;->scrollTo(II)V

    iget-object v1, p0, Lcom/android/email/activity/MessageViewFragmentBase;->mMainView:Lcom/android/email/view/NonLockingScrollView;

    invoke-virtual {v1}, Lcom/android/email/view/NonLockingScrollView;->scrollFreeze()V

    iget-object v1, p0, Lcom/android/email/activity/MessageViewFragmentBase;->mMainView:Lcom/android/email/view/NonLockingScrollView;

    invoke-virtual {v1, v5, v5}, Lcom/android/email/view/NonLockingScrollView;->scrollTo(II)V

    iget-object v1, p0, Lcom/android/email/activity/MessageViewFragmentBase;->mMainView:Lcom/android/email/view/NonLockingScrollView;

    new-instance v2, Lcom/android/email/activity/MessageViewFragmentBase$1;

    invoke-direct {v2, p0}, Lcom/android/email/activity/MessageViewFragmentBase$1;-><init>(Lcom/android/email/activity/MessageViewFragmentBase;)V

    invoke-virtual {v1, v2}, Lcom/android/email/view/NonLockingScrollView;->setOnOverScrollListener(Lcom/android/email/view/NonLockingScrollView$OnOverScrollListener;)V

    iget-object v1, p0, Lcom/android/email/activity/MessageViewFragmentBase;->mAttachments:Landroid/widget/LinearLayout;

    invoke-virtual {v1}, Landroid/view/ViewGroup;->removeAllViews()V

    iget-object v1, p0, Lcom/android/email/activity/MessageViewFragmentBase;->mAttachments:Landroid/widget/LinearLayout;

    const/16 v2, 0x8

    invoke-virtual {v1, v2}, Landroid/view/View;->setVisibility(I)V

    invoke-direct {p0}, Lcom/android/email/activity/MessageViewFragmentBase;->initContactStatusViews()V

    return-void

    :cond_2
    invoke-direct {p0, v3}, Lcom/android/email/activity/MessageViewFragmentBase;->setCurrentTab(I)V

    goto :goto_0
.end method

.method public setCallback(Lcom/android/email/activity/MessageViewFragmentBase$Callback;)V
    .locals 0
    .param p1    # Lcom/android/email/activity/MessageViewFragmentBase$Callback;

    if-nez p1, :cond_0

    sget-object p1, Lcom/android/email/activity/MessageViewFragmentBase$EmptyCallback;->INSTANCE:Lcom/android/email/activity/MessageViewFragmentBase$Callback;

    :cond_0
    iput-object p1, p0, Lcom/android/email/activity/MessageViewFragmentBase;->mCallback:Lcom/android/email/activity/MessageViewFragmentBase$Callback;

    return-void
.end method

.method public stopLoading()V
    .locals 3

    const/4 v2, 0x0

    const/4 v1, 0x1

    iget-object v0, p0, Lcom/android/email/activity/MessageViewFragmentBase;->mLoadMessageTask:Lcom/android/email/activity/MessageViewFragmentBase$LoadMessageTask;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/email/activity/MessageViewFragmentBase;->mLoadMessageTask:Lcom/android/email/activity/MessageViewFragmentBase$LoadMessageTask;

    invoke-virtual {v0, v1}, Lcom/android/emailcommon/utility/EmailAsyncTask;->cancel(Z)V

    iput-object v2, p0, Lcom/android/email/activity/MessageViewFragmentBase;->mLoadMessageTask:Lcom/android/email/activity/MessageViewFragmentBase$LoadMessageTask;

    :cond_0
    iget-object v0, p0, Lcom/android/email/activity/MessageViewFragmentBase;->mLoadBodyTask:Lcom/android/email/activity/MessageViewFragmentBase$LoadBodyTask;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/android/email/activity/MessageViewFragmentBase;->mLoadBodyTask:Lcom/android/email/activity/MessageViewFragmentBase$LoadBodyTask;

    invoke-virtual {v0, v1}, Lcom/android/emailcommon/utility/EmailAsyncTask;->cancel(Z)V

    iput-object v2, p0, Lcom/android/email/activity/MessageViewFragmentBase;->mLoadBodyTask:Lcom/android/email/activity/MessageViewFragmentBase$LoadBodyTask;

    :cond_1
    iget-object v0, p0, Lcom/android/email/activity/MessageViewFragmentBase;->mLoadAttachmentsTask:Lcom/android/email/activity/MessageViewFragmentBase$LoadAttachmentsTask;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/android/email/activity/MessageViewFragmentBase;->mLoadAttachmentsTask:Lcom/android/email/activity/MessageViewFragmentBase$LoadAttachmentsTask;

    invoke-virtual {v0, v1}, Lcom/android/emailcommon/utility/EmailAsyncTask;->cancel(Z)V

    iput-object v2, p0, Lcom/android/email/activity/MessageViewFragmentBase;->mLoadAttachmentsTask:Lcom/android/email/activity/MessageViewFragmentBase$LoadAttachmentsTask;

    :cond_2
    iget-object v0, p0, Lcom/android/email/activity/MessageViewFragmentBase;->mLinkParserTask:Lcom/android/emailcommon/utility/LinkParserTask;

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/android/email/activity/MessageViewFragmentBase;->mLinkParserTask:Lcom/android/emailcommon/utility/LinkParserTask;

    invoke-virtual {v0, v1}, Landroid/os/AsyncTask;->cancel(Z)Z

    iget-object v0, p0, Lcom/android/email/activity/MessageViewFragmentBase;->mLinkParserTask:Lcom/android/emailcommon/utility/LinkParserTask;

    invoke-virtual {v0}, Lcom/android/emailcommon/utility/LinkParserTask;->stopWebView()V

    iput-object v2, p0, Lcom/android/email/activity/MessageViewFragmentBase;->mLinkParserTask:Lcom/android/emailcommon/utility/LinkParserTask;

    :cond_3
    iget-object v0, p0, Lcom/android/email/activity/MessageViewFragmentBase;->mRenderAttachmentTask:Landroid/os/AsyncTask;

    if-eqz v0, :cond_4

    iget-object v0, p0, Lcom/android/email/activity/MessageViewFragmentBase;->mRenderAttachmentTask:Landroid/os/AsyncTask;

    invoke-virtual {v0, v1}, Landroid/os/AsyncTask;->cancel(Z)Z

    iput-object v2, p0, Lcom/android/email/activity/MessageViewFragmentBase;->mRenderAttachmentTask:Landroid/os/AsyncTask;

    :cond_4
    iget-object v0, p0, Lcom/android/email/activity/MessageViewFragmentBase;->mTaskTracker:Lcom/android/emailcommon/utility/EmailAsyncTask$Tracker;

    invoke-virtual {v0}, Lcom/android/emailcommon/utility/EmailAsyncTask$Tracker;->cancellAllInterrupt()V

    const-string v0, "MessageViewFragmentBase"

    const-string v1, "Stop all loading AsyncTask..."

    invoke-static {v0, v1}, Lcom/android/emailcommon/Logging;->d(Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method protected updateHeaderView(Lcom/android/emailcommon/provider/EmailContent$Message;)V
    .locals 7
    .param p1    # Lcom/android/emailcommon/provider/EmailContent$Message;

    iget-object v3, p0, Lcom/android/email/activity/MessageViewFragmentBase;->mSubjectView:Landroid/widget/TextView;

    iget-object v4, p1, Lcom/android/emailcommon/provider/EmailContent$Message;->mSubject:Ljava/lang/String;

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v3, p1, Lcom/android/emailcommon/provider/EmailContent$Message;->mFrom:Ljava/lang/String;

    invoke-static {v3}, Lcom/android/emailcommon/mail/Address;->unpackFirst(Ljava/lang/String;)Lcom/android/emailcommon/mail/Address;

    move-result-object v0

    if-eqz v0, :cond_1

    invoke-virtual {v0}, Lcom/android/emailcommon/mail/Address;->toFriendly()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0}, Lcom/android/emailcommon/mail/Address;->getAddress()Ljava/lang/String;

    move-result-object v1

    iget-object v3, p0, Lcom/android/email/activity/MessageViewFragmentBase;->mFromNameView:Landroid/widget/TextView;

    invoke-virtual {v3, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v3, p0, Lcom/android/email/activity/MessageViewFragmentBase;->mFromAddressView:Landroid/widget/TextView;

    invoke-virtual {v2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_0

    const-string v1, " "

    :cond_0
    invoke-virtual {v3, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    :goto_0
    iget-object v3, p0, Lcom/android/email/activity/MessageViewFragmentBase;->mDateTimeView:Landroid/widget/TextView;

    iget-object v4, p0, Lcom/android/email/activity/MessageViewFragmentBase;->mContext:Landroid/content/Context;

    iget-wide v5, p1, Lcom/android/emailcommon/provider/EmailContent$Message;->mTimeStamp:J

    invoke-static {v4, v5, v6}, Landroid/text/format/DateUtils;->getRelativeTimeSpanString(Landroid/content/Context;J)Ljava/lang/CharSequence;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v3, p0, Lcom/android/email/activity/MessageViewFragmentBase;->mMessage:Lcom/android/emailcommon/provider/EmailContent$Message;

    iget-object v4, p0, Lcom/android/email/activity/MessageViewFragmentBase;->mContext:Landroid/content/Context;

    iget-object v5, p0, Lcom/android/email/activity/MessageViewFragmentBase;->mAddressesView:Landroid/widget/TextView;

    invoke-static {v3, v4, v5}, Lcom/mediatek/email/emailvip/utils/MessageViewUtils;->updateAddressesView(Lcom/android/emailcommon/provider/EmailContent$Message;Landroid/content/Context;Landroid/widget/TextView;)V

    iget-object v3, p1, Lcom/android/emailcommon/provider/EmailContent$Message;->mFrom:Ljava/lang/String;

    invoke-static {v3}, Lcom/mediatek/email/emailvip/VipMemberCache;->isVIP(Ljava/lang/String;)Z

    move-result v3

    invoke-direct {p0, v3}, Lcom/android/email/activity/MessageViewFragmentBase;->updateVipIcon(Z)V

    return-void

    :cond_1
    iget-object v3, p0, Lcom/android/email/activity/MessageViewFragmentBase;->mFromNameView:Landroid/widget/TextView;

    const-string v4, " "

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v3, p0, Lcom/android/email/activity/MessageViewFragmentBase;->mFromAddressView:Landroid/widget/TextView;

    const-string v4, " "

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0
.end method

.method public updateSenderVipIcon(Z)V
    .locals 4
    .param p1    # Z

    const/4 v3, 0x0

    iget-object v1, p0, Lcom/android/email/activity/MessageViewFragmentBase;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    if-eqz p1, :cond_0

    iget-object v1, p0, Lcom/android/email/activity/MessageViewFragmentBase;->mFromNameView:Landroid/widget/TextView;

    const v2, 0x7f02002c

    invoke-virtual {v1, v2, v3, v3, v3}, Landroid/widget/TextView;->setCompoundDrawablesWithIntrinsicBounds(IIII)V

    iget-object v1, p0, Lcom/android/email/activity/MessageViewFragmentBase;->mVipSwitcher:Landroid/widget/ImageView;

    const v2, 0x7f020029

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    iget-object v1, p0, Lcom/android/email/activity/MessageViewFragmentBase;->mVipSwitcher:Landroid/widget/ImageView;

    iget-object v2, p0, Lcom/android/email/activity/MessageViewFragmentBase;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f08003d

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/view/View;->setContentDescription(Ljava/lang/CharSequence;)V

    :goto_0
    return-void

    :cond_0
    iget-object v1, p0, Lcom/android/email/activity/MessageViewFragmentBase;->mFromNameView:Landroid/widget/TextView;

    invoke-virtual {v1, v3, v3, v3, v3}, Landroid/widget/TextView;->setCompoundDrawablesWithIntrinsicBounds(IIII)V

    iget-object v1, p0, Lcom/android/email/activity/MessageViewFragmentBase;->mVipSwitcher:Landroid/widget/ImageView;

    const v2, 0x7f020027

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    iget-object v1, p0, Lcom/android/email/activity/MessageViewFragmentBase;->mVipSwitcher:Landroid/widget/ImageView;

    iget-object v2, p0, Lcom/android/email/activity/MessageViewFragmentBase;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f08003e

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/view/View;->setContentDescription(Ljava/lang/CharSequence;)V

    goto :goto_0
.end method

.method public updateVipInformation(Lcom/mediatek/email/emailvip/utils/MessageViewUtils$TempAddress;)V
    .locals 4
    .param p1    # Lcom/mediatek/email/emailvip/utils/MessageViewUtils$TempAddress;

    iget-object v0, p0, Lcom/android/email/activity/MessageViewFragmentBase;->mContext:Landroid/content/Context;

    iget-object v1, p0, Lcom/android/email/activity/MessageViewFragmentBase;->mMessage:Lcom/android/emailcommon/provider/EmailContent$Message;

    iget-object v2, p0, Lcom/android/email/activity/MessageViewFragmentBase;->mDetailsExpanded:Landroid/view/View;

    iget-object v3, p0, Lcom/android/email/activity/MessageViewFragmentBase;->mAddressesView:Landroid/widget/TextView;

    invoke-static {v0, v1, v2, v3, p1}, Lcom/mediatek/email/emailvip/utils/MessageViewUtils;->updateVipInformation(Landroid/content/Context;Lcom/android/emailcommon/provider/EmailContent$Message;Landroid/view/View;Landroid/widget/TextView;Lcom/mediatek/email/emailvip/utils/MessageViewUtils$TempAddress;)V

    return-void
.end method
