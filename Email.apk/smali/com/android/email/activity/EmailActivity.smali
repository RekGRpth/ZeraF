.class public Lcom/android/email/activity/EmailActivity;
.super Landroid/app/Activity;
.source "EmailActivity.java"

# interfaces
.implements Landroid/view/View$OnClickListener;
.implements Lcom/android/email/activity/FragmentInstallable;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/email/activity/EmailActivity$WaitingDialog;,
        Lcom/android/email/activity/EmailActivity$ControllerResult;
    }
.end annotation


# static fields
.field static final ACTION_BAR_CONTROLLER_LOADER_ID_BASE:I = 0xc8

.field public static final ACTION_LOCAL_SEARCH:Ljava/lang/String; = "com.android.email.action.LOCAL_SEARCH"

.field public static final ACTION_REMOTE_SEARCH:Ljava/lang/String; = "com.android.email.action.REMOTE_SEARCH"

.field private static final DIALOG_DISMISS_TIME:I = 0x7d0

.field public static final EXTRA_ACCOUNT_ID:Ljava/lang/String; = "ACCOUNT_ID"

.field public static final EXTRA_MAILBOX_ID:Ljava/lang/String; = "MAILBOX_ID"

.field public static final EXTRA_MESSAGE_ID:Ljava/lang/String; = "MESSAGE_ID"

.field public static final EXTRA_QUERY_FIELD:Ljava/lang/String; = "QUERY_FIELD"

.field public static final EXTRA_QUERY_STRING:Ljava/lang/String; = "QUERY_STRING"

.field static final UI_CONTROLLER_LOADER_ID_BASE:I = 0x64

.field public static sEmailActivityResumed:Z

.field private static sLastFontScale:F

.field public static sRecordOpening:Z


# instance fields
.field private mController:Lcom/android/email/Controller;

.field private mControllerResult:Lcom/android/email/Controller$Result;

.field private mErrorBanner:Lcom/android/email/activity/BannerController;

.field private mHandler:Landroid/os/Handler;

.field private mLastErrorAccountId:J

.field private mRecordAccountId:J

.field private final mTaskTracker:Lcom/android/emailcommon/utility/EmailAsyncTask$Tracker;

.field private mUIController:Lcom/android/email/activity/UIControllerBase;

.field private mWaitingDialog:Lcom/android/email/activity/EmailActivity$WaitingDialog;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const/high16 v0, -0x40800000

    sput v0, Lcom/android/email/activity/EmailActivity;->sLastFontScale:F

    const/4 v0, 0x1

    sput-boolean v0, Lcom/android/email/activity/EmailActivity;->sRecordOpening:Z

    return-void
.end method

.method public constructor <init>()V
    .locals 2

    invoke-direct {p0}, Landroid/app/Activity;-><init>()V

    new-instance v0, Lcom/android/emailcommon/utility/EmailAsyncTask$Tracker;

    invoke-direct {v0}, Lcom/android/emailcommon/utility/EmailAsyncTask$Tracker;-><init>()V

    iput-object v0, p0, Lcom/android/email/activity/EmailActivity;->mTaskTracker:Lcom/android/emailcommon/utility/EmailAsyncTask$Tracker;

    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    iput-object v0, p0, Lcom/android/email/activity/EmailActivity;->mHandler:Landroid/os/Handler;

    const-wide/16 v0, -0x1

    iput-wide v0, p0, Lcom/android/email/activity/EmailActivity;->mRecordAccountId:J

    return-void
.end method

.method static synthetic access$100(Lcom/android/email/activity/EmailActivity;)J
    .locals 2
    .param p0    # Lcom/android/email/activity/EmailActivity;

    iget-wide v0, p0, Lcom/android/email/activity/EmailActivity;->mLastErrorAccountId:J

    return-wide v0
.end method

.method static synthetic access$102(Lcom/android/email/activity/EmailActivity;J)J
    .locals 0
    .param p0    # Lcom/android/email/activity/EmailActivity;
    .param p1    # J

    iput-wide p1, p0, Lcom/android/email/activity/EmailActivity;->mLastErrorAccountId:J

    return-wide p1
.end method

.method static synthetic access$200(Lcom/android/email/activity/EmailActivity;)V
    .locals 0
    .param p0    # Lcom/android/email/activity/EmailActivity;

    invoke-direct {p0}, Lcom/android/email/activity/EmailActivity;->dismissErrorMessage()V

    return-void
.end method

.method static synthetic access$300(Lcom/android/email/activity/EmailActivity;)Lcom/android/email/activity/BannerController;
    .locals 1
    .param p0    # Lcom/android/email/activity/EmailActivity;

    iget-object v0, p0, Lcom/android/email/activity/EmailActivity;->mErrorBanner:Lcom/android/email/activity/BannerController;

    return-object v0
.end method

.method static synthetic access$400(Lcom/android/email/activity/EmailActivity;)Lcom/android/email/activity/EmailActivity$WaitingDialog;
    .locals 1
    .param p0    # Lcom/android/email/activity/EmailActivity;

    iget-object v0, p0, Lcom/android/email/activity/EmailActivity;->mWaitingDialog:Lcom/android/email/activity/EmailActivity$WaitingDialog;

    return-object v0
.end method

.method static synthetic access$402(Lcom/android/email/activity/EmailActivity;Lcom/android/email/activity/EmailActivity$WaitingDialog;)Lcom/android/email/activity/EmailActivity$WaitingDialog;
    .locals 0
    .param p0    # Lcom/android/email/activity/EmailActivity;
    .param p1    # Lcom/android/email/activity/EmailActivity$WaitingDialog;

    iput-object p1, p0, Lcom/android/email/activity/EmailActivity;->mWaitingDialog:Lcom/android/email/activity/EmailActivity$WaitingDialog;

    return-object p1
.end method

.method public static createLocalSearchIntent(Landroid/app/Activity;JJLjava/lang/String;Ljava/lang/String;)Landroid/content/Intent;
    .locals 2
    .param p0    # Landroid/app/Activity;
    .param p1    # J
    .param p3    # J
    .param p5    # Ljava/lang/String;
    .param p6    # Ljava/lang/String;

    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/android/email/activity/EmailActivity;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const-string v1, "ACCOUNT_ID"

    invoke-virtual {v0, v1, p1, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;J)Landroid/content/Intent;

    const-string v1, "MAILBOX_ID"

    invoke-virtual {v0, v1, p3, p4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;J)Landroid/content/Intent;

    const-string v1, "QUERY_STRING"

    invoke-virtual {v0, v1, p5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const-string v1, "QUERY_FIELD"

    invoke-virtual {v0, v1, p6}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const-string v1, "com.android.email.action.LOCAL_SEARCH"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    return-object v0
.end method

.method public static createOpenAccountIntent(Landroid/app/Activity;J)Landroid/content/Intent;
    .locals 3
    .param p0    # Landroid/app/Activity;
    .param p1    # J

    const-class v1, Lcom/android/email/activity/EmailActivity;

    invoke-static {p0, v1}, Lcom/android/emailcommon/utility/IntentUtilities;->createRestartAppIntent(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    move-result-object v0

    const-wide/16 v1, -0x1

    cmp-long v1, p1, v1

    if-eqz v1, :cond_0

    const-string v1, "ACCOUNT_ID"

    invoke-virtual {v0, v1, p1, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;J)Landroid/content/Intent;

    :cond_0
    return-object v0
.end method

.method public static createOpenMailboxIntent(Landroid/app/Activity;JJ)Landroid/content/Intent;
    .locals 4
    .param p0    # Landroid/app/Activity;
    .param p1    # J
    .param p3    # J

    const-wide/16 v2, -0x1

    cmp-long v1, p1, v2

    if-eqz v1, :cond_0

    cmp-long v1, p3, v2

    if-nez v1, :cond_1

    :cond_0
    new-instance v1, Ljava/lang/IllegalArgumentException;

    invoke-direct {v1}, Ljava/lang/IllegalArgumentException;-><init>()V

    throw v1

    :cond_1
    const-class v1, Lcom/android/email/activity/EmailActivity;

    invoke-static {p0, v1}, Lcom/android/emailcommon/utility/IntentUtilities;->createRestartAppIntent(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    move-result-object v0

    const-string v1, "ACCOUNT_ID"

    invoke-virtual {v0, v1, p1, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;J)Landroid/content/Intent;

    const-string v1, "MAILBOX_ID"

    invoke-virtual {v0, v1, p3, p4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;J)Landroid/content/Intent;

    return-object v0
.end method

.method public static createOpenMessageIntent(Landroid/app/Activity;JJJ)Landroid/content/Intent;
    .locals 4
    .param p0    # Landroid/app/Activity;
    .param p1    # J
    .param p3    # J
    .param p5    # J

    const-wide/16 v2, -0x1

    cmp-long v1, p1, v2

    if-eqz v1, :cond_0

    cmp-long v1, p3, v2

    if-eqz v1, :cond_0

    cmp-long v1, p5, v2

    if-nez v1, :cond_1

    :cond_0
    new-instance v1, Ljava/lang/IllegalArgumentException;

    invoke-direct {v1}, Ljava/lang/IllegalArgumentException;-><init>()V

    throw v1

    :cond_1
    const-class v1, Lcom/android/email/activity/EmailActivity;

    invoke-static {p0, v1}, Lcom/android/emailcommon/utility/IntentUtilities;->createRestartAppIntent(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    move-result-object v0

    const-string v1, "ACCOUNT_ID"

    invoke-virtual {v0, v1, p1, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;J)Landroid/content/Intent;

    const-string v1, "MAILBOX_ID"

    invoke-virtual {v0, v1, p3, p4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;J)Landroid/content/Intent;

    const-string v1, "MESSAGE_ID"

    invoke-virtual {v0, v1, p5, p6}, Landroid/content/Intent;->putExtra(Ljava/lang/String;J)Landroid/content/Intent;

    return-object v0
.end method

.method public static createRemoteSearchIntent(Landroid/app/Activity;JJLjava/lang/String;Ljava/lang/String;)Landroid/content/Intent;
    .locals 3
    .param p0    # Landroid/app/Activity;
    .param p1    # J
    .param p3    # J
    .param p5    # Ljava/lang/String;
    .param p6    # Ljava/lang/String;

    invoke-static {p1, p2}, Lcom/android/emailcommon/provider/Account;->isNormalAccount(J)Z

    move-result v1

    const-string v2, "Can only search in normal accounts"

    invoke-static {v1, v2}, Lcom/google/common/base/Preconditions;->checkArgument(ZLjava/lang/Object;)V

    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/android/email/activity/EmailActivity;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const-string v1, "ACCOUNT_ID"

    invoke-virtual {v0, v1, p1, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;J)Landroid/content/Intent;

    const-string v1, "MAILBOX_ID"

    invoke-virtual {v0, v1, p3, p4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;J)Landroid/content/Intent;

    const-string v1, "QUERY_STRING"

    invoke-virtual {v0, v1, p5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const-string v1, "QUERY_FIELD"

    invoke-virtual {v0, v1, p6}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const-string v1, "com.android.email.action.REMOTE_SEARCH"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    return-object v0
.end method

.method private dismissErrorMessage()V
    .locals 1

    iget-object v0, p0, Lcom/android/email/activity/EmailActivity;->mErrorBanner:Lcom/android/email/activity/BannerController;

    invoke-virtual {v0}, Lcom/android/email/activity/BannerController;->dismiss()V

    return-void
.end method

.method private initFromIntent()V
    .locals 8

    const-wide/16 v6, -0x1

    invoke-virtual {p0}, Landroid/app/Activity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v4, "ACCOUNT_ID"

    invoke-virtual {v0, v4, v6, v7}, Landroid/content/Intent;->getLongExtra(Ljava/lang/String;J)J

    move-result-wide v4

    iput-wide v4, p0, Lcom/android/email/activity/EmailActivity;->mRecordAccountId:J

    invoke-static {p0, v0}, Lcom/android/email/MessageListContext;->forIntent(Landroid/content/Context;Landroid/content/Intent;)Lcom/android/email/MessageListContext;

    move-result-object v3

    if-nez v3, :cond_0

    invoke-static {p0}, Lcom/android/email/activity/Welcome;->actionStart(Landroid/app/Activity;)V

    invoke-virtual {p0}, Landroid/app/Activity;->finish()V

    :goto_0
    return-void

    :cond_0
    const-string v4, "MESSAGE_ID"

    invoke-virtual {v0, v4, v6, v7}, Landroid/content/Intent;->getLongExtra(Ljava/lang/String;J)J

    move-result-wide v1

    iget-object v4, p0, Lcom/android/email/activity/EmailActivity;->mUIController:Lcom/android/email/activity/UIControllerBase;

    invoke-virtual {v4, v3, v1, v2}, Lcom/android/email/activity/UIControllerBase;->open(Lcom/android/email/MessageListContext;J)V

    goto :goto_0
.end method

.method private initUIController()V
    .locals 2

    invoke-static {p0}, Lcom/android/email/activity/UiUtilities;->useTwoPane(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-virtual {p0}, Landroid/app/Activity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    const-string v0, "com.android.email.action.REMOTE_SEARCH"

    invoke-virtual {p0}, Landroid/app/Activity;->getIntent()Landroid/content/Intent;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-static {p0}, Lcom/android/email/activity/UiUtilities;->showTwoPaneSearchResults(Landroid/content/Context;)Z

    move-result v0

    if-nez v0, :cond_0

    new-instance v0, Lcom/android/email/activity/UIControllerSearchTwoPane;

    invoke-direct {v0, p0}, Lcom/android/email/activity/UIControllerSearchTwoPane;-><init>(Lcom/android/email/activity/EmailActivity;)V

    iput-object v0, p0, Lcom/android/email/activity/EmailActivity;->mUIController:Lcom/android/email/activity/UIControllerBase;

    :goto_0
    return-void

    :cond_0
    new-instance v0, Lcom/android/email/activity/UIControllerTwoPane;

    invoke-direct {v0, p0}, Lcom/android/email/activity/UIControllerTwoPane;-><init>(Lcom/android/email/activity/EmailActivity;)V

    iput-object v0, p0, Lcom/android/email/activity/EmailActivity;->mUIController:Lcom/android/email/activity/UIControllerBase;

    goto :goto_0

    :cond_1
    new-instance v0, Lcom/android/email/activity/UIControllerOnePane;

    invoke-direct {v0, p0}, Lcom/android/email/activity/UIControllerOnePane;-><init>(Lcom/android/email/activity/EmailActivity;)V

    iput-object v0, p0, Lcom/android/email/activity/EmailActivity;->mUIController:Lcom/android/email/activity/UIControllerBase;

    goto :goto_0
.end method

.method private onFontScaleChangeDetected()V
    .locals 0

    invoke-static {}, Lcom/android/email/activity/MessageListItem;->resetDrawingCaches()V

    return-void
.end method


# virtual methods
.method public getUIController()Lcom/android/email/activity/UIControllerBase;
    .locals 1

    iget-object v0, p0, Lcom/android/email/activity/EmailActivity;->mUIController:Lcom/android/email/activity/UIControllerBase;

    return-object v0
.end method

.method public onBackPressed()V
    .locals 3

    sget-boolean v0, Lcom/android/emailcommon/Logging;->DEBUG_LIFECYCLE:Z

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/android/email/Email;->DEBUG:Z

    if-eqz v0, :cond_0

    const-string v0, "Email"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " onBackPressed"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    iget-object v0, p0, Lcom/android/email/activity/EmailActivity;->mUIController:Lcom/android/email/activity/UIControllerBase;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/android/email/activity/UIControllerBase;->onBackPressed(Z)Z

    move-result v0

    if-nez v0, :cond_1

    invoke-super {p0}, Landroid/app/Activity;->onBackPressed()V

    :cond_1
    return-void
.end method

.method public onClick(Landroid/view/View;)V
    .locals 1
    .param p1    # Landroid/view/View;

    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    :goto_0
    return-void

    :pswitch_0
    invoke-direct {p0}, Lcom/android/email/activity/EmailActivity;->dismissErrorMessage()V

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x7f0f008e
        :pswitch_0
    .end packed-switch
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 7
    .param p1    # Landroid/os/Bundle;

    sget-boolean v3, Lcom/android/emailcommon/Logging;->DEBUG_LIFECYCLE:Z

    if-eqz v3, :cond_0

    sget-boolean v3, Lcom/android/email/Email;->DEBUG:Z

    if-eqz v3, :cond_0

    const-string v3, "Email"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v4, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " onCreate"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    invoke-super {p0, p1}, Landroid/app/Activity;->onCreate(Landroid/os/Bundle;)V

    invoke-virtual {p0}, Landroid/view/ContextThemeWrapper;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    invoke-virtual {v3}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v3

    iget v2, v3, Landroid/content/res/Configuration;->fontScale:F

    sget v3, Lcom/android/email/activity/EmailActivity;->sLastFontScale:F

    const/high16 v4, -0x40800000

    cmpl-float v3, v3, v4

    if-eqz v3, :cond_1

    sget v3, Lcom/android/email/activity/EmailActivity;->sLastFontScale:F

    cmpl-float v3, v3, v2

    if-eqz v3, :cond_1

    invoke-direct {p0}, Lcom/android/email/activity/EmailActivity;->onFontScaleChangeDetected()V

    :cond_1
    sput v2, Lcom/android/email/activity/EmailActivity;->sLastFontScale:F

    invoke-static {}, Lcom/android/email/FolderProperties;->removeInstance()V

    invoke-direct {p0}, Lcom/android/email/activity/EmailActivity;->initUIController()V

    invoke-static {p0}, Lcom/android/email/activity/ActivityHelper;->debugSetWindowFlags(Landroid/app/Activity;)V

    iget-object v3, p0, Lcom/android/email/activity/EmailActivity;->mUIController:Lcom/android/email/activity/UIControllerBase;

    invoke-virtual {v3}, Lcom/android/email/activity/UIControllerBase;->getLayoutId()I

    move-result v3

    invoke-virtual {p0, v3}, Landroid/app/Activity;->setContentView(I)V

    iget-object v3, p0, Lcom/android/email/activity/EmailActivity;->mUIController:Lcom/android/email/activity/UIControllerBase;

    invoke-virtual {v3}, Lcom/android/email/activity/UIControllerBase;->onActivityViewReady()V

    invoke-static {p0}, Lcom/android/email/Controller;->getInstance(Landroid/content/Context;)Lcom/android/email/Controller;

    move-result-object v3

    iput-object v3, p0, Lcom/android/email/activity/EmailActivity;->mController:Lcom/android/email/Controller;

    new-instance v3, Lcom/android/email/ControllerResultUiThreadWrapper;

    new-instance v4, Landroid/os/Handler;

    invoke-direct {v4}, Landroid/os/Handler;-><init>()V

    new-instance v5, Lcom/android/email/activity/EmailActivity$ControllerResult;

    const/4 v6, 0x0

    invoke-direct {v5, p0, v6}, Lcom/android/email/activity/EmailActivity$ControllerResult;-><init>(Lcom/android/email/activity/EmailActivity;Lcom/android/email/activity/EmailActivity$1;)V

    invoke-direct {v3, v4, v5}, Lcom/android/email/ControllerResultUiThreadWrapper;-><init>(Landroid/os/Handler;Lcom/android/email/Controller$Result;)V

    iput-object v3, p0, Lcom/android/email/activity/EmailActivity;->mControllerResult:Lcom/android/email/Controller$Result;

    iget-object v3, p0, Lcom/android/email/activity/EmailActivity;->mController:Lcom/android/email/Controller;

    iget-object v4, p0, Lcom/android/email/activity/EmailActivity;->mControllerResult:Lcom/android/email/Controller$Result;

    invoke-virtual {v3, v4}, Lcom/android/email/Controller;->addResultCallback(Lcom/android/email/Controller$Result;)V

    const v3, 0x7f0f008e

    invoke-virtual {p0, v3}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    invoke-virtual {v1, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    invoke-virtual {p0}, Landroid/view/ContextThemeWrapper;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f0a0010

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    new-instance v3, Lcom/android/email/activity/BannerController;

    invoke-direct {v3, p0, v1, v0}, Lcom/android/email/activity/BannerController;-><init>(Landroid/content/Context;Landroid/widget/TextView;I)V

    iput-object v3, p0, Lcom/android/email/activity/EmailActivity;->mErrorBanner:Lcom/android/email/activity/BannerController;

    if-eqz p1, :cond_2

    iget-object v3, p0, Lcom/android/email/activity/EmailActivity;->mUIController:Lcom/android/email/activity/UIControllerBase;

    invoke-virtual {v3, p1}, Lcom/android/email/activity/UIControllerBase;->onRestoreInstanceState(Landroid/os/Bundle;)V

    :goto_0
    iget-object v3, p0, Lcom/android/email/activity/EmailActivity;->mUIController:Lcom/android/email/activity/UIControllerBase;

    invoke-virtual {v3}, Lcom/android/email/activity/UIControllerBase;->onActivityCreated()V

    return-void

    :cond_2
    invoke-direct {p0}, Lcom/android/email/activity/EmailActivity;->initFromIntent()V

    goto :goto_0
.end method

.method public onCreateOptionsMenu(Landroid/view/Menu;)Z
    .locals 2
    .param p1    # Landroid/view/Menu;

    iget-object v0, p0, Lcom/android/email/activity/EmailActivity;->mUIController:Lcom/android/email/activity/UIControllerBase;

    invoke-virtual {p0}, Landroid/app/Activity;->getMenuInflater()Landroid/view/MenuInflater;

    move-result-object v1

    invoke-virtual {v0, v1, p1}, Lcom/android/email/activity/UIControllerBase;->onCreateOptionsMenu(Landroid/view/MenuInflater;Landroid/view/Menu;)Z

    move-result v0

    return v0
.end method

.method protected onDestroy()V
    .locals 3

    sget-boolean v0, Lcom/android/emailcommon/Logging;->DEBUG_LIFECYCLE:Z

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/android/email/Email;->DEBUG:Z

    if-eqz v0, :cond_0

    const-string v0, "Email"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " onDestroy"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    iget-object v0, p0, Lcom/android/email/activity/EmailActivity;->mController:Lcom/android/email/Controller;

    iget-object v1, p0, Lcom/android/email/activity/EmailActivity;->mControllerResult:Lcom/android/email/Controller$Result;

    invoke-virtual {v0, v1}, Lcom/android/email/Controller;->removeResultCallback(Lcom/android/email/Controller$Result;)V

    iget-object v0, p0, Lcom/android/email/activity/EmailActivity;->mTaskTracker:Lcom/android/emailcommon/utility/EmailAsyncTask$Tracker;

    invoke-virtual {v0}, Lcom/android/emailcommon/utility/EmailAsyncTask$Tracker;->cancellAllInterrupt()V

    iget-object v0, p0, Lcom/android/email/activity/EmailActivity;->mUIController:Lcom/android/email/activity/UIControllerBase;

    invoke-virtual {v0}, Lcom/android/email/activity/UIControllerBase;->onActivityDestroy()V

    invoke-static {}, Lcom/android/email/activity/MessageListItemCoordinates;->resetCaches()V

    invoke-super {p0}, Landroid/app/Activity;->onDestroy()V

    return-void
.end method

.method public onInstallFragment(Landroid/app/Fragment;)V
    .locals 3
    .param p1    # Landroid/app/Fragment;

    sget-boolean v0, Lcom/android/emailcommon/Logging;->DEBUG_LIFECYCLE:Z

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/android/email/Email;->DEBUG:Z

    if-eqz v0, :cond_0

    const-string v0, "Email"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " onInstallFragment fragment="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    iget-object v0, p0, Lcom/android/email/activity/EmailActivity;->mUIController:Lcom/android/email/activity/UIControllerBase;

    invoke-virtual {v0, p1}, Lcom/android/email/activity/UIControllerBase;->onInstallFragment(Landroid/app/Fragment;)V

    return-void
.end method

.method public onOptionsItemSelected(Landroid/view/MenuItem;)Z
    .locals 1
    .param p1    # Landroid/view/MenuItem;

    iget-object v0, p0, Lcom/android/email/activity/EmailActivity;->mUIController:Lcom/android/email/activity/UIControllerBase;

    invoke-virtual {v0, p1}, Lcom/android/email/activity/UIControllerBase;->onOptionsItemSelected(Landroid/view/MenuItem;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    invoke-super {p0, p1}, Landroid/app/Activity;->onOptionsItemSelected(Landroid/view/MenuItem;)Z

    move-result v0

    goto :goto_0
.end method

.method protected onPause()V
    .locals 4

    invoke-static {p0}, Lcom/android/emailcommon/utility/DataCollectUtils;->stopRecord(Landroid/content/Context;)V

    sget-boolean v1, Lcom/android/email/activity/EmailActivity;->sRecordOpening:Z

    if-eqz v1, :cond_1

    invoke-virtual {p0}, Landroid/app/Activity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    if-eqz v0, :cond_0

    const-string v1, "com.android.email.action.REMOTE_SEARCH"

    invoke-virtual {v0}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_1

    :cond_0
    invoke-static {}, Lcom/android/emailcommon/utility/DataCollectUtils;->clearRecordedList()V

    :cond_1
    sget-boolean v1, Lcom/android/emailcommon/Logging;->DEBUG_LIFECYCLE:Z

    if-eqz v1, :cond_2

    sget-boolean v1, Lcom/android/email/Email;->DEBUG:Z

    if-eqz v1, :cond_2

    const-string v1, "Email"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " onPause"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_2
    invoke-super {p0}, Landroid/app/Activity;->onPause()V

    iget-object v1, p0, Lcom/android/email/activity/EmailActivity;->mUIController:Lcom/android/email/activity/UIControllerBase;

    invoke-virtual {v1}, Lcom/android/email/activity/UIControllerBase;->onActivityPause()V

    return-void
.end method

.method public onPrepareOptionsMenu(Landroid/view/Menu;)Z
    .locals 2
    .param p1    # Landroid/view/Menu;

    iget-object v0, p0, Lcom/android/email/activity/EmailActivity;->mUIController:Lcom/android/email/activity/UIControllerBase;

    invoke-virtual {p0}, Landroid/app/Activity;->getMenuInflater()Landroid/view/MenuInflater;

    move-result-object v1

    invoke-virtual {v0, v1, p1}, Lcom/android/email/activity/UIControllerBase;->onPrepareOptionsMenu(Landroid/view/MenuInflater;Landroid/view/Menu;)Z

    move-result v0

    return v0
.end method

.method public onRemoveFragment(Landroid/app/Fragment;)V
    .locals 3
    .param p1    # Landroid/app/Fragment;

    sget-boolean v0, Lcom/android/emailcommon/Logging;->DEBUG_LIFECYCLE:Z

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/android/email/Email;->DEBUG:Z

    if-eqz v0, :cond_0

    const-string v0, "Email"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " onRemoveFragment fragment="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    iget-object v0, p0, Lcom/android/email/activity/EmailActivity;->mUIController:Lcom/android/email/activity/UIControllerBase;

    invoke-virtual {v0, p1}, Lcom/android/email/activity/UIControllerBase;->onRemoveFragment(Landroid/app/Fragment;)V

    return-void
.end method

.method protected onResume()V
    .locals 5

    const/4 v4, 0x1

    iget-wide v1, p0, Lcom/android/email/activity/EmailActivity;->mRecordAccountId:J

    sget-boolean v3, Lcom/android/email/activity/EmailActivity;->sRecordOpening:Z

    invoke-static {p0, v1, v2, v3}, Lcom/android/emailcommon/utility/DataCollectUtils;->startRecord(Landroid/content/Context;JZ)V

    sput-boolean v4, Lcom/android/email/activity/EmailActivity;->sRecordOpening:Z

    sput-boolean v4, Lcom/android/email/activity/EmailActivity;->sEmailActivityResumed:Z

    sget-boolean v1, Lcom/android/emailcommon/Logging;->DEBUG_LIFECYCLE:Z

    if-eqz v1, :cond_0

    sget-boolean v1, Lcom/android/email/Email;->DEBUG:Z

    if-eqz v1, :cond_0

    const-string v1, "Email"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " onResume"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    invoke-super {p0}, Landroid/app/Activity;->onResume()V

    iget-object v1, p0, Lcom/android/email/activity/EmailActivity;->mUIController:Lcom/android/email/activity/UIControllerBase;

    invoke-virtual {v1}, Lcom/android/email/activity/UIControllerBase;->onActivityResume()V

    invoke-virtual {p0}, Landroid/app/Activity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    if-eqz v0, :cond_1

    const-string v1, "com.android.email.action.LOCAL_SEARCH"

    invoke-virtual {v0}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/android/email/activity/EmailActivity;->mUIController:Lcom/android/email/activity/UIControllerBase;

    invoke-virtual {p0}, Landroid/app/Activity;->getIntent()Landroid/content/Intent;

    move-result-object v2

    const-string v3, "QUERY_STRING"

    invoke-virtual {v2, v3}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/android/email/activity/UIControllerBase;->onSearchRequested(Ljava/lang/String;)V

    const-string v1, ""

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    invoke-virtual {p0, v0}, Landroid/app/Activity;->setIntent(Landroid/content/Intent;)V

    :cond_1
    return-void
.end method

.method protected onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 3
    .param p1    # Landroid/os/Bundle;

    sget-boolean v0, Lcom/android/emailcommon/Logging;->DEBUG_LIFECYCLE:Z

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/android/email/Email;->DEBUG:Z

    if-eqz v0, :cond_0

    const-string v0, "Email"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " onSaveInstanceState"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    invoke-super {p0, p1}, Landroid/app/Activity;->onSaveInstanceState(Landroid/os/Bundle;)V

    iget-object v0, p0, Lcom/android/email/activity/EmailActivity;->mUIController:Lcom/android/email/activity/UIControllerBase;

    invoke-virtual {v0, p1}, Lcom/android/email/activity/UIControllerBase;->onSaveInstanceState(Landroid/os/Bundle;)V

    return-void
.end method

.method public onSearchRequested()Z
    .locals 3

    sget-boolean v0, Lcom/android/email/Email;->DEBUG:Z

    if-eqz v0, :cond_0

    const-string v0, "Email"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " onSearchRequested"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    iget-object v0, p0, Lcom/android/email/activity/EmailActivity;->mUIController:Lcom/android/email/activity/UIControllerBase;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/android/email/activity/UIControllerBase;->onSearchRequested(Ljava/lang/String;)V

    const/4 v0, 0x1

    return v0
.end method

.method protected onStart()V
    .locals 3

    sget-boolean v0, Lcom/android/emailcommon/Logging;->DEBUG_LIFECYCLE:Z

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/android/email/Email;->DEBUG:Z

    if-eqz v0, :cond_0

    const-string v0, "Email"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " onStart"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    invoke-super {p0}, Landroid/app/Activity;->onStart()V

    iget-object v0, p0, Lcom/android/email/activity/EmailActivity;->mUIController:Lcom/android/email/activity/UIControllerBase;

    invoke-virtual {v0}, Lcom/android/email/activity/UIControllerBase;->onActivityStart()V

    return-void
.end method

.method protected onStop()V
    .locals 3

    sget-boolean v0, Lcom/android/emailcommon/Logging;->DEBUG_LIFECYCLE:Z

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/android/email/Email;->DEBUG:Z

    if-eqz v0, :cond_0

    const-string v0, "Email"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " onStop"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    invoke-super {p0}, Landroid/app/Activity;->onStop()V

    iget-object v0, p0, Lcom/android/email/activity/EmailActivity;->mUIController:Lcom/android/email/activity/UIControllerBase;

    invoke-virtual {v0}, Lcom/android/email/activity/UIControllerBase;->onActivityStop()V

    return-void
.end method

.method public onUninstallFragment(Landroid/app/Fragment;)V
    .locals 3
    .param p1    # Landroid/app/Fragment;

    sget-boolean v0, Lcom/android/emailcommon/Logging;->DEBUG_LIFECYCLE:Z

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/android/email/Email;->DEBUG:Z

    if-eqz v0, :cond_0

    const-string v0, "Email"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " onUninstallFragment fragment="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    iget-object v0, p0, Lcom/android/email/activity/EmailActivity;->mUIController:Lcom/android/email/activity/UIControllerBase;

    invoke-virtual {v0, p1}, Lcom/android/email/activity/UIControllerBase;->onUninstallFragment(Landroid/app/Fragment;)V

    return-void
.end method

.method public setRecordAccount(J)V
    .locals 0
    .param p1    # J

    iput-wide p1, p0, Lcom/android/email/activity/EmailActivity;->mRecordAccountId:J

    return-void
.end method

.method public showWaitingDialog()V
    .locals 4

    iget-object v0, p0, Lcom/android/email/activity/EmailActivity;->mWaitingDialog:Lcom/android/email/activity/EmailActivity$WaitingDialog;

    if-eqz v0, :cond_0

    :goto_0
    return-void

    :cond_0
    new-instance v0, Lcom/android/email/activity/EmailActivity$WaitingDialog;

    invoke-direct {v0}, Lcom/android/email/activity/EmailActivity$WaitingDialog;-><init>()V

    iput-object v0, p0, Lcom/android/email/activity/EmailActivity;->mWaitingDialog:Lcom/android/email/activity/EmailActivity$WaitingDialog;

    iget-object v0, p0, Lcom/android/email/activity/EmailActivity;->mWaitingDialog:Lcom/android/email/activity/EmailActivity$WaitingDialog;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/app/DialogFragment;->setCancelable(Z)V

    invoke-virtual {p0}, Landroid/app/Activity;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/FragmentManager;->beginTransaction()Landroid/app/FragmentTransaction;

    move-result-object v0

    iget-object v1, p0, Lcom/android/email/activity/EmailActivity;->mWaitingDialog:Lcom/android/email/activity/EmailActivity$WaitingDialog;

    const-string v2, "waiting"

    invoke-virtual {v0, v1, v2}, Landroid/app/FragmentTransaction;->add(Landroid/app/Fragment;Ljava/lang/String;)Landroid/app/FragmentTransaction;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/FragmentTransaction;->commitAllowingStateLoss()I

    iget-object v0, p0, Lcom/android/email/activity/EmailActivity;->mHandler:Landroid/os/Handler;

    new-instance v1, Lcom/android/email/activity/EmailActivity$1;

    invoke-direct {v1, p0}, Lcom/android/email/activity/EmailActivity$1;-><init>(Lcom/android/email/activity/EmailActivity;)V

    const-wide/16 v2, 0x7d0

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    goto :goto_0
.end method
