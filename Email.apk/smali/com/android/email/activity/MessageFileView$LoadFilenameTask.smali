.class Lcom/android/email/activity/MessageFileView$LoadFilenameTask;
.super Lcom/android/emailcommon/utility/EmailAsyncTask;
.source "MessageFileView.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/email/activity/MessageFileView;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "LoadFilenameTask"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/android/emailcommon/utility/EmailAsyncTask",
        "<",
        "Ljava/lang/Void;",
        "Ljava/lang/Void;",
        "Ljava/lang/String;",
        ">;"
    }
.end annotation


# instance fields
.field private final mContentUri:Landroid/net/Uri;

.field final synthetic this$0:Lcom/android/email/activity/MessageFileView;


# direct methods
.method public constructor <init>(Lcom/android/email/activity/MessageFileView;Landroid/net/Uri;)V
    .locals 1
    .param p2    # Landroid/net/Uri;

    iput-object p1, p0, Lcom/android/email/activity/MessageFileView$LoadFilenameTask;->this$0:Lcom/android/email/activity/MessageFileView;

    invoke-static {p1}, Lcom/android/email/activity/MessageFileView;->access$000(Lcom/android/email/activity/MessageFileView;)Lcom/android/emailcommon/utility/EmailAsyncTask$Tracker;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/android/emailcommon/utility/EmailAsyncTask;-><init>(Lcom/android/emailcommon/utility/EmailAsyncTask$Tracker;)V

    iput-object p2, p0, Lcom/android/email/activity/MessageFileView$LoadFilenameTask;->mContentUri:Landroid/net/Uri;

    return-void
.end method


# virtual methods
.method protected bridge synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1
    .param p1    # [Ljava/lang/Object;

    check-cast p1, [Ljava/lang/Void;

    invoke-virtual {p0, p1}, Lcom/android/email/activity/MessageFileView$LoadFilenameTask;->doInBackground([Ljava/lang/Void;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method protected varargs doInBackground([Ljava/lang/Void;)Ljava/lang/String;
    .locals 3
    .param p1    # [Ljava/lang/Void;

    const-string v1, "LoadFilenameTask#doInBackground"

    invoke-static {v1}, Lcom/android/emailcommon/utility/EmailAsyncTask;->printStartLog(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/android/email/activity/MessageFileView$LoadFilenameTask;->this$0:Lcom/android/email/activity/MessageFileView;

    iget-object v2, p0, Lcom/android/email/activity/MessageFileView$LoadFilenameTask;->mContentUri:Landroid/net/Uri;

    invoke-static {v1, v2}, Lcom/android/emailcommon/utility/Utility;->getContentFileName(Landroid/content/Context;Landroid/net/Uri;)Ljava/lang/String;

    move-result-object v0

    const-string v1, "LoadFilenameTask#doInBackground"

    invoke-static {v1}, Lcom/android/emailcommon/utility/EmailAsyncTask;->printStopLog(Ljava/lang/String;)V

    return-object v0
.end method

.method protected bridge synthetic onSuccess(Ljava/lang/Object;)V
    .locals 0
    .param p1    # Ljava/lang/Object;

    check-cast p1, Ljava/lang/String;

    invoke-virtual {p0, p1}, Lcom/android/email/activity/MessageFileView$LoadFilenameTask;->onSuccess(Ljava/lang/String;)V

    return-void
.end method

.method protected onSuccess(Ljava/lang/String;)V
    .locals 1
    .param p1    # Ljava/lang/String;

    if-nez p1, :cond_0

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/android/email/activity/MessageFileView$LoadFilenameTask;->this$0:Lcom/android/email/activity/MessageFileView;

    invoke-static {v0, p1}, Lcom/android/email/activity/MessageFileView;->access$100(Lcom/android/email/activity/MessageFileView;Ljava/lang/String;)V

    goto :goto_0
.end method
