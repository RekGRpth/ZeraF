.class public Lcom/android/email/activity/AttachmentInfoDialog;
.super Landroid/app/DialogFragment;
.source "AttachmentInfoDialog.java"


# static fields
.field private static final BUNDLE_DENY_FLAG:Ljava/lang/String; = "denyFlag"


# instance fields
.field private mActionIntent:Landroid/content/Intent;


# direct methods
.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Landroid/app/DialogFragment;-><init>()V

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/android/email/activity/AttachmentInfoDialog;->mActionIntent:Landroid/content/Intent;

    return-void
.end method

.method static synthetic access$000(Lcom/android/email/activity/AttachmentInfoDialog;)Landroid/content/Intent;
    .locals 1
    .param p0    # Lcom/android/email/activity/AttachmentInfoDialog;

    iget-object v0, p0, Lcom/android/email/activity/AttachmentInfoDialog;->mActionIntent:Landroid/content/Intent;

    return-object v0
.end method

.method public static newInstance(Landroid/content/Context;I)Lcom/android/email/activity/AttachmentInfoDialog;
    .locals 3
    .param p0    # Landroid/content/Context;
    .param p1    # I

    new-instance v1, Lcom/android/email/activity/AttachmentInfoDialog;

    invoke-direct {v1}, Lcom/android/email/activity/AttachmentInfoDialog;-><init>()V

    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    const-string v2, "denyFlag"

    invoke-virtual {v0, v2, p1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    invoke-virtual {v1, v0}, Landroid/app/Fragment;->setArguments(Landroid/os/Bundle;)V

    return-object v1
.end method


# virtual methods
.method public onCreateDialog(Landroid/os/Bundle;)Landroid/app/Dialog;
    .locals 13
    .param p1    # Landroid/os/Bundle;

    const/high16 v12, 0x10000000

    const v11, 0x8000

    invoke-virtual {p0}, Landroid/app/Fragment;->getArguments()Landroid/os/Bundle;

    move-result-object v1

    invoke-virtual {p0}, Landroid/app/Fragment;->getActivity()Landroid/app/Activity;

    move-result-object v4

    const-string v9, "denyFlag"

    invoke-virtual {v1, v9}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v5

    invoke-virtual {v4}, Landroid/view/ContextThemeWrapper;->getResources()Landroid/content/res/Resources;

    move-result-object v7

    const v9, 0x7f0800fb

    invoke-virtual {v7, v9}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v8

    const v9, 0x7f0800ff

    invoke-virtual {v7, v9}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    const/4 v0, 0x0

    and-int/lit8 v9, v5, 0x1

    if-eqz v9, :cond_2

    const v9, 0x7f080100

    invoke-virtual {v7, v9}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    :cond_0
    :goto_0
    new-instance v6, Lcom/android/email/activity/AttachmentInfoDialog$1;

    invoke-direct {v6, p0}, Lcom/android/email/activity/AttachmentInfoDialog$1;-><init>(Lcom/android/email/activity/AttachmentInfoDialog;)V

    new-instance v3, Landroid/app/AlertDialog$Builder;

    invoke-direct {v3, v4}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    invoke-virtual {v3, v8}, Landroid/app/AlertDialog$Builder;->setTitle(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    invoke-virtual {v3, v2}, Landroid/app/AlertDialog$Builder;->setMessage(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    const v9, 0x7f080073

    invoke-virtual {v3, v9, v6}, Landroid/app/AlertDialog$Builder;->setNeutralButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    if-eqz v0, :cond_1

    iget-object v9, p0, Lcom/android/email/activity/AttachmentInfoDialog;->mActionIntent:Landroid/content/Intent;

    if-eqz v9, :cond_1

    invoke-virtual {v3, v0, v6}, Landroid/app/AlertDialog$Builder;->setPositiveButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    :cond_1
    invoke-virtual {v3}, Landroid/app/AlertDialog$Builder;->show()Landroid/app/AlertDialog;

    move-result-object v9

    return-object v9

    :cond_2
    and-int/lit8 v9, v5, 0x20

    if-eqz v9, :cond_3

    const v9, 0x7f080101

    invoke-virtual {v7, v9}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    goto :goto_0

    :cond_3
    and-int/lit8 v9, v5, 0x40

    if-eqz v9, :cond_4

    const v9, 0x7f080022

    invoke-virtual {v7, v9}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    goto :goto_0

    :cond_4
    and-int/lit8 v9, v5, 0x4

    if-eqz v9, :cond_5

    const v9, 0x7f080103

    invoke-virtual {v7, v9}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    goto :goto_0

    :cond_5
    and-int/lit8 v9, v5, 0x8

    if-eqz v9, :cond_6

    const v9, 0x7f080104

    invoke-virtual {v7, v9}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    const v9, 0x7f0800fe

    invoke-virtual {v7, v9}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    new-instance v9, Landroid/content/Intent;

    const-string v10, "android.settings.SECURITY_SETTINGS"

    invoke-direct {v9, v10}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    iput-object v9, p0, Lcom/android/email/activity/AttachmentInfoDialog;->mActionIntent:Landroid/content/Intent;

    iget-object v9, p0, Lcom/android/email/activity/AttachmentInfoDialog;->mActionIntent:Landroid/content/Intent;

    invoke-virtual {v9, v12}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    iget-object v9, p0, Lcom/android/email/activity/AttachmentInfoDialog;->mActionIntent:Landroid/content/Intent;

    invoke-virtual {v9, v11}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    goto :goto_0

    :cond_6
    and-int/lit8 v9, v5, 0x10

    if-eqz v9, :cond_7

    const v9, 0x7f080105

    invoke-virtual {v7, v9}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    goto :goto_0

    :cond_7
    and-int/lit8 v9, v5, 0x2

    if-eqz v9, :cond_0

    const v9, 0x7f0800fc

    invoke-virtual {v7, v9}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v8

    const v9, 0x7f080102

    invoke-virtual {v7, v9}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    const v9, 0x7f0800fd

    invoke-virtual {v7, v9}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    new-instance v9, Landroid/content/Intent;

    const-string v10, "android.settings.WIFI_SETTINGS"

    invoke-direct {v9, v10}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    iput-object v9, p0, Lcom/android/email/activity/AttachmentInfoDialog;->mActionIntent:Landroid/content/Intent;

    iget-object v9, p0, Lcom/android/email/activity/AttachmentInfoDialog;->mActionIntent:Landroid/content/Intent;

    invoke-virtual {v9, v12}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    iget-object v9, p0, Lcom/android/email/activity/AttachmentInfoDialog;->mActionIntent:Landroid/content/Intent;

    invoke-virtual {v9, v11}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    goto/16 :goto_0
.end method
