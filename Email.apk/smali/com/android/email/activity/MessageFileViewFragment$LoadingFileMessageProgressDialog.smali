.class public Lcom/android/email/activity/MessageFileViewFragment$LoadingFileMessageProgressDialog;
.super Landroid/app/DialogFragment;
.source "MessageFileViewFragment.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/email/activity/MessageFileViewFragment;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "LoadingFileMessageProgressDialog"
.end annotation


# static fields
.field public static final TAG:Ljava/lang/String; = "LoadingFileMessageProgressDialog"

.field private static sActivity:Landroid/app/Activity;

.field private static sFragment:Landroid/app/Fragment;


# instance fields
.field private mProgressString:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const/4 v0, 0x0

    sput-object v0, Lcom/android/email/activity/MessageFileViewFragment$LoadingFileMessageProgressDialog;->sActivity:Landroid/app/Activity;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Landroid/app/DialogFragment;-><init>()V

    return-void
.end method

.method private getProgressString(I)Ljava/lang/String;
    .locals 2
    .param p1    # I

    const/4 v0, 0x0

    packed-switch p1, :pswitch_data_0

    :goto_0
    sget-object v1, Lcom/android/email/activity/MessageFileViewFragment$LoadingFileMessageProgressDialog;->sActivity:Landroid/app/Activity;

    invoke-virtual {v1, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    return-object v1

    :pswitch_0
    const v0, 0x7f08010e

    goto :goto_0

    :pswitch_1
    const v0, 0x7f08001c

    goto :goto_0

    :pswitch_2
    const v0, 0x7f08001d

    goto :goto_0

    :pswitch_3
    const v0, 0x7f08001e

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method public static newInstance(Landroid/app/Activity;Landroid/app/Fragment;)Lcom/android/email/activity/MessageFileViewFragment$LoadingFileMessageProgressDialog;
    .locals 2
    .param p0    # Landroid/app/Activity;
    .param p1    # Landroid/app/Fragment;

    new-instance v0, Lcom/android/email/activity/MessageFileViewFragment$LoadingFileMessageProgressDialog;

    invoke-direct {v0}, Lcom/android/email/activity/MessageFileViewFragment$LoadingFileMessageProgressDialog;-><init>()V

    const/4 v1, 0x0

    invoke-virtual {v0, p1, v1}, Landroid/app/Fragment;->setTargetFragment(Landroid/app/Fragment;I)V

    sput-object p1, Lcom/android/email/activity/MessageFileViewFragment$LoadingFileMessageProgressDialog;->sFragment:Landroid/app/Fragment;

    sput-object p0, Lcom/android/email/activity/MessageFileViewFragment$LoadingFileMessageProgressDialog;->sActivity:Landroid/app/Activity;

    return-object v0
.end method


# virtual methods
.method public onCreateDialog(Landroid/os/Bundle;)Landroid/app/Dialog;
    .locals 4
    .param p1    # Landroid/os/Bundle;

    sget-object v0, Lcom/android/email/activity/MessageFileViewFragment$LoadingFileMessageProgressDialog;->sActivity:Landroid/app/Activity;

    invoke-virtual {p0}, Landroid/app/Fragment;->getTargetFragment()Landroid/app/Fragment;

    move-result-object v2

    check-cast v2, Lcom/android/email/activity/MessageFileViewFragment;

    new-instance v1, Landroid/app/ProgressDialog;

    invoke-direct {v1, v0}, Landroid/app/ProgressDialog;-><init>(Landroid/content/Context;)V

    const/4 v3, 0x1

    invoke-virtual {v1, v3}, Landroid/app/ProgressDialog;->setIndeterminate(Z)V

    const v3, 0x7f08010e

    invoke-virtual {v0, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v3}, Landroid/app/ProgressDialog;->setMessage(Ljava/lang/CharSequence;)V

    const/4 v3, 0x0

    invoke-virtual {v1, v3}, Landroid/app/Dialog;->setCanceledOnTouchOutside(Z)V

    return-object v1
.end method

.method public onDismiss(Landroid/content/DialogInterface;)V
    .locals 2
    .param p1    # Landroid/content/DialogInterface;

    invoke-super {p0, p1}, Landroid/app/DialogFragment;->onDismiss(Landroid/content/DialogInterface;)V

    invoke-virtual {p0}, Landroid/app/Fragment;->getTargetFragment()Landroid/app/Fragment;

    move-result-object v0

    check-cast v0, Lcom/android/email/activity/MessageFileViewFragment;

    invoke-static {v0}, Lcom/android/email/activity/MessageFileViewFragment;->access$100(Lcom/android/email/activity/MessageFileViewFragment;)Z

    move-result v1

    if-nez v1, :cond_0

    invoke-virtual {v0}, Lcom/android/email/activity/MessageFileViewFragment;->onCheckingDialogCancel()V

    :cond_0
    return-void
.end method

.method public updateProgress(I)V
    .locals 2
    .param p1    # I

    invoke-virtual {p0}, Landroid/app/DialogFragment;->getDialog()Landroid/app/Dialog;

    move-result-object v0

    check-cast v0, Landroid/app/ProgressDialog;

    if-eqz v0, :cond_0

    invoke-direct {p0, p1}, Lcom/android/email/activity/MessageFileViewFragment$LoadingFileMessageProgressDialog;->getProgressString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/app/ProgressDialog;->setMessage(Ljava/lang/CharSequence;)V

    :cond_0
    return-void
.end method
