.class public Lcom/android/email/activity/setup/EmailAccountAutoCompleteTextView;
.super Landroid/widget/MultiAutoCompleteTextView;
.source "EmailAccountAutoCompleteTextView.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/email/activity/setup/EmailAccountAutoCompleteTextView$EmailAccountTokenizer;
    }
.end annotation


# instance fields
.field mAccountTokenizer:Landroid/widget/MultiAutoCompleteTextView$Tokenizer;

.field mCommaTokenizer:Landroid/widget/MultiAutoCompleteTextView$Tokenizer;

.field private mIMEFullScreenMode:Z

.field private mLastTextLength:I

.field mPopup:Landroid/widget/ListPopupWindow;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1    # Landroid/content/Context;

    const/4 v0, 0x0

    invoke-direct {p0, p1}, Landroid/widget/MultiAutoCompleteTextView;-><init>(Landroid/content/Context;)V

    iput-boolean v0, p0, Lcom/android/email/activity/setup/EmailAccountAutoCompleteTextView;->mIMEFullScreenMode:Z

    iput v0, p0, Lcom/android/email/activity/setup/EmailAccountAutoCompleteTextView;->mLastTextLength:I

    new-instance v0, Lcom/android/email/activity/setup/EmailAccountAutoCompleteTextView$EmailAccountTokenizer;

    invoke-direct {v0}, Lcom/android/email/activity/setup/EmailAccountAutoCompleteTextView$EmailAccountTokenizer;-><init>()V

    iput-object v0, p0, Lcom/android/email/activity/setup/EmailAccountAutoCompleteTextView;->mAccountTokenizer:Landroid/widget/MultiAutoCompleteTextView$Tokenizer;

    new-instance v0, Landroid/widget/MultiAutoCompleteTextView$CommaTokenizer;

    invoke-direct {v0}, Landroid/widget/MultiAutoCompleteTextView$CommaTokenizer;-><init>()V

    iput-object v0, p0, Lcom/android/email/activity/setup/EmailAccountAutoCompleteTextView;->mCommaTokenizer:Landroid/widget/MultiAutoCompleteTextView$Tokenizer;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/util/AttributeSet;

    const/4 v0, 0x0

    invoke-direct {p0, p1, p2}, Landroid/widget/MultiAutoCompleteTextView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    iput-boolean v0, p0, Lcom/android/email/activity/setup/EmailAccountAutoCompleteTextView;->mIMEFullScreenMode:Z

    iput v0, p0, Lcom/android/email/activity/setup/EmailAccountAutoCompleteTextView;->mLastTextLength:I

    new-instance v0, Lcom/android/email/activity/setup/EmailAccountAutoCompleteTextView$EmailAccountTokenizer;

    invoke-direct {v0}, Lcom/android/email/activity/setup/EmailAccountAutoCompleteTextView$EmailAccountTokenizer;-><init>()V

    iput-object v0, p0, Lcom/android/email/activity/setup/EmailAccountAutoCompleteTextView;->mAccountTokenizer:Landroid/widget/MultiAutoCompleteTextView$Tokenizer;

    new-instance v0, Landroid/widget/MultiAutoCompleteTextView$CommaTokenizer;

    invoke-direct {v0}, Landroid/widget/MultiAutoCompleteTextView$CommaTokenizer;-><init>()V

    iput-object v0, p0, Lcom/android/email/activity/setup/EmailAccountAutoCompleteTextView;->mCommaTokenizer:Landroid/widget/MultiAutoCompleteTextView$Tokenizer;

    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Landroid/widget/AutoCompleteTextView;->setThreshold(I)V

    new-instance v0, Lcom/android/email/activity/setup/EmailAccountAutoCompleteTextView$EmailAccountTokenizer;

    invoke-direct {v0}, Lcom/android/email/activity/setup/EmailAccountAutoCompleteTextView$EmailAccountTokenizer;-><init>()V

    invoke-virtual {p0, v0}, Landroid/widget/MultiAutoCompleteTextView;->setTokenizer(Landroid/widget/MultiAutoCompleteTextView$Tokenizer;)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 1
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/util/AttributeSet;
    .param p3    # I

    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, p3}, Landroid/widget/MultiAutoCompleteTextView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    iput-boolean v0, p0, Lcom/android/email/activity/setup/EmailAccountAutoCompleteTextView;->mIMEFullScreenMode:Z

    iput v0, p0, Lcom/android/email/activity/setup/EmailAccountAutoCompleteTextView;->mLastTextLength:I

    new-instance v0, Lcom/android/email/activity/setup/EmailAccountAutoCompleteTextView$EmailAccountTokenizer;

    invoke-direct {v0}, Lcom/android/email/activity/setup/EmailAccountAutoCompleteTextView$EmailAccountTokenizer;-><init>()V

    iput-object v0, p0, Lcom/android/email/activity/setup/EmailAccountAutoCompleteTextView;->mAccountTokenizer:Landroid/widget/MultiAutoCompleteTextView$Tokenizer;

    new-instance v0, Landroid/widget/MultiAutoCompleteTextView$CommaTokenizer;

    invoke-direct {v0}, Landroid/widget/MultiAutoCompleteTextView$CommaTokenizer;-><init>()V

    iput-object v0, p0, Lcom/android/email/activity/setup/EmailAccountAutoCompleteTextView;->mCommaTokenizer:Landroid/widget/MultiAutoCompleteTextView$Tokenizer;

    return-void
.end method

.method private getEditTextLength()I
    .locals 2

    invoke-virtual {p0}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v1

    return v1
.end method

.method private replaceTextAndHighLight(Ljava/lang/CharSequence;)V
    .locals 9
    .param p1    # Ljava/lang/CharSequence;

    invoke-virtual {p0}, Landroid/widget/TextView;->clearComposingText()V

    invoke-virtual {p0}, Landroid/widget/TextView;->getSelectionEnd()I

    move-result v3

    invoke-virtual {p0}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v2

    invoke-virtual {p0}, Landroid/widget/AutoCompleteTextView;->getAdapter()Landroid/widget/ListAdapter;

    move-result-object v0

    check-cast v0, Lcom/android/email/activity/setup/DropdownAccountsArrayAdapter;

    invoke-virtual {v0}, Lcom/android/email/activity/setup/DropdownAccountsArrayAdapter;->getFilter()Landroid/widget/Filter;

    move-result-object v4

    const/4 v7, 0x0

    const/4 v5, 0x0

    instance-of v8, v4, Lcom/android/email/activity/setup/DropdownAddressFilter;

    if-eqz v8, :cond_1

    check-cast v4, Lcom/android/email/activity/setup/DropdownAddressFilter;

    invoke-virtual {v4}, Lcom/android/email/activity/setup/DropdownAccountsFilter;->getFilterString()Ljava/lang/String;

    move-result-object v5

    iget-object v8, p0, Lcom/android/email/activity/setup/EmailAccountAutoCompleteTextView;->mCommaTokenizer:Landroid/widget/MultiAutoCompleteTextView$Tokenizer;

    invoke-interface {v8, v2, v3}, Landroid/widget/MultiAutoCompleteTextView$Tokenizer;->findTokenStart(Ljava/lang/CharSequence;I)I

    move-result v7

    :goto_0
    invoke-interface {v2, v7, v3}, Landroid/text/Editable;->subSequence(II)Ljava/lang/CharSequence;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v5, v1}, Lcom/android/emailcommon/utility/TextUtilities;->stringOrNullEquals(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v8

    if-eqz v8, :cond_0

    const/4 v8, 0x0

    invoke-virtual {p0, p1, v8}, Landroid/widget/AutoCompleteTextView;->setText(Ljava/lang/CharSequence;Z)V

    invoke-virtual {p0}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v6

    invoke-interface {v6}, Landroid/text/Editable;->length()I

    move-result v8

    invoke-static {v6, v3, v8}, Landroid/text/Selection;->setSelection(Landroid/text/Spannable;II)V

    :cond_0
    return-void

    :cond_1
    check-cast v4, Lcom/android/email/activity/setup/DropdownAccountsFilter;

    invoke-virtual {v4}, Lcom/android/email/activity/setup/DropdownAccountsFilter;->getFilterString()Ljava/lang/String;

    move-result-object v5

    iget-object v8, p0, Lcom/android/email/activity/setup/EmailAccountAutoCompleteTextView;->mAccountTokenizer:Landroid/widget/MultiAutoCompleteTextView$Tokenizer;

    invoke-interface {v8, v2, v3}, Landroid/widget/MultiAutoCompleteTextView$Tokenizer;->findTokenStart(Ljava/lang/CharSequence;I)I

    move-result v7

    goto :goto_0
.end method


# virtual methods
.method public onFilterComplete(I)V
    .locals 2
    .param p1    # I

    invoke-super {p0, p1}, Landroid/widget/AutoCompleteTextView;->onFilterComplete(I)V

    invoke-direct {p0}, Lcom/android/email/activity/setup/EmailAccountAutoCompleteTextView;->getEditTextLength()I

    move-result v0

    iget v1, p0, Lcom/android/email/activity/setup/EmailAccountAutoCompleteTextView;->mLastTextLength:I

    if-le v0, v1, :cond_0

    invoke-virtual {p0}, Lcom/android/email/activity/setup/EmailAccountAutoCompleteTextView;->replacePartTextIfNeed()V

    :cond_0
    iput v0, p0, Lcom/android/email/activity/setup/EmailAccountAutoCompleteTextView;->mLastTextLength:I

    return-void
.end method

.method public onRestoreInstanceState(Landroid/os/Parcelable;)V
    .locals 1
    .param p1    # Landroid/os/Parcelable;

    invoke-super {p0, p1}, Landroid/widget/TextView;->onRestoreInstanceState(Landroid/os/Parcelable;)V

    invoke-direct {p0}, Lcom/android/email/activity/setup/EmailAccountAutoCompleteTextView;->getEditTextLength()I

    move-result v0

    iput v0, p0, Lcom/android/email/activity/setup/EmailAccountAutoCompleteTextView;->mLastTextLength:I

    return-void
.end method

.method public replacePartTextIfNeed()V
    .locals 5

    invoke-virtual {p0}, Landroid/widget/AutoCompleteTextView;->getAdapter()Landroid/widget/ListAdapter;

    move-result-object v0

    check-cast v0, Landroid/widget/BaseAdapter;

    invoke-virtual {v0}, Landroid/widget/BaseAdapter;->getCount()I

    move-result v4

    if-nez v4, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    invoke-virtual {p0}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x0

    invoke-virtual {v0, v4}, Landroid/widget/BaseAdapter;->getItem(I)Ljava/lang/Object;

    move-result-object v4

    invoke-virtual {p0, v4}, Landroid/widget/AutoCompleteTextView;->convertSelectionToString(Ljava/lang/Object;)Ljava/lang/CharSequence;

    move-result-object v2

    invoke-virtual {v2, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_2

    invoke-virtual {p0}, Landroid/widget/AutoCompleteTextView;->dismissDropDown()V

    goto :goto_0

    :cond_2
    invoke-virtual {v0}, Landroid/widget/BaseAdapter;->getCount()I

    move-result v1

    const/4 v4, 0x1

    if-ne v1, v4, :cond_0

    iget-boolean v4, p0, Lcom/android/email/activity/setup/EmailAccountAutoCompleteTextView;->mIMEFullScreenMode:Z

    if-nez v4, :cond_0

    invoke-direct {p0, v2}, Lcom/android/email/activity/setup/EmailAccountAutoCompleteTextView;->replaceTextAndHighLight(Ljava/lang/CharSequence;)V

    invoke-virtual {p0}, Landroid/widget/AutoCompleteTextView;->dismissDropDown()V

    goto :goto_0
.end method

.method protected replaceText(Ljava/lang/CharSequence;)V
    .locals 2
    .param p1    # Ljava/lang/CharSequence;

    invoke-virtual {p0}, Landroid/widget/TextView;->clearComposingText()V

    invoke-virtual {p0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    invoke-virtual {p0}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-interface {v0}, Landroid/text/Editable;->length()I

    move-result v1

    invoke-static {v0, v1}, Landroid/text/Selection;->setSelection(Landroid/text/Spannable;I)V

    invoke-direct {p0}, Lcom/android/email/activity/setup/EmailAccountAutoCompleteTextView;->getEditTextLength()I

    move-result v1

    iput v1, p0, Lcom/android/email/activity/setup/EmailAccountAutoCompleteTextView;->mLastTextLength:I

    return-void
.end method
