.class public Lcom/android/email/activity/MessageCompose$LoadingAttachProgressDialog;
.super Landroid/app/DialogFragment;
.source "MessageCompose.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/email/activity/MessageCompose;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "LoadingAttachProgressDialog"
.end annotation


# static fields
.field public static final TAG:Ljava/lang/String; = "LoadingAttachProgressDialog"

.field static sLoadingTask:Lcom/android/emailcommon/utility/EmailAsyncTask;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const/4 v0, 0x0

    sput-object v0, Lcom/android/email/activity/MessageCompose$LoadingAttachProgressDialog;->sLoadingTask:Lcom/android/emailcommon/utility/EmailAsyncTask;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Landroid/app/DialogFragment;-><init>()V

    return-void
.end method

.method public static newInstance(Lcom/android/emailcommon/utility/EmailAsyncTask;Landroid/app/Fragment;)Lcom/android/email/activity/MessageCompose$LoadingAttachProgressDialog;
    .locals 2
    .param p0    # Lcom/android/emailcommon/utility/EmailAsyncTask;
    .param p1    # Landroid/app/Fragment;

    new-instance v0, Lcom/android/email/activity/MessageCompose$LoadingAttachProgressDialog;

    invoke-direct {v0}, Lcom/android/email/activity/MessageCompose$LoadingAttachProgressDialog;-><init>()V

    const/4 v1, 0x0

    invoke-virtual {v0, p1, v1}, Landroid/app/Fragment;->setTargetFragment(Landroid/app/Fragment;I)V

    sput-object p0, Lcom/android/email/activity/MessageCompose$LoadingAttachProgressDialog;->sLoadingTask:Lcom/android/emailcommon/utility/EmailAsyncTask;

    return-object v0
.end method


# virtual methods
.method public onCancel(Landroid/content/DialogInterface;)V
    .locals 2
    .param p1    # Landroid/content/DialogInterface;

    invoke-super {p0, p1}, Landroid/app/DialogFragment;->onCancel(Landroid/content/DialogInterface;)V

    const-string v0, "MessageCompose"

    const-string v1, "LoadingAttachProgressDialog is onCancel and mLoadingTask will be canceled too"

    invoke-static {v0, v1}, Lcom/android/emailcommon/Logging;->d(Ljava/lang/String;Ljava/lang/String;)V

    sget-object v0, Lcom/android/email/activity/MessageCompose$LoadingAttachProgressDialog;->sLoadingTask:Lcom/android/emailcommon/utility/EmailAsyncTask;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/android/emailcommon/utility/EmailAsyncTask;->cancel(Z)V

    return-void
.end method

.method public onCreateDialog(Landroid/os/Bundle;)Landroid/app/Dialog;
    .locals 4
    .param p1    # Landroid/os/Bundle;

    const/4 v3, 0x1

    invoke-virtual {p0}, Landroid/app/Fragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    new-instance v1, Landroid/app/ProgressDialog;

    invoke-direct {v1, v0}, Landroid/app/ProgressDialog;-><init>(Landroid/content/Context;)V

    invoke-virtual {v1, v3}, Landroid/app/ProgressDialog;->setIndeterminate(Z)V

    const v2, 0x7f08001a

    invoke-virtual {p0, v2}, Landroid/app/Fragment;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/app/ProgressDialog;->setMessage(Ljava/lang/CharSequence;)V

    invoke-virtual {v1, v3}, Landroid/app/Dialog;->setCancelable(Z)V

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Landroid/app/Dialog;->setCanceledOnTouchOutside(Z)V

    return-object v1
.end method
