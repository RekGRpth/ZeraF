.class Lcom/android/email/activity/MailboxMoveToAdapter;
.super Landroid/widget/CursorAdapter;
.source "MailboxMoveToAdapter.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/email/activity/MailboxMoveToAdapter$MailboxMoveToLoader;
    }
.end annotation


# static fields
.field private static final ALL_MAILBOX_SELECTION:Ljava/lang/String; = "accountKey=? AND type<64 AND flagVisible=1"

.field private static final COLUMN_ID:I = 0x1

.field private static final COLUMN_MAILBOX_NAME:I = 0x2

.field private static final COLUMN_TYPE:I = 0x3

.field private static final EXCLUDE_MAILBOX_SELECTION:Ljava/lang/String; = "_id!=?"

.field private static final MOVE_TO_ORDER_BY_DISPLAY_NAME:Ljava/lang/String; = "CASE type WHEN 0 THEN 0 WHEN 7 THEN 1 ELSE 10 END, displayName"

.field private static final MOVE_TO_ORDER_BY_SERVER_ID:Ljava/lang/String; = "CASE type WHEN 0 THEN 0 WHEN 7 THEN 1 ELSE 10 END, serverId"

.field private static final MOVE_TO_ORDER_BY_STATIC:Ljava/lang/String; = "CASE type WHEN 0 THEN 0 WHEN 7 THEN 1 ELSE 10 END"

.field private static final MOVE_TO_PROJECTION_DISPLAY_NAME:[Ljava/lang/String;

.field private static final MOVE_TO_PROJECTION_SERVER_ID:[Ljava/lang/String;

.field private static final MOVE_TO_SELECTION:Ljava/lang/String; = "accountKey=? AND type<64 AND flagVisible=1 AND type NOT IN (3,4,5,6) AND (flags & 16 != 0) AND _id!=?"

.field private static final TARGET_MAILBOX_SELECTION:Ljava/lang/String; = "type NOT IN (3,4,5,6) AND (flags & 16 != 0)"


# instance fields
.field private final mInflater:Landroid/view/LayoutInflater;


# direct methods
.method static constructor <clinit>()V
    .locals 7

    const/4 v6, 0x4

    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    new-array v0, v6, [Ljava/lang/String;

    const-string v1, "_id"

    aput-object v1, v0, v2

    const-string v1, "_id AS org_mailbox_id"

    aput-object v1, v0, v3

    const-string v1, "serverId"

    aput-object v1, v0, v4

    const-string v1, "type"

    aput-object v1, v0, v5

    sput-object v0, Lcom/android/email/activity/MailboxMoveToAdapter;->MOVE_TO_PROJECTION_SERVER_ID:[Ljava/lang/String;

    new-array v0, v6, [Ljava/lang/String;

    const-string v1, "_id"

    aput-object v1, v0, v2

    const-string v1, "_id AS org_mailbox_id"

    aput-object v1, v0, v3

    const-string v1, "displayName"

    aput-object v1, v0, v4

    const-string v1, "type"

    aput-object v1, v0, v5

    sput-object v0, Lcom/android/email/activity/MailboxMoveToAdapter;->MOVE_TO_PROJECTION_DISPLAY_NAME:[Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 2
    .param p1    # Landroid/content/Context;

    const/4 v0, 0x0

    const/4 v1, 0x0

    invoke-direct {p0, p1, v0, v1}, Landroid/widget/CursorAdapter;-><init>(Landroid/content/Context;Landroid/database/Cursor;I)V

    invoke-static {p1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    iput-object v0, p0, Lcom/android/email/activity/MailboxMoveToAdapter;->mInflater:Landroid/view/LayoutInflater;

    return-void
.end method

.method static synthetic access$000()[Ljava/lang/String;
    .locals 1

    sget-object v0, Lcom/android/email/activity/MailboxMoveToAdapter;->MOVE_TO_PROJECTION_DISPLAY_NAME:[Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$100()[Ljava/lang/String;
    .locals 1

    sget-object v0, Lcom/android/email/activity/MailboxMoveToAdapter;->MOVE_TO_PROJECTION_SERVER_ID:[Ljava/lang/String;

    return-object v0
.end method

.method static createLoader(Landroid/content/Context;JJ)Landroid/content/Loader;
    .locals 6
    .param p0    # Landroid/content/Context;
    .param p1    # J
    .param p3    # J
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "JJ)",
            "Landroid/content/Loader",
            "<",
            "Landroid/database/Cursor;",
            ">;"
        }
    .end annotation

    sget-boolean v0, Lcom/android/emailcommon/Logging;->DEBUG_LIFECYCLE:Z

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/android/email/Email;->DEBUG:Z

    if-eqz v0, :cond_0

    const-string v0, "Email"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "MailboxDialogAdapter#createLoader accountId="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1, p2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", mailboxId="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p3, p4}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    new-instance v0, Lcom/android/email/activity/MailboxMoveToAdapter$MailboxMoveToLoader;

    move-object v1, p0

    move-wide v2, p1

    move-wide v4, p3

    invoke-direct/range {v0 .. v5}, Lcom/android/email/activity/MailboxMoveToAdapter$MailboxMoveToLoader;-><init>(Landroid/content/Context;JJ)V

    return-object v0
.end method

.method private static getDisplayText(Landroid/content/Context;Landroid/database/Cursor;)Ljava/lang/String;
    .locals 5
    .param p0    # Landroid/content/Context;
    .param p1    # Landroid/database/Cursor;

    const/4 v3, 0x3

    invoke-interface {p1, v3}, Landroid/database/Cursor;->getInt(I)I

    move-result v2

    const/4 v3, 0x1

    invoke-interface {p1, v3}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v0

    invoke-static {p0}, Lcom/android/email/FolderProperties;->getInstance(Landroid/content/Context;)Lcom/android/email/FolderProperties;

    move-result-object v3

    const/4 v4, 0x2

    invoke-interface {p1, v4}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v2, v0, v1, v4}, Lcom/android/email/FolderProperties;->getDisplayName(IJLjava/lang/String;)Ljava/lang/String;

    move-result-object v3

    return-object v3
.end method


# virtual methods
.method public bindView(Landroid/view/View;Landroid/content/Context;Landroid/database/Cursor;)V
    .locals 2
    .param p1    # Landroid/view/View;
    .param p2    # Landroid/content/Context;
    .param p3    # Landroid/database/Cursor;

    instance-of v1, p1, Landroid/widget/TextView;

    if-eqz v1, :cond_0

    move-object v0, p1

    check-cast v0, Landroid/widget/TextView;

    invoke-static {p2, p3}, Lcom/android/email/activity/MailboxMoveToAdapter;->getDisplayText(Landroid/content/Context;Landroid/database/Cursor;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    :cond_0
    return-void
.end method

.method public newView(Landroid/content/Context;Landroid/database/Cursor;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 3
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/database/Cursor;
    .param p3    # Landroid/view/ViewGroup;

    iget-object v0, p0, Lcom/android/email/activity/MailboxMoveToAdapter;->mInflater:Landroid/view/LayoutInflater;

    const v1, 0x1090003

    const/4 v2, 0x0

    invoke-virtual {v0, v1, p3, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    return-object v0
.end method
