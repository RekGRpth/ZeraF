.class public Lcom/mediatek/lbs/em/ViewGps;
.super Landroid/view/View;
.source "ViewGps.java"


# instance fields
.field private final CIRCLE_SIZE:F

.field private final MAX_SATELLITE_NUM:I

.field mAzi:[F

.field private mBackground:Landroid/graphics/Paint;

.field private mBaseSize:F

.field private mBaseX:F

.field private mBaseY:F

.field mEle:[F

.field private mGreenCircle:Landroid/graphics/Paint;

.field private mLine:Landroid/graphics/Paint;

.field mPnrs:[I

.field private mRedCircle:Landroid/graphics/Paint;

.field mSatInView:I

.field mSnrs:[F

.field mUsed:[Z

.field private mWhiteText:Landroid/graphics/Paint;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 2
    .param p1    # Landroid/content/Context;

    const/4 v0, 0x0

    const/4 v1, 0x0

    invoke-direct {p0, p1, v0, v1}, Lcom/mediatek/lbs/em/ViewGps;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/util/AttributeSet;

    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/mediatek/lbs/em/ViewGps;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 5
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/util/AttributeSet;
    .param p3    # I

    const/4 v4, -0x1

    const/4 v3, 0x1

    const/high16 v2, 0x3f800000

    const/16 v1, 0x20

    invoke-direct {p0, p1, p2, p3}, Landroid/view/View;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    iput v1, p0, Lcom/mediatek/lbs/em/ViewGps;->MAX_SATELLITE_NUM:I

    const/high16 v0, 0x41700000

    iput v0, p0, Lcom/mediatek/lbs/em/ViewGps;->CIRCLE_SIZE:F

    const/4 v0, 0x0

    iput v0, p0, Lcom/mediatek/lbs/em/ViewGps;->mSatInView:I

    new-array v0, v1, [I

    iput-object v0, p0, Lcom/mediatek/lbs/em/ViewGps;->mPnrs:[I

    new-array v0, v1, [F

    iput-object v0, p0, Lcom/mediatek/lbs/em/ViewGps;->mSnrs:[F

    new-array v0, v1, [F

    iput-object v0, p0, Lcom/mediatek/lbs/em/ViewGps;->mEle:[F

    new-array v0, v1, [F

    iput-object v0, p0, Lcom/mediatek/lbs/em/ViewGps;->mAzi:[F

    new-array v0, v1, [Z

    iput-object v0, p0, Lcom/mediatek/lbs/em/ViewGps;->mUsed:[Z

    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0}, Landroid/graphics/Paint;-><init>()V

    iput-object v0, p0, Lcom/mediatek/lbs/em/ViewGps;->mBackground:Landroid/graphics/Paint;

    iget-object v0, p0, Lcom/mediatek/lbs/em/ViewGps;->mBackground:Landroid/graphics/Paint;

    const v1, -0xcccccd

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setColor(I)V

    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0}, Landroid/graphics/Paint;-><init>()V

    iput-object v0, p0, Lcom/mediatek/lbs/em/ViewGps;->mLine:Landroid/graphics/Paint;

    iget-object v0, p0, Lcom/mediatek/lbs/em/ViewGps;->mLine:Landroid/graphics/Paint;

    invoke-virtual {v0, v4}, Landroid/graphics/Paint;->setColor(I)V

    iget-object v0, p0, Lcom/mediatek/lbs/em/ViewGps;->mLine:Landroid/graphics/Paint;

    invoke-virtual {v0, v3}, Landroid/graphics/Paint;->setAntiAlias(Z)V

    iget-object v0, p0, Lcom/mediatek/lbs/em/ViewGps;->mLine:Landroid/graphics/Paint;

    sget-object v1, Landroid/graphics/Paint$Style;->STROKE:Landroid/graphics/Paint$Style;

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    iget-object v0, p0, Lcom/mediatek/lbs/em/ViewGps;->mLine:Landroid/graphics/Paint;

    invoke-virtual {v0, v2}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0}, Landroid/graphics/Paint;-><init>()V

    iput-object v0, p0, Lcom/mediatek/lbs/em/ViewGps;->mRedCircle:Landroid/graphics/Paint;

    iget-object v0, p0, Lcom/mediatek/lbs/em/ViewGps;->mRedCircle:Landroid/graphics/Paint;

    const/high16 v1, -0x10010000

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setColor(I)V

    iget-object v0, p0, Lcom/mediatek/lbs/em/ViewGps;->mRedCircle:Landroid/graphics/Paint;

    invoke-virtual {v0, v3}, Landroid/graphics/Paint;->setAntiAlias(Z)V

    iget-object v0, p0, Lcom/mediatek/lbs/em/ViewGps;->mRedCircle:Landroid/graphics/Paint;

    sget-object v1, Landroid/graphics/Paint$Style;->FILL_AND_STROKE:Landroid/graphics/Paint$Style;

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    iget-object v0, p0, Lcom/mediatek/lbs/em/ViewGps;->mRedCircle:Landroid/graphics/Paint;

    invoke-virtual {v0, v2}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0}, Landroid/graphics/Paint;-><init>()V

    iput-object v0, p0, Lcom/mediatek/lbs/em/ViewGps;->mGreenCircle:Landroid/graphics/Paint;

    iget-object v0, p0, Lcom/mediatek/lbs/em/ViewGps;->mGreenCircle:Landroid/graphics/Paint;

    const v1, -0x44ff4500

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setColor(I)V

    iget-object v0, p0, Lcom/mediatek/lbs/em/ViewGps;->mGreenCircle:Landroid/graphics/Paint;

    invoke-virtual {v0, v3}, Landroid/graphics/Paint;->setAntiAlias(Z)V

    iget-object v0, p0, Lcom/mediatek/lbs/em/ViewGps;->mGreenCircle:Landroid/graphics/Paint;

    sget-object v1, Landroid/graphics/Paint$Style;->FILL:Landroid/graphics/Paint$Style;

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    iget-object v0, p0, Lcom/mediatek/lbs/em/ViewGps;->mGreenCircle:Landroid/graphics/Paint;

    invoke-virtual {v0, v2}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0}, Landroid/graphics/Paint;-><init>()V

    iput-object v0, p0, Lcom/mediatek/lbs/em/ViewGps;->mWhiteText:Landroid/graphics/Paint;

    iget-object v0, p0, Lcom/mediatek/lbs/em/ViewGps;->mWhiteText:Landroid/graphics/Paint;

    invoke-virtual {v0, v4}, Landroid/graphics/Paint;->setColor(I)V

    iget-object v0, p0, Lcom/mediatek/lbs/em/ViewGps;->mWhiteText:Landroid/graphics/Paint;

    invoke-virtual {v0, v3}, Landroid/graphics/Paint;->setAntiAlias(Z)V

    iget-object v0, p0, Lcom/mediatek/lbs/em/ViewGps;->mWhiteText:Landroid/graphics/Paint;

    sget-object v1, Landroid/graphics/Paint$Style;->FILL:Landroid/graphics/Paint$Style;

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    iget-object v0, p0, Lcom/mediatek/lbs/em/ViewGps;->mWhiteText:Landroid/graphics/Paint;

    invoke-virtual {v0, v2}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    iget-object v0, p0, Lcom/mediatek/lbs/em/ViewGps;->mWhiteText:Landroid/graphics/Paint;

    sget-object v1, Landroid/graphics/Paint$Align;->CENTER:Landroid/graphics/Paint$Align;

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setTextAlign(Landroid/graphics/Paint$Align;)V

    iget-object v0, p0, Lcom/mediatek/lbs/em/ViewGps;->mWhiteText:Landroid/graphics/Paint;

    const/high16 v1, 0x41a00000

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setTextSize(F)V

    return-void
.end method

.method private log(Ljava/lang/String;)V
    .locals 1
    .param p1    # Ljava/lang/String;

    const-string v0, "LocationEM"

    invoke-static {v0, p1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    return-void
.end method


# virtual methods
.method protected onDraw(Landroid/graphics/Canvas;)V
    .locals 16
    .param p1    # Landroid/graphics/Canvas;

    invoke-super/range {p0 .. p1}, Landroid/view/View;->onDraw(Landroid/graphics/Canvas;)V

    invoke-virtual/range {p0 .. p0}, Lcom/mediatek/lbs/em/ViewGps;->getWidth()I

    move-result v1

    div-int/lit8 v1, v1, 0x3

    int-to-float v1, v1

    move-object/from16 v0, p0

    iput v1, v0, Lcom/mediatek/lbs/em/ViewGps;->mBaseSize:F

    invoke-virtual/range {p0 .. p0}, Lcom/mediatek/lbs/em/ViewGps;->getWidth()I

    move-result v1

    div-int/lit8 v1, v1, 0x2

    int-to-float v1, v1

    move-object/from16 v0, p0

    iput v1, v0, Lcom/mediatek/lbs/em/ViewGps;->mBaseX:F

    invoke-virtual/range {p0 .. p0}, Lcom/mediatek/lbs/em/ViewGps;->getWidth()I

    move-result v1

    div-int/lit8 v1, v1, 0x2

    int-to-float v1, v1

    invoke-virtual/range {p0 .. p0}, Lcom/mediatek/lbs/em/ViewGps;->getWidth()I

    move-result v2

    div-int/lit8 v2, v2, 0x2

    int-to-float v2, v2

    move-object/from16 v0, p0

    iget v3, v0, Lcom/mediatek/lbs/em/ViewGps;->mBaseSize:F

    sub-float/2addr v2, v3

    sub-float/2addr v1, v2

    move-object/from16 v0, p0

    iput v1, v0, Lcom/mediatek/lbs/em/ViewGps;->mBaseY:F

    move-object/from16 v0, p0

    iget v1, v0, Lcom/mediatek/lbs/em/ViewGps;->mBaseY:F

    const/high16 v2, 0x41700000

    add-float/2addr v1, v2

    move-object/from16 v0, p0

    iput v1, v0, Lcom/mediatek/lbs/em/ViewGps;->mBaseY:F

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/mediatek/lbs/em/ViewGps;->mBackground:Landroid/graphics/Paint;

    move-object/from16 v0, p1

    invoke-virtual {v0, v1}, Landroid/graphics/Canvas;->drawPaint(Landroid/graphics/Paint;)V

    move-object/from16 v0, p0

    iget v1, v0, Lcom/mediatek/lbs/em/ViewGps;->mBaseX:F

    move-object/from16 v0, p0

    iget v2, v0, Lcom/mediatek/lbs/em/ViewGps;->mBaseY:F

    move-object/from16 v0, p0

    iget v3, v0, Lcom/mediatek/lbs/em/ViewGps;->mBaseSize:F

    const/high16 v4, 0x40400000

    div-float/2addr v3, v4

    const/high16 v4, 0x40400000

    mul-float/2addr v3, v4

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/mediatek/lbs/em/ViewGps;->mLine:Landroid/graphics/Paint;

    move-object/from16 v0, p1

    invoke-virtual {v0, v1, v2, v3, v4}, Landroid/graphics/Canvas;->drawCircle(FFFLandroid/graphics/Paint;)V

    move-object/from16 v0, p0

    iget v1, v0, Lcom/mediatek/lbs/em/ViewGps;->mBaseX:F

    move-object/from16 v0, p0

    iget v2, v0, Lcom/mediatek/lbs/em/ViewGps;->mBaseY:F

    move-object/from16 v0, p0

    iget v3, v0, Lcom/mediatek/lbs/em/ViewGps;->mBaseSize:F

    const/high16 v4, 0x40400000

    div-float/2addr v3, v4

    const/high16 v4, 0x40000000

    mul-float/2addr v3, v4

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/mediatek/lbs/em/ViewGps;->mLine:Landroid/graphics/Paint;

    move-object/from16 v0, p1

    invoke-virtual {v0, v1, v2, v3, v4}, Landroid/graphics/Canvas;->drawCircle(FFFLandroid/graphics/Paint;)V

    move-object/from16 v0, p0

    iget v1, v0, Lcom/mediatek/lbs/em/ViewGps;->mBaseX:F

    move-object/from16 v0, p0

    iget v2, v0, Lcom/mediatek/lbs/em/ViewGps;->mBaseY:F

    move-object/from16 v0, p0

    iget v3, v0, Lcom/mediatek/lbs/em/ViewGps;->mBaseSize:F

    const/high16 v4, 0x40400000

    div-float/2addr v3, v4

    const/high16 v4, 0x3f800000

    mul-float/2addr v3, v4

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/mediatek/lbs/em/ViewGps;->mLine:Landroid/graphics/Paint;

    move-object/from16 v0, p1

    invoke-virtual {v0, v1, v2, v3, v4}, Landroid/graphics/Canvas;->drawCircle(FFFLandroid/graphics/Paint;)V

    move-object/from16 v0, p0

    iget v2, v0, Lcom/mediatek/lbs/em/ViewGps;->mBaseX:F

    move-object/from16 v0, p0

    iget v1, v0, Lcom/mediatek/lbs/em/ViewGps;->mBaseY:F

    move-object/from16 v0, p0

    iget v3, v0, Lcom/mediatek/lbs/em/ViewGps;->mBaseSize:F

    sub-float v3, v1, v3

    move-object/from16 v0, p0

    iget v4, v0, Lcom/mediatek/lbs/em/ViewGps;->mBaseX:F

    move-object/from16 v0, p0

    iget v1, v0, Lcom/mediatek/lbs/em/ViewGps;->mBaseY:F

    move-object/from16 v0, p0

    iget v5, v0, Lcom/mediatek/lbs/em/ViewGps;->mBaseSize:F

    add-float/2addr v5, v1

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/mediatek/lbs/em/ViewGps;->mLine:Landroid/graphics/Paint;

    move-object/from16 v1, p1

    invoke-virtual/range {v1 .. v6}, Landroid/graphics/Canvas;->drawLine(FFFFLandroid/graphics/Paint;)V

    move-object/from16 v0, p0

    iget v1, v0, Lcom/mediatek/lbs/em/ViewGps;->mBaseX:F

    move-object/from16 v0, p0

    iget v2, v0, Lcom/mediatek/lbs/em/ViewGps;->mBaseSize:F

    sub-float v2, v1, v2

    move-object/from16 v0, p0

    iget v3, v0, Lcom/mediatek/lbs/em/ViewGps;->mBaseY:F

    move-object/from16 v0, p0

    iget v1, v0, Lcom/mediatek/lbs/em/ViewGps;->mBaseX:F

    move-object/from16 v0, p0

    iget v4, v0, Lcom/mediatek/lbs/em/ViewGps;->mBaseSize:F

    add-float/2addr v4, v1

    move-object/from16 v0, p0

    iget v5, v0, Lcom/mediatek/lbs/em/ViewGps;->mBaseY:F

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/mediatek/lbs/em/ViewGps;->mLine:Landroid/graphics/Paint;

    move-object/from16 v1, p1

    invoke-virtual/range {v1 .. v6}, Landroid/graphics/Canvas;->drawLine(FFFFLandroid/graphics/Paint;)V

    const/4 v2, 0x0

    invoke-virtual/range {p0 .. p0}, Lcom/mediatek/lbs/em/ViewGps;->getHeight()I

    move-result v1

    add-int/lit16 v1, v1, -0x8c

    int-to-float v3, v1

    invoke-virtual/range {p0 .. p0}, Lcom/mediatek/lbs/em/ViewGps;->getWidth()I

    move-result v1

    int-to-float v4, v1

    invoke-virtual/range {p0 .. p0}, Lcom/mediatek/lbs/em/ViewGps;->getHeight()I

    move-result v1

    add-int/lit16 v1, v1, -0x8c

    int-to-float v5, v1

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/mediatek/lbs/em/ViewGps;->mLine:Landroid/graphics/Paint;

    move-object/from16 v1, p1

    invoke-virtual/range {v1 .. v6}, Landroid/graphics/Canvas;->drawLine(FFFFLandroid/graphics/Paint;)V

    const/4 v2, 0x0

    invoke-virtual/range {p0 .. p0}, Lcom/mediatek/lbs/em/ViewGps;->getHeight()I

    move-result v1

    add-int/lit16 v1, v1, -0xb4

    int-to-float v3, v1

    invoke-virtual/range {p0 .. p0}, Lcom/mediatek/lbs/em/ViewGps;->getWidth()I

    move-result v1

    int-to-float v4, v1

    invoke-virtual/range {p0 .. p0}, Lcom/mediatek/lbs/em/ViewGps;->getHeight()I

    move-result v1

    add-int/lit16 v1, v1, -0xb4

    int-to-float v5, v1

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/mediatek/lbs/em/ViewGps;->mLine:Landroid/graphics/Paint;

    move-object/from16 v1, p1

    invoke-virtual/range {v1 .. v6}, Landroid/graphics/Canvas;->drawLine(FFFFLandroid/graphics/Paint;)V

    const/4 v2, 0x0

    invoke-virtual/range {p0 .. p0}, Lcom/mediatek/lbs/em/ViewGps;->getHeight()I

    move-result v1

    add-int/lit16 v1, v1, -0xdc

    int-to-float v3, v1

    invoke-virtual/range {p0 .. p0}, Lcom/mediatek/lbs/em/ViewGps;->getWidth()I

    move-result v1

    int-to-float v4, v1

    invoke-virtual/range {p0 .. p0}, Lcom/mediatek/lbs/em/ViewGps;->getHeight()I

    move-result v1

    add-int/lit16 v1, v1, -0xdc

    int-to-float v5, v1

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/mediatek/lbs/em/ViewGps;->mLine:Landroid/graphics/Paint;

    move-object/from16 v1, p1

    invoke-virtual/range {v1 .. v6}, Landroid/graphics/Canvas;->drawLine(FFFFLandroid/graphics/Paint;)V

    invoke-virtual/range {p0 .. p0}, Lcom/mediatek/lbs/em/ViewGps;->getWidth()I

    move-result v1

    div-int/lit8 v12, v1, 0x10

    const/4 v11, 0x0

    :goto_0
    move-object/from16 v0, p0

    iget v1, v0, Lcom/mediatek/lbs/em/ViewGps;->mSatInView:I

    if-ge v11, v1, :cond_6

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/mediatek/lbs/em/ViewGps;->mPnrs:[I

    aget v1, v1, v11

    invoke-static {v1}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v13

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/mediatek/lbs/em/ViewGps;->mSnrs:[F

    aget v1, v1, v11

    float-to-int v9, v1

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/mediatek/lbs/em/ViewGps;->mEle:[F

    aget v10, v1, v11

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/mediatek/lbs/em/ViewGps;->mAzi:[F

    aget v8, v1, v11

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/mediatek/lbs/em/ViewGps;->mUsed:[Z

    aget-boolean v7, v1, v11

    const/16 v1, 0x10

    if-ge v11, v1, :cond_2

    mul-int v1, v12, v11

    int-to-float v1, v1

    invoke-virtual/range {p0 .. p0}, Lcom/mediatek/lbs/em/ViewGps;->getHeight()I

    move-result v2

    add-int/lit8 v2, v2, -0x7f

    int-to-float v2, v2

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/mediatek/lbs/em/ViewGps;->mLine:Landroid/graphics/Paint;

    move-object/from16 v0, p1

    invoke-virtual {v0, v13, v1, v2, v3}, Landroid/graphics/Canvas;->drawText(Ljava/lang/String;FFLandroid/graphics/Paint;)V

    const/4 v1, 0x1

    if-ne v7, v1, :cond_1

    mul-int v1, v12, v11

    int-to-float v2, v1

    invoke-virtual/range {p0 .. p0}, Lcom/mediatek/lbs/em/ViewGps;->getHeight()I

    move-result v1

    add-int/lit16 v1, v1, -0x8c

    mul-int/lit8 v3, v9, 0x2

    sub-int/2addr v1, v3

    int-to-float v3, v1

    add-int/lit8 v1, v11, 0x1

    mul-int/2addr v1, v12

    int-to-float v4, v1

    invoke-virtual/range {p0 .. p0}, Lcom/mediatek/lbs/em/ViewGps;->getHeight()I

    move-result v1

    add-int/lit16 v1, v1, -0x8c

    int-to-float v5, v1

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/mediatek/lbs/em/ViewGps;->mGreenCircle:Landroid/graphics/Paint;

    move-object/from16 v1, p1

    invoke-virtual/range {v1 .. v6}, Landroid/graphics/Canvas;->drawRect(FFFFLandroid/graphics/Paint;)V

    :goto_1
    invoke-static {v9}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v1

    mul-int v2, v12, v11

    int-to-float v2, v2

    invoke-virtual/range {p0 .. p0}, Lcom/mediatek/lbs/em/ViewGps;->getHeight()I

    move-result v3

    add-int/lit16 v3, v3, -0x96

    int-to-float v3, v3

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/mediatek/lbs/em/ViewGps;->mLine:Landroid/graphics/Paint;

    move-object/from16 v0, p1

    invoke-virtual {v0, v1, v2, v3, v4}, Landroid/graphics/Canvas;->drawText(Ljava/lang/String;FFLandroid/graphics/Paint;)V

    mul-int v1, v12, v11

    int-to-float v2, v1

    invoke-virtual/range {p0 .. p0}, Lcom/mediatek/lbs/em/ViewGps;->getHeight()I

    move-result v1

    add-int/lit16 v1, v1, -0x8c

    mul-int/lit8 v3, v9, 0x2

    sub-int/2addr v1, v3

    int-to-float v3, v1

    add-int/lit8 v1, v11, 0x1

    mul-int/2addr v1, v12

    int-to-float v4, v1

    invoke-virtual/range {p0 .. p0}, Lcom/mediatek/lbs/em/ViewGps;->getHeight()I

    move-result v1

    add-int/lit16 v1, v1, -0x8c

    int-to-float v5, v1

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/mediatek/lbs/em/ViewGps;->mLine:Landroid/graphics/Paint;

    move-object/from16 v1, p1

    invoke-virtual/range {v1 .. v6}, Landroid/graphics/Canvas;->drawRect(FFFFLandroid/graphics/Paint;)V

    :goto_2
    const/high16 v1, 0x42b40000

    cmpl-float v1, v10, v1

    if-lez v1, :cond_4

    const/high16 v10, 0x42b40000

    :cond_0
    :goto_3
    const/high16 v1, 0x3f800000

    const/high16 v2, 0x42b40000

    div-float v2, v10, v2

    sub-float/2addr v1, v2

    move-object/from16 v0, p0

    iget v2, v0, Lcom/mediatek/lbs/em/ViewGps;->mBaseSize:F

    mul-float v10, v1, v2

    float-to-double v1, v8

    const-wide v3, 0x400921fb54442d18L

    mul-double/2addr v1, v3

    const-wide v3, 0x4066800000000000L

    div-double/2addr v1, v3

    invoke-static {v1, v2}, Ljava/lang/Math;->sin(D)D

    move-result-wide v1

    double-to-float v1, v1

    mul-float v14, v1, v10

    float-to-double v1, v8

    const-wide v3, 0x400921fb54442d18L

    mul-double/2addr v1, v3

    const-wide v3, 0x4066800000000000L

    div-double/2addr v1, v3

    invoke-static {v1, v2}, Ljava/lang/Math;->cos(D)D

    move-result-wide v1

    double-to-float v1, v1

    neg-float v1, v1

    mul-float v15, v1, v10

    const/4 v1, 0x1

    if-ne v7, v1, :cond_5

    move-object/from16 v0, p0

    iget v1, v0, Lcom/mediatek/lbs/em/ViewGps;->mBaseX:F

    add-float/2addr v1, v14

    move-object/from16 v0, p0

    iget v2, v0, Lcom/mediatek/lbs/em/ViewGps;->mBaseY:F

    add-float/2addr v2, v15

    const/high16 v3, 0x41700000

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/mediatek/lbs/em/ViewGps;->mGreenCircle:Landroid/graphics/Paint;

    move-object/from16 v0, p1

    invoke-virtual {v0, v1, v2, v3, v4}, Landroid/graphics/Canvas;->drawCircle(FFFLandroid/graphics/Paint;)V

    :goto_4
    move-object/from16 v0, p0

    iget v1, v0, Lcom/mediatek/lbs/em/ViewGps;->mBaseX:F

    add-float/2addr v1, v14

    move-object/from16 v0, p0

    iget v2, v0, Lcom/mediatek/lbs/em/ViewGps;->mBaseY:F

    add-float/2addr v2, v15

    const/high16 v3, 0x40f00000

    add-float/2addr v2, v3

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/mediatek/lbs/em/ViewGps;->mWhiteText:Landroid/graphics/Paint;

    move-object/from16 v0, p1

    invoke-virtual {v0, v13, v1, v2, v3}, Landroid/graphics/Canvas;->drawText(Ljava/lang/String;FFLandroid/graphics/Paint;)V

    add-int/lit8 v11, v11, 0x1

    goto/16 :goto_0

    :cond_1
    mul-int v1, v12, v11

    int-to-float v2, v1

    invoke-virtual/range {p0 .. p0}, Lcom/mediatek/lbs/em/ViewGps;->getHeight()I

    move-result v1

    add-int/lit16 v1, v1, -0x8c

    mul-int/lit8 v3, v9, 0x2

    sub-int/2addr v1, v3

    int-to-float v3, v1

    add-int/lit8 v1, v11, 0x1

    mul-int/2addr v1, v12

    int-to-float v4, v1

    invoke-virtual/range {p0 .. p0}, Lcom/mediatek/lbs/em/ViewGps;->getHeight()I

    move-result v1

    add-int/lit16 v1, v1, -0x8c

    int-to-float v5, v1

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/mediatek/lbs/em/ViewGps;->mRedCircle:Landroid/graphics/Paint;

    move-object/from16 v1, p1

    invoke-virtual/range {v1 .. v6}, Landroid/graphics/Canvas;->drawRect(FFFFLandroid/graphics/Paint;)V

    goto/16 :goto_1

    :cond_2
    add-int/lit8 v1, v11, -0x10

    mul-int/2addr v1, v12

    int-to-float v1, v1

    invoke-virtual/range {p0 .. p0}, Lcom/mediatek/lbs/em/ViewGps;->getHeight()I

    move-result v2

    add-int/lit8 v2, v2, -0x7

    int-to-float v2, v2

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/mediatek/lbs/em/ViewGps;->mLine:Landroid/graphics/Paint;

    move-object/from16 v0, p1

    invoke-virtual {v0, v13, v1, v2, v3}, Landroid/graphics/Canvas;->drawText(Ljava/lang/String;FFLandroid/graphics/Paint;)V

    const/4 v1, 0x1

    if-ne v7, v1, :cond_3

    add-int/lit8 v1, v11, -0x10

    mul-int/2addr v1, v12

    int-to-float v2, v1

    invoke-virtual/range {p0 .. p0}, Lcom/mediatek/lbs/em/ViewGps;->getHeight()I

    move-result v1

    add-int/lit8 v1, v1, -0x14

    mul-int/lit8 v3, v9, 0x2

    sub-int/2addr v1, v3

    int-to-float v3, v1

    add-int/lit8 v1, v11, -0x10

    add-int/lit8 v1, v1, 0x1

    mul-int/2addr v1, v12

    int-to-float v4, v1

    invoke-virtual/range {p0 .. p0}, Lcom/mediatek/lbs/em/ViewGps;->getHeight()I

    move-result v1

    add-int/lit8 v1, v1, -0x14

    int-to-float v5, v1

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/mediatek/lbs/em/ViewGps;->mGreenCircle:Landroid/graphics/Paint;

    move-object/from16 v1, p1

    invoke-virtual/range {v1 .. v6}, Landroid/graphics/Canvas;->drawRect(FFFFLandroid/graphics/Paint;)V

    :goto_5
    invoke-static {v9}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v1

    add-int/lit8 v2, v11, -0x10

    mul-int/2addr v2, v12

    int-to-float v2, v2

    invoke-virtual/range {p0 .. p0}, Lcom/mediatek/lbs/em/ViewGps;->getHeight()I

    move-result v3

    add-int/lit8 v3, v3, -0x1e

    int-to-float v3, v3

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/mediatek/lbs/em/ViewGps;->mLine:Landroid/graphics/Paint;

    move-object/from16 v0, p1

    invoke-virtual {v0, v1, v2, v3, v4}, Landroid/graphics/Canvas;->drawText(Ljava/lang/String;FFLandroid/graphics/Paint;)V

    add-int/lit8 v1, v11, -0x10

    mul-int/2addr v1, v12

    int-to-float v2, v1

    invoke-virtual/range {p0 .. p0}, Lcom/mediatek/lbs/em/ViewGps;->getHeight()I

    move-result v1

    add-int/lit8 v1, v1, -0x14

    mul-int/lit8 v3, v9, 0x2

    sub-int/2addr v1, v3

    int-to-float v3, v1

    add-int/lit8 v1, v11, -0x10

    add-int/lit8 v1, v1, 0x1

    mul-int/2addr v1, v12

    int-to-float v4, v1

    invoke-virtual/range {p0 .. p0}, Lcom/mediatek/lbs/em/ViewGps;->getHeight()I

    move-result v1

    add-int/lit8 v1, v1, -0x14

    int-to-float v5, v1

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/mediatek/lbs/em/ViewGps;->mLine:Landroid/graphics/Paint;

    move-object/from16 v1, p1

    invoke-virtual/range {v1 .. v6}, Landroid/graphics/Canvas;->drawRect(FFFFLandroid/graphics/Paint;)V

    goto/16 :goto_2

    :cond_3
    add-int/lit8 v1, v11, -0x10

    mul-int/2addr v1, v12

    int-to-float v2, v1

    invoke-virtual/range {p0 .. p0}, Lcom/mediatek/lbs/em/ViewGps;->getHeight()I

    move-result v1

    add-int/lit8 v1, v1, -0x14

    mul-int/lit8 v3, v9, 0x2

    sub-int/2addr v1, v3

    int-to-float v3, v1

    add-int/lit8 v1, v11, -0x10

    add-int/lit8 v1, v1, 0x1

    mul-int/2addr v1, v12

    int-to-float v4, v1

    invoke-virtual/range {p0 .. p0}, Lcom/mediatek/lbs/em/ViewGps;->getHeight()I

    move-result v1

    add-int/lit8 v1, v1, -0x14

    int-to-float v5, v1

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/mediatek/lbs/em/ViewGps;->mRedCircle:Landroid/graphics/Paint;

    move-object/from16 v1, p1

    invoke-virtual/range {v1 .. v6}, Landroid/graphics/Canvas;->drawRect(FFFFLandroid/graphics/Paint;)V

    goto :goto_5

    :cond_4
    const/4 v1, 0x0

    cmpg-float v1, v10, v1

    if-gez v1, :cond_0

    const/4 v10, 0x0

    goto/16 :goto_3

    :cond_5
    move-object/from16 v0, p0

    iget v1, v0, Lcom/mediatek/lbs/em/ViewGps;->mBaseX:F

    add-float/2addr v1, v14

    move-object/from16 v0, p0

    iget v2, v0, Lcom/mediatek/lbs/em/ViewGps;->mBaseY:F

    add-float/2addr v2, v15

    const/high16 v3, 0x41700000

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/mediatek/lbs/em/ViewGps;->mRedCircle:Landroid/graphics/Paint;

    move-object/from16 v0, p1

    invoke-virtual {v0, v1, v2, v3, v4}, Landroid/graphics/Canvas;->drawCircle(FFFLandroid/graphics/Paint;)V

    goto/16 :goto_4

    :cond_6
    return-void
.end method

.method public resetGpsView()V
    .locals 1

    const/4 v0, 0x0

    iput v0, p0, Lcom/mediatek/lbs/em/ViewGps;->mSatInView:I

    invoke-virtual {p0}, Lcom/mediatek/lbs/em/ViewGps;->invalidate()V

    return-void
.end method

.method public setGpsStatus(Landroid/location/GpsStatus;)V
    .locals 6
    .param p1    # Landroid/location/GpsStatus;

    const/4 v3, 0x0

    invoke-virtual {p1}, Landroid/location/GpsStatus;->getSatellites()Ljava/lang/Iterable;

    move-result-object v0

    invoke-interface {v0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/location/GpsSatellite;

    const/16 v4, 0x20

    if-lt v3, v4, :cond_1

    const-string v4, "ERR: the number of satellite is over 32"

    invoke-direct {p0, v4}, Lcom/mediatek/lbs/em/ViewGps;->log(Ljava/lang/String;)V

    :cond_0
    iput v3, p0, Lcom/mediatek/lbs/em/ViewGps;->mSatInView:I

    invoke-virtual {p0}, Lcom/mediatek/lbs/em/ViewGps;->invalidate()V

    return-void

    :cond_1
    iget-object v4, p0, Lcom/mediatek/lbs/em/ViewGps;->mPnrs:[I

    invoke-virtual {v2}, Landroid/location/GpsSatellite;->getPrn()I

    move-result v5

    aput v5, v4, v3

    iget-object v4, p0, Lcom/mediatek/lbs/em/ViewGps;->mSnrs:[F

    invoke-virtual {v2}, Landroid/location/GpsSatellite;->getSnr()F

    move-result v5

    aput v5, v4, v3

    iget-object v4, p0, Lcom/mediatek/lbs/em/ViewGps;->mEle:[F

    invoke-virtual {v2}, Landroid/location/GpsSatellite;->getElevation()F

    move-result v5

    aput v5, v4, v3

    iget-object v4, p0, Lcom/mediatek/lbs/em/ViewGps;->mAzi:[F

    invoke-virtual {v2}, Landroid/location/GpsSatellite;->getAzimuth()F

    move-result v5

    aput v5, v4, v3

    iget-object v4, p0, Lcom/mediatek/lbs/em/ViewGps;->mUsed:[Z

    invoke-virtual {v2}, Landroid/location/GpsSatellite;->usedInFix()Z

    move-result v5

    aput-boolean v5, v4, v3

    add-int/lit8 v3, v3, 0x1

    goto :goto_0
.end method
