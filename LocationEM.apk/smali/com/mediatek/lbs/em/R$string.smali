.class public final Lcom/mediatek/lbs/em/R$string;
.super Ljava/lang/Object;
.source "R.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/mediatek/lbs/em/R;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "string"
.end annotation


# static fields
.field public static final Abort:I = 0x7f040073

.field public static final Area:I = 0x7f040076

.field public static final AreaMaxNum:I = 0x7f04007f

.field public static final AreaMinInter:I = 0x7f04007e

.field public static final AreaSettings:I = 0x7f04007b

.field public static final AreaType:I = 0x7f04007d

.field public static final GeoLat:I = 0x7f040082

.field public static final GeoLong:I = 0x7f040083

.field public static final GeoRadius:I = 0x7f040081

.field public static final GeoRadiusSign:I = 0x7f040086

.field public static final Geographic:I = 0x7f040080

.field public static final MSISDN:I = 0x7f040070

.field public static final North:I = 0x7f040084

.field public static final Periodic:I = 0x7f040075

.field public static final PeriodicInter:I = 0x7f040079

.field public static final PeriodicNFix:I = 0x7f040078

.field public static final PeriodicSTime:I = 0x7f04007a

.field public static final PeriodicSettings:I = 0x7f04007c

.field public static final PosMethod:I = 0x7f040077

.field public static final SILR:I = 0x7f04006f

.field public static final SUPLTwoDuZero:I = 0x7f04008a

.field public static final SUPLTwoDuZeroEnable:I = 0x7f04008b

.field public static final South:I = 0x7f040085

.field public static final Start:I = 0x7f040072

.field public static final StartTime:I = 0x7f040088

.field public static final StopTime:I = 0x7f040089

.field public static final ThirdEnable:I = 0x7f040087

.field public static final Trigger_Settings:I = 0x7f040071

.field public static final Trigger_Type:I = 0x7f040074

.field public static final accuracy_unit:I = 0x7f04005d

.field public static final agps_mode:I = 0x7f040034

.field public static final agps_onoff:I = 0x7f040032

.field public static final agps_test:I = 0x7f04002d

.field public static final allow_em_notification:I = 0x7f04003b

.field public static final allow_ni:I = 0x7f04003a

.field public static final allow_roaming:I = 0x7f04003c

.field public static final apn:I = 0x7f0400b7

.field public static final app_name:I = 0x7f040000

.field public static final apply_last_fix:I = 0x7f040005

.field public static final assistance_data:I = 0x7f040048

.field public static final auto_test_off:I = 0x7f04001f

.field public static final auto_test_on:I = 0x7f04001e

.field public static final both:I = 0x7f040059

.field public static final cancel:I = 0x7f040002

.field public static final cdma_addr:I = 0x7f040091

.field public static final cdma_force:I = 0x7f04008d

.field public static final cdma_port:I = 0x7f040092

.field public static final cdma_prefer:I = 0x7f04008c

.field public static final cdma_settings:I = 0x7f04008f

.field public static final cdma_template:I = 0x7f040090

.field public static final cell_info:I = 0x7f0400bf

.field public static final certificate_verification:I = 0x7f040065

.field public static final cold:I = 0x7f040012

.field public static final connect:I = 0x7f0400ba

.field public static final connectionTest:I = 0x7f0400b9

.field public static final connection_off:I = 0x7f0400bd

.field public static final connection_on:I = 0x7f0400be

.field public static final control_plane:I = 0x7f040044

.field public static final cp_number:I = 0x7f04004a

.field public static final cp_settings:I = 0x7f040046

.field public static final cp_up_switch:I = 0x7f040043

.field public static final delay:I = 0x7f040063

.field public static final delay_before_deleting_data:I = 0x7f040017

.field public static final delay_before_starting_epo:I = 0x7f0400aa

.field public static final delay_before_starting_gps:I = 0x7f040016

.field public static final delay_before_stopping_gps:I = 0x7f040019

.field public static final delete:I = 0x7f04000c

.field public static final delete_aiding_data_first:I = 0x7f04001a

.field public static final dsd:I = 0x7f0400b4

.field public static final ecid_enable:I = 0x7f04003e

.field public static final edit:I = 0x7f040054

.field public static final enable_agps:I = 0x7f040033

.field public static final enable_iot:I = 0x7f040039

.field public static final enable_ni_timer:I = 0x7f040040

.field public static final epo_auto_download:I = 0x7f0400a0

.field public static final epo_disabled:I = 0x7f04009f

.field public static final epo_download_timeout:I = 0x7f0400ab

.field public static final epo_downloading:I = 0x7f0400a1

.field public static final epo_enable:I = 0x7f04009e

.field public static final epo_file_info:I = 0x7f0400a7

.field public static final epo_functionality:I = 0x7f04009d

.field public static final epo_idle:I = 0x7f0400a2

.field public static final epo_log:I = 0x7f0400a6

.field public static final epo_num_of_loop:I = 0x7f0400a9

.field public static final epo_period_setting:I = 0x7f0400a5

.field public static final epo_stress:I = 0x7f0400ac

.field public static final epo_stress_off:I = 0x7f0400ae

.field public static final epo_stress_on:I = 0x7f0400ad

.field public static final epo_stress_test_settings:I = 0x7f0400a8

.field public static final epo_update:I = 0x7f0400a4

.field public static final external_addr:I = 0x7f040049

.field public static final feature_enabler:I = 0x7f040038

.field public static final file_location_em:I = 0x7f0400b1

.field public static final file_sdcard:I = 0x7f0400b0

.field public static final full:I = 0x7f040013

.field public static final gps:I = 0x7f0400b5

.field public static final gps_off:I = 0x7f040009

.field public static final gps_on:I = 0x7f040008

.field public static final gps_reset:I = 0x7f04002c

.field public static final horizontal_accuracy:I = 0x7f040060

.field public static final hot:I = 0x7f040010

.field public static final imsi:I = 0x7f04005c

.field public static final ipv4:I = 0x7f04005b

.field public static final k_value:I = 0x7f04005e

.field public static final lab_performance:I = 0x7f04006c

.field public static final latitude:I = 0x7f040003

.field public static final load_profile:I = 0x7f040030

.field public static final locale:I = 0x7f040006

.field public static final location_age:I = 0x7f040062

.field public static final location_estimate:I = 0x7f040047

.field public static final locationem_ver:I = 0x7f04002b

.field public static final log_nmea:I = 0x7f04000e

.field public static final log_supl_to_file:I = 0x7f04003d

.field public static final log_to_sdcard:I = 0x7f04000f

.field public static final longitude:I = 0x7f040004

.field public static final mcp_addr:I = 0x7f040094

.field public static final mcp_enable:I = 0x7f040093

.field public static final mcp_port:I = 0x7f040095

.field public static final menu:I = 0x7f0400af

.field public static final meter:I = 0x7f04005f

.field public static final minute:I = 0x7f0400a3

.field public static final mlc_number:I = 0x7f04004b

.field public static final mnl_controller:I = 0x7f04001c

.field public static final mnl_test:I = 0x7f04002a

.field public static final mnl_verifier:I = 0x7f04001b

.field public static final molr_position_method:I = 0x7f040069

.field public static final msa:I = 0x7f040037

.field public static final msb:I = 0x7f040036

.field public static final msic_goto:I = 0x7f0400b2

.field public static final ni_dialog_custom:I = 0x7f04003f

.field public static final ni_dialog_test:I = 0x7f04002f

.field public static final ni_tests:I = 0x7f04006e

.field public static final notification_timeout:I = 0x7f040041

.field public static final num_0:I = 0x7f040068

.field public static final num_10:I = 0x7f040066

.field public static final num_16:I = 0x7f040067

.field public static final num_of_fix:I = 0x7f040014

.field public static final ok:I = 0x7f040001

.field public static final pde_addr_valid:I = 0x7f040096

.field public static final pde_ip4_addr:I = 0x7f040099

.field public static final pde_ip6_addr:I = 0x7f04009a

.field public static final pde_ip_type:I = 0x7f040097

.field public static final pde_port:I = 0x7f04009b

.field public static final pde_url:I = 0x7f04009c

.field public static final pde_url_valid:I = 0x7f040098

.field public static final ref_latlng:I = 0x7f04000d

.field public static final reset_agpsd:I = 0x7f04002e

.field public static final reset_to_default:I = 0x7f040064

.field public static final rrc:I = 0x7f040058

.field public static final rrlp:I = 0x7f040057

.field public static final select:I = 0x7f040007

.field public static final server_addr:I = 0x7f0400bb

.field public static final server_port:I = 0x7f0400bc

.field public static final session_timeout:I = 0x7f040018

.field public static final set_id:I = 0x7f04005a

.field public static final set_profile:I = 0x7f040031

.field public static final sim1_preferred:I = 0x7f04004c

.field public static final sim2_preferred:I = 0x7f04004d

.field public static final sim3_preferred:I = 0x7f04004e

.field public static final sim4_preferred:I = 0x7f04004f

.field public static final sim_selection:I = 0x7f04006a

.field public static final slp_addr:I = 0x7f040052

.field public static final slp_port:I = 0x7f040053

.field public static final slp_profile:I = 0x7f040051

.field public static final slp_template:I = 0x7f04006b

.field public static final slp_test:I = 0x7f04006d

.field public static final standalone:I = 0x7f040035

.field public static final start_cp_auto_test:I = 0x7f04001d

.field public static final stress_off:I = 0x7f04000b

.field public static final stress_on:I = 0x7f04000a

.field public static final stress_test_settings:I = 0x7f040015

.field public static final test:I = 0x7f040020

.field public static final test1:I = 0x7f040021

.field public static final test2:I = 0x7f040022

.field public static final test3:I = 0x7f040023

.field public static final test4:I = 0x7f040024

.field public static final test5:I = 0x7f040025

.field public static final test6:I = 0x7f040026

.field public static final test7:I = 0x7f040027

.field public static final test8:I = 0x7f040028

.field public static final test9:I = 0x7f040029

.field public static final time:I = 0x7f0400b6

.field public static final tls_enable:I = 0x7f040055

.field public static final type:I = 0x7f040056

.field public static final up_settings:I = 0x7f040050

.field public static final user_plane:I = 0x7f040045

.field public static final verification_timeout:I = 0x7f040042

.field public static final vertical_accuracy:I = 0x7f040061

.field public static final warm:I = 0x7f040011

.field public static final wcdma_prefer:I = 0x7f04008e

.field public static final wifi:I = 0x7f0400b8

.field public static final ygps:I = 0x7f0400b3


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
