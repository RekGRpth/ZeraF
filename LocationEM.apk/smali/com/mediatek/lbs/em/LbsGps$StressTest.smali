.class Lcom/mediatek/lbs/em/LbsGps$StressTest;
.super Ljava/lang/Thread;
.source "LbsGps.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/mediatek/lbs/em/LbsGps;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = "StressTest"
.end annotation


# instance fields
.field private delay1:I

.field private delay2:I

.field private delay3:I

.field private delay4:I

.field private enable:Z

.field private numOfLoop:I

.field final synthetic this$0:Lcom/mediatek/lbs/em/LbsGps;


# direct methods
.method public constructor <init>(Lcom/mediatek/lbs/em/LbsGps;)V
    .locals 7

    const/16 v2, 0x64

    const/16 v3, 0x14

    const/4 v4, 0x1

    const/16 v5, 0x258

    const/4 v6, 0x0

    move-object v0, p0

    move-object v1, p1

    invoke-direct/range {v0 .. v6}, Lcom/mediatek/lbs/em/LbsGps$StressTest;-><init>(Lcom/mediatek/lbs/em/LbsGps;IIIII)V

    return-void
.end method

.method public constructor <init>(Lcom/mediatek/lbs/em/LbsGps;IIIII)V
    .locals 1
    .param p2    # I
    .param p3    # I
    .param p4    # I
    .param p5    # I
    .param p6    # I

    const/4 v0, 0x1

    iput-object p1, p0, Lcom/mediatek/lbs/em/LbsGps$StressTest;->this$0:Lcom/mediatek/lbs/em/LbsGps;

    invoke-direct {p0}, Ljava/lang/Thread;-><init>()V

    iput-boolean v0, p0, Lcom/mediatek/lbs/em/LbsGps$StressTest;->enable:Z

    iput p2, p0, Lcom/mediatek/lbs/em/LbsGps$StressTest;->numOfLoop:I

    iput p3, p0, Lcom/mediatek/lbs/em/LbsGps$StressTest;->delay1:I

    iput p4, p0, Lcom/mediatek/lbs/em/LbsGps$StressTest;->delay2:I

    iput p5, p0, Lcom/mediatek/lbs/em/LbsGps$StressTest;->delay3:I

    iput p6, p0, Lcom/mediatek/lbs/em/LbsGps$StressTest;->delay4:I

    iput-boolean v0, p0, Lcom/mediatek/lbs/em/LbsGps$StressTest;->enable:Z

    return-void
.end method

.method private oneSession()V
    .locals 6

    const/4 v5, 0x5

    const/4 v4, 0x3

    const/4 v3, 0x0

    iget-object v1, p0, Lcom/mediatek/lbs/em/LbsGps$StressTest;->this$0:Lcom/mediatek/lbs/em/LbsGps;

    # getter for: Lcom/mediatek/lbs/em/LbsGps;->mCheckBoxSwitchDelet2First:Landroid/widget/CheckBox;
    invoke-static {v1}, Lcom/mediatek/lbs/em/LbsGps;->access$3600(Lcom/mediatek/lbs/em/LbsGps;)Landroid/widget/CheckBox;

    move-result-object v1

    invoke-virtual {v1}, Landroid/widget/CheckBox;->isChecked()Z

    move-result v1

    if-nez v1, :cond_1

    :try_start_0
    iget v1, p0, Lcom/mediatek/lbs/em/LbsGps$StressTest;->delay1:I

    mul-int/lit16 v1, v1, 0x3e8

    int-to-long v1, v1

    invoke-static {v1, v2}, Ljava/lang/Thread;->sleep(J)V
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0

    iget-object v1, p0, Lcom/mediatek/lbs/em/LbsGps$StressTest;->this$0:Lcom/mediatek/lbs/em/LbsGps;

    # invokes: Lcom/mediatek/lbs/em/LbsGps;->sendMessage(II)V
    invoke-static {v1, v4, v3}, Lcom/mediatek/lbs/em/LbsGps;->access$3500(Lcom/mediatek/lbs/em/LbsGps;II)V

    :try_start_1
    iget v1, p0, Lcom/mediatek/lbs/em/LbsGps$StressTest;->delay2:I

    mul-int/lit16 v1, v1, 0x3e8

    int-to-long v1, v1

    invoke-static {v1, v2}, Ljava/lang/Thread;->sleep(J)V
    :try_end_1
    .catch Ljava/lang/InterruptedException; {:try_start_1 .. :try_end_1} :catch_1

    iget-object v1, p0, Lcom/mediatek/lbs/em/LbsGps$StressTest;->this$0:Lcom/mediatek/lbs/em/LbsGps;

    # invokes: Lcom/mediatek/lbs/em/LbsGps;->sendMessage(II)V
    invoke-static {v1, v5, v3}, Lcom/mediatek/lbs/em/LbsGps;->access$3500(Lcom/mediatek/lbs/em/LbsGps;II)V

    :goto_0
    :try_start_2
    iget v1, p0, Lcom/mediatek/lbs/em/LbsGps$StressTest;->delay3:I

    mul-int/lit16 v1, v1, 0x3e8

    int-to-long v1, v1

    invoke-static {v1, v2}, Ljava/lang/Thread;->sleep(J)V
    :try_end_2
    .catch Ljava/lang/InterruptedException; {:try_start_2 .. :try_end_2} :catch_4

    :cond_0
    :goto_1
    return-void

    :catch_0
    move-exception v0

    goto :goto_1

    :catch_1
    move-exception v0

    goto :goto_1

    :cond_1
    :try_start_3
    iget v1, p0, Lcom/mediatek/lbs/em/LbsGps$StressTest;->delay2:I

    mul-int/lit16 v1, v1, 0x3e8

    int-to-long v1, v1

    invoke-static {v1, v2}, Ljava/lang/Thread;->sleep(J)V
    :try_end_3
    .catch Ljava/lang/InterruptedException; {:try_start_3 .. :try_end_3} :catch_2

    iget-object v1, p0, Lcom/mediatek/lbs/em/LbsGps$StressTest;->this$0:Lcom/mediatek/lbs/em/LbsGps;

    # invokes: Lcom/mediatek/lbs/em/LbsGps;->sendMessage(II)V
    invoke-static {v1, v5, v3}, Lcom/mediatek/lbs/em/LbsGps;->access$3500(Lcom/mediatek/lbs/em/LbsGps;II)V

    :try_start_4
    iget v1, p0, Lcom/mediatek/lbs/em/LbsGps$StressTest;->delay1:I

    mul-int/lit16 v1, v1, 0x3e8

    int-to-long v1, v1

    invoke-static {v1, v2}, Ljava/lang/Thread;->sleep(J)V
    :try_end_4
    .catch Ljava/lang/InterruptedException; {:try_start_4 .. :try_end_4} :catch_3

    iget-object v1, p0, Lcom/mediatek/lbs/em/LbsGps$StressTest;->this$0:Lcom/mediatek/lbs/em/LbsGps;

    # invokes: Lcom/mediatek/lbs/em/LbsGps;->sendMessage(II)V
    invoke-static {v1, v4, v3}, Lcom/mediatek/lbs/em/LbsGps;->access$3500(Lcom/mediatek/lbs/em/LbsGps;II)V

    goto :goto_0

    :catch_2
    move-exception v0

    goto :goto_1

    :catch_3
    move-exception v0

    goto :goto_1

    :catch_4
    move-exception v0

    iget-boolean v1, p0, Lcom/mediatek/lbs/em/LbsGps$StressTest;->enable:Z

    if-eqz v1, :cond_0

    :try_start_5
    iget v1, p0, Lcom/mediatek/lbs/em/LbsGps$StressTest;->delay4:I

    mul-int/lit16 v1, v1, 0x3e8

    int-to-long v1, v1

    invoke-static {v1, v2}, Ljava/lang/Thread;->sleep(J)V
    :try_end_5
    .catch Ljava/lang/InterruptedException; {:try_start_5 .. :try_end_5} :catch_5

    goto :goto_1

    :catch_5
    move-exception v0

    goto :goto_1
.end method


# virtual methods
.method public gotFixNotify()V
    .locals 0

    invoke-virtual {p0}, Lcom/mediatek/lbs/em/LbsGps$StressTest;->interrupt()V

    return-void
.end method

.method public run()V
    .locals 5

    const/4 v4, 0x0

    iget-object v1, p0, Lcom/mediatek/lbs/em/LbsGps$StressTest;->this$0:Lcom/mediatek/lbs/em/LbsGps;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "numOfLoop="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget v3, p0, Lcom/mediatek/lbs/em/LbsGps$StressTest;->numOfLoop:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " delay1="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget v3, p0, Lcom/mediatek/lbs/em/LbsGps$StressTest;->delay1:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " delay2="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget v3, p0, Lcom/mediatek/lbs/em/LbsGps$StressTest;->delay2:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " delay3="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget v3, p0, Lcom/mediatek/lbs/em/LbsGps$StressTest;->delay3:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " delay4="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget v3, p0, Lcom/mediatek/lbs/em/LbsGps$StressTest;->delay4:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    # invokes: Lcom/mediatek/lbs/em/LbsGps;->log(Ljava/lang/String;)V
    invoke-static {v1, v2}, Lcom/mediatek/lbs/em/LbsGps;->access$2200(Lcom/mediatek/lbs/em/LbsGps;Ljava/lang/String;)V

    const/4 v0, 0x0

    :goto_0
    iget v1, p0, Lcom/mediatek/lbs/em/LbsGps$StressTest;->numOfLoop:I

    if-ge v0, v1, :cond_0

    iget-object v1, p0, Lcom/mediatek/lbs/em/LbsGps$StressTest;->this$0:Lcom/mediatek/lbs/em/LbsGps;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "=== stress test loop="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "==="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    # invokes: Lcom/mediatek/lbs/em/LbsGps;->log(Ljava/lang/String;)V
    invoke-static {v1, v2}, Lcom/mediatek/lbs/em/LbsGps;->access$2200(Lcom/mediatek/lbs/em/LbsGps;Ljava/lang/String;)V

    iget-object v1, p0, Lcom/mediatek/lbs/em/LbsGps$StressTest;->this$0:Lcom/mediatek/lbs/em/LbsGps;

    const/4 v2, 0x2

    add-int/lit8 v3, v0, 0x1

    # invokes: Lcom/mediatek/lbs/em/LbsGps;->sendMessage(II)V
    invoke-static {v1, v2, v3}, Lcom/mediatek/lbs/em/LbsGps;->access$3500(Lcom/mediatek/lbs/em/LbsGps;II)V

    invoke-direct {p0}, Lcom/mediatek/lbs/em/LbsGps$StressTest;->oneSession()V

    iget-object v1, p0, Lcom/mediatek/lbs/em/LbsGps$StressTest;->this$0:Lcom/mediatek/lbs/em/LbsGps;

    const/4 v2, 0x4

    # invokes: Lcom/mediatek/lbs/em/LbsGps;->sendMessage(II)V
    invoke-static {v1, v2, v4}, Lcom/mediatek/lbs/em/LbsGps;->access$3500(Lcom/mediatek/lbs/em/LbsGps;II)V

    iget-boolean v1, p0, Lcom/mediatek/lbs/em/LbsGps$StressTest;->enable:Z

    if-nez v1, :cond_1

    :cond_0
    iget-object v1, p0, Lcom/mediatek/lbs/em/LbsGps$StressTest;->this$0:Lcom/mediatek/lbs/em/LbsGps;

    const-string v2, "end of stress test"

    # invokes: Lcom/mediatek/lbs/em/LbsGps;->log(Ljava/lang/String;)V
    invoke-static {v1, v2}, Lcom/mediatek/lbs/em/LbsGps;->access$2200(Lcom/mediatek/lbs/em/LbsGps;Ljava/lang/String;)V

    iget-object v1, p0, Lcom/mediatek/lbs/em/LbsGps$StressTest;->this$0:Lcom/mediatek/lbs/em/LbsGps;

    const/4 v2, 0x1

    # invokes: Lcom/mediatek/lbs/em/LbsGps;->sendMessage(II)V
    invoke-static {v1, v2, v4}, Lcom/mediatek/lbs/em/LbsGps;->access$3500(Lcom/mediatek/lbs/em/LbsGps;II)V

    return-void

    :cond_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method

.method public startStress()V
    .locals 0

    invoke-virtual {p0}, Lcom/mediatek/lbs/em/LbsGps$StressTest;->start()V

    return-void
.end method

.method public stopStress()V
    .locals 1

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/mediatek/lbs/em/LbsGps$StressTest;->enable:Z

    invoke-virtual {p0}, Lcom/mediatek/lbs/em/LbsGps$StressTest;->interrupt()V

    return-void
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "StressTest  numOfLoop="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/mediatek/lbs/em/LbsGps$StressTest;->numOfLoop:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " delay1="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/mediatek/lbs/em/LbsGps$StressTest;->delay1:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " delay2="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/mediatek/lbs/em/LbsGps$StressTest;->delay2:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " delay3="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/mediatek/lbs/em/LbsGps$StressTest;->delay3:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " delay4="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/mediatek/lbs/em/LbsGps$StressTest;->delay4:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
