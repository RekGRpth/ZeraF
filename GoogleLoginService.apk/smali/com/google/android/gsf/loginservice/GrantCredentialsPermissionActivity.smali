.class public Lcom/google/android/gsf/loginservice/GrantCredentialsPermissionActivity;
.super Lcom/google/android/gsf/loginservice/BaseActivity;
.source "GrantCredentialsPermissionActivity.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field protected mInflater:Landroid/view/LayoutInflater;


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Lcom/google/android/gsf/loginservice/BaseActivity;-><init>()V

    return-void
.end method

.method private newPackageView(Ljava/lang/String;)Landroid/view/View;
    .locals 4
    .param p1    # Ljava/lang/String;

    iget-object v1, p0, Lcom/google/android/gsf/loginservice/GrantCredentialsPermissionActivity;->mInflater:Landroid/view/LayoutInflater;

    const v2, 0x7f030014

    const/4 v3, 0x0

    invoke-virtual {v1, v2, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    const v1, 0x7f0b0038

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    invoke-virtual {v1, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    return-object v0
.end method

.method private newScopeView(Ljava/lang/String;)Landroid/view/View;
    .locals 4
    .param p1    # Ljava/lang/String;

    iget-object v1, p0, Lcom/google/android/gsf/loginservice/GrantCredentialsPermissionActivity;->mInflater:Landroid/view/LayoutInflater;

    const v2, 0x7f030015

    const/4 v3, 0x0

    invoke-virtual {v1, v2, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    const v1, 0x7f0b003a

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    invoke-virtual {v1, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    return-object v0
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 6
    .param p1    # Landroid/view/View;

    iget-object v1, p0, Lcom/google/android/gsf/loginservice/GrantCredentialsPermissionActivity;->mSession:Lcom/google/android/gsf/loginservice/GLSUser$GLSSession;

    const/4 v2, 0x0

    iput-object v2, v1, Lcom/google/android/gsf/loginservice/GLSUser$GLSSession;->mPermission:Ljava/util/ArrayList;

    invoke-static {p0}, Landroid/accounts/AccountManager;->get(Landroid/content/Context;)Landroid/accounts/AccountManager;

    move-result-object v0

    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v1

    packed-switch v1, :pswitch_data_0

    :goto_0
    invoke-virtual {p0}, Lcom/google/android/gsf/loginservice/GrantCredentialsPermissionActivity;->finish()V

    return-void

    :pswitch_0
    iget-object v1, p0, Lcom/google/android/gsf/loginservice/GrantCredentialsPermissionActivity;->mGlsUser:Lcom/google/android/gsf/loginservice/GLSUser;

    iget-object v2, p0, Lcom/google/android/gsf/loginservice/GrantCredentialsPermissionActivity;->mService:Ljava/lang/String;

    iget-object v3, p0, Lcom/google/android/gsf/loginservice/GrantCredentialsPermissionActivity;->mSession:Lcom/google/android/gsf/loginservice/GLSUser$GLSSession;

    iget v3, v3, Lcom/google/android/gsf/loginservice/GLSUser$GLSSession;->mCallingUID:I

    const-string v4, "1"

    iget-object v5, p0, Lcom/google/android/gsf/loginservice/GrantCredentialsPermissionActivity;->mSession:Lcom/google/android/gsf/loginservice/GLSUser$GLSSession;

    iget-object v5, v5, Lcom/google/android/gsf/loginservice/GLSUser$GLSSession;->mAccountManagerOptions:Landroid/os/Bundle;

    invoke-virtual {v1, v2, v3, v4, v5}, Lcom/google/android/gsf/loginservice/GLSUser;->setStoredPermission(Ljava/lang/String;ILjava/lang/String;Landroid/os/Bundle;)V

    invoke-virtual {p0}, Lcom/google/android/gsf/loginservice/GrantCredentialsPermissionActivity;->accountAuthenticatorRetryResult()V

    const/4 v1, -0x1

    invoke-virtual {p0, v1}, Lcom/google/android/gsf/loginservice/GrantCredentialsPermissionActivity;->setResult(I)V

    goto :goto_0

    :pswitch_1
    iget-object v1, p0, Lcom/google/android/gsf/loginservice/GrantCredentialsPermissionActivity;->mGlsUser:Lcom/google/android/gsf/loginservice/GLSUser;

    iget-object v2, p0, Lcom/google/android/gsf/loginservice/GrantCredentialsPermissionActivity;->mService:Ljava/lang/String;

    iget-object v3, p0, Lcom/google/android/gsf/loginservice/GrantCredentialsPermissionActivity;->mSession:Lcom/google/android/gsf/loginservice/GLSUser$GLSSession;

    iget v3, v3, Lcom/google/android/gsf/loginservice/GLSUser$GLSSession;->mCallingUID:I

    const-string v4, "0"

    iget-object v5, p0, Lcom/google/android/gsf/loginservice/GrantCredentialsPermissionActivity;->mSession:Lcom/google/android/gsf/loginservice/GLSUser$GLSSession;

    iget-object v5, v5, Lcom/google/android/gsf/loginservice/GLSUser$GLSSession;->mAccountManagerOptions:Landroid/os/Bundle;

    invoke-virtual {v1, v2, v3, v4, v5}, Lcom/google/android/gsf/loginservice/GLSUser;->setStoredPermission(Ljava/lang/String;ILjava/lang/String;Landroid/os/Bundle;)V

    const/4 v1, 0x0

    invoke-virtual {p0, v1}, Lcom/google/android/gsf/loginservice/GrantCredentialsPermissionActivity;->setResult(I)V

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x7f0b0028
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 15
    .param p1    # Landroid/os/Bundle;

    invoke-super/range {p0 .. p1}, Lcom/google/android/gsf/loginservice/BaseActivity;->onCreate(Landroid/os/Bundle;)V

    const v13, 0x7f0800ce

    invoke-virtual {p0, v13}, Lcom/google/android/gsf/loginservice/GrantCredentialsPermissionActivity;->setTitle(I)V

    const v13, 0x7f03000e

    invoke-virtual {p0, v13}, Lcom/google/android/gsf/loginservice/GrantCredentialsPermissionActivity;->setContentView(I)V

    const-string v13, "layout_inflater"

    invoke-virtual {p0, v13}, Lcom/google/android/gsf/loginservice/GrantCredentialsPermissionActivity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v13

    check-cast v13, Landroid/view/LayoutInflater;

    iput-object v13, p0, Lcom/google/android/gsf/loginservice/GrantCredentialsPermissionActivity;->mInflater:Landroid/view/LayoutInflater;

    invoke-virtual {p0}, Lcom/google/android/gsf/loginservice/GrantCredentialsPermissionActivity;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v9

    iget-object v13, p0, Lcom/google/android/gsf/loginservice/GrantCredentialsPermissionActivity;->mSession:Lcom/google/android/gsf/loginservice/GLSUser$GLSSession;

    iget v13, v13, Lcom/google/android/gsf/loginservice/GLSUser$GLSSession;->mCallingUID:I

    invoke-virtual {v9, v13}, Landroid/content/pm/PackageManager;->getPackagesForUid(I)[Ljava/lang/String;

    move-result-object v6

    iget-object v13, p0, Lcom/google/android/gsf/loginservice/GrantCredentialsPermissionActivity;->mSession:Lcom/google/android/gsf/loginservice/GLSUser$GLSSession;

    iget-object v13, v13, Lcom/google/android/gsf/loginservice/GLSUser$GLSSession;->mUsername:Ljava/lang/String;

    if-eqz v13, :cond_0

    iget-object v13, p0, Lcom/google/android/gsf/loginservice/GrantCredentialsPermissionActivity;->mService:Ljava/lang/String;

    if-eqz v13, :cond_0

    if-nez v6, :cond_1

    :cond_0
    const/4 v13, 0x0

    invoke-virtual {p0, v13}, Lcom/google/android/gsf/loginservice/GrantCredentialsPermissionActivity;->setResult(I)V

    invoke-virtual {p0}, Lcom/google/android/gsf/loginservice/GrantCredentialsPermissionActivity;->finish()V

    :goto_0
    return-void

    :cond_1
    const v13, 0x7f080002

    invoke-virtual {p0, v13}, Lcom/google/android/gsf/loginservice/GrantCredentialsPermissionActivity;->getString(I)Ljava/lang/String;

    move-result-object v0

    iget-object v13, p0, Lcom/google/android/gsf/loginservice/GrantCredentialsPermissionActivity;->mSession:Lcom/google/android/gsf/loginservice/GLSUser$GLSSession;

    if-nez v13, :cond_5

    const/4 v11, 0x0

    :goto_1
    if-eqz v11, :cond_2

    invoke-interface {v11}, Ljava/util/List;->size()I

    move-result v13

    if-nez v13, :cond_4

    :cond_2
    new-instance v11, Ljava/util/ArrayList;

    invoke-direct {v11}, Ljava/util/ArrayList;-><init>()V

    iget-object v13, p0, Lcom/google/android/gsf/loginservice/GrantCredentialsPermissionActivity;->mService:Ljava/lang/String;

    invoke-static {p0, v13}, Lcom/google/android/gsf/loginservice/GLSUser;->getAuthTokenLabel(Landroid/content/Context;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v10

    if-nez v10, :cond_3

    iget-object v10, p0, Lcom/google/android/gsf/loginservice/GrantCredentialsPermissionActivity;->mService:Ljava/lang/String;

    :cond_3
    invoke-interface {v11, v10}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :cond_4
    const v13, 0x7f0b0027

    invoke-virtual {p0, v13}, Lcom/google/android/gsf/loginservice/GrantCredentialsPermissionActivity;->findViewById(I)Landroid/view/View;

    move-result-object v12

    check-cast v12, Landroid/widget/LinearLayout;

    invoke-interface {v11}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_2
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v13

    if-eqz v13, :cond_6

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v10

    check-cast v10, Ljava/lang/String;

    invoke-direct {p0, v10}, Lcom/google/android/gsf/loginservice/GrantCredentialsPermissionActivity;->newScopeView(Ljava/lang/String;)Landroid/view/View;

    move-result-object v13

    invoke-virtual {v12, v13}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    goto :goto_2

    :cond_5
    iget-object v13, p0, Lcom/google/android/gsf/loginservice/GrantCredentialsPermissionActivity;->mSession:Lcom/google/android/gsf/loginservice/GLSUser$GLSSession;

    iget-object v11, v13, Lcom/google/android/gsf/loginservice/GLSUser$GLSSession;->mPermission:Ljava/util/ArrayList;

    goto :goto_1

    :cond_6
    const v13, 0x7f0b0025

    invoke-virtual {p0, v13}, Lcom/google/android/gsf/loginservice/GrantCredentialsPermissionActivity;->findViewById(I)Landroid/view/View;

    move-result-object v7

    check-cast v7, Landroid/widget/LinearLayout;

    move-object v1, v6

    array-length v4, v1

    const/4 v3, 0x0

    :goto_3
    if-ge v3, v4, :cond_7

    aget-object v8, v1, v3

    const/4 v13, 0x0

    :try_start_0
    invoke-virtual {v9, v8, v13}, Landroid/content/pm/PackageManager;->getApplicationInfo(Ljava/lang/String;I)Landroid/content/pm/ApplicationInfo;

    move-result-object v13

    invoke-virtual {v9, v13}, Landroid/content/pm/PackageManager;->getApplicationLabel(Landroid/content/pm/ApplicationInfo;)Ljava/lang/CharSequence;

    move-result-object v13

    invoke-virtual {v13}, Ljava/lang/Object;->toString()Ljava/lang/String;
    :try_end_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v5

    :goto_4
    invoke-direct {p0, v5}, Lcom/google/android/gsf/loginservice/GrantCredentialsPermissionActivity;->newPackageView(Ljava/lang/String;)Landroid/view/View;

    move-result-object v13

    invoke-virtual {v7, v13}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    add-int/lit8 v3, v3, 0x1

    goto :goto_3

    :catch_0
    move-exception v2

    move-object v5, v8

    goto :goto_4

    :cond_7
    const v13, 0x7f0b0023

    invoke-virtual {p0, v13}, Lcom/google/android/gsf/loginservice/GrantCredentialsPermissionActivity;->findViewById(I)Landroid/view/View;

    move-result-object v13

    check-cast v13, Landroid/widget/TextView;

    iget-object v14, p0, Lcom/google/android/gsf/loginservice/GrantCredentialsPermissionActivity;->mSession:Lcom/google/android/gsf/loginservice/GLSUser$GLSSession;

    iget-object v14, v14, Lcom/google/android/gsf/loginservice/GLSUser$GLSSession;->mUsername:Ljava/lang/String;

    invoke-virtual {v13, v14}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    const v13, 0x7f0b0029

    invoke-virtual {p0, v13}, Lcom/google/android/gsf/loginservice/GrantCredentialsPermissionActivity;->findViewById(I)Landroid/view/View;

    move-result-object v13

    invoke-virtual {v13, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    const v13, 0x7f0b0028

    invoke-virtual {p0, v13}, Lcom/google/android/gsf/loginservice/GrantCredentialsPermissionActivity;->findViewById(I)Landroid/view/View;

    move-result-object v13

    invoke-virtual {v13, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    goto/16 :goto_0
.end method
