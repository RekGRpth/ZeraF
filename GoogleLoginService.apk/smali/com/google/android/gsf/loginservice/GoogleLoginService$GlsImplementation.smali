.class Lcom/google/android/gsf/loginservice/GoogleLoginService$GlsImplementation;
.super Lcom/google/android/gsf/IGoogleLoginService$Stub;
.source "GoogleLoginService.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/gsf/loginservice/GoogleLoginService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = "GlsImplementation"
.end annotation


# instance fields
.field private final mAccountManager:Landroid/accounts/AccountManager;

.field final synthetic this$0:Lcom/google/android/gsf/loginservice/GoogleLoginService;


# direct methods
.method constructor <init>(Lcom/google/android/gsf/loginservice/GoogleLoginService;Landroid/content/Context;)V
    .locals 1
    .param p2    # Landroid/content/Context;

    iput-object p1, p0, Lcom/google/android/gsf/loginservice/GoogleLoginService$GlsImplementation;->this$0:Lcom/google/android/gsf/loginservice/GoogleLoginService;

    invoke-direct {p0}, Lcom/google/android/gsf/IGoogleLoginService$Stub;-><init>()V

    invoke-virtual {p2}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Landroid/accounts/AccountManager;->get(Landroid/content/Context;)Landroid/accounts/AccountManager;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gsf/loginservice/GoogleLoginService$GlsImplementation;->mAccountManager:Landroid/accounts/AccountManager;

    return-void
.end method


# virtual methods
.method public blockingGetCredentials(Ljava/lang/String;Ljava/lang/String;Z)Lcom/google/android/gsf/GoogleLoginCredentialsResult;
    .locals 12
    .param p1    # Ljava/lang/String;
    .param p2    # Ljava/lang/String;
    .param p3    # Z

    if-nez p2, :cond_0

    const-string p2, "SID"

    :cond_0
    new-instance v11, Lcom/google/android/gsf/GoogleLoginCredentialsResult;

    invoke-direct {v11}, Lcom/google/android/gsf/GoogleLoginCredentialsResult;-><init>()V

    if-nez p1, :cond_1

    new-instance v1, Ljava/lang/IllegalArgumentException;

    const-string v3, "No user"

    invoke-direct {v1, v3}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v1

    :cond_1
    new-instance v2, Landroid/accounts/Account;

    const-string v1, "com.google"

    invoke-direct {v2, p1, v1}, Landroid/accounts/Account;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    const/4 v8, 0x0

    :try_start_0
    iget-object v1, p0, Lcom/google/android/gsf/loginservice/GoogleLoginService$GlsImplementation;->mAccountManager:Landroid/accounts/AccountManager;

    const/4 v5, 0x0

    const/4 v6, 0x0

    move-object v3, p2

    move v4, p3

    invoke-virtual/range {v1 .. v6}, Landroid/accounts/AccountManager;->getAuthToken(Landroid/accounts/Account;Ljava/lang/String;ZLandroid/accounts/AccountManagerCallback;Landroid/os/Handler;)Landroid/accounts/AccountManagerFuture;

    move-result-object v1

    invoke-interface {v1}, Landroid/accounts/AccountManagerFuture;->getResult()Ljava/lang/Object;

    move-result-object v1

    move-object v0, v1

    check-cast v0, Landroid/os/Bundle;

    move-object v8, v0
    :try_end_0
    .catch Landroid/accounts/OperationCanceledException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Landroid/accounts/AuthenticatorException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_2

    :goto_0
    invoke-virtual {v11, p1}, Lcom/google/android/gsf/GoogleLoginCredentialsResult;->setAccount(Ljava/lang/String;)V

    if-eqz v8, :cond_3

    const-string v1, "authtoken"

    invoke-virtual {v8, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    if-eqz v7, :cond_2

    invoke-virtual {v11, v7}, Lcom/google/android/gsf/GoogleLoginCredentialsResult;->setCredentialsString(Ljava/lang/String;)V

    :goto_1
    return-object v11

    :catch_0
    move-exception v9

    const-string v1, "GoogleLoginService"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Exception in GLS.getAuthToken "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v1, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    :catch_1
    move-exception v9

    const-string v1, "GoogleLoginService"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Exception in GLS.getAuthToken "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v1, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    :catch_2
    move-exception v9

    const-string v1, "GoogleLoginService"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Exception in GLS.getAuthToken "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v1, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    :cond_2
    const-string v1, "intent"

    invoke-virtual {v8, v1}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v10

    check-cast v10, Landroid/content/Intent;

    if-eqz v10, :cond_3

    invoke-virtual {v11, v10}, Lcom/google/android/gsf/GoogleLoginCredentialsResult;->setCredentialsIntent(Landroid/content/Intent;)V

    goto :goto_1

    :cond_3
    const-string v1, "GoogleLoginService"

    const-string v3, "Not bundle, token or intent returned"

    invoke-static {v1, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1
.end method

.method public deleteAllAccounts()V
    .locals 8

    const/4 v7, 0x0

    iget-object v5, p0, Lcom/google/android/gsf/loginservice/GoogleLoginService$GlsImplementation;->this$0:Lcom/google/android/gsf/loginservice/GoogleLoginService;

    const-string v6, "com.google.android.googleapps.permission.GOOGLE_AUTH"

    # invokes: Lcom/google/android/gsf/loginservice/GoogleLoginService;->checkBinderPermissions(Ljava/lang/String;)V
    invoke-static {v5, v6}, Lcom/google/android/gsf/loginservice/GoogleLoginService;->access$100(Lcom/google/android/gsf/loginservice/GoogleLoginService;Ljava/lang/String;)V

    iget-object v5, p0, Lcom/google/android/gsf/loginservice/GoogleLoginService$GlsImplementation;->this$0:Lcom/google/android/gsf/loginservice/GoogleLoginService;

    # invokes: Lcom/google/android/gsf/loginservice/GoogleLoginService;->checkPasswordPermission()V
    invoke-static {v5}, Lcom/google/android/gsf/loginservice/GoogleLoginService;->access$300(Lcom/google/android/gsf/loginservice/GoogleLoginService;)V

    iget-object v5, p0, Lcom/google/android/gsf/loginservice/GoogleLoginService$GlsImplementation;->mAccountManager:Landroid/accounts/AccountManager;

    const-string v6, "com.google"

    invoke-virtual {v5, v6}, Landroid/accounts/AccountManager;->getAccountsByType(Ljava/lang/String;)[Landroid/accounts/Account;

    move-result-object v1

    move-object v2, v1

    array-length v4, v2

    const/4 v3, 0x0

    :goto_0
    if-ge v3, v4, :cond_0

    aget-object v0, v2, v3

    iget-object v5, p0, Lcom/google/android/gsf/loginservice/GoogleLoginService$GlsImplementation;->mAccountManager:Landroid/accounts/AccountManager;

    invoke-virtual {v5, v0, v7, v7}, Landroid/accounts/AccountManager;->removeAccount(Landroid/accounts/Account;Landroid/accounts/AccountManagerCallback;Landroid/os/Handler;)Landroid/accounts/AccountManagerFuture;

    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    :cond_0
    return-void
.end method

.method public deleteOneAccount(Ljava/lang/String;)V
    .locals 4
    .param p1    # Ljava/lang/String;

    const/4 v3, 0x0

    iget-object v1, p0, Lcom/google/android/gsf/loginservice/GoogleLoginService$GlsImplementation;->this$0:Lcom/google/android/gsf/loginservice/GoogleLoginService;

    const-string v2, "com.google.android.googleapps.permission.GOOGLE_AUTH"

    # invokes: Lcom/google/android/gsf/loginservice/GoogleLoginService;->checkBinderPermissions(Ljava/lang/String;)V
    invoke-static {v1, v2}, Lcom/google/android/gsf/loginservice/GoogleLoginService;->access$100(Lcom/google/android/gsf/loginservice/GoogleLoginService;Ljava/lang/String;)V

    iget-object v1, p0, Lcom/google/android/gsf/loginservice/GoogleLoginService$GlsImplementation;->this$0:Lcom/google/android/gsf/loginservice/GoogleLoginService;

    # invokes: Lcom/google/android/gsf/loginservice/GoogleLoginService;->checkPasswordPermission()V
    invoke-static {v1}, Lcom/google/android/gsf/loginservice/GoogleLoginService;->access$300(Lcom/google/android/gsf/loginservice/GoogleLoginService;)V

    new-instance v0, Landroid/accounts/Account;

    const-string v1, "com.google"

    invoke-direct {v0, p1, v1}, Landroid/accounts/Account;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v1, p0, Lcom/google/android/gsf/loginservice/GoogleLoginService$GlsImplementation;->mAccountManager:Landroid/accounts/AccountManager;

    invoke-virtual {v1, v0, v3, v3}, Landroid/accounts/AccountManager;->removeAccount(Landroid/accounts/Account;Landroid/accounts/AccountManagerCallback;Landroid/os/Handler;)Landroid/accounts/AccountManagerFuture;

    return-void
.end method

.method public getAccount(Z)Ljava/lang/String;
    .locals 2
    .param p1    # Z
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    iget-object v0, p0, Lcom/google/android/gsf/loginservice/GoogleLoginService$GlsImplementation;->this$0:Lcom/google/android/gsf/loginservice/GoogleLoginService;

    const-string v1, "com.google.android.googleapps.permission.GOOGLE_AUTH"

    # invokes: Lcom/google/android/gsf/loginservice/GoogleLoginService;->checkBinderPermissions(Ljava/lang/String;)V
    invoke-static {v0, v1}, Lcom/google/android/gsf/loginservice/GoogleLoginService;->access$100(Lcom/google/android/gsf/loginservice/GoogleLoginService;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/google/android/gsf/loginservice/GoogleLoginService$GlsImplementation;->mAccountManager:Landroid/accounts/AccountManager;

    # invokes: Lcom/google/android/gsf/loginservice/GoogleLoginService;->getAccountInternal(Landroid/accounts/AccountManager;Z)Ljava/lang/String;
    invoke-static {v0, p1}, Lcom/google/android/gsf/loginservice/GoogleLoginService;->access$200(Landroid/accounts/AccountManager;Z)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getAccounts()[Ljava/lang/String;
    .locals 5

    iget-object v3, p0, Lcom/google/android/gsf/loginservice/GoogleLoginService$GlsImplementation;->this$0:Lcom/google/android/gsf/loginservice/GoogleLoginService;

    const-string v4, "com.google.android.googleapps.permission.GOOGLE_AUTH"

    # invokes: Lcom/google/android/gsf/loginservice/GoogleLoginService;->checkBinderPermissions(Ljava/lang/String;)V
    invoke-static {v3, v4}, Lcom/google/android/gsf/loginservice/GoogleLoginService;->access$100(Lcom/google/android/gsf/loginservice/GoogleLoginService;Ljava/lang/String;)V

    iget-object v3, p0, Lcom/google/android/gsf/loginservice/GoogleLoginService$GlsImplementation;->mAccountManager:Landroid/accounts/AccountManager;

    const-string v4, "com.google"

    invoke-virtual {v3, v4}, Landroid/accounts/AccountManager;->getAccountsByType(Ljava/lang/String;)[Landroid/accounts/Account;

    move-result-object v1

    array-length v3, v1

    new-array v0, v3, [Ljava/lang/String;

    const/4 v2, 0x0

    :goto_0
    array-length v3, v1

    if-ge v2, v3, :cond_0

    aget-object v3, v1, v2

    iget-object v3, v3, Landroid/accounts/Account;->name:Ljava/lang/String;

    aput-object v3, v0, v2

    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    :cond_0
    return-object v0
.end method

.method public getAndroidId()J
    .locals 4

    iget-object v0, p0, Lcom/google/android/gsf/loginservice/GoogleLoginService$GlsImplementation;->this$0:Lcom/google/android/gsf/loginservice/GoogleLoginService;

    const-string v1, "com.google.android.googleapps.permission.GOOGLE_AUTH"

    # invokes: Lcom/google/android/gsf/loginservice/GoogleLoginService;->checkBinderPermissions(Ljava/lang/String;)V
    invoke-static {v0, v1}, Lcom/google/android/gsf/loginservice/GoogleLoginService;->access$100(Lcom/google/android/gsf/loginservice/GoogleLoginService;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/google/android/gsf/loginservice/GoogleLoginService$GlsImplementation;->this$0:Lcom/google/android/gsf/loginservice/GoogleLoginService;

    invoke-virtual {v0}, Lcom/google/android/gsf/loginservice/GoogleLoginService;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const-string v1, "android_id"

    const-wide/16 v2, 0x0

    invoke-static {v0, v1, v2, v3}, Lcom/google/android/gsf/Gservices;->getLong(Landroid/content/ContentResolver;Ljava/lang/String;J)J

    move-result-wide v0

    return-wide v0
.end method

.method public getPrimaryAccount()Ljava/lang/String;
    .locals 2
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    iget-object v0, p0, Lcom/google/android/gsf/loginservice/GoogleLoginService$GlsImplementation;->this$0:Lcom/google/android/gsf/loginservice/GoogleLoginService;

    const-string v1, "com.google.android.googleapps.permission.GOOGLE_AUTH"

    # invokes: Lcom/google/android/gsf/loginservice/GoogleLoginService;->checkBinderPermissions(Ljava/lang/String;)V
    invoke-static {v0, v1}, Lcom/google/android/gsf/loginservice/GoogleLoginService;->access$100(Lcom/google/android/gsf/loginservice/GoogleLoginService;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/google/android/gsf/loginservice/GoogleLoginService$GlsImplementation;->mAccountManager:Landroid/accounts/AccountManager;

    const/4 v1, 0x0

    # invokes: Lcom/google/android/gsf/loginservice/GoogleLoginService;->getAccountInternal(Landroid/accounts/AccountManager;Z)Ljava/lang/String;
    invoke-static {v0, v1}, Lcom/google/android/gsf/loginservice/GoogleLoginService;->access$200(Landroid/accounts/AccountManager;Z)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public invalidateAuthToken(Ljava/lang/String;)V
    .locals 2
    .param p1    # Ljava/lang/String;

    iget-object v0, p0, Lcom/google/android/gsf/loginservice/GoogleLoginService$GlsImplementation;->this$0:Lcom/google/android/gsf/loginservice/GoogleLoginService;

    const-string v1, "com.google.android.googleapps.permission.GOOGLE_AUTH"

    # invokes: Lcom/google/android/gsf/loginservice/GoogleLoginService;->checkBinderPermissions(Ljava/lang/String;)V
    invoke-static {v0, v1}, Lcom/google/android/gsf/loginservice/GoogleLoginService;->access$100(Lcom/google/android/gsf/loginservice/GoogleLoginService;Ljava/lang/String;)V

    if-nez p1, :cond_0

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/google/android/gsf/loginservice/GoogleLoginService$GlsImplementation;->mAccountManager:Landroid/accounts/AccountManager;

    const-string v1, "com.google"

    invoke-virtual {v0, v1, p1}, Landroid/accounts/AccountManager;->invalidateAuthToken(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public peekCredentials(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    .locals 2
    .param p1    # Ljava/lang/String;
    .param p2    # Ljava/lang/String;

    new-instance v0, Ljava/lang/RuntimeException;

    const-string v1, "Deprecated"

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public saveAuthToken(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 2
    .param p1    # Ljava/lang/String;
    .param p2    # Ljava/lang/String;
    .param p3    # Ljava/lang/String;

    new-instance v0, Ljava/lang/UnsupportedOperationException;

    const-string v1, "Deprecated"

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public saveNewAccount(Lcom/google/android/gsf/LoginData;)V
    .locals 2
    .param p1    # Lcom/google/android/gsf/LoginData;

    new-instance v0, Ljava/lang/RuntimeException;

    const-string v1, "Deprecated"

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public saveUsernameAndPassword(Ljava/lang/String;Ljava/lang/String;I)V
    .locals 2
    .param p1    # Ljava/lang/String;
    .param p2    # Ljava/lang/String;
    .param p3    # I

    new-instance v0, Ljava/lang/RuntimeException;

    const-string v1, "Deprecated"

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public tryNewAccount(Lcom/google/android/gsf/LoginData;)V
    .locals 2
    .param p1    # Lcom/google/android/gsf/LoginData;

    new-instance v0, Ljava/lang/RuntimeException;

    const-string v1, "Deprecated"

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public updatePassword(Lcom/google/android/gsf/LoginData;)V
    .locals 2
    .param p1    # Lcom/google/android/gsf/LoginData;

    new-instance v0, Ljava/lang/UnsupportedOperationException;

    const-string v1, "Deprecated"

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public verifyStoredPassword(Ljava/lang/String;Ljava/lang/String;)Z
    .locals 2
    .param p1    # Ljava/lang/String;
    .param p2    # Ljava/lang/String;

    new-instance v0, Ljava/lang/UnsupportedOperationException;

    const-string v1, "Deprecated"

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public waitForAndroidId()I
    .locals 5

    invoke-virtual {p0}, Lcom/google/android/gsf/loginservice/GoogleLoginService$GlsImplementation;->getAndroidId()J

    move-result-wide v1

    const-wide/16 v3, 0x0

    cmp-long v1, v1, v3

    if-nez v1, :cond_0

    const/4 v0, -0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
