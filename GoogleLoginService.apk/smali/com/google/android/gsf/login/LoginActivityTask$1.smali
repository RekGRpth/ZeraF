.class Lcom/google/android/gsf/login/LoginActivityTask$1;
.super Lcom/google/android/gsf/login/CancelableCallbackThread;
.source "LoginActivityTask.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/gsf/login/LoginActivityTask;->start()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/gsf/login/LoginActivityTask;

.field final synthetic val$resultMessage:Landroid/os/Message;


# direct methods
.method constructor <init>(Lcom/google/android/gsf/login/LoginActivityTask;Landroid/os/Message;Landroid/os/Message;)V
    .locals 0
    .param p2    # Landroid/os/Message;

    iput-object p1, p0, Lcom/google/android/gsf/login/LoginActivityTask$1;->this$0:Lcom/google/android/gsf/login/LoginActivityTask;

    iput-object p3, p0, Lcom/google/android/gsf/login/LoginActivityTask$1;->val$resultMessage:Landroid/os/Message;

    invoke-direct {p0, p2}, Lcom/google/android/gsf/login/CancelableCallbackThread;-><init>(Landroid/os/Message;)V

    return-void
.end method


# virtual methods
.method protected runInBackground()V
    .locals 18

    const/4 v15, 0x0

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/gsf/login/LoginActivityTask$1;->this$0:Lcom/google/android/gsf/login/LoginActivityTask;

    invoke-virtual {v3}, Lcom/google/android/gsf/login/LoginActivityTask;->getUserData()Ljava/util/HashMap;

    move-result-object v3

    sget-object v4, Lcom/google/android/gsf/loginservice/GLSUser$RequestKey;->PASSWORD:Lcom/google/android/gsf/loginservice/GLSUser$RequestKey;

    invoke-virtual {v4}, Lcom/google/android/gsf/loginservice/GLSUser$RequestKey;->getWire()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v14

    check-cast v14, Ljava/lang/String;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/gsf/login/LoginActivityTask$1;->this$0:Lcom/google/android/gsf/login/LoginActivityTask;

    # getter for: Lcom/google/android/gsf/login/LoginActivityTask;->mConfirmCredentials:Z
    invoke-static {v3}, Lcom/google/android/gsf/login/LoginActivityTask;->access$000(Lcom/google/android/gsf/login/LoginActivityTask;)Z

    move-result v3

    if-eqz v3, :cond_1

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/gsf/login/LoginActivityTask$1;->this$0:Lcom/google/android/gsf/login/LoginActivityTask;

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/gsf/login/LoginActivityTask$1;->val$resultMessage:Landroid/os/Message;

    invoke-virtual {v3, v4, v14}, Lcom/google/android/gsf/login/LoginActivityTask;->confirmCredentials(Landroid/os/Message;Ljava/lang/String;)V

    :cond_0
    :goto_0
    return-void

    :cond_1
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/gsf/login/LoginActivityTask$1;->this$0:Lcom/google/android/gsf/login/LoginActivityTask;

    # getter for: Lcom/google/android/gsf/login/LoginActivityTask;->mAddAccount:Z
    invoke-static {v3}, Lcom/google/android/gsf/login/LoginActivityTask;->access$100(Lcom/google/android/gsf/login/LoginActivityTask;)Z

    move-result v3

    if-nez v3, :cond_2

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/gsf/login/LoginActivityTask$1;->this$0:Lcom/google/android/gsf/login/LoginActivityTask;

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/gsf/login/LoginActivityTask$1;->val$resultMessage:Landroid/os/Message;

    # invokes: Lcom/google/android/gsf/login/LoginActivityTask;->updateAccount(Landroid/os/Message;Ljava/lang/String;)V
    invoke-static {v3, v4, v14}, Lcom/google/android/gsf/login/LoginActivityTask;->access$200(Lcom/google/android/gsf/login/LoginActivityTask;Landroid/os/Message;Ljava/lang/String;)V

    goto :goto_0

    :cond_2
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/gsf/login/LoginActivityTask$1;->this$0:Lcom/google/android/gsf/login/LoginActivityTask;

    # getter for: Lcom/google/android/gsf/login/LoginActivityTask;->mSession:Lcom/google/android/gsf/loginservice/GLSUser$GLSSession;
    invoke-static {v3}, Lcom/google/android/gsf/login/LoginActivityTask;->access$300(Lcom/google/android/gsf/login/LoginActivityTask;)Lcom/google/android/gsf/loginservice/GLSUser$GLSSession;

    move-result-object v3

    iget-object v3, v3, Lcom/google/android/gsf/loginservice/GLSUser$GLSSession;->mAccessToken:Ljava/lang/String;

    if-eqz v3, :cond_4

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/gsf/login/LoginActivityTask$1;->this$0:Lcom/google/android/gsf/login/LoginActivityTask;

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/gsf/login/LoginActivityTask$1;->this$0:Lcom/google/android/gsf/login/LoginActivityTask;

    const/4 v5, 0x0

    invoke-static {v4, v5}, Lcom/google/android/gsf/loginservice/GLSUser;->get(Landroid/content/Context;Ljava/lang/String;)Lcom/google/android/gsf/loginservice/GLSUser;

    move-result-object v4

    # setter for: Lcom/google/android/gsf/login/LoginActivityTask;->mGlsUser:Lcom/google/android/gsf/loginservice/GLSUser;
    invoke-static {v3, v4}, Lcom/google/android/gsf/login/LoginActivityTask;->access$402(Lcom/google/android/gsf/login/LoginActivityTask;Lcom/google/android/gsf/loginservice/GLSUser;)Lcom/google/android/gsf/loginservice/GLSUser;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/gsf/login/LoginActivityTask$1;->this$0:Lcom/google/android/gsf/login/LoginActivityTask;

    # getter for: Lcom/google/android/gsf/login/LoginActivityTask;->mGlsUser:Lcom/google/android/gsf/loginservice/GLSUser;
    invoke-static {v3}, Lcom/google/android/gsf/login/LoginActivityTask;->access$700(Lcom/google/android/gsf/login/LoginActivityTask;)Lcom/google/android/gsf/loginservice/GLSUser;

    move-result-object v3

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/gsf/login/LoginActivityTask$1;->this$0:Lcom/google/android/gsf/login/LoginActivityTask;

    # getter for: Lcom/google/android/gsf/login/LoginActivityTask;->mSession:Lcom/google/android/gsf/loginservice/GLSUser$GLSSession;
    invoke-static {v4}, Lcom/google/android/gsf/login/LoginActivityTask;->access$500(Lcom/google/android/gsf/login/LoginActivityTask;)Lcom/google/android/gsf/loginservice/GLSUser$GLSSession;

    move-result-object v4

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/google/android/gsf/login/LoginActivityTask$1;->this$0:Lcom/google/android/gsf/login/LoginActivityTask;

    # getter for: Lcom/google/android/gsf/login/LoginActivityTask;->mSession:Lcom/google/android/gsf/loginservice/GLSUser$GLSSession;
    invoke-static {v5}, Lcom/google/android/gsf/login/LoginActivityTask;->access$600(Lcom/google/android/gsf/login/LoginActivityTask;)Lcom/google/android/gsf/loginservice/GLSUser$GLSSession;

    move-result-object v5

    iget-object v5, v5, Lcom/google/android/gsf/loginservice/GLSUser$GLSSession;->mAccessToken:Ljava/lang/String;

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/google/android/gsf/login/LoginActivityTask$1;->this$0:Lcom/google/android/gsf/login/LoginActivityTask;

    invoke-virtual {v6}, Lcom/google/android/gsf/login/LoginActivityTask;->isSetupWizard()Z

    move-result v6

    invoke-virtual {v3, v4, v5, v6}, Lcom/google/android/gsf/loginservice/GLSUser;->addWithRequestToken(Lcom/google/android/gsf/loginservice/GLSUser$GLSSession;Ljava/lang/String;Z)Landroid/content/Intent;

    move-result-object v15

    :goto_1
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/gsf/login/LoginActivityTask$1;->this$0:Lcom/google/android/gsf/login/LoginActivityTask;

    const/4 v4, 0x0

    iput-boolean v4, v3, Lcom/google/android/gsf/login/LoginActivityTask;->mCancelable:Z

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/gsf/login/LoginActivityTask$1;->this$0:Lcom/google/android/gsf/login/LoginActivityTask;

    const/4 v4, 0x0

    iput-boolean v4, v3, Lcom/google/android/gsf/login/LoginActivityTask;->mAllowBackHardKey:Z

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/gsf/login/LoginActivityTask$1;->mCallbackMessage:Landroid/os/Message;

    invoke-virtual {v3}, Landroid/os/Message;->getData()Landroid/os/Bundle;

    move-result-object v3

    const-string v4, "intent"

    invoke-virtual {v3, v4, v15}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    const-string v3, "authtoken"

    invoke-virtual {v15, v3}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    if-eqz v3, :cond_8

    new-instance v3, Ljava/lang/Thread;

    new-instance v4, Lcom/google/android/gsf/login/LoginActivityTask$1$1;

    move-object/from16 v0, p0

    invoke-direct {v4, v0}, Lcom/google/android/gsf/login/LoginActivityTask$1$1;-><init>(Lcom/google/android/gsf/login/LoginActivityTask$1;)V

    invoke-direct {v3, v4}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;)V

    invoke-virtual {v3}, Ljava/lang/Thread;->start()V

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/gsf/login/LoginActivityTask$1;->this$0:Lcom/google/android/gsf/login/LoginActivityTask;

    iget-object v3, v3, Lcom/google/android/gsf/login/LoginActivityTask;->mPostAddLock:Ljava/util/concurrent/locks/Lock;

    invoke-interface {v3}, Ljava/util/concurrent/locks/Lock;->lock()V

    :try_start_0
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v3

    const-wide/32 v5, 0x1d4c0

    add-long v11, v3, v5

    :goto_2
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v3

    sub-long v16, v11, v3

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/gsf/login/LoginActivityTask$1;->this$0:Lcom/google/android/gsf/login/LoginActivityTask;

    iget-object v3, v3, Lcom/google/android/gsf/login/LoginActivityTask;->mPostAddTime:Ljava/util/concurrent/atomic/AtomicLong;

    invoke-virtual {v3}, Ljava/util/concurrent/atomic/AtomicLong;->get()J

    move-result-wide v3

    const-wide/16 v5, 0x0

    cmp-long v3, v3, v5

    if-gez v3, :cond_7

    const-wide/16 v3, 0x0

    cmp-long v3, v16, v3

    if-lez v3, :cond_7

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/gsf/login/LoginActivityTask$1;->this$0:Lcom/google/android/gsf/login/LoginActivityTask;

    iget-object v3, v3, Lcom/google/android/gsf/login/LoginActivityTask;->mPostAddCond:Ljava/util/concurrent/locks/Condition;

    sget-object v4, Ljava/util/concurrent/TimeUnit;->MILLISECONDS:Ljava/util/concurrent/TimeUnit;

    move-wide/from16 v0, v16

    invoke-interface {v3, v0, v1, v4}, Ljava/util/concurrent/locks/Condition;->await(JLjava/util/concurrent/TimeUnit;)Z
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_2

    :catch_0
    move-exception v10

    :try_start_1
    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Thread;->interrupt()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/gsf/login/LoginActivityTask$1;->this$0:Lcom/google/android/gsf/login/LoginActivityTask;

    iget-object v3, v3, Lcom/google/android/gsf/login/LoginActivityTask;->mPostAddLock:Ljava/util/concurrent/locks/Lock;

    invoke-interface {v3}, Ljava/util/concurrent/locks/Lock;->unlock()V

    :goto_3
    const-string v3, "GLSUser"

    const/4 v4, 0x2

    invoke-static {v3, v4}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v3

    if-eqz v3, :cond_3

    const-string v3, "GLSUser"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Market check "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/google/android/gsf/login/LoginActivityTask$1;->this$0:Lcom/google/android/gsf/login/LoginActivityTask;

    iget-object v5, v5, Lcom/google/android/gsf/login/LoginActivityTask;->mPostAddTime:Ljava/util/concurrent/atomic/AtomicLong;

    invoke-virtual {v5}, Ljava/util/concurrent/atomic/AtomicLong;->get()J

    move-result-wide v5

    invoke-virtual {v4, v5, v6}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "\n  showOffer "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/google/android/gsf/login/LoginActivityTask$1;->this$0:Lcom/google/android/gsf/login/LoginActivityTask;

    # getter for: Lcom/google/android/gsf/login/LoginActivityTask;->mSession:Lcom/google/android/gsf/loginservice/GLSUser$GLSSession;
    invoke-static {v5}, Lcom/google/android/gsf/login/LoginActivityTask;->access$1500(Lcom/google/android/gsf/login/LoginActivityTask;)Lcom/google/android/gsf/loginservice/GLSUser$GLSSession;

    move-result-object v5

    iget-boolean v5, v5, Lcom/google/android/gsf/loginservice/GLSUser$GLSSession;->mShowOffer:Z

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "\n  offerIntent "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/google/android/gsf/login/LoginActivityTask$1;->this$0:Lcom/google/android/gsf/login/LoginActivityTask;

    # getter for: Lcom/google/android/gsf/login/LoginActivityTask;->mSession:Lcom/google/android/gsf/loginservice/GLSUser$GLSSession;
    invoke-static {v5}, Lcom/google/android/gsf/login/LoginActivityTask;->access$1600(Lcom/google/android/gsf/login/LoginActivityTask;)Lcom/google/android/gsf/loginservice/GLSUser$GLSSession;

    move-result-object v5

    iget-object v5, v5, Lcom/google/android/gsf/loginservice/GLSUser$GLSSession;->mOfferIntent:Landroid/content/Intent;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "\n  offerMessageHtml "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/google/android/gsf/login/LoginActivityTask$1;->this$0:Lcom/google/android/gsf/login/LoginActivityTask;

    # getter for: Lcom/google/android/gsf/login/LoginActivityTask;->mSession:Lcom/google/android/gsf/loginservice/GLSUser$GLSSession;
    invoke-static {v5}, Lcom/google/android/gsf/login/LoginActivityTask;->access$1700(Lcom/google/android/gsf/login/LoginActivityTask;)Lcom/google/android/gsf/loginservice/GLSUser$GLSSession;

    move-result-object v5

    iget-object v5, v5, Lcom/google/android/gsf/loginservice/GLSUser$GLSSession;->mOfferMessageHtml:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    :cond_3
    sget-object v3, Lcom/google/android/gsf/loginservice/GLSUser$Status;->SUCCESS:Lcom/google/android/gsf/loginservice/GLSUser$Status;

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/gsf/login/LoginActivityTask$1;->val$resultMessage:Landroid/os/Message;

    invoke-virtual {v3, v4}, Lcom/google/android/gsf/loginservice/GLSUser$Status;->toMessage(Landroid/os/Message;)V

    :goto_4
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/gsf/login/LoginActivityTask$1;->mIsCanceled:Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-virtual {v3}, Ljava/util/concurrent/atomic/AtomicBoolean;->get()Z

    move-result v3

    if-nez v3, :cond_0

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/gsf/login/LoginActivityTask$1;->this$0:Lcom/google/android/gsf/login/LoginActivityTask;

    const-wide/16 v4, 0x3e8

    invoke-virtual {v3, v4, v5}, Lcom/google/android/gsf/login/LoginActivityTask;->ensureDelay(J)V

    goto/16 :goto_0

    :cond_4
    if-eqz v14, :cond_6

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/gsf/login/LoginActivityTask$1;->this$0:Lcom/google/android/gsf/login/LoginActivityTask;

    # getter for: Lcom/google/android/gsf/login/LoginActivityTask;->mSession:Lcom/google/android/gsf/loginservice/GLSUser$GLSSession;
    invoke-static {v3}, Lcom/google/android/gsf/login/LoginActivityTask;->access$800(Lcom/google/android/gsf/login/LoginActivityTask;)Lcom/google/android/gsf/loginservice/GLSUser$GLSSession;

    move-result-object v3

    iget-object v9, v3, Lcom/google/android/gsf/loginservice/GLSUser$GLSSession;->mCaptchaToken:Ljava/lang/String;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/gsf/login/LoginActivityTask$1;->this$0:Lcom/google/android/gsf/login/LoginActivityTask;

    # getter for: Lcom/google/android/gsf/login/LoginActivityTask;->mSession:Lcom/google/android/gsf/loginservice/GLSUser$GLSSession;
    invoke-static {v3}, Lcom/google/android/gsf/login/LoginActivityTask;->access$900(Lcom/google/android/gsf/login/LoginActivityTask;)Lcom/google/android/gsf/loginservice/GLSUser$GLSSession;

    move-result-object v3

    iget-object v8, v3, Lcom/google/android/gsf/loginservice/GLSUser$GLSSession;->mCaptchaAnswer:Ljava/lang/String;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/gsf/login/LoginActivityTask$1;->this$0:Lcom/google/android/gsf/login/LoginActivityTask;

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/gsf/login/LoginActivityTask$1;->this$0:Lcom/google/android/gsf/login/LoginActivityTask;

    # getter for: Lcom/google/android/gsf/login/LoginActivityTask;->mSession:Lcom/google/android/gsf/loginservice/GLSUser$GLSSession;
    invoke-static {v4}, Lcom/google/android/gsf/login/LoginActivityTask;->access$1000(Lcom/google/android/gsf/login/LoginActivityTask;)Lcom/google/android/gsf/loginservice/GLSUser$GLSSession;

    move-result-object v4

    iget-object v4, v4, Lcom/google/android/gsf/loginservice/GLSUser$GLSSession;->mUsername:Ljava/lang/String;

    invoke-static {v3, v4}, Lcom/google/android/gsf/loginservice/GLSUser;->get(Landroid/content/Context;Ljava/lang/String;)Lcom/google/android/gsf/loginservice/GLSUser;

    move-result-object v2

    invoke-virtual {v2, v14}, Lcom/google/android/gsf/loginservice/GLSUser;->setPassword(Ljava/lang/String;)V

    if-eqz v8, :cond_5

    invoke-virtual {v2, v9, v8}, Lcom/google/android/gsf/loginservice/GLSUser;->setCaptcha(Ljava/lang/String;Ljava/lang/String;)V

    :cond_5
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/gsf/login/LoginActivityTask$1;->this$0:Lcom/google/android/gsf/login/LoginActivityTask;

    # getter for: Lcom/google/android/gsf/login/LoginActivityTask;->mSession:Lcom/google/android/gsf/loginservice/GLSUser$GLSSession;
    invoke-static {v3}, Lcom/google/android/gsf/login/LoginActivityTask;->access$1100(Lcom/google/android/gsf/login/LoginActivityTask;)Lcom/google/android/gsf/loginservice/GLSUser$GLSSession;

    move-result-object v3

    const/4 v4, 0x0

    const/4 v5, 0x0

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/google/android/gsf/login/LoginActivityTask$1;->this$0:Lcom/google/android/gsf/login/LoginActivityTask;

    # getter for: Lcom/google/android/gsf/login/LoginActivityTask;->mSession:Lcom/google/android/gsf/loginservice/GLSUser$GLSSession;
    invoke-static {v6}, Lcom/google/android/gsf/login/LoginActivityTask;->access$1200(Lcom/google/android/gsf/login/LoginActivityTask;)Lcom/google/android/gsf/loginservice/GLSUser$GLSSession;

    move-result-object v6

    iget-boolean v6, v6, Lcom/google/android/gsf/loginservice/GLSUser$GLSSession;->mSetupWizard:Z

    const/4 v7, 0x0

    invoke-virtual/range {v2 .. v7}, Lcom/google/android/gsf/loginservice/GLSUser;->addAccount(Lcom/google/android/gsf/loginservice/GLSUser$GLSSession;ZZZLjava/lang/String;)Landroid/content/Intent;

    move-result-object v15

    goto/16 :goto_1

    :cond_6
    sget-object v3, Lcom/google/android/gsf/loginservice/GLSUser$Status;->BAD_AUTHENTICATION:Lcom/google/android/gsf/loginservice/GLSUser$Status;

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/gsf/login/LoginActivityTask$1;->val$resultMessage:Landroid/os/Message;

    invoke-virtual {v3, v4}, Lcom/google/android/gsf/loginservice/GLSUser$Status;->toMessage(Landroid/os/Message;)V

    goto/16 :goto_0

    :cond_7
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/gsf/login/LoginActivityTask$1;->this$0:Lcom/google/android/gsf/login/LoginActivityTask;

    iget-object v3, v3, Lcom/google/android/gsf/login/LoginActivityTask;->mPostAddLock:Ljava/util/concurrent/locks/Lock;

    invoke-interface {v3}, Ljava/util/concurrent/locks/Lock;->unlock()V

    goto/16 :goto_3

    :catchall_0
    move-exception v3

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/gsf/login/LoginActivityTask$1;->this$0:Lcom/google/android/gsf/login/LoginActivityTask;

    iget-object v4, v4, Lcom/google/android/gsf/login/LoginActivityTask;->mPostAddLock:Ljava/util/concurrent/locks/Lock;

    invoke-interface {v4}, Ljava/util/concurrent/locks/Lock;->unlock()V

    throw v3

    :cond_8
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/gsf/login/LoginActivityTask$1;->this$0:Lcom/google/android/gsf/login/LoginActivityTask;

    invoke-static {v3, v15}, Lcom/google/android/gsf/loginservice/GLSUser$Status;->fromIntent(Landroid/content/Context;Landroid/content/Intent;)Lcom/google/android/gsf/loginservice/GLSUser$Status;

    move-result-object v13

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/gsf/login/LoginActivityTask$1;->val$resultMessage:Landroid/os/Message;

    invoke-virtual {v13, v3}, Lcom/google/android/gsf/loginservice/GLSUser$Status;->toMessage(Landroid/os/Message;)V

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/gsf/login/LoginActivityTask$1;->val$resultMessage:Landroid/os/Message;

    invoke-virtual {v3}, Landroid/os/Message;->getData()Landroid/os/Bundle;

    move-result-object v3

    const-string v4, "intent"

    invoke-virtual {v3, v4, v15}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    goto/16 :goto_4
.end method
