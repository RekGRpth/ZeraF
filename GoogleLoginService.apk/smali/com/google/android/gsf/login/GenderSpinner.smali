.class public Lcom/google/android/gsf/login/GenderSpinner;
.super Landroid/widget/Spinner;
.source "GenderSpinner.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/gsf/login/GenderSpinner$OnSetSelectionListener;
    }
.end annotation


# instance fields
.field private mListener:Lcom/google/android/gsf/login/GenderSpinner$OnSetSelectionListener;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/util/AttributeSet;

    invoke-direct {p0, p1, p2}, Landroid/widget/Spinner;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    return-void
.end method


# virtual methods
.method public dismissMenu()V
    .locals 0

    invoke-virtual {p0}, Lcom/google/android/gsf/login/GenderSpinner;->onDetachedFromWindow()V

    invoke-virtual {p0}, Lcom/google/android/gsf/login/GenderSpinner;->clearFocus()V

    return-void
.end method

.method public setOnSetSelectionListener(Lcom/google/android/gsf/login/GenderSpinner$OnSetSelectionListener;)V
    .locals 0
    .param p1    # Lcom/google/android/gsf/login/GenderSpinner$OnSetSelectionListener;

    iput-object p1, p0, Lcom/google/android/gsf/login/GenderSpinner;->mListener:Lcom/google/android/gsf/login/GenderSpinner$OnSetSelectionListener;

    return-void
.end method

.method public setSelection(I)V
    .locals 1
    .param p1    # I

    invoke-super {p0, p1}, Landroid/widget/Spinner;->setSelection(I)V

    iget-object v0, p0, Lcom/google/android/gsf/login/GenderSpinner;->mListener:Lcom/google/android/gsf/login/GenderSpinner$OnSetSelectionListener;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/gsf/login/GenderSpinner;->mListener:Lcom/google/android/gsf/login/GenderSpinner$OnSetSelectionListener;

    invoke-interface {v0, p0, p1}, Lcom/google/android/gsf/login/GenderSpinner$OnSetSelectionListener;->onSetSelection(Lcom/google/android/gsf/login/GenderSpinner;I)V

    :cond_0
    return-void
.end method
