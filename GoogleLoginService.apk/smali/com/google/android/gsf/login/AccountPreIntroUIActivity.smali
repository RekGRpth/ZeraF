.class public Lcom/google/android/gsf/login/AccountPreIntroUIActivity;
.super Lcom/google/android/gsf/login/BaseActivity;
.source "AccountPreIntroUIActivity.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field private mMessageTextView:Landroid/widget/TextView;

.field private mNoButton:Landroid/widget/Button;

.field private mYesButton:Landroid/widget/Button;


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Lcom/google/android/gsf/login/BaseActivity;-><init>()V

    return-void
.end method

.method private initView()V
    .locals 6

    const v0, 0x7f030002

    invoke-virtual {p0, v0}, Lcom/google/android/gsf/login/AccountPreIntroUIActivity;->setContentView(I)V

    const v0, 0x7f0b000c

    invoke-virtual {p0, v0}, Lcom/google/android/gsf/login/AccountPreIntroUIActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lcom/google/android/gsf/login/AccountPreIntroUIActivity;->mYesButton:Landroid/widget/Button;

    iget-object v0, p0, Lcom/google/android/gsf/login/AccountPreIntroUIActivity;->mYesButton:Landroid/widget/Button;

    invoke-virtual {v0, p0}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    const v0, 0x7f0b000d

    invoke-virtual {p0, v0}, Lcom/google/android/gsf/login/AccountPreIntroUIActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lcom/google/android/gsf/login/AccountPreIntroUIActivity;->mNoButton:Landroid/widget/Button;

    iget-object v0, p0, Lcom/google/android/gsf/login/AccountPreIntroUIActivity;->mNoButton:Landroid/widget/Button;

    invoke-virtual {v0, p0}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    const v0, 0x7f0b000b

    invoke-virtual {p0, v0}, Lcom/google/android/gsf/login/AccountPreIntroUIActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/google/android/gsf/login/AccountPreIntroUIActivity;->mMessageTextView:Landroid/widget/TextView;

    iget-object v0, p0, Lcom/google/android/gsf/login/AccountPreIntroUIActivity;->mMessageTextView:Landroid/widget/TextView;

    const v1, 0x7f08009e

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    invoke-virtual {p0}, Lcom/google/android/gsf/login/AccountPreIntroUIActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v5, 0x7f0800c6

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-virtual {p0, v1, v2}, Lcom/google/android/gsf/login/AccountPreIntroUIActivity;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    return-void
.end method


# virtual methods
.method protected disableBackKey()Z
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public onActivityResult(IILandroid/content/Intent;)V
    .locals 0
    .param p1    # I
    .param p2    # I
    .param p3    # Landroid/content/Intent;

    packed-switch p1, :pswitch_data_0

    invoke-super {p0, p1, p2, p3}, Lcom/google/android/gsf/login/BaseActivity;->onActivityResult(IILandroid/content/Intent;)V

    :goto_0
    return-void

    :pswitch_0
    invoke-virtual {p0, p2}, Lcom/google/android/gsf/login/AccountPreIntroUIActivity;->setResult(I)V

    invoke-virtual {p0}, Lcom/google/android/gsf/login/AccountPreIntroUIActivity;->finish()V

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x40c
        :pswitch_0
    .end packed-switch
.end method

.method public onBackPressed()V
    .locals 1

    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/google/android/gsf/login/AccountPreIntroUIActivity;->setResult(I)V

    invoke-super {p0}, Lcom/google/android/gsf/login/BaseActivity;->onBackPressed()V

    return-void
.end method

.method public onClick(Landroid/view/View;)V
    .locals 3

    iget-object v0, p0, Lcom/google/android/gsf/login/AccountPreIntroUIActivity;->mYesButton:Landroid/widget/Button;

    if-ne p1, v0, :cond_1

    const/16 v0, 0x8

    invoke-virtual {p0, v0}, Lcom/google/android/gsf/login/AccountPreIntroUIActivity;->setResult(I)V

    invoke-virtual {p0}, Lcom/google/android/gsf/login/AccountPreIntroUIActivity;->finish()V

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v0, p0, Lcom/google/android/gsf/login/AccountPreIntroUIActivity;->mNoButton:Landroid/widget/Button;

    if-ne p1, v0, :cond_0

    iget-object v0, p0, Lcom/google/android/gsf/login/AccountPreIntroUIActivity;->mSession:Lcom/google/android/gsf/loginservice/GLSUser$GLSSession;

    iget-object v0, v0, Lcom/google/android/gsf/loginservice/GLSUser$GLSSession;->mAccountManagerOptions:Landroid/os/Bundle;

    const-string v1, "allowSkip"

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    if-eqz v0, :cond_2

    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/google/android/gsf/login/AccountIntroUIActivity;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const/16 v1, 0x40c

    invoke-virtual {p0, v0, v1}, Lcom/google/android/gsf/login/AccountPreIntroUIActivity;->startActivityForResult(Landroid/content/Intent;I)V

    goto :goto_0

    :cond_2
    const/16 v0, 0x9

    invoke-virtual {p0, v0}, Lcom/google/android/gsf/login/AccountPreIntroUIActivity;->setResult(I)V

    invoke-virtual {p0}, Lcom/google/android/gsf/login/AccountPreIntroUIActivity;->finish()V

    goto :goto_0
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 0
    .param p1    # Landroid/os/Bundle;

    invoke-super {p0, p1}, Lcom/google/android/gsf/login/BaseActivity;->onCreate(Landroid/os/Bundle;)V

    invoke-direct {p0}, Lcom/google/android/gsf/login/AccountPreIntroUIActivity;->initView()V

    invoke-virtual {p0}, Lcom/google/android/gsf/login/AccountPreIntroUIActivity;->overrideAllowBackHardkey()V

    return-void
.end method
