.class public Lcom/google/android/gsf/login/PlusFaqActivity;
.super Lcom/google/android/gsf/login/BaseActivity;
.source "PlusFaqActivity.java"


# instance fields
.field mNextButton:Landroid/view/View;


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Lcom/google/android/gsf/login/BaseActivity;-><init>()V

    return-void
.end method


# virtual methods
.method protected onCreate(Landroid/os/Bundle;)V
    .locals 12
    .param p1    # Landroid/os/Bundle;

    const v11, 0x7f080034

    const/4 v7, 0x2

    const/4 v10, 0x1

    const/4 v9, 0x0

    invoke-super {p0, p1}, Lcom/google/android/gsf/login/BaseActivity;->onCreate(Landroid/os/Bundle;)V

    const v6, 0x7f030018

    invoke-virtual {p0, v6}, Lcom/google/android/gsf/login/PlusFaqActivity;->setContentView(I)V

    const v6, 0x7f0b000a

    invoke-virtual {p0, v6}, Lcom/google/android/gsf/login/PlusFaqActivity;->findViewById(I)Landroid/view/View;

    move-result-object v6

    iput-object v6, p0, Lcom/google/android/gsf/login/PlusFaqActivity;->mNextButton:Landroid/view/View;

    const v6, 0x7f0b0013

    invoke-virtual {p0, v6}, Lcom/google/android/gsf/login/PlusFaqActivity;->findViewById(I)Landroid/view/View;

    move-result-object v6

    invoke-virtual {p0, v6}, Lcom/google/android/gsf/login/PlusFaqActivity;->setBackButton(Landroid/view/View;)V

    iget-object v6, p0, Lcom/google/android/gsf/login/PlusFaqActivity;->mNextButton:Landroid/view/View;

    invoke-virtual {p0, v6, v10}, Lcom/google/android/gsf/login/PlusFaqActivity;->setDefaultButton(Landroid/view/View;Z)V

    const v6, 0x7f0b000e

    invoke-virtual {p0, v6}, Lcom/google/android/gsf/login/PlusFaqActivity;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Lcom/google/android/gsf/login/BottomScrollView;

    if-eqz v4, :cond_0

    invoke-virtual {v4, p0}, Lcom/google/android/gsf/login/BottomScrollView;->setBottomScrollListener(Lcom/google/android/gsf/login/BottomScrollView$BottomScrollListener;)V

    iget-object v6, p0, Lcom/google/android/gsf/login/PlusFaqActivity;->mNextButton:Landroid/view/View;

    invoke-virtual {v6, v9}, Landroid/view/View;->setEnabled(Z)V

    :cond_0
    const v6, 0x7f0b000f

    invoke-virtual {p0, v6}, Lcom/google/android/gsf/login/PlusFaqActivity;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/TextView;

    invoke-virtual {p0}, Lcom/google/android/gsf/login/PlusFaqActivity;->shouldDisplayLastNameFirst()Z

    move-result v6

    if-eqz v6, :cond_2

    new-array v6, v7, [Ljava/lang/Object;

    invoke-virtual {p0}, Lcom/google/android/gsf/login/PlusFaqActivity;->getUserData()Ljava/util/HashMap;

    move-result-object v7

    sget-object v8, Lcom/google/android/gsf/login/BackendStub$Key;->LAST_NAME:Lcom/google/android/gsf/login/BackendStub$Key;

    invoke-virtual {v8}, Lcom/google/android/gsf/login/BackendStub$Key;->getWire()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v7

    aput-object v7, v6, v9

    invoke-virtual {p0}, Lcom/google/android/gsf/login/PlusFaqActivity;->getUserData()Ljava/util/HashMap;

    move-result-object v7

    sget-object v8, Lcom/google/android/gsf/login/BackendStub$Key;->FIRST_NAME:Lcom/google/android/gsf/login/BackendStub$Key;

    invoke-virtual {v8}, Lcom/google/android/gsf/login/BackendStub$Key;->getWire()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v7

    aput-object v7, v6, v10

    invoke-virtual {p0, v11, v6}, Lcom/google/android/gsf/login/PlusFaqActivity;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v3, v6}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    :goto_0
    const v6, 0x7f0b003e

    invoke-virtual {p0, v6}, Lcom/google/android/gsf/login/PlusFaqActivity;->findViewById(I)Landroid/view/View;

    move-result-object v5

    check-cast v5, Landroid/widget/TextView;

    const/4 v1, 0x0

    iget-object v6, p0, Lcom/google/android/gsf/login/PlusFaqActivity;->mSession:Lcom/google/android/gsf/loginservice/GLSUser$GLSSession;

    iget-boolean v6, v6, Lcom/google/android/gsf/loginservice/GLSUser$GLSSession;->mCreatingAccount:Z

    if-nez v6, :cond_1

    iget-object v6, p0, Lcom/google/android/gsf/login/PlusFaqActivity;->mGlsUser:Lcom/google/android/gsf/loginservice/GLSUser;

    sget-object v7, Lcom/google/android/gsf/loginservice/GLSUser$ResponseKey;->SERVICE_HOSTED:Lcom/google/android/gsf/loginservice/GLSUser$ResponseKey;

    invoke-virtual {v7}, Lcom/google/android/gsf/loginservice/GLSUser$ResponseKey;->getWire()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Lcom/google/android/gsf/loginservice/GLSUser;->hasService(Ljava/lang/String;)Z

    move-result v6

    if-eqz v6, :cond_1

    iget-object v6, p0, Lcom/google/android/gsf/login/PlusFaqActivity;->mSession:Lcom/google/android/gsf/loginservice/GLSUser$GLSSession;

    iget-object v2, v6, Lcom/google/android/gsf/loginservice/GLSUser$GLSSession;->mUsername:Ljava/lang/String;

    if-eqz v2, :cond_1

    const/16 v6, 0x40

    invoke-virtual {v2, v6}, Ljava/lang/String;->indexOf(I)I

    move-result v0

    if-ltz v0, :cond_1

    add-int/lit8 v6, v0, 0x1

    invoke-virtual {v2, v6}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v1

    :cond_1
    if-eqz v1, :cond_3

    const v6, 0x7f080035

    new-array v7, v10, [Ljava/lang/Object;

    aput-object v1, v7, v9

    invoke-virtual {p0, v6, v7}, Lcom/google/android/gsf/login/PlusFaqActivity;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    :goto_1
    return-void

    :cond_2
    new-array v6, v7, [Ljava/lang/Object;

    invoke-virtual {p0}, Lcom/google/android/gsf/login/PlusFaqActivity;->getUserData()Ljava/util/HashMap;

    move-result-object v7

    sget-object v8, Lcom/google/android/gsf/login/BackendStub$Key;->FIRST_NAME:Lcom/google/android/gsf/login/BackendStub$Key;

    invoke-virtual {v8}, Lcom/google/android/gsf/login/BackendStub$Key;->getWire()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v7

    aput-object v7, v6, v9

    invoke-virtual {p0}, Lcom/google/android/gsf/login/PlusFaqActivity;->getUserData()Ljava/util/HashMap;

    move-result-object v7

    sget-object v8, Lcom/google/android/gsf/login/BackendStub$Key;->LAST_NAME:Lcom/google/android/gsf/login/BackendStub$Key;

    invoke-virtual {v8}, Lcom/google/android/gsf/login/BackendStub$Key;->getWire()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v7

    aput-object v7, v6, v10

    invoke-virtual {p0, v11, v6}, Lcom/google/android/gsf/login/PlusFaqActivity;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v3, v6}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0

    :cond_3
    const/16 v6, 0x8

    invoke-virtual {v5, v6}, Landroid/widget/TextView;->setVisibility(I)V

    goto :goto_1
.end method

.method public onScrolledToBottom()V
    .locals 2

    invoke-super {p0}, Lcom/google/android/gsf/login/BaseActivity;->onScrolledToBottom()V

    iget-object v0, p0, Lcom/google/android/gsf/login/PlusFaqActivity;->mNextButton:Landroid/view/View;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/view/View;->setEnabled(Z)V

    return-void
.end method

.method public start()V
    .locals 1

    invoke-super {p0}, Lcom/google/android/gsf/login/BaseActivity;->start()V

    const/4 v0, -0x1

    invoke-virtual {p0, v0}, Lcom/google/android/gsf/login/PlusFaqActivity;->setResult(I)V

    invoke-virtual {p0}, Lcom/google/android/gsf/login/PlusFaqActivity;->finish()V

    return-void
.end method
