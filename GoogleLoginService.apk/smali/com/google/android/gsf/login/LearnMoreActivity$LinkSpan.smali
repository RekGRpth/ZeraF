.class Lcom/google/android/gsf/login/LearnMoreActivity$LinkSpan;
.super Landroid/text/style/ClickableSpan;
.source "LearnMoreActivity.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/gsf/login/LearnMoreActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "LinkSpan"
.end annotation


# instance fields
.field private mMsgId:I

.field private mParent:Lcom/google/android/gsf/login/BaseActivity;

.field private mTitleId:I


# direct methods
.method constructor <init>(Lcom/google/android/gsf/login/BaseActivity;II)V
    .locals 0
    .param p1    # Lcom/google/android/gsf/login/BaseActivity;
    .param p2    # I
    .param p3    # I

    invoke-direct {p0}, Landroid/text/style/ClickableSpan;-><init>()V

    iput-object p1, p0, Lcom/google/android/gsf/login/LearnMoreActivity$LinkSpan;->mParent:Lcom/google/android/gsf/login/BaseActivity;

    iput p2, p0, Lcom/google/android/gsf/login/LearnMoreActivity$LinkSpan;->mTitleId:I

    iput p3, p0, Lcom/google/android/gsf/login/LearnMoreActivity$LinkSpan;->mMsgId:I

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 3
    .param p1    # Landroid/view/View;

    new-instance v0, Landroid/content/Intent;

    iget-object v1, p0, Lcom/google/android/gsf/login/LearnMoreActivity$LinkSpan;->mParent:Lcom/google/android/gsf/login/BaseActivity;

    const-class v2, Lcom/google/android/gsf/login/LearnMoreActivity;

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const-string v1, "title_id"

    iget v2, p0, Lcom/google/android/gsf/login/LearnMoreActivity$LinkSpan;->mTitleId:I

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    const-string v1, "msg_id"

    iget v2, p0, Lcom/google/android/gsf/login/LearnMoreActivity$LinkSpan;->mMsgId:I

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    iget-object v1, p0, Lcom/google/android/gsf/login/LearnMoreActivity$LinkSpan;->mParent:Lcom/google/android/gsf/login/BaseActivity;

    invoke-virtual {v1, v0}, Lcom/google/android/gsf/login/BaseActivity;->startActivity(Landroid/content/Intent;)V

    return-void
.end method
