.class public Lcom/google/android/gsf/login/BadNameActivity;
.super Lcom/google/android/gsf/login/BaseActivity;
.source "BadNameActivity.java"

# interfaces
.implements Landroid/view/View$OnClickListener;
.implements Landroid/widget/CompoundButton$OnCheckedChangeListener;


# instance fields
.field private mAgreeToReview:Landroid/widget/CheckBox;

.field private mBackButton:Landroid/widget/Button;

.field private mNextButton:Landroid/widget/Button;


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Lcom/google/android/gsf/login/BaseActivity;-><init>()V

    return-void
.end method

.method private initViews()V
    .locals 9

    const v8, 0x7f080040

    const/4 v4, 0x2

    const/4 v7, 0x1

    const/4 v6, 0x0

    const v3, 0x7f0b000a

    invoke-virtual {p0, v3}, Lcom/google/android/gsf/login/BadNameActivity;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/Button;

    iput-object v3, p0, Lcom/google/android/gsf/login/BadNameActivity;->mNextButton:Landroid/widget/Button;

    iget-object v3, p0, Lcom/google/android/gsf/login/BadNameActivity;->mNextButton:Landroid/widget/Button;

    invoke-virtual {v3, p0}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    const v3, 0x7f0b0013

    invoke-virtual {p0, v3}, Lcom/google/android/gsf/login/BadNameActivity;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/Button;

    iput-object v3, p0, Lcom/google/android/gsf/login/BadNameActivity;->mBackButton:Landroid/widget/Button;

    iget-object v3, p0, Lcom/google/android/gsf/login/BadNameActivity;->mBackButton:Landroid/widget/Button;

    invoke-virtual {v3, p0}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    const v3, 0x7f0b0015

    invoke-virtual {p0, v3}, Lcom/google/android/gsf/login/BadNameActivity;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/CheckBox;

    iput-object v3, p0, Lcom/google/android/gsf/login/BadNameActivity;->mAgreeToReview:Landroid/widget/CheckBox;

    const v3, 0x7f0b0014

    invoke-virtual {p0, v3}, Lcom/google/android/gsf/login/BadNameActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    invoke-virtual {p0}, Lcom/google/android/gsf/login/BadNameActivity;->shouldDisplayLastNameFirst()Z

    move-result v3

    if-eqz v3, :cond_1

    new-array v3, v4, [Ljava/lang/Object;

    invoke-virtual {p0}, Lcom/google/android/gsf/login/BadNameActivity;->getUserData()Ljava/util/HashMap;

    move-result-object v4

    sget-object v5, Lcom/google/android/gsf/login/BackendStub$Key;->LAST_NAME:Lcom/google/android/gsf/login/BackendStub$Key;

    invoke-virtual {v5}, Lcom/google/android/gsf/login/BackendStub$Key;->getWire()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v4

    aput-object v4, v3, v6

    invoke-virtual {p0}, Lcom/google/android/gsf/login/BadNameActivity;->getUserData()Ljava/util/HashMap;

    move-result-object v4

    sget-object v5, Lcom/google/android/gsf/login/BackendStub$Key;->FIRST_NAME:Lcom/google/android/gsf/login/BackendStub$Key;

    invoke-virtual {v5}, Lcom/google/android/gsf/login/BackendStub$Key;->getWire()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v4

    aput-object v4, v3, v7

    invoke-virtual {p0, v8, v3}, Lcom/google/android/gsf/login/BadNameActivity;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    :goto_0
    const v3, 0x7f0b000f

    invoke-virtual {p0, v3}, Lcom/google/android/gsf/login/BadNameActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    const v3, 0x7f0b0002

    invoke-virtual {p0, v3}, Lcom/google/android/gsf/login/BadNameActivity;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    iget-object v3, p0, Lcom/google/android/gsf/login/BadNameActivity;->mSession:Lcom/google/android/gsf/loginservice/GLSUser$GLSSession;

    iget-object v3, v3, Lcom/google/android/gsf/loginservice/GLSUser$GLSSession;->mError:Lcom/google/android/gsf/loginservice/GLSUser$Status;

    sget-object v4, Lcom/google/android/gsf/loginservice/GLSUser$Status;->GPLUS_INTERSTITIAL:Lcom/google/android/gsf/loginservice/GLSUser$Status;

    if-eq v3, v4, :cond_4

    iget-object v3, p0, Lcom/google/android/gsf/login/BadNameActivity;->mSession:Lcom/google/android/gsf/loginservice/GLSUser$GLSSession;

    iget-object v3, v3, Lcom/google/android/gsf/loginservice/GLSUser$GLSSession;->mError:Lcom/google/android/gsf/loginservice/GLSUser$Status;

    sget-object v4, Lcom/google/android/gsf/loginservice/GLSUser$Status;->GPLUS_INVALID_CHAR:Lcom/google/android/gsf/loginservice/GLSUser$Status;

    if-ne v3, v4, :cond_2

    const v3, 0x7f080042

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setText(I)V

    :cond_0
    :goto_1
    const v3, 0x7f08003f

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(I)V

    iget-object v3, p0, Lcom/google/android/gsf/login/BadNameActivity;->mAgreeToReview:Landroid/widget/CheckBox;

    const/16 v4, 0x8

    invoke-virtual {v3, v4}, Landroid/widget/CheckBox;->setVisibility(I)V

    :goto_2
    return-void

    :cond_1
    new-array v3, v4, [Ljava/lang/Object;

    invoke-virtual {p0}, Lcom/google/android/gsf/login/BadNameActivity;->getUserData()Ljava/util/HashMap;

    move-result-object v4

    sget-object v5, Lcom/google/android/gsf/login/BackendStub$Key;->FIRST_NAME:Lcom/google/android/gsf/login/BackendStub$Key;

    invoke-virtual {v5}, Lcom/google/android/gsf/login/BackendStub$Key;->getWire()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v4

    aput-object v4, v3, v6

    invoke-virtual {p0}, Lcom/google/android/gsf/login/BadNameActivity;->getUserData()Ljava/util/HashMap;

    move-result-object v4

    sget-object v5, Lcom/google/android/gsf/login/BackendStub$Key;->LAST_NAME:Lcom/google/android/gsf/login/BackendStub$Key;

    invoke-virtual {v5}, Lcom/google/android/gsf/login/BackendStub$Key;->getWire()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v4

    aput-object v4, v3, v7

    invoke-virtual {p0, v8, v3}, Lcom/google/android/gsf/login/BadNameActivity;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0

    :cond_2
    iget-object v3, p0, Lcom/google/android/gsf/login/BadNameActivity;->mSession:Lcom/google/android/gsf/loginservice/GLSUser$GLSSession;

    iget-object v3, v3, Lcom/google/android/gsf/loginservice/GLSUser$GLSSession;->mError:Lcom/google/android/gsf/loginservice/GLSUser$Status;

    sget-object v4, Lcom/google/android/gsf/loginservice/GLSUser$Status;->GPLUS_NICKNAME:Lcom/google/android/gsf/loginservice/GLSUser$Status;

    if-ne v3, v4, :cond_3

    const v3, 0x7f080043

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setText(I)V

    goto :goto_1

    :cond_3
    iget-object v3, p0, Lcom/google/android/gsf/login/BadNameActivity;->mSession:Lcom/google/android/gsf/loginservice/GLSUser$GLSSession;

    iget-object v3, v3, Lcom/google/android/gsf/loginservice/GLSUser$GLSSession;->mError:Lcom/google/android/gsf/loginservice/GLSUser$Status;

    sget-object v4, Lcom/google/android/gsf/loginservice/GLSUser$Status;->GPLUS_OTHER:Lcom/google/android/gsf/loginservice/GLSUser$Status;

    if-ne v3, v4, :cond_0

    const v3, 0x7f080044

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setText(I)V

    goto :goto_1

    :cond_4
    iget-object v3, p0, Lcom/google/android/gsf/login/BadNameActivity;->mAgreeToReview:Landroid/widget/CheckBox;

    invoke-virtual {v3, p0}, Landroid/widget/CheckBox;->setOnCheckedChangeListener(Landroid/widget/CompoundButton$OnCheckedChangeListener;)V

    const v3, 0x7f080041

    const v4, 0x7f0800ad

    const v5, 0x7f0800ac

    invoke-static {p0, v0, v3, v4, v5}, Lcom/google/android/gsf/login/LearnMoreActivity;->linkifyAndSetText(Lcom/google/android/gsf/login/BaseActivity;Landroid/widget/TextView;III)V

    iget-object v3, p0, Lcom/google/android/gsf/login/BadNameActivity;->mNextButton:Landroid/widget/Button;

    const v4, 0x7f080047

    invoke-virtual {v3, v4}, Landroid/widget/Button;->setText(I)V

    iget-object v3, p0, Lcom/google/android/gsf/login/BadNameActivity;->mNextButton:Landroid/widget/Button;

    invoke-virtual {v3, v6}, Landroid/widget/Button;->setEnabled(Z)V

    goto :goto_2
.end method


# virtual methods
.method public onCheckedChanged(Landroid/widget/CompoundButton;Z)V
    .locals 1
    .param p1    # Landroid/widget/CompoundButton;
    .param p2    # Z

    iget-object v0, p0, Lcom/google/android/gsf/login/BadNameActivity;->mNextButton:Landroid/widget/Button;

    invoke-virtual {v0, p2}, Landroid/widget/Button;->setEnabled(Z)V

    return-void
.end method

.method public onClick(Landroid/view/View;)V
    .locals 3
    .param p1    # Landroid/view/View;

    const/4 v2, 0x0

    iget-object v0, p0, Lcom/google/android/gsf/login/BadNameActivity;->mBackButton:Landroid/widget/Button;

    if-ne p1, v0, :cond_1

    invoke-virtual {p0, v2}, Lcom/google/android/gsf/login/BadNameActivity;->setResult(I)V

    invoke-virtual {p0}, Lcom/google/android/gsf/login/BadNameActivity;->finish()V

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v0, p0, Lcom/google/android/gsf/login/BadNameActivity;->mNextButton:Landroid/widget/Button;

    if-ne p1, v0, :cond_0

    iget-object v0, p0, Lcom/google/android/gsf/login/BadNameActivity;->mSession:Lcom/google/android/gsf/loginservice/GLSUser$GLSSession;

    iget-object v0, v0, Lcom/google/android/gsf/loginservice/GLSUser$GLSSession;->mError:Lcom/google/android/gsf/loginservice/GLSUser$Status;

    sget-object v1, Lcom/google/android/gsf/loginservice/GLSUser$Status;->GPLUS_INTERSTITIAL:Lcom/google/android/gsf/loginservice/GLSUser$Status;

    if-eq v0, v1, :cond_2

    iget-object v0, p0, Lcom/google/android/gsf/login/BadNameActivity;->mSession:Lcom/google/android/gsf/loginservice/GLSUser$GLSSession;

    iput-boolean v2, v0, Lcom/google/android/gsf/loginservice/GLSUser$GLSSession;->mUserSelectedGooglePlus:Z

    :cond_2
    const/4 v0, -0x1

    invoke-virtual {p0, v0}, Lcom/google/android/gsf/login/BadNameActivity;->setResult(I)V

    invoke-virtual {p0}, Lcom/google/android/gsf/login/BadNameActivity;->finish()V

    goto :goto_0
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 1
    .param p1    # Landroid/os/Bundle;

    invoke-super {p0, p1}, Lcom/google/android/gsf/login/BaseActivity;->onCreate(Landroid/os/Bundle;)V

    const v0, 0x7f030005

    invoke-virtual {p0, v0}, Lcom/google/android/gsf/login/BadNameActivity;->setContentView(I)V

    invoke-direct {p0}, Lcom/google/android/gsf/login/BadNameActivity;->initViews()V

    return-void
.end method
