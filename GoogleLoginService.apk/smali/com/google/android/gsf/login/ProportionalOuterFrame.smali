.class public Lcom/google/android/gsf/login/ProportionalOuterFrame;
.super Landroid/widget/RelativeLayout;
.source "ProportionalOuterFrame.java"


# instance fields
.field private mSideMargin:I

.field private mTitleHeight:I


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0
    .param p1    # Landroid/content/Context;

    invoke-direct {p0, p1}, Landroid/widget/RelativeLayout;-><init>(Landroid/content/Context;)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/util/AttributeSet;

    invoke-direct {p0, p1, p2}, Landroid/widget/RelativeLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 0
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/util/AttributeSet;
    .param p3    # I

    invoke-direct {p0, p1, p2, p3}, Landroid/widget/RelativeLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    return-void
.end method


# virtual methods
.method public onConfigurationChanged(Landroid/content/res/Configuration;)V
    .locals 1
    .param p1    # Landroid/content/res/Configuration;

    const/4 v0, 0x0

    invoke-super {p0, p1}, Landroid/widget/RelativeLayout;->onConfigurationChanged(Landroid/content/res/Configuration;)V

    iput v0, p0, Lcom/google/android/gsf/login/ProportionalOuterFrame;->mTitleHeight:I

    iput v0, p0, Lcom/google/android/gsf/login/ProportionalOuterFrame;->mSideMargin:I

    return-void
.end method

.method protected onMeasure(II)V
    .locals 9
    .param p1    # I
    .param p2    # I

    const/4 v8, 0x1

    invoke-virtual {p0}, Lcom/google/android/gsf/login/ProportionalOuterFrame;->getContext()Landroid/content/Context;

    move-result-object v5

    invoke-virtual {v5}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    invoke-static {p1}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v2

    invoke-static {p2}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v1

    iget v5, p0, Lcom/google/android/gsf/login/ProportionalOuterFrame;->mTitleHeight:I

    int-to-float v6, v1

    const v7, 0x7f070005

    invoke-virtual {v3, v7, v8, v8}, Landroid/content/res/Resources;->getFraction(III)F

    move-result v7

    mul-float/2addr v6, v7

    float-to-int v6, v6

    invoke-static {v5, v6}, Ljava/lang/Math;->max(II)I

    move-result v5

    iput v5, p0, Lcom/google/android/gsf/login/ProportionalOuterFrame;->mTitleHeight:I

    iget v5, p0, Lcom/google/android/gsf/login/ProportionalOuterFrame;->mSideMargin:I

    int-to-float v6, v2

    const v7, 0x7f070006

    invoke-virtual {v3, v7, v8, v8}, Landroid/content/res/Resources;->getFraction(III)F

    move-result v7

    mul-float/2addr v6, v7

    float-to-int v6, v6

    invoke-static {v5, v6}, Ljava/lang/Math;->max(II)I

    move-result v5

    iput v5, p0, Lcom/google/android/gsf/login/ProportionalOuterFrame;->mSideMargin:I

    const v5, 0x7f070007

    invoke-virtual {v3, v5}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    iget v5, p0, Lcom/google/android/gsf/login/ProportionalOuterFrame;->mSideMargin:I

    const/4 v6, 0x0

    iget v7, p0, Lcom/google/android/gsf/login/ProportionalOuterFrame;->mSideMargin:I

    invoke-virtual {p0, v5, v6, v7, v0}, Lcom/google/android/gsf/login/ProportionalOuterFrame;->setPadding(IIII)V

    const/high16 v5, 0x7f0b0000

    invoke-virtual {p0, v5}, Lcom/google/android/gsf/login/ProportionalOuterFrame;->findViewById(I)Landroid/view/View;

    move-result-object v4

    if-eqz v4, :cond_0

    iget v5, p0, Lcom/google/android/gsf/login/ProportionalOuterFrame;->mTitleHeight:I

    invoke-virtual {v4, v5}, Landroid/view/View;->setMinimumHeight(I)V

    :cond_0
    invoke-super {p0, p1, p2}, Landroid/widget/RelativeLayout;->onMeasure(II)V

    return-void
.end method
