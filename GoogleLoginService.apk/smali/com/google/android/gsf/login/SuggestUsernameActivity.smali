.class public Lcom/google/android/gsf/login/SuggestUsernameActivity;
.super Lcom/google/android/gsf/login/BaseActivity;
.source "SuggestUsernameActivity.java"

# interfaces
.implements Landroid/content/DialogInterface$OnClickListener;
.implements Landroid/view/View$OnClickListener;
.implements Landroid/view/View$OnFocusChangeListener;


# instance fields
.field private mAlternativesButton:Landroid/widget/Button;

.field private mCurrentSuggestion:Ljava/lang/String;

.field private mCurrentSuggestionPosition:I

.field private mDetailMessage:Landroid/widget/TextView;

.field private mNextButton:Landroid/view/View;

.field private mSkipButton:Landroid/view/View;

.field private mSuggestionsAdapter:Landroid/widget/ArrayAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/widget/ArrayAdapter",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field protected mUsernameEdit:Landroid/widget/EditText;

.field private mUsernameError:Z


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Lcom/google/android/gsf/login/BaseActivity;-><init>()V

    return-void
.end method

.method static synthetic access$002(Lcom/google/android/gsf/login/SuggestUsernameActivity;Z)Z
    .locals 0
    .param p0    # Lcom/google/android/gsf/login/SuggestUsernameActivity;
    .param p1    # Z

    iput-boolean p1, p0, Lcom/google/android/gsf/login/SuggestUsernameActivity;->mUsernameError:Z

    return p1
.end method


# virtual methods
.method protected getContentView()I
    .locals 1

    const v0, 0x7f03001b

    return v0
.end method

.method protected initViews()V
    .locals 11

    const/4 v10, 0x0

    const/4 v9, 0x1

    iget-object v6, p0, Lcom/google/android/gsf/login/SuggestUsernameActivity;->mSession:Lcom/google/android/gsf/loginservice/GLSUser$GLSSession;

    iget-object v6, v6, Lcom/google/android/gsf/loginservice/GLSUser$GLSSession;->mUsername:Ljava/lang/String;

    invoke-static {v6}, Lcom/google/android/gsf/login/BackendStub;->usernameOnly(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    const v6, 0x7f0b0020

    invoke-virtual {p0, v6}, Lcom/google/android/gsf/login/SuggestUsernameActivity;->findViewById(I)Landroid/view/View;

    move-result-object v6

    check-cast v6, Landroid/widget/EditText;

    iput-object v6, p0, Lcom/google/android/gsf/login/SuggestUsernameActivity;->mUsernameEdit:Landroid/widget/EditText;

    iget-object v6, p0, Lcom/google/android/gsf/login/SuggestUsernameActivity;->mUsernameEdit:Landroid/widget/EditText;

    invoke-virtual {v6, v5}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    iget-object v6, p0, Lcom/google/android/gsf/login/SuggestUsernameActivity;->mUsernameEdit:Landroid/widget/EditText;

    invoke-virtual {v6, p0}, Landroid/widget/EditText;->addTextChangedListener(Landroid/text/TextWatcher;)V

    iget-object v6, p0, Lcom/google/android/gsf/login/SuggestUsernameActivity;->mUsernameEdit:Landroid/widget/EditText;

    invoke-virtual {v6, p0}, Landroid/widget/EditText;->setOnFocusChangeListener(Landroid/view/View$OnFocusChangeListener;)V

    iget-object v6, p0, Lcom/google/android/gsf/login/SuggestUsernameActivity;->mUsernameEdit:Landroid/widget/EditText;

    new-array v7, v9, [Landroid/text/InputFilter;

    new-instance v8, Lcom/google/android/gsf/login/SuggestUsernameActivity$1;

    invoke-direct {v8, p0, v9}, Lcom/google/android/gsf/login/SuggestUsernameActivity$1;-><init>(Lcom/google/android/gsf/login/SuggestUsernameActivity;Z)V

    aput-object v8, v7, v10

    invoke-virtual {v6, v7}, Landroid/widget/EditText;->setFilters([Landroid/text/InputFilter;)V

    iget-object v6, p0, Lcom/google/android/gsf/login/SuggestUsernameActivity;->mUsernameEdit:Landroid/widget/EditText;

    invoke-virtual {v6}, Landroid/widget/EditText;->requestFocus()Z

    iget-object v6, p0, Lcom/google/android/gsf/login/SuggestUsernameActivity;->mUsernameEdit:Landroid/widget/EditText;

    invoke-virtual {p0, v6, v10}, Lcom/google/android/gsf/login/SuggestUsernameActivity;->setDefaultButton(Landroid/view/View;Z)V

    const v6, 0x7f0b0046

    invoke-virtual {p0, v6}, Lcom/google/android/gsf/login/SuggestUsernameActivity;->findViewById(I)Landroid/view/View;

    move-result-object v6

    check-cast v6, Landroid/widget/Button;

    iput-object v6, p0, Lcom/google/android/gsf/login/SuggestUsernameActivity;->mAlternativesButton:Landroid/widget/Button;

    iget-object v6, p0, Lcom/google/android/gsf/login/SuggestUsernameActivity;->mAlternativesButton:Landroid/widget/Button;

    invoke-virtual {v6, p0}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    const v6, 0x7f0b000a

    invoke-virtual {p0, v6}, Lcom/google/android/gsf/login/SuggestUsernameActivity;->findViewById(I)Landroid/view/View;

    move-result-object v6

    iput-object v6, p0, Lcom/google/android/gsf/login/SuggestUsernameActivity;->mNextButton:Landroid/view/View;

    iget-object v6, p0, Lcom/google/android/gsf/login/SuggestUsernameActivity;->mNextButton:Landroid/view/View;

    invoke-virtual {p0, v6, v9}, Lcom/google/android/gsf/login/SuggestUsernameActivity;->setDefaultButton(Landroid/view/View;Z)V

    const v6, 0x7f0b0008

    invoke-virtual {p0, v6}, Lcom/google/android/gsf/login/SuggestUsernameActivity;->findViewById(I)Landroid/view/View;

    move-result-object v6

    iput-object v6, p0, Lcom/google/android/gsf/login/SuggestUsernameActivity;->mSkipButton:Landroid/view/View;

    iget-object v6, p0, Lcom/google/android/gsf/login/SuggestUsernameActivity;->mSkipButton:Landroid/view/View;

    invoke-virtual {v6, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    const v6, 0x7f0b0045

    invoke-virtual {p0, v6}, Lcom/google/android/gsf/login/SuggestUsernameActivity;->findViewById(I)Landroid/view/View;

    move-result-object v6

    check-cast v6, Landroid/widget/TextView;

    iput-object v6, p0, Lcom/google/android/gsf/login/SuggestUsernameActivity;->mDetailMessage:Landroid/widget/TextView;

    iget-object v6, p0, Lcom/google/android/gsf/login/SuggestUsernameActivity;->mDetailMessage:Landroid/widget/TextView;

    if-eqz v6, :cond_2

    invoke-virtual {p0}, Lcom/google/android/gsf/login/SuggestUsernameActivity;->getUserData()Ljava/util/HashMap;

    move-result-object v6

    sget-object v7, Lcom/google/android/gsf/login/BackendStub$Key;->DETAIL:Lcom/google/android/gsf/login/BackendStub$Key;

    invoke-virtual {v7}, Lcom/google/android/gsf/login/BackendStub$Key;->getWire()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/String;

    const/4 v1, 0x0

    invoke-static {v4}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v6

    if-nez v6, :cond_0

    :try_start_0
    invoke-static {v4}, Lcom/google/android/gsf/login/BackendStub$Detail;->valueOf(Ljava/lang/String;)Lcom/google/android/gsf/login/BackendStub$Detail;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gsf/login/BackendStub$Detail;->getMessageId()I

    move-result v6

    invoke-virtual {p0, v6}, Lcom/google/android/gsf/login/SuggestUsernameActivity;->getString(I)Ljava/lang/String;

    move-result-object v3

    const/4 v6, 0x1

    new-array v6, v6, [Ljava/lang/CharSequence;

    const/4 v7, 0x0

    aput-object v5, v6, v7

    invoke-static {v3, v6}, Landroid/text/TextUtils;->expandTemplate(Ljava/lang/CharSequence;[Ljava/lang/CharSequence;)Ljava/lang/CharSequence;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/Object;->toString()Ljava/lang/String;
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    :cond_0
    :goto_0
    if-nez v1, :cond_1

    invoke-static {}, Lcom/google/android/gsf/login/BackendStub$Detail;->defaultMessageId()I

    move-result v6

    invoke-virtual {p0, v6}, Lcom/google/android/gsf/login/SuggestUsernameActivity;->getString(I)Ljava/lang/String;

    move-result-object v6

    new-array v7, v9, [Ljava/lang/CharSequence;

    iget-object v8, p0, Lcom/google/android/gsf/login/SuggestUsernameActivity;->mSession:Lcom/google/android/gsf/loginservice/GLSUser$GLSSession;

    iget-object v8, v8, Lcom/google/android/gsf/loginservice/GLSUser$GLSSession;->mUsername:Ljava/lang/String;

    aput-object v8, v7, v10

    invoke-static {v6, v7}, Landroid/text/TextUtils;->expandTemplate(Ljava/lang/CharSequence;[Ljava/lang/CharSequence;)Ljava/lang/CharSequence;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    :cond_1
    iget-object v6, p0, Lcom/google/android/gsf/login/SuggestUsernameActivity;->mDetailMessage:Landroid/widget/TextView;

    invoke-virtual {v6, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    :cond_2
    return-void

    :catch_0
    move-exception v2

    const-string v6, "GLSActivity"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "Unknown enum "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method public onClick(Landroid/content/DialogInterface;I)V
    .locals 2
    .param p1    # Landroid/content/DialogInterface;
    .param p2    # I

    iget-object v0, p0, Lcom/google/android/gsf/login/SuggestUsernameActivity;->mSuggestionsAdapter:Landroid/widget/ArrayAdapter;

    invoke-virtual {v0, p2}, Landroid/widget/ArrayAdapter;->getItem(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iput-object v0, p0, Lcom/google/android/gsf/login/SuggestUsernameActivity;->mCurrentSuggestion:Ljava/lang/String;

    iput p2, p0, Lcom/google/android/gsf/login/SuggestUsernameActivity;->mCurrentSuggestionPosition:I

    iget-object v0, p0, Lcom/google/android/gsf/login/SuggestUsernameActivity;->mUsernameEdit:Landroid/widget/EditText;

    iget-object v1, p0, Lcom/google/android/gsf/login/SuggestUsernameActivity;->mCurrentSuggestion:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lcom/google/android/gsf/login/SuggestUsernameActivity;->mUsernameEdit:Landroid/widget/EditText;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setError(Ljava/lang/CharSequence;)V

    invoke-interface {p1}, Landroid/content/DialogInterface;->cancel()V

    invoke-virtual {p0}, Lcom/google/android/gsf/login/SuggestUsernameActivity;->updateWidgetState()V

    return-void
.end method

.method public onClick(Landroid/view/View;)V
    .locals 3
    .param p1    # Landroid/view/View;

    iget-object v1, p0, Lcom/google/android/gsf/login/SuggestUsernameActivity;->mAlternativesButton:Landroid/widget/Button;

    if-ne p1, v1, :cond_1

    new-instance v0, Landroid/app/AlertDialog$Builder;

    invoke-direct {v0, p0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    const v1, 0x7f080079

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    iget-object v1, p0, Lcom/google/android/gsf/login/SuggestUsernameActivity;->mSuggestionsAdapter:Landroid/widget/ArrayAdapter;

    iget v2, p0, Lcom/google/android/gsf/login/SuggestUsernameActivity;->mCurrentSuggestionPosition:I

    invoke-virtual {v0, v1, v2, p0}, Landroid/app/AlertDialog$Builder;->setSingleChoiceItems(Landroid/widget/ListAdapter;ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->show()Landroid/app/AlertDialog;

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v1, p0, Lcom/google/android/gsf/login/SuggestUsernameActivity;->mSkipButton:Landroid/view/View;

    if-ne p1, v1, :cond_0

    const/4 v1, 0x1

    invoke-virtual {p0, v1}, Lcom/google/android/gsf/login/SuggestUsernameActivity;->setResult(I)V

    invoke-virtual {p0}, Lcom/google/android/gsf/login/SuggestUsernameActivity;->finish()V

    goto :goto_0
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 1
    .param p1    # Landroid/os/Bundle;

    invoke-super {p0, p1}, Lcom/google/android/gsf/login/BaseActivity;->onCreate(Landroid/os/Bundle;)V

    invoke-virtual {p0}, Lcom/google/android/gsf/login/SuggestUsernameActivity;->getContentView()I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/google/android/gsf/login/SuggestUsernameActivity;->setContentView(I)V

    invoke-virtual {p0}, Lcom/google/android/gsf/login/SuggestUsernameActivity;->initViews()V

    invoke-virtual {p0}, Lcom/google/android/gsf/login/SuggestUsernameActivity;->populateFields()V

    invoke-virtual {p0}, Lcom/google/android/gsf/login/SuggestUsernameActivity;->updateWidgetState()V

    iget-object v0, p0, Lcom/google/android/gsf/login/SuggestUsernameActivity;->mUsernameEdit:Landroid/widget/EditText;

    invoke-virtual {v0}, Landroid/widget/EditText;->requestFocus()Z

    return-void
.end method

.method public onFocusChange(Landroid/view/View;Z)V
    .locals 3
    .param p1    # Landroid/view/View;
    .param p2    # Z

    iget-object v1, p0, Lcom/google/android/gsf/login/SuggestUsernameActivity;->mUsernameEdit:Landroid/widget/EditText;

    if-ne p1, v1, :cond_0

    if-nez p2, :cond_0

    iget-boolean v1, p0, Lcom/google/android/gsf/login/SuggestUsernameActivity;->mUsernameError:Z

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/google/android/gsf/login/SuggestUsernameActivity;->mUsernameEdit:Landroid/widget/EditText;

    const v2, 0x7f08001e

    invoke-virtual {p0, v2}, Lcom/google/android/gsf/login/SuggestUsernameActivity;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/EditText;->setError(Ljava/lang/CharSequence;)V

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v1, p0, Lcom/google/android/gsf/login/SuggestUsernameActivity;->mUsernameEdit:Landroid/widget/EditText;

    invoke-virtual {v1}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/google/android/gsf/login/SuggestUsernameActivity;->mUsernameEdit:Landroid/widget/EditText;

    const v2, 0x7f080026

    invoke-virtual {p0, v2}, Lcom/google/android/gsf/login/SuggestUsernameActivity;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/EditText;->setError(Ljava/lang/CharSequence;)V

    goto :goto_0

    :cond_2
    invoke-virtual {p0, v0}, Lcom/google/android/gsf/login/SuggestUsernameActivity;->validateUsername(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_3

    iget-object v1, p0, Lcom/google/android/gsf/login/SuggestUsernameActivity;->mUsernameEdit:Landroid/widget/EditText;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Landroid/widget/EditText;->setError(Ljava/lang/CharSequence;)V

    goto :goto_0

    :cond_3
    iget-object v1, p0, Lcom/google/android/gsf/login/SuggestUsernameActivity;->mUsernameEdit:Landroid/widget/EditText;

    const v2, 0x7f080064

    invoke-virtual {p0, v2}, Lcom/google/android/gsf/login/SuggestUsernameActivity;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/EditText;->setError(Ljava/lang/CharSequence;)V

    goto :goto_0
.end method

.method public onResume()V
    .locals 3

    invoke-super {p0}, Lcom/google/android/gsf/login/BaseActivity;->onResume()V

    invoke-static {}, Lcom/google/android/gsf/login/SuggestUsernameActivity;->isTabletLayout()Z

    move-result v1

    if-nez v1, :cond_0

    invoke-virtual {p0}, Lcom/google/android/gsf/login/SuggestUsernameActivity;->getWindow()Landroid/view/Window;

    move-result-object v1

    const/4 v2, 0x3

    invoke-virtual {v1, v2}, Landroid/view/Window;->setSoftInputMode(I)V

    const-string v1, "input_method"

    invoke-virtual {p0, v1}, Lcom/google/android/gsf/login/SuggestUsernameActivity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/inputmethod/InputMethodManager;

    const v1, 0x7f0b0044

    invoke-virtual {p0, v1}, Lcom/google/android/gsf/login/SuggestUsernameActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    invoke-virtual {v1}, Landroid/view/View;->getWindowToken()Landroid/os/IBinder;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/view/inputmethod/InputMethodManager;->hideSoftInputFromWindow(Landroid/os/IBinder;I)Z

    :cond_0
    return-void
.end method

.method protected populateFields()V
    .locals 4

    invoke-virtual {p0}, Lcom/google/android/gsf/login/SuggestUsernameActivity;->getUserData()Ljava/util/HashMap;

    move-result-object v2

    sget-object v3, Lcom/google/android/gsf/login/BackendStub$Key;->SUGGESTIONS:Lcom/google/android/gsf/login/BackendStub$Key;

    invoke-virtual {v3}, Lcom/google/android/gsf/login/BackendStub$Key;->getWire()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, [Ljava/lang/String;

    move-object v1, v2

    check-cast v1, [Ljava/lang/String;

    if-eqz v1, :cond_1

    array-length v2, v1

    if-lez v2, :cond_1

    const/4 v0, 0x0

    :goto_0
    array-length v2, v1

    if-ge v0, v2, :cond_0

    aget-object v2, v1, v0

    invoke-static {v2}, Lcom/google/android/gsf/login/BackendStub;->usernameOnly(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    aput-object v2, v1, v0

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_0
    iget-object v2, p0, Lcom/google/android/gsf/login/SuggestUsernameActivity;->mAlternativesButton:Landroid/widget/Button;

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Landroid/widget/Button;->setVisibility(I)V

    new-instance v2, Landroid/widget/ArrayAdapter;

    const v3, 0x1090009

    invoke-direct {v2, p0, v3, v1}, Landroid/widget/ArrayAdapter;-><init>(Landroid/content/Context;I[Ljava/lang/Object;)V

    iput-object v2, p0, Lcom/google/android/gsf/login/SuggestUsernameActivity;->mSuggestionsAdapter:Landroid/widget/ArrayAdapter;

    :goto_1
    return-void

    :cond_1
    iget-object v2, p0, Lcom/google/android/gsf/login/SuggestUsernameActivity;->mAlternativesButton:Landroid/widget/Button;

    const/16 v3, 0x8

    invoke-virtual {v2, v3}, Landroid/widget/Button;->setVisibility(I)V

    goto :goto_1
.end method

.method public start()V
    .locals 5

    iget-object v2, p0, Lcom/google/android/gsf/login/SuggestUsernameActivity;->mUsernameEdit:Landroid/widget/EditText;

    invoke-virtual {v2}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_0

    const-string v2, "GLSActivity"

    const-string v3, "empty username in SuggestUsernameActivity"

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v2, p0, Lcom/google/android/gsf/login/SuggestUsernameActivity;->mUsernameEdit:Landroid/widget/EditText;

    const v3, 0x7f080026

    invoke-virtual {p0, v3}, Lcom/google/android/gsf/login/SuggestUsernameActivity;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/widget/EditText;->setError(Ljava/lang/CharSequence;)V

    :goto_0
    return-void

    :cond_0
    invoke-virtual {p0, v0}, Lcom/google/android/gsf/login/SuggestUsernameActivity;->validateUsername(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    if-nez v1, :cond_1

    const-string v2, "GLSActivity"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Can\'t create a valid email name from \'"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "\'"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v2, p0, Lcom/google/android/gsf/login/SuggestUsernameActivity;->mUsernameEdit:Landroid/widget/EditText;

    const v3, 0x7f080064

    invoke-virtual {p0, v3}, Lcom/google/android/gsf/login/SuggestUsernameActivity;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/widget/EditText;->setError(Ljava/lang/CharSequence;)V

    goto :goto_0

    :cond_1
    iget-object v2, p0, Lcom/google/android/gsf/login/SuggestUsernameActivity;->mCurrentSuggestion:Ljava/lang/String;

    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_2

    iget-object v2, p0, Lcom/google/android/gsf/login/SuggestUsernameActivity;->mCurrentSuggestion:Ljava/lang/String;

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    iget-object v2, p0, Lcom/google/android/gsf/login/SuggestUsernameActivity;->mSession:Lcom/google/android/gsf/loginservice/GLSUser$GLSSession;

    iput-object v1, v2, Lcom/google/android/gsf/loginservice/GLSUser$GLSSession;->mUsername:Ljava/lang/String;

    const/4 v2, -0x1

    invoke-virtual {p0, v2}, Lcom/google/android/gsf/login/SuggestUsernameActivity;->setResult(I)V

    invoke-virtual {p0}, Lcom/google/android/gsf/login/SuggestUsernameActivity;->finish()V

    goto :goto_0

    :cond_2
    iget-object v2, p0, Lcom/google/android/gsf/login/SuggestUsernameActivity;->mSession:Lcom/google/android/gsf/loginservice/GLSUser$GLSSession;

    iput-object v1, v2, Lcom/google/android/gsf/loginservice/GLSUser$GLSSession;->mUsername:Ljava/lang/String;

    const/4 v2, 0x3

    invoke-virtual {p0, v2}, Lcom/google/android/gsf/login/SuggestUsernameActivity;->setResult(I)V

    invoke-virtual {p0}, Lcom/google/android/gsf/login/SuggestUsernameActivity;->finish()V

    goto :goto_0
.end method

.method public updateWidgetState()V
    .locals 5

    invoke-super {p0}, Lcom/google/android/gsf/login/BaseActivity;->updateWidgetState()V

    iget-object v4, p0, Lcom/google/android/gsf/login/SuggestUsernameActivity;->mUsernameEdit:Landroid/widget/EditText;

    invoke-virtual {v4}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v2

    iget-object v4, p0, Lcom/google/android/gsf/login/SuggestUsernameActivity;->mSession:Lcom/google/android/gsf/loginservice/GLSUser$GLSSession;

    iget-object v4, v4, Lcom/google/android/gsf/loginservice/GLSUser$GLSSession;->mUsername:Ljava/lang/String;

    invoke-static {v4}, Lcom/google/android/gsf/login/BackendStub;->usernameOnly(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iget-boolean v4, p0, Lcom/google/android/gsf/login/SuggestUsernameActivity;->mUsernameError:Z

    if-nez v4, :cond_0

    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-nez v4, :cond_0

    invoke-virtual {v2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v4

    if-nez v4, :cond_0

    const/4 v3, 0x1

    :goto_0
    move v0, v3

    iget-object v4, p0, Lcom/google/android/gsf/login/SuggestUsernameActivity;->mNextButton:Landroid/view/View;

    invoke-virtual {v4, v0}, Landroid/view/View;->setEnabled(Z)V

    iget-object v4, p0, Lcom/google/android/gsf/login/SuggestUsernameActivity;->mNextButton:Landroid/view/View;

    invoke-virtual {v4, v0}, Landroid/view/View;->setFocusable(Z)V

    return-void

    :cond_0
    const/4 v3, 0x0

    goto :goto_0
.end method
