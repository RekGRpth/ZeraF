.class Lcom/mediatek/vlw/VideoScene$1;
.super Landroid/os/Handler;
.source "VideoScene.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/mediatek/vlw/VideoScene;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/mediatek/vlw/VideoScene;


# direct methods
.method constructor <init>(Lcom/mediatek/vlw/VideoScene;)V
    .locals 0

    iput-object p1, p0, Lcom/mediatek/vlw/VideoScene$1;->this$0:Lcom/mediatek/vlw/VideoScene;

    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 10
    .param p1    # Landroid/os/Message;

    iget v7, p1, Landroid/os/Message;->what:I

    packed-switch v7, :pswitch_data_0

    :pswitch_0
    const-string v7, "VideoScene"

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "unknown message "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    iget v9, p1, Landroid/os/Message;->what:I

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8}, Lcom/mediatek/xlog/Xlog;->w(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    :goto_0
    return-void

    :pswitch_1
    iget-object v7, p0, Lcom/mediatek/vlw/VideoScene$1;->this$0:Lcom/mediatek/vlw/VideoScene;

    invoke-virtual {v7}, Lcom/mediatek/vlw/VideoScene;->getCurrentPosition()I

    move-result v7

    int-to-long v3, v7

    iget-object v7, p0, Lcom/mediatek/vlw/VideoScene$1;->this$0:Lcom/mediatek/vlw/VideoScene;

    invoke-virtual {v7}, Lcom/mediatek/vlw/VideoScene;->getDuration()I

    move-result v7

    int-to-long v0, v7

    const-wide/16 v7, -0x1

    cmp-long v7, v0, v7

    if-nez v7, :cond_1

    const/4 v7, 0x1

    invoke-virtual {p0, v7}, Landroid/os/Handler;->obtainMessage(I)Landroid/os/Message;

    move-result-object v2

    const-wide/16 v7, 0x3e8

    invoke-virtual {p0, v2, v7, v8}, Landroid/os/Handler;->sendMessageDelayed(Landroid/os/Message;J)Z

    goto :goto_0

    :cond_1
    iget-object v7, p0, Lcom/mediatek/vlw/VideoScene$1;->this$0:Lcom/mediatek/vlw/VideoScene;

    invoke-static {v7}, Lcom/mediatek/vlw/VideoScene;->access$000(Lcom/mediatek/vlw/VideoScene;)I

    move-result v7

    int-to-long v7, v7

    cmp-long v7, v7, v0

    if-gez v7, :cond_3

    iget-object v7, p0, Lcom/mediatek/vlw/VideoScene$1;->this$0:Lcom/mediatek/vlw/VideoScene;

    invoke-static {v7}, Lcom/mediatek/vlw/VideoScene;->access$000(Lcom/mediatek/vlw/VideoScene;)I

    move-result v7

    int-to-long v7, v7

    cmp-long v7, v3, v7

    if-lez v7, :cond_2

    iget-object v7, p0, Lcom/mediatek/vlw/VideoScene$1;->this$0:Lcom/mediatek/vlw/VideoScene;

    iget-object v8, p0, Lcom/mediatek/vlw/VideoScene$1;->this$0:Lcom/mediatek/vlw/VideoScene;

    invoke-static {v8}, Lcom/mediatek/vlw/VideoScene;->access$100(Lcom/mediatek/vlw/VideoScene;)I

    move-result v8

    int-to-long v8, v8

    invoke-virtual {v7, v8, v9}, Lcom/mediatek/vlw/VideoScene;->seekTo(J)V

    :cond_2
    const/4 v7, 0x1

    invoke-virtual {p0, v7}, Landroid/os/Handler;->obtainMessage(I)Landroid/os/Message;

    move-result-object v2

    const-wide/16 v7, 0x3e8

    invoke-virtual {p0, v2, v7, v8}, Landroid/os/Handler;->sendMessageDelayed(Landroid/os/Message;J)Z

    goto :goto_0

    :cond_3
    const/4 v7, 0x1

    invoke-virtual {p0, v7}, Landroid/os/Handler;->removeMessages(I)V

    goto :goto_0

    :pswitch_2
    iget-object v7, p0, Lcom/mediatek/vlw/VideoScene$1;->this$0:Lcom/mediatek/vlw/VideoScene;

    const/4 v8, 0x0

    invoke-static {v7, v8}, Lcom/mediatek/vlw/VideoScene;->access$200(Lcom/mediatek/vlw/VideoScene;Z)V

    const/4 v7, 0x3

    invoke-virtual {p0, v7}, Landroid/os/Handler;->removeMessages(I)V

    goto :goto_0

    :pswitch_3
    iget-object v7, p0, Lcom/mediatek/vlw/VideoScene$1;->this$0:Lcom/mediatek/vlw/VideoScene;

    invoke-static {v7}, Lcom/mediatek/vlw/VideoScene;->access$300(Lcom/mediatek/vlw/VideoScene;)Landroid/net/Uri;

    move-result-object v7

    invoke-virtual {v7}, Landroid/net/Uri;->getPath()Ljava/lang/String;

    move-result-object v6

    invoke-static {v6}, Lcom/mediatek/vlw/Utils;->isExternalFileExists(Ljava/lang/String;)Z

    move-result v7

    if-nez v7, :cond_5

    const/4 v5, 0x1

    :goto_1
    const-string v7, "VideoScene"

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "MSG_RELOAD_VIDEO, videoExistNot = "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v5}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, ", mHasShutdown = "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    iget-object v9, p0, Lcom/mediatek/vlw/VideoScene$1;->this$0:Lcom/mediatek/vlw/VideoScene;

    invoke-static {v9}, Lcom/mediatek/vlw/VideoScene;->access$400(Lcom/mediatek/vlw/VideoScene;)Z

    move-result v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8}, Lcom/mediatek/xlog/Xlog;->d(Ljava/lang/String;Ljava/lang/String;)I

    if-nez v5, :cond_4

    iget-object v7, p0, Lcom/mediatek/vlw/VideoScene$1;->this$0:Lcom/mediatek/vlw/VideoScene;

    invoke-static {v7}, Lcom/mediatek/vlw/VideoScene;->access$400(Lcom/mediatek/vlw/VideoScene;)Z

    move-result v7

    if-eqz v7, :cond_6

    :cond_4
    const/4 v7, 0x4

    invoke-virtual {p0, v7}, Landroid/os/Handler;->obtainMessage(I)Landroid/os/Message;

    move-result-object v2

    const-wide/16 v7, 0x3e8

    invoke-virtual {p0, v2, v7, v8}, Landroid/os/Handler;->sendMessageDelayed(Landroid/os/Message;J)Z

    const-string v7, "VideoScene"

    const-string v8, "Cannot query video path, reload it in 1sec"

    invoke-static {v7, v8}, Lcom/mediatek/xlog/Xlog;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0

    :cond_5
    const/4 v5, 0x0

    goto :goto_1

    :cond_6
    iget-object v7, p0, Lcom/mediatek/vlw/VideoScene$1;->this$0:Lcom/mediatek/vlw/VideoScene;

    const/4 v8, 0x0

    invoke-static {v7, v8}, Lcom/mediatek/vlw/VideoScene;->access$502(Lcom/mediatek/vlw/VideoScene;Z)Z

    const/4 v7, 0x4

    invoke-virtual {p0, v7}, Landroid/os/Handler;->removeMessages(I)V

    const/4 v7, 0x5

    invoke-virtual {p0, v7}, Landroid/os/Handler;->removeMessages(I)V

    iget-object v7, p0, Lcom/mediatek/vlw/VideoScene$1;->this$0:Lcom/mediatek/vlw/VideoScene;

    invoke-static {v7}, Lcom/mediatek/vlw/VideoScene;->access$600(Lcom/mediatek/vlw/VideoScene;)Z

    move-result v7

    if-nez v7, :cond_7

    iget-object v7, p0, Lcom/mediatek/vlw/VideoScene$1;->this$0:Lcom/mediatek/vlw/VideoScene;

    iget-object v8, p0, Lcom/mediatek/vlw/VideoScene$1;->this$0:Lcom/mediatek/vlw/VideoScene;

    invoke-static {v8}, Lcom/mediatek/vlw/VideoScene;->access$800(Lcom/mediatek/vlw/VideoScene;)I

    move-result v8

    int-to-long v8, v8

    invoke-static {v7, v8, v9}, Lcom/mediatek/vlw/VideoScene;->access$702(Lcom/mediatek/vlw/VideoScene;J)J

    :cond_7
    iget-object v7, p0, Lcom/mediatek/vlw/VideoScene$1;->this$0:Lcom/mediatek/vlw/VideoScene;

    invoke-static {v7}, Lcom/mediatek/vlw/VideoScene;->access$900(Lcom/mediatek/vlw/VideoScene;)V

    iget-object v7, p0, Lcom/mediatek/vlw/VideoScene$1;->this$0:Lcom/mediatek/vlw/VideoScene;

    invoke-static {v7}, Lcom/mediatek/vlw/VideoScene;->access$1000(Lcom/mediatek/vlw/VideoScene;)Z

    move-result v7

    if-eqz v7, :cond_0

    iget-object v7, p0, Lcom/mediatek/vlw/VideoScene$1;->this$0:Lcom/mediatek/vlw/VideoScene;

    invoke-virtual {v7}, Lcom/mediatek/vlw/VideoScene;->start()V

    goto/16 :goto_0

    :pswitch_4
    const/4 v7, 0x5

    invoke-virtual {p0, v7}, Landroid/os/Handler;->removeMessages(I)V

    const/4 v7, 0x4

    invoke-virtual {p0, v7}, Landroid/os/Handler;->removeMessages(I)V

    iget-object v7, p0, Lcom/mediatek/vlw/VideoScene$1;->this$0:Lcom/mediatek/vlw/VideoScene;

    invoke-static {v7}, Lcom/mediatek/vlw/VideoScene;->access$500(Lcom/mediatek/vlw/VideoScene;)Z

    move-result v7

    if-eqz v7, :cond_0

    const-string v7, "VideoScene"

    const-string v8, "MSG_CLEAR sdcard removed, play default video"

    invoke-static {v7, v8}, Lcom/mediatek/xlog/Xlog;->w(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v7, p0, Lcom/mediatek/vlw/VideoScene$1;->this$0:Lcom/mediatek/vlw/VideoScene;

    const/4 v8, 0x0

    invoke-static {v7, v8}, Lcom/mediatek/vlw/VideoScene;->access$502(Lcom/mediatek/vlw/VideoScene;Z)Z

    iget-object v7, p0, Lcom/mediatek/vlw/VideoScene$1;->this$0:Lcom/mediatek/vlw/VideoScene;

    invoke-static {v7}, Lcom/mediatek/vlw/VideoScene;->access$1100(Lcom/mediatek/vlw/VideoScene;)Landroid/content/Context;

    move-result-object v7

    const v8, 0x7f080005

    const/4 v9, 0x1

    invoke-static {v7, v8, v9}, Lcom/mediatek/vlw/Utils;->showInfo(Landroid/content/Context;IZ)V

    iget-object v7, p0, Lcom/mediatek/vlw/VideoScene$1;->this$0:Lcom/mediatek/vlw/VideoScene;

    const/4 v8, 0x0

    invoke-static {v7, v8}, Lcom/mediatek/vlw/VideoScene;->access$200(Lcom/mediatek/vlw/VideoScene;Z)V

    iget-object v7, p0, Lcom/mediatek/vlw/VideoScene$1;->this$0:Lcom/mediatek/vlw/VideoScene;

    const/4 v8, 0x1

    const/4 v9, 0x1

    invoke-static {v7, v8, v9}, Lcom/mediatek/vlw/VideoScene;->access$1200(Lcom/mediatek/vlw/VideoScene;ZZ)V

    iget-object v7, p0, Lcom/mediatek/vlw/VideoScene$1;->this$0:Lcom/mediatek/vlw/VideoScene;

    invoke-static {v7}, Lcom/mediatek/vlw/VideoScene;->access$900(Lcom/mediatek/vlw/VideoScene;)V

    iget-object v7, p0, Lcom/mediatek/vlw/VideoScene$1;->this$0:Lcom/mediatek/vlw/VideoScene;

    invoke-static {v7}, Lcom/mediatek/vlw/VideoScene;->access$1000(Lcom/mediatek/vlw/VideoScene;)Z

    move-result v7

    if-eqz v7, :cond_0

    iget-object v7, p0, Lcom/mediatek/vlw/VideoScene$1;->this$0:Lcom/mediatek/vlw/VideoScene;

    invoke-virtual {v7}, Lcom/mediatek/vlw/VideoScene;->start()V

    goto/16 :goto_0

    :pswitch_5
    const-string v7, "VideoScene"

    const-string v8, "find the invalid video"

    invoke-static {v7, v8}, Lcom/mediatek/xlog/Xlog;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v7, p0, Lcom/mediatek/vlw/VideoScene$1;->this$0:Lcom/mediatek/vlw/VideoScene;

    invoke-virtual {v7}, Lcom/mediatek/vlw/VideoScene;->handleInvalid()V

    const/4 v7, 0x0

    invoke-virtual {p0, v7}, Landroid/os/Handler;->removeMessages(I)V

    goto/16 :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_5
        :pswitch_1
        :pswitch_0
        :pswitch_2
        :pswitch_3
        :pswitch_4
    .end packed-switch
.end method
