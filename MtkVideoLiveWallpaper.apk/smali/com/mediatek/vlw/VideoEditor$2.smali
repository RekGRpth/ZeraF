.class Lcom/mediatek/vlw/VideoEditor$2;
.super Ljava/lang/Object;
.source "VideoEditor.java"

# interfaces
.implements Landroid/media/MediaPlayer$OnErrorListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/mediatek/vlw/VideoEditor;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/mediatek/vlw/VideoEditor;


# direct methods
.method constructor <init>(Lcom/mediatek/vlw/VideoEditor;)V
    .locals 0

    iput-object p1, p0, Lcom/mediatek/vlw/VideoEditor$2;->this$0:Lcom/mediatek/vlw/VideoEditor;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onError(Landroid/media/MediaPlayer;II)Z
    .locals 7
    .param p1    # Landroid/media/MediaPlayer;
    .param p2    # I
    .param p3    # I

    const/4 v6, 0x1

    const/4 v5, 0x0

    iget-object v0, p0, Lcom/mediatek/vlw/VideoEditor$2;->this$0:Lcom/mediatek/vlw/VideoEditor;

    invoke-static {v0}, Lcom/mediatek/vlw/VideoEditor;->access$300(Lcom/mediatek/vlw/VideoEditor;)Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/mediatek/vlw/VideoEditor$2;->this$0:Lcom/mediatek/vlw/VideoEditor;

    iget-object v1, p0, Lcom/mediatek/vlw/VideoEditor$2;->this$0:Lcom/mediatek/vlw/VideoEditor;

    iget-object v2, p0, Lcom/mediatek/vlw/VideoEditor$2;->this$0:Lcom/mediatek/vlw/VideoEditor;

    invoke-static {v2}, Lcom/mediatek/vlw/VideoEditor;->access$300(Lcom/mediatek/vlw/VideoEditor;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/mediatek/vlw/Utils;->getUrisFromBucketId(Landroid/content/Context;Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/mediatek/vlw/VideoEditor;->access$402(Lcom/mediatek/vlw/VideoEditor;Ljava/util/ArrayList;)Ljava/util/ArrayList;

    iget-object v0, p0, Lcom/mediatek/vlw/VideoEditor$2;->this$0:Lcom/mediatek/vlw/VideoEditor;

    invoke-static {v0}, Lcom/mediatek/vlw/VideoEditor;->access$500(Lcom/mediatek/vlw/VideoEditor;)Ljava/util/ArrayList;

    move-result-object v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/mediatek/vlw/VideoEditor$2;->this$0:Lcom/mediatek/vlw/VideoEditor;

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    invoke-static {v0, v1}, Lcom/mediatek/vlw/VideoEditor;->access$502(Lcom/mediatek/vlw/VideoEditor;Ljava/util/ArrayList;)Ljava/util/ArrayList;

    :cond_0
    iget-object v0, p0, Lcom/mediatek/vlw/VideoEditor$2;->this$0:Lcom/mediatek/vlw/VideoEditor;

    invoke-static {v0}, Lcom/mediatek/vlw/VideoEditor;->access$500(Lcom/mediatek/vlw/VideoEditor;)Ljava/util/ArrayList;

    move-result-object v0

    iget-object v1, p0, Lcom/mediatek/vlw/VideoEditor$2;->this$0:Lcom/mediatek/vlw/VideoEditor;

    invoke-static {v1}, Lcom/mediatek/vlw/VideoEditor;->access$600(Lcom/mediatek/vlw/VideoEditor;)Landroid/net/Uri;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/mediatek/vlw/VideoEditor$2;->this$0:Lcom/mediatek/vlw/VideoEditor;

    invoke-static {v0}, Lcom/mediatek/vlw/VideoEditor;->access$500(Lcom/mediatek/vlw/VideoEditor;)Ljava/util/ArrayList;

    move-result-object v0

    iget-object v1, p0, Lcom/mediatek/vlw/VideoEditor$2;->this$0:Lcom/mediatek/vlw/VideoEditor;

    invoke-static {v1}, Lcom/mediatek/vlw/VideoEditor;->access$600(Lcom/mediatek/vlw/VideoEditor;)Landroid/net/Uri;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :cond_1
    iget-object v0, p0, Lcom/mediatek/vlw/VideoEditor$2;->this$0:Lcom/mediatek/vlw/VideoEditor;

    iget-object v1, p0, Lcom/mediatek/vlw/VideoEditor$2;->this$0:Lcom/mediatek/vlw/VideoEditor;

    invoke-static {v1}, Lcom/mediatek/vlw/VideoEditor;->access$800(Lcom/mediatek/vlw/VideoEditor;)Lcom/mediatek/vlw/Utils$LoopMode;

    move-result-object v1

    iget-object v2, p0, Lcom/mediatek/vlw/VideoEditor$2;->this$0:Lcom/mediatek/vlw/VideoEditor;

    invoke-static {v2}, Lcom/mediatek/vlw/VideoEditor;->access$700(Lcom/mediatek/vlw/VideoEditor;)I

    move-result v2

    iget-object v3, p0, Lcom/mediatek/vlw/VideoEditor$2;->this$0:Lcom/mediatek/vlw/VideoEditor;

    invoke-static {v3}, Lcom/mediatek/vlw/VideoEditor;->access$400(Lcom/mediatek/vlw/VideoEditor;)Ljava/util/ArrayList;

    move-result-object v3

    iget-object v4, p0, Lcom/mediatek/vlw/VideoEditor$2;->this$0:Lcom/mediatek/vlw/VideoEditor;

    invoke-static {v4}, Lcom/mediatek/vlw/VideoEditor;->access$500(Lcom/mediatek/vlw/VideoEditor;)Ljava/util/ArrayList;

    move-result-object v4

    invoke-static {v1, v2, v3, v4}, Lcom/mediatek/vlw/Utils;->getLoopIndex(Lcom/mediatek/vlw/Utils$LoopMode;ILjava/util/ArrayList;Ljava/util/ArrayList;)I

    move-result v1

    invoke-static {v0, v1}, Lcom/mediatek/vlw/VideoEditor;->access$702(Lcom/mediatek/vlw/VideoEditor;I)I

    iget-object v0, p0, Lcom/mediatek/vlw/VideoEditor$2;->this$0:Lcom/mediatek/vlw/VideoEditor;

    invoke-static {v0}, Lcom/mediatek/vlw/VideoEditor;->access$700(Lcom/mediatek/vlw/VideoEditor;)I

    move-result v0

    if-ltz v0, :cond_2

    iget-object v1, p0, Lcom/mediatek/vlw/VideoEditor$2;->this$0:Lcom/mediatek/vlw/VideoEditor;

    iget-object v0, p0, Lcom/mediatek/vlw/VideoEditor$2;->this$0:Lcom/mediatek/vlw/VideoEditor;

    invoke-static {v0}, Lcom/mediatek/vlw/VideoEditor;->access$400(Lcom/mediatek/vlw/VideoEditor;)Ljava/util/ArrayList;

    move-result-object v0

    iget-object v2, p0, Lcom/mediatek/vlw/VideoEditor$2;->this$0:Lcom/mediatek/vlw/VideoEditor;

    invoke-static {v2}, Lcom/mediatek/vlw/VideoEditor;->access$700(Lcom/mediatek/vlw/VideoEditor;)I

    move-result v2

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/net/Uri;

    invoke-static {v1, v0}, Lcom/mediatek/vlw/VideoEditor;->access$602(Lcom/mediatek/vlw/VideoEditor;Landroid/net/Uri;)Landroid/net/Uri;

    :goto_0
    iget-object v0, p0, Lcom/mediatek/vlw/VideoEditor$2;->this$0:Lcom/mediatek/vlw/VideoEditor;

    const v1, 0x7f080005

    invoke-static {v0, v1, v6}, Lcom/mediatek/vlw/Utils;->showInfo(Landroid/content/Context;IZ)V

    iget-object v0, p0, Lcom/mediatek/vlw/VideoEditor$2;->this$0:Lcom/mediatek/vlw/VideoEditor;

    invoke-static {v0}, Lcom/mediatek/vlw/VideoEditor;->access$1100(Lcom/mediatek/vlw/VideoEditor;)V

    return v6

    :cond_2
    const-string v0, "VideoEditor"

    const-string v1, "Error: No valid videos, play default video"

    invoke-static {v0, v1}, Lcom/mediatek/xlog/Xlog;->w(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v0, p0, Lcom/mediatek/vlw/VideoEditor$2;->this$0:Lcom/mediatek/vlw/VideoEditor;

    invoke-static {v0, v5, v6, v6}, Lcom/mediatek/vlw/VideoEditor;->access$900(Lcom/mediatek/vlw/VideoEditor;ZZZ)V

    iget-object v0, p0, Lcom/mediatek/vlw/VideoEditor$2;->this$0:Lcom/mediatek/vlw/VideoEditor;

    invoke-static {v0, v5}, Lcom/mediatek/vlw/VideoEditor;->access$702(Lcom/mediatek/vlw/VideoEditor;I)I

    goto :goto_0

    :cond_3
    const-string v0, "VideoEditor"

    const-string v1, "errors, play default video"

    invoke-static {v0, v1}, Lcom/mediatek/xlog/Xlog;->w(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v0, p0, Lcom/mediatek/vlw/VideoEditor$2;->this$0:Lcom/mediatek/vlw/VideoEditor;

    invoke-static {v0, v5, v5}, Lcom/mediatek/vlw/VideoEditor;->access$1000(Lcom/mediatek/vlw/VideoEditor;ZZ)V

    goto :goto_0
.end method
