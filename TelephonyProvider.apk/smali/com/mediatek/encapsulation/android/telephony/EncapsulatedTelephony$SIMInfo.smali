.class public Lcom/mediatek/encapsulation/android/telephony/EncapsulatedTelephony$SIMInfo;
.super Ljava/lang/Object;
.source "EncapsulatedTelephony.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/mediatek/encapsulation/android/telephony/EncapsulatedTelephony;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "SIMInfo"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/mediatek/encapsulation/android/telephony/EncapsulatedTelephony$SIMInfo$ErrorCode;
    }
.end annotation


# instance fields
.field private mColor:I

.field private mDataRoaming:I

.field private mDispalyNumberFormat:I

.field private mDisplayName:Ljava/lang/String;

.field private mICCId:Ljava/lang/String;

.field private mNumber:Ljava/lang/String;

.field private mSIMInfo:Landroid/provider/Telephony$SIMInfo;

.field private mSimBackgroundRes:I

.field private mSimId:J

.field private mSlot:I


# direct methods
.method private constructor <init>()V
    .locals 2

    const/4 v1, 0x0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const-string v0, ""

    iput-object v0, p0, Lcom/mediatek/encapsulation/android/telephony/EncapsulatedTelephony$SIMInfo;->mDisplayName:Ljava/lang/String;

    const-string v0, ""

    iput-object v0, p0, Lcom/mediatek/encapsulation/android/telephony/EncapsulatedTelephony$SIMInfo;->mNumber:Ljava/lang/String;

    const/4 v0, 0x1

    iput v0, p0, Lcom/mediatek/encapsulation/android/telephony/EncapsulatedTelephony$SIMInfo;->mDispalyNumberFormat:I

    iput v1, p0, Lcom/mediatek/encapsulation/android/telephony/EncapsulatedTelephony$SIMInfo;->mDataRoaming:I

    const/4 v0, -0x1

    iput v0, p0, Lcom/mediatek/encapsulation/android/telephony/EncapsulatedTelephony$SIMInfo;->mSlot:I

    sget-object v0, Lcom/mediatek/encapsulation/android/telephony/EncapsulatedTelephony;->SIMBackgroundRes:[I

    aget v0, v0, v1

    iput v0, p0, Lcom/mediatek/encapsulation/android/telephony/EncapsulatedTelephony$SIMInfo;->mSimBackgroundRes:I

    return-void
.end method

.method public constructor <init>(Landroid/provider/Telephony$SIMInfo;)V
    .locals 2
    .param p1    # Landroid/provider/Telephony$SIMInfo;

    const/4 v1, 0x0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const-string v0, ""

    iput-object v0, p0, Lcom/mediatek/encapsulation/android/telephony/EncapsulatedTelephony$SIMInfo;->mDisplayName:Ljava/lang/String;

    const-string v0, ""

    iput-object v0, p0, Lcom/mediatek/encapsulation/android/telephony/EncapsulatedTelephony$SIMInfo;->mNumber:Ljava/lang/String;

    const/4 v0, 0x1

    iput v0, p0, Lcom/mediatek/encapsulation/android/telephony/EncapsulatedTelephony$SIMInfo;->mDispalyNumberFormat:I

    iput v1, p0, Lcom/mediatek/encapsulation/android/telephony/EncapsulatedTelephony$SIMInfo;->mDataRoaming:I

    const/4 v0, -0x1

    iput v0, p0, Lcom/mediatek/encapsulation/android/telephony/EncapsulatedTelephony$SIMInfo;->mSlot:I

    sget-object v0, Lcom/mediatek/encapsulation/android/telephony/EncapsulatedTelephony;->SIMBackgroundRes:[I

    aget v0, v0, v1

    iput v0, p0, Lcom/mediatek/encapsulation/android/telephony/EncapsulatedTelephony$SIMInfo;->mSimBackgroundRes:I

    if-eqz p1, :cond_0

    iput-object p1, p0, Lcom/mediatek/encapsulation/android/telephony/EncapsulatedTelephony$SIMInfo;->mSIMInfo:Landroid/provider/Telephony$SIMInfo;

    :cond_0
    return-void
.end method

.method private static fromCursor(Landroid/database/Cursor;)Lcom/mediatek/encapsulation/android/telephony/EncapsulatedTelephony$SIMInfo;
    .locals 4
    .param p0    # Landroid/database/Cursor;

    new-instance v0, Lcom/mediatek/encapsulation/android/telephony/EncapsulatedTelephony$SIMInfo;

    invoke-direct {v0}, Lcom/mediatek/encapsulation/android/telephony/EncapsulatedTelephony$SIMInfo;-><init>()V

    const-string v2, "_id"

    invoke-interface {p0, v2}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v2

    invoke-interface {p0, v2}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v2

    iput-wide v2, v0, Lcom/mediatek/encapsulation/android/telephony/EncapsulatedTelephony$SIMInfo;->mSimId:J

    const-string v2, "icc_id"

    invoke-interface {p0, v2}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v2

    invoke-interface {p0, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v2

    iput-object v2, v0, Lcom/mediatek/encapsulation/android/telephony/EncapsulatedTelephony$SIMInfo;->mICCId:Ljava/lang/String;

    const-string v2, "display_name"

    invoke-interface {p0, v2}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v2

    invoke-interface {p0, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v2

    iput-object v2, v0, Lcom/mediatek/encapsulation/android/telephony/EncapsulatedTelephony$SIMInfo;->mDisplayName:Ljava/lang/String;

    const-string v2, "number"

    invoke-interface {p0, v2}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v2

    invoke-interface {p0, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v2

    iput-object v2, v0, Lcom/mediatek/encapsulation/android/telephony/EncapsulatedTelephony$SIMInfo;->mNumber:Ljava/lang/String;

    const-string v2, "display_number_format"

    invoke-interface {p0, v2}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v2

    invoke-interface {p0, v2}, Landroid/database/Cursor;->getInt(I)I

    move-result v2

    iput v2, v0, Lcom/mediatek/encapsulation/android/telephony/EncapsulatedTelephony$SIMInfo;->mDispalyNumberFormat:I

    const-string v2, "color"

    invoke-interface {p0, v2}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v2

    invoke-interface {p0, v2}, Landroid/database/Cursor;->getInt(I)I

    move-result v2

    iput v2, v0, Lcom/mediatek/encapsulation/android/telephony/EncapsulatedTelephony$SIMInfo;->mColor:I

    const-string v2, "data_roaming"

    invoke-interface {p0, v2}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v2

    invoke-interface {p0, v2}, Landroid/database/Cursor;->getInt(I)I

    move-result v2

    iput v2, v0, Lcom/mediatek/encapsulation/android/telephony/EncapsulatedTelephony$SIMInfo;->mDataRoaming:I

    const-string v2, "slot"

    invoke-interface {p0, v2}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v2

    invoke-interface {p0, v2}, Landroid/database/Cursor;->getInt(I)I

    move-result v2

    iput v2, v0, Lcom/mediatek/encapsulation/android/telephony/EncapsulatedTelephony$SIMInfo;->mSlot:I

    sget-object v2, Lcom/mediatek/encapsulation/android/telephony/EncapsulatedTelephony;->SIMBackgroundRes:[I

    array-length v1, v2

    iget v2, v0, Lcom/mediatek/encapsulation/android/telephony/EncapsulatedTelephony$SIMInfo;->mColor:I

    if-ltz v2, :cond_0

    iget v2, v0, Lcom/mediatek/encapsulation/android/telephony/EncapsulatedTelephony$SIMInfo;->mColor:I

    if-ge v2, v1, :cond_0

    sget-object v2, Lcom/mediatek/encapsulation/android/telephony/EncapsulatedTelephony;->SIMBackgroundRes:[I

    iget v3, v0, Lcom/mediatek/encapsulation/android/telephony/EncapsulatedTelephony$SIMInfo;->mColor:I

    aget v2, v2, v3

    iput v2, v0, Lcom/mediatek/encapsulation/android/telephony/EncapsulatedTelephony$SIMInfo;->mSimBackgroundRes:I

    :cond_0
    return-object v0
.end method

.method public static getAllSIMCount(Landroid/content/Context;)I
    .locals 1
    .param p0    # Landroid/content/Context;

    invoke-static {p0}, Landroid/provider/Telephony$SIMInfo;->getAllSIMCount(Landroid/content/Context;)I

    move-result v0

    return v0
.end method

.method public static getAllSIMList(Landroid/content/Context;)Ljava/util/List;
    .locals 5
    .param p0    # Landroid/content/Context;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            ")",
            "Ljava/util/List",
            "<",
            "Lcom/mediatek/encapsulation/android/telephony/EncapsulatedTelephony$SIMInfo;",
            ">;"
        }
    .end annotation

    invoke-static {p0}, Landroid/provider/Telephony$SIMInfo;->getAllSIMList(Landroid/content/Context;)Ljava/util/List;

    move-result-object v2

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    const/4 v0, 0x0

    :goto_0
    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v3

    if-ge v0, v3, :cond_0

    new-instance v4, Lcom/mediatek/encapsulation/android/telephony/EncapsulatedTelephony$SIMInfo;

    invoke-interface {v2, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/provider/Telephony$SIMInfo;

    invoke-direct {v4, v3}, Lcom/mediatek/encapsulation/android/telephony/EncapsulatedTelephony$SIMInfo;-><init>(Landroid/provider/Telephony$SIMInfo;)V

    invoke-virtual {v1, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_0
    return-object v1
.end method

.method private static getAppropriateIndex(Landroid/content/Context;JLjava/lang/String;)I
    .locals 19
    .param p0    # Landroid/content/Context;
    .param p1    # J
    .param p3    # Ljava/lang/String;

    const v2, 0x2050086

    move-object/from16 v0, p0

    invoke-virtual {v0, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v10

    new-instance v15, Ljava/lang/StringBuilder;

    const-string v2, "display_name LIKE "

    invoke-direct {v15, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    if-nez p3, :cond_1

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const/16 v3, 0x25

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v15, v2}, Landroid/database/DatabaseUtils;->appendEscapedSQLString(Ljava/lang/StringBuilder;Ljava/lang/String;)V

    :goto_0
    const-string v2, " AND ("

    invoke-virtual {v15, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "_id!="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    move-wide/from16 v0, p1

    invoke-virtual {v2, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v15, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v2, ")"

    invoke-virtual {v15, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual/range {p0 .. p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    sget-object v3, Lcom/mediatek/encapsulation/android/telephony/EncapsulatedTelephony$SimInfo;->CONTENT_URI:Landroid/net/Uri;

    const/4 v4, 0x2

    new-array v4, v4, [Ljava/lang/String;

    const/4 v5, 0x0

    const-string v6, "_id"

    aput-object v6, v4, v5

    const/4 v5, 0x1

    const-string v6, "display_name"

    aput-object v6, v4, v5

    invoke-virtual {v15}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    const/4 v6, 0x0

    const-string v7, "display_name"

    invoke-virtual/range {v2 .. v7}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v9

    new-instance v8, Ljava/util/ArrayList;

    invoke-direct {v8}, Ljava/util/ArrayList;-><init>()V

    const/4 v13, 0x1

    if-eqz v9, :cond_3

    :cond_0
    :goto_1
    invoke-interface {v9}, Landroid/database/Cursor;->moveToNext()Z

    move-result v2

    if-eqz v2, :cond_2

    const/4 v2, 0x1

    invoke-interface {v9, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v11

    if-eqz v11, :cond_0

    invoke-virtual {v11}, Ljava/lang/String;->length()I

    move-result v14

    const/4 v2, 0x2

    if-lt v14, v2, :cond_0

    add-int/lit8 v2, v14, -0x2

    invoke-virtual {v11, v2}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v16

    invoke-static/range {v16 .. v16}, Landroid/text/TextUtils;->isDigitsOnly(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-static/range {v16 .. v16}, Ljava/lang/Long;->valueOf(Ljava/lang/String;)Ljava/lang/Long;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Long;->longValue()J

    move-result-wide v17

    invoke-static/range {v17 .. v18}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-virtual {v8, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_1

    :cond_1
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, p3

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const/16 v3, 0x25

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v15, v2}, Landroid/database/DatabaseUtils;->appendEscapedSQLString(Ljava/lang/StringBuilder;Ljava/lang/String;)V

    goto/16 :goto_0

    :cond_2
    invoke-interface {v9}, Landroid/database/Cursor;->close()V

    :cond_3
    const/4 v12, 0x1

    :goto_2
    const/16 v2, 0x63

    if-gt v12, v2, :cond_5

    int-to-long v2, v12

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-virtual {v8, v2}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_4

    add-int/lit8 v12, v12, 0x1

    goto :goto_2

    :cond_4
    move v13, v12

    :cond_5
    return v13
.end method

.method public static getIdBySlot(Landroid/content/Context;I)J
    .locals 2
    .param p0    # Landroid/content/Context;
    .param p1    # I

    invoke-static {p0, p1}, Landroid/provider/Telephony$SIMInfo;->getIdBySlot(Landroid/content/Context;I)J

    move-result-wide v0

    return-wide v0
.end method

.method public static getInsertedSIMCount(Landroid/content/Context;)I
    .locals 1
    .param p0    # Landroid/content/Context;

    invoke-static {p0}, Landroid/provider/Telephony$SIMInfo;->getInsertedSIMCount(Landroid/content/Context;)I

    move-result v0

    return v0
.end method

.method public static getInsertedSIMList(Landroid/content/Context;)Ljava/util/List;
    .locals 5
    .param p0    # Landroid/content/Context;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            ")",
            "Ljava/util/List",
            "<",
            "Lcom/mediatek/encapsulation/android/telephony/EncapsulatedTelephony$SIMInfo;",
            ">;"
        }
    .end annotation

    invoke-static {p0}, Landroid/provider/Telephony$SIMInfo;->getInsertedSIMList(Landroid/content/Context;)Ljava/util/List;

    move-result-object v2

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    const/4 v0, 0x0

    :goto_0
    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v3

    if-ge v0, v3, :cond_0

    new-instance v4, Lcom/mediatek/encapsulation/android/telephony/EncapsulatedTelephony$SIMInfo;

    invoke-interface {v2, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/provider/Telephony$SIMInfo;

    invoke-direct {v4, v3}, Lcom/mediatek/encapsulation/android/telephony/EncapsulatedTelephony$SIMInfo;-><init>(Landroid/provider/Telephony$SIMInfo;)V

    invoke-virtual {v1, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_0
    return-object v1
.end method

.method public static getSIMInfoByICCId(Landroid/content/Context;Ljava/lang/String;)Lcom/mediatek/encapsulation/android/telephony/EncapsulatedTelephony$SIMInfo;
    .locals 2
    .param p0    # Landroid/content/Context;
    .param p1    # Ljava/lang/String;

    new-instance v0, Lcom/mediatek/encapsulation/android/telephony/EncapsulatedTelephony$SIMInfo;

    invoke-static {p0, p1}, Landroid/provider/Telephony$SIMInfo;->getSIMInfoByICCId(Landroid/content/Context;Ljava/lang/String;)Landroid/provider/Telephony$SIMInfo;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/mediatek/encapsulation/android/telephony/EncapsulatedTelephony$SIMInfo;-><init>(Landroid/provider/Telephony$SIMInfo;)V

    return-object v0
.end method

.method public static getSIMInfoById(Landroid/content/Context;J)Lcom/mediatek/encapsulation/android/telephony/EncapsulatedTelephony$SIMInfo;
    .locals 2
    .param p0    # Landroid/content/Context;
    .param p1    # J

    new-instance v0, Lcom/mediatek/encapsulation/android/telephony/EncapsulatedTelephony$SIMInfo;

    invoke-static {p0, p1, p2}, Landroid/provider/Telephony$SIMInfo;->getSIMInfoById(Landroid/content/Context;J)Landroid/provider/Telephony$SIMInfo;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/mediatek/encapsulation/android/telephony/EncapsulatedTelephony$SIMInfo;-><init>(Landroid/provider/Telephony$SIMInfo;)V

    return-object v0
.end method

.method public static getSIMInfoByName(Landroid/content/Context;Ljava/lang/String;)Lcom/mediatek/encapsulation/android/telephony/EncapsulatedTelephony$SIMInfo;
    .locals 2
    .param p0    # Landroid/content/Context;
    .param p1    # Ljava/lang/String;

    new-instance v0, Lcom/mediatek/encapsulation/android/telephony/EncapsulatedTelephony$SIMInfo;

    invoke-static {p0, p1}, Landroid/provider/Telephony$SIMInfo;->getSIMInfoByName(Landroid/content/Context;Ljava/lang/String;)Landroid/provider/Telephony$SIMInfo;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/mediatek/encapsulation/android/telephony/EncapsulatedTelephony$SIMInfo;-><init>(Landroid/provider/Telephony$SIMInfo;)V

    return-object v0
.end method

.method public static getSIMInfoBySlot(Landroid/content/Context;I)Lcom/mediatek/encapsulation/android/telephony/EncapsulatedTelephony$SIMInfo;
    .locals 2
    .param p0    # Landroid/content/Context;
    .param p1    # I

    new-instance v0, Lcom/mediatek/encapsulation/android/telephony/EncapsulatedTelephony$SIMInfo;

    invoke-static {p0, p1}, Landroid/provider/Telephony$SIMInfo;->getSIMInfoBySlot(Landroid/content/Context;I)Landroid/provider/Telephony$SIMInfo;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/mediatek/encapsulation/android/telephony/EncapsulatedTelephony$SIMInfo;-><init>(Landroid/provider/Telephony$SIMInfo;)V

    return-object v0
.end method

.method public static getSlotById(Landroid/content/Context;J)I
    .locals 1
    .param p0    # Landroid/content/Context;
    .param p1    # J

    invoke-static {p0, p1, p2}, Landroid/provider/Telephony$SIMInfo;->getSlotById(Landroid/content/Context;J)I

    move-result v0

    return v0
.end method

.method public static getSlotByName(Landroid/content/Context;Ljava/lang/String;)I
    .locals 1
    .param p0    # Landroid/content/Context;
    .param p1    # Ljava/lang/String;

    invoke-static {p0, p1}, Landroid/provider/Telephony$SIMInfo;->getSlotByName(Landroid/content/Context;Ljava/lang/String;)I

    move-result v0

    return v0
.end method

.method private static getSuffixFromIndex(I)Ljava/lang/String;
    .locals 2
    .param p0    # I

    const/16 v0, 0xa

    if-ge p0, v0, :cond_0

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "0"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    invoke-static {p0}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public static insertICCId(Landroid/content/Context;Ljava/lang/String;I)Landroid/net/Uri;
    .locals 1
    .param p0    # Landroid/content/Context;
    .param p1    # Ljava/lang/String;
    .param p2    # I

    invoke-static {p0, p1, p2}, Landroid/provider/Telephony$SIMInfo;->insertICCId(Landroid/content/Context;Ljava/lang/String;I)Landroid/net/Uri;

    move-result-object v0

    return-object v0
.end method

.method public static setColor(Landroid/content/Context;IJ)I
    .locals 1
    .param p0    # Landroid/content/Context;
    .param p1    # I
    .param p2    # J

    invoke-static {p0, p1, p2, p3}, Landroid/provider/Telephony$SIMInfo;->setColor(Landroid/content/Context;IJ)I

    move-result v0

    return v0
.end method

.method public static setDataRoaming(Landroid/content/Context;IJ)I
    .locals 1
    .param p0    # Landroid/content/Context;
    .param p1    # I
    .param p2    # J

    invoke-static {p0, p1, p2, p3}, Landroid/provider/Telephony$SIMInfo;->setDataRoaming(Landroid/content/Context;IJ)I

    move-result v0

    return v0
.end method

.method public static setDefaultName(Landroid/content/Context;JLjava/lang/String;)I
    .locals 1
    .param p0    # Landroid/content/Context;
    .param p1    # J
    .param p3    # Ljava/lang/String;

    invoke-static {p0, p1, p2, p3}, Landroid/provider/Telephony$SIMInfo;->setDefaultName(Landroid/content/Context;JLjava/lang/String;)I

    move-result v0

    return v0
.end method

.method public static setDispalyNumberFormat(Landroid/content/Context;IJ)I
    .locals 1
    .param p0    # Landroid/content/Context;
    .param p1    # I
    .param p2    # J

    invoke-static {p0, p1, p2, p3}, Landroid/provider/Telephony$SIMInfo;->setDispalyNumberFormat(Landroid/content/Context;IJ)I

    move-result v0

    return v0
.end method

.method public static setDisplayName(Landroid/content/Context;Ljava/lang/String;J)I
    .locals 1
    .param p0    # Landroid/content/Context;
    .param p1    # Ljava/lang/String;
    .param p2    # J

    invoke-static {p0, p1, p2, p3}, Landroid/provider/Telephony$SIMInfo;->setDisplayName(Landroid/content/Context;Ljava/lang/String;J)I

    move-result v0

    return v0
.end method

.method public static setNumber(Landroid/content/Context;Ljava/lang/String;J)I
    .locals 1
    .param p0    # Landroid/content/Context;
    .param p1    # Ljava/lang/String;
    .param p2    # J

    invoke-static {p0, p1, p2, p3}, Landroid/provider/Telephony$SIMInfo;->setNumber(Landroid/content/Context;Ljava/lang/String;J)I

    move-result v0

    return v0
.end method


# virtual methods
.method public getColor()I
    .locals 1

    iget-object v0, p0, Lcom/mediatek/encapsulation/android/telephony/EncapsulatedTelephony$SIMInfo;->mSIMInfo:Landroid/provider/Telephony$SIMInfo;

    iget v0, v0, Landroid/provider/Telephony$SIMInfo;->mColor:I

    return v0
.end method

.method public getDataRoaming()I
    .locals 1

    iget-object v0, p0, Lcom/mediatek/encapsulation/android/telephony/EncapsulatedTelephony$SIMInfo;->mSIMInfo:Landroid/provider/Telephony$SIMInfo;

    iget v0, v0, Landroid/provider/Telephony$SIMInfo;->mDataRoaming:I

    return v0
.end method

.method public getDispalyNumberFormat()I
    .locals 1

    iget-object v0, p0, Lcom/mediatek/encapsulation/android/telephony/EncapsulatedTelephony$SIMInfo;->mSIMInfo:Landroid/provider/Telephony$SIMInfo;

    iget v0, v0, Landroid/provider/Telephony$SIMInfo;->mDispalyNumberFormat:I

    return v0
.end method

.method public getDisplayName()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/mediatek/encapsulation/android/telephony/EncapsulatedTelephony$SIMInfo;->mSIMInfo:Landroid/provider/Telephony$SIMInfo;

    iget-object v0, v0, Landroid/provider/Telephony$SIMInfo;->mDisplayName:Ljava/lang/String;

    return-object v0
.end method

.method public getICCId()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/mediatek/encapsulation/android/telephony/EncapsulatedTelephony$SIMInfo;->mSIMInfo:Landroid/provider/Telephony$SIMInfo;

    iget-object v0, v0, Landroid/provider/Telephony$SIMInfo;->mICCId:Ljava/lang/String;

    return-object v0
.end method

.method public getNumber()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/mediatek/encapsulation/android/telephony/EncapsulatedTelephony$SIMInfo;->mSIMInfo:Landroid/provider/Telephony$SIMInfo;

    iget-object v0, v0, Landroid/provider/Telephony$SIMInfo;->mNumber:Ljava/lang/String;

    return-object v0
.end method

.method public getSimBackgroundRes()I
    .locals 1

    iget-object v0, p0, Lcom/mediatek/encapsulation/android/telephony/EncapsulatedTelephony$SIMInfo;->mSIMInfo:Landroid/provider/Telephony$SIMInfo;

    iget v0, v0, Landroid/provider/Telephony$SIMInfo;->mSimBackgroundRes:I

    return v0
.end method

.method public getSimId()J
    .locals 2

    iget-object v0, p0, Lcom/mediatek/encapsulation/android/telephony/EncapsulatedTelephony$SIMInfo;->mSIMInfo:Landroid/provider/Telephony$SIMInfo;

    iget-wide v0, v0, Landroid/provider/Telephony$SIMInfo;->mSimId:J

    return-wide v0
.end method

.method public getSlot()I
    .locals 1

    iget-object v0, p0, Lcom/mediatek/encapsulation/android/telephony/EncapsulatedTelephony$SIMInfo;->mSIMInfo:Landroid/provider/Telephony$SIMInfo;

    iget v0, v0, Landroid/provider/Telephony$SIMInfo;->mSlot:I

    return v0
.end method
