.class public Lcom/mediatek/appwidget/worldclock/ChooseActivity;
.super Landroid/app/Activity;
.source "ChooseActivity.java"


# static fields
.field private static final AUTOCOMPLETECHOOSEACTIVITYRESULTCODE:I = 0xa

.field private static final CHOOSEACTIVITYREQUESTCODE:I = 0xa

.field private static final DELETE_INTENT:Ljava/lang/String; = "android.intent.action.mtk.worldclock.deleteIntent"

.field private static final MENU_ADD:I = 0x1

.field private static final MENU_UPDATE:I = 0x2

.field private static final MTKWORLDCLOCKCHOOSE:Ljava/lang/String; = "mtkworldclockchoose"

.field private static final ON_CLICK_APPWIDGETID:Ljava/lang/String; = "onClickAppWidgetId"

.field private static final TAG:Ljava/lang/String; = "MTKWORLDCHOOSE"

.field private static final TIMEZONE_ID:I = 0x0

.field private static final WEATHER_ID:I = 0x1

.field private static mAppwidgetId:I

.field private static mPosition:I


# instance fields
.field private data:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ">;>;"
        }
    .end annotation
.end field

.field private mAdapterCityArray:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private mAdapterLocalCityArray:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private mChooserAdapter:Lcom/mediatek/appwidget/worldclock/SelectedCityListAdapter;

.field private mCityInfo:Lcom/mediatek/appwidget/worldclock/ClockCityInfo;

.field private mCityNameArrayBak:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private mCityNumberInXml:I

.field private mContext:Landroid/content/Context;

.field private mListView:Landroid/widget/ListView;

.field private mNoCity:Landroid/widget/TextView;

.field private mTimeZoneArray:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private mWeatherIDArray:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const/4 v0, -0x1

    sput v0, Lcom/mediatek/appwidget/worldclock/ChooseActivity;->mPosition:I

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Landroid/app/Activity;-><init>()V

    new-instance v0, Lcom/mediatek/appwidget/worldclock/ClockCityInfo;

    invoke-direct {v0}, Lcom/mediatek/appwidget/worldclock/ClockCityInfo;-><init>()V

    iput-object v0, p0, Lcom/mediatek/appwidget/worldclock/ChooseActivity;->mCityInfo:Lcom/mediatek/appwidget/worldclock/ClockCityInfo;

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/mediatek/appwidget/worldclock/ChooseActivity;->mTimeZoneArray:Ljava/util/ArrayList;

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/mediatek/appwidget/worldclock/ChooseActivity;->mCityNameArrayBak:Ljava/util/ArrayList;

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/mediatek/appwidget/worldclock/ChooseActivity;->mWeatherIDArray:Ljava/util/ArrayList;

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/mediatek/appwidget/worldclock/ChooseActivity;->mAdapterCityArray:Ljava/util/ArrayList;

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/mediatek/appwidget/worldclock/ChooseActivity;->mAdapterLocalCityArray:Ljava/util/ArrayList;

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/mediatek/appwidget/worldclock/ChooseActivity;->data:Ljava/util/ArrayList;

    return-void
.end method

.method static synthetic access$000(Lcom/mediatek/appwidget/worldclock/ChooseActivity;)Lcom/mediatek/appwidget/worldclock/ClockCityInfo;
    .locals 1
    .param p0    # Lcom/mediatek/appwidget/worldclock/ChooseActivity;

    iget-object v0, p0, Lcom/mediatek/appwidget/worldclock/ChooseActivity;->mCityInfo:Lcom/mediatek/appwidget/worldclock/ClockCityInfo;

    return-object v0
.end method

.method static synthetic access$002(Lcom/mediatek/appwidget/worldclock/ChooseActivity;Lcom/mediatek/appwidget/worldclock/ClockCityInfo;)Lcom/mediatek/appwidget/worldclock/ClockCityInfo;
    .locals 0
    .param p0    # Lcom/mediatek/appwidget/worldclock/ChooseActivity;
    .param p1    # Lcom/mediatek/appwidget/worldclock/ClockCityInfo;

    iput-object p1, p0, Lcom/mediatek/appwidget/worldclock/ChooseActivity;->mCityInfo:Lcom/mediatek/appwidget/worldclock/ClockCityInfo;

    return-object p1
.end method

.method static synthetic access$100(Lcom/mediatek/appwidget/worldclock/ChooseActivity;Ljava/lang/String;)Lcom/mediatek/appwidget/worldclock/ClockCityInfo;
    .locals 1
    .param p0    # Lcom/mediatek/appwidget/worldclock/ChooseActivity;
    .param p1    # Ljava/lang/String;

    invoke-direct {p0, p1}, Lcom/mediatek/appwidget/worldclock/ChooseActivity;->setCityInfoByWeatherID(Ljava/lang/String;)Lcom/mediatek/appwidget/worldclock/ClockCityInfo;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$200()I
    .locals 1

    sget v0, Lcom/mediatek/appwidget/worldclock/ChooseActivity;->mAppwidgetId:I

    return v0
.end method

.method static synthetic access$302(I)I
    .locals 0
    .param p0    # I

    sput p0, Lcom/mediatek/appwidget/worldclock/ChooseActivity;->mPosition:I

    return p0
.end method

.method private getCityNameBYWeatherId(Ljava/lang/String;)Ljava/lang/String;
    .locals 3
    .param p1    # Ljava/lang/String;

    const/4 v0, 0x0

    const/4 v1, 0x0

    :goto_0
    iget-object v2, p0, Lcom/mediatek/appwidget/worldclock/ChooseActivity;->mWeatherIDArray:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v2

    if-ge v1, v2, :cond_0

    iget-object v2, p0, Lcom/mediatek/appwidget/worldclock/ChooseActivity;->mWeatherIDArray:Ljava/util/ArrayList;

    invoke-virtual {v2, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    invoke-virtual {p1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    iget-object v2, p0, Lcom/mediatek/appwidget/worldclock/ChooseActivity;->mCityNameArrayBak:Ljava/util/ArrayList;

    invoke-virtual {v2, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    :cond_0
    return-object v0

    :cond_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_0
.end method

.method private getData(Ljava/lang/String;)V
    .locals 14
    .param p1    # Ljava/lang/String;

    const/4 v13, 0x0

    iget-object v10, p0, Lcom/mediatek/appwidget/worldclock/ChooseActivity;->data:Ljava/util/ArrayList;

    if-nez v10, :cond_0

    iget-object v10, p0, Lcom/mediatek/appwidget/worldclock/ChooseActivity;->data:Ljava/util/ArrayList;

    invoke-virtual {v10}, Ljava/util/ArrayList;->size()I

    move-result v10

    if-lez v10, :cond_1

    :cond_0
    iget-object v10, p0, Lcom/mediatek/appwidget/worldclock/ChooseActivity;->data:Ljava/util/ArrayList;

    invoke-virtual {v10}, Ljava/util/ArrayList;->clear()V

    :cond_1
    if-eqz p1, :cond_6

    const-string v10, "chooseshared"

    invoke-virtual {p0, v10, v13}, Landroid/content/ContextWrapper;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v7

    const-string v10, "mtkworldclockchoose"

    const-string v11, "first"

    invoke-interface {v7, v10, v11}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    const-string v10, ","

    invoke-virtual {v0, v10}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v6

    const/4 v5, 0x0

    :goto_0
    array-length v10, v6

    if-ge v5, v10, :cond_4

    aget-object v10, v6, v5

    invoke-virtual {p1, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_3

    const-string p1, ""

    aget-object v10, v6, v5

    invoke-direct {p0, v10}, Lcom/mediatek/appwidget/worldclock/ChooseActivity;->getCityNameBYWeatherId(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v9

    invoke-static {v9}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v10

    if-nez v10, :cond_2

    const-string v10, ","

    invoke-virtual {v9, v10}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v10

    if-eqz v10, :cond_2

    const-string v10, ","

    invoke-virtual {v9, v10}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v8

    array-length v10, v8

    add-int/lit8 v10, v10, -0x1

    aget-object v9, v8, v10

    :cond_2
    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v10, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    const-string v11, " "

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    iget-object v11, p0, Lcom/mediatek/appwidget/worldclock/ChooseActivity;->mContext:Landroid/content/Context;

    invoke-virtual {v11}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v11

    const v12, 0x7f07000e

    invoke-virtual {v11, v12}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    iget-object v10, p0, Lcom/mediatek/appwidget/worldclock/ChooseActivity;->mContext:Landroid/content/Context;

    invoke-static {v10, v9, v13}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v10

    invoke-virtual {v10}, Landroid/widget/Toast;->show()V

    :cond_3
    add-int/lit8 v5, v5, 0x1

    goto :goto_0

    :cond_4
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v10

    if-nez v10, :cond_5

    invoke-interface {v7}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v3

    const-string v10, "mtkworldclockchoose"

    new-instance v11, Ljava/lang/StringBuilder;

    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v11, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    const-string v12, ","

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-interface {v3, v10, v11}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    invoke-interface {v3}, Landroid/content/SharedPreferences$Editor;->commit()Z

    :cond_5
    const-string v10, "mtkworldclockchoose"

    const-string v11, "first"

    invoke-interface {v7, v10, v11}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_6

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v10

    const/4 v11, 0x5

    if-le v10, v11, :cond_6

    const-string v10, ","

    invoke-virtual {v0, v10}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v2

    const/4 v5, 0x1

    :goto_1
    array-length v10, v2

    if-ge v5, v10, :cond_6

    new-instance v4, Ljava/util/HashMap;

    invoke-direct {v4}, Ljava/util/HashMap;-><init>()V

    const-string v10, "chooselistviewimageview"

    const v11, 0x7f020004

    invoke-static {v11}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v11

    invoke-virtual {v4, v10, v11}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    aget-object v10, v2, v5

    invoke-direct {p0, v10}, Lcom/mediatek/appwidget/worldclock/ChooseActivity;->getCityNameBYWeatherId(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    const-string v10, "chooselistviewtextview"

    invoke-virtual {v4, v10, v1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const-string v10, "chooselistviewweatherid"

    aget-object v11, v2, v5

    invoke-virtual {v4, v10, v11}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v10, p0, Lcom/mediatek/appwidget/worldclock/ChooseActivity;->data:Ljava/util/ArrayList;

    invoke-virtual {v10, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    add-int/lit8 v5, v5, 0x1

    goto :goto_1

    :cond_6
    return-void
.end method

.method private getLocalGMTString()Ljava/lang/String;
    .locals 10

    const/16 v9, 0x30

    const/16 v8, 0xa

    invoke-static {}, Ljava/util/TimeZone;->getDefault()Ljava/util/TimeZone;

    move-result-object v6

    invoke-virtual {v6}, Ljava/util/TimeZone;->getID()Ljava/lang/String;

    move-result-object v6

    invoke-static {v6}, Ljava/util/TimeZone;->getTimeZone(Ljava/lang/String;)Ljava/util/TimeZone;

    move-result-object v5

    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v6

    invoke-virtual {v6}, Ljava/util/Calendar;->getTimeInMillis()J

    move-result-wide v6

    invoke-virtual {v5, v6, v7}, Ljava/util/TimeZone;->getOffset(J)I

    move-result v3

    invoke-static {v3}, Ljava/lang/Math;->abs(I)I

    move-result v4

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "GMT"

    invoke-virtual {v2, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    if-gez v3, :cond_1

    const/16 v6, 0x2d

    invoke-virtual {v2, v6}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    :goto_0
    const v6, 0x36ee80

    div-int v0, v4, v6

    if-ge v0, v8, :cond_2

    invoke-virtual {v2, v9}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    :goto_1
    const/16 v6, 0x3a

    invoke-virtual {v2, v6}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    const v6, 0xea60

    div-int v1, v4, v6

    rem-int/lit8 v1, v1, 0x3c

    if-ge v1, v8, :cond_0

    invoke-virtual {v2, v9}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    :cond_0
    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    return-object v6

    :cond_1
    const/16 v6, 0x2b

    invoke-virtual {v2, v6}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    goto :goto_0

    :cond_2
    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    goto :goto_1
.end method

.method private setCityInfoByWeatherID(Ljava/lang/String;)Lcom/mediatek/appwidget/worldclock/ClockCityInfo;
    .locals 5
    .param p1    # Ljava/lang/String;

    new-instance v0, Lcom/mediatek/appwidget/worldclock/ClockCityInfo;

    invoke-direct {v0}, Lcom/mediatek/appwidget/worldclock/ClockCityInfo;-><init>()V

    invoke-virtual {v0, p1}, Lcom/mediatek/appwidget/worldclock/ClockCityInfo;->setWeatherID(Ljava/lang/String;)V

    const/4 v2, 0x0

    :goto_0
    iget-object v4, p0, Lcom/mediatek/appwidget/worldclock/ChooseActivity;->mWeatherIDArray:Ljava/util/ArrayList;

    invoke-virtual {v4}, Ljava/util/ArrayList;->size()I

    move-result v4

    if-ge v2, v4, :cond_0

    iget-object v4, p0, Lcom/mediatek/appwidget/worldclock/ChooseActivity;->mWeatherIDArray:Ljava/util/ArrayList;

    invoke-virtual {v4, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/String;

    invoke-virtual {v4, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_1

    iget-object v4, p0, Lcom/mediatek/appwidget/worldclock/ChooseActivity;->mTimeZoneArray:Ljava/util/ArrayList;

    invoke-virtual {v4, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    iget-object v4, p0, Lcom/mediatek/appwidget/worldclock/ChooseActivity;->mCityNameArrayBak:Ljava/util/ArrayList;

    invoke-virtual {v4, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    invoke-virtual {v0, v3}, Lcom/mediatek/appwidget/worldclock/ClockCityInfo;->setTimeZone(Ljava/lang/String;)V

    invoke-static {v2}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0, v4}, Lcom/mediatek/appwidget/worldclock/ClockCityInfo;->setIndex(Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Lcom/mediatek/appwidget/worldclock/ClockCityInfo;->setCityName(Ljava/lang/String;)V

    :cond_0
    return-object v0

    :cond_1
    add-int/lit8 v2, v2, 0x1

    goto :goto_0
.end method


# virtual methods
.method public getZones()V
    .locals 13

    const/4 v10, 0x0

    invoke-direct {p0}, Lcom/mediatek/appwidget/worldclock/ChooseActivity;->getLocalGMTString()Ljava/lang/String;

    move-result-object v3

    :try_start_0
    invoke-virtual {p0}, Landroid/view/ContextThemeWrapper;->getResources()Landroid/content/res/Resources;

    move-result-object v11

    const v12, 0x7f040001

    invoke-virtual {v11, v12}, Landroid/content/res/Resources;->getXml(I)Landroid/content/res/XmlResourceParser;

    move-result-object v10

    :cond_0
    invoke-interface {v10}, Landroid/content/res/XmlResourceParser;->next()I

    move-result v11

    const/4 v12, 0x2

    if-ne v11, v12, :cond_0

    invoke-interface {v10}, Landroid/content/res/XmlResourceParser;->next()I

    const/4 v4, 0x0

    const/16 v11, 0x1f4

    new-array v5, v11, [Ljava/lang/String;

    const/16 v11, 0x1f4

    new-array v7, v11, [Ljava/lang/String;

    const/16 v11, 0x1f4

    new-array v6, v11, [Ljava/lang/String;

    :goto_0
    invoke-interface {v10}, Landroid/content/res/XmlResourceParser;->getEventType()I

    move-result v11

    const/4 v12, 0x3

    if-eq v11, v12, :cond_8

    :goto_1
    invoke-interface {v10}, Landroid/content/res/XmlResourceParser;->getEventType()I

    move-result v11

    const/4 v12, 0x2

    if-eq v11, v12, :cond_3

    invoke-interface {v10}, Landroid/content/res/XmlResourceParser;->getEventType()I
    :try_end_0
    .catch Lorg/xmlpull/v1/XmlPullParserException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v11

    const/4 v12, 0x1

    if-ne v11, v12, :cond_2

    if-eqz v10, :cond_1

    invoke-interface {v10}, Landroid/content/res/XmlResourceParser;->close()V

    :cond_1
    :goto_2
    return-void

    :cond_2
    :try_start_1
    invoke-interface {v10}, Landroid/content/res/XmlResourceParser;->next()I
    :try_end_1
    .catch Lorg/xmlpull/v1/XmlPullParserException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_1

    :catch_0
    move-exception v9

    :try_start_2
    const-string v11, "aaa"

    const-string v12, "Ill-formatted timezones.xml file"

    invoke-static {v11, v12}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    if-eqz v10, :cond_1

    invoke-interface {v10}, Landroid/content/res/XmlResourceParser;->close()V

    goto :goto_2

    :cond_3
    :try_start_3
    invoke-interface {v10}, Landroid/content/res/XmlResourceParser;->getName()Ljava/lang/String;

    move-result-object v11

    const-string v12, "timezone"

    invoke-virtual {v11, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_5

    const/4 v11, 0x0

    invoke-interface {v10, v11}, Landroid/content/res/XmlResourceParser;->getAttributeValue(I)Ljava/lang/String;

    move-result-object v1

    const/4 v11, 0x1

    invoke-interface {v10, v11}, Landroid/content/res/XmlResourceParser;->getAttributeValue(I)Ljava/lang/String;

    move-result-object v8

    invoke-interface {v10}, Landroid/content/res/XmlResourceParser;->nextText()Ljava/lang/String;

    move-result-object v0

    const/16 v11, 0x1f4

    if-ge v4, v11, :cond_5

    iget-object v11, p0, Lcom/mediatek/appwidget/worldclock/ChooseActivity;->mCityNameArrayBak:Ljava/util/ArrayList;

    invoke-virtual {v11, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    iget-object v11, p0, Lcom/mediatek/appwidget/worldclock/ChooseActivity;->mAdapterCityArray:Ljava/util/ArrayList;

    invoke-virtual {v11, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    iget-object v11, p0, Lcom/mediatek/appwidget/worldclock/ChooseActivity;->mTimeZoneArray:Ljava/util/ArrayList;

    invoke-virtual {v11, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    invoke-virtual {v1, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_4

    iget-object v11, p0, Lcom/mediatek/appwidget/worldclock/ChooseActivity;->mAdapterLocalCityArray:Ljava/util/ArrayList;

    invoke-virtual {v11, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :cond_4
    iget-object v11, p0, Lcom/mediatek/appwidget/worldclock/ChooseActivity;->mWeatherIDArray:Ljava/util/ArrayList;

    invoke-virtual {v11, v8}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    add-int/lit8 v4, v4, 0x1

    :cond_5
    :goto_3
    invoke-interface {v10}, Landroid/content/res/XmlResourceParser;->getEventType()I

    move-result v11

    const/4 v12, 0x3

    if-eq v11, v12, :cond_6

    invoke-interface {v10}, Landroid/content/res/XmlResourceParser;->next()I
    :try_end_3
    .catch Lorg/xmlpull/v1/XmlPullParserException; {:try_start_3 .. :try_end_3} :catch_0
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_1
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto :goto_3

    :catch_1
    move-exception v2

    :try_start_4
    const-string v11, "bbb"

    const-string v12, "Unable to read timezones.xml file"

    invoke-static {v11, v12}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    if-eqz v10, :cond_1

    invoke-interface {v10}, Landroid/content/res/XmlResourceParser;->close()V

    goto :goto_2

    :cond_6
    :try_start_5
    invoke-interface {v10}, Landroid/content/res/XmlResourceParser;->next()I
    :try_end_5
    .catch Lorg/xmlpull/v1/XmlPullParserException; {:try_start_5 .. :try_end_5} :catch_0
    .catch Ljava/io/IOException; {:try_start_5 .. :try_end_5} :catch_1
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    goto/16 :goto_0

    :catchall_0
    move-exception v11

    if-eqz v10, :cond_7

    invoke-interface {v10}, Landroid/content/res/XmlResourceParser;->close()V

    :cond_7
    throw v11

    :cond_8
    :try_start_6
    iput v4, p0, Lcom/mediatek/appwidget/worldclock/ChooseActivity;->mCityNumberInXml:I

    invoke-interface {v10}, Landroid/content/res/XmlResourceParser;->close()V
    :try_end_6
    .catch Lorg/xmlpull/v1/XmlPullParserException; {:try_start_6 .. :try_end_6} :catch_0
    .catch Ljava/io/IOException; {:try_start_6 .. :try_end_6} :catch_1
    .catchall {:try_start_6 .. :try_end_6} :catchall_0

    if-eqz v10, :cond_1

    invoke-interface {v10}, Landroid/content/res/XmlResourceParser;->close()V

    goto/16 :goto_2
.end method

.method protected onActivityResult(IILandroid/content/Intent;)V
    .locals 3
    .param p1    # I
    .param p2    # I
    .param p3    # Landroid/content/Intent;

    const/16 v1, 0xa

    invoke-super {p0, p1, p2, p3}, Landroid/app/Activity;->onActivityResult(IILandroid/content/Intent;)V

    if-ne p1, v1, :cond_0

    if-ne p2, v1, :cond_0

    invoke-virtual {p3}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v1

    const-string v2, "citynamefromautoweatherid"

    invoke-virtual {v1, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/mediatek/appwidget/worldclock/ChooseActivity;->getData(Ljava/lang/String;)V

    :cond_0
    return-void
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 4
    .param p1    # Landroid/os/Bundle;

    invoke-super {p0, p1}, Landroid/app/Activity;->onCreate(Landroid/os/Bundle;)V

    const-string v1, "MTKWORLDCHOOSE"

    const-string v2, "on create ...."

    invoke-static {v1, v2}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    invoke-virtual {p0}, Landroid/app/Activity;->getIntent()Landroid/content/Intent;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v0

    if-eqz v0, :cond_0

    const-string v1, "onClickAppWidgetId"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v1

    sput v1, Lcom/mediatek/appwidget/worldclock/ChooseActivity;->mAppwidgetId:I

    :cond_0
    const-string v1, "MTKWORLDCHOOSE"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "mAppwidgetId = "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    sget v3, Lcom/mediatek/appwidget/worldclock/ChooseActivity;->mAppwidgetId:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    iput-object p0, p0, Lcom/mediatek/appwidget/worldclock/ChooseActivity;->mContext:Landroid/content/Context;

    const v1, 0x7f030001

    invoke-virtual {p0, v1}, Landroid/app/Activity;->setContentView(I)V

    const v1, 0x7f090003

    invoke-virtual {p0, v1}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ListView;

    iput-object v1, p0, Lcom/mediatek/appwidget/worldclock/ChooseActivity;->mListView:Landroid/widget/ListView;

    const v1, 0x7f090002

    invoke-virtual {p0, v1}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iput-object v1, p0, Lcom/mediatek/appwidget/worldclock/ChooseActivity;->mNoCity:Landroid/widget/TextView;

    return-void
.end method

.method public onCreateOptionsMenu(Landroid/view/Menu;)Z
    .locals 3
    .param p1    # Landroid/view/Menu;

    const/4 v2, 0x0

    const/4 v0, 0x1

    const v1, 0x7f07000b

    invoke-interface {p1, v2, v0, v2, v1}, Landroid/view/Menu;->add(IIII)Landroid/view/MenuItem;

    move-result-object v0

    const v1, 0x7f020002

    invoke-interface {v0, v1}, Landroid/view/MenuItem;->setIcon(I)Landroid/view/MenuItem;

    move-result-object v0

    const/4 v1, 0x2

    invoke-interface {v0, v1}, Landroid/view/MenuItem;->setShowAsAction(I)V

    invoke-super {p0, p1}, Landroid/app/Activity;->onCreateOptionsMenu(Landroid/view/Menu;)Z

    move-result v0

    return v0
.end method

.method public onMenuItemSelected(ILandroid/view/MenuItem;)Z
    .locals 2
    .param p1    # I
    .param p2    # Landroid/view/MenuItem;

    invoke-interface {p2}, Landroid/view/MenuItem;->getItemId()I

    move-result v1

    packed-switch v1, :pswitch_data_0

    invoke-super {p0, p1, p2}, Landroid/app/Activity;->onMenuItemSelected(ILandroid/view/MenuItem;)Z

    move-result v1

    :goto_0
    return v1

    :pswitch_0
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/mediatek/appwidget/worldclock/AutoCompleteTextViewActivity;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const/16 v1, 0xa

    invoke-virtual {p0, v0, v1}, Landroid/app/Activity;->startActivityForResult(Landroid/content/Intent;I)V

    const/4 v1, 0x1

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
    .end packed-switch
.end method

.method protected onNewIntent(Landroid/content/Intent;)V
    .locals 4
    .param p1    # Landroid/content/Intent;

    invoke-super {p0, p1}, Landroid/app/Activity;->onNewIntent(Landroid/content/Intent;)V

    invoke-virtual {p1}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v0

    if-eqz v0, :cond_0

    const-string v1, "onClickAppWidgetId"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v1

    sput v1, Lcom/mediatek/appwidget/worldclock/ChooseActivity;->mAppwidgetId:I

    :cond_0
    const-string v1, "MTKWORLDCHOOSE"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "onNewIntent mAppwidgetId = "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    sget v3, Lcom/mediatek/appwidget/worldclock/ChooseActivity;->mAppwidgetId:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    return-void
.end method

.method protected onRestoreInstanceState(Landroid/os/Bundle;)V
    .locals 3
    .param p1    # Landroid/os/Bundle;

    invoke-super {p0, p1}, Landroid/app/Activity;->onRestoreInstanceState(Landroid/os/Bundle;)V

    if-eqz p1, :cond_0

    const-string v0, "onClickAppWidgetId"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v0

    sput v0, Lcom/mediatek/appwidget/worldclock/ChooseActivity;->mAppwidgetId:I

    const-string v0, "MTKWORLDCHOOSE"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "onRestoreInstanceState mAppwidgetId = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    sget v2, Lcom/mediatek/appwidget/worldclock/ChooseActivity;->mAppwidgetId:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    return-void
.end method

.method protected onResume()V
    .locals 3

    invoke-super {p0}, Landroid/app/Activity;->onResume()V

    const-string v0, ""

    invoke-direct {p0, v0}, Lcom/mediatek/appwidget/worldclock/ChooseActivity;->getData(Ljava/lang/String;)V

    new-instance v0, Lcom/mediatek/appwidget/worldclock/SelectedCityListAdapter;

    iget-object v1, p0, Lcom/mediatek/appwidget/worldclock/ChooseActivity;->data:Ljava/util/ArrayList;

    sget v2, Lcom/mediatek/appwidget/worldclock/ChooseActivity;->mAppwidgetId:I

    invoke-direct {v0, p0, v1, v2}, Lcom/mediatek/appwidget/worldclock/SelectedCityListAdapter;-><init>(Landroid/content/Context;Ljava/util/ArrayList;I)V

    iput-object v0, p0, Lcom/mediatek/appwidget/worldclock/ChooseActivity;->mChooserAdapter:Lcom/mediatek/appwidget/worldclock/SelectedCityListAdapter;

    iget-object v0, p0, Lcom/mediatek/appwidget/worldclock/ChooseActivity;->data:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/mediatek/appwidget/worldclock/ChooseActivity;->mNoCity:Landroid/widget/TextView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    :goto_0
    iget-object v0, p0, Lcom/mediatek/appwidget/worldclock/ChooseActivity;->mListView:Landroid/widget/ListView;

    iget-object v1, p0, Lcom/mediatek/appwidget/worldclock/ChooseActivity;->mChooserAdapter:Lcom/mediatek/appwidget/worldclock/SelectedCityListAdapter;

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    iget-object v0, p0, Lcom/mediatek/appwidget/worldclock/ChooseActivity;->mListView:Landroid/widget/ListView;

    new-instance v1, Lcom/mediatek/appwidget/worldclock/ChooseActivity$1;

    invoke-direct {v1, p0}, Lcom/mediatek/appwidget/worldclock/ChooseActivity$1;-><init>(Lcom/mediatek/appwidget/worldclock/ChooseActivity;)V

    invoke-virtual {v0, v1}, Landroid/widget/AdapterView;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    iget-object v0, p0, Lcom/mediatek/appwidget/worldclock/ChooseActivity;->mListView:Landroid/widget/ListView;

    new-instance v1, Lcom/mediatek/appwidget/worldclock/ChooseActivity$2;

    invoke-direct {v1, p0}, Lcom/mediatek/appwidget/worldclock/ChooseActivity$2;-><init>(Lcom/mediatek/appwidget/worldclock/ChooseActivity;)V

    invoke-virtual {v0, v1}, Landroid/widget/AdapterView;->setOnItemLongClickListener(Landroid/widget/AdapterView$OnItemLongClickListener;)V

    sget v0, Lcom/mediatek/appwidget/worldclock/ChooseActivity;->mPosition:I

    const/4 v1, -0x1

    if-eq v0, v1, :cond_0

    iget-object v0, p0, Lcom/mediatek/appwidget/worldclock/ChooseActivity;->mListView:Landroid/widget/ListView;

    sget v1, Lcom/mediatek/appwidget/worldclock/ChooseActivity;->mPosition:I

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setSelection(I)V

    :cond_0
    return-void

    :cond_1
    iget-object v0, p0, Lcom/mediatek/appwidget/worldclock/ChooseActivity;->mNoCity:Landroid/widget/TextView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    goto :goto_0
.end method

.method protected onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 2
    .param p1    # Landroid/os/Bundle;

    invoke-super {p0, p1}, Landroid/app/Activity;->onSaveInstanceState(Landroid/os/Bundle;)V

    const-string v0, "onClickAppWidgetId"

    sget v1, Lcom/mediatek/appwidget/worldclock/ChooseActivity;->mAppwidgetId:I

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    return-void
.end method

.method protected onStart()V
    .locals 2

    invoke-super {p0}, Landroid/app/Activity;->onStart()V

    iget-object v0, p0, Lcom/mediatek/appwidget/worldclock/ChooseActivity;->mTimeZoneArray:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v0, "MTKWORLDCHOOSE"

    const-string v1, "mTimeZoneArray.isEmpty()"

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    invoke-virtual {p0}, Lcom/mediatek/appwidget/worldclock/ChooseActivity;->getZones()V

    :cond_0
    return-void
.end method
