.class Lcom/mediatek/appwidget/worldclock/AutoCompleteTextViewActivity$3;
.super Ljava/lang/Object;
.source "AutoCompleteTextViewActivity.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/mediatek/appwidget/worldclock/AutoCompleteTextViewActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/mediatek/appwidget/worldclock/AutoCompleteTextViewActivity;


# direct methods
.method constructor <init>(Lcom/mediatek/appwidget/worldclock/AutoCompleteTextViewActivity;)V
    .locals 0

    iput-object p1, p0, Lcom/mediatek/appwidget/worldclock/AutoCompleteTextViewActivity$3;->this$0:Lcom/mediatek/appwidget/worldclock/AutoCompleteTextViewActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 7
    .param p1    # Landroid/view/View;

    const/4 v0, 0x0

    const/4 v3, 0x0

    iget-object v4, p0, Lcom/mediatek/appwidget/worldclock/AutoCompleteTextViewActivity$3;->this$0:Lcom/mediatek/appwidget/worldclock/AutoCompleteTextViewActivity;

    invoke-static {v4}, Lcom/mediatek/appwidget/worldclock/AutoCompleteTextViewActivity;->access$000(Lcom/mediatek/appwidget/worldclock/AutoCompleteTextViewActivity;)Landroid/widget/AutoCompleteTextView;

    move-result-object v4

    invoke-virtual {v4}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v4

    invoke-static {v4}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    iget-object v4, p0, Lcom/mediatek/appwidget/worldclock/AutoCompleteTextViewActivity$3;->this$0:Lcom/mediatek/appwidget/worldclock/AutoCompleteTextViewActivity;

    iget-object v5, p0, Lcom/mediatek/appwidget/worldclock/AutoCompleteTextViewActivity$3;->this$0:Lcom/mediatek/appwidget/worldclock/AutoCompleteTextViewActivity;

    invoke-static {v5, v0}, Lcom/mediatek/appwidget/worldclock/AutoCompleteTextViewActivity;->access$200(Lcom/mediatek/appwidget/worldclock/AutoCompleteTextViewActivity;Ljava/lang/String;)Lcom/mediatek/appwidget/worldclock/ClockCityInfo;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/mediatek/appwidget/worldclock/AutoCompleteTextViewActivity;->access$102(Lcom/mediatek/appwidget/worldclock/AutoCompleteTextViewActivity;Lcom/mediatek/appwidget/worldclock/ClockCityInfo;)Lcom/mediatek/appwidget/worldclock/ClockCityInfo;

    const/4 v2, 0x0

    iget-object v4, p0, Lcom/mediatek/appwidget/worldclock/AutoCompleteTextViewActivity$3;->this$0:Lcom/mediatek/appwidget/worldclock/AutoCompleteTextViewActivity;

    invoke-static {v4}, Lcom/mediatek/appwidget/worldclock/AutoCompleteTextViewActivity;->access$100(Lcom/mediatek/appwidget/worldclock/AutoCompleteTextViewActivity;)Lcom/mediatek/appwidget/worldclock/ClockCityInfo;

    move-result-object v4

    invoke-virtual {v4}, Lcom/mediatek/appwidget/worldclock/ClockCityInfo;->getTimeZone()Ljava/lang/String;

    move-result-object v4

    if-nez v4, :cond_0

    iget-object v4, p0, Lcom/mediatek/appwidget/worldclock/AutoCompleteTextViewActivity$3;->this$0:Lcom/mediatek/appwidget/worldclock/AutoCompleteTextViewActivity;

    const v5, 0x7f070008

    const/4 v6, 0x0

    invoke-static {v4, v5, v6}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v4

    invoke-virtual {v4}, Landroid/widget/Toast;->show()V

    :goto_0
    return-void

    :cond_0
    new-instance v1, Landroid/content/Intent;

    invoke-direct {v1}, Landroid/content/Intent;-><init>()V

    const-string v4, "citynamefromauto"

    invoke-virtual {v1, v4, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const-string v4, "citynamefromautoweatherid"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v6, p0, Lcom/mediatek/appwidget/worldclock/AutoCompleteTextViewActivity$3;->this$0:Lcom/mediatek/appwidget/worldclock/AutoCompleteTextViewActivity;

    invoke-static {v6}, Lcom/mediatek/appwidget/worldclock/AutoCompleteTextViewActivity;->access$100(Lcom/mediatek/appwidget/worldclock/AutoCompleteTextViewActivity;)Lcom/mediatek/appwidget/worldclock/ClockCityInfo;

    move-result-object v6

    invoke-virtual {v6}, Lcom/mediatek/appwidget/worldclock/ClockCityInfo;->getWeatherID()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "        mm"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    const-string v4, "citynamefromautoweatherid"

    iget-object v5, p0, Lcom/mediatek/appwidget/worldclock/AutoCompleteTextViewActivity$3;->this$0:Lcom/mediatek/appwidget/worldclock/AutoCompleteTextViewActivity;

    invoke-static {v5}, Lcom/mediatek/appwidget/worldclock/AutoCompleteTextViewActivity;->access$100(Lcom/mediatek/appwidget/worldclock/AutoCompleteTextViewActivity;)Lcom/mediatek/appwidget/worldclock/ClockCityInfo;

    move-result-object v5

    invoke-virtual {v5}, Lcom/mediatek/appwidget/worldclock/ClockCityInfo;->getWeatherID()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v1, v4, v5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    iget-object v4, p0, Lcom/mediatek/appwidget/worldclock/AutoCompleteTextViewActivity$3;->this$0:Lcom/mediatek/appwidget/worldclock/AutoCompleteTextViewActivity;

    const/16 v5, 0xa

    invoke-virtual {v4, v5, v1}, Landroid/app/Activity;->setResult(ILandroid/content/Intent;)V

    iget-object v4, p0, Lcom/mediatek/appwidget/worldclock/AutoCompleteTextViewActivity$3;->this$0:Lcom/mediatek/appwidget/worldclock/AutoCompleteTextViewActivity;

    invoke-virtual {v4}, Landroid/app/Activity;->finish()V

    goto :goto_0
.end method
