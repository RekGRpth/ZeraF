.class public Lcom/mediatek/appwidget/worldclock/ClockCityInfo;
.super Ljava/lang/Object;
.source "ClockCityInfo.java"


# static fields
.field public static final CITY_NAME:I = 0x3

.field public static final INDEX:I = 0x0

.field public static final TIME_ZONE:I = 0x2

.field public static final WEATHER_ID:I = 0x1


# instance fields
.field private cityName:Ljava/lang/String;

.field private index:Ljava/lang/String;

.field private timeZone:Ljava/lang/String;

.field private weatherID:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const-string v0, "-1"

    iput-object v0, p0, Lcom/mediatek/appwidget/worldclock/ClockCityInfo;->index:Ljava/lang/String;

    const-string v0, ""

    iput-object v0, p0, Lcom/mediatek/appwidget/worldclock/ClockCityInfo;->weatherID:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 1
    .param p1    # Ljava/lang/String;
    .param p2    # Ljava/lang/String;
    .param p3    # Ljava/lang/String;
    .param p4    # Ljava/lang/String;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const-string v0, "-1"

    iput-object v0, p0, Lcom/mediatek/appwidget/worldclock/ClockCityInfo;->index:Ljava/lang/String;

    const-string v0, ""

    iput-object v0, p0, Lcom/mediatek/appwidget/worldclock/ClockCityInfo;->weatherID:Ljava/lang/String;

    iput-object p1, p0, Lcom/mediatek/appwidget/worldclock/ClockCityInfo;->index:Ljava/lang/String;

    iput-object p2, p0, Lcom/mediatek/appwidget/worldclock/ClockCityInfo;->weatherID:Ljava/lang/String;

    iput-object p3, p0, Lcom/mediatek/appwidget/worldclock/ClockCityInfo;->timeZone:Ljava/lang/String;

    iput-object p4, p0, Lcom/mediatek/appwidget/worldclock/ClockCityInfo;->cityName:Ljava/lang/String;

    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 3
    .param p1    # Ljava/lang/Object;

    const/4 v1, 0x0

    instance-of v2, p1, Lcom/mediatek/appwidget/worldclock/ClockCityInfo;

    if-eqz v2, :cond_0

    check-cast p1, Lcom/mediatek/appwidget/worldclock/ClockCityInfo;

    invoke-virtual {p1}, Lcom/mediatek/appwidget/worldclock/ClockCityInfo;->getCityName()Ljava/lang/String;

    move-result-object v0

    iget-object v2, p0, Lcom/mediatek/appwidget/worldclock/ClockCityInfo;->cityName:Ljava/lang/String;

    if-eqz v2, :cond_0

    if-eqz v0, :cond_0

    iget-object v2, p0, Lcom/mediatek/appwidget/worldclock/ClockCityInfo;->cityName:Ljava/lang/String;

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    :cond_0
    return v1
.end method

.method public getCityName()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/mediatek/appwidget/worldclock/ClockCityInfo;->cityName:Ljava/lang/String;

    return-object v0
.end method

.method public getIndex()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/mediatek/appwidget/worldclock/ClockCityInfo;->index:Ljava/lang/String;

    return-object v0
.end method

.method public getTimeZone()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/mediatek/appwidget/worldclock/ClockCityInfo;->timeZone:Ljava/lang/String;

    return-object v0
.end method

.method public getWeatherID()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/mediatek/appwidget/worldclock/ClockCityInfo;->weatherID:Ljava/lang/String;

    return-object v0
.end method

.method public setCityName(Ljava/lang/String;)V
    .locals 0
    .param p1    # Ljava/lang/String;

    iput-object p1, p0, Lcom/mediatek/appwidget/worldclock/ClockCityInfo;->cityName:Ljava/lang/String;

    return-void
.end method

.method public setIndex(Ljava/lang/String;)V
    .locals 0
    .param p1    # Ljava/lang/String;

    iput-object p1, p0, Lcom/mediatek/appwidget/worldclock/ClockCityInfo;->index:Ljava/lang/String;

    return-void
.end method

.method public setTimeZone(Ljava/lang/String;)V
    .locals 0
    .param p1    # Ljava/lang/String;

    iput-object p1, p0, Lcom/mediatek/appwidget/worldclock/ClockCityInfo;->timeZone:Ljava/lang/String;

    return-void
.end method

.method public setWeatherID(Ljava/lang/String;)V
    .locals 0
    .param p1    # Ljava/lang/String;

    iput-object p1, p0, Lcom/mediatek/appwidget/worldclock/ClockCityInfo;->weatherID:Ljava/lang/String;

    return-void
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "index:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/mediatek/appwidget/worldclock/ClockCityInfo;->index:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ",weatherID:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/mediatek/appwidget/worldclock/ClockCityInfo;->weatherID:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ",timeZone:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/mediatek/appwidget/worldclock/ClockCityInfo;->timeZone:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ",cityName:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/mediatek/appwidget/worldclock/ClockCityInfo;->cityName:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public toStringArray()[Ljava/lang/String;
    .locals 3

    const/4 v1, 0x4

    new-array v0, v1, [Ljava/lang/String;

    const/4 v1, 0x0

    iget-object v2, p0, Lcom/mediatek/appwidget/worldclock/ClockCityInfo;->index:Ljava/lang/String;

    aput-object v2, v0, v1

    const/4 v1, 0x1

    iget-object v2, p0, Lcom/mediatek/appwidget/worldclock/ClockCityInfo;->weatherID:Ljava/lang/String;

    aput-object v2, v0, v1

    const/4 v1, 0x2

    iget-object v2, p0, Lcom/mediatek/appwidget/worldclock/ClockCityInfo;->timeZone:Ljava/lang/String;

    aput-object v2, v0, v1

    const/4 v1, 0x3

    iget-object v2, p0, Lcom/mediatek/appwidget/worldclock/ClockCityInfo;->cityName:Ljava/lang/String;

    aput-object v2, v0, v1

    return-object v0
.end method
