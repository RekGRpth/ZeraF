.class public Lcom/mediatek/appwidget/worldclock/WorldClockWidgetProvider;
.super Landroid/appwidget/AppWidgetProvider;
.source "WorldClockWidgetProvider.java"


# static fields
.field private static final ANOTHER_BUTTON:I = 0x1

.field private static final DELETE_INTENT:Ljava/lang/String; = "android.intent.action.mtk.worldclock.deleteIntent"

.field private static final ON_CLICK_APPWIDGETID:Ljava/lang/String; = "onClickAppWidgetId"

.field private static final TAG:Ljava/lang/String; = "WorldClockWidgetProvider"


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Landroid/appwidget/AppWidgetProvider;-><init>()V

    return-void
.end method

.method private static createPendingIntent(Landroid/content/Context;II)Landroid/app/PendingIntent;
    .locals 7
    .param p0    # Landroid/content/Context;
    .param p1    # I
    .param p2    # I

    const/4 v6, 0x0

    new-instance v1, Landroid/content/Intent;

    invoke-direct {v1}, Landroid/content/Intent;-><init>()V

    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    const-string v3, "WorldClockWidgetProvider"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "setPendingIntent appWidgetId = "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/mediatek/xlog/Xlog;->d(Ljava/lang/String;Ljava/lang/String;)I

    const-string v3, "onClickAppWidgetId"

    invoke-virtual {v0, v3, p1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    invoke-virtual {p0}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v3

    const-class v4, Lcom/mediatek/appwidget/worldclock/ChooseActivity;

    invoke-virtual {v4}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v1, v3, v4}, Landroid/content/Intent;->setClassName(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "android.intent.action.VIEW"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v3}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    invoke-virtual {v1, v0}, Landroid/content/Intent;->putExtras(Landroid/os/Bundle;)Landroid/content/Intent;

    invoke-static {p0, v6, v1, v6}, Landroid/app/PendingIntent;->getActivity(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v2

    return-object v2
.end method

.method public static updateCity(Landroid/content/Context;ILcom/mediatek/appwidget/worldclock/ClockCityInfo;)V
    .locals 11
    .param p0    # Landroid/content/Context;
    .param p1    # I
    .param p2    # Lcom/mediatek/appwidget/worldclock/ClockCityInfo;

    const/4 v10, 0x1

    const v9, 0x7f090009

    const/4 v8, 0x0

    const v7, 0x7f090008

    const v6, 0x7f09000a

    invoke-static {p0}, Landroid/appwidget/AppWidgetManager;->getInstance(Landroid/content/Context;)Landroid/appwidget/AppWidgetManager;

    move-result-object v0

    new-instance v3, Landroid/widget/RemoteViews;

    invoke-virtual {p0}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v4

    const v5, 0x7f030003

    invoke-direct {v3, v4, v5}, Landroid/widget/RemoteViews;-><init>(Ljava/lang/String;I)V

    invoke-virtual {p2}, Lcom/mediatek/appwidget/worldclock/ClockCityInfo;->getCityName()Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_3

    const-string v4, ","

    invoke-virtual {v1, v4}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v4

    if-eqz v4, :cond_0

    const-string v4, ","

    invoke-virtual {v1, v4}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v2

    array-length v4, v2

    const/4 v5, 0x2

    if-ne v4, v5, :cond_2

    aget-object v4, v2, v10

    const-string v5, "D.C."

    invoke-virtual {v4, v5}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v4

    if-eqz v4, :cond_2

    :cond_0
    :goto_0
    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v4

    const/16 v5, 0xa

    if-le v4, v5, :cond_1

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const/16 v5, 0x9

    invoke-virtual {v1, v8, v5}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " ..."

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    :cond_1
    const v4, 0x7f09000c

    invoke-virtual {v3, v4, v1}, Landroid/widget/RemoteViews;->setTextViewText(ILjava/lang/CharSequence;)V

    :goto_1
    invoke-static {p0, p1, v10}, Lcom/mediatek/appwidget/worldclock/WorldClockWidgetProvider;->createPendingIntent(Landroid/content/Context;II)Landroid/app/PendingIntent;

    move-result-object v4

    invoke-virtual {v3, v9, v4}, Landroid/widget/RemoteViews;->setOnClickPendingIntent(ILandroid/app/PendingIntent;)V

    invoke-virtual {v3, v9, v8}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    const/4 v4, 0x4

    invoke-virtual {v3, v6, v4}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    invoke-virtual {p2}, Lcom/mediatek/appwidget/worldclock/ClockCityInfo;->getCityName()Ljava/lang/String;

    move-result-object v4

    if-nez v4, :cond_4

    invoke-virtual {v3, v9, v8}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    const/4 v4, 0x4

    invoke-virtual {v3, v6, v4}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    :goto_2
    const-string v4, "setDateHeight"

    const v5, 0x7f060001

    invoke-virtual {v3, v7, v4, v5}, Landroid/widget/RemoteViews;->setInt(ILjava/lang/String;I)V

    const-string v4, "setDateFormatString"

    const/high16 v5, 0x7f070000

    invoke-virtual {v3, v7, v4, v5}, Landroid/widget/RemoteViews;->setInt(ILjava/lang/String;I)V

    const-string v4, "setDateFontSize"

    const/high16 v5, 0x7f060000

    invoke-virtual {v3, v7, v4, v5}, Landroid/widget/RemoteViews;->setInt(ILjava/lang/String;I)V

    const-string v4, "setDateHeight"

    const v5, 0x7f060001

    invoke-virtual {v3, v6, v4, v5}, Landroid/widget/RemoteViews;->setInt(ILjava/lang/String;I)V

    const-string v4, "setDateFormatString"

    const/high16 v5, 0x7f070000

    invoke-virtual {v3, v6, v4, v5}, Landroid/widget/RemoteViews;->setInt(ILjava/lang/String;I)V

    const-string v4, "setDateFontSize"

    const/high16 v5, 0x7f060000

    invoke-virtual {v3, v6, v4, v5}, Landroid/widget/RemoteViews;->setInt(ILjava/lang/String;I)V

    const-string v4, "setDayDialResource"

    const v5, 0x7f020006

    invoke-virtual {v3, v7, v4, v5}, Landroid/widget/RemoteViews;->setInt(ILjava/lang/String;I)V

    const-string v4, "setDayHourResource"

    const v5, 0x7f020008

    invoke-virtual {v3, v7, v4, v5}, Landroid/widget/RemoteViews;->setInt(ILjava/lang/String;I)V

    const-string v4, "setDayMinuteResource"

    const v5, 0x7f020009

    invoke-virtual {v3, v7, v4, v5}, Landroid/widget/RemoteViews;->setInt(ILjava/lang/String;I)V

    const-string v4, "setNightDialResource"

    const v5, 0x7f02000a

    invoke-virtual {v3, v7, v4, v5}, Landroid/widget/RemoteViews;->setInt(ILjava/lang/String;I)V

    const-string v4, "setNightHourResource"

    const v5, 0x7f02000c

    invoke-virtual {v3, v7, v4, v5}, Landroid/widget/RemoteViews;->setInt(ILjava/lang/String;I)V

    const-string v4, "setNightMinuteResource"

    const v5, 0x7f02000d

    invoke-virtual {v3, v7, v4, v5}, Landroid/widget/RemoteViews;->setInt(ILjava/lang/String;I)V

    const-string v4, "setDayHatResource"

    const v5, 0x7f020007

    invoke-virtual {v3, v7, v4, v5}, Landroid/widget/RemoteViews;->setInt(ILjava/lang/String;I)V

    const-string v4, "setNightHatResource"

    const v5, 0x7f02000b

    invoke-virtual {v3, v7, v4, v5}, Landroid/widget/RemoteViews;->setInt(ILjava/lang/String;I)V

    const-string v4, "setDayDialResource"

    const v5, 0x7f020006

    invoke-virtual {v3, v6, v4, v5}, Landroid/widget/RemoteViews;->setInt(ILjava/lang/String;I)V

    const-string v4, "setDayHourResource"

    const v5, 0x7f020008

    invoke-virtual {v3, v6, v4, v5}, Landroid/widget/RemoteViews;->setInt(ILjava/lang/String;I)V

    const-string v4, "setDayMinuteResource"

    const v5, 0x7f020009

    invoke-virtual {v3, v6, v4, v5}, Landroid/widget/RemoteViews;->setInt(ILjava/lang/String;I)V

    const-string v4, "setNightDialResource"

    const v5, 0x7f02000a

    invoke-virtual {v3, v6, v4, v5}, Landroid/widget/RemoteViews;->setInt(ILjava/lang/String;I)V

    const-string v4, "setNightHourResource"

    const v5, 0x7f02000c

    invoke-virtual {v3, v6, v4, v5}, Landroid/widget/RemoteViews;->setInt(ILjava/lang/String;I)V

    const-string v4, "setNightMinuteResource"

    const v5, 0x7f02000d

    invoke-virtual {v3, v6, v4, v5}, Landroid/widget/RemoteViews;->setInt(ILjava/lang/String;I)V

    const-string v4, "setDayHatResource"

    const v5, 0x7f020007

    invoke-virtual {v3, v6, v4, v5}, Landroid/widget/RemoteViews;->setInt(ILjava/lang/String;I)V

    const-string v4, "setNightHatResource"

    const v5, 0x7f02000b

    invoke-virtual {v3, v6, v4, v5}, Landroid/widget/RemoteViews;->setInt(ILjava/lang/String;I)V

    const-string v4, "setTimeZone"

    invoke-virtual {p2}, Lcom/mediatek/appwidget/worldclock/ClockCityInfo;->getTimeZone()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v3, v6, v4, v5}, Landroid/widget/RemoteViews;->setString(ILjava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v0, p1, v3}, Landroid/appwidget/AppWidgetManager;->updateAppWidget(ILandroid/widget/RemoteViews;)V

    return-void

    :cond_2
    array-length v4, v2

    add-int/lit8 v4, v4, -0x1

    aget-object v4, v2, v4

    invoke-virtual {v4}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v1

    goto/16 :goto_0

    :cond_3
    const v4, 0x7f09000c

    const v5, 0x7f070003

    invoke-virtual {p0, v5}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v3, v4, v5}, Landroid/widget/RemoteViews;->setTextViewText(ILjava/lang/CharSequence;)V

    goto/16 :goto_1

    :cond_4
    const/4 v4, 0x4

    invoke-virtual {v3, v9, v4}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    invoke-virtual {v3, v6, v8}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    invoke-static {p0, p1, v10}, Lcom/mediatek/appwidget/worldclock/WorldClockWidgetProvider;->createPendingIntent(Landroid/content/Context;II)Landroid/app/PendingIntent;

    move-result-object v4

    invoke-virtual {v3, v6, v4}, Landroid/widget/RemoteViews;->setOnClickPendingIntent(ILandroid/app/PendingIntent;)V

    goto/16 :goto_2
.end method


# virtual methods
.method public onDeleted(Landroid/content/Context;[I)V
    .locals 0
    .param p1    # Landroid/content/Context;
    .param p2    # [I

    invoke-static {p2}, Lcom/mediatek/appwidget/worldclock/ClockCityUtils;->deletePreferences([I)V

    return-void
.end method

.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 8
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/content/Intent;

    const-string v5, "android.intent.action.mtk.worldclock.deleteIntent"

    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_1

    const-string v5, "weatherNameDelete"

    invoke-virtual {p2, v5}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    if-eqz v4, :cond_1

    const-string v5, ""

    invoke-virtual {v4}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-nez v5, :cond_1

    invoke-static {p1}, Lcom/mediatek/appwidget/worldclock/ClockCityUtils;->initPreference(Landroid/content/Context;)V

    invoke-static {p1}, Landroid/appwidget/AppWidgetManager;->getInstance(Landroid/content/Context;)Landroid/appwidget/AppWidgetManager;

    move-result-object v0

    invoke-static {v4}, Lcom/mediatek/appwidget/worldclock/ClockCityUtils;->getIntDelete(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v3

    if-eqz v3, :cond_1

    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    move-result v5

    if-lez v5, :cond_1

    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    move-result v5

    new-array v2, v5, [I

    const/4 v1, 0x0

    :goto_0
    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    move-result v5

    if-ge v1, v5, :cond_0

    invoke-virtual {v3, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/lang/Integer;

    invoke-virtual {v5}, Ljava/lang/Integer;->intValue()I

    move-result v5

    aput v5, v2, v1

    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_0
    invoke-static {v2}, Lcom/mediatek/appwidget/worldclock/ClockCityUtils;->deletePreferences([I)V

    const-string v5, "WorldClockWidgetProvider"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "WorldClockWidgetProvider delete city weatherNameDelete = "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Lcom/mediatek/xlog/Xlog;->d(Ljava/lang/String;Ljava/lang/String;)I

    invoke-virtual {p0, p1, v0, v2}, Lcom/mediatek/appwidget/worldclock/WorldClockWidgetProvider;->onUpdate(Landroid/content/Context;Landroid/appwidget/AppWidgetManager;[I)V

    :cond_1
    invoke-super {p0, p1, p2}, Landroid/appwidget/AppWidgetProvider;->onReceive(Landroid/content/Context;Landroid/content/Intent;)V

    return-void
.end method

.method public onUpdate(Landroid/content/Context;Landroid/appwidget/AppWidgetManager;[I)V
    .locals 9
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/appwidget/AppWidgetManager;
    .param p3    # [I

    invoke-static {p1}, Lcom/mediatek/appwidget/worldclock/ClockCityUtils;->initPreference(Landroid/content/Context;)V

    array-length v6, p3

    const-string v0, "WorldClockWidgetProvider"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "onUpdate N = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/mediatek/xlog/Xlog;->d(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v8, 0x0

    :goto_0
    if-ge v8, v6, :cond_1

    aget v5, p3, v8

    invoke-static {v5}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/mediatek/appwidget/worldclock/ClockCityUtils;->getCityCPref(Ljava/lang/String;)Lcom/mediatek/appwidget/worldclock/ClockCityInfo;

    move-result-object v7

    invoke-virtual {v7}, Lcom/mediatek/appwidget/worldclock/ClockCityInfo;->getWeatherID()Ljava/lang/String;

    move-result-object v3

    const-string v0, "WorldClockWidgetProvider"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "cityInfo = cityName = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v7}, Lcom/mediatek/appwidget/worldclock/ClockCityInfo;->getCityName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/mediatek/xlog/Xlog;->d(Ljava/lang/String;Ljava/lang/String;)I

    invoke-static {p1, v5, v7}, Lcom/mediatek/appwidget/worldclock/WorldClockWidgetProvider;->updateCity(Landroid/content/Context;ILcom/mediatek/appwidget/worldclock/ClockCityInfo;)V

    invoke-virtual {v7}, Lcom/mediatek/appwidget/worldclock/ClockCityInfo;->getCityName()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    move-object v4, v7

    new-instance v0, Lcom/mediatek/appwidget/worldclock/WorldClockWidgetProvider$1;

    move-object v1, p0

    move-object v2, p1

    invoke-direct/range {v0 .. v5}, Lcom/mediatek/appwidget/worldclock/WorldClockWidgetProvider$1;-><init>(Lcom/mediatek/appwidget/worldclock/WorldClockWidgetProvider;Landroid/content/Context;Ljava/lang/String;Lcom/mediatek/appwidget/worldclock/ClockCityInfo;I)V

    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/Void;

    invoke-virtual {v0, v1}, Landroid/os/AsyncTask;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    :cond_0
    add-int/lit8 v8, v8, 0x1

    goto :goto_0

    :cond_1
    return-void
.end method
