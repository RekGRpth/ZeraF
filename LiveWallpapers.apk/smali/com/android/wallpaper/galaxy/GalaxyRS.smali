.class Lcom/android/wallpaper/galaxy/GalaxyRS;
.super Lcom/android/wallpaper/RenderScriptScene;
.source "GalaxyRS.java"


# static fields
.field private static final PARTICLES_COUNT:I = 0x2ee0


# instance fields
.field private final mOptionsARGB:Landroid/graphics/BitmapFactory$Options;

.field private mParticlesMesh:Landroid/renderscript/Mesh;

.field private mPvOrthoAlloc:Landroid/renderscript/ProgramVertexFixedFunction$Constants;

.field private mPvProjectionAlloc:Landroid/renderscript/ProgramVertexFixedFunction$Constants;

.field private mPvStarAlloc:Lcom/android/wallpaper/galaxy/ScriptField_VpConsts;

.field private mScript:Lcom/android/wallpaper/galaxy/ScriptC_galaxy;


# direct methods
.method constructor <init>(II)V
    .locals 2
    .param p1    # I
    .param p2    # I

    invoke-direct {p0, p1, p2}, Lcom/android/wallpaper/RenderScriptScene;-><init>(II)V

    new-instance v0, Landroid/graphics/BitmapFactory$Options;

    invoke-direct {v0}, Landroid/graphics/BitmapFactory$Options;-><init>()V

    iput-object v0, p0, Lcom/android/wallpaper/galaxy/GalaxyRS;->mOptionsARGB:Landroid/graphics/BitmapFactory$Options;

    iget-object v0, p0, Lcom/android/wallpaper/galaxy/GalaxyRS;->mOptionsARGB:Landroid/graphics/BitmapFactory$Options;

    const/4 v1, 0x0

    iput-boolean v1, v0, Landroid/graphics/BitmapFactory$Options;->inScaled:Z

    iget-object v0, p0, Lcom/android/wallpaper/galaxy/GalaxyRS;->mOptionsARGB:Landroid/graphics/BitmapFactory$Options;

    sget-object v1, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    iput-object v1, v0, Landroid/graphics/BitmapFactory$Options;->inPreferredConfig:Landroid/graphics/Bitmap$Config;

    return-void
.end method

.method private createParticlesMesh()V
    .locals 5

    new-instance v1, Lcom/android/wallpaper/galaxy/ScriptField_Particle;

    iget-object v3, p0, Lcom/android/wallpaper/RenderScriptScene;->mRS:Landroid/renderscript/RenderScriptGL;

    const/16 v4, 0x2ee0

    invoke-direct {v1, v3, v4}, Lcom/android/wallpaper/galaxy/ScriptField_Particle;-><init>(Landroid/renderscript/RenderScript;I)V

    new-instance v0, Landroid/renderscript/Mesh$AllocationBuilder;

    iget-object v3, p0, Lcom/android/wallpaper/RenderScriptScene;->mRS:Landroid/renderscript/RenderScriptGL;

    invoke-direct {v0, v3}, Landroid/renderscript/Mesh$AllocationBuilder;-><init>(Landroid/renderscript/RenderScript;)V

    invoke-virtual {v1}, Landroid/renderscript/Script$FieldBase;->getAllocation()Landroid/renderscript/Allocation;

    move-result-object v3

    invoke-virtual {v0, v3}, Landroid/renderscript/Mesh$AllocationBuilder;->addVertexAllocation(Landroid/renderscript/Allocation;)Landroid/renderscript/Mesh$AllocationBuilder;

    invoke-virtual {v0}, Landroid/renderscript/Mesh$AllocationBuilder;->getCurrentVertexTypeIndex()I

    move-result v2

    sget-object v3, Landroid/renderscript/Mesh$Primitive;->POINT:Landroid/renderscript/Mesh$Primitive;

    invoke-virtual {v0, v3}, Landroid/renderscript/Mesh$AllocationBuilder;->addIndexSetType(Landroid/renderscript/Mesh$Primitive;)Landroid/renderscript/Mesh$AllocationBuilder;

    invoke-virtual {v0}, Landroid/renderscript/Mesh$AllocationBuilder;->create()Landroid/renderscript/Mesh;

    move-result-object v3

    iput-object v3, p0, Lcom/android/wallpaper/galaxy/GalaxyRS;->mParticlesMesh:Landroid/renderscript/Mesh;

    iget-object v3, p0, Lcom/android/wallpaper/galaxy/GalaxyRS;->mScript:Lcom/android/wallpaper/galaxy/ScriptC_galaxy;

    iget-object v4, p0, Lcom/android/wallpaper/galaxy/GalaxyRS;->mParticlesMesh:Landroid/renderscript/Mesh;

    invoke-virtual {v3, v4}, Lcom/android/wallpaper/galaxy/ScriptC_galaxy;->set_gParticlesMesh(Landroid/renderscript/Mesh;)V

    iget-object v3, p0, Lcom/android/wallpaper/galaxy/GalaxyRS;->mScript:Lcom/android/wallpaper/galaxy/ScriptC_galaxy;

    invoke-virtual {v3, v1}, Lcom/android/wallpaper/galaxy/ScriptC_galaxy;->bind_Particles(Lcom/android/wallpaper/galaxy/ScriptField_Particle;)V

    return-void
.end method

.method private createProgramFragment()V
    .locals 7

    const/4 v6, 0x1

    const/4 v5, 0x0

    new-instance v0, Landroid/renderscript/ProgramFragmentFixedFunction$Builder;

    iget-object v3, p0, Lcom/android/wallpaper/RenderScriptScene;->mRS:Landroid/renderscript/RenderScriptGL;

    invoke-direct {v0, v3}, Landroid/renderscript/ProgramFragmentFixedFunction$Builder;-><init>(Landroid/renderscript/RenderScript;)V

    sget-object v3, Landroid/renderscript/ProgramFragmentFixedFunction$Builder$EnvMode;->REPLACE:Landroid/renderscript/ProgramFragmentFixedFunction$Builder$EnvMode;

    sget-object v4, Landroid/renderscript/ProgramFragmentFixedFunction$Builder$Format;->RGB:Landroid/renderscript/ProgramFragmentFixedFunction$Builder$Format;

    invoke-virtual {v0, v3, v4, v5}, Landroid/renderscript/ProgramFragmentFixedFunction$Builder;->setTexture(Landroid/renderscript/ProgramFragmentFixedFunction$Builder$EnvMode;Landroid/renderscript/ProgramFragmentFixedFunction$Builder$Format;I)Landroid/renderscript/ProgramFragmentFixedFunction$Builder;

    invoke-virtual {v0}, Landroid/renderscript/ProgramFragmentFixedFunction$Builder;->create()Landroid/renderscript/ProgramFragmentFixedFunction;

    move-result-object v1

    iget-object v3, p0, Lcom/android/wallpaper/RenderScriptScene;->mRS:Landroid/renderscript/RenderScriptGL;

    invoke-static {v3}, Landroid/renderscript/Sampler;->WRAP_NEAREST(Landroid/renderscript/RenderScript;)Landroid/renderscript/Sampler;

    move-result-object v3

    invoke-virtual {v1, v3, v5}, Landroid/renderscript/Program;->bindSampler(Landroid/renderscript/Sampler;I)V

    iget-object v3, p0, Lcom/android/wallpaper/galaxy/GalaxyRS;->mScript:Lcom/android/wallpaper/galaxy/ScriptC_galaxy;

    invoke-virtual {v3, v1}, Lcom/android/wallpaper/galaxy/ScriptC_galaxy;->set_gPFBackground(Landroid/renderscript/ProgramFragment;)V

    new-instance v0, Landroid/renderscript/ProgramFragmentFixedFunction$Builder;

    iget-object v3, p0, Lcom/android/wallpaper/RenderScriptScene;->mRS:Landroid/renderscript/RenderScriptGL;

    invoke-direct {v0, v3}, Landroid/renderscript/ProgramFragmentFixedFunction$Builder;-><init>(Landroid/renderscript/RenderScript;)V

    invoke-virtual {v0, v6}, Landroid/renderscript/ProgramFragmentFixedFunction$Builder;->setPointSpriteTexCoordinateReplacement(Z)Landroid/renderscript/ProgramFragmentFixedFunction$Builder;

    sget-object v3, Landroid/renderscript/ProgramFragmentFixedFunction$Builder$EnvMode;->MODULATE:Landroid/renderscript/ProgramFragmentFixedFunction$Builder$EnvMode;

    sget-object v4, Landroid/renderscript/ProgramFragmentFixedFunction$Builder$Format;->RGBA:Landroid/renderscript/ProgramFragmentFixedFunction$Builder$Format;

    invoke-virtual {v0, v3, v4, v5}, Landroid/renderscript/ProgramFragmentFixedFunction$Builder;->setTexture(Landroid/renderscript/ProgramFragmentFixedFunction$Builder$EnvMode;Landroid/renderscript/ProgramFragmentFixedFunction$Builder$Format;I)Landroid/renderscript/ProgramFragmentFixedFunction$Builder;

    invoke-virtual {v0, v6}, Landroid/renderscript/ProgramFragmentFixedFunction$Builder;->setVaryingColor(Z)Landroid/renderscript/ProgramFragmentFixedFunction$Builder;

    invoke-virtual {v0}, Landroid/renderscript/ProgramFragmentFixedFunction$Builder;->create()Landroid/renderscript/ProgramFragmentFixedFunction;

    move-result-object v2

    iget-object v3, p0, Lcom/android/wallpaper/RenderScriptScene;->mRS:Landroid/renderscript/RenderScriptGL;

    invoke-static {v3}, Landroid/renderscript/Sampler;->WRAP_LINEAR(Landroid/renderscript/RenderScript;)Landroid/renderscript/Sampler;

    move-result-object v3

    invoke-virtual {v2, v3, v5}, Landroid/renderscript/Program;->bindSampler(Landroid/renderscript/Sampler;I)V

    iget-object v3, p0, Lcom/android/wallpaper/galaxy/GalaxyRS;->mScript:Lcom/android/wallpaper/galaxy/ScriptC_galaxy;

    invoke-virtual {v3, v2}, Lcom/android/wallpaper/galaxy/ScriptC_galaxy;->set_gPFStars(Landroid/renderscript/ProgramFragment;)V

    return-void
.end method

.method private createProgramFragmentStore()V
    .locals 3

    new-instance v0, Landroid/renderscript/ProgramStore$Builder;

    iget-object v1, p0, Lcom/android/wallpaper/RenderScriptScene;->mRS:Landroid/renderscript/RenderScriptGL;

    invoke-direct {v0, v1}, Landroid/renderscript/ProgramStore$Builder;-><init>(Landroid/renderscript/RenderScript;)V

    sget-object v1, Landroid/renderscript/ProgramStore$BlendSrcFunc;->ONE:Landroid/renderscript/ProgramStore$BlendSrcFunc;

    sget-object v2, Landroid/renderscript/ProgramStore$BlendDstFunc;->ZERO:Landroid/renderscript/ProgramStore$BlendDstFunc;

    invoke-virtual {v0, v1, v2}, Landroid/renderscript/ProgramStore$Builder;->setBlendFunc(Landroid/renderscript/ProgramStore$BlendSrcFunc;Landroid/renderscript/ProgramStore$BlendDstFunc;)Landroid/renderscript/ProgramStore$Builder;

    iget-object v1, p0, Lcom/android/wallpaper/RenderScriptScene;->mRS:Landroid/renderscript/RenderScriptGL;

    invoke-virtual {v0}, Landroid/renderscript/ProgramStore$Builder;->create()Landroid/renderscript/ProgramStore;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/renderscript/RenderScriptGL;->bindProgramStore(Landroid/renderscript/ProgramStore;)V

    sget-object v1, Landroid/renderscript/ProgramStore$BlendSrcFunc;->SRC_ALPHA:Landroid/renderscript/ProgramStore$BlendSrcFunc;

    sget-object v2, Landroid/renderscript/ProgramStore$BlendDstFunc;->ONE:Landroid/renderscript/ProgramStore$BlendDstFunc;

    invoke-virtual {v0, v1, v2}, Landroid/renderscript/ProgramStore$Builder;->setBlendFunc(Landroid/renderscript/ProgramStore$BlendSrcFunc;Landroid/renderscript/ProgramStore$BlendDstFunc;)Landroid/renderscript/ProgramStore$Builder;

    iget-object v1, p0, Lcom/android/wallpaper/galaxy/GalaxyRS;->mScript:Lcom/android/wallpaper/galaxy/ScriptC_galaxy;

    invoke-virtual {v0}, Landroid/renderscript/ProgramStore$Builder;->create()Landroid/renderscript/ProgramStore;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/android/wallpaper/galaxy/ScriptC_galaxy;->set_gPSLights(Landroid/renderscript/ProgramStore;)V

    return-void
.end method

.method private createProgramRaster()V
    .locals 3

    new-instance v0, Landroid/renderscript/ProgramRaster$Builder;

    iget-object v2, p0, Lcom/android/wallpaper/RenderScriptScene;->mRS:Landroid/renderscript/RenderScriptGL;

    invoke-direct {v0, v2}, Landroid/renderscript/ProgramRaster$Builder;-><init>(Landroid/renderscript/RenderScript;)V

    const/4 v2, 0x1

    invoke-virtual {v0, v2}, Landroid/renderscript/ProgramRaster$Builder;->setPointSpriteEnabled(Z)Landroid/renderscript/ProgramRaster$Builder;

    invoke-virtual {v0}, Landroid/renderscript/ProgramRaster$Builder;->create()Landroid/renderscript/ProgramRaster;

    move-result-object v1

    iget-object v2, p0, Lcom/android/wallpaper/RenderScriptScene;->mRS:Landroid/renderscript/RenderScriptGL;

    invoke-virtual {v2, v1}, Landroid/renderscript/RenderScriptGL;->bindProgramRaster(Landroid/renderscript/ProgramRaster;)V

    return-void
.end method

.method private createProgramVertex()V
    .locals 10

    const/4 v9, 0x0

    new-instance v6, Landroid/renderscript/ProgramVertexFixedFunction$Constants;

    iget-object v7, p0, Lcom/android/wallpaper/RenderScriptScene;->mRS:Landroid/renderscript/RenderScriptGL;

    invoke-direct {v6, v7}, Landroid/renderscript/ProgramVertexFixedFunction$Constants;-><init>(Landroid/renderscript/RenderScript;)V

    iput-object v6, p0, Lcom/android/wallpaper/galaxy/GalaxyRS;->mPvOrthoAlloc:Landroid/renderscript/ProgramVertexFixedFunction$Constants;

    new-instance v0, Landroid/renderscript/ProgramVertexFixedFunction$Builder;

    iget-object v6, p0, Lcom/android/wallpaper/RenderScriptScene;->mRS:Landroid/renderscript/RenderScriptGL;

    invoke-direct {v0, v6}, Landroid/renderscript/ProgramVertexFixedFunction$Builder;-><init>(Landroid/renderscript/RenderScript;)V

    invoke-virtual {v0}, Landroid/renderscript/ProgramVertexFixedFunction$Builder;->create()Landroid/renderscript/ProgramVertexFixedFunction;

    move-result-object v1

    move-object v6, v1

    check-cast v6, Landroid/renderscript/ProgramVertexFixedFunction;

    iget-object v7, p0, Lcom/android/wallpaper/galaxy/GalaxyRS;->mPvOrthoAlloc:Landroid/renderscript/ProgramVertexFixedFunction$Constants;

    invoke-virtual {v6, v7}, Landroid/renderscript/ProgramVertexFixedFunction;->bindConstants(Landroid/renderscript/ProgramVertexFixedFunction$Constants;)V

    iget-object v6, p0, Lcom/android/wallpaper/RenderScriptScene;->mRS:Landroid/renderscript/RenderScriptGL;

    invoke-virtual {v6, v1}, Landroid/renderscript/RenderScriptGL;->bindProgramVertex(Landroid/renderscript/ProgramVertex;)V

    new-instance v6, Lcom/android/wallpaper/galaxy/ScriptField_VpConsts;

    iget-object v7, p0, Lcom/android/wallpaper/RenderScriptScene;->mRS:Landroid/renderscript/RenderScriptGL;

    const/4 v8, 0x1

    invoke-direct {v6, v7, v8}, Lcom/android/wallpaper/galaxy/ScriptField_VpConsts;-><init>(Landroid/renderscript/RenderScript;I)V

    iput-object v6, p0, Lcom/android/wallpaper/galaxy/GalaxyRS;->mPvStarAlloc:Lcom/android/wallpaper/galaxy/ScriptField_VpConsts;

    iget-object v6, p0, Lcom/android/wallpaper/galaxy/GalaxyRS;->mScript:Lcom/android/wallpaper/galaxy/ScriptC_galaxy;

    iget-object v7, p0, Lcom/android/wallpaper/galaxy/GalaxyRS;->mPvStarAlloc:Lcom/android/wallpaper/galaxy/ScriptField_VpConsts;

    invoke-virtual {v6, v7}, Lcom/android/wallpaper/galaxy/ScriptC_galaxy;->bind_vpConstants(Lcom/android/wallpaper/galaxy/ScriptField_VpConsts;)V

    new-instance v6, Landroid/renderscript/ProgramVertexFixedFunction$Constants;

    iget-object v7, p0, Lcom/android/wallpaper/RenderScriptScene;->mRS:Landroid/renderscript/RenderScriptGL;

    invoke-direct {v6, v7}, Landroid/renderscript/ProgramVertexFixedFunction$Constants;-><init>(Landroid/renderscript/RenderScript;)V

    iput-object v6, p0, Lcom/android/wallpaper/galaxy/GalaxyRS;->mPvProjectionAlloc:Landroid/renderscript/ProgramVertexFixedFunction$Constants;

    invoke-direct {p0}, Lcom/android/wallpaper/galaxy/GalaxyRS;->updateProjectionMatrices()V

    new-instance v0, Landroid/renderscript/ProgramVertexFixedFunction$Builder;

    iget-object v6, p0, Lcom/android/wallpaper/RenderScriptScene;->mRS:Landroid/renderscript/RenderScriptGL;

    invoke-direct {v0, v6}, Landroid/renderscript/ProgramVertexFixedFunction$Builder;-><init>(Landroid/renderscript/RenderScript;)V

    invoke-virtual {v0}, Landroid/renderscript/ProgramVertexFixedFunction$Builder;->create()Landroid/renderscript/ProgramVertexFixedFunction;

    move-result-object v2

    move-object v6, v2

    check-cast v6, Landroid/renderscript/ProgramVertexFixedFunction;

    iget-object v7, p0, Lcom/android/wallpaper/galaxy/GalaxyRS;->mPvProjectionAlloc:Landroid/renderscript/ProgramVertexFixedFunction$Constants;

    invoke-virtual {v6, v7}, Landroid/renderscript/ProgramVertexFixedFunction;->bindConstants(Landroid/renderscript/ProgramVertexFixedFunction$Constants;)V

    iget-object v6, p0, Lcom/android/wallpaper/galaxy/GalaxyRS;->mScript:Lcom/android/wallpaper/galaxy/ScriptC_galaxy;

    invoke-virtual {v6, v2}, Lcom/android/wallpaper/galaxy/ScriptC_galaxy;->set_gPVBkProj(Landroid/renderscript/ProgramVertex;)V

    new-instance v4, Landroid/renderscript/ProgramVertex$Builder;

    iget-object v6, p0, Lcom/android/wallpaper/RenderScriptScene;->mRS:Landroid/renderscript/RenderScriptGL;

    invoke-direct {v4, v6}, Landroid/renderscript/ProgramVertex$Builder;-><init>(Landroid/renderscript/RenderScript;)V

    const-string v5, "varying vec4 varColor;\nvarying vec2 varTex0;\nvoid main() {\n  float dist = ATTRIB_position.y;\n  float angle = ATTRIB_position.x;\n  float x = dist * sin(angle);\n  float y = dist * cos(angle) * 0.892;\n  float p = dist * 5.5;\n  float s = cos(p);\n  float t = sin(p);\n  vec4 pos;\n  pos.x = t * x + s * y;\n  pos.y = s * x - t * y;\n  pos.z = ATTRIB_position.z;\n  pos.w = 1.0;\n  gl_Position = UNI_MVP * pos;\n  gl_PointSize = ATTRIB_color.a * 10.0;\n  varColor.rgb = ATTRIB_color.rgb;\n  varColor.a = 1.0;\n}\n"

    invoke-virtual {v4, v5}, Landroid/renderscript/Program$BaseProgramBuilder;->setShader(Ljava/lang/String;)Landroid/renderscript/Program$BaseProgramBuilder;

    iget-object v6, p0, Lcom/android/wallpaper/galaxy/GalaxyRS;->mParticlesMesh:Landroid/renderscript/Mesh;

    invoke-virtual {v6, v9}, Landroid/renderscript/Mesh;->getVertexAllocation(I)Landroid/renderscript/Allocation;

    move-result-object v6

    invoke-virtual {v6}, Landroid/renderscript/Allocation;->getType()Landroid/renderscript/Type;

    move-result-object v6

    invoke-virtual {v6}, Landroid/renderscript/Type;->getElement()Landroid/renderscript/Element;

    move-result-object v6

    invoke-virtual {v4, v6}, Landroid/renderscript/ProgramVertex$Builder;->addInput(Landroid/renderscript/Element;)Landroid/renderscript/ProgramVertex$Builder;

    iget-object v6, p0, Lcom/android/wallpaper/galaxy/GalaxyRS;->mPvStarAlloc:Lcom/android/wallpaper/galaxy/ScriptField_VpConsts;

    invoke-virtual {v6}, Landroid/renderscript/Script$FieldBase;->getType()Landroid/renderscript/Type;

    move-result-object v6

    invoke-virtual {v4, v6}, Landroid/renderscript/Program$BaseProgramBuilder;->addConstant(Landroid/renderscript/Type;)Landroid/renderscript/Program$BaseProgramBuilder;

    invoke-virtual {v4}, Landroid/renderscript/ProgramVertex$Builder;->create()Landroid/renderscript/ProgramVertex;

    move-result-object v3

    iget-object v6, p0, Lcom/android/wallpaper/galaxy/GalaxyRS;->mPvStarAlloc:Lcom/android/wallpaper/galaxy/ScriptField_VpConsts;

    invoke-virtual {v6}, Landroid/renderscript/Script$FieldBase;->getAllocation()Landroid/renderscript/Allocation;

    move-result-object v6

    invoke-virtual {v3, v6, v9}, Landroid/renderscript/Program;->bindConstants(Landroid/renderscript/Allocation;I)V

    iget-object v6, p0, Lcom/android/wallpaper/galaxy/GalaxyRS;->mScript:Lcom/android/wallpaper/galaxy/ScriptC_galaxy;

    invoke-virtual {v6, v3}, Lcom/android/wallpaper/galaxy/ScriptC_galaxy;->set_gPVStars(Landroid/renderscript/ProgramVertex;)V

    return-void
.end method

.method private getProjectionNormalized(II)Landroid/renderscript/Matrix4f;
    .locals 11
    .param p1    # I
    .param p2    # I

    new-instance v0, Landroid/renderscript/Matrix4f;

    invoke-direct {v0}, Landroid/renderscript/Matrix4f;-><init>()V

    new-instance v10, Landroid/renderscript/Matrix4f;

    invoke-direct {v10}, Landroid/renderscript/Matrix4f;-><init>()V

    if-le p1, p2, :cond_0

    int-to-float v1, p1

    int-to-float v3, p2

    div-float v2, v1, v3

    neg-float v1, v2

    const/high16 v3, -0x40800000

    const/high16 v4, 0x3f800000

    const/high16 v5, 0x3f800000

    const/high16 v6, 0x42c80000

    invoke-virtual/range {v0 .. v6}, Landroid/renderscript/Matrix4f;->loadFrustum(FFFFFF)V

    :goto_0
    const/high16 v1, 0x43340000

    const/4 v3, 0x0

    const/high16 v4, 0x3f800000

    const/4 v5, 0x0

    invoke-virtual {v10, v1, v3, v4, v5}, Landroid/renderscript/Matrix4f;->loadRotate(FFFF)V

    invoke-virtual {v0, v0, v10}, Landroid/renderscript/Matrix4f;->loadMultiply(Landroid/renderscript/Matrix4f;Landroid/renderscript/Matrix4f;)V

    const/high16 v1, -0x40000000

    const/high16 v3, 0x40000000

    const/high16 v4, 0x3f800000

    invoke-virtual {v10, v1, v3, v4}, Landroid/renderscript/Matrix4f;->loadScale(FFF)V

    invoke-virtual {v0, v0, v10}, Landroid/renderscript/Matrix4f;->loadMultiply(Landroid/renderscript/Matrix4f;Landroid/renderscript/Matrix4f;)V

    const/4 v1, 0x0

    const/4 v3, 0x0

    const/high16 v4, 0x40000000

    invoke-virtual {v10, v1, v3, v4}, Landroid/renderscript/Matrix4f;->loadTranslate(FFF)V

    invoke-virtual {v0, v0, v10}, Landroid/renderscript/Matrix4f;->loadMultiply(Landroid/renderscript/Matrix4f;Landroid/renderscript/Matrix4f;)V

    return-object v0

    :cond_0
    int-to-float v1, p2

    int-to-float v3, p1

    div-float v2, v1, v3

    const/high16 v4, -0x40800000

    const/high16 v5, 0x3f800000

    neg-float v6, v2

    const/high16 v8, 0x3f800000

    const/high16 v9, 0x42c80000

    move-object v3, v0

    move v7, v2

    invoke-virtual/range {v3 .. v9}, Landroid/renderscript/Matrix4f;->loadFrustum(FFFFFF)V

    goto :goto_0
.end method

.method private loadTexture(I)Landroid/renderscript/Allocation;
    .locals 3
    .param p1    # I

    iget-object v1, p0, Lcom/android/wallpaper/RenderScriptScene;->mRS:Landroid/renderscript/RenderScriptGL;

    iget-object v2, p0, Lcom/android/wallpaper/RenderScriptScene;->mResources:Landroid/content/res/Resources;

    invoke-static {v1, v2, p1}, Landroid/renderscript/Allocation;->createFromBitmapResource(Landroid/renderscript/RenderScript;Landroid/content/res/Resources;I)Landroid/renderscript/Allocation;

    move-result-object v0

    return-object v0
.end method

.method private loadTextureARGB(I)Landroid/renderscript/Allocation;
    .locals 4
    .param p1    # I

    iget-object v2, p0, Lcom/android/wallpaper/RenderScriptScene;->mResources:Landroid/content/res/Resources;

    iget-object v3, p0, Lcom/android/wallpaper/galaxy/GalaxyRS;->mOptionsARGB:Landroid/graphics/BitmapFactory$Options;

    invoke-static {v2, p1, v3}, Landroid/graphics/BitmapFactory;->decodeResource(Landroid/content/res/Resources;ILandroid/graphics/BitmapFactory$Options;)Landroid/graphics/Bitmap;

    move-result-object v1

    iget-object v2, p0, Lcom/android/wallpaper/RenderScriptScene;->mRS:Landroid/renderscript/RenderScriptGL;

    invoke-static {v2, v1}, Landroid/renderscript/Allocation;->createFromBitmap(Landroid/renderscript/RenderScript;Landroid/graphics/Bitmap;)Landroid/renderscript/Allocation;

    move-result-object v0

    return-object v0
.end method

.method private loadTextures()V
    .locals 2

    iget-object v0, p0, Lcom/android/wallpaper/galaxy/GalaxyRS;->mScript:Lcom/android/wallpaper/galaxy/ScriptC_galaxy;

    const v1, 0x7f020013

    invoke-direct {p0, v1}, Lcom/android/wallpaper/galaxy/GalaxyRS;->loadTexture(I)Landroid/renderscript/Allocation;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/android/wallpaper/galaxy/ScriptC_galaxy;->set_gTSpace(Landroid/renderscript/Allocation;)V

    iget-object v0, p0, Lcom/android/wallpaper/galaxy/GalaxyRS;->mScript:Lcom/android/wallpaper/galaxy/ScriptC_galaxy;

    const v1, 0x7f02000a

    invoke-direct {p0, v1}, Lcom/android/wallpaper/galaxy/GalaxyRS;->loadTexture(I)Landroid/renderscript/Allocation;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/android/wallpaper/galaxy/ScriptC_galaxy;->set_gTLight1(Landroid/renderscript/Allocation;)V

    iget-object v0, p0, Lcom/android/wallpaper/galaxy/GalaxyRS;->mScript:Lcom/android/wallpaper/galaxy/ScriptC_galaxy;

    const/high16 v1, 0x7f020000

    invoke-direct {p0, v1}, Lcom/android/wallpaper/galaxy/GalaxyRS;->loadTextureARGB(I)Landroid/renderscript/Allocation;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/android/wallpaper/galaxy/ScriptC_galaxy;->set_gTFlares(Landroid/renderscript/Allocation;)V

    return-void
.end method

.method private updateProjectionMatrices()V
    .locals 6

    new-instance v1, Landroid/renderscript/Matrix4f;

    invoke-direct {v1}, Landroid/renderscript/Matrix4f;-><init>()V

    iget v3, p0, Lcom/android/wallpaper/RenderScriptScene;->mWidth:I

    iget v4, p0, Lcom/android/wallpaper/RenderScriptScene;->mHeight:I

    invoke-virtual {v1, v3, v4}, Landroid/renderscript/Matrix4f;->loadOrthoWindow(II)V

    iget-object v3, p0, Lcom/android/wallpaper/galaxy/GalaxyRS;->mPvOrthoAlloc:Landroid/renderscript/ProgramVertexFixedFunction$Constants;

    invoke-virtual {v3, v1}, Landroid/renderscript/ProgramVertexFixedFunction$Constants;->setProjection(Landroid/renderscript/Matrix4f;)V

    iget v3, p0, Lcom/android/wallpaper/RenderScriptScene;->mWidth:I

    iget v4, p0, Lcom/android/wallpaper/RenderScriptScene;->mHeight:I

    invoke-direct {p0, v3, v4}, Lcom/android/wallpaper/galaxy/GalaxyRS;->getProjectionNormalized(II)Landroid/renderscript/Matrix4f;

    move-result-object v2

    new-instance v0, Lcom/android/wallpaper/galaxy/ScriptField_VpConsts$Item;

    invoke-direct {v0}, Lcom/android/wallpaper/galaxy/ScriptField_VpConsts$Item;-><init>()V

    iput-object v2, v0, Lcom/android/wallpaper/galaxy/ScriptField_VpConsts$Item;->Proj:Landroid/renderscript/Matrix4f;

    iput-object v2, v0, Lcom/android/wallpaper/galaxy/ScriptField_VpConsts$Item;->MVP:Landroid/renderscript/Matrix4f;

    iget-object v3, p0, Lcom/android/wallpaper/galaxy/GalaxyRS;->mPvStarAlloc:Lcom/android/wallpaper/galaxy/ScriptField_VpConsts;

    const/4 v4, 0x0

    const/4 v5, 0x1

    invoke-virtual {v3, v0, v4, v5}, Lcom/android/wallpaper/galaxy/ScriptField_VpConsts;->set(Lcom/android/wallpaper/galaxy/ScriptField_VpConsts$Item;IZ)V

    iget-object v3, p0, Lcom/android/wallpaper/galaxy/GalaxyRS;->mPvProjectionAlloc:Landroid/renderscript/ProgramVertexFixedFunction$Constants;

    invoke-virtual {v3, v2}, Landroid/renderscript/ProgramVertexFixedFunction$Constants;->setProjection(Landroid/renderscript/Matrix4f;)V

    return-void
.end method


# virtual methods
.method protected createScript()Landroid/renderscript/ScriptC;
    .locals 4

    new-instance v0, Lcom/android/wallpaper/galaxy/ScriptC_galaxy;

    iget-object v1, p0, Lcom/android/wallpaper/RenderScriptScene;->mRS:Landroid/renderscript/RenderScriptGL;

    iget-object v2, p0, Lcom/android/wallpaper/RenderScriptScene;->mResources:Landroid/content/res/Resources;

    const v3, 0x7f050001

    invoke-direct {v0, v1, v2, v3}, Lcom/android/wallpaper/galaxy/ScriptC_galaxy;-><init>(Landroid/renderscript/RenderScript;Landroid/content/res/Resources;I)V

    iput-object v0, p0, Lcom/android/wallpaper/galaxy/GalaxyRS;->mScript:Lcom/android/wallpaper/galaxy/ScriptC_galaxy;

    iget-object v1, p0, Lcom/android/wallpaper/galaxy/GalaxyRS;->mScript:Lcom/android/wallpaper/galaxy/ScriptC_galaxy;

    invoke-virtual {p0}, Lcom/android/wallpaper/RenderScriptScene;->isPreview()Z

    move-result v0

    if-eqz v0, :cond_1

    const/4 v0, 0x1

    :goto_0
    invoke-virtual {v1, v0}, Lcom/android/wallpaper/galaxy/ScriptC_galaxy;->set_gIsPreview(I)V

    invoke-virtual {p0}, Lcom/android/wallpaper/RenderScriptScene;->isPreview()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/wallpaper/galaxy/GalaxyRS;->mScript:Lcom/android/wallpaper/galaxy/ScriptC_galaxy;

    const/high16 v1, 0x3f000000

    invoke-virtual {v0, v1}, Lcom/android/wallpaper/galaxy/ScriptC_galaxy;->set_gXOffset(F)V

    :cond_0
    invoke-direct {p0}, Lcom/android/wallpaper/galaxy/GalaxyRS;->createParticlesMesh()V

    invoke-direct {p0}, Lcom/android/wallpaper/galaxy/GalaxyRS;->createProgramVertex()V

    invoke-direct {p0}, Lcom/android/wallpaper/galaxy/GalaxyRS;->createProgramRaster()V

    invoke-direct {p0}, Lcom/android/wallpaper/galaxy/GalaxyRS;->createProgramFragmentStore()V

    invoke-direct {p0}, Lcom/android/wallpaper/galaxy/GalaxyRS;->createProgramFragment()V

    invoke-direct {p0}, Lcom/android/wallpaper/galaxy/GalaxyRS;->loadTextures()V

    iget-object v0, p0, Lcom/android/wallpaper/galaxy/GalaxyRS;->mScript:Lcom/android/wallpaper/galaxy/ScriptC_galaxy;

    invoke-static {}, Ljava/util/TimeZone;->getDefault()Ljava/util/TimeZone;

    move-result-object v1

    invoke-virtual {v1}, Ljava/util/TimeZone;->getID()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/renderscript/Script;->setTimeZone(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/android/wallpaper/galaxy/GalaxyRS;->mScript:Lcom/android/wallpaper/galaxy/ScriptC_galaxy;

    return-object v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public resize(II)V
    .locals 0
    .param p1    # I
    .param p2    # I

    invoke-super {p0, p1, p2}, Lcom/android/wallpaper/RenderScriptScene;->resize(II)V

    invoke-direct {p0}, Lcom/android/wallpaper/galaxy/GalaxyRS;->updateProjectionMatrices()V

    return-void
.end method

.method public setOffset(FFII)V
    .locals 1
    .param p1    # F
    .param p2    # F
    .param p3    # I
    .param p4    # I

    iget-object v0, p0, Lcom/android/wallpaper/galaxy/GalaxyRS;->mScript:Lcom/android/wallpaper/galaxy/ScriptC_galaxy;

    invoke-virtual {v0, p1}, Lcom/android/wallpaper/galaxy/ScriptC_galaxy;->set_gXOffset(F)V

    return-void
.end method
