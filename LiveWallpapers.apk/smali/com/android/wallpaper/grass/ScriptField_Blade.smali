.class public Lcom/android/wallpaper/grass/ScriptField_Blade;
.super Landroid/renderscript/Script$FieldBase;
.source "ScriptField_Blade.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/wallpaper/grass/ScriptField_Blade$Item;
    }
.end annotation


# static fields
.field private static mElementCache:Ljava/lang/ref/WeakReference;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/ref/WeakReference",
            "<",
            "Landroid/renderscript/Element;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private mIOBuffer:Landroid/renderscript/FieldPacker;

.field private mItemArray:[Lcom/android/wallpaper/grass/ScriptField_Blade$Item;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    new-instance v0, Ljava/lang/ref/WeakReference;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    sput-object v0, Lcom/android/wallpaper/grass/ScriptField_Blade;->mElementCache:Ljava/lang/ref/WeakReference;

    return-void
.end method

.method private constructor <init>(Landroid/renderscript/RenderScript;)V
    .locals 1
    .param p1    # Landroid/renderscript/RenderScript;

    const/4 v0, 0x0

    invoke-direct {p0}, Landroid/renderscript/Script$FieldBase;-><init>()V

    iput-object v0, p0, Lcom/android/wallpaper/grass/ScriptField_Blade;->mItemArray:[Lcom/android/wallpaper/grass/ScriptField_Blade$Item;

    iput-object v0, p0, Lcom/android/wallpaper/grass/ScriptField_Blade;->mIOBuffer:Landroid/renderscript/FieldPacker;

    invoke-static {p1}, Lcom/android/wallpaper/grass/ScriptField_Blade;->createElement(Landroid/renderscript/RenderScript;)Landroid/renderscript/Element;

    move-result-object v0

    iput-object v0, p0, Landroid/renderscript/Script$FieldBase;->mElement:Landroid/renderscript/Element;

    return-void
.end method

.method public constructor <init>(Landroid/renderscript/RenderScript;I)V
    .locals 1
    .param p1    # Landroid/renderscript/RenderScript;
    .param p2    # I

    const/4 v0, 0x0

    invoke-direct {p0}, Landroid/renderscript/Script$FieldBase;-><init>()V

    iput-object v0, p0, Lcom/android/wallpaper/grass/ScriptField_Blade;->mItemArray:[Lcom/android/wallpaper/grass/ScriptField_Blade$Item;

    iput-object v0, p0, Lcom/android/wallpaper/grass/ScriptField_Blade;->mIOBuffer:Landroid/renderscript/FieldPacker;

    invoke-static {p1}, Lcom/android/wallpaper/grass/ScriptField_Blade;->createElement(Landroid/renderscript/RenderScript;)Landroid/renderscript/Element;

    move-result-object v0

    iput-object v0, p0, Landroid/renderscript/Script$FieldBase;->mElement:Landroid/renderscript/Element;

    invoke-virtual {p0, p1, p2}, Landroid/renderscript/Script$FieldBase;->init(Landroid/renderscript/RenderScript;I)V

    return-void
.end method

.method public constructor <init>(Landroid/renderscript/RenderScript;II)V
    .locals 1
    .param p1    # Landroid/renderscript/RenderScript;
    .param p2    # I
    .param p3    # I

    const/4 v0, 0x0

    invoke-direct {p0}, Landroid/renderscript/Script$FieldBase;-><init>()V

    iput-object v0, p0, Lcom/android/wallpaper/grass/ScriptField_Blade;->mItemArray:[Lcom/android/wallpaper/grass/ScriptField_Blade$Item;

    iput-object v0, p0, Lcom/android/wallpaper/grass/ScriptField_Blade;->mIOBuffer:Landroid/renderscript/FieldPacker;

    invoke-static {p1}, Lcom/android/wallpaper/grass/ScriptField_Blade;->createElement(Landroid/renderscript/RenderScript;)Landroid/renderscript/Element;

    move-result-object v0

    iput-object v0, p0, Landroid/renderscript/Script$FieldBase;->mElement:Landroid/renderscript/Element;

    invoke-virtual {p0, p1, p2, p3}, Landroid/renderscript/Script$FieldBase;->init(Landroid/renderscript/RenderScript;II)V

    return-void
.end method

.method private copyToArray(Lcom/android/wallpaper/grass/ScriptField_Blade$Item;I)V
    .locals 2
    .param p1    # Lcom/android/wallpaper/grass/ScriptField_Blade$Item;
    .param p2    # I

    iget-object v0, p0, Lcom/android/wallpaper/grass/ScriptField_Blade;->mIOBuffer:Landroid/renderscript/FieldPacker;

    if-nez v0, :cond_0

    new-instance v0, Landroid/renderscript/FieldPacker;

    invoke-virtual {p0}, Landroid/renderscript/Script$FieldBase;->getType()Landroid/renderscript/Type;

    move-result-object v1

    invoke-virtual {v1}, Landroid/renderscript/Type;->getX()I

    move-result v1

    mul-int/lit8 v1, v1, 0x34

    invoke-direct {v0, v1}, Landroid/renderscript/FieldPacker;-><init>(I)V

    iput-object v0, p0, Lcom/android/wallpaper/grass/ScriptField_Blade;->mIOBuffer:Landroid/renderscript/FieldPacker;

    :cond_0
    iget-object v0, p0, Lcom/android/wallpaper/grass/ScriptField_Blade;->mIOBuffer:Landroid/renderscript/FieldPacker;

    mul-int/lit8 v1, p2, 0x34

    invoke-virtual {v0, v1}, Landroid/renderscript/FieldPacker;->reset(I)V

    iget-object v0, p0, Lcom/android/wallpaper/grass/ScriptField_Blade;->mIOBuffer:Landroid/renderscript/FieldPacker;

    invoke-direct {p0, p1, v0}, Lcom/android/wallpaper/grass/ScriptField_Blade;->copyToArrayLocal(Lcom/android/wallpaper/grass/ScriptField_Blade$Item;Landroid/renderscript/FieldPacker;)V

    return-void
.end method

.method private copyToArrayLocal(Lcom/android/wallpaper/grass/ScriptField_Blade$Item;Landroid/renderscript/FieldPacker;)V
    .locals 1
    .param p1    # Lcom/android/wallpaper/grass/ScriptField_Blade$Item;
    .param p2    # Landroid/renderscript/FieldPacker;

    iget v0, p1, Lcom/android/wallpaper/grass/ScriptField_Blade$Item;->angle:F

    invoke-virtual {p2, v0}, Landroid/renderscript/FieldPacker;->addF32(F)V

    iget v0, p1, Lcom/android/wallpaper/grass/ScriptField_Blade$Item;->size:I

    invoke-virtual {p2, v0}, Landroid/renderscript/FieldPacker;->addI32(I)V

    iget v0, p1, Lcom/android/wallpaper/grass/ScriptField_Blade$Item;->xPos:F

    invoke-virtual {p2, v0}, Landroid/renderscript/FieldPacker;->addF32(F)V

    iget v0, p1, Lcom/android/wallpaper/grass/ScriptField_Blade$Item;->yPos:F

    invoke-virtual {p2, v0}, Landroid/renderscript/FieldPacker;->addF32(F)V

    iget v0, p1, Lcom/android/wallpaper/grass/ScriptField_Blade$Item;->offset:F

    invoke-virtual {p2, v0}, Landroid/renderscript/FieldPacker;->addF32(F)V

    iget v0, p1, Lcom/android/wallpaper/grass/ScriptField_Blade$Item;->scale:F

    invoke-virtual {p2, v0}, Landroid/renderscript/FieldPacker;->addF32(F)V

    iget v0, p1, Lcom/android/wallpaper/grass/ScriptField_Blade$Item;->lengthX:F

    invoke-virtual {p2, v0}, Landroid/renderscript/FieldPacker;->addF32(F)V

    iget v0, p1, Lcom/android/wallpaper/grass/ScriptField_Blade$Item;->lengthY:F

    invoke-virtual {p2, v0}, Landroid/renderscript/FieldPacker;->addF32(F)V

    iget v0, p1, Lcom/android/wallpaper/grass/ScriptField_Blade$Item;->hardness:F

    invoke-virtual {p2, v0}, Landroid/renderscript/FieldPacker;->addF32(F)V

    iget v0, p1, Lcom/android/wallpaper/grass/ScriptField_Blade$Item;->h:F

    invoke-virtual {p2, v0}, Landroid/renderscript/FieldPacker;->addF32(F)V

    iget v0, p1, Lcom/android/wallpaper/grass/ScriptField_Blade$Item;->s:F

    invoke-virtual {p2, v0}, Landroid/renderscript/FieldPacker;->addF32(F)V

    iget v0, p1, Lcom/android/wallpaper/grass/ScriptField_Blade$Item;->b:F

    invoke-virtual {p2, v0}, Landroid/renderscript/FieldPacker;->addF32(F)V

    iget v0, p1, Lcom/android/wallpaper/grass/ScriptField_Blade$Item;->turbulencex:F

    invoke-virtual {p2, v0}, Landroid/renderscript/FieldPacker;->addF32(F)V

    return-void
.end method

.method public static create1D(Landroid/renderscript/RenderScript;I)Lcom/android/wallpaper/grass/ScriptField_Blade;
    .locals 1
    .param p0    # Landroid/renderscript/RenderScript;
    .param p1    # I

    const/4 v0, 0x1

    invoke-static {p0, p1, v0}, Lcom/android/wallpaper/grass/ScriptField_Blade;->create1D(Landroid/renderscript/RenderScript;II)Lcom/android/wallpaper/grass/ScriptField_Blade;

    move-result-object v0

    return-object v0
.end method

.method public static create1D(Landroid/renderscript/RenderScript;II)Lcom/android/wallpaper/grass/ScriptField_Blade;
    .locals 2
    .param p0    # Landroid/renderscript/RenderScript;
    .param p1    # I
    .param p2    # I

    new-instance v0, Lcom/android/wallpaper/grass/ScriptField_Blade;

    invoke-direct {v0, p0}, Lcom/android/wallpaper/grass/ScriptField_Blade;-><init>(Landroid/renderscript/RenderScript;)V

    iget-object v1, v0, Landroid/renderscript/Script$FieldBase;->mElement:Landroid/renderscript/Element;

    invoke-static {p0, v1, p1, p2}, Landroid/renderscript/Allocation;->createSized(Landroid/renderscript/RenderScript;Landroid/renderscript/Element;II)Landroid/renderscript/Allocation;

    move-result-object v1

    iput-object v1, v0, Landroid/renderscript/Script$FieldBase;->mAllocation:Landroid/renderscript/Allocation;

    return-object v0
.end method

.method public static create2D(Landroid/renderscript/RenderScript;II)Lcom/android/wallpaper/grass/ScriptField_Blade;
    .locals 1
    .param p0    # Landroid/renderscript/RenderScript;
    .param p1    # I
    .param p2    # I

    const/4 v0, 0x1

    invoke-static {p0, p1, p2, v0}, Lcom/android/wallpaper/grass/ScriptField_Blade;->create2D(Landroid/renderscript/RenderScript;III)Lcom/android/wallpaper/grass/ScriptField_Blade;

    move-result-object v0

    return-object v0
.end method

.method public static create2D(Landroid/renderscript/RenderScript;III)Lcom/android/wallpaper/grass/ScriptField_Blade;
    .locals 4
    .param p0    # Landroid/renderscript/RenderScript;
    .param p1    # I
    .param p2    # I
    .param p3    # I

    new-instance v1, Lcom/android/wallpaper/grass/ScriptField_Blade;

    invoke-direct {v1, p0}, Lcom/android/wallpaper/grass/ScriptField_Blade;-><init>(Landroid/renderscript/RenderScript;)V

    new-instance v0, Landroid/renderscript/Type$Builder;

    iget-object v3, v1, Landroid/renderscript/Script$FieldBase;->mElement:Landroid/renderscript/Element;

    invoke-direct {v0, p0, v3}, Landroid/renderscript/Type$Builder;-><init>(Landroid/renderscript/RenderScript;Landroid/renderscript/Element;)V

    invoke-virtual {v0, p1}, Landroid/renderscript/Type$Builder;->setX(I)Landroid/renderscript/Type$Builder;

    invoke-virtual {v0, p2}, Landroid/renderscript/Type$Builder;->setY(I)Landroid/renderscript/Type$Builder;

    invoke-virtual {v0}, Landroid/renderscript/Type$Builder;->create()Landroid/renderscript/Type;

    move-result-object v2

    invoke-static {p0, v2, p3}, Landroid/renderscript/Allocation;->createTyped(Landroid/renderscript/RenderScript;Landroid/renderscript/Type;I)Landroid/renderscript/Allocation;

    move-result-object v3

    iput-object v3, v1, Landroid/renderscript/Script$FieldBase;->mAllocation:Landroid/renderscript/Allocation;

    return-object v1
.end method

.method public static createCustom(Landroid/renderscript/RenderScript;Landroid/renderscript/Type$Builder;I)Lcom/android/wallpaper/grass/ScriptField_Blade;
    .locals 4
    .param p0    # Landroid/renderscript/RenderScript;
    .param p1    # Landroid/renderscript/Type$Builder;
    .param p2    # I

    new-instance v0, Lcom/android/wallpaper/grass/ScriptField_Blade;

    invoke-direct {v0, p0}, Lcom/android/wallpaper/grass/ScriptField_Blade;-><init>(Landroid/renderscript/RenderScript;)V

    invoke-virtual {p1}, Landroid/renderscript/Type$Builder;->create()Landroid/renderscript/Type;

    move-result-object v1

    invoke-virtual {v1}, Landroid/renderscript/Type;->getElement()Landroid/renderscript/Element;

    move-result-object v2

    iget-object v3, v0, Landroid/renderscript/Script$FieldBase;->mElement:Landroid/renderscript/Element;

    if-eq v2, v3, :cond_0

    new-instance v2, Landroid/renderscript/RSIllegalArgumentException;

    const-string v3, "Type.Builder did not match expected element type."

    invoke-direct {v2, v3}, Landroid/renderscript/RSIllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v2

    :cond_0
    invoke-static {p0, v1, p2}, Landroid/renderscript/Allocation;->createTyped(Landroid/renderscript/RenderScript;Landroid/renderscript/Type;I)Landroid/renderscript/Allocation;

    move-result-object v2

    iput-object v2, v0, Landroid/renderscript/Script$FieldBase;->mAllocation:Landroid/renderscript/Allocation;

    return-object v0
.end method

.method public static createElement(Landroid/renderscript/RenderScript;)Landroid/renderscript/Element;
    .locals 3
    .param p0    # Landroid/renderscript/RenderScript;

    new-instance v0, Landroid/renderscript/Element$Builder;

    invoke-direct {v0, p0}, Landroid/renderscript/Element$Builder;-><init>(Landroid/renderscript/RenderScript;)V

    invoke-static {p0}, Landroid/renderscript/Element;->F32(Landroid/renderscript/RenderScript;)Landroid/renderscript/Element;

    move-result-object v1

    const-string v2, "angle"

    invoke-virtual {v0, v1, v2}, Landroid/renderscript/Element$Builder;->add(Landroid/renderscript/Element;Ljava/lang/String;)Landroid/renderscript/Element$Builder;

    invoke-static {p0}, Landroid/renderscript/Element;->I32(Landroid/renderscript/RenderScript;)Landroid/renderscript/Element;

    move-result-object v1

    const-string v2, "size"

    invoke-virtual {v0, v1, v2}, Landroid/renderscript/Element$Builder;->add(Landroid/renderscript/Element;Ljava/lang/String;)Landroid/renderscript/Element$Builder;

    invoke-static {p0}, Landroid/renderscript/Element;->F32(Landroid/renderscript/RenderScript;)Landroid/renderscript/Element;

    move-result-object v1

    const-string v2, "xPos"

    invoke-virtual {v0, v1, v2}, Landroid/renderscript/Element$Builder;->add(Landroid/renderscript/Element;Ljava/lang/String;)Landroid/renderscript/Element$Builder;

    invoke-static {p0}, Landroid/renderscript/Element;->F32(Landroid/renderscript/RenderScript;)Landroid/renderscript/Element;

    move-result-object v1

    const-string v2, "yPos"

    invoke-virtual {v0, v1, v2}, Landroid/renderscript/Element$Builder;->add(Landroid/renderscript/Element;Ljava/lang/String;)Landroid/renderscript/Element$Builder;

    invoke-static {p0}, Landroid/renderscript/Element;->F32(Landroid/renderscript/RenderScript;)Landroid/renderscript/Element;

    move-result-object v1

    const-string v2, "offset"

    invoke-virtual {v0, v1, v2}, Landroid/renderscript/Element$Builder;->add(Landroid/renderscript/Element;Ljava/lang/String;)Landroid/renderscript/Element$Builder;

    invoke-static {p0}, Landroid/renderscript/Element;->F32(Landroid/renderscript/RenderScript;)Landroid/renderscript/Element;

    move-result-object v1

    const-string v2, "scale"

    invoke-virtual {v0, v1, v2}, Landroid/renderscript/Element$Builder;->add(Landroid/renderscript/Element;Ljava/lang/String;)Landroid/renderscript/Element$Builder;

    invoke-static {p0}, Landroid/renderscript/Element;->F32(Landroid/renderscript/RenderScript;)Landroid/renderscript/Element;

    move-result-object v1

    const-string v2, "lengthX"

    invoke-virtual {v0, v1, v2}, Landroid/renderscript/Element$Builder;->add(Landroid/renderscript/Element;Ljava/lang/String;)Landroid/renderscript/Element$Builder;

    invoke-static {p0}, Landroid/renderscript/Element;->F32(Landroid/renderscript/RenderScript;)Landroid/renderscript/Element;

    move-result-object v1

    const-string v2, "lengthY"

    invoke-virtual {v0, v1, v2}, Landroid/renderscript/Element$Builder;->add(Landroid/renderscript/Element;Ljava/lang/String;)Landroid/renderscript/Element$Builder;

    invoke-static {p0}, Landroid/renderscript/Element;->F32(Landroid/renderscript/RenderScript;)Landroid/renderscript/Element;

    move-result-object v1

    const-string v2, "hardness"

    invoke-virtual {v0, v1, v2}, Landroid/renderscript/Element$Builder;->add(Landroid/renderscript/Element;Ljava/lang/String;)Landroid/renderscript/Element$Builder;

    invoke-static {p0}, Landroid/renderscript/Element;->F32(Landroid/renderscript/RenderScript;)Landroid/renderscript/Element;

    move-result-object v1

    const-string v2, "h"

    invoke-virtual {v0, v1, v2}, Landroid/renderscript/Element$Builder;->add(Landroid/renderscript/Element;Ljava/lang/String;)Landroid/renderscript/Element$Builder;

    invoke-static {p0}, Landroid/renderscript/Element;->F32(Landroid/renderscript/RenderScript;)Landroid/renderscript/Element;

    move-result-object v1

    const-string v2, "s"

    invoke-virtual {v0, v1, v2}, Landroid/renderscript/Element$Builder;->add(Landroid/renderscript/Element;Ljava/lang/String;)Landroid/renderscript/Element$Builder;

    invoke-static {p0}, Landroid/renderscript/Element;->F32(Landroid/renderscript/RenderScript;)Landroid/renderscript/Element;

    move-result-object v1

    const-string v2, "b"

    invoke-virtual {v0, v1, v2}, Landroid/renderscript/Element$Builder;->add(Landroid/renderscript/Element;Ljava/lang/String;)Landroid/renderscript/Element$Builder;

    invoke-static {p0}, Landroid/renderscript/Element;->F32(Landroid/renderscript/RenderScript;)Landroid/renderscript/Element;

    move-result-object v1

    const-string v2, "turbulencex"

    invoke-virtual {v0, v1, v2}, Landroid/renderscript/Element$Builder;->add(Landroid/renderscript/Element;Ljava/lang/String;)Landroid/renderscript/Element$Builder;

    invoke-virtual {v0}, Landroid/renderscript/Element$Builder;->create()Landroid/renderscript/Element;

    move-result-object v1

    return-object v1
.end method

.method public static createTypeBuilder(Landroid/renderscript/RenderScript;)Landroid/renderscript/Type$Builder;
    .locals 2
    .param p0    # Landroid/renderscript/RenderScript;

    invoke-static {p0}, Lcom/android/wallpaper/grass/ScriptField_Blade;->createElement(Landroid/renderscript/RenderScript;)Landroid/renderscript/Element;

    move-result-object v0

    new-instance v1, Landroid/renderscript/Type$Builder;

    invoke-direct {v1, p0, v0}, Landroid/renderscript/Type$Builder;-><init>(Landroid/renderscript/RenderScript;Landroid/renderscript/Element;)V

    return-object v1
.end method


# virtual methods
.method public declared-synchronized copyAll()V
    .locals 4

    monitor-enter p0

    const/4 v0, 0x0

    :goto_0
    :try_start_0
    iget-object v1, p0, Lcom/android/wallpaper/grass/ScriptField_Blade;->mItemArray:[Lcom/android/wallpaper/grass/ScriptField_Blade$Item;

    array-length v1, v1

    if-ge v0, v1, :cond_0

    iget-object v1, p0, Lcom/android/wallpaper/grass/ScriptField_Blade;->mItemArray:[Lcom/android/wallpaper/grass/ScriptField_Blade$Item;

    aget-object v1, v1, v0

    invoke-direct {p0, v1, v0}, Lcom/android/wallpaper/grass/ScriptField_Blade;->copyToArray(Lcom/android/wallpaper/grass/ScriptField_Blade$Item;I)V

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_0
    iget-object v1, p0, Landroid/renderscript/Script$FieldBase;->mAllocation:Landroid/renderscript/Allocation;

    const/4 v2, 0x0

    iget-object v3, p0, Lcom/android/wallpaper/grass/ScriptField_Blade;->mIOBuffer:Landroid/renderscript/FieldPacker;

    invoke-virtual {v1, v2, v3}, Landroid/renderscript/Allocation;->setFromFieldPacker(ILandroid/renderscript/FieldPacker;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-void

    :catchall_0
    move-exception v1

    monitor-exit p0

    throw v1
.end method

.method public declared-synchronized get(I)Lcom/android/wallpaper/grass/ScriptField_Blade$Item;
    .locals 1
    .param p1    # I

    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/android/wallpaper/grass/ScriptField_Blade;->mItemArray:[Lcom/android/wallpaper/grass/ScriptField_Blade$Item;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    monitor-exit p0

    return-object v0

    :cond_0
    :try_start_1
    iget-object v0, p0, Lcom/android/wallpaper/grass/ScriptField_Blade;->mItemArray:[Lcom/android/wallpaper/grass/ScriptField_Blade$Item;

    aget-object v0, v0, p1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized get_angle(I)F
    .locals 1
    .param p1    # I

    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/android/wallpaper/grass/ScriptField_Blade;->mItemArray:[Lcom/android/wallpaper/grass/ScriptField_Blade$Item;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    monitor-exit p0

    return v0

    :cond_0
    :try_start_1
    iget-object v0, p0, Lcom/android/wallpaper/grass/ScriptField_Blade;->mItemArray:[Lcom/android/wallpaper/grass/ScriptField_Blade$Item;

    aget-object v0, v0, p1

    iget v0, v0, Lcom/android/wallpaper/grass/ScriptField_Blade$Item;->angle:F
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized get_b(I)F
    .locals 1
    .param p1    # I

    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/android/wallpaper/grass/ScriptField_Blade;->mItemArray:[Lcom/android/wallpaper/grass/ScriptField_Blade$Item;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    monitor-exit p0

    return v0

    :cond_0
    :try_start_1
    iget-object v0, p0, Lcom/android/wallpaper/grass/ScriptField_Blade;->mItemArray:[Lcom/android/wallpaper/grass/ScriptField_Blade$Item;

    aget-object v0, v0, p1

    iget v0, v0, Lcom/android/wallpaper/grass/ScriptField_Blade$Item;->b:F
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized get_h(I)F
    .locals 1
    .param p1    # I

    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/android/wallpaper/grass/ScriptField_Blade;->mItemArray:[Lcom/android/wallpaper/grass/ScriptField_Blade$Item;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    monitor-exit p0

    return v0

    :cond_0
    :try_start_1
    iget-object v0, p0, Lcom/android/wallpaper/grass/ScriptField_Blade;->mItemArray:[Lcom/android/wallpaper/grass/ScriptField_Blade$Item;

    aget-object v0, v0, p1

    iget v0, v0, Lcom/android/wallpaper/grass/ScriptField_Blade$Item;->h:F
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized get_hardness(I)F
    .locals 1
    .param p1    # I

    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/android/wallpaper/grass/ScriptField_Blade;->mItemArray:[Lcom/android/wallpaper/grass/ScriptField_Blade$Item;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    monitor-exit p0

    return v0

    :cond_0
    :try_start_1
    iget-object v0, p0, Lcom/android/wallpaper/grass/ScriptField_Blade;->mItemArray:[Lcom/android/wallpaper/grass/ScriptField_Blade$Item;

    aget-object v0, v0, p1

    iget v0, v0, Lcom/android/wallpaper/grass/ScriptField_Blade$Item;->hardness:F
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized get_lengthX(I)F
    .locals 1
    .param p1    # I

    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/android/wallpaper/grass/ScriptField_Blade;->mItemArray:[Lcom/android/wallpaper/grass/ScriptField_Blade$Item;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    monitor-exit p0

    return v0

    :cond_0
    :try_start_1
    iget-object v0, p0, Lcom/android/wallpaper/grass/ScriptField_Blade;->mItemArray:[Lcom/android/wallpaper/grass/ScriptField_Blade$Item;

    aget-object v0, v0, p1

    iget v0, v0, Lcom/android/wallpaper/grass/ScriptField_Blade$Item;->lengthX:F
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized get_lengthY(I)F
    .locals 1
    .param p1    # I

    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/android/wallpaper/grass/ScriptField_Blade;->mItemArray:[Lcom/android/wallpaper/grass/ScriptField_Blade$Item;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    monitor-exit p0

    return v0

    :cond_0
    :try_start_1
    iget-object v0, p0, Lcom/android/wallpaper/grass/ScriptField_Blade;->mItemArray:[Lcom/android/wallpaper/grass/ScriptField_Blade$Item;

    aget-object v0, v0, p1

    iget v0, v0, Lcom/android/wallpaper/grass/ScriptField_Blade$Item;->lengthY:F
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized get_offset(I)F
    .locals 1
    .param p1    # I

    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/android/wallpaper/grass/ScriptField_Blade;->mItemArray:[Lcom/android/wallpaper/grass/ScriptField_Blade$Item;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    monitor-exit p0

    return v0

    :cond_0
    :try_start_1
    iget-object v0, p0, Lcom/android/wallpaper/grass/ScriptField_Blade;->mItemArray:[Lcom/android/wallpaper/grass/ScriptField_Blade$Item;

    aget-object v0, v0, p1

    iget v0, v0, Lcom/android/wallpaper/grass/ScriptField_Blade$Item;->offset:F
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized get_s(I)F
    .locals 1
    .param p1    # I

    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/android/wallpaper/grass/ScriptField_Blade;->mItemArray:[Lcom/android/wallpaper/grass/ScriptField_Blade$Item;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    monitor-exit p0

    return v0

    :cond_0
    :try_start_1
    iget-object v0, p0, Lcom/android/wallpaper/grass/ScriptField_Blade;->mItemArray:[Lcom/android/wallpaper/grass/ScriptField_Blade$Item;

    aget-object v0, v0, p1

    iget v0, v0, Lcom/android/wallpaper/grass/ScriptField_Blade$Item;->s:F
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized get_scale(I)F
    .locals 1
    .param p1    # I

    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/android/wallpaper/grass/ScriptField_Blade;->mItemArray:[Lcom/android/wallpaper/grass/ScriptField_Blade$Item;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    monitor-exit p0

    return v0

    :cond_0
    :try_start_1
    iget-object v0, p0, Lcom/android/wallpaper/grass/ScriptField_Blade;->mItemArray:[Lcom/android/wallpaper/grass/ScriptField_Blade$Item;

    aget-object v0, v0, p1

    iget v0, v0, Lcom/android/wallpaper/grass/ScriptField_Blade$Item;->scale:F
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized get_size(I)I
    .locals 1
    .param p1    # I

    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/android/wallpaper/grass/ScriptField_Blade;->mItemArray:[Lcom/android/wallpaper/grass/ScriptField_Blade$Item;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    monitor-exit p0

    return v0

    :cond_0
    :try_start_1
    iget-object v0, p0, Lcom/android/wallpaper/grass/ScriptField_Blade;->mItemArray:[Lcom/android/wallpaper/grass/ScriptField_Blade$Item;

    aget-object v0, v0, p1

    iget v0, v0, Lcom/android/wallpaper/grass/ScriptField_Blade$Item;->size:I
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized get_turbulencex(I)F
    .locals 1
    .param p1    # I

    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/android/wallpaper/grass/ScriptField_Blade;->mItemArray:[Lcom/android/wallpaper/grass/ScriptField_Blade$Item;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    monitor-exit p0

    return v0

    :cond_0
    :try_start_1
    iget-object v0, p0, Lcom/android/wallpaper/grass/ScriptField_Blade;->mItemArray:[Lcom/android/wallpaper/grass/ScriptField_Blade$Item;

    aget-object v0, v0, p1

    iget v0, v0, Lcom/android/wallpaper/grass/ScriptField_Blade$Item;->turbulencex:F
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized get_xPos(I)F
    .locals 1
    .param p1    # I

    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/android/wallpaper/grass/ScriptField_Blade;->mItemArray:[Lcom/android/wallpaper/grass/ScriptField_Blade$Item;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    monitor-exit p0

    return v0

    :cond_0
    :try_start_1
    iget-object v0, p0, Lcom/android/wallpaper/grass/ScriptField_Blade;->mItemArray:[Lcom/android/wallpaper/grass/ScriptField_Blade$Item;

    aget-object v0, v0, p1

    iget v0, v0, Lcom/android/wallpaper/grass/ScriptField_Blade$Item;->xPos:F
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized get_yPos(I)F
    .locals 1
    .param p1    # I

    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/android/wallpaper/grass/ScriptField_Blade;->mItemArray:[Lcom/android/wallpaper/grass/ScriptField_Blade$Item;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    monitor-exit p0

    return v0

    :cond_0
    :try_start_1
    iget-object v0, p0, Lcom/android/wallpaper/grass/ScriptField_Blade;->mItemArray:[Lcom/android/wallpaper/grass/ScriptField_Blade$Item;

    aget-object v0, v0, p1

    iget v0, v0, Lcom/android/wallpaper/grass/ScriptField_Blade$Item;->yPos:F
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized resize(I)V
    .locals 6
    .param p1    # I

    monitor-enter p0

    :try_start_0
    iget-object v3, p0, Lcom/android/wallpaper/grass/ScriptField_Blade;->mItemArray:[Lcom/android/wallpaper/grass/ScriptField_Blade$Item;

    if-eqz v3, :cond_2

    iget-object v3, p0, Lcom/android/wallpaper/grass/ScriptField_Blade;->mItemArray:[Lcom/android/wallpaper/grass/ScriptField_Blade$Item;

    array-length v2, v3

    invoke-static {v2, p1}, Ljava/lang/Math;->min(II)I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    if-ne p1, v2, :cond_1

    :cond_0
    :goto_0
    monitor-exit p0

    return-void

    :cond_1
    :try_start_1
    new-array v1, p1, [Lcom/android/wallpaper/grass/ScriptField_Blade$Item;

    iget-object v3, p0, Lcom/android/wallpaper/grass/ScriptField_Blade;->mItemArray:[Lcom/android/wallpaper/grass/ScriptField_Blade$Item;

    const/4 v4, 0x0

    const/4 v5, 0x0

    invoke-static {v3, v4, v1, v5, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    iput-object v1, p0, Lcom/android/wallpaper/grass/ScriptField_Blade;->mItemArray:[Lcom/android/wallpaper/grass/ScriptField_Blade$Item;

    :cond_2
    iget-object v3, p0, Landroid/renderscript/Script$FieldBase;->mAllocation:Landroid/renderscript/Allocation;

    invoke-virtual {v3, p1}, Landroid/renderscript/Allocation;->resize(I)V

    iget-object v3, p0, Lcom/android/wallpaper/grass/ScriptField_Blade;->mIOBuffer:Landroid/renderscript/FieldPacker;

    if-eqz v3, :cond_0

    new-instance v3, Landroid/renderscript/FieldPacker;

    invoke-virtual {p0}, Landroid/renderscript/Script$FieldBase;->getType()Landroid/renderscript/Type;

    move-result-object v4

    invoke-virtual {v4}, Landroid/renderscript/Type;->getX()I

    move-result v4

    mul-int/lit8 v4, v4, 0x34

    invoke-direct {v3, v4}, Landroid/renderscript/FieldPacker;-><init>(I)V

    iput-object v3, p0, Lcom/android/wallpaper/grass/ScriptField_Blade;->mIOBuffer:Landroid/renderscript/FieldPacker;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v3

    monitor-exit p0

    throw v3
.end method

.method public declared-synchronized set(Lcom/android/wallpaper/grass/ScriptField_Blade$Item;IZ)V
    .locals 2
    .param p1    # Lcom/android/wallpaper/grass/ScriptField_Blade$Item;
    .param p2    # I
    .param p3    # Z

    monitor-enter p0

    :try_start_0
    iget-object v1, p0, Lcom/android/wallpaper/grass/ScriptField_Blade;->mItemArray:[Lcom/android/wallpaper/grass/ScriptField_Blade$Item;

    if-nez v1, :cond_0

    invoke-virtual {p0}, Landroid/renderscript/Script$FieldBase;->getType()Landroid/renderscript/Type;

    move-result-object v1

    invoke-virtual {v1}, Landroid/renderscript/Type;->getX()I

    move-result v1

    new-array v1, v1, [Lcom/android/wallpaper/grass/ScriptField_Blade$Item;

    iput-object v1, p0, Lcom/android/wallpaper/grass/ScriptField_Blade;->mItemArray:[Lcom/android/wallpaper/grass/ScriptField_Blade$Item;

    :cond_0
    iget-object v1, p0, Lcom/android/wallpaper/grass/ScriptField_Blade;->mItemArray:[Lcom/android/wallpaper/grass/ScriptField_Blade$Item;

    aput-object p1, v1, p2

    if-eqz p3, :cond_1

    invoke-direct {p0, p1, p2}, Lcom/android/wallpaper/grass/ScriptField_Blade;->copyToArray(Lcom/android/wallpaper/grass/ScriptField_Blade$Item;I)V

    new-instance v0, Landroid/renderscript/FieldPacker;

    const/16 v1, 0x34

    invoke-direct {v0, v1}, Landroid/renderscript/FieldPacker;-><init>(I)V

    invoke-direct {p0, p1, v0}, Lcom/android/wallpaper/grass/ScriptField_Blade;->copyToArrayLocal(Lcom/android/wallpaper/grass/ScriptField_Blade$Item;Landroid/renderscript/FieldPacker;)V

    iget-object v1, p0, Landroid/renderscript/Script$FieldBase;->mAllocation:Landroid/renderscript/Allocation;

    invoke-virtual {v1, p2, v0}, Landroid/renderscript/Allocation;->setFromFieldPacker(ILandroid/renderscript/FieldPacker;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :cond_1
    monitor-exit p0

    return-void

    :catchall_0
    move-exception v1

    monitor-exit p0

    throw v1
.end method

.method public declared-synchronized set_angle(IFZ)V
    .locals 3
    .param p1    # I
    .param p2    # F
    .param p3    # Z

    monitor-enter p0

    :try_start_0
    iget-object v1, p0, Lcom/android/wallpaper/grass/ScriptField_Blade;->mIOBuffer:Landroid/renderscript/FieldPacker;

    if-nez v1, :cond_0

    new-instance v1, Landroid/renderscript/FieldPacker;

    invoke-virtual {p0}, Landroid/renderscript/Script$FieldBase;->getType()Landroid/renderscript/Type;

    move-result-object v2

    invoke-virtual {v2}, Landroid/renderscript/Type;->getX()I

    move-result v2

    mul-int/lit8 v2, v2, 0x34

    invoke-direct {v1, v2}, Landroid/renderscript/FieldPacker;-><init>(I)V

    iput-object v1, p0, Lcom/android/wallpaper/grass/ScriptField_Blade;->mIOBuffer:Landroid/renderscript/FieldPacker;

    :cond_0
    iget-object v1, p0, Lcom/android/wallpaper/grass/ScriptField_Blade;->mItemArray:[Lcom/android/wallpaper/grass/ScriptField_Blade$Item;

    if-nez v1, :cond_1

    invoke-virtual {p0}, Landroid/renderscript/Script$FieldBase;->getType()Landroid/renderscript/Type;

    move-result-object v1

    invoke-virtual {v1}, Landroid/renderscript/Type;->getX()I

    move-result v1

    new-array v1, v1, [Lcom/android/wallpaper/grass/ScriptField_Blade$Item;

    iput-object v1, p0, Lcom/android/wallpaper/grass/ScriptField_Blade;->mItemArray:[Lcom/android/wallpaper/grass/ScriptField_Blade$Item;

    :cond_1
    iget-object v1, p0, Lcom/android/wallpaper/grass/ScriptField_Blade;->mItemArray:[Lcom/android/wallpaper/grass/ScriptField_Blade$Item;

    aget-object v1, v1, p1

    if-nez v1, :cond_2

    iget-object v1, p0, Lcom/android/wallpaper/grass/ScriptField_Blade;->mItemArray:[Lcom/android/wallpaper/grass/ScriptField_Blade$Item;

    new-instance v2, Lcom/android/wallpaper/grass/ScriptField_Blade$Item;

    invoke-direct {v2}, Lcom/android/wallpaper/grass/ScriptField_Blade$Item;-><init>()V

    aput-object v2, v1, p1

    :cond_2
    iget-object v1, p0, Lcom/android/wallpaper/grass/ScriptField_Blade;->mItemArray:[Lcom/android/wallpaper/grass/ScriptField_Blade$Item;

    aget-object v1, v1, p1

    iput p2, v1, Lcom/android/wallpaper/grass/ScriptField_Blade$Item;->angle:F

    if-eqz p3, :cond_3

    iget-object v1, p0, Lcom/android/wallpaper/grass/ScriptField_Blade;->mIOBuffer:Landroid/renderscript/FieldPacker;

    mul-int/lit8 v2, p1, 0x34

    invoke-virtual {v1, v2}, Landroid/renderscript/FieldPacker;->reset(I)V

    iget-object v1, p0, Lcom/android/wallpaper/grass/ScriptField_Blade;->mIOBuffer:Landroid/renderscript/FieldPacker;

    invoke-virtual {v1, p2}, Landroid/renderscript/FieldPacker;->addF32(F)V

    new-instance v0, Landroid/renderscript/FieldPacker;

    const/4 v1, 0x4

    invoke-direct {v0, v1}, Landroid/renderscript/FieldPacker;-><init>(I)V

    invoke-virtual {v0, p2}, Landroid/renderscript/FieldPacker;->addF32(F)V

    iget-object v1, p0, Landroid/renderscript/Script$FieldBase;->mAllocation:Landroid/renderscript/Allocation;

    const/4 v2, 0x0

    invoke-virtual {v1, p1, v2, v0}, Landroid/renderscript/Allocation;->setFromFieldPacker(IILandroid/renderscript/FieldPacker;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :cond_3
    monitor-exit p0

    return-void

    :catchall_0
    move-exception v1

    monitor-exit p0

    throw v1
.end method

.method public declared-synchronized set_b(IFZ)V
    .locals 3
    .param p1    # I
    .param p2    # F
    .param p3    # Z

    monitor-enter p0

    :try_start_0
    iget-object v1, p0, Lcom/android/wallpaper/grass/ScriptField_Blade;->mIOBuffer:Landroid/renderscript/FieldPacker;

    if-nez v1, :cond_0

    new-instance v1, Landroid/renderscript/FieldPacker;

    invoke-virtual {p0}, Landroid/renderscript/Script$FieldBase;->getType()Landroid/renderscript/Type;

    move-result-object v2

    invoke-virtual {v2}, Landroid/renderscript/Type;->getX()I

    move-result v2

    mul-int/lit8 v2, v2, 0x34

    invoke-direct {v1, v2}, Landroid/renderscript/FieldPacker;-><init>(I)V

    iput-object v1, p0, Lcom/android/wallpaper/grass/ScriptField_Blade;->mIOBuffer:Landroid/renderscript/FieldPacker;

    :cond_0
    iget-object v1, p0, Lcom/android/wallpaper/grass/ScriptField_Blade;->mItemArray:[Lcom/android/wallpaper/grass/ScriptField_Blade$Item;

    if-nez v1, :cond_1

    invoke-virtual {p0}, Landroid/renderscript/Script$FieldBase;->getType()Landroid/renderscript/Type;

    move-result-object v1

    invoke-virtual {v1}, Landroid/renderscript/Type;->getX()I

    move-result v1

    new-array v1, v1, [Lcom/android/wallpaper/grass/ScriptField_Blade$Item;

    iput-object v1, p0, Lcom/android/wallpaper/grass/ScriptField_Blade;->mItemArray:[Lcom/android/wallpaper/grass/ScriptField_Blade$Item;

    :cond_1
    iget-object v1, p0, Lcom/android/wallpaper/grass/ScriptField_Blade;->mItemArray:[Lcom/android/wallpaper/grass/ScriptField_Blade$Item;

    aget-object v1, v1, p1

    if-nez v1, :cond_2

    iget-object v1, p0, Lcom/android/wallpaper/grass/ScriptField_Blade;->mItemArray:[Lcom/android/wallpaper/grass/ScriptField_Blade$Item;

    new-instance v2, Lcom/android/wallpaper/grass/ScriptField_Blade$Item;

    invoke-direct {v2}, Lcom/android/wallpaper/grass/ScriptField_Blade$Item;-><init>()V

    aput-object v2, v1, p1

    :cond_2
    iget-object v1, p0, Lcom/android/wallpaper/grass/ScriptField_Blade;->mItemArray:[Lcom/android/wallpaper/grass/ScriptField_Blade$Item;

    aget-object v1, v1, p1

    iput p2, v1, Lcom/android/wallpaper/grass/ScriptField_Blade$Item;->b:F

    if-eqz p3, :cond_3

    iget-object v1, p0, Lcom/android/wallpaper/grass/ScriptField_Blade;->mIOBuffer:Landroid/renderscript/FieldPacker;

    mul-int/lit8 v2, p1, 0x34

    add-int/lit8 v2, v2, 0x2c

    invoke-virtual {v1, v2}, Landroid/renderscript/FieldPacker;->reset(I)V

    iget-object v1, p0, Lcom/android/wallpaper/grass/ScriptField_Blade;->mIOBuffer:Landroid/renderscript/FieldPacker;

    invoke-virtual {v1, p2}, Landroid/renderscript/FieldPacker;->addF32(F)V

    new-instance v0, Landroid/renderscript/FieldPacker;

    const/4 v1, 0x4

    invoke-direct {v0, v1}, Landroid/renderscript/FieldPacker;-><init>(I)V

    invoke-virtual {v0, p2}, Landroid/renderscript/FieldPacker;->addF32(F)V

    iget-object v1, p0, Landroid/renderscript/Script$FieldBase;->mAllocation:Landroid/renderscript/Allocation;

    const/16 v2, 0xb

    invoke-virtual {v1, p1, v2, v0}, Landroid/renderscript/Allocation;->setFromFieldPacker(IILandroid/renderscript/FieldPacker;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :cond_3
    monitor-exit p0

    return-void

    :catchall_0
    move-exception v1

    monitor-exit p0

    throw v1
.end method

.method public declared-synchronized set_h(IFZ)V
    .locals 3
    .param p1    # I
    .param p2    # F
    .param p3    # Z

    monitor-enter p0

    :try_start_0
    iget-object v1, p0, Lcom/android/wallpaper/grass/ScriptField_Blade;->mIOBuffer:Landroid/renderscript/FieldPacker;

    if-nez v1, :cond_0

    new-instance v1, Landroid/renderscript/FieldPacker;

    invoke-virtual {p0}, Landroid/renderscript/Script$FieldBase;->getType()Landroid/renderscript/Type;

    move-result-object v2

    invoke-virtual {v2}, Landroid/renderscript/Type;->getX()I

    move-result v2

    mul-int/lit8 v2, v2, 0x34

    invoke-direct {v1, v2}, Landroid/renderscript/FieldPacker;-><init>(I)V

    iput-object v1, p0, Lcom/android/wallpaper/grass/ScriptField_Blade;->mIOBuffer:Landroid/renderscript/FieldPacker;

    :cond_0
    iget-object v1, p0, Lcom/android/wallpaper/grass/ScriptField_Blade;->mItemArray:[Lcom/android/wallpaper/grass/ScriptField_Blade$Item;

    if-nez v1, :cond_1

    invoke-virtual {p0}, Landroid/renderscript/Script$FieldBase;->getType()Landroid/renderscript/Type;

    move-result-object v1

    invoke-virtual {v1}, Landroid/renderscript/Type;->getX()I

    move-result v1

    new-array v1, v1, [Lcom/android/wallpaper/grass/ScriptField_Blade$Item;

    iput-object v1, p0, Lcom/android/wallpaper/grass/ScriptField_Blade;->mItemArray:[Lcom/android/wallpaper/grass/ScriptField_Blade$Item;

    :cond_1
    iget-object v1, p0, Lcom/android/wallpaper/grass/ScriptField_Blade;->mItemArray:[Lcom/android/wallpaper/grass/ScriptField_Blade$Item;

    aget-object v1, v1, p1

    if-nez v1, :cond_2

    iget-object v1, p0, Lcom/android/wallpaper/grass/ScriptField_Blade;->mItemArray:[Lcom/android/wallpaper/grass/ScriptField_Blade$Item;

    new-instance v2, Lcom/android/wallpaper/grass/ScriptField_Blade$Item;

    invoke-direct {v2}, Lcom/android/wallpaper/grass/ScriptField_Blade$Item;-><init>()V

    aput-object v2, v1, p1

    :cond_2
    iget-object v1, p0, Lcom/android/wallpaper/grass/ScriptField_Blade;->mItemArray:[Lcom/android/wallpaper/grass/ScriptField_Blade$Item;

    aget-object v1, v1, p1

    iput p2, v1, Lcom/android/wallpaper/grass/ScriptField_Blade$Item;->h:F

    if-eqz p3, :cond_3

    iget-object v1, p0, Lcom/android/wallpaper/grass/ScriptField_Blade;->mIOBuffer:Landroid/renderscript/FieldPacker;

    mul-int/lit8 v2, p1, 0x34

    add-int/lit8 v2, v2, 0x24

    invoke-virtual {v1, v2}, Landroid/renderscript/FieldPacker;->reset(I)V

    iget-object v1, p0, Lcom/android/wallpaper/grass/ScriptField_Blade;->mIOBuffer:Landroid/renderscript/FieldPacker;

    invoke-virtual {v1, p2}, Landroid/renderscript/FieldPacker;->addF32(F)V

    new-instance v0, Landroid/renderscript/FieldPacker;

    const/4 v1, 0x4

    invoke-direct {v0, v1}, Landroid/renderscript/FieldPacker;-><init>(I)V

    invoke-virtual {v0, p2}, Landroid/renderscript/FieldPacker;->addF32(F)V

    iget-object v1, p0, Landroid/renderscript/Script$FieldBase;->mAllocation:Landroid/renderscript/Allocation;

    const/16 v2, 0x9

    invoke-virtual {v1, p1, v2, v0}, Landroid/renderscript/Allocation;->setFromFieldPacker(IILandroid/renderscript/FieldPacker;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :cond_3
    monitor-exit p0

    return-void

    :catchall_0
    move-exception v1

    monitor-exit p0

    throw v1
.end method

.method public declared-synchronized set_hardness(IFZ)V
    .locals 3
    .param p1    # I
    .param p2    # F
    .param p3    # Z

    monitor-enter p0

    :try_start_0
    iget-object v1, p0, Lcom/android/wallpaper/grass/ScriptField_Blade;->mIOBuffer:Landroid/renderscript/FieldPacker;

    if-nez v1, :cond_0

    new-instance v1, Landroid/renderscript/FieldPacker;

    invoke-virtual {p0}, Landroid/renderscript/Script$FieldBase;->getType()Landroid/renderscript/Type;

    move-result-object v2

    invoke-virtual {v2}, Landroid/renderscript/Type;->getX()I

    move-result v2

    mul-int/lit8 v2, v2, 0x34

    invoke-direct {v1, v2}, Landroid/renderscript/FieldPacker;-><init>(I)V

    iput-object v1, p0, Lcom/android/wallpaper/grass/ScriptField_Blade;->mIOBuffer:Landroid/renderscript/FieldPacker;

    :cond_0
    iget-object v1, p0, Lcom/android/wallpaper/grass/ScriptField_Blade;->mItemArray:[Lcom/android/wallpaper/grass/ScriptField_Blade$Item;

    if-nez v1, :cond_1

    invoke-virtual {p0}, Landroid/renderscript/Script$FieldBase;->getType()Landroid/renderscript/Type;

    move-result-object v1

    invoke-virtual {v1}, Landroid/renderscript/Type;->getX()I

    move-result v1

    new-array v1, v1, [Lcom/android/wallpaper/grass/ScriptField_Blade$Item;

    iput-object v1, p0, Lcom/android/wallpaper/grass/ScriptField_Blade;->mItemArray:[Lcom/android/wallpaper/grass/ScriptField_Blade$Item;

    :cond_1
    iget-object v1, p0, Lcom/android/wallpaper/grass/ScriptField_Blade;->mItemArray:[Lcom/android/wallpaper/grass/ScriptField_Blade$Item;

    aget-object v1, v1, p1

    if-nez v1, :cond_2

    iget-object v1, p0, Lcom/android/wallpaper/grass/ScriptField_Blade;->mItemArray:[Lcom/android/wallpaper/grass/ScriptField_Blade$Item;

    new-instance v2, Lcom/android/wallpaper/grass/ScriptField_Blade$Item;

    invoke-direct {v2}, Lcom/android/wallpaper/grass/ScriptField_Blade$Item;-><init>()V

    aput-object v2, v1, p1

    :cond_2
    iget-object v1, p0, Lcom/android/wallpaper/grass/ScriptField_Blade;->mItemArray:[Lcom/android/wallpaper/grass/ScriptField_Blade$Item;

    aget-object v1, v1, p1

    iput p2, v1, Lcom/android/wallpaper/grass/ScriptField_Blade$Item;->hardness:F

    if-eqz p3, :cond_3

    iget-object v1, p0, Lcom/android/wallpaper/grass/ScriptField_Blade;->mIOBuffer:Landroid/renderscript/FieldPacker;

    mul-int/lit8 v2, p1, 0x34

    add-int/lit8 v2, v2, 0x20

    invoke-virtual {v1, v2}, Landroid/renderscript/FieldPacker;->reset(I)V

    iget-object v1, p0, Lcom/android/wallpaper/grass/ScriptField_Blade;->mIOBuffer:Landroid/renderscript/FieldPacker;

    invoke-virtual {v1, p2}, Landroid/renderscript/FieldPacker;->addF32(F)V

    new-instance v0, Landroid/renderscript/FieldPacker;

    const/4 v1, 0x4

    invoke-direct {v0, v1}, Landroid/renderscript/FieldPacker;-><init>(I)V

    invoke-virtual {v0, p2}, Landroid/renderscript/FieldPacker;->addF32(F)V

    iget-object v1, p0, Landroid/renderscript/Script$FieldBase;->mAllocation:Landroid/renderscript/Allocation;

    const/16 v2, 0x8

    invoke-virtual {v1, p1, v2, v0}, Landroid/renderscript/Allocation;->setFromFieldPacker(IILandroid/renderscript/FieldPacker;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :cond_3
    monitor-exit p0

    return-void

    :catchall_0
    move-exception v1

    monitor-exit p0

    throw v1
.end method

.method public declared-synchronized set_lengthX(IFZ)V
    .locals 3
    .param p1    # I
    .param p2    # F
    .param p3    # Z

    monitor-enter p0

    :try_start_0
    iget-object v1, p0, Lcom/android/wallpaper/grass/ScriptField_Blade;->mIOBuffer:Landroid/renderscript/FieldPacker;

    if-nez v1, :cond_0

    new-instance v1, Landroid/renderscript/FieldPacker;

    invoke-virtual {p0}, Landroid/renderscript/Script$FieldBase;->getType()Landroid/renderscript/Type;

    move-result-object v2

    invoke-virtual {v2}, Landroid/renderscript/Type;->getX()I

    move-result v2

    mul-int/lit8 v2, v2, 0x34

    invoke-direct {v1, v2}, Landroid/renderscript/FieldPacker;-><init>(I)V

    iput-object v1, p0, Lcom/android/wallpaper/grass/ScriptField_Blade;->mIOBuffer:Landroid/renderscript/FieldPacker;

    :cond_0
    iget-object v1, p0, Lcom/android/wallpaper/grass/ScriptField_Blade;->mItemArray:[Lcom/android/wallpaper/grass/ScriptField_Blade$Item;

    if-nez v1, :cond_1

    invoke-virtual {p0}, Landroid/renderscript/Script$FieldBase;->getType()Landroid/renderscript/Type;

    move-result-object v1

    invoke-virtual {v1}, Landroid/renderscript/Type;->getX()I

    move-result v1

    new-array v1, v1, [Lcom/android/wallpaper/grass/ScriptField_Blade$Item;

    iput-object v1, p0, Lcom/android/wallpaper/grass/ScriptField_Blade;->mItemArray:[Lcom/android/wallpaper/grass/ScriptField_Blade$Item;

    :cond_1
    iget-object v1, p0, Lcom/android/wallpaper/grass/ScriptField_Blade;->mItemArray:[Lcom/android/wallpaper/grass/ScriptField_Blade$Item;

    aget-object v1, v1, p1

    if-nez v1, :cond_2

    iget-object v1, p0, Lcom/android/wallpaper/grass/ScriptField_Blade;->mItemArray:[Lcom/android/wallpaper/grass/ScriptField_Blade$Item;

    new-instance v2, Lcom/android/wallpaper/grass/ScriptField_Blade$Item;

    invoke-direct {v2}, Lcom/android/wallpaper/grass/ScriptField_Blade$Item;-><init>()V

    aput-object v2, v1, p1

    :cond_2
    iget-object v1, p0, Lcom/android/wallpaper/grass/ScriptField_Blade;->mItemArray:[Lcom/android/wallpaper/grass/ScriptField_Blade$Item;

    aget-object v1, v1, p1

    iput p2, v1, Lcom/android/wallpaper/grass/ScriptField_Blade$Item;->lengthX:F

    if-eqz p3, :cond_3

    iget-object v1, p0, Lcom/android/wallpaper/grass/ScriptField_Blade;->mIOBuffer:Landroid/renderscript/FieldPacker;

    mul-int/lit8 v2, p1, 0x34

    add-int/lit8 v2, v2, 0x18

    invoke-virtual {v1, v2}, Landroid/renderscript/FieldPacker;->reset(I)V

    iget-object v1, p0, Lcom/android/wallpaper/grass/ScriptField_Blade;->mIOBuffer:Landroid/renderscript/FieldPacker;

    invoke-virtual {v1, p2}, Landroid/renderscript/FieldPacker;->addF32(F)V

    new-instance v0, Landroid/renderscript/FieldPacker;

    const/4 v1, 0x4

    invoke-direct {v0, v1}, Landroid/renderscript/FieldPacker;-><init>(I)V

    invoke-virtual {v0, p2}, Landroid/renderscript/FieldPacker;->addF32(F)V

    iget-object v1, p0, Landroid/renderscript/Script$FieldBase;->mAllocation:Landroid/renderscript/Allocation;

    const/4 v2, 0x6

    invoke-virtual {v1, p1, v2, v0}, Landroid/renderscript/Allocation;->setFromFieldPacker(IILandroid/renderscript/FieldPacker;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :cond_3
    monitor-exit p0

    return-void

    :catchall_0
    move-exception v1

    monitor-exit p0

    throw v1
.end method

.method public declared-synchronized set_lengthY(IFZ)V
    .locals 3
    .param p1    # I
    .param p2    # F
    .param p3    # Z

    monitor-enter p0

    :try_start_0
    iget-object v1, p0, Lcom/android/wallpaper/grass/ScriptField_Blade;->mIOBuffer:Landroid/renderscript/FieldPacker;

    if-nez v1, :cond_0

    new-instance v1, Landroid/renderscript/FieldPacker;

    invoke-virtual {p0}, Landroid/renderscript/Script$FieldBase;->getType()Landroid/renderscript/Type;

    move-result-object v2

    invoke-virtual {v2}, Landroid/renderscript/Type;->getX()I

    move-result v2

    mul-int/lit8 v2, v2, 0x34

    invoke-direct {v1, v2}, Landroid/renderscript/FieldPacker;-><init>(I)V

    iput-object v1, p0, Lcom/android/wallpaper/grass/ScriptField_Blade;->mIOBuffer:Landroid/renderscript/FieldPacker;

    :cond_0
    iget-object v1, p0, Lcom/android/wallpaper/grass/ScriptField_Blade;->mItemArray:[Lcom/android/wallpaper/grass/ScriptField_Blade$Item;

    if-nez v1, :cond_1

    invoke-virtual {p0}, Landroid/renderscript/Script$FieldBase;->getType()Landroid/renderscript/Type;

    move-result-object v1

    invoke-virtual {v1}, Landroid/renderscript/Type;->getX()I

    move-result v1

    new-array v1, v1, [Lcom/android/wallpaper/grass/ScriptField_Blade$Item;

    iput-object v1, p0, Lcom/android/wallpaper/grass/ScriptField_Blade;->mItemArray:[Lcom/android/wallpaper/grass/ScriptField_Blade$Item;

    :cond_1
    iget-object v1, p0, Lcom/android/wallpaper/grass/ScriptField_Blade;->mItemArray:[Lcom/android/wallpaper/grass/ScriptField_Blade$Item;

    aget-object v1, v1, p1

    if-nez v1, :cond_2

    iget-object v1, p0, Lcom/android/wallpaper/grass/ScriptField_Blade;->mItemArray:[Lcom/android/wallpaper/grass/ScriptField_Blade$Item;

    new-instance v2, Lcom/android/wallpaper/grass/ScriptField_Blade$Item;

    invoke-direct {v2}, Lcom/android/wallpaper/grass/ScriptField_Blade$Item;-><init>()V

    aput-object v2, v1, p1

    :cond_2
    iget-object v1, p0, Lcom/android/wallpaper/grass/ScriptField_Blade;->mItemArray:[Lcom/android/wallpaper/grass/ScriptField_Blade$Item;

    aget-object v1, v1, p1

    iput p2, v1, Lcom/android/wallpaper/grass/ScriptField_Blade$Item;->lengthY:F

    if-eqz p3, :cond_3

    iget-object v1, p0, Lcom/android/wallpaper/grass/ScriptField_Blade;->mIOBuffer:Landroid/renderscript/FieldPacker;

    mul-int/lit8 v2, p1, 0x34

    add-int/lit8 v2, v2, 0x1c

    invoke-virtual {v1, v2}, Landroid/renderscript/FieldPacker;->reset(I)V

    iget-object v1, p0, Lcom/android/wallpaper/grass/ScriptField_Blade;->mIOBuffer:Landroid/renderscript/FieldPacker;

    invoke-virtual {v1, p2}, Landroid/renderscript/FieldPacker;->addF32(F)V

    new-instance v0, Landroid/renderscript/FieldPacker;

    const/4 v1, 0x4

    invoke-direct {v0, v1}, Landroid/renderscript/FieldPacker;-><init>(I)V

    invoke-virtual {v0, p2}, Landroid/renderscript/FieldPacker;->addF32(F)V

    iget-object v1, p0, Landroid/renderscript/Script$FieldBase;->mAllocation:Landroid/renderscript/Allocation;

    const/4 v2, 0x7

    invoke-virtual {v1, p1, v2, v0}, Landroid/renderscript/Allocation;->setFromFieldPacker(IILandroid/renderscript/FieldPacker;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :cond_3
    monitor-exit p0

    return-void

    :catchall_0
    move-exception v1

    monitor-exit p0

    throw v1
.end method

.method public declared-synchronized set_offset(IFZ)V
    .locals 3
    .param p1    # I
    .param p2    # F
    .param p3    # Z

    monitor-enter p0

    :try_start_0
    iget-object v1, p0, Lcom/android/wallpaper/grass/ScriptField_Blade;->mIOBuffer:Landroid/renderscript/FieldPacker;

    if-nez v1, :cond_0

    new-instance v1, Landroid/renderscript/FieldPacker;

    invoke-virtual {p0}, Landroid/renderscript/Script$FieldBase;->getType()Landroid/renderscript/Type;

    move-result-object v2

    invoke-virtual {v2}, Landroid/renderscript/Type;->getX()I

    move-result v2

    mul-int/lit8 v2, v2, 0x34

    invoke-direct {v1, v2}, Landroid/renderscript/FieldPacker;-><init>(I)V

    iput-object v1, p0, Lcom/android/wallpaper/grass/ScriptField_Blade;->mIOBuffer:Landroid/renderscript/FieldPacker;

    :cond_0
    iget-object v1, p0, Lcom/android/wallpaper/grass/ScriptField_Blade;->mItemArray:[Lcom/android/wallpaper/grass/ScriptField_Blade$Item;

    if-nez v1, :cond_1

    invoke-virtual {p0}, Landroid/renderscript/Script$FieldBase;->getType()Landroid/renderscript/Type;

    move-result-object v1

    invoke-virtual {v1}, Landroid/renderscript/Type;->getX()I

    move-result v1

    new-array v1, v1, [Lcom/android/wallpaper/grass/ScriptField_Blade$Item;

    iput-object v1, p0, Lcom/android/wallpaper/grass/ScriptField_Blade;->mItemArray:[Lcom/android/wallpaper/grass/ScriptField_Blade$Item;

    :cond_1
    iget-object v1, p0, Lcom/android/wallpaper/grass/ScriptField_Blade;->mItemArray:[Lcom/android/wallpaper/grass/ScriptField_Blade$Item;

    aget-object v1, v1, p1

    if-nez v1, :cond_2

    iget-object v1, p0, Lcom/android/wallpaper/grass/ScriptField_Blade;->mItemArray:[Lcom/android/wallpaper/grass/ScriptField_Blade$Item;

    new-instance v2, Lcom/android/wallpaper/grass/ScriptField_Blade$Item;

    invoke-direct {v2}, Lcom/android/wallpaper/grass/ScriptField_Blade$Item;-><init>()V

    aput-object v2, v1, p1

    :cond_2
    iget-object v1, p0, Lcom/android/wallpaper/grass/ScriptField_Blade;->mItemArray:[Lcom/android/wallpaper/grass/ScriptField_Blade$Item;

    aget-object v1, v1, p1

    iput p2, v1, Lcom/android/wallpaper/grass/ScriptField_Blade$Item;->offset:F

    if-eqz p3, :cond_3

    iget-object v1, p0, Lcom/android/wallpaper/grass/ScriptField_Blade;->mIOBuffer:Landroid/renderscript/FieldPacker;

    mul-int/lit8 v2, p1, 0x34

    add-int/lit8 v2, v2, 0x10

    invoke-virtual {v1, v2}, Landroid/renderscript/FieldPacker;->reset(I)V

    iget-object v1, p0, Lcom/android/wallpaper/grass/ScriptField_Blade;->mIOBuffer:Landroid/renderscript/FieldPacker;

    invoke-virtual {v1, p2}, Landroid/renderscript/FieldPacker;->addF32(F)V

    new-instance v0, Landroid/renderscript/FieldPacker;

    const/4 v1, 0x4

    invoke-direct {v0, v1}, Landroid/renderscript/FieldPacker;-><init>(I)V

    invoke-virtual {v0, p2}, Landroid/renderscript/FieldPacker;->addF32(F)V

    iget-object v1, p0, Landroid/renderscript/Script$FieldBase;->mAllocation:Landroid/renderscript/Allocation;

    const/4 v2, 0x4

    invoke-virtual {v1, p1, v2, v0}, Landroid/renderscript/Allocation;->setFromFieldPacker(IILandroid/renderscript/FieldPacker;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :cond_3
    monitor-exit p0

    return-void

    :catchall_0
    move-exception v1

    monitor-exit p0

    throw v1
.end method

.method public declared-synchronized set_s(IFZ)V
    .locals 3
    .param p1    # I
    .param p2    # F
    .param p3    # Z

    monitor-enter p0

    :try_start_0
    iget-object v1, p0, Lcom/android/wallpaper/grass/ScriptField_Blade;->mIOBuffer:Landroid/renderscript/FieldPacker;

    if-nez v1, :cond_0

    new-instance v1, Landroid/renderscript/FieldPacker;

    invoke-virtual {p0}, Landroid/renderscript/Script$FieldBase;->getType()Landroid/renderscript/Type;

    move-result-object v2

    invoke-virtual {v2}, Landroid/renderscript/Type;->getX()I

    move-result v2

    mul-int/lit8 v2, v2, 0x34

    invoke-direct {v1, v2}, Landroid/renderscript/FieldPacker;-><init>(I)V

    iput-object v1, p0, Lcom/android/wallpaper/grass/ScriptField_Blade;->mIOBuffer:Landroid/renderscript/FieldPacker;

    :cond_0
    iget-object v1, p0, Lcom/android/wallpaper/grass/ScriptField_Blade;->mItemArray:[Lcom/android/wallpaper/grass/ScriptField_Blade$Item;

    if-nez v1, :cond_1

    invoke-virtual {p0}, Landroid/renderscript/Script$FieldBase;->getType()Landroid/renderscript/Type;

    move-result-object v1

    invoke-virtual {v1}, Landroid/renderscript/Type;->getX()I

    move-result v1

    new-array v1, v1, [Lcom/android/wallpaper/grass/ScriptField_Blade$Item;

    iput-object v1, p0, Lcom/android/wallpaper/grass/ScriptField_Blade;->mItemArray:[Lcom/android/wallpaper/grass/ScriptField_Blade$Item;

    :cond_1
    iget-object v1, p0, Lcom/android/wallpaper/grass/ScriptField_Blade;->mItemArray:[Lcom/android/wallpaper/grass/ScriptField_Blade$Item;

    aget-object v1, v1, p1

    if-nez v1, :cond_2

    iget-object v1, p0, Lcom/android/wallpaper/grass/ScriptField_Blade;->mItemArray:[Lcom/android/wallpaper/grass/ScriptField_Blade$Item;

    new-instance v2, Lcom/android/wallpaper/grass/ScriptField_Blade$Item;

    invoke-direct {v2}, Lcom/android/wallpaper/grass/ScriptField_Blade$Item;-><init>()V

    aput-object v2, v1, p1

    :cond_2
    iget-object v1, p0, Lcom/android/wallpaper/grass/ScriptField_Blade;->mItemArray:[Lcom/android/wallpaper/grass/ScriptField_Blade$Item;

    aget-object v1, v1, p1

    iput p2, v1, Lcom/android/wallpaper/grass/ScriptField_Blade$Item;->s:F

    if-eqz p3, :cond_3

    iget-object v1, p0, Lcom/android/wallpaper/grass/ScriptField_Blade;->mIOBuffer:Landroid/renderscript/FieldPacker;

    mul-int/lit8 v2, p1, 0x34

    add-int/lit8 v2, v2, 0x28

    invoke-virtual {v1, v2}, Landroid/renderscript/FieldPacker;->reset(I)V

    iget-object v1, p0, Lcom/android/wallpaper/grass/ScriptField_Blade;->mIOBuffer:Landroid/renderscript/FieldPacker;

    invoke-virtual {v1, p2}, Landroid/renderscript/FieldPacker;->addF32(F)V

    new-instance v0, Landroid/renderscript/FieldPacker;

    const/4 v1, 0x4

    invoke-direct {v0, v1}, Landroid/renderscript/FieldPacker;-><init>(I)V

    invoke-virtual {v0, p2}, Landroid/renderscript/FieldPacker;->addF32(F)V

    iget-object v1, p0, Landroid/renderscript/Script$FieldBase;->mAllocation:Landroid/renderscript/Allocation;

    const/16 v2, 0xa

    invoke-virtual {v1, p1, v2, v0}, Landroid/renderscript/Allocation;->setFromFieldPacker(IILandroid/renderscript/FieldPacker;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :cond_3
    monitor-exit p0

    return-void

    :catchall_0
    move-exception v1

    monitor-exit p0

    throw v1
.end method

.method public declared-synchronized set_scale(IFZ)V
    .locals 3
    .param p1    # I
    .param p2    # F
    .param p3    # Z

    monitor-enter p0

    :try_start_0
    iget-object v1, p0, Lcom/android/wallpaper/grass/ScriptField_Blade;->mIOBuffer:Landroid/renderscript/FieldPacker;

    if-nez v1, :cond_0

    new-instance v1, Landroid/renderscript/FieldPacker;

    invoke-virtual {p0}, Landroid/renderscript/Script$FieldBase;->getType()Landroid/renderscript/Type;

    move-result-object v2

    invoke-virtual {v2}, Landroid/renderscript/Type;->getX()I

    move-result v2

    mul-int/lit8 v2, v2, 0x34

    invoke-direct {v1, v2}, Landroid/renderscript/FieldPacker;-><init>(I)V

    iput-object v1, p0, Lcom/android/wallpaper/grass/ScriptField_Blade;->mIOBuffer:Landroid/renderscript/FieldPacker;

    :cond_0
    iget-object v1, p0, Lcom/android/wallpaper/grass/ScriptField_Blade;->mItemArray:[Lcom/android/wallpaper/grass/ScriptField_Blade$Item;

    if-nez v1, :cond_1

    invoke-virtual {p0}, Landroid/renderscript/Script$FieldBase;->getType()Landroid/renderscript/Type;

    move-result-object v1

    invoke-virtual {v1}, Landroid/renderscript/Type;->getX()I

    move-result v1

    new-array v1, v1, [Lcom/android/wallpaper/grass/ScriptField_Blade$Item;

    iput-object v1, p0, Lcom/android/wallpaper/grass/ScriptField_Blade;->mItemArray:[Lcom/android/wallpaper/grass/ScriptField_Blade$Item;

    :cond_1
    iget-object v1, p0, Lcom/android/wallpaper/grass/ScriptField_Blade;->mItemArray:[Lcom/android/wallpaper/grass/ScriptField_Blade$Item;

    aget-object v1, v1, p1

    if-nez v1, :cond_2

    iget-object v1, p0, Lcom/android/wallpaper/grass/ScriptField_Blade;->mItemArray:[Lcom/android/wallpaper/grass/ScriptField_Blade$Item;

    new-instance v2, Lcom/android/wallpaper/grass/ScriptField_Blade$Item;

    invoke-direct {v2}, Lcom/android/wallpaper/grass/ScriptField_Blade$Item;-><init>()V

    aput-object v2, v1, p1

    :cond_2
    iget-object v1, p0, Lcom/android/wallpaper/grass/ScriptField_Blade;->mItemArray:[Lcom/android/wallpaper/grass/ScriptField_Blade$Item;

    aget-object v1, v1, p1

    iput p2, v1, Lcom/android/wallpaper/grass/ScriptField_Blade$Item;->scale:F

    if-eqz p3, :cond_3

    iget-object v1, p0, Lcom/android/wallpaper/grass/ScriptField_Blade;->mIOBuffer:Landroid/renderscript/FieldPacker;

    mul-int/lit8 v2, p1, 0x34

    add-int/lit8 v2, v2, 0x14

    invoke-virtual {v1, v2}, Landroid/renderscript/FieldPacker;->reset(I)V

    iget-object v1, p0, Lcom/android/wallpaper/grass/ScriptField_Blade;->mIOBuffer:Landroid/renderscript/FieldPacker;

    invoke-virtual {v1, p2}, Landroid/renderscript/FieldPacker;->addF32(F)V

    new-instance v0, Landroid/renderscript/FieldPacker;

    const/4 v1, 0x4

    invoke-direct {v0, v1}, Landroid/renderscript/FieldPacker;-><init>(I)V

    invoke-virtual {v0, p2}, Landroid/renderscript/FieldPacker;->addF32(F)V

    iget-object v1, p0, Landroid/renderscript/Script$FieldBase;->mAllocation:Landroid/renderscript/Allocation;

    const/4 v2, 0x5

    invoke-virtual {v1, p1, v2, v0}, Landroid/renderscript/Allocation;->setFromFieldPacker(IILandroid/renderscript/FieldPacker;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :cond_3
    monitor-exit p0

    return-void

    :catchall_0
    move-exception v1

    monitor-exit p0

    throw v1
.end method

.method public declared-synchronized set_size(IIZ)V
    .locals 3
    .param p1    # I
    .param p2    # I
    .param p3    # Z

    monitor-enter p0

    :try_start_0
    iget-object v1, p0, Lcom/android/wallpaper/grass/ScriptField_Blade;->mIOBuffer:Landroid/renderscript/FieldPacker;

    if-nez v1, :cond_0

    new-instance v1, Landroid/renderscript/FieldPacker;

    invoke-virtual {p0}, Landroid/renderscript/Script$FieldBase;->getType()Landroid/renderscript/Type;

    move-result-object v2

    invoke-virtual {v2}, Landroid/renderscript/Type;->getX()I

    move-result v2

    mul-int/lit8 v2, v2, 0x34

    invoke-direct {v1, v2}, Landroid/renderscript/FieldPacker;-><init>(I)V

    iput-object v1, p0, Lcom/android/wallpaper/grass/ScriptField_Blade;->mIOBuffer:Landroid/renderscript/FieldPacker;

    :cond_0
    iget-object v1, p0, Lcom/android/wallpaper/grass/ScriptField_Blade;->mItemArray:[Lcom/android/wallpaper/grass/ScriptField_Blade$Item;

    if-nez v1, :cond_1

    invoke-virtual {p0}, Landroid/renderscript/Script$FieldBase;->getType()Landroid/renderscript/Type;

    move-result-object v1

    invoke-virtual {v1}, Landroid/renderscript/Type;->getX()I

    move-result v1

    new-array v1, v1, [Lcom/android/wallpaper/grass/ScriptField_Blade$Item;

    iput-object v1, p0, Lcom/android/wallpaper/grass/ScriptField_Blade;->mItemArray:[Lcom/android/wallpaper/grass/ScriptField_Blade$Item;

    :cond_1
    iget-object v1, p0, Lcom/android/wallpaper/grass/ScriptField_Blade;->mItemArray:[Lcom/android/wallpaper/grass/ScriptField_Blade$Item;

    aget-object v1, v1, p1

    if-nez v1, :cond_2

    iget-object v1, p0, Lcom/android/wallpaper/grass/ScriptField_Blade;->mItemArray:[Lcom/android/wallpaper/grass/ScriptField_Blade$Item;

    new-instance v2, Lcom/android/wallpaper/grass/ScriptField_Blade$Item;

    invoke-direct {v2}, Lcom/android/wallpaper/grass/ScriptField_Blade$Item;-><init>()V

    aput-object v2, v1, p1

    :cond_2
    iget-object v1, p0, Lcom/android/wallpaper/grass/ScriptField_Blade;->mItemArray:[Lcom/android/wallpaper/grass/ScriptField_Blade$Item;

    aget-object v1, v1, p1

    iput p2, v1, Lcom/android/wallpaper/grass/ScriptField_Blade$Item;->size:I

    if-eqz p3, :cond_3

    iget-object v1, p0, Lcom/android/wallpaper/grass/ScriptField_Blade;->mIOBuffer:Landroid/renderscript/FieldPacker;

    mul-int/lit8 v2, p1, 0x34

    add-int/lit8 v2, v2, 0x4

    invoke-virtual {v1, v2}, Landroid/renderscript/FieldPacker;->reset(I)V

    iget-object v1, p0, Lcom/android/wallpaper/grass/ScriptField_Blade;->mIOBuffer:Landroid/renderscript/FieldPacker;

    invoke-virtual {v1, p2}, Landroid/renderscript/FieldPacker;->addI32(I)V

    new-instance v0, Landroid/renderscript/FieldPacker;

    const/4 v1, 0x4

    invoke-direct {v0, v1}, Landroid/renderscript/FieldPacker;-><init>(I)V

    invoke-virtual {v0, p2}, Landroid/renderscript/FieldPacker;->addI32(I)V

    iget-object v1, p0, Landroid/renderscript/Script$FieldBase;->mAllocation:Landroid/renderscript/Allocation;

    const/4 v2, 0x1

    invoke-virtual {v1, p1, v2, v0}, Landroid/renderscript/Allocation;->setFromFieldPacker(IILandroid/renderscript/FieldPacker;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :cond_3
    monitor-exit p0

    return-void

    :catchall_0
    move-exception v1

    monitor-exit p0

    throw v1
.end method

.method public declared-synchronized set_turbulencex(IFZ)V
    .locals 3
    .param p1    # I
    .param p2    # F
    .param p3    # Z

    monitor-enter p0

    :try_start_0
    iget-object v1, p0, Lcom/android/wallpaper/grass/ScriptField_Blade;->mIOBuffer:Landroid/renderscript/FieldPacker;

    if-nez v1, :cond_0

    new-instance v1, Landroid/renderscript/FieldPacker;

    invoke-virtual {p0}, Landroid/renderscript/Script$FieldBase;->getType()Landroid/renderscript/Type;

    move-result-object v2

    invoke-virtual {v2}, Landroid/renderscript/Type;->getX()I

    move-result v2

    mul-int/lit8 v2, v2, 0x34

    invoke-direct {v1, v2}, Landroid/renderscript/FieldPacker;-><init>(I)V

    iput-object v1, p0, Lcom/android/wallpaper/grass/ScriptField_Blade;->mIOBuffer:Landroid/renderscript/FieldPacker;

    :cond_0
    iget-object v1, p0, Lcom/android/wallpaper/grass/ScriptField_Blade;->mItemArray:[Lcom/android/wallpaper/grass/ScriptField_Blade$Item;

    if-nez v1, :cond_1

    invoke-virtual {p0}, Landroid/renderscript/Script$FieldBase;->getType()Landroid/renderscript/Type;

    move-result-object v1

    invoke-virtual {v1}, Landroid/renderscript/Type;->getX()I

    move-result v1

    new-array v1, v1, [Lcom/android/wallpaper/grass/ScriptField_Blade$Item;

    iput-object v1, p0, Lcom/android/wallpaper/grass/ScriptField_Blade;->mItemArray:[Lcom/android/wallpaper/grass/ScriptField_Blade$Item;

    :cond_1
    iget-object v1, p0, Lcom/android/wallpaper/grass/ScriptField_Blade;->mItemArray:[Lcom/android/wallpaper/grass/ScriptField_Blade$Item;

    aget-object v1, v1, p1

    if-nez v1, :cond_2

    iget-object v1, p0, Lcom/android/wallpaper/grass/ScriptField_Blade;->mItemArray:[Lcom/android/wallpaper/grass/ScriptField_Blade$Item;

    new-instance v2, Lcom/android/wallpaper/grass/ScriptField_Blade$Item;

    invoke-direct {v2}, Lcom/android/wallpaper/grass/ScriptField_Blade$Item;-><init>()V

    aput-object v2, v1, p1

    :cond_2
    iget-object v1, p0, Lcom/android/wallpaper/grass/ScriptField_Blade;->mItemArray:[Lcom/android/wallpaper/grass/ScriptField_Blade$Item;

    aget-object v1, v1, p1

    iput p2, v1, Lcom/android/wallpaper/grass/ScriptField_Blade$Item;->turbulencex:F

    if-eqz p3, :cond_3

    iget-object v1, p0, Lcom/android/wallpaper/grass/ScriptField_Blade;->mIOBuffer:Landroid/renderscript/FieldPacker;

    mul-int/lit8 v2, p1, 0x34

    add-int/lit8 v2, v2, 0x30

    invoke-virtual {v1, v2}, Landroid/renderscript/FieldPacker;->reset(I)V

    iget-object v1, p0, Lcom/android/wallpaper/grass/ScriptField_Blade;->mIOBuffer:Landroid/renderscript/FieldPacker;

    invoke-virtual {v1, p2}, Landroid/renderscript/FieldPacker;->addF32(F)V

    new-instance v0, Landroid/renderscript/FieldPacker;

    const/4 v1, 0x4

    invoke-direct {v0, v1}, Landroid/renderscript/FieldPacker;-><init>(I)V

    invoke-virtual {v0, p2}, Landroid/renderscript/FieldPacker;->addF32(F)V

    iget-object v1, p0, Landroid/renderscript/Script$FieldBase;->mAllocation:Landroid/renderscript/Allocation;

    const/16 v2, 0xc

    invoke-virtual {v1, p1, v2, v0}, Landroid/renderscript/Allocation;->setFromFieldPacker(IILandroid/renderscript/FieldPacker;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :cond_3
    monitor-exit p0

    return-void

    :catchall_0
    move-exception v1

    monitor-exit p0

    throw v1
.end method

.method public declared-synchronized set_xPos(IFZ)V
    .locals 3
    .param p1    # I
    .param p2    # F
    .param p3    # Z

    monitor-enter p0

    :try_start_0
    iget-object v1, p0, Lcom/android/wallpaper/grass/ScriptField_Blade;->mIOBuffer:Landroid/renderscript/FieldPacker;

    if-nez v1, :cond_0

    new-instance v1, Landroid/renderscript/FieldPacker;

    invoke-virtual {p0}, Landroid/renderscript/Script$FieldBase;->getType()Landroid/renderscript/Type;

    move-result-object v2

    invoke-virtual {v2}, Landroid/renderscript/Type;->getX()I

    move-result v2

    mul-int/lit8 v2, v2, 0x34

    invoke-direct {v1, v2}, Landroid/renderscript/FieldPacker;-><init>(I)V

    iput-object v1, p0, Lcom/android/wallpaper/grass/ScriptField_Blade;->mIOBuffer:Landroid/renderscript/FieldPacker;

    :cond_0
    iget-object v1, p0, Lcom/android/wallpaper/grass/ScriptField_Blade;->mItemArray:[Lcom/android/wallpaper/grass/ScriptField_Blade$Item;

    if-nez v1, :cond_1

    invoke-virtual {p0}, Landroid/renderscript/Script$FieldBase;->getType()Landroid/renderscript/Type;

    move-result-object v1

    invoke-virtual {v1}, Landroid/renderscript/Type;->getX()I

    move-result v1

    new-array v1, v1, [Lcom/android/wallpaper/grass/ScriptField_Blade$Item;

    iput-object v1, p0, Lcom/android/wallpaper/grass/ScriptField_Blade;->mItemArray:[Lcom/android/wallpaper/grass/ScriptField_Blade$Item;

    :cond_1
    iget-object v1, p0, Lcom/android/wallpaper/grass/ScriptField_Blade;->mItemArray:[Lcom/android/wallpaper/grass/ScriptField_Blade$Item;

    aget-object v1, v1, p1

    if-nez v1, :cond_2

    iget-object v1, p0, Lcom/android/wallpaper/grass/ScriptField_Blade;->mItemArray:[Lcom/android/wallpaper/grass/ScriptField_Blade$Item;

    new-instance v2, Lcom/android/wallpaper/grass/ScriptField_Blade$Item;

    invoke-direct {v2}, Lcom/android/wallpaper/grass/ScriptField_Blade$Item;-><init>()V

    aput-object v2, v1, p1

    :cond_2
    iget-object v1, p0, Lcom/android/wallpaper/grass/ScriptField_Blade;->mItemArray:[Lcom/android/wallpaper/grass/ScriptField_Blade$Item;

    aget-object v1, v1, p1

    iput p2, v1, Lcom/android/wallpaper/grass/ScriptField_Blade$Item;->xPos:F

    if-eqz p3, :cond_3

    iget-object v1, p0, Lcom/android/wallpaper/grass/ScriptField_Blade;->mIOBuffer:Landroid/renderscript/FieldPacker;

    mul-int/lit8 v2, p1, 0x34

    add-int/lit8 v2, v2, 0x8

    invoke-virtual {v1, v2}, Landroid/renderscript/FieldPacker;->reset(I)V

    iget-object v1, p0, Lcom/android/wallpaper/grass/ScriptField_Blade;->mIOBuffer:Landroid/renderscript/FieldPacker;

    invoke-virtual {v1, p2}, Landroid/renderscript/FieldPacker;->addF32(F)V

    new-instance v0, Landroid/renderscript/FieldPacker;

    const/4 v1, 0x4

    invoke-direct {v0, v1}, Landroid/renderscript/FieldPacker;-><init>(I)V

    invoke-virtual {v0, p2}, Landroid/renderscript/FieldPacker;->addF32(F)V

    iget-object v1, p0, Landroid/renderscript/Script$FieldBase;->mAllocation:Landroid/renderscript/Allocation;

    const/4 v2, 0x2

    invoke-virtual {v1, p1, v2, v0}, Landroid/renderscript/Allocation;->setFromFieldPacker(IILandroid/renderscript/FieldPacker;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :cond_3
    monitor-exit p0

    return-void

    :catchall_0
    move-exception v1

    monitor-exit p0

    throw v1
.end method

.method public declared-synchronized set_yPos(IFZ)V
    .locals 3
    .param p1    # I
    .param p2    # F
    .param p3    # Z

    monitor-enter p0

    :try_start_0
    iget-object v1, p0, Lcom/android/wallpaper/grass/ScriptField_Blade;->mIOBuffer:Landroid/renderscript/FieldPacker;

    if-nez v1, :cond_0

    new-instance v1, Landroid/renderscript/FieldPacker;

    invoke-virtual {p0}, Landroid/renderscript/Script$FieldBase;->getType()Landroid/renderscript/Type;

    move-result-object v2

    invoke-virtual {v2}, Landroid/renderscript/Type;->getX()I

    move-result v2

    mul-int/lit8 v2, v2, 0x34

    invoke-direct {v1, v2}, Landroid/renderscript/FieldPacker;-><init>(I)V

    iput-object v1, p0, Lcom/android/wallpaper/grass/ScriptField_Blade;->mIOBuffer:Landroid/renderscript/FieldPacker;

    :cond_0
    iget-object v1, p0, Lcom/android/wallpaper/grass/ScriptField_Blade;->mItemArray:[Lcom/android/wallpaper/grass/ScriptField_Blade$Item;

    if-nez v1, :cond_1

    invoke-virtual {p0}, Landroid/renderscript/Script$FieldBase;->getType()Landroid/renderscript/Type;

    move-result-object v1

    invoke-virtual {v1}, Landroid/renderscript/Type;->getX()I

    move-result v1

    new-array v1, v1, [Lcom/android/wallpaper/grass/ScriptField_Blade$Item;

    iput-object v1, p0, Lcom/android/wallpaper/grass/ScriptField_Blade;->mItemArray:[Lcom/android/wallpaper/grass/ScriptField_Blade$Item;

    :cond_1
    iget-object v1, p0, Lcom/android/wallpaper/grass/ScriptField_Blade;->mItemArray:[Lcom/android/wallpaper/grass/ScriptField_Blade$Item;

    aget-object v1, v1, p1

    if-nez v1, :cond_2

    iget-object v1, p0, Lcom/android/wallpaper/grass/ScriptField_Blade;->mItemArray:[Lcom/android/wallpaper/grass/ScriptField_Blade$Item;

    new-instance v2, Lcom/android/wallpaper/grass/ScriptField_Blade$Item;

    invoke-direct {v2}, Lcom/android/wallpaper/grass/ScriptField_Blade$Item;-><init>()V

    aput-object v2, v1, p1

    :cond_2
    iget-object v1, p0, Lcom/android/wallpaper/grass/ScriptField_Blade;->mItemArray:[Lcom/android/wallpaper/grass/ScriptField_Blade$Item;

    aget-object v1, v1, p1

    iput p2, v1, Lcom/android/wallpaper/grass/ScriptField_Blade$Item;->yPos:F

    if-eqz p3, :cond_3

    iget-object v1, p0, Lcom/android/wallpaper/grass/ScriptField_Blade;->mIOBuffer:Landroid/renderscript/FieldPacker;

    mul-int/lit8 v2, p1, 0x34

    add-int/lit8 v2, v2, 0xc

    invoke-virtual {v1, v2}, Landroid/renderscript/FieldPacker;->reset(I)V

    iget-object v1, p0, Lcom/android/wallpaper/grass/ScriptField_Blade;->mIOBuffer:Landroid/renderscript/FieldPacker;

    invoke-virtual {v1, p2}, Landroid/renderscript/FieldPacker;->addF32(F)V

    new-instance v0, Landroid/renderscript/FieldPacker;

    const/4 v1, 0x4

    invoke-direct {v0, v1}, Landroid/renderscript/FieldPacker;-><init>(I)V

    invoke-virtual {v0, p2}, Landroid/renderscript/FieldPacker;->addF32(F)V

    iget-object v1, p0, Landroid/renderscript/Script$FieldBase;->mAllocation:Landroid/renderscript/Allocation;

    const/4 v2, 0x3

    invoke-virtual {v1, p1, v2, v0}, Landroid/renderscript/Allocation;->setFromFieldPacker(IILandroid/renderscript/FieldPacker;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :cond_3
    monitor-exit p0

    return-void

    :catchall_0
    move-exception v1

    monitor-exit p0

    throw v1
.end method
