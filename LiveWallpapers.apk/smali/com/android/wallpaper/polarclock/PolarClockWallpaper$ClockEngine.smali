.class Lcom/android/wallpaper/polarclock/PolarClockWallpaper$ClockEngine;
.super Landroid/service/wallpaper/WallpaperService$Engine;
.source "PolarClockWallpaper.java"

# interfaces
.implements Landroid/content/SharedPreferences$OnSharedPreferenceChangeListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/wallpaper/polarclock/PolarClockWallpaper;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = "ClockEngine"
.end annotation


# static fields
.field private static final DEFAULT_RING_THICKNESS:F = 24.0f

.field private static final LARGE_GAP:F = 38.0f

.field private static final LARGE_RING_THICKNESS:F = 32.0f

.field private static final MEDIUM_RING_THICKNESS:F = 16.0f

.field private static final SMALL_GAP:F = 14.0f

.field private static final SMALL_RING_THICKNESS:F = 8.0f


# instance fields
.field private mCalendar:Landroid/text/format/Time;

.field private final mDrawClock:Ljava/lang/Runnable;

.field private mOffsetX:F

.field private final mPaint:Landroid/graphics/Paint;

.field private mPalette:Lcom/android/wallpaper/polarclock/PolarClockWallpaper$ClockPalette;

.field private final mPalettes:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Lcom/android/wallpaper/polarclock/PolarClockWallpaper$ClockPalette;",
            ">;"
        }
    .end annotation
.end field

.field private mPrefs:Landroid/content/SharedPreferences;

.field private final mRect:Landroid/graphics/RectF;

.field private mShowSeconds:Z

.field private mVariableLineWidth:Z

.field private mVisible:Z

.field private final mWatcher:Landroid/content/BroadcastReceiver;

.field private mWatcherRegistered:Z

.field final synthetic this$0:Lcom/android/wallpaper/polarclock/PolarClockWallpaper;


# direct methods
.method constructor <init>(Lcom/android/wallpaper/polarclock/PolarClockWallpaper;)V
    .locals 6

    iput-object p1, p0, Lcom/android/wallpaper/polarclock/PolarClockWallpaper$ClockEngine;->this$0:Lcom/android/wallpaper/polarclock/PolarClockWallpaper;

    invoke-direct {p0, p1}, Landroid/service/wallpaper/WallpaperService$Engine;-><init>(Landroid/service/wallpaper/WallpaperService;)V

    new-instance v4, Ljava/util/HashMap;

    invoke-direct {v4}, Ljava/util/HashMap;-><init>()V

    iput-object v4, p0, Lcom/android/wallpaper/polarclock/PolarClockWallpaper$ClockEngine;->mPalettes:Ljava/util/HashMap;

    new-instance v4, Landroid/graphics/Paint;

    invoke-direct {v4}, Landroid/graphics/Paint;-><init>()V

    iput-object v4, p0, Lcom/android/wallpaper/polarclock/PolarClockWallpaper$ClockEngine;->mPaint:Landroid/graphics/Paint;

    new-instance v4, Landroid/graphics/RectF;

    invoke-direct {v4}, Landroid/graphics/RectF;-><init>()V

    iput-object v4, p0, Lcom/android/wallpaper/polarclock/PolarClockWallpaper$ClockEngine;->mRect:Landroid/graphics/RectF;

    new-instance v4, Lcom/android/wallpaper/polarclock/PolarClockWallpaper$ClockEngine$1;

    invoke-direct {v4, p0}, Lcom/android/wallpaper/polarclock/PolarClockWallpaper$ClockEngine$1;-><init>(Lcom/android/wallpaper/polarclock/PolarClockWallpaper$ClockEngine;)V

    iput-object v4, p0, Lcom/android/wallpaper/polarclock/PolarClockWallpaper$ClockEngine;->mWatcher:Landroid/content/BroadcastReceiver;

    new-instance v4, Lcom/android/wallpaper/polarclock/PolarClockWallpaper$ClockEngine$2;

    invoke-direct {v4, p0}, Lcom/android/wallpaper/polarclock/PolarClockWallpaper$ClockEngine$2;-><init>(Lcom/android/wallpaper/polarclock/PolarClockWallpaper$ClockEngine;)V

    iput-object v4, p0, Lcom/android/wallpaper/polarclock/PolarClockWallpaper$ClockEngine;->mDrawClock:Ljava/lang/Runnable;

    invoke-virtual {p1}, Landroid/content/ContextWrapper;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v5, 0x7f040005

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getXml(I)Landroid/content/res/XmlResourceParser;

    move-result-object v3

    :try_start_0
    invoke-interface {v3}, Landroid/content/res/XmlResourceParser;->getEventType()I

    move-result v2

    :goto_0
    const/4 v4, 0x1

    if-eq v2, v4, :cond_1

    const/4 v4, 0x2

    if-ne v2, v4, :cond_0

    const-string v4, "palette"

    invoke-interface {v3}, Landroid/content/res/XmlResourceParser;->getName()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_0

    invoke-static {v3}, Lcom/android/wallpaper/polarclock/PolarClockWallpaper$ClockPalette;->parseXmlPaletteTag(Landroid/content/res/XmlResourceParser;)Lcom/android/wallpaper/polarclock/PolarClockWallpaper$ClockPalette;

    move-result-object v1

    invoke-virtual {v1}, Lcom/android/wallpaper/polarclock/PolarClockWallpaper$ClockPalette;->getId()Ljava/lang/String;

    move-result-object v4

    if-eqz v4, :cond_0

    iget-object v4, p0, Lcom/android/wallpaper/polarclock/PolarClockWallpaper$ClockEngine;->mPalettes:Ljava/util/HashMap;

    invoke-virtual {v1}, Lcom/android/wallpaper/polarclock/PolarClockWallpaper$ClockPalette;->getId()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5, v1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    :cond_0
    invoke-interface {v3}, Landroid/content/res/XmlResourceParser;->next()I
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Lorg/xmlpull/v1/XmlPullParserException; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v2

    goto :goto_0

    :cond_1
    invoke-interface {v3}, Landroid/content/res/XmlResourceParser;->close()V

    :goto_1
    invoke-static {}, Lcom/android/wallpaper/polarclock/PolarClockWallpaper$CyclingClockPalette;->getFallback()Lcom/android/wallpaper/polarclock/PolarClockWallpaper$CyclingClockPalette;

    move-result-object v4

    iput-object v4, p0, Lcom/android/wallpaper/polarclock/PolarClockWallpaper$ClockEngine;->mPalette:Lcom/android/wallpaper/polarclock/PolarClockWallpaper$ClockPalette;

    return-void

    :catch_0
    move-exception v0

    :try_start_1
    const-string v4, "PolarClock"

    const-string v5, "An error occured during wallpaper configuration:"

    invoke-static {v4, v5, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    invoke-interface {v3}, Landroid/content/res/XmlResourceParser;->close()V

    goto :goto_1

    :catch_1
    move-exception v0

    :try_start_2
    const-string v4, "PolarClock"

    const-string v5, "An error occured during wallpaper configuration:"

    invoke-static {v4, v5, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    invoke-interface {v3}, Landroid/content/res/XmlResourceParser;->close()V

    goto :goto_1

    :catchall_0
    move-exception v4

    invoke-interface {v3}, Landroid/content/res/XmlResourceParser;->close()V

    throw v4
.end method

.method static synthetic access$002(Lcom/android/wallpaper/polarclock/PolarClockWallpaper$ClockEngine;Landroid/text/format/Time;)Landroid/text/format/Time;
    .locals 0
    .param p0    # Lcom/android/wallpaper/polarclock/PolarClockWallpaper$ClockEngine;
    .param p1    # Landroid/text/format/Time;

    iput-object p1, p0, Lcom/android/wallpaper/polarclock/PolarClockWallpaper$ClockEngine;->mCalendar:Landroid/text/format/Time;

    return-object p1
.end method


# virtual methods
.method drawFrame()V
    .locals 22

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/wallpaper/polarclock/PolarClockWallpaper$ClockEngine;->mPalette:Lcom/android/wallpaper/polarclock/PolarClockWallpaper$ClockPalette;

    if-nez v4, :cond_1

    const-string v4, "PolarClockWallpaper"

    const-string v5, "no palette?!"

    invoke-static {v4, v5}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    :goto_0
    return-void

    :cond_1
    invoke-virtual/range {p0 .. p0}, Landroid/service/wallpaper/WallpaperService$Engine;->getSurfaceHolder()Landroid/view/SurfaceHolder;

    move-result-object v12

    invoke-interface {v12}, Landroid/view/SurfaceHolder;->getSurfaceFrame()Landroid/graphics/Rect;

    move-result-object v10

    invoke-virtual {v10}, Landroid/graphics/Rect;->width()I

    move-result v19

    invoke-virtual {v10}, Landroid/graphics/Rect;->height()I

    move-result v11

    const/4 v2, 0x0

    :try_start_0
    invoke-interface {v12}, Landroid/view/SurfaceHolder;->lockCanvas()Landroid/graphics/Canvas;

    move-result-object v2

    if-eqz v2, :cond_9

    move-object/from16 v0, p0

    iget-object v9, v0, Lcom/android/wallpaper/polarclock/PolarClockWallpaper$ClockEngine;->mCalendar:Landroid/text/format/Time;

    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/android/wallpaper/polarclock/PolarClockWallpaper$ClockEngine;->mPaint:Landroid/graphics/Paint;

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v14

    invoke-virtual {v9, v14, v15}, Landroid/text/format/Time;->set(J)V

    const/4 v4, 0x0

    invoke-virtual {v9, v4}, Landroid/text/format/Time;->normalize(Z)J

    div-int/lit8 v16, v19, 0x2

    div-int/lit8 v18, v11, 0x2

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/wallpaper/polarclock/PolarClockWallpaper$ClockEngine;->mPalette:Lcom/android/wallpaper/polarclock/PolarClockWallpaper$ClockPalette;

    invoke-virtual {v4}, Lcom/android/wallpaper/polarclock/PolarClockWallpaper$ClockPalette;->getBackgroundColor()I

    move-result v4

    invoke-virtual {v2, v4}, Landroid/graphics/Canvas;->drawColor(I)V

    move/from16 v0, v16

    int-to-float v4, v0

    move/from16 v0, v16

    int-to-float v5, v0

    move/from16 v0, v16

    neg-int v6, v0

    int-to-float v6, v6

    move-object/from16 v0, p0

    iget v0, v0, Lcom/android/wallpaper/polarclock/PolarClockWallpaper$ClockEngine;->mOffsetX:F

    move/from16 v20, v0

    move/from16 v0, v20

    invoke-static {v5, v6, v0}, Landroid/util/MathUtils;->lerp(FFF)F

    move-result v5

    add-float/2addr v4, v5

    move/from16 v0, v18

    int-to-float v5, v0

    invoke-virtual {v2, v4, v5}, Landroid/graphics/Canvas;->translate(FF)V

    const/high16 v4, -0x3d4c0000

    invoke-virtual {v2, v4}, Landroid/graphics/Canvas;->rotate(F)V

    move/from16 v0, v19

    if-ge v11, v0, :cond_2

    const v4, 0x3f666666

    const v5, 0x3f666666

    invoke-virtual {v2, v4, v5}, Landroid/graphics/Canvas;->scale(FF)V

    :cond_2
    move/from16 v0, v19

    invoke-static {v0, v11}, Ljava/lang/Math;->min(II)I

    move-result v4

    int-to-float v4, v4

    const/high16 v5, 0x3f000000

    mul-float/2addr v4, v5

    const/high16 v5, 0x41c00000

    sub-float v17, v4, v5

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/android/wallpaper/polarclock/PolarClockWallpaper$ClockEngine;->mRect:Landroid/graphics/RectF;

    move/from16 v0, v17

    neg-float v4, v0

    move/from16 v0, v17

    neg-float v5, v0

    move/from16 v0, v17

    move/from16 v1, v17

    invoke-virtual {v3, v4, v5, v0, v1}, Landroid/graphics/RectF;->set(FFFF)V

    const/high16 v13, 0x41c00000

    move-object/from16 v0, p0

    iget-boolean v4, v0, Lcom/android/wallpaper/polarclock/PolarClockWallpaper$ClockEngine;->mShowSeconds:Z

    if-eqz v4, :cond_4

    const-wide/32 v4, 0xea60

    rem-long v4, v14, v4

    long-to-float v4, v4

    const v5, 0x476a6000

    div-float v8, v4, v5

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/wallpaper/polarclock/PolarClockWallpaper$ClockEngine;->mPalette:Lcom/android/wallpaper/polarclock/PolarClockWallpaper$ClockPalette;

    invoke-virtual {v4, v8}, Lcom/android/wallpaper/polarclock/PolarClockWallpaper$ClockPalette;->getSecondColor(F)I

    move-result v4

    invoke-virtual {v7, v4}, Landroid/graphics/Paint;->setColor(I)V

    move-object/from16 v0, p0

    iget-boolean v4, v0, Lcom/android/wallpaper/polarclock/PolarClockWallpaper$ClockEngine;->mVariableLineWidth:Z

    if-eqz v4, :cond_3

    const/high16 v13, 0x41000000

    invoke-virtual {v7, v13}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    :cond_3
    const/4 v4, 0x0

    const/high16 v5, 0x43b40000

    mul-float/2addr v5, v8

    const/4 v6, 0x0

    invoke-virtual/range {v2 .. v7}, Landroid/graphics/Canvas;->drawArc(Landroid/graphics/RectF;FFZLandroid/graphics/Paint;)V

    :cond_4
    const/high16 v4, 0x41600000

    add-float/2addr v4, v13

    sub-float v17, v17, v4

    move/from16 v0, v17

    neg-float v4, v0

    move/from16 v0, v17

    neg-float v5, v0

    move/from16 v0, v17

    move/from16 v1, v17

    invoke-virtual {v3, v4, v5, v0, v1}, Landroid/graphics/RectF;->set(FFFF)V

    iget v4, v9, Landroid/text/format/Time;->minute:I

    int-to-float v4, v4

    const/high16 v5, 0x42700000

    mul-float/2addr v4, v5

    iget v5, v9, Landroid/text/format/Time;->second:I

    int-to-float v5, v5

    add-float/2addr v4, v5

    const/high16 v5, 0x45610000

    rem-float/2addr v4, v5

    const/high16 v5, 0x45610000

    div-float v8, v4, v5

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/wallpaper/polarclock/PolarClockWallpaper$ClockEngine;->mPalette:Lcom/android/wallpaper/polarclock/PolarClockWallpaper$ClockPalette;

    invoke-virtual {v4, v8}, Lcom/android/wallpaper/polarclock/PolarClockWallpaper$ClockPalette;->getMinuteColor(F)I

    move-result v4

    invoke-virtual {v7, v4}, Landroid/graphics/Paint;->setColor(I)V

    move-object/from16 v0, p0

    iget-boolean v4, v0, Lcom/android/wallpaper/polarclock/PolarClockWallpaper$ClockEngine;->mVariableLineWidth:Z

    if-eqz v4, :cond_5

    const/high16 v13, 0x41800000

    invoke-virtual {v7, v13}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    :cond_5
    const/4 v4, 0x0

    const/high16 v5, 0x43b40000

    mul-float/2addr v5, v8

    const/4 v6, 0x0

    invoke-virtual/range {v2 .. v7}, Landroid/graphics/Canvas;->drawArc(Landroid/graphics/RectF;FFZLandroid/graphics/Paint;)V

    const/high16 v4, 0x41600000

    add-float/2addr v4, v13

    sub-float v17, v17, v4

    move/from16 v0, v17

    neg-float v4, v0

    move/from16 v0, v17

    neg-float v5, v0

    move/from16 v0, v17

    move/from16 v1, v17

    invoke-virtual {v3, v4, v5, v0, v1}, Landroid/graphics/RectF;->set(FFFF)V

    iget v4, v9, Landroid/text/format/Time;->hour:I

    int-to-float v4, v4

    const/high16 v5, 0x42700000

    mul-float/2addr v4, v5

    iget v5, v9, Landroid/text/format/Time;->minute:I

    int-to-float v5, v5

    add-float/2addr v4, v5

    const/high16 v5, 0x44b40000

    rem-float/2addr v4, v5

    const/high16 v5, 0x44b40000

    div-float v8, v4, v5

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/wallpaper/polarclock/PolarClockWallpaper$ClockEngine;->mPalette:Lcom/android/wallpaper/polarclock/PolarClockWallpaper$ClockPalette;

    invoke-virtual {v4, v8}, Lcom/android/wallpaper/polarclock/PolarClockWallpaper$ClockPalette;->getHourColor(F)I

    move-result v4

    invoke-virtual {v7, v4}, Landroid/graphics/Paint;->setColor(I)V

    move-object/from16 v0, p0

    iget-boolean v4, v0, Lcom/android/wallpaper/polarclock/PolarClockWallpaper$ClockEngine;->mVariableLineWidth:Z

    if-eqz v4, :cond_6

    const/high16 v13, 0x42000000

    invoke-virtual {v7, v13}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    :cond_6
    const/4 v4, 0x0

    const/high16 v5, 0x43b40000

    mul-float/2addr v5, v8

    const/4 v6, 0x0

    invoke-virtual/range {v2 .. v7}, Landroid/graphics/Canvas;->drawArc(Landroid/graphics/RectF;FFZLandroid/graphics/Paint;)V

    const/high16 v4, 0x42180000

    add-float/2addr v4, v13

    sub-float v17, v17, v4

    move/from16 v0, v17

    neg-float v4, v0

    move/from16 v0, v17

    neg-float v5, v0

    move/from16 v0, v17

    move/from16 v1, v17

    invoke-virtual {v3, v4, v5, v0, v1}, Landroid/graphics/RectF;->set(FFFF)V

    iget v4, v9, Landroid/text/format/Time;->monthDay:I

    add-int/lit8 v4, v4, -0x1

    int-to-float v4, v4

    const/4 v5, 0x4

    invoke-virtual {v9, v5}, Landroid/text/format/Time;->getActualMaximum(I)I

    move-result v5

    add-int/lit8 v5, v5, -0x1

    int-to-float v5, v5

    div-float v8, v4, v5

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/wallpaper/polarclock/PolarClockWallpaper$ClockEngine;->mPalette:Lcom/android/wallpaper/polarclock/PolarClockWallpaper$ClockPalette;

    invoke-virtual {v4, v8}, Lcom/android/wallpaper/polarclock/PolarClockWallpaper$ClockPalette;->getDayColor(F)I

    move-result v4

    invoke-virtual {v7, v4}, Landroid/graphics/Paint;->setColor(I)V

    move-object/from16 v0, p0

    iget-boolean v4, v0, Lcom/android/wallpaper/polarclock/PolarClockWallpaper$ClockEngine;->mVariableLineWidth:Z

    if-eqz v4, :cond_7

    const/high16 v13, 0x41800000

    invoke-virtual {v7, v13}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    :cond_7
    const/4 v4, 0x0

    const/high16 v5, 0x43b40000

    mul-float/2addr v5, v8

    const/4 v6, 0x0

    invoke-virtual/range {v2 .. v7}, Landroid/graphics/Canvas;->drawArc(Landroid/graphics/RectF;FFZLandroid/graphics/Paint;)V

    const/high16 v4, 0x41600000

    add-float/2addr v4, v13

    sub-float v17, v17, v4

    move/from16 v0, v17

    neg-float v4, v0

    move/from16 v0, v17

    neg-float v5, v0

    move/from16 v0, v17

    move/from16 v1, v17

    invoke-virtual {v3, v4, v5, v0, v1}, Landroid/graphics/RectF;->set(FFFF)V

    iget v4, v9, Landroid/text/format/Time;->month:I

    int-to-float v4, v4

    const/high16 v5, 0x41300000

    div-float v8, v4, v5

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/wallpaper/polarclock/PolarClockWallpaper$ClockEngine;->mPalette:Lcom/android/wallpaper/polarclock/PolarClockWallpaper$ClockPalette;

    invoke-virtual {v4, v8}, Lcom/android/wallpaper/polarclock/PolarClockWallpaper$ClockPalette;->getMonthColor(F)I

    move-result v4

    invoke-virtual {v7, v4}, Landroid/graphics/Paint;->setColor(I)V

    move-object/from16 v0, p0

    iget-boolean v4, v0, Lcom/android/wallpaper/polarclock/PolarClockWallpaper$ClockEngine;->mVariableLineWidth:Z

    if-eqz v4, :cond_8

    const/high16 v13, 0x42000000

    invoke-virtual {v7, v13}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    :cond_8
    const/4 v4, 0x0

    const/high16 v5, 0x43b40000

    mul-float/2addr v5, v8

    const/4 v6, 0x0

    invoke-virtual/range {v2 .. v7}, Landroid/graphics/Canvas;->drawArc(Landroid/graphics/RectF;FFZLandroid/graphics/Paint;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :cond_9
    if-eqz v2, :cond_a

    invoke-interface {v12, v2}, Landroid/view/SurfaceHolder;->unlockCanvasAndPost(Landroid/graphics/Canvas;)V

    :cond_a
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/wallpaper/polarclock/PolarClockWallpaper$ClockEngine;->this$0:Lcom/android/wallpaper/polarclock/PolarClockWallpaper;

    invoke-static {v4}, Lcom/android/wallpaper/polarclock/PolarClockWallpaper;->access$100(Lcom/android/wallpaper/polarclock/PolarClockWallpaper;)Landroid/os/Handler;

    move-result-object v4

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/android/wallpaper/polarclock/PolarClockWallpaper$ClockEngine;->mDrawClock:Ljava/lang/Runnable;

    invoke-virtual {v4, v5}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    move-object/from16 v0, p0

    iget-boolean v4, v0, Lcom/android/wallpaper/polarclock/PolarClockWallpaper$ClockEngine;->mVisible:Z

    if-eqz v4, :cond_0

    move-object/from16 v0, p0

    iget-boolean v4, v0, Lcom/android/wallpaper/polarclock/PolarClockWallpaper$ClockEngine;->mShowSeconds:Z

    if-eqz v4, :cond_c

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/wallpaper/polarclock/PolarClockWallpaper$ClockEngine;->this$0:Lcom/android/wallpaper/polarclock/PolarClockWallpaper;

    invoke-static {v4}, Lcom/android/wallpaper/polarclock/PolarClockWallpaper;->access$100(Lcom/android/wallpaper/polarclock/PolarClockWallpaper;)Landroid/os/Handler;

    move-result-object v4

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/android/wallpaper/polarclock/PolarClockWallpaper$ClockEngine;->mDrawClock:Ljava/lang/Runnable;

    const-wide/16 v20, 0x28

    move-wide/from16 v0, v20

    invoke-virtual {v4, v5, v0, v1}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    goto/16 :goto_0

    :catchall_0
    move-exception v4

    if-eqz v2, :cond_b

    invoke-interface {v12, v2}, Landroid/view/SurfaceHolder;->unlockCanvasAndPost(Landroid/graphics/Canvas;)V

    :cond_b
    throw v4

    :cond_c
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/wallpaper/polarclock/PolarClockWallpaper$ClockEngine;->this$0:Lcom/android/wallpaper/polarclock/PolarClockWallpaper;

    invoke-static {v4}, Lcom/android/wallpaper/polarclock/PolarClockWallpaper;->access$100(Lcom/android/wallpaper/polarclock/PolarClockWallpaper;)Landroid/os/Handler;

    move-result-object v4

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/android/wallpaper/polarclock/PolarClockWallpaper$ClockEngine;->mDrawClock:Ljava/lang/Runnable;

    const-wide/16 v20, 0x7d0

    move-wide/from16 v0, v20

    invoke-virtual {v4, v5, v0, v1}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    goto/16 :goto_0
.end method

.method public onCreate(Landroid/view/SurfaceHolder;)V
    .locals 4
    .param p1    # Landroid/view/SurfaceHolder;

    invoke-super {p0, p1}, Landroid/service/wallpaper/WallpaperService$Engine;->onCreate(Landroid/view/SurfaceHolder;)V

    iget-object v1, p0, Lcom/android/wallpaper/polarclock/PolarClockWallpaper$ClockEngine;->this$0:Lcom/android/wallpaper/polarclock/PolarClockWallpaper;

    const-string v2, "polar_clock_settings"

    const/4 v3, 0x0

    invoke-virtual {v1, v2, v3}, Landroid/content/ContextWrapper;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v1

    iput-object v1, p0, Lcom/android/wallpaper/polarclock/PolarClockWallpaper$ClockEngine;->mPrefs:Landroid/content/SharedPreferences;

    iget-object v1, p0, Lcom/android/wallpaper/polarclock/PolarClockWallpaper$ClockEngine;->mPrefs:Landroid/content/SharedPreferences;

    invoke-interface {v1, p0}, Landroid/content/SharedPreferences;->registerOnSharedPreferenceChangeListener(Landroid/content/SharedPreferences$OnSharedPreferenceChangeListener;)V

    iget-object v1, p0, Lcom/android/wallpaper/polarclock/PolarClockWallpaper$ClockEngine;->mPrefs:Landroid/content/SharedPreferences;

    const/4 v2, 0x0

    invoke-virtual {p0, v1, v2}, Lcom/android/wallpaper/polarclock/PolarClockWallpaper$ClockEngine;->onSharedPreferenceChanged(Landroid/content/SharedPreferences;Ljava/lang/String;)V

    new-instance v1, Landroid/text/format/Time;

    invoke-direct {v1}, Landroid/text/format/Time;-><init>()V

    iput-object v1, p0, Lcom/android/wallpaper/polarclock/PolarClockWallpaper$ClockEngine;->mCalendar:Landroid/text/format/Time;

    iget-object v1, p0, Lcom/android/wallpaper/polarclock/PolarClockWallpaper$ClockEngine;->mCalendar:Landroid/text/format/Time;

    invoke-virtual {v1}, Landroid/text/format/Time;->setToNow()V

    iget-object v0, p0, Lcom/android/wallpaper/polarclock/PolarClockWallpaper$ClockEngine;->mPaint:Landroid/graphics/Paint;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setAntiAlias(Z)V

    const/high16 v1, 0x41c00000

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    sget-object v1, Landroid/graphics/Paint$Cap;->ROUND:Landroid/graphics/Paint$Cap;

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setStrokeCap(Landroid/graphics/Paint$Cap;)V

    sget-object v1, Landroid/graphics/Paint$Style;->STROKE:Landroid/graphics/Paint$Style;

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    invoke-virtual {p0}, Landroid/service/wallpaper/WallpaperService$Engine;->isPreview()Z

    move-result v1

    if-eqz v1, :cond_0

    const/high16 v1, 0x3f000000

    iput v1, p0, Lcom/android/wallpaper/polarclock/PolarClockWallpaper$ClockEngine;->mOffsetX:F

    :cond_0
    return-void
.end method

.method public onDestroy()V
    .locals 2

    invoke-super {p0}, Landroid/service/wallpaper/WallpaperService$Engine;->onDestroy()V

    iget-boolean v0, p0, Lcom/android/wallpaper/polarclock/PolarClockWallpaper$ClockEngine;->mWatcherRegistered:Z

    if-eqz v0, :cond_0

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/android/wallpaper/polarclock/PolarClockWallpaper$ClockEngine;->mWatcherRegistered:Z

    iget-object v0, p0, Lcom/android/wallpaper/polarclock/PolarClockWallpaper$ClockEngine;->this$0:Lcom/android/wallpaper/polarclock/PolarClockWallpaper;

    iget-object v1, p0, Lcom/android/wallpaper/polarclock/PolarClockWallpaper$ClockEngine;->mWatcher:Landroid/content/BroadcastReceiver;

    invoke-virtual {v0, v1}, Landroid/content/ContextWrapper;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    :cond_0
    iget-object v0, p0, Lcom/android/wallpaper/polarclock/PolarClockWallpaper$ClockEngine;->this$0:Lcom/android/wallpaper/polarclock/PolarClockWallpaper;

    invoke-static {v0}, Lcom/android/wallpaper/polarclock/PolarClockWallpaper;->access$100(Lcom/android/wallpaper/polarclock/PolarClockWallpaper;)Landroid/os/Handler;

    move-result-object v0

    iget-object v1, p0, Lcom/android/wallpaper/polarclock/PolarClockWallpaper$ClockEngine;->mDrawClock:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    return-void
.end method

.method public onOffsetsChanged(FFFFII)V
    .locals 1
    .param p1    # F
    .param p2    # F
    .param p3    # F
    .param p4    # F
    .param p5    # I
    .param p6    # I

    invoke-virtual {p0}, Landroid/service/wallpaper/WallpaperService$Engine;->isPreview()Z

    move-result v0

    if-eqz v0, :cond_0

    :goto_0
    return-void

    :cond_0
    iput p1, p0, Lcom/android/wallpaper/polarclock/PolarClockWallpaper$ClockEngine;->mOffsetX:F

    invoke-virtual {p0}, Lcom/android/wallpaper/polarclock/PolarClockWallpaper$ClockEngine;->drawFrame()V

    goto :goto_0
.end method

.method public onSharedPreferenceChanged(Landroid/content/SharedPreferences;Ljava/lang/String;)V
    .locals 5
    .param p1    # Landroid/content/SharedPreferences;
    .param p2    # Ljava/lang/String;

    const/4 v4, 0x1

    const/4 v0, 0x0

    if-eqz p2, :cond_0

    const-string v3, "show_seconds"

    invoke-virtual {v3, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_1

    :cond_0
    const-string v3, "show_seconds"

    invoke-interface {p1, v3, v4}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v3

    iput-boolean v3, p0, Lcom/android/wallpaper/polarclock/PolarClockWallpaper$ClockEngine;->mShowSeconds:Z

    const/4 v0, 0x1

    :cond_1
    if-eqz p2, :cond_2

    const-string v3, "variable_line_width"

    invoke-virtual {v3, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    :cond_2
    const-string v3, "variable_line_width"

    invoke-interface {p1, v3, v4}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v3

    iput-boolean v3, p0, Lcom/android/wallpaper/polarclock/PolarClockWallpaper$ClockEngine;->mVariableLineWidth:Z

    const/4 v0, 0x1

    :cond_3
    if-eqz p2, :cond_4

    const-string v3, "palette"

    invoke-virtual {v3, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_5

    :cond_4
    const-string v3, "palette"

    const-string v4, ""

    invoke-interface {p1, v3, v4}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, Lcom/android/wallpaper/polarclock/PolarClockWallpaper$ClockEngine;->mPalettes:Ljava/util/HashMap;

    invoke-virtual {v3, v2}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/android/wallpaper/polarclock/PolarClockWallpaper$ClockPalette;

    if-eqz v1, :cond_5

    iput-object v1, p0, Lcom/android/wallpaper/polarclock/PolarClockWallpaper$ClockEngine;->mPalette:Lcom/android/wallpaper/polarclock/PolarClockWallpaper$ClockPalette;

    const/4 v0, 0x1

    :cond_5
    iget-boolean v3, p0, Lcom/android/wallpaper/polarclock/PolarClockWallpaper$ClockEngine;->mVisible:Z

    if-eqz v3, :cond_6

    if-eqz v0, :cond_6

    invoke-virtual {p0}, Lcom/android/wallpaper/polarclock/PolarClockWallpaper$ClockEngine;->drawFrame()V

    :cond_6
    return-void
.end method

.method public onSurfaceChanged(Landroid/view/SurfaceHolder;III)V
    .locals 0
    .param p1    # Landroid/view/SurfaceHolder;
    .param p2    # I
    .param p3    # I
    .param p4    # I

    invoke-super {p0, p1, p2, p3, p4}, Landroid/service/wallpaper/WallpaperService$Engine;->onSurfaceChanged(Landroid/view/SurfaceHolder;III)V

    invoke-virtual {p0}, Lcom/android/wallpaper/polarclock/PolarClockWallpaper$ClockEngine;->drawFrame()V

    return-void
.end method

.method public onSurfaceCreated(Landroid/view/SurfaceHolder;)V
    .locals 0
    .param p1    # Landroid/view/SurfaceHolder;

    invoke-super {p0, p1}, Landroid/service/wallpaper/WallpaperService$Engine;->onSurfaceCreated(Landroid/view/SurfaceHolder;)V

    return-void
.end method

.method public onSurfaceDestroyed(Landroid/view/SurfaceHolder;)V
    .locals 2
    .param p1    # Landroid/view/SurfaceHolder;

    invoke-super {p0, p1}, Landroid/service/wallpaper/WallpaperService$Engine;->onSurfaceDestroyed(Landroid/view/SurfaceHolder;)V

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/android/wallpaper/polarclock/PolarClockWallpaper$ClockEngine;->mVisible:Z

    iget-object v0, p0, Lcom/android/wallpaper/polarclock/PolarClockWallpaper$ClockEngine;->this$0:Lcom/android/wallpaper/polarclock/PolarClockWallpaper;

    invoke-static {v0}, Lcom/android/wallpaper/polarclock/PolarClockWallpaper;->access$100(Lcom/android/wallpaper/polarclock/PolarClockWallpaper;)Landroid/os/Handler;

    move-result-object v0

    iget-object v1, p0, Lcom/android/wallpaper/polarclock/PolarClockWallpaper$ClockEngine;->mDrawClock:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    return-void
.end method

.method public onVisibilityChanged(Z)V
    .locals 5
    .param p1    # Z

    iput-boolean p1, p0, Lcom/android/wallpaper/polarclock/PolarClockWallpaper$ClockEngine;->mVisible:Z

    if-eqz p1, :cond_1

    iget-boolean v0, p0, Lcom/android/wallpaper/polarclock/PolarClockWallpaper$ClockEngine;->mWatcherRegistered:Z

    if-nez v0, :cond_0

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/android/wallpaper/polarclock/PolarClockWallpaper$ClockEngine;->mWatcherRegistered:Z

    iget-object v0, p0, Lcom/android/wallpaper/polarclock/PolarClockWallpaper$ClockEngine;->this$0:Lcom/android/wallpaper/polarclock/PolarClockWallpaper;

    iget-object v1, p0, Lcom/android/wallpaper/polarclock/PolarClockWallpaper$ClockEngine;->mWatcher:Landroid/content/BroadcastReceiver;

    iget-object v2, p0, Lcom/android/wallpaper/polarclock/PolarClockWallpaper$ClockEngine;->this$0:Lcom/android/wallpaper/polarclock/PolarClockWallpaper;

    invoke-static {v2}, Lcom/android/wallpaper/polarclock/PolarClockWallpaper;->access$200(Lcom/android/wallpaper/polarclock/PolarClockWallpaper;)Landroid/content/IntentFilter;

    move-result-object v2

    const/4 v3, 0x0

    iget-object v4, p0, Lcom/android/wallpaper/polarclock/PolarClockWallpaper$ClockEngine;->this$0:Lcom/android/wallpaper/polarclock/PolarClockWallpaper;

    invoke-static {v4}, Lcom/android/wallpaper/polarclock/PolarClockWallpaper;->access$100(Lcom/android/wallpaper/polarclock/PolarClockWallpaper;)Landroid/os/Handler;

    move-result-object v4

    invoke-virtual {v0, v1, v2, v3, v4}, Landroid/content/ContextWrapper;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;Ljava/lang/String;Landroid/os/Handler;)Landroid/content/Intent;

    :cond_0
    new-instance v0, Landroid/text/format/Time;

    invoke-direct {v0}, Landroid/text/format/Time;-><init>()V

    iput-object v0, p0, Lcom/android/wallpaper/polarclock/PolarClockWallpaper$ClockEngine;->mCalendar:Landroid/text/format/Time;

    iget-object v0, p0, Lcom/android/wallpaper/polarclock/PolarClockWallpaper$ClockEngine;->mCalendar:Landroid/text/format/Time;

    invoke-virtual {v0}, Landroid/text/format/Time;->setToNow()V

    :goto_0
    invoke-virtual {p0}, Lcom/android/wallpaper/polarclock/PolarClockWallpaper$ClockEngine;->drawFrame()V

    return-void

    :cond_1
    iget-boolean v0, p0, Lcom/android/wallpaper/polarclock/PolarClockWallpaper$ClockEngine;->mWatcherRegistered:Z

    if-eqz v0, :cond_2

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/android/wallpaper/polarclock/PolarClockWallpaper$ClockEngine;->mWatcherRegistered:Z

    iget-object v0, p0, Lcom/android/wallpaper/polarclock/PolarClockWallpaper$ClockEngine;->this$0:Lcom/android/wallpaper/polarclock/PolarClockWallpaper;

    iget-object v1, p0, Lcom/android/wallpaper/polarclock/PolarClockWallpaper$ClockEngine;->mWatcher:Landroid/content/BroadcastReceiver;

    invoke-virtual {v0, v1}, Landroid/content/ContextWrapper;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    :cond_2
    iget-object v0, p0, Lcom/android/wallpaper/polarclock/PolarClockWallpaper$ClockEngine;->this$0:Lcom/android/wallpaper/polarclock/PolarClockWallpaper;

    invoke-static {v0}, Lcom/android/wallpaper/polarclock/PolarClockWallpaper;->access$100(Lcom/android/wallpaper/polarclock/PolarClockWallpaper;)Landroid/os/Handler;

    move-result-object v0

    iget-object v1, p0, Lcom/android/wallpaper/polarclock/PolarClockWallpaper$ClockEngine;->mDrawClock:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    goto :goto_0
.end method
