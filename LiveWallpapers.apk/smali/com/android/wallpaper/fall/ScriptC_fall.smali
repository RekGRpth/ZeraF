.class public Lcom/android/wallpaper/fall/ScriptC_fall;
.super Landroid/renderscript/ScriptC;
.source "ScriptC_fall.java"


# static fields
.field private static final __rs_resource_name:Ljava/lang/String; = "fall"

.field private static final mExportFuncIdx_addDrop:I = 0x1

.field private static final mExportFuncIdx_initLeaves:I = 0x0

.field private static final mExportVarIdx_g_Constants:I = 0xe

.field private static final mExportVarIdx_g_PFBackground:I = 0xa

.field private static final mExportVarIdx_g_PFSBackground:I = 0xf

.field private static final mExportVarIdx_g_PFSLeaf:I = 0x9

.field private static final mExportVarIdx_g_PFSky:I = 0x8

.field private static final mExportVarIdx_g_PVSky:I = 0x7

.field private static final mExportVarIdx_g_PVWater:I = 0x6

.field private static final mExportVarIdx_g_TLeaves:I = 0xb

.field private static final mExportVarIdx_g_TRiverbed:I = 0xc

.field private static final mExportVarIdx_g_WaterMesh:I = 0xd

.field private static final mExportVarIdx_g_glHeight:I = 0x1

.field private static final mExportVarIdx_g_glWidth:I = 0x0

.field private static final mExportVarIdx_g_meshHeight:I = 0x3

.field private static final mExportVarIdx_g_meshWidth:I = 0x2

.field private static final mExportVarIdx_g_rotate:I = 0x5

.field private static final mExportVarIdx_g_xOffset:I = 0x4


# instance fields
.field private __ALLOCATION:Landroid/renderscript/Element;

.field private __F32:Landroid/renderscript/Element;

.field private __MESH:Landroid/renderscript/Element;

.field private __PROGRAM_FRAGMENT:Landroid/renderscript/Element;

.field private __PROGRAM_STORE:Landroid/renderscript/Element;

.field private __PROGRAM_VERTEX:Landroid/renderscript/Element;

.field private __rs_fp_ALLOCATION:Landroid/renderscript/FieldPacker;

.field private __rs_fp_F32:Landroid/renderscript/FieldPacker;

.field private __rs_fp_MESH:Landroid/renderscript/FieldPacker;

.field private __rs_fp_PROGRAM_FRAGMENT:Landroid/renderscript/FieldPacker;

.field private __rs_fp_PROGRAM_STORE:Landroid/renderscript/FieldPacker;

.field private __rs_fp_PROGRAM_VERTEX:Landroid/renderscript/FieldPacker;

.field private mExportVar_g_Constants:Lcom/android/wallpaper/fall/ScriptField_Constants;

.field private mExportVar_g_PFBackground:Landroid/renderscript/ProgramFragment;

.field private mExportVar_g_PFSBackground:Landroid/renderscript/ProgramStore;

.field private mExportVar_g_PFSLeaf:Landroid/renderscript/ProgramStore;

.field private mExportVar_g_PFSky:Landroid/renderscript/ProgramFragment;

.field private mExportVar_g_PVSky:Landroid/renderscript/ProgramVertex;

.field private mExportVar_g_PVWater:Landroid/renderscript/ProgramVertex;

.field private mExportVar_g_TLeaves:Landroid/renderscript/Allocation;

.field private mExportVar_g_TRiverbed:Landroid/renderscript/Allocation;

.field private mExportVar_g_WaterMesh:Landroid/renderscript/Mesh;

.field private mExportVar_g_glHeight:F

.field private mExportVar_g_glWidth:F

.field private mExportVar_g_meshHeight:F

.field private mExportVar_g_meshWidth:F

.field private mExportVar_g_rotate:F

.field private mExportVar_g_xOffset:F


# direct methods
.method public constructor <init>(Landroid/renderscript/RenderScript;)V
    .locals 5
    .param p1    # Landroid/renderscript/RenderScript;

    invoke-virtual {p1}, Landroid/renderscript/RenderScript;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {p1}, Landroid/renderscript/RenderScript;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const-string v2, "fall"

    const-string v3, "raw"

    invoke-virtual {p1}, Landroid/renderscript/RenderScript;->getApplicationContext()Landroid/content/Context;

    move-result-object v4

    invoke-virtual {v4}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v1, v2, v3, v4}, Landroid/content/res/Resources;->getIdentifier(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)I

    move-result v1

    invoke-direct {p0, p1, v0, v1}, Lcom/android/wallpaper/fall/ScriptC_fall;-><init>(Landroid/renderscript/RenderScript;Landroid/content/res/Resources;I)V

    return-void
.end method

.method public constructor <init>(Landroid/renderscript/RenderScript;Landroid/content/res/Resources;I)V
    .locals 1
    .param p1    # Landroid/renderscript/RenderScript;
    .param p2    # Landroid/content/res/Resources;
    .param p3    # I

    invoke-direct {p0, p1, p2, p3}, Landroid/renderscript/ScriptC;-><init>(Landroid/renderscript/RenderScript;Landroid/content/res/Resources;I)V

    invoke-static {p1}, Landroid/renderscript/Element;->F32(Landroid/renderscript/RenderScript;)Landroid/renderscript/Element;

    move-result-object v0

    iput-object v0, p0, Lcom/android/wallpaper/fall/ScriptC_fall;->__F32:Landroid/renderscript/Element;

    invoke-static {p1}, Landroid/renderscript/Element;->PROGRAM_VERTEX(Landroid/renderscript/RenderScript;)Landroid/renderscript/Element;

    move-result-object v0

    iput-object v0, p0, Lcom/android/wallpaper/fall/ScriptC_fall;->__PROGRAM_VERTEX:Landroid/renderscript/Element;

    invoke-static {p1}, Landroid/renderscript/Element;->PROGRAM_FRAGMENT(Landroid/renderscript/RenderScript;)Landroid/renderscript/Element;

    move-result-object v0

    iput-object v0, p0, Lcom/android/wallpaper/fall/ScriptC_fall;->__PROGRAM_FRAGMENT:Landroid/renderscript/Element;

    invoke-static {p1}, Landroid/renderscript/Element;->PROGRAM_STORE(Landroid/renderscript/RenderScript;)Landroid/renderscript/Element;

    move-result-object v0

    iput-object v0, p0, Lcom/android/wallpaper/fall/ScriptC_fall;->__PROGRAM_STORE:Landroid/renderscript/Element;

    invoke-static {p1}, Landroid/renderscript/Element;->ALLOCATION(Landroid/renderscript/RenderScript;)Landroid/renderscript/Element;

    move-result-object v0

    iput-object v0, p0, Lcom/android/wallpaper/fall/ScriptC_fall;->__ALLOCATION:Landroid/renderscript/Element;

    invoke-static {p1}, Landroid/renderscript/Element;->MESH(Landroid/renderscript/RenderScript;)Landroid/renderscript/Element;

    move-result-object v0

    iput-object v0, p0, Lcom/android/wallpaper/fall/ScriptC_fall;->__MESH:Landroid/renderscript/Element;

    return-void
.end method


# virtual methods
.method public bind_g_Constants(Lcom/android/wallpaper/fall/ScriptField_Constants;)V
    .locals 2
    .param p1    # Lcom/android/wallpaper/fall/ScriptField_Constants;

    const/16 v1, 0xe

    iput-object p1, p0, Lcom/android/wallpaper/fall/ScriptC_fall;->mExportVar_g_Constants:Lcom/android/wallpaper/fall/ScriptField_Constants;

    if-nez p1, :cond_0

    const/4 v0, 0x0

    invoke-virtual {p0, v0, v1}, Landroid/renderscript/Script;->bindAllocation(Landroid/renderscript/Allocation;I)V

    :goto_0
    return-void

    :cond_0
    invoke-virtual {p1}, Landroid/renderscript/Script$FieldBase;->getAllocation()Landroid/renderscript/Allocation;

    move-result-object v0

    invoke-virtual {p0, v0, v1}, Landroid/renderscript/Script;->bindAllocation(Landroid/renderscript/Allocation;I)V

    goto :goto_0
.end method

.method public getFieldID_g_PFBackground()Landroid/renderscript/Script$FieldID;
    .locals 2

    const/16 v0, 0xa

    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, Landroid/renderscript/Script;->createFieldID(ILandroid/renderscript/Element;)Landroid/renderscript/Script$FieldID;

    move-result-object v0

    return-object v0
.end method

.method public getFieldID_g_PFSBackground()Landroid/renderscript/Script$FieldID;
    .locals 2

    const/16 v0, 0xf

    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, Landroid/renderscript/Script;->createFieldID(ILandroid/renderscript/Element;)Landroid/renderscript/Script$FieldID;

    move-result-object v0

    return-object v0
.end method

.method public getFieldID_g_PFSLeaf()Landroid/renderscript/Script$FieldID;
    .locals 2

    const/16 v0, 0x9

    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, Landroid/renderscript/Script;->createFieldID(ILandroid/renderscript/Element;)Landroid/renderscript/Script$FieldID;

    move-result-object v0

    return-object v0
.end method

.method public getFieldID_g_PFSky()Landroid/renderscript/Script$FieldID;
    .locals 2

    const/16 v0, 0x8

    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, Landroid/renderscript/Script;->createFieldID(ILandroid/renderscript/Element;)Landroid/renderscript/Script$FieldID;

    move-result-object v0

    return-object v0
.end method

.method public getFieldID_g_PVSky()Landroid/renderscript/Script$FieldID;
    .locals 2

    const/4 v0, 0x7

    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, Landroid/renderscript/Script;->createFieldID(ILandroid/renderscript/Element;)Landroid/renderscript/Script$FieldID;

    move-result-object v0

    return-object v0
.end method

.method public getFieldID_g_PVWater()Landroid/renderscript/Script$FieldID;
    .locals 2

    const/4 v0, 0x6

    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, Landroid/renderscript/Script;->createFieldID(ILandroid/renderscript/Element;)Landroid/renderscript/Script$FieldID;

    move-result-object v0

    return-object v0
.end method

.method public getFieldID_g_TLeaves()Landroid/renderscript/Script$FieldID;
    .locals 2

    const/16 v0, 0xb

    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, Landroid/renderscript/Script;->createFieldID(ILandroid/renderscript/Element;)Landroid/renderscript/Script$FieldID;

    move-result-object v0

    return-object v0
.end method

.method public getFieldID_g_TRiverbed()Landroid/renderscript/Script$FieldID;
    .locals 2

    const/16 v0, 0xc

    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, Landroid/renderscript/Script;->createFieldID(ILandroid/renderscript/Element;)Landroid/renderscript/Script$FieldID;

    move-result-object v0

    return-object v0
.end method

.method public getFieldID_g_WaterMesh()Landroid/renderscript/Script$FieldID;
    .locals 2

    const/16 v0, 0xd

    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, Landroid/renderscript/Script;->createFieldID(ILandroid/renderscript/Element;)Landroid/renderscript/Script$FieldID;

    move-result-object v0

    return-object v0
.end method

.method public getFieldID_g_glHeight()Landroid/renderscript/Script$FieldID;
    .locals 2

    const/4 v0, 0x1

    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, Landroid/renderscript/Script;->createFieldID(ILandroid/renderscript/Element;)Landroid/renderscript/Script$FieldID;

    move-result-object v0

    return-object v0
.end method

.method public getFieldID_g_glWidth()Landroid/renderscript/Script$FieldID;
    .locals 2

    const/4 v0, 0x0

    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, Landroid/renderscript/Script;->createFieldID(ILandroid/renderscript/Element;)Landroid/renderscript/Script$FieldID;

    move-result-object v0

    return-object v0
.end method

.method public getFieldID_g_meshHeight()Landroid/renderscript/Script$FieldID;
    .locals 2

    const/4 v0, 0x3

    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, Landroid/renderscript/Script;->createFieldID(ILandroid/renderscript/Element;)Landroid/renderscript/Script$FieldID;

    move-result-object v0

    return-object v0
.end method

.method public getFieldID_g_meshWidth()Landroid/renderscript/Script$FieldID;
    .locals 2

    const/4 v0, 0x2

    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, Landroid/renderscript/Script;->createFieldID(ILandroid/renderscript/Element;)Landroid/renderscript/Script$FieldID;

    move-result-object v0

    return-object v0
.end method

.method public getFieldID_g_rotate()Landroid/renderscript/Script$FieldID;
    .locals 2

    const/4 v0, 0x5

    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, Landroid/renderscript/Script;->createFieldID(ILandroid/renderscript/Element;)Landroid/renderscript/Script$FieldID;

    move-result-object v0

    return-object v0
.end method

.method public getFieldID_g_xOffset()Landroid/renderscript/Script$FieldID;
    .locals 2

    const/4 v0, 0x4

    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, Landroid/renderscript/Script;->createFieldID(ILandroid/renderscript/Element;)Landroid/renderscript/Script$FieldID;

    move-result-object v0

    return-object v0
.end method

.method public get_g_Constants()Lcom/android/wallpaper/fall/ScriptField_Constants;
    .locals 1

    iget-object v0, p0, Lcom/android/wallpaper/fall/ScriptC_fall;->mExportVar_g_Constants:Lcom/android/wallpaper/fall/ScriptField_Constants;

    return-object v0
.end method

.method public get_g_PFBackground()Landroid/renderscript/ProgramFragment;
    .locals 1

    iget-object v0, p0, Lcom/android/wallpaper/fall/ScriptC_fall;->mExportVar_g_PFBackground:Landroid/renderscript/ProgramFragment;

    return-object v0
.end method

.method public get_g_PFSBackground()Landroid/renderscript/ProgramStore;
    .locals 1

    iget-object v0, p0, Lcom/android/wallpaper/fall/ScriptC_fall;->mExportVar_g_PFSBackground:Landroid/renderscript/ProgramStore;

    return-object v0
.end method

.method public get_g_PFSLeaf()Landroid/renderscript/ProgramStore;
    .locals 1

    iget-object v0, p0, Lcom/android/wallpaper/fall/ScriptC_fall;->mExportVar_g_PFSLeaf:Landroid/renderscript/ProgramStore;

    return-object v0
.end method

.method public get_g_PFSky()Landroid/renderscript/ProgramFragment;
    .locals 1

    iget-object v0, p0, Lcom/android/wallpaper/fall/ScriptC_fall;->mExportVar_g_PFSky:Landroid/renderscript/ProgramFragment;

    return-object v0
.end method

.method public get_g_PVSky()Landroid/renderscript/ProgramVertex;
    .locals 1

    iget-object v0, p0, Lcom/android/wallpaper/fall/ScriptC_fall;->mExportVar_g_PVSky:Landroid/renderscript/ProgramVertex;

    return-object v0
.end method

.method public get_g_PVWater()Landroid/renderscript/ProgramVertex;
    .locals 1

    iget-object v0, p0, Lcom/android/wallpaper/fall/ScriptC_fall;->mExportVar_g_PVWater:Landroid/renderscript/ProgramVertex;

    return-object v0
.end method

.method public get_g_TLeaves()Landroid/renderscript/Allocation;
    .locals 1

    iget-object v0, p0, Lcom/android/wallpaper/fall/ScriptC_fall;->mExportVar_g_TLeaves:Landroid/renderscript/Allocation;

    return-object v0
.end method

.method public get_g_TRiverbed()Landroid/renderscript/Allocation;
    .locals 1

    iget-object v0, p0, Lcom/android/wallpaper/fall/ScriptC_fall;->mExportVar_g_TRiverbed:Landroid/renderscript/Allocation;

    return-object v0
.end method

.method public get_g_WaterMesh()Landroid/renderscript/Mesh;
    .locals 1

    iget-object v0, p0, Lcom/android/wallpaper/fall/ScriptC_fall;->mExportVar_g_WaterMesh:Landroid/renderscript/Mesh;

    return-object v0
.end method

.method public get_g_glHeight()F
    .locals 1

    iget v0, p0, Lcom/android/wallpaper/fall/ScriptC_fall;->mExportVar_g_glHeight:F

    return v0
.end method

.method public get_g_glWidth()F
    .locals 1

    iget v0, p0, Lcom/android/wallpaper/fall/ScriptC_fall;->mExportVar_g_glWidth:F

    return v0
.end method

.method public get_g_meshHeight()F
    .locals 1

    iget v0, p0, Lcom/android/wallpaper/fall/ScriptC_fall;->mExportVar_g_meshHeight:F

    return v0
.end method

.method public get_g_meshWidth()F
    .locals 1

    iget v0, p0, Lcom/android/wallpaper/fall/ScriptC_fall;->mExportVar_g_meshWidth:F

    return v0
.end method

.method public get_g_rotate()F
    .locals 1

    iget v0, p0, Lcom/android/wallpaper/fall/ScriptC_fall;->mExportVar_g_rotate:F

    return v0
.end method

.method public get_g_xOffset()F
    .locals 1

    iget v0, p0, Lcom/android/wallpaper/fall/ScriptC_fall;->mExportVar_g_xOffset:F

    return v0
.end method

.method public invoke_addDrop(II)V
    .locals 2
    .param p1    # I
    .param p2    # I

    new-instance v0, Landroid/renderscript/FieldPacker;

    const/16 v1, 0x8

    invoke-direct {v0, v1}, Landroid/renderscript/FieldPacker;-><init>(I)V

    invoke-virtual {v0, p1}, Landroid/renderscript/FieldPacker;->addI32(I)V

    invoke-virtual {v0, p2}, Landroid/renderscript/FieldPacker;->addI32(I)V

    const/4 v1, 0x1

    invoke-virtual {p0, v1, v0}, Landroid/renderscript/Script;->invoke(ILandroid/renderscript/FieldPacker;)V

    return-void
.end method

.method public invoke_initLeaves()V
    .locals 1

    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Landroid/renderscript/Script;->invoke(I)V

    return-void
.end method

.method public declared-synchronized set_g_PFBackground(Landroid/renderscript/ProgramFragment;)V
    .locals 1
    .param p1    # Landroid/renderscript/ProgramFragment;

    monitor-enter p0

    const/16 v0, 0xa

    :try_start_0
    invoke-virtual {p0, v0, p1}, Landroid/renderscript/Script;->setVar(ILandroid/renderscript/BaseObj;)V

    iput-object p1, p0, Lcom/android/wallpaper/fall/ScriptC_fall;->mExportVar_g_PFBackground:Landroid/renderscript/ProgramFragment;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized set_g_PFSBackground(Landroid/renderscript/ProgramStore;)V
    .locals 1
    .param p1    # Landroid/renderscript/ProgramStore;

    monitor-enter p0

    const/16 v0, 0xf

    :try_start_0
    invoke-virtual {p0, v0, p1}, Landroid/renderscript/Script;->setVar(ILandroid/renderscript/BaseObj;)V

    iput-object p1, p0, Lcom/android/wallpaper/fall/ScriptC_fall;->mExportVar_g_PFSBackground:Landroid/renderscript/ProgramStore;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized set_g_PFSLeaf(Landroid/renderscript/ProgramStore;)V
    .locals 1
    .param p1    # Landroid/renderscript/ProgramStore;

    monitor-enter p0

    const/16 v0, 0x9

    :try_start_0
    invoke-virtual {p0, v0, p1}, Landroid/renderscript/Script;->setVar(ILandroid/renderscript/BaseObj;)V

    iput-object p1, p0, Lcom/android/wallpaper/fall/ScriptC_fall;->mExportVar_g_PFSLeaf:Landroid/renderscript/ProgramStore;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized set_g_PFSky(Landroid/renderscript/ProgramFragment;)V
    .locals 1
    .param p1    # Landroid/renderscript/ProgramFragment;

    monitor-enter p0

    const/16 v0, 0x8

    :try_start_0
    invoke-virtual {p0, v0, p1}, Landroid/renderscript/Script;->setVar(ILandroid/renderscript/BaseObj;)V

    iput-object p1, p0, Lcom/android/wallpaper/fall/ScriptC_fall;->mExportVar_g_PFSky:Landroid/renderscript/ProgramFragment;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized set_g_PVSky(Landroid/renderscript/ProgramVertex;)V
    .locals 1
    .param p1    # Landroid/renderscript/ProgramVertex;

    monitor-enter p0

    const/4 v0, 0x7

    :try_start_0
    invoke-virtual {p0, v0, p1}, Landroid/renderscript/Script;->setVar(ILandroid/renderscript/BaseObj;)V

    iput-object p1, p0, Lcom/android/wallpaper/fall/ScriptC_fall;->mExportVar_g_PVSky:Landroid/renderscript/ProgramVertex;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized set_g_PVWater(Landroid/renderscript/ProgramVertex;)V
    .locals 1
    .param p1    # Landroid/renderscript/ProgramVertex;

    monitor-enter p0

    const/4 v0, 0x6

    :try_start_0
    invoke-virtual {p0, v0, p1}, Landroid/renderscript/Script;->setVar(ILandroid/renderscript/BaseObj;)V

    iput-object p1, p0, Lcom/android/wallpaper/fall/ScriptC_fall;->mExportVar_g_PVWater:Landroid/renderscript/ProgramVertex;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized set_g_TLeaves(Landroid/renderscript/Allocation;)V
    .locals 1
    .param p1    # Landroid/renderscript/Allocation;

    monitor-enter p0

    const/16 v0, 0xb

    :try_start_0
    invoke-virtual {p0, v0, p1}, Landroid/renderscript/Script;->setVar(ILandroid/renderscript/BaseObj;)V

    iput-object p1, p0, Lcom/android/wallpaper/fall/ScriptC_fall;->mExportVar_g_TLeaves:Landroid/renderscript/Allocation;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized set_g_TRiverbed(Landroid/renderscript/Allocation;)V
    .locals 1
    .param p1    # Landroid/renderscript/Allocation;

    monitor-enter p0

    const/16 v0, 0xc

    :try_start_0
    invoke-virtual {p0, v0, p1}, Landroid/renderscript/Script;->setVar(ILandroid/renderscript/BaseObj;)V

    iput-object p1, p0, Lcom/android/wallpaper/fall/ScriptC_fall;->mExportVar_g_TRiverbed:Landroid/renderscript/Allocation;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized set_g_WaterMesh(Landroid/renderscript/Mesh;)V
    .locals 1
    .param p1    # Landroid/renderscript/Mesh;

    monitor-enter p0

    const/16 v0, 0xd

    :try_start_0
    invoke-virtual {p0, v0, p1}, Landroid/renderscript/Script;->setVar(ILandroid/renderscript/BaseObj;)V

    iput-object p1, p0, Lcom/android/wallpaper/fall/ScriptC_fall;->mExportVar_g_WaterMesh:Landroid/renderscript/Mesh;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized set_g_glHeight(F)V
    .locals 1
    .param p1    # F

    monitor-enter p0

    const/4 v0, 0x1

    :try_start_0
    invoke-virtual {p0, v0, p1}, Landroid/renderscript/Script;->setVar(IF)V

    iput p1, p0, Lcom/android/wallpaper/fall/ScriptC_fall;->mExportVar_g_glHeight:F
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized set_g_glWidth(F)V
    .locals 1
    .param p1    # F

    monitor-enter p0

    const/4 v0, 0x0

    :try_start_0
    invoke-virtual {p0, v0, p1}, Landroid/renderscript/Script;->setVar(IF)V

    iput p1, p0, Lcom/android/wallpaper/fall/ScriptC_fall;->mExportVar_g_glWidth:F
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized set_g_meshHeight(F)V
    .locals 1
    .param p1    # F

    monitor-enter p0

    const/4 v0, 0x3

    :try_start_0
    invoke-virtual {p0, v0, p1}, Landroid/renderscript/Script;->setVar(IF)V

    iput p1, p0, Lcom/android/wallpaper/fall/ScriptC_fall;->mExportVar_g_meshHeight:F
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized set_g_meshWidth(F)V
    .locals 1
    .param p1    # F

    monitor-enter p0

    const/4 v0, 0x2

    :try_start_0
    invoke-virtual {p0, v0, p1}, Landroid/renderscript/Script;->setVar(IF)V

    iput p1, p0, Lcom/android/wallpaper/fall/ScriptC_fall;->mExportVar_g_meshWidth:F
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized set_g_rotate(F)V
    .locals 1
    .param p1    # F

    monitor-enter p0

    const/4 v0, 0x5

    :try_start_0
    invoke-virtual {p0, v0, p1}, Landroid/renderscript/Script;->setVar(IF)V

    iput p1, p0, Lcom/android/wallpaper/fall/ScriptC_fall;->mExportVar_g_rotate:F
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized set_g_xOffset(F)V
    .locals 1
    .param p1    # F

    monitor-enter p0

    const/4 v0, 0x4

    :try_start_0
    invoke-virtual {p0, v0, p1}, Landroid/renderscript/Script;->setVar(IF)V

    iput p1, p0, Lcom/android/wallpaper/fall/ScriptC_fall;->mExportVar_g_xOffset:F
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method
