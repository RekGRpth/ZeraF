.class Lcom/mediatek/voiceunlock/PswPreview$2;
.super Ljava/lang/Object;
.source "PswPreview.java"

# interfaces
.implements Landroid/media/AudioManager$OnAudioFocusChangeListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/mediatek/voiceunlock/PswPreview;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/mediatek/voiceunlock/PswPreview;


# direct methods
.method constructor <init>(Lcom/mediatek/voiceunlock/PswPreview;)V
    .locals 0

    iput-object p1, p0, Lcom/mediatek/voiceunlock/PswPreview$2;->this$0:Lcom/mediatek/voiceunlock/PswPreview;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onAudioFocusChange(I)V
    .locals 2
    .param p1    # I

    const/4 v1, 0x0

    iget-object v0, p0, Lcom/mediatek/voiceunlock/PswPreview$2;->this$0:Lcom/mediatek/voiceunlock/PswPreview;

    invoke-static {v0}, Lcom/mediatek/voiceunlock/PswPreview;->access$200(Lcom/mediatek/voiceunlock/PswPreview;)Lcom/mediatek/voiceunlock/PswPreview$PreviewPlayer;

    move-result-object v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/mediatek/voiceunlock/PswPreview$2;->this$0:Lcom/mediatek/voiceunlock/PswPreview;

    invoke-static {v0}, Lcom/mediatek/voiceunlock/PswPreview;->access$400(Lcom/mediatek/voiceunlock/PswPreview;)Landroid/media/AudioManager;

    move-result-object v0

    invoke-virtual {v0, p0}, Landroid/media/AudioManager;->abandonAudioFocus(Landroid/media/AudioManager$OnAudioFocusChangeListener;)I

    :goto_0
    return-void

    :cond_0
    packed-switch p1, :pswitch_data_0

    :cond_1
    :goto_1
    :pswitch_0
    iget-object v0, p0, Lcom/mediatek/voiceunlock/PswPreview$2;->this$0:Lcom/mediatek/voiceunlock/PswPreview;

    invoke-static {v0}, Lcom/mediatek/voiceunlock/PswPreview;->access$700(Lcom/mediatek/voiceunlock/PswPreview;)V

    goto :goto_0

    :pswitch_1
    iget-object v0, p0, Lcom/mediatek/voiceunlock/PswPreview$2;->this$0:Lcom/mediatek/voiceunlock/PswPreview;

    invoke-static {v0, v1}, Lcom/mediatek/voiceunlock/PswPreview;->access$502(Lcom/mediatek/voiceunlock/PswPreview;Z)Z

    iget-object v0, p0, Lcom/mediatek/voiceunlock/PswPreview$2;->this$0:Lcom/mediatek/voiceunlock/PswPreview;

    invoke-static {v0}, Lcom/mediatek/voiceunlock/PswPreview;->access$200(Lcom/mediatek/voiceunlock/PswPreview;)Lcom/mediatek/voiceunlock/PswPreview$PreviewPlayer;

    move-result-object v0

    invoke-virtual {v0}, Landroid/media/MediaPlayer;->pause()V

    goto :goto_1

    :pswitch_2
    iget-object v0, p0, Lcom/mediatek/voiceunlock/PswPreview$2;->this$0:Lcom/mediatek/voiceunlock/PswPreview;

    invoke-static {v0}, Lcom/mediatek/voiceunlock/PswPreview;->access$200(Lcom/mediatek/voiceunlock/PswPreview;)Lcom/mediatek/voiceunlock/PswPreview$PreviewPlayer;

    move-result-object v0

    invoke-virtual {v0}, Landroid/media/MediaPlayer;->isPlaying()Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/mediatek/voiceunlock/PswPreview$2;->this$0:Lcom/mediatek/voiceunlock/PswPreview;

    const/4 v1, 0x1

    invoke-static {v0, v1}, Lcom/mediatek/voiceunlock/PswPreview;->access$502(Lcom/mediatek/voiceunlock/PswPreview;Z)Z

    iget-object v0, p0, Lcom/mediatek/voiceunlock/PswPreview$2;->this$0:Lcom/mediatek/voiceunlock/PswPreview;

    invoke-static {v0}, Lcom/mediatek/voiceunlock/PswPreview;->access$200(Lcom/mediatek/voiceunlock/PswPreview;)Lcom/mediatek/voiceunlock/PswPreview$PreviewPlayer;

    move-result-object v0

    invoke-virtual {v0}, Landroid/media/MediaPlayer;->pause()V

    goto :goto_1

    :pswitch_3
    iget-object v0, p0, Lcom/mediatek/voiceunlock/PswPreview$2;->this$0:Lcom/mediatek/voiceunlock/PswPreview;

    invoke-static {v0}, Lcom/mediatek/voiceunlock/PswPreview;->access$500(Lcom/mediatek/voiceunlock/PswPreview;)Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/mediatek/voiceunlock/PswPreview$2;->this$0:Lcom/mediatek/voiceunlock/PswPreview;

    invoke-static {v0, v1}, Lcom/mediatek/voiceunlock/PswPreview;->access$502(Lcom/mediatek/voiceunlock/PswPreview;Z)Z

    iget-object v0, p0, Lcom/mediatek/voiceunlock/PswPreview$2;->this$0:Lcom/mediatek/voiceunlock/PswPreview;

    invoke-static {v0}, Lcom/mediatek/voiceunlock/PswPreview;->access$600(Lcom/mediatek/voiceunlock/PswPreview;)V

    goto :goto_1

    :pswitch_data_0
    .packed-switch -0x3
        :pswitch_2
        :pswitch_2
        :pswitch_1
        :pswitch_0
        :pswitch_3
    .end packed-switch
.end method
