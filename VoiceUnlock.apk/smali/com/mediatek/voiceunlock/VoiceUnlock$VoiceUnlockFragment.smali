.class public Lcom/mediatek/voiceunlock/VoiceUnlock$VoiceUnlockFragment;
.super Lcom/mediatek/voiceunlock/SettingsPreferenceFragment;
.source "VoiceUnlock.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/mediatek/voiceunlock/VoiceUnlock;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "VoiceUnlockFragment"
.end annotation


# static fields
.field private static final COMFIRM_RESET_DIALOG:I = 0x1

.field private static final CONFIRM_CREDENTIALS:Ljava/lang/String; = "confirm_credentials"

.field static final KEY_COMMAND_SUMMARY:Ljava/lang/String; = "command_summary"

.field static final KEY_VOICE_COMMAND1:Ljava/lang/String; = "voice_command1"

.field static final KEY_VOICE_COMMAND2:Ljava/lang/String; = "voice_command2"

.field static final KEY_VOICE_COMMAND3:Ljava/lang/String; = "voice_command3"

.field static final KEY_VOICE_UNLOCK:Ljava/lang/String; = "voice_unlock"

.field private static final MSG_PLAY_PASSWORD:I = 0x0

.field private static final MSG_SERVICE_ERROR:I = 0x1

.field private static final OPTIONS_DIALOG:I


# instance fields
.field private mClickedCmdKey:Ljava/lang/String;

.field private mClickedCmdSummary:Ljava/lang/CharSequence;

.field private mCommand1:Lcom/mediatek/voiceunlock/VoiceUnlockPreference;

.field private mCommand2:Lcom/mediatek/voiceunlock/VoiceUnlockPreference;

.field private mCommand3:Lcom/mediatek/voiceunlock/VoiceUnlockPreference;

.field private mHandler:Landroid/os/Handler;

.field private mUnlock:Lcom/mediatek/voiceunlock/VoiceUnlockPreference;

.field private mVoiceCmdListener:Lcom/mediatek/common/voicecommand/VoiceCommandListener;

.field private mVoiceCmdManager:Lcom/mediatek/common/voicecommand/IVoiceCommandManager;

.field voice_command1_set:Z

.field voice_command2_set:Z

.field voice_command3_set:Z

.field voice_unlock_set:Z


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Lcom/mediatek/voiceunlock/SettingsPreferenceFragment;-><init>()V

    return-void
.end method

.method static synthetic access$000(Lcom/mediatek/voiceunlock/VoiceUnlock$VoiceUnlockFragment;Ljava/lang/String;)V
    .locals 0
    .param p0    # Lcom/mediatek/voiceunlock/VoiceUnlock$VoiceUnlockFragment;
    .param p1    # Ljava/lang/String;

    invoke-direct {p0, p1}, Lcom/mediatek/voiceunlock/VoiceUnlock$VoiceUnlockFragment;->log(Ljava/lang/String;)V

    return-void
.end method

.method static synthetic access$100(Lcom/mediatek/voiceunlock/VoiceUnlock$VoiceUnlockFragment;)Landroid/os/Handler;
    .locals 1
    .param p0    # Lcom/mediatek/voiceunlock/VoiceUnlock$VoiceUnlockFragment;

    iget-object v0, p0, Lcom/mediatek/voiceunlock/VoiceUnlock$VoiceUnlockFragment;->mHandler:Landroid/os/Handler;

    return-object v0
.end method

.method static synthetic access$200(Lcom/mediatek/voiceunlock/VoiceUnlock$VoiceUnlockFragment;Ljava/lang/String;)V
    .locals 0
    .param p0    # Lcom/mediatek/voiceunlock/VoiceUnlock$VoiceUnlockFragment;
    .param p1    # Ljava/lang/String;

    invoke-direct {p0, p1}, Lcom/mediatek/voiceunlock/VoiceUnlock$VoiceUnlockFragment;->handlePlayPassword(Ljava/lang/String;)V

    return-void
.end method

.method static synthetic access$300(Lcom/mediatek/voiceunlock/VoiceUnlock$VoiceUnlockFragment;Ljava/lang/String;)V
    .locals 0
    .param p0    # Lcom/mediatek/voiceunlock/VoiceUnlock$VoiceUnlockFragment;
    .param p1    # Ljava/lang/String;

    invoke-direct {p0, p1}, Lcom/mediatek/voiceunlock/VoiceUnlock$VoiceUnlockFragment;->handleServiceError(Ljava/lang/String;)V

    return-void
.end method

.method static synthetic access$400(Lcom/mediatek/voiceunlock/VoiceUnlock$VoiceUnlockFragment;)Ljava/lang/String;
    .locals 1
    .param p0    # Lcom/mediatek/voiceunlock/VoiceUnlock$VoiceUnlockFragment;

    iget-object v0, p0, Lcom/mediatek/voiceunlock/VoiceUnlock$VoiceUnlockFragment;->mClickedCmdKey:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$500(Lcom/mediatek/voiceunlock/VoiceUnlock$VoiceUnlockFragment;Ljava/lang/String;)V
    .locals 0
    .param p0    # Lcom/mediatek/voiceunlock/VoiceUnlock$VoiceUnlockFragment;
    .param p1    # Ljava/lang/String;

    invoke-direct {p0, p1}, Lcom/mediatek/voiceunlock/VoiceUnlock$VoiceUnlockFragment;->playCommand(Ljava/lang/String;)V

    return-void
.end method

.method static synthetic access$600(Lcom/mediatek/voiceunlock/VoiceUnlock$VoiceUnlockFragment;)Z
    .locals 1
    .param p0    # Lcom/mediatek/voiceunlock/VoiceUnlock$VoiceUnlockFragment;

    invoke-direct {p0}, Lcom/mediatek/voiceunlock/VoiceUnlock$VoiceUnlockFragment;->isLastCommand()Z

    move-result v0

    return v0
.end method

.method static synthetic access$700(Lcom/mediatek/voiceunlock/VoiceUnlock$VoiceUnlockFragment;Ljava/lang/String;Z)V
    .locals 0
    .param p0    # Lcom/mediatek/voiceunlock/VoiceUnlock$VoiceUnlockFragment;
    .param p1    # Ljava/lang/String;
    .param p2    # Z

    invoke-direct {p0, p1, p2}, Lcom/mediatek/voiceunlock/VoiceUnlock$VoiceUnlockFragment;->resetCommand(Ljava/lang/String;Z)V

    return-void
.end method

.method private getCommandId(Ljava/lang/String;)I
    .locals 1
    .param p1    # Ljava/lang/String;

    const-string v0, "voice_command1"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const-string v0, "voice_command2"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    const/4 v0, 0x2

    goto :goto_0

    :cond_1
    const-string v0, "voice_command3"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    const/4 v0, 0x3

    goto :goto_0

    :cond_2
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private getCommandSummary(Ljava/lang/String;)Ljava/lang/String;
    .locals 9
    .param p1    # Ljava/lang/String;

    invoke-static {p1}, Landroid/content/ComponentName;->unflattenFromString(Ljava/lang/String;)Landroid/content/ComponentName;

    move-result-object v0

    const-string v3, ""

    :try_start_0
    invoke-virtual {p0}, Lcom/mediatek/voiceunlock/SettingsPreferenceFragment;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v5

    const/16 v6, 0x400

    invoke-virtual {v5, v0, v6}, Landroid/content/pm/PackageManager;->getActivityInfo(Landroid/content/ComponentName;I)Landroid/content/pm/ActivityInfo;

    move-result-object v2

    invoke-virtual {p0}, Lcom/mediatek/voiceunlock/SettingsPreferenceFragment;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v5

    invoke-virtual {v2, v5}, Landroid/content/pm/ComponentInfo;->loadLabel(Landroid/content/pm/PackageManager;)Ljava/lang/CharSequence;
    :try_end_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v3

    :goto_0
    invoke-virtual {p0}, Landroid/app/Fragment;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    const v6, 0x7f080013

    const/4 v7, 0x1

    new-array v7, v7, [Ljava/lang/Object;

    const/4 v8, 0x0

    aput-object v3, v7, v8

    invoke-virtual {v5, v6, v7}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    return-object v4

    :catch_0
    move-exception v1

    const-string v5, "Cann\'t get app activityInfo via mCommandValue"

    invoke-direct {p0, v5}, Lcom/mediatek/voiceunlock/VoiceUnlock$VoiceUnlockFragment;->log(Ljava/lang/String;)V

    goto :goto_0
.end method

.method private handlePlayPassword(Ljava/lang/String;)V
    .locals 7
    .param p1    # Ljava/lang/String;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "handlePlayPassword path = "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-direct {p0, v4}, Lcom/mediatek/voiceunlock/VoiceUnlock$VoiceUnlockFragment;->log(Ljava/lang/String;)V

    new-instance v1, Ljava/io/File;

    invoke-direct {v1, p1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    new-instance v2, Landroid/content/Intent;

    invoke-direct {v2}, Landroid/content/Intent;-><init>()V

    invoke-virtual {p0}, Landroid/app/Fragment;->getActivity()Landroid/app/Activity;

    move-result-object v4

    const-class v5, Lcom/mediatek/voiceunlock/PswPreview;

    invoke-virtual {v2, v4, v5}, Landroid/content/Intent;->setClass(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    const-string v4, "command_summary"

    iget-object v5, p0, Lcom/mediatek/voiceunlock/VoiceUnlock$VoiceUnlockFragment;->mClickedCmdSummary:Ljava/lang/CharSequence;

    invoke-virtual {v2, v4, v5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/CharSequence;)Landroid/content/Intent;

    invoke-static {v1}, Landroid/net/Uri;->fromFile(Ljava/io/File;)Landroid/net/Uri;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/content/Intent;->setData(Landroid/net/Uri;)Landroid/content/Intent;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "handlePlayPassword uri = "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-direct {p0, v4}, Lcom/mediatek/voiceunlock/VoiceUnlock$VoiceUnlockFragment;->log(Ljava/lang/String;)V

    :try_start_0
    invoke-virtual {p0, v2}, Landroid/app/Fragment;->startActivity(Landroid/content/Intent;)V
    :try_end_0
    .catch Landroid/content/ActivityNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    return-void

    :catch_0
    move-exception v0

    invoke-virtual {p0}, Landroid/app/Fragment;->getActivity()Landroid/app/Activity;

    move-result-object v4

    const v5, 0x7f080028

    const/4 v6, 0x0

    invoke-static {v4, v5, v6}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v4

    invoke-virtual {v4}, Landroid/widget/Toast;->show()V

    goto :goto_0
.end method

.method private handleServiceError(Ljava/lang/String;)V
    .locals 2
    .param p1    # Ljava/lang/String;

    invoke-virtual {p0}, Landroid/app/Fragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    const/4 v1, 0x0

    invoke-static {v0, p1, v1}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    return-void
.end method

.method private isLastCommand()Z
    .locals 8

    const/4 v5, 0x1

    const/4 v0, 0x0

    invoke-virtual {p0}, Lcom/mediatek/voiceunlock/SettingsPreferenceFragment;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v6

    const-string v7, "voice_unlock_screen"

    invoke-static {v6, v7}, Landroid/provider/Settings$System;->getString(Landroid/content/ContentResolver;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    if-eqz v4, :cond_0

    add-int/lit8 v0, v0, 0x1

    :cond_0
    invoke-virtual {p0}, Lcom/mediatek/voiceunlock/SettingsPreferenceFragment;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v6

    const-string v7, "voice_unlock_and_launch1"

    invoke-static {v6, v7}, Landroid/provider/Settings$System;->getString(Landroid/content/ContentResolver;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_1

    add-int/lit8 v0, v0, 0x1

    :cond_1
    invoke-virtual {p0}, Lcom/mediatek/voiceunlock/SettingsPreferenceFragment;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v6

    const-string v7, "voice_unlock_and_launch2"

    invoke-static {v6, v7}, Landroid/provider/Settings$System;->getString(Landroid/content/ContentResolver;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    if-eqz v2, :cond_2

    add-int/lit8 v0, v0, 0x1

    :cond_2
    invoke-virtual {p0}, Lcom/mediatek/voiceunlock/SettingsPreferenceFragment;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v6

    const-string v7, "voice_unlock_and_launch3"

    invoke-static {v6, v7}, Landroid/provider/Settings$System;->getString(Landroid/content/ContentResolver;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    if-eqz v3, :cond_3

    add-int/lit8 v0, v0, 0x1

    :cond_3
    if-ne v0, v5, :cond_4

    :goto_0
    return v5

    :cond_4
    const/4 v5, 0x0

    goto :goto_0
.end method

.method private log(Ljava/lang/String;)V
    .locals 3
    .param p1    # Ljava/lang/String;

    const-string v0, "VoiceUnlockSetting"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "VoiceUnlock: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/mediatek/xlog/Xlog;->d(Ljava/lang/String;Ljava/lang/String;)I

    return-void
.end method

.method private playCommand(Ljava/lang/String;)V
    .locals 7
    .param p1    # Ljava/lang/String;

    iget-object v3, p0, Lcom/mediatek/voiceunlock/VoiceUnlock$VoiceUnlockFragment;->mVoiceCmdManager:Lcom/mediatek/common/voicecommand/IVoiceCommandManager;

    if-eqz v3, :cond_0

    :try_start_0
    new-instance v2, Landroid/os/Bundle;

    invoke-direct {v2}, Landroid/os/Bundle;-><init>()V

    invoke-direct {p0, p1}, Lcom/mediatek/voiceunlock/VoiceUnlock$VoiceUnlockFragment;->getCommandId(Ljava/lang/String;)I

    move-result v0

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "sendCommand TRAINING_PSWDFILE commandId = "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-direct {p0, v3}, Lcom/mediatek/voiceunlock/VoiceUnlock$VoiceUnlockFragment;->log(Ljava/lang/String;)V

    const-string v3, "Send_Info"

    invoke-virtual {v2, v3, v0}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    iget-object v3, p0, Lcom/mediatek/voiceunlock/VoiceUnlock$VoiceUnlockFragment;->mVoiceCmdManager:Lcom/mediatek/common/voicecommand/IVoiceCommandManager;

    invoke-virtual {p0}, Landroid/app/Fragment;->getActivity()Landroid/app/Activity;

    move-result-object v4

    const/4 v5, 0x3

    const/4 v6, 0x4

    invoke-interface {v3, v4, v5, v6, v2}, Lcom/mediatek/common/voicecommand/IVoiceCommandManager;->sendCommand(Landroid/content/Context;IILandroid/os/Bundle;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/IllegalAccessException; {:try_start_0 .. :try_end_0} :catch_1

    :cond_0
    :goto_0
    return-void

    :catch_0
    move-exception v1

    invoke-virtual {v1}, Ljava/lang/Throwable;->printStackTrace()V

    goto :goto_0

    :catch_1
    move-exception v1

    invoke-virtual {v1}, Ljava/lang/Throwable;->printStackTrace()V

    goto :goto_0
.end method

.method private resetCommand(Ljava/lang/String;Z)V
    .locals 10
    .param p1    # Ljava/lang/String;
    .param p2    # Z

    const/4 v9, 0x0

    const/4 v8, 0x0

    iget-object v4, p0, Lcom/mediatek/voiceunlock/VoiceUnlock$VoiceUnlockFragment;->mVoiceCmdManager:Lcom/mediatek/common/voicecommand/IVoiceCommandManager;

    if-eqz v4, :cond_0

    :try_start_0
    new-instance v2, Landroid/os/Bundle;

    invoke-direct {v2}, Landroid/os/Bundle;-><init>()V

    invoke-direct {p0, p1}, Lcom/mediatek/voiceunlock/VoiceUnlock$VoiceUnlockFragment;->getCommandId(Ljava/lang/String;)I

    move-result v0

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "sendCommand TRAINING_RESET commandId = "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-direct {p0, v4}, Lcom/mediatek/voiceunlock/VoiceUnlock$VoiceUnlockFragment;->log(Ljava/lang/String;)V

    const-string v4, "Send_Info"

    invoke-virtual {v2, v4, v0}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    iget-object v4, p0, Lcom/mediatek/voiceunlock/VoiceUnlock$VoiceUnlockFragment;->mVoiceCmdManager:Lcom/mediatek/common/voicecommand/IVoiceCommandManager;

    invoke-virtual {p0}, Landroid/app/Fragment;->getActivity()Landroid/app/Activity;

    move-result-object v5

    const/4 v6, 0x3

    const/4 v7, 0x6

    invoke-interface {v4, v5, v6, v7, v2}, Lcom/mediatek/common/voicecommand/IVoiceCommandManager;->sendCommand(Landroid/content/Context;IILandroid/os/Bundle;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/IllegalAccessException; {:try_start_0 .. :try_end_0} :catch_1

    :cond_0
    :goto_0
    const-string v4, "voice_unlock"

    invoke-virtual {v4, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_3

    invoke-virtual {p0}, Lcom/mediatek/voiceunlock/SettingsPreferenceFragment;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v4

    const-string v5, "voice_unlock_screen"

    invoke-static {v4, v5, v8}, Landroid/provider/Settings$System;->putString(Landroid/content/ContentResolver;Ljava/lang/String;Ljava/lang/String;)Z

    :cond_1
    :goto_1
    if-eqz p2, :cond_2

    new-instance v3, Lcom/android/internal/widget/LockPatternUtils;

    invoke-virtual {p0}, Landroid/app/Fragment;->getActivity()Landroid/app/Activity;

    move-result-object v4

    invoke-direct {v3, v4}, Lcom/android/internal/widget/LockPatternUtils;-><init>(Landroid/content/Context;)V

    invoke-virtual {v3, v9}, Lcom/android/internal/widget/LockPatternUtils;->clearLock(Z)V

    invoke-virtual {v3, v9}, Lcom/android/internal/widget/LockPatternUtils;->setLockScreenDisabled(Z)V

    :cond_2
    invoke-direct {p0}, Lcom/mediatek/voiceunlock/VoiceUnlock$VoiceUnlockFragment;->updateCommandStatusAndSummary()V

    return-void

    :catch_0
    move-exception v1

    invoke-virtual {v1}, Ljava/lang/Throwable;->printStackTrace()V

    goto :goto_0

    :catch_1
    move-exception v1

    invoke-virtual {v1}, Ljava/lang/Throwable;->printStackTrace()V

    goto :goto_0

    :cond_3
    const-string v4, "voice_command1"

    invoke-virtual {v4, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_4

    invoke-virtual {p0}, Lcom/mediatek/voiceunlock/SettingsPreferenceFragment;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v4

    const-string v5, "voice_unlock_and_launch1"

    invoke-static {v4, v5, v8}, Landroid/provider/Settings$System;->putString(Landroid/content/ContentResolver;Ljava/lang/String;Ljava/lang/String;)Z

    goto :goto_1

    :cond_4
    const-string v4, "voice_command2"

    invoke-virtual {v4, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_5

    invoke-virtual {p0}, Lcom/mediatek/voiceunlock/SettingsPreferenceFragment;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v4

    const-string v5, "voice_unlock_and_launch2"

    invoke-static {v4, v5, v8}, Landroid/provider/Settings$System;->putString(Landroid/content/ContentResolver;Ljava/lang/String;Ljava/lang/String;)Z

    goto :goto_1

    :cond_5
    const-string v4, "voice_command3"

    invoke-virtual {v4, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_1

    invoke-virtual {p0}, Lcom/mediatek/voiceunlock/SettingsPreferenceFragment;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v4

    const-string v5, "voice_unlock_and_launch3"

    invoke-static {v4, v5, v8}, Landroid/provider/Settings$System;->putString(Landroid/content/ContentResolver;Ljava/lang/String;Ljava/lang/String;)Z

    goto :goto_1
.end method

.method private updateCommandStatusAndSummary()V
    .locals 8

    const/4 v5, 0x1

    const/4 v6, 0x0

    const-string v4, "updateCommandStatus "

    invoke-direct {p0, v4}, Lcom/mediatek/voiceunlock/VoiceUnlock$VoiceUnlockFragment;->log(Ljava/lang/String;)V

    invoke-virtual {p0}, Lcom/mediatek/voiceunlock/SettingsPreferenceFragment;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v4

    const-string v7, "voice_unlock_screen"

    invoke-static {v4, v7}, Landroid/provider/Settings$System;->getString(Landroid/content/ContentResolver;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    if-eqz v3, :cond_3

    move v4, v5

    :goto_0
    iput-boolean v4, p0, Lcom/mediatek/voiceunlock/VoiceUnlock$VoiceUnlockFragment;->voice_unlock_set:Z

    iget-object v4, p0, Lcom/mediatek/voiceunlock/VoiceUnlock$VoiceUnlockFragment;->mUnlock:Lcom/mediatek/voiceunlock/VoiceUnlockPreference;

    iget-boolean v7, p0, Lcom/mediatek/voiceunlock/VoiceUnlock$VoiceUnlockFragment;->voice_unlock_set:Z

    invoke-virtual {v4, v7}, Lcom/mediatek/voiceunlock/VoiceUnlockPreference;->setChecked(Z)V

    invoke-virtual {p0}, Lcom/mediatek/voiceunlock/SettingsPreferenceFragment;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v4

    const-string v7, "voice_unlock_and_launch1"

    invoke-static {v4, v7}, Landroid/provider/Settings$System;->getString(Landroid/content/ContentResolver;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_4

    move v4, v5

    :goto_1
    iput-boolean v4, p0, Lcom/mediatek/voiceunlock/VoiceUnlock$VoiceUnlockFragment;->voice_command1_set:Z

    iget-boolean v4, p0, Lcom/mediatek/voiceunlock/VoiceUnlock$VoiceUnlockFragment;->voice_command1_set:Z

    if-eqz v4, :cond_0

    iget-object v4, p0, Lcom/mediatek/voiceunlock/VoiceUnlock$VoiceUnlockFragment;->mCommand1:Lcom/mediatek/voiceunlock/VoiceUnlockPreference;

    invoke-direct {p0, v0}, Lcom/mediatek/voiceunlock/VoiceUnlock$VoiceUnlockFragment;->getCommandSummary(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v4, v7}, Landroid/preference/Preference;->setSummary(Ljava/lang/CharSequence;)V

    :cond_0
    iget-object v4, p0, Lcom/mediatek/voiceunlock/VoiceUnlock$VoiceUnlockFragment;->mCommand1:Lcom/mediatek/voiceunlock/VoiceUnlockPreference;

    iget-boolean v7, p0, Lcom/mediatek/voiceunlock/VoiceUnlock$VoiceUnlockFragment;->voice_command1_set:Z

    invoke-virtual {v4, v7}, Lcom/mediatek/voiceunlock/VoiceUnlockPreference;->setChecked(Z)V

    invoke-virtual {p0}, Lcom/mediatek/voiceunlock/SettingsPreferenceFragment;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v4

    const-string v7, "voice_unlock_and_launch2"

    invoke-static {v4, v7}, Landroid/provider/Settings$System;->getString(Landroid/content/ContentResolver;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_5

    move v4, v5

    :goto_2
    iput-boolean v4, p0, Lcom/mediatek/voiceunlock/VoiceUnlock$VoiceUnlockFragment;->voice_command2_set:Z

    iget-boolean v4, p0, Lcom/mediatek/voiceunlock/VoiceUnlock$VoiceUnlockFragment;->voice_command2_set:Z

    if-eqz v4, :cond_1

    iget-object v4, p0, Lcom/mediatek/voiceunlock/VoiceUnlock$VoiceUnlockFragment;->mCommand2:Lcom/mediatek/voiceunlock/VoiceUnlockPreference;

    invoke-direct {p0, v1}, Lcom/mediatek/voiceunlock/VoiceUnlock$VoiceUnlockFragment;->getCommandSummary(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v4, v7}, Landroid/preference/Preference;->setSummary(Ljava/lang/CharSequence;)V

    :cond_1
    iget-object v4, p0, Lcom/mediatek/voiceunlock/VoiceUnlock$VoiceUnlockFragment;->mCommand2:Lcom/mediatek/voiceunlock/VoiceUnlockPreference;

    iget-boolean v7, p0, Lcom/mediatek/voiceunlock/VoiceUnlock$VoiceUnlockFragment;->voice_command2_set:Z

    invoke-virtual {v4, v7}, Lcom/mediatek/voiceunlock/VoiceUnlockPreference;->setChecked(Z)V

    invoke-virtual {p0}, Lcom/mediatek/voiceunlock/SettingsPreferenceFragment;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v4

    const-string v7, "voice_unlock_and_launch3"

    invoke-static {v4, v7}, Landroid/provider/Settings$System;->getString(Landroid/content/ContentResolver;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    if-eqz v2, :cond_6

    :goto_3
    iput-boolean v5, p0, Lcom/mediatek/voiceunlock/VoiceUnlock$VoiceUnlockFragment;->voice_command3_set:Z

    iget-boolean v4, p0, Lcom/mediatek/voiceunlock/VoiceUnlock$VoiceUnlockFragment;->voice_command3_set:Z

    if-eqz v4, :cond_2

    iget-object v4, p0, Lcom/mediatek/voiceunlock/VoiceUnlock$VoiceUnlockFragment;->mCommand3:Lcom/mediatek/voiceunlock/VoiceUnlockPreference;

    invoke-direct {p0, v2}, Lcom/mediatek/voiceunlock/VoiceUnlock$VoiceUnlockFragment;->getCommandSummary(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Landroid/preference/Preference;->setSummary(Ljava/lang/CharSequence;)V

    :cond_2
    iget-object v4, p0, Lcom/mediatek/voiceunlock/VoiceUnlock$VoiceUnlockFragment;->mCommand3:Lcom/mediatek/voiceunlock/VoiceUnlockPreference;

    iget-boolean v5, p0, Lcom/mediatek/voiceunlock/VoiceUnlock$VoiceUnlockFragment;->voice_command3_set:Z

    invoke-virtual {v4, v5}, Lcom/mediatek/voiceunlock/VoiceUnlockPreference;->setChecked(Z)V

    return-void

    :cond_3
    move v4, v6

    goto :goto_0

    :cond_4
    move v4, v6

    goto :goto_1

    :cond_5
    move v4, v6

    goto :goto_2

    :cond_6
    move v5, v6

    goto :goto_3
.end method


# virtual methods
.method public onCreate(Landroid/os/Bundle;)V
    .locals 2
    .param p1    # Landroid/os/Bundle;

    invoke-super {p0, p1}, Landroid/preference/PreferenceFragment;->onCreate(Landroid/os/Bundle;)V

    const/high16 v0, 0x7f050000

    invoke-virtual {p0, v0}, Landroid/preference/PreferenceFragment;->addPreferencesFromResource(I)V

    const-string v0, "voicecommand"

    invoke-virtual {p0, v0}, Lcom/mediatek/voiceunlock/SettingsPreferenceFragment;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/mediatek/common/voicecommand/IVoiceCommandManager;

    iput-object v0, p0, Lcom/mediatek/voiceunlock/VoiceUnlock$VoiceUnlockFragment;->mVoiceCmdManager:Lcom/mediatek/common/voicecommand/IVoiceCommandManager;

    iget-object v0, p0, Lcom/mediatek/voiceunlock/VoiceUnlock$VoiceUnlockFragment;->mVoiceCmdManager:Lcom/mediatek/common/voicecommand/IVoiceCommandManager;

    if-eqz v0, :cond_0

    new-instance v0, Lcom/mediatek/voiceunlock/VoiceUnlock$VoiceUnlockFragment$1;

    invoke-virtual {p0}, Landroid/app/Fragment;->getActivity()Landroid/app/Activity;

    move-result-object v1

    invoke-direct {v0, p0, v1}, Lcom/mediatek/voiceunlock/VoiceUnlock$VoiceUnlockFragment$1;-><init>(Lcom/mediatek/voiceunlock/VoiceUnlock$VoiceUnlockFragment;Landroid/content/Context;)V

    iput-object v0, p0, Lcom/mediatek/voiceunlock/VoiceUnlock$VoiceUnlockFragment;->mVoiceCmdListener:Lcom/mediatek/common/voicecommand/VoiceCommandListener;

    :cond_0
    new-instance v0, Lcom/mediatek/voiceunlock/VoiceUnlock$VoiceUnlockFragment$2;

    invoke-direct {v0, p0}, Lcom/mediatek/voiceunlock/VoiceUnlock$VoiceUnlockFragment$2;-><init>(Lcom/mediatek/voiceunlock/VoiceUnlock$VoiceUnlockFragment;)V

    iput-object v0, p0, Lcom/mediatek/voiceunlock/VoiceUnlock$VoiceUnlockFragment;->mHandler:Landroid/os/Handler;

    return-void
.end method

.method public onCreateDialog(I)Landroid/app/Dialog;
    .locals 4
    .param p1    # I

    packed-switch p1, :pswitch_data_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :pswitch_0
    new-instance v1, Landroid/app/AlertDialog$Builder;

    invoke-virtual {p0}, Landroid/app/Fragment;->getActivity()Landroid/app/Activity;

    move-result-object v2

    invoke-direct {v1, v2}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    const v2, 0x7f080008

    invoke-virtual {v1, v2}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    const/high16 v2, 0x7f070000

    new-instance v3, Lcom/mediatek/voiceunlock/VoiceUnlock$VoiceUnlockFragment$3;

    invoke-direct {v3, p0}, Lcom/mediatek/voiceunlock/VoiceUnlock$VoiceUnlockFragment$3;-><init>(Lcom/mediatek/voiceunlock/VoiceUnlock$VoiceUnlockFragment;)V

    invoke-virtual {v1, v2, v3}, Landroid/app/AlertDialog$Builder;->setItems(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    invoke-virtual {v1}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v0

    goto :goto_0

    :pswitch_1
    new-instance v1, Landroid/app/AlertDialog$Builder;

    invoke-virtual {p0}, Landroid/app/Fragment;->getActivity()Landroid/app/Activity;

    move-result-object v2

    invoke-direct {v1, v2}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    const v2, 0x7f080004

    invoke-virtual {v1, v2}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    const v2, 0x7f08000a

    new-instance v3, Lcom/mediatek/voiceunlock/VoiceUnlock$VoiceUnlockFragment$5;

    invoke-direct {v3, p0}, Lcom/mediatek/voiceunlock/VoiceUnlock$VoiceUnlockFragment$5;-><init>(Lcom/mediatek/voiceunlock/VoiceUnlock$VoiceUnlockFragment;)V

    invoke-virtual {v1, v2, v3}, Landroid/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    const v2, 0x7f080009

    new-instance v3, Lcom/mediatek/voiceunlock/VoiceUnlock$VoiceUnlockFragment$4;

    invoke-direct {v3, p0}, Lcom/mediatek/voiceunlock/VoiceUnlock$VoiceUnlockFragment$4;-><init>(Lcom/mediatek/voiceunlock/VoiceUnlock$VoiceUnlockFragment;)V

    invoke-virtual {v1, v2, v3}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    invoke-virtual {v1}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v0

    invoke-direct {p0}, Lcom/mediatek/voiceunlock/VoiceUnlock$VoiceUnlockFragment;->isLastCommand()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-virtual {p0}, Landroid/app/Fragment;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f080007

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog;->setMessage(Ljava/lang/CharSequence;)V

    goto :goto_0

    :cond_0
    invoke-virtual {p0}, Landroid/app/Fragment;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f080006

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog;->setMessage(Ljava/lang/CharSequence;)V

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 2
    .param p1    # Landroid/view/LayoutInflater;
    .param p2    # Landroid/view/ViewGroup;
    .param p3    # Landroid/os/Bundle;

    invoke-super {p0, p1, p2, p3}, Landroid/preference/PreferenceFragment;->onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;

    move-result-object v0

    const-string v1, "voice_unlock"

    invoke-virtual {p0, v1}, Landroid/preference/PreferenceFragment;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v1

    check-cast v1, Lcom/mediatek/voiceunlock/VoiceUnlockPreference;

    iput-object v1, p0, Lcom/mediatek/voiceunlock/VoiceUnlock$VoiceUnlockFragment;->mUnlock:Lcom/mediatek/voiceunlock/VoiceUnlockPreference;

    const-string v1, "voice_command1"

    invoke-virtual {p0, v1}, Landroid/preference/PreferenceFragment;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v1

    check-cast v1, Lcom/mediatek/voiceunlock/VoiceUnlockPreference;

    iput-object v1, p0, Lcom/mediatek/voiceunlock/VoiceUnlock$VoiceUnlockFragment;->mCommand1:Lcom/mediatek/voiceunlock/VoiceUnlockPreference;

    const-string v1, "voice_command2"

    invoke-virtual {p0, v1}, Landroid/preference/PreferenceFragment;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v1

    check-cast v1, Lcom/mediatek/voiceunlock/VoiceUnlockPreference;

    iput-object v1, p0, Lcom/mediatek/voiceunlock/VoiceUnlock$VoiceUnlockFragment;->mCommand2:Lcom/mediatek/voiceunlock/VoiceUnlockPreference;

    const-string v1, "voice_command3"

    invoke-virtual {p0, v1}, Landroid/preference/PreferenceFragment;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v1

    check-cast v1, Lcom/mediatek/voiceunlock/VoiceUnlockPreference;

    iput-object v1, p0, Lcom/mediatek/voiceunlock/VoiceUnlock$VoiceUnlockFragment;->mCommand3:Lcom/mediatek/voiceunlock/VoiceUnlockPreference;

    return-object v0
.end method

.method public onPause()V
    .locals 6

    invoke-super {p0}, Landroid/app/Fragment;->onPause()V

    iget-object v1, p0, Lcom/mediatek/voiceunlock/VoiceUnlock$VoiceUnlockFragment;->mVoiceCmdManager:Lcom/mediatek/common/voicecommand/IVoiceCommandManager;

    if-eqz v1, :cond_0

    :try_start_0
    const-string v1, "sendCommand TRAINING_STOP"

    invoke-direct {p0, v1}, Lcom/mediatek/voiceunlock/VoiceUnlock$VoiceUnlockFragment;->log(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/mediatek/voiceunlock/VoiceUnlock$VoiceUnlockFragment;->mVoiceCmdManager:Lcom/mediatek/common/voicecommand/IVoiceCommandManager;

    invoke-virtual {p0}, Landroid/app/Fragment;->getActivity()Landroid/app/Activity;

    move-result-object v2

    const/4 v3, 0x3

    const/4 v4, 0x2

    const/4 v5, 0x0

    invoke-interface {v1, v2, v3, v4, v5}, Lcom/mediatek/common/voicecommand/IVoiceCommandManager;->sendCommand(Landroid/content/Context;IILandroid/os/Bundle;)V

    const-string v1, "unregister to service"

    invoke-direct {p0, v1}, Lcom/mediatek/voiceunlock/VoiceUnlock$VoiceUnlockFragment;->log(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/mediatek/voiceunlock/VoiceUnlock$VoiceUnlockFragment;->mVoiceCmdManager:Lcom/mediatek/common/voicecommand/IVoiceCommandManager;

    iget-object v2, p0, Lcom/mediatek/voiceunlock/VoiceUnlock$VoiceUnlockFragment;->mVoiceCmdListener:Lcom/mediatek/common/voicecommand/VoiceCommandListener;

    invoke-interface {v1, v2}, Lcom/mediatek/common/voicecommand/IVoiceCommandManager;->unRegisterListener(Lcom/mediatek/common/voicecommand/VoiceCommandListener;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/IllegalAccessException; {:try_start_0 .. :try_end_0} :catch_1

    :cond_0
    :goto_0
    return-void

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Ljava/lang/Throwable;->printStackTrace()V

    goto :goto_0

    :catch_1
    move-exception v0

    invoke-virtual {v0}, Ljava/lang/Throwable;->printStackTrace()V

    goto :goto_0
.end method

.method public onPreferenceTreeClick(Landroid/preference/PreferenceScreen;Landroid/preference/Preference;)Z
    .locals 7
    .param p1    # Landroid/preference/PreferenceScreen;
    .param p2    # Landroid/preference/Preference;

    invoke-virtual {p2}, Landroid/preference/Preference;->getKey()Ljava/lang/String;

    move-result-object v3

    move-object v4, p2

    check-cast v4, Lcom/mediatek/voiceunlock/VoiceUnlockPreference;

    const/4 v0, 0x1

    invoke-virtual {v4}, Landroid/preference/TwoStatePreference;->isChecked()Z

    move-result v5

    if-eqz v5, :cond_1

    iput-object v3, p0, Lcom/mediatek/voiceunlock/VoiceUnlock$VoiceUnlockFragment;->mClickedCmdKey:Ljava/lang/String;

    const-string v5, "voice_unlock"

    invoke-virtual {v5, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_0

    invoke-virtual {p0}, Landroid/app/Fragment;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    const v6, 0x7f080017

    invoke-virtual {v5, v6}, Landroid/content/res/Resources;->getText(I)Ljava/lang/CharSequence;

    move-result-object v5

    iput-object v5, p0, Lcom/mediatek/voiceunlock/VoiceUnlock$VoiceUnlockFragment;->mClickedCmdSummary:Ljava/lang/CharSequence;

    :goto_0
    const/4 v5, 0x0

    invoke-virtual {p0, v5}, Lcom/mediatek/voiceunlock/SettingsPreferenceFragment;->showDialog(I)V

    move v1, v0

    :goto_1
    return v1

    :cond_0
    invoke-virtual {v4}, Landroid/preference/Preference;->getSummary()Ljava/lang/CharSequence;

    move-result-object v5

    iput-object v5, p0, Lcom/mediatek/voiceunlock/VoiceUnlock$VoiceUnlockFragment;->mClickedCmdSummary:Ljava/lang/CharSequence;

    goto :goto_0

    :cond_1
    const-string v5, "voice_unlock"

    invoke-virtual {v5, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_2

    new-instance v2, Landroid/content/Intent;

    invoke-direct {v2}, Landroid/content/Intent;-><init>()V

    invoke-virtual {p0}, Landroid/app/Fragment;->getActivity()Landroid/app/Activity;

    move-result-object v5

    const-class v6, Lcom/mediatek/voiceunlock/VoiceUnlockSetupIntro;

    invoke-virtual {v2, v5, v6}, Landroid/content/Intent;->setClass(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    const-string v5, "settings_command_key"

    const-string v6, "voice_unlock_screen"

    invoke-virtual {v2, v5, v6}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const-string v5, "settings_command_value"

    const-string v6, "set"

    invoke-virtual {v2, v5, v6}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    invoke-virtual {p0, v2}, Landroid/app/Fragment;->startActivity(Landroid/content/Intent;)V

    :goto_2
    move v1, v0

    goto :goto_1

    :cond_2
    const-string v5, "voice_command1"

    invoke-virtual {v5, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_3

    new-instance v2, Landroid/content/Intent;

    invoke-direct {v2}, Landroid/content/Intent;-><init>()V

    invoke-virtual {p0}, Landroid/app/Fragment;->getActivity()Landroid/app/Activity;

    move-result-object v5

    const-class v6, Lcom/mediatek/voiceunlock/VoiceCommandSelect;

    invoke-virtual {v2, v5, v6}, Landroid/content/Intent;->setClass(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    const-string v5, "settings_command_key"

    const-string v6, "voice_unlock_and_launch1"

    invoke-virtual {v2, v5, v6}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    invoke-virtual {p0, v2}, Landroid/app/Fragment;->startActivity(Landroid/content/Intent;)V

    goto :goto_2

    :cond_3
    const-string v5, "voice_command2"

    invoke-virtual {v5, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_4

    new-instance v2, Landroid/content/Intent;

    invoke-direct {v2}, Landroid/content/Intent;-><init>()V

    invoke-virtual {p0}, Landroid/app/Fragment;->getActivity()Landroid/app/Activity;

    move-result-object v5

    const-class v6, Lcom/mediatek/voiceunlock/VoiceCommandSelect;

    invoke-virtual {v2, v5, v6}, Landroid/content/Intent;->setClass(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    const-string v5, "settings_command_key"

    const-string v6, "voice_unlock_and_launch2"

    invoke-virtual {v2, v5, v6}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    invoke-virtual {p0, v2}, Landroid/app/Fragment;->startActivity(Landroid/content/Intent;)V

    goto :goto_2

    :cond_4
    const-string v5, "voice_command3"

    invoke-virtual {v5, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_5

    new-instance v2, Landroid/content/Intent;

    invoke-direct {v2}, Landroid/content/Intent;-><init>()V

    invoke-virtual {p0}, Landroid/app/Fragment;->getActivity()Landroid/app/Activity;

    move-result-object v5

    const-class v6, Lcom/mediatek/voiceunlock/VoiceCommandSelect;

    invoke-virtual {v2, v5, v6}, Landroid/content/Intent;->setClass(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    const-string v5, "settings_command_key"

    const-string v6, "voice_unlock_and_launch3"

    invoke-virtual {v2, v5, v6}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    invoke-virtual {p0, v2}, Landroid/app/Fragment;->startActivity(Landroid/content/Intent;)V

    goto :goto_2

    :cond_5
    const/4 v0, 0x0

    goto :goto_2
.end method

.method public onResume()V
    .locals 3

    invoke-direct {p0}, Lcom/mediatek/voiceunlock/VoiceUnlock$VoiceUnlockFragment;->updateCommandStatusAndSummary()V

    invoke-super {p0}, Landroid/app/Fragment;->onResume()V

    iget-object v1, p0, Lcom/mediatek/voiceunlock/VoiceUnlock$VoiceUnlockFragment;->mVoiceCmdManager:Lcom/mediatek/common/voicecommand/IVoiceCommandManager;

    if-eqz v1, :cond_0

    :try_start_0
    const-string v1, "register to service"

    invoke-direct {p0, v1}, Lcom/mediatek/voiceunlock/VoiceUnlock$VoiceUnlockFragment;->log(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/mediatek/voiceunlock/VoiceUnlock$VoiceUnlockFragment;->mVoiceCmdManager:Lcom/mediatek/common/voicecommand/IVoiceCommandManager;

    iget-object v2, p0, Lcom/mediatek/voiceunlock/VoiceUnlock$VoiceUnlockFragment;->mVoiceCmdListener:Lcom/mediatek/common/voicecommand/VoiceCommandListener;

    invoke-interface {v1, v2}, Lcom/mediatek/common/voicecommand/IVoiceCommandManager;->registerListener(Lcom/mediatek/common/voicecommand/VoiceCommandListener;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/IllegalAccessException; {:try_start_0 .. :try_end_0} :catch_1

    :cond_0
    :goto_0
    return-void

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Ljava/lang/Throwable;->printStackTrace()V

    goto :goto_0

    :catch_1
    move-exception v0

    invoke-virtual {v0}, Ljava/lang/Throwable;->printStackTrace()V

    goto :goto_0
.end method
