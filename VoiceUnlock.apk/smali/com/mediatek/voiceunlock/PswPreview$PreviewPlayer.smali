.class Lcom/mediatek/voiceunlock/PswPreview$PreviewPlayer;
.super Landroid/media/MediaPlayer;
.source "PswPreview.java"

# interfaces
.implements Landroid/media/MediaPlayer$OnPreparedListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/mediatek/voiceunlock/PswPreview;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "PreviewPlayer"
.end annotation


# instance fields
.field mActivity:Lcom/mediatek/voiceunlock/PswPreview;

.field mIsPrepared:Z


# direct methods
.method private constructor <init>()V
    .locals 1

    invoke-direct {p0}, Landroid/media/MediaPlayer;-><init>()V

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/mediatek/voiceunlock/PswPreview$PreviewPlayer;->mIsPrepared:Z

    return-void
.end method

.method synthetic constructor <init>(Lcom/mediatek/voiceunlock/PswPreview$1;)V
    .locals 0
    .param p1    # Lcom/mediatek/voiceunlock/PswPreview$1;

    invoke-direct {p0}, Lcom/mediatek/voiceunlock/PswPreview$PreviewPlayer;-><init>()V

    return-void
.end method


# virtual methods
.method isPrepared()Z
    .locals 1

    iget-boolean v0, p0, Lcom/mediatek/voiceunlock/PswPreview$PreviewPlayer;->mIsPrepared:Z

    return v0
.end method

.method public onPrepared(Landroid/media/MediaPlayer;)V
    .locals 1
    .param p1    # Landroid/media/MediaPlayer;

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/mediatek/voiceunlock/PswPreview$PreviewPlayer;->mIsPrepared:Z

    iget-object v0, p0, Lcom/mediatek/voiceunlock/PswPreview$PreviewPlayer;->mActivity:Lcom/mediatek/voiceunlock/PswPreview;

    invoke-virtual {v0, p1}, Lcom/mediatek/voiceunlock/PswPreview;->onPrepared(Landroid/media/MediaPlayer;)V

    return-void
.end method

.method public setActivity(Lcom/mediatek/voiceunlock/PswPreview;)V
    .locals 1
    .param p1    # Lcom/mediatek/voiceunlock/PswPreview;

    iput-object p1, p0, Lcom/mediatek/voiceunlock/PswPreview$PreviewPlayer;->mActivity:Lcom/mediatek/voiceunlock/PswPreview;

    invoke-virtual {p0, p0}, Landroid/media/MediaPlayer;->setOnPreparedListener(Landroid/media/MediaPlayer$OnPreparedListener;)V

    iget-object v0, p0, Lcom/mediatek/voiceunlock/PswPreview$PreviewPlayer;->mActivity:Lcom/mediatek/voiceunlock/PswPreview;

    invoke-virtual {p0, v0}, Landroid/media/MediaPlayer;->setOnErrorListener(Landroid/media/MediaPlayer$OnErrorListener;)V

    iget-object v0, p0, Lcom/mediatek/voiceunlock/PswPreview$PreviewPlayer;->mActivity:Lcom/mediatek/voiceunlock/PswPreview;

    invoke-virtual {p0, v0}, Landroid/media/MediaPlayer;->setOnCompletionListener(Landroid/media/MediaPlayer$OnCompletionListener;)V

    return-void
.end method

.method public setDataSourceAndPrepare(Landroid/net/Uri;)V
    .locals 1
    .param p1    # Landroid/net/Uri;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/IllegalArgumentException;,
            Ljava/lang/SecurityException;,
            Ljava/lang/IllegalStateException;,
            Ljava/io/IOException;
        }
    .end annotation

    iget-object v0, p0, Lcom/mediatek/voiceunlock/PswPreview$PreviewPlayer;->mActivity:Lcom/mediatek/voiceunlock/PswPreview;

    invoke-virtual {p0, v0, p1}, Landroid/media/MediaPlayer;->setDataSource(Landroid/content/Context;Landroid/net/Uri;)V

    invoke-virtual {p0}, Landroid/media/MediaPlayer;->prepareAsync()V

    return-void
.end method
