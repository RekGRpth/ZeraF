.class public Lcom/mediatek/voiceunlock/PswPreview;
.super Landroid/app/Activity;
.source "PswPreview.java"

# interfaces
.implements Landroid/media/MediaPlayer$OnCompletionListener;
.implements Landroid/media/MediaPlayer$OnErrorListener;
.implements Landroid/media/MediaPlayer$OnPreparedListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/mediatek/voiceunlock/PswPreview$ProgressRefresher;,
        Lcom/mediatek/voiceunlock/PswPreview$PreviewPlayer;
    }
.end annotation


# instance fields
.field private mAudioFocusListener:Landroid/media/AudioManager$OnAudioFocusChangeListener;

.field private mAudioManager:Landroid/media/AudioManager;

.field private mDuration:I

.field private mIsComplete:Z

.field private mPauseRefreshingProgressBar:Z

.field private mPausedByTransientLossOfFocus:Z

.field private mPlayer:Lcom/mediatek/voiceunlock/PswPreview$PreviewPlayer;

.field private mProgressRefresher:Landroid/os/Handler;

.field private mSeekBar:Landroid/widget/SeekBar;

.field private mSeekListener:Landroid/widget/SeekBar$OnSeekBarChangeListener;

.field private mSeeking:Z

.field private mTitle:Ljava/lang/CharSequence;

.field private mTitleView:Landroid/widget/TextView;

.field private mUri:Landroid/net/Uri;


# direct methods
.method public constructor <init>()V
    .locals 2

    const/4 v1, 0x0

    invoke-direct {p0}, Landroid/app/Activity;-><init>()V

    iput-boolean v1, p0, Lcom/mediatek/voiceunlock/PswPreview;->mSeeking:Z

    const/4 v0, -0x1

    iput v0, p0, Lcom/mediatek/voiceunlock/PswPreview;->mDuration:I

    iput-boolean v1, p0, Lcom/mediatek/voiceunlock/PswPreview;->mIsComplete:Z

    iput-boolean v1, p0, Lcom/mediatek/voiceunlock/PswPreview;->mPauseRefreshingProgressBar:Z

    new-instance v0, Lcom/mediatek/voiceunlock/PswPreview$1;

    invoke-direct {v0, p0}, Lcom/mediatek/voiceunlock/PswPreview$1;-><init>(Lcom/mediatek/voiceunlock/PswPreview;)V

    iput-object v0, p0, Lcom/mediatek/voiceunlock/PswPreview;->mSeekListener:Landroid/widget/SeekBar$OnSeekBarChangeListener;

    new-instance v0, Lcom/mediatek/voiceunlock/PswPreview$2;

    invoke-direct {v0, p0}, Lcom/mediatek/voiceunlock/PswPreview$2;-><init>(Lcom/mediatek/voiceunlock/PswPreview;)V

    iput-object v0, p0, Lcom/mediatek/voiceunlock/PswPreview;->mAudioFocusListener:Landroid/media/AudioManager$OnAudioFocusChangeListener;

    return-void
.end method

.method static synthetic access$100(Lcom/mediatek/voiceunlock/PswPreview;)Z
    .locals 1
    .param p0    # Lcom/mediatek/voiceunlock/PswPreview;

    iget-boolean v0, p0, Lcom/mediatek/voiceunlock/PswPreview;->mSeeking:Z

    return v0
.end method

.method static synthetic access$1000(Lcom/mediatek/voiceunlock/PswPreview;)Landroid/widget/SeekBar;
    .locals 1
    .param p0    # Lcom/mediatek/voiceunlock/PswPreview;

    iget-object v0, p0, Lcom/mediatek/voiceunlock/PswPreview;->mSeekBar:Landroid/widget/SeekBar;

    return-object v0
.end method

.method static synthetic access$102(Lcom/mediatek/voiceunlock/PswPreview;Z)Z
    .locals 0
    .param p0    # Lcom/mediatek/voiceunlock/PswPreview;
    .param p1    # Z

    iput-boolean p1, p0, Lcom/mediatek/voiceunlock/PswPreview;->mSeeking:Z

    return p1
.end method

.method static synthetic access$1100(Lcom/mediatek/voiceunlock/PswPreview;)Landroid/os/Handler;
    .locals 1
    .param p0    # Lcom/mediatek/voiceunlock/PswPreview;

    iget-object v0, p0, Lcom/mediatek/voiceunlock/PswPreview;->mProgressRefresher:Landroid/os/Handler;

    return-object v0
.end method

.method static synthetic access$1200(Lcom/mediatek/voiceunlock/PswPreview;)Z
    .locals 1
    .param p0    # Lcom/mediatek/voiceunlock/PswPreview;

    iget-boolean v0, p0, Lcom/mediatek/voiceunlock/PswPreview;->mPauseRefreshingProgressBar:Z

    return v0
.end method

.method static synthetic access$200(Lcom/mediatek/voiceunlock/PswPreview;)Lcom/mediatek/voiceunlock/PswPreview$PreviewPlayer;
    .locals 1
    .param p0    # Lcom/mediatek/voiceunlock/PswPreview;

    iget-object v0, p0, Lcom/mediatek/voiceunlock/PswPreview;->mPlayer:Lcom/mediatek/voiceunlock/PswPreview$PreviewPlayer;

    return-object v0
.end method

.method static synthetic access$300(Lcom/mediatek/voiceunlock/PswPreview;)Z
    .locals 1
    .param p0    # Lcom/mediatek/voiceunlock/PswPreview;

    iget-boolean v0, p0, Lcom/mediatek/voiceunlock/PswPreview;->mIsComplete:Z

    return v0
.end method

.method static synthetic access$302(Lcom/mediatek/voiceunlock/PswPreview;Z)Z
    .locals 0
    .param p0    # Lcom/mediatek/voiceunlock/PswPreview;
    .param p1    # Z

    iput-boolean p1, p0, Lcom/mediatek/voiceunlock/PswPreview;->mIsComplete:Z

    return p1
.end method

.method static synthetic access$400(Lcom/mediatek/voiceunlock/PswPreview;)Landroid/media/AudioManager;
    .locals 1
    .param p0    # Lcom/mediatek/voiceunlock/PswPreview;

    iget-object v0, p0, Lcom/mediatek/voiceunlock/PswPreview;->mAudioManager:Landroid/media/AudioManager;

    return-object v0
.end method

.method static synthetic access$500(Lcom/mediatek/voiceunlock/PswPreview;)Z
    .locals 1
    .param p0    # Lcom/mediatek/voiceunlock/PswPreview;

    iget-boolean v0, p0, Lcom/mediatek/voiceunlock/PswPreview;->mPausedByTransientLossOfFocus:Z

    return v0
.end method

.method static synthetic access$502(Lcom/mediatek/voiceunlock/PswPreview;Z)Z
    .locals 0
    .param p0    # Lcom/mediatek/voiceunlock/PswPreview;
    .param p1    # Z

    iput-boolean p1, p0, Lcom/mediatek/voiceunlock/PswPreview;->mPausedByTransientLossOfFocus:Z

    return p1
.end method

.method static synthetic access$600(Lcom/mediatek/voiceunlock/PswPreview;)V
    .locals 0
    .param p0    # Lcom/mediatek/voiceunlock/PswPreview;

    invoke-direct {p0}, Lcom/mediatek/voiceunlock/PswPreview;->start()V

    return-void
.end method

.method static synthetic access$700(Lcom/mediatek/voiceunlock/PswPreview;)V
    .locals 0
    .param p0    # Lcom/mediatek/voiceunlock/PswPreview;

    invoke-direct {p0}, Lcom/mediatek/voiceunlock/PswPreview;->updatePlayPause()V

    return-void
.end method

.method static synthetic access$800(Lcom/mediatek/voiceunlock/PswPreview;)I
    .locals 1
    .param p0    # Lcom/mediatek/voiceunlock/PswPreview;

    iget v0, p0, Lcom/mediatek/voiceunlock/PswPreview;->mDuration:I

    return v0
.end method

.method static synthetic access$900(Lcom/mediatek/voiceunlock/PswPreview;Ljava/lang/String;)V
    .locals 0
    .param p0    # Lcom/mediatek/voiceunlock/PswPreview;
    .param p1    # Ljava/lang/String;

    invoke-direct {p0, p1}, Lcom/mediatek/voiceunlock/PswPreview;->log(Ljava/lang/String;)V

    return-void
.end method

.method private log(Ljava/lang/String;)V
    .locals 3
    .param p1    # Ljava/lang/String;

    const-string v0, "VoiceUnlockSetting"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "PswPreview: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/mediatek/xlog/Xlog;->d(Ljava/lang/String;Ljava/lang/String;)I

    return-void
.end method

.method private showPostPrepareUI()V
    .locals 5

    const/4 v3, 0x0

    iget-object v1, p0, Lcom/mediatek/voiceunlock/PswPreview;->mPlayer:Lcom/mediatek/voiceunlock/PswPreview$PreviewPlayer;

    invoke-virtual {v1}, Landroid/media/MediaPlayer;->getDuration()I

    move-result v1

    iput v1, p0, Lcom/mediatek/voiceunlock/PswPreview;->mDuration:I

    iget v1, p0, Lcom/mediatek/voiceunlock/PswPreview;->mDuration:I

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/mediatek/voiceunlock/PswPreview;->mSeekBar:Landroid/widget/SeekBar;

    iget v2, p0, Lcom/mediatek/voiceunlock/PswPreview;->mDuration:I

    invoke-virtual {v1, v2}, Landroid/widget/AbsSeekBar;->setMax(I)V

    iget-object v1, p0, Lcom/mediatek/voiceunlock/PswPreview;->mSeekBar:Landroid/widget/SeekBar;

    invoke-virtual {v1, v3}, Landroid/widget/ProgressBar;->setVisibility(I)V

    :cond_0
    iget-object v1, p0, Lcom/mediatek/voiceunlock/PswPreview;->mSeekBar:Landroid/widget/SeekBar;

    iget-object v2, p0, Lcom/mediatek/voiceunlock/PswPreview;->mSeekListener:Landroid/widget/SeekBar$OnSeekBarChangeListener;

    invoke-virtual {v1, v2}, Landroid/widget/SeekBar;->setOnSeekBarChangeListener(Landroid/widget/SeekBar$OnSeekBarChangeListener;)V

    iget-object v1, p0, Lcom/mediatek/voiceunlock/PswPreview;->mSeekBar:Landroid/widget/SeekBar;

    invoke-virtual {v1}, Landroid/view/View;->isInTouchMode()Z

    move-result v1

    if-nez v1, :cond_1

    iget-object v1, p0, Lcom/mediatek/voiceunlock/PswPreview;->mSeekBar:Landroid/widget/SeekBar;

    invoke-virtual {v1}, Landroid/view/View;->requestFocus()Z

    :cond_1
    const v1, 0x7f0a0001

    invoke-virtual {p0, v1}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v3}, Landroid/view/View;->setVisibility(I)V

    iget-object v1, p0, Lcom/mediatek/voiceunlock/PswPreview;->mAudioManager:Landroid/media/AudioManager;

    iget-object v2, p0, Lcom/mediatek/voiceunlock/PswPreview;->mAudioFocusListener:Landroid/media/AudioManager$OnAudioFocusChangeListener;

    const/4 v3, 0x3

    const/4 v4, 0x2

    invoke-virtual {v1, v2, v3, v4}, Landroid/media/AudioManager;->requestAudioFocus(Landroid/media/AudioManager$OnAudioFocusChangeListener;II)I

    iget-object v1, p0, Lcom/mediatek/voiceunlock/PswPreview;->mProgressRefresher:Landroid/os/Handler;

    new-instance v2, Lcom/mediatek/voiceunlock/PswPreview$ProgressRefresher;

    invoke-direct {v2, p0}, Lcom/mediatek/voiceunlock/PswPreview$ProgressRefresher;-><init>(Lcom/mediatek/voiceunlock/PswPreview;)V

    const-wide/16 v3, 0xc8

    invoke-virtual {v1, v2, v3, v4}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    invoke-direct {p0}, Lcom/mediatek/voiceunlock/PswPreview;->updatePlayPause()V

    return-void
.end method

.method private start()V
    .locals 4

    iget-object v0, p0, Lcom/mediatek/voiceunlock/PswPreview;->mAudioManager:Landroid/media/AudioManager;

    iget-object v1, p0, Lcom/mediatek/voiceunlock/PswPreview;->mAudioFocusListener:Landroid/media/AudioManager$OnAudioFocusChangeListener;

    const/4 v2, 0x3

    const/4 v3, 0x2

    invoke-virtual {v0, v1, v2, v3}, Landroid/media/AudioManager;->requestAudioFocus(Landroid/media/AudioManager$OnAudioFocusChangeListener;II)I

    iget-object v0, p0, Lcom/mediatek/voiceunlock/PswPreview;->mPlayer:Lcom/mediatek/voiceunlock/PswPreview$PreviewPlayer;

    invoke-virtual {v0}, Landroid/media/MediaPlayer;->start()V

    iget-object v0, p0, Lcom/mediatek/voiceunlock/PswPreview;->mProgressRefresher:Landroid/os/Handler;

    new-instance v1, Lcom/mediatek/voiceunlock/PswPreview$ProgressRefresher;

    invoke-direct {v1, p0}, Lcom/mediatek/voiceunlock/PswPreview$ProgressRefresher;-><init>(Lcom/mediatek/voiceunlock/PswPreview;)V

    const-wide/16 v2, 0xc8

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    return-void
.end method

.method private stopPlayback()V
    .locals 2

    const/4 v1, 0x0

    iget-object v0, p0, Lcom/mediatek/voiceunlock/PswPreview;->mProgressRefresher:Landroid/os/Handler;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/mediatek/voiceunlock/PswPreview;->mProgressRefresher:Landroid/os/Handler;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacksAndMessages(Ljava/lang/Object;)V

    :cond_0
    iget-object v0, p0, Lcom/mediatek/voiceunlock/PswPreview;->mPlayer:Lcom/mediatek/voiceunlock/PswPreview$PreviewPlayer;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/mediatek/voiceunlock/PswPreview;->mPlayer:Lcom/mediatek/voiceunlock/PswPreview$PreviewPlayer;

    invoke-virtual {v0}, Landroid/media/MediaPlayer;->release()V

    iput-object v1, p0, Lcom/mediatek/voiceunlock/PswPreview;->mPlayer:Lcom/mediatek/voiceunlock/PswPreview$PreviewPlayer;

    iget-object v0, p0, Lcom/mediatek/voiceunlock/PswPreview;->mAudioManager:Landroid/media/AudioManager;

    iget-object v1, p0, Lcom/mediatek/voiceunlock/PswPreview;->mAudioFocusListener:Landroid/media/AudioManager$OnAudioFocusChangeListener;

    invoke-virtual {v0, v1}, Landroid/media/AudioManager;->abandonAudioFocus(Landroid/media/AudioManager$OnAudioFocusChangeListener;)I

    :cond_1
    return-void
.end method

.method private updatePlayPause()V
    .locals 3

    const v1, 0x7f0a0003

    invoke-virtual {p0, v1}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageButton;

    if-eqz v0, :cond_0

    iget-object v1, p0, Lcom/mediatek/voiceunlock/PswPreview;->mPlayer:Lcom/mediatek/voiceunlock/PswPreview$PreviewPlayer;

    invoke-virtual {v1}, Landroid/media/MediaPlayer;->isPlaying()Z

    move-result v1

    if-eqz v1, :cond_1

    const/high16 v1, 0x7f020000

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    :cond_0
    :goto_0
    return-void

    :cond_1
    const v1, 0x7f020001

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    iget-object v1, p0, Lcom/mediatek/voiceunlock/PswPreview;->mProgressRefresher:Landroid/os/Handler;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Landroid/os/Handler;->removeCallbacksAndMessages(Ljava/lang/Object;)V

    goto :goto_0
.end method


# virtual methods
.method public onCompletion(Landroid/media/MediaPlayer;)V
    .locals 2
    .param p1    # Landroid/media/MediaPlayer;

    invoke-direct {p0}, Lcom/mediatek/voiceunlock/PswPreview;->updatePlayPause()V

    iget-object v0, p0, Lcom/mediatek/voiceunlock/PswPreview;->mSeekBar:Landroid/widget/SeekBar;

    iget-object v1, p0, Lcom/mediatek/voiceunlock/PswPreview;->mSeekBar:Landroid/widget/SeekBar;

    invoke-virtual {v1}, Landroid/widget/ProgressBar;->getMax()I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/ProgressBar;->setProgress(I)V

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/mediatek/voiceunlock/PswPreview;->mIsComplete:Z

    return-void
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 5
    .param p1    # Landroid/os/Bundle;

    invoke-super {p0, p1}, Landroid/app/Activity;->onCreate(Landroid/os/Bundle;)V

    invoke-virtual {p0}, Landroid/app/Activity;->getIntent()Landroid/content/Intent;

    move-result-object v1

    if-nez v1, :cond_1

    invoke-virtual {p0}, Landroid/app/Activity;->finish()V

    :cond_0
    :goto_0
    return-void

    :cond_1
    invoke-virtual {v1}, Landroid/content/Intent;->getData()Landroid/net/Uri;

    move-result-object v3

    iput-object v3, p0, Lcom/mediatek/voiceunlock/PswPreview;->mUri:Landroid/net/Uri;

    iget-object v3, p0, Lcom/mediatek/voiceunlock/PswPreview;->mUri:Landroid/net/Uri;

    if-nez v3, :cond_2

    invoke-virtual {p0}, Landroid/app/Activity;->finish()V

    goto :goto_0

    :cond_2
    const-string v3, "command_summary"

    invoke-virtual {v1, v3}, Landroid/content/Intent;->getCharSequenceExtra(Ljava/lang/String;)Ljava/lang/CharSequence;

    move-result-object v3

    iput-object v3, p0, Lcom/mediatek/voiceunlock/PswPreview;->mTitle:Ljava/lang/CharSequence;

    const/4 v3, 0x3

    invoke-virtual {p0, v3}, Landroid/app/Activity;->setVolumeControlStream(I)V

    const/4 v3, 0x1

    invoke-virtual {p0, v3}, Landroid/app/Activity;->requestWindowFeature(I)Z

    const/high16 v3, 0x7f040000

    invoke-virtual {p0, v3}, Landroid/app/Activity;->setContentView(I)V

    const v3, 0x7f0a0002

    invoke-virtual {p0, v3}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/TextView;

    iput-object v3, p0, Lcom/mediatek/voiceunlock/PswPreview;->mTitleView:Landroid/widget/TextView;

    const/high16 v3, 0x7f0a0000

    invoke-virtual {p0, v3}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/SeekBar;

    iput-object v3, p0, Lcom/mediatek/voiceunlock/PswPreview;->mSeekBar:Landroid/widget/SeekBar;

    new-instance v3, Landroid/os/Handler;

    invoke-direct {v3}, Landroid/os/Handler;-><init>()V

    iput-object v3, p0, Lcom/mediatek/voiceunlock/PswPreview;->mProgressRefresher:Landroid/os/Handler;

    const-string v3, "audio"

    invoke-virtual {p0, v3}, Landroid/app/Activity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/media/AudioManager;

    iput-object v3, p0, Lcom/mediatek/voiceunlock/PswPreview;->mAudioManager:Landroid/media/AudioManager;

    invoke-virtual {p0}, Landroid/app/Activity;->getLastNonConfigurationInstance()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/mediatek/voiceunlock/PswPreview$PreviewPlayer;

    if-nez v2, :cond_3

    new-instance v3, Lcom/mediatek/voiceunlock/PswPreview$PreviewPlayer;

    const/4 v4, 0x0

    invoke-direct {v3, v4}, Lcom/mediatek/voiceunlock/PswPreview$PreviewPlayer;-><init>(Lcom/mediatek/voiceunlock/PswPreview$1;)V

    iput-object v3, p0, Lcom/mediatek/voiceunlock/PswPreview;->mPlayer:Lcom/mediatek/voiceunlock/PswPreview$PreviewPlayer;

    iget-object v3, p0, Lcom/mediatek/voiceunlock/PswPreview;->mPlayer:Lcom/mediatek/voiceunlock/PswPreview$PreviewPlayer;

    invoke-virtual {v3, p0}, Lcom/mediatek/voiceunlock/PswPreview$PreviewPlayer;->setActivity(Lcom/mediatek/voiceunlock/PswPreview;)V

    :try_start_0
    iget-object v3, p0, Lcom/mediatek/voiceunlock/PswPreview;->mPlayer:Lcom/mediatek/voiceunlock/PswPreview$PreviewPlayer;

    iget-object v4, p0, Lcom/mediatek/voiceunlock/PswPreview;->mUri:Landroid/net/Uri;

    invoke-virtual {v3, v4}, Lcom/mediatek/voiceunlock/PswPreview$PreviewPlayer;->setDataSourceAndPrepare(Landroid/net/Uri;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v0

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Failed to open file: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-direct {p0, v3}, Lcom/mediatek/voiceunlock/PswPreview;->log(Ljava/lang/String;)V

    const v3, 0x7f080005

    const/4 v4, 0x0

    invoke-static {p0, v3, v4}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v3

    invoke-virtual {v3}, Landroid/widget/Toast;->show()V

    invoke-virtual {p0}, Landroid/app/Activity;->finish()V

    goto/16 :goto_0

    :cond_3
    iput-object v2, p0, Lcom/mediatek/voiceunlock/PswPreview;->mPlayer:Lcom/mediatek/voiceunlock/PswPreview$PreviewPlayer;

    iget-object v3, p0, Lcom/mediatek/voiceunlock/PswPreview;->mPlayer:Lcom/mediatek/voiceunlock/PswPreview$PreviewPlayer;

    invoke-virtual {v3, p0}, Lcom/mediatek/voiceunlock/PswPreview$PreviewPlayer;->setActivity(Lcom/mediatek/voiceunlock/PswPreview;)V

    iget-object v3, p0, Lcom/mediatek/voiceunlock/PswPreview;->mPlayer:Lcom/mediatek/voiceunlock/PswPreview$PreviewPlayer;

    invoke-virtual {v3}, Lcom/mediatek/voiceunlock/PswPreview$PreviewPlayer;->isPrepared()Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-direct {p0}, Lcom/mediatek/voiceunlock/PswPreview;->showPostPrepareUI()V

    goto/16 :goto_0
.end method

.method public onDestroy()V
    .locals 0

    invoke-direct {p0}, Lcom/mediatek/voiceunlock/PswPreview;->stopPlayback()V

    invoke-super {p0}, Landroid/app/Activity;->onDestroy()V

    return-void
.end method

.method public onDurationUpdate(Landroid/media/MediaPlayer;I)V
    .locals 2
    .param p1    # Landroid/media/MediaPlayer;
    .param p2    # I

    if-lez p2, :cond_0

    iput p2, p0, Lcom/mediatek/voiceunlock/PswPreview;->mDuration:I

    iget-object v0, p0, Lcom/mediatek/voiceunlock/PswPreview;->mSeekBar:Landroid/widget/SeekBar;

    iget v1, p0, Lcom/mediatek/voiceunlock/PswPreview;->mDuration:I

    invoke-virtual {v0, v1}, Landroid/widget/AbsSeekBar;->setMax(I)V

    :cond_0
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "onDurationUpdate("

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/mediatek/voiceunlock/PswPreview;->mDuration:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/mediatek/voiceunlock/PswPreview;->log(Ljava/lang/String;)V

    return-void
.end method

.method public onError(Landroid/media/MediaPlayer;II)Z
    .locals 2
    .param p1    # Landroid/media/MediaPlayer;
    .param p2    # I
    .param p3    # I

    const v0, 0x7f080005

    const/4 v1, 0x0

    invoke-static {p0, v0, v1}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    invoke-virtual {p0}, Landroid/app/Activity;->finish()V

    const/4 v0, 0x1

    return v0
.end method

.method public onPause()V
    .locals 2

    const-string v0, "onPause for stop ProgressRefresher!"

    invoke-direct {p0, v0}, Lcom/mediatek/voiceunlock/PswPreview;->log(Ljava/lang/String;)V

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/mediatek/voiceunlock/PswPreview;->mPauseRefreshingProgressBar:Z

    iget-object v0, p0, Lcom/mediatek/voiceunlock/PswPreview;->mProgressRefresher:Landroid/os/Handler;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacksAndMessages(Ljava/lang/Object;)V

    invoke-super {p0}, Landroid/app/Activity;->onPause()V

    return-void
.end method

.method public onPrepared(Landroid/media/MediaPlayer;)V
    .locals 1
    .param p1    # Landroid/media/MediaPlayer;

    invoke-virtual {p0}, Landroid/app/Activity;->isFinishing()Z

    move-result v0

    if-eqz v0, :cond_0

    :goto_0
    return-void

    :cond_0
    check-cast p1, Lcom/mediatek/voiceunlock/PswPreview$PreviewPlayer;

    iput-object p1, p0, Lcom/mediatek/voiceunlock/PswPreview;->mPlayer:Lcom/mediatek/voiceunlock/PswPreview$PreviewPlayer;

    invoke-virtual {p0}, Lcom/mediatek/voiceunlock/PswPreview;->setCommandTitle()V

    iget-object v0, p0, Lcom/mediatek/voiceunlock/PswPreview;->mPlayer:Lcom/mediatek/voiceunlock/PswPreview$PreviewPlayer;

    invoke-virtual {v0}, Landroid/media/MediaPlayer;->start()V

    invoke-direct {p0}, Lcom/mediatek/voiceunlock/PswPreview;->showPostPrepareUI()V

    goto :goto_0
.end method

.method public onResume()V
    .locals 5

    invoke-super {p0}, Landroid/app/Activity;->onResume()V

    const/16 v0, 0xc8

    const-string v1, "onResume for start ProgressRefresher!"

    invoke-direct {p0, v1}, Lcom/mediatek/voiceunlock/PswPreview;->log(Ljava/lang/String;)V

    iget-boolean v1, p0, Lcom/mediatek/voiceunlock/PswPreview;->mPauseRefreshingProgressBar:Z

    if-eqz v1, :cond_0

    const/4 v1, 0x0

    iput-boolean v1, p0, Lcom/mediatek/voiceunlock/PswPreview;->mPauseRefreshingProgressBar:Z

    iget-object v1, p0, Lcom/mediatek/voiceunlock/PswPreview;->mProgressRefresher:Landroid/os/Handler;

    new-instance v2, Lcom/mediatek/voiceunlock/PswPreview$ProgressRefresher;

    invoke-direct {v2, p0}, Lcom/mediatek/voiceunlock/PswPreview$ProgressRefresher;-><init>(Lcom/mediatek/voiceunlock/PswPreview;)V

    const-wide/16 v3, 0xc8

    invoke-virtual {v1, v2, v3, v4}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    :cond_0
    return-void
.end method

.method public onRetainNonConfigurationInstance()Ljava/lang/Object;
    .locals 2

    iget-object v0, p0, Lcom/mediatek/voiceunlock/PswPreview;->mPlayer:Lcom/mediatek/voiceunlock/PswPreview$PreviewPlayer;

    const/4 v1, 0x0

    iput-object v1, p0, Lcom/mediatek/voiceunlock/PswPreview;->mPlayer:Lcom/mediatek/voiceunlock/PswPreview$PreviewPlayer;

    return-object v0
.end method

.method public onUserLeaveHint()V
    .locals 0

    invoke-direct {p0}, Lcom/mediatek/voiceunlock/PswPreview;->stopPlayback()V

    invoke-virtual {p0}, Landroid/app/Activity;->finish()V

    invoke-super {p0}, Landroid/app/Activity;->onUserLeaveHint()V

    return-void
.end method

.method public playPauseClicked(Landroid/view/View;)V
    .locals 1
    .param p1    # Landroid/view/View;

    iget-object v0, p0, Lcom/mediatek/voiceunlock/PswPreview;->mPlayer:Lcom/mediatek/voiceunlock/PswPreview$PreviewPlayer;

    invoke-virtual {v0}, Landroid/media/MediaPlayer;->isPlaying()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/mediatek/voiceunlock/PswPreview;->mPlayer:Lcom/mediatek/voiceunlock/PswPreview$PreviewPlayer;

    invoke-virtual {v0}, Landroid/media/MediaPlayer;->pause()V

    :goto_0
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/mediatek/voiceunlock/PswPreview;->mIsComplete:Z

    invoke-direct {p0}, Lcom/mediatek/voiceunlock/PswPreview;->updatePlayPause()V

    return-void

    :cond_0
    invoke-direct {p0}, Lcom/mediatek/voiceunlock/PswPreview;->start()V

    goto :goto_0
.end method

.method public setCommandTitle()V
    .locals 2

    iget-object v0, p0, Lcom/mediatek/voiceunlock/PswPreview;->mTitleView:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/mediatek/voiceunlock/PswPreview;->mTitle:Ljava/lang/CharSequence;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    return-void
.end method
