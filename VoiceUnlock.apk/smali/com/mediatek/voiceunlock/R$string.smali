.class public final Lcom/mediatek/voiceunlock/R$string;
.super Ljava/lang/Object;
.source "R.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/mediatek/voiceunlock/R;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "string"
.end annotation


# static fields
.field public static final command_summary:I = 0x7f080013

.field public static final during_call_message:I = 0x7f08002c

.field public static final during_call_title:I = 0x7f08002b

.field public static final pass_word_file_missing:I = 0x7f080028

.field public static final playback_failed:I = 0x7f080005

.field public static final reset_command_prompt:I = 0x7f080006

.field public static final reset_command_prompt_last:I = 0x7f080007

.field public static final reset_command_title:I = 0x7f080004

.field public static final stop_playing_message:I = 0x7f08001b

.field public static final stop_playing_title:I = 0x7f08001a

.field public static final time_out_message:I = 0x7f08002a

.field public static final time_out_title:I = 0x7f080029

.field public static final unlock_set_unlock_mode_voice_weak:I = 0x7f080000

.field public static final voice_command1:I = 0x7f080010

.field public static final voice_command2:I = 0x7f080011

.field public static final voice_command3:I = 0x7f080012

.field public static final voice_command_customize:I = 0x7f080015

.field public static final voice_command_launch_app_title:I = 0x7f080003

.field public static final voice_command_primary:I = 0x7f080014

.field public static final voice_command_record_description_command:I = 0x7f080018

.field public static final voice_command_record_description_unlock_screen:I = 0x7f080017

.field public static final voice_command_record_first_recording:I = 0x7f08001e

.field public static final voice_command_record_introdution:I = 0x7f08001c

.field public static final voice_command_record_non_first_recording:I = 0x7f08001f

.field public static final voice_command_record_one_round_diff:I = 0x7f080023

.field public static final voice_command_record_one_round_exist:I = 0x7f080024

.field public static final voice_command_record_one_round_noisy:I = 0x7f080021

.field public static final voice_command_record_one_round_ok:I = 0x7f080020

.field public static final voice_command_record_one_round_weak:I = 0x7f080022

.field public static final voice_command_record_prepare:I = 0x7f08001d

.field public static final voice_command_record_recording_ok:I = 0x7f080025

.field public static final voice_command_secondary:I = 0x7f080016

.field public static final voice_command_select_title:I = 0x7f080002

.field public static final voice_service_error_title:I = 0x7f080019

.field public static final voice_unlock_cancel_label:I = 0x7f08000a

.field public static final voice_unlock_command:I = 0x7f08000f

.field public static final voice_unlock_continue_label:I = 0x7f08000d

.field public static final voice_unlock_ok_label:I = 0x7f080009

.field public static final voice_unlock_options_title:I = 0x7f080008

.field public static final voice_unlock_record_label:I = 0x7f08000c

.field public static final voice_unlock_retry_label:I = 0x7f08000b

.field public static final voice_unlock_setup_end_all_done:I = 0x7f080026

.field public static final voice_unlock_setup_end_prompt:I = 0x7f080027

.field public static final voice_unlock_setup_intro:I = 0x7f08000e

.field public static final voice_unlock_setup_intro_header:I = 0x7f080001


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
