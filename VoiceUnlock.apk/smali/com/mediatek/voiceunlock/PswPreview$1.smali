.class Lcom/mediatek/voiceunlock/PswPreview$1;
.super Ljava/lang/Object;
.source "PswPreview.java"

# interfaces
.implements Landroid/widget/SeekBar$OnSeekBarChangeListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/mediatek/voiceunlock/PswPreview;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/mediatek/voiceunlock/PswPreview;


# direct methods
.method constructor <init>(Lcom/mediatek/voiceunlock/PswPreview;)V
    .locals 0

    iput-object p1, p0, Lcom/mediatek/voiceunlock/PswPreview$1;->this$0:Lcom/mediatek/voiceunlock/PswPreview;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onProgressChanged(Landroid/widget/SeekBar;IZ)V
    .locals 1
    .param p1    # Landroid/widget/SeekBar;
    .param p2    # I
    .param p3    # Z

    if-nez p3, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v0, p0, Lcom/mediatek/voiceunlock/PswPreview$1;->this$0:Lcom/mediatek/voiceunlock/PswPreview;

    invoke-static {v0}, Lcom/mediatek/voiceunlock/PswPreview;->access$100(Lcom/mediatek/voiceunlock/PswPreview;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/mediatek/voiceunlock/PswPreview$1;->this$0:Lcom/mediatek/voiceunlock/PswPreview;

    invoke-static {v0}, Lcom/mediatek/voiceunlock/PswPreview;->access$200(Lcom/mediatek/voiceunlock/PswPreview;)Lcom/mediatek/voiceunlock/PswPreview$PreviewPlayer;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/mediatek/voiceunlock/PswPreview$1;->this$0:Lcom/mediatek/voiceunlock/PswPreview;

    invoke-static {v0}, Lcom/mediatek/voiceunlock/PswPreview;->access$200(Lcom/mediatek/voiceunlock/PswPreview;)Lcom/mediatek/voiceunlock/PswPreview$PreviewPlayer;

    move-result-object v0

    invoke-virtual {v0, p2}, Landroid/media/MediaPlayer;->seekTo(I)V

    goto :goto_0
.end method

.method public onStartTrackingTouch(Landroid/widget/SeekBar;)V
    .locals 2
    .param p1    # Landroid/widget/SeekBar;

    iget-object v0, p0, Lcom/mediatek/voiceunlock/PswPreview$1;->this$0:Lcom/mediatek/voiceunlock/PswPreview;

    const/4 v1, 0x1

    invoke-static {v0, v1}, Lcom/mediatek/voiceunlock/PswPreview;->access$102(Lcom/mediatek/voiceunlock/PswPreview;Z)Z

    return-void
.end method

.method public onStopTrackingTouch(Landroid/widget/SeekBar;)V
    .locals 3
    .param p1    # Landroid/widget/SeekBar;

    const/4 v2, 0x0

    iget-object v0, p0, Lcom/mediatek/voiceunlock/PswPreview$1;->this$0:Lcom/mediatek/voiceunlock/PswPreview;

    invoke-static {v0}, Lcom/mediatek/voiceunlock/PswPreview;->access$200(Lcom/mediatek/voiceunlock/PswPreview;)Lcom/mediatek/voiceunlock/PswPreview$PreviewPlayer;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/mediatek/voiceunlock/PswPreview$1;->this$0:Lcom/mediatek/voiceunlock/PswPreview;

    invoke-static {v0}, Lcom/mediatek/voiceunlock/PswPreview;->access$200(Lcom/mediatek/voiceunlock/PswPreview;)Lcom/mediatek/voiceunlock/PswPreview$PreviewPlayer;

    move-result-object v0

    invoke-virtual {p1}, Landroid/widget/ProgressBar;->getProgress()I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/media/MediaPlayer;->seekTo(I)V

    :cond_0
    iget-object v0, p0, Lcom/mediatek/voiceunlock/PswPreview$1;->this$0:Lcom/mediatek/voiceunlock/PswPreview;

    invoke-static {v0, v2}, Lcom/mediatek/voiceunlock/PswPreview;->access$102(Lcom/mediatek/voiceunlock/PswPreview;Z)Z

    iget-object v0, p0, Lcom/mediatek/voiceunlock/PswPreview$1;->this$0:Lcom/mediatek/voiceunlock/PswPreview;

    invoke-static {v0, v2}, Lcom/mediatek/voiceunlock/PswPreview;->access$302(Lcom/mediatek/voiceunlock/PswPreview;Z)Z

    return-void
.end method
