.class public Lcom/google/android/gm/provider/Label;
.super Ljava/lang/Object;
.source "Label.java"


# static fields
.field private static final LABEL_COMPONENT_SEPARATOR_PATTERN:Ljava/util/regex/Pattern;

.field private static sLabelRequeryDelayMs:I

.field private static sSystemLabelCache:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/CharSequence;",
            ">;"
        }
    .end annotation
.end field

.field private static sSystemLabelCacheLock:Ljava/lang/Object;


# instance fields
.field private final mAccount:Ljava/lang/String;

.field private mCanonicalName:Ljava/lang/String;

.field private mColor:Ljava/lang/String;

.field private mCountsInitialized:Z

.field private final mDataSetObservable:Landroid/database/DataSetObservable;

.field private final mFactorySystemLabelMap:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/CharSequence;",
            ">;"
        }
    .end annotation
.end field

.field private final mId:J

.field private final mIsHidden:Z

.field private mIsSystemLabel:Z

.field private mLabelCountBehavior:I

.field private mLabelSyncPolicy:I

.field private mLastTouched:J

.field private mName:Ljava/lang/String;

.field private mNumConversations:I

.field private mNumUnreadConversations:I

.field private mSerializedInfo:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    sput-object v0, Lcom/google/android/gm/provider/Label;->sSystemLabelCacheLock:Ljava/lang/Object;

    const/4 v0, -0x1

    sput v0, Lcom/google/android/gm/provider/Label;->sLabelRequeryDelayMs:I

    const-string v0, "\\^\\*\\^"

    invoke-static {v0}, Ljava/util/regex/Pattern;->compile(Ljava/lang/String;)Ljava/util/regex/Pattern;

    move-result-object v0

    sput-object v0, Lcom/google/android/gm/provider/Label;->LABEL_COMPONENT_SEPARATOR_PATTERN:Ljava/util/regex/Pattern;

    return-void
.end method

.method constructor <init>(Landroid/content/Context;Ljava/lang/String;JLjava/lang/String;Ljava/lang/String;Ljava/lang/String;IIZIIJLjava/util/Map;)V
    .locals 12
    .param p1    # Landroid/content/Context;
    .param p2    # Ljava/lang/String;
    .param p3    # J
    .param p5    # Ljava/lang/String;
    .param p6    # Ljava/lang/String;
    .param p7    # Ljava/lang/String;
    .param p8    # I
    .param p9    # I
    .param p10    # Z
    .param p11    # I
    .param p12    # I
    .param p13    # J
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Ljava/lang/String;",
            "J",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "IIZIIJ",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/CharSequence;",
            ">;)V"
        }
    .end annotation

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v1, 0x0

    iput v1, p0, Lcom/google/android/gm/provider/Label;->mLabelCountBehavior:I

    const/4 v1, 0x0

    iput v1, p0, Lcom/google/android/gm/provider/Label;->mLabelSyncPolicy:I

    new-instance v1, Landroid/database/DataSetObservable;

    invoke-direct {v1}, Landroid/database/DataSetObservable;-><init>()V

    iput-object v1, p0, Lcom/google/android/gm/provider/Label;->mDataSetObservable:Landroid/database/DataSetObservable;

    iput-object p2, p0, Lcom/google/android/gm/provider/Label;->mAccount:Ljava/lang/String;

    iput-wide p3, p0, Lcom/google/android/gm/provider/Label;->mId:J

    move/from16 v0, p10

    iput-boolean v0, p0, Lcom/google/android/gm/provider/Label;->mIsHidden:Z

    move-object/from16 v0, p15

    iput-object v0, p0, Lcom/google/android/gm/provider/Label;->mFactorySystemLabelMap:Ljava/util/Map;

    move-object v1, p0

    move-object v2, p1

    move-object/from16 v3, p5

    move-object/from16 v4, p6

    move-object/from16 v5, p7

    move/from16 v6, p8

    move/from16 v7, p9

    move/from16 v8, p11

    move/from16 v9, p12

    move-wide/from16 v10, p13

    invoke-virtual/range {v1 .. v11}, Lcom/google/android/gm/provider/Label;->loadInternal(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;IIIIJ)V

    return-void
.end method

.method constructor <init>(Landroid/content/Context;Ljava/lang/String;JLjava/lang/String;Ljava/lang/String;Ljava/lang/String;ZLjava/util/Map;)V
    .locals 2
    .param p1    # Landroid/content/Context;
    .param p2    # Ljava/lang/String;
    .param p3    # J
    .param p5    # Ljava/lang/String;
    .param p6    # Ljava/lang/String;
    .param p7    # Ljava/lang/String;
    .param p8    # Z
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Ljava/lang/String;",
            "J",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Z",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/CharSequence;",
            ">;)V"
        }
    .end annotation

    const/4 v1, 0x0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput v1, p0, Lcom/google/android/gm/provider/Label;->mLabelCountBehavior:I

    iput v1, p0, Lcom/google/android/gm/provider/Label;->mLabelSyncPolicy:I

    new-instance v0, Landroid/database/DataSetObservable;

    invoke-direct {v0}, Landroid/database/DataSetObservable;-><init>()V

    iput-object v0, p0, Lcom/google/android/gm/provider/Label;->mDataSetObservable:Landroid/database/DataSetObservable;

    iput-object p2, p0, Lcom/google/android/gm/provider/Label;->mAccount:Ljava/lang/String;

    iput-wide p3, p0, Lcom/google/android/gm/provider/Label;->mId:J

    iput-object p5, p0, Lcom/google/android/gm/provider/Label;->mCanonicalName:Ljava/lang/String;

    invoke-static {p5}, Lcom/google/android/gm/provider/Gmail;->isSystemLabel(Ljava/lang/String;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/google/android/gm/provider/Label;->mIsSystemLabel:Z

    iput-object p9, p0, Lcom/google/android/gm/provider/Label;->mFactorySystemLabelMap:Ljava/util/Map;

    iget-boolean v0, p0, Lcom/google/android/gm/provider/Label;->mIsSystemLabel:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/gm/provider/Label;->mCanonicalName:Ljava/lang/String;

    invoke-virtual {p6, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-direct {p0, p1}, Lcom/google/android/gm/provider/Label;->getHumanSystemLabelName(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gm/provider/Label;->mName:Ljava/lang/String;

    :goto_0
    iput-object p7, p0, Lcom/google/android/gm/provider/Label;->mColor:Ljava/lang/String;

    iput-boolean v1, p0, Lcom/google/android/gm/provider/Label;->mCountsInitialized:Z

    iput-boolean p8, p0, Lcom/google/android/gm/provider/Label;->mIsHidden:Z

    return-void

    :cond_0
    iput-object p6, p0, Lcom/google/android/gm/provider/Label;->mName:Ljava/lang/String;

    goto :goto_0
.end method

.method public static getBackgroundColor(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)I
    .locals 3
    .param p0    # Ljava/lang/String;
    .param p1    # Ljava/lang/String;
    .param p2    # Ljava/lang/String;

    invoke-static {p1}, Lcom/google/android/gm/provider/Gmail;->isSystemLabel(Ljava/lang/String;)Z

    move-result v0

    const-string v2, "^g"

    invoke-virtual {p1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-static {}, Lcom/google/android/gm/utils/LabelColorUtils;->getMutedColorInts()[I

    move-result-object v1

    :goto_0
    if-eqz v0, :cond_1

    const v2, 0xffffff

    :goto_1
    return v2

    :cond_0
    invoke-static {v0, p2}, Lcom/google/android/gm/provider/Label;->getColor(ZLjava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2, p0}, Lcom/google/android/gm/utils/LabelColorUtils;->getLabelColorInts(Ljava/lang/String;Ljava/lang/String;)[I

    move-result-object v1

    goto :goto_0

    :cond_1
    const/4 v2, 0x0

    aget v2, v1, v2

    goto :goto_1
.end method

.method static getColor(ZLjava/lang/String;)Ljava/lang/String;
    .locals 0
    .param p0    # Z
    .param p1    # Ljava/lang/String;

    if-eqz p0, :cond_0

    const-string p1, "2147483647"

    :cond_0
    return-object p1
.end method

.method private getHumanSystemLabelName(Landroid/content/Context;)Ljava/lang/String;
    .locals 5
    .param p1    # Landroid/content/Context;

    const/4 v1, 0x0

    iget-object v2, p0, Lcom/google/android/gm/provider/Label;->mFactorySystemLabelMap:Ljava/util/Map;

    if-eqz v2, :cond_1

    iget-object v2, p0, Lcom/google/android/gm/provider/Label;->mFactorySystemLabelMap:Ljava/util/Map;

    iget-object v3, p0, Lcom/google/android/gm/provider/Label;->mCanonicalName:Ljava/lang/String;

    invoke-interface {v2, v3}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/CharSequence;

    :cond_0
    :goto_0
    if-nez v1, :cond_2

    iget-object v2, p0, Lcom/google/android/gm/provider/Label;->mCanonicalName:Ljava/lang/String;

    :goto_1
    return-object v2

    :cond_1
    if-eqz p1, :cond_0

    sget-object v3, Lcom/google/android/gm/provider/Label;->sSystemLabelCacheLock:Ljava/lang/Object;

    monitor-enter v3

    :try_start_0
    invoke-direct {p0, p1}, Lcom/google/android/gm/provider/Label;->initLabelCache(Landroid/content/Context;)V

    sget-object v2, Lcom/google/android/gm/provider/Label;->sSystemLabelCache:Ljava/util/HashMap;

    iget-object v4, p0, Lcom/google/android/gm/provider/Label;->mCanonicalName:Ljava/lang/String;

    invoke-virtual {v2, v4}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    move-object v0, v2

    check-cast v0, Ljava/lang/CharSequence;

    move-object v1, v0

    monitor-exit v3

    goto :goto_0

    :catchall_0
    move-exception v2

    monitor-exit v3
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v2

    :cond_2
    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v2

    goto :goto_1
.end method

.method static getLabelUri(Ljava/lang/String;Ljava/lang/Long;)Landroid/net/Uri;
    .locals 2
    .param p0    # Ljava/lang/String;
    .param p1    # Ljava/lang/Long;

    invoke-static {p0}, Lcom/google/android/gm/provider/Gmail;->getLabelUri(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    invoke-virtual {p1}, Ljava/lang/Long;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/net/Uri;->withAppendedPath(Landroid/net/Uri;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    return-object v0
.end method

.method static getSystemLabelNameMap(Landroid/content/Context;)Ljava/util/Map;
    .locals 1
    .param p0    # Landroid/content/Context;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            ")",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/CharSequence;",
            ">;"
        }
    .end annotation

    const/4 v0, 0x0

    if-eqz p0, :cond_0

    invoke-static {}, Lcom/google/common/collect/Maps;->newHashMap()Ljava/util/HashMap;

    move-result-object v0

    invoke-static {p0, v0}, Lcom/google/android/gm/provider/Label;->loadSystemLabelNameMap(Landroid/content/Context;Ljava/util/Map;)V

    :cond_0
    return-object v0
.end method

.method public static getTextColor(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)I
    .locals 3
    .param p0    # Ljava/lang/String;
    .param p1    # Ljava/lang/String;
    .param p2    # Ljava/lang/String;

    invoke-static {p1}, Lcom/google/android/gm/provider/Gmail;->isSystemLabel(Ljava/lang/String;)Z

    move-result v0

    const-string v2, "^g"

    invoke-virtual {p1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-static {}, Lcom/google/android/gm/utils/LabelColorUtils;->getMutedColorInts()[I

    move-result-object v1

    :goto_0
    const/4 v2, 0x1

    aget v2, v1, v2

    return v2

    :cond_0
    invoke-static {v0, p2}, Lcom/google/android/gm/provider/Label;->getColor(ZLjava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2, p0}, Lcom/google/android/gm/utils/LabelColorUtils;->getLabelColorInts(Ljava/lang/String;Ljava/lang/String;)[I

    move-result-object v1

    goto :goto_0
.end method

.method private initLabelCache(Landroid/content/Context;)V
    .locals 1
    .param p1    # Landroid/content/Context;

    sget-object v0, Lcom/google/android/gm/provider/Label;->sSystemLabelCache:Ljava/util/HashMap;

    if-nez v0, :cond_0

    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    sput-object v0, Lcom/google/android/gm/provider/Label;->sSystemLabelCache:Ljava/util/HashMap;

    :cond_0
    sget-object v0, Lcom/google/android/gm/provider/Label;->sSystemLabelCache:Ljava/util/HashMap;

    invoke-static {p1, v0}, Lcom/google/android/gm/provider/Label;->loadSystemLabelNameMap(Landroid/content/Context;Ljava/util/Map;)V

    return-void
.end method

.method private static loadSystemLabelNameMap(Landroid/content/Context;Ljava/util/Map;)V
    .locals 2
    .param p0    # Landroid/content/Context;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/CharSequence;",
            ">;)V"
        }
    .end annotation

    const-string v0, "^f"

    const v1, 0x7f0901b5

    invoke-virtual {p0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-interface {p1, v0, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const-string v0, "^^out"

    const v1, 0x7f0901b6

    invoke-virtual {p0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-interface {p1, v0, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const-string v0, "^i"

    const v1, 0x7f0901b7

    invoke-virtual {p0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-interface {p1, v0, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const-string v0, "^r"

    const v1, 0x7f0901b8

    invoke-virtual {p0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-interface {p1, v0, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const-string v0, "^b"

    const v1, 0x7f0901b9

    invoke-virtual {p0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-interface {p1, v0, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const-string v0, "^all"

    const v1, 0x7f0901ba

    invoke-virtual {p0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-interface {p1, v0, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const-string v0, "^u"

    const v1, 0x7f0901bb

    invoke-virtual {p0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-interface {p1, v0, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const-string v0, "^k"

    const v1, 0x7f0901bc

    invoke-virtual {p0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-interface {p1, v0, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const-string v0, "^s"

    const v1, 0x7f0901bd

    invoke-virtual {p0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-interface {p1, v0, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const-string v0, "^t"

    const v1, 0x7f0901be

    invoke-virtual {p0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-interface {p1, v0, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const-string v0, "^g"

    const v1, 0x7f0901bf

    invoke-virtual {p0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-interface {p1, v0, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const-string v0, "^io_im"

    const v1, 0x7f0901c0

    invoke-virtual {p0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-interface {p1, v0, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const-string v0, "^iim"

    const v1, 0x7f0901c1

    invoke-virtual {p0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-interface {p1, v0, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    return-void
.end method

.method public static parseJoinedString(Ljava/lang/String;)Lcom/google/android/gm/provider/Label;
    .locals 1
    .param p0    # Ljava/lang/String;

    const/4 v0, 0x0

    invoke-static {p0, v0}, Lcom/google/android/gm/provider/Label;->parseJoinedString(Ljava/lang/String;Ljava/util/Map;)Lcom/google/android/gm/provider/Label;

    move-result-object v0

    return-object v0
.end method

.method static parseJoinedString(Ljava/lang/String;Ljava/lang/String;Ljava/util/Map;Ljava/util/Map;)Lcom/google/android/gm/provider/Label;
    .locals 20
    .param p0    # Ljava/lang/String;
    .param p1    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/CharSequence;",
            ">;",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Lcom/google/android/gm/provider/Label;",
            ">;)",
            "Lcom/google/android/gm/provider/Label;"
        }
    .end annotation

    const-string v3, "^*^"

    move-object/from16 v0, p1

    invoke-virtual {v0, v3}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v17

    const/4 v3, -0x1

    move/from16 v0, v17

    if-eq v0, v3, :cond_0

    const/4 v3, 0x0

    move-object/from16 v0, p1

    move/from16 v1, v17

    invoke-virtual {v0, v3, v1}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v16

    if-eqz p3, :cond_1

    move-object/from16 v0, p3

    move-object/from16 v1, v16

    invoke-interface {v0, v1}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_1

    move-object/from16 v0, p3

    move-object/from16 v1, v16

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/google/android/gm/provider/Label;

    :goto_0
    return-object v3

    :cond_0
    const-string v3, "Gmail"

    const-string v4, "Problem parsing labelId: original string: %s"

    const/4 v11, 0x1

    new-array v11, v11, [Ljava/lang/Object;

    const/16 v18, 0x0

    aput-object p1, v11, v18

    invoke-static {v3, v4, v11}, Lcom/google/android/gm/provider/LogUtils;->w(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)I

    const/4 v3, 0x0

    goto :goto_0

    :cond_1
    sget-object v3, Lcom/google/android/gm/provider/Label;->LABEL_COMPONENT_SEPARATOR_PATTERN:Ljava/util/regex/Pattern;

    move-object/from16 v0, p1

    invoke-static {v0, v3}, Landroid/text/TextUtils;->split(Ljava/lang/String;Ljava/util/regex/Pattern;)[Ljava/lang/String;

    move-result-object v13

    array-length v3, v13

    const/4 v4, 0x5

    if-ge v3, v4, :cond_2

    const/4 v3, 0x0

    goto :goto_0

    :cond_2
    const/4 v14, 0x0

    :try_start_0
    invoke-static/range {v16 .. v16}, Ljava/lang/Long;->valueOf(Ljava/lang/String;)Ljava/lang/Long;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Long;->longValue()J
    :try_end_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-wide v5

    add-int/lit8 v14, v14, 0x1

    add-int/lit8 v15, v14, 0x1

    aget-object v7, v13, v14

    add-int/lit8 v14, v15, 0x1

    aget-object v8, v13, v15

    add-int/lit8 v15, v14, 0x1

    aget-object v9, v13, v14

    const/4 v10, 0x0

    add-int/lit8 v14, v15, 0x1

    :try_start_1
    aget-object v3, v13, v15

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I
    :try_end_1
    .catch Ljava/lang/NumberFormatException; {:try_start_1 .. :try_end_1} :catch_1

    move-result v3

    if-lez v3, :cond_4

    const/4 v10, 0x1

    :goto_1
    new-instance v2, Lcom/google/android/gm/provider/Label;

    const/4 v3, 0x0

    move-object/from16 v4, p0

    move-object/from16 v11, p2

    invoke-direct/range {v2 .. v11}, Lcom/google/android/gm/provider/Label;-><init>(Landroid/content/Context;Ljava/lang/String;JLjava/lang/String;Ljava/lang/String;Ljava/lang/String;ZLjava/util/Map;)V

    if-eqz p3, :cond_3

    move-object/from16 v0, p3

    move-object/from16 v1, v16

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    :cond_3
    move-object v3, v2

    goto :goto_0

    :catch_0
    move-exception v12

    const-string v3, "Gmail"

    const-string v4, "Problem parsing labelId: %s original string: %s"

    const/4 v11, 0x2

    new-array v11, v11, [Ljava/lang/Object;

    const/16 v18, 0x0

    invoke-virtual {v12}, Ljava/lang/NumberFormatException;->getMessage()Ljava/lang/String;

    move-result-object v19

    aput-object v19, v11, v18

    const/16 v18, 0x1

    aput-object p1, v11, v18

    invoke-static {v3, v4, v11}, Lcom/google/android/gm/provider/LogUtils;->w(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)I

    const/4 v3, 0x0

    goto :goto_0

    :cond_4
    const/4 v10, 0x0

    goto :goto_1

    :catch_1
    move-exception v12

    const-string v3, "Gmail"

    const-string v4, "Problem parsing isHidden: %s original string: %s"

    const/4 v11, 0x2

    new-array v11, v11, [Ljava/lang/Object;

    const/16 v18, 0x0

    invoke-virtual {v12}, Ljava/lang/NumberFormatException;->getMessage()Ljava/lang/String;

    move-result-object v19

    aput-object v19, v11, v18

    const/16 v18, 0x1

    aput-object p1, v11, v18

    invoke-static {v3, v4, v11}, Lcom/google/android/gm/provider/LogUtils;->w(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)I

    const/4 v3, 0x0

    goto/16 :goto_0
.end method

.method static parseJoinedString(Ljava/lang/String;Ljava/util/Map;)Lcom/google/android/gm/provider/Label;
    .locals 20
    .param p0    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/CharSequence;",
            ">;)",
            "Lcom/google/android/gm/provider/Label;"
        }
    .end annotation

    sget-object v2, Lcom/google/android/gm/provider/Label;->LABEL_COMPONENT_SEPARATOR_PATTERN:Ljava/util/regex/Pattern;

    move-object/from16 v0, p0

    invoke-static {v0, v2}, Landroid/text/TextUtils;->split(Ljava/lang/String;Ljava/util/regex/Pattern;)[Ljava/lang/String;

    move-result-object v14

    array-length v2, v14

    const/4 v10, 0x6

    if-ge v2, v10, :cond_0

    const/4 v1, 0x0

    :goto_0
    return-object v1

    :cond_0
    const/4 v15, 0x0

    add-int/lit8 v16, v15, 0x1

    aget-object v13, v14, v15

    const-wide/16 v4, 0x0

    add-int/lit8 v15, v16, 0x1

    :try_start_0
    aget-object v2, v14, v16

    invoke-static {v2}, Ljava/lang/Long;->valueOf(Ljava/lang/String;)Ljava/lang/Long;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Long;->longValue()J
    :try_end_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-wide v4

    add-int/lit8 v16, v15, 0x1

    aget-object v6, v14, v15

    add-int/lit8 v15, v16, 0x1

    aget-object v12, v14, v16

    const-string v7, ""

    const-string v3, ""

    const-string v8, ""

    const/4 v9, 0x0

    :try_start_1
    const-string v2, "UTF-8"

    invoke-static {v12, v2}, Ljava/net/URLDecoder;->decode(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    :try_end_1
    .catch Ljava/lang/IllegalArgumentException; {:try_start_1 .. :try_end_1} :catch_1
    .catch Ljava/io/UnsupportedEncodingException; {:try_start_1 .. :try_end_1} :catch_2

    move-result-object v7

    :goto_1
    :try_start_2
    const-string v2, "UTF-8"

    invoke-static {v13, v2}, Ljava/net/URLDecoder;->decode(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    :try_end_2
    .catch Ljava/io/UnsupportedEncodingException; {:try_start_2 .. :try_end_2} :catch_2

    move-result-object v3

    add-int/lit8 v16, v15, 0x1

    aget-object v8, v14, v15

    add-int/lit8 v15, v16, 0x1

    :try_start_3
    aget-object v2, v14, v16

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I
    :try_end_3
    .catch Ljava/lang/NumberFormatException; {:try_start_3 .. :try_end_3} :catch_3

    move-result v2

    if-lez v2, :cond_1

    const/4 v9, 0x1

    :goto_2
    new-instance v1, Lcom/google/android/gm/provider/Label;

    const/4 v2, 0x0

    move-object/from16 v10, p1

    invoke-direct/range {v1 .. v10}, Lcom/google/android/gm/provider/Label;-><init>(Landroid/content/Context;Ljava/lang/String;JLjava/lang/String;Ljava/lang/String;Ljava/lang/String;ZLjava/util/Map;)V

    goto :goto_0

    :catch_0
    move-exception v11

    const-string v2, "Gmail"

    const-string v10, "Problem parsing labelId: %s original string: %s"

    const/16 v17, 0x2

    move/from16 v0, v17

    new-array v0, v0, [Ljava/lang/Object;

    move-object/from16 v17, v0

    const/16 v18, 0x0

    invoke-virtual {v11}, Ljava/lang/NumberFormatException;->getMessage()Ljava/lang/String;

    move-result-object v19

    aput-object v19, v17, v18

    const/16 v18, 0x1

    aput-object p0, v17, v18

    move-object/from16 v0, v17

    invoke-static {v2, v10, v0}, Lcom/google/android/gm/provider/LogUtils;->w(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)I

    const/4 v1, 0x0

    goto :goto_0

    :catch_1
    move-exception v11

    :try_start_4
    const-string v2, "Gmail"

    const-string v10, "illegal argument"

    const/16 v17, 0x0

    move/from16 v0, v17

    new-array v0, v0, [Ljava/lang/Object;

    move-object/from16 v17, v0

    move-object/from16 v0, v17

    invoke-static {v2, v11, v10, v0}, Lcom/google/android/gm/provider/LogUtils;->wtf(Ljava/lang/String;Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)I
    :try_end_4
    .catch Ljava/io/UnsupportedEncodingException; {:try_start_4 .. :try_end_4} :catch_2

    move-object v7, v12

    goto :goto_1

    :catch_2
    move-exception v11

    const-string v2, "Gmail"

    const-string v10, "unsupported encoding"

    const/16 v17, 0x0

    move/from16 v0, v17

    new-array v0, v0, [Ljava/lang/Object;

    move-object/from16 v17, v0

    move-object/from16 v0, v17

    invoke-static {v2, v11, v10, v0}, Lcom/google/android/gm/provider/LogUtils;->wtf(Ljava/lang/String;Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)I

    const/4 v1, 0x0

    goto/16 :goto_0

    :cond_1
    const/4 v9, 0x0

    goto :goto_2

    :catch_3
    move-exception v11

    const-string v2, "Gmail"

    const-string v10, "Problem parsing isHidden: %s original string: %s"

    const/16 v17, 0x2

    move/from16 v0, v17

    new-array v0, v0, [Ljava/lang/Object;

    move-object/from16 v17, v0

    const/16 v18, 0x0

    invoke-virtual {v11}, Ljava/lang/NumberFormatException;->getMessage()Ljava/lang/String;

    move-result-object v19

    aput-object v19, v17, v18

    const/16 v18, 0x1

    aput-object p0, v17, v18

    move-object/from16 v0, v17

    invoke-static {v2, v10, v0}, Lcom/google/android/gm/provider/LogUtils;->w(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)I

    const/4 v1, 0x0

    goto/16 :goto_0
.end method


# virtual methods
.method public declared-synchronized getBackgroundColor()I
    .locals 3

    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/google/android/gm/provider/Label;->mAccount:Ljava/lang/String;

    iget-object v1, p0, Lcom/google/android/gm/provider/Label;->mCanonicalName:Ljava/lang/String;

    iget-object v2, p0, Lcom/google/android/gm/provider/Label;->mColor:Ljava/lang/String;

    invoke-static {v0, v1, v2}, Lcom/google/android/gm/provider/Label;->getBackgroundColor(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    monitor-exit p0

    return v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized getCanonicalName()Ljava/lang/String;
    .locals 1

    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/google/android/gm/provider/Label;->mCanonicalName:Ljava/lang/String;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-object v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized getColor()Ljava/lang/String;
    .locals 2

    monitor-enter p0

    :try_start_0
    invoke-virtual {p0}, Lcom/google/android/gm/provider/Label;->isSystemLabel()Z

    move-result v0

    iget-object v1, p0, Lcom/google/android/gm/provider/Label;->mColor:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/google/android/gm/provider/Label;->getColor(ZLjava/lang/String;)Ljava/lang/String;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v0

    monitor-exit p0

    return-object v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized getDisplayNoConversationCount()Z
    .locals 2

    monitor-enter p0

    :try_start_0
    iget v0, p0, Lcom/google/android/gm/provider/Label;->mLabelCountBehavior:I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    const/4 v1, 0x2

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    monitor-exit p0

    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized getDisplayTotalConversationCount()Z
    .locals 2

    const/4 v0, 0x1

    monitor-enter p0

    :try_start_0
    iget v1, p0, Lcom/google/android/gm/provider/Label;->mLabelCountBehavior:I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-ne v1, v0, :cond_0

    :goto_0
    monitor-exit p0

    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized getForceSyncAll()Z
    .locals 2

    const/4 v0, 0x1

    monitor-enter p0

    :try_start_0
    iget v1, p0, Lcom/google/android/gm/provider/Label;->mLabelSyncPolicy:I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-ne v1, v0, :cond_0

    :goto_0
    monitor-exit p0

    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized getForceSyncAllOrPartial()Z
    .locals 2

    monitor-enter p0

    :try_start_0
    iget v0, p0, Lcom/google/android/gm/provider/Label;->mLabelSyncPolicy:I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    const/4 v1, 0x3

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    monitor-exit p0

    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized getForceSyncNone()Z
    .locals 2

    monitor-enter p0

    :try_start_0
    iget v0, p0, Lcom/google/android/gm/provider/Label;->mLabelSyncPolicy:I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    const/4 v1, 0x2

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    monitor-exit p0

    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized getHidden()Z
    .locals 1

    monitor-enter p0

    :try_start_0
    iget-boolean v0, p0, Lcom/google/android/gm/provider/Label;->mIsHidden:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized getId()J
    .locals 2

    monitor-enter p0

    :try_start_0
    iget-wide v0, p0, Lcom/google/android/gm/provider/Label;->mId:J
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-wide v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized getLastTouched()J
    .locals 2

    monitor-enter p0

    :try_start_0
    iget-wide v0, p0, Lcom/google/android/gm/provider/Label;->mLastTouched:J
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-wide v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized getName()Ljava/lang/String;
    .locals 1

    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/google/android/gm/provider/Label;->mName:Ljava/lang/String;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-object v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized getNumConversations()I
    .locals 2

    monitor-enter p0

    :try_start_0
    iget-boolean v0, p0, Lcom/google/android/gm/provider/Label;->mCountsInitialized:Z

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "conversation counts were not initialized"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    :cond_0
    :try_start_1
    iget v0, p0, Lcom/google/android/gm/provider/Label;->mNumConversations:I
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    monitor-exit p0

    return v0
.end method

.method public declared-synchronized getNumUnreadConversations()I
    .locals 2

    monitor-enter p0

    :try_start_0
    iget-boolean v0, p0, Lcom/google/android/gm/provider/Label;->mCountsInitialized:Z

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "unread conversation counts were not initialized"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    :cond_0
    :try_start_1
    iget v0, p0, Lcom/google/android/gm/provider/Label;->mNumUnreadConversations:I
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    monitor-exit p0

    return v0
.end method

.method public declared-synchronized getTextColor()I
    .locals 3

    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/google/android/gm/provider/Label;->mAccount:Ljava/lang/String;

    iget-object v1, p0, Lcom/google/android/gm/provider/Label;->mCanonicalName:Ljava/lang/String;

    iget-object v2, p0, Lcom/google/android/gm/provider/Label;->mColor:Ljava/lang/String;

    invoke-static {v0, v1, v2}, Lcom/google/android/gm/provider/Label;->getTextColor(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    monitor-exit p0

    return v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized isSystemLabel()Z
    .locals 1

    monitor-enter p0

    :try_start_0
    iget-boolean v0, p0, Lcom/google/android/gm/provider/Label;->mIsSystemLabel:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method loadInternal(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;IIIIJ)V
    .locals 2
    .param p1    # Landroid/content/Context;
    .param p2    # Ljava/lang/String;
    .param p3    # Ljava/lang/String;
    .param p4    # Ljava/lang/String;
    .param p5    # I
    .param p6    # I
    .param p7    # I
    .param p8    # I
    .param p9    # J

    const/4 v1, 0x0

    iget-object v0, p0, Lcom/google/android/gm/provider/Label;->mCanonicalName:Ljava/lang/String;

    invoke-virtual {p2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    iput-object p2, p0, Lcom/google/android/gm/provider/Label;->mCanonicalName:Ljava/lang/String;

    iget-object v0, p0, Lcom/google/android/gm/provider/Label;->mCanonicalName:Ljava/lang/String;

    invoke-static {v0}, Lcom/google/android/gm/provider/Gmail;->isSystemLabel(Ljava/lang/String;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/google/android/gm/provider/Label;->mIsSystemLabel:Z

    iput-object v1, p0, Lcom/google/android/gm/provider/Label;->mSerializedInfo:Ljava/lang/String;

    :cond_0
    iget-object v0, p0, Lcom/google/android/gm/provider/Label;->mColor:Ljava/lang/String;

    invoke-static {p4, v0}, Lcom/google/common/base/Objects;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    iput-object p4, p0, Lcom/google/android/gm/provider/Label;->mColor:Ljava/lang/String;

    iput-object v1, p0, Lcom/google/android/gm/provider/Label;->mSerializedInfo:Ljava/lang/String;

    :cond_1
    iget-boolean v0, p0, Lcom/google/android/gm/provider/Label;->mIsSystemLabel:Z

    if-eqz v0, :cond_3

    if-eqz p3, :cond_2

    iget-object v0, p0, Lcom/google/android/gm/provider/Label;->mCanonicalName:Ljava/lang/String;

    invoke-virtual {v0, p3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    :cond_2
    invoke-direct {p0, p1}, Lcom/google/android/gm/provider/Label;->getHumanSystemLabelName(Landroid/content/Context;)Ljava/lang/String;

    move-result-object p3

    :cond_3
    iget-object v0, p0, Lcom/google/android/gm/provider/Label;->mName:Ljava/lang/String;

    invoke-static {p3, v0}, Lcom/google/common/base/Objects;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_4

    iput-object p3, p0, Lcom/google/android/gm/provider/Label;->mName:Ljava/lang/String;

    iput-object v1, p0, Lcom/google/android/gm/provider/Label;->mSerializedInfo:Ljava/lang/String;

    :cond_4
    iput p5, p0, Lcom/google/android/gm/provider/Label;->mNumConversations:I

    iput p6, p0, Lcom/google/android/gm/provider/Label;->mNumUnreadConversations:I

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/gm/provider/Label;->mCountsInitialized:Z

    iput p7, p0, Lcom/google/android/gm/provider/Label;->mLabelCountBehavior:I

    iput p8, p0, Lcom/google/android/gm/provider/Label;->mLabelSyncPolicy:I

    iput-wide p9, p0, Lcom/google/android/gm/provider/Label;->mLastTouched:J

    return-void
.end method

.method public declared-synchronized serialize()Ljava/lang/String;
    .locals 7

    monitor-enter p0

    :try_start_0
    iget-object v4, p0, Lcom/google/android/gm/provider/Label;->mSerializedInfo:Ljava/lang/String;

    if-eqz v4, :cond_0

    iget-object v4, p0, Lcom/google/android/gm/provider/Label;->mSerializedInfo:Ljava/lang/String;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :goto_0
    monitor-exit p0

    return-object v4

    :cond_0
    :try_start_1
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, ""

    const-string v2, ""
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    :try_start_2
    iget-object v4, p0, Lcom/google/android/gm/provider/Label;->mAccount:Ljava/lang/String;

    const-string v5, "UTF-8"

    invoke-static {v4, v5}, Ljava/net/URLEncoder;->encode(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iget-object v4, p0, Lcom/google/android/gm/provider/Label;->mName:Ljava/lang/String;

    const-string v5, "UTF-8"

    invoke-static {v4, v5}, Ljava/net/URLEncoder;->encode(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    :try_end_2
    .catch Ljava/io/UnsupportedEncodingException; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    move-result-object v2

    :goto_1
    :try_start_3
    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "^*^"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-wide v4, p0, Lcom/google/android/gm/provider/Label;->mId:J

    invoke-virtual {v3, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "^*^"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v4, p0, Lcom/google/android/gm/provider/Label;->mCanonicalName:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "^*^"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "^*^"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v4, p0, Lcom/google/android/gm/provider/Label;->mColor:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "^*^"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-boolean v4, p0, Lcom/google/android/gm/provider/Label;->mIsHidden:Z

    if-eqz v4, :cond_1

    const-string v4, "1"

    :goto_2
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    iput-object v4, p0, Lcom/google/android/gm/provider/Label;->mSerializedInfo:Ljava/lang/String;

    iget-object v4, p0, Lcom/google/android/gm/provider/Label;->mSerializedInfo:Ljava/lang/String;

    goto :goto_0

    :catch_0
    move-exception v0

    const-string v4, "Gmail"

    const-string v5, "unsupported encoding"

    const/4 v6, 0x0

    new-array v6, v6, [Ljava/lang/Object;

    invoke-static {v4, v0, v5, v6}, Lcom/google/android/gm/provider/LogUtils;->wtf(Ljava/lang/String;Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)I
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto :goto_1

    :catchall_0
    move-exception v4

    monitor-exit p0

    throw v4

    :cond_1
    :try_start_4
    const-string v4, "0"
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    goto :goto_2
.end method
