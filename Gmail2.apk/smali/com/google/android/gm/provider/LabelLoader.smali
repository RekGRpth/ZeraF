.class public Lcom/google/android/gm/provider/LabelLoader;
.super Landroid/content/AsyncTaskLoader;
.source "LabelLoader.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/content/AsyncTaskLoader",
        "<",
        "Lcom/google/android/gm/provider/LabelList;",
        ">;"
    }
.end annotation


# instance fields
.field private final mAccount:Ljava/lang/String;

.field private final mAutoRefresh:Z

.field private mLabelList:Lcom/google/android/gm/provider/LabelList;


# direct methods
.method public constructor <init>(Landroid/content/Context;Ljava/lang/String;Z)V
    .locals 0
    .param p1    # Landroid/content/Context;
    .param p2    # Ljava/lang/String;
    .param p3    # Z

    invoke-direct {p0, p1}, Landroid/content/AsyncTaskLoader;-><init>(Landroid/content/Context;)V

    iput-object p2, p0, Lcom/google/android/gm/provider/LabelLoader;->mAccount:Ljava/lang/String;

    iput-boolean p3, p0, Lcom/google/android/gm/provider/LabelLoader;->mAutoRefresh:Z

    return-void
.end method


# virtual methods
.method public deliverResult(Lcom/google/android/gm/provider/LabelList;)V
    .locals 1
    .param p1    # Lcom/google/android/gm/provider/LabelList;

    invoke-super {p0, p1}, Landroid/content/AsyncTaskLoader;->deliverResult(Ljava/lang/Object;)V

    iget-boolean v0, p0, Lcom/google/android/gm/provider/LabelLoader;->mAutoRefresh:Z

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/gm/provider/LabelLoader;->mLabelList:Lcom/google/android/gm/provider/LabelList;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/gm/provider/LabelLoader;->mLabelList:Lcom/google/android/gm/provider/LabelList;

    invoke-virtual {v0}, Lcom/google/android/gm/provider/LabelList;->unregisterForLabelChanges()V

    :cond_0
    iput-object p1, p0, Lcom/google/android/gm/provider/LabelLoader;->mLabelList:Lcom/google/android/gm/provider/LabelList;

    iget-object v0, p0, Lcom/google/android/gm/provider/LabelLoader;->mLabelList:Lcom/google/android/gm/provider/LabelList;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/gm/provider/LabelLoader;->mLabelList:Lcom/google/android/gm/provider/LabelList;

    invoke-virtual {v0}, Lcom/google/android/gm/provider/LabelList;->registerForLabelChanges()V

    :cond_1
    return-void
.end method

.method public bridge synthetic deliverResult(Ljava/lang/Object;)V
    .locals 0
    .param p1    # Ljava/lang/Object;

    check-cast p1, Lcom/google/android/gm/provider/LabelList;

    invoke-virtual {p0, p1}, Lcom/google/android/gm/provider/LabelLoader;->deliverResult(Lcom/google/android/gm/provider/LabelList;)V

    return-void
.end method

.method public loadInBackground()Lcom/google/android/gm/provider/LabelList;
    .locals 5

    invoke-virtual {p0}, Lcom/google/android/gm/provider/LabelLoader;->getContext()Landroid/content/Context;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/gm/provider/LabelLoader;->mAccount:Ljava/lang/String;

    const/4 v3, 0x0

    const/4 v4, 0x0

    invoke-static {v1, v2, v3, v4}, Lcom/google/android/gm/provider/LabelManager;->getLabelList(Landroid/content/Context;Ljava/lang/String;Ljava/util/List;Z)Lcom/google/android/gm/provider/LabelList;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic loadInBackground()Ljava/lang/Object;
    .locals 1

    invoke-virtual {p0}, Lcom/google/android/gm/provider/LabelLoader;->loadInBackground()Lcom/google/android/gm/provider/LabelList;

    move-result-object v0

    return-object v0
.end method

.method protected onReset()V
    .locals 0

    invoke-virtual {p0}, Lcom/google/android/gm/provider/LabelLoader;->stopLoading()V

    return-void
.end method

.method protected onStartLoading()V
    .locals 0

    invoke-virtual {p0}, Lcom/google/android/gm/provider/LabelLoader;->forceLoad()V

    return-void
.end method

.method protected onStopLoading()V
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/gm/provider/LabelLoader;->mAutoRefresh:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/gm/provider/LabelLoader;->mLabelList:Lcom/google/android/gm/provider/LabelList;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/gm/provider/LabelLoader;->mLabelList:Lcom/google/android/gm/provider/LabelList;

    invoke-virtual {v0}, Lcom/google/android/gm/provider/LabelList;->unregisterForLabelChanges()V

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/gm/provider/LabelLoader;->mLabelList:Lcom/google/android/gm/provider/LabelList;

    :cond_0
    invoke-virtual {p0}, Lcom/google/android/gm/provider/LabelLoader;->cancelLoad()Z

    return-void
.end method
