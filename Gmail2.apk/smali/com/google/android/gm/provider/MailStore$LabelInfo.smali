.class public Lcom/google/android/gm/provider/MailStore$LabelInfo;
.super Ljava/lang/Object;
.source "MailStore.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/gm/provider/MailStore;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "LabelInfo"
.end annotation


# instance fields
.field public canonicalName:Ljava/lang/String;

.field public color:I

.field public name:Ljava/lang/String;

.field public numConversations:I

.field public numUnreadConversations:I

.field public visibility:Ljava/lang/String;


# direct methods
.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;IIILjava/lang/String;)V
    .locals 0
    .param p1    # Ljava/lang/String;
    .param p2    # Ljava/lang/String;
    .param p3    # I
    .param p4    # I
    .param p5    # I
    .param p6    # Ljava/lang/String;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/google/android/gm/provider/MailStore$LabelInfo;->canonicalName:Ljava/lang/String;

    iput-object p2, p0, Lcom/google/android/gm/provider/MailStore$LabelInfo;->name:Ljava/lang/String;

    iput p3, p0, Lcom/google/android/gm/provider/MailStore$LabelInfo;->numConversations:I

    iput p4, p0, Lcom/google/android/gm/provider/MailStore$LabelInfo;->numUnreadConversations:I

    iput p5, p0, Lcom/google/android/gm/provider/MailStore$LabelInfo;->color:I

    iput-object p6, p0, Lcom/google/android/gm/provider/MailStore$LabelInfo;->visibility:Ljava/lang/String;

    return-void
.end method
