.class Lcom/google/android/gm/provider/MailEngine$CloneableMessageCursor;
.super Lcom/google/android/gm/provider/MailEngine$NetworkCursor;
.source "MailEngine.java"

# interfaces
.implements Ljava/lang/Cloneable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/gm/provider/MailEngine;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "CloneableMessageCursor"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/gm/provider/MailEngine;


# direct methods
.method public constructor <init>(Lcom/google/android/gm/provider/MailEngine;Landroid/database/sqlite/SQLiteDatabase;Landroid/database/sqlite/SQLiteCursorDriver;Ljava/lang/String;Landroid/database/sqlite/SQLiteQuery;Lcom/google/android/gm/provider/MailEngine$NetworkCursorLogic;)V
    .locals 0
    .param p2    # Landroid/database/sqlite/SQLiteDatabase;
    .param p3    # Landroid/database/sqlite/SQLiteCursorDriver;
    .param p4    # Ljava/lang/String;
    .param p5    # Landroid/database/sqlite/SQLiteQuery;
    .param p6    # Lcom/google/android/gm/provider/MailEngine$NetworkCursorLogic;

    iput-object p1, p0, Lcom/google/android/gm/provider/MailEngine$CloneableMessageCursor;->this$0:Lcom/google/android/gm/provider/MailEngine;

    invoke-direct/range {p0 .. p6}, Lcom/google/android/gm/provider/MailEngine$NetworkCursor;-><init>(Lcom/google/android/gm/provider/MailEngine;Landroid/database/sqlite/SQLiteDatabase;Landroid/database/sqlite/SQLiteCursorDriver;Ljava/lang/String;Landroid/database/sqlite/SQLiteQuery;Lcom/google/android/gm/provider/MailEngine$NetworkCursorLogic;)V

    return-void
.end method


# virtual methods
.method public clone()Ljava/lang/Object;
    .locals 9

    new-instance v1, Lcom/android/mail/utils/MatrixCursorWithExtra;

    invoke-virtual {p0}, Lcom/google/android/gm/provider/MailEngine$CloneableMessageCursor;->getColumnNames()[Ljava/lang/String;

    move-result-object v5

    invoke-virtual {p0}, Lcom/google/android/gm/provider/MailEngine$CloneableMessageCursor;->getCount()I

    move-result v6

    new-instance v7, Landroid/os/Bundle;

    invoke-virtual {p0}, Lcom/google/android/gm/provider/MailEngine$CloneableMessageCursor;->getExtras()Landroid/os/Bundle;

    move-result-object v8

    invoke-direct {v7, v8}, Landroid/os/Bundle;-><init>(Landroid/os/Bundle;)V

    invoke-direct {v1, v5, v6, v7}, Lcom/android/mail/utils/MatrixCursorWithExtra;-><init>([Ljava/lang/String;ILandroid/os/Bundle;)V

    invoke-super {p0}, Lcom/google/android/gm/provider/MailEngine$NetworkCursor;->getColumnCount()I

    move-result v3

    const-string v5, "body"

    invoke-virtual {p0, v5}, Lcom/google/android/gm/provider/MailEngine$CloneableMessageCursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v0

    const/4 v5, -0x1

    invoke-virtual {p0, v5}, Lcom/google/android/gm/provider/MailEngine$CloneableMessageCursor;->moveToPosition(I)Z

    :goto_0
    invoke-virtual {p0}, Lcom/google/android/gm/provider/MailEngine$CloneableMessageCursor;->moveToNext()Z

    move-result v5

    if-eqz v5, :cond_2

    new-array v4, v3, [Ljava/lang/Object;

    const/4 v2, 0x0

    :goto_1
    if-ge v2, v3, :cond_1

    if-ne v2, v0, :cond_0

    invoke-virtual {p0, v0}, Lcom/google/android/gm/provider/MailEngine$CloneableMessageCursor;->getBlob(I)[B

    move-result-object v5

    aput-object v5, v4, v2

    :goto_2
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    :cond_0
    invoke-virtual {p0, v2}, Lcom/google/android/gm/provider/MailEngine$CloneableMessageCursor;->getType(I)I

    move-result v5

    packed-switch v5, :pswitch_data_0

    const/4 v5, 0x0

    aput-object v5, v4, v2

    goto :goto_2

    :pswitch_0
    invoke-virtual {p0, v2}, Lcom/google/android/gm/provider/MailEngine$CloneableMessageCursor;->getBlob(I)[B

    move-result-object v5

    aput-object v5, v4, v2

    goto :goto_2

    :pswitch_1
    invoke-virtual {p0, v2}, Lcom/google/android/gm/provider/MailEngine$CloneableMessageCursor;->getDouble(I)D

    move-result-wide v5

    invoke-static {v5, v6}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v5

    aput-object v5, v4, v2

    goto :goto_2

    :pswitch_2
    invoke-virtual {p0, v2}, Lcom/google/android/gm/provider/MailEngine$CloneableMessageCursor;->getLong(I)J

    move-result-wide v5

    invoke-static {v5, v6}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v5

    aput-object v5, v4, v2

    goto :goto_2

    :pswitch_3
    invoke-virtual {p0, v2}, Lcom/google/android/gm/provider/MailEngine$CloneableMessageCursor;->getString(I)Ljava/lang/String;

    move-result-object v5

    aput-object v5, v4, v2

    goto :goto_2

    :cond_1
    invoke-virtual {v1, v4}, Landroid/database/MatrixCursor;->addRow([Ljava/lang/Object;)V

    goto :goto_0

    :cond_2
    return-object v1

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_2
        :pswitch_1
        :pswitch_3
        :pswitch_0
    .end packed-switch
.end method
