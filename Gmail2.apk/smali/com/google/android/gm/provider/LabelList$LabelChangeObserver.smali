.class Lcom/google/android/gm/provider/LabelList$LabelChangeObserver;
.super Landroid/database/ContentObserver;
.source "LabelList.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/gm/provider/LabelList;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "LabelChangeObserver"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/gm/provider/LabelList$LabelChangeObserver$UpdateListTask;
    }
.end annotation


# instance fields
.field private final mAccount:Ljava/lang/String;

.field private final mContext:Landroid/content/Context;

.field private final mIncludeHiddenLabels:Z

.field private mListRefreshTask:Lcom/google/android/gm/provider/LabelList$LabelChangeObserver$UpdateListTask;

.field private mUpdateRequested:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;Ljava/lang/String;Z)V
    .locals 1
    .param p1    # Landroid/content/Context;
    .param p2    # Ljava/lang/String;
    .param p3    # Z

    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    invoke-direct {p0, v0}, Landroid/database/ContentObserver;-><init>(Landroid/os/Handler;)V

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/gm/provider/LabelList$LabelChangeObserver;->mListRefreshTask:Lcom/google/android/gm/provider/LabelList$LabelChangeObserver$UpdateListTask;

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/gm/provider/LabelList$LabelChangeObserver;->mUpdateRequested:Z

    invoke-virtual {p1}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gm/provider/LabelList$LabelChangeObserver;->mContext:Landroid/content/Context;

    iput-object p2, p0, Lcom/google/android/gm/provider/LabelList$LabelChangeObserver;->mAccount:Ljava/lang/String;

    iput-boolean p3, p0, Lcom/google/android/gm/provider/LabelList$LabelChangeObserver;->mIncludeHiddenLabels:Z

    return-void
.end method

.method static synthetic access$1000(Lcom/google/android/gm/provider/LabelList$LabelChangeObserver;)V
    .locals 0
    .param p0    # Lcom/google/android/gm/provider/LabelList$LabelChangeObserver;

    invoke-direct {p0}, Lcom/google/android/gm/provider/LabelList$LabelChangeObserver;->startUpdateTask()V

    return-void
.end method

.method static synthetic access$400(Lcom/google/android/gm/provider/LabelList$LabelChangeObserver;)Landroid/content/Context;
    .locals 1
    .param p0    # Lcom/google/android/gm/provider/LabelList$LabelChangeObserver;

    iget-object v0, p0, Lcom/google/android/gm/provider/LabelList$LabelChangeObserver;->mContext:Landroid/content/Context;

    return-object v0
.end method

.method static synthetic access$500(Lcom/google/android/gm/provider/LabelList$LabelChangeObserver;)Ljava/lang/String;
    .locals 1
    .param p0    # Lcom/google/android/gm/provider/LabelList$LabelChangeObserver;

    iget-object v0, p0, Lcom/google/android/gm/provider/LabelList$LabelChangeObserver;->mAccount:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$600(Lcom/google/android/gm/provider/LabelList$LabelChangeObserver;)Z
    .locals 1
    .param p0    # Lcom/google/android/gm/provider/LabelList$LabelChangeObserver;

    iget-boolean v0, p0, Lcom/google/android/gm/provider/LabelList$LabelChangeObserver;->mIncludeHiddenLabels:Z

    return v0
.end method

.method static synthetic access$700(Lcom/google/android/gm/provider/LabelList$LabelChangeObserver;Lcom/google/android/gm/provider/LabelList;)V
    .locals 0
    .param p0    # Lcom/google/android/gm/provider/LabelList$LabelChangeObserver;
    .param p1    # Lcom/google/android/gm/provider/LabelList;

    invoke-direct {p0, p1}, Lcom/google/android/gm/provider/LabelList$LabelChangeObserver;->updateAllRegisteredLists(Lcom/google/android/gm/provider/LabelList;)V

    return-void
.end method

.method static synthetic access$802(Lcom/google/android/gm/provider/LabelList$LabelChangeObserver;Lcom/google/android/gm/provider/LabelList$LabelChangeObserver$UpdateListTask;)Lcom/google/android/gm/provider/LabelList$LabelChangeObserver$UpdateListTask;
    .locals 0
    .param p0    # Lcom/google/android/gm/provider/LabelList$LabelChangeObserver;
    .param p1    # Lcom/google/android/gm/provider/LabelList$LabelChangeObserver$UpdateListTask;

    iput-object p1, p0, Lcom/google/android/gm/provider/LabelList$LabelChangeObserver;->mListRefreshTask:Lcom/google/android/gm/provider/LabelList$LabelChangeObserver$UpdateListTask;

    return-object p1
.end method

.method static synthetic access$900(Lcom/google/android/gm/provider/LabelList$LabelChangeObserver;)Z
    .locals 1
    .param p0    # Lcom/google/android/gm/provider/LabelList$LabelChangeObserver;

    iget-boolean v0, p0, Lcom/google/android/gm/provider/LabelList$LabelChangeObserver;->mUpdateRequested:Z

    return v0
.end method

.method private startUpdateTask()V
    .locals 4

    const/4 v3, 0x0

    new-instance v0, Lcom/google/android/gm/provider/LabelList$LabelChangeObserver$UpdateListTask;

    invoke-direct {v0, p0}, Lcom/google/android/gm/provider/LabelList$LabelChangeObserver$UpdateListTask;-><init>(Lcom/google/android/gm/provider/LabelList$LabelChangeObserver;)V

    iput-object v0, p0, Lcom/google/android/gm/provider/LabelList$LabelChangeObserver;->mListRefreshTask:Lcom/google/android/gm/provider/LabelList$LabelChangeObserver$UpdateListTask;

    iget-object v0, p0, Lcom/google/android/gm/provider/LabelList$LabelChangeObserver;->mListRefreshTask:Lcom/google/android/gm/provider/LabelList$LabelChangeObserver$UpdateListTask;

    sget-object v1, Landroid/os/AsyncTask;->THREAD_POOL_EXECUTOR:Ljava/util/concurrent/Executor;

    new-array v2, v3, [Ljava/lang/Void;

    invoke-virtual {v0, v1, v2}, Lcom/google/android/gm/provider/LabelList$LabelChangeObserver$UpdateListTask;->executeOnExecutor(Ljava/util/concurrent/Executor;[Ljava/lang/Object;)Landroid/os/AsyncTask;

    iput-boolean v3, p0, Lcom/google/android/gm/provider/LabelList$LabelChangeObserver;->mUpdateRequested:Z

    return-void
.end method

.method private updateAllRegisteredLists(Lcom/google/android/gm/provider/LabelList;)V
    .locals 7
    .param p1    # Lcom/google/android/gm/provider/LabelList;

    # getter for: Lcom/google/android/gm/provider/LabelList;->sLabelListObserverLock:Ljava/lang/Object;
    invoke-static {}, Lcom/google/android/gm/provider/LabelList;->access$000()Ljava/lang/Object;

    move-result-object v5

    monitor-enter v5

    :try_start_0
    # getter for: Lcom/google/android/gm/provider/LabelList;->sAutoUpdateLists:Ljava/util/Map;
    invoke-static {}, Lcom/google/android/gm/provider/LabelList;->access$100()Ljava/util/Map;

    move-result-object v4

    invoke-interface {v4, p0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/util/Set;

    if-nez v3, :cond_0

    monitor-exit v5

    :goto_0
    return-void

    :cond_0
    invoke-static {v3}, Lcom/google/common/collect/Sets;->newHashSet(Ljava/lang/Iterable;)Ljava/util/HashSet;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_1
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/gm/provider/LabelList;

    # getter for: Lcom/google/android/gm/provider/LabelList;->mList:Ljava/util/ArrayList;
    invoke-static {v1}, Lcom/google/android/gm/provider/LabelList;->access$200(Lcom/google/android/gm/provider/LabelList;)Ljava/util/ArrayList;

    move-result-object v4

    invoke-virtual {v4}, Ljava/util/ArrayList;->clear()V

    # getter for: Lcom/google/android/gm/provider/LabelList;->mList:Ljava/util/ArrayList;
    invoke-static {v1}, Lcom/google/android/gm/provider/LabelList;->access$200(Lcom/google/android/gm/provider/LabelList;)Ljava/util/ArrayList;

    move-result-object v4

    # getter for: Lcom/google/android/gm/provider/LabelList;->mList:Ljava/util/ArrayList;
    invoke-static {p1}, Lcom/google/android/gm/provider/LabelList;->access$200(Lcom/google/android/gm/provider/LabelList;)Ljava/util/ArrayList;

    move-result-object v6

    invoke-virtual {v4, v6}, Ljava/util/ArrayList;->addAll(Ljava/util/Collection;)Z

    # invokes: Lcom/google/android/gm/provider/LabelList;->onLabelDataSetChanged()V
    invoke-static {v1}, Lcom/google/android/gm/provider/LabelList;->access$300(Lcom/google/android/gm/provider/LabelList;)V

    goto :goto_1

    :catchall_0
    move-exception v4

    monitor-exit v5
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v4

    :cond_1
    :try_start_1
    monitor-exit v5
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0
.end method


# virtual methods
.method public deliverSelfNotifications()Z
    .locals 1

    const/4 v0, 0x1

    return v0
.end method

.method public onChange(Z)V
    .locals 1
    .param p1    # Z

    iget-object v0, p0, Lcom/google/android/gm/provider/LabelList$LabelChangeObserver;->mListRefreshTask:Lcom/google/android/gm/provider/LabelList$LabelChangeObserver$UpdateListTask;

    if-nez v0, :cond_0

    invoke-direct {p0}, Lcom/google/android/gm/provider/LabelList$LabelChangeObserver;->startUpdateTask()V

    :goto_0
    return-void

    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/gm/provider/LabelList$LabelChangeObserver;->mUpdateRequested:Z

    goto :goto_0
.end method
