.class Lcom/google/android/gm/provider/LabelManager$LabelFactory;
.super Ljava/lang/Object;
.source "LabelManager.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/gm/provider/LabelManager;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = "LabelFactory"
.end annotation


# instance fields
.field final mAccount:Ljava/lang/String;

.field final mCanonicalNameIndex:I

.field final mColorIndex:I

.field final mContext:Landroid/content/Context;

.field final mHiddenLabelIndex:I

.field final mIdIndex:I

.field final mLabelCountBehaviorIndex:I

.field final mLabelSyncPolicyIndex:I

.field final mLastTouchedIndex:I

.field final mNameIndex:I

.field final mNumConversationsIndex:I

.field final mNumUnreadConversationsIndex:I

.field final mSystemLabelNameMap:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/CharSequence;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method constructor <init>(Landroid/content/Context;Ljava/lang/String;Landroid/database/Cursor;)V
    .locals 1
    .param p1    # Landroid/content/Context;
    .param p2    # Ljava/lang/String;
    .param p3    # Landroid/database/Cursor;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/google/android/gm/provider/LabelManager$LabelFactory;->mContext:Landroid/content/Context;

    iput-object p2, p0, Lcom/google/android/gm/provider/LabelManager$LabelFactory;->mAccount:Ljava/lang/String;

    const-string v0, "_id"

    invoke-interface {p3, v0}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lcom/google/android/gm/provider/LabelManager$LabelFactory;->mIdIndex:I

    const-string v0, "canonicalName"

    invoke-interface {p3, v0}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lcom/google/android/gm/provider/LabelManager$LabelFactory;->mCanonicalNameIndex:I

    const-string v0, "name"

    invoke-interface {p3, v0}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lcom/google/android/gm/provider/LabelManager$LabelFactory;->mNameIndex:I

    const-string v0, "numConversations"

    invoke-interface {p3, v0}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lcom/google/android/gm/provider/LabelManager$LabelFactory;->mNumConversationsIndex:I

    const-string v0, "numUnreadConversations"

    invoke-interface {p3, v0}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lcom/google/android/gm/provider/LabelManager$LabelFactory;->mNumUnreadConversationsIndex:I

    const-string v0, "color"

    invoke-interface {p3, v0}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lcom/google/android/gm/provider/LabelManager$LabelFactory;->mColorIndex:I

    const-string v0, "hidden"

    invoke-interface {p3, v0}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lcom/google/android/gm/provider/LabelManager$LabelFactory;->mHiddenLabelIndex:I

    const-string v0, "labelCountDisplayBehavior"

    invoke-interface {p3, v0}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lcom/google/android/gm/provider/LabelManager$LabelFactory;->mLabelCountBehaviorIndex:I

    const-string v0, "labelSyncPolicy"

    invoke-interface {p3, v0}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lcom/google/android/gm/provider/LabelManager$LabelFactory;->mLabelSyncPolicyIndex:I

    const-string v0, "lastTouched"

    invoke-interface {p3, v0}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lcom/google/android/gm/provider/LabelManager$LabelFactory;->mLastTouchedIndex:I

    invoke-static {p1}, Lcom/google/android/gm/provider/Label;->getSystemLabelNameMap(Landroid/content/Context;)Ljava/util/Map;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gm/provider/LabelManager$LabelFactory;->mSystemLabelNameMap:Ljava/util/Map;

    return-void
.end method


# virtual methods
.method newLabel(Landroid/database/Cursor;)Lcom/google/android/gm/provider/Label;
    .locals 17
    .param p1    # Landroid/database/Cursor;

    const/4 v1, 0x0

    if-eqz p1, :cond_0

    move-object/from16 v0, p0

    iget v2, v0, Lcom/google/android/gm/provider/LabelManager$LabelFactory;->mIdIndex:I

    move-object/from16 v0, p1

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v4

    move-object/from16 v0, p0

    iget v2, v0, Lcom/google/android/gm/provider/LabelManager$LabelFactory;->mCanonicalNameIndex:I

    move-object/from16 v0, p1

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v6

    move-object/from16 v0, p0

    iget v2, v0, Lcom/google/android/gm/provider/LabelManager$LabelFactory;->mNameIndex:I

    move-object/from16 v0, p1

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v7

    move-object/from16 v0, p0

    iget v2, v0, Lcom/google/android/gm/provider/LabelManager$LabelFactory;->mNumConversationsIndex:I

    move-object/from16 v0, p1

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getInt(I)I

    move-result v9

    move-object/from16 v0, p0

    iget v2, v0, Lcom/google/android/gm/provider/LabelManager$LabelFactory;->mNumUnreadConversationsIndex:I

    move-object/from16 v0, p1

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getInt(I)I

    move-result v10

    move-object/from16 v0, p0

    iget v2, v0, Lcom/google/android/gm/provider/LabelManager$LabelFactory;->mHiddenLabelIndex:I

    move-object/from16 v0, p1

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getInt(I)I

    move-result v2

    if-eqz v2, :cond_1

    const/4 v11, 0x1

    :goto_0
    move-object/from16 v0, p0

    iget v2, v0, Lcom/google/android/gm/provider/LabelManager$LabelFactory;->mColorIndex:I

    move-object/from16 v0, p1

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v8

    move-object/from16 v0, p0

    iget v2, v0, Lcom/google/android/gm/provider/LabelManager$LabelFactory;->mLabelCountBehaviorIndex:I

    move-object/from16 v0, p1

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getInt(I)I

    move-result v12

    move-object/from16 v0, p0

    iget v2, v0, Lcom/google/android/gm/provider/LabelManager$LabelFactory;->mLabelSyncPolicyIndex:I

    move-object/from16 v0, p1

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getInt(I)I

    move-result v13

    move-object/from16 v0, p0

    iget v2, v0, Lcom/google/android/gm/provider/LabelManager$LabelFactory;->mLastTouchedIndex:I

    move-object/from16 v0, p1

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v14

    new-instance v1, Lcom/google/android/gm/provider/Label;

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/gm/provider/LabelManager$LabelFactory;->mContext:Landroid/content/Context;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/gm/provider/LabelManager$LabelFactory;->mAccount:Ljava/lang/String;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/gm/provider/LabelManager$LabelFactory;->mSystemLabelNameMap:Ljava/util/Map;

    move-object/from16 v16, v0

    invoke-direct/range {v1 .. v16}, Lcom/google/android/gm/provider/Label;-><init>(Landroid/content/Context;Ljava/lang/String;JLjava/lang/String;Ljava/lang/String;Ljava/lang/String;IIZIIJLjava/util/Map;)V

    :cond_0
    return-object v1

    :cond_1
    const/4 v11, 0x0

    goto :goto_0
.end method
