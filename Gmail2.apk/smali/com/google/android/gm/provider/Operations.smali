.class public Lcom/google/android/gm/provider/Operations;
.super Ljava/lang/Object;
.source "Operations.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/gm/provider/Operations$OperationInfo;,
        Lcom/google/android/gm/provider/Operations$RecordHistory;
    }
.end annotation


# static fields
.field private static final NUM_RETRY_UPHILL_OPS:Ljava/lang/Integer;


# instance fields
.field private final PROJECTION_PROVIDE_OPERATIONS:[Ljava/lang/String;

.field private mContext:Landroid/content/Context;

.field private final mDb:Landroid/database/sqlite/SQLiteDatabase;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const/16 v0, 0x14

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    sput-object v0, Lcom/google/android/gm/provider/Operations;->NUM_RETRY_UPHILL_OPS:Ljava/lang/Integer;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/database/sqlite/SQLiteDatabase;)V
    .locals 3
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/database/sqlite/SQLiteDatabase;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/16 v0, 0x8

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v2, "_id"

    aput-object v2, v0, v1

    const/4 v1, 0x1

    const-string v2, "action"

    aput-object v2, v0, v1

    const/4 v1, 0x2

    const-string v2, "message_messageId"

    aput-object v2, v0, v1

    const/4 v1, 0x3

    const-string v2, "value1"

    aput-object v2, v0, v1

    const/4 v1, 0x4

    const-string v2, "value2"

    aput-object v2, v0, v1

    const/4 v1, 0x5

    const-string v2, "numAttempts"

    aput-object v2, v0, v1

    const/4 v1, 0x6

    const-string v2, "nextTimeToAttempt"

    aput-object v2, v0, v1

    const/4 v1, 0x7

    const-string v2, "delay"

    aput-object v2, v0, v1

    iput-object v0, p0, Lcom/google/android/gm/provider/Operations;->PROJECTION_PROVIDE_OPERATIONS:[Ljava/lang/String;

    iput-object p2, p0, Lcom/google/android/gm/provider/Operations;->mDb:Landroid/database/sqlite/SQLiteDatabase;

    iput-object p1, p0, Lcom/google/android/gm/provider/Operations;->mContext:Landroid/content/Context;

    return-void
.end method

.method private calculateAndUpdateOpDelay(JJIIJLcom/google/android/gm/provider/Operations$OperationInfo;Lcom/google/android/gm/provider/MailEngine$SyncInfo;Lcom/google/android/gm/provider/MailEngine;)Z
    .locals 16
    .param p1    # J
    .param p3    # J
    .param p5    # I
    .param p6    # I
    .param p7    # J
    .param p9    # Lcom/google/android/gm/provider/Operations$OperationInfo;
    .param p10    # Lcom/google/android/gm/provider/MailEngine$SyncInfo;
    .param p11    # Lcom/google/android/gm/provider/MailEngine;

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/google/android/gm/provider/Operations;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    const-string v2, "gmail_delay_bad_op"

    const/4 v3, 0x1

    invoke-static {v1, v2, v3}, Lcom/google/android/gsf/Gservices;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v1

    if-eqz v1, :cond_0

    const/4 v11, 0x1

    :goto_0
    if-nez v11, :cond_1

    const/4 v1, 0x1

    :goto_1
    return v1

    :cond_0
    const/4 v11, 0x0

    goto :goto_0

    :cond_1
    move-object/from16 v0, p9

    iget-object v10, v0, Lcom/google/android/gm/provider/Operations$OperationInfo;->mAction:Ljava/lang/String;

    const-string v1, "Gmail"

    const-string v2, "calculateAndUpdateOpDelay: currentTime = %d, nextTimeToAttempt = %d, numAttempts = %d delay=%d %s"

    const/4 v3, 0x5

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    invoke-static/range {p1 .. p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v5

    aput-object v5, v3, v4

    const/4 v4, 0x1

    invoke-static/range {p7 .. p8}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v5

    aput-object v5, v3, v4

    const/4 v4, 0x2

    invoke-static/range {p5 .. p5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v3, v4

    const/4 v4, 0x3

    invoke-static/range {p6 .. p6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v3, v4

    const/4 v4, 0x4

    aput-object p10, v3, v4

    invoke-static {v1, v2, v3}, Lcom/google/android/gm/provider/LogUtils;->d(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)I

    cmp-long v1, p7, p1

    if-lez v1, :cond_2

    move-object/from16 v1, p0

    move-wide/from16 v2, p3

    move-object/from16 v4, p9

    move-object/from16 v5, p10

    move/from16 v6, p5

    move-wide/from16 v7, p7

    move/from16 v9, p6

    invoke-direct/range {v1 .. v9}, Lcom/google/android/gm/provider/Operations;->moveOperationToEnd(JLcom/google/android/gm/provider/Operations$OperationInfo;Lcom/google/android/gm/provider/MailEngine$SyncInfo;IJI)J

    const/4 v1, 0x0

    goto :goto_1

    :cond_2
    move-object/from16 v0, p10

    iget-boolean v1, v0, Lcom/google/android/gm/provider/MailEngine$SyncInfo;->receivedHandledClientOp:Z

    if-nez v1, :cond_3

    if-lez p5, :cond_3

    const-string v1, "Gmail"

    const-string v2, "Not retrying this operation id %d as we have not received what client operations the server has handled."

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    invoke-static/range {p3 .. p4}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v5

    aput-object v5, v3, v4

    invoke-static {v1, v2, v3}, Lcom/google/android/gm/provider/LogUtils;->i(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)I

    move-object/from16 v0, p11

    iget-object v1, v0, Lcom/google/android/gm/provider/MailEngine;->mMailSync:Lcom/google/android/gm/provider/MailSync;

    const-string v2, "unackedSentOperations"

    const/4 v3, 0x1

    invoke-virtual {v1, v2, v3}, Lcom/google/android/gm/provider/MailSync;->setBooleanSetting(Ljava/lang/String;Z)Z

    move-object/from16 v0, p11

    iget-object v1, v0, Lcom/google/android/gm/provider/MailEngine;->mMailSync:Lcom/google/android/gm/provider/MailSync;

    invoke-virtual {v1}, Lcom/google/android/gm/provider/MailSync;->saveDirtySettings()V

    move-object/from16 v1, p0

    move-wide/from16 v2, p3

    move-object/from16 v4, p9

    move-object/from16 v5, p10

    move/from16 v6, p5

    move-wide/from16 v7, p7

    move/from16 v9, p6

    invoke-direct/range {v1 .. v9}, Lcom/google/android/gm/provider/Operations;->moveOperationToEnd(JLcom/google/android/gm/provider/Operations$OperationInfo;Lcom/google/android/gm/provider/MailEngine$SyncInfo;IJI)J

    const/4 v1, 0x0

    goto/16 :goto_1

    :cond_3
    const/4 v1, 0x3

    move/from16 v0, p5

    if-lt v0, v1, :cond_5

    if-nez p6, :cond_4

    const/16 p6, 0x1e

    :goto_2
    move/from16 v0, p6

    int-to-long v1, v0

    add-long v7, p1, v1

    const/4 v12, 0x2

    const/4 v6, 0x2

    move-object/from16 v1, p0

    move-wide/from16 v2, p3

    move-object/from16 v4, p9

    move-object/from16 v5, p10

    move/from16 v9, p6

    invoke-direct/range {v1 .. v9}, Lcom/google/android/gm/provider/Operations;->moveOperationToEnd(JLcom/google/android/gm/provider/Operations$OperationInfo;Lcom/google/android/gm/provider/MailEngine$SyncInfo;IJI)J

    move-result-wide v13

    const-string v1, "Gmail"

    const-string v2, "Backing off operation %d with newAttempts %d, delay %d, newBackOffTime %d, newOpId %d"

    const/4 v3, 0x5

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    invoke-static/range {p3 .. p4}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v5

    aput-object v5, v3, v4

    const/4 v4, 0x1

    const/4 v5, 0x2

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v3, v4

    const/4 v4, 0x2

    invoke-static/range {p6 .. p6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v3, v4

    const/4 v4, 0x3

    invoke-static {v7, v8}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v5

    aput-object v5, v3, v4

    const/4 v4, 0x4

    invoke-static {v13, v14}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v5

    aput-object v5, v3, v4

    invoke-static {v1, v2, v3}, Lcom/google/android/gm/provider/LogUtils;->i(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)I

    const/4 v1, 0x0

    goto/16 :goto_1

    :cond_4
    const v1, 0x15180

    mul-int/lit8 v2, p6, 0x2

    invoke-static {v1, v2}, Ljava/lang/Math;->min(II)I

    move-result p6

    goto :goto_2

    :cond_5
    const/4 v1, 0x2

    new-array v15, v1, [Ljava/lang/String;

    const/4 v1, 0x0

    add-int/lit8 v2, p5, 0x1

    invoke-static {v2}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v2

    aput-object v2, v15, v1

    const/4 v1, 0x1

    invoke-static/range {p3 .. p4}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v2

    aput-object v2, v15, v1

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/google/android/gm/provider/Operations;->mDb:Landroid/database/sqlite/SQLiteDatabase;

    const-string v2, "UPDATE operations SET numAttempts = ? WHERE _id = ?"

    invoke-virtual {v1, v2, v15}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;[Ljava/lang/Object;)V

    const/4 v1, 0x1

    goto/16 :goto_1
.end method

.method private checkForMessageToDiscard(Lcom/google/android/gm/provider/MailEngine;)V
    .locals 16
    .param p1    # Lcom/google/android/gm/provider/MailEngine;

    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/google/android/gm/provider/Operations;->mContext:Landroid/content/Context;

    invoke-virtual {v12}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    const-string v12, "gmail_num_retry_uphill_op"

    sget-object v13, Lcom/google/android/gm/provider/Operations;->NUM_RETRY_UPHILL_OPS:Ljava/lang/Integer;

    invoke-virtual {v13}, Ljava/lang/Integer;->intValue()I

    move-result v13

    invoke-static {v1, v12, v13}, Lcom/google/android/gsf/Gservices;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v12

    invoke-static {v12}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v9

    invoke-virtual/range {p0 .. p0}, Lcom/google/android/gm/provider/Operations;->nextOperationId()I

    move-result v5

    move-object/from16 v0, p1

    iget-object v12, v0, Lcom/google/android/gm/provider/MailEngine;->mMailSync:Lcom/google/android/gm/provider/MailSync;

    const-string v13, "nextUnackedSentOp"

    invoke-virtual {v12, v13}, Lcom/google/android/gm/provider/MailSync;->getIntegerSetting(Ljava/lang/String;)I

    move-result v8

    move-object/from16 v0, p1

    iget-object v12, v0, Lcom/google/android/gm/provider/MailEngine;->mMailSync:Lcom/google/android/gm/provider/MailSync;

    const-string v13, "errorCountNextUnackedSentOp"

    invoke-virtual {v12, v13}, Lcom/google/android/gm/provider/MailSync;->getIntegerSetting(Ljava/lang/String;)I

    move-result v4

    move-object/from16 v0, p1

    iget-object v12, v0, Lcom/google/android/gm/provider/MailEngine;->mMailSync:Lcom/google/android/gm/provider/MailSync;

    const-string v13, "nextUnackedOpWriteTime"

    invoke-virtual {v12, v13}, Lcom/google/android/gm/provider/MailSync;->getLongSetting(Ljava/lang/String;)J

    move-result-wide v6

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v12

    const-wide/16 v14, 0x3e8

    div-long v2, v12, v14

    const-string v12, "gmail_wait_time_retry_uphill_op"

    const-wide/32 v13, 0x8e30

    invoke-static {v1, v12, v13, v14}, Lcom/google/android/gsf/Gservices;->getLong(Landroid/content/ContentResolver;Ljava/lang/String;J)J

    move-result-wide v10

    if-ne v5, v8, :cond_0

    invoke-virtual {v9}, Ljava/lang/Integer;->intValue()I

    move-result v12

    if-lt v4, v12, :cond_0

    sub-long v12, v2, v6

    cmp-long v12, v12, v10

    if-lez v12, :cond_0

    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/google/android/gm/provider/Operations;->mDb:Landroid/database/sqlite/SQLiteDatabase;

    const-string v13, "operations"

    new-instance v14, Ljava/lang/StringBuilder;

    invoke-direct {v14}, Ljava/lang/StringBuilder;-><init>()V

    const-string v15, "_id == "

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    invoke-virtual {v14, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v14

    invoke-virtual {v14}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v14

    const/4 v15, 0x0

    invoke-virtual {v12, v13, v14, v15}, Landroid/database/sqlite/SQLiteDatabase;->delete(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I

    :cond_0
    return-void
.end method

.method private doesLabelMatter(Ljava/lang/String;)Z
    .locals 1
    .param p1    # Ljava/lang/String;

    const-string v0, "messageSaved"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    const-string v0, "messageSent"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    const-string v0, "messageExpunged"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x1

    goto :goto_0
.end method

.method private moveOperationToEnd(JLcom/google/android/gm/provider/Operations$OperationInfo;Lcom/google/android/gm/provider/MailEngine$SyncInfo;IJI)J
    .locals 17
    .param p1    # J
    .param p3    # Lcom/google/android/gm/provider/Operations$OperationInfo;
    .param p4    # Lcom/google/android/gm/provider/MailEngine$SyncInfo;
    .param p5    # I
    .param p6    # J
    .param p8    # I

    move-object/from16 v0, p3

    iget-object v14, v0, Lcom/google/android/gm/provider/Operations$OperationInfo;->mAction:Ljava/lang/String;

    move-object/from16 v0, p4

    iget-boolean v1, v0, Lcom/google/android/gm/provider/MailEngine$SyncInfo;->normalSync:Z

    if-eqz v1, :cond_1

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/google/android/gm/provider/Operations;->mDb:Landroid/database/sqlite/SQLiteDatabase;

    const-string v2, "DELETE FROM operations where _id = ?"

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/String;

    const/4 v4, 0x0

    invoke-static/range {p1 .. p2}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v5

    aput-object v5, v3, v4

    invoke-virtual {v1, v2, v3}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;[Ljava/lang/Object;)V

    move-object/from16 v0, p0

    invoke-direct {v0, v14}, Lcom/google/android/gm/provider/Operations;->doesLabelMatter(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    move-object/from16 v0, p3

    iget-wide v2, v0, Lcom/google/android/gm/provider/Operations$OperationInfo;->mConversationId:J

    move-object/from16 v0, p3

    iget-wide v4, v0, Lcom/google/android/gm/provider/Operations$OperationInfo;->mMessageId:J

    move-object/from16 v0, p3

    iget-object v6, v0, Lcom/google/android/gm/provider/Operations$OperationInfo;->mAction:Ljava/lang/String;

    move-object/from16 v0, p3

    iget-wide v7, v0, Lcom/google/android/gm/provider/Operations$OperationInfo;->mLabelId:J

    move/from16 v0, p5

    int-to-long v9, v0

    move-object/from16 v1, p0

    move/from16 v11, p8

    move-wide/from16 v12, p6

    invoke-virtual/range {v1 .. v13}, Lcom/google/android/gm/provider/Operations;->recordOperationWithLabelId(JJLjava/lang/String;JJIJ)J

    move-result-wide v15

    :goto_0
    const-string v1, "Gmail"

    const-string v2, "Moving delayed operation %d to end of list with newAttempts %d, delay %d, newBackOffTime %d, newOpId %d"

    const/4 v3, 0x5

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    invoke-static/range {p1 .. p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v5

    aput-object v5, v3, v4

    const/4 v4, 0x1

    invoke-static/range {p5 .. p5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v3, v4

    const/4 v4, 0x2

    invoke-static/range {p8 .. p8}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v3, v4

    const/4 v4, 0x3

    invoke-static/range {p6 .. p7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v5

    aput-object v5, v3, v4

    const/4 v4, 0x4

    invoke-static/range {v15 .. v16}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v5

    aput-object v5, v3, v4

    invoke-static {v1, v2, v3}, Lcom/google/android/gm/provider/LogUtils;->i(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)I

    return-wide v15

    :cond_0
    move-object/from16 v0, p3

    iget-wide v2, v0, Lcom/google/android/gm/provider/Operations$OperationInfo;->mConversationId:J

    move-object/from16 v0, p3

    iget-wide v4, v0, Lcom/google/android/gm/provider/Operations$OperationInfo;->mMessageId:J

    move-object/from16 v0, p3

    iget-object v6, v0, Lcom/google/android/gm/provider/Operations$OperationInfo;->mAction:Ljava/lang/String;

    move/from16 v0, p5

    int-to-long v7, v0

    move-object/from16 v1, p0

    move/from16 v9, p8

    move-wide/from16 v10, p6

    invoke-virtual/range {v1 .. v11}, Lcom/google/android/gm/provider/Operations;->recordOperation(JJLjava/lang/String;JIJ)J

    move-result-wide v15

    goto :goto_0

    :cond_1
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/google/android/gm/provider/Operations;->mDb:Landroid/database/sqlite/SQLiteDatabase;

    const-string v2, "DELETE FROM operations where _id = ?"

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/String;

    const/4 v4, 0x0

    invoke-static/range {p1 .. p2}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v5

    aput-object v5, v3, v4

    invoke-virtual {v1, v2, v3}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;[Ljava/lang/Object;)V

    new-instance v1, Lcom/google/android/gm/provider/Operations$OperationInfo;

    move-object/from16 v0, p4

    iget-wide v2, v0, Lcom/google/android/gm/provider/MailEngine$SyncInfo;->conversationId:J

    move-object/from16 v0, p4

    iget-wide v4, v0, Lcom/google/android/gm/provider/MailEngine$SyncInfo;->messageId:J

    move-object/from16 v0, p3

    iget-object v6, v0, Lcom/google/android/gm/provider/Operations$OperationInfo;->mAction:Ljava/lang/String;

    const-wide/16 v7, 0x0

    move/from16 v9, p5

    move/from16 v10, p8

    move-wide/from16 v11, p6

    invoke-direct/range {v1 .. v12}, Lcom/google/android/gm/provider/Operations$OperationInfo;-><init>(JJLjava/lang/String;JIIJ)V

    move-object/from16 v0, p0

    invoke-virtual {v0, v1}, Lcom/google/android/gm/provider/Operations;->incrementAndAddOperations(Lcom/google/android/gm/provider/Operations$OperationInfo;)J

    move-result-wide v15

    goto :goto_0
.end method

.method public static updateLabelId(Landroid/database/sqlite/SQLiteDatabase;JJ)V
    .locals 3
    .param p0    # Landroid/database/sqlite/SQLiteDatabase;
    .param p1    # J
    .param p3    # J

    const/4 v1, 0x2

    new-array v0, v1, [Ljava/lang/String;

    const/4 v1, 0x0

    invoke-static {p3, p4}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x1

    invoke-static {p1, p2}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v2

    aput-object v2, v0, v1

    const-string v1, "UPDATE operations SET value1 = ? WHERE action IN (\'messageLabelAdded\', \'messageLabelRemoved\', \'conversationLabelAdded\', \'conversationLabelRemoved\') AND value1 = ?"

    invoke-virtual {p0, v1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;[Ljava/lang/Object;)V

    return-void
.end method


# virtual methods
.method public deleteOperationsForLabelId(J)V
    .locals 4
    .param p1    # J

    const/4 v1, 0x1

    new-array v0, v1, [Ljava/lang/String;

    const/4 v1, 0x0

    invoke-static {p1, p2}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v2

    aput-object v2, v0, v1

    iget-object v1, p0, Lcom/google/android/gm/provider/Operations;->mDb:Landroid/database/sqlite/SQLiteDatabase;

    const-string v2, "operations"

    const-string v3, "action IN (\'messageLabelAdded\', \'messageLabelRemoved\', \'conversationLabelAdded\', \'conversationLabelRemoved\') AND value1 = ?"

    invoke-virtual {v1, v2, v3, v0}, Landroid/database/sqlite/SQLiteDatabase;->delete(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I

    return-void
.end method

.method public deleteOperationsForMessageId(J)V
    .locals 6
    .param p1    # J

    iget-object v0, p0, Lcom/google/android/gm/provider/Operations;->mDb:Landroid/database/sqlite/SQLiteDatabase;

    const-string v1, "operations"

    const-string v2, "message_messageId = ?"

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/String;

    const/4 v4, 0x0

    invoke-static {p1, p2}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v5

    aput-object v5, v3, v4

    invoke-virtual {v0, v1, v2, v3}, Landroid/database/sqlite/SQLiteDatabase;->delete(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I

    return-void
.end method

.method public deleteOperationsForMessageIds(Ljava/util/List;)V
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Ljava/lang/Long;",
            ">;)V"
        }
    .end annotation

    const-string v1, ", "

    invoke-static {v1, p1}, Landroid/text/TextUtils;->join(Ljava/lang/CharSequence;Ljava/lang/Iterable;)Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/gm/provider/Operations;->mDb:Landroid/database/sqlite/SQLiteDatabase;

    const-string v2, "operations"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "message_messageId IN ("

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ")"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x0

    invoke-virtual {v1, v2, v3, v4}, Landroid/database/sqlite/SQLiteDatabase;->delete(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I

    return-void
.end method

.method public hasUnackedSendOrSaveOperationsForConversation(J)Z
    .locals 8
    .param p1    # J

    const/4 v0, 0x1

    const/4 v1, 0x0

    const-wide/16 v2, 0x0

    iget-object v4, p0, Lcom/google/android/gm/provider/Operations;->mDb:Landroid/database/sqlite/SQLiteDatabase;

    const-string v5, "SELECT COUNT(*) FROM operations WHERE ACTION IN (\'messageSaved\', \'messageSent\') AND value2 = ?"

    new-array v6, v0, [Ljava/lang/String;

    invoke-static {p1, p2}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v7

    aput-object v7, v6, v1

    invoke-static {v4, v5, v6}, Landroid/database/DatabaseUtils;->longForQuery(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;[Ljava/lang/String;)J

    move-result-wide v4

    cmp-long v2, v2, v4

    if-eqz v2, :cond_0

    :goto_0
    return v0

    :cond_0
    move v0, v1

    goto :goto_0
.end method

.method public incrementAndAddOperations(Lcom/google/android/gm/provider/Operations$OperationInfo;)J
    .locals 31
    .param p1    # Lcom/google/android/gm/provider/Operations$OperationInfo;

    const-wide/16 v20, -0x1

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/gm/provider/Operations;->mDb:Landroid/database/sqlite/SQLiteDatabase;

    invoke-virtual {v3}, Landroid/database/sqlite/SQLiteDatabase;->beginTransactionNonExclusive()V

    :try_start_0
    new-instance v24, Ljava/util/ArrayList;

    invoke-direct/range {v24 .. v24}, Ljava/util/ArrayList;-><init>()V

    new-instance v2, Landroid/database/sqlite/SQLiteQueryBuilder;

    invoke-direct {v2}, Landroid/database/sqlite/SQLiteQueryBuilder;-><init>()V

    const-string v3, "operations"

    invoke-virtual {v2, v3}, Landroid/database/sqlite/SQLiteQueryBuilder;->setTables(Ljava/lang/String;)V

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/gm/provider/Operations;->mDb:Landroid/database/sqlite/SQLiteDatabase;

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/gm/provider/Operations;->PROJECTION_PROVIDE_OPERATIONS:[Ljava/lang/String;

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    const/4 v8, 0x0

    const-string v9, "_id"

    invoke-virtual/range {v2 .. v9}, Landroid/database/sqlite/SQLiteQueryBuilder;->query(Landroid/database/sqlite/SQLiteDatabase;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v16

    const-string v3, "_id"

    move-object/from16 v0, v16

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v27

    const-string v3, "action"

    move-object/from16 v0, v16

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v15

    const-string v3, "message_messageId"

    move-object/from16 v0, v16

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v19

    const-string v3, "value1"

    move-object/from16 v0, v16

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v28

    const-string v3, "value2"

    move-object/from16 v0, v16

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v29

    const-string v3, "numAttempts"

    move-object/from16 v0, v16

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v23

    const-string v3, "nextTimeToAttempt"

    move-object/from16 v0, v16

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v22

    const-string v3, "delay"

    move-object/from16 v0, v16

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v17

    :goto_0
    invoke-interface/range {v16 .. v16}, Landroid/database/Cursor;->moveToNext()Z

    move-result v3

    if-eqz v3, :cond_0

    move-object/from16 v0, v16

    move/from16 v1, v27

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v26

    move-object/from16 v0, v16

    invoke-interface {v0, v15}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v8

    move-object/from16 v0, v16

    move/from16 v1, v19

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v6

    move-object/from16 v0, v16

    move/from16 v1, v23

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v11

    move-object/from16 v0, v16

    move/from16 v1, v22

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v13

    invoke-interface/range {v16 .. v17}, Landroid/database/Cursor;->getInt(I)I

    move-result v12

    move-object/from16 v0, v16

    move/from16 v1, v29

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v4

    move-object/from16 v0, v16

    move/from16 v1, v28

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v9

    new-instance v3, Lcom/google/android/gm/provider/Operations$OperationInfo;

    invoke-direct/range {v3 .. v14}, Lcom/google/android/gm/provider/Operations$OperationInfo;-><init>(JJLjava/lang/String;JIIJ)V

    move-object/from16 v0, v24

    invoke-virtual {v0, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v3

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/gm/provider/Operations;->mDb:Landroid/database/sqlite/SQLiteDatabase;

    move-object/from16 v30, v0

    invoke-virtual/range {v30 .. v30}, Landroid/database/sqlite/SQLiteDatabase;->endTransaction()V

    throw v3

    :cond_0
    :try_start_1
    invoke-interface/range {v16 .. v16}, Landroid/database/Cursor;->close()V

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/gm/provider/Operations;->mDb:Landroid/database/sqlite/SQLiteDatabase;

    const-string v30, "DELETE FROM operations"

    move-object/from16 v0, v30

    invoke-virtual {v3, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    invoke-virtual/range {p0 .. p1}, Lcom/google/android/gm/provider/Operations;->recordOperation(Lcom/google/android/gm/provider/Operations$OperationInfo;)J

    move-result-wide v20

    invoke-virtual/range {v24 .. v24}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v18

    :goto_1
    invoke-interface/range {v18 .. v18}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_1

    invoke-interface/range {v18 .. v18}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v25

    check-cast v25, Lcom/google/android/gm/provider/Operations$OperationInfo;

    move-object/from16 v0, p0

    move-object/from16 v1, v25

    invoke-virtual {v0, v1}, Lcom/google/android/gm/provider/Operations;->recordOperation(Lcom/google/android/gm/provider/Operations$OperationInfo;)J

    goto :goto_1

    :cond_1
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/gm/provider/Operations;->mDb:Landroid/database/sqlite/SQLiteDatabase;

    invoke-virtual {v3}, Landroid/database/sqlite/SQLiteDatabase;->setTransactionSuccessful()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/gm/provider/Operations;->mDb:Landroid/database/sqlite/SQLiteDatabase;

    invoke-virtual {v3}, Landroid/database/sqlite/SQLiteDatabase;->endTransaction()V

    return-wide v20
.end method

.method public nextOperationId()I
    .locals 4

    :try_start_0
    iget-object v1, p0, Lcom/google/android/gm/provider/Operations;->mDb:Landroid/database/sqlite/SQLiteDatabase;

    const-string v2, "SELECT _id FROM operations LIMIT 1"

    const/4 v3, 0x0

    invoke-static {v1, v2, v3}, Landroid/database/DatabaseUtils;->longForQuery(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;[Ljava/lang/String;)J
    :try_end_0
    .catch Landroid/database/sqlite/SQLiteDoneException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-wide v1

    long-to-int v1, v1

    :goto_0
    return v1

    :catch_0
    move-exception v0

    const/4 v1, 0x0

    goto :goto_0
.end method

.method public provideNormalOperations(Lcom/google/android/gm/provider/MailStore$OperationSink;Lcom/google/android/gm/provider/MailEngine;Lcom/google/android/gm/provider/MailEngine$SyncInfo;J)V
    .locals 51
    .param p1    # Lcom/google/android/gm/provider/MailStore$OperationSink;
    .param p2    # Lcom/google/android/gm/provider/MailEngine;
    .param p3    # Lcom/google/android/gm/provider/MailEngine$SyncInfo;
    .param p4    # J

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/google/android/gm/provider/Operations;->mContext:Landroid/content/Context;

    invoke-virtual {v6}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v6

    const-string v7, "gmail_discard_error_uphill_op_old_froyo"

    const/4 v8, 0x0

    invoke-static {v6, v7, v8}, Lcom/google/android/gsf/Gservices;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v6

    if-eqz v6, :cond_2

    const/16 v41, 0x1

    :goto_0
    if-eqz v41, :cond_0

    move-object/from16 v0, p0

    move-object/from16 v1, p2

    invoke-direct {v0, v1}, Lcom/google/android/gm/provider/Operations;->checkForMessageToDiscard(Lcom/google/android/gm/provider/MailEngine;)V

    :cond_0
    new-instance v5, Landroid/database/sqlite/SQLiteQueryBuilder;

    invoke-direct {v5}, Landroid/database/sqlite/SQLiteQueryBuilder;-><init>()V

    const-string v6, "operations"

    invoke-virtual {v5, v6}, Landroid/database/sqlite/SQLiteQueryBuilder;->setTables(Ljava/lang/String;)V

    const-string v13, "50"

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/google/android/gm/provider/Operations;->mDb:Landroid/database/sqlite/SQLiteDatabase;

    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/google/android/gm/provider/Operations;->PROJECTION_PROVIDE_OPERATIONS:[Ljava/lang/String;

    const/4 v8, 0x0

    const/4 v9, 0x0

    const/4 v10, 0x0

    const/4 v11, 0x0

    const-string v12, "_id"

    invoke-virtual/range {v5 .. v13}, Landroid/database/sqlite/SQLiteQueryBuilder;->query(Landroid/database/sqlite/SQLiteDatabase;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v42

    const-string v6, "_id"

    move-object/from16 v0, v42

    invoke-interface {v0, v6}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v47

    const-string v6, "action"

    move-object/from16 v0, v42

    invoke-interface {v0, v6}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v40

    const-string v6, "message_messageId"

    move-object/from16 v0, v42

    invoke-interface {v0, v6}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v44

    const-string v6, "value1"

    move-object/from16 v0, v42

    invoke-interface {v0, v6}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v49

    const-string v6, "value2"

    move-object/from16 v0, v42

    invoke-interface {v0, v6}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v50

    const-string v6, "numAttempts"

    move-object/from16 v0, v42

    invoke-interface {v0, v6}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v46

    const-string v6, "nextTimeToAttempt"

    move-object/from16 v0, v42

    invoke-interface {v0, v6}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v45

    const-string v6, "delay"

    move-object/from16 v0, v42

    invoke-interface {v0, v6}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v43

    :cond_1
    :goto_1
    invoke-interface/range {v42 .. v42}, Landroid/database/Cursor;->moveToNext()Z

    move-result v6

    if-eqz v6, :cond_b

    move-object/from16 v0, v42

    move/from16 v1, v47

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v25

    move-object/from16 v0, v42

    move/from16 v1, v40

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v19

    move-object/from16 v0, v42

    move/from16 v1, v44

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v17

    move-object/from16 v0, v42

    move/from16 v1, v46

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v27

    move-object/from16 v0, v42

    move/from16 v1, v45

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v29

    invoke-interface/range {v42 .. v43}, Landroid/database/Cursor;->getInt(I)I

    move-result v28

    move-object/from16 v0, v42

    move/from16 v1, v50

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v15

    move-object/from16 v0, v42

    move/from16 v1, v49

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v20

    new-instance v14, Lcom/google/android/gm/provider/Operations$OperationInfo;

    invoke-direct/range {v14 .. v21}, Lcom/google/android/gm/provider/Operations$OperationInfo;-><init>(JJLjava/lang/String;J)V

    move-object/from16 v22, p0

    move-wide/from16 v23, p4

    move-object/from16 v31, v14

    move-object/from16 v32, p3

    move-object/from16 v33, p2

    invoke-direct/range {v22 .. v33}, Lcom/google/android/gm/provider/Operations;->calculateAndUpdateOpDelay(JJIIJLcom/google/android/gm/provider/Operations$OperationInfo;Lcom/google/android/gm/provider/MailEngine$SyncInfo;Lcom/google/android/gm/provider/MailEngine;)Z

    move-result v48

    if-eqz v48, :cond_1

    const-string v6, "messageLabelAdded"

    move-object/from16 v0, v19

    invoke-virtual {v6, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_3

    move-object/from16 v6, p1

    move-wide/from16 v7, v25

    move-wide/from16 v9, v17

    move-wide/from16 v11, v20

    invoke-interface/range {v6 .. v12}, Lcom/google/android/gm/provider/MailStore$OperationSink;->messageLabelAdded(JJJ)V

    goto :goto_1

    :cond_2
    const/16 v41, 0x0

    goto/16 :goto_0

    :cond_3
    const-string v6, "messageLabelRemoved"

    move-object/from16 v0, v19

    invoke-virtual {v6, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_4

    move-object/from16 v6, p1

    move-wide/from16 v7, v25

    move-wide/from16 v9, v17

    move-wide/from16 v11, v20

    invoke-interface/range {v6 .. v12}, Lcom/google/android/gm/provider/MailStore$OperationSink;->messageLabelRemoved(JJJ)V

    goto/16 :goto_1

    :cond_4
    const-string v6, "conversationLabelAdded"

    move-object/from16 v0, v19

    invoke-virtual {v6, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_5

    const/16 v38, 0x1

    move-object/from16 v31, p1

    move-wide/from16 v32, v25

    move-wide/from16 v34, v17

    move-wide/from16 v36, v20

    invoke-interface/range {v31 .. v38}, Lcom/google/android/gm/provider/MailStore$OperationSink;->conversationLabelAddedOrRemoved(JJJZ)V

    goto/16 :goto_1

    :cond_5
    const-string v6, "conversationLabelRemoved"

    move-object/from16 v0, v19

    invoke-virtual {v6, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_6

    const/16 v38, 0x0

    move-object/from16 v31, p1

    move-wide/from16 v32, v25

    move-wide/from16 v34, v17

    move-wide/from16 v36, v20

    invoke-interface/range {v31 .. v38}, Lcom/google/android/gm/provider/MailStore$OperationSink;->conversationLabelAddedOrRemoved(JJJZ)V

    goto/16 :goto_1

    :cond_6
    const-string v6, "messageSaved"

    move-object/from16 v0, v19

    invoke-virtual {v6, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-nez v6, :cond_7

    const-string v6, "messageSent"

    move-object/from16 v0, v19

    invoke-virtual {v6, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_9

    :cond_7
    const/4 v6, 0x1

    move-object/from16 v0, p2

    move-wide/from16 v1, v17

    invoke-virtual {v0, v1, v2, v6}, Lcom/google/android/gm/provider/MailEngine;->getMessage(JZ)Lcom/google/android/gm/provider/MailSync$Message;

    move-result-object v34

    if-nez v34, :cond_8

    const-string v6, "Gmail"

    const-string v7, "Cannot find message with id = %d for operations!"

    const/4 v8, 0x1

    new-array v8, v8, [Ljava/lang/Object;

    const/4 v9, 0x0

    invoke-static/range {v17 .. v18}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v10

    aput-object v10, v8, v9

    invoke-static {v6, v7, v8}, Lcom/google/android/gm/provider/LogUtils;->e(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)I

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/google/android/gm/provider/Operations;->mDb:Landroid/database/sqlite/SQLiteDatabase;

    const-string v7, "operations"

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "_id == "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    move-wide/from16 v0, v25

    invoke-virtual {v8, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    const/4 v9, 0x0

    invoke-virtual {v6, v7, v8, v9}, Landroid/database/sqlite/SQLiteDatabase;->delete(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I

    goto/16 :goto_1

    :cond_8
    move-wide/from16 v35, v17

    move-object/from16 v0, v34

    iget-wide v0, v0, Lcom/google/android/gm/provider/MailSync$Message;->refMessageId:J

    move-wide/from16 v37, v0

    const-string v6, "messageSaved"

    move-object/from16 v0, v19

    invoke-virtual {v6, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v39

    move-object/from16 v31, p1

    move-wide/from16 v32, v25

    invoke-interface/range {v31 .. v39}, Lcom/google/android/gm/provider/MailStore$OperationSink;->messageSavedOrSent(JLcom/google/android/gm/provider/MailSync$Message;JJZ)V

    goto/16 :goto_1

    :cond_9
    const-string v6, "messageExpunged"

    move-object/from16 v0, v19

    invoke-virtual {v6, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_a

    move-object/from16 v0, p1

    move-wide/from16 v1, v25

    move-wide/from16 v3, v17

    invoke-interface {v0, v1, v2, v3, v4}, Lcom/google/android/gm/provider/MailStore$OperationSink;->messageExpunged(JJ)V

    goto/16 :goto_1

    :cond_a
    new-instance v6, Ljava/lang/RuntimeException;

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "Unknown action: "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    move-object/from16 v0, v19

    invoke-virtual {v7, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-direct {v6, v7}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v6

    :cond_b
    invoke-interface/range {v42 .. v42}, Landroid/database/Cursor;->close()V

    return-void
.end method

.method public provideOperations(Lcom/google/android/gm/provider/MailStore$OperationSink;Lcom/google/android/gm/provider/MailEngine;Lcom/google/android/gm/provider/MailEngine$SyncInfo;J)V
    .locals 33
    .param p1    # Lcom/google/android/gm/provider/MailStore$OperationSink;
    .param p2    # Lcom/google/android/gm/provider/MailEngine;
    .param p3    # Lcom/google/android/gm/provider/MailEngine$SyncInfo;
    .param p4    # J

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/gm/provider/Operations;->mDb:Landroid/database/sqlite/SQLiteDatabase;

    const-string v4, "SELECT \n  _id,\n  action,\n  numAttempts,\n  nextTimeToAttempt,\n  delay\nFROM\n operations\nWHERE\n  message_messageId = ? AND value2 = ?\n"

    const/4 v5, 0x2

    new-array v5, v5, [Ljava/lang/String;

    const/4 v6, 0x0

    move-object/from16 v0, p3

    iget-wide v8, v0, Lcom/google/android/gm/provider/MailEngine$SyncInfo;->messageId:J

    invoke-static {v8, v9}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v8

    aput-object v8, v5, v6

    const/4 v6, 0x1

    move-object/from16 v0, p3

    iget-wide v8, v0, Lcom/google/android/gm/provider/MailEngine$SyncInfo;->conversationId:J

    invoke-static {v8, v9}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v8

    aput-object v8, v5, v6

    invoke-virtual {v3, v4, v5}, Landroid/database/sqlite/SQLiteDatabase;->rawQuery(Ljava/lang/String;[Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v27

    const-string v3, "_id"

    move-object/from16 v0, v27

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v31

    const-string v3, "action"

    move-object/from16 v0, v27

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v26

    const-string v3, "numAttempts"

    move-object/from16 v0, v27

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v30

    const-string v3, "nextTimeToAttempt"

    move-object/from16 v0, v27

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v29

    const-string v3, "delay"

    move-object/from16 v0, v27

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v28

    :goto_0
    invoke-interface/range {v27 .. v27}, Landroid/database/Cursor;->moveToNext()Z

    move-result v3

    if-eqz v3, :cond_3

    move-object/from16 v0, v27

    move/from16 v1, v31

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v11

    move-object/from16 v0, v27

    move/from16 v1, v26

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v7

    move-object/from16 v0, v27

    move/from16 v1, v30

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v13

    move-object/from16 v0, v27

    move/from16 v1, v29

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v15

    invoke-interface/range {v27 .. v28}, Landroid/database/Cursor;->getInt(I)I

    move-result v14

    new-instance v2, Lcom/google/android/gm/provider/Operations$OperationInfo;

    move-object/from16 v0, p3

    iget-wide v3, v0, Lcom/google/android/gm/provider/MailEngine$SyncInfo;->conversationId:J

    move-object/from16 v0, p3

    iget-wide v5, v0, Lcom/google/android/gm/provider/MailEngine$SyncInfo;->messageId:J

    const-wide/16 v8, 0x0

    invoke-direct/range {v2 .. v9}, Lcom/google/android/gm/provider/Operations$OperationInfo;-><init>(JJLjava/lang/String;J)V

    const-string v3, "messageSent"

    invoke-virtual {v3, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_1

    :cond_0
    :goto_1
    return-void

    :cond_1
    move-object/from16 v8, p0

    move-wide/from16 v9, p4

    move-object/from16 v17, v2

    move-object/from16 v18, p3

    move-object/from16 v19, p2

    invoke-direct/range {v8 .. v19}, Lcom/google/android/gm/provider/Operations;->calculateAndUpdateOpDelay(JJIIJLcom/google/android/gm/provider/Operations$OperationInfo;Lcom/google/android/gm/provider/MailEngine$SyncInfo;Lcom/google/android/gm/provider/MailEngine;)Z

    move-result v32

    if-eqz v32, :cond_0

    move-object/from16 v0, p3

    iget-wide v3, v0, Lcom/google/android/gm/provider/MailEngine$SyncInfo;->messageId:J

    const/4 v5, 0x1

    move-object/from16 v0, p2

    invoke-virtual {v0, v3, v4, v5}, Lcom/google/android/gm/provider/MailEngine;->getMessage(JZ)Lcom/google/android/gm/provider/MailSync$Message;

    move-result-object v20

    if-nez v20, :cond_2

    const-string v3, "Gmail"

    const-string v4, "Cannot find message with id = %d for operations!"

    const/4 v5, 0x1

    new-array v5, v5, [Ljava/lang/Object;

    const/4 v6, 0x0

    move-object/from16 v0, p3

    iget-wide v8, v0, Lcom/google/android/gm/provider/MailEngine$SyncInfo;->messageId:J

    invoke-static {v8, v9}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v8

    aput-object v8, v5, v6

    invoke-static {v3, v4, v5}, Lcom/google/android/gm/provider/LogUtils;->e(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)I

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/gm/provider/Operations;->mDb:Landroid/database/sqlite/SQLiteDatabase;

    const-string v4, "operations"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "_id == "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v11, v12}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    const/4 v6, 0x0

    invoke-virtual {v3, v4, v5, v6}, Landroid/database/sqlite/SQLiteDatabase;->delete(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I

    goto/16 :goto_0

    :cond_2
    move-object/from16 v0, p3

    iget-wide v0, v0, Lcom/google/android/gm/provider/MailEngine$SyncInfo;->messageId:J

    move-wide/from16 v21, v0

    move-object/from16 v0, v20

    iget-wide v0, v0, Lcom/google/android/gm/provider/MailSync$Message;->refMessageId:J

    move-wide/from16 v23, v0

    const/16 v25, 0x0

    move-object/from16 v17, p1

    move-wide/from16 v18, v11

    invoke-interface/range {v17 .. v25}, Lcom/google/android/gm/provider/MailStore$OperationSink;->messageSavedOrSent(JLcom/google/android/gm/provider/MailSync$Message;JJZ)V

    goto/16 :goto_0

    :cond_3
    invoke-interface/range {v27 .. v27}, Landroid/database/Cursor;->close()V

    goto :goto_1
.end method

.method public recordOperation(JJLjava/lang/String;)J
    .locals 11
    .param p1    # J
    .param p3    # J
    .param p5    # Ljava/lang/String;

    const-wide/16 v6, 0x0

    const/4 v8, 0x0

    const-wide/16 v9, 0x0

    move-object v0, p0

    move-wide v1, p1

    move-wide v3, p3

    move-object/from16 v5, p5

    invoke-virtual/range {v0 .. v10}, Lcom/google/android/gm/provider/Operations;->recordOperation(JJLjava/lang/String;JIJ)J

    move-result-wide v0

    return-wide v0
.end method

.method public recordOperation(JJLjava/lang/String;JIJ)J
    .locals 5
    .param p1    # J
    .param p3    # J
    .param p5    # Ljava/lang/String;
    .param p6    # J
    .param p8    # I
    .param p9    # J

    const-wide/16 v3, 0x0

    new-instance v0, Landroid/content/ContentValues;

    invoke-direct {v0}, Landroid/content/ContentValues;-><init>()V

    const-string v1, "action"

    invoke-virtual {v0, v1, p5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    const-string v1, "message_messageId"

    invoke-static {p3, p4}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    const-string v1, "value2"

    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    cmp-long v1, p6, v3

    if-lez v1, :cond_0

    cmp-long v1, p9, v3

    if-lez v1, :cond_0

    const-string v1, "numAttempts"

    invoke-static {p6, p7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    const-string v1, "nextTimeToAttempt"

    invoke-static {p9, p10}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    const-string v1, "delay"

    invoke-static {p8}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    :cond_0
    iget-object v1, p0, Lcom/google/android/gm/provider/Operations;->mDb:Landroid/database/sqlite/SQLiteDatabase;

    const-string v2, "operations"

    const/4 v3, 0x0

    invoke-virtual {v1, v2, v3, v0}, Landroid/database/sqlite/SQLiteDatabase;->insert(Ljava/lang/String;Ljava/lang/String;Landroid/content/ContentValues;)J

    move-result-wide v1

    return-wide v1
.end method

.method public recordOperation(Lcom/google/android/gm/provider/Operations$OperationInfo;)J
    .locals 13
    .param p1    # Lcom/google/android/gm/provider/Operations$OperationInfo;

    iget-object v0, p1, Lcom/google/android/gm/provider/Operations$OperationInfo;->mAction:Ljava/lang/String;

    invoke-direct {p0, v0}, Lcom/google/android/gm/provider/Operations;->doesLabelMatter(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-wide v1, p1, Lcom/google/android/gm/provider/Operations$OperationInfo;->mConversationId:J

    iget-wide v3, p1, Lcom/google/android/gm/provider/Operations$OperationInfo;->mMessageId:J

    iget-object v5, p1, Lcom/google/android/gm/provider/Operations$OperationInfo;->mAction:Ljava/lang/String;

    iget-wide v6, p1, Lcom/google/android/gm/provider/Operations$OperationInfo;->mLabelId:J

    iget v0, p1, Lcom/google/android/gm/provider/Operations$OperationInfo;->mNumAttempts:I

    int-to-long v8, v0

    iget v10, p1, Lcom/google/android/gm/provider/Operations$OperationInfo;->mDelay:I

    iget-wide v11, p1, Lcom/google/android/gm/provider/Operations$OperationInfo;->mNextTimeToAttempt:J

    move-object v0, p0

    invoke-virtual/range {v0 .. v12}, Lcom/google/android/gm/provider/Operations;->recordOperationWithLabelId(JJLjava/lang/String;JJIJ)J

    move-result-wide v0

    :goto_0
    return-wide v0

    :cond_0
    iget-wide v1, p1, Lcom/google/android/gm/provider/Operations$OperationInfo;->mConversationId:J

    iget-wide v3, p1, Lcom/google/android/gm/provider/Operations$OperationInfo;->mMessageId:J

    iget-object v5, p1, Lcom/google/android/gm/provider/Operations$OperationInfo;->mAction:Ljava/lang/String;

    iget v0, p1, Lcom/google/android/gm/provider/Operations$OperationInfo;->mNumAttempts:I

    int-to-long v6, v0

    iget v8, p1, Lcom/google/android/gm/provider/Operations$OperationInfo;->mDelay:I

    iget-wide v9, p1, Lcom/google/android/gm/provider/Operations$OperationInfo;->mNextTimeToAttempt:J

    move-object v0, p0

    invoke-virtual/range {v0 .. v10}, Lcom/google/android/gm/provider/Operations;->recordOperation(JJLjava/lang/String;JIJ)J

    move-result-wide v0

    goto :goto_0
.end method

.method public recordOperationWithLabelId(JJLjava/lang/String;J)J
    .locals 13
    .param p1    # J
    .param p3    # J
    .param p5    # Ljava/lang/String;
    .param p6    # J

    const-wide/16 v8, 0x0

    const/4 v10, 0x0

    const-wide/16 v11, 0x0

    move-object v0, p0

    move-wide v1, p1

    move-wide/from16 v3, p3

    move-object/from16 v5, p5

    move-wide/from16 v6, p6

    invoke-virtual/range {v0 .. v12}, Lcom/google/android/gm/provider/Operations;->recordOperationWithLabelId(JJLjava/lang/String;JJIJ)J

    move-result-wide v0

    return-wide v0
.end method

.method public recordOperationWithLabelId(JJLjava/lang/String;JJIJ)J
    .locals 4
    .param p1    # J
    .param p3    # J
    .param p5    # Ljava/lang/String;
    .param p6    # J
    .param p8    # J
    .param p10    # I
    .param p11    # J

    new-instance v0, Landroid/content/ContentValues;

    invoke-direct {v0}, Landroid/content/ContentValues;-><init>()V

    const-string v1, "action"

    invoke-virtual {v0, v1, p5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    const-string v1, "message_messageId"

    invoke-static {p3, p4}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    const-string v1, "value1"

    invoke-static {p6, p7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    const-string v1, "value2"

    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    const-wide/16 v1, 0x0

    cmp-long v1, p8, v1

    if-lez v1, :cond_0

    const-wide/16 v1, 0x0

    cmp-long v1, p11, v1

    if-lez v1, :cond_0

    const-string v1, "numAttempts"

    invoke-static {p8, p9}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    const-string v1, "nextTimeToAttempt"

    invoke-static/range {p11 .. p12}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    const-string v1, "delay"

    invoke-static {p10}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    :cond_0
    iget-object v1, p0, Lcom/google/android/gm/provider/Operations;->mDb:Landroid/database/sqlite/SQLiteDatabase;

    const-string v2, "operations"

    const/4 v3, 0x0

    invoke-virtual {v1, v2, v3, v0}, Landroid/database/sqlite/SQLiteDatabase;->insert(Ljava/lang/String;Ljava/lang/String;Landroid/content/ContentValues;)J

    move-result-wide v1

    return-wide v1
.end method

.method public updateMessageId(JJ)V
    .locals 5
    .param p1    # J
    .param p3    # J

    new-instance v0, Landroid/content/ContentValues;

    invoke-direct {v0}, Landroid/content/ContentValues;-><init>()V

    const-string v1, "message_messageId"

    invoke-static {p3, p4}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    iget-object v1, p0, Lcom/google/android/gm/provider/Operations;->mDb:Landroid/database/sqlite/SQLiteDatabase;

    const-string v2, "operations"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "message_messageId = "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1, p2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x0

    invoke-virtual {v1, v2, v0, v3, v4}, Landroid/database/sqlite/SQLiteDatabase;->update(Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    return-void
.end method
