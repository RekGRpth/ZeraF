.class public Lcom/google/android/gm/provider/AppDataSearch;
.super Ljava/lang/Object;
.source "AppDataSearch.java"


# static fields
.field private static sAvailability:I

.field private static final sQuerySpec:Lcom/google/android/gms/appdatasearch/QuerySpecification;

.field private static final sSectionInfo:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/android/gms/appdatasearch/RegisterSectionInfo;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private final mClient:Lcom/google/android/gms/appdatasearch/AppDataSearchClient;

.field private final mContext:Landroid/content/Context;


# direct methods
.method static constructor <clinit>()V
    .locals 12

    const/4 v11, 0x2

    const/4 v10, 0x1

    const/4 v9, 0x0

    sput v9, Lcom/google/android/gm/provider/AppDataSearch;->sAvailability:I

    new-array v1, v11, [Ljava/lang/String;

    const-string v8, "conversation"

    aput-object v8, v1, v9

    const-string v8, "message_id"

    aput-object v8, v1, v10

    const/4 v8, 0x4

    new-array v0, v8, [Ljava/lang/String;

    const-string v8, "from_address"

    aput-object v8, v0, v9

    const-string v8, "to_addresses"

    aput-object v8, v0, v10

    const-string v8, "subject"

    aput-object v8, v0, v11

    const/4 v8, 0x3

    const-string v9, "body"

    aput-object v9, v0, v8

    array-length v8, v1

    array-length v9, v0

    add-int/2addr v8, v9

    invoke-static {v8}, Lcom/google/common/collect/Lists;->newArrayListWithExpectedSize(I)Ljava/util/ArrayList;

    move-result-object v7

    move-object v2, v1

    array-length v5, v2

    const/4 v3, 0x0

    :goto_0
    if-ge v3, v5, :cond_0

    aget-object v6, v2, v3

    new-instance v4, Lcom/google/android/gms/appdatasearch/RegisterSectionInfo;

    const-string v8, "plain"

    invoke-direct {v4, v6, v8, v10}, Lcom/google/android/gms/appdatasearch/RegisterSectionInfo;-><init>(Ljava/lang/String;Ljava/lang/String;Z)V

    invoke-interface {v7, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    :cond_0
    move-object v2, v0

    array-length v5, v2

    const/4 v3, 0x0

    :goto_1
    if-ge v3, v5, :cond_3

    aget-object v6, v2, v3

    const/4 v4, 0x0

    const-string v8, "body"

    invoke-virtual {v6, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_1

    new-instance v4, Lcom/google/android/gms/appdatasearch/RegisterSectionInfo;

    const-string v8, "html"

    invoke-direct {v4, v6, v8}, Lcom/google/android/gms/appdatasearch/RegisterSectionInfo;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    :goto_2
    invoke-interface {v7, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    add-int/lit8 v3, v3, 0x1

    goto :goto_1

    :cond_1
    const-string v8, "address"

    invoke-virtual {v6, v8}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v8

    if-eqz v8, :cond_2

    new-instance v4, Lcom/google/android/gms/appdatasearch/RegisterSectionInfo;

    const-string v8, "rfc822"

    invoke-direct {v4, v6, v8}, Lcom/google/android/gms/appdatasearch/RegisterSectionInfo;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_2

    :cond_2
    new-instance v4, Lcom/google/android/gms/appdatasearch/RegisterSectionInfo;

    const-string v8, "plain"

    invoke-direct {v4, v6, v8}, Lcom/google/android/gms/appdatasearch/RegisterSectionInfo;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_2

    :cond_3
    sput-object v7, Lcom/google/android/gm/provider/AppDataSearch;->sSectionInfo:Ljava/util/List;

    new-instance v8, Lcom/google/android/gms/appdatasearch/QuerySpecification;

    invoke-direct {v8}, Lcom/google/android/gms/appdatasearch/QuerySpecification;-><init>()V

    const-string v9, "^f"

    invoke-virtual {v8, v9}, Lcom/google/android/gms/appdatasearch/QuerySpecification;->addWantedTags(Ljava/lang/String;)Lcom/google/android/gms/appdatasearch/QuerySpecification;

    move-result-object v8

    const-string v9, "conversation"

    invoke-virtual {v8, v9}, Lcom/google/android/gms/appdatasearch/QuerySpecification;->addFullSection(Ljava/lang/String;)Lcom/google/android/gms/appdatasearch/QuerySpecification;

    move-result-object v8

    const-string v9, "body"

    const/16 v10, 0x50

    invoke-virtual {v8, v9, v10}, Lcom/google/android/gms/appdatasearch/QuerySpecification;->addSnippetedSection(Ljava/lang/String;I)Lcom/google/android/gms/appdatasearch/QuerySpecification;

    move-result-object v8

    sput-object v8, Lcom/google/android/gm/provider/AppDataSearch;->sQuerySpec:Lcom/google/android/gms/appdatasearch/QuerySpecification;

    return-void
.end method

.method private constructor <init>(Landroid/content/Context;)V
    .locals 2
    .param p1    # Landroid/content/Context;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/google/android/gm/provider/AppDataSearch;->mContext:Landroid/content/Context;

    new-instance v0, Lcom/google/android/gms/appdatasearch/AppDataSearchClient;

    iget-object v1, p0, Lcom/google/android/gm/provider/AppDataSearch;->mContext:Landroid/content/Context;

    invoke-direct {v0, v1}, Lcom/google/android/gms/appdatasearch/AppDataSearchClient;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/google/android/gm/provider/AppDataSearch;->mClient:Lcom/google/android/gms/appdatasearch/AppDataSearchClient;

    return-void
.end method

.method public static createIfAvailable(Landroid/content/Context;)Lcom/google/android/gm/provider/AppDataSearch;
    .locals 1
    .param p0    # Landroid/content/Context;

    invoke-static {p0}, Lcom/google/android/gm/provider/AppDataSearch;->isAvailable(Landroid/content/Context;)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_0
    new-instance v0, Lcom/google/android/gm/provider/AppDataSearch;

    invoke-direct {v0, p0}, Lcom/google/android/gm/provider/AppDataSearch;-><init>(Landroid/content/Context;)V

    goto :goto_0
.end method

.method private getRegisterCorpusInfo(Ljava/lang/String;)Lcom/google/android/gms/appdatasearch/RegisterCorpusInfo;
    .locals 4
    .param p1    # Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "content://gmail-ls/"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "/appdatasearch"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    new-instance v1, Lcom/google/android/gms/appdatasearch/RegisterCorpusInfo;

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v2

    sget-object v3, Lcom/google/android/gm/provider/AppDataSearch;->sSectionInfo:Ljava/util/List;

    invoke-direct {v1, p1, v2, v3}, Lcom/google/android/gms/appdatasearch/RegisterCorpusInfo;-><init>(Ljava/lang/String;Landroid/net/Uri;Ljava/util/List;)V

    return-object v1
.end method

.method public static isAvailable(Landroid/content/Context;)Z
    .locals 7
    .param p0    # Landroid/content/Context;

    const/4 v2, 0x0

    const/4 v1, 0x1

    sget v3, Lcom/google/android/gm/provider/AppDataSearch;->sAvailability:I

    if-nez v3, :cond_0

    invoke-static {p0}, Lcom/google/android/gms/common/GooglePlayServicesUtil;->isGooglePlayServicesAvailable(Landroid/content/Context;)I

    move-result v0

    if-nez v0, :cond_1

    sput v1, Lcom/google/android/gm/provider/AppDataSearch;->sAvailability:I

    :cond_0
    :goto_0
    sget v3, Lcom/google/android/gm/provider/AppDataSearch;->sAvailability:I

    if-ne v3, v1, :cond_2

    :goto_1
    return v1

    :cond_1
    const/4 v3, 0x2

    sput v3, Lcom/google/android/gm/provider/AppDataSearch;->sAvailability:I

    const-string v3, "Gmail"

    const-string v4, "Google play services not available: %d"

    new-array v5, v1, [Ljava/lang/Object;

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    aput-object v6, v5, v2

    invoke-static {v3, v4, v5}, Lcom/google/android/gm/provider/LogUtils;->e(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)I

    goto :goto_0

    :cond_2
    move v1, v2

    goto :goto_1
.end method


# virtual methods
.method public onAccountsUpdated([Landroid/accounts/Account;)V
    .locals 16
    .param p1    # [Landroid/accounts/Account;

    move-object/from16 v0, p1

    array-length v10, v0

    invoke-static {v10}, Lcom/google/common/collect/Sets;->newHashSetWithExpectedSize(I)Ljava/util/HashSet;

    move-result-object v8

    move-object/from16 v2, p1

    array-length v7, v2

    const/4 v5, 0x0

    :goto_0
    if-ge v5, v7, :cond_1

    aget-object v1, v2, v5

    iget-object v10, v1, Landroid/accounts/Account;->type:Ljava/lang/String;

    const-string v11, "com.google"

    invoke-virtual {v10, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-nez v10, :cond_0

    :goto_1
    add-int/lit8 v5, v5, 0x1

    goto :goto_0

    :cond_0
    iget-object v10, v1, Landroid/accounts/Account;->name:Ljava/lang/String;

    invoke-interface {v8, v10}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    goto :goto_1

    :cond_1
    invoke-interface {v8}, Ljava/util/Set;->size()I

    move-result v10

    invoke-static {v10}, Lcom/google/common/collect/Lists;->newArrayListWithExpectedSize(I)Ljava/util/ArrayList;

    move-result-object v3

    invoke-interface {v8}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v5

    :goto_2
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v10

    if-eqz v10, :cond_2

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    move-object/from16 v0, p0

    invoke-direct {v0, v1}, Lcom/google/android/gm/provider/AppDataSearch;->getRegisterCorpusInfo(Ljava/lang/String;)Lcom/google/android/gms/appdatasearch/RegisterCorpusInfo;

    move-result-object v10

    invoke-interface {v3, v10}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_2

    :cond_2
    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/google/android/gm/provider/AppDataSearch;->mClient:Lcom/google/android/gms/appdatasearch/AppDataSearchClient;

    monitor-enter v11

    :try_start_0
    move-object/from16 v0, p0

    iget-object v10, v0, Lcom/google/android/gm/provider/AppDataSearch;->mClient:Lcom/google/android/gms/appdatasearch/AppDataSearchClient;

    const-wide/16 v12, 0x7530

    invoke-virtual {v10, v12, v13}, Lcom/google/android/gms/appdatasearch/AppDataSearchClient;->connectWithTimeout(J)Lcom/google/android/gms/common/ConnectionResult;

    move-result-object v9

    invoke-virtual {v9}, Lcom/google/android/gms/common/ConnectionResult;->isSuccess()Z

    move-result v10

    if-nez v10, :cond_3

    const-string v10, "Gmail"

    const-string v12, "Search registration failed: %d"

    const/4 v13, 0x1

    new-array v13, v13, [Ljava/lang/Object;

    const/4 v14, 0x0

    invoke-virtual {v9}, Lcom/google/android/gms/common/ConnectionResult;->getErrorCode()I

    move-result v15

    invoke-static {v15}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v15

    aput-object v15, v13, v14

    invoke-static {v10, v12, v13}, Lcom/google/android/gm/provider/LogUtils;->e(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)I

    monitor-exit v11
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :goto_3
    return-void

    :cond_3
    :try_start_1
    move-object/from16 v0, p0

    iget-object v10, v0, Lcom/google/android/gm/provider/AppDataSearch;->mClient:Lcom/google/android/gms/appdatasearch/AppDataSearchClient;

    invoke-virtual {v10, v3}, Lcom/google/android/gms/appdatasearch/AppDataSearchClient;->setRegisteredCorpora(Ljava/util/Collection;)Z
    :try_end_1
    .catch Ljava/lang/NullPointerException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Ljava/lang/IllegalStateException; {:try_start_1 .. :try_end_1} :catch_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    :try_start_2
    move-object/from16 v0, p0

    iget-object v10, v0, Lcom/google/android/gm/provider/AppDataSearch;->mClient:Lcom/google/android/gms/appdatasearch/AppDataSearchClient;

    invoke-virtual {v10}, Lcom/google/android/gms/appdatasearch/AppDataSearchClient;->disconnect()V

    :goto_4
    monitor-exit v11

    goto :goto_3

    :catchall_0
    move-exception v10

    monitor-exit v11
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    throw v10

    :catch_0
    move-exception v4

    :try_start_3
    const-string v10, "Gmail"

    const-string v12, "Error registering corpora"

    const/4 v13, 0x0

    new-array v13, v13, [Ljava/lang/Object;

    invoke-static {v10, v4, v12, v13}, Lcom/google/android/gm/provider/LogUtils;->e(Ljava/lang/String;Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)I
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    :try_start_4
    move-object/from16 v0, p0

    iget-object v10, v0, Lcom/google/android/gm/provider/AppDataSearch;->mClient:Lcom/google/android/gms/appdatasearch/AppDataSearchClient;

    invoke-virtual {v10}, Lcom/google/android/gms/appdatasearch/AppDataSearchClient;->disconnect()V
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    goto :goto_4

    :catch_1
    move-exception v6

    :try_start_5
    const-string v10, "Gmail"

    const-string v12, "Error registering corpora"

    const/4 v13, 0x0

    new-array v13, v13, [Ljava/lang/Object;

    invoke-static {v10, v6, v12, v13}, Lcom/google/android/gm/provider/LogUtils;->e(Ljava/lang/String;Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)I
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_1

    :try_start_6
    move-object/from16 v0, p0

    iget-object v10, v0, Lcom/google/android/gm/provider/AppDataSearch;->mClient:Lcom/google/android/gms/appdatasearch/AppDataSearchClient;

    invoke-virtual {v10}, Lcom/google/android/gms/appdatasearch/AppDataSearchClient;->disconnect()V

    goto :goto_4

    :catchall_1
    move-exception v10

    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/google/android/gm/provider/AppDataSearch;->mClient:Lcom/google/android/gms/appdatasearch/AppDataSearchClient;

    invoke-virtual {v12}, Lcom/google/android/gms/appdatasearch/AppDataSearchClient;->disconnect()V

    throw v10
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_0
.end method

.method public query(Ljava/lang/String;Ljava/lang/String;II)Lcom/google/android/gms/appdatasearch/SearchResults;
    .locals 11
    .param p1    # Ljava/lang/String;
    .param p2    # Ljava/lang/String;
    .param p3    # I
    .param p4    # I

    const/4 v9, 0x0

    iget-object v10, p0, Lcom/google/android/gm/provider/AppDataSearch;->mClient:Lcom/google/android/gms/appdatasearch/AppDataSearchClient;

    monitor-enter v10

    :try_start_0
    iget-object v0, p0, Lcom/google/android/gm/provider/AppDataSearch;->mClient:Lcom/google/android/gms/appdatasearch/AppDataSearchClient;

    const-wide/16 v1, 0x2710

    invoke-virtual {v0, v1, v2}, Lcom/google/android/gms/appdatasearch/AppDataSearchClient;->connectWithTimeout(J)Lcom/google/android/gms/common/ConnectionResult;

    move-result-object v8

    invoke-virtual {v8}, Lcom/google/android/gms/common/ConnectionResult;->isSuccess()Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    if-eqz v0, :cond_1

    :try_start_1
    iget-object v0, p0, Lcom/google/android/gm/provider/AppDataSearch;->mClient:Lcom/google/android/gms/appdatasearch/AppDataSearchClient;

    const/4 v1, 0x1

    new-array v2, v1, [Ljava/lang/String;

    const/4 v1, 0x0

    aput-object p2, v2, v1

    sget-object v5, Lcom/google/android/gm/provider/AppDataSearch;->sQuerySpec:Lcom/google/android/gms/appdatasearch/QuerySpecification;

    move-object v1, p1

    move v3, p3

    move v4, p4

    invoke-virtual/range {v0 .. v5}, Lcom/google/android/gms/appdatasearch/AppDataSearchClient;->query(Ljava/lang/String;[Ljava/lang/String;IILcom/google/android/gms/appdatasearch/QuerySpecification;)Lcom/google/android/gms/appdatasearch/SearchResults;

    move-result-object v9

    invoke-virtual {v9}, Lcom/google/android/gms/appdatasearch/SearchResults;->hasError()Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v0, "Gmail"

    const-string v1, "Error searching: %s"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    invoke-virtual {v9}, Lcom/google/android/gms/appdatasearch/SearchResults;->getErrorMessage()Ljava/lang/String;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-static {v0, v1, v2}, Lcom/google/android/gm/provider/LogUtils;->e(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)I
    :try_end_1
    .catch Ljava/lang/NullPointerException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Ljava/lang/IllegalStateException; {:try_start_1 .. :try_end_1} :catch_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    const/4 v9, 0x0

    :cond_0
    :try_start_2
    iget-object v0, p0, Lcom/google/android/gm/provider/AppDataSearch;->mClient:Lcom/google/android/gms/appdatasearch/AppDataSearchClient;

    invoke-virtual {v0}, Lcom/google/android/gms/appdatasearch/AppDataSearchClient;->disconnect()V

    :goto_0
    monitor-exit v10
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    return-object v9

    :catch_0
    move-exception v6

    :try_start_3
    const-string v0, "Gmail"

    const-string v1, "Error executing search query"

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {v0, v6, v1, v2}, Lcom/google/android/gm/provider/LogUtils;->e(Ljava/lang/String;Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)I
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    :try_start_4
    iget-object v0, p0, Lcom/google/android/gm/provider/AppDataSearch;->mClient:Lcom/google/android/gms/appdatasearch/AppDataSearchClient;

    invoke-virtual {v0}, Lcom/google/android/gms/appdatasearch/AppDataSearchClient;->disconnect()V

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit v10
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    throw v0

    :catch_1
    move-exception v7

    :try_start_5
    const-string v0, "Gmail"

    const-string v1, "Error executing search query"

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {v0, v7, v1, v2}, Lcom/google/android/gm/provider/LogUtils;->e(Ljava/lang/String;Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)I
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_1

    :try_start_6
    iget-object v0, p0, Lcom/google/android/gm/provider/AppDataSearch;->mClient:Lcom/google/android/gms/appdatasearch/AppDataSearchClient;

    invoke-virtual {v0}, Lcom/google/android/gms/appdatasearch/AppDataSearchClient;->disconnect()V

    goto :goto_0

    :catchall_1
    move-exception v0

    iget-object v1, p0, Lcom/google/android/gm/provider/AppDataSearch;->mClient:Lcom/google/android/gms/appdatasearch/AppDataSearchClient;

    invoke-virtual {v1}, Lcom/google/android/gms/appdatasearch/AppDataSearchClient;->disconnect()V

    throw v0

    :cond_1
    const-string v0, "Gmail"

    const-string v1, "Couldn\'t connect to appdatasearch for search: %d"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    invoke-virtual {v8}, Lcom/google/android/gms/common/ConnectionResult;->getErrorCode()I

    move-result v4

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-static {v0, v1, v2}, Lcom/google/android/gm/provider/LogUtils;->d(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)I
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_0

    goto :goto_0
.end method

.method public scheduleIndexing(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;)V
    .locals 17
    .param p1    # Landroid/database/sqlite/SQLiteDatabase;
    .param p2    # Ljava/lang/String;

    const-string v10, "Gmail"

    const-string v11, "Schedule indexing for %s"

    const/4 v12, 0x1

    new-array v12, v12, [Ljava/lang/Object;

    const/4 v13, 0x0

    aput-object p2, v12, v13

    invoke-static {v10, v11, v12}, Lcom/google/android/gm/provider/LogUtils;->i(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)I

    const-wide/16 v6, 0x0

    const-wide/16 v4, 0x0

    const-string v10, "SELECT min(_id), max(_id) FROM search_sequence"

    const/4 v11, 0x0

    move-object/from16 v0, p1

    invoke-virtual {v0, v10, v11}, Landroid/database/sqlite/SQLiteDatabase;->rawQuery(Ljava/lang/String;[Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v1

    :try_start_0
    invoke-interface {v1}, Landroid/database/Cursor;->moveToNext()Z

    move-result v10

    if-eqz v10, :cond_0

    const/4 v10, 0x0

    invoke-interface {v1, v10}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v6

    const/4 v10, 0x1

    invoke-interface {v1, v10}, Landroid/database/Cursor;->getLong(I)J
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-wide v4

    :cond_0
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/google/android/gm/provider/AppDataSearch;->mClient:Lcom/google/android/gms/appdatasearch/AppDataSearchClient;

    monitor-enter v11

    :try_start_1
    move-object/from16 v0, p0

    iget-object v10, v0, Lcom/google/android/gm/provider/AppDataSearch;->mClient:Lcom/google/android/gms/appdatasearch/AppDataSearchClient;

    const-wide/16 v12, 0x7530

    invoke-virtual {v10, v12, v13}, Lcom/google/android/gms/appdatasearch/AppDataSearchClient;->connectWithTimeout(J)Lcom/google/android/gms/common/ConnectionResult;

    move-result-object v8

    invoke-virtual {v8}, Lcom/google/android/gms/common/ConnectionResult;->isSuccess()Z

    move-result v10

    if-nez v10, :cond_1

    const-string v10, "Gmail"

    const-string v12, "Connection to search failed: %d"

    const/4 v13, 0x1

    new-array v13, v13, [Ljava/lang/Object;

    const/4 v14, 0x0

    invoke-virtual {v8}, Lcom/google/android/gms/common/ConnectionResult;->getErrorCode()I

    move-result v15

    invoke-static {v15}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v15

    aput-object v15, v13, v14

    invoke-static {v10, v12, v13}, Lcom/google/android/gm/provider/LogUtils;->e(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)I

    monitor-exit v11
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    :goto_0
    return-void

    :catchall_0
    move-exception v10

    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    throw v10

    :cond_1
    :try_start_2
    const-string v10, "Gmail"

    const-string v12, "Getting status for %s"

    const/4 v13, 0x1

    new-array v13, v13, [Ljava/lang/Object;

    const/4 v14, 0x0

    aput-object p2, v13, v14

    invoke-static {v10, v12, v13}, Lcom/google/android/gm/provider/LogUtils;->d(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)I

    move-object/from16 v0, p0

    iget-object v10, v0, Lcom/google/android/gm/provider/AppDataSearch;->mClient:Lcom/google/android/gms/appdatasearch/AppDataSearchClient;

    move-object/from16 v0, p2

    invoke-virtual {v10, v0}, Lcom/google/android/gms/appdatasearch/AppDataSearchClient;->getCorpusStatus(Ljava/lang/String;)Lcom/google/android/gms/appdatasearch/CorpusStatus;

    move-result-object v9

    iget-boolean v10, v9, Lcom/google/android/gms/appdatasearch/CorpusStatus;->found:Z

    if-nez v10, :cond_2

    const-string v10, "Gmail"

    new-instance v12, Ljava/lang/StringBuilder;

    invoke-direct {v12}, Ljava/lang/StringBuilder;-><init>()V

    const-string v13, "Account "

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    move-object/from16 v0, p2

    invoke-virtual {v12, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    const-string v13, " is not registered for search"

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v12

    const/4 v13, 0x0

    new-array v13, v13, [Ljava/lang/Object;

    invoke-static {v10, v12, v13}, Lcom/google/android/gm/provider/LogUtils;->e(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)I
    :try_end_2
    .catch Ljava/lang/NullPointerException; {:try_start_2 .. :try_end_2} :catch_0
    .catch Ljava/lang/IllegalStateException; {:try_start_2 .. :try_end_2} :catch_1
    .catchall {:try_start_2 .. :try_end_2} :catchall_2

    :try_start_3
    move-object/from16 v0, p0

    iget-object v10, v0, Lcom/google/android/gm/provider/AppDataSearch;->mClient:Lcom/google/android/gms/appdatasearch/AppDataSearchClient;

    invoke-virtual {v10}, Lcom/google/android/gms/appdatasearch/AppDataSearchClient;->disconnect()V

    monitor-exit v11

    goto :goto_0

    :catchall_1
    move-exception v10

    monitor-exit v11
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v10

    :cond_2
    :try_start_4
    iget-wide v12, v9, Lcom/google/android/gms/appdatasearch/CorpusStatus;->lastCommittedSeqno:J

    cmp-long v10, v6, v12

    if-gtz v10, :cond_3

    const-string v10, "search_sequence"

    const-string v12, "_id <= ?"

    const/4 v13, 0x1

    new-array v13, v13, [Ljava/lang/String;

    const/4 v14, 0x0

    iget-wide v15, v9, Lcom/google/android/gms/appdatasearch/CorpusStatus;->lastCommittedSeqno:J

    invoke-static/range {v15 .. v16}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v15

    aput-object v15, v13, v14

    move-object/from16 v0, p1

    invoke-virtual {v0, v10, v12, v13}, Landroid/database/sqlite/SQLiteDatabase;->delete(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I

    :cond_3
    iget-wide v12, v9, Lcom/google/android/gms/appdatasearch/CorpusStatus;->lastIndexedSeqno:J

    cmp-long v10, v4, v12

    if-lez v10, :cond_4

    move-object/from16 v0, p0

    iget-object v10, v0, Lcom/google/android/gm/provider/AppDataSearch;->mClient:Lcom/google/android/gms/appdatasearch/AppDataSearchClient;

    move-object/from16 v0, p2

    invoke-virtual {v10, v0, v4, v5}, Lcom/google/android/gms/appdatasearch/AppDataSearchClient;->requestIndexing(Ljava/lang/String;J)Z
    :try_end_4
    .catch Ljava/lang/NullPointerException; {:try_start_4 .. :try_end_4} :catch_0
    .catch Ljava/lang/IllegalStateException; {:try_start_4 .. :try_end_4} :catch_1
    .catchall {:try_start_4 .. :try_end_4} :catchall_2

    :cond_4
    :try_start_5
    move-object/from16 v0, p0

    iget-object v10, v0, Lcom/google/android/gm/provider/AppDataSearch;->mClient:Lcom/google/android/gms/appdatasearch/AppDataSearchClient;

    invoke-virtual {v10}, Lcom/google/android/gms/appdatasearch/AppDataSearchClient;->disconnect()V

    :goto_1
    monitor-exit v11
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_1

    goto/16 :goto_0

    :catch_0
    move-exception v2

    :try_start_6
    const-string v10, "Gmail"

    const-string v12, "Error scheduling search indexing"

    const/4 v13, 0x0

    new-array v13, v13, [Ljava/lang/Object;

    invoke-static {v10, v2, v12, v13}, Lcom/google/android/gm/provider/LogUtils;->e(Ljava/lang/String;Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)I
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_2

    :try_start_7
    move-object/from16 v0, p0

    iget-object v10, v0, Lcom/google/android/gm/provider/AppDataSearch;->mClient:Lcom/google/android/gms/appdatasearch/AppDataSearchClient;

    invoke-virtual {v10}, Lcom/google/android/gms/appdatasearch/AppDataSearchClient;->disconnect()V
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_1

    goto :goto_1

    :catch_1
    move-exception v3

    :try_start_8
    const-string v10, "Gmail"

    const-string v12, "Error scheduling search indexing"

    const/4 v13, 0x0

    new-array v13, v13, [Ljava/lang/Object;

    invoke-static {v10, v3, v12, v13}, Lcom/google/android/gm/provider/LogUtils;->e(Ljava/lang/String;Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)I
    :try_end_8
    .catchall {:try_start_8 .. :try_end_8} :catchall_2

    :try_start_9
    move-object/from16 v0, p0

    iget-object v10, v0, Lcom/google/android/gm/provider/AppDataSearch;->mClient:Lcom/google/android/gms/appdatasearch/AppDataSearchClient;

    invoke-virtual {v10}, Lcom/google/android/gms/appdatasearch/AppDataSearchClient;->disconnect()V

    goto :goto_1

    :catchall_2
    move-exception v10

    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/google/android/gm/provider/AppDataSearch;->mClient:Lcom/google/android/gms/appdatasearch/AppDataSearchClient;

    invoke-virtual {v12}, Lcom/google/android/gms/appdatasearch/AppDataSearchClient;->disconnect()V

    throw v10
    :try_end_9
    .catchall {:try_start_9 .. :try_end_9} :catchall_1
.end method

.method public suggest(Ljava/lang/String;Ljava/lang/String;I)Lcom/google/android/gms/appdatasearch/SuggestionResults;
    .locals 11
    .param p1    # Ljava/lang/String;
    .param p2    # Ljava/lang/String;
    .param p3    # I

    const/4 v4, 0x0

    iget-object v6, p0, Lcom/google/android/gm/provider/AppDataSearch;->mClient:Lcom/google/android/gms/appdatasearch/AppDataSearchClient;

    monitor-enter v6

    :try_start_0
    iget-object v5, p0, Lcom/google/android/gm/provider/AppDataSearch;->mClient:Lcom/google/android/gms/appdatasearch/AppDataSearchClient;

    const-wide/16 v7, 0x2710

    invoke-virtual {v5, v7, v8}, Lcom/google/android/gms/appdatasearch/AppDataSearchClient;->connectWithTimeout(J)Lcom/google/android/gms/common/ConnectionResult;

    move-result-object v3

    invoke-virtual {v3}, Lcom/google/android/gms/common/ConnectionResult;->isSuccess()Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v5

    if-eqz v5, :cond_2

    if-nez p2, :cond_1

    const/4 v0, 0x0

    :goto_0
    :try_start_1
    iget-object v5, p0, Lcom/google/android/gm/provider/AppDataSearch;->mClient:Lcom/google/android/gms/appdatasearch/AppDataSearchClient;

    invoke-virtual {v5, p1, v0, p3}, Lcom/google/android/gms/appdatasearch/AppDataSearchClient;->suggest(Ljava/lang/String;[Ljava/lang/String;I)Lcom/google/android/gms/appdatasearch/SuggestionResults;

    move-result-object v4

    invoke-virtual {v4}, Lcom/google/android/gms/appdatasearch/SuggestionResults;->hasError()Z

    move-result v5

    if-eqz v5, :cond_0

    const-string v5, "Gmail"

    const-string v7, "Error from suggestions: %s"

    const/4 v8, 0x1

    new-array v8, v8, [Ljava/lang/Object;

    const/4 v9, 0x0

    invoke-virtual {v4}, Lcom/google/android/gms/appdatasearch/SuggestionResults;->getErrorMessage()Ljava/lang/String;

    move-result-object v10

    aput-object v10, v8, v9

    invoke-static {v5, v7, v8}, Lcom/google/android/gm/provider/LogUtils;->e(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)I
    :try_end_1
    .catch Ljava/lang/NullPointerException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Ljava/lang/IllegalStateException; {:try_start_1 .. :try_end_1} :catch_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    const/4 v4, 0x0

    :cond_0
    :try_start_2
    iget-object v5, p0, Lcom/google/android/gm/provider/AppDataSearch;->mClient:Lcom/google/android/gms/appdatasearch/AppDataSearchClient;

    invoke-virtual {v5}, Lcom/google/android/gms/appdatasearch/AppDataSearchClient;->disconnect()V

    :goto_1
    monitor-exit v6
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    return-object v4

    :cond_1
    const/4 v5, 0x1

    :try_start_3
    new-array v0, v5, [Ljava/lang/String;

    const/4 v5, 0x0

    aput-object p2, v0, v5
    :try_end_3
    .catch Ljava/lang/NullPointerException; {:try_start_3 .. :try_end_3} :catch_0
    .catch Ljava/lang/IllegalStateException; {:try_start_3 .. :try_end_3} :catch_1
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    goto :goto_0

    :catch_0
    move-exception v1

    :try_start_4
    const-string v5, "Gmail"

    const-string v7, "Error executing suggestion query"

    const/4 v8, 0x0

    new-array v8, v8, [Ljava/lang/Object;

    invoke-static {v5, v1, v7, v8}, Lcom/google/android/gm/provider/LogUtils;->e(Ljava/lang/String;Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)I
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    :try_start_5
    iget-object v5, p0, Lcom/google/android/gm/provider/AppDataSearch;->mClient:Lcom/google/android/gms/appdatasearch/AppDataSearchClient;

    invoke-virtual {v5}, Lcom/google/android/gms/appdatasearch/AppDataSearchClient;->disconnect()V

    goto :goto_1

    :catchall_0
    move-exception v5

    monitor-exit v6
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    throw v5

    :catch_1
    move-exception v2

    :try_start_6
    const-string v5, "Gmail"

    const-string v7, "Error executing suggestion query"

    const/4 v8, 0x0

    new-array v8, v8, [Ljava/lang/Object;

    invoke-static {v5, v2, v7, v8}, Lcom/google/android/gm/provider/LogUtils;->e(Ljava/lang/String;Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)I
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_1

    :try_start_7
    iget-object v5, p0, Lcom/google/android/gm/provider/AppDataSearch;->mClient:Lcom/google/android/gms/appdatasearch/AppDataSearchClient;

    invoke-virtual {v5}, Lcom/google/android/gms/appdatasearch/AppDataSearchClient;->disconnect()V

    goto :goto_1

    :catchall_1
    move-exception v5

    iget-object v7, p0, Lcom/google/android/gm/provider/AppDataSearch;->mClient:Lcom/google/android/gms/appdatasearch/AppDataSearchClient;

    invoke-virtual {v7}, Lcom/google/android/gms/appdatasearch/AppDataSearchClient;->disconnect()V

    throw v5

    :cond_2
    const-string v5, "Gmail"

    const-string v7, "Couldn\'t connect to appdatasearch for suggestions: %d"

    const/4 v8, 0x1

    new-array v8, v8, [Ljava/lang/Object;

    const/4 v9, 0x0

    invoke-virtual {v3}, Lcom/google/android/gms/common/ConnectionResult;->getErrorCode()I

    move-result v10

    invoke-static {v10}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v10

    aput-object v10, v8, v9

    invoke-static {v5, v7, v8}, Lcom/google/android/gm/provider/LogUtils;->d(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)I
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_0

    goto :goto_1
.end method
