.class public Lcom/google/android/gm/provider/uiprovider/UICursorWapper;
.super Landroid/database/CursorWrapper;
.source "UICursorWapper.java"


# instance fields
.field private final mColumnNameMap:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field protected final mResultProjection:[Ljava/lang/String;


# direct methods
.method public constructor <init>(Landroid/database/Cursor;[Ljava/lang/String;)V
    .locals 5
    .param p1    # Landroid/database/Cursor;
    .param p2    # [Ljava/lang/String;

    invoke-direct {p0, p1}, Landroid/database/CursorWrapper;-><init>(Landroid/database/Cursor;)V

    iput-object p2, p0, Lcom/google/android/gm/provider/uiprovider/UICursorWapper;->mResultProjection:[Ljava/lang/String;

    iget-object v2, p0, Lcom/google/android/gm/provider/uiprovider/UICursorWapper;->mResultProjection:[Ljava/lang/String;

    array-length v0, v2

    new-instance v2, Ljava/util/HashMap;

    const/high16 v3, 0x3f800000

    invoke-direct {v2, v0, v3}, Ljava/util/HashMap;-><init>(IF)V

    iput-object v2, p0, Lcom/google/android/gm/provider/uiprovider/UICursorWapper;->mColumnNameMap:Ljava/util/Map;

    const/4 v1, 0x0

    :goto_0
    if-ge v1, v0, :cond_0

    iget-object v2, p0, Lcom/google/android/gm/provider/uiprovider/UICursorWapper;->mColumnNameMap:Ljava/util/Map;

    iget-object v3, p0, Lcom/google/android/gm/provider/uiprovider/UICursorWapper;->mResultProjection:[Ljava/lang/String;

    aget-object v3, v3, v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-interface {v2, v3, v4}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_0
    return-void
.end method

.method private static toNonnullString(Ljava/lang/String;)Ljava/lang/String;
    .locals 0
    .param p0    # Ljava/lang/String;

    if-nez p0, :cond_0

    const-string p0, ""

    :cond_0
    return-object p0
.end method


# virtual methods
.method public getColumnIndex(Ljava/lang/String;)I
    .locals 2
    .param p1    # Ljava/lang/String;

    iget-object v1, p0, Lcom/google/android/gm/provider/uiprovider/UICursorWapper;->mColumnNameMap:Ljava/util/Map;

    invoke-interface {v1, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v1

    :goto_0
    return v1

    :cond_0
    const/4 v1, -0x1

    goto :goto_0
.end method

.method public getColumnIndexOrThrow(Ljava/lang/String;)I
    .locals 4
    .param p1    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/IllegalArgumentException;
        }
    .end annotation

    invoke-virtual {p0, p1}, Lcom/google/android/gm/provider/uiprovider/UICursorWapper;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    const/4 v1, -0x1

    if-ne v0, v1, :cond_0

    new-instance v1, Ljava/lang/IllegalArgumentException;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Column not found: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v1

    :cond_0
    return v0
.end method

.method public getColumnName(I)Ljava/lang/String;
    .locals 1
    .param p1    # I

    iget-object v0, p0, Lcom/google/android/gm/provider/uiprovider/UICursorWapper;->mResultProjection:[Ljava/lang/String;

    aget-object v0, v0, p1

    return-object v0
.end method

.method public getColumnNames()[Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/android/gm/provider/uiprovider/UICursorWapper;->mResultProjection:[Ljava/lang/String;

    return-object v0
.end method

.method protected getStringInColumn(I)Ljava/lang/String;
    .locals 1
    .param p1    # I

    invoke-super {p0, p1}, Landroid/database/CursorWrapper;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/gm/provider/uiprovider/UICursorWapper;->toNonnullString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public move(I)Z
    .locals 1
    .param p1    # I

    invoke-virtual {p0}, Lcom/google/android/gm/provider/uiprovider/UICursorWapper;->resetCursorRowState()V

    invoke-super {p0, p1}, Landroid/database/CursorWrapper;->move(I)Z

    move-result v0

    return v0
.end method

.method public moveToFirst()Z
    .locals 1

    invoke-virtual {p0}, Lcom/google/android/gm/provider/uiprovider/UICursorWapper;->resetCursorRowState()V

    invoke-super {p0}, Landroid/database/CursorWrapper;->moveToFirst()Z

    move-result v0

    return v0
.end method

.method public moveToNext()Z
    .locals 1

    invoke-virtual {p0}, Lcom/google/android/gm/provider/uiprovider/UICursorWapper;->resetCursorRowState()V

    invoke-super {p0}, Landroid/database/CursorWrapper;->moveToNext()Z

    move-result v0

    return v0
.end method

.method public moveToPosition(I)Z
    .locals 1
    .param p1    # I

    invoke-virtual {p0}, Lcom/google/android/gm/provider/uiprovider/UICursorWapper;->resetCursorRowState()V

    invoke-super {p0, p1}, Landroid/database/CursorWrapper;->moveToPosition(I)Z

    move-result v0

    return v0
.end method

.method public moveToPrevious()Z
    .locals 1

    invoke-virtual {p0}, Lcom/google/android/gm/provider/uiprovider/UICursorWapper;->resetCursorRowState()V

    invoke-super {p0}, Landroid/database/CursorWrapper;->moveToPrevious()Z

    move-result v0

    return v0
.end method

.method protected resetCursorRowState()V
    .locals 0

    return-void
.end method
