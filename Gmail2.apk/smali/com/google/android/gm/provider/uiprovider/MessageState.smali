.class public Lcom/google/android/gm/provider/uiprovider/MessageState;
.super Ljava/lang/Object;
.source "MessageState.java"


# instance fields
.field private final mAccount:Ljava/lang/String;

.field private final mContext:Landroid/content/Context;

.field private final mConversationId:J

.field private final mLocalMessageId:J

.field private final mMessageAttachments:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/android/gm/provider/uiprovider/GmailAttachment;",
            ">;"
        }
    .end annotation
.end field

.field mMessageAttachmentsInitialized:Z

.field private final mMessageId:J


# direct methods
.method public constructor <init>(Landroid/content/Context;Ljava/lang/String;JJJ)V
    .locals 1
    .param p1    # Landroid/content/Context;
    .param p2    # Ljava/lang/String;
    .param p3    # J
    .param p5    # J
    .param p7    # J

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    invoke-static {}, Lcom/google/common/collect/Lists;->newArrayList()Ljava/util/ArrayList;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gm/provider/uiprovider/MessageState;->mMessageAttachments:Ljava/util/List;

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/gm/provider/uiprovider/MessageState;->mMessageAttachmentsInitialized:Z

    iput-object p1, p0, Lcom/google/android/gm/provider/uiprovider/MessageState;->mContext:Landroid/content/Context;

    iput-object p2, p0, Lcom/google/android/gm/provider/uiprovider/MessageState;->mAccount:Ljava/lang/String;

    iput-wide p3, p0, Lcom/google/android/gm/provider/uiprovider/MessageState;->mConversationId:J

    iput-wide p5, p0, Lcom/google/android/gm/provider/uiprovider/MessageState;->mMessageId:J

    iput-wide p7, p0, Lcom/google/android/gm/provider/uiprovider/MessageState;->mLocalMessageId:J

    return-void
.end method


# virtual methods
.method public getMessageAttachment(Ljava/lang/String;)Lcom/google/android/gm/provider/uiprovider/GmailAttachment;
    .locals 4
    .param p1    # Ljava/lang/String;

    if-eqz p1, :cond_2

    iget-object v3, p0, Lcom/google/android/gm/provider/uiprovider/MessageState;->mMessageAttachments:Ljava/util/List;

    monitor-enter v3

    :try_start_0
    iget-object v2, p0, Lcom/google/android/gm/provider/uiprovider/MessageState;->mMessageAttachments:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gm/provider/uiprovider/GmailAttachment;

    iget-object v2, v0, Lcom/google/android/gm/provider/uiprovider/GmailAttachment;->partId:Ljava/lang/String;

    invoke-virtual {p1, v2}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_0

    monitor-exit v3

    :goto_0
    return-object v0

    :cond_1
    monitor-exit v3

    :cond_2
    const/4 v0, 0x0

    goto :goto_0

    :catchall_0
    move-exception v2

    monitor-exit v3
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v2
.end method

.method public getMessageAttachments()Ljava/util/List;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/gm/provider/uiprovider/GmailAttachment;",
            ">;"
        }
    .end annotation

    iget-object v1, p0, Lcom/google/android/gm/provider/uiprovider/MessageState;->mMessageAttachments:Ljava/util/List;

    monitor-enter v1

    :try_start_0
    iget-object v0, p0, Lcom/google/android/gm/provider/uiprovider/MessageState;->mMessageAttachments:Ljava/util/List;

    invoke-static {v0}, Lcom/google/common/collect/ImmutableList;->copyOf(Ljava/util/Collection;)Lcom/google/common/collect/ImmutableList;

    move-result-object v0

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method gmailAttachmentDataLoaded()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/gm/provider/uiprovider/MessageState;->mMessageAttachmentsInitialized:Z

    return v0
.end method

.method initializeAttachmentsFromMessage(Lcom/google/android/gm/provider/MailSync$Message;)V
    .locals 1
    .param p1    # Lcom/google/android/gm/provider/MailSync$Message;

    iget-object v0, p1, Lcom/google/android/gm/provider/MailSync$Message;->rawAttachments:Ljava/util/List;

    invoke-virtual {p0, v0}, Lcom/google/android/gm/provider/uiprovider/MessageState;->initializeAttachmentsFromMessageAttachments(Ljava/util/List;)V

    return-void
.end method

.method public initializeAttachmentsFromMessageAttachments(Ljava/util/List;)V
    .locals 14
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/gm/provider/uiprovider/GmailAttachment;",
            ">;)V"
        }
    .end annotation

    const/4 v12, 0x0

    const/4 v11, 0x1

    iget-object v13, p0, Lcom/google/android/gm/provider/uiprovider/MessageState;->mMessageAttachments:Ljava/util/List;

    monitor-enter v13

    :try_start_0
    iget-object v0, p0, Lcom/google/android/gm/provider/uiprovider/MessageState;->mMessageAttachments:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v10

    :goto_0
    invoke-interface {v10}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_5

    invoke-interface {v10}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Lcom/google/android/gm/provider/uiprovider/GmailAttachment;

    iget-object v0, v9, Lcom/google/android/gm/provider/uiprovider/GmailAttachment;->uri:Landroid/net/Uri;

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/gm/provider/uiprovider/MessageState;->mAccount:Ljava/lang/String;

    iget-wide v1, p0, Lcom/google/android/gm/provider/uiprovider/MessageState;->mConversationId:J

    iget-wide v3, p0, Lcom/google/android/gm/provider/uiprovider/MessageState;->mMessageId:J

    iget-wide v5, p0, Lcom/google/android/gm/provider/uiprovider/MessageState;->mLocalMessageId:J

    iget-object v7, v9, Lcom/google/android/gm/provider/uiprovider/GmailAttachment;->partId:Ljava/lang/String;

    invoke-virtual {v9}, Lcom/google/android/gm/provider/uiprovider/GmailAttachment;->getContentType()Ljava/lang/String;

    move-result-object v8

    invoke-static/range {v0 .. v8}, Lcom/google/android/gm/provider/UiProvider;->getMessageAttachmentUri(Ljava/lang/String;JJJLjava/lang/String;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    iput-object v0, v9, Lcom/google/android/gm/provider/uiprovider/GmailAttachment;->uri:Landroid/net/Uri;

    :cond_0
    iget-object v0, v9, Lcom/google/android/gm/provider/uiprovider/GmailAttachment;->contentUri:Landroid/net/Uri;

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/google/android/gm/provider/uiprovider/MessageState;->mAccount:Ljava/lang/String;

    iget-wide v1, p0, Lcom/google/android/gm/provider/uiprovider/MessageState;->mLocalMessageId:J

    iget-object v3, v9, Lcom/google/android/gm/provider/uiprovider/GmailAttachment;->partId:Ljava/lang/String;

    const/4 v4, 0x1

    iget v5, v9, Lcom/google/android/gm/provider/uiprovider/GmailAttachment;->destination:I

    if-ne v5, v11, :cond_3

    move v5, v11

    :goto_1
    invoke-static/range {v0 .. v5}, Lcom/google/android/gm/provider/Gmail;->getAttachmentDefaultUri(Ljava/lang/String;JLjava/lang/String;IZ)Landroid/net/Uri;

    move-result-object v0

    iput-object v0, v9, Lcom/google/android/gm/provider/uiprovider/GmailAttachment;->contentUri:Landroid/net/Uri;

    :cond_1
    iget-object v0, v9, Lcom/google/android/gm/provider/uiprovider/GmailAttachment;->thumbnailUri:Landroid/net/Uri;

    if-nez v0, :cond_2

    iget-object v0, p0, Lcom/google/android/gm/provider/uiprovider/MessageState;->mAccount:Ljava/lang/String;

    iget-wide v1, p0, Lcom/google/android/gm/provider/uiprovider/MessageState;->mLocalMessageId:J

    iget-object v3, v9, Lcom/google/android/gm/provider/uiprovider/GmailAttachment;->partId:Ljava/lang/String;

    const/4 v4, 0x0

    iget v5, v9, Lcom/google/android/gm/provider/uiprovider/GmailAttachment;->destination:I

    if-ne v5, v11, :cond_4

    move v5, v11

    :goto_2
    invoke-static/range {v0 .. v5}, Lcom/google/android/gm/provider/Gmail;->getAttachmentDefaultUri(Ljava/lang/String;JLjava/lang/String;IZ)Landroid/net/Uri;

    move-result-object v0

    iput-object v0, v9, Lcom/google/android/gm/provider/uiprovider/GmailAttachment;->thumbnailUri:Landroid/net/Uri;

    :cond_2
    iget-object v0, p0, Lcom/google/android/gm/provider/uiprovider/MessageState;->mMessageAttachments:Ljava/util/List;

    invoke-interface {v0, v9}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit v13
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0

    :cond_3
    move v5, v12

    goto :goto_1

    :cond_4
    move v5, v12

    goto :goto_2

    :cond_5
    :try_start_1
    monitor-exit v13
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    iput-boolean v11, p0, Lcom/google/android/gm/provider/uiprovider/MessageState;->mMessageAttachmentsInitialized:Z

    return-void
.end method

.method public notifyAttachmentChange()V
    .locals 11

    invoke-static {}, Lcom/google/common/collect/Sets;->newHashSet()Ljava/util/HashSet;

    move-result-object v8

    invoke-virtual {p0}, Lcom/google/android/gm/provider/uiprovider/MessageState;->getMessageAttachments()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v10

    :goto_0
    invoke-interface {v10}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v10}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Lcom/google/android/gm/provider/uiprovider/GmailAttachment;

    iget-object v0, v9, Lcom/google/android/gm/provider/uiprovider/GmailAttachment;->partId:Ljava/lang/String;

    invoke-interface {v8, v0}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_0
    const-string v0, "Gmail"

    const-string v1, "Notifying about attachment change conversation message %d/%d"

    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    iget-wide v4, p0, Lcom/google/android/gm/provider/uiprovider/MessageState;->mConversationId:J

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    aput-object v4, v2, v3

    const/4 v3, 0x1

    iget-wide v4, p0, Lcom/google/android/gm/provider/uiprovider/MessageState;->mMessageId:J

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-static {v0, v1, v2}, Lcom/google/android/gm/provider/LogUtils;->v(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)I

    iget-object v0, p0, Lcom/google/android/gm/provider/uiprovider/MessageState;->mContext:Landroid/content/Context;

    iget-object v1, p0, Lcom/google/android/gm/provider/uiprovider/MessageState;->mAccount:Ljava/lang/String;

    iget-wide v2, p0, Lcom/google/android/gm/provider/uiprovider/MessageState;->mConversationId:J

    iget-wide v4, p0, Lcom/google/android/gm/provider/uiprovider/MessageState;->mMessageId:J

    iget-wide v6, p0, Lcom/google/android/gm/provider/uiprovider/MessageState;->mLocalMessageId:J

    invoke-static/range {v0 .. v8}, Lcom/google/android/gm/provider/UiProvider;->notifyMessageAttachmentsChanged(Landroid/content/Context;Ljava/lang/String;JJJLjava/util/Set;)V

    return-void
.end method

.method populateGmailAttachmentData(Lcom/google/android/gm/provider/MailSync$Message;)V
    .locals 1
    .param p1    # Lcom/google/android/gm/provider/MailSync$Message;

    iget-boolean v0, p0, Lcom/google/android/gm/provider/uiprovider/MessageState;->mMessageAttachmentsInitialized:Z

    if-nez v0, :cond_0

    invoke-virtual {p0, p1}, Lcom/google/android/gm/provider/uiprovider/MessageState;->initializeAttachmentsFromMessage(Lcom/google/android/gm/provider/MailSync$Message;)V

    :cond_0
    return-void
.end method
