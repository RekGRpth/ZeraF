.class Lcom/google/android/gm/provider/GmailTransactionListener;
.super Ljava/lang/Object;
.source "GmailTransactionListener.java"

# interfaces
.implements Lcom/google/android/gm/provider/TransactionHelper$BetterTransactionListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/gm/provider/GmailTransactionListener$GmailTransactionState;
    }
.end annotation


# instance fields
.field private final DEBUG:Z

.field private final mAccount:Ljava/lang/String;

.field private final mContext:Landroid/content/Context;

.field private final mDb:Landroid/database/sqlite/SQLiteDatabase;

.field private final mEngine:Lcom/google/android/gm/provider/MailEngine;

.field private final mState:Ljava/lang/ThreadLocal;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/ThreadLocal",
            "<",
            "Lcom/google/android/gm/provider/GmailTransactionListener$GmailTransactionState;",
            ">;"
        }
    .end annotation
.end field

.field private mTestTransactionListener:Landroid/database/sqlite/SQLiteTransactionListener;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/google/android/gm/provider/MailEngine;Ljava/lang/String;)V
    .locals 1
    .param p1    # Landroid/content/Context;
    .param p2    # Lcom/google/android/gm/provider/MailEngine;
    .param p3    # Ljava/lang/String;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/gm/provider/GmailTransactionListener;->DEBUG:Z

    iput-object p1, p0, Lcom/google/android/gm/provider/GmailTransactionListener;->mContext:Landroid/content/Context;

    iput-object p3, p0, Lcom/google/android/gm/provider/GmailTransactionListener;->mAccount:Ljava/lang/String;

    iput-object p2, p0, Lcom/google/android/gm/provider/GmailTransactionListener;->mEngine:Lcom/google/android/gm/provider/MailEngine;

    iget-object v0, p0, Lcom/google/android/gm/provider/GmailTransactionListener;->mEngine:Lcom/google/android/gm/provider/MailEngine;

    iget-object v0, v0, Lcom/google/android/gm/provider/MailEngine;->mDb:Landroid/database/sqlite/SQLiteDatabase;

    iput-object v0, p0, Lcom/google/android/gm/provider/GmailTransactionListener;->mDb:Landroid/database/sqlite/SQLiteDatabase;

    new-instance v0, Lcom/google/android/gm/provider/GmailTransactionListener$1;

    invoke-direct {v0, p0}, Lcom/google/android/gm/provider/GmailTransactionListener$1;-><init>(Lcom/google/android/gm/provider/GmailTransactionListener;)V

    iput-object v0, p0, Lcom/google/android/gm/provider/GmailTransactionListener;->mState:Ljava/lang/ThreadLocal;

    return-void
.end method

.method private getConversationIdsSet()Ljava/util/Set;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Set",
            "<",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/google/android/gm/provider/GmailTransactionListener;->mState:Ljava/lang/ThreadLocal;

    invoke-virtual {v0}, Ljava/lang/ThreadLocal;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gm/provider/GmailTransactionListener$GmailTransactionState;

    iget-object v0, v0, Lcom/google/android/gm/provider/GmailTransactionListener$GmailTransactionState;->mConversationIds:Ljava/util/Set;

    return-object v0
.end method

.method private getLabelIdsSet()Ljava/util/Set;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Set",
            "<",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/google/android/gm/provider/GmailTransactionListener;->mState:Ljava/lang/ThreadLocal;

    invoke-virtual {v0}, Ljava/lang/ThreadLocal;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gm/provider/GmailTransactionListener$GmailTransactionState;

    iget-object v0, v0, Lcom/google/android/gm/provider/GmailTransactionListener$GmailTransactionState;->mLabelIds:Ljava/util/Set;

    return-object v0
.end method


# virtual methods
.method public addConversationToNotify(J)V
    .locals 6
    .param p1    # J

    const/4 v3, 0x1

    const/4 v5, 0x0

    iget-object v1, p0, Lcom/google/android/gm/provider/GmailTransactionListener;->mDb:Landroid/database/sqlite/SQLiteDatabase;

    invoke-virtual {v1}, Landroid/database/sqlite/SQLiteDatabase;->inTransaction()Z

    move-result v1

    if-nez v1, :cond_1

    const-string v1, "Gmail"

    const-string v2, "Must already be in a transaction with listener to add conversation to notify. (id=%d)"

    new-array v3, v3, [Ljava/lang/Object;

    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    aput-object v4, v3, v5

    invoke-static {v1, v2, v3}, Lcom/google/android/gm/provider/LogUtils;->e(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)I

    :cond_0
    :goto_0
    return-void

    :cond_1
    invoke-direct {p0}, Lcom/google/android/gm/provider/GmailTransactionListener;->getConversationIdsSet()Ljava/util/Set;

    move-result-object v1

    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v1, "Gmail"

    const-string v2, "adding convId (%d) to notify"

    new-array v3, v3, [Ljava/lang/Object;

    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    aput-object v4, v3, v5

    invoke-static {v1, v2, v3}, Lcom/google/android/gm/provider/LogUtils;->d(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)I

    goto :goto_0
.end method

.method public addLabelToNotify(Ljava/util/Set;)V
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Set",
            "<",
            "Ljava/lang/Long;",
            ">;)V"
        }
    .end annotation

    const/4 v3, 0x1

    const/4 v4, 0x0

    iget-object v1, p0, Lcom/google/android/gm/provider/GmailTransactionListener;->mDb:Landroid/database/sqlite/SQLiteDatabase;

    invoke-virtual {v1}, Landroid/database/sqlite/SQLiteDatabase;->inTransaction()Z

    move-result v1

    if-nez v1, :cond_1

    const-string v1, "Gmail"

    const-string v2, "Must already be in a transaction with listener to add label to notify. (ids=%s)"

    new-array v3, v3, [Ljava/lang/Object;

    aput-object p1, v3, v4

    invoke-static {v1, v2, v3}, Lcom/google/android/gm/provider/LogUtils;->e(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)I

    :cond_0
    :goto_0
    return-void

    :cond_1
    invoke-direct {p0}, Lcom/google/android/gm/provider/GmailTransactionListener;->getLabelIdsSet()Ljava/util/Set;

    move-result-object v1

    invoke-interface {v1, p1}, Ljava/util/Set;->addAll(Ljava/util/Collection;)Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v1, "Gmail"

    const-string v2, "adding labelIds (%s) to notify"

    new-array v3, v3, [Ljava/lang/Object;

    aput-object p1, v3, v4

    invoke-static {v1, v2, v3}, Lcom/google/android/gm/provider/LogUtils;->d(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)I

    goto :goto_0
.end method

.method public enableGmailAccountNotifications(Z)V
    .locals 6
    .param p1    # Z

    const/4 v3, 0x1

    iget-object v1, p0, Lcom/google/android/gm/provider/GmailTransactionListener;->mDb:Landroid/database/sqlite/SQLiteDatabase;

    invoke-virtual {v1}, Landroid/database/sqlite/SQLiteDatabase;->inTransaction()Z

    move-result v1

    if-nez v1, :cond_1

    const-string v1, "Gmail"

    const-string v2, "Must already be in a transaction with listener to enable notifications for account %s."

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    iget-object v5, p0, Lcom/google/android/gm/provider/GmailTransactionListener;->mAccount:Ljava/lang/String;

    aput-object v5, v3, v4

    invoke-static {v1, v2, v3}, Lcom/google/android/gm/provider/LogUtils;->e(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)I

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v1, p0, Lcom/google/android/gm/provider/GmailTransactionListener;->mState:Ljava/lang/ThreadLocal;

    invoke-virtual {v1}, Ljava/lang/ThreadLocal;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gm/provider/GmailTransactionListener$GmailTransactionState;

    iput-boolean v3, v0, Lcom/google/android/gm/provider/GmailTransactionListener$GmailTransactionState;->mSendGmailAccountNotifications:Z

    if-eqz p1, :cond_0

    iget-boolean v1, v0, Lcom/google/android/gm/provider/GmailTransactionListener$GmailTransactionState;->mScheduleSyncOnAccountNotification:Z

    if-nez v1, :cond_0

    iput-boolean v3, v0, Lcom/google/android/gm/provider/GmailTransactionListener$GmailTransactionState;->mScheduleSyncOnAccountNotification:Z

    goto :goto_0
.end method

.method public onBegin()V
    .locals 1

    iget-object v0, p0, Lcom/google/android/gm/provider/GmailTransactionListener;->mAccount:Ljava/lang/String;

    invoke-static {v0}, Lcom/google/android/gm/provider/MailIndexerService;->onContentProviderAccess(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/google/android/gm/provider/GmailTransactionListener;->mTestTransactionListener:Landroid/database/sqlite/SQLiteTransactionListener;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/gm/provider/GmailTransactionListener;->mTestTransactionListener:Landroid/database/sqlite/SQLiteTransactionListener;

    invoke-interface {v0}, Landroid/database/sqlite/SQLiteTransactionListener;->onBegin()V

    :cond_0
    return-void
.end method

.method public onCommit()V
    .locals 1

    iget-object v0, p0, Lcom/google/android/gm/provider/GmailTransactionListener;->mTestTransactionListener:Landroid/database/sqlite/SQLiteTransactionListener;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/gm/provider/GmailTransactionListener;->mTestTransactionListener:Landroid/database/sqlite/SQLiteTransactionListener;

    invoke-interface {v0}, Landroid/database/sqlite/SQLiteTransactionListener;->onCommit()V

    :cond_0
    return-void
.end method

.method public onCommitCompleted(Z)V
    .locals 10
    .param p1    # Z

    const/4 v9, 0x0

    iget-object v5, p0, Lcom/google/android/gm/provider/GmailTransactionListener;->mDb:Landroid/database/sqlite/SQLiteDatabase;

    invoke-virtual {v5}, Landroid/database/sqlite/SQLiteDatabase;->inTransaction()Z

    move-result v5

    if-nez v5, :cond_3

    invoke-direct {p0}, Lcom/google/android/gm/provider/GmailTransactionListener;->getConversationIdsSet()Ljava/util/Set;

    move-result-object v1

    if-nez p1, :cond_1

    invoke-interface {v1}, Ljava/util/Set;->isEmpty()Z

    move-result v5

    if-nez v5, :cond_0

    const-string v5, "Gmail"

    const-string v6, "Outermost commit complete, notifying on conversations: %s"

    const/4 v7, 0x1

    new-array v7, v7, [Ljava/lang/Object;

    aput-object v1, v7, v9

    invoke-static {v5, v6, v7}, Lcom/google/android/gm/provider/LogUtils;->d(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)I

    invoke-interface {v1}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Long;

    iget-object v5, p0, Lcom/google/android/gm/provider/GmailTransactionListener;->mContext:Landroid/content/Context;

    iget-object v6, p0, Lcom/google/android/gm/provider/GmailTransactionListener;->mAccount:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v7

    invoke-static {v5, v6, v7, v8}, Lcom/google/android/gm/provider/UiProvider;->onConversationChanged(Landroid/content/Context;Ljava/lang/String;J)V

    goto :goto_0

    :cond_0
    iget-object v5, p0, Lcom/google/android/gm/provider/GmailTransactionListener;->mContext:Landroid/content/Context;

    iget-object v6, p0, Lcom/google/android/gm/provider/GmailTransactionListener;->mAccount:Ljava/lang/String;

    invoke-static {v5, v6}, Lcom/google/android/gm/provider/UiProvider;->broadcastAccountChangeNotification(Landroid/content/Context;Ljava/lang/String;)V

    :cond_1
    invoke-interface {v1}, Ljava/util/Set;->clear()V

    invoke-direct {p0}, Lcom/google/android/gm/provider/GmailTransactionListener;->getLabelIdsSet()Ljava/util/Set;

    move-result-object v3

    iget-object v5, p0, Lcom/google/android/gm/provider/GmailTransactionListener;->mEngine:Lcom/google/android/gm/provider/MailEngine;

    invoke-virtual {v5, v3}, Lcom/google/android/gm/provider/MailEngine;->broadcastLabelNotificationsImpl(Ljava/util/Set;)V

    invoke-interface {v3}, Ljava/util/Set;->clear()V

    iget-object v5, p0, Lcom/google/android/gm/provider/GmailTransactionListener;->mState:Ljava/lang/ThreadLocal;

    invoke-virtual {v5}, Ljava/lang/ThreadLocal;->get()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/google/android/gm/provider/GmailTransactionListener$GmailTransactionState;

    iget-boolean v5, v4, Lcom/google/android/gm/provider/GmailTransactionListener$GmailTransactionState;->mSendGmailAccountNotifications:Z

    if-eqz v5, :cond_2

    iget-object v5, p0, Lcom/google/android/gm/provider/GmailTransactionListener;->mEngine:Lcom/google/android/gm/provider/MailEngine;

    iget-boolean v6, v4, Lcom/google/android/gm/provider/GmailTransactionListener$GmailTransactionState;->mScheduleSyncOnAccountNotification:Z

    invoke-virtual {v5, v6}, Lcom/google/android/gm/provider/MailEngine;->sendAccountNotifications(Z)V

    :cond_2
    iput-boolean v9, v4, Lcom/google/android/gm/provider/GmailTransactionListener$GmailTransactionState;->mSendGmailAccountNotifications:Z

    iput-boolean v9, v4, Lcom/google/android/gm/provider/GmailTransactionListener$GmailTransactionState;->mScheduleSyncOnAccountNotification:Z

    :cond_3
    return-void
.end method

.method public onRollback()V
    .locals 1

    iget-object v0, p0, Lcom/google/android/gm/provider/GmailTransactionListener;->mTestTransactionListener:Landroid/database/sqlite/SQLiteTransactionListener;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/gm/provider/GmailTransactionListener;->mTestTransactionListener:Landroid/database/sqlite/SQLiteTransactionListener;

    invoke-interface {v0}, Landroid/database/sqlite/SQLiteTransactionListener;->onRollback()V

    :cond_0
    return-void
.end method

.method public onRollbackCompleted()V
    .locals 7

    const/4 v6, 0x1

    const/4 v5, 0x0

    iget-object v2, p0, Lcom/google/android/gm/provider/GmailTransactionListener;->mDb:Landroid/database/sqlite/SQLiteDatabase;

    invoke-virtual {v2}, Landroid/database/sqlite/SQLiteDatabase;->inTransaction()Z

    move-result v2

    if-nez v2, :cond_2

    invoke-direct {p0}, Lcom/google/android/gm/provider/GmailTransactionListener;->getConversationIdsSet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_0

    const-string v2, "Gmail"

    const-string v3, "Rolled back outermost conversation commit, NOT notifying on: %s"

    new-array v4, v6, [Ljava/lang/Object;

    aput-object v0, v4, v5

    invoke-static {v2, v3, v4}, Lcom/google/android/gm/provider/LogUtils;->d(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)I

    :cond_0
    invoke-interface {v0}, Ljava/util/Set;->clear()V

    invoke-direct {p0}, Lcom/google/android/gm/provider/GmailTransactionListener;->getLabelIdsSet()Ljava/util/Set;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/Set;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_1

    const-string v2, "Gmail"

    const-string v3, "Rolled back outermost label commit, NOT notifying on: %s"

    new-array v4, v6, [Ljava/lang/Object;

    aput-object v1, v4, v5

    invoke-static {v2, v3, v4}, Lcom/google/android/gm/provider/LogUtils;->d(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)I

    :cond_1
    invoke-interface {v1}, Ljava/util/Set;->clear()V

    :cond_2
    return-void
.end method

.method public setTestTransactionListener(Landroid/database/sqlite/SQLiteTransactionListener;)V
    .locals 0
    .param p1    # Landroid/database/sqlite/SQLiteTransactionListener;

    iput-object p1, p0, Lcom/google/android/gm/provider/GmailTransactionListener;->mTestTransactionListener:Landroid/database/sqlite/SQLiteTransactionListener;

    return-void
.end method
