.class public abstract Lcom/google/android/gm/provider/DatabaseInitializer;
.super Ljava/lang/Object;
.source "DatabaseInitializer.java"


# instance fields
.field protected final mDb:Landroid/database/sqlite/SQLiteDatabase;

.field protected final mEngine:Lcom/google/android/gm/provider/MailEngine;

.field protected final mInitialDbVersion:I


# direct methods
.method protected constructor <init>(I)V
    .locals 1
    .param p1    # I

    const/4 v0, 0x0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lcom/google/android/gm/provider/DatabaseInitializer;->mEngine:Lcom/google/android/gm/provider/MailEngine;

    iput-object v0, p0, Lcom/google/android/gm/provider/DatabaseInitializer;->mDb:Landroid/database/sqlite/SQLiteDatabase;

    iput p1, p0, Lcom/google/android/gm/provider/DatabaseInitializer;->mInitialDbVersion:I

    return-void
.end method

.method constructor <init>(Lcom/google/android/gm/provider/MailEngine;Landroid/database/sqlite/SQLiteDatabase;)V
    .locals 1
    .param p1    # Lcom/google/android/gm/provider/MailEngine;
    .param p2    # Landroid/database/sqlite/SQLiteDatabase;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/google/android/gm/provider/DatabaseInitializer;->mEngine:Lcom/google/android/gm/provider/MailEngine;

    iput-object p2, p0, Lcom/google/android/gm/provider/DatabaseInitializer;->mDb:Landroid/database/sqlite/SQLiteDatabase;

    invoke-virtual {p2}, Landroid/database/sqlite/SQLiteDatabase;->getVersion()I

    move-result v0

    iput v0, p0, Lcom/google/android/gm/provider/DatabaseInitializer;->mInitialDbVersion:I

    return-void
.end method

.method private upgradeDatabase(I)I
    .locals 6
    .param p1    # I

    invoke-virtual {p0, p1}, Lcom/google/android/gm/provider/DatabaseInitializer;->getTargetDbVersion(I)I

    move-result v3

    invoke-virtual {p0, v3}, Lcom/google/android/gm/provider/DatabaseInitializer;->findUpgradeMethod(I)Ljava/lang/reflect/Method;

    move-result-object v2

    const/4 v1, 0x0

    const/4 v4, 0x0

    :try_start_0
    new-array v4, v4, [Ljava/lang/Object;

    invoke-virtual {v2, p0, v4}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v4, p0, Lcom/google/android/gm/provider/DatabaseInitializer;->mDb:Landroid/database/sqlite/SQLiteDatabase;

    invoke-virtual {v4, v3}, Landroid/database/sqlite/SQLiteDatabase;->setVersion(I)V
    :try_end_0
    .catch Ljava/lang/reflect/InvocationTargetException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/IllegalAccessException; {:try_start_0 .. :try_end_0} :catch_1

    :goto_0
    if-eqz v1, :cond_0

    new-instance v4, Ljava/lang/IllegalStateException;

    const-string v5, "Failed to invoke upgrade Method"

    invoke-direct {v4, v5, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v4

    :catch_0
    move-exception v0

    move-object v1, v0

    goto :goto_0

    :catch_1
    move-exception v0

    move-object v1, v0

    goto :goto_0

    :cond_0
    return v3
.end method


# virtual methods
.method public abstract bootstrapDatabase()V
.end method

.method findUpgradeMethod(I)Ljava/lang/reflect/Method;
    .locals 5
    .param p1    # I

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    :try_start_0
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "upgradeDbTo"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    const/4 v2, 0x0

    check-cast v2, [Ljava/lang/Class;

    invoke-virtual {v0, v3, v2}, Ljava/lang/Class;->getMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;
    :try_end_0
    .catch Ljava/lang/NoSuchMethodException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v2

    return-object v2

    :catch_0
    move-exception v1

    new-instance v2, Ljava/lang/IllegalStateException;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Missing upgrade to version: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v2
.end method

.method getTargetDbVersion(I)I
    .locals 1
    .param p1    # I

    add-int/lit8 v0, p1, 0x1

    return v0
.end method

.method performUpgrade(I)V
    .locals 2
    .param p1    # I

    iget v0, p0, Lcom/google/android/gm/provider/DatabaseInitializer;->mInitialDbVersion:I

    :goto_0
    if-ge v0, p1, :cond_0

    invoke-direct {p0, v0}, Lcom/google/android/gm/provider/DatabaseInitializer;->upgradeDatabase(I)I

    move-result v1

    move v0, v1

    goto :goto_0

    :cond_0
    return-void
.end method
