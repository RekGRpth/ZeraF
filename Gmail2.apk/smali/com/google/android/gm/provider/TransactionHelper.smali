.class public Lcom/google/android/gm/provider/TransactionHelper;
.super Ljava/lang/Object;
.source "TransactionHelper.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/gm/provider/TransactionHelper$ThreadTransactionState;,
        Lcom/google/android/gm/provider/TransactionHelper$BetterTransactionListener;
    }
.end annotation


# instance fields
.field private final mDb:Landroid/database/sqlite/SQLiteDatabase;

.field private final mStates:Ljava/lang/ThreadLocal;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/ThreadLocal",
            "<",
            "Ljava/util/Stack",
            "<",
            "Lcom/google/android/gm/provider/TransactionHelper$ThreadTransactionState;",
            ">;>;"
        }
    .end annotation
.end field

.field private final mSuppressUiNotifications:Ljava/lang/ThreadLocal;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/ThreadLocal",
            "<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/database/sqlite/SQLiteDatabase;)V
    .locals 1
    .param p1    # Landroid/database/sqlite/SQLiteDatabase;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Lcom/google/android/gm/provider/TransactionHelper$1;

    invoke-direct {v0, p0}, Lcom/google/android/gm/provider/TransactionHelper$1;-><init>(Lcom/google/android/gm/provider/TransactionHelper;)V

    iput-object v0, p0, Lcom/google/android/gm/provider/TransactionHelper;->mSuppressUiNotifications:Ljava/lang/ThreadLocal;

    iput-object p1, p0, Lcom/google/android/gm/provider/TransactionHelper;->mDb:Landroid/database/sqlite/SQLiteDatabase;

    new-instance v0, Lcom/google/android/gm/provider/TransactionHelper$2;

    invoke-direct {v0, p0}, Lcom/google/android/gm/provider/TransactionHelper$2;-><init>(Lcom/google/android/gm/provider/TransactionHelper;)V

    iput-object v0, p0, Lcom/google/android/gm/provider/TransactionHelper;->mStates:Ljava/lang/ThreadLocal;

    return-void
.end method

.method private getStates()Ljava/util/Stack;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Stack",
            "<",
            "Lcom/google/android/gm/provider/TransactionHelper$ThreadTransactionState;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/google/android/gm/provider/TransactionHelper;->mStates:Ljava/lang/ThreadLocal;

    invoke-virtual {v0}, Ljava/lang/ThreadLocal;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Stack;

    return-object v0
.end method


# virtual methods
.method public beginTransactionNonExclusive()V
    .locals 3

    invoke-direct {p0}, Lcom/google/android/gm/provider/TransactionHelper;->getStates()Ljava/util/Stack;

    move-result-object v0

    new-instance v1, Lcom/google/android/gm/provider/TransactionHelper$ThreadTransactionState;

    const/4 v2, 0x0

    invoke-direct {v1, v2}, Lcom/google/android/gm/provider/TransactionHelper$ThreadTransactionState;-><init>(Lcom/google/android/gm/provider/TransactionHelper$BetterTransactionListener;)V

    invoke-virtual {v0, v1}, Ljava/util/Stack;->push(Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v0, p0, Lcom/google/android/gm/provider/TransactionHelper;->mDb:Landroid/database/sqlite/SQLiteDatabase;

    invoke-virtual {v0}, Landroid/database/sqlite/SQLiteDatabase;->beginTransactionNonExclusive()V

    return-void
.end method

.method public beginTransactionWithListenerNonExclusive(Lcom/google/android/gm/provider/TransactionHelper$BetterTransactionListener;)V
    .locals 2
    .param p1    # Lcom/google/android/gm/provider/TransactionHelper$BetterTransactionListener;

    invoke-direct {p0}, Lcom/google/android/gm/provider/TransactionHelper;->getStates()Ljava/util/Stack;

    move-result-object v0

    new-instance v1, Lcom/google/android/gm/provider/TransactionHelper$ThreadTransactionState;

    invoke-direct {v1, p1}, Lcom/google/android/gm/provider/TransactionHelper$ThreadTransactionState;-><init>(Lcom/google/android/gm/provider/TransactionHelper$BetterTransactionListener;)V

    invoke-virtual {v0, v1}, Ljava/util/Stack;->push(Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v0, p0, Lcom/google/android/gm/provider/TransactionHelper;->mDb:Landroid/database/sqlite/SQLiteDatabase;

    invoke-virtual {v0, p1}, Landroid/database/sqlite/SQLiteDatabase;->beginTransactionWithListenerNonExclusive(Landroid/database/sqlite/SQLiteTransactionListener;)V

    return-void
.end method

.method public endTransaction()V
    .locals 6

    const/4 v4, 0x1

    iget-object v3, p0, Lcom/google/android/gm/provider/TransactionHelper;->mDb:Landroid/database/sqlite/SQLiteDatabase;

    invoke-virtual {v3}, Landroid/database/sqlite/SQLiteDatabase;->endTransaction()V

    invoke-direct {p0}, Lcom/google/android/gm/provider/TransactionHelper;->getStates()Ljava/util/Stack;

    move-result-object v3

    invoke-virtual {v3}, Ljava/util/Stack;->pop()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gm/provider/TransactionHelper$ThreadTransactionState;

    iget-boolean v3, v0, Lcom/google/android/gm/provider/TransactionHelper$ThreadTransactionState;->mSuccess:Z

    if-eqz v3, :cond_2

    iget-boolean v3, v0, Lcom/google/android/gm/provider/TransactionHelper$ThreadTransactionState;->mChildFailed:Z

    if-nez v3, :cond_2

    move v2, v4

    :goto_0
    iget-object v3, v0, Lcom/google/android/gm/provider/TransactionHelper$ThreadTransactionState;->mListener:Lcom/google/android/gm/provider/TransactionHelper$BetterTransactionListener;

    if-eqz v3, :cond_0

    if-eqz v2, :cond_3

    iget-object v5, v0, Lcom/google/android/gm/provider/TransactionHelper$ThreadTransactionState;->mListener:Lcom/google/android/gm/provider/TransactionHelper$BetterTransactionListener;

    iget-object v3, p0, Lcom/google/android/gm/provider/TransactionHelper;->mSuppressUiNotifications:Ljava/lang/ThreadLocal;

    invoke-virtual {v3}, Ljava/lang/ThreadLocal;->get()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Boolean;

    invoke-virtual {v3}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v3

    invoke-interface {v5, v3}, Lcom/google/android/gm/provider/TransactionHelper$BetterTransactionListener;->onCommitCompleted(Z)V

    :cond_0
    :goto_1
    if-nez v2, :cond_1

    invoke-direct {p0}, Lcom/google/android/gm/provider/TransactionHelper;->getStates()Ljava/util/Stack;

    move-result-object v1

    invoke-virtual {v1}, Ljava/util/Stack;->empty()Z

    move-result v3

    if-nez v3, :cond_1

    invoke-virtual {v1}, Ljava/util/Stack;->peek()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/google/android/gm/provider/TransactionHelper$ThreadTransactionState;

    iput-boolean v4, v3, Lcom/google/android/gm/provider/TransactionHelper$ThreadTransactionState;->mChildFailed:Z

    :cond_1
    return-void

    :cond_2
    const/4 v2, 0x0

    goto :goto_0

    :cond_3
    iget-object v3, v0, Lcom/google/android/gm/provider/TransactionHelper$ThreadTransactionState;->mListener:Lcom/google/android/gm/provider/TransactionHelper$BetterTransactionListener;

    invoke-interface {v3}, Lcom/google/android/gm/provider/TransactionHelper$BetterTransactionListener;->onRollbackCompleted()V

    goto :goto_1
.end method

.method public setTransactionSuccessful()V
    .locals 2

    iget-object v0, p0, Lcom/google/android/gm/provider/TransactionHelper;->mDb:Landroid/database/sqlite/SQLiteDatabase;

    invoke-virtual {v0}, Landroid/database/sqlite/SQLiteDatabase;->setTransactionSuccessful()V

    invoke-direct {p0}, Lcom/google/android/gm/provider/TransactionHelper;->getStates()Ljava/util/Stack;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/Stack;->peek()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gm/provider/TransactionHelper$ThreadTransactionState;

    const/4 v1, 0x1

    iput-boolean v1, v0, Lcom/google/android/gm/provider/TransactionHelper$ThreadTransactionState;->mSuccess:Z

    return-void
.end method

.method suppressUiNotifications()V
    .locals 2

    iget-object v0, p0, Lcom/google/android/gm/provider/TransactionHelper;->mSuppressUiNotifications:Ljava/lang/ThreadLocal;

    sget-object v1, Ljava/lang/Boolean;->TRUE:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Ljava/lang/ThreadLocal;->set(Ljava/lang/Object;)V

    return-void
.end method
