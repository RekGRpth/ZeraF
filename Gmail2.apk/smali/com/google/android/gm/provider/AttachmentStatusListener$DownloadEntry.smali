.class Lcom/google/android/gm/provider/AttachmentStatusListener$DownloadEntry;
.super Ljava/lang/Object;
.source "AttachmentStatusListener.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/gm/provider/AttachmentStatusListener;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "DownloadEntry"
.end annotation


# instance fields
.field final mDownloadId:J

.field final mDownloadSize:J

.field final mDownloadStatus:I

.field final mError:I


# direct methods
.method constructor <init>(JJII)V
    .locals 0
    .param p1    # J
    .param p3    # J
    .param p5    # I
    .param p6    # I

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-wide p1, p0, Lcom/google/android/gm/provider/AttachmentStatusListener$DownloadEntry;->mDownloadId:J

    iput-wide p3, p0, Lcom/google/android/gm/provider/AttachmentStatusListener$DownloadEntry;->mDownloadSize:J

    iput p5, p0, Lcom/google/android/gm/provider/AttachmentStatusListener$DownloadEntry;->mDownloadStatus:I

    iput p6, p0, Lcom/google/android/gm/provider/AttachmentStatusListener$DownloadEntry;->mError:I

    return-void
.end method
