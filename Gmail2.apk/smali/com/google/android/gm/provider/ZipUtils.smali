.class public Lcom/google/android/gm/provider/ZipUtils;
.super Ljava/lang/Object;
.source "ZipUtils.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static deflate([B)[B
    .locals 2
    .param p0    # [B

    const/4 v0, 0x0

    array-length v1, p0

    invoke-static {p0, v0, v1}, Lcom/google/android/gm/provider/ZipUtils;->deflate([BII)[B

    move-result-object v0

    return-object v0
.end method

.method public static deflate([BII)[B
    .locals 5
    .param p0    # [B
    .param p1    # I
    .param p2    # I

    new-instance v2, Ljava/io/ByteArrayOutputStream;

    invoke-direct {v2}, Ljava/io/ByteArrayOutputStream;-><init>()V

    new-instance v1, Ljava/util/zip/DeflaterOutputStream;

    new-instance v3, Ljava/util/zip/Deflater;

    invoke-direct {v3}, Ljava/util/zip/Deflater;-><init>()V

    invoke-direct {v1, v2, v3}, Ljava/util/zip/DeflaterOutputStream;-><init>(Ljava/io/OutputStream;Ljava/util/zip/Deflater;)V

    :try_start_0
    invoke-virtual {v1, p0, p1, p2}, Ljava/util/zip/DeflaterOutputStream;->write([BII)V

    invoke-virtual {v1}, Ljava/util/zip/DeflaterOutputStream;->close()V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    invoke-virtual {v2}, Ljava/io/ByteArrayOutputStream;->toByteArray()[B

    move-result-object v3

    return-object v3

    :catch_0
    move-exception v0

    new-instance v3, Ljava/lang/IllegalStateException;

    const-string v4, "ByteArrayOutputStream threw "

    invoke-direct {v3, v4, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v3
.end method

.method public static inflate(Ljava/util/zip/Inflater;)[B
    .locals 2
    .param p0    # Ljava/util/zip/Inflater;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/util/zip/DataFormatException;
        }
    .end annotation

    invoke-static {p0}, Lcom/google/android/gm/provider/ZipUtils;->inflateToStream(Ljava/util/zip/Inflater;)Ljava/io/ByteArrayOutputStream;

    move-result-object v0

    invoke-virtual {v0}, Ljava/io/ByteArrayOutputStream;->toByteArray()[B

    move-result-object v1

    return-object v1
.end method

.method private static inflateToStream(Ljava/util/zip/Inflater;)Ljava/io/ByteArrayOutputStream;
    .locals 4
    .param p0    # Ljava/util/zip/Inflater;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/util/zip/DataFormatException;
        }
    .end annotation

    new-instance v2, Ljava/io/ByteArrayOutputStream;

    invoke-direct {v2}, Ljava/io/ByteArrayOutputStream;-><init>()V

    const/16 v3, 0x400

    new-array v0, v3, [B

    const/4 v1, 0x0

    :cond_0
    invoke-virtual {p0, v0}, Ljava/util/zip/Inflater;->inflate([B)I

    move-result v1

    if-eqz v1, :cond_1

    const/4 v3, 0x0

    invoke-virtual {v2, v0, v3, v1}, Ljava/io/ByteArrayOutputStream;->write([BII)V

    :cond_1
    if-nez v1, :cond_0

    return-object v2
.end method

.method public static inflateToStream([B[B)Ljava/io/InputStream;
    .locals 3
    .param p0    # [B
    .param p1    # [B

    new-instance v0, Lcom/google/android/gm/provider/ZipUtils$1;

    invoke-direct {v0, p1}, Lcom/google/android/gm/provider/ZipUtils$1;-><init>([B)V

    new-instance v1, Lcom/google/android/gm/provider/ZipUtils$2;

    new-instance v2, Ljava/io/ByteArrayInputStream;

    invoke-direct {v2, p0}, Ljava/io/ByteArrayInputStream;-><init>([B)V

    invoke-direct {v1, v2, v0}, Lcom/google/android/gm/provider/ZipUtils$2;-><init>(Ljava/io/InputStream;Ljava/util/zip/Inflater;)V

    return-object v1
.end method

.method public static inflateToUTF8(Ljava/util/zip/Inflater;)Ljava/lang/String;
    .locals 2
    .param p0    # Ljava/util/zip/Inflater;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/util/zip/DataFormatException;,
            Ljava/io/UnsupportedEncodingException;
        }
    .end annotation

    invoke-static {p0}, Lcom/google/android/gm/provider/ZipUtils;->inflateToStream(Ljava/util/zip/Inflater;)Ljava/io/ByteArrayOutputStream;

    move-result-object v0

    const-string v1, "UTF-8"

    invoke-virtual {v0, v1}, Ljava/io/ByteArrayOutputStream;->toString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    return-object v1
.end method

.method public static inflateToUTF8([BII)Ljava/lang/String;
    .locals 2
    .param p0    # [B
    .param p1    # I
    .param p2    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/util/zip/DataFormatException;,
            Ljava/io/UnsupportedEncodingException;
        }
    .end annotation

    new-instance v0, Ljava/util/zip/Inflater;

    invoke-direct {v0}, Ljava/util/zip/Inflater;-><init>()V

    :try_start_0
    invoke-virtual {v0, p0, p1, p2}, Ljava/util/zip/Inflater;->setInput([BII)V

    invoke-static {v0}, Lcom/google/android/gm/provider/ZipUtils;->inflateToUTF8(Ljava/util/zip/Inflater;)Ljava/lang/String;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v1

    invoke-virtual {v0}, Ljava/util/zip/Inflater;->end()V

    return-object v1

    :catchall_0
    move-exception v1

    invoke-virtual {v0}, Ljava/util/zip/Inflater;->end()V

    throw v1
.end method
