.class Lcom/google/android/gm/provider/LabelRecord;
.super Ljava/lang/Object;
.source "LabelRecord.java"


# instance fields
.field public dateReceived:J

.field public isZombie:Z

.field public sortMessageId:J


# direct methods
.method public constructor <init>()V
    .locals 2

    const-wide/16 v0, 0x0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-wide v0, p0, Lcom/google/android/gm/provider/LabelRecord;->sortMessageId:J

    iput-wide v0, p0, Lcom/google/android/gm/provider/LabelRecord;->dateReceived:J

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/gm/provider/LabelRecord;->isZombie:Z

    return-void
.end method

.method public constructor <init>(JJZ)V
    .locals 2
    .param p1    # J
    .param p3    # J
    .param p5    # Z

    const-wide/16 v0, 0x0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-wide v0, p0, Lcom/google/android/gm/provider/LabelRecord;->sortMessageId:J

    iput-wide v0, p0, Lcom/google/android/gm/provider/LabelRecord;->dateReceived:J

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/gm/provider/LabelRecord;->isZombie:Z

    iput-wide p1, p0, Lcom/google/android/gm/provider/LabelRecord;->sortMessageId:J

    iput-wide p3, p0, Lcom/google/android/gm/provider/LabelRecord;->dateReceived:J

    iput-boolean p5, p0, Lcom/google/android/gm/provider/LabelRecord;->isZombie:Z

    return-void
.end method
