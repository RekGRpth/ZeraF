.class public Lcom/google/android/gm/OnePaneLabelsController;
.super Lcom/google/android/gm/BaseLabelsController;
.source "OnePaneLabelsController.java"


# instance fields
.field private mFromShortcut:Z


# direct methods
.method public constructor <init>(Lcom/google/android/gm/LabelsActivityController$ControllableLabelsActivity;)V
    .locals 0
    .param p1    # Lcom/google/android/gm/LabelsActivityController$ControllableLabelsActivity;

    invoke-direct {p0, p1}, Lcom/google/android/gm/BaseLabelsController;-><init>(Lcom/google/android/gm/LabelsActivityController$ControllableLabelsActivity;)V

    return-void
.end method


# virtual methods
.method public handleLabelListResumed(Lcom/google/android/gm/LabelListFragment;)V
    .locals 2
    .param p1    # Lcom/google/android/gm/LabelListFragment;

    invoke-virtual {p1}, Lcom/google/android/gm/LabelListFragment;->isManageLabelMode()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/gm/OnePaneLabelsController;->mActionBar:Landroid/app/ActionBar;

    const v1, 0x7f090060

    invoke-virtual {v0, v1}, Landroid/app/ActionBar;->setTitle(I)V

    iget-object v0, p0, Lcom/google/android/gm/OnePaneLabelsController;->mActionBar:Landroid/app/ActionBar;

    const v1, 0x7f090169

    invoke-virtual {v0, v1}, Landroid/app/ActionBar;->setSubtitle(I)V

    :goto_0
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/google/android/gm/OnePaneLabelsController;->toggleUpButton(Z)V

    return-void

    :cond_0
    iget-object v0, p0, Lcom/google/android/gm/OnePaneLabelsController;->mActionBar:Landroid/app/ActionBar;

    const v1, 0x7f090168

    invoke-virtual {v0, v1}, Landroid/app/ActionBar;->setTitle(I)V

    iget-object v0, p0, Lcom/google/android/gm/OnePaneLabelsController;->mActionBar:Landroid/app/ActionBar;

    iget-object v1, p0, Lcom/google/android/gm/OnePaneLabelsController;->mAccount:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/app/ActionBar;->setSubtitle(Ljava/lang/CharSequence;)V

    goto :goto_0
.end method

.method public initialize(Landroid/os/Bundle;)V
    .locals 4
    .param p1    # Landroid/os/Bundle;

    const/4 v2, 0x0

    const/4 v1, 0x1

    invoke-super {p0, p1}, Lcom/google/android/gm/BaseLabelsController;->initialize(Landroid/os/Bundle;)V

    iget-object v0, p0, Lcom/google/android/gm/OnePaneLabelsController;->mActivity:Lcom/google/android/gm/LabelsActivityController$ControllableLabelsActivity;

    const v3, 0x7f040055

    invoke-interface {v0, v3}, Lcom/google/android/gm/LabelsActivityController$ControllableLabelsActivity;->setContentView(I)V

    iget-object v0, p0, Lcom/google/android/gm/OnePaneLabelsController;->mDefaultLabel:Ljava/lang/String;

    if-eqz v0, :cond_0

    move v0, v1

    :goto_0
    iput-boolean v0, p0, Lcom/google/android/gm/OnePaneLabelsController;->mFromShortcut:Z

    if-nez p1, :cond_2

    iget-boolean v0, p0, Lcom/google/android/gm/OnePaneLabelsController;->mFromShortcut:Z

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/gm/OnePaneLabelsController;->mDefaultLabel:Ljava/lang/String;

    invoke-virtual {p0, v0}, Lcom/google/android/gm/OnePaneLabelsController;->showLabelSettings(Ljava/lang/String;)V

    :goto_1
    return-void

    :cond_0
    move v0, v2

    goto :goto_0

    :cond_1
    invoke-virtual {p0}, Lcom/google/android/gm/OnePaneLabelsController;->showManageLabelList()V

    goto :goto_1

    :cond_2
    const-string v0, "label-list-visible"

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    iput-boolean v0, p0, Lcom/google/android/gm/OnePaneLabelsController;->mLabelListVisbile:Z

    iget-boolean v0, p0, Lcom/google/android/gm/OnePaneLabelsController;->mLabelListVisbile:Z

    if-nez v0, :cond_3

    :goto_2
    invoke-virtual {p0, v1}, Lcom/google/android/gm/OnePaneLabelsController;->toggleUpButton(Z)V

    goto :goto_1

    :cond_3
    move v1, v2

    goto :goto_2
.end method

.method protected showLabelSettings(Ljava/lang/String;)V
    .locals 4
    .param p1    # Ljava/lang/String;

    iget-object v2, p0, Lcom/google/android/gm/OnePaneLabelsController;->mContext:Landroid/content/Context;

    iget-object v3, p0, Lcom/google/android/gm/OnePaneLabelsController;->mAccount:Ljava/lang/String;

    invoke-static {v2, v3, p1}, Lcom/google/android/gm/provider/LabelManager;->getLabel(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/gm/provider/Label;

    move-result-object v1

    if-nez v1, :cond_0

    iget-object v2, p0, Lcom/google/android/gm/OnePaneLabelsController;->mActivity:Lcom/google/android/gm/LabelsActivityController$ControllableLabelsActivity;

    invoke-interface {v2}, Lcom/google/android/gm/LabelsActivityController$ControllableLabelsActivity;->onBackPressed()V

    :goto_0
    return-void

    :cond_0
    iget-object v2, p0, Lcom/google/android/gm/OnePaneLabelsController;->mActivity:Lcom/google/android/gm/LabelsActivityController$ControllableLabelsActivity;

    invoke-interface {v2}, Lcom/google/android/gm/LabelsActivityController$ControllableLabelsActivity;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v2

    invoke-virtual {v2}, Landroid/app/FragmentManager;->beginTransaction()Landroid/app/FragmentTransaction;

    move-result-object v0

    iget-boolean v2, p0, Lcom/google/android/gm/OnePaneLabelsController;->mFromShortcut:Z

    if-nez v2, :cond_1

    const/4 v2, 0x0

    invoke-virtual {v0, v2}, Landroid/app/FragmentTransaction;->addToBackStack(Ljava/lang/String;)Landroid/app/FragmentTransaction;

    const/16 v2, 0x1001

    invoke-virtual {v0, v2}, Landroid/app/FragmentTransaction;->setTransition(I)Landroid/app/FragmentTransaction;

    :cond_1
    const v2, 0x7f0800b4

    iget-object v3, p0, Lcom/google/android/gm/OnePaneLabelsController;->mAccount:Ljava/lang/String;

    invoke-static {v3, p1}, Lcom/google/android/gm/preference/LabelSettingsFragment;->newInstance(Ljava/lang/String;Ljava/lang/String;)Landroid/app/Fragment;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, Landroid/app/FragmentTransaction;->replace(ILandroid/app/Fragment;)Landroid/app/FragmentTransaction;

    invoke-virtual {v0}, Landroid/app/FragmentTransaction;->commitAllowingStateLoss()I

    iget-object v2, p0, Lcom/google/android/gm/OnePaneLabelsController;->mActionBar:Landroid/app/ActionBar;

    invoke-virtual {v1}, Lcom/google/android/gm/provider/Label;->getName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/app/ActionBar;->setTitle(Ljava/lang/CharSequence;)V

    iget-object v2, p0, Lcom/google/android/gm/OnePaneLabelsController;->mActionBar:Landroid/app/ActionBar;

    const v3, 0x7f090169

    invoke-virtual {v2, v3}, Landroid/app/ActionBar;->setSubtitle(I)V

    const/4 v2, 0x0

    iput-boolean v2, p0, Lcom/google/android/gm/OnePaneLabelsController;->mLabelListVisbile:Z

    const/4 v2, 0x1

    invoke-virtual {p0, v2}, Lcom/google/android/gm/OnePaneLabelsController;->toggleUpButton(Z)V

    iget-object v2, p0, Lcom/google/android/gm/OnePaneLabelsController;->mActivity:Lcom/google/android/gm/LabelsActivityController$ControllableLabelsActivity;

    invoke-interface {v2}, Lcom/google/android/gm/LabelsActivityController$ControllableLabelsActivity;->invalidateOptionsMenu()V

    goto :goto_0
.end method

.method protected showManageLabelList()V
    .locals 5

    const/4 v4, 0x1

    iget-object v1, p0, Lcom/google/android/gm/OnePaneLabelsController;->mActivity:Lcom/google/android/gm/LabelsActivityController$ControllableLabelsActivity;

    invoke-interface {v1}, Lcom/google/android/gm/LabelsActivityController$ControllableLabelsActivity;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v1

    invoke-virtual {v1}, Landroid/app/FragmentManager;->beginTransaction()Landroid/app/FragmentTransaction;

    move-result-object v0

    const v1, 0x7f0800b4

    iget-object v2, p0, Lcom/google/android/gm/OnePaneLabelsController;->mAccount:Ljava/lang/String;

    const/4 v3, 0x0

    invoke-static {v2, v3, v4, v4}, Lcom/google/android/gm/LabelListFragment;->newInstance(Ljava/lang/String;Ljava/lang/String;II)Lcom/google/android/gm/LabelListFragment;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/app/FragmentTransaction;->replace(ILandroid/app/Fragment;)Landroid/app/FragmentTransaction;

    invoke-virtual {v0}, Landroid/app/FragmentTransaction;->commitAllowingStateLoss()I

    return-void
.end method
