.class public Lcom/google/android/gm/utils/OutgoingMsgPrefs;
.super Ljava/lang/Object;
.source "OutgoingMsgPrefs.java"


# static fields
.field public static ACCOUNT_OUTGOING_PREFS:Ljava/util/concurrent/ConcurrentHashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/concurrent/ConcurrentHashMap",
            "<",
            "Ljava/lang/String;",
            "[",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private static VALUE_COL:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const-string v0, "value"

    sput-object v0, Lcom/google/android/gm/utils/OutgoingMsgPrefs;->VALUE_COL:Ljava/lang/String;

    new-instance v0, Ljava/util/concurrent/ConcurrentHashMap;

    invoke-direct {v0}, Ljava/util/concurrent/ConcurrentHashMap;-><init>()V

    sput-object v0, Lcom/google/android/gm/utils/OutgoingMsgPrefs;->ACCOUNT_OUTGOING_PREFS:Ljava/util/concurrent/ConcurrentHashMap;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static addOrUpdateDisplayName(Ljava/lang/String;Ljava/lang/String;)V
    .locals 2
    .param p0    # Ljava/lang/String;
    .param p1    # Ljava/lang/String;

    invoke-static {p0}, Lcom/google/android/gm/utils/OutgoingMsgPrefs;->ensurePrefs(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x1

    aput-object p1, v0, v1

    sget-object v1, Lcom/google/android/gm/utils/OutgoingMsgPrefs;->ACCOUNT_OUTGOING_PREFS:Ljava/util/concurrent/ConcurrentHashMap;

    invoke-virtual {v1, p0, v0}, Ljava/util/concurrent/ConcurrentHashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    return-void
.end method

.method public static addOrUpdateReplyTo(Ljava/lang/String;Ljava/lang/String;)V
    .locals 2
    .param p0    # Ljava/lang/String;
    .param p1    # Ljava/lang/String;

    invoke-static {p0}, Lcom/google/android/gm/utils/OutgoingMsgPrefs;->ensurePrefs(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x0

    aput-object p1, v0, v1

    sget-object v1, Lcom/google/android/gm/utils/OutgoingMsgPrefs;->ACCOUNT_OUTGOING_PREFS:Ljava/util/concurrent/ConcurrentHashMap;

    invoke-virtual {v1, p0, v0}, Ljava/util/concurrent/ConcurrentHashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    return-void
.end method

.method private static ensurePrefs(Ljava/lang/String;)[Ljava/lang/String;
    .locals 2
    .param p0    # Ljava/lang/String;

    sget-object v1, Lcom/google/android/gm/utils/OutgoingMsgPrefs;->ACCOUNT_OUTGOING_PREFS:Ljava/util/concurrent/ConcurrentHashMap;

    if-nez v1, :cond_1

    const/4 v0, 0x0

    :goto_0
    if-nez v0, :cond_0

    const/4 v1, 0x2

    new-array v0, v1, [Ljava/lang/String;

    :cond_0
    return-object v0

    :cond_1
    sget-object v1, Lcom/google/android/gm/utils/OutgoingMsgPrefs;->ACCOUNT_OUTGOING_PREFS:Ljava/util/concurrent/ConcurrentHashMap;

    invoke-virtual {v1, p0}, Ljava/util/concurrent/ConcurrentHashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, [Ljava/lang/String;

    move-object v0, v1

    goto :goto_0
.end method

.method public static instantiateOutgoingPrefs(Ljava/lang/String;Landroid/database/Cursor;)V
    .locals 6
    .param p0    # Ljava/lang/String;
    .param p1    # Landroid/database/Cursor;

    const/4 v5, 0x2

    new-array v0, v5, [Ljava/lang/String;

    :try_start_0
    invoke-interface {p1}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v5

    if-eqz v5, :cond_3

    sget-object v5, Lcom/google/android/gm/utils/OutgoingMsgPrefs;->VALUE_COL:Ljava/lang/String;

    invoke-interface {p1, v5}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v4

    const-string v5, "name"

    invoke-interface {p1, v5}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v3

    :cond_0
    invoke-interface {p1, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v2

    const/4 v1, -0x1

    const-string v5, "sx_rt"

    invoke-virtual {v2, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_4

    const/4 v1, 0x0

    :cond_1
    :goto_0
    if-ltz v1, :cond_2

    invoke-interface {p1, v4}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v5

    aput-object v5, v0, v1

    :cond_2
    invoke-interface {p1}, Landroid/database/Cursor;->moveToNext()Z

    move-result v5

    if-nez v5, :cond_0

    sget-object v5, Lcom/google/android/gm/utils/OutgoingMsgPrefs;->ACCOUNT_OUTGOING_PREFS:Ljava/util/concurrent/ConcurrentHashMap;

    invoke-virtual {v5, p0, v0}, Ljava/util/concurrent/ConcurrentHashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :cond_3
    invoke-interface {p1}, Landroid/database/Cursor;->close()V

    return-void

    :cond_4
    :try_start_1
    const-string v5, "sx_dn"

    invoke-virtual {v2, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result v5

    if-eqz v5, :cond_1

    const/4 v1, 0x1

    goto :goto_0

    :catchall_0
    move-exception v5

    invoke-interface {p1}, Landroid/database/Cursor;->close()V

    throw v5
.end method
