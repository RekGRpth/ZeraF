.class public Lcom/google/android/gm/utils/LabelColorUtils;
.super Ljava/lang/Object;
.source "LabelColorUtils.java"


# static fields
.field public static ACCOUNT_CUSTOM_LABEL_COLORS:Ljava/util/concurrent/ConcurrentHashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/concurrent/ConcurrentHashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/util/concurrent/ConcurrentHashMap",
            "<",
            "Ljava/lang/String;",
            "[",
            "Ljava/lang/String;",
            ">;>;"
        }
    .end annotation
.end field

.field public static CUSTOM_BACKGROUND_COLOR:Ljava/lang/String;

.field public static CUSTOM_COLOR_INDEX:Ljava/lang/String;

.field public static CUSTOM_TEXT_COLOR:Ljava/lang/String;

.field private static final DEFAULT_COLORS:[Ljava/lang/String;

.field public static DEFAULT_COLOR_ID:I

.field private static final LABEL_COLORS:[[Ljava/lang/String;

.field private static final MUTED_COLORS:[I

.field private static final MUTED_COLOR_STRINGS:[Ljava/lang/String;

.field private static final NUM_COLORS:I


# direct methods
.method static constructor <clinit>()V
    .locals 7

    const/4 v6, 0x1

    const/4 v5, 0x0

    const/4 v4, 0x2

    const-string v0, "background_color"

    sput-object v0, Lcom/google/android/gm/utils/LabelColorUtils;->CUSTOM_BACKGROUND_COLOR:Ljava/lang/String;

    const-string v0, "color_index"

    sput-object v0, Lcom/google/android/gm/utils/LabelColorUtils;->CUSTOM_COLOR_INDEX:Ljava/lang/String;

    const-string v0, "text_color"

    sput-object v0, Lcom/google/android/gm/utils/LabelColorUtils;->CUSTOM_TEXT_COLOR:Ljava/lang/String;

    const/4 v0, -0x1

    sput v0, Lcom/google/android/gm/utils/LabelColorUtils;->DEFAULT_COLOR_ID:I

    new-instance v0, Ljava/util/concurrent/ConcurrentHashMap;

    invoke-direct {v0}, Ljava/util/concurrent/ConcurrentHashMap;-><init>()V

    sput-object v0, Lcom/google/android/gm/utils/LabelColorUtils;->ACCOUNT_CUSTOM_LABEL_COLORS:Ljava/util/concurrent/ConcurrentHashMap;

    new-array v0, v4, [Ljava/lang/String;

    const-string v1, "#eeeeee"

    aput-object v1, v0, v5

    const-string v1, "#888888"

    aput-object v1, v0, v6

    sput-object v0, Lcom/google/android/gm/utils/LabelColorUtils;->DEFAULT_COLORS:[Ljava/lang/String;

    const/16 v0, 0x19

    new-array v0, v0, [[Ljava/lang/String;

    new-array v1, v4, [Ljava/lang/String;

    const-string v2, "#f1f5ec"

    aput-object v2, v1, v5

    const-string v2, "#006633"

    aput-object v2, v1, v6

    aput-object v1, v0, v5

    new-array v1, v4, [Ljava/lang/String;

    const-string v2, "#dee5f2"

    aput-object v2, v1, v5

    const-string v2, "#5a6986"

    aput-object v2, v1, v6

    aput-object v1, v0, v6

    new-array v1, v4, [Ljava/lang/String;

    const-string v2, "#e0ecff"

    aput-object v2, v1, v5

    const-string v2, "#206cff"

    aput-object v2, v1, v6

    aput-object v1, v0, v4

    const/4 v1, 0x3

    new-array v2, v4, [Ljava/lang/String;

    const-string v3, "#dfe2ff"

    aput-object v3, v2, v5

    const-string v3, "#0000cc"

    aput-object v3, v2, v6

    aput-object v2, v0, v1

    const/4 v1, 0x4

    new-array v2, v4, [Ljava/lang/String;

    const-string v3, "#e0d5f9"

    aput-object v3, v2, v5

    const-string v3, "#5229a3"

    aput-object v3, v2, v6

    aput-object v2, v0, v1

    const/4 v1, 0x5

    new-array v2, v4, [Ljava/lang/String;

    const-string v3, "#fde9f4"

    aput-object v3, v2, v5

    const-string v3, "#854f61"

    aput-object v3, v2, v6

    aput-object v2, v0, v1

    const/4 v1, 0x6

    new-array v2, v4, [Ljava/lang/String;

    const-string v3, "#ffe3e3"

    aput-object v3, v2, v5

    const-string v3, "#cc0000"

    aput-object v3, v2, v6

    aput-object v2, v0, v1

    const/4 v1, 0x7

    new-array v2, v4, [Ljava/lang/String;

    const-string v3, "#fff0e1"

    aput-object v3, v2, v5

    const-string v3, "#ec7000"

    aput-object v3, v2, v6

    aput-object v2, v0, v1

    const/16 v1, 0x8

    new-array v2, v4, [Ljava/lang/String;

    const-string v3, "#fadcb3"

    aput-object v3, v2, v5

    const-string v3, "#b36d00"

    aput-object v3, v2, v6

    aput-object v2, v0, v1

    const/16 v1, 0x9

    new-array v2, v4, [Ljava/lang/String;

    const-string v3, "#f3e7b3"

    aput-object v3, v2, v5

    const-string v3, "#ab8b00"

    aput-object v3, v2, v6

    aput-object v2, v0, v1

    const/16 v1, 0xa

    new-array v2, v4, [Ljava/lang/String;

    const-string v3, "#ffffd4"

    aput-object v3, v2, v5

    const-string v3, "#636330"

    aput-object v3, v2, v6

    aput-object v2, v0, v1

    const/16 v1, 0xb

    new-array v2, v4, [Ljava/lang/String;

    const-string v3, "#f9ffef"

    aput-object v3, v2, v5

    const-string v3, "#64992c"

    aput-object v3, v2, v6

    aput-object v2, v0, v1

    const/16 v1, 0xc

    new-array v2, v4, [Ljava/lang/String;

    const-string v3, "#f1f5ec"

    aput-object v3, v2, v5

    const-string v3, "#006633"

    aput-object v3, v2, v6

    aput-object v2, v0, v1

    const/16 v1, 0xd

    new-array v2, v4, [Ljava/lang/String;

    const-string v3, "#5a6986"

    aput-object v3, v2, v5

    const-string v3, "#dee5f2"

    aput-object v3, v2, v6

    aput-object v2, v0, v1

    const/16 v1, 0xe

    new-array v2, v4, [Ljava/lang/String;

    const-string v3, "#206cff"

    aput-object v3, v2, v5

    const-string v3, "#e0ecff"

    aput-object v3, v2, v6

    aput-object v2, v0, v1

    const/16 v1, 0xf

    new-array v2, v4, [Ljava/lang/String;

    const-string v3, "#0000cc"

    aput-object v3, v2, v5

    const-string v3, "#dfe2ff"

    aput-object v3, v2, v6

    aput-object v2, v0, v1

    const/16 v1, 0x10

    new-array v2, v4, [Ljava/lang/String;

    const-string v3, "#5229a3"

    aput-object v3, v2, v5

    const-string v3, "#e0d5f9"

    aput-object v3, v2, v6

    aput-object v2, v0, v1

    const/16 v1, 0x11

    new-array v2, v4, [Ljava/lang/String;

    const-string v3, "#854f61"

    aput-object v3, v2, v5

    const-string v3, "#fde9f4"

    aput-object v3, v2, v6

    aput-object v2, v0, v1

    const/16 v1, 0x12

    new-array v2, v4, [Ljava/lang/String;

    const-string v3, "#cc0000"

    aput-object v3, v2, v5

    const-string v3, "#ffe3e3"

    aput-object v3, v2, v6

    aput-object v2, v0, v1

    const/16 v1, 0x13

    new-array v2, v4, [Ljava/lang/String;

    const-string v3, "#ec7000"

    aput-object v3, v2, v5

    const-string v3, "#fff0e1"

    aput-object v3, v2, v6

    aput-object v2, v0, v1

    const/16 v1, 0x14

    new-array v2, v4, [Ljava/lang/String;

    const-string v3, "#b36d00"

    aput-object v3, v2, v5

    const-string v3, "#fadcb3"

    aput-object v3, v2, v6

    aput-object v2, v0, v1

    const/16 v1, 0x15

    new-array v2, v4, [Ljava/lang/String;

    const-string v3, "#ab8b00"

    aput-object v3, v2, v5

    const-string v3, "#f3e7b3"

    aput-object v3, v2, v6

    aput-object v2, v0, v1

    const/16 v1, 0x16

    new-array v2, v4, [Ljava/lang/String;

    const-string v3, "#636330"

    aput-object v3, v2, v5

    const-string v3, "#ffffd4"

    aput-object v3, v2, v6

    aput-object v2, v0, v1

    const/16 v1, 0x17

    new-array v2, v4, [Ljava/lang/String;

    const-string v3, "#64992c"

    aput-object v3, v2, v5

    const-string v3, "#f9ffef"

    aput-object v3, v2, v6

    aput-object v2, v0, v1

    const/16 v1, 0x18

    new-array v2, v4, [Ljava/lang/String;

    const-string v3, "#006633"

    aput-object v3, v2, v5

    const-string v3, "#f1f5ec"

    aput-object v3, v2, v6

    aput-object v2, v0, v1

    sput-object v0, Lcom/google/android/gm/utils/LabelColorUtils;->LABEL_COLORS:[[Ljava/lang/String;

    new-array v0, v4, [I

    fill-array-data v0, :array_0

    sput-object v0, Lcom/google/android/gm/utils/LabelColorUtils;->MUTED_COLORS:[I

    new-array v0, v4, [Ljava/lang/String;

    const-string v1, "#ff006633"

    aput-object v1, v0, v5

    const-string v1, "#ff006633"

    aput-object v1, v0, v6

    sput-object v0, Lcom/google/android/gm/utils/LabelColorUtils;->MUTED_COLOR_STRINGS:[Ljava/lang/String;

    sget-object v0, Lcom/google/android/gm/utils/LabelColorUtils;->LABEL_COLORS:[[Ljava/lang/String;

    array-length v0, v0

    sput v0, Lcom/google/android/gm/utils/LabelColorUtils;->NUM_COLORS:I

    return-void

    nop

    :array_0
    .array-data 4
        -0xff99cd
        -0xff99cd
    .end array-data
.end method

.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static addOrUpdateCustomLabelColor(Ljava/lang/String;Ljava/lang/String;Lcom/google/android/gm/provider/MailStore$CustomLabelColorPreference;)V
    .locals 4
    .param p0    # Ljava/lang/String;
    .param p1    # Ljava/lang/String;
    .param p2    # Lcom/google/android/gm/provider/MailStore$CustomLabelColorPreference;

    sget-object v1, Lcom/google/android/gm/utils/LabelColorUtils;->ACCOUNT_CUSTOM_LABEL_COLORS:Ljava/util/concurrent/ConcurrentHashMap;

    invoke-virtual {v1, p0}, Ljava/util/concurrent/ConcurrentHashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/concurrent/ConcurrentHashMap;

    if-nez v0, :cond_0

    new-instance v0, Ljava/util/concurrent/ConcurrentHashMap;

    invoke-direct {v0}, Ljava/util/concurrent/ConcurrentHashMap;-><init>()V

    sget-object v1, Lcom/google/android/gm/utils/LabelColorUtils;->ACCOUNT_CUSTOM_LABEL_COLORS:Ljava/util/concurrent/ConcurrentHashMap;

    invoke-virtual {v1, p0, v0}, Ljava/util/concurrent/ConcurrentHashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    :cond_0
    const/4 v1, 0x2

    new-array v1, v1, [Ljava/lang/String;

    const/4 v2, 0x0

    iget-object v3, p2, Lcom/google/android/gm/provider/MailStore$CustomLabelColorPreference;->backgroundColor:Ljava/lang/String;

    aput-object v3, v1, v2

    const/4 v2, 0x1

    iget-object v3, p2, Lcom/google/android/gm/provider/MailStore$CustomLabelColorPreference;->textColor:Ljava/lang/String;

    aput-object v3, v1, v2

    invoke-virtual {v0, p1, v1}, Ljava/util/concurrent/ConcurrentHashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    return-void
.end method

.method public static getDefaultLabelBackgroundColor()I
    .locals 2

    const/4 v1, 0x0

    sget-object v0, Lcom/google/android/gm/utils/LabelColorUtils;->LABEL_COLORS:[[Ljava/lang/String;

    aget-object v0, v0, v1

    aget-object v0, v0, v1

    invoke-static {v0}, Landroid/graphics/Color;->parseColor(Ljava/lang/String;)I

    move-result v0

    return v0
.end method

.method public static getLabelColorInts(ILjava/lang/String;)[I
    .locals 5
    .param p0    # I
    .param p1    # Ljava/lang/String;

    const/4 v4, 0x1

    const/4 v3, 0x0

    const/4 v2, 0x2

    new-array v0, v2, [I

    invoke-static {p0, p1}, Lcom/google/android/gm/utils/LabelColorUtils;->getLabelColorStrings(ILjava/lang/String;)[Ljava/lang/String;

    move-result-object v1

    aget-object v2, v1, v3

    invoke-static {v2}, Landroid/graphics/Color;->parseColor(Ljava/lang/String;)I

    move-result v2

    aput v2, v0, v3

    aget-object v2, v1, v4

    invoke-static {v2}, Landroid/graphics/Color;->parseColor(Ljava/lang/String;)I

    move-result v2

    aput v2, v0, v4

    return-object v0
.end method

.method public static getLabelColorInts(Ljava/lang/String;Ljava/lang/String;)[I
    .locals 3
    .param p0    # Ljava/lang/String;
    .param p1    # Ljava/lang/String;

    :try_start_0
    invoke-static {p0}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v1

    invoke-static {v1, p1}, Lcom/google/android/gm/utils/LabelColorUtils;->getLabelColorInts(ILjava/lang/String;)[I
    :try_end_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v2

    :goto_0
    return-object v2

    :catch_0
    move-exception v0

    const/4 v2, 0x0

    goto :goto_0
.end method

.method public static getLabelColorStrings(ILjava/lang/String;)[Ljava/lang/String;
    .locals 4
    .param p0    # I
    .param p1    # Ljava/lang/String;

    sget v2, Lcom/google/android/gm/utils/LabelColorUtils;->DEFAULT_COLOR_ID:I

    if-ne p0, v2, :cond_1

    sget-object v0, Lcom/google/android/gm/utils/LabelColorUtils;->DEFAULT_COLORS:[Ljava/lang/String;

    :cond_0
    :goto_0
    return-object v0

    :cond_1
    if-gez p0, :cond_3

    sget-object v2, Lcom/google/android/gm/utils/LabelColorUtils;->ACCOUNT_CUSTOM_LABEL_COLORS:Ljava/util/concurrent/ConcurrentHashMap;

    invoke-virtual {v2, p1}, Ljava/util/concurrent/ConcurrentHashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/Map;

    if-eqz v1, :cond_2

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, p0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ""

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, [Ljava/lang/String;

    move-object v0, v2

    :goto_1
    if-nez v0, :cond_0

    sget-object v0, Lcom/google/android/gm/utils/LabelColorUtils;->DEFAULT_COLORS:[Ljava/lang/String;

    goto :goto_0

    :cond_2
    sget-object v0, Lcom/google/android/gm/utils/LabelColorUtils;->DEFAULT_COLORS:[Ljava/lang/String;

    goto :goto_1

    :cond_3
    sget v2, Lcom/google/android/gm/utils/LabelColorUtils;->NUM_COLORS:I

    if-lt p0, v2, :cond_4

    sget-object v0, Lcom/google/android/gm/utils/LabelColorUtils;->DEFAULT_COLORS:[Ljava/lang/String;

    goto :goto_0

    :cond_4
    sget-object v2, Lcom/google/android/gm/utils/LabelColorUtils;->LABEL_COLORS:[[Ljava/lang/String;

    aget-object v0, v2, p0

    goto :goto_0
.end method

.method public static getLabelColorStrings(Ljava/lang/String;Ljava/lang/String;)[Ljava/lang/String;
    .locals 6
    .param p0    # Ljava/lang/String;
    .param p1    # Ljava/lang/String;

    :try_start_0
    invoke-static {p0}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v1

    invoke-static {v1, p1}, Lcom/google/android/gm/utils/LabelColorUtils;->getLabelColorStrings(ILjava/lang/String;)[Ljava/lang/String;
    :try_end_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v2

    :goto_0
    return-object v2

    :catch_0
    move-exception v0

    const-string v2, "Gmail"

    const-string v3, "Invalid labelColorId String: %s"

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    aput-object p0, v4, v5

    invoke-static {v2, v3, v4}, Lcom/google/android/gm/provider/LogUtils;->i(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)I

    sget-object v2, Lcom/google/android/gm/utils/LabelColorUtils;->DEFAULT_COLORS:[Ljava/lang/String;

    goto :goto_0
.end method

.method public static getMutedColorInts()[I
    .locals 1

    sget-object v0, Lcom/google/android/gm/utils/LabelColorUtils;->MUTED_COLORS:[I

    return-object v0
.end method

.method public static instantiateCustomLabelColors(Ljava/lang/String;Landroid/database/Cursor;)V
    .locals 8
    .param p0    # Ljava/lang/String;
    .param p1    # Landroid/database/Cursor;

    invoke-interface {p1}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v4

    if-eqz v4, :cond_1

    new-instance v1, Ljava/util/concurrent/ConcurrentHashMap;

    invoke-direct {v1}, Ljava/util/concurrent/ConcurrentHashMap;-><init>()V

    sget-object v4, Lcom/google/android/gm/utils/LabelColorUtils;->CUSTOM_COLOR_INDEX:Ljava/lang/String;

    invoke-interface {p1, v4}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v2

    sget-object v4, Lcom/google/android/gm/utils/LabelColorUtils;->CUSTOM_BACKGROUND_COLOR:Ljava/lang/String;

    invoke-interface {p1, v4}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v0

    sget-object v4, Lcom/google/android/gm/utils/LabelColorUtils;->CUSTOM_TEXT_COLOR:Ljava/lang/String;

    invoke-interface {p1, v4}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v3

    :cond_0
    invoke-interface {p1, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v4

    const/4 v5, 0x2

    new-array v5, v5, [Ljava/lang/String;

    const/4 v6, 0x0

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v7

    aput-object v7, v5, v6

    const/4 v6, 0x1

    invoke-interface {p1, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v7

    aput-object v7, v5, v6

    invoke-virtual {v1, v4, v5}, Ljava/util/concurrent/ConcurrentHashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    invoke-interface {p1}, Landroid/database/Cursor;->moveToNext()Z

    move-result v4

    if-nez v4, :cond_0

    sget-object v4, Lcom/google/android/gm/utils/LabelColorUtils;->ACCOUNT_CUSTOM_LABEL_COLORS:Ljava/util/concurrent/ConcurrentHashMap;

    invoke-virtual {v4, p0, v1}, Ljava/util/concurrent/ConcurrentHashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    :cond_1
    invoke-interface {p1}, Landroid/database/Cursor;->close()V

    return-void
.end method
