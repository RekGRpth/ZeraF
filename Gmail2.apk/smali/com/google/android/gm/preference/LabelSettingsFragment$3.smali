.class Lcom/google/android/gm/preference/LabelSettingsFragment$3;
.super Ljava/lang/Object;
.source "LabelSettingsFragment.java"

# interfaces
.implements Landroid/content/DialogInterface$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/gm/preference/LabelSettingsFragment;->promptEnableAccountNotifications()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/gm/preference/LabelSettingsFragment;


# direct methods
.method constructor <init>(Lcom/google/android/gm/preference/LabelSettingsFragment;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/gm/preference/LabelSettingsFragment$3;->this$0:Lcom/google/android/gm/preference/LabelSettingsFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/content/DialogInterface;I)V
    .locals 6
    .param p1    # Landroid/content/DialogInterface;
    .param p2    # I

    const/4 v5, 0x1

    invoke-static {}, Lcom/google/android/gm/persistence/Persistence;->getInstance()Lcom/google/android/gm/persistence/Persistence;

    move-result-object v2

    iget-object v3, p0, Lcom/google/android/gm/preference/LabelSettingsFragment$3;->this$0:Lcom/google/android/gm/preference/LabelSettingsFragment;

    invoke-virtual {v3}, Lcom/google/android/gm/preference/LabelSettingsFragment;->getActivity()Landroid/app/Activity;

    move-result-object v3

    iget-object v4, p0, Lcom/google/android/gm/preference/LabelSettingsFragment$3;->this$0:Lcom/google/android/gm/preference/LabelSettingsFragment;

    # getter for: Lcom/google/android/gm/preference/LabelSettingsFragment;->mAccount:Ljava/lang/String;
    invoke-static {v4}, Lcom/google/android/gm/preference/LabelSettingsFragment;->access$300(Lcom/google/android/gm/preference/LabelSettingsFragment;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v3, v4, v5}, Lcom/google/android/gm/persistence/Persistence;->setEnableNotifications(Landroid/content/Context;Ljava/lang/String;Z)V

    iget-object v2, p0, Lcom/google/android/gm/preference/LabelSettingsFragment$3;->this$0:Lcom/google/android/gm/preference/LabelSettingsFragment;

    # setter for: Lcom/google/android/gm/preference/LabelSettingsFragment;->mDoesAccountNotify:Z
    invoke-static {v2, v5}, Lcom/google/android/gm/preference/LabelSettingsFragment;->access$402(Lcom/google/android/gm/preference/LabelSettingsFragment;Z)Z

    iget-object v2, p0, Lcom/google/android/gm/preference/LabelSettingsFragment$3;->this$0:Lcom/google/android/gm/preference/LabelSettingsFragment;

    # setter for: Lcom/google/android/gm/preference/LabelSettingsFragment;->mDoesLabelNotify:Z
    invoke-static {v2, v5}, Lcom/google/android/gm/preference/LabelSettingsFragment;->access$502(Lcom/google/android/gm/preference/LabelSettingsFragment;Z)Z

    iget-object v2, p0, Lcom/google/android/gm/preference/LabelSettingsFragment$3;->this$0:Lcom/google/android/gm/preference/LabelSettingsFragment;

    # getter for: Lcom/google/android/gm/preference/LabelSettingsFragment;->mAttributeList:Ljava/util/List;
    invoke-static {v2}, Lcom/google/android/gm/preference/LabelSettingsFragment;->access$600(Lcom/google/android/gm/preference/LabelSettingsFragment;)Ljava/util/List;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :cond_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/gm/preference/LabelSettingsFragment$Pair;

    iget v2, v1, Lcom/google/android/gm/preference/LabelSettingsFragment$Pair;->key:I

    sget v3, Lcom/google/android/gm/persistence/Persistence;->LABEL_NOTIFICATION_ON:I

    if-ne v2, v3, :cond_0

    iget-object v2, p0, Lcom/google/android/gm/preference/LabelSettingsFragment$3;->this$0:Lcom/google/android/gm/preference/LabelSettingsFragment;

    # getter for: Lcom/google/android/gm/preference/LabelSettingsFragment;->mDoesLabelNotify:Z
    invoke-static {v2}, Lcom/google/android/gm/preference/LabelSettingsFragment;->access$500(Lcom/google/android/gm/preference/LabelSettingsFragment;)Z

    move-result v2

    invoke-static {v2}, Ljava/lang/Boolean;->toString(Z)Ljava/lang/String;

    move-result-object v2

    iput-object v2, v1, Lcom/google/android/gm/preference/LabelSettingsFragment$Pair;->value:Ljava/lang/String;

    :cond_1
    iget-object v2, p0, Lcom/google/android/gm/preference/LabelSettingsFragment$3;->this$0:Lcom/google/android/gm/preference/LabelSettingsFragment;

    # invokes: Lcom/google/android/gm/preference/LabelSettingsFragment;->saveSettings()V
    invoke-static {v2}, Lcom/google/android/gm/preference/LabelSettingsFragment;->access$700(Lcom/google/android/gm/preference/LabelSettingsFragment;)V

    iget-object v2, p0, Lcom/google/android/gm/preference/LabelSettingsFragment$3;->this$0:Lcom/google/android/gm/preference/LabelSettingsFragment;

    # invokes: Lcom/google/android/gm/preference/LabelSettingsFragment;->refreshPreferences()V
    invoke-static {v2}, Lcom/google/android/gm/preference/LabelSettingsFragment;->access$100(Lcom/google/android/gm/preference/LabelSettingsFragment;)V

    return-void
.end method
