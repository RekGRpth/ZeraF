.class public Lcom/google/android/gm/preference/GmailPreferenceActivity;
.super Landroid/preference/PreferenceActivity;
.source "GmailPreferenceActivity.java"

# interfaces
.implements Landroid/content/SharedPreferences$OnSharedPreferenceChangeListener;
.implements Lcom/google/android/gm/AccountHelper$AddAccountCallback;
.implements Lcom/google/android/gm/ApplicationMenuHandler$HelpCallback;


# static fields
.field private static sCreatedAccount:Z


# instance fields
.field private mAccounts:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private mRestartAccountQuery:Z

.field private mSynced:Z


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const/4 v0, 0x0

    sput-boolean v0, Lcom/google/android/gm/preference/GmailPreferenceActivity;->sCreatedAccount:Z

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    const/4 v0, 0x0

    invoke-direct {p0}, Landroid/preference/PreferenceActivity;-><init>()V

    iput-boolean v0, p0, Lcom/google/android/gm/preference/GmailPreferenceActivity;->mSynced:Z

    iput-boolean v0, p0, Lcom/google/android/gm/preference/GmailPreferenceActivity;->mRestartAccountQuery:Z

    return-void
.end method

.method static synthetic access$000(Lcom/google/android/gm/preference/GmailPreferenceActivity;)Ljava/util/List;
    .locals 1
    .param p0    # Lcom/google/android/gm/preference/GmailPreferenceActivity;

    iget-object v0, p0, Lcom/google/android/gm/preference/GmailPreferenceActivity;->mAccounts:Ljava/util/List;

    return-object v0
.end method

.method static synthetic access$002(Lcom/google/android/gm/preference/GmailPreferenceActivity;Ljava/util/List;)Ljava/util/List;
    .locals 0
    .param p0    # Lcom/google/android/gm/preference/GmailPreferenceActivity;
    .param p1    # Ljava/util/List;

    iput-object p1, p0, Lcom/google/android/gm/preference/GmailPreferenceActivity;->mAccounts:Ljava/util/List;

    return-object p1
.end method

.method private addAccountHeaders(Ljava/util/List;)V
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Landroid/preference/PreferenceActivity$Header;",
            ">;)V"
        }
    .end annotation

    const/4 v6, 0x1

    invoke-static {}, Lcom/google/android/gm/persistence/Persistence;->getInstance()Lcom/google/android/gm/persistence/Persistence;

    move-result-object v4

    const/4 v5, 0x0

    invoke-virtual {v4, p0, v5}, Lcom/google/android/gm/persistence/Persistence;->getCachedConfiguredGoogleAccounts(Landroid/content/Context;Z)Ljava/util/List;

    move-result-object v4

    iput-object v4, p0, Lcom/google/android/gm/preference/GmailPreferenceActivity;->mAccounts:Ljava/util/List;

    iget-boolean v4, p0, Lcom/google/android/gm/preference/GmailPreferenceActivity;->mSynced:Z

    if-nez v4, :cond_1

    invoke-direct {p0}, Lcom/google/android/gm/preference/GmailPreferenceActivity;->asyncInitAccounts()V

    iput-boolean v6, p0, Lcom/google/android/gm/preference/GmailPreferenceActivity;->mSynced:Z

    :cond_0
    return-void

    :cond_1
    iget-object v4, p0, Lcom/google/android/gm/preference/GmailPreferenceActivity;->mAccounts:Ljava/util/List;

    invoke-interface {v4}, Ljava/util/List;->size()I

    move-result v4

    add-int/lit8 v3, v4, -0x1

    :goto_0
    if-ltz v3, :cond_0

    iget-object v4, p0, Lcom/google/android/gm/preference/GmailPreferenceActivity;->mAccounts:Ljava/util/List;

    invoke-interface {v4, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    new-instance v2, Landroid/preference/PreferenceActivity$Header;

    invoke-direct {v2}, Landroid/preference/PreferenceActivity$Header;-><init>()V

    iput-object v0, v2, Landroid/preference/PreferenceActivity$Header;->title:Ljava/lang/CharSequence;

    const-string v4, "com.google.android.gm.preference.AccountPreferenceFragment"

    iput-object v4, v2, Landroid/preference/PreferenceActivity$Header;->fragment:Ljava/lang/String;

    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    const-string v4, "account"

    invoke-virtual {v1, v4, v0}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    iput-object v1, v2, Landroid/preference/PreferenceActivity$Header;->fragmentArguments:Landroid/os/Bundle;

    invoke-interface {p1, v6, v2}, Ljava/util/List;->add(ILjava/lang/Object;)V

    add-int/lit8 v3, v3, -0x1

    goto :goto_0
.end method

.method private asyncInitAccounts()V
    .locals 1

    new-instance v0, Lcom/google/android/gm/preference/GmailPreferenceActivity$1;

    invoke-direct {v0, p0}, Lcom/google/android/gm/preference/GmailPreferenceActivity$1;-><init>(Lcom/google/android/gm/preference/GmailPreferenceActivity;)V

    invoke-static {p0, v0}, Lcom/google/android/gm/AccountHelper;->getSyncingAccounts(Landroid/content/Context;Landroid/accounts/AccountManagerCallback;)V

    return-void
.end method

.method private final getInitialHeader(JLjava/util/List;)Landroid/preference/PreferenceActivity$Header;
    .locals 4
    .param p1    # J
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(J",
            "Ljava/util/List",
            "<",
            "Landroid/preference/PreferenceActivity$Header;",
            ">;)",
            "Landroid/preference/PreferenceActivity$Header;"
        }
    .end annotation

    invoke-interface {p3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/preference/PreferenceActivity$Header;

    iget-wide v2, v0, Landroid/preference/PreferenceActivity$Header;->id:J

    cmp-long v2, v2, p1

    if-nez v2, :cond_0

    iget-object v2, v0, Landroid/preference/PreferenceActivity$Header;->fragment:Ljava/lang/String;

    if-eqz v2, :cond_0

    :goto_0
    return-object v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private launchLabelSettings(Landroid/content/Intent;)V
    .locals 4

    const-string v0, "extra_folder"

    invoke-virtual {p1, v0}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/android/mail/providers/Folder;

    const-string v1, "extra_account"

    invoke-virtual {p1, v1}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v1

    check-cast v1, Lcom/android/mail/providers/Account;

    iget-object v1, v1, Lcom/android/mail/providers/Account;->name:Ljava/lang/String;

    iget-object v0, v0, Lcom/android/mail/providers/Folder;->uri:Landroid/net/Uri;

    invoke-virtual {v0}, Landroid/net/Uri;->getLastPathSegment()Ljava/lang/String;

    move-result-object v0

    new-instance v2, Landroid/content/Intent;

    const-class v3, Lcom/google/android/gm/LabelsActivity;

    invoke-direct {v2, p0, v3}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const-string v3, "account_key"

    invoke-virtual {v2, v3, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const-string v1, "label"

    invoke-virtual {v2, v1, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    invoke-virtual {p0, v2}, Lcom/google/android/gm/preference/GmailPreferenceActivity;->startActivity(Landroid/content/Intent;)V

    invoke-virtual {p0}, Lcom/google/android/gm/preference/GmailPreferenceActivity;->finish()V

    return-void
.end method

.method private launchManageLabels(Landroid/content/Intent;)V
    .locals 3

    const-string v0, "extra_account"

    invoke-virtual {p1, v0}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/android/mail/providers/Account;

    iget-object v0, v0, Lcom/android/mail/providers/Account;->name:Ljava/lang/String;

    new-instance v1, Landroid/content/Intent;

    const-class v2, Lcom/google/android/gm/LabelsActivity;

    invoke-direct {v1, p0, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const-string v2, "account_key"

    invoke-virtual {v1, v2, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    invoke-virtual {p0, v1}, Lcom/google/android/gm/preference/GmailPreferenceActivity;->startActivity(Landroid/content/Intent;)V

    invoke-virtual {p0}, Lcom/google/android/gm/preference/GmailPreferenceActivity;->finish()V

    return-void
.end method

.method private loadHeaders(Ljava/util/List;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Landroid/preference/PreferenceActivity$Header;",
            ">;)V"
        }
    .end annotation

    const v0, 0x7f060006

    invoke-virtual {p0, v0, p1}, Lcom/google/android/gm/preference/GmailPreferenceActivity;->loadHeadersFromResource(ILjava/util/List;)V

    invoke-direct {p0, p1}, Lcom/google/android/gm/preference/GmailPreferenceActivity;->addAccountHeaders(Ljava/util/List;)V

    return-void
.end method

.method private loadInitialHeader(J)V
    .locals 7
    .param p1    # J

    invoke-static {}, Lcom/google/common/collect/Lists;->newArrayList()Ljava/util/ArrayList;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/google/android/gm/preference/GmailPreferenceActivity;->loadHeaders(Ljava/util/List;)V

    invoke-direct {p0, p1, p2, v0}, Lcom/google/android/gm/preference/GmailPreferenceActivity;->getInitialHeader(JLjava/util/List;)Landroid/preference/PreferenceActivity$Header;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-virtual {p0}, Lcom/google/android/gm/preference/GmailPreferenceActivity;->isMultiPane()Z

    move-result v3

    if-nez v3, :cond_1

    iget-object v3, v1, Landroid/preference/PreferenceActivity$Header;->fragment:Ljava/lang/String;

    new-instance v4, Landroid/os/Bundle;

    invoke-direct {v4}, Landroid/os/Bundle;-><init>()V

    iget v5, v1, Landroid/preference/PreferenceActivity$Header;->titleRes:I

    const/4 v6, 0x0

    invoke-virtual {p0, v3, v4, v5, v6}, Lcom/google/android/gm/preference/GmailPreferenceActivity;->onBuildStartFragmentIntent(Ljava/lang/String;Landroid/os/Bundle;II)Landroid/content/Intent;

    move-result-object v2

    invoke-virtual {p0, v2}, Lcom/google/android/gm/preference/GmailPreferenceActivity;->startActivity(Landroid/content/Intent;)V

    invoke-virtual {p0}, Lcom/google/android/gm/preference/GmailPreferenceActivity;->finish()V

    :cond_0
    :goto_0
    return-void

    :cond_1
    invoke-virtual {p0, v1}, Lcom/google/android/gm/preference/GmailPreferenceActivity;->switchToHeader(Landroid/preference/PreferenceActivity$Header;)V

    goto :goto_0
.end method


# virtual methods
.method public getHelpContext()Ljava/lang/String;
    .locals 1

    const-string v0, "gm_settings"

    return-object v0
.end method

.method public onBuildHeaders(Ljava/util/List;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Landroid/preference/PreferenceActivity$Header;",
            ">;)V"
        }
    .end annotation

    invoke-direct {p0, p1}, Lcom/google/android/gm/preference/GmailPreferenceActivity;->loadHeaders(Ljava/util/List;)V

    return-void
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 15
    .param p1    # Landroid/os/Bundle;

    invoke-super/range {p0 .. p1}, Landroid/preference/PreferenceActivity;->onCreate(Landroid/os/Bundle;)V

    invoke-virtual {p0}, Lcom/google/android/gm/preference/GmailPreferenceActivity;->getIntent()Landroid/content/Intent;

    move-result-object v5

    invoke-virtual {v5}, Landroid/content/Intent;->getData()Landroid/net/Uri;

    move-result-object v1

    const-wide/16 v3, -0x1

    const-string v12, "initial_fragment_id"

    invoke-virtual {v5, v12}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    move-result v12

    if-eqz v12, :cond_5

    const-string v12, "initial_fragment_id"

    const-wide/16 v13, -0x1

    invoke-virtual {v5, v12, v13, v14}, Landroid/content/Intent;->getLongExtra(Ljava/lang/String;J)J

    move-result-wide v3

    const-string v12, "initial_fragment_id"

    invoke-virtual {v5, v12}, Landroid/content/Intent;->removeExtra(Ljava/lang/String;)V

    :cond_0
    :goto_0
    const-wide/16 v12, -0x1

    cmp-long v12, v3, v12

    if-eqz v12, :cond_3

    const/4 v6, 0x1

    const-wide/32 v12, 0x7f080110

    cmp-long v12, v3, v12

    if-nez v12, :cond_2

    const-string v12, "reporting_problem"

    const/4 v13, 0x0

    invoke-virtual {v5, v12, v13}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v8

    if-eqz v1, :cond_8

    const-string v12, "reporting_problem"

    invoke-virtual {v1, v12}, Landroid/net/Uri;->getQueryParameter(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v10

    :goto_1
    if-eqz v10, :cond_9

    invoke-static {v10}, Ljava/lang/Boolean;->parseBoolean(Ljava/lang/String;)Z

    move-result v12

    if-eqz v12, :cond_9

    const/4 v9, 0x1

    :goto_2
    if-nez v8, :cond_1

    if-eqz v9, :cond_2

    :cond_1
    const-string v12, "screen_shot"

    invoke-virtual {v5, v12}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v11

    check-cast v11, Landroid/graphics/Bitmap;

    invoke-static {p0, v11}, Lcom/google/android/gm/Utils;->launchGoogleFeedback(Landroid/content/Context;Landroid/graphics/Bitmap;)V

    const/4 v6, 0x0

    invoke-virtual {p0}, Lcom/google/android/gm/preference/GmailPreferenceActivity;->finish()V

    :cond_2
    if-eqz v6, :cond_3

    invoke-direct {p0, v3, v4}, Lcom/google/android/gm/preference/GmailPreferenceActivity;->loadInitialHeader(J)V

    :cond_3
    invoke-virtual {p0}, Lcom/google/android/gm/preference/GmailPreferenceActivity;->getActionBar()Landroid/app/ActionBar;

    move-result-object v0

    if-eqz v0, :cond_4

    const/4 v12, 0x4

    const/4 v13, 0x4

    invoke-virtual {v0, v12, v13}, Landroid/app/ActionBar;->setDisplayOptions(II)V

    :cond_4
    invoke-static {p0}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v7

    invoke-interface {v7, p0}, Landroid/content/SharedPreferences;->registerOnSharedPreferenceChangeListener(Landroid/content/SharedPreferences$OnSharedPreferenceChangeListener;)V

    :goto_3
    return-void

    :cond_5
    const-string v12, "extra_folder"

    invoke-virtual {v5, v12}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    move-result v12

    if-eqz v12, :cond_6

    invoke-direct {p0, v5}, Lcom/google/android/gm/preference/GmailPreferenceActivity;->launchLabelSettings(Landroid/content/Intent;)V

    goto :goto_3

    :cond_6
    const-string v12, "extra_manage_folders"

    invoke-virtual {v5, v12}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    move-result v12

    if-eqz v12, :cond_7

    invoke-direct {p0, v5}, Lcom/google/android/gm/preference/GmailPreferenceActivity;->launchManageLabels(Landroid/content/Intent;)V

    goto :goto_0

    :cond_7
    if-eqz v1, :cond_0

    const-string v12, "preference_fragment_id"

    invoke-virtual {v1, v12}, Landroid/net/Uri;->getQueryParameter(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    if-eqz v2, :cond_0

    invoke-static {v2}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v3

    goto :goto_0

    :cond_8
    const/4 v10, 0x0

    goto :goto_1

    :cond_9
    const/4 v9, 0x0

    goto :goto_2
.end method

.method public onCreateOptionsMenu(Landroid/view/Menu;)Z
    .locals 2
    .param p1    # Landroid/view/Menu;

    invoke-virtual {p0}, Lcom/google/android/gm/preference/GmailPreferenceActivity;->getMenuInflater()Landroid/view/MenuInflater;

    move-result-object v0

    const v1, 0x7f11000d

    invoke-virtual {v0, v1, p1}, Landroid/view/MenuInflater;->inflate(ILandroid/view/Menu;)V

    const/4 v1, 0x1

    return v1
.end method

.method public onOptionsItemSelected(Landroid/view/MenuItem;)Z
    .locals 2
    .param p1    # Landroid/view/MenuItem;

    const/4 v0, 0x1

    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v1

    sparse-switch v1, :sswitch_data_0

    invoke-static {p1, p0, p0}, Lcom/google/android/gm/ApplicationMenuHandler;->handleApplicationMenu(Landroid/view/MenuItem;Landroid/content/Context;Lcom/google/android/gm/ApplicationMenuHandler$HelpCallback;)Z

    move-result v0

    :goto_0
    return v0

    :sswitch_0
    invoke-static {p0, p0}, Lcom/google/android/gm/AccountHelper;->showAddAccount(Landroid/app/Activity;Lcom/google/android/gm/AccountHelper$AddAccountCallback;)V

    goto :goto_0

    :sswitch_1
    invoke-virtual {p0}, Lcom/google/android/gm/preference/GmailPreferenceActivity;->finish()V

    goto :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x102002c -> :sswitch_1
        0x7f08012c -> :sswitch_0
    .end sparse-switch
.end method

.method public onPause()V
    .locals 1

    invoke-super {p0}, Landroid/preference/PreferenceActivity;->onPause()V

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/gm/preference/GmailPreferenceActivity;->mRestartAccountQuery:Z

    return-void
.end method

.method public onResult(Landroid/accounts/Account;)V
    .locals 1
    .param p1    # Landroid/accounts/Account;

    if-eqz p1, :cond_0

    const/4 v0, 0x1

    sput-boolean v0, Lcom/google/android/gm/preference/GmailPreferenceActivity;->sCreatedAccount:Z

    :cond_0
    return-void
.end method

.method public onResume()V
    .locals 3

    const/4 v1, 0x0

    invoke-super {p0}, Landroid/preference/PreferenceActivity;->onResume()V

    sget-boolean v2, Lcom/google/android/gm/preference/GmailPreferenceActivity;->sCreatedAccount:Z

    if-nez v2, :cond_0

    iget-boolean v2, p0, Lcom/google/android/gm/preference/GmailPreferenceActivity;->mRestartAccountQuery:Z

    if-eqz v2, :cond_2

    :cond_0
    const/4 v0, 0x1

    :goto_0
    sput-boolean v1, Lcom/google/android/gm/preference/GmailPreferenceActivity;->sCreatedAccount:Z

    if-eqz v0, :cond_1

    invoke-direct {p0}, Lcom/google/android/gm/preference/GmailPreferenceActivity;->asyncInitAccounts()V

    :cond_1
    return-void

    :cond_2
    move v0, v1

    goto :goto_0
.end method

.method public onSharedPreferenceChanged(Landroid/content/SharedPreferences;Ljava/lang/String;)V
    .locals 0
    .param p1    # Landroid/content/SharedPreferences;
    .param p2    # Ljava/lang/String;

    invoke-static {p0}, Lcom/google/android/gm/provider/UiProvider;->notifyAccountListChanged(Landroid/content/Context;)V

    return-void
.end method

.method public onStop()V
    .locals 1

    invoke-super {p0}, Landroid/preference/PreferenceActivity;->onStop()V

    invoke-static {p0}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v0

    invoke-interface {v0, p0}, Landroid/content/SharedPreferences;->unregisterOnSharedPreferenceChangeListener(Landroid/content/SharedPreferences$OnSharedPreferenceChangeListener;)V

    return-void
.end method
