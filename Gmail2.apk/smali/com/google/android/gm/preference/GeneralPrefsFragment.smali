.class public final Lcom/google/android/gm/preference/GeneralPrefsFragment;
.super Lcom/google/android/gm/preference/GmailPreferenceFragment;
.source "GeneralPrefsFragment.java"

# interfaces
.implements Landroid/content/DialogInterface$OnClickListener;


# instance fields
.field private final mCalledFromTest:Z

.field private mClearDisplayImagesDialog:Landroid/app/AlertDialog;

.field private mClearSearchHistoryDialog:Landroid/app/AlertDialog;


# direct methods
.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Lcom/google/android/gm/preference/GmailPreferenceFragment;-><init>()V

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/gm/preference/GeneralPrefsFragment;->mCalledFromTest:Z

    return-void
.end method

.method constructor <init>(Z)V
    .locals 0
    .param p1    # Z

    invoke-direct {p0}, Lcom/google/android/gm/preference/GmailPreferenceFragment;-><init>()V

    iput-boolean p1, p0, Lcom/google/android/gm/preference/GeneralPrefsFragment;->mCalledFromTest:Z

    return-void
.end method

.method private clearDisplayImages()V
    .locals 2

    new-instance v0, Landroid/app/AlertDialog$Builder;

    invoke-virtual {p0}, Lcom/google/android/gm/preference/GeneralPrefsFragment;->getActivity()Landroid/app/Activity;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    const v1, 0x7f090181

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    const v1, 0x7f090182

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setMessage(I)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    const v1, 0x1010355

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setIconAttribute(I)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    const v1, 0x7f090161

    invoke-virtual {v0, v1, p0}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    const v1, 0x7f0900e1

    invoke-virtual {v0, v1, p0}, Landroid/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->show()Landroid/app/AlertDialog;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gm/preference/GeneralPrefsFragment;->mClearDisplayImagesDialog:Landroid/app/AlertDialog;

    return-void
.end method

.method private clearSearchHistory()V
    .locals 2

    new-instance v0, Landroid/app/AlertDialog$Builder;

    invoke-virtual {p0}, Lcom/google/android/gm/preference/GeneralPrefsFragment;->getActivity()Landroid/app/Activity;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    const v1, 0x7f09017f

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setMessage(I)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    const v1, 0x7f09017e

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    const v1, 0x1010355

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setIconAttribute(I)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    const v1, 0x7f090161

    invoke-virtual {v0, v1, p0}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    const v1, 0x7f0900e1

    invoke-virtual {v0, v1, p0}, Landroid/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->show()Landroid/app/AlertDialog;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gm/preference/GeneralPrefsFragment;->mClearSearchHistoryDialog:Landroid/app/AlertDialog;

    return-void
.end method

.method private initializeConfirmActions(Lcom/google/android/gm/persistence/Persistence;Landroid/content/Context;)V
    .locals 3
    .param p1    # Lcom/google/android/gm/persistence/Persistence;
    .param p2    # Landroid/content/Context;

    invoke-virtual {p1, p2}, Lcom/google/android/gm/persistence/Persistence;->getConfirmActions(Landroid/content/Context;)Ljava/util/Set;

    move-result-object v0

    const-string v1, "confirm-actions-delete"

    const-string v2, "delete"

    invoke-interface {v0, v2}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v2

    invoke-virtual {p0, v1, v2}, Lcom/google/android/gm/preference/GeneralPrefsFragment;->initializeCheckBox(Ljava/lang/String;Z)V

    const-string v1, "confirm-actions-archive"

    const-string v2, "archive"

    invoke-interface {v0, v2}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v2

    invoke-virtual {p0, v1, v2}, Lcom/google/android/gm/preference/GeneralPrefsFragment;->initializeCheckBox(Ljava/lang/String;Z)V

    const-string v1, "confirm-actions-send"

    const-string v2, "send"

    invoke-interface {v0, v2}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v2

    invoke-virtual {p0, v1, v2}, Lcom/google/android/gm/preference/GeneralPrefsFragment;->initializeCheckBox(Ljava/lang/String;Z)V

    return-void
.end method

.method private varargs listenForPreferenceChange([Ljava/lang/String;)V
    .locals 5
    .param p1    # [Ljava/lang/String;

    move-object v0, p1

    array-length v3, v0

    const/4 v1, 0x0

    :goto_0
    if-ge v1, v3, :cond_1

    aget-object v2, v0, v1

    invoke-virtual {p0, v2}, Lcom/google/android/gm/preference/GeneralPrefsFragment;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v4

    if-eqz v4, :cond_0

    invoke-virtual {v4, p0}, Landroid/preference/Preference;->setOnPreferenceChangeListener(Landroid/preference/Preference$OnPreferenceChangeListener;)V

    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_1
    return-void
.end method

.method private updateConfirmActions(Ljava/lang/String;Ljava/lang/Boolean;)V
    .locals 4
    .param p1    # Ljava/lang/String;
    .param p2    # Ljava/lang/Boolean;

    invoke-virtual {p0}, Lcom/google/android/gm/preference/GeneralPrefsFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    invoke-static {}, Lcom/google/android/gm/persistence/Persistence;->getInstance()Lcom/google/android/gm/persistence/Persistence;

    move-result-object v2

    invoke-virtual {v2, v0}, Lcom/google/android/gm/persistence/Persistence;->getConfirmActions(Landroid/content/Context;)Ljava/util/Set;

    move-result-object v1

    invoke-virtual {p2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v3

    if-eqz v3, :cond_1

    const-string v3, "none"

    invoke-interface {v1, v3}, Ljava/util/Set;->remove(Ljava/lang/Object;)Z

    invoke-interface {v1, p1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    :cond_0
    :goto_0
    invoke-virtual {v2, v0, v1}, Lcom/google/android/gm/persistence/Persistence;->setConfirmActions(Landroid/content/Context;Ljava/util/Set;)V

    return-void

    :cond_1
    invoke-interface {v1, p1}, Ljava/util/Set;->remove(Ljava/lang/Object;)Z

    invoke-interface {v1}, Ljava/util/Set;->size()I

    move-result v3

    if-nez v3, :cond_0

    const-string v3, "none"

    invoke-interface {v1, v3}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    goto :goto_0
.end method


# virtual methods
.method public onClick(Landroid/content/DialogInterface;I)V
    .locals 6
    .param p1    # Landroid/content/DialogInterface;
    .param p2    # I

    const/4 v5, 0x0

    const/4 v3, -0x1

    const/4 v4, 0x0

    iget-object v2, p0, Lcom/google/android/gm/preference/GeneralPrefsFragment;->mClearSearchHistoryDialog:Landroid/app/AlertDialog;

    invoke-virtual {p1, v2}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    if-ne p2, v3, :cond_0

    invoke-virtual {p0}, Lcom/google/android/gm/preference/GeneralPrefsFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    new-instance v2, Lcom/google/android/gm/preference/GeneralPrefsFragment$1;

    invoke-direct {v2, p0, v0}, Lcom/google/android/gm/preference/GeneralPrefsFragment$1;-><init>(Lcom/google/android/gm/preference/GeneralPrefsFragment;Landroid/content/Context;)V

    new-array v3, v4, [Ljava/lang/Void;

    invoke-virtual {v2, v3}, Lcom/google/android/gm/preference/GeneralPrefsFragment$1;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    invoke-virtual {p0}, Lcom/google/android/gm/preference/GeneralPrefsFragment;->getActivity()Landroid/app/Activity;

    move-result-object v2

    const v3, 0x7f0901c7

    invoke-static {v2, v3, v4}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v2

    invoke-virtual {v2}, Landroid/widget/Toast;->show()V

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v2, p0, Lcom/google/android/gm/preference/GeneralPrefsFragment;->mClearDisplayImagesDialog:Landroid/app/AlertDialog;

    invoke-virtual {p1, v2}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    if-ne p2, v3, :cond_0

    invoke-static {}, Lcom/google/android/gm/persistence/Persistence;->getInstance()Lcom/google/android/gm/persistence/Persistence;

    move-result-object v1

    invoke-virtual {p0}, Lcom/google/android/gm/preference/GeneralPrefsFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    const-string v2, "display_images"

    const-string v3, ""

    invoke-virtual {v1, v0, v5, v2, v3}, Lcom/google/android/gm/persistence/Persistence;->setString(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    const-string v2, "display_sender_images_patterns_set"

    new-instance v3, Ljava/util/HashSet;

    invoke-direct {v3}, Ljava/util/HashSet;-><init>()V

    invoke-virtual {v1, v0, v5, v2, v3}, Lcom/google/android/gm/persistence/Persistence;->setStringSet(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/util/Set;)V

    invoke-virtual {p0}, Lcom/google/android/gm/preference/GeneralPrefsFragment;->getActivity()Landroid/app/Activity;

    move-result-object v2

    const v3, 0x7f0901c8

    invoke-static {v2, v3, v4}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v2

    invoke-virtual {v2}, Landroid/widget/Toast;->show()V

    goto :goto_0
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 3
    .param p1    # Landroid/os/Bundle;

    invoke-super {p0, p1}, Lcom/google/android/gm/preference/GmailPreferenceFragment;->onCreate(Landroid/os/Bundle;)V

    const/4 v1, 0x1

    invoke-virtual {p0, v1}, Lcom/google/android/gm/preference/GeneralPrefsFragment;->setHasOptionsMenu(Z)V

    invoke-virtual {p0}, Lcom/google/android/gm/preference/GeneralPrefsFragment;->getPreferenceManager()Landroid/preference/PreferenceManager;

    move-result-object v1

    invoke-static {}, Lcom/google/android/gm/persistence/Persistence;->getInstance()Lcom/google/android/gm/persistence/Persistence;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/android/gm/persistence/Persistence;->getSharedPreferencesName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/preference/PreferenceManager;->setSharedPreferencesName(Ljava/lang/String;)V

    const v1, 0x7f060003

    invoke-virtual {p0, v1}, Lcom/google/android/gm/preference/GeneralPrefsFragment;->addPreferencesFromResource(I)V

    iget-boolean v1, p0, Lcom/google/android/gm/preference/GeneralPrefsFragment;->mCalledFromTest:Z

    if-nez v1, :cond_0

    invoke-virtual {p0}, Lcom/google/android/gm/preference/GeneralPrefsFragment;->getActivity()Landroid/app/Activity;

    move-result-object v1

    invoke-virtual {v1}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-static {v1}, Lcom/android/mail/utils/Utils;->useTabletUI(Landroid/content/res/Resources;)Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-virtual {p0}, Lcom/google/android/gm/preference/GeneralPrefsFragment;->getPreferenceScreen()Landroid/preference/PreferenceScreen;

    move-result-object v0

    const-string v1, "snap-headers"

    invoke-virtual {p0, v1}, Lcom/google/android/gm/preference/GeneralPrefsFragment;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/preference/PreferenceScreen;->removePreference(Landroid/preference/Preference;)Z

    const-string v1, "action-strip-action-reply-all"

    invoke-virtual {p0, v1}, Lcom/google/android/gm/preference/GeneralPrefsFragment;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/preference/PreferenceScreen;->removePreference(Landroid/preference/Preference;)Z

    const-string v1, "hide-checkboxes"

    invoke-virtual {p0, v1}, Lcom/google/android/gm/preference/GeneralPrefsFragment;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/preference/PreferenceScreen;->removePreference(Landroid/preference/Preference;)Z

    :cond_0
    invoke-virtual {p0}, Lcom/google/android/gm/preference/GeneralPrefsFragment;->getPreferenceScreen()Landroid/preference/PreferenceScreen;

    move-result-object v1

    const-string v2, "message-text-key"

    invoke-virtual {p0, v2}, Lcom/google/android/gm/preference/GeneralPrefsFragment;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/preference/PreferenceScreen;->removePreference(Landroid/preference/Preference;)Z

    return-void
.end method

.method public onCreateOptionsMenu(Landroid/view/Menu;Landroid/view/MenuInflater;)V
    .locals 1
    .param p1    # Landroid/view/Menu;
    .param p2    # Landroid/view/MenuInflater;

    invoke-interface {p1}, Landroid/view/Menu;->clear()V

    const v0, 0x7f110007

    invoke-virtual {p2, v0, p1}, Landroid/view/MenuInflater;->inflate(ILandroid/view/Menu;)V

    return-void
.end method

.method public onOptionsItemSelected(Landroid/view/MenuItem;)Z
    .locals 2
    .param p1    # Landroid/view/MenuItem;

    const/4 v0, 0x1

    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v1

    packed-switch v1, :pswitch_data_0

    invoke-super {p0, p1}, Lcom/google/android/gm/preference/GmailPreferenceFragment;->onOptionsItemSelected(Landroid/view/MenuItem;)Z

    move-result v0

    :goto_0
    return v0

    :pswitch_0
    invoke-direct {p0}, Lcom/google/android/gm/preference/GeneralPrefsFragment;->clearSearchHistory()V

    goto :goto_0

    :pswitch_1
    invoke-direct {p0}, Lcom/google/android/gm/preference/GeneralPrefsFragment;->clearDisplayImages()V

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x7f08012a
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public onPreferenceChange(Landroid/preference/Preference;Ljava/lang/Object;)Z
    .locals 6
    .param p1    # Landroid/preference/Preference;
    .param p2    # Ljava/lang/Object;

    const/4 v5, 0x0

    invoke-virtual {p0}, Lcom/google/android/gm/preference/GeneralPrefsFragment;->getActivity()Landroid/app/Activity;

    move-result-object v4

    if-nez v4, :cond_0

    const/4 v3, 0x0

    :goto_0
    return v3

    :cond_0
    const/4 v3, 0x1

    invoke-virtual {p1}, Landroid/preference/Preference;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {}, Lcom/google/android/gm/persistence/Persistence;->getInstance()Lcom/google/android/gm/persistence/Persistence;

    move-result-object v2

    invoke-virtual {p1}, Landroid/preference/Preference;->getKey()Ljava/lang/String;

    move-result-object v1

    const-string v4, "confirm-actions-delete"

    invoke-virtual {v4, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_1

    const-string v4, "delete"

    check-cast p2, Ljava/lang/Boolean;

    invoke-direct {p0, v4, p2}, Lcom/google/android/gm/preference/GeneralPrefsFragment;->updateConfirmActions(Ljava/lang/String;Ljava/lang/Boolean;)V

    :goto_1
    invoke-virtual {p0}, Lcom/google/android/gm/preference/GeneralPrefsFragment;->getActivity()Landroid/app/Activity;

    move-result-object v4

    invoke-static {v4}, Lcom/google/android/gm/provider/UiProvider;->notifyAccountListChanged(Landroid/content/Context;)V

    goto :goto_0

    :cond_1
    const-string v4, "confirm-actions-archive"

    invoke-virtual {v4, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_2

    const-string v4, "archive"

    check-cast p2, Ljava/lang/Boolean;

    invoke-direct {p0, v4, p2}, Lcom/google/android/gm/preference/GeneralPrefsFragment;->updateConfirmActions(Ljava/lang/String;Ljava/lang/Boolean;)V

    goto :goto_1

    :cond_2
    const-string v4, "confirm-actions-send"

    invoke-virtual {v4, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_3

    const-string v4, "send"

    check-cast p2, Ljava/lang/Boolean;

    invoke-direct {p0, v4, p2}, Lcom/google/android/gm/preference/GeneralPrefsFragment;->updateConfirmActions(Ljava/lang/String;Ljava/lang/Boolean;)V

    goto :goto_1

    :cond_3
    const-string v4, "swipe-key"

    invoke-virtual {v4, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_4

    invoke-virtual {p2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v0, v4}, Lcom/google/android/gm/persistence/Persistence;->setSwipe(Landroid/content/Context;Ljava/lang/String;)V

    goto :goto_1

    :cond_4
    const-string v4, "action-strip-action-reply-all"

    invoke-virtual {v4, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_5

    check-cast p2, Ljava/lang/Boolean;

    invoke-virtual {p2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v4

    invoke-virtual {v2, v0, v4}, Lcom/google/android/gm/persistence/Persistence;->setActionStripActionReplyAll(Landroid/content/Context;Z)V

    goto :goto_1

    :cond_5
    const-string v4, "conversation-mode"

    invoke-virtual {v4, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_6

    check-cast p2, Ljava/lang/Boolean;

    invoke-virtual {p2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v4

    invoke-virtual {v2, v0, v4}, Lcom/google/android/gm/persistence/Persistence;->setConversationOverviewMode(Landroid/content/Context;Z)V

    goto :goto_1

    :cond_6
    const-string v4, "auto-advance-key"

    invoke-virtual {v4, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_7

    invoke-virtual {p2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v0, v4}, Lcom/google/android/gm/persistence/Persistence;->setAutoAdvanceMode(Landroid/content/Context;Ljava/lang/String;)V

    goto :goto_1

    :cond_7
    const-string v4, "message-text-key"

    invoke-virtual {v4, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_8

    invoke-virtual {p2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v0, v4}, Lcom/google/android/gm/persistence/Persistence;->setMessageTextSize(Landroid/content/Context;Ljava/lang/String;)V

    goto :goto_1

    :cond_8
    const-string v4, "hide-checkboxes"

    invoke-virtual {v4, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_9

    check-cast p2, Ljava/lang/Boolean;

    invoke-virtual {v2, v0, v5, v1, p2}, Lcom/google/android/gm/persistence/Persistence;->setBoolean(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Boolean;)V

    goto/16 :goto_1

    :cond_9
    const-string v4, "snap-headers"

    invoke-virtual {v4, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_a

    invoke-virtual {p2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v0, v4}, Lcom/google/android/gm/persistence/Persistence;->setSnapHeaderMode(Landroid/content/Context;Ljava/lang/String;)V

    goto/16 :goto_1

    :cond_a
    const-string v4, "display_images"

    invoke-virtual {v4, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_b

    check-cast p2, Ljava/lang/Boolean;

    invoke-virtual {v2, v0, v5, v1, p2}, Lcom/google/android/gm/persistence/Persistence;->setBoolean(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Boolean;)V

    goto/16 :goto_1

    :cond_b
    const/4 v3, 0x0

    goto/16 :goto_1
.end method

.method public onResume()V
    .locals 6

    invoke-super {p0}, Lcom/google/android/gm/preference/GmailPreferenceFragment;->onResume()V

    invoke-virtual {p0}, Lcom/google/android/gm/preference/GeneralPrefsFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    invoke-static {}, Lcom/google/android/gm/persistence/Persistence;->getInstance()Lcom/google/android/gm/persistence/Persistence;

    move-result-object v1

    invoke-direct {p0, v1, v0}, Lcom/google/android/gm/preference/GeneralPrefsFragment;->initializeConfirmActions(Lcom/google/android/gm/persistence/Persistence;Landroid/content/Context;)V

    const-string v3, "auto-advance-key"

    invoke-virtual {p0, v3}, Lcom/google/android/gm/preference/GeneralPrefsFragment;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v3

    check-cast v3, Lcom/google/android/gm/preference/FancySummaryListPreference;

    invoke-virtual {v1, v0}, Lcom/google/android/gm/persistence/Persistence;->getAutoAdvanceMode(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Lcom/google/android/gm/preference/FancySummaryListPreference;->setValue(Ljava/lang/String;)V

    const-string v3, "swipe-key"

    invoke-virtual {p0, v3}, Lcom/google/android/gm/preference/GeneralPrefsFragment;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v3

    check-cast v3, Lcom/google/android/gm/preference/FancySummaryListPreference;

    invoke-virtual {v1, v0}, Lcom/google/android/gm/persistence/Persistence;->getSwipe(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Lcom/google/android/gm/preference/FancySummaryListPreference;->setValue(Ljava/lang/String;)V

    const-string v3, "message-text-key"

    invoke-virtual {p0, v3}, Lcom/google/android/gm/preference/GeneralPrefsFragment;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v2

    check-cast v2, Lcom/google/android/gm/preference/FancySummaryListPreference;

    if-eqz v2, :cond_0

    invoke-virtual {v1, v0}, Lcom/google/android/gm/persistence/Persistence;->getMessageTextSize(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/google/android/gm/preference/FancySummaryListPreference;->setValue(Ljava/lang/String;)V

    :cond_0
    const-string v3, "action-strip-action-reply-all"

    invoke-virtual {v1, v0}, Lcom/google/android/gm/persistence/Persistence;->getActionStripActionReplyAll(Landroid/content/Context;)Z

    move-result v4

    invoke-virtual {p0, v3, v4}, Lcom/google/android/gm/preference/GeneralPrefsFragment;->initializeCheckBox(Ljava/lang/String;Z)V

    const/16 v3, 0xb

    new-array v3, v3, [Ljava/lang/String;

    const/4 v4, 0x0

    const-string v5, "confirm-actions-delete"

    aput-object v5, v3, v4

    const/4 v4, 0x1

    const-string v5, "confirm-actions-archive"

    aput-object v5, v3, v4

    const/4 v4, 0x2

    const-string v5, "confirm-actions-send"

    aput-object v5, v3, v4

    const/4 v4, 0x3

    const-string v5, "swipe-key"

    aput-object v5, v3, v4

    const/4 v4, 0x4

    const-string v5, "action-strip-action-reply-all"

    aput-object v5, v3, v4

    const/4 v4, 0x5

    const-string v5, "auto-advance-key"

    aput-object v5, v3, v4

    const/4 v4, 0x6

    const-string v5, "message-text-key"

    aput-object v5, v3, v4

    const/4 v4, 0x7

    const-string v5, "hide-checkboxes"

    aput-object v5, v3, v4

    const/16 v4, 0x8

    const-string v5, "conversation-mode"

    aput-object v5, v3, v4

    const/16 v4, 0x9

    const-string v5, "snap-headers"

    aput-object v5, v3, v4

    const/16 v4, 0xa

    const-string v5, "display_images"

    aput-object v5, v3, v4

    invoke-direct {p0, v3}, Lcom/google/android/gm/preference/GeneralPrefsFragment;->listenForPreferenceChange([Ljava/lang/String;)V

    return-void
.end method

.method public onStop()V
    .locals 1

    invoke-super {p0}, Lcom/google/android/gm/preference/GmailPreferenceFragment;->onStop()V

    iget-object v0, p0, Lcom/google/android/gm/preference/GeneralPrefsFragment;->mClearSearchHistoryDialog:Landroid/app/AlertDialog;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/gm/preference/GeneralPrefsFragment;->mClearSearchHistoryDialog:Landroid/app/AlertDialog;

    invoke-virtual {v0}, Landroid/app/AlertDialog;->isShowing()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/gm/preference/GeneralPrefsFragment;->mClearSearchHistoryDialog:Landroid/app/AlertDialog;

    invoke-virtual {v0}, Landroid/app/AlertDialog;->dismiss()V

    :cond_0
    iget-object v0, p0, Lcom/google/android/gm/preference/GeneralPrefsFragment;->mClearDisplayImagesDialog:Landroid/app/AlertDialog;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/gm/preference/GeneralPrefsFragment;->mClearDisplayImagesDialog:Landroid/app/AlertDialog;

    invoke-virtual {v0}, Landroid/app/AlertDialog;->isShowing()Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/gm/preference/GeneralPrefsFragment;->mClearDisplayImagesDialog:Landroid/app/AlertDialog;

    invoke-virtual {v0}, Landroid/app/AlertDialog;->dismiss()V

    :cond_1
    return-void
.end method
