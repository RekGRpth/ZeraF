.class Lcom/google/android/gm/preference/DisableAccountNotificationsDialogFragment$1;
.super Ljava/lang/Object;
.source "DisableAccountNotificationsDialogFragment.java"

# interfaces
.implements Landroid/content/DialogInterface$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/gm/preference/DisableAccountNotificationsDialogFragment;->onCreateDialog(Landroid/os/Bundle;)Landroid/app/Dialog;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/gm/preference/DisableAccountNotificationsDialogFragment;

.field final synthetic val$accountName:Ljava/lang/String;


# direct methods
.method constructor <init>(Lcom/google/android/gm/preference/DisableAccountNotificationsDialogFragment;Ljava/lang/String;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/gm/preference/DisableAccountNotificationsDialogFragment$1;->this$0:Lcom/google/android/gm/preference/DisableAccountNotificationsDialogFragment;

    iput-object p2, p0, Lcom/google/android/gm/preference/DisableAccountNotificationsDialogFragment$1;->val$accountName:Ljava/lang/String;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/content/DialogInterface;I)V
    .locals 5
    .param p1    # Landroid/content/DialogInterface;
    .param p2    # I

    invoke-static {}, Lcom/google/android/gm/persistence/Persistence;->getInstance()Lcom/google/android/gm/persistence/Persistence;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/gm/preference/DisableAccountNotificationsDialogFragment$1;->this$0:Lcom/google/android/gm/preference/DisableAccountNotificationsDialogFragment;

    invoke-virtual {v2}, Lcom/google/android/gm/preference/DisableAccountNotificationsDialogFragment;->getActivity()Landroid/app/Activity;

    move-result-object v2

    iget-object v3, p0, Lcom/google/android/gm/preference/DisableAccountNotificationsDialogFragment$1;->val$accountName:Ljava/lang/String;

    const/4 v4, 0x0

    invoke-virtual {v1, v2, v3, v4}, Lcom/google/android/gm/persistence/Persistence;->setEnableNotifications(Landroid/content/Context;Ljava/lang/String;Z)V

    iget-object v1, p0, Lcom/google/android/gm/preference/DisableAccountNotificationsDialogFragment$1;->this$0:Lcom/google/android/gm/preference/DisableAccountNotificationsDialogFragment;

    invoke-virtual {v1}, Lcom/google/android/gm/preference/DisableAccountNotificationsDialogFragment;->getActivity()Landroid/app/Activity;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/gm/preference/DisableAccountNotificationsDialogFragment$1;->val$accountName:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/google/android/gm/preference/PreferenceUtils;->validateNotificationsForAccount(Landroid/content/Context;Ljava/lang/String;)V

    iget-object v1, p0, Lcom/google/android/gm/preference/DisableAccountNotificationsDialogFragment$1;->this$0:Lcom/google/android/gm/preference/DisableAccountNotificationsDialogFragment;

    # getter for: Lcom/google/android/gm/preference/DisableAccountNotificationsDialogFragment;->mListener:Ljava/lang/ref/WeakReference;
    invoke-static {v1}, Lcom/google/android/gm/preference/DisableAccountNotificationsDialogFragment;->access$000(Lcom/google/android/gm/preference/DisableAccountNotificationsDialogFragment;)Ljava/lang/ref/WeakReference;

    move-result-object v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/google/android/gm/preference/DisableAccountNotificationsDialogFragment$1;->this$0:Lcom/google/android/gm/preference/DisableAccountNotificationsDialogFragment;

    # getter for: Lcom/google/android/gm/preference/DisableAccountNotificationsDialogFragment;->mListener:Ljava/lang/ref/WeakReference;
    invoke-static {v1}, Lcom/google/android/gm/preference/DisableAccountNotificationsDialogFragment;->access$000(Lcom/google/android/gm/preference/DisableAccountNotificationsDialogFragment;)Ljava/lang/ref/WeakReference;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gm/preference/DisableAccountNotificationsDialogFragment$DisableAccountNotificationsDialogFragmentListener;

    if-eqz v0, :cond_0

    invoke-interface {v0}, Lcom/google/android/gm/preference/DisableAccountNotificationsDialogFragment$DisableAccountNotificationsDialogFragmentListener;->onNotificationsDisabled()V

    :cond_0
    return-void
.end method
