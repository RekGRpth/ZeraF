.class public final Lcom/google/android/gm/preference/LabelSettingsFragment;
.super Lcom/google/android/gm/preference/GmailPreferenceFragment;
.source "LabelSettingsFragment.java"

# interfaces
.implements Lcom/google/android/gm/LabelSettingsObserver;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/gm/preference/LabelSettingsFragment$Pair;
    }
.end annotation


# instance fields
.field private mAccount:Ljava/lang/String;

.field private mAttributeList:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/android/gm/preference/LabelSettingsFragment$Pair;",
            ">;"
        }
    .end annotation
.end field

.field private mCanVibrate:Z

.field private mDoesAccountNotify:Z

.field private mDoesLabelNotify:Z

.field private mEnableGmailSyncPreference:Lcom/google/android/gm/preference/TextButtonPreference;

.field private final mHandler:Landroid/os/Handler;

.field private mLabel:Ljava/lang/String;

.field private mNotifyOnce:Z

.field private mRingtone:Landroid/media/Ringtone;

.field private mRingtoneUri:Ljava/lang/String;

.field private mSettingsObservable:Lcom/google/android/gm/LabelSettingsObservable;

.field private mStatusChangeListenerHandle:Ljava/lang/Object;

.field private final mSyncStatusObserver:Landroid/content/SyncStatusObserver;

.field private mVibrate:Z


# direct methods
.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Lcom/google/android/gm/preference/GmailPreferenceFragment;-><init>()V

    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    iput-object v0, p0, Lcom/google/android/gm/preference/LabelSettingsFragment;->mHandler:Landroid/os/Handler;

    new-instance v0, Lcom/google/android/gm/preference/LabelSettingsFragment$2;

    invoke-direct {v0, p0}, Lcom/google/android/gm/preference/LabelSettingsFragment$2;-><init>(Lcom/google/android/gm/preference/LabelSettingsFragment;)V

    iput-object v0, p0, Lcom/google/android/gm/preference/LabelSettingsFragment;->mSyncStatusObserver:Landroid/content/SyncStatusObserver;

    return-void
.end method

.method static synthetic access$000(Lcom/google/android/gm/preference/LabelSettingsFragment;)V
    .locals 0
    .param p0    # Lcom/google/android/gm/preference/LabelSettingsFragment;

    invoke-direct {p0}, Lcom/google/android/gm/preference/LabelSettingsFragment;->showSynchronizationSettings()V

    return-void
.end method

.method static synthetic access$100(Lcom/google/android/gm/preference/LabelSettingsFragment;)V
    .locals 0
    .param p0    # Lcom/google/android/gm/preference/LabelSettingsFragment;

    invoke-direct {p0}, Lcom/google/android/gm/preference/LabelSettingsFragment;->refreshPreferences()V

    return-void
.end method

.method static synthetic access$1000(Lcom/google/android/gm/preference/LabelSettingsFragment;)V
    .locals 0
    .param p0    # Lcom/google/android/gm/preference/LabelSettingsFragment;

    invoke-direct {p0}, Lcom/google/android/gm/preference/LabelSettingsFragment;->promptEnableAccountNotifications()V

    return-void
.end method

.method static synthetic access$200(Lcom/google/android/gm/preference/LabelSettingsFragment;)Landroid/os/Handler;
    .locals 1
    .param p0    # Lcom/google/android/gm/preference/LabelSettingsFragment;

    iget-object v0, p0, Lcom/google/android/gm/preference/LabelSettingsFragment;->mHandler:Landroid/os/Handler;

    return-object v0
.end method

.method static synthetic access$300(Lcom/google/android/gm/preference/LabelSettingsFragment;)Ljava/lang/String;
    .locals 1
    .param p0    # Lcom/google/android/gm/preference/LabelSettingsFragment;

    iget-object v0, p0, Lcom/google/android/gm/preference/LabelSettingsFragment;->mAccount:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$402(Lcom/google/android/gm/preference/LabelSettingsFragment;Z)Z
    .locals 0
    .param p0    # Lcom/google/android/gm/preference/LabelSettingsFragment;
    .param p1    # Z

    iput-boolean p1, p0, Lcom/google/android/gm/preference/LabelSettingsFragment;->mDoesAccountNotify:Z

    return p1
.end method

.method static synthetic access$500(Lcom/google/android/gm/preference/LabelSettingsFragment;)Z
    .locals 1
    .param p0    # Lcom/google/android/gm/preference/LabelSettingsFragment;

    iget-boolean v0, p0, Lcom/google/android/gm/preference/LabelSettingsFragment;->mDoesLabelNotify:Z

    return v0
.end method

.method static synthetic access$502(Lcom/google/android/gm/preference/LabelSettingsFragment;Z)Z
    .locals 0
    .param p0    # Lcom/google/android/gm/preference/LabelSettingsFragment;
    .param p1    # Z

    iput-boolean p1, p0, Lcom/google/android/gm/preference/LabelSettingsFragment;->mDoesLabelNotify:Z

    return p1
.end method

.method static synthetic access$600(Lcom/google/android/gm/preference/LabelSettingsFragment;)Ljava/util/List;
    .locals 1
    .param p0    # Lcom/google/android/gm/preference/LabelSettingsFragment;

    iget-object v0, p0, Lcom/google/android/gm/preference/LabelSettingsFragment;->mAttributeList:Ljava/util/List;

    return-object v0
.end method

.method static synthetic access$700(Lcom/google/android/gm/preference/LabelSettingsFragment;)V
    .locals 0
    .param p0    # Lcom/google/android/gm/preference/LabelSettingsFragment;

    invoke-direct {p0}, Lcom/google/android/gm/preference/LabelSettingsFragment;->saveSettings()V

    return-void
.end method

.method static synthetic access$800(Lcom/google/android/gm/preference/LabelSettingsFragment;)V
    .locals 0
    .param p0    # Lcom/google/android/gm/preference/LabelSettingsFragment;

    invoke-direct {p0}, Lcom/google/android/gm/preference/LabelSettingsFragment;->onClickEnableSync()V

    return-void
.end method

.method static synthetic access$900(Lcom/google/android/gm/preference/LabelSettingsFragment;)V
    .locals 0
    .param p0    # Lcom/google/android/gm/preference/LabelSettingsFragment;

    invoke-direct {p0}, Lcom/google/android/gm/preference/LabelSettingsFragment;->showRingtonePicker()V

    return-void
.end method

.method private getRingtoneString(Landroid/media/Ringtone;)Ljava/lang/String;
    .locals 2
    .param p1    # Landroid/media/Ringtone;

    if-eqz p1, :cond_0

    invoke-virtual {p0}, Lcom/google/android/gm/preference/LabelSettingsFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/media/Ringtone;->getTitle(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    invoke-virtual {p0}, Lcom/google/android/gm/preference/LabelSettingsFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    const v1, 0x7f090198

    invoke-virtual {v0, v1}, Landroid/app/Activity;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method private varargs listenForPreferenceChange([Ljava/lang/String;)V
    .locals 5
    .param p1    # [Ljava/lang/String;

    move-object v0, p1

    array-length v3, v0

    const/4 v1, 0x0

    :goto_0
    if-ge v1, v3, :cond_1

    aget-object v2, v0, v1

    invoke-virtual {p0, v2}, Lcom/google/android/gm/preference/LabelSettingsFragment;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v4

    if-eqz v4, :cond_0

    invoke-virtual {v4, p0}, Landroid/preference/Preference;->setOnPreferenceChangeListener(Landroid/preference/Preference$OnPreferenceChangeListener;)V

    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_1
    return-void
.end method

.method private loadInitialSettings()V
    .locals 7

    const/4 v6, 0x1

    const/4 v5, 0x0

    invoke-static {}, Lcom/google/android/gm/persistence/Persistence;->getInstance()Lcom/google/android/gm/persistence/Persistence;

    move-result-object v1

    invoke-virtual {p0}, Lcom/google/android/gm/preference/LabelSettingsFragment;->getActivity()Landroid/app/Activity;

    move-result-object v2

    iget-object v3, p0, Lcom/google/android/gm/preference/LabelSettingsFragment;->mAccount:Ljava/lang/String;

    iget-object v4, p0, Lcom/google/android/gm/preference/LabelSettingsFragment;->mLabel:Ljava/lang/String;

    invoke-virtual {v1, v2, v3, v4}, Lcom/google/android/gm/persistence/Persistence;->getNotificationLabelInformation(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)Ljava/util/Set;

    move-result-object v0

    iput-boolean v5, p0, Lcom/google/android/gm/preference/LabelSettingsFragment;->mDoesLabelNotify:Z

    iput-boolean v6, p0, Lcom/google/android/gm/preference/LabelSettingsFragment;->mNotifyOnce:Z

    sget-object v2, Landroid/provider/Settings$System;->DEFAULT_NOTIFICATION_URI:Landroid/net/Uri;

    invoke-virtual {v2}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/google/android/gm/preference/LabelSettingsFragment;->mRingtoneUri:Ljava/lang/String;

    iget-boolean v2, p0, Lcom/google/android/gm/preference/LabelSettingsFragment;->mCanVibrate:Z

    if-eqz v2, :cond_0

    iput-boolean v5, p0, Lcom/google/android/gm/preference/LabelSettingsFragment;->mVibrate:Z

    :cond_0
    invoke-virtual {p0}, Lcom/google/android/gm/preference/LabelSettingsFragment;->getActivity()Landroid/app/Activity;

    move-result-object v2

    iget-object v3, p0, Lcom/google/android/gm/preference/LabelSettingsFragment;->mAccount:Ljava/lang/String;

    iget-object v4, p0, Lcom/google/android/gm/preference/LabelSettingsFragment;->mLabel:Ljava/lang/String;

    invoke-virtual {v1, v2, v3, v4}, Lcom/google/android/gm/persistence/Persistence;->shouldNotifyForLabel(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_2

    iput-boolean v6, p0, Lcom/google/android/gm/preference/LabelSettingsFragment;->mDoesLabelNotify:Z

    iget-boolean v2, p0, Lcom/google/android/gm/preference/LabelSettingsFragment;->mCanVibrate:Z

    if-eqz v2, :cond_1

    invoke-virtual {p0}, Lcom/google/android/gm/preference/LabelSettingsFragment;->getActivity()Landroid/app/Activity;

    move-result-object v2

    invoke-static {v2, v0}, Lcom/google/android/gm/persistence/Persistence;->extractVibrateSetting(Landroid/content/Context;Ljava/util/Set;)Z

    move-result v2

    iput-boolean v2, p0, Lcom/google/android/gm/preference/LabelSettingsFragment;->mVibrate:Z

    :cond_1
    invoke-virtual {p0}, Lcom/google/android/gm/preference/LabelSettingsFragment;->getActivity()Landroid/app/Activity;

    move-result-object v2

    iget-object v3, p0, Lcom/google/android/gm/preference/LabelSettingsFragment;->mAccount:Ljava/lang/String;

    iget-object v4, p0, Lcom/google/android/gm/preference/LabelSettingsFragment;->mLabel:Ljava/lang/String;

    invoke-virtual {v1, v2, v3, v4}, Lcom/google/android/gm/persistence/Persistence;->shouldNotifyOnceForLabel(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)Z

    move-result v2

    iput-boolean v2, p0, Lcom/google/android/gm/preference/LabelSettingsFragment;->mNotifyOnce:Z

    sget v2, Lcom/google/android/gm/persistence/Persistence;->LABEL_NOTIFICATION_RINGTONE:I

    invoke-static {v0, v2}, Lcom/google/android/gm/persistence/Persistence;->extract(Ljava/util/Set;I)Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/google/android/gm/preference/LabelSettingsFragment;->mRingtoneUri:Ljava/lang/String;

    :cond_2
    iget-object v2, p0, Lcom/google/android/gm/preference/LabelSettingsFragment;->mRingtoneUri:Ljava/lang/String;

    if-eqz v2, :cond_3

    iget-object v2, p0, Lcom/google/android/gm/preference/LabelSettingsFragment;->mRingtoneUri:Ljava/lang/String;

    invoke-virtual {v2}, Ljava/lang/String;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_3

    invoke-virtual {p0}, Lcom/google/android/gm/preference/LabelSettingsFragment;->getActivity()Landroid/app/Activity;

    move-result-object v2

    iget-object v3, p0, Lcom/google/android/gm/preference/LabelSettingsFragment;->mRingtoneUri:Ljava/lang/String;

    invoke-static {v3}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/media/RingtoneManager;->getRingtone(Landroid/content/Context;Landroid/net/Uri;)Landroid/media/Ringtone;

    move-result-object v2

    iput-object v2, p0, Lcom/google/android/gm/preference/LabelSettingsFragment;->mRingtone:Landroid/media/Ringtone;

    :cond_3
    invoke-virtual {p0}, Lcom/google/android/gm/preference/LabelSettingsFragment;->getActivity()Landroid/app/Activity;

    move-result-object v2

    iget-object v3, p0, Lcom/google/android/gm/preference/LabelSettingsFragment;->mAccount:Ljava/lang/String;

    invoke-virtual {v1, v2, v3}, Lcom/google/android/gm/persistence/Persistence;->getEnableNotifications(Landroid/content/Context;Ljava/lang/String;)Z

    move-result v2

    iput-boolean v2, p0, Lcom/google/android/gm/preference/LabelSettingsFragment;->mDoesAccountNotify:Z

    invoke-static {}, Lcom/google/common/collect/Lists;->newArrayList()Ljava/util/ArrayList;

    move-result-object v2

    iput-object v2, p0, Lcom/google/android/gm/preference/LabelSettingsFragment;->mAttributeList:Ljava/util/List;

    iget-object v2, p0, Lcom/google/android/gm/preference/LabelSettingsFragment;->mAttributeList:Ljava/util/List;

    new-instance v3, Lcom/google/android/gm/preference/LabelSettingsFragment$Pair;

    sget v4, Lcom/google/android/gm/persistence/Persistence;->LABEL_NOTIFICATION_ON:I

    iget-boolean v5, p0, Lcom/google/android/gm/preference/LabelSettingsFragment;->mDoesLabelNotify:Z

    invoke-static {v5}, Ljava/lang/Boolean;->toString(Z)Ljava/lang/String;

    move-result-object v5

    invoke-direct {v3, v4, v5}, Lcom/google/android/gm/preference/LabelSettingsFragment$Pair;-><init>(ILjava/lang/String;)V

    invoke-interface {v2, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    iget-object v2, p0, Lcom/google/android/gm/preference/LabelSettingsFragment;->mAttributeList:Ljava/util/List;

    new-instance v3, Lcom/google/android/gm/preference/LabelSettingsFragment$Pair;

    sget v4, Lcom/google/android/gm/persistence/Persistence;->LABEL_NOTIFICATION_RINGTONE:I

    iget-object v5, p0, Lcom/google/android/gm/preference/LabelSettingsFragment;->mRingtoneUri:Ljava/lang/String;

    invoke-direct {v3, v4, v5}, Lcom/google/android/gm/preference/LabelSettingsFragment$Pair;-><init>(ILjava/lang/String;)V

    invoke-interface {v2, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    iget-boolean v2, p0, Lcom/google/android/gm/preference/LabelSettingsFragment;->mCanVibrate:Z

    if-eqz v2, :cond_4

    iget-object v2, p0, Lcom/google/android/gm/preference/LabelSettingsFragment;->mAttributeList:Ljava/util/List;

    new-instance v3, Lcom/google/android/gm/preference/LabelSettingsFragment$Pair;

    sget v4, Lcom/google/android/gm/persistence/Persistence;->LABEL_NOTIFICATION_VIBRATE:I

    iget-boolean v5, p0, Lcom/google/android/gm/preference/LabelSettingsFragment;->mVibrate:Z

    invoke-direct {p0, v5}, Lcom/google/android/gm/preference/LabelSettingsFragment;->vibrateAttributeValue(Z)Ljava/lang/String;

    move-result-object v5

    invoke-direct {v3, v4, v5}, Lcom/google/android/gm/preference/LabelSettingsFragment$Pair;-><init>(ILjava/lang/String;)V

    invoke-interface {v2, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :cond_4
    iget-object v2, p0, Lcom/google/android/gm/preference/LabelSettingsFragment;->mAttributeList:Ljava/util/List;

    new-instance v3, Lcom/google/android/gm/preference/LabelSettingsFragment$Pair;

    sget v4, Lcom/google/android/gm/persistence/Persistence;->LABEL_NOTIFICATION_ONCE:I

    iget-boolean v5, p0, Lcom/google/android/gm/preference/LabelSettingsFragment;->mNotifyOnce:Z

    invoke-static {v5}, Ljava/lang/Boolean;->toString(Z)Ljava/lang/String;

    move-result-object v5

    invoke-direct {v3, v4, v5}, Lcom/google/android/gm/preference/LabelSettingsFragment$Pair;-><init>(ILjava/lang/String;)V

    invoke-interface {v2, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    invoke-direct {p0}, Lcom/google/android/gm/preference/LabelSettingsFragment;->refreshPreferences()V

    return-void
.end method

.method public static newInstance(Ljava/lang/String;Ljava/lang/String;)Landroid/app/Fragment;
    .locals 3
    .param p0    # Ljava/lang/String;
    .param p1    # Ljava/lang/String;

    new-instance v1, Lcom/google/android/gm/preference/LabelSettingsFragment;

    invoke-direct {v1}, Lcom/google/android/gm/preference/LabelSettingsFragment;-><init>()V

    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    const-string v2, "account"

    invoke-virtual {v0, v2, p0}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    const-string v2, "label"

    invoke-virtual {v0, v2, p1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v1, v0}, Landroid/app/Fragment;->setArguments(Landroid/os/Bundle;)V

    return-object v1
.end method

.method private onClickEnableSync()V
    .locals 6

    invoke-static {}, Landroid/content/ContentResolver;->getMasterSyncAutomatically()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-direct {p0}, Lcom/google/android/gm/preference/LabelSettingsFragment;->promptEnableAccountSync()V

    :goto_0
    return-void

    :cond_0
    new-instance v0, Landroid/content/Intent;

    const-string v2, "android.settings.SYNC_SETTINGS"

    invoke-direct {v0, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    const-string v2, "authorities"

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/String;

    const/4 v4, 0x0

    const-string v5, "gmail-ls"

    aput-object v5, v3, v4

    invoke-virtual {v0, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;[Ljava/lang/String;)Landroid/content/Intent;

    const/high16 v2, 0x80000

    invoke-virtual {v0, v2}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    const/4 v2, 0x2

    invoke-virtual {p0, v0, v2}, Lcom/google/android/gm/preference/LabelSettingsFragment;->startActivityForResult(Landroid/content/Intent;I)V

    goto :goto_0
.end method

.method private promptEnableAccountNotifications()V
    .locals 5

    new-instance v0, Landroid/app/AlertDialog$Builder;

    invoke-virtual {p0}, Lcom/google/android/gm/preference/LabelSettingsFragment;->getActivity()Landroid/app/Activity;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    const v1, 0x7f0901a3

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    iget-object v4, p0, Lcom/google/android/gm/preference/LabelSettingsFragment;->mAccount:Ljava/lang/String;

    aput-object v4, v2, v3

    invoke-virtual {p0, v1, v2}, Lcom/google/android/gm/preference/LabelSettingsFragment;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setMessage(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    const v1, 0x7f0900e1

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    const v1, 0x7f0900e0

    new-instance v2, Lcom/google/android/gm/preference/LabelSettingsFragment$3;

    invoke-direct {v2, p0}, Lcom/google/android/gm/preference/LabelSettingsFragment$3;-><init>(Lcom/google/android/gm/preference/LabelSettingsFragment;)V

    invoke-virtual {v0, v1, v2}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/AlertDialog;->show()V

    return-void
.end method

.method private promptEnableAccountSync()V
    .locals 3

    iget-object v0, p0, Lcom/google/android/gm/preference/LabelSettingsFragment;->mAccount:Ljava/lang/String;

    invoke-static {v0}, Lcom/google/android/gm/preference/EnableAccountSyncDialogFragment;->newInstance(Ljava/lang/String;)Lcom/google/android/gm/preference/EnableAccountSyncDialogFragment;

    move-result-object v0

    invoke-virtual {p0}, Lcom/google/android/gm/preference/LabelSettingsFragment;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v1

    const-string v2, "EnableAccountSyncDialogFragment"

    invoke-virtual {v0, v1, v2}, Lcom/google/android/gm/preference/EnableAccountSyncDialogFragment;->show(Landroid/app/FragmentManager;Ljava/lang/String;)V

    return-void
.end method

.method private refreshPreferences()V
    .locals 22

    invoke-static {}, Landroid/content/ContentResolver;->getMasterSyncAutomatically()Z

    move-result v12

    new-instance v2, Landroid/accounts/Account;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/gm/preference/LabelSettingsFragment;->mAccount:Ljava/lang/String;

    move-object/from16 v19, v0

    const-string v20, "com.google"

    move-object/from16 v0, v19

    move-object/from16 v1, v20

    invoke-direct {v2, v0, v1}, Landroid/accounts/Account;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    const-string v19, "gmail-ls"

    move-object/from16 v0, v19

    invoke-static {v2, v0}, Landroid/content/ContentResolver;->getSyncAutomatically(Landroid/accounts/Account;Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_4

    if-eqz v12, :cond_4

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/gm/preference/LabelSettingsFragment;->mEnableGmailSyncPreference:Lcom/google/android/gm/preference/TextButtonPreference;

    move-object/from16 v19, v0

    if-eqz v19, :cond_0

    invoke-virtual/range {p0 .. p0}, Lcom/google/android/gm/preference/LabelSettingsFragment;->getPreferenceScreen()Landroid/preference/PreferenceScreen;

    move-result-object v19

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/gm/preference/LabelSettingsFragment;->mEnableGmailSyncPreference:Lcom/google/android/gm/preference/TextButtonPreference;

    move-object/from16 v20, v0

    invoke-virtual/range {v19 .. v20}, Landroid/preference/PreferenceScreen;->removePreference(Landroid/preference/Preference;)Z

    :cond_0
    :goto_0
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/gm/preference/LabelSettingsFragment;->mSettingsObservable:Lcom/google/android/gm/LabelSettingsObservable;

    move-object/from16 v19, v0

    invoke-interface/range {v19 .. v19}, Lcom/google/android/gm/LabelSettingsObservable;->getIncludedLabels()Ljava/util/ArrayList;

    move-result-object v19

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/gm/preference/LabelSettingsFragment;->mLabel:Ljava/lang/String;

    move-object/from16 v20, v0

    invoke-virtual/range {v19 .. v20}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    move-result v11

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/gm/preference/LabelSettingsFragment;->mSettingsObservable:Lcom/google/android/gm/LabelSettingsObservable;

    move-object/from16 v19, v0

    invoke-interface/range {v19 .. v19}, Lcom/google/android/gm/LabelSettingsObservable;->getPartialLabels()Ljava/util/ArrayList;

    move-result-object v19

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/gm/preference/LabelSettingsFragment;->mLabel:Ljava/lang/String;

    move-object/from16 v20, v0

    invoke-virtual/range {v19 .. v20}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    move-result v15

    if-nez v11, :cond_6

    if-nez v15, :cond_6

    const/4 v14, 0x1

    :goto_1
    const-string v19, "label-sync"

    move-object/from16 v0, p0

    move-object/from16 v1, v19

    invoke-virtual {v0, v1}, Lcom/google/android/gm/preference/LabelSettingsFragment;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v16

    if-eqz v11, :cond_7

    invoke-virtual/range {p0 .. p0}, Lcom/google/android/gm/preference/LabelSettingsFragment;->getActivity()Landroid/app/Activity;

    move-result-object v19

    const v20, 0x7f0901d1

    invoke-virtual/range {v19 .. v20}, Landroid/app/Activity;->getString(I)Ljava/lang/String;

    move-result-object v17

    :goto_2
    invoke-virtual/range {v16 .. v17}, Landroid/preference/Preference;->setSummary(Ljava/lang/CharSequence;)V

    const-string v19, "label-notifications-category"

    move-object/from16 v0, p0

    move-object/from16 v1, v19

    invoke-virtual {v0, v1}, Lcom/google/android/gm/preference/LabelSettingsFragment;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v5

    check-cast v5, Landroid/preference/PreferenceGroup;

    if-nez v14, :cond_d

    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/google/android/gm/preference/LabelSettingsFragment;->mDoesAccountNotify:Z

    move/from16 v19, v0

    if-eqz v19, :cond_c

    const-string v19, "label-notifications"

    move-object/from16 v0, p0

    move-object/from16 v1, v19

    invoke-virtual {v0, v1}, Lcom/google/android/gm/preference/LabelSettingsFragment;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v19

    if-eqz v19, :cond_9

    const/4 v7, 0x1

    :goto_3
    if-nez v7, :cond_2

    invoke-virtual {v5}, Landroid/preference/PreferenceGroup;->removeAll()V

    new-instance v8, Landroid/preference/CheckBoxPreference;

    invoke-virtual/range {p0 .. p0}, Lcom/google/android/gm/preference/LabelSettingsFragment;->getActivity()Landroid/app/Activity;

    move-result-object v19

    move-object/from16 v0, v19

    invoke-direct {v8, v0}, Landroid/preference/CheckBoxPreference;-><init>(Landroid/content/Context;)V

    invoke-virtual {v5, v8}, Landroid/preference/PreferenceGroup;->addPreference(Landroid/preference/Preference;)Z

    const/16 v19, 0x0

    invoke-static/range {v19 .. v19}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v19

    move-object/from16 v0, v19

    invoke-virtual {v8, v0}, Landroid/preference/CheckBoxPreference;->setDefaultValue(Ljava/lang/Object;)V

    const-string v19, "label-notifications"

    move-object/from16 v0, v19

    invoke-virtual {v8, v0}, Landroid/preference/CheckBoxPreference;->setKey(Ljava/lang/String;)V

    const/16 v19, 0x0

    move/from16 v0, v19

    invoke-virtual {v8, v0}, Landroid/preference/CheckBoxPreference;->setPersistent(Z)V

    const v19, 0x7f0901a1

    move/from16 v0, v19

    invoke-virtual {v8, v0}, Landroid/preference/CheckBoxPreference;->setTitle(I)V

    new-instance v10, Landroid/preference/Preference;

    invoke-virtual/range {p0 .. p0}, Lcom/google/android/gm/preference/LabelSettingsFragment;->getActivity()Landroid/app/Activity;

    move-result-object v19

    move-object/from16 v0, v19

    invoke-direct {v10, v0}, Landroid/preference/Preference;-><init>(Landroid/content/Context;)V

    invoke-virtual {v5, v10}, Landroid/preference/PreferenceGroup;->addPreference(Landroid/preference/Preference;)Z

    const-string v19, "label-notifications"

    move-object/from16 v0, v19

    invoke-virtual {v10, v0}, Landroid/preference/Preference;->setDependency(Ljava/lang/String;)V

    const-string v19, "label-sound"

    move-object/from16 v0, v19

    invoke-virtual {v10, v0}, Landroid/preference/Preference;->setKey(Ljava/lang/String;)V

    const/16 v19, 0x0

    move/from16 v0, v19

    invoke-virtual {v10, v0}, Landroid/preference/Preference;->setPersistent(Z)V

    const v19, 0x7f090197

    move/from16 v0, v19

    invoke-virtual {v10, v0}, Landroid/preference/Preference;->setTitle(I)V

    new-instance v19, Lcom/google/android/gm/preference/LabelSettingsFragment$5;

    move-object/from16 v0, v19

    move-object/from16 v1, p0

    invoke-direct {v0, v1}, Lcom/google/android/gm/preference/LabelSettingsFragment$5;-><init>(Lcom/google/android/gm/preference/LabelSettingsFragment;)V

    move-object/from16 v0, v19

    invoke-virtual {v10, v0}, Landroid/preference/Preference;->setOnPreferenceClickListener(Landroid/preference/Preference$OnPreferenceClickListener;)V

    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/google/android/gm/preference/LabelSettingsFragment;->mCanVibrate:Z

    move/from16 v19, v0

    if-eqz v19, :cond_1

    new-instance v18, Landroid/preference/CheckBoxPreference;

    invoke-virtual/range {p0 .. p0}, Lcom/google/android/gm/preference/LabelSettingsFragment;->getActivity()Landroid/app/Activity;

    move-result-object v19

    invoke-direct/range {v18 .. v19}, Landroid/preference/CheckBoxPreference;-><init>(Landroid/content/Context;)V

    move-object/from16 v0, v18

    invoke-virtual {v5, v0}, Landroid/preference/PreferenceGroup;->addPreference(Landroid/preference/Preference;)Z

    const v19, 0x7f090196

    move-object/from16 v0, p0

    move/from16 v1, v19

    invoke-virtual {v0, v1}, Lcom/google/android/gm/preference/LabelSettingsFragment;->getString(I)Ljava/lang/String;

    move-result-object v19

    invoke-virtual/range {v18 .. v19}, Landroid/preference/CheckBoxPreference;->setDefaultValue(Ljava/lang/Object;)V

    const-string v19, "label-notifications"

    invoke-virtual/range {v18 .. v19}, Landroid/preference/CheckBoxPreference;->setDependency(Ljava/lang/String;)V

    const-string v19, "label-vibrate"

    invoke-virtual/range {v18 .. v19}, Landroid/preference/CheckBoxPreference;->setKey(Ljava/lang/String;)V

    const/16 v19, 0x0

    invoke-virtual/range {v18 .. v19}, Landroid/preference/CheckBoxPreference;->setPersistent(Z)V

    const v19, 0x7f0901a4

    invoke-virtual/range {v18 .. v19}, Landroid/preference/CheckBoxPreference;->setTitle(I)V

    :cond_1
    new-instance v9, Landroid/preference/CheckBoxPreference;

    invoke-virtual/range {p0 .. p0}, Lcom/google/android/gm/preference/LabelSettingsFragment;->getActivity()Landroid/app/Activity;

    move-result-object v19

    move-object/from16 v0, v19

    invoke-direct {v9, v0}, Landroid/preference/CheckBoxPreference;-><init>(Landroid/content/Context;)V

    invoke-virtual {v5, v9}, Landroid/preference/PreferenceGroup;->addPreference(Landroid/preference/Preference;)Z

    const/16 v19, 0x0

    invoke-static/range {v19 .. v19}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v19

    move-object/from16 v0, v19

    invoke-virtual {v9, v0}, Landroid/preference/CheckBoxPreference;->setDefaultValue(Ljava/lang/Object;)V

    const-string v19, "label-notifications"

    move-object/from16 v0, v19

    invoke-virtual {v9, v0}, Landroid/preference/CheckBoxPreference;->setDependency(Ljava/lang/String;)V

    const-string v19, "label-notify-every-message"

    move-object/from16 v0, v19

    invoke-virtual {v9, v0}, Landroid/preference/CheckBoxPreference;->setKey(Ljava/lang/String;)V

    const/16 v19, 0x0

    move/from16 v0, v19

    invoke-virtual {v9, v0}, Landroid/preference/CheckBoxPreference;->setPersistent(Z)V

    const v19, 0x7f090176

    move/from16 v0, v19

    invoke-virtual {v9, v0}, Landroid/preference/CheckBoxPreference;->setSummary(I)V

    const v19, 0x7f090175

    move/from16 v0, v19

    invoke-virtual {v9, v0}, Landroid/preference/CheckBoxPreference;->setTitle(I)V

    :cond_2
    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/google/android/gm/preference/LabelSettingsFragment;->mDoesAccountNotify:Z

    move/from16 v19, v0

    if-eqz v19, :cond_a

    if-nez v14, :cond_a

    const/4 v3, 0x1

    :goto_4
    const-string v19, "label-notifications"

    move-object/from16 v0, p0

    move-object/from16 v1, v19

    invoke-virtual {v0, v1}, Lcom/google/android/gm/preference/LabelSettingsFragment;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v8

    check-cast v8, Landroid/preference/CheckBoxPreference;

    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/google/android/gm/preference/LabelSettingsFragment;->mDoesLabelNotify:Z

    move/from16 v19, v0

    move/from16 v0, v19

    invoke-virtual {v8, v0}, Landroid/preference/CheckBoxPreference;->setChecked(Z)V

    invoke-virtual {v8, v3}, Landroid/preference/CheckBoxPreference;->setEnabled(Z)V

    const-string v19, "label-sound"

    move-object/from16 v0, p0

    move-object/from16 v1, v19

    invoke-virtual {v0, v1}, Lcom/google/android/gm/preference/LabelSettingsFragment;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v10

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/gm/preference/LabelSettingsFragment;->mRingtone:Landroid/media/Ringtone;

    move-object/from16 v19, v0

    move-object/from16 v0, p0

    move-object/from16 v1, v19

    invoke-direct {v0, v1}, Lcom/google/android/gm/preference/LabelSettingsFragment;->getRingtoneString(Landroid/media/Ringtone;)Ljava/lang/String;

    move-result-object v19

    move-object/from16 v0, v19

    invoke-virtual {v10, v0}, Landroid/preference/Preference;->setSummary(Ljava/lang/CharSequence;)V

    const-string v19, "label-vibrate"

    move-object/from16 v0, p0

    move-object/from16 v1, v19

    invoke-virtual {v0, v1}, Lcom/google/android/gm/preference/LabelSettingsFragment;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v18

    check-cast v18, Landroid/preference/CheckBoxPreference;

    if-eqz v18, :cond_3

    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/google/android/gm/preference/LabelSettingsFragment;->mVibrate:Z

    move/from16 v19, v0

    invoke-virtual/range {v18 .. v19}, Landroid/preference/CheckBoxPreference;->setChecked(Z)V

    :cond_3
    const-string v19, "label-notify-every-message"

    move-object/from16 v0, p0

    move-object/from16 v1, v19

    invoke-virtual {v0, v1}, Lcom/google/android/gm/preference/LabelSettingsFragment;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v9

    check-cast v9, Landroid/preference/CheckBoxPreference;

    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/google/android/gm/preference/LabelSettingsFragment;->mNotifyOnce:Z

    move/from16 v19, v0

    if-nez v19, :cond_b

    const/16 v19, 0x1

    :goto_5
    move/from16 v0, v19

    invoke-virtual {v9, v0}, Landroid/preference/CheckBoxPreference;->setChecked(Z)V

    :goto_6
    return-void

    :cond_4
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/gm/preference/LabelSettingsFragment;->mEnableGmailSyncPreference:Lcom/google/android/gm/preference/TextButtonPreference;

    move-object/from16 v19, v0

    if-nez v19, :cond_5

    new-instance v19, Lcom/google/android/gm/preference/TextButtonPreference;

    invoke-virtual/range {p0 .. p0}, Lcom/google/android/gm/preference/LabelSettingsFragment;->getActivity()Landroid/app/Activity;

    move-result-object v20

    invoke-direct/range {v19 .. v20}, Lcom/google/android/gm/preference/TextButtonPreference;-><init>(Landroid/content/Context;)V

    move-object/from16 v0, v19

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/google/android/gm/preference/LabelSettingsFragment;->mEnableGmailSyncPreference:Lcom/google/android/gm/preference/TextButtonPreference;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/gm/preference/LabelSettingsFragment;->mEnableGmailSyncPreference:Lcom/google/android/gm/preference/TextButtonPreference;

    move-object/from16 v19, v0

    const v20, 0x7f0901ce

    invoke-virtual/range {v19 .. v20}, Lcom/google/android/gm/preference/TextButtonPreference;->setTitle(I)V

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/gm/preference/LabelSettingsFragment;->mEnableGmailSyncPreference:Lcom/google/android/gm/preference/TextButtonPreference;

    move-object/from16 v19, v0

    const v20, 0x7f0901cd

    invoke-virtual/range {v19 .. v20}, Lcom/google/android/gm/preference/TextButtonPreference;->setSummary(I)V

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/gm/preference/LabelSettingsFragment;->mEnableGmailSyncPreference:Lcom/google/android/gm/preference/TextButtonPreference;

    move-object/from16 v19, v0

    const/16 v20, 0x0

    invoke-virtual/range {v19 .. v20}, Lcom/google/android/gm/preference/TextButtonPreference;->setOrder(I)V

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/gm/preference/LabelSettingsFragment;->mEnableGmailSyncPreference:Lcom/google/android/gm/preference/TextButtonPreference;

    move-object/from16 v19, v0

    new-instance v20, Lcom/google/android/gm/preference/LabelSettingsFragment$4;

    move-object/from16 v0, v20

    move-object/from16 v1, p0

    invoke-direct {v0, v1}, Lcom/google/android/gm/preference/LabelSettingsFragment$4;-><init>(Lcom/google/android/gm/preference/LabelSettingsFragment;)V

    invoke-virtual/range {v19 .. v20}, Lcom/google/android/gm/preference/TextButtonPreference;->setOnPreferenceClickListener(Landroid/preference/Preference$OnPreferenceClickListener;)V

    :cond_5
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/gm/preference/LabelSettingsFragment;->getPreferenceScreen()Landroid/preference/PreferenceScreen;

    move-result-object v19

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/gm/preference/LabelSettingsFragment;->mEnableGmailSyncPreference:Lcom/google/android/gm/preference/TextButtonPreference;

    move-object/from16 v20, v0

    invoke-virtual/range {v19 .. v20}, Landroid/preference/PreferenceScreen;->addPreference(Landroid/preference/Preference;)Z

    goto/16 :goto_0

    :cond_6
    const/4 v14, 0x0

    goto/16 :goto_1

    :cond_7
    if-eqz v15, :cond_8

    invoke-virtual/range {p0 .. p0}, Lcom/google/android/gm/preference/LabelSettingsFragment;->getActivity()Landroid/app/Activity;

    move-result-object v19

    const v20, 0x7f100014

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/gm/preference/LabelSettingsFragment;->mSettingsObservable:Lcom/google/android/gm/LabelSettingsObservable;

    move-object/from16 v21, v0

    invoke-interface/range {v21 .. v21}, Lcom/google/android/gm/LabelSettingsObservable;->getNumberOfSyncDays()I

    move-result v21

    invoke-static/range {v19 .. v21}, Lcom/google/android/gm/Utils;->formatPlural(Landroid/content/Context;II)Ljava/lang/String;

    move-result-object v17

    goto/16 :goto_2

    :cond_8
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/gm/preference/LabelSettingsFragment;->getActivity()Landroid/app/Activity;

    move-result-object v19

    const v20, 0x7f0901d2

    invoke-virtual/range {v19 .. v20}, Landroid/app/Activity;->getString(I)Ljava/lang/String;

    move-result-object v17

    goto/16 :goto_2

    :cond_9
    const/4 v7, 0x0

    goto/16 :goto_3

    :cond_a
    const/4 v3, 0x0

    goto/16 :goto_4

    :cond_b
    const/16 v19, 0x0

    goto/16 :goto_5

    :cond_c
    invoke-virtual {v5}, Landroid/preference/PreferenceGroup;->removeAll()V

    new-instance v6, Landroid/preference/Preference;

    invoke-virtual/range {p0 .. p0}, Lcom/google/android/gm/preference/LabelSettingsFragment;->getActivity()Landroid/app/Activity;

    move-result-object v19

    move-object/from16 v0, v19

    invoke-direct {v6, v0}, Landroid/preference/Preference;-><init>(Landroid/content/Context;)V

    invoke-virtual {v5, v6}, Landroid/preference/PreferenceGroup;->addPreference(Landroid/preference/Preference;)Z

    const/16 v19, 0x0

    move/from16 v0, v19

    invoke-virtual {v6, v0}, Landroid/preference/Preference;->setPersistent(Z)V

    const v19, 0x7f0901a1

    move/from16 v0, v19

    invoke-virtual {v6, v0}, Landroid/preference/Preference;->setTitle(I)V

    const v19, 0x7f0901a2

    move/from16 v0, v19

    invoke-virtual {v6, v0}, Landroid/preference/Preference;->setSummary(I)V

    new-instance v19, Lcom/google/android/gm/preference/LabelSettingsFragment$6;

    move-object/from16 v0, v19

    move-object/from16 v1, p0

    invoke-direct {v0, v1}, Lcom/google/android/gm/preference/LabelSettingsFragment$6;-><init>(Lcom/google/android/gm/preference/LabelSettingsFragment;)V

    move-object/from16 v0, v19

    invoke-virtual {v6, v0}, Landroid/preference/Preference;->setOnPreferenceClickListener(Landroid/preference/Preference$OnPreferenceClickListener;)V

    goto/16 :goto_6

    :cond_d
    invoke-virtual {v5}, Landroid/preference/PreferenceGroup;->removeAll()V

    new-instance v13, Lcom/google/android/gm/preference/OffsetTextPreference;

    invoke-virtual/range {p0 .. p0}, Lcom/google/android/gm/preference/LabelSettingsFragment;->getActivity()Landroid/app/Activity;

    move-result-object v19

    move-object/from16 v0, v19

    invoke-direct {v13, v0}, Lcom/google/android/gm/preference/OffsetTextPreference;-><init>(Landroid/content/Context;)V

    invoke-virtual {v5, v13}, Landroid/preference/PreferenceGroup;->addPreference(Landroid/preference/Preference;)Z

    const v19, 0x7f0901d3

    move/from16 v0, v19

    invoke-virtual {v13, v0}, Landroid/preference/Preference;->setTitle(I)V

    goto/16 :goto_6
.end method

.method private saveSettings()V
    .locals 7

    iget-boolean v3, p0, Lcom/google/android/gm/preference/LabelSettingsFragment;->mDoesLabelNotify:Z

    if-eqz v3, :cond_1

    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iget-object v3, p0, Lcom/google/android/gm/preference/LabelSettingsFragment;->mAttributeList:Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/android/gm/preference/LabelSettingsFragment$Pair;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    iget v4, v2, Lcom/google/android/gm/preference/LabelSettingsFragment$Pair;->key:I

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, v2, Lcom/google/android/gm/preference/LabelSettingsFragment$Pair;->value:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-interface {v0, v3}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_0
    invoke-static {}, Lcom/google/android/gm/persistence/Persistence;->getInstance()Lcom/google/android/gm/persistence/Persistence;

    move-result-object v3

    invoke-virtual {p0}, Lcom/google/android/gm/preference/LabelSettingsFragment;->getActivity()Landroid/app/Activity;

    move-result-object v4

    iget-object v5, p0, Lcom/google/android/gm/preference/LabelSettingsFragment;->mAccount:Ljava/lang/String;

    iget-object v6, p0, Lcom/google/android/gm/preference/LabelSettingsFragment;->mLabel:Ljava/lang/String;

    invoke-virtual {v3, v4, v5, v6, v0}, Lcom/google/android/gm/persistence/Persistence;->addNotificationLabel(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/util/Set;)V

    :goto_1
    iget-object v3, p0, Lcom/google/android/gm/preference/LabelSettingsFragment;->mSettingsObservable:Lcom/google/android/gm/LabelSettingsObservable;

    invoke-interface {v3}, Lcom/google/android/gm/LabelSettingsObservable;->notifyChanged()V

    return-void

    :cond_1
    invoke-static {}, Lcom/google/android/gm/persistence/Persistence;->getInstance()Lcom/google/android/gm/persistence/Persistence;

    move-result-object v3

    invoke-virtual {p0}, Lcom/google/android/gm/preference/LabelSettingsFragment;->getActivity()Landroid/app/Activity;

    move-result-object v4

    iget-object v5, p0, Lcom/google/android/gm/preference/LabelSettingsFragment;->mAccount:Ljava/lang/String;

    iget-object v6, p0, Lcom/google/android/gm/preference/LabelSettingsFragment;->mLabel:Ljava/lang/String;

    invoke-virtual {v3, v4, v5, v6}, Lcom/google/android/gm/persistence/Persistence;->removeNotificationLabel(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {p0}, Lcom/google/android/gm/preference/LabelSettingsFragment;->getActivity()Landroid/app/Activity;

    move-result-object v3

    iget-object v4, p0, Lcom/google/android/gm/preference/LabelSettingsFragment;->mAccount:Ljava/lang/String;

    invoke-static {v3, v4}, Lcom/google/android/gm/preference/PreferenceUtils;->validateNotificationsForAccount(Landroid/content/Context;Ljava/lang/String;)V

    goto :goto_1
.end method

.method private setRingtone(Landroid/net/Uri;)V
    .locals 4
    .param p1    # Landroid/net/Uri;

    if-eqz p1, :cond_2

    invoke-virtual {p1}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/google/android/gm/preference/LabelSettingsFragment;->mRingtoneUri:Ljava/lang/String;

    invoke-virtual {p0}, Lcom/google/android/gm/preference/LabelSettingsFragment;->getActivity()Landroid/app/Activity;

    move-result-object v2

    invoke-static {v2, p1}, Landroid/media/RingtoneManager;->getRingtone(Landroid/content/Context;Landroid/net/Uri;)Landroid/media/Ringtone;

    move-result-object v2

    iput-object v2, p0, Lcom/google/android/gm/preference/LabelSettingsFragment;->mRingtone:Landroid/media/Ringtone;

    :goto_0
    iget-object v2, p0, Lcom/google/android/gm/preference/LabelSettingsFragment;->mAttributeList:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :cond_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/gm/preference/LabelSettingsFragment$Pair;

    iget v2, v1, Lcom/google/android/gm/preference/LabelSettingsFragment$Pair;->key:I

    sget v3, Lcom/google/android/gm/persistence/Persistence;->LABEL_NOTIFICATION_RINGTONE:I

    if-ne v2, v3, :cond_0

    iget-object v2, p0, Lcom/google/android/gm/preference/LabelSettingsFragment;->mRingtoneUri:Ljava/lang/String;

    iput-object v2, v1, Lcom/google/android/gm/preference/LabelSettingsFragment$Pair;->value:Ljava/lang/String;

    :cond_1
    return-void

    :cond_2
    const-string v2, ""

    iput-object v2, p0, Lcom/google/android/gm/preference/LabelSettingsFragment;->mRingtoneUri:Ljava/lang/String;

    const/4 v2, 0x0

    iput-object v2, p0, Lcom/google/android/gm/preference/LabelSettingsFragment;->mRingtone:Landroid/media/Ringtone;

    goto :goto_0
.end method

.method private showRingtonePicker()V
    .locals 4

    const/4 v3, 0x1

    new-instance v0, Landroid/content/Intent;

    const-string v1, "android.intent.action.RINGTONE_PICKER"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/google/android/gm/preference/LabelSettingsFragment;->mRingtoneUri:Ljava/lang/String;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/google/android/gm/preference/LabelSettingsFragment;->mRingtoneUri:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_0

    const-string v1, "android.intent.extra.ringtone.EXISTING_URI"

    iget-object v2, p0, Lcom/google/android/gm/preference/LabelSettingsFragment;->mRingtoneUri:Ljava/lang/String;

    invoke-static {v2}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    :cond_0
    const-string v1, "android.intent.extra.ringtone.SHOW_DEFAULT"

    invoke-virtual {v0, v1, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    const-string v1, "android.intent.extra.ringtone.DEFAULT_URI"

    sget-object v2, Landroid/provider/Settings$System;->DEFAULT_NOTIFICATION_URI:Landroid/net/Uri;

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    const-string v1, "android.intent.extra.ringtone.SHOW_SILENT"

    invoke-virtual {v0, v1, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    const-string v1, "android.intent.extra.ringtone.TYPE"

    const/4 v2, 0x2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, Lcom/google/android/gm/preference/LabelSettingsFragment;->startActivityForResult(Landroid/content/Intent;I)V

    return-void
.end method

.method private showSynchronizationSettings()V
    .locals 3

    new-instance v0, Landroid/content/Intent;

    invoke-virtual {p0}, Lcom/google/android/gm/preference/LabelSettingsFragment;->getActivity()Landroid/app/Activity;

    move-result-object v1

    const-class v2, Lcom/google/android/gm/LabelSynchronizationActivity;

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const-string v1, "account"

    iget-object v2, p0, Lcom/google/android/gm/preference/LabelSettingsFragment;->mAccount:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const-string v1, "folder"

    iget-object v2, p0, Lcom/google/android/gm/preference/LabelSettingsFragment;->mLabel:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const-string v1, "included-labels"

    iget-object v2, p0, Lcom/google/android/gm/preference/LabelSettingsFragment;->mSettingsObservable:Lcom/google/android/gm/LabelSettingsObservable;

    invoke-interface {v2}, Lcom/google/android/gm/LabelSettingsObservable;->getIncludedLabels()Ljava/util/ArrayList;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/io/Serializable;)Landroid/content/Intent;

    const-string v1, "partial-labels"

    iget-object v2, p0, Lcom/google/android/gm/preference/LabelSettingsFragment;->mSettingsObservable:Lcom/google/android/gm/LabelSettingsObservable;

    invoke-interface {v2}, Lcom/google/android/gm/LabelSettingsObservable;->getPartialLabels()Ljava/util/ArrayList;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/io/Serializable;)Landroid/content/Intent;

    const-string v1, "num-of-sync-days"

    iget-object v2, p0, Lcom/google/android/gm/preference/LabelSettingsFragment;->mSettingsObservable:Lcom/google/android/gm/LabelSettingsObservable;

    invoke-interface {v2}, Lcom/google/android/gm/LabelSettingsObservable;->getNumberOfSyncDays()I

    move-result v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    const/4 v1, 0x1

    invoke-virtual {p0, v0, v1}, Lcom/google/android/gm/preference/LabelSettingsFragment;->startActivityForResult(Landroid/content/Intent;I)V

    return-void
.end method

.method private vibrateAttributeValue(Z)Ljava/lang/String;
    .locals 2
    .param p1    # Z

    if-eqz p1, :cond_0

    invoke-virtual {p0}, Lcom/google/android/gm/preference/LabelSettingsFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    const v1, 0x7f090195

    invoke-virtual {v0, v1}, Landroid/app/Activity;->getString(I)Ljava/lang/String;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    invoke-virtual {p0}, Lcom/google/android/gm/preference/LabelSettingsFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    const v1, 0x7f090196

    invoke-virtual {v0, v1}, Landroid/app/Activity;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method


# virtual methods
.method public onActivityCreated(Landroid/os/Bundle;)V
    .locals 1
    .param p1    # Landroid/os/Bundle;

    invoke-super {p0, p1}, Lcom/google/android/gm/preference/GmailPreferenceFragment;->onActivityCreated(Landroid/os/Bundle;)V

    invoke-virtual {p0}, Lcom/google/android/gm/preference/LabelSettingsFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    check-cast v0, Lcom/google/android/gm/LabelSettingsObservable;

    iput-object v0, p0, Lcom/google/android/gm/preference/LabelSettingsFragment;->mSettingsObservable:Lcom/google/android/gm/LabelSettingsObservable;

    iget-object v0, p0, Lcom/google/android/gm/preference/LabelSettingsFragment;->mSettingsObservable:Lcom/google/android/gm/LabelSettingsObservable;

    invoke-interface {v0, p0}, Lcom/google/android/gm/LabelSettingsObservable;->registerObserver(Lcom/google/android/gm/LabelSettingsObserver;)V

    invoke-direct {p0}, Lcom/google/android/gm/preference/LabelSettingsFragment;->loadInitialSettings()V

    return-void
.end method

.method public onActivityResult(IILandroid/content/Intent;)V
    .locals 6
    .param p1    # I
    .param p2    # I
    .param p3    # Landroid/content/Intent;

    const/4 v4, -0x1

    packed-switch p1, :pswitch_data_0

    :cond_0
    :goto_0
    invoke-direct {p0}, Lcom/google/android/gm/preference/LabelSettingsFragment;->saveSettings()V

    return-void

    :pswitch_0
    if-ne p2, v4, :cond_0

    if-eqz p3, :cond_0

    const-string v4, "android.intent.extra.ringtone.PICKED_URI"

    invoke-virtual {p3, v4}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v3

    check-cast v3, Landroid/net/Uri;

    invoke-direct {p0, v3}, Lcom/google/android/gm/preference/LabelSettingsFragment;->setRingtone(Landroid/net/Uri;)V

    goto :goto_0

    :pswitch_1
    if-ne p2, v4, :cond_0

    if-eqz p3, :cond_0

    iget-object v4, p0, Lcom/google/android/gm/preference/LabelSettingsFragment;->mSettingsObservable:Lcom/google/android/gm/LabelSettingsObservable;

    const-string v5, "included-labels"

    invoke-virtual {p3, v5}, Landroid/content/Intent;->getStringArrayListExtra(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v5

    invoke-interface {v4, v5}, Lcom/google/android/gm/LabelSettingsObservable;->setIncludedLabels(Ljava/util/ArrayList;)V

    iget-object v4, p0, Lcom/google/android/gm/preference/LabelSettingsFragment;->mSettingsObservable:Lcom/google/android/gm/LabelSettingsObservable;

    const-string v5, "partial-labels"

    invoke-virtual {p3, v5}, Landroid/content/Intent;->getStringArrayListExtra(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v5

    invoke-interface {v4, v5}, Lcom/google/android/gm/LabelSettingsObservable;->setPartialLabels(Ljava/util/ArrayList;)V

    goto :goto_0

    :pswitch_2
    invoke-static {}, Landroid/content/ContentResolver;->getMasterSyncAutomatically()Z

    move-result v2

    new-instance v0, Landroid/accounts/Account;

    iget-object v4, p0, Lcom/google/android/gm/preference/LabelSettingsFragment;->mAccount:Ljava/lang/String;

    const-string v5, "com.google"

    invoke-direct {v0, v4, v5}, Landroid/accounts/Account;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    const-string v4, "gmail-ls"

    invoke-static {v0, v4}, Landroid/content/ContentResolver;->getSyncAutomatically(Landroid/accounts/Account;Ljava/lang/String;)Z

    move-result v1

    if-eqz v2, :cond_0

    if-nez v1, :cond_0

    invoke-direct {p0}, Lcom/google/android/gm/preference/LabelSettingsFragment;->promptEnableAccountSync()V

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public onChanged()V
    .locals 0

    invoke-direct {p0}, Lcom/google/android/gm/preference/LabelSettingsFragment;->refreshPreferences()V

    return-void
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 4
    .param p1    # Landroid/os/Bundle;

    invoke-super {p0, p1}, Lcom/google/android/gm/preference/GmailPreferenceFragment;->onCreate(Landroid/os/Bundle;)V

    invoke-virtual {p0}, Lcom/google/android/gm/preference/LabelSettingsFragment;->getArguments()Landroid/os/Bundle;

    move-result-object v0

    const-string v2, "account"

    invoke-virtual {v0, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/google/android/gm/preference/LabelSettingsFragment;->mAccount:Ljava/lang/String;

    const-string v2, "label"

    invoke-virtual {v0, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/google/android/gm/preference/LabelSettingsFragment;->mLabel:Ljava/lang/String;

    invoke-virtual {p0}, Lcom/google/android/gm/preference/LabelSettingsFragment;->getActivity()Landroid/app/Activity;

    move-result-object v2

    const-string v3, "vibrator"

    invoke-virtual {v2, v3}, Landroid/app/Activity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/os/Vibrator;

    if-eqz v1, :cond_0

    invoke-virtual {v1}, Landroid/os/Vibrator;->hasVibrator()Z

    move-result v2

    if-eqz v2, :cond_0

    const/4 v2, 0x1

    :goto_0
    iput-boolean v2, p0, Lcom/google/android/gm/preference/LabelSettingsFragment;->mCanVibrate:Z

    invoke-virtual {p0}, Lcom/google/android/gm/preference/LabelSettingsFragment;->getPreferenceManager()Landroid/preference/PreferenceManager;

    move-result-object v2

    invoke-static {}, Lcom/google/android/gm/persistence/Persistence;->getInstance()Lcom/google/android/gm/persistence/Persistence;

    move-result-object v3

    invoke-virtual {v3}, Lcom/google/android/gm/persistence/Persistence;->getSharedPreferencesName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/preference/PreferenceManager;->setSharedPreferencesName(Ljava/lang/String;)V

    const v2, 0x7f060005

    invoke-virtual {p0, v2}, Lcom/google/android/gm/preference/LabelSettingsFragment;->addPreferencesFromResource(I)V

    const-string v2, "label-sync"

    invoke-virtual {p0, v2}, Lcom/google/android/gm/preference/LabelSettingsFragment;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v2

    new-instance v3, Lcom/google/android/gm/preference/LabelSettingsFragment$1;

    invoke-direct {v3, p0}, Lcom/google/android/gm/preference/LabelSettingsFragment$1;-><init>(Lcom/google/android/gm/preference/LabelSettingsFragment;)V

    invoke-virtual {v2, v3}, Landroid/preference/Preference;->setOnPreferenceClickListener(Landroid/preference/Preference$OnPreferenceClickListener;)V

    return-void

    :cond_0
    const/4 v2, 0x0

    goto :goto_0
.end method

.method public onDestroyView()V
    .locals 1

    iget-object v0, p0, Lcom/google/android/gm/preference/LabelSettingsFragment;->mSettingsObservable:Lcom/google/android/gm/LabelSettingsObservable;

    invoke-interface {v0, p0}, Lcom/google/android/gm/LabelSettingsObservable;->unregisterObserver(Lcom/google/android/gm/LabelSettingsObserver;)V

    invoke-super {p0}, Lcom/google/android/gm/preference/GmailPreferenceFragment;->onDestroyView()V

    return-void
.end method

.method public onPause()V
    .locals 1

    invoke-super {p0}, Lcom/google/android/gm/preference/GmailPreferenceFragment;->onPause()V

    iget-object v0, p0, Lcom/google/android/gm/preference/LabelSettingsFragment;->mStatusChangeListenerHandle:Ljava/lang/Object;

    invoke-static {v0}, Landroid/content/ContentResolver;->removeStatusChangeListener(Ljava/lang/Object;)V

    return-void
.end method

.method public onPreferenceChange(Landroid/preference/Preference;Ljava/lang/Object;)Z
    .locals 15
    .param p1    # Landroid/preference/Preference;
    .param p2    # Ljava/lang/Object;

    invoke-virtual {p0}, Lcom/google/android/gm/preference/LabelSettingsFragment;->getActivity()Landroid/app/Activity;

    move-result-object v11

    if-nez v11, :cond_1

    const/4 v9, 0x0

    :cond_0
    :goto_0
    return v9

    :cond_1
    const/4 v9, 0x1

    invoke-virtual/range {p1 .. p1}, Landroid/preference/Preference;->getKey()Ljava/lang/String;

    move-result-object v4

    const-string v11, "label-sync"

    invoke-virtual {v11, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_4

    :cond_2
    :goto_1
    invoke-direct {p0}, Lcom/google/android/gm/preference/LabelSettingsFragment;->saveSettings()V

    iget-boolean v11, p0, Lcom/google/android/gm/preference/LabelSettingsFragment;->mDoesLabelNotify:Z

    if-nez v11, :cond_0

    const-string v11, "label-notifications"

    invoke-virtual {v11, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_0

    invoke-virtual {p0}, Lcom/google/android/gm/preference/LabelSettingsFragment;->getActivity()Landroid/app/Activity;

    move-result-object v11

    iget-object v12, p0, Lcom/google/android/gm/preference/LabelSettingsFragment;->mAccount:Ljava/lang/String;

    const/4 v13, 0x0

    const/4 v14, 0x0

    invoke-static {v11, v12, v13, v14}, Lcom/google/android/gm/provider/LabelManager;->getLabelList(Landroid/content/Context;Ljava/lang/String;Ljava/util/List;Z)Lcom/google/android/gm/provider/LabelList;

    move-result-object v6

    const/4 v8, 0x0

    new-instance v10, Ljava/util/ArrayList;

    iget-object v11, p0, Lcom/google/android/gm/preference/LabelSettingsFragment;->mSettingsObservable:Lcom/google/android/gm/LabelSettingsObservable;

    invoke-interface {v11}, Lcom/google/android/gm/LabelSettingsObservable;->getIncludedLabels()Ljava/util/ArrayList;

    move-result-object v11

    invoke-direct {v10, v11}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    iget-object v11, p0, Lcom/google/android/gm/preference/LabelSettingsFragment;->mSettingsObservable:Lcom/google/android/gm/LabelSettingsObservable;

    invoke-interface {v11}, Lcom/google/android/gm/LabelSettingsObservable;->getPartialLabels()Ljava/util/ArrayList;

    move-result-object v11

    invoke-interface {v10, v11}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    invoke-virtual {v6}, Lcom/google/android/gm/provider/LabelList;->size()I

    move-result v11

    add-int/lit8 v0, v11, -0x1

    :goto_2
    if-ltz v0, :cond_3

    invoke-virtual {v6, v0}, Lcom/google/android/gm/provider/LabelList;->get(I)Lcom/google/android/gm/provider/Label;

    move-result-object v5

    invoke-static {}, Lcom/google/android/gm/persistence/Persistence;->getInstance()Lcom/google/android/gm/persistence/Persistence;

    move-result-object v11

    invoke-virtual {p0}, Lcom/google/android/gm/preference/LabelSettingsFragment;->getActivity()Landroid/app/Activity;

    move-result-object v12

    iget-object v13, p0, Lcom/google/android/gm/preference/LabelSettingsFragment;->mAccount:Ljava/lang/String;

    invoke-virtual {v5}, Lcom/google/android/gm/provider/Label;->getCanonicalName()Ljava/lang/String;

    move-result-object v14

    invoke-virtual {v11, v12, v13, v14}, Lcom/google/android/gm/persistence/Persistence;->shouldNotifyForLabel(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)Z

    move-result v7

    const-string v11, "^i"

    invoke-virtual {v5}, Lcom/google/android/gm/provider/Label;->getCanonicalName()Ljava/lang/String;

    move-result-object v12

    invoke-virtual {v11, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_c

    const-string v11, "^i"

    invoke-interface {v10, v11}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v11

    if-nez v11, :cond_c

    const/4 v2, 0x1

    :goto_3
    if-eqz v7, :cond_d

    if-nez v2, :cond_d

    const/4 v8, 0x1

    :cond_3
    if-nez v8, :cond_0

    invoke-static {}, Lcom/google/android/gm/persistence/Persistence;->getInstance()Lcom/google/android/gm/persistence/Persistence;

    move-result-object v11

    invoke-virtual {p0}, Lcom/google/android/gm/preference/LabelSettingsFragment;->getActivity()Landroid/app/Activity;

    move-result-object v12

    iget-object v13, p0, Lcom/google/android/gm/preference/LabelSettingsFragment;->mAccount:Ljava/lang/String;

    const/4 v14, 0x0

    invoke-virtual {v11, v12, v13, v14}, Lcom/google/android/gm/persistence/Persistence;->setEnableNotifications(Landroid/content/Context;Ljava/lang/String;Z)V

    const/4 v11, 0x0

    iput-boolean v11, p0, Lcom/google/android/gm/preference/LabelSettingsFragment;->mDoesAccountNotify:Z

    invoke-direct {p0}, Lcom/google/android/gm/preference/LabelSettingsFragment;->refreshPreferences()V

    goto/16 :goto_0

    :cond_4
    const-string v11, "label-notifications"

    invoke-virtual {v11, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_6

    check-cast p2, Ljava/lang/Boolean;

    invoke-virtual/range {p2 .. p2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v11

    iput-boolean v11, p0, Lcom/google/android/gm/preference/LabelSettingsFragment;->mDoesLabelNotify:Z

    iget-object v11, p0, Lcom/google/android/gm/preference/LabelSettingsFragment;->mAttributeList:Ljava/util/List;

    invoke-interface {v11}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_5
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v11

    if-eqz v11, :cond_2

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/google/android/gm/preference/LabelSettingsFragment$Pair;

    iget v11, v3, Lcom/google/android/gm/preference/LabelSettingsFragment$Pair;->key:I

    sget v12, Lcom/google/android/gm/persistence/Persistence;->LABEL_NOTIFICATION_ON:I

    if-ne v11, v12, :cond_5

    iget-boolean v11, p0, Lcom/google/android/gm/preference/LabelSettingsFragment;->mDoesLabelNotify:Z

    invoke-static {v11}, Ljava/lang/Boolean;->toString(Z)Ljava/lang/String;

    move-result-object v11

    iput-object v11, v3, Lcom/google/android/gm/preference/LabelSettingsFragment$Pair;->value:Ljava/lang/String;

    goto/16 :goto_1

    :cond_6
    const-string v11, "label-sound"

    invoke-virtual {v11, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-nez v11, :cond_2

    const-string v11, "label-vibrate"

    invoke-virtual {v11, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_8

    check-cast p2, Ljava/lang/Boolean;

    invoke-virtual/range {p2 .. p2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v11

    iput-boolean v11, p0, Lcom/google/android/gm/preference/LabelSettingsFragment;->mVibrate:Z

    iget-object v11, p0, Lcom/google/android/gm/preference/LabelSettingsFragment;->mAttributeList:Ljava/util/List;

    invoke-interface {v11}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_7
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v11

    if-eqz v11, :cond_2

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/google/android/gm/preference/LabelSettingsFragment$Pair;

    iget v11, v3, Lcom/google/android/gm/preference/LabelSettingsFragment$Pair;->key:I

    sget v12, Lcom/google/android/gm/persistence/Persistence;->LABEL_NOTIFICATION_VIBRATE:I

    if-ne v11, v12, :cond_7

    iget-boolean v11, p0, Lcom/google/android/gm/preference/LabelSettingsFragment;->mVibrate:Z

    invoke-direct {p0, v11}, Lcom/google/android/gm/preference/LabelSettingsFragment;->vibrateAttributeValue(Z)Ljava/lang/String;

    move-result-object v11

    iput-object v11, v3, Lcom/google/android/gm/preference/LabelSettingsFragment$Pair;->value:Ljava/lang/String;

    goto/16 :goto_1

    :cond_8
    const-string v11, "label-notify-every-message"

    invoke-virtual {v11, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_b

    check-cast p2, Ljava/lang/Boolean;

    invoke-virtual/range {p2 .. p2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v11

    if-nez v11, :cond_a

    const/4 v11, 0x1

    :goto_4
    iput-boolean v11, p0, Lcom/google/android/gm/preference/LabelSettingsFragment;->mNotifyOnce:Z

    iget-object v11, p0, Lcom/google/android/gm/preference/LabelSettingsFragment;->mAttributeList:Ljava/util/List;

    invoke-interface {v11}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_9
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v11

    if-eqz v11, :cond_2

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/google/android/gm/preference/LabelSettingsFragment$Pair;

    iget v11, v3, Lcom/google/android/gm/preference/LabelSettingsFragment$Pair;->key:I

    sget v12, Lcom/google/android/gm/persistence/Persistence;->LABEL_NOTIFICATION_ONCE:I

    if-ne v11, v12, :cond_9

    iget-boolean v11, p0, Lcom/google/android/gm/preference/LabelSettingsFragment;->mNotifyOnce:Z

    invoke-static {v11}, Ljava/lang/Boolean;->toString(Z)Ljava/lang/String;

    move-result-object v11

    iput-object v11, v3, Lcom/google/android/gm/preference/LabelSettingsFragment$Pair;->value:Ljava/lang/String;

    goto/16 :goto_1

    :cond_a
    const/4 v11, 0x0

    goto :goto_4

    :cond_b
    const/4 v9, 0x0

    goto/16 :goto_1

    :cond_c
    const/4 v2, 0x0

    goto/16 :goto_3

    :cond_d
    add-int/lit8 v0, v0, -0x1

    goto/16 :goto_2
.end method

.method public onResume()V
    .locals 4

    const/4 v3, 0x1

    invoke-super {p0}, Lcom/google/android/gm/preference/GmailPreferenceFragment;->onResume()V

    const/4 v0, 0x5

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v2, "label-sync"

    aput-object v2, v0, v1

    const-string v1, "label-notifications"

    aput-object v1, v0, v3

    const/4 v1, 0x2

    const-string v2, "label-sound"

    aput-object v2, v0, v1

    const/4 v1, 0x3

    const-string v2, "label-vibrate"

    aput-object v2, v0, v1

    const/4 v1, 0x4

    const-string v2, "label-notify-every-message"

    aput-object v2, v0, v1

    invoke-direct {p0, v0}, Lcom/google/android/gm/preference/LabelSettingsFragment;->listenForPreferenceChange([Ljava/lang/String;)V

    iget-object v0, p0, Lcom/google/android/gm/preference/LabelSettingsFragment;->mSyncStatusObserver:Landroid/content/SyncStatusObserver;

    invoke-static {v3, v0}, Landroid/content/ContentResolver;->addStatusChangeListener(ILandroid/content/SyncStatusObserver;)Ljava/lang/Object;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gm/preference/LabelSettingsFragment;->mStatusChangeListenerHandle:Ljava/lang/Object;

    return-void
.end method
