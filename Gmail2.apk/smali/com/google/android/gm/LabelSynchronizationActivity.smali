.class public Lcom/google/android/gm/LabelSynchronizationActivity;
.super Lcom/google/android/gm/GmailBaseListActivity;
.source "LabelSynchronizationActivity.java"

# interfaces
.implements Landroid/view/View$OnClickListener;
.implements Landroid/widget/AdapterView$OnItemClickListener;


# instance fields
.field private mAccount:Lcom/android/mail/providers/Account;

.field private mAccountName:Ljava/lang/String;

.field private mCurrentOption:Ljava/lang/String;

.field private mFolder:Lcom/android/mail/providers/Folder;

.field private mGmail:Lcom/google/android/gm/provider/Gmail;

.field private mIncludedLabels:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private mLabelName:Ljava/lang/String;

.field private mPartialLabels:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private mPerformActionsInternally:Z

.field private mSettings:Lcom/google/android/gm/provider/Gmail$Settings;

.field private mSyncAll:Ljava/lang/String;

.field private mSyncNone:Ljava/lang/String;

.field private mSyncPartial:Ljava/lang/String;

.field private mWidgetIdToUpdate:I


# direct methods
.method public constructor <init>()V
    .locals 2

    const/4 v1, 0x0

    invoke-direct {p0}, Lcom/google/android/gm/GmailBaseListActivity;-><init>()V

    const/4 v0, 0x1

    iput v0, p0, Lcom/google/android/gm/LabelSynchronizationActivity;->mWidgetIdToUpdate:I

    iput-object v1, p0, Lcom/google/android/gm/LabelSynchronizationActivity;->mGmail:Lcom/google/android/gm/provider/Gmail;

    iput-object v1, p0, Lcom/google/android/gm/LabelSynchronizationActivity;->mSettings:Lcom/google/android/gm/provider/Gmail$Settings;

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 1
    .param p1    # Landroid/view/View;

    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/google/android/gm/LabelSynchronizationActivity;->setResult(I)V

    invoke-virtual {p0}, Lcom/google/android/gm/LabelSynchronizationActivity;->finish()V

    return-void
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 14
    .param p1    # Landroid/os/Bundle;

    invoke-super {p0, p1}, Lcom/google/android/gm/GmailBaseListActivity;->onCreate(Landroid/os/Bundle;)V

    const v9, 0x7f040048

    invoke-virtual {p0, v9}, Lcom/google/android/gm/LabelSynchronizationActivity;->setContentView(I)V

    invoke-virtual {p0}, Lcom/google/android/gm/LabelSynchronizationActivity;->getIntent()Landroid/content/Intent;

    move-result-object v3

    const-string v9, "perform-actions-internally"

    const/4 v10, 0x0

    invoke-virtual {v3, v9, v10}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v9

    iput-boolean v9, p0, Lcom/google/android/gm/LabelSynchronizationActivity;->mPerformActionsInternally:Z

    invoke-virtual {v3}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v9

    const-string v10, "update-widgetid-on-sync-change"

    invoke-virtual {v9, v10}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v9

    if-eqz v9, :cond_0

    const-string v9, "update-widgetid-on-sync-change"

    const/4 v10, -0x1

    invoke-virtual {v3, v9, v10}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v9

    iput v9, p0, Lcom/google/android/gm/LabelSynchronizationActivity;->mWidgetIdToUpdate:I

    const-string v9, "folder"

    invoke-virtual {v3, v9}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v9

    check-cast v9, Lcom/android/mail/providers/Folder;

    iput-object v9, p0, Lcom/google/android/gm/LabelSynchronizationActivity;->mFolder:Lcom/android/mail/providers/Folder;

    iget-object v9, p0, Lcom/google/android/gm/LabelSynchronizationActivity;->mFolder:Lcom/android/mail/providers/Folder;

    iget-object v9, v9, Lcom/android/mail/providers/Folder;->uri:Landroid/net/Uri;

    invoke-virtual {v9}, Landroid/net/Uri;->getLastPathSegment()Ljava/lang/String;

    move-result-object v9

    iput-object v9, p0, Lcom/google/android/gm/LabelSynchronizationActivity;->mLabelName:Ljava/lang/String;

    const-string v9, "account"

    invoke-virtual {v3, v9}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v9

    check-cast v9, Lcom/android/mail/providers/Account;

    iput-object v9, p0, Lcom/google/android/gm/LabelSynchronizationActivity;->mAccount:Lcom/android/mail/providers/Account;

    iget-object v9, p0, Lcom/google/android/gm/LabelSynchronizationActivity;->mAccount:Lcom/android/mail/providers/Account;

    iget-object v9, v9, Lcom/android/mail/providers/Account;->name:Ljava/lang/String;

    iput-object v9, p0, Lcom/google/android/gm/LabelSynchronizationActivity;->mAccountName:Ljava/lang/String;

    :goto_0
    iget-boolean v9, p0, Lcom/google/android/gm/LabelSynchronizationActivity;->mPerformActionsInternally:Z

    if-nez v9, :cond_1

    const-string v9, "included-labels"

    invoke-virtual {v3, v9}, Landroid/content/Intent;->getStringArrayListExtra(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v9

    iput-object v9, p0, Lcom/google/android/gm/LabelSynchronizationActivity;->mIncludedLabels:Ljava/util/ArrayList;

    const-string v9, "partial-labels"

    invoke-virtual {v3, v9}, Landroid/content/Intent;->getStringArrayListExtra(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v9

    iput-object v9, p0, Lcom/google/android/gm/LabelSynchronizationActivity;->mPartialLabels:Ljava/util/ArrayList;

    const-string v9, "num-of-sync-days"

    const/4 v10, 0x0

    invoke-virtual {v3, v9, v10}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v5

    :goto_1
    iget-object v9, p0, Lcom/google/android/gm/LabelSynchronizationActivity;->mAccountName:Ljava/lang/String;

    iget-object v10, p0, Lcom/google/android/gm/LabelSynchronizationActivity;->mLabelName:Ljava/lang/String;

    invoke-static {p0, v9, v10}, Lcom/google/android/gm/provider/LabelManager;->getLabel(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/gm/provider/Label;

    move-result-object v4

    if-nez v4, :cond_2

    const-string v9, "Gmail"

    const-string v10, "Unable to get label: %s for account: %s"

    const/4 v11, 0x2

    new-array v11, v11, [Ljava/lang/Object;

    const/4 v12, 0x0

    iget-object v13, p0, Lcom/google/android/gm/LabelSynchronizationActivity;->mLabelName:Ljava/lang/String;

    aput-object v13, v11, v12

    const/4 v12, 0x1

    iget-object v13, p0, Lcom/google/android/gm/LabelSynchronizationActivity;->mAccountName:Ljava/lang/String;

    aput-object v13, v11, v12

    invoke-static {v9, v10, v11}, Lcom/google/android/gm/provider/LogUtils;->e(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)I

    invoke-virtual {p0}, Lcom/google/android/gm/LabelSynchronizationActivity;->finish()V

    :goto_2
    return-void

    :cond_0
    const-string v9, "folder"

    invoke-virtual {v3, v9}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v9

    iput-object v9, p0, Lcom/google/android/gm/LabelSynchronizationActivity;->mLabelName:Ljava/lang/String;

    const-string v9, "account"

    invoke-virtual {v3, v9}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v9

    iput-object v9, p0, Lcom/google/android/gm/LabelSynchronizationActivity;->mAccountName:Ljava/lang/String;

    goto :goto_0

    :cond_1
    invoke-virtual {p0}, Lcom/google/android/gm/LabelSynchronizationActivity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v7

    invoke-static {v7}, Lcom/google/android/gm/comm/longshadow/LongShadowUtils;->getContentProviderMailAccess(Landroid/content/ContentResolver;)Lcom/google/android/gm/provider/Gmail;

    move-result-object v9

    iput-object v9, p0, Lcom/google/android/gm/LabelSynchronizationActivity;->mGmail:Lcom/google/android/gm/provider/Gmail;

    iget-object v9, p0, Lcom/google/android/gm/LabelSynchronizationActivity;->mGmail:Lcom/google/android/gm/provider/Gmail;

    iget-object v10, p0, Lcom/google/android/gm/LabelSynchronizationActivity;->mAccountName:Ljava/lang/String;

    invoke-virtual {v9, p0, v10}, Lcom/google/android/gm/provider/Gmail;->getSettings(Landroid/content/Context;Ljava/lang/String;)Lcom/google/android/gm/provider/Gmail$Settings;

    move-result-object v9

    iput-object v9, p0, Lcom/google/android/gm/LabelSynchronizationActivity;->mSettings:Lcom/google/android/gm/provider/Gmail$Settings;

    invoke-static {}, Lcom/google/common/collect/Lists;->newArrayList()Ljava/util/ArrayList;

    move-result-object v9

    iput-object v9, p0, Lcom/google/android/gm/LabelSynchronizationActivity;->mIncludedLabels:Ljava/util/ArrayList;

    iget-object v9, p0, Lcom/google/android/gm/LabelSynchronizationActivity;->mIncludedLabels:Ljava/util/ArrayList;

    iget-object v10, p0, Lcom/google/android/gm/LabelSynchronizationActivity;->mSettings:Lcom/google/android/gm/provider/Gmail$Settings;

    invoke-virtual {v10}, Lcom/google/android/gm/provider/Gmail$Settings;->getLabelsIncluded()Ljava/util/List;

    move-result-object v10

    invoke-virtual {v9, v10}, Ljava/util/ArrayList;->addAll(Ljava/util/Collection;)Z

    invoke-static {}, Lcom/google/common/collect/Lists;->newArrayList()Ljava/util/ArrayList;

    move-result-object v9

    iput-object v9, p0, Lcom/google/android/gm/LabelSynchronizationActivity;->mPartialLabels:Ljava/util/ArrayList;

    iget-object v9, p0, Lcom/google/android/gm/LabelSynchronizationActivity;->mPartialLabels:Ljava/util/ArrayList;

    iget-object v10, p0, Lcom/google/android/gm/LabelSynchronizationActivity;->mSettings:Lcom/google/android/gm/provider/Gmail$Settings;

    invoke-virtual {v10}, Lcom/google/android/gm/provider/Gmail$Settings;->getLabelsPartial()Ljava/util/List;

    move-result-object v10

    invoke-virtual {v9, v10}, Ljava/util/ArrayList;->addAll(Ljava/util/Collection;)Z

    iget-object v9, p0, Lcom/google/android/gm/LabelSynchronizationActivity;->mSettings:Lcom/google/android/gm/provider/Gmail$Settings;

    invoke-virtual {v9}, Lcom/google/android/gm/provider/Gmail$Settings;->getConversationAgeDays()J

    move-result-wide v9

    long-to-int v5, v9

    goto :goto_1

    :cond_2
    invoke-virtual {v4}, Lcom/google/android/gm/provider/Label;->getName()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {p0, v9}, Lcom/google/android/gm/LabelSynchronizationActivity;->setTitle(Ljava/lang/CharSequence;)V

    invoke-virtual {p0}, Lcom/google/android/gm/LabelSynchronizationActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v8

    const v9, 0x7f0900ef

    invoke-virtual {v8, v9}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v9

    iput-object v9, p0, Lcom/google/android/gm/LabelSynchronizationActivity;->mSyncNone:Ljava/lang/String;

    const v9, 0x7f100010

    invoke-static {p0, v9, v5}, Lcom/google/android/gm/Utils;->formatPlural(Landroid/content/Context;II)Ljava/lang/String;

    move-result-object v9

    iput-object v9, p0, Lcom/google/android/gm/LabelSynchronizationActivity;->mSyncPartial:Ljava/lang/String;

    const v9, 0x7f0900ee

    invoke-virtual {v8, v9}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v9

    iput-object v9, p0, Lcom/google/android/gm/LabelSynchronizationActivity;->mSyncAll:Ljava/lang/String;

    invoke-virtual {v4}, Lcom/google/android/gm/provider/Label;->getForceSyncAllOrPartial()Z

    move-result v9

    if-eqz v9, :cond_4

    const/4 v9, 0x2

    new-array v1, v9, [Ljava/lang/String;

    const/4 v9, 0x0

    iget-object v10, p0, Lcom/google/android/gm/LabelSynchronizationActivity;->mSyncPartial:Ljava/lang/String;

    aput-object v10, v1, v9

    const/4 v9, 0x1

    iget-object v10, p0, Lcom/google/android/gm/LabelSynchronizationActivity;->mSyncAll:Ljava/lang/String;

    aput-object v10, v1, v9

    :goto_3
    iget-object v9, p0, Lcom/google/android/gm/LabelSynchronizationActivity;->mIncludedLabels:Ljava/util/ArrayList;

    iget-object v10, p0, Lcom/google/android/gm/LabelSynchronizationActivity;->mLabelName:Ljava/lang/String;

    invoke-virtual {v9, v10}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_5

    iget-object v9, p0, Lcom/google/android/gm/LabelSynchronizationActivity;->mSyncAll:Ljava/lang/String;

    iput-object v9, p0, Lcom/google/android/gm/LabelSynchronizationActivity;->mCurrentOption:Ljava/lang/String;

    :goto_4
    const/4 v6, 0x0

    const/4 v2, 0x0

    :goto_5
    array-length v9, v1

    if-ge v2, v9, :cond_3

    aget-object v9, v1, v2

    iget-object v10, p0, Lcom/google/android/gm/LabelSynchronizationActivity;->mCurrentOption:Ljava/lang/String;

    invoke-virtual {v9, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_7

    move v6, v2

    :cond_3
    new-instance v9, Landroid/widget/ArrayAdapter;

    const v10, 0x7f040049

    invoke-direct {v9, p0, v10, v1}, Landroid/widget/ArrayAdapter;-><init>(Landroid/content/Context;I[Ljava/lang/Object;)V

    invoke-virtual {p0, v9}, Lcom/google/android/gm/LabelSynchronizationActivity;->setListAdapter(Landroid/widget/ListAdapter;)V

    invoke-virtual {p0}, Lcom/google/android/gm/LabelSynchronizationActivity;->getListView()Landroid/widget/ListView;

    move-result-object v9

    const/4 v10, 0x1

    invoke-virtual {v9, v10}, Landroid/widget/ListView;->setChoiceMode(I)V

    invoke-virtual {p0}, Lcom/google/android/gm/LabelSynchronizationActivity;->getListView()Landroid/widget/ListView;

    move-result-object v9

    const/4 v10, 0x1

    invoke-virtual {v9, v6, v10}, Landroid/widget/ListView;->setItemChecked(IZ)V

    invoke-virtual {p0}, Lcom/google/android/gm/LabelSynchronizationActivity;->getListView()Landroid/widget/ListView;

    move-result-object v9

    invoke-virtual {v9, p0}, Landroid/widget/ListView;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    const v9, 0x7f0800b5

    invoke-virtual {p0, v9}, Lcom/google/android/gm/LabelSynchronizationActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    invoke-virtual {v0, p0}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    goto/16 :goto_2

    :cond_4
    const/4 v9, 0x3

    new-array v1, v9, [Ljava/lang/String;

    const/4 v9, 0x0

    iget-object v10, p0, Lcom/google/android/gm/LabelSynchronizationActivity;->mSyncNone:Ljava/lang/String;

    aput-object v10, v1, v9

    const/4 v9, 0x1

    iget-object v10, p0, Lcom/google/android/gm/LabelSynchronizationActivity;->mSyncPartial:Ljava/lang/String;

    aput-object v10, v1, v9

    const/4 v9, 0x2

    iget-object v10, p0, Lcom/google/android/gm/LabelSynchronizationActivity;->mSyncAll:Ljava/lang/String;

    aput-object v10, v1, v9

    goto :goto_3

    :cond_5
    iget-object v9, p0, Lcom/google/android/gm/LabelSynchronizationActivity;->mPartialLabels:Ljava/util/ArrayList;

    iget-object v10, p0, Lcom/google/android/gm/LabelSynchronizationActivity;->mLabelName:Ljava/lang/String;

    invoke-virtual {v9, v10}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_6

    iget-object v9, p0, Lcom/google/android/gm/LabelSynchronizationActivity;->mSyncPartial:Ljava/lang/String;

    iput-object v9, p0, Lcom/google/android/gm/LabelSynchronizationActivity;->mCurrentOption:Ljava/lang/String;

    goto :goto_4

    :cond_6
    iget-object v9, p0, Lcom/google/android/gm/LabelSynchronizationActivity;->mSyncNone:Ljava/lang/String;

    iput-object v9, p0, Lcom/google/android/gm/LabelSynchronizationActivity;->mCurrentOption:Ljava/lang/String;

    goto :goto_4

    :cond_7
    add-int/lit8 v2, v2, 0x1

    goto :goto_5
.end method

.method public onItemClick(Landroid/widget/AdapterView;Landroid/view/View;IJ)V
    .locals 6
    .param p2    # Landroid/view/View;
    .param p3    # I
    .param p4    # J
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/widget/AdapterView",
            "<*>;",
            "Landroid/view/View;",
            "IJ)V"
        }
    .end annotation

    const/4 v5, -0x1

    invoke-virtual {p0}, Lcom/google/android/gm/LabelSynchronizationActivity;->getListAdapter()Landroid/widget/ListAdapter;

    move-result-object v2

    invoke-interface {v2, p3}, Landroid/widget/ListAdapter;->getItem(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    iget-object v2, p0, Lcom/google/android/gm/LabelSynchronizationActivity;->mCurrentOption:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-virtual {p0}, Lcom/google/android/gm/LabelSynchronizationActivity;->finish()V

    :goto_0
    return-void

    :cond_0
    iget-object v2, p0, Lcom/google/android/gm/LabelSynchronizationActivity;->mIncludedLabels:Ljava/util/ArrayList;

    iget-object v3, p0, Lcom/google/android/gm/LabelSynchronizationActivity;->mLabelName:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z

    iget-object v2, p0, Lcom/google/android/gm/LabelSynchronizationActivity;->mPartialLabels:Ljava/util/ArrayList;

    iget-object v3, p0, Lcom/google/android/gm/LabelSynchronizationActivity;->mLabelName:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z

    iget-object v2, p0, Lcom/google/android/gm/LabelSynchronizationActivity;->mSyncAll:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    iget-object v2, p0, Lcom/google/android/gm/LabelSynchronizationActivity;->mIncludedLabels:Ljava/util/ArrayList;

    iget-object v3, p0, Lcom/google/android/gm/LabelSynchronizationActivity;->mLabelName:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :cond_1
    :goto_1
    iget-boolean v2, p0, Lcom/google/android/gm/LabelSynchronizationActivity;->mPerformActionsInternally:Z

    if-nez v2, :cond_4

    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    const-string v2, "included-labels"

    iget-object v3, p0, Lcom/google/android/gm/LabelSynchronizationActivity;->mIncludedLabels:Ljava/util/ArrayList;

    invoke-virtual {v0, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/io/Serializable;)Landroid/content/Intent;

    const-string v2, "partial-labels"

    iget-object v3, p0, Lcom/google/android/gm/LabelSynchronizationActivity;->mPartialLabels:Ljava/util/ArrayList;

    invoke-virtual {v0, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/io/Serializable;)Landroid/content/Intent;

    invoke-virtual {p0, v5, v0}, Lcom/google/android/gm/LabelSynchronizationActivity;->setResult(ILandroid/content/Intent;)V

    :goto_2
    iget v2, p0, Lcom/google/android/gm/LabelSynchronizationActivity;->mWidgetIdToUpdate:I

    if-eq v2, v5, :cond_2

    iget v2, p0, Lcom/google/android/gm/LabelSynchronizationActivity;->mWidgetIdToUpdate:I

    iget-object v3, p0, Lcom/google/android/gm/LabelSynchronizationActivity;->mAccount:Lcom/android/mail/providers/Account;

    iget-object v4, p0, Lcom/google/android/gm/LabelSynchronizationActivity;->mFolder:Lcom/android/mail/providers/Folder;

    invoke-static {p0, v2, v3, v4}, Lcom/android/mail/widget/BaseGmailWidgetProvider;->updateWidget(Landroid/content/Context;ILcom/android/mail/providers/Account;Lcom/android/mail/providers/Folder;)V

    :cond_2
    invoke-virtual {p0}, Lcom/google/android/gm/LabelSynchronizationActivity;->finish()V

    goto :goto_0

    :cond_3
    iget-object v2, p0, Lcom/google/android/gm/LabelSynchronizationActivity;->mSyncPartial:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    iget-object v2, p0, Lcom/google/android/gm/LabelSynchronizationActivity;->mPartialLabels:Ljava/util/ArrayList;

    iget-object v3, p0, Lcom/google/android/gm/LabelSynchronizationActivity;->mLabelName:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_1

    :cond_4
    iget-object v2, p0, Lcom/google/android/gm/LabelSynchronizationActivity;->mSettings:Lcom/google/android/gm/provider/Gmail$Settings;

    iget-object v3, p0, Lcom/google/android/gm/LabelSynchronizationActivity;->mIncludedLabels:Ljava/util/ArrayList;

    invoke-virtual {v2, v3}, Lcom/google/android/gm/provider/Gmail$Settings;->setLabelsIncluded(Ljava/util/Collection;)V

    iget-object v2, p0, Lcom/google/android/gm/LabelSynchronizationActivity;->mSettings:Lcom/google/android/gm/provider/Gmail$Settings;

    iget-object v3, p0, Lcom/google/android/gm/LabelSynchronizationActivity;->mPartialLabels:Ljava/util/ArrayList;

    invoke-virtual {v2, v3}, Lcom/google/android/gm/provider/Gmail$Settings;->setLabelsPartial(Ljava/util/Collection;)V

    iget-object v2, p0, Lcom/google/android/gm/LabelSynchronizationActivity;->mGmail:Lcom/google/android/gm/provider/Gmail;

    iget-object v3, p0, Lcom/google/android/gm/LabelSynchronizationActivity;->mAccountName:Ljava/lang/String;

    iget-object v4, p0, Lcom/google/android/gm/LabelSynchronizationActivity;->mSettings:Lcom/google/android/gm/provider/Gmail$Settings;

    invoke-virtual {v2, v3, v4}, Lcom/google/android/gm/provider/Gmail;->setSettings(Ljava/lang/String;Lcom/google/android/gm/provider/Gmail$Settings;)V

    goto :goto_2
.end method
