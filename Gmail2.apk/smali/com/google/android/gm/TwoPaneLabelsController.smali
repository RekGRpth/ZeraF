.class public Lcom/google/android/gm/TwoPaneLabelsController;
.super Lcom/google/android/gm/BaseLabelsController;
.source "TwoPaneLabelsController.java"


# direct methods
.method public constructor <init>(Lcom/google/android/gm/LabelsActivityController$ControllableLabelsActivity;)V
    .locals 0
    .param p1    # Lcom/google/android/gm/LabelsActivityController$ControllableLabelsActivity;

    invoke-direct {p0, p1}, Lcom/google/android/gm/BaseLabelsController;-><init>(Lcom/google/android/gm/LabelsActivityController$ControllableLabelsActivity;)V

    return-void
.end method


# virtual methods
.method public handleLabelListResumed(Lcom/google/android/gm/LabelListFragment;)V
    .locals 1
    .param p1    # Lcom/google/android/gm/LabelListFragment;

    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/google/android/gm/TwoPaneLabelsController;->toggleUpButton(Z)V

    return-void
.end method

.method public initialize(Landroid/os/Bundle;)V
    .locals 2
    .param p1    # Landroid/os/Bundle;

    invoke-super {p0, p1}, Lcom/google/android/gm/BaseLabelsController;->initialize(Landroid/os/Bundle;)V

    iget-object v0, p0, Lcom/google/android/gm/TwoPaneLabelsController;->mActivity:Lcom/google/android/gm/LabelsActivityController$ControllableLabelsActivity;

    const v1, 0x7f040069

    invoke-interface {v0, v1}, Lcom/google/android/gm/LabelsActivityController$ControllableLabelsActivity;->setContentView(I)V

    if-nez p1, :cond_1

    iget-object v0, p0, Lcom/google/android/gm/TwoPaneLabelsController;->mDefaultLabel:Ljava/lang/String;

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/gm/TwoPaneLabelsController;->mContext:Landroid/content/Context;

    iget-object v1, p0, Lcom/google/android/gm/TwoPaneLabelsController;->mAccount:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/google/android/gm/persistence/Persistence;->getAccountInbox(Landroid/content/Context;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gm/TwoPaneLabelsController;->mDefaultLabel:Ljava/lang/String;

    :cond_0
    invoke-virtual {p0}, Lcom/google/android/gm/TwoPaneLabelsController;->showManageLabelList()V

    iget-object v0, p0, Lcom/google/android/gm/TwoPaneLabelsController;->mDefaultLabel:Ljava/lang/String;

    invoke-virtual {p0, v0}, Lcom/google/android/gm/TwoPaneLabelsController;->showLabelSettings(Ljava/lang/String;)V

    :cond_1
    return-void
.end method

.method protected showLabelSettings(Ljava/lang/String;)V
    .locals 3
    .param p1    # Ljava/lang/String;

    iget-object v1, p0, Lcom/google/android/gm/TwoPaneLabelsController;->mActivity:Lcom/google/android/gm/LabelsActivityController$ControllableLabelsActivity;

    invoke-interface {v1}, Lcom/google/android/gm/LabelsActivityController$ControllableLabelsActivity;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v1

    invoke-virtual {v1}, Landroid/app/FragmentManager;->beginTransaction()Landroid/app/FragmentTransaction;

    move-result-object v0

    const/16 v1, 0x1003

    invoke-virtual {v0, v1}, Landroid/app/FragmentTransaction;->setTransition(I)Landroid/app/FragmentTransaction;

    const v1, 0x7f0800f4

    iget-object v2, p0, Lcom/google/android/gm/TwoPaneLabelsController;->mAccount:Ljava/lang/String;

    invoke-static {v2, p1}, Lcom/google/android/gm/preference/LabelSettingsFragment;->newInstance(Ljava/lang/String;Ljava/lang/String;)Landroid/app/Fragment;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/app/FragmentTransaction;->replace(ILandroid/app/Fragment;)Landroid/app/FragmentTransaction;

    invoke-virtual {v0}, Landroid/app/FragmentTransaction;->commitAllowingStateLoss()I

    return-void
.end method

.method protected showManageLabelList()V
    .locals 5

    iget-object v1, p0, Lcom/google/android/gm/TwoPaneLabelsController;->mActivity:Lcom/google/android/gm/LabelsActivityController$ControllableLabelsActivity;

    invoke-interface {v1}, Lcom/google/android/gm/LabelsActivityController$ControllableLabelsActivity;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v1

    invoke-virtual {v1}, Landroid/app/FragmentManager;->beginTransaction()Landroid/app/FragmentTransaction;

    move-result-object v0

    const v1, 0x7f0800f3

    iget-object v2, p0, Lcom/google/android/gm/TwoPaneLabelsController;->mAccount:Ljava/lang/String;

    iget-object v3, p0, Lcom/google/android/gm/TwoPaneLabelsController;->mDefaultLabel:Ljava/lang/String;

    const/4 v4, 0x1

    invoke-static {v2, v3, v4}, Lcom/google/android/gm/LabelListFragment;->newInstance(Ljava/lang/String;Ljava/lang/String;I)Lcom/google/android/gm/LabelListFragment;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/app/FragmentTransaction;->replace(ILandroid/app/Fragment;)Landroid/app/FragmentTransaction;

    invoke-virtual {v0}, Landroid/app/FragmentTransaction;->commitAllowingStateLoss()I

    return-void
.end method
