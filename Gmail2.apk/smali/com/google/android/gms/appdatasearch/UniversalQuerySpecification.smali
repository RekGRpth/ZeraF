.class public Lcom/google/android/gms/appdatasearch/UniversalQuerySpecification;
.super Ljava/lang/Object;

# interfaces
.implements Lcom/google/android/gms/internal/cn;


# static fields
.field public static final CREATOR:Lcom/google/android/gms/internal/dd;


# instance fields
.field public D:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Lcom/google/android/gms/internal/dd;

    invoke-direct {v0}, Lcom/google/android/gms/internal/dd;-><init>()V

    sput-object v0, Lcom/google/android/gms/appdatasearch/UniversalQuerySpecification;->CREATOR:Lcom/google/android/gms/internal/dd;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x1

    iput v0, p0, Lcom/google/android/gms/appdatasearch/UniversalQuerySpecification;->D:I

    return-void
.end method


# virtual methods
.method public describeContents()I
    .locals 1

    sget-object v0, Lcom/google/android/gms/appdatasearch/UniversalQuerySpecification;->CREATOR:Lcom/google/android/gms/internal/dd;

    const/4 v0, 0x0

    return v0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    sget-object v0, Lcom/google/android/gms/appdatasearch/UniversalQuerySpecification;->CREATOR:Lcom/google/android/gms/internal/dd;

    invoke-static {p0, p1, p2}, Lcom/google/android/gms/internal/dd;->a(Lcom/google/android/gms/appdatasearch/UniversalQuerySpecification;Landroid/os/Parcel;I)V

    return-void
.end method
