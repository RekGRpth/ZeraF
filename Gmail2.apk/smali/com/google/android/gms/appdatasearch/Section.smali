.class public Lcom/google/android/gms/appdatasearch/Section;
.super Ljava/lang/Object;

# interfaces
.implements Lcom/google/android/gms/internal/cn;


# static fields
.field public static final CREATOR:Lcom/google/android/gms/internal/v;


# instance fields
.field public D:I

.field public name:Ljava/lang/String;

.field public snippetLength:I

.field public snippeted:Z


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Lcom/google/android/gms/internal/v;

    invoke-direct {v0}, Lcom/google/android/gms/internal/v;-><init>()V

    sput-object v0, Lcom/google/android/gms/appdatasearch/Section;->CREATOR:Lcom/google/android/gms/internal/v;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x1

    iput v0, p0, Lcom/google/android/gms/appdatasearch/Section;->D:I

    return-void
.end method

.method constructor <init>(Ljava/lang/String;ZI)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/google/android/gms/appdatasearch/Section;->name:Ljava/lang/String;

    iput-boolean p2, p0, Lcom/google/android/gms/appdatasearch/Section;->snippeted:Z

    iput p3, p0, Lcom/google/android/gms/appdatasearch/Section;->snippetLength:I

    return-void
.end method


# virtual methods
.method public describeContents()I
    .locals 1

    sget-object v0, Lcom/google/android/gms/appdatasearch/Section;->CREATOR:Lcom/google/android/gms/internal/v;

    const/4 v0, 0x0

    return v0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    sget-object v0, Lcom/google/android/gms/appdatasearch/Section;->CREATOR:Lcom/google/android/gms/internal/v;

    invoke-static {p0, p1, p2}, Lcom/google/android/gms/internal/v;->a(Lcom/google/android/gms/appdatasearch/Section;Landroid/os/Parcel;I)V

    return-void
.end method
