.class public final Lcom/google/android/gms/appdatasearch/QuerySpecification;
.super Ljava/lang/Object;

# interfaces
.implements Lcom/google/android/gms/internal/cn;


# static fields
.field public static final CREATOR:Lcom/google/android/gms/internal/cq;


# instance fields
.field public D:I

.field public hl:Z

.field public hm:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public hn:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/android/gms/appdatasearch/Section;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Lcom/google/android/gms/internal/cq;

    invoke-direct {v0}, Lcom/google/android/gms/internal/cq;-><init>()V

    sput-object v0, Lcom/google/android/gms/appdatasearch/QuerySpecification;->CREATOR:Lcom/google/android/gms/internal/cq;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x1

    iput v0, p0, Lcom/google/android/gms/appdatasearch/QuerySpecification;->D:I

    return-void
.end method


# virtual methods
.method public addFullSection(Ljava/lang/String;)Lcom/google/android/gms/appdatasearch/QuerySpecification;
    .locals 1

    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0, v0}, Lcom/google/android/gms/appdatasearch/QuerySpecification;->addSection(Ljava/lang/String;ZI)Lcom/google/android/gms/appdatasearch/QuerySpecification;

    move-result-object v0

    return-object v0
.end method

.method public addSection(Ljava/lang/String;ZI)Lcom/google/android/gms/appdatasearch/QuerySpecification;
    .locals 5

    const/16 v4, 0x10

    iget-object v0, p0, Lcom/google/android/gms/appdatasearch/QuerySpecification;->hn:Ljava/util/List;

    if-nez v0, :cond_0

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/appdatasearch/QuerySpecification;->hn:Ljava/util/List;

    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/appdatasearch/QuerySpecification;->hn:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v0, v4, :cond_1

    iget-object v0, p0, Lcom/google/android/gms/appdatasearch/QuerySpecification;->hn:Ljava/util/List;

    new-instance v1, Lcom/google/android/gms/appdatasearch/Section;

    invoke-direct {v1, p1, p2, p3}, Lcom/google/android/gms/appdatasearch/Section;-><init>(Ljava/lang/String;ZI)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    return-object p0

    :cond_1
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Exceeds maximum number of sections (%d)"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public addSnippetedSection(Ljava/lang/String;I)Lcom/google/android/gms/appdatasearch/QuerySpecification;
    .locals 1

    const/4 v0, 0x1

    invoke-virtual {p0, p1, v0, p2}, Lcom/google/android/gms/appdatasearch/QuerySpecification;->addSection(Ljava/lang/String;ZI)Lcom/google/android/gms/appdatasearch/QuerySpecification;

    move-result-object v0

    return-object v0
.end method

.method public addWantedTags(Ljava/lang/String;)Lcom/google/android/gms/appdatasearch/QuerySpecification;
    .locals 5

    const/16 v4, 0x10

    iget-object v0, p0, Lcom/google/android/gms/appdatasearch/QuerySpecification;->hm:Ljava/util/List;

    if-nez v0, :cond_0

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/appdatasearch/QuerySpecification;->hm:Ljava/util/List;

    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/appdatasearch/QuerySpecification;->hm:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v0, v4, :cond_1

    :cond_1
    iget-object v0, p0, Lcom/google/android/gms/appdatasearch/QuerySpecification;->hm:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v0, v4, :cond_2

    iget-object v0, p0, Lcom/google/android/gms/appdatasearch/QuerySpecification;->hm:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    return-object p0

    :cond_2
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Exceeds maximum number of tags (%d)"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public describeContents()I
    .locals 1

    sget-object v0, Lcom/google/android/gms/appdatasearch/QuerySpecification;->CREATOR:Lcom/google/android/gms/internal/cq;

    const/4 v0, 0x0

    return v0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    sget-object v0, Lcom/google/android/gms/appdatasearch/QuerySpecification;->CREATOR:Lcom/google/android/gms/internal/cq;

    invoke-static {p0, p1, p2}, Lcom/google/android/gms/internal/cq;->a(Lcom/google/android/gms/appdatasearch/QuerySpecification;Landroid/os/Parcel;I)V

    return-void
.end method
