.class public Lcom/google/android/gms/internal/cf;
.super Ljava/lang/Object;

# interfaces
.implements Landroid/os/Parcelable$Creator;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Landroid/os/Parcelable$Creator",
        "<",
        "Lcom/google/android/gms/appdatasearch/RegisterSectionInfo;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static a(Lcom/google/android/gms/appdatasearch/RegisterSectionInfo;Landroid/os/Parcel;I)V
    .locals 4

    const/4 v3, 0x0

    invoke-static {p1}, Lcom/google/android/gms/internal/w;->c(Landroid/os/Parcel;)I

    move-result v0

    const/4 v1, 0x1

    iget-object v2, p0, Lcom/google/android/gms/appdatasearch/RegisterSectionInfo;->name:Ljava/lang/String;

    invoke-static {p1, v1, v2, v3}, Lcom/google/android/gms/internal/w;->a(Landroid/os/Parcel;ILjava/lang/String;Z)V

    const/16 v1, 0x3e8

    iget v2, p0, Lcom/google/android/gms/appdatasearch/RegisterSectionInfo;->D:I

    invoke-static {p1, v1, v2}, Lcom/google/android/gms/internal/w;->b(Landroid/os/Parcel;II)V

    const/4 v1, 0x2

    iget-object v2, p0, Lcom/google/android/gms/appdatasearch/RegisterSectionInfo;->format:Ljava/lang/String;

    invoke-static {p1, v1, v2, v3}, Lcom/google/android/gms/internal/w;->a(Landroid/os/Parcel;ILjava/lang/String;Z)V

    const/4 v1, 0x3

    iget-boolean v2, p0, Lcom/google/android/gms/appdatasearch/RegisterSectionInfo;->noIndex:Z

    invoke-static {p1, v1, v2}, Lcom/google/android/gms/internal/w;->a(Landroid/os/Parcel;IZ)V

    const/4 v1, 0x4

    iget v2, p0, Lcom/google/android/gms/appdatasearch/RegisterSectionInfo;->weight:I

    invoke-static {p1, v1, v2}, Lcom/google/android/gms/internal/w;->b(Landroid/os/Parcel;II)V

    invoke-static {p1, v0}, Lcom/google/android/gms/internal/w;->c(Landroid/os/Parcel;I)V

    return-void
.end method


# virtual methods
.method public bridge synthetic createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;
    .locals 1

    invoke-virtual {p0, p1}, Lcom/google/android/gms/internal/cf;->s(Landroid/os/Parcel;)Lcom/google/android/gms/appdatasearch/RegisterSectionInfo;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic newArray(I)[Ljava/lang/Object;
    .locals 1

    invoke-virtual {p0, p1}, Lcom/google/android/gms/internal/cf;->v(I)[Lcom/google/android/gms/appdatasearch/RegisterSectionInfo;

    move-result-object v0

    return-object v0
.end method

.method public s(Landroid/os/Parcel;)Lcom/google/android/gms/appdatasearch/RegisterSectionInfo;
    .locals 4

    new-instance v0, Lcom/google/android/gms/appdatasearch/RegisterSectionInfo;

    invoke-direct {v0}, Lcom/google/android/gms/appdatasearch/RegisterSectionInfo;-><init>()V

    invoke-static {p1}, Lcom/google/android/gms/internal/ca;->r(Landroid/os/Parcel;)I

    move-result v1

    :goto_0
    invoke-virtual {p1}, Landroid/os/Parcel;->dataPosition()I

    move-result v2

    if-ge v2, v1, :cond_0

    invoke-static {p1}, Lcom/google/android/gms/internal/ca;->q(Landroid/os/Parcel;)I

    move-result v2

    invoke-static {v2}, Lcom/google/android/gms/internal/ca;->u(I)I

    move-result v3

    sparse-switch v3, :sswitch_data_0

    invoke-static {p1, v2}, Lcom/google/android/gms/internal/ca;->e(Landroid/os/Parcel;I)V

    goto :goto_0

    :sswitch_0
    invoke-static {p1, v2}, Lcom/google/android/gms/internal/ca;->n(Landroid/os/Parcel;I)Ljava/lang/String;

    move-result-object v2

    iput-object v2, v0, Lcom/google/android/gms/appdatasearch/RegisterSectionInfo;->name:Ljava/lang/String;

    goto :goto_0

    :sswitch_1
    invoke-static {p1, v2}, Lcom/google/android/gms/internal/ca;->h(Landroid/os/Parcel;I)I

    move-result v2

    iput v2, v0, Lcom/google/android/gms/appdatasearch/RegisterSectionInfo;->D:I

    goto :goto_0

    :sswitch_2
    invoke-static {p1, v2}, Lcom/google/android/gms/internal/ca;->n(Landroid/os/Parcel;I)Ljava/lang/String;

    move-result-object v2

    iput-object v2, v0, Lcom/google/android/gms/appdatasearch/RegisterSectionInfo;->format:Ljava/lang/String;

    goto :goto_0

    :sswitch_3
    invoke-static {p1, v2}, Lcom/google/android/gms/internal/ca;->f(Landroid/os/Parcel;I)Z

    move-result v2

    iput-boolean v2, v0, Lcom/google/android/gms/appdatasearch/RegisterSectionInfo;->noIndex:Z

    goto :goto_0

    :sswitch_4
    invoke-static {p1, v2}, Lcom/google/android/gms/internal/ca;->h(Landroid/os/Parcel;I)I

    move-result v2

    iput v2, v0, Lcom/google/android/gms/appdatasearch/RegisterSectionInfo;->weight:I

    goto :goto_0

    :cond_0
    invoke-virtual {p1}, Landroid/os/Parcel;->dataPosition()I

    move-result v2

    if-eq v2, v1, :cond_1

    new-instance v0, Lcom/google/android/gms/internal/ca$a;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Overread allowed size end="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1, p1}, Lcom/google/android/gms/internal/ca$a;-><init>(Ljava/lang/String;Landroid/os/Parcel;)V

    throw v0

    :cond_1
    return-object v0

    nop

    :sswitch_data_0
    .sparse-switch
        0x1 -> :sswitch_0
        0x2 -> :sswitch_2
        0x3 -> :sswitch_3
        0x4 -> :sswitch_4
        0x3e8 -> :sswitch_1
    .end sparse-switch
.end method

.method public v(I)[Lcom/google/android/gms/appdatasearch/RegisterSectionInfo;
    .locals 1

    new-array v0, p1, [Lcom/google/android/gms/appdatasearch/RegisterSectionInfo;

    return-object v0
.end method
