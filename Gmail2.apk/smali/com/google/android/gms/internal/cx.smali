.class public Lcom/google/android/gms/internal/cx;
.super Lcom/google/android/gms/internal/t;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/android/gms/internal/t",
        "<",
        "Lcom/google/android/gms/internal/bf;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/google/android/gms/common/GooglePlayServicesClient$ConnectionCallbacks;Lcom/google/android/gms/common/GooglePlayServicesClient$OnConnectionFailedListener;)V
    .locals 1

    const/4 v0, 0x0

    new-array v0, v0, [Ljava/lang/String;

    invoke-direct {p0, p1, p2, p3, v0}, Lcom/google/android/gms/internal/t;-><init>(Landroid/content/Context;Lcom/google/android/gms/common/GooglePlayServicesClient$ConnectionCallbacks;Lcom/google/android/gms/common/GooglePlayServicesClient$OnConnectionFailedListener;[Ljava/lang/String;)V

    return-void
.end method


# virtual methods
.method protected E(Landroid/os/IBinder;)Lcom/google/android/gms/internal/bf;
    .locals 1

    invoke-static {p1}, Lcom/google/android/gms/internal/bf$a;->m(Landroid/os/IBinder;)Lcom/google/android/gms/internal/bf;

    move-result-object v0

    return-object v0
.end method

.method protected a(Lcom/google/android/gms/internal/h;Lcom/google/android/gms/internal/t$a;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/gms/internal/h;",
            "Lcom/google/android/gms/internal/t",
            "<",
            "Lcom/google/android/gms/internal/bf;",
            ">.a;)V"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    const v0, 0x2e28cc

    invoke-virtual {p0}, Lcom/google/android/gms/internal/cx;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v1

    invoke-interface {p1, p2, v0, v1}, Lcom/google/android/gms/internal/h;->a(Lcom/google/android/gms/internal/bc;ILjava/lang/String;)V

    return-void
.end method

.method public bG()Lcom/google/android/gms/internal/bf;
    .locals 1

    invoke-virtual {p0}, Lcom/google/android/gms/internal/cx;->s()V

    invoke-virtual {p0}, Lcom/google/android/gms/internal/cx;->t()Landroid/os/IInterface;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/internal/bf;

    return-object v0
.end method

.method protected bridge synthetic f(Landroid/os/IBinder;)Landroid/os/IInterface;
    .locals 1

    invoke-virtual {p0, p1}, Lcom/google/android/gms/internal/cx;->E(Landroid/os/IBinder;)Lcom/google/android/gms/internal/bf;

    move-result-object v0

    return-object v0
.end method

.method protected n()Ljava/lang/String;
    .locals 1

    const-string v0, "com.google.android.gms.icing.INDEX_SERVICE"

    return-object v0
.end method

.method protected o()Ljava/lang/String;
    .locals 1

    const-string v0, "com.google.android.gms.appdatasearch.internal.IAppDataSearch"

    return-object v0
.end method
