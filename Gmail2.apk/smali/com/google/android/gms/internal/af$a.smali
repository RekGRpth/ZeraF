.class final Lcom/google/android/gms/internal/af$a;
.super Lcom/google/android/gms/internal/t$b;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/gms/internal/af;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x10
    name = "a"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/android/gms/internal/t",
        "<",
        "Lcom/google/android/gms/internal/bl;",
        ">.b<",
        "Lcom/google/android/gms/panorama/PanoramaClient$OnFullPanoramaInfoLoadedListener;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic aA:Lcom/google/android/gms/internal/af;

.field public final ay:Lcom/google/android/gms/common/ConnectionResult;

.field public final az:Landroid/content/Intent;

.field public final type:I


# direct methods
.method public constructor <init>(Lcom/google/android/gms/internal/af;Lcom/google/android/gms/panorama/PanoramaClient$OnFullPanoramaInfoLoadedListener;Lcom/google/android/gms/common/ConnectionResult;ILandroid/content/Intent;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/gms/internal/af$a;->aA:Lcom/google/android/gms/internal/af;

    invoke-direct {p0, p1, p2}, Lcom/google/android/gms/internal/t$b;-><init>(Lcom/google/android/gms/internal/t;Ljava/lang/Object;)V

    iput-object p3, p0, Lcom/google/android/gms/internal/af$a;->ay:Lcom/google/android/gms/common/ConnectionResult;

    iput p4, p0, Lcom/google/android/gms/internal/af$a;->type:I

    iput-object p5, p0, Lcom/google/android/gms/internal/af$a;->az:Landroid/content/Intent;

    return-void
.end method


# virtual methods
.method protected a(Lcom/google/android/gms/panorama/PanoramaClient$OnFullPanoramaInfoLoadedListener;)V
    .locals 3

    if-eqz p1, :cond_0

    iget-object v0, p0, Lcom/google/android/gms/internal/af$a;->ay:Lcom/google/android/gms/common/ConnectionResult;

    iget v1, p0, Lcom/google/android/gms/internal/af$a;->type:I

    iget-object v2, p0, Lcom/google/android/gms/internal/af$a;->az:Landroid/content/Intent;

    invoke-interface {p1, v0, v1, v2}, Lcom/google/android/gms/panorama/PanoramaClient$OnFullPanoramaInfoLoadedListener;->onFullPanoramaInfoLoaded(Lcom/google/android/gms/common/ConnectionResult;ILandroid/content/Intent;)V

    :cond_0
    return-void
.end method

.method protected bridge synthetic a(Ljava/lang/Object;)V
    .locals 0

    check-cast p1, Lcom/google/android/gms/panorama/PanoramaClient$OnFullPanoramaInfoLoadedListener;

    invoke-virtual {p0, p1}, Lcom/google/android/gms/internal/af$a;->a(Lcom/google/android/gms/panorama/PanoramaClient$OnFullPanoramaInfoLoadedListener;)V

    return-void
.end method
