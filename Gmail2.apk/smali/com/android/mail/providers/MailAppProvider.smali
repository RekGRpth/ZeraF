.class public abstract Lcom/android/mail/providers/MailAppProvider;
.super Landroid/content/ContentProvider;
.source "MailAppProvider.java"

# interfaces
.implements Landroid/content/Loader$OnLoadCompleteListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/mail/providers/MailAppProvider$AccountCacheEntry;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/content/ContentProvider;",
        "Landroid/content/Loader$OnLoadCompleteListener",
        "<",
        "Landroid/database/Cursor;",
        ">;"
    }
.end annotation


# static fields
.field private static final LOG_TAG:Ljava/lang/String;

.field private static final PENDING_ACCOUNT_URIS:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Landroid/net/Uri;",
            ">;"
        }
    .end annotation
.end field

.field private static sAuthority:Ljava/lang/String;

.field private static sInstance:Lcom/android/mail/providers/MailAppProvider;


# instance fields
.field private final mAccountCache:Ljava/util/LinkedHashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/LinkedHashMap",
            "<",
            "Landroid/net/Uri;",
            "Lcom/android/mail/providers/MailAppProvider$AccountCacheEntry;",
            ">;"
        }
    .end annotation
.end field

.field private volatile mAccountsFullyLoaded:Z

.field private final mCursorLoaderMap:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Landroid/net/Uri;",
            "Landroid/content/CursorLoader;",
            ">;"
        }
    .end annotation
.end field

.field private mResolver:Landroid/content/ContentResolver;

.field private mSharedPrefs:Landroid/content/SharedPreferences;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    invoke-static {}, Lcom/android/mail/utils/LogTag;->getLogTag()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/android/mail/providers/MailAppProvider;->LOG_TAG:Ljava/lang/String;

    invoke-static {}, Lcom/google/common/collect/Sets;->newHashSet()Ljava/util/HashSet;

    move-result-object v0

    sput-object v0, Lcom/android/mail/providers/MailAppProvider;->PENDING_ACCOUNT_URIS:Ljava/util/Set;

    return-void
.end method

.method protected constructor <init>()V
    .locals 1

    invoke-direct {p0}, Landroid/content/ContentProvider;-><init>()V

    new-instance v0, Ljava/util/LinkedHashMap;

    invoke-direct {v0}, Ljava/util/LinkedHashMap;-><init>()V

    iput-object v0, p0, Lcom/android/mail/providers/MailAppProvider;->mAccountCache:Ljava/util/LinkedHashMap;

    invoke-static {}, Lcom/google/common/collect/Maps;->newHashMap()Ljava/util/HashMap;

    move-result-object v0

    iput-object v0, p0, Lcom/android/mail/providers/MailAppProvider;->mCursorLoaderMap:Ljava/util/Map;

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/android/mail/providers/MailAppProvider;->mAccountsFullyLoaded:Z

    return-void
.end method

.method private addAccountImpl(Landroid/net/Uri;Lcom/android/mail/providers/MailAppProvider$AccountCacheEntry;)V
    .locals 6
    .param p1    # Landroid/net/Uri;
    .param p2    # Lcom/android/mail/providers/MailAppProvider$AccountCacheEntry;

    iget-object v1, p0, Lcom/android/mail/providers/MailAppProvider;->mAccountCache:Ljava/util/LinkedHashMap;

    monitor-enter v1

    :try_start_0
    sget-object v0, Lcom/android/mail/providers/MailAppProvider;->LOG_TAG:Ljava/lang/String;

    const-string v2, "adding account %s"

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    iget-object v5, p2, Lcom/android/mail/providers/MailAppProvider$AccountCacheEntry;->mAccount:Lcom/android/mail/providers/Account;

    aput-object v5, v3, v4

    invoke-static {v0, v2, v3}, Lcom/android/mail/utils/LogUtils;->v(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)I

    iget-object v0, p0, Lcom/android/mail/providers/MailAppProvider;->mAccountCache:Ljava/util/LinkedHashMap;

    invoke-virtual {v0, p1, p2}, Ljava/util/LinkedHashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    monitor-exit v1

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method private addAccountImpl(Lcom/android/mail/providers/Account;Landroid/net/Uri;Z)V
    .locals 2
    .param p1    # Lcom/android/mail/providers/Account;
    .param p2    # Landroid/net/Uri;
    .param p3    # Z

    iget-object v0, p1, Lcom/android/mail/providers/Account;->uri:Landroid/net/Uri;

    new-instance v1, Lcom/android/mail/providers/MailAppProvider$AccountCacheEntry;

    invoke-direct {v1, p1, p2}, Lcom/android/mail/providers/MailAppProvider$AccountCacheEntry;-><init>(Lcom/android/mail/providers/Account;Landroid/net/Uri;)V

    invoke-direct {p0, v0, v1}, Lcom/android/mail/providers/MailAppProvider;->addAccountImpl(Landroid/net/Uri;Lcom/android/mail/providers/MailAppProvider$AccountCacheEntry;)V

    if-eqz p3, :cond_0

    invoke-static {}, Lcom/android/mail/providers/MailAppProvider;->broadcastAccountChange()V

    :cond_0
    return-void
.end method

.method public static addAccountsForUriAsync(Landroid/net/Uri;)V
    .locals 3
    .param p0    # Landroid/net/Uri;

    sget-object v2, Lcom/android/mail/providers/MailAppProvider;->PENDING_ACCOUNT_URIS:Ljava/util/Set;

    monitor-enter v2

    :try_start_0
    invoke-static {}, Lcom/android/mail/providers/MailAppProvider;->getInstance()Lcom/android/mail/providers/MailAppProvider;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-direct {v0, p0}, Lcom/android/mail/providers/MailAppProvider;->startAccountsLoader(Landroid/net/Uri;)V

    :goto_0
    monitor-exit v2

    return-void

    :cond_0
    sget-object v1, Lcom/android/mail/providers/MailAppProvider;->PENDING_ACCOUNT_URIS:Ljava/util/Set;

    invoke-interface {v1, p0}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :catchall_0
    move-exception v1

    monitor-exit v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1
.end method

.method private static broadcastAccountChange()V
    .locals 4

    sget-object v0, Lcom/android/mail/providers/MailAppProvider;->sInstance:Lcom/android/mail/providers/MailAppProvider;

    if-eqz v0, :cond_0

    iget-object v1, v0, Lcom/android/mail/providers/MailAppProvider;->mResolver:Landroid/content/ContentResolver;

    invoke-static {}, Lcom/android/mail/providers/MailAppProvider;->getAccountsUri()Landroid/net/Uri;

    move-result-object v2

    const/4 v3, 0x0

    invoke-virtual {v1, v2, v3}, Landroid/content/ContentResolver;->notifyChange(Landroid/net/Uri;Landroid/database/ContentObserver;)V

    :cond_0
    return-void
.end method

.method private cacheAccountList()V
    .locals 7

    iget-object v6, p0, Lcom/android/mail/providers/MailAppProvider;->mAccountCache:Ljava/util/LinkedHashMap;

    monitor-enter v6

    :try_start_0
    iget-object v5, p0, Lcom/android/mail/providers/MailAppProvider;->mAccountCache:Ljava/util/LinkedHashMap;

    invoke-virtual {v5}, Ljava/util/LinkedHashMap;->values()Ljava/util/Collection;

    move-result-object v5

    invoke-static {v5}, Lcom/google/common/collect/ImmutableList;->copyOf(Ljava/util/Collection;)Lcom/google/common/collect/ImmutableList;

    move-result-object v1

    monitor-exit v6
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    new-instance v2, Lorg/json/JSONArray;

    invoke-direct {v2}, Lorg/json/JSONArray;-><init>()V

    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :goto_0
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_0

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/mail/providers/MailAppProvider$AccountCacheEntry;

    invoke-virtual {v0}, Lcom/android/mail/providers/MailAppProvider$AccountCacheEntry;->toJSONObject()Lorg/json/JSONObject;

    move-result-object v5

    invoke-virtual {v2, v5}, Lorg/json/JSONArray;->put(Ljava/lang/Object;)Lorg/json/JSONArray;

    goto :goto_0

    :catchall_0
    move-exception v5

    :try_start_1
    monitor-exit v6
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v5

    :cond_0
    invoke-direct {p0}, Lcom/android/mail/providers/MailAppProvider;->getPreferences()Landroid/content/SharedPreferences;

    move-result-object v5

    invoke-interface {v5}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v3

    const-string v5, "accountList"

    invoke-virtual {v2}, Lorg/json/JSONArray;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-interface {v3, v5, v6}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    invoke-interface {v3}, Landroid/content/SharedPreferences$Editor;->apply()V

    return-void
.end method

.method public static getAccountFromAccountUri(Landroid/net/Uri;)Lcom/android/mail/providers/Account;
    .locals 4
    .param p0    # Landroid/net/Uri;

    invoke-static {}, Lcom/android/mail/providers/MailAppProvider;->getInstance()Lcom/android/mail/providers/MailAppProvider;

    move-result-object v1

    if-eqz v1, :cond_1

    iget-boolean v2, v1, Lcom/android/mail/providers/MailAppProvider;->mAccountsFullyLoaded:Z

    if-eqz v2, :cond_1

    iget-object v3, v1, Lcom/android/mail/providers/MailAppProvider;->mAccountCache:Ljava/util/LinkedHashMap;

    monitor-enter v3

    :try_start_0
    iget-object v2, v1, Lcom/android/mail/providers/MailAppProvider;->mAccountCache:Ljava/util/LinkedHashMap;

    invoke-virtual {v2, p0}, Ljava/util/LinkedHashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/mail/providers/MailAppProvider$AccountCacheEntry;

    if-eqz v0, :cond_0

    iget-object v2, v0, Lcom/android/mail/providers/MailAppProvider$AccountCacheEntry;->mAccount:Lcom/android/mail/providers/Account;

    monitor-exit v3

    :goto_0
    return-object v2

    :cond_0
    monitor-exit v3

    :cond_1
    const/4 v2, 0x0

    goto :goto_0

    :catchall_0
    move-exception v2

    monitor-exit v3
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v2
.end method

.method public static getAccountsUri()Landroid/net/Uri;
    .locals 2

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "content://"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    sget-object v1, Lcom/android/mail/providers/MailAppProvider;->sAuthority:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "/"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    return-object v0
.end method

.method public static getInstance()Lcom/android/mail/providers/MailAppProvider;
    .locals 1

    sget-object v0, Lcom/android/mail/providers/MailAppProvider;->sInstance:Lcom/android/mail/providers/MailAppProvider;

    return-object v0
.end method

.method public static getNoAccountIntent(Landroid/content/Context;)Landroid/content/Intent;
    .locals 1
    .param p0    # Landroid/content/Context;

    invoke-static {}, Lcom/android/mail/providers/MailAppProvider;->getInstance()Lcom/android/mail/providers/MailAppProvider;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/android/mail/providers/MailAppProvider;->getNoAccountsIntent(Landroid/content/Context;)Landroid/content/Intent;

    move-result-object v0

    return-object v0
.end method

.method private getPreferences()Landroid/content/SharedPreferences;
    .locals 3

    iget-object v0, p0, Lcom/android/mail/providers/MailAppProvider;->mSharedPrefs:Landroid/content/SharedPreferences;

    if-nez v0, :cond_0

    invoke-virtual {p0}, Lcom/android/mail/providers/MailAppProvider;->getContext()Landroid/content/Context;

    move-result-object v0

    const-string v1, "MailAppProvider"

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v0

    iput-object v0, p0, Lcom/android/mail/providers/MailAppProvider;->mSharedPrefs:Landroid/content/SharedPreferences;

    :cond_0
    iget-object v0, p0, Lcom/android/mail/providers/MailAppProvider;->mSharedPrefs:Landroid/content/SharedPreferences;

    return-object v0
.end method

.method private loadCachedAccountList()V
    .locals 14

    const/4 v13, 0x0

    const/4 v2, 0x0

    :try_start_0
    invoke-direct {p0}, Lcom/android/mail/providers/MailAppProvider;->getPreferences()Landroid/content/SharedPreferences;

    move-result-object v8

    const-string v9, "accountList"

    const/4 v10, 0x0

    invoke-interface {v8, v9, v10}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    if-eqz v4, :cond_0

    new-instance v3, Lorg/json/JSONArray;

    invoke-direct {v3, v4}, Lorg/json/JSONArray;-><init>(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-object v2, v3

    :cond_0
    :goto_0
    if-nez v2, :cond_1

    :goto_1
    return-void

    :catch_0
    move-exception v6

    sget-object v8, Lcom/android/mail/providers/MailAppProvider;->LOG_TAG:Ljava/lang/String;

    const-string v9, "ignoring unparsable accounts cache"

    new-array v10, v13, [Ljava/lang/Object;

    invoke-static {v8, v6, v9, v10}, Lcom/android/mail/utils/LogUtils;->e(Ljava/lang/String;Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)I

    goto :goto_0

    :cond_1
    const/4 v7, 0x0

    :goto_2
    invoke-virtual {v2}, Lorg/json/JSONArray;->length()I

    move-result v8

    if-ge v7, v8, :cond_4

    :try_start_1
    new-instance v1, Lcom/android/mail/providers/MailAppProvider$AccountCacheEntry;

    invoke-virtual {v2, v7}, Lorg/json/JSONArray;->getJSONObject(I)Lorg/json/JSONObject;

    move-result-object v8

    invoke-direct {v1, v8}, Lcom/android/mail/providers/MailAppProvider$AccountCacheEntry;-><init>(Lorg/json/JSONObject;)V

    iget-object v8, v1, Lcom/android/mail/providers/MailAppProvider$AccountCacheEntry;->mAccount:Lcom/android/mail/providers/Account;

    iget-object v8, v8, Lcom/android/mail/providers/Account;->settings:Lcom/android/mail/providers/Settings;

    if-nez v8, :cond_2

    sget-object v8, Lcom/android/mail/providers/MailAppProvider;->LOG_TAG:Ljava/lang/String;

    const-string v9, "Dropping account that doesn\'t specify settings"

    const/4 v10, 0x0

    new-array v10, v10, [Ljava/lang/Object;

    invoke-static {v8, v9, v10}, Lcom/android/mail/utils/LogUtils;->e(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)I

    :goto_3
    add-int/lit8 v7, v7, 0x1

    goto :goto_2

    :cond_2
    iget-object v0, v1, Lcom/android/mail/providers/MailAppProvider$AccountCacheEntry;->mAccount:Lcom/android/mail/providers/Account;

    iget-object v8, p0, Lcom/android/mail/providers/MailAppProvider;->mResolver:Landroid/content/ContentResolver;

    iget-object v9, v0, Lcom/android/mail/providers/Account;->uri:Landroid/net/Uri;

    invoke-virtual {v8, v9}, Landroid/content/ContentResolver;->acquireContentProviderClient(Landroid/net/Uri;)Landroid/content/ContentProviderClient;

    move-result-object v5

    if-eqz v5, :cond_3

    invoke-virtual {v5}, Landroid/content/ContentProviderClient;->release()Z

    iget-object v8, v0, Lcom/android/mail/providers/Account;->uri:Landroid/net/Uri;

    invoke-direct {p0, v8, v1}, Lcom/android/mail/providers/MailAppProvider;->addAccountImpl(Landroid/net/Uri;Lcom/android/mail/providers/MailAppProvider$AccountCacheEntry;)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1

    goto :goto_3

    :catch_1
    move-exception v6

    sget-object v8, Lcom/android/mail/providers/MailAppProvider;->LOG_TAG:Ljava/lang/String;

    const-string v9, "Unable to create account object from serialized form"

    new-array v10, v13, [Ljava/lang/Object;

    invoke-static {v8, v6, v9, v10}, Lcom/android/mail/utils/LogUtils;->e(Ljava/lang/String;Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)I

    goto :goto_3

    :cond_3
    :try_start_2
    sget-object v8, Lcom/android/mail/providers/MailAppProvider;->LOG_TAG:Ljava/lang/String;

    const-string v9, "Dropping account without provider: %s"

    const/4 v10, 0x1

    new-array v10, v10, [Ljava/lang/Object;

    const/4 v11, 0x0

    iget-object v12, v0, Lcom/android/mail/providers/Account;->name:Ljava/lang/String;

    aput-object v12, v10, v11

    invoke-static {v8, v9, v10}, Lcom/android/mail/utils/LogUtils;->e(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)I
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_1

    goto :goto_3

    :cond_4
    invoke-static {}, Lcom/android/mail/providers/MailAppProvider;->broadcastAccountChange()V

    goto :goto_1
.end method

.method private declared-synchronized startAccountsLoader(Landroid/net/Uri;)V
    .locals 8
    .param p1    # Landroid/net/Uri;

    monitor-enter p0

    :try_start_0
    new-instance v0, Landroid/content/CursorLoader;

    invoke-virtual {p0}, Lcom/android/mail/providers/MailAppProvider;->getContext()Landroid/content/Context;

    move-result-object v1

    sget-object v3, Lcom/android/mail/providers/UIProvider;->ACCOUNTS_PROJECTION:[Ljava/lang/String;

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x0

    move-object v2, p1

    invoke-direct/range {v0 .. v6}, Landroid/content/CursorLoader;-><init>(Landroid/content/Context;Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {p1}, Landroid/net/Uri;->hashCode()I

    move-result v1

    invoke-virtual {v0, v1, p0}, Landroid/content/CursorLoader;->registerListener(ILandroid/content/Loader$OnLoadCompleteListener;)V

    invoke-virtual {v0}, Landroid/content/CursorLoader;->startLoading()V

    iget-object v1, p0, Lcom/android/mail/providers/MailAppProvider;->mCursorLoaderMap:Ljava/util/Map;

    invoke-interface {v1, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Landroid/content/CursorLoader;

    if-eqz v7, :cond_0

    invoke-virtual {v7}, Landroid/content/CursorLoader;->stopLoading()V

    :cond_0
    iget-object v1, p0, Lcom/android/mail/providers/MailAppProvider;->mCursorLoaderMap:Ljava/util/Map;

    invoke-interface {v1, p1, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-void

    :catchall_0
    move-exception v1

    monitor-exit p0

    throw v1
.end method


# virtual methods
.method public delete(Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)I
    .locals 1
    .param p1    # Landroid/net/Uri;
    .param p2    # Ljava/lang/String;
    .param p3    # [Ljava/lang/String;

    const/4 v0, 0x0

    return v0
.end method

.method protected abstract getAuthority()Ljava/lang/String;
.end method

.method public getLastSentFromAccount()Ljava/lang/String;
    .locals 3

    invoke-direct {p0}, Lcom/android/mail/providers/MailAppProvider;->getPreferences()Landroid/content/SharedPreferences;

    move-result-object v0

    const-string v1, "lastSendFromAccount"

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getLastViewedAccount()Ljava/lang/String;
    .locals 3

    invoke-direct {p0}, Lcom/android/mail/providers/MailAppProvider;->getPreferences()Landroid/content/SharedPreferences;

    move-result-object v0

    const-string v1, "lastViewedAccount"

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method protected abstract getNoAccountsIntent(Landroid/content/Context;)Landroid/content/Intent;
.end method

.method public getType(Landroid/net/Uri;)Ljava/lang/String;
    .locals 1
    .param p1    # Landroid/net/Uri;

    const/4 v0, 0x0

    return-object v0
.end method

.method public insert(Landroid/net/Uri;Landroid/content/ContentValues;)Landroid/net/Uri;
    .locals 0
    .param p1    # Landroid/net/Uri;
    .param p2    # Landroid/content/ContentValues;

    return-object p1
.end method

.method public onCreate()Z
    .locals 6

    invoke-virtual {p0}, Lcom/android/mail/providers/MailAppProvider;->getAuthority()Ljava/lang/String;

    move-result-object v4

    sput-object v4, Lcom/android/mail/providers/MailAppProvider;->sAuthority:Ljava/lang/String;

    invoke-virtual {p0}, Lcom/android/mail/providers/MailAppProvider;->getContext()Landroid/content/Context;

    move-result-object v4

    invoke-virtual {v4}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v4

    iput-object v4, p0, Lcom/android/mail/providers/MailAppProvider;->mResolver:Landroid/content/ContentResolver;

    new-instance v2, Landroid/content/Intent;

    const-string v4, "com.google.android.gm2.providers.protos.boot.intent.ACTION_PROVIDER_CREATED"

    invoke-direct {v2, v4}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0}, Lcom/android/mail/providers/MailAppProvider;->getContext()Landroid/content/Context;

    move-result-object v4

    invoke-virtual {v4, v2}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    invoke-direct {p0}, Lcom/android/mail/providers/MailAppProvider;->loadCachedAccountList()V

    sget-object v5, Lcom/android/mail/providers/MailAppProvider;->PENDING_ACCOUNT_URIS:Ljava/util/Set;

    monitor-enter v5

    :try_start_0
    sput-object p0, Lcom/android/mail/providers/MailAppProvider;->sInstance:Lcom/android/mail/providers/MailAppProvider;

    sget-object v4, Lcom/android/mail/providers/MailAppProvider;->PENDING_ACCOUNT_URIS:Ljava/util/Set;

    invoke-static {v4}, Lcom/google/common/collect/ImmutableSet;->copyOf(Ljava/util/Collection;)Lcom/google/common/collect/ImmutableSet;

    move-result-object v3

    sget-object v4, Lcom/android/mail/providers/MailAppProvider;->PENDING_ACCOUNT_URIS:Ljava/util/Set;

    invoke-interface {v4}, Ljava/util/Set;->clear()V

    invoke-interface {v3}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/net/Uri;

    invoke-static {v0}, Lcom/android/mail/providers/MailAppProvider;->addAccountsForUriAsync(Landroid/net/Uri;)V

    goto :goto_0

    :catchall_0
    move-exception v4

    monitor-exit v5
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v4

    :cond_0
    :try_start_1
    monitor-exit v5
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    const/4 v4, 0x1

    return v4
.end method

.method public onLoadComplete(Landroid/content/Loader;Landroid/database/Cursor;)V
    .locals 16
    .param p2    # Landroid/database/Cursor;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Loader",
            "<",
            "Landroid/database/Cursor;",
            ">;",
            "Landroid/database/Cursor;",
            ")V"
        }
    .end annotation

    if-nez p2, :cond_0

    sget-object v11, Lcom/android/mail/providers/MailAppProvider;->LOG_TAG:Ljava/lang/String;

    const-string v12, "null account cursor returned"

    const/4 v13, 0x0

    new-array v13, v13, [Ljava/lang/Object;

    invoke-static {v11, v12, v13}, Lcom/android/mail/utils/LogUtils;->d(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)I

    :goto_0
    return-void

    :cond_0
    sget-object v11, Lcom/android/mail/providers/MailAppProvider;->LOG_TAG:Ljava/lang/String;

    const-string v12, "Cursor with %d accounts returned"

    const/4 v13, 0x1

    new-array v13, v13, [Ljava/lang/Object;

    const/4 v14, 0x0

    invoke-interface/range {p2 .. p2}, Landroid/database/Cursor;->getCount()I

    move-result v15

    invoke-static {v15}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v15

    aput-object v15, v13, v14

    invoke-static {v11, v12, v13}, Lcom/android/mail/utils/LogUtils;->d(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)I

    move-object/from16 v5, p1

    check-cast v5, Landroid/content/CursorLoader;

    invoke-virtual {v5}, Landroid/content/CursorLoader;->getUri()Landroid/net/Uri;

    move-result-object v4

    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/android/mail/providers/MailAppProvider;->mAccountCache:Ljava/util/LinkedHashMap;

    monitor-enter v12

    :try_start_0
    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/android/mail/providers/MailAppProvider;->mAccountCache:Ljava/util/LinkedHashMap;

    invoke-virtual {v11}, Ljava/util/LinkedHashMap;->values()Ljava/util/Collection;

    move-result-object v11

    invoke-static {v11}, Lcom/google/common/collect/ImmutableList;->copyOf(Ljava/util/Collection;)Lcom/google/common/collect/ImmutableList;

    move-result-object v2

    monitor-exit v12
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    invoke-static {}, Lcom/google/common/collect/Sets;->newHashSet()Ljava/util/HashSet;

    move-result-object v10

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v8

    :cond_1
    :goto_1
    invoke-interface {v8}, Ljava/util/Iterator;->hasNext()Z

    move-result v11

    if-eqz v11, :cond_2

    invoke-interface {v8}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lcom/android/mail/providers/MailAppProvider$AccountCacheEntry;

    iget-object v11, v6, Lcom/android/mail/providers/MailAppProvider$AccountCacheEntry;->mAccountsQueryUri:Landroid/net/Uri;

    invoke-virtual {v4, v11}, Landroid/net/Uri;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_1

    iget-object v11, v6, Lcom/android/mail/providers/MailAppProvider$AccountCacheEntry;->mAccount:Lcom/android/mail/providers/Account;

    iget-object v11, v11, Lcom/android/mail/providers/Account;->uri:Landroid/net/Uri;

    invoke-interface {v10, v11}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    goto :goto_1

    :catchall_0
    move-exception v11

    :try_start_1
    monitor-exit v12
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v11

    :cond_2
    invoke-interface/range {p2 .. p2}, Landroid/database/Cursor;->getExtras()Landroid/os/Bundle;

    move-result-object v7

    const-string v11, "accounts_loaded"

    invoke-virtual {v7, v11}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v11

    if-eqz v11, :cond_4

    const/4 v11, 0x1

    :goto_2
    move-object/from16 v0, p0

    iput-boolean v11, v0, Lcom/android/mail/providers/MailAppProvider;->mAccountsFullyLoaded:Z

    invoke-static {}, Lcom/google/common/collect/Sets;->newHashSet()Ljava/util/HashSet;

    move-result-object v9

    :goto_3
    invoke-interface/range {p2 .. p2}, Landroid/database/Cursor;->moveToNext()Z

    move-result v11

    if-eqz v11, :cond_5

    new-instance v1, Lcom/android/mail/providers/Account;

    move-object/from16 v0, p2

    invoke-direct {v1, v0}, Lcom/android/mail/providers/Account;-><init>(Landroid/database/Cursor;)V

    iget-object v3, v1, Lcom/android/mail/providers/Account;->uri:Landroid/net/Uri;

    invoke-interface {v9, v3}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    move-object/from16 v0, p0

    iget-boolean v11, v0, Lcom/android/mail/providers/MailAppProvider;->mAccountsFullyLoaded:Z

    if-eqz v11, :cond_3

    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/android/mail/providers/MailAppProvider;->mAccountCache:Ljava/util/LinkedHashMap;

    monitor-enter v12

    :try_start_2
    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/android/mail/providers/MailAppProvider;->mAccountCache:Ljava/util/LinkedHashMap;

    invoke-virtual {v11, v3}, Ljava/util/LinkedHashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    monitor-exit v12
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    :cond_3
    const/4 v11, 0x0

    move-object/from16 v0, p0

    invoke-direct {v0, v1, v4, v11}, Lcom/android/mail/providers/MailAppProvider;->addAccountImpl(Lcom/android/mail/providers/Account;Landroid/net/Uri;Z)V

    goto :goto_3

    :cond_4
    const/4 v11, 0x0

    goto :goto_2

    :catchall_1
    move-exception v11

    :try_start_3
    monitor-exit v12
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v11

    :cond_5
    invoke-interface {v10, v9}, Ljava/util/Set;->removeAll(Ljava/util/Collection;)Z

    invoke-interface {v10}, Ljava/util/Set;->size()I

    move-result v11

    if-lez v11, :cond_7

    move-object/from16 v0, p0

    iget-boolean v11, v0, Lcom/android/mail/providers/MailAppProvider;->mAccountsFullyLoaded:Z

    if-eqz v11, :cond_7

    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/android/mail/providers/MailAppProvider;->mAccountCache:Ljava/util/LinkedHashMap;

    monitor-enter v12

    :try_start_4
    invoke-interface {v10}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v8

    :goto_4
    invoke-interface {v8}, Ljava/util/Iterator;->hasNext()Z

    move-result v11

    if-eqz v11, :cond_6

    invoke-interface {v8}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/net/Uri;

    sget-object v11, Lcom/android/mail/providers/MailAppProvider;->LOG_TAG:Ljava/lang/String;

    const-string v13, "Removing account %s"

    const/4 v14, 0x1

    new-array v14, v14, [Ljava/lang/Object;

    const/4 v15, 0x0

    aput-object v3, v14, v15

    invoke-static {v11, v13, v14}, Lcom/android/mail/utils/LogUtils;->d(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)I

    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/android/mail/providers/MailAppProvider;->mAccountCache:Ljava/util/LinkedHashMap;

    invoke-virtual {v11, v3}, Ljava/util/LinkedHashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_4

    :catchall_2
    move-exception v11

    monitor-exit v12
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_2

    throw v11

    :cond_6
    :try_start_5
    monitor-exit v12
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_2

    :cond_7
    invoke-static {}, Lcom/android/mail/providers/MailAppProvider;->broadcastAccountChange()V

    invoke-direct/range {p0 .. p0}, Lcom/android/mail/providers/MailAppProvider;->cacheAccountList()V

    goto/16 :goto_0
.end method

.method public bridge synthetic onLoadComplete(Landroid/content/Loader;Ljava/lang/Object;)V
    .locals 0
    .param p1    # Landroid/content/Loader;
    .param p2    # Ljava/lang/Object;

    check-cast p2, Landroid/database/Cursor;

    invoke-virtual {p0, p1, p2}, Lcom/android/mail/providers/MailAppProvider;->onLoadComplete(Landroid/content/Loader;Landroid/database/Cursor;)V

    return-void
.end method

.method public query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    .locals 17
    .param p1    # Landroid/net/Uri;
    .param p2    # [Ljava/lang/String;
    .param p3    # Ljava/lang/String;
    .param p4    # [Ljava/lang/String;
    .param p5    # Ljava/lang/String;

    invoke-static/range {p2 .. p2}, Lcom/android/mail/providers/UIProviderValidator;->validateAccountProjection([Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v13

    new-instance v9, Landroid/os/Bundle;

    invoke-direct {v9}, Landroid/os/Bundle;-><init>()V

    const-string v15, "accounts_loaded"

    move-object/from16 v0, p0

    iget-boolean v14, v0, Lcom/android/mail/providers/MailAppProvider;->mAccountsFullyLoaded:Z

    if-eqz v14, :cond_1

    const/4 v14, 0x1

    :goto_0
    invoke-virtual {v9, v15, v14}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/android/mail/providers/MailAppProvider;->mAccountCache:Ljava/util/LinkedHashMap;

    monitor-enter v15

    :try_start_0
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/android/mail/providers/MailAppProvider;->mAccountCache:Ljava/util/LinkedHashMap;

    invoke-virtual {v14}, Ljava/util/LinkedHashMap;->values()Ljava/util/Collection;

    move-result-object v14

    invoke-static {v14}, Lcom/google/common/collect/ImmutableList;->copyOf(Ljava/util/Collection;)Lcom/google/common/collect/ImmutableList;

    move-result-object v3

    monitor-exit v15
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    new-instance v8, Lcom/android/mail/utils/MatrixCursorWithExtra;

    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v14

    invoke-direct {v8, v13, v14, v9}, Lcom/android/mail/utils/MatrixCursorWithExtra;-><init>([Ljava/lang/String;ILandroid/os/Bundle;)V

    invoke-interface {v3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v10

    :cond_0
    invoke-interface {v10}, Ljava/util/Iterator;->hasNext()Z

    move-result v14

    if-eqz v14, :cond_8

    invoke-interface {v10}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/android/mail/providers/MailAppProvider$AccountCacheEntry;

    iget-object v1, v2, Lcom/android/mail/providers/MailAppProvider$AccountCacheEntry;->mAccount:Lcom/android/mail/providers/Account;

    invoke-virtual {v8}, Landroid/database/MatrixCursor;->newRow()Landroid/database/MatrixCursor$RowBuilder;

    move-result-object v5

    move-object v4, v13

    array-length v12, v4

    const/4 v11, 0x0

    :goto_1
    if-ge v11, v12, :cond_0

    aget-object v7, v4, v11

    invoke-static {v7}, Lcom/android/mail/providers/UIProvider;->getAccountColumn(Ljava/lang/String;)I

    move-result v6

    packed-switch v6, :pswitch_data_0

    new-instance v14, Ljava/lang/IllegalStateException;

    new-instance v15, Ljava/lang/StringBuilder;

    invoke-direct {v15}, Ljava/lang/StringBuilder;-><init>()V

    const-string v16, "Column not found: "

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    invoke-virtual {v15, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    invoke-virtual {v15}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v15

    invoke-direct {v14, v15}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v14

    :cond_1
    const/4 v14, 0x0

    goto :goto_0

    :catchall_0
    move-exception v14

    :try_start_1
    monitor-exit v15
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v14

    :pswitch_0
    const/4 v14, 0x0

    invoke-static {v14}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v14

    invoke-virtual {v5, v14}, Landroid/database/MatrixCursor$RowBuilder;->add(Ljava/lang/Object;)Landroid/database/MatrixCursor$RowBuilder;

    :goto_2
    add-int/lit8 v11, v11, 0x1

    goto :goto_1

    :pswitch_1
    iget-object v14, v1, Lcom/android/mail/providers/Account;->name:Ljava/lang/String;

    invoke-virtual {v5, v14}, Landroid/database/MatrixCursor$RowBuilder;->add(Ljava/lang/Object;)Landroid/database/MatrixCursor$RowBuilder;

    goto :goto_2

    :pswitch_2
    iget v14, v1, Lcom/android/mail/providers/Account;->providerVersion:I

    invoke-static {v14}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v14

    invoke-virtual {v5, v14}, Landroid/database/MatrixCursor$RowBuilder;->add(Ljava/lang/Object;)Landroid/database/MatrixCursor$RowBuilder;

    goto :goto_2

    :pswitch_3
    iget-object v14, v1, Lcom/android/mail/providers/Account;->uri:Landroid/net/Uri;

    invoke-virtual {v5, v14}, Landroid/database/MatrixCursor$RowBuilder;->add(Ljava/lang/Object;)Landroid/database/MatrixCursor$RowBuilder;

    goto :goto_2

    :pswitch_4
    iget v14, v1, Lcom/android/mail/providers/Account;->capabilities:I

    invoke-static {v14}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v14

    invoke-virtual {v5, v14}, Landroid/database/MatrixCursor$RowBuilder;->add(Ljava/lang/Object;)Landroid/database/MatrixCursor$RowBuilder;

    goto :goto_2

    :pswitch_5
    iget-object v14, v1, Lcom/android/mail/providers/Account;->folderListUri:Landroid/net/Uri;

    invoke-virtual {v5, v14}, Landroid/database/MatrixCursor$RowBuilder;->add(Ljava/lang/Object;)Landroid/database/MatrixCursor$RowBuilder;

    goto :goto_2

    :pswitch_6
    iget-object v14, v1, Lcom/android/mail/providers/Account;->fullFolderListUri:Landroid/net/Uri;

    invoke-virtual {v5, v14}, Landroid/database/MatrixCursor$RowBuilder;->add(Ljava/lang/Object;)Landroid/database/MatrixCursor$RowBuilder;

    goto :goto_2

    :pswitch_7
    iget-object v14, v1, Lcom/android/mail/providers/Account;->searchUri:Landroid/net/Uri;

    invoke-virtual {v5, v14}, Landroid/database/MatrixCursor$RowBuilder;->add(Ljava/lang/Object;)Landroid/database/MatrixCursor$RowBuilder;

    goto :goto_2

    :pswitch_8
    iget-object v14, v1, Lcom/android/mail/providers/Account;->accountFromAddresses:Ljava/lang/String;

    invoke-virtual {v5, v14}, Landroid/database/MatrixCursor$RowBuilder;->add(Ljava/lang/Object;)Landroid/database/MatrixCursor$RowBuilder;

    goto :goto_2

    :pswitch_9
    iget-object v14, v1, Lcom/android/mail/providers/Account;->saveDraftUri:Landroid/net/Uri;

    invoke-virtual {v5, v14}, Landroid/database/MatrixCursor$RowBuilder;->add(Ljava/lang/Object;)Landroid/database/MatrixCursor$RowBuilder;

    goto :goto_2

    :pswitch_a
    iget-object v14, v1, Lcom/android/mail/providers/Account;->sendMessageUri:Landroid/net/Uri;

    invoke-virtual {v5, v14}, Landroid/database/MatrixCursor$RowBuilder;->add(Ljava/lang/Object;)Landroid/database/MatrixCursor$RowBuilder;

    goto :goto_2

    :pswitch_b
    iget-object v14, v1, Lcom/android/mail/providers/Account;->expungeMessageUri:Landroid/net/Uri;

    invoke-virtual {v5, v14}, Landroid/database/MatrixCursor$RowBuilder;->add(Ljava/lang/Object;)Landroid/database/MatrixCursor$RowBuilder;

    goto :goto_2

    :pswitch_c
    iget-object v14, v1, Lcom/android/mail/providers/Account;->undoUri:Landroid/net/Uri;

    invoke-virtual {v5, v14}, Landroid/database/MatrixCursor$RowBuilder;->add(Ljava/lang/Object;)Landroid/database/MatrixCursor$RowBuilder;

    goto :goto_2

    :pswitch_d
    iget-object v14, v1, Lcom/android/mail/providers/Account;->settingsIntentUri:Landroid/net/Uri;

    invoke-virtual {v5, v14}, Landroid/database/MatrixCursor$RowBuilder;->add(Ljava/lang/Object;)Landroid/database/MatrixCursor$RowBuilder;

    goto :goto_2

    :pswitch_e
    iget-object v14, v1, Lcom/android/mail/providers/Account;->helpIntentUri:Landroid/net/Uri;

    invoke-virtual {v5, v14}, Landroid/database/MatrixCursor$RowBuilder;->add(Ljava/lang/Object;)Landroid/database/MatrixCursor$RowBuilder;

    goto :goto_2

    :pswitch_f
    iget-object v14, v1, Lcom/android/mail/providers/Account;->sendFeedbackIntentUri:Landroid/net/Uri;

    invoke-virtual {v5, v14}, Landroid/database/MatrixCursor$RowBuilder;->add(Ljava/lang/Object;)Landroid/database/MatrixCursor$RowBuilder;

    goto :goto_2

    :pswitch_10
    iget-object v14, v1, Lcom/android/mail/providers/Account;->reauthenticationIntentUri:Landroid/net/Uri;

    invoke-virtual {v5, v14}, Landroid/database/MatrixCursor$RowBuilder;->add(Ljava/lang/Object;)Landroid/database/MatrixCursor$RowBuilder;

    goto :goto_2

    :pswitch_11
    iget v14, v1, Lcom/android/mail/providers/Account;->syncStatus:I

    invoke-static {v14}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v14

    invoke-virtual {v5, v14}, Landroid/database/MatrixCursor$RowBuilder;->add(Ljava/lang/Object;)Landroid/database/MatrixCursor$RowBuilder;

    goto :goto_2

    :pswitch_12
    iget-object v14, v1, Lcom/android/mail/providers/Account;->composeIntentUri:Landroid/net/Uri;

    invoke-virtual {v5, v14}, Landroid/database/MatrixCursor$RowBuilder;->add(Ljava/lang/Object;)Landroid/database/MatrixCursor$RowBuilder;

    goto :goto_2

    :pswitch_13
    iget-object v14, v1, Lcom/android/mail/providers/Account;->mimeType:Ljava/lang/String;

    invoke-virtual {v5, v14}, Landroid/database/MatrixCursor$RowBuilder;->add(Ljava/lang/Object;)Landroid/database/MatrixCursor$RowBuilder;

    goto :goto_2

    :pswitch_14
    iget-object v14, v1, Lcom/android/mail/providers/Account;->recentFolderListUri:Landroid/net/Uri;

    invoke-virtual {v5, v14}, Landroid/database/MatrixCursor$RowBuilder;->add(Ljava/lang/Object;)Landroid/database/MatrixCursor$RowBuilder;

    goto/16 :goto_2

    :pswitch_15
    iget-object v14, v1, Lcom/android/mail/providers/Account;->defaultRecentFolderListUri:Landroid/net/Uri;

    invoke-virtual {v5, v14}, Landroid/database/MatrixCursor$RowBuilder;->add(Ljava/lang/Object;)Landroid/database/MatrixCursor$RowBuilder;

    goto/16 :goto_2

    :pswitch_16
    iget-object v14, v1, Lcom/android/mail/providers/Account;->manualSyncUri:Landroid/net/Uri;

    invoke-virtual {v5, v14}, Landroid/database/MatrixCursor$RowBuilder;->add(Ljava/lang/Object;)Landroid/database/MatrixCursor$RowBuilder;

    goto/16 :goto_2

    :pswitch_17
    iget-object v14, v1, Lcom/android/mail/providers/Account;->viewIntentProxyUri:Landroid/net/Uri;

    invoke-virtual {v5, v14}, Landroid/database/MatrixCursor$RowBuilder;->add(Ljava/lang/Object;)Landroid/database/MatrixCursor$RowBuilder;

    goto/16 :goto_2

    :pswitch_18
    iget-object v14, v1, Lcom/android/mail/providers/Account;->accoutCookieQueryUri:Landroid/net/Uri;

    invoke-virtual {v5, v14}, Landroid/database/MatrixCursor$RowBuilder;->add(Ljava/lang/Object;)Landroid/database/MatrixCursor$RowBuilder;

    goto/16 :goto_2

    :pswitch_19
    iget v14, v1, Lcom/android/mail/providers/Account;->color:I

    invoke-static {v14}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v14

    invoke-virtual {v5, v14}, Landroid/database/MatrixCursor$RowBuilder;->add(Ljava/lang/Object;)Landroid/database/MatrixCursor$RowBuilder;

    goto/16 :goto_2

    :pswitch_1a
    iget-object v14, v1, Lcom/android/mail/providers/Account;->settings:Lcom/android/mail/providers/Settings;

    iget-object v14, v14, Lcom/android/mail/providers/Settings;->signature:Ljava/lang/String;

    invoke-virtual {v5, v14}, Landroid/database/MatrixCursor$RowBuilder;->add(Ljava/lang/Object;)Landroid/database/MatrixCursor$RowBuilder;

    goto/16 :goto_2

    :pswitch_1b
    iget-object v14, v1, Lcom/android/mail/providers/Account;->settings:Lcom/android/mail/providers/Settings;

    invoke-virtual {v14}, Lcom/android/mail/providers/Settings;->getAutoAdvanceSetting()I

    move-result v14

    invoke-static {v14}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v14

    invoke-virtual {v5, v14}, Landroid/database/MatrixCursor$RowBuilder;->add(Ljava/lang/Object;)Landroid/database/MatrixCursor$RowBuilder;

    goto/16 :goto_2

    :pswitch_1c
    iget-object v14, v1, Lcom/android/mail/providers/Account;->settings:Lcom/android/mail/providers/Settings;

    iget v14, v14, Lcom/android/mail/providers/Settings;->messageTextSize:I

    invoke-static {v14}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v14

    invoke-virtual {v5, v14}, Landroid/database/MatrixCursor$RowBuilder;->add(Ljava/lang/Object;)Landroid/database/MatrixCursor$RowBuilder;

    goto/16 :goto_2

    :pswitch_1d
    iget-object v14, v1, Lcom/android/mail/providers/Account;->settings:Lcom/android/mail/providers/Settings;

    iget v14, v14, Lcom/android/mail/providers/Settings;->replyBehavior:I

    invoke-static {v14}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v14

    invoke-virtual {v5, v14}, Landroid/database/MatrixCursor$RowBuilder;->add(Ljava/lang/Object;)Landroid/database/MatrixCursor$RowBuilder;

    goto/16 :goto_2

    :pswitch_1e
    iget-object v14, v1, Lcom/android/mail/providers/Account;->settings:Lcom/android/mail/providers/Settings;

    iget-boolean v14, v14, Lcom/android/mail/providers/Settings;->hideCheckboxes:Z

    if-eqz v14, :cond_2

    const/4 v14, 0x1

    :goto_3
    invoke-static {v14}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v14

    invoke-virtual {v5, v14}, Landroid/database/MatrixCursor$RowBuilder;->add(Ljava/lang/Object;)Landroid/database/MatrixCursor$RowBuilder;

    goto/16 :goto_2

    :cond_2
    const/4 v14, 0x0

    goto :goto_3

    :pswitch_1f
    iget-object v14, v1, Lcom/android/mail/providers/Account;->settings:Lcom/android/mail/providers/Settings;

    iget-boolean v14, v14, Lcom/android/mail/providers/Settings;->confirmDelete:Z

    if-eqz v14, :cond_3

    const/4 v14, 0x1

    :goto_4
    invoke-static {v14}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v14

    invoke-virtual {v5, v14}, Landroid/database/MatrixCursor$RowBuilder;->add(Ljava/lang/Object;)Landroid/database/MatrixCursor$RowBuilder;

    goto/16 :goto_2

    :cond_3
    const/4 v14, 0x0

    goto :goto_4

    :pswitch_20
    iget-object v14, v1, Lcom/android/mail/providers/Account;->settings:Lcom/android/mail/providers/Settings;

    iget-boolean v14, v14, Lcom/android/mail/providers/Settings;->confirmArchive:Z

    if-eqz v14, :cond_4

    const/4 v14, 0x1

    :goto_5
    invoke-static {v14}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v14

    invoke-virtual {v5, v14}, Landroid/database/MatrixCursor$RowBuilder;->add(Ljava/lang/Object;)Landroid/database/MatrixCursor$RowBuilder;

    goto/16 :goto_2

    :cond_4
    const/4 v14, 0x0

    goto :goto_5

    :pswitch_21
    iget-object v14, v1, Lcom/android/mail/providers/Account;->settings:Lcom/android/mail/providers/Settings;

    iget-boolean v14, v14, Lcom/android/mail/providers/Settings;->confirmSend:Z

    if-eqz v14, :cond_5

    const/4 v14, 0x1

    :goto_6
    invoke-static {v14}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v14

    invoke-virtual {v5, v14}, Landroid/database/MatrixCursor$RowBuilder;->add(Ljava/lang/Object;)Landroid/database/MatrixCursor$RowBuilder;

    goto/16 :goto_2

    :cond_5
    const/4 v14, 0x0

    goto :goto_6

    :pswitch_22
    iget-object v14, v1, Lcom/android/mail/providers/Account;->settings:Lcom/android/mail/providers/Settings;

    iget-object v14, v14, Lcom/android/mail/providers/Settings;->defaultInbox:Landroid/net/Uri;

    invoke-virtual {v5, v14}, Landroid/database/MatrixCursor$RowBuilder;->add(Ljava/lang/Object;)Landroid/database/MatrixCursor$RowBuilder;

    goto/16 :goto_2

    :pswitch_23
    iget-object v14, v1, Lcom/android/mail/providers/Account;->settings:Lcom/android/mail/providers/Settings;

    iget-object v14, v14, Lcom/android/mail/providers/Settings;->defaultInboxName:Ljava/lang/String;

    invoke-virtual {v5, v14}, Landroid/database/MatrixCursor$RowBuilder;->add(Ljava/lang/Object;)Landroid/database/MatrixCursor$RowBuilder;

    goto/16 :goto_2

    :pswitch_24
    iget-object v14, v1, Lcom/android/mail/providers/Account;->settings:Lcom/android/mail/providers/Settings;

    iget v14, v14, Lcom/android/mail/providers/Settings;->snapHeaders:I

    invoke-static {v14}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v14

    invoke-virtual {v5, v14}, Landroid/database/MatrixCursor$RowBuilder;->add(Ljava/lang/Object;)Landroid/database/MatrixCursor$RowBuilder;

    goto/16 :goto_2

    :pswitch_25
    iget-object v14, v1, Lcom/android/mail/providers/Account;->settings:Lcom/android/mail/providers/Settings;

    iget-boolean v14, v14, Lcom/android/mail/providers/Settings;->forceReplyFromDefault:Z

    if-eqz v14, :cond_6

    const/4 v14, 0x1

    :goto_7
    invoke-static {v14}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v14

    invoke-virtual {v5, v14}, Landroid/database/MatrixCursor$RowBuilder;->add(Ljava/lang/Object;)Landroid/database/MatrixCursor$RowBuilder;

    goto/16 :goto_2

    :cond_6
    const/4 v14, 0x0

    goto :goto_7

    :pswitch_26
    iget-object v14, v1, Lcom/android/mail/providers/Account;->settings:Lcom/android/mail/providers/Settings;

    iget v14, v14, Lcom/android/mail/providers/Settings;->maxAttachmentSize:I

    invoke-static {v14}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v14

    invoke-virtual {v5, v14}, Landroid/database/MatrixCursor$RowBuilder;->add(Ljava/lang/Object;)Landroid/database/MatrixCursor$RowBuilder;

    goto/16 :goto_2

    :pswitch_27
    iget-object v14, v1, Lcom/android/mail/providers/Account;->settings:Lcom/android/mail/providers/Settings;

    iget v14, v14, Lcom/android/mail/providers/Settings;->swipe:I

    invoke-static {v14}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v14

    invoke-virtual {v5, v14}, Landroid/database/MatrixCursor$RowBuilder;->add(Ljava/lang/Object;)Landroid/database/MatrixCursor$RowBuilder;

    goto/16 :goto_2

    :pswitch_28
    iget-object v14, v1, Lcom/android/mail/providers/Account;->settings:Lcom/android/mail/providers/Settings;

    iget-boolean v14, v14, Lcom/android/mail/providers/Settings;->priorityArrowsEnabled:Z

    if-eqz v14, :cond_7

    const/4 v14, 0x1

    :goto_8
    invoke-static {v14}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v14

    invoke-virtual {v5, v14}, Landroid/database/MatrixCursor$RowBuilder;->add(Ljava/lang/Object;)Landroid/database/MatrixCursor$RowBuilder;

    goto/16 :goto_2

    :cond_7
    const/4 v14, 0x0

    goto :goto_8

    :pswitch_29
    iget-object v14, v1, Lcom/android/mail/providers/Account;->settings:Lcom/android/mail/providers/Settings;

    iget-object v14, v14, Lcom/android/mail/providers/Settings;->setupIntentUri:Landroid/net/Uri;

    invoke-virtual {v5, v14}, Landroid/database/MatrixCursor$RowBuilder;->add(Ljava/lang/Object;)Landroid/database/MatrixCursor$RowBuilder;

    goto/16 :goto_2

    :pswitch_2a
    iget-object v14, v1, Lcom/android/mail/providers/Account;->settings:Lcom/android/mail/providers/Settings;

    iget v14, v14, Lcom/android/mail/providers/Settings;->conversationViewMode:I

    invoke-static {v14}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v14

    invoke-virtual {v5, v14}, Landroid/database/MatrixCursor$RowBuilder;->add(Ljava/lang/Object;)Landroid/database/MatrixCursor$RowBuilder;

    goto/16 :goto_2

    :pswitch_2b
    iget-object v14, v1, Lcom/android/mail/providers/Account;->settings:Lcom/android/mail/providers/Settings;

    iget-object v14, v14, Lcom/android/mail/providers/Settings;->veiledAddressPattern:Ljava/lang/String;

    invoke-virtual {v5, v14}, Landroid/database/MatrixCursor$RowBuilder;->add(Ljava/lang/Object;)Landroid/database/MatrixCursor$RowBuilder;

    goto/16 :goto_2

    :pswitch_2c
    iget-object v14, v1, Lcom/android/mail/providers/Account;->updateSettingsUri:Landroid/net/Uri;

    invoke-virtual {v5, v14}, Landroid/database/MatrixCursor$RowBuilder;->add(Ljava/lang/Object;)Landroid/database/MatrixCursor$RowBuilder;

    goto/16 :goto_2

    :cond_8
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/android/mail/providers/MailAppProvider;->mResolver:Landroid/content/ContentResolver;

    invoke-static {}, Lcom/android/mail/providers/MailAppProvider;->getAccountsUri()Landroid/net/Uri;

    move-result-object v15

    invoke-virtual {v8, v14, v15}, Landroid/database/MatrixCursor;->setNotificationUri(Landroid/content/ContentResolver;Landroid/net/Uri;)V

    return-object v8

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_7
        :pswitch_8
        :pswitch_9
        :pswitch_a
        :pswitch_b
        :pswitch_c
        :pswitch_d
        :pswitch_11
        :pswitch_e
        :pswitch_f
        :pswitch_10
        :pswitch_12
        :pswitch_13
        :pswitch_14
        :pswitch_19
        :pswitch_15
        :pswitch_16
        :pswitch_17
        :pswitch_18
        :pswitch_1a
        :pswitch_1b
        :pswitch_1c
        :pswitch_24
        :pswitch_1d
        :pswitch_1e
        :pswitch_1f
        :pswitch_20
        :pswitch_21
        :pswitch_22
        :pswitch_23
        :pswitch_25
        :pswitch_26
        :pswitch_27
        :pswitch_28
        :pswitch_29
        :pswitch_2a
        :pswitch_2b
        :pswitch_2c
    .end packed-switch
.end method

.method public setLastSentFromAccount(Ljava/lang/String;)V
    .locals 2
    .param p1    # Ljava/lang/String;

    invoke-direct {p0}, Lcom/android/mail/providers/MailAppProvider;->getPreferences()Landroid/content/SharedPreferences;

    move-result-object v1

    invoke-interface {v1}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string v1, "lastSendFromAccount"

    invoke-interface {v0, v1, p1}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->apply()V

    return-void
.end method

.method public setLastViewedAccount(Ljava/lang/String;)V
    .locals 2
    .param p1    # Ljava/lang/String;

    invoke-direct {p0}, Lcom/android/mail/providers/MailAppProvider;->getPreferences()Landroid/content/SharedPreferences;

    move-result-object v1

    invoke-interface {v1}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string v1, "lastViewedAccount"

    invoke-interface {v0, v1, p1}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->apply()V

    return-void
.end method

.method public shutdown()V
    .locals 4

    sget-object v3, Lcom/android/mail/providers/MailAppProvider;->PENDING_ACCOUNT_URIS:Ljava/util/Set;

    monitor-enter v3

    const/4 v2, 0x0

    :try_start_0
    sput-object v2, Lcom/android/mail/providers/MailAppProvider;->sInstance:Lcom/android/mail/providers/MailAppProvider;

    monitor-exit v3
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    iget-object v2, p0, Lcom/android/mail/providers/MailAppProvider;->mCursorLoaderMap:Ljava/util/Map;

    invoke-interface {v2}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/content/CursorLoader;

    invoke-virtual {v1}, Landroid/content/CursorLoader;->stopLoading()V

    goto :goto_0

    :catchall_0
    move-exception v2

    :try_start_1
    monitor-exit v3
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v2

    :cond_0
    iget-object v2, p0, Lcom/android/mail/providers/MailAppProvider;->mCursorLoaderMap:Ljava/util/Map;

    invoke-interface {v2}, Ljava/util/Map;->clear()V

    return-void
.end method

.method public update(Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I
    .locals 1
    .param p1    # Landroid/net/Uri;
    .param p2    # Landroid/content/ContentValues;
    .param p3    # Ljava/lang/String;
    .param p4    # [Ljava/lang/String;

    const/4 v0, 0x0

    return v0
.end method
