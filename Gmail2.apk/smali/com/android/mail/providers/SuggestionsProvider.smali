.class public Lcom/android/mail/providers/SuggestionsProvider;
.super Lcom/android/mail/providers/SearchRecentSuggestionsProvider;
.source "SuggestionsProvider.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/mail/providers/SuggestionsProvider$ContactsCursor;
    }
.end annotation


# static fields
.field private static final CONTACTS_COLUMNS:[Ljava/lang/String;

.field private static final sContract:[Ljava/lang/String;


# instance fields
.field private mFullQueryTerms:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private final mTermsLock:Ljava/lang/Object;


# direct methods
.method static constructor <clinit>()V
    .locals 6

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    const/4 v0, 0x4

    new-array v0, v0, [Ljava/lang/String;

    const-string v1, "_id"

    aput-object v1, v0, v3

    const-string v1, "suggest_text_1"

    aput-object v1, v0, v4

    const-string v1, "suggest_intent_query"

    aput-object v1, v0, v5

    const/4 v1, 0x3

    const-string v2, "suggest_icon_1"

    aput-object v2, v0, v1

    sput-object v0, Lcom/android/mail/providers/SuggestionsProvider;->CONTACTS_COLUMNS:[Ljava/lang/String;

    new-array v0, v5, [Ljava/lang/String;

    const-string v1, "data4"

    aput-object v1, v0, v3

    const-string v1, "data1"

    aput-object v1, v0, v4

    sput-object v0, Lcom/android/mail/providers/SuggestionsProvider;->sContract:[Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Lcom/android/mail/providers/SearchRecentSuggestionsProvider;-><init>()V

    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lcom/android/mail/providers/SuggestionsProvider;->mTermsLock:Ljava/lang/Object;

    return-void
.end method

.method static synthetic access$000()[Ljava/lang/String;
    .locals 1

    sget-object v0, Lcom/android/mail/providers/SuggestionsProvider;->CONTACTS_COLUMNS:[Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$100()[Ljava/lang/String;
    .locals 1

    sget-object v0, Lcom/android/mail/providers/SuggestionsProvider;->sContract:[Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$200(Lcom/android/mail/providers/SuggestionsProvider;Ljava/lang/String;)Ljava/lang/String;
    .locals 1
    .param p0    # Lcom/android/mail/providers/SuggestionsProvider;
    .param p1    # Ljava/lang/String;

    invoke-direct {p0, p1}, Lcom/android/mail/providers/SuggestionsProvider;->createQuery(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private createQuery(Ljava/lang/String;)Ljava/lang/String;
    .locals 6
    .param p1    # Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v3, p0, Lcom/android/mail/providers/SuggestionsProvider;->mFullQueryTerms:Ljava/util/ArrayList;

    if-eqz v3, :cond_1

    iget-object v4, p0, Lcom/android/mail/providers/SuggestionsProvider;->mTermsLock:Ljava/lang/Object;

    monitor-enter v4

    const/4 v0, 0x0

    :try_start_0
    iget-object v3, p0, Lcom/android/mail/providers/SuggestionsProvider;->mFullQueryTerms:Ljava/util/ArrayList;

    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    move-result v2

    :goto_0
    if-ge v0, v2, :cond_0

    iget-object v3, p0, Lcom/android/mail/providers/SuggestionsProvider;->mFullQueryTerms:Ljava/util/ArrayList;

    invoke-virtual {v3, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v5, " "

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_0
    monitor-exit v4
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :cond_1
    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    return-object v3

    :catchall_0
    move-exception v3

    :try_start_1
    monitor-exit v4
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v3
.end method


# virtual methods
.method public onCreate()Z
    .locals 4

    const/4 v3, 0x1

    invoke-virtual {p0}, Lcom/android/mail/providers/SuggestionsProvider;->getContext()Landroid/content/Context;

    move-result-object v1

    const v2, 0x7f090023

    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0, v3}, Lcom/android/mail/providers/SuggestionsProvider;->setupSuggestions(Ljava/lang/String;I)V

    invoke-super {p0}, Lcom/android/mail/providers/SearchRecentSuggestionsProvider;->onCreate()Z

    return v3
.end method

.method public query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    .locals 12
    .param p1    # Landroid/net/Uri;
    .param p2    # [Ljava/lang/String;
    .param p3    # Ljava/lang/String;
    .param p4    # [Ljava/lang/String;
    .param p5    # Ljava/lang/String;

    const/4 v0, 0x0

    aget-object v9, p4, v0

    const/4 v8, 0x0

    iget-object v1, p0, Lcom/android/mail/providers/SuggestionsProvider;->mTermsLock:Ljava/lang/Object;

    monitor-enter v1

    const/4 v0, 0x0

    :try_start_0
    iput-object v0, p0, Lcom/android/mail/providers/SuggestionsProvider;->mFullQueryTerms:Ljava/util/ArrayList;

    iget-object v0, p0, Lcom/android/mail/providers/SuggestionsProvider;->mFullQueryTerms:Ljava/util/ArrayList;

    invoke-super {p0, v0}, Lcom/android/mail/providers/SearchRecentSuggestionsProvider;->setFullQueryTerms(Ljava/util/ArrayList;)V

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-eqz v9, :cond_2

    const-string v0, " "

    invoke-static {v9, v0}, Landroid/text/TextUtils;->split(Ljava/lang/String;Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v11

    if-eqz v11, :cond_3

    array-length v0, v11

    const/4 v1, 0x1

    if-le v0, v1, :cond_3

    array-length v0, v11

    add-int/lit8 v0, v0, -0x1

    aget-object v9, v11, v0

    iget-object v1, p0, Lcom/android/mail/providers/SuggestionsProvider;->mTermsLock:Ljava/lang/Object;

    monitor-enter v1

    :try_start_1
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/android/mail/providers/SuggestionsProvider;->mFullQueryTerms:Ljava/util/ArrayList;

    const/4 v7, 0x0

    array-length v0, v11

    add-int/lit8 v10, v0, -0x1

    :goto_0
    if-ge v7, v10, :cond_0

    iget-object v0, p0, Lcom/android/mail/providers/SuggestionsProvider;->mFullQueryTerms:Ljava/util/ArrayList;

    aget-object v2, v11, v7

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    add-int/lit8 v7, v7, 0x1

    goto :goto_0

    :catchall_0
    move-exception v0

    :try_start_2
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    throw v0

    :cond_0
    :try_start_3
    iget-object v0, p0, Lcom/android/mail/providers/SuggestionsProvider;->mFullQueryTerms:Ljava/util/ArrayList;

    invoke-super {p0, v0}, Lcom/android/mail/providers/SearchRecentSuggestionsProvider;->setFullQueryTerms(Ljava/util/ArrayList;)V

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    :goto_1
    new-instance v6, Ljava/util/ArrayList;

    invoke-direct {v6}, Ljava/util/ArrayList;-><init>()V

    const/4 v0, 0x1

    new-array v4, v0, [Ljava/lang/String;

    const/4 v0, 0x0

    aput-object v9, v4, v0

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object/from16 v5, p5

    invoke-super/range {v0 .. v5}, Lcom/android/mail/providers/SearchRecentSuggestionsProvider;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v0

    invoke-virtual {v6, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    invoke-virtual {v9}, Ljava/lang/String;->length()I

    move-result v0

    const/4 v1, 0x2

    if-lt v0, v1, :cond_1

    new-instance v0, Lcom/android/mail/providers/SuggestionsProvider$ContactsCursor;

    invoke-direct {v0, p0}, Lcom/android/mail/providers/SuggestionsProvider$ContactsCursor;-><init>(Lcom/android/mail/providers/SuggestionsProvider;)V

    invoke-virtual {v0, v9}, Lcom/android/mail/providers/SuggestionsProvider$ContactsCursor;->query(Ljava/lang/String;)Lcom/android/mail/providers/SuggestionsProvider$ContactsCursor;

    move-result-object v0

    invoke-virtual {v6, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :cond_1
    new-instance v8, Landroid/database/MergeCursor;

    invoke-virtual {v6}, Ljava/util/ArrayList;->size()I

    move-result v0

    new-array v0, v0, [Landroid/database/Cursor;

    invoke-virtual {v6, v0}, Ljava/util/ArrayList;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Landroid/database/Cursor;

    invoke-direct {v8, v0}, Landroid/database/MergeCursor;-><init>([Landroid/database/Cursor;)V

    :cond_2
    return-object v8

    :catchall_1
    move-exception v0

    :try_start_4
    monitor-exit v1
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    throw v0

    :cond_3
    invoke-virtual {v9}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v9

    goto :goto_1
.end method
