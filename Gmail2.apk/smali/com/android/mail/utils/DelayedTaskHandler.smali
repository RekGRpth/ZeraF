.class public abstract Lcom/android/mail/utils/DelayedTaskHandler;
.super Landroid/os/Handler;
.source "DelayedTaskHandler.java"


# instance fields
.field private final mDelayMs:I

.field private mLastTaskExecuteTime:J


# direct methods
.method public constructor <init>(Landroid/os/Looper;I)V
    .locals 2
    .param p1    # Landroid/os/Looper;
    .param p2    # I

    invoke-direct {p0, p1}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    const-wide/16 v0, -0x1

    iput-wide v0, p0, Lcom/android/mail/utils/DelayedTaskHandler;->mLastTaskExecuteTime:J

    iput p2, p0, Lcom/android/mail/utils/DelayedTaskHandler;->mDelayMs:I

    return-void
.end method


# virtual methods
.method public dispatchMessage(Landroid/os/Message;)V
    .locals 0
    .param p1    # Landroid/os/Message;

    invoke-virtual {p0}, Lcom/android/mail/utils/DelayedTaskHandler;->onTaskExecution()V

    invoke-virtual {p0}, Lcom/android/mail/utils/DelayedTaskHandler;->performTask()V

    return-void
.end method

.method public onTaskExecution()V
    .locals 2

    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/android/mail/utils/DelayedTaskHandler;->mLastTaskExecuteTime:J

    return-void
.end method

.method protected abstract performTask()V
.end method

.method public scheduleTask()V
    .locals 7

    const/4 v6, 0x0

    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v0

    invoke-virtual {p0, v6}, Lcom/android/mail/utils/DelayedTaskHandler;->removeMessages(I)V

    iget-wide v2, p0, Lcom/android/mail/utils/DelayedTaskHandler;->mLastTaskExecuteTime:J

    const-wide/16 v4, -0x1

    cmp-long v2, v2, v4

    if-eqz v2, :cond_0

    iget-wide v2, p0, Lcom/android/mail/utils/DelayedTaskHandler;->mLastTaskExecuteTime:J

    iget v4, p0, Lcom/android/mail/utils/DelayedTaskHandler;->mDelayMs:I

    int-to-long v4, v4

    add-long/2addr v2, v4

    cmp-long v2, v2, v0

    if-gez v2, :cond_1

    :cond_0
    invoke-virtual {p0, v6}, Lcom/android/mail/utils/DelayedTaskHandler;->sendEmptyMessage(I)Z

    :goto_0
    return-void

    :cond_1
    iget v2, p0, Lcom/android/mail/utils/DelayedTaskHandler;->mDelayMs:I

    int-to-long v2, v2

    invoke-virtual {p0, v6, v2, v3}, Lcom/android/mail/utils/DelayedTaskHandler;->sendEmptyMessageDelayed(IJ)Z

    goto :goto_0
.end method
