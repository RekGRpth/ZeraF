.class Lcom/android/mail/ui/FolderListFragment$HierarchicalFolderListAdapter;
.super Landroid/widget/ArrayAdapter;
.source "FolderListFragment.java"

# interfaces
.implements Lcom/android/mail/ui/FolderListFragment$FolderListFragmentCursorAdapter;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/mail/ui/FolderListFragment;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "HierarchicalFolderListAdapter"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/widget/ArrayAdapter",
        "<",
        "Lcom/android/mail/providers/Folder;",
        ">;",
        "Lcom/android/mail/ui/FolderListFragment$FolderListFragmentCursorAdapter;"
    }
.end annotation


# instance fields
.field private mCursor:Landroid/database/Cursor;

.field private final mDropHandler:Lcom/android/mail/ui/FolderItemView$DropHandler;

.field private final mParent:Lcom/android/mail/providers/Folder;

.field private final mParentUri:Landroid/net/Uri;

.field final synthetic this$0:Lcom/android/mail/ui/FolderListFragment;


# direct methods
.method public constructor <init>(Lcom/android/mail/ui/FolderListFragment;Landroid/database/Cursor;Lcom/android/mail/providers/Folder;)V
    .locals 2
    .param p2    # Landroid/database/Cursor;
    .param p3    # Lcom/android/mail/providers/Folder;

    iput-object p1, p0, Lcom/android/mail/ui/FolderListFragment$HierarchicalFolderListAdapter;->this$0:Lcom/android/mail/ui/FolderListFragment;

    # getter for: Lcom/android/mail/ui/FolderListFragment;->mActivity:Lcom/android/mail/ui/ControllableActivity;
    invoke-static {p1}, Lcom/android/mail/ui/FolderListFragment;->access$000(Lcom/android/mail/ui/FolderListFragment;)Lcom/android/mail/ui/ControllableActivity;

    move-result-object v0

    invoke-interface {v0}, Lcom/android/mail/ui/ControllableActivity;->getActivityContext()Landroid/content/Context;

    move-result-object v0

    const v1, 0x7f040039

    invoke-direct {p0, v0, v1}, Landroid/widget/ArrayAdapter;-><init>(Landroid/content/Context;I)V

    # getter for: Lcom/android/mail/ui/FolderListFragment;->mActivity:Lcom/android/mail/ui/ControllableActivity;
    invoke-static {p1}, Lcom/android/mail/ui/FolderListFragment;->access$000(Lcom/android/mail/ui/FolderListFragment;)Lcom/android/mail/ui/ControllableActivity;

    move-result-object v0

    iput-object v0, p0, Lcom/android/mail/ui/FolderListFragment$HierarchicalFolderListAdapter;->mDropHandler:Lcom/android/mail/ui/FolderItemView$DropHandler;

    iput-object p3, p0, Lcom/android/mail/ui/FolderListFragment$HierarchicalFolderListAdapter;->mParent:Lcom/android/mail/providers/Folder;

    iget-object v0, p3, Lcom/android/mail/providers/Folder;->uri:Landroid/net/Uri;

    iput-object v0, p0, Lcom/android/mail/ui/FolderListFragment$HierarchicalFolderListAdapter;->mParentUri:Landroid/net/Uri;

    invoke-virtual {p0, p2}, Lcom/android/mail/ui/FolderListFragment$HierarchicalFolderListAdapter;->setCursor(Landroid/database/Cursor;)V

    return-void
.end method


# virtual methods
.method public destroy()V
    .locals 0

    return-void
.end method

.method public getFullFolder(Lcom/android/mail/ui/FolderListFragment$FolderListAdapter$Item;)Lcom/android/mail/providers/Folder;
    .locals 4
    .param p1    # Lcom/android/mail/ui/FolderListFragment$FolderListAdapter$Item;

    const/4 v1, 0x0

    iget v0, p1, Lcom/android/mail/ui/FolderListFragment$FolderListAdapter$Item;->mPosition:I

    iget-object v2, p0, Lcom/android/mail/ui/FolderListFragment$HierarchicalFolderListAdapter;->mCursor:Landroid/database/Cursor;

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/android/mail/ui/FolderListFragment$HierarchicalFolderListAdapter;->mCursor:Landroid/database/Cursor;

    invoke-interface {v2}, Landroid/database/Cursor;->isClosed()Z

    move-result v2

    if-eqz v2, :cond_1

    :cond_0
    iget-object v2, p0, Lcom/android/mail/ui/FolderListFragment$HierarchicalFolderListAdapter;->this$0:Lcom/android/mail/ui/FolderListFragment;

    # getter for: Lcom/android/mail/ui/FolderListFragment;->mFutureData:Landroid/database/Cursor;
    invoke-static {v2}, Lcom/android/mail/ui/FolderListFragment;->access$1300(Lcom/android/mail/ui/FolderListFragment;)Landroid/database/Cursor;

    move-result-object v2

    iput-object v2, p0, Lcom/android/mail/ui/FolderListFragment$HierarchicalFolderListAdapter;->mCursor:Landroid/database/Cursor;

    iget-object v2, p0, Lcom/android/mail/ui/FolderListFragment$HierarchicalFolderListAdapter;->this$0:Lcom/android/mail/ui/FolderListFragment;

    # setter for: Lcom/android/mail/ui/FolderListFragment;->mFutureData:Landroid/database/Cursor;
    invoke-static {v2, v1}, Lcom/android/mail/ui/FolderListFragment;->access$1302(Lcom/android/mail/ui/FolderListFragment;Landroid/database/Cursor;)Landroid/database/Cursor;

    :cond_1
    const/4 v2, -0x1

    if-le v0, v2, :cond_2

    iget-object v2, p0, Lcom/android/mail/ui/FolderListFragment$HierarchicalFolderListAdapter;->mCursor:Landroid/database/Cursor;

    if-eqz v2, :cond_2

    iget-object v2, p0, Lcom/android/mail/ui/FolderListFragment$HierarchicalFolderListAdapter;->mCursor:Landroid/database/Cursor;

    invoke-interface {v2}, Landroid/database/Cursor;->isClosed()Z

    move-result v2

    if-nez v2, :cond_2

    iget-object v2, p0, Lcom/android/mail/ui/FolderListFragment$HierarchicalFolderListAdapter;->mCursor:Landroid/database/Cursor;

    iget v3, p1, Lcom/android/mail/ui/FolderListFragment$FolderListAdapter$Item;->mPosition:I

    invoke-interface {v2, v3}, Landroid/database/Cursor;->moveToPosition(I)Z

    move-result v2

    if-eqz v2, :cond_2

    new-instance v1, Lcom/android/mail/providers/Folder;

    iget-object v2, p0, Lcom/android/mail/ui/FolderListFragment$HierarchicalFolderListAdapter;->mCursor:Landroid/database/Cursor;

    invoke-direct {v1, v2}, Lcom/android/mail/providers/Folder;-><init>(Landroid/database/Cursor;)V

    :cond_2
    return-object v1
.end method

.method public getItemViewType(I)I
    .locals 3
    .param p1    # I

    invoke-virtual {p0, p1}, Lcom/android/mail/ui/FolderListFragment$HierarchicalFolderListAdapter;->getItem(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/mail/providers/Folder;

    iget-object v1, v0, Lcom/android/mail/providers/Folder;->uri:Landroid/net/Uri;

    iget-object v2, p0, Lcom/android/mail/ui/FolderListFragment$HierarchicalFolderListAdapter;->mParentUri:Landroid/net/Uri;

    invoke-virtual {v1, v2}, Landroid/net/Uri;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v1, 0x0

    :goto_0
    return v1

    :cond_0
    const/4 v1, 0x1

    goto :goto_0
.end method

.method public getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 7
    .param p1    # I
    .param p2    # Landroid/view/View;
    .param p3    # Landroid/view/ViewGroup;

    const/4 v4, 0x1

    invoke-virtual {p0, p1}, Lcom/android/mail/ui/FolderListFragment$HierarchicalFolderListAdapter;->getItem(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/mail/providers/Folder;

    iget-object v5, v0, Lcom/android/mail/providers/Folder;->uri:Landroid/net/Uri;

    iget-object v6, p0, Lcom/android/mail/ui/FolderListFragment$HierarchicalFolderListAdapter;->mParentUri:Landroid/net/Uri;

    invoke-virtual {v5, v6}, Landroid/net/Uri;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz p2, :cond_1

    move-object v1, p2

    check-cast v1, Lcom/android/mail/ui/FolderItemView;

    :goto_0
    iget-object v5, p0, Lcom/android/mail/ui/FolderListFragment$HierarchicalFolderListAdapter;->mDropHandler:Lcom/android/mail/ui/FolderItemView$DropHandler;

    invoke-virtual {v1, v0, v5}, Lcom/android/mail/ui/FolderItemView;->bind(Lcom/android/mail/providers/Folder;Lcom/android/mail/ui/FolderItemView$DropHandler;)V

    iget-object v5, v0, Lcom/android/mail/providers/Folder;->uri:Landroid/net/Uri;

    iget-object v6, p0, Lcom/android/mail/ui/FolderListFragment$HierarchicalFolderListAdapter;->this$0:Lcom/android/mail/ui/FolderListFragment;

    # getter for: Lcom/android/mail/ui/FolderListFragment;->mSelectedFolderUri:Landroid/net/Uri;
    invoke-static {v6}, Lcom/android/mail/ui/FolderListFragment;->access$700(Lcom/android/mail/ui/FolderListFragment;)Landroid/net/Uri;

    move-result-object v6

    invoke-virtual {v5, v6}, Landroid/net/Uri;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_0

    iget-object v5, p0, Lcom/android/mail/ui/FolderListFragment$HierarchicalFolderListAdapter;->this$0:Lcom/android/mail/ui/FolderListFragment;

    invoke-virtual {v5}, Lcom/android/mail/ui/FolderListFragment;->getListView()Landroid/widget/ListView;

    move-result-object v5

    invoke-virtual {v5, p1, v4}, Landroid/widget/ListView;->setItemChecked(IZ)V

    iget-object v5, p0, Lcom/android/mail/ui/FolderListFragment$HierarchicalFolderListAdapter;->this$0:Lcom/android/mail/ui/FolderListFragment;

    # getter for: Lcom/android/mail/ui/FolderListFragment;->mCurrentFolderForUnreadCheck:Lcom/android/mail/providers/Folder;
    invoke-static {v5}, Lcom/android/mail/ui/FolderListFragment;->access$800(Lcom/android/mail/ui/FolderListFragment;)Lcom/android/mail/providers/Folder;

    move-result-object v5

    if-eqz v5, :cond_3

    iget v5, v0, Lcom/android/mail/providers/Folder;->unreadCount:I

    iget-object v6, p0, Lcom/android/mail/ui/FolderListFragment$HierarchicalFolderListAdapter;->this$0:Lcom/android/mail/ui/FolderListFragment;

    # getter for: Lcom/android/mail/ui/FolderListFragment;->mCurrentFolderForUnreadCheck:Lcom/android/mail/providers/Folder;
    invoke-static {v6}, Lcom/android/mail/ui/FolderListFragment;->access$800(Lcom/android/mail/ui/FolderListFragment;)Lcom/android/mail/providers/Folder;

    move-result-object v6

    iget v6, v6, Lcom/android/mail/providers/Folder;->unreadCount:I

    if-eq v5, v6, :cond_3

    :goto_1
    if-eqz v4, :cond_0

    iget-object v5, p0, Lcom/android/mail/ui/FolderListFragment$HierarchicalFolderListAdapter;->this$0:Lcom/android/mail/ui/FolderListFragment;

    # getter for: Lcom/android/mail/ui/FolderListFragment;->mCurrentFolderForUnreadCheck:Lcom/android/mail/providers/Folder;
    invoke-static {v5}, Lcom/android/mail/ui/FolderListFragment;->access$800(Lcom/android/mail/ui/FolderListFragment;)Lcom/android/mail/providers/Folder;

    move-result-object v5

    iget v5, v5, Lcom/android/mail/providers/Folder;->unreadCount:I

    invoke-virtual {v1, v5}, Lcom/android/mail/ui/FolderItemView;->overrideUnreadCount(I)V

    :cond_0
    const v5, 0x7f080032

    invoke-virtual {v1, v5}, Lcom/android/mail/ui/FolderItemView;->findViewById(I)Landroid/view/View;

    move-result-object v5

    invoke-static {v0, v5}, Lcom/android/mail/providers/Folder;->setFolderBlockColor(Lcom/android/mail/providers/Folder;Landroid/view/View;)V

    return-object v1

    :cond_1
    if-eqz v2, :cond_2

    const v3, 0x7f040039

    :goto_2
    iget-object v5, p0, Lcom/android/mail/ui/FolderListFragment$HierarchicalFolderListAdapter;->this$0:Lcom/android/mail/ui/FolderListFragment;

    # getter for: Lcom/android/mail/ui/FolderListFragment;->mActivity:Lcom/android/mail/ui/ControllableActivity;
    invoke-static {v5}, Lcom/android/mail/ui/FolderListFragment;->access$000(Lcom/android/mail/ui/FolderListFragment;)Lcom/android/mail/ui/ControllableActivity;

    move-result-object v5

    invoke-interface {v5}, Lcom/android/mail/ui/ControllableActivity;->getActivityContext()Landroid/content/Context;

    move-result-object v5

    invoke-static {v5}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v5

    const/4 v6, 0x0

    invoke-virtual {v5, v3, v6}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/android/mail/ui/FolderItemView;

    goto :goto_0

    :cond_2
    const v3, 0x7f04000f

    goto :goto_2

    :cond_3
    const/4 v4, 0x0

    goto :goto_1
.end method

.method public getViewTypeCount()I
    .locals 1

    const/4 v0, 0x2

    return v0
.end method

.method public setCursor(Landroid/database/Cursor;)V
    .locals 2
    .param p1    # Landroid/database/Cursor;

    iput-object p1, p0, Lcom/android/mail/ui/FolderListFragment$HierarchicalFolderListAdapter;->mCursor:Landroid/database/Cursor;

    invoke-virtual {p0}, Lcom/android/mail/ui/FolderListFragment$HierarchicalFolderListAdapter;->clear()V

    iget-object v1, p0, Lcom/android/mail/ui/FolderListFragment$HierarchicalFolderListAdapter;->mParent:Lcom/android/mail/providers/Folder;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/android/mail/ui/FolderListFragment$HierarchicalFolderListAdapter;->mParent:Lcom/android/mail/providers/Folder;

    invoke-virtual {p0, v1}, Lcom/android/mail/ui/FolderListFragment$HierarchicalFolderListAdapter;->add(Ljava/lang/Object;)V

    :cond_0
    if-eqz p1, :cond_2

    invoke-interface {p1}, Landroid/database/Cursor;->getCount()I

    move-result v1

    if-lez v1, :cond_2

    invoke-interface {p1}, Landroid/database/Cursor;->moveToFirst()Z

    :cond_1
    new-instance v0, Lcom/android/mail/providers/Folder;

    invoke-direct {v0, p1}, Lcom/android/mail/providers/Folder;-><init>(Landroid/database/Cursor;)V

    iget-object v1, p0, Lcom/android/mail/ui/FolderListFragment$HierarchicalFolderListAdapter;->mParent:Lcom/android/mail/providers/Folder;

    iput-object v1, v0, Lcom/android/mail/providers/Folder;->parent:Lcom/android/mail/providers/Folder;

    invoke-virtual {p0, v0}, Lcom/android/mail/ui/FolderListFragment$HierarchicalFolderListAdapter;->add(Ljava/lang/Object;)V

    invoke-interface {p1}, Landroid/database/Cursor;->moveToNext()Z

    move-result v1

    if-nez v1, :cond_1

    :cond_2
    return-void
.end method
