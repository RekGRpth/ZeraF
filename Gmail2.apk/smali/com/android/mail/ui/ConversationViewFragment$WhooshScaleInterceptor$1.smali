.class Lcom/android/mail/ui/ConversationViewFragment$WhooshScaleInterceptor$1;
.super Landroid/animation/AnimatorListenerAdapter;
.source "ConversationViewFragment.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/android/mail/ui/ConversationViewFragment$WhooshScaleInterceptor;->loadSingleMessageView(ZII)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$1:Lcom/android/mail/ui/ConversationViewFragment$WhooshScaleInterceptor;

.field final synthetic val$otherOverlays:Ljava/util/List;

.field final synthetic val$zoomingHeader:Landroid/view/View;


# direct methods
.method constructor <init>(Lcom/android/mail/ui/ConversationViewFragment$WhooshScaleInterceptor;Landroid/view/View;Ljava/util/List;)V
    .locals 0

    iput-object p1, p0, Lcom/android/mail/ui/ConversationViewFragment$WhooshScaleInterceptor$1;->this$1:Lcom/android/mail/ui/ConversationViewFragment$WhooshScaleInterceptor;

    iput-object p2, p0, Lcom/android/mail/ui/ConversationViewFragment$WhooshScaleInterceptor$1;->val$zoomingHeader:Landroid/view/View;

    iput-object p3, p0, Lcom/android/mail/ui/ConversationViewFragment$WhooshScaleInterceptor$1;->val$otherOverlays:Ljava/util/List;

    invoke-direct {p0}, Landroid/animation/AnimatorListenerAdapter;-><init>()V

    return-void
.end method


# virtual methods
.method public onAnimationEnd(Landroid/animation/Animator;)V
    .locals 6
    .param p1    # Landroid/animation/Animator;

    const/4 v5, 0x0

    const/high16 v4, 0x3f800000

    iget-object v2, p0, Lcom/android/mail/ui/ConversationViewFragment$WhooshScaleInterceptor$1;->this$1:Lcom/android/mail/ui/ConversationViewFragment$WhooshScaleInterceptor;

    iget-object v2, v2, Lcom/android/mail/ui/ConversationViewFragment$WhooshScaleInterceptor;->this$0:Lcom/android/mail/ui/ConversationViewFragment;

    # getter for: Lcom/android/mail/ui/ConversationViewFragment;->mMessageInnerGroup:Landroid/view/View;
    invoke-static {v2}, Lcom/android/mail/ui/ConversationViewFragment;->access$3400(Lcom/android/mail/ui/ConversationViewFragment;)Landroid/view/View;

    move-result-object v2

    const/4 v3, 0x0

    invoke-virtual {v2, v5, v3}, Landroid/view/View;->setLayerType(ILandroid/graphics/Paint;)V

    iget-object v2, p0, Lcom/android/mail/ui/ConversationViewFragment$WhooshScaleInterceptor$1;->val$zoomingHeader:Landroid/view/View;

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/android/mail/ui/ConversationViewFragment$WhooshScaleInterceptor$1;->val$zoomingHeader:Landroid/view/View;

    invoke-virtual {v2, v5}, Landroid/view/View;->setVisibility(I)V

    :cond_0
    iget-object v2, p0, Lcom/android/mail/ui/ConversationViewFragment$WhooshScaleInterceptor$1;->this$1:Lcom/android/mail/ui/ConversationViewFragment$WhooshScaleInterceptor;

    iget-object v2, v2, Lcom/android/mail/ui/ConversationViewFragment$WhooshScaleInterceptor;->this$0:Lcom/android/mail/ui/ConversationViewFragment;

    # getter for: Lcom/android/mail/ui/ConversationViewFragment;->mWebView:Lcom/android/mail/browse/ConversationWebView;
    invoke-static {v2}, Lcom/android/mail/ui/ConversationViewFragment;->access$500(Lcom/android/mail/ui/ConversationViewFragment;)Lcom/android/mail/browse/ConversationWebView;

    move-result-object v2

    invoke-virtual {v2}, Lcom/android/mail/browse/ConversationWebView;->animate()Landroid/view/ViewPropertyAnimator;

    move-result-object v2

    invoke-virtual {v2}, Landroid/view/ViewPropertyAnimator;->cancel()V

    iget-object v2, p0, Lcom/android/mail/ui/ConversationViewFragment$WhooshScaleInterceptor$1;->this$1:Lcom/android/mail/ui/ConversationViewFragment$WhooshScaleInterceptor;

    iget-object v2, v2, Lcom/android/mail/ui/ConversationViewFragment$WhooshScaleInterceptor;->this$0:Lcom/android/mail/ui/ConversationViewFragment;

    # getter for: Lcom/android/mail/ui/ConversationViewFragment;->mWebView:Lcom/android/mail/browse/ConversationWebView;
    invoke-static {v2}, Lcom/android/mail/ui/ConversationViewFragment;->access$500(Lcom/android/mail/ui/ConversationViewFragment;)Lcom/android/mail/browse/ConversationWebView;

    move-result-object v2

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Lcom/android/mail/browse/ConversationWebView;->setTranslationY(F)V

    iget-object v2, p0, Lcom/android/mail/ui/ConversationViewFragment$WhooshScaleInterceptor$1;->this$1:Lcom/android/mail/ui/ConversationViewFragment$WhooshScaleInterceptor;

    iget-object v2, v2, Lcom/android/mail/ui/ConversationViewFragment$WhooshScaleInterceptor;->this$0:Lcom/android/mail/ui/ConversationViewFragment;

    # getter for: Lcom/android/mail/ui/ConversationViewFragment;->mWebView:Lcom/android/mail/browse/ConversationWebView;
    invoke-static {v2}, Lcom/android/mail/ui/ConversationViewFragment;->access$500(Lcom/android/mail/ui/ConversationViewFragment;)Lcom/android/mail/browse/ConversationWebView;

    move-result-object v2

    invoke-virtual {v2, v4}, Lcom/android/mail/browse/ConversationWebView;->setAlpha(F)V

    iget-object v2, p0, Lcom/android/mail/ui/ConversationViewFragment$WhooshScaleInterceptor$1;->this$1:Lcom/android/mail/ui/ConversationViewFragment$WhooshScaleInterceptor;

    iget-object v2, v2, Lcom/android/mail/ui/ConversationViewFragment$WhooshScaleInterceptor;->this$0:Lcom/android/mail/ui/ConversationViewFragment;

    # getter for: Lcom/android/mail/ui/ConversationViewFragment;->mWebView:Lcom/android/mail/browse/ConversationWebView;
    invoke-static {v2}, Lcom/android/mail/ui/ConversationViewFragment;->access$500(Lcom/android/mail/ui/ConversationViewFragment;)Lcom/android/mail/browse/ConversationWebView;

    move-result-object v2

    invoke-virtual {v2, v4}, Lcom/android/mail/browse/ConversationWebView;->setScaleX(F)V

    iget-object v2, p0, Lcom/android/mail/ui/ConversationViewFragment$WhooshScaleInterceptor$1;->this$1:Lcom/android/mail/ui/ConversationViewFragment$WhooshScaleInterceptor;

    iget-object v2, v2, Lcom/android/mail/ui/ConversationViewFragment$WhooshScaleInterceptor;->this$0:Lcom/android/mail/ui/ConversationViewFragment;

    # getter for: Lcom/android/mail/ui/ConversationViewFragment;->mWebView:Lcom/android/mail/browse/ConversationWebView;
    invoke-static {v2}, Lcom/android/mail/ui/ConversationViewFragment;->access$500(Lcom/android/mail/ui/ConversationViewFragment;)Lcom/android/mail/browse/ConversationWebView;

    move-result-object v2

    invoke-virtual {v2, v4}, Lcom/android/mail/browse/ConversationWebView;->setScaleY(F)V

    iget-object v2, p0, Lcom/android/mail/ui/ConversationViewFragment$WhooshScaleInterceptor$1;->this$1:Lcom/android/mail/ui/ConversationViewFragment$WhooshScaleInterceptor;

    iget-object v2, v2, Lcom/android/mail/ui/ConversationViewFragment$WhooshScaleInterceptor;->this$0:Lcom/android/mail/ui/ConversationViewFragment;

    # getter for: Lcom/android/mail/ui/ConversationViewFragment;->mMessageGroup:Landroid/view/View;
    invoke-static {v2}, Lcom/android/mail/ui/ConversationViewFragment;->access$1400(Lcom/android/mail/ui/ConversationViewFragment;)Landroid/view/View;

    move-result-object v2

    const/4 v3, -0x1

    invoke-virtual {v2, v3}, Landroid/view/View;->setBackgroundColor(I)V

    iget-object v2, p0, Lcom/android/mail/ui/ConversationViewFragment$WhooshScaleInterceptor$1;->val$otherOverlays:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/view/View;

    invoke-virtual {v1}, Landroid/view/View;->animate()Landroid/view/ViewPropertyAnimator;

    move-result-object v2

    invoke-virtual {v2}, Landroid/view/ViewPropertyAnimator;->cancel()V

    invoke-virtual {v1, v4}, Landroid/view/View;->setAlpha(F)V

    goto :goto_0

    :cond_1
    return-void
.end method

.method public onAnimationStart(Landroid/animation/Animator;)V
    .locals 0
    .param p1    # Landroid/animation/Animator;

    return-void
.end method
