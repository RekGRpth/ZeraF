.class public final Lcom/android/mail/ui/OnePaneController;
.super Lcom/android/mail/ui/AbstractActivityController;
.source "OnePaneController.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/mail/ui/OnePaneController$PopBackStackRunnable;
    }
.end annotation


# instance fields
.field private mConversationListNeverShown:Z

.field private mConversationListVisible:Z

.field private mInbox:Lcom/android/mail/providers/Folder;

.field private mLastConversationListTransactionId:I

.field private mLastConversationTransactionId:I

.field private mLastFolderListTransactionId:I

.field private mLastInboxConversationListTransactionId:I


# direct methods
.method public constructor <init>(Lcom/android/mail/ui/MailActivity;Lcom/android/mail/ui/ViewMode;)V
    .locals 2
    .param p1    # Lcom/android/mail/ui/MailActivity;
    .param p2    # Lcom/android/mail/ui/ViewMode;

    const/4 v1, -0x1

    invoke-direct {p0, p1, p2}, Lcom/android/mail/ui/AbstractActivityController;-><init>(Lcom/android/mail/ui/MailActivity;Lcom/android/mail/ui/ViewMode;)V

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/android/mail/ui/OnePaneController;->mConversationListVisible:Z

    iput v1, p0, Lcom/android/mail/ui/OnePaneController;->mLastInboxConversationListTransactionId:I

    iput v1, p0, Lcom/android/mail/ui/OnePaneController;->mLastConversationListTransactionId:I

    iput v1, p0, Lcom/android/mail/ui/OnePaneController;->mLastConversationTransactionId:I

    iput v1, p0, Lcom/android/mail/ui/OnePaneController;->mLastFolderListTransactionId:I

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/android/mail/ui/OnePaneController;->mConversationListNeverShown:Z

    return-void
.end method

.method private goUpFolderHierarchy(Lcom/android/mail/providers/Folder;)V
    .locals 4
    .param p1    # Lcom/android/mail/providers/Folder;

    iget-object v0, p1, Lcom/android/mail/providers/Folder;->parent:Lcom/android/mail/providers/Folder;

    if-eqz v0, :cond_0

    invoke-virtual {p0, v0}, Lcom/android/mail/ui/OnePaneController;->setHierarchyFolder(Lcom/android/mail/providers/Folder;)V

    iget-object v1, v0, Lcom/android/mail/providers/Folder;->childFoldersListUri:Landroid/net/Uri;

    const/4 v2, 0x0

    invoke-static {v0, v1, v2}, Lcom/android/mail/ui/FolderListFragment;->newInstance(Lcom/android/mail/providers/Folder;Landroid/net/Uri;Z)Lcom/android/mail/ui/FolderListFragment;

    move-result-object v1

    const/16 v2, 0x1001

    const-string v3, "tag-folder-list"

    invoke-direct {p0, v1, v2, v3}, Lcom/android/mail/ui/OnePaneController;->replaceFragment(Landroid/app/Fragment;ILjava/lang/String;)I

    move-result v1

    iput v1, p0, Lcom/android/mail/ui/OnePaneController;->mLastFolderListTransactionId:I

    iget-object v1, p0, Lcom/android/mail/ui/OnePaneController;->mActionBarView:Lcom/android/mail/ui/MailActionBarView;

    invoke-virtual {v1}, Lcom/android/mail/ui/MailActionBarView;->setBackButton()V

    :goto_0
    return-void

    :cond_0
    invoke-virtual {p0}, Lcom/android/mail/ui/OnePaneController;->showFolderList()V

    goto :goto_0
.end method

.method private static inInbox(Lcom/android/mail/providers/Account;Lcom/android/mail/ConversationListContext;)Z
    .locals 2
    .param p0    # Lcom/android/mail/providers/Account;
    .param p1    # Lcom/android/mail/ConversationListContext;

    const/4 v0, 0x0

    if-eqz p0, :cond_0

    if-eqz p1, :cond_0

    iget-object v1, p1, Lcom/android/mail/ConversationListContext;->folder:Lcom/android/mail/providers/Folder;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/android/mail/providers/Account;->settings:Lcom/android/mail/providers/Settings;

    if-nez v1, :cond_1

    :cond_0
    :goto_0
    return v0

    :cond_1
    invoke-static {p1}, Lcom/android/mail/ConversationListContext;->isSearchResult(Lcom/android/mail/ConversationListContext;)Z

    move-result v1

    if-nez v1, :cond_0

    iget-object v1, p1, Lcom/android/mail/ConversationListContext;->folder:Lcom/android/mail/providers/Folder;

    iget-object v1, v1, Lcom/android/mail/providers/Folder;->uri:Landroid/net/Uri;

    invoke-static {v1, p0}, Lcom/android/mail/ui/OnePaneController;->isDefaultInbox(Landroid/net/Uri;Lcom/android/mail/providers/Account;)Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v0, 0x1

    goto :goto_0
.end method

.method private static final isDefaultInbox(Landroid/net/Uri;Lcom/android/mail/providers/Account;)Z
    .locals 2
    .param p0    # Landroid/net/Uri;
    .param p1    # Lcom/android/mail/providers/Account;

    if-eqz p0, :cond_0

    if-nez p1, :cond_1

    :cond_0
    const/4 v1, 0x0

    :goto_0
    return v1

    :cond_1
    iget-object v1, p1, Lcom/android/mail/providers/Account;->settings:Lcom/android/mail/providers/Settings;

    invoke-static {v1}, Lcom/android/mail/providers/Settings;->getDefaultInboxUri(Lcom/android/mail/providers/Settings;)Landroid/net/Uri;

    move-result-object v0

    iget-object v1, p1, Lcom/android/mail/providers/Account;->settings:Lcom/android/mail/providers/Settings;

    iget-object v1, v1, Lcom/android/mail/providers/Settings;->defaultInbox:Landroid/net/Uri;

    invoke-virtual {p0, v1}, Landroid/net/Uri;->equals(Ljava/lang/Object;)Z

    move-result v1

    goto :goto_0
.end method

.method private isTransactionIdValid(I)Z
    .locals 1
    .param p1    # I

    if-ltz p1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private replaceFragment(Landroid/app/Fragment;ILjava/lang/String;)I
    .locals 2
    .param p1    # Landroid/app/Fragment;
    .param p2    # I
    .param p3    # Ljava/lang/String;

    iget-object v1, p0, Lcom/android/mail/ui/OnePaneController;->mActivity:Lcom/android/mail/ui/ControllableActivity;

    invoke-interface {v1}, Lcom/android/mail/ui/ControllableActivity;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v1

    invoke-virtual {v1}, Landroid/app/FragmentManager;->beginTransaction()Landroid/app/FragmentTransaction;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/app/FragmentTransaction;->addToBackStack(Ljava/lang/String;)Landroid/app/FragmentTransaction;

    invoke-virtual {v0, p2}, Landroid/app/FragmentTransaction;->setTransition(I)Landroid/app/FragmentTransaction;

    const v1, 0x7f0800b4

    invoke-virtual {v0, v1, p1, p3}, Landroid/app/FragmentTransaction;->replace(ILandroid/app/Fragment;Ljava/lang/String;)Landroid/app/FragmentTransaction;

    invoke-virtual {v0}, Landroid/app/FragmentTransaction;->commitAllowingStateLoss()I

    move-result v1

    return v1
.end method

.method private safelyPopBackStack(IZ)V
    .locals 4
    .param p1    # I
    .param p2    # Z

    new-instance v0, Lcom/android/mail/ui/OnePaneController$PopBackStackRunnable;

    invoke-direct {v0, p0, p1}, Lcom/android/mail/ui/OnePaneController$PopBackStackRunnable;-><init>(Lcom/android/mail/ui/OnePaneController;I)V

    if-eqz p2, :cond_0

    iget-object v1, p0, Lcom/android/mail/ui/OnePaneController;->mHandler:Landroid/os/Handler;

    invoke-virtual {v1, v0}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    :goto_0
    return-void

    :cond_0
    invoke-virtual {p0}, Lcom/android/mail/ui/OnePaneController;->safeToModifyFragments()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-virtual {v0}, Lcom/android/mail/ui/OnePaneController$PopBackStackRunnable;->popBackStack()V

    goto :goto_0

    :cond_1
    sget-object v1, Lcom/android/mail/ui/OnePaneController;->LOG_TAG:Ljava/lang/String;

    const-string v2, "Activity has been saved; ignoring unsafe immediate request to pop back stack"

    const/4 v3, 0x0

    new-array v3, v3, [Ljava/lang/Object;

    invoke-static {v1, v2, v3}, Lcom/android/mail/utils/LogUtils;->i(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)I

    goto :goto_0
.end method

.method private transitionBackToConversationListMode(Z)V
    .locals 5
    .param p1    # Z

    const/4 v4, 0x1

    iget-object v2, p0, Lcom/android/mail/ui/OnePaneController;->mViewMode:Lcom/android/mail/ui/ViewMode;

    invoke-virtual {v2}, Lcom/android/mail/ui/ViewMode;->getMode()I

    move-result v1

    invoke-virtual {p0}, Lcom/android/mail/ui/OnePaneController;->enableCabMode()V

    const/4 v2, 0x5

    if-ne v1, v2, :cond_0

    iget-object v2, p0, Lcom/android/mail/ui/OnePaneController;->mViewMode:Lcom/android/mail/ui/ViewMode;

    invoke-virtual {v2}, Lcom/android/mail/ui/ViewMode;->enterSearchResultsListMode()Z

    :goto_0
    iget v2, p0, Lcom/android/mail/ui/OnePaneController;->mLastConversationListTransactionId:I

    invoke-direct {p0, v2}, Lcom/android/mail/ui/OnePaneController;->isTransactionIdValid(I)Z

    move-result v2

    if-eqz v2, :cond_1

    iget v2, p0, Lcom/android/mail/ui/OnePaneController;->mLastConversationListTransactionId:I

    invoke-direct {p0, v2, p1}, Lcom/android/mail/ui/OnePaneController;->safelyPopBackStack(IZ)V

    :goto_1
    iput-boolean v4, p0, Lcom/android/mail/ui/OnePaneController;->mConversationListVisible:Z

    const/4 v2, 0x0

    invoke-virtual {p0, v2}, Lcom/android/mail/ui/OnePaneController;->onConversationVisibilityChanged(Z)V

    invoke-virtual {p0, v4}, Lcom/android/mail/ui/OnePaneController;->onConversationListVisibilityChanged(Z)V

    return-void

    :cond_0
    iget-object v2, p0, Lcom/android/mail/ui/OnePaneController;->mViewMode:Lcom/android/mail/ui/ViewMode;

    invoke-virtual {v2}, Lcom/android/mail/ui/ViewMode;->enterConversationListMode()Z

    goto :goto_0

    :cond_1
    iget v2, p0, Lcom/android/mail/ui/OnePaneController;->mLastInboxConversationListTransactionId:I

    invoke-direct {p0, v2}, Lcom/android/mail/ui/OnePaneController;->isTransactionIdValid(I)Z

    move-result v2

    if-eqz v2, :cond_2

    iget v2, p0, Lcom/android/mail/ui/OnePaneController;->mLastInboxConversationListTransactionId:I

    invoke-direct {p0, v2, p1}, Lcom/android/mail/ui/OnePaneController;->safelyPopBackStack(IZ)V

    iget-object v2, p0, Lcom/android/mail/ui/OnePaneController;->mInbox:Lcom/android/mail/providers/Folder;

    invoke-virtual {p0, v2}, Lcom/android/mail/ui/OnePaneController;->onFolderChanged(Lcom/android/mail/providers/Folder;)V

    goto :goto_1

    :cond_2
    iget-object v2, p0, Lcom/android/mail/ui/OnePaneController;->mAccount:Lcom/android/mail/providers/Account;

    iget-object v3, p0, Lcom/android/mail/ui/OnePaneController;->mInbox:Lcom/android/mail/providers/Folder;

    invoke-static {v2, v3}, Lcom/android/mail/ConversationListContext;->forFolder(Lcom/android/mail/providers/Account;Lcom/android/mail/providers/Folder;)Lcom/android/mail/ConversationListContext;

    move-result-object v0

    iget-object v2, p0, Lcom/android/mail/ui/OnePaneController;->mInbox:Lcom/android/mail/providers/Folder;

    invoke-virtual {p0, v2}, Lcom/android/mail/ui/OnePaneController;->onFolderChanged(Lcom/android/mail/providers/Folder;)V

    invoke-virtual {p0, v0}, Lcom/android/mail/ui/OnePaneController;->showConversationList(Lcom/android/mail/ConversationListContext;)V

    goto :goto_1
.end method

.method private transitionToInbox()V
    .locals 4

    iget-object v2, p0, Lcom/android/mail/ui/OnePaneController;->mViewMode:Lcom/android/mail/ui/ViewMode;

    invoke-virtual {v2}, Lcom/android/mail/ui/ViewMode;->enterConversationListMode()Z

    iget-object v2, p0, Lcom/android/mail/ui/OnePaneController;->mConvListContext:Lcom/android/mail/ConversationListContext;

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/android/mail/ui/OnePaneController;->mConvListContext:Lcom/android/mail/ConversationListContext;

    iget-object v2, v2, Lcom/android/mail/ConversationListContext;->folder:Lcom/android/mail/providers/Folder;

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/android/mail/ui/OnePaneController;->mConvListContext:Lcom/android/mail/ConversationListContext;

    iget-object v2, v2, Lcom/android/mail/ConversationListContext;->folder:Lcom/android/mail/providers/Folder;

    iget-object v2, v2, Lcom/android/mail/providers/Folder;->uri:Landroid/net/Uri;

    iget-object v3, p0, Lcom/android/mail/ui/OnePaneController;->mAccount:Lcom/android/mail/providers/Account;

    invoke-static {v2, v3}, Lcom/android/mail/ui/OnePaneController;->isDefaultInbox(Landroid/net/Uri;Lcom/android/mail/providers/Account;)Z

    move-result v2

    if-nez v2, :cond_2

    :cond_0
    const/4 v0, 0x1

    :goto_0
    iget-object v2, p0, Lcom/android/mail/ui/OnePaneController;->mInbox:Lcom/android/mail/providers/Folder;

    if-eqz v2, :cond_1

    if-eqz v0, :cond_3

    :cond_1
    invoke-virtual {p0}, Lcom/android/mail/ui/OnePaneController;->loadAccountInbox()V

    :goto_1
    return-void

    :cond_2
    const/4 v0, 0x0

    goto :goto_0

    :cond_3
    iget-object v2, p0, Lcom/android/mail/ui/OnePaneController;->mAccount:Lcom/android/mail/providers/Account;

    iget-object v3, p0, Lcom/android/mail/ui/OnePaneController;->mInbox:Lcom/android/mail/providers/Folder;

    invoke-static {v2, v3}, Lcom/android/mail/ConversationListContext;->forFolder(Lcom/android/mail/providers/Account;Lcom/android/mail/providers/Folder;)Lcom/android/mail/ConversationListContext;

    move-result-object v1

    iget-object v2, p0, Lcom/android/mail/ui/OnePaneController;->mInbox:Lcom/android/mail/providers/Folder;

    invoke-virtual {p0, v2}, Lcom/android/mail/ui/OnePaneController;->onFolderChanged(Lcom/android/mail/providers/Folder;)V

    invoke-virtual {p0, v1}, Lcom/android/mail/ui/OnePaneController;->showConversationList(Lcom/android/mail/ConversationListContext;)V

    goto :goto_1
.end method


# virtual methods
.method public doesActionChangeConversationListVisibility(I)Z
    .locals 1
    .param p1    # I

    packed-switch p1, :pswitch_data_0

    :pswitch_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :pswitch_1
    const/4 v0, 0x0

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x7f080118
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_0
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public getHelpContext()Ljava/lang/String;
    .locals 3

    iget-object v1, p0, Lcom/android/mail/ui/OnePaneController;->mViewMode:Lcom/android/mail/ui/ViewMode;

    invoke-virtual {v1}, Lcom/android/mail/ui/ViewMode;->getMode()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    invoke-super {p0}, Lcom/android/mail/ui/AbstractActivityController;->getHelpContext()Ljava/lang/String;

    move-result-object v1

    :goto_0
    return-object v1

    :pswitch_0
    iget-object v1, p0, Lcom/android/mail/ui/OnePaneController;->mContext:Landroid/content/Context;

    const v2, 0x7f09001c

    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x3
        :pswitch_0
    .end packed-switch
.end method

.method public handleBackPress()Z
    .locals 8

    const/4 v7, 0x1

    const/4 v6, -0x1

    const/4 v5, 0x0

    iget-object v3, p0, Lcom/android/mail/ui/OnePaneController;->mViewMode:Lcom/android/mail/ui/ViewMode;

    invoke-virtual {v3}, Lcom/android/mail/ui/ViewMode;->getMode()I

    move-result v2

    const/4 v3, 0x3

    if-ne v2, v3, :cond_1

    invoke-virtual {p0}, Lcom/android/mail/ui/OnePaneController;->getHierarchyFolder()Lcom/android/mail/providers/Folder;

    move-result-object v1

    invoke-virtual {p0}, Lcom/android/mail/ui/OnePaneController;->getFolderListFragment()Lcom/android/mail/ui/FolderListFragment;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/android/mail/ui/FolderListFragment;->showingHierarchy()Z

    move-result v3

    if-eqz v3, :cond_0

    if-eqz v1, :cond_0

    invoke-direct {p0, v1}, Lcom/android/mail/ui/OnePaneController;->goUpFolderHierarchy(Lcom/android/mail/providers/Folder;)V

    :goto_0
    iget-object v3, p0, Lcom/android/mail/ui/OnePaneController;->mToastBar:Lcom/android/mail/ui/ActionableToastBar;

    invoke-virtual {v3, v5}, Lcom/android/mail/ui/ActionableToastBar;->hide(Z)V

    return v7

    :cond_0
    iput v6, p0, Lcom/android/mail/ui/OnePaneController;->mLastFolderListTransactionId:I

    invoke-direct {p0}, Lcom/android/mail/ui/OnePaneController;->transitionToInbox()V

    goto :goto_0

    :cond_1
    const/4 v3, 0x4

    if-ne v2, v3, :cond_2

    iget-object v3, p0, Lcom/android/mail/ui/OnePaneController;->mActivity:Lcom/android/mail/ui/ControllableActivity;

    invoke-interface {v3}, Lcom/android/mail/ui/ControllableActivity;->finish()V

    goto :goto_0

    :cond_2
    iget-object v3, p0, Lcom/android/mail/ui/OnePaneController;->mViewMode:Lcom/android/mail/ui/ViewMode;

    invoke-virtual {v3}, Lcom/android/mail/ui/ViewMode;->isListMode()Z

    move-result v3

    if-eqz v3, :cond_4

    iget-object v3, p0, Lcom/android/mail/ui/OnePaneController;->mAccount:Lcom/android/mail/providers/Account;

    iget-object v4, p0, Lcom/android/mail/ui/OnePaneController;->mConvListContext:Lcom/android/mail/ConversationListContext;

    invoke-static {v3, v4}, Lcom/android/mail/ui/OnePaneController;->inInbox(Lcom/android/mail/providers/Account;Lcom/android/mail/ConversationListContext;)Z

    move-result v3

    if-nez v3, :cond_4

    iget v3, p0, Lcom/android/mail/ui/OnePaneController;->mLastFolderListTransactionId:I

    if-eq v3, v6, :cond_3

    iget-object v3, p0, Lcom/android/mail/ui/OnePaneController;->mViewMode:Lcom/android/mail/ui/ViewMode;

    invoke-virtual {v3}, Lcom/android/mail/ui/ViewMode;->enterFolderListMode()Z

    iget-object v3, p0, Lcom/android/mail/ui/OnePaneController;->mActivity:Lcom/android/mail/ui/ControllableActivity;

    invoke-interface {v3}, Lcom/android/mail/ui/ControllableActivity;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v3

    iget v4, p0, Lcom/android/mail/ui/OnePaneController;->mLastFolderListTransactionId:I

    invoke-virtual {v3, v4, v5}, Landroid/app/FragmentManager;->popBackStack(II)V

    goto :goto_0

    :cond_3
    invoke-direct {p0}, Lcom/android/mail/ui/OnePaneController;->transitionToInbox()V

    goto :goto_0

    :cond_4
    if-eq v2, v7, :cond_5

    const/4 v3, 0x5

    if-ne v2, v3, :cond_6

    :cond_5
    invoke-direct {p0, v5}, Lcom/android/mail/ui/OnePaneController;->transitionBackToConversationListMode(Z)V

    goto :goto_0

    :cond_6
    iget-object v3, p0, Lcom/android/mail/ui/OnePaneController;->mActivity:Lcom/android/mail/ui/ControllableActivity;

    invoke-interface {v3}, Lcom/android/mail/ui/ControllableActivity;->finish()V

    goto :goto_0
.end method

.method public handleUpPress()Z
    .locals 4

    const/4 v3, 0x1

    iget-object v1, p0, Lcom/android/mail/ui/OnePaneController;->mViewMode:Lcom/android/mail/ui/ViewMode;

    invoke-virtual {v1}, Lcom/android/mail/ui/ViewMode;->getMode()I

    move-result v0

    const/4 v1, 0x4

    if-ne v0, v1, :cond_1

    iget-object v1, p0, Lcom/android/mail/ui/OnePaneController;->mActivity:Lcom/android/mail/ui/ControllableActivity;

    invoke-interface {v1}, Lcom/android/mail/ui/ControllableActivity;->finish()V

    :cond_0
    :goto_0
    return v3

    :cond_1
    iget-object v1, p0, Lcom/android/mail/ui/OnePaneController;->mAccount:Lcom/android/mail/providers/Account;

    iget-object v2, p0, Lcom/android/mail/ui/OnePaneController;->mConvListContext:Lcom/android/mail/ConversationListContext;

    invoke-static {v1, v2}, Lcom/android/mail/ui/OnePaneController;->inInbox(Lcom/android/mail/providers/Account;Lcom/android/mail/ConversationListContext;)Z

    move-result v1

    if-nez v1, :cond_2

    iget-object v1, p0, Lcom/android/mail/ui/OnePaneController;->mViewMode:Lcom/android/mail/ui/ViewMode;

    invoke-virtual {v1}, Lcom/android/mail/ui/ViewMode;->isListMode()Z

    move-result v1

    if-nez v1, :cond_3

    :cond_2
    if-eq v0, v3, :cond_3

    const/4 v1, 0x3

    if-eq v0, v1, :cond_3

    const/4 v1, 0x5

    if-ne v0, v1, :cond_0

    :cond_3
    invoke-virtual {p0}, Lcom/android/mail/ui/OnePaneController;->handleBackPress()Z

    goto :goto_0
.end method

.method protected hideOrRepositionToastBar(Z)V
    .locals 1
    .param p1    # Z

    iget-object v0, p0, Lcom/android/mail/ui/OnePaneController;->mToastBar:Lcom/android/mail/ui/ActionableToastBar;

    invoke-virtual {v0, p1}, Lcom/android/mail/ui/ActionableToastBar;->hide(Z)V

    return-void
.end method

.method protected hideWaitForInitialization()V
    .locals 0

    invoke-direct {p0}, Lcom/android/mail/ui/OnePaneController;->transitionToInbox()V

    invoke-super {p0}, Lcom/android/mail/ui/AbstractActivityController;->hideWaitForInitialization()V

    return-void
.end method

.method protected isConversationListVisible()Z
    .locals 1

    iget-boolean v0, p0, Lcom/android/mail/ui/OnePaneController;->mConversationListVisible:Z

    return v0
.end method

.method public onAccountChanged(Lcom/android/mail/providers/Account;)V
    .locals 1
    .param p1    # Lcom/android/mail/providers/Account;

    invoke-super {p0, p1}, Lcom/android/mail/ui/AbstractActivityController;->onAccountChanged(Lcom/android/mail/providers/Account;)V

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/android/mail/ui/OnePaneController;->mConversationListNeverShown:Z

    return-void
.end method

.method public onCreate(Landroid/os/Bundle;)Z
    .locals 2
    .param p1    # Landroid/os/Bundle;

    iget-object v0, p0, Lcom/android/mail/ui/OnePaneController;->mActivity:Lcom/android/mail/ui/ControllableActivity;

    const v1, 0x7f040054

    invoke-interface {v0, v1}, Lcom/android/mail/ui/ControllableActivity;->setContentView(I)V

    invoke-super {p0, p1}, Lcom/android/mail/ui/AbstractActivityController;->onCreate(Landroid/os/Bundle;)Z

    move-result v0

    return v0
.end method

.method public onError(Lcom/android/mail/providers/Folder;Z)V
    .locals 2
    .param p1    # Lcom/android/mail/providers/Folder;
    .param p2    # Z

    iget-object v1, p0, Lcom/android/mail/ui/OnePaneController;->mViewMode:Lcom/android/mail/ui/ViewMode;

    invoke-virtual {v1}, Lcom/android/mail/ui/ViewMode;->getMode()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    :goto_0
    :pswitch_0
    return-void

    :pswitch_1
    invoke-virtual {p0, p1, p2}, Lcom/android/mail/ui/OnePaneController;->showErrorToast(Lcom/android/mail/providers/Folder;Z)V

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x2
        :pswitch_1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public onFolderSelected(Lcom/android/mail/providers/Folder;)V
    .locals 3
    .param p1    # Lcom/android/mail/providers/Folder;

    iget-boolean v0, p1, Lcom/android/mail/providers/Folder;->hasChildren:Z

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/android/mail/ui/OnePaneController;->getHierarchyFolder()Lcom/android/mail/providers/Folder;

    move-result-object v0

    invoke-virtual {p1, v0}, Lcom/android/mail/providers/Folder;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/android/mail/ui/OnePaneController;->mViewMode:Lcom/android/mail/ui/ViewMode;

    invoke-virtual {v0}, Lcom/android/mail/ui/ViewMode;->enterFolderListMode()Z

    invoke-virtual {p0, p1}, Lcom/android/mail/ui/OnePaneController;->setHierarchyFolder(Lcom/android/mail/providers/Folder;)V

    iget-object v0, p1, Lcom/android/mail/providers/Folder;->childFoldersListUri:Landroid/net/Uri;

    const/4 v1, 0x0

    invoke-static {p1, v0, v1}, Lcom/android/mail/ui/FolderListFragment;->newInstance(Lcom/android/mail/providers/Folder;Landroid/net/Uri;Z)Lcom/android/mail/ui/FolderListFragment;

    move-result-object v0

    const/16 v1, 0x1001

    const-string v2, "tag-folder-list"

    invoke-direct {p0, v0, v1, v2}, Lcom/android/mail/ui/OnePaneController;->replaceFragment(Landroid/app/Fragment;ILjava/lang/String;)I

    move-result v0

    iput v0, p0, Lcom/android/mail/ui/OnePaneController;->mLastFolderListTransactionId:I

    iget-object v0, p0, Lcom/android/mail/ui/OnePaneController;->mActionBarView:Lcom/android/mail/ui/MailActionBarView;

    invoke-virtual {v0}, Lcom/android/mail/ui/MailActionBarView;->setBackButton()V

    :goto_0
    return-void

    :cond_0
    invoke-super {p0, p1}, Lcom/android/mail/ui/AbstractActivityController;->onFolderSelected(Lcom/android/mail/providers/Folder;)V

    goto :goto_0
.end method

.method public onRestoreInstanceState(Landroid/os/Bundle;)V
    .locals 2
    .param p1    # Landroid/os/Bundle;

    const/4 v1, -0x1

    invoke-super {p0, p1}, Lcom/android/mail/ui/AbstractActivityController;->onRestoreInstanceState(Landroid/os/Bundle;)V

    if-nez p1, :cond_0

    :goto_0
    return-void

    :cond_0
    const-string v0, "folder-list-transaction"

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v0

    iput v0, p0, Lcom/android/mail/ui/OnePaneController;->mLastFolderListTransactionId:I

    const-string v0, "inbox_conversation-list-transaction"

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v0

    iput v0, p0, Lcom/android/mail/ui/OnePaneController;->mLastInboxConversationListTransactionId:I

    const-string v0, "conversation-list-transaction"

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v0

    iput v0, p0, Lcom/android/mail/ui/OnePaneController;->mLastConversationListTransactionId:I

    const-string v0, "conversation-transaction"

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v0

    iput v0, p0, Lcom/android/mail/ui/OnePaneController;->mLastConversationTransactionId:I

    const-string v0, "conversation-list-visible"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/android/mail/ui/OnePaneController;->mConversationListVisible:Z

    const-string v0, "conversation-list-never-shown"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/android/mail/ui/OnePaneController;->mConversationListNeverShown:Z

    const-string v0, "m-inbox"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/android/mail/providers/Folder;

    iput-object v0, p0, Lcom/android/mail/ui/OnePaneController;->mInbox:Lcom/android/mail/providers/Folder;

    goto :goto_0
.end method

.method public onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 2
    .param p1    # Landroid/os/Bundle;

    invoke-super {p0, p1}, Lcom/android/mail/ui/AbstractActivityController;->onSaveInstanceState(Landroid/os/Bundle;)V

    const-string v0, "folder-list-transaction"

    iget v1, p0, Lcom/android/mail/ui/OnePaneController;->mLastFolderListTransactionId:I

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    const-string v0, "inbox_conversation-list-transaction"

    iget v1, p0, Lcom/android/mail/ui/OnePaneController;->mLastInboxConversationListTransactionId:I

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    const-string v0, "conversation-list-transaction"

    iget v1, p0, Lcom/android/mail/ui/OnePaneController;->mLastConversationListTransactionId:I

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    const-string v0, "conversation-transaction"

    iget v1, p0, Lcom/android/mail/ui/OnePaneController;->mLastConversationTransactionId:I

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    const-string v0, "conversation-list-visible"

    iget-boolean v1, p0, Lcom/android/mail/ui/OnePaneController;->mConversationListVisible:Z

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    const-string v0, "conversation-list-never-shown"

    iget-boolean v1, p0, Lcom/android/mail/ui/OnePaneController;->mConversationListNeverShown:Z

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    const-string v0, "m-inbox"

    iget-object v1, p0, Lcom/android/mail/ui/OnePaneController;->mInbox:Lcom/android/mail/providers/Folder;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    return-void
.end method

.method public onUndoAvailable(Lcom/android/mail/ui/ToastBarOperation;)V
    .locals 10
    .param p1    # Lcom/android/mail/ui/ToastBarOperation;

    const v5, 0x7f0900ae

    const/4 v2, 0x0

    const/4 v4, 0x1

    if-eqz p1, :cond_0

    iget-object v0, p0, Lcom/android/mail/ui/OnePaneController;->mAccount:Lcom/android/mail/providers/Account;

    const/16 v1, 0x4000

    invoke-virtual {v0, v1}, Lcom/android/mail/providers/Account;->supportsCapability(I)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/mail/ui/OnePaneController;->mViewMode:Lcom/android/mail/ui/ViewMode;

    invoke-virtual {v0}, Lcom/android/mail/ui/ViewMode;->getMode()I

    move-result v9

    invoke-virtual {p0}, Lcom/android/mail/ui/OnePaneController;->getConversationListFragment()Lcom/android/mail/ui/ConversationListFragment;

    move-result-object v8

    packed-switch v9, :pswitch_data_0

    :cond_0
    :goto_0
    :pswitch_0
    return-void

    :pswitch_1
    iget-object v0, p0, Lcom/android/mail/ui/OnePaneController;->mToastBar:Lcom/android/mail/ui/ActionableToastBar;

    invoke-virtual {v0, v4}, Lcom/android/mail/ui/ActionableToastBar;->setConversationMode(Z)V

    iget-object v0, p0, Lcom/android/mail/ui/OnePaneController;->mToastBar:Lcom/android/mail/ui/ActionableToastBar;

    if-eqz v8, :cond_1

    invoke-virtual {v8}, Lcom/android/mail/ui/ConversationListFragment;->getAnimatedAdapter()Lcom/android/mail/ui/AnimatedAdapter;

    move-result-object v1

    :goto_1
    invoke-virtual {p0, v1}, Lcom/android/mail/ui/OnePaneController;->getUndoClickedListener(Lcom/android/mail/ui/AnimatedAdapter;)Lcom/android/mail/ui/ActionableToastBar$ActionClickedListener;

    move-result-object v1

    iget-object v3, p0, Lcom/android/mail/ui/OnePaneController;->mActivity:Lcom/android/mail/ui/ControllableActivity;

    invoke-interface {v3}, Lcom/android/mail/ui/ControllableActivity;->getActivityContext()Landroid/content/Context;

    move-result-object v3

    iget-object v6, p0, Lcom/android/mail/ui/OnePaneController;->mFolder:Lcom/android/mail/providers/Folder;

    invoke-virtual {p1, v3, v6}, Lcom/android/mail/ui/ToastBarOperation;->getDescription(Landroid/content/Context;Lcom/android/mail/providers/Folder;)Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lcom/android/mail/utils/Utils;->convertHtmlToPlainText(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    move v6, v4

    move-object v7, p1

    invoke-virtual/range {v0 .. v7}, Lcom/android/mail/ui/ActionableToastBar;->show(Lcom/android/mail/ui/ActionableToastBar$ActionClickedListener;ILjava/lang/CharSequence;ZIZLcom/android/mail/ui/ToastBarOperation;)V

    goto :goto_0

    :cond_1
    const/4 v1, 0x0

    goto :goto_1

    :pswitch_2
    if-eqz v8, :cond_2

    iget-object v0, p0, Lcom/android/mail/ui/OnePaneController;->mToastBar:Lcom/android/mail/ui/ActionableToastBar;

    invoke-virtual {v0, v2}, Lcom/android/mail/ui/ActionableToastBar;->setConversationMode(Z)V

    iget-object v0, p0, Lcom/android/mail/ui/OnePaneController;->mToastBar:Lcom/android/mail/ui/ActionableToastBar;

    invoke-virtual {v8}, Lcom/android/mail/ui/ConversationListFragment;->getAnimatedAdapter()Lcom/android/mail/ui/AnimatedAdapter;

    move-result-object v1

    invoke-virtual {p0, v1}, Lcom/android/mail/ui/OnePaneController;->getUndoClickedListener(Lcom/android/mail/ui/AnimatedAdapter;)Lcom/android/mail/ui/ActionableToastBar$ActionClickedListener;

    move-result-object v1

    iget-object v3, p0, Lcom/android/mail/ui/OnePaneController;->mActivity:Lcom/android/mail/ui/ControllableActivity;

    invoke-interface {v3}, Lcom/android/mail/ui/ControllableActivity;->getActivityContext()Landroid/content/Context;

    move-result-object v3

    iget-object v6, p0, Lcom/android/mail/ui/OnePaneController;->mFolder:Lcom/android/mail/providers/Folder;

    invoke-virtual {p1, v3, v6}, Lcom/android/mail/ui/ToastBarOperation;->getDescription(Landroid/content/Context;Lcom/android/mail/providers/Folder;)Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lcom/android/mail/utils/Utils;->convertHtmlToPlainText(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    move v6, v4

    move-object v7, p1

    invoke-virtual/range {v0 .. v7}, Lcom/android/mail/ui/ActionableToastBar;->show(Lcom/android/mail/ui/ActionableToastBar$ActionClickedListener;ILjava/lang/CharSequence;ZIZLcom/android/mail/ui/ToastBarOperation;)V

    goto :goto_0

    :cond_2
    iget-object v0, p0, Lcom/android/mail/ui/OnePaneController;->mActivity:Lcom/android/mail/ui/ControllableActivity;

    invoke-interface {v0, p1}, Lcom/android/mail/ui/ControllableActivity;->setPendingToastOperation(Lcom/android/mail/ui/ToastBarOperation;)V

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_2
        :pswitch_0
        :pswitch_2
        :pswitch_1
    .end packed-switch
.end method

.method public onViewModeChanged(I)V
    .locals 2
    .param p1    # I

    invoke-super {p0, p1}, Lcom/android/mail/ui/AbstractActivityController;->onViewModeChanged(I)V

    invoke-static {p1}, Lcom/android/mail/ui/ViewMode;->isListMode(I)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/mail/ui/OnePaneController;->mPagerController:Lcom/android/mail/browse/ConversationPagerController;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/android/mail/browse/ConversationPagerController;->hide(Z)V

    :cond_0
    invoke-static {p1}, Lcom/android/mail/ui/ViewMode;->isConversationMode(I)Z

    move-result v0

    if-nez v0, :cond_1

    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/android/mail/ui/OnePaneController;->setCurrentConversation(Lcom/android/mail/providers/Conversation;)V

    :cond_1
    return-void
.end method

.method public resetActionBarIcon()V
    .locals 3

    iget-object v1, p0, Lcom/android/mail/ui/OnePaneController;->mViewMode:Lcom/android/mail/ui/ViewMode;

    invoke-virtual {v1}, Lcom/android/mail/ui/ViewMode;->getMode()I

    move-result v0

    const/4 v1, 0x2

    if-ne v0, v1, :cond_0

    iget-object v1, p0, Lcom/android/mail/ui/OnePaneController;->mAccount:Lcom/android/mail/providers/Account;

    iget-object v2, p0, Lcom/android/mail/ui/OnePaneController;->mConvListContext:Lcom/android/mail/ConversationListContext;

    invoke-static {v1, v2}, Lcom/android/mail/ui/OnePaneController;->inInbox(Lcom/android/mail/providers/Account;Lcom/android/mail/ConversationListContext;)Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/android/mail/ui/OnePaneController;->mActionBarView:Lcom/android/mail/ui/MailActionBarView;

    invoke-virtual {v1}, Lcom/android/mail/ui/MailActionBarView;->removeBackButton()V

    :goto_0
    return-void

    :cond_0
    iget-object v1, p0, Lcom/android/mail/ui/OnePaneController;->mActionBarView:Lcom/android/mail/ui/MailActionBarView;

    invoke-virtual {v1}, Lcom/android/mail/ui/MailActionBarView;->setBackButton()V

    goto :goto_0
.end method

.method public shouldShowFirstConversation()Z
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method protected showConversation(Lcom/android/mail/providers/Conversation;Z)V
    .locals 8
    .param p1    # Lcom/android/mail/providers/Conversation;
    .param p2    # Z

    const/4 v7, 0x1

    const/4 v6, 0x0

    invoke-super {p0, p1, p2}, Lcom/android/mail/ui/AbstractActivityController;->showConversation(Lcom/android/mail/providers/Conversation;Z)V

    if-nez p1, :cond_0

    invoke-direct {p0, p2}, Lcom/android/mail/ui/OnePaneController;->transitionBackToConversationListMode(Z)V

    :goto_0
    return-void

    :cond_0
    invoke-virtual {p0}, Lcom/android/mail/ui/OnePaneController;->disableCabMode()V

    iget-object v3, p0, Lcom/android/mail/ui/OnePaneController;->mConvListContext:Lcom/android/mail/ConversationListContext;

    invoke-static {v3}, Lcom/android/mail/ConversationListContext;->isSearchResult(Lcom/android/mail/ConversationListContext;)Z

    move-result v3

    if-eqz v3, :cond_2

    iget-object v3, p0, Lcom/android/mail/ui/OnePaneController;->mViewMode:Lcom/android/mail/ui/ViewMode;

    invoke-virtual {v3}, Lcom/android/mail/ui/ViewMode;->enterSearchResultsConversationMode()Z

    :goto_1
    iget-object v3, p0, Lcom/android/mail/ui/OnePaneController;->mActivity:Lcom/android/mail/ui/ControllableActivity;

    invoke-interface {v3}, Lcom/android/mail/ui/ControllableActivity;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v1

    invoke-virtual {v1}, Landroid/app/FragmentManager;->beginTransaction()Landroid/app/FragmentTransaction;

    move-result-object v2

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Landroid/app/FragmentTransaction;->addToBackStack(Ljava/lang/String;)Landroid/app/FragmentTransaction;

    const v3, 0x7f0800b4

    invoke-virtual {v1, v3}, Landroid/app/FragmentManager;->findFragmentById(I)Landroid/app/Fragment;

    move-result-object v0

    if-eqz v0, :cond_1

    invoke-virtual {v0}, Landroid/app/Fragment;->isAdded()Z

    move-result v3

    if-eqz v3, :cond_1

    const/16 v3, 0x1001

    invoke-virtual {v2, v3}, Landroid/app/FragmentTransaction;->setTransition(I)Landroid/app/FragmentTransaction;

    invoke-virtual {v2, v0}, Landroid/app/FragmentTransaction;->remove(Landroid/app/Fragment;)Landroid/app/FragmentTransaction;

    invoke-virtual {v2}, Landroid/app/FragmentTransaction;->commitAllowingStateLoss()I

    :cond_1
    iget-object v3, p0, Lcom/android/mail/ui/OnePaneController;->mPagerController:Lcom/android/mail/browse/ConversationPagerController;

    iget-object v4, p0, Lcom/android/mail/ui/OnePaneController;->mAccount:Lcom/android/mail/providers/Account;

    iget-object v5, p0, Lcom/android/mail/ui/OnePaneController;->mFolder:Lcom/android/mail/providers/Folder;

    invoke-virtual {v3, v4, v5, p1, v7}, Lcom/android/mail/browse/ConversationPagerController;->show(Lcom/android/mail/providers/Account;Lcom/android/mail/providers/Folder;Lcom/android/mail/providers/Conversation;Z)V

    invoke-virtual {p0, v7}, Lcom/android/mail/ui/OnePaneController;->onConversationVisibilityChanged(Z)V

    iput-boolean v6, p0, Lcom/android/mail/ui/OnePaneController;->mConversationListVisible:Z

    invoke-virtual {p0, v6}, Lcom/android/mail/ui/OnePaneController;->onConversationListVisibilityChanged(Z)V

    goto :goto_0

    :cond_2
    iget-object v3, p0, Lcom/android/mail/ui/OnePaneController;->mViewMode:Lcom/android/mail/ui/ViewMode;

    invoke-virtual {v3}, Lcom/android/mail/ui/ViewMode;->enterConversationMode()Z

    goto :goto_1
.end method

.method public showConversationList(Lcom/android/mail/ConversationListContext;)V
    .locals 7
    .param p1    # Lcom/android/mail/ConversationListContext;

    const/4 v6, 0x1

    const/4 v5, 0x0

    const/4 v4, -0x1

    invoke-super {p0, p1}, Lcom/android/mail/ui/AbstractActivityController;->showConversationList(Lcom/android/mail/ConversationListContext;)V

    invoke-virtual {p0}, Lcom/android/mail/ui/OnePaneController;->enableCabMode()V

    invoke-static {p1}, Lcom/android/mail/ConversationListContext;->isSearchResult(Lcom/android/mail/ConversationListContext;)Z

    move-result v2

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/android/mail/ui/OnePaneController;->mViewMode:Lcom/android/mail/ui/ViewMode;

    invoke-virtual {v2}, Lcom/android/mail/ui/ViewMode;->enterSearchResultsListMode()Z

    :goto_0
    iget-boolean v2, p0, Lcom/android/mail/ui/OnePaneController;->mConversationListNeverShown:Z

    if-eqz v2, :cond_1

    const/16 v1, 0x1003

    :goto_1
    invoke-static {p1}, Lcom/android/mail/ui/ConversationListFragment;->newInstance(Lcom/android/mail/ConversationListContext;)Lcom/android/mail/ui/ConversationListFragment;

    move-result-object v0

    iget-object v2, p0, Lcom/android/mail/ui/OnePaneController;->mAccount:Lcom/android/mail/providers/Account;

    iget-object v3, p0, Lcom/android/mail/ui/OnePaneController;->mConvListContext:Lcom/android/mail/ConversationListContext;

    invoke-static {v2, v3}, Lcom/android/mail/ui/OnePaneController;->inInbox(Lcom/android/mail/providers/Account;Lcom/android/mail/ConversationListContext;)Z

    move-result v2

    if-nez v2, :cond_2

    const-string v2, "tag-conversation-list"

    invoke-direct {p0, v0, v1, v2}, Lcom/android/mail/ui/OnePaneController;->replaceFragment(Landroid/app/Fragment;ILjava/lang/String;)I

    move-result v2

    iput v2, p0, Lcom/android/mail/ui/OnePaneController;->mLastConversationListTransactionId:I

    :goto_2
    iput-boolean v6, p0, Lcom/android/mail/ui/OnePaneController;->mConversationListVisible:Z

    invoke-virtual {p0, v5}, Lcom/android/mail/ui/OnePaneController;->onConversationVisibilityChanged(Z)V

    invoke-virtual {p0, v6}, Lcom/android/mail/ui/OnePaneController;->onConversationListVisibilityChanged(Z)V

    iput-boolean v5, p0, Lcom/android/mail/ui/OnePaneController;->mConversationListNeverShown:Z

    return-void

    :cond_0
    iget-object v2, p0, Lcom/android/mail/ui/OnePaneController;->mViewMode:Lcom/android/mail/ui/ViewMode;

    invoke-virtual {v2}, Lcom/android/mail/ui/ViewMode;->enterConversationListMode()Z

    goto :goto_0

    :cond_1
    const/16 v1, 0x1001

    goto :goto_1

    :cond_2
    iget-object v2, p1, Lcom/android/mail/ConversationListContext;->folder:Lcom/android/mail/providers/Folder;

    iput-object v2, p0, Lcom/android/mail/ui/OnePaneController;->mInbox:Lcom/android/mail/providers/Folder;

    const-string v2, "tag-conversation-list"

    invoke-direct {p0, v0, v1, v2}, Lcom/android/mail/ui/OnePaneController;->replaceFragment(Landroid/app/Fragment;ILjava/lang/String;)I

    move-result v2

    iput v2, p0, Lcom/android/mail/ui/OnePaneController;->mLastInboxConversationListTransactionId:I

    iput v4, p0, Lcom/android/mail/ui/OnePaneController;->mLastFolderListTransactionId:I

    iput v4, p0, Lcom/android/mail/ui/OnePaneController;->mLastConversationListTransactionId:I

    goto :goto_2
.end method

.method public showFolderList()V
    .locals 4

    const/4 v1, 0x0

    const/4 v3, 0x0

    iget-object v0, p0, Lcom/android/mail/ui/OnePaneController;->mAccount:Lcom/android/mail/providers/Account;

    if-nez v0, :cond_0

    sget-object v0, Lcom/android/mail/ui/OnePaneController;->LOG_TAG:Ljava/lang/String;

    const-string v1, "Null account in showFolderList"

    new-array v2, v3, [Ljava/lang/Object;

    invoke-static {v0, v1, v2}, Lcom/android/mail/utils/LogUtils;->e(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)I

    :goto_0
    return-void

    :cond_0
    invoke-virtual {p0, v1}, Lcom/android/mail/ui/OnePaneController;->setHierarchyFolder(Lcom/android/mail/providers/Folder;)V

    iget-object v0, p0, Lcom/android/mail/ui/OnePaneController;->mViewMode:Lcom/android/mail/ui/ViewMode;

    invoke-virtual {v0}, Lcom/android/mail/ui/ViewMode;->enterFolderListMode()Z

    invoke-virtual {p0}, Lcom/android/mail/ui/OnePaneController;->enableCabMode()V

    iget-object v0, p0, Lcom/android/mail/ui/OnePaneController;->mAccount:Lcom/android/mail/providers/Account;

    iget-object v0, v0, Lcom/android/mail/providers/Account;->folderListUri:Landroid/net/Uri;

    invoke-static {v1, v0, v3}, Lcom/android/mail/ui/FolderListFragment;->newInstance(Lcom/android/mail/providers/Folder;Landroid/net/Uri;Z)Lcom/android/mail/ui/FolderListFragment;

    move-result-object v0

    const/16 v1, 0x1001

    const-string v2, "tag-folder-list"

    invoke-direct {p0, v0, v1, v2}, Lcom/android/mail/ui/OnePaneController;->replaceFragment(Landroid/app/Fragment;ILjava/lang/String;)I

    move-result v0

    iput v0, p0, Lcom/android/mail/ui/OnePaneController;->mLastFolderListTransactionId:I

    iput-boolean v3, p0, Lcom/android/mail/ui/OnePaneController;->mConversationListVisible:Z

    invoke-virtual {p0, v3}, Lcom/android/mail/ui/OnePaneController;->onConversationVisibilityChanged(Z)V

    invoke-virtual {p0, v3}, Lcom/android/mail/ui/OnePaneController;->onConversationListVisibilityChanged(Z)V

    goto :goto_0
.end method

.method public showWaitForInitialization()V
    .locals 3

    invoke-super {p0}, Lcom/android/mail/ui/AbstractActivityController;->showWaitForInitialization()V

    invoke-virtual {p0}, Lcom/android/mail/ui/OnePaneController;->getWaitFragment()Lcom/android/mail/ui/WaitFragment;

    move-result-object v0

    const/16 v1, 0x1001

    const-string v2, "wait-fragment"

    invoke-direct {p0, v0, v1, v2}, Lcom/android/mail/ui/OnePaneController;->replaceFragment(Landroid/app/Fragment;ILjava/lang/String;)I

    return-void
.end method
