.class public Lcom/android/mail/ui/AutoFitPromptDialogFragment;
.super Landroid/app/DialogFragment;
.source "AutoFitPromptDialogFragment.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Landroid/app/DialogFragment;-><init>()V

    return-void
.end method

.method static synthetic access$000(Lcom/android/mail/ui/AutoFitPromptDialogFragment;I)V
    .locals 0
    .param p0    # Lcom/android/mail/ui/AutoFitPromptDialogFragment;
    .param p1    # I

    invoke-direct {p0, p1}, Lcom/android/mail/ui/AutoFitPromptDialogFragment;->saveAutoFitSetting(I)V

    return-void
.end method

.method public static newInstance(Landroid/net/Uri;)Lcom/android/mail/ui/AutoFitPromptDialogFragment;
    .locals 3
    .param p0    # Landroid/net/Uri;

    new-instance v1, Lcom/android/mail/ui/AutoFitPromptDialogFragment;

    invoke-direct {v1}, Lcom/android/mail/ui/AutoFitPromptDialogFragment;-><init>()V

    new-instance v0, Landroid/os/Bundle;

    const/4 v2, 0x1

    invoke-direct {v0, v2}, Landroid/os/Bundle;-><init>(I)V

    const-string v2, "updateSettingsUri"

    invoke-virtual {v0, v2, p0}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    invoke-virtual {v1, v0}, Lcom/android/mail/ui/AutoFitPromptDialogFragment;->setArguments(Landroid/os/Bundle;)V

    return-object v1
.end method

.method private saveAutoFitSetting(I)V
    .locals 6
    .param p1    # I

    const/4 v5, 0x0

    invoke-virtual {p0}, Lcom/android/mail/ui/AutoFitPromptDialogFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    if-nez v0, :cond_0

    :goto_0
    return-void

    :cond_0
    new-instance v2, Landroid/content/ContentValues;

    const/4 v3, 0x1

    invoke-direct {v2, v3}, Landroid/content/ContentValues;-><init>(I)V

    const-string v3, "conversation_view_mode"

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    invoke-virtual {p0}, Lcom/android/mail/ui/AutoFitPromptDialogFragment;->getArguments()Landroid/os/Bundle;

    move-result-object v3

    const-string v4, "updateSettingsUri"

    invoke-virtual {v3, v4}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v3

    check-cast v3, Landroid/net/Uri;

    invoke-virtual {v1, v3, v2, v5, v5}, Landroid/content/ContentResolver;->update(Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    goto :goto_0
.end method


# virtual methods
.method public onCancel(Landroid/content/DialogInterface;)V
    .locals 1
    .param p1    # Landroid/content/DialogInterface;

    invoke-super {p0, p1}, Landroid/app/DialogFragment;->onCancel(Landroid/content/DialogInterface;)V

    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/android/mail/ui/AutoFitPromptDialogFragment;->saveAutoFitSetting(I)V

    return-void
.end method

.method public onCreateDialog(Landroid/os/Bundle;)Landroid/app/Dialog;
    .locals 3
    .param p1    # Landroid/os/Bundle;

    new-instance v0, Landroid/app/AlertDialog$Builder;

    invoke-virtual {p0}, Lcom/android/mail/ui/AutoFitPromptDialogFragment;->getActivity()Landroid/app/Activity;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    const v1, 0x7f09011d

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    const v1, 0x7f09011e

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setMessage(I)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    const v1, 0x7f0900e3

    new-instance v2, Lcom/android/mail/ui/AutoFitPromptDialogFragment$2;

    invoke-direct {v2, p0}, Lcom/android/mail/ui/AutoFitPromptDialogFragment$2;-><init>(Lcom/android/mail/ui/AutoFitPromptDialogFragment;)V

    invoke-virtual {v0, v1, v2}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    const v1, 0x7f0900e4

    new-instance v2, Lcom/android/mail/ui/AutoFitPromptDialogFragment$1;

    invoke-direct {v2, p0}, Lcom/android/mail/ui/AutoFitPromptDialogFragment$1;-><init>(Lcom/android/mail/ui/AutoFitPromptDialogFragment;)V

    invoke-virtual {v0, v1, v2}, Landroid/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v0

    return-object v0
.end method
