.class public Lcom/android/mail/ui/MailActionBarView;
.super Landroid/widget/LinearLayout;
.source "MailActionBarView.java"

# interfaces
.implements Landroid/view/MenuItem$OnActionExpandListener;
.implements Landroid/widget/SearchView$OnQueryTextListener;
.implements Landroid/widget/SearchView$OnSuggestionListener;
.implements Lcom/android/mail/ui/SubjectDisplayChanger;
.implements Lcom/android/mail/ui/ViewMode$ModeChangeListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/mail/ui/MailActionBarView$FolderObserver;
    }
.end annotation


# static fields
.field public static final LOG_TAG:Ljava/lang/String;


# instance fields
.field private mAccount:Lcom/android/mail/providers/Account;

.field private final mAccountObserver:Lcom/android/mail/providers/AccountObserver;

.field protected mActionBar:Landroid/app/ActionBar;

.field protected mActivity:Lcom/android/mail/ui/ControllableActivity;

.field protected mController:Lcom/android/mail/ui/ActivityController;

.field private mCurrentConversation:Lcom/android/mail/providers/Conversation;

.field private mFolder:Lcom/android/mail/providers/Folder;

.field private mFolderObserver:Landroid/database/DataSetObserver;

.field private mFolderSettingsItem:Landroid/view/MenuItem;

.field private final mHandler:Landroid/os/Handler;

.field private mHasManyAccounts:Z

.field private mHelpItem:Landroid/view/MenuItem;

.field private final mInvalidateMenu:Ljava/lang/Runnable;

.field private final mIsOnTablet:Z

.field private mMode:I

.field private mRefreshActionView:Landroid/view/View;

.field private mRefreshInProgress:Z

.field private mRefreshItem:Landroid/view/MenuItem;

.field private mSearch:Landroid/view/MenuItem;

.field private mSearchWidget:Landroid/widget/SearchView;

.field private mSendFeedbackItem:Landroid/view/MenuItem;

.field private final mShowConversationSubject:Z

.field private final mSnippetLayoutListener:Landroid/view/View$OnLayoutChangeListener;

.field private mSpinner:Lcom/android/mail/ui/MailSpinner;

.field private mSpinnerAdapter:Lcom/android/mail/AccountSpinnerAdapter;

.field private mSubjectView:Lcom/android/mail/browse/SnippetTextView;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    invoke-static {}, Lcom/android/mail/utils/LogTag;->getLogTag()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/android/mail/ui/MailActionBarView;->LOG_TAG:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1    # Landroid/content/Context;

    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/android/mail/ui/MailActionBarView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/util/AttributeSet;

    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/android/mail/ui/MailActionBarView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 2
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/util/AttributeSet;
    .param p3    # I

    invoke-direct {p0, p1, p2, p3}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    const/4 v1, 0x0

    iput v1, p0, Lcom/android/mail/ui/MailActionBarView;->mMode:I

    new-instance v1, Landroid/os/Handler;

    invoke-direct {v1}, Landroid/os/Handler;-><init>()V

    iput-object v1, p0, Lcom/android/mail/ui/MailActionBarView;->mHandler:Landroid/os/Handler;

    new-instance v1, Lcom/android/mail/ui/MailActionBarView$1;

    invoke-direct {v1, p0}, Lcom/android/mail/ui/MailActionBarView$1;-><init>(Lcom/android/mail/ui/MailActionBarView;)V

    iput-object v1, p0, Lcom/android/mail/ui/MailActionBarView;->mInvalidateMenu:Ljava/lang/Runnable;

    new-instance v1, Lcom/android/mail/ui/MailActionBarView$2;

    invoke-direct {v1, p0}, Lcom/android/mail/ui/MailActionBarView$2;-><init>(Lcom/android/mail/ui/MailActionBarView;)V

    iput-object v1, p0, Lcom/android/mail/ui/MailActionBarView;->mAccountObserver:Lcom/android/mail/providers/AccountObserver;

    new-instance v1, Lcom/android/mail/ui/MailActionBarView$3;

    invoke-direct {v1, p0}, Lcom/android/mail/ui/MailActionBarView$3;-><init>(Lcom/android/mail/ui/MailActionBarView;)V

    iput-object v1, p0, Lcom/android/mail/ui/MailActionBarView;->mSnippetLayoutListener:Landroid/view/View$OnLayoutChangeListener;

    invoke-virtual {p0}, Lcom/android/mail/ui/MailActionBarView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0f0003

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getBoolean(I)Z

    move-result v1

    iput-boolean v1, p0, Lcom/android/mail/ui/MailActionBarView;->mShowConversationSubject:Z

    invoke-static {v0}, Lcom/android/mail/utils/Utils;->useTabletUI(Landroid/content/res/Resources;)Z

    move-result v1

    iput-boolean v1, p0, Lcom/android/mail/ui/MailActionBarView;->mIsOnTablet:Z

    return-void
.end method

.method static synthetic access$000(Lcom/android/mail/ui/MailActionBarView;Lcom/android/mail/providers/Account;)V
    .locals 0
    .param p0    # Lcom/android/mail/ui/MailActionBarView;
    .param p1    # Lcom/android/mail/providers/Account;

    invoke-direct {p0, p1}, Lcom/android/mail/ui/MailActionBarView;->updateAccount(Lcom/android/mail/providers/Account;)V

    return-void
.end method

.method static synthetic access$100(Lcom/android/mail/ui/MailActionBarView;)Lcom/android/mail/providers/Account;
    .locals 1
    .param p0    # Lcom/android/mail/ui/MailActionBarView;

    iget-object v0, p0, Lcom/android/mail/ui/MailActionBarView;->mAccount:Lcom/android/mail/providers/Account;

    return-object v0
.end method

.method static synthetic access$200(Lcom/android/mail/ui/MailActionBarView;)Lcom/android/mail/ui/MailSpinner;
    .locals 1
    .param p0    # Lcom/android/mail/ui/MailActionBarView;

    iget-object v0, p0, Lcom/android/mail/ui/MailActionBarView;->mSpinner:Lcom/android/mail/ui/MailSpinner;

    return-object v0
.end method

.method static synthetic access$300(Landroid/app/ActionBar;)Z
    .locals 1
    .param p0    # Landroid/app/ActionBar;

    invoke-static {p0}, Lcom/android/mail/ui/MailActionBarView;->actionBarReportsMultipleLineTitle(Landroid/app/ActionBar;)Z

    move-result v0

    return v0
.end method

.method static synthetic access$400(Lcom/android/mail/ui/MailActionBarView;I)V
    .locals 0
    .param p0    # Lcom/android/mail/ui/MailActionBarView;
    .param p1    # I

    invoke-direct {p0, p1}, Lcom/android/mail/ui/MailActionBarView;->setTitleModeFlags(I)V

    return-void
.end method

.method private static actionBarReportsMultipleLineTitle(Landroid/app/ActionBar;)Z
    .locals 3

    const/4 v0, 0x0

    if-eqz p0, :cond_0

    :try_start_0
    const-class v1, Landroid/app/ActionBar;

    const-string v2, "DISPLAY_TITLE_MULTIPLE_LINES"

    invoke-virtual {v1, v2}, Ljava/lang/Class;->getField(Ljava/lang/String;)Ljava/lang/reflect/Field;
    :try_end_0
    .catch Ljava/lang/NoSuchFieldException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    if-eqz v1, :cond_0

    const/4 v0, 0x1

    :cond_0
    :goto_0
    return v0

    :catch_0
    move-exception v1

    goto :goto_0
.end method

.method private final closeSearchField()V
    .locals 1

    iget-object v0, p0, Lcom/android/mail/ui/MailActionBarView;->mSearch:Landroid/view/MenuItem;

    if-nez v0, :cond_0

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/android/mail/ui/MailActionBarView;->mSearch:Landroid/view/MenuItem;

    invoke-interface {v0}, Landroid/view/MenuItem;->collapseActionView()Z

    goto :goto_0
.end method

.method private final enableDisableSpinnner()V
    .locals 5

    const/4 v2, 0x1

    const/4 v3, 0x0

    iget-boolean v4, p0, Lcom/android/mail/ui/MailActionBarView;->mIsOnTablet:Z

    if-nez v4, :cond_0

    :goto_0
    return-void

    :cond_0
    iget v4, p0, Lcom/android/mail/ui/MailActionBarView;->mMode:I

    invoke-static {v4}, Lcom/android/mail/ui/ViewMode;->isConversationMode(I)Z

    move-result v4

    if-eqz v4, :cond_2

    iget-object v4, p0, Lcom/android/mail/ui/MailActionBarView;->mSpinnerAdapter:Lcom/android/mail/AccountSpinnerAdapter;

    invoke-virtual {v4}, Lcom/android/mail/AccountSpinnerAdapter;->hasRecentFolders()Z

    move-result v4

    if-eqz v4, :cond_2

    move v1, v2

    :goto_1
    iget-boolean v4, p0, Lcom/android/mail/ui/MailActionBarView;->mHasManyAccounts:Z

    if-nez v4, :cond_1

    if-eqz v1, :cond_3

    :cond_1
    move v0, v2

    :goto_2
    iget-object v2, p0, Lcom/android/mail/ui/MailActionBarView;->mSpinner:Lcom/android/mail/ui/MailSpinner;

    invoke-virtual {v2, v0}, Lcom/android/mail/ui/MailSpinner;->changeEnabledState(Z)V

    goto :goto_0

    :cond_2
    move v1, v3

    goto :goto_1

    :cond_3
    move v0, v3

    goto :goto_2
.end method

.method private onRefreshStarted()V
    .locals 1

    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/android/mail/ui/MailActionBarView;->setRefreshInProgress(Z)Z

    return-void
.end method

.method private onRefreshStopped()V
    .locals 1

    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/android/mail/ui/MailActionBarView;->setRefreshInProgress(Z)Z

    return-void
.end method

.method private setFoldersMode()V
    .locals 2

    const/16 v0, 0x8

    invoke-direct {p0, v0}, Lcom/android/mail/ui/MailActionBarView;->setTitleModeFlags(I)V

    iget-object v0, p0, Lcom/android/mail/ui/MailActionBarView;->mActionBar:Landroid/app/ActionBar;

    const v1, 0x7f0900f4

    invoke-virtual {v0, v1}, Landroid/app/ActionBar;->setTitle(I)V

    iget-object v0, p0, Lcom/android/mail/ui/MailActionBarView;->mActionBar:Landroid/app/ActionBar;

    iget-object v1, p0, Lcom/android/mail/ui/MailActionBarView;->mAccount:Lcom/android/mail/providers/Account;

    iget-object v1, v1, Lcom/android/mail/providers/Account;->name:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/app/ActionBar;->setSubtitle(Ljava/lang/CharSequence;)V

    return-void
.end method

.method private setRefreshInProgress(Z)Z
    .locals 2
    .param p1    # Z

    iget-boolean v0, p0, Lcom/android/mail/ui/MailActionBarView;->mRefreshInProgress:Z

    if-eq p1, v0, :cond_2

    iput-boolean p1, p0, Lcom/android/mail/ui/MailActionBarView;->mRefreshInProgress:Z

    iget-object v0, p0, Lcom/android/mail/ui/MailActionBarView;->mSearch:Landroid/view/MenuItem;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/mail/ui/MailActionBarView;->mSearch:Landroid/view/MenuItem;

    invoke-interface {v0}, Landroid/view/MenuItem;->isActionViewExpanded()Z

    move-result v0

    if-nez v0, :cond_1

    :cond_0
    iget-object v0, p0, Lcom/android/mail/ui/MailActionBarView;->mHandler:Landroid/os/Handler;

    iget-object v1, p0, Lcom/android/mail/ui/MailActionBarView;->mInvalidateMenu:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    :cond_1
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_2
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private setTitleModeFlags(I)V
    .locals 3
    .param p1    # I

    const/16 v0, 0x38

    iget-object v1, p0, Lcom/android/mail/ui/MailActionBarView;->mActionBar:Landroid/app/ActionBar;

    const/16 v2, 0x38

    invoke-virtual {v1, p1, v2}, Landroid/app/ActionBar;->setDisplayOptions(II)V

    return-void
.end method

.method private showNavList()V
    .locals 2

    const/16 v0, 0x10

    invoke-direct {p0, v0}, Lcom/android/mail/ui/MailActionBarView;->setTitleModeFlags(I)V

    iget-object v0, p0, Lcom/android/mail/ui/MailActionBarView;->mSpinner:Lcom/android/mail/ui/MailSpinner;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/android/mail/ui/MailSpinner;->setVisibility(I)V

    iget-object v0, p0, Lcom/android/mail/ui/MailActionBarView;->mSubjectView:Lcom/android/mail/browse/SnippetTextView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Lcom/android/mail/browse/SnippetTextView;->setVisibility(I)V

    return-void
.end method

.method private updateAccount(Lcom/android/mail/providers/Account;)V
    .locals 5
    .param p1    # Lcom/android/mail/providers/Account;

    iput-object p1, p0, Lcom/android/mail/ui/MailActionBarView;->mAccount:Lcom/android/mail/providers/Account;

    iget-object v2, p0, Lcom/android/mail/ui/MailActionBarView;->mAccount:Lcom/android/mail/providers/Account;

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/android/mail/ui/MailActionBarView;->mActivity:Lcom/android/mail/ui/ControllableActivity;

    invoke-interface {v2}, Lcom/android/mail/ui/ControllableActivity;->getActivityContext()Landroid/content/Context;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    new-instance v0, Landroid/os/Bundle;

    const/4 v2, 0x1

    invoke-direct {v0, v2}, Landroid/os/Bundle;-><init>(I)V

    const-string v2, "account"

    invoke-virtual {v0, v2, p1}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    iget-object v2, p0, Lcom/android/mail/ui/MailActionBarView;->mAccount:Lcom/android/mail/providers/Account;

    iget-object v2, v2, Lcom/android/mail/providers/Account;->uri:Landroid/net/Uri;

    const-string v3, "set_current_account"

    iget-object v4, p0, Lcom/android/mail/ui/MailActionBarView;->mAccount:Lcom/android/mail/providers/Account;

    iget-object v4, v4, Lcom/android/mail/providers/Account;->uri:Landroid/net/Uri;

    invoke-virtual {v4}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v1, v2, v3, v4, v0}, Landroid/content/ContentResolver;->call(Landroid/net/Uri;Ljava/lang/String;Ljava/lang/String;Landroid/os/Bundle;)Landroid/os/Bundle;

    :cond_0
    return-void
.end method


# virtual methods
.method public attach()V
    .locals 0

    return-void
.end method

.method public clearSubject()V
    .locals 2

    const/4 v1, 0x0

    iget-boolean v0, p0, Lcom/android/mail/ui/MailActionBarView;->mShowConversationSubject:Z

    if-nez v0, :cond_0

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/android/mail/ui/MailActionBarView;->mActionBar:Landroid/app/ActionBar;

    invoke-virtual {v0, v1}, Landroid/app/ActionBar;->setTitle(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lcom/android/mail/ui/MailActionBarView;->mActionBar:Landroid/app/ActionBar;

    invoke-virtual {v0, v1}, Landroid/app/ActionBar;->setSubtitle(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lcom/android/mail/ui/MailActionBarView;->mSubjectView:Lcom/android/mail/browse/SnippetTextView;

    invoke-virtual {v0, v1}, Lcom/android/mail/browse/SnippetTextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0
.end method

.method public collapseSearch()V
    .locals 1

    iget-object v0, p0, Lcom/android/mail/ui/MailActionBarView;->mSearch:Landroid/view/MenuItem;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/mail/ui/MailActionBarView;->mSearch:Landroid/view/MenuItem;

    invoke-interface {v0}, Landroid/view/MenuItem;->collapseActionView()Z

    :cond_0
    return-void
.end method

.method public expandSearch()V
    .locals 1

    iget-object v0, p0, Lcom/android/mail/ui/MailActionBarView;->mSearch:Landroid/view/MenuItem;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/mail/ui/MailActionBarView;->mSearch:Landroid/view/MenuItem;

    invoke-interface {v0}, Landroid/view/MenuItem;->expandActionView()Z

    :cond_0
    return-void
.end method

.method protected getMode()I
    .locals 1

    iget v0, p0, Lcom/android/mail/ui/MailActionBarView;->mMode:I

    return v0
.end method

.method public getOptionsMenuId()I
    .locals 2

    const/4 v1, 0x7

    new-array v0, v1, [I

    fill-array-data v0, :array_0

    iget v1, p0, Lcom/android/mail/ui/MailActionBarView;->mMode:I

    aget v1, v0, v1

    return v1

    nop

    :array_0
    .array-data 4
        0x7f110002
        0x7f110001
        0x7f110002
        0x7f110006
        0x7f110003
        0x7f110005
        0x7f11000e
    .end array-data
.end method

.method protected getSearch()Landroid/view/MenuItem;
    .locals 1

    iget-object v0, p0, Lcom/android/mail/ui/MailActionBarView;->mSearch:Landroid/view/MenuItem;

    return-object v0
.end method

.method public getUnshownSubject(Ljava/lang/String;)Ljava/lang/String;
    .locals 1
    .param p1    # Ljava/lang/String;

    iget-boolean v0, p0, Lcom/android/mail/ui/MailActionBarView;->mShowConversationSubject:Z

    if-nez v0, :cond_0

    :goto_0
    return-object p1

    :cond_0
    iget-object v0, p0, Lcom/android/mail/ui/MailActionBarView;->mSubjectView:Lcom/android/mail/browse/SnippetTextView;

    invoke-virtual {v0, p1}, Lcom/android/mail/browse/SnippetTextView;->getTextRemainder(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    goto :goto_0
.end method

.method public initialize(Lcom/android/mail/ui/ControllableActivity;Lcom/android/mail/ui/ActivityController;Lcom/android/mail/ui/ViewMode;Landroid/app/ActionBar;Lcom/android/mail/ui/RecentFolderList;)V
    .locals 4
    .param p1    # Lcom/android/mail/ui/ControllableActivity;
    .param p2    # Lcom/android/mail/ui/ActivityController;
    .param p3    # Lcom/android/mail/ui/ViewMode;
    .param p4    # Landroid/app/ActionBar;
    .param p5    # Lcom/android/mail/ui/RecentFolderList;

    iput-object p4, p0, Lcom/android/mail/ui/MailActionBarView;->mActionBar:Landroid/app/ActionBar;

    iput-object p2, p0, Lcom/android/mail/ui/MailActionBarView;->mController:Lcom/android/mail/ui/ActivityController;

    iput-object p1, p0, Lcom/android/mail/ui/MailActionBarView;->mActivity:Lcom/android/mail/ui/ControllableActivity;

    new-instance v2, Lcom/android/mail/ui/MailActionBarView$FolderObserver;

    const/4 v3, 0x0

    invoke-direct {v2, p0, v3}, Lcom/android/mail/ui/MailActionBarView$FolderObserver;-><init>(Lcom/android/mail/ui/MailActionBarView;Lcom/android/mail/ui/MailActionBarView$1;)V

    iput-object v2, p0, Lcom/android/mail/ui/MailActionBarView;->mFolderObserver:Landroid/database/DataSetObserver;

    iget-object v2, p0, Lcom/android/mail/ui/MailActionBarView;->mController:Lcom/android/mail/ui/ActivityController;

    iget-object v3, p0, Lcom/android/mail/ui/MailActionBarView;->mFolderObserver:Landroid/database/DataSetObserver;

    invoke-interface {v2, v3}, Lcom/android/mail/ui/ActivityController;->registerFolderObserver(Landroid/database/DataSetObserver;)V

    invoke-virtual {p0}, Lcom/android/mail/ui/MailActionBarView;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-static {v2}, Lcom/android/mail/utils/Utils;->useTabletUI(Landroid/content/res/Resources;)Z

    move-result v2

    if-nez v2, :cond_0

    const/4 v1, 0x1

    :goto_0
    new-instance v2, Lcom/android/mail/AccountSpinnerAdapter;

    invoke-direct {v2, p1, v0, v1}, Lcom/android/mail/AccountSpinnerAdapter;-><init>(Lcom/android/mail/ui/ControllableActivity;Landroid/content/Context;Z)V

    iput-object v2, p0, Lcom/android/mail/ui/MailActionBarView;->mSpinnerAdapter:Lcom/android/mail/AccountSpinnerAdapter;

    const v2, 0x7f080023

    invoke-virtual {p0, v2}, Lcom/android/mail/ui/MailActionBarView;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Lcom/android/mail/ui/MailSpinner;

    iput-object v2, p0, Lcom/android/mail/ui/MailActionBarView;->mSpinner:Lcom/android/mail/ui/MailSpinner;

    iget-object v2, p0, Lcom/android/mail/ui/MailActionBarView;->mSpinner:Lcom/android/mail/ui/MailSpinner;

    iget-object v3, p0, Lcom/android/mail/ui/MailActionBarView;->mSpinnerAdapter:Lcom/android/mail/AccountSpinnerAdapter;

    invoke-virtual {v2, v3}, Lcom/android/mail/ui/MailSpinner;->setAdapter(Lcom/android/mail/AccountSpinnerAdapter;)V

    iget-object v2, p0, Lcom/android/mail/ui/MailActionBarView;->mSpinner:Lcom/android/mail/ui/MailSpinner;

    iget-object v3, p0, Lcom/android/mail/ui/MailActionBarView;->mController:Lcom/android/mail/ui/ActivityController;

    invoke-virtual {v2, v3}, Lcom/android/mail/ui/MailSpinner;->setController(Lcom/android/mail/ui/ActivityController;)V

    iget-object v2, p0, Lcom/android/mail/ui/MailActionBarView;->mAccountObserver:Lcom/android/mail/providers/AccountObserver;

    invoke-interface {p1}, Lcom/android/mail/ui/ControllableActivity;->getAccountController()Lcom/android/mail/ui/AccountController;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/android/mail/providers/AccountObserver;->initialize(Lcom/android/mail/ui/AccountController;)Lcom/android/mail/providers/Account;

    move-result-object v2

    invoke-direct {p0, v2}, Lcom/android/mail/ui/MailActionBarView;->updateAccount(Lcom/android/mail/providers/Account;)V

    return-void

    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public onCreateOptionsMenu(Landroid/view/Menu;)Z
    .locals 5
    .param p1    # Landroid/view/Menu;

    const/4 v3, 0x1

    iget v2, p0, Lcom/android/mail/ui/MailActionBarView;->mMode:I

    if-nez v2, :cond_0

    const/4 v2, 0x0

    :goto_0
    return v2

    :cond_0
    const v2, 0x7f080124

    invoke-interface {p1, v2}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v2

    iput-object v2, p0, Lcom/android/mail/ui/MailActionBarView;->mSearch:Landroid/view/MenuItem;

    iget-object v2, p0, Lcom/android/mail/ui/MailActionBarView;->mSearch:Landroid/view/MenuItem;

    if-eqz v2, :cond_1

    iget-object v2, p0, Lcom/android/mail/ui/MailActionBarView;->mSearch:Landroid/view/MenuItem;

    invoke-interface {v2}, Landroid/view/MenuItem;->getActionView()Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/SearchView;

    iput-object v2, p0, Lcom/android/mail/ui/MailActionBarView;->mSearchWidget:Landroid/widget/SearchView;

    iget-object v2, p0, Lcom/android/mail/ui/MailActionBarView;->mSearch:Landroid/view/MenuItem;

    invoke-interface {v2, p0}, Landroid/view/MenuItem;->setOnActionExpandListener(Landroid/view/MenuItem$OnActionExpandListener;)Landroid/view/MenuItem;

    iget-object v2, p0, Lcom/android/mail/ui/MailActionBarView;->mActivity:Lcom/android/mail/ui/ControllableActivity;

    invoke-interface {v2}, Lcom/android/mail/ui/ControllableActivity;->getActivityContext()Landroid/content/Context;

    move-result-object v2

    const-string v4, "search"

    invoke-virtual {v2, v4}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/app/SearchManager;

    if-eqz v1, :cond_1

    iget-object v2, p0, Lcom/android/mail/ui/MailActionBarView;->mSearchWidget:Landroid/widget/SearchView;

    if-eqz v2, :cond_1

    iget-object v2, p0, Lcom/android/mail/ui/MailActionBarView;->mActivity:Lcom/android/mail/ui/ControllableActivity;

    invoke-interface {v2}, Lcom/android/mail/ui/ControllableActivity;->getComponentName()Landroid/content/ComponentName;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/app/SearchManager;->getSearchableInfo(Landroid/content/ComponentName;)Landroid/app/SearchableInfo;

    move-result-object v0

    iget-object v2, p0, Lcom/android/mail/ui/MailActionBarView;->mSearchWidget:Landroid/widget/SearchView;

    invoke-virtual {v2, v0}, Landroid/widget/SearchView;->setSearchableInfo(Landroid/app/SearchableInfo;)V

    iget-object v2, p0, Lcom/android/mail/ui/MailActionBarView;->mSearchWidget:Landroid/widget/SearchView;

    invoke-virtual {v2, p0}, Landroid/widget/SearchView;->setOnQueryTextListener(Landroid/widget/SearchView$OnQueryTextListener;)V

    iget-object v2, p0, Lcom/android/mail/ui/MailActionBarView;->mSearchWidget:Landroid/widget/SearchView;

    invoke-virtual {v2, p0}, Landroid/widget/SearchView;->setOnSuggestionListener(Landroid/widget/SearchView$OnSuggestionListener;)V

    iget-object v2, p0, Lcom/android/mail/ui/MailActionBarView;->mSearchWidget:Landroid/widget/SearchView;

    invoke-virtual {v2, v3}, Landroid/widget/SearchView;->setIconifiedByDefault(Z)V

    :cond_1
    const v2, 0x7f080117

    invoke-interface {p1, v2}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v2

    iput-object v2, p0, Lcom/android/mail/ui/MailActionBarView;->mHelpItem:Landroid/view/MenuItem;

    const v2, 0x7f080116

    invoke-interface {p1, v2}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v2

    iput-object v2, p0, Lcom/android/mail/ui/MailActionBarView;->mSendFeedbackItem:Landroid/view/MenuItem;

    const v2, 0x7f080125

    invoke-interface {p1, v2}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v2

    iput-object v2, p0, Lcom/android/mail/ui/MailActionBarView;->mRefreshItem:Landroid/view/MenuItem;

    const v2, 0x7f080126

    invoke-interface {p1, v2}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v2

    iput-object v2, p0, Lcom/android/mail/ui/MailActionBarView;->mFolderSettingsItem:Landroid/view/MenuItem;

    move v2, v3

    goto :goto_0
.end method

.method public onDestroy()V
    .locals 2

    iget-object v0, p0, Lcom/android/mail/ui/MailActionBarView;->mFolderObserver:Landroid/database/DataSetObserver;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/mail/ui/MailActionBarView;->mController:Lcom/android/mail/ui/ActivityController;

    iget-object v1, p0, Lcom/android/mail/ui/MailActionBarView;->mFolderObserver:Landroid/database/DataSetObserver;

    invoke-interface {v0, v1}, Lcom/android/mail/ui/ActivityController;->unregisterFolderObserver(Landroid/database/DataSetObserver;)V

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/android/mail/ui/MailActionBarView;->mFolderObserver:Landroid/database/DataSetObserver;

    :cond_0
    iget-object v0, p0, Lcom/android/mail/ui/MailActionBarView;->mSpinnerAdapter:Lcom/android/mail/AccountSpinnerAdapter;

    invoke-virtual {v0}, Lcom/android/mail/AccountSpinnerAdapter;->destroy()V

    iget-object v0, p0, Lcom/android/mail/ui/MailActionBarView;->mAccountObserver:Lcom/android/mail/providers/AccountObserver;

    invoke-virtual {v0}, Lcom/android/mail/providers/AccountObserver;->unregisterAndDestroy()V

    return-void
.end method

.method protected onFinishInflate()V
    .locals 1

    invoke-super {p0}, Landroid/widget/LinearLayout;->onFinishInflate()V

    const v0, 0x7f080022

    invoke-virtual {p0, v0}, Lcom/android/mail/ui/MailActionBarView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/android/mail/browse/SnippetTextView;

    iput-object v0, p0, Lcom/android/mail/ui/MailActionBarView;->mSubjectView:Lcom/android/mail/browse/SnippetTextView;

    return-void
.end method

.method public onFolderUpdated(Lcom/android/mail/providers/Folder;)V
    .locals 2
    .param p1    # Lcom/android/mail/providers/Folder;

    iget-object v1, p0, Lcom/android/mail/ui/MailActionBarView;->mSpinner:Lcom/android/mail/ui/MailSpinner;

    invoke-virtual {v1, p1}, Lcom/android/mail/ui/MailSpinner;->onFolderUpdated(Lcom/android/mail/providers/Folder;)V

    invoke-virtual {p1}, Lcom/android/mail/providers/Folder;->isSyncInProgress()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-direct {p0}, Lcom/android/mail/ui/MailActionBarView;->onRefreshStarted()V

    :goto_0
    iget-object v1, p0, Lcom/android/mail/ui/MailActionBarView;->mController:Lcom/android/mail/ui/ActivityController;

    if-nez v1, :cond_2

    const/4 v0, 0x0

    :goto_1
    invoke-static {v0}, Lcom/android/mail/ConversationListContext;->isSearchResult(Lcom/android/mail/ConversationListContext;)Z

    move-result v1

    if-nez v1, :cond_0

    invoke-direct {p0}, Lcom/android/mail/ui/MailActionBarView;->closeSearchField()V

    :cond_0
    return-void

    :cond_1
    invoke-direct {p0}, Lcom/android/mail/ui/MailActionBarView;->onRefreshStopped()V

    goto :goto_0

    :cond_2
    iget-object v1, p0, Lcom/android/mail/ui/MailActionBarView;->mController:Lcom/android/mail/ui/ActivityController;

    invoke-interface {v1}, Lcom/android/mail/ui/ActivityController;->getCurrentListContext()Lcom/android/mail/ConversationListContext;

    move-result-object v0

    goto :goto_1
.end method

.method public onMenuItemActionCollapse(Landroid/view/MenuItem;)Z
    .locals 1
    .param p1    # Landroid/view/MenuItem;

    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/android/mail/ui/MailActionBarView;->setVisibility(I)V

    const/4 v0, 0x1

    return v0
.end method

.method public onMenuItemActionExpand(Landroid/view/MenuItem;)Z
    .locals 1
    .param p1    # Landroid/view/MenuItem;

    const/4 v0, 0x1

    return v0
.end method

.method public onPrepareOptionsMenu(Landroid/view/Menu;)Z
    .locals 5
    .param p1    # Landroid/view/Menu;

    const/4 v1, 0x1

    const/4 v2, 0x0

    sget-object v0, Lcom/android/mail/ui/MailActionBarView;->LOG_TAG:Ljava/lang/String;

    const-string v3, "ActionBarView.onPrepareOptionsMenu()."

    new-array v4, v2, [Ljava/lang/Object;

    invoke-static {v0, v3, v4}, Lcom/android/mail/utils/LogUtils;->d(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)I

    iget-boolean v0, p0, Lcom/android/mail/ui/MailActionBarView;->mRefreshInProgress:Z

    if-eqz v0, :cond_5

    iget-object v0, p0, Lcom/android/mail/ui/MailActionBarView;->mRefreshItem:Landroid/view/MenuItem;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/mail/ui/MailActionBarView;->mRefreshActionView:Landroid/view/View;

    if-nez v0, :cond_4

    iget-object v0, p0, Lcom/android/mail/ui/MailActionBarView;->mRefreshItem:Landroid/view/MenuItem;

    const v3, 0x7f040006

    invoke-interface {v0, v3}, Landroid/view/MenuItem;->setActionView(I)Landroid/view/MenuItem;

    iget-object v0, p0, Lcom/android/mail/ui/MailActionBarView;->mRefreshItem:Landroid/view/MenuItem;

    invoke-interface {v0}, Landroid/view/MenuItem;->getActionView()Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/android/mail/ui/MailActionBarView;->mRefreshActionView:Landroid/view/View;

    :cond_0
    :goto_0
    iget-object v0, p0, Lcom/android/mail/ui/MailActionBarView;->mHelpItem:Landroid/view/MenuItem;

    if-eqz v0, :cond_1

    iget-object v3, p0, Lcom/android/mail/ui/MailActionBarView;->mHelpItem:Landroid/view/MenuItem;

    iget-object v0, p0, Lcom/android/mail/ui/MailActionBarView;->mAccount:Lcom/android/mail/providers/Account;

    if-eqz v0, :cond_6

    iget-object v0, p0, Lcom/android/mail/ui/MailActionBarView;->mAccount:Lcom/android/mail/providers/Account;

    const v4, 0x8000

    invoke-virtual {v0, v4}, Lcom/android/mail/providers/Account;->supportsCapability(I)Z

    move-result v0

    if-eqz v0, :cond_6

    move v0, v1

    :goto_1
    invoke-interface {v3, v0}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    :cond_1
    iget-object v0, p0, Lcom/android/mail/ui/MailActionBarView;->mSendFeedbackItem:Landroid/view/MenuItem;

    if-eqz v0, :cond_2

    iget-object v3, p0, Lcom/android/mail/ui/MailActionBarView;->mSendFeedbackItem:Landroid/view/MenuItem;

    iget-object v0, p0, Lcom/android/mail/ui/MailActionBarView;->mAccount:Lcom/android/mail/providers/Account;

    if-eqz v0, :cond_7

    iget-object v0, p0, Lcom/android/mail/ui/MailActionBarView;->mAccount:Lcom/android/mail/providers/Account;

    const/high16 v4, 0x10000

    invoke-virtual {v0, v4}, Lcom/android/mail/providers/Account;->supportsCapability(I)Z

    move-result v0

    if-eqz v0, :cond_7

    move v0, v1

    :goto_2
    invoke-interface {v3, v0}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    :cond_2
    iget-object v0, p0, Lcom/android/mail/ui/MailActionBarView;->mFolderSettingsItem:Landroid/view/MenuItem;

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/android/mail/ui/MailActionBarView;->mFolderSettingsItem:Landroid/view/MenuItem;

    iget-object v3, p0, Lcom/android/mail/ui/MailActionBarView;->mFolder:Lcom/android/mail/providers/Folder;

    if-eqz v3, :cond_8

    iget-object v3, p0, Lcom/android/mail/ui/MailActionBarView;->mFolder:Lcom/android/mail/providers/Folder;

    const/16 v4, 0x200

    invoke-virtual {v3, v4}, Lcom/android/mail/providers/Folder;->supportsCapability(I)Z

    move-result v3

    if-eqz v3, :cond_8

    :goto_3
    invoke-interface {v0, v1}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    :cond_3
    iget v0, p0, Lcom/android/mail/ui/MailActionBarView;->mMode:I

    packed-switch v0, :pswitch_data_0

    :goto_4
    :pswitch_0
    return v2

    :cond_4
    iget-object v0, p0, Lcom/android/mail/ui/MailActionBarView;->mRefreshItem:Landroid/view/MenuItem;

    iget-object v3, p0, Lcom/android/mail/ui/MailActionBarView;->mRefreshActionView:Landroid/view/View;

    invoke-interface {v0, v3}, Landroid/view/MenuItem;->setActionView(Landroid/view/View;)Landroid/view/MenuItem;

    goto :goto_0

    :cond_5
    iget-object v0, p0, Lcom/android/mail/ui/MailActionBarView;->mRefreshItem:Landroid/view/MenuItem;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/mail/ui/MailActionBarView;->mRefreshItem:Landroid/view/MenuItem;

    const/4 v3, 0x0

    invoke-interface {v0, v3}, Landroid/view/MenuItem;->setActionView(Landroid/view/View;)Landroid/view/MenuItem;

    goto :goto_0

    :cond_6
    move v0, v2

    goto :goto_1

    :cond_7
    move v0, v2

    goto :goto_2

    :cond_8
    move v1, v2

    goto :goto_3

    :pswitch_1
    invoke-virtual {p0, p1}, Lcom/android/mail/ui/MailActionBarView;->setConversationModeOptions(Landroid/view/Menu;)V

    goto :goto_4

    :pswitch_2
    const v0, 0x7f080124

    iget-object v1, p0, Lcom/android/mail/ui/MailActionBarView;->mAccount:Lcom/android/mail/providers/Account;

    const/16 v3, 0x40

    invoke-virtual {v1, v3}, Lcom/android/mail/providers/Account;->supportsCapability(I)Z

    move-result v1

    invoke-static {p1, v0, v1}, Lcom/android/mail/utils/Utils;->setMenuItemVisibility(Landroid/view/Menu;IZ)V

    goto :goto_4

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_2
        :pswitch_0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public onQueryTextChange(Ljava/lang/String;)Z
    .locals 1
    .param p1    # Ljava/lang/String;

    const/4 v0, 0x0

    return v0
.end method

.method public onQueryTextSubmit(Ljava/lang/String;)Z
    .locals 3
    .param p1    # Ljava/lang/String;

    iget-object v0, p0, Lcom/android/mail/ui/MailActionBarView;->mSearch:Landroid/view/MenuItem;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/mail/ui/MailActionBarView;->mSearch:Landroid/view/MenuItem;

    invoke-interface {v0}, Landroid/view/MenuItem;->collapseActionView()Z

    iget-object v0, p0, Lcom/android/mail/ui/MailActionBarView;->mSearchWidget:Landroid/widget/SearchView;

    const-string v1, ""

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/widget/SearchView;->setQuery(Ljava/lang/CharSequence;Z)V

    :cond_0
    iget-object v0, p0, Lcom/android/mail/ui/MailActionBarView;->mController:Lcom/android/mail/ui/ActivityController;

    invoke-virtual {p1}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/android/mail/ui/ActivityController;->executeSearch(Ljava/lang/String;)V

    const/4 v0, 0x1

    return v0
.end method

.method public onSuggestionClick(I)Z
    .locals 10
    .param p1    # I

    const/4 v9, -0x1

    const/4 v6, 0x1

    const/4 v7, 0x0

    iget-object v8, p0, Lcom/android/mail/ui/MailActionBarView;->mSearchWidget:Landroid/widget/SearchView;

    invoke-virtual {v8}, Landroid/widget/SearchView;->getSuggestionsAdapter()Landroid/widget/CursorAdapter;

    move-result-object v8

    invoke-virtual {v8}, Landroid/widget/CursorAdapter;->getCursor()Landroid/database/Cursor;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-interface {v0, p1}, Landroid/database/Cursor;->moveToPosition(I)Z

    move-result v8

    if-eqz v8, :cond_0

    move v1, v6

    :goto_0
    if-nez v1, :cond_1

    sget-object v8, Lcom/android/mail/ui/MailActionBarView;->LOG_TAG:Ljava/lang/String;

    const-string v9, "onSuggestionClick: Couldn\'t get a search query"

    new-array v7, v7, [Ljava/lang/Object;

    invoke-static {v8, v9, v7}, Lcom/android/mail/utils/LogUtils;->d(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)I

    :goto_1
    return v6

    :cond_0
    move v1, v7

    goto :goto_0

    :cond_1
    invoke-virtual {p0}, Lcom/android/mail/ui/MailActionBarView;->collapseSearch()V

    iget-object v8, p0, Lcom/android/mail/ui/MailActionBarView;->mSearchWidget:Landroid/widget/SearchView;

    invoke-virtual {v8}, Landroid/widget/SearchView;->getQuery()Ljava/lang/CharSequence;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v3

    const-string v8, "suggest_intent_query"

    invoke-interface {v0, v8}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v8

    invoke-interface {v0, v8}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-static {v3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v8

    if-nez v8, :cond_3

    invoke-virtual {v2, v3}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v8

    if-eqz v8, :cond_3

    const-string v8, " "

    invoke-virtual {v3, v8}, Ljava/lang/String;->lastIndexOf(Ljava/lang/String;)I

    move-result v4

    if-le v4, v9, :cond_2

    invoke-virtual {v3, v7, v4}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v3

    :cond_2
    if-le v4, v9, :cond_3

    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v8

    if-nez v8, :cond_3

    invoke-virtual {v2, v3}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v8

    if-eqz v8, :cond_3

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v8

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v9

    if-ge v8, v9, :cond_3

    invoke-virtual {v2, v3}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v5

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v7, v5}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v8, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v8

    add-int/2addr v8, v5

    invoke-virtual {v2, v8}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    :cond_3
    iget-object v7, p0, Lcom/android/mail/ui/MailActionBarView;->mController:Lcom/android/mail/ui/ActivityController;

    invoke-virtual {v2}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v8

    invoke-interface {v7, v8}, Lcom/android/mail/ui/ActivityController;->executeSearch(Ljava/lang/String;)V

    goto :goto_1
.end method

.method public onSuggestionSelect(I)Z
    .locals 1
    .param p1    # I

    invoke-virtual {p0, p1}, Lcom/android/mail/ui/MailActionBarView;->onSuggestionClick(I)Z

    move-result v0

    return v0
.end method

.method public onViewModeChanged(I)V
    .locals 2
    .param p1    # I

    const/4 v1, 0x1

    iput p1, p0, Lcom/android/mail/ui/MailActionBarView;->mMode:I

    invoke-direct {p0}, Lcom/android/mail/ui/MailActionBarView;->enableDisableSpinnner()V

    iget-object v0, p0, Lcom/android/mail/ui/MailActionBarView;->mActivity:Lcom/android/mail/ui/ControllableActivity;

    invoke-interface {v0}, Lcom/android/mail/ui/ControllableActivity;->invalidateOptionsMenu()V

    iget-boolean v0, p0, Lcom/android/mail/ui/MailActionBarView;->mIsOnTablet:Z

    if-eqz v0, :cond_0

    iget v0, p0, Lcom/android/mail/ui/MailActionBarView;->mMode:I

    if-ne v0, v1, :cond_1

    :cond_0
    iget-object v0, p0, Lcom/android/mail/ui/MailActionBarView;->mSpinnerAdapter:Lcom/android/mail/AccountSpinnerAdapter;

    invoke-virtual {v0}, Lcom/android/mail/AccountSpinnerAdapter;->enableRecentFolders()V

    :goto_0
    iget v0, p0, Lcom/android/mail/ui/MailActionBarView;->mMode:I

    packed-switch v0, :pswitch_data_0

    :goto_1
    :pswitch_0
    return-void

    :cond_1
    iget-object v0, p0, Lcom/android/mail/ui/MailActionBarView;->mSpinnerAdapter:Lcom/android/mail/AccountSpinnerAdapter;

    invoke-virtual {v0}, Lcom/android/mail/AccountSpinnerAdapter;->disableRecentFolders()V

    goto :goto_0

    :pswitch_1
    invoke-direct {p0}, Lcom/android/mail/ui/MailActionBarView;->closeSearchField()V

    goto :goto_1

    :pswitch_2
    invoke-direct {p0}, Lcom/android/mail/ui/MailActionBarView;->closeSearchField()V

    invoke-direct {p0}, Lcom/android/mail/ui/MailActionBarView;->showNavList()V

    goto :goto_1

    :pswitch_3
    invoke-direct {p0}, Lcom/android/mail/ui/MailActionBarView;->closeSearchField()V

    iget-object v0, p0, Lcom/android/mail/ui/MailActionBarView;->mActionBar:Landroid/app/ActionBar;

    invoke-virtual {v0, v1}, Landroid/app/ActionBar;->setDisplayHomeAsUpEnabled(Z)V

    iget-boolean v0, p0, Lcom/android/mail/ui/MailActionBarView;->mShowConversationSubject:Z

    if-nez v0, :cond_2

    invoke-direct {p0}, Lcom/android/mail/ui/MailActionBarView;->showNavList()V

    goto :goto_1

    :cond_2
    invoke-virtual {p0}, Lcom/android/mail/ui/MailActionBarView;->setSnippetMode()V

    goto :goto_1

    :pswitch_4
    invoke-direct {p0}, Lcom/android/mail/ui/MailActionBarView;->closeSearchField()V

    iget-object v0, p0, Lcom/android/mail/ui/MailActionBarView;->mActionBar:Landroid/app/ActionBar;

    invoke-virtual {v0, v1}, Landroid/app/ActionBar;->setDisplayHomeAsUpEnabled(Z)V

    invoke-direct {p0}, Lcom/android/mail/ui/MailActionBarView;->setFoldersMode()V

    goto :goto_1

    :pswitch_5
    invoke-direct {p0}, Lcom/android/mail/ui/MailActionBarView;->showNavList()V

    goto :goto_1

    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_3
        :pswitch_2
        :pswitch_4
        :pswitch_0
        :pswitch_0
        :pswitch_5
    .end packed-switch
.end method

.method public removeBackButton()V
    .locals 4

    iget-object v1, p0, Lcom/android/mail/ui/MailActionBarView;->mActionBar:Landroid/app/ActionBar;

    if-nez v1, :cond_0

    :goto_0
    return-void

    :cond_0
    const/4 v0, 0x6

    iget-object v1, p0, Lcom/android/mail/ui/MailActionBarView;->mActionBar:Landroid/app/ActionBar;

    const/4 v2, 0x2

    const/4 v3, 0x6

    invoke-virtual {v1, v2, v3}, Landroid/app/ActionBar;->setDisplayOptions(II)V

    iget-object v1, p0, Lcom/android/mail/ui/MailActionBarView;->mActivity:Lcom/android/mail/ui/ControllableActivity;

    invoke-interface {v1}, Lcom/android/mail/ui/ControllableActivity;->getActionBar()Landroid/app/ActionBar;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Landroid/app/ActionBar;->setHomeButtonEnabled(Z)V

    goto :goto_0
.end method

.method public setAccounts([Lcom/android/mail/providers/Account;)V
    .locals 2
    .param p1    # [Lcom/android/mail/providers/Account;

    const/4 v0, 0x1

    iget-object v1, p0, Lcom/android/mail/ui/MailActionBarView;->mSpinnerAdapter:Lcom/android/mail/AccountSpinnerAdapter;

    invoke-virtual {v1, p1}, Lcom/android/mail/AccountSpinnerAdapter;->setAccountArray([Lcom/android/mail/providers/Account;)V

    array-length v1, p1

    if-le v1, v0, :cond_0

    :goto_0
    iput-boolean v0, p0, Lcom/android/mail/ui/MailActionBarView;->mHasManyAccounts:Z

    invoke-direct {p0}, Lcom/android/mail/ui/MailActionBarView;->enableDisableSpinnner()V

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public setBackButton()V
    .locals 3

    const/4 v2, 0x6

    iget-object v1, p0, Lcom/android/mail/ui/MailActionBarView;->mActionBar:Landroid/app/ActionBar;

    if-nez v1, :cond_0

    :goto_0
    return-void

    :cond_0
    const/4 v0, 0x6

    iget-object v1, p0, Lcom/android/mail/ui/MailActionBarView;->mActionBar:Landroid/app/ActionBar;

    invoke-virtual {v1, v2, v2}, Landroid/app/ActionBar;->setDisplayOptions(II)V

    iget-object v1, p0, Lcom/android/mail/ui/MailActionBarView;->mActivity:Lcom/android/mail/ui/ControllableActivity;

    invoke-interface {v1}, Lcom/android/mail/ui/ControllableActivity;->getActionBar()Landroid/app/ActionBar;

    move-result-object v1

    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Landroid/app/ActionBar;->setHomeButtonEnabled(Z)V

    goto :goto_0
.end method

.method public setConversationModeOptions(Landroid/view/Menu;)V
    .locals 13
    .param p1    # Landroid/view/Menu;

    const/16 v12, 0x10

    const/16 v10, 0x8

    const/4 v11, 0x2

    const/4 v6, 0x1

    const/4 v7, 0x0

    iget-object v5, p0, Lcom/android/mail/ui/MailActionBarView;->mCurrentConversation:Lcom/android/mail/providers/Conversation;

    if-nez v5, :cond_0

    :goto_0
    return-void

    :cond_0
    iget-object v5, p0, Lcom/android/mail/ui/MailActionBarView;->mCurrentConversation:Lcom/android/mail/providers/Conversation;

    invoke-virtual {v5}, Lcom/android/mail/providers/Conversation;->isImportant()Z

    move-result v5

    if-nez v5, :cond_2

    move v4, v6

    :goto_1
    const v8, 0x7f08011e

    if-eqz v4, :cond_3

    iget-object v5, p0, Lcom/android/mail/ui/MailActionBarView;->mAccount:Lcom/android/mail/providers/Account;

    const/high16 v9, 0x20000

    invoke-virtual {v5, v9}, Lcom/android/mail/providers/Account;->supportsCapability(I)Z

    move-result v5

    if-eqz v5, :cond_3

    move v5, v6

    :goto_2
    invoke-static {p1, v8, v5}, Lcom/android/mail/utils/Utils;->setMenuItemVisibility(Landroid/view/Menu;IZ)V

    const v8, 0x7f08011f

    if-nez v4, :cond_4

    iget-object v5, p0, Lcom/android/mail/ui/MailActionBarView;->mAccount:Lcom/android/mail/providers/Account;

    const/high16 v9, 0x20000

    invoke-virtual {v5, v9}, Lcom/android/mail/providers/Account;->supportsCapability(I)Z

    move-result v5

    if-eqz v5, :cond_4

    move v5, v6

    :goto_3
    invoke-static {p1, v8, v5}, Lcom/android/mail/utils/Utils;->setMenuItemVisibility(Landroid/view/Menu;IZ)V

    iget-object v5, p0, Lcom/android/mail/ui/MailActionBarView;->mFolder:Lcom/android/mail/providers/Folder;

    if-eqz v5, :cond_5

    iget-object v5, p0, Lcom/android/mail/ui/MailActionBarView;->mFolder:Lcom/android/mail/providers/Folder;

    const/16 v8, 0x20

    invoke-virtual {v5, v8}, Lcom/android/mail/providers/Folder;->supportsCapability(I)Z

    move-result v5

    if-eqz v5, :cond_5

    move v2, v6

    :goto_4
    const v5, 0x7f08011a

    invoke-static {p1, v5, v2}, Lcom/android/mail/utils/Utils;->setMenuItemVisibility(Landroid/view/Menu;IZ)V

    if-nez v2, :cond_6

    iget-object v5, p0, Lcom/android/mail/ui/MailActionBarView;->mFolder:Lcom/android/mail/providers/Folder;

    if-eqz v5, :cond_6

    iget-object v5, p0, Lcom/android/mail/ui/MailActionBarView;->mFolder:Lcom/android/mail/providers/Folder;

    invoke-virtual {v5}, Lcom/android/mail/providers/Folder;->isDraft()Z

    move-result v5

    if-eqz v5, :cond_6

    iget-object v5, p0, Lcom/android/mail/ui/MailActionBarView;->mAccount:Lcom/android/mail/providers/Account;

    const/high16 v8, 0x100000

    invoke-virtual {v5, v8}, Lcom/android/mail/providers/Account;->supportsCapability(I)Z

    move-result v5

    if-eqz v5, :cond_6

    move v3, v6

    :goto_5
    const v5, 0x7f08011b

    invoke-static {p1, v5, v3}, Lcom/android/mail/utils/Utils;->setMenuItemVisibility(Landroid/view/Menu;IZ)V

    iget-object v5, p0, Lcom/android/mail/ui/MailActionBarView;->mAccount:Lcom/android/mail/providers/Account;

    invoke-virtual {v5, v10}, Lcom/android/mail/providers/Account;->supportsCapability(I)Z

    move-result v5

    if-eqz v5, :cond_7

    iget-object v5, p0, Lcom/android/mail/ui/MailActionBarView;->mFolder:Lcom/android/mail/providers/Folder;

    if-eqz v5, :cond_7

    iget-object v5, p0, Lcom/android/mail/ui/MailActionBarView;->mFolder:Lcom/android/mail/providers/Folder;

    invoke-virtual {v5, v12}, Lcom/android/mail/providers/Folder;->supportsCapability(I)Z

    move-result v5

    if-eqz v5, :cond_7

    iget-object v5, p0, Lcom/android/mail/ui/MailActionBarView;->mFolder:Lcom/android/mail/providers/Folder;

    invoke-virtual {v5}, Lcom/android/mail/providers/Folder;->isTrash()Z

    move-result v5

    if-nez v5, :cond_7

    move v0, v6

    :goto_6
    const v5, 0x7f080118

    invoke-static {p1, v5, v0}, Lcom/android/mail/utils/Utils;->setMenuItemVisibility(Landroid/view/Menu;IZ)V

    const v8, 0x7f080119

    if-nez v0, :cond_8

    iget-object v5, p0, Lcom/android/mail/ui/MailActionBarView;->mFolder:Lcom/android/mail/providers/Folder;

    if-eqz v5, :cond_8

    iget-object v5, p0, Lcom/android/mail/ui/MailActionBarView;->mFolder:Lcom/android/mail/providers/Folder;

    invoke-virtual {v5, v10}, Lcom/android/mail/providers/Folder;->supportsCapability(I)Z

    move-result v5

    if-eqz v5, :cond_8

    iget-object v5, p0, Lcom/android/mail/ui/MailActionBarView;->mFolder:Lcom/android/mail/providers/Folder;

    invoke-virtual {v5}, Lcom/android/mail/providers/Folder;->isProviderFolder()Z

    move-result v5

    if-nez v5, :cond_8

    move v5, v6

    :goto_7
    invoke-static {p1, v8, v5}, Lcom/android/mail/utils/Utils;->setMenuItemVisibility(Landroid/view/Menu;IZ)V

    const v5, 0x7f080119

    invoke-interface {p1, v5}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v1

    if-eqz v1, :cond_1

    iget-object v5, p0, Lcom/android/mail/ui/MailActionBarView;->mActivity:Lcom/android/mail/ui/ControllableActivity;

    invoke-interface {v5}, Lcom/android/mail/ui/ControllableActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v5

    const v8, 0x7f090050

    new-array v9, v6, [Ljava/lang/Object;

    iget-object v10, p0, Lcom/android/mail/ui/MailActionBarView;->mFolder:Lcom/android/mail/providers/Folder;

    iget-object v10, v10, Lcom/android/mail/providers/Folder;->name:Ljava/lang/String;

    aput-object v10, v9, v7

    invoke-virtual {v5, v8, v9}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    invoke-interface {v1, v5}, Landroid/view/MenuItem;->setTitle(Ljava/lang/CharSequence;)Landroid/view/MenuItem;

    :cond_1
    const v8, 0x7f080121

    iget-object v5, p0, Lcom/android/mail/ui/MailActionBarView;->mAccount:Lcom/android/mail/providers/Account;

    invoke-virtual {v5, v11}, Lcom/android/mail/providers/Account;->supportsCapability(I)Z

    move-result v5

    if-eqz v5, :cond_9

    iget-object v5, p0, Lcom/android/mail/ui/MailActionBarView;->mFolder:Lcom/android/mail/providers/Folder;

    if-eqz v5, :cond_9

    iget-object v5, p0, Lcom/android/mail/ui/MailActionBarView;->mFolder:Lcom/android/mail/providers/Folder;

    const/16 v9, 0x40

    invoke-virtual {v5, v9}, Lcom/android/mail/providers/Folder;->supportsCapability(I)Z

    move-result v5

    if-eqz v5, :cond_9

    iget-object v5, p0, Lcom/android/mail/ui/MailActionBarView;->mCurrentConversation:Lcom/android/mail/providers/Conversation;

    iget-boolean v5, v5, Lcom/android/mail/providers/Conversation;->spam:Z

    if-nez v5, :cond_9

    move v5, v6

    :goto_8
    invoke-static {p1, v8, v5}, Lcom/android/mail/utils/Utils;->setMenuItemVisibility(Landroid/view/Menu;IZ)V

    const v8, 0x7f080122

    iget-object v5, p0, Lcom/android/mail/ui/MailActionBarView;->mAccount:Lcom/android/mail/providers/Account;

    invoke-virtual {v5, v11}, Lcom/android/mail/providers/Account;->supportsCapability(I)Z

    move-result v5

    if-eqz v5, :cond_a

    iget-object v5, p0, Lcom/android/mail/ui/MailActionBarView;->mFolder:Lcom/android/mail/providers/Folder;

    if-eqz v5, :cond_a

    iget-object v5, p0, Lcom/android/mail/ui/MailActionBarView;->mFolder:Lcom/android/mail/providers/Folder;

    const/16 v9, 0x80

    invoke-virtual {v5, v9}, Lcom/android/mail/providers/Folder;->supportsCapability(I)Z

    move-result v5

    if-eqz v5, :cond_a

    iget-object v5, p0, Lcom/android/mail/ui/MailActionBarView;->mCurrentConversation:Lcom/android/mail/providers/Conversation;

    iget-boolean v5, v5, Lcom/android/mail/providers/Conversation;->spam:Z

    if-eqz v5, :cond_a

    move v5, v6

    :goto_9
    invoke-static {p1, v8, v5}, Lcom/android/mail/utils/Utils;->setMenuItemVisibility(Landroid/view/Menu;IZ)V

    const v8, 0x7f080123

    iget-object v5, p0, Lcom/android/mail/ui/MailActionBarView;->mAccount:Lcom/android/mail/providers/Account;

    const/4 v9, 0x4

    invoke-virtual {v5, v9}, Lcom/android/mail/providers/Account;->supportsCapability(I)Z

    move-result v5

    if-eqz v5, :cond_b

    iget-object v5, p0, Lcom/android/mail/ui/MailActionBarView;->mFolder:Lcom/android/mail/providers/Folder;

    if-eqz v5, :cond_b

    iget-object v5, p0, Lcom/android/mail/ui/MailActionBarView;->mFolder:Lcom/android/mail/providers/Folder;

    const/16 v9, 0x2000

    invoke-virtual {v5, v9}, Lcom/android/mail/providers/Folder;->supportsCapability(I)Z

    move-result v5

    if-eqz v5, :cond_b

    iget-object v5, p0, Lcom/android/mail/ui/MailActionBarView;->mCurrentConversation:Lcom/android/mail/providers/Conversation;

    iget-boolean v5, v5, Lcom/android/mail/providers/Conversation;->phishing:Z

    if-nez v5, :cond_b

    move v5, v6

    :goto_a
    invoke-static {p1, v8, v5}, Lcom/android/mail/utils/Utils;->setMenuItemVisibility(Landroid/view/Menu;IZ)V

    const v5, 0x7f080120

    iget-object v8, p0, Lcom/android/mail/ui/MailActionBarView;->mAccount:Lcom/android/mail/providers/Account;

    invoke-virtual {v8, v12}, Lcom/android/mail/providers/Account;->supportsCapability(I)Z

    move-result v8

    if-eqz v8, :cond_c

    iget-object v8, p0, Lcom/android/mail/ui/MailActionBarView;->mFolder:Lcom/android/mail/providers/Folder;

    if-eqz v8, :cond_c

    iget-object v8, p0, Lcom/android/mail/ui/MailActionBarView;->mFolder:Lcom/android/mail/providers/Folder;

    const/16 v9, 0x100

    invoke-virtual {v8, v9}, Lcom/android/mail/providers/Folder;->supportsCapability(I)Z

    move-result v8

    if-eqz v8, :cond_c

    iget-object v8, p0, Lcom/android/mail/ui/MailActionBarView;->mCurrentConversation:Lcom/android/mail/providers/Conversation;

    iget-boolean v8, v8, Lcom/android/mail/providers/Conversation;->muted:Z

    if-nez v8, :cond_c

    :goto_b
    invoke-static {p1, v5, v6}, Lcom/android/mail/utils/Utils;->setMenuItemVisibility(Landroid/view/Menu;IZ)V

    goto/16 :goto_0

    :cond_2
    move v4, v7

    goto/16 :goto_1

    :cond_3
    move v5, v7

    goto/16 :goto_2

    :cond_4
    move v5, v7

    goto/16 :goto_3

    :cond_5
    move v2, v7

    goto/16 :goto_4

    :cond_6
    move v3, v7

    goto/16 :goto_5

    :cond_7
    move v0, v7

    goto/16 :goto_6

    :cond_8
    move v5, v7

    goto/16 :goto_7

    :cond_9
    move v5, v7

    goto/16 :goto_8

    :cond_a
    move v5, v7

    goto :goto_9

    :cond_b
    move v5, v7

    goto :goto_a

    :cond_c
    move v6, v7

    goto :goto_b
.end method

.method public setCurrentConversation(Lcom/android/mail/providers/Conversation;)V
    .locals 0
    .param p1    # Lcom/android/mail/providers/Conversation;

    iput-object p1, p0, Lcom/android/mail/ui/MailActionBarView;->mCurrentConversation:Lcom/android/mail/providers/Conversation;

    return-void
.end method

.method protected setEmptyMode()V
    .locals 2

    const/16 v1, 0x8

    const/16 v0, 0x10

    invoke-direct {p0, v0}, Lcom/android/mail/ui/MailActionBarView;->setTitleModeFlags(I)V

    iget-object v0, p0, Lcom/android/mail/ui/MailActionBarView;->mSpinner:Lcom/android/mail/ui/MailSpinner;

    invoke-virtual {v0, v1}, Lcom/android/mail/ui/MailSpinner;->setVisibility(I)V

    iget-object v0, p0, Lcom/android/mail/ui/MailActionBarView;->mSubjectView:Lcom/android/mail/browse/SnippetTextView;

    invoke-virtual {v0, v1}, Lcom/android/mail/browse/SnippetTextView;->setVisibility(I)V

    return-void
.end method

.method public setFolder(Lcom/android/mail/providers/Folder;)V
    .locals 1
    .param p1    # Lcom/android/mail/providers/Folder;

    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/android/mail/ui/MailActionBarView;->setRefreshInProgress(Z)Z

    iput-object p1, p0, Lcom/android/mail/ui/MailActionBarView;->mFolder:Lcom/android/mail/providers/Folder;

    iget-object v0, p0, Lcom/android/mail/ui/MailActionBarView;->mSpinner:Lcom/android/mail/ui/MailSpinner;

    invoke-virtual {v0, p1}, Lcom/android/mail/ui/MailSpinner;->setFolder(Lcom/android/mail/providers/Folder;)V

    iget-object v0, p0, Lcom/android/mail/ui/MailActionBarView;->mActivity:Lcom/android/mail/ui/ControllableActivity;

    invoke-interface {v0}, Lcom/android/mail/ui/ControllableActivity;->invalidateOptionsMenu()V

    return-void
.end method

.method protected setSnippetMode()V
    .locals 2

    const/16 v0, 0x10

    invoke-direct {p0, v0}, Lcom/android/mail/ui/MailActionBarView;->setTitleModeFlags(I)V

    iget-object v0, p0, Lcom/android/mail/ui/MailActionBarView;->mSpinner:Lcom/android/mail/ui/MailSpinner;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Lcom/android/mail/ui/MailSpinner;->setVisibility(I)V

    iget-object v0, p0, Lcom/android/mail/ui/MailActionBarView;->mSubjectView:Lcom/android/mail/browse/SnippetTextView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/android/mail/browse/SnippetTextView;->setVisibility(I)V

    iget-object v0, p0, Lcom/android/mail/ui/MailActionBarView;->mSubjectView:Lcom/android/mail/browse/SnippetTextView;

    iget-object v1, p0, Lcom/android/mail/ui/MailActionBarView;->mSnippetLayoutListener:Landroid/view/View$OnLayoutChangeListener;

    invoke-virtual {v0, v1}, Lcom/android/mail/browse/SnippetTextView;->addOnLayoutChangeListener(Landroid/view/View$OnLayoutChangeListener;)V

    return-void
.end method

.method public setSubject(Ljava/lang/String;)V
    .locals 6
    .param p1    # Ljava/lang/String;

    const/4 v5, 0x0

    iget-boolean v2, p0, Lcom/android/mail/ui/MailActionBarView;->mShowConversationSubject:Z

    if-nez v2, :cond_0

    :goto_0
    return-void

    :cond_0
    new-instance v0, Landroid/text/SpannableStringBuilder;

    invoke-direct {v0}, Landroid/text/SpannableStringBuilder;-><init>()V

    new-instance v1, Landroid/text/SpannableString;

    invoke-direct {v1, p1}, Landroid/text/SpannableString;-><init>(Ljava/lang/CharSequence;)V

    new-instance v2, Landroid/text/style/TextAppearanceSpan;

    invoke-virtual {p0}, Lcom/android/mail/ui/MailActionBarView;->getContext()Landroid/content/Context;

    move-result-object v3

    const v4, 0x7f0d003c

    invoke-direct {v2, v3, v4}, Landroid/text/style/TextAppearanceSpan;-><init>(Landroid/content/Context;I)V

    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v3

    invoke-virtual {v1, v2, v5, v3, v5}, Landroid/text/SpannableString;->setSpan(Ljava/lang/Object;III)V

    invoke-virtual {v0, v1}, Landroid/text/SpannableStringBuilder;->append(Ljava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    iget-object v2, p0, Lcom/android/mail/ui/MailActionBarView;->mActionBar:Landroid/app/ActionBar;

    invoke-virtual {v2, v0}, Landroid/app/ActionBar;->setTitle(Ljava/lang/CharSequence;)V

    iget-object v2, p0, Lcom/android/mail/ui/MailActionBarView;->mActionBar:Landroid/app/ActionBar;

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Landroid/app/ActionBar;->setSubtitle(Ljava/lang/CharSequence;)V

    iget-object v2, p0, Lcom/android/mail/ui/MailActionBarView;->mSubjectView:Lcom/android/mail/browse/SnippetTextView;

    invoke-virtual {v2, p1}, Lcom/android/mail/browse/SnippetTextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0
.end method

.method protected showConversationSubject()Z
    .locals 1

    iget v0, p0, Lcom/android/mail/ui/MailActionBarView;->mMode:I

    invoke-static {v0}, Lcom/android/mail/ui/ViewMode;->isConversationMode(I)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/android/mail/ui/MailActionBarView;->mShowConversationSubject:Z

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
