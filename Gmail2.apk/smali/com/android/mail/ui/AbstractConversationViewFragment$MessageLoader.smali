.class Lcom/android/mail/ui/AbstractConversationViewFragment$MessageLoader;
.super Landroid/content/CursorLoader;
.source "AbstractConversationViewFragment.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/mail/ui/AbstractConversationViewFragment;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "MessageLoader"
.end annotation


# instance fields
.field private mDeliveredFirstResults:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/net/Uri;)V
    .locals 7
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/net/Uri;

    const/4 v4, 0x0

    sget-object v3, Lcom/android/mail/providers/UIProvider;->MESSAGE_PROJECTION:[Ljava/lang/String;

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v5, v4

    move-object v6, v4

    invoke-direct/range {v0 .. v6}, Landroid/content/CursorLoader;-><init>(Landroid/content/Context;Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)V

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/android/mail/ui/AbstractConversationViewFragment$MessageLoader;->mDeliveredFirstResults:Z

    return-void
.end method


# virtual methods
.method public deliverResult(Landroid/database/Cursor;)V
    .locals 5
    .param p1    # Landroid/database/Cursor;

    invoke-super {p0, p1}, Landroid/content/CursorLoader;->deliverResult(Landroid/database/Cursor;)V

    iget-boolean v2, p0, Lcom/android/mail/ui/AbstractConversationViewFragment$MessageLoader;->mDeliveredFirstResults:Z

    if-nez v2, :cond_0

    const/4 v2, 0x1

    iput-boolean v2, p0, Lcom/android/mail/ui/AbstractConversationViewFragment$MessageLoader;->mDeliveredFirstResults:Z

    invoke-virtual {p0}, Lcom/android/mail/ui/AbstractConversationViewFragment$MessageLoader;->getUri()Landroid/net/Uri;

    move-result-object v1

    new-instance v0, Lcom/android/mail/providers/ListParams;

    const/4 v2, -0x1

    const/4 v3, 0x0

    invoke-direct {v0, v2, v3}, Lcom/android/mail/providers/ListParams;-><init>(IZ)V

    invoke-virtual {v1}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v2

    const-string v3, "listParams"

    invoke-virtual {v0}, Lcom/android/mail/providers/ListParams;->serialize()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v2

    invoke-virtual {v2}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v1

    invoke-virtual {p0, v1}, Lcom/android/mail/ui/AbstractConversationViewFragment$MessageLoader;->setUri(Landroid/net/Uri;)V

    :cond_0
    return-void
.end method

.method public bridge synthetic deliverResult(Ljava/lang/Object;)V
    .locals 0
    .param p1    # Ljava/lang/Object;

    check-cast p1, Landroid/database/Cursor;

    invoke-virtual {p0, p1}, Lcom/android/mail/ui/AbstractConversationViewFragment$MessageLoader;->deliverResult(Landroid/database/Cursor;)V

    return-void
.end method

.method public loadInBackground()Landroid/database/Cursor;
    .locals 2

    new-instance v0, Lcom/android/mail/browse/MessageCursor;

    invoke-super {p0}, Landroid/content/CursorLoader;->loadInBackground()Landroid/database/Cursor;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/android/mail/browse/MessageCursor;-><init>(Landroid/database/Cursor;)V

    return-object v0
.end method

.method public bridge synthetic loadInBackground()Ljava/lang/Object;
    .locals 1

    invoke-virtual {p0}, Lcom/android/mail/ui/AbstractConversationViewFragment$MessageLoader;->loadInBackground()Landroid/database/Cursor;

    move-result-object v0

    return-object v0
.end method
