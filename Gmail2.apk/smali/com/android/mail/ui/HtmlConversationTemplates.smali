.class public Lcom/android/mail/ui/HtmlConversationTemplates;
.super Ljava/lang/Object;
.source "HtmlConversationTemplates.java"


# static fields
.field public static final MESSAGE_PREFIX_LENGTH:I

.field private static final TAG:Ljava/lang/String;

.field private static final sAbsoluteImgUrlPattern:Ljava/util/regex/Pattern;

.field private static sConversationLower:Ljava/lang/String;

.field private static sConversationUpper:Ljava/lang/String;

.field private static sLoadedTemplates:Z

.field private static sMessage:Ljava/lang/String;

.field private static sSuperCollapsed:Ljava/lang/String;


# instance fields
.field private mBuilder:Ljava/lang/StringBuilder;

.field private mContext:Landroid/content/Context;

.field private mFormatter:Ljava/util/Formatter;

.field private mInProgress:Z


# direct methods
.method static constructor <clinit>()V
    .locals 2

    const-string v0, "m"

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v0

    sput v0, Lcom/android/mail/ui/HtmlConversationTemplates;->MESSAGE_PREFIX_LENGTH:I

    invoke-static {}, Lcom/android/mail/utils/LogTag;->getLogTag()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/android/mail/ui/HtmlConversationTemplates;->TAG:Ljava/lang/String;

    const-string v0, "(<\\s*img\\s+(?:[^>]*\\s+)?)src(\\s*=[\\s\'\"]*http)"

    const/16 v1, 0xa

    invoke-static {v0, v1}, Ljava/util/regex/Pattern;->compile(Ljava/lang/String;I)Ljava/util/regex/Pattern;

    move-result-object v0

    sput-object v0, Lcom/android/mail/ui/HtmlConversationTemplates;->sAbsoluteImgUrlPattern:Ljava/util/regex/Pattern;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1    # Landroid/content/Context;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/android/mail/ui/HtmlConversationTemplates;->mInProgress:Z

    iput-object p1, p0, Lcom/android/mail/ui/HtmlConversationTemplates;->mContext:Landroid/content/Context;

    sget-boolean v0, Lcom/android/mail/ui/HtmlConversationTemplates;->sLoadedTemplates:Z

    if-nez v0, :cond_0

    const/4 v0, 0x1

    sput-boolean v0, Lcom/android/mail/ui/HtmlConversationTemplates;->sLoadedTemplates:Z

    const v0, 0x7f070004

    invoke-direct {p0, v0}, Lcom/android/mail/ui/HtmlConversationTemplates;->readTemplate(I)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/android/mail/ui/HtmlConversationTemplates;->sSuperCollapsed:Ljava/lang/String;

    const v0, 0x7f070003

    invoke-direct {p0, v0}, Lcom/android/mail/ui/HtmlConversationTemplates;->readTemplate(I)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/android/mail/ui/HtmlConversationTemplates;->sMessage:Ljava/lang/String;

    const v0, 0x7f070002

    invoke-direct {p0, v0}, Lcom/android/mail/ui/HtmlConversationTemplates;->readTemplate(I)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/android/mail/ui/HtmlConversationTemplates;->sConversationUpper:Ljava/lang/String;

    const v0, 0x7f070001

    invoke-direct {p0, v0}, Lcom/android/mail/ui/HtmlConversationTemplates;->readTemplate(I)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/android/mail/ui/HtmlConversationTemplates;->sConversationLower:Ljava/lang/String;

    :cond_0
    return-void
.end method

.method private varargs append(Ljava/lang/String;[Ljava/lang/Object;)V
    .locals 1
    .param p1    # Ljava/lang/String;
    .param p2    # [Ljava/lang/Object;

    iget-object v0, p0, Lcom/android/mail/ui/HtmlConversationTemplates;->mFormatter:Ljava/util/Formatter;

    invoke-virtual {v0, p1, p2}, Ljava/util/Formatter;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/util/Formatter;

    return-void
.end method

.method private readTemplate(I)Ljava/lang/String;
    .locals 9
    .param p1    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/content/res/Resources$NotFoundException;
        }
    .end annotation

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const/4 v3, 0x0

    :try_start_0
    new-instance v4, Ljava/io/InputStreamReader;

    iget-object v6, p0, Lcom/android/mail/ui/HtmlConversationTemplates;->mContext:Landroid/content/Context;

    invoke-virtual {v6}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v6

    invoke-virtual {v6, p1}, Landroid/content/res/Resources;->openRawResource(I)Ljava/io/InputStream;

    move-result-object v6

    const-string v7, "UTF-8"

    invoke-direct {v4, v6, v7}, Ljava/io/InputStreamReader;-><init>(Ljava/io/InputStream;Ljava/lang/String;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    const/16 v6, 0x1000

    :try_start_1
    new-array v0, v6, [C

    :goto_0
    invoke-virtual {v4, v0}, Ljava/io/InputStreamReader;->read([C)I

    move-result v1

    if-lez v1, :cond_1

    const/4 v6, 0x0

    invoke-virtual {v5, v0, v6, v1}, Ljava/lang/StringBuilder;->append([CII)Ljava/lang/StringBuilder;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v6

    move-object v3, v4

    :goto_1
    if-eqz v3, :cond_0

    :try_start_2
    invoke-virtual {v3}, Ljava/io/InputStreamReader;->close()V

    :cond_0
    throw v6
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_0

    :catch_0
    move-exception v2

    :goto_2
    new-instance v6, Landroid/content/res/Resources$NotFoundException;

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "Unable to open template id="

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-static {p1}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, " exception="

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v2}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-direct {v6, v7}, Landroid/content/res/Resources$NotFoundException;-><init>(Ljava/lang/String;)V

    throw v6

    :cond_1
    :try_start_3
    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    move-result-object v6

    if-eqz v4, :cond_2

    :try_start_4
    invoke-virtual {v4}, Ljava/io/InputStreamReader;->close()V
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_1

    :cond_2
    return-object v6

    :catch_1
    move-exception v2

    move-object v3, v4

    goto :goto_2

    :catchall_1
    move-exception v6

    goto :goto_1
.end method

.method static replaceAbsoluteImgUrls(Ljava/lang/String;)Ljava/lang/String;
    .locals 2
    .param p0    # Ljava/lang/String;

    sget-object v0, Lcom/android/mail/ui/HtmlConversationTemplates;->sAbsoluteImgUrlPattern:Ljava/util/regex/Pattern;

    invoke-virtual {v0, p0}, Ljava/util/regex/Pattern;->matcher(Ljava/lang/CharSequence;)Ljava/util/regex/Matcher;

    move-result-object v0

    const-string v1, "$1src=\'data:\' blocked-src$2"

    invoke-virtual {v0, v1}, Ljava/util/regex/Matcher;->replaceAll(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public appendMessageHtml(Lcom/android/mail/providers/Message;ZZII)V
    .locals 8
    .param p1    # Lcom/android/mail/providers/Message;
    .param p2    # Z
    .param p3    # Z
    .param p4    # I
    .param p5    # I

    if-eqz p2, :cond_1

    const-string v1, "block"

    :goto_0
    if-eqz p2, :cond_2

    const-string v2, "expanded"

    :goto_1
    if-eqz p3, :cond_3

    const-string v3, "mail-show-images"

    :goto_2
    invoke-virtual {p1}, Lcom/android/mail/providers/Message;->getBodyAsHtml()Ljava/lang/String;

    move-result-object v0

    if-nez p3, :cond_0

    iget-boolean v4, p1, Lcom/android/mail/providers/Message;->embedsExternalResources:Z

    if-eqz v4, :cond_0

    invoke-static {v0}, Lcom/android/mail/ui/HtmlConversationTemplates;->replaceAbsoluteImgUrls(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    :cond_0
    sget-object v4, Lcom/android/mail/ui/HtmlConversationTemplates;->sMessage:Ljava/lang/String;

    const/16 v5, 0x8

    new-array v5, v5, [Ljava/lang/Object;

    const/4 v6, 0x0

    invoke-virtual {p0, p1}, Lcom/android/mail/ui/HtmlConversationTemplates;->getMessageDomId(Lcom/android/mail/providers/Message;)Ljava/lang/String;

    move-result-object v7

    aput-object v7, v5, v6

    const/4 v6, 0x1

    aput-object v2, v5, v6

    const/4 v6, 0x2

    invoke-static {p4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    aput-object v7, v5, v6

    const/4 v6, 0x3

    aput-object v3, v5, v6

    const/4 v6, 0x4

    aput-object v1, v5, v6

    const/4 v6, 0x5

    aput-object v0, v5, v6

    const/4 v6, 0x6

    aput-object v1, v5, v6

    const/4 v6, 0x7

    invoke-static {p5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    aput-object v7, v5, v6

    invoke-direct {p0, v4, v5}, Lcom/android/mail/ui/HtmlConversationTemplates;->append(Ljava/lang/String;[Ljava/lang/Object;)V

    return-void

    :cond_1
    const-string v1, "none"

    goto :goto_0

    :cond_2
    const-string v2, ""

    goto :goto_1

    :cond_3
    const-string v3, ""

    goto :goto_2
.end method

.method public appendSuperCollapsedHtml(II)V
    .locals 4
    .param p1    # I
    .param p2    # I

    iget-boolean v0, p0, Lcom/android/mail/ui/HtmlConversationTemplates;->mInProgress:Z

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "must call startConversation first"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    sget-object v0, Lcom/android/mail/ui/HtmlConversationTemplates;->sSuperCollapsed:Ljava/lang/String;

    const/4 v1, 0x2

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v1, v2

    const/4 v2, 0x1

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v1, v2

    invoke-direct {p0, v0, v1}, Lcom/android/mail/ui/HtmlConversationTemplates;->append(Ljava/lang/String;[Ljava/lang/Object;)V

    return-void
.end method

.method public emit()Ljava/lang/String;
    .locals 3

    const/4 v2, 0x0

    iget-object v1, p0, Lcom/android/mail/ui/HtmlConversationTemplates;->mFormatter:Ljava/util/Formatter;

    invoke-virtual {v1}, Ljava/util/Formatter;->toString()Ljava/lang/String;

    move-result-object v0

    iput-object v2, p0, Lcom/android/mail/ui/HtmlConversationTemplates;->mFormatter:Ljava/util/Formatter;

    iput-object v2, p0, Lcom/android/mail/ui/HtmlConversationTemplates;->mBuilder:Ljava/lang/StringBuilder;

    return-object v0
.end method

.method public endConversation(Ljava/lang/String;Ljava/lang/String;IIZZ)Ljava/lang/String;
    .locals 8
    .param p1    # Ljava/lang/String;
    .param p2    # Ljava/lang/String;
    .param p3    # I
    .param p4    # I
    .param p5    # Z
    .param p6    # Z

    const/4 v7, 0x2

    const/4 v6, 0x1

    const/4 v5, 0x0

    iget-boolean v1, p0, Lcom/android/mail/ui/HtmlConversationTemplates;->mInProgress:Z

    if-nez v1, :cond_0

    new-instance v1, Ljava/lang/IllegalStateException;

    const-string v2, "must call startConversation first"

    invoke-direct {v1, v2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v1

    :cond_0
    if-eqz p5, :cond_1

    const-string v0, "initial-load"

    :goto_0
    sget-object v1, Lcom/android/mail/ui/HtmlConversationTemplates;->sConversationLower:Ljava/lang/String;

    const/16 v2, 0x9

    new-array v2, v2, [Ljava/lang/Object;

    aput-object v0, v2, v5

    iget-object v3, p0, Lcom/android/mail/ui/HtmlConversationTemplates;->mContext:Landroid/content/Context;

    const v4, 0x7f0900d8

    invoke-virtual {v3, v4}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v6

    iget-object v3, p0, Lcom/android/mail/ui/HtmlConversationTemplates;->mContext:Landroid/content/Context;

    const v4, 0x7f0900d7

    invoke-virtual {v3, v4}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v7

    const/4 v3, 0x3

    aput-object p1, v2, v3

    const/4 v3, 0x4

    aput-object p2, v2, v3

    const/4 v3, 0x5

    invoke-static {p3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v2, v3

    const/4 v3, 0x6

    invoke-static {p4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v2, v3

    const/4 v3, 0x7

    invoke-static {p5}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v4

    aput-object v4, v2, v3

    const/16 v3, 0x8

    invoke-static {p6}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-direct {p0, v1, v2}, Lcom/android/mail/ui/HtmlConversationTemplates;->append(Ljava/lang/String;[Ljava/lang/Object;)V

    iput-boolean v5, p0, Lcom/android/mail/ui/HtmlConversationTemplates;->mInProgress:Z

    sget-object v1, Lcom/android/mail/ui/HtmlConversationTemplates;->TAG:Ljava/lang/String;

    const-string v2, "rendered conversation of %d bytes, buffer capacity=%d"

    new-array v3, v7, [Ljava/lang/Object;

    iget-object v4, p0, Lcom/android/mail/ui/HtmlConversationTemplates;->mBuilder:Ljava/lang/StringBuilder;

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->length()I

    move-result v4

    shl-int/lit8 v4, v4, 0x1

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v3, v5

    iget-object v4, p0, Lcom/android/mail/ui/HtmlConversationTemplates;->mBuilder:Ljava/lang/StringBuilder;

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->capacity()I

    move-result v4

    shl-int/lit8 v4, v4, 0x1

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v3, v6

    invoke-static {v1, v2, v3}, Lcom/android/mail/utils/LogUtils;->d(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)I

    invoke-virtual {p0}, Lcom/android/mail/ui/HtmlConversationTemplates;->emit()Ljava/lang/String;

    move-result-object v1

    return-object v1

    :cond_1
    const-string v0, ""

    goto :goto_0
.end method

.method public getMessageDomId(Lcom/android/mail/providers/Message;)Ljava/lang/String;
    .locals 3
    .param p1    # Lcom/android/mail/providers/Message;

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "m"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-wide v1, p1, Lcom/android/mail/providers/Message;->id:J

    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public reset()V
    .locals 3

    new-instance v0, Ljava/lang/StringBuilder;

    const/high16 v1, 0x10000

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(I)V

    iput-object v0, p0, Lcom/android/mail/ui/HtmlConversationTemplates;->mBuilder:Ljava/lang/StringBuilder;

    new-instance v0, Ljava/util/Formatter;

    iget-object v1, p0, Lcom/android/mail/ui/HtmlConversationTemplates;->mBuilder:Ljava/lang/StringBuilder;

    const/4 v2, 0x0

    invoke-direct {v0, v1, v2}, Ljava/util/Formatter;-><init>(Ljava/lang/Appendable;Ljava/util/Locale;)V

    iput-object v0, p0, Lcom/android/mail/ui/HtmlConversationTemplates;->mFormatter:Ljava/util/Formatter;

    return-void
.end method

.method public startConversation(II)V
    .locals 5
    .param p1    # I
    .param p2    # I

    const/4 v4, 0x1

    iget-boolean v0, p0, Lcom/android/mail/ui/HtmlConversationTemplates;->mInProgress:Z

    if-eqz v0, :cond_0

    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "must call startConversation first"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    invoke-virtual {p0}, Lcom/android/mail/ui/HtmlConversationTemplates;->reset()V

    sget-object v0, Lcom/android/mail/ui/HtmlConversationTemplates;->sConversationUpper:Ljava/lang/String;

    const/4 v1, 0x2

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v1, v2

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v1, v4

    invoke-direct {p0, v0, v1}, Lcom/android/mail/ui/HtmlConversationTemplates;->append(Ljava/lang/String;[Ljava/lang/Object;)V

    iput-boolean v4, p0, Lcom/android/mail/ui/HtmlConversationTemplates;->mInProgress:Z

    return-void
.end method
