.class public Lcom/android/mail/ui/SeparatedFolderListAdapter;
.super Landroid/widget/BaseAdapter;
.source "SeparatedFolderListAdapter.java"


# instance fields
.field public final sections:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/android/mail/ui/FolderSelectorAdapter;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1    # Landroid/content/Context;

    invoke-direct {p0}, Landroid/widget/BaseAdapter;-><init>()V

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/android/mail/ui/SeparatedFolderListAdapter;->sections:Ljava/util/ArrayList;

    return-void
.end method


# virtual methods
.method public addSection(Lcom/android/mail/ui/FolderSelectorAdapter;)V
    .locals 1
    .param p1    # Lcom/android/mail/ui/FolderSelectorAdapter;

    iget-object v0, p0, Lcom/android/mail/ui/SeparatedFolderListAdapter;->sections:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    return-void
.end method

.method public areAllItemsEnabled()Z
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public getCount()I
    .locals 4

    const/4 v2, 0x0

    iget-object v3, p0, Lcom/android/mail/ui/SeparatedFolderListAdapter;->sections:Ljava/util/ArrayList;

    invoke-virtual {v3}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/mail/ui/FolderSelectorAdapter;

    invoke-virtual {v0}, Lcom/android/mail/ui/FolderSelectorAdapter;->getCount()I

    move-result v3

    add-int/2addr v2, v3

    goto :goto_0

    :cond_0
    return v2
.end method

.method public getItem(I)Ljava/lang/Object;
    .locals 4
    .param p1    # I

    iget-object v3, p0, Lcom/android/mail/ui/SeparatedFolderListAdapter;->sections:Ljava/util/ArrayList;

    invoke-virtual {v3}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_2

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/mail/ui/FolderSelectorAdapter;

    invoke-virtual {v0}, Lcom/android/mail/ui/FolderSelectorAdapter;->getCount()I

    move-result v2

    if-eqz p1, :cond_0

    if-ge p1, v2, :cond_1

    :cond_0
    invoke-virtual {v0, p1}, Lcom/android/mail/ui/FolderSelectorAdapter;->getItem(I)Ljava/lang/Object;

    move-result-object v3

    :goto_1
    return-object v3

    :cond_1
    sub-int/2addr p1, v2

    goto :goto_0

    :cond_2
    const/4 v3, 0x0

    goto :goto_1
.end method

.method public getItemId(I)J
    .locals 2
    .param p1    # I

    int-to-long v0, p1

    return-wide v0
.end method

.method public getItemViewType(I)I
    .locals 5
    .param p1    # I

    const/4 v3, 0x0

    iget-object v4, p0, Lcom/android/mail/ui/SeparatedFolderListAdapter;->sections:Ljava/util/ArrayList;

    invoke-virtual {v4}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_2

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/mail/ui/FolderSelectorAdapter;

    invoke-virtual {v0}, Lcom/android/mail/ui/FolderSelectorAdapter;->getCount()I

    move-result v2

    if-eqz p1, :cond_0

    if-ge p1, v2, :cond_1

    :cond_0
    invoke-virtual {v0, p1}, Lcom/android/mail/ui/FolderSelectorAdapter;->getItemViewType(I)I

    move-result v4

    add-int/2addr v4, v3

    :goto_1
    return v4

    :cond_1
    sub-int/2addr p1, v2

    invoke-virtual {v0}, Lcom/android/mail/ui/FolderSelectorAdapter;->getViewTypeCount()I

    move-result v4

    add-int/2addr v3, v4

    goto :goto_0

    :cond_2
    const/4 v4, -0x1

    goto :goto_1
.end method

.method public getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 4
    .param p1    # I
    .param p2    # Landroid/view/View;
    .param p3    # Landroid/view/ViewGroup;

    iget-object v3, p0, Lcom/android/mail/ui/SeparatedFolderListAdapter;->sections:Ljava/util/ArrayList;

    invoke-virtual {v3}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_2

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/mail/ui/FolderSelectorAdapter;

    invoke-virtual {v0}, Lcom/android/mail/ui/FolderSelectorAdapter;->getCount()I

    move-result v2

    if-eqz p1, :cond_0

    if-ge p1, v2, :cond_1

    :cond_0
    invoke-virtual {v0, p1, p2, p3}, Lcom/android/mail/ui/FolderSelectorAdapter;->getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v3

    :goto_1
    return-object v3

    :cond_1
    sub-int/2addr p1, v2

    goto :goto_0

    :cond_2
    const/4 v3, 0x0

    goto :goto_1
.end method

.method public getViewTypeCount()I
    .locals 4

    const/4 v2, 0x0

    iget-object v3, p0, Lcom/android/mail/ui/SeparatedFolderListAdapter;->sections:Ljava/util/ArrayList;

    invoke-virtual {v3}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/mail/ui/FolderSelectorAdapter;

    invoke-interface {v0}, Landroid/widget/Adapter;->getViewTypeCount()I

    move-result v3

    add-int/2addr v2, v3

    goto :goto_0

    :cond_0
    return v2
.end method

.method public isEnabled(I)Z
    .locals 1
    .param p1    # I

    invoke-virtual {p0, p1}, Lcom/android/mail/ui/SeparatedFolderListAdapter;->getItemViewType(I)I

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
