.class public Lcom/android/mail/ui/ToastBarOperation;
.super Ljava/lang/Object;
.source "ToastBarOperation.java"

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/android/mail/ui/ToastBarOperation;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private final mAction:I

.field private final mBatch:Z

.field private final mCount:I

.field private final mType:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Lcom/android/mail/ui/ToastBarOperation$1;

    invoke-direct {v0}, Lcom/android/mail/ui/ToastBarOperation$1;-><init>()V

    sput-object v0, Lcom/android/mail/ui/ToastBarOperation;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(IIIZ)V
    .locals 0
    .param p1    # I
    .param p2    # I
    .param p3    # I
    .param p4    # Z

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput p1, p0, Lcom/android/mail/ui/ToastBarOperation;->mCount:I

    iput p2, p0, Lcom/android/mail/ui/ToastBarOperation;->mAction:I

    iput-boolean p4, p0, Lcom/android/mail/ui/ToastBarOperation;->mBatch:Z

    iput p3, p0, Lcom/android/mail/ui/ToastBarOperation;->mType:I

    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 1
    .param p1    # Landroid/os/Parcel;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/android/mail/ui/ToastBarOperation;->mCount:I

    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/android/mail/ui/ToastBarOperation;->mAction:I

    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    iput-boolean v0, p0, Lcom/android/mail/ui/ToastBarOperation;->mBatch:Z

    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/android/mail/ui/ToastBarOperation;->mType:I

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public describeContents()I
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public getDescription(Landroid/content/Context;Lcom/android/mail/providers/Folder;)Ljava/lang/String;
    .locals 6
    .param p1    # Landroid/content/Context;
    .param p2    # Lcom/android/mail/providers/Folder;

    const/4 v4, 0x1

    const/4 v5, 0x0

    const/4 v1, -0x1

    iget v2, p0, Lcom/android/mail/ui/ToastBarOperation;->mAction:I

    packed-switch v2, :pswitch_data_0

    :pswitch_0
    const/4 v1, -0x1

    :goto_0
    const/4 v2, -0x1

    if-ne v1, v2, :cond_0

    const-string v0, ""

    :goto_1
    return-object v0

    :pswitch_1
    const v1, 0x7f10000e

    goto :goto_0

    :pswitch_2
    const v2, 0x7f0900b1

    new-array v3, v4, [Ljava/lang/Object;

    iget-object v4, p2, Lcom/android/mail/providers/Folder;->name:Ljava/lang/String;

    aput-object v4, v3, v5

    invoke-virtual {p1, v2, v3}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto :goto_1

    :pswitch_3
    const v1, 0x7f10000f

    goto :goto_0

    :pswitch_4
    const v1, 0x7f10000d

    goto :goto_0

    :pswitch_5
    const v1, 0x7f100009

    goto :goto_0

    :pswitch_6
    const v1, 0x7f10000a

    goto :goto_0

    :pswitch_7
    const v1, 0x7f10000b

    goto :goto_0

    :pswitch_8
    const v1, 0x7f100008

    goto :goto_0

    :pswitch_9
    const v1, 0x7f100007

    goto :goto_0

    :pswitch_a
    const v1, 0x7f10000c

    goto :goto_0

    :cond_0
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    iget v3, p0, Lcom/android/mail/ui/ToastBarOperation;->mCount:I

    invoke-virtual {v2, v1, v3}, Landroid/content/res/Resources;->getQuantityString(II)Ljava/lang/String;

    move-result-object v2

    new-array v3, v4, [Ljava/lang/Object;

    iget v4, p0, Lcom/android/mail/ui/ToastBarOperation;->mCount:I

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v3, v5

    invoke-static {v2, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto :goto_1

    :pswitch_data_0
    .packed-switch 0x7f080118
        :pswitch_4
        :pswitch_2
        :pswitch_1
        :pswitch_0
        :pswitch_3
        :pswitch_0
        :pswitch_0
        :pswitch_7
        :pswitch_8
        :pswitch_5
        :pswitch_6
        :pswitch_a
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_9
    .end packed-switch
.end method

.method public getSingularDescription(Landroid/content/Context;Lcom/android/mail/providers/Folder;)Ljava/lang/String;
    .locals 5
    .param p1    # Landroid/content/Context;
    .param p2    # Lcom/android/mail/providers/Folder;

    iget v1, p0, Lcom/android/mail/ui/ToastBarOperation;->mAction:I

    const v2, 0x7f080119

    if-ne v1, v2, :cond_0

    const v1, 0x7f0900b1

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    iget-object v4, p2, Lcom/android/mail/providers/Folder;->name:Ljava/lang/String;

    aput-object v4, v2, v3

    invoke-virtual {p1, v1, v2}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    :goto_0
    return-object v1

    :cond_0
    const/4 v0, -0x1

    iget v1, p0, Lcom/android/mail/ui/ToastBarOperation;->mAction:I

    packed-switch v1, :pswitch_data_0

    :goto_1
    :pswitch_0
    const/4 v1, -0x1

    if-ne v0, v1, :cond_1

    const-string v1, ""

    goto :goto_0

    :pswitch_1
    const v0, 0x7f0900af

    goto :goto_1

    :pswitch_2
    const v0, 0x7f0900b0

    goto :goto_1

    :cond_1
    invoke-virtual {p1, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x7f080118
        :pswitch_2
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public getType()I
    .locals 1

    iget v0, p0, Lcom/android/mail/ui/ToastBarOperation;->mType:I

    return v0
.end method

.method public isBatchUndo()Z
    .locals 1

    iget-boolean v0, p0, Lcom/android/mail/ui/ToastBarOperation;->mBatch:Z

    return v0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 1
    .param p1    # Landroid/os/Parcel;
    .param p2    # I

    iget v0, p0, Lcom/android/mail/ui/ToastBarOperation;->mCount:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    iget v0, p0, Lcom/android/mail/ui/ToastBarOperation;->mAction:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    iget-boolean v0, p0, Lcom/android/mail/ui/ToastBarOperation;->mBatch:Z

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    iget v0, p0, Lcom/android/mail/ui/ToastBarOperation;->mType:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
