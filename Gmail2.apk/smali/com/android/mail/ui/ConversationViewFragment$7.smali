.class Lcom/android/mail/ui/ConversationViewFragment$7;
.super Ljava/lang/Object;
.source "ConversationViewFragment.java"

# interfaces
.implements Lcom/android/mail/browse/MailWebView$ContentSizeChangeListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/android/mail/ui/ConversationViewFragment;->onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/mail/ui/ConversationViewFragment;


# direct methods
.method constructor <init>(Lcom/android/mail/ui/ConversationViewFragment;)V
    .locals 0

    iput-object p1, p0, Lcom/android/mail/ui/ConversationViewFragment$7;->this$0:Lcom/android/mail/ui/ConversationViewFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onHeightChange(I)V
    .locals 3
    .param p1    # I

    if-eqz p1, :cond_0

    iget-object v0, p0, Lcom/android/mail/ui/ConversationViewFragment$7;->this$0:Lcom/android/mail/ui/ConversationViewFragment;

    # getter for: Lcom/android/mail/ui/ConversationViewFragment;->mMessageViewIsLoading:Z
    invoke-static {v0}, Lcom/android/mail/ui/ConversationViewFragment;->access$900(Lcom/android/mail/ui/ConversationViewFragment;)Z

    move-result v0

    if-nez v0, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v0, p0, Lcom/android/mail/ui/ConversationViewFragment$7;->this$0:Lcom/android/mail/ui/ConversationViewFragment;

    const/4 v1, 0x0

    # setter for: Lcom/android/mail/ui/ConversationViewFragment;->mMessageViewIsLoading:Z
    invoke-static {v0, v1}, Lcom/android/mail/ui/ConversationViewFragment;->access$902(Lcom/android/mail/ui/ConversationViewFragment;Z)Z

    iget-object v0, p0, Lcom/android/mail/ui/ConversationViewFragment$7;->this$0:Lcom/android/mail/ui/ConversationViewFragment;

    invoke-virtual {v0}, Lcom/android/mail/ui/ConversationViewFragment;->getHandler()Landroid/os/Handler;

    move-result-object v0

    new-instance v1, Lcom/android/mail/ui/ConversationViewFragment$7$1;

    const-string v2, "MessageView.onHeightChange"

    invoke-direct {v1, p0, v2, p1}, Lcom/android/mail/ui/ConversationViewFragment$7$1;-><init>(Lcom/android/mail/ui/ConversationViewFragment$7;Ljava/lang/String;I)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    goto :goto_0
.end method
