.class public Lcom/android/mail/ui/DividedImageCanvas;
.super Ljava/lang/Object;
.source "DividedImageCanvas.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/mail/ui/DividedImageCanvas$Dimensions;,
        Lcom/android/mail/ui/DividedImageCanvas$InvalidateCallback;
    }
.end annotation


# static fields
.field private static final sDest:Landroid/graphics/Rect;

.field private static sDividerColor:I

.field private static sDividerLineWidth:I

.field private static final sPaint:Landroid/graphics/Paint;

.field private static final sSrc:Landroid/graphics/Rect;


# instance fields
.field private final mCallback:Lcom/android/mail/ui/DividedImageCanvas$InvalidateCallback;

.field private mCanvas:Landroid/graphics/Canvas;

.field private final mContext:Landroid/content/Context;

.field private mDividedBitmap:Landroid/graphics/Bitmap;

.field private mDivisionIds:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private final mDivisionImages:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Landroid/graphics/Bitmap;",
            ">;"
        }
    .end annotation
.end field

.field private mHeight:I

.field private mWidth:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0}, Landroid/graphics/Paint;-><init>()V

    sput-object v0, Lcom/android/mail/ui/DividedImageCanvas;->sPaint:Landroid/graphics/Paint;

    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    sput-object v0, Lcom/android/mail/ui/DividedImageCanvas;->sSrc:Landroid/graphics/Rect;

    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    sput-object v0, Lcom/android/mail/ui/DividedImageCanvas;->sDest:Landroid/graphics/Rect;

    const/4 v0, -0x1

    sput v0, Lcom/android/mail/ui/DividedImageCanvas;->sDividerLineWidth:I

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Lcom/android/mail/ui/DividedImageCanvas$InvalidateCallback;)V
    .locals 2
    .param p1    # Landroid/content/Context;
    .param p2    # Lcom/android/mail/ui/DividedImageCanvas$InvalidateCallback;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Ljava/util/ArrayList;

    const/4 v1, 0x4

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(I)V

    iput-object v0, p0, Lcom/android/mail/ui/DividedImageCanvas;->mDivisionImages:Ljava/util/ArrayList;

    iput-object p1, p0, Lcom/android/mail/ui/DividedImageCanvas;->mContext:Landroid/content/Context;

    iput-object p2, p0, Lcom/android/mail/ui/DividedImageCanvas;->mCallback:Lcom/android/mail/ui/DividedImageCanvas$InvalidateCallback;

    invoke-direct {p0}, Lcom/android/mail/ui/DividedImageCanvas;->setupDividerLines()V

    return-void
.end method

.method private draw(Landroid/graphics/Bitmap;Landroid/graphics/Canvas;IIII)V
    .locals 4
    .param p1    # Landroid/graphics/Bitmap;
    .param p2    # Landroid/graphics/Canvas;
    .param p3    # I
    .param p4    # I
    .param p5    # I
    .param p6    # I

    const/4 v3, 0x0

    if-eqz p1, :cond_0

    sget-object v0, Lcom/android/mail/ui/DividedImageCanvas;->sSrc:Landroid/graphics/Rect;

    invoke-virtual {p1}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v1

    invoke-virtual {p1}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v2

    invoke-virtual {v0, v3, v3, v1, v2}, Landroid/graphics/Rect;->set(IIII)V

    sget-object v0, Lcom/android/mail/ui/DividedImageCanvas;->sDest:Landroid/graphics/Rect;

    invoke-virtual {v0, p3, p4, p5, p6}, Landroid/graphics/Rect;->set(IIII)V

    sget-object v0, Lcom/android/mail/ui/DividedImageCanvas;->sSrc:Landroid/graphics/Rect;

    sget-object v1, Lcom/android/mail/ui/DividedImageCanvas;->sDest:Landroid/graphics/Rect;

    sget-object v2, Lcom/android/mail/ui/DividedImageCanvas;->sPaint:Landroid/graphics/Paint;

    invoke-virtual {p2, p1, v0, v1, v2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;Landroid/graphics/Rect;Landroid/graphics/Rect;Landroid/graphics/Paint;)V

    :cond_0
    return-void
.end method

.method private drawHorizontalDivider(IIII)V
    .locals 6
    .param p1    # I
    .param p2    # I
    .param p3    # I
    .param p4    # I

    invoke-direct {p0}, Lcom/android/mail/ui/DividedImageCanvas;->setupPaint()V

    iget-object v0, p0, Lcom/android/mail/ui/DividedImageCanvas;->mCanvas:Landroid/graphics/Canvas;

    int-to-float v1, p1

    int-to-float v2, p2

    int-to-float v3, p3

    int-to-float v4, p4

    sget-object v5, Lcom/android/mail/ui/DividedImageCanvas;->sPaint:Landroid/graphics/Paint;

    invoke-virtual/range {v0 .. v5}, Landroid/graphics/Canvas;->drawLine(FFFFLandroid/graphics/Paint;)V

    return-void
.end method

.method private drawVerticalDivider(II)V
    .locals 10
    .param p1    # I
    .param p2    # I

    div-int/lit8 v6, p1, 0x2

    const/4 v8, 0x0

    div-int/lit8 v7, p1, 0x2

    move v9, p2

    invoke-direct {p0}, Lcom/android/mail/ui/DividedImageCanvas;->setupPaint()V

    iget-object v0, p0, Lcom/android/mail/ui/DividedImageCanvas;->mCanvas:Landroid/graphics/Canvas;

    int-to-float v1, v6

    int-to-float v2, v8

    int-to-float v3, v7

    int-to-float v4, v9

    sget-object v5, Lcom/android/mail/ui/DividedImageCanvas;->sPaint:Landroid/graphics/Paint;

    invoke-virtual/range {v0 .. v5}, Landroid/graphics/Canvas;->drawLine(FFFFLandroid/graphics/Paint;)V

    return-void
.end method

.method public static generateHash(Lcom/android/mail/ui/DividedImageCanvas;ILjava/lang/String;)J
    .locals 3
    .param p0    # Lcom/android/mail/ui/DividedImageCanvas;
    .param p1    # I
    .param p2    # Ljava/lang/String;

    const/4 v0, 0x3

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    aput-object p0, v0, v1

    const/4 v1, 0x1

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x2

    aput-object p2, v0, v1

    invoke-static {v0}, Lcom/google/common/base/Objects;->hashCode([Ljava/lang/Object;)I

    move-result v0

    int-to-long v0, v0

    return-wide v0
.end method

.method private setupDividerLines()V
    .locals 3

    sget v1, Lcom/android/mail/ui/DividedImageCanvas;->sDividerLineWidth:I

    const/4 v2, -0x1

    if-ne v1, v2, :cond_0

    invoke-virtual {p0}, Lcom/android/mail/ui/DividedImageCanvas;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0c006e

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    sput v1, Lcom/android/mail/ui/DividedImageCanvas;->sDividerLineWidth:I

    const v1, 0x7f0a002f

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    sput v1, Lcom/android/mail/ui/DividedImageCanvas;->sDividerColor:I

    :cond_0
    return-void
.end method

.method private setupPaint()V
    .locals 2

    sget-object v0, Lcom/android/mail/ui/DividedImageCanvas;->sPaint:Landroid/graphics/Paint;

    sget v1, Lcom/android/mail/ui/DividedImageCanvas;->sDividerLineWidth:I

    int-to-float v1, v1

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    sget-object v0, Lcom/android/mail/ui/DividedImageCanvas;->sPaint:Landroid/graphics/Paint;

    sget v1, Lcom/android/mail/ui/DividedImageCanvas;->sDividerColor:I

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setColor(I)V

    return-void
.end method


# virtual methods
.method public addDivisionImage([BLjava/lang/String;)Landroid/graphics/Bitmap;
    .locals 7
    .param p1    # [B
    .param p2    # Ljava/lang/String;

    const/4 v0, 0x0

    iget-object v5, p0, Lcom/android/mail/ui/DividedImageCanvas;->mDivisionIds:Ljava/util/ArrayList;

    invoke-virtual {v5, p2}, Ljava/util/ArrayList;->indexOf(Ljava/lang/Object;)I

    move-result v2

    if-ltz v2, :cond_0

    if-eqz p1, :cond_0

    array-length v5, p1

    if-lez v5, :cond_0

    iget v4, p0, Lcom/android/mail/ui/DividedImageCanvas;->mWidth:I

    iget v1, p0, Lcom/android/mail/ui/DividedImageCanvas;->mHeight:I

    iget-object v5, p0, Lcom/android/mail/ui/DividedImageCanvas;->mDivisionIds:Ljava/util/ArrayList;

    invoke-virtual {v5}, Ljava/util/ArrayList;->size()I

    move-result v3

    packed-switch v3, :pswitch_data_0

    :cond_0
    :goto_0
    invoke-virtual {p0, v0, p2}, Lcom/android/mail/ui/DividedImageCanvas;->addDivisionImage(Landroid/graphics/Bitmap;Ljava/lang/String;)V

    return-object v0

    :pswitch_0
    invoke-static {p1, v4, v1}, Lcom/android/mail/photomanager/BitmapUtil;->decodeBitmapFromBytes([BII)Landroid/graphics/Bitmap;

    move-result-object v0

    goto :goto_0

    :pswitch_1
    invoke-static {p1, v4, v1}, Lcom/android/mail/photomanager/BitmapUtil;->obtainBitmapWithHalfWidth([BII)Landroid/graphics/Bitmap;

    move-result-object v0

    goto :goto_0

    :pswitch_2
    packed-switch v2, :pswitch_data_1

    goto :goto_0

    :pswitch_3
    invoke-static {p1, v4, v1}, Lcom/android/mail/photomanager/BitmapUtil;->obtainBitmapWithHalfWidth([BII)Landroid/graphics/Bitmap;

    move-result-object v0

    goto :goto_0

    :pswitch_4
    div-int/lit8 v5, v4, 0x2

    div-int/lit8 v6, v1, 0x2

    invoke-static {p1, v5, v6}, Lcom/android/mail/photomanager/BitmapUtil;->decodeBitmapFromBytes([BII)Landroid/graphics/Bitmap;

    move-result-object v0

    goto :goto_0

    :pswitch_5
    div-int/lit8 v5, v4, 0x2

    div-int/lit8 v6, v1, 0x2

    invoke-static {p1, v5, v6}, Lcom/android/mail/photomanager/BitmapUtil;->decodeBitmapFromBytes([BII)Landroid/graphics/Bitmap;

    move-result-object v0

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_5
    .end packed-switch

    :pswitch_data_1
    .packed-switch 0x0
        :pswitch_3
        :pswitch_4
        :pswitch_4
    .end packed-switch
.end method

.method public addDivisionImage(Landroid/graphics/Bitmap;Ljava/lang/String;)V
    .locals 19
    .param p1    # Landroid/graphics/Bitmap;
    .param p2    # Ljava/lang/String;

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/mail/ui/DividedImageCanvas;->mDivisionIds:Ljava/util/ArrayList;

    move-object/from16 v0, p2

    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->indexOf(Ljava/lang/Object;)I

    move-result v17

    if-ltz v17, :cond_2

    if-eqz p1, :cond_2

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/mail/ui/DividedImageCanvas;->mDivisionImages:Ljava/util/ArrayList;

    move/from16 v0, v17

    move-object/from16 v1, p1

    invoke-virtual {v2, v0, v1}, Ljava/util/ArrayList;->set(ILjava/lang/Object;)Ljava/lang/Object;

    const/16 v16, 0x0

    move-object/from16 v0, p0

    iget v7, v0, Lcom/android/mail/ui/DividedImageCanvas;->mWidth:I

    move-object/from16 v0, p0

    iget v8, v0, Lcom/android/mail/ui/DividedImageCanvas;->mHeight:I

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/mail/ui/DividedImageCanvas;->mDividedBitmap:Landroid/graphics/Bitmap;

    if-nez v2, :cond_0

    sget-object v2, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    invoke-static {v7, v8, v2}, Landroid/graphics/Bitmap;->createBitmap(IILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;

    move-result-object v2

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/android/mail/ui/DividedImageCanvas;->mDividedBitmap:Landroid/graphics/Bitmap;

    new-instance v2, Landroid/graphics/Canvas;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/android/mail/ui/DividedImageCanvas;->mDividedBitmap:Landroid/graphics/Bitmap;

    invoke-direct {v2, v3}, Landroid/graphics/Canvas;-><init>(Landroid/graphics/Bitmap;)V

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/android/mail/ui/DividedImageCanvas;->mCanvas:Landroid/graphics/Canvas;

    :cond_0
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/mail/ui/DividedImageCanvas;->mDivisionIds:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v18

    packed-switch v18, :pswitch_data_0

    packed-switch v17, :pswitch_data_1

    :goto_0
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/mail/ui/DividedImageCanvas;->mDivisionImages:Ljava/util/ArrayList;

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    if-eqz v2, :cond_5

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/mail/ui/DividedImageCanvas;->mDivisionImages:Ljava/util/ArrayList;

    const/4 v3, 0x1

    invoke-virtual {v2, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    if-eqz v2, :cond_5

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/mail/ui/DividedImageCanvas;->mDivisionImages:Ljava/util/ArrayList;

    const/4 v3, 0x2

    invoke-virtual {v2, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    if-eqz v2, :cond_5

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/mail/ui/DividedImageCanvas;->mDivisionImages:Ljava/util/ArrayList;

    const/4 v3, 0x3

    invoke-virtual {v2, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    if-eqz v2, :cond_5

    const/16 v16, 0x1

    :goto_1
    if-eqz v16, :cond_1

    move-object/from16 v0, p0

    invoke-direct {v0, v7, v8}, Lcom/android/mail/ui/DividedImageCanvas;->drawVerticalDivider(II)V

    const/4 v2, 0x0

    div-int/lit8 v3, v8, 0x2

    div-int/lit8 v4, v8, 0x2

    move-object/from16 v0, p0

    invoke-direct {v0, v2, v3, v7, v4}, Lcom/android/mail/ui/DividedImageCanvas;->drawHorizontalDivider(IIII)V

    :cond_1
    :goto_2
    :pswitch_0
    if-eqz v16, :cond_2

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/mail/ui/DividedImageCanvas;->mCallback:Lcom/android/mail/ui/DividedImageCanvas$InvalidateCallback;

    invoke-interface {v2}, Lcom/android/mail/ui/DividedImageCanvas$InvalidateCallback;->invalidate()V

    :cond_2
    return-void

    :pswitch_1
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/mail/ui/DividedImageCanvas;->mDivisionImages:Ljava/util/ArrayList;

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/graphics/Bitmap;

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/mail/ui/DividedImageCanvas;->mCanvas:Landroid/graphics/Canvas;

    const/4 v5, 0x0

    const/4 v6, 0x0

    move-object/from16 v2, p0

    invoke-direct/range {v2 .. v8}, Lcom/android/mail/ui/DividedImageCanvas;->draw(Landroid/graphics/Bitmap;Landroid/graphics/Canvas;IIII)V

    const/16 v16, 0x1

    goto :goto_2

    :pswitch_2
    packed-switch v17, :pswitch_data_2

    :goto_3
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/mail/ui/DividedImageCanvas;->mDivisionImages:Ljava/util/ArrayList;

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    if-eqz v2, :cond_3

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/mail/ui/DividedImageCanvas;->mDivisionImages:Ljava/util/ArrayList;

    const/4 v3, 0x1

    invoke-virtual {v2, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    if-eqz v2, :cond_3

    const/16 v16, 0x1

    :goto_4
    if-eqz v16, :cond_1

    move-object/from16 v0, p0

    invoke-direct {v0, v7, v8}, Lcom/android/mail/ui/DividedImageCanvas;->drawVerticalDivider(II)V

    goto :goto_2

    :pswitch_3
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/mail/ui/DividedImageCanvas;->mDivisionImages:Ljava/util/ArrayList;

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v10

    check-cast v10, Landroid/graphics/Bitmap;

    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/android/mail/ui/DividedImageCanvas;->mCanvas:Landroid/graphics/Canvas;

    const/4 v12, 0x0

    const/4 v13, 0x0

    div-int/lit8 v14, v7, 0x2

    move-object/from16 v9, p0

    move v15, v8

    invoke-direct/range {v9 .. v15}, Lcom/android/mail/ui/DividedImageCanvas;->draw(Landroid/graphics/Bitmap;Landroid/graphics/Canvas;IIII)V

    goto :goto_3

    :pswitch_4
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/mail/ui/DividedImageCanvas;->mDivisionImages:Ljava/util/ArrayList;

    const/4 v3, 0x1

    invoke-virtual {v2, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/graphics/Bitmap;

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/mail/ui/DividedImageCanvas;->mCanvas:Landroid/graphics/Canvas;

    div-int/lit8 v5, v7, 0x2

    const/4 v6, 0x0

    move-object/from16 v2, p0

    invoke-direct/range {v2 .. v8}, Lcom/android/mail/ui/DividedImageCanvas;->draw(Landroid/graphics/Bitmap;Landroid/graphics/Canvas;IIII)V

    goto :goto_3

    :cond_3
    const/16 v16, 0x0

    goto :goto_4

    :pswitch_5
    packed-switch v17, :pswitch_data_3

    :goto_5
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/mail/ui/DividedImageCanvas;->mDivisionImages:Ljava/util/ArrayList;

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    if-eqz v2, :cond_4

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/mail/ui/DividedImageCanvas;->mDivisionImages:Ljava/util/ArrayList;

    const/4 v3, 0x1

    invoke-virtual {v2, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    if-eqz v2, :cond_4

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/mail/ui/DividedImageCanvas;->mDivisionImages:Ljava/util/ArrayList;

    const/4 v3, 0x2

    invoke-virtual {v2, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    if-eqz v2, :cond_4

    const/16 v16, 0x1

    :goto_6
    if-eqz v16, :cond_1

    move-object/from16 v0, p0

    invoke-direct {v0, v7, v8}, Lcom/android/mail/ui/DividedImageCanvas;->drawVerticalDivider(II)V

    div-int/lit8 v2, v7, 0x2

    div-int/lit8 v3, v8, 0x2

    div-int/lit8 v4, v8, 0x2

    move-object/from16 v0, p0

    invoke-direct {v0, v2, v3, v7, v4}, Lcom/android/mail/ui/DividedImageCanvas;->drawHorizontalDivider(IIII)V

    goto/16 :goto_2

    :pswitch_6
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/mail/ui/DividedImageCanvas;->mDivisionImages:Ljava/util/ArrayList;

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v10

    check-cast v10, Landroid/graphics/Bitmap;

    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/android/mail/ui/DividedImageCanvas;->mCanvas:Landroid/graphics/Canvas;

    const/4 v12, 0x0

    const/4 v13, 0x0

    div-int/lit8 v14, v7, 0x2

    move-object/from16 v9, p0

    move v15, v8

    invoke-direct/range {v9 .. v15}, Lcom/android/mail/ui/DividedImageCanvas;->draw(Landroid/graphics/Bitmap;Landroid/graphics/Canvas;IIII)V

    goto :goto_5

    :pswitch_7
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/mail/ui/DividedImageCanvas;->mDivisionImages:Ljava/util/ArrayList;

    const/4 v3, 0x1

    invoke-virtual {v2, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v10

    check-cast v10, Landroid/graphics/Bitmap;

    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/android/mail/ui/DividedImageCanvas;->mCanvas:Landroid/graphics/Canvas;

    div-int/lit8 v12, v7, 0x2

    const/4 v13, 0x0

    div-int/lit8 v15, v8, 0x2

    move-object/from16 v9, p0

    move v14, v7

    invoke-direct/range {v9 .. v15}, Lcom/android/mail/ui/DividedImageCanvas;->draw(Landroid/graphics/Bitmap;Landroid/graphics/Canvas;IIII)V

    goto :goto_5

    :pswitch_8
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/mail/ui/DividedImageCanvas;->mDivisionImages:Ljava/util/ArrayList;

    const/4 v3, 0x2

    invoke-virtual {v2, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/graphics/Bitmap;

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/mail/ui/DividedImageCanvas;->mCanvas:Landroid/graphics/Canvas;

    div-int/lit8 v5, v7, 0x2

    div-int/lit8 v6, v8, 0x2

    move-object/from16 v2, p0

    invoke-direct/range {v2 .. v8}, Lcom/android/mail/ui/DividedImageCanvas;->draw(Landroid/graphics/Bitmap;Landroid/graphics/Canvas;IIII)V

    goto/16 :goto_5

    :cond_4
    const/16 v16, 0x0

    goto :goto_6

    :pswitch_9
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/mail/ui/DividedImageCanvas;->mDivisionImages:Ljava/util/ArrayList;

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v10

    check-cast v10, Landroid/graphics/Bitmap;

    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/android/mail/ui/DividedImageCanvas;->mCanvas:Landroid/graphics/Canvas;

    const/4 v12, 0x0

    const/4 v13, 0x0

    div-int/lit8 v14, v7, 0x2

    div-int/lit8 v15, v8, 0x2

    move-object/from16 v9, p0

    invoke-direct/range {v9 .. v15}, Lcom/android/mail/ui/DividedImageCanvas;->draw(Landroid/graphics/Bitmap;Landroid/graphics/Canvas;IIII)V

    goto/16 :goto_0

    :pswitch_a
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/mail/ui/DividedImageCanvas;->mDivisionImages:Ljava/util/ArrayList;

    const/4 v3, 0x1

    invoke-virtual {v2, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v10

    check-cast v10, Landroid/graphics/Bitmap;

    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/android/mail/ui/DividedImageCanvas;->mCanvas:Landroid/graphics/Canvas;

    div-int/lit8 v12, v7, 0x2

    const/4 v13, 0x0

    div-int/lit8 v15, v8, 0x2

    move-object/from16 v9, p0

    move v14, v7

    invoke-direct/range {v9 .. v15}, Lcom/android/mail/ui/DividedImageCanvas;->draw(Landroid/graphics/Bitmap;Landroid/graphics/Canvas;IIII)V

    goto/16 :goto_0

    :pswitch_b
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/mail/ui/DividedImageCanvas;->mDivisionImages:Ljava/util/ArrayList;

    const/4 v3, 0x2

    invoke-virtual {v2, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v10

    check-cast v10, Landroid/graphics/Bitmap;

    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/android/mail/ui/DividedImageCanvas;->mCanvas:Landroid/graphics/Canvas;

    const/4 v12, 0x0

    div-int/lit8 v13, v8, 0x2

    div-int/lit8 v14, v7, 0x2

    move-object/from16 v9, p0

    move v15, v8

    invoke-direct/range {v9 .. v15}, Lcom/android/mail/ui/DividedImageCanvas;->draw(Landroid/graphics/Bitmap;Landroid/graphics/Canvas;IIII)V

    goto/16 :goto_0

    :pswitch_c
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/mail/ui/DividedImageCanvas;->mDivisionImages:Ljava/util/ArrayList;

    const/4 v3, 0x3

    invoke-virtual {v2, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/graphics/Bitmap;

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/mail/ui/DividedImageCanvas;->mCanvas:Landroid/graphics/Canvas;

    div-int/lit8 v5, v7, 0x2

    div-int/lit8 v6, v8, 0x2

    move-object/from16 v2, p0

    invoke-direct/range {v2 .. v8}, Lcom/android/mail/ui/DividedImageCanvas;->draw(Landroid/graphics/Bitmap;Landroid/graphics/Canvas;IIII)V

    goto/16 :goto_0

    :cond_5
    const/16 v16, 0x0

    goto/16 :goto_1

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_5
    .end packed-switch

    :pswitch_data_1
    .packed-switch 0x0
        :pswitch_9
        :pswitch_a
        :pswitch_b
        :pswitch_c
    .end packed-switch

    :pswitch_data_2
    .packed-switch 0x0
        :pswitch_3
        :pswitch_4
    .end packed-switch

    :pswitch_data_3
    .packed-switch 0x0
        :pswitch_6
        :pswitch_7
        :pswitch_8
    .end packed-switch
.end method

.method public draw(Landroid/graphics/Canvas;)V
    .locals 3
    .param p1    # Landroid/graphics/Canvas;

    const/4 v2, 0x0

    iget-object v0, p0, Lcom/android/mail/ui/DividedImageCanvas;->mDividedBitmap:Landroid/graphics/Bitmap;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/mail/ui/DividedImageCanvas;->mDividedBitmap:Landroid/graphics/Bitmap;

    sget-object v1, Lcom/android/mail/ui/DividedImageCanvas;->sPaint:Landroid/graphics/Paint;

    invoke-virtual {p1, v0, v2, v2, v1}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    :cond_0
    return-void
.end method

.method public getContext()Landroid/content/Context;
    .locals 1

    iget-object v0, p0, Lcom/android/mail/ui/DividedImageCanvas;->mContext:Landroid/content/Context;

    return-object v0
.end method

.method public getDesiredDimensions(Ljava/lang/String;)Lcom/android/mail/ui/DividedImageCanvas$Dimensions;
    .locals 6
    .param p1    # Ljava/lang/String;

    const/4 v4, 0x0

    const/4 v0, 0x0

    const/4 v2, 0x0

    iget-object v5, p0, Lcom/android/mail/ui/DividedImageCanvas;->mDivisionIds:Ljava/util/ArrayList;

    invoke-virtual {v5, p1}, Ljava/util/ArrayList;->indexOf(Ljava/lang/Object;)I

    move-result v1

    if-ltz v1, :cond_0

    iget-object v5, p0, Lcom/android/mail/ui/DividedImageCanvas;->mDivisionIds:Ljava/util/ArrayList;

    invoke-virtual {v5}, Ljava/util/ArrayList;->size()I

    move-result v3

    packed-switch v3, :pswitch_data_0

    :cond_0
    :goto_0
    :pswitch_0
    new-instance v5, Lcom/android/mail/ui/DividedImageCanvas$Dimensions;

    invoke-direct {v5, p0, v4, v0, v2}, Lcom/android/mail/ui/DividedImageCanvas$Dimensions;-><init>(Lcom/android/mail/ui/DividedImageCanvas;IIF)V

    return-object v5

    :pswitch_1
    iget v4, p0, Lcom/android/mail/ui/DividedImageCanvas;->mWidth:I

    iget v0, p0, Lcom/android/mail/ui/DividedImageCanvas;->mHeight:I

    const/high16 v2, 0x3f800000

    goto :goto_0

    :pswitch_2
    iget v5, p0, Lcom/android/mail/ui/DividedImageCanvas;->mWidth:I

    div-int/lit8 v4, v5, 0x2

    iget v0, p0, Lcom/android/mail/ui/DividedImageCanvas;->mHeight:I

    const/high16 v2, 0x3f000000

    goto :goto_0

    :pswitch_3
    packed-switch v1, :pswitch_data_1

    iget v5, p0, Lcom/android/mail/ui/DividedImageCanvas;->mWidth:I

    div-int/lit8 v4, v5, 0x2

    iget v5, p0, Lcom/android/mail/ui/DividedImageCanvas;->mHeight:I

    div-int/lit8 v0, v5, 0x2

    const/high16 v2, 0x3e800000

    goto :goto_0

    :pswitch_4
    iget v5, p0, Lcom/android/mail/ui/DividedImageCanvas;->mWidth:I

    div-int/lit8 v4, v5, 0x2

    iget v0, p0, Lcom/android/mail/ui/DividedImageCanvas;->mHeight:I

    const/high16 v2, 0x3f000000

    goto :goto_0

    :pswitch_5
    iget v5, p0, Lcom/android/mail/ui/DividedImageCanvas;->mWidth:I

    div-int/lit8 v4, v5, 0x2

    iget v5, p0, Lcom/android/mail/ui/DividedImageCanvas;->mHeight:I

    div-int/lit8 v0, v5, 0x2

    const/high16 v2, 0x3e800000

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_5
    .end packed-switch

    :pswitch_data_1
    .packed-switch 0x0
        :pswitch_4
    .end packed-switch
.end method

.method public getDivisionIds()Ljava/util/ArrayList;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/android/mail/ui/DividedImageCanvas;->mDivisionIds:Ljava/util/ArrayList;

    return-object v0
.end method

.method public reset()V
    .locals 2

    iget-object v0, p0, Lcom/android/mail/ui/DividedImageCanvas;->mCanvas:Landroid/graphics/Canvas;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/mail/ui/DividedImageCanvas;->mDividedBitmap:Landroid/graphics/Bitmap;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/mail/ui/DividedImageCanvas;->mCanvas:Landroid/graphics/Canvas;

    const/4 v1, -0x1

    invoke-virtual {v0, v1}, Landroid/graphics/Canvas;->drawColor(I)V

    :cond_0
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/android/mail/ui/DividedImageCanvas;->mDivisionIds:Ljava/util/ArrayList;

    iget-object v0, p0, Lcom/android/mail/ui/DividedImageCanvas;->mDivisionImages:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    return-void
.end method

.method public setDimensions(II)V
    .locals 0
    .param p1    # I
    .param p2    # I

    iput p1, p0, Lcom/android/mail/ui/DividedImageCanvas;->mWidth:I

    iput p2, p0, Lcom/android/mail/ui/DividedImageCanvas;->mHeight:I

    return-void
.end method

.method public setDivisionIds(Ljava/util/ArrayList;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    iput-object p1, p0, Lcom/android/mail/ui/DividedImageCanvas;->mDivisionIds:Ljava/util/ArrayList;

    const/4 v0, 0x0

    :goto_0
    iget-object v1, p0, Lcom/android/mail/ui/DividedImageCanvas;->mDivisionIds:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    if-ge v0, v1, :cond_0

    iget-object v1, p0, Lcom/android/mail/ui/DividedImageCanvas;->mDivisionImages:Ljava/util/ArrayList;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_0
    return-void
.end method
