.class public Lcom/android/mail/ui/SwipeHelper;
.super Ljava/lang/Object;
.source "SwipeHelper.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/mail/ui/SwipeHelper$Callback;
    }
.end annotation


# static fields
.field public static ALPHA_FADE_START:F

.field public static ALPHA_TEXT_FADE_START:F

.field private static DEFAULT_ESCAPE_ANIMATION_DURATION:I

.field private static DISMISS_ANIMATION_DURATION:I

.field private static MAX_DISMISS_VELOCITY:I

.field private static MAX_ESCAPE_ANIMATION_DURATION:I

.field private static MIN_LOCK:F

.field private static MIN_SWIPE:F

.field private static MIN_VERT:F

.field private static SNAP_ANIM_LEN:I

.field private static SWIPE_ESCAPE_VELOCITY:I

.field private static sDecelerateInterpolator:Landroid/view/animation/DecelerateInterpolator;


# instance fields
.field private mCallback:Lcom/android/mail/ui/SwipeHelper$Callback;

.field private mCanCurrViewBeDimissed:Z

.field private mCurrAnimView:Landroid/view/View;

.field private mCurrView:Lcom/android/mail/ui/SwipeableItemView;

.field private mDensityScale:F

.field private mDragging:Z

.field private mInitialTouchPosX:F

.field private mInitialTouchPosY:F

.field private mLastY:F

.field private mMinAlpha:F

.field private mPagingTouchSlop:F

.field private mPrevView:Lcom/android/mail/ui/LeaveBehindItem;

.field private mSwipeDirection:I

.field private mVelocityTracker:Landroid/view/VelocityTracker;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    new-instance v0, Landroid/view/animation/DecelerateInterpolator;

    const/high16 v1, 0x3f800000

    invoke-direct {v0, v1}, Landroid/view/animation/DecelerateInterpolator;-><init>(F)V

    sput-object v0, Lcom/android/mail/ui/SwipeHelper;->sDecelerateInterpolator:Landroid/view/animation/DecelerateInterpolator;

    const/4 v0, -0x1

    sput v0, Lcom/android/mail/ui/SwipeHelper;->SWIPE_ESCAPE_VELOCITY:I

    const/4 v0, 0x0

    sput v0, Lcom/android/mail/ui/SwipeHelper;->ALPHA_FADE_START:F

    const v0, 0x3ecccccd

    sput v0, Lcom/android/mail/ui/SwipeHelper;->ALPHA_TEXT_FADE_START:F

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;ILcom/android/mail/ui/SwipeHelper$Callback;FF)V
    .locals 3
    .param p1    # Landroid/content/Context;
    .param p2    # I
    .param p3    # Lcom/android/mail/ui/SwipeHelper$Callback;
    .param p4    # F
    .param p5    # F

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/high16 v1, 0x3f000000

    iput v1, p0, Lcom/android/mail/ui/SwipeHelper;->mMinAlpha:F

    iput-object p3, p0, Lcom/android/mail/ui/SwipeHelper;->mCallback:Lcom/android/mail/ui/SwipeHelper$Callback;

    iput p2, p0, Lcom/android/mail/ui/SwipeHelper;->mSwipeDirection:I

    invoke-static {}, Landroid/view/VelocityTracker;->obtain()Landroid/view/VelocityTracker;

    move-result-object v1

    iput-object v1, p0, Lcom/android/mail/ui/SwipeHelper;->mVelocityTracker:Landroid/view/VelocityTracker;

    iput p4, p0, Lcom/android/mail/ui/SwipeHelper;->mDensityScale:F

    iput p5, p0, Lcom/android/mail/ui/SwipeHelper;->mPagingTouchSlop:F

    sget v1, Lcom/android/mail/ui/SwipeHelper;->SWIPE_ESCAPE_VELOCITY:I

    const/4 v2, -0x1

    if-ne v1, v2, :cond_0

    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0b000d

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v1

    sput v1, Lcom/android/mail/ui/SwipeHelper;->SWIPE_ESCAPE_VELOCITY:I

    const v1, 0x7f0b000e

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v1

    sput v1, Lcom/android/mail/ui/SwipeHelper;->DEFAULT_ESCAPE_ANIMATION_DURATION:I

    const v1, 0x7f0b000f

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v1

    sput v1, Lcom/android/mail/ui/SwipeHelper;->MAX_ESCAPE_ANIMATION_DURATION:I

    const v1, 0x7f0b0010

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v1

    sput v1, Lcom/android/mail/ui/SwipeHelper;->MAX_DISMISS_VELOCITY:I

    const v1, 0x7f0b0011

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v1

    sput v1, Lcom/android/mail/ui/SwipeHelper;->SNAP_ANIM_LEN:I

    const v1, 0x7f0b0012

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v1

    sput v1, Lcom/android/mail/ui/SwipeHelper;->DISMISS_ANIMATION_DURATION:I

    const v1, 0x7f0c0009

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v1

    sput v1, Lcom/android/mail/ui/SwipeHelper;->MIN_SWIPE:F

    const v1, 0x7f0c000a

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v1

    sput v1, Lcom/android/mail/ui/SwipeHelper;->MIN_VERT:F

    const v1, 0x7f0c000b

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v1

    sput v1, Lcom/android/mail/ui/SwipeHelper;->MIN_LOCK:F

    :cond_0
    return-void
.end method

.method static synthetic access$000(Lcom/android/mail/ui/SwipeHelper;)Lcom/android/mail/ui/SwipeableItemView;
    .locals 1
    .param p0    # Lcom/android/mail/ui/SwipeHelper;

    iget-object v0, p0, Lcom/android/mail/ui/SwipeHelper;->mCurrView:Lcom/android/mail/ui/SwipeableItemView;

    return-object v0
.end method

.method static synthetic access$100(Lcom/android/mail/ui/SwipeHelper;)Lcom/android/mail/ui/SwipeHelper$Callback;
    .locals 1
    .param p0    # Lcom/android/mail/ui/SwipeHelper;

    iget-object v0, p0, Lcom/android/mail/ui/SwipeHelper;->mCallback:Lcom/android/mail/ui/SwipeHelper$Callback;

    return-object v0
.end method

.method static synthetic access$200(Lcom/android/mail/ui/SwipeHelper;Landroid/view/View;)F
    .locals 1
    .param p0    # Lcom/android/mail/ui/SwipeHelper;
    .param p1    # Landroid/view/View;

    invoke-direct {p0, p1}, Lcom/android/mail/ui/SwipeHelper;->getAlphaForOffset(Landroid/view/View;)F

    move-result v0

    return v0
.end method

.method private createDismissAnimation(Landroid/view/View;FI)Landroid/animation/ObjectAnimator;
    .locals 3
    .param p1    # Landroid/view/View;
    .param p2    # F
    .param p3    # I

    invoke-direct {p0, p1, p2}, Lcom/android/mail/ui/SwipeHelper;->createTranslationAnimation(Landroid/view/View;F)Landroid/animation/ObjectAnimator;

    move-result-object v0

    sget-object v1, Lcom/android/mail/ui/SwipeHelper;->sDecelerateInterpolator:Landroid/view/animation/DecelerateInterpolator;

    invoke-virtual {v0, v1}, Landroid/animation/ObjectAnimator;->setInterpolator(Landroid/animation/TimeInterpolator;)V

    int-to-long v1, p3

    invoke-virtual {v0, v1, v2}, Landroid/animation/ObjectAnimator;->setDuration(J)Landroid/animation/ObjectAnimator;

    return-object v0
.end method

.method private createTranslationAnimation(Landroid/view/View;F)Landroid/animation/ObjectAnimator;
    .locals 4
    .param p1    # Landroid/view/View;
    .param p2    # F

    iget v1, p0, Lcom/android/mail/ui/SwipeHelper;->mSwipeDirection:I

    if-nez v1, :cond_0

    const-string v1, "translationX"

    :goto_0
    const/4 v2, 0x1

    new-array v2, v2, [F

    const/4 v3, 0x0

    aput p2, v2, v3

    invoke-static {p1, v1, v2}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Ljava/lang/String;[F)Landroid/animation/ObjectAnimator;

    move-result-object v0

    return-object v0

    :cond_0
    const-string v1, "translationY"

    goto :goto_0
.end method

.method private determineDuration(Landroid/view/View;FF)I
    .locals 3
    .param p1    # Landroid/view/View;
    .param p2    # F
    .param p3    # F

    sget v0, Lcom/android/mail/ui/SwipeHelper;->MAX_ESCAPE_ANIMATION_DURATION:I

    const/4 v1, 0x0

    cmpl-float v1, p3, v1

    if-eqz v1, :cond_0

    invoke-virtual {p1}, Landroid/view/View;->getTranslationX()F

    move-result v1

    sub-float v1, p2, v1

    invoke-static {v1}, Ljava/lang/Math;->abs(F)F

    move-result v1

    const/high16 v2, 0x447a0000

    mul-float/2addr v1, v2

    invoke-static {p3}, Ljava/lang/Math;->abs(F)F

    move-result v2

    div-float/2addr v1, v2

    float-to-int v1, v1

    invoke-static {v0, v1}, Ljava/lang/Math;->min(II)I

    move-result v0

    :goto_0
    return v0

    :cond_0
    sget v0, Lcom/android/mail/ui/SwipeHelper;->DEFAULT_ESCAPE_ANIMATION_DURATION:I

    goto :goto_0
.end method

.method private determinePos(Landroid/view/View;F)F
    .locals 3
    .param p1    # Landroid/view/View;
    .param p2    # F

    const/4 v2, 0x0

    const/4 v0, 0x0

    cmpg-float v1, p2, v2

    if-ltz v1, :cond_1

    cmpl-float v1, p2, v2

    if-nez v1, :cond_0

    invoke-virtual {p1}, Landroid/view/View;->getTranslationX()F

    move-result v1

    cmpg-float v1, v1, v2

    if-ltz v1, :cond_1

    :cond_0
    cmpl-float v1, p2, v2

    if-nez v1, :cond_2

    invoke-virtual {p1}, Landroid/view/View;->getTranslationX()F

    move-result v1

    cmpl-float v1, v1, v2

    if-nez v1, :cond_2

    iget v1, p0, Lcom/android/mail/ui/SwipeHelper;->mSwipeDirection:I

    const/4 v2, 0x1

    if-ne v1, v2, :cond_2

    :cond_1
    invoke-direct {p0, p1}, Lcom/android/mail/ui/SwipeHelper;->getSize(Landroid/view/View;)F

    move-result v1

    neg-float v0, v1

    :goto_0
    return v0

    :cond_2
    invoke-direct {p0, p1}, Lcom/android/mail/ui/SwipeHelper;->getSize(Landroid/view/View;)F

    move-result v0

    goto :goto_0
.end method

.method private dismissChild(Lcom/android/mail/ui/SwipeableItemView;F)V
    .locals 6
    .param p1    # Lcom/android/mail/ui/SwipeableItemView;
    .param p2    # F

    iget-object v5, p0, Lcom/android/mail/ui/SwipeHelper;->mCurrView:Lcom/android/mail/ui/SwipeableItemView;

    invoke-interface {v5}, Lcom/android/mail/ui/SwipeableItemView;->getSwipeableView()Landroid/view/View;

    move-result-object v1

    iget-object v5, p0, Lcom/android/mail/ui/SwipeHelper;->mCallback:Lcom/android/mail/ui/SwipeHelper$Callback;

    invoke-interface {v5, p1}, Lcom/android/mail/ui/SwipeHelper$Callback;->canChildBeDismissed(Lcom/android/mail/ui/SwipeableItemView;)Z

    move-result v2

    invoke-direct {p0, v1, p2}, Lcom/android/mail/ui/SwipeHelper;->determinePos(Landroid/view/View;F)F

    move-result v4

    invoke-direct {p0, v1, v4, p2}, Lcom/android/mail/ui/SwipeHelper;->determineDuration(Landroid/view/View;FF)I

    move-result v3

    invoke-static {v1}, Lcom/android/mail/utils/Utils;->enableHardwareLayer(Landroid/view/View;)V

    invoke-direct {p0, v1, v4, v3}, Lcom/android/mail/ui/SwipeHelper;->createDismissAnimation(Landroid/view/View;FI)Landroid/animation/ObjectAnimator;

    move-result-object v0

    new-instance v5, Lcom/android/mail/ui/SwipeHelper$1;

    invoke-direct {v5, p0, v1}, Lcom/android/mail/ui/SwipeHelper$1;-><init>(Lcom/android/mail/ui/SwipeHelper;Landroid/view/View;)V

    invoke-virtual {v0, v5}, Landroid/animation/ObjectAnimator;->addListener(Landroid/animation/Animator$AnimatorListener;)V

    new-instance v5, Lcom/android/mail/ui/SwipeHelper$2;

    invoke-direct {v5, p0, v2, v1}, Lcom/android/mail/ui/SwipeHelper$2;-><init>(Lcom/android/mail/ui/SwipeHelper;ZLandroid/view/View;)V

    invoke-virtual {v0, v5}, Landroid/animation/ObjectAnimator;->addUpdateListener(Landroid/animation/ValueAnimator$AnimatorUpdateListener;)V

    invoke-virtual {v0}, Landroid/animation/ObjectAnimator;->start()V

    return-void
.end method

.method private getAlphaForOffset(Landroid/view/View;)F
    .locals 6
    .param p1    # Landroid/view/View;

    const/high16 v5, 0x3f800000

    invoke-direct {p0, p1}, Lcom/android/mail/ui/SwipeHelper;->getSize(Landroid/view/View;)F

    move-result v3

    const v4, 0x3f333333

    mul-float v0, v4, v3

    const/high16 v2, 0x3f800000

    invoke-virtual {p1}, Landroid/view/View;->getTranslationX()F

    move-result v1

    sget v4, Lcom/android/mail/ui/SwipeHelper;->ALPHA_FADE_START:F

    mul-float/2addr v4, v3

    cmpl-float v4, v1, v4

    if-ltz v4, :cond_1

    sget v4, Lcom/android/mail/ui/SwipeHelper;->ALPHA_FADE_START:F

    mul-float/2addr v4, v3

    sub-float v4, v1, v4

    div-float/2addr v4, v0

    sub-float v2, v5, v4

    :cond_0
    :goto_0
    iget v4, p0, Lcom/android/mail/ui/SwipeHelper;->mMinAlpha:F

    invoke-static {v4, v2}, Ljava/lang/Math;->max(FF)F

    move-result v4

    return v4

    :cond_1
    sget v4, Lcom/android/mail/ui/SwipeHelper;->ALPHA_FADE_START:F

    sub-float v4, v5, v4

    mul-float/2addr v4, v3

    cmpg-float v4, v1, v4

    if-gez v4, :cond_0

    sget v4, Lcom/android/mail/ui/SwipeHelper;->ALPHA_FADE_START:F

    mul-float/2addr v4, v3

    add-float/2addr v4, v1

    div-float/2addr v4, v0

    add-float v2, v5, v4

    goto :goto_0
.end method

.method private getPerpendicularVelocity(Landroid/view/VelocityTracker;)F
    .locals 1
    .param p1    # Landroid/view/VelocityTracker;

    iget v0, p0, Lcom/android/mail/ui/SwipeHelper;->mSwipeDirection:I

    if-nez v0, :cond_0

    invoke-virtual {p1}, Landroid/view/VelocityTracker;->getYVelocity()F

    move-result v0

    :goto_0
    return v0

    :cond_0
    invoke-virtual {p1}, Landroid/view/VelocityTracker;->getXVelocity()F

    move-result v0

    goto :goto_0
.end method

.method private getSize(Landroid/view/View;)F
    .locals 1
    .param p1    # Landroid/view/View;

    iget v0, p0, Lcom/android/mail/ui/SwipeHelper;->mSwipeDirection:I

    if-nez v0, :cond_0

    invoke-virtual {p1}, Landroid/view/View;->getMeasuredWidth()I

    move-result v0

    int-to-float v0, v0

    :goto_0
    return v0

    :cond_0
    invoke-virtual {p1}, Landroid/view/View;->getMeasuredHeight()I

    move-result v0

    int-to-float v0, v0

    goto :goto_0
.end method

.method private getTextAlphaForOffset(Landroid/view/View;)F
    .locals 7
    .param p1    # Landroid/view/View;

    const/high16 v6, 0x3f800000

    const/4 v5, 0x0

    invoke-direct {p0, p1}, Lcom/android/mail/ui/SwipeHelper;->getSize(Landroid/view/View;)F

    move-result v3

    sget v4, Lcom/android/mail/ui/SwipeHelper;->ALPHA_TEXT_FADE_START:F

    mul-float v0, v4, v3

    const/high16 v2, 0x3f800000

    invoke-virtual {p1}, Landroid/view/View;->getTranslationX()F

    move-result v1

    cmpl-float v4, v1, v5

    if-ltz v4, :cond_1

    div-float v4, v1, v0

    sub-float v2, v6, v4

    :cond_0
    :goto_0
    invoke-static {v5, v2}, Ljava/lang/Math;->max(FF)F

    move-result v4

    return v4

    :cond_1
    cmpg-float v4, v1, v5

    if-gez v4, :cond_0

    div-float v4, v1, v0

    add-float v2, v6, v4

    goto :goto_0
.end method

.method private getVelocity(Landroid/view/VelocityTracker;)F
    .locals 1
    .param p1    # Landroid/view/VelocityTracker;

    iget v0, p0, Lcom/android/mail/ui/SwipeHelper;->mSwipeDirection:I

    if-nez v0, :cond_0

    invoke-virtual {p1}, Landroid/view/VelocityTracker;->getXVelocity()F

    move-result v0

    :goto_0
    return v0

    :cond_0
    invoke-virtual {p1}, Landroid/view/VelocityTracker;->getYVelocity()F

    move-result v0

    goto :goto_0
.end method

.method public static invalidateGlobalRegion(Landroid/view/View;)V
    .locals 5
    .param p0    # Landroid/view/View;

    new-instance v0, Landroid/graphics/RectF;

    invoke-virtual {p0}, Landroid/view/View;->getLeft()I

    move-result v1

    int-to-float v1, v1

    invoke-virtual {p0}, Landroid/view/View;->getTop()I

    move-result v2

    int-to-float v2, v2

    invoke-virtual {p0}, Landroid/view/View;->getRight()I

    move-result v3

    int-to-float v3, v3

    invoke-virtual {p0}, Landroid/view/View;->getBottom()I

    move-result v4

    int-to-float v4, v4

    invoke-direct {v0, v1, v2, v3, v4}, Landroid/graphics/RectF;-><init>(FFFF)V

    invoke-static {p0, v0}, Lcom/android/mail/ui/SwipeHelper;->invalidateGlobalRegion(Landroid/view/View;Landroid/graphics/RectF;)V

    return-void
.end method

.method public static invalidateGlobalRegion(Landroid/view/View;Landroid/graphics/RectF;)V
    .locals 5
    .param p0    # Landroid/view/View;
    .param p1    # Landroid/graphics/RectF;

    :goto_0
    invoke-virtual {p0}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    instance-of v0, v0, Landroid/view/View;

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    move-result-object p0

    check-cast p0, Landroid/view/View;

    invoke-virtual {p0}, Landroid/view/View;->getMatrix()Landroid/graphics/Matrix;

    move-result-object v0

    invoke-virtual {v0, p1}, Landroid/graphics/Matrix;->mapRect(Landroid/graphics/RectF;)Z

    iget v0, p1, Landroid/graphics/RectF;->left:F

    float-to-double v0, v0

    invoke-static {v0, v1}, Ljava/lang/Math;->floor(D)D

    move-result-wide v0

    double-to-int v0, v0

    iget v1, p1, Landroid/graphics/RectF;->top:F

    float-to-double v1, v1

    invoke-static {v1, v2}, Ljava/lang/Math;->floor(D)D

    move-result-wide v1

    double-to-int v1, v1

    iget v2, p1, Landroid/graphics/RectF;->right:F

    float-to-double v2, v2

    invoke-static {v2, v3}, Ljava/lang/Math;->ceil(D)D

    move-result-wide v2

    double-to-int v2, v2

    iget v3, p1, Landroid/graphics/RectF;->bottom:F

    float-to-double v3, v3

    invoke-static {v3, v4}, Ljava/lang/Math;->ceil(D)D

    move-result-wide v3

    double-to-int v3, v3

    invoke-virtual {p0, v0, v1, v2, v3}, Landroid/view/View;->invalidate(IIII)V

    goto :goto_0

    :cond_0
    return-void
.end method

.method private setTranslation(Landroid/view/View;F)V
    .locals 1
    .param p1    # Landroid/view/View;
    .param p2    # F

    iget v0, p0, Lcom/android/mail/ui/SwipeHelper;->mSwipeDirection:I

    if-nez v0, :cond_0

    invoke-virtual {p1, p2}, Landroid/view/View;->setTranslationX(F)V

    :goto_0
    return-void

    :cond_0
    invoke-virtual {p1, p2}, Landroid/view/View;->setTranslationY(F)V

    goto :goto_0
.end method


# virtual methods
.method public onInterceptTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 11
    .param p1    # Landroid/view/MotionEvent;

    const/4 v9, 0x0

    const/4 v8, 0x0

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    :goto_0
    iget-boolean v8, p0, Lcom/android/mail/ui/SwipeHelper;->mDragging:Z

    :goto_1
    return v8

    :pswitch_0
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v9

    iput v9, p0, Lcom/android/mail/ui/SwipeHelper;->mLastY:F

    iput-boolean v8, p0, Lcom/android/mail/ui/SwipeHelper;->mDragging:Z

    iget-object v8, p0, Lcom/android/mail/ui/SwipeHelper;->mCallback:Lcom/android/mail/ui/SwipeHelper$Callback;

    invoke-interface {v8, p1}, Lcom/android/mail/ui/SwipeHelper$Callback;->getChildAtPosition(Landroid/view/MotionEvent;)Landroid/view/View;

    move-result-object v7

    instance-of v8, v7, Lcom/android/mail/ui/SwipeableItemView;

    if-eqz v8, :cond_0

    check-cast v7, Lcom/android/mail/ui/SwipeableItemView;

    iput-object v7, p0, Lcom/android/mail/ui/SwipeHelper;->mCurrView:Lcom/android/mail/ui/SwipeableItemView;

    :cond_0
    iget-object v8, p0, Lcom/android/mail/ui/SwipeHelper;->mVelocityTracker:Landroid/view/VelocityTracker;

    invoke-virtual {v8}, Landroid/view/VelocityTracker;->clear()V

    iget-object v8, p0, Lcom/android/mail/ui/SwipeHelper;->mCurrView:Lcom/android/mail/ui/SwipeableItemView;

    if-eqz v8, :cond_1

    iget-object v8, p0, Lcom/android/mail/ui/SwipeHelper;->mCurrView:Lcom/android/mail/ui/SwipeableItemView;

    invoke-interface {v8}, Lcom/android/mail/ui/SwipeableItemView;->getSwipeableView()Landroid/view/View;

    move-result-object v8

    iput-object v8, p0, Lcom/android/mail/ui/SwipeHelper;->mCurrAnimView:Landroid/view/View;

    iget-object v8, p0, Lcom/android/mail/ui/SwipeHelper;->mCallback:Lcom/android/mail/ui/SwipeHelper$Callback;

    iget-object v9, p0, Lcom/android/mail/ui/SwipeHelper;->mCurrView:Lcom/android/mail/ui/SwipeableItemView;

    invoke-interface {v8, v9}, Lcom/android/mail/ui/SwipeHelper$Callback;->canChildBeDismissed(Lcom/android/mail/ui/SwipeableItemView;)Z

    move-result v8

    iput-boolean v8, p0, Lcom/android/mail/ui/SwipeHelper;->mCanCurrViewBeDimissed:Z

    iget-object v8, p0, Lcom/android/mail/ui/SwipeHelper;->mVelocityTracker:Landroid/view/VelocityTracker;

    invoke-virtual {v8, p1}, Landroid/view/VelocityTracker;->addMovement(Landroid/view/MotionEvent;)V

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v8

    iput v8, p0, Lcom/android/mail/ui/SwipeHelper;->mInitialTouchPosX:F

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v8

    iput v8, p0, Lcom/android/mail/ui/SwipeHelper;->mInitialTouchPosY:F

    :cond_1
    iget-object v8, p0, Lcom/android/mail/ui/SwipeHelper;->mCallback:Lcom/android/mail/ui/SwipeHelper$Callback;

    invoke-interface {v8}, Lcom/android/mail/ui/SwipeHelper$Callback;->cancelDismissCounter()V

    goto :goto_0

    :pswitch_1
    iget-object v9, p0, Lcom/android/mail/ui/SwipeHelper;->mCurrView:Lcom/android/mail/ui/SwipeableItemView;

    if-eqz v9, :cond_3

    iget v9, p0, Lcom/android/mail/ui/SwipeHelper;->mLastY:F

    const/4 v10, 0x0

    cmpl-float v9, v9, v10

    if-ltz v9, :cond_2

    iget-boolean v9, p0, Lcom/android/mail/ui/SwipeHelper;->mDragging:Z

    if-nez v9, :cond_2

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v2

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v1

    iget v9, p0, Lcom/android/mail/ui/SwipeHelper;->mInitialTouchPosY:F

    sub-float v9, v2, v9

    invoke-static {v9}, Ljava/lang/Math;->abs(F)F

    move-result v5

    iget v9, p0, Lcom/android/mail/ui/SwipeHelper;->mInitialTouchPosX:F

    sub-float v9, v1, v9

    invoke-static {v9}, Ljava/lang/Math;->abs(F)F

    move-result v4

    iget-object v9, p0, Lcom/android/mail/ui/SwipeHelper;->mCurrView:Lcom/android/mail/ui/SwipeableItemView;

    invoke-interface {v9}, Lcom/android/mail/ui/SwipeableItemView;->getMinAllowScrollDistance()F

    move-result v9

    cmpl-float v9, v5, v9

    if-lez v9, :cond_2

    const v9, 0x3f99999a

    mul-float/2addr v9, v4

    cmpl-float v9, v5, v9

    if-lez v9, :cond_2

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v9

    iput v9, p0, Lcom/android/mail/ui/SwipeHelper;->mLastY:F

    iget-object v9, p0, Lcom/android/mail/ui/SwipeHelper;->mCallback:Lcom/android/mail/ui/SwipeHelper$Callback;

    invoke-interface {v9}, Lcom/android/mail/ui/SwipeHelper$Callback;->onScroll()V

    goto/16 :goto_1

    :cond_2
    iget-object v8, p0, Lcom/android/mail/ui/SwipeHelper;->mVelocityTracker:Landroid/view/VelocityTracker;

    invoke-virtual {v8, p1}, Landroid/view/VelocityTracker;->addMovement(Landroid/view/MotionEvent;)V

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v6

    iget v8, p0, Lcom/android/mail/ui/SwipeHelper;->mInitialTouchPosX:F

    sub-float v3, v6, v8

    invoke-static {v3}, Ljava/lang/Math;->abs(F)F

    move-result v8

    iget v9, p0, Lcom/android/mail/ui/SwipeHelper;->mPagingTouchSlop:F

    cmpl-float v8, v8, v9

    if-lez v8, :cond_3

    iget-object v8, p0, Lcom/android/mail/ui/SwipeHelper;->mCallback:Lcom/android/mail/ui/SwipeHelper$Callback;

    iget-object v9, p0, Lcom/android/mail/ui/SwipeHelper;->mCurrView:Lcom/android/mail/ui/SwipeableItemView;

    invoke-interface {v9}, Lcom/android/mail/ui/SwipeableItemView;->getSwipeableView()Landroid/view/View;

    move-result-object v9

    invoke-interface {v8, v9}, Lcom/android/mail/ui/SwipeHelper$Callback;->onBeginDrag(Landroid/view/View;)V

    iget-object v8, p0, Lcom/android/mail/ui/SwipeHelper;->mCallback:Lcom/android/mail/ui/SwipeHelper$Callback;

    invoke-interface {v8}, Lcom/android/mail/ui/SwipeHelper$Callback;->getLastSwipedItem()Lcom/android/mail/ui/LeaveBehindItem;

    move-result-object v8

    iput-object v8, p0, Lcom/android/mail/ui/SwipeHelper;->mPrevView:Lcom/android/mail/ui/LeaveBehindItem;

    const/4 v8, 0x1

    iput-boolean v8, p0, Lcom/android/mail/ui/SwipeHelper;->mDragging:Z

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v8

    iget-object v9, p0, Lcom/android/mail/ui/SwipeHelper;->mCurrAnimView:Landroid/view/View;

    invoke-virtual {v9}, Landroid/view/View;->getTranslationX()F

    move-result v9

    sub-float/2addr v8, v9

    iput v8, p0, Lcom/android/mail/ui/SwipeHelper;->mInitialTouchPosX:F

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v8

    iput v8, p0, Lcom/android/mail/ui/SwipeHelper;->mInitialTouchPosY:F

    :cond_3
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v8

    iput v8, p0, Lcom/android/mail/ui/SwipeHelper;->mLastY:F

    goto/16 :goto_0

    :pswitch_2
    iput-boolean v8, p0, Lcom/android/mail/ui/SwipeHelper;->mDragging:Z

    iput-object v9, p0, Lcom/android/mail/ui/SwipeHelper;->mCurrView:Lcom/android/mail/ui/SwipeableItemView;

    iput-object v9, p0, Lcom/android/mail/ui/SwipeHelper;->mCurrAnimView:Landroid/view/View;

    const/high16 v8, -0x40800000

    iput v8, p0, Lcom/android/mail/ui/SwipeHelper;->mLastY:F

    goto/16 :goto_0

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_2
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public onTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 24
    .param p1    # Landroid/view/MotionEvent;

    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/android/mail/ui/SwipeHelper;->mDragging:Z

    move/from16 v18, v0

    if-nez v18, :cond_0

    const/16 v18, 0x0

    :goto_0
    return v18

    :cond_0
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/mail/ui/SwipeHelper;->mVelocityTracker:Landroid/view/VelocityTracker;

    move-object/from16 v18, v0

    move-object/from16 v0, v18

    move-object/from16 v1, p1

    invoke-virtual {v0, v1}, Landroid/view/VelocityTracker;->addMovement(Landroid/view/MotionEvent;)V

    invoke-virtual/range {p1 .. p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v3

    packed-switch v3, :pswitch_data_0

    :cond_1
    :goto_1
    const/16 v18, 0x1

    goto :goto_0

    :pswitch_0
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/mail/ui/SwipeHelper;->mCurrView:Lcom/android/mail/ui/SwipeableItemView;

    move-object/from16 v18, v0

    if-eqz v18, :cond_1

    invoke-virtual/range {p1 .. p1}, Landroid/view/MotionEvent;->getX()F

    move-result v18

    move-object/from16 v0, p0

    iget v0, v0, Lcom/android/mail/ui/SwipeHelper;->mInitialTouchPosX:F

    move/from16 v19, v0

    sub-float v7, v18, v19

    invoke-virtual/range {p1 .. p1}, Landroid/view/MotionEvent;->getY()F

    move-result v18

    move-object/from16 v0, p0

    iget v0, v0, Lcom/android/mail/ui/SwipeHelper;->mInitialTouchPosY:F

    move/from16 v19, v0

    sub-float v18, v18, v19

    invoke-static/range {v18 .. v18}, Ljava/lang/Math;->abs(F)F

    move-result v8

    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/android/mail/ui/SwipeHelper;->mDragging:Z

    move/from16 v18, v0

    if-nez v18, :cond_2

    sget v18, Lcom/android/mail/ui/SwipeHelper;->MIN_VERT:F

    cmpl-float v18, v8, v18

    if-lez v18, :cond_2

    invoke-static {v7}, Ljava/lang/Math;->abs(F)F

    move-result v18

    sget v19, Lcom/android/mail/ui/SwipeHelper;->MIN_LOCK:F

    cmpg-float v18, v18, v19

    if-gez v18, :cond_2

    const v18, 0x3f99999a

    invoke-static {v7}, Ljava/lang/Math;->abs(F)F

    move-result v19

    mul-float v18, v18, v19

    cmpl-float v18, v8, v18

    if-lez v18, :cond_2

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/mail/ui/SwipeHelper;->mCallback:Lcom/android/mail/ui/SwipeHelper$Callback;

    move-object/from16 v18, v0

    invoke-interface/range {v18 .. v18}, Lcom/android/mail/ui/SwipeHelper$Callback;->onScroll()V

    const/16 v18, 0x0

    goto :goto_0

    :cond_2
    sget v13, Lcom/android/mail/ui/SwipeHelper;->MIN_SWIPE:F

    invoke-static {v7}, Ljava/lang/Math;->abs(F)F

    move-result v18

    cmpg-float v18, v18, v13

    if-gez v18, :cond_3

    const/16 v18, 0x1

    goto :goto_0

    :cond_3
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/mail/ui/SwipeHelper;->mCallback:Lcom/android/mail/ui/SwipeHelper$Callback;

    move-object/from16 v18, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/mail/ui/SwipeHelper;->mCurrView:Lcom/android/mail/ui/SwipeableItemView;

    move-object/from16 v19, v0

    invoke-interface/range {v18 .. v19}, Lcom/android/mail/ui/SwipeHelper$Callback;->canChildBeDismissed(Lcom/android/mail/ui/SwipeableItemView;)Z

    move-result v18

    if-nez v18, :cond_4

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/mail/ui/SwipeHelper;->mCurrAnimView:Landroid/view/View;

    move-object/from16 v18, v0

    move-object/from16 v0, p0

    move-object/from16 v1, v18

    invoke-direct {v0, v1}, Lcom/android/mail/ui/SwipeHelper;->getSize(Landroid/view/View;)F

    move-result v15

    const v18, 0x3e19999a

    mul-float v11, v18, v15

    invoke-static {v7}, Ljava/lang/Math;->abs(F)F

    move-result v18

    cmpl-float v18, v18, v15

    if-ltz v18, :cond_7

    const/16 v18, 0x0

    cmpl-float v18, v7, v18

    if-lez v18, :cond_6

    move v7, v11

    :cond_4
    :goto_2
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/mail/ui/SwipeHelper;->mCurrAnimView:Landroid/view/View;

    move-object/from16 v18, v0

    move-object/from16 v0, p0

    move-object/from16 v1, v18

    invoke-direct {v0, v1, v7}, Lcom/android/mail/ui/SwipeHelper;->setTranslation(Landroid/view/View;F)V

    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/android/mail/ui/SwipeHelper;->mCanCurrViewBeDimissed:Z

    move/from16 v18, v0

    if-eqz v18, :cond_5

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/mail/ui/SwipeHelper;->mCurrAnimView:Landroid/view/View;

    move-object/from16 v18, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/mail/ui/SwipeHelper;->mCurrAnimView:Landroid/view/View;

    move-object/from16 v19, v0

    move-object/from16 v0, p0

    move-object/from16 v1, v19

    invoke-direct {v0, v1}, Lcom/android/mail/ui/SwipeHelper;->getAlphaForOffset(Landroid/view/View;)F

    move-result v19

    invoke-virtual/range {v18 .. v19}, Landroid/view/View;->setAlpha(F)V

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/mail/ui/SwipeHelper;->mPrevView:Lcom/android/mail/ui/LeaveBehindItem;

    move-object/from16 v18, v0

    if-eqz v18, :cond_5

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/mail/ui/SwipeHelper;->mPrevView:Lcom/android/mail/ui/LeaveBehindItem;

    move-object/from16 v18, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/mail/ui/SwipeHelper;->mCurrAnimView:Landroid/view/View;

    move-object/from16 v19, v0

    move-object/from16 v0, p0

    move-object/from16 v1, v19

    invoke-direct {v0, v1}, Lcom/android/mail/ui/SwipeHelper;->getTextAlphaForOffset(Landroid/view/View;)F

    move-result v19

    invoke-virtual/range {v18 .. v19}, Lcom/android/mail/ui/LeaveBehindItem;->setTextAlpha(F)V

    :cond_5
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/mail/ui/SwipeHelper;->mCurrView:Lcom/android/mail/ui/SwipeableItemView;

    move-object/from16 v18, v0

    invoke-interface/range {v18 .. v18}, Lcom/android/mail/ui/SwipeableItemView;->getSwipeableView()Landroid/view/View;

    move-result-object v18

    invoke-static/range {v18 .. v18}, Lcom/android/mail/ui/SwipeHelper;->invalidateGlobalRegion(Landroid/view/View;)V

    goto/16 :goto_1

    :cond_6
    neg-float v7, v11

    goto :goto_2

    :cond_7
    div-float v18, v7, v15

    move/from16 v0, v18

    float-to-double v0, v0

    move-wide/from16 v18, v0

    const-wide v20, 0x3ff921fb54442d18L

    mul-double v18, v18, v20

    invoke-static/range {v18 .. v19}, Ljava/lang/Math;->sin(D)D

    move-result-wide v18

    move-wide/from16 v0, v18

    double-to-float v0, v0

    move/from16 v18, v0

    mul-float v7, v11, v18

    goto :goto_2

    :pswitch_1
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/mail/ui/SwipeHelper;->mCurrView:Lcom/android/mail/ui/SwipeableItemView;

    move-object/from16 v18, v0

    if-eqz v18, :cond_1

    sget v18, Lcom/android/mail/ui/SwipeHelper;->MAX_DISMISS_VELOCITY:I

    move/from16 v0, v18

    int-to-float v0, v0

    move/from16 v18, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/android/mail/ui/SwipeHelper;->mDensityScale:F

    move/from16 v19, v0

    mul-float v12, v18, v19

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/mail/ui/SwipeHelper;->mVelocityTracker:Landroid/view/VelocityTracker;

    move-object/from16 v18, v0

    const/16 v19, 0x3e8

    move-object/from16 v0, v18

    move/from16 v1, v19

    invoke-virtual {v0, v1, v12}, Landroid/view/VelocityTracker;->computeCurrentVelocity(IF)V

    sget v18, Lcom/android/mail/ui/SwipeHelper;->SWIPE_ESCAPE_VELOCITY:I

    move/from16 v0, v18

    int-to-float v0, v0

    move/from16 v18, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/android/mail/ui/SwipeHelper;->mDensityScale:F

    move/from16 v19, v0

    mul-float v10, v18, v19

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/mail/ui/SwipeHelper;->mVelocityTracker:Landroid/view/VelocityTracker;

    move-object/from16 v18, v0

    move-object/from16 v0, p0

    move-object/from16 v1, v18

    invoke-direct {v0, v1}, Lcom/android/mail/ui/SwipeHelper;->getVelocity(Landroid/view/VelocityTracker;)F

    move-result v17

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/mail/ui/SwipeHelper;->mVelocityTracker:Landroid/view/VelocityTracker;

    move-object/from16 v18, v0

    move-object/from16 v0, p0

    move-object/from16 v1, v18

    invoke-direct {v0, v1}, Lcom/android/mail/ui/SwipeHelper;->getPerpendicularVelocity(Landroid/view/VelocityTracker;)F

    move-result v14

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/mail/ui/SwipeHelper;->mCurrAnimView:Landroid/view/View;

    move-object/from16 v18, v0

    invoke-virtual/range {v18 .. v18}, Landroid/view/View;->getTranslationX()F

    move-result v18

    invoke-static/range {v18 .. v18}, Ljava/lang/Math;->abs(F)F

    move-result v16

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/mail/ui/SwipeHelper;->mCurrAnimView:Landroid/view/View;

    move-object/from16 v18, v0

    move-object/from16 v0, p0

    move-object/from16 v1, v18

    invoke-direct {v0, v1}, Lcom/android/mail/ui/SwipeHelper;->getSize(Landroid/view/View;)F

    move-result v6

    move/from16 v0, v16

    float-to-double v0, v0

    move-wide/from16 v18, v0

    const-wide v20, 0x3fd999999999999aL

    float-to-double v0, v6

    move-wide/from16 v22, v0

    mul-double v20, v20, v22

    cmpl-double v18, v18, v20

    if-lez v18, :cond_9

    const/4 v4, 0x1

    :goto_3
    invoke-static/range {v17 .. v17}, Ljava/lang/Math;->abs(F)F

    move-result v18

    cmpl-float v18, v18, v10

    if-lez v18, :cond_c

    invoke-static/range {v17 .. v17}, Ljava/lang/Math;->abs(F)F

    move-result v18

    invoke-static {v14}, Ljava/lang/Math;->abs(F)F

    move-result v19

    cmpl-float v18, v18, v19

    if-lez v18, :cond_c

    const/16 v18, 0x0

    cmpl-float v18, v17, v18

    if-lez v18, :cond_a

    const/16 v18, 0x1

    :goto_4
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/mail/ui/SwipeHelper;->mCurrAnimView:Landroid/view/View;

    move-object/from16 v19, v0

    invoke-virtual/range {v19 .. v19}, Landroid/view/View;->getTranslationX()F

    move-result v19

    const/16 v20, 0x0

    cmpl-float v19, v19, v20

    if-lez v19, :cond_b

    const/16 v19, 0x1

    :goto_5
    move/from16 v0, v18

    move/from16 v1, v19

    if-ne v0, v1, :cond_c

    move/from16 v0, v16

    float-to-double v0, v0

    move-wide/from16 v18, v0

    const-wide v20, 0x3fa999999999999aL

    float-to-double v0, v6

    move-wide/from16 v22, v0

    mul-double v20, v20, v22

    cmpl-double v18, v18, v20

    if-lez v18, :cond_c

    const/4 v5, 0x1

    :goto_6
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/mail/ui/SwipeHelper;->mCallback:Lcom/android/mail/ui/SwipeHelper$Callback;

    move-object/from16 v18, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/mail/ui/SwipeHelper;->mCurrView:Lcom/android/mail/ui/SwipeableItemView;

    move-object/from16 v19, v0

    invoke-interface/range {v18 .. v19}, Lcom/android/mail/ui/SwipeHelper$Callback;->canChildBeDismissed(Lcom/android/mail/ui/SwipeableItemView;)Z

    move-result v18

    if-eqz v18, :cond_d

    if-nez v5, :cond_8

    if-eqz v4, :cond_d

    :cond_8
    const/4 v9, 0x1

    :goto_7
    if-eqz v9, :cond_f

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/mail/ui/SwipeHelper;->mCurrView:Lcom/android/mail/ui/SwipeableItemView;

    move-object/from16 v18, v0

    if-eqz v5, :cond_e

    :goto_8
    move-object/from16 v0, p0

    move-object/from16 v1, v18

    move/from16 v2, v17

    invoke-direct {v0, v1, v2}, Lcom/android/mail/ui/SwipeHelper;->dismissChild(Lcom/android/mail/ui/SwipeableItemView;F)V

    goto/16 :goto_1

    :cond_9
    const/4 v4, 0x0

    goto :goto_3

    :cond_a
    const/16 v18, 0x0

    goto :goto_4

    :cond_b
    const/16 v19, 0x0

    goto :goto_5

    :cond_c
    const/4 v5, 0x0

    goto :goto_6

    :cond_d
    const/4 v9, 0x0

    goto :goto_7

    :cond_e
    const/16 v17, 0x0

    goto :goto_8

    :cond_f
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/mail/ui/SwipeHelper;->mCurrView:Lcom/android/mail/ui/SwipeableItemView;

    move-object/from16 v18, v0

    move-object/from16 v0, p0

    move-object/from16 v1, v18

    move/from16 v2, v17

    invoke-virtual {v0, v1, v2}, Lcom/android/mail/ui/SwipeHelper;->snapChild(Lcom/android/mail/ui/SwipeableItemView;F)V

    goto/16 :goto_1

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_0
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method public setDensityScale(F)V
    .locals 0
    .param p1    # F

    iput p1, p0, Lcom/android/mail/ui/SwipeHelper;->mDensityScale:F

    return-void
.end method

.method public setPagingTouchSlop(F)V
    .locals 0
    .param p1    # F

    iput p1, p0, Lcom/android/mail/ui/SwipeHelper;->mPagingTouchSlop:F

    return-void
.end method

.method public snapChild(Lcom/android/mail/ui/SwipeableItemView;F)V
    .locals 6
    .param p1    # Lcom/android/mail/ui/SwipeableItemView;
    .param p2    # F

    invoke-interface {p1}, Lcom/android/mail/ui/SwipeableItemView;->getSwipeableView()Landroid/view/View;

    move-result-object v1

    iget-object v4, p0, Lcom/android/mail/ui/SwipeHelper;->mCallback:Lcom/android/mail/ui/SwipeHelper$Callback;

    invoke-interface {v4, p1}, Lcom/android/mail/ui/SwipeHelper$Callback;->canChildBeDismissed(Lcom/android/mail/ui/SwipeableItemView;)Z

    move-result v2

    const/4 v4, 0x0

    invoke-direct {p0, v1, v4}, Lcom/android/mail/ui/SwipeHelper;->createTranslationAnimation(Landroid/view/View;F)Landroid/animation/ObjectAnimator;

    move-result-object v0

    sget v3, Lcom/android/mail/ui/SwipeHelper;->SNAP_ANIM_LEN:I

    int-to-long v4, v3

    invoke-virtual {v0, v4, v5}, Landroid/animation/ObjectAnimator;->setDuration(J)Landroid/animation/ObjectAnimator;

    new-instance v4, Lcom/android/mail/ui/SwipeHelper$5;

    invoke-direct {v4, p0, v2, v1}, Lcom/android/mail/ui/SwipeHelper$5;-><init>(Lcom/android/mail/ui/SwipeHelper;ZLandroid/view/View;)V

    invoke-virtual {v0, v4}, Landroid/animation/ObjectAnimator;->addUpdateListener(Landroid/animation/ValueAnimator$AnimatorUpdateListener;)V

    new-instance v4, Lcom/android/mail/ui/SwipeHelper$6;

    invoke-direct {v4, p0, v1}, Lcom/android/mail/ui/SwipeHelper$6;-><init>(Lcom/android/mail/ui/SwipeHelper;Landroid/view/View;)V

    invoke-virtual {v0, v4}, Landroid/animation/ObjectAnimator;->addListener(Landroid/animation/Animator$AnimatorListener;)V

    invoke-virtual {v0}, Landroid/animation/ObjectAnimator;->start()V

    return-void
.end method
