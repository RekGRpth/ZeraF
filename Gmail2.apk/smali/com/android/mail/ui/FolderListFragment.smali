.class public final Lcom/android/mail/ui/FolderListFragment;
.super Landroid/app/ListFragment;
.source "FolderListFragment.java"

# interfaces
.implements Landroid/app/LoaderManager$LoaderCallbacks;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/mail/ui/FolderListFragment$1;,
        Lcom/android/mail/ui/FolderListFragment$FolderListSelectionListener;,
        Lcom/android/mail/ui/FolderListFragment$HierarchicalFolderListAdapter;,
        Lcom/android/mail/ui/FolderListFragment$FolderListAdapter;,
        Lcom/android/mail/ui/FolderListFragment$FolderListFragmentCursorAdapter;,
        Lcom/android/mail/ui/FolderListFragment$FolderObserver;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/app/ListFragment;",
        "Landroid/app/LoaderManager$LoaderCallbacks",
        "<",
        "Landroid/database/Cursor;",
        ">;"
    }
.end annotation


# static fields
.field private static final LOG_TAG:Ljava/lang/String;


# instance fields
.field private mActivity:Lcom/android/mail/ui/ControllableActivity;

.field private mConversationListCallback:Lcom/android/mail/ui/ConversationListCallbacks;

.field private mCurrentFolderForUnreadCheck:Lcom/android/mail/providers/Folder;

.field private mCursorAdapter:Lcom/android/mail/ui/FolderListFragment$FolderListFragmentCursorAdapter;

.field private mEmptyView:Landroid/view/View;

.field private mExcludedFolderTypes:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private mFolderListUri:Landroid/net/Uri;

.field private mFolderObserver:Lcom/android/mail/ui/FolderListFragment$FolderObserver;

.field private mFutureData:Landroid/database/Cursor;

.field private mIsSectioned:Z

.field private mListView:Landroid/widget/ListView;

.field private mListener:Lcom/android/mail/ui/FolderListFragment$FolderListSelectionListener;

.field private mParentFolder:Lcom/android/mail/providers/Folder;

.field private mSelectedFolderType:I

.field private mSelectedFolderUri:Landroid/net/Uri;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    invoke-static {}, Lcom/android/mail/utils/LogTag;->getLogTag()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/android/mail/ui/FolderListFragment;->LOG_TAG:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Landroid/app/ListFragment;-><init>()V

    sget-object v0, Landroid/net/Uri;->EMPTY:Landroid/net/Uri;

    iput-object v0, p0, Lcom/android/mail/ui/FolderListFragment;->mSelectedFolderUri:Landroid/net/Uri;

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/android/mail/ui/FolderListFragment;->mFolderObserver:Lcom/android/mail/ui/FolderListFragment$FolderObserver;

    const/4 v0, 0x0

    iput v0, p0, Lcom/android/mail/ui/FolderListFragment;->mSelectedFolderType:I

    return-void
.end method

.method static synthetic access$000(Lcom/android/mail/ui/FolderListFragment;)Lcom/android/mail/ui/ControllableActivity;
    .locals 1
    .param p0    # Lcom/android/mail/ui/FolderListFragment;

    iget-object v0, p0, Lcom/android/mail/ui/FolderListFragment;->mActivity:Lcom/android/mail/ui/ControllableActivity;

    return-object v0
.end method

.method static synthetic access$100(Lcom/android/mail/ui/FolderListFragment;Lcom/android/mail/providers/Folder;)V
    .locals 0
    .param p0    # Lcom/android/mail/ui/FolderListFragment;
    .param p1    # Lcom/android/mail/providers/Folder;

    invoke-direct {p0, p1}, Lcom/android/mail/ui/FolderListFragment;->setSelectedFolder(Lcom/android/mail/providers/Folder;)V

    return-void
.end method

.method static synthetic access$1000(Lcom/android/mail/ui/FolderListFragment;)Ljava/util/ArrayList;
    .locals 1
    .param p0    # Lcom/android/mail/ui/FolderListFragment;

    iget-object v0, p0, Lcom/android/mail/ui/FolderListFragment;->mExcludedFolderTypes:Ljava/util/ArrayList;

    return-object v0
.end method

.method static synthetic access$1300(Lcom/android/mail/ui/FolderListFragment;)Landroid/database/Cursor;
    .locals 1
    .param p0    # Lcom/android/mail/ui/FolderListFragment;

    iget-object v0, p0, Lcom/android/mail/ui/FolderListFragment;->mFutureData:Landroid/database/Cursor;

    return-object v0
.end method

.method static synthetic access$1302(Lcom/android/mail/ui/FolderListFragment;Landroid/database/Cursor;)Landroid/database/Cursor;
    .locals 0
    .param p0    # Lcom/android/mail/ui/FolderListFragment;
    .param p1    # Landroid/database/Cursor;

    iput-object p1, p0, Lcom/android/mail/ui/FolderListFragment;->mFutureData:Landroid/database/Cursor;

    return-object p1
.end method

.method static synthetic access$500(Lcom/android/mail/ui/FolderListFragment;)Landroid/widget/ListView;
    .locals 1
    .param p0    # Lcom/android/mail/ui/FolderListFragment;

    iget-object v0, p0, Lcom/android/mail/ui/FolderListFragment;->mListView:Landroid/widget/ListView;

    return-object v0
.end method

.method static synthetic access$600(Lcom/android/mail/ui/FolderListFragment;)I
    .locals 1
    .param p0    # Lcom/android/mail/ui/FolderListFragment;

    iget v0, p0, Lcom/android/mail/ui/FolderListFragment;->mSelectedFolderType:I

    return v0
.end method

.method static synthetic access$700(Lcom/android/mail/ui/FolderListFragment;)Landroid/net/Uri;
    .locals 1
    .param p0    # Lcom/android/mail/ui/FolderListFragment;

    iget-object v0, p0, Lcom/android/mail/ui/FolderListFragment;->mSelectedFolderUri:Landroid/net/Uri;

    return-object v0
.end method

.method static synthetic access$800(Lcom/android/mail/ui/FolderListFragment;)Lcom/android/mail/providers/Folder;
    .locals 1
    .param p0    # Lcom/android/mail/ui/FolderListFragment;

    iget-object v0, p0, Lcom/android/mail/ui/FolderListFragment;->mCurrentFolderForUnreadCheck:Lcom/android/mail/providers/Folder;

    return-object v0
.end method

.method public static newInstance(Lcom/android/mail/providers/Folder;Landroid/net/Uri;Z)Lcom/android/mail/ui/FolderListFragment;
    .locals 1
    .param p0    # Lcom/android/mail/providers/Folder;
    .param p1    # Landroid/net/Uri;
    .param p2    # Z

    const/4 v0, 0x0

    invoke-static {p0, p1, p2, v0}, Lcom/android/mail/ui/FolderListFragment;->newInstance(Lcom/android/mail/providers/Folder;Landroid/net/Uri;ZLjava/util/ArrayList;)Lcom/android/mail/ui/FolderListFragment;

    move-result-object v0

    return-object v0
.end method

.method public static newInstance(Lcom/android/mail/providers/Folder;Landroid/net/Uri;ZLjava/util/ArrayList;)Lcom/android/mail/ui/FolderListFragment;
    .locals 4
    .param p0    # Lcom/android/mail/providers/Folder;
    .param p1    # Landroid/net/Uri;
    .param p2    # Z
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/android/mail/providers/Folder;",
            "Landroid/net/Uri;",
            "Z",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/Integer;",
            ">;)",
            "Lcom/android/mail/ui/FolderListFragment;"
        }
    .end annotation

    new-instance v1, Lcom/android/mail/ui/FolderListFragment;

    invoke-direct {v1}, Lcom/android/mail/ui/FolderListFragment;-><init>()V

    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    if-eqz p0, :cond_0

    const-string v2, "arg-parent-folder"

    invoke-virtual {v0, v2, p0}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    :cond_0
    const-string v2, "arg-folder-list-uri"

    invoke-virtual {p1}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    const-string v2, "arg-is-sectioned"

    invoke-virtual {v0, v2, p2}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    if-eqz p3, :cond_1

    const-string v2, "arg-excluded-folder-types"

    invoke-virtual {v0, v2, p3}, Landroid/os/Bundle;->putIntegerArrayList(Ljava/lang/String;Ljava/util/ArrayList;)V

    :cond_1
    invoke-virtual {v1, v0}, Lcom/android/mail/ui/FolderListFragment;->setArguments(Landroid/os/Bundle;)V

    return-object v1
.end method

.method private setSelectedFolder(Lcom/android/mail/providers/Folder;)V
    .locals 3
    .param p1    # Lcom/android/mail/providers/Folder;

    const/4 v2, 0x0

    if-nez p1, :cond_1

    sget-object v0, Landroid/net/Uri;->EMPTY:Landroid/net/Uri;

    iput-object v0, p0, Lcom/android/mail/ui/FolderListFragment;->mSelectedFolderUri:Landroid/net/Uri;

    sget-object v0, Lcom/android/mail/ui/FolderListFragment;->LOG_TAG:Ljava/lang/String;

    const-string v1, "FolderListFragment.setSelectedFolder(null) called!"

    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {v0, v1, v2}, Lcom/android/mail/utils/LogUtils;->e(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)I

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v0, p1, Lcom/android/mail/providers/Folder;->uri:Landroid/net/Uri;

    iget-object v1, p0, Lcom/android/mail/ui/FolderListFragment;->mSelectedFolderUri:Landroid/net/Uri;

    invoke-virtual {v0, v1}, Landroid/net/Uri;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_2

    iput v2, p0, Lcom/android/mail/ui/FolderListFragment;->mSelectedFolderType:I

    :cond_2
    iput-object p1, p0, Lcom/android/mail/ui/FolderListFragment;->mCurrentFolderForUnreadCheck:Lcom/android/mail/providers/Folder;

    iget-object v0, p1, Lcom/android/mail/providers/Folder;->uri:Landroid/net/Uri;

    iput-object v0, p0, Lcom/android/mail/ui/FolderListFragment;->mSelectedFolderUri:Landroid/net/Uri;

    invoke-direct {p0, p1}, Lcom/android/mail/ui/FolderListFragment;->setSelectedFolderType(Lcom/android/mail/providers/Folder;)V

    iget-object v0, p0, Lcom/android/mail/ui/FolderListFragment;->mCursorAdapter:Lcom/android/mail/ui/FolderListFragment$FolderListFragmentCursorAdapter;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/mail/ui/FolderListFragment;->mCursorAdapter:Lcom/android/mail/ui/FolderListFragment$FolderListFragmentCursorAdapter;

    invoke-interface {v0}, Lcom/android/mail/ui/FolderListFragment$FolderListFragmentCursorAdapter;->notifyDataSetChanged()V

    goto :goto_0
.end method

.method private setSelectedFolderType(Lcom/android/mail/providers/Folder;)V
    .locals 1
    .param p1    # Lcom/android/mail/providers/Folder;

    iget v0, p0, Lcom/android/mail/ui/FolderListFragment;->mSelectedFolderType:I

    if-eqz v0, :cond_0

    :goto_0
    return-void

    :cond_0
    invoke-virtual {p1}, Lcom/android/mail/providers/Folder;->isProviderFolder()Z

    move-result v0

    if-eqz v0, :cond_1

    const/4 v0, 0x1

    :goto_1
    iput v0, p0, Lcom/android/mail/ui/FolderListFragment;->mSelectedFolderType:I

    goto :goto_0

    :cond_1
    const/4 v0, 0x3

    goto :goto_1
.end method

.method private updateCursorAdapter(Landroid/database/Cursor;)V
    .locals 2
    .param p1    # Landroid/database/Cursor;

    iget-object v0, p0, Lcom/android/mail/ui/FolderListFragment;->mCursorAdapter:Lcom/android/mail/ui/FolderListFragment$FolderListFragmentCursorAdapter;

    invoke-interface {v0, p1}, Lcom/android/mail/ui/FolderListFragment$FolderListFragmentCursorAdapter;->setCursor(Landroid/database/Cursor;)V

    if-eqz p1, :cond_0

    invoke-interface {p1}, Landroid/database/Cursor;->getCount()I

    move-result v0

    if-nez v0, :cond_1

    :cond_0
    iget-object v0, p0, Lcom/android/mail/ui/FolderListFragment;->mEmptyView:Landroid/view/View;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    iget-object v0, p0, Lcom/android/mail/ui/FolderListFragment;->mListView:Landroid/widget/ListView;

    iget-object v1, p0, Lcom/android/mail/ui/FolderListFragment;->mEmptyView:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setEmptyView(Landroid/view/View;)V

    :cond_1
    return-void
.end method

.method private viewFolder(I)V
    .locals 8
    .param p1    # I

    invoke-virtual {p0}, Lcom/android/mail/ui/FolderListFragment;->getListAdapter()Landroid/widget/ListAdapter;

    move-result-object v3

    invoke-interface {v3, p1}, Landroid/widget/ListAdapter;->getItem(I)Ljava/lang/Object;

    move-result-object v2

    instance-of v3, v2, Lcom/android/mail/ui/FolderListFragment$FolderListAdapter$Item;

    if-eqz v3, :cond_0

    move-object v1, v2

    check-cast v1, Lcom/android/mail/ui/FolderListFragment$FolderListAdapter$Item;

    iget-object v3, p0, Lcom/android/mail/ui/FolderListFragment;->mCursorAdapter:Lcom/android/mail/ui/FolderListFragment$FolderListFragmentCursorAdapter;

    invoke-interface {v3, v1}, Lcom/android/mail/ui/FolderListFragment$FolderListFragmentCursorAdapter;->getFullFolder(Lcom/android/mail/ui/FolderListFragment$FolderListAdapter$Item;)Lcom/android/mail/providers/Folder;

    move-result-object v0

    iget v3, v1, Lcom/android/mail/ui/FolderListFragment$FolderListAdapter$Item;->mFolderType:I

    iput v3, p0, Lcom/android/mail/ui/FolderListFragment;->mSelectedFolderType:I

    :goto_0
    if-eqz v0, :cond_3

    iget-object v3, p0, Lcom/android/mail/ui/FolderListFragment;->mParentFolder:Lcom/android/mail/providers/Folder;

    invoke-virtual {v0, v3}, Lcom/android/mail/providers/Folder;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_2

    const/4 v3, 0x0

    :goto_1
    iput-object v3, v0, Lcom/android/mail/providers/Folder;->parent:Lcom/android/mail/providers/Folder;

    iget-object v3, p0, Lcom/android/mail/ui/FolderListFragment;->mListener:Lcom/android/mail/ui/FolderListFragment$FolderListSelectionListener;

    invoke-interface {v3, v0}, Lcom/android/mail/ui/FolderListFragment$FolderListSelectionListener;->onFolderSelected(Lcom/android/mail/providers/Folder;)V

    :goto_2
    return-void

    :cond_0
    instance-of v3, v2, Lcom/android/mail/providers/Folder;

    if-eqz v3, :cond_1

    move-object v0, v2

    check-cast v0, Lcom/android/mail/providers/Folder;

    goto :goto_0

    :cond_1
    new-instance v0, Lcom/android/mail/providers/Folder;

    check-cast v2, Landroid/database/Cursor;

    invoke-direct {v0, v2}, Lcom/android/mail/providers/Folder;-><init>(Landroid/database/Cursor;)V

    goto :goto_0

    :cond_2
    iget-object v3, p0, Lcom/android/mail/ui/FolderListFragment;->mParentFolder:Lcom/android/mail/providers/Folder;

    goto :goto_1

    :cond_3
    sget-object v3, Lcom/android/mail/ui/FolderListFragment;->LOG_TAG:Ljava/lang/String;

    const-string v4, "FolderListFragment unable to get a full fledged folder to hand to the listener for position %d"

    const/4 v5, 0x1

    new-array v5, v5, [Ljava/lang/Object;

    const/4 v6, 0x0

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    aput-object v7, v5, v6

    invoke-static {v3, v4, v5}, Lcom/android/mail/utils/LogUtils;->d(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)I

    goto :goto_2
.end method


# virtual methods
.method public onActivityCreated(Landroid/os/Bundle;)V
    .locals 7
    .param p1    # Landroid/os/Bundle;

    const/4 v6, 0x0

    const/4 v2, 0x0

    invoke-super {p0, p1}, Landroid/app/ListFragment;->onActivityCreated(Landroid/os/Bundle;)V

    invoke-virtual {p0}, Lcom/android/mail/ui/FolderListFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    instance-of v3, v0, Lcom/android/mail/ui/ControllableActivity;

    if-nez v3, :cond_0

    sget-object v3, Lcom/android/mail/ui/FolderListFragment;->LOG_TAG:Ljava/lang/String;

    const-string v4, "FolderListFragment expects only a ControllableActivity tocreate it. Cannot proceed."

    new-array v5, v6, [Ljava/lang/Object;

    invoke-static {v3, v4, v5}, Lcom/android/mail/utils/LogUtils;->wtf(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)I

    :cond_0
    check-cast v0, Lcom/android/mail/ui/ControllableActivity;

    iput-object v0, p0, Lcom/android/mail/ui/FolderListFragment;->mActivity:Lcom/android/mail/ui/ControllableActivity;

    iget-object v3, p0, Lcom/android/mail/ui/FolderListFragment;->mActivity:Lcom/android/mail/ui/ControllableActivity;

    invoke-interface {v3}, Lcom/android/mail/ui/ControllableActivity;->getListHandler()Lcom/android/mail/ui/ConversationListCallbacks;

    move-result-object v3

    iput-object v3, p0, Lcom/android/mail/ui/FolderListFragment;->mConversationListCallback:Lcom/android/mail/ui/ConversationListCallbacks;

    iget-object v3, p0, Lcom/android/mail/ui/FolderListFragment;->mActivity:Lcom/android/mail/ui/ControllableActivity;

    invoke-interface {v3}, Lcom/android/mail/ui/ControllableActivity;->getFolderController()Lcom/android/mail/ui/FolderController;

    move-result-object v1

    new-instance v3, Lcom/android/mail/ui/FolderListFragment$FolderObserver;

    invoke-direct {v3, p0, v2}, Lcom/android/mail/ui/FolderListFragment$FolderObserver;-><init>(Lcom/android/mail/ui/FolderListFragment;Lcom/android/mail/ui/FolderListFragment$1;)V

    iput-object v3, p0, Lcom/android/mail/ui/FolderListFragment;->mFolderObserver:Lcom/android/mail/ui/FolderListFragment$FolderObserver;

    if-eqz v1, :cond_1

    iget-object v3, p0, Lcom/android/mail/ui/FolderListFragment;->mFolderObserver:Lcom/android/mail/ui/FolderListFragment$FolderObserver;

    invoke-interface {v1, v3}, Lcom/android/mail/ui/FolderController;->registerFolderObserver(Landroid/database/DataSetObserver;)V

    invoke-interface {v1}, Lcom/android/mail/ui/FolderController;->getFolder()Lcom/android/mail/providers/Folder;

    move-result-object v3

    iput-object v3, p0, Lcom/android/mail/ui/FolderListFragment;->mCurrentFolderForUnreadCheck:Lcom/android/mail/providers/Folder;

    :cond_1
    iget-object v3, p0, Lcom/android/mail/ui/FolderListFragment;->mActivity:Lcom/android/mail/ui/ControllableActivity;

    invoke-interface {v3}, Lcom/android/mail/ui/ControllableActivity;->getFolderListSelectionListener()Lcom/android/mail/ui/FolderListFragment$FolderListSelectionListener;

    move-result-object v3

    iput-object v3, p0, Lcom/android/mail/ui/FolderListFragment;->mListener:Lcom/android/mail/ui/FolderListFragment$FolderListSelectionListener;

    iget-object v3, p0, Lcom/android/mail/ui/FolderListFragment;->mActivity:Lcom/android/mail/ui/ControllableActivity;

    invoke-interface {v3}, Lcom/android/mail/ui/ControllableActivity;->isFinishing()Z

    move-result v3

    if-eqz v3, :cond_2

    :goto_0
    return-void

    :cond_2
    iget-object v3, p0, Lcom/android/mail/ui/FolderListFragment;->mParentFolder:Lcom/android/mail/providers/Folder;

    if-eqz v3, :cond_4

    new-instance v3, Lcom/android/mail/ui/FolderListFragment$HierarchicalFolderListAdapter;

    iget-object v4, p0, Lcom/android/mail/ui/FolderListFragment;->mParentFolder:Lcom/android/mail/providers/Folder;

    invoke-direct {v3, p0, v2, v4}, Lcom/android/mail/ui/FolderListFragment$HierarchicalFolderListAdapter;-><init>(Lcom/android/mail/ui/FolderListFragment;Landroid/database/Cursor;Lcom/android/mail/providers/Folder;)V

    iput-object v3, p0, Lcom/android/mail/ui/FolderListFragment;->mCursorAdapter:Lcom/android/mail/ui/FolderListFragment$FolderListFragmentCursorAdapter;

    iget-object v3, p0, Lcom/android/mail/ui/FolderListFragment;->mActivity:Lcom/android/mail/ui/ControllableActivity;

    invoke-interface {v3}, Lcom/android/mail/ui/ControllableActivity;->getHierarchyFolder()Lcom/android/mail/providers/Folder;

    move-result-object v2

    :goto_1
    if-eqz v2, :cond_3

    iget-object v3, v2, Lcom/android/mail/providers/Folder;->uri:Landroid/net/Uri;

    iget-object v4, p0, Lcom/android/mail/ui/FolderListFragment;->mSelectedFolderUri:Landroid/net/Uri;

    invoke-virtual {v3, v4}, Landroid/net/Uri;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_3

    invoke-direct {p0, v2}, Lcom/android/mail/ui/FolderListFragment;->setSelectedFolder(Lcom/android/mail/providers/Folder;)V

    :cond_3
    iget-object v3, p0, Lcom/android/mail/ui/FolderListFragment;->mCursorAdapter:Lcom/android/mail/ui/FolderListFragment$FolderListFragmentCursorAdapter;

    invoke-virtual {p0, v3}, Lcom/android/mail/ui/FolderListFragment;->setListAdapter(Landroid/widget/ListAdapter;)V

    invoke-virtual {p0}, Lcom/android/mail/ui/FolderListFragment;->getLoaderManager()Landroid/app/LoaderManager;

    move-result-object v3

    sget-object v4, Landroid/os/Bundle;->EMPTY:Landroid/os/Bundle;

    invoke-virtual {v3, v6, v4, p0}, Landroid/app/LoaderManager;->initLoader(ILandroid/os/Bundle;Landroid/app/LoaderManager$LoaderCallbacks;)Landroid/content/Loader;

    goto :goto_0

    :cond_4
    new-instance v3, Lcom/android/mail/ui/FolderListFragment$FolderListAdapter;

    const v4, 0x7f040039

    iget-boolean v5, p0, Lcom/android/mail/ui/FolderListFragment;->mIsSectioned:Z

    invoke-direct {v3, p0, v4, v5}, Lcom/android/mail/ui/FolderListFragment$FolderListAdapter;-><init>(Lcom/android/mail/ui/FolderListFragment;IZ)V

    iput-object v3, p0, Lcom/android/mail/ui/FolderListFragment;->mCursorAdapter:Lcom/android/mail/ui/FolderListFragment$FolderListFragmentCursorAdapter;

    if-nez v1, :cond_5

    :goto_2
    goto :goto_1

    :cond_5
    invoke-interface {v1}, Lcom/android/mail/ui/FolderController;->getFolder()Lcom/android/mail/providers/Folder;

    move-result-object v2

    goto :goto_2
.end method

.method public onAnimationEnd()V
    .locals 1

    iget-object v0, p0, Lcom/android/mail/ui/FolderListFragment;->mFutureData:Landroid/database/Cursor;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/mail/ui/FolderListFragment;->mFutureData:Landroid/database/Cursor;

    invoke-direct {p0, v0}, Lcom/android/mail/ui/FolderListFragment;->updateCursorAdapter(Landroid/database/Cursor;)V

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/android/mail/ui/FolderListFragment;->mFutureData:Landroid/database/Cursor;

    :cond_0
    return-void
.end method

.method public onCreateLoader(ILandroid/os/Bundle;)Landroid/content/Loader;
    .locals 7
    .param p1    # I
    .param p2    # Landroid/os/Bundle;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Landroid/os/Bundle;",
            ")",
            "Landroid/content/Loader",
            "<",
            "Landroid/database/Cursor;",
            ">;"
        }
    .end annotation

    const/4 v4, 0x0

    iget-object v0, p0, Lcom/android/mail/ui/FolderListFragment;->mListView:Landroid/widget/ListView;

    invoke-virtual {v0, v4}, Landroid/widget/ListView;->setEmptyView(Landroid/view/View;)V

    iget-object v0, p0, Lcom/android/mail/ui/FolderListFragment;->mEmptyView:Landroid/view/View;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    new-instance v0, Landroid/content/CursorLoader;

    iget-object v1, p0, Lcom/android/mail/ui/FolderListFragment;->mActivity:Lcom/android/mail/ui/ControllableActivity;

    invoke-interface {v1}, Lcom/android/mail/ui/ControllableActivity;->getActivityContext()Landroid/content/Context;

    move-result-object v1

    iget-object v2, p0, Lcom/android/mail/ui/FolderListFragment;->mFolderListUri:Landroid/net/Uri;

    sget-object v3, Lcom/android/mail/providers/UIProvider;->FOLDERS_PROJECTION:[Ljava/lang/String;

    move-object v5, v4

    move-object v6, v4

    invoke-direct/range {v0 .. v6}, Landroid/content/CursorLoader;-><init>(Landroid/content/Context;Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)V

    return-object v0
.end method

.method public onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 5
    .param p1    # Landroid/view/LayoutInflater;
    .param p2    # Landroid/view/ViewGroup;
    .param p3    # Landroid/os/Bundle;

    const/4 v4, 0x0

    invoke-virtual {p0}, Lcom/android/mail/ui/FolderListFragment;->getArguments()Landroid/os/Bundle;

    move-result-object v0

    const-string v2, "arg-folder-list-uri"

    invoke-virtual {v0, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v2

    iput-object v2, p0, Lcom/android/mail/ui/FolderListFragment;->mFolderListUri:Landroid/net/Uri;

    const-string v2, "arg-parent-folder"

    invoke-virtual {v0, v2}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v2

    check-cast v2, Lcom/android/mail/providers/Folder;

    iput-object v2, p0, Lcom/android/mail/ui/FolderListFragment;->mParentFolder:Lcom/android/mail/providers/Folder;

    const-string v2, "arg-is-sectioned"

    invoke-virtual {v0, v2}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v2

    iput-boolean v2, p0, Lcom/android/mail/ui/FolderListFragment;->mIsSectioned:Z

    const-string v2, "arg-excluded-folder-types"

    invoke-virtual {v0, v2}, Landroid/os/Bundle;->getIntegerArrayList(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v2

    iput-object v2, p0, Lcom/android/mail/ui/FolderListFragment;->mExcludedFolderTypes:Ljava/util/ArrayList;

    const v2, 0x7f04003a

    invoke-virtual {p1, v2, v4}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v1

    const v2, 0x102000a

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/ListView;

    iput-object v2, p0, Lcom/android/mail/ui/FolderListFragment;->mListView:Landroid/widget/ListView;

    iget-object v2, p0, Lcom/android/mail/ui/FolderListFragment;->mListView:Landroid/widget/ListView;

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Landroid/widget/ListView;->setHeaderDividersEnabled(Z)V

    iget-object v2, p0, Lcom/android/mail/ui/FolderListFragment;->mListView:Landroid/widget/ListView;

    const/4 v3, 0x1

    invoke-virtual {v2, v3}, Landroid/widget/ListView;->setChoiceMode(I)V

    iget-object v2, p0, Lcom/android/mail/ui/FolderListFragment;->mListView:Landroid/widget/ListView;

    invoke-virtual {v2, v4}, Landroid/widget/ListView;->setEmptyView(Landroid/view/View;)V

    if-eqz p3, :cond_0

    const-string v2, "flf-list-state"

    invoke-virtual {p3, v2}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/android/mail/ui/FolderListFragment;->mListView:Landroid/widget/ListView;

    const-string v3, "flf-list-state"

    invoke-virtual {p3, v3}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/widget/ListView;->onRestoreInstanceState(Landroid/os/Parcelable;)V

    :cond_0
    const v2, 0x7f08005a

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    iput-object v2, p0, Lcom/android/mail/ui/FolderListFragment;->mEmptyView:Landroid/view/View;

    if-eqz p3, :cond_2

    const-string v2, "flf-selected-folder"

    invoke-virtual {p3, v2}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_2

    const-string v2, "flf-selected-folder"

    invoke-virtual {p3, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v2

    iput-object v2, p0, Lcom/android/mail/ui/FolderListFragment;->mSelectedFolderUri:Landroid/net/Uri;

    const-string v2, "flf-selected-type"

    invoke-virtual {p3, v2}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v2

    iput v2, p0, Lcom/android/mail/ui/FolderListFragment;->mSelectedFolderType:I

    :cond_1
    :goto_0
    return-object v1

    :cond_2
    iget-object v2, p0, Lcom/android/mail/ui/FolderListFragment;->mParentFolder:Lcom/android/mail/providers/Folder;

    if-eqz v2, :cond_1

    iget-object v2, p0, Lcom/android/mail/ui/FolderListFragment;->mParentFolder:Lcom/android/mail/providers/Folder;

    iget-object v2, v2, Lcom/android/mail/providers/Folder;->uri:Landroid/net/Uri;

    iput-object v2, p0, Lcom/android/mail/ui/FolderListFragment;->mSelectedFolderUri:Landroid/net/Uri;

    goto :goto_0
.end method

.method public onDestroyView()V
    .locals 3

    const/4 v2, 0x0

    iget-object v1, p0, Lcom/android/mail/ui/FolderListFragment;->mCursorAdapter:Lcom/android/mail/ui/FolderListFragment$FolderListFragmentCursorAdapter;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/android/mail/ui/FolderListFragment;->mCursorAdapter:Lcom/android/mail/ui/FolderListFragment$FolderListFragmentCursorAdapter;

    invoke-interface {v1}, Lcom/android/mail/ui/FolderListFragment$FolderListFragmentCursorAdapter;->destroy()V

    :cond_0
    invoke-virtual {p0, v2}, Lcom/android/mail/ui/FolderListFragment;->setListAdapter(Landroid/widget/ListAdapter;)V

    iget-object v1, p0, Lcom/android/mail/ui/FolderListFragment;->mFolderObserver:Lcom/android/mail/ui/FolderListFragment$FolderObserver;

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/android/mail/ui/FolderListFragment;->mActivity:Lcom/android/mail/ui/ControllableActivity;

    invoke-interface {v1}, Lcom/android/mail/ui/ControllableActivity;->getFolderController()Lcom/android/mail/ui/FolderController;

    move-result-object v0

    if-eqz v0, :cond_1

    iget-object v1, p0, Lcom/android/mail/ui/FolderListFragment;->mFolderObserver:Lcom/android/mail/ui/FolderListFragment$FolderObserver;

    invoke-interface {v0, v1}, Lcom/android/mail/ui/FolderController;->unregisterFolderObserver(Landroid/database/DataSetObserver;)V

    iput-object v2, p0, Lcom/android/mail/ui/FolderListFragment;->mFolderObserver:Lcom/android/mail/ui/FolderListFragment$FolderObserver;

    :cond_1
    invoke-super {p0}, Landroid/app/ListFragment;->onDestroyView()V

    return-void
.end method

.method public onListItemClick(Landroid/widget/ListView;Landroid/view/View;IJ)V
    .locals 0
    .param p1    # Landroid/widget/ListView;
    .param p2    # Landroid/view/View;
    .param p3    # I
    .param p4    # J

    invoke-direct {p0, p3}, Lcom/android/mail/ui/FolderListFragment;->viewFolder(I)V

    return-void
.end method

.method public onLoadFinished(Landroid/content/Loader;Landroid/database/Cursor;)V
    .locals 2
    .param p2    # Landroid/database/Cursor;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Loader",
            "<",
            "Landroid/database/Cursor;",
            ">;",
            "Landroid/database/Cursor;",
            ")V"
        }
    .end annotation

    iget-object v0, p0, Lcom/android/mail/ui/FolderListFragment;->mConversationListCallback:Lcom/android/mail/ui/ConversationListCallbacks;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/mail/ui/FolderListFragment;->mConversationListCallback:Lcom/android/mail/ui/ConversationListCallbacks;

    invoke-interface {v0}, Lcom/android/mail/ui/ConversationListCallbacks;->isAnimating()Z

    move-result v0

    if-nez v0, :cond_1

    :cond_0
    invoke-direct {p0, p2}, Lcom/android/mail/ui/FolderListFragment;->updateCursorAdapter(Landroid/database/Cursor;)V

    :goto_0
    return-void

    :cond_1
    iput-object p2, p0, Lcom/android/mail/ui/FolderListFragment;->mFutureData:Landroid/database/Cursor;

    iget-object v0, p0, Lcom/android/mail/ui/FolderListFragment;->mCursorAdapter:Lcom/android/mail/ui/FolderListFragment$FolderListFragmentCursorAdapter;

    const/4 v1, 0x0

    invoke-interface {v0, v1}, Lcom/android/mail/ui/FolderListFragment$FolderListFragmentCursorAdapter;->setCursor(Landroid/database/Cursor;)V

    goto :goto_0
.end method

.method public bridge synthetic onLoadFinished(Landroid/content/Loader;Ljava/lang/Object;)V
    .locals 0
    .param p1    # Landroid/content/Loader;
    .param p2    # Ljava/lang/Object;

    check-cast p2, Landroid/database/Cursor;

    invoke-virtual {p0, p1, p2}, Lcom/android/mail/ui/FolderListFragment;->onLoadFinished(Landroid/content/Loader;Landroid/database/Cursor;)V

    return-void
.end method

.method public onLoaderReset(Landroid/content/Loader;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Loader",
            "<",
            "Landroid/database/Cursor;",
            ">;)V"
        }
    .end annotation

    iget-object v0, p0, Lcom/android/mail/ui/FolderListFragment;->mCursorAdapter:Lcom/android/mail/ui/FolderListFragment$FolderListFragmentCursorAdapter;

    const/4 v1, 0x0

    invoke-interface {v0, v1}, Lcom/android/mail/ui/FolderListFragment$FolderListFragmentCursorAdapter;->setCursor(Landroid/database/Cursor;)V

    return-void
.end method

.method public onPause()V
    .locals 0

    invoke-super {p0}, Landroid/app/ListFragment;->onPause()V

    return-void
.end method

.method public onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 2
    .param p1    # Landroid/os/Bundle;

    invoke-super {p0, p1}, Landroid/app/ListFragment;->onSaveInstanceState(Landroid/os/Bundle;)V

    iget-object v0, p0, Lcom/android/mail/ui/FolderListFragment;->mListView:Landroid/widget/ListView;

    if-eqz v0, :cond_0

    const-string v0, "flf-list-state"

    iget-object v1, p0, Lcom/android/mail/ui/FolderListFragment;->mListView:Landroid/widget/ListView;

    invoke-virtual {v1}, Landroid/widget/ListView;->onSaveInstanceState()Landroid/os/Parcelable;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    :cond_0
    iget-object v0, p0, Lcom/android/mail/ui/FolderListFragment;->mSelectedFolderUri:Landroid/net/Uri;

    if-eqz v0, :cond_1

    const-string v0, "flf-selected-folder"

    iget-object v1, p0, Lcom/android/mail/ui/FolderListFragment;->mSelectedFolderUri:Landroid/net/Uri;

    invoke-virtual {v1}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    :cond_1
    const-string v0, "flf-selected-type"

    iget v1, p0, Lcom/android/mail/ui/FolderListFragment;->mSelectedFolderType:I

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    return-void
.end method

.method public onStart()V
    .locals 0

    invoke-super {p0}, Landroid/app/ListFragment;->onStart()V

    return-void
.end method

.method public onStop()V
    .locals 0

    invoke-super {p0}, Landroid/app/ListFragment;->onStop()V

    return-void
.end method

.method public showingHierarchy()Z
    .locals 1

    iget-object v0, p0, Lcom/android/mail/ui/FolderListFragment;->mParentFolder:Lcom/android/mail/providers/Folder;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
