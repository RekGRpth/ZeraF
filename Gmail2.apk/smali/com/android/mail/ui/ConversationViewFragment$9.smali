.class Lcom/android/mail/ui/ConversationViewFragment$9;
.super Lcom/android/mail/utils/HardwareLayerEnabler;
.source "ConversationViewFragment.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/android/mail/ui/ConversationViewFragment;->dismissWhooshMode()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/mail/ui/ConversationViewFragment;


# direct methods
.method constructor <init>(Lcom/android/mail/ui/ConversationViewFragment;Landroid/view/View;)V
    .locals 0
    .param p2    # Landroid/view/View;

    iput-object p1, p0, Lcom/android/mail/ui/ConversationViewFragment$9;->this$0:Lcom/android/mail/ui/ConversationViewFragment;

    invoke-direct {p0, p2}, Lcom/android/mail/utils/HardwareLayerEnabler;-><init>(Landroid/view/View;)V

    return-void
.end method


# virtual methods
.method public onAnimationEnd(Landroid/animation/Animator;)V
    .locals 3
    .param p1    # Landroid/animation/Animator;

    const/high16 v2, 0x3f800000

    invoke-super {p0, p1}, Lcom/android/mail/utils/HardwareLayerEnabler;->onAnimationEnd(Landroid/animation/Animator;)V

    iget-object v0, p0, Lcom/android/mail/ui/ConversationViewFragment$9;->this$0:Lcom/android/mail/ui/ConversationViewFragment;

    # getter for: Lcom/android/mail/ui/ConversationViewFragment;->mMessageGroup:Landroid/view/View;
    invoke-static {v0}, Lcom/android/mail/ui/ConversationViewFragment;->access$1400(Lcom/android/mail/ui/ConversationViewFragment;)Landroid/view/View;

    move-result-object v0

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    iget-object v0, p0, Lcom/android/mail/ui/ConversationViewFragment$9;->this$0:Lcom/android/mail/ui/ConversationViewFragment;

    # getter for: Lcom/android/mail/ui/ConversationViewFragment;->mMessageView:Lcom/android/mail/browse/MessageView;
    invoke-static {v0}, Lcom/android/mail/ui/ConversationViewFragment;->access$1200(Lcom/android/mail/ui/ConversationViewFragment;)Lcom/android/mail/browse/MessageView;

    move-result-object v0

    invoke-virtual {v0}, Lcom/android/mail/browse/MessageView;->clearView()V

    iget-object v0, p0, Lcom/android/mail/ui/ConversationViewFragment$9;->this$0:Lcom/android/mail/ui/ConversationViewFragment;

    # getter for: Lcom/android/mail/ui/ConversationViewFragment;->mMessageGroup:Landroid/view/View;
    invoke-static {v0}, Lcom/android/mail/ui/ConversationViewFragment;->access$1400(Lcom/android/mail/ui/ConversationViewFragment;)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/view/View;->setAlpha(F)V

    iget-object v0, p0, Lcom/android/mail/ui/ConversationViewFragment$9;->this$0:Lcom/android/mail/ui/ConversationViewFragment;

    # getter for: Lcom/android/mail/ui/ConversationViewFragment;->mMessageGroup:Landroid/view/View;
    invoke-static {v0}, Lcom/android/mail/ui/ConversationViewFragment;->access$1400(Lcom/android/mail/ui/ConversationViewFragment;)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/view/View;->setScaleX(F)V

    iget-object v0, p0, Lcom/android/mail/ui/ConversationViewFragment$9;->this$0:Lcom/android/mail/ui/ConversationViewFragment;

    # getter for: Lcom/android/mail/ui/ConversationViewFragment;->mMessageGroup:Landroid/view/View;
    invoke-static {v0}, Lcom/android/mail/ui/ConversationViewFragment;->access$1400(Lcom/android/mail/ui/ConversationViewFragment;)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/view/View;->setScaleY(F)V

    return-void
.end method
