.class public Lcom/android/mail/ui/AttachmentTileGrid;
.super Landroid/widget/FrameLayout;
.source "AttachmentTileGrid.java"

# interfaces
.implements Lcom/android/mail/ui/AttachmentTile$AttachmentPreviewCache;


# instance fields
.field private final mAttachmentPreviews:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Lcom/android/mail/ui/AttachmentTile$AttachmentPreview;",
            ">;"
        }
    .end annotation
.end field

.field private mAttachments:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/android/mail/providers/Attachment;",
            ">;"
        }
    .end annotation
.end field

.field private mAttachmentsListUri:Landroid/net/Uri;

.field private mColumnCount:I

.field private mFragmentManager:Landroid/app/FragmentManager;

.field private final mInflater:Landroid/view/LayoutInflater;

.field private final mTileMinSize:I


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 2
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/util/AttributeSet;

    invoke-direct {p0, p1, p2}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    invoke-static {p1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    iput-object v0, p0, Lcom/android/mail/ui/AttachmentTileGrid;->mInflater:Landroid/view/LayoutInflater;

    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0c0064

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    iput v0, p0, Lcom/android/mail/ui/AttachmentTileGrid;->mTileMinSize:I

    invoke-static {}, Lcom/google/common/collect/Maps;->newHashMap()Ljava/util/HashMap;

    move-result-object v0

    iput-object v0, p0, Lcom/android/mail/ui/AttachmentTileGrid;->mAttachmentPreviews:Ljava/util/HashMap;

    return-void
.end method

.method private addMessageTileFromAttachment(Lcom/android/mail/providers/Attachment;IZ)V
    .locals 6
    .param p1    # Lcom/android/mail/providers/Attachment;
    .param p2    # I
    .param p3    # Z

    invoke-virtual {p0}, Lcom/android/mail/ui/AttachmentTileGrid;->getChildCount()I

    move-result v1

    if-gt v1, p2, :cond_0

    iget-object v1, p0, Lcom/android/mail/ui/AttachmentTileGrid;->mInflater:Landroid/view/LayoutInflater;

    invoke-static {v1, p0}, Lcom/android/mail/browse/MessageAttachmentTile;->inflate(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;)Lcom/android/mail/browse/MessageAttachmentTile;

    move-result-object v0

    iget-object v1, p0, Lcom/android/mail/ui/AttachmentTileGrid;->mFragmentManager:Landroid/app/FragmentManager;

    invoke-virtual {v0, v1}, Lcom/android/mail/browse/MessageAttachmentTile;->initialize(Landroid/app/FragmentManager;)V

    invoke-virtual {p0, v0}, Lcom/android/mail/ui/AttachmentTileGrid;->addView(Landroid/view/View;)V

    :goto_0
    iget-object v2, p0, Lcom/android/mail/ui/AttachmentTileGrid;->mAttachmentsListUri:Landroid/net/Uri;

    move-object v1, p1

    move v3, p2

    move-object v4, p0

    move v5, p3

    invoke-virtual/range {v0 .. v5}, Lcom/android/mail/browse/MessageAttachmentTile;->render(Lcom/android/mail/providers/Attachment;Landroid/net/Uri;ILcom/android/mail/ui/AttachmentTile$AttachmentPreviewCache;Z)V

    return-void

    :cond_0
    invoke-virtual {p0, p2}, Lcom/android/mail/ui/AttachmentTileGrid;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/android/mail/browse/MessageAttachmentTile;

    goto :goto_0
.end method

.method private onLayoutForTiles()V
    .locals 10

    invoke-virtual {p0}, Lcom/android/mail/ui/AttachmentTileGrid;->getChildCount()I

    move-result v5

    const/4 v2, 0x0

    const/4 v3, 0x0

    const/4 v7, 0x1

    const/4 v6, 0x0

    :goto_0
    if-ge v6, v5, :cond_1

    invoke-virtual {p0, v6}, Lcom/android/mail/ui/AttachmentTileGrid;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/View;->getMeasuredWidth()I

    move-result v4

    invoke-virtual {v0}, Landroid/view/View;->getMeasuredHeight()I

    move-result v1

    if-nez v7, :cond_0

    iget v8, p0, Lcom/android/mail/ui/AttachmentTileGrid;->mColumnCount:I

    rem-int v8, v6, v8

    if-nez v8, :cond_0

    const/4 v2, 0x0

    add-int/2addr v3, v1

    :goto_1
    add-int v8, v2, v4

    add-int v9, v3, v1

    invoke-virtual {v0, v2, v3, v8, v9}, Landroid/view/View;->layout(IIII)V

    add-int/2addr v2, v4

    add-int/lit8 v6, v6, 0x1

    goto :goto_0

    :cond_0
    const/4 v7, 0x0

    goto :goto_1

    :cond_1
    return-void
.end method

.method private onMeasureForTiles(I)V
    .locals 13
    .param p1    # I

    const/high16 v12, 0x40000000

    const/4 v9, 0x1

    const/4 v10, 0x0

    invoke-static {p1}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v7

    invoke-virtual {p0}, Lcom/android/mail/ui/AttachmentTileGrid;->getChildCount()I

    move-result v1

    if-nez v1, :cond_0

    invoke-virtual {p0, v7, v10}, Lcom/android/mail/ui/AttachmentTileGrid;->setMeasuredDimension(II)V

    :goto_0
    return-void

    :cond_0
    iget v8, p0, Lcom/android/mail/ui/AttachmentTileGrid;->mTileMinSize:I

    div-int v8, v7, v8

    iput v8, p0, Lcom/android/mail/ui/AttachmentTileGrid;->mColumnCount:I

    iget v8, p0, Lcom/android/mail/ui/AttachmentTileGrid;->mColumnCount:I

    if-nez v8, :cond_1

    iput v9, p0, Lcom/android/mail/ui/AttachmentTileGrid;->mColumnCount:I

    :cond_1
    iget v8, p0, Lcom/android/mail/ui/AttachmentTileGrid;->mColumnCount:I

    div-int v4, v7, v8

    iget v8, p0, Lcom/android/mail/ui/AttachmentTileGrid;->mColumnCount:I

    mul-int/2addr v8, v4

    sub-int v6, v7, v8

    const/4 v3, 0x0

    :goto_1
    if-ge v3, v1, :cond_3

    invoke-virtual {p0, v3}, Lcom/android/mail/ui/AttachmentTileGrid;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    if-ge v3, v6, :cond_2

    move v8, v9

    :goto_2
    add-int v2, v4, v8

    invoke-static {v2, v12}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v8

    invoke-static {v4, v12}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v11

    invoke-virtual {v0, v8, v11}, Landroid/view/View;->measure(II)V

    add-int/lit8 v3, v3, 0x1

    goto :goto_1

    :cond_2
    move v8, v10

    goto :goto_2

    :cond_3
    add-int/lit8 v8, v1, -0x1

    iget v9, p0, Lcom/android/mail/ui/AttachmentTileGrid;->mColumnCount:I

    div-int/2addr v8, v9

    add-int/lit8 v5, v8, 0x1

    invoke-virtual {p0, v10}, Lcom/android/mail/ui/AttachmentTileGrid;->getChildAt(I)Landroid/view/View;

    move-result-object v8

    invoke-virtual {v8}, Landroid/view/View;->getPaddingBottom()I

    move-result v8

    add-int/2addr v8, v4

    mul-int/2addr v8, v5

    invoke-virtual {p0, v7, v8}, Lcom/android/mail/ui/AttachmentTileGrid;->setMeasuredDimension(II)V

    goto :goto_0
.end method


# virtual methods
.method public addComposeTileFromAttachment(Lcom/android/mail/providers/Attachment;)Lcom/android/mail/compose/ComposeAttachmentTile;
    .locals 6
    .param p1    # Lcom/android/mail/providers/Attachment;

    iget-object v1, p0, Lcom/android/mail/ui/AttachmentTileGrid;->mInflater:Landroid/view/LayoutInflater;

    invoke-static {v1, p0}, Lcom/android/mail/compose/ComposeAttachmentTile;->inflate(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;)Lcom/android/mail/compose/ComposeAttachmentTile;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/android/mail/ui/AttachmentTileGrid;->addView(Landroid/view/View;)V

    const/4 v2, 0x0

    const/4 v3, -0x1

    const/4 v5, 0x0

    move-object v1, p1

    move-object v4, p0

    invoke-virtual/range {v0 .. v5}, Lcom/android/mail/compose/ComposeAttachmentTile;->render(Lcom/android/mail/providers/Attachment;Landroid/net/Uri;ILcom/android/mail/ui/AttachmentTile$AttachmentPreviewCache;Z)V

    return-object v0
.end method

.method public configureGrid(Landroid/app/FragmentManager;Landroid/net/Uri;Ljava/util/List;Z)V
    .locals 5
    .param p1    # Landroid/app/FragmentManager;
    .param p2    # Landroid/net/Uri;
    .param p4    # Z
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/app/FragmentManager;",
            "Landroid/net/Uri;",
            "Ljava/util/List",
            "<",
            "Lcom/android/mail/providers/Attachment;",
            ">;Z)V"
        }
    .end annotation

    iput-object p1, p0, Lcom/android/mail/ui/AttachmentTileGrid;->mFragmentManager:Landroid/app/FragmentManager;

    iput-object p2, p0, Lcom/android/mail/ui/AttachmentTileGrid;->mAttachmentsListUri:Landroid/net/Uri;

    iput-object p3, p0, Lcom/android/mail/ui/AttachmentTileGrid;->mAttachments:Ljava/util/List;

    const/4 v2, 0x0

    invoke-interface {p3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/mail/providers/Attachment;

    add-int/lit8 v3, v2, 0x1

    invoke-direct {p0, v0, v2, p4}, Lcom/android/mail/ui/AttachmentTileGrid;->addMessageTileFromAttachment(Lcom/android/mail/providers/Attachment;IZ)V

    move v2, v3

    goto :goto_0

    :cond_0
    return-void
.end method

.method public get(Lcom/android/mail/providers/Attachment;)Landroid/graphics/Bitmap;
    .locals 3
    .param p1    # Lcom/android/mail/providers/Attachment;

    invoke-virtual {p1}, Lcom/android/mail/providers/Attachment;->getIdentifierUri()Landroid/net/Uri;

    move-result-object v2

    invoke-virtual {v2}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v2, p0, Lcom/android/mail/ui/AttachmentTileGrid;->mAttachmentPreviews:Ljava/util/HashMap;

    invoke-virtual {v2, v0}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/android/mail/ui/AttachmentTile$AttachmentPreview;

    if-eqz v1, :cond_0

    iget-object v2, v1, Lcom/android/mail/ui/AttachmentTile$AttachmentPreview;->preview:Landroid/graphics/Bitmap;

    :goto_0
    return-object v2

    :cond_0
    const/4 v2, 0x0

    goto :goto_0
.end method

.method public getAttachmentPreviews()Ljava/util/ArrayList;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/android/mail/ui/AttachmentTile$AttachmentPreview;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/android/mail/ui/AttachmentTileGrid;->mAttachmentPreviews:Ljava/util/HashMap;

    invoke-virtual {v0}, Ljava/util/HashMap;->values()Ljava/util/Collection;

    move-result-object v0

    invoke-static {v0}, Lcom/google/common/collect/Lists;->newArrayList(Ljava/lang/Iterable;)Ljava/util/ArrayList;

    move-result-object v0

    return-object v0
.end method

.method protected onLayout(ZIIII)V
    .locals 0
    .param p1    # Z
    .param p2    # I
    .param p3    # I
    .param p4    # I
    .param p5    # I

    invoke-direct {p0}, Lcom/android/mail/ui/AttachmentTileGrid;->onLayoutForTiles()V

    return-void
.end method

.method protected onMeasure(II)V
    .locals 0
    .param p1    # I
    .param p2    # I

    invoke-direct {p0, p1}, Lcom/android/mail/ui/AttachmentTileGrid;->onMeasureForTiles(I)V

    return-void
.end method

.method public sendAccessibilityEvent(I)V
    .locals 0
    .param p1    # I

    return-void
.end method

.method public set(Lcom/android/mail/providers/Attachment;Landroid/graphics/Bitmap;)V
    .locals 3
    .param p1    # Lcom/android/mail/providers/Attachment;
    .param p2    # Landroid/graphics/Bitmap;

    invoke-virtual {p1}, Lcom/android/mail/providers/Attachment;->getIdentifierUri()Landroid/net/Uri;

    move-result-object v1

    invoke-virtual {v1}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v1, p0, Lcom/android/mail/ui/AttachmentTileGrid;->mAttachmentPreviews:Ljava/util/HashMap;

    new-instance v2, Lcom/android/mail/ui/AttachmentTile$AttachmentPreview;

    invoke-direct {v2, p1, p2}, Lcom/android/mail/ui/AttachmentTile$AttachmentPreview;-><init>(Lcom/android/mail/providers/Attachment;Landroid/graphics/Bitmap;)V

    invoke-virtual {v1, v0, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    :cond_0
    return-void
.end method

.method public setAttachmentPreviews(Ljava/util/ArrayList;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/android/mail/ui/AttachmentTile$AttachmentPreview;",
            ">;)V"
        }
    .end annotation

    if-eqz p1, :cond_0

    invoke-virtual {p1}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/android/mail/ui/AttachmentTile$AttachmentPreview;

    iget-object v2, p0, Lcom/android/mail/ui/AttachmentTileGrid;->mAttachmentPreviews:Ljava/util/HashMap;

    iget-object v3, v1, Lcom/android/mail/ui/AttachmentTile$AttachmentPreview;->attachmentIdentifier:Ljava/lang/String;

    invoke-virtual {v2, v3, v1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    :cond_0
    return-void
.end method
