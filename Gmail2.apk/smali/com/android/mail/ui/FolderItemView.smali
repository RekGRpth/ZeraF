.class public Lcom/android/mail/ui/FolderItemView;
.super Landroid/widget/RelativeLayout;
.source "FolderItemView.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/mail/ui/FolderItemView$DropHandler;
    }
.end annotation


# static fields
.field private static DRAG_STEADY_STATE_BACKGROUND:Landroid/graphics/drawable/Drawable;

.field private static DROPPABLE_HOVER_BACKGROUND:Landroid/graphics/drawable/Drawable;

.field private static NON_DROPPABLE_TARGET_TEXT_COLOR:I

.field private static SHORTCUT_ICON:Landroid/graphics/Bitmap;


# instance fields
.field private final LOG_TAG:Ljava/lang/String;

.field private mBackground:Landroid/graphics/drawable/Drawable;

.field private mDropHandler:Lcom/android/mail/ui/FolderItemView$DropHandler;

.field private mFolder:Lcom/android/mail/providers/Folder;

.field private mFolderParentIcon:Landroid/widget/ImageView;

.field private mFolderTextView:Landroid/widget/TextView;

.field private mInitialFolderTextColor:Landroid/content/res/ColorStateList;

.field private mInitialUnreadCountTextColor:Landroid/content/res/ColorStateList;

.field private mUnreadCountTextView:Landroid/widget/TextView;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1    # Landroid/content/Context;

    invoke-direct {p0, p1}, Landroid/widget/RelativeLayout;-><init>(Landroid/content/Context;)V

    invoke-static {}, Lcom/android/mail/utils/LogTag;->getLogTag()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/android/mail/ui/FolderItemView;->LOG_TAG:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/util/AttributeSet;

    invoke-direct {p0, p1, p2}, Landroid/widget/RelativeLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    invoke-static {}, Lcom/android/mail/utils/LogTag;->getLogTag()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/android/mail/ui/FolderItemView;->LOG_TAG:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 1
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/util/AttributeSet;
    .param p3    # I

    invoke-direct {p0, p1, p2, p3}, Landroid/widget/RelativeLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    invoke-static {}, Lcom/android/mail/utils/LogTag;->getLogTag()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/android/mail/ui/FolderItemView;->LOG_TAG:Ljava/lang/String;

    return-void
.end method

.method private isDroppableTarget(Landroid/view/DragEvent;)Z
    .locals 2
    .param p1    # Landroid/view/DragEvent;

    iget-object v0, p0, Lcom/android/mail/ui/FolderItemView;->mDropHandler:Lcom/android/mail/ui/FolderItemView$DropHandler;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/mail/ui/FolderItemView;->mDropHandler:Lcom/android/mail/ui/FolderItemView$DropHandler;

    iget-object v1, p0, Lcom/android/mail/ui/FolderItemView;->mFolder:Lcom/android/mail/providers/Folder;

    invoke-interface {v0, p1, v1}, Lcom/android/mail/ui/FolderItemView$DropHandler;->supportsDrag(Landroid/view/DragEvent;Lcom/android/mail/providers/Folder;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private final setUnreadCount(I)V
    .locals 2
    .param p1    # I

    iget-object v1, p0, Lcom/android/mail/ui/FolderItemView;->mUnreadCountTextView:Landroid/widget/TextView;

    if-lez p1, :cond_1

    const/4 v0, 0x0

    :goto_0
    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setVisibility(I)V

    if-lez p1, :cond_0

    iget-object v0, p0, Lcom/android/mail/ui/FolderItemView;->mUnreadCountTextView:Landroid/widget/TextView;

    invoke-virtual {p0}, Lcom/android/mail/ui/FolderItemView;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1, p1}, Lcom/android/mail/utils/Utils;->getUnreadCountString(Landroid/content/Context;I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    :cond_0
    return-void

    :cond_1
    const/16 v0, 0x8

    goto :goto_0
.end method


# virtual methods
.method public bind(Lcom/android/mail/providers/Folder;Lcom/android/mail/ui/FolderItemView$DropHandler;)V
    .locals 2
    .param p1    # Lcom/android/mail/providers/Folder;
    .param p2    # Lcom/android/mail/ui/FolderItemView$DropHandler;

    iput-object p1, p0, Lcom/android/mail/ui/FolderItemView;->mFolder:Lcom/android/mail/providers/Folder;

    iput-object p2, p0, Lcom/android/mail/ui/FolderItemView;->mDropHandler:Lcom/android/mail/ui/FolderItemView$DropHandler;

    iget-object v0, p0, Lcom/android/mail/ui/FolderItemView;->mFolderTextView:Landroid/widget/TextView;

    iget-object v1, p1, Lcom/android/mail/providers/Folder;->name:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v1, p0, Lcom/android/mail/ui/FolderItemView;->mFolderParentIcon:Landroid/widget/ImageView;

    iget-object v0, p0, Lcom/android/mail/ui/FolderItemView;->mFolder:Lcom/android/mail/providers/Folder;

    iget-boolean v0, v0, Lcom/android/mail/providers/Folder;->hasChildren:Z

    if-eqz v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    invoke-virtual {v1, v0}, Landroid/widget/ImageView;->setVisibility(I)V

    iget-object v0, p0, Lcom/android/mail/ui/FolderItemView;->mFolder:Lcom/android/mail/providers/Folder;

    invoke-static {v0}, Lcom/android/mail/utils/Utils;->getFolderUnreadDisplayCount(Lcom/android/mail/providers/Folder;)I

    move-result v0

    invoke-direct {p0, v0}, Lcom/android/mail/ui/FolderItemView;->setUnreadCount(I)V

    return-void

    :cond_0
    const/16 v0, 0x8

    goto :goto_0
.end method

.method public onDragEvent(Landroid/view/DragEvent;)Z
    .locals 3
    .param p1    # Landroid/view/DragEvent;

    const/4 v1, 0x0

    const/4 v0, 0x1

    invoke-virtual {p1}, Landroid/view/DragEvent;->getAction()I

    move-result v2

    packed-switch v2, :pswitch_data_0

    :cond_0
    move v0, v1

    :goto_0
    :pswitch_0
    return v0

    :pswitch_1
    invoke-direct {p0, p1}, Lcom/android/mail/ui/FolderItemView;->isDroppableTarget(Landroid/view/DragEvent;)Z

    move-result v1

    if-nez v1, :cond_1

    iget-object v1, p0, Lcom/android/mail/ui/FolderItemView;->mFolderTextView:Landroid/widget/TextView;

    invoke-virtual {v1}, Landroid/widget/TextView;->getTextColors()Landroid/content/res/ColorStateList;

    move-result-object v1

    iput-object v1, p0, Lcom/android/mail/ui/FolderItemView;->mInitialFolderTextColor:Landroid/content/res/ColorStateList;

    iget-object v1, p0, Lcom/android/mail/ui/FolderItemView;->mUnreadCountTextView:Landroid/widget/TextView;

    invoke-virtual {v1}, Landroid/widget/TextView;->getTextColors()Landroid/content/res/ColorStateList;

    move-result-object v1

    iput-object v1, p0, Lcom/android/mail/ui/FolderItemView;->mInitialUnreadCountTextColor:Landroid/content/res/ColorStateList;

    iget-object v1, p0, Lcom/android/mail/ui/FolderItemView;->mFolderTextView:Landroid/widget/TextView;

    sget v2, Lcom/android/mail/ui/FolderItemView;->NON_DROPPABLE_TARGET_TEXT_COLOR:I

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setTextColor(I)V

    iget-object v1, p0, Lcom/android/mail/ui/FolderItemView;->mUnreadCountTextView:Landroid/widget/TextView;

    sget v2, Lcom/android/mail/ui/FolderItemView;->NON_DROPPABLE_TARGET_TEXT_COLOR:I

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setTextColor(I)V

    :cond_1
    sget-object v1, Lcom/android/mail/ui/FolderItemView;->DRAG_STEADY_STATE_BACKGROUND:Landroid/graphics/drawable/Drawable;

    invoke-virtual {p0, v1}, Lcom/android/mail/ui/FolderItemView;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    goto :goto_0

    :pswitch_2
    invoke-direct {p0, p1}, Lcom/android/mail/ui/FolderItemView;->isDroppableTarget(Landroid/view/DragEvent;)Z

    move-result v2

    if-eqz v2, :cond_0

    sget-object v1, Lcom/android/mail/ui/FolderItemView;->DROPPABLE_HOVER_BACKGROUND:Landroid/graphics/drawable/Drawable;

    invoke-virtual {p0, v1}, Lcom/android/mail/ui/FolderItemView;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    goto :goto_0

    :pswitch_3
    invoke-direct {p0, p1}, Lcom/android/mail/ui/FolderItemView;->isDroppableTarget(Landroid/view/DragEvent;)Z

    move-result v2

    if-eqz v2, :cond_0

    sget-object v1, Lcom/android/mail/ui/FolderItemView;->DRAG_STEADY_STATE_BACKGROUND:Landroid/graphics/drawable/Drawable;

    invoke-virtual {p0, v1}, Lcom/android/mail/ui/FolderItemView;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    goto :goto_0

    :pswitch_4
    invoke-direct {p0, p1}, Lcom/android/mail/ui/FolderItemView;->isDroppableTarget(Landroid/view/DragEvent;)Z

    move-result v1

    if-nez v1, :cond_2

    iget-object v1, p0, Lcom/android/mail/ui/FolderItemView;->mFolderTextView:Landroid/widget/TextView;

    iget-object v2, p0, Lcom/android/mail/ui/FolderItemView;->mInitialFolderTextColor:Landroid/content/res/ColorStateList;

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setTextColor(Landroid/content/res/ColorStateList;)V

    iget-object v1, p0, Lcom/android/mail/ui/FolderItemView;->mUnreadCountTextView:Landroid/widget/TextView;

    iget-object v2, p0, Lcom/android/mail/ui/FolderItemView;->mInitialUnreadCountTextColor:Landroid/content/res/ColorStateList;

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setTextColor(Landroid/content/res/ColorStateList;)V

    :cond_2
    iget-object v1, p0, Lcom/android/mail/ui/FolderItemView;->mBackground:Landroid/graphics/drawable/Drawable;

    invoke-virtual {p0, v1}, Lcom/android/mail/ui/FolderItemView;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    goto :goto_0

    :pswitch_5
    iget-object v2, p0, Lcom/android/mail/ui/FolderItemView;->mDropHandler:Lcom/android/mail/ui/FolderItemView$DropHandler;

    if-nez v2, :cond_3

    move v0, v1

    goto :goto_0

    :cond_3
    iget-object v1, p0, Lcom/android/mail/ui/FolderItemView;->mDropHandler:Lcom/android/mail/ui/FolderItemView$DropHandler;

    iget-object v2, p0, Lcom/android/mail/ui/FolderItemView;->mFolder:Lcom/android/mail/providers/Folder;

    invoke-interface {v1, p1, v2}, Lcom/android/mail/ui/FolderItemView$DropHandler;->handleDrop(Landroid/view/DragEvent;Lcom/android/mail/providers/Folder;)V

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_0
        :pswitch_5
        :pswitch_4
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method protected onFinishInflate()V
    .locals 2

    invoke-super {p0}, Landroid/widget/RelativeLayout;->onFinishInflate()V

    sget-object v1, Lcom/android/mail/ui/FolderItemView;->SHORTCUT_ICON:Landroid/graphics/Bitmap;

    if-nez v1, :cond_0

    invoke-virtual {p0}, Lcom/android/mail/ui/FolderItemView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f030001

    invoke-static {v0, v1}, Landroid/graphics/BitmapFactory;->decodeResource(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;

    move-result-object v1

    sput-object v1, Lcom/android/mail/ui/FolderItemView;->SHORTCUT_ICON:Landroid/graphics/Bitmap;

    const v1, 0x7f02001e

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    sput-object v1, Lcom/android/mail/ui/FolderItemView;->DROPPABLE_HOVER_BACKGROUND:Landroid/graphics/drawable/Drawable;

    const v1, 0x7f020020

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    sput-object v1, Lcom/android/mail/ui/FolderItemView;->DRAG_STEADY_STATE_BACKGROUND:Landroid/graphics/drawable/Drawable;

    const v1, 0x7f0a0024

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    sput v1, Lcom/android/mail/ui/FolderItemView;->NON_DROPPABLE_TARGET_TEXT_COLOR:I

    :cond_0
    const v1, 0x7f080034

    invoke-virtual {p0, v1}, Lcom/android/mail/ui/FolderItemView;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iput-object v1, p0, Lcom/android/mail/ui/FolderItemView;->mFolderTextView:Landroid/widget/TextView;

    const v1, 0x7f080033

    invoke-virtual {p0, v1}, Lcom/android/mail/ui/FolderItemView;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iput-object v1, p0, Lcom/android/mail/ui/FolderItemView;->mUnreadCountTextView:Landroid/widget/TextView;

    invoke-virtual {p0}, Lcom/android/mail/ui/FolderItemView;->getBackground()Landroid/graphics/drawable/Drawable;

    move-result-object v1

    iput-object v1, p0, Lcom/android/mail/ui/FolderItemView;->mBackground:Landroid/graphics/drawable/Drawable;

    iget-object v1, p0, Lcom/android/mail/ui/FolderItemView;->mFolderTextView:Landroid/widget/TextView;

    invoke-virtual {v1}, Landroid/widget/TextView;->getTextColors()Landroid/content/res/ColorStateList;

    move-result-object v1

    iput-object v1, p0, Lcom/android/mail/ui/FolderItemView;->mInitialFolderTextColor:Landroid/content/res/ColorStateList;

    iget-object v1, p0, Lcom/android/mail/ui/FolderItemView;->mUnreadCountTextView:Landroid/widget/TextView;

    invoke-virtual {v1}, Landroid/widget/TextView;->getTextColors()Landroid/content/res/ColorStateList;

    move-result-object v1

    iput-object v1, p0, Lcom/android/mail/ui/FolderItemView;->mInitialUnreadCountTextColor:Landroid/content/res/ColorStateList;

    const v1, 0x7f080031

    invoke-virtual {p0, v1}, Lcom/android/mail/ui/FolderItemView;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ImageView;

    iput-object v1, p0, Lcom/android/mail/ui/FolderItemView;->mFolderParentIcon:Landroid/widget/ImageView;

    return-void
.end method

.method public final overrideUnreadCount(I)V
    .locals 5
    .param p1    # I

    iget-object v0, p0, Lcom/android/mail/ui/FolderItemView;->LOG_TAG:Ljava/lang/String;

    const-string v1, "FLF->FolderItem.getFolderView: unread count mismatch found (%s vs %d)"

    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    iget-object v4, p0, Lcom/android/mail/ui/FolderItemView;->mUnreadCountTextView:Landroid/widget/TextView;

    invoke-virtual {v4}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v4

    aput-object v4, v2, v3

    const/4 v3, 0x1

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-static {v0, v1, v2}, Lcom/android/mail/utils/LogUtils;->e(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)I

    invoke-direct {p0, p1}, Lcom/android/mail/ui/FolderItemView;->setUnreadCount(I)V

    return-void
.end method
