.class public interface abstract Lcom/android/mail/ui/SwipeableItemView;
.super Ljava/lang/Object;
.source "SwipeableItemView.java"


# virtual methods
.method public abstract canChildBeDismissed()Z
.end method

.method public abstract dismiss()V
.end method

.method public abstract getMinAllowScrollDistance()F
.end method

.method public abstract getSwipeableView()Landroid/view/View;
.end method
