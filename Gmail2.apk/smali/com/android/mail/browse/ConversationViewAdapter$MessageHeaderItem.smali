.class public Lcom/android/mail/browse/ConversationViewAdapter$MessageHeaderItem;
.super Lcom/android/mail/browse/ConversationOverlayItem;
.source "ConversationViewAdapter.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/mail/browse/ConversationViewAdapter;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1
    name = "MessageHeaderItem"
.end annotation


# instance fields
.field public detailsExpanded:Z

.field private mExpanded:Z

.field private mMessage:Lcom/android/mail/browse/MessageCursor$ConversationMessage;

.field private mShowImages:Z

.field public recipientSummaryText:Ljava/lang/CharSequence;

.field final synthetic this$0:Lcom/android/mail/browse/ConversationViewAdapter;

.field public timestampLong:Ljava/lang/CharSequence;

.field public timestampShort:Ljava/lang/CharSequence;


# direct methods
.method constructor <init>(Lcom/android/mail/browse/ConversationViewAdapter;Lcom/android/mail/browse/MessageCursor$ConversationMessage;ZZ)V
    .locals 1
    .param p2    # Lcom/android/mail/browse/MessageCursor$ConversationMessage;
    .param p3    # Z
    .param p4    # Z

    iput-object p1, p0, Lcom/android/mail/browse/ConversationViewAdapter$MessageHeaderItem;->this$0:Lcom/android/mail/browse/ConversationViewAdapter;

    invoke-direct {p0}, Lcom/android/mail/browse/ConversationOverlayItem;-><init>()V

    iput-object p2, p0, Lcom/android/mail/browse/ConversationViewAdapter$MessageHeaderItem;->mMessage:Lcom/android/mail/browse/MessageCursor$ConversationMessage;

    iput-boolean p3, p0, Lcom/android/mail/browse/ConversationViewAdapter$MessageHeaderItem;->mExpanded:Z

    iput-boolean p4, p0, Lcom/android/mail/browse/ConversationViewAdapter$MessageHeaderItem;->mShowImages:Z

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/android/mail/browse/ConversationViewAdapter$MessageHeaderItem;->detailsExpanded:Z

    return-void
.end method


# virtual methods
.method public belongsToMessage(Lcom/android/mail/browse/MessageCursor$ConversationMessage;)Z
    .locals 1
    .param p1    # Lcom/android/mail/browse/MessageCursor$ConversationMessage;

    iget-object v0, p0, Lcom/android/mail/browse/ConversationViewAdapter$MessageHeaderItem;->mMessage:Lcom/android/mail/browse/MessageCursor$ConversationMessage;

    invoke-static {v0, p1}, Lcom/google/common/base/Objects;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public bindView(Landroid/view/View;Z)V
    .locals 1
    .param p1    # Landroid/view/View;
    .param p2    # Z

    move-object v0, p1

    check-cast v0, Lcom/android/mail/browse/MessageHeaderView;

    invoke-virtual {v0, p0, p2}, Lcom/android/mail/browse/MessageHeaderView;->bind(Lcom/android/mail/browse/ConversationViewAdapter$MessageHeaderItem;Z)V

    return-void
.end method

.method public canBecomeSnapHeader()Z
    .locals 1

    invoke-virtual {p0}, Lcom/android/mail/browse/ConversationViewAdapter$MessageHeaderItem;->isExpanded()Z

    move-result v0

    return v0
.end method

.method public canPushSnapHeader()Z
    .locals 1

    const/4 v0, 0x1

    return v0
.end method

.method public createView(Landroid/content/Context;Landroid/view/LayoutInflater;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 4
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/view/LayoutInflater;
    .param p3    # Landroid/view/ViewGroup;

    const v1, 0x7f040028

    const/4 v2, 0x0

    invoke-virtual {p2, v1, p3, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/android/mail/browse/MessageHeaderView;

    iget-object v1, p0, Lcom/android/mail/browse/ConversationViewAdapter$MessageHeaderItem;->this$0:Lcom/android/mail/browse/ConversationViewAdapter;

    # getter for: Lcom/android/mail/browse/ConversationViewAdapter;->mDateBuilder:Lcom/android/mail/FormattedDateBuilder;
    invoke-static {v1}, Lcom/android/mail/browse/ConversationViewAdapter;->access$200(Lcom/android/mail/browse/ConversationViewAdapter;)Lcom/android/mail/FormattedDateBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/android/mail/browse/ConversationViewAdapter$MessageHeaderItem;->this$0:Lcom/android/mail/browse/ConversationViewAdapter;

    # getter for: Lcom/android/mail/browse/ConversationViewAdapter;->mAccountController:Lcom/android/mail/browse/ConversationAccountController;
    invoke-static {v2}, Lcom/android/mail/browse/ConversationViewAdapter;->access$100(Lcom/android/mail/browse/ConversationViewAdapter;)Lcom/android/mail/browse/ConversationAccountController;

    move-result-object v2

    iget-object v3, p0, Lcom/android/mail/browse/ConversationViewAdapter$MessageHeaderItem;->this$0:Lcom/android/mail/browse/ConversationViewAdapter;

    # getter for: Lcom/android/mail/browse/ConversationViewAdapter;->mAddressCache:Ljava/util/Map;
    invoke-static {v3}, Lcom/android/mail/browse/ConversationViewAdapter;->access$300(Lcom/android/mail/browse/ConversationViewAdapter;)Ljava/util/Map;

    move-result-object v3

    invoke-virtual {v0, v1, v2, v3}, Lcom/android/mail/browse/MessageHeaderView;->initialize(Lcom/android/mail/FormattedDateBuilder;Lcom/android/mail/browse/ConversationAccountController;Ljava/util/Map;)V

    iget-object v1, p0, Lcom/android/mail/browse/ConversationViewAdapter$MessageHeaderItem;->this$0:Lcom/android/mail/browse/ConversationViewAdapter;

    # getter for: Lcom/android/mail/browse/ConversationViewAdapter;->mMessageCallbacks:Lcom/android/mail/browse/MessageHeaderView$MessageHeaderViewCallbacks;
    invoke-static {v1}, Lcom/android/mail/browse/ConversationViewAdapter;->access$400(Lcom/android/mail/browse/ConversationViewAdapter;)Lcom/android/mail/browse/MessageHeaderView$MessageHeaderViewCallbacks;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/android/mail/browse/MessageHeaderView;->setCallbacks(Lcom/android/mail/browse/MessageHeaderView$MessageHeaderViewCallbacks;)V

    iget-object v1, p0, Lcom/android/mail/browse/ConversationViewAdapter$MessageHeaderItem;->this$0:Lcom/android/mail/browse/ConversationViewAdapter;

    # getter for: Lcom/android/mail/browse/ConversationViewAdapter;->mContactInfoSource:Lcom/android/mail/ContactInfoSource;
    invoke-static {v1}, Lcom/android/mail/browse/ConversationViewAdapter;->access$500(Lcom/android/mail/browse/ConversationViewAdapter;)Lcom/android/mail/ContactInfoSource;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/android/mail/browse/MessageHeaderView;->setContactInfoSource(Lcom/android/mail/ContactInfoSource;)V

    iget-object v1, p0, Lcom/android/mail/browse/ConversationViewAdapter$MessageHeaderItem;->this$0:Lcom/android/mail/browse/ConversationViewAdapter;

    # getter for: Lcom/android/mail/browse/ConversationViewAdapter;->mMatcher:Lcom/android/mail/utils/VeiledAddressMatcher;
    invoke-static {v1}, Lcom/android/mail/browse/ConversationViewAdapter;->access$600(Lcom/android/mail/browse/ConversationViewAdapter;)Lcom/android/mail/utils/VeiledAddressMatcher;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/android/mail/browse/MessageHeaderView;->setVeiledMatcher(Lcom/android/mail/utils/VeiledAddressMatcher;)V

    return-object v0
.end method

.method public getMessage()Lcom/android/mail/browse/MessageCursor$ConversationMessage;
    .locals 1

    iget-object v0, p0, Lcom/android/mail/browse/ConversationViewAdapter$MessageHeaderItem;->mMessage:Lcom/android/mail/browse/MessageCursor$ConversationMessage;

    return-object v0
.end method

.method public getShowImages()Z
    .locals 1

    iget-boolean v0, p0, Lcom/android/mail/browse/ConversationViewAdapter$MessageHeaderItem;->mShowImages:Z

    return v0
.end method

.method public getType()I
    .locals 1

    const/4 v0, 0x1

    return v0
.end method

.method public isContiguous()Z
    .locals 1

    invoke-virtual {p0}, Lcom/android/mail/browse/ConversationViewAdapter$MessageHeaderItem;->isExpanded()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isExpanded()Z
    .locals 1

    iget-boolean v0, p0, Lcom/android/mail/browse/ConversationViewAdapter$MessageHeaderItem;->mExpanded:Z

    return v0
.end method

.method public onModelUpdated(Landroid/view/View;)V
    .locals 1
    .param p1    # Landroid/view/View;

    move-object v0, p1

    check-cast v0, Lcom/android/mail/browse/MessageHeaderView;

    invoke-virtual {v0}, Lcom/android/mail/browse/MessageHeaderView;->refresh()V

    return-void
.end method

.method public setExpanded(Z)V
    .locals 1
    .param p1    # Z

    iget-boolean v0, p0, Lcom/android/mail/browse/ConversationViewAdapter$MessageHeaderItem;->mExpanded:Z

    if-eq v0, p1, :cond_0

    iput-boolean p1, p0, Lcom/android/mail/browse/ConversationViewAdapter$MessageHeaderItem;->mExpanded:Z

    :cond_0
    return-void
.end method

.method public setMessage(Lcom/android/mail/browse/MessageCursor$ConversationMessage;)V
    .locals 0
    .param p1    # Lcom/android/mail/browse/MessageCursor$ConversationMessage;

    iput-object p1, p0, Lcom/android/mail/browse/ConversationViewAdapter$MessageHeaderItem;->mMessage:Lcom/android/mail/browse/MessageCursor$ConversationMessage;

    return-void
.end method

.method public setShowImages(Z)V
    .locals 0
    .param p1    # Z

    iput-boolean p1, p0, Lcom/android/mail/browse/ConversationViewAdapter$MessageHeaderItem;->mShowImages:Z

    return-void
.end method
