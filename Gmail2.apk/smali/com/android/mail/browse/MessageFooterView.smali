.class public Lcom/android/mail/browse/MessageFooterView;
.super Landroid/widget/LinearLayout;
.source "MessageFooterView.java"

# interfaces
.implements Landroid/app/LoaderManager$LoaderCallbacks;
.implements Lcom/android/mail/browse/ConversationContainer$DetachListener;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/widget/LinearLayout;",
        "Lcom/android/mail/browse/ConversationContainer$DetachListener;",
        "Landroid/app/LoaderManager$LoaderCallbacks",
        "<",
        "Landroid/database/Cursor;",
        ">;"
    }
.end annotation


# static fields
.field private static final LOG_TAG:Ljava/lang/String;


# instance fields
.field private mAttachmentBarList:Landroid/widget/LinearLayout;

.field private mAttachmentGrid:Lcom/android/mail/ui/AttachmentTileGrid;

.field private mAttachmentsCursor:Lcom/android/mail/browse/AttachmentLoader$AttachmentCursor;

.field private mFragmentManager:Landroid/app/FragmentManager;

.field private final mInflater:Landroid/view/LayoutInflater;

.field private mLoaderManager:Landroid/app/LoaderManager;

.field private mMessageHeaderItem:Lcom/android/mail/browse/ConversationViewAdapter$MessageHeaderItem;

.field private mTitleBar:Landroid/view/View;

.field private mTitleText:Landroid/widget/TextView;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    invoke-static {}, Lcom/android/mail/utils/LogTag;->getLogTag()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/android/mail/browse/MessageFooterView;->LOG_TAG:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1    # Landroid/content/Context;

    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/android/mail/browse/MessageFooterView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/util/AttributeSet;

    invoke-direct {p0, p1, p2}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    invoke-static {p1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    iput-object v0, p0, Lcom/android/mail/browse/MessageFooterView;->mInflater:Landroid/view/LayoutInflater;

    return-void
.end method

.method private getAttachmentLoaderId()Ljava/lang/Integer;
    .locals 3

    const/4 v0, 0x0

    iget-object v2, p0, Lcom/android/mail/browse/MessageFooterView;->mMessageHeaderItem:Lcom/android/mail/browse/ConversationViewAdapter$MessageHeaderItem;

    if-nez v2, :cond_1

    const/4 v1, 0x0

    :goto_0
    if-eqz v1, :cond_0

    iget-boolean v2, v1, Lcom/android/mail/providers/Message;->hasAttachments:Z

    if-eqz v2, :cond_0

    iget-object v2, v1, Lcom/android/mail/providers/Message;->attachmentListUri:Landroid/net/Uri;

    if-eqz v2, :cond_0

    iget-object v2, v1, Lcom/android/mail/providers/Message;->attachmentListUri:Landroid/net/Uri;

    invoke-virtual {v2}, Landroid/net/Uri;->hashCode()I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    :cond_0
    return-object v0

    :cond_1
    iget-object v2, p0, Lcom/android/mail/browse/MessageFooterView;->mMessageHeaderItem:Lcom/android/mail/browse/ConversationViewAdapter$MessageHeaderItem;

    invoke-virtual {v2}, Lcom/android/mail/browse/ConversationViewAdapter$MessageHeaderItem;->getMessage()Lcom/android/mail/browse/MessageCursor$ConversationMessage;

    move-result-object v1

    goto :goto_0
.end method

.method private renderAttachments(Ljava/util/List;Z)V
    .locals 8
    .param p2    # Z
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/android/mail/providers/Attachment;",
            ">;Z)V"
        }
    .end annotation

    const/4 v7, 0x0

    if-eqz p1, :cond_0

    invoke-interface {p1}, Ljava/util/List;->isEmpty()Z

    move-result v5

    if-eqz v5, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v3

    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4, v3}, Ljava/util/ArrayList;-><init>(I)V

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1, v3}, Ljava/util/ArrayList;-><init>(I)V

    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_3

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/mail/providers/Attachment;

    invoke-static {v0}, Lcom/android/mail/ui/AttachmentTile;->isTiledAttachment(Lcom/android/mail/providers/Attachment;)Z

    move-result v5

    if-eqz v5, :cond_2

    invoke-interface {v4, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_1

    :cond_2
    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_1

    :cond_3
    iget-object v5, p0, Lcom/android/mail/browse/MessageFooterView;->mMessageHeaderItem:Lcom/android/mail/browse/ConversationViewAdapter$MessageHeaderItem;

    invoke-virtual {v5}, Lcom/android/mail/browse/ConversationViewAdapter$MessageHeaderItem;->getMessage()Lcom/android/mail/browse/MessageCursor$ConversationMessage;

    move-result-object v5

    invoke-static {p1}, Lcom/android/mail/providers/Attachment;->toJSONArray(Ljava/util/Collection;)Ljava/lang/String;

    move-result-object v6

    iput-object v6, v5, Lcom/android/mail/browse/MessageCursor$ConversationMessage;->attachmentsJson:Ljava/lang/String;

    iget-object v5, p0, Lcom/android/mail/browse/MessageFooterView;->mTitleText:Landroid/widget/TextView;

    invoke-virtual {v5, v7}, Landroid/widget/TextView;->setVisibility(I)V

    iget-object v5, p0, Lcom/android/mail/browse/MessageFooterView;->mTitleBar:Landroid/view/View;

    invoke-virtual {v5, v7}, Landroid/view/View;->setVisibility(I)V

    invoke-direct {p0, v4, p2}, Lcom/android/mail/browse/MessageFooterView;->renderTiledAttachments(Ljava/util/List;Z)V

    invoke-direct {p0, v1, p2}, Lcom/android/mail/browse/MessageFooterView;->renderBarAttachments(Ljava/util/List;Z)V

    goto :goto_0
.end method

.method private renderAttachments(Z)V
    .locals 3
    .param p1    # Z

    iget-object v2, p0, Lcom/android/mail/browse/MessageFooterView;->mAttachmentsCursor:Lcom/android/mail/browse/AttachmentLoader$AttachmentCursor;

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/android/mail/browse/MessageFooterView;->mAttachmentsCursor:Lcom/android/mail/browse/AttachmentLoader$AttachmentCursor;

    invoke-virtual {v2}, Lcom/android/mail/browse/AttachmentLoader$AttachmentCursor;->isClosed()Z

    move-result v2

    if-nez v2, :cond_0

    const/4 v1, -0x1

    invoke-static {}, Lcom/google/common/collect/Lists;->newArrayList()Ljava/util/ArrayList;

    move-result-object v0

    :goto_0
    iget-object v2, p0, Lcom/android/mail/browse/MessageFooterView;->mAttachmentsCursor:Lcom/android/mail/browse/AttachmentLoader$AttachmentCursor;

    add-int/lit8 v1, v1, 0x1

    invoke-virtual {v2, v1}, Lcom/android/mail/browse/AttachmentLoader$AttachmentCursor;->moveToPosition(I)Z

    move-result v2

    if-eqz v2, :cond_1

    iget-object v2, p0, Lcom/android/mail/browse/MessageFooterView;->mAttachmentsCursor:Lcom/android/mail/browse/AttachmentLoader$AttachmentCursor;

    invoke-virtual {v2}, Lcom/android/mail/browse/AttachmentLoader$AttachmentCursor;->get()Lcom/android/mail/providers/Attachment;

    move-result-object v2

    invoke-interface {v0, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_0
    iget-object v2, p0, Lcom/android/mail/browse/MessageFooterView;->mMessageHeaderItem:Lcom/android/mail/browse/ConversationViewAdapter$MessageHeaderItem;

    invoke-virtual {v2}, Lcom/android/mail/browse/ConversationViewAdapter$MessageHeaderItem;->getMessage()Lcom/android/mail/browse/MessageCursor$ConversationMessage;

    move-result-object v2

    invoke-virtual {v2}, Lcom/android/mail/browse/MessageCursor$ConversationMessage;->getAttachments()Ljava/util/List;

    move-result-object v0

    :cond_1
    invoke-direct {p0, v0, p1}, Lcom/android/mail/browse/MessageFooterView;->renderAttachments(Ljava/util/List;Z)V

    return-void
.end method

.method private renderBarAttachments(Ljava/util/List;Z)V
    .locals 6
    .param p2    # Z
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/android/mail/providers/Attachment;",
            ">;Z)V"
        }
    .end annotation

    iget-object v4, p0, Lcom/android/mail/browse/MessageFooterView;->mAttachmentBarList:Landroid/widget/LinearLayout;

    const/4 v5, 0x0

    invoke-virtual {v4, v5}, Landroid/widget/LinearLayout;->setVisibility(I)V

    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/mail/providers/Attachment;

    invoke-virtual {v0}, Lcom/android/mail/providers/Attachment;->getIdentifierUri()Landroid/net/Uri;

    move-result-object v3

    iget-object v4, p0, Lcom/android/mail/browse/MessageFooterView;->mAttachmentBarList:Landroid/widget/LinearLayout;

    invoke-virtual {v4, v3}, Landroid/widget/LinearLayout;->findViewWithTag(Ljava/lang/Object;)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/android/mail/browse/MessageAttachmentBar;

    if-nez v1, :cond_0

    iget-object v4, p0, Lcom/android/mail/browse/MessageFooterView;->mInflater:Landroid/view/LayoutInflater;

    invoke-static {v4, p0}, Lcom/android/mail/browse/MessageAttachmentBar;->inflate(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;)Lcom/android/mail/browse/MessageAttachmentBar;

    move-result-object v1

    invoke-virtual {v1, v3}, Lcom/android/mail/browse/MessageAttachmentBar;->setTag(Ljava/lang/Object;)V

    iget-object v4, p0, Lcom/android/mail/browse/MessageFooterView;->mFragmentManager:Landroid/app/FragmentManager;

    invoke-virtual {v1, v4}, Lcom/android/mail/browse/MessageAttachmentBar;->initialize(Landroid/app/FragmentManager;)V

    iget-object v4, p0, Lcom/android/mail/browse/MessageFooterView;->mAttachmentBarList:Landroid/widget/LinearLayout;

    invoke-virtual {v4, v1}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    :cond_0
    invoke-virtual {v1, v0, p2}, Lcom/android/mail/browse/MessageAttachmentBar;->render(Lcom/android/mail/providers/Attachment;Z)V

    goto :goto_0

    :cond_1
    return-void
.end method

.method private renderTiledAttachments(Ljava/util/List;Z)V
    .locals 3
    .param p2    # Z
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/android/mail/providers/Attachment;",
            ">;Z)V"
        }
    .end annotation

    iget-object v0, p0, Lcom/android/mail/browse/MessageFooterView;->mAttachmentGrid:Lcom/android/mail/ui/AttachmentTileGrid;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/android/mail/ui/AttachmentTileGrid;->setVisibility(I)V

    iget-object v0, p0, Lcom/android/mail/browse/MessageFooterView;->mAttachmentGrid:Lcom/android/mail/ui/AttachmentTileGrid;

    iget-object v1, p0, Lcom/android/mail/browse/MessageFooterView;->mFragmentManager:Landroid/app/FragmentManager;

    iget-object v2, p0, Lcom/android/mail/browse/MessageFooterView;->mMessageHeaderItem:Lcom/android/mail/browse/ConversationViewAdapter$MessageHeaderItem;

    invoke-virtual {v2}, Lcom/android/mail/browse/ConversationViewAdapter$MessageHeaderItem;->getMessage()Lcom/android/mail/browse/MessageCursor$ConversationMessage;

    move-result-object v2

    iget-object v2, v2, Lcom/android/mail/browse/MessageCursor$ConversationMessage;->attachmentListUri:Landroid/net/Uri;

    invoke-virtual {v0, v1, v2, p1, p2}, Lcom/android/mail/ui/AttachmentTileGrid;->configureGrid(Landroid/app/FragmentManager;Landroid/net/Uri;Ljava/util/List;Z)V

    return-void
.end method


# virtual methods
.method public bind(Lcom/android/mail/browse/ConversationViewAdapter$MessageHeaderItem;Z)V
    .locals 7
    .param p1    # Lcom/android/mail/browse/ConversationViewAdapter$MessageHeaderItem;
    .param p2    # Z

    const/4 v2, 0x0

    const/16 v3, 0x8

    iget-object v4, p0, Lcom/android/mail/browse/MessageFooterView;->mMessageHeaderItem:Lcom/android/mail/browse/ConversationViewAdapter$MessageHeaderItem;

    if-eqz v4, :cond_0

    iget-object v4, p0, Lcom/android/mail/browse/MessageFooterView;->mMessageHeaderItem:Lcom/android/mail/browse/ConversationViewAdapter$MessageHeaderItem;

    invoke-virtual {v4}, Lcom/android/mail/browse/ConversationViewAdapter$MessageHeaderItem;->getMessage()Lcom/android/mail/browse/MessageCursor$ConversationMessage;

    move-result-object v4

    if-eqz v4, :cond_0

    iget-object v4, p0, Lcom/android/mail/browse/MessageFooterView;->mMessageHeaderItem:Lcom/android/mail/browse/ConversationViewAdapter$MessageHeaderItem;

    invoke-virtual {v4}, Lcom/android/mail/browse/ConversationViewAdapter$MessageHeaderItem;->getMessage()Lcom/android/mail/browse/MessageCursor$ConversationMessage;

    move-result-object v4

    iget-object v4, v4, Lcom/android/mail/browse/MessageCursor$ConversationMessage;->attachmentListUri:Landroid/net/Uri;

    if-eqz v4, :cond_0

    iget-object v4, p0, Lcom/android/mail/browse/MessageFooterView;->mMessageHeaderItem:Lcom/android/mail/browse/ConversationViewAdapter$MessageHeaderItem;

    invoke-virtual {v4}, Lcom/android/mail/browse/ConversationViewAdapter$MessageHeaderItem;->getMessage()Lcom/android/mail/browse/MessageCursor$ConversationMessage;

    move-result-object v4

    iget-object v4, v4, Lcom/android/mail/browse/MessageCursor$ConversationMessage;->attachmentListUri:Landroid/net/Uri;

    invoke-virtual {p1}, Lcom/android/mail/browse/ConversationViewAdapter$MessageHeaderItem;->getMessage()Lcom/android/mail/browse/MessageCursor$ConversationMessage;

    move-result-object v5

    iget-object v5, v5, Lcom/android/mail/browse/MessageCursor$ConversationMessage;->attachmentListUri:Landroid/net/Uri;

    invoke-virtual {v4, v5}, Landroid/net/Uri;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_0

    iget-object v4, p0, Lcom/android/mail/browse/MessageFooterView;->mAttachmentGrid:Lcom/android/mail/ui/AttachmentTileGrid;

    invoke-virtual {v4}, Lcom/android/mail/ui/AttachmentTileGrid;->removeAllViewsInLayout()V

    iget-object v4, p0, Lcom/android/mail/browse/MessageFooterView;->mAttachmentBarList:Landroid/widget/LinearLayout;

    invoke-virtual {v4}, Landroid/widget/LinearLayout;->removeAllViewsInLayout()V

    iget-object v4, p0, Lcom/android/mail/browse/MessageFooterView;->mTitleText:Landroid/widget/TextView;

    invoke-virtual {v4, v3}, Landroid/widget/TextView;->setVisibility(I)V

    iget-object v4, p0, Lcom/android/mail/browse/MessageFooterView;->mTitleBar:Landroid/view/View;

    invoke-virtual {v4, v3}, Landroid/view/View;->setVisibility(I)V

    iget-object v4, p0, Lcom/android/mail/browse/MessageFooterView;->mAttachmentGrid:Lcom/android/mail/ui/AttachmentTileGrid;

    invoke-virtual {v4, v3}, Lcom/android/mail/ui/AttachmentTileGrid;->setVisibility(I)V

    iget-object v4, p0, Lcom/android/mail/browse/MessageFooterView;->mAttachmentBarList:Landroid/widget/LinearLayout;

    invoke-virtual {v4, v3}, Landroid/widget/LinearLayout;->setVisibility(I)V

    :cond_0
    invoke-direct {p0}, Lcom/android/mail/browse/MessageFooterView;->getAttachmentLoaderId()Ljava/lang/Integer;

    move-result-object v1

    iput-object p1, p0, Lcom/android/mail/browse/MessageFooterView;->mMessageHeaderItem:Lcom/android/mail/browse/ConversationViewAdapter$MessageHeaderItem;

    invoke-direct {p0}, Lcom/android/mail/browse/MessageFooterView;->getAttachmentLoaderId()Ljava/lang/Integer;

    move-result-object v0

    if-eqz v1, :cond_1

    invoke-static {v1, v0}, Lcom/google/common/base/Objects;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_1

    iget-object v4, p0, Lcom/android/mail/browse/MessageFooterView;->mLoaderManager:Landroid/app/LoaderManager;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v5

    invoke-virtual {v4, v5}, Landroid/app/LoaderManager;->destroyLoader(I)V

    :cond_1
    if-nez p2, :cond_2

    if-eqz v0, :cond_2

    sget-object v4, Lcom/android/mail/browse/MessageFooterView;->LOG_TAG:Ljava/lang/String;

    const-string v5, "binding footer view, calling initLoader for message %d"

    const/4 v6, 0x1

    new-array v6, v6, [Ljava/lang/Object;

    aput-object v0, v6, v2

    invoke-static {v4, v5, v6}, Lcom/android/mail/utils/LogUtils;->i(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)I

    iget-object v4, p0, Lcom/android/mail/browse/MessageFooterView;->mLoaderManager:Landroid/app/LoaderManager;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v5

    sget-object v6, Landroid/os/Bundle;->EMPTY:Landroid/os/Bundle;

    invoke-virtual {v4, v5, v6, p0}, Landroid/app/LoaderManager;->initLoader(ILandroid/os/Bundle;Landroid/app/LoaderManager$LoaderCallbacks;)Landroid/content/Loader;

    :cond_2
    iget-object v4, p0, Lcom/android/mail/browse/MessageFooterView;->mAttachmentGrid:Lcom/android/mail/ui/AttachmentTileGrid;

    invoke-virtual {v4}, Lcom/android/mail/ui/AttachmentTileGrid;->getChildCount()I

    move-result v4

    if-nez v4, :cond_3

    iget-object v4, p0, Lcom/android/mail/browse/MessageFooterView;->mAttachmentBarList:Landroid/widget/LinearLayout;

    invoke-virtual {v4}, Landroid/widget/LinearLayout;->getChildCount()I

    move-result v4

    if-nez v4, :cond_3

    invoke-direct {p0, v2}, Lcom/android/mail/browse/MessageFooterView;->renderAttachments(Z)V

    :cond_3
    iget-object v4, p0, Lcom/android/mail/browse/MessageFooterView;->mMessageHeaderItem:Lcom/android/mail/browse/ConversationViewAdapter$MessageHeaderItem;

    invoke-virtual {v4}, Lcom/android/mail/browse/ConversationViewAdapter$MessageHeaderItem;->isExpanded()Z

    move-result v4

    if-eqz v4, :cond_4

    :goto_0
    invoke-virtual {p0, v2}, Lcom/android/mail/browse/MessageFooterView;->setVisibility(I)V

    return-void

    :cond_4
    move v2, v3

    goto :goto_0
.end method

.method public initialize(Landroid/app/LoaderManager;Landroid/app/FragmentManager;)V
    .locals 0
    .param p1    # Landroid/app/LoaderManager;
    .param p2    # Landroid/app/FragmentManager;

    iput-object p1, p0, Lcom/android/mail/browse/MessageFooterView;->mLoaderManager:Landroid/app/LoaderManager;

    iput-object p2, p0, Lcom/android/mail/browse/MessageFooterView;->mFragmentManager:Landroid/app/FragmentManager;

    return-void
.end method

.method public onCreateLoader(ILandroid/os/Bundle;)Landroid/content/Loader;
    .locals 3
    .param p1    # I
    .param p2    # Landroid/os/Bundle;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Landroid/os/Bundle;",
            ")",
            "Landroid/content/Loader",
            "<",
            "Landroid/database/Cursor;",
            ">;"
        }
    .end annotation

    new-instance v0, Lcom/android/mail/browse/AttachmentLoader;

    invoke-virtual {p0}, Lcom/android/mail/browse/MessageFooterView;->getContext()Landroid/content/Context;

    move-result-object v1

    iget-object v2, p0, Lcom/android/mail/browse/MessageFooterView;->mMessageHeaderItem:Lcom/android/mail/browse/ConversationViewAdapter$MessageHeaderItem;

    invoke-virtual {v2}, Lcom/android/mail/browse/ConversationViewAdapter$MessageHeaderItem;->getMessage()Lcom/android/mail/browse/MessageCursor$ConversationMessage;

    move-result-object v2

    iget-object v2, v2, Lcom/android/mail/browse/MessageCursor$ConversationMessage;->attachmentListUri:Landroid/net/Uri;

    invoke-direct {v0, v1, v2}, Lcom/android/mail/browse/AttachmentLoader;-><init>(Landroid/content/Context;Landroid/net/Uri;)V

    return-object v0
.end method

.method public onDetachedFromParent()V
    .locals 0

    return-void
.end method

.method protected onFinishInflate()V
    .locals 1

    invoke-super {p0}, Landroid/widget/LinearLayout;->onFinishInflate()V

    const v0, 0x7f080081

    invoke-virtual {p0, v0}, Lcom/android/mail/browse/MessageFooterView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/android/mail/browse/MessageFooterView;->mTitleText:Landroid/widget/TextView;

    const v0, 0x7f080082

    invoke-virtual {p0, v0}, Lcom/android/mail/browse/MessageFooterView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/android/mail/browse/MessageFooterView;->mTitleBar:Landroid/view/View;

    const v0, 0x7f080049

    invoke-virtual {p0, v0}, Lcom/android/mail/browse/MessageFooterView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/android/mail/ui/AttachmentTileGrid;

    iput-object v0, p0, Lcom/android/mail/browse/MessageFooterView;->mAttachmentGrid:Lcom/android/mail/ui/AttachmentTileGrid;

    const v0, 0x7f08004a

    invoke-virtual {p0, v0}, Lcom/android/mail/browse/MessageFooterView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/android/mail/browse/MessageFooterView;->mAttachmentBarList:Landroid/widget/LinearLayout;

    return-void
.end method

.method public onLoadFinished(Landroid/content/Loader;Landroid/database/Cursor;)V
    .locals 1
    .param p2    # Landroid/database/Cursor;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Loader",
            "<",
            "Landroid/database/Cursor;",
            ">;",
            "Landroid/database/Cursor;",
            ")V"
        }
    .end annotation

    check-cast p2, Lcom/android/mail/browse/AttachmentLoader$AttachmentCursor;

    iput-object p2, p0, Lcom/android/mail/browse/MessageFooterView;->mAttachmentsCursor:Lcom/android/mail/browse/AttachmentLoader$AttachmentCursor;

    iget-object v0, p0, Lcom/android/mail/browse/MessageFooterView;->mAttachmentsCursor:Lcom/android/mail/browse/AttachmentLoader$AttachmentCursor;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/mail/browse/MessageFooterView;->mAttachmentsCursor:Lcom/android/mail/browse/AttachmentLoader$AttachmentCursor;

    invoke-virtual {v0}, Lcom/android/mail/browse/AttachmentLoader$AttachmentCursor;->isClosed()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/android/mail/browse/MessageFooterView;->renderAttachments(Z)V

    goto :goto_0
.end method

.method public bridge synthetic onLoadFinished(Landroid/content/Loader;Ljava/lang/Object;)V
    .locals 0
    .param p1    # Landroid/content/Loader;
    .param p2    # Ljava/lang/Object;

    check-cast p2, Landroid/database/Cursor;

    invoke-virtual {p0, p1, p2}, Lcom/android/mail/browse/MessageFooterView;->onLoadFinished(Landroid/content/Loader;Landroid/database/Cursor;)V

    return-void
.end method

.method public onLoaderReset(Landroid/content/Loader;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Loader",
            "<",
            "Landroid/database/Cursor;",
            ">;)V"
        }
    .end annotation

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/android/mail/browse/MessageFooterView;->mAttachmentsCursor:Lcom/android/mail/browse/AttachmentLoader$AttachmentCursor;

    return-void
.end method
