.class public Lcom/android/mail/browse/PriorityIndicatorSpan;
.super Landroid/text/style/ReplacementSpan;
.source "PriorityIndicatorSpan.java"


# instance fields
.field private final mContext:Landroid/content/Context;

.field private mDrawableRef:Ljava/lang/ref/WeakReference;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/ref/WeakReference",
            "<",
            "Landroid/graphics/drawable/Drawable;",
            ">;"
        }
    .end annotation
.end field

.field private final mPaddingH:I

.field private final mPaddingV:I

.field private final mResId:I


# direct methods
.method public constructor <init>(Landroid/content/Context;III)V
    .locals 0
    .param p1    # Landroid/content/Context;
    .param p2    # I
    .param p3    # I
    .param p4    # I

    invoke-direct {p0}, Landroid/text/style/ReplacementSpan;-><init>()V

    iput-object p1, p0, Lcom/android/mail/browse/PriorityIndicatorSpan;->mContext:Landroid/content/Context;

    iput p2, p0, Lcom/android/mail/browse/PriorityIndicatorSpan;->mResId:I

    iput p3, p0, Lcom/android/mail/browse/PriorityIndicatorSpan;->mPaddingV:I

    iput p4, p0, Lcom/android/mail/browse/PriorityIndicatorSpan;->mPaddingH:I

    return-void
.end method

.method private getCachedDrawable()Landroid/graphics/drawable/Drawable;
    .locals 3

    iget-object v1, p0, Lcom/android/mail/browse/PriorityIndicatorSpan;->mDrawableRef:Ljava/lang/ref/WeakReference;

    const/4 v0, 0x0

    if-eqz v1, :cond_0

    invoke-virtual {v1}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/graphics/drawable/Drawable;

    :cond_0
    if-nez v0, :cond_1

    invoke-direct {p0}, Lcom/android/mail/browse/PriorityIndicatorSpan;->getDrawable()Landroid/graphics/drawable/Drawable;

    move-result-object v0

    new-instance v2, Ljava/lang/ref/WeakReference;

    invoke-direct {v2, v0}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    iput-object v2, p0, Lcom/android/mail/browse/PriorityIndicatorSpan;->mDrawableRef:Ljava/lang/ref/WeakReference;

    :cond_1
    return-object v0
.end method

.method private getDrawable()Landroid/graphics/drawable/Drawable;
    .locals 4

    const/4 v3, 0x0

    iget-object v1, p0, Lcom/android/mail/browse/PriorityIndicatorSpan;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    iget v2, p0, Lcom/android/mail/browse/PriorityIndicatorSpan;->mResId:I

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    invoke-virtual {v0}, Landroid/graphics/drawable/Drawable;->getIntrinsicWidth()I

    move-result v1

    invoke-virtual {v0}, Landroid/graphics/drawable/Drawable;->getIntrinsicHeight()I

    move-result v2

    invoke-virtual {v0, v3, v3, v1, v2}, Landroid/graphics/drawable/Drawable;->setBounds(IIII)V

    return-object v0
.end method


# virtual methods
.method public draw(Landroid/graphics/Canvas;Ljava/lang/CharSequence;IIFIIILandroid/graphics/Paint;)V
    .locals 4
    .param p1    # Landroid/graphics/Canvas;
    .param p2    # Ljava/lang/CharSequence;
    .param p3    # I
    .param p4    # I
    .param p5    # F
    .param p6    # I
    .param p7    # I
    .param p8    # I
    .param p9    # Landroid/graphics/Paint;

    invoke-direct {p0}, Lcom/android/mail/browse/PriorityIndicatorSpan;->getCachedDrawable()Landroid/graphics/drawable/Drawable;

    move-result-object v0

    invoke-virtual {p1}, Landroid/graphics/Canvas;->save()I

    add-int v2, p6, p8

    invoke-virtual {v0}, Landroid/graphics/drawable/Drawable;->getBounds()Landroid/graphics/Rect;

    move-result-object v3

    iget v3, v3, Landroid/graphics/Rect;->bottom:I

    sub-int/2addr v2, v3

    div-int/lit8 v1, v2, 0x2

    iget v2, p0, Lcom/android/mail/browse/PriorityIndicatorSpan;->mPaddingH:I

    int-to-float v2, v2

    add-float/2addr v2, p5

    int-to-float v3, v1

    invoke-virtual {p1, v2, v3}, Landroid/graphics/Canvas;->translate(FF)V

    invoke-virtual {v0, p1}, Landroid/graphics/drawable/Drawable;->draw(Landroid/graphics/Canvas;)V

    invoke-virtual {p1}, Landroid/graphics/Canvas;->restore()V

    return-void
.end method

.method public getSize(Landroid/graphics/Paint;Ljava/lang/CharSequence;IILandroid/graphics/Paint$FontMetricsInt;)I
    .locals 4
    .param p1    # Landroid/graphics/Paint;
    .param p2    # Ljava/lang/CharSequence;
    .param p3    # I
    .param p4    # I
    .param p5    # Landroid/graphics/Paint$FontMetricsInt;

    invoke-direct {p0}, Lcom/android/mail/browse/PriorityIndicatorSpan;->getCachedDrawable()Landroid/graphics/drawable/Drawable;

    move-result-object v0

    invoke-virtual {v0}, Landroid/graphics/drawable/Drawable;->getBounds()Landroid/graphics/Rect;

    move-result-object v1

    if-eqz p5, :cond_0

    invoke-virtual {p1, p5}, Landroid/graphics/Paint;->getFontMetricsInt(Landroid/graphics/Paint$FontMetricsInt;)I

    iget v2, p5, Landroid/graphics/Paint$FontMetricsInt;->ascent:I

    iget v3, p0, Lcom/android/mail/browse/PriorityIndicatorSpan;->mPaddingV:I

    sub-int/2addr v2, v3

    iput v2, p5, Landroid/graphics/Paint$FontMetricsInt;->ascent:I

    iget v2, p5, Landroid/graphics/Paint$FontMetricsInt;->ascent:I

    iput v2, p5, Landroid/graphics/Paint$FontMetricsInt;->top:I

    iget v2, p5, Landroid/graphics/Paint$FontMetricsInt;->bottom:I

    iget v3, p0, Lcom/android/mail/browse/PriorityIndicatorSpan;->mPaddingV:I

    add-int/2addr v2, v3

    iput v2, p5, Landroid/graphics/Paint$FontMetricsInt;->bottom:I

    iget v2, p5, Landroid/graphics/Paint$FontMetricsInt;->descent:I

    iget v3, p0, Lcom/android/mail/browse/PriorityIndicatorSpan;->mPaddingV:I

    add-int/2addr v2, v3

    iput v2, p5, Landroid/graphics/Paint$FontMetricsInt;->descent:I

    :cond_0
    iget v2, v1, Landroid/graphics/Rect;->right:I

    iget v3, p0, Lcom/android/mail/browse/PriorityIndicatorSpan;->mPaddingH:I

    mul-int/lit8 v3, v3, 0x2

    add-int/2addr v2, v3

    return v2
.end method
