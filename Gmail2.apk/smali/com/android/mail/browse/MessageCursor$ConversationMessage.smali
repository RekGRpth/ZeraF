.class public final Lcom/android/mail/browse/MessageCursor$ConversationMessage;
.super Lcom/android/mail/providers/Message;
.source "MessageCursor.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/mail/browse/MessageCursor;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "ConversationMessage"
.end annotation


# instance fields
.field private transient mController:Lcom/android/mail/browse/MessageCursor$ConversationController;


# direct methods
.method private constructor <init>(Lcom/android/mail/browse/MessageCursor;)V
    .locals 0
    .param p1    # Lcom/android/mail/browse/MessageCursor;

    invoke-direct {p0, p1}, Lcom/android/mail/providers/Message;-><init>(Landroid/database/Cursor;)V

    return-void
.end method

.method synthetic constructor <init>(Lcom/android/mail/browse/MessageCursor;Lcom/android/mail/browse/MessageCursor$1;)V
    .locals 0
    .param p1    # Lcom/android/mail/browse/MessageCursor;
    .param p2    # Lcom/android/mail/browse/MessageCursor$1;

    invoke-direct {p0, p1}, Lcom/android/mail/browse/MessageCursor$ConversationMessage;-><init>(Lcom/android/mail/browse/MessageCursor;)V

    return-void
.end method

.method private getAttachmentsStateHashCode()I
    .locals 5

    const/4 v1, 0x0

    invoke-virtual {p0}, Lcom/android/mail/browse/MessageCursor$ConversationMessage;->getAttachments()Ljava/util/List;

    move-result-object v4

    invoke-interface {v4}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/mail/providers/Attachment;

    invoke-virtual {v0}, Lcom/android/mail/providers/Attachment;->getIdentifierUri()Landroid/net/Uri;

    move-result-object v3

    if-eqz v3, :cond_0

    invoke-virtual {v3}, Landroid/net/Uri;->hashCode()I

    move-result v4

    :goto_1
    add-int/2addr v1, v4

    goto :goto_0

    :cond_0
    const/4 v4, 0x0

    goto :goto_1

    :cond_1
    return v1
.end method


# virtual methods
.method public getConversation()Lcom/android/mail/providers/Conversation;
    .locals 1

    iget-object v0, p0, Lcom/android/mail/browse/MessageCursor$ConversationMessage;->mController:Lcom/android/mail/browse/MessageCursor$ConversationController;

    invoke-interface {v0}, Lcom/android/mail/browse/MessageCursor$ConversationController;->getConversation()Lcom/android/mail/providers/Conversation;

    move-result-object v0

    return-object v0
.end method

.method public getStateHashCode()I
    .locals 3

    const/4 v0, 0x4

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    iget-object v2, p0, Lcom/android/mail/browse/MessageCursor$ConversationMessage;->uri:Landroid/net/Uri;

    aput-object v2, v0, v1

    const/4 v1, 0x1

    iget-boolean v2, p0, Lcom/android/mail/browse/MessageCursor$ConversationMessage;->read:Z

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x2

    iget-boolean v2, p0, Lcom/android/mail/browse/MessageCursor$ConversationMessage;->starred:Z

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x3

    invoke-direct {p0}, Lcom/android/mail/browse/MessageCursor$ConversationMessage;->getAttachmentsStateHashCode()I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v0, v1

    invoke-static {v0}, Lcom/google/common/base/Objects;->hashCode([Ljava/lang/Object;)I

    move-result v0

    return v0
.end method

.method public isConversationStarred()Z
    .locals 2

    iget-object v1, p0, Lcom/android/mail/browse/MessageCursor$ConversationMessage;->mController:Lcom/android/mail/browse/MessageCursor$ConversationController;

    invoke-interface {v1}, Lcom/android/mail/browse/MessageCursor$ConversationController;->getMessageCursor()Lcom/android/mail/browse/MessageCursor;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/android/mail/browse/MessageCursor;->isConversationStarred()Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v1, 0x1

    :goto_0
    return v1

    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public setController(Lcom/android/mail/browse/MessageCursor$ConversationController;)V
    .locals 0
    .param p1    # Lcom/android/mail/browse/MessageCursor$ConversationController;

    iput-object p1, p0, Lcom/android/mail/browse/MessageCursor$ConversationMessage;->mController:Lcom/android/mail/browse/MessageCursor$ConversationController;

    return-void
.end method

.method public star(Z)V
    .locals 2
    .param p1    # Z

    iget-object v1, p0, Lcom/android/mail/browse/MessageCursor$ConversationMessage;->mController:Lcom/android/mail/browse/MessageCursor$ConversationController;

    invoke-interface {v1}, Lcom/android/mail/browse/MessageCursor$ConversationController;->getListController()Lcom/android/mail/ui/ConversationUpdater;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-interface {v0, p0, p1}, Lcom/android/mail/ui/ConversationUpdater;->starMessage(Lcom/android/mail/browse/MessageCursor$ConversationMessage;Z)V

    :cond_0
    return-void
.end method
