.class public abstract Lcom/android/mail/browse/ConversationCursor$ConversationProvider;
.super Landroid/content/ContentProvider;
.source "ConversationCursor.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/mail/browse/ConversationCursor;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x409
    name = "ConversationProvider"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/mail/browse/ConversationCursor$ConversationProvider$ProviderExecute;
    }
.end annotation


# static fields
.field public static AUTHORITY:Ljava/lang/String;


# instance fields
.field private mResolver:Landroid/content/ContentResolver;

.field private mUndoDeleteUris:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Landroid/net/Uri;",
            ">;"
        }
    .end annotation
.end field

.field private mUndoSequence:I


# direct methods
.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Landroid/content/ContentProvider;-><init>()V

    const/4 v0, 0x0

    iput v0, p0, Lcom/android/mail/browse/ConversationCursor$ConversationProvider;->mUndoSequence:I

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/android/mail/browse/ConversationCursor$ConversationProvider;->mUndoDeleteUris:Ljava/util/ArrayList;

    return-void
.end method

.method static synthetic access$1900(Lcom/android/mail/browse/ConversationCursor$ConversationProvider;)Landroid/content/ContentResolver;
    .locals 1
    .param p0    # Lcom/android/mail/browse/ConversationCursor$ConversationProvider;

    iget-object v0, p0, Lcom/android/mail/browse/ConversationCursor$ConversationProvider;->mResolver:Landroid/content/ContentResolver;

    return-object v0
.end method

.method static synthetic access$2000(Lcom/android/mail/browse/ConversationCursor$ConversationProvider;Landroid/net/Uri;Landroid/content/ContentValues;)V
    .locals 0
    .param p0    # Lcom/android/mail/browse/ConversationCursor$ConversationProvider;
    .param p1    # Landroid/net/Uri;
    .param p2    # Landroid/content/ContentValues;

    invoke-direct {p0, p1, p2}, Lcom/android/mail/browse/ConversationCursor$ConversationProvider;->insertLocal(Landroid/net/Uri;Landroid/content/ContentValues;)V

    return-void
.end method

.method private insertLocal(Landroid/net/Uri;Landroid/content/ContentValues;)V
    .locals 0
    .param p1    # Landroid/net/Uri;
    .param p2    # Landroid/content/ContentValues;

    return-void
.end method


# virtual methods
.method addToUndoSequence(Landroid/net/Uri;)V
    .locals 2
    .param p1    # Landroid/net/Uri;

    # getter for: Lcom/android/mail/browse/ConversationCursor;->sSequence:I
    invoke-static {}, Lcom/android/mail/browse/ConversationCursor;->access$1100()I

    move-result v0

    iget v1, p0, Lcom/android/mail/browse/ConversationCursor$ConversationProvider;->mUndoSequence:I

    if-eq v0, v1, :cond_0

    # getter for: Lcom/android/mail/browse/ConversationCursor;->sSequence:I
    invoke-static {}, Lcom/android/mail/browse/ConversationCursor;->access$1100()I

    move-result v0

    iput v0, p0, Lcom/android/mail/browse/ConversationCursor$ConversationProvider;->mUndoSequence:I

    iget-object v0, p0, Lcom/android/mail/browse/ConversationCursor$ConversationProvider;->mUndoDeleteUris:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    :cond_0
    iget-object v0, p0, Lcom/android/mail/browse/ConversationCursor$ConversationProvider;->mUndoDeleteUris:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    return-void
.end method

.method public apply(Ljava/util/Collection;Lcom/android/mail/browse/ConversationCursor;)I
    .locals 12
    .param p2    # Lcom/android/mail/browse/ConversationCursor;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Collection",
            "<",
            "Lcom/android/mail/browse/ConversationCursor$ConversationOperation;",
            ">;",
            "Lcom/android/mail/browse/ConversationCursor;",
            ")I"
        }
    .end annotation

    new-instance v2, Ljava/util/HashMap;

    invoke-direct {v2}, Ljava/util/HashMap;-><init>()V

    # operator++ for: Lcom/android/mail/browse/ConversationCursor;->sSequence:I
    invoke-static {}, Lcom/android/mail/browse/ConversationCursor;->access$1108()I

    const/4 v8, 0x0

    invoke-interface {p1}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :cond_0
    :goto_0
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v10

    if-eqz v10, :cond_3

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lcom/android/mail/browse/ConversationCursor$ConversationOperation;

    # getter for: Lcom/android/mail/browse/ConversationCursor$ConversationOperation;->mUri:Landroid/net/Uri;
    invoke-static {v6}, Lcom/android/mail/browse/ConversationCursor$ConversationOperation;->access$1600(Lcom/android/mail/browse/ConversationCursor$ConversationOperation;)Landroid/net/Uri;

    move-result-object v10

    # invokes: Lcom/android/mail/browse/ConversationCursor;->uriFromCachingUri(Landroid/net/Uri;)Landroid/net/Uri;
    invoke-static {v10}, Lcom/android/mail/browse/ConversationCursor;->access$1000(Landroid/net/Uri;)Landroid/net/Uri;

    move-result-object v9

    invoke-virtual {v9}, Landroid/net/Uri;->getAuthority()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v2, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/ArrayList;

    if-nez v0, :cond_1

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    invoke-virtual {v2, v1, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    :cond_1
    # invokes: Lcom/android/mail/browse/ConversationCursor$ConversationOperation;->execute(Landroid/net/Uri;)Landroid/content/ContentProviderOperation;
    invoke-static {v6, v9}, Lcom/android/mail/browse/ConversationCursor$ConversationOperation;->access$1700(Lcom/android/mail/browse/ConversationCursor$ConversationOperation;Landroid/net/Uri;)Landroid/content/ContentProviderOperation;

    move-result-object v3

    if-eqz v3, :cond_2

    invoke-virtual {v0, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :cond_2
    # getter for: Lcom/android/mail/browse/ConversationCursor$ConversationOperation;->mRecalibrateRequired:Z
    invoke-static {v6}, Lcom/android/mail/browse/ConversationCursor$ConversationOperation;->access$1800(Lcom/android/mail/browse/ConversationCursor$ConversationOperation;)Z

    move-result v10

    if-eqz v10, :cond_0

    const/4 v8, 0x1

    goto :goto_0

    :cond_3
    if-eqz v8, :cond_4

    # invokes: Lcom/android/mail/browse/ConversationCursor;->recalibratePosition()V
    invoke-static {p2}, Lcom/android/mail/browse/ConversationCursor;->access$1400(Lcom/android/mail/browse/ConversationCursor;)V

    :cond_4
    # invokes: Lcom/android/mail/browse/ConversationCursor;->notifyDataChanged()V
    invoke-static {p2}, Lcom/android/mail/browse/ConversationCursor;->access$1500(Lcom/android/mail/browse/ConversationCursor;)V

    invoke-static {}, Lcom/android/mail/browse/ConversationCursor;->offUiThread()Z

    move-result v5

    invoke-virtual {v2}, Ljava/util/HashMap;->keySet()Ljava/util/Set;

    move-result-object v10

    invoke-interface {v10}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :goto_1
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v10

    if-eqz v10, :cond_6

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    invoke-virtual {v2, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Ljava/util/ArrayList;

    if-eqz v5, :cond_5

    :try_start_0
    iget-object v10, p0, Lcom/android/mail/browse/ConversationCursor$ConversationProvider;->mResolver:Landroid/content/ContentResolver;

    invoke-virtual {v10, v1, v7}, Landroid/content/ContentResolver;->applyBatch(Ljava/lang/String;Ljava/util/ArrayList;)[Landroid/content/ContentProviderResult;
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Landroid/content/OperationApplicationException; {:try_start_0 .. :try_end_0} :catch_1

    goto :goto_1

    :catch_0
    move-exception v10

    goto :goto_1

    :cond_5
    new-instance v10, Ljava/lang/Thread;

    new-instance v11, Lcom/android/mail/browse/ConversationCursor$ConversationProvider$1;

    invoke-direct {v11, p0, v1, v7}, Lcom/android/mail/browse/ConversationCursor$ConversationProvider$1;-><init>(Lcom/android/mail/browse/ConversationCursor$ConversationProvider;Ljava/lang/String;Ljava/util/ArrayList;)V

    invoke-direct {v10, v11}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;)V

    invoke-virtual {v10}, Ljava/lang/Thread;->start()V

    goto :goto_1

    :cond_6
    # getter for: Lcom/android/mail/browse/ConversationCursor;->sSequence:I
    invoke-static {}, Lcom/android/mail/browse/ConversationCursor;->access$1100()I

    move-result v10

    return v10

    :catch_1
    move-exception v10

    goto :goto_1
.end method

.method clearMostlyDead(Landroid/net/Uri;Lcom/android/mail/browse/ConversationCursor;)Z
    .locals 2
    .param p1    # Landroid/net/Uri;
    .param p2    # Lcom/android/mail/browse/ConversationCursor;

    # invokes: Lcom/android/mail/browse/ConversationCursor;->uriStringFromCachingUri(Landroid/net/Uri;)Ljava/lang/String;
    invoke-static {p1}, Lcom/android/mail/browse/ConversationCursor;->access$1200(Landroid/net/Uri;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2, v0}, Lcom/android/mail/browse/ConversationCursor;->clearMostlyDead(Ljava/lang/String;)Z

    move-result v1

    return v1
.end method

.method commitMostlyDead(Lcom/android/mail/providers/Conversation;Lcom/android/mail/browse/ConversationCursor;)V
    .locals 0
    .param p1    # Lcom/android/mail/providers/Conversation;
    .param p2    # Lcom/android/mail/browse/ConversationCursor;

    invoke-virtual {p2, p1}, Lcom/android/mail/browse/ConversationCursor;->commitMostlyDead(Lcom/android/mail/providers/Conversation;)V

    return-void
.end method

.method public delete(Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)I
    .locals 2
    .param p1    # Landroid/net/Uri;
    .param p2    # Ljava/lang/String;
    .param p3    # [Ljava/lang/String;

    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Unexpected call to ConversationProvider.delete"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method deleteLocal(Landroid/net/Uri;Lcom/android/mail/browse/ConversationCursor;)V
    .locals 3
    .param p1    # Landroid/net/Uri;
    .param p2    # Lcom/android/mail/browse/ConversationCursor;

    # invokes: Lcom/android/mail/browse/ConversationCursor;->uriStringFromCachingUri(Landroid/net/Uri;)Ljava/lang/String;
    invoke-static {p1}, Lcom/android/mail/browse/ConversationCursor;->access$1200(Landroid/net/Uri;)Ljava/lang/String;

    move-result-object v0

    const-string v1, "__deleted__"

    const/4 v2, 0x1

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    # invokes: Lcom/android/mail/browse/ConversationCursor;->cacheValue(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Object;)V
    invoke-static {p2, v0, v1, v2}, Lcom/android/mail/browse/ConversationCursor;->access$1300(Lcom/android/mail/browse/ConversationCursor;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Object;)V

    invoke-virtual {p0, p1}, Lcom/android/mail/browse/ConversationCursor$ConversationProvider;->addToUndoSequence(Landroid/net/Uri;)V

    return-void
.end method

.method protected abstract getAuthority()Ljava/lang/String;
.end method

.method public getType(Landroid/net/Uri;)Ljava/lang/String;
    .locals 1
    .param p1    # Landroid/net/Uri;

    const/4 v0, 0x0

    return-object v0
.end method

.method public insert(Landroid/net/Uri;Landroid/content/ContentValues;)Landroid/net/Uri;
    .locals 1
    .param p1    # Landroid/net/Uri;
    .param p2    # Landroid/content/ContentValues;

    invoke-direct {p0, p1, p2}, Lcom/android/mail/browse/ConversationCursor$ConversationProvider;->insertLocal(Landroid/net/Uri;Landroid/content/ContentValues;)V

    iget-object v0, p0, Lcom/android/mail/browse/ConversationCursor$ConversationProvider;->mResolver:Landroid/content/ContentResolver;

    invoke-static {v0, p1, p2}, Lcom/android/mail/browse/ConversationCursor$ConversationProvider$ProviderExecute;->opInsert(Landroid/content/ContentResolver;Landroid/net/Uri;Landroid/content/ContentValues;)Landroid/net/Uri;

    move-result-object v0

    return-object v0
.end method

.method public onCreate()Z
    .locals 1

    sput-object p0, Lcom/android/mail/browse/ConversationCursor;->sProvider:Lcom/android/mail/browse/ConversationCursor$ConversationProvider;

    invoke-virtual {p0}, Lcom/android/mail/browse/ConversationCursor$ConversationProvider;->getAuthority()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/android/mail/browse/ConversationCursor$ConversationProvider;->AUTHORITY:Ljava/lang/String;

    invoke-virtual {p0}, Lcom/android/mail/browse/ConversationCursor$ConversationProvider;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    iput-object v0, p0, Lcom/android/mail/browse/ConversationCursor$ConversationProvider;->mResolver:Landroid/content/ContentResolver;

    const/4 v0, 0x1

    return v0
.end method

.method public query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    .locals 6
    .param p1    # Landroid/net/Uri;
    .param p2    # [Ljava/lang/String;
    .param p3    # Ljava/lang/String;
    .param p4    # [Ljava/lang/String;
    .param p5    # Ljava/lang/String;

    iget-object v0, p0, Lcom/android/mail/browse/ConversationCursor$ConversationProvider;->mResolver:Landroid/content/ContentResolver;

    # invokes: Lcom/android/mail/browse/ConversationCursor;->uriFromCachingUri(Landroid/net/Uri;)Landroid/net/Uri;
    invoke-static {p1}, Lcom/android/mail/browse/ConversationCursor;->access$1000(Landroid/net/Uri;)Landroid/net/Uri;

    move-result-object v1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    move-object v5, p5

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v0

    return-object v0
.end method

.method setMostlyDead(Lcom/android/mail/providers/Conversation;Lcom/android/mail/browse/ConversationCursor;)V
    .locals 2
    .param p1    # Lcom/android/mail/providers/Conversation;
    .param p2    # Lcom/android/mail/browse/ConversationCursor;

    iget-object v0, p1, Lcom/android/mail/providers/Conversation;->uri:Landroid/net/Uri;

    # invokes: Lcom/android/mail/browse/ConversationCursor;->uriStringFromCachingUri(Landroid/net/Uri;)Ljava/lang/String;
    invoke-static {v0}, Lcom/android/mail/browse/ConversationCursor;->access$1200(Landroid/net/Uri;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p2, v1, p1}, Lcom/android/mail/browse/ConversationCursor;->setMostlyDead(Ljava/lang/String;Lcom/android/mail/providers/Conversation;)V

    invoke-virtual {p0, v0}, Lcom/android/mail/browse/ConversationCursor$ConversationProvider;->addToUndoSequence(Landroid/net/Uri;)V

    return-void
.end method

.method undeleteLocal(Landroid/net/Uri;Lcom/android/mail/browse/ConversationCursor;)V
    .locals 3
    .param p1    # Landroid/net/Uri;
    .param p2    # Lcom/android/mail/browse/ConversationCursor;

    # invokes: Lcom/android/mail/browse/ConversationCursor;->uriStringFromCachingUri(Landroid/net/Uri;)Ljava/lang/String;
    invoke-static {p1}, Lcom/android/mail/browse/ConversationCursor;->access$1200(Landroid/net/Uri;)Ljava/lang/String;

    move-result-object v0

    const-string v1, "__deleted__"

    const/4 v2, 0x0

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    # invokes: Lcom/android/mail/browse/ConversationCursor;->cacheValue(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Object;)V
    invoke-static {p2, v0, v1, v2}, Lcom/android/mail/browse/ConversationCursor;->access$1300(Lcom/android/mail/browse/ConversationCursor;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Object;)V

    return-void
.end method

.method public undo(Lcom/android/mail/browse/ConversationCursor;)V
    .locals 4
    .param p1    # Lcom/android/mail/browse/ConversationCursor;

    # getter for: Lcom/android/mail/browse/ConversationCursor;->sSequence:I
    invoke-static {}, Lcom/android/mail/browse/ConversationCursor;->access$1100()I

    move-result v2

    iget v3, p0, Lcom/android/mail/browse/ConversationCursor$ConversationProvider;->mUndoSequence:I

    if-ne v2, v3, :cond_2

    iget-object v2, p0, Lcom/android/mail/browse/ConversationCursor$ConversationProvider;->mUndoDeleteUris:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :cond_0
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/net/Uri;

    invoke-virtual {p0, v1, p1}, Lcom/android/mail/browse/ConversationCursor$ConversationProvider;->clearMostlyDead(Landroid/net/Uri;Lcom/android/mail/browse/ConversationCursor;)Z

    move-result v2

    if-nez v2, :cond_0

    invoke-virtual {p0, v1, p1}, Lcom/android/mail/browse/ConversationCursor$ConversationProvider;->undeleteLocal(Landroid/net/Uri;Lcom/android/mail/browse/ConversationCursor;)V

    goto :goto_0

    :cond_1
    const/4 v2, 0x0

    iput v2, p0, Lcom/android/mail/browse/ConversationCursor$ConversationProvider;->mUndoSequence:I

    # invokes: Lcom/android/mail/browse/ConversationCursor;->recalibratePosition()V
    invoke-static {p1}, Lcom/android/mail/browse/ConversationCursor;->access$1400(Lcom/android/mail/browse/ConversationCursor;)V

    # invokes: Lcom/android/mail/browse/ConversationCursor;->notifyDataChanged()V
    invoke-static {p1}, Lcom/android/mail/browse/ConversationCursor;->access$1500(Lcom/android/mail/browse/ConversationCursor;)V

    :cond_2
    return-void
.end method

.method public update(Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I
    .locals 2
    .param p1    # Landroid/net/Uri;
    .param p2    # Landroid/content/ContentValues;
    .param p3    # Ljava/lang/String;
    .param p4    # [Ljava/lang/String;

    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Unexpected call to ConversationProvider.update"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method updateLocal(Landroid/net/Uri;Landroid/content/ContentValues;Lcom/android/mail/browse/ConversationCursor;)V
    .locals 4
    .param p1    # Landroid/net/Uri;
    .param p2    # Landroid/content/ContentValues;
    .param p3    # Lcom/android/mail/browse/ConversationCursor;

    if-nez p2, :cond_1

    :cond_0
    return-void

    :cond_1
    # invokes: Lcom/android/mail/browse/ConversationCursor;->uriStringFromCachingUri(Landroid/net/Uri;)Ljava/lang/String;
    invoke-static {p1}, Lcom/android/mail/browse/ConversationCursor;->access$1200(Landroid/net/Uri;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p2}, Landroid/content/ContentValues;->keySet()Ljava/util/Set;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-virtual {p2, v0}, Landroid/content/ContentValues;->get(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v3

    # invokes: Lcom/android/mail/browse/ConversationCursor;->cacheValue(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Object;)V
    invoke-static {p3, v2, v0, v3}, Lcom/android/mail/browse/ConversationCursor;->access$1300(Lcom/android/mail/browse/ConversationCursor;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Object;)V

    goto :goto_0
.end method
