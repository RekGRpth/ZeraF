.class public Lcom/android/mail/browse/MessageHeaderView;
.super Landroid/widget/LinearLayout;
.source "MessageHeaderView.java"

# interfaces
.implements Landroid/view/View$OnClickListener;
.implements Landroid/view/View$OnLongClickListener;
.implements Landroid/widget/PopupMenu$OnMenuItemClickListener;
.implements Lcom/android/mail/browse/ConversationContainer$DetachListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/mail/browse/MessageHeaderView$RecipientListsBuilder;,
        Lcom/android/mail/browse/MessageHeaderView$MessageHeaderViewCallbacks;
    }
.end annotation


# static fields
.field private static final LOG_TAG:Ljava/lang/String;


# instance fields
.field private mAccountController:Lcom/android/mail/browse/ConversationAccountController;

.field private mAddressCache:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Lcom/android/mail/providers/Address;",
            ">;"
        }
    .end annotation
.end field

.field private mAttachmentIcon:Landroid/view/View;

.field private mBcc:[Ljava/lang/String;

.field private mBottomBorderView:Landroid/view/View;

.field private mCallbacks:Lcom/android/mail/browse/MessageHeaderView$MessageHeaderViewCallbacks;

.field private mCc:[Ljava/lang/String;

.field private mCollapsedDetailsValid:Z

.field private mCollapsedDetailsView:Landroid/view/ViewGroup;

.field private mCollapsedStarVisible:Z

.field private final mContactInfoObserver:Landroid/database/DataSetObserver;

.field private mContactInfoSource:Lcom/android/mail/ContactInfoSource;

.field private mCopyAddress:Ljava/lang/String;

.field private mDateBuilder:Lcom/android/mail/FormattedDateBuilder;

.field private mDetailsPopup:Landroid/app/AlertDialog;

.field private mDraftIcon:Landroid/view/View;

.field private mEditDraftButton:Landroid/view/View;

.field private mEmailCopyPopup:Landroid/app/Dialog;

.field private mExpandMode:I

.field private mExpandable:Z

.field private mExpandedDetailsValid:Z

.field private mExpandedDetailsView:Landroid/view/ViewGroup;

.field private mForwardButton:Landroid/view/View;

.field private mFrom:[Ljava/lang/String;

.field private mImagePromptView:Landroid/view/ViewGroup;

.field private final mInflater:Landroid/view/LayoutInflater;

.field private mInviteView:Lcom/android/mail/browse/MessageInviteView;

.field private mIsDraft:Z

.field private mIsSending:Z

.field private mIsSnappy:Z

.field private mLeftSpacer:Landroid/view/View;

.field private mMessage:Lcom/android/mail/browse/MessageCursor$ConversationMessage;

.field private mMessageHeaderItem:Lcom/android/mail/browse/ConversationViewAdapter$MessageHeaderItem;

.field private final mMyName:Ljava/lang/String;

.field private mObservingContactInfo:Z

.field private mOverflowButton:Landroid/view/View;

.field private mPhotoSpacerView:Landroid/view/View;

.field private mPhotoView:Landroid/widget/QuickContactBadge;

.field private mPopup:Landroid/widget/PopupMenu;

.field private mPreMeasuring:Z

.field private mPresenceView:Landroid/widget/ImageView;

.field private mQueryHandler:Landroid/content/AsyncQueryHandler;

.field private mReplyAllButton:Landroid/view/View;

.field private mReplyButton:Landroid/view/View;

.field private mReplyTo:[Ljava/lang/String;

.field private mRightSpacer:Landroid/view/View;

.field private mSender:Lcom/android/mail/providers/Address;

.field private mSenderEmailView:Landroid/widget/TextView;

.field private mSenderNameView:Landroid/widget/TextView;

.field private mShowImagePrompt:Z

.field private mSnippet:Ljava/lang/String;

.field private mSpamWarningView:Lcom/android/mail/browse/SpamWarningView;

.field private mStarShown:Z

.field private mStarView:Landroid/widget/ImageView;

.field private mTimestampMs:J

.field private mTimestampShort:Ljava/lang/CharSequence;

.field private mTitleContainerCollapsedMarginRight:I

.field private mTitleContainerView:Landroid/view/ViewGroup;

.field private mTo:[Ljava/lang/String;

.field private mUpperDateView:Landroid/widget/TextView;

.field private mUpperHeaderView:Landroid/view/ViewGroup;

.field private mVeiledMatcher:Lcom/android/mail/utils/VeiledAddressMatcher;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    invoke-static {}, Lcom/android/mail/utils/LogTag;->getLogTag()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/android/mail/browse/MessageHeaderView;->LOG_TAG:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1    # Landroid/content/Context;

    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/android/mail/browse/MessageHeaderView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/util/AttributeSet;

    const/4 v0, -0x1

    invoke-direct {p0, p1, p2, v0}, Lcom/android/mail/browse/MessageHeaderView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 2
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/util/AttributeSet;
    .param p3    # I

    const/4 v1, 0x0

    invoke-direct {p0, p1, p2, p3}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    iput-boolean v1, p0, Lcom/android/mail/browse/MessageHeaderView;->mIsDraft:Z

    new-instance v0, Lcom/android/mail/browse/MessageHeaderView$1;

    invoke-direct {v0, p0}, Lcom/android/mail/browse/MessageHeaderView$1;-><init>(Lcom/android/mail/browse/MessageHeaderView;)V

    iput-object v0, p0, Lcom/android/mail/browse/MessageHeaderView;->mContactInfoObserver:Landroid/database/DataSetObserver;

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/android/mail/browse/MessageHeaderView;->mExpandable:Z

    iput v1, p0, Lcom/android/mail/browse/MessageHeaderView;->mExpandMode:I

    invoke-static {p1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    iput-object v0, p0, Lcom/android/mail/browse/MessageHeaderView;->mInflater:Landroid/view/LayoutInflater;

    const v0, 0x7f0900a9

    invoke-virtual {p1, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/android/mail/browse/MessageHeaderView;->mMyName:Ljava/lang/String;

    return-void
.end method

.method static synthetic access$000(Lcom/android/mail/browse/MessageHeaderView;)V
    .locals 0
    .param p0    # Lcom/android/mail/browse/MessageHeaderView;

    invoke-direct {p0}, Lcom/android/mail/browse/MessageHeaderView;->updateContactInfo()V

    return-void
.end method

.method static synthetic access$100(Ljava/util/Map;Ljava/lang/String;)Lcom/android/mail/providers/Address;
    .locals 1
    .param p0    # Ljava/util/Map;
    .param p1    # Ljava/lang/String;

    invoke-static {p0, p1}, Lcom/android/mail/browse/MessageHeaderView;->getAddress(Ljava/util/Map;Ljava/lang/String;)Lcom/android/mail/providers/Address;

    move-result-object v0

    return-object v0
.end method

.method private ensureExpandedDetailsView()Z
    .locals 13

    const/4 v12, 0x1

    const/4 v4, 0x0

    iget-object v0, p0, Lcom/android/mail/browse/MessageHeaderView;->mExpandedDetailsView:Landroid/view/ViewGroup;

    if-nez v0, :cond_3

    iget-object v0, p0, Lcom/android/mail/browse/MessageHeaderView;->mInflater:Landroid/view/LayoutInflater;

    const v1, 0x7f040026

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2, v4}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    check-cast v0, Landroid/view/ViewGroup;

    iput-object v0, p0, Lcom/android/mail/browse/MessageHeaderView;->mExpandedDetailsView:Landroid/view/ViewGroup;

    move v11, v12

    :goto_0
    iget-boolean v0, p0, Lcom/android/mail/browse/MessageHeaderView;->mExpandedDetailsValid:Z

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/android/mail/browse/MessageHeaderView;->mMessageHeaderItem:Lcom/android/mail/browse/ConversationViewAdapter$MessageHeaderItem;

    iget-object v0, v0, Lcom/android/mail/browse/ConversationViewAdapter$MessageHeaderItem;->timestampLong:Ljava/lang/CharSequence;

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/android/mail/browse/MessageHeaderView;->mMessageHeaderItem:Lcom/android/mail/browse/ConversationViewAdapter$MessageHeaderItem;

    iget-object v1, p0, Lcom/android/mail/browse/MessageHeaderView;->mDateBuilder:Lcom/android/mail/FormattedDateBuilder;

    iget-wide v2, p0, Lcom/android/mail/browse/MessageHeaderView;->mTimestampMs:J

    invoke-virtual {v1, v2, v3}, Lcom/android/mail/FormattedDateBuilder;->formatLongDateTime(J)Ljava/lang/CharSequence;

    move-result-object v1

    iput-object v1, v0, Lcom/android/mail/browse/ConversationViewAdapter$MessageHeaderItem;->timestampLong:Ljava/lang/CharSequence;

    :cond_0
    iget-object v0, p0, Lcom/android/mail/browse/MessageHeaderView;->mExpandedDetailsView:Landroid/view/ViewGroup;

    const v1, 0x7f080070

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iget-object v1, p0, Lcom/android/mail/browse/MessageHeaderView;->mMessageHeaderItem:Lcom/android/mail/browse/ConversationViewAdapter$MessageHeaderItem;

    iget-object v1, v1, Lcom/android/mail/browse/ConversationViewAdapter$MessageHeaderItem;->timestampLong:Ljava/lang/CharSequence;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    const v1, 0x7f080074

    const v2, 0x7f080076

    iget-object v3, p0, Lcom/android/mail/browse/MessageHeaderView;->mReplyTo:[Ljava/lang/String;

    iget-object v5, p0, Lcom/android/mail/browse/MessageHeaderView;->mExpandedDetailsView:Landroid/view/ViewGroup;

    move-object v0, p0

    invoke-direct/range {v0 .. v5}, Lcom/android/mail/browse/MessageHeaderView;->renderEmailList(II[Ljava/lang/String;ZLandroid/view/View;)V

    const v6, 0x7f080071

    const v7, 0x7f080073

    iget-object v8, p0, Lcom/android/mail/browse/MessageHeaderView;->mFrom:[Ljava/lang/String;

    iget-object v0, p0, Lcom/android/mail/browse/MessageHeaderView;->mMessage:Lcom/android/mail/browse/MessageCursor$ConversationMessage;

    iget-object v0, v0, Lcom/android/mail/browse/MessageCursor$ConversationMessage;->viaDomain:Ljava/lang/String;

    if-eqz v0, :cond_2

    move v9, v12

    :goto_1
    iget-object v10, p0, Lcom/android/mail/browse/MessageHeaderView;->mExpandedDetailsView:Landroid/view/ViewGroup;

    move-object v5, p0

    invoke-direct/range {v5 .. v10}, Lcom/android/mail/browse/MessageHeaderView;->renderEmailList(II[Ljava/lang/String;ZLandroid/view/View;)V

    const v1, 0x7f080077

    const v2, 0x7f080079

    iget-object v3, p0, Lcom/android/mail/browse/MessageHeaderView;->mTo:[Ljava/lang/String;

    iget-object v5, p0, Lcom/android/mail/browse/MessageHeaderView;->mExpandedDetailsView:Landroid/view/ViewGroup;

    move-object v0, p0

    invoke-direct/range {v0 .. v5}, Lcom/android/mail/browse/MessageHeaderView;->renderEmailList(II[Ljava/lang/String;ZLandroid/view/View;)V

    const v1, 0x7f08007a

    const v2, 0x7f08007c

    iget-object v3, p0, Lcom/android/mail/browse/MessageHeaderView;->mCc:[Ljava/lang/String;

    iget-object v5, p0, Lcom/android/mail/browse/MessageHeaderView;->mExpandedDetailsView:Landroid/view/ViewGroup;

    move-object v0, p0

    invoke-direct/range {v0 .. v5}, Lcom/android/mail/browse/MessageHeaderView;->renderEmailList(II[Ljava/lang/String;ZLandroid/view/View;)V

    const v1, 0x7f08007d

    const v2, 0x7f08007f

    iget-object v3, p0, Lcom/android/mail/browse/MessageHeaderView;->mBcc:[Ljava/lang/String;

    iget-object v5, p0, Lcom/android/mail/browse/MessageHeaderView;->mExpandedDetailsView:Landroid/view/ViewGroup;

    move-object v0, p0

    invoke-direct/range {v0 .. v5}, Lcom/android/mail/browse/MessageHeaderView;->renderEmailList(II[Ljava/lang/String;ZLandroid/view/View;)V

    iput-boolean v12, p0, Lcom/android/mail/browse/MessageHeaderView;->mExpandedDetailsValid:Z

    :cond_1
    return v11

    :cond_2
    move v9, v4

    goto :goto_1

    :cond_3
    move v11, v4

    goto :goto_0
.end method

.method private getAccount()Lcom/android/mail/providers/Account;
    .locals 1

    iget-object v0, p0, Lcom/android/mail/browse/MessageHeaderView;->mAccountController:Lcom/android/mail/browse/ConversationAccountController;

    invoke-interface {v0}, Lcom/android/mail/browse/ConversationAccountController;->getAccount()Lcom/android/mail/providers/Account;

    move-result-object v0

    return-object v0
.end method

.method private getAddress(Ljava/lang/String;)Lcom/android/mail/providers/Address;
    .locals 1
    .param p1    # Ljava/lang/String;

    iget-object v0, p0, Lcom/android/mail/browse/MessageHeaderView;->mAddressCache:Ljava/util/Map;

    invoke-static {v0, p1}, Lcom/android/mail/browse/MessageHeaderView;->getAddress(Ljava/util/Map;Ljava/lang/String;)Lcom/android/mail/providers/Address;

    move-result-object v0

    return-object v0
.end method

.method private static getAddress(Ljava/util/Map;Ljava/lang/String;)Lcom/android/mail/providers/Address;
    .locals 1
    .param p1    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Lcom/android/mail/providers/Address;",
            ">;",
            "Ljava/lang/String;",
            ")",
            "Lcom/android/mail/providers/Address;"
        }
    .end annotation

    const/4 v0, 0x0

    if-eqz p0, :cond_0

    invoke-interface {p0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/mail/providers/Address;

    :cond_0
    if-nez v0, :cond_1

    invoke-static {p1}, Lcom/android/mail/providers/Address;->getEmailAddress(Ljava/lang/String;)Lcom/android/mail/providers/Address;

    move-result-object v0

    if-eqz p0, :cond_1

    invoke-interface {p0, p1, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    :cond_1
    return-object v0
.end method

.method private getHeaderSubtitle()Ljava/lang/CharSequence;
    .locals 6

    iget-boolean v1, p0, Lcom/android/mail/browse/MessageHeaderView;->mIsSending:Z

    if-eqz v1, :cond_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_0
    invoke-virtual {p0}, Lcom/android/mail/browse/MessageHeaderView;->isExpanded()Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/android/mail/browse/MessageHeaderView;->mMessage:Lcom/android/mail/browse/MessageCursor$ConversationMessage;

    iget-object v1, v1, Lcom/android/mail/browse/MessageCursor$ConversationMessage;->viaDomain:Ljava/lang/String;

    if-eqz v1, :cond_1

    invoke-virtual {p0}, Lcom/android/mail/browse/MessageHeaderView;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f090104

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    iget-object v5, p0, Lcom/android/mail/browse/MessageHeaderView;->mMessage:Lcom/android/mail/browse/MessageCursor$ConversationMessage;

    iget-object v5, v5, Lcom/android/mail/browse/MessageCursor$ConversationMessage;->viaDomain:Ljava/lang/String;

    aput-object v5, v3, v4

    invoke-virtual {v1, v2, v3}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    :cond_1
    iget-object v1, p0, Lcom/android/mail/browse/MessageHeaderView;->mSender:Lcom/android/mail/providers/Address;

    invoke-static {v1}, Lcom/android/mail/browse/MessageHeaderView;->getSenderAddress(Lcom/android/mail/providers/Address;)Ljava/lang/CharSequence;

    move-result-object v0

    goto :goto_0

    :cond_2
    iget-object v0, p0, Lcom/android/mail/browse/MessageHeaderView;->mSnippet:Ljava/lang/String;

    goto :goto_0
.end method

.method private getHeaderTitle()Ljava/lang/CharSequence;
    .locals 4

    iget-boolean v1, p0, Lcom/android/mail/browse/MessageHeaderView;->mIsDraft:Z

    if-eqz v1, :cond_0

    invoke-virtual {p0}, Lcom/android/mail/browse/MessageHeaderView;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f100003

    const/4 v3, 0x1

    invoke-virtual {v1, v2, v3}, Landroid/content/res/Resources;->getQuantityText(II)Ljava/lang/CharSequence;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    iget-boolean v1, p0, Lcom/android/mail/browse/MessageHeaderView;->mIsSending:Z

    if-eqz v1, :cond_1

    invoke-virtual {p0}, Lcom/android/mail/browse/MessageHeaderView;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0900a7

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    :cond_1
    iget-object v1, p0, Lcom/android/mail/browse/MessageHeaderView;->mSender:Lcom/android/mail/providers/Address;

    invoke-static {v1}, Lcom/android/mail/browse/MessageHeaderView;->getSenderName(Lcom/android/mail/providers/Address;)Ljava/lang/CharSequence;

    move-result-object v0

    goto :goto_0
.end method

.method private getQueryHandler()Landroid/content/AsyncQueryHandler;
    .locals 2

    iget-object v0, p0, Lcom/android/mail/browse/MessageHeaderView;->mQueryHandler:Landroid/content/AsyncQueryHandler;

    if-nez v0, :cond_0

    new-instance v0, Lcom/android/mail/browse/MessageHeaderView$2;

    invoke-virtual {p0}, Lcom/android/mail/browse/MessageHeaderView;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    invoke-direct {v0, p0, v1}, Lcom/android/mail/browse/MessageHeaderView$2;-><init>(Lcom/android/mail/browse/MessageHeaderView;Landroid/content/ContentResolver;)V

    iput-object v0, p0, Lcom/android/mail/browse/MessageHeaderView;->mQueryHandler:Landroid/content/AsyncQueryHandler;

    :cond_0
    iget-object v0, p0, Lcom/android/mail/browse/MessageHeaderView;->mQueryHandler:Landroid/content/AsyncQueryHandler;

    return-object v0
.end method

.method static getRecipientSummaryText(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;[Ljava/lang/String;[Ljava/lang/String;Ljava/util/Map;Lcom/android/mail/utils/VeiledAddressMatcher;)Ljava/lang/CharSequence;
    .locals 6
    .param p0    # Landroid/content/Context;
    .param p1    # Ljava/lang/String;
    .param p2    # Ljava/lang/String;
    .param p3    # [Ljava/lang/String;
    .param p4    # [Ljava/lang/String;
    .param p5    # [Ljava/lang/String;
    .param p7    # Lcom/android/mail/utils/VeiledAddressMatcher;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "[",
            "Ljava/lang/String;",
            "[",
            "Ljava/lang/String;",
            "[",
            "Ljava/lang/String;",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Lcom/android/mail/providers/Address;",
            ">;",
            "Lcom/android/mail/utils/VeiledAddressMatcher;",
            ")",
            "Ljava/lang/CharSequence;"
        }
    .end annotation

    new-instance v0, Lcom/android/mail/browse/MessageHeaderView$RecipientListsBuilder;

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p6

    move-object v5, p7

    invoke-direct/range {v0 .. v5}, Lcom/android/mail/browse/MessageHeaderView$RecipientListsBuilder;-><init>(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/util/Map;Lcom/android/mail/utils/VeiledAddressMatcher;)V

    const v1, 0x7f0900cc

    invoke-virtual {v0, p3, v1}, Lcom/android/mail/browse/MessageHeaderView$RecipientListsBuilder;->append([Ljava/lang/String;I)V

    const v1, 0x7f0900cd

    invoke-virtual {v0, p4, v1}, Lcom/android/mail/browse/MessageHeaderView$RecipientListsBuilder;->append([Ljava/lang/String;I)V

    const v1, 0x7f0900ce

    invoke-virtual {v0, p5, v1}, Lcom/android/mail/browse/MessageHeaderView$RecipientListsBuilder;->append([Ljava/lang/String;I)V

    invoke-virtual {v0}, Lcom/android/mail/browse/MessageHeaderView$RecipientListsBuilder;->build()Ljava/lang/CharSequence;

    move-result-object v1

    return-object v1
.end method

.method private static getSenderAddress(Lcom/android/mail/providers/Address;)Ljava/lang/CharSequence;
    .locals 2
    .param p0    # Lcom/android/mail/providers/Address;

    if-nez p0, :cond_0

    const-string v0, ""

    :goto_0
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_1

    const/4 v1, 0x0

    :goto_1
    return-object v1

    :cond_0
    invoke-virtual {p0}, Lcom/android/mail/providers/Address;->getName()Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    :cond_1
    invoke-virtual {p0}, Lcom/android/mail/providers/Address;->getAddress()Ljava/lang/String;

    move-result-object v1

    goto :goto_1
.end method

.method private static getSenderName(Lcom/android/mail/providers/Address;)Ljava/lang/CharSequence;
    .locals 2
    .param p0    # Lcom/android/mail/providers/Address;

    invoke-virtual {p0}, Lcom/android/mail/providers/Address;->getName()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-virtual {p0}, Lcom/android/mail/providers/Address;->getAddress()Ljava/lang/String;

    move-result-object v0

    :cond_0
    return-object v0
.end method

.method private handleShowImagePromptClick(Landroid/view/View;)V
    .locals 4

    const/4 v3, 0x0

    const/4 v2, 0x0

    invoke-virtual {p1}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    if-nez v0, :cond_0

    :goto_0
    return-void

    :cond_0
    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    goto :goto_0

    :pswitch_0
    iget-object v0, p0, Lcom/android/mail/browse/MessageHeaderView;->mCallbacks:Lcom/android/mail/browse/MessageHeaderView$MessageHeaderViewCallbacks;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/android/mail/browse/MessageHeaderView;->mCallbacks:Lcom/android/mail/browse/MessageHeaderView$MessageHeaderViewCallbacks;

    iget-object v1, p0, Lcom/android/mail/browse/MessageHeaderView;->mMessage:Lcom/android/mail/browse/MessageCursor$ConversationMessage;

    invoke-interface {v0, v1}, Lcom/android/mail/browse/MessageHeaderView$MessageHeaderViewCallbacks;->showExternalResources(Lcom/android/mail/providers/Message;)V

    :cond_1
    iget-object v0, p0, Lcom/android/mail/browse/MessageHeaderView;->mMessageHeaderItem:Lcom/android/mail/browse/ConversationViewAdapter$MessageHeaderItem;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/android/mail/browse/MessageHeaderView;->mMessageHeaderItem:Lcom/android/mail/browse/ConversationViewAdapter$MessageHeaderItem;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/android/mail/browse/ConversationViewAdapter$MessageHeaderItem;->setShowImages(Z)V

    :cond_2
    invoke-direct {p0, v2}, Lcom/android/mail/browse/MessageHeaderView;->showImagePromptAlways(Z)V

    goto :goto_0

    :pswitch_1
    iget-object v0, p0, Lcom/android/mail/browse/MessageHeaderView;->mMessage:Lcom/android/mail/browse/MessageCursor$ConversationMessage;

    invoke-direct {p0}, Lcom/android/mail/browse/MessageHeaderView;->getQueryHandler()Landroid/content/AsyncQueryHandler;

    move-result-object v1

    invoke-virtual {v0, v1, v2, v3}, Lcom/android/mail/browse/MessageCursor$ConversationMessage;->markAlwaysShowImages(Landroid/content/AsyncQueryHandler;ILjava/lang/Object;)V

    iget-object v0, p0, Lcom/android/mail/browse/MessageHeaderView;->mCallbacks:Lcom/android/mail/browse/MessageHeaderView$MessageHeaderViewCallbacks;

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/android/mail/browse/MessageHeaderView;->mCallbacks:Lcom/android/mail/browse/MessageHeaderView$MessageHeaderViewCallbacks;

    iget-object v1, p0, Lcom/android/mail/browse/MessageHeaderView;->mMessage:Lcom/android/mail/browse/MessageCursor$ConversationMessage;

    invoke-virtual {v1}, Lcom/android/mail/browse/MessageCursor$ConversationMessage;->getFrom()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/android/mail/browse/MessageHeaderView$MessageHeaderViewCallbacks;->showExternalResources(Ljava/lang/String;)V

    :cond_3
    iput-boolean v2, p0, Lcom/android/mail/browse/MessageHeaderView;->mShowImagePrompt:Z

    invoke-virtual {p1, v3}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    const/16 v0, 0x8

    invoke-virtual {p1, v0}, Landroid/view/View;->setVisibility(I)V

    invoke-direct {p0}, Lcom/android/mail/browse/MessageHeaderView;->updateSpacerHeight()V

    invoke-virtual {p0}, Lcom/android/mail/browse/MessageHeaderView;->getContext()Landroid/content/Context;

    move-result-object v0

    const v1, 0x7f0900d1

    invoke-static {v0, v1, v2}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method private hideCollapsedDetails()V
    .locals 2

    iget-object v0, p0, Lcom/android/mail/browse/MessageHeaderView;->mCollapsedDetailsView:Landroid/view/ViewGroup;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/mail/browse/MessageHeaderView;->mCollapsedDetailsView:Landroid/view/ViewGroup;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->setVisibility(I)V

    :cond_0
    return-void
.end method

.method private hideDetailsPopup()V
    .locals 1

    iget-object v0, p0, Lcom/android/mail/browse/MessageHeaderView;->mDetailsPopup:Landroid/app/AlertDialog;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/mail/browse/MessageHeaderView;->mDetailsPopup:Landroid/app/AlertDialog;

    invoke-virtual {v0}, Landroid/app/AlertDialog;->hide()V

    :cond_0
    return-void
.end method

.method private hideExpandedDetails()V
    .locals 2

    iget-object v0, p0, Lcom/android/mail/browse/MessageHeaderView;->mExpandedDetailsView:Landroid/view/ViewGroup;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/mail/browse/MessageHeaderView;->mExpandedDetailsView:Landroid/view/ViewGroup;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->setVisibility(I)V

    :cond_0
    return-void
.end method

.method private hideInvite()V
    .locals 2

    iget-object v0, p0, Lcom/android/mail/browse/MessageHeaderView;->mInviteView:Lcom/android/mail/browse/MessageInviteView;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/mail/browse/MessageHeaderView;->mInviteView:Lcom/android/mail/browse/MessageInviteView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Lcom/android/mail/browse/MessageInviteView;->setVisibility(I)V

    :cond_0
    return-void
.end method

.method private hideShowImagePrompt()V
    .locals 2

    iget-object v0, p0, Lcom/android/mail/browse/MessageHeaderView;->mImagePromptView:Landroid/view/ViewGroup;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/mail/browse/MessageHeaderView;->mImagePromptView:Landroid/view/ViewGroup;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->setVisibility(I)V

    :cond_0
    return-void
.end method

.method private hideSpamWarning()V
    .locals 2

    iget-object v0, p0, Lcom/android/mail/browse/MessageHeaderView;->mSpamWarningView:Lcom/android/mail/browse/SpamWarningView;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/mail/browse/MessageHeaderView;->mSpamWarningView:Lcom/android/mail/browse/SpamWarningView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Lcom/android/mail/browse/SpamWarningView;->setVisibility(I)V

    :cond_0
    return-void
.end method

.method static makeSnippet(Ljava/lang/String;)Ljava/lang/String;
    .locals 9

    const/16 v8, 0x3c

    const/16 v7, 0x3b

    const/16 v6, 0x26

    const/4 v5, -0x1

    invoke-static {p0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_0
    new-instance v1, Ljava/lang/StringBuilder;

    const/16 v0, 0x64

    invoke-direct {v1, v0}, Ljava/lang/StringBuilder;-><init>(I)V

    new-instance v2, Ljava/io/StringReader;

    invoke-direct {v2, p0}, Ljava/io/StringReader;-><init>(Ljava/lang/String;)V

    :cond_1
    :goto_1
    :try_start_0
    invoke-virtual {v2}, Ljava/io/StringReader;->read()I

    move-result v0

    if-eq v0, v5, :cond_3

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->length()I

    move-result v3

    const/16 v4, 0x64

    if-ge v3, v4, :cond_3

    invoke-static {v0}, Ljava/lang/Character;->isWhitespace(I)Z

    move-result v3

    if-eqz v3, :cond_4

    const/16 v0, 0x20

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    :cond_2
    invoke-virtual {v2}, Ljava/io/StringReader;->read()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Character;->isWhitespace(I)Z
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v3

    if-nez v3, :cond_2

    if-ne v0, v5, :cond_4

    :cond_3
    :goto_2
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    :cond_4
    if-ne v0, v8, :cond_7

    :cond_5
    :try_start_1
    invoke-virtual {v2}, Ljava/io/StringReader;->read()I

    move-result v0

    if-eq v0, v5, :cond_6

    const/16 v3, 0x3e

    if-ne v0, v3, :cond_5

    :cond_6
    if-ne v0, v5, :cond_1

    goto :goto_2

    :cond_7
    if-ne v0, v6, :cond_12

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    :goto_3
    invoke-virtual {v2}, Ljava/io/StringReader;->read()I

    move-result v3

    if-eq v3, v5, :cond_8

    if-ne v3, v7, :cond_a

    :cond_8
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v4, "nbsp"

    invoke-virtual {v4, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_b

    const/16 v0, 0x20

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    :cond_9
    :goto_4
    if-ne v3, v5, :cond_1

    goto :goto_2

    :cond_a
    int-to-char v3, v3

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_3

    :catch_0
    move-exception v0

    sget-object v2, Lcom/android/mail/browse/MessageHeaderView;->LOG_TAG:Ljava/lang/String;

    const-string v3, "Really? IOException while reading a freaking string?!? "

    const/4 v4, 0x0

    new-array v4, v4, [Ljava/lang/Object;

    invoke-static {v2, v0, v3, v4}, Lcom/android/mail/utils/LogUtils;->wtf(Ljava/lang/String;Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)I

    goto :goto_2

    :cond_b
    :try_start_2
    const-string v4, "lt"

    invoke-virtual {v4, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_c

    const/16 v0, 0x3c

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    goto :goto_4

    :cond_c
    const-string v4, "gt"

    invoke-virtual {v4, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_d

    const/16 v0, 0x3e

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    goto :goto_4

    :cond_d
    const-string v4, "amp"

    invoke-virtual {v4, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_e

    const/16 v0, 0x26

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    goto :goto_4

    :cond_e
    const-string v4, "quot"

    invoke-virtual {v4, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_f

    const/16 v0, 0x22

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    goto :goto_4

    :cond_f
    const-string v4, "apos"

    invoke-virtual {v4, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_10

    const-string v4, "#39"

    invoke-virtual {v4, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_11

    :cond_10
    const/16 v0, 0x27

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    goto :goto_4

    :cond_11
    const/16 v4, 0x26

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    if-ne v3, v7, :cond_9

    const/16 v0, 0x3b

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    goto :goto_4

    :cond_12
    int-to-char v0, v0

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_0

    goto/16 :goto_1
.end method

.method private measureHeight()I
    .locals 6

    const/4 v5, 0x0

    invoke-virtual {p0}, Lcom/android/mail/browse/MessageHeaderView;->getParent()Landroid/view/ViewParent;

    move-result-object v1

    check-cast v1, Landroid/view/ViewGroup;

    if-nez v1, :cond_0

    sget-object v2, Lcom/android/mail/browse/MessageHeaderView;->LOG_TAG:Ljava/lang/String;

    new-instance v3, Ljava/lang/Error;

    invoke-direct {v3}, Ljava/lang/Error;-><init>()V

    const-string v4, "Unable to measure height of detached header"

    new-array v5, v5, [Ljava/lang/Object;

    invoke-static {v2, v3, v4, v5}, Lcom/android/mail/utils/LogUtils;->e(Ljava/lang/String;Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)I

    invoke-virtual {p0}, Lcom/android/mail/browse/MessageHeaderView;->getHeight()I

    move-result v0

    :goto_0
    return v0

    :cond_0
    const/4 v2, 0x1

    iput-boolean v2, p0, Lcom/android/mail/browse/MessageHeaderView;->mPreMeasuring:Z

    invoke-static {p0, v1}, Lcom/android/mail/utils/Utils;->measureViewHeight(Landroid/view/View;Landroid/view/ViewGroup;)I

    move-result v0

    iput-boolean v5, p0, Lcom/android/mail/browse/MessageHeaderView;->mPreMeasuring:Z

    goto :goto_0
.end method

.method private onLongClick(I)Z
    .locals 3

    const/4 v1, 0x1

    const v0, 0x7f080083

    if-ne p1, v0, :cond_0

    invoke-virtual {p0}, Lcom/android/mail/browse/MessageHeaderView;->isExpanded()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/mail/browse/MessageHeaderView;->mSender:Lcom/android/mail/providers/Address;

    invoke-virtual {v0}, Lcom/android/mail/providers/Address;->getAddress()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/android/mail/browse/MessageHeaderView;->mCopyAddress:Ljava/lang/String;

    new-instance v0, Landroid/app/Dialog;

    invoke-virtual {p0}, Lcom/android/mail/browse/MessageHeaderView;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-direct {v0, v2}, Landroid/app/Dialog;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/android/mail/browse/MessageHeaderView;->mEmailCopyPopup:Landroid/app/Dialog;

    iget-object v0, p0, Lcom/android/mail/browse/MessageHeaderView;->mEmailCopyPopup:Landroid/app/Dialog;

    iget-object v2, p0, Lcom/android/mail/browse/MessageHeaderView;->mCopyAddress:Ljava/lang/String;

    invoke-virtual {v0, v2}, Landroid/app/Dialog;->setTitle(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lcom/android/mail/browse/MessageHeaderView;->mEmailCopyPopup:Landroid/app/Dialog;

    const v2, 0x7f040033

    invoke-virtual {v0, v2}, Landroid/app/Dialog;->setContentView(I)V

    iget-object v0, p0, Lcom/android/mail/browse/MessageHeaderView;->mEmailCopyPopup:Landroid/app/Dialog;

    invoke-virtual {v0, v1}, Landroid/app/Dialog;->setCancelable(Z)V

    iget-object v0, p0, Lcom/android/mail/browse/MessageHeaderView;->mEmailCopyPopup:Landroid/app/Dialog;

    invoke-virtual {v0, v1}, Landroid/app/Dialog;->setCanceledOnTouchOutside(Z)V

    iget-object v0, p0, Lcom/android/mail/browse/MessageHeaderView;->mEmailCopyPopup:Landroid/app/Dialog;

    const v2, 0x1020019

    invoke-virtual {v0, v2}, Landroid/app/Dialog;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    invoke-virtual {v0, p0}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    const v2, 0x7f090014

    invoke-virtual {v0, v2}, Landroid/widget/Button;->setText(I)V

    iget-object v0, p0, Lcom/android/mail/browse/MessageHeaderView;->mEmailCopyPopup:Landroid/app/Dialog;

    invoke-virtual {v0}, Landroid/app/Dialog;->show()V

    move v0, v1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private varargs registerMessageClickTargets([I)V
    .locals 5
    .param p1    # [I

    move-object v0, p1

    array-length v3, v0

    const/4 v1, 0x0

    :goto_0
    if-ge v1, v3, :cond_1

    aget v2, v0, v1

    invoke-virtual {p0, v2}, Lcom/android/mail/browse/MessageHeaderView;->findViewById(I)Landroid/view/View;

    move-result-object v4

    if-eqz v4, :cond_0

    invoke-virtual {v4, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    invoke-virtual {v4, p0}, Landroid/view/View;->setOnLongClickListener(Landroid/view/View$OnLongClickListener;)V

    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_1
    return-void
.end method

.method private render(Z)V
    .locals 9
    .param p1    # Z

    const/4 v5, 0x1

    const/4 v6, 0x0

    iget-object v4, p0, Lcom/android/mail/browse/MessageHeaderView;->mMessageHeaderItem:Lcom/android/mail/browse/ConversationViewAdapter$MessageHeaderItem;

    if-nez v4, :cond_0

    :goto_0
    return-void

    :cond_0
    new-instance v3, Lcom/android/mail/perf/Timer;

    invoke-direct {v3}, Lcom/android/mail/perf/Timer;-><init>()V

    const-string v4, "message header render"

    invoke-virtual {v3, v4}, Lcom/android/mail/perf/Timer;->start(Ljava/lang/String;)V

    iput-boolean v6, p0, Lcom/android/mail/browse/MessageHeaderView;->mCollapsedDetailsValid:Z

    iput-boolean v6, p0, Lcom/android/mail/browse/MessageHeaderView;->mExpandedDetailsValid:Z

    iget-object v4, p0, Lcom/android/mail/browse/MessageHeaderView;->mMessageHeaderItem:Lcom/android/mail/browse/ConversationViewAdapter$MessageHeaderItem;

    invoke-virtual {v4}, Lcom/android/mail/browse/ConversationViewAdapter$MessageHeaderItem;->getMessage()Lcom/android/mail/browse/MessageCursor$ConversationMessage;

    move-result-object v4

    iput-object v4, p0, Lcom/android/mail/browse/MessageHeaderView;->mMessage:Lcom/android/mail/browse/MessageCursor$ConversationMessage;

    iget-object v4, p0, Lcom/android/mail/browse/MessageHeaderView;->mMessage:Lcom/android/mail/browse/MessageCursor$ConversationMessage;

    invoke-virtual {v4}, Lcom/android/mail/browse/MessageCursor$ConversationMessage;->shouldShowImagePrompt()Z

    move-result v4

    iput-boolean v4, p0, Lcom/android/mail/browse/MessageHeaderView;->mShowImagePrompt:Z

    iget-object v4, p0, Lcom/android/mail/browse/MessageHeaderView;->mMessageHeaderItem:Lcom/android/mail/browse/ConversationViewAdapter$MessageHeaderItem;

    invoke-virtual {v4}, Lcom/android/mail/browse/ConversationViewAdapter$MessageHeaderItem;->isExpanded()Z

    move-result v4

    invoke-direct {p0, v4}, Lcom/android/mail/browse/MessageHeaderView;->setExpanded(Z)V

    iget-object v4, p0, Lcom/android/mail/browse/MessageHeaderView;->mMessage:Lcom/android/mail/browse/MessageCursor$ConversationMessage;

    iget-wide v7, v4, Lcom/android/mail/browse/MessageCursor$ConversationMessage;->dateReceivedMs:J

    iput-wide v7, p0, Lcom/android/mail/browse/MessageHeaderView;->mTimestampMs:J

    iget-object v4, p0, Lcom/android/mail/browse/MessageHeaderView;->mMessageHeaderItem:Lcom/android/mail/browse/ConversationViewAdapter$MessageHeaderItem;

    iget-object v4, v4, Lcom/android/mail/browse/ConversationViewAdapter$MessageHeaderItem;->timestampShort:Ljava/lang/CharSequence;

    iput-object v4, p0, Lcom/android/mail/browse/MessageHeaderView;->mTimestampShort:Ljava/lang/CharSequence;

    iget-object v4, p0, Lcom/android/mail/browse/MessageHeaderView;->mTimestampShort:Ljava/lang/CharSequence;

    if-nez v4, :cond_1

    iget-object v4, p0, Lcom/android/mail/browse/MessageHeaderView;->mDateBuilder:Lcom/android/mail/FormattedDateBuilder;

    iget-wide v7, p0, Lcom/android/mail/browse/MessageHeaderView;->mTimestampMs:J

    invoke-virtual {v4, v7, v8}, Lcom/android/mail/FormattedDateBuilder;->formatShortDate(J)Ljava/lang/CharSequence;

    move-result-object v4

    iput-object v4, p0, Lcom/android/mail/browse/MessageHeaderView;->mTimestampShort:Ljava/lang/CharSequence;

    iget-object v4, p0, Lcom/android/mail/browse/MessageHeaderView;->mMessageHeaderItem:Lcom/android/mail/browse/ConversationViewAdapter$MessageHeaderItem;

    iget-object v7, p0, Lcom/android/mail/browse/MessageHeaderView;->mTimestampShort:Ljava/lang/CharSequence;

    iput-object v7, v4, Lcom/android/mail/browse/ConversationViewAdapter$MessageHeaderItem;->timestampShort:Ljava/lang/CharSequence;

    :cond_1
    iget-object v4, p0, Lcom/android/mail/browse/MessageHeaderView;->mMessage:Lcom/android/mail/browse/MessageCursor$ConversationMessage;

    invoke-virtual {v4}, Lcom/android/mail/browse/MessageCursor$ConversationMessage;->getFromAddresses()[Ljava/lang/String;

    move-result-object v4

    iput-object v4, p0, Lcom/android/mail/browse/MessageHeaderView;->mFrom:[Ljava/lang/String;

    iget-object v4, p0, Lcom/android/mail/browse/MessageHeaderView;->mMessage:Lcom/android/mail/browse/MessageCursor$ConversationMessage;

    invoke-virtual {v4}, Lcom/android/mail/browse/MessageCursor$ConversationMessage;->getToAddresses()[Ljava/lang/String;

    move-result-object v4

    iput-object v4, p0, Lcom/android/mail/browse/MessageHeaderView;->mTo:[Ljava/lang/String;

    iget-object v4, p0, Lcom/android/mail/browse/MessageHeaderView;->mMessage:Lcom/android/mail/browse/MessageCursor$ConversationMessage;

    invoke-virtual {v4}, Lcom/android/mail/browse/MessageCursor$ConversationMessage;->getCcAddresses()[Ljava/lang/String;

    move-result-object v4

    iput-object v4, p0, Lcom/android/mail/browse/MessageHeaderView;->mCc:[Ljava/lang/String;

    iget-object v4, p0, Lcom/android/mail/browse/MessageHeaderView;->mMessage:Lcom/android/mail/browse/MessageCursor$ConversationMessage;

    invoke-virtual {v4}, Lcom/android/mail/browse/MessageCursor$ConversationMessage;->getBccAddresses()[Ljava/lang/String;

    move-result-object v4

    iput-object v4, p0, Lcom/android/mail/browse/MessageHeaderView;->mBcc:[Ljava/lang/String;

    iget-object v4, p0, Lcom/android/mail/browse/MessageHeaderView;->mMessage:Lcom/android/mail/browse/MessageCursor$ConversationMessage;

    invoke-virtual {v4}, Lcom/android/mail/browse/MessageCursor$ConversationMessage;->getReplyToAddresses()[Ljava/lang/String;

    move-result-object v4

    iput-object v4, p0, Lcom/android/mail/browse/MessageHeaderView;->mReplyTo:[Ljava/lang/String;

    iget-object v4, p0, Lcom/android/mail/browse/MessageHeaderView;->mMessage:Lcom/android/mail/browse/MessageCursor$ConversationMessage;

    iget v4, v4, Lcom/android/mail/browse/MessageCursor$ConversationMessage;->draftType:I

    if-eqz v4, :cond_8

    move v4, v5

    :goto_1
    iput-boolean v4, p0, Lcom/android/mail/browse/MessageHeaderView;->mIsDraft:Z

    iget-object v4, p0, Lcom/android/mail/browse/MessageHeaderView;->mMessage:Lcom/android/mail/browse/MessageCursor$ConversationMessage;

    iget-boolean v4, v4, Lcom/android/mail/browse/MessageCursor$ConversationMessage;->isSending:Z

    iput-boolean v4, p0, Lcom/android/mail/browse/MessageHeaderView;->mIsSending:Z

    iget-object v4, p0, Lcom/android/mail/browse/MessageHeaderView;->mMessage:Lcom/android/mail/browse/MessageCursor$ConversationMessage;

    invoke-virtual {v4}, Lcom/android/mail/browse/MessageCursor$ConversationMessage;->getFrom()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-eqz v4, :cond_2

    invoke-direct {p0}, Lcom/android/mail/browse/MessageHeaderView;->getAccount()Lcom/android/mail/providers/Account;

    move-result-object v4

    iget-object v1, v4, Lcom/android/mail/providers/Account;->name:Ljava/lang/String;

    :cond_2
    invoke-direct {p0, v1}, Lcom/android/mail/browse/MessageHeaderView;->getAddress(Ljava/lang/String;)Lcom/android/mail/providers/Address;

    move-result-object v4

    iput-object v4, p0, Lcom/android/mail/browse/MessageHeaderView;->mSender:Lcom/android/mail/providers/Address;

    iget-object v4, p0, Lcom/android/mail/browse/MessageHeaderView;->mStarView:Landroid/widget/ImageView;

    iget-object v7, p0, Lcom/android/mail/browse/MessageHeaderView;->mMessage:Lcom/android/mail/browse/MessageCursor$ConversationMessage;

    iget-boolean v7, v7, Lcom/android/mail/browse/MessageCursor$ConversationMessage;->starred:Z

    invoke-virtual {v4, v7}, Landroid/widget/ImageView;->setSelected(Z)V

    iget-object v7, p0, Lcom/android/mail/browse/MessageHeaderView;->mStarView:Landroid/widget/ImageView;

    invoke-virtual {p0}, Lcom/android/mail/browse/MessageHeaderView;->getResources()Landroid/content/res/Resources;

    move-result-object v8

    iget-object v4, p0, Lcom/android/mail/browse/MessageHeaderView;->mStarView:Landroid/widget/ImageView;

    invoke-virtual {v4}, Landroid/widget/ImageView;->isSelected()Z

    move-result v4

    if-eqz v4, :cond_9

    const v4, 0x7f09004f

    :goto_2
    invoke-virtual {v8, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v7, v4}, Landroid/widget/ImageView;->setContentDescription(Ljava/lang/CharSequence;)V

    iput-boolean v5, p0, Lcom/android/mail/browse/MessageHeaderView;->mStarShown:Z

    iget-object v4, p0, Lcom/android/mail/browse/MessageHeaderView;->mMessage:Lcom/android/mail/browse/MessageCursor$ConversationMessage;

    invoke-virtual {v4}, Lcom/android/mail/browse/MessageCursor$ConversationMessage;->getConversation()Lcom/android/mail/providers/Conversation;

    move-result-object v4

    invoke-virtual {v4}, Lcom/android/mail/providers/Conversation;->getRawFolders()Ljava/util/List;

    move-result-object v4

    invoke-interface {v4}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_3
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_4

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/mail/providers/Folder;

    invoke-virtual {v0}, Lcom/android/mail/providers/Folder;->isTrash()Z

    move-result v4

    if-eqz v4, :cond_3

    iput-boolean v6, p0, Lcom/android/mail/browse/MessageHeaderView;->mStarShown:Z

    :cond_4
    invoke-direct {p0}, Lcom/android/mail/browse/MessageHeaderView;->updateChildVisibility()V

    iget-boolean v4, p0, Lcom/android/mail/browse/MessageHeaderView;->mIsDraft:Z

    if-nez v4, :cond_5

    iget-boolean v4, p0, Lcom/android/mail/browse/MessageHeaderView;->mIsSending:Z

    if-eqz v4, :cond_a

    :cond_5
    iget-object v4, p0, Lcom/android/mail/browse/MessageHeaderView;->mMessage:Lcom/android/mail/browse/MessageCursor$ConversationMessage;

    iget-object v4, v4, Lcom/android/mail/browse/MessageCursor$ConversationMessage;->snippet:Ljava/lang/String;

    invoke-static {v4}, Lcom/android/mail/browse/MessageHeaderView;->makeSnippet(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    iput-object v4, p0, Lcom/android/mail/browse/MessageHeaderView;->mSnippet:Ljava/lang/String;

    :goto_3
    iget-object v4, p0, Lcom/android/mail/browse/MessageHeaderView;->mSenderNameView:Landroid/widget/TextView;

    invoke-direct {p0}, Lcom/android/mail/browse/MessageHeaderView;->getHeaderTitle()Ljava/lang/CharSequence;

    move-result-object v6

    invoke-virtual {v4, v6}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v4, p0, Lcom/android/mail/browse/MessageHeaderView;->mSenderEmailView:Landroid/widget/TextView;

    invoke-direct {p0}, Lcom/android/mail/browse/MessageHeaderView;->getHeaderSubtitle()Ljava/lang/CharSequence;

    move-result-object v6

    invoke-virtual {v4, v6}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v4, p0, Lcom/android/mail/browse/MessageHeaderView;->mUpperDateView:Landroid/widget/TextView;

    if-eqz v4, :cond_6

    iget-object v4, p0, Lcom/android/mail/browse/MessageHeaderView;->mUpperDateView:Landroid/widget/TextView;

    iget-object v6, p0, Lcom/android/mail/browse/MessageHeaderView;->mTimestampShort:Ljava/lang/CharSequence;

    invoke-virtual {v4, v6}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    :cond_6
    if-eqz p1, :cond_b

    invoke-virtual {p0}, Lcom/android/mail/browse/MessageHeaderView;->unbind()V

    :cond_7
    :goto_4
    const-string v4, "message header render"

    invoke-virtual {v3, v4}, Lcom/android/mail/perf/Timer;->pause(Ljava/lang/String;)V

    goto/16 :goto_0

    :cond_8
    move v4, v6

    goto/16 :goto_1

    :cond_9
    const v4, 0x7f09004e

    goto :goto_2

    :cond_a
    iget-object v4, p0, Lcom/android/mail/browse/MessageHeaderView;->mMessage:Lcom/android/mail/browse/MessageCursor$ConversationMessage;

    iget-object v4, v4, Lcom/android/mail/browse/MessageCursor$ConversationMessage;->snippet:Ljava/lang/String;

    iput-object v4, p0, Lcom/android/mail/browse/MessageHeaderView;->mSnippet:Ljava/lang/String;

    goto :goto_3

    :cond_b
    invoke-direct {p0}, Lcom/android/mail/browse/MessageHeaderView;->updateContactInfo()V

    iget-boolean v4, p0, Lcom/android/mail/browse/MessageHeaderView;->mObservingContactInfo:Z

    if-nez v4, :cond_7

    iget-object v4, p0, Lcom/android/mail/browse/MessageHeaderView;->mContactInfoSource:Lcom/android/mail/ContactInfoSource;

    iget-object v6, p0, Lcom/android/mail/browse/MessageHeaderView;->mContactInfoObserver:Landroid/database/DataSetObserver;

    invoke-interface {v4, v6}, Lcom/android/mail/ContactInfoSource;->registerObserver(Landroid/database/DataSetObserver;)V

    iput-boolean v5, p0, Lcom/android/mail/browse/MessageHeaderView;->mObservingContactInfo:Z

    goto :goto_4
.end method

.method private renderEmailList(II[Ljava/lang/String;ZLandroid/view/View;)V
    .locals 13
    .param p1    # I
    .param p2    # I
    .param p3    # [Ljava/lang/String;
    .param p4    # Z
    .param p5    # Landroid/view/View;

    if-eqz p3, :cond_0

    move-object/from16 v0, p3

    array-length v9, v0

    if-nez v9, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    move-object/from16 v0, p3

    array-length v9, v0

    new-array v4, v9, [Ljava/lang/String;

    invoke-virtual {p0}, Lcom/android/mail/browse/MessageHeaderView;->getResources()Landroid/content/res/Resources;

    move-result-object v8

    const/4 v5, 0x0

    :goto_1
    move-object/from16 v0, p3

    array-length v9, v0

    if-ge v5, v9, :cond_7

    aget-object v9, p3, v5

    invoke-direct {p0, v9}, Lcom/android/mail/browse/MessageHeaderView;->getAddress(Ljava/lang/String;)Lcom/android/mail/providers/Address;

    move-result-object v3

    invoke-virtual {v3}, Lcom/android/mail/providers/Address;->getName()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v3}, Lcom/android/mail/providers/Address;->getAddress()Ljava/lang/String;

    move-result-object v1

    iget-object v9, p0, Lcom/android/mail/browse/MessageHeaderView;->mVeiledMatcher:Lcom/android/mail/utils/VeiledAddressMatcher;

    invoke-virtual {v9, v1}, Lcom/android/mail/utils/VeiledAddressMatcher;->isVeiledAddress(Ljava/lang/String;)Z

    move-result v6

    if-eqz v6, :cond_4

    const-string v2, ""

    invoke-static {v7}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v9

    if-eqz v9, :cond_3

    const v9, 0x7f09012c

    invoke-virtual {v8, v9}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v7

    :goto_2
    if-eqz v7, :cond_2

    invoke-virtual {v7}, Ljava/lang/String;->length()I

    move-result v9

    if-nez v9, :cond_5

    :cond_2
    aput-object v2, v4, v5

    :goto_3
    add-int/lit8 v5, v5, 0x1

    goto :goto_1

    :cond_3
    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v9, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    const v10, 0x7f09012b

    invoke-virtual {v8, v10}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    goto :goto_2

    :cond_4
    move-object v2, v1

    goto :goto_2

    :cond_5
    if-eqz p4, :cond_6

    const v9, 0x7f0900d3

    const/4 v10, 0x3

    new-array v10, v10, [Ljava/lang/Object;

    const/4 v11, 0x0

    aput-object v7, v10, v11

    const/4 v11, 0x1

    aput-object v2, v10, v11

    const/4 v11, 0x2

    iget-object v12, p0, Lcom/android/mail/browse/MessageHeaderView;->mMessage:Lcom/android/mail/browse/MessageCursor$ConversationMessage;

    iget-object v12, v12, Lcom/android/mail/browse/MessageCursor$ConversationMessage;->viaDomain:Ljava/lang/String;

    aput-object v12, v10, v11

    invoke-virtual {v8, v9, v10}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v9

    aput-object v9, v4, v5

    goto :goto_3

    :cond_6
    const v9, 0x7f0900d2

    const/4 v10, 0x2

    new-array v10, v10, [Ljava/lang/Object;

    const/4 v11, 0x0

    aput-object v7, v10, v11

    const/4 v11, 0x1

    aput-object v2, v10, v11

    invoke-virtual {v8, v9, v10}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v9

    aput-object v9, v4, v5

    goto :goto_3

    :cond_7
    move-object/from16 v0, p5

    invoke-virtual {v0, p2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v9

    check-cast v9, Landroid/widget/TextView;

    const-string v10, "\n"

    invoke-static {v10, v4}, Landroid/text/TextUtils;->join(Ljava/lang/CharSequence;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v9, v10}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    move-object/from16 v0, p5

    invoke-virtual {v0, p1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v9

    const/4 v10, 0x0

    invoke-virtual {v9, v10}, Landroid/view/View;->setVisibility(I)V

    goto/16 :goto_0
.end method

.method private static setChildMarginRight(Landroid/view/View;I)V
    .locals 1
    .param p0    # Landroid/view/View;
    .param p1    # I

    invoke-virtual {p0}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup$MarginLayoutParams;

    iput p1, v0, Landroid/view/ViewGroup$MarginLayoutParams;->rightMargin:I

    invoke-virtual {p0, v0}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    return-void
.end method

.method private varargs setChildVisibility(I[Landroid/view/View;)V
    .locals 4
    .param p1    # I
    .param p2    # [Landroid/view/View;

    move-object v0, p2

    array-length v2, v0

    const/4 v1, 0x0

    :goto_0
    if-ge v1, v2, :cond_1

    aget-object v3, v0, v1

    if-eqz v3, :cond_0

    invoke-virtual {v3, p1}, Landroid/view/View;->setVisibility(I)V

    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_1
    return-void
.end method

.method private setExpanded(Z)V
    .locals 1
    .param p1    # Z

    invoke-virtual {p0, p1}, Lcom/android/mail/browse/MessageHeaderView;->setActivated(Z)V

    iget-object v0, p0, Lcom/android/mail/browse/MessageHeaderView;->mMessageHeaderItem:Lcom/android/mail/browse/ConversationViewAdapter$MessageHeaderItem;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/mail/browse/MessageHeaderView;->mMessageHeaderItem:Lcom/android/mail/browse/ConversationViewAdapter$MessageHeaderItem;

    invoke-virtual {v0, p1}, Lcom/android/mail/browse/ConversationViewAdapter$MessageHeaderItem;->setExpanded(Z)V

    :cond_0
    return-void
.end method

.method private setMessageDetailsExpanded(Z)V
    .locals 2
    .param p1    # Z

    iget v0, p0, Lcom/android/mail/browse/MessageHeaderView;->mExpandMode:I

    if-nez v0, :cond_3

    if-eqz p1, :cond_2

    invoke-direct {p0}, Lcom/android/mail/browse/MessageHeaderView;->showExpandedDetails()V

    invoke-direct {p0}, Lcom/android/mail/browse/MessageHeaderView;->hideCollapsedDetails()V

    :cond_0
    :goto_0
    iget-object v0, p0, Lcom/android/mail/browse/MessageHeaderView;->mMessageHeaderItem:Lcom/android/mail/browse/ConversationViewAdapter$MessageHeaderItem;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/android/mail/browse/MessageHeaderView;->mMessageHeaderItem:Lcom/android/mail/browse/ConversationViewAdapter$MessageHeaderItem;

    iput-boolean p1, v0, Lcom/android/mail/browse/ConversationViewAdapter$MessageHeaderItem;->detailsExpanded:Z

    :cond_1
    return-void

    :cond_2
    invoke-direct {p0}, Lcom/android/mail/browse/MessageHeaderView;->hideExpandedDetails()V

    invoke-direct {p0}, Lcom/android/mail/browse/MessageHeaderView;->showCollapsedDetails()V

    goto :goto_0

    :cond_3
    iget v0, p0, Lcom/android/mail/browse/MessageHeaderView;->mExpandMode:I

    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    if-eqz p1, :cond_4

    invoke-direct {p0}, Lcom/android/mail/browse/MessageHeaderView;->showDetailsPopup()V

    goto :goto_0

    :cond_4
    invoke-direct {p0}, Lcom/android/mail/browse/MessageHeaderView;->hideDetailsPopup()V

    invoke-direct {p0}, Lcom/android/mail/browse/MessageHeaderView;->showCollapsedDetails()V

    goto :goto_0
.end method

.method private setReplyOrReplyAllVisible()V
    .locals 7

    const/4 v5, 0x2

    const/16 v2, 0x8

    const/4 v4, 0x1

    const/4 v3, 0x0

    iget-boolean v1, p0, Lcom/android/mail/browse/MessageHeaderView;->mIsDraft:Z

    if-eqz v1, :cond_0

    new-array v1, v5, [Landroid/view/View;

    iget-object v5, p0, Lcom/android/mail/browse/MessageHeaderView;->mReplyButton:Landroid/view/View;

    aput-object v5, v1, v3

    iget-object v3, p0, Lcom/android/mail/browse/MessageHeaderView;->mReplyAllButton:Landroid/view/View;

    aput-object v3, v1, v4

    invoke-direct {p0, v2, v1}, Lcom/android/mail/browse/MessageHeaderView;->setChildVisibility(I[Landroid/view/View;)V

    :goto_0
    return-void

    :cond_0
    iget-object v1, p0, Lcom/android/mail/browse/MessageHeaderView;->mOverflowButton:Landroid/view/View;

    if-nez v1, :cond_1

    new-array v1, v5, [Landroid/view/View;

    iget-object v2, p0, Lcom/android/mail/browse/MessageHeaderView;->mReplyButton:Landroid/view/View;

    aput-object v2, v1, v3

    iget-object v2, p0, Lcom/android/mail/browse/MessageHeaderView;->mReplyAllButton:Landroid/view/View;

    aput-object v2, v1, v4

    invoke-direct {p0, v3, v1}, Lcom/android/mail/browse/MessageHeaderView;->setChildVisibility(I[Landroid/view/View;)V

    goto :goto_0

    :cond_1
    invoke-direct {p0}, Lcom/android/mail/browse/MessageHeaderView;->getAccount()Lcom/android/mail/providers/Account;

    move-result-object v1

    iget-object v1, v1, Lcom/android/mail/providers/Account;->settings:Lcom/android/mail/providers/Settings;

    iget v1, v1, Lcom/android/mail/providers/Settings;->replyBehavior:I

    if-ne v1, v4, :cond_3

    move v0, v4

    :goto_1
    if-eqz v0, :cond_4

    move v1, v2

    :goto_2
    new-array v5, v4, [Landroid/view/View;

    iget-object v6, p0, Lcom/android/mail/browse/MessageHeaderView;->mReplyButton:Landroid/view/View;

    aput-object v6, v5, v3

    invoke-direct {p0, v1, v5}, Lcom/android/mail/browse/MessageHeaderView;->setChildVisibility(I[Landroid/view/View;)V

    if-eqz v0, :cond_2

    move v2, v3

    :cond_2
    new-array v1, v4, [Landroid/view/View;

    iget-object v4, p0, Lcom/android/mail/browse/MessageHeaderView;->mReplyAllButton:Landroid/view/View;

    aput-object v4, v1, v3

    invoke-direct {p0, v2, v1}, Lcom/android/mail/browse/MessageHeaderView;->setChildVisibility(I[Landroid/view/View;)V

    goto :goto_0

    :cond_3
    move v0, v3

    goto :goto_1

    :cond_4
    move v1, v3

    goto :goto_2
.end method

.method private showCollapsedDetails()V
    .locals 10

    const/4 v9, 0x0

    iget-object v0, p0, Lcom/android/mail/browse/MessageHeaderView;->mCollapsedDetailsView:Landroid/view/ViewGroup;

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/android/mail/browse/MessageHeaderView;->mInflater:Landroid/view/LayoutInflater;

    const v1, 0x7f040025

    invoke-virtual {v0, v1, p0, v9}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    iput-object v0, p0, Lcom/android/mail/browse/MessageHeaderView;->mCollapsedDetailsView:Landroid/view/ViewGroup;

    iget-object v0, p0, Lcom/android/mail/browse/MessageHeaderView;->mCollapsedDetailsView:Landroid/view/ViewGroup;

    iget-object v1, p0, Lcom/android/mail/browse/MessageHeaderView;->mUpperHeaderView:Landroid/view/ViewGroup;

    invoke-virtual {p0, v1}, Lcom/android/mail/browse/MessageHeaderView;->indexOfChild(Landroid/view/View;)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    invoke-virtual {p0, v0, v1}, Lcom/android/mail/browse/MessageHeaderView;->addView(Landroid/view/View;I)V

    iget-object v0, p0, Lcom/android/mail/browse/MessageHeaderView;->mCollapsedDetailsView:Landroid/view/ViewGroup;

    invoke-virtual {v0, p0}, Landroid/view/ViewGroup;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    :cond_0
    iget-boolean v0, p0, Lcom/android/mail/browse/MessageHeaderView;->mCollapsedDetailsValid:Z

    if-nez v0, :cond_2

    iget-object v0, p0, Lcom/android/mail/browse/MessageHeaderView;->mMessageHeaderItem:Lcom/android/mail/browse/ConversationViewAdapter$MessageHeaderItem;

    iget-object v0, v0, Lcom/android/mail/browse/ConversationViewAdapter$MessageHeaderItem;->recipientSummaryText:Ljava/lang/CharSequence;

    if-nez v0, :cond_1

    iget-object v8, p0, Lcom/android/mail/browse/MessageHeaderView;->mMessageHeaderItem:Lcom/android/mail/browse/ConversationViewAdapter$MessageHeaderItem;

    invoke-virtual {p0}, Lcom/android/mail/browse/MessageHeaderView;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-direct {p0}, Lcom/android/mail/browse/MessageHeaderView;->getAccount()Lcom/android/mail/providers/Account;

    move-result-object v1

    iget-object v1, v1, Lcom/android/mail/providers/Account;->name:Ljava/lang/String;

    iget-object v2, p0, Lcom/android/mail/browse/MessageHeaderView;->mMyName:Ljava/lang/String;

    iget-object v3, p0, Lcom/android/mail/browse/MessageHeaderView;->mTo:[Ljava/lang/String;

    iget-object v4, p0, Lcom/android/mail/browse/MessageHeaderView;->mCc:[Ljava/lang/String;

    iget-object v5, p0, Lcom/android/mail/browse/MessageHeaderView;->mBcc:[Ljava/lang/String;

    iget-object v6, p0, Lcom/android/mail/browse/MessageHeaderView;->mAddressCache:Ljava/util/Map;

    iget-object v7, p0, Lcom/android/mail/browse/MessageHeaderView;->mVeiledMatcher:Lcom/android/mail/utils/VeiledAddressMatcher;

    invoke-static/range {v0 .. v7}, Lcom/android/mail/browse/MessageHeaderView;->getRecipientSummaryText(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;[Ljava/lang/String;[Ljava/lang/String;Ljava/util/Map;Lcom/android/mail/utils/VeiledAddressMatcher;)Ljava/lang/CharSequence;

    move-result-object v0

    iput-object v0, v8, Lcom/android/mail/browse/ConversationViewAdapter$MessageHeaderItem;->recipientSummaryText:Ljava/lang/CharSequence;

    :cond_1
    const v0, 0x7f08006c

    invoke-virtual {p0, v0}, Lcom/android/mail/browse/MessageHeaderView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iget-object v1, p0, Lcom/android/mail/browse/MessageHeaderView;->mMessageHeaderItem:Lcom/android/mail/browse/ConversationViewAdapter$MessageHeaderItem;

    iget-object v1, v1, Lcom/android/mail/browse/ConversationViewAdapter$MessageHeaderItem;->recipientSummaryText:Ljava/lang/CharSequence;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    const v0, 0x7f08006d

    invoke-virtual {p0, v0}, Lcom/android/mail/browse/MessageHeaderView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iget-object v1, p0, Lcom/android/mail/browse/MessageHeaderView;->mTimestampShort:Ljava/lang/CharSequence;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/android/mail/browse/MessageHeaderView;->mCollapsedDetailsValid:Z

    :cond_2
    iget-object v0, p0, Lcom/android/mail/browse/MessageHeaderView;->mCollapsedDetailsView:Landroid/view/ViewGroup;

    invoke-virtual {v0, v9}, Landroid/view/ViewGroup;->setVisibility(I)V

    return-void
.end method

.method private showDetailsPopup()V
    .locals 4

    invoke-direct {p0}, Lcom/android/mail/browse/MessageHeaderView;->ensureExpandedDetailsView()Z

    iget-object v0, p0, Lcom/android/mail/browse/MessageHeaderView;->mDetailsPopup:Landroid/app/AlertDialog;

    if-nez v0, :cond_0

    new-instance v0, Landroid/app/AlertDialog$Builder;

    invoke-virtual {p0}, Lcom/android/mail/browse/MessageHeaderView;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    iget-object v1, p0, Lcom/android/mail/browse/MessageHeaderView;->mExpandedDetailsView:Landroid/view/ViewGroup;

    const v2, 0x7f080080

    invoke-virtual {v1, v2}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v1

    const/16 v2, 0x8

    invoke-virtual {v1, v2}, Landroid/view/View;->setVisibility(I)V

    iget-object v1, p0, Lcom/android/mail/browse/MessageHeaderView;->mExpandedDetailsView:Landroid/view/ViewGroup;

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setView(Landroid/view/View;)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Landroid/app/AlertDialog$Builder;->setCancelable(Z)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    invoke-virtual {p0}, Lcom/android/mail/browse/MessageHeaderView;->getContext()Landroid/content/Context;

    move-result-object v2

    const v3, 0x7f090117

    invoke-virtual {v2, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/app/AlertDialog$Builder;->setTitle(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->show()Landroid/app/AlertDialog;

    move-result-object v0

    iput-object v0, p0, Lcom/android/mail/browse/MessageHeaderView;->mDetailsPopup:Landroid/app/AlertDialog;

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/android/mail/browse/MessageHeaderView;->mDetailsPopup:Landroid/app/AlertDialog;

    invoke-virtual {v0}, Landroid/app/AlertDialog;->show()V

    goto :goto_0
.end method

.method private showExpandedDetails()V
    .locals 3

    invoke-direct {p0}, Lcom/android/mail/browse/MessageHeaderView;->ensureExpandedDetailsView()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v1, p0, Lcom/android/mail/browse/MessageHeaderView;->mExpandedDetailsView:Landroid/view/ViewGroup;

    iget-object v2, p0, Lcom/android/mail/browse/MessageHeaderView;->mUpperHeaderView:Landroid/view/ViewGroup;

    invoke-virtual {p0, v2}, Lcom/android/mail/browse/MessageHeaderView;->indexOfChild(Landroid/view/View;)I

    move-result v2

    add-int/lit8 v2, v2, 0x1

    invoke-virtual {p0, v1, v2}, Lcom/android/mail/browse/MessageHeaderView;->addView(Landroid/view/View;I)V

    :cond_0
    iget-object v1, p0, Lcom/android/mail/browse/MessageHeaderView;->mExpandedDetailsView:Landroid/view/ViewGroup;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Landroid/view/ViewGroup;->setVisibility(I)V

    return-void
.end method

.method private showImagePromptAlways(Z)V
    .locals 3

    const v2, 0x7f0900d0

    if-eqz p1, :cond_0

    invoke-direct {p0}, Lcom/android/mail/browse/MessageHeaderView;->showImagePromptOnce()V

    :cond_0
    iget-object v0, p0, Lcom/android/mail/browse/MessageHeaderView;->mImagePromptView:Landroid/view/ViewGroup;

    const v1, 0x7f08008b

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    invoke-virtual {p0}, Lcom/android/mail/browse/MessageHeaderView;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setContentDescription(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lcom/android/mail/browse/MessageHeaderView;->mImagePromptView:Landroid/view/ViewGroup;

    const v1, 0x7f08008c

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setText(I)V

    iget-object v0, p0, Lcom/android/mail/browse/MessageHeaderView;->mImagePromptView:Landroid/view/ViewGroup;

    const/4 v1, 0x2

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->setTag(Ljava/lang/Object;)V

    if-nez p1, :cond_1

    invoke-direct {p0}, Lcom/android/mail/browse/MessageHeaderView;->updateSpacerHeight()V

    :cond_1
    return-void
.end method

.method private showImagePromptOnce()V
    .locals 4

    const v3, 0x7f0900cf

    const/4 v2, 0x0

    iget-object v0, p0, Lcom/android/mail/browse/MessageHeaderView;->mImagePromptView:Landroid/view/ViewGroup;

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/android/mail/browse/MessageHeaderView;->mInflater:Landroid/view/LayoutInflater;

    const v1, 0x7f04002a

    invoke-virtual {v0, v1, p0, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    invoke-virtual {p0, v0}, Lcom/android/mail/browse/MessageHeaderView;->addView(Landroid/view/View;)V

    invoke-virtual {v0, p0}, Landroid/view/ViewGroup;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iput-object v0, p0, Lcom/android/mail/browse/MessageHeaderView;->mImagePromptView:Landroid/view/ViewGroup;

    :cond_0
    iget-object v0, p0, Lcom/android/mail/browse/MessageHeaderView;->mImagePromptView:Landroid/view/ViewGroup;

    invoke-virtual {v0, v2}, Landroid/view/ViewGroup;->setVisibility(I)V

    iget-object v0, p0, Lcom/android/mail/browse/MessageHeaderView;->mImagePromptView:Landroid/view/ViewGroup;

    const v1, 0x7f08008b

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    invoke-virtual {p0}, Lcom/android/mail/browse/MessageHeaderView;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setContentDescription(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lcom/android/mail/browse/MessageHeaderView;->mImagePromptView:Landroid/view/ViewGroup;

    const v1, 0x7f08008c

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setText(I)V

    iget-object v0, p0, Lcom/android/mail/browse/MessageHeaderView;->mImagePromptView:Landroid/view/ViewGroup;

    const/4 v1, 0x1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->setTag(Ljava/lang/Object;)V

    return-void
.end method

.method private showInvite()V
    .locals 3

    const/4 v2, 0x0

    iget-object v0, p0, Lcom/android/mail/browse/MessageHeaderView;->mInviteView:Lcom/android/mail/browse/MessageInviteView;

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/android/mail/browse/MessageHeaderView;->mInflater:Landroid/view/LayoutInflater;

    const v1, 0x7f040029

    invoke-virtual {v0, v1, p0, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/android/mail/browse/MessageInviteView;

    iput-object v0, p0, Lcom/android/mail/browse/MessageHeaderView;->mInviteView:Lcom/android/mail/browse/MessageInviteView;

    iget-object v0, p0, Lcom/android/mail/browse/MessageHeaderView;->mInviteView:Lcom/android/mail/browse/MessageInviteView;

    invoke-virtual {p0, v0}, Lcom/android/mail/browse/MessageHeaderView;->addView(Landroid/view/View;)V

    :cond_0
    iget-object v0, p0, Lcom/android/mail/browse/MessageHeaderView;->mInviteView:Lcom/android/mail/browse/MessageInviteView;

    iget-object v1, p0, Lcom/android/mail/browse/MessageHeaderView;->mMessage:Lcom/android/mail/browse/MessageCursor$ConversationMessage;

    invoke-virtual {v0, v1}, Lcom/android/mail/browse/MessageInviteView;->bind(Lcom/android/mail/providers/Message;)V

    iget-object v0, p0, Lcom/android/mail/browse/MessageHeaderView;->mInviteView:Lcom/android/mail/browse/MessageInviteView;

    invoke-virtual {v0, v2}, Lcom/android/mail/browse/MessageInviteView;->setVisibility(I)V

    return-void
.end method

.method private showSpamWarning()V
    .locals 3

    iget-object v0, p0, Lcom/android/mail/browse/MessageHeaderView;->mSpamWarningView:Lcom/android/mail/browse/SpamWarningView;

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/android/mail/browse/MessageHeaderView;->mInflater:Landroid/view/LayoutInflater;

    const v1, 0x7f04002b

    const/4 v2, 0x0

    invoke-virtual {v0, v1, p0, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/android/mail/browse/SpamWarningView;

    iput-object v0, p0, Lcom/android/mail/browse/MessageHeaderView;->mSpamWarningView:Lcom/android/mail/browse/SpamWarningView;

    iget-object v0, p0, Lcom/android/mail/browse/MessageHeaderView;->mSpamWarningView:Lcom/android/mail/browse/SpamWarningView;

    invoke-virtual {p0, v0}, Lcom/android/mail/browse/MessageHeaderView;->addView(Landroid/view/View;)V

    :cond_0
    iget-object v0, p0, Lcom/android/mail/browse/MessageHeaderView;->mSpamWarningView:Lcom/android/mail/browse/SpamWarningView;

    iget-object v1, p0, Lcom/android/mail/browse/MessageHeaderView;->mMessage:Lcom/android/mail/browse/MessageCursor$ConversationMessage;

    iget-object v2, p0, Lcom/android/mail/browse/MessageHeaderView;->mSender:Lcom/android/mail/providers/Address;

    invoke-virtual {v0, v1, v2}, Lcom/android/mail/browse/SpamWarningView;->showSpamWarning(Lcom/android/mail/providers/Message;Lcom/android/mail/providers/Address;)V

    return-void
.end method

.method private toggleMessageDetails(Landroid/view/View;)V
    .locals 4
    .param p1    # Landroid/view/View;

    invoke-direct {p0}, Lcom/android/mail/browse/MessageHeaderView;->measureHeight()I

    move-result v1

    iget-object v2, p0, Lcom/android/mail/browse/MessageHeaderView;->mCollapsedDetailsView:Landroid/view/ViewGroup;

    if-ne p1, v2, :cond_1

    const/4 v0, 0x1

    :goto_0
    invoke-direct {p0, v0}, Lcom/android/mail/browse/MessageHeaderView;->setMessageDetailsExpanded(Z)V

    invoke-direct {p0}, Lcom/android/mail/browse/MessageHeaderView;->updateSpacerHeight()V

    iget-object v2, p0, Lcom/android/mail/browse/MessageHeaderView;->mCallbacks:Lcom/android/mail/browse/MessageHeaderView$MessageHeaderViewCallbacks;

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/android/mail/browse/MessageHeaderView;->mCallbacks:Lcom/android/mail/browse/MessageHeaderView$MessageHeaderViewCallbacks;

    iget-object v3, p0, Lcom/android/mail/browse/MessageHeaderView;->mMessageHeaderItem:Lcom/android/mail/browse/ConversationViewAdapter$MessageHeaderItem;

    invoke-interface {v2, v3, v0, v1}, Lcom/android/mail/browse/MessageHeaderView$MessageHeaderViewCallbacks;->setMessageDetailsExpanded(Lcom/android/mail/browse/ConversationViewAdapter$MessageHeaderItem;ZI)V

    :cond_0
    return-void

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private updateChildVisibility()V
    .locals 9

    const/4 v6, 0x3

    const/4 v8, 0x2

    const/16 v3, 0x8

    const/4 v7, 0x1

    const/4 v4, 0x0

    invoke-virtual {p0}, Lcom/android/mail/browse/MessageHeaderView;->isExpanded()Z

    move-result v2

    if-eqz v2, :cond_3

    iget-boolean v2, p0, Lcom/android/mail/browse/MessageHeaderView;->mIsSnappy:Z

    if-eqz v2, :cond_1

    move v2, v3

    :goto_0
    invoke-virtual {p0, v2}, Lcom/android/mail/browse/MessageHeaderView;->setMessageDetailsVisibility(I)V

    iget-boolean v2, p0, Lcom/android/mail/browse/MessageHeaderView;->mIsDraft:Z

    if-eqz v2, :cond_2

    const/16 v1, 0x8

    const/4 v0, 0x0

    :goto_1
    invoke-direct {p0}, Lcom/android/mail/browse/MessageHeaderView;->setReplyOrReplyAllVisible()V

    const/4 v2, 0x5

    new-array v2, v2, [Landroid/view/View;

    iget-object v5, p0, Lcom/android/mail/browse/MessageHeaderView;->mPhotoView:Landroid/widget/QuickContactBadge;

    aput-object v5, v2, v4

    iget-object v5, p0, Lcom/android/mail/browse/MessageHeaderView;->mPhotoSpacerView:Landroid/view/View;

    aput-object v5, v2, v7

    iget-object v5, p0, Lcom/android/mail/browse/MessageHeaderView;->mForwardButton:Landroid/view/View;

    aput-object v5, v2, v8

    iget-object v5, p0, Lcom/android/mail/browse/MessageHeaderView;->mSenderEmailView:Landroid/widget/TextView;

    aput-object v5, v2, v6

    const/4 v5, 0x4

    iget-object v6, p0, Lcom/android/mail/browse/MessageHeaderView;->mOverflowButton:Landroid/view/View;

    aput-object v6, v2, v5

    invoke-direct {p0, v1, v2}, Lcom/android/mail/browse/MessageHeaderView;->setChildVisibility(I[Landroid/view/View;)V

    new-array v2, v8, [Landroid/view/View;

    iget-object v5, p0, Lcom/android/mail/browse/MessageHeaderView;->mDraftIcon:Landroid/view/View;

    aput-object v5, v2, v4

    iget-object v5, p0, Lcom/android/mail/browse/MessageHeaderView;->mEditDraftButton:Landroid/view/View;

    aput-object v5, v2, v7

    invoke-direct {p0, v0, v2}, Lcom/android/mail/browse/MessageHeaderView;->setChildVisibility(I[Landroid/view/View;)V

    new-array v2, v8, [Landroid/view/View;

    iget-object v5, p0, Lcom/android/mail/browse/MessageHeaderView;->mAttachmentIcon:Landroid/view/View;

    aput-object v5, v2, v4

    iget-object v5, p0, Lcom/android/mail/browse/MessageHeaderView;->mUpperDateView:Landroid/widget/TextView;

    aput-object v5, v2, v7

    invoke-direct {p0, v3, v2}, Lcom/android/mail/browse/MessageHeaderView;->setChildVisibility(I[Landroid/view/View;)V

    iget-boolean v2, p0, Lcom/android/mail/browse/MessageHeaderView;->mStarShown:Z

    if-eqz v2, :cond_0

    move v3, v4

    :cond_0
    new-array v2, v7, [Landroid/view/View;

    iget-object v5, p0, Lcom/android/mail/browse/MessageHeaderView;->mStarView:Landroid/widget/ImageView;

    aput-object v5, v2, v4

    invoke-direct {p0, v3, v2}, Lcom/android/mail/browse/MessageHeaderView;->setChildVisibility(I[Landroid/view/View;)V

    iget-object v2, p0, Lcom/android/mail/browse/MessageHeaderView;->mTitleContainerView:Landroid/view/ViewGroup;

    invoke-static {v2, v4}, Lcom/android/mail/browse/MessageHeaderView;->setChildMarginRight(Landroid/view/View;I)V

    :goto_2
    return-void

    :cond_1
    move v2, v4

    goto :goto_0

    :cond_2
    const/4 v1, 0x0

    const/16 v0, 0x8

    goto :goto_1

    :cond_3
    invoke-virtual {p0, v3}, Lcom/android/mail/browse/MessageHeaderView;->setMessageDetailsVisibility(I)V

    new-array v2, v8, [Landroid/view/View;

    iget-object v5, p0, Lcom/android/mail/browse/MessageHeaderView;->mSenderEmailView:Landroid/widget/TextView;

    aput-object v5, v2, v4

    iget-object v5, p0, Lcom/android/mail/browse/MessageHeaderView;->mUpperDateView:Landroid/widget/TextView;

    aput-object v5, v2, v7

    invoke-direct {p0, v4, v2}, Lcom/android/mail/browse/MessageHeaderView;->setChildVisibility(I[Landroid/view/View;)V

    const/4 v2, 0x4

    new-array v2, v2, [Landroid/view/View;

    iget-object v5, p0, Lcom/android/mail/browse/MessageHeaderView;->mEditDraftButton:Landroid/view/View;

    aput-object v5, v2, v4

    iget-object v5, p0, Lcom/android/mail/browse/MessageHeaderView;->mReplyButton:Landroid/view/View;

    aput-object v5, v2, v7

    iget-object v5, p0, Lcom/android/mail/browse/MessageHeaderView;->mReplyAllButton:Landroid/view/View;

    aput-object v5, v2, v8

    iget-object v5, p0, Lcom/android/mail/browse/MessageHeaderView;->mForwardButton:Landroid/view/View;

    aput-object v5, v2, v6

    invoke-direct {p0, v3, v2}, Lcom/android/mail/browse/MessageHeaderView;->setChildVisibility(I[Landroid/view/View;)V

    new-array v2, v7, [Landroid/view/View;

    iget-object v5, p0, Lcom/android/mail/browse/MessageHeaderView;->mOverflowButton:Landroid/view/View;

    aput-object v5, v2, v4

    invoke-direct {p0, v3, v2}, Lcom/android/mail/browse/MessageHeaderView;->setChildVisibility(I[Landroid/view/View;)V

    iget-object v2, p0, Lcom/android/mail/browse/MessageHeaderView;->mMessage:Lcom/android/mail/browse/MessageCursor$ConversationMessage;

    iget-boolean v2, v2, Lcom/android/mail/browse/MessageCursor$ConversationMessage;->hasAttachments:Z

    if-eqz v2, :cond_4

    move v2, v4

    :goto_3
    new-array v5, v7, [Landroid/view/View;

    iget-object v6, p0, Lcom/android/mail/browse/MessageHeaderView;->mAttachmentIcon:Landroid/view/View;

    aput-object v6, v5, v4

    invoke-direct {p0, v2, v5}, Lcom/android/mail/browse/MessageHeaderView;->setChildVisibility(I[Landroid/view/View;)V

    iget-boolean v2, p0, Lcom/android/mail/browse/MessageHeaderView;->mCollapsedStarVisible:Z

    if-eqz v2, :cond_5

    iget-boolean v2, p0, Lcom/android/mail/browse/MessageHeaderView;->mStarShown:Z

    if-eqz v2, :cond_5

    move v2, v4

    :goto_4
    new-array v5, v7, [Landroid/view/View;

    iget-object v6, p0, Lcom/android/mail/browse/MessageHeaderView;->mStarView:Landroid/widget/ImageView;

    aput-object v6, v5, v4

    invoke-direct {p0, v2, v5}, Lcom/android/mail/browse/MessageHeaderView;->setChildVisibility(I[Landroid/view/View;)V

    iget-object v2, p0, Lcom/android/mail/browse/MessageHeaderView;->mTitleContainerView:Landroid/view/ViewGroup;

    iget v5, p0, Lcom/android/mail/browse/MessageHeaderView;->mTitleContainerCollapsedMarginRight:I

    invoke-static {v2, v5}, Lcom/android/mail/browse/MessageHeaderView;->setChildMarginRight(Landroid/view/View;I)V

    iget-boolean v2, p0, Lcom/android/mail/browse/MessageHeaderView;->mIsDraft:Z

    if-eqz v2, :cond_6

    new-array v2, v7, [Landroid/view/View;

    iget-object v5, p0, Lcom/android/mail/browse/MessageHeaderView;->mDraftIcon:Landroid/view/View;

    aput-object v5, v2, v4

    invoke-direct {p0, v4, v2}, Lcom/android/mail/browse/MessageHeaderView;->setChildVisibility(I[Landroid/view/View;)V

    new-array v2, v8, [Landroid/view/View;

    iget-object v5, p0, Lcom/android/mail/browse/MessageHeaderView;->mPhotoView:Landroid/widget/QuickContactBadge;

    aput-object v5, v2, v4

    iget-object v4, p0, Lcom/android/mail/browse/MessageHeaderView;->mPhotoSpacerView:Landroid/view/View;

    aput-object v4, v2, v7

    invoke-direct {p0, v3, v2}, Lcom/android/mail/browse/MessageHeaderView;->setChildVisibility(I[Landroid/view/View;)V

    goto :goto_2

    :cond_4
    move v2, v3

    goto :goto_3

    :cond_5
    move v2, v3

    goto :goto_4

    :cond_6
    new-array v2, v7, [Landroid/view/View;

    iget-object v5, p0, Lcom/android/mail/browse/MessageHeaderView;->mDraftIcon:Landroid/view/View;

    aput-object v5, v2, v4

    invoke-direct {p0, v3, v2}, Lcom/android/mail/browse/MessageHeaderView;->setChildVisibility(I[Landroid/view/View;)V

    new-array v2, v8, [Landroid/view/View;

    iget-object v3, p0, Lcom/android/mail/browse/MessageHeaderView;->mPhotoView:Landroid/widget/QuickContactBadge;

    aput-object v3, v2, v4

    iget-object v3, p0, Lcom/android/mail/browse/MessageHeaderView;->mPhotoSpacerView:Landroid/view/View;

    aput-object v3, v2, v7

    invoke-direct {p0, v4, v2}, Lcom/android/mail/browse/MessageHeaderView;->setChildVisibility(I[Landroid/view/View;)V

    goto/16 :goto_2
.end method

.method private updateContactInfo()V
    .locals 10

    const/4 v9, 0x1

    const/4 v8, 0x0

    iget-object v4, p0, Lcom/android/mail/browse/MessageHeaderView;->mPresenceView:Landroid/widget/ImageView;

    const/4 v5, 0x0

    invoke-virtual {v4, v5}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    iget-object v4, p0, Lcom/android/mail/browse/MessageHeaderView;->mPresenceView:Landroid/widget/ImageView;

    const/16 v5, 0x8

    invoke-virtual {v4, v5}, Landroid/widget/ImageView;->setVisibility(I)V

    iget-object v4, p0, Lcom/android/mail/browse/MessageHeaderView;->mContactInfoSource:Lcom/android/mail/ContactInfoSource;

    if-eqz v4, :cond_0

    iget-object v4, p0, Lcom/android/mail/browse/MessageHeaderView;->mSender:Lcom/android/mail/providers/Address;

    if-nez v4, :cond_2

    :cond_0
    iget-object v4, p0, Lcom/android/mail/browse/MessageHeaderView;->mPhotoView:Landroid/widget/QuickContactBadge;

    invoke-virtual {v4}, Landroid/widget/QuickContactBadge;->setImageToDefault()V

    iget-object v4, p0, Lcom/android/mail/browse/MessageHeaderView;->mPhotoView:Landroid/widget/QuickContactBadge;

    invoke-virtual {p0}, Lcom/android/mail/browse/MessageHeaderView;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    const v6, 0x7f0900c7

    invoke-virtual {v5, v6}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Landroid/widget/QuickContactBadge;->setContentDescription(Ljava/lang/CharSequence;)V

    :cond_1
    :goto_0
    return-void

    :cond_2
    invoke-virtual {p0}, Lcom/android/mail/browse/MessageHeaderView;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    const v6, 0x7f0900c6

    new-array v7, v9, [Ljava/lang/Object;

    iget-object v4, p0, Lcom/android/mail/browse/MessageHeaderView;->mSender:Lcom/android/mail/providers/Address;

    invoke-virtual {v4}, Lcom/android/mail/providers/Address;->getName()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-nez v4, :cond_5

    iget-object v4, p0, Lcom/android/mail/browse/MessageHeaderView;->mSender:Lcom/android/mail/providers/Address;

    invoke-virtual {v4}, Lcom/android/mail/providers/Address;->getName()Ljava/lang/String;

    move-result-object v4

    :goto_1
    aput-object v4, v7, v8

    invoke-virtual {v5, v6, v7}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    iget-object v4, p0, Lcom/android/mail/browse/MessageHeaderView;->mPhotoView:Landroid/widget/QuickContactBadge;

    invoke-virtual {v4, v0}, Landroid/widget/QuickContactBadge;->setContentDescription(Ljava/lang/CharSequence;)V

    const/4 v3, 0x0

    iget-object v4, p0, Lcom/android/mail/browse/MessageHeaderView;->mSender:Lcom/android/mail/providers/Address;

    invoke-virtual {v4}, Lcom/android/mail/providers/Address;->getAddress()Ljava/lang/String;

    move-result-object v1

    iget-object v4, p0, Lcom/android/mail/browse/MessageHeaderView;->mContactInfoSource:Lcom/android/mail/ContactInfoSource;

    invoke-interface {v4, v1}, Lcom/android/mail/ContactInfoSource;->getContactInfo(Ljava/lang/String;)Lcom/android/mail/ContactInfo;

    move-result-object v2

    if-eqz v2, :cond_6

    iget-object v4, p0, Lcom/android/mail/browse/MessageHeaderView;->mPhotoView:Landroid/widget/QuickContactBadge;

    iget-object v5, v2, Lcom/android/mail/ContactInfo;->contactUri:Landroid/net/Uri;

    invoke-virtual {v4, v5}, Landroid/widget/QuickContactBadge;->assignContactUri(Landroid/net/Uri;)V

    iget-object v4, v2, Lcom/android/mail/ContactInfo;->photo:Landroid/graphics/Bitmap;

    if-eqz v4, :cond_3

    iget-object v4, p0, Lcom/android/mail/browse/MessageHeaderView;->mPhotoView:Landroid/widget/QuickContactBadge;

    iget-object v5, v2, Lcom/android/mail/ContactInfo;->photo:Landroid/graphics/Bitmap;

    invoke-virtual {v4, v5}, Landroid/widget/QuickContactBadge;->setImageBitmap(Landroid/graphics/Bitmap;)V

    new-array v4, v9, [Ljava/lang/Object;

    iget-object v5, p0, Lcom/android/mail/browse/MessageHeaderView;->mSender:Lcom/android/mail/providers/Address;

    invoke-virtual {v5}, Lcom/android/mail/providers/Address;->getName()Ljava/lang/String;

    move-result-object v5

    aput-object v5, v4, v8

    invoke-static {v0, v4}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    const/4 v3, 0x1

    :cond_3
    iget-boolean v4, p0, Lcom/android/mail/browse/MessageHeaderView;->mIsDraft:Z

    if-nez v4, :cond_4

    iget-object v4, v2, Lcom/android/mail/ContactInfo;->status:Ljava/lang/Integer;

    if-eqz v4, :cond_4

    iget-object v4, p0, Lcom/android/mail/browse/MessageHeaderView;->mPresenceView:Landroid/widget/ImageView;

    iget-object v5, v2, Lcom/android/mail/ContactInfo;->status:Ljava/lang/Integer;

    invoke-virtual {v5}, Ljava/lang/Integer;->intValue()I

    move-result v5

    invoke-static {v5}, Landroid/provider/ContactsContract$StatusUpdates;->getPresenceIconResourceId(I)I

    move-result v5

    invoke-virtual {v4, v5}, Landroid/widget/ImageView;->setImageResource(I)V

    iget-object v4, p0, Lcom/android/mail/browse/MessageHeaderView;->mPresenceView:Landroid/widget/ImageView;

    invoke-virtual {v4, v8}, Landroid/widget/ImageView;->setVisibility(I)V

    :cond_4
    :goto_2
    if-nez v3, :cond_1

    iget-object v4, p0, Lcom/android/mail/browse/MessageHeaderView;->mPhotoView:Landroid/widget/QuickContactBadge;

    invoke-virtual {v4}, Landroid/widget/QuickContactBadge;->setImageToDefault()V

    goto :goto_0

    :cond_5
    iget-object v4, p0, Lcom/android/mail/browse/MessageHeaderView;->mSender:Lcom/android/mail/providers/Address;

    invoke-virtual {v4}, Lcom/android/mail/providers/Address;->getAddress()Ljava/lang/String;

    move-result-object v4

    goto :goto_1

    :cond_6
    iget-object v4, p0, Lcom/android/mail/browse/MessageHeaderView;->mPhotoView:Landroid/widget/QuickContactBadge;

    invoke-virtual {v4, v1, v9}, Landroid/widget/QuickContactBadge;->assignContactFromEmail(Ljava/lang/String;Z)V

    goto :goto_2
.end method

.method private updateSpacerHeight()V
    .locals 3

    invoke-direct {p0}, Lcom/android/mail/browse/MessageHeaderView;->measureHeight()I

    move-result v0

    iget-object v1, p0, Lcom/android/mail/browse/MessageHeaderView;->mMessageHeaderItem:Lcom/android/mail/browse/ConversationViewAdapter$MessageHeaderItem;

    invoke-virtual {v1, v0}, Lcom/android/mail/browse/ConversationViewAdapter$MessageHeaderItem;->setHeight(I)Z

    iget-object v1, p0, Lcom/android/mail/browse/MessageHeaderView;->mCallbacks:Lcom/android/mail/browse/MessageHeaderView$MessageHeaderViewCallbacks;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/android/mail/browse/MessageHeaderView;->mCallbacks:Lcom/android/mail/browse/MessageHeaderView$MessageHeaderViewCallbacks;

    iget-object v2, p0, Lcom/android/mail/browse/MessageHeaderView;->mMessageHeaderItem:Lcom/android/mail/browse/ConversationViewAdapter$MessageHeaderItem;

    invoke-interface {v1, v2, v0}, Lcom/android/mail/browse/MessageHeaderView$MessageHeaderViewCallbacks;->setMessageSpacerHeight(Lcom/android/mail/browse/ConversationViewAdapter$MessageHeaderItem;I)V

    :cond_0
    return-void
.end method


# virtual methods
.method public bind(Lcom/android/mail/browse/ConversationViewAdapter$MessageHeaderItem;Z)V
    .locals 1
    .param p1    # Lcom/android/mail/browse/ConversationViewAdapter$MessageHeaderItem;
    .param p2    # Z

    iget-object v0, p0, Lcom/android/mail/browse/MessageHeaderView;->mMessageHeaderItem:Lcom/android/mail/browse/ConversationViewAdapter$MessageHeaderItem;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/mail/browse/MessageHeaderView;->mMessageHeaderItem:Lcom/android/mail/browse/ConversationViewAdapter$MessageHeaderItem;

    if-ne v0, p1, :cond_0

    :goto_0
    return-void

    :cond_0
    iput-object p1, p0, Lcom/android/mail/browse/MessageHeaderView;->mMessageHeaderItem:Lcom/android/mail/browse/ConversationViewAdapter$MessageHeaderItem;

    invoke-direct {p0, p2}, Lcom/android/mail/browse/MessageHeaderView;->render(Z)V

    goto :goto_0
.end method

.method public hideMessageDetails()V
    .locals 1

    const/16 v0, 0x8

    invoke-virtual {p0, v0}, Lcom/android/mail/browse/MessageHeaderView;->setMessageDetailsVisibility(I)V

    return-void
.end method

.method public initialize(Lcom/android/mail/FormattedDateBuilder;Lcom/android/mail/browse/ConversationAccountController;Ljava/util/Map;)V
    .locals 0
    .param p1    # Lcom/android/mail/FormattedDateBuilder;
    .param p2    # Lcom/android/mail/browse/ConversationAccountController;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/android/mail/FormattedDateBuilder;",
            "Lcom/android/mail/browse/ConversationAccountController;",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Lcom/android/mail/providers/Address;",
            ">;)V"
        }
    .end annotation

    iput-object p1, p0, Lcom/android/mail/browse/MessageHeaderView;->mDateBuilder:Lcom/android/mail/FormattedDateBuilder;

    iput-object p2, p0, Lcom/android/mail/browse/MessageHeaderView;->mAccountController:Lcom/android/mail/browse/ConversationAccountController;

    iput-object p3, p0, Lcom/android/mail/browse/MessageHeaderView;->mAddressCache:Ljava/util/Map;

    return-void
.end method

.method public isBoundTo(Lcom/android/mail/browse/ConversationOverlayItem;)Z
    .locals 1
    .param p1    # Lcom/android/mail/browse/ConversationOverlayItem;

    iget-object v0, p0, Lcom/android/mail/browse/MessageHeaderView;->mMessageHeaderItem:Lcom/android/mail/browse/ConversationViewAdapter$MessageHeaderItem;

    if-ne p1, v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isExpanded()Z
    .locals 1

    iget-object v0, p0, Lcom/android/mail/browse/MessageHeaderView;->mMessageHeaderItem:Lcom/android/mail/browse/ConversationViewAdapter$MessageHeaderItem;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/mail/browse/MessageHeaderView;->mMessageHeaderItem:Lcom/android/mail/browse/ConversationViewAdapter$MessageHeaderItem;

    invoke-virtual {v0}, Lcom/android/mail/browse/ConversationViewAdapter$MessageHeaderItem;->isExpanded()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public onClick(Landroid/view/View;)V
    .locals 1
    .param p1    # Landroid/view/View;

    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v0

    invoke-virtual {p0, p1, v0}, Lcom/android/mail/browse/MessageHeaderView;->onClick(Landroid/view/View;I)Z

    return-void
.end method

.method public onClick(Landroid/view/View;I)Z
    .locals 5

    const/4 v1, 0x1

    const/4 v0, 0x0

    iget-object v2, p0, Lcom/android/mail/browse/MessageHeaderView;->mMessage:Lcom/android/mail/browse/MessageCursor$ConversationMessage;

    if-nez v2, :cond_0

    sget-object v1, Lcom/android/mail/browse/MessageHeaderView;->LOG_TAG:Ljava/lang/String;

    const-string v2, "ignoring message header tap on unbound view"

    new-array v3, v0, [Ljava/lang/Object;

    invoke-static {v1, v2, v3}, Lcom/android/mail/utils/LogUtils;->i(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)I

    :goto_0
    return v0

    :cond_0
    sparse-switch p2, :sswitch_data_0

    sget-object v2, Lcom/android/mail/browse/MessageHeaderView;->LOG_TAG:Ljava/lang/String;

    const-string v3, "unrecognized header tap: %d"

    new-array v1, v1, [Ljava/lang/Object;

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v1, v0

    invoke-static {v2, v3, v1}, Lcom/android/mail/utils/LogUtils;->i(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)I

    move v1, v0

    :cond_1
    :goto_1
    move v0, v1

    goto :goto_0

    :sswitch_0
    invoke-virtual {p0}, Lcom/android/mail/browse/MessageHeaderView;->getContext()Landroid/content/Context;

    move-result-object v0

    const-string v2, "clipboard"

    invoke-virtual {v0, v2}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/ClipboardManager;

    const-string v2, ""

    iget-object v3, p0, Lcom/android/mail/browse/MessageHeaderView;->mCopyAddress:Ljava/lang/String;

    invoke-static {v2, v3}, Landroid/content/ClipData;->newPlainText(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Landroid/content/ClipData;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/content/ClipboardManager;->setPrimaryClip(Landroid/content/ClipData;)V

    iget-object v0, p0, Lcom/android/mail/browse/MessageHeaderView;->mEmailCopyPopup:Landroid/app/Dialog;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/android/mail/browse/MessageHeaderView;->mEmailCopyPopup:Landroid/app/Dialog;

    invoke-virtual {v0}, Landroid/app/Dialog;->dismiss()V

    goto :goto_1

    :sswitch_1
    invoke-virtual {p0}, Lcom/android/mail/browse/MessageHeaderView;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-direct {p0}, Lcom/android/mail/browse/MessageHeaderView;->getAccount()Lcom/android/mail/providers/Account;

    move-result-object v2

    iget-object v3, p0, Lcom/android/mail/browse/MessageHeaderView;->mMessage:Lcom/android/mail/browse/MessageCursor$ConversationMessage;

    invoke-static {v0, v2, v3}, Lcom/android/mail/compose/ComposeActivity;->reply(Landroid/content/Context;Lcom/android/mail/providers/Account;Lcom/android/mail/providers/Message;)V

    goto :goto_1

    :sswitch_2
    invoke-virtual {p0}, Lcom/android/mail/browse/MessageHeaderView;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-direct {p0}, Lcom/android/mail/browse/MessageHeaderView;->getAccount()Lcom/android/mail/providers/Account;

    move-result-object v2

    iget-object v3, p0, Lcom/android/mail/browse/MessageHeaderView;->mMessage:Lcom/android/mail/browse/MessageCursor$ConversationMessage;

    invoke-static {v0, v2, v3}, Lcom/android/mail/compose/ComposeActivity;->replyAll(Landroid/content/Context;Lcom/android/mail/providers/Account;Lcom/android/mail/providers/Message;)V

    goto :goto_1

    :sswitch_3
    invoke-virtual {p0}, Lcom/android/mail/browse/MessageHeaderView;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-direct {p0}, Lcom/android/mail/browse/MessageHeaderView;->getAccount()Lcom/android/mail/providers/Account;

    move-result-object v2

    iget-object v3, p0, Lcom/android/mail/browse/MessageHeaderView;->mMessage:Lcom/android/mail/browse/MessageCursor$ConversationMessage;

    invoke-static {v0, v2, v3}, Lcom/android/mail/compose/ComposeActivity;->forward(Landroid/content/Context;Lcom/android/mail/providers/Account;Lcom/android/mail/providers/Message;)V

    goto :goto_1

    :sswitch_4
    invoke-virtual {p1}, Landroid/view/View;->isSelected()Z

    move-result v2

    if-nez v2, :cond_2

    move v0, v1

    :cond_2
    invoke-virtual {p1, v0}, Landroid/view/View;->setSelected(Z)V

    iget-object v2, p0, Lcom/android/mail/browse/MessageHeaderView;->mMessage:Lcom/android/mail/browse/MessageCursor$ConversationMessage;

    invoke-virtual {v2, v0}, Lcom/android/mail/browse/MessageCursor$ConversationMessage;->star(Z)V

    goto :goto_1

    :sswitch_5
    invoke-virtual {p0}, Lcom/android/mail/browse/MessageHeaderView;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-direct {p0}, Lcom/android/mail/browse/MessageHeaderView;->getAccount()Lcom/android/mail/providers/Account;

    move-result-object v2

    iget-object v3, p0, Lcom/android/mail/browse/MessageHeaderView;->mMessage:Lcom/android/mail/browse/MessageCursor$ConversationMessage;

    invoke-static {v0, v2, v3}, Lcom/android/mail/compose/ComposeActivity;->editDraft(Landroid/content/Context;Lcom/android/mail/providers/Account;Lcom/android/mail/providers/Message;)V

    goto :goto_1

    :sswitch_6
    iget-object v2, p0, Lcom/android/mail/browse/MessageHeaderView;->mPopup:Landroid/widget/PopupMenu;

    if-nez v2, :cond_3

    new-instance v2, Landroid/widget/PopupMenu;

    invoke-virtual {p0}, Lcom/android/mail/browse/MessageHeaderView;->getContext()Landroid/content/Context;

    move-result-object v3

    invoke-direct {v2, v3, p1}, Landroid/widget/PopupMenu;-><init>(Landroid/content/Context;Landroid/view/View;)V

    iput-object v2, p0, Lcom/android/mail/browse/MessageHeaderView;->mPopup:Landroid/widget/PopupMenu;

    iget-object v2, p0, Lcom/android/mail/browse/MessageHeaderView;->mPopup:Landroid/widget/PopupMenu;

    invoke-virtual {v2}, Landroid/widget/PopupMenu;->getMenuInflater()Landroid/view/MenuInflater;

    move-result-object v2

    const v3, 0x7f11000a

    iget-object v4, p0, Lcom/android/mail/browse/MessageHeaderView;->mPopup:Landroid/widget/PopupMenu;

    invoke-virtual {v4}, Landroid/widget/PopupMenu;->getMenu()Landroid/view/Menu;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Landroid/view/MenuInflater;->inflate(ILandroid/view/Menu;)V

    iget-object v2, p0, Lcom/android/mail/browse/MessageHeaderView;->mPopup:Landroid/widget/PopupMenu;

    invoke-virtual {v2, p0}, Landroid/widget/PopupMenu;->setOnMenuItemClickListener(Landroid/widget/PopupMenu$OnMenuItemClickListener;)V

    :cond_3
    invoke-direct {p0}, Lcom/android/mail/browse/MessageHeaderView;->getAccount()Lcom/android/mail/providers/Account;

    move-result-object v2

    iget-object v2, v2, Lcom/android/mail/providers/Account;->settings:Lcom/android/mail/providers/Settings;

    iget v2, v2, Lcom/android/mail/providers/Settings;->replyBehavior:I

    if-ne v2, v1, :cond_5

    move v2, v1

    :goto_2
    iget-object v3, p0, Lcom/android/mail/browse/MessageHeaderView;->mPopup:Landroid/widget/PopupMenu;

    invoke-virtual {v3}, Landroid/widget/PopupMenu;->getMenu()Landroid/view/Menu;

    move-result-object v3

    const v4, 0x7f080098

    invoke-interface {v3, v4}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v3

    invoke-interface {v3, v2}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    iget-object v3, p0, Lcom/android/mail/browse/MessageHeaderView;->mPopup:Landroid/widget/PopupMenu;

    invoke-virtual {v3}, Landroid/widget/PopupMenu;->getMenu()Landroid/view/Menu;

    move-result-object v3

    const v4, 0x7f080099

    invoke-interface {v3, v4}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v3

    if-nez v2, :cond_4

    move v0, v1

    :cond_4
    invoke-interface {v3, v0}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    iget-object v0, p0, Lcom/android/mail/browse/MessageHeaderView;->mPopup:Landroid/widget/PopupMenu;

    invoke-virtual {v0}, Landroid/widget/PopupMenu;->show()V

    goto/16 :goto_1

    :cond_5
    move v2, v0

    goto :goto_2

    :sswitch_7
    invoke-direct {p0, p1}, Lcom/android/mail/browse/MessageHeaderView;->toggleMessageDetails(Landroid/view/View;)V

    goto/16 :goto_1

    :sswitch_8
    invoke-virtual {p0}, Lcom/android/mail/browse/MessageHeaderView;->toggleExpanded()V

    goto/16 :goto_1

    :sswitch_9
    invoke-direct {p0, p1}, Lcom/android/mail/browse/MessageHeaderView;->handleShowImagePromptClick(Landroid/view/View;)V

    goto/16 :goto_1

    :sswitch_data_0
    .sparse-switch
        0x1020019 -> :sswitch_0
        0x7f080057 -> :sswitch_4
        0x7f080066 -> :sswitch_6
        0x7f08006b -> :sswitch_7
        0x7f08006e -> :sswitch_7
        0x7f080083 -> :sswitch_8
        0x7f08008a -> :sswitch_9
        0x7f080098 -> :sswitch_1
        0x7f080099 -> :sswitch_2
        0x7f08009a -> :sswitch_5
        0x7f08009b -> :sswitch_3
    .end sparse-switch
.end method

.method public onDetachedFromParent()V
    .locals 0

    invoke-virtual {p0}, Lcom/android/mail/browse/MessageHeaderView;->unbind()V

    return-void
.end method

.method protected onFinishInflate()V
    .locals 2

    const/4 v1, 0x1

    invoke-super {p0}, Landroid/widget/LinearLayout;->onFinishInflate()V

    const v0, 0x7f080083

    invoke-virtual {p0, v0}, Lcom/android/mail/browse/MessageHeaderView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    iput-object v0, p0, Lcom/android/mail/browse/MessageHeaderView;->mUpperHeaderView:Landroid/view/ViewGroup;

    const v0, 0x7f08009f

    invoke-virtual {p0, v0}, Lcom/android/mail/browse/MessageHeaderView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/android/mail/browse/MessageHeaderView;->mSenderNameView:Landroid/widget/TextView;

    const v0, 0x7f0800a1

    invoke-virtual {p0, v0}, Lcom/android/mail/browse/MessageHeaderView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/android/mail/browse/MessageHeaderView;->mSenderEmailView:Landroid/widget/TextView;

    const v0, 0x7f080094

    invoke-virtual {p0, v0}, Lcom/android/mail/browse/MessageHeaderView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/QuickContactBadge;

    iput-object v0, p0, Lcom/android/mail/browse/MessageHeaderView;->mPhotoView:Landroid/widget/QuickContactBadge;

    const v0, 0x7f080095

    invoke-virtual {p0, v0}, Lcom/android/mail/browse/MessageHeaderView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/android/mail/browse/MessageHeaderView;->mPhotoSpacerView:Landroid/view/View;

    const v0, 0x7f080098

    invoke-virtual {p0, v0}, Lcom/android/mail/browse/MessageHeaderView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/android/mail/browse/MessageHeaderView;->mReplyButton:Landroid/view/View;

    const v0, 0x7f080099

    invoke-virtual {p0, v0}, Lcom/android/mail/browse/MessageHeaderView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/android/mail/browse/MessageHeaderView;->mReplyAllButton:Landroid/view/View;

    const v0, 0x7f08009b

    invoke-virtual {p0, v0}, Lcom/android/mail/browse/MessageHeaderView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/android/mail/browse/MessageHeaderView;->mForwardButton:Landroid/view/View;

    const v0, 0x7f080057

    invoke-virtual {p0, v0}, Lcom/android/mail/browse/MessageHeaderView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/android/mail/browse/MessageHeaderView;->mStarView:Landroid/widget/ImageView;

    const v0, 0x7f0800a0

    invoke-virtual {p0, v0}, Lcom/android/mail/browse/MessageHeaderView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/android/mail/browse/MessageHeaderView;->mPresenceView:Landroid/widget/ImageView;

    const v0, 0x7f08009c

    invoke-virtual {p0, v0}, Lcom/android/mail/browse/MessageHeaderView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    iput-object v0, p0, Lcom/android/mail/browse/MessageHeaderView;->mTitleContainerView:Landroid/view/ViewGroup;

    const v0, 0x7f080066

    invoke-virtual {p0, v0}, Lcom/android/mail/browse/MessageHeaderView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/android/mail/browse/MessageHeaderView;->mOverflowButton:Landroid/view/View;

    const v0, 0x7f080096

    invoke-virtual {p0, v0}, Lcom/android/mail/browse/MessageHeaderView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/android/mail/browse/MessageHeaderView;->mDraftIcon:Landroid/view/View;

    const v0, 0x7f08009a

    invoke-virtual {p0, v0}, Lcom/android/mail/browse/MessageHeaderView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/android/mail/browse/MessageHeaderView;->mEditDraftButton:Landroid/view/View;

    const v0, 0x7f08009d

    invoke-virtual {p0, v0}, Lcom/android/mail/browse/MessageHeaderView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/android/mail/browse/MessageHeaderView;->mUpperDateView:Landroid/widget/TextView;

    const v0, 0x7f08009e

    invoke-virtual {p0, v0}, Lcom/android/mail/browse/MessageHeaderView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/android/mail/browse/MessageHeaderView;->mAttachmentIcon:Landroid/view/View;

    iget-object v0, p0, Lcom/android/mail/browse/MessageHeaderView;->mStarView:Landroid/widget/ImageView;

    invoke-virtual {v0}, Landroid/widget/ImageView;->getVisibility()I

    move-result v0

    if-nez v0, :cond_0

    move v0, v1

    :goto_0
    iput-boolean v0, p0, Lcom/android/mail/browse/MessageHeaderView;->mCollapsedStarVisible:Z

    iget-object v0, p0, Lcom/android/mail/browse/MessageHeaderView;->mTitleContainerView:Landroid/view/ViewGroup;

    invoke-virtual {v0}, Landroid/view/ViewGroup;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup$MarginLayoutParams;

    iget v0, v0, Landroid/view/ViewGroup$MarginLayoutParams;->rightMargin:I

    iput v0, p0, Lcom/android/mail/browse/MessageHeaderView;->mTitleContainerCollapsedMarginRight:I

    const v0, 0x7f080084

    invoke-virtual {p0, v0}, Lcom/android/mail/browse/MessageHeaderView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/android/mail/browse/MessageHeaderView;->mBottomBorderView:Landroid/view/View;

    const v0, 0x7f080093

    invoke-virtual {p0, v0}, Lcom/android/mail/browse/MessageHeaderView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/android/mail/browse/MessageHeaderView;->mLeftSpacer:Landroid/view/View;

    const v0, 0x7f080097

    invoke-virtual {p0, v0}, Lcom/android/mail/browse/MessageHeaderView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/android/mail/browse/MessageHeaderView;->mRightSpacer:Landroid/view/View;

    invoke-direct {p0, v1}, Lcom/android/mail/browse/MessageHeaderView;->setExpanded(Z)V

    const/4 v0, 0x7

    new-array v0, v0, [I

    fill-array-data v0, :array_0

    invoke-direct {p0, v0}, Lcom/android/mail/browse/MessageHeaderView;->registerMessageClickTargets([I)V

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    nop

    :array_0
    .array-data 4
        0x7f080098
        0x7f080099
        0x7f08009b
        0x7f080057
        0x7f08009a
        0x7f080066
        0x7f080083
    .end array-data
.end method

.method protected onLayout(ZIIII)V
    .locals 2

    new-instance v0, Lcom/android/mail/perf/Timer;

    invoke-direct {v0}, Lcom/android/mail/perf/Timer;-><init>()V

    const-string v1, "message header layout"

    invoke-virtual {v0, v1}, Lcom/android/mail/perf/Timer;->start(Ljava/lang/String;)V

    invoke-super/range {p0 .. p5}, Landroid/widget/LinearLayout;->onLayout(ZIIII)V

    const-string v1, "message header layout"

    invoke-virtual {v0, v1}, Lcom/android/mail/perf/Timer;->pause(Ljava/lang/String;)V

    return-void
.end method

.method public onLongClick(Landroid/view/View;)Z
    .locals 1
    .param p1    # Landroid/view/View;

    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v0

    invoke-direct {p0, v0}, Lcom/android/mail/browse/MessageHeaderView;->onLongClick(I)Z

    move-result v0

    return v0
.end method

.method protected onMeasure(II)V
    .locals 2

    new-instance v0, Lcom/android/mail/perf/Timer;

    invoke-direct {v0}, Lcom/android/mail/perf/Timer;-><init>()V

    invoke-super {p0, p1, p2}, Landroid/widget/LinearLayout;->onMeasure(II)V

    iget-boolean v1, p0, Lcom/android/mail/browse/MessageHeaderView;->mPreMeasuring:Z

    if-nez v1, :cond_0

    const-string v1, "message header measure"

    invoke-virtual {v0, v1}, Lcom/android/mail/perf/Timer;->pause(Ljava/lang/String;)V

    :cond_0
    return-void
.end method

.method public onMenuItemClick(Landroid/view/MenuItem;)Z
    .locals 2
    .param p1    # Landroid/view/MenuItem;

    iget-object v0, p0, Lcom/android/mail/browse/MessageHeaderView;->mPopup:Landroid/widget/PopupMenu;

    invoke-virtual {v0}, Landroid/widget/PopupMenu;->dismiss()V

    const/4 v0, 0x0

    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v1

    invoke-virtual {p0, v0, v1}, Lcom/android/mail/browse/MessageHeaderView;->onClick(Landroid/view/View;I)Z

    move-result v0

    return v0
.end method

.method public refresh()V
    .locals 1

    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/android/mail/browse/MessageHeaderView;->render(Z)V

    return-void
.end method

.method public setCallbacks(Lcom/android/mail/browse/MessageHeaderView$MessageHeaderViewCallbacks;)V
    .locals 0
    .param p1    # Lcom/android/mail/browse/MessageHeaderView$MessageHeaderViewCallbacks;

    iput-object p1, p0, Lcom/android/mail/browse/MessageHeaderView;->mCallbacks:Lcom/android/mail/browse/MessageHeaderView$MessageHeaderViewCallbacks;

    return-void
.end method

.method public setContactInfoSource(Lcom/android/mail/ContactInfoSource;)V
    .locals 0
    .param p1    # Lcom/android/mail/ContactInfoSource;

    iput-object p1, p0, Lcom/android/mail/browse/MessageHeaderView;->mContactInfoSource:Lcom/android/mail/ContactInfoSource;

    return-void
.end method

.method public setExpandMode(I)V
    .locals 0
    .param p1    # I

    iput p1, p0, Lcom/android/mail/browse/MessageHeaderView;->mExpandMode:I

    return-void
.end method

.method public setExpandable(Z)V
    .locals 0
    .param p1    # Z

    iput-boolean p1, p0, Lcom/android/mail/browse/MessageHeaderView;->mExpandable:Z

    return-void
.end method

.method public setMessageDetailsVisibility(I)V
    .locals 1
    .param p1    # I

    const/16 v0, 0x8

    if-ne p1, v0, :cond_1

    invoke-direct {p0}, Lcom/android/mail/browse/MessageHeaderView;->hideCollapsedDetails()V

    invoke-direct {p0}, Lcom/android/mail/browse/MessageHeaderView;->hideExpandedDetails()V

    invoke-direct {p0}, Lcom/android/mail/browse/MessageHeaderView;->hideSpamWarning()V

    invoke-direct {p0}, Lcom/android/mail/browse/MessageHeaderView;->hideShowImagePrompt()V

    invoke-direct {p0}, Lcom/android/mail/browse/MessageHeaderView;->hideInvite()V

    :goto_0
    iget-object v0, p0, Lcom/android/mail/browse/MessageHeaderView;->mBottomBorderView:Landroid/view/View;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/mail/browse/MessageHeaderView;->mBottomBorderView:Landroid/view/View;

    invoke-virtual {v0, p1}, Landroid/view/View;->setVisibility(I)V

    :cond_0
    return-void

    :cond_1
    iget-object v0, p0, Lcom/android/mail/browse/MessageHeaderView;->mMessageHeaderItem:Lcom/android/mail/browse/ConversationViewAdapter$MessageHeaderItem;

    iget-boolean v0, v0, Lcom/android/mail/browse/ConversationViewAdapter$MessageHeaderItem;->detailsExpanded:Z

    invoke-direct {p0, v0}, Lcom/android/mail/browse/MessageHeaderView;->setMessageDetailsExpanded(Z)V

    iget-object v0, p0, Lcom/android/mail/browse/MessageHeaderView;->mMessage:Lcom/android/mail/browse/MessageCursor$ConversationMessage;

    iget-object v0, v0, Lcom/android/mail/browse/MessageCursor$ConversationMessage;->spamWarningString:Ljava/lang/String;

    if-nez v0, :cond_2

    invoke-direct {p0}, Lcom/android/mail/browse/MessageHeaderView;->hideSpamWarning()V

    :goto_1
    iget-boolean v0, p0, Lcom/android/mail/browse/MessageHeaderView;->mShowImagePrompt:Z

    if-eqz v0, :cond_4

    iget-object v0, p0, Lcom/android/mail/browse/MessageHeaderView;->mMessageHeaderItem:Lcom/android/mail/browse/ConversationViewAdapter$MessageHeaderItem;

    invoke-virtual {v0}, Lcom/android/mail/browse/ConversationViewAdapter$MessageHeaderItem;->getShowImages()Z

    move-result v0

    if-eqz v0, :cond_3

    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/android/mail/browse/MessageHeaderView;->showImagePromptAlways(Z)V

    :goto_2
    iget-object v0, p0, Lcom/android/mail/browse/MessageHeaderView;->mMessage:Lcom/android/mail/browse/MessageCursor$ConversationMessage;

    invoke-virtual {v0}, Lcom/android/mail/browse/MessageCursor$ConversationMessage;->isFlaggedCalendarInvite()Z

    move-result v0

    if-eqz v0, :cond_5

    invoke-direct {p0}, Lcom/android/mail/browse/MessageHeaderView;->showInvite()V

    goto :goto_0

    :cond_2
    invoke-direct {p0}, Lcom/android/mail/browse/MessageHeaderView;->showSpamWarning()V

    goto :goto_1

    :cond_3
    invoke-direct {p0}, Lcom/android/mail/browse/MessageHeaderView;->showImagePromptOnce()V

    goto :goto_2

    :cond_4
    invoke-direct {p0}, Lcom/android/mail/browse/MessageHeaderView;->hideShowImagePrompt()V

    goto :goto_2

    :cond_5
    invoke-direct {p0}, Lcom/android/mail/browse/MessageHeaderView;->hideInvite()V

    goto :goto_0
.end method

.method public setSnappy(Z)V
    .locals 3
    .param p1    # Z

    const/16 v2, 0x8

    const/4 v1, 0x0

    iput-boolean p1, p0, Lcom/android/mail/browse/MessageHeaderView;->mIsSnappy:Z

    invoke-virtual {p0}, Lcom/android/mail/browse/MessageHeaderView;->hideMessageDetails()V

    if-eqz p1, :cond_0

    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/android/mail/browse/MessageHeaderView;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    iget-object v0, p0, Lcom/android/mail/browse/MessageHeaderView;->mLeftSpacer:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    iget-object v0, p0, Lcom/android/mail/browse/MessageHeaderView;->mRightSpacer:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    :goto_0
    return-void

    :cond_0
    const v0, 0x106000b

    invoke-virtual {p0, v0}, Lcom/android/mail/browse/MessageHeaderView;->setBackgroundColor(I)V

    iget-object v0, p0, Lcom/android/mail/browse/MessageHeaderView;->mLeftSpacer:Landroid/view/View;

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    iget-object v0, p0, Lcom/android/mail/browse/MessageHeaderView;->mRightSpacer:Landroid/view/View;

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    goto :goto_0
.end method

.method public setVeiledMatcher(Lcom/android/mail/utils/VeiledAddressMatcher;)V
    .locals 0
    .param p1    # Lcom/android/mail/utils/VeiledAddressMatcher;

    iput-object p1, p0, Lcom/android/mail/browse/MessageHeaderView;->mVeiledMatcher:Lcom/android/mail/utils/VeiledAddressMatcher;

    return-void
.end method

.method public toggleExpanded()V
    .locals 3

    iget-boolean v1, p0, Lcom/android/mail/browse/MessageHeaderView;->mExpandable:Z

    if-nez v1, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    invoke-virtual {p0}, Lcom/android/mail/browse/MessageHeaderView;->isExpanded()Z

    move-result v1

    if-nez v1, :cond_3

    const/4 v1, 0x1

    :goto_1
    invoke-direct {p0, v1}, Lcom/android/mail/browse/MessageHeaderView;->setExpanded(Z)V

    iget-boolean v1, p0, Lcom/android/mail/browse/MessageHeaderView;->mIsSnappy:Z

    if-nez v1, :cond_2

    iget-object v1, p0, Lcom/android/mail/browse/MessageHeaderView;->mSenderNameView:Landroid/widget/TextView;

    invoke-direct {p0}, Lcom/android/mail/browse/MessageHeaderView;->getHeaderTitle()Ljava/lang/CharSequence;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v1, p0, Lcom/android/mail/browse/MessageHeaderView;->mSenderEmailView:Landroid/widget/TextView;

    invoke-direct {p0}, Lcom/android/mail/browse/MessageHeaderView;->getHeaderSubtitle()Ljava/lang/CharSequence;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    :cond_2
    invoke-direct {p0}, Lcom/android/mail/browse/MessageHeaderView;->updateChildVisibility()V

    invoke-direct {p0}, Lcom/android/mail/browse/MessageHeaderView;->measureHeight()I

    move-result v0

    iget-object v1, p0, Lcom/android/mail/browse/MessageHeaderView;->mMessageHeaderItem:Lcom/android/mail/browse/ConversationViewAdapter$MessageHeaderItem;

    invoke-virtual {v1, v0}, Lcom/android/mail/browse/ConversationViewAdapter$MessageHeaderItem;->setHeight(I)Z

    iget-object v1, p0, Lcom/android/mail/browse/MessageHeaderView;->mCallbacks:Lcom/android/mail/browse/MessageHeaderView$MessageHeaderViewCallbacks;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/android/mail/browse/MessageHeaderView;->mCallbacks:Lcom/android/mail/browse/MessageHeaderView$MessageHeaderViewCallbacks;

    iget-object v2, p0, Lcom/android/mail/browse/MessageHeaderView;->mMessageHeaderItem:Lcom/android/mail/browse/ConversationViewAdapter$MessageHeaderItem;

    invoke-interface {v1, v2, v0}, Lcom/android/mail/browse/MessageHeaderView$MessageHeaderViewCallbacks;->setMessageExpanded(Lcom/android/mail/browse/ConversationViewAdapter$MessageHeaderItem;I)V

    goto :goto_0

    :cond_3
    const/4 v1, 0x0

    goto :goto_1
.end method

.method public unbind()V
    .locals 2

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/android/mail/browse/MessageHeaderView;->mMessageHeaderItem:Lcom/android/mail/browse/ConversationViewAdapter$MessageHeaderItem;

    iput-object v0, p0, Lcom/android/mail/browse/MessageHeaderView;->mMessage:Lcom/android/mail/browse/MessageCursor$ConversationMessage;

    iget-boolean v0, p0, Lcom/android/mail/browse/MessageHeaderView;->mObservingContactInfo:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/mail/browse/MessageHeaderView;->mContactInfoSource:Lcom/android/mail/ContactInfoSource;

    iget-object v1, p0, Lcom/android/mail/browse/MessageHeaderView;->mContactInfoObserver:Landroid/database/DataSetObserver;

    invoke-interface {v0, v1}, Lcom/android/mail/ContactInfoSource;->unregisterObserver(Landroid/database/DataSetObserver;)V

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/android/mail/browse/MessageHeaderView;->mObservingContactInfo:Z

    :cond_0
    return-void
.end method
