.class Lcom/android/mail/browse/ConversationItemView$ConversationItemFolderDisplayer;
.super Lcom/android/mail/ui/FolderDisplayer;
.source "ConversationItemView.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/mail/browse/ConversationItemView;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = "ConversationItemFolderDisplayer"
.end annotation


# instance fields
.field private mFoldersCount:I

.field private mHasMoreFolders:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0
    .param p1    # Landroid/content/Context;

    invoke-direct {p0, p1}, Lcom/android/mail/ui/FolderDisplayer;-><init>(Landroid/content/Context;)V

    return-void
.end method

.method static synthetic access$300(Lcom/android/mail/browse/ConversationItemView$ConversationItemFolderDisplayer;I)I
    .locals 1
    .param p0    # Lcom/android/mail/browse/ConversationItemView$ConversationItemFolderDisplayer;
    .param p1    # I

    invoke-direct {p0, p1}, Lcom/android/mail/browse/ConversationItemView$ConversationItemFolderDisplayer;->measureFolders(I)I

    move-result v0

    return v0
.end method

.method private measureFolders(I)I
    .locals 9
    .param p1    # I

    iget-object v7, p0, Lcom/android/mail/browse/ConversationItemView$ConversationItemFolderDisplayer;->mContext:Landroid/content/Context;

    invoke-static {v7, p1}, Lcom/android/mail/browse/ConversationItemViewCoordinates;->getFoldersWidth(Landroid/content/Context;I)I

    move-result v0

    iget-object v7, p0, Lcom/android/mail/browse/ConversationItemView$ConversationItemFolderDisplayer;->mContext:Landroid/content/Context;

    iget v8, p0, Lcom/android/mail/browse/ConversationItemView$ConversationItemFolderDisplayer;->mFoldersCount:I

    invoke-static {v7, p1, v8}, Lcom/android/mail/browse/ConversationItemViewCoordinates;->getFolderCellWidth(Landroid/content/Context;II)I

    move-result v1

    const/4 v5, 0x0

    iget-object v7, p0, Lcom/android/mail/browse/ConversationItemView$ConversationItemFolderDisplayer;->mFoldersSortedSet:Ljava/util/SortedSet;

    invoke-interface {v7}, Ljava/util/SortedSet;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :cond_0
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v7

    if-eqz v7, :cond_2

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/android/mail/providers/Folder;

    iget-object v3, v2, Lcom/android/mail/providers/Folder;->name:Ljava/lang/String;

    # getter for: Lcom/android/mail/browse/ConversationItemView;->sFoldersPaint:Landroid/text/TextPaint;
    invoke-static {}, Lcom/android/mail/browse/ConversationItemView;->access$000()Landroid/text/TextPaint;

    move-result-object v7

    invoke-virtual {v7, v3}, Landroid/text/TextPaint;->measureText(Ljava/lang/String;)F

    move-result v7

    float-to-int v7, v7

    add-int v6, v7, v1

    rem-int v7, v6, v1

    if-eqz v7, :cond_1

    rem-int v7, v6, v1

    sub-int v7, v1, v7

    add-int/2addr v6, v7

    :cond_1
    add-int/2addr v5, v6

    if-le v5, v0, :cond_0

    :cond_2
    return v5
.end method


# virtual methods
.method public drawFolders(Landroid/graphics/Canvas;Lcom/android/mail/browse/ConversationItemViewCoordinates;II)V
    .locals 33
    .param p1    # Landroid/graphics/Canvas;
    .param p2    # Lcom/android/mail/browse/ConversationItemViewCoordinates;
    .param p3    # I
    .param p4    # I

    move-object/from16 v0, p0

    iget v4, v0, Lcom/android/mail/browse/ConversationItemView$ConversationItemFolderDisplayer;->mFoldersCount:I

    if-nez v4, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    move/from16 v30, p3

    move-object/from16 v0, p2

    iget v0, v0, Lcom/android/mail/browse/ConversationItemViewCoordinates;->foldersY:I

    move/from16 v32, v0

    move-object/from16 v0, p2

    iget v0, v0, Lcom/android/mail/browse/ConversationItemViewCoordinates;->foldersHeight:I

    move/from16 v19, v0

    move-object/from16 v0, p2

    iget v0, v0, Lcom/android/mail/browse/ConversationItemViewCoordinates;->foldersTopPadding:I

    move/from16 v27, v0

    move-object/from16 v0, p2

    iget v11, v0, Lcom/android/mail/browse/ConversationItemViewCoordinates;->foldersAscent:I

    move-object/from16 v0, p2

    iget v0, v0, Lcom/android/mail/browse/ConversationItemViewCoordinates;->foldersTextBottomPadding:I

    move/from16 v26, v0

    # getter for: Lcom/android/mail/browse/ConversationItemView;->sFoldersPaint:Landroid/text/TextPaint;
    invoke-static {}, Lcom/android/mail/browse/ConversationItemView;->access$000()Landroid/text/TextPaint;

    move-result-object v4

    move-object/from16 v0, p2

    iget v5, v0, Lcom/android/mail/browse/ConversationItemViewCoordinates;->foldersFontSize:I

    int-to-float v5, v5

    invoke-virtual {v4, v5}, Landroid/text/TextPaint;->setTextSize(F)V

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/mail/browse/ConversationItemView$ConversationItemFolderDisplayer;->mContext:Landroid/content/Context;

    move/from16 v0, p4

    invoke-static {v4, v0}, Lcom/android/mail/browse/ConversationItemViewCoordinates;->getFoldersWidth(Landroid/content/Context;I)I

    move-result v12

    move-object/from16 v0, p0

    iget v4, v0, Lcom/android/mail/browse/ConversationItemView$ConversationItemFolderDisplayer;->mFoldersCount:I

    div-int v13, v12, v4

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/mail/browse/ConversationItemView$ConversationItemFolderDisplayer;->mContext:Landroid/content/Context;

    move-object/from16 v0, p0

    iget v5, v0, Lcom/android/mail/browse/ConversationItemView$ConversationItemFolderDisplayer;->mFoldersCount:I

    move/from16 v0, p4

    invoke-static {v4, v0, v5}, Lcom/android/mail/browse/ConversationItemViewCoordinates;->getFolderCellWidth(Landroid/content/Context;II)I

    move-result v15

    move-object/from16 v0, p0

    move/from16 v1, p4

    invoke-direct {v0, v1}, Lcom/android/mail/browse/ConversationItemView$ConversationItemFolderDisplayer;->measureFolders(I)I

    move-result v28

    move/from16 v0, v28

    invoke-static {v12, v0}, Ljava/lang/Math;->min(II)I

    move-result v4

    sub-int v31, v30, v4

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/mail/browse/ConversationItemView$ConversationItemFolderDisplayer;->mFoldersSortedSet:Ljava/util/SortedSet;

    invoke-interface {v4}, Ljava/util/SortedSet;->iterator()Ljava/util/Iterator;

    move-result-object v20

    :cond_2
    invoke-interface/range {v20 .. v20}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_0

    invoke-interface/range {v20 .. v20}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v16

    check-cast v16, Lcom/android/mail/providers/Folder;

    move-object/from16 v0, v16

    iget-object v0, v0, Lcom/android/mail/providers/Folder;->name:Ljava/lang/String;

    move-object/from16 v18, v0

    move-object/from16 v0, p0

    iget v4, v0, Lcom/android/mail/browse/ConversationItemView$ConversationItemFolderDisplayer;->mDefaultFgColor:I

    move-object/from16 v0, v16

    invoke-virtual {v0, v4}, Lcom/android/mail/providers/Folder;->getForegroundColor(I)I

    move-result v17

    move-object/from16 v0, p0

    iget v4, v0, Lcom/android/mail/browse/ConversationItemView$ConversationItemFolderDisplayer;->mDefaultBgColor:I

    move-object/from16 v0, v16

    invoke-virtual {v0, v4}, Lcom/android/mail/providers/Folder;->getBackgroundColor(I)I

    move-result v14

    move/from16 v29, v15

    const/16 v22, 0x0

    # getter for: Lcom/android/mail/browse/ConversationItemView;->sFoldersPaint:Landroid/text/TextPaint;
    invoke-static {}, Lcom/android/mail/browse/ConversationItemView;->access$000()Landroid/text/TextPaint;

    move-result-object v4

    move-object/from16 v0, v18

    invoke-virtual {v4, v0}, Landroid/text/TextPaint;->measureText(Ljava/lang/String;)F

    move-result v4

    float-to-int v4, v4

    add-int v29, v4, v15

    rem-int v4, v29, v15

    if-eqz v4, :cond_3

    rem-int v4, v29, v15

    sub-int v4, v15, v4

    add-int v29, v29, v4

    :cond_3
    move/from16 v0, v28

    if-le v0, v12, :cond_4

    move/from16 v0, v29

    if-le v0, v13, :cond_4

    move/from16 v29, v13

    const/16 v22, 0x1

    :cond_4
    const/16 v21, 0x0

    # getter for: Lcom/android/mail/browse/ConversationItemView;->sFoldersPaint:Landroid/text/TextPaint;
    invoke-static {}, Lcom/android/mail/browse/ConversationItemView;->access$000()Landroid/text/TextPaint;

    move-result-object v4

    invoke-virtual {v4, v14}, Landroid/text/TextPaint;->setColor(I)V

    # getter for: Lcom/android/mail/browse/ConversationItemView;->sFoldersPaint:Landroid/text/TextPaint;
    invoke-static {}, Lcom/android/mail/browse/ConversationItemView;->access$000()Landroid/text/TextPaint;

    move-result-object v4

    sget-object v5, Landroid/graphics/Paint$Style;->FILL_AND_STROKE:Landroid/graphics/Paint$Style;

    invoke-virtual {v4, v5}, Landroid/text/TextPaint;->setStyle(Landroid/graphics/Paint$Style;)V

    move/from16 v0, v31

    int-to-float v4, v0

    move/from16 v0, v32

    int-to-float v5, v0

    add-int v6, v31, v29

    int-to-float v6, v6

    add-int v7, v32, v19

    sub-int v7, v7, v27

    int-to-float v7, v7

    # getter for: Lcom/android/mail/browse/ConversationItemView;->sFoldersPaint:Landroid/text/TextPaint;
    invoke-static {}, Lcom/android/mail/browse/ConversationItemView;->access$000()Landroid/text/TextPaint;

    move-result-object v8

    move-object/from16 v3, p1

    invoke-virtual/range {v3 .. v8}, Landroid/graphics/Canvas;->drawRect(FFFFLandroid/graphics/Paint;)V

    # getter for: Lcom/android/mail/browse/ConversationItemView;->sFoldersPaint:Landroid/text/TextPaint;
    invoke-static {}, Lcom/android/mail/browse/ConversationItemView;->access$000()Landroid/text/TextPaint;

    move-result-object v4

    move-object/from16 v0, v18

    invoke-virtual {v4, v0}, Landroid/text/TextPaint;->measureText(Ljava/lang/String;)F

    move-result v4

    float-to-int v4, v4

    move/from16 v0, v29

    # invokes: Lcom/android/mail/browse/ConversationItemView;->getPadding(II)I
    invoke-static {v0, v4}, Lcom/android/mail/browse/ConversationItemView;->access$100(II)I

    move-result v23

    if-eqz v22, :cond_5

    new-instance v25, Landroid/text/TextPaint;

    invoke-direct/range {v25 .. v25}, Landroid/text/TextPaint;-><init>()V

    move-object/from16 v0, v25

    move/from16 v1, v17

    invoke-virtual {v0, v1}, Landroid/text/TextPaint;->setColor(I)V

    move-object/from16 v0, p2

    iget v4, v0, Lcom/android/mail/browse/ConversationItemViewCoordinates;->foldersFontSize:I

    int-to-float v4, v4

    move-object/from16 v0, v25

    invoke-virtual {v0, v4}, Landroid/text/TextPaint;->setTextSize(F)V

    div-int/lit8 v23, v15, 0x2

    add-int v4, v31, v29

    sub-int v24, v4, v23

    new-instance v3, Landroid/graphics/LinearGradient;

    sub-int v4, v24, v23

    int-to-float v4, v4

    move/from16 v0, v32

    int-to-float v5, v0

    move/from16 v0, v24

    int-to-float v6, v0

    move/from16 v0, v32

    int-to-float v7, v0

    invoke-static/range {v17 .. v17}, Lcom/android/mail/utils/Utils;->getTransparentColor(I)I

    move-result v9

    sget-object v10, Landroid/graphics/Shader$TileMode;->CLAMP:Landroid/graphics/Shader$TileMode;

    move/from16 v8, v17

    invoke-direct/range {v3 .. v10}, Landroid/graphics/LinearGradient;-><init>(FFFFIILandroid/graphics/Shader$TileMode;)V

    move-object/from16 v0, v25

    invoke-virtual {v0, v3}, Landroid/text/TextPaint;->setShader(Landroid/graphics/Shader;)Landroid/graphics/Shader;

    add-int v4, v31, v23

    int-to-float v4, v4

    add-int v5, v32, v19

    sub-int v5, v5, v26

    int-to-float v5, v5

    move-object/from16 v0, p1

    move-object/from16 v1, v18

    move-object/from16 v2, v25

    invoke-virtual {v0, v1, v4, v5, v2}, Landroid/graphics/Canvas;->drawText(Ljava/lang/String;FFLandroid/graphics/Paint;)V

    :goto_1
    sub-int v12, v12, v29

    add-int v31, v31, v29

    if-gtz v12, :cond_2

    move-object/from16 v0, p0

    iget-boolean v4, v0, Lcom/android/mail/browse/ConversationItemView$ConversationItemFolderDisplayer;->mHasMoreFolders:Z

    if-eqz v4, :cond_2

    # getter for: Lcom/android/mail/browse/ConversationItemView;->MORE_FOLDERS:Landroid/graphics/Bitmap;
    invoke-static {}, Lcom/android/mail/browse/ConversationItemView;->access$200()Landroid/graphics/Bitmap;

    move-result-object v4

    move/from16 v0, v30

    int-to-float v5, v0

    add-int v6, v32, v11

    int-to-float v6, v6

    # getter for: Lcom/android/mail/browse/ConversationItemView;->sFoldersPaint:Landroid/text/TextPaint;
    invoke-static {}, Lcom/android/mail/browse/ConversationItemView;->access$000()Landroid/text/TextPaint;

    move-result-object v7

    move-object/from16 v0, p1

    invoke-virtual {v0, v4, v5, v6, v7}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    goto/16 :goto_0

    :cond_5
    # getter for: Lcom/android/mail/browse/ConversationItemView;->sFoldersPaint:Landroid/text/TextPaint;
    invoke-static {}, Lcom/android/mail/browse/ConversationItemView;->access$000()Landroid/text/TextPaint;

    move-result-object v4

    move/from16 v0, v17

    invoke-virtual {v4, v0}, Landroid/text/TextPaint;->setColor(I)V

    add-int v4, v31, v23

    int-to-float v4, v4

    add-int v5, v32, v19

    sub-int v5, v5, v26

    int-to-float v5, v5

    # getter for: Lcom/android/mail/browse/ConversationItemView;->sFoldersPaint:Landroid/text/TextPaint;
    invoke-static {}, Lcom/android/mail/browse/ConversationItemView;->access$000()Landroid/text/TextPaint;

    move-result-object v6

    move-object/from16 v0, p1

    move-object/from16 v1, v18

    invoke-virtual {v0, v1, v4, v5, v6}, Landroid/graphics/Canvas;->drawText(Ljava/lang/String;FFLandroid/graphics/Paint;)V

    goto :goto_1
.end method

.method public hasVisibleFolders()Z
    .locals 1

    iget v0, p0, Lcom/android/mail/browse/ConversationItemView$ConversationItemFolderDisplayer;->mFoldersCount:I

    if-lez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public loadConversationFolders(Lcom/android/mail/providers/Conversation;Lcom/android/mail/providers/Folder;)V
    .locals 2
    .param p1    # Lcom/android/mail/providers/Conversation;
    .param p2    # Lcom/android/mail/providers/Folder;

    const/4 v1, 0x4

    invoke-super {p0, p1, p2}, Lcom/android/mail/ui/FolderDisplayer;->loadConversationFolders(Lcom/android/mail/providers/Conversation;Lcom/android/mail/providers/Folder;)V

    iget-object v0, p0, Lcom/android/mail/browse/ConversationItemView$ConversationItemFolderDisplayer;->mFoldersSortedSet:Ljava/util/SortedSet;

    invoke-interface {v0}, Ljava/util/SortedSet;->size()I

    move-result v0

    iput v0, p0, Lcom/android/mail/browse/ConversationItemView$ConversationItemFolderDisplayer;->mFoldersCount:I

    iget v0, p0, Lcom/android/mail/browse/ConversationItemView$ConversationItemFolderDisplayer;->mFoldersCount:I

    if-le v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    iput-boolean v0, p0, Lcom/android/mail/browse/ConversationItemView$ConversationItemFolderDisplayer;->mHasMoreFolders:Z

    iget v0, p0, Lcom/android/mail/browse/ConversationItemView$ConversationItemFolderDisplayer;->mFoldersCount:I

    invoke-static {v0, v1}, Ljava/lang/Math;->min(II)I

    move-result v0

    iput v0, p0, Lcom/android/mail/browse/ConversationItemView$ConversationItemFolderDisplayer;->mFoldersCount:I

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public reset()V
    .locals 1

    const/4 v0, 0x0

    invoke-super {p0}, Lcom/android/mail/ui/FolderDisplayer;->reset()V

    iput v0, p0, Lcom/android/mail/browse/ConversationItemView$ConversationItemFolderDisplayer;->mFoldersCount:I

    iput-boolean v0, p0, Lcom/android/mail/browse/ConversationItemView$ConversationItemFolderDisplayer;->mHasMoreFolders:Z

    return-void
.end method
