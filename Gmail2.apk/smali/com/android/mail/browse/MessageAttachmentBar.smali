.class public Lcom/android/mail/browse/MessageAttachmentBar;
.super Landroid/widget/FrameLayout;
.source "MessageAttachmentBar.java"

# interfaces
.implements Landroid/view/View$OnClickListener;
.implements Landroid/widget/PopupMenu$OnMenuItemClickListener;
.implements Lcom/android/mail/browse/AttachmentViewInterface;


# static fields
.field private static final LOG_TAG:Ljava/lang/String;


# instance fields
.field private final mActionHandler:Lcom/android/mail/browse/AttachmentActionHandler;

.field private mAttachment:Lcom/android/mail/providers/Attachment;

.field private mAttachmentSizeText:Ljava/lang/String;

.field private mCancelButton:Landroid/widget/ImageButton;

.field private mDisplayType:Ljava/lang/String;

.field private mOverflowButton:Landroid/widget/ImageView;

.field private mPopup:Landroid/widget/PopupMenu;

.field private mProgress:Landroid/widget/ProgressBar;

.field private mSaveClicked:Z

.field private mSubTitle:Landroid/widget/TextView;

.field private mTitle:Landroid/widget/TextView;

.field private final mUpdateRunnable:Ljava/lang/Runnable;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    invoke-static {}, Lcom/android/mail/utils/LogTag;->getLogTag()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/android/mail/browse/MessageAttachmentBar;->LOG_TAG:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1    # Landroid/content/Context;

    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/android/mail/browse/MessageAttachmentBar;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/util/AttributeSet;

    invoke-direct {p0, p1, p2}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    new-instance v0, Lcom/android/mail/browse/MessageAttachmentBar$1;

    invoke-direct {v0, p0}, Lcom/android/mail/browse/MessageAttachmentBar$1;-><init>(Lcom/android/mail/browse/MessageAttachmentBar;)V

    iput-object v0, p0, Lcom/android/mail/browse/MessageAttachmentBar;->mUpdateRunnable:Ljava/lang/Runnable;

    new-instance v0, Lcom/android/mail/browse/AttachmentActionHandler;

    invoke-direct {v0, p1, p0}, Lcom/android/mail/browse/AttachmentActionHandler;-><init>(Landroid/content/Context;Lcom/android/mail/browse/AttachmentViewInterface;)V

    iput-object v0, p0, Lcom/android/mail/browse/MessageAttachmentBar;->mActionHandler:Lcom/android/mail/browse/AttachmentActionHandler;

    return-void
.end method

.method static synthetic access$000(Lcom/android/mail/browse/MessageAttachmentBar;)V
    .locals 0
    .param p0    # Lcom/android/mail/browse/MessageAttachmentBar;

    invoke-direct {p0}, Lcom/android/mail/browse/MessageAttachmentBar;->updateActionsInternal()V

    return-void
.end method

.method public static inflate(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;)Lcom/android/mail/browse/MessageAttachmentBar;
    .locals 3
    .param p0    # Landroid/view/LayoutInflater;
    .param p1    # Landroid/view/ViewGroup;

    const v1, 0x7f040023

    const/4 v2, 0x0

    invoke-virtual {p0, v1, p1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/android/mail/browse/MessageAttachmentBar;

    return-object v0
.end method

.method private onClick(ILandroid/view/View;)Z
    .locals 8
    .param p1    # I
    .param p2    # Landroid/view/View;

    const/4 v7, 0x0

    const/4 v6, 0x1

    sparse-switch p1, :sswitch_data_0

    iget-object v3, p0, Lcom/android/mail/browse/MessageAttachmentBar;->mAttachment:Lcom/android/mail/providers/Attachment;

    invoke-virtual {v3}, Lcom/android/mail/providers/Attachment;->getContentType()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lcom/android/mail/utils/MimeType;->isInstallable(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_2

    iget-object v3, p0, Lcom/android/mail/browse/MessageAttachmentBar;->mActionHandler:Lcom/android/mail/browse/AttachmentActionHandler;

    invoke-virtual {v3, v6}, Lcom/android/mail/browse/AttachmentActionHandler;->showAttachment(I)V

    :cond_0
    :goto_0
    return v6

    :sswitch_0
    invoke-direct {p0}, Lcom/android/mail/browse/MessageAttachmentBar;->previewAttachment()V

    goto :goto_0

    :sswitch_1
    iget-object v3, p0, Lcom/android/mail/browse/MessageAttachmentBar;->mAttachment:Lcom/android/mail/providers/Attachment;

    invoke-virtual {v3}, Lcom/android/mail/providers/Attachment;->canSave()Z

    move-result v3

    if-eqz v3, :cond_0

    iget-object v3, p0, Lcom/android/mail/browse/MessageAttachmentBar;->mActionHandler:Lcom/android/mail/browse/AttachmentActionHandler;

    invoke-virtual {v3, v6}, Lcom/android/mail/browse/AttachmentActionHandler;->startDownloadingAttachment(I)V

    iput-boolean v6, p0, Lcom/android/mail/browse/MessageAttachmentBar;->mSaveClicked:Z

    goto :goto_0

    :sswitch_2
    iget-object v3, p0, Lcom/android/mail/browse/MessageAttachmentBar;->mAttachment:Lcom/android/mail/providers/Attachment;

    invoke-virtual {v3}, Lcom/android/mail/providers/Attachment;->isPresentLocally()Z

    move-result v3

    if-eqz v3, :cond_0

    iget-object v3, p0, Lcom/android/mail/browse/MessageAttachmentBar;->mActionHandler:Lcom/android/mail/browse/AttachmentActionHandler;

    iget-object v4, p0, Lcom/android/mail/browse/MessageAttachmentBar;->mAttachment:Lcom/android/mail/providers/Attachment;

    invoke-virtual {v3, v4}, Lcom/android/mail/browse/AttachmentActionHandler;->startRedownloadingAttachment(Lcom/android/mail/providers/Attachment;)V

    goto :goto_0

    :sswitch_3
    iget-object v3, p0, Lcom/android/mail/browse/MessageAttachmentBar;->mActionHandler:Lcom/android/mail/browse/AttachmentActionHandler;

    invoke-virtual {v3}, Lcom/android/mail/browse/AttachmentActionHandler;->cancelAttachment()V

    iput-boolean v7, p0, Lcom/android/mail/browse/MessageAttachmentBar;->mSaveClicked:Z

    goto :goto_0

    :sswitch_4
    invoke-direct {p0}, Lcom/android/mail/browse/MessageAttachmentBar;->shouldShowOverflow()Z

    move-result v3

    if-eqz v3, :cond_0

    iget-object v3, p0, Lcom/android/mail/browse/MessageAttachmentBar;->mPopup:Landroid/widget/PopupMenu;

    if-nez v3, :cond_1

    new-instance v3, Landroid/widget/PopupMenu;

    invoke-virtual {p0}, Lcom/android/mail/browse/MessageAttachmentBar;->getContext()Landroid/content/Context;

    move-result-object v4

    invoke-direct {v3, v4, p2}, Landroid/widget/PopupMenu;-><init>(Landroid/content/Context;Landroid/view/View;)V

    iput-object v3, p0, Lcom/android/mail/browse/MessageAttachmentBar;->mPopup:Landroid/widget/PopupMenu;

    iget-object v3, p0, Lcom/android/mail/browse/MessageAttachmentBar;->mPopup:Landroid/widget/PopupMenu;

    invoke-virtual {v3}, Landroid/widget/PopupMenu;->getMenuInflater()Landroid/view/MenuInflater;

    move-result-object v3

    const v4, 0x7f110009

    iget-object v5, p0, Lcom/android/mail/browse/MessageAttachmentBar;->mPopup:Landroid/widget/PopupMenu;

    invoke-virtual {v5}, Landroid/widget/PopupMenu;->getMenu()Landroid/view/Menu;

    move-result-object v5

    invoke-virtual {v3, v4, v5}, Landroid/view/MenuInflater;->inflate(ILandroid/view/Menu;)V

    iget-object v3, p0, Lcom/android/mail/browse/MessageAttachmentBar;->mPopup:Landroid/widget/PopupMenu;

    invoke-virtual {v3, p0}, Landroid/widget/PopupMenu;->setOnMenuItemClickListener(Landroid/widget/PopupMenu$OnMenuItemClickListener;)V

    :cond_1
    iget-object v3, p0, Lcom/android/mail/browse/MessageAttachmentBar;->mPopup:Landroid/widget/PopupMenu;

    invoke-virtual {v3}, Landroid/widget/PopupMenu;->getMenu()Landroid/view/Menu;

    move-result-object v2

    const v3, 0x7f08012e

    invoke-interface {v2, v3}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v3

    invoke-direct {p0}, Lcom/android/mail/browse/MessageAttachmentBar;->shouldShowPreview()Z

    move-result v4

    invoke-interface {v3, v4}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    const v3, 0x7f08012f

    invoke-interface {v2, v3}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v3

    invoke-direct {p0}, Lcom/android/mail/browse/MessageAttachmentBar;->shouldShowSave()Z

    move-result v4

    invoke-interface {v3, v4}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    const v3, 0x7f080130

    invoke-interface {v2, v3}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v3

    invoke-direct {p0}, Lcom/android/mail/browse/MessageAttachmentBar;->shouldShowDownloadAgain()Z

    move-result v4

    invoke-interface {v3, v4}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    iget-object v3, p0, Lcom/android/mail/browse/MessageAttachmentBar;->mPopup:Landroid/widget/PopupMenu;

    invoke-virtual {v3}, Landroid/widget/PopupMenu;->show()V

    goto/16 :goto_0

    :cond_2
    invoke-virtual {p0}, Lcom/android/mail/browse/MessageAttachmentBar;->getContext()Landroid/content/Context;

    move-result-object v3

    iget-object v4, p0, Lcom/android/mail/browse/MessageAttachmentBar;->mAttachment:Lcom/android/mail/providers/Attachment;

    iget-object v4, v4, Lcom/android/mail/providers/Attachment;->contentUri:Landroid/net/Uri;

    iget-object v5, p0, Lcom/android/mail/browse/MessageAttachmentBar;->mAttachment:Lcom/android/mail/providers/Attachment;

    invoke-virtual {v5}, Lcom/android/mail/providers/Attachment;->getContentType()Ljava/lang/String;

    move-result-object v5

    invoke-static {v3, v4, v5}, Lcom/android/mail/utils/MimeType;->isViewable(Landroid/content/Context;Landroid/net/Uri;Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_3

    iget-object v3, p0, Lcom/android/mail/browse/MessageAttachmentBar;->mActionHandler:Lcom/android/mail/browse/AttachmentActionHandler;

    invoke-virtual {v3, v7}, Lcom/android/mail/browse/AttachmentActionHandler;->showAttachment(I)V

    goto/16 :goto_0

    :cond_3
    iget-object v3, p0, Lcom/android/mail/browse/MessageAttachmentBar;->mAttachment:Lcom/android/mail/providers/Attachment;

    invoke-virtual {v3}, Lcom/android/mail/providers/Attachment;->canPreview()Z

    move-result v3

    if-eqz v3, :cond_4

    invoke-direct {p0}, Lcom/android/mail/browse/MessageAttachmentBar;->previewAttachment()V

    goto/16 :goto_0

    :cond_4
    new-instance v0, Landroid/app/AlertDialog$Builder;

    invoke-virtual {p0}, Lcom/android/mail/browse/MessageAttachmentBar;->getContext()Landroid/content/Context;

    move-result-object v3

    invoke-direct {v0, v3}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    iget-object v3, p0, Lcom/android/mail/browse/MessageAttachmentBar;->mAttachment:Lcom/android/mail/providers/Attachment;

    invoke-virtual {v3}, Lcom/android/mail/providers/Attachment;->getContentType()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lcom/android/mail/utils/MimeType;->isBlocked(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_5

    const v1, 0x7f090087

    :goto_1
    const v3, 0x7f090086

    invoke-virtual {v0, v3}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    move-result-object v3

    invoke-virtual {v3, v1}, Landroid/app/AlertDialog$Builder;->setMessage(I)Landroid/app/AlertDialog$Builder;

    move-result-object v3

    invoke-virtual {v3}, Landroid/app/AlertDialog$Builder;->show()Landroid/app/AlertDialog;

    goto/16 :goto_0

    :cond_5
    const v1, 0x7f090088

    goto :goto_1

    :sswitch_data_0
    .sparse-switch
        0x7f080066 -> :sswitch_4
        0x7f080067 -> :sswitch_3
        0x7f08012e -> :sswitch_0
        0x7f08012f -> :sswitch_1
        0x7f080130 -> :sswitch_2
    .end sparse-switch
.end method

.method private previewAttachment()V
    .locals 3

    iget-object v1, p0, Lcom/android/mail/browse/MessageAttachmentBar;->mAttachment:Lcom/android/mail/providers/Attachment;

    invoke-virtual {v1}, Lcom/android/mail/providers/Attachment;->canPreview()Z

    move-result v1

    if-eqz v1, :cond_0

    new-instance v0, Landroid/content/Intent;

    const-string v1, "android.intent.action.VIEW"

    iget-object v2, p0, Lcom/android/mail/browse/MessageAttachmentBar;->mAttachment:Lcom/android/mail/providers/Attachment;

    iget-object v2, v2, Lcom/android/mail/providers/Attachment;->previewIntentUri:Landroid/net/Uri;

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;Landroid/net/Uri;)V

    invoke-virtual {p0}, Lcom/android/mail/browse/MessageAttachmentBar;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    :cond_0
    return-void
.end method

.method private setButtonVisible(Landroid/view/View;Z)V
    .locals 1
    .param p1    # Landroid/view/View;
    .param p2    # Z

    if-eqz p2, :cond_0

    const/4 v0, 0x0

    :goto_0
    invoke-virtual {p1, v0}, Landroid/view/View;->setVisibility(I)V

    return-void

    :cond_0
    const/16 v0, 0x8

    goto :goto_0
.end method

.method private shouldShowCancel()Z
    .locals 1

    iget-object v0, p0, Lcom/android/mail/browse/MessageAttachmentBar;->mAttachment:Lcom/android/mail/providers/Attachment;

    invoke-virtual {v0}, Lcom/android/mail/providers/Attachment;->isDownloading()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/android/mail/browse/MessageAttachmentBar;->mSaveClicked:Z

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private shouldShowDownloadAgain()Z
    .locals 1

    iget-object v0, p0, Lcom/android/mail/browse/MessageAttachmentBar;->mAttachment:Lcom/android/mail/providers/Attachment;

    invoke-virtual {v0}, Lcom/android/mail/providers/Attachment;->isDownloadFinishedOrFailed()Z

    move-result v0

    return v0
.end method

.method private shouldShowOverflow()Z
    .locals 1

    invoke-direct {p0}, Lcom/android/mail/browse/MessageAttachmentBar;->shouldShowPreview()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-direct {p0}, Lcom/android/mail/browse/MessageAttachmentBar;->shouldShowSave()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-direct {p0}, Lcom/android/mail/browse/MessageAttachmentBar;->shouldShowDownloadAgain()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    invoke-direct {p0}, Lcom/android/mail/browse/MessageAttachmentBar;->shouldShowCancel()Z

    move-result v0

    if-nez v0, :cond_1

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private shouldShowPreview()Z
    .locals 1

    iget-object v0, p0, Lcom/android/mail/browse/MessageAttachmentBar;->mAttachment:Lcom/android/mail/providers/Attachment;

    invoke-virtual {v0}, Lcom/android/mail/providers/Attachment;->canPreview()Z

    move-result v0

    return v0
.end method

.method private shouldShowSave()Z
    .locals 1

    iget-object v0, p0, Lcom/android/mail/browse/MessageAttachmentBar;->mAttachment:Lcom/android/mail/providers/Attachment;

    invoke-virtual {v0}, Lcom/android/mail/providers/Attachment;->canSave()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/android/mail/browse/MessageAttachmentBar;->mSaveClicked:Z

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private updateActions()V
    .locals 1

    iget-object v0, p0, Lcom/android/mail/browse/MessageAttachmentBar;->mUpdateRunnable:Ljava/lang/Runnable;

    invoke-virtual {p0, v0}, Lcom/android/mail/browse/MessageAttachmentBar;->removeCallbacks(Ljava/lang/Runnable;)Z

    iget-object v0, p0, Lcom/android/mail/browse/MessageAttachmentBar;->mUpdateRunnable:Ljava/lang/Runnable;

    invoke-virtual {p0, v0}, Lcom/android/mail/browse/MessageAttachmentBar;->post(Ljava/lang/Runnable;)Z

    return-void
.end method

.method private updateActionsInternal()V
    .locals 2

    iget-object v0, p0, Lcom/android/mail/browse/MessageAttachmentBar;->mActionHandler:Lcom/android/mail/browse/AttachmentActionHandler;

    invoke-virtual {v0}, Lcom/android/mail/browse/AttachmentActionHandler;->isProgressDialogVisible()Z

    move-result v0

    if-eqz v0, :cond_0

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/android/mail/browse/MessageAttachmentBar;->mCancelButton:Landroid/widget/ImageButton;

    invoke-direct {p0}, Lcom/android/mail/browse/MessageAttachmentBar;->shouldShowCancel()Z

    move-result v1

    invoke-direct {p0, v0, v1}, Lcom/android/mail/browse/MessageAttachmentBar;->setButtonVisible(Landroid/view/View;Z)V

    iget-object v0, p0, Lcom/android/mail/browse/MessageAttachmentBar;->mOverflowButton:Landroid/widget/ImageView;

    invoke-direct {p0}, Lcom/android/mail/browse/MessageAttachmentBar;->shouldShowOverflow()Z

    move-result v1

    invoke-direct {p0, v0, v1}, Lcom/android/mail/browse/MessageAttachmentBar;->setButtonVisible(Landroid/view/View;Z)V

    goto :goto_0
.end method

.method private updateSubtitleText()V
    .locals 6

    const/4 v3, 0x1

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v1, p0, Lcom/android/mail/browse/MessageAttachmentBar;->mAttachment:Lcom/android/mail/providers/Attachment;

    iget v1, v1, Lcom/android/mail/providers/Attachment;->state:I

    if-ne v1, v3, :cond_0

    invoke-virtual {p0}, Lcom/android/mail/browse/MessageAttachmentBar;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f09008c

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :goto_0
    iget-object v1, p0, Lcom/android/mail/browse/MessageAttachmentBar;->mSubTitle:Landroid/widget/TextView;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    return-void

    :cond_0
    iget-object v1, p0, Lcom/android/mail/browse/MessageAttachmentBar;->mAttachment:Lcom/android/mail/providers/Attachment;

    invoke-virtual {v1}, Lcom/android/mail/providers/Attachment;->isSavedToExternal()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-virtual {p0}, Lcom/android/mail/browse/MessageAttachmentBar;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f09008b

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    iget-object v5, p0, Lcom/android/mail/browse/MessageAttachmentBar;->mAttachmentSizeText:Ljava/lang/String;

    aput-object v5, v3, v4

    invoke-virtual {v1, v2, v3}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :goto_1
    const/16 v1, 0x20

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/android/mail/browse/MessageAttachmentBar;->mDisplayType:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_0

    :cond_1
    iget-object v1, p0, Lcom/android/mail/browse/MessageAttachmentBar;->mAttachmentSizeText:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_1
.end method


# virtual methods
.method public initialize(Landroid/app/FragmentManager;)V
    .locals 1
    .param p1    # Landroid/app/FragmentManager;

    iget-object v0, p0, Lcom/android/mail/browse/MessageAttachmentBar;->mActionHandler:Lcom/android/mail/browse/AttachmentActionHandler;

    invoke-virtual {v0, p1}, Lcom/android/mail/browse/AttachmentActionHandler;->initialize(Landroid/app/FragmentManager;)V

    return-void
.end method

.method public onClick(Landroid/view/View;)V
    .locals 1
    .param p1    # Landroid/view/View;

    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v0

    invoke-direct {p0, v0, p1}, Lcom/android/mail/browse/MessageAttachmentBar;->onClick(ILandroid/view/View;)Z

    return-void
.end method

.method protected onFinishInflate()V
    .locals 1

    invoke-super {p0}, Landroid/widget/FrameLayout;->onFinishInflate()V

    const v0, 0x7f080063

    invoke-virtual {p0, v0}, Lcom/android/mail/browse/MessageAttachmentBar;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/android/mail/browse/MessageAttachmentBar;->mTitle:Landroid/widget/TextView;

    const v0, 0x7f080064

    invoke-virtual {p0, v0}, Lcom/android/mail/browse/MessageAttachmentBar;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/android/mail/browse/MessageAttachmentBar;->mSubTitle:Landroid/widget/TextView;

    const v0, 0x7f080065

    invoke-virtual {p0, v0}, Lcom/android/mail/browse/MessageAttachmentBar;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ProgressBar;

    iput-object v0, p0, Lcom/android/mail/browse/MessageAttachmentBar;->mProgress:Landroid/widget/ProgressBar;

    const v0, 0x7f080066

    invoke-virtual {p0, v0}, Lcom/android/mail/browse/MessageAttachmentBar;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/android/mail/browse/MessageAttachmentBar;->mOverflowButton:Landroid/widget/ImageView;

    const v0, 0x7f080067

    invoke-virtual {p0, v0}, Lcom/android/mail/browse/MessageAttachmentBar;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageButton;

    iput-object v0, p0, Lcom/android/mail/browse/MessageAttachmentBar;->mCancelButton:Landroid/widget/ImageButton;

    invoke-virtual {p0, p0}, Lcom/android/mail/browse/MessageAttachmentBar;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v0, p0, Lcom/android/mail/browse/MessageAttachmentBar;->mOverflowButton:Landroid/widget/ImageView;

    invoke-virtual {v0, p0}, Landroid/widget/ImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v0, p0, Lcom/android/mail/browse/MessageAttachmentBar;->mCancelButton:Landroid/widget/ImageButton;

    invoke-virtual {v0, p0}, Landroid/widget/ImageButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    return-void
.end method

.method public onMenuItemClick(Landroid/view/MenuItem;)Z
    .locals 2
    .param p1    # Landroid/view/MenuItem;

    iget-object v0, p0, Lcom/android/mail/browse/MessageAttachmentBar;->mPopup:Landroid/widget/PopupMenu;

    invoke-virtual {v0}, Landroid/widget/PopupMenu;->dismiss()V

    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v0

    const/4 v1, 0x0

    invoke-direct {p0, v0, v1}, Lcom/android/mail/browse/MessageAttachmentBar;->onClick(ILandroid/view/View;)Z

    move-result v0

    return v0
.end method

.method public onUpdateStatus()V
    .locals 0

    invoke-direct {p0}, Lcom/android/mail/browse/MessageAttachmentBar;->updateSubtitleText()V

    return-void
.end method

.method public render(Lcom/android/mail/providers/Attachment;Z)V
    .locals 6
    .param p1    # Lcom/android/mail/providers/Attachment;
    .param p2    # Z

    const/4 v2, 0x0

    iget-object v0, p0, Lcom/android/mail/browse/MessageAttachmentBar;->mAttachment:Lcom/android/mail/providers/Attachment;

    iput-object p1, p0, Lcom/android/mail/browse/MessageAttachmentBar;->mAttachment:Lcom/android/mail/providers/Attachment;

    iget-object v1, p0, Lcom/android/mail/browse/MessageAttachmentBar;->mActionHandler:Lcom/android/mail/browse/AttachmentActionHandler;

    iget-object v3, p0, Lcom/android/mail/browse/MessageAttachmentBar;->mAttachment:Lcom/android/mail/providers/Attachment;

    invoke-virtual {v1, v3}, Lcom/android/mail/browse/AttachmentActionHandler;->setAttachment(Lcom/android/mail/providers/Attachment;)V

    invoke-virtual {p1}, Lcom/android/mail/providers/Attachment;->isDownloading()Z

    move-result v1

    if-nez v1, :cond_4

    move v1, v2

    :goto_0
    iput-boolean v1, p0, Lcom/android/mail/browse/MessageAttachmentBar;->mSaveClicked:Z

    sget-object v1, Lcom/android/mail/browse/MessageAttachmentBar;->LOG_TAG:Ljava/lang/String;

    const-string v3, "got attachment list row: name=%s state/dest=%d/%d dled=%d contentUri=%s MIME=%s"

    const/4 v4, 0x6

    new-array v4, v4, [Ljava/lang/Object;

    invoke-virtual {p1}, Lcom/android/mail/providers/Attachment;->getName()Ljava/lang/String;

    move-result-object v5

    aput-object v5, v4, v2

    const/4 v2, 0x1

    iget v5, p1, Lcom/android/mail/providers/Attachment;->state:I

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v4, v2

    const/4 v2, 0x2

    iget v5, p1, Lcom/android/mail/providers/Attachment;->destination:I

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v4, v2

    const/4 v2, 0x3

    iget v5, p1, Lcom/android/mail/providers/Attachment;->downloadedSize:I

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v4, v2

    const/4 v2, 0x4

    iget-object v5, p1, Lcom/android/mail/providers/Attachment;->contentUri:Landroid/net/Uri;

    aput-object v5, v4, v2

    const/4 v2, 0x5

    invoke-virtual {p1}, Lcom/android/mail/providers/Attachment;->getContentType()Ljava/lang/String;

    move-result-object v5

    aput-object v5, v4, v2

    invoke-static {v1, v3, v4}, Lcom/android/mail/utils/LogUtils;->d(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)I

    if-eqz v0, :cond_0

    invoke-virtual {p1}, Lcom/android/mail/providers/Attachment;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0}, Lcom/android/mail/providers/Attachment;->getName()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_1

    :cond_0
    iget-object v1, p0, Lcom/android/mail/browse/MessageAttachmentBar;->mTitle:Landroid/widget/TextView;

    invoke-virtual {p1}, Lcom/android/mail/providers/Attachment;->getName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    :cond_1
    if-eqz v0, :cond_2

    iget v1, p1, Lcom/android/mail/providers/Attachment;->size:I

    iget v2, v0, Lcom/android/mail/providers/Attachment;->size:I

    if-eq v1, v2, :cond_3

    :cond_2
    invoke-virtual {p0}, Lcom/android/mail/browse/MessageAttachmentBar;->getContext()Landroid/content/Context;

    move-result-object v1

    iget v2, p1, Lcom/android/mail/providers/Attachment;->size:I

    int-to-long v2, v2

    invoke-static {v1, v2, v3}, Lcom/android/mail/utils/AttachmentUtils;->convertToHumanReadableSize(Landroid/content/Context;J)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/android/mail/browse/MessageAttachmentBar;->mAttachmentSizeText:Ljava/lang/String;

    invoke-virtual {p0}, Lcom/android/mail/browse/MessageAttachmentBar;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1, p1}, Lcom/android/mail/utils/AttachmentUtils;->getDisplayType(Landroid/content/Context;Lcom/android/mail/providers/Attachment;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/android/mail/browse/MessageAttachmentBar;->mDisplayType:Ljava/lang/String;

    invoke-direct {p0}, Lcom/android/mail/browse/MessageAttachmentBar;->updateSubtitleText()V

    :cond_3
    invoke-direct {p0}, Lcom/android/mail/browse/MessageAttachmentBar;->updateActions()V

    iget-object v1, p0, Lcom/android/mail/browse/MessageAttachmentBar;->mActionHandler:Lcom/android/mail/browse/AttachmentActionHandler;

    invoke-virtual {v1, p2}, Lcom/android/mail/browse/AttachmentActionHandler;->updateStatus(Z)V

    return-void

    :cond_4
    iget-boolean v1, p0, Lcom/android/mail/browse/MessageAttachmentBar;->mSaveClicked:Z

    goto :goto_0
.end method

.method public updateProgress(Z)V
    .locals 4
    .param p1    # Z

    const/4 v3, 0x4

    const/4 v1, 0x0

    iget-object v0, p0, Lcom/android/mail/browse/MessageAttachmentBar;->mAttachment:Lcom/android/mail/providers/Attachment;

    invoke-virtual {v0}, Lcom/android/mail/providers/Attachment;->isDownloading()Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/android/mail/browse/MessageAttachmentBar;->mProgress:Landroid/widget/ProgressBar;

    iget-object v2, p0, Lcom/android/mail/browse/MessageAttachmentBar;->mAttachment:Lcom/android/mail/providers/Attachment;

    iget v2, v2, Lcom/android/mail/providers/Attachment;->size:I

    invoke-virtual {v0, v2}, Landroid/widget/ProgressBar;->setMax(I)V

    iget-object v0, p0, Lcom/android/mail/browse/MessageAttachmentBar;->mProgress:Landroid/widget/ProgressBar;

    iget-object v2, p0, Lcom/android/mail/browse/MessageAttachmentBar;->mAttachment:Lcom/android/mail/providers/Attachment;

    iget v2, v2, Lcom/android/mail/providers/Attachment;->downloadedSize:I

    invoke-virtual {v0, v2}, Landroid/widget/ProgressBar;->setProgress(I)V

    iget-object v2, p0, Lcom/android/mail/browse/MessageAttachmentBar;->mProgress:Landroid/widget/ProgressBar;

    if-nez p1, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-virtual {v2, v0}, Landroid/widget/ProgressBar;->setIndeterminate(Z)V

    iget-object v0, p0, Lcom/android/mail/browse/MessageAttachmentBar;->mProgress:Landroid/widget/ProgressBar;

    invoke-virtual {v0, v1}, Landroid/widget/ProgressBar;->setVisibility(I)V

    iget-object v0, p0, Lcom/android/mail/browse/MessageAttachmentBar;->mSubTitle:Landroid/widget/TextView;

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setVisibility(I)V

    :goto_1
    return-void

    :cond_0
    move v0, v1

    goto :goto_0

    :cond_1
    iget-object v0, p0, Lcom/android/mail/browse/MessageAttachmentBar;->mProgress:Landroid/widget/ProgressBar;

    invoke-virtual {v0, v3}, Landroid/widget/ProgressBar;->setVisibility(I)V

    iget-object v0, p0, Lcom/android/mail/browse/MessageAttachmentBar;->mSubTitle:Landroid/widget/TextView;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    goto :goto_1
.end method

.method public viewAttachment()V
    .locals 5

    const/4 v4, 0x0

    iget-object v2, p0, Lcom/android/mail/browse/MessageAttachmentBar;->mAttachment:Lcom/android/mail/providers/Attachment;

    iget-object v2, v2, Lcom/android/mail/providers/Attachment;->contentUri:Landroid/net/Uri;

    if-nez v2, :cond_0

    sget-object v2, Lcom/android/mail/browse/MessageAttachmentBar;->LOG_TAG:Ljava/lang/String;

    const-string v3, "viewAttachment with null content uri"

    new-array v4, v4, [Ljava/lang/Object;

    invoke-static {v2, v3, v4}, Lcom/android/mail/utils/LogUtils;->e(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)I

    :goto_0
    return-void

    :cond_0
    new-instance v1, Landroid/content/Intent;

    const-string v2, "android.intent.action.VIEW"

    invoke-direct {v1, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    const v2, 0x80001

    invoke-virtual {v1, v2}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    iget-object v2, p0, Lcom/android/mail/browse/MessageAttachmentBar;->mAttachment:Lcom/android/mail/providers/Attachment;

    iget-object v2, v2, Lcom/android/mail/providers/Attachment;->contentUri:Landroid/net/Uri;

    iget-object v3, p0, Lcom/android/mail/browse/MessageAttachmentBar;->mAttachment:Lcom/android/mail/providers/Attachment;

    invoke-virtual {v3}, Lcom/android/mail/providers/Attachment;->getContentType()Ljava/lang/String;

    move-result-object v3

    invoke-static {v1, v2, v3}, Lcom/android/mail/utils/Utils;->setIntentDataAndTypeAndNormalize(Landroid/content/Intent;Landroid/net/Uri;Ljava/lang/String;)Landroid/content/Intent;

    :try_start_0
    invoke-virtual {p0}, Lcom/android/mail/browse/MessageAttachmentBar;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-virtual {v2, v1}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V
    :try_end_0
    .catch Landroid/content/ActivityNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v0

    sget-object v2, Lcom/android/mail/browse/MessageAttachmentBar;->LOG_TAG:Ljava/lang/String;

    const-string v3, "Couldn\'t find Activity for intent"

    new-array v4, v4, [Ljava/lang/Object;

    invoke-static {v2, v0, v3, v4}, Lcom/android/mail/utils/LogUtils;->e(Ljava/lang/String;Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)I

    goto :goto_0
.end method
