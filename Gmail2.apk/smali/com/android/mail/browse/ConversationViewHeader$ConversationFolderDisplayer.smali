.class Lcom/android/mail/browse/ConversationViewHeader$ConversationFolderDisplayer;
.super Lcom/android/mail/ui/FolderDisplayer;
.source "ConversationViewHeader.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/mail/browse/ConversationViewHeader;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "ConversationFolderDisplayer"
.end annotation


# instance fields
.field private mDims:Lcom/android/mail/browse/FolderSpan$FolderSpanDimensions;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/android/mail/browse/FolderSpan$FolderSpanDimensions;)V
    .locals 0
    .param p1    # Landroid/content/Context;
    .param p2    # Lcom/android/mail/browse/FolderSpan$FolderSpanDimensions;

    invoke-direct {p0, p1}, Lcom/android/mail/ui/FolderDisplayer;-><init>(Landroid/content/Context;)V

    iput-object p2, p0, Lcom/android/mail/browse/ConversationViewHeader$ConversationFolderDisplayer;->mDims:Lcom/android/mail/browse/FolderSpan$FolderSpanDimensions;

    return-void
.end method

.method private addSpan(Landroid/text/SpannableStringBuilder;Lcom/android/mail/providers/Folder;)V
    .locals 7
    .param p1    # Landroid/text/SpannableStringBuilder;
    .param p2    # Lcom/android/mail/providers/Folder;

    const/16 v6, 0x21

    invoke-virtual {p1}, Landroid/text/SpannableStringBuilder;->length()I

    move-result v3

    iget-object v4, p2, Lcom/android/mail/providers/Folder;->name:Ljava/lang/String;

    invoke-virtual {p1, v4}, Landroid/text/SpannableStringBuilder;->append(Ljava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    invoke-virtual {p1}, Landroid/text/SpannableStringBuilder;->length()I

    move-result v1

    iget v4, p0, Lcom/android/mail/browse/ConversationViewHeader$ConversationFolderDisplayer;->mDefaultFgColor:I

    invoke-virtual {p2, v4}, Lcom/android/mail/providers/Folder;->getForegroundColor(I)I

    move-result v2

    iget v4, p0, Lcom/android/mail/browse/ConversationViewHeader$ConversationFolderDisplayer;->mDefaultBgColor:I

    invoke-virtual {p2, v4}, Lcom/android/mail/providers/Folder;->getBackgroundColor(I)I

    move-result v0

    new-instance v4, Landroid/text/style/BackgroundColorSpan;

    invoke-direct {v4, v0}, Landroid/text/style/BackgroundColorSpan;-><init>(I)V

    invoke-virtual {p1, v4, v3, v1, v6}, Landroid/text/SpannableStringBuilder;->setSpan(Ljava/lang/Object;III)V

    new-instance v4, Landroid/text/style/ForegroundColorSpan;

    invoke-direct {v4, v2}, Landroid/text/style/ForegroundColorSpan;-><init>(I)V

    invoke-virtual {p1, v4, v3, v1, v6}, Landroid/text/SpannableStringBuilder;->setSpan(Ljava/lang/Object;III)V

    new-instance v4, Lcom/android/mail/browse/FolderSpan;

    iget-object v5, p0, Lcom/android/mail/browse/ConversationViewHeader$ConversationFolderDisplayer;->mDims:Lcom/android/mail/browse/FolderSpan$FolderSpanDimensions;

    invoke-direct {v4, p1, v5}, Lcom/android/mail/browse/FolderSpan;-><init>(Landroid/text/Spanned;Lcom/android/mail/browse/FolderSpan$FolderSpanDimensions;)V

    invoke-virtual {p1, v4, v3, v1, v6}, Landroid/text/SpannableStringBuilder;->setSpan(Ljava/lang/Object;III)V

    return-void
.end method


# virtual methods
.method public appendFolderSpans(Landroid/text/SpannableStringBuilder;)V
    .locals 6
    .param p1    # Landroid/text/SpannableStringBuilder;

    iget-object v4, p0, Lcom/android/mail/browse/ConversationViewHeader$ConversationFolderDisplayer;->mFoldersSortedSet:Ljava/util/SortedSet;

    invoke-interface {v4}, Ljava/util/SortedSet;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/android/mail/providers/Folder;

    invoke-direct {p0, p1, v1}, Lcom/android/mail/browse/ConversationViewHeader$ConversationFolderDisplayer;->addSpan(Landroid/text/SpannableStringBuilder;Lcom/android/mail/providers/Folder;)V

    goto :goto_0

    :cond_0
    iget-object v4, p0, Lcom/android/mail/browse/ConversationViewHeader$ConversationFolderDisplayer;->mFoldersSortedSet:Ljava/util/SortedSet;

    invoke-interface {v4}, Ljava/util/SortedSet;->isEmpty()Z

    move-result v4

    if-eqz v4, :cond_1

    invoke-static {}, Lcom/android/mail/providers/Folder;->newUnsafeInstance()Lcom/android/mail/providers/Folder;

    move-result-object v0

    iget-object v4, p0, Lcom/android/mail/browse/ConversationViewHeader$ConversationFolderDisplayer;->mContext:Landroid/content/Context;

    invoke-virtual {v4}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f0900bd

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v4

    iput-object v4, v0, Lcom/android/mail/providers/Folder;->name:Ljava/lang/String;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, ""

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const v5, 0x7f0a0020

    invoke-virtual {v3, v5}, Landroid/content/res/Resources;->getColor(I)I

    move-result v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    iput-object v4, v0, Lcom/android/mail/providers/Folder;->bgColor:Ljava/lang/String;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, ""

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const v5, 0x7f0a001f

    invoke-virtual {v3, v5}, Landroid/content/res/Resources;->getColor(I)I

    move-result v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    iput-object v4, v0, Lcom/android/mail/providers/Folder;->fgColor:Ljava/lang/String;

    invoke-direct {p0, p1, v0}, Lcom/android/mail/browse/ConversationViewHeader$ConversationFolderDisplayer;->addSpan(Landroid/text/SpannableStringBuilder;Lcom/android/mail/providers/Folder;)V

    :cond_1
    return-void
.end method
