.class public final Lcom/android/mail/browse/ConversationCursor;
.super Ljava/lang/Object;
.source "ConversationCursor.java"

# interfaces
.implements Landroid/database/Cursor;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/mail/browse/ConversationCursor$ConversationListener;,
        Lcom/android/mail/browse/ConversationCursor$ConversationOperation;,
        Lcom/android/mail/browse/ConversationCursor$ConversationProvider;,
        Lcom/android/mail/browse/ConversationCursor$CursorObserver;,
        Lcom/android/mail/browse/ConversationCursor$RefreshTask;,
        Lcom/android/mail/browse/ConversationCursor$UnderlyingCursorWrapper;
    }
.end annotation


# static fields
.field private static final LOG_TAG:Ljava/lang/String;

.field static sProvider:Lcom/android/mail/browse/ConversationCursor$ConversationProvider;

.field private static sSequence:I


# instance fields
.field private final mCacheMap:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Landroid/content/ContentValues;",
            ">;"
        }
    .end annotation
.end field

.field private final mCacheMapLock:Ljava/lang/Object;

.field private mColumnNames:[Ljava/lang/String;

.field private final mCursorObserver:Lcom/android/mail/browse/ConversationCursor$CursorObserver;

.field private mCursorObserverRegistered:Z

.field private mDeferSync:Z

.field private mDeletedCount:I

.field private mInitialConversationLimit:Z

.field private final mListeners:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/android/mail/browse/ConversationCursor$ConversationListener;",
            ">;"
        }
    .end annotation
.end field

.field private final mMainThreadHandler:Landroid/os/Handler;

.field private final mMostlyDead:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/android/mail/providers/Conversation;",
            ">;"
        }
    .end annotation
.end field

.field private final mName:Ljava/lang/String;

.field private final mNotificationTempDeleted:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Lcom/android/mail/providers/Conversation;",
            ">;"
        }
    .end annotation
.end field

.field private mPaused:Z

.field private mPosition:I

.field private mRefreshReady:Z

.field private mRefreshRequired:Z

.field private mRefreshTask:Lcom/android/mail/browse/ConversationCursor$RefreshTask;

.field private volatile mRequeryCursor:Lcom/android/mail/browse/ConversationCursor$UnderlyingCursorWrapper;

.field private final mResolver:Landroid/content/ContentResolver;

.field mUnderlyingCursor:Lcom/android/mail/browse/ConversationCursor$UnderlyingCursorWrapper;

.field private qProjection:[Ljava/lang/String;

.field private qUri:Landroid/net/Uri;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    invoke-static {}, Lcom/android/mail/utils/LogTag;->getLogTag()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/android/mail/browse/ConversationCursor;->LOG_TAG:Ljava/lang/String;

    const/4 v0, 0x0

    sput v0, Lcom/android/mail/browse/ConversationCursor;->sSequence:I

    return-void
.end method

.method public constructor <init>(Landroid/app/Activity;Landroid/net/Uri;ZLjava/lang/String;)V
    .locals 3
    .param p1    # Landroid/app/Activity;
    .param p2    # Landroid/net/Uri;
    .param p3    # Z
    .param p4    # Ljava/lang/String;

    const/4 v1, 0x0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/android/mail/browse/ConversationCursor;->mCacheMap:Ljava/util/HashMap;

    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lcom/android/mail/browse/ConversationCursor;->mCacheMapLock:Ljava/lang/Object;

    invoke-static {}, Lcom/google/common/collect/Lists;->newArrayList()Ljava/util/ArrayList;

    move-result-object v0

    iput-object v0, p0, Lcom/android/mail/browse/ConversationCursor;->mListeners:Ljava/util/List;

    iput-boolean v1, p0, Lcom/android/mail/browse/ConversationCursor;->mRefreshReady:Z

    iput-boolean v1, p0, Lcom/android/mail/browse/ConversationCursor;->mRefreshRequired:Z

    iput-boolean v1, p0, Lcom/android/mail/browse/ConversationCursor;->mInitialConversationLimit:Z

    invoke-static {}, Lcom/google/common/collect/Lists;->newArrayList()Ljava/util/ArrayList;

    move-result-object v0

    iput-object v0, p0, Lcom/android/mail/browse/ConversationCursor;->mMostlyDead:Ljava/util/List;

    invoke-static {}, Lcom/google/common/collect/Sets;->newHashSet()Ljava/util/HashSet;

    move-result-object v0

    iput-object v0, p0, Lcom/android/mail/browse/ConversationCursor;->mNotificationTempDeleted:Ljava/util/Set;

    iput-boolean v1, p0, Lcom/android/mail/browse/ConversationCursor;->mCursorObserverRegistered:Z

    iput-boolean v1, p0, Lcom/android/mail/browse/ConversationCursor;->mPaused:Z

    iput-boolean v1, p0, Lcom/android/mail/browse/ConversationCursor;->mDeferSync:Z

    const/4 v0, -0x1

    iput v0, p0, Lcom/android/mail/browse/ConversationCursor;->mPosition:I

    iput v1, p0, Lcom/android/mail/browse/ConversationCursor;->mDeletedCount:I

    new-instance v0, Landroid/os/Handler;

    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    iput-object v0, p0, Lcom/android/mail/browse/ConversationCursor;->mMainThreadHandler:Landroid/os/Handler;

    iput-boolean p3, p0, Lcom/android/mail/browse/ConversationCursor;->mInitialConversationLimit:Z

    invoke-virtual {p1}, Landroid/app/Activity;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    iput-object v0, p0, Lcom/android/mail/browse/ConversationCursor;->mResolver:Landroid/content/ContentResolver;

    iput-object p2, p0, Lcom/android/mail/browse/ConversationCursor;->qUri:Landroid/net/Uri;

    iput-object p4, p0, Lcom/android/mail/browse/ConversationCursor;->mName:Ljava/lang/String;

    sget-object v0, Lcom/android/mail/providers/UIProvider;->CONVERSATION_PROJECTION:[Ljava/lang/String;

    iput-object v0, p0, Lcom/android/mail/browse/ConversationCursor;->qProjection:[Ljava/lang/String;

    new-instance v0, Lcom/android/mail/browse/ConversationCursor$CursorObserver;

    new-instance v1, Landroid/os/Handler;

    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v2

    invoke-direct {v1, v2}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    invoke-direct {v0, p0, v1}, Lcom/android/mail/browse/ConversationCursor$CursorObserver;-><init>(Lcom/android/mail/browse/ConversationCursor;Landroid/os/Handler;)V

    iput-object v0, p0, Lcom/android/mail/browse/ConversationCursor;->mCursorObserver:Lcom/android/mail/browse/ConversationCursor$CursorObserver;

    return-void
.end method

.method static synthetic access$000(Lcom/android/mail/browse/ConversationCursor;Z)Lcom/android/mail/browse/ConversationCursor$UnderlyingCursorWrapper;
    .locals 1
    .param p0    # Lcom/android/mail/browse/ConversationCursor;
    .param p1    # Z

    invoke-direct {p0, p1}, Lcom/android/mail/browse/ConversationCursor;->doQuery(Z)Lcom/android/mail/browse/ConversationCursor$UnderlyingCursorWrapper;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$100(Lcom/android/mail/browse/ConversationCursor;)Ljava/lang/Object;
    .locals 1
    .param p0    # Lcom/android/mail/browse/ConversationCursor;

    iget-object v0, p0, Lcom/android/mail/browse/ConversationCursor;->mCacheMapLock:Ljava/lang/Object;

    return-object v0
.end method

.method static synthetic access$1000(Landroid/net/Uri;)Landroid/net/Uri;
    .locals 1
    .param p0    # Landroid/net/Uri;

    invoke-static {p0}, Lcom/android/mail/browse/ConversationCursor;->uriFromCachingUri(Landroid/net/Uri;)Landroid/net/Uri;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$1100()I
    .locals 1

    sget v0, Lcom/android/mail/browse/ConversationCursor;->sSequence:I

    return v0
.end method

.method static synthetic access$1108()I
    .locals 2

    sget v0, Lcom/android/mail/browse/ConversationCursor;->sSequence:I

    add-int/lit8 v1, v0, 0x1

    sput v1, Lcom/android/mail/browse/ConversationCursor;->sSequence:I

    return v0
.end method

.method static synthetic access$1200(Landroid/net/Uri;)Ljava/lang/String;
    .locals 1
    .param p0    # Landroid/net/Uri;

    invoke-static {p0}, Lcom/android/mail/browse/ConversationCursor;->uriStringFromCachingUri(Landroid/net/Uri;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$1300(Lcom/android/mail/browse/ConversationCursor;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Object;)V
    .locals 0
    .param p0    # Lcom/android/mail/browse/ConversationCursor;
    .param p1    # Ljava/lang/String;
    .param p2    # Ljava/lang/String;
    .param p3    # Ljava/lang/Object;

    invoke-direct {p0, p1, p2, p3}, Lcom/android/mail/browse/ConversationCursor;->cacheValue(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Object;)V

    return-void
.end method

.method static synthetic access$1400(Lcom/android/mail/browse/ConversationCursor;)V
    .locals 0
    .param p0    # Lcom/android/mail/browse/ConversationCursor;

    invoke-direct {p0}, Lcom/android/mail/browse/ConversationCursor;->recalibratePosition()V

    return-void
.end method

.method static synthetic access$1500(Lcom/android/mail/browse/ConversationCursor;)V
    .locals 0
    .param p0    # Lcom/android/mail/browse/ConversationCursor;

    invoke-direct {p0}, Lcom/android/mail/browse/ConversationCursor;->notifyDataChanged()V

    return-void
.end method

.method static synthetic access$200()Ljava/lang/String;
    .locals 1

    sget-object v0, Lcom/android/mail/browse/ConversationCursor;->LOG_TAG:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$300(Lcom/android/mail/browse/ConversationCursor;)Z
    .locals 1
    .param p0    # Lcom/android/mail/browse/ConversationCursor;

    iget-boolean v0, p0, Lcom/android/mail/browse/ConversationCursor;->mPaused:Z

    return v0
.end method

.method static synthetic access$400(Lcom/android/mail/browse/ConversationCursor;)Z
    .locals 1
    .param p0    # Lcom/android/mail/browse/ConversationCursor;

    iget-boolean v0, p0, Lcom/android/mail/browse/ConversationCursor;->mDeferSync:Z

    return v0
.end method

.method static synthetic access$502(Lcom/android/mail/browse/ConversationCursor;Lcom/android/mail/browse/ConversationCursor$UnderlyingCursorWrapper;)Lcom/android/mail/browse/ConversationCursor$UnderlyingCursorWrapper;
    .locals 0
    .param p0    # Lcom/android/mail/browse/ConversationCursor;
    .param p1    # Lcom/android/mail/browse/ConversationCursor$UnderlyingCursorWrapper;

    iput-object p1, p0, Lcom/android/mail/browse/ConversationCursor;->mRequeryCursor:Lcom/android/mail/browse/ConversationCursor$UnderlyingCursorWrapper;

    return-object p1
.end method

.method static synthetic access$602(Lcom/android/mail/browse/ConversationCursor;Z)Z
    .locals 0
    .param p0    # Lcom/android/mail/browse/ConversationCursor;
    .param p1    # Z

    iput-boolean p1, p0, Lcom/android/mail/browse/ConversationCursor;->mRefreshReady:Z

    return p1
.end method

.method static synthetic access$700(Lcom/android/mail/browse/ConversationCursor;)V
    .locals 0
    .param p0    # Lcom/android/mail/browse/ConversationCursor;

    invoke-direct {p0}, Lcom/android/mail/browse/ConversationCursor;->notifyRefreshReady()V

    return-void
.end method

.method static synthetic access$900(Lcom/android/mail/browse/ConversationCursor;)V
    .locals 0
    .param p0    # Lcom/android/mail/browse/ConversationCursor;

    invoke-direct {p0}, Lcom/android/mail/browse/ConversationCursor;->underlyingChanged()V

    return-void
.end method

.method private apply(Landroid/content/Context;Ljava/util/Collection;)I
    .locals 1
    .param p1    # Landroid/content/Context;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Ljava/util/Collection",
            "<",
            "Lcom/android/mail/browse/ConversationCursor$ConversationOperation;",
            ">;)I"
        }
    .end annotation

    sget-object v0, Lcom/android/mail/browse/ConversationCursor;->sProvider:Lcom/android/mail/browse/ConversationCursor$ConversationProvider;

    invoke-virtual {v0, p2, p0}, Lcom/android/mail/browse/ConversationCursor$ConversationProvider;->apply(Ljava/util/Collection;Lcom/android/mail/browse/ConversationCursor;)I

    move-result v0

    return v0
.end method

.method private applyAction(Landroid/content/Context;Ljava/util/Collection;I)I
    .locals 5
    .param p1    # Landroid/content/Context;
    .param p3    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Ljava/util/Collection",
            "<",
            "Lcom/android/mail/providers/Conversation;",
            ">;I)I"
        }
    .end annotation

    invoke-static {}, Lcom/google/common/collect/Lists;->newArrayList()Ljava/util/ArrayList;

    move-result-object v3

    invoke-interface {p2}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/mail/providers/Conversation;

    new-instance v2, Lcom/android/mail/browse/ConversationCursor$ConversationOperation;

    invoke-direct {v2, p0, p3, v0}, Lcom/android/mail/browse/ConversationCursor$ConversationOperation;-><init>(Lcom/android/mail/browse/ConversationCursor;ILcom/android/mail/providers/Conversation;)V

    invoke-virtual {v3, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_0
    invoke-direct {p0, p1, v3}, Lcom/android/mail/browse/ConversationCursor;->apply(Landroid/content/Context;Ljava/util/Collection;)I

    move-result v4

    return v4
.end method

.method private cacheValue(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Object;)V
    .locals 11
    .param p1    # Ljava/lang/String;
    .param p2    # Ljava/lang/String;
    .param p3    # Ljava/lang/Object;

    const/4 v6, 0x1

    const/4 v7, 0x0

    invoke-static {}, Lcom/android/mail/browse/ConversationCursor;->offUiThread()Z

    move-result v5

    if-eqz v5, :cond_0

    sget-object v5, Lcom/android/mail/browse/ConversationCursor;->LOG_TAG:Ljava/lang/String;

    new-instance v8, Ljava/lang/Error;

    invoke-direct {v8}, Ljava/lang/Error;-><init>()V

    const-string v9, "cacheValue incorrectly being called from non-UI thread"

    new-array v10, v7, [Ljava/lang/Object;

    invoke-static {v5, v8, v9, v10}, Lcom/android/mail/utils/LogUtils;->e(Ljava/lang/String;Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)I

    :cond_0
    iget-object v8, p0, Lcom/android/mail/browse/ConversationCursor;->mCacheMapLock:Ljava/lang/Object;

    monitor-enter v8

    :try_start_0
    iget-object v5, p0, Lcom/android/mail/browse/ConversationCursor;->mCacheMap:Ljava/util/HashMap;

    invoke-virtual {v5, p1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/content/ContentValues;

    if-nez v3, :cond_1

    new-instance v3, Landroid/content/ContentValues;

    invoke-direct {v3}, Landroid/content/ContentValues;-><init>()V

    iget-object v5, p0, Lcom/android/mail/browse/ConversationCursor;->mCacheMap:Ljava/util/HashMap;

    invoke-virtual {v5, p1, v3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    :cond_1
    const-string v5, "__deleted__"

    if-ne p2, v5, :cond_2

    move-object v0, p3

    check-cast v0, Ljava/lang/Boolean;

    move-object v5, v0

    invoke-virtual {v5}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v4

    invoke-virtual {v3, p2}, Landroid/content/ContentValues;->get(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v5

    if-eqz v5, :cond_3

    move v2, v6

    :goto_0
    if-eqz v4, :cond_4

    if-nez v2, :cond_4

    iget v5, p0, Lcom/android/mail/browse/ConversationCursor;->mDeletedCount:I

    add-int/lit8 v5, v5, 0x1

    iput v5, p0, Lcom/android/mail/browse/ConversationCursor;->mDeletedCount:I

    :cond_2
    instance-of v5, p3, Ljava/lang/Boolean;

    if-eqz v5, :cond_7

    check-cast p3, Ljava/lang/Boolean;

    invoke-virtual {p3}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v5

    if-eqz v5, :cond_6

    move v5, v6

    :goto_1
    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v3, p2, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    :goto_2
    const-string v5, "__updatetime__"

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v6

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v6

    invoke-virtual {v3, v5, v6}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    monitor-exit v8

    :goto_3
    return-void

    :cond_3
    move v2, v7

    goto :goto_0

    :cond_4
    if-nez v4, :cond_5

    if-eqz v2, :cond_5

    iget v5, p0, Lcom/android/mail/browse/ConversationCursor;->mDeletedCount:I

    add-int/lit8 v5, v5, -0x1

    iput v5, p0, Lcom/android/mail/browse/ConversationCursor;->mDeletedCount:I

    invoke-virtual {v3, p2}, Landroid/content/ContentValues;->remove(Ljava/lang/String;)V

    monitor-exit v8

    goto :goto_3

    :catchall_0
    move-exception v5

    monitor-exit v8
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v5

    :cond_5
    if-nez v4, :cond_2

    :try_start_1
    monitor-exit v8

    goto :goto_3

    :cond_6
    move v5, v7

    goto :goto_1

    :cond_7
    instance-of v5, p3, Ljava/lang/Integer;

    if-eqz v5, :cond_8

    check-cast p3, Ljava/lang/Integer;

    invoke-virtual {v3, p2, p3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    goto :goto_2

    :cond_8
    instance-of v5, p3, Ljava/lang/String;

    if-eqz v5, :cond_9

    check-cast p3, Ljava/lang/String;

    invoke-virtual {v3, p2, p3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_2

    :cond_9
    instance-of v5, p3, [B

    if-eqz v5, :cond_a

    check-cast p3, [B

    check-cast p3, [B

    invoke-virtual {v3, p2, p3}, Landroid/content/ContentValues;->put(Ljava/lang/String;[B)V

    goto :goto_2

    :cond_a
    invoke-virtual {p3}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v1

    new-instance v5, Ljava/lang/IllegalArgumentException;

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "Value class not compatible with cache: "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-direct {v5, v6}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v5
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0
.end method

.method private checkNotifyUI()V
    .locals 7

    const/4 v6, 0x2

    const/4 v1, 0x1

    const/4 v2, 0x0

    sget-object v3, Lcom/android/mail/browse/ConversationCursor;->LOG_TAG:Ljava/lang/String;

    const-string v4, "Received notify ui callback and sending a notification is enabled? %s and refresh ready ? %s"

    new-array v5, v6, [Ljava/lang/Object;

    iget-boolean v0, p0, Lcom/android/mail/browse/ConversationCursor;->mPaused:Z

    if-nez v0, :cond_2

    iget-boolean v0, p0, Lcom/android/mail/browse/ConversationCursor;->mDeferSync:Z

    if-nez v0, :cond_2

    move v0, v1

    :goto_0
    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    aput-object v0, v5, v2

    iget-boolean v0, p0, Lcom/android/mail/browse/ConversationCursor;->mRefreshReady:Z

    if-nez v0, :cond_0

    iget-boolean v0, p0, Lcom/android/mail/browse/ConversationCursor;->mRefreshRequired:Z

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/android/mail/browse/ConversationCursor;->mRefreshTask:Lcom/android/mail/browse/ConversationCursor$RefreshTask;

    if-nez v0, :cond_3

    :cond_0
    move v0, v1

    :goto_1
    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    aput-object v0, v5, v1

    invoke-static {v3, v4, v5}, Lcom/android/mail/utils/LogUtils;->d(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)I

    iget-boolean v0, p0, Lcom/android/mail/browse/ConversationCursor;->mPaused:Z

    if-nez v0, :cond_5

    iget-boolean v0, p0, Lcom/android/mail/browse/ConversationCursor;->mDeferSync:Z

    if-nez v0, :cond_5

    iget-boolean v0, p0, Lcom/android/mail/browse/ConversationCursor;->mRefreshRequired:Z

    if-eqz v0, :cond_4

    iget-object v0, p0, Lcom/android/mail/browse/ConversationCursor;->mRefreshTask:Lcom/android/mail/browse/ConversationCursor$RefreshTask;

    if-nez v0, :cond_4

    invoke-direct {p0}, Lcom/android/mail/browse/ConversationCursor;->notifyRefreshRequired()V

    :cond_1
    :goto_2
    return-void

    :cond_2
    move v0, v2

    goto :goto_0

    :cond_3
    move v0, v2

    goto :goto_1

    :cond_4
    iget-boolean v0, p0, Lcom/android/mail/browse/ConversationCursor;->mRefreshReady:Z

    if-eqz v0, :cond_1

    invoke-direct {p0}, Lcom/android/mail/browse/ConversationCursor;->notifyRefreshReady()V

    goto :goto_2

    :cond_5
    sget-object v3, Lcom/android/mail/browse/ConversationCursor;->LOG_TAG:Ljava/lang/String;

    const-string v4, "[checkNotifyUI: %s%s"

    new-array v5, v6, [Ljava/lang/Object;

    iget-boolean v0, p0, Lcom/android/mail/browse/ConversationCursor;->mPaused:Z

    if-eqz v0, :cond_6

    const-string v0, "Paused "

    :goto_3
    aput-object v0, v5, v2

    iget-boolean v0, p0, Lcom/android/mail/browse/ConversationCursor;->mDeferSync:Z

    if-eqz v0, :cond_7

    const-string v0, "Defer"

    :goto_4
    aput-object v0, v5, v1

    invoke-static {v3, v4, v5}, Lcom/android/mail/utils/LogUtils;->d(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)I

    goto :goto_2

    :cond_6
    const-string v0, ""

    goto :goto_3

    :cond_7
    const-string v0, ""

    goto :goto_4
.end method

.method private doQuery(Z)Lcom/android/mail/browse/ConversationCursor$UnderlyingCursorWrapper;
    .locals 9
    .param p1    # Z

    const/4 v3, 0x0

    iget-object v1, p0, Lcom/android/mail/browse/ConversationCursor;->qUri:Landroid/net/Uri;

    if-eqz p1, :cond_0

    invoke-virtual {v1}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v0

    const-string v2, "limit"

    const-string v4, "50"

    invoke-virtual {v0, v2, v4}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v1

    :cond_0
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v7

    iget-object v0, p0, Lcom/android/mail/browse/ConversationCursor;->mResolver:Landroid/content/ContentResolver;

    iget-object v2, p0, Lcom/android/mail/browse/ConversationCursor;->qProjection:[Ljava/lang/String;

    move-object v4, v3

    move-object v5, v3

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v6

    if-nez v6, :cond_1

    sget-object v0, Lcom/android/mail/browse/ConversationCursor;->LOG_TAG:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "doQuery returning null cursor, uri: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    :cond_1
    new-instance v0, Lcom/android/mail/browse/ConversationCursor$UnderlyingCursorWrapper;

    invoke-direct {v0, p0, v6}, Lcom/android/mail/browse/ConversationCursor$UnderlyingCursorWrapper;-><init>(Lcom/android/mail/browse/ConversationCursor;Landroid/database/Cursor;)V

    return-object v0
.end method

.method private getCachedValue(I)Ljava/lang/Object;
    .locals 3
    .param p1    # I

    iget-object v1, p0, Lcom/android/mail/browse/ConversationCursor;->mUnderlyingCursor:Lcom/android/mail/browse/ConversationCursor$UnderlyingCursorWrapper;

    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Lcom/android/mail/browse/ConversationCursor$UnderlyingCursorWrapper;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0, p1}, Lcom/android/mail/browse/ConversationCursor;->getCachedValue(Ljava/lang/String;I)Ljava/lang/Object;

    move-result-object v1

    return-object v1
.end method

.method private getCachedValue(Ljava/lang/String;I)Ljava/lang/Object;
    .locals 3
    .param p1    # Ljava/lang/String;
    .param p2    # I

    iget-object v2, p0, Lcom/android/mail/browse/ConversationCursor;->mCacheMap:Ljava/util/HashMap;

    invoke-virtual {v2, p1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/content/ContentValues;

    if-eqz v1, :cond_1

    const/4 v2, -0x1

    if-ne p2, v2, :cond_0

    const-string v0, "__deleted__"

    :goto_0
    invoke-virtual {v1, v0}, Landroid/content/ContentValues;->get(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v2

    :goto_1
    return-object v2

    :cond_0
    iget-object v2, p0, Lcom/android/mail/browse/ConversationCursor;->mColumnNames:[Ljava/lang/String;

    aget-object v0, v2, p2

    goto :goto_0

    :cond_1
    const/4 v2, 0x0

    goto :goto_1
.end method

.method private getOperationsForConversations(Ljava/util/Collection;ILandroid/content/ContentValues;)Ljava/util/ArrayList;
    .locals 4
    .param p2    # I
    .param p3    # Landroid/content/ContentValues;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Collection",
            "<",
            "Lcom/android/mail/providers/Conversation;",
            ">;I",
            "Landroid/content/ContentValues;",
            ")",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/android/mail/browse/ConversationCursor$ConversationOperation;",
            ">;"
        }
    .end annotation

    invoke-static {}, Lcom/google/common/collect/Lists;->newArrayList()Ljava/util/ArrayList;

    move-result-object v2

    invoke-interface {p1}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/mail/providers/Conversation;

    invoke-virtual {p0, v0, p2, p3}, Lcom/android/mail/browse/ConversationCursor;->getOperationForConversation(Lcom/android/mail/providers/Conversation;ILandroid/content/ContentValues;)Lcom/android/mail/browse/ConversationCursor$ConversationOperation;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_0
    return-object v2
.end method

.method private notifyDataChanged()V
    .locals 4

    iget-object v3, p0, Lcom/android/mail/browse/ConversationCursor;->mListeners:Ljava/util/List;

    monitor-enter v3

    :try_start_0
    iget-object v2, p0, Lcom/android/mail/browse/ConversationCursor;->mListeners:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/android/mail/browse/ConversationCursor$ConversationListener;

    invoke-interface {v1}, Lcom/android/mail/browse/ConversationCursor$ConversationListener;->onDataSetChanged()V

    goto :goto_0

    :catchall_0
    move-exception v2

    monitor-exit v3
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v2

    :cond_0
    :try_start_1
    monitor-exit v3
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    invoke-virtual {p0}, Lcom/android/mail/browse/ConversationCursor;->handleNotificationActions()V

    return-void
.end method

.method private notifyRefreshReady()V
    .locals 4

    iget-object v3, p0, Lcom/android/mail/browse/ConversationCursor;->mListeners:Ljava/util/List;

    monitor-enter v3

    :try_start_0
    iget-object v2, p0, Lcom/android/mail/browse/ConversationCursor;->mListeners:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/android/mail/browse/ConversationCursor$ConversationListener;

    invoke-interface {v1}, Lcom/android/mail/browse/ConversationCursor$ConversationListener;->onRefreshReady()V

    goto :goto_0

    :catchall_0
    move-exception v2

    monitor-exit v3
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v2

    :cond_0
    :try_start_1
    monitor-exit v3
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    return-void
.end method

.method private notifyRefreshRequired()V
    .locals 4

    iget-boolean v2, p0, Lcom/android/mail/browse/ConversationCursor;->mDeferSync:Z

    if-nez v2, :cond_1

    iget-object v3, p0, Lcom/android/mail/browse/ConversationCursor;->mListeners:Ljava/util/List;

    monitor-enter v3

    :try_start_0
    iget-object v2, p0, Lcom/android/mail/browse/ConversationCursor;->mListeners:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/android/mail/browse/ConversationCursor$ConversationListener;

    invoke-interface {v1}, Lcom/android/mail/browse/ConversationCursor$ConversationListener;->onRefreshRequired()V

    goto :goto_0

    :catchall_0
    move-exception v2

    monitor-exit v3
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v2

    :cond_0
    :try_start_1
    monitor-exit v3
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    :cond_1
    return-void
.end method

.method static offUiThread()Z
    .locals 2

    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v0

    invoke-virtual {v0}, Landroid/os/Looper;->getThread()Ljava/lang/Thread;

    move-result-object v0

    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v1

    if-eq v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private recalibratePosition()V
    .locals 1

    iget v0, p0, Lcom/android/mail/browse/ConversationCursor;->mPosition:I

    invoke-virtual {p0}, Lcom/android/mail/browse/ConversationCursor;->moveToFirst()Z

    invoke-virtual {p0, v0}, Lcom/android/mail/browse/ConversationCursor;->moveToPosition(I)Z

    return-void
.end method

.method private resetCursor(Lcom/android/mail/browse/ConversationCursor$UnderlyingCursorWrapper;)V
    .locals 17
    .param p1    # Lcom/android/mail/browse/ConversationCursor$UnderlyingCursorWrapper;

    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/android/mail/browse/ConversationCursor;->mCacheMapLock:Ljava/lang/Object;

    monitor-enter v12

    :try_start_0
    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/android/mail/browse/ConversationCursor;->mCacheMap:Ljava/util/HashMap;

    invoke-virtual {v11}, Ljava/util/HashMap;->entrySet()Ljava/util/Set;

    move-result-object v11

    invoke-interface {v11}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v3

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v5

    :cond_0
    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v11

    if-eqz v11, :cond_6

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/util/Map$Entry;

    invoke-interface {v2}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Landroid/content/ContentValues;

    invoke-interface {v2}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/String;

    const/4 v10, 0x0

    const/4 v7, 0x0

    if-eqz v9, :cond_5

    const-string v11, "__updatetime__"

    invoke-virtual {v9, v11}, Landroid/content/ContentValues;->getAsLong(Ljava/lang/String;)Ljava/lang/Long;

    move-result-object v8

    if-eqz v8, :cond_4

    invoke-virtual {v8}, Ljava/lang/Long;->longValue()J

    move-result-wide v13

    sub-long v13, v5, v13

    const-wide/16 v15, 0x2710

    cmp-long v11, v13, v15

    if-gez v11, :cond_4

    sget-object v11, Lcom/android/mail/browse/ConversationCursor;->LOG_TAG:Ljava/lang/String;

    const-string v13, "IN resetCursor, keep recent changes to %s"

    const/4 v14, 0x1

    new-array v14, v14, [Ljava/lang/Object;

    const/4 v15, 0x0

    aput-object v4, v14, v15

    invoke-static {v11, v13, v14}, Lcom/android/mail/utils/LogUtils;->d(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)I

    const/4 v10, 0x1

    :cond_1
    :goto_1
    const-string v11, "__deleted__"

    invoke-virtual {v9, v11}, Landroid/content/ContentValues;->containsKey(Ljava/lang/String;)Z

    move-result v11

    if-eqz v11, :cond_2

    move-object/from16 v0, p1

    invoke-virtual {v0, v4}, Lcom/android/mail/browse/ConversationCursor$UnderlyingCursorWrapper;->contains(Ljava/lang/String;)Z

    move-result v11

    if-nez v11, :cond_2

    move-object/from16 v0, p0

    iget v11, v0, Lcom/android/mail/browse/ConversationCursor;->mDeletedCount:I

    add-int/lit8 v11, v11, -0x1

    move-object/from16 v0, p0

    iput v11, v0, Lcom/android/mail/browse/ConversationCursor;->mDeletedCount:I

    const/4 v7, 0x1

    sget-object v11, Lcom/android/mail/browse/ConversationCursor;->LOG_TAG:Ljava/lang/String;

    const-string v13, "IN resetCursor, sDeletedCount decremented to: %d by %s"

    const/4 v14, 0x2

    new-array v14, v14, [Ljava/lang/Object;

    const/4 v15, 0x0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/android/mail/browse/ConversationCursor;->mDeletedCount:I

    move/from16 v16, v0

    invoke-static/range {v16 .. v16}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v16

    aput-object v16, v14, v15

    const/4 v15, 0x1

    aput-object v4, v14, v15

    invoke-static {v11, v13, v14}, Lcom/android/mail/utils/LogUtils;->d(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)I

    :cond_2
    :goto_2
    if-eqz v10, :cond_3

    if-eqz v7, :cond_0

    :cond_3
    invoke-interface {v3}, Ljava/util/Iterator;->remove()V

    goto :goto_0

    :catchall_0
    move-exception v11

    monitor-exit v12
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v11

    :cond_4
    if-nez v8, :cond_1

    :try_start_1
    sget-object v11, Lcom/android/mail/browse/ConversationCursor;->LOG_TAG:Ljava/lang/String;

    const-string v13, "null updateTime from mCacheMap for key: %s"

    const/4 v14, 0x1

    new-array v14, v14, [Ljava/lang/Object;

    const/4 v15, 0x0

    aput-object v4, v14, v15

    invoke-static {v11, v13, v14}, Lcom/android/mail/utils/LogUtils;->e(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)I

    goto :goto_1

    :cond_5
    sget-object v11, Lcom/android/mail/browse/ConversationCursor;->LOG_TAG:Ljava/lang/String;

    const-string v13, "null ContentValues from mCacheMap for key: %s"

    const/4 v14, 0x1

    new-array v14, v14, [Ljava/lang/Object;

    const/4 v15, 0x0

    aput-object v4, v14, v15

    invoke-static {v11, v13, v14}, Lcom/android/mail/utils/LogUtils;->e(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)I

    goto :goto_2

    :cond_6
    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/android/mail/browse/ConversationCursor;->mUnderlyingCursor:Lcom/android/mail/browse/ConversationCursor$UnderlyingCursorWrapper;

    if-eqz v11, :cond_7

    invoke-virtual/range {p0 .. p0}, Lcom/android/mail/browse/ConversationCursor;->close()V

    :cond_7
    move-object/from16 v0, p1

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/android/mail/browse/ConversationCursor;->mUnderlyingCursor:Lcom/android/mail/browse/ConversationCursor$UnderlyingCursorWrapper;

    const/4 v11, -0x1

    move-object/from16 v0, p0

    iput v11, v0, Lcom/android/mail/browse/ConversationCursor;->mPosition:I

    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/android/mail/browse/ConversationCursor;->mUnderlyingCursor:Lcom/android/mail/browse/ConversationCursor$UnderlyingCursorWrapper;

    move-object/from16 v0, p0

    iget v13, v0, Lcom/android/mail/browse/ConversationCursor;->mPosition:I

    invoke-virtual {v11, v13}, Lcom/android/mail/browse/ConversationCursor$UnderlyingCursorWrapper;->moveToPosition(I)Z

    move-object/from16 v0, p0

    iget-boolean v11, v0, Lcom/android/mail/browse/ConversationCursor;->mCursorObserverRegistered:Z

    if-nez v11, :cond_8

    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/android/mail/browse/ConversationCursor;->mUnderlyingCursor:Lcom/android/mail/browse/ConversationCursor$UnderlyingCursorWrapper;

    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/android/mail/browse/ConversationCursor;->mCursorObserver:Lcom/android/mail/browse/ConversationCursor$CursorObserver;

    invoke-virtual {v11, v13}, Lcom/android/mail/browse/ConversationCursor$UnderlyingCursorWrapper;->registerContentObserver(Landroid/database/ContentObserver;)V

    const/4 v11, 0x1

    move-object/from16 v0, p0

    iput-boolean v11, v0, Lcom/android/mail/browse/ConversationCursor;->mCursorObserverRegistered:Z

    :cond_8
    const/4 v11, 0x0

    move-object/from16 v0, p0

    iput-boolean v11, v0, Lcom/android/mail/browse/ConversationCursor;->mRefreshRequired:Z

    monitor-exit v12
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    return-void
.end method

.method private resetNotificationActions()V
    .locals 5

    iget-object v3, p0, Lcom/android/mail/browse/ConversationCursor;->mNotificationTempDeleted:Ljava/util/Set;

    invoke-interface {v3}, Ljava/util/Set;->isEmpty()Z

    move-result v3

    if-nez v3, :cond_0

    const/4 v0, 0x1

    :goto_0
    iget-object v3, p0, Lcom/android/mail/browse/ConversationCursor;->mNotificationTempDeleted:Ljava/util/Set;

    invoke-interface {v3}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/android/mail/providers/Conversation;

    sget-object v3, Lcom/android/mail/browse/ConversationCursor;->sProvider:Lcom/android/mail/browse/ConversationCursor$ConversationProvider;

    iget-object v4, v1, Lcom/android/mail/providers/Conversation;->uri:Landroid/net/Uri;

    invoke-virtual {v3, v4, p0}, Lcom/android/mail/browse/ConversationCursor$ConversationProvider;->undeleteLocal(Landroid/net/Uri;Lcom/android/mail/browse/ConversationCursor;)V

    goto :goto_1

    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    :cond_1
    iget-object v3, p0, Lcom/android/mail/browse/ConversationCursor;->mNotificationTempDeleted:Ljava/util/Set;

    invoke-interface {v3}, Ljava/util/Set;->clear()V

    if-eqz v0, :cond_2

    iget-object v3, p0, Lcom/android/mail/browse/ConversationCursor;->mMainThreadHandler:Landroid/os/Handler;

    new-instance v4, Lcom/android/mail/browse/ConversationCursor$2;

    invoke-direct {v4, p0}, Lcom/android/mail/browse/ConversationCursor$2;-><init>(Lcom/android/mail/browse/ConversationCursor;)V

    invoke-virtual {v3, v4}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    :cond_2
    return-void
.end method

.method private setCursor(Lcom/android/mail/browse/ConversationCursor$UnderlyingCursorWrapper;)V
    .locals 2
    .param p1    # Lcom/android/mail/browse/ConversationCursor$UnderlyingCursorWrapper;

    const/4 v1, 0x0

    iget-object v0, p0, Lcom/android/mail/browse/ConversationCursor;->mUnderlyingCursor:Lcom/android/mail/browse/ConversationCursor$UnderlyingCursorWrapper;

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/android/mail/browse/ConversationCursor;->close()V

    :cond_0
    invoke-virtual {p1}, Lcom/android/mail/browse/ConversationCursor$UnderlyingCursorWrapper;->getColumnNames()[Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/android/mail/browse/ConversationCursor;->mColumnNames:[Ljava/lang/String;

    iput-boolean v1, p0, Lcom/android/mail/browse/ConversationCursor;->mRefreshRequired:Z

    iput-boolean v1, p0, Lcom/android/mail/browse/ConversationCursor;->mRefreshReady:Z

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/android/mail/browse/ConversationCursor;->mRefreshTask:Lcom/android/mail/browse/ConversationCursor$RefreshTask;

    invoke-direct {p0, p1}, Lcom/android/mail/browse/ConversationCursor;->resetCursor(Lcom/android/mail/browse/ConversationCursor$UnderlyingCursorWrapper;)V

    invoke-direct {p0}, Lcom/android/mail/browse/ConversationCursor;->resetNotificationActions()V

    invoke-virtual {p0}, Lcom/android/mail/browse/ConversationCursor;->handleNotificationActions()V

    return-void
.end method

.method private underlyingChanged()V
    .locals 3

    iget-object v1, p0, Lcom/android/mail/browse/ConversationCursor;->mCacheMapLock:Ljava/lang/Object;

    monitor-enter v1

    :try_start_0
    iget-boolean v0, p0, Lcom/android/mail/browse/ConversationCursor;->mCursorObserverRegistered:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-eqz v0, :cond_0

    :try_start_1
    iget-object v0, p0, Lcom/android/mail/browse/ConversationCursor;->mUnderlyingCursor:Lcom/android/mail/browse/ConversationCursor$UnderlyingCursorWrapper;

    iget-object v2, p0, Lcom/android/mail/browse/ConversationCursor;->mCursorObserver:Lcom/android/mail/browse/ConversationCursor$CursorObserver;

    invoke-virtual {v0, v2}, Lcom/android/mail/browse/ConversationCursor$UnderlyingCursorWrapper;->unregisterContentObserver(Landroid/database/ContentObserver;)V
    :try_end_1
    .catch Ljava/lang/IllegalStateException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    :goto_0
    const/4 v0, 0x0

    :try_start_2
    iput-boolean v0, p0, Lcom/android/mail/browse/ConversationCursor;->mCursorObserverRegistered:Z

    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/android/mail/browse/ConversationCursor;->mRefreshRequired:Z

    iget-boolean v0, p0, Lcom/android/mail/browse/ConversationCursor;->mPaused:Z

    if-nez v0, :cond_1

    invoke-direct {p0}, Lcom/android/mail/browse/ConversationCursor;->notifyRefreshRequired()V

    :cond_1
    monitor-exit v1

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    throw v0

    :catch_0
    move-exception v0

    goto :goto_0
.end method

.method private undoLocal()V
    .locals 1

    sget-object v0, Lcom/android/mail/browse/ConversationCursor;->sProvider:Lcom/android/mail/browse/ConversationCursor$ConversationProvider;

    invoke-virtual {v0, p0}, Lcom/android/mail/browse/ConversationCursor$ConversationProvider;->undo(Lcom/android/mail/browse/ConversationCursor;)V

    return-void
.end method

.method private static uriFromCachingUri(Landroid/net/Uri;)Landroid/net/Uri;
    .locals 6
    .param p0    # Landroid/net/Uri;

    invoke-virtual {p0}, Landroid/net/Uri;->getAuthority()Ljava/lang/String;

    move-result-object v0

    sget-object v4, Lcom/android/mail/browse/ConversationCursor$ConversationProvider;->AUTHORITY:Ljava/lang/String;

    invoke-virtual {v0, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_0

    :goto_0
    return-object p0

    :cond_0
    invoke-virtual {p0}, Landroid/net/Uri;->getPathSegments()Ljava/util/List;

    move-result-object v3

    new-instance v4, Landroid/net/Uri$Builder;

    invoke-direct {v4}, Landroid/net/Uri$Builder;-><init>()V

    invoke-virtual {p0}, Landroid/net/Uri;->getScheme()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Landroid/net/Uri$Builder;->scheme(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v5

    const/4 v4, 0x0

    invoke-interface {v3, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/String;

    invoke-virtual {v5, v4}, Landroid/net/Uri$Builder;->authority(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v1

    const/4 v2, 0x1

    :goto_1
    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v4

    if-ge v2, v4, :cond_1

    invoke-interface {v3, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/String;

    invoke-virtual {v1, v4}, Landroid/net/Uri$Builder;->appendPath(Ljava/lang/String;)Landroid/net/Uri$Builder;

    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    :cond_1
    invoke-virtual {v1}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object p0

    goto :goto_0
.end method

.method private static uriStringFromCachingUri(Landroid/net/Uri;)Ljava/lang/String;
    .locals 2
    .param p0    # Landroid/net/Uri;

    invoke-static {p0}, Lcom/android/mail/browse/ConversationCursor;->uriFromCachingUri(Landroid/net/Uri;)Landroid/net/Uri;

    move-result-object v0

    invoke-virtual {v0}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Landroid/net/Uri;->decode(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    return-object v1
.end method

.method private static uriToCachingUriString(Landroid/net/Uri;)Ljava/lang/String;
    .locals 3
    .param p0    # Landroid/net/Uri;

    invoke-virtual {p0}, Landroid/net/Uri;->getAuthority()Ljava/lang/String;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0}, Landroid/net/Uri;->getScheme()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "://"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    sget-object v2, Lcom/android/mail/browse/ConversationCursor$ConversationProvider;->AUTHORITY:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "/"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Landroid/net/Uri;->getPath()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    return-object v1
.end method


# virtual methods
.method public addFolderUpdates(Ljava/util/ArrayList;Ljava/util/ArrayList;Landroid/content/ContentValues;)V
    .locals 5
    .param p3    # Landroid/content/ContentValues;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Landroid/net/Uri;",
            ">;",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/Boolean;",
            ">;",
            "Landroid/content/ContentValues;",
            ")V"
        }
    .end annotation

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    const/4 v1, 0x0

    :goto_0
    invoke-virtual {p1}, Ljava/util/ArrayList;->size()I

    move-result v2

    if-ge v1, v2, :cond_0

    invoke-virtual {p1, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/net/Uri;

    invoke-virtual {v2}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v2

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p2, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ""

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/net/Uri$Builder;->appendPath(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v2

    invoke-virtual {v2}, Landroid/net/Uri$Builder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_0
    const-string v2, "folders_updated"

    const-string v3, ","

    invoke-static {v3, v0}, Landroid/text/TextUtils;->join(Ljava/lang/CharSequence;Ljava/lang/Iterable;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p3, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method public addListener(Lcom/android/mail/browse/ConversationCursor$ConversationListener;)V
    .locals 4
    .param p1    # Lcom/android/mail/browse/ConversationCursor$ConversationListener;

    iget-object v1, p0, Lcom/android/mail/browse/ConversationCursor;->mListeners:Ljava/util/List;

    monitor-enter v1

    :try_start_0
    iget-object v0, p0, Lcom/android/mail/browse/ConversationCursor;->mListeners:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/android/mail/browse/ConversationCursor;->mListeners:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :goto_0
    monitor-exit v1

    return-void

    :cond_0
    sget-object v0, Lcom/android/mail/browse/ConversationCursor;->LOG_TAG:Ljava/lang/String;

    const-string v2, "Ignoring duplicate add of listener"

    const/4 v3, 0x0

    new-array v3, v3, [Ljava/lang/Object;

    invoke-static {v0, v2, v3}, Lcom/android/mail/utils/LogUtils;->d(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)I

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public addTargetFolders(Ljava/util/Collection;Landroid/content/ContentValues;)V
    .locals 2
    .param p2    # Landroid/content/ContentValues;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Collection",
            "<",
            "Lcom/android/mail/providers/Folder;",
            ">;",
            "Landroid/content/ContentValues;",
            ")V"
        }
    .end annotation

    const-string v0, "rawFolders"

    invoke-static {p1}, Lcom/android/mail/providers/FolderList;->copyOf(Ljava/util/Collection;)Lcom/android/mail/providers/FolderList;

    move-result-object v1

    invoke-virtual {v1}, Lcom/android/mail/providers/FolderList;->toBlob()[B

    move-result-object v1

    invoke-virtual {p2, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;[B)V

    return-void
.end method

.method public archive(Landroid/content/Context;Ljava/util/Collection;)I
    .locals 1
    .param p1    # Landroid/content/Context;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Ljava/util/Collection",
            "<",
            "Lcom/android/mail/providers/Conversation;",
            ">;)I"
        }
    .end annotation

    const/4 v0, 0x3

    invoke-direct {p0, p1, p2, v0}, Lcom/android/mail/browse/ConversationCursor;->applyAction(Landroid/content/Context;Ljava/util/Collection;I)I

    move-result v0

    return v0
.end method

.method clearMostlyDead(Ljava/lang/String;)Z
    .locals 4
    .param p1    # Ljava/lang/String;

    const/16 v2, 0xf

    invoke-direct {p0, p1, v2}, Lcom/android/mail/browse/ConversationCursor;->getCachedValue(Ljava/lang/String;I)Ljava/lang/Object;

    move-result-object v1

    if-eqz v1, :cond_0

    check-cast v1, Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v0

    and-int/lit8 v2, v0, 0x1

    if-eqz v2, :cond_0

    const-string v2, "conversationFlags"

    and-int/lit8 v0, v0, -0x2

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-direct {p0, p1, v2, v3}, Lcom/android/mail/browse/ConversationCursor;->cacheValue(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Object;)V

    const/4 v2, 0x1

    :goto_0
    return v2

    :cond_0
    const/4 v2, 0x0

    goto :goto_0
.end method

.method public close()V
    .locals 2

    iget-object v0, p0, Lcom/android/mail/browse/ConversationCursor;->mUnderlyingCursor:Lcom/android/mail/browse/ConversationCursor$UnderlyingCursorWrapper;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/android/mail/browse/ConversationCursor;->mUnderlyingCursor:Lcom/android/mail/browse/ConversationCursor$UnderlyingCursorWrapper;

    invoke-virtual {v0}, Lcom/android/mail/browse/ConversationCursor$UnderlyingCursorWrapper;->isClosed()Z

    move-result v0

    if-nez v0, :cond_1

    iget-boolean v0, p0, Lcom/android/mail/browse/ConversationCursor;->mCursorObserverRegistered:Z

    if-eqz v0, :cond_0

    :try_start_0
    iget-object v0, p0, Lcom/android/mail/browse/ConversationCursor;->mUnderlyingCursor:Lcom/android/mail/browse/ConversationCursor$UnderlyingCursorWrapper;

    iget-object v1, p0, Lcom/android/mail/browse/ConversationCursor;->mCursorObserver:Lcom/android/mail/browse/ConversationCursor$CursorObserver;

    invoke-virtual {v0, v1}, Lcom/android/mail/browse/ConversationCursor$UnderlyingCursorWrapper;->unregisterContentObserver(Landroid/database/ContentObserver;)V
    :try_end_0
    .catch Ljava/lang/IllegalStateException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/android/mail/browse/ConversationCursor;->mCursorObserverRegistered:Z

    :cond_0
    iget-object v0, p0, Lcom/android/mail/browse/ConversationCursor;->mUnderlyingCursor:Lcom/android/mail/browse/ConversationCursor$UnderlyingCursorWrapper;

    invoke-virtual {v0}, Lcom/android/mail/browse/ConversationCursor$UnderlyingCursorWrapper;->close()V

    :cond_1
    return-void

    :catch_0
    move-exception v0

    goto :goto_0
.end method

.method commitMostlyDead(Lcom/android/mail/providers/Conversation;)V
    .locals 5
    .param p1    # Lcom/android/mail/providers/Conversation;

    const/4 v4, 0x0

    iget v0, p1, Lcom/android/mail/providers/Conversation;->convFlags:I

    and-int/lit8 v0, v0, -0x2

    iput v0, p1, Lcom/android/mail/providers/Conversation;->convFlags:I

    iget-object v0, p0, Lcom/android/mail/browse/ConversationCursor;->mMostlyDead:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->remove(Ljava/lang/Object;)Z

    sget-object v0, Lcom/android/mail/browse/ConversationCursor;->LOG_TAG:Ljava/lang/String;

    const-string v1, "[All dead: %s]"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    iget-object v3, p1, Lcom/android/mail/providers/Conversation;->uri:Landroid/net/Uri;

    aput-object v3, v2, v4

    invoke-static {v0, v1, v2}, Lcom/android/mail/utils/LogUtils;->d(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)I

    iget-object v0, p0, Lcom/android/mail/browse/ConversationCursor;->mMostlyDead:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    iput-boolean v4, p0, Lcom/android/mail/browse/ConversationCursor;->mDeferSync:Z

    invoke-direct {p0}, Lcom/android/mail/browse/ConversationCursor;->checkNotifyUI()V

    :cond_0
    return-void
.end method

.method public copyStringToBuffer(ILandroid/database/CharArrayBuffer;)V
    .locals 1
    .param p1    # I
    .param p2    # Landroid/database/CharArrayBuffer;

    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public deactivate()V
    .locals 1

    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public delete(Landroid/content/Context;Ljava/util/Collection;)I
    .locals 1
    .param p1    # Landroid/content/Context;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Ljava/util/Collection",
            "<",
            "Lcom/android/mail/providers/Conversation;",
            ">;)I"
        }
    .end annotation

    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/android/mail/browse/ConversationCursor;->applyAction(Landroid/content/Context;Ljava/util/Collection;I)I

    move-result v0

    return v0
.end method

.method public disable()V
    .locals 1

    invoke-virtual {p0}, Lcom/android/mail/browse/ConversationCursor;->close()V

    iget-object v0, p0, Lcom/android/mail/browse/ConversationCursor;->mCacheMap:Ljava/util/HashMap;

    invoke-virtual {v0}, Ljava/util/HashMap;->clear()V

    iget-object v0, p0, Lcom/android/mail/browse/ConversationCursor;->mListeners:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/android/mail/browse/ConversationCursor;->mUnderlyingCursor:Lcom/android/mail/browse/ConversationCursor$UnderlyingCursorWrapper;

    return-void
.end method

.method public discardDrafts(Landroid/content/Context;Ljava/util/Collection;)I
    .locals 1
    .param p1    # Landroid/content/Context;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Ljava/util/Collection",
            "<",
            "Lcom/android/mail/providers/Conversation;",
            ">;)I"
        }
    .end annotation

    const/16 v0, 0x8

    invoke-direct {p0, p1, p2, v0}, Lcom/android/mail/browse/ConversationCursor;->applyAction(Landroid/content/Context;Ljava/util/Collection;I)I

    move-result v0

    return v0
.end method

.method public getBlob(I)[B
    .locals 2
    .param p1    # I

    invoke-direct {p0, p1}, Lcom/android/mail/browse/ConversationCursor;->getCachedValue(I)Ljava/lang/Object;

    move-result-object v0

    if-eqz v0, :cond_0

    check-cast v0, [B

    check-cast v0, [B

    :goto_0
    return-object v0

    :cond_0
    iget-object v1, p0, Lcom/android/mail/browse/ConversationCursor;->mUnderlyingCursor:Lcom/android/mail/browse/ConversationCursor$UnderlyingCursorWrapper;

    invoke-virtual {v1, p1}, Lcom/android/mail/browse/ConversationCursor$UnderlyingCursorWrapper;->getBlob(I)[B

    move-result-object v0

    goto :goto_0
.end method

.method public getColumnCount()I
    .locals 1

    iget-object v0, p0, Lcom/android/mail/browse/ConversationCursor;->mUnderlyingCursor:Lcom/android/mail/browse/ConversationCursor$UnderlyingCursorWrapper;

    invoke-virtual {v0}, Lcom/android/mail/browse/ConversationCursor$UnderlyingCursorWrapper;->getColumnCount()I

    move-result v0

    return v0
.end method

.method public getColumnIndex(Ljava/lang/String;)I
    .locals 1
    .param p1    # Ljava/lang/String;

    iget-object v0, p0, Lcom/android/mail/browse/ConversationCursor;->mUnderlyingCursor:Lcom/android/mail/browse/ConversationCursor$UnderlyingCursorWrapper;

    invoke-virtual {v0, p1}, Lcom/android/mail/browse/ConversationCursor$UnderlyingCursorWrapper;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    return v0
.end method

.method public getColumnIndexOrThrow(Ljava/lang/String;)I
    .locals 1
    .param p1    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/IllegalArgumentException;
        }
    .end annotation

    iget-object v0, p0, Lcom/android/mail/browse/ConversationCursor;->mUnderlyingCursor:Lcom/android/mail/browse/ConversationCursor$UnderlyingCursorWrapper;

    invoke-virtual {v0, p1}, Lcom/android/mail/browse/ConversationCursor$UnderlyingCursorWrapper;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v0

    return v0
.end method

.method public getColumnName(I)Ljava/lang/String;
    .locals 1
    .param p1    # I

    iget-object v0, p0, Lcom/android/mail/browse/ConversationCursor;->mUnderlyingCursor:Lcom/android/mail/browse/ConversationCursor$UnderlyingCursorWrapper;

    invoke-virtual {v0, p1}, Lcom/android/mail/browse/ConversationCursor$UnderlyingCursorWrapper;->getColumnName(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getColumnNames()[Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/android/mail/browse/ConversationCursor;->mUnderlyingCursor:Lcom/android/mail/browse/ConversationCursor$UnderlyingCursorWrapper;

    invoke-virtual {v0}, Lcom/android/mail/browse/ConversationCursor$UnderlyingCursorWrapper;->getColumnNames()[Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getConversationFolderOperation(Lcom/android/mail/providers/Conversation;Ljava/util/ArrayList;Ljava/util/ArrayList;Ljava/util/Collection;)Lcom/android/mail/browse/ConversationCursor$ConversationOperation;
    .locals 6
    .param p1    # Lcom/android/mail/providers/Conversation;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/android/mail/providers/Conversation;",
            "Ljava/util/ArrayList",
            "<",
            "Landroid/net/Uri;",
            ">;",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/Boolean;",
            ">;",
            "Ljava/util/Collection",
            "<",
            "Lcom/android/mail/providers/Folder;",
            ">;)",
            "Lcom/android/mail/browse/ConversationCursor$ConversationOperation;"
        }
    .end annotation

    new-instance v5, Landroid/content/ContentValues;

    invoke-direct {v5}, Landroid/content/ContentValues;-><init>()V

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    invoke-virtual/range {v0 .. v5}, Lcom/android/mail/browse/ConversationCursor;->getConversationFolderOperation(Lcom/android/mail/providers/Conversation;Ljava/util/ArrayList;Ljava/util/ArrayList;Ljava/util/Collection;Landroid/content/ContentValues;)Lcom/android/mail/browse/ConversationCursor$ConversationOperation;

    move-result-object v0

    return-object v0
.end method

.method public getConversationFolderOperation(Lcom/android/mail/providers/Conversation;Ljava/util/ArrayList;Ljava/util/ArrayList;Ljava/util/Collection;Landroid/content/ContentValues;)Lcom/android/mail/browse/ConversationCursor$ConversationOperation;
    .locals 1
    .param p1    # Lcom/android/mail/providers/Conversation;
    .param p5    # Landroid/content/ContentValues;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/android/mail/providers/Conversation;",
            "Ljava/util/ArrayList",
            "<",
            "Landroid/net/Uri;",
            ">;",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/Boolean;",
            ">;",
            "Ljava/util/Collection",
            "<",
            "Lcom/android/mail/providers/Folder;",
            ">;",
            "Landroid/content/ContentValues;",
            ")",
            "Lcom/android/mail/browse/ConversationCursor$ConversationOperation;"
        }
    .end annotation

    invoke-virtual {p0, p2, p3, p5}, Lcom/android/mail/browse/ConversationCursor;->addFolderUpdates(Ljava/util/ArrayList;Ljava/util/ArrayList;Landroid/content/ContentValues;)V

    invoke-virtual {p0, p4, p5}, Lcom/android/mail/browse/ConversationCursor;->addTargetFolders(Ljava/util/Collection;Landroid/content/ContentValues;)V

    const/4 v0, 0x2

    invoke-virtual {p0, p1, v0, p5}, Lcom/android/mail/browse/ConversationCursor;->getOperationForConversation(Lcom/android/mail/providers/Conversation;ILandroid/content/ContentValues;)Lcom/android/mail/browse/ConversationCursor$ConversationOperation;

    move-result-object v0

    return-object v0
.end method

.method public getConversationIds()Ljava/util/Set;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Set",
            "<",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/android/mail/browse/ConversationCursor;->mUnderlyingCursor:Lcom/android/mail/browse/ConversationCursor$UnderlyingCursorWrapper;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/mail/browse/ConversationCursor;->mUnderlyingCursor:Lcom/android/mail/browse/ConversationCursor$UnderlyingCursorWrapper;

    invoke-virtual {v0}, Lcom/android/mail/browse/ConversationCursor$UnderlyingCursorWrapper;->conversationIds()Ljava/util/Set;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getConversationPosition(J)I
    .locals 9
    .param p1    # J

    iget-object v7, p0, Lcom/android/mail/browse/ConversationCursor;->mUnderlyingCursor:Lcom/android/mail/browse/ConversationCursor$UnderlyingCursorWrapper;

    invoke-virtual {v7, p1, p2}, Lcom/android/mail/browse/ConversationCursor$UnderlyingCursorWrapper;->getPosition(J)I

    move-result v4

    if-gez v4, :cond_0

    :goto_0
    return v4

    :cond_0
    iget-object v8, p0, Lcom/android/mail/browse/ConversationCursor;->mCacheMapLock:Ljava/lang/Object;

    monitor-enter v8

    move v5, v4

    :try_start_0
    iget-object v7, p0, Lcom/android/mail/browse/ConversationCursor;->mCacheMap:Ljava/util/HashMap;

    invoke-virtual {v7}, Ljava/util/HashMap;->entrySet()Ljava/util/Set;

    move-result-object v7

    invoke-interface {v7}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_1
    :goto_1
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v7

    if-eqz v7, :cond_3

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/util/Map$Entry;

    invoke-interface {v2}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Landroid/content/ContentValues;

    const-string v7, "__deleted__"

    invoke-virtual {v6, v7}, Landroid/content/ContentValues;->containsKey(Ljava/lang/String;)Z

    move-result v7

    if-eqz v7, :cond_1

    invoke-interface {v2}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iget-object v7, p0, Lcom/android/mail/browse/ConversationCursor;->mUnderlyingCursor:Lcom/android/mail/browse/ConversationCursor$UnderlyingCursorWrapper;

    invoke-virtual {v7, v0}, Lcom/android/mail/browse/ConversationCursor$UnderlyingCursorWrapper;->getPosition(Ljava/lang/String;)I

    move-result v1

    if-ne v1, v4, :cond_2

    const/4 v4, -0x1

    monitor-exit v8

    goto :goto_0

    :catchall_0
    move-exception v7

    monitor-exit v8
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v7

    :cond_2
    if-ltz v1, :cond_1

    if-ge v1, v4, :cond_1

    add-int/lit8 v5, v5, -0x1

    goto :goto_1

    :cond_3
    :try_start_1
    monitor-exit v8
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move v4, v5

    goto :goto_0
.end method

.method public getCount()I
    .locals 3

    iget-object v0, p0, Lcom/android/mail/browse/ConversationCursor;->mUnderlyingCursor:Lcom/android/mail/browse/ConversationCursor$UnderlyingCursorWrapper;

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/IllegalStateException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "getCount() on disabled cursor: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/android/mail/browse/ConversationCursor;->mName:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "("

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/android/mail/browse/ConversationCursor;->qUri:Landroid/net/Uri;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ")"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    iget-object v0, p0, Lcom/android/mail/browse/ConversationCursor;->mUnderlyingCursor:Lcom/android/mail/browse/ConversationCursor$UnderlyingCursorWrapper;

    invoke-virtual {v0}, Lcom/android/mail/browse/ConversationCursor$UnderlyingCursorWrapper;->getCount()I

    move-result v0

    iget v1, p0, Lcom/android/mail/browse/ConversationCursor;->mDeletedCount:I

    sub-int/2addr v0, v1

    return v0
.end method

.method public getDeletedItems()Ljava/util/Set;
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    iget-object v6, p0, Lcom/android/mail/browse/ConversationCursor;->mCacheMapLock:Ljava/lang/Object;

    monitor-enter v6

    :try_start_0
    invoke-static {}, Lcom/google/common/collect/Sets;->newHashSet()Ljava/util/HashSet;

    move-result-object v1

    iget-object v5, p0, Lcom/android/mail/browse/ConversationCursor;->mCacheMap:Ljava/util/HashMap;

    invoke-virtual {v5}, Ljava/util/HashMap;->entrySet()Ljava/util/Set;

    move-result-object v5

    invoke-interface {v5}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_0
    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_1

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/util/Map$Entry;

    invoke-interface {v2}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Landroid/content/ContentValues;

    const-string v5, "__deleted__"

    invoke-virtual {v4, v5}, Landroid/content/ContentValues;->containsKey(Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_0

    invoke-interface {v2}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/lang/String;

    invoke-static {v5}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    invoke-static {v0}, Lcom/android/mail/browse/ConversationCursor;->uriToCachingUriString(Landroid/net/Uri;)Ljava/lang/String;

    move-result-object v5

    invoke-interface {v1, v5}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :catchall_0
    move-exception v5

    monitor-exit v6
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v5

    :cond_1
    :try_start_1
    monitor-exit v6
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    return-object v1
.end method

.method public getDouble(I)D
    .locals 3
    .param p1    # I

    invoke-direct {p0, p1}, Lcom/android/mail/browse/ConversationCursor;->getCachedValue(I)Ljava/lang/Object;

    move-result-object v0

    if-eqz v0, :cond_0

    check-cast v0, Ljava/lang/Double;

    invoke-virtual {v0}, Ljava/lang/Double;->doubleValue()D

    move-result-wide v1

    :goto_0
    return-wide v1

    :cond_0
    iget-object v1, p0, Lcom/android/mail/browse/ConversationCursor;->mUnderlyingCursor:Lcom/android/mail/browse/ConversationCursor$UnderlyingCursorWrapper;

    invoke-virtual {v1, p1}, Lcom/android/mail/browse/ConversationCursor$UnderlyingCursorWrapper;->getDouble(I)D

    move-result-wide v1

    goto :goto_0
.end method

.method public getExtras()Landroid/os/Bundle;
    .locals 1

    iget-object v0, p0, Lcom/android/mail/browse/ConversationCursor;->mUnderlyingCursor:Lcom/android/mail/browse/ConversationCursor$UnderlyingCursorWrapper;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/mail/browse/ConversationCursor;->mUnderlyingCursor:Lcom/android/mail/browse/ConversationCursor$UnderlyingCursorWrapper;

    invoke-virtual {v0}, Lcom/android/mail/browse/ConversationCursor$UnderlyingCursorWrapper;->getExtras()Landroid/os/Bundle;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    sget-object v0, Landroid/os/Bundle;->EMPTY:Landroid/os/Bundle;

    goto :goto_0
.end method

.method public getFloat(I)F
    .locals 2
    .param p1    # I

    invoke-direct {p0, p1}, Lcom/android/mail/browse/ConversationCursor;->getCachedValue(I)Ljava/lang/Object;

    move-result-object v0

    if-eqz v0, :cond_0

    check-cast v0, Ljava/lang/Float;

    invoke-virtual {v0}, Ljava/lang/Float;->floatValue()F

    move-result v1

    :goto_0
    return v1

    :cond_0
    iget-object v1, p0, Lcom/android/mail/browse/ConversationCursor;->mUnderlyingCursor:Lcom/android/mail/browse/ConversationCursor$UnderlyingCursorWrapper;

    invoke-virtual {v1, p1}, Lcom/android/mail/browse/ConversationCursor$UnderlyingCursorWrapper;->getFloat(I)F

    move-result v1

    goto :goto_0
.end method

.method public getInt(I)I
    .locals 2
    .param p1    # I

    invoke-direct {p0, p1}, Lcom/android/mail/browse/ConversationCursor;->getCachedValue(I)Ljava/lang/Object;

    move-result-object v0

    if-eqz v0, :cond_0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v1

    :goto_0
    return v1

    :cond_0
    iget-object v1, p0, Lcom/android/mail/browse/ConversationCursor;->mUnderlyingCursor:Lcom/android/mail/browse/ConversationCursor$UnderlyingCursorWrapper;

    invoke-virtual {v1, p1}, Lcom/android/mail/browse/ConversationCursor$UnderlyingCursorWrapper;->getInt(I)I

    move-result v1

    goto :goto_0
.end method

.method public getLong(I)J
    .locals 3
    .param p1    # I

    invoke-direct {p0, p1}, Lcom/android/mail/browse/ConversationCursor;->getCachedValue(I)Ljava/lang/Object;

    move-result-object v0

    if-eqz v0, :cond_0

    check-cast v0, Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v1

    :goto_0
    return-wide v1

    :cond_0
    iget-object v1, p0, Lcom/android/mail/browse/ConversationCursor;->mUnderlyingCursor:Lcom/android/mail/browse/ConversationCursor$UnderlyingCursorWrapper;

    invoke-virtual {v1, p1}, Lcom/android/mail/browse/ConversationCursor$UnderlyingCursorWrapper;->getLong(I)J

    move-result-wide v1

    goto :goto_0
.end method

.method public getOperationForConversation(Lcom/android/mail/providers/Conversation;ILandroid/content/ContentValues;)Lcom/android/mail/browse/ConversationCursor$ConversationOperation;
    .locals 1
    .param p1    # Lcom/android/mail/providers/Conversation;
    .param p2    # I
    .param p3    # Landroid/content/ContentValues;

    new-instance v0, Lcom/android/mail/browse/ConversationCursor$ConversationOperation;

    invoke-direct {v0, p0, p2, p1, p3}, Lcom/android/mail/browse/ConversationCursor$ConversationOperation;-><init>(Lcom/android/mail/browse/ConversationCursor;ILcom/android/mail/providers/Conversation;Landroid/content/ContentValues;)V

    return-object v0
.end method

.method public getPosition()I
    .locals 1

    iget v0, p0, Lcom/android/mail/browse/ConversationCursor;->mPosition:I

    return v0
.end method

.method public getShort(I)S
    .locals 2
    .param p1    # I

    invoke-direct {p0, p1}, Lcom/android/mail/browse/ConversationCursor;->getCachedValue(I)Ljava/lang/Object;

    move-result-object v0

    if-eqz v0, :cond_0

    check-cast v0, Ljava/lang/Short;

    invoke-virtual {v0}, Ljava/lang/Short;->shortValue()S

    move-result v1

    :goto_0
    return v1

    :cond_0
    iget-object v1, p0, Lcom/android/mail/browse/ConversationCursor;->mUnderlyingCursor:Lcom/android/mail/browse/ConversationCursor$UnderlyingCursorWrapper;

    invoke-virtual {v1, p1}, Lcom/android/mail/browse/ConversationCursor$UnderlyingCursorWrapper;->getShort(I)S

    move-result v1

    goto :goto_0
.end method

.method public getString(I)Ljava/lang/String;
    .locals 3
    .param p1    # I

    const/4 v2, 0x1

    if-ne p1, v2, :cond_0

    iget-object v2, p0, Lcom/android/mail/browse/ConversationCursor;->mUnderlyingCursor:Lcom/android/mail/browse/ConversationCursor$UnderlyingCursorWrapper;

    invoke-virtual {v2, p1}, Lcom/android/mail/browse/ConversationCursor$UnderlyingCursorWrapper;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    invoke-static {v1}, Lcom/android/mail/browse/ConversationCursor;->uriToCachingUriString(Landroid/net/Uri;)Ljava/lang/String;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    invoke-direct {p0, p1}, Lcom/android/mail/browse/ConversationCursor;->getCachedValue(I)Ljava/lang/Object;

    move-result-object v0

    if-eqz v0, :cond_1

    check-cast v0, Ljava/lang/String;

    goto :goto_0

    :cond_1
    iget-object v2, p0, Lcom/android/mail/browse/ConversationCursor;->mUnderlyingCursor:Lcom/android/mail/browse/ConversationCursor$UnderlyingCursorWrapper;

    invoke-virtual {v2, p1}, Lcom/android/mail/browse/ConversationCursor$UnderlyingCursorWrapper;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public getType(I)I
    .locals 1
    .param p1    # I

    iget-object v0, p0, Lcom/android/mail/browse/ConversationCursor;->mUnderlyingCursor:Lcom/android/mail/browse/ConversationCursor$UnderlyingCursorWrapper;

    invoke-virtual {v0, p1}, Lcom/android/mail/browse/ConversationCursor$UnderlyingCursorWrapper;->getType(I)I

    move-result v0

    return v0
.end method

.method public getWantsAllOnMoveCalls()Z
    .locals 1

    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public handleNotificationActions()V
    .locals 11

    sget-object v8, Lcom/android/mail/utils/NotificationActionUtils;->sUndoNotifications:Lcom/android/mail/utils/ObservableSparseArrayCompat;

    invoke-virtual {v8}, Lvedroid/support/v4/util/SparseArrayCompat;->size()I

    move-result v9

    invoke-static {v9}, Lcom/google/common/collect/Sets;->newHashSetWithExpectedSize(I)Ljava/util/HashSet;

    move-result-object v7

    const/4 v0, 0x0

    const/4 v4, 0x0

    :goto_0
    invoke-virtual {v8}, Lvedroid/support/v4/util/SparseArrayCompat;->size()I

    move-result v9

    if-ge v4, v9, :cond_3

    invoke-virtual {v8, v4}, Lvedroid/support/v4/util/SparseArrayCompat;->keyAt(I)I

    move-result v9

    invoke-virtual {v8, v9}, Lvedroid/support/v4/util/SparseArrayCompat;->get(I)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lcom/android/mail/utils/NotificationActionUtils$NotificationAction;

    invoke-virtual {v6}, Lcom/android/mail/utils/NotificationActionUtils$NotificationAction;->getFolder()Lcom/android/mail/providers/Folder;

    move-result-object v3

    invoke-virtual {v6}, Lcom/android/mail/utils/NotificationActionUtils$NotificationAction;->getNotificationActionType()Lcom/android/mail/utils/NotificationActionUtils$NotificationActionType;

    move-result-object v9

    sget-object v10, Lcom/android/mail/utils/NotificationActionUtils$NotificationActionType;->DELETE:Lcom/android/mail/utils/NotificationActionUtils$NotificationActionType;

    if-ne v9, v10, :cond_2

    const/4 v2, 0x1

    :goto_1
    iget-object v9, v3, Lcom/android/mail/providers/Folder;->conversationListUri:Landroid/net/Uri;

    iget-object v10, p0, Lcom/android/mail/browse/ConversationCursor;->qUri:Landroid/net/Uri;

    invoke-virtual {v9, v10}, Landroid/net/Uri;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-nez v9, :cond_0

    if-eqz v2, :cond_1

    :cond_0
    invoke-virtual {v6}, Lcom/android/mail/utils/NotificationActionUtils$NotificationAction;->getNotificationActionType()Lcom/android/mail/utils/NotificationActionUtils$NotificationActionType;

    move-result-object v9

    invoke-virtual {v9}, Lcom/android/mail/utils/NotificationActionUtils$NotificationActionType;->getIsDestructive()Z

    move-result v9

    if-eqz v9, :cond_1

    invoke-virtual {v6}, Lcom/android/mail/utils/NotificationActionUtils$NotificationAction;->getConversation()Lcom/android/mail/providers/Conversation;

    move-result-object v1

    invoke-interface {v7, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    iget-object v9, p0, Lcom/android/mail/browse/ConversationCursor;->mNotificationTempDeleted:Ljava/util/Set;

    invoke-interface {v9, v1}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v9

    if-nez v9, :cond_1

    sget-object v9, Lcom/android/mail/browse/ConversationCursor;->sProvider:Lcom/android/mail/browse/ConversationCursor$ConversationProvider;

    iget-object v10, v1, Lcom/android/mail/providers/Conversation;->uri:Landroid/net/Uri;

    invoke-virtual {v9, v10, p0}, Lcom/android/mail/browse/ConversationCursor$ConversationProvider;->deleteLocal(Landroid/net/Uri;Lcom/android/mail/browse/ConversationCursor;)V

    iget-object v9, p0, Lcom/android/mail/browse/ConversationCursor;->mNotificationTempDeleted:Ljava/util/Set;

    invoke-interface {v9, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    const/4 v0, 0x1

    :cond_1
    add-int/lit8 v4, v4, 0x1

    goto :goto_0

    :cond_2
    const/4 v2, 0x0

    goto :goto_1

    :cond_3
    iget-object v9, p0, Lcom/android/mail/browse/ConversationCursor;->mNotificationTempDeleted:Ljava/util/Set;

    invoke-interface {v9}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v5

    :cond_4
    :goto_2
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v9

    if-eqz v9, :cond_5

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/android/mail/providers/Conversation;

    invoke-interface {v7, v1}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v9

    if-nez v9, :cond_4

    sget-object v9, Lcom/android/mail/browse/ConversationCursor;->sProvider:Lcom/android/mail/browse/ConversationCursor$ConversationProvider;

    iget-object v10, v1, Lcom/android/mail/providers/Conversation;->uri:Landroid/net/Uri;

    invoke-virtual {v9, v10, p0}, Lcom/android/mail/browse/ConversationCursor$ConversationProvider;->undeleteLocal(Landroid/net/Uri;Lcom/android/mail/browse/ConversationCursor;)V

    invoke-interface {v5}, Ljava/util/Iterator;->remove()V

    const/4 v0, 0x1

    goto :goto_2

    :cond_5
    if-eqz v0, :cond_6

    iget-object v9, p0, Lcom/android/mail/browse/ConversationCursor;->mMainThreadHandler:Landroid/os/Handler;

    new-instance v10, Lcom/android/mail/browse/ConversationCursor$3;

    invoke-direct {v10, p0}, Lcom/android/mail/browse/ConversationCursor$3;-><init>(Lcom/android/mail/browse/ConversationCursor;)V

    invoke-virtual {v9, v10}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    :cond_6
    return-void
.end method

.method public hashCode()I
    .locals 1

    invoke-super {p0}, Ljava/lang/Object;->hashCode()I

    move-result v0

    return v0
.end method

.method public isAfterLast()Z
    .locals 1

    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public isBeforeFirst()Z
    .locals 1

    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public isClosed()Z
    .locals 1

    iget-object v0, p0, Lcom/android/mail/browse/ConversationCursor;->mUnderlyingCursor:Lcom/android/mail/browse/ConversationCursor$UnderlyingCursorWrapper;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/mail/browse/ConversationCursor;->mUnderlyingCursor:Lcom/android/mail/browse/ConversationCursor$UnderlyingCursorWrapper;

    invoke-virtual {v0}, Lcom/android/mail/browse/ConversationCursor$UnderlyingCursorWrapper;->isClosed()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isFirst()Z
    .locals 1

    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public isLast()Z
    .locals 1

    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public isNull(I)Z
    .locals 1
    .param p1    # I

    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public isRefreshReady()Z
    .locals 1

    iget-boolean v0, p0, Lcom/android/mail/browse/ConversationCursor;->mRefreshReady:Z

    return v0
.end method

.method public isRefreshRequired()Z
    .locals 1

    iget-boolean v0, p0, Lcom/android/mail/browse/ConversationCursor;->mRefreshRequired:Z

    return v0
.end method

.method public load()V
    .locals 4

    iget-object v1, p0, Lcom/android/mail/browse/ConversationCursor;->mCacheMapLock:Ljava/lang/Object;

    monitor-enter v1

    :try_start_0
    sget-object v0, Lcom/android/mail/browse/ConversationCursor;->LOG_TAG:Ljava/lang/String;

    const-string v2, "Create: initial creation"

    const/4 v3, 0x0

    new-array v3, v3, [Ljava/lang/Object;

    invoke-static {v0, v2, v3}, Lcom/android/mail/utils/LogUtils;->d(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)I

    iget-boolean v0, p0, Lcom/android/mail/browse/ConversationCursor;->mInitialConversationLimit:Z

    invoke-direct {p0, v0}, Lcom/android/mail/browse/ConversationCursor;->doQuery(Z)Lcom/android/mail/browse/ConversationCursor$UnderlyingCursorWrapper;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/android/mail/browse/ConversationCursor;->setCursor(Lcom/android/mail/browse/ConversationCursor$UnderlyingCursorWrapper;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :try_start_1
    iget-boolean v0, p0, Lcom/android/mail/browse/ConversationCursor;->mInitialConversationLimit:Z

    if-eqz v0, :cond_0

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/android/mail/browse/ConversationCursor;->mInitialConversationLimit:Z

    invoke-virtual {p0}, Lcom/android/mail/browse/ConversationCursor;->refresh()Z

    :cond_0
    monitor-exit v1

    return-void

    :catchall_0
    move-exception v0

    iget-boolean v2, p0, Lcom/android/mail/browse/ConversationCursor;->mInitialConversationLimit:Z

    if-eqz v2, :cond_1

    const/4 v2, 0x0

    iput-boolean v2, p0, Lcom/android/mail/browse/ConversationCursor;->mInitialConversationLimit:Z

    invoke-virtual {p0}, Lcom/android/mail/browse/ConversationCursor;->refresh()Z

    :cond_1
    throw v0

    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    throw v0
.end method

.method public mostlyArchive(Landroid/content/Context;Ljava/util/Collection;)I
    .locals 1
    .param p1    # Landroid/content/Context;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Ljava/util/Collection",
            "<",
            "Lcom/android/mail/providers/Conversation;",
            ">;)I"
        }
    .end annotation

    const/16 v0, 0x83

    invoke-direct {p0, p1, p2, v0}, Lcom/android/mail/browse/ConversationCursor;->applyAction(Landroid/content/Context;Ljava/util/Collection;I)I

    move-result v0

    return v0
.end method

.method public mostlyDelete(Landroid/content/Context;Ljava/util/Collection;)I
    .locals 1
    .param p1    # Landroid/content/Context;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Ljava/util/Collection",
            "<",
            "Lcom/android/mail/providers/Conversation;",
            ">;)I"
        }
    .end annotation

    const/16 v0, 0x80

    invoke-direct {p0, p1, p2, v0}, Lcom/android/mail/browse/ConversationCursor;->applyAction(Landroid/content/Context;Ljava/util/Collection;I)I

    move-result v0

    return v0
.end method

.method public mostlyDestructiveUpdate(Landroid/content/Context;Ljava/util/Collection;Landroid/content/ContentValues;)I
    .locals 1
    .param p1    # Landroid/content/Context;
    .param p3    # Landroid/content/ContentValues;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Ljava/util/Collection",
            "<",
            "Lcom/android/mail/providers/Conversation;",
            ">;",
            "Landroid/content/ContentValues;",
            ")I"
        }
    .end annotation

    const/16 v0, 0x82

    invoke-direct {p0, p2, v0, p3}, Lcom/android/mail/browse/ConversationCursor;->getOperationsForConversations(Ljava/util/Collection;ILandroid/content/ContentValues;)Ljava/util/ArrayList;

    move-result-object v0

    invoke-direct {p0, p1, v0}, Lcom/android/mail/browse/ConversationCursor;->apply(Landroid/content/Context;Ljava/util/Collection;)I

    move-result v0

    return v0
.end method

.method public move(I)Z
    .locals 2
    .param p1    # I

    new-instance v0, Ljava/lang/UnsupportedOperationException;

    const-string v1, "move unsupported!"

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public moveToFirst()Z
    .locals 3

    const/4 v1, -0x1

    iget-object v0, p0, Lcom/android/mail/browse/ConversationCursor;->mUnderlyingCursor:Lcom/android/mail/browse/ConversationCursor$UnderlyingCursorWrapper;

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/IllegalStateException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "moveToFirst() on disabled cursor: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/android/mail/browse/ConversationCursor;->mName:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "("

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/android/mail/browse/ConversationCursor;->qUri:Landroid/net/Uri;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ")"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    iget-object v0, p0, Lcom/android/mail/browse/ConversationCursor;->mUnderlyingCursor:Lcom/android/mail/browse/ConversationCursor$UnderlyingCursorWrapper;

    invoke-virtual {v0, v1}, Lcom/android/mail/browse/ConversationCursor$UnderlyingCursorWrapper;->moveToPosition(I)Z

    iput v1, p0, Lcom/android/mail/browse/ConversationCursor;->mPosition:I

    invoke-virtual {p0}, Lcom/android/mail/browse/ConversationCursor;->moveToNext()Z

    move-result v0

    return v0
.end method

.method public moveToLast()Z
    .locals 2

    new-instance v0, Ljava/lang/UnsupportedOperationException;

    const-string v1, "moveToLast unsupported!"

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public moveToNext()Z
    .locals 2

    :cond_0
    iget-object v1, p0, Lcom/android/mail/browse/ConversationCursor;->mUnderlyingCursor:Lcom/android/mail/browse/ConversationCursor$UnderlyingCursorWrapper;

    invoke-virtual {v1}, Lcom/android/mail/browse/ConversationCursor$UnderlyingCursorWrapper;->moveToNext()Z

    move-result v0

    if-nez v0, :cond_1

    invoke-virtual {p0}, Lcom/android/mail/browse/ConversationCursor;->getCount()I

    move-result v1

    iput v1, p0, Lcom/android/mail/browse/ConversationCursor;->mPosition:I

    const/4 v1, 0x0

    :goto_0
    return v1

    :cond_1
    const/4 v1, -0x1

    invoke-direct {p0, v1}, Lcom/android/mail/browse/ConversationCursor;->getCachedValue(I)Ljava/lang/Object;

    move-result-object v1

    instance-of v1, v1, Ljava/lang/Integer;

    if-nez v1, :cond_0

    iget v1, p0, Lcom/android/mail/browse/ConversationCursor;->mPosition:I

    add-int/lit8 v1, v1, 0x1

    iput v1, p0, Lcom/android/mail/browse/ConversationCursor;->mPosition:I

    const/4 v1, 0x1

    goto :goto_0
.end method

.method public moveToPosition(I)Z
    .locals 7
    .param p1    # I

    const/4 v6, -0x1

    const/4 v0, 0x1

    const/4 v1, 0x0

    iget-object v2, p0, Lcom/android/mail/browse/ConversationCursor;->mUnderlyingCursor:Lcom/android/mail/browse/ConversationCursor$UnderlyingCursorWrapper;

    if-nez v2, :cond_0

    new-instance v0, Ljava/lang/IllegalStateException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "moveToPosition() on disabled cursor: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/android/mail/browse/ConversationCursor;->mName:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "("

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/android/mail/browse/ConversationCursor;->qUri:Landroid/net/Uri;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ")"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    iget-object v2, p0, Lcom/android/mail/browse/ConversationCursor;->mUnderlyingCursor:Lcom/android/mail/browse/ConversationCursor$UnderlyingCursorWrapper;

    invoke-virtual {v2}, Lcom/android/mail/browse/ConversationCursor$UnderlyingCursorWrapper;->getPosition()I

    move-result v2

    if-ne v2, v6, :cond_1

    sget-object v2, Lcom/android/mail/browse/ConversationCursor;->LOG_TAG:Ljava/lang/String;

    const-string v3, "*** Underlying cursor position is -1 asking to move from %d to %d"

    const/4 v4, 0x2

    new-array v4, v4, [Ljava/lang/Object;

    iget v5, p0, Lcom/android/mail/browse/ConversationCursor;->mPosition:I

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v4, v1

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v4, v0

    invoke-static {v2, v3, v4}, Lcom/android/mail/utils/LogUtils;->d(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)I

    :cond_1
    if-nez p1, :cond_2

    invoke-virtual {p0}, Lcom/android/mail/browse/ConversationCursor;->moveToFirst()Z

    move-result v1

    :goto_0
    return v1

    :cond_2
    if-gez p1, :cond_3

    iput v6, p0, Lcom/android/mail/browse/ConversationCursor;->mPosition:I

    iget-object v0, p0, Lcom/android/mail/browse/ConversationCursor;->mUnderlyingCursor:Lcom/android/mail/browse/ConversationCursor$UnderlyingCursorWrapper;

    iget v2, p0, Lcom/android/mail/browse/ConversationCursor;->mPosition:I

    invoke-virtual {v0, v2}, Lcom/android/mail/browse/ConversationCursor$UnderlyingCursorWrapper;->moveToPosition(I)Z

    goto :goto_0

    :cond_3
    iget v2, p0, Lcom/android/mail/browse/ConversationCursor;->mPosition:I

    if-ne p1, v2, :cond_5

    invoke-virtual {p0}, Lcom/android/mail/browse/ConversationCursor;->getCount()I

    move-result v2

    if-ge p1, v2, :cond_4

    :goto_1
    move v1, v0

    goto :goto_0

    :cond_4
    move v0, v1

    goto :goto_1

    :cond_5
    iget v2, p0, Lcom/android/mail/browse/ConversationCursor;->mPosition:I

    if-le p1, v2, :cond_8

    :cond_6
    iget v2, p0, Lcom/android/mail/browse/ConversationCursor;->mPosition:I

    if-le p1, v2, :cond_7

    invoke-virtual {p0}, Lcom/android/mail/browse/ConversationCursor;->moveToNext()Z

    move-result v2

    if-nez v2, :cond_6

    goto :goto_0

    :cond_7
    move v1, v0

    goto :goto_0

    :cond_8
    if-ltz p1, :cond_9

    iget v2, p0, Lcom/android/mail/browse/ConversationCursor;->mPosition:I

    sub-int/2addr v2, p1

    if-le v2, p1, :cond_9

    invoke-virtual {p0}, Lcom/android/mail/browse/ConversationCursor;->moveToFirst()Z

    invoke-virtual {p0, p1}, Lcom/android/mail/browse/ConversationCursor;->moveToPosition(I)Z

    move-result v1

    goto :goto_0

    :cond_9
    iget v2, p0, Lcom/android/mail/browse/ConversationCursor;->mPosition:I

    if-ge p1, v2, :cond_a

    invoke-virtual {p0}, Lcom/android/mail/browse/ConversationCursor;->moveToPrevious()Z

    move-result v2

    if-nez v2, :cond_9

    goto :goto_0

    :cond_a
    move v1, v0

    goto :goto_0
.end method

.method public moveToPrevious()Z
    .locals 3

    const/4 v2, -0x1

    :cond_0
    iget-object v1, p0, Lcom/android/mail/browse/ConversationCursor;->mUnderlyingCursor:Lcom/android/mail/browse/ConversationCursor$UnderlyingCursorWrapper;

    invoke-virtual {v1}, Lcom/android/mail/browse/ConversationCursor$UnderlyingCursorWrapper;->moveToPrevious()Z

    move-result v0

    if-nez v0, :cond_1

    iput v2, p0, Lcom/android/mail/browse/ConversationCursor;->mPosition:I

    const/4 v1, 0x0

    :goto_0
    return v1

    :cond_1
    invoke-direct {p0, v2}, Lcom/android/mail/browse/ConversationCursor;->getCachedValue(I)Ljava/lang/Object;

    move-result-object v1

    instance-of v1, v1, Ljava/lang/Integer;

    if-nez v1, :cond_0

    iget v1, p0, Lcom/android/mail/browse/ConversationCursor;->mPosition:I

    add-int/lit8 v1, v1, -0x1

    iput v1, p0, Lcom/android/mail/browse/ConversationCursor;->mPosition:I

    const/4 v1, 0x1

    goto :goto_0
.end method

.method public mute(Landroid/content/Context;Ljava/util/Collection;)I
    .locals 1
    .param p1    # Landroid/content/Context;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Ljava/util/Collection",
            "<",
            "Lcom/android/mail/providers/Conversation;",
            ">;)I"
        }
    .end annotation

    const/4 v0, 0x4

    invoke-direct {p0, p1, p2, v0}, Lcom/android/mail/browse/ConversationCursor;->applyAction(Landroid/content/Context;Ljava/util/Collection;I)I

    move-result v0

    return v0
.end method

.method public pause()V
    .locals 1

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/android/mail/browse/ConversationCursor;->mPaused:Z

    return-void
.end method

.method public refresh()Z
    .locals 4

    const/4 v0, 0x0

    iget-object v1, p0, Lcom/android/mail/browse/ConversationCursor;->mCacheMapLock:Ljava/lang/Object;

    monitor-enter v1

    :try_start_0
    iget-object v2, p0, Lcom/android/mail/browse/ConversationCursor;->mRefreshTask:Lcom/android/mail/browse/ConversationCursor$RefreshTask;

    if-eqz v2, :cond_0

    monitor-exit v1

    :goto_0
    return v0

    :cond_0
    new-instance v0, Lcom/android/mail/browse/ConversationCursor$RefreshTask;

    const/4 v2, 0x0

    invoke-direct {v0, p0, v2}, Lcom/android/mail/browse/ConversationCursor$RefreshTask;-><init>(Lcom/android/mail/browse/ConversationCursor;Lcom/android/mail/browse/ConversationCursor$1;)V

    iput-object v0, p0, Lcom/android/mail/browse/ConversationCursor;->mRefreshTask:Lcom/android/mail/browse/ConversationCursor$RefreshTask;

    iget-object v0, p0, Lcom/android/mail/browse/ConversationCursor;->mRefreshTask:Lcom/android/mail/browse/ConversationCursor$RefreshTask;

    sget-object v2, Landroid/os/AsyncTask;->THREAD_POOL_EXECUTOR:Ljava/util/concurrent/Executor;

    const/4 v3, 0x0

    new-array v3, v3, [Ljava/lang/Void;

    invoke-virtual {v0, v2, v3}, Lcom/android/mail/browse/ConversationCursor$RefreshTask;->executeOnExecutor(Ljava/util/concurrent/Executor;[Ljava/lang/Object;)Landroid/os/AsyncTask;

    monitor-exit v1

    const/4 v0, 0x1

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public registerContentObserver(Landroid/database/ContentObserver;)V
    .locals 0
    .param p1    # Landroid/database/ContentObserver;

    return-void
.end method

.method public registerDataSetObserver(Landroid/database/DataSetObserver;)V
    .locals 0
    .param p1    # Landroid/database/DataSetObserver;

    return-void
.end method

.method public removeListener(Lcom/android/mail/browse/ConversationCursor$ConversationListener;)V
    .locals 2
    .param p1    # Lcom/android/mail/browse/ConversationCursor$ConversationListener;

    iget-object v1, p0, Lcom/android/mail/browse/ConversationCursor;->mListeners:Ljava/util/List;

    monitor-enter v1

    :try_start_0
    iget-object v0, p0, Lcom/android/mail/browse/ConversationCursor;->mListeners:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->remove(Ljava/lang/Object;)Z

    monitor-exit v1

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public reportNotSpam(Landroid/content/Context;Ljava/util/Collection;)I
    .locals 1
    .param p1    # Landroid/content/Context;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Ljava/util/Collection",
            "<",
            "Lcom/android/mail/providers/Conversation;",
            ">;)I"
        }
    .end annotation

    const/4 v0, 0x6

    invoke-direct {p0, p1, p2, v0}, Lcom/android/mail/browse/ConversationCursor;->applyAction(Landroid/content/Context;Ljava/util/Collection;I)I

    move-result v0

    return v0
.end method

.method public reportPhishing(Landroid/content/Context;Ljava/util/Collection;)I
    .locals 1
    .param p1    # Landroid/content/Context;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Ljava/util/Collection",
            "<",
            "Lcom/android/mail/providers/Conversation;",
            ">;)I"
        }
    .end annotation

    const/4 v0, 0x7

    invoke-direct {p0, p1, p2, v0}, Lcom/android/mail/browse/ConversationCursor;->applyAction(Landroid/content/Context;Ljava/util/Collection;I)I

    move-result v0

    return v0
.end method

.method public reportSpam(Landroid/content/Context;Ljava/util/Collection;)I
    .locals 1
    .param p1    # Landroid/content/Context;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Ljava/util/Collection",
            "<",
            "Lcom/android/mail/providers/Conversation;",
            ">;)I"
        }
    .end annotation

    const/4 v0, 0x5

    invoke-direct {p0, p1, p2, v0}, Lcom/android/mail/browse/ConversationCursor;->applyAction(Landroid/content/Context;Ljava/util/Collection;I)I

    move-result v0

    return v0
.end method

.method public requery()Z
    .locals 1

    const/4 v0, 0x1

    return v0
.end method

.method public respond(Landroid/os/Bundle;)Landroid/os/Bundle;
    .locals 1
    .param p1    # Landroid/os/Bundle;

    iget-object v0, p0, Lcom/android/mail/browse/ConversationCursor;->mUnderlyingCursor:Lcom/android/mail/browse/ConversationCursor$UnderlyingCursorWrapper;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/mail/browse/ConversationCursor;->mUnderlyingCursor:Lcom/android/mail/browse/ConversationCursor$UnderlyingCursorWrapper;

    invoke-virtual {v0, p1}, Lcom/android/mail/browse/ConversationCursor$UnderlyingCursorWrapper;->respond(Landroid/os/Bundle;)Landroid/os/Bundle;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    sget-object v0, Landroid/os/Bundle;->EMPTY:Landroid/os/Bundle;

    goto :goto_0
.end method

.method public resume()V
    .locals 1

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/android/mail/browse/ConversationCursor;->mPaused:Z

    invoke-direct {p0}, Lcom/android/mail/browse/ConversationCursor;->checkNotifyUI()V

    return-void
.end method

.method public setConversationColumn(Landroid/net/Uri;Ljava/lang/String;Ljava/lang/Object;)V
    .locals 3
    .param p1    # Landroid/net/Uri;
    .param p2    # Ljava/lang/String;
    .param p3    # Ljava/lang/Object;

    invoke-static {p1}, Lcom/android/mail/browse/ConversationCursor;->uriStringFromCachingUri(Landroid/net/Uri;)Ljava/lang/String;

    move-result-object v0

    iget-object v2, p0, Lcom/android/mail/browse/ConversationCursor;->mCacheMapLock:Ljava/lang/Object;

    monitor-enter v2

    :try_start_0
    invoke-direct {p0, v0, p2, p3}, Lcom/android/mail/browse/ConversationCursor;->cacheValue(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Object;)V

    monitor-exit v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    invoke-direct {p0}, Lcom/android/mail/browse/ConversationCursor;->notifyDataChanged()V

    return-void

    :catchall_0
    move-exception v1

    :try_start_1
    monitor-exit v2
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v1
.end method

.method setMostlyDead(Ljava/lang/String;Lcom/android/mail/providers/Conversation;)V
    .locals 5
    .param p1    # Ljava/lang/String;
    .param p2    # Lcom/android/mail/providers/Conversation;

    const/4 v4, 0x1

    sget-object v0, Lcom/android/mail/browse/ConversationCursor;->LOG_TAG:Ljava/lang/String;

    const-string v1, "[Mostly dead, deferring: %s] "

    new-array v2, v4, [Ljava/lang/Object;

    const/4 v3, 0x0

    aput-object p1, v2, v3

    invoke-static {v0, v1, v2}, Lcom/android/mail/utils/LogUtils;->d(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)I

    const-string v0, "conversationFlags"

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-direct {p0, p1, v0, v1}, Lcom/android/mail/browse/ConversationCursor;->cacheValue(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Object;)V

    iget v0, p2, Lcom/android/mail/providers/Conversation;->convFlags:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p2, Lcom/android/mail/providers/Conversation;->convFlags:I

    iget-object v0, p0, Lcom/android/mail/browse/ConversationCursor;->mMostlyDead:Ljava/util/List;

    invoke-interface {v0, p2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    iput-boolean v4, p0, Lcom/android/mail/browse/ConversationCursor;->mDeferSync:Z

    return-void
.end method

.method public setNotificationUri(Landroid/content/ContentResolver;Landroid/net/Uri;)V
    .locals 1
    .param p1    # Landroid/content/ContentResolver;
    .param p2    # Landroid/net/Uri;

    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public sync()V
    .locals 2

    iget-object v0, p0, Lcom/android/mail/browse/ConversationCursor;->mRequeryCursor:Lcom/android/mail/browse/ConversationCursor$UnderlyingCursorWrapper;

    if-nez v0, :cond_0

    :goto_0
    return-void

    :cond_0
    iget-object v1, p0, Lcom/android/mail/browse/ConversationCursor;->mCacheMapLock:Ljava/lang/Object;

    monitor-enter v1

    :try_start_0
    iget-object v0, p0, Lcom/android/mail/browse/ConversationCursor;->mRequeryCursor:Lcom/android/mail/browse/ConversationCursor$UnderlyingCursorWrapper;

    invoke-direct {p0, v0}, Lcom/android/mail/browse/ConversationCursor;->resetCursor(Lcom/android/mail/browse/ConversationCursor$UnderlyingCursorWrapper;)V

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/android/mail/browse/ConversationCursor;->mRequeryCursor:Lcom/android/mail/browse/ConversationCursor$UnderlyingCursorWrapper;

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/android/mail/browse/ConversationCursor;->mRefreshTask:Lcom/android/mail/browse/ConversationCursor$RefreshTask;

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/android/mail/browse/ConversationCursor;->mRefreshReady:Z

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    invoke-direct {p0}, Lcom/android/mail/browse/ConversationCursor;->notifyDataChanged()V

    goto :goto_0

    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method

.method public undo(Landroid/content/Context;Landroid/net/Uri;)V
    .locals 2
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/net/Uri;

    new-instance v0, Ljava/lang/Thread;

    new-instance v1, Lcom/android/mail/browse/ConversationCursor$1;

    invoke-direct {v1, p0, p1, p2}, Lcom/android/mail/browse/ConversationCursor$1;-><init>(Lcom/android/mail/browse/ConversationCursor;Landroid/content/Context;Landroid/net/Uri;)V

    invoke-direct {v0, v1}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;)V

    invoke-virtual {v0}, Ljava/lang/Thread;->start()V

    invoke-direct {p0}, Lcom/android/mail/browse/ConversationCursor;->undoLocal()V

    return-void
.end method

.method public unregisterContentObserver(Landroid/database/ContentObserver;)V
    .locals 0
    .param p1    # Landroid/database/ContentObserver;

    return-void
.end method

.method public unregisterDataSetObserver(Landroid/database/DataSetObserver;)V
    .locals 0
    .param p1    # Landroid/database/DataSetObserver;

    return-void
.end method

.method public updateBoolean(Landroid/content/Context;Lcom/android/mail/providers/Conversation;Ljava/lang/String;Z)I
    .locals 2
    .param p1    # Landroid/content/Context;
    .param p2    # Lcom/android/mail/providers/Conversation;
    .param p3    # Ljava/lang/String;
    .param p4    # Z

    const/4 v0, 0x1

    new-array v0, v0, [Lcom/android/mail/providers/Conversation;

    const/4 v1, 0x0

    aput-object p2, v0, v1

    invoke-static {v0}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    invoke-virtual {p0, p1, v0, p3, p4}, Lcom/android/mail/browse/ConversationCursor;->updateBoolean(Landroid/content/Context;Ljava/util/Collection;Ljava/lang/String;Z)I

    move-result v0

    return v0
.end method

.method public updateBoolean(Landroid/content/Context;Ljava/util/Collection;Ljava/lang/String;Z)I
    .locals 2
    .param p1    # Landroid/content/Context;
    .param p3    # Ljava/lang/String;
    .param p4    # Z
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Ljava/util/Collection",
            "<",
            "Lcom/android/mail/providers/Conversation;",
            ">;",
            "Ljava/lang/String;",
            "Z)I"
        }
    .end annotation

    new-instance v0, Landroid/content/ContentValues;

    invoke-direct {v0}, Landroid/content/ContentValues;-><init>()V

    invoke-static {p4}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {v0, p3, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Boolean;)V

    invoke-virtual {p0, p1, p2, v0}, Lcom/android/mail/browse/ConversationCursor;->updateValues(Landroid/content/Context;Ljava/util/Collection;Landroid/content/ContentValues;)I

    move-result v1

    return v1
.end method

.method public updateBulkValues(Landroid/content/Context;Ljava/util/Collection;)I
    .locals 1
    .param p1    # Landroid/content/Context;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Ljava/util/Collection",
            "<",
            "Lcom/android/mail/browse/ConversationCursor$ConversationOperation;",
            ">;)I"
        }
    .end annotation

    invoke-direct {p0, p1, p2}, Lcom/android/mail/browse/ConversationCursor;->apply(Landroid/content/Context;Ljava/util/Collection;)I

    move-result v0

    return v0
.end method

.method public updateInt(Landroid/content/Context;Ljava/util/Collection;Ljava/lang/String;I)I
    .locals 6
    .param p1    # Landroid/content/Context;
    .param p3    # Ljava/lang/String;
    .param p4    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Ljava/util/Collection",
            "<",
            "Lcom/android/mail/providers/Conversation;",
            ">;",
            "Ljava/lang/String;",
            "I)I"
        }
    .end annotation

    sget-object v1, Lcom/android/mail/browse/ConversationCursor;->LOG_TAG:Ljava/lang/String;

    const/4 v2, 0x3

    invoke-static {v1, v2}, Lcom/android/mail/utils/LogUtils;->isLoggable(Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_0

    sget-object v1, Lcom/android/mail/browse/ConversationCursor;->LOG_TAG:Ljava/lang/String;

    const-string v2, "ConversationCursor.updateInt(conversations=%s, columnName=%s)"

    const/4 v3, 0x2

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    invoke-interface {p2}, Ljava/util/Collection;->toArray()[Ljava/lang/Object;

    move-result-object v5

    aput-object v5, v3, v4

    const/4 v4, 0x1

    aput-object p3, v3, v4

    invoke-static {v1, v2, v3}, Lcom/android/mail/utils/LogUtils;->d(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)I

    :cond_0
    new-instance v0, Landroid/content/ContentValues;

    invoke-direct {v0}, Landroid/content/ContentValues;-><init>()V

    invoke-static {p4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, p3, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    invoke-virtual {p0, p1, p2, v0}, Lcom/android/mail/browse/ConversationCursor;->updateValues(Landroid/content/Context;Ljava/util/Collection;Landroid/content/ContentValues;)I

    move-result v1

    return v1
.end method

.method public updateValues(Landroid/content/Context;Ljava/util/Collection;Landroid/content/ContentValues;)I
    .locals 1
    .param p1    # Landroid/content/Context;
    .param p3    # Landroid/content/ContentValues;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Ljava/util/Collection",
            "<",
            "Lcom/android/mail/providers/Conversation;",
            ">;",
            "Landroid/content/ContentValues;",
            ")I"
        }
    .end annotation

    const/4 v0, 0x2

    invoke-direct {p0, p2, v0, p3}, Lcom/android/mail/browse/ConversationCursor;->getOperationsForConversations(Ljava/util/Collection;ILandroid/content/ContentValues;)Ljava/util/ArrayList;

    move-result-object v0

    invoke-direct {p0, p1, v0}, Lcom/android/mail/browse/ConversationCursor;->apply(Landroid/content/Context;Ljava/util/Collection;)I

    move-result v0

    return v0
.end method
