.class public Lcom/android/mail/browse/SendersView;
.super Ljava/lang/Object;
.source "SendersView.java"


# static fields
.field private static final DOES_NOT_EXIST:Ljava/lang/Integer;

.field private static final PRIORITY_LENGTH_MAP_CACHE:Lcom/android/mail/utils/ObjectCache;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/android/mail/utils/ObjectCache",
            "<",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/Integer;",
            "Ljava/lang/Integer;",
            ">;>;"
        }
    .end annotation
.end field

.field public static SENDERS_VERSION_SEPARATOR:Ljava/lang/String;

.field public static SENDERS_VERSION_SEPARATOR_PATTERN:Ljava/util/regex/Pattern;

.field private static sConfigurationChangedReceiver:Landroid/content/BroadcastReceiver;

.field private static sDraftCountFormatString:Ljava/lang/String;

.field private static sDraftPluralString:Ljava/lang/CharSequence;

.field private static sDraftSingularString:Ljava/lang/CharSequence;

.field private static sDraftsStyleSpan:Landroid/text/style/CharacterStyle;

.field public static sElidedString:Ljava/lang/CharSequence;

.field private static sMeString:Ljava/lang/String;

.field private static sMeStringLocale:Ljava/util/Locale;

.field private static sMessageCountSpacerString:Ljava/lang/String;

.field private static sMessageInfoReadStyleSpan:Landroid/text/style/TextAppearanceSpan;

.field private static sMessageInfoUnreadStyleSpan:Landroid/text/style/TextAppearanceSpan;

.field private static sReadStyleSpan:Landroid/text/style/CharacterStyle;

.field private static sSendersSplitToken:Ljava/lang/String;

.field private static sSendingString:Ljava/lang/CharSequence;

.field private static sSendingStyleSpan:Landroid/text/style/CharacterStyle;

.field private static sUnreadStyleSpan:Landroid/text/style/TextAppearanceSpan;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    const/4 v0, -0x5

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    sput-object v0, Lcom/android/mail/browse/SendersView;->DOES_NOT_EXIST:Ljava/lang/Integer;

    const-string v0, "^**^"

    sput-object v0, Lcom/android/mail/browse/SendersView;->SENDERS_VERSION_SEPARATOR:Ljava/lang/String;

    const-string v0, "\\^\\*\\*\\^"

    invoke-static {v0}, Ljava/util/regex/Pattern;->compile(Ljava/lang/String;)Ljava/util/regex/Pattern;

    move-result-object v0

    sput-object v0, Lcom/android/mail/browse/SendersView;->SENDERS_VERSION_SEPARATOR_PATTERN:Ljava/util/regex/Pattern;

    new-instance v0, Lcom/android/mail/utils/ObjectCache;

    new-instance v1, Lcom/android/mail/browse/SendersView$1;

    invoke-direct {v1}, Lcom/android/mail/browse/SendersView$1;-><init>()V

    const/4 v2, 0x2

    invoke-direct {v0, v1, v2}, Lcom/android/mail/utils/ObjectCache;-><init>(Lcom/android/mail/utils/ObjectCache$Callback;I)V

    sput-object v0, Lcom/android/mail/browse/SendersView;->PRIORITY_LENGTH_MAP_CACHE:Lcom/android/mail/utils/ObjectCache;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method static synthetic access$002(Ljava/lang/CharSequence;)Ljava/lang/CharSequence;
    .locals 0
    .param p0    # Ljava/lang/CharSequence;

    sput-object p0, Lcom/android/mail/browse/SendersView;->sDraftSingularString:Ljava/lang/CharSequence;

    return-object p0
.end method

.method static synthetic access$100(Landroid/content/Context;)V
    .locals 0
    .param p0    # Landroid/content/Context;

    invoke-static {p0}, Lcom/android/mail/browse/SendersView;->getSenderResources(Landroid/content/Context;)V

    return-void
.end method

.method public static createMessageInfo(Landroid/content/Context;Lcom/android/mail/providers/Conversation;)Landroid/text/SpannableStringBuilder;
    .locals 17
    .param p0    # Landroid/content/Context;
    .param p1    # Lcom/android/mail/providers/Conversation;

    move-object/from16 v0, p1

    iget-object v1, v0, Lcom/android/mail/providers/Conversation;->conversationInfo:Lcom/android/mail/providers/ConversationInfo;

    move-object/from16 v0, p1

    iget v10, v0, Lcom/android/mail/providers/Conversation;->sendingState:I

    new-instance v8, Landroid/text/SpannableStringBuilder;

    invoke-direct {v8}, Landroid/text/SpannableStringBuilder;-><init>()V

    const/4 v5, 0x0

    iget-object v12, v1, Lcom/android/mail/providers/ConversationInfo;->messageInfos:Ljava/util/ArrayList;

    invoke-virtual {v12}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v6

    :cond_0
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v12

    if-eqz v12, :cond_1

    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Lcom/android/mail/providers/MessageInfo;

    iget-object v12, v7, Lcom/android/mail/providers/MessageInfo;->sender:Ljava/lang/String;

    invoke-static {v12}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v12

    if-nez v12, :cond_0

    const/4 v5, 0x1

    :cond_1
    invoke-static/range {p0 .. p0}, Lcom/android/mail/browse/SendersView;->getSenderResources(Landroid/content/Context;)V

    if-eqz v1, :cond_b

    iget v2, v1, Lcom/android/mail/providers/ConversationInfo;->messageCount:I

    iget v3, v1, Lcom/android/mail/providers/ConversationInfo;->draftCount:I

    const/4 v12, 0x2

    if-ne v10, v12, :cond_c

    const/4 v11, 0x1

    :goto_0
    const/4 v12, 0x1

    if-le v2, v12, :cond_2

    new-instance v12, Ljava/lang/StringBuilder;

    invoke-direct {v12}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v12, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v12

    const-string v13, ""

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v12

    invoke-virtual {v8, v12}, Landroid/text/SpannableStringBuilder;->append(Ljava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    :cond_2
    move-object/from16 v0, p1

    iget-boolean v12, v0, Lcom/android/mail/providers/Conversation;->read:Z

    if-eqz v12, :cond_d

    sget-object v12, Lcom/android/mail/browse/SendersView;->sMessageInfoReadStyleSpan:Landroid/text/style/TextAppearanceSpan;

    :goto_1
    invoke-static {v12}, Landroid/text/style/CharacterStyle;->wrap(Landroid/text/style/CharacterStyle;)Landroid/text/style/CharacterStyle;

    move-result-object v12

    const/4 v13, 0x0

    invoke-virtual {v8}, Landroid/text/SpannableStringBuilder;->length()I

    move-result v14

    const/4 v15, 0x0

    invoke-virtual {v8, v12, v13, v14, v15}, Landroid/text/SpannableStringBuilder;->setSpan(Ljava/lang/Object;III)V

    if-lez v3, :cond_5

    if-nez v5, :cond_3

    const/4 v12, 0x1

    if-le v2, v12, :cond_4

    :cond_3
    sget-object v12, Lcom/android/mail/browse/SendersView;->sSendersSplitToken:Ljava/lang/String;

    invoke-virtual {v8, v12}, Landroid/text/SpannableStringBuilder;->append(Ljava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    :cond_4
    new-instance v4, Landroid/text/SpannableStringBuilder;

    invoke-direct {v4}, Landroid/text/SpannableStringBuilder;-><init>()V

    const/4 v12, 0x1

    if-ne v3, v12, :cond_e

    sget-object v12, Lcom/android/mail/browse/SendersView;->sDraftSingularString:Ljava/lang/CharSequence;

    invoke-virtual {v4, v12}, Landroid/text/SpannableStringBuilder;->append(Ljava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    :goto_2
    sget-object v12, Lcom/android/mail/browse/SendersView;->sDraftsStyleSpan:Landroid/text/style/CharacterStyle;

    invoke-static {v12}, Landroid/text/style/CharacterStyle;->wrap(Landroid/text/style/CharacterStyle;)Landroid/text/style/CharacterStyle;

    move-result-object v12

    const/4 v13, 0x0

    invoke-virtual {v4}, Landroid/text/SpannableStringBuilder;->length()I

    move-result v14

    const/16 v15, 0x21

    invoke-virtual {v4, v12, v13, v14, v15}, Landroid/text/SpannableStringBuilder;->setSpan(Ljava/lang/Object;III)V

    invoke-virtual {v8, v4}, Landroid/text/SpannableStringBuilder;->append(Ljava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    :cond_5
    if-eqz v11, :cond_8

    const/4 v12, 0x1

    if-gt v2, v12, :cond_6

    if-lez v3, :cond_7

    :cond_6
    sget-object v12, Lcom/android/mail/browse/SendersView;->sSendersSplitToken:Ljava/lang/String;

    invoke-virtual {v8, v12}, Landroid/text/SpannableStringBuilder;->append(Ljava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    :cond_7
    new-instance v9, Landroid/text/SpannableStringBuilder;

    invoke-direct {v9}, Landroid/text/SpannableStringBuilder;-><init>()V

    sget-object v12, Lcom/android/mail/browse/SendersView;->sSendingString:Ljava/lang/CharSequence;

    invoke-virtual {v9, v12}, Landroid/text/SpannableStringBuilder;->append(Ljava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    sget-object v12, Lcom/android/mail/browse/SendersView;->sSendingStyleSpan:Landroid/text/style/CharacterStyle;

    const/4 v13, 0x0

    invoke-virtual {v9}, Landroid/text/SpannableStringBuilder;->length()I

    move-result v14

    const/4 v15, 0x0

    invoke-virtual {v9, v12, v13, v14, v15}, Landroid/text/SpannableStringBuilder;->setSpan(Ljava/lang/Object;III)V

    invoke-virtual {v8, v9}, Landroid/text/SpannableStringBuilder;->append(Ljava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    :cond_8
    const/4 v12, 0x1

    if-gt v2, v12, :cond_a

    if-lez v3, :cond_9

    if-nez v5, :cond_a

    :cond_9
    if-eqz v11, :cond_b

    :cond_a
    new-instance v12, Landroid/text/SpannableStringBuilder;

    sget-object v13, Lcom/android/mail/browse/SendersView;->sMessageCountSpacerString:Ljava/lang/String;

    invoke-direct {v12, v13}, Landroid/text/SpannableStringBuilder;-><init>(Ljava/lang/CharSequence;)V

    invoke-virtual {v12, v8}, Landroid/text/SpannableStringBuilder;->append(Ljava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    move-result-object v8

    :cond_b
    return-object v8

    :cond_c
    const/4 v11, 0x0

    goto/16 :goto_0

    :cond_d
    sget-object v12, Lcom/android/mail/browse/SendersView;->sMessageInfoUnreadStyleSpan:Landroid/text/style/TextAppearanceSpan;

    goto :goto_1

    :cond_e
    new-instance v12, Ljava/lang/StringBuilder;

    invoke-direct {v12}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v13, Lcom/android/mail/browse/SendersView;->sDraftPluralString:Ljava/lang/CharSequence;

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v12

    sget-object v13, Lcom/android/mail/browse/SendersView;->sDraftCountFormatString:Ljava/lang/String;

    const/4 v14, 0x1

    new-array v14, v14, [Ljava/lang/Object;

    const/4 v15, 0x0

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v16

    aput-object v16, v14, v15

    invoke-static {v13, v14}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v13

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v12

    invoke-virtual {v4, v12}, Landroid/text/SpannableStringBuilder;->append(Ljava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    goto :goto_2
.end method

.method public static format(Landroid/content/Context;Lcom/android/mail/providers/ConversationInfo;Ljava/lang/String;ILjava/util/ArrayList;Ljava/util/ArrayList;Ljava/util/ArrayList;Ljava/lang/String;)V
    .locals 10
    .param p0    # Landroid/content/Context;
    .param p1    # Lcom/android/mail/providers/ConversationInfo;
    .param p2    # Ljava/lang/String;
    .param p3    # I
    .param p7    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Lcom/android/mail/providers/ConversationInfo;",
            "Ljava/lang/String;",
            "I",
            "Ljava/util/ArrayList",
            "<",
            "Landroid/text/SpannableString;",
            ">;",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    invoke-static {p0}, Lcom/android/mail/browse/SendersView;->getSenderResources(Landroid/content/Context;)V

    sget-object v8, Lcom/android/mail/browse/SendersView;->sUnreadStyleSpan:Landroid/text/style/TextAppearanceSpan;

    sget-object v9, Lcom/android/mail/browse/SendersView;->sReadStyleSpan:Landroid/text/style/CharacterStyle;

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move v3, p3

    move-object v4, p4

    move-object v5, p5

    move-object/from16 v6, p6

    move-object/from16 v7, p7

    invoke-static/range {v0 .. v9}, Lcom/android/mail/browse/SendersView;->format(Landroid/content/Context;Lcom/android/mail/providers/ConversationInfo;Ljava/lang/String;ILjava/util/ArrayList;Ljava/util/ArrayList;Ljava/util/ArrayList;Ljava/lang/String;Landroid/text/style/TextAppearanceSpan;Landroid/text/style/CharacterStyle;)V

    return-void
.end method

.method public static format(Landroid/content/Context;Lcom/android/mail/providers/ConversationInfo;Ljava/lang/String;ILjava/util/ArrayList;Ljava/util/ArrayList;Ljava/util/ArrayList;Ljava/lang/String;Landroid/text/style/TextAppearanceSpan;Landroid/text/style/CharacterStyle;)V
    .locals 10
    .param p0    # Landroid/content/Context;
    .param p1    # Lcom/android/mail/providers/ConversationInfo;
    .param p2    # Ljava/lang/String;
    .param p3    # I
    .param p7    # Ljava/lang/String;
    .param p8    # Landroid/text/style/TextAppearanceSpan;
    .param p9    # Landroid/text/style/CharacterStyle;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Lcom/android/mail/providers/ConversationInfo;",
            "Ljava/lang/String;",
            "I",
            "Ljava/util/ArrayList",
            "<",
            "Landroid/text/SpannableString;",
            ">;",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;",
            "Ljava/lang/String;",
            "Landroid/text/style/TextAppearanceSpan;",
            "Landroid/text/style/CharacterStyle;",
            ")V"
        }
    .end annotation

    invoke-static {p0}, Lcom/android/mail/browse/SendersView;->getSenderResources(Landroid/content/Context;)V

    move-object v0, p0

    move v1, p3

    move-object v2, p2

    move-object v3, p1

    move-object v4, p4

    move-object v5, p5

    move-object/from16 v6, p6

    move-object/from16 v7, p7

    move-object/from16 v8, p8

    move-object/from16 v9, p9

    invoke-static/range {v0 .. v9}, Lcom/android/mail/browse/SendersView;->handlePriority(Landroid/content/Context;ILjava/lang/String;Lcom/android/mail/providers/ConversationInfo;Ljava/util/ArrayList;Ljava/util/ArrayList;Ljava/util/ArrayList;Ljava/lang/String;Landroid/text/style/TextAppearanceSpan;Landroid/text/style/CharacterStyle;)V

    return-void
.end method

.method private static formatDefault(Lcom/android/mail/browse/ConversationItemViewModel;Ljava/lang/String;Landroid/content/Context;Landroid/text/style/CharacterStyle;)V
    .locals 7
    .param p0    # Lcom/android/mail/browse/ConversationItemViewModel;
    .param p1    # Ljava/lang/String;
    .param p2    # Landroid/content/Context;
    .param p3    # Landroid/text/style/CharacterStyle;

    const/4 v6, 0x0

    invoke-static {p2}, Lcom/android/mail/browse/SendersView;->getSenderResources(Landroid/content/Context;)V

    iget-object v5, p0, Lcom/android/mail/browse/ConversationItemViewModel;->senderFragments:Ljava/util/ArrayList;

    invoke-virtual {v5}, Ljava/util/ArrayList;->clear()V

    const-string v5, ","

    invoke-static {p1, v5}, Landroid/text/TextUtils;->split(Ljava/lang/String;Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v4

    array-length v5, v4

    new-array v2, v5, [Ljava/lang/String;

    const/4 v1, 0x0

    :goto_0
    array-length v5, v4

    if-ge v1, v5, :cond_2

    aget-object v5, v4, v1

    invoke-static {v5}, Landroid/text/util/Rfc822Tokenizer;->tokenize(Ljava/lang/CharSequence;)[Landroid/text/util/Rfc822Token;

    move-result-object v3

    if-eqz v3, :cond_1

    array-length v5, v3

    if-lez v5, :cond_1

    aget-object v5, v3, v6

    invoke-virtual {v5}, Landroid/text/util/Rfc822Token;->getName()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v5

    if-eqz v5, :cond_0

    aget-object v5, v3, v6

    invoke-virtual {v5}, Landroid/text/util/Rfc822Token;->getAddress()Ljava/lang/String;

    move-result-object v0

    :cond_0
    aput-object v0, v2, v1

    :cond_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_2
    invoke-static {p0, v2, p3}, Lcom/android/mail/browse/SendersView;->generateSenderFragments(Lcom/android/mail/browse/ConversationItemViewModel;[Ljava/lang/String;Landroid/text/style/CharacterStyle;)V

    return-void
.end method

.method public static formatSenders(Lcom/android/mail/browse/ConversationItemViewModel;Landroid/content/Context;)V
    .locals 1
    .param p0    # Lcom/android/mail/browse/ConversationItemViewModel;
    .param p1    # Landroid/content/Context;

    invoke-static {p1}, Lcom/android/mail/browse/SendersView;->getSenderResources(Landroid/content/Context;)V

    sget-object v0, Lcom/android/mail/browse/SendersView;->sReadStyleSpan:Landroid/text/style/CharacterStyle;

    invoke-static {p0, p1, v0}, Lcom/android/mail/browse/SendersView;->formatSenders(Lcom/android/mail/browse/ConversationItemViewModel;Landroid/content/Context;Landroid/text/style/CharacterStyle;)V

    return-void
.end method

.method public static formatSenders(Lcom/android/mail/browse/ConversationItemViewModel;Landroid/content/Context;Landroid/text/style/CharacterStyle;)V
    .locals 1
    .param p0    # Lcom/android/mail/browse/ConversationItemViewModel;
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/text/style/CharacterStyle;

    iget-object v0, p0, Lcom/android/mail/browse/ConversationItemViewModel;->conversation:Lcom/android/mail/providers/Conversation;

    iget-object v0, v0, Lcom/android/mail/providers/Conversation;->senders:Ljava/lang/String;

    invoke-static {p0, v0, p1, p2}, Lcom/android/mail/browse/SendersView;->formatDefault(Lcom/android/mail/browse/ConversationItemViewModel;Ljava/lang/String;Landroid/content/Context;Landroid/text/style/CharacterStyle;)V

    return-void
.end method

.method private static generateSenderFragments(Lcom/android/mail/browse/ConversationItemViewModel;[Ljava/lang/String;Landroid/text/style/CharacterStyle;)V
    .locals 4
    .param p0    # Lcom/android/mail/browse/ConversationItemViewModel;
    .param p1    # [Ljava/lang/String;
    .param p2    # Landroid/text/style/CharacterStyle;

    const-string v0, ", "

    invoke-static {v0, p1}, Landroid/text/TextUtils;->join(Ljava/lang/CharSequence;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/android/mail/browse/ConversationItemViewModel;->sendersText:Ljava/lang/String;

    const/4 v0, 0x0

    iget-object v1, p0, Lcom/android/mail/browse/ConversationItemViewModel;->sendersText:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v1

    invoke-static {p2}, Lcom/android/mail/browse/SendersView;->getWrappedStyleSpan(Landroid/text/style/CharacterStyle;)Landroid/text/style/CharacterStyle;

    move-result-object v2

    const/4 v3, 0x1

    invoke-virtual {p0, v0, v1, v2, v3}, Lcom/android/mail/browse/ConversationItemViewModel;->addSenderFragment(IILandroid/text/style/CharacterStyle;Z)V

    return-void
.end method

.method static getMe(Landroid/content/Context;)Ljava/lang/String;
    .locals 3
    .param p0    # Landroid/content/Context;

    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v2

    iget-object v0, v2, Landroid/content/res/Configuration;->locale:Ljava/util/Locale;

    sget-object v2, Lcom/android/mail/browse/SendersView;->sMeString:Ljava/lang/String;

    if-eqz v2, :cond_0

    sget-object v2, Lcom/android/mail/browse/SendersView;->sMeStringLocale:Ljava/util/Locale;

    invoke-virtual {v0, v2}, Ljava/util/Locale;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_1

    :cond_0
    const v2, 0x7f0900a9

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    sput-object v2, Lcom/android/mail/browse/SendersView;->sMeString:Ljava/lang/String;

    sput-object v0, Lcom/android/mail/browse/SendersView;->sMeStringLocale:Ljava/util/Locale;

    :cond_1
    sget-object v2, Lcom/android/mail/browse/SendersView;->sMeString:Ljava/lang/String;

    return-object v2
.end method

.method private static getSenderResources(Landroid/content/Context;)V
    .locals 5
    .param p0    # Landroid/content/Context;

    const v4, 0x7f100003

    sget-object v1, Lcom/android/mail/browse/SendersView;->sConfigurationChangedReceiver:Landroid/content/BroadcastReceiver;

    if-nez v1, :cond_0

    new-instance v1, Lcom/android/mail/browse/SendersView$2;

    invoke-direct {v1}, Lcom/android/mail/browse/SendersView$2;-><init>()V

    sput-object v1, Lcom/android/mail/browse/SendersView;->sConfigurationChangedReceiver:Landroid/content/BroadcastReceiver;

    sget-object v1, Lcom/android/mail/browse/SendersView;->sConfigurationChangedReceiver:Landroid/content/BroadcastReceiver;

    new-instance v2, Landroid/content/IntentFilter;

    const-string v3, "android.intent.action.CONFIGURATION_CHANGED"

    invoke-direct {v2, v3}, Landroid/content/IntentFilter;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0, v1, v2}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    :cond_0
    sget-object v1, Lcom/android/mail/browse/SendersView;->sDraftSingularString:Ljava/lang/CharSequence;

    if-nez v1, :cond_1

    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f09010e

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    sput-object v1, Lcom/android/mail/browse/SendersView;->sSendersSplitToken:Ljava/lang/String;

    const v1, 0x7f090110

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    sput-object v1, Lcom/android/mail/browse/SendersView;->sElidedString:Ljava/lang/CharSequence;

    const/4 v1, 0x1

    invoke-virtual {v0, v4, v1}, Landroid/content/res/Resources;->getQuantityText(II)Ljava/lang/CharSequence;

    move-result-object v1

    sput-object v1, Lcom/android/mail/browse/SendersView;->sDraftSingularString:Ljava/lang/CharSequence;

    const/4 v1, 0x2

    invoke-virtual {v0, v4, v1}, Landroid/content/res/Resources;->getQuantityText(II)Ljava/lang/CharSequence;

    move-result-object v1

    sput-object v1, Lcom/android/mail/browse/SendersView;->sDraftPluralString:Ljava/lang/CharSequence;

    const v1, 0x7f09010f

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    sput-object v1, Lcom/android/mail/browse/SendersView;->sDraftCountFormatString:Ljava/lang/String;

    new-instance v1, Landroid/text/style/TextAppearanceSpan;

    const v2, 0x7f0d005b

    invoke-direct {v1, p0, v2}, Landroid/text/style/TextAppearanceSpan;-><init>(Landroid/content/Context;I)V

    sput-object v1, Lcom/android/mail/browse/SendersView;->sMessageInfoUnreadStyleSpan:Landroid/text/style/TextAppearanceSpan;

    new-instance v1, Landroid/text/style/TextAppearanceSpan;

    const v2, 0x7f0d005c

    invoke-direct {v1, p0, v2}, Landroid/text/style/TextAppearanceSpan;-><init>(Landroid/content/Context;I)V

    sput-object v1, Lcom/android/mail/browse/SendersView;->sMessageInfoReadStyleSpan:Landroid/text/style/TextAppearanceSpan;

    new-instance v1, Landroid/text/style/TextAppearanceSpan;

    const v2, 0x7f0d005d

    invoke-direct {v1, p0, v2}, Landroid/text/style/TextAppearanceSpan;-><init>(Landroid/content/Context;I)V

    sput-object v1, Lcom/android/mail/browse/SendersView;->sDraftsStyleSpan:Landroid/text/style/CharacterStyle;

    new-instance v1, Landroid/text/style/TextAppearanceSpan;

    const v2, 0x7f0d0057

    invoke-direct {v1, p0, v2}, Landroid/text/style/TextAppearanceSpan;-><init>(Landroid/content/Context;I)V

    sput-object v1, Lcom/android/mail/browse/SendersView;->sUnreadStyleSpan:Landroid/text/style/TextAppearanceSpan;

    new-instance v1, Landroid/text/style/TextAppearanceSpan;

    const v2, 0x7f0d005e

    invoke-direct {v1, p0, v2}, Landroid/text/style/TextAppearanceSpan;-><init>(Landroid/content/Context;I)V

    sput-object v1, Lcom/android/mail/browse/SendersView;->sSendingStyleSpan:Landroid/text/style/CharacterStyle;

    new-instance v1, Landroid/text/style/TextAppearanceSpan;

    const v2, 0x7f0d0058

    invoke-direct {v1, p0, v2}, Landroid/text/style/TextAppearanceSpan;-><init>(Landroid/content/Context;I)V

    sput-object v1, Lcom/android/mail/browse/SendersView;->sReadStyleSpan:Landroid/text/style/CharacterStyle;

    const v1, 0x7f090112

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    sput-object v1, Lcom/android/mail/browse/SendersView;->sMessageCountSpacerString:Ljava/lang/String;

    const v1, 0x7f0900a7

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    sput-object v1, Lcom/android/mail/browse/SendersView;->sSendingString:Ljava/lang/CharSequence;

    :cond_1
    return-void
.end method

.method public static getTypeface(Z)Landroid/graphics/Typeface;
    .locals 1
    .param p0    # Z

    if-eqz p0, :cond_0

    sget-object v0, Landroid/graphics/Typeface;->DEFAULT_BOLD:Landroid/graphics/Typeface;

    :goto_0
    return-object v0

    :cond_0
    sget-object v0, Landroid/graphics/Typeface;->DEFAULT:Landroid/graphics/Typeface;

    goto :goto_0
.end method

.method private static getWrappedStyleSpan(Landroid/text/style/CharacterStyle;)Landroid/text/style/CharacterStyle;
    .locals 1
    .param p0    # Landroid/text/style/CharacterStyle;

    invoke-static {p0}, Landroid/text/style/CharacterStyle;->wrap(Landroid/text/style/CharacterStyle;)Landroid/text/style/CharacterStyle;

    move-result-object v0

    return-object v0
.end method

.method public static handlePriority(Landroid/content/Context;ILjava/lang/String;Lcom/android/mail/providers/ConversationInfo;Ljava/util/ArrayList;Ljava/util/ArrayList;Ljava/util/ArrayList;Ljava/lang/String;Landroid/text/style/TextAppearanceSpan;Landroid/text/style/CharacterStyle;)V
    .locals 30
    .param p0    # Landroid/content/Context;
    .param p1    # I
    .param p2    # Ljava/lang/String;
    .param p3    # Lcom/android/mail/providers/ConversationInfo;
    .param p7    # Ljava/lang/String;
    .param p8    # Landroid/text/style/TextAppearanceSpan;
    .param p9    # Landroid/text/style/CharacterStyle;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "I",
            "Ljava/lang/String;",
            "Lcom/android/mail/providers/ConversationInfo;",
            "Ljava/util/ArrayList",
            "<",
            "Landroid/text/SpannableString;",
            ">;",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;",
            "Ljava/lang/String;",
            "Landroid/text/style/TextAppearanceSpan;",
            "Landroid/text/style/CharacterStyle;",
            ")V"
        }
    .end annotation

    if-eqz p6, :cond_1

    const/16 v24, 0x1

    :goto_0
    const/4 v14, -0x1

    invoke-virtual/range {p2 .. p2}, Ljava/lang/String;->length()I

    move-result v17

    const/16 v18, 0x0

    const/16 v16, 0x0

    const/4 v13, 0x0

    move/from16 v0, v17

    move/from16 v1, p1

    if-le v0, v1, :cond_0

    sub-int v16, v17, p1

    :cond_0
    sget-object v27, Lcom/android/mail/browse/SendersView;->PRIORITY_LENGTH_MAP_CACHE:Lcom/android/mail/utils/ObjectCache;

    invoke-virtual/range {v27 .. v27}, Lcom/android/mail/utils/ObjectCache;->get()Ljava/lang/Object;

    move-result-object v21

    check-cast v21, Ljava/util/Map;

    :try_start_0
    invoke-interface/range {v21 .. v21}, Ljava/util/Map;->clear()V

    move-object/from16 v0, p3

    iget-object v0, v0, Lcom/android/mail/providers/ConversationInfo;->messageInfos:Ljava/util/ArrayList;

    move-object/from16 v27, v0

    invoke-virtual/range {v27 .. v27}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v9

    :goto_1
    invoke-interface {v9}, Ljava/util/Iterator;->hasNext()Z

    move-result v27

    if-eqz v27, :cond_5

    invoke-interface {v9}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v11

    check-cast v11, Lcom/android/mail/providers/MessageInfo;

    iget-object v0, v11, Lcom/android/mail/providers/MessageInfo;->sender:Ljava/lang/String;

    move-object/from16 v27, v0

    invoke-static/range {v27 .. v27}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v27

    if-nez v27, :cond_2

    iget-object v0, v11, Lcom/android/mail/providers/MessageInfo;->sender:Ljava/lang/String;

    move-object/from16 v27, v0

    invoke-virtual/range {v27 .. v27}, Ljava/lang/String;->length()I

    move-result v23

    :goto_2
    iget v0, v11, Lcom/android/mail/providers/MessageInfo;->priority:I

    move/from16 v27, v0

    invoke-static/range {v27 .. v27}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v27

    invoke-static/range {v23 .. v23}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v28

    move-object/from16 v0, v21

    move-object/from16 v1, v27

    move-object/from16 v2, v28

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    iget v0, v11, Lcom/android/mail/providers/MessageInfo;->priority:I

    move/from16 v27, v0

    move/from16 v0, v27

    invoke-static {v13, v0}, Ljava/lang/Math;->max(II)I

    move-result v13

    goto :goto_1

    :cond_1
    const/16 v24, 0x0

    goto :goto_0

    :cond_2
    const/16 v23, 0x0

    goto :goto_2

    :cond_3
    move/from16 v17, v12

    add-int/lit8 v18, v18, 0x1

    :cond_4
    add-int/lit8 v14, v14, 0x1

    :cond_5
    if-ge v14, v13, :cond_7

    add-int/lit8 v27, v14, 0x1

    invoke-static/range {v27 .. v27}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v27

    move-object/from16 v0, v21

    move-object/from16 v1, v27

    invoke-interface {v0, v1}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v27

    if-eqz v27, :cond_4

    add-int/lit8 v27, v14, 0x1

    invoke-static/range {v27 .. v27}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v27

    move-object/from16 v0, v21

    move-object/from16 v1, v27

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v27

    check-cast v27, Ljava/lang/Integer;

    invoke-virtual/range {v27 .. v27}, Ljava/lang/Integer;->intValue()I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v27

    add-int v12, v17, v27

    if-lez v17, :cond_6

    add-int/lit8 v12, v12, 0x2

    :cond_6
    move/from16 v0, p1

    if-le v12, v0, :cond_3

    const/16 v27, 0x2

    move/from16 v0, v18

    move/from16 v1, v27

    if-lt v0, v1, :cond_3

    :cond_7
    sget-object v27, Lcom/android/mail/browse/SendersView;->PRIORITY_LENGTH_MAP_CACHE:Lcom/android/mail/utils/ObjectCache;

    move-object/from16 v0, v27

    move-object/from16 v1, v21

    invoke-virtual {v0, v1}, Lcom/android/mail/utils/ObjectCache;->release(Ljava/lang/Object;)V

    const/4 v3, 0x0

    invoke-static {}, Lcom/google/common/collect/Maps;->newHashMap()Ljava/util/HashMap;

    move-result-object v5

    const/4 v7, 0x0

    const/4 v6, 0x0

    const/4 v8, 0x0

    :goto_3
    move-object/from16 v0, p3

    iget-object v0, v0, Lcom/android/mail/providers/ConversationInfo;->messageInfos:Ljava/util/ArrayList;

    move-object/from16 v27, v0

    invoke-virtual/range {v27 .. v27}, Ljava/util/ArrayList;->size()I

    move-result v27

    move/from16 v0, v27

    if-ge v8, v0, :cond_16

    move-object/from16 v0, p3

    iget-object v0, v0, Lcom/android/mail/providers/ConversationInfo;->messageInfos:Ljava/util/ArrayList;

    move-object/from16 v27, v0

    move-object/from16 v0, v27

    invoke-virtual {v0, v8}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/android/mail/providers/MessageInfo;

    iget-object v0, v4, Lcom/android/mail/providers/MessageInfo;->sender:Ljava/lang/String;

    move-object/from16 v27, v0

    invoke-static/range {v27 .. v27}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v27

    if-nez v27, :cond_e

    iget-object v15, v4, Lcom/android/mail/providers/MessageInfo;->sender:Ljava/lang/String;

    :goto_4
    invoke-virtual {v15}, Ljava/lang/String;->length()I

    move-result v27

    if-nez v27, :cond_8

    invoke-static/range {p0 .. p0}, Lcom/android/mail/browse/SendersView;->getMe(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v15

    :cond_8
    if-eqz v16, :cond_9

    const/16 v27, 0x0

    invoke-virtual {v15}, Ljava/lang/String;->length()I

    move-result v28

    sub-int v28, v28, v16

    const/16 v29, 0x0

    invoke-static/range {v28 .. v29}, Ljava/lang/Math;->max(II)I

    move-result v28

    move/from16 v0, v27

    move/from16 v1, v28

    invoke-virtual {v15, v0, v1}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v15

    :cond_9
    iget v0, v4, Lcom/android/mail/providers/MessageInfo;->priority:I

    move/from16 v20, v0

    iget-boolean v0, v4, Lcom/android/mail/providers/MessageInfo;->read:Z

    move/from16 v27, v0

    if-nez v27, :cond_f

    invoke-static/range {p8 .. p8}, Lcom/android/mail/browse/SendersView;->getWrappedStyleSpan(Landroid/text/style/CharacterStyle;)Landroid/text/style/CharacterStyle;

    move-result-object v26

    :goto_5
    move/from16 v0, v20

    if-gt v0, v14, :cond_11

    new-instance v25, Landroid/text/SpannableString;

    move-object/from16 v0, v25

    invoke-direct {v0, v15}, Landroid/text/SpannableString;-><init>(Ljava/lang/CharSequence;)V

    iget-object v0, v4, Lcom/android/mail/providers/MessageInfo;->sender:Ljava/lang/String;

    move-object/from16 v27, v0

    move-object/from16 v0, v27

    invoke-interface {v5, v0}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v27

    if-eqz v27, :cond_10

    iget-object v0, v4, Lcom/android/mail/providers/MessageInfo;->sender:Ljava/lang/String;

    move-object/from16 v27, v0

    move-object/from16 v0, v27

    invoke-interface {v5, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v27

    check-cast v27, Ljava/lang/Integer;

    :goto_6
    invoke-virtual/range {v27 .. v27}, Ljava/lang/Integer;->intValue()I

    move-result v19

    sget-object v27, Lcom/android/mail/browse/SendersView;->DOES_NOT_EXIST:Ljava/lang/Integer;

    invoke-virtual/range {v27 .. v27}, Ljava/lang/Integer;->intValue()I

    move-result v27

    move/from16 v0, v19

    move/from16 v1, v27

    if-eq v0, v1, :cond_a

    iget-boolean v0, v4, Lcom/android/mail/providers/MessageInfo;->read:Z

    move/from16 v27, v0

    if-nez v27, :cond_c

    :cond_a
    sget-object v27, Lcom/android/mail/browse/SendersView;->DOES_NOT_EXIST:Ljava/lang/Integer;

    invoke-virtual/range {v27 .. v27}, Ljava/lang/Integer;->intValue()I

    move-result v27

    move/from16 v0, v19

    move/from16 v1, v27

    if-eq v0, v1, :cond_b

    if-lez v8, :cond_b

    add-int/lit8 v27, v8, -0x1

    move/from16 v0, v19

    move/from16 v1, v27

    if-ne v0, v1, :cond_b

    invoke-virtual/range {p4 .. p4}, Ljava/util/ArrayList;->size()I

    move-result v27

    move/from16 v0, v19

    move/from16 v1, v27

    if-ge v0, v1, :cond_b

    const/16 v27, 0x0

    move-object/from16 v0, p4

    move/from16 v1, v19

    move-object/from16 v2, v27

    invoke-virtual {v0, v1, v2}, Ljava/util/ArrayList;->set(ILjava/lang/Object;)Ljava/lang/Object;

    if-eqz v24, :cond_b

    iget-object v0, v4, Lcom/android/mail/providers/MessageInfo;->senderEmail:Ljava/lang/String;

    move-object/from16 v27, v0

    invoke-static/range {v27 .. v27}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v27

    if-nez v27, :cond_b

    iget-object v0, v4, Lcom/android/mail/providers/MessageInfo;->senderEmail:Ljava/lang/String;

    move-object/from16 v27, v0

    move-object/from16 v0, p6

    move-object/from16 v1, v27

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z

    iget-object v0, v4, Lcom/android/mail/providers/MessageInfo;->sender:Ljava/lang/String;

    move-object/from16 v27, v0

    move-object/from16 v0, p5

    move-object/from16 v1, v27

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z

    :cond_b
    iget-object v0, v4, Lcom/android/mail/providers/MessageInfo;->sender:Ljava/lang/String;

    move-object/from16 v27, v0

    invoke-static {v8}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v28

    move-object/from16 v0, v27

    move-object/from16 v1, v28

    invoke-interface {v5, v0, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const/16 v27, 0x0

    invoke-virtual/range {v25 .. v25}, Landroid/text/SpannableString;->length()I

    move-result v28

    const/16 v29, 0x0

    invoke-virtual/range {v25 .. v29}, Landroid/text/SpannableString;->setSpan(Ljava/lang/Object;III)V

    move-object/from16 v0, p4

    move-object/from16 v1, v25

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :cond_c
    :goto_7
    if-eqz v24, :cond_d

    iget-object v0, v4, Lcom/android/mail/providers/MessageInfo;->sender:Ljava/lang/String;

    move-object/from16 v27, v0

    invoke-static/range {v27 .. v27}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v27

    if-eqz v27, :cond_12

    move-object/from16 v22, p7

    :goto_8
    if-nez v8, :cond_14

    move-object/from16 v7, v22

    iget-object v6, v4, Lcom/android/mail/providers/MessageInfo;->sender:Ljava/lang/String;

    :cond_d
    :goto_9
    add-int/lit8 v8, v8, 0x1

    goto/16 :goto_3

    :catchall_0
    move-exception v27

    sget-object v28, Lcom/android/mail/browse/SendersView;->PRIORITY_LENGTH_MAP_CACHE:Lcom/android/mail/utils/ObjectCache;

    move-object/from16 v0, v28

    move-object/from16 v1, v21

    invoke-virtual {v0, v1}, Lcom/android/mail/utils/ObjectCache;->release(Ljava/lang/Object;)V

    throw v27

    :cond_e
    const-string v15, ""

    goto/16 :goto_4

    :cond_f
    invoke-static/range {p9 .. p9}, Lcom/android/mail/browse/SendersView;->getWrappedStyleSpan(Landroid/text/style/CharacterStyle;)Landroid/text/style/CharacterStyle;

    move-result-object v26

    goto/16 :goto_5

    :cond_10
    sget-object v27, Lcom/android/mail/browse/SendersView;->DOES_NOT_EXIST:Ljava/lang/Integer;

    goto/16 :goto_6

    :cond_11
    if-nez v3, :cond_c

    new-instance v25, Landroid/text/SpannableString;

    sget-object v27, Lcom/android/mail/browse/SendersView;->sElidedString:Ljava/lang/CharSequence;

    move-object/from16 v0, v25

    move-object/from16 v1, v27

    invoke-direct {v0, v1}, Landroid/text/SpannableString;-><init>(Ljava/lang/CharSequence;)V

    const/16 v27, 0x0

    invoke-virtual/range {v25 .. v25}, Landroid/text/SpannableString;->length()I

    move-result v28

    const/16 v29, 0x0

    invoke-virtual/range {v25 .. v29}, Landroid/text/SpannableString;->setSpan(Ljava/lang/Object;III)V

    const/4 v3, 0x1

    move-object/from16 v0, p4

    move-object/from16 v1, v25

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_7

    :cond_12
    iget-object v0, v4, Lcom/android/mail/providers/MessageInfo;->senderEmail:Ljava/lang/String;

    move-object/from16 v27, v0

    invoke-static/range {v27 .. v27}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v27

    if-eqz v27, :cond_13

    iget-object v0, v4, Lcom/android/mail/providers/MessageInfo;->sender:Ljava/lang/String;

    move-object/from16 v22, v0

    goto :goto_8

    :cond_13
    iget-object v0, v4, Lcom/android/mail/providers/MessageInfo;->senderEmail:Ljava/lang/String;

    move-object/from16 v22, v0

    goto :goto_8

    :cond_14
    move-object/from16 v0, v22

    invoke-static {v7, v0}, Lcom/google/common/base/Objects;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v27

    if-nez v27, :cond_d

    move-object/from16 v0, p6

    move-object/from16 v1, v22

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->indexOf(Ljava/lang/Object;)I

    move-result v10

    const/16 v27, -0x1

    move/from16 v0, v27

    if-le v10, v0, :cond_15

    move-object/from16 v0, p6

    invoke-virtual {v0, v10}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;

    move-object/from16 v0, p5

    invoke-virtual {v0, v10}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;

    :cond_15
    move-object/from16 v0, p6

    move-object/from16 v1, v22

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    iget-object v0, v4, Lcom/android/mail/providers/MessageInfo;->sender:Ljava/lang/String;

    move-object/from16 v27, v0

    move-object/from16 v0, p5

    move-object/from16 v1, v27

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    invoke-virtual/range {p6 .. p6}, Ljava/util/ArrayList;->size()I

    move-result v27

    const/16 v28, 0x4

    move/from16 v0, v27

    move/from16 v1, v28

    if-le v0, v1, :cond_d

    const/16 v27, 0x0

    move-object/from16 v0, p6

    move/from16 v1, v27

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;

    const/16 v27, 0x0

    move-object/from16 v0, p5

    move/from16 v1, v27

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;

    goto/16 :goto_9

    :cond_16
    if-eqz v24, :cond_17

    invoke-static {v7}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v27

    if-nez v27, :cond_17

    invoke-virtual/range {p6 .. p6}, Ljava/util/ArrayList;->size()I

    move-result v27

    const/16 v28, 0x4

    move/from16 v0, v27

    move/from16 v1, v28

    if-ge v0, v1, :cond_18

    const/16 v27, 0x0

    move-object/from16 v0, p6

    move/from16 v1, v27

    invoke-virtual {v0, v1, v7}, Ljava/util/ArrayList;->add(ILjava/lang/Object;)V

    const/16 v27, 0x0

    move-object/from16 v0, p5

    move/from16 v1, v27

    invoke-virtual {v0, v1, v6}, Ljava/util/ArrayList;->add(ILjava/lang/Object;)V

    :cond_17
    :goto_a
    return-void

    :cond_18
    const/16 v27, 0x0

    move-object/from16 v0, p6

    move/from16 v1, v27

    invoke-virtual {v0, v1, v7}, Ljava/util/ArrayList;->set(ILjava/lang/Object;)Ljava/lang/Object;

    const/16 v27, 0x0

    move-object/from16 v0, p5

    move/from16 v1, v27

    invoke-virtual {v0, v1, v6}, Ljava/util/ArrayList;->set(ILjava/lang/Object;)Ljava/lang/Object;

    goto :goto_a
.end method
