.class public Lcom/android/mail/browse/AttachmentActionHandler;
.super Ljava/lang/Object;
.source "AttachmentActionHandler.java"


# static fields
.field private static final LOG_TAG:Ljava/lang/String;


# instance fields
.field private mAttachment:Lcom/android/mail/providers/Attachment;

.field private final mCommandHandler:Lcom/android/mail/browse/AttachmentCommandHandler;

.field private final mContext:Landroid/content/Context;

.field private mFragmentManager:Landroid/app/FragmentManager;

.field private final mHandler:Landroid/os/Handler;

.field private final mView:Lcom/android/mail/browse/AttachmentViewInterface;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    invoke-static {}, Lcom/android/mail/utils/LogTag;->getLogTag()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/android/mail/browse/AttachmentActionHandler;->LOG_TAG:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Lcom/android/mail/browse/AttachmentViewInterface;)V
    .locals 1
    .param p1    # Landroid/content/Context;
    .param p2    # Lcom/android/mail/browse/AttachmentViewInterface;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Lcom/android/mail/browse/AttachmentCommandHandler;

    invoke-direct {v0, p1}, Lcom/android/mail/browse/AttachmentCommandHandler;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/android/mail/browse/AttachmentActionHandler;->mCommandHandler:Lcom/android/mail/browse/AttachmentCommandHandler;

    iput-object p2, p0, Lcom/android/mail/browse/AttachmentActionHandler;->mView:Lcom/android/mail/browse/AttachmentViewInterface;

    iput-object p1, p0, Lcom/android/mail/browse/AttachmentActionHandler;->mContext:Landroid/content/Context;

    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    iput-object v0, p0, Lcom/android/mail/browse/AttachmentActionHandler;->mHandler:Landroid/os/Handler;

    return-void
.end method

.method private showDownloadingDialog(I)V
    .locals 5
    .param p1    # I

    iget-object v3, p0, Lcom/android/mail/browse/AttachmentActionHandler;->mFragmentManager:Landroid/app/FragmentManager;

    invoke-virtual {v3}, Landroid/app/FragmentManager;->beginTransaction()Landroid/app/FragmentTransaction;

    move-result-object v0

    iget-object v3, p0, Lcom/android/mail/browse/AttachmentActionHandler;->mFragmentManager:Landroid/app/FragmentManager;

    const-string v4, "attachment-progress"

    invoke-virtual {v3, v4}, Landroid/app/FragmentManager;->findFragmentByTag(Ljava/lang/String;)Landroid/app/Fragment;

    move-result-object v2

    if-eqz v2, :cond_0

    invoke-virtual {v0, v2}, Landroid/app/FragmentTransaction;->remove(Landroid/app/Fragment;)Landroid/app/FragmentTransaction;

    :cond_0
    const/4 v3, 0x0

    invoke-virtual {v0, v3}, Landroid/app/FragmentTransaction;->addToBackStack(Ljava/lang/String;)Landroid/app/FragmentTransaction;

    iget-object v3, p0, Lcom/android/mail/browse/AttachmentActionHandler;->mAttachment:Lcom/android/mail/providers/Attachment;

    invoke-static {v3, p1}, Lcom/android/mail/browse/AttachmentProgressDialogFragment;->newInstance(Lcom/android/mail/providers/Attachment;I)Lcom/android/mail/browse/AttachmentProgressDialogFragment;

    move-result-object v1

    const-string v3, "attachment-progress"

    invoke-virtual {v1, v0, v3}, Landroid/app/DialogFragment;->show(Landroid/app/FragmentTransaction;Ljava/lang/String;)I

    return-void
.end method

.method private startDownloadingAttachment(Lcom/android/mail/providers/Attachment;IIIZ)V
    .locals 3
    .param p1    # Lcom/android/mail/providers/Attachment;
    .param p2    # I
    .param p3    # I
    .param p4    # I
    .param p5    # Z

    const/4 v2, 0x2

    new-instance v0, Landroid/content/ContentValues;

    invoke-direct {v0, v2}, Landroid/content/ContentValues;-><init>(I)V

    const-string v1, "state"

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    const-string v1, "destination"

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    const-string v1, "rendition"

    invoke-static {p3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    const-string v1, "additionalPriority"

    invoke-static {p4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    const-string v1, "delayDownload"

    invoke-static {p5}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Boolean;)V

    iget-object v1, p0, Lcom/android/mail/browse/AttachmentActionHandler;->mCommandHandler:Lcom/android/mail/browse/AttachmentCommandHandler;

    iget-object v2, p1, Lcom/android/mail/providers/Attachment;->uri:Landroid/net/Uri;

    invoke-virtual {v1, v2, v0}, Lcom/android/mail/browse/AttachmentCommandHandler;->sendCommand(Landroid/net/Uri;Landroid/content/ContentValues;)V

    return-void
.end method


# virtual methods
.method public cancelAttachment()V
    .locals 3

    new-instance v0, Landroid/content/ContentValues;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Landroid/content/ContentValues;-><init>(I)V

    const-string v1, "state"

    const/4 v2, 0x0

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    iget-object v1, p0, Lcom/android/mail/browse/AttachmentActionHandler;->mCommandHandler:Lcom/android/mail/browse/AttachmentCommandHandler;

    iget-object v2, p0, Lcom/android/mail/browse/AttachmentActionHandler;->mAttachment:Lcom/android/mail/providers/Attachment;

    iget-object v2, v2, Lcom/android/mail/providers/Attachment;->uri:Landroid/net/Uri;

    invoke-virtual {v1, v2, v0}, Lcom/android/mail/browse/AttachmentCommandHandler;->sendCommand(Landroid/net/Uri;Landroid/content/ContentValues;)V

    return-void
.end method

.method public initialize(Landroid/app/FragmentManager;)V
    .locals 0
    .param p1    # Landroid/app/FragmentManager;

    iput-object p1, p0, Lcom/android/mail/browse/AttachmentActionHandler;->mFragmentManager:Landroid/app/FragmentManager;

    return-void
.end method

.method public isProgressDialogVisible()Z
    .locals 3

    iget-object v1, p0, Lcom/android/mail/browse/AttachmentActionHandler;->mFragmentManager:Landroid/app/FragmentManager;

    const-string v2, "attachment-progress"

    invoke-virtual {v1, v2}, Landroid/app/FragmentManager;->findFragmentByTag(Ljava/lang/String;)Landroid/app/Fragment;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Landroid/app/Fragment;->isVisible()Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v1, 0x1

    :goto_0
    return v1

    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public setAttachment(Lcom/android/mail/providers/Attachment;)V
    .locals 0
    .param p1    # Lcom/android/mail/providers/Attachment;

    iput-object p1, p0, Lcom/android/mail/browse/AttachmentActionHandler;->mAttachment:Lcom/android/mail/providers/Attachment;

    return-void
.end method

.method public shareAttachment()V
    .locals 7

    iget-object v3, p0, Lcom/android/mail/browse/AttachmentActionHandler;->mAttachment:Lcom/android/mail/providers/Attachment;

    iget-object v3, v3, Lcom/android/mail/providers/Attachment;->contentUri:Landroid/net/Uri;

    if-nez v3, :cond_0

    :goto_0
    return-void

    :cond_0
    new-instance v1, Landroid/content/Intent;

    const-string v3, "android.intent.action.SEND"

    invoke-direct {v1, v3}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    const v3, 0x80001

    invoke-virtual {v1, v3}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    iget-object v3, p0, Lcom/android/mail/browse/AttachmentActionHandler;->mAttachment:Lcom/android/mail/providers/Attachment;

    iget-object v3, v3, Lcom/android/mail/providers/Attachment;->contentUri:Landroid/net/Uri;

    invoke-static {v3}, Lcom/android/mail/utils/Utils;->normalizeUri(Landroid/net/Uri;)Landroid/net/Uri;

    move-result-object v2

    const-string v3, "android.intent.extra.STREAM"

    invoke-virtual {v1, v3, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    iget-object v3, p0, Lcom/android/mail/browse/AttachmentActionHandler;->mAttachment:Lcom/android/mail/providers/Attachment;

    invoke-virtual {v3}, Lcom/android/mail/providers/Attachment;->getContentType()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lcom/android/mail/utils/Utils;->normalizeMimeType(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v3}, Landroid/content/Intent;->setType(Ljava/lang/String;)Landroid/content/Intent;

    :try_start_0
    iget-object v3, p0, Lcom/android/mail/browse/AttachmentActionHandler;->mContext:Landroid/content/Context;

    invoke-virtual {v3, v1}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V
    :try_end_0
    .catch Landroid/content/ActivityNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v0

    sget-object v3, Lcom/android/mail/browse/AttachmentActionHandler;->LOG_TAG:Ljava/lang/String;

    const-string v4, "Couldn\'t find Activity for intent"

    const/4 v5, 0x1

    new-array v5, v5, [Ljava/lang/Object;

    const/4 v6, 0x0

    aput-object v0, v5, v6

    invoke-static {v3, v4, v5}, Lcom/android/mail/utils/LogUtils;->e(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)I

    goto :goto_0
.end method

.method public shareAttachments(Ljava/util/ArrayList;)V
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Landroid/os/Parcelable;",
            ">;)V"
        }
    .end annotation

    new-instance v1, Landroid/content/Intent;

    const-string v2, "android.intent.action.SEND_MULTIPLE"

    invoke-direct {v1, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    const v2, 0x80001

    invoke-virtual {v1, v2}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    const-string v2, "image/*"

    invoke-virtual {v1, v2}, Landroid/content/Intent;->setType(Ljava/lang/String;)Landroid/content/Intent;

    const-string v2, "android.intent.extra.STREAM"

    invoke-virtual {v1, v2, p1}, Landroid/content/Intent;->putParcelableArrayListExtra(Ljava/lang/String;Ljava/util/ArrayList;)Landroid/content/Intent;

    :try_start_0
    iget-object v2, p0, Lcom/android/mail/browse/AttachmentActionHandler;->mContext:Landroid/content/Context;

    invoke-virtual {v2, v1}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V
    :try_end_0
    .catch Landroid/content/ActivityNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    return-void

    :catch_0
    move-exception v0

    sget-object v2, Lcom/android/mail/browse/AttachmentActionHandler;->LOG_TAG:Ljava/lang/String;

    const-string v3, "Couldn\'t find Activity for intent"

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    aput-object v0, v4, v5

    invoke-static {v2, v3, v4}, Lcom/android/mail/utils/LogUtils;->e(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)I

    goto :goto_0
.end method

.method public showAttachment(I)V
    .locals 1
    .param p1    # I

    iget-object v0, p0, Lcom/android/mail/browse/AttachmentActionHandler;->mView:Lcom/android/mail/browse/AttachmentViewInterface;

    if-nez v0, :cond_0

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/android/mail/browse/AttachmentActionHandler;->mAttachment:Lcom/android/mail/providers/Attachment;

    invoke-virtual {v0}, Lcom/android/mail/providers/Attachment;->isPresentLocally()Z

    move-result v0

    if-eqz v0, :cond_2

    if-eqz p1, :cond_1

    iget-object v0, p0, Lcom/android/mail/browse/AttachmentActionHandler;->mAttachment:Lcom/android/mail/providers/Attachment;

    iget v0, v0, Lcom/android/mail/providers/Attachment;->destination:I

    if-ne v0, p1, :cond_2

    :cond_1
    iget-object v0, p0, Lcom/android/mail/browse/AttachmentActionHandler;->mView:Lcom/android/mail/browse/AttachmentViewInterface;

    invoke-interface {v0}, Lcom/android/mail/browse/AttachmentViewInterface;->viewAttachment()V

    goto :goto_0

    :cond_2
    invoke-direct {p0, p1}, Lcom/android/mail/browse/AttachmentActionHandler;->showDownloadingDialog(I)V

    invoke-virtual {p0, p1}, Lcom/android/mail/browse/AttachmentActionHandler;->startDownloadingAttachment(I)V

    goto :goto_0
.end method

.method public startDownloadingAttachment(I)V
    .locals 2
    .param p1    # I

    const/4 v1, 0x0

    const/4 v0, 0x1

    invoke-virtual {p0, p1, v0, v1, v1}, Lcom/android/mail/browse/AttachmentActionHandler;->startDownloadingAttachment(IIIZ)V

    return-void
.end method

.method public startDownloadingAttachment(IIIZ)V
    .locals 6
    .param p1    # I
    .param p2    # I
    .param p3    # I
    .param p4    # Z

    iget-object v1, p0, Lcom/android/mail/browse/AttachmentActionHandler;->mAttachment:Lcom/android/mail/providers/Attachment;

    move-object v0, p0

    move v2, p1

    move v3, p2

    move v4, p3

    move v5, p4

    invoke-direct/range {v0 .. v5}, Lcom/android/mail/browse/AttachmentActionHandler;->startDownloadingAttachment(Lcom/android/mail/providers/Attachment;IIIZ)V

    return-void
.end method

.method public startRedownloadingAttachment(Lcom/android/mail/providers/Attachment;)V
    .locals 3
    .param p1    # Lcom/android/mail/providers/Attachment;

    iget v1, p1, Lcom/android/mail/providers/Attachment;->destination:I

    invoke-direct {p0, v1}, Lcom/android/mail/browse/AttachmentActionHandler;->showDownloadingDialog(I)V

    new-instance v0, Landroid/content/ContentValues;

    const/4 v1, 0x2

    invoke-direct {v0, v1}, Landroid/content/ContentValues;-><init>(I)V

    const-string v1, "state"

    const/4 v2, 0x4

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    const-string v1, "destination"

    iget v2, p1, Lcom/android/mail/providers/Attachment;->destination:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    iget-object v1, p0, Lcom/android/mail/browse/AttachmentActionHandler;->mCommandHandler:Lcom/android/mail/browse/AttachmentCommandHandler;

    iget-object v2, p1, Lcom/android/mail/providers/Attachment;->uri:Landroid/net/Uri;

    invoke-virtual {v1, v2, v0}, Lcom/android/mail/browse/AttachmentCommandHandler;->sendCommand(Landroid/net/Uri;Landroid/content/ContentValues;)V

    return-void
.end method

.method public updateStatus(Z)V
    .locals 5
    .param p1    # Z

    iget-object v3, p0, Lcom/android/mail/browse/AttachmentActionHandler;->mView:Lcom/android/mail/browse/AttachmentViewInterface;

    if-nez v3, :cond_0

    :goto_0
    return-void

    :cond_0
    iget-object v3, p0, Lcom/android/mail/browse/AttachmentActionHandler;->mAttachment:Lcom/android/mail/providers/Attachment;

    invoke-virtual {v3}, Lcom/android/mail/providers/Attachment;->shouldShowProgress()Z

    move-result v2

    iget-object v3, p0, Lcom/android/mail/browse/AttachmentActionHandler;->mFragmentManager:Landroid/app/FragmentManager;

    const-string v4, "attachment-progress"

    invoke-virtual {v3, v4}, Landroid/app/FragmentManager;->findFragmentByTag(Ljava/lang/String;)Landroid/app/Fragment;

    move-result-object v0

    check-cast v0, Lcom/android/mail/browse/AttachmentProgressDialogFragment;

    if-eqz v0, :cond_4

    iget-object v3, p0, Lcom/android/mail/browse/AttachmentActionHandler;->mAttachment:Lcom/android/mail/providers/Attachment;

    invoke-virtual {v0, v3}, Lcom/android/mail/browse/AttachmentProgressDialogFragment;->isShowingDialogForAttachment(Lcom/android/mail/providers/Attachment;)Z

    move-result v3

    if-eqz v3, :cond_4

    iget-object v3, p0, Lcom/android/mail/browse/AttachmentActionHandler;->mAttachment:Lcom/android/mail/providers/Attachment;

    iget v3, v3, Lcom/android/mail/providers/Attachment;->downloadedSize:I

    invoke-virtual {v0, v3}, Lcom/android/mail/browse/AttachmentProgressDialogFragment;->setProgress(I)V

    if-nez v2, :cond_3

    invoke-virtual {v0}, Lcom/android/mail/browse/AttachmentProgressDialogFragment;->isIndeterminate()Z

    move-result v3

    if-eqz v3, :cond_3

    const/4 v1, 0x1

    :goto_1
    invoke-virtual {v0, v1}, Lcom/android/mail/browse/AttachmentProgressDialogFragment;->setIndeterminate(Z)V

    if-eqz p1, :cond_1

    iget-object v3, p0, Lcom/android/mail/browse/AttachmentActionHandler;->mAttachment:Lcom/android/mail/providers/Attachment;

    invoke-virtual {v3}, Lcom/android/mail/providers/Attachment;->isDownloadFinishedOrFailed()Z

    move-result v3

    if-eqz v3, :cond_1

    iget-object v3, p0, Lcom/android/mail/browse/AttachmentActionHandler;->mHandler:Landroid/os/Handler;

    new-instance v4, Lcom/android/mail/browse/AttachmentActionHandler$1;

    invoke-direct {v4, p0, v0}, Lcom/android/mail/browse/AttachmentActionHandler$1;-><init>(Lcom/android/mail/browse/AttachmentActionHandler;Lcom/android/mail/browse/AttachmentProgressDialogFragment;)V

    invoke-virtual {v3, v4}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    :cond_1
    iget-object v3, p0, Lcom/android/mail/browse/AttachmentActionHandler;->mAttachment:Lcom/android/mail/providers/Attachment;

    iget v3, v3, Lcom/android/mail/providers/Attachment;->state:I

    const/4 v4, 0x3

    if-ne v3, v4, :cond_2

    iget-object v3, p0, Lcom/android/mail/browse/AttachmentActionHandler;->mView:Lcom/android/mail/browse/AttachmentViewInterface;

    invoke-interface {v3}, Lcom/android/mail/browse/AttachmentViewInterface;->viewAttachment()V

    :cond_2
    :goto_2
    iget-object v3, p0, Lcom/android/mail/browse/AttachmentActionHandler;->mView:Lcom/android/mail/browse/AttachmentViewInterface;

    invoke-interface {v3}, Lcom/android/mail/browse/AttachmentViewInterface;->onUpdateStatus()V

    goto :goto_0

    :cond_3
    const/4 v1, 0x0

    goto :goto_1

    :cond_4
    iget-object v3, p0, Lcom/android/mail/browse/AttachmentActionHandler;->mView:Lcom/android/mail/browse/AttachmentViewInterface;

    invoke-interface {v3, v2}, Lcom/android/mail/browse/AttachmentViewInterface;->updateProgress(Z)V

    goto :goto_2
.end method
