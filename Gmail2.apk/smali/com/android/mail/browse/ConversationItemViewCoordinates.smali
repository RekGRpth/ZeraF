.class public Lcom/android/mail/browse/ConversationItemViewCoordinates;
.super Ljava/lang/Object;
.source "ConversationItemViewCoordinates.java"


# static fields
.field private static COLOR_BLOCK_HEIGHT:I

.field private static COLOR_BLOCK_WIDTH:I

.field private static FOLDER_CELL_WIDTH:I

.field private static TOTAL_FOLDER_WIDTH:I

.field private static TOTAL_FOLDER_WIDTH_WIDE:I

.field private static sCache:Landroid/util/SparseArray;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/util/SparseArray",
            "<",
            "Lcom/android/mail/browse/ConversationItemViewCoordinates;",
            ">;"
        }
    .end annotation
.end field

.field private static sConversationHeights:[I

.field private static sPaint:Landroid/text/TextPaint;


# instance fields
.field checkmarkX:I

.field checkmarkY:I

.field contactImagesHeight:I

.field contactImagesWidth:I

.field contactImagesX:F

.field contactImagesY:F

.field dateAscent:I

.field dateFontSize:I

.field dateXEnd:I

.field dateY:I

.field foldersAscent:I

.field foldersFontSize:I

.field foldersHeight:I

.field foldersTextBottomPadding:I

.field foldersTopPadding:I

.field foldersXEnd:I

.field foldersY:I

.field inlinePersonalLevel:Z

.field paperclipY:I

.field personalLevelX:I

.field personalLevelY:I

.field replyStateX:I

.field replyStateY:I

.field sendersAscent:I

.field sendersFontSize:I

.field sendersLineCount:I

.field sendersLineHeight:I

.field sendersView:Landroid/widget/TextView;

.field sendersWidth:I

.field sendersX:I

.field sendersY:I

.field showColorBlock:Z

.field showFolders:Z

.field showPersonalLevel:Z

.field showReplyState:Z

.field starX:I

.field starY:I

.field subjectAscent:I

.field subjectFontSize:I

.field subjectLineCount:I

.field subjectWidth:I

.field subjectX:I

.field subjectY:I


# direct methods
.method static constructor <clinit>()V
    .locals 2

    const/4 v0, -0x1

    sput v0, Lcom/android/mail/browse/ConversationItemViewCoordinates;->TOTAL_FOLDER_WIDTH:I

    sput v0, Lcom/android/mail/browse/ConversationItemViewCoordinates;->TOTAL_FOLDER_WIDTH_WIDE:I

    sput v0, Lcom/android/mail/browse/ConversationItemViewCoordinates;->FOLDER_CELL_WIDTH:I

    sput v0, Lcom/android/mail/browse/ConversationItemViewCoordinates;->COLOR_BLOCK_WIDTH:I

    sput v0, Lcom/android/mail/browse/ConversationItemViewCoordinates;->COLOR_BLOCK_HEIGHT:I

    new-instance v0, Landroid/util/SparseArray;

    invoke-direct {v0}, Landroid/util/SparseArray;-><init>()V

    sput-object v0, Lcom/android/mail/browse/ConversationItemViewCoordinates;->sCache:Landroid/util/SparseArray;

    new-instance v0, Landroid/text/TextPaint;

    invoke-direct {v0}, Landroid/text/TextPaint;-><init>()V

    sput-object v0, Lcom/android/mail/browse/ConversationItemViewCoordinates;->sPaint:Landroid/text/TextPaint;

    sget-object v0, Lcom/android/mail/browse/ConversationItemViewCoordinates;->sPaint:Landroid/text/TextPaint;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/text/TextPaint;->setAntiAlias(Z)V

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static displaySendersInline(I)Z
    .locals 3
    .param p0    # I

    packed-switch p0, :pswitch_data_0

    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Unknown conversation header view mode "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    :pswitch_0
    const/4 v0, 0x0

    :goto_0
    return v0

    :pswitch_1
    const/4 v0, 0x1

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public static forWidth(Landroid/content/Context;IIIZ)Lcom/android/mail/browse/ConversationItemViewCoordinates;
    .locals 25
    .param p0    # Landroid/content/Context;
    .param p1    # I
    .param p2    # I
    .param p3    # I
    .param p4    # Z

    sget-object v21, Lcom/android/mail/browse/ConversationItemViewCoordinates;->sCache:Landroid/util/SparseArray;

    const/16 v22, 0x3

    move/from16 v0, v22

    new-array v0, v0, [Ljava/lang/Object;

    move-object/from16 v22, v0

    const/16 v23, 0x0

    invoke-static/range {p1 .. p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v24

    aput-object v24, v22, v23

    const/16 v23, 0x1

    invoke-static/range {p2 .. p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v24

    aput-object v24, v22, v23

    const/16 v23, 0x2

    invoke-static/range {p4 .. p4}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v24

    aput-object v24, v22, v23

    invoke-static/range {v22 .. v22}, Lcom/google/common/base/Objects;->hashCode([Ljava/lang/Object;)I

    move-result v22

    invoke-virtual/range {v21 .. v22}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Lcom/android/mail/browse/ConversationItemViewCoordinates;

    if-nez v7, :cond_2

    new-instance v7, Lcom/android/mail/browse/ConversationItemViewCoordinates;

    invoke-direct {v7}, Lcom/android/mail/browse/ConversationItemViewCoordinates;-><init>()V

    sget-object v21, Lcom/android/mail/browse/ConversationItemViewCoordinates;->sCache:Landroid/util/SparseArray;

    const/16 v22, 0x3

    move/from16 v0, v22

    new-array v0, v0, [Ljava/lang/Object;

    move-object/from16 v22, v0

    const/16 v23, 0x0

    invoke-static/range {p1 .. p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v24

    aput-object v24, v22, v23

    const/16 v23, 0x1

    invoke-static/range {p2 .. p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v24

    aput-object v24, v22, v23

    const/16 v23, 0x2

    invoke-static/range {p4 .. p4}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v24

    aput-object v24, v22, v23

    invoke-static/range {v22 .. v22}, Lcom/google/common/base/Objects;->hashCode([Ljava/lang/Object;)I

    move-result v22

    move-object/from16 v0, v21

    move/from16 v1, v22

    invoke-virtual {v0, v1, v7}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    move-object/from16 v0, p0

    move/from16 v1, p2

    invoke-static {v0, v1}, Lcom/android/mail/browse/ConversationItemViewCoordinates;->getHeight(Landroid/content/Context;I)I

    move-result v10

    invoke-static/range {p0 .. p0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v21

    move/from16 v0, p2

    move/from16 v1, p4

    invoke-static {v0, v1}, Lcom/android/mail/browse/ConversationItemViewCoordinates;->getLayoutId(IZ)I

    move-result v22

    const/16 v23, 0x0

    invoke-virtual/range {v21 .. v23}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v19

    const/high16 v21, 0x40000000

    move/from16 v0, p1

    move/from16 v1, v21

    invoke-static {v0, v1}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v20

    const/high16 v21, 0x40000000

    move/from16 v0, v21

    invoke-static {v10, v0}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v11

    invoke-virtual/range {p0 .. p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v15

    move-object/from16 v0, v19

    move/from16 v1, v20

    invoke-virtual {v0, v1, v11}, Landroid/view/View;->measure(II)V

    const/16 v21, 0x0

    const/16 v22, 0x0

    move-object/from16 v0, v19

    move/from16 v1, v21

    move/from16 v2, v22

    move/from16 v3, p1

    invoke-virtual {v0, v1, v2, v3, v10}, Landroid/view/View;->layout(IIII)V

    const v21, 0x7f080056

    move-object/from16 v0, v19

    move/from16 v1, v21

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    invoke-static {v4}, Lcom/android/mail/browse/ConversationItemViewCoordinates;->getX(Landroid/view/View;)I

    move-result v21

    move/from16 v0, v21

    iput v0, v7, Lcom/android/mail/browse/ConversationItemViewCoordinates;->checkmarkX:I

    invoke-static {v4}, Lcom/android/mail/browse/ConversationItemViewCoordinates;->getY(Landroid/view/View;)I

    move-result v21

    move/from16 v0, v21

    iput v0, v7, Lcom/android/mail/browse/ConversationItemViewCoordinates;->checkmarkY:I

    const v21, 0x7f080057

    move-object/from16 v0, v19

    move/from16 v1, v21

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v17

    invoke-static/range {v17 .. v17}, Lcom/android/mail/browse/ConversationItemViewCoordinates;->getX(Landroid/view/View;)I

    move-result v21

    move/from16 v0, v21

    iput v0, v7, Lcom/android/mail/browse/ConversationItemViewCoordinates;->starX:I

    invoke-static/range {v17 .. v17}, Lcom/android/mail/browse/ConversationItemViewCoordinates;->getY(Landroid/view/View;)I

    move-result v21

    move/from16 v0, v21

    iput v0, v7, Lcom/android/mail/browse/ConversationItemViewCoordinates;->starY:I

    const v21, 0x7f080006

    move-object/from16 v0, v19

    move/from16 v1, v21

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v13

    if-eqz v13, :cond_3

    const/16 v21, 0x1

    move/from16 v0, v21

    iput-boolean v0, v7, Lcom/android/mail/browse/ConversationItemViewCoordinates;->showPersonalLevel:Z

    invoke-static {v13}, Lcom/android/mail/browse/ConversationItemViewCoordinates;->getX(Landroid/view/View;)I

    move-result v21

    move/from16 v0, v21

    iput v0, v7, Lcom/android/mail/browse/ConversationItemViewCoordinates;->personalLevelX:I

    invoke-static {v13}, Lcom/android/mail/browse/ConversationItemViewCoordinates;->getY(Landroid/view/View;)I

    move-result v21

    move/from16 v0, v21

    iput v0, v7, Lcom/android/mail/browse/ConversationItemViewCoordinates;->personalLevelY:I

    const v21, 0x7f0f0006

    move/from16 v0, v21

    invoke-virtual {v15, v0}, Landroid/content/res/Resources;->getBoolean(I)Z

    move-result v21

    move/from16 v0, v21

    iput-boolean v0, v7, Lcom/android/mail/browse/ConversationItemViewCoordinates;->inlinePersonalLevel:Z

    :goto_0
    const v21, 0x7f080053

    move-object/from16 v0, v19

    move/from16 v1, v21

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v16

    check-cast v16, Landroid/widget/TextView;

    move-object/from16 v0, v16

    iput-object v0, v7, Lcom/android/mail/browse/ConversationItemViewCoordinates;->sendersView:Landroid/widget/TextView;

    invoke-static/range {v16 .. v16}, Lcom/android/mail/browse/ConversationItemViewCoordinates;->getX(Landroid/view/View;)I

    move-result v21

    move/from16 v0, v21

    iput v0, v7, Lcom/android/mail/browse/ConversationItemViewCoordinates;->sendersX:I

    invoke-static/range {v16 .. v16}, Lcom/android/mail/browse/ConversationItemViewCoordinates;->getY(Landroid/view/View;)I

    move-result v21

    move/from16 v0, v21

    iput v0, v7, Lcom/android/mail/browse/ConversationItemViewCoordinates;->sendersY:I

    invoke-virtual/range {v16 .. v16}, Landroid/widget/TextView;->getWidth()I

    move-result v21

    move/from16 v0, v21

    iput v0, v7, Lcom/android/mail/browse/ConversationItemViewCoordinates;->sendersWidth:I

    invoke-static/range {v16 .. v16}, Lcom/android/mail/browse/ConversationItemViewCoordinates;->getLineCount(Landroid/widget/TextView;)I

    move-result v21

    move/from16 v0, v21

    iput v0, v7, Lcom/android/mail/browse/ConversationItemViewCoordinates;->sendersLineCount:I

    invoke-virtual/range {v16 .. v16}, Landroid/widget/TextView;->getLineHeight()I

    move-result v21

    move/from16 v0, v21

    iput v0, v7, Lcom/android/mail/browse/ConversationItemViewCoordinates;->sendersLineHeight:I

    invoke-virtual/range {v16 .. v16}, Landroid/widget/TextView;->getTextSize()F

    move-result v21

    move/from16 v0, v21

    float-to-int v0, v0

    move/from16 v21, v0

    move/from16 v0, v21

    iput v0, v7, Lcom/android/mail/browse/ConversationItemViewCoordinates;->sendersFontSize:I

    sget-object v21, Lcom/android/mail/browse/ConversationItemViewCoordinates;->sPaint:Landroid/text/TextPaint;

    iget v0, v7, Lcom/android/mail/browse/ConversationItemViewCoordinates;->sendersFontSize:I

    move/from16 v22, v0

    move/from16 v0, v22

    int-to-float v0, v0

    move/from16 v22, v0

    invoke-virtual/range {v21 .. v22}, Landroid/text/TextPaint;->setTextSize(F)V

    sget-object v21, Lcom/android/mail/browse/ConversationItemViewCoordinates;->sPaint:Landroid/text/TextPaint;

    invoke-virtual/range {v21 .. v21}, Landroid/text/TextPaint;->ascent()F

    move-result v21

    move/from16 v0, v21

    float-to-int v0, v0

    move/from16 v21, v0

    move/from16 v0, v21

    iput v0, v7, Lcom/android/mail/browse/ConversationItemViewCoordinates;->sendersAscent:I

    const v21, 0x7f080042

    move-object/from16 v0, v19

    move/from16 v1, v21

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v18

    check-cast v18, Landroid/widget/TextView;

    invoke-static/range {v18 .. v18}, Lcom/android/mail/browse/ConversationItemViewCoordinates;->getX(Landroid/view/View;)I

    move-result v21

    move/from16 v0, v21

    iput v0, v7, Lcom/android/mail/browse/ConversationItemViewCoordinates;->subjectX:I

    invoke-static/range {v18 .. v18}, Lcom/android/mail/browse/ConversationItemViewCoordinates;->getY(Landroid/view/View;)I

    move-result v21

    move/from16 v0, v21

    iput v0, v7, Lcom/android/mail/browse/ConversationItemViewCoordinates;->subjectY:I

    invoke-virtual/range {v18 .. v18}, Landroid/widget/TextView;->getWidth()I

    move-result v21

    move/from16 v0, v21

    iput v0, v7, Lcom/android/mail/browse/ConversationItemViewCoordinates;->subjectWidth:I

    invoke-static/range {v18 .. v18}, Lcom/android/mail/browse/ConversationItemViewCoordinates;->getLineCount(Landroid/widget/TextView;)I

    move-result v21

    move/from16 v0, v21

    iput v0, v7, Lcom/android/mail/browse/ConversationItemViewCoordinates;->subjectLineCount:I

    invoke-virtual/range {v18 .. v18}, Landroid/widget/TextView;->getTextSize()F

    move-result v21

    move/from16 v0, v21

    float-to-int v0, v0

    move/from16 v21, v0

    move/from16 v0, v21

    iput v0, v7, Lcom/android/mail/browse/ConversationItemViewCoordinates;->subjectFontSize:I

    sget-object v21, Lcom/android/mail/browse/ConversationItemViewCoordinates;->sPaint:Landroid/text/TextPaint;

    iget v0, v7, Lcom/android/mail/browse/ConversationItemViewCoordinates;->subjectFontSize:I

    move/from16 v22, v0

    move/from16 v0, v22

    int-to-float v0, v0

    move/from16 v22, v0

    invoke-virtual/range {v21 .. v22}, Landroid/text/TextPaint;->setTextSize(F)V

    sget-object v21, Lcom/android/mail/browse/ConversationItemViewCoordinates;->sPaint:Landroid/text/TextPaint;

    invoke-virtual/range {v21 .. v21}, Landroid/text/TextPaint;->ascent()F

    move-result v21

    move/from16 v0, v21

    float-to-int v0, v0

    move/from16 v21, v0

    move/from16 v0, v21

    iput v0, v7, Lcom/android/mail/browse/ConversationItemViewCoordinates;->subjectAscent:I

    const v21, 0x7f080058

    move-object/from16 v0, v19

    move/from16 v1, v21

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v9

    if-eqz v9, :cond_4

    const/16 v21, 0x1

    move/from16 v0, v21

    iput-boolean v0, v7, Lcom/android/mail/browse/ConversationItemViewCoordinates;->showFolders:Z

    invoke-static {v9}, Lcom/android/mail/browse/ConversationItemViewCoordinates;->getX(Landroid/view/View;)I

    move-result v21

    invoke-virtual {v9}, Landroid/view/View;->getWidth()I

    move-result v22

    add-int v21, v21, v22

    move/from16 v0, v21

    iput v0, v7, Lcom/android/mail/browse/ConversationItemViewCoordinates;->foldersXEnd:I

    invoke-static {v9}, Lcom/android/mail/browse/ConversationItemViewCoordinates;->getY(Landroid/view/View;)I

    move-result v21

    move/from16 v0, v21

    iput v0, v7, Lcom/android/mail/browse/ConversationItemViewCoordinates;->foldersY:I

    invoke-virtual {v9}, Landroid/view/View;->getHeight()I

    move-result v21

    move/from16 v0, v21

    iput v0, v7, Lcom/android/mail/browse/ConversationItemViewCoordinates;->foldersHeight:I

    invoke-virtual {v9}, Landroid/view/View;->getPaddingTop()I

    move-result v21

    move/from16 v0, v21

    iput v0, v7, Lcom/android/mail/browse/ConversationItemViewCoordinates;->foldersTopPadding:I

    const v21, 0x7f0c0015

    move/from16 v0, v21

    invoke-virtual {v15, v0}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v21

    move/from16 v0, v21

    iput v0, v7, Lcom/android/mail/browse/ConversationItemViewCoordinates;->foldersTextBottomPadding:I

    instance-of v0, v9, Landroid/widget/TextView;

    move/from16 v21, v0

    if-eqz v21, :cond_0

    check-cast v9, Landroid/widget/TextView;

    invoke-virtual {v9}, Landroid/widget/TextView;->getTextSize()F

    move-result v21

    move/from16 v0, v21

    float-to-int v0, v0

    move/from16 v21, v0

    move/from16 v0, v21

    iput v0, v7, Lcom/android/mail/browse/ConversationItemViewCoordinates;->foldersFontSize:I

    sget-object v21, Lcom/android/mail/browse/ConversationItemViewCoordinates;->sPaint:Landroid/text/TextPaint;

    iget v0, v7, Lcom/android/mail/browse/ConversationItemViewCoordinates;->foldersFontSize:I

    move/from16 v22, v0

    move/from16 v0, v22

    int-to-float v0, v0

    move/from16 v22, v0

    invoke-virtual/range {v21 .. v22}, Landroid/text/TextPaint;->setTextSize(F)V

    sget-object v21, Lcom/android/mail/browse/ConversationItemViewCoordinates;->sPaint:Landroid/text/TextPaint;

    invoke-virtual/range {v21 .. v21}, Landroid/text/TextPaint;->ascent()F

    move-result v21

    move/from16 v0, v21

    float-to-int v0, v0

    move/from16 v21, v0

    move/from16 v0, v21

    iput v0, v7, Lcom/android/mail/browse/ConversationItemViewCoordinates;->foldersAscent:I

    :cond_0
    :goto_1
    const v21, 0x7f0800b2

    move-object/from16 v0, v19

    move/from16 v1, v21

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v5

    if-eqz v5, :cond_1

    const/16 v21, 0x1

    move/from16 v0, v21

    iput-boolean v0, v7, Lcom/android/mail/browse/ConversationItemViewCoordinates;->showColorBlock:Z

    :cond_1
    const v21, 0x7f080007

    move-object/from16 v0, v19

    move/from16 v1, v21

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v14

    if-eqz v14, :cond_5

    const/16 v21, 0x1

    move/from16 v0, v21

    iput-boolean v0, v7, Lcom/android/mail/browse/ConversationItemViewCoordinates;->showReplyState:Z

    invoke-static {v14}, Lcom/android/mail/browse/ConversationItemViewCoordinates;->getX(Landroid/view/View;)I

    move-result v21

    move/from16 v0, v21

    iput v0, v7, Lcom/android/mail/browse/ConversationItemViewCoordinates;->replyStateX:I

    invoke-static {v14}, Lcom/android/mail/browse/ConversationItemViewCoordinates;->getY(Landroid/view/View;)I

    move-result v21

    move/from16 v0, v21

    iput v0, v7, Lcom/android/mail/browse/ConversationItemViewCoordinates;->replyStateY:I

    :goto_2
    const v21, 0x7f080055

    move-object/from16 v0, v19

    move/from16 v1, v21

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v8

    check-cast v8, Landroid/widget/TextView;

    invoke-static {v8}, Lcom/android/mail/browse/ConversationItemViewCoordinates;->getX(Landroid/view/View;)I

    move-result v21

    invoke-virtual {v8}, Landroid/widget/TextView;->getWidth()I

    move-result v22

    add-int v21, v21, v22

    move/from16 v0, v21

    iput v0, v7, Lcom/android/mail/browse/ConversationItemViewCoordinates;->dateXEnd:I

    invoke-static {v8}, Lcom/android/mail/browse/ConversationItemViewCoordinates;->getY(Landroid/view/View;)I

    move-result v21

    move/from16 v0, v21

    iput v0, v7, Lcom/android/mail/browse/ConversationItemViewCoordinates;->dateY:I

    invoke-virtual {v8}, Landroid/widget/TextView;->getTextSize()F

    move-result v21

    move/from16 v0, v21

    float-to-int v0, v0

    move/from16 v21, v0

    move/from16 v0, v21

    iput v0, v7, Lcom/android/mail/browse/ConversationItemViewCoordinates;->dateFontSize:I

    sget-object v21, Lcom/android/mail/browse/ConversationItemViewCoordinates;->sPaint:Landroid/text/TextPaint;

    iget v0, v7, Lcom/android/mail/browse/ConversationItemViewCoordinates;->dateFontSize:I

    move/from16 v22, v0

    move/from16 v0, v22

    int-to-float v0, v0

    move/from16 v22, v0

    invoke-virtual/range {v21 .. v22}, Landroid/text/TextPaint;->setTextSize(F)V

    sget-object v21, Lcom/android/mail/browse/ConversationItemViewCoordinates;->sPaint:Landroid/text/TextPaint;

    invoke-virtual/range {v21 .. v21}, Landroid/text/TextPaint;->ascent()F

    move-result v21

    move/from16 v0, v21

    float-to-int v0, v0

    move/from16 v21, v0

    move/from16 v0, v21

    iput v0, v7, Lcom/android/mail/browse/ConversationItemViewCoordinates;->dateAscent:I

    const v21, 0x7f080054

    move-object/from16 v0, v19

    move/from16 v1, v21

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v12

    invoke-static {v12}, Lcom/android/mail/browse/ConversationItemViewCoordinates;->getY(Landroid/view/View;)I

    move-result v21

    move/from16 v0, v21

    iput v0, v7, Lcom/android/mail/browse/ConversationItemViewCoordinates;->paperclipY:I

    if-eqz p4, :cond_2

    const v21, 0x7f080009

    move-object/from16 v0, v19

    move/from16 v1, v21

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v6

    if-eqz v6, :cond_2

    invoke-virtual {v6}, Landroid/view/View;->getWidth()I

    move-result v21

    move/from16 v0, v21

    iput v0, v7, Lcom/android/mail/browse/ConversationItemViewCoordinates;->contactImagesWidth:I

    invoke-virtual {v6}, Landroid/view/View;->getHeight()I

    move-result v21

    move/from16 v0, v21

    iput v0, v7, Lcom/android/mail/browse/ConversationItemViewCoordinates;->contactImagesHeight:I

    invoke-static {v6}, Lcom/android/mail/browse/ConversationItemViewCoordinates;->getX(Landroid/view/View;)I

    move-result v21

    move/from16 v0, v21

    int-to-float v0, v0

    move/from16 v21, v0

    move/from16 v0, v21

    iput v0, v7, Lcom/android/mail/browse/ConversationItemViewCoordinates;->contactImagesX:F

    invoke-static {v6}, Lcom/android/mail/browse/ConversationItemViewCoordinates;->getY(Landroid/view/View;)I

    move-result v21

    move/from16 v0, v21

    int-to-float v0, v0

    move/from16 v21, v0

    move/from16 v0, v21

    iput v0, v7, Lcom/android/mail/browse/ConversationItemViewCoordinates;->contactImagesY:F

    :cond_2
    return-object v7

    :cond_3
    const/16 v21, 0x0

    move/from16 v0, v21

    iput-boolean v0, v7, Lcom/android/mail/browse/ConversationItemViewCoordinates;->showPersonalLevel:Z

    const/16 v21, 0x0

    move/from16 v0, v21

    iput-boolean v0, v7, Lcom/android/mail/browse/ConversationItemViewCoordinates;->inlinePersonalLevel:Z

    goto/16 :goto_0

    :cond_4
    const/16 v21, 0x0

    move/from16 v0, v21

    iput-boolean v0, v7, Lcom/android/mail/browse/ConversationItemViewCoordinates;->showFolders:Z

    goto/16 :goto_1

    :cond_5
    const/16 v21, 0x0

    move/from16 v0, v21

    iput-boolean v0, v7, Lcom/android/mail/browse/ConversationItemViewCoordinates;->showReplyState:Z

    goto/16 :goto_2
.end method

.method public static getColorBlockHeight(Landroid/content/Context;)I
    .locals 2
    .param p0    # Landroid/content/Context;

    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    sget v1, Lcom/android/mail/browse/ConversationItemViewCoordinates;->COLOR_BLOCK_HEIGHT:I

    if-gtz v1, :cond_0

    const v1, 0x7f0c0022

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    sput v1, Lcom/android/mail/browse/ConversationItemViewCoordinates;->COLOR_BLOCK_HEIGHT:I

    :cond_0
    sget v1, Lcom/android/mail/browse/ConversationItemViewCoordinates;->COLOR_BLOCK_HEIGHT:I

    return v1
.end method

.method public static getColorBlockWidth(Landroid/content/Context;)I
    .locals 2
    .param p0    # Landroid/content/Context;

    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    sget v1, Lcom/android/mail/browse/ConversationItemViewCoordinates;->COLOR_BLOCK_WIDTH:I

    if-gtz v1, :cond_0

    const v1, 0x7f0c0021

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    sput v1, Lcom/android/mail/browse/ConversationItemViewCoordinates;->COLOR_BLOCK_WIDTH:I

    :cond_0
    sget v1, Lcom/android/mail/browse/ConversationItemViewCoordinates;->COLOR_BLOCK_WIDTH:I

    return v1
.end method

.method public static getDensityDependentArray([IF)[I
    .locals 3
    .param p0    # [I
    .param p1    # F

    array-length v2, p0

    new-array v1, v2, [I

    const/4 v0, 0x0

    :goto_0
    array-length v2, p0

    if-ge v0, v2, :cond_0

    aget v2, p0, v0

    int-to-float v2, v2

    mul-float/2addr v2, p1

    float-to-int v2, v2

    aput v2, v1, v0

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_0
    return-object v1
.end method

.method public static getFolderCellWidth(Landroid/content/Context;II)I
    .locals 4
    .param p0    # Landroid/content/Context;
    .param p1    # I
    .param p2    # I

    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    sget v1, Lcom/android/mail/browse/ConversationItemViewCoordinates;->FOLDER_CELL_WIDTH:I

    if-gtz v1, :cond_0

    const v1, 0x7f0c002e

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    sput v1, Lcom/android/mail/browse/ConversationItemViewCoordinates;->FOLDER_CELL_WIDTH:I

    :cond_0
    packed-switch p1, :pswitch_data_0

    new-instance v1, Ljava/lang/IllegalArgumentException;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Unknown conversation header view mode "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v1

    :pswitch_0
    sget v1, Lcom/android/mail/browse/ConversationItemViewCoordinates;->FOLDER_CELL_WIDTH:I

    return v1

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_0
    .end packed-switch
.end method

.method public static getFoldersWidth(Landroid/content/Context;I)I
    .locals 4
    .param p0    # Landroid/content/Context;
    .param p1    # I

    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    sget v1, Lcom/android/mail/browse/ConversationItemViewCoordinates;->TOTAL_FOLDER_WIDTH:I

    if-gtz v1, :cond_0

    const v1, 0x7f0c001f

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    sput v1, Lcom/android/mail/browse/ConversationItemViewCoordinates;->TOTAL_FOLDER_WIDTH:I

    const v1, 0x7f0c0020

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    sput v1, Lcom/android/mail/browse/ConversationItemViewCoordinates;->TOTAL_FOLDER_WIDTH_WIDE:I

    :cond_0
    packed-switch p1, :pswitch_data_0

    new-instance v1, Ljava/lang/IllegalArgumentException;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Unknown conversation header view mode "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v1

    :pswitch_0
    sget v1, Lcom/android/mail/browse/ConversationItemViewCoordinates;->TOTAL_FOLDER_WIDTH_WIDE:I

    :goto_0
    return v1

    :pswitch_1
    sget v1, Lcom/android/mail/browse/ConversationItemViewCoordinates;->TOTAL_FOLDER_WIDTH:I

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public static getHeight(Landroid/content/Context;I)I
    .locals 3
    .param p0    # Landroid/content/Context;
    .param p1    # I

    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v2

    iget v0, v2, Landroid/util/DisplayMetrics;->scaledDensity:F

    sget-object v2, Lcom/android/mail/browse/ConversationItemViewCoordinates;->sConversationHeights:[I

    if-nez v2, :cond_0

    const/high16 v2, 0x7f0e0000

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getIntArray(I)[I

    move-result-object v2

    invoke-static {v2, v0}, Lcom/android/mail/browse/ConversationItemViewCoordinates;->getDensityDependentArray([IF)[I

    move-result-object v2

    sput-object v2, Lcom/android/mail/browse/ConversationItemViewCoordinates;->sConversationHeights:[I

    :cond_0
    sget-object v2, Lcom/android/mail/browse/ConversationItemViewCoordinates;->sConversationHeights:[I

    aget v2, v2, p1

    return v2
.end method

.method private static getLayoutId(IZ)I
    .locals 3
    .param p0    # I
    .param p1    # Z

    if-nez p1, :cond_0

    packed-switch p0, :pswitch_data_0

    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Unknown conversation header view mode "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    :pswitch_0
    const v0, 0x7f04001e

    :goto_0
    return v0

    :pswitch_1
    const v0, 0x7f04001b

    goto :goto_0

    :cond_0
    packed-switch p0, :pswitch_data_1

    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Unknown conversation header view mode "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    :pswitch_2
    const v0, 0x7f04001f

    goto :goto_0

    :pswitch_3
    const v0, 0x7f04001c

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
    .end packed-switch

    :pswitch_data_1
    .packed-switch 0x0
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method private static getLineCount(Landroid/widget/TextView;)I
    .locals 2
    .param p0    # Landroid/widget/TextView;

    invoke-virtual {p0}, Landroid/widget/TextView;->getHeight()I

    move-result v0

    int-to-float v0, v0

    invoke-virtual {p0}, Landroid/widget/TextView;->getLineHeight()I

    move-result v1

    int-to-float v1, v1

    div-float/2addr v0, v1

    invoke-static {v0}, Ljava/lang/Math;->round(F)I

    move-result v0

    return v0
.end method

.method public static getMinHeight(Landroid/content/Context;Lcom/android/mail/ui/ViewMode;)I
    .locals 3
    .param p0    # Landroid/content/Context;
    .param p1    # Lcom/android/mail/ui/ViewMode;

    invoke-static {p0, p1}, Lcom/android/mail/browse/ConversationItemViewCoordinates;->getMode(Landroid/content/Context;Lcom/android/mail/ui/ViewMode;)I

    move-result v0

    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    if-nez v0, :cond_0

    const v1, 0x7f0c005c

    :goto_0
    invoke-virtual {v2, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    return v1

    :cond_0
    const v1, 0x7f0c005b

    goto :goto_0
.end method

.method public static getMode(Landroid/content/Context;I)I
    .locals 2
    .param p0    # Landroid/content/Context;
    .param p1    # I

    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    packed-switch p1, :pswitch_data_0

    :pswitch_0
    const v1, 0x7f0b0017

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v1

    :goto_0
    return v1

    :pswitch_1
    const v1, 0x7f0b0016

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v1

    goto :goto_0

    :pswitch_2
    const v1, 0x7f0b0018

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v1

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x2
        :pswitch_1
        :pswitch_0
        :pswitch_2
    .end packed-switch
.end method

.method public static getMode(Landroid/content/Context;Lcom/android/mail/ui/ViewMode;)I
    .locals 1
    .param p0    # Landroid/content/Context;
    .param p1    # Lcom/android/mail/ui/ViewMode;

    invoke-virtual {p1}, Lcom/android/mail/ui/ViewMode;->getMode()I

    move-result v0

    invoke-static {p0, v0}, Lcom/android/mail/browse/ConversationItemViewCoordinates;->getMode(Landroid/content/Context;I)I

    move-result v0

    return v0
.end method

.method public static getSendersLength(Landroid/content/Context;IZ)I
    .locals 2
    .param p0    # Landroid/content/Context;
    .param p1    # I
    .param p2    # Z

    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    if-eqz p2, :cond_0

    const v1, 0x7f0e0001

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getIntArray(I)[I

    move-result-object v1

    aget v1, v1, p1

    :goto_0
    return v1

    :cond_0
    const v1, 0x7f0e0002

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getIntArray(I)[I

    move-result-object v1

    aget v1, v1, p1

    goto :goto_0
.end method

.method private static getX(Landroid/view/View;)I
    .locals 3
    .param p0    # Landroid/view/View;

    const/4 v1, 0x0

    :goto_0
    if-eqz p0, :cond_1

    invoke-virtual {p0}, Landroid/view/View;->getX()F

    move-result v2

    float-to-int v2, v2

    add-int/2addr v1, v2

    invoke-virtual {p0}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    if-eqz v0, :cond_0

    check-cast v0, Landroid/view/View;

    move-object p0, v0

    :goto_1
    goto :goto_0

    :cond_0
    const/4 p0, 0x0

    goto :goto_1

    :cond_1
    return v1
.end method

.method private static getY(Landroid/view/View;)I
    .locals 3
    .param p0    # Landroid/view/View;

    const/4 v1, 0x0

    :goto_0
    if-eqz p0, :cond_1

    invoke-virtual {p0}, Landroid/view/View;->getY()F

    move-result v2

    float-to-int v2, v2

    add-int/2addr v1, v2

    invoke-virtual {p0}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    if-eqz v0, :cond_0

    check-cast v0, Landroid/view/View;

    move-object p0, v0

    :goto_1
    goto :goto_0

    :cond_0
    const/4 p0, 0x0

    goto :goto_1

    :cond_1
    return v1
.end method

.method public static isWideMode(I)Z
    .locals 1
    .param p0    # I

    if-nez p0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static refreshConversationHeights(Landroid/content/Context;)V
    .locals 3
    .param p0    # Landroid/content/Context;

    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v2

    iget v0, v2, Landroid/util/DisplayMetrics;->scaledDensity:F

    const/high16 v2, 0x7f0e0000

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getIntArray(I)[I

    move-result-object v2

    invoke-static {v2, v0}, Lcom/android/mail/browse/ConversationItemViewCoordinates;->getDensityDependentArray([IF)[I

    move-result-object v2

    sput-object v2, Lcom/android/mail/browse/ConversationItemViewCoordinates;->sConversationHeights:[I

    return-void
.end method
