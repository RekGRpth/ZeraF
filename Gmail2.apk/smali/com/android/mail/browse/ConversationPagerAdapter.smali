.class public Lcom/android/mail/browse/ConversationPagerAdapter;
.super Lcom/android/mail/utils/FragmentStatePagerAdapter2;
.source "ConversationPagerAdapter.java"

# interfaces
.implements Lvedroid/support/v4/view/ViewPager$OnPageChangeListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/mail/browse/ConversationPagerAdapter$1;,
        Lcom/android/mail/browse/ConversationPagerAdapter$ListObserver;,
        Lcom/android/mail/browse/ConversationPagerAdapter$FolderObserver;
    }
.end annotation


# static fields
.field private static final BUNDLE_DETACHED_MODE:Ljava/lang/String;

.field private static final LOG_TAG:Ljava/lang/String;


# instance fields
.field private final mAccount:Lcom/android/mail/providers/Account;

.field private final mCommonFragmentArgs:Landroid/os/Bundle;

.field private mController:Lcom/android/mail/ui/ActivityController;

.field private mDetachedMode:Z

.field private final mFolder:Lcom/android/mail/providers/Folder;

.field private final mFolderObserver:Landroid/database/DataSetObserver;

.field private final mInitialConversation:Lcom/android/mail/providers/Conversation;

.field private mLastKnownCount:I

.field private final mListObserver:Landroid/database/DataSetObserver;

.field private mPager:Lvedroid/support/v4/view/ViewPager;

.field private mResources:Landroid/content/res/Resources;

.field private mSafeToNotify:Z

.field private mSanitizedHtml:Z

.field private mSingletonMode:Z

.field private mStopListeningMode:Z


# direct methods
.method static constructor <clinit>()V
    .locals 2

    invoke-static {}, Lcom/android/mail/utils/LogTag;->getLogTag()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/android/mail/browse/ConversationPagerAdapter;->LOG_TAG:Ljava/lang/String;

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-class v1, Lcom/android/mail/browse/ConversationPagerAdapter;

    invoke-virtual {v1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "-detachedmode"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/android/mail/browse/ConversationPagerAdapter;->BUNDLE_DETACHED_MODE:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Landroid/content/res/Resources;Landroid/app/FragmentManager;Lcom/android/mail/providers/Account;Lcom/android/mail/providers/Folder;Lcom/android/mail/providers/Conversation;)V
    .locals 3
    .param p1    # Landroid/content/res/Resources;
    .param p2    # Landroid/app/FragmentManager;
    .param p3    # Lcom/android/mail/providers/Account;
    .param p4    # Lcom/android/mail/providers/Folder;
    .param p5    # Lcom/android/mail/providers/Conversation;

    const/4 v2, 0x0

    const/4 v1, 0x0

    invoke-direct {p0, p2, v1}, Lcom/android/mail/utils/FragmentStatePagerAdapter2;-><init>(Landroid/app/FragmentManager;Z)V

    new-instance v0, Lcom/android/mail/browse/ConversationPagerAdapter$ListObserver;

    invoke-direct {v0, p0, v2}, Lcom/android/mail/browse/ConversationPagerAdapter$ListObserver;-><init>(Lcom/android/mail/browse/ConversationPagerAdapter;Lcom/android/mail/browse/ConversationPagerAdapter$1;)V

    iput-object v0, p0, Lcom/android/mail/browse/ConversationPagerAdapter;->mListObserver:Landroid/database/DataSetObserver;

    new-instance v0, Lcom/android/mail/browse/ConversationPagerAdapter$FolderObserver;

    invoke-direct {v0, p0, v2}, Lcom/android/mail/browse/ConversationPagerAdapter$FolderObserver;-><init>(Lcom/android/mail/browse/ConversationPagerAdapter;Lcom/android/mail/browse/ConversationPagerAdapter$1;)V

    iput-object v0, p0, Lcom/android/mail/browse/ConversationPagerAdapter;->mFolderObserver:Landroid/database/DataSetObserver;

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/android/mail/browse/ConversationPagerAdapter;->mSingletonMode:Z

    iput-boolean v1, p0, Lcom/android/mail/browse/ConversationPagerAdapter;->mDetachedMode:Z

    iput-boolean v1, p0, Lcom/android/mail/browse/ConversationPagerAdapter;->mStopListeningMode:Z

    iput-object p1, p0, Lcom/android/mail/browse/ConversationPagerAdapter;->mResources:Landroid/content/res/Resources;

    invoke-static {p3, p4}, Lcom/android/mail/ui/AbstractConversationViewFragment;->makeBasicArgs(Lcom/android/mail/providers/Account;Lcom/android/mail/providers/Folder;)Landroid/os/Bundle;

    move-result-object v0

    iput-object v0, p0, Lcom/android/mail/browse/ConversationPagerAdapter;->mCommonFragmentArgs:Landroid/os/Bundle;

    iput-object p5, p0, Lcom/android/mail/browse/ConversationPagerAdapter;->mInitialConversation:Lcom/android/mail/providers/Conversation;

    iput-object p3, p0, Lcom/android/mail/browse/ConversationPagerAdapter;->mAccount:Lcom/android/mail/providers/Account;

    iput-object p4, p0, Lcom/android/mail/browse/ConversationPagerAdapter;->mFolder:Lcom/android/mail/providers/Folder;

    iget-object v0, p0, Lcom/android/mail/browse/ConversationPagerAdapter;->mAccount:Lcom/android/mail/providers/Account;

    const/16 v1, 0x80

    invoke-virtual {v0, v1}, Lcom/android/mail/providers/Account;->supportsCapability(I)Z

    move-result v0

    iput-boolean v0, p0, Lcom/android/mail/browse/ConversationPagerAdapter;->mSanitizedHtml:Z

    return-void
.end method

.method private getConversationViewFragment(Lcom/android/mail/providers/Conversation;)Lcom/android/mail/ui/AbstractConversationViewFragment;
    .locals 1
    .param p1    # Lcom/android/mail/providers/Conversation;

    iget-boolean v0, p0, Lcom/android/mail/browse/ConversationPagerAdapter;->mSanitizedHtml:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/mail/browse/ConversationPagerAdapter;->mCommonFragmentArgs:Landroid/os/Bundle;

    invoke-static {v0, p1}, Lcom/android/mail/ui/ConversationViewFragment;->newInstance(Landroid/os/Bundle;Lcom/android/mail/providers/Conversation;)Lcom/android/mail/ui/ConversationViewFragment;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/android/mail/browse/ConversationPagerAdapter;->mCommonFragmentArgs:Landroid/os/Bundle;

    invoke-static {v0, p1}, Lcom/android/mail/ui/SecureConversationViewFragment;->newInstance(Landroid/os/Bundle;Lcom/android/mail/providers/Conversation;)Lcom/android/mail/ui/SecureConversationViewFragment;

    move-result-object v0

    goto :goto_0
.end method

.method private getCursor()Lcom/android/mail/browse/ConversationCursor;
    .locals 4

    const/4 v0, 0x0

    iget-boolean v1, p0, Lcom/android/mail/browse/ConversationPagerAdapter;->mDetachedMode:Z

    if-eqz v1, :cond_0

    :goto_0
    return-object v0

    :cond_0
    iget-object v1, p0, Lcom/android/mail/browse/ConversationPagerAdapter;->mController:Lcom/android/mail/ui/ActivityController;

    if-nez v1, :cond_1

    sget-object v1, Lcom/android/mail/browse/ConversationPagerAdapter;->LOG_TAG:Ljava/lang/String;

    const-string v2, "Pager adapter has a null controller. If the conversation view is going away, this is fine.  Otherwise, the state is inconsistent"

    const/4 v3, 0x0

    new-array v3, v3, [Ljava/lang/Object;

    invoke-static {v1, v2, v3}, Lcom/android/mail/utils/LogUtils;->i(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)I

    goto :goto_0

    :cond_1
    iget-object v0, p0, Lcom/android/mail/browse/ConversationPagerAdapter;->mController:Lcom/android/mail/ui/ActivityController;

    invoke-interface {v0}, Lcom/android/mail/ui/ActivityController;->getConversationListCursor()Lcom/android/mail/browse/ConversationCursor;

    move-result-object v0

    goto :goto_0
.end method

.method private getDefaultConversation()Lcom/android/mail/providers/Conversation;
    .locals 2

    iget-object v1, p0, Lcom/android/mail/browse/ConversationPagerAdapter;->mController:Lcom/android/mail/ui/ActivityController;

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/android/mail/browse/ConversationPagerAdapter;->mController:Lcom/android/mail/ui/ActivityController;

    invoke-interface {v1}, Lcom/android/mail/ui/ActivityController;->getCurrentConversation()Lcom/android/mail/providers/Conversation;

    move-result-object v0

    :goto_0
    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/android/mail/browse/ConversationPagerAdapter;->mInitialConversation:Lcom/android/mail/providers/Conversation;

    :cond_0
    return-object v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public finishUpdate(Landroid/view/ViewGroup;)V
    .locals 1
    .param p1    # Landroid/view/ViewGroup;

    invoke-super {p0, p1}, Lcom/android/mail/utils/FragmentStatePagerAdapter2;->finishUpdate(Landroid/view/ViewGroup;)V

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/android/mail/browse/ConversationPagerAdapter;->mSafeToNotify:Z

    return-void
.end method

.method public getConversationPosition(Lcom/android/mail/providers/Conversation;)I
    .locals 8
    .param p1    # Lcom/android/mail/providers/Conversation;

    const/4 v7, 0x1

    const/4 v2, -0x2

    const/4 v3, 0x0

    invoke-direct {p0}, Lcom/android/mail/browse/ConversationPagerAdapter;->getCursor()Lcom/android/mail/browse/ConversationCursor;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/android/mail/browse/ConversationPagerAdapter;->isPagingDisabled(Landroid/database/Cursor;)Z

    move-result v4

    if-eqz v4, :cond_2

    invoke-direct {p0}, Lcom/android/mail/browse/ConversationPagerAdapter;->getDefaultConversation()Lcom/android/mail/providers/Conversation;

    move-result-object v4

    if-eq p1, v4, :cond_1

    sget-object v4, Lcom/android/mail/browse/ConversationPagerAdapter;->LOG_TAG:Ljava/lang/String;

    const-string v5, "unable to find conversation in singleton mode. c=%s"

    new-array v6, v7, [Ljava/lang/Object;

    aput-object p1, v6, v3

    invoke-static {v4, v5, v6}, Lcom/android/mail/utils/LogUtils;->d(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)I

    :cond_0
    :goto_0
    return v2

    :cond_1
    move v2, v3

    goto :goto_0

    :cond_2
    if-eqz p1, :cond_0

    const/4 v2, -0x2

    iget-wide v4, p1, Lcom/android/mail/providers/Conversation;->id:J

    invoke-virtual {v0, v4, v5}, Lcom/android/mail/browse/ConversationCursor;->getConversationPosition(J)I

    move-result v1

    if-ltz v1, :cond_0

    sget-object v4, Lcom/android/mail/browse/ConversationPagerAdapter;->LOG_TAG:Ljava/lang/String;

    const-string v5, "pager adapter found repositioned convo %s at pos=%d"

    const/4 v6, 0x2

    new-array v6, v6, [Ljava/lang/Object;

    aput-object p1, v6, v3

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v6, v7

    invoke-static {v4, v5, v6}, Lcom/android/mail/utils/LogUtils;->d(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)I

    move v2, v1

    goto :goto_0
.end method

.method public getCount()I
    .locals 6

    const/4 v1, 0x1

    iget-boolean v2, p0, Lcom/android/mail/browse/ConversationPagerAdapter;->mStopListeningMode:Z

    if-eqz v2, :cond_0

    iget v1, p0, Lcom/android/mail/browse/ConversationPagerAdapter;->mLastKnownCount:I

    :goto_0
    return v1

    :cond_0
    invoke-direct {p0}, Lcom/android/mail/browse/ConversationPagerAdapter;->getCursor()Lcom/android/mail/browse/ConversationCursor;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/android/mail/browse/ConversationPagerAdapter;->isPagingDisabled(Landroid/database/Cursor;)Z

    move-result v2

    if-eqz v2, :cond_1

    sget-object v2, Lcom/android/mail/browse/ConversationPagerAdapter;->LOG_TAG:Ljava/lang/String;

    const-string v3, "IN CPA.getCount, returning 1 (effective singleton). cursor=%s"

    new-array v4, v1, [Ljava/lang/Object;

    const/4 v5, 0x0

    aput-object v0, v4, v5

    invoke-static {v2, v3, v4}, Lcom/android/mail/utils/LogUtils;->d(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)I

    goto :goto_0

    :cond_1
    invoke-interface {v0}, Landroid/database/Cursor;->getCount()I

    move-result v1

    goto :goto_0
.end method

.method public getItem(I)Landroid/app/Fragment;
    .locals 10
    .param p1    # I

    const/4 v9, 0x2

    const/4 v8, 0x1

    const/4 v7, 0x0

    invoke-direct {p0}, Lcom/android/mail/browse/ConversationPagerAdapter;->getCursor()Lcom/android/mail/browse/ConversationCursor;

    move-result-object v1

    invoke-virtual {p0, v1}, Lcom/android/mail/browse/ConversationPagerAdapter;->isPagingDisabled(Landroid/database/Cursor;)Z

    move-result v3

    if-eqz v3, :cond_1

    if-eqz p1, :cond_0

    sget-object v3, Lcom/android/mail/browse/ConversationPagerAdapter;->LOG_TAG:Ljava/lang/String;

    const-string v4, "pager cursor is null and position is non-zero: %d"

    new-array v5, v8, [Ljava/lang/Object;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    aput-object v6, v5, v7

    invoke-static {v3, v4, v5}, Lcom/android/mail/utils/LogUtils;->wtf(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)I

    :cond_0
    invoke-direct {p0}, Lcom/android/mail/browse/ConversationPagerAdapter;->getDefaultConversation()Lcom/android/mail/providers/Conversation;

    move-result-object v0

    iput v7, v0, Lcom/android/mail/providers/Conversation;->position:I

    :goto_0
    invoke-direct {p0, v0}, Lcom/android/mail/browse/ConversationPagerAdapter;->getConversationViewFragment(Lcom/android/mail/providers/Conversation;)Lcom/android/mail/ui/AbstractConversationViewFragment;

    move-result-object v2

    sget-object v3, Lcom/android/mail/browse/ConversationPagerAdapter;->LOG_TAG:Ljava/lang/String;

    const-string v4, "IN PagerAdapter.getItem, frag=%s conv=%s"

    new-array v5, v9, [Ljava/lang/Object;

    aput-object v2, v5, v7

    aput-object v0, v5, v8

    invoke-static {v3, v4, v5}, Lcom/android/mail/utils/LogUtils;->d(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)I

    :goto_1
    return-object v2

    :cond_1
    invoke-interface {v1, p1}, Landroid/database/Cursor;->moveToPosition(I)Z

    move-result v3

    if-nez v3, :cond_2

    sget-object v3, Lcom/android/mail/browse/ConversationPagerAdapter;->LOG_TAG:Ljava/lang/String;

    const-string v4, "unable to seek to ConversationCursor pos=%d (%s)"

    new-array v5, v9, [Ljava/lang/Object;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    aput-object v6, v5, v7

    aput-object v1, v5, v8

    invoke-static {v3, v4, v5}, Lcom/android/mail/utils/LogUtils;->wtf(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)I

    const/4 v2, 0x0

    goto :goto_1

    :cond_2
    new-instance v0, Lcom/android/mail/providers/Conversation;

    invoke-direct {v0, v1}, Lcom/android/mail/providers/Conversation;-><init>(Landroid/database/Cursor;)V

    iput p1, v0, Lcom/android/mail/providers/Conversation;->position:I

    goto :goto_0
.end method

.method public getItemPosition(Ljava/lang/Object;)I
    .locals 5
    .param p1    # Ljava/lang/Object;

    instance-of v1, p1, Lcom/android/mail/ui/AbstractConversationViewFragment;

    if-nez v1, :cond_0

    sget-object v1, Lcom/android/mail/browse/ConversationPagerAdapter;->LOG_TAG:Ljava/lang/String;

    const-string v2, "getItemPosition received unexpected item: %s"

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    aput-object p1, v3, v4

    invoke-static {v1, v2, v3}, Lcom/android/mail/utils/LogUtils;->wtf(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)I

    :cond_0
    move-object v0, p1

    check-cast v0, Lcom/android/mail/ui/AbstractConversationViewFragment;

    invoke-virtual {v0}, Lcom/android/mail/ui/AbstractConversationViewFragment;->getConversation()Lcom/android/mail/providers/Conversation;

    move-result-object v1

    invoke-virtual {p0, v1}, Lcom/android/mail/browse/ConversationPagerAdapter;->getConversationPosition(Lcom/android/mail/providers/Conversation;)I

    move-result v1

    return v1
.end method

.method public getPageTitle(I)Ljava/lang/CharSequence;
    .locals 10
    .param p1    # I

    iget-object v5, p0, Lcom/android/mail/browse/ConversationPagerAdapter;->mPager:Lvedroid/support/v4/view/ViewPager;

    invoke-virtual {v5}, Lvedroid/support/v4/view/ViewPager;->getCurrentItem()I

    move-result v0

    invoke-direct {p0}, Lcom/android/mail/browse/ConversationPagerAdapter;->getCursor()Lcom/android/mail/browse/ConversationCursor;

    move-result-object v1

    invoke-virtual {p0, v1}, Lcom/android/mail/browse/ConversationPagerAdapter;->isPagingDisabled(Landroid/database/Cursor;)Z

    move-result v5

    if-eqz v5, :cond_0

    const/4 v3, 0x0

    :goto_0
    return-object v3

    :cond_0
    if-ne p1, v0, :cond_2

    invoke-virtual {p0}, Lcom/android/mail/browse/ConversationPagerAdapter;->getCount()I

    move-result v4

    iget-object v5, p0, Lcom/android/mail/browse/ConversationPagerAdapter;->mController:Lcom/android/mail/ui/ActivityController;

    if-eqz v5, :cond_1

    iget-object v5, p0, Lcom/android/mail/browse/ConversationPagerAdapter;->mController:Lcom/android/mail/ui/ActivityController;

    invoke-interface {v5}, Lcom/android/mail/ui/ActivityController;->getFolder()Lcom/android/mail/providers/Folder;

    move-result-object v2

    if-eqz v2, :cond_1

    iget v5, v2, Lcom/android/mail/providers/Folder;->totalCount:I

    if-le v5, v4, :cond_1

    iget v4, v2, Lcom/android/mail/providers/Folder;->totalCount:I

    :cond_1
    iget-object v5, p0, Lcom/android/mail/browse/ConversationPagerAdapter;->mResources:Landroid/content/res/Resources;

    const v6, 0x7f0900be

    const/4 v7, 0x2

    new-array v7, v7, [Ljava/lang/Object;

    const/4 v8, 0x0

    add-int/lit8 v9, p1, 0x1

    invoke-static {v9}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v9

    aput-object v9, v7, v8

    const/4 v8, 0x1

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v9

    aput-object v9, v7, v8

    invoke-virtual {v5, v6, v7}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    goto :goto_0

    :cond_2
    iget-object v6, p0, Lcom/android/mail/browse/ConversationPagerAdapter;->mResources:Landroid/content/res/Resources;

    if-ge p1, v0, :cond_3

    const v5, 0x7f0900bf

    :goto_1
    invoke-virtual {v6, v5}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    goto :goto_0

    :cond_3
    const v5, 0x7f0900c0

    goto :goto_1
.end method

.method public isDetached()Z
    .locals 1

    iget-boolean v0, p0, Lcom/android/mail/browse/ConversationPagerAdapter;->mDetachedMode:Z

    return v0
.end method

.method public isPagingDisabled(Landroid/database/Cursor;)Z
    .locals 1
    .param p1    # Landroid/database/Cursor;

    iget-boolean v0, p0, Lcom/android/mail/browse/ConversationPagerAdapter;->mSingletonMode:Z

    if-nez v0, :cond_0

    iget-boolean v0, p0, Lcom/android/mail/browse/ConversationPagerAdapter;->mDetachedMode:Z

    if-nez v0, :cond_0

    if-nez p1, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isSingletonMode()Z
    .locals 1

    iget-boolean v0, p0, Lcom/android/mail/browse/ConversationPagerAdapter;->mSingletonMode:Z

    return v0
.end method

.method public matches(Lcom/android/mail/providers/Account;Lcom/android/mail/providers/Folder;)Z
    .locals 1
    .param p1    # Lcom/android/mail/providers/Account;
    .param p2    # Lcom/android/mail/providers/Folder;

    iget-object v0, p0, Lcom/android/mail/browse/ConversationPagerAdapter;->mAccount:Lcom/android/mail/providers/Account;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/mail/browse/ConversationPagerAdapter;->mFolder:Lcom/android/mail/providers/Folder;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/mail/browse/ConversationPagerAdapter;->mAccount:Lcom/android/mail/providers/Account;

    invoke-virtual {v0, p1}, Lcom/android/mail/providers/Account;->matches(Lcom/android/mail/providers/Account;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/mail/browse/ConversationPagerAdapter;->mFolder:Lcom/android/mail/providers/Folder;

    invoke-virtual {v0, p2}, Lcom/android/mail/providers/Folder;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public notifyDataSetChanged()V
    .locals 13

    const/4 v3, 0x0

    const/4 v8, 0x0

    const/4 v7, 0x1

    iget-boolean v9, p0, Lcom/android/mail/browse/ConversationPagerAdapter;->mSafeToNotify:Z

    if-nez v9, :cond_1

    sget-object v7, Lcom/android/mail/browse/ConversationPagerAdapter;->LOG_TAG:Ljava/lang/String;

    const-string v9, "IN PagerAdapter.notifyDataSetChanged, ignoring unsafe update"

    new-array v8, v8, [Ljava/lang/Object;

    invoke-static {v7, v9, v8}, Lcom/android/mail/utils/LogUtils;->d(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)I

    :cond_0
    :goto_0
    return-void

    :cond_1
    const/4 v5, 0x1

    iget-object v9, p0, Lcom/android/mail/browse/ConversationPagerAdapter;->mController:Lcom/android/mail/ui/ActivityController;

    if-eqz v9, :cond_2

    iget-boolean v9, p0, Lcom/android/mail/browse/ConversationPagerAdapter;->mDetachedMode:Z

    if-nez v9, :cond_2

    iget-object v9, p0, Lcom/android/mail/browse/ConversationPagerAdapter;->mPager:Lvedroid/support/v4/view/ViewPager;

    if-eqz v9, :cond_2

    iget-object v9, p0, Lcom/android/mail/browse/ConversationPagerAdapter;->mController:Lcom/android/mail/ui/ActivityController;

    invoke-interface {v9}, Lcom/android/mail/ui/ActivityController;->getCurrentConversation()Lcom/android/mail/providers/Conversation;

    move-result-object v1

    invoke-virtual {p0, v1}, Lcom/android/mail/browse/ConversationPagerAdapter;->getConversationPosition(Lcom/android/mail/providers/Conversation;)I

    move-result v6

    invoke-direct {p0}, Lcom/android/mail/browse/ConversationPagerAdapter;->getCursor()Lcom/android/mail/browse/ConversationCursor;

    move-result-object v2

    const/4 v9, -0x2

    if-ne v6, v9, :cond_5

    if-eqz v2, :cond_5

    if-eqz v1, :cond_5

    invoke-virtual {v1}, Lcom/android/mail/providers/Conversation;->getNumMessages()I

    move-result v9

    if-ne v9, v7, :cond_3

    invoke-virtual {v1}, Lcom/android/mail/providers/Conversation;->numDrafts()I

    move-result v9

    if-ne v9, v7, :cond_3

    move v4, v7

    :goto_1
    if-eqz v4, :cond_4

    sget-object v9, Lcom/android/mail/browse/ConversationPagerAdapter;->LOG_TAG:Ljava/lang/String;

    const-string v10, "CPA: current singleton draft conv is not in cursor, popping out. c=%s"

    new-array v11, v7, [Ljava/lang/Object;

    iget-object v12, v1, Lcom/android/mail/providers/Conversation;->uri:Landroid/net/Uri;

    aput-object v12, v11, v8

    invoke-static {v9, v10, v11}, Lcom/android/mail/utils/LogUtils;->i(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)I

    iget-object v8, p0, Lcom/android/mail/browse/ConversationPagerAdapter;->mController:Lcom/android/mail/ui/ActivityController;

    invoke-interface {v8, v3, v7}, Lcom/android/mail/ui/ActivityController;->onConversationSelected(Lcom/android/mail/providers/Conversation;Z)V

    const/4 v5, 0x0

    :cond_2
    :goto_2
    if-eqz v5, :cond_0

    invoke-super {p0}, Lcom/android/mail/utils/FragmentStatePagerAdapter2;->notifyDataSetChanged()V

    goto :goto_0

    :cond_3
    move v4, v8

    goto :goto_1

    :cond_4
    iput-boolean v7, p0, Lcom/android/mail/browse/ConversationPagerAdapter;->mDetachedMode:Z

    iget-object v9, p0, Lcom/android/mail/browse/ConversationPagerAdapter;->mController:Lcom/android/mail/ui/ActivityController;

    invoke-interface {v9}, Lcom/android/mail/ui/ActivityController;->setDetachedMode()V

    sget-object v9, Lcom/android/mail/browse/ConversationPagerAdapter;->LOG_TAG:Ljava/lang/String;

    const-string v10, "CPA: current conv is gone, reverting to detached mode. c=%s"

    new-array v7, v7, [Ljava/lang/Object;

    iget-object v11, v1, Lcom/android/mail/providers/Conversation;->uri:Landroid/net/Uri;

    aput-object v11, v7, v8

    invoke-static {v9, v10, v7}, Lcom/android/mail/utils/LogUtils;->i(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)I

    goto :goto_2

    :cond_5
    if-nez v2, :cond_6

    :goto_3
    if-eqz v3, :cond_2

    invoke-interface {v2, v6}, Landroid/database/Cursor;->moveToPosition(I)Z

    move-result v7

    if-eqz v7, :cond_2

    invoke-virtual {v3}, Lcom/android/mail/ui/AbstractConversationViewFragment;->isUserVisible()Z

    move-result v7

    if-eqz v7, :cond_2

    new-instance v0, Lcom/android/mail/providers/Conversation;

    invoke-direct {v0, v2}, Lcom/android/mail/providers/Conversation;-><init>(Landroid/database/Cursor;)V

    iput v6, v0, Lcom/android/mail/providers/Conversation;->position:I

    invoke-virtual {v3, v0}, Lcom/android/mail/ui/AbstractConversationViewFragment;->onConversationUpdated(Lcom/android/mail/providers/Conversation;)V

    iget-object v7, p0, Lcom/android/mail/browse/ConversationPagerAdapter;->mController:Lcom/android/mail/ui/ActivityController;

    invoke-interface {v7, v0}, Lcom/android/mail/ui/ActivityController;->setCurrentConversation(Lcom/android/mail/providers/Conversation;)V

    goto :goto_2

    :cond_6
    invoke-virtual {p0, v6}, Lcom/android/mail/browse/ConversationPagerAdapter;->getFragmentAt(I)Landroid/app/Fragment;

    move-result-object v7

    check-cast v7, Lcom/android/mail/ui/AbstractConversationViewFragment;

    move-object v3, v7

    goto :goto_3
.end method

.method public onPageScrollStateChanged(I)V
    .locals 0
    .param p1    # I

    return-void
.end method

.method public onPageScrolled(IFI)V
    .locals 0
    .param p1    # I
    .param p2    # F
    .param p3    # I

    return-void
.end method

.method public onPageSelected(I)V
    .locals 6
    .param p1    # I

    iget-object v2, p0, Lcom/android/mail/browse/ConversationPagerAdapter;->mController:Lcom/android/mail/ui/ActivityController;

    if-nez v2, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    invoke-direct {p0}, Lcom/android/mail/browse/ConversationPagerAdapter;->getCursor()Lcom/android/mail/browse/ConversationCursor;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-interface {v1, p1}, Landroid/database/Cursor;->moveToPosition(I)Z

    move-result v2

    if-eqz v2, :cond_0

    new-instance v0, Lcom/android/mail/providers/Conversation;

    invoke-direct {v0, v1}, Lcom/android/mail/providers/Conversation;-><init>(Landroid/database/Cursor;)V

    iput p1, v0, Lcom/android/mail/providers/Conversation;->position:I

    sget-object v2, Lcom/android/mail/browse/ConversationPagerAdapter;->LOG_TAG:Ljava/lang/String;

    const-string v3, "pager adapter setting current conv: %s"

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    aput-object v0, v4, v5

    invoke-static {v2, v3, v4}, Lcom/android/mail/utils/LogUtils;->d(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)I

    iget-object v2, p0, Lcom/android/mail/browse/ConversationPagerAdapter;->mController:Lcom/android/mail/ui/ActivityController;

    invoke-interface {v2, v0}, Lcom/android/mail/ui/ActivityController;->setCurrentConversation(Lcom/android/mail/providers/Conversation;)V

    goto :goto_0
.end method

.method public restoreState(Landroid/os/Parcelable;Ljava/lang/ClassLoader;)V
    .locals 5
    .param p1    # Landroid/os/Parcelable;
    .param p2    # Ljava/lang/ClassLoader;

    sget-object v1, Lcom/android/mail/browse/ConversationPagerAdapter;->LOG_TAG:Ljava/lang/String;

    const-string v2, "IN PagerAdapter.restoreState. this=%s"

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    aput-object p0, v3, v4

    invoke-static {v1, v2, v3}, Lcom/android/mail/utils/LogUtils;->d(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)I

    invoke-super {p0, p1, p2}, Lcom/android/mail/utils/FragmentStatePagerAdapter2;->restoreState(Landroid/os/Parcelable;Ljava/lang/ClassLoader;)V

    if-eqz p1, :cond_0

    move-object v0, p1

    check-cast v0, Landroid/os/Bundle;

    invoke-virtual {v0, p2}, Landroid/os/Bundle;->setClassLoader(Ljava/lang/ClassLoader;)V

    sget-object v1, Lcom/android/mail/browse/ConversationPagerAdapter;->BUNDLE_DETACHED_MODE:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v1

    iput-boolean v1, p0, Lcom/android/mail/browse/ConversationPagerAdapter;->mDetachedMode:Z

    :cond_0
    return-void
.end method

.method public saveState()Landroid/os/Parcelable;
    .locals 5

    sget-object v1, Lcom/android/mail/browse/ConversationPagerAdapter;->LOG_TAG:Ljava/lang/String;

    const-string v2, "IN PagerAdapter.saveState. this=%s"

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    aput-object p0, v3, v4

    invoke-static {v1, v2, v3}, Lcom/android/mail/utils/LogUtils;->d(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)I

    invoke-super {p0}, Lcom/android/mail/utils/FragmentStatePagerAdapter2;->saveState()Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Landroid/os/Bundle;

    if-nez v0, :cond_0

    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    :cond_0
    sget-object v1, Lcom/android/mail/browse/ConversationPagerAdapter;->BUNDLE_DETACHED_MODE:Ljava/lang/String;

    iget-boolean v2, p0, Lcom/android/mail/browse/ConversationPagerAdapter;->mDetachedMode:Z

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    return-object v0
.end method

.method public setActivityController(Lcom/android/mail/ui/ActivityController;)V
    .locals 2
    .param p1    # Lcom/android/mail/ui/ActivityController;

    iget-object v0, p0, Lcom/android/mail/browse/ConversationPagerAdapter;->mController:Lcom/android/mail/ui/ActivityController;

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/android/mail/browse/ConversationPagerAdapter;->mStopListeningMode:Z

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/android/mail/browse/ConversationPagerAdapter;->mController:Lcom/android/mail/ui/ActivityController;

    iget-object v1, p0, Lcom/android/mail/browse/ConversationPagerAdapter;->mListObserver:Landroid/database/DataSetObserver;

    invoke-interface {v0, v1}, Lcom/android/mail/ui/ActivityController;->unregisterConversationListObserver(Landroid/database/DataSetObserver;)V

    iget-object v0, p0, Lcom/android/mail/browse/ConversationPagerAdapter;->mController:Lcom/android/mail/ui/ActivityController;

    iget-object v1, p0, Lcom/android/mail/browse/ConversationPagerAdapter;->mFolderObserver:Landroid/database/DataSetObserver;

    invoke-interface {v0, v1}, Lcom/android/mail/ui/ActivityController;->unregisterFolderObserver(Landroid/database/DataSetObserver;)V

    :cond_0
    iput-object p1, p0, Lcom/android/mail/browse/ConversationPagerAdapter;->mController:Lcom/android/mail/ui/ActivityController;

    iget-object v0, p0, Lcom/android/mail/browse/ConversationPagerAdapter;->mController:Lcom/android/mail/ui/ActivityController;

    if-eqz v0, :cond_1

    iget-boolean v0, p0, Lcom/android/mail/browse/ConversationPagerAdapter;->mStopListeningMode:Z

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/android/mail/browse/ConversationPagerAdapter;->mController:Lcom/android/mail/ui/ActivityController;

    iget-object v1, p0, Lcom/android/mail/browse/ConversationPagerAdapter;->mListObserver:Landroid/database/DataSetObserver;

    invoke-interface {v0, v1}, Lcom/android/mail/ui/ActivityController;->registerConversationListObserver(Landroid/database/DataSetObserver;)V

    iget-object v0, p0, Lcom/android/mail/browse/ConversationPagerAdapter;->mController:Lcom/android/mail/ui/ActivityController;

    iget-object v1, p0, Lcom/android/mail/browse/ConversationPagerAdapter;->mFolderObserver:Landroid/database/DataSetObserver;

    invoke-interface {v0, v1}, Lcom/android/mail/ui/ActivityController;->registerFolderObserver(Landroid/database/DataSetObserver;)V

    invoke-virtual {p0}, Lcom/android/mail/browse/ConversationPagerAdapter;->notifyDataSetChanged()V

    :cond_1
    return-void
.end method

.method public setItemVisible(Landroid/app/Fragment;Z)V
    .locals 1
    .param p1    # Landroid/app/Fragment;
    .param p2    # Z

    invoke-super {p0, p1, p2}, Lcom/android/mail/utils/FragmentStatePagerAdapter2;->setItemVisible(Landroid/app/Fragment;Z)V

    move-object v0, p1

    check-cast v0, Lcom/android/mail/ui/AbstractConversationViewFragment;

    invoke-virtual {v0, p2}, Lcom/android/mail/ui/AbstractConversationViewFragment;->setExtraUserVisibleHint(Z)V

    return-void
.end method

.method public setPager(Lvedroid/support/v4/view/ViewPager;)V
    .locals 2
    .param p1    # Lvedroid/support/v4/view/ViewPager;

    iget-object v0, p0, Lcom/android/mail/browse/ConversationPagerAdapter;->mPager:Lvedroid/support/v4/view/ViewPager;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/mail/browse/ConversationPagerAdapter;->mPager:Lvedroid/support/v4/view/ViewPager;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lvedroid/support/v4/view/ViewPager;->setOnPageChangeListener(Lvedroid/support/v4/view/ViewPager$OnPageChangeListener;)V

    :cond_0
    iput-object p1, p0, Lcom/android/mail/browse/ConversationPagerAdapter;->mPager:Lvedroid/support/v4/view/ViewPager;

    iget-object v0, p0, Lcom/android/mail/browse/ConversationPagerAdapter;->mPager:Lvedroid/support/v4/view/ViewPager;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/android/mail/browse/ConversationPagerAdapter;->mPager:Lvedroid/support/v4/view/ViewPager;

    invoke-virtual {v0, p0}, Lvedroid/support/v4/view/ViewPager;->setOnPageChangeListener(Lvedroid/support/v4/view/ViewPager$OnPageChangeListener;)V

    :cond_1
    return-void
.end method

.method public setPrimaryItem(Landroid/view/ViewGroup;ILjava/lang/Object;)V
    .locals 5
    .param p1    # Landroid/view/ViewGroup;
    .param p2    # I
    .param p3    # Ljava/lang/Object;

    sget-object v0, Lcom/android/mail/browse/ConversationPagerAdapter;->LOG_TAG:Ljava/lang/String;

    const-string v1, "IN PagerAdapter.setPrimaryItem, pos=%d, frag=%s"

    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v2, v3

    const/4 v3, 0x1

    aput-object p3, v2, v3

    invoke-static {v0, v1, v2}, Lcom/android/mail/utils/LogUtils;->d(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)I

    invoke-super {p0, p1, p2, p3}, Lcom/android/mail/utils/FragmentStatePagerAdapter2;->setPrimaryItem(Landroid/view/ViewGroup;ILjava/lang/Object;)V

    return-void
.end method

.method public setSingletonMode(Z)V
    .locals 1
    .param p1    # Z

    iget-boolean v0, p0, Lcom/android/mail/browse/ConversationPagerAdapter;->mSingletonMode:Z

    if-eq v0, p1, :cond_0

    iput-boolean p1, p0, Lcom/android/mail/browse/ConversationPagerAdapter;->mSingletonMode:Z

    invoke-virtual {p0}, Lcom/android/mail/browse/ConversationPagerAdapter;->notifyDataSetChanged()V

    :cond_0
    return-void
.end method

.method public startUpdate(Landroid/view/ViewGroup;)V
    .locals 1
    .param p1    # Landroid/view/ViewGroup;

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/android/mail/browse/ConversationPagerAdapter;->mSafeToNotify:Z

    invoke-super {p0, p1}, Lcom/android/mail/utils/FragmentStatePagerAdapter2;->startUpdate(Landroid/view/ViewGroup;)V

    return-void
.end method

.method public stopListening()V
    .locals 2

    iget-boolean v0, p0, Lcom/android/mail/browse/ConversationPagerAdapter;->mStopListeningMode:Z

    if-eqz v0, :cond_0

    :goto_0
    return-void

    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/android/mail/browse/ConversationPagerAdapter;->mStopListeningMode:Z

    iget-object v0, p0, Lcom/android/mail/browse/ConversationPagerAdapter;->mController:Lcom/android/mail/ui/ActivityController;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/android/mail/browse/ConversationPagerAdapter;->mController:Lcom/android/mail/ui/ActivityController;

    iget-object v1, p0, Lcom/android/mail/browse/ConversationPagerAdapter;->mListObserver:Landroid/database/DataSetObserver;

    invoke-interface {v0, v1}, Lcom/android/mail/ui/ActivityController;->unregisterConversationListObserver(Landroid/database/DataSetObserver;)V

    iget-object v0, p0, Lcom/android/mail/browse/ConversationPagerAdapter;->mController:Lcom/android/mail/ui/ActivityController;

    iget-object v1, p0, Lcom/android/mail/browse/ConversationPagerAdapter;->mFolderObserver:Landroid/database/DataSetObserver;

    invoke-interface {v0, v1}, Lcom/android/mail/ui/ActivityController;->unregisterFolderObserver(Landroid/database/DataSetObserver;)V

    :cond_1
    invoke-virtual {p0}, Lcom/android/mail/browse/ConversationPagerAdapter;->getCount()I

    move-result v0

    iput v0, p0, Lcom/android/mail/browse/ConversationPagerAdapter;->mLastKnownCount:I

    goto :goto_0
.end method
