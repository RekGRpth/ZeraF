.class public Lcom/android/mail/browse/SpamWarningView;
.super Landroid/widget/RelativeLayout;
.source "SpamWarningView.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field private final mHighWarningColor:I

.field private final mLowWarningColor:I

.field private mSpamWarningIcon:Landroid/widget/ImageView;

.field private mSpamWarningLink:Landroid/widget/TextView;

.field private mSpamWarningText:Landroid/widget/TextView;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1    # Landroid/content/Context;

    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/android/mail/browse/SpamWarningView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 2
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/util/AttributeSet;

    invoke-direct {p0, p1, p2}, Landroid/widget/RelativeLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    invoke-virtual {p0}, Lcom/android/mail/browse/SpamWarningView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0a0029

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    iput v0, p0, Lcom/android/mail/browse/SpamWarningView;->mHighWarningColor:I

    invoke-virtual {p0}, Lcom/android/mail/browse/SpamWarningView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0a0022

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    iput v0, p0, Lcom/android/mail/browse/SpamWarningView;->mLowWarningColor:I

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 1
    .param p1    # Landroid/view/View;

    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    :pswitch_0
    return-void

    :pswitch_data_0
    .packed-switch 0x7f08008e
        :pswitch_0
    .end packed-switch
.end method

.method public onFinishInflate()V
    .locals 1

    invoke-virtual {p0, p0}, Lcom/android/mail/browse/SpamWarningView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    const v0, 0x7f08008f

    invoke-virtual {p0, v0}, Lcom/android/mail/browse/SpamWarningView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/android/mail/browse/SpamWarningView;->mSpamWarningIcon:Landroid/widget/ImageView;

    const v0, 0x7f080090

    invoke-virtual {p0, v0}, Lcom/android/mail/browse/SpamWarningView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/android/mail/browse/SpamWarningView;->mSpamWarningText:Landroid/widget/TextView;

    const v0, 0x7f080091

    invoke-virtual {p0, v0}, Lcom/android/mail/browse/SpamWarningView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/android/mail/browse/SpamWarningView;->mSpamWarningLink:Landroid/widget/TextView;

    iget-object v0, p0, Lcom/android/mail/browse/SpamWarningView;->mSpamWarningLink:Landroid/widget/TextView;

    invoke-virtual {v0, p0}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    return-void
.end method

.method public showSpamWarning(Lcom/android/mail/providers/Message;Lcom/android/mail/providers/Address;)V
    .locals 9
    .param p1    # Lcom/android/mail/providers/Message;
    .param p2    # Lcom/android/mail/providers/Address;

    const/4 v8, 0x2

    const/4 v7, 0x0

    invoke-virtual {p0, v7}, Lcom/android/mail/browse/SpamWarningView;->setVisibility(I)V

    invoke-virtual {p2}, Lcom/android/mail/providers/Address;->getAddress()Ljava/lang/String;

    move-result-object v1

    const/16 v3, 0x40

    invoke-virtual {v1, v3}, Ljava/lang/String;->indexOf(I)I

    move-result v3

    add-int/lit8 v3, v3, 0x1

    invoke-virtual {v1, v3}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, Lcom/android/mail/browse/SpamWarningView;->mSpamWarningText:Landroid/widget/TextView;

    iget-object v4, p1, Lcom/android/mail/providers/Message;->spamWarningString:Ljava/lang/String;

    new-array v5, v8, [Ljava/lang/Object;

    aput-object v1, v5, v7

    const/4 v6, 0x1

    aput-object v2, v5, v6

    invoke-static {v4, v5}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Lcom/android/mail/utils/Utils;->convertHtmlToPlainText(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget v3, p1, Lcom/android/mail/providers/Message;->spamWarningLevel:I

    if-ne v3, v8, :cond_0

    iget-object v3, p0, Lcom/android/mail/browse/SpamWarningView;->mSpamWarningText:Landroid/widget/TextView;

    iget v4, p0, Lcom/android/mail/browse/SpamWarningView;->mHighWarningColor:I

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setTextColor(I)V

    iget-object v3, p0, Lcom/android/mail/browse/SpamWarningView;->mSpamWarningIcon:Landroid/widget/ImageView;

    const v4, 0x7f02002b

    invoke-virtual {v3, v4}, Landroid/widget/ImageView;->setImageResource(I)V

    :goto_0
    iget v0, p1, Lcom/android/mail/providers/Message;->spamLinkType:I

    packed-switch v0, :pswitch_data_0

    :goto_1
    return-void

    :cond_0
    iget-object v3, p0, Lcom/android/mail/browse/SpamWarningView;->mSpamWarningText:Landroid/widget/TextView;

    iget v4, p0, Lcom/android/mail/browse/SpamWarningView;->mLowWarningColor:I

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setTextColor(I)V

    iget-object v3, p0, Lcom/android/mail/browse/SpamWarningView;->mSpamWarningIcon:Landroid/widget/ImageView;

    const v4, 0x7f02002a

    invoke-virtual {v3, v4}, Landroid/widget/ImageView;->setImageResource(I)V

    goto :goto_0

    :pswitch_0
    iget-object v3, p0, Lcom/android/mail/browse/SpamWarningView;->mSpamWarningLink:Landroid/widget/TextView;

    const/16 v4, 0x8

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setVisibility(I)V

    goto :goto_1

    :pswitch_1
    iget-object v3, p0, Lcom/android/mail/browse/SpamWarningView;->mSpamWarningLink:Landroid/widget/TextView;

    invoke-virtual {v3, v7}, Landroid/widget/TextView;->setVisibility(I)V

    iget-object v3, p0, Lcom/android/mail/browse/SpamWarningView;->mSpamWarningLink:Landroid/widget/TextView;

    const v4, 0x7f090103

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setText(I)V

    goto :goto_1

    :pswitch_2
    iget-object v3, p0, Lcom/android/mail/browse/SpamWarningView;->mSpamWarningLink:Landroid/widget/TextView;

    invoke-virtual {v3, v7}, Landroid/widget/TextView;->setVisibility(I)V

    iget-object v3, p0, Lcom/android/mail/browse/SpamWarningView;->mSpamWarningLink:Landroid/widget/TextView;

    const v4, 0x7f090054

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setText(I)V

    goto :goto_1

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method
