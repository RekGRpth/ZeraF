.class Lcom/android/mail/browse/ConversationCursor$RefreshTask;
.super Landroid/os/AsyncTask;
.source "ConversationCursor.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/mail/browse/ConversationCursor;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "RefreshTask"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/os/AsyncTask",
        "<",
        "Ljava/lang/Void;",
        "Ljava/lang/Void;",
        "Ljava/lang/Void;",
        ">;"
    }
.end annotation


# instance fields
.field private mCursor:Lcom/android/mail/browse/ConversationCursor$UnderlyingCursorWrapper;

.field final synthetic this$0:Lcom/android/mail/browse/ConversationCursor;


# direct methods
.method private constructor <init>(Lcom/android/mail/browse/ConversationCursor;)V
    .locals 1

    iput-object p1, p0, Lcom/android/mail/browse/ConversationCursor$RefreshTask;->this$0:Lcom/android/mail/browse/ConversationCursor;

    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/android/mail/browse/ConversationCursor$RefreshTask;->mCursor:Lcom/android/mail/browse/ConversationCursor$UnderlyingCursorWrapper;

    return-void
.end method

.method synthetic constructor <init>(Lcom/android/mail/browse/ConversationCursor;Lcom/android/mail/browse/ConversationCursor$1;)V
    .locals 0
    .param p1    # Lcom/android/mail/browse/ConversationCursor;
    .param p2    # Lcom/android/mail/browse/ConversationCursor$1;

    invoke-direct {p0, p1}, Lcom/android/mail/browse/ConversationCursor$RefreshTask;-><init>(Lcom/android/mail/browse/ConversationCursor;)V

    return-void
.end method


# virtual methods
.method protected bridge synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1
    .param p1    # [Ljava/lang/Object;

    check-cast p1, [Ljava/lang/Void;

    invoke-virtual {p0, p1}, Lcom/android/mail/browse/ConversationCursor$RefreshTask;->doInBackground([Ljava/lang/Void;)Ljava/lang/Void;

    move-result-object v0

    return-object v0
.end method

.method protected varargs doInBackground([Ljava/lang/Void;)Ljava/lang/Void;
    .locals 2
    .param p1    # [Ljava/lang/Void;

    iget-object v0, p0, Lcom/android/mail/browse/ConversationCursor$RefreshTask;->this$0:Lcom/android/mail/browse/ConversationCursor;

    const/4 v1, 0x0

    # invokes: Lcom/android/mail/browse/ConversationCursor;->doQuery(Z)Lcom/android/mail/browse/ConversationCursor$UnderlyingCursorWrapper;
    invoke-static {v0, v1}, Lcom/android/mail/browse/ConversationCursor;->access$000(Lcom/android/mail/browse/ConversationCursor;Z)Lcom/android/mail/browse/ConversationCursor$UnderlyingCursorWrapper;

    move-result-object v0

    iput-object v0, p0, Lcom/android/mail/browse/ConversationCursor$RefreshTask;->mCursor:Lcom/android/mail/browse/ConversationCursor$UnderlyingCursorWrapper;

    iget-object v0, p0, Lcom/android/mail/browse/ConversationCursor$RefreshTask;->mCursor:Lcom/android/mail/browse/ConversationCursor$UnderlyingCursorWrapper;

    invoke-virtual {v0}, Lcom/android/mail/browse/ConversationCursor$UnderlyingCursorWrapper;->getCount()I

    const/4 v0, 0x0

    return-object v0
.end method

.method protected onCancelled()V
    .locals 1

    iget-object v0, p0, Lcom/android/mail/browse/ConversationCursor$RefreshTask;->mCursor:Lcom/android/mail/browse/ConversationCursor$UnderlyingCursorWrapper;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/mail/browse/ConversationCursor$RefreshTask;->mCursor:Lcom/android/mail/browse/ConversationCursor$UnderlyingCursorWrapper;

    invoke-virtual {v0}, Lcom/android/mail/browse/ConversationCursor$UnderlyingCursorWrapper;->close()V

    :cond_0
    return-void
.end method

.method protected bridge synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 0
    .param p1    # Ljava/lang/Object;

    check-cast p1, Ljava/lang/Void;

    invoke-virtual {p0, p1}, Lcom/android/mail/browse/ConversationCursor$RefreshTask;->onPostExecute(Ljava/lang/Void;)V

    return-void
.end method

.method protected onPostExecute(Ljava/lang/Void;)V
    .locals 8
    .param p1    # Ljava/lang/Void;

    const/4 v1, 0x0

    const/4 v0, 0x1

    iget-object v2, p0, Lcom/android/mail/browse/ConversationCursor$RefreshTask;->this$0:Lcom/android/mail/browse/ConversationCursor;

    # getter for: Lcom/android/mail/browse/ConversationCursor;->mCacheMapLock:Ljava/lang/Object;
    invoke-static {v2}, Lcom/android/mail/browse/ConversationCursor;->access$100(Lcom/android/mail/browse/ConversationCursor;)Ljava/lang/Object;

    move-result-object v2

    monitor-enter v2

    :try_start_0
    # getter for: Lcom/android/mail/browse/ConversationCursor;->LOG_TAG:Ljava/lang/String;
    invoke-static {}, Lcom/android/mail/browse/ConversationCursor;->access$200()Ljava/lang/String;

    move-result-object v3

    const-string v4, "Received notify ui callback and sending a notification is enabled? %s"

    const/4 v5, 0x1

    new-array v5, v5, [Ljava/lang/Object;

    const/4 v6, 0x0

    iget-object v7, p0, Lcom/android/mail/browse/ConversationCursor$RefreshTask;->this$0:Lcom/android/mail/browse/ConversationCursor;

    # getter for: Lcom/android/mail/browse/ConversationCursor;->mPaused:Z
    invoke-static {v7}, Lcom/android/mail/browse/ConversationCursor;->access$300(Lcom/android/mail/browse/ConversationCursor;)Z

    move-result v7

    if-nez v7, :cond_0

    iget-object v7, p0, Lcom/android/mail/browse/ConversationCursor$RefreshTask;->this$0:Lcom/android/mail/browse/ConversationCursor;

    # getter for: Lcom/android/mail/browse/ConversationCursor;->mDeferSync:Z
    invoke-static {v7}, Lcom/android/mail/browse/ConversationCursor;->access$400(Lcom/android/mail/browse/ConversationCursor;)Z

    move-result v7

    if-nez v7, :cond_0

    :goto_0
    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    aput-object v0, v5, v6

    invoke-static {v3, v4, v5}, Lcom/android/mail/utils/LogUtils;->d(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)I

    iget-object v0, p0, Lcom/android/mail/browse/ConversationCursor$RefreshTask;->this$0:Lcom/android/mail/browse/ConversationCursor;

    invoke-virtual {v0}, Lcom/android/mail/browse/ConversationCursor;->isClosed()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-virtual {p0}, Lcom/android/mail/browse/ConversationCursor$RefreshTask;->onCancelled()V

    monitor-exit v2

    :goto_1
    return-void

    :cond_0
    move v0, v1

    goto :goto_0

    :cond_1
    iget-object v0, p0, Lcom/android/mail/browse/ConversationCursor$RefreshTask;->this$0:Lcom/android/mail/browse/ConversationCursor;

    iget-object v1, p0, Lcom/android/mail/browse/ConversationCursor$RefreshTask;->mCursor:Lcom/android/mail/browse/ConversationCursor$UnderlyingCursorWrapper;

    # setter for: Lcom/android/mail/browse/ConversationCursor;->mRequeryCursor:Lcom/android/mail/browse/ConversationCursor$UnderlyingCursorWrapper;
    invoke-static {v0, v1}, Lcom/android/mail/browse/ConversationCursor;->access$502(Lcom/android/mail/browse/ConversationCursor;Lcom/android/mail/browse/ConversationCursor$UnderlyingCursorWrapper;)Lcom/android/mail/browse/ConversationCursor$UnderlyingCursorWrapper;

    iget-object v0, p0, Lcom/android/mail/browse/ConversationCursor$RefreshTask;->this$0:Lcom/android/mail/browse/ConversationCursor;

    const/4 v1, 0x1

    # setter for: Lcom/android/mail/browse/ConversationCursor;->mRefreshReady:Z
    invoke-static {v0, v1}, Lcom/android/mail/browse/ConversationCursor;->access$602(Lcom/android/mail/browse/ConversationCursor;Z)Z

    iget-object v0, p0, Lcom/android/mail/browse/ConversationCursor$RefreshTask;->this$0:Lcom/android/mail/browse/ConversationCursor;

    # getter for: Lcom/android/mail/browse/ConversationCursor;->mDeferSync:Z
    invoke-static {v0}, Lcom/android/mail/browse/ConversationCursor;->access$400(Lcom/android/mail/browse/ConversationCursor;)Z

    move-result v0

    if-nez v0, :cond_2

    iget-object v0, p0, Lcom/android/mail/browse/ConversationCursor$RefreshTask;->this$0:Lcom/android/mail/browse/ConversationCursor;

    # getter for: Lcom/android/mail/browse/ConversationCursor;->mPaused:Z
    invoke-static {v0}, Lcom/android/mail/browse/ConversationCursor;->access$300(Lcom/android/mail/browse/ConversationCursor;)Z

    move-result v0

    if-nez v0, :cond_2

    iget-object v0, p0, Lcom/android/mail/browse/ConversationCursor$RefreshTask;->this$0:Lcom/android/mail/browse/ConversationCursor;

    # invokes: Lcom/android/mail/browse/ConversationCursor;->notifyRefreshReady()V
    invoke-static {v0}, Lcom/android/mail/browse/ConversationCursor;->access$700(Lcom/android/mail/browse/ConversationCursor;)V

    :cond_2
    monitor-exit v2

    goto :goto_1

    :catchall_0
    move-exception v0

    monitor-exit v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method
