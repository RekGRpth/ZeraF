.class public Lcom/android/mail/browse/SelectedConversationsActionMenu;
.super Ljava/lang/Object;
.source "SelectedConversationsActionMenu.java"

# interfaces
.implements Landroid/view/ActionMode$Callback;
.implements Lcom/android/mail/ui/ConversationSetObserver;


# static fields
.field private static final LOG_TAG:Ljava/lang/String;


# instance fields
.field private mAccount:Lcom/android/mail/providers/Account;

.field private mAccountObserver:Lcom/android/mail/providers/AccountObserver;

.field private mActionMode:Landroid/view/ActionMode;

.field private mActivated:Z

.field private final mActivity:Lcom/android/mail/ui/ControllableActivity;

.field private final mContext:Landroid/content/Context;

.field private final mFolder:Lcom/android/mail/providers/Folder;

.field private final mListController:Lcom/android/mail/ui/ConversationListCallbacks;

.field private mMenu:Landroid/view/Menu;

.field protected final mSelectionSet:Lcom/android/mail/ui/ConversationSelectionSet;

.field private final mUpdater:Lcom/android/mail/ui/ConversationUpdater;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    invoke-static {}, Lcom/android/mail/utils/LogTag;->getLogTag()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/android/mail/browse/SelectedConversationsActionMenu;->LOG_TAG:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Lcom/android/mail/ui/ControllableActivity;Lcom/android/mail/ui/ConversationSelectionSet;Lcom/android/mail/providers/Folder;)V
    .locals 2
    .param p1    # Lcom/android/mail/ui/ControllableActivity;
    .param p2    # Lcom/android/mail/ui/ConversationSelectionSet;
    .param p3    # Lcom/android/mail/providers/Folder;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/android/mail/browse/SelectedConversationsActionMenu;->mActivated:Z

    iput-object p1, p0, Lcom/android/mail/browse/SelectedConversationsActionMenu;->mActivity:Lcom/android/mail/ui/ControllableActivity;

    invoke-interface {p1}, Lcom/android/mail/ui/ControllableActivity;->getListHandler()Lcom/android/mail/ui/ConversationListCallbacks;

    move-result-object v0

    iput-object v0, p0, Lcom/android/mail/browse/SelectedConversationsActionMenu;->mListController:Lcom/android/mail/ui/ConversationListCallbacks;

    iput-object p2, p0, Lcom/android/mail/browse/SelectedConversationsActionMenu;->mSelectionSet:Lcom/android/mail/ui/ConversationSelectionSet;

    new-instance v0, Lcom/android/mail/browse/SelectedConversationsActionMenu$1;

    invoke-direct {v0, p0}, Lcom/android/mail/browse/SelectedConversationsActionMenu$1;-><init>(Lcom/android/mail/browse/SelectedConversationsActionMenu;)V

    iput-object v0, p0, Lcom/android/mail/browse/SelectedConversationsActionMenu;->mAccountObserver:Lcom/android/mail/providers/AccountObserver;

    iget-object v0, p0, Lcom/android/mail/browse/SelectedConversationsActionMenu;->mAccountObserver:Lcom/android/mail/providers/AccountObserver;

    invoke-interface {p1}, Lcom/android/mail/ui/ControllableActivity;->getAccountController()Lcom/android/mail/ui/AccountController;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/android/mail/providers/AccountObserver;->initialize(Lcom/android/mail/ui/AccountController;)Lcom/android/mail/providers/Account;

    move-result-object v0

    iput-object v0, p0, Lcom/android/mail/browse/SelectedConversationsActionMenu;->mAccount:Lcom/android/mail/providers/Account;

    iput-object p3, p0, Lcom/android/mail/browse/SelectedConversationsActionMenu;->mFolder:Lcom/android/mail/providers/Folder;

    iget-object v0, p0, Lcom/android/mail/browse/SelectedConversationsActionMenu;->mActivity:Lcom/android/mail/ui/ControllableActivity;

    invoke-interface {v0}, Lcom/android/mail/ui/ControllableActivity;->getActivityContext()Landroid/content/Context;

    move-result-object v0

    iput-object v0, p0, Lcom/android/mail/browse/SelectedConversationsActionMenu;->mContext:Landroid/content/Context;

    invoke-interface {p1}, Lcom/android/mail/ui/ControllableActivity;->getConversationUpdater()Lcom/android/mail/ui/ConversationUpdater;

    move-result-object v0

    iput-object v0, p0, Lcom/android/mail/browse/SelectedConversationsActionMenu;->mUpdater:Lcom/android/mail/ui/ConversationUpdater;

    invoke-static {}, Lcom/android/mail/ui/FolderSelectionDialog;->setDialogDismissed()V

    return-void
.end method

.method static synthetic access$002(Lcom/android/mail/browse/SelectedConversationsActionMenu;Lcom/android/mail/providers/Account;)Lcom/android/mail/providers/Account;
    .locals 0
    .param p0    # Lcom/android/mail/browse/SelectedConversationsActionMenu;
    .param p1    # Lcom/android/mail/providers/Account;

    iput-object p1, p0, Lcom/android/mail/browse/SelectedConversationsActionMenu;->mAccount:Lcom/android/mail/providers/Account;

    return-object p1
.end method

.method private clearSelection()V
    .locals 1

    iget-object v0, p0, Lcom/android/mail/browse/SelectedConversationsActionMenu;->mSelectionSet:Lcom/android/mail/ui/ConversationSelectionSet;

    invoke-virtual {v0}, Lcom/android/mail/ui/ConversationSelectionSet;->clear()V

    return-void
.end method

.method private destroy()V
    .locals 1

    invoke-virtual {p0}, Lcom/android/mail/browse/SelectedConversationsActionMenu;->deactivate()V

    iget-object v0, p0, Lcom/android/mail/browse/SelectedConversationsActionMenu;->mSelectionSet:Lcom/android/mail/ui/ConversationSelectionSet;

    invoke-virtual {v0, p0}, Lcom/android/mail/ui/ConversationSelectionSet;->removeObserver(Lcom/android/mail/ui/ConversationSetObserver;)V

    invoke-direct {p0}, Lcom/android/mail/browse/SelectedConversationsActionMenu;->clearSelection()V

    iget-object v0, p0, Lcom/android/mail/browse/SelectedConversationsActionMenu;->mUpdater:Lcom/android/mail/ui/ConversationUpdater;

    invoke-interface {v0}, Lcom/android/mail/ui/ConversationUpdater;->refreshConversationList()V

    iget-object v0, p0, Lcom/android/mail/browse/SelectedConversationsActionMenu;->mAccountObserver:Lcom/android/mail/providers/AccountObserver;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/mail/browse/SelectedConversationsActionMenu;->mAccountObserver:Lcom/android/mail/providers/AccountObserver;

    invoke-virtual {v0}, Lcom/android/mail/providers/AccountObserver;->unregisterAndDestroy()V

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/android/mail/browse/SelectedConversationsActionMenu;->mAccountObserver:Lcom/android/mail/providers/AccountObserver;

    :cond_0
    return-void
.end method

.method private destroy(ILjava/util/Collection;Lcom/android/mail/ui/DestructiveAction;)V
    .locals 1
    .param p1    # I
    .param p3    # Lcom/android/mail/ui/DestructiveAction;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Ljava/util/Collection",
            "<",
            "Lcom/android/mail/providers/Conversation;",
            ">;",
            "Lcom/android/mail/ui/DestructiveAction;",
            ")V"
        }
    .end annotation

    iget-object v0, p0, Lcom/android/mail/browse/SelectedConversationsActionMenu;->mUpdater:Lcom/android/mail/ui/ConversationUpdater;

    invoke-interface {v0, p1, p2, p3}, Lcom/android/mail/ui/ConversationUpdater;->delete(ILjava/util/Collection;Lcom/android/mail/ui/DestructiveAction;)V

    return-void
.end method

.method private markConversationsImportant(Z)V
    .locals 6
    .param p1    # Z

    iget-object v4, p0, Lcom/android/mail/browse/SelectedConversationsActionMenu;->mSelectionSet:Lcom/android/mail/ui/ConversationSelectionSet;

    invoke-virtual {v4}, Lcom/android/mail/ui/ConversationSelectionSet;->values()Ljava/util/Collection;

    move-result-object v3

    if-eqz p1, :cond_0

    const/4 v2, 0x1

    :goto_0
    iget-object v4, p0, Lcom/android/mail/browse/SelectedConversationsActionMenu;->mUpdater:Lcom/android/mail/ui/ConversationUpdater;

    sget-object v5, Lcom/android/mail/providers/UIProvider$ConversationColumns;->PRIORITY:Ljava/lang/String;

    invoke-interface {v4, v3, v5, v2}, Lcom/android/mail/ui/ConversationUpdater;->updateConversation(Ljava/util/Collection;Ljava/lang/String;I)V

    invoke-interface {v3}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/mail/providers/Conversation;

    iput v2, v0, Lcom/android/mail/providers/Conversation;->priority:I

    goto :goto_1

    :cond_0
    const/4 v2, 0x0

    goto :goto_0

    :cond_1
    invoke-direct {p0}, Lcom/android/mail/browse/SelectedConversationsActionMenu;->updateSelection()V

    return-void
.end method

.method private markConversationsRead(Z)V
    .locals 3
    .param p1    # Z

    iget-object v1, p0, Lcom/android/mail/browse/SelectedConversationsActionMenu;->mSelectionSet:Lcom/android/mail/ui/ConversationSelectionSet;

    invoke-virtual {v1}, Lcom/android/mail/ui/ConversationSelectionSet;->values()Ljava/util/Collection;

    move-result-object v0

    iget-object v1, p0, Lcom/android/mail/browse/SelectedConversationsActionMenu;->mUpdater:Lcom/android/mail/ui/ConversationUpdater;

    const/4 v2, 0x0

    invoke-interface {v1, v0, p1, v2}, Lcom/android/mail/ui/ConversationUpdater;->markConversationsRead(Ljava/util/Collection;ZZ)V

    invoke-direct {p0}, Lcom/android/mail/browse/SelectedConversationsActionMenu;->updateSelection()V

    return-void
.end method

.method private performDestructiveAction(I)V
    .locals 8
    .param p1    # I

    iget-object v6, p0, Lcom/android/mail/browse/SelectedConversationsActionMenu;->mSelectionSet:Lcom/android/mail/ui/ConversationSelectionSet;

    invoke-virtual {v6}, Lcom/android/mail/ui/ConversationSelectionSet;->values()Ljava/util/Collection;

    move-result-object v1

    iget-object v6, p0, Lcom/android/mail/browse/SelectedConversationsActionMenu;->mAccount:Lcom/android/mail/providers/Account;

    iget-object v4, v6, Lcom/android/mail/providers/Account;->settings:Lcom/android/mail/providers/Settings;

    if-eqz v4, :cond_1

    const v6, 0x7f08011a

    if-eq p1, v6, :cond_0

    const v6, 0x7f08011b

    if-ne p1, v6, :cond_1

    :cond_0
    iget-boolean v5, v4, Lcom/android/mail/providers/Settings;->confirmDelete:Z

    :goto_0
    if-eqz v5, :cond_2

    iget-object v6, p0, Lcom/android/mail/browse/SelectedConversationsActionMenu;->mUpdater:Lcom/android/mail/ui/ConversationUpdater;

    const/4 v7, 0x1

    invoke-interface {v6, p1, v7}, Lcom/android/mail/ui/ConversationUpdater;->makeDialogListener(IZ)V

    packed-switch p1, :pswitch_data_0

    const v3, 0x7f100005

    :goto_1
    iget-object v6, p0, Lcom/android/mail/browse/SelectedConversationsActionMenu;->mContext:Landroid/content/Context;

    invoke-interface {v1}, Ljava/util/Collection;->size()I

    move-result v7

    invoke-static {v6, v3, v7}, Lcom/android/mail/utils/Utils;->formatPlural(Landroid/content/Context;II)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/android/mail/browse/ConfirmDialogFragment;->newInstance(Ljava/lang/CharSequence;)Lcom/android/mail/browse/ConfirmDialogFragment;

    move-result-object v0

    iget-object v6, p0, Lcom/android/mail/browse/SelectedConversationsActionMenu;->mActivity:Lcom/android/mail/ui/ControllableActivity;

    invoke-interface {v6}, Lcom/android/mail/ui/ControllableActivity;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v6

    invoke-virtual {v0, v6}, Lcom/android/mail/browse/ConfirmDialogFragment;->displayDialog(Landroid/app/FragmentManager;)V

    :goto_2
    return-void

    :cond_1
    iget-boolean v5, v4, Lcom/android/mail/providers/Settings;->confirmArchive:Z

    goto :goto_0

    :pswitch_0
    const v3, 0x7f100004

    goto :goto_1

    :pswitch_1
    const v3, 0x7f100006

    goto :goto_1

    :cond_2
    iget-object v6, p0, Lcom/android/mail/browse/SelectedConversationsActionMenu;->mUpdater:Lcom/android/mail/ui/ConversationUpdater;

    invoke-interface {v6, p1}, Lcom/android/mail/ui/ConversationUpdater;->getDeferredBatchAction(I)Lcom/android/mail/ui/DestructiveAction;

    move-result-object v6

    invoke-direct {p0, p1, v1, v6}, Lcom/android/mail/browse/SelectedConversationsActionMenu;->destroy(ILjava/util/Collection;Lcom/android/mail/ui/DestructiveAction;)V

    goto :goto_2

    nop

    :pswitch_data_0
    .packed-switch 0x7f08011a
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method private starConversations(Z)V
    .locals 5
    .param p1    # Z

    iget-object v3, p0, Lcom/android/mail/browse/SelectedConversationsActionMenu;->mSelectionSet:Lcom/android/mail/ui/ConversationSelectionSet;

    invoke-virtual {v3}, Lcom/android/mail/ui/ConversationSelectionSet;->values()Ljava/util/Collection;

    move-result-object v2

    iget-object v3, p0, Lcom/android/mail/browse/SelectedConversationsActionMenu;->mUpdater:Lcom/android/mail/ui/ConversationUpdater;

    sget-object v4, Lcom/android/mail/providers/UIProvider$ConversationColumns;->STARRED:Ljava/lang/String;

    invoke-interface {v3, v2, v4, p1}, Lcom/android/mail/ui/ConversationUpdater;->updateConversation(Ljava/util/Collection;Ljava/lang/String;Z)V

    invoke-interface {v2}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/mail/providers/Conversation;

    iput-boolean p1, v0, Lcom/android/mail/providers/Conversation;->starred:Z

    goto :goto_0

    :cond_0
    invoke-direct {p0}, Lcom/android/mail/browse/SelectedConversationsActionMenu;->updateSelection()V

    return-void
.end method

.method private updateCount()V
    .locals 6

    iget-object v0, p0, Lcom/android/mail/browse/SelectedConversationsActionMenu;->mActionMode:Landroid/view/ActionMode;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/mail/browse/SelectedConversationsActionMenu;->mActionMode:Landroid/view/ActionMode;

    iget-object v1, p0, Lcom/android/mail/browse/SelectedConversationsActionMenu;->mContext:Landroid/content/Context;

    const v2, 0x7f0900a0

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    iget-object v5, p0, Lcom/android/mail/browse/SelectedConversationsActionMenu;->mSelectionSet:Lcom/android/mail/ui/ConversationSelectionSet;

    invoke-virtual {v5}, Lcom/android/mail/ui/ConversationSelectionSet;->size()I

    move-result v5

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v3, v4

    invoke-virtual {v1, v2, v3}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/view/ActionMode;->setTitle(Ljava/lang/CharSequence;)V

    :cond_0
    return-void
.end method

.method private updateSelection()V
    .locals 2

    iget-object v0, p0, Lcom/android/mail/browse/SelectedConversationsActionMenu;->mUpdater:Lcom/android/mail/ui/ConversationUpdater;

    invoke-interface {v0}, Lcom/android/mail/ui/ConversationUpdater;->refreshConversationList()V

    iget-object v0, p0, Lcom/android/mail/browse/SelectedConversationsActionMenu;->mActionMode:Landroid/view/ActionMode;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/mail/browse/SelectedConversationsActionMenu;->mActionMode:Landroid/view/ActionMode;

    iget-object v1, p0, Lcom/android/mail/browse/SelectedConversationsActionMenu;->mActionMode:Landroid/view/ActionMode;

    invoke-virtual {v1}, Landroid/view/ActionMode;->getMenu()Landroid/view/Menu;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lcom/android/mail/browse/SelectedConversationsActionMenu;->onPrepareActionMode(Landroid/view/ActionMode;Landroid/view/Menu;)Z

    :cond_0
    return-void
.end method


# virtual methods
.method public activate()V
    .locals 1

    iget-object v0, p0, Lcom/android/mail/browse/SelectedConversationsActionMenu;->mSelectionSet:Lcom/android/mail/ui/ConversationSelectionSet;

    invoke-virtual {v0}, Lcom/android/mail/ui/ConversationSelectionSet;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/android/mail/browse/SelectedConversationsActionMenu;->mActivated:Z

    iget-object v0, p0, Lcom/android/mail/browse/SelectedConversationsActionMenu;->mActionMode:Landroid/view/ActionMode;

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/android/mail/browse/SelectedConversationsActionMenu;->mActivity:Lcom/android/mail/ui/ControllableActivity;

    invoke-interface {v0, p0}, Lcom/android/mail/ui/ControllableActivity;->startActionMode(Landroid/view/ActionMode$Callback;)Landroid/view/ActionMode;

    goto :goto_0
.end method

.method public deactivate()V
    .locals 1

    iget-object v0, p0, Lcom/android/mail/browse/SelectedConversationsActionMenu;->mActionMode:Landroid/view/ActionMode;

    if-eqz v0, :cond_0

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/android/mail/browse/SelectedConversationsActionMenu;->mActivated:Z

    iget-object v0, p0, Lcom/android/mail/browse/SelectedConversationsActionMenu;->mActionMode:Landroid/view/ActionMode;

    invoke-virtual {v0}, Landroid/view/ActionMode;->finish()V

    :cond_0
    return-void
.end method

.method public isActivated()Z
    .locals 1

    iget-boolean v0, p0, Lcom/android/mail/browse/SelectedConversationsActionMenu;->mActivated:Z

    return v0
.end method

.method public onActionItemClicked(Landroid/view/ActionMode;Landroid/view/MenuItem;)Z
    .locals 14
    .param p1    # Landroid/view/ActionMode;
    .param p2    # Landroid/view/MenuItem;

    const/4 v10, 0x1

    iget-object v0, p0, Lcom/android/mail/browse/SelectedConversationsActionMenu;->mListController:Lcom/android/mail/ui/ConversationListCallbacks;

    const/4 v2, 0x1

    invoke-interface {v0, v2}, Lcom/android/mail/ui/ConversationListCallbacks;->commitDestructiveActions(Z)V

    invoke-interface/range {p2 .. p2}, Landroid/view/MenuItem;->getItemId()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    const/4 v10, 0x0

    :cond_0
    :goto_0
    return v10

    :sswitch_0
    const v0, 0x7f08011a

    invoke-direct {p0, v0}, Lcom/android/mail/browse/SelectedConversationsActionMenu;->performDestructiveAction(I)V

    goto :goto_0

    :sswitch_1
    const v0, 0x7f08011b

    invoke-direct {p0, v0}, Lcom/android/mail/browse/SelectedConversationsActionMenu;->performDestructiveAction(I)V

    goto :goto_0

    :sswitch_2
    const v0, 0x7f080118

    invoke-direct {p0, v0}, Lcom/android/mail/browse/SelectedConversationsActionMenu;->performDestructiveAction(I)V

    goto :goto_0

    :sswitch_3
    const v12, 0x7f080119

    iget-object v0, p0, Lcom/android/mail/browse/SelectedConversationsActionMenu;->mSelectionSet:Lcom/android/mail/ui/ConversationSelectionSet;

    invoke-virtual {v0}, Lcom/android/mail/ui/ConversationSelectionSet;->values()Ljava/util/Collection;

    move-result-object v13

    iget-object v0, p0, Lcom/android/mail/browse/SelectedConversationsActionMenu;->mUpdater:Lcom/android/mail/ui/ConversationUpdater;

    iget-object v2, p0, Lcom/android/mail/browse/SelectedConversationsActionMenu;->mSelectionSet:Lcom/android/mail/ui/ConversationSelectionSet;

    invoke-virtual {v2}, Lcom/android/mail/ui/ConversationSelectionSet;->values()Ljava/util/Collection;

    move-result-object v1

    iget-object v2, p0, Lcom/android/mail/browse/SelectedConversationsActionMenu;->mFolder:Lcom/android/mail/providers/Folder;

    const/4 v3, 0x1

    const/4 v4, 0x1

    const/4 v5, 0x1

    invoke-interface/range {v0 .. v5}, Lcom/android/mail/ui/ConversationUpdater;->getDeferredRemoveFolder(Ljava/util/Collection;Lcom/android/mail/providers/Folder;ZZZ)Lcom/android/mail/ui/DestructiveAction;

    move-result-object v0

    invoke-direct {p0, v12, v13, v0}, Lcom/android/mail/browse/SelectedConversationsActionMenu;->destroy(ILjava/util/Collection;Lcom/android/mail/ui/DestructiveAction;)V

    goto :goto_0

    :sswitch_4
    iget-object v0, p0, Lcom/android/mail/browse/SelectedConversationsActionMenu;->mUpdater:Lcom/android/mail/ui/ConversationUpdater;

    const v2, 0x7f080120

    iget-object v3, p0, Lcom/android/mail/browse/SelectedConversationsActionMenu;->mSelectionSet:Lcom/android/mail/ui/ConversationSelectionSet;

    invoke-virtual {v3}, Lcom/android/mail/ui/ConversationSelectionSet;->values()Ljava/util/Collection;

    move-result-object v3

    iget-object v4, p0, Lcom/android/mail/browse/SelectedConversationsActionMenu;->mUpdater:Lcom/android/mail/ui/ConversationUpdater;

    const v5, 0x7f080120

    invoke-interface {v4, v5}, Lcom/android/mail/ui/ConversationUpdater;->getBatchAction(I)Lcom/android/mail/ui/DestructiveAction;

    move-result-object v4

    invoke-interface {v0, v2, v3, v4}, Lcom/android/mail/ui/ConversationUpdater;->delete(ILjava/util/Collection;Lcom/android/mail/ui/DestructiveAction;)V

    goto :goto_0

    :sswitch_5
    iget-object v0, p0, Lcom/android/mail/browse/SelectedConversationsActionMenu;->mUpdater:Lcom/android/mail/ui/ConversationUpdater;

    const v2, 0x7f080121

    iget-object v3, p0, Lcom/android/mail/browse/SelectedConversationsActionMenu;->mSelectionSet:Lcom/android/mail/ui/ConversationSelectionSet;

    invoke-virtual {v3}, Lcom/android/mail/ui/ConversationSelectionSet;->values()Ljava/util/Collection;

    move-result-object v3

    iget-object v4, p0, Lcom/android/mail/browse/SelectedConversationsActionMenu;->mUpdater:Lcom/android/mail/ui/ConversationUpdater;

    const v5, 0x7f080121

    invoke-interface {v4, v5}, Lcom/android/mail/ui/ConversationUpdater;->getBatchAction(I)Lcom/android/mail/ui/DestructiveAction;

    move-result-object v4

    invoke-interface {v0, v2, v3, v4}, Lcom/android/mail/ui/ConversationUpdater;->delete(ILjava/util/Collection;Lcom/android/mail/ui/DestructiveAction;)V

    goto :goto_0

    :sswitch_6
    iget-object v0, p0, Lcom/android/mail/browse/SelectedConversationsActionMenu;->mUpdater:Lcom/android/mail/ui/ConversationUpdater;

    const v2, 0x7f080122

    iget-object v3, p0, Lcom/android/mail/browse/SelectedConversationsActionMenu;->mSelectionSet:Lcom/android/mail/ui/ConversationSelectionSet;

    invoke-virtual {v3}, Lcom/android/mail/ui/ConversationSelectionSet;->values()Ljava/util/Collection;

    move-result-object v3

    iget-object v4, p0, Lcom/android/mail/browse/SelectedConversationsActionMenu;->mUpdater:Lcom/android/mail/ui/ConversationUpdater;

    const v5, 0x7f080122

    invoke-interface {v4, v5}, Lcom/android/mail/ui/ConversationUpdater;->getBatchAction(I)Lcom/android/mail/ui/DestructiveAction;

    move-result-object v4

    invoke-interface {v0, v2, v3, v4}, Lcom/android/mail/ui/ConversationUpdater;->delete(ILjava/util/Collection;Lcom/android/mail/ui/DestructiveAction;)V

    goto :goto_0

    :sswitch_7
    iget-object v0, p0, Lcom/android/mail/browse/SelectedConversationsActionMenu;->mUpdater:Lcom/android/mail/ui/ConversationUpdater;

    const v2, 0x7f080123

    iget-object v3, p0, Lcom/android/mail/browse/SelectedConversationsActionMenu;->mSelectionSet:Lcom/android/mail/ui/ConversationSelectionSet;

    invoke-virtual {v3}, Lcom/android/mail/ui/ConversationSelectionSet;->values()Ljava/util/Collection;

    move-result-object v3

    iget-object v4, p0, Lcom/android/mail/browse/SelectedConversationsActionMenu;->mUpdater:Lcom/android/mail/ui/ConversationUpdater;

    const v5, 0x7f080123

    invoke-interface {v4, v5}, Lcom/android/mail/ui/ConversationUpdater;->getBatchAction(I)Lcom/android/mail/ui/DestructiveAction;

    move-result-object v4

    invoke-interface {v0, v2, v3, v4}, Lcom/android/mail/ui/ConversationUpdater;->delete(ILjava/util/Collection;Lcom/android/mail/ui/DestructiveAction;)V

    goto/16 :goto_0

    :sswitch_8
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/android/mail/browse/SelectedConversationsActionMenu;->markConversationsRead(Z)V

    goto/16 :goto_0

    :sswitch_9
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/android/mail/browse/SelectedConversationsActionMenu;->markConversationsRead(Z)V

    goto/16 :goto_0

    :sswitch_a
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/android/mail/browse/SelectedConversationsActionMenu;->starConversations(Z)V

    goto/16 :goto_0

    :sswitch_b
    iget-object v0, p0, Lcom/android/mail/browse/SelectedConversationsActionMenu;->mFolder:Lcom/android/mail/providers/Folder;

    iget v0, v0, Lcom/android/mail/providers/Folder;->type:I

    const/4 v2, 0x7

    if-ne v0, v2, :cond_1

    sget-object v0, Lcom/android/mail/browse/SelectedConversationsActionMenu;->LOG_TAG:Ljava/lang/String;

    const-string v2, "We are in a starred folder, removing the star"

    const/4 v3, 0x0

    new-array v3, v3, [Ljava/lang/Object;

    invoke-static {v0, v2, v3}, Lcom/android/mail/utils/LogUtils;->d(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)I

    const v0, 0x7f080129

    invoke-direct {p0, v0}, Lcom/android/mail/browse/SelectedConversationsActionMenu;->performDestructiveAction(I)V

    goto/16 :goto_0

    :cond_1
    sget-object v0, Lcom/android/mail/browse/SelectedConversationsActionMenu;->LOG_TAG:Ljava/lang/String;

    const-string v2, "Not in a starred folder."

    const/4 v3, 0x0

    new-array v3, v3, [Ljava/lang/Object;

    invoke-static {v0, v2, v3}, Lcom/android/mail/utils/LogUtils;->d(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)I

    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/android/mail/browse/SelectedConversationsActionMenu;->starConversations(Z)V

    goto/16 :goto_0

    :sswitch_c
    const/4 v7, 0x0

    iget-object v1, p0, Lcom/android/mail/browse/SelectedConversationsActionMenu;->mAccount:Lcom/android/mail/providers/Account;

    iget-object v0, p0, Lcom/android/mail/browse/SelectedConversationsActionMenu;->mFolder:Lcom/android/mail/providers/Folder;

    const/16 v2, 0x1000

    invoke-virtual {v0, v2}, Lcom/android/mail/providers/Folder;->supportsCapability(I)Z

    move-result v0

    if-eqz v0, :cond_5

    const/4 v6, 0x0

    iget-object v0, p0, Lcom/android/mail/browse/SelectedConversationsActionMenu;->mSelectionSet:Lcom/android/mail/ui/ConversationSelectionSet;

    invoke-virtual {v0}, Lcom/android/mail/ui/ConversationSelectionSet;->values()Ljava/util/Collection;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v11

    :cond_2
    :goto_1
    invoke-interface {v11}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_4

    invoke-interface {v11}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Lcom/android/mail/providers/Conversation;

    if-nez v6, :cond_3

    iget-object v6, v8, Lcom/android/mail/providers/Conversation;->accountUri:Landroid/net/Uri;

    goto :goto_1

    :cond_3
    iget-object v0, v8, Lcom/android/mail/providers/Conversation;->accountUri:Landroid/net/Uri;

    invoke-virtual {v6, v0}, Landroid/net/Uri;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_2

    iget-object v0, p0, Lcom/android/mail/browse/SelectedConversationsActionMenu;->mContext:Landroid/content/Context;

    const v2, 0x7f090100

    const/4 v3, 0x1

    invoke-static {v0, v2, v3}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    const/4 v7, 0x1

    :cond_4
    if-nez v7, :cond_5

    invoke-static {v6}, Lcom/android/mail/providers/MailAppProvider;->getAccountFromAccountUri(Landroid/net/Uri;)Lcom/android/mail/providers/Account;

    move-result-object v1

    :cond_5
    if-nez v7, :cond_0

    iget-object v0, p0, Lcom/android/mail/browse/SelectedConversationsActionMenu;->mContext:Landroid/content/Context;

    iget-object v2, p0, Lcom/android/mail/browse/SelectedConversationsActionMenu;->mUpdater:Lcom/android/mail/ui/ConversationUpdater;

    iget-object v3, p0, Lcom/android/mail/browse/SelectedConversationsActionMenu;->mSelectionSet:Lcom/android/mail/ui/ConversationSelectionSet;

    invoke-virtual {v3}, Lcom/android/mail/ui/ConversationSelectionSet;->values()Ljava/util/Collection;

    move-result-object v3

    const/4 v4, 0x1

    iget-object v5, p0, Lcom/android/mail/browse/SelectedConversationsActionMenu;->mFolder:Lcom/android/mail/providers/Folder;

    invoke-static/range {v0 .. v5}, Lcom/android/mail/ui/FolderSelectionDialog;->getInstance(Landroid/content/Context;Lcom/android/mail/providers/Account;Lcom/android/mail/ui/ConversationUpdater;Ljava/util/Collection;ZLcom/android/mail/providers/Folder;)Lcom/android/mail/ui/FolderSelectionDialog;

    move-result-object v9

    if-eqz v9, :cond_0

    invoke-virtual {v9}, Lcom/android/mail/ui/FolderSelectionDialog;->show()V

    goto/16 :goto_0

    :sswitch_d
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/android/mail/browse/SelectedConversationsActionMenu;->markConversationsImportant(Z)V

    goto/16 :goto_0

    :sswitch_e
    iget-object v0, p0, Lcom/android/mail/browse/SelectedConversationsActionMenu;->mFolder:Lcom/android/mail/providers/Folder;

    const/16 v2, 0x400

    invoke-virtual {v0, v2}, Lcom/android/mail/providers/Folder;->supportsCapability(I)Z

    move-result v0

    if-eqz v0, :cond_6

    const v0, 0x7f08011f

    invoke-direct {p0, v0}, Lcom/android/mail/browse/SelectedConversationsActionMenu;->performDestructiveAction(I)V

    goto/16 :goto_0

    :cond_6
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/android/mail/browse/SelectedConversationsActionMenu;->markConversationsImportant(Z)V

    goto/16 :goto_0

    :sswitch_data_0
    .sparse-switch
        0x7f080033 -> :sswitch_9
        0x7f080057 -> :sswitch_a
        0x7f080118 -> :sswitch_2
        0x7f080119 -> :sswitch_3
        0x7f08011a -> :sswitch_0
        0x7f08011b -> :sswitch_1
        0x7f08011c -> :sswitch_c
        0x7f08011e -> :sswitch_d
        0x7f08011f -> :sswitch_e
        0x7f080120 -> :sswitch_4
        0x7f080121 -> :sswitch_5
        0x7f080122 -> :sswitch_6
        0x7f080123 -> :sswitch_7
        0x7f080128 -> :sswitch_8
        0x7f080129 -> :sswitch_b
    .end sparse-switch
.end method

.method public onCreateActionMode(Landroid/view/ActionMode;Landroid/view/Menu;)Z
    .locals 2
    .param p1    # Landroid/view/ActionMode;
    .param p2    # Landroid/view/Menu;

    iget-object v1, p0, Lcom/android/mail/browse/SelectedConversationsActionMenu;->mSelectionSet:Lcom/android/mail/ui/ConversationSelectionSet;

    invoke-virtual {v1, p0}, Lcom/android/mail/ui/ConversationSelectionSet;->addObserver(Lcom/android/mail/ui/ConversationSetObserver;)V

    iget-object v1, p0, Lcom/android/mail/browse/SelectedConversationsActionMenu;->mActivity:Lcom/android/mail/ui/ControllableActivity;

    invoke-interface {v1}, Lcom/android/mail/ui/ControllableActivity;->getMenuInflater()Landroid/view/MenuInflater;

    move-result-object v0

    const v1, 0x7f110004

    invoke-virtual {v0, v1, p2}, Landroid/view/MenuInflater;->inflate(ILandroid/view/Menu;)V

    iput-object p1, p0, Lcom/android/mail/browse/SelectedConversationsActionMenu;->mActionMode:Landroid/view/ActionMode;

    iput-object p2, p0, Lcom/android/mail/browse/SelectedConversationsActionMenu;->mMenu:Landroid/view/Menu;

    invoke-direct {p0}, Lcom/android/mail/browse/SelectedConversationsActionMenu;->updateCount()V

    const/4 v1, 0x1

    return v1
.end method

.method public onDestroyActionMode(Landroid/view/ActionMode;)V
    .locals 3
    .param p1    # Landroid/view/ActionMode;

    const/4 v2, 0x0

    iput-object v2, p0, Lcom/android/mail/browse/SelectedConversationsActionMenu;->mActionMode:Landroid/view/ActionMode;

    iget-boolean v0, p0, Lcom/android/mail/browse/SelectedConversationsActionMenu;->mActivated:Z

    if-eqz v0, :cond_0

    invoke-direct {p0}, Lcom/android/mail/browse/SelectedConversationsActionMenu;->destroy()V

    iget-object v0, p0, Lcom/android/mail/browse/SelectedConversationsActionMenu;->mActivity:Lcom/android/mail/ui/ControllableActivity;

    invoke-interface {v0}, Lcom/android/mail/ui/ControllableActivity;->getListHandler()Lcom/android/mail/ui/ConversationListCallbacks;

    move-result-object v0

    const/4 v1, 0x1

    invoke-interface {v0, v1}, Lcom/android/mail/ui/ConversationListCallbacks;->commitDestructiveActions(Z)V

    :cond_0
    iput-object v2, p0, Lcom/android/mail/browse/SelectedConversationsActionMenu;->mMenu:Landroid/view/Menu;

    return-void
.end method

.method public onPrepareActionMode(Landroid/view/ActionMode;Landroid/view/Menu;)Z
    .locals 34
    .param p1    # Landroid/view/ActionMode;
    .param p2    # Landroid/view/Menu;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/mail/browse/SelectedConversationsActionMenu;->mSelectionSet:Lcom/android/mail/ui/ConversationSelectionSet;

    move-object/from16 v29, v0

    invoke-virtual/range {v29 .. v29}, Lcom/android/mail/ui/ConversationSelectionSet;->values()Ljava/util/Collection;

    move-result-object v5

    const/16 v23, 0x0

    const/16 v21, 0x0

    const/16 v19, 0x0

    const/16 v20, 0x0

    const/16 v18, 0x0

    invoke-interface {v5}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v7

    :cond_0
    invoke-interface {v7}, Ljava/util/Iterator;->hasNext()Z

    move-result v29

    if-eqz v29, :cond_6

    invoke-interface {v7}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/android/mail/providers/Conversation;

    iget-boolean v0, v4, Lcom/android/mail/providers/Conversation;->starred:Z

    move/from16 v29, v0

    if-nez v29, :cond_1

    const/16 v23, 0x1

    :cond_1
    iget-boolean v0, v4, Lcom/android/mail/providers/Conversation;->read:Z

    move/from16 v29, v0

    if-eqz v29, :cond_2

    const/16 v21, 0x1

    :cond_2
    invoke-virtual {v4}, Lcom/android/mail/providers/Conversation;->isImportant()Z

    move-result v29

    if-nez v29, :cond_3

    const/16 v19, 0x1

    :cond_3
    iget-boolean v0, v4, Lcom/android/mail/providers/Conversation;->spam:Z

    move/from16 v29, v0

    if-eqz v29, :cond_4

    const/16 v20, 0x1

    :cond_4
    iget-boolean v0, v4, Lcom/android/mail/providers/Conversation;->phishing:Z

    move/from16 v29, v0

    if-nez v29, :cond_5

    const/16 v18, 0x1

    :cond_5
    if-eqz v23, :cond_0

    if-eqz v21, :cond_0

    if-eqz v19, :cond_0

    if-eqz v20, :cond_0

    if-eqz v18, :cond_0

    :cond_6
    const v29, 0x7f080057

    move-object/from16 v0, p2

    move/from16 v1, v29

    invoke-interface {v0, v1}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v25

    move-object/from16 v0, v25

    move/from16 v1, v23

    invoke-interface {v0, v1}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    const v29, 0x7f080129

    move-object/from16 v0, p2

    move/from16 v1, v29

    invoke-interface {v0, v1}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v28

    if-nez v23, :cond_b

    const/16 v29, 0x1

    :goto_0
    invoke-interface/range {v28 .. v29}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    const v29, 0x7f080128

    move-object/from16 v0, p2

    move/from16 v1, v29

    invoke-interface {v0, v1}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v13

    if-nez v21, :cond_c

    const/16 v29, 0x1

    :goto_1
    move/from16 v0, v29

    invoke-interface {v13, v0}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    const v29, 0x7f080033

    move-object/from16 v0, p2

    move/from16 v1, v29

    invoke-interface {v0, v1}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v27

    move-object/from16 v0, v27

    move/from16 v1, v21

    invoke-interface {v0, v1}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    const v29, 0x7f080119

    move-object/from16 v0, p2

    move/from16 v1, v29

    invoke-interface {v0, v1}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v14

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/mail/browse/SelectedConversationsActionMenu;->mFolder:Lcom/android/mail/providers/Folder;

    move-object/from16 v29, v0

    if-eqz v29, :cond_d

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/mail/browse/SelectedConversationsActionMenu;->mFolder:Lcom/android/mail/providers/Folder;

    move-object/from16 v29, v0

    move-object/from16 v0, v29

    iget v0, v0, Lcom/android/mail/providers/Folder;->type:I

    move/from16 v29, v0

    if-nez v29, :cond_d

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/mail/browse/SelectedConversationsActionMenu;->mFolder:Lcom/android/mail/providers/Folder;

    move-object/from16 v29, v0

    const/16 v30, 0x8

    invoke-virtual/range {v29 .. v30}, Lcom/android/mail/providers/Folder;->supportsCapability(I)Z

    move-result v29

    if-eqz v29, :cond_d

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/mail/browse/SelectedConversationsActionMenu;->mFolder:Lcom/android/mail/providers/Folder;

    move-object/from16 v29, v0

    invoke-virtual/range {v29 .. v29}, Lcom/android/mail/providers/Folder;->isProviderFolder()Z

    move-result v29

    if-nez v29, :cond_d

    const/16 v22, 0x1

    :goto_2
    move/from16 v0, v22

    invoke-interface {v14, v0}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/mail/browse/SelectedConversationsActionMenu;->mFolder:Lcom/android/mail/providers/Folder;

    move-object/from16 v29, v0

    if-eqz v29, :cond_7

    if-eqz v22, :cond_7

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/mail/browse/SelectedConversationsActionMenu;->mActivity:Lcom/android/mail/ui/ControllableActivity;

    move-object/from16 v29, v0

    invoke-interface/range {v29 .. v29}, Lcom/android/mail/ui/ControllableActivity;->getActivityContext()Landroid/content/Context;

    move-result-object v29

    const v30, 0x7f090050

    const/16 v31, 0x1

    move/from16 v0, v31

    new-array v0, v0, [Ljava/lang/Object;

    move-object/from16 v31, v0

    const/16 v32, 0x0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/mail/browse/SelectedConversationsActionMenu;->mFolder:Lcom/android/mail/providers/Folder;

    move-object/from16 v33, v0

    move-object/from16 v0, v33

    iget-object v0, v0, Lcom/android/mail/providers/Folder;->name:Ljava/lang/String;

    move-object/from16 v33, v0

    aput-object v33, v31, v32

    invoke-virtual/range {v29 .. v31}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v29

    move-object/from16 v0, v29

    invoke-interface {v14, v0}, Landroid/view/MenuItem;->setTitle(Ljava/lang/CharSequence;)Landroid/view/MenuItem;

    :cond_7
    const v29, 0x7f080118

    move-object/from16 v0, p2

    move/from16 v1, v29

    invoke-interface {v0, v1}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v3

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/mail/browse/SelectedConversationsActionMenu;->mAccount:Lcom/android/mail/providers/Account;

    move-object/from16 v29, v0

    const/16 v30, 0x8

    invoke-virtual/range {v29 .. v30}, Lcom/android/mail/providers/Account;->supportsCapability(I)Z

    move-result v2

    if-eqz v2, :cond_e

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/mail/browse/SelectedConversationsActionMenu;->mFolder:Lcom/android/mail/providers/Folder;

    move-object/from16 v29, v0

    const/16 v30, 0x10

    invoke-virtual/range {v29 .. v30}, Lcom/android/mail/providers/Folder;->supportsCapability(I)Z

    move-result v29

    if-eqz v29, :cond_e

    const/4 v15, 0x1

    :goto_3
    if-nez v3, :cond_f

    const/4 v15, 0x0

    :goto_4
    if-nez v15, :cond_8

    if-eqz v2, :cond_8

    if-nez v22, :cond_8

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/mail/browse/SelectedConversationsActionMenu;->mActivity:Lcom/android/mail/ui/ControllableActivity;

    move-object/from16 v29, v0

    invoke-interface/range {v29 .. v29}, Lcom/android/mail/ui/ControllableActivity;->getActivityContext()Landroid/content/Context;

    move-result-object v29

    invoke-static/range {v29 .. v29}, Lcom/android/mail/utils/Utils;->shouldShowDisabledArchiveIcon(Landroid/content/Context;)Z

    move-result v29

    if-eqz v29, :cond_8

    const/16 v29, 0x0

    move/from16 v0, v29

    invoke-interface {v3, v0}, Landroid/view/MenuItem;->setEnabled(Z)Landroid/view/MenuItem;

    const/16 v29, 0x1

    move/from16 v0, v29

    invoke-interface {v3, v0}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    :cond_8
    const v29, 0x7f080121

    move-object/from16 v0, p2

    move/from16 v1, v29

    invoke-interface {v0, v1}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v24

    if-nez v20, :cond_10

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/mail/browse/SelectedConversationsActionMenu;->mAccount:Lcom/android/mail/providers/Account;

    move-object/from16 v29, v0

    const/16 v30, 0x2

    invoke-virtual/range {v29 .. v30}, Lcom/android/mail/providers/Account;->supportsCapability(I)Z

    move-result v29

    if-eqz v29, :cond_10

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/mail/browse/SelectedConversationsActionMenu;->mFolder:Lcom/android/mail/providers/Folder;

    move-object/from16 v29, v0

    const/16 v30, 0x40

    invoke-virtual/range {v29 .. v30}, Lcom/android/mail/providers/Folder;->supportsCapability(I)Z

    move-result v29

    if-eqz v29, :cond_10

    const/16 v29, 0x1

    :goto_5
    move-object/from16 v0, v24

    move/from16 v1, v29

    invoke-interface {v0, v1}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    const v29, 0x7f080122

    move-object/from16 v0, p2

    move/from16 v1, v29

    invoke-interface {v0, v1}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v11

    if-eqz v20, :cond_11

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/mail/browse/SelectedConversationsActionMenu;->mAccount:Lcom/android/mail/providers/Account;

    move-object/from16 v29, v0

    const/16 v30, 0x2

    invoke-virtual/range {v29 .. v30}, Lcom/android/mail/providers/Account;->supportsCapability(I)Z

    move-result v29

    if-eqz v29, :cond_11

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/mail/browse/SelectedConversationsActionMenu;->mFolder:Lcom/android/mail/providers/Folder;

    move-object/from16 v29, v0

    const/16 v30, 0x80

    invoke-virtual/range {v29 .. v30}, Lcom/android/mail/providers/Folder;->supportsCapability(I)Z

    move-result v29

    if-eqz v29, :cond_11

    const/16 v29, 0x1

    :goto_6
    move/from16 v0, v29

    invoke-interface {v11, v0}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    const v29, 0x7f080123

    move-object/from16 v0, p2

    move/from16 v1, v29

    invoke-interface {v0, v1}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v12

    if-eqz v18, :cond_12

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/mail/browse/SelectedConversationsActionMenu;->mAccount:Lcom/android/mail/providers/Account;

    move-object/from16 v29, v0

    const/16 v30, 0x4

    invoke-virtual/range {v29 .. v30}, Lcom/android/mail/providers/Account;->supportsCapability(I)Z

    move-result v29

    if-eqz v29, :cond_12

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/mail/browse/SelectedConversationsActionMenu;->mFolder:Lcom/android/mail/providers/Folder;

    move-object/from16 v29, v0

    const/16 v30, 0x2000

    invoke-virtual/range {v29 .. v30}, Lcom/android/mail/providers/Folder;->supportsCapability(I)Z

    move-result v29

    if-eqz v29, :cond_12

    const/16 v29, 0x1

    :goto_7
    move/from16 v0, v29

    invoke-interface {v12, v0}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    const v29, 0x7f080120

    move-object/from16 v0, p2

    move/from16 v1, v29

    invoke-interface {v0, v1}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v10

    if-eqz v10, :cond_9

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/mail/browse/SelectedConversationsActionMenu;->mAccount:Lcom/android/mail/providers/Account;

    move-object/from16 v29, v0

    const/16 v30, 0x10

    invoke-virtual/range {v29 .. v30}, Lcom/android/mail/providers/Account;->supportsCapability(I)Z

    move-result v29

    if-eqz v29, :cond_13

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/mail/browse/SelectedConversationsActionMenu;->mFolder:Lcom/android/mail/providers/Folder;

    move-object/from16 v29, v0

    if-eqz v29, :cond_13

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/mail/browse/SelectedConversationsActionMenu;->mFolder:Lcom/android/mail/providers/Folder;

    move-object/from16 v29, v0

    move-object/from16 v0, v29

    iget v0, v0, Lcom/android/mail/providers/Folder;->type:I

    move/from16 v29, v0

    const/16 v30, 0x1

    move/from16 v0, v29

    move/from16 v1, v30

    if-ne v0, v1, :cond_13

    const/16 v29, 0x1

    :goto_8
    move/from16 v0, v29

    invoke-interface {v10, v0}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    :cond_9
    const v29, 0x7f08011e

    move-object/from16 v0, p2

    move/from16 v1, v29

    invoke-interface {v0, v1}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v8

    if-eqz v19, :cond_14

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/mail/browse/SelectedConversationsActionMenu;->mAccount:Lcom/android/mail/providers/Account;

    move-object/from16 v29, v0

    const/high16 v30, 0x20000

    invoke-virtual/range {v29 .. v30}, Lcom/android/mail/providers/Account;->supportsCapability(I)Z

    move-result v29

    if-eqz v29, :cond_14

    const/16 v29, 0x1

    :goto_9
    move/from16 v0, v29

    invoke-interface {v8, v0}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    const v29, 0x7f08011f

    move-object/from16 v0, p2

    move/from16 v1, v29

    invoke-interface {v0, v1}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v9

    if-nez v19, :cond_15

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/mail/browse/SelectedConversationsActionMenu;->mAccount:Lcom/android/mail/providers/Account;

    move-object/from16 v29, v0

    const/high16 v30, 0x20000

    invoke-virtual/range {v29 .. v30}, Lcom/android/mail/providers/Account;->supportsCapability(I)Z

    move-result v29

    if-eqz v29, :cond_15

    const/16 v29, 0x1

    :goto_a
    move/from16 v0, v29

    invoke-interface {v9, v0}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/mail/browse/SelectedConversationsActionMenu;->mFolder:Lcom/android/mail/providers/Folder;

    move-object/from16 v29, v0

    if-eqz v29, :cond_16

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/mail/browse/SelectedConversationsActionMenu;->mFolder:Lcom/android/mail/providers/Folder;

    move-object/from16 v29, v0

    const/16 v30, 0x20

    invoke-virtual/range {v29 .. v30}, Lcom/android/mail/providers/Folder;->supportsCapability(I)Z

    move-result v29

    if-eqz v29, :cond_16

    const/16 v16, 0x1

    :goto_b
    const v29, 0x7f08011a

    move-object/from16 v0, p2

    move/from16 v1, v29

    invoke-interface {v0, v1}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v26

    move-object/from16 v0, v26

    move/from16 v1, v16

    invoke-interface {v0, v1}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    if-nez v16, :cond_17

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/mail/browse/SelectedConversationsActionMenu;->mFolder:Lcom/android/mail/providers/Folder;

    move-object/from16 v29, v0

    if-eqz v29, :cond_17

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/mail/browse/SelectedConversationsActionMenu;->mFolder:Lcom/android/mail/providers/Folder;

    move-object/from16 v29, v0

    invoke-virtual/range {v29 .. v29}, Lcom/android/mail/providers/Folder;->isDraft()Z

    move-result v29

    if-eqz v29, :cond_17

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/mail/browse/SelectedConversationsActionMenu;->mAccount:Lcom/android/mail/providers/Account;

    move-object/from16 v29, v0

    const/high16 v30, 0x100000

    invoke-virtual/range {v29 .. v30}, Lcom/android/mail/providers/Account;->supportsCapability(I)Z

    move-result v29

    if-eqz v29, :cond_17

    const/16 v17, 0x1

    :goto_c
    const v29, 0x7f08011b

    move-object/from16 v0, p2

    move/from16 v1, v29

    invoke-interface {v0, v1}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v6

    if-eqz v6, :cond_a

    move/from16 v0, v17

    invoke-interface {v6, v0}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    :cond_a
    const/16 v29, 0x1

    return v29

    :cond_b
    const/16 v29, 0x0

    goto/16 :goto_0

    :cond_c
    const/16 v29, 0x0

    goto/16 :goto_1

    :cond_d
    const/16 v22, 0x0

    goto/16 :goto_2

    :cond_e
    const/4 v15, 0x0

    goto/16 :goto_3

    :cond_f
    invoke-interface {v3, v15}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    goto/16 :goto_4

    :cond_10
    const/16 v29, 0x0

    goto/16 :goto_5

    :cond_11
    const/16 v29, 0x0

    goto/16 :goto_6

    :cond_12
    const/16 v29, 0x0

    goto/16 :goto_7

    :cond_13
    const/16 v29, 0x0

    goto/16 :goto_8

    :cond_14
    const/16 v29, 0x0

    goto/16 :goto_9

    :cond_15
    const/16 v29, 0x0

    goto/16 :goto_a

    :cond_16
    const/16 v16, 0x0

    goto :goto_b

    :cond_17
    const/16 v17, 0x0

    goto :goto_c
.end method

.method public onSetChanged(Lcom/android/mail/ui/ConversationSelectionSet;)V
    .locals 1
    .param p1    # Lcom/android/mail/ui/ConversationSelectionSet;

    invoke-virtual {p1}, Lcom/android/mail/ui/ConversationSelectionSet;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    :goto_0
    return-void

    :cond_0
    invoke-direct {p0}, Lcom/android/mail/browse/SelectedConversationsActionMenu;->updateCount()V

    goto :goto_0
.end method

.method public onSetEmpty()V
    .locals 3

    sget-object v0, Lcom/android/mail/browse/SelectedConversationsActionMenu;->LOG_TAG:Ljava/lang/String;

    const-string v1, "onSetEmpty called."

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {v0, v1, v2}, Lcom/android/mail/utils/LogUtils;->d(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)I

    invoke-direct {p0}, Lcom/android/mail/browse/SelectedConversationsActionMenu;->destroy()V

    return-void
.end method

.method public onSetPopulated(Lcom/android/mail/ui/ConversationSelectionSet;)V
    .locals 0
    .param p1    # Lcom/android/mail/ui/ConversationSelectionSet;

    return-void
.end method
