.class public Lcom/android/mail/browse/ScrollIndicatorsView;
.super Landroid/view/View;
.source "ScrollIndicatorsView.java"

# interfaces
.implements Lcom/android/mail/browse/ScrollNotifier$ScrollListener;


# instance fields
.field private mSource:Lcom/android/mail/browse/ScrollNotifier;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0
    .param p1    # Landroid/content/Context;

    invoke-direct {p0, p1}, Landroid/view/View;-><init>(Landroid/content/Context;)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/util/AttributeSet;

    invoke-direct {p0, p1, p2}, Landroid/view/View;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    return-void
.end method


# virtual methods
.method protected computeHorizontalScrollExtent()I
    .locals 1

    iget-object v0, p0, Lcom/android/mail/browse/ScrollIndicatorsView;->mSource:Lcom/android/mail/browse/ScrollNotifier;

    invoke-interface {v0}, Lcom/android/mail/browse/ScrollNotifier;->computeHorizontalScrollExtent()I

    move-result v0

    return v0
.end method

.method protected computeHorizontalScrollOffset()I
    .locals 1

    iget-object v0, p0, Lcom/android/mail/browse/ScrollIndicatorsView;->mSource:Lcom/android/mail/browse/ScrollNotifier;

    invoke-interface {v0}, Lcom/android/mail/browse/ScrollNotifier;->computeHorizontalScrollOffset()I

    move-result v0

    return v0
.end method

.method protected computeHorizontalScrollRange()I
    .locals 1

    iget-object v0, p0, Lcom/android/mail/browse/ScrollIndicatorsView;->mSource:Lcom/android/mail/browse/ScrollNotifier;

    invoke-interface {v0}, Lcom/android/mail/browse/ScrollNotifier;->computeHorizontalScrollRange()I

    move-result v0

    return v0
.end method

.method protected computeVerticalScrollExtent()I
    .locals 1

    iget-object v0, p0, Lcom/android/mail/browse/ScrollIndicatorsView;->mSource:Lcom/android/mail/browse/ScrollNotifier;

    invoke-interface {v0}, Lcom/android/mail/browse/ScrollNotifier;->computeVerticalScrollExtent()I

    move-result v0

    return v0
.end method

.method protected computeVerticalScrollOffset()I
    .locals 1

    iget-object v0, p0, Lcom/android/mail/browse/ScrollIndicatorsView;->mSource:Lcom/android/mail/browse/ScrollNotifier;

    invoke-interface {v0}, Lcom/android/mail/browse/ScrollNotifier;->computeVerticalScrollOffset()I

    move-result v0

    return v0
.end method

.method protected computeVerticalScrollRange()I
    .locals 1

    iget-object v0, p0, Lcom/android/mail/browse/ScrollIndicatorsView;->mSource:Lcom/android/mail/browse/ScrollNotifier;

    invoke-interface {v0}, Lcom/android/mail/browse/ScrollNotifier;->computeVerticalScrollRange()I

    move-result v0

    return v0
.end method

.method public onNotifierScroll(II)V
    .locals 0
    .param p1    # I
    .param p2    # I

    invoke-virtual {p0}, Lcom/android/mail/browse/ScrollIndicatorsView;->awakenScrollBars()Z

    return-void
.end method

.method public setSourceView(Lcom/android/mail/browse/ScrollNotifier;)V
    .locals 1
    .param p1    # Lcom/android/mail/browse/ScrollNotifier;

    iput-object p1, p0, Lcom/android/mail/browse/ScrollIndicatorsView;->mSource:Lcom/android/mail/browse/ScrollNotifier;

    iget-object v0, p0, Lcom/android/mail/browse/ScrollIndicatorsView;->mSource:Lcom/android/mail/browse/ScrollNotifier;

    invoke-interface {v0, p0}, Lcom/android/mail/browse/ScrollNotifier;->addScrollListener(Lcom/android/mail/browse/ScrollNotifier$ScrollListener;)V

    return-void
.end method
