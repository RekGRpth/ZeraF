.class Lcom/android/mail/compose/QuotedTextView;
.super Landroid/widget/LinearLayout;
.source "QuotedTextView.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/mail/compose/QuotedTextView$RespondInlineListener;,
        Lcom/android/mail/compose/QuotedTextView$ShowHideQuotedTextListener;
    }
.end annotation


# static fields
.field private static final HEADER_SEPARATOR_LENGTH:I

.field private static sQuoteBegin:Ljava/lang/String;


# instance fields
.field private mIncludeText:Z

.field private mQuotedText:Ljava/lang/CharSequence;

.field private mQuotedTextWebView:Landroid/webkit/WebView;

.field private mRespondInlineButton:Landroid/widget/Button;

.field private mRespondInlineListener:Lcom/android/mail/compose/QuotedTextView$RespondInlineListener;

.field private mShowHideCheckBox:Landroid/widget/CheckBox;

.field private mShowHideListener:Lcom/android/mail/compose/QuotedTextView$ShowHideQuotedTextListener;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const-string v0, "<br type=\'attribution\'>"

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v0

    sput v0, Lcom/android/mail/compose/QuotedTextView;->HEADER_SEPARATOR_LENGTH:I

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1    # Landroid/content/Context;

    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/android/mail/compose/QuotedTextView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/util/AttributeSet;

    const/4 v0, -0x1

    invoke-direct {p0, p1, p2, v0}, Lcom/android/mail/compose/QuotedTextView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 4
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/util/AttributeSet;
    .param p3    # I

    const/4 v3, 0x1

    invoke-direct {p0, p1, p2}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    iput-boolean v3, p0, Lcom/android/mail/compose/QuotedTextView;->mIncludeText:Z

    invoke-static {p1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    const v2, 0x7f040058

    invoke-virtual {v0, v2, p0}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    const v2, 0x7f0800d8

    invoke-virtual {p0, v2}, Lcom/android/mail/compose/QuotedTextView;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/webkit/WebView;

    iput-object v2, p0, Lcom/android/mail/compose/QuotedTextView;->mQuotedTextWebView:Landroid/webkit/WebView;

    iget-object v2, p0, Lcom/android/mail/compose/QuotedTextView;->mQuotedTextWebView:Landroid/webkit/WebView;

    invoke-static {v2}, Lcom/android/mail/utils/Utils;->restrictWebView(Landroid/webkit/WebView;)V

    iget-object v2, p0, Lcom/android/mail/compose/QuotedTextView;->mQuotedTextWebView:Landroid/webkit/WebView;

    invoke-virtual {v2}, Landroid/webkit/WebView;->getSettings()Landroid/webkit/WebSettings;

    move-result-object v1

    invoke-virtual {v1, v3}, Landroid/webkit/WebSettings;->setBlockNetworkLoads(Z)V

    const v2, 0x7f0800d4

    invoke-virtual {p0, v2}, Lcom/android/mail/compose/QuotedTextView;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/CheckBox;

    iput-object v2, p0, Lcom/android/mail/compose/QuotedTextView;->mShowHideCheckBox:Landroid/widget/CheckBox;

    iget-object v2, p0, Lcom/android/mail/compose/QuotedTextView;->mShowHideCheckBox:Landroid/widget/CheckBox;

    invoke-virtual {v2, v3}, Landroid/widget/CheckBox;->setChecked(Z)V

    iget-object v2, p0, Lcom/android/mail/compose/QuotedTextView;->mShowHideCheckBox:Landroid/widget/CheckBox;

    invoke-virtual {v2, p0}, Landroid/widget/CheckBox;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f090114

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    sput-object v2, Lcom/android/mail/compose/QuotedTextView;->sQuoteBegin:Ljava/lang/String;

    const v2, 0x7f0800d5

    invoke-virtual {p0, v2}, Lcom/android/mail/compose/QuotedTextView;->findViewById(I)Landroid/view/View;

    move-result-object v2

    invoke-virtual {v2, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    const v2, 0x7f0800d6

    invoke-virtual {p0, v2}, Lcom/android/mail/compose/QuotedTextView;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/Button;

    iput-object v2, p0, Lcom/android/mail/compose/QuotedTextView;->mRespondInlineButton:Landroid/widget/Button;

    iget-object v2, p0, Lcom/android/mail/compose/QuotedTextView;->mRespondInlineButton:Landroid/widget/Button;

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/android/mail/compose/QuotedTextView;->mRespondInlineButton:Landroid/widget/Button;

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Landroid/widget/Button;->setEnabled(Z)V

    :cond_0
    return-void
.end method

.method public static containsQuotedText(Ljava/lang/String;)Z
    .locals 2
    .param p0    # Ljava/lang/String;

    sget-object v1, Lcom/android/mail/compose/QuotedTextView;->sQuoteBegin:Ljava/lang/String;

    invoke-virtual {p0, v1}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v0

    if-ltz v0, :cond_0

    const/4 v1, 0x1

    :goto_0
    return v1

    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public static findQuotedTextIndex(Ljava/lang/CharSequence;)I
    .locals 2
    .param p0    # Ljava/lang/CharSequence;

    invoke-static {p0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v1, -0x1

    :goto_0
    return v1

    :cond_0
    invoke-virtual {p0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    sget-object v1, Lcom/android/mail/compose/QuotedTextView;->sQuoteBegin:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v1

    goto :goto_0
.end method

.method private getHtmlText(Lcom/android/mail/providers/Message;)Ljava/lang/String;
    .locals 2
    .param p1    # Lcom/android/mail/providers/Message;

    iget-object v0, p1, Lcom/android/mail/providers/Message;->bodyHtml:Ljava/lang/String;

    if-eqz v0, :cond_0

    iget-object v0, p1, Lcom/android/mail/providers/Message;->bodyHtml:Ljava/lang/String;

    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p1, Lcom/android/mail/providers/Message;->bodyText:Ljava/lang/String;

    if-eqz v0, :cond_1

    new-instance v0, Landroid/text/SpannedString;

    iget-object v1, p1, Lcom/android/mail/providers/Message;->bodyText:Ljava/lang/String;

    invoke-direct {v0, v1}, Landroid/text/SpannedString;-><init>(Ljava/lang/CharSequence;)V

    invoke-static {v0}, Landroid/text/Html;->toHtml(Landroid/text/Spanned;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    :cond_1
    const-string v0, ""

    goto :goto_0
.end method

.method public static getQuotedTextOffset(Ljava/lang/String;)I
    .locals 2
    .param p0    # Ljava/lang/String;

    const-string v0, "<br type=\'attribution\'>"

    invoke-virtual {p0, v0}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v0

    sget v1, Lcom/android/mail/compose/QuotedTextView;->HEADER_SEPARATOR_LENGTH:I

    add-int/2addr v0, v1

    return v0
.end method

.method private populateData()V
    .locals 8

    const/4 v1, 0x0

    invoke-virtual {p0}, Lcom/android/mail/compose/QuotedTextView;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v3, 0x7f090017

    invoke-virtual {v0, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {p0}, Lcom/android/mail/compose/QuotedTextView;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v3, 0x7f090018

    invoke-virtual {v0, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v7

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "<head><style type=\"text/css\">* body { background-color: "

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v3, "; color: "

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v3, "; }</style></head>"

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v3, p0, Lcom/android/mail/compose/QuotedTextView;->mQuotedText:Ljava/lang/CharSequence;

    invoke-virtual {v3}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    iget-object v0, p0, Lcom/android/mail/compose/QuotedTextView;->mQuotedTextWebView:Landroid/webkit/WebView;

    const-string v3, "text/html"

    const-string v4, "utf-8"

    move-object v5, v1

    invoke-virtual/range {v0 .. v5}, Landroid/webkit/WebView;->loadDataWithBaseURL(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method private respondInline()V
    .locals 6

    const/16 v5, 0x8

    invoke-virtual {p0}, Lcom/android/mail/compose/QuotedTextView;->getQuotedText()Ljava/lang/CharSequence;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/android/mail/utils/Utils;->convertHtmlToPlainText(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iget-object v2, p0, Lcom/android/mail/compose/QuotedTextView;->mRespondInlineListener:Lcom/android/mail/compose/QuotedTextView$RespondInlineListener;

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/android/mail/compose/QuotedTextView;->mRespondInlineListener:Lcom/android/mail/compose/QuotedTextView$RespondInlineListener;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "\n"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-interface {v2, v3}, Lcom/android/mail/compose/QuotedTextView$RespondInlineListener;->onRespondInline(Ljava/lang/String;)V

    :cond_0
    const/4 v2, 0x0

    invoke-virtual {p0, v2}, Lcom/android/mail/compose/QuotedTextView;->updateCheckedState(Z)V

    iget-object v2, p0, Lcom/android/mail/compose/QuotedTextView;->mRespondInlineButton:Landroid/widget/Button;

    invoke-virtual {v2, v5}, Landroid/widget/Button;->setVisibility(I)V

    const v2, 0x7f0800d0

    invoke-virtual {p0, v2}, Lcom/android/mail/compose/QuotedTextView;->findViewById(I)Landroid/view/View;

    move-result-object v1

    if-eqz v1, :cond_1

    invoke-virtual {v1, v5}, Landroid/view/View;->setVisibility(I)V

    :cond_1
    return-void
.end method

.method private setQuotedText(Ljava/lang/CharSequence;)V
    .locals 3
    .param p1    # Ljava/lang/CharSequence;

    const/4 v2, 0x0

    iput-object p1, p0, Lcom/android/mail/compose/QuotedTextView;->mQuotedText:Ljava/lang/CharSequence;

    invoke-direct {p0}, Lcom/android/mail/compose/QuotedTextView;->populateData()V

    iget-object v0, p0, Lcom/android/mail/compose/QuotedTextView;->mRespondInlineButton:Landroid/widget/Button;

    if-eqz v0, :cond_0

    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/android/mail/compose/QuotedTextView;->mRespondInlineButton:Landroid/widget/Button;

    invoke-virtual {v0, v2}, Landroid/widget/Button;->setVisibility(I)V

    iget-object v0, p0, Lcom/android/mail/compose/QuotedTextView;->mRespondInlineButton:Landroid/widget/Button;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setEnabled(Z)V

    iget-object v0, p0, Lcom/android/mail/compose/QuotedTextView;->mRespondInlineButton:Landroid/widget/Button;

    invoke-virtual {v0, p0}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v0, p0, Lcom/android/mail/compose/QuotedTextView;->mRespondInlineButton:Landroid/widget/Button;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setVisibility(I)V

    iget-object v0, p0, Lcom/android/mail/compose/QuotedTextView;->mRespondInlineButton:Landroid/widget/Button;

    invoke-virtual {v0, v2}, Landroid/widget/Button;->setEnabled(Z)V

    goto :goto_0
.end method

.method private updateQuotedTextVisibility(Z)V
    .locals 2
    .param p1    # Z

    iget-object v1, p0, Lcom/android/mail/compose/QuotedTextView;->mQuotedTextWebView:Landroid/webkit/WebView;

    if-eqz p1, :cond_0

    const/4 v0, 0x0

    :goto_0
    invoke-virtual {v1, v0}, Landroid/webkit/WebView;->setVisibility(I)V

    iput-boolean p1, p0, Lcom/android/mail/compose/QuotedTextView;->mIncludeText:Z

    return-void

    :cond_0
    const/16 v0, 0x8

    goto :goto_0
.end method


# virtual methods
.method public allowQuotedText(Z)V
    .locals 2
    .param p1    # Z

    const v1, 0x7f0800d3

    invoke-virtual {p0, v1}, Lcom/android/mail/compose/QuotedTextView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    if-eqz v0, :cond_0

    if-eqz p1, :cond_1

    const/4 v1, 0x0

    :goto_0
    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    :cond_0
    return-void

    :cond_1
    const/4 v1, 0x4

    goto :goto_0
.end method

.method public allowRespondInline(Z)V
    .locals 2
    .param p1    # Z

    iget-object v0, p0, Lcom/android/mail/compose/QuotedTextView;->mRespondInlineButton:Landroid/widget/Button;

    if-eqz v0, :cond_0

    iget-object v1, p0, Lcom/android/mail/compose/QuotedTextView;->mRespondInlineButton:Landroid/widget/Button;

    if-eqz p1, :cond_1

    const/4 v0, 0x0

    :goto_0
    invoke-virtual {v1, v0}, Landroid/widget/Button;->setVisibility(I)V

    :cond_0
    return-void

    :cond_1
    const/16 v0, 0x8

    goto :goto_0
.end method

.method public getQuotedText()Ljava/lang/CharSequence;
    .locals 1

    iget-object v0, p0, Lcom/android/mail/compose/QuotedTextView;->mQuotedText:Ljava/lang/CharSequence;

    return-object v0
.end method

.method public getQuotedTextIfIncluded()Ljava/lang/CharSequence;
    .locals 1

    iget-boolean v0, p0, Lcom/android/mail/compose/QuotedTextView;->mIncludeText:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/mail/compose/QuotedTextView;->mQuotedText:Ljava/lang/CharSequence;

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isTextIncluded()Z
    .locals 1

    iget-boolean v0, p0, Lcom/android/mail/compose/QuotedTextView;->mIncludeText:Z

    return v0
.end method

.method public onClick(Landroid/view/View;)V
    .locals 1
    .param p1    # Landroid/view/View;

    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    :goto_0
    return-void

    :pswitch_0
    invoke-direct {p0}, Lcom/android/mail/compose/QuotedTextView;->respondInline()V

    goto :goto_0

    :pswitch_1
    iget-object v0, p0, Lcom/android/mail/compose/QuotedTextView;->mShowHideCheckBox:Landroid/widget/CheckBox;

    invoke-virtual {v0}, Landroid/widget/CheckBox;->isChecked()Z

    move-result v0

    invoke-virtual {p0, v0}, Lcom/android/mail/compose/QuotedTextView;->updateCheckedState(Z)V

    goto :goto_0

    :pswitch_2
    iget-object v0, p0, Lcom/android/mail/compose/QuotedTextView;->mShowHideCheckBox:Landroid/widget/CheckBox;

    invoke-virtual {v0}, Landroid/widget/CheckBox;->isChecked()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_1
    invoke-virtual {p0, v0}, Lcom/android/mail/compose/QuotedTextView;->updateCheckedState(Z)V

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    goto :goto_1

    nop

    :pswitch_data_0
    .packed-switch 0x7f0800d4
        :pswitch_1
        :pswitch_2
        :pswitch_0
    .end packed-switch
.end method

.method public setQuotedText(ILcom/android/mail/providers/Message;Z)V
    .locals 11
    .param p1    # I
    .param p2    # Lcom/android/mail/providers/Message;
    .param p3    # Z

    const/4 v6, 0x0

    invoke-virtual {p0, v6}, Lcom/android/mail/compose/QuotedTextView;->setVisibility(I)V

    invoke-direct {p0, p2}, Lcom/android/mail/compose/QuotedTextView;->getHtmlText(Lcom/android/mail/providers/Message;)Ljava/lang/String;

    move-result-object v3

    new-instance v4, Ljava/lang/StringBuffer;

    invoke-direct {v4}, Ljava/lang/StringBuffer;-><init>()V

    const/4 v6, 0x2

    const/4 v7, 0x3

    invoke-static {v6, v7}, Ljava/text/DateFormat;->getDateTimeInstance(II)Ljava/text/DateFormat;

    move-result-object v2

    new-instance v1, Ljava/util/Date;

    iget-wide v6, p2, Lcom/android/mail/providers/Message;->dateReceivedMs:J

    invoke-direct {v1, v6, v7}, Ljava/util/Date;-><init>(J)V

    invoke-virtual {p0}, Lcom/android/mail/compose/QuotedTextView;->getContext()Landroid/content/Context;

    move-result-object v6

    invoke-virtual {v6}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    if-eqz p1, :cond_0

    const/4 v6, 0x1

    if-ne p1, v6, :cond_2

    :cond_0
    sget-object v6, Lcom/android/mail/compose/QuotedTextView;->sQuoteBegin:Ljava/lang/String;

    invoke-virtual {v4, v6}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    const v6, 0x7f090039

    invoke-virtual {v5, v6}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v6

    const/4 v7, 0x2

    new-array v7, v7, [Ljava/lang/Object;

    const/4 v8, 0x0

    invoke-virtual {v2, v1}, Ljava/text/DateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v9

    aput-object v9, v7, v8

    const/4 v8, 0x1

    invoke-virtual {p2}, Lcom/android/mail/providers/Message;->getFrom()Ljava/lang/String;

    move-result-object v9

    const/4 v10, 0x1

    invoke-static {v9, v10}, Lcom/android/mail/utils/Utils;->cleanUpString(Ljava/lang/String;Z)Ljava/lang/Object;

    move-result-object v9

    aput-object v9, v7, v8

    invoke-static {v6, v7}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v4, v6}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    const-string v6, "<br type=\'attribution\'>"

    invoke-virtual {v4, v6}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    const-string v6, "<blockquote class=\"quote\" style=\"margin:0 0 0 .8ex;border-left:1px #ccc solid;padding-left:1ex\">"

    invoke-virtual {v4, v6}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    invoke-virtual {v4, v3}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    const-string v6, "</blockquote>"

    invoke-virtual {v4, v6}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    const-string v6, "</div>"

    invoke-virtual {v4, v6}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    :cond_1
    :goto_0
    invoke-direct {p0, v4}, Lcom/android/mail/compose/QuotedTextView;->setQuotedText(Ljava/lang/CharSequence;)V

    invoke-virtual {p0, p3}, Lcom/android/mail/compose/QuotedTextView;->allowQuotedText(Z)V

    const/4 v6, 0x1

    invoke-virtual {p0, v6}, Lcom/android/mail/compose/QuotedTextView;->allowRespondInline(Z)V

    return-void

    :cond_2
    const/4 v6, 0x2

    if-ne p1, v6, :cond_1

    sget-object v6, Lcom/android/mail/compose/QuotedTextView;->sQuoteBegin:Ljava/lang/String;

    invoke-virtual {v4, v6}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    const v6, 0x7f09003a

    invoke-virtual {v5, v6}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v6

    const/4 v7, 0x4

    new-array v7, v7, [Ljava/lang/Object;

    const/4 v8, 0x0

    invoke-virtual {p2}, Lcom/android/mail/providers/Message;->getFrom()Ljava/lang/String;

    move-result-object v9

    const/4 v10, 0x1

    invoke-static {v9, v10}, Lcom/android/mail/utils/Utils;->cleanUpString(Ljava/lang/String;Z)Ljava/lang/Object;

    move-result-object v9

    aput-object v9, v7, v8

    const/4 v8, 0x1

    invoke-virtual {v2, v1}, Ljava/text/DateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v9

    aput-object v9, v7, v8

    const/4 v8, 0x2

    iget-object v9, p2, Lcom/android/mail/providers/Message;->subject:Ljava/lang/String;

    const/4 v10, 0x0

    invoke-static {v9, v10}, Lcom/android/mail/utils/Utils;->cleanUpString(Ljava/lang/String;Z)Ljava/lang/Object;

    move-result-object v9

    aput-object v9, v7, v8

    const/4 v8, 0x3

    invoke-virtual {p2}, Lcom/android/mail/providers/Message;->getTo()Ljava/lang/String;

    move-result-object v9

    const/4 v10, 0x1

    invoke-static {v9, v10}, Lcom/android/mail/utils/Utils;->cleanUpString(Ljava/lang/String;Z)Ljava/lang/Object;

    move-result-object v9

    aput-object v9, v7, v8

    invoke-static {v6, v7}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v4, v6}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    invoke-virtual {p2}, Lcom/android/mail/providers/Message;->getCc()Ljava/lang/String;

    move-result-object v0

    const v6, 0x7f09003b

    invoke-virtual {v5, v6}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v6

    const/4 v7, 0x1

    new-array v7, v7, [Ljava/lang/Object;

    const/4 v8, 0x0

    const/4 v9, 0x1

    invoke-static {v0, v9}, Lcom/android/mail/utils/Utils;->cleanUpString(Ljava/lang/String;Z)Ljava/lang/Object;

    move-result-object v9

    aput-object v9, v7, v8

    invoke-static {v6, v7}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v4, v6}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    const-string v6, "<br type=\'attribution\'>"

    invoke-virtual {v4, v6}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    const-string v6, "<blockquote class=\"quote\" style=\"margin:0 0 0 .8ex;border-left:1px #ccc solid;padding-left:1ex\">"

    invoke-virtual {v4, v6}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    invoke-virtual {v4, v3}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    const-string v6, "</blockquote>"

    invoke-virtual {v4, v6}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    const-string v6, "</div>"

    invoke-virtual {v4, v6}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    goto/16 :goto_0
.end method

.method public setQuotedTextFromDraft(Ljava/lang/CharSequence;Z)V
    .locals 2
    .param p1    # Ljava/lang/CharSequence;
    .param p2    # Z

    const/4 v1, 0x1

    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/android/mail/compose/QuotedTextView;->setVisibility(I)V

    invoke-direct {p0, p1}, Lcom/android/mail/compose/QuotedTextView;->setQuotedText(Ljava/lang/CharSequence;)V

    if-nez p2, :cond_0

    move v0, v1

    :cond_0
    invoke-virtual {p0, v0}, Lcom/android/mail/compose/QuotedTextView;->allowQuotedText(Z)V

    invoke-virtual {p0, v1}, Lcom/android/mail/compose/QuotedTextView;->allowRespondInline(Z)V

    return-void
.end method

.method public setRespondInlineListener(Lcom/android/mail/compose/QuotedTextView$RespondInlineListener;)V
    .locals 0
    .param p1    # Lcom/android/mail/compose/QuotedTextView$RespondInlineListener;

    iput-object p1, p0, Lcom/android/mail/compose/QuotedTextView;->mRespondInlineListener:Lcom/android/mail/compose/QuotedTextView$RespondInlineListener;

    return-void
.end method

.method public setUpperDividerVisible(Z)V
    .locals 2
    .param p1    # Z

    const v0, 0x7f0800d1

    invoke-virtual {p0, v0}, Lcom/android/mail/compose/QuotedTextView;->findViewById(I)Landroid/view/View;

    move-result-object v1

    if-eqz p1, :cond_0

    const/4 v0, 0x0

    :goto_0
    invoke-virtual {v1, v0}, Landroid/view/View;->setVisibility(I)V

    return-void

    :cond_0
    const/16 v0, 0x8

    goto :goto_0
.end method

.method public updateCheckedState(Z)V
    .locals 1
    .param p1    # Z

    iget-object v0, p0, Lcom/android/mail/compose/QuotedTextView;->mShowHideCheckBox:Landroid/widget/CheckBox;

    invoke-virtual {v0, p1}, Landroid/widget/CheckBox;->setChecked(Z)V

    invoke-direct {p0, p1}, Lcom/android/mail/compose/QuotedTextView;->updateQuotedTextVisibility(Z)V

    iget-object v0, p0, Lcom/android/mail/compose/QuotedTextView;->mShowHideListener:Lcom/android/mail/compose/QuotedTextView$ShowHideQuotedTextListener;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/mail/compose/QuotedTextView;->mShowHideListener:Lcom/android/mail/compose/QuotedTextView$ShowHideQuotedTextListener;

    invoke-interface {v0, p1}, Lcom/android/mail/compose/QuotedTextView$ShowHideQuotedTextListener;->onShowHideQuotedText(Z)V

    :cond_0
    return-void
.end method
