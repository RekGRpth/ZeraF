.class public Lcom/android/mail/compose/ComposeActivity;
.super Landroid/app/Activity;
.source "ComposeActivity.java"

# interfaces
.implements Landroid/app/ActionBar$OnNavigationListener;
.implements Landroid/app/LoaderManager$LoaderCallbacks;
.implements Landroid/content/DialogInterface$OnClickListener;
.implements Landroid/text/TextWatcher;
.implements Landroid/view/View$OnClickListener;
.implements Landroid/widget/TextView$OnEditorActionListener;
.implements Lcom/android/mail/compose/AttachmentsView$AttachmentAddedOrDeletedListener;
.implements Lcom/android/mail/compose/FromAddressSpinner$OnAccountChangedListener;
.implements Lcom/android/mail/compose/QuotedTextView$RespondInlineListener;
.implements Lcom/android/mail/ui/FeedbackEnabledActivity;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/mail/compose/ComposeActivity$RecipientTextWatcher;,
        Lcom/android/mail/compose/ComposeActivity$ComposeModeAdapter;,
        Lcom/android/mail/compose/ComposeActivity$SendOrSaveMessage;,
        Lcom/android/mail/compose/ComposeActivity$SendOrSaveTask;,
        Lcom/android/mail/compose/ComposeActivity$SendOrSaveCallback;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/app/Activity;",
        "Landroid/view/View$OnClickListener;",
        "Landroid/app/ActionBar$OnNavigationListener;",
        "Lcom/android/mail/compose/QuotedTextView$RespondInlineListener;",
        "Landroid/content/DialogInterface$OnClickListener;",
        "Landroid/text/TextWatcher;",
        "Lcom/android/mail/compose/AttachmentsView$AttachmentAddedOrDeletedListener;",
        "Lcom/android/mail/compose/FromAddressSpinner$OnAccountChangedListener;",
        "Landroid/app/LoaderManager$LoaderCallbacks",
        "<",
        "Landroid/database/Cursor;",
        ">;",
        "Landroid/widget/TextView$OnEditorActionListener;",
        "Lcom/android/mail/ui/FeedbackEnabledActivity;"
    }
.end annotation


# static fields
.field static final ALL_EXTRAS:[Ljava/lang/String;

.field private static final EXTRA_FOCUS_SELECTION_END:Ljava/lang/String;

.field private static final LOG_TAG:Ljava/lang/String;

.field private static sRequestMessageIdMap:Ljava/util/concurrent/ConcurrentHashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/concurrent/ConcurrentHashMap",
            "<",
            "Ljava/lang/Integer;",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation
.end field

.field private static sTestSendOrSaveCallback:Lcom/android/mail/compose/ComposeActivity$SendOrSaveCallback;


# instance fields
.field private mAccount:Lcom/android/mail/providers/Account;

.field private mAccounts:[Lcom/android/mail/providers/Account;

.field public mActiveTasks:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/android/mail/compose/ComposeActivity$SendOrSaveTask;",
            ">;"
        }
    .end annotation
.end field

.field private mAddingAttachment:Z

.field private mAttachmentsChanged:Z

.field private mAttachmentsView:Lcom/android/mail/compose/AttachmentsView;

.field private mBcc:Lcom/android/ex/chips/RecipientEditTextView;

.field private mBccListener:Lcom/android/mail/compose/ComposeActivity$RecipientTextWatcher;

.field private mBodyView:Landroid/widget/EditText;

.field private mCachedSettings:Lcom/android/mail/providers/Settings;

.field private mCc:Lcom/android/ex/chips/RecipientEditTextView;

.field private mCcBccButton:Landroid/widget/Button;

.field private mCcBccView:Lcom/android/mail/compose/CcBccView;

.field private mCcListener:Lcom/android/mail/compose/ComposeActivity$RecipientTextWatcher;

.field private mComposeMode:I

.field private mComposeModeAdapter:Lcom/android/mail/compose/ComposeActivity$ComposeModeAdapter;

.field private mDraft:Lcom/android/mail/providers/Message;

.field private mDraftAccount:Lcom/android/mail/providers/ReplyFromAccount;

.field private mDraftId:J

.field private mDraftLock:Ljava/lang/Object;

.field private mForward:Z

.field protected mFromSpinner:Lcom/android/mail/compose/FromAddressSpinner;

.field private mFromSpinnerWrapper:Landroid/view/View;

.field private mFromStatic:Landroid/view/View;

.field private mFromStaticText:Landroid/widget/TextView;

.field private mLaunchedFromEmail:Z

.field private mPhotoAttachmentsButton:Landroid/view/View;

.field private mQuotedTextView:Lcom/android/mail/compose/QuotedTextView;

.field private mRecipient:Ljava/lang/String;

.field private mRecipientErrorDialog:Landroid/app/AlertDialog;

.field protected mRefMessage:Lcom/android/mail/providers/Message;

.field private mRefMessageUri:Landroid/net/Uri;

.field private mReplyFromAccount:Lcom/android/mail/providers/ReplyFromAccount;

.field private mReplyFromChanged:Z

.field private mRequestId:I

.field private mRespondedInline:Z

.field private mSave:Landroid/view/MenuItem;

.field private mSavedInstanceState:Landroid/os/Bundle;

.field private mSend:Landroid/view/MenuItem;

.field private mSendConfirmDialog:Landroid/app/AlertDialog;

.field private mSendSaveTaskHandler:Landroid/os/Handler;

.field private mSignature:Ljava/lang/String;

.field private mSubject:Landroid/widget/TextView;

.field private mTextChanged:Z

.field private mTo:Lcom/android/ex/chips/RecipientEditTextView;

.field private mToListener:Lcom/android/mail/compose/ComposeActivity$RecipientTextWatcher;

.field private mValidator:Lcom/android/common/Rfc822Validator;

.field private mVideoAttachmentsButton:Landroid/view/View;


# direct methods
.method static constructor <clinit>()V
    .locals 4

    const/4 v3, 0x0

    const/4 v0, 0x5

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v2, "subject"

    aput-object v2, v0, v1

    const/4 v1, 0x1

    const-string v2, "body"

    aput-object v2, v0, v1

    const/4 v1, 0x2

    const-string v2, "to"

    aput-object v2, v0, v1

    const/4 v1, 0x3

    const-string v2, "cc"

    aput-object v2, v0, v1

    const/4 v1, 0x4

    const-string v2, "bcc"

    aput-object v2, v0, v1

    sput-object v0, Lcom/android/mail/compose/ComposeActivity;->ALL_EXTRAS:[Ljava/lang/String;

    sput-object v3, Lcom/android/mail/compose/ComposeActivity;->sTestSendOrSaveCallback:Lcom/android/mail/compose/ComposeActivity$SendOrSaveCallback;

    sput-object v3, Lcom/android/mail/compose/ComposeActivity;->sRequestMessageIdMap:Ljava/util/concurrent/ConcurrentHashMap;

    invoke-static {}, Lcom/android/mail/utils/LogTag;->getLogTag()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/android/mail/compose/ComposeActivity;->LOG_TAG:Ljava/lang/String;

    sput-object v3, Lcom/android/mail/compose/ComposeActivity;->EXTRA_FOCUS_SELECTION_END:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 2

    invoke-direct {p0}, Landroid/app/Activity;-><init>()V

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/android/mail/compose/ComposeActivity;->mSendSaveTaskHandler:Landroid/os/Handler;

    const/4 v0, -0x1

    iput v0, p0, Lcom/android/mail/compose/ComposeActivity;->mComposeMode:I

    const-wide/16 v0, -0x1

    iput-wide v0, p0, Lcom/android/mail/compose/ComposeActivity;->mDraftId:J

    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lcom/android/mail/compose/ComposeActivity;->mDraftLock:Ljava/lang/Object;

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/android/mail/compose/ComposeActivity;->mLaunchedFromEmail:Z

    invoke-static {}, Lcom/google/common/collect/Lists;->newArrayList()Ljava/util/ArrayList;

    move-result-object v0

    iput-object v0, p0, Lcom/android/mail/compose/ComposeActivity;->mActiveTasks:Ljava/util/ArrayList;

    return-void
.end method

.method static synthetic access$000()Ljava/lang/String;
    .locals 1

    sget-object v0, Lcom/android/mail/compose/ComposeActivity;->LOG_TAG:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$100(Lcom/android/mail/compose/ComposeActivity;)Lcom/android/ex/chips/RecipientEditTextView;
    .locals 1
    .param p0    # Lcom/android/mail/compose/ComposeActivity;

    iget-object v0, p0, Lcom/android/mail/compose/ComposeActivity;->mTo:Lcom/android/ex/chips/RecipientEditTextView;

    return-object v0
.end method

.method static synthetic access$1000()Ljava/util/concurrent/ConcurrentHashMap;
    .locals 1

    sget-object v0, Lcom/android/mail/compose/ComposeActivity;->sRequestMessageIdMap:Ljava/util/concurrent/ConcurrentHashMap;

    return-object v0
.end method

.method static synthetic access$1100(Lcom/android/mail/compose/ComposeActivity;)V
    .locals 0
    .param p0    # Lcom/android/mail/compose/ComposeActivity;

    invoke-direct {p0}, Lcom/android/mail/compose/ComposeActivity;->saveRequestMap()V

    return-void
.end method

.method static synthetic access$1200(Lcom/android/mail/compose/ComposeActivity;)Lcom/android/mail/providers/Account;
    .locals 1
    .param p0    # Lcom/android/mail/compose/ComposeActivity;

    iget-object v0, p0, Lcom/android/mail/compose/ComposeActivity;->mAccount:Lcom/android/mail/providers/Account;

    return-object v0
.end method

.method static synthetic access$1300(Lcom/android/mail/compose/ComposeActivity;)V
    .locals 0
    .param p0    # Lcom/android/mail/compose/ComposeActivity;

    invoke-direct {p0}, Lcom/android/mail/compose/ComposeActivity;->discardChanges()V

    return-void
.end method

.method static synthetic access$202(Lcom/android/mail/compose/ComposeActivity;Landroid/app/AlertDialog;)Landroid/app/AlertDialog;
    .locals 0
    .param p0    # Lcom/android/mail/compose/ComposeActivity;
    .param p1    # Landroid/app/AlertDialog;

    iput-object p1, p0, Lcom/android/mail/compose/ComposeActivity;->mRecipientErrorDialog:Landroid/app/AlertDialog;

    return-object p1
.end method

.method static synthetic access$300(Lcom/android/mail/compose/ComposeActivity;)Landroid/widget/EditText;
    .locals 1
    .param p0    # Lcom/android/mail/compose/ComposeActivity;

    iget-object v0, p0, Lcom/android/mail/compose/ComposeActivity;->mBodyView:Landroid/widget/EditText;

    return-object v0
.end method

.method static synthetic access$400(Lcom/android/mail/compose/ComposeActivity;Landroid/text/Spanned;ZZZ)V
    .locals 0
    .param p0    # Lcom/android/mail/compose/ComposeActivity;
    .param p1    # Landroid/text/Spanned;
    .param p2    # Z
    .param p3    # Z
    .param p4    # Z

    invoke-direct {p0, p1, p2, p3, p4}, Lcom/android/mail/compose/ComposeActivity;->sendOrSave(Landroid/text/Spanned;ZZZ)V

    return-void
.end method

.method static synthetic access$500()Lcom/android/mail/compose/ComposeActivity$SendOrSaveCallback;
    .locals 1

    sget-object v0, Lcom/android/mail/compose/ComposeActivity;->sTestSendOrSaveCallback:Lcom/android/mail/compose/ComposeActivity$SendOrSaveCallback;

    return-object v0
.end method

.method static synthetic access$600(Lcom/android/mail/compose/ComposeActivity;)Ljava/lang/Object;
    .locals 1
    .param p0    # Lcom/android/mail/compose/ComposeActivity;

    iget-object v0, p0, Lcom/android/mail/compose/ComposeActivity;->mDraftLock:Ljava/lang/Object;

    return-object v0
.end method

.method static synthetic access$702(Lcom/android/mail/compose/ComposeActivity;Lcom/android/mail/providers/ReplyFromAccount;)Lcom/android/mail/providers/ReplyFromAccount;
    .locals 0
    .param p0    # Lcom/android/mail/compose/ComposeActivity;
    .param p1    # Lcom/android/mail/providers/ReplyFromAccount;

    iput-object p1, p0, Lcom/android/mail/compose/ComposeActivity;->mDraftAccount:Lcom/android/mail/providers/ReplyFromAccount;

    return-object p1
.end method

.method static synthetic access$800(Lcom/android/mail/compose/ComposeActivity;)J
    .locals 2
    .param p0    # Lcom/android/mail/compose/ComposeActivity;

    iget-wide v0, p0, Lcom/android/mail/compose/ComposeActivity;->mDraftId:J

    return-wide v0
.end method

.method static synthetic access$802(Lcom/android/mail/compose/ComposeActivity;J)J
    .locals 0
    .param p0    # Lcom/android/mail/compose/ComposeActivity;
    .param p1    # J

    iput-wide p1, p0, Lcom/android/mail/compose/ComposeActivity;->mDraftId:J

    return-wide p1
.end method

.method static synthetic access$900(Lcom/android/mail/compose/ComposeActivity;)Lcom/android/mail/providers/Message;
    .locals 1
    .param p0    # Lcom/android/mail/compose/ComposeActivity;

    iget-object v0, p0, Lcom/android/mail/compose/ComposeActivity;->mDraft:Lcom/android/mail/providers/Message;

    return-object v0
.end method

.method static synthetic access$902(Lcom/android/mail/compose/ComposeActivity;Lcom/android/mail/providers/Message;)Lcom/android/mail/providers/Message;
    .locals 0
    .param p0    # Lcom/android/mail/compose/ComposeActivity;
    .param p1    # Lcom/android/mail/providers/Message;

    iput-object p1, p0, Lcom/android/mail/compose/ComposeActivity;->mDraft:Lcom/android/mail/providers/Message;

    return-object p1
.end method

.method private addAddressToList(Ljava/lang/String;Lcom/android/ex/chips/RecipientEditTextView;)V
    .locals 4

    if-eqz p1, :cond_0

    if-nez p2, :cond_1

    :cond_0
    return-void

    :cond_1
    invoke-static {p1}, Landroid/text/util/Rfc822Tokenizer;->tokenize(Ljava/lang/CharSequence;)[Landroid/text/util/Rfc822Token;

    move-result-object v1

    const/4 v0, 0x0

    :goto_0
    array-length v2, v1

    if-ge v0, v2, :cond_0

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    aget-object v3, v1, v0

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ", "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p2, v2}, Lcom/android/ex/chips/RecipientEditTextView;->append(Ljava/lang/CharSequence;)V

    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method

.method private addBccAddresses(Ljava/util/Collection;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Collection",
            "<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    iget-object v0, p0, Lcom/android/mail/compose/ComposeActivity;->mBcc:Lcom/android/ex/chips/RecipientEditTextView;

    invoke-virtual {p0, p1, v0}, Lcom/android/mail/compose/ComposeActivity;->addAddressesToList(Ljava/util/Collection;Lcom/android/ex/chips/RecipientEditTextView;)V

    return-void
.end method

.method private addCcAddresses(Ljava/util/Collection;Ljava/util/Collection;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Collection",
            "<",
            "Ljava/lang/String;",
            ">;",
            "Ljava/util/Collection",
            "<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    invoke-virtual {p0, p1}, Lcom/android/mail/compose/ComposeActivity;->tokenizeAddressList(Ljava/util/Collection;)Ljava/util/List;

    move-result-object v1

    if-eqz p2, :cond_0

    invoke-virtual {p0, p2}, Lcom/android/mail/compose/ComposeActivity;->tokenizeAddressList(Ljava/util/Collection;)Ljava/util/List;

    move-result-object v0

    :goto_0
    iget-object v2, p0, Lcom/android/mail/compose/ComposeActivity;->mCc:Lcom/android/ex/chips/RecipientEditTextView;

    invoke-virtual {p0, v1, v0, v2}, Lcom/android/mail/compose/ComposeActivity;->addCcAddressesToList(Ljava/util/List;Ljava/util/List;Lcom/android/ex/chips/RecipientEditTextView;)V

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private addRecipients(Ljava/lang/String;Ljava/util/Set;[Ljava/lang/String;)V
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;[",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    array-length v1, p3

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v1, :cond_1

    aget-object v2, p3, v0

    invoke-static {v2}, Lcom/android/mail/providers/Address;->getEmailAddress(Ljava/lang/String;)Lcom/android/mail/providers/Address;

    move-result-object v3

    invoke-virtual {v3}, Lcom/android/mail/providers/Address;->getAddress()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p0, v3}, Lcom/android/mail/compose/ComposeActivity;->recipientMatchesThisAccount(Ljava/lang/String;)Z

    move-result v3

    if-nez v3, :cond_0

    const-string v3, "\"\""

    const-string v4, ""

    invoke-virtual {v2, v3, v4}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v2

    invoke-interface {p2, v2}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_1
    return-void
.end method

.method private addToAddresses(Ljava/util/Collection;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Collection",
            "<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    iget-object v0, p0, Lcom/android/mail/compose/ComposeActivity;->mTo:Lcom/android/ex/chips/RecipientEditTextView;

    invoke-virtual {p0, p1, v0}, Lcom/android/mail/compose/ComposeActivity;->addAddressesToList(Ljava/util/Collection;Lcom/android/ex/chips/RecipientEditTextView;)V

    return-void
.end method

.method private appendSignature()V
    .locals 5

    iget-object v3, p0, Lcom/android/mail/compose/ComposeActivity;->mCachedSettings:Lcom/android/mail/providers/Settings;

    if-eqz v3, :cond_3

    iget-object v3, p0, Lcom/android/mail/compose/ComposeActivity;->mCachedSettings:Lcom/android/mail/providers/Settings;

    iget-object v1, v3, Lcom/android/mail/providers/Settings;->signature:Ljava/lang/String;

    :goto_0
    iget-object v3, p0, Lcom/android/mail/compose/ComposeActivity;->mBodyView:Landroid/widget/EditText;

    invoke-virtual {v3}, Landroid/widget/EditText;->hasFocus()Z

    move-result v0

    iget-object v3, p0, Lcom/android/mail/compose/ComposeActivity;->mSignature:Ljava/lang/String;

    iget-object v4, p0, Lcom/android/mail/compose/ComposeActivity;->mBodyView:Landroid/widget/EditText;

    invoke-virtual {v4}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p0, v3, v4}, Lcom/android/mail/compose/ComposeActivity;->getSignatureStartPosition(Ljava/lang/String;Ljava/lang/String;)I

    move-result v2

    iget-object v3, p0, Lcom/android/mail/compose/ComposeActivity;->mSignature:Ljava/lang/String;

    invoke-static {v1, v3}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v3

    if-eqz v3, :cond_0

    if-gez v2, :cond_2

    :cond_0
    iput-object v1, p0, Lcom/android/mail/compose/ComposeActivity;->mSignature:Ljava/lang/String;

    iget-object v3, p0, Lcom/android/mail/compose/ComposeActivity;->mSignature:Ljava/lang/String;

    invoke-static {v3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_1

    iget-object v3, p0, Lcom/android/mail/compose/ComposeActivity;->mBodyView:Landroid/widget/EditText;

    invoke-virtual {v3, p0}, Landroid/widget/EditText;->removeTextChangedListener(Landroid/text/TextWatcher;)V

    iget-object v3, p0, Lcom/android/mail/compose/ComposeActivity;->mBodyView:Landroid/widget/EditText;

    iget-object v4, p0, Lcom/android/mail/compose/ComposeActivity;->mSignature:Ljava/lang/String;

    invoke-direct {p0, v4}, Lcom/android/mail/compose/ComposeActivity;->convertToPrintableSignature(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/widget/EditText;->append(Ljava/lang/CharSequence;)V

    iget-object v3, p0, Lcom/android/mail/compose/ComposeActivity;->mBodyView:Landroid/widget/EditText;

    invoke-virtual {v3, p0}, Landroid/widget/EditText;->addTextChangedListener(Landroid/text/TextWatcher;)V

    :cond_1
    if-eqz v0, :cond_2

    invoke-virtual {p0}, Lcom/android/mail/compose/ComposeActivity;->focusBody()V

    :cond_2
    return-void

    :cond_3
    const/4 v1, 0x0

    goto :goto_0
.end method

.method private checkValidAccounts()V
    .locals 10

    const/4 v9, 0x0

    invoke-static {p0}, Lcom/android/mail/utils/AccountUtils;->getAccounts(Landroid/content/Context;)[Lcom/android/mail/providers/Account;

    move-result-object v1

    if-eqz v1, :cond_0

    array-length v7, v1

    if-nez v7, :cond_2

    :cond_0
    invoke-static {p0}, Lcom/android/mail/providers/MailAppProvider;->getNoAccountIntent(Landroid/content/Context;)Landroid/content/Intent;

    move-result-object v6

    if-eqz v6, :cond_1

    iput-object v9, p0, Lcom/android/mail/compose/ComposeActivity;->mAccounts:[Lcom/android/mail/providers/Account;

    const/4 v7, 0x2

    invoke-virtual {p0, v6, v7}, Lcom/android/mail/compose/ComposeActivity;->startActivityForResult(Landroid/content/Intent;I)V

    :cond_1
    :goto_0
    return-void

    :cond_2
    const/4 v2, 0x0

    move-object v3, v1

    array-length v5, v3

    const/4 v4, 0x0

    :goto_1
    if-ge v4, v5, :cond_3

    aget-object v0, v3, v4

    invoke-virtual {v0}, Lcom/android/mail/providers/Account;->isAccountReady()Z

    move-result v7

    if-eqz v7, :cond_4

    const/4 v2, 0x1

    :cond_3
    if-nez v2, :cond_5

    iput-object v9, p0, Lcom/android/mail/compose/ComposeActivity;->mAccounts:[Lcom/android/mail/providers/Account;

    invoke-virtual {p0}, Lcom/android/mail/compose/ComposeActivity;->getLoaderManager()Landroid/app/LoaderManager;

    move-result-object v7

    const/4 v8, 0x1

    invoke-virtual {v7, v8, v9, p0}, Landroid/app/LoaderManager;->initLoader(ILandroid/os/Bundle;Landroid/app/LoaderManager$LoaderCallbacks;)Landroid/content/Loader;

    goto :goto_0

    :cond_4
    add-int/lit8 v4, v4, 0x1

    goto :goto_1

    :cond_5
    invoke-static {p0}, Lcom/android/mail/utils/AccountUtils;->getSyncingAccounts(Landroid/content/Context;)[Lcom/android/mail/providers/Account;

    move-result-object v7

    iput-object v7, p0, Lcom/android/mail/compose/ComposeActivity;->mAccounts:[Lcom/android/mail/providers/Account;

    invoke-direct {p0}, Lcom/android/mail/compose/ComposeActivity;->finishCreate()V

    goto :goto_0
.end method

.method private clearChangeListeners()V
    .locals 3

    const/4 v2, 0x0

    iget-object v0, p0, Lcom/android/mail/compose/ComposeActivity;->mSubject:Landroid/widget/TextView;

    invoke-virtual {v0, p0}, Landroid/widget/TextView;->removeTextChangedListener(Landroid/text/TextWatcher;)V

    iget-object v0, p0, Lcom/android/mail/compose/ComposeActivity;->mBodyView:Landroid/widget/EditText;

    invoke-virtual {v0, p0}, Landroid/widget/EditText;->removeTextChangedListener(Landroid/text/TextWatcher;)V

    iget-object v0, p0, Lcom/android/mail/compose/ComposeActivity;->mTo:Lcom/android/ex/chips/RecipientEditTextView;

    iget-object v1, p0, Lcom/android/mail/compose/ComposeActivity;->mToListener:Lcom/android/mail/compose/ComposeActivity$RecipientTextWatcher;

    invoke-virtual {v0, v1}, Lcom/android/ex/chips/RecipientEditTextView;->removeTextChangedListener(Landroid/text/TextWatcher;)V

    iget-object v0, p0, Lcom/android/mail/compose/ComposeActivity;->mCc:Lcom/android/ex/chips/RecipientEditTextView;

    iget-object v1, p0, Lcom/android/mail/compose/ComposeActivity;->mCcListener:Lcom/android/mail/compose/ComposeActivity$RecipientTextWatcher;

    invoke-virtual {v0, v1}, Lcom/android/ex/chips/RecipientEditTextView;->removeTextChangedListener(Landroid/text/TextWatcher;)V

    iget-object v0, p0, Lcom/android/mail/compose/ComposeActivity;->mBcc:Lcom/android/ex/chips/RecipientEditTextView;

    iget-object v1, p0, Lcom/android/mail/compose/ComposeActivity;->mBccListener:Lcom/android/mail/compose/ComposeActivity$RecipientTextWatcher;

    invoke-virtual {v0, v1}, Lcom/android/ex/chips/RecipientEditTextView;->removeTextChangedListener(Landroid/text/TextWatcher;)V

    iget-object v0, p0, Lcom/android/mail/compose/ComposeActivity;->mFromSpinner:Lcom/android/mail/compose/FromAddressSpinner;

    invoke-virtual {v0, v2}, Lcom/android/mail/compose/FromAddressSpinner;->setOnAccountChangedListener(Lcom/android/mail/compose/FromAddressSpinner$OnAccountChangedListener;)V

    iget-object v0, p0, Lcom/android/mail/compose/ComposeActivity;->mAttachmentsView:Lcom/android/mail/compose/AttachmentsView;

    invoke-virtual {v0, v2}, Lcom/android/mail/compose/AttachmentsView;->setAttachmentChangesListener(Lcom/android/mail/compose/AttachmentsView$AttachmentAddedOrDeletedListener;)V

    return-void
.end method

.method public static compose(Landroid/content/Context;Lcom/android/mail/providers/Account;)V
    .locals 2
    .param p0    # Landroid/content/Context;
    .param p1    # Lcom/android/mail/providers/Account;

    const/4 v0, 0x0

    const/4 v1, -0x1

    invoke-static {p0, p1, v0, v1}, Lcom/android/mail/compose/ComposeActivity;->launch(Landroid/content/Context;Lcom/android/mail/providers/Account;Lcom/android/mail/providers/Message;I)V

    return-void
.end method

.method private convertToHashSet(Ljava/util/List;)Ljava/util/HashSet;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<[",
            "Landroid/text/util/Rfc822Token;",
            ">;)",
            "Ljava/util/HashSet",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, [Landroid/text/util/Rfc822Token;

    const/4 v1, 0x0

    :goto_0
    array-length v4, v3

    if-ge v1, v4, :cond_0

    aget-object v4, v3, v1

    invoke-virtual {v4}, Landroid/text/util/Rfc822Token;->getAddress()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0, v4}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_1
    return-object v0
.end method

.method private convertToPrintableSignature(Ljava/lang/String;)Ljava/lang/String;
    .locals 3

    invoke-virtual {p0}, Lcom/android/mail/compose/ComposeActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f090048

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    if-nez p1, :cond_0

    const-string p1, ""

    :cond_0
    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    aput-object p1, v1, v2

    invoke-static {v0, v1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private static createActionIntent(Landroid/content/Context;Lcom/android/mail/providers/Account;Landroid/net/Uri;I)Landroid/content/Intent;
    .locals 3

    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/android/mail/compose/ComposeActivity;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const-string v1, "fromemail"

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    const-string v1, "action"

    invoke-virtual {v0, v1, p3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    const-string v1, "account"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    const-string v1, "in-reference-to-message-uri"

    invoke-virtual {v0, v1, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    return-object v0
.end method

.method public static createForwardIntent(Landroid/content/Context;Lcom/android/mail/providers/Account;Landroid/net/Uri;)Landroid/content/Intent;
    .locals 1
    .param p0    # Landroid/content/Context;
    .param p1    # Lcom/android/mail/providers/Account;
    .param p2    # Landroid/net/Uri;

    const/4 v0, 0x2

    invoke-static {p0, p1, p2, v0}, Lcom/android/mail/compose/ComposeActivity;->createActionIntent(Landroid/content/Context;Lcom/android/mail/providers/Account;Landroid/net/Uri;I)Landroid/content/Intent;

    move-result-object v0

    return-object v0
.end method

.method private createMessage(Lcom/android/mail/providers/ReplyFromAccount;I)Lcom/android/mail/providers/Message;
    .locals 13
    .param p1    # Lcom/android/mail/providers/ReplyFromAccount;
    .param p2    # I

    const-wide/16 v11, 0x0

    const/4 v7, 0x1

    const/4 v8, 0x0

    const/4 v6, 0x0

    new-instance v3, Lcom/android/mail/providers/Message;

    invoke-direct {v3}, Lcom/android/mail/providers/Message;-><init>()V

    const-wide/16 v9, -0x1

    iput-wide v9, v3, Lcom/android/mail/providers/Message;->id:J

    iput-object v6, v3, Lcom/android/mail/providers/Message;->serverId:Ljava/lang/String;

    iput-object v6, v3, Lcom/android/mail/providers/Message;->uri:Landroid/net/Uri;

    iput-object v6, v3, Lcom/android/mail/providers/Message;->conversationUri:Landroid/net/Uri;

    iget-object v5, p0, Lcom/android/mail/compose/ComposeActivity;->mSubject:Landroid/widget/TextView;

    invoke-virtual {v5}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v5

    iput-object v5, v3, Lcom/android/mail/providers/Message;->subject:Ljava/lang/String;

    iput-object v6, v3, Lcom/android/mail/providers/Message;->snippet:Ljava/lang/String;

    iget-object v5, p0, Lcom/android/mail/compose/ComposeActivity;->mTo:Lcom/android/ex/chips/RecipientEditTextView;

    invoke-virtual {v5}, Lcom/android/ex/chips/RecipientEditTextView;->getText()Landroid/text/Editable;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-direct {p0, v5}, Lcom/android/mail/compose/ComposeActivity;->formatSenders(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v3, v5}, Lcom/android/mail/providers/Message;->setTo(Ljava/lang/String;)V

    iget-object v5, p0, Lcom/android/mail/compose/ComposeActivity;->mCc:Lcom/android/ex/chips/RecipientEditTextView;

    invoke-virtual {v5}, Lcom/android/ex/chips/RecipientEditTextView;->getText()Landroid/text/Editable;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-direct {p0, v5}, Lcom/android/mail/compose/ComposeActivity;->formatSenders(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v3, v5}, Lcom/android/mail/providers/Message;->setCc(Ljava/lang/String;)V

    iget-object v5, p0, Lcom/android/mail/compose/ComposeActivity;->mBcc:Lcom/android/ex/chips/RecipientEditTextView;

    invoke-virtual {v5}, Lcom/android/ex/chips/RecipientEditTextView;->getText()Landroid/text/Editable;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-direct {p0, v5}, Lcom/android/mail/compose/ComposeActivity;->formatSenders(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v3, v5}, Lcom/android/mail/providers/Message;->setBcc(Ljava/lang/String;)V

    invoke-virtual {v3, v6}, Lcom/android/mail/providers/Message;->setReplyTo(Ljava/lang/String;)V

    iput-wide v11, v3, Lcom/android/mail/providers/Message;->dateReceivedMs:J

    iget-object v5, p0, Lcom/android/mail/compose/ComposeActivity;->mBodyView:Landroid/widget/EditText;

    invoke-virtual {v5}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v5

    invoke-static {v5}, Lcom/android/mail/compose/ComposeActivity;->removeComposingSpans(Landroid/text/Spanned;)Landroid/text/SpannableString;

    move-result-object v5

    invoke-static {v5}, Landroid/text/Html;->toHtml(Landroid/text/Spanned;)Ljava/lang/String;

    move-result-object v2

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    iput-object v5, v3, Lcom/android/mail/providers/Message;->bodyHtml:Ljava/lang/String;

    iget-object v5, p0, Lcom/android/mail/compose/ComposeActivity;->mBodyView:Landroid/widget/EditText;

    invoke-virtual {v5}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v5

    iput-object v5, v3, Lcom/android/mail/providers/Message;->bodyText:Ljava/lang/String;

    iput-boolean v8, v3, Lcom/android/mail/providers/Message;->embedsExternalResources:Z

    iget-object v5, p0, Lcom/android/mail/compose/ComposeActivity;->mRefMessage:Lcom/android/mail/providers/Message;

    if-eqz v5, :cond_1

    iget-object v5, p0, Lcom/android/mail/compose/ComposeActivity;->mRefMessage:Lcom/android/mail/providers/Message;

    iget-object v5, v5, Lcom/android/mail/providers/Message;->uri:Landroid/net/Uri;

    invoke-virtual {v5}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v5

    :goto_0
    iput-object v5, v3, Lcom/android/mail/providers/Message;->refMessageId:Ljava/lang/String;

    iget-object v5, p0, Lcom/android/mail/compose/ComposeActivity;->mQuotedTextView:Lcom/android/mail/compose/QuotedTextView;

    invoke-virtual {v5}, Lcom/android/mail/compose/QuotedTextView;->getQuotedTextIfIncluded()Ljava/lang/CharSequence;

    move-result-object v5

    if-eqz v5, :cond_2

    move v5, v7

    :goto_1
    iput-boolean v5, v3, Lcom/android/mail/providers/Message;->appendRefMessageContent:Z

    iget-object v5, p0, Lcom/android/mail/compose/ComposeActivity;->mAttachmentsView:Lcom/android/mail/compose/AttachmentsView;

    invoke-virtual {v5}, Lcom/android/mail/compose/AttachmentsView;->getAttachments()Ljava/util/ArrayList;

    move-result-object v0

    if-eqz v0, :cond_3

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v5

    if-lez v5, :cond_3

    :goto_2
    iput-boolean v7, v3, Lcom/android/mail/providers/Message;->hasAttachments:Z

    iput-object v6, v3, Lcom/android/mail/providers/Message;->attachmentListUri:Landroid/net/Uri;

    iput-wide v11, v3, Lcom/android/mail/providers/Message;->messageFlags:J

    iput-object v6, v3, Lcom/android/mail/providers/Message;->saveUri:Ljava/lang/String;

    iput-object v6, v3, Lcom/android/mail/providers/Message;->sendUri:Ljava/lang/String;

    iput-boolean v8, v3, Lcom/android/mail/providers/Message;->alwaysShowImages:Z

    invoke-static {v0}, Lcom/android/mail/providers/Attachment;->toJSONArray(Ljava/util/Collection;)Ljava/lang/String;

    move-result-object v5

    iput-object v5, v3, Lcom/android/mail/providers/Message;->attachmentsJson:Ljava/lang/String;

    iget-object v5, p0, Lcom/android/mail/compose/ComposeActivity;->mQuotedTextView:Lcom/android/mail/compose/QuotedTextView;

    invoke-virtual {v5}, Lcom/android/mail/compose/QuotedTextView;->getQuotedText()Ljava/lang/CharSequence;

    move-result-object v4

    invoke-static {v4}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v5

    if-nez v5, :cond_4

    invoke-virtual {v4}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Lcom/android/mail/compose/QuotedTextView;->getQuotedTextOffset(Ljava/lang/String;)I

    move-result v5

    :goto_3
    iput v5, v3, Lcom/android/mail/providers/Message;->quotedTextOffset:I

    iput-object v6, v3, Lcom/android/mail/providers/Message;->accountUri:Landroid/net/Uri;

    if-eqz p1, :cond_5

    iget-object v6, p1, Lcom/android/mail/providers/ReplyFromAccount;->address:Ljava/lang/String;

    :cond_0
    :goto_4
    invoke-virtual {v3, v6}, Lcom/android/mail/providers/Message;->setFrom(Ljava/lang/String;)V

    invoke-static {p2}, Lcom/android/mail/compose/ComposeActivity;->getDraftType(I)I

    move-result v5

    iput v5, v3, Lcom/android/mail/providers/Message;->draftType:I

    iget-object v5, p0, Lcom/android/mail/compose/ComposeActivity;->mTo:Lcom/android/ex/chips/RecipientEditTextView;

    invoke-virtual {v5}, Lcom/android/ex/chips/RecipientEditTextView;->getText()Landroid/text/Editable;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-direct {p0, v5}, Lcom/android/mail/compose/ComposeActivity;->formatSenders(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v3, v5}, Lcom/android/mail/providers/Message;->setTo(Ljava/lang/String;)V

    iget-object v5, p0, Lcom/android/mail/compose/ComposeActivity;->mCc:Lcom/android/ex/chips/RecipientEditTextView;

    invoke-virtual {v5}, Lcom/android/ex/chips/RecipientEditTextView;->getText()Landroid/text/Editable;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-direct {p0, v5}, Lcom/android/mail/compose/ComposeActivity;->formatSenders(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v3, v5}, Lcom/android/mail/providers/Message;->setCc(Ljava/lang/String;)V

    iget-object v5, p0, Lcom/android/mail/compose/ComposeActivity;->mBcc:Lcom/android/ex/chips/RecipientEditTextView;

    invoke-virtual {v5}, Lcom/android/ex/chips/RecipientEditTextView;->getText()Landroid/text/Editable;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-direct {p0, v5}, Lcom/android/mail/compose/ComposeActivity;->formatSenders(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v3, v5}, Lcom/android/mail/providers/Message;->setBcc(Ljava/lang/String;)V

    return-object v3

    :cond_1
    move-object v5, v6

    goto/16 :goto_0

    :cond_2
    move v5, v8

    goto :goto_1

    :cond_3
    move v7, v8

    goto :goto_2

    :cond_4
    const/4 v5, -0x1

    goto :goto_3

    :cond_5
    iget-object v5, p0, Lcom/android/mail/compose/ComposeActivity;->mAccount:Lcom/android/mail/providers/Account;

    if-eqz v5, :cond_0

    iget-object v5, p0, Lcom/android/mail/compose/ComposeActivity;->mAccount:Lcom/android/mail/providers/Account;

    iget-object v6, v5, Lcom/android/mail/providers/Account;->name:Ljava/lang/String;

    goto :goto_4
.end method

.method public static createReplyIntent(Landroid/content/Context;Lcom/android/mail/providers/Account;Landroid/net/Uri;Z)Landroid/content/Intent;
    .locals 1
    .param p0    # Landroid/content/Context;
    .param p1    # Lcom/android/mail/providers/Account;
    .param p2    # Landroid/net/Uri;
    .param p3    # Z

    if-eqz p3, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-static {p0, p1, p2, v0}, Lcom/android/mail/compose/ComposeActivity;->createActionIntent(Landroid/content/Context;Lcom/android/mail/providers/Account;Landroid/net/Uri;I)Landroid/content/Intent;

    move-result-object v0

    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private discardChanges()V
    .locals 1

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/android/mail/compose/ComposeActivity;->mTextChanged:Z

    iput-boolean v0, p0, Lcom/android/mail/compose/ComposeActivity;->mAttachmentsChanged:Z

    iput-boolean v0, p0, Lcom/android/mail/compose/ComposeActivity;->mReplyFromChanged:Z

    return-void
.end method

.method private doAttach(Ljava/lang/String;)V
    .locals 3

    const/4 v2, 0x1

    new-instance v0, Landroid/content/Intent;

    const-string v1, "android.intent.action.GET_CONTENT"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    const-string v1, "android.intent.category.OPENABLE"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->addCategory(Ljava/lang/String;)Landroid/content/Intent;

    const/high16 v1, 0x80000

    invoke-virtual {v0, v1}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    invoke-virtual {v0, p1}, Landroid/content/Intent;->setType(Ljava/lang/String;)Landroid/content/Intent;

    iput-boolean v2, p0, Lcom/android/mail/compose/ComposeActivity;->mAddingAttachment:Z

    const v1, 0x7f09003c

    invoke-virtual {p0, v1}, Lcom/android/mail/compose/ComposeActivity;->getText(I)Ljava/lang/CharSequence;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/content/Intent;->createChooser(Landroid/content/Intent;Ljava/lang/CharSequence;)Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {p0, v0, v2}, Lcom/android/mail/compose/ComposeActivity;->startActivityForResult(Landroid/content/Intent;I)V

    return-void
.end method

.method private doDiscard()V
    .locals 3

    new-instance v0, Landroid/app/AlertDialog$Builder;

    invoke-direct {v0, p0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    const v1, 0x7f0900ab

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setMessage(I)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    const v1, 0x7f0900e0

    invoke-virtual {v0, v1, p0}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    const v1, 0x7f0900e1

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/AlertDialog;->show()V

    return-void
.end method

.method private doDiscardWithoutConfirmation(Z)V
    .locals 6

    const-wide/16 v4, -0x1

    iget-object v1, p0, Lcom/android/mail/compose/ComposeActivity;->mDraftLock:Ljava/lang/Object;

    monitor-enter v1

    :try_start_0
    iget-wide v2, p0, Lcom/android/mail/compose/ComposeActivity;->mDraftId:J

    cmp-long v0, v2, v4

    if-eqz v0, :cond_0

    new-instance v0, Landroid/content/ContentValues;

    invoke-direct {v0}, Landroid/content/ContentValues;-><init>()V

    const-string v2, "_id"

    iget-wide v3, p0, Lcom/android/mail/compose/ComposeActivity;->mDraftId:J

    invoke-static {v3, v4}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    iget-object v2, p0, Lcom/android/mail/compose/ComposeActivity;->mAccount:Lcom/android/mail/providers/Account;

    iget-object v2, v2, Lcom/android/mail/providers/Account;->expungeMessageUri:Landroid/net/Uri;

    sget-object v3, Landroid/net/Uri;->EMPTY:Landroid/net/Uri;

    invoke-virtual {v2, v3}, Landroid/net/Uri;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_2

    invoke-virtual {p0}, Lcom/android/mail/compose/ComposeActivity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    iget-object v3, p0, Lcom/android/mail/compose/ComposeActivity;->mAccount:Lcom/android/mail/providers/Account;

    iget-object v3, v3, Lcom/android/mail/providers/Account;->expungeMessageUri:Landroid/net/Uri;

    const/4 v4, 0x0

    const/4 v5, 0x0

    invoke-virtual {v2, v3, v0, v4, v5}, Landroid/content/ContentResolver;->update(Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    :goto_0
    const-wide/16 v2, -0x1

    iput-wide v2, p0, Lcom/android/mail/compose/ComposeActivity;->mDraftId:J

    :cond_0
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-eqz p1, :cond_1

    const v0, 0x7f090047

    const/4 v1, 0x0

    invoke-static {p0, v0, v1}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    :cond_1
    invoke-direct {p0}, Lcom/android/mail/compose/ComposeActivity;->discardChanges()V

    invoke-virtual {p0}, Lcom/android/mail/compose/ComposeActivity;->finish()V

    return-void

    :cond_2
    :try_start_1
    invoke-virtual {p0}, Lcom/android/mail/compose/ComposeActivity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    iget-object v2, p0, Lcom/android/mail/compose/ComposeActivity;->mDraft:Lcom/android/mail/providers/Message;

    iget-object v2, v2, Lcom/android/mail/providers/Message;->uri:Landroid/net/Uri;

    const/4 v3, 0x0

    const/4 v4, 0x0

    invoke-virtual {v0, v2, v3, v4}, Landroid/content/ContentResolver;->delete(Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)I

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method

.method private doSave(Z)V
    .locals 2
    .param p1    # Z

    const/4 v1, 0x0

    const/4 v0, 0x1

    invoke-virtual {p0, v0, p1, v1, v1}, Lcom/android/mail/compose/ComposeActivity;->sendOrSaveWithSanityChecks(ZZZZ)Z

    return-void
.end method

.method private doSend()V
    .locals 2

    const/4 v1, 0x0

    const/4 v0, 0x1

    invoke-virtual {p0, v1, v0, v1, v1}, Lcom/android/mail/compose/ComposeActivity;->sendOrSaveWithSanityChecks(ZZZZ)Z

    return-void
.end method

.method public static editDraft(Landroid/content/Context;Lcom/android/mail/providers/Account;Lcom/android/mail/providers/Message;)V
    .locals 1
    .param p0    # Landroid/content/Context;
    .param p1    # Lcom/android/mail/providers/Account;
    .param p2    # Lcom/android/mail/providers/Message;

    const/4 v0, 0x3

    invoke-static {p0, p1, p2, v0}, Lcom/android/mail/compose/ComposeActivity;->launch(Landroid/content/Context;Lcom/android/mail/providers/Account;Lcom/android/mail/providers/Message;I)V

    return-void
.end method

.method private findViews()V
    .locals 2

    const v0, 0x7f080036

    invoke-virtual {p0, v0}, Lcom/android/mail/compose/ComposeActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    const v0, 0x7f08003f

    invoke-virtual {p0, v0}, Lcom/android/mail/compose/ComposeActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lcom/android/mail/compose/ComposeActivity;->mCcBccButton:Landroid/widget/Button;

    iget-object v0, p0, Lcom/android/mail/compose/ComposeActivity;->mCcBccButton:Landroid/widget/Button;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/mail/compose/ComposeActivity;->mCcBccButton:Landroid/widget/Button;

    invoke-virtual {v0, p0}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    :cond_0
    const v0, 0x7f080040

    invoke-virtual {p0, v0}, Lcom/android/mail/compose/ComposeActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/android/mail/compose/CcBccView;

    iput-object v0, p0, Lcom/android/mail/compose/ComposeActivity;->mCcBccView:Lcom/android/mail/compose/CcBccView;

    const v0, 0x7f080038

    invoke-virtual {p0, v0}, Lcom/android/mail/compose/ComposeActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/android/mail/compose/AttachmentsView;

    iput-object v0, p0, Lcom/android/mail/compose/ComposeActivity;->mAttachmentsView:Lcom/android/mail/compose/AttachmentsView;

    const v0, 0x7f080043

    invoke-virtual {p0, v0}, Lcom/android/mail/compose/ComposeActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/android/mail/compose/ComposeActivity;->mPhotoAttachmentsButton:Landroid/view/View;

    iget-object v0, p0, Lcom/android/mail/compose/ComposeActivity;->mPhotoAttachmentsButton:Landroid/view/View;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/android/mail/compose/ComposeActivity;->mPhotoAttachmentsButton:Landroid/view/View;

    invoke-virtual {v0, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    :cond_1
    const v0, 0x7f080112

    invoke-virtual {p0, v0}, Lcom/android/mail/compose/ComposeActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/android/mail/compose/ComposeActivity;->mVideoAttachmentsButton:Landroid/view/View;

    iget-object v0, p0, Lcom/android/mail/compose/ComposeActivity;->mVideoAttachmentsButton:Landroid/view/View;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/android/mail/compose/ComposeActivity;->mVideoAttachmentsButton:Landroid/view/View;

    invoke-virtual {v0, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    :cond_2
    const v0, 0x7f08003e

    invoke-virtual {p0, v0}, Lcom/android/mail/compose/ComposeActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/android/ex/chips/RecipientEditTextView;

    iput-object v0, p0, Lcom/android/mail/compose/ComposeActivity;->mTo:Lcom/android/ex/chips/RecipientEditTextView;

    const v0, 0x7f08002d

    invoke-virtual {p0, v0}, Lcom/android/mail/compose/ComposeActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/android/ex/chips/RecipientEditTextView;

    iput-object v0, p0, Lcom/android/mail/compose/ComposeActivity;->mCc:Lcom/android/ex/chips/RecipientEditTextView;

    const v0, 0x7f080030

    invoke-virtual {p0, v0}, Lcom/android/mail/compose/ComposeActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/android/ex/chips/RecipientEditTextView;

    iput-object v0, p0, Lcom/android/mail/compose/ComposeActivity;->mBcc:Lcom/android/ex/chips/RecipientEditTextView;

    const v0, 0x7f080042

    invoke-virtual {p0, v0}, Lcom/android/mail/compose/ComposeActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/android/mail/compose/ComposeActivity;->mSubject:Landroid/widget/TextView;

    iget-object v0, p0, Lcom/android/mail/compose/ComposeActivity;->mSubject:Landroid/widget/TextView;

    invoke-virtual {v0, p0}, Landroid/widget/TextView;->setOnEditorActionListener(Landroid/widget/TextView$OnEditorActionListener;)V

    const v0, 0x7f080039

    invoke-virtual {p0, v0}, Lcom/android/mail/compose/ComposeActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/android/mail/compose/QuotedTextView;

    iput-object v0, p0, Lcom/android/mail/compose/ComposeActivity;->mQuotedTextView:Lcom/android/mail/compose/QuotedTextView;

    iget-object v0, p0, Lcom/android/mail/compose/ComposeActivity;->mQuotedTextView:Lcom/android/mail/compose/QuotedTextView;

    invoke-virtual {v0, p0}, Lcom/android/mail/compose/QuotedTextView;->setRespondInlineListener(Lcom/android/mail/compose/QuotedTextView$RespondInlineListener;)V

    const v0, 0x7f08004c

    invoke-virtual {p0, v0}, Lcom/android/mail/compose/ComposeActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/EditText;

    iput-object v0, p0, Lcom/android/mail/compose/ComposeActivity;->mBodyView:Landroid/widget/EditText;

    const v0, 0x7f08004f

    invoke-virtual {p0, v0}, Lcom/android/mail/compose/ComposeActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/android/mail/compose/ComposeActivity;->mFromStatic:Landroid/view/View;

    const v0, 0x7f080050

    invoke-virtual {p0, v0}, Lcom/android/mail/compose/ComposeActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/android/mail/compose/ComposeActivity;->mFromStaticText:Landroid/widget/TextView;

    const v0, 0x7f08004d

    invoke-virtual {p0, v0}, Lcom/android/mail/compose/ComposeActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/android/mail/compose/ComposeActivity;->mFromSpinnerWrapper:Landroid/view/View;

    const v0, 0x7f08004e

    invoke-virtual {p0, v0}, Lcom/android/mail/compose/ComposeActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/android/mail/compose/FromAddressSpinner;

    iput-object v0, p0, Lcom/android/mail/compose/ComposeActivity;->mFromSpinner:Lcom/android/mail/compose/FromAddressSpinner;

    return-void
.end method

.method private finishCreate()V
    .locals 18

    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/android/mail/compose/ComposeActivity;->mSavedInstanceState:Landroid/os/Bundle;

    invoke-direct/range {p0 .. p0}, Lcom/android/mail/compose/ComposeActivity;->findViews()V

    invoke-virtual/range {p0 .. p0}, Lcom/android/mail/compose/ComposeActivity;->getIntent()Landroid/content/Intent;

    move-result-object v7

    const/4 v14, 0x0

    const/4 v1, 0x0

    move-object/from16 v0, p0

    invoke-direct {v0, v11}, Lcom/android/mail/compose/ComposeActivity;->hadSavedInstanceStateMessage(Landroid/os/Bundle;)Z

    move-result v15

    if-eqz v15, :cond_0

    const-string v15, "action"

    const/16 v16, -0x1

    move/from16 v0, v16

    invoke-virtual {v11, v15, v0}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v3

    const-string v15, "account"

    invoke-virtual {v11, v15}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v1

    check-cast v1, Lcom/android/mail/providers/Account;

    const-string v15, "extraMessage"

    invoke-virtual {v11, v15}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v8

    check-cast v8, Lcom/android/mail/providers/Message;

    const-string v15, "attachmentPreviews"

    invoke-virtual {v11, v15}, Landroid/os/Bundle;->getParcelableArrayList(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v10

    const-string v15, "in-reference-to-message"

    invoke-virtual {v11, v15}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v15

    check-cast v15, Lcom/android/mail/providers/Message;

    move-object/from16 v0, p0

    iput-object v15, v0, Lcom/android/mail/compose/ComposeActivity;->mRefMessage:Lcom/android/mail/providers/Message;

    :goto_0
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/android/mail/compose/ComposeActivity;->mAttachmentsView:Lcom/android/mail/compose/AttachmentsView;

    invoke-virtual {v15, v10}, Lcom/android/mail/compose/AttachmentsView;->setAttachmentPreviews(Ljava/util/ArrayList;)V

    move-object/from16 v0, p0

    invoke-virtual {v0, v1}, Lcom/android/mail/compose/ComposeActivity;->setAccount(Lcom/android/mail/providers/Account;)V

    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/android/mail/compose/ComposeActivity;->mAccount:Lcom/android/mail/providers/Account;

    if-nez v15, :cond_1

    :goto_1
    return-void

    :cond_0
    move-object/from16 v0, p0

    invoke-direct {v0, v7}, Lcom/android/mail/compose/ComposeActivity;->obtainAccount(Landroid/content/Intent;)Lcom/android/mail/providers/Account;

    move-result-object v1

    const-string v15, "action"

    const/16 v16, -0x1

    move/from16 v0, v16

    invoke-virtual {v7, v15, v0}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v3

    const-string v15, "original-draft-message"

    invoke-virtual {v7, v15}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v8

    check-cast v8, Lcom/android/mail/providers/Message;

    const-string v15, "attachmentPreviews"

    invoke-virtual {v7, v15}, Landroid/content/Intent;->getParcelableArrayListExtra(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v10

    const-string v15, "in-reference-to-message"

    invoke-virtual {v7, v15}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v15

    check-cast v15, Lcom/android/mail/providers/Message;

    move-object/from16 v0, p0

    iput-object v15, v0, Lcom/android/mail/compose/ComposeActivity;->mRefMessage:Lcom/android/mail/providers/Message;

    const-string v15, "in-reference-to-message-uri"

    invoke-virtual {v7, v15}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v15

    check-cast v15, Landroid/net/Uri;

    move-object/from16 v0, p0

    iput-object v15, v0, Lcom/android/mail/compose/ComposeActivity;->mRefMessageUri:Landroid/net/Uri;

    goto :goto_0

    :cond_1
    const-string v15, "extra-notification-folder"

    invoke-virtual {v7, v15}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v9

    check-cast v9, Lcom/android/mail/providers/Folder;

    if-eqz v9, :cond_2

    new-instance v4, Landroid/content/Intent;

    const-string v15, "com.android.mail.action.CLEAR_NEW_MAIL_NOTIFICATIONS"

    invoke-direct {v4, v15}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    const-string v15, "account"

    iget-object v0, v1, Lcom/android/mail/providers/Account;->name:Ljava/lang/String;

    move-object/from16 v16, v0

    move-object/from16 v0, v16

    invoke-virtual {v4, v15, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const-string v15, "folder"

    invoke-virtual {v4, v15, v9}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    move-object/from16 v0, p0

    invoke-virtual {v0, v4}, Lcom/android/mail/compose/ComposeActivity;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;

    :cond_2
    const-string v15, "fromemail"

    const/16 v16, 0x0

    move/from16 v0, v16

    invoke-virtual {v7, v15, v0}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v15

    if-eqz v15, :cond_4

    const/4 v15, 0x1

    move-object/from16 v0, p0

    iput-boolean v15, v0, Lcom/android/mail/compose/ComposeActivity;->mLaunchedFromEmail:Z

    :cond_3
    :goto_2
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/android/mail/compose/ComposeActivity;->mRefMessageUri:Landroid/net/Uri;

    if-eqz v15, :cond_5

    invoke-virtual/range {p0 .. p0}, Lcom/android/mail/compose/ComposeActivity;->getLoaderManager()Landroid/app/LoaderManager;

    move-result-object v15

    const/16 v16, 0x0

    const/16 v17, 0x0

    invoke-virtual/range {v15 .. v18}, Landroid/app/LoaderManager;->initLoader(ILandroid/os/Bundle;Landroid/app/LoaderManager$LoaderCallbacks;)Landroid/content/Loader;

    goto :goto_1

    :cond_4
    const-string v15, "android.intent.action.SEND"

    invoke-virtual {v7}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v16

    invoke-virtual/range {v15 .. v16}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v15

    if-eqz v15, :cond_3

    invoke-virtual {v7}, Landroid/content/Intent;->getData()Landroid/net/Uri;

    move-result-object v6

    if-eqz v6, :cond_3

    invoke-virtual {v7}, Landroid/content/Intent;->getData()Landroid/net/Uri;

    move-result-object v15

    invoke-virtual {v15}, Landroid/net/Uri;->getScheme()Ljava/lang/String;

    move-result-object v5

    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/android/mail/compose/ComposeActivity;->mAccount:Lcom/android/mail/providers/Account;

    iget-object v15, v15, Lcom/android/mail/providers/Account;->composeIntentUri:Landroid/net/Uri;

    invoke-virtual {v15}, Landroid/net/Uri;->getScheme()Ljava/lang/String;

    move-result-object v2

    invoke-static {v5, v2}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v15

    move-object/from16 v0, p0

    iput-boolean v15, v0, Lcom/android/mail/compose/ComposeActivity;->mLaunchedFromEmail:Z

    goto :goto_2

    :cond_5
    if-eqz v8, :cond_7

    const/4 v15, 0x3

    if-eq v3, v15, :cond_7

    move-object/from16 v0, p0

    invoke-direct {v0, v8}, Lcom/android/mail/compose/ComposeActivity;->initFromDraftMessage(Lcom/android/mail/providers/Message;)V

    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/android/mail/compose/ComposeActivity;->mRefMessage:Lcom/android/mail/providers/Message;

    move-object/from16 v0, p0

    invoke-direct {v0, v15, v3}, Lcom/android/mail/compose/ComposeActivity;->initQuotedTextFromRefMessage(Lcom/android/mail/providers/Message;I)V

    move-object/from16 v0, p0

    invoke-direct {v0, v11}, Lcom/android/mail/compose/ComposeActivity;->showCcBcc(Landroid/os/Bundle;)V

    iget-boolean v14, v8, Lcom/android/mail/providers/Message;->appendRefMessageContent:Z

    :cond_6
    :goto_3
    move-object/from16 v0, p0

    invoke-direct {v0, v3, v7, v11, v14}, Lcom/android/mail/compose/ComposeActivity;->finishSetup(ILandroid/content/Intent;Landroid/os/Bundle;Z)V

    goto/16 :goto_1

    :cond_7
    const/4 v15, 0x3

    if-ne v3, v15, :cond_b

    move-object/from16 v0, p0

    invoke-direct {v0, v8}, Lcom/android/mail/compose/ComposeActivity;->initFromDraftMessage(Lcom/android/mail/providers/Message;)V

    invoke-virtual {v8}, Lcom/android/mail/providers/Message;->getBcc()Ljava/lang/String;

    move-result-object v15

    invoke-static {v15}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v15

    if-nez v15, :cond_9

    const/4 v12, 0x1

    :goto_4
    if-nez v12, :cond_8

    invoke-virtual {v8}, Lcom/android/mail/providers/Message;->getCc()Ljava/lang/String;

    move-result-object v15

    invoke-static {v15}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v15

    if-nez v15, :cond_a

    :cond_8
    const/4 v13, 0x1

    :goto_5
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/android/mail/compose/ComposeActivity;->mCcBccView:Lcom/android/mail/compose/CcBccView;

    const/16 v16, 0x0

    move/from16 v0, v16

    invoke-virtual {v15, v0, v13, v12}, Lcom/android/mail/compose/CcBccView;->show(ZZZ)V

    iget v15, v8, Lcom/android/mail/providers/Message;->draftType:I

    packed-switch v15, :pswitch_data_0

    const/4 v3, -0x1

    :goto_6
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/android/mail/compose/ComposeActivity;->mRefMessage:Lcom/android/mail/providers/Message;

    move-object/from16 v0, p0

    invoke-direct {v0, v15, v3}, Lcom/android/mail/compose/ComposeActivity;->initQuotedTextFromRefMessage(Lcom/android/mail/providers/Message;I)V

    iget-boolean v14, v8, Lcom/android/mail/providers/Message;->appendRefMessageContent:Z

    goto :goto_3

    :cond_9
    const/4 v12, 0x0

    goto :goto_4

    :cond_a
    const/4 v13, 0x0

    goto :goto_5

    :pswitch_0
    const/4 v3, 0x0

    goto :goto_6

    :pswitch_1
    const/4 v3, 0x1

    goto :goto_6

    :pswitch_2
    const/4 v3, 0x2

    goto :goto_6

    :cond_b
    if-eqz v3, :cond_c

    const/4 v15, 0x1

    if-eq v3, v15, :cond_c

    const/4 v15, 0x2

    if-ne v3, v15, :cond_d

    :cond_c
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/android/mail/compose/ComposeActivity;->mRefMessage:Lcom/android/mail/providers/Message;

    if-eqz v15, :cond_6

    move-object/from16 v0, p0

    invoke-direct {v0, v3}, Lcom/android/mail/compose/ComposeActivity;->initFromRefMessage(I)V

    const/4 v14, 0x1

    goto :goto_3

    :cond_d
    move-object/from16 v0, p0

    invoke-virtual {v0, v7}, Lcom/android/mail/compose/ComposeActivity;->initFromExtras(Landroid/content/Intent;)V

    goto :goto_3

    :pswitch_data_0
    .packed-switch 0x2
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method private finishSetup(ILandroid/content/Intent;Landroid/os/Bundle;Z)V
    .locals 3
    .param p1    # I
    .param p2    # Landroid/content/Intent;
    .param p3    # Landroid/os/Bundle;
    .param p4    # Z

    const/16 v2, 0x8

    invoke-direct {p0, p1}, Lcom/android/mail/compose/ComposeActivity;->setFocus(I)V

    const/4 v0, -0x1

    if-ne p1, v0, :cond_0

    iget-object v0, p0, Lcom/android/mail/compose/ComposeActivity;->mQuotedTextView:Lcom/android/mail/compose/QuotedTextView;

    invoke-virtual {v0, v2}, Lcom/android/mail/compose/QuotedTextView;->setVisibility(I)V

    :cond_0
    invoke-direct {p0}, Lcom/android/mail/compose/ComposeActivity;->initRecipients()V

    invoke-direct {p0, p3}, Lcom/android/mail/compose/ComposeActivity;->hadSavedInstanceStateMessage(Landroid/os/Bundle;)Z

    move-result v0

    if-nez v0, :cond_1

    invoke-direct {p0, p2}, Lcom/android/mail/compose/ComposeActivity;->initAttachmentsFromIntent(Landroid/content/Intent;)V

    :cond_1
    invoke-direct {p0, p1}, Lcom/android/mail/compose/ComposeActivity;->initActionBar(I)V

    if-eqz p3, :cond_4

    :goto_0
    invoke-direct {p0, p3, p1}, Lcom/android/mail/compose/ComposeActivity;->initFromSpinner(Landroid/os/Bundle;I)V

    iget-object v0, p0, Lcom/android/mail/compose/ComposeActivity;->mDraft:Lcom/android/mail/providers/Message;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/android/mail/compose/ComposeActivity;->mReplyFromAccount:Lcom/android/mail/providers/ReplyFromAccount;

    iput-object v0, p0, Lcom/android/mail/compose/ComposeActivity;->mDraftAccount:Lcom/android/mail/providers/ReplyFromAccount;

    :cond_2
    invoke-direct {p0}, Lcom/android/mail/compose/ComposeActivity;->initChangeListeners()V

    invoke-direct {p0}, Lcom/android/mail/compose/ComposeActivity;->updateHideOrShowCcBcc()V

    invoke-direct {p0, p4}, Lcom/android/mail/compose/ComposeActivity;->updateHideOrShowQuotedText(Z)V

    iget-object v0, p0, Lcom/android/mail/compose/ComposeActivity;->mSavedInstanceState:Landroid/os/Bundle;

    if-eqz v0, :cond_5

    iget-object v0, p0, Lcom/android/mail/compose/ComposeActivity;->mSavedInstanceState:Landroid/os/Bundle;

    const-string v1, "respondedInline"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v0

    :goto_1
    iput-boolean v0, p0, Lcom/android/mail/compose/ComposeActivity;->mRespondedInline:Z

    iget-boolean v0, p0, Lcom/android/mail/compose/ComposeActivity;->mRespondedInline:Z

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/android/mail/compose/ComposeActivity;->mQuotedTextView:Lcom/android/mail/compose/QuotedTextView;

    invoke-virtual {v0, v2}, Lcom/android/mail/compose/QuotedTextView;->setVisibility(I)V

    :cond_3
    return-void

    :cond_4
    invoke-virtual {p2}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object p3

    goto :goto_0

    :cond_5
    const/4 v0, 0x0

    goto :goto_1
.end method

.method private formatSenders(Ljava/lang/String;)Ljava/lang/String;
    .locals 2
    .param p1    # Ljava/lang/String;

    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    invoke-virtual {p1, v0}, Ljava/lang/String;->charAt(I)C

    move-result v0

    const/16 v1, 0x2c

    if-ne v0, v1, :cond_0

    const/4 v0, 0x0

    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    invoke-virtual {p1, v0, v1}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object p1

    :cond_0
    return-object p1
.end method

.method public static forward(Landroid/content/Context;Lcom/android/mail/providers/Account;Lcom/android/mail/providers/Message;)V
    .locals 1
    .param p0    # Landroid/content/Context;
    .param p1    # Lcom/android/mail/providers/Account;
    .param p2    # Lcom/android/mail/providers/Message;

    const/4 v0, 0x2

    invoke-static {p0, p1, p2, v0}, Lcom/android/mail/compose/ComposeActivity;->launch(Landroid/content/Context;Lcom/android/mail/providers/Account;Lcom/android/mail/providers/Message;I)V

    return-void
.end method

.method private getDefaultReplyFromAccount(Lcom/android/mail/providers/Account;)Lcom/android/mail/providers/ReplyFromAccount;
    .locals 10
    .param p1    # Lcom/android/mail/providers/Account;

    invoke-virtual {p1}, Lcom/android/mail/providers/Account;->getReplyFroms()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v9

    :cond_0
    invoke-interface {v9}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v9}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Lcom/android/mail/providers/ReplyFromAccount;

    iget-boolean v0, v8, Lcom/android/mail/providers/ReplyFromAccount;->isDefault:Z

    if-eqz v0, :cond_0

    :goto_0
    return-object v8

    :cond_1
    new-instance v0, Lcom/android/mail/providers/ReplyFromAccount;

    iget-object v2, p1, Lcom/android/mail/providers/Account;->uri:Landroid/net/Uri;

    iget-object v3, p1, Lcom/android/mail/providers/Account;->name:Ljava/lang/String;

    iget-object v4, p1, Lcom/android/mail/providers/Account;->name:Ljava/lang/String;

    iget-object v5, p1, Lcom/android/mail/providers/Account;->name:Ljava/lang/String;

    const/4 v6, 0x1

    const/4 v7, 0x0

    move-object v1, p1

    invoke-direct/range {v0 .. v7}, Lcom/android/mail/providers/ReplyFromAccount;-><init>(Lcom/android/mail/providers/Account;Landroid/net/Uri;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ZZ)V

    move-object v8, v0

    goto :goto_0
.end method

.method private static getDraftType(I)I
    .locals 1
    .param p0    # I

    const/4 v0, -0x1

    packed-switch p0, :pswitch_data_0

    :goto_0
    return v0

    :pswitch_0
    const/4 v0, 0x1

    goto :goto_0

    :pswitch_1
    const/4 v0, 0x2

    goto :goto_0

    :pswitch_2
    const/4 v0, 0x3

    goto :goto_0

    :pswitch_3
    const/4 v0, 0x4

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch -0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method private getMode()I
    .locals 4

    const/4 v1, -0x1

    invoke-virtual {p0}, Lcom/android/mail/compose/ComposeActivity;->getActionBar()Landroid/app/ActionBar;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Landroid/app/ActionBar;->getNavigationMode()I

    move-result v2

    const/4 v3, 0x1

    if-ne v2, v3, :cond_0

    invoke-virtual {v0}, Landroid/app/ActionBar;->getSelectedNavigationIndex()I

    move-result v1

    :cond_0
    return v1
.end method

.method private getReplyFromAccountForReply(Lcom/android/mail/providers/Account;Lcom/android/mail/providers/Message;)Lcom/android/mail/providers/ReplyFromAccount;
    .locals 5
    .param p1    # Lcom/android/mail/providers/Account;
    .param p2    # Lcom/android/mail/providers/Message;

    iget-object v3, p2, Lcom/android/mail/providers/Message;->accountUri:Landroid/net/Uri;

    if-eqz v3, :cond_2

    iget-object v3, p0, Lcom/android/mail/compose/ComposeActivity;->mFromSpinner:Lcom/android/mail/compose/FromAddressSpinner;

    invoke-virtual {v3}, Lcom/android/mail/compose/FromAddressSpinner;->getReplyFromAccounts()Ljava/util/List;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/mail/providers/ReplyFromAccount;

    iget-object v3, v0, Lcom/android/mail/providers/ReplyFromAccount;->account:Lcom/android/mail/providers/Account;

    iget-object v3, v3, Lcom/android/mail/providers/Account;->uri:Landroid/net/Uri;

    iget-object v4, p2, Lcom/android/mail/providers/Message;->accountUri:Landroid/net/Uri;

    invoke-virtual {v3, v4}, Landroid/net/Uri;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    :goto_0
    return-object v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0

    :cond_2
    invoke-virtual {p0, p1, p2}, Lcom/android/mail/compose/ComposeActivity;->getReplyFromAccount(Lcom/android/mail/providers/Account;Lcom/android/mail/providers/Message;)Lcom/android/mail/providers/ReplyFromAccount;

    move-result-object v0

    goto :goto_0
.end method

.method private getReplyFromAccountFromDraft(Lcom/android/mail/providers/Account;Lcom/android/mail/providers/Message;)Lcom/android/mail/providers/ReplyFromAccount;
    .locals 12
    .param p1    # Lcom/android/mail/providers/Account;
    .param p2    # Lcom/android/mail/providers/Message;

    invoke-virtual {p2}, Lcom/android/mail/providers/Message;->getFrom()Ljava/lang/String;

    move-result-object v11

    const/4 v0, 0x0

    iget-object v1, p0, Lcom/android/mail/compose/ComposeActivity;->mFromSpinner:Lcom/android/mail/compose/FromAddressSpinner;

    invoke-virtual {v1}, Lcom/android/mail/compose/FromAddressSpinner;->getReplyFromAccounts()Ljava/util/List;

    move-result-object v10

    iget-object v1, p1, Lcom/android/mail/providers/Account;->name:Ljava/lang/String;

    invoke-static {v1, v11}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_1

    new-instance v0, Lcom/android/mail/providers/ReplyFromAccount;

    iget-object v1, p0, Lcom/android/mail/compose/ComposeActivity;->mAccount:Lcom/android/mail/providers/Account;

    iget-object v2, p0, Lcom/android/mail/compose/ComposeActivity;->mAccount:Lcom/android/mail/providers/Account;

    iget-object v2, v2, Lcom/android/mail/providers/Account;->uri:Landroid/net/Uri;

    iget-object v3, p0, Lcom/android/mail/compose/ComposeActivity;->mAccount:Lcom/android/mail/providers/Account;

    iget-object v3, v3, Lcom/android/mail/providers/Account;->name:Ljava/lang/String;

    iget-object v4, p0, Lcom/android/mail/compose/ComposeActivity;->mAccount:Lcom/android/mail/providers/Account;

    iget-object v4, v4, Lcom/android/mail/providers/Account;->name:Ljava/lang/String;

    iget-object v5, p0, Lcom/android/mail/compose/ComposeActivity;->mAccount:Lcom/android/mail/providers/Account;

    iget-object v5, v5, Lcom/android/mail/providers/Account;->name:Ljava/lang/String;

    const/4 v6, 0x1

    const/4 v7, 0x0

    invoke-direct/range {v0 .. v7}, Lcom/android/mail/providers/ReplyFromAccount;-><init>(Lcom/android/mail/providers/Account;Landroid/net/Uri;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ZZ)V

    :cond_0
    :goto_0
    return-object v0

    :cond_1
    invoke-interface {v10}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v9

    :cond_2
    invoke-interface {v9}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {v9}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Lcom/android/mail/providers/ReplyFromAccount;

    iget-object v1, v8, Lcom/android/mail/providers/ReplyFromAccount;->name:Ljava/lang/String;

    invoke-static {v1, v11}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_2

    move-object v0, v8

    goto :goto_0
.end method

.method private getWaitFragment()Lcom/android/mail/ui/WaitFragment;
    .locals 2

    invoke-virtual {p0}, Lcom/android/mail/compose/ComposeActivity;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v0

    const-string v1, "wait-fragment"

    invoke-virtual {v0, v1}, Landroid/app/FragmentManager;->findFragmentByTag(Ljava/lang/String;)Landroid/app/Fragment;

    move-result-object v0

    check-cast v0, Lcom/android/mail/ui/WaitFragment;

    return-object v0
.end method

.method private hadSavedInstanceStateMessage(Landroid/os/Bundle;)Z
    .locals 1
    .param p1    # Landroid/os/Bundle;

    if-eqz p1, :cond_0

    const-string v0, "extraMessage"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private initActionBar(I)V
    .locals 5

    const/4 v4, 0x6

    const/4 v3, 0x0

    const/4 v2, 0x1

    iput p1, p0, Lcom/android/mail/compose/ComposeActivity;->mComposeMode:I

    invoke-virtual {p0}, Lcom/android/mail/compose/ComposeActivity;->getActionBar()Landroid/app/ActionBar;

    move-result-object v0

    if-nez v0, :cond_0

    :goto_0
    return-void

    :cond_0
    const/4 v1, -0x1

    if-ne p1, v1, :cond_1

    invoke-virtual {v0, v3}, Landroid/app/ActionBar;->setNavigationMode(I)V

    const v1, 0x7f090035

    invoke-virtual {v0, v1}, Landroid/app/ActionBar;->setTitle(I)V

    :goto_1
    invoke-virtual {v0, v4, v4}, Landroid/app/ActionBar;->setDisplayOptions(II)V

    invoke-virtual {v0, v2}, Landroid/app/ActionBar;->setHomeButtonEnabled(Z)V

    goto :goto_0

    :cond_1
    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/app/ActionBar;->setTitle(Ljava/lang/CharSequence;)V

    iget-object v1, p0, Lcom/android/mail/compose/ComposeActivity;->mComposeModeAdapter:Lcom/android/mail/compose/ComposeActivity$ComposeModeAdapter;

    if-nez v1, :cond_2

    new-instance v1, Lcom/android/mail/compose/ComposeActivity$ComposeModeAdapter;

    invoke-direct {v1, p0, p0}, Lcom/android/mail/compose/ComposeActivity$ComposeModeAdapter;-><init>(Lcom/android/mail/compose/ComposeActivity;Landroid/content/Context;)V

    iput-object v1, p0, Lcom/android/mail/compose/ComposeActivity;->mComposeModeAdapter:Lcom/android/mail/compose/ComposeActivity$ComposeModeAdapter;

    :cond_2
    invoke-virtual {v0, v2}, Landroid/app/ActionBar;->setNavigationMode(I)V

    iget-object v1, p0, Lcom/android/mail/compose/ComposeActivity;->mComposeModeAdapter:Lcom/android/mail/compose/ComposeActivity$ComposeModeAdapter;

    invoke-virtual {v0, v1, p0}, Landroid/app/ActionBar;->setListNavigationCallbacks(Landroid/widget/SpinnerAdapter;Landroid/app/ActionBar$OnNavigationListener;)V

    packed-switch p1, :pswitch_data_0

    goto :goto_1

    :pswitch_0
    invoke-virtual {v0, v3}, Landroid/app/ActionBar;->setSelectedNavigationItem(I)V

    goto :goto_1

    :pswitch_1
    invoke-virtual {v0, v2}, Landroid/app/ActionBar;->setSelectedNavigationItem(I)V

    goto :goto_1

    :pswitch_2
    const/4 v1, 0x2

    invoke-virtual {v0, v1}, Landroid/app/ActionBar;->setSelectedNavigationItem(I)V

    goto :goto_1

    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method private initAttachmentsFromIntent(Landroid/content/Intent;)V
    .locals 13

    invoke-virtual {p1}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v0

    if-nez v0, :cond_6

    sget-object v0, Landroid/os/Bundle;->EMPTY:Landroid/os/Bundle;

    move-object v1, v0

    :goto_0
    invoke-virtual {p1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v8

    iget-boolean v0, p0, Lcom/android/mail/compose/ComposeActivity;->mAttachmentsChanged:Z

    if-nez v0, :cond_4

    const-wide/16 v2, 0x0

    const-string v0, "attachments"

    invoke-virtual {v1, v0}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    const-string v0, "attachments"

    invoke-virtual {v1, v0}, Landroid/os/Bundle;->getSerializable(Ljava/lang/String;)Ljava/io/Serializable;

    move-result-object v0

    check-cast v0, [Ljava/lang/String;

    check-cast v0, [Ljava/lang/String;

    array-length v9, v0

    const/4 v4, 0x0

    move v7, v4

    move-wide v4, v2

    :goto_1
    if-ge v7, v9, :cond_0

    aget-object v2, v0, v7

    invoke-static {v2}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v6

    const-wide/16 v2, 0x0

    :try_start_0
    iget-object v10, p0, Lcom/android/mail/compose/ComposeActivity;->mAttachmentsView:Lcom/android/mail/compose/AttachmentsView;

    iget-object v11, p0, Lcom/android/mail/compose/ComposeActivity;->mAccount:Lcom/android/mail/providers/Account;

    invoke-virtual {v10, v11, v6}, Lcom/android/mail/compose/AttachmentsView;->addAttachment(Lcom/android/mail/providers/Account;Landroid/net/Uri;)J
    :try_end_0
    .catch Lcom/android/mail/compose/AttachmentsView$AttachmentFailureException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-wide v2

    :goto_2
    add-long/2addr v4, v2

    add-int/lit8 v2, v7, 0x1

    move v7, v2

    goto :goto_1

    :catch_0
    move-exception v6

    sget-object v10, Lcom/android/mail/compose/ComposeActivity;->LOG_TAG:Ljava/lang/String;

    const-string v11, "Error adding attachment"

    const/4 v12, 0x0

    new-array v12, v12, [Ljava/lang/Object;

    invoke-static {v10, v6, v11, v12}, Lcom/android/mail/utils/LogUtils;->e(Ljava/lang/String;Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)I

    invoke-virtual {v6}, Lcom/android/mail/compose/AttachmentsView$AttachmentFailureException;->getErrorRes()I

    move-result v6

    invoke-direct {p0, v6}, Lcom/android/mail/compose/ComposeActivity;->showAttachmentTooBigToast(I)V

    goto :goto_2

    :cond_0
    move-wide v2, v4

    :cond_1
    const-string v0, "android.intent.extra.STREAM"

    invoke-virtual {v1, v0}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_3

    const-string v0, "android.intent.action.SEND_MULTIPLE"

    invoke-virtual {v0, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_5

    const-string v0, "android.intent.extra.STREAM"

    invoke-virtual {v1, v0}, Landroid/os/Bundle;->getParcelableArrayList(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v0

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :goto_3
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/os/Parcelable;

    :try_start_1
    iget-object v5, p0, Lcom/android/mail/compose/ComposeActivity;->mAttachmentsView:Lcom/android/mail/compose/AttachmentsView;

    check-cast v0, Landroid/net/Uri;

    invoke-virtual {v5, v0}, Lcom/android/mail/compose/AttachmentsView;->generateLocalAttachment(Landroid/net/Uri;)Lcom/android/mail/providers/Attachment;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
    :try_end_1
    .catch Lcom/android/mail/compose/AttachmentsView$AttachmentFailureException; {:try_start_1 .. :try_end_1} :catch_1

    goto :goto_3

    :catch_1
    move-exception v0

    sget-object v5, Lcom/android/mail/compose/ComposeActivity;->LOG_TAG:Ljava/lang/String;

    const-string v6, "Error adding attachment"

    const/4 v7, 0x0

    new-array v7, v7, [Ljava/lang/Object;

    invoke-static {v5, v0, v6, v7}, Lcom/android/mail/utils/LogUtils;->e(Ljava/lang/String;Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)I

    invoke-virtual {p0}, Lcom/android/mail/compose/ComposeActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    iget-object v5, p0, Lcom/android/mail/compose/ComposeActivity;->mAccount:Lcom/android/mail/providers/Account;

    iget-object v5, v5, Lcom/android/mail/providers/Account;->settings:Lcom/android/mail/providers/Settings;

    invoke-virtual {v5}, Lcom/android/mail/providers/Settings;->getMaxAttachmentSize()I

    move-result v5

    int-to-long v5, v5

    invoke-static {v0, v5, v6}, Lcom/android/mail/utils/AttachmentUtils;->convertToHumanReadableSize(Landroid/content/Context;J)Ljava/lang/String;

    move-result-object v0

    const v5, 0x7f090040

    const/4 v6, 0x1

    new-array v6, v6, [Ljava/lang/Object;

    const/4 v7, 0x0

    aput-object v0, v6, v7

    invoke-virtual {p0, v5, v6}, Lcom/android/mail/compose/ComposeActivity;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/android/mail/compose/ComposeActivity;->showErrorToast(Ljava/lang/String;)V

    goto :goto_3

    :cond_2
    invoke-virtual {p0, v1}, Lcom/android/mail/compose/ComposeActivity;->addAttachments(Ljava/util/List;)J

    move-result-wide v0

    add-long/2addr v2, v0

    :cond_3
    :goto_4
    const-wide/16 v0, 0x0

    cmp-long v0, v2, v0

    if-lez v0, :cond_4

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/android/mail/compose/ComposeActivity;->mAttachmentsChanged:Z

    invoke-virtual {p0}, Lcom/android/mail/compose/ComposeActivity;->updateSaveUi()V

    :cond_4
    return-void

    :cond_5
    const-string v0, "android.intent.extra.STREAM"

    invoke-virtual {v1, v0}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Landroid/net/Uri;

    const-wide/16 v4, 0x0

    :try_start_2
    iget-object v1, p0, Lcom/android/mail/compose/ComposeActivity;->mAttachmentsView:Lcom/android/mail/compose/AttachmentsView;

    iget-object v6, p0, Lcom/android/mail/compose/ComposeActivity;->mAccount:Lcom/android/mail/providers/Account;

    invoke-virtual {v1, v6, v0}, Lcom/android/mail/compose/AttachmentsView;->addAttachment(Lcom/android/mail/providers/Account;Landroid/net/Uri;)J
    :try_end_2
    .catch Lcom/android/mail/compose/AttachmentsView$AttachmentFailureException; {:try_start_2 .. :try_end_2} :catch_2

    move-result-wide v0

    :goto_5
    add-long/2addr v2, v0

    goto :goto_4

    :catch_2
    move-exception v0

    sget-object v1, Lcom/android/mail/compose/ComposeActivity;->LOG_TAG:Ljava/lang/String;

    const-string v6, "Error adding attachment"

    const/4 v7, 0x0

    new-array v7, v7, [Ljava/lang/Object;

    invoke-static {v1, v0, v6, v7}, Lcom/android/mail/utils/LogUtils;->e(Ljava/lang/String;Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)I

    invoke-virtual {v0}, Lcom/android/mail/compose/AttachmentsView$AttachmentFailureException;->getErrorRes()I

    move-result v0

    invoke-direct {p0, v0}, Lcom/android/mail/compose/ComposeActivity;->showAttachmentTooBigToast(I)V

    move-wide v0, v4

    goto :goto_5

    :cond_6
    move-object v1, v0

    goto/16 :goto_0
.end method

.method private initChangeListeners()V
    .locals 2

    invoke-direct {p0}, Lcom/android/mail/compose/ComposeActivity;->clearChangeListeners()V

    iget-object v0, p0, Lcom/android/mail/compose/ComposeActivity;->mSubject:Landroid/widget/TextView;

    invoke-virtual {v0, p0}, Landroid/widget/TextView;->addTextChangedListener(Landroid/text/TextWatcher;)V

    iget-object v0, p0, Lcom/android/mail/compose/ComposeActivity;->mBodyView:Landroid/widget/EditText;

    invoke-virtual {v0, p0}, Landroid/widget/EditText;->addTextChangedListener(Landroid/text/TextWatcher;)V

    iget-object v0, p0, Lcom/android/mail/compose/ComposeActivity;->mToListener:Lcom/android/mail/compose/ComposeActivity$RecipientTextWatcher;

    if-nez v0, :cond_0

    new-instance v0, Lcom/android/mail/compose/ComposeActivity$RecipientTextWatcher;

    iget-object v1, p0, Lcom/android/mail/compose/ComposeActivity;->mTo:Lcom/android/ex/chips/RecipientEditTextView;

    invoke-direct {v0, p0, v1, p0}, Lcom/android/mail/compose/ComposeActivity$RecipientTextWatcher;-><init>(Lcom/android/mail/compose/ComposeActivity;Lcom/android/ex/chips/RecipientEditTextView;Landroid/text/TextWatcher;)V

    iput-object v0, p0, Lcom/android/mail/compose/ComposeActivity;->mToListener:Lcom/android/mail/compose/ComposeActivity$RecipientTextWatcher;

    :cond_0
    iget-object v0, p0, Lcom/android/mail/compose/ComposeActivity;->mTo:Lcom/android/ex/chips/RecipientEditTextView;

    iget-object v1, p0, Lcom/android/mail/compose/ComposeActivity;->mToListener:Lcom/android/mail/compose/ComposeActivity$RecipientTextWatcher;

    invoke-virtual {v0, v1}, Lcom/android/ex/chips/RecipientEditTextView;->addTextChangedListener(Landroid/text/TextWatcher;)V

    iget-object v0, p0, Lcom/android/mail/compose/ComposeActivity;->mCcListener:Lcom/android/mail/compose/ComposeActivity$RecipientTextWatcher;

    if-nez v0, :cond_1

    new-instance v0, Lcom/android/mail/compose/ComposeActivity$RecipientTextWatcher;

    iget-object v1, p0, Lcom/android/mail/compose/ComposeActivity;->mCc:Lcom/android/ex/chips/RecipientEditTextView;

    invoke-direct {v0, p0, v1, p0}, Lcom/android/mail/compose/ComposeActivity$RecipientTextWatcher;-><init>(Lcom/android/mail/compose/ComposeActivity;Lcom/android/ex/chips/RecipientEditTextView;Landroid/text/TextWatcher;)V

    iput-object v0, p0, Lcom/android/mail/compose/ComposeActivity;->mCcListener:Lcom/android/mail/compose/ComposeActivity$RecipientTextWatcher;

    :cond_1
    iget-object v0, p0, Lcom/android/mail/compose/ComposeActivity;->mCc:Lcom/android/ex/chips/RecipientEditTextView;

    iget-object v1, p0, Lcom/android/mail/compose/ComposeActivity;->mCcListener:Lcom/android/mail/compose/ComposeActivity$RecipientTextWatcher;

    invoke-virtual {v0, v1}, Lcom/android/ex/chips/RecipientEditTextView;->addTextChangedListener(Landroid/text/TextWatcher;)V

    iget-object v0, p0, Lcom/android/mail/compose/ComposeActivity;->mBccListener:Lcom/android/mail/compose/ComposeActivity$RecipientTextWatcher;

    if-nez v0, :cond_2

    new-instance v0, Lcom/android/mail/compose/ComposeActivity$RecipientTextWatcher;

    iget-object v1, p0, Lcom/android/mail/compose/ComposeActivity;->mBcc:Lcom/android/ex/chips/RecipientEditTextView;

    invoke-direct {v0, p0, v1, p0}, Lcom/android/mail/compose/ComposeActivity$RecipientTextWatcher;-><init>(Lcom/android/mail/compose/ComposeActivity;Lcom/android/ex/chips/RecipientEditTextView;Landroid/text/TextWatcher;)V

    iput-object v0, p0, Lcom/android/mail/compose/ComposeActivity;->mBccListener:Lcom/android/mail/compose/ComposeActivity$RecipientTextWatcher;

    :cond_2
    iget-object v0, p0, Lcom/android/mail/compose/ComposeActivity;->mBcc:Lcom/android/ex/chips/RecipientEditTextView;

    iget-object v1, p0, Lcom/android/mail/compose/ComposeActivity;->mBccListener:Lcom/android/mail/compose/ComposeActivity$RecipientTextWatcher;

    invoke-virtual {v0, v1}, Lcom/android/ex/chips/RecipientEditTextView;->addTextChangedListener(Landroid/text/TextWatcher;)V

    iget-object v0, p0, Lcom/android/mail/compose/ComposeActivity;->mFromSpinner:Lcom/android/mail/compose/FromAddressSpinner;

    invoke-virtual {v0, p0}, Lcom/android/mail/compose/FromAddressSpinner;->setOnAccountChangedListener(Lcom/android/mail/compose/FromAddressSpinner$OnAccountChangedListener;)V

    iget-object v0, p0, Lcom/android/mail/compose/ComposeActivity;->mAttachmentsView:Lcom/android/mail/compose/AttachmentsView;

    invoke-virtual {v0, p0}, Lcom/android/mail/compose/AttachmentsView;->setAttachmentChangesListener(Lcom/android/mail/compose/AttachmentsView$AttachmentAddedOrDeletedListener;)V

    return-void
.end method

.method private initFromDraftMessage(Lcom/android/mail/providers/Message;)V
    .locals 7

    const/4 v4, 0x0

    const/4 v0, 0x1

    const/4 v1, 0x0

    const/4 v2, -0x1

    sget-object v3, Lcom/android/mail/compose/ComposeActivity;->LOG_TAG:Ljava/lang/String;

    const-string v5, "Intializing draft from previous draft message: %s"

    new-array v6, v0, [Ljava/lang/Object;

    aput-object p1, v6, v1

    invoke-static {v3, v5, v6}, Lcom/android/mail/utils/LogUtils;->d(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)I

    iput-object p1, p0, Lcom/android/mail/compose/ComposeActivity;->mDraft:Lcom/android/mail/providers/Message;

    iget-wide v5, p1, Lcom/android/mail/providers/Message;->id:J

    iput-wide v5, p0, Lcom/android/mail/compose/ComposeActivity;->mDraftId:J

    iget-object v3, p0, Lcom/android/mail/compose/ComposeActivity;->mSubject:Landroid/widget/TextView;

    iget-object v5, p1, Lcom/android/mail/providers/Message;->subject:Ljava/lang/String;

    invoke-virtual {v3, v5}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget v3, p1, Lcom/android/mail/providers/Message;->draftType:I

    const/4 v5, 0x4

    if-ne v3, v5, :cond_0

    :goto_0
    iput-boolean v0, p0, Lcom/android/mail/compose/ComposeActivity;->mForward:Z

    invoke-virtual {p1}, Lcom/android/mail/providers/Message;->getToAddresses()[Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/android/mail/compose/ComposeActivity;->addToAddresses(Ljava/util/Collection;)V

    invoke-virtual {p1}, Lcom/android/mail/providers/Message;->getCcAddresses()[Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v3

    invoke-direct {p0, v3, v0}, Lcom/android/mail/compose/ComposeActivity;->addCcAddresses(Ljava/util/Collection;Ljava/util/Collection;)V

    invoke-virtual {p1}, Lcom/android/mail/providers/Message;->getBccAddresses()[Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/android/mail/compose/ComposeActivity;->addBccAddresses(Ljava/util/Collection;)V

    iget-boolean v0, p1, Lcom/android/mail/providers/Message;->hasAttachments:Z

    if-eqz v0, :cond_1

    invoke-virtual {p1}, Lcom/android/mail/providers/Message;->getAttachments()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_1
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/mail/providers/Attachment;

    invoke-virtual {p0, v0}, Lcom/android/mail/compose/ComposeActivity;->addAttachmentAndUpdateView(Lcom/android/mail/providers/Attachment;)V

    goto :goto_1

    :cond_0
    move v0, v1

    goto :goto_0

    :cond_1
    iget-boolean v0, p1, Lcom/android/mail/providers/Message;->appendRefMessageContent:Z

    if-eqz v0, :cond_3

    iget v0, p1, Lcom/android/mail/providers/Message;->quotedTextOffset:I

    :goto_2
    iget-object v3, p1, Lcom/android/mail/providers/Message;->bodyHtml:Ljava/lang/String;

    invoke-static {v3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_5

    const-string v3, ""

    if-le v0, v2, :cond_4

    iget-object v0, p1, Lcom/android/mail/providers/Message;->bodyHtml:Ljava/lang/String;

    invoke-static {v0}, Lcom/android/mail/compose/QuotedTextView;->findQuotedTextIndex(Ljava/lang/CharSequence;)I

    move-result v5

    if-le v5, v2, :cond_9

    iget-object v0, p1, Lcom/android/mail/providers/Message;->bodyHtml:Ljava/lang/String;

    invoke-virtual {v0, v1, v5}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/android/mail/utils/Utils;->convertHtmlToPlainText(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iget-object v1, p1, Lcom/android/mail/providers/Message;->bodyHtml:Ljava/lang/String;

    iget-object v3, p1, Lcom/android/mail/providers/Message;->bodyHtml:Ljava/lang/String;

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v3

    invoke-virtual {v1, v5, v3}, Ljava/lang/String;->subSequence(II)Ljava/lang/CharSequence;

    move-result-object v1

    move v3, v5

    :goto_3
    iget-object v4, p0, Lcom/android/mail/compose/ComposeActivity;->mBodyView:Landroid/widget/EditText;

    invoke-virtual {v4, v0}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    :goto_4
    if-le v3, v2, :cond_2

    if-eqz v1, :cond_2

    iget-object v0, p0, Lcom/android/mail/compose/ComposeActivity;->mQuotedTextView:Lcom/android/mail/compose/QuotedTextView;

    iget-boolean v2, p0, Lcom/android/mail/compose/ComposeActivity;->mForward:Z

    invoke-virtual {v0, v1, v2}, Lcom/android/mail/compose/QuotedTextView;->setQuotedTextFromDraft(Ljava/lang/CharSequence;Z)V

    :cond_2
    return-void

    :cond_3
    move v0, v2

    goto :goto_2

    :cond_4
    iget-object v1, p1, Lcom/android/mail/providers/Message;->bodyHtml:Ljava/lang/String;

    invoke-static {v1}, Lcom/android/mail/utils/Utils;->convertHtmlToPlainText(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    move v3, v0

    move-object v0, v1

    move-object v1, v4

    goto :goto_3

    :cond_5
    iget-object v3, p1, Lcom/android/mail/providers/Message;->bodyText:Ljava/lang/String;

    invoke-static {v3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v5

    if-nez v5, :cond_8

    if-le v0, v2, :cond_7

    iget-object v5, p1, Lcom/android/mail/providers/Message;->bodyText:Ljava/lang/String;

    invoke-virtual {v5, v1, v0}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v1

    :goto_5
    if-le v0, v2, :cond_6

    invoke-static {v3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_6

    iget-object v3, p1, Lcom/android/mail/providers/Message;->bodyText:Ljava/lang/String;

    invoke-virtual {v3, v0}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v4

    :cond_6
    iget-object v3, p0, Lcom/android/mail/compose/ComposeActivity;->mBodyView:Landroid/widget/EditText;

    invoke-virtual {v3, v1}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    move-object v1, v4

    move v3, v0

    goto :goto_4

    :cond_7
    iget-object v1, p1, Lcom/android/mail/providers/Message;->bodyText:Ljava/lang/String;

    goto :goto_5

    :cond_8
    const-string v1, ""

    goto :goto_5

    :cond_9
    move-object v0, v3

    move-object v1, v4

    move v3, v5

    goto :goto_3
.end method

.method private initFromRefMessage(I)V
    .locals 3
    .param p1    # I

    const/4 v2, 0x1

    const/4 v1, 0x0

    invoke-direct {p0, p1}, Lcom/android/mail/compose/ComposeActivity;->setFieldsFromRefMessage(I)V

    iget-object v0, p0, Lcom/android/mail/compose/ComposeActivity;->mRefMessage:Lcom/android/mail/providers/Message;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/mail/compose/ComposeActivity;->mCc:Lcom/android/ex/chips/RecipientEditTextView;

    invoke-virtual {v0}, Lcom/android/ex/chips/RecipientEditTextView;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    if-ne p1, v2, :cond_0

    iget-object v0, p0, Lcom/android/mail/compose/ComposeActivity;->mCcBccView:Lcom/android/mail/compose/CcBccView;

    invoke-virtual {v0, v1, v2, v1}, Lcom/android/mail/compose/CcBccView;->show(ZZZ)V

    :cond_0
    invoke-direct {p0}, Lcom/android/mail/compose/ComposeActivity;->updateHideOrShowCcBcc()V

    return-void
.end method

.method private initFromSpinner(Landroid/os/Bundle;I)V
    .locals 7
    .param p1    # Landroid/os/Bundle;
    .param p2    # I

    const/16 v6, 0x8

    const/4 v5, 0x1

    const/4 v4, 0x0

    const/4 v0, 0x0

    const/4 v1, 0x3

    if-ne p2, v1, :cond_0

    iget-object v1, p0, Lcom/android/mail/compose/ComposeActivity;->mDraft:Lcom/android/mail/providers/Message;

    iget v1, v1, Lcom/android/mail/providers/Message;->draftType:I

    if-ne v1, v5, :cond_0

    const/4 p2, -0x1

    :cond_0
    iget-object v1, p0, Lcom/android/mail/compose/ComposeActivity;->mFromSpinner:Lcom/android/mail/compose/FromAddressSpinner;

    iget-object v2, p0, Lcom/android/mail/compose/ComposeActivity;->mAccount:Lcom/android/mail/providers/Account;

    iget-object v3, p0, Lcom/android/mail/compose/ComposeActivity;->mAccounts:[Lcom/android/mail/providers/Account;

    invoke-virtual {v1, p2, v2, v3}, Lcom/android/mail/compose/FromAddressSpinner;->asyncInitFromSpinner(ILcom/android/mail/providers/Account;[Lcom/android/mail/providers/Account;)V

    if-eqz p1, :cond_1

    const-string v1, "replyFromAccount"

    invoke-virtual {p1, v1}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_4

    iget-object v1, p0, Lcom/android/mail/compose/ComposeActivity;->mAccount:Lcom/android/mail/providers/Account;

    const-string v2, "replyFromAccount"

    invoke-virtual {p1, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/android/mail/providers/ReplyFromAccount;->deserialize(Lcom/android/mail/providers/Account;Ljava/lang/String;)Lcom/android/mail/providers/ReplyFromAccount;

    move-result-object v1

    iput-object v1, p0, Lcom/android/mail/compose/ComposeActivity;->mReplyFromAccount:Lcom/android/mail/providers/ReplyFromAccount;

    :cond_1
    :goto_0
    iget-object v1, p0, Lcom/android/mail/compose/ComposeActivity;->mReplyFromAccount:Lcom/android/mail/providers/ReplyFromAccount;

    if-nez v1, :cond_2

    iget-object v1, p0, Lcom/android/mail/compose/ComposeActivity;->mDraft:Lcom/android/mail/providers/Message;

    if-eqz v1, :cond_5

    iget-object v1, p0, Lcom/android/mail/compose/ComposeActivity;->mAccount:Lcom/android/mail/providers/Account;

    iget-object v2, p0, Lcom/android/mail/compose/ComposeActivity;->mDraft:Lcom/android/mail/providers/Message;

    invoke-direct {p0, v1, v2}, Lcom/android/mail/compose/ComposeActivity;->getReplyFromAccountFromDraft(Lcom/android/mail/providers/Account;Lcom/android/mail/providers/Message;)Lcom/android/mail/providers/ReplyFromAccount;

    move-result-object v1

    iput-object v1, p0, Lcom/android/mail/compose/ComposeActivity;->mReplyFromAccount:Lcom/android/mail/providers/ReplyFromAccount;

    :cond_2
    :goto_1
    iget-object v1, p0, Lcom/android/mail/compose/ComposeActivity;->mReplyFromAccount:Lcom/android/mail/providers/ReplyFromAccount;

    if-nez v1, :cond_3

    iget-object v1, p0, Lcom/android/mail/compose/ComposeActivity;->mAccount:Lcom/android/mail/providers/Account;

    invoke-direct {p0, v1}, Lcom/android/mail/compose/ComposeActivity;->getDefaultReplyFromAccount(Lcom/android/mail/providers/Account;)Lcom/android/mail/providers/ReplyFromAccount;

    move-result-object v1

    iput-object v1, p0, Lcom/android/mail/compose/ComposeActivity;->mReplyFromAccount:Lcom/android/mail/providers/ReplyFromAccount;

    :cond_3
    iget-object v1, p0, Lcom/android/mail/compose/ComposeActivity;->mFromSpinner:Lcom/android/mail/compose/FromAddressSpinner;

    iget-object v2, p0, Lcom/android/mail/compose/ComposeActivity;->mReplyFromAccount:Lcom/android/mail/providers/ReplyFromAccount;

    invoke-virtual {v1, v2}, Lcom/android/mail/compose/FromAddressSpinner;->setCurrentAccount(Lcom/android/mail/providers/ReplyFromAccount;)V

    iget-object v1, p0, Lcom/android/mail/compose/ComposeActivity;->mFromSpinner:Lcom/android/mail/compose/FromAddressSpinner;

    invoke-virtual {v1}, Lcom/android/mail/compose/FromAddressSpinner;->getCount()I

    move-result v1

    if-le v1, v5, :cond_6

    iget-object v1, p0, Lcom/android/mail/compose/ComposeActivity;->mFromStatic:Landroid/view/View;

    invoke-virtual {v1, v6}, Landroid/view/View;->setVisibility(I)V

    iget-object v1, p0, Lcom/android/mail/compose/ComposeActivity;->mFromStaticText:Landroid/widget/TextView;

    iget-object v2, p0, Lcom/android/mail/compose/ComposeActivity;->mAccount:Lcom/android/mail/providers/Account;

    iget-object v2, v2, Lcom/android/mail/providers/Account;->name:Ljava/lang/String;

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v1, p0, Lcom/android/mail/compose/ComposeActivity;->mFromSpinnerWrapper:Landroid/view/View;

    invoke-virtual {v1, v4}, Landroid/view/View;->setVisibility(I)V

    :goto_2
    return-void

    :cond_4
    const-string v1, "fromAccountString"

    invoke-virtual {p1, v1}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_1

    const-string v1, "fromAccountString"

    invoke-virtual {p1, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/android/mail/compose/ComposeActivity;->mFromSpinner:Lcom/android/mail/compose/FromAddressSpinner;

    invoke-virtual {v1, v0}, Lcom/android/mail/compose/FromAddressSpinner;->getMatchingReplyFromAccount(Ljava/lang/String;)Lcom/android/mail/providers/ReplyFromAccount;

    move-result-object v1

    iput-object v1, p0, Lcom/android/mail/compose/ComposeActivity;->mReplyFromAccount:Lcom/android/mail/providers/ReplyFromAccount;

    goto :goto_0

    :cond_5
    iget-object v1, p0, Lcom/android/mail/compose/ComposeActivity;->mRefMessage:Lcom/android/mail/providers/Message;

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/android/mail/compose/ComposeActivity;->mAccount:Lcom/android/mail/providers/Account;

    iget-object v2, p0, Lcom/android/mail/compose/ComposeActivity;->mRefMessage:Lcom/android/mail/providers/Message;

    invoke-direct {p0, v1, v2}, Lcom/android/mail/compose/ComposeActivity;->getReplyFromAccountForReply(Lcom/android/mail/providers/Account;Lcom/android/mail/providers/Message;)Lcom/android/mail/providers/ReplyFromAccount;

    move-result-object v1

    iput-object v1, p0, Lcom/android/mail/compose/ComposeActivity;->mReplyFromAccount:Lcom/android/mail/providers/ReplyFromAccount;

    goto :goto_1

    :cond_6
    iget-object v1, p0, Lcom/android/mail/compose/ComposeActivity;->mFromStatic:Landroid/view/View;

    invoke-virtual {v1, v4}, Landroid/view/View;->setVisibility(I)V

    iget-object v1, p0, Lcom/android/mail/compose/ComposeActivity;->mFromStaticText:Landroid/widget/TextView;

    iget-object v2, p0, Lcom/android/mail/compose/ComposeActivity;->mAccount:Lcom/android/mail/providers/Account;

    iget-object v2, v2, Lcom/android/mail/providers/Account;->name:Ljava/lang/String;

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v1, p0, Lcom/android/mail/compose/ComposeActivity;->mFromSpinnerWrapper:Landroid/view/View;

    invoke-virtual {v1, v6}, Landroid/view/View;->setVisibility(I)V

    goto :goto_2
.end method

.method private initQuotedTextFromRefMessage(Lcom/android/mail/providers/Message;I)V
    .locals 3
    .param p1    # Lcom/android/mail/providers/Message;
    .param p2    # I

    const/4 v2, 0x2

    const/4 v0, 0x1

    iget-object v1, p0, Lcom/android/mail/compose/ComposeActivity;->mRefMessage:Lcom/android/mail/providers/Message;

    if-eqz v1, :cond_1

    if-eqz p2, :cond_0

    if-eq p2, v0, :cond_0

    if-ne p2, v2, :cond_1

    :cond_0
    iget-object v1, p0, Lcom/android/mail/compose/ComposeActivity;->mQuotedTextView:Lcom/android/mail/compose/QuotedTextView;

    if-eq p2, v2, :cond_2

    :goto_0
    invoke-virtual {v1, p2, p1, v0}, Lcom/android/mail/compose/QuotedTextView;->setQuotedText(ILcom/android/mail/providers/Message;Z)V

    :cond_1
    return-void

    :cond_2
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private initRecipients()V
    .locals 1

    iget-object v0, p0, Lcom/android/mail/compose/ComposeActivity;->mTo:Lcom/android/ex/chips/RecipientEditTextView;

    invoke-direct {p0, v0}, Lcom/android/mail/compose/ComposeActivity;->setupRecipients(Lcom/android/ex/chips/RecipientEditTextView;)V

    iget-object v0, p0, Lcom/android/mail/compose/ComposeActivity;->mCc:Lcom/android/ex/chips/RecipientEditTextView;

    invoke-direct {p0, v0}, Lcom/android/mail/compose/ComposeActivity;->setupRecipients(Lcom/android/ex/chips/RecipientEditTextView;)V

    iget-object v0, p0, Lcom/android/mail/compose/ComposeActivity;->mBcc:Lcom/android/ex/chips/RecipientEditTextView;

    invoke-direct {p0, v0}, Lcom/android/mail/compose/ComposeActivity;->setupRecipients(Lcom/android/ex/chips/RecipientEditTextView;)V

    return-void
.end method

.method private static launch(Landroid/content/Context;Lcom/android/mail/providers/Account;Lcom/android/mail/providers/Message;I)V
    .locals 3

    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/android/mail/compose/ComposeActivity;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const-string v1, "fromemail"

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    const-string v1, "action"

    invoke-virtual {v0, v1, p3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    const-string v1, "account"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    const/4 v1, 0x3

    if-ne p3, v1, :cond_0

    const-string v1, "original-draft-message"

    invoke-virtual {v0, v1, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    :goto_0
    invoke-virtual {p0, v0}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    return-void

    :cond_0
    const-string v1, "in-reference-to-message"

    invoke-virtual {v0, v1, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    goto :goto_0
.end method

.method private obtainAccount(Landroid/content/Intent;)Lcom/android/mail/providers/Account;
    .locals 10
    .param p1    # Landroid/content/Intent;

    const/4 v1, 0x0

    const/4 v2, 0x0

    if-eqz p1, :cond_2

    invoke-virtual {p1}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v8

    if-eqz v8, :cond_2

    invoke-virtual {p1}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v8

    const-string v9, "account"

    invoke-virtual {v8, v9}, Landroid/os/Bundle;->get(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v2

    instance-of v8, v2, Lcom/android/mail/providers/Account;

    if-eqz v8, :cond_0

    check-cast v2, Lcom/android/mail/providers/Account;

    :goto_0
    return-object v2

    :cond_0
    instance-of v8, v2, Ljava/lang/String;

    if-eqz v8, :cond_1

    check-cast v2, Ljava/lang/String;

    invoke-static {v2}, Lcom/android/mail/providers/Account;->newinstance(Ljava/lang/String;)Lcom/android/mail/providers/Account;

    move-result-object v1

    if-eqz v1, :cond_1

    move-object v2, v1

    goto :goto_0

    :cond_1
    const-string v8, "account"

    invoke-virtual {p1, v8}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    move-result v8

    if-eqz v8, :cond_5

    const-string v8, "account"

    invoke-virtual {p1, v8}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    :cond_2
    :goto_1
    if-nez v1, :cond_a

    invoke-static {}, Lcom/android/mail/providers/MailAppProvider;->getInstance()Lcom/android/mail/providers/MailAppProvider;

    move-result-object v7

    invoke-virtual {v7}, Lcom/android/mail/providers/MailAppProvider;->getLastSentFromAccount()Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v8

    if-eqz v8, :cond_3

    invoke-virtual {v7}, Lcom/android/mail/providers/MailAppProvider;->getLastViewedAccount()Ljava/lang/String;

    move-result-object v5

    :cond_3
    invoke-static {v5}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v8

    if-nez v8, :cond_a

    invoke-static {v5}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v2

    move-object v9, v2

    :goto_2
    iget-object v8, p0, Lcom/android/mail/compose/ComposeActivity;->mAccounts:[Lcom/android/mail/providers/Account;

    if-eqz v8, :cond_9

    iget-object v8, p0, Lcom/android/mail/compose/ComposeActivity;->mAccounts:[Lcom/android/mail/providers/Account;

    array-length v8, v8

    if-lez v8, :cond_9

    instance-of v8, v9, Ljava/lang/String;

    if-eqz v8, :cond_6

    move-object v8, v9

    check-cast v8, Ljava/lang/String;

    invoke-static {v8}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v8

    if-nez v8, :cond_6

    iget-object v3, p0, Lcom/android/mail/compose/ComposeActivity;->mAccounts:[Lcom/android/mail/providers/Account;

    array-length v6, v3

    const/4 v4, 0x0

    :goto_3
    if-ge v4, v6, :cond_8

    aget-object v0, v3, v4

    iget-object v8, v0, Lcom/android/mail/providers/Account;->name:Ljava/lang/String;

    invoke-virtual {v8, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_4

    move-object v1, v0

    :cond_4
    add-int/lit8 v4, v4, 0x1

    goto :goto_3

    :cond_5
    const-string v8, "selectedAccount"

    invoke-virtual {p1, v8}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    goto :goto_1

    :cond_6
    instance-of v8, v9, Landroid/net/Uri;

    if-eqz v8, :cond_8

    iget-object v3, p0, Lcom/android/mail/compose/ComposeActivity;->mAccounts:[Lcom/android/mail/providers/Account;

    array-length v6, v3

    const/4 v4, 0x0

    :goto_4
    if-ge v4, v6, :cond_8

    aget-object v0, v3, v4

    iget-object v8, v0, Lcom/android/mail/providers/Account;->uri:Landroid/net/Uri;

    invoke-virtual {v8, v9}, Landroid/net/Uri;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_7

    move-object v1, v0

    :cond_7
    add-int/lit8 v4, v4, 0x1

    goto :goto_4

    :cond_8
    if-nez v1, :cond_9

    iget-object v8, p0, Lcom/android/mail/compose/ComposeActivity;->mAccounts:[Lcom/android/mail/providers/Account;

    const/4 v9, 0x0

    aget-object v1, v8, v9

    :cond_9
    move-object v2, v1

    goto/16 :goto_0

    :cond_a
    move-object v9, v2

    goto :goto_2
.end method

.method private onAppUpPressed()V
    .locals 2

    iget-boolean v0, p0, Lcom/android/mail/compose/ComposeActivity;->mLaunchedFromEmail:Z

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/android/mail/compose/ComposeActivity;->onBackPressed()V

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/android/mail/compose/ComposeActivity;->mAccount:Lcom/android/mail/providers/Account;

    invoke-static {v0}, Lcom/android/mail/utils/Utils;->createViewInboxIntent(Lcom/android/mail/providers/Account;)Landroid/content/Intent;

    move-result-object v0

    const v1, 0x1000c000

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    invoke-virtual {p0, v0}, Lcom/android/mail/compose/ComposeActivity;->startActivity(Landroid/content/Intent;)V

    invoke-virtual {p0}, Lcom/android/mail/compose/ComposeActivity;->finish()V

    goto :goto_0
.end method

.method public static registerTestSendOrSaveCallback(Lcom/android/mail/compose/ComposeActivity$SendOrSaveCallback;)V
    .locals 2

    sget-object v0, Lcom/android/mail/compose/ComposeActivity;->sTestSendOrSaveCallback:Lcom/android/mail/compose/ComposeActivity$SendOrSaveCallback;

    if-eqz v0, :cond_0

    if-eqz p0, :cond_0

    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Attempting to register more than one test callback"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    sput-object p0, Lcom/android/mail/compose/ComposeActivity;->sTestSendOrSaveCallback:Lcom/android/mail/compose/ComposeActivity$SendOrSaveCallback;

    return-void
.end method

.method private static removeComposingSpans(Landroid/text/Spanned;)Landroid/text/SpannableString;
    .locals 1
    .param p0    # Landroid/text/Spanned;

    new-instance v0, Landroid/text/SpannableString;

    invoke-direct {v0, p0}, Landroid/text/SpannableString;-><init>(Ljava/lang/CharSequence;)V

    invoke-static {v0}, Landroid/view/inputmethod/BaseInputConnection;->removeComposingSpans(Landroid/text/Spannable;)V

    return-object v0
.end method

.method private replaceFragment(Landroid/app/Fragment;ILjava/lang/String;)I
    .locals 2

    invoke-virtual {p0}, Lcom/android/mail/compose/ComposeActivity;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/FragmentManager;->beginTransaction()Landroid/app/FragmentTransaction;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/app/FragmentTransaction;->addToBackStack(Ljava/lang/String;)Landroid/app/FragmentTransaction;

    invoke-virtual {v0, p2}, Landroid/app/FragmentTransaction;->setTransition(I)Landroid/app/FragmentTransaction;

    const v1, 0x7f08003b

    invoke-virtual {v0, v1, p1, p3}, Landroid/app/FragmentTransaction;->replace(ILandroid/app/Fragment;Ljava/lang/String;)Landroid/app/FragmentTransaction;

    invoke-virtual {v0}, Landroid/app/FragmentTransaction;->commitAllowingStateLoss()I

    move-result v0

    return v0
.end method

.method public static reply(Landroid/content/Context;Lcom/android/mail/providers/Account;Lcom/android/mail/providers/Message;)V
    .locals 1
    .param p0    # Landroid/content/Context;
    .param p1    # Lcom/android/mail/providers/Account;
    .param p2    # Lcom/android/mail/providers/Message;

    const/4 v0, 0x0

    invoke-static {p0, p1, p2, v0}, Lcom/android/mail/compose/ComposeActivity;->launch(Landroid/content/Context;Lcom/android/mail/providers/Account;Lcom/android/mail/providers/Message;I)V

    return-void
.end method

.method public static replyAll(Landroid/content/Context;Lcom/android/mail/providers/Account;Lcom/android/mail/providers/Message;)V
    .locals 1
    .param p0    # Landroid/content/Context;
    .param p1    # Lcom/android/mail/providers/Account;
    .param p2    # Lcom/android/mail/providers/Message;

    const/4 v0, 0x1

    invoke-static {p0, p1, p2, v0}, Lcom/android/mail/compose/ComposeActivity;->launch(Landroid/content/Context;Lcom/android/mail/providers/Account;Lcom/android/mail/providers/Message;I)V

    return-void
.end method

.method private saveIfNeeded()V
    .locals 1

    iget-object v0, p0, Lcom/android/mail/compose/ComposeActivity;->mAccount:Lcom/android/mail/providers/Account;

    if-nez v0, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    invoke-direct {p0}, Lcom/android/mail/compose/ComposeActivity;->shouldSave()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/android/mail/compose/ComposeActivity;->mAddingAttachment:Z

    if-nez v0, :cond_2

    const/4 v0, 0x1

    :goto_1
    invoke-direct {p0, v0}, Lcom/android/mail/compose/ComposeActivity;->doSave(Z)V

    goto :goto_0

    :cond_2
    const/4 v0, 0x0

    goto :goto_1
.end method

.method private saveRequestMap()V
    .locals 0

    return-void
.end method

.method private sendOrSave(Landroid/text/Spanned;ZZZ)V
    .locals 12

    invoke-static {}, Landroid/app/ActivityManager;->isUserAMonkey()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    if-eqz p4, :cond_6

    const/4 v0, 0x0

    new-array v0, v0, [Ljava/lang/String;

    :goto_1
    new-instance v6, Lcom/android/mail/compose/ComposeActivity$3;

    invoke-direct {v6, p0}, Lcom/android/mail/compose/ComposeActivity$3;-><init>(Lcom/android/mail/compose/ComposeActivity;)V

    iget-object v0, p0, Lcom/android/mail/compose/ComposeActivity;->mReplyFromAccount:Lcom/android/mail/providers/ReplyFromAccount;

    iget-object v1, v0, Lcom/android/mail/providers/ReplyFromAccount;->name:Ljava/lang/String;

    if-eqz v0, :cond_2

    if-nez v1, :cond_8

    :cond_2
    iget-object v0, p0, Lcom/android/mail/compose/ComposeActivity;->mReplyFromAccount:Lcom/android/mail/providers/ReplyFromAccount;

    iget-object v1, p0, Lcom/android/mail/compose/ComposeActivity;->mAccount:Lcom/android/mail/providers/Account;

    iget-object v1, v1, Lcom/android/mail/providers/Account;->name:Ljava/lang/String;

    move-object v11, v0

    :goto_2
    iget-object v0, p0, Lcom/android/mail/compose/ComposeActivity;->mSendSaveTaskHandler:Landroid/os/Handler;

    if-nez v0, :cond_3

    new-instance v0, Landroid/os/HandlerThread;

    const-string v1, "Send Message Task Thread"

    invoke-direct {v0, v1}, Landroid/os/HandlerThread;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Landroid/os/HandlerThread;->start()V

    new-instance v1, Landroid/os/Handler;

    invoke-virtual {v0}, Landroid/os/HandlerThread;->getLooper()Landroid/os/Looper;

    move-result-object v0

    invoke-direct {v1, v0}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    iput-object v1, p0, Lcom/android/mail/compose/ComposeActivity;->mSendSaveTaskHandler:Landroid/os/Handler;

    :cond_3
    iget-object v0, p0, Lcom/android/mail/compose/ComposeActivity;->mReplyFromAccount:Lcom/android/mail/providers/ReplyFromAccount;

    invoke-direct {p0}, Lcom/android/mail/compose/ComposeActivity;->getMode()I

    move-result v1

    invoke-direct {p0, v0, v1}, Lcom/android/mail/compose/ComposeActivity;->createMessage(Lcom/android/mail/providers/ReplyFromAccount;I)Lcom/android/mail/providers/Message;

    move-result-object v2

    iget-object v1, p0, Lcom/android/mail/compose/ComposeActivity;->mReplyFromAccount:Lcom/android/mail/providers/ReplyFromAccount;

    iget-object v3, p0, Lcom/android/mail/compose/ComposeActivity;->mRefMessage:Lcom/android/mail/providers/Message;

    iget-object v0, p0, Lcom/android/mail/compose/ComposeActivity;->mQuotedTextView:Lcom/android/mail/compose/QuotedTextView;

    invoke-virtual {v0}, Lcom/android/mail/compose/QuotedTextView;->getQuotedTextIfIncluded()Ljava/lang/CharSequence;

    move-result-object v5

    iget-object v7, p0, Lcom/android/mail/compose/ComposeActivity;->mSendSaveTaskHandler:Landroid/os/Handler;

    iget v9, p0, Lcom/android/mail/compose/ComposeActivity;->mComposeMode:I

    iget-object v10, p0, Lcom/android/mail/compose/ComposeActivity;->mDraftAccount:Lcom/android/mail/providers/ReplyFromAccount;

    move-object v0, p0

    move-object v4, p1

    move v8, p2

    invoke-static/range {v0 .. v10}, Lcom/android/mail/compose/ComposeActivity;->sendOrSaveInternal(Landroid/content/Context;Lcom/android/mail/providers/ReplyFromAccount;Lcom/android/mail/providers/Message;Lcom/android/mail/providers/Message;Landroid/text/Spanned;Ljava/lang/CharSequence;Lcom/android/mail/compose/ComposeActivity$SendOrSaveCallback;Landroid/os/Handler;ZILcom/android/mail/providers/ReplyFromAccount;)I

    move-result v0

    iput v0, p0, Lcom/android/mail/compose/ComposeActivity;->mRequestId:I

    iget-object v0, p0, Lcom/android/mail/compose/ComposeActivity;->mRecipient:Ljava/lang/String;

    if-eqz v0, :cond_4

    iget-object v0, p0, Lcom/android/mail/compose/ComposeActivity;->mRecipient:Ljava/lang/String;

    iget-object v1, p0, Lcom/android/mail/compose/ComposeActivity;->mAccount:Lcom/android/mail/providers/Account;

    iget-object v1, v1, Lcom/android/mail/providers/Account;->name:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_4

    iget-object v0, v11, Lcom/android/mail/providers/ReplyFromAccount;->name:Ljava/lang/String;

    iput-object v0, p0, Lcom/android/mail/compose/ComposeActivity;->mRecipient:Ljava/lang/String;

    :cond_4
    iget-object v0, v11, Lcom/android/mail/providers/ReplyFromAccount;->account:Lcom/android/mail/providers/Account;

    invoke-virtual {p0, v0}, Lcom/android/mail/compose/ComposeActivity;->setAccount(Lcom/android/mail/providers/Account;)V

    if-eqz p3, :cond_5

    invoke-virtual {p0}, Lcom/android/mail/compose/ComposeActivity;->getChangingConfigurations()I

    move-result v0

    and-int/lit16 v0, v0, 0x80

    if-nez v0, :cond_5

    if-eqz p2, :cond_7

    const v0, 0x7f0900d4

    :goto_3
    const/4 v1, 0x1

    invoke-static {p0, v0, v1}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    :cond_5
    invoke-direct {p0}, Lcom/android/mail/compose/ComposeActivity;->discardChanges()V

    invoke-virtual {p0}, Lcom/android/mail/compose/ComposeActivity;->updateSaveUi()V

    if-nez p2, :cond_0

    invoke-virtual {p0}, Lcom/android/mail/compose/ComposeActivity;->finish()V

    goto/16 :goto_0

    :cond_6
    invoke-virtual {p0}, Lcom/android/mail/compose/ComposeActivity;->getToAddresses()[Ljava/lang/String;

    invoke-virtual {p0}, Lcom/android/mail/compose/ComposeActivity;->getCcAddresses()[Ljava/lang/String;

    invoke-virtual {p0}, Lcom/android/mail/compose/ComposeActivity;->getBccAddresses()[Ljava/lang/String;

    goto/16 :goto_1

    :cond_7
    const v0, 0x7f0900d5

    goto :goto_3

    :cond_8
    move-object v11, v0

    goto/16 :goto_2
.end method

.method static sendOrSaveInternal(Landroid/content/Context;Lcom/android/mail/providers/ReplyFromAccount;Lcom/android/mail/providers/Message;Lcom/android/mail/providers/Message;Landroid/text/Spanned;Ljava/lang/CharSequence;Lcom/android/mail/compose/ComposeActivity$SendOrSaveCallback;Landroid/os/Handler;ZILcom/android/mail/providers/ReplyFromAccount;)I
    .locals 8

    new-instance v4, Landroid/content/ContentValues;

    invoke-direct {v4}, Landroid/content/ContentValues;-><init>()V

    if-eqz p3, :cond_4

    iget-object v1, p3, Lcom/android/mail/providers/Message;->uri:Landroid/net/Uri;

    invoke-virtual {v1}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v5

    :goto_0
    invoke-virtual {p2}, Lcom/android/mail/providers/Message;->getToAddresses()[Ljava/lang/String;

    move-result-object v1

    invoke-static {v4, v1}, Lcom/android/mail/providers/MessageModification;->putToAddresses(Landroid/content/ContentValues;[Ljava/lang/String;)V

    invoke-virtual {p2}, Lcom/android/mail/providers/Message;->getCcAddresses()[Ljava/lang/String;

    move-result-object v1

    invoke-static {v4, v1}, Lcom/android/mail/providers/MessageModification;->putCcAddresses(Landroid/content/ContentValues;[Ljava/lang/String;)V

    invoke-virtual {p2}, Lcom/android/mail/providers/Message;->getBccAddresses()[Ljava/lang/String;

    move-result-object v1

    invoke-static {v4, v1}, Lcom/android/mail/providers/MessageModification;->putBccAddresses(Landroid/content/ContentValues;[Ljava/lang/String;)V

    invoke-virtual {p2}, Lcom/android/mail/providers/Message;->getFrom()Ljava/lang/String;

    move-result-object v1

    invoke-static {v4, v1}, Lcom/android/mail/providers/MessageModification;->putCustomFromAddress(Landroid/content/ContentValues;Ljava/lang/String;)V

    iget-object v1, p2, Lcom/android/mail/providers/Message;->subject:Ljava/lang/String;

    invoke-static {v4, v1}, Lcom/android/mail/providers/MessageModification;->putSubject(Landroid/content/ContentValues;Ljava/lang/String;)V

    invoke-static {p4}, Lcom/android/mail/compose/ComposeActivity;->removeComposingSpans(Landroid/text/Spanned;)Landroid/text/SpannableString;

    move-result-object v1

    invoke-static {v1}, Landroid/text/Html;->toHtml(Landroid/text/Spanned;)Ljava/lang/String;

    move-result-object v2

    invoke-static {p5}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_5

    const/4 v1, 0x1

    :goto_1
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    if-eqz v1, :cond_0

    invoke-virtual {p5}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/android/mail/compose/QuotedTextView;->containsQuotedText(Ljava/lang/String;)Z

    move-result v6

    if-eqz v6, :cond_7

    invoke-static {v2}, Lcom/android/mail/compose/QuotedTextView;->getQuotedTextOffset(Ljava/lang/String;)I

    move-result v6

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->length()I

    move-result v7

    add-int/2addr v6, v7

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-static {v4, v6}, Lcom/android/mail/providers/MessageModification;->putQuoteStartPos(Landroid/content/ContentValues;I)V

    const/4 v2, 0x2

    move/from16 v0, p9

    if-ne v0, v2, :cond_6

    const/4 v2, 0x1

    :goto_2
    invoke-static {v4, v2}, Lcom/android/mail/providers/MessageModification;->putForward(Landroid/content/ContentValues;Z)V

    invoke-static {v4, v1}, Lcom/android/mail/providers/MessageModification;->putAppendRefMessageContent(Landroid/content/ContentValues;Z)V

    :cond_0
    :goto_3
    invoke-static/range {p9 .. p9}, Lcom/android/mail/compose/ComposeActivity;->getDraftType(I)I

    move-result v1

    invoke-static {v4, v1}, Lcom/android/mail/providers/MessageModification;->putDraftType(Landroid/content/ContentValues;I)V

    if-eqz p3, :cond_8

    iget-object v1, p3, Lcom/android/mail/providers/Message;->bodyHtml:Ljava/lang/String;

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_1

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v4, v1}, Lcom/android/mail/providers/MessageModification;->putBodyHtml(Landroid/content/ContentValues;Ljava/lang/String;)V

    :cond_1
    iget-object v1, p3, Lcom/android/mail/providers/Message;->bodyText:Ljava/lang/String;

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_2

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/android/mail/utils/Utils;->convertHtmlToPlainText(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v4, v1}, Lcom/android/mail/providers/MessageModification;->putBody(Landroid/content/ContentValues;Ljava/lang/String;)V

    :cond_2
    :goto_4
    invoke-virtual {p2}, Lcom/android/mail/providers/Message;->getAttachments()Ljava/util/List;

    move-result-object v1

    invoke-static {v4, v1}, Lcom/android/mail/providers/MessageModification;->putAttachments(Landroid/content/ContentValues;Ljava/util/List;)V

    invoke-static {v5}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_3

    invoke-static {v4, v5}, Lcom/android/mail/providers/MessageModification;->putRefMessageId(Landroid/content/ContentValues;Ljava/lang/String;)V

    :cond_3
    new-instance v1, Lcom/android/mail/compose/ComposeActivity$SendOrSaveMessage;

    invoke-virtual {p2}, Lcom/android/mail/providers/Message;->getAttachments()Ljava/util/List;

    move-result-object v6

    move-object v2, p0

    move-object v3, p1

    move/from16 v7, p8

    invoke-direct/range {v1 .. v7}, Lcom/android/mail/compose/ComposeActivity$SendOrSaveMessage;-><init>(Landroid/content/Context;Lcom/android/mail/providers/ReplyFromAccount;Landroid/content/ContentValues;Ljava/lang/String;Ljava/util/List;Z)V

    new-instance v2, Lcom/android/mail/compose/ComposeActivity$SendOrSaveTask;

    move-object/from16 v0, p10

    invoke-direct {v2, p0, v1, p6, v0}, Lcom/android/mail/compose/ComposeActivity$SendOrSaveTask;-><init>(Landroid/content/Context;Lcom/android/mail/compose/ComposeActivity$SendOrSaveMessage;Lcom/android/mail/compose/ComposeActivity$SendOrSaveCallback;Lcom/android/mail/providers/ReplyFromAccount;)V

    invoke-interface {p6, v2}, Lcom/android/mail/compose/ComposeActivity$SendOrSaveCallback;->initializeSendOrSave(Lcom/android/mail/compose/ComposeActivity$SendOrSaveTask;)V

    invoke-virtual {p7, v2}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    invoke-virtual {v1}, Lcom/android/mail/compose/ComposeActivity$SendOrSaveMessage;->requestId()I

    move-result v1

    return v1

    :cond_4
    const-string v5, ""

    goto/16 :goto_0

    :cond_5
    const/4 v1, 0x0

    goto/16 :goto_1

    :cond_6
    const/4 v2, 0x0

    goto :goto_2

    :cond_7
    sget-object v1, Lcom/android/mail/compose/ComposeActivity;->LOG_TAG:Ljava/lang/String;

    const-string v6, "Couldn\'t find quoted text"

    const/4 v7, 0x0

    new-array v7, v7, [Ljava/lang/Object;

    invoke-static {v1, v6, v7}, Lcom/android/mail/utils/LogUtils;->w(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)I

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_3

    :cond_8
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v4, v1}, Lcom/android/mail/providers/MessageModification;->putBodyHtml(Landroid/content/ContentValues;Ljava/lang/String;)V

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/android/mail/utils/Utils;->convertHtmlToPlainText(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v4, v1}, Lcom/android/mail/providers/MessageModification;->putBody(Landroid/content/ContentValues;Ljava/lang/String;)V

    goto :goto_4
.end method

.method private setFieldsFromRefMessage(I)V
    .locals 2
    .param p1    # I

    const/4 v1, 0x2

    iget-object v0, p0, Lcom/android/mail/compose/ComposeActivity;->mRefMessage:Lcom/android/mail/providers/Message;

    invoke-direct {p0, v0, p1}, Lcom/android/mail/compose/ComposeActivity;->setSubject(Lcom/android/mail/providers/Message;I)V

    if-ne p1, v1, :cond_0

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/android/mail/compose/ComposeActivity;->mForward:Z

    :cond_0
    iget-object v0, p0, Lcom/android/mail/compose/ComposeActivity;->mRefMessage:Lcom/android/mail/providers/Message;

    invoke-virtual {p0, v0, p1}, Lcom/android/mail/compose/ComposeActivity;->initRecipientsFromRefMessage(Lcom/android/mail/providers/Message;I)V

    iget-object v0, p0, Lcom/android/mail/compose/ComposeActivity;->mRefMessage:Lcom/android/mail/providers/Message;

    invoke-direct {p0, v0, p1}, Lcom/android/mail/compose/ComposeActivity;->initQuotedTextFromRefMessage(Lcom/android/mail/providers/Message;I)V

    if-eq p1, v1, :cond_1

    iget-boolean v0, p0, Lcom/android/mail/compose/ComposeActivity;->mAttachmentsChanged:Z

    if-eqz v0, :cond_2

    :cond_1
    iget-object v0, p0, Lcom/android/mail/compose/ComposeActivity;->mRefMessage:Lcom/android/mail/providers/Message;

    invoke-virtual {p0, v0}, Lcom/android/mail/compose/ComposeActivity;->initAttachments(Lcom/android/mail/providers/Message;)V

    :cond_2
    return-void
.end method

.method private setFocus(I)V
    .locals 2
    .param p1    # I

    const/4 v1, 0x3

    if-ne p1, v1, :cond_0

    iget-object v1, p0, Lcom/android/mail/compose/ComposeActivity;->mDraft:Lcom/android/mail/providers/Message;

    iget v0, v1, Lcom/android/mail/providers/Message;->draftType:I

    packed-switch v0, :pswitch_data_0

    :pswitch_0
    const/4 p1, 0x0

    :cond_0
    :goto_0
    packed-switch p1, :pswitch_data_1

    :cond_1
    :pswitch_1
    invoke-virtual {p0}, Lcom/android/mail/compose/ComposeActivity;->focusBody()V

    :goto_1
    return-void

    :pswitch_2
    const/4 p1, -0x1

    goto :goto_0

    :pswitch_3
    iget-object v1, p0, Lcom/android/mail/compose/ComposeActivity;->mTo:Lcom/android/ex/chips/RecipientEditTextView;

    invoke-virtual {v1}, Lcom/android/ex/chips/RecipientEditTextView;->getText()Landroid/text/Editable;

    move-result-object v1

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/android/mail/compose/ComposeActivity;->mTo:Lcom/android/ex/chips/RecipientEditTextView;

    invoke-virtual {v1}, Lcom/android/ex/chips/RecipientEditTextView;->requestFocus()Z

    goto :goto_1

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_2
        :pswitch_0
        :pswitch_0
        :pswitch_2
    .end packed-switch

    :pswitch_data_1
    .packed-switch -0x1
        :pswitch_3
        :pswitch_1
        :pswitch_1
        :pswitch_3
    .end packed-switch
.end method

.method private setSubject(Lcom/android/mail/providers/Message;I)V
    .locals 5

    const/4 v4, 0x2

    iget-object v1, p1, Lcom/android/mail/providers/Message;->subject:Ljava/lang/String;

    const/4 v0, -0x1

    if-ne p2, v0, :cond_0

    const-string v0, ""

    :goto_0
    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_2

    invoke-virtual {v1}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_2

    move-object v0, v1

    :goto_1
    iget-object v1, p0, Lcom/android/mail/compose/ComposeActivity;->mSubject:Landroid/widget/TextView;

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    return-void

    :cond_0
    if-ne p2, v4, :cond_1

    const v0, 0x7f090038

    invoke-virtual {p0, v0}, Lcom/android/mail/compose/ComposeActivity;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    :cond_1
    const v0, 0x7f090037

    invoke-virtual {p0, v0}, Lcom/android/mail/compose/ComposeActivity;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    :cond_2
    const v2, 0x7f090036

    invoke-virtual {p0, v2}, Lcom/android/mail/compose/ComposeActivity;->getString(I)Ljava/lang/String;

    move-result-object v2

    new-array v3, v4, [Ljava/lang/Object;

    const/4 v4, 0x0

    aput-object v0, v3, v4

    const/4 v0, 0x1

    aput-object v1, v3, v0

    invoke-static {v2, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto :goto_1
.end method

.method private setupRecipients(Lcom/android/ex/chips/RecipientEditTextView;)V
    .locals 3

    new-instance v0, Lcom/android/mail/compose/RecipientAdapter;

    iget-object v1, p0, Lcom/android/mail/compose/ComposeActivity;->mAccount:Lcom/android/mail/providers/Account;

    invoke-direct {v0, p0, v1}, Lcom/android/mail/compose/RecipientAdapter;-><init>(Landroid/content/Context;Lcom/android/mail/providers/Account;)V

    invoke-virtual {p1, v0}, Lcom/android/ex/chips/RecipientEditTextView;->setAdapter(Landroid/widget/ListAdapter;)V

    new-instance v0, Landroid/text/util/Rfc822Tokenizer;

    invoke-direct {v0}, Landroid/text/util/Rfc822Tokenizer;-><init>()V

    invoke-virtual {p1, v0}, Lcom/android/ex/chips/RecipientEditTextView;->setTokenizer(Landroid/widget/MultiAutoCompleteTextView$Tokenizer;)V

    iget-object v0, p0, Lcom/android/mail/compose/ComposeActivity;->mValidator:Lcom/android/common/Rfc822Validator;

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/android/mail/compose/ComposeActivity;->mAccount:Lcom/android/mail/providers/Account;

    iget-object v0, v0, Lcom/android/mail/providers/Account;->name:Ljava/lang/String;

    const-string v1, "@"

    invoke-virtual {v0, v1}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    const/4 v2, -0x1

    if-le v1, v2, :cond_0

    const-string v1, "@"

    invoke-virtual {v0, v1}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    invoke-virtual {v0, v1}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v0

    :cond_0
    new-instance v1, Lcom/android/common/Rfc822Validator;

    invoke-direct {v1, v0}, Lcom/android/common/Rfc822Validator;-><init>(Ljava/lang/String;)V

    iput-object v1, p0, Lcom/android/mail/compose/ComposeActivity;->mValidator:Lcom/android/common/Rfc822Validator;

    :cond_1
    iget-object v0, p0, Lcom/android/mail/compose/ComposeActivity;->mValidator:Lcom/android/common/Rfc822Validator;

    invoke-virtual {p1, v0}, Lcom/android/ex/chips/RecipientEditTextView;->setValidator(Landroid/widget/AutoCompleteTextView$Validator;)V

    return-void
.end method

.method private shouldSave()Z
    .locals 2

    iget-object v1, p0, Lcom/android/mail/compose/ComposeActivity;->mDraftLock:Ljava/lang/Object;

    monitor-enter v1

    :try_start_0
    iget-boolean v0, p0, Lcom/android/mail/compose/ComposeActivity;->mTextChanged:Z

    if-nez v0, :cond_0

    iget-boolean v0, p0, Lcom/android/mail/compose/ComposeActivity;->mAttachmentsChanged:Z

    if-nez v0, :cond_0

    iget-boolean v0, p0, Lcom/android/mail/compose/ComposeActivity;->mReplyFromChanged:Z

    if-eqz v0, :cond_1

    :cond_0
    invoke-virtual {p0}, Lcom/android/mail/compose/ComposeActivity;->isBlank()Z

    move-result v0

    if-nez v0, :cond_1

    const/4 v0, 0x1

    :goto_0
    monitor-exit v1

    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method private showAttachmentTooBigToast(I)V
    .locals 4
    .param p1    # I

    invoke-virtual {p0}, Lcom/android/mail/compose/ComposeActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    iget-object v2, p0, Lcom/android/mail/compose/ComposeActivity;->mAccount:Lcom/android/mail/providers/Account;

    iget-object v2, v2, Lcom/android/mail/providers/Account;->settings:Lcom/android/mail/providers/Settings;

    invoke-virtual {v2}, Lcom/android/mail/providers/Settings;->getMaxAttachmentSize()I

    move-result v2

    int-to-long v2, v2

    invoke-static {v1, v2, v3}, Lcom/android/mail/utils/AttachmentUtils;->convertToHumanReadableSize(Landroid/content/Context;J)Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    aput-object v0, v1, v2

    invoke-virtual {p0, p1, v1}, Lcom/android/mail/compose/ComposeActivity;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {p0, v1}, Lcom/android/mail/compose/ComposeActivity;->showErrorToast(Ljava/lang/String;)V

    return-void
.end method

.method private showCcBcc(Landroid/os/Bundle;)V
    .locals 4
    .param p1    # Landroid/os/Bundle;

    if-eqz p1, :cond_1

    const-string v2, "showCc"

    invoke-virtual {p1, v2}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_1

    const-string v2, "showCc"

    invoke-virtual {p1, v2}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v1

    const-string v2, "showBcc"

    invoke-virtual {p1, v2}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v0

    if-nez v1, :cond_0

    if-eqz v0, :cond_1

    :cond_0
    iget-object v2, p0, Lcom/android/mail/compose/ComposeActivity;->mCcBccView:Lcom/android/mail/compose/CcBccView;

    const/4 v3, 0x0

    invoke-virtual {v2, v3, v1, v0}, Lcom/android/mail/compose/CcBccView;->show(ZZZ)V

    :cond_1
    return-void
.end method

.method private showCcBccViews()V
    .locals 2

    const/4 v1, 0x1

    iget-object v0, p0, Lcom/android/mail/compose/ComposeActivity;->mCcBccView:Lcom/android/mail/compose/CcBccView;

    invoke-virtual {v0, v1, v1, v1}, Lcom/android/mail/compose/CcBccView;->show(ZZZ)V

    iget-object v0, p0, Lcom/android/mail/compose/ComposeActivity;->mCcBccButton:Landroid/widget/Button;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/mail/compose/ComposeActivity;->mCcBccButton:Landroid/widget/Button;

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setVisibility(I)V

    :cond_0
    return-void
.end method

.method private showErrorToast(Ljava/lang/String;)V
    .locals 5

    const/4 v4, 0x1

    invoke-static {p0, p1, v4}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0, p1}, Landroid/widget/Toast;->setText(Ljava/lang/CharSequence;)V

    const/4 v1, 0x0

    invoke-virtual {p0}, Lcom/android/mail/compose/ComposeActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0c004b

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v2

    invoke-virtual {v0, v4, v1, v2}, Landroid/widget/Toast;->setGravity(III)V

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    return-void
.end method

.method private showSendConfirmDialog(ILandroid/content/DialogInterface$OnClickListener;)V
    .locals 2

    iget-object v0, p0, Lcom/android/mail/compose/ComposeActivity;->mSendConfirmDialog:Landroid/app/AlertDialog;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/mail/compose/ComposeActivity;->mSendConfirmDialog:Landroid/app/AlertDialog;

    invoke-virtual {v0}, Landroid/app/AlertDialog;->dismiss()V

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/android/mail/compose/ComposeActivity;->mSendConfirmDialog:Landroid/app/AlertDialog;

    :cond_0
    new-instance v0, Landroid/app/AlertDialog$Builder;

    invoke-direct {v0, p0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    invoke-virtual {v0, p1}, Landroid/app/AlertDialog$Builder;->setMessage(I)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    const v1, 0x7f090043

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    const v1, 0x1010355

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setIconAttribute(I)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    const v1, 0x7f09004a

    invoke-virtual {v0, v1, p2}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    const v1, 0x7f0900e1

    invoke-virtual {v0, v1, p0}, Landroid/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->show()Landroid/app/AlertDialog;

    move-result-object v0

    iput-object v0, p0, Lcom/android/mail/compose/ComposeActivity;->mSendConfirmDialog:Landroid/app/AlertDialog;

    return-void
.end method

.method private showWaitFragment(Lcom/android/mail/providers/Account;)V
    .locals 3

    invoke-direct {p0}, Lcom/android/mail/compose/ComposeActivity;->getWaitFragment()Lcom/android/mail/ui/WaitFragment;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {v0, p1}, Lcom/android/mail/ui/WaitFragment;->updateAccount(Lcom/android/mail/providers/Account;)V

    :goto_0
    return-void

    :cond_0
    const v0, 0x7f08003b

    invoke-virtual {p0, v0}, Lcom/android/mail/compose/ComposeActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    const/4 v0, 0x1

    invoke-static {p1, v0}, Lcom/android/mail/ui/WaitFragment;->newInstance(Lcom/android/mail/providers/Account;Z)Lcom/android/mail/ui/WaitFragment;

    move-result-object v0

    const/16 v1, 0x1001

    const-string v2, "wait-fragment"

    invoke-direct {p0, v0, v1, v2}, Lcom/android/mail/compose/ComposeActivity;->replaceFragment(Landroid/app/Fragment;ILjava/lang/String;)I

    goto :goto_0
.end method

.method private updateHideOrShowCcBcc()V
    .locals 3

    iget-object v0, p0, Lcom/android/mail/compose/ComposeActivity;->mCcBccView:Lcom/android/mail/compose/CcBccView;

    invoke-virtual {v0}, Lcom/android/mail/compose/CcBccView;->isCcVisible()Z

    move-result v0

    iget-object v1, p0, Lcom/android/mail/compose/ComposeActivity;->mCcBccView:Lcom/android/mail/compose/CcBccView;

    invoke-virtual {v1}, Lcom/android/mail/compose/CcBccView;->isBccVisible()Z

    move-result v1

    iget-object v2, p0, Lcom/android/mail/compose/ComposeActivity;->mCcBccButton:Landroid/widget/Button;

    if-eqz v2, :cond_1

    if-eqz v0, :cond_0

    if-nez v1, :cond_3

    :cond_0
    iget-object v1, p0, Lcom/android/mail/compose/ComposeActivity;->mCcBccButton:Landroid/widget/Button;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Landroid/widget/Button;->setVisibility(I)V

    iget-object v1, p0, Lcom/android/mail/compose/ComposeActivity;->mCcBccButton:Landroid/widget/Button;

    if-nez v0, :cond_2

    const v0, 0x7f09006d

    :goto_0
    invoke-virtual {p0, v0}, Lcom/android/mail/compose/ComposeActivity;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    :cond_1
    :goto_1
    return-void

    :cond_2
    const v0, 0x7f09006e

    goto :goto_0

    :cond_3
    iget-object v0, p0, Lcom/android/mail/compose/ComposeActivity;->mCcBccButton:Landroid/widget/Button;

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setVisibility(I)V

    goto :goto_1
.end method

.method private updateHideOrShowQuotedText(Z)V
    .locals 2
    .param p1    # Z

    iget-object v0, p0, Lcom/android/mail/compose/ComposeActivity;->mQuotedTextView:Lcom/android/mail/compose/QuotedTextView;

    invoke-virtual {v0, p1}, Lcom/android/mail/compose/QuotedTextView;->updateCheckedState(Z)V

    iget-object v1, p0, Lcom/android/mail/compose/ComposeActivity;->mQuotedTextView:Lcom/android/mail/compose/QuotedTextView;

    iget-object v0, p0, Lcom/android/mail/compose/ComposeActivity;->mAttachmentsView:Lcom/android/mail/compose/AttachmentsView;

    invoke-virtual {v0}, Lcom/android/mail/compose/AttachmentsView;->getAttachments()Ljava/util/ArrayList;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-lez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-virtual {v1, v0}, Lcom/android/mail/compose/QuotedTextView;->setUpperDividerVisible(Z)V

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method addAddressesToList(Ljava/util/Collection;Lcom/android/ex/chips/RecipientEditTextView;)V
    .locals 3
    .param p2    # Lcom/android/ex/chips/RecipientEditTextView;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Collection",
            "<",
            "Ljava/lang/String;",
            ">;",
            "Lcom/android/ex/chips/RecipientEditTextView;",
            ")V"
        }
    .end annotation

    invoke-interface {p1}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-direct {p0, v0, p2}, Lcom/android/mail/compose/ComposeActivity;->addAddressToList(Ljava/lang/String;Lcom/android/ex/chips/RecipientEditTextView;)V

    goto :goto_0

    :cond_0
    return-void
.end method

.method public addAttachmentAndUpdateView(Landroid/content/Intent;)V
    .locals 1
    .param p1    # Landroid/content/Intent;

    if-eqz p1, :cond_0

    invoke-virtual {p1}, Landroid/content/Intent;->getData()Landroid/net/Uri;

    move-result-object v0

    :goto_0
    invoke-virtual {p0, v0}, Lcom/android/mail/compose/ComposeActivity;->addAttachmentAndUpdateView(Landroid/net/Uri;)V

    return-void

    :cond_0
    const/4 v0, 0x0

    check-cast v0, Landroid/net/Uri;

    goto :goto_0
.end method

.method public addAttachmentAndUpdateView(Landroid/net/Uri;)V
    .locals 7

    const/4 v6, 0x0

    if-nez p1, :cond_0

    :goto_0
    return-void

    :cond_0
    :try_start_0
    iget-object v0, p0, Lcom/android/mail/compose/ComposeActivity;->mAttachmentsView:Lcom/android/mail/compose/AttachmentsView;

    invoke-virtual {v0, p1}, Lcom/android/mail/compose/AttachmentsView;->generateLocalAttachment(Landroid/net/Uri;)Lcom/android/mail/providers/Attachment;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/android/mail/compose/ComposeActivity;->addAttachmentAndUpdateView(Lcom/android/mail/providers/Attachment;)V
    :try_end_0
    .catch Lcom/android/mail/compose/AttachmentsView$AttachmentFailureException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v0

    sget-object v1, Lcom/android/mail/compose/ComposeActivity;->LOG_TAG:Ljava/lang/String;

    const-string v2, "Error adding attachment"

    new-array v3, v6, [Ljava/lang/Object;

    invoke-static {v1, v0, v2, v3}, Lcom/android/mail/utils/LogUtils;->e(Ljava/lang/String;Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)I

    invoke-virtual {p0}, Lcom/android/mail/compose/ComposeActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v0}, Lcom/android/mail/compose/AttachmentsView$AttachmentFailureException;->getErrorRes()I

    move-result v0

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    invoke-virtual {p0}, Lcom/android/mail/compose/ComposeActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v3

    iget-object v4, p0, Lcom/android/mail/compose/ComposeActivity;->mAccount:Lcom/android/mail/providers/Account;

    iget-object v4, v4, Lcom/android/mail/providers/Account;->settings:Lcom/android/mail/providers/Settings;

    invoke-virtual {v4}, Lcom/android/mail/providers/Settings;->getMaxAttachmentSize()I

    move-result v4

    int-to-long v4, v4

    invoke-static {v3, v4, v5}, Lcom/android/mail/utils/AttachmentUtils;->convertToHumanReadableSize(Landroid/content/Context;J)Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v6

    invoke-virtual {v1, v0, v2}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/android/mail/compose/ComposeActivity;->showErrorToast(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public addAttachmentAndUpdateView(Lcom/android/mail/providers/Attachment;)V
    .locals 4

    :try_start_0
    iget-object v0, p0, Lcom/android/mail/compose/ComposeActivity;->mAttachmentsView:Lcom/android/mail/compose/AttachmentsView;

    iget-object v1, p0, Lcom/android/mail/compose/ComposeActivity;->mAccount:Lcom/android/mail/providers/Account;

    invoke-virtual {v0, v1, p1}, Lcom/android/mail/compose/AttachmentsView;->addAttachment(Lcom/android/mail/providers/Account;Lcom/android/mail/providers/Attachment;)J

    move-result-wide v0

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-lez v0, :cond_0

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/android/mail/compose/ComposeActivity;->mAttachmentsChanged:Z

    invoke-virtual {p0}, Lcom/android/mail/compose/ComposeActivity;->updateSaveUi()V
    :try_end_0
    .catch Lcom/android/mail/compose/AttachmentsView$AttachmentFailureException; {:try_start_0 .. :try_end_0} :catch_0

    :cond_0
    :goto_0
    return-void

    :catch_0
    move-exception v0

    sget-object v1, Lcom/android/mail/compose/ComposeActivity;->LOG_TAG:Ljava/lang/String;

    const-string v2, "Error adding attachment"

    const/4 v3, 0x0

    new-array v3, v3, [Ljava/lang/Object;

    invoke-static {v1, v0, v2, v3}, Lcom/android/mail/utils/LogUtils;->e(Ljava/lang/String;Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)I

    invoke-virtual {v0}, Lcom/android/mail/compose/AttachmentsView$AttachmentFailureException;->getErrorRes()I

    move-result v0

    invoke-direct {p0, v0}, Lcom/android/mail/compose/ComposeActivity;->showAttachmentTooBigToast(I)V

    goto :goto_0
.end method

.method public addAttachments(Ljava/util/List;)J
    .locals 9
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/android/mail/providers/Attachment;",
            ">;)J"
        }
    .end annotation

    const-wide/16 v2, 0x0

    const/4 v1, 0x0

    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :goto_0
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/mail/providers/Attachment;

    :try_start_0
    iget-object v5, p0, Lcom/android/mail/compose/ComposeActivity;->mAttachmentsView:Lcom/android/mail/compose/AttachmentsView;

    iget-object v6, p0, Lcom/android/mail/compose/ComposeActivity;->mAccount:Lcom/android/mail/providers/Account;

    invoke-virtual {v5, v6, v0}, Lcom/android/mail/compose/AttachmentsView;->addAttachment(Lcom/android/mail/providers/Account;Lcom/android/mail/providers/Attachment;)J
    :try_end_0
    .catch Lcom/android/mail/compose/AttachmentsView$AttachmentFailureException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-wide v5

    add-long/2addr v2, v5

    move-object v0, v1

    move-wide v7, v2

    move-wide v1, v7

    :goto_1
    move-wide v7, v1

    move-wide v2, v7

    move-object v1, v0

    goto :goto_0

    :catch_0
    move-exception v0

    move-wide v1, v2

    goto :goto_1

    :cond_0
    if-eqz v1, :cond_1

    sget-object v0, Lcom/android/mail/compose/ComposeActivity;->LOG_TAG:Ljava/lang/String;

    const-string v4, "Error adding attachment"

    const/4 v5, 0x0

    new-array v5, v5, [Ljava/lang/Object;

    invoke-static {v0, v1, v4, v5}, Lcom/android/mail/utils/LogUtils;->e(Ljava/lang/String;Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)I

    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v0

    const/4 v4, 0x1

    if-le v0, v4, :cond_2

    const v0, 0x7f09003e

    invoke-direct {p0, v0}, Lcom/android/mail/compose/ComposeActivity;->showAttachmentTooBigToast(I)V

    :cond_1
    :goto_2
    return-wide v2

    :cond_2
    invoke-virtual {v1}, Lcom/android/mail/compose/AttachmentsView$AttachmentFailureException;->getErrorRes()I

    move-result v0

    invoke-direct {p0, v0}, Lcom/android/mail/compose/ComposeActivity;->showAttachmentTooBigToast(I)V

    goto :goto_2
.end method

.method protected addCcAddressesToList(Ljava/util/List;Ljava/util/List;Lcom/android/ex/chips/RecipientEditTextView;)V
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<[",
            "Landroid/text/util/Rfc822Token;",
            ">;",
            "Ljava/util/List",
            "<[",
            "Landroid/text/util/Rfc822Token;",
            ">;",
            "Lcom/android/ex/chips/RecipientEditTextView;",
            ")V"
        }
    .end annotation

    const/4 v2, 0x0

    if-nez p2, :cond_1

    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_4

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Landroid/text/util/Rfc822Token;

    move v1, v2

    :goto_0
    array-length v4, v0

    if-ge v1, v4, :cond_0

    aget-object v4, v0, v1

    invoke-virtual {v4}, Landroid/text/util/Rfc822Token;->toString()Ljava/lang/String;

    move-result-object v4

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v5, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ", "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p3, v4}, Lcom/android/ex/chips/RecipientEditTextView;->append(Ljava/lang/CharSequence;)V

    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_1
    invoke-direct {p0, p2}, Lcom/android/mail/compose/ComposeActivity;->convertToHashSet(Ljava/util/List;)Ljava/util/HashSet;

    move-result-object v3

    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :cond_2
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_4

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Landroid/text/util/Rfc822Token;

    move v1, v2

    :goto_1
    array-length v5, v0

    if-ge v1, v5, :cond_2

    aget-object v5, v0, v1

    invoke-virtual {v5}, Landroid/text/util/Rfc822Token;->toString()Ljava/lang/String;

    move-result-object v5

    aget-object v6, v0, v1

    invoke-virtual {v6}, Landroid/text/util/Rfc822Token;->getAddress()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v3, v6}, Ljava/util/HashSet;->contains(Ljava/lang/Object;)Z

    move-result v6

    if-nez v6, :cond_3

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v6, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, ", "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {p3, v5}, Lcom/android/ex/chips/RecipientEditTextView;->append(Ljava/lang/CharSequence;)V

    :cond_3
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    :cond_4
    return-void
.end method

.method public afterTextChanged(Landroid/text/Editable;)V
    .locals 1
    .param p1    # Landroid/text/Editable;

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/android/mail/compose/ComposeActivity;->mTextChanged:Z

    invoke-virtual {p0}, Lcom/android/mail/compose/ComposeActivity;->updateSaveUi()V

    return-void
.end method

.method public appendToBody(Ljava/lang/CharSequence;Z)V
    .locals 2
    .param p1    # Ljava/lang/CharSequence;
    .param p2    # Z

    iget-object v1, p0, Lcom/android/mail/compose/ComposeActivity;->mBodyView:Landroid/widget/EditText;

    invoke-virtual {v1}, Landroid/widget/EditText;->getEditableText()Landroid/text/Editable;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-interface {v0}, Landroid/text/Editable;->length()I

    move-result v1

    if-lez v1, :cond_0

    invoke-interface {v0, p1}, Landroid/text/Editable;->append(Ljava/lang/CharSequence;)Landroid/text/Editable;

    :goto_0
    return-void

    :cond_0
    invoke-virtual {p0, p1, p2}, Lcom/android/mail/compose/ComposeActivity;->setBody(Ljava/lang/CharSequence;Z)V

    goto :goto_0
.end method

.method public beforeTextChanged(Ljava/lang/CharSequence;III)V
    .locals 0
    .param p1    # Ljava/lang/CharSequence;
    .param p2    # I
    .param p3    # I
    .param p4    # I

    return-void
.end method

.method public checkInvalidEmails([Ljava/lang/String;Ljava/util/List;)V
    .locals 5
    .param p1    # [Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "([",
            "Ljava/lang/String;",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    iget-object v4, p0, Lcom/android/mail/compose/ComposeActivity;->mValidator:Lcom/android/common/Rfc822Validator;

    if-nez v4, :cond_1

    :cond_0
    return-void

    :cond_1
    move-object v0, p1

    array-length v3, v0

    const/4 v2, 0x0

    :goto_0
    if-ge v2, v3, :cond_0

    aget-object v1, v0, v2

    iget-object v4, p0, Lcom/android/mail/compose/ComposeActivity;->mValidator:Lcom/android/common/Rfc822Validator;

    invoke-virtual {v4, v1}, Lcom/android/common/Rfc822Validator;->isValid(Ljava/lang/CharSequence;)Z

    move-result v4

    if-nez v4, :cond_2

    invoke-interface {p2, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :cond_2
    add-int/lit8 v2, v2, 0x1

    goto :goto_0
.end method

.method protected decodeEmailInUri(Ljava/lang/String;)Ljava/lang/String;
    .locals 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/UnsupportedEncodingException;
        }
    .end annotation

    const/4 v3, 0x2

    const/4 v4, 0x0

    const-string v0, "+"

    const-string v1, "%2B"

    invoke-virtual {p1, v0, v1}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v0

    :try_start_0
    const-string v1, "UTF-8"

    invoke-static {v0, v1}, Ljava/net/URLDecoder;->decode(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    :goto_0
    return-object v0

    :catch_0
    move-exception v0

    sget-object v1, Lcom/android/mail/compose/ComposeActivity;->LOG_TAG:Ljava/lang/String;

    invoke-static {v1, v3}, Lcom/android/mail/utils/LogUtils;->isLoggable(Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_0

    sget-object v1, Lcom/android/mail/compose/ComposeActivity;->LOG_TAG:Ljava/lang/String;

    const-string v2, "%s while decoding \'%s\'"

    new-array v3, v3, [Ljava/lang/Object;

    invoke-virtual {v0}, Ljava/lang/IllegalArgumentException;->getMessage()Ljava/lang/String;

    move-result-object v0

    aput-object v0, v3, v4

    const/4 v0, 0x1

    aput-object p1, v3, v0

    invoke-static {v1, v2, v3}, Lcom/android/mail/utils/LogUtils;->e(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)I

    :goto_1
    const/4 v0, 0x0

    goto :goto_0

    :cond_0
    sget-object v1, Lcom/android/mail/compose/ComposeActivity;->LOG_TAG:Ljava/lang/String;

    const-string v2, "Exception  while decoding mailto address"

    new-array v3, v4, [Ljava/lang/Object;

    invoke-static {v1, v0, v2, v3}, Lcom/android/mail/utils/LogUtils;->e(Ljava/lang/String;Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)I

    goto :goto_1
.end method

.method public enableSave(Z)V
    .locals 1
    .param p1    # Z

    iget-object v0, p0, Lcom/android/mail/compose/ComposeActivity;->mSave:Landroid/view/MenuItem;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/mail/compose/ComposeActivity;->mSave:Landroid/view/MenuItem;

    invoke-interface {v0, p1}, Landroid/view/MenuItem;->setEnabled(Z)Landroid/view/MenuItem;

    :cond_0
    return-void
.end method

.method public enableSend(Z)V
    .locals 1
    .param p1    # Z

    iget-object v0, p0, Lcom/android/mail/compose/ComposeActivity;->mSend:Landroid/view/MenuItem;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/mail/compose/ComposeActivity;->mSend:Landroid/view/MenuItem;

    invoke-interface {v0, p1}, Landroid/view/MenuItem;->setEnabled(Z)Landroid/view/MenuItem;

    :cond_0
    return-void
.end method

.method public focusBody()V
    .locals 4

    iget-object v2, p0, Lcom/android/mail/compose/ComposeActivity;->mBodyView:Landroid/widget/EditText;

    invoke-virtual {v2}, Landroid/widget/EditText;->requestFocus()Z

    iget-object v2, p0, Lcom/android/mail/compose/ComposeActivity;->mBodyView:Landroid/widget/EditText;

    invoke-virtual {v2}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v2

    invoke-interface {v2}, Landroid/text/Editable;->length()I

    move-result v0

    iget-object v2, p0, Lcom/android/mail/compose/ComposeActivity;->mSignature:Ljava/lang/String;

    iget-object v3, p0, Lcom/android/mail/compose/ComposeActivity;->mBodyView:Landroid/widget/EditText;

    invoke-virtual {v3}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p0, v2, v3}, Lcom/android/mail/compose/ComposeActivity;->getSignatureStartPosition(Ljava/lang/String;Ljava/lang/String;)I

    move-result v1

    const/4 v2, -0x1

    if-le v1, v2, :cond_1

    iget-object v2, p0, Lcom/android/mail/compose/ComposeActivity;->mBodyView:Landroid/widget/EditText;

    invoke-virtual {v2, v1}, Landroid/widget/EditText;->setSelection(I)V

    :cond_0
    :goto_0
    return-void

    :cond_1
    if-ltz v0, :cond_0

    iget-object v2, p0, Lcom/android/mail/compose/ComposeActivity;->mBodyView:Landroid/widget/EditText;

    invoke-virtual {v2, v0}, Landroid/widget/EditText;->setSelection(I)V

    goto :goto_0
.end method

.method public getActivityContext()Landroid/content/Context;
    .locals 0

    return-object p0
.end method

.method public getAddressesFromList(Lcom/android/ex/chips/RecipientEditTextView;)[Ljava/lang/String;
    .locals 5
    .param p1    # Lcom/android/ex/chips/RecipientEditTextView;

    if-nez p1, :cond_1

    const/4 v4, 0x0

    new-array v2, v4, [Ljava/lang/String;

    :cond_0
    return-object v2

    :cond_1
    invoke-virtual {p1}, Lcom/android/ex/chips/RecipientEditTextView;->getText()Landroid/text/Editable;

    move-result-object v4

    invoke-static {v4}, Landroid/text/util/Rfc822Tokenizer;->tokenize(Ljava/lang/CharSequence;)[Landroid/text/util/Rfc822Token;

    move-result-object v3

    array-length v0, v3

    new-array v2, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    :goto_0
    if-ge v1, v0, :cond_0

    aget-object v4, v3, v1

    invoke-virtual {v4}, Landroid/text/util/Rfc822Token;->toString()Ljava/lang/String;

    move-result-object v4

    aput-object v4, v2, v1

    add-int/lit8 v1, v1, 0x1

    goto :goto_0
.end method

.method protected getAttachments()Ljava/util/ArrayList;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/android/mail/providers/Attachment;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/android/mail/compose/ComposeActivity;->mAttachmentsView:Lcom/android/mail/compose/AttachmentsView;

    invoke-virtual {v0}, Lcom/android/mail/compose/AttachmentsView;->getAttachments()Ljava/util/ArrayList;

    move-result-object v0

    return-object v0
.end method

.method public getBccAddresses()[Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/android/mail/compose/ComposeActivity;->mBcc:Lcom/android/ex/chips/RecipientEditTextView;

    invoke-virtual {p0, v0}, Lcom/android/mail/compose/ComposeActivity;->getAddressesFromList(Lcom/android/ex/chips/RecipientEditTextView;)[Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method protected getBody()Landroid/widget/TextView;
    .locals 1

    iget-object v0, p0, Lcom/android/mail/compose/ComposeActivity;->mBodyView:Landroid/widget/EditText;

    return-object v0
.end method

.method public getCcAddresses()[Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/android/mail/compose/ComposeActivity;->mCc:Lcom/android/ex/chips/RecipientEditTextView;

    invoke-virtual {p0, v0}, Lcom/android/mail/compose/ComposeActivity;->getAddressesFromList(Lcom/android/ex/chips/RecipientEditTextView;)[Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getFromAccount()Lcom/android/mail/providers/Account;
    .locals 1

    iget-object v0, p0, Lcom/android/mail/compose/ComposeActivity;->mReplyFromAccount:Lcom/android/mail/providers/ReplyFromAccount;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/mail/compose/ComposeActivity;->mReplyFromAccount:Lcom/android/mail/providers/ReplyFromAccount;

    iget-object v0, v0, Lcom/android/mail/providers/ReplyFromAccount;->account:Lcom/android/mail/providers/Account;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/mail/compose/ComposeActivity;->mReplyFromAccount:Lcom/android/mail/providers/ReplyFromAccount;

    iget-object v0, v0, Lcom/android/mail/providers/ReplyFromAccount;->account:Lcom/android/mail/providers/Account;

    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/android/mail/compose/ComposeActivity;->mAccount:Lcom/android/mail/providers/Account;

    goto :goto_0
.end method

.method protected getMatchingRecipient(Lcom/android/mail/providers/Account;Ljava/util/List;)Lcom/android/mail/providers/ReplyFromAccount;
    .locals 10
    .param p1    # Lcom/android/mail/providers/Account;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/android/mail/providers/Account;",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;)",
            "Lcom/android/mail/providers/ReplyFromAccount;"
        }
    .end annotation

    const/4 v6, 0x0

    new-instance v7, Ljava/util/HashSet;

    invoke-direct {v7}, Ljava/util/HashSet;-><init>()V

    invoke-interface {p2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :cond_0
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v9

    if-eqz v9, :cond_1

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Landroid/text/util/Rfc822Tokenizer;->tokenize(Ljava/lang/CharSequence;)[Landroid/text/util/Rfc822Token;

    move-result-object v8

    const/4 v3, 0x0

    :goto_0
    array-length v9, v8

    if-ge v3, v9, :cond_0

    aget-object v9, v8, v3

    invoke-virtual {v9}, Landroid/text/util/Rfc822Token;->getAddress()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v7, v9}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    :cond_1
    const/4 v5, 0x0

    invoke-virtual {p1}, Lcom/android/mail/providers/Account;->getReplyFroms()Ljava/util/List;

    move-result-object v1

    if-eqz v1, :cond_3

    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :cond_2
    :goto_1
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v9

    if-eqz v9, :cond_3

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/android/mail/providers/ReplyFromAccount;

    iget-object v9, v2, Lcom/android/mail/providers/ReplyFromAccount;->address:Ljava/lang/String;

    invoke-virtual {v7, v9}, Ljava/util/HashSet;->contains(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_2

    move-object v6, v2

    add-int/lit8 v5, v5, 0x1

    goto :goto_1

    :cond_3
    const/4 v9, 0x1

    if-le v5, v9, :cond_4

    invoke-direct {p0, p1}, Lcom/android/mail/compose/ComposeActivity;->getDefaultReplyFromAccount(Lcom/android/mail/providers/Account;)Lcom/android/mail/providers/ReplyFromAccount;

    move-result-object v6

    :cond_4
    return-object v6
.end method

.method public getReplyFromAccount(Lcom/android/mail/providers/Account;Lcom/android/mail/providers/Message;)Lcom/android/mail/providers/ReplyFromAccount;
    .locals 2
    .param p1    # Lcom/android/mail/providers/Account;
    .param p2    # Lcom/android/mail/providers/Message;

    iget-object v1, p0, Lcom/android/mail/compose/ComposeActivity;->mCachedSettings:Lcom/android/mail/providers/Settings;

    iget-boolean v1, v1, Lcom/android/mail/providers/Settings;->forceReplyFromDefault:Z

    if-eqz v1, :cond_0

    invoke-direct {p0, p1}, Lcom/android/mail/compose/ComposeActivity;->getDefaultReplyFromAccount(Lcom/android/mail/providers/Account;)Lcom/android/mail/providers/ReplyFromAccount;

    move-result-object v1

    :goto_0
    return-object v1

    :cond_0
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    invoke-virtual {p2}, Lcom/android/mail/providers/Message;->getToAddresses()[Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    invoke-virtual {p2}, Lcom/android/mail/providers/Message;->getCcAddresses()[Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    invoke-virtual {p0, p1, v0}, Lcom/android/mail/compose/ComposeActivity;->getMatchingRecipient(Lcom/android/mail/providers/Account;Ljava/util/List;)Lcom/android/mail/providers/ReplyFromAccount;

    move-result-object v1

    goto :goto_0
.end method

.method protected getSignatureStartPosition(Ljava/lang/String;Ljava/lang/String;)I
    .locals 7
    .param p1    # Ljava/lang/String;
    .param p2    # Ljava/lang/String;

    const/4 v4, -0x1

    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v6

    if-nez v6, :cond_0

    invoke-static {p2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v6

    if-eqz v6, :cond_1

    :cond_0
    move v5, v4

    :goto_0
    return v5

    :cond_1
    invoke-virtual {p2}, Ljava/lang/String;->length()I

    move-result v0

    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v3

    invoke-direct {p0, p1}, Lcom/android/mail/compose/ComposeActivity;->convertToPrintableSignature(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v1

    if-lt v0, v1, :cond_3

    sub-int v6, v0, v1

    invoke-virtual {p2, v6}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v6, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_3

    sub-int v4, v0, v1

    :cond_2
    :goto_1
    move v5, v4

    goto :goto_0

    :cond_3
    if-lt v0, v3, :cond_2

    sub-int v6, v0, v3

    invoke-virtual {p2, v6}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v6, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_2

    sub-int v4, v0, v3

    goto :goto_1
.end method

.method public getToAddresses()[Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/android/mail/compose/ComposeActivity;->mTo:Lcom/android/ex/chips/RecipientEditTextView;

    invoke-virtual {p0, v0}, Lcom/android/mail/compose/ComposeActivity;->getAddressesFromList(Lcom/android/ex/chips/RecipientEditTextView;)[Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method protected initAttachments(Lcom/android/mail/providers/Message;)V
    .locals 1
    .param p1    # Lcom/android/mail/providers/Message;

    invoke-virtual {p1}, Lcom/android/mail/providers/Message;->getAttachments()Ljava/util/List;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/android/mail/compose/ComposeActivity;->addAttachments(Ljava/util/List;)J

    return-void
.end method

.method public initFromExtras(Landroid/content/Intent;)V
    .locals 8

    const/4 v7, 0x0

    const/4 v6, 0x1

    invoke-virtual {p1}, Landroid/content/Intent;->getData()Landroid/net/Uri;

    move-result-object v0

    if-eqz v0, :cond_0

    const-string v1, "mailto"

    invoke-virtual {v0}, Landroid/net/Uri;->getScheme()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_6

    invoke-virtual {v0}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/android/mail/compose/ComposeActivity;->initFromMailTo(Ljava/lang/String;)V

    :cond_0
    :goto_0
    const-string v0, "android.intent.extra.EMAIL"

    invoke-virtual {p1, v0}, Landroid/content/Intent;->getStringArrayExtra(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_1

    invoke-static {v0}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/android/mail/compose/ComposeActivity;->addToAddresses(Ljava/util/Collection;)V

    :cond_1
    const-string v0, "android.intent.extra.CC"

    invoke-virtual {p1, v0}, Landroid/content/Intent;->getStringArrayExtra(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_2

    invoke-static {v0}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    invoke-direct {p0, v0, v7}, Lcom/android/mail/compose/ComposeActivity;->addCcAddresses(Ljava/util/Collection;Ljava/util/Collection;)V

    :cond_2
    const-string v0, "android.intent.extra.BCC"

    invoke-virtual {p1, v0}, Landroid/content/Intent;->getStringArrayExtra(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_3

    invoke-static {v0}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/android/mail/compose/ComposeActivity;->addBccAddresses(Ljava/util/Collection;)V

    :cond_3
    const-string v0, "android.intent.extra.SUBJECT"

    invoke-virtual {p1, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_4

    iget-object v1, p0, Lcom/android/mail/compose/ComposeActivity;->mSubject:Landroid/widget/TextView;

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    :cond_4
    sget-object v1, Lcom/android/mail/compose/ComposeActivity;->ALL_EXTRAS:[Ljava/lang/String;

    array-length v2, v1

    const/4 v0, 0x0

    :goto_1
    if-ge v0, v2, :cond_b

    aget-object v3, v1, v0

    invoke-virtual {p1, v3}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_5

    invoke-virtual {p1, v3}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    const-string v5, "to"

    invoke-virtual {v5, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_7

    const-string v3, ","

    invoke-static {v4, v3}, Landroid/text/TextUtils;->split(Ljava/lang/String;Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v3

    invoke-direct {p0, v3}, Lcom/android/mail/compose/ComposeActivity;->addToAddresses(Ljava/util/Collection;)V

    :cond_5
    :goto_2
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    :cond_6
    iget-object v1, p0, Lcom/android/mail/compose/ComposeActivity;->mAccount:Lcom/android/mail/providers/Account;

    iget-object v1, v1, Lcom/android/mail/providers/Account;->composeIntentUri:Landroid/net/Uri;

    invoke-virtual {v1, v0}, Landroid/net/Uri;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    invoke-virtual {v0}, Landroid/net/Uri;->getSchemeSpecificPart()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v1, p0, Lcom/android/mail/compose/ComposeActivity;->mTo:Lcom/android/ex/chips/RecipientEditTextView;

    const-string v2, ""

    invoke-virtual {v1, v2}, Lcom/android/ex/chips/RecipientEditTextView;->setText(Ljava/lang/CharSequence;)V

    const-string v1, ","

    invoke-static {v0, v1}, Landroid/text/TextUtils;->split(Ljava/lang/String;Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/android/mail/compose/ComposeActivity;->addToAddresses(Ljava/util/Collection;)V

    goto/16 :goto_0

    :cond_7
    const-string v5, "cc"

    invoke-virtual {v5, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_8

    const-string v3, ","

    invoke-static {v4, v3}, Landroid/text/TextUtils;->split(Ljava/lang/String;Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v3

    invoke-direct {p0, v3, v7}, Lcom/android/mail/compose/ComposeActivity;->addCcAddresses(Ljava/util/Collection;Ljava/util/Collection;)V

    goto :goto_2

    :cond_8
    const-string v5, "bcc"

    invoke-virtual {v5, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_9

    const-string v3, ","

    invoke-static {v4, v3}, Landroid/text/TextUtils;->split(Ljava/lang/String;Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v3

    invoke-direct {p0, v3}, Lcom/android/mail/compose/ComposeActivity;->addBccAddresses(Ljava/util/Collection;)V

    goto :goto_2

    :cond_9
    const-string v5, "subject"

    invoke-virtual {v5, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_a

    iget-object v3, p0, Lcom/android/mail/compose/ComposeActivity;->mSubject:Landroid/widget/TextView;

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_2

    :cond_a
    const-string v5, "body"

    invoke-virtual {v5, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_5

    invoke-virtual {p0, v4, v6}, Lcom/android/mail/compose/ComposeActivity;->setBody(Ljava/lang/CharSequence;Z)V

    goto :goto_2

    :cond_b
    invoke-virtual {p1}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v0

    if-eqz v0, :cond_c

    const-string v1, "android.intent.extra.TEXT"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getCharSequence(Ljava/lang/String;)Ljava/lang/CharSequence;

    move-result-object v0

    if-eqz v0, :cond_c

    invoke-virtual {p0, v0, v6}, Lcom/android/mail/compose/ComposeActivity;->setBody(Ljava/lang/CharSequence;Z)V

    :cond_c
    return-void
.end method

.method public initFromMailTo(Ljava/lang/String;)V
    .locals 9

    const/4 v8, 0x2

    const/4 v7, 0x1

    const/4 v6, 0x0

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "foo://"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    const-string v0, "?"

    invoke-virtual {p1, v0}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v0

    const-string v2, "mailto"

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v2

    add-int/lit8 v2, v2, 0x1

    const/4 v3, -0x1

    if-ne v0, v3, :cond_3

    :try_start_0
    invoke-virtual {p1, v2}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/android/mail/compose/ComposeActivity;->decodeEmailInUri(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    :goto_0
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_0

    const-string v2, ","

    invoke-static {v0, v2}, Landroid/text/TextUtils;->split(Ljava/lang/String;Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/android/mail/compose/ComposeActivity;->addToAddresses(Ljava/util/Collection;)V
    :try_end_0
    .catch Ljava/io/UnsupportedEncodingException; {:try_start_0 .. :try_end_0} :catch_0

    :cond_0
    :goto_1
    const-string v0, "cc"

    invoke-virtual {v1, v0}, Landroid/net/Uri;->getQueryParameters(Ljava/lang/String;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v2

    new-array v2, v2, [Ljava/lang/String;

    invoke-interface {v0, v2}, Ljava/util/List;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v0

    invoke-static {v0}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    const/4 v2, 0x0

    invoke-direct {p0, v0, v2}, Lcom/android/mail/compose/ComposeActivity;->addCcAddresses(Ljava/util/Collection;Ljava/util/Collection;)V

    const-string v0, "to"

    invoke-virtual {v1, v0}, Landroid/net/Uri;->getQueryParameters(Ljava/lang/String;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v2

    new-array v2, v2, [Ljava/lang/String;

    invoke-interface {v0, v2}, Ljava/util/List;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v0

    invoke-static {v0}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/android/mail/compose/ComposeActivity;->addToAddresses(Ljava/util/Collection;)V

    const-string v0, "bcc"

    invoke-virtual {v1, v0}, Landroid/net/Uri;->getQueryParameters(Ljava/lang/String;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v2

    new-array v2, v2, [Ljava/lang/String;

    invoke-interface {v0, v2}, Ljava/util/List;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v0

    invoke-static {v0}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/android/mail/compose/ComposeActivity;->addBccAddresses(Ljava/util/Collection;)V

    const-string v0, "subject"

    invoke-virtual {v1, v0}, Landroid/net/Uri;->getQueryParameters(Ljava/lang/String;)Ljava/util/List;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v0

    if-lez v0, :cond_1

    :try_start_1
    iget-object v3, p0, Lcom/android/mail/compose/ComposeActivity;->mSubject:Landroid/widget/TextView;

    const/4 v0, 0x0

    invoke-interface {v2, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    const-string v4, "UTF-8"

    invoke-static {v0, v4}, Ljava/net/URLDecoder;->decode(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v3, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V
    :try_end_1
    .catch Ljava/io/UnsupportedEncodingException; {:try_start_1 .. :try_end_1} :catch_1

    :cond_1
    :goto_2
    const-string v0, "body"

    invoke-virtual {v1, v0}, Landroid/net/Uri;->getQueryParameters(Ljava/lang/String;)Ljava/util/List;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v0

    if-lez v0, :cond_2

    const/4 v0, 0x0

    :try_start_2
    invoke-interface {v1, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    const-string v2, "UTF-8"

    invoke-static {v0, v2}, Ljava/net/URLDecoder;->decode(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    const/4 v2, 0x1

    invoke-virtual {p0, v0, v2}, Lcom/android/mail/compose/ComposeActivity;->setBody(Ljava/lang/CharSequence;Z)V
    :try_end_2
    .catch Ljava/io/UnsupportedEncodingException; {:try_start_2 .. :try_end_2} :catch_2

    :cond_2
    :goto_3
    return-void

    :cond_3
    :try_start_3
    invoke-virtual {p1, v2, v0}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/android/mail/compose/ComposeActivity;->decodeEmailInUri(Ljava/lang/String;)Ljava/lang/String;
    :try_end_3
    .catch Ljava/io/UnsupportedEncodingException; {:try_start_3 .. :try_end_3} :catch_0

    move-result-object v0

    goto/16 :goto_0

    :catch_0
    move-exception v0

    sget-object v2, Lcom/android/mail/compose/ComposeActivity;->LOG_TAG:Ljava/lang/String;

    invoke-static {v2, v8}, Lcom/android/mail/utils/LogUtils;->isLoggable(Ljava/lang/String;I)Z

    move-result v2

    if-eqz v2, :cond_4

    sget-object v2, Lcom/android/mail/compose/ComposeActivity;->LOG_TAG:Ljava/lang/String;

    const-string v3, "%s while decoding \'%s\'"

    new-array v4, v8, [Ljava/lang/Object;

    invoke-virtual {v0}, Ljava/io/UnsupportedEncodingException;->getMessage()Ljava/lang/String;

    move-result-object v0

    aput-object v0, v4, v6

    aput-object p1, v4, v7

    invoke-static {v2, v3, v4}, Lcom/android/mail/utils/LogUtils;->e(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)I

    goto/16 :goto_1

    :cond_4
    sget-object v2, Lcom/android/mail/compose/ComposeActivity;->LOG_TAG:Ljava/lang/String;

    const-string v3, "Exception  while decoding mailto address"

    new-array v4, v6, [Ljava/lang/Object;

    invoke-static {v2, v0, v3, v4}, Lcom/android/mail/utils/LogUtils;->e(Ljava/lang/String;Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)I

    goto/16 :goto_1

    :catch_1
    move-exception v0

    sget-object v3, Lcom/android/mail/compose/ComposeActivity;->LOG_TAG:Ljava/lang/String;

    const-string v4, "%s while decoding subject \'%s\'"

    new-array v5, v8, [Ljava/lang/Object;

    invoke-virtual {v0}, Ljava/io/UnsupportedEncodingException;->getMessage()Ljava/lang/String;

    move-result-object v0

    aput-object v0, v5, v6

    aput-object v2, v5, v7

    invoke-static {v3, v4, v5}, Lcom/android/mail/utils/LogUtils;->e(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)I

    goto :goto_2

    :catch_2
    move-exception v0

    sget-object v2, Lcom/android/mail/compose/ComposeActivity;->LOG_TAG:Ljava/lang/String;

    const-string v3, "%s while decoding body \'%s\'"

    new-array v4, v8, [Ljava/lang/Object;

    invoke-virtual {v0}, Ljava/io/UnsupportedEncodingException;->getMessage()Ljava/lang/String;

    move-result-object v0

    aput-object v0, v4, v6

    aput-object v1, v4, v7

    invoke-static {v2, v3, v4}, Lcom/android/mail/utils/LogUtils;->e(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)I

    goto :goto_3
.end method

.method initRecipientsFromRefMessage(Lcom/android/mail/providers/Message;I)V
    .locals 1
    .param p1    # Lcom/android/mail/providers/Message;
    .param p2    # I

    const/4 v0, 0x2

    if-ne p2, v0, :cond_0

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/android/mail/compose/ComposeActivity;->mAccount:Lcom/android/mail/providers/Account;

    iget-object v0, v0, Lcom/android/mail/providers/Account;->name:Ljava/lang/String;

    invoke-virtual {p0, v0, p1, p2}, Lcom/android/mail/compose/ComposeActivity;->initReplyRecipients(Ljava/lang/String;Lcom/android/mail/providers/Message;I)V

    goto :goto_0
.end method

.method initReplyRecipients(Ljava/lang/String;Lcom/android/mail/providers/Message;I)V
    .locals 6
    .param p1    # Ljava/lang/String;
    .param p2    # Lcom/android/mail/providers/Message;
    .param p3    # I

    invoke-static {p1}, Lcom/android/mail/providers/Address;->getEmailAddress(Ljava/lang/String;)Lcom/android/mail/providers/Address;

    move-result-object v5

    invoke-virtual {v5}, Lcom/android/mail/providers/Address;->getAddress()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2}, Lcom/android/mail/providers/Message;->getToAddresses()[Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p2}, Lcom/android/mail/providers/Message;->getReplyTo()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v5

    if-eqz v5, :cond_0

    invoke-virtual {p2}, Lcom/android/mail/providers/Message;->getFrom()Ljava/lang/String;

    move-result-object v2

    :cond_0
    if-nez p3, :cond_2

    invoke-virtual {p2}, Lcom/android/mail/providers/Message;->getFrom()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {p0, v0, v5, v2, v3}, Lcom/android/mail/compose/ComposeActivity;->initToRecipients(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)Ljava/util/Collection;

    move-result-object v4

    invoke-direct {p0, v4}, Lcom/android/mail/compose/ComposeActivity;->addToAddresses(Ljava/util/Collection;)V

    :cond_1
    :goto_0
    return-void

    :cond_2
    const/4 v5, 0x1

    if-ne p3, v5, :cond_1

    invoke-static {}, Lcom/google/common/collect/Sets;->newHashSet()Ljava/util/HashSet;

    move-result-object v1

    invoke-virtual {p2}, Lcom/android/mail/providers/Message;->getFrom()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {p0, v0, v5, v2, v3}, Lcom/android/mail/compose/ComposeActivity;->initToRecipients(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)Ljava/util/Collection;

    move-result-object v4

    invoke-direct {p0, v4}, Lcom/android/mail/compose/ComposeActivity;->addToAddresses(Ljava/util/Collection;)V

    invoke-direct {p0, v0, v1, v3}, Lcom/android/mail/compose/ComposeActivity;->addRecipients(Ljava/lang/String;Ljava/util/Set;[Ljava/lang/String;)V

    invoke-virtual {p2}, Lcom/android/mail/providers/Message;->getCcAddresses()[Ljava/lang/String;

    move-result-object v5

    invoke-direct {p0, v0, v1, v5}, Lcom/android/mail/compose/ComposeActivity;->addRecipients(Ljava/lang/String;Ljava/util/Set;[Ljava/lang/String;)V

    invoke-direct {p0, v1, v4}, Lcom/android/mail/compose/ComposeActivity;->addCcAddresses(Ljava/util/Collection;Ljava/util/Collection;)V

    goto :goto_0
.end method

.method protected initToRecipients(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)Ljava/util/Collection;
    .locals 8
    .param p1    # Ljava/lang/String;
    .param p2    # Ljava/lang/String;
    .param p3    # Ljava/lang/String;
    .param p4    # [Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "[",
            "Ljava/lang/String;",
            ")",
            "Ljava/util/Collection",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    const/4 v7, 0x0

    invoke-static {}, Lcom/google/common/collect/Sets;->newHashSet()Ljava/util/HashSet;

    move-result-object v4

    invoke-static {p3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v5

    if-nez v5, :cond_1

    invoke-virtual {p0, p3}, Lcom/android/mail/compose/ComposeActivity;->recipientMatchesThisAccount(Ljava/lang/String;)Z

    move-result v5

    if-nez v5, :cond_1

    invoke-interface {v4, p3}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    :cond_0
    :goto_0
    return-object v4

    :cond_1
    array-length v5, p4

    const/4 v6, 0x1

    if-ne v5, v6, :cond_2

    invoke-virtual {p0, p2}, Lcom/android/mail/compose/ComposeActivity;->recipientMatchesThisAccount(Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_2

    aget-object v5, p4, v7

    invoke-virtual {p0, v5}, Lcom/android/mail/compose/ComposeActivity;->recipientMatchesThisAccount(Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_2

    aget-object v5, p4, v7

    invoke-interface {v4, v5}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_2
    move-object v1, p4

    array-length v3, v1

    const/4 v2, 0x0

    :goto_1
    if-ge v2, v3, :cond_0

    aget-object v0, v1, v2

    invoke-virtual {p0, v0}, Lcom/android/mail/compose/ComposeActivity;->recipientMatchesThisAccount(Ljava/lang/String;)Z

    move-result v5

    if-nez v5, :cond_3

    invoke-interface {v4, v0}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    :cond_3
    add-int/lit8 v2, v2, 0x1

    goto :goto_1
.end method

.method public isBlank()Z
    .locals 2

    iget-object v0, p0, Lcom/android/mail/compose/ComposeActivity;->mSubject:Landroid/widget/TextView;

    invoke-virtual {v0}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v0

    invoke-interface {v0}, Ljava/lang/CharSequence;->length()I

    move-result v0

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/android/mail/compose/ComposeActivity;->mBodyView:Landroid/widget/EditText;

    invoke-virtual {v0}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-interface {v0}, Landroid/text/Editable;->length()I

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/mail/compose/ComposeActivity;->mSignature:Ljava/lang/String;

    iget-object v1, p0, Lcom/android/mail/compose/ComposeActivity;->mBodyView:Landroid/widget/EditText;

    invoke-virtual {v1}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lcom/android/mail/compose/ComposeActivity;->getSignatureStartPosition(Ljava/lang/String;Ljava/lang/String;)I

    move-result v0

    if-nez v0, :cond_1

    :cond_0
    iget-object v0, p0, Lcom/android/mail/compose/ComposeActivity;->mTo:Lcom/android/ex/chips/RecipientEditTextView;

    invoke-virtual {v0}, Lcom/android/ex/chips/RecipientEditTextView;->length()I

    move-result v0

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/android/mail/compose/ComposeActivity;->mCc:Lcom/android/ex/chips/RecipientEditTextView;

    invoke-virtual {v0}, Lcom/android/ex/chips/RecipientEditTextView;->length()I

    move-result v0

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/android/mail/compose/ComposeActivity;->mBcc:Lcom/android/ex/chips/RecipientEditTextView;

    invoke-virtual {v0}, Lcom/android/ex/chips/RecipientEditTextView;->length()I

    move-result v0

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/android/mail/compose/ComposeActivity;->mAttachmentsView:Lcom/android/mail/compose/AttachmentsView;

    invoke-virtual {v0}, Lcom/android/mail/compose/AttachmentsView;->getAttachments()Ljava/util/ArrayList;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-nez v0, :cond_1

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isBodyEmpty()Z
    .locals 1

    iget-object v0, p0, Lcom/android/mail/compose/ComposeActivity;->mQuotedTextView:Lcom/android/mail/compose/QuotedTextView;

    invoke-virtual {v0}, Lcom/android/mail/compose/QuotedTextView;->isTextIncluded()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isSubjectEmpty()Z
    .locals 1

    iget-object v0, p0, Lcom/android/mail/compose/ComposeActivity;->mSubject:Landroid/widget/TextView;

    invoke-virtual {v0}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->getTrimmedLength(Ljava/lang/CharSequence;)I

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public onAccountChanged()V
    .locals 6

    const/4 v5, 0x1

    iget-object v3, p0, Lcom/android/mail/compose/ComposeActivity;->mFromSpinner:Lcom/android/mail/compose/FromAddressSpinner;

    invoke-virtual {v3}, Lcom/android/mail/compose/FromAddressSpinner;->getCurrentAccount()Lcom/android/mail/providers/ReplyFromAccount;

    move-result-object v3

    iput-object v3, p0, Lcom/android/mail/compose/ComposeActivity;->mReplyFromAccount:Lcom/android/mail/providers/ReplyFromAccount;

    iget-object v3, p0, Lcom/android/mail/compose/ComposeActivity;->mAccount:Lcom/android/mail/providers/Account;

    iget-object v4, p0, Lcom/android/mail/compose/ComposeActivity;->mReplyFromAccount:Lcom/android/mail/providers/ReplyFromAccount;

    iget-object v4, v4, Lcom/android/mail/providers/ReplyFromAccount;->account:Lcom/android/mail/providers/Account;

    invoke-virtual {v3, v4}, Lcom/android/mail/providers/Account;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_2

    iget-object v3, p0, Lcom/android/mail/compose/ComposeActivity;->mBodyView:Landroid/widget/EditText;

    invoke-virtual {v3, p0}, Landroid/widget/EditText;->removeTextChangedListener(Landroid/text/TextWatcher;)V

    iget-object v1, p0, Lcom/android/mail/compose/ComposeActivity;->mSignature:Ljava/lang/String;

    invoke-virtual {p0}, Lcom/android/mail/compose/ComposeActivity;->getBody()Landroid/widget/TextView;

    move-result-object v3

    invoke-virtual {v3}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_0

    invoke-virtual {p0, v1, v0}, Lcom/android/mail/compose/ComposeActivity;->getSignatureStartPosition(Ljava/lang/String;Ljava/lang/String;)I

    move-result v2

    const/4 v3, -0x1

    if-le v2, v3, :cond_0

    iget-object v3, p0, Lcom/android/mail/compose/ComposeActivity;->mBodyView:Landroid/widget/EditText;

    const/4 v4, 0x0

    invoke-virtual {v0, v4, v2}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    :cond_0
    iget-object v3, p0, Lcom/android/mail/compose/ComposeActivity;->mReplyFromAccount:Lcom/android/mail/providers/ReplyFromAccount;

    iget-object v3, v3, Lcom/android/mail/providers/ReplyFromAccount;->account:Lcom/android/mail/providers/Account;

    invoke-virtual {p0, v3}, Lcom/android/mail/compose/ComposeActivity;->setAccount(Lcom/android/mail/providers/Account;)V

    iget-object v3, p0, Lcom/android/mail/compose/ComposeActivity;->mBodyView:Landroid/widget/EditText;

    invoke-virtual {v3, p0}, Landroid/widget/EditText;->addTextChangedListener(Landroid/text/TextWatcher;)V

    invoke-virtual {p0}, Lcom/android/mail/compose/ComposeActivity;->isBlank()Z

    move-result v3

    if-nez v3, :cond_1

    invoke-virtual {p0, v5}, Lcom/android/mail/compose/ComposeActivity;->enableSave(Z)V

    :cond_1
    iput-boolean v5, p0, Lcom/android/mail/compose/ComposeActivity;->mReplyFromChanged:Z

    invoke-direct {p0}, Lcom/android/mail/compose/ComposeActivity;->initRecipients()V

    :cond_2
    return-void
.end method

.method protected final onActivityResult(IILandroid/content/Intent;)V
    .locals 4
    .param p1    # I
    .param p2    # I
    .param p3    # Landroid/content/Intent;

    const/4 v3, 0x0

    const/4 v2, 0x1

    const/4 v1, -0x1

    if-ne p1, v2, :cond_1

    if-ne p2, v1, :cond_1

    invoke-virtual {p0, p3}, Lcom/android/mail/compose/ComposeActivity;->addAttachmentAndUpdateView(Landroid/content/Intent;)V

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/android/mail/compose/ComposeActivity;->mAddingAttachment:Z

    :cond_0
    :goto_0
    return-void

    :cond_1
    const/4 v0, 0x2

    if-ne p1, v0, :cond_0

    if-eq p2, v1, :cond_2

    invoke-virtual {p0}, Lcom/android/mail/compose/ComposeActivity;->finish()V

    goto :goto_0

    :cond_2
    invoke-virtual {p0}, Lcom/android/mail/compose/ComposeActivity;->getLoaderManager()Landroid/app/LoaderManager;

    move-result-object v0

    invoke-virtual {v0, v2, v3, p0}, Landroid/app/LoaderManager;->initLoader(ILandroid/os/Bundle;Landroid/app/LoaderManager$LoaderCallbacks;)Landroid/content/Loader;

    invoke-direct {p0, v3}, Lcom/android/mail/compose/ComposeActivity;->showWaitFragment(Lcom/android/mail/providers/Account;)V

    goto :goto_0
.end method

.method public onAttachmentAdded()V
    .locals 2

    iget-object v1, p0, Lcom/android/mail/compose/ComposeActivity;->mQuotedTextView:Lcom/android/mail/compose/QuotedTextView;

    iget-object v0, p0, Lcom/android/mail/compose/ComposeActivity;->mAttachmentsView:Lcom/android/mail/compose/AttachmentsView;

    invoke-virtual {v0}, Lcom/android/mail/compose/AttachmentsView;->getAttachments()Ljava/util/ArrayList;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-lez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-virtual {v1, v0}, Lcom/android/mail/compose/QuotedTextView;->setUpperDividerVisible(Z)V

    iget-object v0, p0, Lcom/android/mail/compose/ComposeActivity;->mAttachmentsView:Lcom/android/mail/compose/AttachmentsView;

    invoke-virtual {v0}, Lcom/android/mail/compose/AttachmentsView;->focusLastAttachment()V

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public onAttachmentDeleted()V
    .locals 3

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/android/mail/compose/ComposeActivity;->mAttachmentsChanged:Z

    iget-object v1, p0, Lcom/android/mail/compose/ComposeActivity;->mQuotedTextView:Lcom/android/mail/compose/QuotedTextView;

    iget-object v2, p0, Lcom/android/mail/compose/ComposeActivity;->mAttachmentsView:Lcom/android/mail/compose/AttachmentsView;

    invoke-virtual {v2}, Lcom/android/mail/compose/AttachmentsView;->getAttachments()Ljava/util/ArrayList;

    move-result-object v2

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v2

    if-lez v2, :cond_0

    :goto_0
    invoke-virtual {v1, v0}, Lcom/android/mail/compose/QuotedTextView;->setUpperDividerVisible(Z)V

    invoke-virtual {p0}, Lcom/android/mail/compose/ComposeActivity;->updateSaveUi()V

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public onBackPressed()V
    .locals 1

    invoke-direct {p0}, Lcom/android/mail/compose/ComposeActivity;->getWaitFragment()Lcom/android/mail/ui/WaitFragment;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/android/mail/compose/ComposeActivity;->finish()V

    :goto_0
    return-void

    :cond_0
    invoke-super {p0}, Landroid/app/Activity;->onBackPressed()V

    goto :goto_0
.end method

.method public onClick(Landroid/content/DialogInterface;I)V
    .locals 1
    .param p1    # Landroid/content/DialogInterface;
    .param p2    # I

    const/4 v0, 0x1

    packed-switch p2, :pswitch_data_0

    :goto_0
    return-void

    :pswitch_0
    invoke-direct {p0, v0}, Lcom/android/mail/compose/ComposeActivity;->doDiscardWithoutConfirmation(Z)V

    goto :goto_0

    :pswitch_1
    invoke-virtual {p0, v0}, Lcom/android/mail/compose/ComposeActivity;->enableSend(Z)V

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch -0x2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method public onClick(Landroid/view/View;)V
    .locals 1

    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    :goto_0
    return-void

    :sswitch_0
    invoke-direct {p0}, Lcom/android/mail/compose/ComposeActivity;->showCcBccViews()V

    goto :goto_0

    :sswitch_1
    const-string v0, "image/*"

    invoke-direct {p0, v0}, Lcom/android/mail/compose/ComposeActivity;->doAttach(Ljava/lang/String;)V

    goto :goto_0

    :sswitch_2
    const-string v0, "video/*"

    invoke-direct {p0, v0}, Lcom/android/mail/compose/ComposeActivity;->doAttach(Ljava/lang/String;)V

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        0x7f08003f -> :sswitch_0
        0x7f080043 -> :sswitch_1
        0x7f080112 -> :sswitch_2
    .end sparse-switch
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 1
    .param p1    # Landroid/os/Bundle;

    invoke-super {p0, p1}, Landroid/app/Activity;->onCreate(Landroid/os/Bundle;)V

    const v0, 0x7f040012

    invoke-virtual {p0, v0}, Lcom/android/mail/compose/ComposeActivity;->setContentView(I)V

    iput-object p1, p0, Lcom/android/mail/compose/ComposeActivity;->mSavedInstanceState:Landroid/os/Bundle;

    invoke-direct {p0}, Lcom/android/mail/compose/ComposeActivity;->checkValidAccounts()V

    return-void
.end method

.method public onCreateLoader(ILandroid/os/Bundle;)Landroid/content/Loader;
    .locals 7
    .param p1    # I
    .param p2    # Landroid/os/Bundle;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Landroid/os/Bundle;",
            ")",
            "Landroid/content/Loader",
            "<",
            "Landroid/database/Cursor;",
            ">;"
        }
    .end annotation

    const/4 v4, 0x0

    packed-switch p1, :pswitch_data_0

    move-object v0, v4

    :goto_0
    return-object v0

    :pswitch_0
    new-instance v0, Landroid/content/CursorLoader;

    iget-object v2, p0, Lcom/android/mail/compose/ComposeActivity;->mRefMessageUri:Landroid/net/Uri;

    sget-object v3, Lcom/android/mail/providers/UIProvider;->MESSAGE_PROJECTION:[Ljava/lang/String;

    move-object v1, p0

    move-object v5, v4

    move-object v6, v4

    invoke-direct/range {v0 .. v6}, Landroid/content/CursorLoader;-><init>(Landroid/content/Context;Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    :pswitch_1
    new-instance v0, Landroid/content/CursorLoader;

    invoke-static {}, Lcom/android/mail/providers/MailAppProvider;->getAccountsUri()Landroid/net/Uri;

    move-result-object v2

    sget-object v3, Lcom/android/mail/providers/UIProvider;->ACCOUNTS_PROJECTION:[Ljava/lang/String;

    move-object v1, p0

    move-object v5, v4

    move-object v6, v4

    invoke-direct/range {v0 .. v6}, Landroid/content/CursorLoader;-><init>(Landroid/content/Context;Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public onCreateOptionsMenu(Landroid/view/Menu;)Z
    .locals 6

    const/4 v1, 0x0

    const/4 v2, 0x1

    invoke-super {p0, p1}, Landroid/app/Activity;->onCreateOptionsMenu(Landroid/view/Menu;)Z

    iget-object v0, p0, Lcom/android/mail/compose/ComposeActivity;->mAccounts:[Lcom/android/mail/providers/Account;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/mail/compose/ComposeActivity;->mAccounts:[Lcom/android/mail/providers/Account;

    array-length v0, v0

    if-nez v0, :cond_1

    :cond_0
    :goto_0
    return v2

    :cond_1
    invoke-virtual {p0}, Lcom/android/mail/compose/ComposeActivity;->getMenuInflater()Landroid/view/MenuInflater;

    move-result-object v0

    const/high16 v3, 0x7f110000

    invoke-virtual {v0, v3, p1}, Landroid/view/MenuInflater;->inflate(ILandroid/view/Menu;)V

    const v0, 0x7f080113

    invoke-interface {p1, v0}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v0

    iput-object v0, p0, Lcom/android/mail/compose/ComposeActivity;->mSave:Landroid/view/MenuItem;

    invoke-virtual {p0}, Lcom/android/mail/compose/ComposeActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    if-eqz v0, :cond_4

    invoke-virtual {p0}, Lcom/android/mail/compose/ComposeActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    :goto_1
    iget-object v3, p0, Lcom/android/mail/compose/ComposeActivity;->mSavedInstanceState:Landroid/os/Bundle;

    if-eqz v3, :cond_5

    iget-object v0, p0, Lcom/android/mail/compose/ComposeActivity;->mSavedInstanceState:Landroid/os/Bundle;

    const-string v3, "saveEnabled"

    invoke-virtual {v0, v3}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v0

    :goto_2
    invoke-virtual {p0, v0}, Lcom/android/mail/compose/ComposeActivity;->enableSave(Z)V

    const v0, 0x7f080111

    invoke-interface {p1, v0}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v0

    iput-object v0, p0, Lcom/android/mail/compose/ComposeActivity;->mSend:Landroid/view/MenuItem;

    const v0, 0x7f080117

    invoke-interface {p1, v0}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v3

    const v0, 0x7f080116

    invoke-interface {p1, v0}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v4

    if-eqz v3, :cond_2

    iget-object v0, p0, Lcom/android/mail/compose/ComposeActivity;->mAccount:Lcom/android/mail/providers/Account;

    if-eqz v0, :cond_8

    iget-object v0, p0, Lcom/android/mail/compose/ComposeActivity;->mAccount:Lcom/android/mail/providers/Account;

    const v5, 0x8000

    invoke-virtual {v0, v5}, Lcom/android/mail/providers/Account;->supportsCapability(I)Z

    move-result v0

    if-eqz v0, :cond_8

    move v0, v2

    :goto_3
    invoke-interface {v3, v0}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    :cond_2
    if-eqz v4, :cond_0

    iget-object v0, p0, Lcom/android/mail/compose/ComposeActivity;->mAccount:Lcom/android/mail/providers/Account;

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/android/mail/compose/ComposeActivity;->mAccount:Lcom/android/mail/providers/Account;

    const/high16 v3, 0x10000

    invoke-virtual {v0, v3}, Lcom/android/mail/providers/Account;->supportsCapability(I)Z

    move-result v0

    if-eqz v0, :cond_3

    move v1, v2

    :cond_3
    invoke-interface {v4, v1}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    goto :goto_0

    :cond_4
    const/4 v0, 0x0

    goto :goto_1

    :cond_5
    const-string v3, "android.intent.action.SEND"

    invoke-virtual {v3, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_6

    const-string v3, "android.intent.action.SEND_MULTIPLE"

    invoke-virtual {v3, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_6

    const-string v3, "android.intent.action.SENDTO"

    invoke-virtual {v3, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_6

    invoke-direct {p0}, Lcom/android/mail/compose/ComposeActivity;->shouldSave()Z

    move-result v0

    if-eqz v0, :cond_7

    :cond_6
    move v0, v2

    goto :goto_2

    :cond_7
    move v0, v1

    goto :goto_2

    :cond_8
    move v0, v1

    goto :goto_3
.end method

.method public onEditorAction(Landroid/widget/TextView;ILandroid/view/KeyEvent;)Z
    .locals 1
    .param p1    # Landroid/widget/TextView;
    .param p2    # I
    .param p3    # Landroid/view/KeyEvent;

    const/4 v0, 0x6

    if-ne p2, v0, :cond_0

    invoke-virtual {p0}, Lcom/android/mail/compose/ComposeActivity;->focusBody()V

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public onLoadFinished(Landroid/content/Loader;Landroid/database/Cursor;)V
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Loader",
            "<",
            "Landroid/database/Cursor;",
            ">;",
            "Landroid/database/Cursor;",
            ")V"
        }
    .end annotation

    const/4 v6, 0x1

    const/4 v5, 0x0

    const/4 v0, 0x0

    invoke-virtual {p1}, Landroid/content/Loader;->getId()I

    move-result v1

    packed-switch v1, :pswitch_data_0

    :cond_0
    :goto_0
    return-void

    :pswitch_0
    if-eqz p2, :cond_1

    invoke-interface {p2}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v1

    if-eqz v1, :cond_1

    new-instance v1, Lcom/android/mail/providers/Message;

    invoke-direct {v1, p2}, Lcom/android/mail/providers/Message;-><init>(Landroid/database/Cursor;)V

    iput-object v1, p0, Lcom/android/mail/compose/ComposeActivity;->mRefMessage:Lcom/android/mail/providers/Message;

    invoke-virtual {p0}, Lcom/android/mail/compose/ComposeActivity;->getIntent()Landroid/content/Intent;

    move-result-object v1

    const-string v2, "action"

    const/4 v3, -0x1

    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v2

    invoke-direct {p0, v2}, Lcom/android/mail/compose/ComposeActivity;->initFromRefMessage(I)V

    invoke-direct {p0, v2, v1, v0, v6}, Lcom/android/mail/compose/ComposeActivity;->finishSetup(ILandroid/content/Intent;Landroid/os/Bundle;Z)V

    const/4 v3, 0x2

    if-eq v2, v3, :cond_0

    const-string v2, "to"

    invoke-virtual {v1, v2}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_0

    iget-object v2, p0, Lcom/android/mail/compose/ComposeActivity;->mRefMessage:Lcom/android/mail/providers/Message;

    invoke-virtual {v2, v0}, Lcom/android/mail/providers/Message;->setTo(Ljava/lang/String;)V

    iget-object v2, p0, Lcom/android/mail/compose/ComposeActivity;->mRefMessage:Lcom/android/mail/providers/Message;

    invoke-virtual {v2, v0}, Lcom/android/mail/providers/Message;->setFrom(Ljava/lang/String;)V

    invoke-direct {p0}, Lcom/android/mail/compose/ComposeActivity;->clearChangeListeners()V

    iget-object v0, p0, Lcom/android/mail/compose/ComposeActivity;->mTo:Lcom/android/ex/chips/RecipientEditTextView;

    invoke-virtual {v0, v1}, Lcom/android/ex/chips/RecipientEditTextView;->append(Ljava/lang/CharSequence;)V

    invoke-direct {p0}, Lcom/android/mail/compose/ComposeActivity;->initChangeListeners()V

    goto :goto_0

    :cond_1
    invoke-virtual {p0}, Lcom/android/mail/compose/ComposeActivity;->finish()V

    goto :goto_0

    :pswitch_1
    if-eqz p2, :cond_0

    invoke-interface {p2}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v1

    if-eqz v1, :cond_0

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    :cond_2
    new-instance v3, Lcom/android/mail/providers/Account;

    invoke-direct {v3, p2}, Lcom/android/mail/providers/Account;-><init>(Landroid/database/Cursor;)V

    invoke-virtual {v3}, Lcom/android/mail/providers/Account;->isAccountReady()Z

    move-result v4

    if-eqz v4, :cond_3

    invoke-virtual {v2, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :cond_3
    invoke-virtual {v1, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    invoke-interface {p2}, Landroid/database/Cursor;->moveToNext()Z

    move-result v3

    if-nez v3, :cond_2

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v3

    if-lez v3, :cond_4

    const v0, 0x7f08003b

    invoke-virtual {p0, v0}, Lcom/android/mail/compose/ComposeActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    invoke-virtual {p0}, Lcom/android/mail/compose/ComposeActivity;->getLoaderManager()Landroid/app/LoaderManager;

    move-result-object v0

    invoke-virtual {v0, v6}, Landroid/app/LoaderManager;->destroyLoader(I)V

    const v0, 0x7f080036

    invoke-virtual {p0, v0}, Lcom/android/mail/compose/ComposeActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v5}, Landroid/view/View;->setVisibility(I)V

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v0

    new-array v0, v0, [Lcom/android/mail/providers/Account;

    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/android/mail/providers/Account;

    iput-object v0, p0, Lcom/android/mail/compose/ComposeActivity;->mAccounts:[Lcom/android/mail/providers/Account;

    invoke-direct {p0}, Lcom/android/mail/compose/ComposeActivity;->finishCreate()V

    invoke-virtual {p0}, Lcom/android/mail/compose/ComposeActivity;->invalidateOptionsMenu()V

    goto/16 :goto_0

    :cond_4
    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v2

    if-lez v2, :cond_5

    invoke-virtual {v1, v5}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/mail/providers/Account;

    :cond_5
    invoke-direct {p0, v0}, Lcom/android/mail/compose/ComposeActivity;->showWaitFragment(Lcom/android/mail/providers/Account;)V

    goto/16 :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public bridge synthetic onLoadFinished(Landroid/content/Loader;Ljava/lang/Object;)V
    .locals 0
    .param p1    # Landroid/content/Loader;
    .param p2    # Ljava/lang/Object;

    check-cast p2, Landroid/database/Cursor;

    invoke-virtual {p0, p1, p2}, Lcom/android/mail/compose/ComposeActivity;->onLoadFinished(Landroid/content/Loader;Landroid/database/Cursor;)V

    return-void
.end method

.method public onLoaderReset(Landroid/content/Loader;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Loader",
            "<",
            "Landroid/database/Cursor;",
            ">;)V"
        }
    .end annotation

    return-void
.end method

.method public onNavigationItemSelected(IJ)Z
    .locals 6
    .param p1    # I
    .param p2    # J

    const/4 v5, 0x2

    const/4 v3, 0x0

    const/4 v4, 0x1

    iget v0, p0, Lcom/android/mail/compose/ComposeActivity;->mComposeMode:I

    if-nez p1, :cond_6

    iput v3, p0, Lcom/android/mail/compose/ComposeActivity;->mComposeMode:I

    :cond_0
    :goto_0
    invoke-direct {p0}, Lcom/android/mail/compose/ComposeActivity;->clearChangeListeners()V

    iget v5, p0, Lcom/android/mail/compose/ComposeActivity;->mComposeMode:I

    if-eq v0, v5, :cond_5

    invoke-virtual {p0}, Lcom/android/mail/compose/ComposeActivity;->resetMessageForModeChange()V

    iget-object v5, p0, Lcom/android/mail/compose/ComposeActivity;->mRefMessage:Lcom/android/mail/providers/Message;

    if-eqz v5, :cond_1

    iget v5, p0, Lcom/android/mail/compose/ComposeActivity;->mComposeMode:I

    invoke-direct {p0, v5}, Lcom/android/mail/compose/ComposeActivity;->setFieldsFromRefMessage(I)V

    :cond_1
    const/4 v2, 0x0

    const/4 v1, 0x0

    iget-object v5, p0, Lcom/android/mail/compose/ComposeActivity;->mDraft:Lcom/android/mail/providers/Message;

    if-eqz v5, :cond_3

    iget-object v5, p0, Lcom/android/mail/compose/ComposeActivity;->mDraft:Lcom/android/mail/providers/Message;

    invoke-virtual {v5}, Lcom/android/mail/providers/Message;->getBcc()Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v5

    if-nez v5, :cond_8

    move v1, v4

    :goto_1
    if-nez v1, :cond_2

    iget-object v5, p0, Lcom/android/mail/compose/ComposeActivity;->mDraft:Lcom/android/mail/providers/Message;

    invoke-virtual {v5}, Lcom/android/mail/providers/Message;->getCc()Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v5

    if-nez v5, :cond_9

    iget v5, p0, Lcom/android/mail/compose/ComposeActivity;->mComposeMode:I

    if-ne v5, v4, :cond_9

    :cond_2
    move v2, v4

    :cond_3
    :goto_2
    iget-object v5, p0, Lcom/android/mail/compose/ComposeActivity;->mRefMessage:Lcom/android/mail/providers/Message;

    if-eqz v5, :cond_4

    iget-object v5, p0, Lcom/android/mail/compose/ComposeActivity;->mCc:Lcom/android/ex/chips/RecipientEditTextView;

    invoke-virtual {v5}, Lcom/android/ex/chips/RecipientEditTextView;->getText()Landroid/text/Editable;

    move-result-object v5

    invoke-static {v5}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v5

    if-nez v5, :cond_a

    move v2, v4

    :goto_3
    iget-object v5, p0, Lcom/android/mail/compose/ComposeActivity;->mBcc:Lcom/android/ex/chips/RecipientEditTextView;

    invoke-virtual {v5}, Lcom/android/ex/chips/RecipientEditTextView;->getText()Landroid/text/Editable;

    move-result-object v5

    invoke-static {v5}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v5

    if-nez v5, :cond_b

    move v1, v4

    :cond_4
    :goto_4
    iget-object v5, p0, Lcom/android/mail/compose/ComposeActivity;->mCcBccView:Lcom/android/mail/compose/CcBccView;

    invoke-virtual {v5, v3, v2, v1}, Lcom/android/mail/compose/CcBccView;->show(ZZZ)V

    :cond_5
    invoke-direct {p0}, Lcom/android/mail/compose/ComposeActivity;->updateHideOrShowCcBcc()V

    invoke-direct {p0}, Lcom/android/mail/compose/ComposeActivity;->initChangeListeners()V

    return v4

    :cond_6
    if-ne p1, v4, :cond_7

    iput v4, p0, Lcom/android/mail/compose/ComposeActivity;->mComposeMode:I

    goto :goto_0

    :cond_7
    if-ne p1, v5, :cond_0

    iput v5, p0, Lcom/android/mail/compose/ComposeActivity;->mComposeMode:I

    goto :goto_0

    :cond_8
    move v1, v3

    goto :goto_1

    :cond_9
    move v2, v3

    goto :goto_2

    :cond_a
    move v2, v3

    goto :goto_3

    :cond_b
    move v1, v3

    goto :goto_4
.end method

.method public onOptionsItemSelected(Landroid/view/MenuItem;)Z
    .locals 3

    const/4 v0, 0x1

    const/4 v1, 0x0

    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v2

    sparse-switch v2, :sswitch_data_0

    move v0, v1

    :goto_0
    if-nez v0, :cond_0

    invoke-super {p0, p1}, Landroid/app/Activity;->onOptionsItemSelected(Landroid/view/MenuItem;)Z

    move-result v0

    :cond_0
    return v0

    :sswitch_0
    const-string v1, "image/*"

    invoke-direct {p0, v1}, Lcom/android/mail/compose/ComposeActivity;->doAttach(Ljava/lang/String;)V

    goto :goto_0

    :sswitch_1
    const-string v1, "video/*"

    invoke-direct {p0, v1}, Lcom/android/mail/compose/ComposeActivity;->doAttach(Ljava/lang/String;)V

    goto :goto_0

    :sswitch_2
    invoke-direct {p0}, Lcom/android/mail/compose/ComposeActivity;->showCcBccViews()V

    goto :goto_0

    :sswitch_3
    invoke-direct {p0, v0}, Lcom/android/mail/compose/ComposeActivity;->doSave(Z)V

    goto :goto_0

    :sswitch_4
    invoke-direct {p0}, Lcom/android/mail/compose/ComposeActivity;->doSend()V

    goto :goto_0

    :sswitch_5
    invoke-direct {p0}, Lcom/android/mail/compose/ComposeActivity;->doDiscard()V

    goto :goto_0

    :sswitch_6
    iget-object v1, p0, Lcom/android/mail/compose/ComposeActivity;->mAccount:Lcom/android/mail/providers/Account;

    invoke-static {p0, v1}, Lcom/android/mail/utils/Utils;->showSettings(Landroid/content/Context;Lcom/android/mail/providers/Account;)V

    goto :goto_0

    :sswitch_7
    invoke-direct {p0}, Lcom/android/mail/compose/ComposeActivity;->onAppUpPressed()V

    goto :goto_0

    :sswitch_8
    iget-object v1, p0, Lcom/android/mail/compose/ComposeActivity;->mAccount:Lcom/android/mail/providers/Account;

    const v2, 0x7f09001b

    invoke-virtual {p0, v2}, Lcom/android/mail/compose/ComposeActivity;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-static {p0, v1, v2}, Lcom/android/mail/utils/Utils;->showHelp(Landroid/content/Context;Lcom/android/mail/providers/Account;Ljava/lang/String;)V

    goto :goto_0

    :sswitch_9
    iget-object v2, p0, Lcom/android/mail/compose/ComposeActivity;->mAccount:Lcom/android/mail/providers/Account;

    invoke-static {p0, v2, v1}, Lcom/android/mail/utils/Utils;->sendFeedback(Lcom/android/mail/ui/FeedbackEnabledActivity;Lcom/android/mail/providers/Account;Z)V

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        0x102002c -> :sswitch_7
        0x7f08003f -> :sswitch_2
        0x7f080043 -> :sswitch_0
        0x7f080111 -> :sswitch_4
        0x7f080112 -> :sswitch_1
        0x7f080113 -> :sswitch_3
        0x7f080114 -> :sswitch_5
        0x7f080115 -> :sswitch_6
        0x7f080116 -> :sswitch_9
        0x7f080117 -> :sswitch_8
    .end sparse-switch
.end method

.method protected onPause()V
    .locals 1

    invoke-super {p0}, Landroid/app/Activity;->onPause()V

    iget-object v0, p0, Lcom/android/mail/compose/ComposeActivity;->mSendConfirmDialog:Landroid/app/AlertDialog;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/mail/compose/ComposeActivity;->mSendConfirmDialog:Landroid/app/AlertDialog;

    invoke-virtual {v0}, Landroid/app/AlertDialog;->dismiss()V

    :cond_0
    iget-object v0, p0, Lcom/android/mail/compose/ComposeActivity;->mRecipientErrorDialog:Landroid/app/AlertDialog;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/android/mail/compose/ComposeActivity;->mRecipientErrorDialog:Landroid/app/AlertDialog;

    invoke-virtual {v0}, Landroid/app/AlertDialog;->dismiss()V

    :cond_1
    invoke-virtual {p0}, Lcom/android/mail/compose/ComposeActivity;->isChangingConfigurations()Z

    move-result v0

    if-nez v0, :cond_2

    invoke-direct {p0}, Lcom/android/mail/compose/ComposeActivity;->saveIfNeeded()V

    :cond_2
    return-void
.end method

.method public onPrepareOptionsMenu(Landroid/view/Menu;)Z
    .locals 4

    const/4 v3, 0x1

    const v0, 0x7f08003f

    invoke-interface {p1, v0}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v1

    if-eqz v1, :cond_1

    iget-object v0, p0, Lcom/android/mail/compose/ComposeActivity;->mCc:Lcom/android/ex/chips/RecipientEditTextView;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/android/mail/compose/ComposeActivity;->mCc:Lcom/android/ex/chips/RecipientEditTextView;

    invoke-virtual {v0}, Lcom/android/ex/chips/RecipientEditTextView;->isShown()Z

    move-result v0

    iget-object v2, p0, Lcom/android/mail/compose/ComposeActivity;->mBcc:Lcom/android/ex/chips/RecipientEditTextView;

    invoke-virtual {v2}, Lcom/android/ex/chips/RecipientEditTextView;->isShown()Z

    move-result v2

    if-eqz v0, :cond_0

    if-nez v2, :cond_3

    :cond_0
    invoke-interface {v1, v3}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    if-nez v0, :cond_2

    const v0, 0x7f09006d

    :goto_0
    invoke-virtual {p0, v0}, Lcom/android/mail/compose/ComposeActivity;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-interface {v1, v0}, Landroid/view/MenuItem;->setTitle(Ljava/lang/CharSequence;)Landroid/view/MenuItem;

    :cond_1
    :goto_1
    return v3

    :cond_2
    const v0, 0x7f09006e

    goto :goto_0

    :cond_3
    const/4 v0, 0x0

    invoke-interface {v1, v0}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    goto :goto_1
.end method

.method public onRespondInline(Ljava/lang/String;)V
    .locals 2
    .param p1    # Ljava/lang/String;

    const/4 v1, 0x0

    invoke-virtual {p0, p1, v1}, Lcom/android/mail/compose/ComposeActivity;->appendToBody(Ljava/lang/CharSequence;Z)V

    iget-object v0, p0, Lcom/android/mail/compose/ComposeActivity;->mQuotedTextView:Lcom/android/mail/compose/QuotedTextView;

    invoke-virtual {v0, v1}, Lcom/android/mail/compose/QuotedTextView;->setUpperDividerVisible(Z)V

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/android/mail/compose/ComposeActivity;->mRespondedInline:Z

    iget-object v0, p0, Lcom/android/mail/compose/ComposeActivity;->mBodyView:Landroid/widget/EditText;

    invoke-virtual {v0}, Landroid/widget/EditText;->hasFocus()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/android/mail/compose/ComposeActivity;->mBodyView:Landroid/widget/EditText;

    invoke-virtual {v0}, Landroid/widget/EditText;->requestFocus()Z

    :cond_0
    return-void
.end method

.method public final onRestoreInstanceState(Landroid/os/Bundle;)V
    .locals 5
    .param p1    # Landroid/os/Bundle;

    invoke-direct {p0}, Lcom/android/mail/compose/ComposeActivity;->clearChangeListeners()V

    invoke-super {p0, p1}, Landroid/app/Activity;->onRestoreInstanceState(Landroid/os/Bundle;)V

    if-eqz p1, :cond_0

    const-string v4, "focusSelectionStart"

    invoke-virtual {p1, v4}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_0

    const-string v4, "focusSelectionStart"

    invoke-virtual {p1, v4}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v3

    sget-object v4, Lcom/android/mail/compose/ComposeActivity;->EXTRA_FOCUS_SELECTION_END:Ljava/lang/String;

    invoke-virtual {p1, v4}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v2

    invoke-virtual {p0}, Lcom/android/mail/compose/ComposeActivity;->getCurrentFocus()Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/EditText;

    invoke-virtual {v0}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v4

    invoke-interface {v4}, Landroid/text/Editable;->length()I

    move-result v1

    if-ge v3, v1, :cond_0

    if-ge v2, v1, :cond_0

    invoke-virtual {v0, v3, v2}, Landroid/widget/EditText;->setSelection(II)V

    :cond_0
    invoke-direct {p0}, Lcom/android/mail/compose/ComposeActivity;->initChangeListeners()V

    return-void
.end method

.method protected onResume()V
    .locals 4

    invoke-super {p0}, Landroid/app/Activity;->onResume()V

    iget-object v0, p0, Lcom/android/mail/compose/ComposeActivity;->mFromSpinner:Lcom/android/mail/compose/FromAddressSpinner;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/mail/compose/ComposeActivity;->mAccount:Lcom/android/mail/providers/Account;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/mail/compose/ComposeActivity;->mFromSpinner:Lcom/android/mail/compose/FromAddressSpinner;

    iget v1, p0, Lcom/android/mail/compose/ComposeActivity;->mComposeMode:I

    iget-object v2, p0, Lcom/android/mail/compose/ComposeActivity;->mAccount:Lcom/android/mail/providers/Account;

    iget-object v3, p0, Lcom/android/mail/compose/ComposeActivity;->mAccounts:[Lcom/android/mail/providers/Account;

    invoke-virtual {v0, v1, v2, v3}, Lcom/android/mail/compose/FromAddressSpinner;->asyncInitFromSpinner(ILcom/android/mail/providers/Account;[Lcom/android/mail/providers/Account;)V

    :cond_0
    return-void
.end method

.method public final onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 11
    .param p1    # Landroid/os/Bundle;

    invoke-super {p0, p1}, Landroid/app/Activity;->onSaveInstanceState(Landroid/os/Bundle;)V

    iget-object v7, p0, Lcom/android/mail/compose/ComposeActivity;->mAccounts:[Lcom/android/mail/providers/Account;

    if-eqz v7, :cond_0

    iget-object v7, p0, Lcom/android/mail/compose/ComposeActivity;->mAccounts:[Lcom/android/mail/providers/Account;

    array-length v7, v7

    if-nez v7, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    invoke-virtual {p0}, Lcom/android/mail/compose/ComposeActivity;->getCurrentFocus()Landroid/view/View;

    move-result-object v0

    if-eqz v0, :cond_2

    instance-of v7, v0, Landroid/widget/EditText;

    if-eqz v7, :cond_2

    move-object v1, v0

    check-cast v1, Landroid/widget/EditText;

    const-string v7, "focusSelectionStart"

    invoke-virtual {v1}, Landroid/widget/EditText;->getSelectionStart()I

    move-result v8

    invoke-virtual {p1, v7, v8}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    sget-object v7, Lcom/android/mail/compose/ComposeActivity;->EXTRA_FOCUS_SELECTION_END:Ljava/lang/String;

    invoke-virtual {v1}, Landroid/widget/EditText;->getSelectionEnd()I

    move-result v8

    invoke-virtual {p1, v7, v8}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    :cond_2
    iget-object v7, p0, Lcom/android/mail/compose/ComposeActivity;->mFromSpinner:Lcom/android/mail/compose/FromAddressSpinner;

    invoke-virtual {v7}, Lcom/android/mail/compose/FromAddressSpinner;->getReplyFromAccounts()Ljava/util/List;

    move-result-object v4

    iget-object v7, p0, Lcom/android/mail/compose/ComposeActivity;->mFromSpinner:Lcom/android/mail/compose/FromAddressSpinner;

    invoke-virtual {v7}, Lcom/android/mail/compose/FromAddressSpinner;->getSelectedItemPosition()I

    move-result v5

    if-eqz v4, :cond_6

    invoke-interface {v4}, Ljava/util/List;->size()I

    move-result v7

    if-lez v7, :cond_6

    invoke-interface {v4}, Ljava/util/List;->size()I

    move-result v7

    if-le v7, v5, :cond_6

    invoke-interface {v4, v5}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Lcom/android/mail/providers/ReplyFromAccount;

    move-object v6, v7

    :goto_1
    if-eqz v6, :cond_7

    const-string v7, "replyFromAccount"

    invoke-virtual {v6}, Lcom/android/mail/providers/ReplyFromAccount;->serialize()Lorg/json/JSONObject;

    move-result-object v8

    invoke-virtual {v8}, Lorg/json/JSONObject;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {p1, v7, v8}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    const-string v7, "account"

    iget-object v8, v6, Lcom/android/mail/providers/ReplyFromAccount;->account:Lcom/android/mail/providers/Account;

    invoke-virtual {p1, v7, v8}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    :goto_2
    iget-wide v7, p0, Lcom/android/mail/compose/ComposeActivity;->mDraftId:J

    const-wide/16 v9, -0x1

    cmp-long v7, v7, v9

    if-nez v7, :cond_3

    iget v7, p0, Lcom/android/mail/compose/ComposeActivity;->mRequestId:I

    if-eqz v7, :cond_3

    const-string v7, "requestId"

    iget v8, p0, Lcom/android/mail/compose/ComposeActivity;->mRequestId:I

    invoke-virtual {p1, v7, v8}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    :cond_3
    invoke-direct {p0}, Lcom/android/mail/compose/ComposeActivity;->getMode()I

    move-result v3

    const-string v7, "action"

    invoke-virtual {p1, v7, v3}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    invoke-direct {p0, v6, v3}, Lcom/android/mail/compose/ComposeActivity;->createMessage(Lcom/android/mail/providers/ReplyFromAccount;I)Lcom/android/mail/providers/Message;

    move-result-object v2

    iget-object v7, p0, Lcom/android/mail/compose/ComposeActivity;->mDraft:Lcom/android/mail/providers/Message;

    if-eqz v7, :cond_4

    iget-object v7, p0, Lcom/android/mail/compose/ComposeActivity;->mDraft:Lcom/android/mail/providers/Message;

    iget-wide v7, v7, Lcom/android/mail/providers/Message;->id:J

    iput-wide v7, v2, Lcom/android/mail/providers/Message;->id:J

    iget-object v7, p0, Lcom/android/mail/compose/ComposeActivity;->mDraft:Lcom/android/mail/providers/Message;

    iget-object v7, v7, Lcom/android/mail/providers/Message;->serverId:Ljava/lang/String;

    iput-object v7, v2, Lcom/android/mail/providers/Message;->serverId:Ljava/lang/String;

    iget-object v7, p0, Lcom/android/mail/compose/ComposeActivity;->mDraft:Lcom/android/mail/providers/Message;

    iget-object v7, v7, Lcom/android/mail/providers/Message;->uri:Landroid/net/Uri;

    iput-object v7, v2, Lcom/android/mail/providers/Message;->uri:Landroid/net/Uri;

    :cond_4
    const-string v7, "extraMessage"

    invoke-virtual {p1, v7, v2}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    iget-object v7, p0, Lcom/android/mail/compose/ComposeActivity;->mRefMessage:Lcom/android/mail/providers/Message;

    if-eqz v7, :cond_5

    const-string v7, "in-reference-to-message"

    iget-object v8, p0, Lcom/android/mail/compose/ComposeActivity;->mRefMessage:Lcom/android/mail/providers/Message;

    invoke-virtual {p1, v7, v8}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    :cond_5
    const-string v7, "showCc"

    iget-object v8, p0, Lcom/android/mail/compose/ComposeActivity;->mCcBccView:Lcom/android/mail/compose/CcBccView;

    invoke-virtual {v8}, Lcom/android/mail/compose/CcBccView;->isCcVisible()Z

    move-result v8

    invoke-virtual {p1, v7, v8}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    const-string v7, "showBcc"

    iget-object v8, p0, Lcom/android/mail/compose/ComposeActivity;->mCcBccView:Lcom/android/mail/compose/CcBccView;

    invoke-virtual {v8}, Lcom/android/mail/compose/CcBccView;->isBccVisible()Z

    move-result v8

    invoke-virtual {p1, v7, v8}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    const-string v7, "respondedInline"

    iget-boolean v8, p0, Lcom/android/mail/compose/ComposeActivity;->mRespondedInline:Z

    invoke-virtual {p1, v7, v8}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    const-string v8, "saveEnabled"

    iget-object v7, p0, Lcom/android/mail/compose/ComposeActivity;->mSave:Landroid/view/MenuItem;

    if-eqz v7, :cond_8

    iget-object v7, p0, Lcom/android/mail/compose/ComposeActivity;->mSave:Landroid/view/MenuItem;

    invoke-interface {v7}, Landroid/view/MenuItem;->isEnabled()Z

    move-result v7

    if-eqz v7, :cond_8

    const/4 v7, 0x1

    :goto_3
    invoke-virtual {p1, v8, v7}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    const-string v7, "attachmentPreviews"

    iget-object v8, p0, Lcom/android/mail/compose/ComposeActivity;->mAttachmentsView:Lcom/android/mail/compose/AttachmentsView;

    invoke-virtual {v8}, Lcom/android/mail/compose/AttachmentsView;->getAttachmentPreviews()Ljava/util/ArrayList;

    move-result-object v8

    invoke-virtual {p1, v7, v8}, Landroid/os/Bundle;->putParcelableArrayList(Ljava/lang/String;Ljava/util/ArrayList;)V

    goto/16 :goto_0

    :cond_6
    const/4 v6, 0x0

    goto/16 :goto_1

    :cond_7
    const-string v7, "account"

    iget-object v8, p0, Lcom/android/mail/compose/ComposeActivity;->mAccount:Lcom/android/mail/providers/Account;

    invoke-virtual {p1, v7, v8}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    goto/16 :goto_2

    :cond_8
    const/4 v7, 0x0

    goto :goto_3
.end method

.method public onTextChanged(Ljava/lang/CharSequence;III)V
    .locals 0
    .param p1    # Ljava/lang/CharSequence;
    .param p2    # I
    .param p3    # I
    .param p4    # I

    return-void
.end method

.method protected recipientMatchesThisAccount(Ljava/lang/String;)Z
    .locals 2
    .param p1    # Ljava/lang/String;

    iget-object v0, p0, Lcom/android/mail/compose/ComposeActivity;->mAccount:Lcom/android/mail/providers/Account;

    iget-object v1, p0, Lcom/android/mail/compose/ComposeActivity;->mAccount:Lcom/android/mail/providers/Account;

    invoke-virtual {v1}, Lcom/android/mail/providers/Account;->getReplyFroms()Ljava/util/List;

    move-result-object v1

    invoke-static {v0, p1, v1}, Lcom/android/mail/providers/ReplyFromAccount;->matchesAccountOrCustomFrom(Lcom/android/mail/providers/Account;Ljava/lang/String;Ljava/util/List;)Z

    move-result v0

    return v0
.end method

.method protected resetMessageForModeChange()V
    .locals 2

    iget-object v0, p0, Lcom/android/mail/compose/ComposeActivity;->mTo:Lcom/android/ex/chips/RecipientEditTextView;

    const-string v1, ""

    invoke-virtual {v0, v1}, Lcom/android/ex/chips/RecipientEditTextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lcom/android/mail/compose/ComposeActivity;->mCc:Lcom/android/ex/chips/RecipientEditTextView;

    const-string v1, ""

    invoke-virtual {v0, v1}, Lcom/android/ex/chips/RecipientEditTextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lcom/android/mail/compose/ComposeActivity;->mBcc:Lcom/android/ex/chips/RecipientEditTextView;

    const-string v1, ""

    invoke-virtual {v0, v1}, Lcom/android/ex/chips/RecipientEditTextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lcom/android/mail/compose/ComposeActivity;->mSubject:Landroid/widget/TextView;

    const-string v1, ""

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-boolean v0, p0, Lcom/android/mail/compose/ComposeActivity;->mAttachmentsChanged:Z

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/android/mail/compose/ComposeActivity;->mAttachmentsView:Lcom/android/mail/compose/AttachmentsView;

    invoke-virtual {v0}, Lcom/android/mail/compose/AttachmentsView;->deleteAllAttachments()V

    :cond_0
    return-void
.end method

.method protected sendOrSaveWithSanityChecks(ZZZZ)Z
    .locals 7

    const/4 v1, 0x1

    const/4 v0, 0x0

    iget-object v2, p0, Lcom/android/mail/compose/ComposeActivity;->mAccounts:[Lcom/android/mail/providers/Account;

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/android/mail/compose/ComposeActivity;->mAccount:Lcom/android/mail/providers/Account;

    if-nez v2, :cond_2

    :cond_0
    const v1, 0x7f0900a8

    invoke-static {p0, v1, v0}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v1

    invoke-virtual {v1}, Landroid/widget/Toast;->show()V

    if-eqz p4, :cond_1

    invoke-virtual {p0}, Lcom/android/mail/compose/ComposeActivity;->finish()V

    :cond_1
    :goto_0
    return v0

    :cond_2
    iget-object v2, p0, Lcom/android/mail/compose/ComposeActivity;->mBodyView:Landroid/widget/EditText;

    invoke-virtual {v2}, Landroid/widget/EditText;->getEditableText()Landroid/text/Editable;

    move-result-object v5

    if-eqz p3, :cond_3

    new-array v2, v0, [Ljava/lang/String;

    move-object v3, v2

    move-object v4, v2

    :goto_1
    if-nez p1, :cond_4

    array-length v6, v4

    if-nez v6, :cond_4

    array-length v6, v3

    if-nez v6, :cond_4

    array-length v6, v2

    if-nez v6, :cond_4

    const v1, 0x7f090041

    invoke-virtual {p0, v1}, Lcom/android/mail/compose/ComposeActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v1}, Lcom/android/mail/compose/ComposeActivity;->showRecipientErrorDialog(Ljava/lang/String;)V

    goto :goto_0

    :cond_3
    invoke-virtual {p0}, Lcom/android/mail/compose/ComposeActivity;->getToAddresses()[Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p0}, Lcom/android/mail/compose/ComposeActivity;->getCcAddresses()[Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p0}, Lcom/android/mail/compose/ComposeActivity;->getBccAddresses()[Ljava/lang/String;

    move-result-object v2

    goto :goto_1

    :cond_4
    new-instance v6, Ljava/util/ArrayList;

    invoke-direct {v6}, Ljava/util/ArrayList;-><init>()V

    if-nez p1, :cond_5

    invoke-virtual {p0, v4, v6}, Lcom/android/mail/compose/ComposeActivity;->checkInvalidEmails([Ljava/lang/String;Ljava/util/List;)V

    invoke-virtual {p0, v3, v6}, Lcom/android/mail/compose/ComposeActivity;->checkInvalidEmails([Ljava/lang/String;Ljava/util/List;)V

    invoke-virtual {p0, v2, v6}, Lcom/android/mail/compose/ComposeActivity;->checkInvalidEmails([Ljava/lang/String;Ljava/util/List;)V

    :cond_5
    invoke-interface {v6}, Ljava/util/List;->size()I

    move-result v2

    if-lez v2, :cond_6

    const v2, 0x7f0900d6

    invoke-virtual {p0, v2}, Lcom/android/mail/compose/ComposeActivity;->getString(I)Ljava/lang/String;

    move-result-object v2

    new-array v1, v1, [Ljava/lang/Object;

    invoke-interface {v6, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    aput-object v3, v1, v0

    invoke-static {v2, v1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v1}, Lcom/android/mail/compose/ComposeActivity;->showRecipientErrorDialog(Ljava/lang/String;)V

    goto :goto_0

    :cond_6
    new-instance v3, Lcom/android/mail/compose/ComposeActivity$2;

    invoke-direct {v3, p0, p1, p2, p3}, Lcom/android/mail/compose/ComposeActivity$2;-><init>(Lcom/android/mail/compose/ComposeActivity;ZZZ)V

    if-nez p1, :cond_c

    iget-object v2, p0, Lcom/android/mail/compose/ComposeActivity;->mAttachmentsView:Lcom/android/mail/compose/AttachmentsView;

    invoke-virtual {v2}, Lcom/android/mail/compose/AttachmentsView;->getAttachments()Ljava/util/ArrayList;

    move-result-object v2

    invoke-virtual {v2}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v2

    if-eqz v2, :cond_b

    invoke-virtual {p0}, Lcom/android/mail/compose/ComposeActivity;->showEmptyTextWarnings()Z

    move-result v2

    if-eqz v2, :cond_b

    invoke-virtual {p0}, Lcom/android/mail/compose/ComposeActivity;->isSubjectEmpty()Z

    move-result v4

    invoke-static {v5}, Landroid/text/TextUtils;->getTrimmedLength(Ljava/lang/CharSequence;)I

    move-result v2

    if-nez v2, :cond_8

    move v2, v1

    :goto_2
    if-eqz v2, :cond_9

    iget-boolean v2, p0, Lcom/android/mail/compose/ComposeActivity;->mForward:Z

    if-eqz v2, :cond_7

    invoke-virtual {p0}, Lcom/android/mail/compose/ComposeActivity;->isBodyEmpty()Z

    move-result v2

    if-eqz v2, :cond_9

    :cond_7
    move v2, v1

    :goto_3
    if-eqz v4, :cond_a

    const v0, 0x7f090044

    invoke-direct {p0, v0, v3}, Lcom/android/mail/compose/ComposeActivity;->showSendConfirmDialog(ILandroid/content/DialogInterface$OnClickListener;)V

    move v0, v1

    goto/16 :goto_0

    :cond_8
    move v2, v0

    goto :goto_2

    :cond_9
    move v2, v0

    goto :goto_3

    :cond_a
    if-eqz v2, :cond_b

    const v0, 0x7f090045

    invoke-direct {p0, v0, v3}, Lcom/android/mail/compose/ComposeActivity;->showSendConfirmDialog(ILandroid/content/DialogInterface$OnClickListener;)V

    move v0, v1

    goto/16 :goto_0

    :cond_b
    invoke-virtual {p0}, Lcom/android/mail/compose/ComposeActivity;->showSendConfirmation()Z

    move-result v2

    if-eqz v2, :cond_c

    const v0, 0x7f090046

    invoke-direct {p0, v0, v3}, Lcom/android/mail/compose/ComposeActivity;->showSendConfirmDialog(ILandroid/content/DialogInterface$OnClickListener;)V

    move v0, v1

    goto/16 :goto_0

    :cond_c
    invoke-direct {p0, v5, p1, p2, v0}, Lcom/android/mail/compose/ComposeActivity;->sendOrSave(Landroid/text/Spanned;ZZZ)V

    move v0, v1

    goto/16 :goto_0
.end method

.method setAccount(Lcom/android/mail/providers/Account;)V
    .locals 1
    .param p1    # Lcom/android/mail/providers/Account;

    if-nez p1, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v0, p0, Lcom/android/mail/compose/ComposeActivity;->mAccount:Lcom/android/mail/providers/Account;

    invoke-virtual {p1, v0}, Lcom/android/mail/providers/Account;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_2

    iput-object p1, p0, Lcom/android/mail/compose/ComposeActivity;->mAccount:Lcom/android/mail/providers/Account;

    iget-object v0, p0, Lcom/android/mail/compose/ComposeActivity;->mAccount:Lcom/android/mail/providers/Account;

    iget-object v0, v0, Lcom/android/mail/providers/Account;->settings:Lcom/android/mail/providers/Settings;

    iput-object v0, p0, Lcom/android/mail/compose/ComposeActivity;->mCachedSettings:Lcom/android/mail/providers/Settings;

    invoke-direct {p0}, Lcom/android/mail/compose/ComposeActivity;->appendSignature()V

    :cond_2
    iget-object v0, p0, Lcom/android/mail/compose/ComposeActivity;->mAccount:Lcom/android/mail/providers/Account;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/mail/compose/ComposeActivity;->mAccount:Lcom/android/mail/providers/Account;

    iget-object v0, v0, Lcom/android/mail/providers/Account;->name:Ljava/lang/String;

    invoke-static {v0}, Lcom/android/mail/ui/MailActivity;->setNfcMessage(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public setBody(Ljava/lang/CharSequence;Z)V
    .locals 1
    .param p1    # Ljava/lang/CharSequence;
    .param p2    # Z

    iget-object v0, p0, Lcom/android/mail/compose/ComposeActivity;->mBodyView:Landroid/widget/EditText;

    invoke-virtual {v0, p1}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    if-eqz p2, :cond_0

    invoke-direct {p0}, Lcom/android/mail/compose/ComposeActivity;->appendSignature()V

    :cond_0
    return-void
.end method

.method protected showEmptyTextWarnings()Z
    .locals 1

    iget-object v0, p0, Lcom/android/mail/compose/ComposeActivity;->mAttachmentsView:Lcom/android/mail/compose/AttachmentsView;

    invoke-virtual {v0}, Lcom/android/mail/compose/AttachmentsView;->getAttachments()Ljava/util/ArrayList;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public showRecipientErrorDialog(Ljava/lang/String;)V
    .locals 3

    iget-object v0, p0, Lcom/android/mail/compose/ComposeActivity;->mRecipientErrorDialog:Landroid/app/AlertDialog;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/mail/compose/ComposeActivity;->mRecipientErrorDialog:Landroid/app/AlertDialog;

    invoke-virtual {v0}, Landroid/app/AlertDialog;->dismiss()V

    :cond_0
    new-instance v0, Landroid/app/AlertDialog$Builder;

    invoke-direct {v0, p0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    invoke-virtual {v0, p1}, Landroid/app/AlertDialog$Builder;->setMessage(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    const v1, 0x7f090042

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    const v1, 0x1010355

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setIconAttribute(I)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    const v1, 0x7f0900e0

    new-instance v2, Lcom/android/mail/compose/ComposeActivity$1;

    invoke-direct {v2, p0}, Lcom/android/mail/compose/ComposeActivity$1;-><init>(Lcom/android/mail/compose/ComposeActivity;)V

    invoke-virtual {v0, v1, v2}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->show()Landroid/app/AlertDialog;

    move-result-object v0

    iput-object v0, p0, Lcom/android/mail/compose/ComposeActivity;->mRecipientErrorDialog:Landroid/app/AlertDialog;

    return-void
.end method

.method protected showSendConfirmation()Z
    .locals 1

    iget-object v0, p0, Lcom/android/mail/compose/ComposeActivity;->mCachedSettings:Lcom/android/mail/providers/Settings;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/mail/compose/ComposeActivity;->mCachedSettings:Lcom/android/mail/providers/Settings;

    iget-boolean v0, v0, Lcom/android/mail/providers/Settings;->confirmSend:Z

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected tokenizeAddressList(Ljava/util/Collection;)Ljava/util/List;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Collection",
            "<",
            "Ljava/lang/String;",
            ">;)",
            "Ljava/util/List",
            "<[",
            "Landroid/text/util/Rfc822Token;",
            ">;"
        }
    .end annotation

    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    invoke-interface {p1}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Landroid/text/util/Rfc822Tokenizer;->tokenize(Ljava/lang/CharSequence;)[Landroid/text/util/Rfc822Token;

    move-result-object v3

    invoke-interface {v2, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_0
    return-object v2
.end method

.method public updateSaveUi()V
    .locals 2

    iget-object v0, p0, Lcom/android/mail/compose/ComposeActivity;->mSave:Landroid/view/MenuItem;

    if-eqz v0, :cond_0

    iget-object v1, p0, Lcom/android/mail/compose/ComposeActivity;->mSave:Landroid/view/MenuItem;

    invoke-direct {p0}, Lcom/android/mail/compose/ComposeActivity;->shouldSave()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-virtual {p0}, Lcom/android/mail/compose/ComposeActivity;->isBlank()Z

    move-result v0

    if-nez v0, :cond_1

    const/4 v0, 0x1

    :goto_0
    invoke-interface {v1, v0}, Landroid/view/MenuItem;->setEnabled(Z)Landroid/view/MenuItem;

    :cond_0
    return-void

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method
