.class public Lcom/android/mail/widget/WidgetService$MailFactory;
.super Ljava/lang/Object;
.source "WidgetService.java"

# interfaces
.implements Landroid/content/Loader$OnLoadCompleteListener;
.implements Landroid/widget/RemoteViewsService$RemoteViewsFactory;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/mail/widget/WidgetService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xc
    name = "MailFactory"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/mail/widget/WidgetService$MailFactory$FolderUpdateHandler;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Landroid/widget/RemoteViewsService$RemoteViewsFactory;",
        "Landroid/content/Loader$OnLoadCompleteListener",
        "<",
        "Landroid/database/Cursor;",
        ">;"
    }
.end annotation


# instance fields
.field private final mAccount:Lcom/android/mail/providers/Account;

.field private final mAppWidgetId:I

.field private final mContext:Landroid/content/Context;

.field private mConversationCursor:Landroid/database/Cursor;

.field private mConversationCursorLoader:Landroid/content/CursorLoader;

.field private mElidedPaddingToken:Ljava/lang/String;

.field private mFolder:Lcom/android/mail/providers/Folder;

.field private mFolderCount:I

.field private mFolderInformationShown:Z

.field private mFolderLoader:Landroid/content/CursorLoader;

.field private mFolderUpdateHandler:Lcom/android/mail/widget/WidgetService$MailFactory$FolderUpdateHandler;

.field private mReadStyle:Landroid/text/style/TextAppearanceSpan;

.field private mSendersSplitToken:Ljava/lang/String;

.field private mService:Lcom/android/mail/widget/WidgetService;

.field private mShouldShowViewMore:Z

.field private mUnreadStyle:Landroid/text/style/TextAppearanceSpan;

.field private final mWidgetConversationViewBuilder:Lcom/android/mail/widget/WidgetConversationViewBuilder;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/content/Intent;Lcom/android/mail/widget/WidgetService;)V
    .locals 2
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/content/Intent;
    .param p3    # Lcom/android/mail/widget/WidgetService;

    const/4 v1, 0x0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-boolean v1, p0, Lcom/android/mail/widget/WidgetService$MailFactory;->mFolderInformationShown:Z

    iput-object p1, p0, Lcom/android/mail/widget/WidgetService$MailFactory;->mContext:Landroid/content/Context;

    const-string v0, "appWidgetId"

    invoke-virtual {p2, v0, v1}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v0

    iput v0, p0, Lcom/android/mail/widget/WidgetService$MailFactory;->mAppWidgetId:I

    const-string v0, "account"

    invoke-virtual {p2, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/android/mail/providers/Account;->newinstance(Ljava/lang/String;)Lcom/android/mail/providers/Account;

    move-result-object v0

    iput-object v0, p0, Lcom/android/mail/widget/WidgetService$MailFactory;->mAccount:Lcom/android/mail/providers/Account;

    const-string v0, "folder"

    invoke-virtual {p2, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/android/mail/providers/Folder;->fromString(Ljava/lang/String;)Lcom/android/mail/providers/Folder;

    move-result-object v0

    iput-object v0, p0, Lcom/android/mail/widget/WidgetService$MailFactory;->mFolder:Lcom/android/mail/providers/Folder;

    new-instance v0, Lcom/android/mail/widget/WidgetConversationViewBuilder;

    iget-object v1, p0, Lcom/android/mail/widget/WidgetService$MailFactory;->mAccount:Lcom/android/mail/providers/Account;

    invoke-direct {v0, p1, v1}, Lcom/android/mail/widget/WidgetConversationViewBuilder;-><init>(Landroid/content/Context;Lcom/android/mail/providers/Account;)V

    iput-object v0, p0, Lcom/android/mail/widget/WidgetService$MailFactory;->mWidgetConversationViewBuilder:Lcom/android/mail/widget/WidgetConversationViewBuilder;

    iput-object p3, p0, Lcom/android/mail/widget/WidgetService$MailFactory;->mService:Lcom/android/mail/widget/WidgetService;

    return-void
.end method

.method static synthetic access$200(Lcom/android/mail/widget/WidgetService$MailFactory;)Landroid/content/CursorLoader;
    .locals 1
    .param p0    # Lcom/android/mail/widget/WidgetService$MailFactory;

    iget-object v0, p0, Lcom/android/mail/widget/WidgetService$MailFactory;->mFolderLoader:Landroid/content/CursorLoader;

    return-object v0
.end method

.method private copyStyles([Landroid/text/style/CharacterStyle;Ljava/lang/CharSequence;)Landroid/text/SpannableString;
    .locals 4
    .param p1    # [Landroid/text/style/CharacterStyle;
    .param p2    # Ljava/lang/CharSequence;

    const/4 v3, 0x0

    new-instance v0, Landroid/text/SpannableString;

    invoke-direct {v0, p2}, Landroid/text/SpannableString;-><init>(Ljava/lang/CharSequence;)V

    if-eqz p1, :cond_0

    array-length v1, p1

    if-lez v1, :cond_0

    aget-object v1, p1, v3

    invoke-virtual {v0}, Landroid/text/SpannableString;->length()I

    move-result v2

    invoke-virtual {v0, v1, v3, v2, v3}, Landroid/text/SpannableString;->setSpan(Ljava/lang/Object;III)V

    :cond_0
    return-object v0
.end method

.method private ellipsizeStyledSenders(Lcom/android/mail/providers/ConversationInfo;ILjava/util/ArrayList;)Landroid/text/SpannableStringBuilder;
    .locals 9
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/android/mail/providers/ConversationInfo;",
            "I",
            "Ljava/util/ArrayList",
            "<",
            "Landroid/text/SpannableString;",
            ">;)",
            "Landroid/text/SpannableStringBuilder;"
        }
    .end annotation

    const/4 v7, 0x0

    new-instance v3, Landroid/text/SpannableStringBuilder;

    invoke-direct {v3}, Landroid/text/SpannableStringBuilder;-><init>()V

    const/4 v1, 0x0

    invoke-virtual {p3}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v4

    move-object v2, v1

    :goto_0
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_4

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/text/SpannableString;

    if-nez v0, :cond_0

    # getter for: Lcom/android/mail/widget/WidgetService;->LOG_TAG:Ljava/lang/String;
    invoke-static {}, Lcom/android/mail/widget/WidgetService;->access$100()Ljava/lang/String;

    move-result-object v0

    const-string v1, "null sender while iterating over styledSenders"

    new-array v5, v7, [Ljava/lang/Object;

    invoke-static {v0, v1, v5}, Lcom/android/mail/utils/LogUtils;->e(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)I

    goto :goto_0

    :cond_0
    invoke-virtual {v0}, Landroid/text/SpannableString;->length()I

    move-result v1

    const-class v5, Landroid/text/style/CharacterStyle;

    invoke-virtual {v0, v7, v1, v5}, Landroid/text/SpannableString;->getSpans(IILjava/lang/Class;)[Ljava/lang/Object;

    move-result-object v1

    check-cast v1, [Landroid/text/style/CharacterStyle;

    sget-object v5, Lcom/android/mail/browse/SendersView;->sElidedString:Ljava/lang/CharSequence;

    invoke-virtual {v0}, Landroid/text/SpannableString;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_1

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v5, p0, Lcom/android/mail/widget/WidgetService$MailFactory;->mElidedPaddingToken:Ljava/lang/String;

    invoke-virtual {v2, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v5, p0, Lcom/android/mail/widget/WidgetService$MailFactory;->mElidedPaddingToken:Ljava/lang/String;

    invoke-virtual {v2, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {p0, v1, v2}, Lcom/android/mail/widget/WidgetService$MailFactory;->copyStyles([Landroid/text/style/CharacterStyle;Ljava/lang/CharSequence;)Landroid/text/SpannableString;

    move-result-object v1

    move-object v8, v1

    move-object v1, v0

    move-object v0, v8

    :goto_1
    invoke-virtual {v3, v0}, Landroid/text/SpannableStringBuilder;->append(Ljava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    move-object v2, v1

    goto :goto_0

    :cond_1
    invoke-virtual {v3}, Landroid/text/SpannableStringBuilder;->length()I

    move-result v5

    if-lez v5, :cond_3

    if-eqz v2, :cond_2

    sget-object v5, Lcom/android/mail/browse/SendersView;->sElidedString:Ljava/lang/CharSequence;

    invoke-virtual {v2}, Landroid/text/SpannableString;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v5, v2}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_3

    :cond_2
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v5, p0, Lcom/android/mail/widget/WidgetService$MailFactory;->mSendersSplitToken:Ljava/lang/String;

    invoke-virtual {v2, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {p0, v1, v2}, Lcom/android/mail/widget/WidgetService$MailFactory;->copyStyles([Landroid/text/style/CharacterStyle;Ljava/lang/CharSequence;)Landroid/text/SpannableString;

    move-result-object v1

    move-object v8, v1

    move-object v1, v0

    move-object v0, v8

    goto :goto_1

    :cond_3
    move-object v1, v0

    goto :goto_1

    :cond_4
    return-object v3
.end method

.method private static filterTag(Ljava/lang/String;)Ljava/lang/String;
    .locals 5
    .param p0    # Ljava/lang/String;

    move-object v1, p0

    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v3

    if-lez v3, :cond_0

    const/4 v3, 0x0

    invoke-virtual {p0, v3}, Ljava/lang/String;->charAt(I)C

    move-result v3

    const/16 v4, 0x5b

    if-ne v3, v4, :cond_0

    const/16 v3, 0x5d

    invoke-virtual {p0, v3}, Ljava/lang/String;->indexOf(I)I

    move-result v0

    if-lez v0, :cond_0

    const/4 v3, 0x1

    invoke-virtual {p0, v3, v0}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v2

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "["

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const/4 v4, 0x7

    invoke-static {v2, v4}, Lcom/android/mail/utils/Utils;->ellipsize(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "]"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    add-int/lit8 v4, v0, 0x1

    invoke-virtual {p0, v4}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    :cond_0
    return-object v1
.end method

.method private getConversationCount()I
    .locals 3

    # getter for: Lcom/android/mail/widget/WidgetService;->sWidgetLock:Ljava/lang/Object;
    invoke-static {}, Lcom/android/mail/widget/WidgetService;->access$000()Ljava/lang/Object;

    move-result-object v2

    monitor-enter v2

    :try_start_0
    iget-object v1, p0, Lcom/android/mail/widget/WidgetService$MailFactory;->mConversationCursor:Landroid/database/Cursor;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/android/mail/widget/WidgetService$MailFactory;->mConversationCursor:Landroid/database/Cursor;

    invoke-interface {v1}, Landroid/database/Cursor;->getCount()I

    move-result v0

    :goto_0
    const/16 v1, 0x19

    invoke-static {v0, v1}, Ljava/lang/Math;->min(II)I

    move-result v1

    monitor-exit v2

    return v1

    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    :catchall_0
    move-exception v1

    monitor-exit v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1
.end method

.method private getReadStyle()Landroid/text/style/CharacterStyle;
    .locals 3

    iget-object v0, p0, Lcom/android/mail/widget/WidgetService$MailFactory;->mReadStyle:Landroid/text/style/TextAppearanceSpan;

    if-nez v0, :cond_0

    new-instance v0, Landroid/text/style/TextAppearanceSpan;

    iget-object v1, p0, Lcom/android/mail/widget/WidgetService$MailFactory;->mContext:Landroid/content/Context;

    const v2, 0x7f0d0058

    invoke-direct {v0, v1, v2}, Landroid/text/style/TextAppearanceSpan;-><init>(Landroid/content/Context;I)V

    iput-object v0, p0, Lcom/android/mail/widget/WidgetService$MailFactory;->mReadStyle:Landroid/text/style/TextAppearanceSpan;

    :cond_0
    iget-object v0, p0, Lcom/android/mail/widget/WidgetService$MailFactory;->mReadStyle:Landroid/text/style/TextAppearanceSpan;

    invoke-static {v0}, Landroid/text/style/CharacterStyle;->wrap(Landroid/text/style/CharacterStyle;)Landroid/text/style/CharacterStyle;

    move-result-object v0

    return-object v0
.end method

.method private getUnreadStyle()Landroid/text/style/CharacterStyle;
    .locals 3

    iget-object v0, p0, Lcom/android/mail/widget/WidgetService$MailFactory;->mUnreadStyle:Landroid/text/style/TextAppearanceSpan;

    if-nez v0, :cond_0

    new-instance v0, Landroid/text/style/TextAppearanceSpan;

    iget-object v1, p0, Lcom/android/mail/widget/WidgetService$MailFactory;->mContext:Landroid/content/Context;

    const v2, 0x7f0d0057

    invoke-direct {v0, v1, v2}, Landroid/text/style/TextAppearanceSpan;-><init>(Landroid/content/Context;I)V

    iput-object v0, p0, Lcom/android/mail/widget/WidgetService$MailFactory;->mUnreadStyle:Landroid/text/style/TextAppearanceSpan;

    :cond_0
    iget-object v0, p0, Lcom/android/mail/widget/WidgetService$MailFactory;->mUnreadStyle:Landroid/text/style/TextAppearanceSpan;

    invoke-static {v0}, Landroid/text/style/CharacterStyle;->wrap(Landroid/text/style/CharacterStyle;)Landroid/text/style/CharacterStyle;

    move-result-object v0

    return-object v0
.end method

.method private getViewMoreConversationsView()Landroid/widget/RemoteViews;
    .locals 4

    new-instance v0, Landroid/widget/RemoteViews;

    iget-object v1, p0, Lcom/android/mail/widget/WidgetService$MailFactory;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v1

    const v2, 0x7f040072

    invoke-direct {v0, v1, v2}, Landroid/widget/RemoteViews;-><init>(Ljava/lang/String;I)V

    const v1, 0x7f08010f

    iget-object v2, p0, Lcom/android/mail/widget/WidgetService$MailFactory;->mContext:Landroid/content/Context;

    const v3, 0x7f0900ea

    invoke-virtual {v2, v3}, Landroid/content/Context;->getText(I)Ljava/lang/CharSequence;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/widget/RemoteViews;->setTextViewText(ILjava/lang/CharSequence;)V

    const v1, 0x7f08010e

    iget-object v2, p0, Lcom/android/mail/widget/WidgetService$MailFactory;->mFolder:Lcom/android/mail/providers/Folder;

    iget-object v3, p0, Lcom/android/mail/widget/WidgetService$MailFactory;->mAccount:Lcom/android/mail/providers/Account;

    invoke-static {v2, v3}, Lcom/android/mail/utils/Utils;->createViewFolderIntent(Lcom/android/mail/providers/Folder;Lcom/android/mail/providers/Account;)Landroid/content/Intent;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/widget/RemoteViews;->setOnClickFillInIntent(ILandroid/content/Intent;)V

    return-object v0
.end method

.method private isDataValid(Landroid/database/Cursor;)Z
    .locals 1
    .param p1    # Landroid/database/Cursor;

    if-eqz p1, :cond_0

    invoke-interface {p1}, Landroid/database/Cursor;->isClosed()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-interface {p1}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public getCount()I
    .locals 6

    const/4 v2, 0x1

    const/4 v3, 0x0

    # getter for: Lcom/android/mail/widget/WidgetService;->sWidgetLock:Ljava/lang/Object;
    invoke-static {}, Lcom/android/mail/widget/WidgetService;->access$000()Ljava/lang/Object;

    move-result-object v5

    monitor-enter v5

    :try_start_0
    invoke-direct {p0}, Lcom/android/mail/widget/WidgetService$MailFactory;->getConversationCount()I

    move-result v0

    iget-object v4, p0, Lcom/android/mail/widget/WidgetService$MailFactory;->mConversationCursor:Landroid/database/Cursor;

    if-eqz v4, :cond_1

    iget-object v4, p0, Lcom/android/mail/widget/WidgetService$MailFactory;->mConversationCursor:Landroid/database/Cursor;

    invoke-interface {v4}, Landroid/database/Cursor;->getCount()I

    move-result v1

    :goto_0
    if-lt v0, v1, :cond_0

    iget v4, p0, Lcom/android/mail/widget/WidgetService$MailFactory;->mFolderCount:I

    if-ge v0, v4, :cond_2

    :cond_0
    move v4, v2

    :goto_1
    iput-boolean v4, p0, Lcom/android/mail/widget/WidgetService$MailFactory;->mShouldShowViewMore:Z

    iget-boolean v4, p0, Lcom/android/mail/widget/WidgetService$MailFactory;->mShouldShowViewMore:Z

    if-eqz v4, :cond_3

    :goto_2
    add-int/2addr v2, v0

    monitor-exit v5

    return v2

    :cond_1
    move v1, v3

    goto :goto_0

    :cond_2
    move v4, v3

    goto :goto_1

    :cond_3
    move v2, v3

    goto :goto_2

    :catchall_0
    move-exception v2

    monitor-exit v5
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v2
.end method

.method public getItemId(I)J
    .locals 2
    .param p1    # I

    int-to-long v0, p1

    return-wide v0
.end method

.method public getLoadingView()Landroid/widget/RemoteViews;
    .locals 4

    new-instance v0, Landroid/widget/RemoteViews;

    iget-object v1, p0, Lcom/android/mail/widget/WidgetService$MailFactory;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v1

    const v2, 0x7f040072

    invoke-direct {v0, v1, v2}, Landroid/widget/RemoteViews;-><init>(Ljava/lang/String;I)V

    const v1, 0x7f08010f

    iget-object v2, p0, Lcom/android/mail/widget/WidgetService$MailFactory;->mContext:Landroid/content/Context;

    const v3, 0x7f0900eb

    invoke-virtual {v2, v3}, Landroid/content/Context;->getText(I)Ljava/lang/CharSequence;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/widget/RemoteViews;->setTextViewText(ILjava/lang/CharSequence;)V

    return-object v0
.end method

.method public getViewAt(I)Landroid/widget/RemoteViews;
    .locals 16
    .param p1    # I

    # getter for: Lcom/android/mail/widget/WidgetService;->sWidgetLock:Ljava/lang/Object;
    invoke-static {}, Lcom/android/mail/widget/WidgetService;->access$000()Ljava/lang/Object;

    move-result-object v15

    monitor-enter v15

    :try_start_0
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/android/mail/widget/WidgetService$MailFactory;->mConversationCursor:Landroid/database/Cursor;

    if-eqz v1, :cond_0

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/android/mail/widget/WidgetService$MailFactory;->mConversationCursor:Landroid/database/Cursor;

    invoke-interface {v1}, Landroid/database/Cursor;->isClosed()Z

    move-result v1

    if-nez v1, :cond_0

    move-object/from16 v0, p0

    iget-boolean v1, v0, Lcom/android/mail/widget/WidgetService$MailFactory;->mShouldShowViewMore:Z

    if-eqz v1, :cond_1

    invoke-direct/range {p0 .. p0}, Lcom/android/mail/widget/WidgetService$MailFactory;->getConversationCount()I

    move-result v1

    move/from16 v0, p1

    if-lt v0, v1, :cond_1

    :cond_0
    invoke-direct/range {p0 .. p0}, Lcom/android/mail/widget/WidgetService$MailFactory;->getViewMoreConversationsView()Landroid/widget/RemoteViews;

    move-result-object v13

    monitor-exit v15

    :goto_0
    return-object v13

    :cond_1
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/android/mail/widget/WidgetService$MailFactory;->mConversationCursor:Landroid/database/Cursor;

    move/from16 v0, p1

    invoke-interface {v1, v0}, Landroid/database/Cursor;->moveToPosition(I)Z

    move-result v1

    if-nez v1, :cond_2

    # getter for: Lcom/android/mail/widget/WidgetService;->LOG_TAG:Ljava/lang/String;
    invoke-static {}, Lcom/android/mail/widget/WidgetService;->access$100()Ljava/lang/String;

    move-result-object v1

    const-string v2, "Failed to move to position %d in the cursor."

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    invoke-static/range {p1 .. p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    aput-object v6, v3, v4

    invoke-static {v1, v2, v3}, Lcom/android/mail/utils/LogUtils;->e(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)I

    invoke-direct/range {p0 .. p0}, Lcom/android/mail/widget/WidgetService$MailFactory;->getViewMoreConversationsView()Landroid/widget/RemoteViews;

    move-result-object v13

    monitor-exit v15

    goto :goto_0

    :catchall_0
    move-exception v1

    monitor-exit v15
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1

    :cond_2
    :try_start_1
    new-instance v9, Lcom/android/mail/providers/Conversation;

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/android/mail/widget/WidgetService$MailFactory;->mConversationCursor:Landroid/database/Cursor;

    invoke-direct {v9, v1}, Lcom/android/mail/providers/Conversation;-><init>(Landroid/database/Cursor;)V

    new-instance v11, Landroid/text/SpannableStringBuilder;

    invoke-direct {v11}, Landroid/text/SpannableStringBuilder;-><init>()V

    new-instance v14, Landroid/text/SpannableStringBuilder;

    invoke-direct {v14}, Landroid/text/SpannableStringBuilder;-><init>()V

    iget-object v1, v9, Lcom/android/mail/providers/Conversation;->conversationInfo:Lcom/android/mail/providers/ConversationInfo;

    if-eqz v1, :cond_3

    new-instance v5, Ljava/util/ArrayList;

    invoke-direct {v5}, Ljava/util/ArrayList;-><init>()V

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/android/mail/widget/WidgetService$MailFactory;->mContext:Landroid/content/Context;

    iget-object v2, v9, Lcom/android/mail/providers/Conversation;->conversationInfo:Lcom/android/mail/providers/ConversationInfo;

    const-string v3, ""

    const/16 v4, 0x19

    const/4 v6, 0x0

    const/4 v7, 0x0

    move-object/from16 v0, p0

    iget-object v10, v0, Lcom/android/mail/widget/WidgetService$MailFactory;->mAccount:Lcom/android/mail/providers/Account;

    iget-object v8, v10, Lcom/android/mail/providers/Account;->name:Ljava/lang/String;

    invoke-static/range {v1 .. v8}, Lcom/android/mail/browse/SendersView;->format(Landroid/content/Context;Lcom/android/mail/providers/ConversationInfo;Ljava/lang/String;ILjava/util/ArrayList;Ljava/util/ArrayList;Ljava/util/ArrayList;Ljava/lang/String;)V

    iget-object v1, v9, Lcom/android/mail/providers/Conversation;->conversationInfo:Lcom/android/mail/providers/ConversationInfo;

    const/16 v2, 0x19

    move-object/from16 v0, p0

    invoke-direct {v0, v1, v2, v5}, Lcom/android/mail/widget/WidgetService$MailFactory;->ellipsizeStyledSenders(Lcom/android/mail/providers/ConversationInfo;ILjava/util/ArrayList;)Landroid/text/SpannableStringBuilder;

    move-result-object v11

    :goto_1
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/android/mail/widget/WidgetService$MailFactory;->mContext:Landroid/content/Context;

    iget-wide v2, v9, Lcom/android/mail/providers/Conversation;->dateMs:J

    invoke-static {v1, v2, v3}, Landroid/text/format/DateUtils;->getRelativeTimeSpanString(Landroid/content/Context;J)Ljava/lang/CharSequence;

    move-result-object v8

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/android/mail/widget/WidgetService$MailFactory;->mWidgetConversationViewBuilder:Lcom/android/mail/widget/WidgetConversationViewBuilder;

    move-object/from16 v0, p0

    iget-object v10, v0, Lcom/android/mail/widget/WidgetService$MailFactory;->mFolder:Lcom/android/mail/providers/Folder;

    iget-object v1, v9, Lcom/android/mail/providers/Conversation;->subject:Ljava/lang/String;

    invoke-static {v1}, Lcom/android/mail/widget/WidgetService$MailFactory;->filterTag(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v12

    move-object v7, v14

    invoke-virtual/range {v6 .. v12}, Lcom/android/mail/widget/WidgetConversationViewBuilder;->getStyledView(Ljava/lang/CharSequence;Ljava/lang/CharSequence;Lcom/android/mail/providers/Conversation;Lcom/android/mail/providers/Folder;Landroid/text/SpannableStringBuilder;Ljava/lang/String;)Landroid/widget/RemoteViews;

    move-result-object v13

    const v1, 0x7f080103

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/mail/widget/WidgetService$MailFactory;->mFolder:Lcom/android/mail/providers/Folder;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/android/mail/widget/WidgetService$MailFactory;->mAccount:Lcom/android/mail/providers/Account;

    invoke-static {v9, v2, v3}, Lcom/android/mail/utils/Utils;->createViewConversationIntent(Lcom/android/mail/providers/Conversation;Lcom/android/mail/providers/Folder;Lcom/android/mail/providers/Account;)Landroid/content/Intent;

    move-result-object v2

    invoke-virtual {v13, v1, v2}, Landroid/widget/RemoteViews;->setOnClickFillInIntent(ILandroid/content/Intent;)V

    monitor-exit v15

    goto/16 :goto_0

    :cond_3
    iget-object v1, v9, Lcom/android/mail/providers/Conversation;->senders:Ljava/lang/String;

    invoke-virtual {v11, v1}, Landroid/text/SpannableStringBuilder;->append(Ljava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    iget-boolean v1, v9, Lcom/android/mail/providers/Conversation;->read:Z

    if-eqz v1, :cond_4

    invoke-direct/range {p0 .. p0}, Lcom/android/mail/widget/WidgetService$MailFactory;->getReadStyle()Landroid/text/style/CharacterStyle;

    move-result-object v1

    :goto_2
    const/4 v2, 0x0

    invoke-virtual {v11}, Landroid/text/SpannableStringBuilder;->length()I

    move-result v3

    const/4 v4, 0x0

    invoke-virtual {v11, v1, v2, v3, v4}, Landroid/text/SpannableStringBuilder;->setSpan(Ljava/lang/Object;III)V

    goto :goto_1

    :cond_4
    invoke-direct/range {p0 .. p0}, Lcom/android/mail/widget/WidgetService$MailFactory;->getUnreadStyle()Landroid/text/style/CharacterStyle;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result-object v1

    goto :goto_2
.end method

.method public getViewTypeCount()I
    .locals 1

    const/4 v0, 0x2

    return v0
.end method

.method public hasStableIds()Z
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public onCreate()V
    .locals 13

    iget-object v0, p0, Lcom/android/mail/widget/WidgetService$MailFactory;->mContext:Landroid/content/Context;

    iget v1, p0, Lcom/android/mail/widget/WidgetService$MailFactory;->mAppWidgetId:I

    iget-object v3, p0, Lcom/android/mail/widget/WidgetService$MailFactory;->mAccount:Lcom/android/mail/providers/Account;

    iget-object v4, p0, Lcom/android/mail/widget/WidgetService$MailFactory;->mFolder:Lcom/android/mail/providers/Folder;

    invoke-static {v0, v1, v3, v4}, Lcom/android/mail/widget/WidgetService;->saveWidgetInformation(Landroid/content/Context;ILcom/android/mail/providers/Account;Lcom/android/mail/providers/Folder;)V

    iget-object v0, p0, Lcom/android/mail/widget/WidgetService$MailFactory;->mService:Lcom/android/mail/widget/WidgetService;

    iget-object v1, p0, Lcom/android/mail/widget/WidgetService$MailFactory;->mContext:Landroid/content/Context;

    iget v3, p0, Lcom/android/mail/widget/WidgetService$MailFactory;->mAppWidgetId:I

    iget-object v4, p0, Lcom/android/mail/widget/WidgetService$MailFactory;->mAccount:Lcom/android/mail/providers/Account;

    iget-object v5, p0, Lcom/android/mail/widget/WidgetService$MailFactory;->mFolder:Lcom/android/mail/providers/Folder;

    invoke-virtual {v0, v1, v3, v4, v5}, Lcom/android/mail/widget/WidgetService;->isWidgetConfigured(Landroid/content/Context;ILcom/android/mail/providers/Account;Lcom/android/mail/providers/Folder;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/android/mail/widget/WidgetService$MailFactory;->mContext:Landroid/content/Context;

    iget v1, p0, Lcom/android/mail/widget/WidgetService$MailFactory;->mAppWidgetId:I

    iget-object v3, p0, Lcom/android/mail/widget/WidgetService$MailFactory;->mAccount:Lcom/android/mail/providers/Account;

    iget-object v4, p0, Lcom/android/mail/widget/WidgetService$MailFactory;->mFolder:Lcom/android/mail/providers/Folder;

    invoke-static {v0, v1, v3, v4}, Lcom/android/mail/widget/BaseWidgetProvider;->updateWidget(Landroid/content/Context;ILcom/android/mail/providers/Account;Lcom/android/mail/providers/Folder;)V

    :cond_0
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/android/mail/widget/WidgetService$MailFactory;->mFolderInformationShown:Z

    iget-object v0, p0, Lcom/android/mail/widget/WidgetService$MailFactory;->mFolder:Lcom/android/mail/providers/Folder;

    iget-object v0, v0, Lcom/android/mail/providers/Folder;->conversationListUri:Landroid/net/Uri;

    invoke-virtual {v0}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v10

    const/16 v0, 0x19

    invoke-static {v0}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v11

    const-string v0, "limit"

    invoke-virtual {v10, v0, v11}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    const-string v1, "use_network"

    sget-object v3, Ljava/lang/Boolean;->FALSE:Ljava/lang/Boolean;

    invoke-virtual {v3}, Ljava/lang/Boolean;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v1, v3}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    const-string v1, "all_notifications"

    sget-object v3, Ljava/lang/Boolean;->TRUE:Ljava/lang/Boolean;

    invoke-virtual {v3}, Ljava/lang/Boolean;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v1, v3}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v2

    iget-object v0, p0, Lcom/android/mail/widget/WidgetService$MailFactory;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v12

    new-instance v0, Landroid/content/CursorLoader;

    iget-object v1, p0, Lcom/android/mail/widget/WidgetService$MailFactory;->mContext:Landroid/content/Context;

    sget-object v3, Lcom/android/mail/providers/UIProvider;->CONVERSATION_PROJECTION:[Ljava/lang/String;

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x0

    invoke-direct/range {v0 .. v6}, Landroid/content/CursorLoader;-><init>(Landroid/content/Context;Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)V

    iput-object v0, p0, Lcom/android/mail/widget/WidgetService$MailFactory;->mConversationCursorLoader:Landroid/content/CursorLoader;

    iget-object v0, p0, Lcom/android/mail/widget/WidgetService$MailFactory;->mConversationCursorLoader:Landroid/content/CursorLoader;

    const/4 v1, 0x1

    invoke-virtual {v0, v1, p0}, Landroid/content/CursorLoader;->registerListener(ILandroid/content/Loader$OnLoadCompleteListener;)V

    iget-object v0, p0, Lcom/android/mail/widget/WidgetService$MailFactory;->mConversationCursorLoader:Landroid/content/CursorLoader;

    const v1, 0x7f0b0022

    invoke-virtual {v12, v1}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v1

    int-to-long v3, v1

    invoke-virtual {v0, v3, v4}, Landroid/content/CursorLoader;->setUpdateThrottle(J)V

    iget-object v0, p0, Lcom/android/mail/widget/WidgetService$MailFactory;->mConversationCursorLoader:Landroid/content/CursorLoader;

    invoke-virtual {v0}, Landroid/content/CursorLoader;->startLoading()V

    const v0, 0x7f09010e

    invoke-virtual {v12, v0}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/android/mail/widget/WidgetService$MailFactory;->mSendersSplitToken:Ljava/lang/String;

    const v0, 0x7f090111

    invoke-virtual {v12, v0}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/android/mail/widget/WidgetService$MailFactory;->mElidedPaddingToken:Ljava/lang/String;

    new-instance v3, Landroid/content/CursorLoader;

    iget-object v4, p0, Lcom/android/mail/widget/WidgetService$MailFactory;->mContext:Landroid/content/Context;

    iget-object v0, p0, Lcom/android/mail/widget/WidgetService$MailFactory;->mFolder:Lcom/android/mail/providers/Folder;

    iget-object v5, v0, Lcom/android/mail/providers/Folder;->uri:Landroid/net/Uri;

    sget-object v6, Lcom/android/mail/providers/UIProvider;->FOLDERS_PROJECTION:[Ljava/lang/String;

    const/4 v7, 0x0

    const/4 v8, 0x0

    const/4 v9, 0x0

    invoke-direct/range {v3 .. v9}, Landroid/content/CursorLoader;-><init>(Landroid/content/Context;Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)V

    iput-object v3, p0, Lcom/android/mail/widget/WidgetService$MailFactory;->mFolderLoader:Landroid/content/CursorLoader;

    iget-object v0, p0, Lcom/android/mail/widget/WidgetService$MailFactory;->mFolderLoader:Landroid/content/CursorLoader;

    const/4 v1, 0x0

    invoke-virtual {v0, v1, p0}, Landroid/content/CursorLoader;->registerListener(ILandroid/content/Loader$OnLoadCompleteListener;)V

    new-instance v0, Lcom/android/mail/widget/WidgetService$MailFactory$FolderUpdateHandler;

    const v1, 0x7f0b0023

    invoke-virtual {v12, v1}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v1

    invoke-direct {v0, p0, v1}, Lcom/android/mail/widget/WidgetService$MailFactory$FolderUpdateHandler;-><init>(Lcom/android/mail/widget/WidgetService$MailFactory;I)V

    iput-object v0, p0, Lcom/android/mail/widget/WidgetService$MailFactory;->mFolderUpdateHandler:Lcom/android/mail/widget/WidgetService$MailFactory$FolderUpdateHandler;

    iget-object v0, p0, Lcom/android/mail/widget/WidgetService$MailFactory;->mFolderUpdateHandler:Lcom/android/mail/widget/WidgetService$MailFactory$FolderUpdateHandler;

    invoke-virtual {v0}, Lcom/android/mail/widget/WidgetService$MailFactory$FolderUpdateHandler;->scheduleTask()V

    return-void
.end method

.method public onDataSetChanged()V
    .locals 1

    iget-object v0, p0, Lcom/android/mail/widget/WidgetService$MailFactory;->mFolderUpdateHandler:Lcom/android/mail/widget/WidgetService$MailFactory$FolderUpdateHandler;

    invoke-virtual {v0}, Lcom/android/mail/widget/WidgetService$MailFactory$FolderUpdateHandler;->scheduleTask()V

    return-void
.end method

.method public onDestroy()V
    .locals 3

    const/4 v2, 0x0

    # getter for: Lcom/android/mail/widget/WidgetService;->sWidgetLock:Ljava/lang/Object;
    invoke-static {}, Lcom/android/mail/widget/WidgetService;->access$000()Ljava/lang/Object;

    move-result-object v1

    monitor-enter v1

    :try_start_0
    iget-object v0, p0, Lcom/android/mail/widget/WidgetService$MailFactory;->mConversationCursorLoader:Landroid/content/CursorLoader;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/mail/widget/WidgetService$MailFactory;->mConversationCursorLoader:Landroid/content/CursorLoader;

    invoke-virtual {v0}, Landroid/content/CursorLoader;->reset()V

    iget-object v0, p0, Lcom/android/mail/widget/WidgetService$MailFactory;->mConversationCursorLoader:Landroid/content/CursorLoader;

    invoke-virtual {v0, p0}, Landroid/content/CursorLoader;->unregisterListener(Landroid/content/Loader$OnLoadCompleteListener;)V

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/android/mail/widget/WidgetService$MailFactory;->mConversationCursorLoader:Landroid/content/CursorLoader;

    :cond_0
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/android/mail/widget/WidgetService$MailFactory;->mConversationCursor:Landroid/database/Cursor;

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    iget-object v0, p0, Lcom/android/mail/widget/WidgetService$MailFactory;->mFolderLoader:Landroid/content/CursorLoader;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/android/mail/widget/WidgetService$MailFactory;->mFolderLoader:Landroid/content/CursorLoader;

    invoke-virtual {v0}, Landroid/content/CursorLoader;->reset()V

    iget-object v0, p0, Lcom/android/mail/widget/WidgetService$MailFactory;->mFolderLoader:Landroid/content/CursorLoader;

    invoke-virtual {v0, p0}, Landroid/content/CursorLoader;->unregisterListener(Landroid/content/Loader$OnLoadCompleteListener;)V

    iput-object v2, p0, Lcom/android/mail/widget/WidgetService$MailFactory;->mFolderLoader:Landroid/content/CursorLoader;

    :cond_1
    return-void

    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method

.method public onLoadComplete(Landroid/content/Loader;Landroid/database/Cursor;)V
    .locals 12
    .param p2    # Landroid/database/Cursor;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Loader",
            "<",
            "Landroid/database/Cursor;",
            ">;",
            "Landroid/database/Cursor;",
            ")V"
        }
    .end annotation

    const v11, 0x7f0800fd

    const v10, 0x7f0800fb

    const/4 v9, 0x0

    iget-object v0, p0, Lcom/android/mail/widget/WidgetService$MailFactory;->mContext:Landroid/content/Context;

    invoke-static {v0}, Landroid/appwidget/AppWidgetManager;->getInstance(Landroid/content/Context;)Landroid/appwidget/AppWidgetManager;

    move-result-object v7

    new-instance v2, Landroid/widget/RemoteViews;

    iget-object v0, p0, Lcom/android/mail/widget/WidgetService$MailFactory;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v0

    const v1, 0x7f040070

    invoke-direct {v2, v0, v1}, Landroid/widget/RemoteViews;-><init>(Ljava/lang/String;I)V

    iget-object v0, p0, Lcom/android/mail/widget/WidgetService$MailFactory;->mFolderLoader:Landroid/content/CursorLoader;

    if-ne p1, v0, :cond_5

    invoke-direct {p0, p2}, Lcom/android/mail/widget/WidgetService$MailFactory;->isDataValid(Landroid/database/Cursor;)Z

    move-result v0

    if-nez v0, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    const/16 v0, 0x8

    invoke-interface {p2, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v8

    const/4 v0, 0x2

    invoke-interface {p2, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v6

    const/16 v0, 0x9

    invoke-interface {p2, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    iput v0, p0, Lcom/android/mail/widget/WidgetService$MailFactory;->mFolderCount:I

    iget-boolean v0, p0, Lcom/android/mail/widget/WidgetService$MailFactory;->mFolderInformationShown:Z

    if-nez v0, :cond_2

    invoke-static {v6}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_2

    iget-object v0, p0, Lcom/android/mail/widget/WidgetService$MailFactory;->mAccount:Lcom/android/mail/providers/Account;

    iget-object v0, v0, Lcom/android/mail/providers/Account;->name:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_2

    iget-object v0, p0, Lcom/android/mail/widget/WidgetService$MailFactory;->mService:Lcom/android/mail/widget/WidgetService;

    iget-object v1, p0, Lcom/android/mail/widget/WidgetService$MailFactory;->mContext:Landroid/content/Context;

    iget v3, p0, Lcom/android/mail/widget/WidgetService$MailFactory;->mAppWidgetId:I

    iget-object v4, p0, Lcom/android/mail/widget/WidgetService$MailFactory;->mAccount:Lcom/android/mail/providers/Account;

    iget-object v5, p0, Lcom/android/mail/widget/WidgetService$MailFactory;->mFolder:Lcom/android/mail/providers/Folder;

    invoke-virtual/range {v0 .. v6}, Lcom/android/mail/widget/WidgetService;->configureValidAccountWidget(Landroid/content/Context;Landroid/widget/RemoteViews;ILcom/android/mail/providers/Account;Lcom/android/mail/providers/Folder;Ljava/lang/String;)V

    iget v0, p0, Lcom/android/mail/widget/WidgetService$MailFactory;->mAppWidgetId:I

    invoke-virtual {v7, v0, v2}, Landroid/appwidget/AppWidgetManager;->updateAppWidget(ILandroid/widget/RemoteViews;)V

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/android/mail/widget/WidgetService$MailFactory;->mFolderInformationShown:Z

    :cond_2
    invoke-static {v6}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_4

    invoke-virtual {v2, v10, v9}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    invoke-virtual {v2, v10, v6}, Landroid/widget/RemoteViews;->setTextViewText(ILjava/lang/CharSequence;)V

    :goto_1
    iget-object v0, p0, Lcom/android/mail/widget/WidgetService$MailFactory;->mAccount:Lcom/android/mail/providers/Account;

    iget-object v0, v0, Lcom/android/mail/providers/Account;->name:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_3

    const v0, 0x7f0800fc

    iget-object v1, p0, Lcom/android/mail/widget/WidgetService$MailFactory;->mAccount:Lcom/android/mail/providers/Account;

    iget-object v1, v1, Lcom/android/mail/providers/Account;->name:Ljava/lang/String;

    invoke-virtual {v2, v0, v1}, Landroid/widget/RemoteViews;->setTextViewText(ILjava/lang/CharSequence;)V

    :cond_3
    invoke-virtual {v2, v11, v9}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    iget-object v0, p0, Lcom/android/mail/widget/WidgetService$MailFactory;->mContext:Landroid/content/Context;

    invoke-static {v0, v8}, Lcom/android/mail/utils/Utils;->getUnreadCountString(Landroid/content/Context;I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v11, v0}, Landroid/widget/RemoteViews;->setTextViewText(ILjava/lang/CharSequence;)V

    iget v0, p0, Lcom/android/mail/widget/WidgetService$MailFactory;->mAppWidgetId:I

    invoke-virtual {v7, v0, v2}, Landroid/appwidget/AppWidgetManager;->partiallyUpdateAppWidget(ILandroid/widget/RemoteViews;)V

    goto :goto_0

    :cond_4
    # getter for: Lcom/android/mail/widget/WidgetService;->LOG_TAG:Ljava/lang/String;
    invoke-static {}, Lcom/android/mail/widget/WidgetService;->access$100()Ljava/lang/String;

    move-result-object v0

    const-string v1, "Empty folder name"

    new-array v3, v9, [Ljava/lang/Object;

    invoke-static {v0, v1, v3}, Lcom/android/mail/utils/LogUtils;->e(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)I

    goto :goto_1

    :cond_5
    iget-object v0, p0, Lcom/android/mail/widget/WidgetService$MailFactory;->mConversationCursorLoader:Landroid/content/CursorLoader;

    if-ne p1, v0, :cond_0

    # getter for: Lcom/android/mail/widget/WidgetService;->sWidgetLock:Ljava/lang/Object;
    invoke-static {}, Lcom/android/mail/widget/WidgetService;->access$000()Ljava/lang/Object;

    move-result-object v1

    monitor-enter v1

    :try_start_0
    invoke-direct {p0, p2}, Lcom/android/mail/widget/WidgetService$MailFactory;->isDataValid(Landroid/database/Cursor;)Z

    move-result v0

    if-nez v0, :cond_7

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/android/mail/widget/WidgetService$MailFactory;->mConversationCursor:Landroid/database/Cursor;

    :goto_2
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    iget v0, p0, Lcom/android/mail/widget/WidgetService$MailFactory;->mAppWidgetId:I

    const v1, 0x7f080100

    invoke-virtual {v7, v0, v1}, Landroid/appwidget/AppWidgetManager;->notifyAppWidgetViewDataChanged(II)V

    iget-object v0, p0, Lcom/android/mail/widget/WidgetService$MailFactory;->mConversationCursor:Landroid/database/Cursor;

    if-eqz v0, :cond_6

    iget-object v0, p0, Lcom/android/mail/widget/WidgetService$MailFactory;->mConversationCursor:Landroid/database/Cursor;

    invoke-interface {v0}, Landroid/database/Cursor;->getCount()I

    move-result v0

    if-nez v0, :cond_0

    :cond_6
    const v0, 0x7f0800ff

    iget-object v1, p0, Lcom/android/mail/widget/WidgetService$MailFactory;->mContext:Landroid/content/Context;

    const v3, 0x7f0900ad

    invoke-virtual {v1, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v2, v0, v1}, Landroid/widget/RemoteViews;->setTextViewText(ILjava/lang/CharSequence;)V

    iget v0, p0, Lcom/android/mail/widget/WidgetService$MailFactory;->mAppWidgetId:I

    invoke-virtual {v7, v0, v2}, Landroid/appwidget/AppWidgetManager;->partiallyUpdateAppWidget(ILandroid/widget/RemoteViews;)V

    goto/16 :goto_0

    :cond_7
    :try_start_1
    iput-object p2, p0, Lcom/android/mail/widget/WidgetService$MailFactory;->mConversationCursor:Landroid/database/Cursor;

    goto :goto_2

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method

.method public bridge synthetic onLoadComplete(Landroid/content/Loader;Ljava/lang/Object;)V
    .locals 0
    .param p1    # Landroid/content/Loader;
    .param p2    # Ljava/lang/Object;

    check-cast p2, Landroid/database/Cursor;

    invoke-virtual {p0, p1, p2}, Lcom/android/mail/widget/WidgetService$MailFactory;->onLoadComplete(Landroid/content/Loader;Landroid/database/Cursor;)V

    return-void
.end method
