.class final Lcom/android/mail/photomanager/ContactPhotoManagerImpl$Request;
.super Ljava/lang/Object;
.source "ContactPhotoManager.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/mail/photomanager/ContactPhotoManagerImpl;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1a
    name = "Request"
.end annotation


# instance fields
.field private final mDefaultProvider:Lcom/android/mail/photomanager/ContactPhotoManager$DefaultImageProvider;

.field private final mDisplayName:Ljava/lang/String;

.field private final mEmailAddress:Ljava/lang/String;

.field private final mRequestedExtent:I

.field private final mView:Lcom/android/mail/ui/DividedImageCanvas;


# direct methods
.method private constructor <init>(Ljava/lang/String;Ljava/lang/String;ILcom/android/mail/photomanager/ContactPhotoManager$DefaultImageProvider;Lcom/android/mail/ui/DividedImageCanvas;)V
    .locals 0
    .param p1    # Ljava/lang/String;
    .param p2    # Ljava/lang/String;
    .param p3    # I
    .param p4    # Lcom/android/mail/photomanager/ContactPhotoManager$DefaultImageProvider;
    .param p5    # Lcom/android/mail/ui/DividedImageCanvas;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput p3, p0, Lcom/android/mail/photomanager/ContactPhotoManagerImpl$Request;->mRequestedExtent:I

    iput-object p4, p0, Lcom/android/mail/photomanager/ContactPhotoManagerImpl$Request;->mDefaultProvider:Lcom/android/mail/photomanager/ContactPhotoManager$DefaultImageProvider;

    iput-object p1, p0, Lcom/android/mail/photomanager/ContactPhotoManagerImpl$Request;->mDisplayName:Ljava/lang/String;

    iput-object p2, p0, Lcom/android/mail/photomanager/ContactPhotoManagerImpl$Request;->mEmailAddress:Ljava/lang/String;

    iput-object p5, p0, Lcom/android/mail/photomanager/ContactPhotoManagerImpl$Request;->mView:Lcom/android/mail/ui/DividedImageCanvas;

    return-void
.end method

.method public static createFromEmailAddress(Ljava/lang/String;Ljava/lang/String;Lcom/android/mail/photomanager/ContactPhotoManager$DefaultImageProvider;Lcom/android/mail/ui/DividedImageCanvas;)Lcom/android/mail/photomanager/ContactPhotoManagerImpl$Request;
    .locals 6
    .param p0    # Ljava/lang/String;
    .param p1    # Ljava/lang/String;
    .param p2    # Lcom/android/mail/photomanager/ContactPhotoManager$DefaultImageProvider;
    .param p3    # Lcom/android/mail/ui/DividedImageCanvas;

    new-instance v0, Lcom/android/mail/photomanager/ContactPhotoManagerImpl$Request;

    const/4 v3, -0x1

    move-object v1, p0

    move-object v2, p1

    move-object v4, p2

    move-object v5, p3

    invoke-direct/range {v0 .. v5}, Lcom/android/mail/photomanager/ContactPhotoManagerImpl$Request;-><init>(Ljava/lang/String;Ljava/lang/String;ILcom/android/mail/photomanager/ContactPhotoManager$DefaultImageProvider;Lcom/android/mail/ui/DividedImageCanvas;)V

    return-object v0
.end method


# virtual methods
.method public applyDefaultImage(Lcom/android/mail/ui/DividedImageCanvas;)V
    .locals 4
    .param p1    # Lcom/android/mail/ui/DividedImageCanvas;

    iget-object v0, p0, Lcom/android/mail/photomanager/ContactPhotoManagerImpl$Request;->mDefaultProvider:Lcom/android/mail/photomanager/ContactPhotoManager$DefaultImageProvider;

    invoke-virtual {p0}, Lcom/android/mail/photomanager/ContactPhotoManagerImpl$Request;->getDisplayName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0}, Lcom/android/mail/photomanager/ContactPhotoManagerImpl$Request;->getEmailAddress()Ljava/lang/String;

    move-result-object v2

    iget v3, p0, Lcom/android/mail/photomanager/ContactPhotoManagerImpl$Request;->mRequestedExtent:I

    invoke-virtual {v0, v1, v2, p1, v3}, Lcom/android/mail/photomanager/ContactPhotoManager$DefaultImageProvider;->applyDefaultImage(Ljava/lang/String;Ljava/lang/String;Lcom/android/mail/ui/DividedImageCanvas;I)V

    return-void
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 5
    .param p1    # Ljava/lang/Object;

    const/4 v1, 0x1

    const/4 v2, 0x0

    if-ne p0, p1, :cond_1

    :cond_0
    :goto_0
    return v1

    :cond_1
    if-nez p1, :cond_2

    move v1, v2

    goto :goto_0

    :cond_2
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v4

    if-eq v3, v4, :cond_3

    move v1, v2

    goto :goto_0

    :cond_3
    move-object v0, p1

    check-cast v0, Lcom/android/mail/photomanager/ContactPhotoManagerImpl$Request;

    iget v3, p0, Lcom/android/mail/photomanager/ContactPhotoManagerImpl$Request;->mRequestedExtent:I

    iget v4, v0, Lcom/android/mail/photomanager/ContactPhotoManagerImpl$Request;->mRequestedExtent:I

    if-eq v3, v4, :cond_4

    move v1, v2

    goto :goto_0

    :cond_4
    iget-object v3, p0, Lcom/android/mail/photomanager/ContactPhotoManagerImpl$Request;->mDisplayName:Ljava/lang/String;

    iget-object v4, v0, Lcom/android/mail/photomanager/ContactPhotoManagerImpl$Request;->mDisplayName:Ljava/lang/String;

    invoke-static {v3, v4}, Lcom/google/common/base/Objects;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_5

    move v1, v2

    goto :goto_0

    :cond_5
    iget-object v3, p0, Lcom/android/mail/photomanager/ContactPhotoManagerImpl$Request;->mEmailAddress:Ljava/lang/String;

    iget-object v4, v0, Lcom/android/mail/photomanager/ContactPhotoManagerImpl$Request;->mEmailAddress:Ljava/lang/String;

    invoke-static {v3, v4}, Lcom/google/common/base/Objects;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_0

    move v1, v2

    goto :goto_0
.end method

.method public getDisplayName()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/android/mail/photomanager/ContactPhotoManagerImpl$Request;->mDisplayName:Ljava/lang/String;

    return-object v0
.end method

.method public getEmailAddress()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/android/mail/photomanager/ContactPhotoManagerImpl$Request;->mEmailAddress:Ljava/lang/String;

    return-object v0
.end method

.method public getKey()Ljava/lang/Object;
    .locals 1

    iget-object v0, p0, Lcom/android/mail/photomanager/ContactPhotoManagerImpl$Request;->mEmailAddress:Ljava/lang/String;

    return-object v0
.end method

.method public getView()Lcom/android/mail/ui/DividedImageCanvas;
    .locals 1

    iget-object v0, p0, Lcom/android/mail/photomanager/ContactPhotoManagerImpl$Request;->mView:Lcom/android/mail/ui/DividedImageCanvas;

    return-object v0
.end method

.method public hashCode()I
    .locals 5

    const/4 v3, 0x0

    const/16 v0, 0x1f

    const/4 v1, 0x1

    iget v2, p0, Lcom/android/mail/photomanager/ContactPhotoManagerImpl$Request;->mRequestedExtent:I

    add-int/lit8 v1, v2, 0x1f

    mul-int/lit8 v4, v1, 0x1f

    iget-object v2, p0, Lcom/android/mail/photomanager/ContactPhotoManagerImpl$Request;->mDisplayName:Ljava/lang/String;

    if-nez v2, :cond_0

    move v2, v3

    :goto_0
    add-int v1, v4, v2

    mul-int/lit8 v4, v1, 0x1f

    iget-object v2, p0, Lcom/android/mail/photomanager/ContactPhotoManagerImpl$Request;->mEmailAddress:Ljava/lang/String;

    if-nez v2, :cond_1

    move v2, v3

    :goto_1
    add-int v1, v4, v2

    mul-int/lit8 v2, v1, 0x1f

    iget-object v4, p0, Lcom/android/mail/photomanager/ContactPhotoManagerImpl$Request;->mView:Lcom/android/mail/ui/DividedImageCanvas;

    if-nez v4, :cond_2

    :goto_2
    add-int v1, v2, v3

    return v1

    :cond_0
    iget-object v2, p0, Lcom/android/mail/photomanager/ContactPhotoManagerImpl$Request;->mDisplayName:Ljava/lang/String;

    invoke-virtual {v2}, Ljava/lang/String;->hashCode()I

    move-result v2

    goto :goto_0

    :cond_1
    iget-object v2, p0, Lcom/android/mail/photomanager/ContactPhotoManagerImpl$Request;->mEmailAddress:Ljava/lang/String;

    invoke-virtual {v2}, Ljava/lang/String;->hashCode()I

    move-result v2

    goto :goto_1

    :cond_2
    iget-object v3, p0, Lcom/android/mail/photomanager/ContactPhotoManagerImpl$Request;->mView:Lcom/android/mail/ui/DividedImageCanvas;

    invoke-virtual {v3}, Ljava/lang/Object;->hashCode()I

    move-result v3

    goto :goto_2
.end method
