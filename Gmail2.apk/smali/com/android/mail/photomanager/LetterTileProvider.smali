.class public Lcom/android/mail/photomanager/LetterTileProvider;
.super Lcom/android/mail/photomanager/ContactPhotoManager$DefaultImageProvider;
.source "LetterTileProvider.java"


# static fields
.field private static final ALPHABET:Ljava/util/regex/Pattern;

.field private static DEFAULT_AVATAR_DRAWABLE:I

.field private static sBitmapBackgroundCache:[Landroid/graphics/Bitmap;

.field private static sBounds:Landroid/graphics/Rect;

.field private static sPaint:Landroid/text/TextPaint;

.field private static sSansSerifLight:Landroid/graphics/Typeface;

.field private static sTileColor:I

.field private static sTileFontColor:I

.field private static sTileLetterFontSize:I

.field private static sTileLetterFontSizeSmall:I


# instance fields
.field private mDefaultBitmap:Landroid/graphics/Bitmap;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const/4 v0, -0x1

    sput v0, Lcom/android/mail/photomanager/LetterTileProvider;->sTileLetterFontSize:I

    new-instance v0, Landroid/text/TextPaint;

    invoke-direct {v0}, Landroid/text/TextPaint;-><init>()V

    sput-object v0, Lcom/android/mail/photomanager/LetterTileProvider;->sPaint:Landroid/text/TextPaint;

    const v0, 0x7f02003e

    sput v0, Lcom/android/mail/photomanager/LetterTileProvider;->DEFAULT_AVATAR_DRAWABLE:I

    const-string v0, "^[a-zA-Z0-9]+$"

    invoke-static {v0}, Ljava/util/regex/Pattern;->compile(Ljava/lang/String;)Ljava/util/regex/Pattern;

    move-result-object v0

    sput-object v0, Lcom/android/mail/photomanager/LetterTileProvider;->ALPHABET:Ljava/util/regex/Pattern;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Lcom/android/mail/photomanager/ContactPhotoManager$DefaultImageProvider;-><init>()V

    return-void
.end method

.method private getBitmap(Lcom/android/mail/ui/DividedImageCanvas$Dimensions;)Landroid/graphics/Bitmap;
    .locals 6
    .param p1    # Lcom/android/mail/ui/DividedImageCanvas$Dimensions;

    iget v2, p1, Lcom/android/mail/ui/DividedImageCanvas$Dimensions;->scale:F

    const/high16 v3, 0x3f800000

    cmpl-float v3, v2, v3

    if-nez v3, :cond_1

    const/4 v1, 0x0

    :goto_0
    sget-object v3, Lcom/android/mail/photomanager/LetterTileProvider;->sBitmapBackgroundCache:[Landroid/graphics/Bitmap;

    aget-object v0, v3, v1

    if-nez v0, :cond_0

    iget v3, p1, Lcom/android/mail/ui/DividedImageCanvas$Dimensions;->width:I

    iget v4, p1, Lcom/android/mail/ui/DividedImageCanvas$Dimensions;->height:I

    sget-object v5, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    invoke-static {v3, v4, v5}, Landroid/graphics/Bitmap;->createBitmap(IILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;

    move-result-object v0

    sget-object v3, Lcom/android/mail/photomanager/LetterTileProvider;->sBitmapBackgroundCache:[Landroid/graphics/Bitmap;

    aput-object v0, v3, v1

    :cond_0
    return-object v0

    :cond_1
    const/high16 v3, 0x3f000000

    cmpl-float v3, v2, v3

    if-nez v3, :cond_2

    const/4 v1, 0x1

    goto :goto_0

    :cond_2
    const/4 v1, 0x2

    goto :goto_0
.end method

.method private getFontSize(F)I
    .locals 1
    .param p1    # F

    const/high16 v0, 0x3f800000

    cmpl-float v0, p1, v0

    if-nez v0, :cond_0

    sget v0, Lcom/android/mail/photomanager/LetterTileProvider;->sTileLetterFontSize:I

    :goto_0
    return v0

    :cond_0
    sget v0, Lcom/android/mail/photomanager/LetterTileProvider;->sTileLetterFontSizeSmall:I

    goto :goto_0
.end method

.method private isLetter(Ljava/lang/String;)Z
    .locals 2
    .param p1    # Ljava/lang/String;

    sget-object v1, Lcom/android/mail/photomanager/LetterTileProvider;->ALPHABET:Ljava/util/regex/Pattern;

    invoke-virtual {v1, p1}, Ljava/util/regex/Pattern;->matcher(Ljava/lang/CharSequence;)Ljava/util/regex/Matcher;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/regex/Matcher;->matches()Z

    move-result v1

    return v1
.end method


# virtual methods
.method public applyDefaultImage(Ljava/lang/String;Ljava/lang/String;Lcom/android/mail/ui/DividedImageCanvas;I)V
    .locals 12
    .param p1    # Ljava/lang/String;
    .param p2    # Ljava/lang/String;
    .param p3    # Lcom/android/mail/ui/DividedImageCanvas;
    .param p4    # I

    const/4 v0, 0x0

    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v8

    if-nez v8, :cond_1

    move-object v3, p1

    :goto_0
    const/4 v8, 0x0

    const/4 v9, 0x1

    invoke-virtual {v3, v8, v9}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v5

    invoke-direct {p0, v5}, Lcom/android/mail/photomanager/LetterTileProvider;->isLetter(Ljava/lang/String;)Z

    move-result v8

    if-eqz v8, :cond_2

    sget v8, Lcom/android/mail/photomanager/LetterTileProvider;->sTileLetterFontSize:I

    const/4 v9, -0x1

    if-ne v8, v9, :cond_0

    invoke-virtual {p3}, Lcom/android/mail/ui/DividedImageCanvas;->getContext()Landroid/content/Context;

    move-result-object v8

    invoke-virtual {v8}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v7

    const v8, 0x7f0c006b

    invoke-virtual {v7, v8}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v8

    sput v8, Lcom/android/mail/photomanager/LetterTileProvider;->sTileLetterFontSize:I

    const v8, 0x7f0c006d

    invoke-virtual {v7, v8}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v8

    sput v8, Lcom/android/mail/photomanager/LetterTileProvider;->sTileLetterFontSizeSmall:I

    const v8, 0x7f0a002d

    invoke-virtual {v7, v8}, Landroid/content/res/Resources;->getColor(I)I

    move-result v8

    sput v8, Lcom/android/mail/photomanager/LetterTileProvider;->sTileColor:I

    const v8, 0x7f0a002e

    invoke-virtual {v7, v8}, Landroid/content/res/Resources;->getColor(I)I

    move-result v8

    sput v8, Lcom/android/mail/photomanager/LetterTileProvider;->sTileFontColor:I

    const-string v8, "sans-serif-light"

    const/4 v9, 0x0

    invoke-static {v8, v9}, Landroid/graphics/Typeface;->create(Ljava/lang/String;I)Landroid/graphics/Typeface;

    move-result-object v8

    sput-object v8, Lcom/android/mail/photomanager/LetterTileProvider;->sSansSerifLight:Landroid/graphics/Typeface;

    new-instance v8, Landroid/graphics/Rect;

    invoke-direct {v8}, Landroid/graphics/Rect;-><init>()V

    sput-object v8, Lcom/android/mail/photomanager/LetterTileProvider;->sBounds:Landroid/graphics/Rect;

    sget-object v8, Lcom/android/mail/photomanager/LetterTileProvider;->sPaint:Landroid/text/TextPaint;

    sget-object v9, Lcom/android/mail/photomanager/LetterTileProvider;->sSansSerifLight:Landroid/graphics/Typeface;

    invoke-virtual {v8, v9}, Landroid/text/TextPaint;->setTypeface(Landroid/graphics/Typeface;)Landroid/graphics/Typeface;

    sget-object v8, Lcom/android/mail/photomanager/LetterTileProvider;->sPaint:Landroid/text/TextPaint;

    sget v9, Lcom/android/mail/photomanager/LetterTileProvider;->sTileFontColor:I

    invoke-virtual {v8, v9}, Landroid/text/TextPaint;->setColor(I)V

    sget-object v8, Lcom/android/mail/photomanager/LetterTileProvider;->sPaint:Landroid/text/TextPaint;

    sget-object v9, Landroid/graphics/Paint$Align;->CENTER:Landroid/graphics/Paint$Align;

    invoke-virtual {v8, v9}, Landroid/text/TextPaint;->setTextAlign(Landroid/graphics/Paint$Align;)V

    sget-object v8, Lcom/android/mail/photomanager/LetterTileProvider;->sPaint:Landroid/text/TextPaint;

    const/4 v9, 0x1

    invoke-virtual {v8, v9}, Landroid/text/TextPaint;->setAntiAlias(Z)V

    const/4 v8, 0x3

    new-array v8, v8, [Landroid/graphics/Bitmap;

    sput-object v8, Lcom/android/mail/photomanager/LetterTileProvider;->sBitmapBackgroundCache:[Landroid/graphics/Bitmap;

    :cond_0
    invoke-virtual {v5}, Ljava/lang/String;->toUpperCase()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p3, p2}, Lcom/android/mail/ui/DividedImageCanvas;->getDesiredDimensions(Ljava/lang/String;)Lcom/android/mail/ui/DividedImageCanvas$Dimensions;

    move-result-object v2

    invoke-direct {p0, v2}, Lcom/android/mail/photomanager/LetterTileProvider;->getBitmap(Lcom/android/mail/ui/DividedImageCanvas$Dimensions;)Landroid/graphics/Bitmap;

    move-result-object v0

    new-instance v1, Landroid/graphics/Canvas;

    invoke-direct {v1, v0}, Landroid/graphics/Canvas;-><init>(Landroid/graphics/Bitmap;)V

    sget v8, Lcom/android/mail/photomanager/LetterTileProvider;->sTileColor:I

    invoke-virtual {v1, v8}, Landroid/graphics/Canvas;->drawColor(I)V

    sget-object v8, Lcom/android/mail/photomanager/LetterTileProvider;->sPaint:Landroid/text/TextPaint;

    iget v9, v2, Lcom/android/mail/ui/DividedImageCanvas$Dimensions;->scale:F

    invoke-direct {p0, v9}, Lcom/android/mail/photomanager/LetterTileProvider;->getFontSize(F)I

    move-result v9

    int-to-float v9, v9

    invoke-virtual {v8, v9}, Landroid/text/TextPaint;->setTextSize(F)V

    sget-object v8, Lcom/android/mail/photomanager/LetterTileProvider;->sPaint:Landroid/text/TextPaint;

    const/4 v9, 0x0

    invoke-virtual {v4}, Ljava/lang/String;->length()I

    move-result v10

    sget-object v11, Lcom/android/mail/photomanager/LetterTileProvider;->sBounds:Landroid/graphics/Rect;

    invoke-virtual {v8, v4, v9, v10, v11}, Landroid/text/TextPaint;->getTextBounds(Ljava/lang/String;IILandroid/graphics/Rect;)V

    iget v8, v2, Lcom/android/mail/ui/DividedImageCanvas$Dimensions;->width:I

    div-int/lit8 v8, v8, 0x2

    add-int/lit8 v8, v8, 0x0

    int-to-float v8, v8

    iget v9, v2, Lcom/android/mail/ui/DividedImageCanvas$Dimensions;->height:I

    div-int/lit8 v9, v9, 0x2

    add-int/lit8 v9, v9, 0x0

    sget-object v10, Lcom/android/mail/photomanager/LetterTileProvider;->sBounds:Landroid/graphics/Rect;

    iget v10, v10, Landroid/graphics/Rect;->bottom:I

    sget-object v11, Lcom/android/mail/photomanager/LetterTileProvider;->sBounds:Landroid/graphics/Rect;

    iget v11, v11, Landroid/graphics/Rect;->top:I

    sub-int/2addr v10, v11

    div-int/lit8 v10, v10, 0x2

    add-int/2addr v9, v10

    int-to-float v9, v9

    sget-object v10, Lcom/android/mail/photomanager/LetterTileProvider;->sPaint:Landroid/text/TextPaint;

    invoke-virtual {v1, v4, v8, v9, v10}, Landroid/graphics/Canvas;->drawText(Ljava/lang/String;FFLandroid/graphics/Paint;)V

    :goto_1
    invoke-virtual {p3, v0, p2}, Lcom/android/mail/ui/DividedImageCanvas;->addDivisionImage(Landroid/graphics/Bitmap;Ljava/lang/String;)V

    return-void

    :cond_1
    move-object v3, p2

    goto/16 :goto_0

    :cond_2
    iget-object v8, p0, Lcom/android/mail/photomanager/LetterTileProvider;->mDefaultBitmap:Landroid/graphics/Bitmap;

    if-nez v8, :cond_3

    new-instance v6, Landroid/graphics/BitmapFactory$Options;

    invoke-direct {v6}, Landroid/graphics/BitmapFactory$Options;-><init>()V

    const/4 v8, 0x1

    iput-boolean v8, v6, Landroid/graphics/BitmapFactory$Options;->inMutable:Z

    invoke-virtual {p3}, Lcom/android/mail/ui/DividedImageCanvas;->getContext()Landroid/content/Context;

    move-result-object v8

    invoke-virtual {v8}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v8

    sget v9, Lcom/android/mail/photomanager/LetterTileProvider;->DEFAULT_AVATAR_DRAWABLE:I

    invoke-static {v8, v9, v6}, Landroid/graphics/BitmapFactory;->decodeResource(Landroid/content/res/Resources;ILandroid/graphics/BitmapFactory$Options;)Landroid/graphics/Bitmap;

    move-result-object v8

    iput-object v8, p0, Lcom/android/mail/photomanager/LetterTileProvider;->mDefaultBitmap:Landroid/graphics/Bitmap;

    :cond_3
    iget-object v0, p0, Lcom/android/mail/photomanager/LetterTileProvider;->mDefaultBitmap:Landroid/graphics/Bitmap;

    goto :goto_1
.end method
