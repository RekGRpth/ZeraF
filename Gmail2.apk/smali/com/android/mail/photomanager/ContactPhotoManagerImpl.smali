.class Lcom/android/mail/photomanager/ContactPhotoManagerImpl;
.super Lcom/android/mail/photomanager/ContactPhotoManager;
.source "ContactPhotoManager.java"

# interfaces
.implements Landroid/os/Handler$Callback;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/mail/photomanager/ContactPhotoManagerImpl$Request;,
        Lcom/android/mail/photomanager/ContactPhotoManagerImpl$LoaderThread;,
        Lcom/android/mail/photomanager/ContactPhotoManagerImpl$BitmapHolder;
    }
.end annotation


# static fields
.field private static final COLUMNS:[Ljava/lang/String;

.field private static final EMPTY_STRING_ARRAY:[Ljava/lang/String;


# instance fields
.field private final mBitmapCache:Landroid/util/LruCache;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/util/LruCache",
            "<",
            "Ljava/lang/Object;",
            "Landroid/graphics/Bitmap;",
            ">;"
        }
    .end annotation
.end field

.field private final mBitmapHolderCache:Landroid/util/LruCache;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/util/LruCache",
            "<",
            "Ljava/lang/Object;",
            "Lcom/android/mail/photomanager/ContactPhotoManagerImpl$BitmapHolder;",
            ">;"
        }
    .end annotation
.end field

.field private volatile mBitmapHolderCacheAllUnfresh:Z

.field private final mBitmapHolderCacheRedZoneBytes:I

.field private final mContext:Landroid/content/Context;

.field private final mFreshCacheOverwrite:Ljava/util/concurrent/atomic/AtomicInteger;

.field private mLoaderThread:Lcom/android/mail/photomanager/ContactPhotoManagerImpl$LoaderThread;

.field private mLoadingRequested:Z

.field private final mMainThreadHandler:Landroid/os/Handler;

.field private mPaused:Z

.field private final mPendingRequests:Ljava/util/concurrent/ConcurrentHashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/concurrent/ConcurrentHashMap",
            "<",
            "Ljava/lang/Long;",
            "Lcom/android/mail/photomanager/ContactPhotoManagerImpl$Request;",
            ">;"
        }
    .end annotation
.end field

.field private final mPhotoIdCache:Landroid/util/LruCache;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/util/LruCache",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation
.end field

.field private final mStaleCacheOverwrite:Ljava/util/concurrent/atomic/AtomicInteger;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    const/4 v2, 0x0

    new-array v0, v2, [Ljava/lang/String;

    sput-object v0, Lcom/android/mail/photomanager/ContactPhotoManagerImpl;->EMPTY_STRING_ARRAY:[Ljava/lang/String;

    const/4 v0, 0x2

    new-array v0, v0, [Ljava/lang/String;

    const-string v1, "_id"

    aput-object v1, v0, v2

    const/4 v1, 0x1

    const-string v2, "data15"

    aput-object v2, v0, v1

    sput-object v0, Lcom/android/mail/photomanager/ContactPhotoManagerImpl;->COLUMNS:[Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 7
    .param p1    # Landroid/content/Context;

    invoke-direct {p0}, Lcom/android/mail/photomanager/ContactPhotoManager;-><init>()V

    const/4 v3, 0x1

    iput-boolean v3, p0, Lcom/android/mail/photomanager/ContactPhotoManagerImpl;->mBitmapHolderCacheAllUnfresh:Z

    new-instance v3, Ljava/util/concurrent/ConcurrentHashMap;

    invoke-direct {v3}, Ljava/util/concurrent/ConcurrentHashMap;-><init>()V

    iput-object v3, p0, Lcom/android/mail/photomanager/ContactPhotoManagerImpl;->mPendingRequests:Ljava/util/concurrent/ConcurrentHashMap;

    new-instance v3, Landroid/os/Handler;

    invoke-direct {v3, p0}, Landroid/os/Handler;-><init>(Landroid/os/Handler$Callback;)V

    iput-object v3, p0, Lcom/android/mail/photomanager/ContactPhotoManagerImpl;->mMainThreadHandler:Landroid/os/Handler;

    new-instance v3, Ljava/util/concurrent/atomic/AtomicInteger;

    invoke-direct {v3}, Ljava/util/concurrent/atomic/AtomicInteger;-><init>()V

    iput-object v3, p0, Lcom/android/mail/photomanager/ContactPhotoManagerImpl;->mStaleCacheOverwrite:Ljava/util/concurrent/atomic/AtomicInteger;

    new-instance v3, Ljava/util/concurrent/atomic/AtomicInteger;

    invoke-direct {v3}, Ljava/util/concurrent/atomic/AtomicInteger;-><init>()V

    iput-object v3, p0, Lcom/android/mail/photomanager/ContactPhotoManagerImpl;->mFreshCacheOverwrite:Ljava/util/concurrent/atomic/AtomicInteger;

    iput-object p1, p0, Lcom/android/mail/photomanager/ContactPhotoManagerImpl;->mContext:Landroid/content/Context;

    invoke-static {}, Lcom/android/mail/photomanager/MemoryUtils;->getTotalMemorySize()J

    move-result-wide v3

    const-wide/32 v5, 0x28000000

    cmp-long v3, v3, v5

    if-ltz v3, :cond_0

    const/high16 v1, 0x3f800000

    :goto_0
    const/high16 v3, 0x49d80000

    mul-float/2addr v3, v1

    float-to-int v0, v3

    new-instance v3, Lcom/android/mail/photomanager/ContactPhotoManagerImpl$1;

    invoke-direct {v3, p0, v0}, Lcom/android/mail/photomanager/ContactPhotoManagerImpl$1;-><init>(Lcom/android/mail/photomanager/ContactPhotoManagerImpl;I)V

    iput-object v3, p0, Lcom/android/mail/photomanager/ContactPhotoManagerImpl;->mBitmapCache:Landroid/util/LruCache;

    const v3, 0x49f42400

    mul-float/2addr v3, v1

    float-to-int v2, v3

    new-instance v3, Lcom/android/mail/photomanager/ContactPhotoManagerImpl$2;

    invoke-direct {v3, p0, v2}, Lcom/android/mail/photomanager/ContactPhotoManagerImpl$2;-><init>(Lcom/android/mail/photomanager/ContactPhotoManagerImpl;I)V

    iput-object v3, p0, Lcom/android/mail/photomanager/ContactPhotoManagerImpl;->mBitmapHolderCache:Landroid/util/LruCache;

    int-to-double v3, v2

    const-wide/high16 v5, 0x3fe8000000000000L

    mul-double/2addr v3, v5

    double-to-int v3, v3

    iput v3, p0, Lcom/android/mail/photomanager/ContactPhotoManagerImpl;->mBitmapHolderCacheRedZoneBytes:I

    const-string v3, "ContactPhotoManager"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Cache adj: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    const/4 v5, 0x0

    new-array v5, v5, [Ljava/lang/Object;

    invoke-static {v3, v4, v5}, Lcom/android/mail/utils/LogUtils;->i(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)I

    new-instance v3, Landroid/util/LruCache;

    const/16 v4, 0x1f4

    invoke-direct {v3, v4}, Landroid/util/LruCache;-><init>(I)V

    iput-object v3, p0, Lcom/android/mail/photomanager/ContactPhotoManagerImpl;->mPhotoIdCache:Landroid/util/LruCache;

    return-void

    :cond_0
    const/high16 v1, 0x3f000000

    goto :goto_0
.end method

.method static synthetic access$000(Lcom/android/mail/photomanager/ContactPhotoManagerImpl;)Landroid/util/LruCache;
    .locals 1
    .param p0    # Lcom/android/mail/photomanager/ContactPhotoManagerImpl;

    iget-object v0, p0, Lcom/android/mail/photomanager/ContactPhotoManagerImpl;->mBitmapHolderCache:Landroid/util/LruCache;

    return-object v0
.end method

.method static synthetic access$100(Lcom/android/mail/photomanager/ContactPhotoManagerImpl;)I
    .locals 1
    .param p0    # Lcom/android/mail/photomanager/ContactPhotoManagerImpl;

    iget v0, p0, Lcom/android/mail/photomanager/ContactPhotoManagerImpl;->mBitmapHolderCacheRedZoneBytes:I

    return v0
.end method

.method static synthetic access$200(Lcom/android/mail/photomanager/ContactPhotoManagerImpl;Ljava/util/Set;Ljava/util/Set;Ljava/util/Set;Ljava/util/Set;)V
    .locals 0
    .param p0    # Lcom/android/mail/photomanager/ContactPhotoManagerImpl;
    .param p1    # Ljava/util/Set;
    .param p2    # Ljava/util/Set;
    .param p3    # Ljava/util/Set;
    .param p4    # Ljava/util/Set;

    invoke-direct {p0, p1, p2, p3, p4}, Lcom/android/mail/photomanager/ContactPhotoManagerImpl;->obtainPhotoIdsAndUrisToLoad(Ljava/util/Set;Ljava/util/Set;Ljava/util/Set;Ljava/util/Set;)V

    return-void
.end method

.method static synthetic access$300(Lcom/android/mail/photomanager/ContactPhotoManagerImpl;Ljava/lang/Object;[BI)V
    .locals 0
    .param p0    # Lcom/android/mail/photomanager/ContactPhotoManagerImpl;
    .param p1    # Ljava/lang/Object;
    .param p2    # [B
    .param p3    # I

    invoke-direct {p0, p1, p2, p3}, Lcom/android/mail/photomanager/ContactPhotoManagerImpl;->cacheBitmap(Ljava/lang/Object;[BI)V

    return-void
.end method

.method static synthetic access$400(Lcom/android/mail/photomanager/ContactPhotoManagerImpl;)Landroid/os/Handler;
    .locals 1
    .param p0    # Lcom/android/mail/photomanager/ContactPhotoManagerImpl;

    iget-object v0, p0, Lcom/android/mail/photomanager/ContactPhotoManagerImpl;->mMainThreadHandler:Landroid/os/Handler;

    return-object v0
.end method

.method static synthetic access$500()[Ljava/lang/String;
    .locals 1

    sget-object v0, Lcom/android/mail/photomanager/ContactPhotoManagerImpl;->COLUMNS:[Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$600()[Ljava/lang/String;
    .locals 1

    sget-object v0, Lcom/android/mail/photomanager/ContactPhotoManagerImpl;->EMPTY_STRING_ARRAY:[Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$700(Lcom/android/mail/photomanager/ContactPhotoManagerImpl;)Landroid/util/LruCache;
    .locals 1
    .param p0    # Lcom/android/mail/photomanager/ContactPhotoManagerImpl;

    iget-object v0, p0, Lcom/android/mail/photomanager/ContactPhotoManagerImpl;->mPhotoIdCache:Landroid/util/LruCache;

    return-object v0
.end method

.method static synthetic access$800(Lcom/android/mail/photomanager/ContactPhotoManagerImpl;Ljava/lang/Long;Ljava/lang/String;)V
    .locals 0
    .param p0    # Lcom/android/mail/photomanager/ContactPhotoManagerImpl;
    .param p1    # Ljava/lang/Long;
    .param p2    # Ljava/lang/String;

    invoke-direct {p0, p1, p2}, Lcom/android/mail/photomanager/ContactPhotoManagerImpl;->cachePhotoId(Ljava/lang/Long;Ljava/lang/String;)V

    return-void
.end method

.method private cacheBitmap(Ljava/lang/Object;[BI)V
    .locals 2
    .param p1    # Ljava/lang/Object;
    .param p2    # [B
    .param p3    # I

    new-instance v0, Lcom/android/mail/photomanager/ContactPhotoManagerImpl$BitmapHolder;

    if-nez p2, :cond_0

    const/4 v1, -0x1

    :goto_0
    invoke-direct {v0, p2, v1}, Lcom/android/mail/photomanager/ContactPhotoManagerImpl$BitmapHolder;-><init>([BI)V

    iget-object v1, p0, Lcom/android/mail/photomanager/ContactPhotoManagerImpl;->mBitmapHolderCache:Landroid/util/LruCache;

    invoke-virtual {v1, p1, v0}, Landroid/util/LruCache;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const/4 v1, 0x0

    iput-boolean v1, p0, Lcom/android/mail/photomanager/ContactPhotoManagerImpl;->mBitmapHolderCacheAllUnfresh:Z

    return-void

    :cond_0
    invoke-static {p2}, Lcom/android/mail/photomanager/BitmapUtil;->getSmallerExtentFromBytes([B)I

    move-result v1

    goto :goto_0
.end method

.method private cachePhotoId(Ljava/lang/Long;Ljava/lang/String;)V
    .locals 1
    .param p1    # Ljava/lang/Long;
    .param p2    # Ljava/lang/String;

    iget-object v0, p0, Lcom/android/mail/photomanager/ContactPhotoManagerImpl;->mPhotoIdCache:Landroid/util/LruCache;

    invoke-virtual {v0, p2, p1}, Landroid/util/LruCache;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    return-void
.end method

.method private loadCachedPhoto(Lcom/android/mail/ui/DividedImageCanvas;Lcom/android/mail/photomanager/ContactPhotoManagerImpl$Request;Z)Z
    .locals 4
    .param p1    # Lcom/android/mail/ui/DividedImageCanvas;
    .param p2    # Lcom/android/mail/photomanager/ContactPhotoManagerImpl$Request;
    .param p3    # Z

    iget-object v2, p0, Lcom/android/mail/photomanager/ContactPhotoManagerImpl;->mBitmapHolderCache:Landroid/util/LruCache;

    invoke-virtual {p2}, Lcom/android/mail/photomanager/ContactPhotoManagerImpl$Request;->getKey()Ljava/lang/Object;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/util/LruCache;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/android/mail/photomanager/ContactPhotoManagerImpl$BitmapHolder;

    if-nez v1, :cond_0

    invoke-virtual {p2, p1}, Lcom/android/mail/photomanager/ContactPhotoManagerImpl$Request;->applyDefaultImage(Lcom/android/mail/ui/DividedImageCanvas;)V

    const/4 v2, 0x0

    :goto_0
    return v2

    :cond_0
    iget-object v2, v1, Lcom/android/mail/photomanager/ContactPhotoManagerImpl$BitmapHolder;->bytes:[B

    if-nez v2, :cond_1

    invoke-virtual {p2, p1}, Lcom/android/mail/photomanager/ContactPhotoManagerImpl$Request;->applyDefaultImage(Lcom/android/mail/ui/DividedImageCanvas;)V

    iget-boolean v2, v1, Lcom/android/mail/photomanager/ContactPhotoManagerImpl$BitmapHolder;->fresh:Z

    goto :goto_0

    :cond_1
    iget-object v2, v1, Lcom/android/mail/photomanager/ContactPhotoManagerImpl$BitmapHolder;->bytes:[B

    invoke-virtual {p2}, Lcom/android/mail/photomanager/ContactPhotoManagerImpl$Request;->getEmailAddress()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1, v2, v3}, Lcom/android/mail/ui/DividedImageCanvas;->addDivisionImage([BLjava/lang/String;)Landroid/graphics/Bitmap;

    move-result-object v0

    if-eqz v0, :cond_2

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->getByteCount()I

    move-result v2

    iget-object v3, p0, Lcom/android/mail/photomanager/ContactPhotoManagerImpl;->mBitmapCache:Landroid/util/LruCache;

    invoke-virtual {v3}, Landroid/util/LruCache;->maxSize()I

    move-result v3

    div-int/lit8 v3, v3, 0x6

    if-ge v2, v3, :cond_2

    iget-object v2, p0, Lcom/android/mail/photomanager/ContactPhotoManagerImpl;->mBitmapCache:Landroid/util/LruCache;

    invoke-virtual {p2}, Lcom/android/mail/photomanager/ContactPhotoManagerImpl$Request;->getKey()Ljava/lang/Object;

    move-result-object v3

    invoke-virtual {v2, v3, v0}, Landroid/util/LruCache;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    :cond_2
    iget-boolean v2, v1, Lcom/android/mail/photomanager/ContactPhotoManagerImpl$BitmapHolder;->fresh:Z

    goto :goto_0
.end method

.method private loadPhotoByIdOrUri(Ljava/lang/Long;Lcom/android/mail/ui/DividedImageCanvas;Lcom/android/mail/photomanager/ContactPhotoManagerImpl$Request;)V
    .locals 2
    .param p1    # Ljava/lang/Long;
    .param p2    # Lcom/android/mail/ui/DividedImageCanvas;
    .param p3    # Lcom/android/mail/photomanager/ContactPhotoManagerImpl$Request;

    const/4 v1, 0x0

    invoke-direct {p0, p2, p3, v1}, Lcom/android/mail/photomanager/ContactPhotoManagerImpl;->loadCachedPhoto(Lcom/android/mail/ui/DividedImageCanvas;Lcom/android/mail/photomanager/ContactPhotoManagerImpl$Request;Z)Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v1, p0, Lcom/android/mail/photomanager/ContactPhotoManagerImpl;->mPendingRequests:Ljava/util/concurrent/ConcurrentHashMap;

    invoke-virtual {v1, p1}, Ljava/util/concurrent/ConcurrentHashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v1, p0, Lcom/android/mail/photomanager/ContactPhotoManagerImpl;->mPendingRequests:Ljava/util/concurrent/ConcurrentHashMap;

    invoke-virtual {v1, p1, p3}, Ljava/util/concurrent/ConcurrentHashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    iget-boolean v1, p0, Lcom/android/mail/photomanager/ContactPhotoManagerImpl;->mPaused:Z

    if-nez v1, :cond_0

    invoke-direct {p0}, Lcom/android/mail/photomanager/ContactPhotoManagerImpl;->requestLoading()V

    goto :goto_0
.end method

.method private obtainPhotoIdsAndUrisToLoad(Ljava/util/Set;Ljava/util/Set;Ljava/util/Set;Ljava/util/Set;)V
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Set",
            "<",
            "Ljava/lang/Long;",
            ">;",
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;",
            "Ljava/util/Set",
            "<",
            "Lcom/android/mail/photomanager/ContactPhotoManagerImpl$Request;",
            ">;",
            "Ljava/util/Set",
            "<",
            "Lcom/android/mail/photomanager/ContactPhotoManagerImpl$Request;",
            ">;)V"
        }
    .end annotation

    invoke-interface {p1}, Ljava/util/Set;->clear()V

    invoke-interface {p2}, Ljava/util/Set;->clear()V

    invoke-interface {p3}, Ljava/util/Set;->clear()V

    invoke-interface {p4}, Ljava/util/Set;->clear()V

    iget-object v3, p0, Lcom/android/mail/photomanager/ContactPhotoManagerImpl;->mPendingRequests:Ljava/util/concurrent/ConcurrentHashMap;

    invoke-virtual {v3}, Ljava/util/concurrent/ConcurrentHashMap;->values()Ljava/util/Collection;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_2

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/android/mail/photomanager/ContactPhotoManagerImpl$Request;

    iget-object v3, p0, Lcom/android/mail/photomanager/ContactPhotoManagerImpl;->mBitmapHolderCache:Landroid/util/LruCache;

    invoke-virtual {v2}, Lcom/android/mail/photomanager/ContactPhotoManagerImpl$Request;->getKey()Ljava/lang/Object;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/util/LruCache;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/mail/photomanager/ContactPhotoManagerImpl$BitmapHolder;

    if-eqz v0, :cond_1

    iget-object v3, v0, Lcom/android/mail/photomanager/ContactPhotoManagerImpl$BitmapHolder;->bytes:[B

    if-eqz v3, :cond_1

    iget-boolean v3, v0, Lcom/android/mail/photomanager/ContactPhotoManagerImpl$BitmapHolder;->fresh:Z

    if-nez v3, :cond_0

    :cond_1
    invoke-interface {p4, v2}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_2
    return-void
.end method

.method private processLoadedImages()V
    .locals 6

    iget-object v4, p0, Lcom/android/mail/photomanager/ContactPhotoManagerImpl;->mPendingRequests:Ljava/util/concurrent/ConcurrentHashMap;

    invoke-virtual {v4}, Ljava/util/concurrent/ConcurrentHashMap;->keySet()Ljava/util/Set;

    move-result-object v4

    invoke-interface {v4}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Long;

    iget-object v4, p0, Lcom/android/mail/photomanager/ContactPhotoManagerImpl;->mPendingRequests:Ljava/util/concurrent/ConcurrentHashMap;

    invoke-virtual {v4, v0}, Ljava/util/concurrent/ConcurrentHashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/android/mail/photomanager/ContactPhotoManagerImpl$Request;

    invoke-virtual {v2}, Lcom/android/mail/photomanager/ContactPhotoManagerImpl$Request;->getView()Lcom/android/mail/ui/DividedImageCanvas;

    move-result-object v4

    const/4 v5, 0x1

    invoke-direct {p0, v4, v2, v5}, Lcom/android/mail/photomanager/ContactPhotoManagerImpl;->loadCachedPhoto(Lcom/android/mail/ui/DividedImageCanvas;Lcom/android/mail/photomanager/ContactPhotoManagerImpl$Request;Z)Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->remove()V

    goto :goto_0

    :cond_1
    iget-object v4, p0, Lcom/android/mail/photomanager/ContactPhotoManagerImpl;->mPendingRequests:Ljava/util/concurrent/ConcurrentHashMap;

    invoke-virtual {v4}, Ljava/util/concurrent/ConcurrentHashMap;->isEmpty()Z

    move-result v4

    if-nez v4, :cond_2

    invoke-direct {p0}, Lcom/android/mail/photomanager/ContactPhotoManagerImpl;->requestLoading()V

    :cond_2
    return-void
.end method

.method private requestLoading()V
    .locals 2

    const/4 v1, 0x1

    iget-boolean v0, p0, Lcom/android/mail/photomanager/ContactPhotoManagerImpl;->mLoadingRequested:Z

    if-nez v0, :cond_0

    iput-boolean v1, p0, Lcom/android/mail/photomanager/ContactPhotoManagerImpl;->mLoadingRequested:Z

    iget-object v0, p0, Lcom/android/mail/photomanager/ContactPhotoManagerImpl;->mMainThreadHandler:Landroid/os/Handler;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    :cond_0
    return-void
.end method


# virtual methods
.method public clear()V
    .locals 1

    iget-object v0, p0, Lcom/android/mail/photomanager/ContactPhotoManagerImpl;->mPendingRequests:Ljava/util/concurrent/ConcurrentHashMap;

    invoke-virtual {v0}, Ljava/util/concurrent/ConcurrentHashMap;->clear()V

    iget-object v0, p0, Lcom/android/mail/photomanager/ContactPhotoManagerImpl;->mBitmapHolderCache:Landroid/util/LruCache;

    invoke-virtual {v0}, Landroid/util/LruCache;->evictAll()V

    iget-object v0, p0, Lcom/android/mail/photomanager/ContactPhotoManagerImpl;->mBitmapCache:Landroid/util/LruCache;

    invoke-virtual {v0}, Landroid/util/LruCache;->evictAll()V

    iget-object v0, p0, Lcom/android/mail/photomanager/ContactPhotoManagerImpl;->mPhotoIdCache:Landroid/util/LruCache;

    invoke-virtual {v0}, Landroid/util/LruCache;->evictAll()V

    return-void
.end method

.method public ensureLoaderThread()V
    .locals 2

    iget-object v0, p0, Lcom/android/mail/photomanager/ContactPhotoManagerImpl;->mLoaderThread:Lcom/android/mail/photomanager/ContactPhotoManagerImpl$LoaderThread;

    if-nez v0, :cond_0

    new-instance v0, Lcom/android/mail/photomanager/ContactPhotoManagerImpl$LoaderThread;

    iget-object v1, p0, Lcom/android/mail/photomanager/ContactPhotoManagerImpl;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    invoke-direct {v0, p0, v1}, Lcom/android/mail/photomanager/ContactPhotoManagerImpl$LoaderThread;-><init>(Lcom/android/mail/photomanager/ContactPhotoManagerImpl;Landroid/content/ContentResolver;)V

    iput-object v0, p0, Lcom/android/mail/photomanager/ContactPhotoManagerImpl;->mLoaderThread:Lcom/android/mail/photomanager/ContactPhotoManagerImpl$LoaderThread;

    iget-object v0, p0, Lcom/android/mail/photomanager/ContactPhotoManagerImpl;->mLoaderThread:Lcom/android/mail/photomanager/ContactPhotoManagerImpl$LoaderThread;

    invoke-virtual {v0}, Lcom/android/mail/photomanager/ContactPhotoManagerImpl$LoaderThread;->start()V

    :cond_0
    return-void
.end method

.method public handleMessage(Landroid/os/Message;)Z
    .locals 3
    .param p1    # Landroid/os/Message;

    const/4 v0, 0x1

    const/4 v1, 0x0

    iget v2, p1, Landroid/os/Message;->what:I

    packed-switch v2, :pswitch_data_0

    move v0, v1

    :cond_0
    :goto_0
    return v0

    :pswitch_0
    iput-boolean v1, p0, Lcom/android/mail/photomanager/ContactPhotoManagerImpl;->mLoadingRequested:Z

    iget-boolean v1, p0, Lcom/android/mail/photomanager/ContactPhotoManagerImpl;->mPaused:Z

    if-nez v1, :cond_0

    invoke-virtual {p0}, Lcom/android/mail/photomanager/ContactPhotoManagerImpl;->ensureLoaderThread()V

    iget-object v1, p0, Lcom/android/mail/photomanager/ContactPhotoManagerImpl;->mLoaderThread:Lcom/android/mail/photomanager/ContactPhotoManagerImpl$LoaderThread;

    invoke-virtual {v1}, Lcom/android/mail/photomanager/ContactPhotoManagerImpl$LoaderThread;->requestLoading()V

    goto :goto_0

    :pswitch_1
    iget-boolean v1, p0, Lcom/android/mail/photomanager/ContactPhotoManagerImpl;->mPaused:Z

    if-nez v1, :cond_0

    invoke-direct {p0}, Lcom/android/mail/photomanager/ContactPhotoManagerImpl;->processLoadedImages()V

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public loadThumbnail(Ljava/lang/Long;Lcom/android/mail/ui/DividedImageCanvas;Ljava/lang/String;Ljava/lang/String;Lcom/android/mail/photomanager/ContactPhotoManager$DefaultImageProvider;)V
    .locals 1
    .param p1    # Ljava/lang/Long;
    .param p2    # Lcom/android/mail/ui/DividedImageCanvas;
    .param p3    # Ljava/lang/String;
    .param p4    # Ljava/lang/String;
    .param p5    # Lcom/android/mail/photomanager/ContactPhotoManager$DefaultImageProvider;

    invoke-static {p4}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, -0x1

    invoke-virtual {p5, p3, p4, p2, v0}, Lcom/android/mail/photomanager/ContactPhotoManager$DefaultImageProvider;->applyDefaultImage(Ljava/lang/String;Ljava/lang/String;Lcom/android/mail/ui/DividedImageCanvas;I)V

    iget-object v0, p0, Lcom/android/mail/photomanager/ContactPhotoManagerImpl;->mPendingRequests:Ljava/util/concurrent/ConcurrentHashMap;

    invoke-virtual {v0, p1}, Ljava/util/concurrent/ConcurrentHashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    :goto_0
    return-void

    :cond_0
    invoke-static {p3, p4, p5, p2}, Lcom/android/mail/photomanager/ContactPhotoManagerImpl$Request;->createFromEmailAddress(Ljava/lang/String;Ljava/lang/String;Lcom/android/mail/photomanager/ContactPhotoManager$DefaultImageProvider;Lcom/android/mail/ui/DividedImageCanvas;)Lcom/android/mail/photomanager/ContactPhotoManagerImpl$Request;

    move-result-object v0

    invoke-direct {p0, p1, p2, v0}, Lcom/android/mail/photomanager/ContactPhotoManagerImpl;->loadPhotoByIdOrUri(Ljava/lang/Long;Lcom/android/mail/ui/DividedImageCanvas;Lcom/android/mail/photomanager/ContactPhotoManagerImpl$Request;)V

    goto :goto_0
.end method

.method public onTrimMemory(I)V
    .locals 1
    .param p1    # I

    const/16 v0, 0x3c

    if-lt p1, v0, :cond_0

    invoke-virtual {p0}, Lcom/android/mail/photomanager/ContactPhotoManagerImpl;->clear()V

    :cond_0
    return-void
.end method

.method public removePhoto(Ljava/lang/Long;)V
    .locals 2
    .param p1    # Ljava/lang/Long;

    iget-object v1, p0, Lcom/android/mail/photomanager/ContactPhotoManagerImpl;->mPendingRequests:Ljava/util/concurrent/ConcurrentHashMap;

    invoke-virtual {v1, p1}, Ljava/util/concurrent/ConcurrentHashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/mail/photomanager/ContactPhotoManagerImpl$Request;

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/android/mail/photomanager/ContactPhotoManagerImpl$Request;->getView()Lcom/android/mail/ui/DividedImageCanvas;

    move-result-object v1

    invoke-virtual {v1}, Lcom/android/mail/ui/DividedImageCanvas;->reset()V

    iget-object v1, p0, Lcom/android/mail/photomanager/ContactPhotoManagerImpl;->mPendingRequests:Ljava/util/concurrent/ConcurrentHashMap;

    invoke-virtual {v1, p1}, Ljava/util/concurrent/ConcurrentHashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    :cond_0
    return-void
.end method
