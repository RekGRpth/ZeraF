.class Lcom/android/mail/photomanager/ContactPhotoManagerImpl$BitmapHolder;
.super Ljava/lang/Object;
.source "ContactPhotoManager.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/mail/photomanager/ContactPhotoManagerImpl;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "BitmapHolder"
.end annotation


# instance fields
.field bytes:[B

.field volatile fresh:Z


# direct methods
.method public constructor <init>([BI)V
    .locals 1
    .param p1    # [B
    .param p2    # I

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/android/mail/photomanager/ContactPhotoManagerImpl$BitmapHolder;->bytes:[B

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/android/mail/photomanager/ContactPhotoManagerImpl$BitmapHolder;->fresh:Z

    return-void
.end method
