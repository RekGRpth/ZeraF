.class public Lcom/android/mail/photomanager/BitmapUtil;
.super Ljava/lang/Object;
.source "BitmapUtil.java"


# direct methods
.method private constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static decodeBitmapFromBytes([BII)Landroid/graphics/Bitmap;
    .locals 3
    .param p0    # [B
    .param p1    # I
    .param p2    # I

    new-instance v0, Landroid/graphics/BitmapFactory$Options;

    invoke-direct {v0}, Landroid/graphics/BitmapFactory$Options;-><init>()V

    iput p1, v0, Landroid/graphics/BitmapFactory$Options;->outWidth:I

    iput p2, v0, Landroid/graphics/BitmapFactory$Options;->outHeight:I

    const/4 v1, 0x0

    array-length v2, p0

    invoke-static {p0, v1, v2, v0}, Landroid/graphics/BitmapFactory;->decodeByteArray([BIILandroid/graphics/BitmapFactory$Options;)Landroid/graphics/Bitmap;

    move-result-object v1

    return-object v1
.end method

.method public static getSmallerExtentFromBytes([B)I
    .locals 3
    .param p0    # [B

    new-instance v0, Landroid/graphics/BitmapFactory$Options;

    invoke-direct {v0}, Landroid/graphics/BitmapFactory$Options;-><init>()V

    const/4 v1, 0x1

    iput-boolean v1, v0, Landroid/graphics/BitmapFactory$Options;->inJustDecodeBounds:Z

    const/4 v1, 0x0

    array-length v2, p0

    invoke-static {p0, v1, v2, v0}, Landroid/graphics/BitmapFactory;->decodeByteArray([BIILandroid/graphics/BitmapFactory$Options;)Landroid/graphics/Bitmap;

    iget v1, v0, Landroid/graphics/BitmapFactory$Options;->outWidth:I

    iget v2, v0, Landroid/graphics/BitmapFactory$Options;->outHeight:I

    invoke-static {v1, v2}, Ljava/lang/Math;->min(II)I

    move-result v1

    return v1
.end method

.method public static obtainBitmapWithHalfWidth([BII)Landroid/graphics/Bitmap;
    .locals 12
    .param p0    # [B
    .param p1    # I
    .param p2    # I

    const/high16 v11, 0x40000000

    const/4 v1, 0x0

    if-eqz p0, :cond_3

    array-length v9, p0

    if-lez v9, :cond_3

    div-int/lit8 v9, p1, 0x2

    int-to-float v0, v9

    new-instance v3, Landroid/graphics/BitmapFactory$Options;

    invoke-direct {v3}, Landroid/graphics/BitmapFactory$Options;-><init>()V

    iput p1, v3, Landroid/graphics/BitmapFactory$Options;->outWidth:I

    iput p2, v3, Landroid/graphics/BitmapFactory$Options;->outHeight:I

    const/4 v9, 0x0

    array-length v10, p0

    invoke-static {p0, v9, v10, v3}, Landroid/graphics/BitmapFactory;->decodeByteArray([BIILandroid/graphics/BitmapFactory$Options;)Landroid/graphics/Bitmap;

    move-result-object v4

    invoke-virtual {v4}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v5

    invoke-virtual {v4}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v6

    if-lez v6, :cond_1

    int-to-float v9, v6

    cmpl-float v9, v9, v0

    if-lez v9, :cond_1

    int-to-float v9, v6

    sub-float v2, v9, v0

    :goto_0
    if-lez v5, :cond_0

    if-le v5, p2, :cond_0

    sub-int v9, v5, p2

    int-to-float v1, v9

    :cond_0
    div-float v9, v2, v11

    float-to-int v7, v9

    div-float v9, v1, v11

    float-to-int v8, v9

    if-le p2, v5, :cond_2

    :goto_1
    return-object v4

    :cond_1
    move v2, v1

    goto :goto_0

    :cond_2
    float-to-int v9, v0

    invoke-static {v4, v7, v8, v9, p2}, Landroid/graphics/Bitmap;->createBitmap(Landroid/graphics/Bitmap;IIII)Landroid/graphics/Bitmap;

    move-result-object v4

    goto :goto_1

    :cond_3
    const/4 v4, 0x0

    goto :goto_1
.end method
