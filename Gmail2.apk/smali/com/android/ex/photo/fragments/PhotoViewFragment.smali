.class public Lcom/android/ex/photo/fragments/PhotoViewFragment;
.super Lvedroid/support/v4/app/Fragment;
.source "PhotoViewFragment.java"

# interfaces
.implements Lvedroid/support/v4/app/LoaderManager$LoaderCallbacks;
.implements Landroid/view/View$OnClickListener;
.implements Lcom/android/ex/photo/PhotoViewCallbacks$CursorChangedListener;
.implements Lcom/android/ex/photo/PhotoViewCallbacks$OnScreenListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/ex/photo/fragments/PhotoViewFragment$1;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lvedroid/support/v4/app/Fragment;",
        "Lvedroid/support/v4/app/LoaderManager$LoaderCallbacks",
        "<",
        "Landroid/graphics/Bitmap;",
        ">;",
        "Landroid/view/View$OnClickListener;",
        "Lcom/android/ex/photo/PhotoViewCallbacks$OnScreenListener;",
        "Lcom/android/ex/photo/PhotoViewCallbacks$CursorChangedListener;"
    }
.end annotation


# static fields
.field public static sPhotoSize:Ljava/lang/Integer;


# instance fields
.field protected mAdapter:Lcom/android/ex/photo/adapters/PhotoPagerAdapter;

.field protected mCallback:Lcom/android/ex/photo/PhotoViewCallbacks;

.field protected mEmptyText:Landroid/widget/TextView;

.field protected mFullScreen:Z

.field protected mIntent:Landroid/content/Intent;

.field protected mOnlyShowSpinner:Z

.field protected mPhotoPreviewAndProgress:Landroid/view/View;

.field protected mPhotoPreviewImage:Landroid/widget/ImageView;

.field protected mPhotoProgressBar:Lcom/android/ex/photo/views/ProgressBarWrapper;

.field protected mPhotoView:Lcom/android/ex/photo/views/PhotoView;

.field protected mPosition:I

.field protected mProgressBarNeeded:Z

.field protected mResolvedPhotoUri:Ljava/lang/String;

.field protected mRetryButton:Landroid/widget/ImageView;

.field protected mThumbnailShown:Z

.field protected mThumbnailUri:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Lvedroid/support/v4/app/Fragment;-><init>()V

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/android/ex/photo/fragments/PhotoViewFragment;->mProgressBarNeeded:Z

    return-void
.end method

.method private bindPhoto(Landroid/graphics/Bitmap;)V
    .locals 2
    .param p1    # Landroid/graphics/Bitmap;

    if-eqz p1, :cond_1

    iget-object v0, p0, Lcom/android/ex/photo/fragments/PhotoViewFragment;->mPhotoView:Lcom/android/ex/photo/views/PhotoView;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/ex/photo/fragments/PhotoViewFragment;->mPhotoView:Lcom/android/ex/photo/views/PhotoView;

    invoke-virtual {v0, p1}, Lcom/android/ex/photo/views/PhotoView;->bindPhoto(Landroid/graphics/Bitmap;)V

    :cond_0
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/android/ex/photo/fragments/PhotoViewFragment;->enableImageTransforms(Z)V

    iget-object v0, p0, Lcom/android/ex/photo/fragments/PhotoViewFragment;->mPhotoPreviewAndProgress:Landroid/view/View;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/android/ex/photo/fragments/PhotoViewFragment;->mProgressBarNeeded:Z

    :cond_1
    return-void
.end method

.method public static final newInstance(Landroid/content/Intent;IZ)Lcom/android/ex/photo/fragments/PhotoViewFragment;
    .locals 3
    .param p0    # Landroid/content/Intent;
    .param p1    # I
    .param p2    # Z

    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    const-string v2, "arg-intent"

    invoke-virtual {v0, v2, p0}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    const-string v2, "arg-position"

    invoke-virtual {v0, v2, p1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    const-string v2, "arg-show-spinner"

    invoke-virtual {v0, v2, p2}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    new-instance v1, Lcom/android/ex/photo/fragments/PhotoViewFragment;

    invoke-direct {v1}, Lcom/android/ex/photo/fragments/PhotoViewFragment;-><init>()V

    invoke-virtual {v1, v0}, Lcom/android/ex/photo/fragments/PhotoViewFragment;->setArguments(Landroid/os/Bundle;)V

    return-object v1
.end method

.method private resetPhotoView()V
    .locals 2

    iget-object v0, p0, Lcom/android/ex/photo/fragments/PhotoViewFragment;->mPhotoView:Lcom/android/ex/photo/views/PhotoView;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/ex/photo/fragments/PhotoViewFragment;->mPhotoView:Lcom/android/ex/photo/views/PhotoView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/android/ex/photo/views/PhotoView;->bindPhoto(Landroid/graphics/Bitmap;)V

    :cond_0
    return-void
.end method

.method private setViewVisibility()V
    .locals 2

    iget-object v1, p0, Lcom/android/ex/photo/fragments/PhotoViewFragment;->mCallback:Lcom/android/ex/photo/PhotoViewCallbacks;

    if-nez v1, :cond_0

    const/4 v0, 0x0

    :goto_0
    invoke-virtual {p0, v0}, Lcom/android/ex/photo/fragments/PhotoViewFragment;->setFullScreen(Z)V

    return-void

    :cond_0
    iget-object v1, p0, Lcom/android/ex/photo/fragments/PhotoViewFragment;->mCallback:Lcom/android/ex/photo/PhotoViewCallbacks;

    invoke-interface {v1, p0}, Lcom/android/ex/photo/PhotoViewCallbacks;->isFragmentFullScreen(Lvedroid/support/v4/app/Fragment;)Z

    move-result v0

    goto :goto_0
.end method


# virtual methods
.method public enableImageTransforms(Z)V
    .locals 1
    .param p1    # Z

    iget-object v0, p0, Lcom/android/ex/photo/fragments/PhotoViewFragment;->mPhotoView:Lcom/android/ex/photo/views/PhotoView;

    invoke-virtual {v0, p1}, Lcom/android/ex/photo/views/PhotoView;->enableImageTransforms(Z)V

    return-void
.end method

.method public getEmptyText()Landroid/widget/TextView;
    .locals 1

    iget-object v0, p0, Lcom/android/ex/photo/fragments/PhotoViewFragment;->mEmptyText:Landroid/widget/TextView;

    return-object v0
.end method

.method public getPhotoProgressBar()Lcom/android/ex/photo/views/ProgressBarWrapper;
    .locals 1

    iget-object v0, p0, Lcom/android/ex/photo/fragments/PhotoViewFragment;->mPhotoProgressBar:Lcom/android/ex/photo/views/ProgressBarWrapper;

    return-object v0
.end method

.method public getRetryButton()Landroid/widget/ImageView;
    .locals 1

    iget-object v0, p0, Lcom/android/ex/photo/fragments/PhotoViewFragment;->mRetryButton:Landroid/widget/ImageView;

    return-object v0
.end method

.method protected initializeView(Landroid/view/View;)V
    .locals 7
    .param p1    # Landroid/view/View;

    const/4 v6, 0x0

    sget v2, Lcom/android/ex/photo/R$id;->photo_view:I

    invoke-virtual {p1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Lcom/android/ex/photo/views/PhotoView;

    iput-object v2, p0, Lcom/android/ex/photo/fragments/PhotoViewFragment;->mPhotoView:Lcom/android/ex/photo/views/PhotoView;

    iget-object v2, p0, Lcom/android/ex/photo/fragments/PhotoViewFragment;->mPhotoView:Lcom/android/ex/photo/views/PhotoView;

    iget-object v3, p0, Lcom/android/ex/photo/fragments/PhotoViewFragment;->mIntent:Landroid/content/Intent;

    const-string v4, "max_scale"

    const/high16 v5, 0x3f800000

    invoke-virtual {v3, v4, v5}, Landroid/content/Intent;->getFloatExtra(Ljava/lang/String;F)F

    move-result v3

    invoke-virtual {v2, v3}, Lcom/android/ex/photo/views/PhotoView;->setMaxInitialScale(F)V

    iget-object v2, p0, Lcom/android/ex/photo/fragments/PhotoViewFragment;->mPhotoView:Lcom/android/ex/photo/views/PhotoView;

    invoke-virtual {v2, p0}, Lcom/android/ex/photo/views/PhotoView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v2, p0, Lcom/android/ex/photo/fragments/PhotoViewFragment;->mPhotoView:Lcom/android/ex/photo/views/PhotoView;

    iget-boolean v3, p0, Lcom/android/ex/photo/fragments/PhotoViewFragment;->mFullScreen:Z

    invoke-virtual {v2, v3, v6}, Lcom/android/ex/photo/views/PhotoView;->setFullScreen(ZZ)V

    iget-object v2, p0, Lcom/android/ex/photo/fragments/PhotoViewFragment;->mPhotoView:Lcom/android/ex/photo/views/PhotoView;

    invoke-virtual {v2, v6}, Lcom/android/ex/photo/views/PhotoView;->enableImageTransforms(Z)V

    sget v2, Lcom/android/ex/photo/R$id;->photo_preview:I

    invoke-virtual {p1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    iput-object v2, p0, Lcom/android/ex/photo/fragments/PhotoViewFragment;->mPhotoPreviewAndProgress:Landroid/view/View;

    sget v2, Lcom/android/ex/photo/R$id;->photo_preview_image:I

    invoke-virtual {p1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/ImageView;

    iput-object v2, p0, Lcom/android/ex/photo/fragments/PhotoViewFragment;->mPhotoPreviewImage:Landroid/widget/ImageView;

    iput-boolean v6, p0, Lcom/android/ex/photo/fragments/PhotoViewFragment;->mThumbnailShown:Z

    sget v2, Lcom/android/ex/photo/R$id;->indeterminate_progress:I

    invoke-virtual {p1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ProgressBar;

    sget v2, Lcom/android/ex/photo/R$id;->determinate_progress:I

    invoke-virtual {p1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ProgressBar;

    new-instance v2, Lcom/android/ex/photo/views/ProgressBarWrapper;

    const/4 v3, 0x1

    invoke-direct {v2, v0, v1, v3}, Lcom/android/ex/photo/views/ProgressBarWrapper;-><init>(Landroid/widget/ProgressBar;Landroid/widget/ProgressBar;Z)V

    iput-object v2, p0, Lcom/android/ex/photo/fragments/PhotoViewFragment;->mPhotoProgressBar:Lcom/android/ex/photo/views/ProgressBarWrapper;

    sget v2, Lcom/android/ex/photo/R$id;->empty_text:I

    invoke-virtual {p1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    iput-object v2, p0, Lcom/android/ex/photo/fragments/PhotoViewFragment;->mEmptyText:Landroid/widget/TextView;

    sget v2, Lcom/android/ex/photo/R$id;->retry_button:I

    invoke-virtual {p1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/ImageView;

    iput-object v2, p0, Lcom/android/ex/photo/fragments/PhotoViewFragment;->mRetryButton:Landroid/widget/ImageView;

    return-void
.end method

.method public isPhotoBound()Z
    .locals 1

    iget-object v0, p0, Lcom/android/ex/photo/fragments/PhotoViewFragment;->mPhotoView:Lcom/android/ex/photo/views/PhotoView;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/ex/photo/fragments/PhotoViewFragment;->mPhotoView:Lcom/android/ex/photo/views/PhotoView;

    invoke-virtual {v0}, Lcom/android/ex/photo/views/PhotoView;->isPhotoBound()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isProgressBarNeeded()Z
    .locals 1

    iget-boolean v0, p0, Lcom/android/ex/photo/fragments/PhotoViewFragment;->mProgressBarNeeded:Z

    return v0
.end method

.method public onActivityCreated(Landroid/os/Bundle;)V
    .locals 2
    .param p1    # Landroid/os/Bundle;

    invoke-super {p0, p1}, Lvedroid/support/v4/app/Fragment;->onActivityCreated(Landroid/os/Bundle;)V

    invoke-virtual {p0}, Lcom/android/ex/photo/fragments/PhotoViewFragment;->getActivity()Lvedroid/support/v4/app/FragmentActivity;

    move-result-object v0

    check-cast v0, Lcom/android/ex/photo/PhotoViewCallbacks;

    iput-object v0, p0, Lcom/android/ex/photo/fragments/PhotoViewFragment;->mCallback:Lcom/android/ex/photo/PhotoViewCallbacks;

    iget-object v0, p0, Lcom/android/ex/photo/fragments/PhotoViewFragment;->mCallback:Lcom/android/ex/photo/PhotoViewCallbacks;

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Activity must be a derived class of PhotoViewActivity"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    iget-object v0, p0, Lcom/android/ex/photo/fragments/PhotoViewFragment;->mCallback:Lcom/android/ex/photo/PhotoViewCallbacks;

    invoke-interface {v0}, Lcom/android/ex/photo/PhotoViewCallbacks;->getAdapter()Lcom/android/ex/photo/adapters/PhotoPagerAdapter;

    move-result-object v0

    iput-object v0, p0, Lcom/android/ex/photo/fragments/PhotoViewFragment;->mAdapter:Lcom/android/ex/photo/adapters/PhotoPagerAdapter;

    iget-object v0, p0, Lcom/android/ex/photo/fragments/PhotoViewFragment;->mAdapter:Lcom/android/ex/photo/adapters/PhotoPagerAdapter;

    if-nez v0, :cond_1

    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Callback reported null adapter"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_1
    invoke-direct {p0}, Lcom/android/ex/photo/fragments/PhotoViewFragment;->setViewVisibility()V

    return-void
.end method

.method public onClick(Landroid/view/View;)V
    .locals 1
    .param p1    # Landroid/view/View;

    iget-object v0, p0, Lcom/android/ex/photo/fragments/PhotoViewFragment;->mCallback:Lcom/android/ex/photo/PhotoViewCallbacks;

    invoke-interface {v0}, Lcom/android/ex/photo/PhotoViewCallbacks;->toggleFullScreen()V

    return-void
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 7
    .param p1    # Landroid/os/Bundle;

    invoke-super {p0, p1}, Lvedroid/support/v4/app/Fragment;->onCreate(Landroid/os/Bundle;)V

    sget-object v5, Lcom/android/ex/photo/fragments/PhotoViewFragment;->sPhotoSize:Ljava/lang/Integer;

    if-nez v5, :cond_0

    new-instance v2, Landroid/util/DisplayMetrics;

    invoke-direct {v2}, Landroid/util/DisplayMetrics;-><init>()V

    invoke-virtual {p0}, Lcom/android/ex/photo/fragments/PhotoViewFragment;->getActivity()Lvedroid/support/v4/app/FragmentActivity;

    move-result-object v5

    const-string v6, "window"

    invoke-virtual {v5, v6}, Lvedroid/support/v4/app/FragmentActivity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Landroid/view/WindowManager;

    sget-object v1, Lcom/android/ex/photo/util/ImageUtils;->sUseImageSize:Lcom/android/ex/photo/util/ImageUtils$ImageSize;

    invoke-interface {v4}, Landroid/view/WindowManager;->getDefaultDisplay()Landroid/view/Display;

    move-result-object v5

    invoke-virtual {v5, v2}, Landroid/view/Display;->getMetrics(Landroid/util/DisplayMetrics;)V

    sget-object v5, Lcom/android/ex/photo/fragments/PhotoViewFragment$1;->$SwitchMap$com$android$ex$photo$util$ImageUtils$ImageSize:[I

    invoke-virtual {v1}, Lcom/android/ex/photo/util/ImageUtils$ImageSize;->ordinal()I

    move-result v6

    aget v5, v5, v6

    packed-switch v5, :pswitch_data_0

    iget v5, v2, Landroid/util/DisplayMetrics;->heightPixels:I

    iget v6, v2, Landroid/util/DisplayMetrics;->widthPixels:I

    invoke-static {v5, v6}, Ljava/lang/Math;->min(II)I

    move-result v5

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    sput-object v5, Lcom/android/ex/photo/fragments/PhotoViewFragment;->sPhotoSize:Ljava/lang/Integer;

    :cond_0
    :goto_0
    invoke-virtual {p0}, Lcom/android/ex/photo/fragments/PhotoViewFragment;->getArguments()Landroid/os/Bundle;

    move-result-object v0

    if-nez v0, :cond_2

    :cond_1
    :goto_1
    return-void

    :pswitch_0
    iget v5, v2, Landroid/util/DisplayMetrics;->heightPixels:I

    iget v6, v2, Landroid/util/DisplayMetrics;->widthPixels:I

    invoke-static {v5, v6}, Ljava/lang/Math;->min(II)I

    move-result v5

    mul-int/lit16 v5, v5, 0x320

    div-int/lit16 v5, v5, 0x3e8

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    sput-object v5, Lcom/android/ex/photo/fragments/PhotoViewFragment;->sPhotoSize:Ljava/lang/Integer;

    goto :goto_0

    :cond_2
    const-string v5, "arg-intent"

    invoke-virtual {v0, v5}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v5

    check-cast v5, Landroid/content/Intent;

    iput-object v5, p0, Lcom/android/ex/photo/fragments/PhotoViewFragment;->mIntent:Landroid/content/Intent;

    const-string v5, "arg-position"

    invoke-virtual {v0, v5}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v5

    iput v5, p0, Lcom/android/ex/photo/fragments/PhotoViewFragment;->mPosition:I

    const-string v5, "arg-show-spinner"

    invoke-virtual {v0, v5}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v5

    iput-boolean v5, p0, Lcom/android/ex/photo/fragments/PhotoViewFragment;->mOnlyShowSpinner:Z

    const/4 v5, 0x1

    iput-boolean v5, p0, Lcom/android/ex/photo/fragments/PhotoViewFragment;->mProgressBarNeeded:Z

    if-eqz p1, :cond_3

    const-string v5, "com.android.mail.photo.fragments.PhotoViewFragment.INTENT"

    invoke-virtual {p1, v5}, Landroid/os/Bundle;->getBundle(Ljava/lang/String;)Landroid/os/Bundle;

    move-result-object v3

    if-eqz v3, :cond_3

    new-instance v5, Landroid/content/Intent;

    invoke-direct {v5}, Landroid/content/Intent;-><init>()V

    invoke-virtual {v5, v3}, Landroid/content/Intent;->putExtras(Landroid/os/Bundle;)Landroid/content/Intent;

    move-result-object v5

    iput-object v5, p0, Lcom/android/ex/photo/fragments/PhotoViewFragment;->mIntent:Landroid/content/Intent;

    :cond_3
    iget-object v5, p0, Lcom/android/ex/photo/fragments/PhotoViewFragment;->mIntent:Landroid/content/Intent;

    if-eqz v5, :cond_1

    iget-object v5, p0, Lcom/android/ex/photo/fragments/PhotoViewFragment;->mIntent:Landroid/content/Intent;

    const-string v6, "resolved_photo_uri"

    invoke-virtual {v5, v6}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    iput-object v5, p0, Lcom/android/ex/photo/fragments/PhotoViewFragment;->mResolvedPhotoUri:Ljava/lang/String;

    iget-object v5, p0, Lcom/android/ex/photo/fragments/PhotoViewFragment;->mIntent:Landroid/content/Intent;

    const-string v6, "thumbnail_uri"

    invoke-virtual {v5, v6}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    iput-object v5, p0, Lcom/android/ex/photo/fragments/PhotoViewFragment;->mThumbnailUri:Ljava/lang/String;

    goto :goto_1

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
    .end packed-switch
.end method

.method public onCreateLoader(ILandroid/os/Bundle;)Lvedroid/support/v4/content/Loader;
    .locals 3
    .param p1    # I
    .param p2    # Landroid/os/Bundle;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Landroid/os/Bundle;",
            ")",
            "Lvedroid/support/v4/content/Loader",
            "<",
            "Landroid/graphics/Bitmap;",
            ">;"
        }
    .end annotation

    const/4 v0, 0x0

    iget-boolean v1, p0, Lcom/android/ex/photo/fragments/PhotoViewFragment;->mOnlyShowSpinner:Z

    if-eqz v1, :cond_0

    :goto_0
    return-object v0

    :cond_0
    packed-switch p1, :pswitch_data_0

    goto :goto_0

    :pswitch_0
    new-instance v0, Lcom/android/ex/photo/loaders/PhotoBitmapLoader;

    invoke-virtual {p0}, Lcom/android/ex/photo/fragments/PhotoViewFragment;->getActivity()Lvedroid/support/v4/app/FragmentActivity;

    move-result-object v1

    iget-object v2, p0, Lcom/android/ex/photo/fragments/PhotoViewFragment;->mResolvedPhotoUri:Ljava/lang/String;

    invoke-direct {v0, v1, v2}, Lcom/android/ex/photo/loaders/PhotoBitmapLoader;-><init>(Landroid/content/Context;Ljava/lang/String;)V

    goto :goto_0

    :pswitch_1
    new-instance v0, Lcom/android/ex/photo/loaders/PhotoBitmapLoader;

    invoke-virtual {p0}, Lcom/android/ex/photo/fragments/PhotoViewFragment;->getActivity()Lvedroid/support/v4/app/FragmentActivity;

    move-result-object v1

    iget-object v2, p0, Lcom/android/ex/photo/fragments/PhotoViewFragment;->mThumbnailUri:Ljava/lang/String;

    invoke-direct {v0, v1, v2}, Lcom/android/ex/photo/loaders/PhotoBitmapLoader;-><init>(Landroid/content/Context;Ljava/lang/String;)V

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 3
    .param p1    # Landroid/view/LayoutInflater;
    .param p2    # Landroid/view/ViewGroup;
    .param p3    # Landroid/os/Bundle;

    sget v1, Lcom/android/ex/photo/R$layout;->photo_fragment_view:I

    const/4 v2, 0x0

    invoke-virtual {p1, v1, p2, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/android/ex/photo/fragments/PhotoViewFragment;->initializeView(Landroid/view/View;)V

    return-object v0
.end method

.method public onCursorChanged(Landroid/database/Cursor;)V
    .locals 5
    .param p1    # Landroid/database/Cursor;

    iget-object v4, p0, Lcom/android/ex/photo/fragments/PhotoViewFragment;->mAdapter:Lcom/android/ex/photo/adapters/PhotoPagerAdapter;

    if-nez v4, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget v4, p0, Lcom/android/ex/photo/fragments/PhotoViewFragment;->mPosition:I

    invoke-interface {p1, v4}, Landroid/database/Cursor;->moveToPosition(I)Z

    move-result v4

    if-eqz v4, :cond_0

    invoke-virtual {p0}, Lcom/android/ex/photo/fragments/PhotoViewFragment;->isPhotoBound()Z

    move-result v4

    if-nez v4, :cond_0

    iget-object v4, p0, Lcom/android/ex/photo/fragments/PhotoViewFragment;->mCallback:Lcom/android/ex/photo/PhotoViewCallbacks;

    invoke-interface {v4, p0, p1}, Lcom/android/ex/photo/PhotoViewCallbacks;->onCursorChanged(Lcom/android/ex/photo/fragments/PhotoViewFragment;Landroid/database/Cursor;)V

    invoke-virtual {p0}, Lcom/android/ex/photo/fragments/PhotoViewFragment;->getLoaderManager()Lvedroid/support/v4/app/LoaderManager;

    move-result-object v3

    const/4 v4, 0x1

    invoke-virtual {v3, v4}, Lvedroid/support/v4/app/LoaderManager;->getLoader(I)Lvedroid/support/v4/content/Loader;

    move-result-object v0

    if-eqz v0, :cond_2

    move-object v2, v0

    check-cast v2, Lcom/android/ex/photo/loaders/PhotoBitmapLoader;

    iget-object v4, p0, Lcom/android/ex/photo/fragments/PhotoViewFragment;->mAdapter:Lcom/android/ex/photo/adapters/PhotoPagerAdapter;

    invoke-virtual {v4, p1}, Lcom/android/ex/photo/adapters/PhotoPagerAdapter;->getPhotoUri(Landroid/database/Cursor;)Ljava/lang/String;

    move-result-object v4

    iput-object v4, p0, Lcom/android/ex/photo/fragments/PhotoViewFragment;->mResolvedPhotoUri:Ljava/lang/String;

    iget-object v4, p0, Lcom/android/ex/photo/fragments/PhotoViewFragment;->mResolvedPhotoUri:Ljava/lang/String;

    invoke-virtual {v2, v4}, Lcom/android/ex/photo/loaders/PhotoBitmapLoader;->setPhotoUri(Ljava/lang/String;)V

    invoke-virtual {v2}, Lcom/android/ex/photo/loaders/PhotoBitmapLoader;->forceLoad()V

    :cond_2
    iget-boolean v4, p0, Lcom/android/ex/photo/fragments/PhotoViewFragment;->mThumbnailShown:Z

    if-nez v4, :cond_0

    const/4 v4, 0x2

    invoke-virtual {v3, v4}, Lvedroid/support/v4/app/LoaderManager;->getLoader(I)Lvedroid/support/v4/content/Loader;

    move-result-object v1

    if-eqz v1, :cond_0

    move-object v2, v1

    check-cast v2, Lcom/android/ex/photo/loaders/PhotoBitmapLoader;

    iget-object v4, p0, Lcom/android/ex/photo/fragments/PhotoViewFragment;->mAdapter:Lcom/android/ex/photo/adapters/PhotoPagerAdapter;

    invoke-virtual {v4, p1}, Lcom/android/ex/photo/adapters/PhotoPagerAdapter;->getThumbnailUri(Landroid/database/Cursor;)Ljava/lang/String;

    move-result-object v4

    iput-object v4, p0, Lcom/android/ex/photo/fragments/PhotoViewFragment;->mThumbnailUri:Ljava/lang/String;

    iget-object v4, p0, Lcom/android/ex/photo/fragments/PhotoViewFragment;->mThumbnailUri:Ljava/lang/String;

    invoke-virtual {v2, v4}, Lcom/android/ex/photo/loaders/PhotoBitmapLoader;->setPhotoUri(Ljava/lang/String;)V

    invoke-virtual {v2}, Lcom/android/ex/photo/loaders/PhotoBitmapLoader;->forceLoad()V

    goto :goto_0
.end method

.method public onDestroyView()V
    .locals 1

    iget-object v0, p0, Lcom/android/ex/photo/fragments/PhotoViewFragment;->mPhotoView:Lcom/android/ex/photo/views/PhotoView;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/ex/photo/fragments/PhotoViewFragment;->mPhotoView:Lcom/android/ex/photo/views/PhotoView;

    invoke-virtual {v0}, Lcom/android/ex/photo/views/PhotoView;->clear()V

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/android/ex/photo/fragments/PhotoViewFragment;->mPhotoView:Lcom/android/ex/photo/views/PhotoView;

    :cond_0
    invoke-super {p0}, Lvedroid/support/v4/app/Fragment;->onDestroyView()V

    return-void
.end method

.method public onDetach()V
    .locals 1

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/android/ex/photo/fragments/PhotoViewFragment;->mCallback:Lcom/android/ex/photo/PhotoViewCallbacks;

    invoke-super {p0}, Lvedroid/support/v4/app/Fragment;->onDetach()V

    return-void
.end method

.method public onFullScreenChanged(Z)V
    .locals 0
    .param p1    # Z

    invoke-direct {p0}, Lcom/android/ex/photo/fragments/PhotoViewFragment;->setViewVisibility()V

    return-void
.end method

.method public onInterceptMoveLeft(FF)Z
    .locals 2
    .param p1    # F
    .param p2    # F

    const/4 v0, 0x0

    iget-object v1, p0, Lcom/android/ex/photo/fragments/PhotoViewFragment;->mCallback:Lcom/android/ex/photo/PhotoViewCallbacks;

    invoke-interface {v1, p0}, Lcom/android/ex/photo/PhotoViewCallbacks;->isFragmentActive(Lvedroid/support/v4/app/Fragment;)Z

    move-result v1

    if-nez v1, :cond_1

    :cond_0
    :goto_0
    return v0

    :cond_1
    iget-object v1, p0, Lcom/android/ex/photo/fragments/PhotoViewFragment;->mPhotoView:Lcom/android/ex/photo/views/PhotoView;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/android/ex/photo/fragments/PhotoViewFragment;->mPhotoView:Lcom/android/ex/photo/views/PhotoView;

    invoke-virtual {v1, p1, p2}, Lcom/android/ex/photo/views/PhotoView;->interceptMoveLeft(FF)Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v0, 0x1

    goto :goto_0
.end method

.method public onInterceptMoveRight(FF)Z
    .locals 2
    .param p1    # F
    .param p2    # F

    const/4 v0, 0x0

    iget-object v1, p0, Lcom/android/ex/photo/fragments/PhotoViewFragment;->mCallback:Lcom/android/ex/photo/PhotoViewCallbacks;

    invoke-interface {v1, p0}, Lcom/android/ex/photo/PhotoViewCallbacks;->isFragmentActive(Lvedroid/support/v4/app/Fragment;)Z

    move-result v1

    if-nez v1, :cond_1

    :cond_0
    :goto_0
    return v0

    :cond_1
    iget-object v1, p0, Lcom/android/ex/photo/fragments/PhotoViewFragment;->mPhotoView:Lcom/android/ex/photo/views/PhotoView;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/android/ex/photo/fragments/PhotoViewFragment;->mPhotoView:Lcom/android/ex/photo/views/PhotoView;

    invoke-virtual {v1, p1, p2}, Lcom/android/ex/photo/views/PhotoView;->interceptMoveRight(FF)Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v0, 0x1

    goto :goto_0
.end method

.method public onLoadFinished(Lvedroid/support/v4/content/Loader;Landroid/graphics/Bitmap;)V
    .locals 4
    .param p2    # Landroid/graphics/Bitmap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lvedroid/support/v4/content/Loader",
            "<",
            "Landroid/graphics/Bitmap;",
            ">;",
            "Landroid/graphics/Bitmap;",
            ")V"
        }
    .end annotation

    const/4 v3, 0x0

    invoke-virtual {p0}, Lcom/android/ex/photo/fragments/PhotoViewFragment;->getView()Landroid/view/View;

    move-result-object v1

    if-nez v1, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    invoke-virtual {p1}, Lvedroid/support/v4/content/Loader;->getId()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    :goto_1
    iget-boolean v1, p0, Lcom/android/ex/photo/fragments/PhotoViewFragment;->mProgressBarNeeded:Z

    if-nez v1, :cond_2

    iget-object v1, p0, Lcom/android/ex/photo/fragments/PhotoViewFragment;->mPhotoProgressBar:Lcom/android/ex/photo/views/ProgressBarWrapper;

    const/16 v2, 0x8

    invoke-virtual {v1, v2}, Lcom/android/ex/photo/views/ProgressBarWrapper;->setVisibility(I)V

    :cond_2
    if-eqz p2, :cond_3

    iget-object v1, p0, Lcom/android/ex/photo/fragments/PhotoViewFragment;->mCallback:Lcom/android/ex/photo/PhotoViewCallbacks;

    iget v2, p0, Lcom/android/ex/photo/fragments/PhotoViewFragment;->mPosition:I

    invoke-interface {v1, v2}, Lcom/android/ex/photo/PhotoViewCallbacks;->onNewPhotoLoaded(I)V

    :cond_3
    invoke-direct {p0}, Lcom/android/ex/photo/fragments/PhotoViewFragment;->setViewVisibility()V

    goto :goto_0

    :pswitch_0
    invoke-virtual {p0}, Lcom/android/ex/photo/fragments/PhotoViewFragment;->isPhotoBound()Z

    move-result v1

    if-nez v1, :cond_0

    if-nez p2, :cond_4

    iget-object v1, p0, Lcom/android/ex/photo/fragments/PhotoViewFragment;->mPhotoPreviewImage:Landroid/widget/ImageView;

    sget v2, Lcom/android/ex/photo/R$drawable;->default_image:I

    invoke-virtual {v1, v2}, Landroid/widget/ImageView;->setImageResource(I)V

    iput-boolean v3, p0, Lcom/android/ex/photo/fragments/PhotoViewFragment;->mThumbnailShown:Z

    :goto_2
    iget-object v1, p0, Lcom/android/ex/photo/fragments/PhotoViewFragment;->mPhotoPreviewImage:Landroid/widget/ImageView;

    invoke-virtual {v1, v3}, Landroid/widget/ImageView;->setVisibility(I)V

    iget-object v1, p0, Lcom/android/ex/photo/fragments/PhotoViewFragment;->mPhotoPreviewImage:Landroid/widget/ImageView;

    sget-object v2, Landroid/widget/ImageView$ScaleType;->CENTER:Landroid/widget/ImageView$ScaleType;

    invoke-virtual {v1, v2}, Landroid/widget/ImageView;->setScaleType(Landroid/widget/ImageView$ScaleType;)V

    invoke-virtual {p0, v3}, Lcom/android/ex/photo/fragments/PhotoViewFragment;->enableImageTransforms(Z)V

    goto :goto_1

    :cond_4
    iget-object v1, p0, Lcom/android/ex/photo/fragments/PhotoViewFragment;->mPhotoPreviewImage:Landroid/widget/ImageView;

    invoke-virtual {v1, p2}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    const/4 v1, 0x1

    iput-boolean v1, p0, Lcom/android/ex/photo/fragments/PhotoViewFragment;->mThumbnailShown:Z

    goto :goto_2

    :pswitch_1
    invoke-direct {p0, p2}, Lcom/android/ex/photo/fragments/PhotoViewFragment;->bindPhoto(Landroid/graphics/Bitmap;)V

    goto :goto_1

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method public bridge synthetic onLoadFinished(Lvedroid/support/v4/content/Loader;Ljava/lang/Object;)V
    .locals 0
    .param p1    # Lvedroid/support/v4/content/Loader;
    .param p2    # Ljava/lang/Object;

    check-cast p2, Landroid/graphics/Bitmap;

    invoke-virtual {p0, p1, p2}, Lcom/android/ex/photo/fragments/PhotoViewFragment;->onLoadFinished(Lvedroid/support/v4/content/Loader;Landroid/graphics/Bitmap;)V

    return-void
.end method

.method public onLoaderReset(Lvedroid/support/v4/content/Loader;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lvedroid/support/v4/content/Loader",
            "<",
            "Landroid/graphics/Bitmap;",
            ">;)V"
        }
    .end annotation

    return-void
.end method

.method public onPause()V
    .locals 2

    iget-object v0, p0, Lcom/android/ex/photo/fragments/PhotoViewFragment;->mCallback:Lcom/android/ex/photo/PhotoViewCallbacks;

    invoke-interface {v0, p0}, Lcom/android/ex/photo/PhotoViewCallbacks;->removeCursorListener(Lcom/android/ex/photo/PhotoViewCallbacks$CursorChangedListener;)V

    iget-object v0, p0, Lcom/android/ex/photo/fragments/PhotoViewFragment;->mCallback:Lcom/android/ex/photo/PhotoViewCallbacks;

    iget v1, p0, Lcom/android/ex/photo/fragments/PhotoViewFragment;->mPosition:I

    invoke-interface {v0, v1}, Lcom/android/ex/photo/PhotoViewCallbacks;->removeScreenListener(I)V

    invoke-direct {p0}, Lcom/android/ex/photo/fragments/PhotoViewFragment;->resetPhotoView()V

    invoke-super {p0}, Lvedroid/support/v4/app/Fragment;->onPause()V

    return-void
.end method

.method public onResume()V
    .locals 4

    const/4 v3, 0x0

    const/4 v2, 0x1

    invoke-super {p0}, Lvedroid/support/v4/app/Fragment;->onResume()V

    iget-object v0, p0, Lcom/android/ex/photo/fragments/PhotoViewFragment;->mCallback:Lcom/android/ex/photo/PhotoViewCallbacks;

    iget v1, p0, Lcom/android/ex/photo/fragments/PhotoViewFragment;->mPosition:I

    invoke-interface {v0, v1, p0}, Lcom/android/ex/photo/PhotoViewCallbacks;->addScreenListener(ILcom/android/ex/photo/PhotoViewCallbacks$OnScreenListener;)V

    iget-object v0, p0, Lcom/android/ex/photo/fragments/PhotoViewFragment;->mCallback:Lcom/android/ex/photo/PhotoViewCallbacks;

    invoke-interface {v0, p0}, Lcom/android/ex/photo/PhotoViewCallbacks;->addCursorListener(Lcom/android/ex/photo/PhotoViewCallbacks$CursorChangedListener;)V

    invoke-virtual {p0}, Lcom/android/ex/photo/fragments/PhotoViewFragment;->isPhotoBound()Z

    move-result v0

    if-nez v0, :cond_0

    iput-boolean v2, p0, Lcom/android/ex/photo/fragments/PhotoViewFragment;->mProgressBarNeeded:Z

    iget-object v0, p0, Lcom/android/ex/photo/fragments/PhotoViewFragment;->mPhotoPreviewAndProgress:Landroid/view/View;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    invoke-virtual {p0}, Lcom/android/ex/photo/fragments/PhotoViewFragment;->getLoaderManager()Lvedroid/support/v4/app/LoaderManager;

    move-result-object v0

    const/4 v1, 0x2

    invoke-virtual {v0, v1, v3, p0}, Lvedroid/support/v4/app/LoaderManager;->initLoader(ILandroid/os/Bundle;Lvedroid/support/v4/app/LoaderManager$LoaderCallbacks;)Lvedroid/support/v4/content/Loader;

    invoke-virtual {p0}, Lcom/android/ex/photo/fragments/PhotoViewFragment;->getLoaderManager()Lvedroid/support/v4/app/LoaderManager;

    move-result-object v0

    invoke-virtual {v0, v2, v3, p0}, Lvedroid/support/v4/app/LoaderManager;->initLoader(ILandroid/os/Bundle;Lvedroid/support/v4/app/LoaderManager$LoaderCallbacks;)Lvedroid/support/v4/content/Loader;

    :cond_0
    return-void
.end method

.method public onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 2
    .param p1    # Landroid/os/Bundle;

    invoke-super {p0, p1}, Lvedroid/support/v4/app/Fragment;->onSaveInstanceState(Landroid/os/Bundle;)V

    iget-object v0, p0, Lcom/android/ex/photo/fragments/PhotoViewFragment;->mIntent:Landroid/content/Intent;

    if-eqz v0, :cond_0

    const-string v0, "com.android.mail.photo.fragments.PhotoViewFragment.INTENT"

    iget-object v1, p0, Lcom/android/ex/photo/fragments/PhotoViewFragment;->mIntent:Landroid/content/Intent;

    invoke-virtual {v1}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    :cond_0
    return-void
.end method

.method public onViewActivated()V
    .locals 3

    iget-object v0, p0, Lcom/android/ex/photo/fragments/PhotoViewFragment;->mCallback:Lcom/android/ex/photo/PhotoViewCallbacks;

    invoke-interface {v0, p0}, Lcom/android/ex/photo/PhotoViewCallbacks;->isFragmentActive(Lvedroid/support/v4/app/Fragment;)Z

    move-result v0

    if-nez v0, :cond_0

    invoke-virtual {p0}, Lcom/android/ex/photo/fragments/PhotoViewFragment;->resetViews()V

    :goto_0
    return-void

    :cond_0
    invoke-virtual {p0}, Lcom/android/ex/photo/fragments/PhotoViewFragment;->isPhotoBound()Z

    move-result v0

    if-nez v0, :cond_1

    invoke-virtual {p0}, Lcom/android/ex/photo/fragments/PhotoViewFragment;->getLoaderManager()Lvedroid/support/v4/app/LoaderManager;

    move-result-object v0

    const/4 v1, 0x2

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2, p0}, Lvedroid/support/v4/app/LoaderManager;->restartLoader(ILandroid/os/Bundle;Lvedroid/support/v4/app/LoaderManager$LoaderCallbacks;)Lvedroid/support/v4/content/Loader;

    :cond_1
    iget-object v0, p0, Lcom/android/ex/photo/fragments/PhotoViewFragment;->mCallback:Lcom/android/ex/photo/PhotoViewCallbacks;

    invoke-interface {v0, p0}, Lcom/android/ex/photo/PhotoViewCallbacks;->onFragmentVisible(Lcom/android/ex/photo/fragments/PhotoViewFragment;)V

    goto :goto_0
.end method

.method public resetViews()V
    .locals 1

    iget-object v0, p0, Lcom/android/ex/photo/fragments/PhotoViewFragment;->mPhotoView:Lcom/android/ex/photo/views/PhotoView;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/ex/photo/fragments/PhotoViewFragment;->mPhotoView:Lcom/android/ex/photo/views/PhotoView;

    invoke-virtual {v0}, Lcom/android/ex/photo/views/PhotoView;->resetTransformations()V

    :cond_0
    return-void
.end method

.method public setFullScreen(Z)V
    .locals 0
    .param p1    # Z

    iput-boolean p1, p0, Lcom/android/ex/photo/fragments/PhotoViewFragment;->mFullScreen:Z

    return-void
.end method
