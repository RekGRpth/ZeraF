.class public Lcom/android/ex/photo/PhotoViewPager;
.super Lvedroid/support/v4/view/ViewPager;
.source "PhotoViewPager.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/ex/photo/PhotoViewPager$OnInterceptTouchListener;,
        Lcom/android/ex/photo/PhotoViewPager$InterceptType;
    }
.end annotation


# instance fields
.field private mActivatedX:F

.field private mActivatedY:F

.field private mActivePointerId:I

.field private mLastMotionX:F

.field private mListener:Lcom/android/ex/photo/PhotoViewPager$OnInterceptTouchListener;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0
    .param p1    # Landroid/content/Context;

    invoke-direct {p0, p1}, Lvedroid/support/v4/view/ViewPager;-><init>(Landroid/content/Context;)V

    invoke-direct {p0}, Lcom/android/ex/photo/PhotoViewPager;->initialize()V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/util/AttributeSet;

    invoke-direct {p0, p1, p2}, Lvedroid/support/v4/view/ViewPager;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    invoke-direct {p0}, Lcom/android/ex/photo/PhotoViewPager;->initialize()V

    return-void
.end method

.method private initialize()V
    .locals 2

    const/4 v0, 0x1

    new-instance v1, Lcom/android/ex/photo/PhotoViewPager$1;

    invoke-direct {v1, p0}, Lcom/android/ex/photo/PhotoViewPager$1;-><init>(Lcom/android/ex/photo/PhotoViewPager;)V

    invoke-virtual {p0, v0, v1}, Lcom/android/ex/photo/PhotoViewPager;->setPageTransformer(ZLvedroid/support/v4/view/ViewPager$PageTransformer;)V

    return-void
.end method


# virtual methods
.method public onInterceptTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 12
    .param p1    # Landroid/view/MotionEvent;

    iget-object v9, p0, Lcom/android/ex/photo/PhotoViewPager;->mListener:Lcom/android/ex/photo/PhotoViewPager$OnInterceptTouchListener;

    if-eqz v9, :cond_5

    iget-object v9, p0, Lcom/android/ex/photo/PhotoViewPager;->mListener:Lcom/android/ex/photo/PhotoViewPager$OnInterceptTouchListener;

    iget v10, p0, Lcom/android/ex/photo/PhotoViewPager;->mActivatedX:F

    iget v11, p0, Lcom/android/ex/photo/PhotoViewPager;->mActivatedY:F

    invoke-interface {v9, v10, v11}, Lcom/android/ex/photo/PhotoViewPager$OnInterceptTouchListener;->onTouchIntercept(FF)Lcom/android/ex/photo/PhotoViewPager$InterceptType;

    move-result-object v4

    :goto_0
    sget-object v9, Lcom/android/ex/photo/PhotoViewPager$InterceptType;->BOTH:Lcom/android/ex/photo/PhotoViewPager$InterceptType;

    if-eq v4, v9, :cond_0

    sget-object v9, Lcom/android/ex/photo/PhotoViewPager$InterceptType;->LEFT:Lcom/android/ex/photo/PhotoViewPager$InterceptType;

    if-ne v4, v9, :cond_6

    :cond_0
    const/4 v2, 0x1

    :goto_1
    sget-object v9, Lcom/android/ex/photo/PhotoViewPager$InterceptType;->BOTH:Lcom/android/ex/photo/PhotoViewPager$InterceptType;

    if-eq v4, v9, :cond_1

    sget-object v9, Lcom/android/ex/photo/PhotoViewPager$InterceptType;->RIGHT:Lcom/android/ex/photo/PhotoViewPager$InterceptType;

    if-ne v4, v9, :cond_7

    :cond_1
    const/4 v3, 0x1

    :goto_2
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v9

    and-int/lit16 v0, v9, 0xff

    const/4 v9, 0x3

    if-eq v0, v9, :cond_2

    const/4 v9, 0x1

    if-ne v0, v9, :cond_3

    :cond_2
    const/4 v9, -0x1

    iput v9, p0, Lcom/android/ex/photo/PhotoViewPager;->mActivePointerId:I

    :cond_3
    sparse-switch v0, :sswitch_data_0

    :cond_4
    :goto_3
    invoke-super {p0, p1}, Lvedroid/support/v4/view/ViewPager;->onInterceptTouchEvent(Landroid/view/MotionEvent;)Z

    move-result v9

    :goto_4
    return v9

    :cond_5
    sget-object v4, Lcom/android/ex/photo/PhotoViewPager$InterceptType;->NONE:Lcom/android/ex/photo/PhotoViewPager$InterceptType;

    goto :goto_0

    :cond_6
    const/4 v2, 0x0

    goto :goto_1

    :cond_7
    const/4 v3, 0x0

    goto :goto_2

    :sswitch_0
    if-nez v2, :cond_8

    if-eqz v3, :cond_4

    :cond_8
    iget v1, p0, Lcom/android/ex/photo/PhotoViewPager;->mActivePointerId:I

    const/4 v9, -0x1

    if-eq v1, v9, :cond_4

    invoke-static {p1, v1}, Lvedroid/support/v4/view/MotionEventCompat;->findPointerIndex(Landroid/view/MotionEvent;I)I

    move-result v7

    invoke-static {p1, v7}, Lvedroid/support/v4/view/MotionEventCompat;->getX(Landroid/view/MotionEvent;I)F

    move-result v8

    if-eqz v2, :cond_9

    if-eqz v3, :cond_9

    iput v8, p0, Lcom/android/ex/photo/PhotoViewPager;->mLastMotionX:F

    const/4 v9, 0x0

    goto :goto_4

    :cond_9
    if-eqz v2, :cond_a

    iget v9, p0, Lcom/android/ex/photo/PhotoViewPager;->mLastMotionX:F

    cmpl-float v9, v8, v9

    if-lez v9, :cond_a

    iput v8, p0, Lcom/android/ex/photo/PhotoViewPager;->mLastMotionX:F

    const/4 v9, 0x0

    goto :goto_4

    :cond_a
    if-eqz v3, :cond_4

    iget v9, p0, Lcom/android/ex/photo/PhotoViewPager;->mLastMotionX:F

    cmpg-float v9, v8, v9

    if-gez v9, :cond_4

    iput v8, p0, Lcom/android/ex/photo/PhotoViewPager;->mLastMotionX:F

    const/4 v9, 0x0

    goto :goto_4

    :sswitch_1
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v9

    iput v9, p0, Lcom/android/ex/photo/PhotoViewPager;->mLastMotionX:F

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getRawX()F

    move-result v9

    iput v9, p0, Lcom/android/ex/photo/PhotoViewPager;->mActivatedX:F

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getRawY()F

    move-result v9

    iput v9, p0, Lcom/android/ex/photo/PhotoViewPager;->mActivatedY:F

    const/4 v9, 0x0

    invoke-static {p1, v9}, Lvedroid/support/v4/view/MotionEventCompat;->getPointerId(Landroid/view/MotionEvent;I)I

    move-result v9

    iput v9, p0, Lcom/android/ex/photo/PhotoViewPager;->mActivePointerId:I

    goto :goto_3

    :sswitch_2
    invoke-static {p1}, Lvedroid/support/v4/view/MotionEventCompat;->getActionIndex(Landroid/view/MotionEvent;)I

    move-result v7

    invoke-static {p1, v7}, Lvedroid/support/v4/view/MotionEventCompat;->getPointerId(Landroid/view/MotionEvent;I)I

    move-result v6

    iget v9, p0, Lcom/android/ex/photo/PhotoViewPager;->mActivePointerId:I

    if-ne v6, v9, :cond_4

    if-nez v7, :cond_b

    const/4 v5, 0x1

    :goto_5
    invoke-static {p1, v5}, Lvedroid/support/v4/view/MotionEventCompat;->getX(Landroid/view/MotionEvent;I)F

    move-result v9

    iput v9, p0, Lcom/android/ex/photo/PhotoViewPager;->mLastMotionX:F

    invoke-static {p1, v5}, Lvedroid/support/v4/view/MotionEventCompat;->getPointerId(Landroid/view/MotionEvent;I)I

    move-result v9

    iput v9, p0, Lcom/android/ex/photo/PhotoViewPager;->mActivePointerId:I

    goto :goto_3

    :cond_b
    const/4 v5, 0x0

    goto :goto_5

    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_1
        0x2 -> :sswitch_0
        0x6 -> :sswitch_2
    .end sparse-switch
.end method

.method public setOnInterceptTouchListener(Lcom/android/ex/photo/PhotoViewPager$OnInterceptTouchListener;)V
    .locals 0
    .param p1    # Lcom/android/ex/photo/PhotoViewPager$OnInterceptTouchListener;

    iput-object p1, p0, Lcom/android/ex/photo/PhotoViewPager;->mListener:Lcom/android/ex/photo/PhotoViewPager$OnInterceptTouchListener;

    return-void
.end method
