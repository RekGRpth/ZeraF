.class public Lcom/android/launcher2/InstallShortcutHelper;
.super Ljava/lang/Object;
.source "InstallShortcutHelper.java"


# static fields
.field private static final TAG:Ljava/lang/String; = "InstallShortcutHelper"

.field private static sInstallingCount:I

.field private static sInstallingShortcut:Z

.field private static sSuccessCount:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const/4 v0, 0x0

    sput-boolean v0, Lcom/android/launcher2/InstallShortcutHelper;->sInstallingShortcut:Z

    sput v0, Lcom/android/launcher2/InstallShortcutHelper;->sInstallingCount:I

    sput v0, Lcom/android/launcher2/InstallShortcutHelper;->sSuccessCount:I

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static decreaseInstallingCount(Landroid/content/Context;Z)V
    .locals 5
    .param p0    # Landroid/content/Context;
    .param p1    # Z

    const/4 v4, 0x0

    sget v1, Lcom/android/launcher2/InstallShortcutHelper;->sInstallingCount:I

    add-int/lit8 v1, v1, -0x1

    sput v1, Lcom/android/launcher2/InstallShortcutHelper;->sInstallingCount:I

    if-eqz p1, :cond_0

    sget v1, Lcom/android/launcher2/InstallShortcutHelper;->sSuccessCount:I

    add-int/lit8 v1, v1, 0x1

    sput v1, Lcom/android/launcher2/InstallShortcutHelper;->sSuccessCount:I

    :cond_0
    const-string v1, "InstallShortcutHelper"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "decreaseInstallingCount: sInstallingCount="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    sget v3, Lcom/android/launcher2/InstallShortcutHelper;->sInstallingCount:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ", sSuccessCount="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    sget v3, Lcom/android/launcher2/InstallShortcutHelper;->sSuccessCount:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/android/launcher2/LauncherLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    sget v1, Lcom/android/launcher2/InstallShortcutHelper;->sInstallingCount:I

    if-gtz v1, :cond_1

    sget v1, Lcom/android/launcher2/InstallShortcutHelper;->sSuccessCount:I

    if-eqz v1, :cond_2

    const-string v1, "InstallShortcutHelper"

    const-string v2, "decreaseInstallingCount: triggerLoadingDatabaseManually"

    invoke-static {v1, v2}, Lcom/android/launcher2/LauncherLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {p0}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    check-cast v0, Lcom/android/launcher2/LauncherApplication;

    invoke-virtual {v0}, Lcom/android/launcher2/LauncherApplication;->triggerLoadingDatabaseManually()V

    :goto_0
    sput v4, Lcom/android/launcher2/InstallShortcutHelper;->sInstallingCount:I

    sput v4, Lcom/android/launcher2/InstallShortcutHelper;->sSuccessCount:I

    :cond_1
    const-string v1, "InstallShortcutHelper"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "decreaseInstallingCount: sInstallingShortcut="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    sget-boolean v3, Lcom/android/launcher2/InstallShortcutHelper;->sInstallingShortcut:Z

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/android/launcher2/LauncherLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    return-void

    :cond_2
    const-string v1, "InstallShortcutHelper"

    const-string v2, "decreaseInstallingCount: all failed, and reset sInstallingShortcut"

    invoke-static {v1, v2}, Lcom/android/launcher2/LauncherLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    sput-boolean v4, Lcom/android/launcher2/InstallShortcutHelper;->sInstallingShortcut:Z

    goto :goto_0
.end method

.method public static increaseInstallingCount(I)V
    .locals 3
    .param p0    # I

    sget v0, Lcom/android/launcher2/InstallShortcutHelper;->sInstallingCount:I

    add-int/2addr v0, p0

    sput v0, Lcom/android/launcher2/InstallShortcutHelper;->sInstallingCount:I

    sget v0, Lcom/android/launcher2/InstallShortcutHelper;->sInstallingCount:I

    if-lez v0, :cond_0

    const/4 v0, 0x1

    sput-boolean v0, Lcom/android/launcher2/InstallShortcutHelper;->sInstallingShortcut:Z

    :cond_0
    const-string v0, "InstallShortcutHelper"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "increaseInstallingCount: sInstallingCount="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    sget v2, Lcom/android/launcher2/InstallShortcutHelper;->sInstallingCount:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", sInstallingShortcut="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    sget-boolean v2, Lcom/android/launcher2/InstallShortcutHelper;->sInstallingShortcut:Z

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/android/launcher2/LauncherLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method public static isInstallingShortcut()Z
    .locals 1

    sget-boolean v0, Lcom/android/launcher2/InstallShortcutHelper;->sInstallingShortcut:Z

    return v0
.end method

.method public static setInstallingShortcut(Z)V
    .locals 3
    .param p0    # Z

    sput-boolean p0, Lcom/android/launcher2/InstallShortcutHelper;->sInstallingShortcut:Z

    const-string v0, "InstallShortcutHelper"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "setInstallingShortcut: sInstallingShortcut="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    sget-boolean v2, Lcom/android/launcher2/InstallShortcutHelper;->sInstallingShortcut:Z

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/android/launcher2/LauncherLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method
