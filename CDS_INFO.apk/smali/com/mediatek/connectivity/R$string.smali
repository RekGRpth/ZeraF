.class public final Lcom/mediatek/connectivity/R$string;
.super Ljava/lang/Object;
.source "R.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/mediatek/connectivity/R;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "string"
.end annotation


# static fields
.field public static final Add:I = 0x7f050050

.field public static final Report:I = 0x7f050052

.field public static final Send:I = 0x7f05004e

.field public static final Start:I = 0x7f05004f

.field public static final Stop:I = 0x7f050051

.field public static final active_link_properties_label:I = 0x7f050020

.field public static final active_network_info_label:I = 0x7f05001f

.field public static final active_network_quota_info_label:I = 0x7f050021

.field public static final announce:I = 0x7f050014

.field public static final app_name:I = 0x7f050000

.field public static final bssid_label:I = 0x7f050034

.field public static final btn_clear:I = 0x7f050012

.field public static final btn_disable_fw:I = 0x7f050088

.field public static final btn_disable_udp:I = 0x7f050090

.field public static final btn_enable_fw:I = 0x7f050087

.field public static final btn_enable_udp:I = 0x7f05008f

.field public static final btn_exit:I = 0x7f050011

.field public static final btn_ps_disable:I = 0x7f05008b

.field public static final btn_ps_enable:I = 0x7f05008a

.field public static final btn_refresh:I = 0x7f05000f

.field public static final btn_snapshot:I = 0x7f050010

.field public static final ccmni0_info_label:I = 0x7f05002b

.field public static final ccmni1_info_label:I = 0x7f05002c

.field public static final ccmni2_info_label:I = 0x7f05002d

.field public static final cds_common_info:I = 0x7f050001

.field public static final cds_common_tab_framework:I = 0x7f050005

.field public static final cds_common_tab_native:I = 0x7f050006

.field public static final cds_connectivity_info:I = 0x7f050009

.field public static final cds_connectivity_mtu:I = 0x7f05000a

.field public static final cds_ds_info:I = 0x7f05000e

.field public static final cds_pdp_info:I = 0x7f05000c

.field public static final cds_radio_info:I = 0x7f050003

.field public static final cds_radio_menu_info:I = 0x7f050004

.field public static final cds_socket_info:I = 0x7f05000d

.field public static final cds_tether_info:I = 0x7f050007

.field public static final cds_traffic_info:I = 0x7f050008

.field public static final cds_utility_info:I = 0x7f05000b

.field public static final cds_wifi_info:I = 0x7f050002

.field public static final connectivity_captive_portal:I = 0x7f05001c

.field public static final connectivity_captive_portal_setting:I = 0x7f05001d

.field public static final connectivity_change_delay:I = 0x7f05001b

.field public static final connectivity_inet_hint:I = 0x7f05001a

.field public static final dns1:I = 0x7f050049

.field public static final dns2:I = 0x7f05004a

.field public static final google:I = 0x7f050048

.field public static final hidden_ssid_label:I = 0x7f050036

.field public static final interfaceUp:I = 0x7f050085

.field public static final ip_route_label:I = 0x7f050026

.field public static final ipaddr_label:I = 0x7f050037

.field public static final link_speed_label:I = 0x7f05003a

.field public static final loading_string:I = 0x7f050046

.field public static final mac_edit_label:I = 0x7f05003c

.field public static final mac_update:I = 0x7f05003d

.field public static final macaddr_label:I = 0x7f050038

.field public static final mobileOnly:I = 0x7f050084

.field public static final mtu_configure_btn:I = 0x7f050083

.field public static final mtu_default_value:I = 0x7f050081

.field public static final mtu_name:I = 0x7f050082

.field public static final multi_apn_hint:I = 0x7f050019

.field public static final na_string:I = 0x7f050044

.field public static final net_stat_label:I = 0x7f050027

.field public static final network_config_label:I = 0x7f050025

.field public static final network_infos_label:I = 0x7f050022

.field public static final network_list_label:I = 0x7f050024

.field public static final network_policy_label:I = 0x7f050023

.field public static final network_state_label:I = 0x7f050031

.field public static final network_utility_hint:I = 0x7f050015

.field public static final networkid_label:I = 0x7f050039

.field public static final ping_test_label:I = 0x7f050067

.field public static final ps_data_flow:I = 0x7f050086

.field public static final ps_fw_app_agps:I = 0x7f05008d

.field public static final ps_fw_app_rule_label:I = 0x7f05008c

.field public static final ps_fw_app_shell:I = 0x7f05008e

.field public static final ps_fw_rule:I = 0x7f050089

.field public static final radioInfo_cid:I = 0x7f050072

.field public static final radioInfo_data_connected:I = 0x7f05007d

.field public static final radioInfo_data_connecting:I = 0x7f05007c

.field public static final radioInfo_data_disconnected:I = 0x7f05007b

.field public static final radioInfo_data_suspended:I = 0x7f05007e

.field public static final radioInfo_display_asu:I = 0x7f050070

.field public static final radioInfo_display_bytes:I = 0x7f05006e

.field public static final radioInfo_display_dbm:I = 0x7f05006f

.field public static final radioInfo_display_packets:I = 0x7f05006d

.field public static final radioInfo_lac:I = 0x7f050071

.field public static final radioInfo_phone_idle:I = 0x7f050078

.field public static final radioInfo_phone_offhook:I = 0x7f05007a

.field public static final radioInfo_phone_ringing:I = 0x7f050079

.field public static final radioInfo_roaming_in:I = 0x7f05007f

.field public static final radioInfo_roaming_not:I = 0x7f050080

.field public static final radioInfo_service_emergency:I = 0x7f050075

.field public static final radioInfo_service_in:I = 0x7f050073

.field public static final radioInfo_service_off:I = 0x7f050076

.field public static final radioInfo_service_on:I = 0x7f050077

.field public static final radioInfo_service_out:I = 0x7f050074

.field public static final radioInfo_unknown:I = 0x7f05006c

.field public static final radio_info_call_status_label:I = 0x7f05005f

.field public static final radio_info_current_network_label:I = 0x7f05005a

.field public static final radio_info_data_attempts_label:I = 0x7f05004d

.field public static final radio_info_data_successes_label:I = 0x7f05005b

.field public static final radio_info_gprs_attach_service_label:I = 0x7f050053

.field public static final radio_info_gprs_service_label:I = 0x7f050054

.field public static final radio_info_gsm_disconnects_label:I = 0x7f050059

.field public static final radio_info_gsm_service_label:I = 0x7f05005d

.field public static final radio_info_http_client_test:I = 0x7f050066

.field public static final radio_info_imei_label:I = 0x7f050056

.field public static final radio_info_imsi_label:I = 0x7f050057

.field public static final radio_info_neighboring_location_label:I = 0x7f05004c

.field public static final radio_info_network_type_label:I = 0x7f050063

.field public static final radio_info_phone_number_label:I = 0x7f050062

.field public static final radio_info_ping_hostname:I = 0x7f050065

.field public static final radio_info_ping_ipaddr:I = 0x7f050064

.field public static final radio_info_ppp_received_label:I = 0x7f05005c

.field public static final radio_info_ppp_resets_label:I = 0x7f050058

.field public static final radio_info_ppp_sent_label:I = 0x7f050060

.field public static final radio_info_radio_resets_label:I = 0x7f050061

.field public static final radio_info_roaming_label:I = 0x7f050055

.field public static final radio_info_service_state_label:I = 0x7f05006b

.field public static final radio_info_signal_location_label:I = 0x7f05004b

.field public static final radio_info_signal_strength_label:I = 0x7f05005e

.field public static final radio_state_label:I = 0x7f050069

.field public static final rssi_label:I = 0x7f050033

.field public static final run:I = 0x7f050068

.field public static final scan_list_label:I = 0x7f05003b

.field public static final sim_state_label:I = 0x7f05006a

.field public static final ssid_label:I = 0x7f050035

.field public static final supplicant_state_label:I = 0x7f050032

.field public static final sys_get_button:I = 0x7f050017

.field public static final sys_property_hint:I = 0x7f050016

.field public static final sys_set_button:I = 0x7f050018

.field public static final system_property_label:I = 0x7f05003e

.field public static final tether_info_label:I = 0x7f050028

.field public static final tethering_test_label:I = 0x7f05001e

.field public static final traffic_info_label:I = 0x7f050029

.field public static final traffic_mobile_info_label:I = 0x7f05002a

.field public static final unknown_string:I = 0x7f050045

.field public static final warning:I = 0x7f050013

.field public static final wifi_scan:I = 0x7f050047

.field public static final wifi_state_disabled:I = 0x7f050040

.field public static final wifi_state_disabling:I = 0x7f05003f

.field public static final wifi_state_enabled:I = 0x7f050042

.field public static final wifi_state_enabling:I = 0x7f050041

.field public static final wifi_state_label:I = 0x7f050030

.field public static final wifi_state_unknown:I = 0x7f050043

.field public static final wifi_update:I = 0x7f05002f

.field public static final wlan_info_label:I = 0x7f05002e


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
