.class public Lcom/mediatek/connectivity/CdsPsControlActivity;
.super Landroid/app/Activity;
.source "CdsPsControlActivity.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/mediatek/connectivity/CdsPsControlActivity$MyAppInfo;
    }
.end annotation


# static fields
.field private static final ADB_PORT:I = 0x13ad

.field private static final INTERNET:Ljava/lang/String; = "android.permission.INTERNET"

.field private static final MDLOGGER_PORT:I = 0x7541

.field private static final TAG:Ljava/lang/String; = "CdsPsControlActivity"

.field private static sStatsService:Landroid/net/INetworkStatsService;


# instance fields
.field private mAdapter:Landroid/widget/SimpleAdapter;

.field private mAppList:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/mediatek/connectivity/CdsPsControlActivity$MyAppInfo;",
            ">;"
        }
    .end annotation
.end field

.field private mAppListViw:Landroid/widget/ListView;

.field private mContext:Landroid/content/Context;

.field private mDisableFwBtn:Landroid/widget/Button;

.field private mEnableFwBtn:Landroid/widget/Button;

.field private mFwStatus:Landroid/widget/TextView;

.field private mIsEnabled:Z

.field private mNetd:Landroid/os/INetworkManagementService;


# direct methods
.method public constructor <init>()V
    .locals 1

    const/4 v0, 0x0

    invoke-direct {p0}, Landroid/app/Activity;-><init>()V

    iput-object v0, p0, Lcom/mediatek/connectivity/CdsPsControlActivity;->mEnableFwBtn:Landroid/widget/Button;

    iput-object v0, p0, Lcom/mediatek/connectivity/CdsPsControlActivity;->mDisableFwBtn:Landroid/widget/Button;

    return-void
.end method

.method static synthetic access$000(Lcom/mediatek/connectivity/CdsPsControlActivity;Z)V
    .locals 0
    .param p0    # Lcom/mediatek/connectivity/CdsPsControlActivity;
    .param p1    # Z

    invoke-direct {p0, p1}, Lcom/mediatek/connectivity/CdsPsControlActivity;->setFirewallEnabled(Z)V

    return-void
.end method

.method private getData()Ljava/util/List;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<+",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "*>;>;"
        }
    .end annotation

    const/4 v0, 0x0

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    const/4 v0, 0x0

    :goto_0
    iget-object v4, p0, Lcom/mediatek/connectivity/CdsPsControlActivity;->mAppList:Ljava/util/ArrayList;

    invoke-virtual {v4}, Ljava/util/ArrayList;->size()I

    move-result v4

    if-ge v0, v4, :cond_0

    iget-object v4, p0, Lcom/mediatek/connectivity/CdsPsControlActivity;->mAppList:Ljava/util/ArrayList;

    invoke-virtual {v4, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/mediatek/connectivity/CdsPsControlActivity$MyAppInfo;

    new-instance v2, Ljava/util/HashMap;

    invoke-direct {v2}, Ljava/util/HashMap;-><init>()V

    const-string v4, "appText"

    invoke-virtual {v3}, Lcom/mediatek/connectivity/CdsPsControlActivity$MyAppInfo;->getPackageName()Ljava/lang/String;

    move-result-object v5

    invoke-interface {v2, v4, v5}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_0
    return-object v1
.end method

.method private static getNetworkStatsForUid(I)Landroid/net/NetworkStats;
    .locals 2
    .param p0    # I

    :try_start_0
    invoke-static {}, Lcom/mediatek/connectivity/CdsPsControlActivity;->getStatsService()Landroid/net/INetworkStatsService;

    move-result-object v1

    invoke-interface {v1, p0}, Landroid/net/INetworkStatsService;->getDataLayerSnapshotForUid(I)Landroid/net/NetworkStats;
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    return-object v1

    :catch_0
    move-exception v0

    new-instance v1, Ljava/lang/RuntimeException;

    invoke-direct {v1, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/Throwable;)V

    throw v1
.end method

.method private static declared-synchronized getStatsService()Landroid/net/INetworkStatsService;
    .locals 2

    const-class v1, Lcom/mediatek/connectivity/CdsPsControlActivity;

    monitor-enter v1

    :try_start_0
    sget-object v0, Lcom/mediatek/connectivity/CdsPsControlActivity;->sStatsService:Landroid/net/INetworkStatsService;

    if-nez v0, :cond_0

    const-string v0, "netstats"

    invoke-static {v0}, Landroid/os/ServiceManager;->getService(Ljava/lang/String;)Landroid/os/IBinder;

    move-result-object v0

    invoke-static {v0}, Landroid/net/INetworkStatsService$Stub;->asInterface(Landroid/os/IBinder;)Landroid/net/INetworkStatsService;

    move-result-object v0

    sput-object v0, Lcom/mediatek/connectivity/CdsPsControlActivity;->sStatsService:Landroid/net/INetworkStatsService;

    :cond_0
    sget-object v0, Lcom/mediatek/connectivity/CdsPsControlActivity;->sStatsService:Landroid/net/INetworkStatsService;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method private setFirewallEnabled(Z)V
    .locals 4
    .param p1    # Z

    iget-object v1, p0, Lcom/mediatek/connectivity/CdsPsControlActivity;->mNetd:Landroid/os/INetworkManagementService;

    if-nez v1, :cond_0

    const-string v1, "CdsPsControlActivity"

    const-string v2, "INetworkManagementService is null"

    invoke-static {v1, v2}, Lcom/mediatek/xlog/Xlog;->e(Ljava/lang/String;Ljava/lang/String;)I

    :goto_0
    return-void

    :cond_0
    const-string v1, "CdsPsControlActivity"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "set firewall:"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/mediatek/xlog/Xlog;->i(Ljava/lang/String;Ljava/lang/String;)I

    iget-boolean v1, p0, Lcom/mediatek/connectivity/CdsPsControlActivity;->mIsEnabled:Z

    if-ne v1, p1, :cond_1

    const-string v1, "CdsPsControlActivity"

    const-string v2, "No change"

    invoke-static {v1, v2}, Lcom/mediatek/xlog/Xlog;->i(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    :cond_1
    if-eqz p1, :cond_2

    :try_start_0
    iget-object v1, p0, Lcom/mediatek/connectivity/CdsPsControlActivity;->mNetd:Landroid/os/INetworkManagementService;

    invoke-interface {v1, p1}, Landroid/os/INetworkManagementService;->setFirewallEnabled(Z)V

    :cond_2
    iget-object v1, p0, Lcom/mediatek/connectivity/CdsPsControlActivity;->mNetd:Landroid/os/INetworkManagementService;

    const-string v2, ""

    const/16 v3, 0x7541

    invoke-interface {v1, v2, v3, p1}, Landroid/os/INetworkManagementService;->setFirewallEgressDestRule(Ljava/lang/String;IZ)V

    iget-object v1, p0, Lcom/mediatek/connectivity/CdsPsControlActivity;->mNetd:Landroid/os/INetworkManagementService;

    const-string v2, ""

    const/16 v3, 0x13ad

    invoke-interface {v1, v2, v3, p1}, Landroid/os/INetworkManagementService;->setFirewallEgressDestRule(Ljava/lang/String;IZ)V

    if-nez p1, :cond_3

    iget-object v1, p0, Lcom/mediatek/connectivity/CdsPsControlActivity;->mNetd:Landroid/os/INetworkManagementService;

    invoke-interface {v1, p1}, Landroid/os/INetworkManagementService;->setFirewallEnabled(Z)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :cond_3
    invoke-direct {p0, p1}, Lcom/mediatek/connectivity/CdsPsControlActivity;->updateFwButton(Z)V

    goto :goto_0

    :catch_0
    move-exception v0

    :try_start_1
    invoke-virtual {v0}, Ljava/lang/Throwable;->printStackTrace()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    invoke-direct {p0, p1}, Lcom/mediatek/connectivity/CdsPsControlActivity;->updateFwButton(Z)V

    goto :goto_0

    :catchall_0
    move-exception v1

    invoke-direct {p0, p1}, Lcom/mediatek/connectivity/CdsPsControlActivity;->updateFwButton(Z)V

    throw v1
.end method

.method private updateAppList()V
    .locals 0

    return-void
.end method

.method private updateFwButton(Z)V
    .locals 2
    .param p1    # Z

    iput-boolean p1, p0, Lcom/mediatek/connectivity/CdsPsControlActivity;->mIsEnabled:Z

    if-eqz p1, :cond_0

    iget-object v0, p0, Lcom/mediatek/connectivity/CdsPsControlActivity;->mFwStatus:Landroid/widget/TextView;

    const-string v1, "enabled"

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/mediatek/connectivity/CdsPsControlActivity;->mFwStatus:Landroid/widget/TextView;

    const-string v1, "disabled"

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0
.end method


# virtual methods
.method getApps(Landroid/content/Context;)V
    .locals 13
    .param p1    # Landroid/content/Context;

    :try_start_0
    invoke-virtual {p1}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v6

    const/4 v10, 0x0

    invoke-virtual {v6, v10}, Landroid/content/pm/PackageManager;->getInstalledApplications(I)Ljava/util/List;

    move-result-object v8

    new-instance v10, Ljava/util/ArrayList;

    invoke-interface {v8}, Ljava/util/List;->size()I

    move-result v11

    invoke-direct {v10, v11}, Ljava/util/ArrayList;-><init>(I)V

    iput-object v10, p0, Lcom/mediatek/connectivity/CdsPsControlActivity;->mAppList:Ljava/util/ArrayList;

    invoke-interface {v8}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_0
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v10

    if-eqz v10, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/pm/ApplicationInfo;

    iget-object v7, v0, Landroid/content/pm/PackageItemInfo;->packageName:Ljava/lang/String;

    const-string v10, "android.permission.INTERNET"

    invoke-virtual {v6, v10, v7}, Landroid/content/pm/PackageManager;->checkPermission(Ljava/lang/String;Ljava/lang/String;)I

    move-result v10

    if-eqz v10, :cond_0

    iget v9, v0, Landroid/content/pm/ApplicationInfo;->uid:I

    const-string v10, "CdsPsControlActivity"

    new-instance v11, Ljava/lang/StringBuilder;

    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    const-string v12, "packageName:"

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    const-string v12, ":"

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11, v9}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-static {v10, v11}, Lcom/mediatek/xlog/Xlog;->d(Ljava/lang/String;Ljava/lang/String;)I

    invoke-static {v9}, Lcom/mediatek/connectivity/CdsPsControlActivity;->getNetworkStatsForUid(I)Landroid/net/NetworkStats;

    move-result-object v3

    new-instance v5, Lcom/mediatek/connectivity/CdsPsControlActivity$MyAppInfo;

    invoke-direct {v5, p0, v7, v0}, Lcom/mediatek/connectivity/CdsPsControlActivity$MyAppInfo;-><init>(Lcom/mediatek/connectivity/CdsPsControlActivity;Ljava/lang/String;Landroid/content/pm/ApplicationInfo;)V

    iget-object v10, p0, Lcom/mediatek/connectivity/CdsPsControlActivity;->mAppList:Ljava/util/ArrayList;

    invoke-virtual {v10, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v4

    invoke-virtual {v4}, Ljava/lang/Throwable;->printStackTrace()V

    :goto_1
    return-void

    :cond_1
    :try_start_1
    new-instance v1, Landroid/content/pm/ApplicationInfo;

    invoke-direct {v1}, Landroid/content/pm/ApplicationInfo;-><init>()V

    const-string v10, "root"

    invoke-static {v10}, Landroid/os/Process;->getUidForName(Ljava/lang/String;)I

    move-result v10

    iput v10, v1, Landroid/content/pm/ApplicationInfo;->uid:I

    const-string v10, "root Applications"

    iput-object v10, v1, Landroid/content/pm/PackageItemInfo;->packageName:Ljava/lang/String;

    new-instance v5, Lcom/mediatek/connectivity/CdsPsControlActivity$MyAppInfo;

    iget-object v10, v1, Landroid/content/pm/PackageItemInfo;->packageName:Ljava/lang/String;

    invoke-direct {v5, p0, v10, v1}, Lcom/mediatek/connectivity/CdsPsControlActivity$MyAppInfo;-><init>(Lcom/mediatek/connectivity/CdsPsControlActivity;Ljava/lang/String;Landroid/content/pm/ApplicationInfo;)V

    iget-object v10, p0, Lcom/mediatek/connectivity/CdsPsControlActivity;->mAppList:Ljava/util/ArrayList;

    invoke-virtual {v10, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    const-string v10, "media"

    invoke-static {v10}, Landroid/os/Process;->getUidForName(Ljava/lang/String;)I

    move-result v10

    iput v10, v1, Landroid/content/pm/ApplicationInfo;->uid:I

    const-string v10, "Media server"

    iput-object v10, v1, Landroid/content/pm/PackageItemInfo;->packageName:Ljava/lang/String;

    new-instance v5, Lcom/mediatek/connectivity/CdsPsControlActivity$MyAppInfo;

    iget-object v10, v1, Landroid/content/pm/PackageItemInfo;->packageName:Ljava/lang/String;

    invoke-direct {v5, p0, v10, v1}, Lcom/mediatek/connectivity/CdsPsControlActivity$MyAppInfo;-><init>(Lcom/mediatek/connectivity/CdsPsControlActivity;Ljava/lang/String;Landroid/content/pm/ApplicationInfo;)V

    iget-object v10, p0, Lcom/mediatek/connectivity/CdsPsControlActivity;->mAppList:Ljava/util/ArrayList;

    invoke-virtual {v10, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    const-string v10, "vpn"

    invoke-static {v10}, Landroid/os/Process;->getUidForName(Ljava/lang/String;)I

    move-result v10

    iput v10, v1, Landroid/content/pm/ApplicationInfo;->uid:I

    const-string v10, "VPN networking"

    iput-object v10, v1, Landroid/content/pm/PackageItemInfo;->packageName:Ljava/lang/String;

    new-instance v5, Lcom/mediatek/connectivity/CdsPsControlActivity$MyAppInfo;

    iget-object v10, v1, Landroid/content/pm/PackageItemInfo;->packageName:Ljava/lang/String;

    invoke-direct {v5, p0, v10, v1}, Lcom/mediatek/connectivity/CdsPsControlActivity$MyAppInfo;-><init>(Lcom/mediatek/connectivity/CdsPsControlActivity;Ljava/lang/String;Landroid/content/pm/ApplicationInfo;)V

    iget-object v10, p0, Lcom/mediatek/connectivity/CdsPsControlActivity;->mAppList:Ljava/util/ArrayList;

    invoke-virtual {v10, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    const-string v10, "shell"

    invoke-static {v10}, Landroid/os/Process;->getUidForName(Ljava/lang/String;)I

    move-result v10

    iput v10, v1, Landroid/content/pm/ApplicationInfo;->uid:I

    const-string v10, "Linux shell"

    iput-object v10, v1, Landroid/content/pm/PackageItemInfo;->packageName:Ljava/lang/String;

    new-instance v5, Lcom/mediatek/connectivity/CdsPsControlActivity$MyAppInfo;

    iget-object v10, v1, Landroid/content/pm/PackageItemInfo;->packageName:Ljava/lang/String;

    invoke-direct {v5, p0, v10, v1}, Lcom/mediatek/connectivity/CdsPsControlActivity$MyAppInfo;-><init>(Lcom/mediatek/connectivity/CdsPsControlActivity;Ljava/lang/String;Landroid/content/pm/ApplicationInfo;)V

    iget-object v10, p0, Lcom/mediatek/connectivity/CdsPsControlActivity;->mAppList:Ljava/util/ArrayList;

    invoke-virtual {v10, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    const-string v10, "gps"

    invoke-static {v10}, Landroid/os/Process;->getUidForName(Ljava/lang/String;)I

    move-result v10

    iput v10, v1, Landroid/content/pm/ApplicationInfo;->uid:I

    const-string v10, "GPS"

    iput-object v10, v1, Landroid/content/pm/PackageItemInfo;->packageName:Ljava/lang/String;

    new-instance v5, Lcom/mediatek/connectivity/CdsPsControlActivity$MyAppInfo;

    iget-object v10, v1, Landroid/content/pm/PackageItemInfo;->packageName:Ljava/lang/String;

    invoke-direct {v5, p0, v10, v1}, Lcom/mediatek/connectivity/CdsPsControlActivity$MyAppInfo;-><init>(Lcom/mediatek/connectivity/CdsPsControlActivity;Ljava/lang/String;Landroid/content/pm/ApplicationInfo;)V

    iget-object v10, p0, Lcom/mediatek/connectivity/CdsPsControlActivity;->mAppList:Ljava/util/ArrayList;

    invoke-virtual {v10, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_1
.end method

.method public onClick(Landroid/view/View;)V
    .locals 1
    .param p1    # Landroid/view/View;

    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v0

    return-void
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 4
    .param p1    # Landroid/os/Bundle;

    invoke-super {p0, p1}, Landroid/app/Activity;->onCreate(Landroid/os/Bundle;)V

    invoke-virtual {p0}, Landroid/content/ContextWrapper;->getBaseContext()Landroid/content/Context;

    move-result-object v2

    iput-object v2, p0, Lcom/mediatek/connectivity/CdsPsControlActivity;->mContext:Landroid/content/Context;

    const v2, 0x7f03000a

    invoke-virtual {p0, v2}, Landroid/app/Activity;->setContentView(I)V

    const-string v2, "network_management"

    invoke-static {v2}, Landroid/os/ServiceManager;->getService(Ljava/lang/String;)Landroid/os/IBinder;

    move-result-object v0

    invoke-static {v0}, Landroid/os/INetworkManagementService$Stub;->asInterface(Landroid/os/IBinder;)Landroid/os/INetworkManagementService;

    move-result-object v2

    iput-object v2, p0, Lcom/mediatek/connectivity/CdsPsControlActivity;->mNetd:Landroid/os/INetworkManagementService;

    const v2, 0x7f070002

    invoke-virtual {p0, v2}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/Button;

    iput-object v2, p0, Lcom/mediatek/connectivity/CdsPsControlActivity;->mEnableFwBtn:Landroid/widget/Button;

    iget-object v2, p0, Lcom/mediatek/connectivity/CdsPsControlActivity;->mEnableFwBtn:Landroid/widget/Button;

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/mediatek/connectivity/CdsPsControlActivity;->mEnableFwBtn:Landroid/widget/Button;

    new-instance v3, Lcom/mediatek/connectivity/CdsPsControlActivity$1;

    invoke-direct {v3, p0}, Lcom/mediatek/connectivity/CdsPsControlActivity$1;-><init>(Lcom/mediatek/connectivity/CdsPsControlActivity;)V

    invoke-virtual {v2, v3}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    :cond_0
    const v2, 0x7f07002f

    invoke-virtual {p0, v2}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/Button;

    iput-object v2, p0, Lcom/mediatek/connectivity/CdsPsControlActivity;->mDisableFwBtn:Landroid/widget/Button;

    iget-object v2, p0, Lcom/mediatek/connectivity/CdsPsControlActivity;->mDisableFwBtn:Landroid/widget/Button;

    if-eqz v2, :cond_1

    iget-object v2, p0, Lcom/mediatek/connectivity/CdsPsControlActivity;->mDisableFwBtn:Landroid/widget/Button;

    new-instance v3, Lcom/mediatek/connectivity/CdsPsControlActivity$2;

    invoke-direct {v3, p0}, Lcom/mediatek/connectivity/CdsPsControlActivity$2;-><init>(Lcom/mediatek/connectivity/CdsPsControlActivity;)V

    invoke-virtual {v2, v3}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    :cond_1
    const v2, 0x7f07002e

    invoke-virtual {p0, v2}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    iput-object v2, p0, Lcom/mediatek/connectivity/CdsPsControlActivity;->mFwStatus:Landroid/widget/TextView;

    iget-object v2, p0, Lcom/mediatek/connectivity/CdsPsControlActivity;->mNetd:Landroid/os/INetworkManagementService;

    if-nez v2, :cond_2

    const-string v2, "CdsPsControlActivity"

    const-string v3, "INetworkManagementService is null"

    invoke-static {v2, v3}, Lcom/mediatek/xlog/Xlog;->e(Ljava/lang/String;Ljava/lang/String;)I

    :goto_0
    return-void

    :cond_2
    :try_start_0
    iget-object v2, p0, Lcom/mediatek/connectivity/CdsPsControlActivity;->mNetd:Landroid/os/INetworkManagementService;

    invoke-interface {v2}, Landroid/os/INetworkManagementService;->isFirewallEnabled()Z

    move-result v2

    invoke-direct {p0, v2}, Lcom/mediatek/connectivity/CdsPsControlActivity;->updateFwButton(Z)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    :goto_1
    const-string v2, "CdsPsControlActivity"

    const-string v3, "CdsPsControlActivity is started"

    invoke-static {v2, v3}, Lcom/mediatek/xlog/Xlog;->i(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    :catch_0
    move-exception v1

    invoke-virtual {v1}, Ljava/lang/Throwable;->printStackTrace()V

    goto :goto_1
.end method

.method protected onPause()V
    .locals 0

    invoke-super {p0}, Landroid/app/Activity;->onPause()V

    return-void
.end method

.method protected onResume()V
    .locals 2

    :try_start_0
    iget-object v1, p0, Lcom/mediatek/connectivity/CdsPsControlActivity;->mNetd:Landroid/os/INetworkManagementService;

    invoke-interface {v1}, Landroid/os/INetworkManagementService;->isFirewallEnabled()Z

    move-result v1

    invoke-direct {p0, v1}, Lcom/mediatek/connectivity/CdsPsControlActivity;->updateFwButton(Z)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    invoke-super {p0}, Landroid/app/Activity;->onResume()V

    return-void

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Ljava/lang/Throwable;->printStackTrace()V

    goto :goto_0
.end method
