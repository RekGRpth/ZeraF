.class public Lcom/android/videoeditor/TransitionsAdapter;
.super Lcom/android/videoeditor/BaseAdapterWithImages;
.source "TransitionsAdapter.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/android/videoeditor/BaseAdapterWithImages",
        "<",
        "Ljava/lang/Integer;",
        ">;"
    }
.end annotation


# instance fields
.field private final mTransitions:[Lcom/android/videoeditor/TransitionType;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/widget/AbsListView;)V
    .locals 1
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/widget/AbsListView;

    invoke-direct {p0, p1, p2}, Lcom/android/videoeditor/BaseAdapterWithImages;-><init>(Landroid/content/Context;Landroid/widget/AbsListView;)V

    invoke-static {p1}, Lcom/android/videoeditor/TransitionType;->getTransitions(Landroid/content/Context;)[Lcom/android/videoeditor/TransitionType;

    move-result-object v0

    iput-object v0, p0, Lcom/android/videoeditor/TransitionsAdapter;->mTransitions:[Lcom/android/videoeditor/TransitionType;

    return-void
.end method


# virtual methods
.method public getCount()I
    .locals 1

    iget-object v0, p0, Lcom/android/videoeditor/TransitionsAdapter;->mTransitions:[Lcom/android/videoeditor/TransitionType;

    array-length v0, v0

    return v0
.end method

.method public getItem(I)Ljava/lang/Object;
    .locals 1
    .param p1    # I

    iget-object v0, p0, Lcom/android/videoeditor/TransitionsAdapter;->mTransitions:[Lcom/android/videoeditor/TransitionType;

    aget-object v0, v0, p1

    return-object v0
.end method

.method public getTransitions()[Lcom/android/videoeditor/TransitionType;
    .locals 1

    iget-object v0, p0, Lcom/android/videoeditor/TransitionsAdapter;->mTransitions:[Lcom/android/videoeditor/TransitionType;

    return-object v0
.end method

.method public getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 7
    .param p1    # I
    .param p2    # Landroid/view/View;
    .param p3    # Landroid/view/ViewGroup;

    if-nez p2, :cond_0

    iget-object v5, p0, Lcom/android/videoeditor/TransitionsAdapter;->mContext:Landroid/content/Context;

    const-string v6, "layout_inflater"

    invoke-virtual {v5, v6}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/view/LayoutInflater;

    const v5, 0x7f040009

    const/4 v6, 0x0

    invoke-virtual {v3, v5, v6}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    new-instance v4, Lcom/android/videoeditor/BaseAdapterWithImages$ImageTextViewHolder;

    invoke-direct {v4, v0}, Lcom/android/videoeditor/BaseAdapterWithImages$ImageTextViewHolder;-><init>(Landroid/view/View;)V

    invoke-virtual {v0, v4}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    :goto_0
    iget-object v5, p0, Lcom/android/videoeditor/TransitionsAdapter;->mTransitions:[Lcom/android/videoeditor/TransitionType;

    aget-object v1, v5, p1

    invoke-virtual {v1}, Lcom/android/videoeditor/TransitionType;->getType()I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    invoke-virtual {p0, v5, v6, v4}, Lcom/android/videoeditor/TransitionsAdapter;->initiateLoad(Ljava/lang/Object;Ljava/lang/Object;Lcom/android/videoeditor/BaseAdapterWithImages$ImageViewHolder;)V

    iget-object v5, v4, Lcom/android/videoeditor/BaseAdapterWithImages$ImageTextViewHolder;->mNameView:Landroid/widget/TextView;

    invoke-virtual {v1}, Lcom/android/videoeditor/TransitionType;->getName()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    return-object v0

    :cond_0
    move-object v0, p2

    invoke-virtual {p2}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/android/videoeditor/BaseAdapterWithImages$ImageTextViewHolder;

    goto :goto_0
.end method

.method protected loadImage(Ljava/lang/Object;)Landroid/graphics/Bitmap;
    .locals 3
    .param p1    # Ljava/lang/Object;

    iget-object v0, p0, Lcom/android/videoeditor/TransitionsAdapter;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    sget-object v1, Lcom/android/videoeditor/TransitionType;->TRANSITION_RESOURCE_IDS:[I

    check-cast p1, Ljava/lang/Integer;

    invoke-virtual {p1}, Ljava/lang/Integer;->intValue()I

    move-result v2

    aget v1, v1, v2

    invoke-static {v0, v1}, Landroid/graphics/BitmapFactory;->decodeResource(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;

    move-result-object v0

    return-object v0
.end method
