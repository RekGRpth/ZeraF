.class public final Lcom/android/videoeditor/R$dimen;
.super Ljava/lang/Object;
.source "R.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/videoeditor/R;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "dimen"
.end annotation


# static fields
.field public static final add_audio_track_button_width:I = 0x7f070010

.field public static final audio_layout_height:I = 0x7f07000c

.field public static final handle_width:I = 0x7f070011

.field public static final media_layout_height:I = 0x7f070008

.field public static final media_layout_padding:I = 0x7f07000f

.field public static final overlay_layout_height:I = 0x7f07000b

.field public static final playhead_layout_height:I = 0x7f07000d

.field public static final playhead_layout_text_size:I = 0x7f07000e

.field public static final playhead_margin_bottom:I = 0x7f070016

.field public static final playhead_margin_top:I = 0x7f070013

.field public static final playhead_margin_top_not_ok:I = 0x7f070015

.field public static final playhead_margin_top_ok:I = 0x7f070014

.field public static final playhead_tick_height:I = 0x7f070017

.field public static final project_picker_column_width:I = 0x7f070004

.field public static final project_picker_item_container_width:I = 0x7f070005

.field public static final project_picker_item_font_size:I = 0x7f070003

.field public static final project_picker_item_height:I = 0x7f070007

.field public static final project_picker_item_overlay_height:I = 0x7f070002

.field public static final project_picker_item_overlay_horizontal_inset:I = 0x7f070001

.field public static final project_picker_item_overlay_vertical_inset:I = 0x7f070000

.field public static final project_picker_item_width:I = 0x7f070006

.field public static final timelime_item_horizontal_padding:I = 0x7f070019

.field public static final timelime_item_vertical_padding:I = 0x7f070018

.field public static final timelime_transition_vertical_inset:I = 0x7f07001a

.field public static final timeline_add_media_button_height:I = 0x7f070009

.field public static final timeline_add_media_button_top_padding:I = 0x7f07000a

.field public static final timeline_separator_height:I = 0x7f070012


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
