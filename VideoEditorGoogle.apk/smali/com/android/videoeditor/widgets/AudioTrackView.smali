.class public Lcom/android/videoeditor/widgets/AudioTrackView;
.super Landroid/view/View;
.source "AudioTrackView.java"


# instance fields
.field private mGestureListener:Lcom/android/videoeditor/widgets/ItemSimpleGestureListener;

.field private final mLinePaint:Landroid/graphics/Paint;

.field private final mLoopPaint:Landroid/graphics/Paint;

.field private mNormalizedGains:[D

.field private mProgress:I

.field private final mProgressDestRect:Landroid/graphics/Rect;

.field private mScreenWidth:I

.field private final mScrollListener:Lcom/android/videoeditor/widgets/ScrollViewListener;

.field private mScrollX:I

.field private final mSimpleGestureDetector:Landroid/view/GestureDetector;

.field private mTimelineDurationMs:J

.field private mWaveformData:Landroid/media/videoeditor/WaveformData;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 2
    .param p1    # Landroid/content/Context;

    const/4 v0, 0x0

    const/4 v1, 0x0

    invoke-direct {p0, p1, v0, v1}, Lcom/android/videoeditor/widgets/AudioTrackView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/util/AttributeSet;

    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/android/videoeditor/widgets/AudioTrackView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 10
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/util/AttributeSet;
    .param p3    # I

    const/high16 v7, 0x3f800000

    const/4 v9, 0x0

    invoke-direct {p0, p1, p2, p3}, Landroid/view/View;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    invoke-virtual {p0}, Lcom/android/videoeditor/widgets/AudioTrackView;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    new-instance v5, Landroid/graphics/Paint;

    invoke-direct {v5}, Landroid/graphics/Paint;-><init>()V

    iput-object v5, p0, Lcom/android/videoeditor/widgets/AudioTrackView;->mLinePaint:Landroid/graphics/Paint;

    iget-object v5, p0, Lcom/android/videoeditor/widgets/AudioTrackView;->mLinePaint:Landroid/graphics/Paint;

    invoke-virtual {v5, v9}, Landroid/graphics/Paint;->setAntiAlias(Z)V

    iget-object v5, p0, Lcom/android/videoeditor/widgets/AudioTrackView;->mLinePaint:Landroid/graphics/Paint;

    invoke-virtual {v5, v7}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    iget-object v5, p0, Lcom/android/videoeditor/widgets/AudioTrackView;->mLinePaint:Landroid/graphics/Paint;

    const v6, 0x7f060002

    invoke-virtual {v4, v6}, Landroid/content/res/Resources;->getColor(I)I

    move-result v6

    invoke-virtual {v5, v6}, Landroid/graphics/Paint;->setColor(I)V

    new-instance v5, Landroid/graphics/Paint;

    invoke-direct {v5}, Landroid/graphics/Paint;-><init>()V

    iput-object v5, p0, Lcom/android/videoeditor/widgets/AudioTrackView;->mLoopPaint:Landroid/graphics/Paint;

    iget-object v5, p0, Lcom/android/videoeditor/widgets/AudioTrackView;->mLoopPaint:Landroid/graphics/Paint;

    invoke-virtual {v5, v9}, Landroid/graphics/Paint;->setAntiAlias(Z)V

    iget-object v5, p0, Lcom/android/videoeditor/widgets/AudioTrackView;->mLoopPaint:Landroid/graphics/Paint;

    invoke-virtual {v5, v7}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    iget-object v5, p0, Lcom/android/videoeditor/widgets/AudioTrackView;->mLoopPaint:Landroid/graphics/Paint;

    const v6, 0x7f060003

    invoke-virtual {v4, v6}, Landroid/content/res/Resources;->getColor(I)I

    move-result v6

    invoke-virtual {v5, v6}, Landroid/graphics/Paint;->setColor(I)V

    invoke-static {p1}, Lcom/android/videoeditor/widgets/ProgressBar;->getProgressBar(Landroid/content/Context;)Lcom/android/videoeditor/widgets/ProgressBar;

    move-result-object v3

    const v5, 0x7f07000c

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v5

    float-to-int v1, v5

    new-instance v5, Landroid/graphics/Rect;

    invoke-virtual {p0}, Lcom/android/videoeditor/widgets/AudioTrackView;->getPaddingLeft()I

    move-result v6

    invoke-virtual {v3}, Lcom/android/videoeditor/widgets/ProgressBar;->getHeight()I

    move-result v7

    sub-int v7, v1, v7

    invoke-virtual {p0}, Lcom/android/videoeditor/widgets/AudioTrackView;->getPaddingBottom()I

    move-result v8

    sub-int/2addr v7, v8

    invoke-virtual {p0}, Lcom/android/videoeditor/widgets/AudioTrackView;->getPaddingBottom()I

    move-result v8

    sub-int v8, v1, v8

    invoke-direct {v5, v6, v7, v9, v8}, Landroid/graphics/Rect;-><init>(IIII)V

    iput-object v5, p0, Lcom/android/videoeditor/widgets/AudioTrackView;->mProgressDestRect:Landroid/graphics/Rect;

    new-instance v5, Landroid/view/GestureDetector;

    new-instance v6, Lcom/android/videoeditor/widgets/AudioTrackView$1;

    invoke-direct {v6, p0}, Lcom/android/videoeditor/widgets/AudioTrackView$1;-><init>(Lcom/android/videoeditor/widgets/AudioTrackView;)V

    invoke-direct {v5, p1, v6}, Landroid/view/GestureDetector;-><init>(Landroid/content/Context;Landroid/view/GestureDetector$OnGestureListener;)V

    iput-object v5, p0, Lcom/android/videoeditor/widgets/AudioTrackView;->mSimpleGestureDetector:Landroid/view/GestureDetector;

    new-instance v5, Lcom/android/videoeditor/widgets/AudioTrackView$2;

    invoke-direct {v5, p0}, Lcom/android/videoeditor/widgets/AudioTrackView$2;-><init>(Lcom/android/videoeditor/widgets/AudioTrackView;)V

    iput-object v5, p0, Lcom/android/videoeditor/widgets/AudioTrackView;->mScrollListener:Lcom/android/videoeditor/widgets/ScrollViewListener;

    const-string v5, "window"

    invoke-virtual {p1, v5}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Landroid/view/WindowManager;

    invoke-interface {v5}, Landroid/view/WindowManager;->getDefaultDisplay()Landroid/view/Display;

    move-result-object v0

    new-instance v2, Landroid/util/DisplayMetrics;

    invoke-direct {v2}, Landroid/util/DisplayMetrics;-><init>()V

    invoke-virtual {v0, v2}, Landroid/view/Display;->getMetrics(Landroid/util/DisplayMetrics;)V

    iget v5, v2, Landroid/util/DisplayMetrics;->widthPixels:I

    iput v5, p0, Lcom/android/videoeditor/widgets/AudioTrackView;->mScreenWidth:I

    const/4 v5, -0x1

    iput v5, p0, Lcom/android/videoeditor/widgets/AudioTrackView;->mProgress:I

    return-void
.end method

.method static synthetic access$000(Lcom/android/videoeditor/widgets/AudioTrackView;)Lcom/android/videoeditor/widgets/ItemSimpleGestureListener;
    .locals 1
    .param p0    # Lcom/android/videoeditor/widgets/AudioTrackView;

    iget-object v0, p0, Lcom/android/videoeditor/widgets/AudioTrackView;->mGestureListener:Lcom/android/videoeditor/widgets/ItemSimpleGestureListener;

    return-object v0
.end method

.method static synthetic access$102(Lcom/android/videoeditor/widgets/AudioTrackView;I)I
    .locals 0
    .param p0    # Lcom/android/videoeditor/widgets/AudioTrackView;
    .param p1    # I

    iput p1, p0, Lcom/android/videoeditor/widgets/AudioTrackView;->mScrollX:I

    return p1
.end method


# virtual methods
.method public getWaveformData()Landroid/media/videoeditor/WaveformData;
    .locals 1

    iget-object v0, p0, Lcom/android/videoeditor/widgets/AudioTrackView;->mWaveformData:Landroid/media/videoeditor/WaveformData;

    return-object v0
.end method

.method protected onAttachedToWindow()V
    .locals 2

    invoke-virtual {p0}, Lcom/android/videoeditor/widgets/AudioTrackView;->getParent()Landroid/view/ViewParent;

    move-result-object v1

    check-cast v1, Landroid/view/View;

    invoke-virtual {v1}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    move-result-object v1

    check-cast v1, Landroid/view/View;

    invoke-virtual {v1}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    check-cast v0, Lcom/android/videoeditor/widgets/TimelineHorizontalScrollView;

    invoke-virtual {v0}, Lcom/android/videoeditor/widgets/TimelineHorizontalScrollView;->getScrollX()I

    move-result v1

    iput v1, p0, Lcom/android/videoeditor/widgets/AudioTrackView;->mScrollX:I

    iget-object v1, p0, Lcom/android/videoeditor/widgets/AudioTrackView;->mScrollListener:Lcom/android/videoeditor/widgets/ScrollViewListener;

    invoke-virtual {v0, v1}, Lcom/android/videoeditor/widgets/TimelineHorizontalScrollView;->addScrollListener(Lcom/android/videoeditor/widgets/ScrollViewListener;)V

    return-void
.end method

.method protected onDetachedFromWindow()V
    .locals 2

    invoke-virtual {p0}, Lcom/android/videoeditor/widgets/AudioTrackView;->getParent()Landroid/view/ViewParent;

    move-result-object v1

    check-cast v1, Landroid/view/View;

    invoke-virtual {v1}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    move-result-object v1

    check-cast v1, Landroid/view/View;

    invoke-virtual {v1}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    check-cast v0, Lcom/android/videoeditor/widgets/TimelineHorizontalScrollView;

    iget-object v1, p0, Lcom/android/videoeditor/widgets/AudioTrackView;->mScrollListener:Lcom/android/videoeditor/widgets/ScrollViewListener;

    invoke-virtual {v0, v1}, Lcom/android/videoeditor/widgets/TimelineHorizontalScrollView;->removeScrollListener(Lcom/android/videoeditor/widgets/ScrollViewListener;)V

    return-void
.end method

.method protected onDraw(Landroid/graphics/Canvas;)V
    .locals 17
    .param p1    # Landroid/graphics/Canvas;

    invoke-super/range {p0 .. p1}, Landroid/view/View;->onDraw(Landroid/graphics/Canvas;)V

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/android/videoeditor/widgets/AudioTrackView;->mWaveformData:Landroid/media/videoeditor/WaveformData;

    if-nez v1, :cond_1

    move-object/from16 v0, p0

    iget v1, v0, Lcom/android/videoeditor/widgets/AudioTrackView;->mProgress:I

    if-ltz v1, :cond_0

    invoke-virtual/range {p0 .. p0}, Lcom/android/videoeditor/widgets/AudioTrackView;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, Lcom/android/videoeditor/widgets/ProgressBar;->getProgressBar(Landroid/content/Context;)Lcom/android/videoeditor/widgets/ProgressBar;

    move-result-object v1

    move-object/from16 v0, p0

    iget v3, v0, Lcom/android/videoeditor/widgets/AudioTrackView;->mProgress:I

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/videoeditor/widgets/AudioTrackView;->mProgressDestRect:Landroid/graphics/Rect;

    invoke-virtual/range {p0 .. p0}, Lcom/android/videoeditor/widgets/AudioTrackView;->getPaddingLeft()I

    move-result v5

    invoke-virtual/range {p0 .. p0}, Lcom/android/videoeditor/widgets/AudioTrackView;->getWidth()I

    move-result v2

    invoke-virtual/range {p0 .. p0}, Lcom/android/videoeditor/widgets/AudioTrackView;->getPaddingRight()I

    move-result v6

    sub-int v6, v2, v6

    move-object/from16 v2, p1

    invoke-virtual/range {v1 .. v6}, Lcom/android/videoeditor/widgets/ProgressBar;->draw(Landroid/graphics/Canvas;ILandroid/graphics/Rect;II)V

    :cond_0
    return-void

    :cond_1
    move-object/from16 v0, p0

    iget-wide v1, v0, Lcom/android/videoeditor/widgets/AudioTrackView;->mTimelineDurationMs:J

    const-wide/16 v3, 0x0

    cmp-long v1, v1, v3

    if-lez v1, :cond_0

    invoke-virtual/range {p0 .. p0}, Lcom/android/videoeditor/widgets/AudioTrackView;->getTag()Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Lcom/android/videoeditor/service/MovieAudioTrack;

    invoke-virtual {v7}, Lcom/android/videoeditor/service/MovieAudioTrack;->getBoundaryBeginTime()J

    move-result-wide v1

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/android/videoeditor/widgets/AudioTrackView;->mWaveformData:Landroid/media/videoeditor/WaveformData;

    invoke-virtual {v3}, Landroid/media/videoeditor/WaveformData;->getFrameDuration()I

    move-result v3

    int-to-long v3, v3

    div-long/2addr v1, v3

    long-to-int v15, v1

    invoke-virtual {v7}, Lcom/android/videoeditor/service/MovieAudioTrack;->getTimelineDuration()J

    move-result-wide v1

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/android/videoeditor/widgets/AudioTrackView;->mWaveformData:Landroid/media/videoeditor/WaveformData;

    invoke-virtual {v3}, Landroid/media/videoeditor/WaveformData;->getFrameDuration()I

    move-result v3

    int-to-long v3, v3

    div-long/2addr v1, v3

    long-to-int v13, v1

    invoke-virtual/range {p0 .. p0}, Lcom/android/videoeditor/widgets/AudioTrackView;->getHeight()I

    move-result v1

    div-int/lit8 v8, v1, 0x2

    move-object/from16 v0, p0

    iget v1, v0, Lcom/android/videoeditor/widgets/AudioTrackView;->mScrollX:I

    move-object/from16 v0, p0

    iget v2, v0, Lcom/android/videoeditor/widgets/AudioTrackView;->mScreenWidth:I

    div-int/lit8 v2, v2, 0x2

    sub-int/2addr v1, v2

    invoke-virtual/range {p0 .. p0}, Lcom/android/videoeditor/widgets/AudioTrackView;->getPaddingLeft()I

    move-result v2

    invoke-static {v1, v2}, Ljava/lang/Math;->max(II)I

    move-result v14

    move-object/from16 v0, p0

    iget v1, v0, Lcom/android/videoeditor/widgets/AudioTrackView;->mScrollX:I

    move-object/from16 v0, p0

    iget v2, v0, Lcom/android/videoeditor/widgets/AudioTrackView;->mScreenWidth:I

    add-int/2addr v1, v2

    invoke-virtual/range {p0 .. p0}, Lcom/android/videoeditor/widgets/AudioTrackView;->getWidth()I

    move-result v2

    invoke-virtual/range {p0 .. p0}, Lcom/android/videoeditor/widgets/AudioTrackView;->getPaddingRight()I

    move-result v3

    sub-int/2addr v2, v3

    invoke-static {v1, v2}, Ljava/lang/Math;->min(II)I

    move-result v12

    invoke-virtual {v7}, Lcom/android/videoeditor/service/MovieAudioTrack;->isAppLooping()Z

    move-result v1

    if-eqz v1, :cond_3

    move-object/from16 v0, p0

    iget-wide v1, v0, Lcom/android/videoeditor/widgets/AudioTrackView;->mTimelineDurationMs:J

    long-to-float v2, v1

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/android/videoeditor/widgets/AudioTrackView;->mWaveformData:Landroid/media/videoeditor/WaveformData;

    invoke-virtual {v1}, Landroid/media/videoeditor/WaveformData;->getFrameDuration()I

    move-result v3

    invoke-virtual/range {p0 .. p0}, Lcom/android/videoeditor/widgets/AudioTrackView;->getParent()Landroid/view/ViewParent;

    move-result-object v1

    check-cast v1, Landroid/view/View;

    invoke-virtual {v1}, Landroid/view/View;->getWidth()I

    move-result v1

    move-object/from16 v0, p0

    iget v4, v0, Lcom/android/videoeditor/widgets/AudioTrackView;->mScreenWidth:I

    sub-int/2addr v1, v4

    mul-int/2addr v1, v3

    int-to-float v1, v1

    div-float v9, v2, v1

    move v10, v14

    :goto_0
    if-ge v10, v12, :cond_0

    int-to-float v1, v10

    mul-float/2addr v1, v9

    float-to-int v1, v1

    add-int v11, v15, v1

    rem-int/2addr v11, v13

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/android/videoeditor/widgets/AudioTrackView;->mNormalizedGains:[D

    aget-wide v1, v1, v11

    double-to-int v1, v1

    int-to-short v0, v1

    move/from16 v16, v0

    int-to-float v2, v10

    sub-int v1, v8, v16

    int-to-float v3, v1

    int-to-float v4, v10

    add-int/lit8 v1, v8, 0x1

    add-int v1, v1, v16

    int-to-float v5, v1

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/android/videoeditor/widgets/AudioTrackView;->mLinePaint:Landroid/graphics/Paint;

    move-object/from16 v1, p1

    invoke-virtual/range {v1 .. v6}, Landroid/graphics/Canvas;->drawLine(FFFFLandroid/graphics/Paint;)V

    if-ne v11, v15, :cond_2

    int-to-float v2, v10

    invoke-virtual/range {p0 .. p0}, Lcom/android/videoeditor/widgets/AudioTrackView;->getPaddingTop()I

    move-result v1

    int-to-float v3, v1

    int-to-float v4, v10

    invoke-virtual/range {p0 .. p0}, Lcom/android/videoeditor/widgets/AudioTrackView;->getHeight()I

    move-result v1

    invoke-virtual/range {p0 .. p0}, Lcom/android/videoeditor/widgets/AudioTrackView;->getPaddingBottom()I

    move-result v5

    sub-int/2addr v1, v5

    int-to-float v5, v1

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/android/videoeditor/widgets/AudioTrackView;->mLinePaint:Landroid/graphics/Paint;

    move-object/from16 v1, p1

    invoke-virtual/range {v1 .. v6}, Landroid/graphics/Canvas;->drawLine(FFFFLandroid/graphics/Paint;)V

    :cond_2
    add-int/lit8 v10, v10, 0x1

    goto :goto_0

    :cond_3
    invoke-virtual {v7}, Lcom/android/videoeditor/service/MovieAudioTrack;->getTimelineDuration()J

    move-result-wide v1

    long-to-float v1, v1

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/videoeditor/widgets/AudioTrackView;->mWaveformData:Landroid/media/videoeditor/WaveformData;

    invoke-virtual {v2}, Landroid/media/videoeditor/WaveformData;->getFrameDuration()I

    move-result v2

    invoke-virtual/range {p0 .. p0}, Lcom/android/videoeditor/widgets/AudioTrackView;->getWidth()I

    move-result v3

    mul-int/2addr v2, v3

    int-to-float v2, v2

    div-float v9, v1, v2

    move v10, v14

    :goto_1
    if-ge v10, v12, :cond_0

    int-to-float v1, v10

    mul-float/2addr v1, v9

    float-to-int v1, v1

    add-int v11, v15, v1

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/android/videoeditor/widgets/AudioTrackView;->mNormalizedGains:[D

    aget-wide v1, v1, v11

    double-to-int v1, v1

    int-to-short v0, v1

    move/from16 v16, v0

    int-to-float v2, v10

    sub-int v1, v8, v16

    int-to-float v3, v1

    int-to-float v4, v10

    add-int/lit8 v1, v8, 0x1

    add-int v1, v1, v16

    int-to-float v5, v1

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/android/videoeditor/widgets/AudioTrackView;->mLinePaint:Landroid/graphics/Paint;

    move-object/from16 v1, p1

    invoke-virtual/range {v1 .. v6}, Landroid/graphics/Canvas;->drawLine(FFFFLandroid/graphics/Paint;)V

    add-int/lit8 v10, v10, 0x1

    goto :goto_1
.end method

.method public onTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 1
    .param p1    # Landroid/view/MotionEvent;

    iget-object v0, p0, Lcom/android/videoeditor/widgets/AudioTrackView;->mSimpleGestureDetector:Landroid/view/GestureDetector;

    invoke-virtual {v0, p1}, Landroid/view/GestureDetector;->onTouchEvent(Landroid/view/MotionEvent;)Z

    invoke-super {p0, p1}, Landroid/view/View;->onTouchEvent(Landroid/view/MotionEvent;)Z

    const/4 v0, 0x1

    return v0
.end method

.method public setGestureListener(Lcom/android/videoeditor/widgets/ItemSimpleGestureListener;)V
    .locals 0
    .param p1    # Lcom/android/videoeditor/widgets/ItemSimpleGestureListener;

    iput-object p1, p0, Lcom/android/videoeditor/widgets/AudioTrackView;->mGestureListener:Lcom/android/videoeditor/widgets/ItemSimpleGestureListener;

    return-void
.end method

.method public setProgress(I)V
    .locals 0
    .param p1    # I

    iput p1, p0, Lcom/android/videoeditor/widgets/AudioTrackView;->mProgress:I

    invoke-virtual {p0}, Lcom/android/videoeditor/widgets/AudioTrackView;->invalidate()V

    return-void
.end method

.method public setWaveformData(Landroid/media/videoeditor/WaveformData;)V
    .locals 29
    .param p1    # Landroid/media/videoeditor/WaveformData;

    move-object/from16 v0, p1

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/android/videoeditor/widgets/AudioTrackView;->mWaveformData:Landroid/media/videoeditor/WaveformData;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/videoeditor/widgets/AudioTrackView;->mWaveformData:Landroid/media/videoeditor/WaveformData;

    move-object/from16 v22, v0

    invoke-virtual/range {v22 .. v22}, Landroid/media/videoeditor/WaveformData;->getFramesCount()I

    move-result v11

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/videoeditor/widgets/AudioTrackView;->mWaveformData:Landroid/media/videoeditor/WaveformData;

    move-object/from16 v22, v0

    invoke-virtual/range {v22 .. v22}, Landroid/media/videoeditor/WaveformData;->getFrameGains()[S

    move-result-object v3

    new-array v0, v11, [D

    move-object/from16 v18, v0

    const/16 v22, 0x1

    move/from16 v0, v22

    if-ne v11, v0, :cond_2

    const/16 v22, 0x0

    const/16 v23, 0x0

    aget-short v23, v3, v23

    move/from16 v0, v23

    int-to-double v0, v0

    move-wide/from16 v23, v0

    aput-wide v23, v18, v22

    :cond_0
    :goto_0
    const-wide/high16 v7, 0x3ff0000000000000L

    const/4 v6, 0x0

    :goto_1
    if-ge v6, v11, :cond_5

    aget-wide v22, v18, v6

    cmpl-double v22, v22, v7

    if-lez v22, :cond_1

    aget-wide v7, v18, v6

    :cond_1
    add-int/lit8 v6, v6, 0x1

    goto :goto_1

    :cond_2
    const/16 v22, 0x2

    move/from16 v0, v22

    if-ne v11, v0, :cond_3

    const/16 v22, 0x0

    const/16 v23, 0x0

    aget-short v23, v3, v23

    move/from16 v0, v23

    int-to-double v0, v0

    move-wide/from16 v23, v0

    aput-wide v23, v18, v22

    const/16 v22, 0x1

    const/16 v23, 0x1

    aget-short v23, v3, v23

    move/from16 v0, v23

    int-to-double v0, v0

    move-wide/from16 v23, v0

    aput-wide v23, v18, v22

    goto :goto_0

    :cond_3
    const/16 v22, 0x2

    move/from16 v0, v22

    if-le v11, v0, :cond_0

    const/16 v22, 0x0

    const/16 v23, 0x0

    aget-short v23, v3, v23

    move/from16 v0, v23

    int-to-double v0, v0

    move-wide/from16 v23, v0

    const-wide/high16 v25, 0x4000000000000000L

    div-double v23, v23, v25

    const/16 v25, 0x1

    aget-short v25, v3, v25

    move/from16 v0, v25

    int-to-double v0, v0

    move-wide/from16 v25, v0

    const-wide/high16 v27, 0x4000000000000000L

    div-double v25, v25, v27

    add-double v23, v23, v25

    aput-wide v23, v18, v22

    const/4 v6, 0x1

    :goto_2
    add-int/lit8 v22, v11, -0x1

    move/from16 v0, v22

    if-ge v6, v0, :cond_4

    add-int/lit8 v22, v6, -0x1

    aget-short v22, v3, v22

    move/from16 v0, v22

    int-to-double v0, v0

    move-wide/from16 v22, v0

    const-wide/high16 v24, 0x4008000000000000L

    div-double v22, v22, v24

    aget-short v24, v3, v6

    move/from16 v0, v24

    int-to-double v0, v0

    move-wide/from16 v24, v0

    const-wide/high16 v26, 0x4008000000000000L

    div-double v24, v24, v26

    add-double v22, v22, v24

    add-int/lit8 v24, v6, 0x1

    aget-short v24, v3, v24

    move/from16 v0, v24

    int-to-double v0, v0

    move-wide/from16 v24, v0

    const-wide/high16 v26, 0x4008000000000000L

    div-double v24, v24, v26

    add-double v22, v22, v24

    aput-wide v22, v18, v6

    add-int/lit8 v6, v6, 0x1

    goto :goto_2

    :cond_4
    add-int/lit8 v22, v11, -0x1

    add-int/lit8 v23, v11, -0x2

    aget-short v23, v3, v23

    move/from16 v0, v23

    int-to-double v0, v0

    move-wide/from16 v23, v0

    const-wide/high16 v25, 0x4000000000000000L

    div-double v23, v23, v25

    add-int/lit8 v25, v11, -0x1

    aget-short v25, v3, v25

    move/from16 v0, v25

    int-to-double v0, v0

    move-wide/from16 v25, v0

    const-wide/high16 v27, 0x4000000000000000L

    div-double v25, v25, v27

    add-double v23, v23, v25

    aput-wide v23, v18, v22

    goto/16 :goto_0

    :cond_5
    const-wide/high16 v15, 0x3ff0000000000000L

    const-wide v22, 0x406fe00000000000L

    cmpl-double v22, v7, v22

    if-lez v22, :cond_6

    const-wide v22, 0x406fe00000000000L

    div-double v15, v22, v7

    :cond_6
    const-wide/16 v7, 0x0

    const/16 v22, 0x100

    move/from16 v0, v22

    new-array v4, v0, [I

    const/4 v6, 0x0

    :goto_3
    if-ge v6, v11, :cond_a

    aget-wide v22, v18, v6

    mul-double v22, v22, v15

    move-wide/from16 v0, v22

    double-to-int v0, v0

    move/from16 v17, v0

    if-gez v17, :cond_7

    const/16 v17, 0x0

    :cond_7
    const/16 v22, 0xff

    move/from16 v0, v17

    move/from16 v1, v22

    if-le v0, v1, :cond_8

    const/16 v17, 0xff

    :cond_8
    move/from16 v0, v17

    int-to-double v0, v0

    move-wide/from16 v22, v0

    cmpl-double v22, v22, v7

    if-lez v22, :cond_9

    move/from16 v0, v17

    int-to-double v7, v0

    :cond_9
    aget v22, v4, v17

    add-int/lit8 v22, v22, 0x1

    aput v22, v4, v17

    add-int/lit8 v6, v6, 0x1

    goto :goto_3

    :cond_a
    const-wide/16 v9, 0x0

    const/16 v19, 0x0

    :goto_4
    const-wide v22, 0x406fe00000000000L

    cmpg-double v22, v9, v22

    if-gez v22, :cond_b

    div-int/lit8 v22, v11, 0x14

    move/from16 v0, v19

    move/from16 v1, v22

    if-ge v0, v1, :cond_b

    double-to-int v0, v9

    move/from16 v22, v0

    aget v22, v4, v22

    add-int v19, v19, v22

    const-wide/high16 v22, 0x3ff0000000000000L

    add-double v9, v9, v22

    goto :goto_4

    :cond_b
    const/16 v19, 0x0

    :goto_5
    const-wide/high16 v22, 0x4000000000000000L

    cmpl-double v22, v7, v22

    if-lez v22, :cond_c

    div-int/lit8 v22, v11, 0x64

    move/from16 v0, v19

    move/from16 v1, v22

    if-ge v0, v1, :cond_c

    double-to-int v0, v7

    move/from16 v22, v0

    aget v22, v4, v22

    add-int v19, v19, v22

    const-wide/high16 v22, 0x3ff0000000000000L

    sub-double v7, v7, v22

    goto :goto_5

    :cond_c
    invoke-virtual/range {p0 .. p0}, Lcom/android/videoeditor/widgets/AudioTrackView;->getResources()Landroid/content/res/Resources;

    move-result-object v22

    const v23, 0x7f07000c

    invoke-virtual/range {v22 .. v23}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v22

    invoke-virtual/range {p0 .. p0}, Lcom/android/videoeditor/widgets/AudioTrackView;->getPaddingTop()I

    move-result v23

    move/from16 v0, v23

    int-to-float v0, v0

    move/from16 v23, v0

    sub-float v22, v22, v23

    invoke-virtual/range {p0 .. p0}, Lcom/android/videoeditor/widgets/AudioTrackView;->getPaddingBottom()I

    move-result v23

    move/from16 v0, v23

    int-to-float v0, v0

    move/from16 v23, v0

    sub-float v22, v22, v23

    const/high16 v23, 0x40800000

    sub-float v22, v22, v23

    const/high16 v23, 0x40000000

    div-float v22, v22, v23

    move/from16 v0, v22

    float-to-int v5, v0

    invoke-virtual/range {p0 .. p0}, Lcom/android/videoeditor/widgets/AudioTrackView;->getTag()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/android/videoeditor/service/MovieAudioTrack;

    invoke-virtual {v2}, Lcom/android/videoeditor/service/MovieAudioTrack;->getDuration()J

    move-result-wide v22

    move-wide/from16 v0, v22

    long-to-int v0, v0

    move/from16 v22, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/videoeditor/widgets/AudioTrackView;->mWaveformData:Landroid/media/videoeditor/WaveformData;

    move-object/from16 v23, v0

    invoke-virtual/range {v23 .. v23}, Landroid/media/videoeditor/WaveformData;->getFrameDuration()I

    move-result v23

    div-int v12, v22, v23

    invoke-static {v12, v11}, Ljava/lang/Math;->max(II)I

    move-result v22

    move/from16 v0, v22

    new-array v0, v0, [D

    move-object/from16 v22, v0

    move-object/from16 v0, v22

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/android/videoeditor/widgets/AudioTrackView;->mNormalizedGains:[D

    sub-double v13, v7, v9

    const/4 v6, 0x0

    :goto_6
    if-ge v6, v11, :cond_f

    aget-wide v22, v18, v6

    mul-double v22, v22, v15

    sub-double v22, v22, v9

    div-double v20, v22, v13

    const-wide/16 v22, 0x0

    cmpg-double v22, v20, v22

    if-gez v22, :cond_d

    const-wide/16 v20, 0x0

    :cond_d
    const-wide/high16 v22, 0x3ff0000000000000L

    cmpl-double v22, v20, v22

    if-lez v22, :cond_e

    const-wide/high16 v20, 0x3ff0000000000000L

    :cond_e
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/videoeditor/widgets/AudioTrackView;->mNormalizedGains:[D

    move-object/from16 v22, v0

    mul-double v23, v20, v20

    int-to-double v0, v5

    move-wide/from16 v25, v0

    mul-double v23, v23, v25

    aput-wide v23, v22, v6

    add-int/lit8 v6, v6, 0x1

    goto :goto_6

    :cond_f
    return-void
.end method

.method public updateTimelineDuration(J)V
    .locals 0
    .param p1    # J

    iput-wide p1, p0, Lcom/android/videoeditor/widgets/AudioTrackView;->mTimelineDurationMs:J

    return-void
.end method
