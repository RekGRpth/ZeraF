.class Lcom/android/videoeditor/widgets/MediaLinearLayout$1;
.super Ljava/lang/Object;
.source "MediaLinearLayout.java"

# interfaces
.implements Lcom/android/videoeditor/widgets/ItemSimpleGestureListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/android/videoeditor/widgets/MediaLinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/videoeditor/widgets/MediaLinearLayout;


# direct methods
.method constructor <init>(Lcom/android/videoeditor/widgets/MediaLinearLayout;)V
    .locals 0

    iput-object p1, p0, Lcom/android/videoeditor/widgets/MediaLinearLayout$1;->this$0:Lcom/android/videoeditor/widgets/MediaLinearLayout;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onLongPress(Landroid/view/View;Landroid/view/MotionEvent;)V
    .locals 5
    .param p1    # Landroid/view/View;
    .param p2    # Landroid/view/MotionEvent;

    iget-object v1, p0, Lcom/android/videoeditor/widgets/MediaLinearLayout$1;->this$0:Lcom/android/videoeditor/widgets/MediaLinearLayout;

    # getter for: Lcom/android/videoeditor/widgets/MediaLinearLayout;->mPlaybackInProgress:Z
    invoke-static {v1}, Lcom/android/videoeditor/widgets/MediaLinearLayout;->access$200(Lcom/android/videoeditor/widgets/MediaLinearLayout;)Z

    move-result v1

    if-eqz v1, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    invoke-virtual {p1}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/videoeditor/service/MovieMediaItem;

    iget-object v1, p0, Lcom/android/videoeditor/widgets/MediaLinearLayout$1;->this$0:Lcom/android/videoeditor/widgets/MediaLinearLayout;

    # getter for: Lcom/android/videoeditor/widgets/MediaLinearLayout;->mProject:Lcom/android/videoeditor/service/VideoEditorProject;
    invoke-static {v1}, Lcom/android/videoeditor/widgets/MediaLinearLayout;->access$100(Lcom/android/videoeditor/widgets/MediaLinearLayout;)Lcom/android/videoeditor/service/VideoEditorProject;

    move-result-object v1

    invoke-virtual {v1}, Lcom/android/videoeditor/service/VideoEditorProject;->getMediaItemCount()I

    move-result v1

    const/4 v2, 0x1

    if-le v1, v2, :cond_2

    const-string v1, "File"

    invoke-virtual {v0}, Lcom/android/videoeditor/service/MovieMediaItem;->getFilename()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/content/ClipData;->newPlainText(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Landroid/content/ClipData;

    move-result-object v2

    move-object v1, p1

    check-cast v1, Lcom/android/videoeditor/widgets/MediaItemView;

    invoke-virtual {v1}, Lcom/android/videoeditor/widgets/MediaItemView;->getShadowBuilder()Landroid/view/View$DragShadowBuilder;

    move-result-object v1

    invoke-virtual {v0}, Lcom/android/videoeditor/service/MovieMediaItem;->getId()Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x0

    invoke-virtual {p1, v2, v1, v3, v4}, Landroid/view/View;->startDrag(Landroid/content/ClipData;Landroid/view/View$DragShadowBuilder;Ljava/lang/Object;I)Z

    :cond_2
    iget-object v1, p0, Lcom/android/videoeditor/widgets/MediaLinearLayout$1;->this$0:Lcom/android/videoeditor/widgets/MediaLinearLayout;

    # invokes: Lcom/android/videoeditor/widgets/MediaLinearLayout;->select(Landroid/view/View;)V
    invoke-static {v1, p1}, Lcom/android/videoeditor/widgets/MediaLinearLayout;->access$1500(Lcom/android/videoeditor/widgets/MediaLinearLayout;Landroid/view/View;)V

    iget-object v1, p0, Lcom/android/videoeditor/widgets/MediaLinearLayout$1;->this$0:Lcom/android/videoeditor/widgets/MediaLinearLayout;

    # getter for: Lcom/android/videoeditor/widgets/MediaLinearLayout;->mMediaItemActionMode:Landroid/view/ActionMode;
    invoke-static {v1}, Lcom/android/videoeditor/widgets/MediaLinearLayout;->access$000(Lcom/android/videoeditor/widgets/MediaLinearLayout;)Landroid/view/ActionMode;

    move-result-object v1

    if-nez v1, :cond_0

    iget-object v1, p0, Lcom/android/videoeditor/widgets/MediaLinearLayout$1;->this$0:Lcom/android/videoeditor/widgets/MediaLinearLayout;

    new-instance v2, Lcom/android/videoeditor/widgets/MediaLinearLayout$MediaItemActionModeCallback;

    iget-object v3, p0, Lcom/android/videoeditor/widgets/MediaLinearLayout$1;->this$0:Lcom/android/videoeditor/widgets/MediaLinearLayout;

    invoke-direct {v2, v3, v0}, Lcom/android/videoeditor/widgets/MediaLinearLayout$MediaItemActionModeCallback;-><init>(Lcom/android/videoeditor/widgets/MediaLinearLayout;Lcom/android/videoeditor/service/MovieMediaItem;)V

    invoke-virtual {v1, v2}, Lcom/android/videoeditor/widgets/MediaLinearLayout;->startActionMode(Landroid/view/ActionMode$Callback;)Landroid/view/ActionMode;

    goto :goto_0
.end method

.method public onSingleTapConfirmed(Landroid/view/View;ILandroid/view/MotionEvent;)Z
    .locals 4
    .param p1    # Landroid/view/View;
    .param p2    # I
    .param p3    # Landroid/view/MotionEvent;

    iget-object v2, p0, Lcom/android/videoeditor/widgets/MediaLinearLayout$1;->this$0:Lcom/android/videoeditor/widgets/MediaLinearLayout;

    # getter for: Lcom/android/videoeditor/widgets/MediaLinearLayout;->mPlaybackInProgress:Z
    invoke-static {v2}, Lcom/android/videoeditor/widgets/MediaLinearLayout;->access$200(Lcom/android/videoeditor/widgets/MediaLinearLayout;)Z

    move-result v2

    if-eqz v2, :cond_0

    const/4 v2, 0x0

    :goto_0
    return v2

    :cond_0
    packed-switch p2, :pswitch_data_0

    :cond_1
    :goto_1
    :pswitch_0
    iget-object v2, p0, Lcom/android/videoeditor/widgets/MediaLinearLayout$1;->this$0:Lcom/android/videoeditor/widgets/MediaLinearLayout;

    # invokes: Lcom/android/videoeditor/widgets/MediaLinearLayout;->select(Landroid/view/View;)V
    invoke-static {v2, p1}, Lcom/android/videoeditor/widgets/MediaLinearLayout;->access$1500(Lcom/android/videoeditor/widgets/MediaLinearLayout;Landroid/view/View;)V

    const/4 v2, 0x1

    goto :goto_0

    :pswitch_1
    invoke-virtual {p1}, Landroid/view/View;->isSelected()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-virtual {p1}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/videoeditor/service/MovieMediaItem;

    iget-object v2, p0, Lcom/android/videoeditor/widgets/MediaLinearLayout$1;->this$0:Lcom/android/videoeditor/widgets/MediaLinearLayout;

    # getter for: Lcom/android/videoeditor/widgets/MediaLinearLayout;->mProject:Lcom/android/videoeditor/service/VideoEditorProject;
    invoke-static {v2}, Lcom/android/videoeditor/widgets/MediaLinearLayout;->access$100(Lcom/android/videoeditor/widgets/MediaLinearLayout;)Lcom/android/videoeditor/service/VideoEditorProject;

    move-result-object v2

    invoke-virtual {v0}, Lcom/android/videoeditor/service/MovieMediaItem;->getId()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/android/videoeditor/service/VideoEditorProject;->getPreviousMediaItem(Ljava/lang/String;)Lcom/android/videoeditor/service/MovieMediaItem;

    move-result-object v1

    iget-object v2, p0, Lcom/android/videoeditor/widgets/MediaLinearLayout$1;->this$0:Lcom/android/videoeditor/widgets/MediaLinearLayout;

    # invokes: Lcom/android/videoeditor/widgets/MediaLinearLayout;->pickTransition(Lcom/android/videoeditor/service/MovieMediaItem;)Z
    invoke-static {v2, v1}, Lcom/android/videoeditor/widgets/MediaLinearLayout;->access$500(Lcom/android/videoeditor/widgets/MediaLinearLayout;Lcom/android/videoeditor/service/MovieMediaItem;)Z

    goto :goto_1

    :pswitch_2
    invoke-virtual {p1}, Landroid/view/View;->isSelected()Z

    move-result v2

    if-eqz v2, :cond_1

    iget-object v3, p0, Lcom/android/videoeditor/widgets/MediaLinearLayout$1;->this$0:Lcom/android/videoeditor/widgets/MediaLinearLayout;

    invoke-virtual {p1}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/android/videoeditor/service/MovieMediaItem;

    # invokes: Lcom/android/videoeditor/widgets/MediaLinearLayout;->pickTransition(Lcom/android/videoeditor/service/MovieMediaItem;)Z
    invoke-static {v3, v2}, Lcom/android/videoeditor/widgets/MediaLinearLayout;->access$500(Lcom/android/videoeditor/widgets/MediaLinearLayout;Lcom/android/videoeditor/service/MovieMediaItem;)Z

    goto :goto_1

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_0
        :pswitch_2
    .end packed-switch
.end method
