.class public Lcom/android/videoeditor/widgets/PlayheadView;
.super Landroid/view/View;
.source "PlayheadView.java"


# instance fields
.field private final mLinePaint:Landroid/graphics/Paint;

.field private mProject:Lcom/android/videoeditor/service/VideoEditorProject;

.field private final mScreenWidth:I

.field private final mScrollListener:Lcom/android/videoeditor/widgets/ScrollViewListener;

.field private mScrollX:I

.field private final mTextPaint:Landroid/graphics/Paint;

.field private final mTicksHeight:I

.field private final mTimeTextSize:F


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 2
    .param p1    # Landroid/content/Context;

    const/4 v0, 0x0

    const/4 v1, 0x0

    invoke-direct {p0, p1, v0, v1}, Lcom/android/videoeditor/widgets/PlayheadView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/util/AttributeSet;

    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/android/videoeditor/widgets/PlayheadView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 6
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/util/AttributeSet;
    .param p3    # I

    const v5, 0x7f060004

    invoke-direct {p0, p1, p2, p3}, Landroid/view/View;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    new-instance v3, Landroid/graphics/Paint;

    invoke-direct {v3}, Landroid/graphics/Paint;-><init>()V

    iput-object v3, p0, Lcom/android/videoeditor/widgets/PlayheadView;->mLinePaint:Landroid/graphics/Paint;

    iget-object v3, p0, Lcom/android/videoeditor/widgets/PlayheadView;->mLinePaint:Landroid/graphics/Paint;

    invoke-virtual {v2, v5}, Landroid/content/res/Resources;->getColor(I)I

    move-result v4

    invoke-virtual {v3, v4}, Landroid/graphics/Paint;->setColor(I)V

    iget-object v3, p0, Lcom/android/videoeditor/widgets/PlayheadView;->mLinePaint:Landroid/graphics/Paint;

    const/high16 v4, 0x40000000

    invoke-virtual {v3, v4}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    iget-object v3, p0, Lcom/android/videoeditor/widgets/PlayheadView;->mLinePaint:Landroid/graphics/Paint;

    sget-object v4, Landroid/graphics/Paint$Style;->STROKE:Landroid/graphics/Paint$Style;

    invoke-virtual {v3, v4}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    new-instance v3, Landroid/graphics/Paint;

    invoke-direct {v3}, Landroid/graphics/Paint;-><init>()V

    iput-object v3, p0, Lcom/android/videoeditor/widgets/PlayheadView;->mTextPaint:Landroid/graphics/Paint;

    iget-object v3, p0, Lcom/android/videoeditor/widgets/PlayheadView;->mTextPaint:Landroid/graphics/Paint;

    const/4 v4, 0x1

    invoke-virtual {v3, v4}, Landroid/graphics/Paint;->setAntiAlias(Z)V

    iget-object v3, p0, Lcom/android/videoeditor/widgets/PlayheadView;->mTextPaint:Landroid/graphics/Paint;

    invoke-virtual {v2, v5}, Landroid/content/res/Resources;->getColor(I)I

    move-result v4

    invoke-virtual {v3, v4}, Landroid/graphics/Paint;->setColor(I)V

    const v3, 0x7f07000e

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v3

    iput v3, p0, Lcom/android/videoeditor/widgets/PlayheadView;->mTimeTextSize:F

    iget-object v3, p0, Lcom/android/videoeditor/widgets/PlayheadView;->mTextPaint:Landroid/graphics/Paint;

    iget v4, p0, Lcom/android/videoeditor/widgets/PlayheadView;->mTimeTextSize:F

    invoke-virtual {v3, v4}, Landroid/graphics/Paint;->setTextSize(F)V

    const v3, 0x7f070017

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v3

    float-to-int v3, v3

    iput v3, p0, Lcom/android/videoeditor/widgets/PlayheadView;->mTicksHeight:I

    const-string v3, "window"

    invoke-virtual {p1, v3}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/view/WindowManager;

    invoke-interface {v3}, Landroid/view/WindowManager;->getDefaultDisplay()Landroid/view/Display;

    move-result-object v0

    new-instance v1, Landroid/util/DisplayMetrics;

    invoke-direct {v1}, Landroid/util/DisplayMetrics;-><init>()V

    invoke-virtual {v0, v1}, Landroid/view/Display;->getMetrics(Landroid/util/DisplayMetrics;)V

    iget v3, v1, Landroid/util/DisplayMetrics;->widthPixels:I

    iput v3, p0, Lcom/android/videoeditor/widgets/PlayheadView;->mScreenWidth:I

    new-instance v3, Lcom/android/videoeditor/widgets/PlayheadView$1;

    invoke-direct {v3, p0}, Lcom/android/videoeditor/widgets/PlayheadView$1;-><init>(Lcom/android/videoeditor/widgets/PlayheadView;)V

    iput-object v3, p0, Lcom/android/videoeditor/widgets/PlayheadView;->mScrollListener:Lcom/android/videoeditor/widgets/ScrollViewListener;

    return-void
.end method

.method static synthetic access$002(Lcom/android/videoeditor/widgets/PlayheadView;I)I
    .locals 0
    .param p0    # Lcom/android/videoeditor/widgets/PlayheadView;
    .param p1    # I

    iput p1, p0, Lcom/android/videoeditor/widgets/PlayheadView;->mScrollX:I

    return p1
.end method


# virtual methods
.method protected onAttachedToWindow()V
    .locals 2

    invoke-virtual {p0}, Lcom/android/videoeditor/widgets/PlayheadView;->getParent()Landroid/view/ViewParent;

    move-result-object v1

    check-cast v1, Landroid/view/View;

    invoke-virtual {v1}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    check-cast v0, Lcom/android/videoeditor/widgets/TimelineHorizontalScrollView;

    invoke-virtual {v0}, Lcom/android/videoeditor/widgets/TimelineHorizontalScrollView;->getScrollX()I

    move-result v1

    iput v1, p0, Lcom/android/videoeditor/widgets/PlayheadView;->mScrollX:I

    iget-object v1, p0, Lcom/android/videoeditor/widgets/PlayheadView;->mScrollListener:Lcom/android/videoeditor/widgets/ScrollViewListener;

    invoke-virtual {v0, v1}, Lcom/android/videoeditor/widgets/TimelineHorizontalScrollView;->addScrollListener(Lcom/android/videoeditor/widgets/ScrollViewListener;)V

    return-void
.end method

.method protected onDetachedFromWindow()V
    .locals 2

    invoke-virtual {p0}, Lcom/android/videoeditor/widgets/PlayheadView;->getParent()Landroid/view/ViewParent;

    move-result-object v1

    check-cast v1, Landroid/view/View;

    invoke-virtual {v1}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    check-cast v0, Lcom/android/videoeditor/widgets/TimelineHorizontalScrollView;

    iget-object v1, p0, Lcom/android/videoeditor/widgets/PlayheadView;->mScrollListener:Lcom/android/videoeditor/widgets/ScrollViewListener;

    invoke-virtual {v0, v1}, Lcom/android/videoeditor/widgets/TimelineHorizontalScrollView;->removeScrollListener(Lcom/android/videoeditor/widgets/ScrollViewListener;)V

    return-void
.end method

.method protected onDraw(Landroid/graphics/Canvas;)V
    .locals 24
    .param p1    # Landroid/graphics/Canvas;

    invoke-super/range {p0 .. p1}, Landroid/view/View;->onDraw(Landroid/graphics/Canvas;)V

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/videoeditor/widgets/PlayheadView;->mProject:Lcom/android/videoeditor/service/VideoEditorProject;

    if-nez v2, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/videoeditor/widgets/PlayheadView;->mProject:Lcom/android/videoeditor/service/VideoEditorProject;

    invoke-virtual {v2}, Lcom/android/videoeditor/service/VideoEditorProject;->computeDuration()J

    move-result-wide v9

    const-wide/16 v4, 0x3e8

    div-long v11, v9, v4

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/videoeditor/widgets/PlayheadView;->mTextPaint:Landroid/graphics/Paint;

    invoke-virtual {v2}, Landroid/graphics/Paint;->getFontMetrics()Landroid/graphics/Paint$FontMetrics;

    move-result-object v2

    iget v2, v2, Landroid/graphics/Paint$FontMetrics;->top:F

    neg-float v2, v2

    float-to-int v0, v2

    move/from16 v23, v0

    const-wide/16 v4, 0x0

    cmp-long v2, v9, v4

    if-eqz v2, :cond_2

    const-wide/16 v4, 0x0

    cmp-long v2, v11, v4

    if-nez v2, :cond_3

    :cond_2
    invoke-virtual/range {p0 .. p0}, Lcom/android/videoeditor/widgets/PlayheadView;->getContext()Landroid/content/Context;

    move-result-object v2

    const-wide/16 v4, 0x0

    invoke-static {v2, v4, v5}, Lcom/android/videoeditor/util/StringUtils;->getSimpleTimestampAsString(Landroid/content/Context;J)Ljava/lang/String;

    move-result-object v20

    invoke-virtual/range {p0 .. p0}, Lcom/android/videoeditor/widgets/PlayheadView;->getWidth()I

    move-result v2

    int-to-float v2, v2

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/videoeditor/widgets/PlayheadView;->mTextPaint:Landroid/graphics/Paint;

    move-object/from16 v0, v20

    invoke-virtual {v4, v0}, Landroid/graphics/Paint;->measureText(Ljava/lang/String;)F

    move-result v4

    sub-float/2addr v2, v4

    const/high16 v4, 0x40000000

    div-float/2addr v2, v4

    float-to-int v0, v2

    move/from16 v22, v0

    move/from16 v0, v22

    int-to-float v2, v0

    move/from16 v0, v23

    int-to-float v4, v0

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/android/videoeditor/widgets/PlayheadView;->mTextPaint:Landroid/graphics/Paint;

    move-object/from16 v0, p1

    move-object/from16 v1, v20

    invoke-virtual {v0, v1, v2, v4, v5}, Landroid/graphics/Canvas;->drawText(Ljava/lang/String;FFLandroid/graphics/Paint;)V

    goto :goto_0

    :cond_3
    invoke-virtual/range {p0 .. p0}, Lcom/android/videoeditor/widgets/PlayheadView;->getWidth()I

    move-result v2

    move-object/from16 v0, p0

    iget v4, v0, Lcom/android/videoeditor/widgets/PlayheadView;->mScreenWidth:I

    sub-int v21, v2, v4

    move/from16 v0, v21

    int-to-long v4, v0

    div-long/2addr v4, v11

    long-to-int v14, v4

    const/4 v2, 0x4

    if-ge v14, v2, :cond_4

    const-wide/32 v18, 0x3a980

    :goto_1
    move/from16 v0, v21

    int-to-long v4, v0

    mul-long v4, v4, v18

    long-to-float v2, v4

    long-to-float v4, v9

    div-float v15, v2, v4

    move-object/from16 v0, p0

    iget v2, v0, Lcom/android/videoeditor/widgets/PlayheadView;->mScrollX:I

    int-to-float v2, v2

    move-object/from16 v0, p0

    iget v4, v0, Lcom/android/videoeditor/widgets/PlayheadView;->mScrollX:I

    move-object/from16 v0, p0

    iget v5, v0, Lcom/android/videoeditor/widgets/PlayheadView;->mScreenWidth:I

    div-int/lit8 v5, v5, 0x2

    sub-int/2addr v4, v5

    int-to-float v4, v4

    rem-float/2addr v4, v15

    sub-float/2addr v2, v4

    move-object/from16 v0, p0

    iget v4, v0, Lcom/android/videoeditor/widgets/PlayheadView;->mScreenWidth:I

    div-int/lit8 v4, v4, 0x2

    int-to-float v4, v4

    invoke-static {v2, v4}, Ljava/lang/Math;->max(FF)F

    move-result v17

    move-wide/from16 v0, v18

    long-to-float v2, v0

    move-object/from16 v0, p0

    iget v4, v0, Lcom/android/videoeditor/widgets/PlayheadView;->mScreenWidth:I

    div-int/lit8 v4, v4, 0x2

    int-to-float v4, v4

    sub-float v4, v17, v4

    mul-float/2addr v2, v4

    div-float v16, v2, v15

    invoke-static/range {v16 .. v16}, Ljava/lang/Math;->round(F)I

    move-result v2

    int-to-float v0, v2

    move/from16 v16, v0

    move-wide/from16 v0, v18

    long-to-float v2, v0

    rem-float v2, v16, v2

    sub-float v16, v16, v2

    invoke-virtual/range {p0 .. p0}, Lcom/android/videoeditor/widgets/PlayheadView;->getContext()Landroid/content/Context;

    move-result-object v8

    move-object/from16 v0, p0

    iget v2, v0, Lcom/android/videoeditor/widgets/PlayheadView;->mScrollX:I

    move-object/from16 v0, p0

    iget v4, v0, Lcom/android/videoeditor/widgets/PlayheadView;->mScreenWidth:I

    add-int/2addr v2, v4

    int-to-float v13, v2

    move/from16 v3, v17

    :goto_2
    cmpg-float v2, v3, v13

    if-gtz v2, :cond_0

    move/from16 v0, v16

    float-to-long v4, v0

    invoke-static {v8, v4, v5}, Lcom/android/videoeditor/util/StringUtils;->getSimpleTimestampAsString(Landroid/content/Context;J)Ljava/lang/String;

    move-result-object v20

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/videoeditor/widgets/PlayheadView;->mTextPaint:Landroid/graphics/Paint;

    move-object/from16 v0, v20

    invoke-virtual {v2, v0}, Landroid/graphics/Paint;->measureText(Ljava/lang/String;)F

    move-result v2

    const/high16 v4, 0x40000000

    div-float/2addr v2, v4

    sub-float v2, v3, v2

    float-to-int v0, v2

    move/from16 v22, v0

    move/from16 v0, v22

    int-to-float v2, v0

    move/from16 v0, v23

    int-to-float v4, v0

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/android/videoeditor/widgets/PlayheadView;->mTextPaint:Landroid/graphics/Paint;

    move-object/from16 v0, p1

    move-object/from16 v1, v20

    invoke-virtual {v0, v1, v2, v4, v5}, Landroid/graphics/Canvas;->drawText(Ljava/lang/String;FFLandroid/graphics/Paint;)V

    const/4 v4, 0x0

    move-object/from16 v0, p0

    iget v2, v0, Lcom/android/videoeditor/widgets/PlayheadView;->mTicksHeight:I

    int-to-float v6, v2

    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/android/videoeditor/widgets/PlayheadView;->mLinePaint:Landroid/graphics/Paint;

    move-object/from16 v2, p1

    move v5, v3

    invoke-virtual/range {v2 .. v7}, Landroid/graphics/Canvas;->drawLine(FFFFLandroid/graphics/Paint;)V

    add-float/2addr v3, v15

    move-wide/from16 v0, v18

    long-to-float v2, v0

    add-float v16, v16, v2

    goto :goto_2

    :cond_4
    const/4 v2, 0x6

    if-ge v14, v2, :cond_5

    const-wide/32 v18, 0x1d4c0

    goto/16 :goto_1

    :cond_5
    const/16 v2, 0xa

    if-ge v14, v2, :cond_6

    const-wide/32 v18, 0xea60

    goto/16 :goto_1

    :cond_6
    const/16 v2, 0x32

    if-ge v14, v2, :cond_7

    const-wide/16 v18, 0x2710

    goto/16 :goto_1

    :cond_7
    const/16 v2, 0xc8

    if-ge v14, v2, :cond_8

    const-wide/16 v18, 0x1388

    goto/16 :goto_1

    :cond_8
    const-wide/16 v18, 0x3e8

    goto/16 :goto_1
.end method

.method public setProject(Lcom/android/videoeditor/service/VideoEditorProject;)V
    .locals 0
    .param p1    # Lcom/android/videoeditor/service/VideoEditorProject;

    iput-object p1, p0, Lcom/android/videoeditor/widgets/PlayheadView;->mProject:Lcom/android/videoeditor/service/VideoEditorProject;

    return-void
.end method
