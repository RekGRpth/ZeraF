.class Lcom/android/videoeditor/widgets/AudioTrackLinearLayout$1;
.super Ljava/lang/Object;
.source "AudioTrackLinearLayout.java"

# interfaces
.implements Lcom/android/videoeditor/widgets/ItemSimpleGestureListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/android/videoeditor/widgets/AudioTrackLinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/videoeditor/widgets/AudioTrackLinearLayout;


# direct methods
.method constructor <init>(Lcom/android/videoeditor/widgets/AudioTrackLinearLayout;)V
    .locals 0

    iput-object p1, p0, Lcom/android/videoeditor/widgets/AudioTrackLinearLayout$1;->this$0:Lcom/android/videoeditor/widgets/AudioTrackLinearLayout;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onLongPress(Landroid/view/View;Landroid/view/MotionEvent;)V
    .locals 4
    .param p1    # Landroid/view/View;
    .param p2    # Landroid/view/MotionEvent;

    iget-object v0, p0, Lcom/android/videoeditor/widgets/AudioTrackLinearLayout$1;->this$0:Lcom/android/videoeditor/widgets/AudioTrackLinearLayout;

    # getter for: Lcom/android/videoeditor/widgets/AudioTrackLinearLayout;->mPlaybackInProgress:Z
    invoke-static {v0}, Lcom/android/videoeditor/widgets/AudioTrackLinearLayout;->access$400(Lcom/android/videoeditor/widgets/AudioTrackLinearLayout;)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    invoke-virtual {p1}, Landroid/view/View;->isSelected()Z

    move-result v0

    if-nez v0, :cond_2

    iget-object v0, p0, Lcom/android/videoeditor/widgets/AudioTrackLinearLayout$1;->this$0:Lcom/android/videoeditor/widgets/AudioTrackLinearLayout;

    const/4 v1, 0x1

    # invokes: Lcom/android/videoeditor/widgets/AudioTrackLinearLayout;->selectView(Landroid/view/View;Z)V
    invoke-static {v0, p1, v1}, Lcom/android/videoeditor/widgets/AudioTrackLinearLayout;->access$300(Lcom/android/videoeditor/widgets/AudioTrackLinearLayout;Landroid/view/View;Z)V

    :cond_2
    iget-object v0, p0, Lcom/android/videoeditor/widgets/AudioTrackLinearLayout$1;->this$0:Lcom/android/videoeditor/widgets/AudioTrackLinearLayout;

    # getter for: Lcom/android/videoeditor/widgets/AudioTrackLinearLayout;->mAudioTrackActionMode:Landroid/view/ActionMode;
    invoke-static {v0}, Lcom/android/videoeditor/widgets/AudioTrackLinearLayout;->access$000(Lcom/android/videoeditor/widgets/AudioTrackLinearLayout;)Landroid/view/ActionMode;

    move-result-object v0

    if-nez v0, :cond_0

    iget-object v1, p0, Lcom/android/videoeditor/widgets/AudioTrackLinearLayout$1;->this$0:Lcom/android/videoeditor/widgets/AudioTrackLinearLayout;

    new-instance v2, Lcom/android/videoeditor/widgets/AudioTrackLinearLayout$AudioTrackActionModeCallback;

    iget-object v3, p0, Lcom/android/videoeditor/widgets/AudioTrackLinearLayout$1;->this$0:Lcom/android/videoeditor/widgets/AudioTrackLinearLayout;

    invoke-virtual {p1}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/videoeditor/service/MovieAudioTrack;

    invoke-direct {v2, v3, v0}, Lcom/android/videoeditor/widgets/AudioTrackLinearLayout$AudioTrackActionModeCallback;-><init>(Lcom/android/videoeditor/widgets/AudioTrackLinearLayout;Lcom/android/videoeditor/service/MovieAudioTrack;)V

    invoke-virtual {v1, v2}, Lcom/android/videoeditor/widgets/AudioTrackLinearLayout;->startActionMode(Landroid/view/ActionMode$Callback;)Landroid/view/ActionMode;

    goto :goto_0
.end method

.method public onSingleTapConfirmed(Landroid/view/View;ILandroid/view/MotionEvent;)Z
    .locals 2
    .param p1    # Landroid/view/View;
    .param p2    # I
    .param p3    # Landroid/view/MotionEvent;

    const/4 v0, 0x1

    iget-object v1, p0, Lcom/android/videoeditor/widgets/AudioTrackLinearLayout$1;->this$0:Lcom/android/videoeditor/widgets/AudioTrackLinearLayout;

    # getter for: Lcom/android/videoeditor/widgets/AudioTrackLinearLayout;->mPlaybackInProgress:Z
    invoke-static {v1}, Lcom/android/videoeditor/widgets/AudioTrackLinearLayout;->access$400(Lcom/android/videoeditor/widgets/AudioTrackLinearLayout;)Z

    move-result v1

    if-eqz v1, :cond_1

    const/4 v0, 0x0

    :cond_0
    :goto_0
    return v0

    :cond_1
    invoke-virtual {p1}, Landroid/view/View;->isSelected()Z

    move-result v1

    if-nez v1, :cond_0

    iget-object v1, p0, Lcom/android/videoeditor/widgets/AudioTrackLinearLayout$1;->this$0:Lcom/android/videoeditor/widgets/AudioTrackLinearLayout;

    # invokes: Lcom/android/videoeditor/widgets/AudioTrackLinearLayout;->selectView(Landroid/view/View;Z)V
    invoke-static {v1, p1, v0}, Lcom/android/videoeditor/widgets/AudioTrackLinearLayout;->access$300(Lcom/android/videoeditor/widgets/AudioTrackLinearLayout;Landroid/view/View;Z)V

    goto :goto_0
.end method
