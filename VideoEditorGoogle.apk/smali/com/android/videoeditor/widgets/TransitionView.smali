.class public Lcom/android/videoeditor/widgets/TransitionView;
.super Landroid/widget/ImageView;
.source "TransitionView.java"


# static fields
.field private static final TAG:Ljava/lang/String; = "TransitionView"


# instance fields
.field private mBitmaps:[Landroid/graphics/Bitmap;

.field private mGeneratingTransitionProgress:I

.field private final mGeneratingTransitionProgressDestRect:Landroid/graphics/Rect;

.field private mGestureListener:Lcom/android/videoeditor/widgets/ItemSimpleGestureListener;

.field private mIsPlaying:Z

.field private mIsScrolling:Z

.field private mProjectPath:Ljava/lang/String;

.field private mScreenWidth:I

.field private final mScrollListener:Lcom/android/videoeditor/widgets/ScrollViewListener;

.field private mScrollView:Lcom/android/videoeditor/widgets/TimelineHorizontalScrollView;

.field private mScrollX:I

.field private final mSeparatorPaint:Landroid/graphics/Paint;

.field private final mSimpleGestureDetector:Landroid/view/GestureDetector;

.field private mTimeline:Lcom/android/videoeditor/widgets/MediaLinearLayout;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 2
    .param p1    # Landroid/content/Context;

    const/4 v0, 0x0

    const/4 v1, 0x0

    invoke-direct {p0, p1, v0, v1}, Lcom/android/videoeditor/widgets/TransitionView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/util/AttributeSet;

    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/android/videoeditor/widgets/TransitionView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 11
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/util/AttributeSet;
    .param p3    # I

    const/high16 v10, 0x40000000

    invoke-direct {p0, p1, p2, p3}, Landroid/widget/ImageView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    new-instance v5, Landroid/view/GestureDetector;

    new-instance v6, Lcom/android/videoeditor/widgets/TransitionView$1;

    invoke-direct {v6, p0}, Lcom/android/videoeditor/widgets/TransitionView$1;-><init>(Lcom/android/videoeditor/widgets/TransitionView;)V

    invoke-direct {v5, p1, v6}, Landroid/view/GestureDetector;-><init>(Landroid/content/Context;Landroid/view/GestureDetector$OnGestureListener;)V

    iput-object v5, p0, Lcom/android/videoeditor/widgets/TransitionView;->mSimpleGestureDetector:Landroid/view/GestureDetector;

    new-instance v5, Lcom/android/videoeditor/widgets/TransitionView$2;

    invoke-direct {v5, p0}, Lcom/android/videoeditor/widgets/TransitionView$2;-><init>(Lcom/android/videoeditor/widgets/TransitionView;)V

    iput-object v5, p0, Lcom/android/videoeditor/widgets/TransitionView;->mScrollListener:Lcom/android/videoeditor/widgets/ScrollViewListener;

    invoke-virtual {p0}, Lcom/android/videoeditor/widgets/TransitionView;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    invoke-static {p1}, Lcom/android/videoeditor/widgets/ProgressBar;->getProgressBar(Landroid/content/Context;)Lcom/android/videoeditor/widgets/ProgressBar;

    move-result-object v3

    const v5, 0x7f070008

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v5

    const v6, 0x7f07000f

    invoke-virtual {v4, v6}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v6

    sub-float/2addr v5, v6

    const v6, 0x7f07001a

    invoke-virtual {v4, v6}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v6

    mul-float/2addr v6, v10

    sub-float/2addr v5, v6

    float-to-int v1, v5

    new-instance v5, Landroid/graphics/Rect;

    invoke-virtual {p0}, Lcom/android/videoeditor/widgets/TransitionView;->getPaddingLeft()I

    move-result v6

    invoke-virtual {v3}, Lcom/android/videoeditor/widgets/ProgressBar;->getHeight()I

    move-result v7

    sub-int v7, v1, v7

    invoke-virtual {p0}, Lcom/android/videoeditor/widgets/TransitionView;->getPaddingBottom()I

    move-result v8

    sub-int/2addr v7, v8

    const/4 v8, 0x0

    invoke-virtual {p0}, Lcom/android/videoeditor/widgets/TransitionView;->getPaddingBottom()I

    move-result v9

    sub-int v9, v1, v9

    invoke-direct {v5, v6, v7, v8, v9}, Landroid/graphics/Rect;-><init>(IIII)V

    iput-object v5, p0, Lcom/android/videoeditor/widgets/TransitionView;->mGeneratingTransitionProgressDestRect:Landroid/graphics/Rect;

    const/4 v5, -0x1

    iput v5, p0, Lcom/android/videoeditor/widgets/TransitionView;->mGeneratingTransitionProgress:I

    invoke-virtual {p0}, Lcom/android/videoeditor/widgets/TransitionView;->getContext()Landroid/content/Context;

    move-result-object v5

    const-string v6, "window"

    invoke-virtual {v5, v6}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Landroid/view/WindowManager;

    invoke-interface {v5}, Landroid/view/WindowManager;->getDefaultDisplay()Landroid/view/Display;

    move-result-object v0

    new-instance v2, Landroid/util/DisplayMetrics;

    invoke-direct {v2}, Landroid/util/DisplayMetrics;-><init>()V

    invoke-virtual {v0, v2}, Landroid/view/Display;->getMetrics(Landroid/util/DisplayMetrics;)V

    iget v5, v2, Landroid/util/DisplayMetrics;->widthPixels:I

    iput v5, p0, Lcom/android/videoeditor/widgets/TransitionView;->mScreenWidth:I

    new-instance v5, Landroid/graphics/Paint;

    invoke-direct {v5}, Landroid/graphics/Paint;-><init>()V

    iput-object v5, p0, Lcom/android/videoeditor/widgets/TransitionView;->mSeparatorPaint:Landroid/graphics/Paint;

    iget-object v5, p0, Lcom/android/videoeditor/widgets/TransitionView;->mSeparatorPaint:Landroid/graphics/Paint;

    const/high16 v6, -0x1000000

    invoke-virtual {v5, v6}, Landroid/graphics/Paint;->setColor(I)V

    iget-object v5, p0, Lcom/android/videoeditor/widgets/TransitionView;->mSeparatorPaint:Landroid/graphics/Paint;

    invoke-virtual {v5, v10}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    return-void
.end method

.method static synthetic access$000(Lcom/android/videoeditor/widgets/TransitionView;)Lcom/android/videoeditor/widgets/ItemSimpleGestureListener;
    .locals 1
    .param p0    # Lcom/android/videoeditor/widgets/TransitionView;

    iget-object v0, p0, Lcom/android/videoeditor/widgets/TransitionView;->mGestureListener:Lcom/android/videoeditor/widgets/ItemSimpleGestureListener;

    return-object v0
.end method

.method static synthetic access$102(Lcom/android/videoeditor/widgets/TransitionView;Z)Z
    .locals 0
    .param p0    # Lcom/android/videoeditor/widgets/TransitionView;
    .param p1    # Z

    iput-boolean p1, p0, Lcom/android/videoeditor/widgets/TransitionView;->mIsScrolling:Z

    return p1
.end method

.method static synthetic access$202(Lcom/android/videoeditor/widgets/TransitionView;I)I
    .locals 0
    .param p0    # Lcom/android/videoeditor/widgets/TransitionView;
    .param p1    # I

    iput p1, p0, Lcom/android/videoeditor/widgets/TransitionView;->mScrollX:I

    return p1
.end method

.method static synthetic access$300(Lcom/android/videoeditor/widgets/TransitionView;)Z
    .locals 1
    .param p0    # Lcom/android/videoeditor/widgets/TransitionView;

    invoke-direct {p0}, Lcom/android/videoeditor/widgets/TransitionView;->requestThumbnails()Z

    move-result v0

    return v0
.end method

.method private requestThumbnails()Z
    .locals 9

    const/4 v5, 0x0

    iget-object v6, p0, Lcom/android/videoeditor/widgets/TransitionView;->mBitmaps:[Landroid/graphics/Bitmap;

    if-eqz v6, :cond_1

    const/4 v5, 0x1

    :cond_0
    :goto_0
    return v5

    :cond_1
    iget-boolean v6, p0, Lcom/android/videoeditor/widgets/TransitionView;->mIsScrolling:Z

    if-nez v6, :cond_0

    invoke-virtual {p0}, Lcom/android/videoeditor/widgets/TransitionView;->getTag()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/android/videoeditor/service/MovieTransition;

    iget-object v6, p0, Lcom/android/videoeditor/widgets/TransitionView;->mProjectPath:Ljava/lang/String;

    invoke-virtual {v4}, Lcom/android/videoeditor/service/MovieTransition;->getId()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Lcom/android/videoeditor/service/ApiService;->isTransitionThumbnailsPending(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v6

    if-nez v6, :cond_0

    invoke-virtual {p0}, Lcom/android/videoeditor/widgets/TransitionView;->getLeft()I

    move-result v6

    invoke-virtual {p0}, Lcom/android/videoeditor/widgets/TransitionView;->getPaddingLeft()I

    move-result v7

    add-int/2addr v6, v7

    iget v7, p0, Lcom/android/videoeditor/widgets/TransitionView;->mScrollX:I

    sub-int v2, v6, v7

    invoke-virtual {p0}, Lcom/android/videoeditor/widgets/TransitionView;->getRight()I

    move-result v6

    invoke-virtual {p0}, Lcom/android/videoeditor/widgets/TransitionView;->getPaddingRight()I

    move-result v7

    sub-int/2addr v6, v7

    iget v7, p0, Lcom/android/videoeditor/widgets/TransitionView;->mScrollX:I

    sub-int v0, v6, v7

    iget v6, p0, Lcom/android/videoeditor/widgets/TransitionView;->mScreenWidth:I

    if-ge v2, v6, :cond_2

    if-ltz v0, :cond_2

    if-ne v2, v0, :cond_6

    :cond_2
    const-string v6, "TransitionView"

    const/4 v7, 0x2

    invoke-static {v6, v7}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v6

    if-eqz v6, :cond_3

    const-string v6, "TransitionView"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "Transition view is off screen: "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v4}, Lcom/android/videoeditor/service/MovieTransition;->getId()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, ", from: "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, " to "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    :cond_3
    iget-object v6, p0, Lcom/android/videoeditor/widgets/TransitionView;->mBitmaps:[Landroid/graphics/Bitmap;

    if-eqz v6, :cond_0

    const/4 v1, 0x0

    :goto_1
    iget-object v6, p0, Lcom/android/videoeditor/widgets/TransitionView;->mBitmaps:[Landroid/graphics/Bitmap;

    array-length v6, v6

    if-ge v1, v6, :cond_5

    iget-object v6, p0, Lcom/android/videoeditor/widgets/TransitionView;->mBitmaps:[Landroid/graphics/Bitmap;

    aget-object v6, v6, v1

    if-eqz v6, :cond_4

    iget-object v6, p0, Lcom/android/videoeditor/widgets/TransitionView;->mBitmaps:[Landroid/graphics/Bitmap;

    aget-object v6, v6, v1

    invoke-virtual {v6}, Landroid/graphics/Bitmap;->recycle()V

    :cond_4
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    :cond_5
    const/4 v6, 0x0

    iput-object v6, p0, Lcom/android/videoeditor/widgets/TransitionView;->mBitmaps:[Landroid/graphics/Bitmap;

    goto/16 :goto_0

    :cond_6
    invoke-virtual {p0}, Lcom/android/videoeditor/widgets/TransitionView;->getHeight()I

    move-result v6

    invoke-virtual {p0}, Lcom/android/videoeditor/widgets/TransitionView;->getPaddingTop()I

    move-result v7

    sub-int/2addr v6, v7

    invoke-virtual {p0}, Lcom/android/videoeditor/widgets/TransitionView;->getPaddingBottom()I

    move-result v7

    sub-int v3, v6, v7

    invoke-virtual {p0}, Lcom/android/videoeditor/widgets/TransitionView;->getContext()Landroid/content/Context;

    move-result-object v6

    iget-object v7, p0, Lcom/android/videoeditor/widgets/TransitionView;->mProjectPath:Ljava/lang/String;

    invoke-virtual {v4}, Lcom/android/videoeditor/service/MovieTransition;->getId()Ljava/lang/String;

    move-result-object v8

    invoke-static {v6, v7, v8, v3}, Lcom/android/videoeditor/service/ApiService;->getTransitionThumbnails(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;I)V

    goto/16 :goto_0
.end method


# virtual methods
.method public isGeneratingTransition()Z
    .locals 1

    iget v0, p0, Lcom/android/videoeditor/widgets/TransitionView;->mGeneratingTransitionProgress:I

    if-ltz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected onAttachedToWindow()V
    .locals 2

    invoke-virtual {p0}, Lcom/android/videoeditor/widgets/TransitionView;->getRootView()Landroid/view/View;

    move-result-object v0

    const v1, 0x7f080031

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/android/videoeditor/widgets/TimelineHorizontalScrollView;

    iput-object v0, p0, Lcom/android/videoeditor/widgets/TransitionView;->mScrollView:Lcom/android/videoeditor/widgets/TimelineHorizontalScrollView;

    iget-object v0, p0, Lcom/android/videoeditor/widgets/TransitionView;->mScrollView:Lcom/android/videoeditor/widgets/TimelineHorizontalScrollView;

    iget-object v1, p0, Lcom/android/videoeditor/widgets/TransitionView;->mScrollListener:Lcom/android/videoeditor/widgets/ScrollViewListener;

    invoke-virtual {v0, v1}, Lcom/android/videoeditor/widgets/TimelineHorizontalScrollView;->addScrollListener(Lcom/android/videoeditor/widgets/ScrollViewListener;)V

    iget-object v0, p0, Lcom/android/videoeditor/widgets/TransitionView;->mScrollView:Lcom/android/videoeditor/widgets/TimelineHorizontalScrollView;

    invoke-virtual {v0}, Lcom/android/videoeditor/widgets/TimelineHorizontalScrollView;->getScrollX()I

    move-result v0

    iput v0, p0, Lcom/android/videoeditor/widgets/TransitionView;->mScrollX:I

    invoke-virtual {p0}, Lcom/android/videoeditor/widgets/TransitionView;->getRootView()Landroid/view/View;

    move-result-object v0

    const v1, 0x7f080033

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/android/videoeditor/widgets/MediaLinearLayout;

    iput-object v0, p0, Lcom/android/videoeditor/widgets/TransitionView;->mTimeline:Lcom/android/videoeditor/widgets/MediaLinearLayout;

    return-void
.end method

.method protected onDetachedFromWindow()V
    .locals 3

    iget-object v1, p0, Lcom/android/videoeditor/widgets/TransitionView;->mScrollView:Lcom/android/videoeditor/widgets/TimelineHorizontalScrollView;

    iget-object v2, p0, Lcom/android/videoeditor/widgets/TransitionView;->mScrollListener:Lcom/android/videoeditor/widgets/ScrollViewListener;

    invoke-virtual {v1, v2}, Lcom/android/videoeditor/widgets/TimelineHorizontalScrollView;->removeScrollListener(Lcom/android/videoeditor/widgets/ScrollViewListener;)V

    iget-object v1, p0, Lcom/android/videoeditor/widgets/TransitionView;->mBitmaps:[Landroid/graphics/Bitmap;

    if-eqz v1, :cond_2

    const/4 v0, 0x0

    :goto_0
    iget-object v1, p0, Lcom/android/videoeditor/widgets/TransitionView;->mBitmaps:[Landroid/graphics/Bitmap;

    array-length v1, v1

    if-ge v0, v1, :cond_1

    iget-object v1, p0, Lcom/android/videoeditor/widgets/TransitionView;->mBitmaps:[Landroid/graphics/Bitmap;

    aget-object v1, v1, v0

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/android/videoeditor/widgets/TransitionView;->mBitmaps:[Landroid/graphics/Bitmap;

    aget-object v1, v1, v0

    invoke-virtual {v1}, Landroid/graphics/Bitmap;->recycle()V

    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_1
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/android/videoeditor/widgets/TransitionView;->mBitmaps:[Landroid/graphics/Bitmap;

    :cond_2
    return-void
.end method

.method protected onDraw(Landroid/graphics/Canvas;)V
    .locals 10
    .param p1    # Landroid/graphics/Canvas;

    const/4 v9, 0x0

    const/4 v5, 0x0

    const/4 v4, 0x1

    const/high16 v8, -0x1000000

    invoke-super {p0, p1}, Landroid/widget/ImageView;->onDraw(Landroid/graphics/Canvas;)V

    invoke-virtual {p0}, Lcom/android/videoeditor/widgets/TransitionView;->getWidth()I

    move-result v0

    invoke-virtual {p0}, Lcom/android/videoeditor/widgets/TransitionView;->getPaddingLeft()I

    move-result v1

    invoke-virtual {p0}, Lcom/android/videoeditor/widgets/TransitionView;->getPaddingRight()I

    move-result v2

    add-int/2addr v1, v2

    if-gt v0, v1, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget v0, p0, Lcom/android/videoeditor/widgets/TransitionView;->mGeneratingTransitionProgress:I

    if-ltz v0, :cond_2

    invoke-virtual {p0}, Lcom/android/videoeditor/widgets/TransitionView;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/android/videoeditor/widgets/ProgressBar;->getProgressBar(Landroid/content/Context;)Lcom/android/videoeditor/widgets/ProgressBar;

    move-result-object v0

    iget v2, p0, Lcom/android/videoeditor/widgets/TransitionView;->mGeneratingTransitionProgress:I

    iget-object v3, p0, Lcom/android/videoeditor/widgets/TransitionView;->mGeneratingTransitionProgressDestRect:Landroid/graphics/Rect;

    invoke-virtual {p0}, Lcom/android/videoeditor/widgets/TransitionView;->getPaddingLeft()I

    move-result v4

    invoke-virtual {p0}, Lcom/android/videoeditor/widgets/TransitionView;->getWidth()I

    move-result v1

    invoke-virtual {p0}, Lcom/android/videoeditor/widgets/TransitionView;->getPaddingRight()I

    move-result v5

    sub-int v5, v1, v5

    move-object v1, p1

    invoke-virtual/range {v0 .. v5}, Lcom/android/videoeditor/widgets/ProgressBar;->draw(Landroid/graphics/Canvas;ILandroid/graphics/Rect;II)V

    goto :goto_0

    :cond_2
    iget-object v0, p0, Lcom/android/videoeditor/widgets/TransitionView;->mBitmaps:[Landroid/graphics/Bitmap;

    if-eqz v0, :cond_5

    invoke-virtual {p0}, Lcom/android/videoeditor/widgets/TransitionView;->getWidth()I

    move-result v0

    div-int/lit8 v6, v0, 0x2

    invoke-virtual {p1}, Landroid/graphics/Canvas;->save()I

    invoke-virtual {p0}, Lcom/android/videoeditor/widgets/TransitionView;->getPaddingLeft()I

    move-result v0

    invoke-virtual {p0}, Lcom/android/videoeditor/widgets/TransitionView;->getPaddingTop()I

    move-result v1

    invoke-virtual {p0}, Lcom/android/videoeditor/widgets/TransitionView;->getHeight()I

    move-result v2

    invoke-virtual {p0}, Lcom/android/videoeditor/widgets/TransitionView;->getPaddingBottom()I

    move-result v3

    sub-int/2addr v2, v3

    invoke-virtual {p1, v0, v1, v6, v2}, Landroid/graphics/Canvas;->clipRect(IIII)Z

    iget-object v0, p0, Lcom/android/videoeditor/widgets/TransitionView;->mBitmaps:[Landroid/graphics/Bitmap;

    aget-object v0, v0, v5

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/android/videoeditor/widgets/TransitionView;->mBitmaps:[Landroid/graphics/Bitmap;

    aget-object v0, v0, v5

    invoke-virtual {p0}, Lcom/android/videoeditor/widgets/TransitionView;->getPaddingLeft()I

    move-result v1

    int-to-float v1, v1

    invoke-virtual {p0}, Lcom/android/videoeditor/widgets/TransitionView;->getPaddingTop()I

    move-result v2

    int-to-float v2, v2

    invoke-virtual {p1, v0, v1, v2, v9}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    :goto_1
    invoke-virtual {p1}, Landroid/graphics/Canvas;->restore()V

    invoke-virtual {p1}, Landroid/graphics/Canvas;->save()I

    invoke-virtual {p0}, Lcom/android/videoeditor/widgets/TransitionView;->getPaddingTop()I

    move-result v0

    invoke-virtual {p0}, Lcom/android/videoeditor/widgets/TransitionView;->getWidth()I

    move-result v1

    invoke-virtual {p0}, Lcom/android/videoeditor/widgets/TransitionView;->getPaddingRight()I

    move-result v2

    sub-int/2addr v1, v2

    invoke-virtual {p0}, Lcom/android/videoeditor/widgets/TransitionView;->getHeight()I

    move-result v2

    invoke-virtual {p0}, Lcom/android/videoeditor/widgets/TransitionView;->getPaddingBottom()I

    move-result v3

    sub-int/2addr v2, v3

    invoke-virtual {p1, v6, v0, v1, v2}, Landroid/graphics/Canvas;->clipRect(IIII)Z

    iget-object v0, p0, Lcom/android/videoeditor/widgets/TransitionView;->mBitmaps:[Landroid/graphics/Bitmap;

    aget-object v0, v0, v4

    if-eqz v0, :cond_4

    iget-object v0, p0, Lcom/android/videoeditor/widgets/TransitionView;->mBitmaps:[Landroid/graphics/Bitmap;

    aget-object v0, v0, v4

    invoke-virtual {p0}, Lcom/android/videoeditor/widgets/TransitionView;->getWidth()I

    move-result v1

    invoke-virtual {p0}, Lcom/android/videoeditor/widgets/TransitionView;->getPaddingRight()I

    move-result v2

    sub-int/2addr v1, v2

    iget-object v2, p0, Lcom/android/videoeditor/widgets/TransitionView;->mBitmaps:[Landroid/graphics/Bitmap;

    aget-object v2, v2, v4

    invoke-virtual {v2}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v2

    sub-int/2addr v1, v2

    int-to-float v1, v1

    invoke-virtual {p0}, Lcom/android/videoeditor/widgets/TransitionView;->getPaddingTop()I

    move-result v2

    int-to-float v2, v2

    invoke-virtual {p1, v0, v1, v2, v9}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    :goto_2
    invoke-virtual {p1}, Landroid/graphics/Canvas;->restore()V

    int-to-float v1, v6

    invoke-virtual {p0}, Lcom/android/videoeditor/widgets/TransitionView;->getPaddingTop()I

    move-result v0

    int-to-float v2, v0

    int-to-float v3, v6

    invoke-virtual {p0}, Lcom/android/videoeditor/widgets/TransitionView;->getHeight()I

    move-result v0

    invoke-virtual {p0}, Lcom/android/videoeditor/widgets/TransitionView;->getPaddingBottom()I

    move-result v4

    sub-int/2addr v0, v4

    int-to-float v4, v0

    iget-object v5, p0, Lcom/android/videoeditor/widgets/TransitionView;->mSeparatorPaint:Landroid/graphics/Paint;

    move-object v0, p1

    invoke-virtual/range {v0 .. v5}, Landroid/graphics/Canvas;->drawLine(FFFFLandroid/graphics/Paint;)V

    invoke-virtual {p0}, Lcom/android/videoeditor/widgets/TransitionView;->isSelected()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/android/videoeditor/widgets/TransitionView;->mTimeline:Lcom/android/videoeditor/widgets/MediaLinearLayout;

    invoke-virtual {v0}, Lcom/android/videoeditor/widgets/MediaLinearLayout;->hasItemSelected()Z

    move-result v0

    if-eqz v0, :cond_0

    new-instance v7, Landroid/graphics/Paint;

    invoke-direct {v7}, Landroid/graphics/Paint;-><init>()V

    invoke-virtual {v7, v8}, Landroid/graphics/Paint;->setColor(I)V

    const/16 v0, 0xc0

    invoke-virtual {v7, v0}, Landroid/graphics/Paint;->setAlpha(I)V

    invoke-virtual {p1, v7}, Landroid/graphics/Canvas;->drawPaint(Landroid/graphics/Paint;)V

    goto/16 :goto_0

    :cond_3
    invoke-virtual {p1, v8}, Landroid/graphics/Canvas;->drawColor(I)V

    goto/16 :goto_1

    :cond_4
    invoke-virtual {p1, v8}, Landroid/graphics/Canvas;->drawColor(I)V

    goto :goto_2

    :cond_5
    iget-boolean v0, p0, Lcom/android/videoeditor/widgets/TransitionView;->mIsPlaying:Z

    if-nez v0, :cond_0

    iget-boolean v0, p0, Lcom/android/videoeditor/widgets/TransitionView;->mIsScrolling:Z

    if-nez v0, :cond_0

    invoke-direct {p0}, Lcom/android/videoeditor/widgets/TransitionView;->requestThumbnails()Z

    goto/16 :goto_0
.end method

.method public onTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 1
    .param p1    # Landroid/view/MotionEvent;

    iget-object v0, p0, Lcom/android/videoeditor/widgets/TransitionView;->mSimpleGestureDetector:Landroid/view/GestureDetector;

    invoke-virtual {v0, p1}, Landroid/view/GestureDetector;->onTouchEvent(Landroid/view/MotionEvent;)Z

    invoke-super {p0, p1}, Landroid/widget/ImageView;->onTouchEvent(Landroid/view/MotionEvent;)Z

    move-result v0

    return v0
.end method

.method public resetGeneratingTransitionProgress()V
    .locals 1

    const/4 v0, -0x1

    invoke-virtual {p0, v0}, Lcom/android/videoeditor/widgets/TransitionView;->setGeneratingTransitionProgress(I)V

    return-void
.end method

.method public setBitmaps([Landroid/graphics/Bitmap;)Z
    .locals 2
    .param p1    # [Landroid/graphics/Bitmap;

    iget v1, p0, Lcom/android/videoeditor/widgets/TransitionView;->mGeneratingTransitionProgress:I

    if-ltz v1, :cond_0

    const/4 v1, 0x0

    :goto_0
    return v1

    :cond_0
    iget-object v1, p0, Lcom/android/videoeditor/widgets/TransitionView;->mBitmaps:[Landroid/graphics/Bitmap;

    if-eqz v1, :cond_2

    const/4 v0, 0x0

    :goto_1
    iget-object v1, p0, Lcom/android/videoeditor/widgets/TransitionView;->mBitmaps:[Landroid/graphics/Bitmap;

    array-length v1, v1

    if-ge v0, v1, :cond_2

    iget-object v1, p0, Lcom/android/videoeditor/widgets/TransitionView;->mBitmaps:[Landroid/graphics/Bitmap;

    aget-object v1, v1, v0

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/android/videoeditor/widgets/TransitionView;->mBitmaps:[Landroid/graphics/Bitmap;

    aget-object v1, v1, v0

    invoke-virtual {v1}, Landroid/graphics/Bitmap;->recycle()V

    :cond_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    :cond_2
    iput-object p1, p0, Lcom/android/videoeditor/widgets/TransitionView;->mBitmaps:[Landroid/graphics/Bitmap;

    invoke-virtual {p0}, Lcom/android/videoeditor/widgets/TransitionView;->invalidate()V

    const/4 v1, 0x1

    goto :goto_0
.end method

.method public setGeneratingTransitionProgress(I)V
    .locals 1
    .param p1    # I

    const/16 v0, 0x64

    if-ne p1, v0, :cond_0

    invoke-direct {p0}, Lcom/android/videoeditor/widgets/TransitionView;->requestThumbnails()Z

    const/4 v0, -0x1

    iput v0, p0, Lcom/android/videoeditor/widgets/TransitionView;->mGeneratingTransitionProgress:I

    :goto_0
    invoke-virtual {p0}, Lcom/android/videoeditor/widgets/TransitionView;->invalidate()V

    return-void

    :cond_0
    iput p1, p0, Lcom/android/videoeditor/widgets/TransitionView;->mGeneratingTransitionProgress:I

    goto :goto_0
.end method

.method public setGestureListener(Lcom/android/videoeditor/widgets/ItemSimpleGestureListener;)V
    .locals 0
    .param p1    # Lcom/android/videoeditor/widgets/ItemSimpleGestureListener;

    iput-object p1, p0, Lcom/android/videoeditor/widgets/TransitionView;->mGestureListener:Lcom/android/videoeditor/widgets/ItemSimpleGestureListener;

    return-void
.end method

.method public setPlaybackMode(Z)V
    .locals 0
    .param p1    # Z

    iput-boolean p1, p0, Lcom/android/videoeditor/widgets/TransitionView;->mIsPlaying:Z

    return-void
.end method

.method public setProjectPath(Ljava/lang/String;)V
    .locals 0
    .param p1    # Ljava/lang/String;

    iput-object p1, p0, Lcom/android/videoeditor/widgets/TransitionView;->mProjectPath:Ljava/lang/String;

    return-void
.end method
