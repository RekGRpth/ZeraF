.class public Lcom/android/videoeditor/widgets/MediaLinearLayout;
.super Landroid/widget/LinearLayout;
.source "MediaLinearLayout.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/videoeditor/widgets/MediaLinearLayout$TransitionActionModeCallback;,
        Lcom/android/videoeditor/widgets/MediaLinearLayout$MediaItemActionModeCallback;
    }
.end annotation


# static fields
.field private static final MAXIMUM_IMAGE_DURATION:J = 0x1770L

.field private static final MAXIMUM_TRANSITION_DURATION:J = 0xbb8L

.field private static final MINIMUM_TRANSITION_DURATION:J = 0xfaL

.field private static final PARAM_DIALOG_CURRENT_RENDERING_MODE:Ljava/lang/String; = "rendering_mode"

.field private static final PARAM_DIALOG_MEDIA_ITEM_ID:Ljava/lang/String; = "media_item_id"

.field private static final PARAM_DIALOG_TRANSITION_ID:Ljava/lang/String; = "transition_id"

.field private static final TAG:Ljava/lang/String; = "MediaLinearLayout"

.field private static final TIME_TOLERANCE:J = 0x1eL


# instance fields
.field private mDragMediaItemId:Ljava/lang/String;

.field private mDropAfterMediaItem:Lcom/android/videoeditor/service/MovieMediaItem;

.field private mDropIndex:I

.field private mFirstEntered:Z

.field private final mHalfParentWidth:I

.field private final mHandleWidth:I

.field private final mHandler:Landroid/os/Handler;

.field private mIsTrimming:Z

.field private final mLeftAddClipButton:Landroid/widget/ImageButton;

.field private mLeftHandle:Lcom/android/videoeditor/widgets/HandleView;

.field private mListener:Lcom/android/videoeditor/widgets/MediaLinearLayoutListener;

.field private mMediaItemActionMode:Landroid/view/ActionMode;

.field private final mMediaItemGestureListener:Lcom/android/videoeditor/widgets/ItemSimpleGestureListener;

.field private mMoveLayoutPending:Z

.field private mPlaybackInProgress:Z

.field private mPrevDragPosition:F

.field private mPrevDragScrollTime:J

.field private mProject:Lcom/android/videoeditor/service/VideoEditorProject;

.field private final mRightAddClipButton:Landroid/widget/ImageButton;

.field private mRightHandle:Lcom/android/videoeditor/widgets/HandleView;

.field private mScrollView:Landroid/view/View;

.field private mSelectedView:Landroid/view/View;

.field private mTransitionActionMode:Landroid/view/ActionMode;

.field private final mTransitionGestureListener:Lcom/android/videoeditor/widgets/ItemSimpleGestureListener;

.field private final mTransitionVerticalInset:I


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 2
    .param p1    # Landroid/content/Context;

    const/4 v0, 0x0

    const/4 v1, 0x0

    invoke-direct {p0, p1, v0, v1}, Lcom/android/videoeditor/widgets/MediaLinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/util/AttributeSet;

    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/android/videoeditor/widgets/MediaLinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 6
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/util/AttributeSet;
    .param p3    # I

    const/4 v5, 0x0

    invoke-direct {p0, p1, p2, p3}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    new-instance v3, Lcom/android/videoeditor/widgets/MediaLinearLayout$1;

    invoke-direct {v3, p0}, Lcom/android/videoeditor/widgets/MediaLinearLayout$1;-><init>(Lcom/android/videoeditor/widgets/MediaLinearLayout;)V

    iput-object v3, p0, Lcom/android/videoeditor/widgets/MediaLinearLayout;->mMediaItemGestureListener:Lcom/android/videoeditor/widgets/ItemSimpleGestureListener;

    new-instance v3, Lcom/android/videoeditor/widgets/MediaLinearLayout$2;

    invoke-direct {v3, p0}, Lcom/android/videoeditor/widgets/MediaLinearLayout$2;-><init>(Lcom/android/videoeditor/widgets/MediaLinearLayout;)V

    iput-object v3, p0, Lcom/android/videoeditor/widgets/MediaLinearLayout;->mTransitionGestureListener:Lcom/android/videoeditor/widgets/ItemSimpleGestureListener;

    invoke-virtual {p0}, Lcom/android/videoeditor/widgets/MediaLinearLayout;->getContext()Landroid/content/Context;

    move-result-object v3

    const v4, 0x7f040004

    invoke-static {v3, v4, v5}, Lcom/android/videoeditor/widgets/MediaLinearLayout;->inflate(Landroid/content/Context;ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    new-instance v3, Lcom/android/videoeditor/widgets/MediaLinearLayout$3;

    invoke-direct {v3, p0}, Lcom/android/videoeditor/widgets/MediaLinearLayout$3;-><init>(Lcom/android/videoeditor/widgets/MediaLinearLayout;)V

    invoke-virtual {v0, v3}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    const v3, 0x7f080008

    invoke-virtual {v0, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/ImageButton;

    iput-object v3, p0, Lcom/android/videoeditor/widgets/MediaLinearLayout;->mLeftAddClipButton:Landroid/widget/ImageButton;

    iget-object v3, p0, Lcom/android/videoeditor/widgets/MediaLinearLayout;->mLeftAddClipButton:Landroid/widget/ImageButton;

    const/16 v4, 0x8

    invoke-virtual {v3, v4}, Landroid/widget/ImageButton;->setVisibility(I)V

    iget-object v3, p0, Lcom/android/videoeditor/widgets/MediaLinearLayout;->mLeftAddClipButton:Landroid/widget/ImageButton;

    new-instance v4, Lcom/android/videoeditor/widgets/MediaLinearLayout$4;

    invoke-direct {v4, p0}, Lcom/android/videoeditor/widgets/MediaLinearLayout$4;-><init>(Lcom/android/videoeditor/widgets/MediaLinearLayout;)V

    invoke-virtual {v3, v4}, Landroid/widget/ImageButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    invoke-virtual {p0, v0}, Lcom/android/videoeditor/widgets/MediaLinearLayout;->addView(Landroid/view/View;)V

    invoke-virtual {p0}, Lcom/android/videoeditor/widgets/MediaLinearLayout;->getContext()Landroid/content/Context;

    move-result-object v3

    const v4, 0x7f040006

    invoke-static {v3, v4, v5}, Lcom/android/videoeditor/widgets/MediaLinearLayout;->inflate(Landroid/content/Context;ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v2

    new-instance v3, Lcom/android/videoeditor/widgets/MediaLinearLayout$5;

    invoke-direct {v3, p0}, Lcom/android/videoeditor/widgets/MediaLinearLayout$5;-><init>(Lcom/android/videoeditor/widgets/MediaLinearLayout;)V

    invoke-virtual {v2, v3}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    const v3, 0x7f08000c

    invoke-virtual {v2, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/ImageButton;

    iput-object v3, p0, Lcom/android/videoeditor/widgets/MediaLinearLayout;->mRightAddClipButton:Landroid/widget/ImageButton;

    iget-object v3, p0, Lcom/android/videoeditor/widgets/MediaLinearLayout;->mRightAddClipButton:Landroid/widget/ImageButton;

    new-instance v4, Lcom/android/videoeditor/widgets/MediaLinearLayout$6;

    invoke-direct {v4, p0}, Lcom/android/videoeditor/widgets/MediaLinearLayout$6;-><init>(Lcom/android/videoeditor/widgets/MediaLinearLayout;)V

    invoke-virtual {v3, v4}, Landroid/widget/ImageButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    invoke-virtual {p0, v2}, Lcom/android/videoeditor/widgets/MediaLinearLayout;->addView(Landroid/view/View;)V

    invoke-virtual {p0}, Lcom/android/videoeditor/widgets/MediaLinearLayout;->getContext()Landroid/content/Context;

    move-result-object v3

    const v4, 0x7f04000b

    invoke-static {v3, v4, v5}, Lcom/android/videoeditor/widgets/MediaLinearLayout;->inflate(Landroid/content/Context;ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v3

    check-cast v3, Lcom/android/videoeditor/widgets/HandleView;

    iput-object v3, p0, Lcom/android/videoeditor/widgets/MediaLinearLayout;->mLeftHandle:Lcom/android/videoeditor/widgets/HandleView;

    iget-object v3, p0, Lcom/android/videoeditor/widgets/MediaLinearLayout;->mLeftHandle:Lcom/android/videoeditor/widgets/HandleView;

    invoke-virtual {p0, v3}, Lcom/android/videoeditor/widgets/MediaLinearLayout;->addView(Landroid/view/View;)V

    invoke-virtual {p0}, Lcom/android/videoeditor/widgets/MediaLinearLayout;->getContext()Landroid/content/Context;

    move-result-object v3

    const v4, 0x7f040014

    invoke-static {v3, v4, v5}, Lcom/android/videoeditor/widgets/MediaLinearLayout;->inflate(Landroid/content/Context;ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v3

    check-cast v3, Lcom/android/videoeditor/widgets/HandleView;

    iput-object v3, p0, Lcom/android/videoeditor/widgets/MediaLinearLayout;->mRightHandle:Lcom/android/videoeditor/widgets/HandleView;

    iget-object v3, p0, Lcom/android/videoeditor/widgets/MediaLinearLayout;->mRightHandle:Lcom/android/videoeditor/widgets/HandleView;

    invoke-virtual {p0, v3}, Lcom/android/videoeditor/widgets/MediaLinearLayout;->addView(Landroid/view/View;)V

    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f070011

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v3

    float-to-int v3, v3

    iput v3, p0, Lcom/android/videoeditor/widgets/MediaLinearLayout;->mHandleWidth:I

    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f07001a

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v3

    float-to-int v3, v3

    iput v3, p0, Lcom/android/videoeditor/widgets/MediaLinearLayout;->mTransitionVerticalInset:I

    check-cast p1, Landroid/app/Activity;

    invoke-virtual {p1}, Landroid/app/Activity;->getWindowManager()Landroid/view/WindowManager;

    move-result-object v3

    invoke-interface {v3}, Landroid/view/WindowManager;->getDefaultDisplay()Landroid/view/Display;

    move-result-object v1

    invoke-virtual {v1}, Landroid/view/Display;->getWidth()I

    move-result v3

    div-int/lit8 v3, v3, 0x2

    iput v3, p0, Lcom/android/videoeditor/widgets/MediaLinearLayout;->mHalfParentWidth:I

    new-instance v3, Landroid/os/Handler;

    invoke-direct {v3}, Landroid/os/Handler;-><init>()V

    iput-object v3, p0, Lcom/android/videoeditor/widgets/MediaLinearLayout;->mHandler:Landroid/os/Handler;

    const/4 v3, 0x0

    invoke-virtual {p0, v3}, Lcom/android/videoeditor/widgets/MediaLinearLayout;->setMotionEventSplittingEnabled(Z)V

    return-void
.end method

.method static synthetic access$000(Lcom/android/videoeditor/widgets/MediaLinearLayout;)Landroid/view/ActionMode;
    .locals 1
    .param p0    # Lcom/android/videoeditor/widgets/MediaLinearLayout;

    iget-object v0, p0, Lcom/android/videoeditor/widgets/MediaLinearLayout;->mMediaItemActionMode:Landroid/view/ActionMode;

    return-object v0
.end method

.method static synthetic access$002(Lcom/android/videoeditor/widgets/MediaLinearLayout;Landroid/view/ActionMode;)Landroid/view/ActionMode;
    .locals 0
    .param p0    # Lcom/android/videoeditor/widgets/MediaLinearLayout;
    .param p1    # Landroid/view/ActionMode;

    iput-object p1, p0, Lcom/android/videoeditor/widgets/MediaLinearLayout;->mMediaItemActionMode:Landroid/view/ActionMode;

    return-object p1
.end method

.method static synthetic access$100(Lcom/android/videoeditor/widgets/MediaLinearLayout;)Lcom/android/videoeditor/service/VideoEditorProject;
    .locals 1
    .param p0    # Lcom/android/videoeditor/widgets/MediaLinearLayout;

    iget-object v0, p0, Lcom/android/videoeditor/widgets/MediaLinearLayout;->mProject:Lcom/android/videoeditor/service/VideoEditorProject;

    return-object v0
.end method

.method static synthetic access$1000(Lcom/android/videoeditor/widgets/MediaLinearLayout;Landroid/view/View;)V
    .locals 0
    .param p0    # Lcom/android/videoeditor/widgets/MediaLinearLayout;
    .param p1    # Landroid/view/View;

    invoke-direct {p0, p1}, Lcom/android/videoeditor/widgets/MediaLinearLayout;->unSelect(Landroid/view/View;)V

    return-void
.end method

.method static synthetic access$1100(Lcom/android/videoeditor/widgets/MediaLinearLayout;Z)V
    .locals 0
    .param p0    # Lcom/android/videoeditor/widgets/MediaLinearLayout;
    .param p1    # Z

    invoke-direct {p0, p1}, Lcom/android/videoeditor/widgets/MediaLinearLayout;->showAddMediaItemButtons(Z)V

    return-void
.end method

.method static synthetic access$1200(Lcom/android/videoeditor/widgets/MediaLinearLayout;)Landroid/view/ActionMode;
    .locals 1
    .param p0    # Lcom/android/videoeditor/widgets/MediaLinearLayout;

    iget-object v0, p0, Lcom/android/videoeditor/widgets/MediaLinearLayout;->mTransitionActionMode:Landroid/view/ActionMode;

    return-object v0
.end method

.method static synthetic access$1202(Lcom/android/videoeditor/widgets/MediaLinearLayout;Landroid/view/ActionMode;)Landroid/view/ActionMode;
    .locals 0
    .param p0    # Lcom/android/videoeditor/widgets/MediaLinearLayout;
    .param p1    # Landroid/view/ActionMode;

    iput-object p1, p0, Lcom/android/videoeditor/widgets/MediaLinearLayout;->mTransitionActionMode:Landroid/view/ActionMode;

    return-object p1
.end method

.method static synthetic access$1300(Lcom/android/videoeditor/widgets/MediaLinearLayout;Lcom/android/videoeditor/service/MovieTransition;)V
    .locals 0
    .param p0    # Lcom/android/videoeditor/widgets/MediaLinearLayout;
    .param p1    # Lcom/android/videoeditor/service/MovieTransition;

    invoke-direct {p0, p1}, Lcom/android/videoeditor/widgets/MediaLinearLayout;->editTransition(Lcom/android/videoeditor/service/MovieTransition;)V

    return-void
.end method

.method static synthetic access$1400(Lcom/android/videoeditor/widgets/MediaLinearLayout;Ljava/lang/String;)Landroid/view/View;
    .locals 1
    .param p0    # Lcom/android/videoeditor/widgets/MediaLinearLayout;
    .param p1    # Ljava/lang/String;

    invoke-direct {p0, p1}, Lcom/android/videoeditor/widgets/MediaLinearLayout;->getTransitionView(Ljava/lang/String;)Landroid/view/View;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$1500(Lcom/android/videoeditor/widgets/MediaLinearLayout;Landroid/view/View;)V
    .locals 0
    .param p0    # Lcom/android/videoeditor/widgets/MediaLinearLayout;
    .param p1    # Landroid/view/View;

    invoke-direct {p0, p1}, Lcom/android/videoeditor/widgets/MediaLinearLayout;->select(Landroid/view/View;)V

    return-void
.end method

.method static synthetic access$1600(Lcom/android/videoeditor/widgets/MediaLinearLayout;)V
    .locals 0
    .param p0    # Lcom/android/videoeditor/widgets/MediaLinearLayout;

    invoke-direct {p0}, Lcom/android/videoeditor/widgets/MediaLinearLayout;->unselectAllTimelineViews()V

    return-void
.end method

.method static synthetic access$1700(Lcom/android/videoeditor/widgets/MediaLinearLayout;)Lcom/android/videoeditor/widgets/MediaLinearLayoutListener;
    .locals 1
    .param p0    # Lcom/android/videoeditor/widgets/MediaLinearLayout;

    iget-object v0, p0, Lcom/android/videoeditor/widgets/MediaLinearLayout;->mListener:Lcom/android/videoeditor/widgets/MediaLinearLayoutListener;

    return-object v0
.end method

.method static synthetic access$1800(Lcom/android/videoeditor/widgets/MediaLinearLayout;Z)V
    .locals 0
    .param p0    # Lcom/android/videoeditor/widgets/MediaLinearLayout;
    .param p1    # Z

    invoke-direct {p0, p1}, Lcom/android/videoeditor/widgets/MediaLinearLayout;->setIsTrimming(Z)V

    return-void
.end method

.method static synthetic access$1900(Lcom/android/videoeditor/widgets/MediaLinearLayout;)V
    .locals 0
    .param p0    # Lcom/android/videoeditor/widgets/MediaLinearLayout;

    invoke-direct {p0}, Lcom/android/videoeditor/widgets/MediaLinearLayout;->invalidateAllChildren()V

    return-void
.end method

.method static synthetic access$200(Lcom/android/videoeditor/widgets/MediaLinearLayout;)Z
    .locals 1
    .param p0    # Lcom/android/videoeditor/widgets/MediaLinearLayout;

    iget-boolean v0, p0, Lcom/android/videoeditor/widgets/MediaLinearLayout;->mPlaybackInProgress:Z

    return v0
.end method

.method static synthetic access$2000(Lcom/android/videoeditor/widgets/MediaLinearLayout;)Landroid/view/View;
    .locals 1
    .param p0    # Lcom/android/videoeditor/widgets/MediaLinearLayout;

    iget-object v0, p0, Lcom/android/videoeditor/widgets/MediaLinearLayout;->mScrollView:Landroid/view/View;

    return-object v0
.end method

.method static synthetic access$2100(Lcom/android/videoeditor/widgets/MediaLinearLayout;)Z
    .locals 1
    .param p0    # Lcom/android/videoeditor/widgets/MediaLinearLayout;

    iget-boolean v0, p0, Lcom/android/videoeditor/widgets/MediaLinearLayout;->mMoveLayoutPending:Z

    return v0
.end method

.method static synthetic access$2102(Lcom/android/videoeditor/widgets/MediaLinearLayout;Z)Z
    .locals 0
    .param p0    # Lcom/android/videoeditor/widgets/MediaLinearLayout;
    .param p1    # Z

    iput-boolean p1, p0, Lcom/android/videoeditor/widgets/MediaLinearLayout;->mMoveLayoutPending:Z

    return p1
.end method

.method static synthetic access$2200(Lcom/android/videoeditor/widgets/MediaLinearLayout;)I
    .locals 1
    .param p0    # Lcom/android/videoeditor/widgets/MediaLinearLayout;

    iget v0, p0, Lcom/android/videoeditor/widgets/MediaLinearLayout;->mHalfParentWidth:I

    return v0
.end method

.method static synthetic access$2300(Lcom/android/videoeditor/widgets/MediaLinearLayout;)Landroid/os/Handler;
    .locals 1
    .param p0    # Lcom/android/videoeditor/widgets/MediaLinearLayout;

    iget-object v0, p0, Lcom/android/videoeditor/widgets/MediaLinearLayout;->mHandler:Landroid/os/Handler;

    return-object v0
.end method

.method static synthetic access$2600(Lcom/android/videoeditor/widgets/MediaLinearLayout;Lcom/android/videoeditor/service/MovieMediaItem;)J
    .locals 2
    .param p0    # Lcom/android/videoeditor/widgets/MediaLinearLayout;
    .param p1    # Lcom/android/videoeditor/service/MovieMediaItem;

    invoke-direct {p0, p1}, Lcom/android/videoeditor/widgets/MediaLinearLayout;->getBeginTime(Lcom/android/videoeditor/service/MovieMediaItem;)J

    move-result-wide v0

    return-wide v0
.end method

.method static synthetic access$2900(Lcom/android/videoeditor/widgets/MediaLinearLayout;Lcom/android/videoeditor/service/MovieMediaItem;)J
    .locals 2
    .param p0    # Lcom/android/videoeditor/widgets/MediaLinearLayout;
    .param p1    # Lcom/android/videoeditor/service/MovieMediaItem;

    invoke-direct {p0, p1}, Lcom/android/videoeditor/widgets/MediaLinearLayout;->getEndTime(Lcom/android/videoeditor/service/MovieMediaItem;)J

    move-result-wide v0

    return-wide v0
.end method

.method static synthetic access$300(Lcom/android/videoeditor/widgets/MediaLinearLayout;Lcom/android/videoeditor/service/MovieMediaItem;)V
    .locals 0
    .param p0    # Lcom/android/videoeditor/widgets/MediaLinearLayout;
    .param p1    # Lcom/android/videoeditor/service/MovieMediaItem;

    invoke-direct {p0, p1}, Lcom/android/videoeditor/widgets/MediaLinearLayout;->editOverlay(Lcom/android/videoeditor/service/MovieMediaItem;)V

    return-void
.end method

.method static synthetic access$400(Lcom/android/videoeditor/widgets/MediaLinearLayout;Lcom/android/videoeditor/service/MovieMediaItem;)V
    .locals 0
    .param p0    # Lcom/android/videoeditor/widgets/MediaLinearLayout;
    .param p1    # Lcom/android/videoeditor/service/MovieMediaItem;

    invoke-direct {p0, p1}, Lcom/android/videoeditor/widgets/MediaLinearLayout;->removeOverlay(Lcom/android/videoeditor/service/MovieMediaItem;)V

    return-void
.end method

.method static synthetic access$500(Lcom/android/videoeditor/widgets/MediaLinearLayout;Lcom/android/videoeditor/service/MovieMediaItem;)Z
    .locals 1
    .param p0    # Lcom/android/videoeditor/widgets/MediaLinearLayout;
    .param p1    # Lcom/android/videoeditor/service/MovieMediaItem;

    invoke-direct {p0, p1}, Lcom/android/videoeditor/widgets/MediaLinearLayout;->pickTransition(Lcom/android/videoeditor/service/MovieMediaItem;)Z

    move-result v0

    return v0
.end method

.method static synthetic access$600(Lcom/android/videoeditor/widgets/MediaLinearLayout;Ljava/lang/String;)Landroid/view/View;
    .locals 1
    .param p0    # Lcom/android/videoeditor/widgets/MediaLinearLayout;
    .param p1    # Ljava/lang/String;

    invoke-direct {p0, p1}, Lcom/android/videoeditor/widgets/MediaLinearLayout;->getMediaItemView(Ljava/lang/String;)Landroid/view/View;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$700(Lcom/android/videoeditor/widgets/MediaLinearLayout;)Landroid/view/View;
    .locals 1
    .param p0    # Lcom/android/videoeditor/widgets/MediaLinearLayout;

    iget-object v0, p0, Lcom/android/videoeditor/widgets/MediaLinearLayout;->mSelectedView:Landroid/view/View;

    return-object v0
.end method

.method static synthetic access$800(Lcom/android/videoeditor/widgets/MediaLinearLayout;)Lcom/android/videoeditor/widgets/HandleView;
    .locals 1
    .param p0    # Lcom/android/videoeditor/widgets/MediaLinearLayout;

    iget-object v0, p0, Lcom/android/videoeditor/widgets/MediaLinearLayout;->mLeftHandle:Lcom/android/videoeditor/widgets/HandleView;

    return-object v0
.end method

.method static synthetic access$900(Lcom/android/videoeditor/widgets/MediaLinearLayout;)Lcom/android/videoeditor/widgets/HandleView;
    .locals 1
    .param p0    # Lcom/android/videoeditor/widgets/MediaLinearLayout;

    iget-object v0, p0, Lcom/android/videoeditor/widgets/MediaLinearLayout;->mRightHandle:Lcom/android/videoeditor/widgets/HandleView;

    return-object v0
.end method

.method private addMediaItem(Lcom/android/videoeditor/service/MovieMediaItem;)V
    .locals 11
    .param p1    # Lcom/android/videoeditor/service/MovieMediaItem;

    const/4 v10, 0x0

    invoke-virtual {p0}, Lcom/android/videoeditor/widgets/MediaLinearLayout;->getContext()Landroid/content/Context;

    move-result-object v8

    const v9, 0x7f04000e

    invoke-static {v8, v9, v10}, Lcom/android/videoeditor/widgets/MediaLinearLayout;->inflate(Landroid/content/Context;ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v4

    move-object v8, v4

    check-cast v8, Lcom/android/videoeditor/widgets/MediaItemView;

    iget-object v9, p0, Lcom/android/videoeditor/widgets/MediaLinearLayout;->mMediaItemGestureListener:Lcom/android/videoeditor/widgets/ItemSimpleGestureListener;

    invoke-virtual {v8, v9}, Lcom/android/videoeditor/widgets/MediaItemView;->setGestureListener(Lcom/android/videoeditor/widgets/ItemSimpleGestureListener;)V

    move-object v8, v4

    check-cast v8, Lcom/android/videoeditor/widgets/MediaItemView;

    iget-object v9, p0, Lcom/android/videoeditor/widgets/MediaLinearLayout;->mProject:Lcom/android/videoeditor/service/VideoEditorProject;

    invoke-virtual {v9}, Lcom/android/videoeditor/service/VideoEditorProject;->getPath()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Lcom/android/videoeditor/widgets/MediaItemView;->setProjectPath(Ljava/lang/String;)V

    invoke-virtual {v4, p1}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    new-instance v3, Landroid/widget/LinearLayout$LayoutParams;

    const/4 v8, -0x2

    const/4 v9, -0x1

    invoke-direct {v3, v8, v9}, Landroid/widget/LinearLayout$LayoutParams;-><init>(II)V

    invoke-virtual {p0}, Lcom/android/videoeditor/widgets/MediaLinearLayout;->getChildCount()I

    move-result v8

    add-int/lit8 v8, v8, -0x3

    invoke-virtual {p0, v4, v8, v3}, Lcom/android/videoeditor/widgets/MediaLinearLayout;->addView(Landroid/view/View;ILandroid/view/ViewGroup$LayoutParams;)V

    invoke-virtual {p1}, Lcom/android/videoeditor/service/MovieMediaItem;->getBeginTransition()Lcom/android/videoeditor/service/MovieTransition;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/android/videoeditor/widgets/MediaLinearLayout;->getChildCount()I

    move-result v1

    const/4 v8, 0x5

    if-le v1, v8, :cond_3

    add-int/lit8 v8, v1, -0x5

    invoke-virtual {p0, v8}, Lcom/android/videoeditor/widgets/MediaLinearLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v7

    invoke-virtual {v7}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v6

    if-eqz v6, :cond_0

    instance-of v8, v6, Lcom/android/videoeditor/service/MovieMediaItem;

    if-eqz v8, :cond_0

    move-object v5, v6

    check-cast v5, Lcom/android/videoeditor/service/MovieMediaItem;

    invoke-virtual {v5}, Lcom/android/videoeditor/service/MovieMediaItem;->getId()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {p0, v0, v8}, Lcom/android/videoeditor/widgets/MediaLinearLayout;->addTransition(Lcom/android/videoeditor/service/MovieTransition;Ljava/lang/String;)Landroid/view/View;

    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/android/videoeditor/service/MovieMediaItem;->getEndTransition()Lcom/android/videoeditor/service/MovieTransition;

    move-result-object v2

    if-eqz v2, :cond_1

    invoke-virtual {p1}, Lcom/android/videoeditor/service/MovieMediaItem;->getId()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {p0, v2, v8}, Lcom/android/videoeditor/widgets/MediaLinearLayout;->addTransition(Lcom/android/videoeditor/service/MovieTransition;Ljava/lang/String;)Landroid/view/View;

    :cond_1
    invoke-virtual {p0}, Lcom/android/videoeditor/widgets/MediaLinearLayout;->requestLayout()V

    iget-object v8, p0, Lcom/android/videoeditor/widgets/MediaLinearLayout;->mMediaItemActionMode:Landroid/view/ActionMode;

    if-eqz v8, :cond_2

    iget-object v8, p0, Lcom/android/videoeditor/widgets/MediaLinearLayout;->mMediaItemActionMode:Landroid/view/ActionMode;

    invoke-virtual {v8}, Landroid/view/ActionMode;->invalidate()V

    :cond_2
    iget-object v8, p0, Lcom/android/videoeditor/widgets/MediaLinearLayout;->mLeftAddClipButton:Landroid/widget/ImageButton;

    const/4 v9, 0x0

    invoke-virtual {v8, v9}, Landroid/widget/ImageButton;->setVisibility(I)V

    return-void

    :cond_3
    invoke-virtual {p0, v0, v10}, Lcom/android/videoeditor/widgets/MediaLinearLayout;->addTransition(Lcom/android/videoeditor/service/MovieTransition;Ljava/lang/String;)Landroid/view/View;

    goto :goto_0
.end method

.method private clearAndHideTrimHandles()V
    .locals 3

    const/4 v2, 0x0

    const/16 v1, 0x8

    iget-object v0, p0, Lcom/android/videoeditor/widgets/MediaLinearLayout;->mLeftHandle:Lcom/android/videoeditor/widgets/HandleView;

    invoke-virtual {v0, v1}, Lcom/android/videoeditor/widgets/HandleView;->setVisibility(I)V

    iget-object v0, p0, Lcom/android/videoeditor/widgets/MediaLinearLayout;->mLeftHandle:Lcom/android/videoeditor/widgets/HandleView;

    invoke-virtual {v0, v2}, Lcom/android/videoeditor/widgets/HandleView;->setListener(Lcom/android/videoeditor/widgets/HandleView$MoveListener;)V

    iget-object v0, p0, Lcom/android/videoeditor/widgets/MediaLinearLayout;->mRightHandle:Lcom/android/videoeditor/widgets/HandleView;

    invoke-virtual {v0, v1}, Lcom/android/videoeditor/widgets/HandleView;->setVisibility(I)V

    iget-object v0, p0, Lcom/android/videoeditor/widgets/MediaLinearLayout;->mRightHandle:Lcom/android/videoeditor/widgets/HandleView;

    invoke-virtual {v0, v2}, Lcom/android/videoeditor/widgets/HandleView;->setListener(Lcom/android/videoeditor/widgets/HandleView$MoveListener;)V

    return-void
.end method

.method private closeActionBars()V
    .locals 2

    const/4 v1, 0x0

    iget-object v0, p0, Lcom/android/videoeditor/widgets/MediaLinearLayout;->mMediaItemActionMode:Landroid/view/ActionMode;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/videoeditor/widgets/MediaLinearLayout;->mMediaItemActionMode:Landroid/view/ActionMode;

    invoke-virtual {v0}, Landroid/view/ActionMode;->finish()V

    iput-object v1, p0, Lcom/android/videoeditor/widgets/MediaLinearLayout;->mMediaItemActionMode:Landroid/view/ActionMode;

    :cond_0
    iget-object v0, p0, Lcom/android/videoeditor/widgets/MediaLinearLayout;->mTransitionActionMode:Landroid/view/ActionMode;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/android/videoeditor/widgets/MediaLinearLayout;->mTransitionActionMode:Landroid/view/ActionMode;

    invoke-virtual {v0}, Landroid/view/ActionMode;->finish()V

    iput-object v1, p0, Lcom/android/videoeditor/widgets/MediaLinearLayout;->mTransitionActionMode:Landroid/view/ActionMode;

    :cond_1
    return-void
.end method

.method private computeViewDuration(Landroid/view/View;)J
    .locals 7
    .param p1    # Landroid/view/View;

    invoke-virtual {p1}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v3

    if-eqz v3, :cond_3

    instance-of v5, v3, Lcom/android/videoeditor/service/MovieMediaItem;

    if-eqz v5, :cond_2

    invoke-virtual {p1}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/android/videoeditor/service/MovieMediaItem;

    invoke-virtual {v2}, Lcom/android/videoeditor/service/MovieMediaItem;->getAppTimelineDuration()J

    move-result-wide v0

    invoke-virtual {v2}, Lcom/android/videoeditor/service/MovieMediaItem;->getBeginTransition()Lcom/android/videoeditor/service/MovieTransition;

    move-result-object v5

    if-eqz v5, :cond_0

    invoke-virtual {v2}, Lcom/android/videoeditor/service/MovieMediaItem;->getBeginTransition()Lcom/android/videoeditor/service/MovieTransition;

    move-result-object v5

    invoke-virtual {v5}, Lcom/android/videoeditor/service/MovieTransition;->getAppDuration()J

    move-result-wide v5

    sub-long/2addr v0, v5

    :cond_0
    invoke-virtual {v2}, Lcom/android/videoeditor/service/MovieMediaItem;->getEndTransition()Lcom/android/videoeditor/service/MovieTransition;

    move-result-object v5

    if-eqz v5, :cond_1

    invoke-virtual {v2}, Lcom/android/videoeditor/service/MovieMediaItem;->getEndTransition()Lcom/android/videoeditor/service/MovieTransition;

    move-result-object v5

    invoke-virtual {v5}, Lcom/android/videoeditor/service/MovieTransition;->getAppDuration()J

    move-result-wide v5

    sub-long/2addr v0, v5

    :cond_1
    :goto_0
    return-wide v0

    :cond_2
    move-object v4, v3

    check-cast v4, Lcom/android/videoeditor/service/MovieTransition;

    invoke-virtual {v4}, Lcom/android/videoeditor/service/MovieTransition;->getAppDuration()J

    move-result-wide v0

    goto :goto_0

    :cond_3
    const-wide/16 v0, 0x0

    goto :goto_0
.end method

.method private editOverlay(Lcom/android/videoeditor/service/MovieMediaItem;)V
    .locals 7
    .param p1    # Lcom/android/videoeditor/service/MovieMediaItem;

    new-instance v1, Landroid/content/Intent;

    invoke-virtual {p0}, Lcom/android/videoeditor/widgets/MediaLinearLayout;->getContext()Landroid/content/Context;

    move-result-object v4

    const-class v5, Lcom/android/videoeditor/OverlayTitleEditor;

    invoke-direct {v1, v4, v5}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const-string v4, "media_item_id"

    invoke-virtual {p1}, Lcom/android/videoeditor/service/MovieMediaItem;->getId()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v1, v4, v5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    invoke-virtual {p1}, Lcom/android/videoeditor/service/MovieMediaItem;->getOverlay()Lcom/android/videoeditor/service/MovieOverlay;

    move-result-object v2

    if-eqz v2, :cond_0

    invoke-virtual {p1}, Lcom/android/videoeditor/service/MovieMediaItem;->getOverlay()Lcom/android/videoeditor/service/MovieOverlay;

    move-result-object v4

    invoke-virtual {v4}, Lcom/android/videoeditor/service/MovieOverlay;->getId()Ljava/lang/String;

    move-result-object v3

    const-string v4, "overlay_id"

    invoke-virtual {v1, v4, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    invoke-virtual {v2}, Lcom/android/videoeditor/service/MovieOverlay;->getType()I

    move-result v4

    invoke-virtual {v2}, Lcom/android/videoeditor/service/MovieOverlay;->getTitle()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v2}, Lcom/android/videoeditor/service/MovieOverlay;->getSubtitle()Ljava/lang/String;

    move-result-object v6

    invoke-static {v4, v5, v6}, Lcom/android/videoeditor/service/MovieOverlay;->buildUserAttributes(ILjava/lang/String;Ljava/lang/String;)Landroid/os/Bundle;

    move-result-object v0

    const-string v4, "attributes"

    invoke-virtual {v1, v4, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Bundle;)Landroid/content/Intent;

    :cond_0
    invoke-virtual {p0}, Lcom/android/videoeditor/widgets/MediaLinearLayout;->getContext()Landroid/content/Context;

    move-result-object v4

    check-cast v4, Landroid/app/Activity;

    const/16 v5, 0xc

    invoke-virtual {v4, v1, v5}, Landroid/app/Activity;->startActivityForResult(Landroid/content/Intent;I)V

    return-void
.end method

.method private editTransition(Lcom/android/videoeditor/service/MovieTransition;)V
    .locals 6
    .param p1    # Lcom/android/videoeditor/service/MovieTransition;

    iget-object v3, p0, Lcom/android/videoeditor/widgets/MediaLinearLayout;->mProject:Lcom/android/videoeditor/service/VideoEditorProject;

    invoke-virtual {v3, p1}, Lcom/android/videoeditor/service/VideoEditorProject;->getPreviousMediaItem(Lcom/android/videoeditor/service/MovieTransition;)Lcom/android/videoeditor/service/MovieMediaItem;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/android/videoeditor/service/MovieMediaItem;->getId()Ljava/lang/String;

    move-result-object v1

    :goto_0
    new-instance v2, Landroid/content/Intent;

    invoke-virtual {p0}, Lcom/android/videoeditor/widgets/MediaLinearLayout;->getContext()Landroid/content/Context;

    move-result-object v3

    const-class v4, Lcom/android/videoeditor/TransitionsActivity;

    invoke-direct {v2, v3, v4}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const-string v3, "media_item_id"

    invoke-virtual {v2, v3, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const-string v3, "transition_id"

    invoke-virtual {p1}, Lcom/android/videoeditor/service/MovieTransition;->getId()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const-string v3, "transition"

    invoke-virtual {p1}, Lcom/android/videoeditor/service/MovieTransition;->getType()I

    move-result v4

    invoke-virtual {v2, v3, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    const-string v3, "min_duration"

    const-wide/16 v4, 0xfa

    invoke-virtual {v2, v3, v4, v5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;J)Landroid/content/Intent;

    const-string v3, "default_duration"

    invoke-virtual {p1}, Lcom/android/videoeditor/service/MovieTransition;->getAppDuration()J

    move-result-wide v4

    invoke-virtual {v2, v3, v4, v5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;J)Landroid/content/Intent;

    const-string v3, "max_duration"

    invoke-direct {p0, v0}, Lcom/android/videoeditor/widgets/MediaLinearLayout;->getMaxTransitionDuration(Lcom/android/videoeditor/service/MovieMediaItem;)J

    move-result-wide v4

    invoke-virtual {v2, v3, v4, v5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;J)Landroid/content/Intent;

    invoke-virtual {p0}, Lcom/android/videoeditor/widgets/MediaLinearLayout;->getContext()Landroid/content/Context;

    move-result-object v3

    check-cast v3, Landroid/app/Activity;

    const/16 v4, 0xa

    invoke-virtual {v3, v2, v4}, Landroid/app/Activity;->startActivityForResult(Landroid/content/Intent;I)V

    return-void

    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method private getBeginTime(Lcom/android/videoeditor/service/MovieMediaItem;)J
    .locals 10
    .param p1    # Lcom/android/videoeditor/service/MovieMediaItem;

    iget-object v8, p0, Lcom/android/videoeditor/widgets/MediaLinearLayout;->mProject:Lcom/android/videoeditor/service/VideoEditorProject;

    invoke-virtual {v8}, Lcom/android/videoeditor/service/VideoEditorProject;->getMediaItems()Ljava/util/List;

    move-result-object v6

    const-wide/16 v0, 0x0

    invoke-interface {v6}, Ljava/util/List;->size()I

    move-result v7

    const/4 v4, 0x0

    :goto_0
    if-ge v4, v7, :cond_3

    invoke-interface {v6, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/android/videoeditor/service/MovieMediaItem;

    invoke-virtual {v5}, Lcom/android/videoeditor/service/MovieMediaItem;->getBeginTransition()Lcom/android/videoeditor/service/MovieTransition;

    move-result-object v2

    invoke-virtual {v5}, Lcom/android/videoeditor/service/MovieMediaItem;->getEndTransition()Lcom/android/videoeditor/service/MovieTransition;

    move-result-object v3

    invoke-virtual {p1}, Lcom/android/videoeditor/service/MovieMediaItem;->getId()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v5}, Lcom/android/videoeditor/service/MovieMediaItem;->getId()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_1

    if-eqz v2, :cond_0

    invoke-virtual {v2}, Lcom/android/videoeditor/service/MovieTransition;->getAppDuration()J

    move-result-wide v8

    add-long/2addr v0, v8

    :cond_0
    move-wide v8, v0

    :goto_1
    return-wide v8

    :cond_1
    invoke-virtual {v5}, Lcom/android/videoeditor/service/MovieMediaItem;->getAppTimelineDuration()J

    move-result-wide v8

    add-long/2addr v0, v8

    if-eqz v3, :cond_2

    invoke-virtual {v3}, Lcom/android/videoeditor/service/MovieTransition;->getAppDuration()J

    move-result-wide v8

    sub-long/2addr v0, v8

    :cond_2
    add-int/lit8 v4, v4, 0x1

    goto :goto_0

    :cond_3
    const-wide/16 v8, 0x0

    goto :goto_1
.end method

.method private getEndTime(Lcom/android/videoeditor/service/MovieMediaItem;)J
    .locals 10
    .param p1    # Lcom/android/videoeditor/service/MovieMediaItem;

    iget-object v8, p0, Lcom/android/videoeditor/widgets/MediaLinearLayout;->mProject:Lcom/android/videoeditor/service/VideoEditorProject;

    invoke-virtual {v8}, Lcom/android/videoeditor/service/VideoEditorProject;->getMediaItems()Ljava/util/List;

    move-result-object v6

    const-wide/16 v1, 0x0

    invoke-interface {v6}, Ljava/util/List;->size()I

    move-result v7

    const/4 v4, 0x0

    :goto_0
    if-ge v4, v7, :cond_2

    invoke-interface {v6, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/android/videoeditor/service/MovieMediaItem;

    invoke-virtual {v5}, Lcom/android/videoeditor/service/MovieMediaItem;->getBeginTransition()Lcom/android/videoeditor/service/MovieTransition;

    move-result-object v0

    invoke-virtual {v5}, Lcom/android/videoeditor/service/MovieMediaItem;->getEndTransition()Lcom/android/videoeditor/service/MovieTransition;

    move-result-object v3

    invoke-virtual {v5}, Lcom/android/videoeditor/service/MovieMediaItem;->getAppTimelineDuration()J

    move-result-wide v8

    add-long/2addr v1, v8

    if-eqz v3, :cond_0

    invoke-virtual {v3}, Lcom/android/videoeditor/service/MovieTransition;->getAppDuration()J

    move-result-wide v8

    sub-long/2addr v1, v8

    :cond_0
    invoke-virtual {p1}, Lcom/android/videoeditor/service/MovieMediaItem;->getId()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v5}, Lcom/android/videoeditor/service/MovieMediaItem;->getId()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_1

    move-wide v8, v1

    :goto_1
    return-wide v8

    :cond_1
    add-int/lit8 v4, v4, 0x1

    goto :goto_0

    :cond_2
    const-wide/16 v8, 0x0

    goto :goto_1
.end method

.method private getLeftDropPosition()J
    .locals 15

    const/4 v14, -0x1

    const/4 v13, 0x0

    iget-object v11, p0, Lcom/android/videoeditor/widgets/MediaLinearLayout;->mProject:Lcom/android/videoeditor/service/VideoEditorProject;

    invoke-virtual {v11}, Lcom/android/videoeditor/service/VideoEditorProject;->getMediaItems()Ljava/util/List;

    move-result-object v6

    const-wide/16 v0, 0x0

    const-wide/16 v2, 0x0

    iget-object v11, p0, Lcom/android/videoeditor/widgets/MediaLinearLayout;->mProject:Lcom/android/videoeditor/service/VideoEditorProject;

    invoke-virtual {v11}, Lcom/android/videoeditor/service/VideoEditorProject;->getPlayheadPos()J

    move-result-wide v9

    invoke-interface {v6}, Ljava/util/List;->size()I

    move-result v7

    const/4 v4, 0x0

    :goto_0
    if-ge v4, v7, :cond_2

    invoke-interface {v6, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/android/videoeditor/service/MovieMediaItem;

    invoke-virtual {v5}, Lcom/android/videoeditor/service/MovieMediaItem;->getAppTimelineDuration()J

    move-result-wide v11

    add-long v2, v0, v11

    invoke-virtual {v5}, Lcom/android/videoeditor/service/MovieMediaItem;->getEndTransition()Lcom/android/videoeditor/service/MovieTransition;

    move-result-object v11

    if-eqz v11, :cond_0

    add-int/lit8 v11, v7, -0x1

    if-ge v4, v11, :cond_0

    invoke-virtual {v5}, Lcom/android/videoeditor/service/MovieMediaItem;->getEndTransition()Lcom/android/videoeditor/service/MovieTransition;

    move-result-object v11

    invoke-virtual {v11}, Lcom/android/videoeditor/service/MovieTransition;->getAppDuration()J

    move-result-wide v11

    sub-long/2addr v2, v11

    :cond_0
    cmp-long v11, v9, v0

    if-lez v11, :cond_6

    cmp-long v11, v9, v2

    if-gtz v11, :cond_6

    invoke-virtual {v5}, Lcom/android/videoeditor/service/MovieMediaItem;->getBeginTransition()Lcom/android/videoeditor/service/MovieTransition;

    move-result-object v11

    if-eqz v11, :cond_1

    invoke-virtual {v5}, Lcom/android/videoeditor/service/MovieMediaItem;->getBeginTransition()Lcom/android/videoeditor/service/MovieTransition;

    move-result-object v11

    invoke-virtual {v11}, Lcom/android/videoeditor/service/MovieTransition;->getAppDuration()J

    move-result-wide v11

    add-long/2addr v0, v11

    :cond_1
    iget-object v11, p0, Lcom/android/videoeditor/widgets/MediaLinearLayout;->mDragMediaItemId:Ljava/lang/String;

    invoke-virtual {v5}, Lcom/android/videoeditor/service/MovieMediaItem;->getId()Ljava/lang/String;

    move-result-object v12

    invoke-virtual {v11, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-nez v11, :cond_5

    if-lez v4, :cond_4

    add-int/lit8 v11, v4, -0x1

    invoke-interface {v6, v11}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Lcom/android/videoeditor/service/MovieMediaItem;

    iget-object v11, p0, Lcom/android/videoeditor/widgets/MediaLinearLayout;->mDragMediaItemId:Ljava/lang/String;

    invoke-virtual {v8}, Lcom/android/videoeditor/service/MovieMediaItem;->getId()Ljava/lang/String;

    move-result-object v12

    invoke-virtual {v11, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-nez v11, :cond_3

    iput-object v8, p0, Lcom/android/videoeditor/widgets/MediaLinearLayout;->mDropAfterMediaItem:Lcom/android/videoeditor/service/MovieMediaItem;

    iput v4, p0, Lcom/android/videoeditor/widgets/MediaLinearLayout;->mDropIndex:I

    move-wide v9, v0

    :cond_2
    :goto_1
    return-wide v9

    :cond_3
    iput-object v13, p0, Lcom/android/videoeditor/widgets/MediaLinearLayout;->mDropAfterMediaItem:Lcom/android/videoeditor/service/MovieMediaItem;

    iput v14, p0, Lcom/android/videoeditor/widgets/MediaLinearLayout;->mDropIndex:I

    move-wide v9, v0

    goto :goto_1

    :cond_4
    iput-object v13, p0, Lcom/android/videoeditor/widgets/MediaLinearLayout;->mDropAfterMediaItem:Lcom/android/videoeditor/service/MovieMediaItem;

    const/4 v11, 0x0

    iput v11, p0, Lcom/android/videoeditor/widgets/MediaLinearLayout;->mDropIndex:I

    const-wide/16 v9, 0x0

    goto :goto_1

    :cond_5
    iput-object v13, p0, Lcom/android/videoeditor/widgets/MediaLinearLayout;->mDropAfterMediaItem:Lcom/android/videoeditor/service/MovieMediaItem;

    iput v14, p0, Lcom/android/videoeditor/widgets/MediaLinearLayout;->mDropIndex:I

    move-wide v9, v0

    goto :goto_1

    :cond_6
    move-wide v0, v2

    add-int/lit8 v4, v4, 0x1

    goto :goto_0
.end method

.method private getMaxTransitionDuration(Lcom/android/videoeditor/service/MovieMediaItem;)J
    .locals 12
    .param p1    # Lcom/android/videoeditor/service/MovieMediaItem;

    const-wide/16 v10, 0xbb8

    const-wide/16 v8, 0x4

    if-nez p1, :cond_0

    iget-object v4, p0, Lcom/android/videoeditor/widgets/MediaLinearLayout;->mProject:Lcom/android/videoeditor/service/VideoEditorProject;

    invoke-virtual {v4}, Lcom/android/videoeditor/service/VideoEditorProject;->getFirstMediaItem()Lcom/android/videoeditor/service/MovieMediaItem;

    move-result-object v1

    invoke-virtual {v1}, Lcom/android/videoeditor/service/MovieMediaItem;->getAppTimelineDuration()J

    move-result-wide v4

    div-long/2addr v4, v8

    invoke-static {v10, v11, v4, v5}, Ljava/lang/Math;->min(JJ)J

    move-result-wide v4

    :goto_0
    return-wide v4

    :cond_0
    iget-object v4, p0, Lcom/android/videoeditor/widgets/MediaLinearLayout;->mProject:Lcom/android/videoeditor/service/VideoEditorProject;

    invoke-virtual {p1}, Lcom/android/videoeditor/service/MovieMediaItem;->getId()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Lcom/android/videoeditor/service/VideoEditorProject;->isLastMediaItem(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_1

    invoke-virtual {p1}, Lcom/android/videoeditor/service/MovieMediaItem;->getAppTimelineDuration()J

    move-result-wide v4

    div-long/2addr v4, v8

    invoke-static {v10, v11, v4, v5}, Ljava/lang/Math;->min(JJ)J

    move-result-wide v4

    goto :goto_0

    :cond_1
    iget-object v4, p0, Lcom/android/videoeditor/widgets/MediaLinearLayout;->mProject:Lcom/android/videoeditor/service/VideoEditorProject;

    invoke-virtual {p1}, Lcom/android/videoeditor/service/MovieMediaItem;->getId()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Lcom/android/videoeditor/service/VideoEditorProject;->getNextMediaItem(Ljava/lang/String;)Lcom/android/videoeditor/service/MovieMediaItem;

    move-result-object v0

    invoke-virtual {p1}, Lcom/android/videoeditor/service/MovieMediaItem;->getAppTimelineDuration()J

    move-result-wide v4

    invoke-virtual {v0}, Lcom/android/videoeditor/service/MovieMediaItem;->getAppTimelineDuration()J

    move-result-wide v6

    invoke-static {v4, v5, v6, v7}, Ljava/lang/Math;->min(JJ)J

    move-result-wide v2

    div-long v4, v2, v8

    invoke-static {v10, v11, v4, v5}, Ljava/lang/Math;->min(JJ)J

    move-result-wide v4

    goto :goto_0
.end method

.method private getMediaItemView(Ljava/lang/String;)Landroid/view/View;
    .locals 6
    .param p1    # Ljava/lang/String;

    invoke-virtual {p0}, Lcom/android/videoeditor/widgets/MediaLinearLayout;->getChildCount()I

    move-result v1

    const/4 v2, 0x0

    :goto_0
    if-ge v2, v1, :cond_1

    invoke-virtual {p0, v2}, Lcom/android/videoeditor/widgets/MediaLinearLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v4

    if-eqz v4, :cond_0

    instance-of v5, v4, Lcom/android/videoeditor/service/MovieMediaItem;

    if-eqz v5, :cond_0

    move-object v3, v4

    check-cast v3, Lcom/android/videoeditor/service/MovieMediaItem;

    invoke-virtual {v3}, Lcom/android/videoeditor/service/MovieMediaItem;->getId()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {p1, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_0

    :goto_1
    return-object v0

    :cond_0
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    :cond_1
    const/4 v0, 0x0

    goto :goto_1
.end method

.method private getMediaItemViewIndex(Ljava/lang/String;)I
    .locals 6
    .param p1    # Ljava/lang/String;

    invoke-virtual {p0}, Lcom/android/videoeditor/widgets/MediaLinearLayout;->getChildCount()I

    move-result v1

    const/4 v2, 0x0

    :goto_0
    if-ge v2, v1, :cond_1

    invoke-virtual {p0, v2}, Lcom/android/videoeditor/widgets/MediaLinearLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v4

    if-eqz v4, :cond_0

    instance-of v5, v4, Lcom/android/videoeditor/service/MovieMediaItem;

    if-eqz v5, :cond_0

    move-object v3, v4

    check-cast v3, Lcom/android/videoeditor/service/MovieMediaItem;

    invoke-virtual {v3}, Lcom/android/videoeditor/service/MovieMediaItem;->getId()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {p1, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_0

    :goto_1
    return v2

    :cond_0
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    :cond_1
    const/4 v2, -0x1

    goto :goto_1
.end method

.method private getRightDropPosition()J
    .locals 15

    const/4 v14, 0x0

    const/4 v13, -0x1

    iget-object v11, p0, Lcom/android/videoeditor/widgets/MediaLinearLayout;->mProject:Lcom/android/videoeditor/service/VideoEditorProject;

    invoke-virtual {v11}, Lcom/android/videoeditor/service/VideoEditorProject;->getMediaItems()Ljava/util/List;

    move-result-object v6

    const-wide/16 v0, 0x0

    const-wide/16 v2, 0x0

    iget-object v11, p0, Lcom/android/videoeditor/widgets/MediaLinearLayout;->mProject:Lcom/android/videoeditor/service/VideoEditorProject;

    invoke-virtual {v11}, Lcom/android/videoeditor/service/VideoEditorProject;->getPlayheadPos()J

    move-result-wide v9

    invoke-interface {v6}, Ljava/util/List;->size()I

    move-result v7

    const/4 v4, 0x0

    :goto_0
    if-ge v4, v7, :cond_1

    invoke-interface {v6, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/android/videoeditor/service/MovieMediaItem;

    invoke-virtual {v5}, Lcom/android/videoeditor/service/MovieMediaItem;->getAppTimelineDuration()J

    move-result-wide v11

    add-long v2, v0, v11

    invoke-virtual {v5}, Lcom/android/videoeditor/service/MovieMediaItem;->getEndTransition()Lcom/android/videoeditor/service/MovieTransition;

    move-result-object v11

    if-eqz v11, :cond_0

    add-int/lit8 v11, v7, -0x1

    if-ge v4, v11, :cond_0

    invoke-virtual {v5}, Lcom/android/videoeditor/service/MovieMediaItem;->getEndTransition()Lcom/android/videoeditor/service/MovieTransition;

    move-result-object v11

    invoke-virtual {v11}, Lcom/android/videoeditor/service/MovieTransition;->getAppDuration()J

    move-result-wide v11

    sub-long/2addr v2, v11

    :cond_0
    cmp-long v11, v9, v0

    if-ltz v11, :cond_5

    cmp-long v11, v9, v2

    if-gez v11, :cond_5

    iget-object v11, p0, Lcom/android/videoeditor/widgets/MediaLinearLayout;->mDragMediaItemId:Ljava/lang/String;

    invoke-virtual {v5}, Lcom/android/videoeditor/service/MovieMediaItem;->getId()Ljava/lang/String;

    move-result-object v12

    invoke-virtual {v11, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-nez v11, :cond_4

    add-int/lit8 v11, v7, -0x1

    if-ge v4, v11, :cond_3

    add-int/lit8 v11, v4, 0x1

    invoke-interface {v6, v11}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Lcom/android/videoeditor/service/MovieMediaItem;

    iget-object v11, p0, Lcom/android/videoeditor/widgets/MediaLinearLayout;->mDragMediaItemId:Ljava/lang/String;

    invoke-virtual {v8}, Lcom/android/videoeditor/service/MovieMediaItem;->getId()Ljava/lang/String;

    move-result-object v12

    invoke-virtual {v11, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-nez v11, :cond_2

    iput-object v5, p0, Lcom/android/videoeditor/widgets/MediaLinearLayout;->mDropAfterMediaItem:Lcom/android/videoeditor/service/MovieMediaItem;

    iput v4, p0, Lcom/android/videoeditor/widgets/MediaLinearLayout;->mDropIndex:I

    move-wide v9, v2

    :cond_1
    :goto_1
    return-wide v9

    :cond_2
    iput-object v14, p0, Lcom/android/videoeditor/widgets/MediaLinearLayout;->mDropAfterMediaItem:Lcom/android/videoeditor/service/MovieMediaItem;

    iput v13, p0, Lcom/android/videoeditor/widgets/MediaLinearLayout;->mDropIndex:I

    move-wide v9, v2

    goto :goto_1

    :cond_3
    iput-object v5, p0, Lcom/android/videoeditor/widgets/MediaLinearLayout;->mDropAfterMediaItem:Lcom/android/videoeditor/service/MovieMediaItem;

    iput v4, p0, Lcom/android/videoeditor/widgets/MediaLinearLayout;->mDropIndex:I

    move-wide v9, v2

    goto :goto_1

    :cond_4
    iput-object v14, p0, Lcom/android/videoeditor/widgets/MediaLinearLayout;->mDropAfterMediaItem:Lcom/android/videoeditor/service/MovieMediaItem;

    iput v13, p0, Lcom/android/videoeditor/widgets/MediaLinearLayout;->mDropIndex:I

    move-wide v9, v2

    goto :goto_1

    :cond_5
    move-wide v0, v2

    add-int/lit8 v4, v4, 0x1

    goto :goto_0
.end method

.method private getTransitionDuration(Lcom/android/videoeditor/service/MovieMediaItem;)J
    .locals 12
    .param p1    # Lcom/android/videoeditor/service/MovieMediaItem;

    const-wide/16 v10, 0x5dc

    const-wide/16 v8, 0x4

    if-nez p1, :cond_0

    iget-object v4, p0, Lcom/android/videoeditor/widgets/MediaLinearLayout;->mProject:Lcom/android/videoeditor/service/VideoEditorProject;

    invoke-virtual {v4}, Lcom/android/videoeditor/service/VideoEditorProject;->getFirstMediaItem()Lcom/android/videoeditor/service/MovieMediaItem;

    move-result-object v1

    invoke-virtual {v1}, Lcom/android/videoeditor/service/MovieMediaItem;->getAppTimelineDuration()J

    move-result-wide v4

    div-long/2addr v4, v8

    invoke-static {v10, v11, v4, v5}, Ljava/lang/Math;->min(JJ)J

    move-result-wide v4

    :goto_0
    return-wide v4

    :cond_0
    iget-object v4, p0, Lcom/android/videoeditor/widgets/MediaLinearLayout;->mProject:Lcom/android/videoeditor/service/VideoEditorProject;

    invoke-virtual {p1}, Lcom/android/videoeditor/service/MovieMediaItem;->getId()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Lcom/android/videoeditor/service/VideoEditorProject;->isLastMediaItem(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_1

    invoke-virtual {p1}, Lcom/android/videoeditor/service/MovieMediaItem;->getAppTimelineDuration()J

    move-result-wide v4

    div-long/2addr v4, v8

    invoke-static {v10, v11, v4, v5}, Ljava/lang/Math;->min(JJ)J

    move-result-wide v4

    goto :goto_0

    :cond_1
    iget-object v4, p0, Lcom/android/videoeditor/widgets/MediaLinearLayout;->mProject:Lcom/android/videoeditor/service/VideoEditorProject;

    invoke-virtual {p1}, Lcom/android/videoeditor/service/MovieMediaItem;->getId()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Lcom/android/videoeditor/service/VideoEditorProject;->getNextMediaItem(Ljava/lang/String;)Lcom/android/videoeditor/service/MovieMediaItem;

    move-result-object v0

    invoke-virtual {p1}, Lcom/android/videoeditor/service/MovieMediaItem;->getAppTimelineDuration()J

    move-result-wide v4

    invoke-virtual {v0}, Lcom/android/videoeditor/service/MovieMediaItem;->getAppTimelineDuration()J

    move-result-wide v6

    invoke-static {v4, v5, v6, v7}, Ljava/lang/Math;->min(JJ)J

    move-result-wide v2

    div-long v4, v2, v8

    invoke-static {v10, v11, v4, v5}, Ljava/lang/Math;->min(JJ)J

    move-result-wide v4

    goto :goto_0
.end method

.method private getTransitionView(Ljava/lang/String;)Landroid/view/View;
    .locals 6
    .param p1    # Ljava/lang/String;

    invoke-virtual {p0}, Lcom/android/videoeditor/widgets/MediaLinearLayout;->getChildCount()I

    move-result v1

    const/4 v2, 0x0

    :goto_0
    if-ge v2, v1, :cond_1

    invoke-virtual {p0, v2}, Lcom/android/videoeditor/widgets/MediaLinearLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v3

    if-eqz v3, :cond_0

    instance-of v5, v3, Lcom/android/videoeditor/service/MovieTransition;

    if-eqz v5, :cond_0

    move-object v4, v3

    check-cast v4, Lcom/android/videoeditor/service/MovieTransition;

    invoke-virtual {v4}, Lcom/android/videoeditor/service/MovieTransition;->getId()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {p1, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_0

    :goto_1
    return-object v0

    :cond_0
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    :cond_1
    const/4 v0, 0x0

    goto :goto_1
.end method

.method private invalidateAllChildren()V
    .locals 3

    invoke-virtual {p0}, Lcom/android/videoeditor/widgets/MediaLinearLayout;->getChildCount()I

    move-result v1

    const/4 v2, 0x0

    :goto_0
    if-ge v2, v1, :cond_0

    invoke-virtual {p0, v2}, Lcom/android/videoeditor/widgets/MediaLinearLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/View;->invalidate()V

    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    :cond_0
    return-void
.end method

.method private moveToPosition(F)V
    .locals 14
    .param p1    # F

    const/4 v13, 0x3

    const/4 v12, 0x2

    const/4 v11, 0x1

    const/high16 v10, 0x42280000

    const v9, 0x7f080002

    float-to-int v5, p1

    iget-object v6, p0, Lcom/android/videoeditor/widgets/MediaLinearLayout;->mScrollView:Landroid/view/View;

    invoke-virtual {v6}, Landroid/view/View;->getScrollX()I

    move-result v6

    sub-int v4, v5, v6

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    iget-wide v5, p0, Lcom/android/videoeditor/widgets/MediaLinearLayout;->mPrevDragScrollTime:J

    sub-long v5, v0, v5

    const-wide/16 v7, 0x12c

    cmp-long v5, v5, v7

    if-lez v5, :cond_4

    int-to-float v5, v4

    iget v6, p0, Lcom/android/videoeditor/widgets/MediaLinearLayout;->mPrevDragPosition:F

    sub-float/2addr v6, v10

    cmpg-float v5, v5, v6

    if-gez v5, :cond_2

    invoke-direct {p0}, Lcom/android/videoeditor/widgets/MediaLinearLayout;->getLeftDropPosition()J

    move-result-wide v2

    iget v5, p0, Lcom/android/videoeditor/widgets/MediaLinearLayout;->mDropIndex:I

    if-ltz v5, :cond_1

    iget-object v5, p0, Lcom/android/videoeditor/widgets/MediaLinearLayout;->mScrollView:Landroid/view/View;

    invoke-static {v12}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    invoke-virtual {v5, v9, v6}, Landroid/view/View;->setTag(ILjava/lang/Object;)V

    :goto_0
    iget-object v5, p0, Lcom/android/videoeditor/widgets/MediaLinearLayout;->mListener:Lcom/android/videoeditor/widgets/MediaLinearLayoutListener;

    invoke-interface {v5, v2, v3, v11}, Lcom/android/videoeditor/widgets/MediaLinearLayoutListener;->onRequestMovePlayhead(JZ)V

    iget-object v5, p0, Lcom/android/videoeditor/widgets/MediaLinearLayout;->mScrollView:Landroid/view/View;

    invoke-virtual {v5}, Landroid/view/View;->invalidate()V

    int-to-float v5, v4

    iput v5, p0, Lcom/android/videoeditor/widgets/MediaLinearLayout;->mPrevDragPosition:F

    iput-wide v0, p0, Lcom/android/videoeditor/widgets/MediaLinearLayout;->mPrevDragScrollTime:J

    :cond_0
    :goto_1
    return-void

    :cond_1
    iget-object v5, p0, Lcom/android/videoeditor/widgets/MediaLinearLayout;->mScrollView:Landroid/view/View;

    invoke-static {v13}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    invoke-virtual {v5, v9, v6}, Landroid/view/View;->setTag(ILjava/lang/Object;)V

    goto :goto_0

    :cond_2
    int-to-float v5, v4

    iget v6, p0, Lcom/android/videoeditor/widgets/MediaLinearLayout;->mPrevDragPosition:F

    add-float/2addr v6, v10

    cmpl-float v5, v5, v6

    if-lez v5, :cond_0

    invoke-direct {p0}, Lcom/android/videoeditor/widgets/MediaLinearLayout;->getRightDropPosition()J

    move-result-wide v2

    iget v5, p0, Lcom/android/videoeditor/widgets/MediaLinearLayout;->mDropIndex:I

    if-ltz v5, :cond_3

    iget-object v5, p0, Lcom/android/videoeditor/widgets/MediaLinearLayout;->mScrollView:Landroid/view/View;

    invoke-static {v12}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    invoke-virtual {v5, v9, v6}, Landroid/view/View;->setTag(ILjava/lang/Object;)V

    :goto_2
    iget-object v5, p0, Lcom/android/videoeditor/widgets/MediaLinearLayout;->mListener:Lcom/android/videoeditor/widgets/MediaLinearLayoutListener;

    invoke-interface {v5, v2, v3, v11}, Lcom/android/videoeditor/widgets/MediaLinearLayoutListener;->onRequestMovePlayhead(JZ)V

    iget-object v5, p0, Lcom/android/videoeditor/widgets/MediaLinearLayout;->mScrollView:Landroid/view/View;

    invoke-virtual {v5}, Landroid/view/View;->invalidate()V

    int-to-float v5, v4

    iput v5, p0, Lcom/android/videoeditor/widgets/MediaLinearLayout;->mPrevDragPosition:F

    iput-wide v0, p0, Lcom/android/videoeditor/widgets/MediaLinearLayout;->mPrevDragScrollTime:J

    goto :goto_1

    :cond_3
    iget-object v5, p0, Lcom/android/videoeditor/widgets/MediaLinearLayout;->mScrollView:Landroid/view/View;

    invoke-static {v13}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    invoke-virtual {v5, v9, v6}, Landroid/view/View;->setTag(ILjava/lang/Object;)V

    goto :goto_2

    :cond_4
    int-to-float v5, v4

    iput v5, p0, Lcom/android/videoeditor/widgets/MediaLinearLayout;->mPrevDragPosition:F

    goto :goto_1
.end method

.method private pickTransition(Lcom/android/videoeditor/service/MovieMediaItem;)Z
    .locals 8
    .param p1    # Lcom/android/videoeditor/service/MovieMediaItem;

    const-wide/16 v6, 0xfa

    const/4 v4, 0x0

    invoke-direct {p0, p1}, Lcom/android/videoeditor/widgets/MediaLinearLayout;->getTransitionDuration(Lcom/android/videoeditor/service/MovieMediaItem;)J

    move-result-wide v2

    cmp-long v5, v2, v6

    if-gez v5, :cond_0

    invoke-virtual {p0}, Lcom/android/videoeditor/widgets/MediaLinearLayout;->getContext()Landroid/content/Context;

    move-result-object v5

    invoke-virtual {p0}, Lcom/android/videoeditor/widgets/MediaLinearLayout;->getContext()Landroid/content/Context;

    move-result-object v6

    const v7, 0x7f090055

    invoke-virtual {v6, v7}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6, v4}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v5

    invoke-virtual {v5}, Landroid/widget/Toast;->show()V

    :goto_0
    return v4

    :cond_0
    if-eqz p1, :cond_1

    invoke-virtual {p1}, Lcom/android/videoeditor/service/MovieMediaItem;->getId()Ljava/lang/String;

    move-result-object v0

    :goto_1
    new-instance v1, Landroid/content/Intent;

    invoke-virtual {p0}, Lcom/android/videoeditor/widgets/MediaLinearLayout;->getContext()Landroid/content/Context;

    move-result-object v4

    const-class v5, Lcom/android/videoeditor/TransitionsActivity;

    invoke-direct {v1, v4, v5}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const-string v4, "media_item_id"

    invoke-virtual {v1, v4, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const-string v4, "min_duration"

    invoke-virtual {v1, v4, v6, v7}, Landroid/content/Intent;->putExtra(Ljava/lang/String;J)Landroid/content/Intent;

    const-string v4, "default_duration"

    invoke-virtual {v1, v4, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;J)Landroid/content/Intent;

    const-string v4, "max_duration"

    invoke-direct {p0, p1}, Lcom/android/videoeditor/widgets/MediaLinearLayout;->getMaxTransitionDuration(Lcom/android/videoeditor/service/MovieMediaItem;)J

    move-result-wide v5

    invoke-virtual {v1, v4, v5, v6}, Landroid/content/Intent;->putExtra(Ljava/lang/String;J)Landroid/content/Intent;

    invoke-virtual {p0}, Lcom/android/videoeditor/widgets/MediaLinearLayout;->getContext()Landroid/content/Context;

    move-result-object v4

    check-cast v4, Landroid/app/Activity;

    const/16 v5, 0xb

    invoke-virtual {v4, v1, v5}, Landroid/app/Activity;->startActivityForResult(Landroid/content/Intent;I)V

    const/4 v4, 0x1

    goto :goto_0

    :cond_1
    const/4 v0, 0x0

    goto :goto_1
.end method

.method private removeAllMediaItemAndTransitionViews()V
    .locals 4

    const/4 v0, 0x0

    :goto_0
    invoke-virtual {p0}, Lcom/android/videoeditor/widgets/MediaLinearLayout;->getChildCount()I

    move-result v2

    if-ge v0, v2, :cond_1

    invoke-virtual {p0, v0}, Lcom/android/videoeditor/widgets/MediaLinearLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v2

    invoke-virtual {v2}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-virtual {p0, v0}, Lcom/android/videoeditor/widgets/MediaLinearLayout;->removeViewAt(I)V

    goto :goto_0

    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_1
    invoke-virtual {p0}, Lcom/android/videoeditor/widgets/MediaLinearLayout;->requestLayout()V

    iget-object v2, p0, Lcom/android/videoeditor/widgets/MediaLinearLayout;->mLeftAddClipButton:Landroid/widget/ImageButton;

    const/16 v3, 0x8

    invoke-virtual {v2, v3}, Landroid/widget/ImageButton;->setVisibility(I)V

    return-void
.end method

.method private removeOverlay(Lcom/android/videoeditor/service/MovieMediaItem;)V
    .locals 3
    .param p1    # Lcom/android/videoeditor/service/MovieMediaItem;

    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    const-string v1, "media_item_id"

    invoke-virtual {p1}, Lcom/android/videoeditor/service/MovieMediaItem;->getId()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {p0}, Lcom/android/videoeditor/widgets/MediaLinearLayout;->getContext()Landroid/content/Context;

    move-result-object v1

    check-cast v1, Landroid/app/Activity;

    const/16 v2, 0xd

    invoke-virtual {v1, v2, v0}, Landroid/app/Activity;->showDialog(ILandroid/os/Bundle;)Z

    return-void
.end method

.method private select(Landroid/view/View;)V
    .locals 13
    .param p1    # Landroid/view/View;

    invoke-virtual {p1}, Landroid/view/View;->isSelected()Z

    move-result v6

    if-eqz v6, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    invoke-direct {p0}, Lcom/android/videoeditor/widgets/MediaLinearLayout;->unselectAllTimelineViews()V

    iput-object p1, p0, Lcom/android/videoeditor/widgets/MediaLinearLayout;->mSelectedView:Landroid/view/View;

    iget-object v6, p0, Lcom/android/videoeditor/widgets/MediaLinearLayout;->mSelectedView:Landroid/view/View;

    const/4 v7, 0x1

    invoke-virtual {v6, v7}, Landroid/view/View;->setSelected(Z)V

    const/4 v6, 0x0

    invoke-direct {p0, v6}, Lcom/android/videoeditor/widgets/MediaLinearLayout;->showAddMediaItemButtons(Z)V

    iget-object v6, p0, Lcom/android/videoeditor/widgets/MediaLinearLayout;->mSelectedView:Landroid/view/View;

    invoke-virtual {v6}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v4

    instance-of v6, v4, Lcom/android/videoeditor/service/MovieMediaItem;

    if-eqz v6, :cond_b

    iget-object v2, p0, Lcom/android/videoeditor/widgets/MediaLinearLayout;->mSelectedView:Landroid/view/View;

    check-cast v2, Lcom/android/videoeditor/widgets/MediaItemView;

    invoke-virtual {v2}, Lcom/android/videoeditor/widgets/MediaItemView;->isGeneratingEffect()Z

    move-result v6

    if-eqz v6, :cond_4

    iget-object v6, p0, Lcom/android/videoeditor/widgets/MediaLinearLayout;->mLeftHandle:Lcom/android/videoeditor/widgets/HandleView;

    const/4 v7, 0x0

    invoke-virtual {v6, v7}, Lcom/android/videoeditor/widgets/HandleView;->setEnabled(Z)V

    iget-object v6, p0, Lcom/android/videoeditor/widgets/MediaLinearLayout;->mRightHandle:Lcom/android/videoeditor/widgets/HandleView;

    const/4 v7, 0x0

    invoke-virtual {v6, v7}, Lcom/android/videoeditor/widgets/HandleView;->setEnabled(Z)V

    :goto_1
    move-object v3, v4

    check-cast v3, Lcom/android/videoeditor/service/MovieMediaItem;

    iget-object v6, p0, Lcom/android/videoeditor/widgets/MediaLinearLayout;->mMediaItemActionMode:Landroid/view/ActionMode;

    if-nez v6, :cond_2

    new-instance v6, Lcom/android/videoeditor/widgets/MediaLinearLayout$MediaItemActionModeCallback;

    invoke-direct {v6, p0, v3}, Lcom/android/videoeditor/widgets/MediaLinearLayout$MediaItemActionModeCallback;-><init>(Lcom/android/videoeditor/widgets/MediaLinearLayout;Lcom/android/videoeditor/service/MovieMediaItem;)V

    invoke-virtual {p0, v6}, Lcom/android/videoeditor/widgets/MediaLinearLayout;->startActionMode(Landroid/view/ActionMode$Callback;)Landroid/view/ActionMode;

    :cond_2
    invoke-virtual {v3}, Lcom/android/videoeditor/service/MovieMediaItem;->isVideoClip()Z

    move-result v5

    if-eqz v5, :cond_3

    iget-object v6, p0, Lcom/android/videoeditor/widgets/MediaLinearLayout;->mLeftHandle:Lcom/android/videoeditor/widgets/HandleView;

    const/4 v7, 0x0

    invoke-virtual {v6, v7}, Lcom/android/videoeditor/widgets/HandleView;->setVisibility(I)V

    iget-object v6, p0, Lcom/android/videoeditor/widgets/MediaLinearLayout;->mLeftHandle:Lcom/android/videoeditor/widgets/HandleView;

    invoke-virtual {v6}, Lcom/android/videoeditor/widgets/HandleView;->bringToFront()V

    iget-object v8, p0, Lcom/android/videoeditor/widgets/MediaLinearLayout;->mLeftHandle:Lcom/android/videoeditor/widgets/HandleView;

    invoke-virtual {v3}, Lcom/android/videoeditor/service/MovieMediaItem;->getAppBoundaryBeginTime()J

    move-result-wide v6

    const-wide/16 v9, 0x0

    cmp-long v6, v6, v9

    if-gtz v6, :cond_5

    const/4 v6, 0x1

    :goto_2
    invoke-virtual {v3}, Lcom/android/videoeditor/service/MovieMediaItem;->getAppTimelineDuration()J

    move-result-wide v9

    invoke-static {}, Lcom/android/videoeditor/util/MediaItemUtils;->getMinimumVideoItemDuration()J

    move-result-wide v11

    cmp-long v7, v9, v11

    if-gtz v7, :cond_6

    const/4 v7, 0x1

    :goto_3
    invoke-virtual {v8, v6, v7}, Lcom/android/videoeditor/widgets/HandleView;->setLimitReached(ZZ)V

    iget-object v6, p0, Lcom/android/videoeditor/widgets/MediaLinearLayout;->mLeftHandle:Lcom/android/videoeditor/widgets/HandleView;

    new-instance v7, Lcom/android/videoeditor/widgets/MediaLinearLayout$18;

    invoke-direct {v7, p0, v2, v5}, Lcom/android/videoeditor/widgets/MediaLinearLayout$18;-><init>(Lcom/android/videoeditor/widgets/MediaLinearLayout;Lcom/android/videoeditor/widgets/MediaItemView;Z)V

    invoke-virtual {v6, v7}, Lcom/android/videoeditor/widgets/HandleView;->setListener(Lcom/android/videoeditor/widgets/HandleView$MoveListener;)V

    :cond_3
    iget-object v6, p0, Lcom/android/videoeditor/widgets/MediaLinearLayout;->mRightHandle:Lcom/android/videoeditor/widgets/HandleView;

    const/4 v7, 0x0

    invoke-virtual {v6, v7}, Lcom/android/videoeditor/widgets/HandleView;->setVisibility(I)V

    iget-object v6, p0, Lcom/android/videoeditor/widgets/MediaLinearLayout;->mRightHandle:Lcom/android/videoeditor/widgets/HandleView;

    invoke-virtual {v6}, Lcom/android/videoeditor/widgets/HandleView;->bringToFront()V

    invoke-virtual {v3}, Lcom/android/videoeditor/service/MovieMediaItem;->getAppTimelineDuration()J

    move-result-wide v0

    iget-object v8, p0, Lcom/android/videoeditor/widgets/MediaLinearLayout;->mRightHandle:Lcom/android/videoeditor/widgets/HandleView;

    invoke-static {v3}, Lcom/android/videoeditor/util/MediaItemUtils;->getMinimumMediaItemDuration(Lcom/android/videoeditor/service/MovieMediaItem;)J

    move-result-wide v6

    cmp-long v6, v0, v6

    if-gtz v6, :cond_7

    const/4 v6, 0x1

    move v7, v6

    :goto_4
    if-eqz v5, :cond_9

    invoke-virtual {v3}, Lcom/android/videoeditor/service/MovieMediaItem;->getAppBoundaryEndTime()J

    move-result-wide v9

    invoke-virtual {v3}, Lcom/android/videoeditor/service/MovieMediaItem;->getDuration()J

    move-result-wide v11

    cmp-long v6, v9, v11

    if-ltz v6, :cond_8

    const/4 v6, 0x1

    :goto_5
    invoke-virtual {v8, v7, v6}, Lcom/android/videoeditor/widgets/HandleView;->setLimitReached(ZZ)V

    iget-object v6, p0, Lcom/android/videoeditor/widgets/MediaLinearLayout;->mRightHandle:Lcom/android/videoeditor/widgets/HandleView;

    new-instance v7, Lcom/android/videoeditor/widgets/MediaLinearLayout$19;

    invoke-direct {v7, p0, v2, v5}, Lcom/android/videoeditor/widgets/MediaLinearLayout$19;-><init>(Lcom/android/videoeditor/widgets/MediaLinearLayout;Lcom/android/videoeditor/widgets/MediaItemView;Z)V

    invoke-virtual {v6, v7}, Lcom/android/videoeditor/widgets/HandleView;->setListener(Lcom/android/videoeditor/widgets/HandleView$MoveListener;)V

    goto/16 :goto_0

    :cond_4
    iget-object v6, p0, Lcom/android/videoeditor/widgets/MediaLinearLayout;->mLeftHandle:Lcom/android/videoeditor/widgets/HandleView;

    const/4 v7, 0x1

    invoke-virtual {v6, v7}, Lcom/android/videoeditor/widgets/HandleView;->setEnabled(Z)V

    iget-object v6, p0, Lcom/android/videoeditor/widgets/MediaLinearLayout;->mRightHandle:Lcom/android/videoeditor/widgets/HandleView;

    const/4 v7, 0x1

    invoke-virtual {v6, v7}, Lcom/android/videoeditor/widgets/HandleView;->setEnabled(Z)V

    goto/16 :goto_1

    :cond_5
    const/4 v6, 0x0

    goto :goto_2

    :cond_6
    const/4 v7, 0x0

    goto :goto_3

    :cond_7
    const/4 v6, 0x0

    move v7, v6

    goto :goto_4

    :cond_8
    const/4 v6, 0x0

    goto :goto_5

    :cond_9
    const-wide/16 v9, 0x1770

    cmp-long v6, v0, v9

    if-ltz v6, :cond_a

    const/4 v6, 0x1

    goto :goto_5

    :cond_a
    const/4 v6, 0x0

    goto :goto_5

    :cond_b
    instance-of v6, v4, Lcom/android/videoeditor/service/MovieTransition;

    if-eqz v6, :cond_0

    iget-object v6, p0, Lcom/android/videoeditor/widgets/MediaLinearLayout;->mTransitionActionMode:Landroid/view/ActionMode;

    if-nez v6, :cond_0

    new-instance v6, Lcom/android/videoeditor/widgets/MediaLinearLayout$TransitionActionModeCallback;

    check-cast v4, Lcom/android/videoeditor/service/MovieTransition;

    invoke-direct {v6, p0, v4}, Lcom/android/videoeditor/widgets/MediaLinearLayout$TransitionActionModeCallback;-><init>(Lcom/android/videoeditor/widgets/MediaLinearLayout;Lcom/android/videoeditor/service/MovieTransition;)V

    invoke-virtual {p0, v6}, Lcom/android/videoeditor/widgets/MediaLinearLayout;->startActionMode(Landroid/view/ActionMode$Callback;)Landroid/view/ActionMode;

    goto/16 :goto_0
.end method

.method private setIsTrimming(Z)V
    .locals 0
    .param p1    # Z

    iput-boolean p1, p0, Lcom/android/videoeditor/widgets/MediaLinearLayout;->mIsTrimming:Z

    return-void
.end method

.method private setPlaybackState(Z)V
    .locals 5
    .param p1    # Z

    invoke-virtual {p0}, Lcom/android/videoeditor/widgets/MediaLinearLayout;->getChildCount()I

    move-result v1

    const/4 v2, 0x0

    :goto_0
    if-ge v2, v1, :cond_2

    invoke-virtual {p0, v2}, Lcom/android/videoeditor/widgets/MediaLinearLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v3

    if-eqz v3, :cond_0

    instance-of v4, v3, Lcom/android/videoeditor/service/MovieMediaItem;

    if-eqz v4, :cond_1

    check-cast v0, Lcom/android/videoeditor/widgets/MediaItemView;

    invoke-virtual {v0, p1}, Lcom/android/videoeditor/widgets/MediaItemView;->setPlaybackMode(Z)V

    :cond_0
    :goto_1
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    :cond_1
    instance-of v4, v3, Lcom/android/videoeditor/service/MovieTransition;

    if-eqz v4, :cond_0

    check-cast v0, Lcom/android/videoeditor/widgets/TransitionView;

    invoke-virtual {v0, p1}, Lcom/android/videoeditor/widgets/TransitionView;->setPlaybackMode(Z)V

    goto :goto_1

    :cond_2
    return-void
.end method

.method private showAddMediaItemButtons(Z)V
    .locals 3
    .param p1    # Z

    const/16 v2, 0x8

    const/4 v1, 0x0

    if-eqz p1, :cond_1

    iget-object v0, p0, Lcom/android/videoeditor/widgets/MediaLinearLayout;->mProject:Lcom/android/videoeditor/service/VideoEditorProject;

    invoke-virtual {v0}, Lcom/android/videoeditor/service/VideoEditorProject;->getMediaItemCount()I

    move-result v0

    if-lez v0, :cond_0

    iget-object v0, p0, Lcom/android/videoeditor/widgets/MediaLinearLayout;->mLeftAddClipButton:Landroid/widget/ImageButton;

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setVisibility(I)V

    :cond_0
    iget-object v0, p0, Lcom/android/videoeditor/widgets/MediaLinearLayout;->mRightAddClipButton:Landroid/widget/ImageButton;

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setVisibility(I)V

    :goto_0
    return-void

    :cond_1
    iget-object v0, p0, Lcom/android/videoeditor/widgets/MediaLinearLayout;->mLeftAddClipButton:Landroid/widget/ImageButton;

    invoke-virtual {v0, v2}, Landroid/widget/ImageButton;->setVisibility(I)V

    iget-object v0, p0, Lcom/android/videoeditor/widgets/MediaLinearLayout;->mRightAddClipButton:Landroid/widget/ImageButton;

    invoke-virtual {v0, v2}, Landroid/widget/ImageButton;->setVisibility(I)V

    goto :goto_0
.end method

.method private unSelect(Landroid/view/View;)V
    .locals 1
    .param p1    # Landroid/view/View;

    if-eqz p1, :cond_0

    invoke-virtual {p1}, Landroid/view/View;->isSelected()Z

    move-result v0

    if-nez v0, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/android/videoeditor/widgets/MediaLinearLayout;->mSelectedView:Landroid/view/View;

    const/4 v0, 0x0

    invoke-virtual {p1, v0}, Landroid/view/View;->setSelected(Z)V

    invoke-direct {p0}, Lcom/android/videoeditor/widgets/MediaLinearLayout;->invalidateAllChildren()V

    invoke-direct {p0}, Lcom/android/videoeditor/widgets/MediaLinearLayout;->clearAndHideTrimHandles()V

    goto :goto_0
.end method

.method private unselectAllTimelineViews()V
    .locals 2

    invoke-virtual {p0}, Lcom/android/videoeditor/widgets/MediaLinearLayout;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    check-cast v0, Landroid/widget/RelativeLayout;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->setSelected(Z)V

    invoke-direct {p0}, Lcom/android/videoeditor/widgets/MediaLinearLayout;->invalidateAllChildren()V

    return-void
.end method


# virtual methods
.method public addEffect(ILjava/lang/String;Landroid/graphics/Rect;Landroid/graphics/Rect;)V
    .locals 11
    .param p1    # I
    .param p2    # Ljava/lang/String;
    .param p3    # Landroid/graphics/Rect;
    .param p4    # Landroid/graphics/Rect;

    const/4 v9, 0x0

    const-wide/16 v4, 0x0

    iget-object v0, p0, Lcom/android/videoeditor/widgets/MediaLinearLayout;->mProject:Lcom/android/videoeditor/service/VideoEditorProject;

    invoke-virtual {v0, p2}, Lcom/android/videoeditor/service/VideoEditorProject;->getMediaItem(Ljava/lang/String;)Lcom/android/videoeditor/service/MovieMediaItem;

    move-result-object v10

    if-nez v10, :cond_1

    const-string v0, "MediaLinearLayout"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "addEffect media item not found: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    :goto_0
    return-void

    :cond_1
    invoke-static {}, Lcom/android/videoeditor/service/ApiService;->generateId()Ljava/lang/String;

    move-result-object v3

    packed-switch p1, :pswitch_data_0

    :goto_1
    iget-object v0, p0, Lcom/android/videoeditor/widgets/MediaLinearLayout;->mMediaItemActionMode:Landroid/view/ActionMode;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/videoeditor/widgets/MediaLinearLayout;->mMediaItemActionMode:Landroid/view/ActionMode;

    invoke-virtual {v0}, Landroid/view/ActionMode;->invalidate()V

    goto :goto_0

    :pswitch_0
    invoke-virtual {p0}, Lcom/android/videoeditor/widgets/MediaLinearLayout;->getContext()Landroid/content/Context;

    move-result-object v0

    iget-object v1, p0, Lcom/android/videoeditor/widgets/MediaLinearLayout;->mProject:Lcom/android/videoeditor/service/VideoEditorProject;

    invoke-virtual {v1}, Lcom/android/videoeditor/service/VideoEditorProject;->getPath()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v10}, Lcom/android/videoeditor/service/MovieMediaItem;->getDuration()J

    move-result-wide v6

    move-object v2, p2

    move-object v8, p3

    move-object v9, p4

    invoke-static/range {v0 .. v9}, Lcom/android/videoeditor/service/ApiService;->addEffectKenBurns(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;JJLandroid/graphics/Rect;Landroid/graphics/Rect;)V

    goto :goto_1

    :pswitch_1
    invoke-virtual {p0}, Lcom/android/videoeditor/widgets/MediaLinearLayout;->getContext()Landroid/content/Context;

    move-result-object v0

    iget-object v1, p0, Lcom/android/videoeditor/widgets/MediaLinearLayout;->mProject:Lcom/android/videoeditor/service/VideoEditorProject;

    invoke-virtual {v1}, Lcom/android/videoeditor/service/VideoEditorProject;->getPath()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v10}, Lcom/android/videoeditor/service/MovieMediaItem;->getDuration()J

    move-result-wide v6

    const/4 v8, 0x2

    const v9, 0x7f7f7f

    move-object v2, p2

    invoke-static/range {v0 .. v9}, Lcom/android/videoeditor/service/ApiService;->addEffectColor(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;JJII)V

    goto :goto_1

    :pswitch_2
    invoke-virtual {p0}, Lcom/android/videoeditor/widgets/MediaLinearLayout;->getContext()Landroid/content/Context;

    move-result-object v0

    iget-object v1, p0, Lcom/android/videoeditor/widgets/MediaLinearLayout;->mProject:Lcom/android/videoeditor/service/VideoEditorProject;

    invoke-virtual {v1}, Lcom/android/videoeditor/service/VideoEditorProject;->getPath()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v10}, Lcom/android/videoeditor/service/MovieMediaItem;->getDuration()J

    move-result-wide v6

    const/4 v8, 0x3

    move-object v2, p2

    invoke-static/range {v0 .. v9}, Lcom/android/videoeditor/service/ApiService;->addEffectColor(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;JJII)V

    goto :goto_1

    :pswitch_3
    invoke-virtual {p0}, Lcom/android/videoeditor/widgets/MediaLinearLayout;->getContext()Landroid/content/Context;

    move-result-object v0

    iget-object v1, p0, Lcom/android/videoeditor/widgets/MediaLinearLayout;->mProject:Lcom/android/videoeditor/service/VideoEditorProject;

    invoke-virtual {v1}, Lcom/android/videoeditor/service/VideoEditorProject;->getPath()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v10}, Lcom/android/videoeditor/service/MovieMediaItem;->getDuration()J

    move-result-wide v6

    const/4 v8, 0x4

    move-object v2, p2

    invoke-static/range {v0 .. v9}, Lcom/android/videoeditor/service/ApiService;->addEffectColor(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;JJII)V

    goto :goto_1

    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method public addMediaItems(Ljava/util/List;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/android/videoeditor/service/MovieMediaItem;",
            ">;)V"
        }
    .end annotation

    invoke-direct {p0}, Lcom/android/videoeditor/widgets/MediaLinearLayout;->closeActionBars()V

    invoke-direct {p0}, Lcom/android/videoeditor/widgets/MediaLinearLayout;->removeAllMediaItemAndTransitionViews()V

    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/android/videoeditor/service/MovieMediaItem;

    invoke-direct {p0, v1}, Lcom/android/videoeditor/widgets/MediaLinearLayout;->addMediaItem(Lcom/android/videoeditor/service/MovieMediaItem;)V

    goto :goto_0

    :cond_0
    return-void
.end method

.method public addTransition(Lcom/android/videoeditor/service/MovieTransition;Ljava/lang/String;)Landroid/view/View;
    .locals 10
    .param p1    # Lcom/android/videoeditor/service/MovieTransition;
    .param p2    # Ljava/lang/String;

    const/4 v6, 0x0

    if-eqz p2, :cond_3

    const/4 v2, -0x1

    invoke-virtual {p0}, Lcom/android/videoeditor/widgets/MediaLinearLayout;->getChildCount()I

    move-result v0

    const/4 v1, 0x0

    :goto_0
    if-ge v1, v0, :cond_0

    invoke-virtual {p0, v1}, Lcom/android/videoeditor/widgets/MediaLinearLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v7

    invoke-virtual {v7}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v5

    if-eqz v5, :cond_2

    instance-of v7, v5, Lcom/android/videoeditor/service/MovieMediaItem;

    if-eqz v7, :cond_2

    move-object v4, v5

    check-cast v4, Lcom/android/videoeditor/service/MovieMediaItem;

    invoke-virtual {v4}, Lcom/android/videoeditor/service/MovieMediaItem;->getId()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {p2, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_2

    add-int/lit8 v2, v1, 0x1

    :cond_0
    if-gez v2, :cond_4

    const-string v7, "MediaLinearLayout"

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "addTransition media item not found: "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    :cond_1
    :goto_1
    return-object v6

    :cond_2
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_3
    const/4 v2, 0x1

    :cond_4
    invoke-virtual {p0}, Lcom/android/videoeditor/widgets/MediaLinearLayout;->getContext()Landroid/content/Context;

    move-result-object v7

    const v8, 0x7f040017

    invoke-static {v7, v8, v6}, Lcom/android/videoeditor/widgets/MediaLinearLayout;->inflate(Landroid/content/Context;ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v6

    move-object v7, v6

    check-cast v7, Lcom/android/videoeditor/widgets/TransitionView;

    iget-object v8, p0, Lcom/android/videoeditor/widgets/MediaLinearLayout;->mTransitionGestureListener:Lcom/android/videoeditor/widgets/ItemSimpleGestureListener;

    invoke-virtual {v7, v8}, Lcom/android/videoeditor/widgets/TransitionView;->setGestureListener(Lcom/android/videoeditor/widgets/ItemSimpleGestureListener;)V

    move-object v7, v6

    check-cast v7, Lcom/android/videoeditor/widgets/TransitionView;

    iget-object v8, p0, Lcom/android/videoeditor/widgets/MediaLinearLayout;->mProject:Lcom/android/videoeditor/service/VideoEditorProject;

    invoke-virtual {v8}, Lcom/android/videoeditor/service/VideoEditorProject;->getPath()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Lcom/android/videoeditor/widgets/TransitionView;->setProjectPath(Ljava/lang/String;)V

    invoke-virtual {v6, p1}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    new-instance v3, Landroid/widget/LinearLayout$LayoutParams;

    const/4 v7, -0x2

    const/4 v8, -0x1

    invoke-direct {v3, v7, v8}, Landroid/widget/LinearLayout$LayoutParams;-><init>(II)V

    invoke-virtual {p0, v6, v2, v3}, Lcom/android/videoeditor/widgets/MediaLinearLayout;->addView(Landroid/view/View;ILandroid/view/ViewGroup$LayoutParams;)V

    invoke-virtual {p0}, Lcom/android/videoeditor/widgets/MediaLinearLayout;->requestLayout()V

    iget-object v7, p0, Lcom/android/videoeditor/widgets/MediaLinearLayout;->mMediaItemActionMode:Landroid/view/ActionMode;

    if-eqz v7, :cond_1

    iget-object v7, p0, Lcom/android/videoeditor/widgets/MediaLinearLayout;->mMediaItemActionMode:Landroid/view/ActionMode;

    invoke-virtual {v7}, Landroid/view/ActionMode;->invalidate()V

    goto :goto_1
.end method

.method public addTransition(Ljava/lang/String;IJ)V
    .locals 11
    .param p1    # Ljava/lang/String;
    .param p2    # I
    .param p3    # J

    const/16 v8, 0x32

    const/4 v9, 0x0

    const/4 v6, 0x2

    invoke-direct {p0}, Lcom/android/videoeditor/widgets/MediaLinearLayout;->unselectAllTimelineViews()V

    if-eqz p1, :cond_1

    iget-object v0, p0, Lcom/android/videoeditor/widgets/MediaLinearLayout;->mProject:Lcom/android/videoeditor/service/VideoEditorProject;

    invoke-virtual {v0, p1}, Lcom/android/videoeditor/service/VideoEditorProject;->getMediaItem(Ljava/lang/String;)Lcom/android/videoeditor/service/MovieMediaItem;

    move-result-object v10

    if-nez v10, :cond_2

    :cond_0
    :goto_0
    return-void

    :cond_1
    const/4 v10, 0x0

    :cond_2
    invoke-static {}, Lcom/android/videoeditor/service/ApiService;->generateId()Ljava/lang/String;

    move-result-object v3

    packed-switch p2, :pswitch_data_0

    :goto_1
    iget-object v0, p0, Lcom/android/videoeditor/widgets/MediaLinearLayout;->mMediaItemActionMode:Landroid/view/ActionMode;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/videoeditor/widgets/MediaLinearLayout;->mMediaItemActionMode:Landroid/view/ActionMode;

    invoke-virtual {v0}, Landroid/view/ActionMode;->invalidate()V

    goto :goto_0

    :pswitch_0
    invoke-virtual {p0}, Lcom/android/videoeditor/widgets/MediaLinearLayout;->getContext()Landroid/content/Context;

    move-result-object v0

    iget-object v1, p0, Lcom/android/videoeditor/widgets/MediaLinearLayout;->mProject:Lcom/android/videoeditor/service/VideoEditorProject;

    invoke-virtual {v1}, Lcom/android/videoeditor/service/VideoEditorProject;->getPath()Ljava/lang/String;

    move-result-object v1

    const/high16 v7, 0x7f050000

    move-object v2, p1

    move-wide v4, p3

    invoke-static/range {v0 .. v9}, Lcom/android/videoeditor/service/ApiService;->insertAlphaTransition(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;JIIIZ)V

    goto :goto_1

    :pswitch_1
    invoke-virtual {p0}, Lcom/android/videoeditor/widgets/MediaLinearLayout;->getContext()Landroid/content/Context;

    move-result-object v0

    iget-object v1, p0, Lcom/android/videoeditor/widgets/MediaLinearLayout;->mProject:Lcom/android/videoeditor/service/VideoEditorProject;

    invoke-virtual {v1}, Lcom/android/videoeditor/service/VideoEditorProject;->getPath()Ljava/lang/String;

    move-result-object v1

    const v7, 0x7f050001

    move-object v2, p1

    move-wide v4, p3

    invoke-static/range {v0 .. v9}, Lcom/android/videoeditor/service/ApiService;->insertAlphaTransition(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;JIIIZ)V

    goto :goto_1

    :pswitch_2
    invoke-virtual {p0}, Lcom/android/videoeditor/widgets/MediaLinearLayout;->getContext()Landroid/content/Context;

    move-result-object v0

    iget-object v1, p0, Lcom/android/videoeditor/widgets/MediaLinearLayout;->mProject:Lcom/android/videoeditor/service/VideoEditorProject;

    invoke-virtual {v1}, Lcom/android/videoeditor/service/VideoEditorProject;->getPath()Ljava/lang/String;

    move-result-object v1

    move-object v2, p1

    move-wide v4, p3

    invoke-static/range {v0 .. v6}, Lcom/android/videoeditor/service/ApiService;->insertCrossfadeTransition(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;JI)V

    goto :goto_1

    :pswitch_3
    invoke-virtual {p0}, Lcom/android/videoeditor/widgets/MediaLinearLayout;->getContext()Landroid/content/Context;

    move-result-object v0

    iget-object v1, p0, Lcom/android/videoeditor/widgets/MediaLinearLayout;->mProject:Lcom/android/videoeditor/service/VideoEditorProject;

    invoke-virtual {v1}, Lcom/android/videoeditor/service/VideoEditorProject;->getPath()Ljava/lang/String;

    move-result-object v1

    move-object v2, p1

    move-wide v4, p3

    invoke-static/range {v0 .. v6}, Lcom/android/videoeditor/service/ApiService;->insertFadeBlackTransition(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;JI)V

    goto :goto_1

    :pswitch_4
    invoke-virtual {p0}, Lcom/android/videoeditor/widgets/MediaLinearLayout;->getContext()Landroid/content/Context;

    move-result-object v0

    iget-object v1, p0, Lcom/android/videoeditor/widgets/MediaLinearLayout;->mProject:Lcom/android/videoeditor/service/VideoEditorProject;

    invoke-virtual {v1}, Lcom/android/videoeditor/service/VideoEditorProject;->getPath()Ljava/lang/String;

    move-result-object v1

    move-object v2, p1

    move-wide v4, p3

    move v7, v9

    invoke-static/range {v0 .. v7}, Lcom/android/videoeditor/service/ApiService;->insertSlidingTransition(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;JII)V

    goto :goto_1

    :pswitch_5
    invoke-virtual {p0}, Lcom/android/videoeditor/widgets/MediaLinearLayout;->getContext()Landroid/content/Context;

    move-result-object v0

    iget-object v1, p0, Lcom/android/videoeditor/widgets/MediaLinearLayout;->mProject:Lcom/android/videoeditor/service/VideoEditorProject;

    invoke-virtual {v1}, Lcom/android/videoeditor/service/VideoEditorProject;->getPath()Ljava/lang/String;

    move-result-object v1

    const/4 v7, 0x1

    move-object v2, p1

    move-wide v4, p3

    invoke-static/range {v0 .. v7}, Lcom/android/videoeditor/service/ApiService;->insertSlidingTransition(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;JII)V

    goto :goto_1

    :pswitch_6
    invoke-virtual {p0}, Lcom/android/videoeditor/widgets/MediaLinearLayout;->getContext()Landroid/content/Context;

    move-result-object v0

    iget-object v1, p0, Lcom/android/videoeditor/widgets/MediaLinearLayout;->mProject:Lcom/android/videoeditor/service/VideoEditorProject;

    invoke-virtual {v1}, Lcom/android/videoeditor/service/VideoEditorProject;->getPath()Ljava/lang/String;

    move-result-object v1

    move-object v2, p1

    move-wide v4, p3

    move v7, v6

    invoke-static/range {v0 .. v7}, Lcom/android/videoeditor/service/ApiService;->insertSlidingTransition(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;JII)V

    goto/16 :goto_1

    :pswitch_7
    invoke-virtual {p0}, Lcom/android/videoeditor/widgets/MediaLinearLayout;->getContext()Landroid/content/Context;

    move-result-object v0

    iget-object v1, p0, Lcom/android/videoeditor/widgets/MediaLinearLayout;->mProject:Lcom/android/videoeditor/service/VideoEditorProject;

    invoke-virtual {v1}, Lcom/android/videoeditor/service/VideoEditorProject;->getPath()Ljava/lang/String;

    move-result-object v1

    const/4 v7, 0x3

    move-object v2, p1

    move-wide v4, p3

    invoke-static/range {v0 .. v7}, Lcom/android/videoeditor/service/ApiService;->insertSlidingTransition(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;JII)V

    goto/16 :goto_1

    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_7
    .end packed-switch
.end method

.method public editTransition(Ljava/lang/String;Ljava/lang/String;IJ)V
    .locals 3
    .param p1    # Ljava/lang/String;
    .param p2    # Ljava/lang/String;
    .param p3    # I
    .param p4    # J

    iget-object v1, p0, Lcom/android/videoeditor/widgets/MediaLinearLayout;->mProject:Lcom/android/videoeditor/service/VideoEditorProject;

    invoke-virtual {v1, p2}, Lcom/android/videoeditor/service/VideoEditorProject;->getTransition(Ljava/lang/String;)Lcom/android/videoeditor/service/MovieTransition;

    move-result-object v0

    if-nez v0, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    invoke-virtual {v0}, Lcom/android/videoeditor/service/MovieTransition;->getType()I

    move-result v1

    if-eq v1, p3, :cond_3

    invoke-virtual {p0}, Lcom/android/videoeditor/widgets/MediaLinearLayout;->getContext()Landroid/content/Context;

    move-result-object v1

    iget-object v2, p0, Lcom/android/videoeditor/widgets/MediaLinearLayout;->mProject:Lcom/android/videoeditor/service/VideoEditorProject;

    invoke-virtual {v2}, Lcom/android/videoeditor/service/VideoEditorProject;->getPath()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2, p2}, Lcom/android/videoeditor/service/ApiService;->removeTransition(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {p0, p1, p3, p4, p5}, Lcom/android/videoeditor/widgets/MediaLinearLayout;->addTransition(Ljava/lang/String;IJ)V

    :cond_2
    :goto_1
    iget-object v1, p0, Lcom/android/videoeditor/widgets/MediaLinearLayout;->mMediaItemActionMode:Landroid/view/ActionMode;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/android/videoeditor/widgets/MediaLinearLayout;->mMediaItemActionMode:Landroid/view/ActionMode;

    invoke-virtual {v1}, Landroid/view/ActionMode;->invalidate()V

    goto :goto_0

    :cond_3
    invoke-virtual {v0}, Lcom/android/videoeditor/service/MovieTransition;->getAppDuration()J

    move-result-wide v1

    cmp-long v1, v1, p4

    if-eqz v1, :cond_2

    invoke-virtual {v0, p4, p5}, Lcom/android/videoeditor/service/MovieTransition;->setAppDuration(J)V

    invoke-virtual {p0}, Lcom/android/videoeditor/widgets/MediaLinearLayout;->getContext()Landroid/content/Context;

    move-result-object v1

    iget-object v2, p0, Lcom/android/videoeditor/widgets/MediaLinearLayout;->mProject:Lcom/android/videoeditor/service/VideoEditorProject;

    invoke-virtual {v2}, Lcom/android/videoeditor/service/VideoEditorProject;->getPath()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2, p2, p4, p5}, Lcom/android/videoeditor/service/ApiService;->setTransitionDuration(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;J)V

    goto :goto_1
.end method

.method public getSelectedViewPos()I
    .locals 1

    iget-object v0, p0, Lcom/android/videoeditor/widgets/MediaLinearLayout;->mSelectedView:Landroid/view/View;

    invoke-virtual {p0, v0}, Lcom/android/videoeditor/widgets/MediaLinearLayout;->indexOfChild(Landroid/view/View;)I

    move-result v0

    return v0
.end method

.method public hasItemSelected()Z
    .locals 1

    iget-object v0, p0, Lcom/android/videoeditor/widgets/MediaLinearLayout;->mSelectedView:Landroid/view/View;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public insertMediaItem(Lcom/android/videoeditor/service/MovieMediaItem;Ljava/lang/String;)V
    .locals 13
    .param p1    # Lcom/android/videoeditor/service/MovieMediaItem;
    .param p2    # Ljava/lang/String;

    const/4 v12, 0x0

    const/4 v11, -0x1

    const/4 v10, 0x1

    invoke-virtual {p0}, Lcom/android/videoeditor/widgets/MediaLinearLayout;->getContext()Landroid/content/Context;

    move-result-object v8

    const v9, 0x7f04000e

    invoke-static {v8, v9, v12}, Lcom/android/videoeditor/widgets/MediaLinearLayout;->inflate(Landroid/content/Context;ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v4

    move-object v8, v4

    check-cast v8, Lcom/android/videoeditor/widgets/MediaItemView;

    iget-object v9, p0, Lcom/android/videoeditor/widgets/MediaLinearLayout;->mMediaItemGestureListener:Lcom/android/videoeditor/widgets/ItemSimpleGestureListener;

    invoke-virtual {v8, v9}, Lcom/android/videoeditor/widgets/MediaItemView;->setGestureListener(Lcom/android/videoeditor/widgets/ItemSimpleGestureListener;)V

    move-object v8, v4

    check-cast v8, Lcom/android/videoeditor/widgets/MediaItemView;

    iget-object v9, p0, Lcom/android/videoeditor/widgets/MediaLinearLayout;->mProject:Lcom/android/videoeditor/service/VideoEditorProject;

    invoke-virtual {v9}, Lcom/android/videoeditor/service/VideoEditorProject;->getPath()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Lcom/android/videoeditor/widgets/MediaItemView;->setProjectPath(Ljava/lang/String;)V

    invoke-virtual {v4, p1}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    if-eqz p2, :cond_5

    invoke-direct {p0, p2}, Lcom/android/videoeditor/widgets/MediaLinearLayout;->getMediaItemViewIndex(Ljava/lang/String;)I

    move-result v2

    if-ne v2, v11, :cond_0

    const-string v8, "MediaLinearLayout"

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "Media item not found: "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v8, v9}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    :goto_0
    return-void

    :cond_0
    add-int/lit8 v2, v2, 0x1

    invoke-virtual {p0}, Lcom/android/videoeditor/widgets/MediaLinearLayout;->getChildCount()I

    move-result v8

    if-ge v2, v8, :cond_1

    invoke-virtual {p0, v2}, Lcom/android/videoeditor/widgets/MediaLinearLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v8

    invoke-virtual {v8}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v6

    if-eqz v6, :cond_1

    instance-of v8, v6, Lcom/android/videoeditor/service/MovieTransition;

    if-eqz v8, :cond_1

    invoke-virtual {p0, v2}, Lcom/android/videoeditor/widgets/MediaLinearLayout;->removeViewAt(I)V

    :cond_1
    :goto_1
    new-instance v3, Landroid/widget/LinearLayout$LayoutParams;

    const/4 v8, -0x2

    invoke-direct {v3, v8, v11}, Landroid/widget/LinearLayout$LayoutParams;-><init>(II)V

    invoke-virtual {p0, v4, v2, v3}, Lcom/android/videoeditor/widgets/MediaLinearLayout;->addView(Landroid/view/View;ILandroid/view/ViewGroup$LayoutParams;)V

    invoke-virtual {p1}, Lcom/android/videoeditor/service/MovieMediaItem;->getBeginTransition()Lcom/android/videoeditor/service/MovieTransition;

    move-result-object v0

    if-eqz v0, :cond_2

    if-le v2, v10, :cond_7

    add-int/lit8 v8, v2, -0x1

    invoke-virtual {p0, v8}, Lcom/android/videoeditor/widgets/MediaLinearLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v7

    invoke-virtual {v7}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v6

    if-eqz v6, :cond_2

    instance-of v8, v6, Lcom/android/videoeditor/service/MovieMediaItem;

    if-eqz v8, :cond_2

    move-object v5, v6

    check-cast v5, Lcom/android/videoeditor/service/MovieMediaItem;

    invoke-virtual {v5}, Lcom/android/videoeditor/service/MovieMediaItem;->getId()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {p0, v0, v8}, Lcom/android/videoeditor/widgets/MediaLinearLayout;->addTransition(Lcom/android/videoeditor/service/MovieTransition;Ljava/lang/String;)Landroid/view/View;

    :cond_2
    :goto_2
    invoke-virtual {p1}, Lcom/android/videoeditor/service/MovieMediaItem;->getEndTransition()Lcom/android/videoeditor/service/MovieTransition;

    move-result-object v1

    if-eqz v1, :cond_3

    invoke-virtual {p1}, Lcom/android/videoeditor/service/MovieMediaItem;->getId()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {p0, v1, v8}, Lcom/android/videoeditor/widgets/MediaLinearLayout;->addTransition(Lcom/android/videoeditor/service/MovieTransition;Ljava/lang/String;)Landroid/view/View;

    :cond_3
    invoke-virtual {p0}, Lcom/android/videoeditor/widgets/MediaLinearLayout;->requestLayout()V

    iget-object v8, p0, Lcom/android/videoeditor/widgets/MediaLinearLayout;->mMediaItemActionMode:Landroid/view/ActionMode;

    if-eqz v8, :cond_4

    iget-object v8, p0, Lcom/android/videoeditor/widgets/MediaLinearLayout;->mMediaItemActionMode:Landroid/view/ActionMode;

    invoke-virtual {v8}, Landroid/view/ActionMode;->invalidate()V

    :cond_4
    iget-object v8, p0, Lcom/android/videoeditor/widgets/MediaLinearLayout;->mLeftAddClipButton:Landroid/widget/ImageButton;

    const/4 v9, 0x0

    invoke-virtual {v8, v9}, Landroid/widget/ImageButton;->setVisibility(I)V

    goto :goto_0

    :cond_5
    invoke-virtual {p0, v10}, Lcom/android/videoeditor/widgets/MediaLinearLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v8

    invoke-virtual {v8}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v6

    if-eqz v6, :cond_6

    instance-of v8, v6, Lcom/android/videoeditor/service/MovieTransition;

    if-eqz v8, :cond_6

    invoke-virtual {p0, v10}, Lcom/android/videoeditor/widgets/MediaLinearLayout;->removeViewAt(I)V

    :cond_6
    const/4 v2, 0x1

    goto :goto_1

    :cond_7
    invoke-virtual {p0, v0, v12}, Lcom/android/videoeditor/widgets/MediaLinearLayout;->addTransition(Lcom/android/videoeditor/service/MovieTransition;Ljava/lang/String;)Landroid/view/View;

    goto :goto_2
.end method

.method public invalidateActionBar()V
    .locals 1

    iget-object v0, p0, Lcom/android/videoeditor/widgets/MediaLinearLayout;->mMediaItemActionMode:Landroid/view/ActionMode;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/videoeditor/widgets/MediaLinearLayout;->mMediaItemActionMode:Landroid/view/ActionMode;

    invoke-virtual {v0}, Landroid/view/ActionMode;->invalidate()V

    :cond_0
    iget-object v0, p0, Lcom/android/videoeditor/widgets/MediaLinearLayout;->mTransitionActionMode:Landroid/view/ActionMode;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/android/videoeditor/widgets/MediaLinearLayout;->mTransitionActionMode:Landroid/view/ActionMode;

    invoke-virtual {v0}, Landroid/view/ActionMode;->invalidate()V

    :cond_1
    return-void
.end method

.method public isTrimming()Z
    .locals 1

    iget-boolean v0, p0, Lcom/android/videoeditor/widgets/MediaLinearLayout;->mIsTrimming:Z

    return v0
.end method

.method public onCreateDialog(ILandroid/os/Bundle;)Landroid/app/Dialog;
    .locals 18
    .param p1    # I
    .param p2    # Landroid/os/Bundle;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/android/videoeditor/widgets/MediaLinearLayout;->mProject:Lcom/android/videoeditor/service/VideoEditorProject;

    if-nez v3, :cond_0

    const/4 v3, 0x0

    :goto_0
    return-object v3

    :cond_0
    packed-switch p1, :pswitch_data_0

    :pswitch_0
    const/4 v3, 0x0

    goto :goto_0

    :pswitch_1
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/android/videoeditor/widgets/MediaLinearLayout;->mProject:Lcom/android/videoeditor/service/VideoEditorProject;

    const-string v4, "media_item_id"

    move-object/from16 v0, p2

    invoke-virtual {v0, v4}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Lcom/android/videoeditor/service/VideoEditorProject;->getMediaItem(Ljava/lang/String;)Lcom/android/videoeditor/service/MovieMediaItem;

    move-result-object v15

    if-nez v15, :cond_1

    const/4 v3, 0x0

    goto :goto_0

    :cond_1
    invoke-virtual/range {p0 .. p0}, Lcom/android/videoeditor/widgets/MediaLinearLayout;->getContext()Landroid/content/Context;

    move-result-object v2

    check-cast v2, Landroid/app/Activity;

    invoke-virtual {v15}, Lcom/android/videoeditor/service/MovieMediaItem;->getFilename()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lcom/android/videoeditor/util/FileUtils;->getSimpleName(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x0

    invoke-virtual {v15}, Lcom/android/videoeditor/service/MovieMediaItem;->isVideoClip()Z

    move-result v5

    if-eqz v5, :cond_2

    const v5, 0x7f09001b

    invoke-virtual {v2, v5}, Landroid/app/Activity;->getString(I)Ljava/lang/String;

    move-result-object v5

    :goto_1
    const v6, 0x7f09008a

    invoke-virtual {v2, v6}, Landroid/app/Activity;->getString(I)Ljava/lang/String;

    move-result-object v6

    new-instance v7, Lcom/android/videoeditor/widgets/MediaLinearLayout$7;

    move-object/from16 v0, p0

    invoke-direct {v7, v0, v2, v15}, Lcom/android/videoeditor/widgets/MediaLinearLayout$7;-><init>(Lcom/android/videoeditor/widgets/MediaLinearLayout;Landroid/app/Activity;Lcom/android/videoeditor/service/MovieMediaItem;)V

    const v8, 0x7f09008b

    invoke-virtual {v2, v8}, Landroid/app/Activity;->getString(I)Ljava/lang/String;

    move-result-object v8

    new-instance v9, Lcom/android/videoeditor/widgets/MediaLinearLayout$8;

    move-object/from16 v0, p0

    invoke-direct {v9, v0, v2}, Lcom/android/videoeditor/widgets/MediaLinearLayout$8;-><init>(Lcom/android/videoeditor/widgets/MediaLinearLayout;Landroid/app/Activity;)V

    new-instance v10, Lcom/android/videoeditor/widgets/MediaLinearLayout$9;

    move-object/from16 v0, p0

    invoke-direct {v10, v0, v2}, Lcom/android/videoeditor/widgets/MediaLinearLayout$9;-><init>(Lcom/android/videoeditor/widgets/MediaLinearLayout;Landroid/app/Activity;)V

    const/4 v11, 0x1

    invoke-static/range {v2 .. v11}, Lcom/android/videoeditor/AlertDialogs;->createAlert(Landroid/content/Context;Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;Landroid/content/DialogInterface$OnClickListener;Ljava/lang/String;Landroid/content/DialogInterface$OnClickListener;Landroid/content/DialogInterface$OnCancelListener;Z)Landroid/app/AlertDialog;

    move-result-object v3

    goto :goto_0

    :cond_2
    const v5, 0x7f09001c

    invoke-virtual {v2, v5}, Landroid/app/Activity;->getString(I)Ljava/lang/String;

    move-result-object v5

    goto :goto_1

    :pswitch_2
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/android/videoeditor/widgets/MediaLinearLayout;->mProject:Lcom/android/videoeditor/service/VideoEditorProject;

    const-string v4, "media_item_id"

    move-object/from16 v0, p2

    invoke-virtual {v0, v4}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Lcom/android/videoeditor/service/VideoEditorProject;->getMediaItem(Ljava/lang/String;)Lcom/android/videoeditor/service/MovieMediaItem;

    move-result-object v15

    if-nez v15, :cond_3

    const/4 v3, 0x0

    goto :goto_0

    :cond_3
    invoke-virtual/range {p0 .. p0}, Lcom/android/videoeditor/widgets/MediaLinearLayout;->getContext()Landroid/content/Context;

    move-result-object v2

    check-cast v2, Landroid/app/Activity;

    new-instance v12, Landroid/app/AlertDialog$Builder;

    invoke-virtual/range {p0 .. p0}, Lcom/android/videoeditor/widgets/MediaLinearLayout;->getContext()Landroid/content/Context;

    move-result-object v3

    invoke-direct {v12, v3}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    const v3, 0x7f090018

    invoke-virtual {v2, v3}, Landroid/app/Activity;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v12, v3}, Landroid/app/AlertDialog$Builder;->setTitle(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    const/4 v3, 0x3

    new-array v0, v3, [Ljava/lang/CharSequence;

    move-object/from16 v16, v0

    const/4 v3, 0x0

    invoke-virtual/range {p0 .. p0}, Lcom/android/videoeditor/widgets/MediaLinearLayout;->getContext()Landroid/content/Context;

    move-result-object v4

    const v5, 0x7f090087

    invoke-virtual {v4, v5}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v4

    aput-object v4, v16, v3

    const/4 v3, 0x1

    invoke-virtual/range {p0 .. p0}, Lcom/android/videoeditor/widgets/MediaLinearLayout;->getContext()Landroid/content/Context;

    move-result-object v4

    const v5, 0x7f090088

    invoke-virtual {v4, v5}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v4

    aput-object v4, v16, v3

    const/4 v3, 0x2

    invoke-virtual/range {p0 .. p0}, Lcom/android/videoeditor/widgets/MediaLinearLayout;->getContext()Landroid/content/Context;

    move-result-object v4

    const v5, 0x7f090089

    invoke-virtual {v4, v5}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v4

    aput-object v4, v16, v3

    const-string v3, "rendering_mode"

    move-object/from16 v0, p2

    invoke-virtual {v0, v3}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v13

    packed-switch v13, :pswitch_data_1

    const/4 v14, 0x0

    :goto_2
    new-instance v3, Lcom/android/videoeditor/widgets/MediaLinearLayout$10;

    move-object/from16 v0, p0

    invoke-direct {v3, v0, v15, v2}, Lcom/android/videoeditor/widgets/MediaLinearLayout$10;-><init>(Lcom/android/videoeditor/widgets/MediaLinearLayout;Lcom/android/videoeditor/service/MovieMediaItem;Landroid/app/Activity;)V

    move-object/from16 v0, v16

    invoke-virtual {v12, v0, v14, v3}, Landroid/app/AlertDialog$Builder;->setSingleChoiceItems([Ljava/lang/CharSequence;ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    const/4 v3, 0x1

    invoke-virtual {v12, v3}, Landroid/app/AlertDialog$Builder;->setCancelable(Z)Landroid/app/AlertDialog$Builder;

    new-instance v3, Lcom/android/videoeditor/widgets/MediaLinearLayout$11;

    move-object/from16 v0, p0

    invoke-direct {v3, v0, v2}, Lcom/android/videoeditor/widgets/MediaLinearLayout$11;-><init>(Lcom/android/videoeditor/widgets/MediaLinearLayout;Landroid/app/Activity;)V

    invoke-virtual {v12, v3}, Landroid/app/AlertDialog$Builder;->setOnCancelListener(Landroid/content/DialogInterface$OnCancelListener;)Landroid/app/AlertDialog$Builder;

    invoke-virtual {v12}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v3

    goto/16 :goto_0

    :pswitch_3
    const/4 v14, 0x2

    goto :goto_2

    :pswitch_4
    const/4 v14, 0x1

    goto :goto_2

    :pswitch_5
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/android/videoeditor/widgets/MediaLinearLayout;->mProject:Lcom/android/videoeditor/service/VideoEditorProject;

    const-string v4, "transition_id"

    move-object/from16 v0, p2

    invoke-virtual {v0, v4}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Lcom/android/videoeditor/service/VideoEditorProject;->getTransition(Ljava/lang/String;)Lcom/android/videoeditor/service/MovieTransition;

    move-result-object v17

    if-nez v17, :cond_4

    const/4 v3, 0x0

    goto/16 :goto_0

    :cond_4
    invoke-virtual/range {p0 .. p0}, Lcom/android/videoeditor/widgets/MediaLinearLayout;->getContext()Landroid/content/Context;

    move-result-object v2

    check-cast v2, Landroid/app/Activity;

    const v3, 0x7f09008e

    invoke-virtual {v2, v3}, Landroid/app/Activity;->getString(I)Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x0

    const v5, 0x7f090019

    invoke-virtual {v2, v5}, Landroid/app/Activity;->getString(I)Ljava/lang/String;

    move-result-object v5

    const v6, 0x7f09008a

    invoke-virtual {v2, v6}, Landroid/app/Activity;->getString(I)Ljava/lang/String;

    move-result-object v6

    new-instance v7, Lcom/android/videoeditor/widgets/MediaLinearLayout$12;

    move-object/from16 v0, p0

    move-object/from16 v1, v17

    invoke-direct {v7, v0, v2, v1}, Lcom/android/videoeditor/widgets/MediaLinearLayout$12;-><init>(Lcom/android/videoeditor/widgets/MediaLinearLayout;Landroid/app/Activity;Lcom/android/videoeditor/service/MovieTransition;)V

    const v8, 0x7f09008b

    invoke-virtual {v2, v8}, Landroid/app/Activity;->getString(I)Ljava/lang/String;

    move-result-object v8

    new-instance v9, Lcom/android/videoeditor/widgets/MediaLinearLayout$13;

    move-object/from16 v0, p0

    invoke-direct {v9, v0, v2}, Lcom/android/videoeditor/widgets/MediaLinearLayout$13;-><init>(Lcom/android/videoeditor/widgets/MediaLinearLayout;Landroid/app/Activity;)V

    new-instance v10, Lcom/android/videoeditor/widgets/MediaLinearLayout$14;

    move-object/from16 v0, p0

    invoke-direct {v10, v0, v2}, Lcom/android/videoeditor/widgets/MediaLinearLayout$14;-><init>(Lcom/android/videoeditor/widgets/MediaLinearLayout;Landroid/app/Activity;)V

    const/4 v11, 0x1

    invoke-static/range {v2 .. v11}, Lcom/android/videoeditor/AlertDialogs;->createAlert(Landroid/content/Context;Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;Landroid/content/DialogInterface$OnClickListener;Ljava/lang/String;Landroid/content/DialogInterface$OnClickListener;Landroid/content/DialogInterface$OnCancelListener;Z)Landroid/app/AlertDialog;

    move-result-object v3

    goto/16 :goto_0

    :pswitch_6
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/android/videoeditor/widgets/MediaLinearLayout;->mProject:Lcom/android/videoeditor/service/VideoEditorProject;

    const-string v4, "media_item_id"

    move-object/from16 v0, p2

    invoke-virtual {v0, v4}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Lcom/android/videoeditor/service/VideoEditorProject;->getMediaItem(Ljava/lang/String;)Lcom/android/videoeditor/service/MovieMediaItem;

    move-result-object v15

    if-nez v15, :cond_5

    const/4 v3, 0x0

    goto/16 :goto_0

    :cond_5
    invoke-virtual/range {p0 .. p0}, Lcom/android/videoeditor/widgets/MediaLinearLayout;->getContext()Landroid/content/Context;

    move-result-object v2

    check-cast v2, Landroid/app/Activity;

    invoke-virtual {v15}, Lcom/android/videoeditor/service/MovieMediaItem;->getFilename()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lcom/android/videoeditor/util/FileUtils;->getSimpleName(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x0

    const v5, 0x7f09001a

    invoke-virtual {v2, v5}, Landroid/app/Activity;->getString(I)Ljava/lang/String;

    move-result-object v5

    const v6, 0x7f09008a

    invoke-virtual {v2, v6}, Landroid/app/Activity;->getString(I)Ljava/lang/String;

    move-result-object v6

    new-instance v7, Lcom/android/videoeditor/widgets/MediaLinearLayout$15;

    move-object/from16 v0, p0

    invoke-direct {v7, v0, v2, v15}, Lcom/android/videoeditor/widgets/MediaLinearLayout$15;-><init>(Lcom/android/videoeditor/widgets/MediaLinearLayout;Landroid/app/Activity;Lcom/android/videoeditor/service/MovieMediaItem;)V

    const v8, 0x7f09008b

    invoke-virtual {v2, v8}, Landroid/app/Activity;->getString(I)Ljava/lang/String;

    move-result-object v8

    new-instance v9, Lcom/android/videoeditor/widgets/MediaLinearLayout$16;

    move-object/from16 v0, p0

    invoke-direct {v9, v0, v2}, Lcom/android/videoeditor/widgets/MediaLinearLayout$16;-><init>(Lcom/android/videoeditor/widgets/MediaLinearLayout;Landroid/app/Activity;)V

    new-instance v10, Lcom/android/videoeditor/widgets/MediaLinearLayout$17;

    move-object/from16 v0, p0

    invoke-direct {v10, v0, v2}, Lcom/android/videoeditor/widgets/MediaLinearLayout$17;-><init>(Lcom/android/videoeditor/widgets/MediaLinearLayout;Landroid/app/Activity;)V

    const/4 v11, 0x1

    invoke-static/range {v2 .. v11}, Lcom/android/videoeditor/AlertDialogs;->createAlert(Landroid/content/Context;Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;Landroid/content/DialogInterface$OnClickListener;Ljava/lang/String;Landroid/content/DialogInterface$OnClickListener;Landroid/content/DialogInterface$OnCancelListener;Z)Landroid/app/AlertDialog;

    move-result-object v3

    goto/16 :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0xa
        :pswitch_1
        :pswitch_5
        :pswitch_2
        :pswitch_0
        :pswitch_6
    .end packed-switch

    :pswitch_data_1
    .packed-switch 0x1
        :pswitch_4
        :pswitch_3
    .end packed-switch
.end method

.method public onDragEvent(Landroid/view/DragEvent;)Z
    .locals 9
    .param p1    # Landroid/view/DragEvent;

    const/4 v8, 0x1

    const/4 v7, 0x0

    const/4 v3, 0x0

    const v6, 0x7f080002

    const/4 v5, 0x2

    const/4 v1, 0x0

    invoke-virtual {p1}, Landroid/view/DragEvent;->getAction()I

    move-result v2

    packed-switch v2, :pswitch_data_0

    const-string v2, "MediaLinearLayout"

    invoke-static {v2, v5}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v2

    if-eqz v2, :cond_0

    const-string v2, "MediaLinearLayout"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Other drag event: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    const/4 v1, 0x1

    :goto_0
    return v1

    :pswitch_0
    const-string v2, "MediaLinearLayout"

    invoke-static {v2, v5}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v2

    if-eqz v2, :cond_1

    const-string v2, "MediaLinearLayout"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "ACTION_DRAG_STARTED: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v2, v4}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    :cond_1
    invoke-virtual {p1}, Landroid/view/DragEvent;->getLocalState()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    iput-object v2, p0, Lcom/android/videoeditor/widgets/MediaLinearLayout;->mDragMediaItemId:Ljava/lang/String;

    iget-object v2, p0, Lcom/android/videoeditor/widgets/MediaLinearLayout;->mLeftHandle:Lcom/android/videoeditor/widgets/HandleView;

    const/16 v4, 0x8

    invoke-virtual {v2, v4}, Lcom/android/videoeditor/widgets/HandleView;->setVisibility(I)V

    iget-object v2, p0, Lcom/android/videoeditor/widgets/MediaLinearLayout;->mRightHandle:Lcom/android/videoeditor/widgets/HandleView;

    const/16 v4, 0x8

    invoke-virtual {v2, v4}, Lcom/android/videoeditor/widgets/HandleView;->setVisibility(I)V

    iput-object v3, p0, Lcom/android/videoeditor/widgets/MediaLinearLayout;->mDropAfterMediaItem:Lcom/android/videoeditor/service/MovieMediaItem;

    const/4 v2, -0x1

    iput v2, p0, Lcom/android/videoeditor/widgets/MediaLinearLayout;->mDropIndex:I

    iput-boolean v8, p0, Lcom/android/videoeditor/widgets/MediaLinearLayout;->mFirstEntered:Z

    const/4 v1, 0x1

    goto :goto_0

    :pswitch_1
    const-string v2, "MediaLinearLayout"

    invoke-static {v2, v5}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v2

    if-eqz v2, :cond_2

    const-string v2, "MediaLinearLayout"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "ACTION_DRAG_ENTERED: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    :cond_2
    iget-boolean v2, p0, Lcom/android/videoeditor/widgets/MediaLinearLayout;->mFirstEntered:Z

    if-nez v2, :cond_3

    iget v2, p0, Lcom/android/videoeditor/widgets/MediaLinearLayout;->mDropIndex:I

    if-ltz v2, :cond_3

    iget-object v2, p0, Lcom/android/videoeditor/widgets/MediaLinearLayout;->mScrollView:Landroid/view/View;

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v2, v6, v3}, Landroid/view/View;->setTag(ILjava/lang/Object;)V

    :goto_1
    iget-object v2, p0, Lcom/android/videoeditor/widgets/MediaLinearLayout;->mScrollView:Landroid/view/View;

    invoke-virtual {v2}, Landroid/view/View;->invalidate()V

    iput-boolean v7, p0, Lcom/android/videoeditor/widgets/MediaLinearLayout;->mFirstEntered:Z

    goto :goto_0

    :cond_3
    iget-object v2, p0, Lcom/android/videoeditor/widgets/MediaLinearLayout;->mScrollView:Landroid/view/View;

    const/4 v3, 0x3

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v2, v6, v3}, Landroid/view/View;->setTag(ILjava/lang/Object;)V

    goto :goto_1

    :pswitch_2
    const-string v2, "MediaLinearLayout"

    invoke-static {v2, v5}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v2

    if-eqz v2, :cond_4

    const-string v2, "MediaLinearLayout"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "ACTION_DRAG_EXITED: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    :cond_4
    iget-object v2, p0, Lcom/android/videoeditor/widgets/MediaLinearLayout;->mScrollView:Landroid/view/View;

    invoke-static {v8}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v2, v6, v3}, Landroid/view/View;->setTag(ILjava/lang/Object;)V

    iget-object v2, p0, Lcom/android/videoeditor/widgets/MediaLinearLayout;->mScrollView:Landroid/view/View;

    invoke-virtual {v2}, Landroid/view/View;->invalidate()V

    goto/16 :goto_0

    :pswitch_3
    const-string v2, "MediaLinearLayout"

    invoke-static {v2, v5}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v2

    if-eqz v2, :cond_5

    const-string v2, "MediaLinearLayout"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "ACTION_DRAG_ENDED: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v2, v4}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    :cond_5
    iput-object v3, p0, Lcom/android/videoeditor/widgets/MediaLinearLayout;->mDragMediaItemId:Ljava/lang/String;

    const/4 v2, -0x1

    iput v2, p0, Lcom/android/videoeditor/widgets/MediaLinearLayout;->mDropIndex:I

    iget-object v2, p0, Lcom/android/videoeditor/widgets/MediaLinearLayout;->mLeftHandle:Lcom/android/videoeditor/widgets/HandleView;

    invoke-virtual {v2, v7}, Lcom/android/videoeditor/widgets/HandleView;->setVisibility(I)V

    iget-object v2, p0, Lcom/android/videoeditor/widgets/MediaLinearLayout;->mRightHandle:Lcom/android/videoeditor/widgets/HandleView;

    invoke-virtual {v2, v7}, Lcom/android/videoeditor/widgets/HandleView;->setVisibility(I)V

    iget-object v2, p0, Lcom/android/videoeditor/widgets/MediaLinearLayout;->mScrollView:Landroid/view/View;

    invoke-static {v8}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v2, v6, v3}, Landroid/view/View;->setTag(ILjava/lang/Object;)V

    iget-object v2, p0, Lcom/android/videoeditor/widgets/MediaLinearLayout;->mScrollView:Landroid/view/View;

    invoke-virtual {v2}, Landroid/view/View;->invalidate()V

    invoke-virtual {p0}, Lcom/android/videoeditor/widgets/MediaLinearLayout;->requestLayout()V

    goto/16 :goto_0

    :pswitch_4
    const-string v2, "MediaLinearLayout"

    invoke-static {v2, v5}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v2

    if-eqz v2, :cond_6

    const-string v2, "MediaLinearLayout"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "ACTION_DRAG_LOCATION: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    :cond_6
    invoke-virtual {p1}, Landroid/view/DragEvent;->getX()F

    move-result v2

    invoke-direct {p0, v2}, Lcom/android/videoeditor/widgets/MediaLinearLayout;->moveToPosition(F)V

    const/4 v1, 0x1

    goto/16 :goto_0

    :pswitch_5
    const-string v2, "MediaLinearLayout"

    invoke-static {v2, v5}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v2

    if-eqz v2, :cond_7

    const-string v2, "MediaLinearLayout"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "ACTION_DROP: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v2, v4}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    :cond_7
    iget v2, p0, Lcom/android/videoeditor/widgets/MediaLinearLayout;->mDropIndex:I

    if-ltz v2, :cond_9

    iget-object v2, p0, Lcom/android/videoeditor/widgets/MediaLinearLayout;->mDropAfterMediaItem:Lcom/android/videoeditor/service/MovieMediaItem;

    if-eqz v2, :cond_a

    iget-object v2, p0, Lcom/android/videoeditor/widgets/MediaLinearLayout;->mDropAfterMediaItem:Lcom/android/videoeditor/service/MovieMediaItem;

    invoke-virtual {v2}, Lcom/android/videoeditor/service/MovieMediaItem;->getId()Ljava/lang/String;

    move-result-object v0

    :goto_2
    const-string v2, "MediaLinearLayout"

    const/4 v4, 0x3

    invoke-static {v2, v4}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v2

    if-eqz v2, :cond_8

    const-string v2, "MediaLinearLayout"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "ACTION_DROP: Index: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget v5, p0, Lcom/android/videoeditor/widgets/MediaLinearLayout;->mDropIndex:I

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " | "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v2, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_8
    invoke-virtual {p0}, Lcom/android/videoeditor/widgets/MediaLinearLayout;->getContext()Landroid/content/Context;

    move-result-object v2

    iget-object v4, p0, Lcom/android/videoeditor/widgets/MediaLinearLayout;->mProject:Lcom/android/videoeditor/service/VideoEditorProject;

    invoke-virtual {v4}, Lcom/android/videoeditor/service/VideoEditorProject;->getPath()Ljava/lang/String;

    move-result-object v4

    iget-object v5, p0, Lcom/android/videoeditor/widgets/MediaLinearLayout;->mDragMediaItemId:Ljava/lang/String;

    invoke-static {v2, v4, v5, v0, v3}, Lcom/android/videoeditor/service/ApiService;->moveMediaItem(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    :cond_9
    const/4 v1, 0x1

    goto/16 :goto_0

    :cond_a
    move-object v0, v3

    goto :goto_2

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_4
        :pswitch_5
        :pswitch_3
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public onGeneratePreviewMediaItemProgress(Ljava/lang/String;II)V
    .locals 4
    .param p1    # Ljava/lang/String;
    .param p2    # I
    .param p3    # I

    const/4 v3, 0x1

    const/4 v2, 0x0

    invoke-direct {p0, p1}, Lcom/android/videoeditor/widgets/MediaLinearLayout;->getMediaItemView(Ljava/lang/String;)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/android/videoeditor/widgets/MediaItemView;

    if-eqz v0, :cond_0

    invoke-virtual {v0, p3}, Lcom/android/videoeditor/widgets/MediaItemView;->setGeneratingEffectProgress(I)V

    invoke-virtual {v0}, Lcom/android/videoeditor/widgets/MediaItemView;->isSelected()Z

    move-result v1

    if-eqz v1, :cond_0

    if-nez p3, :cond_1

    iget-object v1, p0, Lcom/android/videoeditor/widgets/MediaLinearLayout;->mLeftHandle:Lcom/android/videoeditor/widgets/HandleView;

    invoke-virtual {v1, v2}, Lcom/android/videoeditor/widgets/HandleView;->setEnabled(Z)V

    iget-object v1, p0, Lcom/android/videoeditor/widgets/MediaLinearLayout;->mRightHandle:Lcom/android/videoeditor/widgets/HandleView;

    invoke-virtual {v1, v2}, Lcom/android/videoeditor/widgets/HandleView;->setEnabled(Z)V

    :cond_0
    :goto_0
    return-void

    :cond_1
    const/16 v1, 0x64

    if-ne p3, v1, :cond_0

    iget-object v1, p0, Lcom/android/videoeditor/widgets/MediaLinearLayout;->mLeftHandle:Lcom/android/videoeditor/widgets/HandleView;

    invoke-virtual {v1, v3}, Lcom/android/videoeditor/widgets/HandleView;->setEnabled(Z)V

    iget-object v1, p0, Lcom/android/videoeditor/widgets/MediaLinearLayout;->mRightHandle:Lcom/android/videoeditor/widgets/HandleView;

    invoke-virtual {v1, v3}, Lcom/android/videoeditor/widgets/HandleView;->setEnabled(Z)V

    goto :goto_0
.end method

.method public onGeneratePreviewTransitionProgress(Ljava/lang/String;II)V
    .locals 4
    .param p1    # Ljava/lang/String;
    .param p2    # I
    .param p3    # I

    const/4 v3, 0x1

    const/4 v2, 0x0

    invoke-direct {p0, p1}, Lcom/android/videoeditor/widgets/MediaLinearLayout;->getTransitionView(Ljava/lang/String;)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/android/videoeditor/widgets/TransitionView;

    if-eqz v0, :cond_0

    invoke-virtual {v0, p3}, Lcom/android/videoeditor/widgets/TransitionView;->setGeneratingTransitionProgress(I)V

    invoke-virtual {v0}, Lcom/android/videoeditor/widgets/TransitionView;->isSelected()Z

    move-result v1

    if-eqz v1, :cond_0

    if-nez p3, :cond_1

    iget-object v1, p0, Lcom/android/videoeditor/widgets/MediaLinearLayout;->mLeftHandle:Lcom/android/videoeditor/widgets/HandleView;

    invoke-virtual {v1, v2}, Lcom/android/videoeditor/widgets/HandleView;->setEnabled(Z)V

    iget-object v1, p0, Lcom/android/videoeditor/widgets/MediaLinearLayout;->mRightHandle:Lcom/android/videoeditor/widgets/HandleView;

    invoke-virtual {v1, v2}, Lcom/android/videoeditor/widgets/HandleView;->setEnabled(Z)V

    :cond_0
    :goto_0
    return-void

    :cond_1
    const/16 v1, 0x64

    if-ne p3, v1, :cond_0

    iget-object v1, p0, Lcom/android/videoeditor/widgets/MediaLinearLayout;->mLeftHandle:Lcom/android/videoeditor/widgets/HandleView;

    invoke-virtual {v1, v3}, Lcom/android/videoeditor/widgets/HandleView;->setEnabled(Z)V

    iget-object v1, p0, Lcom/android/videoeditor/widgets/MediaLinearLayout;->mRightHandle:Lcom/android/videoeditor/widgets/HandleView;

    invoke-virtual {v1, v3}, Lcom/android/videoeditor/widgets/HandleView;->setEnabled(Z)V

    goto :goto_0
.end method

.method protected onLayout(ZIIII)V
    .locals 27
    .param p1    # Z
    .param p2    # I
    .param p3    # I
    .param p4    # I
    .param p5    # I

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/videoeditor/widgets/MediaLinearLayout;->mProject:Lcom/android/videoeditor/service/VideoEditorProject;

    move-object/from16 v23, v0

    invoke-virtual/range {v23 .. v23}, Lcom/android/videoeditor/service/VideoEditorProject;->computeDuration()J

    move-result-wide v19

    invoke-virtual/range {p0 .. p0}, Lcom/android/videoeditor/widgets/MediaLinearLayout;->getWidth()I

    move-result v23

    move-object/from16 v0, p0

    iget v0, v0, Lcom/android/videoeditor/widgets/MediaLinearLayout;->mHalfParentWidth:I

    move/from16 v24, v0

    mul-int/lit8 v24, v24, 0x2

    sub-int v22, v23, v24

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/videoeditor/widgets/MediaLinearLayout;->mSelectedView:Landroid/view/View;

    move-object/from16 v23, v0

    if-eqz v23, :cond_1

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/videoeditor/widgets/MediaLinearLayout;->mScrollView:Landroid/view/View;

    move-object/from16 v23, v0

    const/high16 v24, 0x7f080000

    invoke-virtual/range {v23 .. v24}, Landroid/view/View;->getTag(I)Ljava/lang/Object;

    move-result-object v23

    check-cast v23, Ljava/lang/Integer;

    invoke-virtual/range {v23 .. v23}, Ljava/lang/Integer;->intValue()I

    move-result v11

    :goto_0
    invoke-virtual/range {p0 .. p0}, Lcom/android/videoeditor/widgets/MediaLinearLayout;->getPaddingTop()I

    move-result v18

    sub-int v5, p5, p3

    const-wide/16 v15, 0x0

    const/4 v10, 0x0

    invoke-virtual/range {p0 .. p0}, Lcom/android/videoeditor/widgets/MediaLinearLayout;->getChildCount()I

    move-result v6

    const/4 v9, 0x0

    :goto_1
    if-ge v9, v6, :cond_8

    move-object/from16 v0, p0

    invoke-virtual {v0, v9}, Lcom/android/videoeditor/widgets/MediaLinearLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v21

    invoke-virtual/range {v21 .. v21}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v17

    if-eqz v17, :cond_4

    move-object/from16 v0, p0

    move-object/from16 v1, v21

    invoke-direct {v0, v1}, Lcom/android/videoeditor/widgets/MediaLinearLayout;->computeViewDuration(Landroid/view/View;)J

    move-result-wide v7

    add-long v23, v15, v7

    move/from16 v0, v22

    int-to-long v0, v0

    move-wide/from16 v25, v0

    mul-long v23, v23, v25

    move-wide/from16 v0, v23

    long-to-float v0, v0

    move/from16 v23, v0

    move-wide/from16 v0, v19

    long-to-float v0, v0

    move/from16 v24, v0

    div-float v23, v23, v24

    move/from16 v0, v23

    float-to-int v0, v0

    move/from16 v23, v0

    add-int v14, v23, v11

    move-object/from16 v0, v17

    instance-of v0, v0, Lcom/android/videoeditor/service/MovieMediaItem;

    move/from16 v23, v0

    if-eqz v23, :cond_3

    invoke-virtual/range {v21 .. v21}, Landroid/view/View;->getLeft()I

    move-result v23

    move/from16 v0, v23

    if-ne v10, v0, :cond_0

    invoke-virtual/range {v21 .. v21}, Landroid/view/View;->getRight()I

    move-result v23

    move/from16 v0, v23

    if-eq v14, v0, :cond_2

    :cond_0
    invoke-virtual/range {v21 .. v21}, Landroid/view/View;->getLeft()I

    move-result v12

    invoke-virtual/range {v21 .. v21}, Landroid/view/View;->getRight()I

    move-result v13

    move-object/from16 v0, v21

    move/from16 v1, v18

    invoke-virtual {v0, v10, v1, v14, v5}, Landroid/view/View;->layout(IIII)V

    check-cast v21, Lcom/android/videoeditor/widgets/MediaItemView;

    move-object/from16 v0, v21

    invoke-virtual {v0, v12, v13}, Lcom/android/videoeditor/widgets/MediaItemView;->onLayoutPerformed(II)V

    :goto_2
    add-long/2addr v15, v7

    move v10, v14

    :goto_3
    add-int/lit8 v9, v9, 0x1

    goto :goto_1

    :cond_1
    move-object/from16 v0, p0

    iget v11, v0, Lcom/android/videoeditor/widgets/MediaLinearLayout;->mHalfParentWidth:I

    goto :goto_0

    :cond_2
    move-object/from16 v0, v21

    move/from16 v1, v18

    invoke-virtual {v0, v10, v1, v14, v5}, Landroid/view/View;->layout(IIII)V

    goto :goto_2

    :cond_3
    move-object/from16 v0, p0

    iget v0, v0, Lcom/android/videoeditor/widgets/MediaLinearLayout;->mTransitionVerticalInset:I

    move/from16 v23, v0

    add-int v23, v23, v18

    move-object/from16 v0, p0

    iget v0, v0, Lcom/android/videoeditor/widgets/MediaLinearLayout;->mTransitionVerticalInset:I

    move/from16 v24, v0

    sub-int v24, v5, v24

    move-object/from16 v0, v21

    move/from16 v1, v23

    move/from16 v2, v24

    invoke-virtual {v0, v10, v1, v14, v2}, Landroid/view/View;->layout(IIII)V

    goto :goto_2

    :cond_4
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/videoeditor/widgets/MediaLinearLayout;->mLeftHandle:Lcom/android/videoeditor/widgets/HandleView;

    move-object/from16 v23, v0

    move-object/from16 v0, v21

    move-object/from16 v1, v23

    if-ne v0, v1, :cond_5

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/videoeditor/widgets/MediaLinearLayout;->mSelectedView:Landroid/view/View;

    move-object/from16 v23, v0

    if-eqz v23, :cond_5

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/videoeditor/widgets/MediaLinearLayout;->mSelectedView:Landroid/view/View;

    move-object/from16 v23, v0

    invoke-virtual/range {v23 .. v23}, Landroid/view/View;->getLeft()I

    move-result v23

    move-object/from16 v0, p0

    iget v0, v0, Lcom/android/videoeditor/widgets/MediaLinearLayout;->mHandleWidth:I

    move/from16 v24, v0

    sub-int v23, v23, v24

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/videoeditor/widgets/MediaLinearLayout;->mSelectedView:Landroid/view/View;

    move-object/from16 v24, v0

    invoke-virtual/range {v24 .. v24}, Landroid/view/View;->getPaddingTop()I

    move-result v24

    add-int v24, v24, v18

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/videoeditor/widgets/MediaLinearLayout;->mSelectedView:Landroid/view/View;

    move-object/from16 v25, v0

    invoke-virtual/range {v25 .. v25}, Landroid/view/View;->getLeft()I

    move-result v25

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/videoeditor/widgets/MediaLinearLayout;->mSelectedView:Landroid/view/View;

    move-object/from16 v26, v0

    invoke-virtual/range {v26 .. v26}, Landroid/view/View;->getPaddingBottom()I

    move-result v26

    sub-int v26, v5, v26

    move-object/from16 v0, v21

    move/from16 v1, v23

    move/from16 v2, v24

    move/from16 v3, v25

    move/from16 v4, v26

    invoke-virtual {v0, v1, v2, v3, v4}, Landroid/view/View;->layout(IIII)V

    goto :goto_3

    :cond_5
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/videoeditor/widgets/MediaLinearLayout;->mRightHandle:Lcom/android/videoeditor/widgets/HandleView;

    move-object/from16 v23, v0

    move-object/from16 v0, v21

    move-object/from16 v1, v23

    if-ne v0, v1, :cond_6

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/videoeditor/widgets/MediaLinearLayout;->mSelectedView:Landroid/view/View;

    move-object/from16 v23, v0

    if-eqz v23, :cond_6

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/videoeditor/widgets/MediaLinearLayout;->mSelectedView:Landroid/view/View;

    move-object/from16 v23, v0

    invoke-virtual/range {v23 .. v23}, Landroid/view/View;->getRight()I

    move-result v23

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/videoeditor/widgets/MediaLinearLayout;->mSelectedView:Landroid/view/View;

    move-object/from16 v24, v0

    invoke-virtual/range {v24 .. v24}, Landroid/view/View;->getPaddingTop()I

    move-result v24

    add-int v24, v24, v18

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/videoeditor/widgets/MediaLinearLayout;->mSelectedView:Landroid/view/View;

    move-object/from16 v25, v0

    invoke-virtual/range {v25 .. v25}, Landroid/view/View;->getRight()I

    move-result v25

    move-object/from16 v0, p0

    iget v0, v0, Lcom/android/videoeditor/widgets/MediaLinearLayout;->mHandleWidth:I

    move/from16 v26, v0

    add-int v25, v25, v26

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/videoeditor/widgets/MediaLinearLayout;->mSelectedView:Landroid/view/View;

    move-object/from16 v26, v0

    invoke-virtual/range {v26 .. v26}, Landroid/view/View;->getPaddingBottom()I

    move-result v26

    sub-int v26, v5, v26

    move-object/from16 v0, v21

    move/from16 v1, v23

    move/from16 v2, v24

    move/from16 v3, v25

    move/from16 v4, v26

    invoke-virtual {v0, v1, v2, v3, v4}, Landroid/view/View;->layout(IIII)V

    goto/16 :goto_3

    :cond_6
    if-nez v9, :cond_7

    const/16 v23, 0x0

    move-object/from16 v0, v21

    move/from16 v1, v23

    move/from16 v2, v18

    invoke-virtual {v0, v1, v2, v11, v5}, Landroid/view/View;->layout(IIII)V

    add-int/2addr v10, v11

    goto/16 :goto_3

    :cond_7
    invoke-virtual/range {p0 .. p0}, Lcom/android/videoeditor/widgets/MediaLinearLayout;->getWidth()I

    move-result v23

    move-object/from16 v0, v21

    move/from16 v1, v18

    move/from16 v2, v23

    invoke-virtual {v0, v10, v1, v2, v5}, Landroid/view/View;->layout(IIII)V

    goto/16 :goto_3

    :cond_8
    const/16 v23, 0x0

    move/from16 v0, v23

    move-object/from16 v1, p0

    iput-boolean v0, v1, Lcom/android/videoeditor/widgets/MediaLinearLayout;->mMoveLayoutPending:Z

    return-void
.end method

.method public onResume()V
    .locals 5

    invoke-virtual {p0}, Lcom/android/videoeditor/widgets/MediaLinearLayout;->getChildCount()I

    move-result v1

    const/4 v2, 0x0

    :goto_0
    if-ge v2, v1, :cond_2

    invoke-virtual {p0, v2}, Lcom/android/videoeditor/widgets/MediaLinearLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v3

    if-eqz v3, :cond_0

    instance-of v4, v3, Lcom/android/videoeditor/service/MovieMediaItem;

    if-eqz v4, :cond_1

    check-cast v0, Lcom/android/videoeditor/widgets/MediaItemView;

    invoke-virtual {v0}, Lcom/android/videoeditor/widgets/MediaItemView;->resetGeneratingEffectProgress()V

    :cond_0
    :goto_1
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    :cond_1
    instance-of v4, v3, Lcom/android/videoeditor/service/MovieTransition;

    if-eqz v4, :cond_0

    check-cast v0, Lcom/android/videoeditor/widgets/TransitionView;

    invoke-virtual {v0}, Lcom/android/videoeditor/widgets/TransitionView;->resetGeneratingTransitionProgress()V

    goto :goto_1

    :cond_2
    return-void
.end method

.method public removeMediaItem(Ljava/lang/String;Lcom/android/videoeditor/service/MovieTransition;)Landroid/view/View;
    .locals 11
    .param p1    # Ljava/lang/String;
    .param p2    # Lcom/android/videoeditor/service/MovieTransition;

    const/4 v9, 0x0

    invoke-virtual {p0}, Lcom/android/videoeditor/widgets/MediaLinearLayout;->getChildCount()I

    move-result v3

    const/4 v7, 0x0

    const/4 v4, 0x0

    :goto_0
    if-ge v4, v3, :cond_8

    invoke-virtual {p0, v4}, Lcom/android/videoeditor/widgets/MediaLinearLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v2

    invoke-virtual {v2}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v8

    if-eqz v8, :cond_7

    instance-of v10, v8, Lcom/android/videoeditor/service/MovieMediaItem;

    if-eqz v10, :cond_7

    move-object v6, v8

    check-cast v6, Lcom/android/videoeditor/service/MovieMediaItem;

    invoke-virtual {v6}, Lcom/android/videoeditor/service/MovieMediaItem;->getId()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {p1, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_6

    move v5, v4

    if-lez v5, :cond_0

    add-int/lit8 v10, v5, -0x1

    invoke-virtual {p0, v10}, Lcom/android/videoeditor/widgets/MediaLinearLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v10

    invoke-virtual {v10}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v1

    if-eqz v1, :cond_0

    instance-of v10, v1, Lcom/android/videoeditor/service/MovieTransition;

    if-eqz v10, :cond_0

    add-int/lit8 v10, v5, -0x1

    invoke-virtual {p0, v10}, Lcom/android/videoeditor/widgets/MediaLinearLayout;->removeViewAt(I)V

    add-int/lit8 v5, v5, -0x1

    :cond_0
    invoke-virtual {p0}, Lcom/android/videoeditor/widgets/MediaLinearLayout;->getChildCount()I

    move-result v10

    add-int/lit8 v10, v10, -0x1

    if-ge v5, v10, :cond_1

    add-int/lit8 v10, v5, 0x1

    invoke-virtual {p0, v10}, Lcom/android/videoeditor/widgets/MediaLinearLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v10

    invoke-virtual {v10}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v0

    if-eqz v0, :cond_1

    instance-of v10, v0, Lcom/android/videoeditor/service/MovieTransition;

    if-eqz v10, :cond_1

    add-int/lit8 v10, v5, 0x1

    invoke-virtual {p0, v10}, Lcom/android/videoeditor/widgets/MediaLinearLayout;->removeViewAt(I)V

    :cond_1
    invoke-virtual {p0, v5}, Lcom/android/videoeditor/widgets/MediaLinearLayout;->removeViewAt(I)V

    if-eqz p2, :cond_3

    if-eqz v7, :cond_2

    invoke-virtual {v7}, Lcom/android/videoeditor/service/MovieMediaItem;->getId()Ljava/lang/String;

    move-result-object v9

    :cond_2
    invoke-virtual {p0, p2, v9}, Lcom/android/videoeditor/widgets/MediaLinearLayout;->addTransition(Lcom/android/videoeditor/service/MovieTransition;Ljava/lang/String;)Landroid/view/View;

    :cond_3
    iget-object v9, p0, Lcom/android/videoeditor/widgets/MediaLinearLayout;->mMediaItemActionMode:Landroid/view/ActionMode;

    if-eqz v9, :cond_4

    iget-object v9, p0, Lcom/android/videoeditor/widgets/MediaLinearLayout;->mMediaItemActionMode:Landroid/view/ActionMode;

    invoke-virtual {v9}, Landroid/view/ActionMode;->invalidate()V

    :cond_4
    iget-object v9, p0, Lcom/android/videoeditor/widgets/MediaLinearLayout;->mProject:Lcom/android/videoeditor/service/VideoEditorProject;

    invoke-virtual {v9}, Lcom/android/videoeditor/service/VideoEditorProject;->getMediaItemCount()I

    move-result v9

    if-nez v9, :cond_5

    iget-object v9, p0, Lcom/android/videoeditor/widgets/MediaLinearLayout;->mLeftAddClipButton:Landroid/widget/ImageButton;

    const/16 v10, 0x8

    invoke-virtual {v9, v10}, Landroid/widget/ImageButton;->setVisibility(I)V

    :cond_5
    :goto_1
    return-object v2

    :cond_6
    move-object v7, v6

    :cond_7
    add-int/lit8 v4, v4, 0x1

    goto :goto_0

    :cond_8
    move-object v2, v9

    goto :goto_1
.end method

.method public removeTransition(Ljava/lang/String;)V
    .locals 5
    .param p1    # Ljava/lang/String;

    invoke-virtual {p0}, Lcom/android/videoeditor/widgets/MediaLinearLayout;->getChildCount()I

    move-result v0

    const/4 v1, 0x0

    :goto_0
    if-ge v1, v0, :cond_0

    invoke-virtual {p0, v1}, Lcom/android/videoeditor/widgets/MediaLinearLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v4

    invoke-virtual {v4}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v2

    if-eqz v2, :cond_1

    instance-of v4, v2, Lcom/android/videoeditor/service/MovieTransition;

    if-eqz v4, :cond_1

    move-object v3, v2

    check-cast v3, Lcom/android/videoeditor/service/MovieTransition;

    invoke-virtual {v3}, Lcom/android/videoeditor/service/MovieTransition;->getId()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p1, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_1

    invoke-virtual {p0, v1}, Lcom/android/videoeditor/widgets/MediaLinearLayout;->removeViewAt(I)V

    invoke-virtual {p0}, Lcom/android/videoeditor/widgets/MediaLinearLayout;->requestLayout()V

    iget-object v4, p0, Lcom/android/videoeditor/widgets/MediaLinearLayout;->mMediaItemActionMode:Landroid/view/ActionMode;

    if-eqz v4, :cond_0

    iget-object v4, p0, Lcom/android/videoeditor/widgets/MediaLinearLayout;->mMediaItemActionMode:Landroid/view/ActionMode;

    invoke-virtual {v4}, Landroid/view/ActionMode;->invalidate()V

    :cond_0
    return-void

    :cond_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_0
.end method

.method public removeTransitionView(Ljava/lang/String;)V
    .locals 5
    .param p1    # Ljava/lang/String;

    invoke-virtual {p0}, Lcom/android/videoeditor/widgets/MediaLinearLayout;->getChildCount()I

    move-result v0

    const/4 v1, 0x0

    :goto_0
    if-ge v1, v0, :cond_0

    invoke-virtual {p0, v1}, Lcom/android/videoeditor/widgets/MediaLinearLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v4

    invoke-virtual {v4}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v2

    if-eqz v2, :cond_1

    instance-of v4, v2, Lcom/android/videoeditor/service/MovieTransition;

    if-eqz v4, :cond_1

    move-object v3, v2

    check-cast v3, Lcom/android/videoeditor/service/MovieTransition;

    invoke-virtual {v3}, Lcom/android/videoeditor/service/MovieTransition;->getId()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p1, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_1

    invoke-virtual {p0, v1}, Lcom/android/videoeditor/widgets/MediaLinearLayout;->removeViewAt(I)V

    invoke-virtual {p0}, Lcom/android/videoeditor/widgets/MediaLinearLayout;->requestLayout()V

    iget-object v4, p0, Lcom/android/videoeditor/widgets/MediaLinearLayout;->mMediaItemActionMode:Landroid/view/ActionMode;

    if-eqz v4, :cond_0

    iget-object v4, p0, Lcom/android/videoeditor/widgets/MediaLinearLayout;->mMediaItemActionMode:Landroid/view/ActionMode;

    invoke-virtual {v4}, Landroid/view/ActionMode;->invalidate()V

    :cond_0
    return-void

    :cond_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_0
.end method

.method public setListener(Lcom/android/videoeditor/widgets/MediaLinearLayoutListener;)V
    .locals 0
    .param p1    # Lcom/android/videoeditor/widgets/MediaLinearLayoutListener;

    iput-object p1, p0, Lcom/android/videoeditor/widgets/MediaLinearLayout;->mListener:Lcom/android/videoeditor/widgets/MediaLinearLayoutListener;

    return-void
.end method

.method public setMediaItemThumbnail(Ljava/lang/String;Landroid/graphics/Bitmap;II)Z
    .locals 5
    .param p1    # Ljava/lang/String;
    .param p2    # Landroid/graphics/Bitmap;
    .param p3    # I
    .param p4    # I

    invoke-virtual {p0}, Lcom/android/videoeditor/widgets/MediaLinearLayout;->getChildCount()I

    move-result v0

    const/4 v1, 0x0

    :goto_0
    if-ge v1, v0, :cond_1

    invoke-virtual {p0, v1}, Lcom/android/videoeditor/widgets/MediaLinearLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v4

    invoke-virtual {v4}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v3

    if-eqz v3, :cond_0

    instance-of v4, v3, Lcom/android/videoeditor/service/MovieMediaItem;

    if-eqz v4, :cond_0

    move-object v2, v3

    check-cast v2, Lcom/android/videoeditor/service/MovieMediaItem;

    invoke-virtual {v2}, Lcom/android/videoeditor/service/MovieMediaItem;->getId()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p1, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_0

    invoke-virtual {p0, v1}, Lcom/android/videoeditor/widgets/MediaLinearLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Lcom/android/videoeditor/widgets/MediaItemView;

    invoke-virtual {v4, p2, p3, p4}, Lcom/android/videoeditor/widgets/MediaItemView;->setBitmap(Landroid/graphics/Bitmap;II)Z

    move-result v4

    :goto_1
    return v4

    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_1
    const/4 v4, 0x0

    goto :goto_1
.end method

.method public setParentTimelineScrollView(Landroid/view/View;)V
    .locals 0
    .param p1    # Landroid/view/View;

    iput-object p1, p0, Lcom/android/videoeditor/widgets/MediaLinearLayout;->mScrollView:Landroid/view/View;

    return-void
.end method

.method public setPlaybackInProgress(Z)V
    .locals 0
    .param p1    # Z

    iput-boolean p1, p0, Lcom/android/videoeditor/widgets/MediaLinearLayout;->mPlaybackInProgress:Z

    invoke-direct {p0, p1}, Lcom/android/videoeditor/widgets/MediaLinearLayout;->setPlaybackState(Z)V

    invoke-direct {p0}, Lcom/android/videoeditor/widgets/MediaLinearLayout;->closeActionBars()V

    return-void
.end method

.method public setProject(Lcom/android/videoeditor/service/VideoEditorProject;)V
    .locals 0
    .param p1    # Lcom/android/videoeditor/service/VideoEditorProject;

    invoke-direct {p0}, Lcom/android/videoeditor/widgets/MediaLinearLayout;->closeActionBars()V

    invoke-direct {p0}, Lcom/android/videoeditor/widgets/MediaLinearLayout;->clearAndHideTrimHandles()V

    invoke-direct {p0}, Lcom/android/videoeditor/widgets/MediaLinearLayout;->removeAllMediaItemAndTransitionViews()V

    iput-object p1, p0, Lcom/android/videoeditor/widgets/MediaLinearLayout;->mProject:Lcom/android/videoeditor/service/VideoEditorProject;

    return-void
.end method

.method public setSelected(Z)V
    .locals 1
    .param p1    # Z

    if-nez p1, :cond_0

    invoke-direct {p0}, Lcom/android/videoeditor/widgets/MediaLinearLayout;->closeActionBars()V

    invoke-direct {p0}, Lcom/android/videoeditor/widgets/MediaLinearLayout;->clearAndHideTrimHandles()V

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/android/videoeditor/widgets/MediaLinearLayout;->mSelectedView:Landroid/view/View;

    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/android/videoeditor/widgets/MediaLinearLayout;->showAddMediaItemButtons(Z)V

    :cond_0
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/android/videoeditor/widgets/MediaLinearLayout;->dispatchSetSelected(Z)V

    return-void
.end method

.method public setSelectedView(I)V
    .locals 1
    .param p1    # I

    if-gez p1, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    invoke-virtual {p0, p1}, Lcom/android/videoeditor/widgets/MediaLinearLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/android/videoeditor/widgets/MediaLinearLayout;->mSelectedView:Landroid/view/View;

    iget-object v0, p0, Lcom/android/videoeditor/widgets/MediaLinearLayout;->mSelectedView:Landroid/view/View;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/videoeditor/widgets/MediaLinearLayout;->mSelectedView:Landroid/view/View;

    invoke-direct {p0, v0}, Lcom/android/videoeditor/widgets/MediaLinearLayout;->select(Landroid/view/View;)V

    goto :goto_0
.end method

.method public setTransitionThumbnails(Ljava/lang/String;[Landroid/graphics/Bitmap;)Z
    .locals 5
    .param p1    # Ljava/lang/String;
    .param p2    # [Landroid/graphics/Bitmap;

    invoke-virtual {p0}, Lcom/android/videoeditor/widgets/MediaLinearLayout;->getChildCount()I

    move-result v0

    const/4 v1, 0x0

    :goto_0
    if-ge v1, v0, :cond_1

    invoke-virtual {p0, v1}, Lcom/android/videoeditor/widgets/MediaLinearLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v4

    invoke-virtual {v4}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v2

    if-eqz v2, :cond_0

    instance-of v4, v2, Lcom/android/videoeditor/service/MovieTransition;

    if-eqz v4, :cond_0

    move-object v3, v2

    check-cast v3, Lcom/android/videoeditor/service/MovieTransition;

    invoke-virtual {v3}, Lcom/android/videoeditor/service/MovieTransition;->getId()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p1, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_0

    invoke-virtual {p0, v1}, Lcom/android/videoeditor/widgets/MediaLinearLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Lcom/android/videoeditor/widgets/TransitionView;

    invoke-virtual {v4, p2}, Lcom/android/videoeditor/widgets/TransitionView;->setBitmaps([Landroid/graphics/Bitmap;)Z

    move-result v4

    :goto_1
    return v4

    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_1
    const/4 v4, 0x0

    goto :goto_1
.end method

.method public updateMediaItem(Lcom/android/videoeditor/service/MovieMediaItem;)V
    .locals 10
    .param p1    # Lcom/android/videoeditor/service/MovieMediaItem;

    const/4 v9, 0x1

    invoke-virtual {p1}, Lcom/android/videoeditor/service/MovieMediaItem;->getId()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p0}, Lcom/android/videoeditor/widgets/MediaLinearLayout;->getChildCount()I

    move-result v1

    const/4 v2, 0x0

    :goto_0
    if-ge v2, v1, :cond_2

    invoke-virtual {p0, v2}, Lcom/android/videoeditor/widgets/MediaLinearLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v6

    if-eqz v6, :cond_4

    instance-of v8, v6, Lcom/android/videoeditor/service/MovieMediaItem;

    if-eqz v8, :cond_4

    move-object v4, v6

    check-cast v4, Lcom/android/videoeditor/service/MovieMediaItem;

    invoke-virtual {v4}, Lcom/android/videoeditor/service/MovieMediaItem;->getId()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v3, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_4

    if-eq p1, v4, :cond_1

    invoke-virtual {v0, p1}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    invoke-virtual {p1}, Lcom/android/videoeditor/service/MovieMediaItem;->getBeginTransition()Lcom/android/videoeditor/service/MovieTransition;

    move-result-object v8

    if-eqz v8, :cond_0

    if-lez v2, :cond_0

    add-int/lit8 v8, v2, -0x1

    invoke-virtual {p0, v8}, Lcom/android/videoeditor/widgets/MediaLinearLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v5

    invoke-virtual {v5}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v7

    if-eqz v7, :cond_0

    instance-of v8, v7, Lcom/android/videoeditor/service/MovieTransition;

    if-eqz v8, :cond_0

    invoke-virtual {p1}, Lcom/android/videoeditor/service/MovieMediaItem;->getBeginTransition()Lcom/android/videoeditor/service/MovieTransition;

    move-result-object v8

    invoke-virtual {v5, v8}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    :cond_0
    invoke-virtual {p1}, Lcom/android/videoeditor/service/MovieMediaItem;->getEndTransition()Lcom/android/videoeditor/service/MovieTransition;

    move-result-object v8

    if-eqz v8, :cond_1

    add-int/lit8 v8, v1, -0x1

    if-ge v2, v8, :cond_1

    add-int/lit8 v8, v2, 0x1

    invoke-virtual {p0, v8}, Lcom/android/videoeditor/widgets/MediaLinearLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v5

    invoke-virtual {v5}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v7

    if-eqz v7, :cond_1

    instance-of v8, v7, Lcom/android/videoeditor/service/MovieTransition;

    if-eqz v8, :cond_1

    invoke-virtual {p1}, Lcom/android/videoeditor/service/MovieMediaItem;->getEndTransition()Lcom/android/videoeditor/service/MovieTransition;

    move-result-object v8

    invoke-virtual {v5, v8}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    :cond_1
    invoke-virtual {v0}, Landroid/view/View;->isSelected()Z

    move-result v8

    if-eqz v8, :cond_2

    iget-object v8, p0, Lcom/android/videoeditor/widgets/MediaLinearLayout;->mLeftHandle:Lcom/android/videoeditor/widgets/HandleView;

    invoke-virtual {v8, v9}, Lcom/android/videoeditor/widgets/HandleView;->setEnabled(Z)V

    iget-object v8, p0, Lcom/android/videoeditor/widgets/MediaLinearLayout;->mRightHandle:Lcom/android/videoeditor/widgets/HandleView;

    invoke-virtual {v8, v9}, Lcom/android/videoeditor/widgets/HandleView;->setEnabled(Z)V

    :cond_2
    invoke-virtual {p0}, Lcom/android/videoeditor/widgets/MediaLinearLayout;->requestLayout()V

    iget-object v8, p0, Lcom/android/videoeditor/widgets/MediaLinearLayout;->mMediaItemActionMode:Landroid/view/ActionMode;

    if-eqz v8, :cond_3

    iget-object v8, p0, Lcom/android/videoeditor/widgets/MediaLinearLayout;->mMediaItemActionMode:Landroid/view/ActionMode;

    invoke-virtual {v8}, Landroid/view/ActionMode;->invalidate()V

    :cond_3
    return-void

    :cond_4
    add-int/lit8 v2, v2, 0x1

    goto/16 :goto_0
.end method

.method public updateTransition(Ljava/lang/String;)V
    .locals 0
    .param p1    # Ljava/lang/String;

    invoke-virtual {p0}, Lcom/android/videoeditor/widgets/MediaLinearLayout;->requestLayout()V

    invoke-virtual {p0}, Lcom/android/videoeditor/widgets/MediaLinearLayout;->invalidate()V

    return-void
.end method
