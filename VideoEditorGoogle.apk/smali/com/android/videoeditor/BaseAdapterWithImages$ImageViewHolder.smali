.class public Lcom/android/videoeditor/BaseAdapterWithImages$ImageViewHolder;
.super Ljava/lang/Object;
.source "BaseAdapterWithImages.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/videoeditor/BaseAdapterWithImages;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xc
    name = "ImageViewHolder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<K:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;"
    }
.end annotation


# instance fields
.field private final mImageView:Landroid/widget/ImageView;

.field private mKey:Ljava/lang/Object;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "TK;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/view/View;)V
    .locals 1
    .param p1    # Landroid/view/View;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const v0, 0x7f08000f

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/android/videoeditor/BaseAdapterWithImages$ImageViewHolder;->mImageView:Landroid/widget/ImageView;

    return-void
.end method

.method static synthetic access$200(Lcom/android/videoeditor/BaseAdapterWithImages$ImageViewHolder;)Ljava/lang/Object;
    .locals 1
    .param p0    # Lcom/android/videoeditor/BaseAdapterWithImages$ImageViewHolder;

    iget-object v0, p0, Lcom/android/videoeditor/BaseAdapterWithImages$ImageViewHolder;->mKey:Ljava/lang/Object;

    return-object v0
.end method

.method static synthetic access$300(Lcom/android/videoeditor/BaseAdapterWithImages$ImageViewHolder;)Landroid/widget/ImageView;
    .locals 1
    .param p0    # Lcom/android/videoeditor/BaseAdapterWithImages$ImageViewHolder;

    iget-object v0, p0, Lcom/android/videoeditor/BaseAdapterWithImages$ImageViewHolder;->mImageView:Landroid/widget/ImageView;

    return-object v0
.end method


# virtual methods
.method public setKey(Ljava/lang/Object;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TK;)V"
        }
    .end annotation

    iput-object p1, p0, Lcom/android/videoeditor/BaseAdapterWithImages$ImageViewHolder;->mKey:Ljava/lang/Object;

    return-void
.end method
