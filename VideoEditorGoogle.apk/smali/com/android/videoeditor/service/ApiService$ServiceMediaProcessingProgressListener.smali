.class final Lcom/android/videoeditor/service/ApiService$ServiceMediaProcessingProgressListener;
.super Ljava/lang/Object;
.source "ApiService.java"

# interfaces
.implements Landroid/media/videoeditor/VideoEditor$MediaProcessingProgressListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/videoeditor/service/ApiService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x12
    name = "ServiceMediaProcessingProgressListener"
.end annotation


# instance fields
.field private final mProjectPath:Ljava/lang/String;

.field final synthetic this$0:Lcom/android/videoeditor/service/ApiService;


# direct methods
.method public constructor <init>(Lcom/android/videoeditor/service/ApiService;Ljava/lang/String;)V
    .locals 0
    .param p2    # Ljava/lang/String;

    iput-object p1, p0, Lcom/android/videoeditor/service/ApiService$ServiceMediaProcessingProgressListener;->this$0:Lcom/android/videoeditor/service/ApiService;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p2, p0, Lcom/android/videoeditor/service/ApiService$ServiceMediaProcessingProgressListener;->mProjectPath:Ljava/lang/String;

    return-void
.end method


# virtual methods
.method public onProgress(Ljava/lang/Object;II)V
    .locals 7
    .param p1    # Ljava/lang/Object;
    .param p2    # I
    .param p3    # I

    const/4 v2, 0x0

    # getter for: Lcom/android/videoeditor/service/ApiService;->mIntentPool:Lcom/android/videoeditor/service/IntentPool;
    invoke-static {}, Lcom/android/videoeditor/service/ApiService;->access$200()Lcom/android/videoeditor/service/IntentPool;

    move-result-object v0

    invoke-virtual {v0}, Lcom/android/videoeditor/service/IntentPool;->get()Landroid/content/Intent;

    move-result-object v1

    const-string v0, "op"

    const/16 v3, 0xc

    invoke-virtual {v1, v0, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    const-string v0, "project"

    iget-object v3, p0, Lcom/android/videoeditor/service/ApiService$ServiceMediaProcessingProgressListener;->mProjectPath:Ljava/lang/String;

    invoke-virtual {v1, v0, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const-string v0, "action"

    invoke-virtual {v1, v0, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    const-string v0, "prog_value"

    invoke-virtual {v1, v0, p3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    if-nez p1, :cond_0

    :goto_0
    iget-object v0, p0, Lcom/android/videoeditor/service/ApiService$ServiceMediaProcessingProgressListener;->this$0:Lcom/android/videoeditor/service/ApiService;

    const/4 v6, 0x1

    move-object v3, v2

    move-object v4, v2

    move-object v5, v2

    # invokes: Lcom/android/videoeditor/service/ApiService;->completeRequest(Landroid/content/Intent;Landroid/media/videoeditor/VideoEditor;Ljava/lang/Exception;Ljava/lang/Object;Ljava/lang/Object;Z)V
    invoke-static/range {v0 .. v6}, Lcom/android/videoeditor/service/ApiService;->access$300(Lcom/android/videoeditor/service/ApiService;Landroid/content/Intent;Landroid/media/videoeditor/VideoEditor;Ljava/lang/Exception;Ljava/lang/Object;Ljava/lang/Object;Z)V

    :goto_1
    return-void

    :cond_0
    instance-of v0, p1, Landroid/media/videoeditor/MediaItem;

    if-eqz v0, :cond_1

    const-string v0, "item_id"

    check-cast p1, Landroid/media/videoeditor/MediaItem;

    invoke-virtual {p1}, Landroid/media/videoeditor/MediaItem;->getId()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v0, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const-string v0, "attributes"

    const-class v3, Landroid/media/videoeditor/MediaItem;

    invoke-virtual {v3}, Ljava/lang/Class;->getCanonicalName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v0, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    goto :goto_0

    :cond_1
    instance-of v0, p1, Landroid/media/videoeditor/Transition;

    if-eqz v0, :cond_2

    const-string v0, "item_id"

    check-cast p1, Landroid/media/videoeditor/Transition;

    invoke-virtual {p1}, Landroid/media/videoeditor/Transition;->getId()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v0, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const-string v0, "attributes"

    const-class v3, Landroid/media/videoeditor/Transition;

    invoke-virtual {v3}, Ljava/lang/Class;->getCanonicalName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v0, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    goto :goto_0

    :cond_2
    instance-of v0, p1, Landroid/media/videoeditor/AudioTrack;

    if-eqz v0, :cond_3

    const-string v0, "item_id"

    check-cast p1, Landroid/media/videoeditor/AudioTrack;

    invoke-virtual {p1}, Landroid/media/videoeditor/AudioTrack;->getId()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v0, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const-string v0, "attributes"

    const-class v3, Landroid/media/videoeditor/AudioTrack;

    invoke-virtual {v3}, Ljava/lang/Class;->getCanonicalName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v0, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    goto :goto_0

    :cond_3
    const-string v0, "VEApiService"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Unsupported storyboard item type: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1
.end method
