.class public Lcom/android/videoeditor/service/VideoEditorProject;
.super Ljava/lang/Object;
.source "VideoEditorProject.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/videoeditor/service/VideoEditorProject$1;,
        Lcom/android/videoeditor/service/VideoEditorProject$Download;
    }
.end annotation


# static fields
.field private static final ATTR_DURATION:Ljava/lang/String; = "duration"

.field private static final ATTR_FILENAME:Ljava/lang/String; = "filename"

.field private static final ATTR_MIME:Ljava/lang/String; = "mime"

.field private static final ATTR_NAME:Ljava/lang/String; = "name"

.field private static final ATTR_PLAYHEAD_POSITION:Ljava/lang/String; = "playhead"

.field private static final ATTR_SAVED:Ljava/lang/String; = "saved"

.field private static final ATTR_THEME:Ljava/lang/String; = "theme"

.field private static final ATTR_TIME:Ljava/lang/String; = "time"

.field private static final ATTR_URI:Ljava/lang/String; = "uri"

.field private static final ATTR_ZOOM_LEVEL:Ljava/lang/String; = "zoom_level"

.field public static final DEFAULT_ZOOM_LEVEL:I = 0x14

.field private static final PROJECT_METADATA_FILENAME:Ljava/lang/String; = "metadata.xml"

.field private static final TAG_DOWNLOAD:Ljava/lang/String; = "download"

.field private static final TAG_MOVIE:Ljava/lang/String; = "movie"

.field private static final TAG_PROJECT:Ljava/lang/String; = "project"


# instance fields
.field private mAspectRatio:I

.field private mAudioTracks:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/android/videoeditor/service/MovieAudioTrack;",
            ">;"
        }
    .end annotation
.end field

.field private mClean:Z

.field private final mDownloads:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/android/videoeditor/service/VideoEditorProject$Download;",
            ">;"
        }
    .end annotation
.end field

.field private mExportedMovieUri:Landroid/net/Uri;

.field private mLastSaved:J

.field private mMediaItems:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/android/videoeditor/service/MovieMediaItem;",
            ">;"
        }
    .end annotation
.end field

.field private mPlayheadPosMs:J

.field private final mProjectDurationMs:J

.field private mProjectName:Ljava/lang/String;

.field private final mProjectPath:Ljava/lang/String;

.field private mTheme:Ljava/lang/String;

.field private final mVideoEditor:Landroid/media/videoeditor/VideoEditor;

.field private mZoomLevel:I


# direct methods
.method constructor <init>(Landroid/media/videoeditor/VideoEditor;Ljava/lang/String;Ljava/lang/String;JJJILandroid/net/Uri;Ljava/lang/String;Ljava/util/List;)V
    .locals 1
    .param p1    # Landroid/media/videoeditor/VideoEditor;
    .param p2    # Ljava/lang/String;
    .param p3    # Ljava/lang/String;
    .param p4    # J
    .param p6    # J
    .param p8    # J
    .param p10    # I
    .param p11    # Landroid/net/Uri;
    .param p12    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/media/videoeditor/VideoEditor;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "JJJI",
            "Landroid/net/Uri;",
            "Ljava/lang/String;",
            "Ljava/util/List",
            "<",
            "Lcom/android/videoeditor/service/VideoEditorProject$Download;",
            ">;)V"
        }
    .end annotation

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/android/videoeditor/service/VideoEditorProject;->mMediaItems:Ljava/util/List;

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/android/videoeditor/service/VideoEditorProject;->mAudioTracks:Ljava/util/List;

    iput-object p1, p0, Lcom/android/videoeditor/service/VideoEditorProject;->mVideoEditor:Landroid/media/videoeditor/VideoEditor;

    if-eqz p1, :cond_0

    invoke-interface {p1}, Landroid/media/videoeditor/VideoEditor;->getAspectRatio()I

    move-result v0

    iput v0, p0, Lcom/android/videoeditor/service/VideoEditorProject;->mAspectRatio:I

    :cond_0
    if-eqz p13, :cond_1

    iput-object p13, p0, Lcom/android/videoeditor/service/VideoEditorProject;->mDownloads:Ljava/util/List;

    :goto_0
    iput-object p2, p0, Lcom/android/videoeditor/service/VideoEditorProject;->mProjectPath:Ljava/lang/String;

    iput-object p3, p0, Lcom/android/videoeditor/service/VideoEditorProject;->mProjectName:Ljava/lang/String;

    iput-wide p4, p0, Lcom/android/videoeditor/service/VideoEditorProject;->mLastSaved:J

    iput-wide p6, p0, Lcom/android/videoeditor/service/VideoEditorProject;->mPlayheadPosMs:J

    iput-wide p8, p0, Lcom/android/videoeditor/service/VideoEditorProject;->mProjectDurationMs:J

    iput p10, p0, Lcom/android/videoeditor/service/VideoEditorProject;->mZoomLevel:I

    iput-object p11, p0, Lcom/android/videoeditor/service/VideoEditorProject;->mExportedMovieUri:Landroid/net/Uri;

    iput-object p12, p0, Lcom/android/videoeditor/service/VideoEditorProject;->mTheme:Ljava/lang/String;

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/android/videoeditor/service/VideoEditorProject;->mClean:Z

    return-void

    :cond_1
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/android/videoeditor/service/VideoEditorProject;->mDownloads:Ljava/util/List;

    goto :goto_0
.end method

.method public static fromXml(Landroid/media/videoeditor/VideoEditor;Ljava/lang/String;)Lcom/android/videoeditor/service/VideoEditorProject;
    .locals 26
    .param p0    # Landroid/media/videoeditor/VideoEditor;
    .param p1    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/xmlpull/v1/XmlPullParserException;,
            Ljava/io/FileNotFoundException;,
            Ljava/io/IOException;
        }
    .end annotation

    new-instance v17, Ljava/io/File;

    const-string v2, "metadata.xml"

    move-object/from16 v0, v17

    move-object/from16 v1, p1

    invoke-direct {v0, v1, v2}, Ljava/io/File;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    new-instance v18, Ljava/io/FileInputStream;

    move-object/from16 v0, v18

    move-object/from16 v1, v17

    invoke-direct {v0, v1}, Ljava/io/FileInputStream;-><init>(Ljava/io/File;)V

    new-instance v15, Ljava/util/ArrayList;

    invoke-direct {v15}, Ljava/util/ArrayList;-><init>()V

    :try_start_0
    invoke-static {}, Landroid/util/Xml;->newPullParser()Lorg/xmlpull/v1/XmlPullParser;

    move-result-object v22

    const-string v2, "UTF-8"

    move-object/from16 v0, v22

    move-object/from16 v1, v18

    invoke-interface {v0, v1, v2}, Lorg/xmlpull/v1/XmlPullParser;->setInput(Ljava/io/InputStream;Ljava/lang/String;)V

    invoke-interface/range {v22 .. v22}, Lorg/xmlpull/v1/XmlPullParser;->getEventType()I

    move-result v16

    const/16 v25, 0x0

    const/4 v14, 0x0

    const/4 v13, 0x0

    const-wide/16 v19, 0x0

    const-wide/16 v23, 0x0

    const-wide/16 v10, 0x0

    const/16 v12, 0x14

    :goto_0
    const/4 v2, 0x1

    move/from16 v0, v16

    if-eq v0, v2, :cond_4

    const/16 v21, 0x0

    packed-switch v16, :pswitch_data_0

    :cond_0
    :goto_1
    invoke-interface/range {v22 .. v22}, Lorg/xmlpull/v1/XmlPullParser;->next()I

    move-result v16

    goto :goto_0

    :pswitch_0
    invoke-interface/range {v22 .. v22}, Lorg/xmlpull/v1/XmlPullParser;->getName()Ljava/lang/String;

    move-result-object v21

    const-string v2, "project"

    move-object/from16 v0, v21

    invoke-virtual {v0, v2}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_1

    const-string v2, ""

    const-string v3, "name"

    move-object/from16 v0, v22

    invoke-interface {v0, v2, v3}, Lorg/xmlpull/v1/XmlPullParser;->getAttributeValue(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v25

    const-string v2, ""

    const-string v3, "theme"

    move-object/from16 v0, v22

    invoke-interface {v0, v2, v3}, Lorg/xmlpull/v1/XmlPullParser;->getAttributeValue(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v14

    const-string v2, ""

    const-string v3, "saved"

    move-object/from16 v0, v22

    invoke-interface {v0, v2, v3}, Lorg/xmlpull/v1/XmlPullParser;->getAttributeValue(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v19

    const-string v2, ""

    const-string v3, "playhead"

    move-object/from16 v0, v22

    invoke-interface {v0, v2, v3}, Lorg/xmlpull/v1/XmlPullParser;->getAttributeValue(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v23

    const-string v2, ""

    const-string v3, "duration"

    move-object/from16 v0, v22

    invoke-interface {v0, v2, v3}, Lorg/xmlpull/v1/XmlPullParser;->getAttributeValue(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v10

    const-string v2, ""

    const-string v3, "zoom_level"

    move-object/from16 v0, v22

    invoke-interface {v0, v2, v3}, Lorg/xmlpull/v1/XmlPullParser;->getAttributeValue(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v12

    goto :goto_1

    :cond_1
    const-string v2, "movie"

    move-object/from16 v0, v21

    invoke-virtual {v0, v2}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_2

    const-string v2, ""

    const-string v3, "uri"

    move-object/from16 v0, v22

    invoke-interface {v0, v2, v3}, Lorg/xmlpull/v1/XmlPullParser;->getAttributeValue(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v13

    goto :goto_1

    :cond_2
    const-string v2, "download"

    move-object/from16 v0, v21

    invoke-virtual {v0, v2}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_0

    new-instance v2, Lcom/android/videoeditor/service/VideoEditorProject$Download;

    const-string v3, ""

    const-string v4, "uri"

    move-object/from16 v0, v22

    invoke-interface {v0, v3, v4}, Lorg/xmlpull/v1/XmlPullParser;->getAttributeValue(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    const-string v4, ""

    const-string v5, "mime"

    move-object/from16 v0, v22

    invoke-interface {v0, v4, v5}, Lorg/xmlpull/v1/XmlPullParser;->getAttributeValue(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    const-string v5, ""

    const-string v6, "filename"

    move-object/from16 v0, v22

    invoke-interface {v0, v5, v6}, Lorg/xmlpull/v1/XmlPullParser;->getAttributeValue(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    const-string v6, ""

    const-string v7, "time"

    move-object/from16 v0, v22

    invoke-interface {v0, v6, v7}, Lorg/xmlpull/v1/XmlPullParser;->getAttributeValue(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    invoke-static {v6}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v6

    const/4 v8, 0x0

    invoke-direct/range {v2 .. v8}, Lcom/android/videoeditor/service/VideoEditorProject$Download;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;JLcom/android/videoeditor/service/VideoEditorProject$1;)V

    invoke-interface {v15, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto/16 :goto_1

    :catchall_0
    move-exception v2

    if-eqz v18, :cond_3

    invoke-virtual/range {v18 .. v18}, Ljava/io/FileInputStream;->close()V

    :cond_3
    throw v2

    :cond_4
    :try_start_1
    new-instance v2, Lcom/android/videoeditor/service/VideoEditorProject;

    move-object/from16 v3, p0

    move-object/from16 v4, p1

    move-object/from16 v5, v25

    move-wide/from16 v6, v19

    move-wide/from16 v8, v23

    invoke-direct/range {v2 .. v15}, Lcom/android/videoeditor/service/VideoEditorProject;-><init>(Landroid/media/videoeditor/VideoEditor;Ljava/lang/String;Ljava/lang/String;JJJILandroid/net/Uri;Ljava/lang/String;Ljava/util/List;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    if-eqz v18, :cond_5

    invoke-virtual/range {v18 .. v18}, Ljava/io/FileInputStream;->close()V

    :cond_5
    return-object v2

    :pswitch_data_0
    .packed-switch 0x2
        :pswitch_0
    .end packed-switch
.end method

.method private static getEndTransitionDuration(Lcom/android/videoeditor/service/MovieMediaItem;)J
    .locals 2
    .param p0    # Lcom/android/videoeditor/service/MovieMediaItem;

    invoke-virtual {p0}, Lcom/android/videoeditor/service/MovieMediaItem;->getEndTransition()Lcom/android/videoeditor/service/MovieTransition;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/android/videoeditor/service/MovieMediaItem;->getEndTransition()Lcom/android/videoeditor/service/MovieTransition;

    move-result-object v0

    invoke-virtual {v0}, Lcom/android/videoeditor/service/MovieTransition;->getAppDuration()J

    move-result-wide v0

    :goto_0
    return-wide v0

    :cond_0
    const-wide/16 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method addAudioTrack(Lcom/android/videoeditor/service/MovieAudioTrack;)V
    .locals 1
    .param p1    # Lcom/android/videoeditor/service/MovieAudioTrack;

    iget-object v0, p0, Lcom/android/videoeditor/service/VideoEditorProject;->mAudioTracks:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/android/videoeditor/service/VideoEditorProject;->mClean:Z

    return-void
.end method

.method public addDownload(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 8
    .param p1    # Ljava/lang/String;
    .param p2    # Ljava/lang/String;
    .param p3    # Ljava/lang/String;

    iget-object v7, p0, Lcom/android/videoeditor/service/VideoEditorProject;->mDownloads:Ljava/util/List;

    new-instance v0, Lcom/android/videoeditor/service/VideoEditorProject$Download;

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v4

    const/4 v6, 0x0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    invoke-direct/range {v0 .. v6}, Lcom/android/videoeditor/service/VideoEditorProject$Download;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;JLcom/android/videoeditor/service/VideoEditorProject$1;)V

    invoke-interface {v7, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/android/videoeditor/service/VideoEditorProject;->mClean:Z

    return-void
.end method

.method addEffect(Ljava/lang/String;Lcom/android/videoeditor/service/MovieEffect;)V
    .locals 3
    .param p1    # Ljava/lang/String;
    .param p2    # Lcom/android/videoeditor/service/MovieEffect;

    invoke-virtual {p0, p1}, Lcom/android/videoeditor/service/VideoEditorProject;->getMediaItem(Ljava/lang/String;)Lcom/android/videoeditor/service/MovieMediaItem;

    move-result-object v0

    invoke-virtual {v0}, Lcom/android/videoeditor/service/MovieMediaItem;->getEffect()Lcom/android/videoeditor/service/MovieEffect;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-virtual {v1}, Lcom/android/videoeditor/service/MovieEffect;->getId()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/android/videoeditor/service/MovieMediaItem;->removeEffect(Ljava/lang/String;)V

    :cond_0
    invoke-virtual {v0, p2}, Lcom/android/videoeditor/service/MovieMediaItem;->addEffect(Lcom/android/videoeditor/service/MovieEffect;)V

    const/4 v2, 0x0

    iput-boolean v2, p0, Lcom/android/videoeditor/service/VideoEditorProject;->mClean:Z

    return-void
.end method

.method addExportedMovieUri(Landroid/net/Uri;)V
    .locals 1
    .param p1    # Landroid/net/Uri;

    iput-object p1, p0, Lcom/android/videoeditor/service/VideoEditorProject;->mExportedMovieUri:Landroid/net/Uri;

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/android/videoeditor/service/VideoEditorProject;->mClean:Z

    return-void
.end method

.method addOverlay(Ljava/lang/String;Lcom/android/videoeditor/service/MovieOverlay;)V
    .locals 3
    .param p1    # Ljava/lang/String;
    .param p2    # Lcom/android/videoeditor/service/MovieOverlay;

    invoke-virtual {p0, p1}, Lcom/android/videoeditor/service/VideoEditorProject;->getMediaItem(Ljava/lang/String;)Lcom/android/videoeditor/service/MovieMediaItem;

    move-result-object v0

    invoke-virtual {v0}, Lcom/android/videoeditor/service/MovieMediaItem;->getOverlay()Lcom/android/videoeditor/service/MovieOverlay;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-virtual {v1}, Lcom/android/videoeditor/service/MovieOverlay;->getId()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/android/videoeditor/service/MovieMediaItem;->removeOverlay(Ljava/lang/String;)V

    :cond_0
    invoke-virtual {v0, p2}, Lcom/android/videoeditor/service/MovieMediaItem;->addOverlay(Lcom/android/videoeditor/service/MovieOverlay;)V

    const/4 v2, 0x0

    iput-boolean v2, p0, Lcom/android/videoeditor/service/VideoEditorProject;->mClean:Z

    return-void
.end method

.method addTransition(Lcom/android/videoeditor/service/MovieTransition;Ljava/lang/String;)V
    .locals 9
    .param p1    # Lcom/android/videoeditor/service/MovieTransition;
    .param p2    # Ljava/lang/String;

    const/4 v8, 0x0

    iget-object v6, p0, Lcom/android/videoeditor/service/VideoEditorProject;->mMediaItems:Ljava/util/List;

    invoke-interface {v6}, Ljava/util/List;->size()I

    move-result v3

    if-eqz p2, :cond_4

    const/4 v0, 0x0

    const/4 v1, -0x1

    const/4 v4, 0x0

    :goto_0
    if-ge v4, v3, :cond_0

    iget-object v6, p0, Lcom/android/videoeditor/service/VideoEditorProject;->mMediaItems:Ljava/util/List;

    invoke-interface {v6, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/android/videoeditor/service/MovieMediaItem;

    invoke-virtual {v5}, Lcom/android/videoeditor/service/MovieMediaItem;->getId()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v6, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_1

    move-object v0, v5

    move v1, v4

    :cond_0
    if-nez v0, :cond_2

    new-instance v6, Ljava/lang/IllegalArgumentException;

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "Media item not found: "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-direct {v6, v7}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v6

    :cond_1
    add-int/lit8 v4, v4, 0x1

    goto :goto_0

    :cond_2
    invoke-virtual {v0, p1}, Lcom/android/videoeditor/service/MovieMediaItem;->setEndTransition(Lcom/android/videoeditor/service/MovieTransition;)V

    add-int/lit8 v6, v3, -0x1

    if-ge v1, v6, :cond_3

    iget-object v6, p0, Lcom/android/videoeditor/service/VideoEditorProject;->mMediaItems:Ljava/util/List;

    add-int/lit8 v7, v1, 0x1

    invoke-interface {v6, v7}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/android/videoeditor/service/MovieMediaItem;

    invoke-virtual {v2, p1}, Lcom/android/videoeditor/service/MovieMediaItem;->setBeginTransition(Lcom/android/videoeditor/service/MovieTransition;)V

    :cond_3
    :goto_1
    iput-boolean v8, p0, Lcom/android/videoeditor/service/VideoEditorProject;->mClean:Z

    return-void

    :cond_4
    if-nez v3, :cond_5

    new-instance v6, Ljava/lang/IllegalArgumentException;

    const-string v7, "Media item not found at the beginning"

    invoke-direct {v6, v7}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v6

    :cond_5
    iget-object v6, p0, Lcom/android/videoeditor/service/VideoEditorProject;->mMediaItems:Ljava/util/List;

    invoke-interface {v6, v8}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/android/videoeditor/service/MovieMediaItem;

    invoke-virtual {v2, p1}, Lcom/android/videoeditor/service/MovieMediaItem;->setBeginTransition(Lcom/android/videoeditor/service/MovieTransition;)V

    goto :goto_1
.end method

.method public clearSurface(Landroid/view/SurfaceHolder;)V
    .locals 1
    .param p1    # Landroid/view/SurfaceHolder;

    iget-object v0, p0, Lcom/android/videoeditor/service/VideoEditorProject;->mVideoEditor:Landroid/media/videoeditor/VideoEditor;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/videoeditor/service/VideoEditorProject;->mVideoEditor:Landroid/media/videoeditor/VideoEditor;

    invoke-interface {v0, p1}, Landroid/media/videoeditor/VideoEditor;->clearSurface(Landroid/view/SurfaceHolder;)V

    :cond_0
    return-void
.end method

.method public computeDuration()J
    .locals 7

    const-wide/16 v3, 0x0

    iget-object v5, p0, Lcom/android/videoeditor/service/VideoEditorProject;->mMediaItems:Ljava/util/List;

    invoke-interface {v5}, Ljava/util/List;->size()I

    move-result v2

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v2, :cond_1

    iget-object v5, p0, Lcom/android/videoeditor/service/VideoEditorProject;->mMediaItems:Ljava/util/List;

    invoke-interface {v5, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/android/videoeditor/service/MovieMediaItem;

    invoke-virtual {v1}, Lcom/android/videoeditor/service/MovieMediaItem;->getAppTimelineDuration()J

    move-result-wide v5

    add-long/2addr v3, v5

    invoke-virtual {v1}, Lcom/android/videoeditor/service/MovieMediaItem;->getEndTransition()Lcom/android/videoeditor/service/MovieTransition;

    move-result-object v5

    if-eqz v5, :cond_0

    add-int/lit8 v5, v2, -0x1

    if-ge v0, v5, :cond_0

    invoke-virtual {v1}, Lcom/android/videoeditor/service/MovieMediaItem;->getEndTransition()Lcom/android/videoeditor/service/MovieTransition;

    move-result-object v5

    invoke-virtual {v5}, Lcom/android/videoeditor/service/MovieTransition;->getAppDuration()J

    move-result-wide v5

    sub-long/2addr v3, v5

    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_1
    return-wide v3
.end method

.method public getAspectRatio()I
    .locals 1

    iget v0, p0, Lcom/android/videoeditor/service/VideoEditorProject;->mAspectRatio:I

    return v0
.end method

.method public getAudioTrack(Ljava/lang/String;)Lcom/android/videoeditor/service/MovieAudioTrack;
    .locals 3
    .param p1    # Ljava/lang/String;

    iget-object v2, p0, Lcom/android/videoeditor/service/VideoEditorProject;->mAudioTracks:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/videoeditor/service/MovieAudioTrack;

    invoke-virtual {v0}, Lcom/android/videoeditor/service/MovieAudioTrack;->getId()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    :goto_0
    return-object v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getAudioTracks()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/android/videoeditor/service/MovieAudioTrack;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/android/videoeditor/service/VideoEditorProject;->mAudioTracks:Ljava/util/List;

    return-object v0
.end method

.method public getDownloads()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/android/videoeditor/service/VideoEditorProject$Download;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/android/videoeditor/service/VideoEditorProject;->mDownloads:Ljava/util/List;

    return-object v0
.end method

.method public getEffect(Ljava/lang/String;Ljava/lang/String;)Lcom/android/videoeditor/service/MovieEffect;
    .locals 2
    .param p1    # Ljava/lang/String;
    .param p2    # Ljava/lang/String;

    invoke-virtual {p0, p1}, Lcom/android/videoeditor/service/VideoEditorProject;->getMediaItem(Ljava/lang/String;)Lcom/android/videoeditor/service/MovieMediaItem;

    move-result-object v0

    invoke-virtual {v0}, Lcom/android/videoeditor/service/MovieMediaItem;->getEffect()Lcom/android/videoeditor/service/MovieEffect;

    move-result-object v1

    return-object v1
.end method

.method public getExportedMovieUri()Landroid/net/Uri;
    .locals 1

    iget-object v0, p0, Lcom/android/videoeditor/service/VideoEditorProject;->mExportedMovieUri:Landroid/net/Uri;

    return-object v0
.end method

.method public getFirstMediaItem()Lcom/android/videoeditor/service/MovieMediaItem;
    .locals 2

    iget-object v0, p0, Lcom/android/videoeditor/service/VideoEditorProject;->mMediaItems:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/android/videoeditor/service/VideoEditorProject;->mMediaItems:Ljava/util/List;

    const/4 v1, 0x0

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/videoeditor/service/MovieMediaItem;

    goto :goto_0
.end method

.method public getInsertAfterMediaItem(J)Lcom/android/videoeditor/service/MovieMediaItem;
    .locals 12
    .param p1    # J

    const-wide/16 v0, 0x0

    const-wide/16 v2, 0x0

    const/4 v7, 0x0

    iget-object v8, p0, Lcom/android/videoeditor/service/VideoEditorProject;->mMediaItems:Ljava/util/List;

    invoke-interface {v8}, Ljava/util/List;->size()I

    move-result v6

    const/4 v4, 0x0

    :goto_0
    if-ge v4, v6, :cond_3

    iget-object v8, p0, Lcom/android/videoeditor/service/VideoEditorProject;->mMediaItems:Ljava/util/List;

    invoke-interface {v8, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/android/videoeditor/service/MovieMediaItem;

    invoke-virtual {v5}, Lcom/android/videoeditor/service/MovieMediaItem;->getAppTimelineDuration()J

    move-result-wide v8

    add-long v2, v0, v8

    invoke-virtual {v5}, Lcom/android/videoeditor/service/MovieMediaItem;->getEndTransition()Lcom/android/videoeditor/service/MovieTransition;

    move-result-object v8

    if-eqz v8, :cond_0

    add-int/lit8 v8, v6, -0x1

    if-ge v4, v8, :cond_0

    invoke-virtual {v5}, Lcom/android/videoeditor/service/MovieMediaItem;->getEndTransition()Lcom/android/videoeditor/service/MovieTransition;

    move-result-object v8

    invoke-virtual {v8}, Lcom/android/videoeditor/service/MovieTransition;->getAppDuration()J

    move-result-wide v8

    sub-long/2addr v2, v8

    :cond_0
    cmp-long v8, p1, v0

    if-ltz v8, :cond_2

    cmp-long v8, p1, v2

    if-gtz v8, :cond_2

    sub-long v8, p1, v0

    sub-long v10, v2, p1

    cmp-long v8, v8, v10

    if-gez v8, :cond_1

    :goto_1
    return-object v7

    :cond_1
    move-object v7, v5

    goto :goto_1

    :cond_2
    move-wide v0, v2

    move-object v7, v5

    add-int/lit8 v4, v4, 0x1

    goto :goto_0

    :cond_3
    const/4 v7, 0x0

    goto :goto_1
.end method

.method public getLastMediaItem()Lcom/android/videoeditor/service/MovieMediaItem;
    .locals 3

    iget-object v1, p0, Lcom/android/videoeditor/service/VideoEditorProject;->mMediaItems:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v0

    if-nez v0, :cond_0

    const/4 v1, 0x0

    :goto_0
    return-object v1

    :cond_0
    iget-object v1, p0, Lcom/android/videoeditor/service/VideoEditorProject;->mMediaItems:Ljava/util/List;

    add-int/lit8 v2, v0, -0x1

    invoke-interface {v1, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/android/videoeditor/service/MovieMediaItem;

    goto :goto_0
.end method

.method public getLastMediaItemId()Ljava/lang/String;
    .locals 2

    invoke-virtual {p0}, Lcom/android/videoeditor/service/VideoEditorProject;->getLastMediaItem()Lcom/android/videoeditor/service/MovieMediaItem;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/android/videoeditor/service/MovieMediaItem;->getId()Ljava/lang/String;

    move-result-object v1

    :goto_0
    return-object v1

    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public getLastSaved()J
    .locals 2

    iget-wide v0, p0, Lcom/android/videoeditor/service/VideoEditorProject;->mLastSaved:J

    return-wide v0
.end method

.method public getMediaItem(Ljava/lang/String;)Lcom/android/videoeditor/service/MovieMediaItem;
    .locals 3
    .param p1    # Ljava/lang/String;

    iget-object v2, p0, Lcom/android/videoeditor/service/VideoEditorProject;->mMediaItems:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :cond_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/android/videoeditor/service/MovieMediaItem;

    invoke-virtual {v1}, Lcom/android/videoeditor/service/MovieMediaItem;->getId()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    :goto_0
    return-object v1

    :cond_1
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public getMediaItemBeginTime(Ljava/lang/String;)J
    .locals 7
    .param p1    # Ljava/lang/String;

    const-wide/16 v0, 0x0

    iget-object v5, p0, Lcom/android/videoeditor/service/VideoEditorProject;->mMediaItems:Ljava/util/List;

    invoke-interface {v5}, Ljava/util/List;->size()I

    move-result v3

    const/4 v2, 0x0

    :goto_0
    if-ge v2, v3, :cond_0

    iget-object v5, p0, Lcom/android/videoeditor/service/VideoEditorProject;->mMediaItems:Ljava/util/List;

    invoke-interface {v5, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/android/videoeditor/service/MovieMediaItem;

    invoke-virtual {v4}, Lcom/android/videoeditor/service/MovieMediaItem;->getId()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v5, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_1

    :cond_0
    return-wide v0

    :cond_1
    invoke-virtual {v4}, Lcom/android/videoeditor/service/MovieMediaItem;->getAppTimelineDuration()J

    move-result-wide v5

    add-long/2addr v0, v5

    invoke-virtual {v4}, Lcom/android/videoeditor/service/MovieMediaItem;->getEndTransition()Lcom/android/videoeditor/service/MovieTransition;

    move-result-object v5

    if-eqz v5, :cond_2

    add-int/lit8 v5, v3, -0x1

    if-ge v2, v5, :cond_2

    invoke-virtual {v4}, Lcom/android/videoeditor/service/MovieMediaItem;->getEndTransition()Lcom/android/videoeditor/service/MovieTransition;

    move-result-object v5

    invoke-virtual {v5}, Lcom/android/videoeditor/service/MovieTransition;->getAppDuration()J

    move-result-wide v5

    sub-long/2addr v0, v5

    :cond_2
    add-int/lit8 v2, v2, 0x1

    goto :goto_0
.end method

.method public getMediaItemCount()I
    .locals 1

    iget-object v0, p0, Lcom/android/videoeditor/service/VideoEditorProject;->mMediaItems:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    return v0
.end method

.method public getMediaItems()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/android/videoeditor/service/MovieMediaItem;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/android/videoeditor/service/VideoEditorProject;->mMediaItems:Ljava/util/List;

    return-object v0
.end method

.method public getName()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/android/videoeditor/service/VideoEditorProject;->mProjectName:Ljava/lang/String;

    return-object v0
.end method

.method public getNextMediaItem(J)Lcom/android/videoeditor/service/MovieMediaItem;
    .locals 10
    .param p1    # J

    const/4 v5, 0x0

    const-wide/16 v3, 0x0

    iget-object v6, p0, Lcom/android/videoeditor/service/VideoEditorProject;->mMediaItems:Ljava/util/List;

    invoke-interface {v6}, Ljava/util/List;->size()I

    move-result v0

    const/4 v1, 0x0

    :goto_0
    if-ge v1, v0, :cond_0

    iget-object v6, p0, Lcom/android/videoeditor/service/VideoEditorProject;->mMediaItems:Ljava/util/List;

    invoke-interface {v6, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/android/videoeditor/service/MovieMediaItem;

    cmp-long v6, p1, v3

    if-ltz v6, :cond_1

    invoke-virtual {v2}, Lcom/android/videoeditor/service/MovieMediaItem;->getAppTimelineDuration()J

    move-result-wide v6

    add-long/2addr v6, v3

    invoke-static {v2}, Lcom/android/videoeditor/service/VideoEditorProject;->getEndTransitionDuration(Lcom/android/videoeditor/service/MovieMediaItem;)J

    move-result-wide v8

    sub-long/2addr v6, v8

    cmp-long v6, p1, v6

    if-gez v6, :cond_1

    add-int/lit8 v6, v0, -0x1

    if-ge v1, v6, :cond_0

    iget-object v5, p0, Lcom/android/videoeditor/service/VideoEditorProject;->mMediaItems:Ljava/util/List;

    add-int/lit8 v6, v1, 0x1

    invoke-interface {v5, v6}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/android/videoeditor/service/MovieMediaItem;

    :cond_0
    :goto_1
    return-object v5

    :cond_1
    cmp-long v6, p1, v3

    if-ltz v6, :cond_2

    invoke-virtual {v2}, Lcom/android/videoeditor/service/MovieMediaItem;->getAppTimelineDuration()J

    move-result-wide v6

    add-long/2addr v6, v3

    cmp-long v6, p1, v6

    if-gez v6, :cond_2

    add-int/lit8 v6, v0, -0x2

    if-ge v1, v6, :cond_0

    iget-object v5, p0, Lcom/android/videoeditor/service/VideoEditorProject;->mMediaItems:Ljava/util/List;

    add-int/lit8 v6, v1, 0x2

    invoke-interface {v5, v6}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/android/videoeditor/service/MovieMediaItem;

    goto :goto_1

    :cond_2
    invoke-virtual {v2}, Lcom/android/videoeditor/service/MovieMediaItem;->getAppTimelineDuration()J

    move-result-wide v6

    add-long/2addr v3, v6

    invoke-static {v2}, Lcom/android/videoeditor/service/VideoEditorProject;->getEndTransitionDuration(Lcom/android/videoeditor/service/MovieMediaItem;)J

    move-result-wide v6

    sub-long/2addr v3, v6

    add-int/lit8 v1, v1, 0x1

    goto :goto_0
.end method

.method public getNextMediaItem(Ljava/lang/String;)Lcom/android/videoeditor/service/MovieMediaItem;
    .locals 5
    .param p1    # Ljava/lang/String;

    const/4 v1, 0x0

    iget-object v4, p0, Lcom/android/videoeditor/service/VideoEditorProject;->mMediaItems:Ljava/util/List;

    invoke-interface {v4}, Ljava/util/List;->size()I

    move-result v0

    const/4 v2, 0x0

    :goto_0
    if-ge v2, v0, :cond_2

    iget-object v4, p0, Lcom/android/videoeditor/service/VideoEditorProject;->mMediaItems:Ljava/util/List;

    invoke-interface {v4, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/android/videoeditor/service/MovieMediaItem;

    if-eqz v1, :cond_0

    :goto_1
    return-object v3

    :cond_0
    invoke-virtual {v3}, Lcom/android/videoeditor/service/MovieMediaItem;->getId()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p1, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_1

    const/4 v1, 0x1

    :cond_1
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    :cond_2
    const/4 v3, 0x0

    goto :goto_1
.end method

.method public getOverlay(Ljava/lang/String;Ljava/lang/String;)Lcom/android/videoeditor/service/MovieOverlay;
    .locals 2
    .param p1    # Ljava/lang/String;
    .param p2    # Ljava/lang/String;

    invoke-virtual {p0, p1}, Lcom/android/videoeditor/service/VideoEditorProject;->getMediaItem(Ljava/lang/String;)Lcom/android/videoeditor/service/MovieMediaItem;

    move-result-object v0

    invoke-virtual {v0}, Lcom/android/videoeditor/service/MovieMediaItem;->getOverlay()Lcom/android/videoeditor/service/MovieOverlay;

    move-result-object v1

    return-object v1
.end method

.method public getPath()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/android/videoeditor/service/VideoEditorProject;->mProjectPath:Ljava/lang/String;

    return-object v0
.end method

.method public getPlayheadPos()J
    .locals 2

    iget-wide v0, p0, Lcom/android/videoeditor/service/VideoEditorProject;->mPlayheadPosMs:J

    return-wide v0
.end method

.method public getPreviousMediaItem(J)Lcom/android/videoeditor/service/MovieMediaItem;
    .locals 7
    .param p1    # J

    const-wide/16 v3, 0x0

    const/4 v2, 0x0

    iget-object v5, p0, Lcom/android/videoeditor/service/VideoEditorProject;->mMediaItems:Ljava/util/List;

    invoke-interface {v5}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :cond_0
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/android/videoeditor/service/MovieMediaItem;

    cmp-long v5, p1, v3

    if-nez v5, :cond_3

    :cond_1
    move-object v1, v2

    :cond_2
    return-object v1

    :cond_3
    cmp-long v5, p1, v3

    if-lez v5, :cond_4

    invoke-virtual {v1}, Lcom/android/videoeditor/service/MovieMediaItem;->getAppTimelineDuration()J

    move-result-wide v5

    add-long/2addr v5, v3

    cmp-long v5, p1, v5

    if-ltz v5, :cond_2

    :cond_4
    move-object v2, v1

    invoke-virtual {v1}, Lcom/android/videoeditor/service/MovieMediaItem;->getAppTimelineDuration()J

    move-result-wide v5

    add-long/2addr v3, v5

    invoke-virtual {v1}, Lcom/android/videoeditor/service/MovieMediaItem;->getEndTransition()Lcom/android/videoeditor/service/MovieTransition;

    move-result-object v5

    if-eqz v5, :cond_0

    invoke-virtual {v1}, Lcom/android/videoeditor/service/MovieMediaItem;->getEndTransition()Lcom/android/videoeditor/service/MovieTransition;

    move-result-object v5

    invoke-virtual {v5}, Lcom/android/videoeditor/service/MovieTransition;->getAppDuration()J

    move-result-wide v5

    sub-long/2addr v3, v5

    goto :goto_0
.end method

.method public getPreviousMediaItem(Lcom/android/videoeditor/service/MovieTransition;)Lcom/android/videoeditor/service/MovieMediaItem;
    .locals 5
    .param p1    # Lcom/android/videoeditor/service/MovieTransition;

    const/4 v3, 0x0

    iget-object v4, p0, Lcom/android/videoeditor/service/VideoEditorProject;->mMediaItems:Ljava/util/List;

    invoke-interface {v4}, Ljava/util/List;->size()I

    move-result v0

    const/4 v1, 0x0

    :goto_0
    if-ge v1, v0, :cond_2

    iget-object v4, p0, Lcom/android/videoeditor/service/VideoEditorProject;->mMediaItems:Ljava/util/List;

    invoke-interface {v4, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/android/videoeditor/service/MovieMediaItem;

    if-nez v1, :cond_1

    invoke-virtual {v2}, Lcom/android/videoeditor/service/MovieMediaItem;->getBeginTransition()Lcom/android/videoeditor/service/MovieTransition;

    move-result-object v4

    if-ne v4, p1, :cond_1

    move-object v2, v3

    :cond_0
    :goto_1
    return-object v2

    :cond_1
    invoke-virtual {v2}, Lcom/android/videoeditor/service/MovieMediaItem;->getEndTransition()Lcom/android/videoeditor/service/MovieTransition;

    move-result-object v4

    if-eq v4, p1, :cond_0

    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_2
    move-object v2, v3

    goto :goto_1
.end method

.method public getPreviousMediaItem(Ljava/lang/String;)Lcom/android/videoeditor/service/MovieMediaItem;
    .locals 4
    .param p1    # Ljava/lang/String;

    const/4 v2, 0x0

    iget-object v3, p0, Lcom/android/videoeditor/service/VideoEditorProject;->mMediaItems:Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/android/videoeditor/service/MovieMediaItem;

    invoke-virtual {v1}, Lcom/android/videoeditor/service/MovieMediaItem;->getId()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_1

    :cond_0
    return-object v2

    :cond_1
    move-object v2, v1

    goto :goto_0
.end method

.method public getProjectDuration()J
    .locals 2

    iget-wide v0, p0, Lcom/android/videoeditor/service/VideoEditorProject;->mProjectDurationMs:J

    return-wide v0
.end method

.method public getTheme()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/android/videoeditor/service/VideoEditorProject;->mTheme:Ljava/lang/String;

    return-object v0
.end method

.method public getTransition(Ljava/lang/String;)Lcom/android/videoeditor/service/MovieTransition;
    .locals 7
    .param p1    # Ljava/lang/String;

    const/4 v5, 0x0

    invoke-virtual {p0}, Lcom/android/videoeditor/service/VideoEditorProject;->getFirstMediaItem()Lcom/android/videoeditor/service/MovieMediaItem;

    move-result-object v2

    if-nez v2, :cond_1

    move-object v0, v5

    :cond_0
    :goto_0
    return-object v0

    :cond_1
    invoke-virtual {v2}, Lcom/android/videoeditor/service/MovieMediaItem;->getBeginTransition()Lcom/android/videoeditor/service/MovieTransition;

    move-result-object v0

    if-eqz v0, :cond_2

    invoke-virtual {v0}, Lcom/android/videoeditor/service/MovieTransition;->getId()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v6, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-nez v6, :cond_0

    :cond_2
    iget-object v6, p0, Lcom/android/videoeditor/service/VideoEditorProject;->mMediaItems:Ljava/util/List;

    invoke-interface {v6}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_3
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v6

    if-eqz v6, :cond_4

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/android/videoeditor/service/MovieMediaItem;

    invoke-virtual {v4}, Lcom/android/videoeditor/service/MovieMediaItem;->getEndTransition()Lcom/android/videoeditor/service/MovieTransition;

    move-result-object v1

    if-eqz v1, :cond_3

    invoke-virtual {v1}, Lcom/android/videoeditor/service/MovieTransition;->getId()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v6, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_3

    move-object v0, v1

    goto :goto_0

    :cond_4
    move-object v0, v5

    goto :goto_0
.end method

.method public getUniqueAspectRatiosList()Ljava/util/ArrayList;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iget-object v4, p0, Lcom/android/videoeditor/service/VideoEditorProject;->mMediaItems:Ljava/util/List;

    invoke-interface {v4}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_0
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/android/videoeditor/service/MovieMediaItem;

    invoke-virtual {v3}, Lcom/android/videoeditor/service/MovieMediaItem;->getAspectRatio()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v1, v4}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v1, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_1
    return-object v1
.end method

.method public getZoomLevel()I
    .locals 1

    iget v0, p0, Lcom/android/videoeditor/service/VideoEditorProject;->mZoomLevel:I

    return v0
.end method

.method public hasMultipleAspectRatios()Z
    .locals 4

    const/4 v0, 0x0

    iget-object v3, p0, Lcom/android/videoeditor/service/VideoEditorProject;->mMediaItems:Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_2

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/android/videoeditor/service/MovieMediaItem;

    if-nez v0, :cond_1

    invoke-virtual {v2}, Lcom/android/videoeditor/service/MovieMediaItem;->getAspectRatio()I

    move-result v0

    goto :goto_0

    :cond_1
    invoke-virtual {v2}, Lcom/android/videoeditor/service/MovieMediaItem;->getAspectRatio()I

    move-result v3

    if-eq v3, v0, :cond_0

    const/4 v3, 0x1

    :goto_1
    return v3

    :cond_2
    const/4 v3, 0x0

    goto :goto_1
.end method

.method insertMediaItem(Lcom/android/videoeditor/service/MovieMediaItem;Ljava/lang/String;)V
    .locals 8
    .param p1    # Lcom/android/videoeditor/service/MovieMediaItem;
    .param p2    # Ljava/lang/String;

    const/4 v7, 0x0

    const/4 v6, 0x0

    if-nez p2, :cond_1

    iget-object v4, p0, Lcom/android/videoeditor/service/VideoEditorProject;->mMediaItems:Ljava/util/List;

    invoke-interface {v4}, Ljava/util/List;->size()I

    move-result v4

    if-lez v4, :cond_0

    iget-object v4, p0, Lcom/android/videoeditor/service/VideoEditorProject;->mMediaItems:Ljava/util/List;

    invoke-interface {v4, v6}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/videoeditor/service/MovieMediaItem;

    invoke-virtual {v0}, Lcom/android/videoeditor/service/MovieMediaItem;->getBeginTransition()Lcom/android/videoeditor/service/MovieTransition;

    move-result-object v4

    if-eqz v4, :cond_0

    invoke-virtual {v0, v7}, Lcom/android/videoeditor/service/MovieMediaItem;->setBeginTransition(Lcom/android/videoeditor/service/MovieTransition;)V

    :cond_0
    iget-object v4, p0, Lcom/android/videoeditor/service/VideoEditorProject;->mMediaItems:Ljava/util/List;

    invoke-interface {v4, v6, p1}, Ljava/util/List;->add(ILjava/lang/Object;)V

    iput-boolean v6, p0, Lcom/android/videoeditor/service/VideoEditorProject;->mClean:Z

    :goto_0
    return-void

    :cond_1
    iget-object v4, p0, Lcom/android/videoeditor/service/VideoEditorProject;->mMediaItems:Ljava/util/List;

    invoke-interface {v4}, Ljava/util/List;->size()I

    move-result v2

    const/4 v1, 0x0

    :goto_1
    if-ge v1, v2, :cond_4

    iget-object v4, p0, Lcom/android/videoeditor/service/VideoEditorProject;->mMediaItems:Ljava/util/List;

    invoke-interface {v4, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/android/videoeditor/service/MovieMediaItem;

    invoke-virtual {v3}, Lcom/android/videoeditor/service/MovieMediaItem;->getId()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_3

    invoke-virtual {v3, v7}, Lcom/android/videoeditor/service/MovieMediaItem;->setEndTransition(Lcom/android/videoeditor/service/MovieTransition;)V

    add-int/lit8 v4, v2, -0x1

    if-ge v1, v4, :cond_2

    iget-object v4, p0, Lcom/android/videoeditor/service/VideoEditorProject;->mMediaItems:Ljava/util/List;

    add-int/lit8 v5, v1, 0x1

    invoke-interface {v4, v5}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/android/videoeditor/service/MovieMediaItem;

    invoke-virtual {v4, v7}, Lcom/android/videoeditor/service/MovieMediaItem;->setBeginTransition(Lcom/android/videoeditor/service/MovieTransition;)V

    :cond_2
    iget-object v4, p0, Lcom/android/videoeditor/service/VideoEditorProject;->mMediaItems:Ljava/util/List;

    add-int/lit8 v5, v1, 0x1

    invoke-interface {v4, v5, p1}, Ljava/util/List;->add(ILjava/lang/Object;)V

    iput-boolean v6, p0, Lcom/android/videoeditor/service/VideoEditorProject;->mClean:Z

    goto :goto_0

    :cond_3
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    :cond_4
    new-instance v4, Ljava/lang/IllegalArgumentException;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "MediaItem not found: "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-direct {v4, v5}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v4
.end method

.method public isClean()Z
    .locals 1

    iget-boolean v0, p0, Lcom/android/videoeditor/service/VideoEditorProject;->mClean:Z

    return v0
.end method

.method public isFirstMediaItem(Ljava/lang/String;)Z
    .locals 2
    .param p1    # Ljava/lang/String;

    invoke-virtual {p0}, Lcom/android/videoeditor/service/VideoEditorProject;->getFirstMediaItem()Lcom/android/videoeditor/service/MovieMediaItem;

    move-result-object v0

    if-nez v0, :cond_0

    const/4 v1, 0x0

    :goto_0
    return v1

    :cond_0
    invoke-virtual {v0}, Lcom/android/videoeditor/service/MovieMediaItem;->getId()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    goto :goto_0
.end method

.method public isLastMediaItem(Ljava/lang/String;)Z
    .locals 2
    .param p1    # Ljava/lang/String;

    invoke-virtual {p0}, Lcom/android/videoeditor/service/VideoEditorProject;->getLastMediaItem()Lcom/android/videoeditor/service/MovieMediaItem;

    move-result-object v0

    if-nez v0, :cond_0

    const/4 v1, 0x0

    :goto_0
    return v1

    :cond_0
    invoke-virtual {v0}, Lcom/android/videoeditor/service/MovieMediaItem;->getId()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    goto :goto_0
.end method

.method public release()V
    .locals 0

    return-void
.end method

.method removeAudioTrack(Ljava/lang/String;)V
    .locals 4
    .param p1    # Ljava/lang/String;

    iget-object v3, p0, Lcom/android/videoeditor/service/VideoEditorProject;->mAudioTracks:Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v1

    const/4 v2, 0x0

    :goto_0
    if-ge v2, v1, :cond_0

    iget-object v3, p0, Lcom/android/videoeditor/service/VideoEditorProject;->mAudioTracks:Ljava/util/List;

    invoke-interface {v3, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/videoeditor/service/MovieAudioTrack;

    invoke-virtual {v0}, Lcom/android/videoeditor/service/MovieAudioTrack;->getId()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_1

    iget-object v3, p0, Lcom/android/videoeditor/service/VideoEditorProject;->mAudioTracks:Ljava/util/List;

    invoke-interface {v3, v2}, Ljava/util/List;->remove(I)Ljava/lang/Object;

    const/4 v3, 0x0

    iput-boolean v3, p0, Lcom/android/videoeditor/service/VideoEditorProject;->mClean:Z

    :cond_0
    return-void

    :cond_1
    add-int/lit8 v2, v2, 0x1

    goto :goto_0
.end method

.method public removeDownload(Ljava/lang/String;)V
    .locals 6
    .param p1    # Ljava/lang/String;

    iget-object v5, p0, Lcom/android/videoeditor/service/VideoEditorProject;->mDownloads:Ljava/util/List;

    invoke-interface {v5}, Ljava/util/List;->size()I

    move-result v0

    const/4 v3, 0x0

    :goto_0
    if-ge v3, v0, :cond_0

    iget-object v5, p0, Lcom/android/videoeditor/service/VideoEditorProject;->mDownloads:Ljava/util/List;

    invoke-interface {v5, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/android/videoeditor/service/VideoEditorProject$Download;

    invoke-virtual {v1}, Lcom/android/videoeditor/service/VideoEditorProject$Download;->getMediaUri()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p1, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_1

    invoke-virtual {v1}, Lcom/android/videoeditor/service/VideoEditorProject$Download;->getFilename()Ljava/lang/String;

    move-result-object v2

    new-instance v5, Ljava/io/File;

    invoke-direct {v5, v2}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v5}, Ljava/io/File;->delete()Z

    iget-object v5, p0, Lcom/android/videoeditor/service/VideoEditorProject;->mDownloads:Ljava/util/List;

    invoke-interface {v5, v3}, Ljava/util/List;->remove(I)Ljava/lang/Object;

    const/4 v5, 0x0

    iput-boolean v5, p0, Lcom/android/videoeditor/service/VideoEditorProject;->mClean:Z

    :cond_0
    return-void

    :cond_1
    add-int/lit8 v3, v3, 0x1

    goto :goto_0
.end method

.method removeEffect(Ljava/lang/String;Ljava/lang/String;)V
    .locals 2
    .param p1    # Ljava/lang/String;
    .param p2    # Ljava/lang/String;

    invoke-virtual {p0, p1}, Lcom/android/videoeditor/service/VideoEditorProject;->getMediaItem(Ljava/lang/String;)Lcom/android/videoeditor/service/MovieMediaItem;

    move-result-object v0

    invoke-virtual {v0, p2}, Lcom/android/videoeditor/service/MovieMediaItem;->removeEffect(Ljava/lang/String;)V

    const/4 v1, 0x0

    iput-boolean v1, p0, Lcom/android/videoeditor/service/VideoEditorProject;->mClean:Z

    return-void
.end method

.method removeMediaItem(Ljava/lang/String;Lcom/android/videoeditor/service/MovieTransition;)V
    .locals 9
    .param p1    # Ljava/lang/String;
    .param p2    # Lcom/android/videoeditor/service/MovieTransition;

    const/4 v8, 0x0

    const/4 v5, 0x0

    iget-object v6, p0, Lcom/android/videoeditor/service/VideoEditorProject;->mMediaItems:Ljava/util/List;

    invoke-interface {v6}, Ljava/util/List;->size()I

    move-result v0

    const/4 v1, 0x0

    :goto_0
    if-ge v1, v0, :cond_0

    iget-object v6, p0, Lcom/android/videoeditor/service/VideoEditorProject;->mMediaItems:Ljava/util/List;

    invoke-interface {v6, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/android/videoeditor/service/MovieMediaItem;

    invoke-virtual {v2}, Lcom/android/videoeditor/service/MovieMediaItem;->getId()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v6, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_3

    iget-object v6, p0, Lcom/android/videoeditor/service/VideoEditorProject;->mMediaItems:Ljava/util/List;

    invoke-interface {v6, v1}, Ljava/util/List;->remove(I)Ljava/lang/Object;

    const/4 v6, 0x0

    iput-boolean v6, p0, Lcom/android/videoeditor/service/VideoEditorProject;->mClean:Z

    if-eqz p2, :cond_1

    invoke-virtual {p0, p2, v5}, Lcom/android/videoeditor/service/VideoEditorProject;->addTransition(Lcom/android/videoeditor/service/MovieTransition;Ljava/lang/String;)V

    :cond_0
    :goto_1
    return-void

    :cond_1
    if-lez v1, :cond_2

    iget-object v6, p0, Lcom/android/videoeditor/service/VideoEditorProject;->mMediaItems:Ljava/util/List;

    add-int/lit8 v7, v1, -0x1

    invoke-interface {v6, v7}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/android/videoeditor/service/MovieMediaItem;

    invoke-virtual {v4, v8}, Lcom/android/videoeditor/service/MovieMediaItem;->setEndTransition(Lcom/android/videoeditor/service/MovieTransition;)V

    :cond_2
    add-int/lit8 v6, v0, -0x1

    if-ge v1, v6, :cond_0

    iget-object v6, p0, Lcom/android/videoeditor/service/VideoEditorProject;->mMediaItems:Ljava/util/List;

    invoke-interface {v6, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/android/videoeditor/service/MovieMediaItem;

    invoke-virtual {v3, v8}, Lcom/android/videoeditor/service/MovieMediaItem;->setBeginTransition(Lcom/android/videoeditor/service/MovieTransition;)V

    goto :goto_1

    :cond_3
    invoke-virtual {v2}, Lcom/android/videoeditor/service/MovieMediaItem;->getId()Ljava/lang/String;

    move-result-object v5

    add-int/lit8 v1, v1, 0x1

    goto :goto_0
.end method

.method removeOverlay(Ljava/lang/String;Ljava/lang/String;)V
    .locals 2
    .param p1    # Ljava/lang/String;
    .param p2    # Ljava/lang/String;

    invoke-virtual {p0, p1}, Lcom/android/videoeditor/service/VideoEditorProject;->getMediaItem(Ljava/lang/String;)Lcom/android/videoeditor/service/MovieMediaItem;

    move-result-object v0

    invoke-virtual {v0, p2}, Lcom/android/videoeditor/service/MovieMediaItem;->removeOverlay(Ljava/lang/String;)V

    const/4 v1, 0x0

    iput-boolean v1, p0, Lcom/android/videoeditor/service/VideoEditorProject;->mClean:Z

    return-void
.end method

.method removeTransition(Ljava/lang/String;)V
    .locals 7
    .param p1    # Ljava/lang/String;

    const/4 v6, 0x0

    iget-object v5, p0, Lcom/android/videoeditor/service/VideoEditorProject;->mMediaItems:Ljava/util/List;

    invoke-interface {v5}, Ljava/util/List;->size()I

    move-result v1

    const/4 v3, 0x0

    :goto_0
    if-ge v3, v1, :cond_0

    iget-object v5, p0, Lcom/android/videoeditor/service/VideoEditorProject;->mMediaItems:Ljava/util/List;

    invoke-interface {v5, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/android/videoeditor/service/MovieMediaItem;

    invoke-virtual {v4}, Lcom/android/videoeditor/service/MovieMediaItem;->getBeginTransition()Lcom/android/videoeditor/service/MovieTransition;

    move-result-object v0

    if-eqz v0, :cond_1

    invoke-virtual {v0}, Lcom/android/videoeditor/service/MovieTransition;->getId()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v5, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_1

    invoke-virtual {v4, v6}, Lcom/android/videoeditor/service/MovieMediaItem;->setBeginTransition(Lcom/android/videoeditor/service/MovieTransition;)V

    :cond_0
    const/4 v5, 0x0

    iput-boolean v5, p0, Lcom/android/videoeditor/service/VideoEditorProject;->mClean:Z

    return-void

    :cond_1
    invoke-virtual {v4}, Lcom/android/videoeditor/service/MovieMediaItem;->getEndTransition()Lcom/android/videoeditor/service/MovieTransition;

    move-result-object v2

    if-eqz v2, :cond_2

    invoke-virtual {v2}, Lcom/android/videoeditor/service/MovieTransition;->getId()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v5, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_2

    invoke-virtual {v4, v6}, Lcom/android/videoeditor/service/MovieMediaItem;->setEndTransition(Lcom/android/videoeditor/service/MovieTransition;)V

    :cond_2
    add-int/lit8 v3, v3, 0x1

    goto :goto_0
.end method

.method public renderMediaItemFrame(Landroid/view/SurfaceHolder;Ljava/lang/String;J)J
    .locals 3
    .param p1    # Landroid/view/SurfaceHolder;
    .param p2    # Ljava/lang/String;
    .param p3    # J

    iget-object v1, p0, Lcom/android/videoeditor/service/VideoEditorProject;->mVideoEditor:Landroid/media/videoeditor/VideoEditor;

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/android/videoeditor/service/VideoEditorProject;->mVideoEditor:Landroid/media/videoeditor/VideoEditor;

    invoke-interface {v1, p2}, Landroid/media/videoeditor/VideoEditor;->getMediaItem(Ljava/lang/String;)Landroid/media/videoeditor/MediaItem;

    move-result-object v0

    check-cast v0, Landroid/media/videoeditor/MediaVideoItem;

    if-eqz v0, :cond_0

    invoke-virtual {v0, p1, p3, p4}, Landroid/media/videoeditor/MediaVideoItem;->renderFrame(Landroid/view/SurfaceHolder;J)J

    move-result-wide v1

    :goto_0
    return-wide v1

    :cond_0
    const-wide/16 v1, -0x1

    goto :goto_0

    :cond_1
    const-wide/16 v1, 0x0

    goto :goto_0
.end method

.method public renderPreviewFrame(Landroid/view/SurfaceHolder;JLandroid/media/videoeditor/VideoEditor$OverlayData;)J
    .locals 2
    .param p1    # Landroid/view/SurfaceHolder;
    .param p2    # J
    .param p4    # Landroid/media/videoeditor/VideoEditor$OverlayData;

    iget-object v0, p0, Lcom/android/videoeditor/service/VideoEditorProject;->mVideoEditor:Landroid/media/videoeditor/VideoEditor;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/videoeditor/service/VideoEditorProject;->mVideoEditor:Landroid/media/videoeditor/VideoEditor;

    invoke-interface {v0, p1, p2, p3, p4}, Landroid/media/videoeditor/VideoEditor;->renderPreviewFrame(Landroid/view/SurfaceHolder;JLandroid/media/videoeditor/VideoEditor$OverlayData;)J

    move-result-wide v0

    :goto_0
    return-wide v0

    :cond_0
    const-wide/16 v0, 0x0

    goto :goto_0
.end method

.method public saveToXml()V
    .locals 9
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-static {}, Landroid/util/Xml;->newSerializer()Lorg/xmlpull/v1/XmlSerializer;

    move-result-object v3

    new-instance v4, Ljava/io/StringWriter;

    invoke-direct {v4}, Ljava/io/StringWriter;-><init>()V

    invoke-interface {v3, v4}, Lorg/xmlpull/v1/XmlSerializer;->setOutput(Ljava/io/Writer;)V

    const-string v5, "UTF-8"

    const/4 v6, 0x1

    invoke-static {v6}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v6

    invoke-interface {v3, v5, v6}, Lorg/xmlpull/v1/XmlSerializer;->startDocument(Ljava/lang/String;Ljava/lang/Boolean;)V

    const-string v5, ""

    const-string v6, "project"

    invoke-interface {v3, v5, v6}, Lorg/xmlpull/v1/XmlSerializer;->startTag(Ljava/lang/String;Ljava/lang/String;)Lorg/xmlpull/v1/XmlSerializer;

    iget-object v5, p0, Lcom/android/videoeditor/service/VideoEditorProject;->mProjectName:Ljava/lang/String;

    if-eqz v5, :cond_0

    const-string v5, ""

    const-string v6, "name"

    iget-object v7, p0, Lcom/android/videoeditor/service/VideoEditorProject;->mProjectName:Ljava/lang/String;

    invoke-interface {v3, v5, v6, v7}, Lorg/xmlpull/v1/XmlSerializer;->attribute(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lorg/xmlpull/v1/XmlSerializer;

    :cond_0
    iget-object v5, p0, Lcom/android/videoeditor/service/VideoEditorProject;->mTheme:Ljava/lang/String;

    if-eqz v5, :cond_1

    const-string v5, ""

    const-string v6, "theme"

    iget-object v7, p0, Lcom/android/videoeditor/service/VideoEditorProject;->mTheme:Ljava/lang/String;

    invoke-interface {v3, v5, v6, v7}, Lorg/xmlpull/v1/XmlSerializer;->attribute(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lorg/xmlpull/v1/XmlSerializer;

    :cond_1
    const-string v5, ""

    const-string v6, "playhead"

    iget-wide v7, p0, Lcom/android/videoeditor/service/VideoEditorProject;->mPlayheadPosMs:J

    invoke-static {v7, v8}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v7

    invoke-interface {v3, v5, v6, v7}, Lorg/xmlpull/v1/XmlSerializer;->attribute(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lorg/xmlpull/v1/XmlSerializer;

    const-string v5, ""

    const-string v6, "duration"

    invoke-virtual {p0}, Lcom/android/videoeditor/service/VideoEditorProject;->computeDuration()J

    move-result-wide v7

    invoke-static {v7, v8}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v7

    invoke-interface {v3, v5, v6, v7}, Lorg/xmlpull/v1/XmlSerializer;->attribute(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lorg/xmlpull/v1/XmlSerializer;

    const-string v5, ""

    const-string v6, "zoom_level"

    iget v7, p0, Lcom/android/videoeditor/service/VideoEditorProject;->mZoomLevel:I

    invoke-static {v7}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v7

    invoke-interface {v3, v5, v6, v7}, Lorg/xmlpull/v1/XmlSerializer;->attribute(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lorg/xmlpull/v1/XmlSerializer;

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v5

    iput-wide v5, p0, Lcom/android/videoeditor/service/VideoEditorProject;->mLastSaved:J

    const-string v5, ""

    const-string v6, "saved"

    iget-wide v7, p0, Lcom/android/videoeditor/service/VideoEditorProject;->mLastSaved:J

    invoke-static {v7, v8}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v7

    invoke-interface {v3, v5, v6, v7}, Lorg/xmlpull/v1/XmlSerializer;->attribute(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lorg/xmlpull/v1/XmlSerializer;

    iget-object v5, p0, Lcom/android/videoeditor/service/VideoEditorProject;->mExportedMovieUri:Landroid/net/Uri;

    if-eqz v5, :cond_2

    const-string v5, ""

    const-string v6, "movie"

    invoke-interface {v3, v5, v6}, Lorg/xmlpull/v1/XmlSerializer;->startTag(Ljava/lang/String;Ljava/lang/String;)Lorg/xmlpull/v1/XmlSerializer;

    const-string v5, ""

    const-string v6, "uri"

    iget-object v7, p0, Lcom/android/videoeditor/service/VideoEditorProject;->mExportedMovieUri:Landroid/net/Uri;

    invoke-virtual {v7}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-interface {v3, v5, v6, v7}, Lorg/xmlpull/v1/XmlSerializer;->attribute(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lorg/xmlpull/v1/XmlSerializer;

    const-string v5, ""

    const-string v6, "movie"

    invoke-interface {v3, v5, v6}, Lorg/xmlpull/v1/XmlSerializer;->endTag(Ljava/lang/String;Ljava/lang/String;)Lorg/xmlpull/v1/XmlSerializer;

    :cond_2
    iget-object v5, p0, Lcom/android/videoeditor/service/VideoEditorProject;->mDownloads:Ljava/util/List;

    invoke-interface {v5}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_3

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/videoeditor/service/VideoEditorProject$Download;

    const-string v5, ""

    const-string v6, "download"

    invoke-interface {v3, v5, v6}, Lorg/xmlpull/v1/XmlSerializer;->startTag(Ljava/lang/String;Ljava/lang/String;)Lorg/xmlpull/v1/XmlSerializer;

    const-string v5, ""

    const-string v6, "uri"

    invoke-virtual {v0}, Lcom/android/videoeditor/service/VideoEditorProject$Download;->getMediaUri()Ljava/lang/String;

    move-result-object v7

    invoke-interface {v3, v5, v6, v7}, Lorg/xmlpull/v1/XmlSerializer;->attribute(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lorg/xmlpull/v1/XmlSerializer;

    const-string v5, ""

    const-string v6, "mime"

    invoke-virtual {v0}, Lcom/android/videoeditor/service/VideoEditorProject$Download;->getMimeType()Ljava/lang/String;

    move-result-object v7

    invoke-interface {v3, v5, v6, v7}, Lorg/xmlpull/v1/XmlSerializer;->attribute(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lorg/xmlpull/v1/XmlSerializer;

    const-string v5, ""

    const-string v6, "filename"

    invoke-virtual {v0}, Lcom/android/videoeditor/service/VideoEditorProject$Download;->getFilename()Ljava/lang/String;

    move-result-object v7

    invoke-interface {v3, v5, v6, v7}, Lorg/xmlpull/v1/XmlSerializer;->attribute(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lorg/xmlpull/v1/XmlSerializer;

    const-string v5, ""

    const-string v6, "time"

    invoke-virtual {v0}, Lcom/android/videoeditor/service/VideoEditorProject$Download;->getTime()J

    move-result-wide v7

    invoke-static {v7, v8}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v7

    invoke-interface {v3, v5, v6, v7}, Lorg/xmlpull/v1/XmlSerializer;->attribute(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lorg/xmlpull/v1/XmlSerializer;

    const-string v5, ""

    const-string v6, "download"

    invoke-interface {v3, v5, v6}, Lorg/xmlpull/v1/XmlSerializer;->endTag(Ljava/lang/String;Ljava/lang/String;)Lorg/xmlpull/v1/XmlSerializer;

    goto :goto_0

    :cond_3
    const-string v5, ""

    const-string v6, "project"

    invoke-interface {v3, v5, v6}, Lorg/xmlpull/v1/XmlSerializer;->endTag(Ljava/lang/String;Ljava/lang/String;)Lorg/xmlpull/v1/XmlSerializer;

    invoke-interface {v3}, Lorg/xmlpull/v1/XmlSerializer;->endDocument()V

    new-instance v2, Ljava/io/FileOutputStream;

    new-instance v5, Ljava/io/File;

    iget-object v6, p0, Lcom/android/videoeditor/service/VideoEditorProject;->mVideoEditor:Landroid/media/videoeditor/VideoEditor;

    invoke-interface {v6}, Landroid/media/videoeditor/VideoEditor;->getPath()Ljava/lang/String;

    move-result-object v6

    const-string v7, "metadata.xml"

    invoke-direct {v5, v6, v7}, Ljava/io/File;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-direct {v2, v5}, Ljava/io/FileOutputStream;-><init>(Ljava/io/File;)V

    invoke-virtual {v4}, Ljava/io/StringWriter;->toString()Ljava/lang/String;

    move-result-object v5

    const-string v6, "UTF-8"

    invoke-virtual {v5, v6}, Ljava/lang/String;->getBytes(Ljava/lang/String;)[B

    move-result-object v5

    invoke-virtual {v2, v5}, Ljava/io/FileOutputStream;->write([B)V

    invoke-virtual {v2}, Ljava/io/FileOutputStream;->flush()V

    invoke-virtual {v2}, Ljava/io/FileOutputStream;->close()V

    return-void
.end method

.method setAspectRatio(I)V
    .locals 1
    .param p1    # I

    iput p1, p0, Lcom/android/videoeditor/service/VideoEditorProject;->mAspectRatio:I

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/android/videoeditor/service/VideoEditorProject;->mClean:Z

    return-void
.end method

.method setAudioTracks(Ljava/util/List;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/android/videoeditor/service/MovieAudioTrack;",
            ">;)V"
        }
    .end annotation

    iput-object p1, p0, Lcom/android/videoeditor/service/VideoEditorProject;->mAudioTracks:Ljava/util/List;

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/android/videoeditor/service/VideoEditorProject;->mClean:Z

    return-void
.end method

.method public setClean(Z)V
    .locals 0
    .param p1    # Z

    iput-boolean p1, p0, Lcom/android/videoeditor/service/VideoEditorProject;->mClean:Z

    return-void
.end method

.method setMediaItems(Ljava/util/List;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/android/videoeditor/service/MovieMediaItem;",
            ">;)V"
        }
    .end annotation

    iput-object p1, p0, Lcom/android/videoeditor/service/VideoEditorProject;->mMediaItems:Ljava/util/List;

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/android/videoeditor/service/VideoEditorProject;->mClean:Z

    return-void
.end method

.method public setPlayheadPos(J)V
    .locals 0
    .param p1    # J

    iput-wide p1, p0, Lcom/android/videoeditor/service/VideoEditorProject;->mPlayheadPosMs:J

    return-void
.end method

.method public setProjectName(Ljava/lang/String;)V
    .locals 1
    .param p1    # Ljava/lang/String;

    iput-object p1, p0, Lcom/android/videoeditor/service/VideoEditorProject;->mProjectName:Ljava/lang/String;

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/android/videoeditor/service/VideoEditorProject;->mClean:Z

    return-void
.end method

.method setTheme(Ljava/lang/String;)V
    .locals 1
    .param p1    # Ljava/lang/String;

    iput-object p1, p0, Lcom/android/videoeditor/service/VideoEditorProject;->mTheme:Ljava/lang/String;

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/android/videoeditor/service/VideoEditorProject;->mClean:Z

    return-void
.end method

.method public setZoomLevel(I)V
    .locals 0
    .param p1    # I

    iput p1, p0, Lcom/android/videoeditor/service/VideoEditorProject;->mZoomLevel:I

    return-void
.end method

.method public startPreview(Landroid/view/SurfaceHolder;JJZILandroid/media/videoeditor/VideoEditor$PreviewProgressListener;)V
    .locals 9
    .param p1    # Landroid/view/SurfaceHolder;
    .param p2    # J
    .param p4    # J
    .param p6    # Z
    .param p7    # I
    .param p8    # Landroid/media/videoeditor/VideoEditor$PreviewProgressListener;

    iget-object v0, p0, Lcom/android/videoeditor/service/VideoEditorProject;->mVideoEditor:Landroid/media/videoeditor/VideoEditor;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/videoeditor/service/VideoEditorProject;->mVideoEditor:Landroid/media/videoeditor/VideoEditor;

    move-object v1, p1

    move-wide v2, p2

    move-wide v4, p4

    move v6, p6

    move/from16 v7, p7

    move-object/from16 v8, p8

    invoke-interface/range {v0 .. v8}, Landroid/media/videoeditor/VideoEditor;->startPreview(Landroid/view/SurfaceHolder;JJZILandroid/media/videoeditor/VideoEditor$PreviewProgressListener;)V

    :cond_0
    return-void
.end method

.method public stopPreview()J
    .locals 2

    iget-object v0, p0, Lcom/android/videoeditor/service/VideoEditorProject;->mVideoEditor:Landroid/media/videoeditor/VideoEditor;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/videoeditor/service/VideoEditorProject;->mVideoEditor:Landroid/media/videoeditor/VideoEditor;

    invoke-interface {v0}, Landroid/media/videoeditor/VideoEditor;->stopPreview()J

    move-result-wide v0

    :goto_0
    return-wide v0

    :cond_0
    const-wide/16 v0, 0x0

    goto :goto_0
.end method

.method updateMediaItem(Lcom/android/videoeditor/service/MovieMediaItem;)V
    .locals 8
    .param p1    # Lcom/android/videoeditor/service/MovieMediaItem;

    invoke-virtual {p1}, Lcom/android/videoeditor/service/MovieMediaItem;->getId()Ljava/lang/String;

    move-result-object v3

    iget-object v6, p0, Lcom/android/videoeditor/service/VideoEditorProject;->mMediaItems:Ljava/util/List;

    invoke-interface {v6}, Ljava/util/List;->size()I

    move-result v0

    const/4 v1, 0x0

    :goto_0
    if-ge v1, v0, :cond_1

    iget-object v6, p0, Lcom/android/videoeditor/service/VideoEditorProject;->mMediaItems:Ljava/util/List;

    invoke-interface {v6, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/android/videoeditor/service/MovieMediaItem;

    invoke-virtual {v2}, Lcom/android/videoeditor/service/MovieMediaItem;->getId()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v6, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_2

    iget-object v6, p0, Lcom/android/videoeditor/service/VideoEditorProject;->mMediaItems:Ljava/util/List;

    invoke-interface {v6, v1, p1}, Ljava/util/List;->set(ILjava/lang/Object;)Ljava/lang/Object;

    const/4 v6, 0x0

    iput-boolean v6, p0, Lcom/android/videoeditor/service/VideoEditorProject;->mClean:Z

    if-lez v1, :cond_0

    iget-object v6, p0, Lcom/android/videoeditor/service/VideoEditorProject;->mMediaItems:Ljava/util/List;

    add-int/lit8 v7, v1, -0x1

    invoke-interface {v6, v7}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/android/videoeditor/service/MovieMediaItem;

    invoke-virtual {p1}, Lcom/android/videoeditor/service/MovieMediaItem;->getBeginTransition()Lcom/android/videoeditor/service/MovieTransition;

    move-result-object v6

    invoke-virtual {v5, v6}, Lcom/android/videoeditor/service/MovieMediaItem;->setEndTransition(Lcom/android/videoeditor/service/MovieTransition;)V

    :cond_0
    add-int/lit8 v6, v0, -0x1

    if-ge v1, v6, :cond_1

    iget-object v6, p0, Lcom/android/videoeditor/service/VideoEditorProject;->mMediaItems:Ljava/util/List;

    add-int/lit8 v7, v1, 0x1

    invoke-interface {v6, v7}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/android/videoeditor/service/MovieMediaItem;

    invoke-virtual {p1}, Lcom/android/videoeditor/service/MovieMediaItem;->getEndTransition()Lcom/android/videoeditor/service/MovieTransition;

    move-result-object v6

    invoke-virtual {v4, v6}, Lcom/android/videoeditor/service/MovieMediaItem;->setBeginTransition(Lcom/android/videoeditor/service/MovieTransition;)V

    :cond_1
    return-void

    :cond_2
    add-int/lit8 v1, v1, 0x1

    goto :goto_0
.end method
