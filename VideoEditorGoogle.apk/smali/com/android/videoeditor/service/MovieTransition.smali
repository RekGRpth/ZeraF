.class public Lcom/android/videoeditor/service/MovieTransition;
.super Ljava/lang/Object;
.source "MovieTransition.java"


# instance fields
.field private final mAlphaInvert:Z

.field private final mAlphaMaskBlendingPercent:I

.field private final mAlphaMaskFilename:Ljava/lang/String;

.field private final mAlphaMaskResId:I

.field private mAppDurationMs:J

.field private final mBehavior:I

.field private mDurationMs:J

.field private final mSlidingDirection:I

.field private final mType:I

.field private final mTypeClass:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<*>;"
        }
    .end annotation
.end field

.field private final mUniqueId:Ljava/lang/String;


# direct methods
.method constructor <init>(Landroid/media/videoeditor/Transition;)V
    .locals 4
    .param p1    # Landroid/media/videoeditor/Transition;

    const/4 v3, 0x0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    iput-object v1, p0, Lcom/android/videoeditor/service/MovieTransition;->mTypeClass:Ljava/lang/Class;

    invoke-virtual {p1}, Landroid/media/videoeditor/Transition;->getId()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/android/videoeditor/service/MovieTransition;->mUniqueId:Ljava/lang/String;

    invoke-virtual {p1}, Landroid/media/videoeditor/Transition;->getDuration()J

    move-result-wide v1

    iput-wide v1, p0, Lcom/android/videoeditor/service/MovieTransition;->mDurationMs:J

    iput-wide v1, p0, Lcom/android/videoeditor/service/MovieTransition;->mAppDurationMs:J

    invoke-virtual {p1}, Landroid/media/videoeditor/Transition;->getBehavior()I

    move-result v1

    iput v1, p0, Lcom/android/videoeditor/service/MovieTransition;->mBehavior:I

    instance-of v1, p1, Landroid/media/videoeditor/TransitionSliding;

    if-eqz v1, :cond_0

    move-object v1, p1

    check-cast v1, Landroid/media/videoeditor/TransitionSliding;

    invoke-virtual {v1}, Landroid/media/videoeditor/TransitionSliding;->getDirection()I

    move-result v1

    iput v1, p0, Lcom/android/videoeditor/service/MovieTransition;->mSlidingDirection:I

    :goto_0
    instance-of v1, p1, Landroid/media/videoeditor/TransitionAlpha;

    if-eqz v1, :cond_1

    move-object v0, p1

    check-cast v0, Landroid/media/videoeditor/TransitionAlpha;

    invoke-virtual {v0}, Landroid/media/videoeditor/TransitionAlpha;->getMaskFilename()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/android/videoeditor/service/MovieTransition;->mAlphaMaskFilename:Ljava/lang/String;

    iput v3, p0, Lcom/android/videoeditor/service/MovieTransition;->mAlphaMaskResId:I

    invoke-virtual {v0}, Landroid/media/videoeditor/TransitionAlpha;->getBlendingPercent()I

    move-result v1

    iput v1, p0, Lcom/android/videoeditor/service/MovieTransition;->mAlphaMaskBlendingPercent:I

    invoke-virtual {v0}, Landroid/media/videoeditor/TransitionAlpha;->isInvert()Z

    move-result v1

    iput-boolean v1, p0, Lcom/android/videoeditor/service/MovieTransition;->mAlphaInvert:Z

    :goto_1
    invoke-direct {p0}, Lcom/android/videoeditor/service/MovieTransition;->toType()I

    move-result v1

    iput v1, p0, Lcom/android/videoeditor/service/MovieTransition;->mType:I

    return-void

    :cond_0
    const/4 v1, -0x1

    iput v1, p0, Lcom/android/videoeditor/service/MovieTransition;->mSlidingDirection:I

    goto :goto_0

    :cond_1
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/android/videoeditor/service/MovieTransition;->mAlphaMaskFilename:Ljava/lang/String;

    iput v3, p0, Lcom/android/videoeditor/service/MovieTransition;->mAlphaMaskResId:I

    iput v3, p0, Lcom/android/videoeditor/service/MovieTransition;->mAlphaMaskBlendingPercent:I

    iput-boolean v3, p0, Lcom/android/videoeditor/service/MovieTransition;->mAlphaInvert:Z

    goto :goto_1
.end method

.method constructor <init>(Ljava/lang/Class;Ljava/lang/String;JI)V
    .locals 2
    .param p2    # Ljava/lang/String;
    .param p3    # J
    .param p5    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Class",
            "<*>;",
            "Ljava/lang/String;",
            "JI)V"
        }
    .end annotation

    const/4 v1, 0x0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/android/videoeditor/service/MovieTransition;->mTypeClass:Ljava/lang/Class;

    iput-object p2, p0, Lcom/android/videoeditor/service/MovieTransition;->mUniqueId:Ljava/lang/String;

    iput-wide p3, p0, Lcom/android/videoeditor/service/MovieTransition;->mDurationMs:J

    iput-wide p3, p0, Lcom/android/videoeditor/service/MovieTransition;->mAppDurationMs:J

    iput p5, p0, Lcom/android/videoeditor/service/MovieTransition;->mBehavior:I

    const/4 v0, -0x1

    iput v0, p0, Lcom/android/videoeditor/service/MovieTransition;->mSlidingDirection:I

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/android/videoeditor/service/MovieTransition;->mAlphaMaskFilename:Ljava/lang/String;

    iput v1, p0, Lcom/android/videoeditor/service/MovieTransition;->mAlphaMaskResId:I

    iput v1, p0, Lcom/android/videoeditor/service/MovieTransition;->mAlphaMaskBlendingPercent:I

    iput-boolean v1, p0, Lcom/android/videoeditor/service/MovieTransition;->mAlphaInvert:Z

    invoke-direct {p0}, Lcom/android/videoeditor/service/MovieTransition;->toType()I

    move-result v0

    iput v0, p0, Lcom/android/videoeditor/service/MovieTransition;->mType:I

    return-void
.end method

.method constructor <init>(Ljava/lang/Class;Ljava/lang/String;JII)V
    .locals 2
    .param p2    # Ljava/lang/String;
    .param p3    # J
    .param p5    # I
    .param p6    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Class",
            "<*>;",
            "Ljava/lang/String;",
            "JII)V"
        }
    .end annotation

    const/4 v1, 0x0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/android/videoeditor/service/MovieTransition;->mTypeClass:Ljava/lang/Class;

    iput-object p2, p0, Lcom/android/videoeditor/service/MovieTransition;->mUniqueId:Ljava/lang/String;

    iput-wide p3, p0, Lcom/android/videoeditor/service/MovieTransition;->mDurationMs:J

    iput-wide p3, p0, Lcom/android/videoeditor/service/MovieTransition;->mAppDurationMs:J

    iput p5, p0, Lcom/android/videoeditor/service/MovieTransition;->mBehavior:I

    iput p6, p0, Lcom/android/videoeditor/service/MovieTransition;->mSlidingDirection:I

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/android/videoeditor/service/MovieTransition;->mAlphaMaskFilename:Ljava/lang/String;

    iput v1, p0, Lcom/android/videoeditor/service/MovieTransition;->mAlphaMaskResId:I

    iput v1, p0, Lcom/android/videoeditor/service/MovieTransition;->mAlphaMaskBlendingPercent:I

    iput-boolean v1, p0, Lcom/android/videoeditor/service/MovieTransition;->mAlphaInvert:Z

    invoke-direct {p0}, Lcom/android/videoeditor/service/MovieTransition;->toType()I

    move-result v0

    iput v0, p0, Lcom/android/videoeditor/service/MovieTransition;->mType:I

    return-void
.end method

.method constructor <init>(Ljava/lang/Class;Ljava/lang/String;JIIIZ)V
    .locals 1
    .param p2    # Ljava/lang/String;
    .param p3    # J
    .param p5    # I
    .param p6    # I
    .param p7    # I
    .param p8    # Z
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Class",
            "<*>;",
            "Ljava/lang/String;",
            "JIIIZ)V"
        }
    .end annotation

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/android/videoeditor/service/MovieTransition;->mTypeClass:Ljava/lang/Class;

    iput-object p2, p0, Lcom/android/videoeditor/service/MovieTransition;->mUniqueId:Ljava/lang/String;

    iput-wide p3, p0, Lcom/android/videoeditor/service/MovieTransition;->mDurationMs:J

    iput-wide p3, p0, Lcom/android/videoeditor/service/MovieTransition;->mAppDurationMs:J

    iput p5, p0, Lcom/android/videoeditor/service/MovieTransition;->mBehavior:I

    const/4 v0, -0x1

    iput v0, p0, Lcom/android/videoeditor/service/MovieTransition;->mSlidingDirection:I

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/android/videoeditor/service/MovieTransition;->mAlphaMaskFilename:Ljava/lang/String;

    iput p6, p0, Lcom/android/videoeditor/service/MovieTransition;->mAlphaMaskResId:I

    iput p7, p0, Lcom/android/videoeditor/service/MovieTransition;->mAlphaMaskBlendingPercent:I

    iput-boolean p8, p0, Lcom/android/videoeditor/service/MovieTransition;->mAlphaInvert:Z

    invoke-direct {p0}, Lcom/android/videoeditor/service/MovieTransition;->toType()I

    move-result v0

    iput v0, p0, Lcom/android/videoeditor/service/MovieTransition;->mType:I

    return-void
.end method

.method private toType()I
    .locals 4

    const-class v1, Landroid/media/videoeditor/TransitionCrossfade;

    iget-object v2, p0, Lcom/android/videoeditor/service/MovieTransition;->mTypeClass:Ljava/lang/Class;

    invoke-virtual {v1, v2}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v1, 0x2

    :goto_0
    return v1

    :cond_0
    const-class v1, Landroid/media/videoeditor/TransitionAlpha;

    iget-object v2, p0, Lcom/android/videoeditor/service/MovieTransition;->mTypeClass:Ljava/lang/Class;

    invoke-virtual {v1, v2}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/android/videoeditor/service/MovieTransition;->mAlphaMaskFilename:Ljava/lang/String;

    invoke-static {v1}, Lcom/android/videoeditor/util/FileUtils;->getMaskRawId(Ljava/lang/String;)I

    move-result v0

    packed-switch v0, :pswitch_data_0

    new-instance v1, Ljava/lang/IllegalArgumentException;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Unknown id for: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/android/videoeditor/service/MovieTransition;->mAlphaMaskFilename:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v1

    :pswitch_0
    const/4 v1, 0x0

    goto :goto_0

    :pswitch_1
    const/4 v1, 0x1

    goto :goto_0

    :cond_1
    const-class v1, Landroid/media/videoeditor/TransitionFadeBlack;

    iget-object v2, p0, Lcom/android/videoeditor/service/MovieTransition;->mTypeClass:Ljava/lang/Class;

    invoke-virtual {v1, v2}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    const/4 v1, 0x3

    goto :goto_0

    :cond_2
    const-class v1, Landroid/media/videoeditor/TransitionSliding;

    iget-object v2, p0, Lcom/android/videoeditor/service/MovieTransition;->mTypeClass:Ljava/lang/Class;

    invoke-virtual {v1, v2}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_3

    iget v1, p0, Lcom/android/videoeditor/service/MovieTransition;->mSlidingDirection:I

    packed-switch v1, :pswitch_data_1

    new-instance v1, Ljava/lang/IllegalArgumentException;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Unknown direction: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget v3, p0, Lcom/android/videoeditor/service/MovieTransition;->mSlidingDirection:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v1

    :pswitch_2
    const/4 v1, 0x7

    goto :goto_0

    :pswitch_3
    const/4 v1, 0x5

    goto :goto_0

    :pswitch_4
    const/4 v1, 0x4

    goto :goto_0

    :pswitch_5
    const/4 v1, 0x6

    goto :goto_0

    :cond_3
    new-instance v1, Ljava/lang/IllegalArgumentException;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Unknown type: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/android/videoeditor/service/MovieTransition;->mTypeClass:Ljava/lang/Class;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v1

    nop

    :pswitch_data_0
    .packed-switch 0x7f050000
        :pswitch_0
        :pswitch_1
    .end packed-switch

    :pswitch_data_1
    .packed-switch 0x0
        :pswitch_4
        :pswitch_3
        :pswitch_5
        :pswitch_2
    .end packed-switch
.end method


# virtual methods
.method buildTransition(Landroid/content/Context;Landroid/media/videoeditor/MediaItem;Landroid/media/videoeditor/MediaItem;)Landroid/media/videoeditor/Transition;
    .locals 10
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/media/videoeditor/MediaItem;
    .param p3    # Landroid/media/videoeditor/MediaItem;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    const-class v0, Landroid/media/videoeditor/TransitionCrossfade;

    iget-object v1, p0, Lcom/android/videoeditor/service/MovieTransition;->mTypeClass:Ljava/lang/Class;

    invoke-virtual {v0, v1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    new-instance v0, Landroid/media/videoeditor/TransitionCrossfade;

    invoke-static {}, Lcom/android/videoeditor/service/ApiService;->generateId()Ljava/lang/String;

    move-result-object v1

    iget-wide v4, p0, Lcom/android/videoeditor/service/MovieTransition;->mDurationMs:J

    iget v6, p0, Lcom/android/videoeditor/service/MovieTransition;->mBehavior:I

    move-object v2, p2

    move-object v3, p3

    invoke-direct/range {v0 .. v6}, Landroid/media/videoeditor/TransitionCrossfade;-><init>(Ljava/lang/String;Landroid/media/videoeditor/MediaItem;Landroid/media/videoeditor/MediaItem;JI)V

    :goto_0
    return-object v0

    :cond_0
    const-class v0, Landroid/media/videoeditor/TransitionAlpha;

    iget-object v1, p0, Lcom/android/videoeditor/service/MovieTransition;->mTypeClass:Ljava/lang/Class;

    invoke-virtual {v0, v1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    new-instance v0, Landroid/media/videoeditor/TransitionAlpha;

    invoke-static {}, Lcom/android/videoeditor/service/ApiService;->generateId()Ljava/lang/String;

    move-result-object v1

    iget-wide v4, p0, Lcom/android/videoeditor/service/MovieTransition;->mDurationMs:J

    iget v6, p0, Lcom/android/videoeditor/service/MovieTransition;->mBehavior:I

    iget v2, p0, Lcom/android/videoeditor/service/MovieTransition;->mAlphaMaskResId:I

    invoke-static {p1, v2}, Lcom/android/videoeditor/util/FileUtils;->getMaskFilename(Landroid/content/Context;I)Ljava/lang/String;

    move-result-object v7

    iget v8, p0, Lcom/android/videoeditor/service/MovieTransition;->mAlphaMaskBlendingPercent:I

    iget-boolean v9, p0, Lcom/android/videoeditor/service/MovieTransition;->mAlphaInvert:Z

    move-object v2, p2

    move-object v3, p3

    invoke-direct/range {v0 .. v9}, Landroid/media/videoeditor/TransitionAlpha;-><init>(Ljava/lang/String;Landroid/media/videoeditor/MediaItem;Landroid/media/videoeditor/MediaItem;JILjava/lang/String;IZ)V

    goto :goto_0

    :cond_1
    const-class v0, Landroid/media/videoeditor/TransitionFadeBlack;

    iget-object v1, p0, Lcom/android/videoeditor/service/MovieTransition;->mTypeClass:Ljava/lang/Class;

    invoke-virtual {v0, v1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    new-instance v0, Landroid/media/videoeditor/TransitionFadeBlack;

    invoke-static {}, Lcom/android/videoeditor/service/ApiService;->generateId()Ljava/lang/String;

    move-result-object v1

    iget-wide v4, p0, Lcom/android/videoeditor/service/MovieTransition;->mDurationMs:J

    iget v6, p0, Lcom/android/videoeditor/service/MovieTransition;->mBehavior:I

    move-object v2, p2

    move-object v3, p3

    invoke-direct/range {v0 .. v6}, Landroid/media/videoeditor/TransitionFadeBlack;-><init>(Ljava/lang/String;Landroid/media/videoeditor/MediaItem;Landroid/media/videoeditor/MediaItem;JI)V

    goto :goto_0

    :cond_2
    const-class v0, Landroid/media/videoeditor/TransitionSliding;

    iget-object v1, p0, Lcom/android/videoeditor/service/MovieTransition;->mTypeClass:Ljava/lang/Class;

    invoke-virtual {v0, v1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    new-instance v0, Landroid/media/videoeditor/TransitionSliding;

    invoke-static {}, Lcom/android/videoeditor/service/ApiService;->generateId()Ljava/lang/String;

    move-result-object v1

    iget-wide v4, p0, Lcom/android/videoeditor/service/MovieTransition;->mDurationMs:J

    iget v6, p0, Lcom/android/videoeditor/service/MovieTransition;->mBehavior:I

    iget v7, p0, Lcom/android/videoeditor/service/MovieTransition;->mSlidingDirection:I

    move-object v2, p2

    move-object v3, p3

    invoke-direct/range {v0 .. v7}, Landroid/media/videoeditor/TransitionSliding;-><init>(Ljava/lang/String;Landroid/media/videoeditor/MediaItem;Landroid/media/videoeditor/MediaItem;JII)V

    goto :goto_0

    :cond_3
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 2
    .param p1    # Ljava/lang/Object;

    instance-of v0, p1, Lcom/android/videoeditor/service/MovieTransition;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lcom/android/videoeditor/service/MovieTransition;->mUniqueId:Ljava/lang/String;

    check-cast p1, Lcom/android/videoeditor/service/MovieTransition;

    iget-object v1, p1, Lcom/android/videoeditor/service/MovieTransition;->mUniqueId:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    goto :goto_0
.end method

.method public getAlphaMaskBlendingPercent()I
    .locals 1

    iget v0, p0, Lcom/android/videoeditor/service/MovieTransition;->mAlphaMaskBlendingPercent:I

    return v0
.end method

.method public getAlphaMaskFilename()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/android/videoeditor/service/MovieTransition;->mAlphaMaskFilename:Ljava/lang/String;

    return-object v0
.end method

.method public getAlphaMaskResId()I
    .locals 1

    iget v0, p0, Lcom/android/videoeditor/service/MovieTransition;->mAlphaMaskResId:I

    return v0
.end method

.method public getAppDuration()J
    .locals 2

    iget-wide v0, p0, Lcom/android/videoeditor/service/MovieTransition;->mAppDurationMs:J

    return-wide v0
.end method

.method public getBehavior()I
    .locals 1

    iget v0, p0, Lcom/android/videoeditor/service/MovieTransition;->mBehavior:I

    return v0
.end method

.method getDuration()J
    .locals 2

    iget-wide v0, p0, Lcom/android/videoeditor/service/MovieTransition;->mDurationMs:J

    return-wide v0
.end method

.method public getId()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/android/videoeditor/service/MovieTransition;->mUniqueId:Ljava/lang/String;

    return-object v0
.end method

.method public getSlidingDirection()I
    .locals 1

    iget v0, p0, Lcom/android/videoeditor/service/MovieTransition;->mSlidingDirection:I

    return v0
.end method

.method public getType()I
    .locals 1

    iget v0, p0, Lcom/android/videoeditor/service/MovieTransition;->mType:I

    return v0
.end method

.method public getTypeClass()Ljava/lang/Class;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/lang/Class",
            "<*>;"
        }
    .end annotation

    iget-object v0, p0, Lcom/android/videoeditor/service/MovieTransition;->mTypeClass:Ljava/lang/Class;

    return-object v0
.end method

.method public hashCode()I
    .locals 1

    iget-object v0, p0, Lcom/android/videoeditor/service/MovieTransition;->mUniqueId:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    return v0
.end method

.method public isAlphaInverted()Z
    .locals 1

    iget-boolean v0, p0, Lcom/android/videoeditor/service/MovieTransition;->mAlphaInvert:Z

    return v0
.end method

.method public setAppDuration(J)V
    .locals 0
    .param p1    # J

    iput-wide p1, p0, Lcom/android/videoeditor/service/MovieTransition;->mAppDurationMs:J

    return-void
.end method

.method setDuration(J)V
    .locals 0
    .param p1    # J

    iput-wide p1, p0, Lcom/android/videoeditor/service/MovieTransition;->mDurationMs:J

    return-void
.end method
