.class public Lcom/android/videoeditor/service/VideoEditorProject$Download;
.super Ljava/lang/Object;
.source "VideoEditorProject.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/videoeditor/service/VideoEditorProject;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "Download"
.end annotation


# instance fields
.field private final mFilename:Ljava/lang/String;

.field private final mMediaUri:Ljava/lang/String;

.field private final mMimeType:Ljava/lang/String;

.field private final mTime:J


# direct methods
.method private constructor <init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;J)V
    .locals 0
    .param p1    # Ljava/lang/String;
    .param p2    # Ljava/lang/String;
    .param p3    # Ljava/lang/String;
    .param p4    # J

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/android/videoeditor/service/VideoEditorProject$Download;->mMediaUri:Ljava/lang/String;

    iput-object p2, p0, Lcom/android/videoeditor/service/VideoEditorProject$Download;->mMimeType:Ljava/lang/String;

    iput-object p3, p0, Lcom/android/videoeditor/service/VideoEditorProject$Download;->mFilename:Ljava/lang/String;

    iput-wide p4, p0, Lcom/android/videoeditor/service/VideoEditorProject$Download;->mTime:J

    return-void
.end method

.method synthetic constructor <init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;JLcom/android/videoeditor/service/VideoEditorProject$1;)V
    .locals 0
    .param p1    # Ljava/lang/String;
    .param p2    # Ljava/lang/String;
    .param p3    # Ljava/lang/String;
    .param p4    # J
    .param p6    # Lcom/android/videoeditor/service/VideoEditorProject$1;

    invoke-direct/range {p0 .. p5}, Lcom/android/videoeditor/service/VideoEditorProject$Download;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;J)V

    return-void
.end method


# virtual methods
.method public getFilename()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/android/videoeditor/service/VideoEditorProject$Download;->mFilename:Ljava/lang/String;

    return-object v0
.end method

.method public getMediaUri()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/android/videoeditor/service/VideoEditorProject$Download;->mMediaUri:Ljava/lang/String;

    return-object v0
.end method

.method public getMimeType()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/android/videoeditor/service/VideoEditorProject$Download;->mMimeType:Ljava/lang/String;

    return-object v0
.end method

.method public getTime()J
    .locals 2

    iget-wide v0, p0, Lcom/android/videoeditor/service/VideoEditorProject$Download;->mTime:J

    return-wide v0
.end method
