.class public Lcom/android/videoeditor/service/MovieMediaItem;
.super Ljava/lang/Object;
.source "MovieMediaItem.java"


# instance fields
.field private mAppBeginBoundaryTimeMs:J

.field private mAppEndBoundaryTimeMs:J

.field private mAppMuted:Z

.field private mAppRenderingMode:I

.field private mAppVolumePercent:I

.field private final mAspectRatio:I

.field private final mBeginBoundaryTimeMs:J

.field private mBeginTransition:Lcom/android/videoeditor/service/MovieTransition;

.field private final mDurationMs:J

.field private mEffect:Lcom/android/videoeditor/service/MovieEffect;

.field private final mEndBoundaryTimeMs:J

.field private mEndTransition:Lcom/android/videoeditor/service/MovieTransition;

.field private final mFilename:Ljava/lang/String;

.field private final mHeight:I

.field private mMuted:Z

.field private mOverlay:Lcom/android/videoeditor/service/MovieOverlay;

.field private mRenderingMode:I

.field private final mType:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<*>;"
        }
    .end annotation
.end field

.field private final mUniqueId:Ljava/lang/String;

.field private mVolumePercent:I

.field private mWaveformData:Landroid/media/videoeditor/WaveformData;

.field private final mWidth:I


# direct methods
.method constructor <init>(Landroid/media/videoeditor/MediaItem;)V
    .locals 2
    .param p1    # Landroid/media/videoeditor/MediaItem;

    invoke-virtual {p1}, Landroid/media/videoeditor/MediaItem;->getBeginTransition()Landroid/media/videoeditor/Transition;

    move-result-object v0

    if-eqz v0, :cond_0

    new-instance v0, Lcom/android/videoeditor/service/MovieTransition;

    invoke-virtual {p1}, Landroid/media/videoeditor/MediaItem;->getBeginTransition()Landroid/media/videoeditor/Transition;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/android/videoeditor/service/MovieTransition;-><init>(Landroid/media/videoeditor/Transition;)V

    :goto_0
    invoke-direct {p0, p1, v0}, Lcom/android/videoeditor/service/MovieMediaItem;-><init>(Landroid/media/videoeditor/MediaItem;Lcom/android/videoeditor/service/MovieTransition;)V

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method constructor <init>(Landroid/media/videoeditor/MediaItem;Lcom/android/videoeditor/service/MovieTransition;)V
    .locals 11
    .param p1    # Landroid/media/videoeditor/MediaItem;
    .param p2    # Lcom/android/videoeditor/service/MovieTransition;

    const/4 v10, 0x0

    const/4 v9, 0x0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v7

    iput-object v7, p0, Lcom/android/videoeditor/service/MovieMediaItem;->mType:Ljava/lang/Class;

    invoke-virtual {p1}, Landroid/media/videoeditor/MediaItem;->getId()Ljava/lang/String;

    move-result-object v7

    iput-object v7, p0, Lcom/android/videoeditor/service/MovieMediaItem;->mUniqueId:Ljava/lang/String;

    invoke-virtual {p1}, Landroid/media/videoeditor/MediaItem;->getFilename()Ljava/lang/String;

    move-result-object v7

    iput-object v7, p0, Lcom/android/videoeditor/service/MovieMediaItem;->mFilename:Ljava/lang/String;

    invoke-virtual {p1}, Landroid/media/videoeditor/MediaItem;->getRenderingMode()I

    move-result v7

    iput v7, p0, Lcom/android/videoeditor/service/MovieMediaItem;->mRenderingMode:I

    iput v7, p0, Lcom/android/videoeditor/service/MovieMediaItem;->mAppRenderingMode:I

    invoke-virtual {p1}, Landroid/media/videoeditor/MediaItem;->getAspectRatio()I

    move-result v7

    iput v7, p0, Lcom/android/videoeditor/service/MovieMediaItem;->mAspectRatio:I

    invoke-virtual {p1}, Landroid/media/videoeditor/MediaItem;->getWidth()I

    move-result v7

    iput v7, p0, Lcom/android/videoeditor/service/MovieMediaItem;->mWidth:I

    invoke-virtual {p1}, Landroid/media/videoeditor/MediaItem;->getHeight()I

    move-result v7

    iput v7, p0, Lcom/android/videoeditor/service/MovieMediaItem;->mHeight:I

    invoke-virtual {p1}, Landroid/media/videoeditor/MediaItem;->getDuration()J

    move-result-wide v7

    iput-wide v7, p0, Lcom/android/videoeditor/service/MovieMediaItem;->mDurationMs:J

    instance-of v7, p1, Landroid/media/videoeditor/MediaVideoItem;

    if-eqz v7, :cond_0

    move-object v6, p1

    check-cast v6, Landroid/media/videoeditor/MediaVideoItem;

    invoke-virtual {v6}, Landroid/media/videoeditor/MediaVideoItem;->getBoundaryBeginTime()J

    move-result-wide v7

    iput-wide v7, p0, Lcom/android/videoeditor/service/MovieMediaItem;->mBeginBoundaryTimeMs:J

    iput-wide v7, p0, Lcom/android/videoeditor/service/MovieMediaItem;->mAppBeginBoundaryTimeMs:J

    invoke-virtual {v6}, Landroid/media/videoeditor/MediaVideoItem;->getBoundaryEndTime()J

    move-result-wide v7

    iput-wide v7, p0, Lcom/android/videoeditor/service/MovieMediaItem;->mEndBoundaryTimeMs:J

    iput-wide v7, p0, Lcom/android/videoeditor/service/MovieMediaItem;->mAppEndBoundaryTimeMs:J

    :try_start_0
    invoke-virtual {v6}, Landroid/media/videoeditor/MediaVideoItem;->getWaveformData()Landroid/media/videoeditor/WaveformData;

    move-result-object v7

    iput-object v7, p0, Lcom/android/videoeditor/service/MovieMediaItem;->mWaveformData:Landroid/media/videoeditor/WaveformData;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    invoke-virtual {v6}, Landroid/media/videoeditor/MediaVideoItem;->getVolume()I

    move-result v7

    iput v7, p0, Lcom/android/videoeditor/service/MovieMediaItem;->mVolumePercent:I

    iput v7, p0, Lcom/android/videoeditor/service/MovieMediaItem;->mAppVolumePercent:I

    invoke-virtual {v6}, Landroid/media/videoeditor/MediaVideoItem;->isMuted()Z

    move-result v7

    iput-boolean v7, p0, Lcom/android/videoeditor/service/MovieMediaItem;->mMuted:Z

    iput-boolean v7, p0, Lcom/android/videoeditor/service/MovieMediaItem;->mAppMuted:Z

    :goto_1
    invoke-virtual {p1}, Landroid/media/videoeditor/MediaItem;->getAllOverlays()Ljava/util/List;

    move-result-object v5

    invoke-interface {v5}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_2
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v7

    if-eqz v7, :cond_1

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Landroid/media/videoeditor/Overlay;

    new-instance v7, Lcom/android/videoeditor/service/MovieOverlay;

    invoke-direct {v7, v4}, Lcom/android/videoeditor/service/MovieOverlay;-><init>(Landroid/media/videoeditor/Overlay;)V

    invoke-virtual {p0, v7}, Lcom/android/videoeditor/service/MovieMediaItem;->addOverlay(Lcom/android/videoeditor/service/MovieOverlay;)V

    goto :goto_2

    :catch_0
    move-exception v2

    iput-object v10, p0, Lcom/android/videoeditor/service/MovieMediaItem;->mWaveformData:Landroid/media/videoeditor/WaveformData;

    goto :goto_0

    :cond_0
    const-wide/16 v7, 0x0

    iput-wide v7, p0, Lcom/android/videoeditor/service/MovieMediaItem;->mBeginBoundaryTimeMs:J

    iput-wide v7, p0, Lcom/android/videoeditor/service/MovieMediaItem;->mAppBeginBoundaryTimeMs:J

    invoke-virtual {p1}, Landroid/media/videoeditor/MediaItem;->getTimelineDuration()J

    move-result-wide v7

    iput-wide v7, p0, Lcom/android/videoeditor/service/MovieMediaItem;->mEndBoundaryTimeMs:J

    iput-wide v7, p0, Lcom/android/videoeditor/service/MovieMediaItem;->mAppEndBoundaryTimeMs:J

    iput-object v10, p0, Lcom/android/videoeditor/service/MovieMediaItem;->mWaveformData:Landroid/media/videoeditor/WaveformData;

    iput v9, p0, Lcom/android/videoeditor/service/MovieMediaItem;->mVolumePercent:I

    iput v9, p0, Lcom/android/videoeditor/service/MovieMediaItem;->mAppVolumePercent:I

    iput-boolean v9, p0, Lcom/android/videoeditor/service/MovieMediaItem;->mMuted:Z

    iput-boolean v9, p0, Lcom/android/videoeditor/service/MovieMediaItem;->mAppMuted:Z

    goto :goto_1

    :cond_1
    invoke-virtual {p1}, Landroid/media/videoeditor/MediaItem;->getAllEffects()Ljava/util/List;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_3
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v7

    if-eqz v7, :cond_2

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/media/videoeditor/Effect;

    new-instance v7, Lcom/android/videoeditor/service/MovieEffect;

    invoke-direct {v7, v0}, Lcom/android/videoeditor/service/MovieEffect;-><init>(Landroid/media/videoeditor/Effect;)V

    invoke-virtual {p0, v7}, Lcom/android/videoeditor/service/MovieMediaItem;->addEffect(Lcom/android/videoeditor/service/MovieEffect;)V

    goto :goto_3

    :cond_2
    invoke-virtual {p0, p2}, Lcom/android/videoeditor/service/MovieMediaItem;->setBeginTransition(Lcom/android/videoeditor/service/MovieTransition;)V

    invoke-virtual {p1}, Landroid/media/videoeditor/MediaItem;->getEndTransition()Landroid/media/videoeditor/Transition;

    move-result-object v7

    if-eqz v7, :cond_3

    new-instance v7, Lcom/android/videoeditor/service/MovieTransition;

    invoke-virtual {p1}, Landroid/media/videoeditor/MediaItem;->getEndTransition()Landroid/media/videoeditor/Transition;

    move-result-object v8

    invoke-direct {v7, v8}, Lcom/android/videoeditor/service/MovieTransition;-><init>(Landroid/media/videoeditor/Transition;)V

    invoke-virtual {p0, v7}, Lcom/android/videoeditor/service/MovieMediaItem;->setEndTransition(Lcom/android/videoeditor/service/MovieTransition;)V

    :cond_3
    return-void
.end method


# virtual methods
.method addEffect(Lcom/android/videoeditor/service/MovieEffect;)V
    .locals 3
    .param p1    # Lcom/android/videoeditor/service/MovieEffect;

    iget-object v0, p0, Lcom/android/videoeditor/service/MovieMediaItem;->mEffect:Lcom/android/videoeditor/service/MovieEffect;

    if-eqz v0, :cond_0

    new-instance v0, Ljava/lang/IllegalStateException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Effect already set for media item: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/android/videoeditor/service/MovieMediaItem;->mUniqueId:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    iput-object p1, p0, Lcom/android/videoeditor/service/MovieMediaItem;->mEffect:Lcom/android/videoeditor/service/MovieEffect;

    return-void
.end method

.method addOverlay(Lcom/android/videoeditor/service/MovieOverlay;)V
    .locals 3
    .param p1    # Lcom/android/videoeditor/service/MovieOverlay;

    iget-object v0, p0, Lcom/android/videoeditor/service/MovieMediaItem;->mOverlay:Lcom/android/videoeditor/service/MovieOverlay;

    if-eqz v0, :cond_0

    new-instance v0, Ljava/lang/IllegalStateException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Overlay already set for media item: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/android/videoeditor/service/MovieMediaItem;->mUniqueId:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    iput-object p1, p0, Lcom/android/videoeditor/service/MovieMediaItem;->mOverlay:Lcom/android/videoeditor/service/MovieOverlay;

    return-void
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 2
    .param p1    # Ljava/lang/Object;

    instance-of v0, p1, Lcom/android/videoeditor/service/MovieMediaItem;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lcom/android/videoeditor/service/MovieMediaItem;->mUniqueId:Ljava/lang/String;

    check-cast p1, Lcom/android/videoeditor/service/MovieMediaItem;

    iget-object v1, p1, Lcom/android/videoeditor/service/MovieMediaItem;->mUniqueId:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    goto :goto_0
.end method

.method public getAppBoundaryBeginTime()J
    .locals 2

    iget-wide v0, p0, Lcom/android/videoeditor/service/MovieMediaItem;->mAppBeginBoundaryTimeMs:J

    return-wide v0
.end method

.method public getAppBoundaryEndTime()J
    .locals 2

    iget-wide v0, p0, Lcom/android/videoeditor/service/MovieMediaItem;->mAppEndBoundaryTimeMs:J

    return-wide v0
.end method

.method public getAppRenderingMode()I
    .locals 1

    iget v0, p0, Lcom/android/videoeditor/service/MovieMediaItem;->mAppRenderingMode:I

    return v0
.end method

.method public getAppTimelineDuration()J
    .locals 4

    iget-wide v0, p0, Lcom/android/videoeditor/service/MovieMediaItem;->mAppEndBoundaryTimeMs:J

    iget-wide v2, p0, Lcom/android/videoeditor/service/MovieMediaItem;->mAppBeginBoundaryTimeMs:J

    sub-long/2addr v0, v2

    return-wide v0
.end method

.method public getAppVolume()I
    .locals 1

    iget v0, p0, Lcom/android/videoeditor/service/MovieMediaItem;->mAppVolumePercent:I

    return v0
.end method

.method public getAspectRatio()I
    .locals 1

    iget v0, p0, Lcom/android/videoeditor/service/MovieMediaItem;->mAspectRatio:I

    return v0
.end method

.method public getBeginTransition()Lcom/android/videoeditor/service/MovieTransition;
    .locals 1

    iget-object v0, p0, Lcom/android/videoeditor/service/MovieMediaItem;->mBeginTransition:Lcom/android/videoeditor/service/MovieTransition;

    return-object v0
.end method

.method getBoundaryBeginTime()J
    .locals 2

    iget-wide v0, p0, Lcom/android/videoeditor/service/MovieMediaItem;->mBeginBoundaryTimeMs:J

    return-wide v0
.end method

.method getBoundaryEndTime()J
    .locals 2

    iget-wide v0, p0, Lcom/android/videoeditor/service/MovieMediaItem;->mEndBoundaryTimeMs:J

    return-wide v0
.end method

.method public getDuration()J
    .locals 2

    iget-wide v0, p0, Lcom/android/videoeditor/service/MovieMediaItem;->mDurationMs:J

    return-wide v0
.end method

.method public getEffect()Lcom/android/videoeditor/service/MovieEffect;
    .locals 1

    iget-object v0, p0, Lcom/android/videoeditor/service/MovieMediaItem;->mEffect:Lcom/android/videoeditor/service/MovieEffect;

    return-object v0
.end method

.method public getEndTransition()Lcom/android/videoeditor/service/MovieTransition;
    .locals 1

    iget-object v0, p0, Lcom/android/videoeditor/service/MovieMediaItem;->mEndTransition:Lcom/android/videoeditor/service/MovieTransition;

    return-object v0
.end method

.method public getFilename()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/android/videoeditor/service/MovieMediaItem;->mFilename:Ljava/lang/String;

    return-object v0
.end method

.method public getHeight()I
    .locals 1

    iget v0, p0, Lcom/android/videoeditor/service/MovieMediaItem;->mHeight:I

    return v0
.end method

.method public getId()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/android/videoeditor/service/MovieMediaItem;->mUniqueId:Ljava/lang/String;

    return-object v0
.end method

.method public getOverlay()Lcom/android/videoeditor/service/MovieOverlay;
    .locals 1

    iget-object v0, p0, Lcom/android/videoeditor/service/MovieMediaItem;->mOverlay:Lcom/android/videoeditor/service/MovieOverlay;

    return-object v0
.end method

.method getRenderingMode()I
    .locals 1

    iget v0, p0, Lcom/android/videoeditor/service/MovieMediaItem;->mRenderingMode:I

    return v0
.end method

.method getVolume()I
    .locals 1

    iget v0, p0, Lcom/android/videoeditor/service/MovieMediaItem;->mVolumePercent:I

    return v0
.end method

.method public getWaveformData()Landroid/media/videoeditor/WaveformData;
    .locals 1

    iget-object v0, p0, Lcom/android/videoeditor/service/MovieMediaItem;->mWaveformData:Landroid/media/videoeditor/WaveformData;

    return-object v0
.end method

.method public getWidth()I
    .locals 1

    iget v0, p0, Lcom/android/videoeditor/service/MovieMediaItem;->mWidth:I

    return v0
.end method

.method public hashCode()I
    .locals 1

    iget-object v0, p0, Lcom/android/videoeditor/service/MovieMediaItem;->mUniqueId:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    return v0
.end method

.method public isAppMuted()Z
    .locals 1

    iget-boolean v0, p0, Lcom/android/videoeditor/service/MovieMediaItem;->mAppMuted:Z

    return v0
.end method

.method public isImage()Z
    .locals 2

    const-class v0, Landroid/media/videoeditor/MediaImageItem;

    iget-object v1, p0, Lcom/android/videoeditor/service/MovieMediaItem;->mType:Ljava/lang/Class;

    invoke-virtual {v0, v1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method isMuted()Z
    .locals 1

    iget-boolean v0, p0, Lcom/android/videoeditor/service/MovieMediaItem;->mMuted:Z

    return v0
.end method

.method public isVideoClip()Z
    .locals 2

    const-class v0, Landroid/media/videoeditor/MediaVideoItem;

    iget-object v1, p0, Lcom/android/videoeditor/service/MovieMediaItem;->mType:Ljava/lang/Class;

    invoke-virtual {v0, v1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method removeEffect(Ljava/lang/String;)V
    .locals 3
    .param p1    # Ljava/lang/String;

    iget-object v0, p0, Lcom/android/videoeditor/service/MovieMediaItem;->mEffect:Lcom/android/videoeditor/service/MovieEffect;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/android/videoeditor/service/MovieMediaItem;->mEffect:Lcom/android/videoeditor/service/MovieEffect;

    invoke-virtual {v0}, Lcom/android/videoeditor/service/MovieEffect;->getId()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/IllegalStateException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Effect does not match: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/android/videoeditor/service/MovieMediaItem;->mEffect:Lcom/android/videoeditor/service/MovieEffect;

    invoke-virtual {v2}, Lcom/android/videoeditor/service/MovieEffect;->getId()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/android/videoeditor/service/MovieMediaItem;->mEffect:Lcom/android/videoeditor/service/MovieEffect;

    :cond_1
    return-void
.end method

.method removeOverlay(Ljava/lang/String;)V
    .locals 3
    .param p1    # Ljava/lang/String;

    iget-object v0, p0, Lcom/android/videoeditor/service/MovieMediaItem;->mOverlay:Lcom/android/videoeditor/service/MovieOverlay;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/android/videoeditor/service/MovieMediaItem;->mOverlay:Lcom/android/videoeditor/service/MovieOverlay;

    invoke-virtual {v0}, Lcom/android/videoeditor/service/MovieOverlay;->getId()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/IllegalStateException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Overlay does not match: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/android/videoeditor/service/MovieMediaItem;->mOverlay:Lcom/android/videoeditor/service/MovieOverlay;

    invoke-virtual {v2}, Lcom/android/videoeditor/service/MovieOverlay;->getId()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/android/videoeditor/service/MovieMediaItem;->mOverlay:Lcom/android/videoeditor/service/MovieOverlay;

    :cond_1
    return-void
.end method

.method public setAppExtractBoundaries(JJ)V
    .locals 0
    .param p1    # J
    .param p3    # J

    iput-wide p1, p0, Lcom/android/videoeditor/service/MovieMediaItem;->mAppBeginBoundaryTimeMs:J

    iput-wide p3, p0, Lcom/android/videoeditor/service/MovieMediaItem;->mAppEndBoundaryTimeMs:J

    return-void
.end method

.method public setAppMute(Z)V
    .locals 0
    .param p1    # Z

    iput-boolean p1, p0, Lcom/android/videoeditor/service/MovieMediaItem;->mAppMuted:Z

    return-void
.end method

.method public setAppRenderingMode(I)V
    .locals 0
    .param p1    # I

    iput p1, p0, Lcom/android/videoeditor/service/MovieMediaItem;->mAppRenderingMode:I

    return-void
.end method

.method public setAppVolume(I)V
    .locals 0
    .param p1    # I

    iput p1, p0, Lcom/android/videoeditor/service/MovieMediaItem;->mAppVolumePercent:I

    return-void
.end method

.method setBeginTransition(Lcom/android/videoeditor/service/MovieTransition;)V
    .locals 0
    .param p1    # Lcom/android/videoeditor/service/MovieTransition;

    iput-object p1, p0, Lcom/android/videoeditor/service/MovieMediaItem;->mBeginTransition:Lcom/android/videoeditor/service/MovieTransition;

    return-void
.end method

.method setEndTransition(Lcom/android/videoeditor/service/MovieTransition;)V
    .locals 0
    .param p1    # Lcom/android/videoeditor/service/MovieTransition;

    iput-object p1, p0, Lcom/android/videoeditor/service/MovieMediaItem;->mEndTransition:Lcom/android/videoeditor/service/MovieTransition;

    return-void
.end method

.method setMute(Z)V
    .locals 0
    .param p1    # Z

    iput-boolean p1, p0, Lcom/android/videoeditor/service/MovieMediaItem;->mMuted:Z

    return-void
.end method

.method setRenderingMode(I)V
    .locals 0
    .param p1    # I

    iput p1, p0, Lcom/android/videoeditor/service/MovieMediaItem;->mRenderingMode:I

    return-void
.end method

.method setVolume(I)V
    .locals 0
    .param p1    # I

    iput p1, p0, Lcom/android/videoeditor/service/MovieMediaItem;->mVolumePercent:I

    return-void
.end method

.method setWaveformData(Landroid/media/videoeditor/WaveformData;)V
    .locals 0
    .param p1    # Landroid/media/videoeditor/WaveformData;

    iput-object p1, p0, Lcom/android/videoeditor/service/MovieMediaItem;->mWaveformData:Landroid/media/videoeditor/WaveformData;

    return-void
.end method
