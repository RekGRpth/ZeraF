.class public abstract Lcom/android/videoeditor/BaseAdapterWithImages;
.super Landroid/widget/BaseAdapter;
.source "BaseAdapterWithImages.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/videoeditor/BaseAdapterWithImages$ImageLoaderAsyncTask;,
        Lcom/android/videoeditor/BaseAdapterWithImages$ImageTextViewHolder;,
        Lcom/android/videoeditor/BaseAdapterWithImages$ImageViewHolder;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<K:",
        "Ljava/lang/Object;",
        ">",
        "Landroid/widget/BaseAdapter;"
    }
.end annotation


# instance fields
.field protected final mContext:Landroid/content/Context;

.field private final mListView:Landroid/widget/AbsListView;

.field private final mLoadingImages:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<TK;>;"
        }
    .end annotation
.end field

.field private final mViewHolders:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/android/videoeditor/BaseAdapterWithImages$ImageViewHolder",
            "<TK;>;>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/widget/AbsListView;)V
    .locals 2
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/widget/AbsListView;

    invoke-direct {p0}, Landroid/widget/BaseAdapter;-><init>()V

    iput-object p1, p0, Lcom/android/videoeditor/BaseAdapterWithImages;->mContext:Landroid/content/Context;

    iput-object p2, p0, Lcom/android/videoeditor/BaseAdapterWithImages;->mListView:Landroid/widget/AbsListView;

    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, Lcom/android/videoeditor/BaseAdapterWithImages;->mLoadingImages:Ljava/util/Set;

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/android/videoeditor/BaseAdapterWithImages;->mViewHolders:Ljava/util/List;

    iget-object v0, p0, Lcom/android/videoeditor/BaseAdapterWithImages;->mListView:Landroid/widget/AbsListView;

    new-instance v1, Lcom/android/videoeditor/BaseAdapterWithImages$1;

    invoke-direct {v1, p0}, Lcom/android/videoeditor/BaseAdapterWithImages$1;-><init>(Lcom/android/videoeditor/BaseAdapterWithImages;)V

    invoke-virtual {v0, v1}, Landroid/widget/AbsListView;->setRecyclerListener(Landroid/widget/AbsListView$RecyclerListener;)V

    return-void
.end method

.method static synthetic access$000(Lcom/android/videoeditor/BaseAdapterWithImages;)Ljava/util/Set;
    .locals 1
    .param p0    # Lcom/android/videoeditor/BaseAdapterWithImages;

    iget-object v0, p0, Lcom/android/videoeditor/BaseAdapterWithImages;->mLoadingImages:Ljava/util/Set;

    return-object v0
.end method

.method static synthetic access$100(Lcom/android/videoeditor/BaseAdapterWithImages;)Ljava/util/List;
    .locals 1
    .param p0    # Lcom/android/videoeditor/BaseAdapterWithImages;

    iget-object v0, p0, Lcom/android/videoeditor/BaseAdapterWithImages;->mViewHolders:Ljava/util/List;

    return-object v0
.end method


# virtual methods
.method public abstract getCount()I
.end method

.method public abstract getItem(I)Ljava/lang/Object;
.end method

.method public getItemId(I)J
    .locals 2
    .param p1    # I

    int-to-long v0, p1

    return-wide v0
.end method

.method public abstract getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
.end method

.method protected initiateLoad(Ljava/lang/Object;Ljava/lang/Object;Lcom/android/videoeditor/BaseAdapterWithImages$ImageViewHolder;)V
    .locals 2
    .param p2    # Ljava/lang/Object;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TK;",
            "Ljava/lang/Object;",
            "Lcom/android/videoeditor/BaseAdapterWithImages$ImageViewHolder",
            "<TK;>;)V"
        }
    .end annotation

    iget-object v0, p0, Lcom/android/videoeditor/BaseAdapterWithImages;->mViewHolders:Ljava/util/List;

    invoke-interface {v0, p3}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/android/videoeditor/BaseAdapterWithImages;->mViewHolders:Ljava/util/List;

    invoke-interface {v0, p3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :cond_0
    invoke-virtual {p3, p1}, Lcom/android/videoeditor/BaseAdapterWithImages$ImageViewHolder;->setKey(Ljava/lang/Object;)V

    iget-object v0, p0, Lcom/android/videoeditor/BaseAdapterWithImages;->mLoadingImages:Ljava/util/Set;

    invoke-interface {v0, p1}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/android/videoeditor/BaseAdapterWithImages;->mLoadingImages:Ljava/util/Set;

    invoke-interface {v0, p1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    new-instance v0, Lcom/android/videoeditor/BaseAdapterWithImages$ImageLoaderAsyncTask;

    invoke-direct {v0, p0, p1, p2}, Lcom/android/videoeditor/BaseAdapterWithImages$ImageLoaderAsyncTask;-><init>(Lcom/android/videoeditor/BaseAdapterWithImages;Ljava/lang/Object;Ljava/lang/Object;)V

    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/Void;

    invoke-virtual {v0, v1}, Lcom/android/videoeditor/BaseAdapterWithImages$ImageLoaderAsyncTask;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    :cond_1
    return-void
.end method

.method protected abstract loadImage(Ljava/lang/Object;)Landroid/graphics/Bitmap;
.end method

.method public onDestroy()V
    .locals 7

    iget-object v5, p0, Lcom/android/videoeditor/BaseAdapterWithImages;->mListView:Landroid/widget/AbsListView;

    invoke-virtual {v5}, Landroid/widget/AbsListView;->getChildCount()I

    move-result v0

    const/4 v2, 0x0

    :goto_0
    if-ge v2, v0, :cond_1

    iget-object v5, p0, Lcom/android/videoeditor/BaseAdapterWithImages;->mListView:Landroid/widget/AbsListView;

    invoke-virtual {v5, v2}, Landroid/widget/AbsListView;->getChildAt(I)Landroid/view/View;

    move-result-object v4

    const v5, 0x7f08000f

    invoke-virtual {v4, v5}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/ImageView;

    invoke-virtual {v3}, Landroid/widget/ImageView;->getDrawable()Landroid/graphics/drawable/Drawable;

    move-result-object v1

    check-cast v1, Landroid/graphics/drawable/BitmapDrawable;

    if-eqz v1, :cond_0

    invoke-virtual {v1}, Landroid/graphics/drawable/BitmapDrawable;->getBitmap()Landroid/graphics/Bitmap;

    move-result-object v5

    if-eqz v5, :cond_0

    invoke-virtual {v1}, Landroid/graphics/drawable/BitmapDrawable;->getBitmap()Landroid/graphics/Bitmap;

    move-result-object v5

    invoke-virtual {v5}, Landroid/graphics/Bitmap;->recycle()V

    :cond_0
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    :cond_1
    iget-object v5, p0, Lcom/android/videoeditor/BaseAdapterWithImages;->mListView:Landroid/widget/AbsListView;

    const/4 v6, 0x0

    invoke-virtual {v5, v6, v0}, Landroid/widget/AbsListView;->removeViews(II)V

    invoke-static {}, Ljava/lang/System;->gc()V

    return-void
.end method

.method public onPause()V
    .locals 1

    iget-object v0, p0, Lcom/android/videoeditor/BaseAdapterWithImages;->mViewHolders:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    return-void
.end method
