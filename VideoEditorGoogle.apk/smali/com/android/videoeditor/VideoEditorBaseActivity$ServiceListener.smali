.class Lcom/android/videoeditor/VideoEditorBaseActivity$ServiceListener;
.super Lcom/android/videoeditor/service/ApiServiceListener;
.source "VideoEditorBaseActivity.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/videoeditor/VideoEditorBaseActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "ServiceListener"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/videoeditor/VideoEditorBaseActivity;


# direct methods
.method private constructor <init>(Lcom/android/videoeditor/VideoEditorBaseActivity;)V
    .locals 0

    iput-object p1, p0, Lcom/android/videoeditor/VideoEditorBaseActivity$ServiceListener;->this$0:Lcom/android/videoeditor/VideoEditorBaseActivity;

    invoke-direct {p0}, Lcom/android/videoeditor/service/ApiServiceListener;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/android/videoeditor/VideoEditorBaseActivity;Lcom/android/videoeditor/VideoEditorBaseActivity$1;)V
    .locals 0
    .param p1    # Lcom/android/videoeditor/VideoEditorBaseActivity;
    .param p2    # Lcom/android/videoeditor/VideoEditorBaseActivity$1;

    invoke-direct {p0, p1}, Lcom/android/videoeditor/VideoEditorBaseActivity$ServiceListener;-><init>(Lcom/android/videoeditor/VideoEditorBaseActivity;)V

    return-void
.end method


# virtual methods
.method public onAudioTrackAdded(Ljava/lang/String;Lcom/android/videoeditor/service/MovieAudioTrack;Ljava/lang/Exception;)V
    .locals 3
    .param p1    # Ljava/lang/String;
    .param p2    # Lcom/android/videoeditor/service/MovieAudioTrack;
    .param p3    # Ljava/lang/Exception;

    iget-object v0, p0, Lcom/android/videoeditor/VideoEditorBaseActivity$ServiceListener;->this$0:Lcom/android/videoeditor/VideoEditorBaseActivity;

    iget-object v0, v0, Lcom/android/videoeditor/VideoEditorBaseActivity;->mProjectPath:Ljava/lang/String;

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v0, p0, Lcom/android/videoeditor/VideoEditorBaseActivity$ServiceListener;->this$0:Lcom/android/videoeditor/VideoEditorBaseActivity;

    iget-object v0, v0, Lcom/android/videoeditor/VideoEditorBaseActivity;->mProject:Lcom/android/videoeditor/service/VideoEditorProject;

    if-eqz v0, :cond_0

    if-eqz p3, :cond_2

    iget-object v0, p0, Lcom/android/videoeditor/VideoEditorBaseActivity$ServiceListener;->this$0:Lcom/android/videoeditor/VideoEditorBaseActivity;

    const v1, 0x7f090052

    const/4 v2, 0x1

    invoke-static {v0, v1, v2}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    goto :goto_0

    :cond_2
    iget-object v0, p0, Lcom/android/videoeditor/VideoEditorBaseActivity$ServiceListener;->this$0:Lcom/android/videoeditor/VideoEditorBaseActivity;

    invoke-virtual {v0}, Lcom/android/videoeditor/VideoEditorBaseActivity;->getAudioTrackLayout()Lcom/android/videoeditor/widgets/AudioTrackLinearLayout;

    move-result-object v0

    invoke-virtual {v0, p2}, Lcom/android/videoeditor/widgets/AudioTrackLinearLayout;->addAudioTrack(Lcom/android/videoeditor/service/MovieAudioTrack;)Landroid/view/View;

    goto :goto_0
.end method

.method public onAudioTrackBoundariesSet(Ljava/lang/String;Ljava/lang/String;JJLjava/lang/Exception;)V
    .locals 3
    .param p1    # Ljava/lang/String;
    .param p2    # Ljava/lang/String;
    .param p3    # J
    .param p5    # J
    .param p7    # Ljava/lang/Exception;

    iget-object v0, p0, Lcom/android/videoeditor/VideoEditorBaseActivity$ServiceListener;->this$0:Lcom/android/videoeditor/VideoEditorBaseActivity;

    iget-object v0, v0, Lcom/android/videoeditor/VideoEditorBaseActivity;->mProjectPath:Ljava/lang/String;

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v0, p0, Lcom/android/videoeditor/VideoEditorBaseActivity$ServiceListener;->this$0:Lcom/android/videoeditor/VideoEditorBaseActivity;

    iget-object v0, v0, Lcom/android/videoeditor/VideoEditorBaseActivity;->mProject:Lcom/android/videoeditor/service/VideoEditorProject;

    if-eqz v0, :cond_0

    if-eqz p7, :cond_2

    iget-object v0, p0, Lcom/android/videoeditor/VideoEditorBaseActivity$ServiceListener;->this$0:Lcom/android/videoeditor/VideoEditorBaseActivity;

    const v1, 0x7f090054

    const/4 v2, 0x1

    invoke-static {v0, v1, v2}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    goto :goto_0

    :cond_2
    iget-object v0, p0, Lcom/android/videoeditor/VideoEditorBaseActivity$ServiceListener;->this$0:Lcom/android/videoeditor/VideoEditorBaseActivity;

    invoke-virtual {v0}, Lcom/android/videoeditor/VideoEditorBaseActivity;->getAudioTrackLayout()Lcom/android/videoeditor/widgets/AudioTrackLinearLayout;

    move-result-object v0

    invoke-virtual {v0, p2}, Lcom/android/videoeditor/widgets/AudioTrackLinearLayout;->updateAudioTrack(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public onAudioTrackExtractAudioWaveformComplete(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Exception;)V
    .locals 1
    .param p1    # Ljava/lang/String;
    .param p2    # Ljava/lang/String;
    .param p3    # Ljava/lang/Exception;

    iget-object v0, p0, Lcom/android/videoeditor/VideoEditorBaseActivity$ServiceListener;->this$0:Lcom/android/videoeditor/VideoEditorBaseActivity;

    iget-object v0, v0, Lcom/android/videoeditor/VideoEditorBaseActivity;->mProjectPath:Ljava/lang/String;

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v0, p0, Lcom/android/videoeditor/VideoEditorBaseActivity$ServiceListener;->this$0:Lcom/android/videoeditor/VideoEditorBaseActivity;

    iget-object v0, v0, Lcom/android/videoeditor/VideoEditorBaseActivity;->mProject:Lcom/android/videoeditor/service/VideoEditorProject;

    if-eqz v0, :cond_0

    if-nez p3, :cond_0

    iget-object v0, p0, Lcom/android/videoeditor/VideoEditorBaseActivity$ServiceListener;->this$0:Lcom/android/videoeditor/VideoEditorBaseActivity;

    invoke-virtual {v0}, Lcom/android/videoeditor/VideoEditorBaseActivity;->getAudioTrackLayout()Lcom/android/videoeditor/widgets/AudioTrackLinearLayout;

    move-result-object v0

    invoke-virtual {v0, p2}, Lcom/android/videoeditor/widgets/AudioTrackLinearLayout;->setWaveformExtractionComplete(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public onAudioTrackExtractAudioWaveformProgress(Ljava/lang/String;Ljava/lang/String;I)V
    .locals 1
    .param p1    # Ljava/lang/String;
    .param p2    # Ljava/lang/String;
    .param p3    # I

    iget-object v0, p0, Lcom/android/videoeditor/VideoEditorBaseActivity$ServiceListener;->this$0:Lcom/android/videoeditor/VideoEditorBaseActivity;

    iget-object v0, v0, Lcom/android/videoeditor/VideoEditorBaseActivity;->mProjectPath:Ljava/lang/String;

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v0, p0, Lcom/android/videoeditor/VideoEditorBaseActivity$ServiceListener;->this$0:Lcom/android/videoeditor/VideoEditorBaseActivity;

    iget-object v0, v0, Lcom/android/videoeditor/VideoEditorBaseActivity;->mProject:Lcom/android/videoeditor/service/VideoEditorProject;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/videoeditor/VideoEditorBaseActivity$ServiceListener;->this$0:Lcom/android/videoeditor/VideoEditorBaseActivity;

    invoke-virtual {v0}, Lcom/android/videoeditor/VideoEditorBaseActivity;->getAudioTrackLayout()Lcom/android/videoeditor/widgets/AudioTrackLinearLayout;

    move-result-object v0

    invoke-virtual {v0, p2, p3}, Lcom/android/videoeditor/widgets/AudioTrackLinearLayout;->setWaveformExtractionProgress(Ljava/lang/String;I)V

    goto :goto_0
.end method

.method public onAudioTrackRemoved(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Exception;)V
    .locals 3
    .param p1    # Ljava/lang/String;
    .param p2    # Ljava/lang/String;
    .param p3    # Ljava/lang/Exception;

    iget-object v0, p0, Lcom/android/videoeditor/VideoEditorBaseActivity$ServiceListener;->this$0:Lcom/android/videoeditor/VideoEditorBaseActivity;

    iget-object v0, v0, Lcom/android/videoeditor/VideoEditorBaseActivity;->mProjectPath:Ljava/lang/String;

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v0, p0, Lcom/android/videoeditor/VideoEditorBaseActivity$ServiceListener;->this$0:Lcom/android/videoeditor/VideoEditorBaseActivity;

    iget-object v0, v0, Lcom/android/videoeditor/VideoEditorBaseActivity;->mProject:Lcom/android/videoeditor/service/VideoEditorProject;

    if-eqz v0, :cond_0

    if-eqz p3, :cond_2

    iget-object v0, p0, Lcom/android/videoeditor/VideoEditorBaseActivity$ServiceListener;->this$0:Lcom/android/videoeditor/VideoEditorBaseActivity;

    const v1, 0x7f090053

    const/4 v2, 0x1

    invoke-static {v0, v1, v2}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    goto :goto_0

    :cond_2
    iget-object v0, p0, Lcom/android/videoeditor/VideoEditorBaseActivity$ServiceListener;->this$0:Lcom/android/videoeditor/VideoEditorBaseActivity;

    invoke-virtual {v0}, Lcom/android/videoeditor/VideoEditorBaseActivity;->getAudioTrackLayout()Lcom/android/videoeditor/widgets/AudioTrackLinearLayout;

    move-result-object v0

    invoke-virtual {v0, p2}, Lcom/android/videoeditor/widgets/AudioTrackLinearLayout;->removeAudioTrack(Ljava/lang/String;)Landroid/view/View;

    goto :goto_0
.end method

.method public onEffectAdded(Ljava/lang/String;Lcom/android/videoeditor/service/MovieEffect;Ljava/lang/String;Ljava/lang/Exception;)V
    .locals 3
    .param p1    # Ljava/lang/String;
    .param p2    # Lcom/android/videoeditor/service/MovieEffect;
    .param p3    # Ljava/lang/String;
    .param p4    # Ljava/lang/Exception;

    iget-object v0, p0, Lcom/android/videoeditor/VideoEditorBaseActivity$ServiceListener;->this$0:Lcom/android/videoeditor/VideoEditorBaseActivity;

    iget-object v0, v0, Lcom/android/videoeditor/VideoEditorBaseActivity;->mProjectPath:Ljava/lang/String;

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v0, p0, Lcom/android/videoeditor/VideoEditorBaseActivity$ServiceListener;->this$0:Lcom/android/videoeditor/VideoEditorBaseActivity;

    iget-object v0, v0, Lcom/android/videoeditor/VideoEditorBaseActivity;->mProject:Lcom/android/videoeditor/service/VideoEditorProject;

    if-eqz v0, :cond_0

    if-eqz p4, :cond_2

    iget-object v0, p0, Lcom/android/videoeditor/VideoEditorBaseActivity$ServiceListener;->this$0:Lcom/android/videoeditor/VideoEditorBaseActivity;

    const v1, 0x7f090050

    const/4 v2, 0x1

    invoke-static {v0, v1, v2}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    goto :goto_0

    :cond_2
    iget-object v0, p0, Lcom/android/videoeditor/VideoEditorBaseActivity$ServiceListener;->this$0:Lcom/android/videoeditor/VideoEditorBaseActivity;

    invoke-virtual {v0}, Lcom/android/videoeditor/VideoEditorBaseActivity;->getMediaLayout()Lcom/android/videoeditor/widgets/MediaLinearLayout;

    move-result-object v0

    iget-object v1, p0, Lcom/android/videoeditor/VideoEditorBaseActivity$ServiceListener;->this$0:Lcom/android/videoeditor/VideoEditorBaseActivity;

    iget-object v1, v1, Lcom/android/videoeditor/VideoEditorBaseActivity;->mProject:Lcom/android/videoeditor/service/VideoEditorProject;

    invoke-virtual {v1, p3}, Lcom/android/videoeditor/service/VideoEditorProject;->getMediaItem(Ljava/lang/String;)Lcom/android/videoeditor/service/MovieMediaItem;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/android/videoeditor/widgets/MediaLinearLayout;->updateMediaItem(Lcom/android/videoeditor/service/MovieMediaItem;)V

    goto :goto_0
.end method

.method public onEffectRemoved(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Exception;)V
    .locals 3
    .param p1    # Ljava/lang/String;
    .param p2    # Ljava/lang/String;
    .param p3    # Ljava/lang/String;
    .param p4    # Ljava/lang/Exception;

    iget-object v0, p0, Lcom/android/videoeditor/VideoEditorBaseActivity$ServiceListener;->this$0:Lcom/android/videoeditor/VideoEditorBaseActivity;

    iget-object v0, v0, Lcom/android/videoeditor/VideoEditorBaseActivity;->mProjectPath:Ljava/lang/String;

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v0, p0, Lcom/android/videoeditor/VideoEditorBaseActivity$ServiceListener;->this$0:Lcom/android/videoeditor/VideoEditorBaseActivity;

    iget-object v0, v0, Lcom/android/videoeditor/VideoEditorBaseActivity;->mProject:Lcom/android/videoeditor/service/VideoEditorProject;

    if-eqz v0, :cond_0

    if-eqz p4, :cond_2

    iget-object v0, p0, Lcom/android/videoeditor/VideoEditorBaseActivity$ServiceListener;->this$0:Lcom/android/videoeditor/VideoEditorBaseActivity;

    const v1, 0x7f090051

    const/4 v2, 0x1

    invoke-static {v0, v1, v2}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    goto :goto_0

    :cond_2
    iget-object v0, p0, Lcom/android/videoeditor/VideoEditorBaseActivity$ServiceListener;->this$0:Lcom/android/videoeditor/VideoEditorBaseActivity;

    invoke-virtual {v0}, Lcom/android/videoeditor/VideoEditorBaseActivity;->getMediaLayout()Lcom/android/videoeditor/widgets/MediaLinearLayout;

    move-result-object v0

    iget-object v1, p0, Lcom/android/videoeditor/VideoEditorBaseActivity$ServiceListener;->this$0:Lcom/android/videoeditor/VideoEditorBaseActivity;

    iget-object v1, v1, Lcom/android/videoeditor/VideoEditorBaseActivity;->mProject:Lcom/android/videoeditor/service/VideoEditorProject;

    invoke-virtual {v1, p3}, Lcom/android/videoeditor/service/VideoEditorProject;->getMediaItem(Ljava/lang/String;)Lcom/android/videoeditor/service/MovieMediaItem;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/android/videoeditor/widgets/MediaLinearLayout;->updateMediaItem(Lcom/android/videoeditor/service/MovieMediaItem;)V

    goto :goto_0
.end method

.method public onMediaItemAdded(Ljava/lang/String;Ljava/lang/String;Lcom/android/videoeditor/service/MovieMediaItem;Ljava/lang/String;Ljava/lang/Class;Ljava/lang/Integer;Ljava/lang/Exception;)V
    .locals 3
    .param p1    # Ljava/lang/String;
    .param p2    # Ljava/lang/String;
    .param p3    # Lcom/android/videoeditor/service/MovieMediaItem;
    .param p4    # Ljava/lang/String;
    .param p6    # Ljava/lang/Integer;
    .param p7    # Ljava/lang/Exception;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Lcom/android/videoeditor/service/MovieMediaItem;",
            "Ljava/lang/String;",
            "Ljava/lang/Class",
            "<*>;",
            "Ljava/lang/Integer;",
            "Ljava/lang/Exception;",
            ")V"
        }
    .end annotation

    const/4 v2, 0x1

    iget-object v0, p0, Lcom/android/videoeditor/VideoEditorBaseActivity$ServiceListener;->this$0:Lcom/android/videoeditor/VideoEditorBaseActivity;

    iget-object v0, v0, Lcom/android/videoeditor/VideoEditorBaseActivity;->mProjectPath:Ljava/lang/String;

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/videoeditor/VideoEditorBaseActivity$ServiceListener;->this$0:Lcom/android/videoeditor/VideoEditorBaseActivity;

    iget-object v0, v0, Lcom/android/videoeditor/VideoEditorBaseActivity;->mProject:Lcom/android/videoeditor/service/VideoEditorProject;

    if-nez v0, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    if-eqz p7, :cond_3

    invoke-virtual {p5}, Ljava/lang/Class;->getCanonicalName()Ljava/lang/String;

    move-result-object v0

    const-class v1, Landroid/media/videoeditor/MediaVideoItem;

    invoke-virtual {v1}, Ljava/lang/Class;->getCanonicalName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/android/videoeditor/VideoEditorBaseActivity$ServiceListener;->this$0:Lcom/android/videoeditor/VideoEditorBaseActivity;

    const v1, 0x7f090041

    invoke-static {v0, v1, v2}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    goto :goto_0

    :cond_2
    iget-object v0, p0, Lcom/android/videoeditor/VideoEditorBaseActivity$ServiceListener;->this$0:Lcom/android/videoeditor/VideoEditorBaseActivity;

    const v1, 0x7f090042

    invoke-static {v0, v1, v2}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    goto :goto_0

    :cond_3
    iget-object v0, p0, Lcom/android/videoeditor/VideoEditorBaseActivity$ServiceListener;->this$0:Lcom/android/videoeditor/VideoEditorBaseActivity;

    invoke-virtual {v0}, Lcom/android/videoeditor/VideoEditorBaseActivity;->getMediaLayout()Lcom/android/videoeditor/widgets/MediaLinearLayout;

    move-result-object v0

    invoke-virtual {v0, p3, p4}, Lcom/android/videoeditor/widgets/MediaLinearLayout;->insertMediaItem(Lcom/android/videoeditor/service/MovieMediaItem;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/android/videoeditor/VideoEditorBaseActivity$ServiceListener;->this$0:Lcom/android/videoeditor/VideoEditorBaseActivity;

    invoke-virtual {v0}, Lcom/android/videoeditor/VideoEditorBaseActivity;->getOverlayLayout()Lcom/android/videoeditor/widgets/OverlayLinearLayout;

    move-result-object v0

    invoke-virtual {v0, p3, p4}, Lcom/android/videoeditor/widgets/OverlayLinearLayout;->insertMediaItem(Lcom/android/videoeditor/service/MovieMediaItem;Ljava/lang/String;)V

    if-eqz p6, :cond_4

    iget-object v0, p0, Lcom/android/videoeditor/VideoEditorBaseActivity$ServiceListener;->this$0:Lcom/android/videoeditor/VideoEditorBaseActivity;

    invoke-virtual {p6}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/android/videoeditor/VideoEditorBaseActivity;->setAspectRatio(I)V

    :cond_4
    iget-object v0, p0, Lcom/android/videoeditor/VideoEditorBaseActivity$ServiceListener;->this$0:Lcom/android/videoeditor/VideoEditorBaseActivity;

    invoke-virtual {v0}, Lcom/android/videoeditor/VideoEditorBaseActivity;->updateTimelineDuration()V

    goto :goto_0
.end method

.method public onMediaItemBoundariesSet(Ljava/lang/String;Ljava/lang/String;JJLjava/lang/Exception;)V
    .locals 4
    .param p1    # Ljava/lang/String;
    .param p2    # Ljava/lang/String;
    .param p3    # J
    .param p5    # J
    .param p7    # Ljava/lang/Exception;

    iget-object v1, p0, Lcom/android/videoeditor/VideoEditorBaseActivity$ServiceListener;->this$0:Lcom/android/videoeditor/VideoEditorBaseActivity;

    iget-object v1, v1, Lcom/android/videoeditor/VideoEditorBaseActivity;->mProjectPath:Ljava/lang/String;

    invoke-virtual {p1, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v1, p0, Lcom/android/videoeditor/VideoEditorBaseActivity$ServiceListener;->this$0:Lcom/android/videoeditor/VideoEditorBaseActivity;

    iget-object v1, v1, Lcom/android/videoeditor/VideoEditorBaseActivity;->mProject:Lcom/android/videoeditor/service/VideoEditorProject;

    if-eqz v1, :cond_0

    if-eqz p7, :cond_2

    iget-object v1, p0, Lcom/android/videoeditor/VideoEditorBaseActivity$ServiceListener;->this$0:Lcom/android/videoeditor/VideoEditorBaseActivity;

    const v2, 0x7f090047

    const/4 v3, 0x1

    invoke-static {v1, v2, v3}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v1

    invoke-virtual {v1}, Landroid/widget/Toast;->show()V

    goto :goto_0

    :cond_2
    iget-object v1, p0, Lcom/android/videoeditor/VideoEditorBaseActivity$ServiceListener;->this$0:Lcom/android/videoeditor/VideoEditorBaseActivity;

    iget-object v1, v1, Lcom/android/videoeditor/VideoEditorBaseActivity;->mProject:Lcom/android/videoeditor/service/VideoEditorProject;

    invoke-virtual {v1, p2}, Lcom/android/videoeditor/service/VideoEditorProject;->getMediaItem(Ljava/lang/String;)Lcom/android/videoeditor/service/MovieMediaItem;

    move-result-object v0

    iget-object v1, p0, Lcom/android/videoeditor/VideoEditorBaseActivity$ServiceListener;->this$0:Lcom/android/videoeditor/VideoEditorBaseActivity;

    invoke-virtual {v1}, Lcom/android/videoeditor/VideoEditorBaseActivity;->getMediaLayout()Lcom/android/videoeditor/widgets/MediaLinearLayout;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/android/videoeditor/widgets/MediaLinearLayout;->updateMediaItem(Lcom/android/videoeditor/service/MovieMediaItem;)V

    iget-object v1, p0, Lcom/android/videoeditor/VideoEditorBaseActivity$ServiceListener;->this$0:Lcom/android/videoeditor/VideoEditorBaseActivity;

    invoke-virtual {v1}, Lcom/android/videoeditor/VideoEditorBaseActivity;->getOverlayLayout()Lcom/android/videoeditor/widgets/OverlayLinearLayout;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/android/videoeditor/widgets/OverlayLinearLayout;->updateMediaItem(Lcom/android/videoeditor/service/MovieMediaItem;)V

    iget-object v1, p0, Lcom/android/videoeditor/VideoEditorBaseActivity$ServiceListener;->this$0:Lcom/android/videoeditor/VideoEditorBaseActivity;

    invoke-virtual {v1}, Lcom/android/videoeditor/VideoEditorBaseActivity;->updateTimelineDuration()V

    goto :goto_0
.end method

.method public onMediaItemDurationSet(Ljava/lang/String;Ljava/lang/String;JLjava/lang/Exception;)V
    .locals 4
    .param p1    # Ljava/lang/String;
    .param p2    # Ljava/lang/String;
    .param p3    # J
    .param p5    # Ljava/lang/Exception;

    iget-object v1, p0, Lcom/android/videoeditor/VideoEditorBaseActivity$ServiceListener;->this$0:Lcom/android/videoeditor/VideoEditorBaseActivity;

    iget-object v1, v1, Lcom/android/videoeditor/VideoEditorBaseActivity;->mProjectPath:Ljava/lang/String;

    invoke-virtual {p1, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v1, p0, Lcom/android/videoeditor/VideoEditorBaseActivity$ServiceListener;->this$0:Lcom/android/videoeditor/VideoEditorBaseActivity;

    iget-object v1, v1, Lcom/android/videoeditor/VideoEditorBaseActivity;->mProject:Lcom/android/videoeditor/service/VideoEditorProject;

    if-eqz v1, :cond_0

    if-eqz p5, :cond_2

    iget-object v1, p0, Lcom/android/videoeditor/VideoEditorBaseActivity$ServiceListener;->this$0:Lcom/android/videoeditor/VideoEditorBaseActivity;

    const v2, 0x7f090046

    const/4 v3, 0x1

    invoke-static {v1, v2, v3}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v1

    invoke-virtual {v1}, Landroid/widget/Toast;->show()V

    goto :goto_0

    :cond_2
    iget-object v1, p0, Lcom/android/videoeditor/VideoEditorBaseActivity$ServiceListener;->this$0:Lcom/android/videoeditor/VideoEditorBaseActivity;

    iget-object v1, v1, Lcom/android/videoeditor/VideoEditorBaseActivity;->mProject:Lcom/android/videoeditor/service/VideoEditorProject;

    invoke-virtual {v1, p2}, Lcom/android/videoeditor/service/VideoEditorProject;->getMediaItem(Ljava/lang/String;)Lcom/android/videoeditor/service/MovieMediaItem;

    move-result-object v0

    iget-object v1, p0, Lcom/android/videoeditor/VideoEditorBaseActivity$ServiceListener;->this$0:Lcom/android/videoeditor/VideoEditorBaseActivity;

    invoke-virtual {v1}, Lcom/android/videoeditor/VideoEditorBaseActivity;->getMediaLayout()Lcom/android/videoeditor/widgets/MediaLinearLayout;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/android/videoeditor/widgets/MediaLinearLayout;->updateMediaItem(Lcom/android/videoeditor/service/MovieMediaItem;)V

    iget-object v1, p0, Lcom/android/videoeditor/VideoEditorBaseActivity$ServiceListener;->this$0:Lcom/android/videoeditor/VideoEditorBaseActivity;

    invoke-virtual {v1}, Lcom/android/videoeditor/VideoEditorBaseActivity;->getOverlayLayout()Lcom/android/videoeditor/widgets/OverlayLinearLayout;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/android/videoeditor/widgets/OverlayLinearLayout;->updateMediaItem(Lcom/android/videoeditor/service/MovieMediaItem;)V

    iget-object v1, p0, Lcom/android/videoeditor/VideoEditorBaseActivity$ServiceListener;->this$0:Lcom/android/videoeditor/VideoEditorBaseActivity;

    invoke-virtual {v1}, Lcom/android/videoeditor/VideoEditorBaseActivity;->updateTimelineDuration()V

    goto :goto_0
.end method

.method public onMediaItemMoved(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Exception;)V
    .locals 3
    .param p1    # Ljava/lang/String;
    .param p2    # Ljava/lang/String;
    .param p3    # Ljava/lang/String;
    .param p4    # Ljava/lang/Exception;

    iget-object v0, p0, Lcom/android/videoeditor/VideoEditorBaseActivity$ServiceListener;->this$0:Lcom/android/videoeditor/VideoEditorBaseActivity;

    iget-object v0, v0, Lcom/android/videoeditor/VideoEditorBaseActivity;->mProjectPath:Ljava/lang/String;

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v0, p0, Lcom/android/videoeditor/VideoEditorBaseActivity$ServiceListener;->this$0:Lcom/android/videoeditor/VideoEditorBaseActivity;

    iget-object v0, v0, Lcom/android/videoeditor/VideoEditorBaseActivity;->mProject:Lcom/android/videoeditor/service/VideoEditorProject;

    if-eqz v0, :cond_0

    if-eqz p4, :cond_2

    iget-object v0, p0, Lcom/android/videoeditor/VideoEditorBaseActivity$ServiceListener;->this$0:Lcom/android/videoeditor/VideoEditorBaseActivity;

    const v1, 0x7f090043

    const/4 v2, 0x1

    invoke-static {v0, v1, v2}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    goto :goto_0

    :cond_2
    iget-object v0, p0, Lcom/android/videoeditor/VideoEditorBaseActivity$ServiceListener;->this$0:Lcom/android/videoeditor/VideoEditorBaseActivity;

    invoke-virtual {v0}, Lcom/android/videoeditor/VideoEditorBaseActivity;->getMediaLayout()Lcom/android/videoeditor/widgets/MediaLinearLayout;

    move-result-object v0

    iget-object v1, p0, Lcom/android/videoeditor/VideoEditorBaseActivity$ServiceListener;->this$0:Lcom/android/videoeditor/VideoEditorBaseActivity;

    iget-object v1, v1, Lcom/android/videoeditor/VideoEditorBaseActivity;->mProject:Lcom/android/videoeditor/service/VideoEditorProject;

    invoke-virtual {v1}, Lcom/android/videoeditor/service/VideoEditorProject;->getMediaItems()Ljava/util/List;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/android/videoeditor/widgets/MediaLinearLayout;->addMediaItems(Ljava/util/List;)V

    iget-object v0, p0, Lcom/android/videoeditor/VideoEditorBaseActivity$ServiceListener;->this$0:Lcom/android/videoeditor/VideoEditorBaseActivity;

    invoke-virtual {v0}, Lcom/android/videoeditor/VideoEditorBaseActivity;->getOverlayLayout()Lcom/android/videoeditor/widgets/OverlayLinearLayout;

    move-result-object v0

    iget-object v1, p0, Lcom/android/videoeditor/VideoEditorBaseActivity$ServiceListener;->this$0:Lcom/android/videoeditor/VideoEditorBaseActivity;

    iget-object v1, v1, Lcom/android/videoeditor/VideoEditorBaseActivity;->mProject:Lcom/android/videoeditor/service/VideoEditorProject;

    invoke-virtual {v1}, Lcom/android/videoeditor/service/VideoEditorProject;->getMediaItems()Ljava/util/List;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/android/videoeditor/widgets/OverlayLinearLayout;->addMediaItems(Ljava/util/List;)V

    iget-object v0, p0, Lcom/android/videoeditor/VideoEditorBaseActivity$ServiceListener;->this$0:Lcom/android/videoeditor/VideoEditorBaseActivity;

    invoke-virtual {v0}, Lcom/android/videoeditor/VideoEditorBaseActivity;->updateTimelineDuration()V

    goto :goto_0
.end method

.method public onMediaItemRemoved(Ljava/lang/String;Ljava/lang/String;Lcom/android/videoeditor/service/MovieTransition;Ljava/lang/Exception;)V
    .locals 3
    .param p1    # Ljava/lang/String;
    .param p2    # Ljava/lang/String;
    .param p3    # Lcom/android/videoeditor/service/MovieTransition;
    .param p4    # Ljava/lang/Exception;

    iget-object v0, p0, Lcom/android/videoeditor/VideoEditorBaseActivity$ServiceListener;->this$0:Lcom/android/videoeditor/VideoEditorBaseActivity;

    iget-object v0, v0, Lcom/android/videoeditor/VideoEditorBaseActivity;->mProjectPath:Ljava/lang/String;

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v0, p0, Lcom/android/videoeditor/VideoEditorBaseActivity$ServiceListener;->this$0:Lcom/android/videoeditor/VideoEditorBaseActivity;

    iget-object v0, v0, Lcom/android/videoeditor/VideoEditorBaseActivity;->mProject:Lcom/android/videoeditor/service/VideoEditorProject;

    if-eqz v0, :cond_0

    if-eqz p4, :cond_2

    iget-object v0, p0, Lcom/android/videoeditor/VideoEditorBaseActivity$ServiceListener;->this$0:Lcom/android/videoeditor/VideoEditorBaseActivity;

    const v1, 0x7f090044

    const/4 v2, 0x1

    invoke-static {v0, v1, v2}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    goto :goto_0

    :cond_2
    iget-object v0, p0, Lcom/android/videoeditor/VideoEditorBaseActivity$ServiceListener;->this$0:Lcom/android/videoeditor/VideoEditorBaseActivity;

    invoke-virtual {v0}, Lcom/android/videoeditor/VideoEditorBaseActivity;->getMediaLayout()Lcom/android/videoeditor/widgets/MediaLinearLayout;

    move-result-object v0

    invoke-virtual {v0, p2, p3}, Lcom/android/videoeditor/widgets/MediaLinearLayout;->removeMediaItem(Ljava/lang/String;Lcom/android/videoeditor/service/MovieTransition;)Landroid/view/View;

    iget-object v0, p0, Lcom/android/videoeditor/VideoEditorBaseActivity$ServiceListener;->this$0:Lcom/android/videoeditor/VideoEditorBaseActivity;

    invoke-virtual {v0}, Lcom/android/videoeditor/VideoEditorBaseActivity;->getOverlayLayout()Lcom/android/videoeditor/widgets/OverlayLinearLayout;

    move-result-object v0

    invoke-virtual {v0, p2}, Lcom/android/videoeditor/widgets/OverlayLinearLayout;->removeMediaItem(Ljava/lang/String;)Landroid/view/View;

    iget-object v0, p0, Lcom/android/videoeditor/VideoEditorBaseActivity$ServiceListener;->this$0:Lcom/android/videoeditor/VideoEditorBaseActivity;

    invoke-virtual {v0}, Lcom/android/videoeditor/VideoEditorBaseActivity;->updateTimelineDuration()V

    goto :goto_0
.end method

.method public onMediaItemRenderingModeSet(Ljava/lang/String;Ljava/lang/String;ILjava/lang/Exception;)V
    .locals 3
    .param p1    # Ljava/lang/String;
    .param p2    # Ljava/lang/String;
    .param p3    # I
    .param p4    # Ljava/lang/Exception;

    iget-object v0, p0, Lcom/android/videoeditor/VideoEditorBaseActivity$ServiceListener;->this$0:Lcom/android/videoeditor/VideoEditorBaseActivity;

    iget-object v0, v0, Lcom/android/videoeditor/VideoEditorBaseActivity;->mProjectPath:Ljava/lang/String;

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v0, p0, Lcom/android/videoeditor/VideoEditorBaseActivity$ServiceListener;->this$0:Lcom/android/videoeditor/VideoEditorBaseActivity;

    iget-object v0, v0, Lcom/android/videoeditor/VideoEditorBaseActivity;->mProject:Lcom/android/videoeditor/service/VideoEditorProject;

    if-eqz v0, :cond_0

    if-eqz p4, :cond_0

    iget-object v0, p0, Lcom/android/videoeditor/VideoEditorBaseActivity$ServiceListener;->this$0:Lcom/android/videoeditor/VideoEditorBaseActivity;

    const v1, 0x7f090045

    const/4 v2, 0x1

    invoke-static {v0, v1, v2}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    goto :goto_0
.end method

.method public onMediaItemThumbnail(Ljava/lang/String;Ljava/lang/String;Landroid/graphics/Bitmap;IILjava/lang/Exception;)Z
    .locals 2
    .param p1    # Ljava/lang/String;
    .param p2    # Ljava/lang/String;
    .param p3    # Landroid/graphics/Bitmap;
    .param p4    # I
    .param p5    # I
    .param p6    # Ljava/lang/Exception;

    const/4 v0, 0x0

    iget-object v1, p0, Lcom/android/videoeditor/VideoEditorBaseActivity$ServiceListener;->this$0:Lcom/android/videoeditor/VideoEditorBaseActivity;

    iget-object v1, v1, Lcom/android/videoeditor/VideoEditorBaseActivity;->mProjectPath:Ljava/lang/String;

    invoke-virtual {p1, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_1

    :cond_0
    :goto_0
    return v0

    :cond_1
    iget-object v1, p0, Lcom/android/videoeditor/VideoEditorBaseActivity$ServiceListener;->this$0:Lcom/android/videoeditor/VideoEditorBaseActivity;

    iget-object v1, v1, Lcom/android/videoeditor/VideoEditorBaseActivity;->mProject:Lcom/android/videoeditor/service/VideoEditorProject;

    if-eqz v1, :cond_0

    if-nez p6, :cond_0

    iget-object v0, p0, Lcom/android/videoeditor/VideoEditorBaseActivity$ServiceListener;->this$0:Lcom/android/videoeditor/VideoEditorBaseActivity;

    invoke-virtual {v0}, Lcom/android/videoeditor/VideoEditorBaseActivity;->getMediaLayout()Lcom/android/videoeditor/widgets/MediaLinearLayout;

    move-result-object v0

    invoke-virtual {v0, p2, p3, p4, p5}, Lcom/android/videoeditor/widgets/MediaLinearLayout;->setMediaItemThumbnail(Ljava/lang/String;Landroid/graphics/Bitmap;II)Z

    move-result v0

    goto :goto_0
.end method

.method public onMediaLoaded(Ljava/lang/String;Landroid/net/Uri;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Exception;)V
    .locals 3
    .param p1    # Ljava/lang/String;
    .param p2    # Landroid/net/Uri;
    .param p3    # Ljava/lang/String;
    .param p4    # Ljava/lang/String;
    .param p5    # Ljava/lang/Exception;

    iget-object v0, p0, Lcom/android/videoeditor/VideoEditorBaseActivity$ServiceListener;->this$0:Lcom/android/videoeditor/VideoEditorBaseActivity;

    iget-object v0, v0, Lcom/android/videoeditor/VideoEditorBaseActivity;->mProjectPath:Ljava/lang/String;

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v0, p0, Lcom/android/videoeditor/VideoEditorBaseActivity$ServiceListener;->this$0:Lcom/android/videoeditor/VideoEditorBaseActivity;

    iget-object v0, v0, Lcom/android/videoeditor/VideoEditorBaseActivity;->mProject:Lcom/android/videoeditor/service/VideoEditorProject;

    if-eqz v0, :cond_0

    if-eqz p5, :cond_0

    iget-object v0, p0, Lcom/android/videoeditor/VideoEditorBaseActivity$ServiceListener;->this$0:Lcom/android/videoeditor/VideoEditorBaseActivity;

    const v1, 0x7f090056

    const/4 v2, 0x1

    invoke-static {v0, v1, v2}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    goto :goto_0
.end method

.method public onOverlayAdded(Ljava/lang/String;Lcom/android/videoeditor/service/MovieOverlay;Ljava/lang/String;Ljava/lang/Exception;)V
    .locals 3
    .param p1    # Ljava/lang/String;
    .param p2    # Lcom/android/videoeditor/service/MovieOverlay;
    .param p3    # Ljava/lang/String;
    .param p4    # Ljava/lang/Exception;

    iget-object v0, p0, Lcom/android/videoeditor/VideoEditorBaseActivity$ServiceListener;->this$0:Lcom/android/videoeditor/VideoEditorBaseActivity;

    iget-object v0, v0, Lcom/android/videoeditor/VideoEditorBaseActivity;->mProjectPath:Ljava/lang/String;

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v0, p0, Lcom/android/videoeditor/VideoEditorBaseActivity$ServiceListener;->this$0:Lcom/android/videoeditor/VideoEditorBaseActivity;

    iget-object v0, v0, Lcom/android/videoeditor/VideoEditorBaseActivity;->mProject:Lcom/android/videoeditor/service/VideoEditorProject;

    if-eqz v0, :cond_0

    if-eqz p4, :cond_2

    iget-object v0, p0, Lcom/android/videoeditor/VideoEditorBaseActivity$ServiceListener;->this$0:Lcom/android/videoeditor/VideoEditorBaseActivity;

    const v1, 0x7f09004b

    const/4 v2, 0x1

    invoke-static {v0, v1, v2}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    goto :goto_0

    :cond_2
    iget-object v0, p0, Lcom/android/videoeditor/VideoEditorBaseActivity$ServiceListener;->this$0:Lcom/android/videoeditor/VideoEditorBaseActivity;

    invoke-virtual {v0}, Lcom/android/videoeditor/VideoEditorBaseActivity;->getMediaLayout()Lcom/android/videoeditor/widgets/MediaLinearLayout;

    move-result-object v0

    invoke-virtual {v0}, Lcom/android/videoeditor/widgets/MediaLinearLayout;->invalidateActionBar()V

    iget-object v0, p0, Lcom/android/videoeditor/VideoEditorBaseActivity$ServiceListener;->this$0:Lcom/android/videoeditor/VideoEditorBaseActivity;

    invoke-virtual {v0}, Lcom/android/videoeditor/VideoEditorBaseActivity;->getOverlayLayout()Lcom/android/videoeditor/widgets/OverlayLinearLayout;

    move-result-object v0

    invoke-virtual {v0, p3, p2}, Lcom/android/videoeditor/widgets/OverlayLinearLayout;->addOverlay(Ljava/lang/String;Lcom/android/videoeditor/service/MovieOverlay;)V

    goto :goto_0
.end method

.method public onOverlayDurationSet(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;JLjava/lang/Exception;)V
    .locals 3
    .param p1    # Ljava/lang/String;
    .param p2    # Ljava/lang/String;
    .param p3    # Ljava/lang/String;
    .param p4    # J
    .param p6    # Ljava/lang/Exception;

    iget-object v0, p0, Lcom/android/videoeditor/VideoEditorBaseActivity$ServiceListener;->this$0:Lcom/android/videoeditor/VideoEditorBaseActivity;

    iget-object v0, v0, Lcom/android/videoeditor/VideoEditorBaseActivity;->mProjectPath:Ljava/lang/String;

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v0, p0, Lcom/android/videoeditor/VideoEditorBaseActivity$ServiceListener;->this$0:Lcom/android/videoeditor/VideoEditorBaseActivity;

    iget-object v0, v0, Lcom/android/videoeditor/VideoEditorBaseActivity;->mProject:Lcom/android/videoeditor/service/VideoEditorProject;

    if-eqz v0, :cond_0

    if-eqz p6, :cond_0

    iget-object v0, p0, Lcom/android/videoeditor/VideoEditorBaseActivity$ServiceListener;->this$0:Lcom/android/videoeditor/VideoEditorBaseActivity;

    const v1, 0x7f09004d

    const/4 v2, 0x1

    invoke-static {v0, v1, v2}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    goto :goto_0
.end method

.method public onOverlayRemoved(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Exception;)V
    .locals 3
    .param p1    # Ljava/lang/String;
    .param p2    # Ljava/lang/String;
    .param p3    # Ljava/lang/String;
    .param p4    # Ljava/lang/Exception;

    iget-object v0, p0, Lcom/android/videoeditor/VideoEditorBaseActivity$ServiceListener;->this$0:Lcom/android/videoeditor/VideoEditorBaseActivity;

    iget-object v0, v0, Lcom/android/videoeditor/VideoEditorBaseActivity;->mProjectPath:Ljava/lang/String;

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v0, p0, Lcom/android/videoeditor/VideoEditorBaseActivity$ServiceListener;->this$0:Lcom/android/videoeditor/VideoEditorBaseActivity;

    iget-object v0, v0, Lcom/android/videoeditor/VideoEditorBaseActivity;->mProject:Lcom/android/videoeditor/service/VideoEditorProject;

    if-eqz v0, :cond_0

    if-eqz p4, :cond_2

    iget-object v0, p0, Lcom/android/videoeditor/VideoEditorBaseActivity$ServiceListener;->this$0:Lcom/android/videoeditor/VideoEditorBaseActivity;

    const v1, 0x7f09004c

    const/4 v2, 0x1

    invoke-static {v0, v1, v2}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    goto :goto_0

    :cond_2
    iget-object v0, p0, Lcom/android/videoeditor/VideoEditorBaseActivity$ServiceListener;->this$0:Lcom/android/videoeditor/VideoEditorBaseActivity;

    invoke-virtual {v0}, Lcom/android/videoeditor/VideoEditorBaseActivity;->getOverlayLayout()Lcom/android/videoeditor/widgets/OverlayLinearLayout;

    move-result-object v0

    invoke-virtual {v0, p3, p2}, Lcom/android/videoeditor/widgets/OverlayLinearLayout;->removeOverlay(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public onOverlayStartTimeSet(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;JLjava/lang/Exception;)V
    .locals 3
    .param p1    # Ljava/lang/String;
    .param p2    # Ljava/lang/String;
    .param p3    # Ljava/lang/String;
    .param p4    # J
    .param p6    # Ljava/lang/Exception;

    iget-object v0, p0, Lcom/android/videoeditor/VideoEditorBaseActivity$ServiceListener;->this$0:Lcom/android/videoeditor/VideoEditorBaseActivity;

    iget-object v0, v0, Lcom/android/videoeditor/VideoEditorBaseActivity;->mProjectPath:Ljava/lang/String;

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v0, p0, Lcom/android/videoeditor/VideoEditorBaseActivity$ServiceListener;->this$0:Lcom/android/videoeditor/VideoEditorBaseActivity;

    iget-object v0, v0, Lcom/android/videoeditor/VideoEditorBaseActivity;->mProject:Lcom/android/videoeditor/service/VideoEditorProject;

    if-eqz v0, :cond_0

    if-eqz p6, :cond_0

    iget-object v0, p0, Lcom/android/videoeditor/VideoEditorBaseActivity$ServiceListener;->this$0:Lcom/android/videoeditor/VideoEditorBaseActivity;

    const v1, 0x7f09004e

    const/4 v2, 0x1

    invoke-static {v0, v1, v2}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    goto :goto_0
.end method

.method public onOverlayUserAttributesSet(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Landroid/os/Bundle;Ljava/lang/Exception;)V
    .locals 3
    .param p1    # Ljava/lang/String;
    .param p2    # Ljava/lang/String;
    .param p3    # Ljava/lang/String;
    .param p4    # Landroid/os/Bundle;
    .param p5    # Ljava/lang/Exception;

    iget-object v0, p0, Lcom/android/videoeditor/VideoEditorBaseActivity$ServiceListener;->this$0:Lcom/android/videoeditor/VideoEditorBaseActivity;

    iget-object v0, v0, Lcom/android/videoeditor/VideoEditorBaseActivity;->mProjectPath:Ljava/lang/String;

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v0, p0, Lcom/android/videoeditor/VideoEditorBaseActivity$ServiceListener;->this$0:Lcom/android/videoeditor/VideoEditorBaseActivity;

    iget-object v0, v0, Lcom/android/videoeditor/VideoEditorBaseActivity;->mProject:Lcom/android/videoeditor/service/VideoEditorProject;

    if-eqz v0, :cond_0

    if-eqz p5, :cond_2

    iget-object v0, p0, Lcom/android/videoeditor/VideoEditorBaseActivity$ServiceListener;->this$0:Lcom/android/videoeditor/VideoEditorBaseActivity;

    const v1, 0x7f09004f

    const/4 v2, 0x1

    invoke-static {v0, v1, v2}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    goto :goto_0

    :cond_2
    iget-object v0, p0, Lcom/android/videoeditor/VideoEditorBaseActivity$ServiceListener;->this$0:Lcom/android/videoeditor/VideoEditorBaseActivity;

    invoke-virtual {v0}, Lcom/android/videoeditor/VideoEditorBaseActivity;->getOverlayLayout()Lcom/android/videoeditor/widgets/OverlayLinearLayout;

    move-result-object v0

    invoke-virtual {v0, p3, p2, p4}, Lcom/android/videoeditor/widgets/OverlayLinearLayout;->updateOverlayAttributes(Ljava/lang/String;Ljava/lang/String;Landroid/os/Bundle;)V

    goto :goto_0
.end method

.method public onProjectEditState(Ljava/lang/String;Z)V
    .locals 1
    .param p1    # Ljava/lang/String;
    .param p2    # Z

    iget-object v0, p0, Lcom/android/videoeditor/VideoEditorBaseActivity$ServiceListener;->this$0:Lcom/android/videoeditor/VideoEditorBaseActivity;

    iget-object v0, v0, Lcom/android/videoeditor/VideoEditorBaseActivity;->mProjectPath:Ljava/lang/String;

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v0, p0, Lcom/android/videoeditor/VideoEditorBaseActivity$ServiceListener;->this$0:Lcom/android/videoeditor/VideoEditorBaseActivity;

    # getter for: Lcom/android/videoeditor/VideoEditorBaseActivity;->mProjectEditState:Z
    invoke-static {v0}, Lcom/android/videoeditor/VideoEditorBaseActivity;->access$100(Lcom/android/videoeditor/VideoEditorBaseActivity;)Z

    move-result v0

    if-eq v0, p2, :cond_0

    iget-object v0, p0, Lcom/android/videoeditor/VideoEditorBaseActivity$ServiceListener;->this$0:Lcom/android/videoeditor/VideoEditorBaseActivity;

    # setter for: Lcom/android/videoeditor/VideoEditorBaseActivity;->mProjectEditState:Z
    invoke-static {v0, p2}, Lcom/android/videoeditor/VideoEditorBaseActivity;->access$102(Lcom/android/videoeditor/VideoEditorBaseActivity;Z)Z

    iget-object v0, p0, Lcom/android/videoeditor/VideoEditorBaseActivity$ServiceListener;->this$0:Lcom/android/videoeditor/VideoEditorBaseActivity;

    invoke-virtual {v0, p2}, Lcom/android/videoeditor/VideoEditorBaseActivity;->onProjectEditStateChange(Z)V

    goto :goto_0
.end method

.method public onTransitionDurationSet(Ljava/lang/String;Ljava/lang/String;JLjava/lang/Exception;)V
    .locals 3
    .param p1    # Ljava/lang/String;
    .param p2    # Ljava/lang/String;
    .param p3    # J
    .param p5    # Ljava/lang/Exception;

    iget-object v0, p0, Lcom/android/videoeditor/VideoEditorBaseActivity$ServiceListener;->this$0:Lcom/android/videoeditor/VideoEditorBaseActivity;

    iget-object v0, v0, Lcom/android/videoeditor/VideoEditorBaseActivity;->mProjectPath:Ljava/lang/String;

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v0, p0, Lcom/android/videoeditor/VideoEditorBaseActivity$ServiceListener;->this$0:Lcom/android/videoeditor/VideoEditorBaseActivity;

    iget-object v0, v0, Lcom/android/videoeditor/VideoEditorBaseActivity;->mProject:Lcom/android/videoeditor/service/VideoEditorProject;

    if-eqz v0, :cond_0

    if-eqz p5, :cond_2

    iget-object v0, p0, Lcom/android/videoeditor/VideoEditorBaseActivity$ServiceListener;->this$0:Lcom/android/videoeditor/VideoEditorBaseActivity;

    const v1, 0x7f09004a

    const/4 v2, 0x1

    invoke-static {v0, v1, v2}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    goto :goto_0

    :cond_2
    iget-object v0, p0, Lcom/android/videoeditor/VideoEditorBaseActivity$ServiceListener;->this$0:Lcom/android/videoeditor/VideoEditorBaseActivity;

    invoke-virtual {v0}, Lcom/android/videoeditor/VideoEditorBaseActivity;->getMediaLayout()Lcom/android/videoeditor/widgets/MediaLinearLayout;

    move-result-object v0

    invoke-virtual {v0, p2}, Lcom/android/videoeditor/widgets/MediaLinearLayout;->updateTransition(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/android/videoeditor/VideoEditorBaseActivity$ServiceListener;->this$0:Lcom/android/videoeditor/VideoEditorBaseActivity;

    invoke-virtual {v0}, Lcom/android/videoeditor/VideoEditorBaseActivity;->getOverlayLayout()Lcom/android/videoeditor/widgets/OverlayLinearLayout;

    move-result-object v0

    invoke-virtual {v0}, Lcom/android/videoeditor/widgets/OverlayLinearLayout;->refresh()V

    iget-object v0, p0, Lcom/android/videoeditor/VideoEditorBaseActivity$ServiceListener;->this$0:Lcom/android/videoeditor/VideoEditorBaseActivity;

    invoke-virtual {v0}, Lcom/android/videoeditor/VideoEditorBaseActivity;->updateTimelineDuration()V

    goto :goto_0
.end method

.method public onTransitionInserted(Ljava/lang/String;Lcom/android/videoeditor/service/MovieTransition;Ljava/lang/String;Ljava/lang/Exception;)V
    .locals 3
    .param p1    # Ljava/lang/String;
    .param p2    # Lcom/android/videoeditor/service/MovieTransition;
    .param p3    # Ljava/lang/String;
    .param p4    # Ljava/lang/Exception;

    iget-object v0, p0, Lcom/android/videoeditor/VideoEditorBaseActivity$ServiceListener;->this$0:Lcom/android/videoeditor/VideoEditorBaseActivity;

    iget-object v0, v0, Lcom/android/videoeditor/VideoEditorBaseActivity;->mProjectPath:Ljava/lang/String;

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v0, p0, Lcom/android/videoeditor/VideoEditorBaseActivity$ServiceListener;->this$0:Lcom/android/videoeditor/VideoEditorBaseActivity;

    iget-object v0, v0, Lcom/android/videoeditor/VideoEditorBaseActivity;->mProject:Lcom/android/videoeditor/service/VideoEditorProject;

    if-eqz v0, :cond_0

    if-eqz p4, :cond_2

    iget-object v0, p0, Lcom/android/videoeditor/VideoEditorBaseActivity$ServiceListener;->this$0:Lcom/android/videoeditor/VideoEditorBaseActivity;

    const v1, 0x7f090048

    const/4 v2, 0x1

    invoke-static {v0, v1, v2}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    goto :goto_0

    :cond_2
    iget-object v0, p0, Lcom/android/videoeditor/VideoEditorBaseActivity$ServiceListener;->this$0:Lcom/android/videoeditor/VideoEditorBaseActivity;

    invoke-virtual {v0}, Lcom/android/videoeditor/VideoEditorBaseActivity;->getMediaLayout()Lcom/android/videoeditor/widgets/MediaLinearLayout;

    move-result-object v0

    invoke-virtual {v0, p2, p3}, Lcom/android/videoeditor/widgets/MediaLinearLayout;->addTransition(Lcom/android/videoeditor/service/MovieTransition;Ljava/lang/String;)Landroid/view/View;

    iget-object v0, p0, Lcom/android/videoeditor/VideoEditorBaseActivity$ServiceListener;->this$0:Lcom/android/videoeditor/VideoEditorBaseActivity;

    invoke-virtual {v0}, Lcom/android/videoeditor/VideoEditorBaseActivity;->updateTimelineDuration()V

    goto :goto_0
.end method

.method public onTransitionRemoved(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Exception;)V
    .locals 3
    .param p1    # Ljava/lang/String;
    .param p2    # Ljava/lang/String;
    .param p3    # Ljava/lang/Exception;

    iget-object v0, p0, Lcom/android/videoeditor/VideoEditorBaseActivity$ServiceListener;->this$0:Lcom/android/videoeditor/VideoEditorBaseActivity;

    iget-object v0, v0, Lcom/android/videoeditor/VideoEditorBaseActivity;->mProjectPath:Ljava/lang/String;

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v0, p0, Lcom/android/videoeditor/VideoEditorBaseActivity$ServiceListener;->this$0:Lcom/android/videoeditor/VideoEditorBaseActivity;

    iget-object v0, v0, Lcom/android/videoeditor/VideoEditorBaseActivity;->mProject:Lcom/android/videoeditor/service/VideoEditorProject;

    if-eqz v0, :cond_0

    if-eqz p3, :cond_2

    iget-object v0, p0, Lcom/android/videoeditor/VideoEditorBaseActivity$ServiceListener;->this$0:Lcom/android/videoeditor/VideoEditorBaseActivity;

    const v1, 0x7f090049

    const/4 v2, 0x1

    invoke-static {v0, v1, v2}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    goto :goto_0

    :cond_2
    iget-object v0, p0, Lcom/android/videoeditor/VideoEditorBaseActivity$ServiceListener;->this$0:Lcom/android/videoeditor/VideoEditorBaseActivity;

    invoke-virtual {v0}, Lcom/android/videoeditor/VideoEditorBaseActivity;->getMediaLayout()Lcom/android/videoeditor/widgets/MediaLinearLayout;

    move-result-object v0

    invoke-virtual {v0, p2}, Lcom/android/videoeditor/widgets/MediaLinearLayout;->removeTransition(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/android/videoeditor/VideoEditorBaseActivity$ServiceListener;->this$0:Lcom/android/videoeditor/VideoEditorBaseActivity;

    invoke-virtual {v0}, Lcom/android/videoeditor/VideoEditorBaseActivity;->updateTimelineDuration()V

    goto :goto_0
.end method

.method public onTransitionThumbnails(Ljava/lang/String;Ljava/lang/String;[Landroid/graphics/Bitmap;Ljava/lang/Exception;)Z
    .locals 2
    .param p1    # Ljava/lang/String;
    .param p2    # Ljava/lang/String;
    .param p3    # [Landroid/graphics/Bitmap;
    .param p4    # Ljava/lang/Exception;

    const/4 v0, 0x0

    iget-object v1, p0, Lcom/android/videoeditor/VideoEditorBaseActivity$ServiceListener;->this$0:Lcom/android/videoeditor/VideoEditorBaseActivity;

    iget-object v1, v1, Lcom/android/videoeditor/VideoEditorBaseActivity;->mProjectPath:Ljava/lang/String;

    invoke-virtual {p1, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_1

    :cond_0
    :goto_0
    return v0

    :cond_1
    iget-object v1, p0, Lcom/android/videoeditor/VideoEditorBaseActivity$ServiceListener;->this$0:Lcom/android/videoeditor/VideoEditorBaseActivity;

    iget-object v1, v1, Lcom/android/videoeditor/VideoEditorBaseActivity;->mProject:Lcom/android/videoeditor/service/VideoEditorProject;

    if-eqz v1, :cond_0

    if-nez p4, :cond_0

    iget-object v0, p0, Lcom/android/videoeditor/VideoEditorBaseActivity$ServiceListener;->this$0:Lcom/android/videoeditor/VideoEditorBaseActivity;

    invoke-virtual {v0}, Lcom/android/videoeditor/VideoEditorBaseActivity;->getMediaLayout()Lcom/android/videoeditor/widgets/MediaLinearLayout;

    move-result-object v0

    invoke-virtual {v0, p2, p3}, Lcom/android/videoeditor/widgets/MediaLinearLayout;->setTransitionThumbnails(Ljava/lang/String;[Landroid/graphics/Bitmap;)Z

    move-result v0

    goto :goto_0
.end method

.method public onVideoEditorAspectRatioSet(Ljava/lang/String;ILjava/lang/Exception;)V
    .locals 3
    .param p1    # Ljava/lang/String;
    .param p2    # I
    .param p3    # Ljava/lang/Exception;

    iget-object v0, p0, Lcom/android/videoeditor/VideoEditorBaseActivity$ServiceListener;->this$0:Lcom/android/videoeditor/VideoEditorBaseActivity;

    iget-object v0, v0, Lcom/android/videoeditor/VideoEditorBaseActivity;->mProjectPath:Ljava/lang/String;

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v0, p0, Lcom/android/videoeditor/VideoEditorBaseActivity$ServiceListener;->this$0:Lcom/android/videoeditor/VideoEditorBaseActivity;

    iget-object v0, v0, Lcom/android/videoeditor/VideoEditorBaseActivity;->mProject:Lcom/android/videoeditor/service/VideoEditorProject;

    if-eqz v0, :cond_0

    if-eqz p3, :cond_2

    iget-object v0, p0, Lcom/android/videoeditor/VideoEditorBaseActivity$ServiceListener;->this$0:Lcom/android/videoeditor/VideoEditorBaseActivity;

    const v1, 0x7f09003b

    const/4 v2, 0x1

    invoke-static {v0, v1, v2}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    goto :goto_0

    :cond_2
    iget-object v0, p0, Lcom/android/videoeditor/VideoEditorBaseActivity$ServiceListener;->this$0:Lcom/android/videoeditor/VideoEditorBaseActivity;

    invoke-virtual {v0, p2}, Lcom/android/videoeditor/VideoEditorBaseActivity;->setAspectRatio(I)V

    goto :goto_0
.end method

.method public onVideoEditorCreated(Ljava/lang/String;Lcom/android/videoeditor/service/VideoEditorProject;Ljava/util/List;Ljava/util/List;Ljava/lang/Exception;)V
    .locals 3
    .param p1    # Ljava/lang/String;
    .param p2    # Lcom/android/videoeditor/service/VideoEditorProject;
    .param p5    # Ljava/lang/Exception;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Lcom/android/videoeditor/service/VideoEditorProject;",
            "Ljava/util/List",
            "<",
            "Landroid/media/videoeditor/MediaItem;",
            ">;",
            "Ljava/util/List",
            "<",
            "Landroid/media/videoeditor/AudioTrack;",
            ">;",
            "Ljava/lang/Exception;",
            ")V"
        }
    .end annotation

    const/4 v2, 0x1

    iget-object v0, p0, Lcom/android/videoeditor/VideoEditorBaseActivity$ServiceListener;->this$0:Lcom/android/videoeditor/VideoEditorBaseActivity;

    iget-object v0, v0, Lcom/android/videoeditor/VideoEditorBaseActivity;->mProjectPath:Ljava/lang/String;

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    :goto_0
    return-void

    :cond_0
    if-eqz p5, :cond_1

    iget-object v0, p0, Lcom/android/videoeditor/VideoEditorBaseActivity$ServiceListener;->this$0:Lcom/android/videoeditor/VideoEditorBaseActivity;

    const/4 v1, 0x0

    iput-object v1, v0, Lcom/android/videoeditor/VideoEditorBaseActivity;->mProjectPath:Ljava/lang/String;

    iget-object v0, p0, Lcom/android/videoeditor/VideoEditorBaseActivity$ServiceListener;->this$0:Lcom/android/videoeditor/VideoEditorBaseActivity;

    const v1, 0x7f090003

    invoke-virtual {v0, v1}, Lcom/android/videoeditor/VideoEditorBaseActivity;->enterDisabledState(I)V

    iget-object v0, p0, Lcom/android/videoeditor/VideoEditorBaseActivity$ServiceListener;->this$0:Lcom/android/videoeditor/VideoEditorBaseActivity;

    const v1, 0x7f090039

    invoke-static {v0, v1, v2}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    goto :goto_0

    :cond_1
    iget-object v0, p0, Lcom/android/videoeditor/VideoEditorBaseActivity$ServiceListener;->this$0:Lcom/android/videoeditor/VideoEditorBaseActivity;

    invoke-virtual {v0}, Lcom/android/videoeditor/VideoEditorBaseActivity;->enterReadyState()V

    iget-object v0, p0, Lcom/android/videoeditor/VideoEditorBaseActivity$ServiceListener;->this$0:Lcom/android/videoeditor/VideoEditorBaseActivity;

    iput-object p2, v0, Lcom/android/videoeditor/VideoEditorBaseActivity;->mProject:Lcom/android/videoeditor/service/VideoEditorProject;

    iget-object v0, p0, Lcom/android/videoeditor/VideoEditorBaseActivity$ServiceListener;->this$0:Lcom/android/videoeditor/VideoEditorBaseActivity;

    invoke-virtual {v0, v2}, Lcom/android/videoeditor/VideoEditorBaseActivity;->initializeFromProject(Z)V

    goto :goto_0
.end method

.method public onVideoEditorDeleted(Ljava/lang/String;Ljava/lang/Exception;)V
    .locals 3
    .param p1    # Ljava/lang/String;
    .param p2    # Ljava/lang/Exception;

    if-eqz p2, :cond_0

    iget-object v0, p0, Lcom/android/videoeditor/VideoEditorBaseActivity$ServiceListener;->this$0:Lcom/android/videoeditor/VideoEditorBaseActivity;

    const v1, 0x7f090040

    const/4 v2, 0x1

    invoke-static {v0, v1, v2}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    :cond_0
    return-void
.end method

.method public onVideoEditorExportComplete(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Exception;Z)V
    .locals 3
    .param p1    # Ljava/lang/String;
    .param p2    # Ljava/lang/String;
    .param p3    # Ljava/lang/Exception;
    .param p4    # Z

    iget-object v0, p0, Lcom/android/videoeditor/VideoEditorBaseActivity$ServiceListener;->this$0:Lcom/android/videoeditor/VideoEditorBaseActivity;

    iget-object v0, v0, Lcom/android/videoeditor/VideoEditorBaseActivity;->mProjectPath:Ljava/lang/String;

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v0, p0, Lcom/android/videoeditor/VideoEditorBaseActivity$ServiceListener;->this$0:Lcom/android/videoeditor/VideoEditorBaseActivity;

    iget-object v0, v0, Lcom/android/videoeditor/VideoEditorBaseActivity;->mProject:Lcom/android/videoeditor/service/VideoEditorProject;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/videoeditor/VideoEditorBaseActivity$ServiceListener;->this$0:Lcom/android/videoeditor/VideoEditorBaseActivity;

    iget-object v0, v0, Lcom/android/videoeditor/VideoEditorBaseActivity;->mPendingExportFilename:Ljava/lang/String;

    invoke-virtual {p2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/videoeditor/VideoEditorBaseActivity$ServiceListener;->this$0:Lcom/android/videoeditor/VideoEditorBaseActivity;

    invoke-virtual {v0}, Lcom/android/videoeditor/VideoEditorBaseActivity;->onExportComplete()V

    iget-object v0, p0, Lcom/android/videoeditor/VideoEditorBaseActivity$ServiceListener;->this$0:Lcom/android/videoeditor/VideoEditorBaseActivity;

    const/4 v1, 0x0

    iput-object v1, v0, Lcom/android/videoeditor/VideoEditorBaseActivity;->mPendingExportFilename:Ljava/lang/String;

    if-eqz p3, :cond_0

    iget-object v0, p0, Lcom/android/videoeditor/VideoEditorBaseActivity$ServiceListener;->this$0:Lcom/android/videoeditor/VideoEditorBaseActivity;

    const v1, 0x7f09003d

    const/4 v2, 0x1

    invoke-static {v0, v1, v2}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    goto :goto_0
.end method

.method public onVideoEditorExportProgress(Ljava/lang/String;Ljava/lang/String;I)V
    .locals 1
    .param p1    # Ljava/lang/String;
    .param p2    # Ljava/lang/String;
    .param p3    # I

    iget-object v0, p0, Lcom/android/videoeditor/VideoEditorBaseActivity$ServiceListener;->this$0:Lcom/android/videoeditor/VideoEditorBaseActivity;

    iget-object v0, v0, Lcom/android/videoeditor/VideoEditorBaseActivity;->mProjectPath:Ljava/lang/String;

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v0, p0, Lcom/android/videoeditor/VideoEditorBaseActivity$ServiceListener;->this$0:Lcom/android/videoeditor/VideoEditorBaseActivity;

    iget-object v0, v0, Lcom/android/videoeditor/VideoEditorBaseActivity;->mProject:Lcom/android/videoeditor/service/VideoEditorProject;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/videoeditor/VideoEditorBaseActivity$ServiceListener;->this$0:Lcom/android/videoeditor/VideoEditorBaseActivity;

    iget-object v0, v0, Lcom/android/videoeditor/VideoEditorBaseActivity;->mPendingExportFilename:Ljava/lang/String;

    invoke-virtual {p2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/videoeditor/VideoEditorBaseActivity$ServiceListener;->this$0:Lcom/android/videoeditor/VideoEditorBaseActivity;

    invoke-virtual {v0, p3}, Lcom/android/videoeditor/VideoEditorBaseActivity;->onExportProgress(I)V

    goto :goto_0
.end method

.method public onVideoEditorGeneratePreviewProgress(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;II)V
    .locals 3
    .param p1    # Ljava/lang/String;
    .param p2    # Ljava/lang/String;
    .param p3    # Ljava/lang/String;
    .param p4    # I
    .param p5    # I

    iget-object v0, p0, Lcom/android/videoeditor/VideoEditorBaseActivity$ServiceListener;->this$0:Lcom/android/videoeditor/VideoEditorBaseActivity;

    iget-object v0, v0, Lcom/android/videoeditor/VideoEditorBaseActivity;->mProjectPath:Ljava/lang/String;

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v0, p0, Lcom/android/videoeditor/VideoEditorBaseActivity$ServiceListener;->this$0:Lcom/android/videoeditor/VideoEditorBaseActivity;

    iget-object v0, v0, Lcom/android/videoeditor/VideoEditorBaseActivity;->mProject:Lcom/android/videoeditor/service/VideoEditorProject;

    if-eqz v0, :cond_0

    const-string v0, "VideoEditorBase"

    const/4 v1, 0x2

    invoke-static {v0, v1}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_2

    const-string v0, "VideoEditorBase"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "onVideoEditorGeneratePreviewProgress: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    :cond_2
    if-nez p2, :cond_3

    const/16 v0, 0x66

    if-ne p4, v0, :cond_0

    iget-object v0, p0, Lcom/android/videoeditor/VideoEditorBaseActivity$ServiceListener;->this$0:Lcom/android/videoeditor/VideoEditorBaseActivity;

    invoke-virtual {v0}, Lcom/android/videoeditor/VideoEditorBaseActivity;->showPreviewFrame()Z

    goto :goto_0

    :cond_3
    const-class v0, Landroid/media/videoeditor/MediaItem;

    invoke-virtual {v0}, Ljava/lang/Class;->getCanonicalName()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_4

    iget-object v0, p0, Lcom/android/videoeditor/VideoEditorBaseActivity$ServiceListener;->this$0:Lcom/android/videoeditor/VideoEditorBaseActivity;

    invoke-virtual {v0}, Lcom/android/videoeditor/VideoEditorBaseActivity;->getMediaLayout()Lcom/android/videoeditor/widgets/MediaLinearLayout;

    move-result-object v0

    invoke-virtual {v0, p3, p4, p5}, Lcom/android/videoeditor/widgets/MediaLinearLayout;->onGeneratePreviewMediaItemProgress(Ljava/lang/String;II)V

    goto :goto_0

    :cond_4
    const-class v0, Landroid/media/videoeditor/Transition;

    invoke-virtual {v0}, Ljava/lang/Class;->getCanonicalName()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_5

    iget-object v0, p0, Lcom/android/videoeditor/VideoEditorBaseActivity$ServiceListener;->this$0:Lcom/android/videoeditor/VideoEditorBaseActivity;

    invoke-virtual {v0}, Lcom/android/videoeditor/VideoEditorBaseActivity;->getMediaLayout()Lcom/android/videoeditor/widgets/MediaLinearLayout;

    move-result-object v0

    invoke-virtual {v0, p3, p4, p5}, Lcom/android/videoeditor/widgets/MediaLinearLayout;->onGeneratePreviewTransitionProgress(Ljava/lang/String;II)V

    goto :goto_0

    :cond_5
    const-class v0, Landroid/media/videoeditor/AudioTrack;

    invoke-virtual {v0}, Ljava/lang/Class;->getCanonicalName()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_6

    iget-object v0, p0, Lcom/android/videoeditor/VideoEditorBaseActivity$ServiceListener;->this$0:Lcom/android/videoeditor/VideoEditorBaseActivity;

    invoke-virtual {v0}, Lcom/android/videoeditor/VideoEditorBaseActivity;->getAudioTrackLayout()Lcom/android/videoeditor/widgets/AudioTrackLinearLayout;

    move-result-object v0

    invoke-virtual {v0, p3, p4, p5}, Lcom/android/videoeditor/widgets/AudioTrackLinearLayout;->onGeneratePreviewProgress(Ljava/lang/String;II)V

    goto :goto_0

    :cond_6
    const-string v0, "VideoEditorBase"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Unsupported storyboard item type: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0
.end method

.method public onVideoEditorLoaded(Ljava/lang/String;Lcom/android/videoeditor/service/VideoEditorProject;Ljava/util/List;Ljava/util/List;Ljava/lang/Exception;)V
    .locals 3
    .param p1    # Ljava/lang/String;
    .param p2    # Lcom/android/videoeditor/service/VideoEditorProject;
    .param p5    # Ljava/lang/Exception;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Lcom/android/videoeditor/service/VideoEditorProject;",
            "Ljava/util/List",
            "<",
            "Landroid/media/videoeditor/MediaItem;",
            ">;",
            "Ljava/util/List",
            "<",
            "Landroid/media/videoeditor/AudioTrack;",
            ">;",
            "Ljava/lang/Exception;",
            ")V"
        }
    .end annotation

    iget-object v1, p0, Lcom/android/videoeditor/VideoEditorBaseActivity$ServiceListener;->this$0:Lcom/android/videoeditor/VideoEditorBaseActivity;

    iget-object v1, v1, Lcom/android/videoeditor/VideoEditorBaseActivity;->mProjectPath:Ljava/lang/String;

    invoke-virtual {p1, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    if-nez p5, :cond_2

    if-nez p2, :cond_3

    :cond_2
    iget-object v1, p0, Lcom/android/videoeditor/VideoEditorBaseActivity$ServiceListener;->this$0:Lcom/android/videoeditor/VideoEditorBaseActivity;

    const/4 v2, 0x0

    iput-object v2, v1, Lcom/android/videoeditor/VideoEditorBaseActivity;->mProjectPath:Ljava/lang/String;

    iget-object v1, p0, Lcom/android/videoeditor/VideoEditorBaseActivity$ServiceListener;->this$0:Lcom/android/videoeditor/VideoEditorBaseActivity;

    const v2, 0x7f090003

    invoke-virtual {v1, v2}, Lcom/android/videoeditor/VideoEditorBaseActivity;->enterDisabledState(I)V

    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    const-string v1, "path"

    invoke-virtual {v0, v1, p1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v1, p0, Lcom/android/videoeditor/VideoEditorBaseActivity$ServiceListener;->this$0:Lcom/android/videoeditor/VideoEditorBaseActivity;

    const/16 v2, 0x64

    invoke-virtual {v1, v2, v0}, Lcom/android/videoeditor/VideoEditorBaseActivity;->showDialog(ILandroid/os/Bundle;)Z

    goto :goto_0

    :cond_3
    iget-object v1, p0, Lcom/android/videoeditor/VideoEditorBaseActivity$ServiceListener;->this$0:Lcom/android/videoeditor/VideoEditorBaseActivity;

    iget-object v1, v1, Lcom/android/videoeditor/VideoEditorBaseActivity;->mProject:Lcom/android/videoeditor/service/VideoEditorProject;

    if-nez v1, :cond_0

    iget-object v1, p0, Lcom/android/videoeditor/VideoEditorBaseActivity$ServiceListener;->this$0:Lcom/android/videoeditor/VideoEditorBaseActivity;

    iput-object p2, v1, Lcom/android/videoeditor/VideoEditorBaseActivity;->mProject:Lcom/android/videoeditor/service/VideoEditorProject;

    iget-object v1, p0, Lcom/android/videoeditor/VideoEditorBaseActivity$ServiceListener;->this$0:Lcom/android/videoeditor/VideoEditorBaseActivity;

    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Lcom/android/videoeditor/VideoEditorBaseActivity;->initializeFromProject(Z)V

    goto :goto_0
.end method

.method public onVideoEditorReleased(Ljava/lang/String;Ljava/lang/Exception;)V
    .locals 3
    .param p1    # Ljava/lang/String;
    .param p2    # Ljava/lang/Exception;

    if-eqz p2, :cond_0

    iget-object v0, p0, Lcom/android/videoeditor/VideoEditorBaseActivity$ServiceListener;->this$0:Lcom/android/videoeditor/VideoEditorBaseActivity;

    const v1, 0x7f09003f

    const/4 v2, 0x1

    invoke-static {v0, v1, v2}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    :cond_0
    return-void
.end method

.method public onVideoEditorSaved(Ljava/lang/String;Ljava/lang/Exception;)V
    .locals 3
    .param p1    # Ljava/lang/String;
    .param p2    # Ljava/lang/Exception;

    iget-object v0, p0, Lcom/android/videoeditor/VideoEditorBaseActivity$ServiceListener;->this$0:Lcom/android/videoeditor/VideoEditorBaseActivity;

    iget-object v0, v0, Lcom/android/videoeditor/VideoEditorBaseActivity;->mProjectPath:Ljava/lang/String;

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    if-eqz p2, :cond_0

    iget-object v0, p0, Lcom/android/videoeditor/VideoEditorBaseActivity$ServiceListener;->this$0:Lcom/android/videoeditor/VideoEditorBaseActivity;

    const v1, 0x7f09003e

    const/4 v2, 0x1

    invoke-static {v0, v1, v2}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    goto :goto_0
.end method

.method public onVideoEditorThemeApplied(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Exception;)V
    .locals 3
    .param p1    # Ljava/lang/String;
    .param p2    # Ljava/lang/String;
    .param p3    # Ljava/lang/Exception;

    iget-object v0, p0, Lcom/android/videoeditor/VideoEditorBaseActivity$ServiceListener;->this$0:Lcom/android/videoeditor/VideoEditorBaseActivity;

    iget-object v0, v0, Lcom/android/videoeditor/VideoEditorBaseActivity;->mProjectPath:Ljava/lang/String;

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v0, p0, Lcom/android/videoeditor/VideoEditorBaseActivity$ServiceListener;->this$0:Lcom/android/videoeditor/VideoEditorBaseActivity;

    iget-object v0, v0, Lcom/android/videoeditor/VideoEditorBaseActivity;->mProject:Lcom/android/videoeditor/service/VideoEditorProject;

    if-eqz v0, :cond_0

    if-eqz p3, :cond_2

    iget-object v0, p0, Lcom/android/videoeditor/VideoEditorBaseActivity$ServiceListener;->this$0:Lcom/android/videoeditor/VideoEditorBaseActivity;

    const v1, 0x7f09003c

    const/4 v2, 0x1

    invoke-static {v0, v1, v2}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    goto :goto_0

    :cond_2
    iget-object v0, p0, Lcom/android/videoeditor/VideoEditorBaseActivity$ServiceListener;->this$0:Lcom/android/videoeditor/VideoEditorBaseActivity;

    invoke-virtual {v0}, Lcom/android/videoeditor/VideoEditorBaseActivity;->getMediaLayout()Lcom/android/videoeditor/widgets/MediaLinearLayout;

    move-result-object v0

    iget-object v1, p0, Lcom/android/videoeditor/VideoEditorBaseActivity$ServiceListener;->this$0:Lcom/android/videoeditor/VideoEditorBaseActivity;

    iget-object v1, v1, Lcom/android/videoeditor/VideoEditorBaseActivity;->mProject:Lcom/android/videoeditor/service/VideoEditorProject;

    invoke-virtual {v1}, Lcom/android/videoeditor/service/VideoEditorProject;->getMediaItems()Ljava/util/List;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/android/videoeditor/widgets/MediaLinearLayout;->addMediaItems(Ljava/util/List;)V

    iget-object v0, p0, Lcom/android/videoeditor/VideoEditorBaseActivity$ServiceListener;->this$0:Lcom/android/videoeditor/VideoEditorBaseActivity;

    invoke-virtual {v0}, Lcom/android/videoeditor/VideoEditorBaseActivity;->getOverlayLayout()Lcom/android/videoeditor/widgets/OverlayLinearLayout;

    move-result-object v0

    iget-object v1, p0, Lcom/android/videoeditor/VideoEditorBaseActivity$ServiceListener;->this$0:Lcom/android/videoeditor/VideoEditorBaseActivity;

    iget-object v1, v1, Lcom/android/videoeditor/VideoEditorBaseActivity;->mProject:Lcom/android/videoeditor/service/VideoEditorProject;

    invoke-virtual {v1}, Lcom/android/videoeditor/service/VideoEditorProject;->getMediaItems()Ljava/util/List;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/android/videoeditor/widgets/OverlayLinearLayout;->addMediaItems(Ljava/util/List;)V

    iget-object v0, p0, Lcom/android/videoeditor/VideoEditorBaseActivity$ServiceListener;->this$0:Lcom/android/videoeditor/VideoEditorBaseActivity;

    invoke-virtual {v0}, Lcom/android/videoeditor/VideoEditorBaseActivity;->getAudioTrackLayout()Lcom/android/videoeditor/widgets/AudioTrackLinearLayout;

    move-result-object v0

    iget-object v1, p0, Lcom/android/videoeditor/VideoEditorBaseActivity$ServiceListener;->this$0:Lcom/android/videoeditor/VideoEditorBaseActivity;

    iget-object v1, v1, Lcom/android/videoeditor/VideoEditorBaseActivity;->mProject:Lcom/android/videoeditor/service/VideoEditorProject;

    invoke-virtual {v1}, Lcom/android/videoeditor/service/VideoEditorProject;->getAudioTracks()Ljava/util/List;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/android/videoeditor/widgets/AudioTrackLinearLayout;->addAudioTracks(Ljava/util/List;)V

    iget-object v0, p0, Lcom/android/videoeditor/VideoEditorBaseActivity$ServiceListener;->this$0:Lcom/android/videoeditor/VideoEditorBaseActivity;

    invoke-virtual {v0}, Lcom/android/videoeditor/VideoEditorBaseActivity;->updateTimelineDuration()V

    goto :goto_0
.end method
