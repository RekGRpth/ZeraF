.class public Lcom/android/videoeditor/EffectType;
.super Ljava/lang/Object;
.source "EffectType.java"


# static fields
.field public static final CATEGORY_IMAGE:I = 0x0

.field public static final CATEGORY_VIDEO:I = 0x1

.field public static final EFFECT_COLOR_GRADIENT:I = 0x1

.field public static final EFFECT_COLOR_NEGATIVE:I = 0x3

.field public static final EFFECT_COLOR_SEPIA:I = 0x2

.field public static final EFFECT_KEN_BURNS:I


# instance fields
.field private final mName:Ljava/lang/String;

.field private final mType:I


# direct methods
.method public constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .param p1    # Ljava/lang/String;
    .param p2    # I

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/android/videoeditor/EffectType;->mName:Ljava/lang/String;

    iput p2, p0, Lcom/android/videoeditor/EffectType;->mType:I

    return-void
.end method

.method public static getEffects(Landroid/content/Context;I)[Lcom/android/videoeditor/EffectType;
    .locals 8
    .param p0    # Landroid/content/Context;
    .param p1    # I

    const v7, 0x7f090079

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    packed-switch p1, :pswitch_data_0

    new-array v0, v3, [Lcom/android/videoeditor/EffectType;

    :goto_0
    return-object v0

    :pswitch_0
    const/4 v1, 0x4

    new-array v0, v1, [Lcom/android/videoeditor/EffectType;

    new-instance v1, Lcom/android/videoeditor/EffectType;

    const v2, 0x7f090078

    invoke-virtual {p0, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2, v3}, Lcom/android/videoeditor/EffectType;-><init>(Ljava/lang/String;I)V

    aput-object v1, v0, v3

    new-instance v1, Lcom/android/videoeditor/EffectType;

    invoke-virtual {p0, v7}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2, v4}, Lcom/android/videoeditor/EffectType;-><init>(Ljava/lang/String;I)V

    aput-object v1, v0, v4

    new-instance v1, Lcom/android/videoeditor/EffectType;

    const v2, 0x7f09007a

    invoke-virtual {p0, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2, v5}, Lcom/android/videoeditor/EffectType;-><init>(Ljava/lang/String;I)V

    aput-object v1, v0, v5

    new-instance v1, Lcom/android/videoeditor/EffectType;

    const v2, 0x7f09007b

    invoke-virtual {p0, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2, v6}, Lcom/android/videoeditor/EffectType;-><init>(Ljava/lang/String;I)V

    aput-object v1, v0, v6

    goto :goto_0

    :pswitch_1
    new-array v0, v6, [Lcom/android/videoeditor/EffectType;

    new-instance v1, Lcom/android/videoeditor/EffectType;

    invoke-virtual {p0, v7}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2, v4}, Lcom/android/videoeditor/EffectType;-><init>(Ljava/lang/String;I)V

    aput-object v1, v0, v3

    new-instance v1, Lcom/android/videoeditor/EffectType;

    const v2, 0x7f09007a

    invoke-virtual {p0, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2, v5}, Lcom/android/videoeditor/EffectType;-><init>(Ljava/lang/String;I)V

    aput-object v1, v0, v4

    new-instance v1, Lcom/android/videoeditor/EffectType;

    const v2, 0x7f09007b

    invoke-virtual {p0, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2, v6}, Lcom/android/videoeditor/EffectType;-><init>(Ljava/lang/String;I)V

    aput-object v1, v0, v5

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method


# virtual methods
.method public getName()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/android/videoeditor/EffectType;->mName:Ljava/lang/String;

    return-object v0
.end method

.method public getType()I
    .locals 1

    iget v0, p0, Lcom/android/videoeditor/EffectType;->mType:I

    return v0
.end method
