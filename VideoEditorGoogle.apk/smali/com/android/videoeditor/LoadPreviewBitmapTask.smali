.class Lcom/android/videoeditor/LoadPreviewBitmapTask;
.super Landroid/os/AsyncTask;
.source "ProjectPickerAdapter.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/os/AsyncTask",
        "<",
        "Ljava/lang/Void;",
        "Ljava/lang/Void;",
        "Landroid/graphics/Bitmap;",
        ">;"
    }
.end annotation


# instance fields
.field private mContextAdapter:Lcom/android/videoeditor/ProjectPickerAdapter;

.field private mDuration:Ljava/lang/String;

.field private mHeight:I

.field private mImageView:Landroid/widget/ImageView;

.field private mPreviewBitmapCache:Landroid/util/LruCache;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/util/LruCache",
            "<",
            "Ljava/lang/String;",
            "Landroid/graphics/Bitmap;",
            ">;"
        }
    .end annotation
.end field

.field private mProjectPath:Ljava/lang/String;

.field private mTitle:Ljava/lang/String;

.field private mWidth:I


# direct methods
.method public constructor <init>(Lcom/android/videoeditor/ProjectPickerAdapter;Ljava/lang/String;Landroid/widget/ImageView;IILjava/lang/String;Ljava/lang/String;Landroid/util/LruCache;)V
    .locals 0
    .param p1    # Lcom/android/videoeditor/ProjectPickerAdapter;
    .param p2    # Ljava/lang/String;
    .param p3    # Landroid/widget/ImageView;
    .param p4    # I
    .param p5    # I
    .param p6    # Ljava/lang/String;
    .param p7    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/android/videoeditor/ProjectPickerAdapter;",
            "Ljava/lang/String;",
            "Landroid/widget/ImageView;",
            "II",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Landroid/util/LruCache",
            "<",
            "Ljava/lang/String;",
            "Landroid/graphics/Bitmap;",
            ">;)V"
        }
    .end annotation

    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    iput-object p1, p0, Lcom/android/videoeditor/LoadPreviewBitmapTask;->mContextAdapter:Lcom/android/videoeditor/ProjectPickerAdapter;

    iput-object p2, p0, Lcom/android/videoeditor/LoadPreviewBitmapTask;->mProjectPath:Ljava/lang/String;

    iput-object p3, p0, Lcom/android/videoeditor/LoadPreviewBitmapTask;->mImageView:Landroid/widget/ImageView;

    iput p4, p0, Lcom/android/videoeditor/LoadPreviewBitmapTask;->mWidth:I

    iput p5, p0, Lcom/android/videoeditor/LoadPreviewBitmapTask;->mHeight:I

    iput-object p6, p0, Lcom/android/videoeditor/LoadPreviewBitmapTask;->mTitle:Ljava/lang/String;

    iput-object p7, p0, Lcom/android/videoeditor/LoadPreviewBitmapTask;->mDuration:Ljava/lang/String;

    iput-object p8, p0, Lcom/android/videoeditor/LoadPreviewBitmapTask;->mPreviewBitmapCache:Landroid/util/LruCache;

    return-void
.end method


# virtual methods
.method protected varargs doInBackground([Ljava/lang/Void;)Landroid/graphics/Bitmap;
    .locals 11
    .param p1    # [Ljava/lang/Void;

    const/4 v6, 0x0

    new-instance v5, Ljava/io/File;

    iget-object v7, p0, Lcom/android/videoeditor/LoadPreviewBitmapTask;->mProjectPath:Ljava/lang/String;

    const-string v8, "thumbnail.jpg"

    invoke-direct {v5, v7, v8}, Ljava/io/File;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v5}, Ljava/io/File;->exists()Z

    move-result v7

    if-nez v7, :cond_0

    move-object v0, v6

    :goto_0
    return-object v0

    :cond_0
    :try_start_0
    invoke-virtual {v5}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v7

    iget v8, p0, Lcom/android/videoeditor/LoadPreviewBitmapTask;->mWidth:I

    iget v9, p0, Lcom/android/videoeditor/LoadPreviewBitmapTask;->mHeight:I

    sget v10, Lcom/android/videoeditor/util/ImageUtils;->MATCH_LARGER_DIMENSION:I

    invoke-static {v7, v8, v9, v10}, Lcom/android/videoeditor/util/ImageUtils;->scaleImage(Ljava/lang/String;III)Landroid/graphics/Bitmap;

    move-result-object v4

    if-eqz v4, :cond_1

    iget v7, p0, Lcom/android/videoeditor/LoadPreviewBitmapTask;->mWidth:I

    iget v8, p0, Lcom/android/videoeditor/LoadPreviewBitmapTask;->mHeight:I

    sget-object v9, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    invoke-static {v7, v8, v9}, Landroid/graphics/Bitmap;->createBitmap(IILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;

    move-result-object v0

    new-instance v1, Landroid/graphics/Canvas;

    invoke-direct {v1, v0}, Landroid/graphics/Canvas;-><init>(Landroid/graphics/Bitmap;)V

    new-instance v3, Landroid/graphics/Paint;

    const/4 v7, 0x3

    invoke-direct {v3, v7}, Landroid/graphics/Paint;-><init>(I)V

    iget v7, p0, Lcom/android/videoeditor/LoadPreviewBitmapTask;->mWidth:I

    invoke-virtual {v4}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v8

    sub-int/2addr v7, v8

    div-int/lit8 v7, v7, 0x2

    int-to-float v7, v7

    iget v8, p0, Lcom/android/videoeditor/LoadPreviewBitmapTask;->mHeight:I

    invoke-virtual {v4}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v9

    sub-int/2addr v8, v9

    div-int/lit8 v8, v8, 0x2

    int-to-float v8, v8

    invoke-virtual {v1, v4, v7, v8, v3}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v2

    invoke-virtual {v2}, Ljava/io/IOException;->printStackTrace()V

    :cond_1
    move-object v0, v6

    goto :goto_0
.end method

.method protected bridge synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1
    .param p1    # [Ljava/lang/Object;

    check-cast p1, [Ljava/lang/Void;

    invoke-virtual {p0, p1}, Lcom/android/videoeditor/LoadPreviewBitmapTask;->doInBackground([Ljava/lang/Void;)Landroid/graphics/Bitmap;

    move-result-object v0

    return-object v0
.end method

.method protected onPostExecute(Landroid/graphics/Bitmap;)V
    .locals 3
    .param p1    # Landroid/graphics/Bitmap;

    if-nez p1, :cond_0

    iget v0, p0, Lcom/android/videoeditor/LoadPreviewBitmapTask;->mWidth:I

    iget v1, p0, Lcom/android/videoeditor/LoadPreviewBitmapTask;->mHeight:I

    sget-object v2, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    invoke-static {v0, v1, v2}, Landroid/graphics/Bitmap;->createBitmap(IILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;

    move-result-object p1

    const/high16 v0, -0x1000000

    invoke-virtual {p1, v0}, Landroid/graphics/Bitmap;->eraseColor(I)V

    :goto_0
    iget-object v0, p0, Lcom/android/videoeditor/LoadPreviewBitmapTask;->mContextAdapter:Lcom/android/videoeditor/ProjectPickerAdapter;

    iget-object v1, p0, Lcom/android/videoeditor/LoadPreviewBitmapTask;->mTitle:Ljava/lang/String;

    iget-object v2, p0, Lcom/android/videoeditor/LoadPreviewBitmapTask;->mDuration:Ljava/lang/String;

    invoke-virtual {v0, p1, v1, v2}, Lcom/android/videoeditor/ProjectPickerAdapter;->drawBottomOverlay(Landroid/graphics/Bitmap;Ljava/lang/String;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/android/videoeditor/LoadPreviewBitmapTask;->mImageView:Landroid/widget/ImageView;

    invoke-virtual {v0, p1}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    return-void

    :cond_0
    iget-object v0, p0, Lcom/android/videoeditor/LoadPreviewBitmapTask;->mPreviewBitmapCache:Landroid/util/LruCache;

    iget-object v1, p0, Lcom/android/videoeditor/LoadPreviewBitmapTask;->mProjectPath:Ljava/lang/String;

    invoke-virtual {v0, v1, p1}, Landroid/util/LruCache;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0
.end method

.method protected bridge synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 0
    .param p1    # Ljava/lang/Object;

    check-cast p1, Landroid/graphics/Bitmap;

    invoke-virtual {p0, p1}, Lcom/android/videoeditor/LoadPreviewBitmapTask;->onPostExecute(Landroid/graphics/Bitmap;)V

    return-void
.end method
