.class public Lcom/android/videoeditor/OverlayType;
.super Ljava/lang/Object;
.source "OverlayType.java"


# instance fields
.field private final mName:Ljava/lang/String;

.field private final mType:I


# direct methods
.method public constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .param p1    # Ljava/lang/String;
    .param p2    # I

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/android/videoeditor/OverlayType;->mName:Ljava/lang/String;

    iput p2, p0, Lcom/android/videoeditor/OverlayType;->mType:I

    return-void
.end method

.method public static getOverlays(Landroid/content/Context;)[Lcom/android/videoeditor/OverlayType;
    .locals 8
    .param p0    # Landroid/content/Context;

    const v7, 0x7f09006e

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    const/4 v1, 0x4

    new-array v0, v1, [Lcom/android/videoeditor/OverlayType;

    new-instance v1, Lcom/android/videoeditor/OverlayType;

    invoke-virtual {p0, v7}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2, v3}, Lcom/android/videoeditor/OverlayType;-><init>(Ljava/lang/String;I)V

    aput-object v1, v0, v3

    new-instance v1, Lcom/android/videoeditor/OverlayType;

    const v2, 0x7f09006f

    invoke-virtual {p0, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2, v4}, Lcom/android/videoeditor/OverlayType;-><init>(Ljava/lang/String;I)V

    aput-object v1, v0, v4

    new-instance v1, Lcom/android/videoeditor/OverlayType;

    invoke-virtual {p0, v7}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2, v5}, Lcom/android/videoeditor/OverlayType;-><init>(Ljava/lang/String;I)V

    aput-object v1, v0, v5

    new-instance v1, Lcom/android/videoeditor/OverlayType;

    const v2, 0x7f09006f

    invoke-virtual {p0, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2, v6}, Lcom/android/videoeditor/OverlayType;-><init>(Ljava/lang/String;I)V

    aput-object v1, v0, v6

    return-object v0
.end method


# virtual methods
.method public getName()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/android/videoeditor/OverlayType;->mName:Ljava/lang/String;

    return-object v0
.end method

.method public getType()I
    .locals 1

    iget v0, p0, Lcom/android/videoeditor/OverlayType;->mType:I

    return v0
.end method
