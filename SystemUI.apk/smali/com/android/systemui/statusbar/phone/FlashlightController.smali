.class public Lcom/android/systemui/statusbar/phone/FlashlightController;
.super Ljava/lang/Object;
.source "FlashlightController.java"


# static fields
.field public static camera:Landroid/hardware/Camera;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const/4 v0, 0x0

    sput-object v0, Lcom/android/systemui/statusbar/phone/FlashlightController;->camera:Landroid/hardware/Camera;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static toggleFlashlight()Z
    .locals 4

    const/4 v1, 0x0

    :try_start_0
    sget-object v3, Lcom/android/systemui/statusbar/phone/FlashlightController;->camera:Landroid/hardware/Camera;

    if-eqz v3, :cond_0

    sget-object v3, Lcom/android/systemui/statusbar/phone/FlashlightController;->camera:Landroid/hardware/Camera;

    invoke-virtual {v3}, Landroid/hardware/Camera;->getParameters()Landroid/hardware/Camera$Parameters;

    move-result-object v2

    const-string v3, "off"

    invoke-virtual {v2, v3}, Landroid/hardware/Camera$Parameters;->setFlashMode(Ljava/lang/String;)V

    sget-object v3, Lcom/android/systemui/statusbar/phone/FlashlightController;->camera:Landroid/hardware/Camera;

    invoke-virtual {v3, v2}, Landroid/hardware/Camera;->setParameters(Landroid/hardware/Camera$Parameters;)V

    sget-object v3, Lcom/android/systemui/statusbar/phone/FlashlightController;->camera:Landroid/hardware/Camera;

    invoke-virtual {v3}, Landroid/hardware/Camera;->release()V

    const/4 v3, 0x0

    sput-object v3, Lcom/android/systemui/statusbar/phone/FlashlightController;->camera:Landroid/hardware/Camera;

    const/4 v1, 0x0

    :goto_0
    return v1

    :cond_0
    invoke-static {}, Landroid/hardware/Camera;->open()Landroid/hardware/Camera;

    move-result-object v3

    sput-object v3, Lcom/android/systemui/statusbar/phone/FlashlightController;->camera:Landroid/hardware/Camera;

    sget-object v3, Lcom/android/systemui/statusbar/phone/FlashlightController;->camera:Landroid/hardware/Camera;

    invoke-virtual {v3}, Landroid/hardware/Camera;->getParameters()Landroid/hardware/Camera$Parameters;

    move-result-object v2

    const-string v3, "torch"

    invoke-virtual {v2, v3}, Landroid/hardware/Camera$Parameters;->setFlashMode(Ljava/lang/String;)V

    sget-object v3, Lcom/android/systemui/statusbar/phone/FlashlightController;->camera:Landroid/hardware/Camera;

    invoke-virtual {v3, v2}, Landroid/hardware/Camera;->setParameters(Landroid/hardware/Camera$Parameters;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    const/4 v1, 0x1

    goto :goto_0

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_0
.end method
