.class public Lcom/mediatek/systemui/ext/IconIdWrapper;
.super Ljava/lang/Object;
.source "IconIdWrapper.java"

# interfaces
.implements Ljava/lang/Cloneable;


# instance fields
.field private mIconId:I

.field private mResources:Landroid/content/res/Resources;


# direct methods
.method public constructor <init>()V
    .locals 2

    const/4 v0, 0x0

    const/4 v1, 0x0

    invoke-direct {p0, v0, v1}, Lcom/mediatek/systemui/ext/IconIdWrapper;-><init>(Landroid/content/res/Resources;I)V

    return-void
.end method

.method public constructor <init>(I)V
    .locals 1
    .param p1    # I

    const/4 v0, 0x0

    invoke-direct {p0, v0, p1}, Lcom/mediatek/systemui/ext/IconIdWrapper;-><init>(Landroid/content/res/Resources;I)V

    return-void
.end method

.method public constructor <init>(Landroid/content/res/Resources;I)V
    .locals 1
    .param p1    # Landroid/content/res/Resources;
    .param p2    # I

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/mediatek/systemui/ext/IconIdWrapper;->mResources:Landroid/content/res/Resources;

    const/4 v0, 0x0

    iput v0, p0, Lcom/mediatek/systemui/ext/IconIdWrapper;->mIconId:I

    iput-object p1, p0, Lcom/mediatek/systemui/ext/IconIdWrapper;->mResources:Landroid/content/res/Resources;

    iput p2, p0, Lcom/mediatek/systemui/ext/IconIdWrapper;->mIconId:I

    return-void
.end method


# virtual methods
.method public clone()Lcom/mediatek/systemui/ext/IconIdWrapper;
    .locals 4

    const/4 v1, 0x0

    :try_start_0
    invoke-super {p0}, Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v3

    move-object v0, v3

    check-cast v0, Lcom/mediatek/systemui/ext/IconIdWrapper;

    move-object v1, v0

    iget-object v3, p0, Lcom/mediatek/systemui/ext/IconIdWrapper;->mResources:Landroid/content/res/Resources;

    iput-object v3, v1, Lcom/mediatek/systemui/ext/IconIdWrapper;->mResources:Landroid/content/res/Resources;

    iget v3, p0, Lcom/mediatek/systemui/ext/IconIdWrapper;->mIconId:I

    iput v3, v1, Lcom/mediatek/systemui/ext/IconIdWrapper;->mIconId:I
    :try_end_0
    .catch Ljava/lang/CloneNotSupportedException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    return-object v1

    :catch_0
    move-exception v2

    const/4 v1, 0x0

    goto :goto_0
.end method

.method public bridge synthetic clone()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/CloneNotSupportedException;
        }
    .end annotation

    invoke-virtual {p0}, Lcom/mediatek/systemui/ext/IconIdWrapper;->clone()Lcom/mediatek/systemui/ext/IconIdWrapper;

    move-result-object v0

    return-object v0
.end method

.method public getDrawable()Landroid/graphics/drawable/Drawable;
    .locals 2

    iget-object v0, p0, Lcom/mediatek/systemui/ext/IconIdWrapper;->mResources:Landroid/content/res/Resources;

    if-eqz v0, :cond_0

    iget v0, p0, Lcom/mediatek/systemui/ext/IconIdWrapper;->mIconId:I

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/mediatek/systemui/ext/IconIdWrapper;->mResources:Landroid/content/res/Resources;

    iget v1, p0, Lcom/mediatek/systemui/ext/IconIdWrapper;->mIconId:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getIconId()I
    .locals 1

    iget v0, p0, Lcom/mediatek/systemui/ext/IconIdWrapper;->mIconId:I

    return v0
.end method

.method public getResources()Landroid/content/res/Resources;
    .locals 1

    iget-object v0, p0, Lcom/mediatek/systemui/ext/IconIdWrapper;->mResources:Landroid/content/res/Resources;

    return-object v0
.end method

.method public setIconId(I)V
    .locals 0
    .param p1    # I

    iput p1, p0, Lcom/mediatek/systemui/ext/IconIdWrapper;->mIconId:I

    return-void
.end method

.method public setResources(Landroid/content/res/Resources;)V
    .locals 0
    .param p1    # Landroid/content/res/Resources;

    iput-object p1, p0, Lcom/mediatek/systemui/ext/IconIdWrapper;->mResources:Landroid/content/res/Resources;

    return-void
.end method
