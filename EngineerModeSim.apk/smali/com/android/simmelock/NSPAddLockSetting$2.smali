.class Lcom/android/simmelock/NSPAddLockSetting$2;
.super Ljava/lang/Object;
.source "NSPAddLockSetting.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/android/simmelock/NSPAddLockSetting;->onCreate(Landroid/os/Bundle;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/simmelock/NSPAddLockSetting;


# direct methods
.method constructor <init>(Lcom/android/simmelock/NSPAddLockSetting;)V
    .locals 0

    iput-object p1, p0, Lcom/android/simmelock/NSPAddLockSetting$2;->this$0:Lcom/android/simmelock/NSPAddLockSetting;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 10
    .param p1    # Landroid/view/View;

    const/4 v9, 0x4

    const/4 v6, 0x0

    const/4 v2, 0x2

    const/4 v5, 0x0

    const/4 v1, 0x1

    const-string v0, "SIMMELOCK"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "clickFlag: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lcom/android/simmelock/NSPAddLockSetting$2;->this$0:Lcom/android/simmelock/NSPAddLockSetting;

    invoke-static {v4}, Lcom/android/simmelock/NSPAddLockSetting;->access$000(Lcom/android/simmelock/NSPAddLockSetting;)Z

    move-result v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v0, v3}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v0, p0, Lcom/android/simmelock/NSPAddLockSetting$2;->this$0:Lcom/android/simmelock/NSPAddLockSetting;

    invoke-static {v0}, Lcom/android/simmelock/NSPAddLockSetting;->access$000(Lcom/android/simmelock/NSPAddLockSetting;)Z

    move-result v0

    if-eqz v0, :cond_0

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/android/simmelock/NSPAddLockSetting$2;->this$0:Lcom/android/simmelock/NSPAddLockSetting;

    invoke-static {v0, v1}, Lcom/android/simmelock/NSPAddLockSetting;->access$002(Lcom/android/simmelock/NSPAddLockSetting;Z)Z

    iget-object v0, p0, Lcom/android/simmelock/NSPAddLockSetting$2;->this$0:Lcom/android/simmelock/NSPAddLockSetting;

    iget-boolean v0, v0, Lcom/android/simmelock/NSPAddLockSetting;->mbMCCMNCHLRReadSIM:Z

    if-nez v0, :cond_2

    iget-object v0, p0, Lcom/android/simmelock/NSPAddLockSetting$2;->this$0:Lcom/android/simmelock/NSPAddLockSetting;

    iget-boolean v0, v0, Lcom/android/simmelock/NSPAddLockSetting;->mbMCCMNCHLRReadSIM1:Z

    if-nez v0, :cond_2

    iget-object v0, p0, Lcom/android/simmelock/NSPAddLockSetting$2;->this$0:Lcom/android/simmelock/NSPAddLockSetting;

    iget-boolean v0, v0, Lcom/android/simmelock/NSPAddLockSetting;->mbMCCMNCHLRReadSIM2:Z

    if-nez v0, :cond_2

    const/4 v0, 0x7

    iget-object v3, p0, Lcom/android/simmelock/NSPAddLockSetting$2;->this$0:Lcom/android/simmelock/NSPAddLockSetting;

    iget-object v3, v3, Lcom/android/simmelock/NSPAddLockSetting;->etMCCMNCHLR:Landroid/widget/EditText;

    invoke-virtual {v3}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v3

    invoke-interface {v3}, Landroid/text/Editable;->length()I

    move-result v3

    if-gt v0, v3, :cond_1

    const/16 v0, 0x8

    iget-object v3, p0, Lcom/android/simmelock/NSPAddLockSetting$2;->this$0:Lcom/android/simmelock/NSPAddLockSetting;

    iget-object v3, v3, Lcom/android/simmelock/NSPAddLockSetting;->etMCCMNCHLR:Landroid/widget/EditText;

    invoke-virtual {v3}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v3

    invoke-interface {v3}, Landroid/text/Editable;->length()I

    move-result v3

    if-ge v0, v3, :cond_2

    :cond_1
    iget-object v0, p0, Lcom/android/simmelock/NSPAddLockSetting$2;->this$0:Lcom/android/simmelock/NSPAddLockSetting;

    invoke-virtual {v0, v1}, Landroid/app/Activity;->showDialog(I)V

    goto :goto_0

    :cond_2
    iget-object v0, p0, Lcom/android/simmelock/NSPAddLockSetting$2;->this$0:Lcom/android/simmelock/NSPAddLockSetting;

    iget-object v0, v0, Lcom/android/simmelock/NSPAddLockSetting;->etPwd:Landroid/widget/EditText;

    invoke-virtual {v0}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-interface {v0}, Landroid/text/Editable;->length()I

    move-result v0

    if-lt v0, v9, :cond_3

    iget-object v0, p0, Lcom/android/simmelock/NSPAddLockSetting$2;->this$0:Lcom/android/simmelock/NSPAddLockSetting;

    iget-object v0, v0, Lcom/android/simmelock/NSPAddLockSetting;->etPwd:Landroid/widget/EditText;

    invoke-virtual {v0}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-interface {v0}, Landroid/text/Editable;->length()I

    move-result v0

    const/16 v3, 0x8

    if-le v0, v3, :cond_4

    :cond_3
    iget-object v0, p0, Lcom/android/simmelock/NSPAddLockSetting$2;->this$0:Lcom/android/simmelock/NSPAddLockSetting;

    invoke-virtual {v0, v9}, Landroid/app/Activity;->showDialog(I)V

    goto :goto_0

    :cond_4
    iget-object v0, p0, Lcom/android/simmelock/NSPAddLockSetting$2;->this$0:Lcom/android/simmelock/NSPAddLockSetting;

    iget-object v0, v0, Lcom/android/simmelock/NSPAddLockSetting;->etPwd:Landroid/widget/EditText;

    invoke-virtual {v0}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    iget-object v3, p0, Lcom/android/simmelock/NSPAddLockSetting$2;->this$0:Lcom/android/simmelock/NSPAddLockSetting;

    iget-object v3, v3, Lcom/android/simmelock/NSPAddLockSetting;->etPwdConfirm:Landroid/widget/EditText;

    invoke-virtual {v3}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_5

    iget-object v0, p0, Lcom/android/simmelock/NSPAddLockSetting$2;->this$0:Lcom/android/simmelock/NSPAddLockSetting;

    const/4 v1, 0x5

    invoke-virtual {v0, v1}, Landroid/app/Activity;->showDialog(I)V

    goto/16 :goto_0

    :cond_5
    iget-object v0, p0, Lcom/android/simmelock/NSPAddLockSetting$2;->this$0:Lcom/android/simmelock/NSPAddLockSetting;

    invoke-static {v0}, Lcom/android/simmelock/NSPAddLockSetting;->access$100(Lcom/android/simmelock/NSPAddLockSetting;)Landroid/os/Handler;

    move-result-object v0

    const/16 v3, 0x6f

    invoke-static {v0, v3}, Landroid/os/Message;->obtain(Landroid/os/Handler;I)Landroid/os/Message;

    move-result-object v7

    invoke-static {}, Lcom/android/internal/telephony/PhoneFactory;->getDefaultPhone()Lcom/android/internal/telephony/Phone;

    move-result-object v8

    check-cast v8, Lcom/android/internal/telephony/gemini/GeminiPhone;

    iget-object v0, p0, Lcom/android/simmelock/NSPAddLockSetting$2;->this$0:Lcom/android/simmelock/NSPAddLockSetting;

    iget-object v3, p0, Lcom/android/simmelock/NSPAddLockSetting$2;->this$0:Lcom/android/simmelock/NSPAddLockSetting;

    iget-object v3, v3, Lcom/android/simmelock/NSPAddLockSetting;->bundle:Landroid/os/Bundle;

    const-string v4, "SIMNo"

    invoke-virtual {v3, v4}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v3

    iput v3, v0, Lcom/android/simmelock/NSPAddLockSetting;->intSIMNumber:I

    iget-object v0, p0, Lcom/android/simmelock/NSPAddLockSetting$2;->this$0:Lcom/android/simmelock/NSPAddLockSetting;

    iget v0, v0, Lcom/android/simmelock/NSPAddLockSetting;->intSIMNumber:I

    if-nez v0, :cond_7

    iget-object v0, p0, Lcom/android/simmelock/NSPAddLockSetting$2;->this$0:Lcom/android/simmelock/NSPAddLockSetting;

    iget-boolean v0, v0, Lcom/android/simmelock/NSPAddLockSetting;->mbMCCMNCHLRReadSIM1:Z

    if-nez v0, :cond_6

    invoke-virtual {v8, v6}, Lcom/android/internal/telephony/gemini/GeminiPhone;->getIccCardGemini(I)Lcom/android/internal/telephony/IccCard;

    move-result-object v0

    iget-object v3, p0, Lcom/android/simmelock/NSPAddLockSetting$2;->this$0:Lcom/android/simmelock/NSPAddLockSetting;

    iget-object v3, v3, Lcom/android/simmelock/NSPAddLockSetting;->etPwd:Landroid/widget/EditText;

    invoke-virtual {v3}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v3

    iget-object v4, p0, Lcom/android/simmelock/NSPAddLockSetting$2;->this$0:Lcom/android/simmelock/NSPAddLockSetting;

    iget-object v4, v4, Lcom/android/simmelock/NSPAddLockSetting;->etMCCMNCHLR:Landroid/widget/EditText;

    invoke-virtual {v4}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v4

    move-object v6, v5

    invoke-interface/range {v0 .. v7}, Lcom/android/internal/telephony/IccCard;->setIccNetworkLockEnabled(IILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Landroid/os/Message;)V

    goto/16 :goto_0

    :cond_6
    invoke-virtual {v8, v6}, Lcom/android/internal/telephony/gemini/GeminiPhone;->getIccCardGemini(I)Lcom/android/internal/telephony/IccCard;

    move-result-object v0

    iget-object v3, p0, Lcom/android/simmelock/NSPAddLockSetting$2;->this$0:Lcom/android/simmelock/NSPAddLockSetting;

    iget-object v3, v3, Lcom/android/simmelock/NSPAddLockSetting;->etPwd:Landroid/widget/EditText;

    invoke-virtual {v3}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v3

    iget-object v4, p0, Lcom/android/simmelock/NSPAddLockSetting$2;->this$0:Lcom/android/simmelock/NSPAddLockSetting;

    iget-object v4, v4, Lcom/android/simmelock/NSPAddLockSetting;->msSIM1MCCMNCHLR:Ljava/lang/String;

    move-object v6, v5

    invoke-interface/range {v0 .. v7}, Lcom/android/internal/telephony/IccCard;->setIccNetworkLockEnabled(IILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Landroid/os/Message;)V

    goto/16 :goto_0

    :cond_7
    iget-object v0, p0, Lcom/android/simmelock/NSPAddLockSetting$2;->this$0:Lcom/android/simmelock/NSPAddLockSetting;

    iget-boolean v0, v0, Lcom/android/simmelock/NSPAddLockSetting;->mbMCCMNCHLRReadSIM2:Z

    if-nez v0, :cond_8

    invoke-virtual {v8, v1}, Lcom/android/internal/telephony/gemini/GeminiPhone;->getIccCardGemini(I)Lcom/android/internal/telephony/IccCard;

    move-result-object v0

    iget-object v3, p0, Lcom/android/simmelock/NSPAddLockSetting$2;->this$0:Lcom/android/simmelock/NSPAddLockSetting;

    iget-object v3, v3, Lcom/android/simmelock/NSPAddLockSetting;->etPwd:Landroid/widget/EditText;

    invoke-virtual {v3}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v3

    iget-object v4, p0, Lcom/android/simmelock/NSPAddLockSetting$2;->this$0:Lcom/android/simmelock/NSPAddLockSetting;

    iget-object v4, v4, Lcom/android/simmelock/NSPAddLockSetting;->etMCCMNCHLR:Landroid/widget/EditText;

    invoke-virtual {v4}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v4

    move-object v6, v5

    invoke-interface/range {v0 .. v7}, Lcom/android/internal/telephony/IccCard;->setIccNetworkLockEnabled(IILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Landroid/os/Message;)V

    goto/16 :goto_0

    :cond_8
    invoke-virtual {v8, v1}, Lcom/android/internal/telephony/gemini/GeminiPhone;->getIccCardGemini(I)Lcom/android/internal/telephony/IccCard;

    move-result-object v0

    iget-object v3, p0, Lcom/android/simmelock/NSPAddLockSetting$2;->this$0:Lcom/android/simmelock/NSPAddLockSetting;

    iget-object v3, v3, Lcom/android/simmelock/NSPAddLockSetting;->etPwd:Landroid/widget/EditText;

    invoke-virtual {v3}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v3

    iget-object v4, p0, Lcom/android/simmelock/NSPAddLockSetting$2;->this$0:Lcom/android/simmelock/NSPAddLockSetting;

    iget-object v4, v4, Lcom/android/simmelock/NSPAddLockSetting;->msSIM2MCCMNCHLR:Ljava/lang/String;

    move-object v6, v5

    invoke-interface/range {v0 .. v7}, Lcom/android/internal/telephony/IccCard;->setIccNetworkLockEnabled(IILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Landroid/os/Message;)V

    goto/16 :goto_0
.end method
