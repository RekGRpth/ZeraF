.class Lcom/android/simmelock/SPAddLockSetting$11;
.super Landroid/os/Handler;
.source "SPAddLockSetting.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/simmelock/SPAddLockSetting;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/simmelock/SPAddLockSetting;


# direct methods
.method constructor <init>(Lcom/android/simmelock/SPAddLockSetting;)V
    .locals 0

    iput-object p1, p0, Lcom/android/simmelock/SPAddLockSetting$11;->this$0:Lcom/android/simmelock/SPAddLockSetting;

    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 12
    .param p1    # Landroid/os/Message;

    const/4 v11, 0x1

    const/4 v10, 0x2

    const/4 v9, 0x0

    const/16 v8, 0xff

    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v0, Landroid/os/AsyncResult;

    iget v5, p1, Landroid/os/Message;->what:I

    sparse-switch v5, :sswitch_data_0

    :goto_0
    return-void

    :sswitch_0
    iget-object v5, v0, Landroid/os/AsyncResult;->exception:Ljava/lang/Throwable;

    if-eqz v5, :cond_0

    iget-object v5, p0, Lcom/android/simmelock/SPAddLockSetting$11;->this$0:Lcom/android/simmelock/SPAddLockSetting;

    invoke-virtual {v5, v10}, Landroid/app/Activity;->showDialog(I)V

    goto :goto_0

    :cond_0
    iget-object v5, p0, Lcom/android/simmelock/SPAddLockSetting$11;->this$0:Lcom/android/simmelock/SPAddLockSetting;

    const/4 v6, 0x3

    invoke-virtual {v5, v6}, Landroid/app/Activity;->showDialog(I)V

    goto :goto_0

    :sswitch_1
    iget-object v5, v0, Landroid/os/AsyncResult;->exception:Ljava/lang/Throwable;

    if-eqz v5, :cond_1

    const-string v5, "SIMME Lock"

    const-string v6, "fail to get SIM GID1"

    invoke-static {v5, v6}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v5, p0, Lcom/android/simmelock/SPAddLockSetting$11;->this$0:Lcom/android/simmelock/SPAddLockSetting;

    iput v8, v5, Lcom/android/simmelock/SPAddLockSetting;->intSIMGID1:I

    goto :goto_0

    :cond_1
    iget-object v5, v0, Landroid/os/AsyncResult;->result:Ljava/lang/Object;

    check-cast v5, [B

    move-object v1, v5

    check-cast v1, [B

    invoke-static {v1}, Lcom/android/internal/telephony/IccUtils;->bytesToHexString([B)Ljava/lang/String;

    move-result-object v4

    aget-byte v5, v1, v9

    and-int/lit16 v5, v5, 0xff

    if-ne v5, v8, :cond_2

    const-string v5, "SIMME Lock"

    const-string v6, "SIM GID1 not initialized"

    invoke-static {v5, v6}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v5, p0, Lcom/android/simmelock/SPAddLockSetting$11;->this$0:Lcom/android/simmelock/SPAddLockSetting;

    iput v8, v5, Lcom/android/simmelock/SPAddLockSetting;->intSIMGID1:I

    goto :goto_0

    :cond_2
    iget-object v5, p0, Lcom/android/simmelock/SPAddLockSetting$11;->this$0:Lcom/android/simmelock/SPAddLockSetting;

    iput v11, v5, Lcom/android/simmelock/SPAddLockSetting;->intSIMGID1:I

    iget-object v5, p0, Lcom/android/simmelock/SPAddLockSetting$11;->this$0:Lcom/android/simmelock/SPAddLockSetting;

    invoke-static {v1}, Lcom/android/internal/telephony/IccUtils;->bytesToHexString([B)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v6, v9, v10}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v6

    invoke-static {v6}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v6

    invoke-static {v6}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v6

    iput-object v6, v5, Lcom/android/simmelock/SPAddLockSetting;->msSIMGID1:Ljava/lang/String;

    goto :goto_0

    :sswitch_2
    iget-object v5, v0, Landroid/os/AsyncResult;->exception:Ljava/lang/Throwable;

    if-eqz v5, :cond_3

    const-string v5, "SIMME Lock"

    const-string v6, "fail to get SIM1 GID1"

    invoke-static {v5, v6}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v5, p0, Lcom/android/simmelock/SPAddLockSetting$11;->this$0:Lcom/android/simmelock/SPAddLockSetting;

    iput v8, v5, Lcom/android/simmelock/SPAddLockSetting;->intSIMGID1:I

    goto :goto_0

    :cond_3
    const-string v5, "SIMME Lock"

    const-string v6, "succeed to get SIM1 GID1"

    invoke-static {v5, v6}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v5, v0, Landroid/os/AsyncResult;->result:Ljava/lang/Object;

    check-cast v5, [B

    move-object v1, v5

    check-cast v1, [B

    const-string v5, "SIMME Lock"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "SIM1 GID1 :"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    invoke-static {v1}, Lcom/android/internal/telephony/IccUtils;->bytesToHexString([B)Ljava/lang/String;

    move-result-object v2

    aget-byte v5, v1, v9

    and-int/lit16 v5, v5, 0xff

    if-ne v5, v8, :cond_4

    const-string v5, "SIMME Lock"

    const-string v6, "SIM1 GID1 not initialized"

    invoke-static {v5, v6}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v5, p0, Lcom/android/simmelock/SPAddLockSetting$11;->this$0:Lcom/android/simmelock/SPAddLockSetting;

    iput v8, v5, Lcom/android/simmelock/SPAddLockSetting;->intSIMGID1:I

    goto/16 :goto_0

    :cond_4
    iget-object v5, p0, Lcom/android/simmelock/SPAddLockSetting$11;->this$0:Lcom/android/simmelock/SPAddLockSetting;

    iput v11, v5, Lcom/android/simmelock/SPAddLockSetting;->intSIMGID1:I

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v5

    if-lt v5, v10, :cond_5

    iget-object v5, p0, Lcom/android/simmelock/SPAddLockSetting$11;->this$0:Lcom/android/simmelock/SPAddLockSetting;

    invoke-static {v1}, Lcom/android/internal/telephony/IccUtils;->bytesToHexString([B)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v6, v9, v10}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v6

    invoke-static {v6}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v6

    invoke-static {v6}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v6

    iput-object v6, v5, Lcom/android/simmelock/SPAddLockSetting;->msSIM1GID1:Ljava/lang/String;

    :goto_1
    const-string v5, "SIMME Lock"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "Normal SIM1 GID1 :"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    iget-object v7, p0, Lcom/android/simmelock/SPAddLockSetting$11;->this$0:Lcom/android/simmelock/SPAddLockSetting;

    iget-object v7, v7, Lcom/android/simmelock/SPAddLockSetting;->msSIM1GID1:Ljava/lang/String;

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0

    :cond_5
    iget-object v5, p0, Lcom/android/simmelock/SPAddLockSetting$11;->this$0:Lcom/android/simmelock/SPAddLockSetting;

    invoke-static {v1}, Lcom/android/internal/telephony/IccUtils;->bytesToHexString([B)Ljava/lang/String;

    move-result-object v6

    invoke-static {v6}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v6

    invoke-static {v6}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v6

    iput-object v6, v5, Lcom/android/simmelock/SPAddLockSetting;->msSIM1GID1:Ljava/lang/String;

    goto :goto_1

    :sswitch_3
    iget-object v5, v0, Landroid/os/AsyncResult;->exception:Ljava/lang/Throwable;

    if-eqz v5, :cond_6

    const-string v5, "SIMME Lock"

    const-string v6, "fail to get SIM2 GID1"

    invoke-static {v5, v6}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v5, p0, Lcom/android/simmelock/SPAddLockSetting$11;->this$0:Lcom/android/simmelock/SPAddLockSetting;

    iput v8, v5, Lcom/android/simmelock/SPAddLockSetting;->intSIMGID1:I

    goto/16 :goto_0

    :cond_6
    iget-object v5, v0, Landroid/os/AsyncResult;->result:Ljava/lang/Object;

    check-cast v5, [B

    move-object v1, v5

    check-cast v1, [B

    invoke-static {v1}, Lcom/android/internal/telephony/IccUtils;->bytesToHexString([B)Ljava/lang/String;

    move-result-object v3

    aget-byte v5, v1, v9

    and-int/lit16 v5, v5, 0xff

    if-ne v5, v8, :cond_7

    const-string v5, "SIMME Lock"

    const-string v6, "SIM2 GID1 not initialized"

    invoke-static {v5, v6}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v5, p0, Lcom/android/simmelock/SPAddLockSetting$11;->this$0:Lcom/android/simmelock/SPAddLockSetting;

    iput v8, v5, Lcom/android/simmelock/SPAddLockSetting;->intSIMGID1:I

    goto/16 :goto_0

    :cond_7
    iget-object v5, p0, Lcom/android/simmelock/SPAddLockSetting$11;->this$0:Lcom/android/simmelock/SPAddLockSetting;

    iput v11, v5, Lcom/android/simmelock/SPAddLockSetting;->intSIMGID1:I

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v5

    if-lt v5, v10, :cond_8

    iget-object v5, p0, Lcom/android/simmelock/SPAddLockSetting$11;->this$0:Lcom/android/simmelock/SPAddLockSetting;

    invoke-static {v1}, Lcom/android/internal/telephony/IccUtils;->bytesToHexString([B)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v6, v9, v10}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v6

    invoke-static {v6}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v6

    invoke-static {v6}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v6

    iput-object v6, v5, Lcom/android/simmelock/SPAddLockSetting;->msSIM2GID1:Ljava/lang/String;

    goto/16 :goto_0

    :cond_8
    iget-object v5, p0, Lcom/android/simmelock/SPAddLockSetting$11;->this$0:Lcom/android/simmelock/SPAddLockSetting;

    invoke-static {v1}, Lcom/android/internal/telephony/IccUtils;->bytesToHexString([B)Ljava/lang/String;

    move-result-object v6

    invoke-static {v6}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v6

    invoke-static {v6}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v6

    iput-object v6, v5, Lcom/android/simmelock/SPAddLockSetting;->msSIM2GID1:Ljava/lang/String;

    const-string v5, "SIMME Lock"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "Normal SIM2 GID1 :"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    iget-object v7, p0, Lcom/android/simmelock/SPAddLockSetting$11;->this$0:Lcom/android/simmelock/SPAddLockSetting;

    iget-object v7, v7, Lcom/android/simmelock/SPAddLockSetting;->msSIM2GID1:Ljava/lang/String;

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x24 -> :sswitch_1
        0x26 -> :sswitch_2
        0x28 -> :sswitch_3
        0x78 -> :sswitch_0
    .end sparse-switch
.end method
