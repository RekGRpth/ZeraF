.class Lcom/android/simmelock/UnlockSetting$8;
.super Landroid/os/Handler;
.source "UnlockSetting.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/simmelock/UnlockSetting;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/simmelock/UnlockSetting;


# direct methods
.method constructor <init>(Lcom/android/simmelock/UnlockSetting;)V
    .locals 0

    iput-object p1, p0, Lcom/android/simmelock/UnlockSetting$8;->this$0:Lcom/android/simmelock/UnlockSetting;

    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 13
    .param p1    # Landroid/os/Message;

    const/16 v12, 0x6e

    const/4 v6, 0x1

    const/4 v5, 0x2

    const/4 v2, 0x0

    const/4 v3, 0x0

    iget-object v9, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v9, Landroid/os/AsyncResult;

    iget v0, p1, Landroid/os/Message;->what:I

    sparse-switch v0, :sswitch_data_0

    :cond_0
    :goto_0
    return-void

    :sswitch_0
    iget-object v0, p0, Lcom/android/simmelock/UnlockSetting$8;->this$0:Lcom/android/simmelock/UnlockSetting;

    invoke-static {v0}, Lcom/android/simmelock/UnlockSetting;->access$000(Lcom/android/simmelock/UnlockSetting;)Z

    move-result v0

    if-ne v0, v6, :cond_0

    const-string v0, "SIMMELOCK"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "set ar: "

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v4, v9, Landroid/os/AsyncResult;->exception:Ljava/lang/Throwable;

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v0, v9, Landroid/os/AsyncResult;->exception:Ljava/lang/Throwable;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/android/simmelock/UnlockSetting$8;->this$0:Lcom/android/simmelock/UnlockSetting;

    iget-object v1, p0, Lcom/android/simmelock/UnlockSetting$8;->this$0:Lcom/android/simmelock/UnlockSetting;

    iget-object v1, v1, Lcom/android/simmelock/UnlockSetting;->bundle:Landroid/os/Bundle;

    const-string v4, "SIMNo"

    invoke-virtual {v1, v4}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v1

    iput v1, v0, Lcom/android/simmelock/UnlockSetting;->intSIMNumber:I

    const-string v0, "SIMMELOCK"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "[btnconfirm]intSIMNumber: "

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v4, p0, Lcom/android/simmelock/UnlockSetting$8;->this$0:Lcom/android/simmelock/UnlockSetting;

    iget v4, v4, Lcom/android/simmelock/UnlockSetting;->intSIMNumber:I

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    invoke-static {}, Lcom/android/internal/telephony/PhoneFactory;->getDefaultPhone()Lcom/android/internal/telephony/Phone;

    move-result-object v11

    check-cast v11, Lcom/android/internal/telephony/gemini/GeminiPhone;

    iget-object v0, p0, Lcom/android/simmelock/UnlockSetting$8;->this$0:Lcom/android/simmelock/UnlockSetting;

    iget v0, v0, Lcom/android/simmelock/UnlockSetting;->intSIMNumber:I

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/android/simmelock/UnlockSetting$8;->this$0:Lcom/android/simmelock/UnlockSetting;

    invoke-static {v0}, Lcom/android/simmelock/UnlockSetting;->access$100(Lcom/android/simmelock/UnlockSetting;)Landroid/os/Handler;

    move-result-object v0

    invoke-static {v0, v12}, Landroid/os/Message;->obtain(Landroid/os/Handler;I)Landroid/os/Message;

    move-result-object v7

    invoke-virtual {v11, v2}, Lcom/android/internal/telephony/gemini/GeminiPhone;->getIccCardGemini(I)Lcom/android/internal/telephony/IccCard;

    move-result-object v0

    iget-object v1, p0, Lcom/android/simmelock/UnlockSetting$8;->this$0:Lcom/android/simmelock/UnlockSetting;

    iget v1, v1, Lcom/android/simmelock/UnlockSetting;->lockCategory:I

    move-object v4, v3

    move-object v5, v3

    move-object v6, v3

    invoke-interface/range {v0 .. v7}, Lcom/android/internal/telephony/IccCard;->QueryIccNetworkLock(IILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Landroid/os/Message;)V

    :goto_1
    iget-object v0, p0, Lcom/android/simmelock/UnlockSetting$8;->this$0:Lcom/android/simmelock/UnlockSetting;

    const/4 v1, 0x3

    invoke-virtual {v0, v1}, Lcom/android/simmelock/UnlockSetting;->showAlertDialog(I)V

    :goto_2
    const-string v0, "SIMMELOCK"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "handler UNLOCK_ICC_SML_COMPLETE mPwdLeftChances: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/android/simmelock/UnlockSetting$8;->this$0:Lcom/android/simmelock/UnlockSetting;

    invoke-static {v2}, Lcom/android/simmelock/UnlockSetting;->access$200(Lcom/android/simmelock/UnlockSetting;)I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0

    :cond_1
    iget-object v0, p0, Lcom/android/simmelock/UnlockSetting$8;->this$0:Lcom/android/simmelock/UnlockSetting;

    invoke-static {v0}, Lcom/android/simmelock/UnlockSetting;->access$100(Lcom/android/simmelock/UnlockSetting;)Landroid/os/Handler;

    move-result-object v0

    invoke-static {v0, v12}, Landroid/os/Message;->obtain(Landroid/os/Handler;I)Landroid/os/Message;

    move-result-object v7

    invoke-virtual {v11, v6}, Lcom/android/internal/telephony/gemini/GeminiPhone;->getIccCardGemini(I)Lcom/android/internal/telephony/IccCard;

    move-result-object v0

    iget-object v1, p0, Lcom/android/simmelock/UnlockSetting$8;->this$0:Lcom/android/simmelock/UnlockSetting;

    iget v1, v1, Lcom/android/simmelock/UnlockSetting;->lockCategory:I

    move-object v4, v3

    move-object v5, v3

    move-object v6, v3

    invoke-interface/range {v0 .. v7}, Lcom/android/internal/telephony/IccCard;->QueryIccNetworkLock(IILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Landroid/os/Message;)V

    goto :goto_1

    :cond_2
    iget-object v0, p0, Lcom/android/simmelock/UnlockSetting$8;->this$0:Lcom/android/simmelock/UnlockSetting;

    invoke-virtual {v0, v5}, Lcom/android/simmelock/UnlockSetting;->showAlertDialog(I)V

    goto :goto_2

    :sswitch_1
    const-string v0, "SIMMELOCK"

    const-string v1, "handler query"

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    const-string v0, "SIMMELOCK"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "query ar: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, v9, Landroid/os/AsyncResult;->exception:Ljava/lang/Throwable;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v0, v9, Landroid/os/AsyncResult;->exception:Ljava/lang/Throwable;

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/android/simmelock/UnlockSetting$8;->this$0:Lcom/android/simmelock/UnlockSetting;

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Lcom/android/simmelock/UnlockSetting;->showAlertDialog(I)V

    goto/16 :goto_0

    :cond_3
    iget-object v10, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v10, Landroid/os/AsyncResult;

    iget-object v0, v10, Landroid/os/AsyncResult;->result:Ljava/lang/Object;

    check-cast v0, [I

    move-object v8, v0

    check-cast v8, [I

    aget v0, v8, v5

    if-lez v0, :cond_4

    iget-object v0, p0, Lcom/android/simmelock/UnlockSetting$8;->this$0:Lcom/android/simmelock/UnlockSetting;

    aget v1, v8, v5

    invoke-static {v0, v1}, Lcom/android/simmelock/UnlockSetting;->access$202(Lcom/android/simmelock/UnlockSetting;I)I

    iget-object v0, p0, Lcom/android/simmelock/UnlockSetting$8;->this$0:Lcom/android/simmelock/UnlockSetting;

    iget-object v0, v0, Lcom/android/simmelock/UnlockSetting;->etPwdLeftChances:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/android/simmelock/UnlockSetting$8;->this$0:Lcom/android/simmelock/UnlockSetting;

    invoke-static {v1}, Lcom/android/simmelock/UnlockSetting;->access$200(Lcom/android/simmelock/UnlockSetting;)I

    move-result v1

    invoke-static {v1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    const-string v0, "SIMMELOCK"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "query mPwdLeftChances: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/android/simmelock/UnlockSetting$8;->this$0:Lcom/android/simmelock/UnlockSetting;

    invoke-static {v2}, Lcom/android/simmelock/UnlockSetting;->access$200(Lcom/android/simmelock/UnlockSetting;)I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0

    :cond_4
    iget-object v0, p0, Lcom/android/simmelock/UnlockSetting$8;->this$0:Lcom/android/simmelock/UnlockSetting;

    invoke-virtual {v0}, Landroid/app/Activity;->finish()V

    goto/16 :goto_0

    :sswitch_data_0
    .sparse-switch
        0x6e -> :sswitch_1
        0x78 -> :sswitch_0
    .end sparse-switch
.end method
