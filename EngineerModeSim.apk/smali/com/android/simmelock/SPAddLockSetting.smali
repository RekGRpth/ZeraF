.class public Lcom/android/simmelock/SPAddLockSetting;
.super Landroid/app/Activity;
.source "SPAddLockSetting.java"

# interfaces
.implements Landroid/content/DialogInterface$OnKeyListener;


# static fields
.field private static final ADDLOCK_ICC_SML_COMPLETE:I = 0x78

.field private static final EVENT_GET_SIM1_GID1:I = 0x26

.field private static final EVENT_GET_SIM2_GID1:I = 0x28

.field private static final EVENT_GET_SIM_GID1:I = 0x24


# instance fields
.field final DIALOG_ADDLOCKFAIL:I

.field final DIALOG_ADDLOCKSUCCEED:I

.field final DIALOG_GID1WRONG:I

.field final DIALOG_MCCMNCLENGTHINCORRECT:I

.field final DIALOG_PASSWORDLENGTHINCORRECT:I

.field final DIALOG_PASSWORDWRONG:I

.field bundle:Landroid/os/Bundle;

.field private clickFlag:Z

.field etGID1:Landroid/widget/EditText;

.field etMCCMNC:Landroid/widget/EditText;

.field etPwd:Landroid/widget/EditText;

.field etPwdConfirm:Landroid/widget/EditText;

.field intSIMGID1:I

.field intSIMNumber:I

.field private lockCategory:I

.field private lockName:Ljava/lang/String;

.field private mHandler:Landroid/os/Handler;

.field private final mReceiver:Landroid/content/BroadcastReceiver;

.field mbGID1Correct:Z

.field mbGID1ReadSIM:Z

.field mbGID1ReadSIM1:Z

.field mbGID1ReadSIM2:Z

.field mbMCCMNCCorrect:Z

.field mbMCCMNCReadSIM:Z

.field mbMCCMNCReadSIM1:Z

.field mbMCCMNCReadSIM2:Z

.field miSIM1State:I

.field miSIM2State:I

.field miSIMState:I

.field msSIM1GID1:Ljava/lang/String;

.field msSIM1MCCMNC:Ljava/lang/String;

.field msSIM2GID1:Ljava/lang/String;

.field msSIM2MCCMNC:Ljava/lang/String;

.field msSIMGID1:Ljava/lang/String;

.field msSIMMCCMNC:Ljava/lang/String;

.field protected phone:Lcom/android/internal/telephony/PhoneBase;

.field protected phone_ori:Lcom/android/internal/telephony/Phone;

.field s1:Landroid/widget/Spinner;

.field s2:Landroid/widget/Spinner;


# direct methods
.method public constructor <init>()V
    .locals 3

    const/4 v2, 0x0

    const/4 v1, 0x0

    invoke-direct {p0}, Landroid/app/Activity;-><init>()V

    new-instance v0, Lcom/android/simmelock/SPAddLockSetting$11;

    invoke-direct {v0, p0}, Lcom/android/simmelock/SPAddLockSetting$11;-><init>(Lcom/android/simmelock/SPAddLockSetting;)V

    iput-object v0, p0, Lcom/android/simmelock/SPAddLockSetting;->mHandler:Landroid/os/Handler;

    new-instance v0, Lcom/android/simmelock/SPAddLockSetting$12;

    invoke-direct {v0, p0}, Lcom/android/simmelock/SPAddLockSetting$12;-><init>(Lcom/android/simmelock/SPAddLockSetting;)V

    iput-object v0, p0, Lcom/android/simmelock/SPAddLockSetting;->mReceiver:Landroid/content/BroadcastReceiver;

    iput-object v2, p0, Lcom/android/simmelock/SPAddLockSetting;->etMCCMNC:Landroid/widget/EditText;

    iput-object v2, p0, Lcom/android/simmelock/SPAddLockSetting;->etGID1:Landroid/widget/EditText;

    iput-object v2, p0, Lcom/android/simmelock/SPAddLockSetting;->etPwd:Landroid/widget/EditText;

    iput-object v2, p0, Lcom/android/simmelock/SPAddLockSetting;->etPwdConfirm:Landroid/widget/EditText;

    iput-object v2, p0, Lcom/android/simmelock/SPAddLockSetting;->lockName:Ljava/lang/String;

    const/4 v0, -0x1

    iput v0, p0, Lcom/android/simmelock/SPAddLockSetting;->lockCategory:I

    iput v1, p0, Lcom/android/simmelock/SPAddLockSetting;->miSIM1State:I

    iput v1, p0, Lcom/android/simmelock/SPAddLockSetting;->miSIM2State:I

    iput v1, p0, Lcom/android/simmelock/SPAddLockSetting;->miSIMState:I

    iput-object v2, p0, Lcom/android/simmelock/SPAddLockSetting;->msSIM1MCCMNC:Ljava/lang/String;

    iput-object v2, p0, Lcom/android/simmelock/SPAddLockSetting;->msSIM2MCCMNC:Ljava/lang/String;

    iput-object v2, p0, Lcom/android/simmelock/SPAddLockSetting;->msSIMMCCMNC:Ljava/lang/String;

    iput-object v2, p0, Lcom/android/simmelock/SPAddLockSetting;->msSIMGID1:Ljava/lang/String;

    iput-object v2, p0, Lcom/android/simmelock/SPAddLockSetting;->msSIM1GID1:Ljava/lang/String;

    iput-object v2, p0, Lcom/android/simmelock/SPAddLockSetting;->msSIM2GID1:Ljava/lang/String;

    iput-boolean v1, p0, Lcom/android/simmelock/SPAddLockSetting;->mbMCCMNCReadSIM1:Z

    iput-boolean v1, p0, Lcom/android/simmelock/SPAddLockSetting;->mbMCCMNCReadSIM2:Z

    iput-boolean v1, p0, Lcom/android/simmelock/SPAddLockSetting;->mbMCCMNCReadSIM:Z

    iput-boolean v1, p0, Lcom/android/simmelock/SPAddLockSetting;->mbGID1ReadSIM:Z

    iput-boolean v1, p0, Lcom/android/simmelock/SPAddLockSetting;->mbGID1ReadSIM1:Z

    iput-boolean v1, p0, Lcom/android/simmelock/SPAddLockSetting;->mbGID1ReadSIM2:Z

    iput-boolean v1, p0, Lcom/android/simmelock/SPAddLockSetting;->mbMCCMNCCorrect:Z

    iput-boolean v1, p0, Lcom/android/simmelock/SPAddLockSetting;->mbGID1Correct:Z

    iput v1, p0, Lcom/android/simmelock/SPAddLockSetting;->intSIMGID1:I

    iput v1, p0, Lcom/android/simmelock/SPAddLockSetting;->intSIMNumber:I

    iput-object v2, p0, Lcom/android/simmelock/SPAddLockSetting;->bundle:Landroid/os/Bundle;

    iput-boolean v1, p0, Lcom/android/simmelock/SPAddLockSetting;->clickFlag:Z

    const/4 v0, 0x1

    iput v0, p0, Lcom/android/simmelock/SPAddLockSetting;->DIALOG_MCCMNCLENGTHINCORRECT:I

    const/4 v0, 0x2

    iput v0, p0, Lcom/android/simmelock/SPAddLockSetting;->DIALOG_ADDLOCKFAIL:I

    const/4 v0, 0x3

    iput v0, p0, Lcom/android/simmelock/SPAddLockSetting;->DIALOG_ADDLOCKSUCCEED:I

    const/4 v0, 0x4

    iput v0, p0, Lcom/android/simmelock/SPAddLockSetting;->DIALOG_PASSWORDLENGTHINCORRECT:I

    const/4 v0, 0x5

    iput v0, p0, Lcom/android/simmelock/SPAddLockSetting;->DIALOG_PASSWORDWRONG:I

    const/4 v0, 0x6

    iput v0, p0, Lcom/android/simmelock/SPAddLockSetting;->DIALOG_GID1WRONG:I

    return-void
.end method

.method static synthetic access$000(Lcom/android/simmelock/SPAddLockSetting;)Z
    .locals 1
    .param p0    # Lcom/android/simmelock/SPAddLockSetting;

    iget-boolean v0, p0, Lcom/android/simmelock/SPAddLockSetting;->clickFlag:Z

    return v0
.end method

.method static synthetic access$002(Lcom/android/simmelock/SPAddLockSetting;Z)Z
    .locals 0
    .param p0    # Lcom/android/simmelock/SPAddLockSetting;
    .param p1    # Z

    iput-boolean p1, p0, Lcom/android/simmelock/SPAddLockSetting;->clickFlag:Z

    return p1
.end method

.method static synthetic access$100(Lcom/android/simmelock/SPAddLockSetting;)Landroid/os/Handler;
    .locals 1
    .param p0    # Lcom/android/simmelock/SPAddLockSetting;

    iget-object v0, p0, Lcom/android/simmelock/SPAddLockSetting;->mHandler:Landroid/os/Handler;

    return-object v0
.end method

.method private getLockName(I)Ljava/lang/String;
    .locals 1
    .param p1    # I

    packed-switch p1, :pswitch_data_0

    const v0, 0x7f060001

    invoke-virtual {p0, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    :goto_0
    return-object v0

    :pswitch_0
    const v0, 0x7f060016

    invoke-virtual {p0, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    :pswitch_1
    const v0, 0x7f060017

    invoke-virtual {p0, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    :pswitch_2
    const v0, 0x7f060018

    invoke-virtual {p0, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    :pswitch_3
    const v0, 0x7f060019

    invoke-virtual {p0, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    :pswitch_4
    const v0, 0x7f06001a

    invoke-virtual {p0, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
    .end packed-switch
.end method


# virtual methods
.method public onCreate(Landroid/os/Bundle;)V
    .locals 14
    .param p1    # Landroid/os/Bundle;

    const v13, 0x1090009

    const v12, 0x1090008

    const/16 v11, 0x81

    const/16 v10, 0x8

    const/4 v9, -0x1

    invoke-super {p0, p1}, Landroid/app/Activity;->onCreate(Landroid/os/Bundle;)V

    const v7, 0x7f030007

    invoke-virtual {p0, v7}, Landroid/app/Activity;->setContentView(I)V

    invoke-virtual {p0}, Landroid/app/Activity;->getIntent()Landroid/content/Intent;

    move-result-object v7

    invoke-virtual {v7}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v7

    iput-object v7, p0, Lcom/android/simmelock/SPAddLockSetting;->bundle:Landroid/os/Bundle;

    iget-object v7, p0, Lcom/android/simmelock/SPAddLockSetting;->bundle:Landroid/os/Bundle;

    if-eqz v7, :cond_0

    iget-object v7, p0, Lcom/android/simmelock/SPAddLockSetting;->bundle:Landroid/os/Bundle;

    const-string v8, "LockCategory"

    invoke-virtual {v7, v8, v9}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v7

    iput v7, p0, Lcom/android/simmelock/SPAddLockSetting;->lockCategory:I

    :cond_0
    iget v7, p0, Lcom/android/simmelock/SPAddLockSetting;->lockCategory:I

    if-ne v7, v9, :cond_1

    invoke-virtual {p0}, Landroid/app/Activity;->finish()V

    :goto_0
    return-void

    :cond_1
    iget v7, p0, Lcom/android/simmelock/SPAddLockSetting;->lockCategory:I

    invoke-direct {p0, v7}, Lcom/android/simmelock/SPAddLockSetting;->getLockName(I)Ljava/lang/String;

    move-result-object v7

    iput-object v7, p0, Lcom/android/simmelock/SPAddLockSetting;->lockName:Ljava/lang/String;

    iget-object v7, p0, Lcom/android/simmelock/SPAddLockSetting;->lockName:Ljava/lang/String;

    invoke-virtual {p0, v7}, Landroid/app/Activity;->setTitle(Ljava/lang/CharSequence;)V

    const v7, 0x7f070032

    invoke-virtual {p0, v7}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v7

    check-cast v7, Landroid/widget/EditText;

    iput-object v7, p0, Lcom/android/simmelock/SPAddLockSetting;->etMCCMNC:Landroid/widget/EditText;

    const v7, 0x7f070035

    invoke-virtual {p0, v7}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v7

    check-cast v7, Landroid/widget/EditText;

    iput-object v7, p0, Lcom/android/simmelock/SPAddLockSetting;->etGID1:Landroid/widget/EditText;

    const v7, 0x7f070031

    invoke-virtual {p0, v7}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v7

    check-cast v7, Landroid/widget/Spinner;

    iput-object v7, p0, Lcom/android/simmelock/SPAddLockSetting;->s1:Landroid/widget/Spinner;

    const/high16 v7, 0x7f050000

    invoke-static {p0, v7, v12}, Landroid/widget/ArrayAdapter;->createFromResource(Landroid/content/Context;II)Landroid/widget/ArrayAdapter;

    move-result-object v0

    invoke-virtual {v0, v13}, Landroid/widget/ArrayAdapter;->setDropDownViewResource(I)V

    iget-object v7, p0, Lcom/android/simmelock/SPAddLockSetting;->s1:Landroid/widget/Spinner;

    invoke-virtual {v7, v0}, Landroid/widget/Spinner;->setAdapter(Landroid/widget/SpinnerAdapter;)V

    new-instance v5, Lcom/android/simmelock/SPAddLockSetting$1;

    invoke-direct {v5, p0}, Lcom/android/simmelock/SPAddLockSetting$1;-><init>(Lcom/android/simmelock/SPAddLockSetting;)V

    iget-object v7, p0, Lcom/android/simmelock/SPAddLockSetting;->s1:Landroid/widget/Spinner;

    invoke-virtual {v7, v5}, Landroid/widget/AdapterView;->setOnItemSelectedListener(Landroid/widget/AdapterView$OnItemSelectedListener;)V

    const v7, 0x7f070034

    invoke-virtual {p0, v7}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v7

    check-cast v7, Landroid/widget/Spinner;

    iput-object v7, p0, Lcom/android/simmelock/SPAddLockSetting;->s2:Landroid/widget/Spinner;

    const/high16 v7, 0x7f050000

    invoke-static {p0, v7, v12}, Landroid/widget/ArrayAdapter;->createFromResource(Landroid/content/Context;II)Landroid/widget/ArrayAdapter;

    move-result-object v1

    invoke-virtual {v1, v13}, Landroid/widget/ArrayAdapter;->setDropDownViewResource(I)V

    iget-object v7, p0, Lcom/android/simmelock/SPAddLockSetting;->s2:Landroid/widget/Spinner;

    invoke-virtual {v7, v1}, Landroid/widget/Spinner;->setAdapter(Landroid/widget/SpinnerAdapter;)V

    new-instance v6, Lcom/android/simmelock/SPAddLockSetting$2;

    invoke-direct {v6, p0}, Lcom/android/simmelock/SPAddLockSetting$2;-><init>(Lcom/android/simmelock/SPAddLockSetting;)V

    iget-object v7, p0, Lcom/android/simmelock/SPAddLockSetting;->s2:Landroid/widget/Spinner;

    invoke-virtual {v7, v6}, Landroid/widget/AdapterView;->setOnItemSelectedListener(Landroid/widget/AdapterView$OnItemSelectedListener;)V

    const v7, 0x7f070037

    invoke-virtual {p0, v7}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v7

    check-cast v7, Landroid/widget/EditText;

    iput-object v7, p0, Lcom/android/simmelock/SPAddLockSetting;->etPwd:Landroid/widget/EditText;

    const v7, 0x7f070039

    invoke-virtual {p0, v7}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v7

    check-cast v7, Landroid/widget/EditText;

    iput-object v7, p0, Lcom/android/simmelock/SPAddLockSetting;->etPwdConfirm:Landroid/widget/EditText;

    iget-object v7, p0, Lcom/android/simmelock/SPAddLockSetting;->etPwd:Landroid/widget/EditText;

    if-eqz v7, :cond_2

    iget-object v7, p0, Lcom/android/simmelock/SPAddLockSetting;->etPwdConfirm:Landroid/widget/EditText;

    if-nez v7, :cond_3

    :cond_2
    const-string v7, "X"

    const-string v8, "clocwork worked..."

    invoke-static {v7, v8}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    :cond_3
    iget-object v7, p0, Lcom/android/simmelock/SPAddLockSetting;->etPwd:Landroid/widget/EditText;

    invoke-virtual {v7, v11}, Landroid/widget/TextView;->setInputType(I)V

    iget-object v7, p0, Lcom/android/simmelock/SPAddLockSetting;->etPwd:Landroid/widget/EditText;

    invoke-static {v7, v10}, Lcom/android/simmelock/SMLCommonProcess;->limitEditTextPassword(Landroid/widget/EditText;I)V

    iget-object v7, p0, Lcom/android/simmelock/SPAddLockSetting;->etPwdConfirm:Landroid/widget/EditText;

    invoke-virtual {v7, v11}, Landroid/widget/TextView;->setInputType(I)V

    iget-object v7, p0, Lcom/android/simmelock/SPAddLockSetting;->etPwdConfirm:Landroid/widget/EditText;

    invoke-static {v7, v10}, Lcom/android/simmelock/SMLCommonProcess;->limitEditTextPassword(Landroid/widget/EditText;I)V

    const v7, 0x7f07003a

    invoke-virtual {p0, v7}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/Button;

    new-instance v7, Lcom/android/simmelock/SPAddLockSetting$3;

    invoke-direct {v7, p0}, Lcom/android/simmelock/SPAddLockSetting$3;-><init>(Lcom/android/simmelock/SPAddLockSetting;)V

    invoke-virtual {v3, v7}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    const v7, 0x7f07003b

    invoke-virtual {p0, v7}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/Button;

    new-instance v7, Lcom/android/simmelock/SPAddLockSetting$4;

    invoke-direct {v7, p0}, Lcom/android/simmelock/SPAddLockSetting$4;-><init>(Lcom/android/simmelock/SPAddLockSetting;)V

    invoke-virtual {v2, v7}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    new-instance v4, Landroid/content/IntentFilter;

    const-string v7, "android.intent.action.AIRPLANE_MODE"

    invoke-direct {v4, v7}, Landroid/content/IntentFilter;-><init>(Ljava/lang/String;)V

    iget-object v7, p0, Lcom/android/simmelock/SPAddLockSetting;->mReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {p0, v7, v4}, Landroid/content/ContextWrapper;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    goto/16 :goto_0
.end method

.method protected onCreateDialog(I)Landroid/app/Dialog;
    .locals 5
    .param p1    # I

    const v4, 0x7f06000f

    const v3, 0x7f06000c

    const v2, 0x1080027

    const/4 v1, 0x0

    new-instance v0, Landroid/app/AlertDialog$Builder;

    invoke-direct {v0, p0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    packed-switch p1, :pswitch_data_0

    :goto_0
    invoke-super {p0, p1}, Landroid/app/Activity;->onCreateDialog(I)Landroid/app/Dialog;

    move-result-object v1

    return-object v1

    :pswitch_0
    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setCancelable(Z)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    invoke-virtual {v1, v3}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    invoke-virtual {v1, v2}, Landroid/app/AlertDialog$Builder;->setIcon(I)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    const v2, 0x7f060022

    invoke-virtual {v1, v2}, Landroid/app/AlertDialog$Builder;->setMessage(I)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    invoke-virtual {v1, p0}, Landroid/app/AlertDialog$Builder;->setOnKeyListener(Landroid/content/DialogInterface$OnKeyListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    new-instance v2, Lcom/android/simmelock/SPAddLockSetting$5;

    invoke-direct {v2, p0}, Lcom/android/simmelock/SPAddLockSetting$5;-><init>(Lcom/android/simmelock/SPAddLockSetting;)V

    invoke-virtual {v1, v4, v2}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    invoke-virtual {v1}, Landroid/app/AlertDialog$Builder;->show()Landroid/app/AlertDialog;

    goto :goto_0

    :pswitch_1
    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setCancelable(Z)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    invoke-virtual {v1, v3}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    invoke-virtual {v1, v2}, Landroid/app/AlertDialog$Builder;->setIcon(I)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    const v2, 0x7f060020

    invoke-virtual {v1, v2}, Landroid/app/AlertDialog$Builder;->setMessage(I)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    invoke-virtual {v1, p0}, Landroid/app/AlertDialog$Builder;->setOnKeyListener(Landroid/content/DialogInterface$OnKeyListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    new-instance v2, Lcom/android/simmelock/SPAddLockSetting$6;

    invoke-direct {v2, p0}, Lcom/android/simmelock/SPAddLockSetting$6;-><init>(Lcom/android/simmelock/SPAddLockSetting;)V

    invoke-virtual {v1, v4, v2}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    invoke-virtual {v1}, Landroid/app/AlertDialog$Builder;->show()Landroid/app/AlertDialog;

    goto :goto_0

    :pswitch_2
    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setCancelable(Z)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    invoke-virtual {v1, v3}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    invoke-virtual {v1, v2}, Landroid/app/AlertDialog$Builder;->setIcon(I)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    const v2, 0x7f060024

    invoke-virtual {v1, v2}, Landroid/app/AlertDialog$Builder;->setMessage(I)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    invoke-virtual {v1, p0}, Landroid/app/AlertDialog$Builder;->setOnKeyListener(Landroid/content/DialogInterface$OnKeyListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    new-instance v2, Lcom/android/simmelock/SPAddLockSetting$7;

    invoke-direct {v2, p0}, Lcom/android/simmelock/SPAddLockSetting$7;-><init>(Lcom/android/simmelock/SPAddLockSetting;)V

    invoke-virtual {v1, v4, v2}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    invoke-virtual {v1}, Landroid/app/AlertDialog$Builder;->show()Landroid/app/AlertDialog;

    goto :goto_0

    :pswitch_3
    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setCancelable(Z)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    invoke-virtual {v1, v3}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    invoke-virtual {v1, v2}, Landroid/app/AlertDialog$Builder;->setIcon(I)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    const v2, 0x7f060023

    invoke-virtual {v1, v2}, Landroid/app/AlertDialog$Builder;->setMessage(I)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    invoke-virtual {v1, p0}, Landroid/app/AlertDialog$Builder;->setOnKeyListener(Landroid/content/DialogInterface$OnKeyListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    new-instance v2, Lcom/android/simmelock/SPAddLockSetting$8;

    invoke-direct {v2, p0}, Lcom/android/simmelock/SPAddLockSetting$8;-><init>(Lcom/android/simmelock/SPAddLockSetting;)V

    invoke-virtual {v1, v4, v2}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    invoke-virtual {v1}, Landroid/app/AlertDialog$Builder;->show()Landroid/app/AlertDialog;

    goto/16 :goto_0

    :pswitch_4
    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setCancelable(Z)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    invoke-virtual {v1, v3}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    invoke-virtual {v1, v2}, Landroid/app/AlertDialog$Builder;->setIcon(I)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    const v2, 0x7f060027

    invoke-virtual {v1, v2}, Landroid/app/AlertDialog$Builder;->setMessage(I)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    invoke-virtual {v1, p0}, Landroid/app/AlertDialog$Builder;->setOnKeyListener(Landroid/content/DialogInterface$OnKeyListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    new-instance v2, Lcom/android/simmelock/SPAddLockSetting$9;

    invoke-direct {v2, p0}, Lcom/android/simmelock/SPAddLockSetting$9;-><init>(Lcom/android/simmelock/SPAddLockSetting;)V

    invoke-virtual {v1, v4, v2}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    invoke-virtual {v1}, Landroid/app/AlertDialog$Builder;->show()Landroid/app/AlertDialog;

    goto/16 :goto_0

    :pswitch_5
    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setCancelable(Z)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    invoke-virtual {v1, v3}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    invoke-virtual {v1, v2}, Landroid/app/AlertDialog$Builder;->setIcon(I)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    const v2, 0x7f060029

    invoke-virtual {v1, v2}, Landroid/app/AlertDialog$Builder;->setMessage(I)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    invoke-virtual {v1, p0}, Landroid/app/AlertDialog$Builder;->setOnKeyListener(Landroid/content/DialogInterface$OnKeyListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    new-instance v2, Lcom/android/simmelock/SPAddLockSetting$10;

    invoke-direct {v2, p0}, Lcom/android/simmelock/SPAddLockSetting$10;-><init>(Lcom/android/simmelock/SPAddLockSetting;)V

    invoke-virtual {v1, v4, v2}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    invoke-virtual {v1}, Landroid/app/AlertDialog$Builder;->show()Landroid/app/AlertDialog;

    goto/16 :goto_0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_2
        :pswitch_0
        :pswitch_3
        :pswitch_1
        :pswitch_5
        :pswitch_4
    .end packed-switch
.end method

.method protected onDestroy()V
    .locals 1

    invoke-super {p0}, Landroid/app/Activity;->onDestroy()V

    iget-object v0, p0, Lcom/android/simmelock/SPAddLockSetting;->mReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {p0, v0}, Landroid/content/ContextWrapper;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    return-void
.end method

.method public onKey(Landroid/content/DialogInterface;ILandroid/view/KeyEvent;)Z
    .locals 2
    .param p1    # Landroid/content/DialogInterface;
    .param p2    # I
    .param p3    # Landroid/view/KeyEvent;

    invoke-virtual {p3}, Landroid/view/KeyEvent;->getKeyCode()I

    move-result v0

    const/4 v1, 0x4

    if-ne v0, v1, :cond_0

    invoke-interface {p1}, Landroid/content/DialogInterface;->dismiss()V

    invoke-virtual {p0}, Landroid/app/Activity;->finish()V

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected onResume()V
    .locals 9

    const/16 v8, 0x6f3e

    const/4 v7, 0x0

    const/4 v6, 0x1

    invoke-super {p0}, Landroid/app/Activity;->onResume()V

    invoke-static {}, Lcom/mediatek/telephony/TelephonyManagerEx;->getDefault()Lcom/mediatek/telephony/TelephonyManagerEx;

    move-result-object v3

    const-string v4, "phone"

    invoke-virtual {p0, v4}, Landroid/app/Activity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/telephony/TelephonyManager;

    invoke-virtual {v3, v7}, Lcom/mediatek/telephony/TelephonyManagerEx;->getSimState(I)I

    move-result v4

    iput v4, p0, Lcom/android/simmelock/SPAddLockSetting;->miSIM1State:I

    invoke-virtual {v3, v6}, Lcom/mediatek/telephony/TelephonyManagerEx;->getSimState(I)I

    move-result v4

    iput v4, p0, Lcom/android/simmelock/SPAddLockSetting;->miSIM2State:I

    iget v4, p0, Lcom/android/simmelock/SPAddLockSetting;->miSIM1State:I

    if-ne v6, v4, :cond_0

    const-string v4, "SIMMELOCK"

    const-string v5, "Add SP lock fail : SIM1 not ready!"

    invoke-static {v4, v5}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    iget v4, p0, Lcom/android/simmelock/SPAddLockSetting;->miSIM2State:I

    if-ne v6, v4, :cond_1

    const-string v4, "SIMMELOCK"

    const-string v5, "Add SP lock fail : SIM2 not ready!"

    invoke-static {v4, v5}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    :cond_1
    invoke-virtual {v3, v7}, Lcom/mediatek/telephony/TelephonyManagerEx;->getSimOperator(I)Ljava/lang/String;

    move-result-object v4

    iput-object v4, p0, Lcom/android/simmelock/SPAddLockSetting;->msSIM1MCCMNC:Ljava/lang/String;

    iget-object v4, p0, Lcom/android/simmelock/SPAddLockSetting;->msSIM1MCCMNC:Ljava/lang/String;

    if-nez v4, :cond_2

    const-string v4, "SIMMELOCK"

    const-string v5, "Fail to read SIM1 MCC+MNC!"

    invoke-static {v4, v5}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    :goto_0
    invoke-virtual {v3, v6}, Lcom/mediatek/telephony/TelephonyManagerEx;->getSimOperator(I)Ljava/lang/String;

    move-result-object v4

    iput-object v4, p0, Lcom/android/simmelock/SPAddLockSetting;->msSIM2MCCMNC:Ljava/lang/String;

    iget-object v4, p0, Lcom/android/simmelock/SPAddLockSetting;->msSIM2MCCMNC:Ljava/lang/String;

    if-nez v4, :cond_3

    const-string v4, "SIMMELOCK"

    const-string v5, "Fail to read SIM2 MCC+MNC!"

    invoke-static {v4, v5}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    :goto_1
    iget-object v4, p0, Lcom/android/simmelock/SPAddLockSetting;->bundle:Landroid/os/Bundle;

    const-string v5, "SIMNo"

    invoke-virtual {v4, v5}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v4

    iput v4, p0, Lcom/android/simmelock/SPAddLockSetting;->intSIMNumber:I

    invoke-static {}, Lcom/android/internal/telephony/PhoneFactory;->getDefaultPhone()Lcom/android/internal/telephony/Phone;

    move-result-object v1

    check-cast v1, Lcom/android/internal/telephony/gemini/GeminiPhone;

    iget v4, p0, Lcom/android/simmelock/SPAddLockSetting;->intSIMNumber:I

    if-nez v4, :cond_4

    invoke-virtual {v1, v7}, Lcom/android/internal/telephony/gemini/GeminiPhone;->getIccFileHandlerGemini(I)Lcom/android/internal/telephony/IccFileHandler;

    move-result-object v0

    iget-object v4, p0, Lcom/android/simmelock/SPAddLockSetting;->mHandler:Landroid/os/Handler;

    const/16 v5, 0x26

    invoke-virtual {v4, v5}, Landroid/os/Handler;->obtainMessage(I)Landroid/os/Message;

    move-result-object v4

    invoke-virtual {v0, v8, v4}, Lcom/android/internal/telephony/IccFileHandler;->loadEFTransparent(ILandroid/os/Message;)V

    :goto_2
    return-void

    :cond_2
    const-string v4, "SIMMELOCK"

    const-string v5, "[Gemini] Succeed to read SIM1 MCC+MNC!"

    invoke-static {v4, v5}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    :cond_3
    const-string v4, "SIMMELOCK"

    const-string v5, "[Gemini] Succeed to read SIM2 MCC+MNC!"

    invoke-static {v4, v5}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1

    :cond_4
    invoke-virtual {v1, v6}, Lcom/android/internal/telephony/gemini/GeminiPhone;->getIccFileHandlerGemini(I)Lcom/android/internal/telephony/IccFileHandler;

    move-result-object v0

    iget-object v4, p0, Lcom/android/simmelock/SPAddLockSetting;->mHandler:Landroid/os/Handler;

    const/16 v5, 0x28

    invoke-virtual {v4, v5}, Landroid/os/Handler;->obtainMessage(I)Landroid/os/Message;

    move-result-object v4

    invoke-virtual {v0, v8, v4}, Lcom/android/internal/telephony/IccFileHandler;->loadEFTransparent(ILandroid/os/Message;)V

    goto :goto_2
.end method
