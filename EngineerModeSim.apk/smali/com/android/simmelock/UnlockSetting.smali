.class public Lcom/android/simmelock/UnlockSetting;
.super Landroid/app/Activity;
.source "UnlockSetting.java"

# interfaces
.implements Landroid/content/DialogInterface$OnKeyListener;


# static fields
.field static final DIALOG_PASSWORDLENGTHINCORRECT:I = 0x1

.field static final DIALOG_QUERYFAIL:I = 0x4

.field static final DIALOG_UNLOCKFAILED:I = 0x3

.field static final DIALOG_UNLOCKSUCCEED:I = 0x2

.field private static final UNLOCK_ICC_SML_COMPLETE:I = 0x78

.field private static final UNLOCK_ICC_SML_QUERYLEFTTIMES:I = 0x6e


# instance fields
.field bundle:Landroid/os/Bundle;

.field private clickFlag:Z

.field etPwd:Landroid/widget/EditText;

.field etPwdLeftChances:Landroid/widget/TextView;

.field intSIMNumber:I

.field lockCategory:I

.field private lockName:Ljava/lang/String;

.field private mHandler:Landroid/os/Handler;

.field private mPwdLeftChances:I

.field private final mReceiver:Landroid/content/BroadcastReceiver;

.field phone:Lcom/android/internal/telephony/PhoneBase;

.field private unlockPwdCorrect:Z


# direct methods
.method public constructor <init>()V
    .locals 3

    const/4 v2, 0x0

    const/4 v1, 0x0

    invoke-direct {p0}, Landroid/app/Activity;-><init>()V

    new-instance v0, Lcom/android/simmelock/UnlockSetting$8;

    invoke-direct {v0, p0}, Lcom/android/simmelock/UnlockSetting$8;-><init>(Lcom/android/simmelock/UnlockSetting;)V

    iput-object v0, p0, Lcom/android/simmelock/UnlockSetting;->mHandler:Landroid/os/Handler;

    iput-object v1, p0, Lcom/android/simmelock/UnlockSetting;->etPwd:Landroid/widget/EditText;

    iput-object v1, p0, Lcom/android/simmelock/UnlockSetting;->etPwdLeftChances:Landroid/widget/TextView;

    iput-object v1, p0, Lcom/android/simmelock/UnlockSetting;->lockName:Ljava/lang/String;

    iput-boolean v2, p0, Lcom/android/simmelock/UnlockSetting;->unlockPwdCorrect:Z

    const/4 v0, 0x5

    iput v0, p0, Lcom/android/simmelock/UnlockSetting;->mPwdLeftChances:I

    iput v2, p0, Lcom/android/simmelock/UnlockSetting;->lockCategory:I

    iput v2, p0, Lcom/android/simmelock/UnlockSetting;->intSIMNumber:I

    iput-object v1, p0, Lcom/android/simmelock/UnlockSetting;->bundle:Landroid/os/Bundle;

    iput-object v1, p0, Lcom/android/simmelock/UnlockSetting;->phone:Lcom/android/internal/telephony/PhoneBase;

    iput-boolean v2, p0, Lcom/android/simmelock/UnlockSetting;->clickFlag:Z

    new-instance v0, Lcom/android/simmelock/UnlockSetting$9;

    invoke-direct {v0, p0}, Lcom/android/simmelock/UnlockSetting$9;-><init>(Lcom/android/simmelock/UnlockSetting;)V

    iput-object v0, p0, Lcom/android/simmelock/UnlockSetting;->mReceiver:Landroid/content/BroadcastReceiver;

    return-void
.end method

.method static synthetic access$000(Lcom/android/simmelock/UnlockSetting;)Z
    .locals 1
    .param p0    # Lcom/android/simmelock/UnlockSetting;

    iget-boolean v0, p0, Lcom/android/simmelock/UnlockSetting;->clickFlag:Z

    return v0
.end method

.method static synthetic access$002(Lcom/android/simmelock/UnlockSetting;Z)Z
    .locals 0
    .param p0    # Lcom/android/simmelock/UnlockSetting;
    .param p1    # Z

    iput-boolean p1, p0, Lcom/android/simmelock/UnlockSetting;->clickFlag:Z

    return p1
.end method

.method static synthetic access$100(Lcom/android/simmelock/UnlockSetting;)Landroid/os/Handler;
    .locals 1
    .param p0    # Lcom/android/simmelock/UnlockSetting;

    iget-object v0, p0, Lcom/android/simmelock/UnlockSetting;->mHandler:Landroid/os/Handler;

    return-object v0
.end method

.method static synthetic access$200(Lcom/android/simmelock/UnlockSetting;)I
    .locals 1
    .param p0    # Lcom/android/simmelock/UnlockSetting;

    iget v0, p0, Lcom/android/simmelock/UnlockSetting;->mPwdLeftChances:I

    return v0
.end method

.method static synthetic access$202(Lcom/android/simmelock/UnlockSetting;I)I
    .locals 0
    .param p0    # Lcom/android/simmelock/UnlockSetting;
    .param p1    # I

    iput p1, p0, Lcom/android/simmelock/UnlockSetting;->mPwdLeftChances:I

    return p1
.end method

.method private getLockName(I)Ljava/lang/String;
    .locals 1
    .param p1    # I

    packed-switch p1, :pswitch_data_0

    const v0, 0x7f060001

    invoke-virtual {p0, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    :goto_0
    return-object v0

    :pswitch_0
    const v0, 0x7f060016

    invoke-virtual {p0, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    :pswitch_1
    const v0, 0x7f060017

    invoke-virtual {p0, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    :pswitch_2
    const v0, 0x7f060018

    invoke-virtual {p0, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    :pswitch_3
    const v0, 0x7f060019

    invoke-virtual {p0, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    :pswitch_4
    const v0, 0x7f06001a

    invoke-virtual {p0, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
    .end packed-switch
.end method


# virtual methods
.method public onConfigurationChanged(Landroid/content/res/Configuration;)V
    .locals 2
    .param p1    # Landroid/content/res/Configuration;

    const-string v0, "SIMMELOCK"

    const-string v1, "[UnlckSetting]onConfigurationChanged "

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    invoke-super {p0, p1}, Landroid/app/Activity;->onConfigurationChanged(Landroid/content/res/Configuration;)V

    return-void
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 9
    .param p1    # Landroid/os/Bundle;

    const/4 v8, -0x1

    invoke-super {p0, p1}, Landroid/app/Activity;->onCreate(Landroid/os/Bundle;)V

    const v6, 0x7f030008

    invoke-virtual {p0, v6}, Landroid/app/Activity;->setContentView(I)V

    new-instance v3, Landroid/content/IntentFilter;

    const-string v6, "android.intent.action.AIRPLANE_MODE"

    invoke-direct {v3, v6}, Landroid/content/IntentFilter;-><init>(Ljava/lang/String;)V

    iget-object v6, p0, Lcom/android/simmelock/UnlockSetting;->mReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {p0, v6, v3}, Landroid/content/ContextWrapper;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    invoke-virtual {p0}, Landroid/app/Activity;->getIntent()Landroid/content/Intent;

    move-result-object v6

    invoke-virtual {v6}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v6

    iput-object v6, p0, Lcom/android/simmelock/UnlockSetting;->bundle:Landroid/os/Bundle;

    iget-object v6, p0, Lcom/android/simmelock/UnlockSetting;->bundle:Landroid/os/Bundle;

    if-eqz v6, :cond_0

    iget-object v6, p0, Lcom/android/simmelock/UnlockSetting;->bundle:Landroid/os/Bundle;

    const-string v7, "LockCategory"

    invoke-virtual {v6, v7, v8}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v6

    iput v6, p0, Lcom/android/simmelock/UnlockSetting;->lockCategory:I

    :cond_0
    iget v6, p0, Lcom/android/simmelock/UnlockSetting;->lockCategory:I

    if-ne v6, v8, :cond_1

    invoke-virtual {p0}, Landroid/app/Activity;->finish()V

    :goto_0
    return-void

    :cond_1
    iget v6, p0, Lcom/android/simmelock/UnlockSetting;->lockCategory:I

    invoke-direct {p0, v6}, Lcom/android/simmelock/UnlockSetting;->getLockName(I)Ljava/lang/String;

    move-result-object v6

    iput-object v6, p0, Lcom/android/simmelock/UnlockSetting;->lockName:Ljava/lang/String;

    iget-object v6, p0, Lcom/android/simmelock/UnlockSetting;->lockName:Ljava/lang/String;

    invoke-virtual {p0, v6}, Landroid/app/Activity;->setTitle(Ljava/lang/CharSequence;)V

    const-string v6, "SIMMELOCK"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "lockName: "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    iget-object v8, p0, Lcom/android/simmelock/UnlockSetting;->lockName:Ljava/lang/String;

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, "    || lockCategory: "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    iget v8, p0, Lcom/android/simmelock/UnlockSetting;->lockCategory:I

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v6, p0, Lcom/android/simmelock/UnlockSetting;->bundle:Landroid/os/Bundle;

    const-string v7, "LOCALNAME"

    invoke-virtual {v6, v7}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p0}, Landroid/view/ContextThemeWrapper;->getResources()Landroid/content/res/Resources;

    move-result-object v6

    invoke-virtual {v6}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v2

    iget-object v6, v2, Landroid/content/res/Configuration;->locale:Ljava/util/Locale;

    iget-object v7, v2, Landroid/content/res/Configuration;->locale:Ljava/util/Locale;

    invoke-virtual {v6, v7}, Ljava/util/Locale;->getDisplayName(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v5

    const-string v6, "SIMMELOCK"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "localName: "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, "    || getLocalClassName: "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    if-eqz v4, :cond_2

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-nez v6, :cond_2

    invoke-virtual {p0}, Landroid/app/Activity;->finish()V

    goto :goto_0

    :cond_2
    const v6, 0x7f07003f

    invoke-virtual {p0, v6}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v6

    check-cast v6, Landroid/widget/TextView;

    iput-object v6, p0, Lcom/android/simmelock/UnlockSetting;->etPwdLeftChances:Landroid/widget/TextView;

    iget-object v6, p0, Lcom/android/simmelock/UnlockSetting;->etPwdLeftChances:Landroid/widget/TextView;

    if-nez v6, :cond_3

    const-string v6, "X"

    const-string v7, "clocwork worked..."

    invoke-static {v6, v7}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    :cond_3
    const v6, 0x7f07003d

    invoke-virtual {p0, v6}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v6

    check-cast v6, Landroid/widget/EditText;

    iput-object v6, p0, Lcom/android/simmelock/UnlockSetting;->etPwd:Landroid/widget/EditText;

    iget-object v6, p0, Lcom/android/simmelock/UnlockSetting;->etPwd:Landroid/widget/EditText;

    if-nez v6, :cond_4

    const-string v6, "X"

    const-string v7, "clocwork worked..."

    invoke-static {v6, v7}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    :cond_4
    iget-object v6, p0, Lcom/android/simmelock/UnlockSetting;->etPwd:Landroid/widget/EditText;

    const/16 v7, 0x81

    invoke-virtual {v6, v7}, Landroid/widget/TextView;->setInputType(I)V

    iget-object v6, p0, Lcom/android/simmelock/UnlockSetting;->etPwd:Landroid/widget/EditText;

    const/16 v7, 0x8

    invoke-static {v6, v7}, Lcom/android/simmelock/SMLCommonProcess;->limitEditTextPassword(Landroid/widget/EditText;I)V

    const v6, 0x7f070040

    invoke-virtual {p0, v6}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/Button;

    if-nez v1, :cond_5

    const-string v6, "X"

    const-string v7, "clocwork worked..."

    invoke-static {v6, v7}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    :cond_5
    new-instance v6, Lcom/android/simmelock/UnlockSetting$1;

    invoke-direct {v6, p0}, Lcom/android/simmelock/UnlockSetting$1;-><init>(Lcom/android/simmelock/UnlockSetting;)V

    invoke-virtual {v1, v6}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    const v6, 0x7f070041

    invoke-virtual {p0, v6}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    if-nez v0, :cond_6

    const-string v6, "X"

    const-string v7, "clocwork worked..."

    invoke-static {v6, v7}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    :cond_6
    new-instance v6, Lcom/android/simmelock/UnlockSetting$2;

    invoke-direct {v6, p0}, Lcom/android/simmelock/UnlockSetting$2;-><init>(Lcom/android/simmelock/UnlockSetting;)V

    invoke-virtual {v0, v6}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    goto/16 :goto_0
.end method

.method protected onDestroy()V
    .locals 1

    invoke-super {p0}, Landroid/app/Activity;->onDestroy()V

    iget-object v0, p0, Lcom/android/simmelock/UnlockSetting;->mReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {p0, v0}, Landroid/content/ContextWrapper;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    return-void
.end method

.method public onKey(Landroid/content/DialogInterface;ILandroid/view/KeyEvent;)Z
    .locals 2
    .param p1    # Landroid/content/DialogInterface;
    .param p2    # I
    .param p3    # Landroid/view/KeyEvent;

    invoke-virtual {p3}, Landroid/view/KeyEvent;->getKeyCode()I

    move-result v0

    const/4 v1, 0x4

    if-ne v0, v1, :cond_0

    invoke-interface {p1}, Landroid/content/DialogInterface;->dismiss()V

    invoke-virtual {p0}, Landroid/app/Activity;->finish()V

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected onResume()V
    .locals 9

    const/4 v2, 0x0

    const/4 v3, 0x0

    invoke-super {p0}, Landroid/app/Activity;->onResume()V

    iget-object v0, p0, Lcom/android/simmelock/UnlockSetting;->mHandler:Landroid/os/Handler;

    const/16 v1, 0x6e

    invoke-static {v0, v1}, Landroid/os/Message;->obtain(Landroid/os/Handler;I)Landroid/os/Message;

    move-result-object v7

    iget-object v0, p0, Lcom/android/simmelock/UnlockSetting;->bundle:Landroid/os/Bundle;

    const-string v1, "SIMNo"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lcom/android/simmelock/UnlockSetting;->intSIMNumber:I

    const-string v0, "SIMMELOCK"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "intSIMNumber: "

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v4, p0, Lcom/android/simmelock/UnlockSetting;->intSIMNumber:I

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    invoke-static {}, Lcom/android/internal/telephony/PhoneFactory;->getDefaultPhone()Lcom/android/internal/telephony/Phone;

    move-result-object v8

    check-cast v8, Lcom/android/internal/telephony/gemini/GeminiPhone;

    iget v0, p0, Lcom/android/simmelock/UnlockSetting;->intSIMNumber:I

    if-nez v0, :cond_0

    invoke-virtual {v8, v2}, Lcom/android/internal/telephony/gemini/GeminiPhone;->getIccCardGemini(I)Lcom/android/internal/telephony/IccCard;

    move-result-object v0

    iget v1, p0, Lcom/android/simmelock/UnlockSetting;->lockCategory:I

    move-object v4, v3

    move-object v5, v3

    move-object v6, v3

    invoke-interface/range {v0 .. v7}, Lcom/android/internal/telephony/IccCard;->QueryIccNetworkLock(IILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Landroid/os/Message;)V

    :goto_0
    return-void

    :cond_0
    const/4 v0, 0x1

    invoke-virtual {v8, v0}, Lcom/android/internal/telephony/gemini/GeminiPhone;->getIccCardGemini(I)Lcom/android/internal/telephony/IccCard;

    move-result-object v0

    iget v1, p0, Lcom/android/simmelock/UnlockSetting;->lockCategory:I

    move-object v4, v3

    move-object v5, v3

    move-object v6, v3

    invoke-interface/range {v0 .. v7}, Lcom/android/internal/telephony/IccCard;->QueryIccNetworkLock(IILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Landroid/os/Message;)V

    goto :goto_0
.end method

.method protected showAlertDialog(I)V
    .locals 8
    .param p1    # I

    const v7, 0x7f06000f

    const v6, 0x7f06000c

    const v5, 0x1080027

    const/4 v4, 0x0

    const/4 v2, 0x3

    if-eq p1, v2, :cond_0

    const/4 v2, 0x1

    if-eq p1, v2, :cond_0

    const/4 v2, 0x2

    if-eq p1, v2, :cond_0

    const/4 v2, 0x4

    if-ne p1, v2, :cond_1

    :cond_0
    new-instance v0, Landroid/app/AlertDialog$Builder;

    invoke-direct {v0, p0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    packed-switch p1, :pswitch_data_0

    :cond_1
    :goto_0
    const-string v2, "SIMMELOCK"

    const-string v3, "show null"

    invoke-static {v2, v3}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    return-void

    :pswitch_0
    const-string v2, "SIMMELOCK"

    const-string v3, "show DIALOG_UNLOCKFAILED"

    invoke-static {v2, v3}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    invoke-virtual {v0, v4}, Landroid/app/AlertDialog$Builder;->setCancelable(Z)Landroid/app/AlertDialog$Builder;

    move-result-object v2

    invoke-virtual {v2, v6}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    move-result-object v2

    invoke-virtual {v2, v5}, Landroid/app/AlertDialog$Builder;->setIcon(I)Landroid/app/AlertDialog$Builder;

    move-result-object v2

    const v3, 0x7f060012

    invoke-virtual {v2, v3}, Landroid/app/AlertDialog$Builder;->setMessage(I)Landroid/app/AlertDialog$Builder;

    move-result-object v2

    invoke-virtual {v2, p0}, Landroid/app/AlertDialog$Builder;->setOnKeyListener(Landroid/content/DialogInterface$OnKeyListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v2

    new-instance v3, Lcom/android/simmelock/UnlockSetting$3;

    invoke-direct {v3, p0}, Lcom/android/simmelock/UnlockSetting$3;-><init>(Lcom/android/simmelock/UnlockSetting;)V

    invoke-virtual {v2, v7, v3}, Landroid/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v2

    invoke-virtual {v2}, Landroid/app/AlertDialog$Builder;->show()Landroid/app/AlertDialog;

    goto :goto_0

    :pswitch_1
    const-string v2, "SIMMELOCK"

    const-string v3, "show DIALOG_PASSWORDLENGTHINCORRECT"

    invoke-static {v2, v3}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    invoke-virtual {v0, v4}, Landroid/app/AlertDialog$Builder;->setCancelable(Z)Landroid/app/AlertDialog$Builder;

    move-result-object v2

    invoke-virtual {v2, v6}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    move-result-object v2

    invoke-virtual {v2, v5}, Landroid/app/AlertDialog$Builder;->setIcon(I)Landroid/app/AlertDialog$Builder;

    move-result-object v2

    const v3, 0x7f060020

    invoke-virtual {v2, v3}, Landroid/app/AlertDialog$Builder;->setMessage(I)Landroid/app/AlertDialog$Builder;

    move-result-object v2

    invoke-virtual {v2, p0}, Landroid/app/AlertDialog$Builder;->setOnKeyListener(Landroid/content/DialogInterface$OnKeyListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v2

    new-instance v3, Lcom/android/simmelock/UnlockSetting$4;

    invoke-direct {v3, p0}, Lcom/android/simmelock/UnlockSetting$4;-><init>(Lcom/android/simmelock/UnlockSetting;)V

    invoke-virtual {v2, v7, v3}, Landroid/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v2

    invoke-virtual {v2}, Landroid/app/AlertDialog$Builder;->show()Landroid/app/AlertDialog;

    goto :goto_0

    :pswitch_2
    const-string v2, "SIMMELOCK"

    const-string v3, "show DIALOG_UNLOCKSUCCEED"

    invoke-static {v2, v3}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    invoke-virtual {v0, v4}, Landroid/app/AlertDialog$Builder;->setCancelable(Z)Landroid/app/AlertDialog$Builder;

    move-result-object v2

    invoke-virtual {v2, v6}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    move-result-object v2

    invoke-virtual {v2, v5}, Landroid/app/AlertDialog$Builder;->setIcon(I)Landroid/app/AlertDialog$Builder;

    move-result-object v2

    const v3, 0x7f060013

    invoke-virtual {v2, v3}, Landroid/app/AlertDialog$Builder;->setMessage(I)Landroid/app/AlertDialog$Builder;

    move-result-object v2

    invoke-virtual {v2, p0}, Landroid/app/AlertDialog$Builder;->setOnKeyListener(Landroid/content/DialogInterface$OnKeyListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v2

    new-instance v3, Lcom/android/simmelock/UnlockSetting$5;

    invoke-direct {v3, p0}, Lcom/android/simmelock/UnlockSetting$5;-><init>(Lcom/android/simmelock/UnlockSetting;)V

    invoke-virtual {v2, v7, v3}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v2

    invoke-virtual {v2}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v1

    new-instance v2, Lcom/android/simmelock/UnlockSetting$6;

    invoke-direct {v2, p0}, Lcom/android/simmelock/UnlockSetting$6;-><init>(Lcom/android/simmelock/UnlockSetting;)V

    invoke-virtual {v1, v2}, Landroid/app/Dialog;->setOnDismissListener(Landroid/content/DialogInterface$OnDismissListener;)V

    invoke-virtual {v1}, Landroid/app/Dialog;->show()V

    goto/16 :goto_0

    :pswitch_3
    const-string v2, "SIMMELOCK"

    const-string v3, "show DIALOG_QUERYFAIL"

    invoke-static {v2, v3}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    invoke-virtual {v0, v4}, Landroid/app/AlertDialog$Builder;->setCancelable(Z)Landroid/app/AlertDialog$Builder;

    move-result-object v2

    invoke-virtual {v2, v6}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    move-result-object v2

    invoke-virtual {v2, v5}, Landroid/app/AlertDialog$Builder;->setIcon(I)Landroid/app/AlertDialog$Builder;

    move-result-object v2

    const v3, 0x7f060021

    invoke-virtual {v2, v3}, Landroid/app/AlertDialog$Builder;->setMessage(I)Landroid/app/AlertDialog$Builder;

    move-result-object v2

    invoke-virtual {v2, p0}, Landroid/app/AlertDialog$Builder;->setOnKeyListener(Landroid/content/DialogInterface$OnKeyListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v2

    new-instance v3, Lcom/android/simmelock/UnlockSetting$7;

    invoke-direct {v3, p0}, Lcom/android/simmelock/UnlockSetting$7;-><init>(Lcom/android/simmelock/UnlockSetting;)V

    invoke-virtual {v2, v7, v3}, Landroid/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v2

    invoke-virtual {v2}, Landroid/app/AlertDialog$Builder;->show()Landroid/app/AlertDialog;

    goto/16 :goto_0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_2
        :pswitch_0
        :pswitch_3
    .end packed-switch
.end method
