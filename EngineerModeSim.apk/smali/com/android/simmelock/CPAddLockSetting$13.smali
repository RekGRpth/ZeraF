.class Lcom/android/simmelock/CPAddLockSetting$13;
.super Landroid/os/Handler;
.source "CPAddLockSetting.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/simmelock/CPAddLockSetting;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/simmelock/CPAddLockSetting;


# direct methods
.method constructor <init>(Lcom/android/simmelock/CPAddLockSetting;)V
    .locals 0

    iput-object p1, p0, Lcom/android/simmelock/CPAddLockSetting$13;->this$0:Lcom/android/simmelock/CPAddLockSetting;

    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 12
    .param p1    # Landroid/os/Message;

    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v0, Landroid/os/AsyncResult;

    iget v8, p1, Landroid/os/Message;->what:I

    sparse-switch v8, :sswitch_data_0

    :goto_0
    return-void

    :sswitch_0
    iget-object v8, v0, Landroid/os/AsyncResult;->exception:Ljava/lang/Throwable;

    if-eqz v8, :cond_0

    iget-object v8, p0, Lcom/android/simmelock/CPAddLockSetting$13;->this$0:Lcom/android/simmelock/CPAddLockSetting;

    const/4 v9, 0x2

    invoke-virtual {v8, v9}, Landroid/app/Activity;->showDialog(I)V

    goto :goto_0

    :cond_0
    iget-object v8, p0, Lcom/android/simmelock/CPAddLockSetting$13;->this$0:Lcom/android/simmelock/CPAddLockSetting;

    const/4 v9, 0x3

    invoke-virtual {v8, v9}, Landroid/app/Activity;->showDialog(I)V

    goto :goto_0

    :sswitch_1
    iget-object v8, v0, Landroid/os/AsyncResult;->exception:Ljava/lang/Throwable;

    if-eqz v8, :cond_1

    const-string v8, "SIMME Lock"

    const-string v9, "fail to get SIM GID1"

    invoke-static {v8, v9}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v8, p0, Lcom/android/simmelock/CPAddLockSetting$13;->this$0:Lcom/android/simmelock/CPAddLockSetting;

    const/16 v9, 0xff

    iput v9, v8, Lcom/android/simmelock/CPAddLockSetting;->intSIMGID1:I

    goto :goto_0

    :cond_1
    iget-object v8, v0, Landroid/os/AsyncResult;->result:Ljava/lang/Object;

    check-cast v8, [B

    move-object v1, v8

    check-cast v1, [B

    invoke-static {v1}, Lcom/android/internal/telephony/IccUtils;->bytesToHexString([B)Ljava/lang/String;

    move-result-object v6

    const/4 v8, 0x0

    aget-byte v8, v1, v8

    and-int/lit16 v8, v8, 0xff

    const/16 v9, 0xff

    if-ne v8, v9, :cond_2

    const-string v8, "SIMME Lock"

    const-string v9, "SIM GID1 not initialized"

    invoke-static {v8, v9}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v8, p0, Lcom/android/simmelock/CPAddLockSetting$13;->this$0:Lcom/android/simmelock/CPAddLockSetting;

    const/16 v9, 0xff

    iput v9, v8, Lcom/android/simmelock/CPAddLockSetting;->intSIMGID1:I

    goto :goto_0

    :cond_2
    iget-object v8, p0, Lcom/android/simmelock/CPAddLockSetting$13;->this$0:Lcom/android/simmelock/CPAddLockSetting;

    const/4 v9, 0x1

    iput v9, v8, Lcom/android/simmelock/CPAddLockSetting;->intSIMGID1:I

    iget-object v8, p0, Lcom/android/simmelock/CPAddLockSetting$13;->this$0:Lcom/android/simmelock/CPAddLockSetting;

    invoke-static {v1}, Lcom/android/internal/telephony/IccUtils;->bytesToHexString([B)Ljava/lang/String;

    move-result-object v9

    const/4 v10, 0x0

    const/4 v11, 0x2

    invoke-virtual {v9, v10, v11}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v9

    invoke-static {v9}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v9

    invoke-static {v9}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v9

    iput-object v9, v8, Lcom/android/simmelock/CPAddLockSetting;->msSIMGID1:Ljava/lang/String;

    goto :goto_0

    :sswitch_2
    iget-object v8, v0, Landroid/os/AsyncResult;->exception:Ljava/lang/Throwable;

    if-eqz v8, :cond_3

    const-string v8, "SIMME Lock"

    const-string v9, "fail to get SIM1 GID1"

    invoke-static {v8, v9}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v8, p0, Lcom/android/simmelock/CPAddLockSetting$13;->this$0:Lcom/android/simmelock/CPAddLockSetting;

    const/16 v9, 0xff

    iput v9, v8, Lcom/android/simmelock/CPAddLockSetting;->intSIMGID1:I

    goto :goto_0

    :cond_3
    const-string v8, "SIMME Lock"

    const-string v9, "succeed to get SIM1 GID1"

    invoke-static {v8, v9}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v8, v0, Landroid/os/AsyncResult;->result:Ljava/lang/Object;

    check-cast v8, [B

    move-object v1, v8

    check-cast v1, [B

    const-string v8, "SIMME Lock"

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "SIM1 GID1 :"

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v8, v9}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    invoke-static {v1}, Lcom/android/internal/telephony/IccUtils;->bytesToHexString([B)Ljava/lang/String;

    move-result-object v2

    const/4 v8, 0x0

    aget-byte v8, v1, v8

    and-int/lit16 v8, v8, 0xff

    const/16 v9, 0xff

    if-ne v8, v9, :cond_4

    const-string v8, "SIMME Lock"

    const-string v9, "SIM1 GID1 not initialized"

    invoke-static {v8, v9}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v8, p0, Lcom/android/simmelock/CPAddLockSetting$13;->this$0:Lcom/android/simmelock/CPAddLockSetting;

    const/16 v9, 0xff

    iput v9, v8, Lcom/android/simmelock/CPAddLockSetting;->intSIMGID1:I

    goto/16 :goto_0

    :cond_4
    iget-object v8, p0, Lcom/android/simmelock/CPAddLockSetting$13;->this$0:Lcom/android/simmelock/CPAddLockSetting;

    const/4 v9, 0x1

    iput v9, v8, Lcom/android/simmelock/CPAddLockSetting;->intSIMGID1:I

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v8

    const/4 v9, 0x2

    if-lt v8, v9, :cond_5

    iget-object v8, p0, Lcom/android/simmelock/CPAddLockSetting$13;->this$0:Lcom/android/simmelock/CPAddLockSetting;

    invoke-static {v1}, Lcom/android/internal/telephony/IccUtils;->bytesToHexString([B)Ljava/lang/String;

    move-result-object v9

    const/4 v10, 0x0

    const/4 v11, 0x2

    invoke-virtual {v9, v10, v11}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v9

    invoke-static {v9}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v9

    invoke-static {v9}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v9

    iput-object v9, v8, Lcom/android/simmelock/CPAddLockSetting;->msSIM1GID1:Ljava/lang/String;

    :goto_1
    const-string v8, "SIMME Lock"

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "Normal SIM1 GID1 :"

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    iget-object v10, p0, Lcom/android/simmelock/CPAddLockSetting$13;->this$0:Lcom/android/simmelock/CPAddLockSetting;

    iget-object v10, v10, Lcom/android/simmelock/CPAddLockSetting;->msSIM1GID1:Ljava/lang/String;

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v8, v9}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0

    :cond_5
    iget-object v8, p0, Lcom/android/simmelock/CPAddLockSetting$13;->this$0:Lcom/android/simmelock/CPAddLockSetting;

    invoke-static {v1}, Lcom/android/internal/telephony/IccUtils;->bytesToHexString([B)Ljava/lang/String;

    move-result-object v9

    invoke-static {v9}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v9

    invoke-static {v9}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v9

    iput-object v9, v8, Lcom/android/simmelock/CPAddLockSetting;->msSIM1GID1:Ljava/lang/String;

    goto :goto_1

    :sswitch_3
    iget-object v8, v0, Landroid/os/AsyncResult;->exception:Ljava/lang/Throwable;

    if-eqz v8, :cond_6

    const-string v8, "SIMME Lock"

    const-string v9, "fail to get SIM2 GID1"

    invoke-static {v8, v9}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v8, p0, Lcom/android/simmelock/CPAddLockSetting$13;->this$0:Lcom/android/simmelock/CPAddLockSetting;

    const/16 v9, 0xff

    iput v9, v8, Lcom/android/simmelock/CPAddLockSetting;->intSIMGID1:I

    goto/16 :goto_0

    :cond_6
    iget-object v8, v0, Landroid/os/AsyncResult;->result:Ljava/lang/Object;

    check-cast v8, [B

    move-object v1, v8

    check-cast v1, [B

    invoke-static {v1}, Lcom/android/internal/telephony/IccUtils;->bytesToHexString([B)Ljava/lang/String;

    move-result-object v4

    const/4 v8, 0x0

    aget-byte v8, v1, v8

    and-int/lit16 v8, v8, 0xff

    const/16 v9, 0xff

    if-ne v8, v9, :cond_7

    const-string v8, "SIMME Lock"

    const-string v9, "SIM2 GID1 not initialized"

    invoke-static {v8, v9}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v8, p0, Lcom/android/simmelock/CPAddLockSetting$13;->this$0:Lcom/android/simmelock/CPAddLockSetting;

    const/16 v9, 0xff

    iput v9, v8, Lcom/android/simmelock/CPAddLockSetting;->intSIMGID1:I

    goto/16 :goto_0

    :cond_7
    iget-object v8, p0, Lcom/android/simmelock/CPAddLockSetting$13;->this$0:Lcom/android/simmelock/CPAddLockSetting;

    const/4 v9, 0x1

    iput v9, v8, Lcom/android/simmelock/CPAddLockSetting;->intSIMGID1:I

    invoke-virtual {v4}, Ljava/lang/String;->length()I

    move-result v8

    const/4 v9, 0x2

    if-lt v8, v9, :cond_8

    iget-object v8, p0, Lcom/android/simmelock/CPAddLockSetting$13;->this$0:Lcom/android/simmelock/CPAddLockSetting;

    invoke-static {v1}, Lcom/android/internal/telephony/IccUtils;->bytesToHexString([B)Ljava/lang/String;

    move-result-object v9

    const/4 v10, 0x0

    const/4 v11, 0x2

    invoke-virtual {v9, v10, v11}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v9

    invoke-static {v9}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v9

    invoke-static {v9}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v9

    iput-object v9, v8, Lcom/android/simmelock/CPAddLockSetting;->msSIM2GID1:Ljava/lang/String;

    goto/16 :goto_0

    :cond_8
    iget-object v8, p0, Lcom/android/simmelock/CPAddLockSetting$13;->this$0:Lcom/android/simmelock/CPAddLockSetting;

    invoke-static {v1}, Lcom/android/internal/telephony/IccUtils;->bytesToHexString([B)Ljava/lang/String;

    move-result-object v9

    invoke-static {v9}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v9

    invoke-static {v9}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v9

    iput-object v9, v8, Lcom/android/simmelock/CPAddLockSetting;->msSIM2GID1:Ljava/lang/String;

    const-string v8, "SIMME Lock"

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "Normal SIM2 GID1 :"

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    iget-object v10, p0, Lcom/android/simmelock/CPAddLockSetting$13;->this$0:Lcom/android/simmelock/CPAddLockSetting;

    iget-object v10, v10, Lcom/android/simmelock/CPAddLockSetting;->msSIM2GID1:Ljava/lang/String;

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v8, v9}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0

    :sswitch_4
    iget-object v8, v0, Landroid/os/AsyncResult;->exception:Ljava/lang/Throwable;

    if-eqz v8, :cond_9

    const-string v8, "SIMME Lock"

    const-string v9, "fail to get SIM GID2"

    invoke-static {v8, v9}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v8, p0, Lcom/android/simmelock/CPAddLockSetting$13;->this$0:Lcom/android/simmelock/CPAddLockSetting;

    const/16 v9, 0xff

    iput v9, v8, Lcom/android/simmelock/CPAddLockSetting;->intSIMGID2:I

    goto/16 :goto_0

    :cond_9
    iget-object v8, v0, Landroid/os/AsyncResult;->result:Ljava/lang/Object;

    check-cast v8, [B

    move-object v1, v8

    check-cast v1, [B

    invoke-static {v1}, Lcom/android/internal/telephony/IccUtils;->bytesToHexString([B)Ljava/lang/String;

    move-result-object v7

    const/4 v8, 0x0

    aget-byte v8, v1, v8

    and-int/lit16 v8, v8, 0xff

    const/16 v9, 0xff

    if-ne v8, v9, :cond_a

    const-string v8, "SIMME Lock"

    const-string v9, "SIM GID2 not initialized"

    invoke-static {v8, v9}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v8, p0, Lcom/android/simmelock/CPAddLockSetting$13;->this$0:Lcom/android/simmelock/CPAddLockSetting;

    const/16 v9, 0xff

    iput v9, v8, Lcom/android/simmelock/CPAddLockSetting;->intSIMGID2:I

    goto/16 :goto_0

    :cond_a
    iget-object v8, p0, Lcom/android/simmelock/CPAddLockSetting$13;->this$0:Lcom/android/simmelock/CPAddLockSetting;

    const/4 v9, 0x1

    iput v9, v8, Lcom/android/simmelock/CPAddLockSetting;->intSIMGID2:I

    iget-object v8, p0, Lcom/android/simmelock/CPAddLockSetting$13;->this$0:Lcom/android/simmelock/CPAddLockSetting;

    invoke-static {v1}, Lcom/android/internal/telephony/IccUtils;->bytesToHexString([B)Ljava/lang/String;

    move-result-object v9

    const/4 v10, 0x0

    const/4 v11, 0x2

    invoke-virtual {v9, v10, v11}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v9

    invoke-static {v9}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v9

    invoke-static {v9}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v9

    iput-object v9, v8, Lcom/android/simmelock/CPAddLockSetting;->msSIMGID2:Ljava/lang/String;

    goto/16 :goto_0

    :sswitch_5
    iget-object v8, v0, Landroid/os/AsyncResult;->exception:Ljava/lang/Throwable;

    if-eqz v8, :cond_b

    const-string v8, "SIMME Lock"

    const-string v9, "fail to get SIM1 GID2"

    invoke-static {v8, v9}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v8, p0, Lcom/android/simmelock/CPAddLockSetting$13;->this$0:Lcom/android/simmelock/CPAddLockSetting;

    const/16 v9, 0xff

    iput v9, v8, Lcom/android/simmelock/CPAddLockSetting;->intSIMGID2:I

    goto/16 :goto_0

    :cond_b
    const-string v8, "SIMME Lock"

    const-string v9, "succeed to get SIM1 GID2"

    invoke-static {v8, v9}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v8, v0, Landroid/os/AsyncResult;->result:Ljava/lang/Object;

    check-cast v8, [B

    move-object v1, v8

    check-cast v1, [B

    const-string v8, "SIMME Lock"

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "SIM1 GID2 :"

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v8, v9}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    invoke-static {v1}, Lcom/android/internal/telephony/IccUtils;->bytesToHexString([B)Ljava/lang/String;

    move-result-object v3

    const/4 v8, 0x0

    aget-byte v8, v1, v8

    and-int/lit16 v8, v8, 0xff

    const/16 v9, 0xff

    if-ne v8, v9, :cond_c

    const-string v8, "SIMME Lock"

    const-string v9, "SIM1 GID2 not initialized"

    invoke-static {v8, v9}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v8, p0, Lcom/android/simmelock/CPAddLockSetting$13;->this$0:Lcom/android/simmelock/CPAddLockSetting;

    const/16 v9, 0xff

    iput v9, v8, Lcom/android/simmelock/CPAddLockSetting;->intSIMGID2:I

    goto/16 :goto_0

    :cond_c
    iget-object v8, p0, Lcom/android/simmelock/CPAddLockSetting$13;->this$0:Lcom/android/simmelock/CPAddLockSetting;

    const/4 v9, 0x1

    iput v9, v8, Lcom/android/simmelock/CPAddLockSetting;->intSIMGID2:I

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v8

    const/4 v9, 0x2

    if-lt v8, v9, :cond_d

    iget-object v8, p0, Lcom/android/simmelock/CPAddLockSetting$13;->this$0:Lcom/android/simmelock/CPAddLockSetting;

    invoke-static {v1}, Lcom/android/internal/telephony/IccUtils;->bytesToHexString([B)Ljava/lang/String;

    move-result-object v9

    const/4 v10, 0x0

    const/4 v11, 0x2

    invoke-virtual {v9, v10, v11}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v9

    invoke-static {v9}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v9

    invoke-static {v9}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v9

    iput-object v9, v8, Lcom/android/simmelock/CPAddLockSetting;->msSIM1GID2:Ljava/lang/String;

    :goto_2
    const-string v8, "SIMME Lock"

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "Normal SIM1 GID2 :"

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    iget-object v10, p0, Lcom/android/simmelock/CPAddLockSetting$13;->this$0:Lcom/android/simmelock/CPAddLockSetting;

    iget-object v10, v10, Lcom/android/simmelock/CPAddLockSetting;->msSIM1GID2:Ljava/lang/String;

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v8, v9}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0

    :cond_d
    iget-object v8, p0, Lcom/android/simmelock/CPAddLockSetting$13;->this$0:Lcom/android/simmelock/CPAddLockSetting;

    invoke-static {v1}, Lcom/android/internal/telephony/IccUtils;->bytesToHexString([B)Ljava/lang/String;

    move-result-object v9

    invoke-static {v9}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v9

    invoke-static {v9}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v9

    iput-object v9, v8, Lcom/android/simmelock/CPAddLockSetting;->msSIM1GID2:Ljava/lang/String;

    goto :goto_2

    :sswitch_6
    iget-object v8, v0, Landroid/os/AsyncResult;->exception:Ljava/lang/Throwable;

    if-eqz v8, :cond_e

    const-string v8, "SIMME Lock"

    const-string v9, "fail to get SIM2 GID2"

    invoke-static {v8, v9}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v8, p0, Lcom/android/simmelock/CPAddLockSetting$13;->this$0:Lcom/android/simmelock/CPAddLockSetting;

    const/16 v9, 0xff

    iput v9, v8, Lcom/android/simmelock/CPAddLockSetting;->intSIMGID2:I

    goto/16 :goto_0

    :cond_e
    iget-object v8, v0, Landroid/os/AsyncResult;->result:Ljava/lang/Object;

    check-cast v8, [B

    move-object v1, v8

    check-cast v1, [B

    invoke-static {v1}, Lcom/android/internal/telephony/IccUtils;->bytesToHexString([B)Ljava/lang/String;

    move-result-object v5

    const/4 v8, 0x0

    aget-byte v8, v1, v8

    and-int/lit16 v8, v8, 0xff

    const/16 v9, 0xff

    if-ne v8, v9, :cond_f

    const-string v8, "SIMME Lock"

    const-string v9, "SIM2 GID2 not initialized"

    invoke-static {v8, v9}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v8, p0, Lcom/android/simmelock/CPAddLockSetting$13;->this$0:Lcom/android/simmelock/CPAddLockSetting;

    const/16 v9, 0xff

    iput v9, v8, Lcom/android/simmelock/CPAddLockSetting;->intSIMGID2:I

    goto/16 :goto_0

    :cond_f
    iget-object v8, p0, Lcom/android/simmelock/CPAddLockSetting$13;->this$0:Lcom/android/simmelock/CPAddLockSetting;

    const/4 v9, 0x1

    iput v9, v8, Lcom/android/simmelock/CPAddLockSetting;->intSIMGID2:I

    invoke-virtual {v5}, Ljava/lang/String;->length()I

    move-result v8

    const/4 v9, 0x2

    if-lt v8, v9, :cond_10

    iget-object v8, p0, Lcom/android/simmelock/CPAddLockSetting$13;->this$0:Lcom/android/simmelock/CPAddLockSetting;

    invoke-static {v1}, Lcom/android/internal/telephony/IccUtils;->bytesToHexString([B)Ljava/lang/String;

    move-result-object v9

    const/4 v10, 0x0

    const/4 v11, 0x2

    invoke-virtual {v9, v10, v11}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v9

    invoke-static {v9}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v9

    invoke-static {v9}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v9

    iput-object v9, v8, Lcom/android/simmelock/CPAddLockSetting;->msSIM2GID2:Ljava/lang/String;

    goto/16 :goto_0

    :cond_10
    iget-object v8, p0, Lcom/android/simmelock/CPAddLockSetting$13;->this$0:Lcom/android/simmelock/CPAddLockSetting;

    invoke-static {v1}, Lcom/android/internal/telephony/IccUtils;->bytesToHexString([B)Ljava/lang/String;

    move-result-object v9

    invoke-static {v9}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v9

    invoke-static {v9}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v9

    iput-object v9, v8, Lcom/android/simmelock/CPAddLockSetting;->msSIM2GID2:Ljava/lang/String;

    const-string v8, "SIMME Lock"

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "Normal SIM2 GID2 :"

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    iget-object v10, p0, Lcom/android/simmelock/CPAddLockSetting$13;->this$0:Lcom/android/simmelock/CPAddLockSetting;

    iget-object v10, v10, Lcom/android/simmelock/CPAddLockSetting;->msSIM2GID2:Ljava/lang/String;

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v8, v9}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0

    :sswitch_data_0
    .sparse-switch
        0x24 -> :sswitch_1
        0x25 -> :sswitch_4
        0x26 -> :sswitch_2
        0x27 -> :sswitch_5
        0x28 -> :sswitch_3
        0x29 -> :sswitch_6
        0x78 -> :sswitch_0
    .end sparse-switch
.end method
