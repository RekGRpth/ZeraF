.class Lcom/android/simmelock/SPAddLockSetting$3;
.super Ljava/lang/Object;
.source "SPAddLockSetting.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/android/simmelock/SPAddLockSetting;->onCreate(Landroid/os/Bundle;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/simmelock/SPAddLockSetting;


# direct methods
.method constructor <init>(Lcom/android/simmelock/SPAddLockSetting;)V
    .locals 0

    iput-object p1, p0, Lcom/android/simmelock/SPAddLockSetting$3;->this$0:Lcom/android/simmelock/SPAddLockSetting;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 10
    .param p1    # Landroid/view/View;

    const/4 v9, 0x6

    const/4 v5, 0x0

    const/4 v4, 0x1

    const/4 v6, 0x0

    const/4 v1, 0x2

    const-string v0, "SIMMELOCK"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "clickFlag: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/android/simmelock/SPAddLockSetting$3;->this$0:Lcom/android/simmelock/SPAddLockSetting;

    invoke-static {v3}, Lcom/android/simmelock/SPAddLockSetting;->access$000(Lcom/android/simmelock/SPAddLockSetting;)Z

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v0, p0, Lcom/android/simmelock/SPAddLockSetting$3;->this$0:Lcom/android/simmelock/SPAddLockSetting;

    invoke-static {v0}, Lcom/android/simmelock/SPAddLockSetting;->access$000(Lcom/android/simmelock/SPAddLockSetting;)Z

    move-result v0

    if-eqz v0, :cond_0

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/android/simmelock/SPAddLockSetting$3;->this$0:Lcom/android/simmelock/SPAddLockSetting;

    invoke-static {v0, v4}, Lcom/android/simmelock/SPAddLockSetting;->access$002(Lcom/android/simmelock/SPAddLockSetting;Z)Z

    iget-object v0, p0, Lcom/android/simmelock/SPAddLockSetting$3;->this$0:Lcom/android/simmelock/SPAddLockSetting;

    iget-boolean v0, v0, Lcom/android/simmelock/SPAddLockSetting;->mbMCCMNCReadSIM:Z

    if-nez v0, :cond_2

    iget-object v0, p0, Lcom/android/simmelock/SPAddLockSetting$3;->this$0:Lcom/android/simmelock/SPAddLockSetting;

    iget-boolean v0, v0, Lcom/android/simmelock/SPAddLockSetting;->mbMCCMNCReadSIM1:Z

    if-nez v0, :cond_2

    iget-object v0, p0, Lcom/android/simmelock/SPAddLockSetting$3;->this$0:Lcom/android/simmelock/SPAddLockSetting;

    iget-boolean v0, v0, Lcom/android/simmelock/SPAddLockSetting;->mbMCCMNCReadSIM2:Z

    if-nez v0, :cond_2

    const/4 v0, 0x5

    iget-object v2, p0, Lcom/android/simmelock/SPAddLockSetting$3;->this$0:Lcom/android/simmelock/SPAddLockSetting;

    iget-object v2, v2, Lcom/android/simmelock/SPAddLockSetting;->etMCCMNC:Landroid/widget/EditText;

    invoke-virtual {v2}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v2

    invoke-interface {v2}, Landroid/text/Editable;->length()I

    move-result v2

    if-gt v0, v2, :cond_1

    iget-object v0, p0, Lcom/android/simmelock/SPAddLockSetting$3;->this$0:Lcom/android/simmelock/SPAddLockSetting;

    iget-object v0, v0, Lcom/android/simmelock/SPAddLockSetting;->etMCCMNC:Landroid/widget/EditText;

    invoke-virtual {v0}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-interface {v0}, Landroid/text/Editable;->length()I

    move-result v0

    if-ge v9, v0, :cond_2

    :cond_1
    iget-object v0, p0, Lcom/android/simmelock/SPAddLockSetting$3;->this$0:Lcom/android/simmelock/SPAddLockSetting;

    invoke-virtual {v0, v4}, Landroid/app/Activity;->showDialog(I)V

    goto :goto_0

    :cond_2
    iget-object v0, p0, Lcom/android/simmelock/SPAddLockSetting$3;->this$0:Lcom/android/simmelock/SPAddLockSetting;

    iget-object v0, v0, Lcom/android/simmelock/SPAddLockSetting;->etGID1:Landroid/widget/EditText;

    invoke-virtual {v0}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-interface {v0}, Landroid/text/Editable;->length()I

    move-result v0

    if-ge v0, v4, :cond_3

    iget-object v0, p0, Lcom/android/simmelock/SPAddLockSetting$3;->this$0:Lcom/android/simmelock/SPAddLockSetting;

    invoke-virtual {v0, v9}, Landroid/app/Activity;->showDialog(I)V

    goto :goto_0

    :cond_3
    iget-object v0, p0, Lcom/android/simmelock/SPAddLockSetting$3;->this$0:Lcom/android/simmelock/SPAddLockSetting;

    iget-object v0, v0, Lcom/android/simmelock/SPAddLockSetting;->etGID1:Landroid/widget/EditText;

    invoke-virtual {v0}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v0

    if-ltz v0, :cond_4

    iget-object v0, p0, Lcom/android/simmelock/SPAddLockSetting$3;->this$0:Lcom/android/simmelock/SPAddLockSetting;

    iget-object v0, v0, Lcom/android/simmelock/SPAddLockSetting;->etGID1:Landroid/widget/EditText;

    invoke-virtual {v0}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v0

    const/16 v2, 0xfe

    if-gt v0, v2, :cond_4

    iget-object v0, p0, Lcom/android/simmelock/SPAddLockSetting$3;->this$0:Lcom/android/simmelock/SPAddLockSetting;

    iget v0, v0, Lcom/android/simmelock/SPAddLockSetting;->intSIMGID1:I

    const/16 v2, 0xfe

    if-le v0, v2, :cond_5

    iget-object v0, p0, Lcom/android/simmelock/SPAddLockSetting$3;->this$0:Lcom/android/simmelock/SPAddLockSetting;

    iget-boolean v0, v0, Lcom/android/simmelock/SPAddLockSetting;->mbGID1ReadSIM:Z

    if-eqz v0, :cond_5

    :cond_4
    iget-object v0, p0, Lcom/android/simmelock/SPAddLockSetting$3;->this$0:Lcom/android/simmelock/SPAddLockSetting;

    invoke-virtual {v0, v9}, Landroid/app/Activity;->showDialog(I)V

    goto/16 :goto_0

    :cond_5
    iget-object v0, p0, Lcom/android/simmelock/SPAddLockSetting$3;->this$0:Lcom/android/simmelock/SPAddLockSetting;

    iget-object v0, v0, Lcom/android/simmelock/SPAddLockSetting;->etPwd:Landroid/widget/EditText;

    invoke-virtual {v0}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-interface {v0}, Landroid/text/Editable;->length()I

    move-result v0

    const/4 v2, 0x4

    if-lt v0, v2, :cond_6

    iget-object v0, p0, Lcom/android/simmelock/SPAddLockSetting$3;->this$0:Lcom/android/simmelock/SPAddLockSetting;

    iget-object v0, v0, Lcom/android/simmelock/SPAddLockSetting;->etPwd:Landroid/widget/EditText;

    invoke-virtual {v0}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-interface {v0}, Landroid/text/Editable;->length()I

    move-result v0

    const/16 v2, 0x8

    if-le v0, v2, :cond_7

    :cond_6
    iget-object v0, p0, Lcom/android/simmelock/SPAddLockSetting$3;->this$0:Lcom/android/simmelock/SPAddLockSetting;

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Landroid/app/Activity;->showDialog(I)V

    goto/16 :goto_0

    :cond_7
    iget-object v0, p0, Lcom/android/simmelock/SPAddLockSetting$3;->this$0:Lcom/android/simmelock/SPAddLockSetting;

    iget-object v0, v0, Lcom/android/simmelock/SPAddLockSetting;->etPwd:Landroid/widget/EditText;

    invoke-virtual {v0}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    iget-object v2, p0, Lcom/android/simmelock/SPAddLockSetting$3;->this$0:Lcom/android/simmelock/SPAddLockSetting;

    iget-object v2, v2, Lcom/android/simmelock/SPAddLockSetting;->etPwdConfirm:Landroid/widget/EditText;

    invoke-virtual {v2}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_8

    iget-object v0, p0, Lcom/android/simmelock/SPAddLockSetting$3;->this$0:Lcom/android/simmelock/SPAddLockSetting;

    const/4 v1, 0x5

    invoke-virtual {v0, v1}, Landroid/app/Activity;->showDialog(I)V

    goto/16 :goto_0

    :cond_8
    iget-object v0, p0, Lcom/android/simmelock/SPAddLockSetting$3;->this$0:Lcom/android/simmelock/SPAddLockSetting;

    invoke-static {v0}, Lcom/android/simmelock/SPAddLockSetting;->access$100(Lcom/android/simmelock/SPAddLockSetting;)Landroid/os/Handler;

    move-result-object v0

    const/16 v2, 0x78

    invoke-static {v0, v2}, Landroid/os/Message;->obtain(Landroid/os/Handler;I)Landroid/os/Message;

    move-result-object v7

    invoke-static {}, Lcom/android/internal/telephony/PhoneFactory;->getDefaultPhone()Lcom/android/internal/telephony/Phone;

    move-result-object v8

    check-cast v8, Lcom/android/internal/telephony/gemini/GeminiPhone;

    iget-object v0, p0, Lcom/android/simmelock/SPAddLockSetting$3;->this$0:Lcom/android/simmelock/SPAddLockSetting;

    iget v0, v0, Lcom/android/simmelock/SPAddLockSetting;->intSIMNumber:I

    if-nez v0, :cond_c

    iget-object v0, p0, Lcom/android/simmelock/SPAddLockSetting$3;->this$0:Lcom/android/simmelock/SPAddLockSetting;

    iget-boolean v0, v0, Lcom/android/simmelock/SPAddLockSetting;->mbMCCMNCReadSIM1:Z

    if-nez v0, :cond_a

    iget-object v0, p0, Lcom/android/simmelock/SPAddLockSetting$3;->this$0:Lcom/android/simmelock/SPAddLockSetting;

    iget-boolean v0, v0, Lcom/android/simmelock/SPAddLockSetting;->mbGID1ReadSIM1:Z

    if-nez v0, :cond_9

    invoke-virtual {v8, v5}, Lcom/android/internal/telephony/gemini/GeminiPhone;->getIccCardGemini(I)Lcom/android/internal/telephony/IccCard;

    move-result-object v0

    iget-object v2, p0, Lcom/android/simmelock/SPAddLockSetting$3;->this$0:Lcom/android/simmelock/SPAddLockSetting;

    iget-object v2, v2, Lcom/android/simmelock/SPAddLockSetting;->etPwd:Landroid/widget/EditText;

    invoke-virtual {v2}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v3

    iget-object v2, p0, Lcom/android/simmelock/SPAddLockSetting$3;->this$0:Lcom/android/simmelock/SPAddLockSetting;

    iget-object v2, v2, Lcom/android/simmelock/SPAddLockSetting;->etMCCMNC:Landroid/widget/EditText;

    invoke-virtual {v2}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v4

    iget-object v2, p0, Lcom/android/simmelock/SPAddLockSetting$3;->this$0:Lcom/android/simmelock/SPAddLockSetting;

    iget-object v2, v2, Lcom/android/simmelock/SPAddLockSetting;->etGID1:Landroid/widget/EditText;

    invoke-virtual {v2}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v5

    move v2, v1

    invoke-interface/range {v0 .. v7}, Lcom/android/internal/telephony/IccCard;->setIccNetworkLockEnabled(IILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Landroid/os/Message;)V

    goto/16 :goto_0

    :cond_9
    invoke-virtual {v8, v5}, Lcom/android/internal/telephony/gemini/GeminiPhone;->getIccCardGemini(I)Lcom/android/internal/telephony/IccCard;

    move-result-object v0

    iget-object v2, p0, Lcom/android/simmelock/SPAddLockSetting$3;->this$0:Lcom/android/simmelock/SPAddLockSetting;

    iget-object v2, v2, Lcom/android/simmelock/SPAddLockSetting;->etPwd:Landroid/widget/EditText;

    invoke-virtual {v2}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v3

    iget-object v2, p0, Lcom/android/simmelock/SPAddLockSetting$3;->this$0:Lcom/android/simmelock/SPAddLockSetting;

    iget-object v2, v2, Lcom/android/simmelock/SPAddLockSetting;->etMCCMNC:Landroid/widget/EditText;

    invoke-virtual {v2}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v4

    iget-object v2, p0, Lcom/android/simmelock/SPAddLockSetting$3;->this$0:Lcom/android/simmelock/SPAddLockSetting;

    iget-object v5, v2, Lcom/android/simmelock/SPAddLockSetting;->msSIM1GID1:Ljava/lang/String;

    move v2, v1

    invoke-interface/range {v0 .. v7}, Lcom/android/internal/telephony/IccCard;->setIccNetworkLockEnabled(IILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Landroid/os/Message;)V

    goto/16 :goto_0

    :cond_a
    iget-object v0, p0, Lcom/android/simmelock/SPAddLockSetting$3;->this$0:Lcom/android/simmelock/SPAddLockSetting;

    iget-boolean v0, v0, Lcom/android/simmelock/SPAddLockSetting;->mbGID1ReadSIM1:Z

    if-nez v0, :cond_b

    invoke-virtual {v8, v5}, Lcom/android/internal/telephony/gemini/GeminiPhone;->getIccCardGemini(I)Lcom/android/internal/telephony/IccCard;

    move-result-object v0

    iget-object v2, p0, Lcom/android/simmelock/SPAddLockSetting$3;->this$0:Lcom/android/simmelock/SPAddLockSetting;

    iget-object v2, v2, Lcom/android/simmelock/SPAddLockSetting;->etPwd:Landroid/widget/EditText;

    invoke-virtual {v2}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v3

    iget-object v2, p0, Lcom/android/simmelock/SPAddLockSetting$3;->this$0:Lcom/android/simmelock/SPAddLockSetting;

    iget-object v4, v2, Lcom/android/simmelock/SPAddLockSetting;->msSIM1MCCMNC:Ljava/lang/String;

    iget-object v2, p0, Lcom/android/simmelock/SPAddLockSetting$3;->this$0:Lcom/android/simmelock/SPAddLockSetting;

    iget-object v2, v2, Lcom/android/simmelock/SPAddLockSetting;->etGID1:Landroid/widget/EditText;

    invoke-virtual {v2}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v5

    move v2, v1

    invoke-interface/range {v0 .. v7}, Lcom/android/internal/telephony/IccCard;->setIccNetworkLockEnabled(IILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Landroid/os/Message;)V

    goto/16 :goto_0

    :cond_b
    invoke-virtual {v8, v5}, Lcom/android/internal/telephony/gemini/GeminiPhone;->getIccCardGemini(I)Lcom/android/internal/telephony/IccCard;

    move-result-object v0

    iget-object v2, p0, Lcom/android/simmelock/SPAddLockSetting$3;->this$0:Lcom/android/simmelock/SPAddLockSetting;

    iget-object v2, v2, Lcom/android/simmelock/SPAddLockSetting;->etPwd:Landroid/widget/EditText;

    invoke-virtual {v2}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v3

    iget-object v2, p0, Lcom/android/simmelock/SPAddLockSetting$3;->this$0:Lcom/android/simmelock/SPAddLockSetting;

    iget-object v4, v2, Lcom/android/simmelock/SPAddLockSetting;->msSIM1MCCMNC:Ljava/lang/String;

    iget-object v2, p0, Lcom/android/simmelock/SPAddLockSetting$3;->this$0:Lcom/android/simmelock/SPAddLockSetting;

    iget-object v5, v2, Lcom/android/simmelock/SPAddLockSetting;->msSIM1GID1:Ljava/lang/String;

    move v2, v1

    invoke-interface/range {v0 .. v7}, Lcom/android/internal/telephony/IccCard;->setIccNetworkLockEnabled(IILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Landroid/os/Message;)V

    goto/16 :goto_0

    :cond_c
    iget-object v0, p0, Lcom/android/simmelock/SPAddLockSetting$3;->this$0:Lcom/android/simmelock/SPAddLockSetting;

    iget-boolean v0, v0, Lcom/android/simmelock/SPAddLockSetting;->mbMCCMNCReadSIM2:Z

    if-nez v0, :cond_e

    iget-object v0, p0, Lcom/android/simmelock/SPAddLockSetting$3;->this$0:Lcom/android/simmelock/SPAddLockSetting;

    iget-boolean v0, v0, Lcom/android/simmelock/SPAddLockSetting;->mbGID1ReadSIM2:Z

    if-nez v0, :cond_d

    invoke-virtual {v8, v4}, Lcom/android/internal/telephony/gemini/GeminiPhone;->getIccCardGemini(I)Lcom/android/internal/telephony/IccCard;

    move-result-object v0

    iget-object v2, p0, Lcom/android/simmelock/SPAddLockSetting$3;->this$0:Lcom/android/simmelock/SPAddLockSetting;

    iget-object v2, v2, Lcom/android/simmelock/SPAddLockSetting;->etPwd:Landroid/widget/EditText;

    invoke-virtual {v2}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v3

    iget-object v2, p0, Lcom/android/simmelock/SPAddLockSetting$3;->this$0:Lcom/android/simmelock/SPAddLockSetting;

    iget-object v2, v2, Lcom/android/simmelock/SPAddLockSetting;->etMCCMNC:Landroid/widget/EditText;

    invoke-virtual {v2}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v4

    iget-object v2, p0, Lcom/android/simmelock/SPAddLockSetting$3;->this$0:Lcom/android/simmelock/SPAddLockSetting;

    iget-object v2, v2, Lcom/android/simmelock/SPAddLockSetting;->etGID1:Landroid/widget/EditText;

    invoke-virtual {v2}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v5

    move v2, v1

    invoke-interface/range {v0 .. v7}, Lcom/android/internal/telephony/IccCard;->setIccNetworkLockEnabled(IILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Landroid/os/Message;)V

    goto/16 :goto_0

    :cond_d
    invoke-virtual {v8, v4}, Lcom/android/internal/telephony/gemini/GeminiPhone;->getIccCardGemini(I)Lcom/android/internal/telephony/IccCard;

    move-result-object v0

    iget-object v2, p0, Lcom/android/simmelock/SPAddLockSetting$3;->this$0:Lcom/android/simmelock/SPAddLockSetting;

    iget-object v2, v2, Lcom/android/simmelock/SPAddLockSetting;->etPwd:Landroid/widget/EditText;

    invoke-virtual {v2}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v3

    iget-object v2, p0, Lcom/android/simmelock/SPAddLockSetting$3;->this$0:Lcom/android/simmelock/SPAddLockSetting;

    iget-object v2, v2, Lcom/android/simmelock/SPAddLockSetting;->etMCCMNC:Landroid/widget/EditText;

    invoke-virtual {v2}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v4

    iget-object v2, p0, Lcom/android/simmelock/SPAddLockSetting$3;->this$0:Lcom/android/simmelock/SPAddLockSetting;

    iget-object v5, v2, Lcom/android/simmelock/SPAddLockSetting;->msSIM2GID1:Ljava/lang/String;

    move v2, v1

    invoke-interface/range {v0 .. v7}, Lcom/android/internal/telephony/IccCard;->setIccNetworkLockEnabled(IILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Landroid/os/Message;)V

    goto/16 :goto_0

    :cond_e
    iget-object v0, p0, Lcom/android/simmelock/SPAddLockSetting$3;->this$0:Lcom/android/simmelock/SPAddLockSetting;

    iget-boolean v0, v0, Lcom/android/simmelock/SPAddLockSetting;->mbGID1ReadSIM2:Z

    if-nez v0, :cond_f

    invoke-virtual {v8, v4}, Lcom/android/internal/telephony/gemini/GeminiPhone;->getIccCardGemini(I)Lcom/android/internal/telephony/IccCard;

    move-result-object v0

    iget-object v2, p0, Lcom/android/simmelock/SPAddLockSetting$3;->this$0:Lcom/android/simmelock/SPAddLockSetting;

    iget-object v2, v2, Lcom/android/simmelock/SPAddLockSetting;->etPwd:Landroid/widget/EditText;

    invoke-virtual {v2}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v3

    iget-object v2, p0, Lcom/android/simmelock/SPAddLockSetting$3;->this$0:Lcom/android/simmelock/SPAddLockSetting;

    iget-object v4, v2, Lcom/android/simmelock/SPAddLockSetting;->msSIM2MCCMNC:Ljava/lang/String;

    iget-object v2, p0, Lcom/android/simmelock/SPAddLockSetting$3;->this$0:Lcom/android/simmelock/SPAddLockSetting;

    iget-object v2, v2, Lcom/android/simmelock/SPAddLockSetting;->etGID1:Landroid/widget/EditText;

    invoke-virtual {v2}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v5

    move v2, v1

    invoke-interface/range {v0 .. v7}, Lcom/android/internal/telephony/IccCard;->setIccNetworkLockEnabled(IILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Landroid/os/Message;)V

    goto/16 :goto_0

    :cond_f
    invoke-virtual {v8, v4}, Lcom/android/internal/telephony/gemini/GeminiPhone;->getIccCardGemini(I)Lcom/android/internal/telephony/IccCard;

    move-result-object v0

    iget-object v2, p0, Lcom/android/simmelock/SPAddLockSetting$3;->this$0:Lcom/android/simmelock/SPAddLockSetting;

    iget-object v2, v2, Lcom/android/simmelock/SPAddLockSetting;->etPwd:Landroid/widget/EditText;

    invoke-virtual {v2}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v3

    iget-object v2, p0, Lcom/android/simmelock/SPAddLockSetting$3;->this$0:Lcom/android/simmelock/SPAddLockSetting;

    iget-object v4, v2, Lcom/android/simmelock/SPAddLockSetting;->msSIM2MCCMNC:Ljava/lang/String;

    iget-object v2, p0, Lcom/android/simmelock/SPAddLockSetting$3;->this$0:Lcom/android/simmelock/SPAddLockSetting;

    iget-object v5, v2, Lcom/android/simmelock/SPAddLockSetting;->msSIM2GID1:Ljava/lang/String;

    move v2, v1

    invoke-interface/range {v0 .. v7}, Lcom/android/internal/telephony/IccCard;->setIccNetworkLockEnabled(IILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Landroid/os/Message;)V

    goto/16 :goto_0
.end method
