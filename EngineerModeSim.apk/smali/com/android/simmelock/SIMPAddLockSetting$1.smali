.class Lcom/android/simmelock/SIMPAddLockSetting$1;
.super Ljava/lang/Object;
.source "SIMPAddLockSetting.java"

# interfaces
.implements Landroid/widget/AdapterView$OnItemSelectedListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/android/simmelock/SIMPAddLockSetting;->onCreate(Landroid/os/Bundle;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/simmelock/SIMPAddLockSetting;


# direct methods
.method constructor <init>(Lcom/android/simmelock/SIMPAddLockSetting;)V
    .locals 0

    iput-object p1, p0, Lcom/android/simmelock/SIMPAddLockSetting$1;->this$0:Lcom/android/simmelock/SIMPAddLockSetting;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onItemSelected(Landroid/widget/AdapterView;Landroid/view/View;IJ)V
    .locals 5
    .param p2    # Landroid/view/View;
    .param p3    # I
    .param p4    # J
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/widget/AdapterView",
            "<*>;",
            "Landroid/view/View;",
            "IJ)V"
        }
    .end annotation

    const/4 v4, 0x1

    const/4 v3, 0x0

    iget-object v0, p0, Lcom/android/simmelock/SIMPAddLockSetting$1;->this$0:Lcom/android/simmelock/SIMPAddLockSetting;

    iget-object v0, v0, Lcom/android/simmelock/SIMPAddLockSetting;->s1:Landroid/widget/Spinner;

    invoke-virtual {v0}, Landroid/widget/AdapterView;->getSelectedItem()Ljava/lang/Object;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/android/simmelock/SIMPAddLockSetting$1;->this$0:Lcom/android/simmelock/SIMPAddLockSetting;

    const v2, 0x7f060014

    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/simmelock/SIMPAddLockSetting$1;->this$0:Lcom/android/simmelock/SIMPAddLockSetting;

    iget-object v0, v0, Lcom/android/simmelock/SIMPAddLockSetting;->etIMSI:Landroid/widget/EditText;

    invoke-virtual {v0, v3}, Landroid/view/View;->setVisibility(I)V

    iget-object v0, p0, Lcom/android/simmelock/SIMPAddLockSetting$1;->this$0:Lcom/android/simmelock/SIMPAddLockSetting;

    iget-object v0, v0, Lcom/android/simmelock/SIMPAddLockSetting;->etIMSI:Landroid/widget/EditText;

    invoke-virtual {v0}, Landroid/view/View;->requestFocus()Z

    iget-object v0, p0, Lcom/android/simmelock/SIMPAddLockSetting$1;->this$0:Lcom/android/simmelock/SIMPAddLockSetting;

    iget-object v0, v0, Lcom/android/simmelock/SIMPAddLockSetting;->etIMSI:Landroid/widget/EditText;

    const/16 v1, 0xf

    invoke-static {v0, v1}, Lcom/android/simmelock/SMLCommonProcess;->limitEditText(Landroid/widget/EditText;I)V

    iget-object v0, p0, Lcom/android/simmelock/SIMPAddLockSetting$1;->this$0:Lcom/android/simmelock/SIMPAddLockSetting;

    iput-boolean v3, v0, Lcom/android/simmelock/SIMPAddLockSetting;->mbIMSIReadSIM1:Z

    iget-object v0, p0, Lcom/android/simmelock/SIMPAddLockSetting$1;->this$0:Lcom/android/simmelock/SIMPAddLockSetting;

    iput-boolean v3, v0, Lcom/android/simmelock/SIMPAddLockSetting;->mbIMSIReadSIM2:Z

    iget-object v0, p0, Lcom/android/simmelock/SIMPAddLockSetting$1;->this$0:Lcom/android/simmelock/SIMPAddLockSetting;

    iput-boolean v3, v0, Lcom/android/simmelock/SIMPAddLockSetting;->mbIMSIReadSIM:Z

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/android/simmelock/SIMPAddLockSetting$1;->this$0:Lcom/android/simmelock/SIMPAddLockSetting;

    iput-boolean v4, v0, Lcom/android/simmelock/SIMPAddLockSetting;->mbIMSIReadSIM1:Z

    iget-object v0, p0, Lcom/android/simmelock/SIMPAddLockSetting$1;->this$0:Lcom/android/simmelock/SIMPAddLockSetting;

    iput-boolean v4, v0, Lcom/android/simmelock/SIMPAddLockSetting;->mbIMSIReadSIM2:Z

    iget-object v0, p0, Lcom/android/simmelock/SIMPAddLockSetting$1;->this$0:Lcom/android/simmelock/SIMPAddLockSetting;

    iput-boolean v4, v0, Lcom/android/simmelock/SIMPAddLockSetting;->mbIMSIReadSIM:Z

    iget-object v0, p0, Lcom/android/simmelock/SIMPAddLockSetting$1;->this$0:Lcom/android/simmelock/SIMPAddLockSetting;

    iget-object v0, v0, Lcom/android/simmelock/SIMPAddLockSetting;->etIMSI:Landroid/widget/EditText;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    goto :goto_0
.end method

.method public onNothingSelected(Landroid/widget/AdapterView;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/widget/AdapterView",
            "<*>;)V"
        }
    .end annotation

    return-void
.end method
