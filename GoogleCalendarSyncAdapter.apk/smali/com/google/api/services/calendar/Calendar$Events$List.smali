.class public Lcom/google/api/services/calendar/Calendar$Events$List;
.super Lcom/google/api/services/calendar/CalendarRequest;
.source "Calendar.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/api/services/calendar/Calendar$Events;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1
    name = "List"
.end annotation


# instance fields
.field private calendarId:Ljava/lang/String;
    .annotation runtime Lcom/google/api/client/util/Key;
    .end annotation
.end field

.field private maxAttendees:Ljava/lang/Integer;
    .annotation runtime Lcom/google/api/client/util/Key;
    .end annotation
.end field

.field private maxResults:Ljava/lang/Integer;
    .annotation runtime Lcom/google/api/client/util/Key;
    .end annotation
.end field

.field private orderBy:Ljava/lang/String;
    .annotation runtime Lcom/google/api/client/util/Key;
    .end annotation
.end field

.field final synthetic this$1:Lcom/google/api/services/calendar/Calendar$Events;

.field private timeMax:Lcom/google/api/client/util/DateTime;
    .annotation runtime Lcom/google/api/client/util/Key;
    .end annotation
.end field

.field private timeMin:Lcom/google/api/client/util/DateTime;
    .annotation runtime Lcom/google/api/client/util/Key;
    .end annotation
.end field

.field private updatedMin:Lcom/google/api/client/util/DateTime;
    .annotation runtime Lcom/google/api/client/util/Key;
    .end annotation
.end field


# direct methods
.method constructor <init>(Lcom/google/api/services/calendar/Calendar$Events;Ljava/lang/String;)V
    .locals 4

    iput-object p1, p0, Lcom/google/api/services/calendar/Calendar$Events$List;->this$1:Lcom/google/api/services/calendar/Calendar$Events;

    iget-object v0, p1, Lcom/google/api/services/calendar/Calendar$Events;->this$0:Lcom/google/api/services/calendar/Calendar;

    sget-object v1, Lcom/google/api/client/http/HttpMethod;->GET:Lcom/google/api/client/http/HttpMethod;

    const-string v2, "calendars/{calendarId}/events"

    const/4 v3, 0x0

    invoke-direct {p0, v0, v1, v2, v3}, Lcom/google/api/services/calendar/CalendarRequest;-><init>(Lcom/google/api/client/http/json/JsonHttpClient;Lcom/google/api/client/http/HttpMethod;Ljava/lang/String;Ljava/lang/Object;)V

    const-string v0, "Required parameter calendarId must be specified."

    invoke-static {p2, v0}, Lcom/google/common/base/Preconditions;->checkNotNull(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iput-object v0, p0, Lcom/google/api/services/calendar/Calendar$Events$List;->calendarId:Ljava/lang/String;

    return-void
.end method


# virtual methods
.method public getUpdatedMin()Lcom/google/api/client/util/DateTime;
    .locals 1

    iget-object v0, p0, Lcom/google/api/services/calendar/Calendar$Events$List;->updatedMin:Lcom/google/api/client/util/DateTime;

    return-object v0
.end method

.method public setMaxAttendees(Ljava/lang/Integer;)Lcom/google/api/services/calendar/Calendar$Events$List;
    .locals 0

    iput-object p1, p0, Lcom/google/api/services/calendar/Calendar$Events$List;->maxAttendees:Ljava/lang/Integer;

    return-object p0
.end method

.method public setMaxResults(Ljava/lang/Integer;)Lcom/google/api/services/calendar/Calendar$Events$List;
    .locals 0

    iput-object p1, p0, Lcom/google/api/services/calendar/Calendar$Events$List;->maxResults:Ljava/lang/Integer;

    return-object p0
.end method

.method public setOrderBy(Ljava/lang/String;)Lcom/google/api/services/calendar/Calendar$Events$List;
    .locals 0

    iput-object p1, p0, Lcom/google/api/services/calendar/Calendar$Events$List;->orderBy:Ljava/lang/String;

    return-object p0
.end method

.method public setTimeMax(Lcom/google/api/client/util/DateTime;)Lcom/google/api/services/calendar/Calendar$Events$List;
    .locals 0

    iput-object p1, p0, Lcom/google/api/services/calendar/Calendar$Events$List;->timeMax:Lcom/google/api/client/util/DateTime;

    return-object p0
.end method

.method public setTimeMin(Lcom/google/api/client/util/DateTime;)Lcom/google/api/services/calendar/Calendar$Events$List;
    .locals 0

    iput-object p1, p0, Lcom/google/api/services/calendar/Calendar$Events$List;->timeMin:Lcom/google/api/client/util/DateTime;

    return-object p0
.end method

.method public setUpdatedMin(Lcom/google/api/client/util/DateTime;)Lcom/google/api/services/calendar/Calendar$Events$List;
    .locals 0

    iput-object p1, p0, Lcom/google/api/services/calendar/Calendar$Events$List;->updatedMin:Lcom/google/api/client/util/DateTime;

    return-object p0
.end method
