.class public Lcom/google/android/syncadapters/calendar/Utils;
.super Ljava/lang/Object;
.source "Utils.java"


# static fields
.field static final JELLY_BEAN_OR_HIGHER:Z

.field public static final WHERE_ACCOUNT_AND_COLOR:Ljava/lang/String;

.field public static final WHERE_ACCOUNT_AND_COLOR_TYPE:Ljava/lang/String;

.field public static final WHERE_ACCOUNT_AND_SYNC:Ljava/lang/String;

.field public static final WHERE_ACCOUNT_AND_TYPE:Ljava/lang/String;

.field public static final WHERE_CALENDARS_ACCOUNT_AND_COLOR:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 5

    const/4 v4, 0x2

    const/4 v0, 0x1

    const/4 v1, 0x0

    new-array v2, v4, [Ljava/lang/String;

    const-string v3, "account_name=?"

    aput-object v3, v2, v1

    const-string v3, "account_type=?"

    aput-object v3, v2, v0

    invoke-static {v2}, Lcom/google/android/syncadapters/calendar/Utils;->makeWhere([Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    sput-object v2, Lcom/google/android/syncadapters/calendar/Utils;->WHERE_ACCOUNT_AND_TYPE:Ljava/lang/String;

    new-array v2, v4, [Ljava/lang/String;

    sget-object v3, Lcom/google/android/syncadapters/calendar/Utils;->WHERE_ACCOUNT_AND_TYPE:Ljava/lang/String;

    aput-object v3, v2, v1

    const-string v3, "sync_events=?"

    aput-object v3, v2, v0

    invoke-static {v2}, Lcom/google/android/syncadapters/calendar/Utils;->makeWhere([Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    sput-object v2, Lcom/google/android/syncadapters/calendar/Utils;->WHERE_ACCOUNT_AND_SYNC:Ljava/lang/String;

    new-array v2, v4, [Ljava/lang/String;

    sget-object v3, Lcom/google/android/syncadapters/calendar/Utils;->WHERE_ACCOUNT_AND_TYPE:Ljava/lang/String;

    aput-object v3, v2, v1

    const-string v3, "color_type=?"

    aput-object v3, v2, v0

    invoke-static {v2}, Lcom/google/android/syncadapters/calendar/Utils;->makeWhere([Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    sput-object v2, Lcom/google/android/syncadapters/calendar/Utils;->WHERE_ACCOUNT_AND_COLOR_TYPE:Ljava/lang/String;

    new-array v2, v4, [Ljava/lang/String;

    sget-object v3, Lcom/google/android/syncadapters/calendar/Utils;->WHERE_ACCOUNT_AND_COLOR_TYPE:Ljava/lang/String;

    aput-object v3, v2, v1

    const-string v3, "color_index=?"

    aput-object v3, v2, v0

    invoke-static {v2}, Lcom/google/android/syncadapters/calendar/Utils;->makeWhere([Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    sput-object v2, Lcom/google/android/syncadapters/calendar/Utils;->WHERE_ACCOUNT_AND_COLOR:Ljava/lang/String;

    new-array v2, v4, [Ljava/lang/String;

    sget-object v3, Lcom/google/android/syncadapters/calendar/Utils;->WHERE_ACCOUNT_AND_TYPE:Ljava/lang/String;

    aput-object v3, v2, v1

    const-string v3, "calendar_color_index=?"

    aput-object v3, v2, v0

    invoke-static {v2}, Lcom/google/android/syncadapters/calendar/Utils;->makeWhere([Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    sput-object v2, Lcom/google/android/syncadapters/calendar/Utils;->WHERE_CALENDARS_ACCOUNT_AND_COLOR:Ljava/lang/String;

    sget v2, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v3, 0x10

    if-lt v2, v3, :cond_0

    :goto_0
    sput-boolean v0, Lcom/google/android/syncadapters/calendar/Utils;->JELLY_BEAN_OR_HIGHER:Z

    return-void

    :cond_0
    move v0, v1

    goto :goto_0
.end method

.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method static createEventId(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    .locals 2
    .param p0    # Ljava/lang/String;
    .param p1    # Ljava/lang/String;

    invoke-static {p0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    :goto_0
    return-object p0

    :cond_1
    sget-boolean v0, Lcom/google/android/syncadapters/calendar/Utils;->JELLY_BEAN_OR_HIGHER:Z

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "\n"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p0

    goto :goto_0
.end method

.method public static extractEventId(Ljava/lang/String;)Ljava/lang/String;
    .locals 2
    .param p0    # Ljava/lang/String;

    if-nez p0, :cond_1

    const/4 p0, 0x0

    :cond_0
    :goto_0
    return-object p0

    :cond_1
    const-string v1, "\n"

    invoke-virtual {p0, v1}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v0

    if-ltz v0, :cond_0

    add-int/lit8 v1, v0, 0x1

    invoke-virtual {p0, v1}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object p0

    goto :goto_0
.end method

.method static getBooleanValue(Ljava/lang/Boolean;Z)Z
    .locals 0
    .param p0    # Ljava/lang/Boolean;
    .param p1    # Z

    if-nez p0, :cond_0

    :goto_0
    return p1

    :cond_0
    invoke-virtual {p0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result p1

    goto :goto_0
.end method

.method public static varargs makeWhere([Ljava/lang/String;)Ljava/lang/String;
    .locals 1
    .param p0    # [Ljava/lang/String;

    const-string v0, " AND "

    invoke-static {v0, p0}, Landroid/text/TextUtils;->join(Ljava/lang/CharSequence;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method static sanitizeRecurrence(Ljava/lang/String;)Ljava/lang/String;
    .locals 2
    .param p0    # Ljava/lang/String;

    if-nez p0, :cond_0

    const/4 v1, 0x0

    :goto_0
    return-object v1

    :cond_0
    new-instance v0, Lcom/android/calendarcommon2/EventRecurrence;

    invoke-direct {v0}, Lcom/android/calendarcommon2/EventRecurrence;-><init>()V

    invoke-virtual {v0, p0}, Lcom/android/calendarcommon2/EventRecurrence;->parse(Ljava/lang/String;)V

    invoke-virtual {v0}, Lcom/android/calendarcommon2/EventRecurrence;->toString()Ljava/lang/String;

    move-result-object v1

    goto :goto_0
.end method
