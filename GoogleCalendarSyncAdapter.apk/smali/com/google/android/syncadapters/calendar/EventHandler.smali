.class Lcom/google/android/syncadapters/calendar/EventHandler;
.super Ljava/lang/Object;
.source "EventHandler.java"

# interfaces
.implements Lcom/google/android/apiary/ItemAndEntityHandler;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/google/android/apiary/ItemAndEntityHandler",
        "<",
        "Lcom/google/api/services/calendar/model/Event;",
        ">;"
    }
.end annotation


# static fields
.field private static final ENTRY_TYPE_TO_PROVIDER_ATTENDEE:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private static final PROVIDER_TYPE_TO_ENTRY_ATTENDEE:Landroid/util/SparseArray;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/util/SparseArray",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private static UNSYNCED_EVENT_KEYS:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private mAccount:Landroid/accounts/Account;

.field private mCalendarId:Ljava/lang/String;

.field private final mClient:Lcom/google/api/services/calendar/Calendar;

.field private final mEventPlusPattern:Ljava/util/regex/Pattern;

.field private mProvider:Landroid/content/ContentProviderClient;


# direct methods
.method static constructor <clinit>()V
    .locals 6

    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    new-array v0, v5, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v2, "reminders"

    aput-object v2, v0, v1

    const-string v1, "extendedProperties"

    aput-object v1, v0, v3

    const-string v1, "ifmatch"

    aput-object v1, v0, v4

    invoke-static {v0}, Lcom/google/common/collect/Sets;->newHashSet([Ljava/lang/Object;)Ljava/util/HashSet;

    move-result-object v0

    sput-object v0, Lcom/google/android/syncadapters/calendar/EventHandler;->UNSYNCED_EVENT_KEYS:Ljava/util/Set;

    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    const-string v1, "needsAction"

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const-string v1, "accepted"

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const-string v1, "declined"

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const-string v1, "tentative"

    const/4 v2, 0x4

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sput-object v0, Lcom/google/android/syncadapters/calendar/EventHandler;->ENTRY_TYPE_TO_PROVIDER_ATTENDEE:Ljava/util/HashMap;

    new-instance v1, Landroid/util/SparseArray;

    invoke-direct {v1}, Landroid/util/SparseArray;-><init>()V

    sput-object v1, Lcom/google/android/syncadapters/calendar/EventHandler;->PROVIDER_TYPE_TO_ENTRY_ATTENDEE:Landroid/util/SparseArray;

    invoke-virtual {v0}, Ljava/util/HashMap;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Integer;

    sget-object v3, Lcom/google/android/syncadapters/calendar/EventHandler;->PROVIDER_TYPE_TO_ENTRY_ATTENDEE:Landroid/util/SparseArray;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v4

    invoke-virtual {v3, v4}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v3

    if-eqz v3, :cond_0

    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "value "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " was already encountered"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    sget-object v3, Lcom/google/android/syncadapters/calendar/EventHandler;->PROVIDER_TYPE_TO_ENTRY_ATTENDEE:Landroid/util/SparseArray;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v0

    invoke-virtual {v3, v1, v0}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    goto :goto_0

    :cond_1
    return-void
.end method

.method public constructor <init>(Lcom/google/api/services/calendar/Calendar;Landroid/accounts/Account;Landroid/content/ContentProviderClient;Landroid/content/ContentResolver;Ljava/lang/String;)V
    .locals 3
    .param p1    # Lcom/google/api/services/calendar/Calendar;
    .param p2    # Landroid/accounts/Account;
    .param p3    # Landroid/content/ContentProviderClient;
    .param p4    # Landroid/content/ContentResolver;
    .param p5    # Ljava/lang/String;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/google/android/syncadapters/calendar/EventHandler;->mClient:Lcom/google/api/services/calendar/Calendar;

    iput-object p2, p0, Lcom/google/android/syncadapters/calendar/EventHandler;->mAccount:Landroid/accounts/Account;

    iput-object p3, p0, Lcom/google/android/syncadapters/calendar/EventHandler;->mProvider:Landroid/content/ContentProviderClient;

    iput-object p5, p0, Lcom/google/android/syncadapters/calendar/EventHandler;->mCalendarId:Ljava/lang/String;

    const-string v1, "google_calendar_event_plus_pattern"

    const-string v2, "^http[s]?://plus(\\.[a-z0-9]+)*\\.google\\.com/events/"

    invoke-static {p4, v1, v2}, Lcom/google/android/gsf/Gservices;->getString(Landroid/content/ContentResolver;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x2

    invoke-static {v0, v1}, Ljava/util/regex/Pattern;->compile(Ljava/lang/String;I)Ljava/util/regex/Pattern;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/syncadapters/calendar/EventHandler;->mEventPlusPattern:Ljava/util/regex/Pattern;

    return-void
.end method

.method static synthetic access$000(Ljava/lang/Comparable;Ljava/lang/Comparable;)I
    .locals 1
    .param p0    # Ljava/lang/Comparable;
    .param p1    # Ljava/lang/Comparable;

    invoke-static {p0, p1}, Lcom/google/android/syncadapters/calendar/EventHandler;->compareObjects(Ljava/lang/Comparable;Ljava/lang/Comparable;)I

    move-result v0

    return v0
.end method

.method private addAttendeesToEntry(Landroid/content/ContentValues;Ljava/util/ArrayList;Lcom/google/api/services/calendar/model/Event;)V
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/ContentValues;",
            "Ljava/util/ArrayList",
            "<",
            "Landroid/content/Entity$NamedContentValues;",
            ">;",
            "Lcom/google/api/services/calendar/model/Event;",
            ")V"
        }
    .end annotation

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    invoke-virtual {p2}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_0
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_7

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/Entity$NamedContentValues;

    iget-object v3, v0, Landroid/content/Entity$NamedContentValues;->uri:Landroid/net/Uri;

    iget-object v4, v0, Landroid/content/Entity$NamedContentValues;->values:Landroid/content/ContentValues;

    sget-object v0, Landroid/provider/CalendarContract$Attendees;->CONTENT_URI:Landroid/net/Uri;

    invoke-virtual {v0, v3}, Landroid/net/Uri;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    new-instance v3, Lcom/google/api/services/calendar/model/EventAttendee;

    invoke-direct {v3}, Lcom/google/api/services/calendar/model/EventAttendee;-><init>()V

    const-string v0, "attendeeName"

    invoke-virtual {v4, v0}, Landroid/content/ContentValues;->getAsString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v3, v0}, Lcom/google/api/services/calendar/model/EventAttendee;->setDisplayName(Ljava/lang/String;)Lcom/google/api/services/calendar/model/EventAttendee;

    const-string v0, "attendeeEmail"

    invoke-virtual {v4, v0}, Landroid/content/ContentValues;->getAsString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    const-string v0, "attendeeIdentity"

    invoke-virtual {v4, v0}, Landroid/content/ContentValues;->getAsString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_3

    const-string v6, "gprofile:"

    invoke-virtual {v0, v6}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v6

    if-eqz v6, :cond_3

    const-string v6, "gprofile:"

    invoke-virtual {v6}, Ljava/lang/String;->length()I

    move-result v6

    invoke-virtual {v0, v6}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v0

    :goto_1
    if-eqz v0, :cond_6

    const-string v5, "id"

    invoke-virtual {v3, v5, v0}, Lcom/google/api/services/calendar/model/EventAttendee;->set(Ljava/lang/String;Ljava/lang/Object;)V

    :goto_2
    const-string v0, "attendeeStatus"

    invoke-virtual {v4, v0}, Landroid/content/ContentValues;->getAsInteger(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v5

    sget-object v0, Lcom/google/android/syncadapters/calendar/EventHandler;->PROVIDER_TYPE_TO_ENTRY_ATTENDEE:Landroid/util/SparseArray;

    invoke-virtual {v0, v5}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    if-nez v0, :cond_1

    const-string v0, "EventHandler"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "Unknown attendee status: "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v0, v5}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    const-string v0, "needsAction"

    :cond_1
    invoke-virtual {v3, v0}, Lcom/google/api/services/calendar/model/EventAttendee;->setResponseStatus(Ljava/lang/String;)Lcom/google/api/services/calendar/model/EventAttendee;

    const-string v0, "attendeeType"

    invoke-virtual {v4, v0}, Landroid/content/ContentValues;->getAsInteger(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    const/4 v4, 0x2

    if-ne v0, v4, :cond_2

    const/4 v0, 0x1

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {v3, v0}, Lcom/google/api/services/calendar/model/EventAttendee;->setOptional(Ljava/lang/Boolean;)Lcom/google/api/services/calendar/model/EventAttendee;

    :cond_2
    invoke-interface {v1, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto/16 :goto_0

    :cond_3
    if-eqz v5, :cond_4

    const-string v0, "@profile.calendar.google.com"

    invoke-virtual {v5, v0}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_4

    const/4 v0, 0x0

    const-string v6, "@profile.calendar.google.com"

    invoke-virtual {v5, v6}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v6

    invoke-virtual {v5, v0, v6}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v0

    goto :goto_1

    :cond_4
    const-string v0, "sync_data3"

    invoke-virtual {p1, v0}, Landroid/content/ContentValues;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_5

    const-string v0, "sync_data3"

    invoke-virtual {p1, v0}, Landroid/content/ContentValues;->getAsString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    goto :goto_1

    :cond_5
    const/4 v0, 0x0

    goto :goto_1

    :cond_6
    invoke-virtual {v3, v5}, Lcom/google/api/services/calendar/model/EventAttendee;->setEmail(Ljava/lang/String;)Lcom/google/api/services/calendar/model/EventAttendee;

    goto :goto_2

    :cond_7
    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v0

    if-lez v0, :cond_8

    invoke-virtual {p3, v1}, Lcom/google/api/services/calendar/model/Event;->setAttendees(Ljava/util/List;)Lcom/google/api/services/calendar/model/Event;

    :cond_8
    return-void
.end method

.method private static addDeletedProperties(Ljava/util/Map;Ljava/util/Map;)Ljava/util/Map;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;)",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    if-nez p0, :cond_1

    :cond_0
    return-object p1

    :cond_1
    if-nez p1, :cond_2

    new-instance p1, Ljava/util/HashMap;

    invoke-direct {p1}, Ljava/util/HashMap;-><init>()V

    :cond_2
    invoke-interface {p0}, Ljava/util/Map;->keySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_3
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-interface {p1, v0}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_3

    const-string v2, ""

    invoke-interface {p1, v0, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0
.end method

.method private addExtendedPropertiesToEntry(Ljava/util/ArrayList;Lcom/google/api/services/calendar/model/Event;)V
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Landroid/content/Entity$NamedContentValues;",
            ">;",
            "Lcom/google/api/services/calendar/model/Event;",
            ")V"
        }
    .end annotation

    invoke-virtual {p2}, Lcom/google/api/services/calendar/model/Event;->getExtendedProperties()Lcom/google/api/services/calendar/model/Event$ExtendedProperties;

    move-result-object v0

    if-nez v0, :cond_0

    new-instance v0, Lcom/google/api/services/calendar/model/Event$ExtendedProperties;

    invoke-direct {v0}, Lcom/google/api/services/calendar/model/Event$ExtendedProperties;-><init>()V

    new-instance v1, Ljava/util/HashMap;

    invoke-direct {v1}, Ljava/util/HashMap;-><init>()V

    invoke-virtual {v0, v1}, Lcom/google/api/services/calendar/model/Event$ExtendedProperties;->setPrivate(Ljava/util/Map;)Lcom/google/api/services/calendar/model/Event$ExtendedProperties;

    move-result-object v0

    invoke-virtual {p2, v0}, Lcom/google/api/services/calendar/model/Event;->setExtendedProperties(Lcom/google/api/services/calendar/model/Event$ExtendedProperties;)Lcom/google/api/services/calendar/model/Event;

    :cond_0
    invoke-virtual {v0}, Lcom/google/api/services/calendar/model/Event$ExtendedProperties;->getPrivate()Ljava/util/Map;

    move-result-object v1

    invoke-virtual {p1}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_1
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/Entity$NamedContentValues;

    iget-object v3, v0, Landroid/content/Entity$NamedContentValues;->uri:Landroid/net/Uri;

    iget-object v0, v0, Landroid/content/Entity$NamedContentValues;->values:Landroid/content/ContentValues;

    sget-object v4, Landroid/provider/CalendarContract$ExtendedProperties;->CONTENT_URI:Landroid/net/Uri;

    invoke-virtual {v4, v3}, Landroid/net/Uri;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_1

    const-string v3, "name"

    invoke-virtual {v0, v3}, Landroid/content/ContentValues;->getAsString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    const-string v4, "value"

    invoke-virtual {v0, v4}, Landroid/content/ContentValues;->getAsString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-interface {v1, v3, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    :cond_2
    return-void
.end method

.method private addRecurrenceToEntry(Lcom/android/calendarcommon2/ICalendar$Component;Lcom/google/api/services/calendar/model/Event;)V
    .locals 6
    .param p1    # Lcom/android/calendarcommon2/ICalendar$Component;
    .param p2    # Lcom/google/api/services/calendar/model/Event;

    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    invoke-virtual {p1}, Lcom/android/calendarcommon2/ICalendar$Component;->getPropertyNames()Ljava/util/Set;

    move-result-object v5

    invoke-interface {v5}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :cond_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_2

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    const-string v5, "RRULE"

    invoke-virtual {v5, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-nez v5, :cond_1

    const-string v5, "RDATE"

    invoke-virtual {v5, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-nez v5, :cond_1

    const-string v5, "EXRULE"

    invoke-virtual {v5, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-nez v5, :cond_1

    const-string v5, "EXDATE"

    invoke-virtual {v5, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_0

    :cond_1
    invoke-virtual {p1, v3}, Lcom/android/calendarcommon2/ICalendar$Component;->getProperties(Ljava/lang/String;)Ljava/util/List;

    move-result-object v5

    invoke-interface {v5}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/android/calendarcommon2/ICalendar$Property;

    invoke-virtual {v2}, Lcom/android/calendarcommon2/ICalendar$Property;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-interface {v4, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_2
    invoke-virtual {p2, v4}, Lcom/google/api/services/calendar/model/Event;->setRecurrence(Ljava/util/List;)Lcom/google/api/services/calendar/model/Event;

    return-void
.end method

.method private addRemindersToEntry(Ljava/util/ArrayList;Lcom/google/api/services/calendar/model/Event;)V
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Landroid/content/Entity$NamedContentValues;",
            ">;",
            "Lcom/google/api/services/calendar/model/Event;",
            ")V"
        }
    .end annotation

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    invoke-static {}, Lcom/google/common/collect/Lists;->newArrayList()Ljava/util/ArrayList;

    move-result-object v2

    invoke-virtual {p1}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_0
    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/Entity$NamedContentValues;

    iget-object v4, v0, Landroid/content/Entity$NamedContentValues;->uri:Landroid/net/Uri;

    iget-object v0, v0, Landroid/content/Entity$NamedContentValues;->values:Landroid/content/ContentValues;

    sget-object v5, Landroid/provider/CalendarContract$Reminders;->CONTENT_URI:Landroid/net/Uri;

    invoke-virtual {v5, v4}, Landroid/net/Uri;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_0

    const-string v4, "method"

    invoke-virtual {v0, v4}, Landroid/content/ContentValues;->getAsInteger(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/Integer;->intValue()I

    move-result v4

    const-string v5, "minutes"

    invoke-virtual {v0, v5}, Landroid/content/ContentValues;->getAsInteger(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v0

    new-instance v5, Lcom/google/api/services/calendar/model/EventReminder;

    invoke-direct {v5}, Lcom/google/api/services/calendar/model/EventReminder;-><init>()V

    invoke-virtual {v5, v0}, Lcom/google/api/services/calendar/model/EventReminder;->setMinutes(Ljava/lang/Integer;)Lcom/google/api/services/calendar/model/EventReminder;

    move-result-object v5

    packed-switch v4, :pswitch_data_0

    const-string v0, "popup"

    invoke-virtual {v5, v0}, Lcom/google/api/services/calendar/model/EventReminder;->setMethod(Ljava/lang/String;)Lcom/google/api/services/calendar/model/EventReminder;

    invoke-interface {v1, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :pswitch_0
    const-string v0, "popup"

    invoke-virtual {v5, v0}, Lcom/google/api/services/calendar/model/EventReminder;->setMethod(Ljava/lang/String;)Lcom/google/api/services/calendar/model/EventReminder;

    invoke-interface {v1, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :pswitch_1
    const-string v0, "email"

    invoke-virtual {v5, v0}, Lcom/google/api/services/calendar/model/EventReminder;->setMethod(Ljava/lang/String;)Lcom/google/api/services/calendar/model/EventReminder;

    invoke-interface {v1, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :pswitch_2
    const-string v0, "sms"

    invoke-virtual {v5, v0}, Lcom/google/api/services/calendar/model/EventReminder;->setMethod(Ljava/lang/String;)Lcom/google/api/services/calendar/model/EventReminder;

    invoke-interface {v1, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :pswitch_3
    new-instance v4, Landroid/content/ContentValues;

    invoke-direct {v4}, Landroid/content/ContentValues;-><init>()V

    const-string v5, "name"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "alarmReminder-"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v4, v5, v6}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    const-string v5, "value"

    invoke-virtual {v4, v5, v0}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    new-instance v0, Landroid/content/Entity$NamedContentValues;

    sget-object v5, Landroid/provider/CalendarContract$ExtendedProperties;->CONTENT_URI:Landroid/net/Uri;

    invoke-direct {v0, v5, v4}, Landroid/content/Entity$NamedContentValues;-><init>(Landroid/net/Uri;Landroid/content/ContentValues;)V

    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto/16 :goto_0

    :cond_1
    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v0

    if-lez v0, :cond_2

    new-instance v0, Lcom/google/api/services/calendar/model/Event$Reminders;

    invoke-direct {v0}, Lcom/google/api/services/calendar/model/Event$Reminders;-><init>()V

    const/4 v3, 0x0

    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v3

    invoke-virtual {v0, v3}, Lcom/google/api/services/calendar/model/Event$Reminders;->setUseDefault(Ljava/lang/Boolean;)Lcom/google/api/services/calendar/model/Event$Reminders;

    invoke-virtual {v0, v1}, Lcom/google/api/services/calendar/model/Event$Reminders;->setOverrides(Ljava/util/List;)Lcom/google/api/services/calendar/model/Event$Reminders;

    invoke-virtual {p2, v0}, Lcom/google/api/services/calendar/model/Event;->setReminders(Lcom/google/api/services/calendar/model/Event$Reminders;)Lcom/google/api/services/calendar/model/Event;

    :cond_2
    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-lez v0, :cond_3

    invoke-direct {p0, v2, p2}, Lcom/google/android/syncadapters/calendar/EventHandler;->addExtendedPropertiesToEntry(Ljava/util/ArrayList;Lcom/google/api/services/calendar/model/Event;)V

    :cond_3
    return-void

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method private static appendRecurrenceString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    .locals 2

    if-nez p0, :cond_0

    :goto_0
    return-object p1

    :cond_0
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "\n"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    goto :goto_0
.end method

.method private static compareObjects(Ljava/lang/Comparable;Ljava/lang/Comparable;)I
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T::",
            "Ljava/lang/Comparable",
            "<TT;>;>(TT;TT;)I"
        }
    .end annotation

    if-nez p0, :cond_1

    if-nez p1, :cond_0

    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_0
    const/4 v0, -0x1

    goto :goto_0

    :cond_1
    if-nez p1, :cond_2

    const/4 v0, 0x1

    goto :goto_0

    :cond_2
    invoke-interface {p0, p1}, Ljava/lang/Comparable;->compareTo(Ljava/lang/Object;)I

    move-result v0

    goto :goto_0
.end method

.method private convertValuesToPartialDiff(Landroid/content/Entity;Landroid/content/Entity;)Lcom/google/api/services/calendar/model/Event;
    .locals 26
    .param p1    # Landroid/content/Entity;
    .param p2    # Landroid/content/Entity;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    invoke-virtual/range {p1 .. p1}, Landroid/content/Entity;->getEntityValues()Landroid/content/ContentValues;

    move-result-object v23

    invoke-virtual/range {p2 .. p2}, Landroid/content/Entity;->getEntityValues()Landroid/content/ContentValues;

    move-result-object v20

    new-instance v4, Landroid/content/ContentValues;

    move-object/from16 v0, v23

    invoke-direct {v4, v0}, Landroid/content/ContentValues;-><init>(Landroid/content/ContentValues;)V

    invoke-virtual/range {v23 .. v23}, Landroid/content/ContentValues;->keySet()Ljava/util/Set;

    move-result-object v24

    invoke-interface/range {v24 .. v24}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v6

    :cond_0
    :goto_0
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v24

    if-eqz v24, :cond_3

    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Ljava/lang/String;

    move-object/from16 v0, v23

    invoke-virtual {v0, v7}, Landroid/content/ContentValues;->get(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v22

    move-object/from16 v0, v20

    invoke-virtual {v0, v7}, Landroid/content/ContentValues;->get(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v19

    if-nez v22, :cond_1

    if-eqz v19, :cond_2

    :cond_1
    if-eqz v22, :cond_0

    move-object/from16 v0, v22

    move-object/from16 v1, v19

    invoke-virtual {v0, v1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v24

    if-eqz v24, :cond_0

    :cond_2
    invoke-virtual {v4, v7}, Landroid/content/ContentValues;->remove(Ljava/lang/String;)V

    goto :goto_0

    :cond_3
    const-string v24, "allDay"

    invoke-virtual/range {v23 .. v24}, Landroid/content/ContentValues;->getAsInteger(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v24

    invoke-virtual/range {v24 .. v24}, Ljava/lang/Integer;->intValue()I

    move-result v8

    const-string v24, "allDay"

    move-object/from16 v0, v20

    move-object/from16 v1, v24

    invoke-virtual {v0, v1}, Landroid/content/ContentValues;->getAsInteger(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v24

    invoke-virtual/range {v24 .. v24}, Ljava/lang/Integer;->intValue()I

    move-result v13

    const-string v24, "dtstart"

    move-object/from16 v0, v24

    invoke-virtual {v4, v0}, Landroid/content/ContentValues;->containsKey(Ljava/lang/String;)Z

    move-result v24

    if-nez v24, :cond_4

    const-string v24, "dtend"

    move-object/from16 v0, v24

    invoke-virtual {v4, v0}, Landroid/content/ContentValues;->containsKey(Ljava/lang/String;)Z

    move-result v24

    if-eqz v24, :cond_5

    :cond_4
    const-string v24, "allDay"

    invoke-static {v8}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v25

    move-object/from16 v0, v24

    move-object/from16 v1, v25

    invoke-virtual {v4, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    :cond_5
    const-string v24, "allDay"

    move-object/from16 v0, v24

    invoke-virtual {v4, v0}, Landroid/content/ContentValues;->containsKey(Ljava/lang/String;)Z

    move-result v24

    if-eqz v24, :cond_6

    const-string v24, "dtstart"

    const-string v25, "dtstart"

    move-object/from16 v0, v23

    move-object/from16 v1, v25

    invoke-virtual {v0, v1}, Landroid/content/ContentValues;->getAsLong(Ljava/lang/String;)Ljava/lang/Long;

    move-result-object v25

    move-object/from16 v0, v24

    move-object/from16 v1, v25

    invoke-virtual {v4, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    const-string v24, "dtend"

    const-string v25, "dtend"

    move-object/from16 v0, v23

    move-object/from16 v1, v25

    invoke-virtual {v0, v1}, Landroid/content/ContentValues;->getAsLong(Ljava/lang/String;)Ljava/lang/Long;

    move-result-object v25

    move-object/from16 v0, v24

    move-object/from16 v1, v25

    invoke-virtual {v4, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    :cond_6
    new-instance v3, Landroid/content/Entity;

    invoke-direct {v3, v4}, Landroid/content/Entity;-><init>(Landroid/content/ContentValues;)V

    move-object/from16 v0, p0

    invoke-virtual {v0, v3}, Lcom/google/android/syncadapters/calendar/EventHandler;->convertEntityToEntry(Landroid/content/Entity;)Lcom/google/api/services/calendar/model/Event;

    move-result-object v5

    if-eq v8, v13, :cond_7

    if-nez v8, :cond_f

    invoke-virtual {v5}, Lcom/google/api/services/calendar/model/Event;->getStart()Lcom/google/api/services/calendar/model/EventDateTime;

    move-result-object v24

    sget-object v25, Lcom/google/api/client/util/Data;->NULL_STRING:Ljava/lang/String;

    invoke-virtual/range {v24 .. v25}, Lcom/google/api/services/calendar/model/EventDateTime;->setDate(Ljava/lang/String;)Lcom/google/api/services/calendar/model/EventDateTime;

    invoke-virtual {v5}, Lcom/google/api/services/calendar/model/Event;->getEnd()Lcom/google/api/services/calendar/model/EventDateTime;

    move-result-object v24

    sget-object v25, Lcom/google/api/client/util/Data;->NULL_STRING:Ljava/lang/String;

    invoke-virtual/range {v24 .. v25}, Lcom/google/api/services/calendar/model/EventDateTime;->setDate(Ljava/lang/String;)Lcom/google/api/services/calendar/model/EventDateTime;

    :cond_7
    :goto_1
    invoke-virtual/range {p1 .. p1}, Landroid/content/Entity;->getSubValues()Ljava/util/ArrayList;

    move-result-object v21

    invoke-virtual/range {p2 .. p2}, Landroid/content/Entity;->getSubValues()Ljava/util/ArrayList;

    move-result-object v18

    new-instance v10, Lcom/google/api/services/calendar/model/Event;

    invoke-direct {v10}, Lcom/google/api/services/calendar/model/Event;-><init>()V

    new-instance v15, Lcom/google/api/services/calendar/model/Event;

    invoke-direct {v15}, Lcom/google/api/services/calendar/model/Event;-><init>()V

    move-object/from16 v0, p0

    move-object/from16 v1, v21

    invoke-direct {v0, v1, v10}, Lcom/google/android/syncadapters/calendar/EventHandler;->addRemindersToEntry(Ljava/util/ArrayList;Lcom/google/api/services/calendar/model/Event;)V

    move-object/from16 v0, p0

    move-object/from16 v1, v18

    invoke-direct {v0, v1, v15}, Lcom/google/android/syncadapters/calendar/EventHandler;->addRemindersToEntry(Ljava/util/ArrayList;Lcom/google/api/services/calendar/model/Event;)V

    invoke-virtual {v10}, Lcom/google/api/services/calendar/model/Event;->getReminders()Lcom/google/api/services/calendar/model/Event$Reminders;

    move-result-object v12

    invoke-virtual {v15}, Lcom/google/api/services/calendar/model/Event;->getReminders()Lcom/google/api/services/calendar/model/Event$Reminders;

    move-result-object v17

    move-object/from16 v0, p0

    move-object/from16 v1, v17

    invoke-direct {v0, v12, v1}, Lcom/google/android/syncadapters/calendar/EventHandler;->sameReminders(Lcom/google/api/services/calendar/model/Event$Reminders;Lcom/google/api/services/calendar/model/Event$Reminders;)Z

    move-result v24

    if-nez v24, :cond_9

    if-eqz v17, :cond_8

    if-nez v12, :cond_8

    new-instance v24, Lcom/google/api/services/calendar/model/Event$Reminders;

    invoke-direct/range {v24 .. v24}, Lcom/google/api/services/calendar/model/Event$Reminders;-><init>()V

    const/16 v25, 0x0

    invoke-static/range {v25 .. v25}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v25

    invoke-virtual/range {v24 .. v25}, Lcom/google/api/services/calendar/model/Event$Reminders;->setUseDefault(Ljava/lang/Boolean;)Lcom/google/api/services/calendar/model/Event$Reminders;

    move-result-object v24

    new-instance v25, Ljava/util/ArrayList;

    invoke-direct/range {v25 .. v25}, Ljava/util/ArrayList;-><init>()V

    invoke-virtual/range {v24 .. v25}, Lcom/google/api/services/calendar/model/Event$Reminders;->setOverrides(Ljava/util/List;)Lcom/google/api/services/calendar/model/Event$Reminders;

    move-result-object v12

    :cond_8
    invoke-virtual {v5, v12}, Lcom/google/api/services/calendar/model/Event;->setReminders(Lcom/google/api/services/calendar/model/Event$Reminders;)Lcom/google/api/services/calendar/model/Event;

    :cond_9
    move-object/from16 v0, p0

    move-object/from16 v1, v23

    move-object/from16 v2, v21

    invoke-direct {v0, v1, v2, v10}, Lcom/google/android/syncadapters/calendar/EventHandler;->addAttendeesToEntry(Landroid/content/ContentValues;Ljava/util/ArrayList;Lcom/google/api/services/calendar/model/Event;)V

    move-object/from16 v0, p0

    move-object/from16 v1, v23

    move-object/from16 v2, v18

    invoke-direct {v0, v1, v2, v15}, Lcom/google/android/syncadapters/calendar/EventHandler;->addAttendeesToEntry(Landroid/content/ContentValues;Ljava/util/ArrayList;Lcom/google/api/services/calendar/model/Event;)V

    invoke-virtual {v10}, Lcom/google/api/services/calendar/model/Event;->getAttendees()Ljava/util/List;

    move-result-object v9

    invoke-virtual {v15}, Lcom/google/api/services/calendar/model/Event;->getAttendees()Ljava/util/List;

    move-result-object v14

    move-object/from16 v0, p0

    invoke-direct {v0, v9, v14}, Lcom/google/android/syncadapters/calendar/EventHandler;->sameAttendees(Ljava/util/List;Ljava/util/List;)Z

    move-result v24

    if-nez v24, :cond_b

    if-eqz v14, :cond_a

    if-nez v9, :cond_a

    new-instance v9, Ljava/util/ArrayList;

    invoke-direct {v9}, Ljava/util/ArrayList;-><init>()V

    :cond_a
    invoke-virtual {v5, v9}, Lcom/google/api/services/calendar/model/Event;->setAttendees(Ljava/util/List;)Lcom/google/api/services/calendar/model/Event;

    :cond_b
    move-object/from16 v0, p0

    move-object/from16 v1, v21

    invoke-direct {v0, v1, v10}, Lcom/google/android/syncadapters/calendar/EventHandler;->addExtendedPropertiesToEntry(Ljava/util/ArrayList;Lcom/google/api/services/calendar/model/Event;)V

    move-object/from16 v0, p0

    move-object/from16 v1, v18

    invoke-direct {v0, v1, v15}, Lcom/google/android/syncadapters/calendar/EventHandler;->addExtendedPropertiesToEntry(Ljava/util/ArrayList;Lcom/google/api/services/calendar/model/Event;)V

    invoke-virtual {v10}, Lcom/google/api/services/calendar/model/Event;->getExtendedProperties()Lcom/google/api/services/calendar/model/Event$ExtendedProperties;

    move-result-object v11

    invoke-virtual {v15}, Lcom/google/api/services/calendar/model/Event;->getExtendedProperties()Lcom/google/api/services/calendar/model/Event$ExtendedProperties;

    move-result-object v16

    move-object/from16 v0, p0

    move-object/from16 v1, v16

    invoke-direct {v0, v11, v1}, Lcom/google/android/syncadapters/calendar/EventHandler;->sameExtendedProperties(Lcom/google/api/services/calendar/model/Event$ExtendedProperties;Lcom/google/api/services/calendar/model/Event$ExtendedProperties;)Z

    move-result v24

    if-nez v24, :cond_e

    if-eqz v16, :cond_d

    if-nez v11, :cond_c

    new-instance v11, Lcom/google/api/services/calendar/model/Event$ExtendedProperties;

    invoke-direct {v11}, Lcom/google/api/services/calendar/model/Event$ExtendedProperties;-><init>()V

    :cond_c
    invoke-virtual/range {v16 .. v16}, Lcom/google/api/services/calendar/model/Event$ExtendedProperties;->getPrivate()Ljava/util/Map;

    move-result-object v24

    invoke-virtual {v11}, Lcom/google/api/services/calendar/model/Event$ExtendedProperties;->getPrivate()Ljava/util/Map;

    move-result-object v25

    invoke-static/range {v24 .. v25}, Lcom/google/android/syncadapters/calendar/EventHandler;->addDeletedProperties(Ljava/util/Map;Ljava/util/Map;)Ljava/util/Map;

    move-result-object v24

    move-object/from16 v0, v24

    invoke-virtual {v11, v0}, Lcom/google/api/services/calendar/model/Event$ExtendedProperties;->setPrivate(Ljava/util/Map;)Lcom/google/api/services/calendar/model/Event$ExtendedProperties;

    invoke-virtual/range {v16 .. v16}, Lcom/google/api/services/calendar/model/Event$ExtendedProperties;->getShared()Ljava/util/Map;

    move-result-object v24

    invoke-virtual {v11}, Lcom/google/api/services/calendar/model/Event$ExtendedProperties;->getShared()Ljava/util/Map;

    move-result-object v25

    invoke-static/range {v24 .. v25}, Lcom/google/android/syncadapters/calendar/EventHandler;->addDeletedProperties(Ljava/util/Map;Ljava/util/Map;)Ljava/util/Map;

    move-result-object v24

    move-object/from16 v0, v24

    invoke-virtual {v11, v0}, Lcom/google/api/services/calendar/model/Event$ExtendedProperties;->setShared(Ljava/util/Map;)Lcom/google/api/services/calendar/model/Event$ExtendedProperties;

    :cond_d
    invoke-virtual {v5, v11}, Lcom/google/api/services/calendar/model/Event;->setExtendedProperties(Lcom/google/api/services/calendar/model/Event$ExtendedProperties;)Lcom/google/api/services/calendar/model/Event;

    :cond_e
    return-object v5

    :cond_f
    invoke-virtual {v5}, Lcom/google/api/services/calendar/model/Event;->getStart()Lcom/google/api/services/calendar/model/EventDateTime;

    move-result-object v24

    sget-object v25, Lcom/google/api/client/util/Data;->NULL_DATE_TIME:Lcom/google/api/client/util/DateTime;

    invoke-virtual/range {v24 .. v25}, Lcom/google/api/services/calendar/model/EventDateTime;->setDateTime(Lcom/google/api/client/util/DateTime;)Lcom/google/api/services/calendar/model/EventDateTime;

    invoke-virtual {v5}, Lcom/google/api/services/calendar/model/Event;->getEnd()Lcom/google/api/services/calendar/model/EventDateTime;

    move-result-object v24

    sget-object v25, Lcom/google/api/client/util/Data;->NULL_DATE_TIME:Lcom/google/api/client/util/DateTime;

    invoke-virtual/range {v24 .. v25}, Lcom/google/api/services/calendar/model/EventDateTime;->setDateTime(Lcom/google/api/client/util/DateTime;)Lcom/google/api/services/calendar/model/EventDateTime;

    goto/16 :goto_1
.end method

.method private static createEventDateTime(JZ)Lcom/google/api/services/calendar/model/EventDateTime;
    .locals 4
    .param p0    # J
    .param p2    # Z

    new-instance v0, Lcom/google/api/services/calendar/model/EventDateTime;

    invoke-direct {v0}, Lcom/google/api/services/calendar/model/EventDateTime;-><init>()V

    if-eqz p2, :cond_0

    new-instance v1, Landroid/text/format/Time;

    const-string v2, "UTC"

    invoke-direct {v1, v2}, Landroid/text/format/Time;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p0, p1}, Landroid/text/format/Time;->set(J)V

    invoke-virtual {v1, p2}, Landroid/text/format/Time;->format3339(Z)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/google/api/services/calendar/model/EventDateTime;->setDate(Ljava/lang/String;)Lcom/google/api/services/calendar/model/EventDateTime;

    :goto_0
    return-object v0

    :cond_0
    new-instance v2, Lcom/google/api/client/util/DateTime;

    const/4 v3, 0x0

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-direct {v2, p0, p1, v3}, Lcom/google/api/client/util/DateTime;-><init>(JLjava/lang/Integer;)V

    invoke-virtual {v0, v2}, Lcom/google/api/services/calendar/model/EventDateTime;->setDateTime(Lcom/google/api/client/util/DateTime;)Lcom/google/api/services/calendar/model/EventDateTime;

    goto :goto_0
.end method

.method private fetchEntity(Ljava/lang/String;[Ljava/lang/String;)Landroid/content/Entity;
    .locals 2
    .param p1    # Ljava/lang/String;
    .param p2    # [Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;,
            Lcom/google/android/apiary/ParseException;
        }
    .end annotation

    invoke-virtual {p0, p1, p2}, Lcom/google/android/syncadapters/calendar/EventHandler;->newEntityIterator(Ljava/lang/String;[Ljava/lang/String;)Landroid/content/EntityIterator;

    move-result-object v0

    :try_start_0
    invoke-interface {v0}, Landroid/content/EntityIterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {v0}, Landroid/content/EntityIterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/content/Entity;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :goto_0
    invoke-interface {v0}, Landroid/content/EntityIterator;->close()V

    return-object v1

    :cond_0
    const/4 v1, 0x0

    goto :goto_0

    :catchall_0
    move-exception v1

    invoke-interface {v0}, Landroid/content/EntityIterator;->close()V

    throw v1
.end method

.method private getAttendeeSet(Ljava/util/List;)Ljava/util/Set;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/google/api/services/calendar/model/EventAttendee;",
            ">;)",
            "Ljava/util/Set",
            "<",
            "Lcom/google/api/services/calendar/model/EventAttendee;",
            ">;"
        }
    .end annotation

    if-nez p1, :cond_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_0
    new-instance v1, Lcom/google/android/syncadapters/calendar/EventHandler$1;

    invoke-direct {v1, p0}, Lcom/google/android/syncadapters/calendar/EventHandler$1;-><init>(Lcom/google/android/syncadapters/calendar/EventHandler;)V

    invoke-static {v1}, Lcom/google/common/collect/Sets;->newTreeSet(Ljava/util/Comparator;)Ljava/util/TreeSet;

    move-result-object v0

    invoke-interface {v0, p1}, Ljava/util/Set;->addAll(Ljava/util/Collection;)Z

    goto :goto_0
.end method

.method private getCalendarOwnerAccount(J)Ljava/lang/String;
    .locals 6
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;,
            Lcom/google/android/apiary/ParseException;
        }
    .end annotation

    const/4 v5, 0x0

    const/4 v3, 0x0

    iget-object v0, p0, Lcom/google/android/syncadapters/calendar/EventHandler;->mProvider:Landroid/content/ContentProviderClient;

    sget-object v1, Landroid/provider/CalendarContract$Calendars;->CONTENT_URI:Landroid/net/Uri;

    invoke-virtual {v1}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v1

    invoke-static {p1, p2}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/net/Uri$Builder;->appendPath(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v1

    invoke-virtual {v1}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v1

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/String;

    const-string v4, "ownerAccount"

    aput-object v4, v2, v5

    move-object v4, v3

    move-object v5, v3

    invoke-static/range {v0 .. v5}, Lcom/google/android/apiary/ProviderHelper;->queryProvider(Landroid/content/ContentProviderClient;Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v1

    if-eqz v1, :cond_1

    :try_start_0
    invoke-interface {v1}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x0

    invoke-interface {v1, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v3

    :cond_0
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    :cond_1
    return-object v3

    :catchall_0
    move-exception v0

    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    throw v0
.end method

.method private static getRecurrenceDate(Lcom/android/calendarcommon2/ICalendar$Property;)Ljava/lang/String;
    .locals 2

    const-string v0, "TZID"

    invoke-virtual {p0, v0}, Lcom/android/calendarcommon2/ICalendar$Property;->getFirstParameter(Ljava/lang/String;)Lcom/android/calendarcommon2/ICalendar$Parameter;

    move-result-object v0

    if-eqz v0, :cond_0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v0, v0, Lcom/android/calendarcommon2/ICalendar$Parameter;->value:Ljava/lang/String;

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ";"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p0}, Lcom/android/calendarcommon2/ICalendar$Property;->getValue()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    invoke-virtual {p0}, Lcom/android/calendarcommon2/ICalendar$Property;->getValue()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method private getReminderSet(Lcom/google/api/services/calendar/model/Event$Reminders;)Ljava/util/Set;
    .locals 2
    .param p1    # Lcom/google/api/services/calendar/model/Event$Reminders;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/api/services/calendar/model/Event$Reminders;",
            ")",
            "Ljava/util/Set",
            "<",
            "Lcom/google/api/services/calendar/model/EventReminder;",
            ">;"
        }
    .end annotation

    if-nez p1, :cond_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_0
    new-instance v1, Lcom/google/android/syncadapters/calendar/EventHandler$2;

    invoke-direct {v1, p0}, Lcom/google/android/syncadapters/calendar/EventHandler$2;-><init>(Lcom/google/android/syncadapters/calendar/EventHandler;)V

    invoke-static {v1}, Lcom/google/common/collect/Sets;->newTreeSet(Ljava/util/Comparator;)Ljava/util/TreeSet;

    move-result-object v0

    invoke-virtual {p1}, Lcom/google/api/services/calendar/model/Event$Reminders;->getOverrides()Ljava/util/List;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Set;->addAll(Ljava/util/Collection;)Z

    goto :goto_0
.end method

.method private static hasOnlyUnsyncedKeys(Lcom/google/api/services/calendar/model/Event;)Z
    .locals 3
    .param p0    # Lcom/google/api/services/calendar/model/Event;

    invoke-virtual {p0}, Lcom/google/api/services/calendar/model/Event;->keySet()Ljava/util/Set;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :cond_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    sget-object v2, Lcom/google/android/syncadapters/calendar/EventHandler;->UNSYNCED_EVENT_KEYS:Ljava/util/Set;

    invoke-interface {v2, v1}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    const/4 v2, 0x0

    :goto_0
    return v2

    :cond_1
    const/4 v2, 0x1

    goto :goto_0
.end method

.method private insertExtendedProperties(Ljava/util/List;Ljava/lang/Long;Ljava/lang/Integer;Ljava/util/Map;)V
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Landroid/content/ContentProviderOperation;",
            ">;",
            "Ljava/lang/Long;",
            "Ljava/lang/Integer;",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    const/4 v5, 0x0

    if-eqz p4, :cond_2

    invoke-interface {p4}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v6

    :cond_0
    :goto_0
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_0

    const-string v2, "alarmReminder"

    invoke-virtual {v1, v2}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_1

    new-instance v2, Landroid/content/ContentValues;

    invoke-direct {v2}, Landroid/content/ContentValues;-><init>()V

    const-string v3, "name"

    invoke-virtual {v2, v3, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    const-string v1, "value"

    invoke-virtual {v2, v1, v0}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    sget-object v0, Landroid/provider/CalendarContract$ExtendedProperties;->CONTENT_URI:Landroid/net/Uri;

    iget-object v1, p0, Lcom/google/android/syncadapters/calendar/EventHandler;->mAccount:Landroid/accounts/Account;

    invoke-static {v0, v1}, Lcom/google/android/syncadapters/calendar/HandlerUtils;->addQueryParameters(Landroid/net/Uri;Landroid/accounts/Account;)Landroid/net/Uri;

    move-result-object v1

    move-object v0, p1

    move-object v3, p2

    move-object v4, p3

    invoke-static/range {v0 .. v5}, Lcom/google/android/syncadapters/calendar/CalendarSyncAdapterApiary;->addInsertOperation(Ljava/util/List;Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/Long;Ljava/lang/Integer;Z)V

    goto :goto_0

    :cond_1
    sget-boolean v1, Lcom/google/android/syncadapters/calendar/Utils;->JELLY_BEAN_OR_HIGHER:Z

    if-eqz v1, :cond_0

    new-instance v2, Landroid/content/ContentValues;

    invoke-direct {v2}, Landroid/content/ContentValues;-><init>()V

    const-string v1, "method"

    const/4 v3, 0x4

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v2, v1, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    const-string v1, "minutes"

    invoke-static {v0}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {v2, v1, v0}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    sget-object v0, Landroid/provider/CalendarContract$Reminders;->CONTENT_URI:Landroid/net/Uri;

    iget-object v1, p0, Lcom/google/android/syncadapters/calendar/EventHandler;->mAccount:Landroid/accounts/Account;

    invoke-static {v0, v1}, Lcom/google/android/syncadapters/calendar/HandlerUtils;->addQueryParameters(Landroid/net/Uri;Landroid/accounts/Account;)Landroid/net/Uri;

    move-result-object v1

    move-object v0, p1

    move-object v3, p2

    move-object v4, p3

    invoke-static/range {v0 .. v5}, Lcom/google/android/syncadapters/calendar/CalendarSyncAdapterApiary;->addInsertOperation(Ljava/util/List;Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/Long;Ljava/lang/Integer;Z)V

    goto :goto_0

    :cond_2
    return-void
.end method

.method private isEqual(Ljava/lang/Object;Ljava/lang/Object;)Z
    .locals 1
    .param p1    # Ljava/lang/Object;
    .param p2    # Ljava/lang/Object;

    if-nez p1, :cond_1

    if-nez p2, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    :cond_1
    invoke-virtual {p1, p2}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    goto :goto_0
.end method

.method private parseDuration(Ljava/lang/String;)J
    .locals 7
    .param p1    # Ljava/lang/String;

    const-wide/16 v2, -0x1

    if-nez p1, :cond_0

    :goto_0
    return-wide v2

    :cond_0
    new-instance v0, Lcom/android/calendarcommon2/Duration;

    invoke-direct {v0}, Lcom/android/calendarcommon2/Duration;-><init>()V

    :try_start_0
    invoke-virtual {v0, p1}, Lcom/android/calendarcommon2/Duration;->parse(Ljava/lang/String;)V

    invoke-virtual {v0}, Lcom/android/calendarcommon2/Duration;->getMillis()J
    :try_end_0
    .catch Lcom/android/calendarcommon2/DateException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-wide v2

    goto :goto_0

    :catch_0
    move-exception v1

    const-string v4, "EventHandler"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "Bad DURATION: "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method private sameAttendees(Ljava/util/List;Ljava/util/List;)Z
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/google/api/services/calendar/model/EventAttendee;",
            ">;",
            "Ljava/util/List",
            "<",
            "Lcom/google/api/services/calendar/model/EventAttendee;",
            ">;)Z"
        }
    .end annotation

    invoke-direct {p0, p1}, Lcom/google/android/syncadapters/calendar/EventHandler;->getAttendeeSet(Ljava/util/List;)Ljava/util/Set;

    move-result-object v0

    invoke-direct {p0, p2}, Lcom/google/android/syncadapters/calendar/EventHandler;->getAttendeeSet(Ljava/util/List;)Ljava/util/Set;

    move-result-object v1

    invoke-direct {p0, v0, v1}, Lcom/google/android/syncadapters/calendar/EventHandler;->isEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method private sameExtendedProperties(Lcom/google/api/services/calendar/model/Event$ExtendedProperties;Lcom/google/api/services/calendar/model/Event$ExtendedProperties;)Z
    .locals 4
    .param p1    # Lcom/google/api/services/calendar/model/Event$ExtendedProperties;
    .param p2    # Lcom/google/api/services/calendar/model/Event$ExtendedProperties;

    const/4 v0, 0x1

    const/4 v1, 0x0

    if-nez p1, :cond_2

    if-nez p2, :cond_1

    :cond_0
    :goto_0
    return v0

    :cond_1
    move v0, v1

    goto :goto_0

    :cond_2
    if-eqz p2, :cond_3

    invoke-virtual {p1}, Lcom/google/api/services/calendar/model/Event$ExtendedProperties;->getPrivate()Ljava/util/Map;

    move-result-object v2

    invoke-virtual {p2}, Lcom/google/api/services/calendar/model/Event$ExtendedProperties;->getPrivate()Ljava/util/Map;

    move-result-object v3

    invoke-direct {p0, v2, v3}, Lcom/google/android/syncadapters/calendar/EventHandler;->isEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    invoke-virtual {p1}, Lcom/google/api/services/calendar/model/Event$ExtendedProperties;->getShared()Ljava/util/Map;

    move-result-object v2

    invoke-virtual {p2}, Lcom/google/api/services/calendar/model/Event$ExtendedProperties;->getShared()Ljava/util/Map;

    move-result-object v3

    invoke-direct {p0, v2, v3}, Lcom/google/android/syncadapters/calendar/EventHandler;->isEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    :cond_3
    move v0, v1

    goto :goto_0
.end method

.method private sameReminders(Lcom/google/api/services/calendar/model/Event$Reminders;Lcom/google/api/services/calendar/model/Event$Reminders;)Z
    .locals 2
    .param p1    # Lcom/google/api/services/calendar/model/Event$Reminders;
    .param p2    # Lcom/google/api/services/calendar/model/Event$Reminders;

    invoke-direct {p0, p1}, Lcom/google/android/syncadapters/calendar/EventHandler;->getReminderSet(Lcom/google/api/services/calendar/model/Event$Reminders;)Ljava/util/Set;

    move-result-object v0

    invoke-direct {p0, p2}, Lcom/google/android/syncadapters/calendar/EventHandler;->getReminderSet(Lcom/google/api/services/calendar/model/Event$Reminders;)Ljava/util/Set;

    move-result-object v1

    invoke-direct {p0, v0, v1}, Lcom/google/android/syncadapters/calendar/EventHandler;->isEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method private setDtendFromDuration(Landroid/content/ContentValues;J)V
    .locals 4
    .param p1    # Landroid/content/ContentValues;
    .param p2    # J

    const-wide/16 v1, 0x0

    cmp-long v1, p2, v1

    if-ltz v1, :cond_0

    const-string v1, "dtstart"

    invoke-virtual {p1, v1}, Landroid/content/ContentValues;->getAsLong(Ljava/lang/String;)Ljava/lang/Long;

    move-result-object v0

    if-nez v0, :cond_1

    const-string v1, "EventHandler"

    const-string v2, "Event has DURATION but no DTSTART"

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    :goto_0
    return-void

    :cond_1
    const-string v1, "dtend"

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    add-long/2addr v2, p2

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-virtual {p1, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    goto :goto_0
.end method

.method private touchDescription(Lcom/google/api/services/calendar/model/Event;Ljava/lang/String;)V
    .locals 2

    if-nez p2, :cond_0

    const-string v0, " "

    :goto_0
    invoke-virtual {p1, v0}, Lcom/google/api/services/calendar/model/Event;->setDescription(Ljava/lang/String;)Lcom/google/api/services/calendar/model/Event;

    return-void

    :cond_0
    const-string v0, " "

    invoke-virtual {p2, v0}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    const/4 v0, 0x0

    invoke-virtual {p2}, Ljava/lang/String;->length()I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    invoke-virtual {p2, v0, v1}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    :cond_1
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method


# virtual methods
.method public applyItemToEntity(Ljava/util/List;Lcom/google/api/services/calendar/model/Event;Landroid/content/Entity;ZLandroid/content/SyncResult;Ljava/lang/Object;)V
    .locals 17
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Landroid/content/ContentProviderOperation;",
            ">;",
            "Lcom/google/api/services/calendar/model/Event;",
            "Landroid/content/Entity;",
            "Z",
            "Landroid/content/SyncResult;",
            "Ljava/lang/Object;",
            ")V"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;,
            Lcom/google/android/apiary/ParseException;
        }
    .end annotation

    const-string v2, "EventHandler"

    const/4 v3, 0x2

    invoke-static {v2, v3}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v2

    if-eqz v2, :cond_0

    const-string v2, "EventHandler"

    const-string v3, "============= applyItemToEntity ============="

    invoke-static {v2, v3}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    const-string v2, "EventHandler"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "event is "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    move-object/from16 v0, p2

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    const-string v2, "EventHandler"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "entity is "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    move-object/from16 v0, p3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    if-nez p3, :cond_3

    const/4 v2, 0x1

    move v8, v2

    :goto_0
    new-instance v4, Landroid/content/ContentValues;

    invoke-direct {v4}, Landroid/content/ContentValues;-><init>()V

    move-object/from16 v5, p6

    check-cast v5, Lcom/google/android/syncadapters/calendar/CalendarSyncInfo;

    if-eqz p3, :cond_4

    invoke-virtual/range {p3 .. p3}, Landroid/content/Entity;->getEntityValues()Landroid/content/ContentValues;

    move-result-object v2

    const-string v3, "calendar_id"

    invoke-virtual {v2, v3}, Landroid/content/ContentValues;->getAsLong(Ljava/lang/String;)Ljava/lang/Long;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Long;->longValue()J

    move-result-wide v6

    :goto_1
    if-nez p2, :cond_6

    const/4 v2, 0x1

    :goto_2
    if-eqz p4, :cond_1

    const-string v3, "dirty"

    const/4 v9, 0x0

    invoke-static {v9}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v9

    invoke-virtual {v4, v3, v9}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    :cond_1
    const/4 v3, 0x1

    if-ne v2, v3, :cond_7

    if-eqz p3, :cond_2

    invoke-virtual/range {p3 .. p3}, Landroid/content/Entity;->getEntityValues()Landroid/content/ContentValues;

    move-result-object v2

    const-string v3, "_id"

    invoke-virtual {v2, v3}, Landroid/content/ContentValues;->getAsLong(Ljava/lang/String;)Ljava/lang/Long;

    move-result-object v2

    sget-object v3, Landroid/provider/CalendarContract$Events;->CONTENT_URI:Landroid/net/Uri;

    invoke-virtual {v2}, Ljava/lang/Long;->longValue()J

    move-result-wide v4

    invoke-static {v3, v4, v5}, Landroid/content/ContentUris;->withAppendedId(Landroid/net/Uri;J)Landroid/net/Uri;

    move-result-object v2

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/syncadapters/calendar/EventHandler;->mAccount:Landroid/accounts/Account;

    invoke-static {v2, v3}, Lcom/google/android/syncadapters/calendar/HandlerUtils;->addQueryParameters(Landroid/net/Uri;Landroid/accounts/Account;)Landroid/net/Uri;

    move-result-object v2

    const/4 v3, 0x0

    const/4 v4, 0x0

    move-object/from16 v0, p1

    invoke-static {v0, v2, v3, v4}, Lcom/google/android/syncadapters/calendar/CalendarSyncAdapterApiary;->addDeleteOperation(Ljava/util/List;Landroid/net/Uri;Ljava/lang/Long;Z)V

    move-object/from16 v0, p5

    iget-object v2, v0, Landroid/content/SyncResult;->stats:Landroid/content/SyncStats;

    iget-wide v3, v2, Landroid/content/SyncStats;->numDeletes:J

    const-wide/16 v5, 0x1

    add-long/2addr v3, v5

    iput-wide v3, v2, Landroid/content/SyncStats;->numDeletes:J

    :cond_2
    :goto_3
    return-void

    :cond_3
    const/4 v2, 0x0

    move v8, v2

    goto :goto_0

    :cond_4
    if-eqz v5, :cond_5

    iget-wide v6, v5, Lcom/google/android/syncadapters/calendar/CalendarSyncInfo;->calendarId:J

    goto :goto_1

    :cond_5
    const-wide/16 v6, -0x1

    goto :goto_1

    :cond_6
    move-object/from16 v2, p0

    move-object/from16 v3, p2

    invoke-virtual/range {v2 .. v7}, Lcom/google/android/syncadapters/calendar/EventHandler;->entryToContentValues(Lcom/google/api/services/calendar/model/Event;Landroid/content/ContentValues;Lcom/google/android/syncadapters/calendar/CalendarSyncInfo;J)I

    move-result v2

    goto :goto_2

    :cond_7
    if-nez v2, :cond_2

    const-string v2, "EventHandler"

    const/4 v3, 0x2

    invoke-static {v2, v3}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v2

    if-eqz v2, :cond_8

    const-string v2, "EventHandler"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "Got eventEntry from server: "

    invoke-virtual {v3, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    :cond_8
    if-eqz v8, :cond_9

    sget-object v2, Landroid/provider/CalendarContract$EventsEntity;->CONTENT_URI:Landroid/net/Uri;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/syncadapters/calendar/EventHandler;->mAccount:Landroid/accounts/Account;

    invoke-static {v2, v3}, Lcom/google/android/syncadapters/SyncAdapterUtils;->addQueryParameters(Landroid/net/Uri;Landroid/accounts/Account;)Landroid/net/Uri;

    move-result-object v2

    invoke-static {v2}, Landroid/content/ContentProviderOperation;->newAssertQuery(Landroid/net/Uri;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v2

    const/4 v3, 0x2

    new-array v3, v3, [Ljava/lang/String;

    const/4 v8, 0x0

    const-string v9, "_sync_id=?"

    aput-object v9, v3, v8

    const/4 v8, 0x1

    const-string v9, "calendar_id=?"

    aput-object v9, v3, v8

    invoke-static {v3}, Lcom/google/android/syncadapters/calendar/Utils;->makeWhere([Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    const/4 v8, 0x2

    new-array v8, v8, [Ljava/lang/String;

    const/4 v9, 0x0

    const-string v10, "_sync_id"

    invoke-virtual {v4, v10}, Landroid/content/ContentValues;->getAsString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v10

    aput-object v10, v8, v9

    const/4 v9, 0x1

    const-string v10, "calendar_id"

    invoke-virtual {v4, v10}, Landroid/content/ContentValues;->getAsInteger(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v10

    invoke-static {v10}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v10

    aput-object v10, v8, v9

    invoke-virtual {v2, v3, v8}, Landroid/content/ContentProviderOperation$Builder;->withSelection(Ljava/lang/String;[Ljava/lang/String;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v2

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Landroid/content/ContentProviderOperation$Builder;->withExpectedCount(I)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/ContentProviderOperation$Builder;->build()Landroid/content/ContentProviderOperation;

    move-result-object v2

    move-object/from16 v0, p1

    invoke-interface {v0, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    invoke-interface/range {p1 .. p1}, Ljava/util/List;->size()I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    sget-object v3, Landroid/provider/CalendarContract$Events;->CONTENT_URI:Landroid/net/Uri;

    move-object/from16 v0, p0

    iget-object v8, v0, Lcom/google/android/syncadapters/calendar/EventHandler;->mAccount:Landroid/accounts/Account;

    invoke-static {v3, v8}, Lcom/google/android/syncadapters/calendar/HandlerUtils;->addQueryParameters(Landroid/net/Uri;Landroid/accounts/Account;)Landroid/net/Uri;

    move-result-object v9

    const/4 v11, 0x0

    const/4 v12, 0x0

    const/4 v13, 0x0

    move-object/from16 v8, p1

    move-object v10, v4

    invoke-static/range {v8 .. v13}, Lcom/google/android/syncadapters/calendar/CalendarSyncAdapterApiary;->addInsertOperation(Ljava/util/List;Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/Long;Ljava/lang/Integer;Z)V

    const/4 v11, 0x0

    move-object/from16 v0, p5

    iget-object v3, v0, Landroid/content/SyncResult;->stats:Landroid/content/SyncStats;

    iget-wide v8, v3, Landroid/content/SyncStats;->numInserts:J

    const-wide/16 v12, 0x1

    add-long/2addr v8, v12

    iput-wide v8, v3, Landroid/content/SyncStats;->numInserts:J

    move-object v12, v2

    :goto_4
    const/4 v2, 0x1

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    const-string v3, "hasAlarm"

    invoke-virtual {v4, v3}, Landroid/content/ContentValues;->getAsInteger(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/Integer;->equals(Ljava/lang/Object;)Z

    move-result v14

    if-eqz v14, :cond_11

    invoke-virtual/range {p2 .. p2}, Lcom/google/api/services/calendar/model/Event;->getReminders()Lcom/google/api/services/calendar/model/Event$Reminders;

    move-result-object v2

    if-eqz v2, :cond_11

    invoke-virtual {v2}, Lcom/google/api/services/calendar/model/Event$Reminders;->getUseDefault()Ljava/lang/Boolean;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v3

    if-eqz v3, :cond_d

    if-nez v5, :cond_c

    const/4 v2, 0x0

    :goto_5
    if-eqz v2, :cond_11

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_6
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_11

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/api/services/calendar/model/EventReminder;

    new-instance v10, Landroid/content/ContentValues;

    invoke-direct {v10}, Landroid/content/ContentValues;-><init>()V

    invoke-virtual {v2}, Lcom/google/api/services/calendar/model/EventReminder;->getMethod()Ljava/lang/String;

    move-result-object v5

    const-string v8, "popup"

    invoke-virtual {v8, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_e

    const-string v5, "method"

    const/4 v8, 0x1

    invoke-static {v8}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    invoke-virtual {v10, v5, v8}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    :goto_7
    invoke-virtual {v2}, Lcom/google/api/services/calendar/model/EventReminder;->getMinutes()Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    const-string v5, "minutes"

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v10, v5, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    sget-object v2, Landroid/provider/CalendarContract$Reminders;->CONTENT_URI:Landroid/net/Uri;

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/google/android/syncadapters/calendar/EventHandler;->mAccount:Landroid/accounts/Account;

    invoke-static {v2, v5}, Lcom/google/android/syncadapters/calendar/HandlerUtils;->addQueryParameters(Landroid/net/Uri;Landroid/accounts/Account;)Landroid/net/Uri;

    move-result-object v9

    const/4 v13, 0x0

    move-object/from16 v8, p1

    invoke-static/range {v8 .. v13}, Lcom/google/android/syncadapters/calendar/CalendarSyncAdapterApiary;->addInsertOperation(Ljava/util/List;Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/Long;Ljava/lang/Integer;Z)V

    goto :goto_6

    :cond_9
    const/4 v2, 0x0

    invoke-virtual/range {p3 .. p3}, Landroid/content/Entity;->getEntityValues()Landroid/content/ContentValues;

    move-result-object v3

    const-string v8, "_id"

    invoke-virtual {v3, v8}, Landroid/content/ContentValues;->getAsLong(Ljava/lang/String;)Ljava/lang/Long;

    move-result-object v11

    sget-object v3, Landroid/provider/CalendarContract$Events;->CONTENT_URI:Landroid/net/Uri;

    move-object/from16 v0, p0

    iget-object v8, v0, Lcom/google/android/syncadapters/calendar/EventHandler;->mAccount:Landroid/accounts/Account;

    invoke-static {v3, v8}, Lcom/google/android/syncadapters/calendar/HandlerUtils;->addQueryParameters(Landroid/net/Uri;Landroid/accounts/Account;)Landroid/net/Uri;

    move-result-object v9

    const/4 v12, 0x0

    const/4 v13, 0x1

    move-object/from16 v8, p1

    move-object v10, v4

    invoke-static/range {v8 .. v13}, Lcom/google/android/syncadapters/calendar/CalendarSyncAdapterApiary;->addUpdateOperation(Ljava/util/List;Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/Long;Ljava/lang/Integer;Z)V

    sget-object v3, Landroid/provider/CalendarContract$Attendees;->CONTENT_URI:Landroid/net/Uri;

    move-object/from16 v0, p0

    iget-object v8, v0, Lcom/google/android/syncadapters/calendar/EventHandler;->mAccount:Landroid/accounts/Account;

    invoke-static {v3, v8}, Lcom/google/android/syncadapters/calendar/HandlerUtils;->addQueryParameters(Landroid/net/Uri;Landroid/accounts/Account;)Landroid/net/Uri;

    move-result-object v3

    const/4 v8, 0x0

    move-object/from16 v0, p1

    invoke-static {v0, v3, v11, v8}, Lcom/google/android/syncadapters/calendar/CalendarSyncAdapterApiary;->addDeleteOperation(Ljava/util/List;Landroid/net/Uri;Ljava/lang/Long;Z)V

    const/4 v3, 0x1

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    const-string v8, "hasAlarm"

    invoke-virtual {v4, v8}, Landroid/content/ContentValues;->getAsInteger(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v8

    invoke-virtual {v3, v8}, Ljava/lang/Integer;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_a

    if-eqz v5, :cond_b

    :cond_a
    sget-object v3, Landroid/provider/CalendarContract$Reminders;->CONTENT_URI:Landroid/net/Uri;

    move-object/from16 v0, p0

    iget-object v8, v0, Lcom/google/android/syncadapters/calendar/EventHandler;->mAccount:Landroid/accounts/Account;

    invoke-static {v3, v8}, Lcom/google/android/syncadapters/calendar/HandlerUtils;->addQueryParameters(Landroid/net/Uri;Landroid/accounts/Account;)Landroid/net/Uri;

    move-result-object v3

    const/4 v8, 0x0

    move-object/from16 v0, p1

    invoke-static {v0, v3, v11, v8}, Lcom/google/android/syncadapters/calendar/CalendarSyncAdapterApiary;->addDeleteOperation(Ljava/util/List;Landroid/net/Uri;Ljava/lang/Long;Z)V

    :cond_b
    sget-object v3, Landroid/provider/CalendarContract$ExtendedProperties;->CONTENT_URI:Landroid/net/Uri;

    move-object/from16 v0, p0

    iget-object v8, v0, Lcom/google/android/syncadapters/calendar/EventHandler;->mAccount:Landroid/accounts/Account;

    invoke-static {v3, v8}, Lcom/google/android/syncadapters/calendar/HandlerUtils;->addQueryParameters(Landroid/net/Uri;Landroid/accounts/Account;)Landroid/net/Uri;

    move-result-object v3

    const/4 v8, 0x0

    move-object/from16 v0, p1

    invoke-static {v0, v3, v11, v8}, Lcom/google/android/syncadapters/calendar/CalendarSyncAdapterApiary;->addDeleteOperation(Ljava/util/List;Landroid/net/Uri;Ljava/lang/Long;Z)V

    move-object/from16 v0, p5

    iget-object v3, v0, Landroid/content/SyncResult;->stats:Landroid/content/SyncStats;

    iget-wide v8, v3, Landroid/content/SyncStats;->numUpdates:J

    const-wide/16 v12, 0x1

    add-long/2addr v8, v12

    iput-wide v8, v3, Landroid/content/SyncStats;->numUpdates:J

    move-object v12, v2

    goto/16 :goto_4

    :cond_c
    iget-object v2, v5, Lcom/google/android/syncadapters/calendar/CalendarSyncInfo;->defaultReminders:Ljava/util/List;

    goto/16 :goto_5

    :cond_d
    invoke-virtual {v2}, Lcom/google/api/services/calendar/model/Event$Reminders;->getOverrides()Ljava/util/List;

    move-result-object v2

    goto/16 :goto_5

    :cond_e
    const-string v8, "email"

    invoke-virtual {v8, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_f

    const-string v5, "method"

    const/4 v8, 0x2

    invoke-static {v8}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    invoke-virtual {v10, v5, v8}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    goto/16 :goto_7

    :cond_f
    const-string v8, "sms"

    invoke-virtual {v8, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_10

    const-string v5, "method"

    const/4 v8, 0x3

    invoke-static {v8}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    invoke-virtual {v10, v5, v8}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    goto/16 :goto_7

    :cond_10
    const-string v8, "method"

    const/4 v9, 0x1

    invoke-static {v9}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v9

    invoke-virtual {v10, v8, v9}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    const-string v8, "EventHandler"

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v13, "Unknown reminder method: "

    invoke-virtual {v9, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v9, " "

    invoke-virtual {v5, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v9, "should not happen!"

    invoke-virtual {v5, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v8, v5}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_7

    :cond_11
    invoke-virtual/range {p2 .. p2}, Lcom/google/api/services/calendar/model/Event;->getAttendees()Ljava/util/List;

    move-result-object v2

    if-eqz v2, :cond_1d

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v15

    :goto_8
    invoke-interface {v15}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1d

    invoke-interface {v15}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/api/services/calendar/model/EventAttendee;

    new-instance v16, Landroid/content/ContentValues;

    invoke-direct/range {v16 .. v16}, Landroid/content/ContentValues;-><init>()V

    invoke-virtual {v2}, Lcom/google/api/services/calendar/model/EventAttendee;->getDisplayName()Ljava/lang/String;

    move-result-object v3

    if-nez v3, :cond_12

    const-string v3, ""

    :cond_12
    const-string v5, "attendeeName"

    move-object/from16 v0, v16

    invoke-virtual {v0, v5, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v2}, Lcom/google/api/services/calendar/model/EventAttendee;->getEmail()Ljava/lang/String;

    move-result-object v3

    if-eqz v3, :cond_13

    const-string v5, "attendeeEmail"

    move-object/from16 v0, v16

    invoke-virtual {v0, v5, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    :cond_13
    const-string v3, "id"

    invoke-virtual {v2, v3}, Lcom/google/api/services/calendar/model/EventAttendee;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    if-eqz v3, :cond_16

    invoke-virtual {v2}, Lcom/google/api/services/calendar/model/EventAttendee;->getSelf()Ljava/lang/Boolean;

    move-result-object v5

    const/4 v8, 0x0

    invoke-static {v5, v8}, Lcom/google/android/syncadapters/calendar/Utils;->getBooleanValue(Ljava/lang/Boolean;Z)Z

    move-result v5

    sget-boolean v8, Lcom/google/android/syncadapters/calendar/Utils;->JELLY_BEAN_OR_HIGHER:Z

    if-eqz v8, :cond_14

    const-string v8, "attendeeIdNamespace"

    const-string v9, "com.google"

    move-object/from16 v0, v16

    invoke-virtual {v0, v8, v9}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    const-string v8, "attendeeIdentity"

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "gprofile:"

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    move-object/from16 v0, v16

    invoke-virtual {v0, v8, v9}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    :cond_14
    if-eqz v5, :cond_15

    new-instance v10, Landroid/content/ContentValues;

    invoke-direct {v10}, Landroid/content/ContentValues;-><init>()V

    const-string v8, "sync_data3"

    invoke-virtual {v10, v8, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    sget-object v8, Landroid/provider/CalendarContract$Events;->CONTENT_URI:Landroid/net/Uri;

    move-object/from16 v0, p0

    iget-object v9, v0, Lcom/google/android/syncadapters/calendar/EventHandler;->mAccount:Landroid/accounts/Account;

    invoke-static {v8, v9}, Lcom/google/android/syncadapters/calendar/HandlerUtils;->addQueryParameters(Landroid/net/Uri;Landroid/accounts/Account;)Landroid/net/Uri;

    move-result-object v9

    const/4 v13, 0x0

    move-object/from16 v8, p1

    invoke-static/range {v8 .. v13}, Lcom/google/android/syncadapters/calendar/CalendarSyncAdapterApiary;->addUpdateOperation(Ljava/util/List;Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/Long;Ljava/lang/Integer;Z)V

    :cond_15
    if-eqz v5, :cond_19

    const-wide/16 v8, 0x0

    cmp-long v5, v6, v8

    if-gez v5, :cond_18

    const-string v5, "EventHandler"

    const-string v8, "Missing calendarId, can\'t get owner"

    new-instance v9, Ljava/lang/Throwable;

    invoke-direct {v9}, Ljava/lang/Throwable;-><init>()V

    invoke-static {v5, v8, v9}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    const/4 v5, 0x0

    :goto_9
    if-nez v5, :cond_1a

    const-string v5, "attendeeEmail"

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v8, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v8, "@profile.calendar.google.com"

    invoke-virtual {v3, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    move-object/from16 v0, v16

    invoke-virtual {v0, v5, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    :cond_16
    :goto_a
    invoke-virtual {v2}, Lcom/google/api/services/calendar/model/EventAttendee;->getResponseStatus()Ljava/lang/String;

    move-result-object v5

    sget-object v3, Lcom/google/android/syncadapters/calendar/EventHandler;->ENTRY_TYPE_TO_PROVIDER_ATTENDEE:Ljava/util/HashMap;

    invoke-virtual {v3, v5}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Integer;

    if-nez v3, :cond_17

    const-string v3, "EventHandler"

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "Unknown attendee status "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v3, v5}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v3, 0x0

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    :cond_17
    const-string v5, "attendeeStatus"

    move-object/from16 v0, v16

    invoke-virtual {v0, v5, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    invoke-virtual {v2}, Lcom/google/api/services/calendar/model/EventAttendee;->getOrganizer()Ljava/lang/Boolean;

    move-result-object v3

    const/4 v5, 0x0

    invoke-static {v3, v5}, Lcom/google/android/syncadapters/calendar/Utils;->getBooleanValue(Ljava/lang/Boolean;Z)Z

    move-result v3

    if-eqz v3, :cond_1b

    const/4 v3, 0x2

    :goto_b
    const-string v5, "attendeeRelationship"

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    move-object/from16 v0, v16

    invoke-virtual {v0, v5, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    invoke-virtual {v2}, Lcom/google/api/services/calendar/model/EventAttendee;->getOptional()Ljava/lang/Boolean;

    move-result-object v2

    const/4 v3, 0x0

    invoke-static {v2, v3}, Lcom/google/android/syncadapters/calendar/Utils;->getBooleanValue(Ljava/lang/Boolean;Z)Z

    move-result v2

    if-eqz v2, :cond_1c

    const/4 v2, 0x2

    :goto_c
    const-string v3, "attendeeType"

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    move-object/from16 v0, v16

    invoke-virtual {v0, v3, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    sget-object v2, Landroid/provider/CalendarContract$Attendees;->CONTENT_URI:Landroid/net/Uri;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/syncadapters/calendar/EventHandler;->mAccount:Landroid/accounts/Account;

    invoke-static {v2, v3}, Lcom/google/android/syncadapters/calendar/HandlerUtils;->addQueryParameters(Landroid/net/Uri;Landroid/accounts/Account;)Landroid/net/Uri;

    move-result-object v9

    const/4 v13, 0x0

    move-object/from16 v8, p1

    move-object/from16 v10, v16

    invoke-static/range {v8 .. v13}, Lcom/google/android/syncadapters/calendar/CalendarSyncAdapterApiary;->addInsertOperation(Ljava/util/List;Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/Long;Ljava/lang/Integer;Z)V

    goto/16 :goto_8

    :cond_18
    move-object/from16 v0, p0

    invoke-direct {v0, v6, v7}, Lcom/google/android/syncadapters/calendar/EventHandler;->getCalendarOwnerAccount(J)Ljava/lang/String;

    move-result-object v5

    goto/16 :goto_9

    :cond_19
    const/4 v5, 0x0

    goto/16 :goto_9

    :cond_1a
    const-string v3, "attendeeEmail"

    move-object/from16 v0, v16

    invoke-virtual {v0, v3, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_a

    :cond_1b
    const/4 v3, 0x1

    goto :goto_b

    :cond_1c
    const/4 v2, 0x1

    goto :goto_c

    :cond_1d
    const/4 v2, 0x1

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    const-string v3, "hasExtendedProperties"

    invoke-virtual {v4, v3}, Landroid/content/ContentValues;->getAsInteger(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/Integer;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_1e

    if-eqz v14, :cond_2

    :cond_1e
    invoke-virtual/range {p2 .. p2}, Lcom/google/api/services/calendar/model/Event;->getExtendedProperties()Lcom/google/api/services/calendar/model/Event$ExtendedProperties;

    move-result-object v2

    if-eqz v2, :cond_2

    invoke-virtual {v2}, Lcom/google/api/services/calendar/model/Event$ExtendedProperties;->getShared()Ljava/util/Map;

    move-result-object v3

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    invoke-direct {v0, v1, v11, v12, v3}, Lcom/google/android/syncadapters/calendar/EventHandler;->insertExtendedProperties(Ljava/util/List;Ljava/lang/Long;Ljava/lang/Integer;Ljava/util/Map;)V

    invoke-virtual {v2}, Lcom/google/api/services/calendar/model/Event$ExtendedProperties;->getPrivate()Ljava/util/Map;

    move-result-object v2

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    invoke-direct {v0, v1, v11, v12, v2}, Lcom/google/android/syncadapters/calendar/EventHandler;->insertExtendedProperties(Ljava/util/List;Ljava/lang/Long;Ljava/lang/Integer;Ljava/util/Map;)V

    goto/16 :goto_3
.end method

.method public convertEntityToEntry(Landroid/content/Entity;)Lcom/google/api/services/calendar/model/Event;
    .locals 40
    .param p1    # Landroid/content/Entity;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    new-instance v17, Lcom/google/api/services/calendar/model/Event;

    invoke-direct/range {v17 .. v17}, Lcom/google/api/services/calendar/model/Event;-><init>()V

    invoke-virtual/range {p1 .. p1}, Landroid/content/Entity;->getEntityValues()Landroid/content/ContentValues;

    move-result-object v38

    const-string v4, "_sync_id"

    move-object/from16 v0, v38

    invoke-virtual {v0, v4}, Landroid/content/ContentValues;->containsKey(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_0

    const-string v4, "_sync_id"

    move-object/from16 v0, v38

    invoke-virtual {v0, v4}, Landroid/content/ContentValues;->getAsString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Lcom/google/android/syncadapters/calendar/Utils;->extractEventId(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    move-object/from16 v0, v17

    invoke-virtual {v0, v4}, Lcom/google/api/services/calendar/model/Event;->setId(Ljava/lang/String;)Lcom/google/api/services/calendar/model/Event;

    :cond_0
    const-string v4, "eventStatus"

    move-object/from16 v0, v38

    invoke-virtual {v0, v4}, Landroid/content/ContentValues;->containsKey(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_2

    const-string v4, "eventStatus"

    move-object/from16 v0, v38

    invoke-virtual {v0, v4}, Landroid/content/ContentValues;->getAsInteger(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v24

    if-nez v24, :cond_1

    const/4 v4, 0x0

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v24

    :cond_1
    invoke-virtual/range {v24 .. v24}, Ljava/lang/Integer;->intValue()I

    move-result v4

    packed-switch v4, :pswitch_data_0

    const-string v4, "EventHandler"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "Unexpected value for status: "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    move-object/from16 v0, v24

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "; using tentative."

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    const-string v34, "tentative"

    :goto_0
    move-object/from16 v0, v17

    move-object/from16 v1, v34

    invoke-virtual {v0, v1}, Lcom/google/api/services/calendar/model/Event;->setStatus(Ljava/lang/String;)Lcom/google/api/services/calendar/model/Event;

    :cond_2
    const-string v4, "accessLevel"

    move-object/from16 v0, v38

    invoke-virtual {v0, v4}, Landroid/content/ContentValues;->containsKey(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_4

    const-string v4, "accessLevel"

    move-object/from16 v0, v38

    invoke-virtual {v0, v4}, Landroid/content/ContentValues;->getAsInteger(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v26

    if-nez v26, :cond_3

    const/4 v4, 0x0

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v26

    :cond_3
    invoke-virtual/range {v26 .. v26}, Ljava/lang/Integer;->intValue()I

    move-result v4

    packed-switch v4, :pswitch_data_1

    const-string v4, "EventHandler"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "Unexpected value for visibility: "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    move-object/from16 v0, v26

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "; using default visibility."

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    const-string v39, "default"

    :goto_1
    move-object/from16 v0, v17

    move-object/from16 v1, v39

    invoke-virtual {v0, v1}, Lcom/google/api/services/calendar/model/Event;->setVisibility(Ljava/lang/String;)Lcom/google/api/services/calendar/model/Event;

    :cond_4
    const-string v4, "availability"

    move-object/from16 v0, v38

    invoke-virtual {v0, v4}, Landroid/content/ContentValues;->containsKey(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_6

    const-string v4, "availability"

    move-object/from16 v0, v38

    invoke-virtual {v0, v4}, Landroid/content/ContentValues;->getAsInteger(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v25

    if-nez v25, :cond_5

    const/4 v4, 0x0

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v25

    :cond_5
    invoke-virtual/range {v25 .. v25}, Ljava/lang/Integer;->intValue()I

    move-result v4

    packed-switch v4, :pswitch_data_2

    const-string v4, "EventHandler"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "Unexpected value for transparency: "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    move-object/from16 v0, v25

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "; using opaque transparency."

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    const-string v37, "opaque"

    :goto_2
    move-object/from16 v0, v17

    move-object/from16 v1, v37

    invoke-virtual {v0, v1}, Lcom/google/api/services/calendar/model/Event;->setTransparency(Ljava/lang/String;)Lcom/google/api/services/calendar/model/Event;

    :cond_6
    const-string v4, "title"

    move-object/from16 v0, v38

    invoke-virtual {v0, v4}, Landroid/content/ContentValues;->containsKey(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_7

    const-string v4, "title"

    move-object/from16 v0, v38

    invoke-virtual {v0, v4}, Landroid/content/ContentValues;->getAsString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    move-object/from16 v0, v17

    invoke-virtual {v0, v4}, Lcom/google/api/services/calendar/model/Event;->setSummary(Ljava/lang/String;)Lcom/google/api/services/calendar/model/Event;

    :cond_7
    const-string v4, "description"

    move-object/from16 v0, v38

    invoke-virtual {v0, v4}, Landroid/content/ContentValues;->containsKey(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_8

    const-string v4, "description"

    move-object/from16 v0, v38

    invoke-virtual {v0, v4}, Landroid/content/ContentValues;->getAsString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    move-object/from16 v0, v17

    invoke-virtual {v0, v4}, Lcom/google/api/services/calendar/model/Event;->setDescription(Ljava/lang/String;)Lcom/google/api/services/calendar/model/Event;

    :cond_8
    const-string v4, "eventLocation"

    move-object/from16 v0, v38

    invoke-virtual {v0, v4}, Landroid/content/ContentValues;->containsKey(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_9

    const-string v4, "eventLocation"

    move-object/from16 v0, v38

    invoke-virtual {v0, v4}, Landroid/content/ContentValues;->getAsString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    move-object/from16 v0, v17

    invoke-virtual {v0, v4}, Lcom/google/api/services/calendar/model/Event;->setLocation(Ljava/lang/String;)Lcom/google/api/services/calendar/model/Event;

    :cond_9
    invoke-virtual/range {p1 .. p1}, Landroid/content/Entity;->getSubValues()Ljava/util/ArrayList;

    move-result-object v35

    move-object/from16 v0, p0

    move-object/from16 v1, v38

    move-object/from16 v2, v35

    move-object/from16 v3, v17

    invoke-direct {v0, v1, v2, v3}, Lcom/google/android/syncadapters/calendar/EventHandler;->addAttendeesToEntry(Landroid/content/ContentValues;Ljava/util/ArrayList;Lcom/google/api/services/calendar/model/Event;)V

    new-instance v22, Lcom/android/calendarcommon2/ICalendar$Component;

    const-string v4, "DUMMY"

    const/4 v5, 0x0

    move-object/from16 v0, v22

    invoke-direct {v0, v4, v5}, Lcom/android/calendarcommon2/ICalendar$Component;-><init>(Ljava/lang/String;Lcom/android/calendarcommon2/ICalendar$Component;)V

    const-string v4, "RRULE"

    const-string v5, "rrule"

    move-object/from16 v0, v38

    invoke-virtual {v0, v5}, Landroid/content/ContentValues;->getAsString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    move-object/from16 v0, v22

    invoke-static {v0, v4, v5}, Lcom/android/calendarcommon2/RecurrenceSet;->addPropertiesForRuleStr(Lcom/android/calendarcommon2/ICalendar$Component;Ljava/lang/String;Ljava/lang/String;)V

    const-string v4, "RDATE"

    const-string v5, "rdate"

    move-object/from16 v0, v38

    invoke-virtual {v0, v5}, Landroid/content/ContentValues;->getAsString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    move-object/from16 v0, v22

    invoke-static {v0, v4, v5}, Lcom/android/calendarcommon2/RecurrenceSet;->addPropertyForDateStr(Lcom/android/calendarcommon2/ICalendar$Component;Ljava/lang/String;Ljava/lang/String;)V

    const-string v4, "EXRULE"

    const-string v5, "exrule"

    move-object/from16 v0, v38

    invoke-virtual {v0, v5}, Landroid/content/ContentValues;->getAsString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    move-object/from16 v0, v22

    invoke-static {v0, v4, v5}, Lcom/android/calendarcommon2/RecurrenceSet;->addPropertiesForRuleStr(Lcom/android/calendarcommon2/ICalendar$Component;Ljava/lang/String;Ljava/lang/String;)V

    const-string v4, "EXDATE"

    const-string v5, "exdate"

    move-object/from16 v0, v38

    invoke-virtual {v0, v5}, Landroid/content/ContentValues;->getAsString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    move-object/from16 v0, v22

    invoke-static {v0, v4, v5}, Lcom/android/calendarcommon2/RecurrenceSet;->addPropertyForDateStr(Lcom/android/calendarcommon2/ICalendar$Component;Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual/range {v22 .. v22}, Lcom/android/calendarcommon2/ICalendar$Component;->getPropertyNames()Ljava/util/Set;

    move-result-object v4

    invoke-interface {v4}, Ljava/util/Set;->size()I

    move-result v4

    if-lez v4, :cond_1a

    move-object/from16 v0, p0

    move-object/from16 v1, v22

    move-object/from16 v2, v17

    invoke-direct {v0, v1, v2}, Lcom/google/android/syncadapters/calendar/EventHandler;->addRecurrenceToEntry(Lcom/android/calendarcommon2/ICalendar$Component;Lcom/google/api/services/calendar/model/Event;)V

    const/16 v23, 0x1

    :goto_3
    const/4 v4, 0x1

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    const-string v5, "allDay"

    move-object/from16 v0, v38

    invoke-virtual {v0, v5}, Landroid/content/ContentValues;->getAsInteger(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/Integer;->equals(Ljava/lang/Object;)Z

    move-result v10

    const-string v4, "eventTimezone"

    move-object/from16 v0, v38

    invoke-virtual {v0, v4}, Landroid/content/ContentValues;->getAsString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v36

    invoke-static/range {v36 .. v36}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-eqz v4, :cond_1b

    invoke-static {}, Ljava/util/TimeZone;->getDefault()Ljava/util/TimeZone;

    move-result-object v4

    invoke-virtual {v4}, Ljava/util/TimeZone;->getID()Ljava/lang/String;

    move-result-object v36

    const/16 v21, 0x0

    :goto_4
    const-string v4, "dtstart"

    move-object/from16 v0, v38

    invoke-virtual {v0, v4}, Landroid/content/ContentValues;->containsKey(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_c

    const-string v4, "dtstart"

    move-object/from16 v0, v38

    invoke-virtual {v0, v4}, Landroid/content/ContentValues;->getAsLong(Ljava/lang/String;)Ljava/lang/Long;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/Long;->longValue()J

    move-result-wide v15

    move-wide v0, v15

    invoke-static {v0, v1, v10}, Lcom/google/android/syncadapters/calendar/EventHandler;->createEventDateTime(JZ)Lcom/google/api/services/calendar/model/EventDateTime;

    move-result-object v12

    if-nez v23, :cond_a

    if-eqz v21, :cond_b

    :cond_a
    move-object/from16 v0, v36

    invoke-virtual {v12, v0}, Lcom/google/api/services/calendar/model/EventDateTime;->setTimeZone(Ljava/lang/String;)Lcom/google/api/services/calendar/model/EventDateTime;

    :cond_b
    move-object/from16 v0, v17

    invoke-virtual {v0, v12}, Lcom/google/api/services/calendar/model/Event;->setStart(Lcom/google/api/services/calendar/model/EventDateTime;)Lcom/google/api/services/calendar/model/Event;

    :cond_c
    const-string v4, "dtend"

    move-object/from16 v0, v38

    invoke-virtual {v0, v4}, Landroid/content/ContentValues;->containsKey(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_f

    const-string v4, "dtend"

    move-object/from16 v0, v38

    invoke-virtual {v0, v4}, Landroid/content/ContentValues;->getAsLong(Ljava/lang/String;)Ljava/lang/Long;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/Long;->longValue()J

    move-result-wide v13

    invoke-static {v13, v14, v10}, Lcom/google/android/syncadapters/calendar/EventHandler;->createEventDateTime(JZ)Lcom/google/api/services/calendar/model/EventDateTime;

    move-result-object v12

    if-nez v23, :cond_d

    if-eqz v21, :cond_e

    :cond_d
    move-object/from16 v0, v36

    invoke-virtual {v12, v0}, Lcom/google/api/services/calendar/model/EventDateTime;->setTimeZone(Ljava/lang/String;)Lcom/google/api/services/calendar/model/EventDateTime;

    :cond_e
    move-object/from16 v0, v17

    invoke-virtual {v0, v12}, Lcom/google/api/services/calendar/model/Event;->setEnd(Lcom/google/api/services/calendar/model/EventDateTime;)Lcom/google/api/services/calendar/model/Event;

    :cond_f
    const-string v4, "hasAlarm"

    move-object/from16 v0, v38

    invoke-virtual {v0, v4}, Landroid/content/ContentValues;->getAsInteger(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v20

    if-eqz v20, :cond_10

    invoke-virtual/range {v20 .. v20}, Ljava/lang/Integer;->intValue()I

    move-result v4

    if-eqz v4, :cond_10

    move-object/from16 v0, p0

    move-object/from16 v1, v35

    move-object/from16 v2, v17

    invoke-direct {v0, v1, v2}, Lcom/google/android/syncadapters/calendar/EventHandler;->addRemindersToEntry(Ljava/util/ArrayList;Lcom/google/api/services/calendar/model/Event;)V

    :cond_10
    const-string v4, "hasExtendedProperties"

    move-object/from16 v0, v38

    invoke-virtual {v0, v4}, Landroid/content/ContentValues;->getAsInteger(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v19

    if-eqz v19, :cond_11

    invoke-virtual/range {v19 .. v19}, Ljava/lang/Integer;->intValue()I

    move-result v4

    if-eqz v4, :cond_11

    move-object/from16 v0, p0

    move-object/from16 v1, v35

    move-object/from16 v2, v17

    invoke-direct {v0, v1, v2}, Lcom/google/android/syncadapters/calendar/EventHandler;->addExtendedPropertiesToEntry(Ljava/util/ArrayList;Lcom/google/api/services/calendar/model/Event;)V

    :cond_11
    const-wide/16 v30, -0x1

    const-string v4, "original_sync_id"

    move-object/from16 v0, v38

    invoke-virtual {v0, v4}, Landroid/content/ContentValues;->getAsString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v29

    const-string v4, "originalInstanceTime"

    move-object/from16 v0, v38

    invoke-virtual {v0, v4}, Landroid/content/ContentValues;->containsKey(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_12

    const-string v4, "originalInstanceTime"

    move-object/from16 v0, v38

    invoke-virtual {v0, v4}, Landroid/content/ContentValues;->getAsLong(Ljava/lang/String;)Ljava/lang/Long;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/Long;->longValue()J

    move-result-wide v30

    :cond_12
    const-wide/16 v4, -0x1

    cmp-long v4, v30, v4

    if-eqz v4, :cond_15

    invoke-static/range {v29 .. v29}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-nez v4, :cond_15

    const/4 v4, 0x1

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    const-string v5, "originalAllDay"

    move-object/from16 v0, v38

    invoke-virtual {v0, v5}, Landroid/content/ContentValues;->getAsInteger(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/Integer;->equals(Ljava/lang/Object;)Z

    move-result v28

    new-instance v32, Landroid/text/format/Time;

    move-object/from16 v0, v32

    move-object/from16 v1, v36

    invoke-direct {v0, v1}, Landroid/text/format/Time;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, v32

    move-wide/from16 v1, v30

    invoke-virtual {v0, v1, v2}, Landroid/text/format/Time;->set(J)V

    move-wide/from16 v0, v30

    move/from16 v2, v28

    invoke-static {v0, v1, v2}, Lcom/google/android/syncadapters/calendar/EventHandler;->createEventDateTime(JZ)Lcom/google/api/services/calendar/model/EventDateTime;

    move-result-object v4

    move-object/from16 v0, v17

    invoke-virtual {v0, v4}, Lcom/google/api/services/calendar/model/Event;->setOriginalStartTime(Lcom/google/api/services/calendar/model/EventDateTime;)Lcom/google/api/services/calendar/model/Event;

    invoke-static/range {v29 .. v29}, Lcom/google/android/syncadapters/calendar/Utils;->extractEventId(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    move-object/from16 v0, v17

    invoke-virtual {v0, v4}, Lcom/google/api/services/calendar/model/Event;->setRecurringEventId(Ljava/lang/String;)Lcom/google/api/services/calendar/model/Event;

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/syncadapters/calendar/EventHandler;->mProvider:Landroid/content/ContentProviderClient;

    sget-object v5, Landroid/provider/CalendarContract$Events;->CONTENT_URI:Landroid/net/Uri;

    const/4 v6, 0x2

    new-array v6, v6, [Ljava/lang/String;

    const/4 v7, 0x0

    const-string v8, "sync_data2"

    aput-object v8, v6, v7

    const/4 v7, 0x1

    const-string v8, "hasAttendeeData"

    aput-object v8, v6, v7

    const-string v7, "_sync_id=?"

    const/4 v8, 0x1

    new-array v8, v8, [Ljava/lang/String;

    const/4 v9, 0x0

    aput-object v29, v8, v9

    const/4 v9, 0x0

    invoke-virtual/range {v4 .. v9}, Landroid/content/ContentProviderClient;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v11

    if-eqz v11, :cond_15

    :try_start_0
    invoke-interface {v11}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v4

    if-eqz v4, :cond_14

    const/4 v4, 0x0

    invoke-interface {v11, v4}, Landroid/database/Cursor;->getInt(I)I

    move-result v33

    if-eqz v33, :cond_13

    invoke-static/range {v33 .. v33}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    move-object/from16 v0, v17

    invoke-virtual {v0, v4}, Lcom/google/api/services/calendar/model/Event;->setSequence(Ljava/lang/Integer;)Lcom/google/api/services/calendar/model/Event;

    :cond_13
    const/4 v4, 0x1

    invoke-interface {v11, v4}, Landroid/database/Cursor;->getInt(I)I

    move-result v18

    if-nez v18, :cond_14

    const/4 v4, 0x1

    invoke-static {v4}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v4

    move-object/from16 v0, v17

    invoke-virtual {v0, v4}, Lcom/google/api/services/calendar/model/Event;->setAttendeesOmitted(Ljava/lang/Boolean;)Lcom/google/api/services/calendar/model/Event;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :cond_14
    invoke-interface {v11}, Landroid/database/Cursor;->close()V

    :cond_15
    const-string v4, "guestsCanInviteOthers"

    move-object/from16 v0, v38

    invoke-virtual {v0, v4}, Landroid/content/ContentValues;->containsKey(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_16

    const-string v4, "guestsCanInviteOthers"

    move-object/from16 v0, v38

    invoke-virtual {v0, v4}, Landroid/content/ContentValues;->getAsInteger(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/Integer;->intValue()I

    move-result v4

    if-eqz v4, :cond_1c

    const/4 v4, 0x1

    :goto_5
    invoke-static {v4}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v4

    move-object/from16 v0, v17

    invoke-virtual {v0, v4}, Lcom/google/api/services/calendar/model/Event;->setGuestsCanInviteOthers(Ljava/lang/Boolean;)Lcom/google/api/services/calendar/model/Event;

    :cond_16
    const-string v4, "guestsCanModify"

    move-object/from16 v0, v38

    invoke-virtual {v0, v4}, Landroid/content/ContentValues;->containsKey(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_17

    const-string v4, "guestsCanModify"

    move-object/from16 v0, v38

    invoke-virtual {v0, v4}, Landroid/content/ContentValues;->getAsInteger(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/Integer;->intValue()I

    move-result v4

    if-eqz v4, :cond_1d

    const/4 v4, 0x1

    :goto_6
    invoke-static {v4}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v4

    move-object/from16 v0, v17

    invoke-virtual {v0, v4}, Lcom/google/api/services/calendar/model/Event;->setGuestsCanModify(Ljava/lang/Boolean;)Lcom/google/api/services/calendar/model/Event;

    :cond_17
    const-string v4, "guestsCanSeeGuests"

    move-object/from16 v0, v38

    invoke-virtual {v0, v4}, Landroid/content/ContentValues;->containsKey(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_18

    const-string v4, "guestsCanSeeGuests"

    move-object/from16 v0, v38

    invoke-virtual {v0, v4}, Landroid/content/ContentValues;->getAsInteger(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/Integer;->intValue()I

    move-result v4

    if-eqz v4, :cond_1e

    const/4 v4, 0x1

    :goto_7
    invoke-static {v4}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v4

    move-object/from16 v0, v17

    invoke-virtual {v0, v4}, Lcom/google/api/services/calendar/model/Event;->setGuestsCanSeeOtherGuests(Ljava/lang/Boolean;)Lcom/google/api/services/calendar/model/Event;

    :cond_18
    const-string v4, "organizer"

    move-object/from16 v0, v38

    invoke-virtual {v0, v4}, Landroid/content/ContentValues;->containsKey(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_19

    new-instance v27, Lcom/google/api/services/calendar/model/Event$Organizer;

    invoke-direct/range {v27 .. v27}, Lcom/google/api/services/calendar/model/Event$Organizer;-><init>()V

    const-string v4, "organizer"

    move-object/from16 v0, v38

    invoke-virtual {v0, v4}, Landroid/content/ContentValues;->getAsString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    move-object/from16 v0, v27

    invoke-virtual {v0, v4}, Lcom/google/api/services/calendar/model/Event$Organizer;->setEmail(Ljava/lang/String;)Lcom/google/api/services/calendar/model/Event$Organizer;

    move-object/from16 v0, v17

    move-object/from16 v1, v27

    invoke-virtual {v0, v1}, Lcom/google/api/services/calendar/model/Event;->setOrganizer(Lcom/google/api/services/calendar/model/Event$Organizer;)Lcom/google/api/services/calendar/model/Event;

    :cond_19
    return-object v17

    :pswitch_0
    const-string v34, "cancelled"

    goto/16 :goto_0

    :pswitch_1
    const-string v34, "confirmed"

    goto/16 :goto_0

    :pswitch_2
    const-string v34, "tentative"

    goto/16 :goto_0

    :pswitch_3
    const-string v39, "default"

    goto/16 :goto_1

    :pswitch_4
    const-string v39, "confidential"

    goto/16 :goto_1

    :pswitch_5
    const-string v39, "private"

    goto/16 :goto_1

    :pswitch_6
    const-string v39, "public"

    goto/16 :goto_1

    :pswitch_7
    const-string v37, "opaque"

    goto/16 :goto_2

    :pswitch_8
    const-string v37, "transparent"

    goto/16 :goto_2

    :cond_1a
    const/16 v23, 0x0

    goto/16 :goto_3

    :cond_1b
    const/16 v21, 0x1

    goto/16 :goto_4

    :catchall_0
    move-exception v4

    invoke-interface {v11}, Landroid/database/Cursor;->close()V

    throw v4

    :cond_1c
    const/4 v4, 0x0

    goto/16 :goto_5

    :cond_1d
    const/4 v4, 0x0

    goto/16 :goto_6

    :cond_1e
    const/4 v4, 0x0

    goto :goto_7

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch

    :pswitch_data_1
    .packed-switch 0x0
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
    .end packed-switch

    :pswitch_data_2
    .packed-switch 0x0
        :pswitch_7
        :pswitch_8
    .end packed-switch
.end method

.method protected entryToContentValues(Lcom/google/api/services/calendar/model/Event;Landroid/content/ContentValues;Lcom/google/android/syncadapters/calendar/CalendarSyncInfo;J)I
    .locals 23
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;,
            Lcom/google/android/apiary/ParseException;
        }
    .end annotation

    invoke-virtual/range {p2 .. p2}, Landroid/content/ContentValues;->clear()V

    invoke-virtual/range {p1 .. p1}, Lcom/google/api/services/calendar/model/Event;->getId()Ljava/lang/String;

    move-result-object v3

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/syncadapters/calendar/EventHandler;->mCalendarId:Ljava/lang/String;

    invoke-static {v3, v4}, Lcom/google/android/syncadapters/calendar/Utils;->createEventId(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v15

    const-string v3, "sync_data4"

    invoke-virtual/range {p1 .. p1}, Lcom/google/api/services/calendar/model/Event;->getEtag()Ljava/lang/String;

    move-result-object v4

    move-object/from16 v0, p2

    invoke-virtual {v0, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    const-string v3, "_sync_id"

    move-object/from16 v0, p2

    invoke-virtual {v0, v3, v15}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    const-string v3, "sync_data1"

    invoke-virtual/range {p1 .. p1}, Lcom/google/api/services/calendar/model/Event;->getICalUID()Ljava/lang/String;

    move-result-object v4

    move-object/from16 v0, p2

    invoke-virtual {v0, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual/range {p1 .. p1}, Lcom/google/api/services/calendar/model/Event;->getSequence()Ljava/lang/Integer;

    move-result-object v3

    if-eqz v3, :cond_0

    const-string v4, "sync_data2"

    move-object/from16 v0, p2

    invoke-virtual {v0, v4, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    :cond_0
    new-instance v8, Landroid/text/format/Time;

    const-string v3, "UTC"

    invoke-direct {v8, v3}, Landroid/text/format/Time;-><init>(Ljava/lang/String;)V

    invoke-virtual/range {p1 .. p1}, Lcom/google/api/services/calendar/model/Event;->getStatus()Ljava/lang/String;

    move-result-object v7

    invoke-virtual/range {p1 .. p1}, Lcom/google/api/services/calendar/model/Event;->getRecurringEventId()Ljava/lang/String;

    move-result-object v3

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/syncadapters/calendar/EventHandler;->mCalendarId:Ljava/lang/String;

    invoke-static {v3, v4}, Lcom/google/android/syncadapters/calendar/Utils;->createEventId(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v9

    invoke-virtual/range {p1 .. p1}, Lcom/google/api/services/calendar/model/Event;->getOriginalStartTime()Lcom/google/api/services/calendar/model/EventDateTime;

    move-result-object v4

    const/4 v3, 0x0

    invoke-static {v9}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v5

    if-nez v5, :cond_36

    if-eqz v4, :cond_36

    const/4 v6, 0x1

    invoke-virtual {v4}, Lcom/google/api/services/calendar/model/EventDateTime;->getDate()Ljava/lang/String;

    move-result-object v3

    if-eqz v3, :cond_5

    invoke-virtual {v8, v3}, Landroid/text/format/Time;->parse3339(Ljava/lang/String;)Z

    const/4 v3, 0x0

    invoke-virtual {v8, v3}, Landroid/text/format/Time;->toMillis(Z)J

    move-result-wide v4

    const/4 v3, 0x1

    :goto_0
    const-string v10, "original_sync_id"

    move-object/from16 v0, p2

    invoke-virtual {v0, v10, v9}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    const-string v9, "originalInstanceTime"

    const-wide/16 v10, 0x3e8

    div-long v10, v4, v10

    const-wide/16 v12, 0x3e8

    mul-long/2addr v10, v12

    invoke-static {v10, v11}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v10

    move-object/from16 v0, p2

    invoke-virtual {v0, v9, v10}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    const-string v9, "originalAllDay"

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    move-object/from16 v0, p2

    invoke-virtual {v0, v9, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    sget-boolean v3, Lcom/google/android/syncadapters/calendar/Utils;->JELLY_BEAN_OR_HIGHER:Z

    if-nez v3, :cond_1

    const-string v3, "cancelled"

    invoke-virtual {v3, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_1

    const-string v3, "dtstart"

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v9

    move-object/from16 v0, p2

    invoke-virtual {v0, v3, v9}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    const-string v3, "dtend"

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    move-object/from16 v0, p2

    invoke-virtual {v0, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    const-string v3, "eventTimezone"

    const-string v4, "UTC"

    move-object/from16 v0, p2

    invoke-virtual {v0, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    :cond_1
    move v4, v6

    :goto_1
    if-eqz v7, :cond_2

    const-string v3, "confirmed"

    invoke-virtual {v3, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_6

    :cond_2
    const/4 v3, 0x1

    :goto_2
    const-string v5, "eventStatus"

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    move-object/from16 v0, p2

    invoke-virtual {v0, v5, v6}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    invoke-virtual/range {p1 .. p1}, Lcom/google/api/services/calendar/model/Event;->getUpdated()Lcom/google/api/client/util/DateTime;

    move-result-object v5

    if-eqz v5, :cond_3

    invoke-virtual {v5}, Lcom/google/api/client/util/DateTime;->getValue()J

    move-result-wide v5

    invoke-virtual {v8, v5, v6}, Landroid/text/format/Time;->set(J)V

    const-string v5, "sync_data5"

    const/4 v6, 0x0

    invoke-virtual {v8, v6}, Landroid/text/format/Time;->format3339(Z)Ljava/lang/String;

    move-result-object v6

    move-object/from16 v0, p2

    invoke-virtual {v0, v5, v6}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    :cond_3
    if-eqz p3, :cond_4

    const-string v5, "calendar_id"

    move-object/from16 v0, p3

    iget-wide v6, v0, Lcom/google/android/syncadapters/calendar/CalendarSyncInfo;->calendarId:J

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v6

    move-object/from16 v0, p2

    invoke-virtual {v0, v5, v6}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    :cond_4
    if-eqz v4, :cond_a

    const/4 v4, 0x2

    if-ne v3, v4, :cond_a

    const/4 v3, 0x0

    :goto_3
    return v3

    :cond_5
    invoke-virtual {v4}, Lcom/google/api/services/calendar/model/EventDateTime;->getDateTime()Lcom/google/api/client/util/DateTime;

    move-result-object v3

    invoke-virtual {v3}, Lcom/google/api/client/util/DateTime;->getValue()J

    move-result-wide v4

    const/4 v3, 0x0

    goto/16 :goto_0

    :cond_6
    const-string v3, "cancelled"

    invoke-virtual {v3, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_8

    if-nez v4, :cond_7

    const/4 v3, 0x1

    goto :goto_3

    :cond_7
    const/4 v3, 0x2

    goto :goto_2

    :cond_8
    const-string v3, "tentative"

    invoke-virtual {v3, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_9

    const/4 v3, 0x0

    goto :goto_2

    :cond_9
    const-string v3, "EventHandler"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Invalid status: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v3, 0x2

    goto :goto_3

    :cond_a
    const-string v3, "dirty"

    const/4 v4, 0x0

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    move-object/from16 v0, p2

    invoke-virtual {v0, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    invoke-virtual/range {p1 .. p1}, Lcom/google/api/services/calendar/model/Event;->getStart()Lcom/google/api/services/calendar/model/EventDateTime;

    move-result-object v4

    invoke-virtual {v4}, Lcom/google/api/services/calendar/model/EventDateTime;->getDate()Ljava/lang/String;

    move-result-object v5

    if-eqz v5, :cond_15

    const-string v3, "eventTimezone"

    const-string v4, "UTC"

    move-object/from16 v0, p2

    invoke-virtual {v0, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    const/4 v3, 0x1

    invoke-virtual {v8, v5}, Landroid/text/format/Time;->parse3339(Ljava/lang/String;)Z

    const/4 v4, 0x0

    invoke-virtual {v8, v4}, Landroid/text/format/Time;->toMillis(Z)J

    move-result-wide v6

    invoke-virtual/range {p1 .. p1}, Lcom/google/api/services/calendar/model/Event;->getEnd()Lcom/google/api/services/calendar/model/EventDateTime;

    move-result-object v4

    invoke-virtual {v4}, Lcom/google/api/services/calendar/model/EventDateTime;->getDate()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v8, v4}, Landroid/text/format/Time;->parse3339(Ljava/lang/String;)Z

    const/4 v4, 0x0

    invoke-virtual {v8, v4}, Landroid/text/format/Time;->toMillis(Z)J

    move-result-wide v4

    move-wide v9, v6

    move-wide v7, v4

    move v6, v3

    :goto_4
    const-string v3, "allDay"

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    move-object/from16 v0, p2

    invoke-virtual {v0, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    const-string v3, "dtstart"

    const-wide/16 v4, 0x3e8

    div-long v4, v9, v4

    const-wide/16 v11, 0x3e8

    mul-long/2addr v4, v11

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    move-object/from16 v0, p2

    invoke-virtual {v0, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    invoke-virtual/range {p1 .. p1}, Lcom/google/api/services/calendar/model/Event;->getVisibility()Ljava/lang/String;

    move-result-object v3

    if-eqz v3, :cond_b

    const-string v4, "default"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_18

    :cond_b
    const-string v3, "accessLevel"

    const/4 v4, 0x0

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    move-object/from16 v0, p2

    invoke-virtual {v0, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    :goto_5
    invoke-virtual/range {p1 .. p1}, Lcom/google/api/services/calendar/model/Event;->getTransparency()Ljava/lang/String;

    move-result-object v3

    if-eqz v3, :cond_c

    const-string v4, "opaque"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_1c

    :cond_c
    const-string v3, "availability"

    const/4 v4, 0x0

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    move-object/from16 v0, p2

    invoke-virtual {v0, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    :goto_6
    invoke-virtual/range {p1 .. p1}, Lcom/google/api/services/calendar/model/Event;->getSummary()Ljava/lang/String;

    move-result-object v3

    if-nez v3, :cond_d

    const-string v3, ""

    :cond_d
    const-string v4, "title"

    move-object/from16 v0, p2

    invoke-virtual {v0, v4, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual/range {p1 .. p1}, Lcom/google/api/services/calendar/model/Event;->getDescription()Ljava/lang/String;

    move-result-object v3

    if-nez v3, :cond_e

    const-string v3, ""

    :cond_e
    const-string v4, "description"

    move-object/from16 v0, p2

    invoke-virtual {v0, v4, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual/range {p1 .. p1}, Lcom/google/api/services/calendar/model/Event;->getLocation()Ljava/lang/String;

    move-result-object v3

    if-nez v3, :cond_f

    const-string v3, ""

    :cond_f
    const-string v4, "eventLocation"

    move-object/from16 v0, p2

    invoke-virtual {v0, v4, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    sget-boolean v3, Lcom/google/android/syncadapters/calendar/Utils;->JELLY_BEAN_OR_HIGHER:Z

    if-eqz v3, :cond_11

    invoke-virtual/range {p1 .. p1}, Lcom/google/api/services/calendar/model/Event;->getHtmlLink()Ljava/lang/String;

    move-result-object v3

    if-eqz v3, :cond_10

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/syncadapters/calendar/EventHandler;->mEventPlusPattern:Ljava/util/regex/Pattern;

    invoke-virtual {v4, v3}, Ljava/util/regex/Pattern;->matcher(Ljava/lang/CharSequence;)Ljava/util/regex/Matcher;

    move-result-object v4

    invoke-virtual {v4}, Ljava/util/regex/Matcher;->find()Z

    move-result v4

    if-eqz v4, :cond_10

    const-string v4, "customAppUri"

    move-object/from16 v0, p2

    invoke-virtual {v0, v4, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    const-string v3, "customAppPackage"

    const-string v4, "com.google.android.apps.plus"

    move-object/from16 v0, p2

    invoke-virtual {v0, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    :cond_10
    invoke-virtual/range {p1 .. p1}, Lcom/google/api/services/calendar/model/Event;->getColorId()Ljava/lang/String;

    move-result-object v3

    if-eqz v3, :cond_11

    const-string v4, "eventColor_index"

    move-object/from16 v0, p2

    invoke-virtual {v0, v4, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    :cond_11
    invoke-virtual/range {p1 .. p1}, Lcom/google/api/services/calendar/model/Event;->getReminders()Lcom/google/api/services/calendar/model/Event$Reminders;

    move-result-object v3

    if-eqz v3, :cond_12

    invoke-virtual {v3}, Lcom/google/api/services/calendar/model/Event$Reminders;->getUseDefault()Ljava/lang/Boolean;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v4

    if-eqz v4, :cond_1f

    if-nez p3, :cond_1e

    const/4 v3, 0x0

    :goto_7
    if-eqz v3, :cond_12

    invoke-interface {v3}, Ljava/util/List;->isEmpty()Z

    move-result v3

    if-nez v3, :cond_12

    const-string v3, "hasAlarm"

    const/4 v4, 0x1

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    move-object/from16 v0, p2

    invoke-virtual {v0, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    :cond_12
    invoke-virtual/range {p1 .. p1}, Lcom/google/api/services/calendar/model/Event;->getExtendedProperties()Lcom/google/api/services/calendar/model/Event$ExtendedProperties;

    move-result-object v5

    const/4 v4, 0x0

    const/4 v3, 0x1

    if-eqz v5, :cond_13

    invoke-virtual {v5}, Lcom/google/api/services/calendar/model/Event$ExtendedProperties;->getPrivate()Ljava/util/Map;

    move-result-object v11

    invoke-virtual {v5}, Lcom/google/api/services/calendar/model/Event$ExtendedProperties;->getShared()Ljava/util/Map;

    move-result-object v5

    if-eqz v5, :cond_20

    invoke-interface {v5}, Ljava/util/Map;->isEmpty()Z

    move-result v5

    if-nez v5, :cond_20

    const/4 v4, 0x1

    :cond_13
    :goto_8
    const-string v3, "hasExtendedProperties"

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    move-object/from16 v0, p2

    invoke-virtual {v0, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    const-string v3, "hasAlarm"

    const/4 v4, 0x1

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    move-object/from16 v0, p2

    invoke-virtual {v0, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    const-string v4, "hasAttendeeData"

    invoke-virtual/range {p1 .. p1}, Lcom/google/api/services/calendar/model/Event;->getAttendeesOmitted()Ljava/lang/Boolean;

    move-result-object v3

    const/4 v5, 0x0

    invoke-static {v3, v5}, Lcom/google/android/syncadapters/calendar/Utils;->getBooleanValue(Ljava/lang/Boolean;Z)Z

    move-result v3

    if-eqz v3, :cond_23

    const/4 v3, 0x0

    :goto_9
    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    move-object/from16 v0, p2

    invoke-virtual {v0, v4, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    invoke-virtual/range {p1 .. p1}, Lcom/google/api/services/calendar/model/Event;->getRecurrence()Ljava/util/List;

    move-result-object v16

    const/4 v14, 0x0

    const/4 v13, 0x0

    const/4 v12, 0x0

    const/4 v11, 0x0

    if-eqz v16, :cond_2f

    invoke-interface/range {v16 .. v16}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v17

    :cond_14
    :goto_a
    invoke-interface/range {v17 .. v17}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_2a

    invoke-interface/range {v17 .. v17}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    move-object v5, v3

    check-cast v5, Ljava/lang/String;

    if-nez v5, :cond_24

    const-string v3, "EventHandler"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Invalid null recurrence line in event "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_a

    :cond_15
    const/4 v3, 0x0

    invoke-virtual {v4}, Lcom/google/api/services/calendar/model/EventDateTime;->getTimeZone()Ljava/lang/String;

    move-result-object v5

    if-eqz v5, :cond_16

    const-string v6, "eventTimezone"

    move-object/from16 v0, p2

    invoke-virtual {v0, v6, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    :goto_b
    invoke-virtual {v4}, Lcom/google/api/services/calendar/model/EventDateTime;->getDateTime()Lcom/google/api/client/util/DateTime;

    move-result-object v4

    invoke-virtual {v4}, Lcom/google/api/client/util/DateTime;->getValue()J

    move-result-wide v6

    invoke-virtual/range {p1 .. p1}, Lcom/google/api/services/calendar/model/Event;->getEnd()Lcom/google/api/services/calendar/model/EventDateTime;

    move-result-object v4

    invoke-virtual {v4}, Lcom/google/api/services/calendar/model/EventDateTime;->getDateTime()Lcom/google/api/client/util/DateTime;

    move-result-object v4

    invoke-virtual {v4}, Lcom/google/api/client/util/DateTime;->getValue()J

    move-result-wide v4

    move-wide v9, v6

    move-wide v7, v4

    move v6, v3

    goto/16 :goto_4

    :cond_16
    if-eqz p3, :cond_17

    const-string v5, "eventTimezone"

    move-object/from16 v0, p3

    iget-object v6, v0, Lcom/google/android/syncadapters/calendar/CalendarSyncInfo;->calendarTimezone:Ljava/lang/String;

    move-object/from16 v0, p2

    invoke-virtual {v0, v5, v6}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_b

    :cond_17
    const-string v5, "eventTimezone"

    invoke-static {}, Ljava/util/TimeZone;->getDefault()Ljava/util/TimeZone;

    move-result-object v6

    invoke-virtual {v6}, Ljava/util/TimeZone;->getID()Ljava/lang/String;

    move-result-object v6

    move-object/from16 v0, p2

    invoke-virtual {v0, v5, v6}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_b

    :cond_18
    const-string v4, "confidential"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_19

    const-string v3, "accessLevel"

    const/4 v4, 0x1

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    move-object/from16 v0, p2

    invoke-virtual {v0, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    goto/16 :goto_5

    :cond_19
    const-string v4, "private"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_1a

    const-string v3, "accessLevel"

    const/4 v4, 0x2

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    move-object/from16 v0, p2

    invoke-virtual {v0, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    goto/16 :goto_5

    :cond_1a
    const-string v4, "public"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_1b

    const-string v3, "accessLevel"

    const/4 v4, 0x3

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    move-object/from16 v0, p2

    invoke-virtual {v0, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    goto/16 :goto_5

    :cond_1b
    const-string v4, "EventHandler"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "Unexpected visibility: "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v4, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v3, 0x2

    goto/16 :goto_3

    :cond_1c
    const-string v4, "transparent"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_1d

    const-string v3, "availability"

    const/4 v4, 0x1

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    move-object/from16 v0, p2

    invoke-virtual {v0, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    goto/16 :goto_6

    :cond_1d
    const-string v4, "EventHandler"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "Unexpected transparency: "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v4, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v3, 0x2

    goto/16 :goto_3

    :cond_1e
    move-object/from16 v0, p3

    iget-object v3, v0, Lcom/google/android/syncadapters/calendar/CalendarSyncInfo;->defaultReminders:Ljava/util/List;

    goto/16 :goto_7

    :cond_1f
    invoke-virtual {v3}, Lcom/google/api/services/calendar/model/Event$Reminders;->getOverrides()Ljava/util/List;

    move-result-object v3

    goto/16 :goto_7

    :cond_20
    if-eqz v11, :cond_13

    invoke-interface {v11}, Ljava/util/Map;->keySet()Ljava/util/Set;

    move-result-object v5

    invoke-interface {v5}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v11

    move v5, v4

    move v4, v3

    :goto_c
    invoke-interface {v11}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_35

    invoke-interface {v11}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    const-string v12, "alarmReminder"

    invoke-virtual {v3, v12}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_22

    const/4 v3, 0x1

    move v4, v5

    :goto_d
    if-eqz v3, :cond_21

    const/4 v5, 0x1

    if-eq v4, v5, :cond_13

    :cond_21
    move v5, v4

    move v4, v3

    goto :goto_c

    :cond_22
    const/4 v5, 0x1

    move v3, v4

    move v4, v5

    goto :goto_d

    :cond_23
    const/4 v3, 0x1

    goto/16 :goto_9

    :cond_24
    :try_start_0
    new-instance v3, Lcom/android/calendarcommon2/ICalendar$Component;

    const-string v4, "DUMMY"

    const/16 v18, 0x0

    move-object/from16 v0, v18

    invoke-direct {v3, v4, v0}, Lcom/android/calendarcommon2/ICalendar$Component;-><init>(Ljava/lang/String;Lcom/android/calendarcommon2/ICalendar$Component;)V

    invoke-static {v3, v5}, Lcom/android/calendarcommon2/ICalendar;->parseComponent(Lcom/android/calendarcommon2/ICalendar$Component;Ljava/lang/String;)Lcom/android/calendarcommon2/ICalendar$Component;
    :try_end_0
    .catch Lcom/android/calendarcommon2/ICalendar$FormatException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v18

    invoke-virtual/range {v18 .. v18}, Lcom/android/calendarcommon2/ICalendar$Component;->getPropertyNames()Ljava/util/Set;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v19

    :cond_25
    invoke-interface/range {v19 .. v19}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_14

    invoke-interface/range {v19 .. v19}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    move-object v4, v3

    check-cast v4, Ljava/lang/String;

    move-object/from16 v0, v18

    invoke-virtual {v0, v4}, Lcom/android/calendarcommon2/ICalendar$Component;->getProperties(Ljava/lang/String;)Ljava/util/List;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v20

    :goto_e
    invoke-interface/range {v20 .. v20}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_25

    invoke-interface/range {v20 .. v20}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/android/calendarcommon2/ICalendar$Property;

    const-string v21, "RRULE"

    move-object/from16 v0, v21

    invoke-virtual {v4, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v21

    if-eqz v21, :cond_26

    :try_start_1
    invoke-virtual {v3}, Lcom/android/calendarcommon2/ICalendar$Property;->getValue()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lcom/google/android/syncadapters/calendar/Utils;->sanitizeRecurrence(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-static {v14, v3}, Lcom/google/android/syncadapters/calendar/EventHandler;->appendRecurrenceString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    :try_end_1
    .catch Lcom/android/calendarcommon2/EventRecurrence$InvalidFormatException; {:try_start_1 .. :try_end_1} :catch_1

    move-result-object v3

    move-object/from16 v22, v11

    move-object v11, v12

    move-object v12, v13

    move-object v13, v3

    move-object/from16 v3, v22

    :goto_f
    move-object v14, v13

    move-object v13, v12

    move-object v12, v11

    move-object v11, v3

    goto :goto_e

    :catch_0
    move-exception v3

    const-string v4, "EventHandler"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "Invalid recurrence line in event "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, ": "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5, v3}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    const/4 v3, 0x2

    goto/16 :goto_3

    :catch_1
    move-exception v3

    const-string v3, "EventHandler"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "Invalid recurrence line in event "

    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v6, ": "

    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v3, 0x2

    goto/16 :goto_3

    :cond_26
    const-string v21, "RDATE"

    move-object/from16 v0, v21

    invoke-virtual {v4, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v21

    if-eqz v21, :cond_27

    invoke-static {v3}, Lcom/google/android/syncadapters/calendar/EventHandler;->getRecurrenceDate(Lcom/android/calendarcommon2/ICalendar$Property;)Ljava/lang/String;

    move-result-object v3

    invoke-static {v13, v3}, Lcom/google/android/syncadapters/calendar/EventHandler;->appendRecurrenceString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    move-object v13, v14

    move-object/from16 v22, v12

    move-object v12, v3

    move-object v3, v11

    move-object/from16 v11, v22

    goto :goto_f

    :cond_27
    const-string v21, "EXRULE"

    move-object/from16 v0, v21

    invoke-virtual {v4, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v21

    if-eqz v21, :cond_28

    :try_start_2
    invoke-virtual {v3}, Lcom/android/calendarcommon2/ICalendar$Property;->getValue()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lcom/google/android/syncadapters/calendar/Utils;->sanitizeRecurrence(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-static {v12, v3}, Lcom/google/android/syncadapters/calendar/EventHandler;->appendRecurrenceString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    :try_end_2
    .catch Lcom/android/calendarcommon2/EventRecurrence$InvalidFormatException; {:try_start_2 .. :try_end_2} :catch_2

    move-result-object v3

    move-object v12, v13

    move-object v13, v14

    move-object/from16 v22, v11

    move-object v11, v3

    move-object/from16 v3, v22

    goto/16 :goto_f

    :catch_2
    move-exception v3

    const-string v3, "EventHandler"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "Invalid recurrence line in event "

    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v6, ": "

    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v3, 0x2

    goto/16 :goto_3

    :cond_28
    const-string v21, "EXDATE"

    move-object/from16 v0, v21

    invoke-virtual {v4, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v21

    if-eqz v21, :cond_29

    invoke-static {v3}, Lcom/google/android/syncadapters/calendar/EventHandler;->getRecurrenceDate(Lcom/android/calendarcommon2/ICalendar$Property;)Ljava/lang/String;

    move-result-object v3

    invoke-static {v11, v3}, Lcom/google/android/syncadapters/calendar/EventHandler;->appendRecurrenceString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    move-object v11, v12

    move-object v12, v13

    move-object v13, v14

    goto/16 :goto_f

    :cond_29
    const-string v3, "EventHandler"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "Invalid recurrence line in event "

    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v6, ": "

    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v3, 0x2

    goto/16 :goto_3

    :cond_2a
    :try_start_3
    new-instance v3, Lcom/android/calendarcommon2/RecurrenceSet;

    invoke-direct {v3, v14, v13, v12, v11}, Lcom/android/calendarcommon2/RecurrenceSet;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    :try_end_3
    .catch Lcom/android/calendarcommon2/EventRecurrence$InvalidFormatException; {:try_start_3 .. :try_end_3} :catch_3

    sub-long v3, v7, v9

    const-wide/16 v7, 0x3e8

    div-long/2addr v3, v7

    if-nez v6, :cond_2e

    const-string v5, "duration"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "P"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v3, v4}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "S"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    move-object/from16 v0, p2

    invoke-virtual {v0, v5, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    :goto_10
    const-string v3, "rrule"

    move-object/from16 v0, p2

    invoke-virtual {v0, v3, v14}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    const-string v3, "rdate"

    move-object/from16 v0, p2

    invoke-virtual {v0, v3, v13}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    const-string v3, "exrule"

    move-object/from16 v0, p2

    invoke-virtual {v0, v3, v12}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    const-string v3, "exdate"

    move-object/from16 v0, p2

    invoke-virtual {v0, v3, v11}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    const-string v4, "guestsCanInviteOthers"

    invoke-virtual/range {p1 .. p1}, Lcom/google/api/services/calendar/model/Event;->getGuestsCanInviteOthers()Ljava/lang/Boolean;

    move-result-object v3

    const/4 v5, 0x1

    invoke-static {v3, v5}, Lcom/google/android/syncadapters/calendar/Utils;->getBooleanValue(Ljava/lang/Boolean;Z)Z

    move-result v3

    if-eqz v3, :cond_30

    const/4 v3, 0x1

    :goto_11
    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    move-object/from16 v0, p2

    invoke-virtual {v0, v4, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    const-string v4, "guestsCanModify"

    invoke-virtual/range {p1 .. p1}, Lcom/google/api/services/calendar/model/Event;->getGuestsCanModify()Ljava/lang/Boolean;

    move-result-object v3

    const/4 v5, 0x0

    invoke-static {v3, v5}, Lcom/google/android/syncadapters/calendar/Utils;->getBooleanValue(Ljava/lang/Boolean;Z)Z

    move-result v3

    if-eqz v3, :cond_31

    const/4 v3, 0x1

    :goto_12
    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    move-object/from16 v0, p2

    invoke-virtual {v0, v4, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    const-string v4, "guestsCanSeeGuests"

    invoke-virtual/range {p1 .. p1}, Lcom/google/api/services/calendar/model/Event;->getGuestsCanSeeOtherGuests()Ljava/lang/Boolean;

    move-result-object v3

    const/4 v5, 0x1

    invoke-static {v3, v5}, Lcom/google/android/syncadapters/calendar/Utils;->getBooleanValue(Ljava/lang/Boolean;Z)Z

    move-result v3

    if-eqz v3, :cond_32

    const/4 v3, 0x1

    :goto_13
    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    move-object/from16 v0, p2

    invoke-virtual {v0, v4, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    invoke-virtual/range {p1 .. p1}, Lcom/google/api/services/calendar/model/Event;->getOrganizer()Lcom/google/api/services/calendar/model/Event$Organizer;

    move-result-object v3

    if-eqz v3, :cond_2d

    const/4 v4, 0x0

    invoke-virtual {v3}, Lcom/google/api/services/calendar/model/Event$Organizer;->getSelf()Ljava/lang/Boolean;

    move-result-object v5

    const/4 v6, 0x0

    invoke-static {v5, v6}, Lcom/google/android/syncadapters/calendar/Utils;->getBooleanValue(Ljava/lang/Boolean;Z)Z

    move-result v5

    if-eqz v5, :cond_2b

    const-wide/16 v5, 0x0

    cmp-long v5, p4, v5

    if-gez v5, :cond_33

    const-string v5, "EventHandler"

    const-string v6, "Missing calendarId, can\'t get owner"

    new-instance v7, Ljava/lang/Throwable;

    invoke-direct {v7}, Ljava/lang/Throwable;-><init>()V

    invoke-static {v5, v6, v7}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    :cond_2b
    :goto_14
    if-nez v4, :cond_2c

    invoke-virtual {v3}, Lcom/google/api/services/calendar/model/Event$Organizer;->getEmail()Ljava/lang/String;

    move-result-object v4

    :cond_2c
    if-nez v4, :cond_34

    const-string v5, "id"

    invoke-virtual {v3, v5}, Lcom/google/api/services/calendar/model/Event$Organizer;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    if-eqz v3, :cond_34

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "@profile.calendar.google.com"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    :goto_15
    const-string v4, "organizer"

    move-object/from16 v0, p2

    invoke-virtual {v0, v4, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    :cond_2d
    const/4 v3, 0x0

    goto/16 :goto_3

    :catch_3
    move-exception v3

    const-string v4, "EventHandler"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "Unable to parse recurrence in event "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, ": "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    move-object/from16 v0, v16

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    const/4 v3, 0x2

    goto/16 :goto_3

    :cond_2e
    const-string v5, "duration"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "P"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-wide/32 v7, 0x15180

    div-long/2addr v3, v7

    invoke-virtual {v6, v3, v4}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "D"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    move-object/from16 v0, p2

    invoke-virtual {v0, v5, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_10

    :cond_2f
    const-string v3, "dtend"

    const-wide/16 v4, 0x3e8

    div-long v4, v7, v4

    const-wide/16 v6, 0x3e8

    mul-long/2addr v4, v6

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    move-object/from16 v0, p2

    invoke-virtual {v0, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    goto/16 :goto_10

    :cond_30
    const/4 v3, 0x0

    goto/16 :goto_11

    :cond_31
    const/4 v3, 0x0

    goto/16 :goto_12

    :cond_32
    const/4 v3, 0x0

    goto/16 :goto_13

    :cond_33
    move-object/from16 v0, p0

    move-wide/from16 v1, p4

    invoke-direct {v0, v1, v2}, Lcom/google/android/syncadapters/calendar/EventHandler;->getCalendarOwnerAccount(J)Ljava/lang/String;

    move-result-object v4

    goto/16 :goto_14

    :cond_34
    move-object v3, v4

    goto :goto_15

    :cond_35
    move v4, v5

    goto/16 :goto_8

    :cond_36
    move v4, v3

    goto/16 :goto_1
.end method

.method public getDeletedColumnName()Ljava/lang/String;
    .locals 1

    const-string v0, "deleted"

    return-object v0
.end method

.method public getEntitySelection()Ljava/lang/String;
    .locals 1

    const-string v0, "_sync_id IS NULL OR (_sync_id IS NOT NULL AND (dirty != 0 OR deleted != 0))"

    return-object v0
.end method

.method public getEntityUri()Landroid/net/Uri;
    .locals 2

    sget-object v0, Landroid/provider/CalendarContract$Events;->CONTENT_URI:Landroid/net/Uri;

    iget-object v1, p0, Lcom/google/android/syncadapters/calendar/EventHandler;->mAccount:Landroid/accounts/Account;

    invoke-static {v0, v1}, Lcom/google/android/syncadapters/calendar/HandlerUtils;->addQueryParameters(Landroid/net/Uri;Landroid/accounts/Account;)Landroid/net/Uri;

    move-result-object v0

    return-object v0
.end method

.method public itemToSourceId(Lcom/google/api/services/calendar/model/Event;)Ljava/lang/String;
    .locals 2
    .param p1    # Lcom/google/api/services/calendar/model/Event;

    invoke-virtual {p1}, Lcom/google/api/services/calendar/model/Event;->getId()Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/syncadapters/calendar/EventHandler;->mCalendarId:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/google/android/syncadapters/calendar/Utils;->createEventId(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic itemToSourceId(Ljava/lang/Object;)Ljava/lang/String;
    .locals 1
    .param p1    # Ljava/lang/Object;

    check-cast p1, Lcom/google/api/services/calendar/model/Event;

    invoke-virtual {p0, p1}, Lcom/google/android/syncadapters/calendar/EventHandler;->itemToSourceId(Lcom/google/api/services/calendar/model/Event;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public newEntityIterator(Ljava/lang/String;[Ljava/lang/String;)Landroid/content/EntityIterator;
    .locals 6
    .param p1    # Ljava/lang/String;
    .param p2    # [Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;,
            Lcom/google/android/apiary/ParseException;
        }
    .end annotation

    const/4 v2, 0x0

    sget-object v0, Landroid/provider/CalendarContract$EventsEntity;->CONTENT_URI:Landroid/net/Uri;

    iget-object v3, p0, Lcom/google/android/syncadapters/calendar/EventHandler;->mAccount:Landroid/accounts/Account;

    invoke-static {v0, v3}, Lcom/google/android/syncadapters/SyncAdapterUtils;->addQueryParameters(Landroid/net/Uri;Landroid/accounts/Account;)Landroid/net/Uri;

    move-result-object v1

    iget-object v0, p0, Lcom/google/android/syncadapters/calendar/EventHandler;->mProvider:Landroid/content/ContentProviderClient;

    move-object v3, p1

    move-object v4, p2

    move-object v5, v2

    invoke-static/range {v0 .. v5}, Lcom/google/android/apiary/ProviderHelper;->queryProvider(Landroid/content/ContentProviderClient;Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v0

    iget-object v2, p0, Lcom/google/android/syncadapters/calendar/EventHandler;->mProvider:Landroid/content/ContentProviderClient;

    invoke-static {v0, v2}, Landroid/provider/CalendarContract$EventsEntity;->newEntityIterator(Landroid/database/Cursor;Landroid/content/ContentProviderClient;)Landroid/content/EntityIterator;

    move-result-object v0

    return-object v0
.end method

.method public sendEntityToServer(Landroid/content/Entity;Landroid/content/SyncResult;)Ljava/util/ArrayList;
    .locals 26
    .param p1    # Landroid/content/Entity;
    .param p2    # Landroid/content/SyncResult;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Entity;",
            "Landroid/content/SyncResult;",
            ")",
            "Ljava/util/ArrayList",
            "<",
            "Landroid/content/ContentProviderOperation;",
            ">;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;,
            Ljava/io/IOException;,
            Lcom/google/android/apiary/ParseException;
        }
    .end annotation

    invoke-virtual/range {p1 .. p1}, Landroid/content/Entity;->getEntityValues()Landroid/content/ContentValues;

    move-result-object v25

    const-string v4, "cal_sync1"

    move-object/from16 v0, v25

    invoke-virtual {v0, v4}, Landroid/content/ContentValues;->getAsString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    move-object/from16 v0, p0

    iput-object v4, v0, Lcom/google/android/syncadapters/calendar/EventHandler;->mCalendarId:Ljava/lang/String;

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/syncadapters/calendar/EventHandler;->mCalendarId:Ljava/lang/String;

    if-nez v4, :cond_0

    const-string v4, "EventHandler"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "Entity with no calendar id found: "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    move-object/from16 v0, p1

    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v4, v6}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v3, 0x0

    :goto_0
    return-object v3

    :cond_0
    const-string v4, "local android etag magic value"

    const-string v6, "sync_data4"

    move-object/from16 v0, v25

    invoke-virtual {v0, v6}, Landroid/content/ContentValues;->getAsString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v4, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_2

    const-string v4, "EventHandler"

    const/4 v6, 0x2

    invoke-static {v4, v6}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v4

    if-eqz v4, :cond_1

    const-string v4, "EventHandler"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "Entity with magic ETAG found: "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    move-object/from16 v0, p1

    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v4, v6}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    :cond_1
    const/4 v3, 0x0

    goto :goto_0

    :cond_2
    const-string v4, "_sync_id"

    move-object/from16 v0, v25

    invoke-virtual {v0, v4}, Landroid/content/ContentValues;->getAsString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v22

    invoke-static/range {v22 .. v22}, Lcom/google/android/syncadapters/calendar/Utils;->extractEventId(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v17

    const-string v4, "deleted"

    move-object/from16 v0, v25

    invoke-virtual {v0, v4}, Landroid/content/ContentValues;->getAsInteger(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/Integer;->intValue()I

    move-result v4

    if-eqz v4, :cond_3

    const/16 v19, 0x1

    :goto_1
    if-eqz v19, :cond_4

    if-nez v17, :cond_4

    const-string v4, "EventHandler"

    const-string v6, "Local event deleted. Do nothing"

    invoke-static {v4, v6}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v3, 0x0

    goto :goto_0

    :cond_3
    const/16 v19, 0x0

    goto :goto_1

    :cond_4
    invoke-static {}, Landroid/net/TrafficStats;->getThreadStatsTag()I

    move-result v13

    const/16 v24, 0x0

    const-string v4, "duration"

    move-object/from16 v0, v25

    invoke-virtual {v0, v4}, Landroid/content/ContentValues;->getAsString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    move-object/from16 v0, p0

    invoke-direct {v0, v4}, Lcom/google/android/syncadapters/calendar/EventHandler;->parseDuration(Ljava/lang/String;)J

    move-result-wide v6

    move-object/from16 v0, p0

    move-object/from16 v1, v25

    invoke-direct {v0, v1, v6, v7}, Lcom/google/android/syncadapters/calendar/EventHandler;->setDtendFromDuration(Landroid/content/ContentValues;J)V

    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    if-nez v17, :cond_8

    or-int/lit8 v24, v13, 0x1

    :try_start_0
    invoke-static/range {v24 .. v24}, Landroid/net/TrafficStats;->setThreadStatsTag(I)V

    invoke-virtual/range {p0 .. p1}, Lcom/google/android/syncadapters/calendar/EventHandler;->convertEntityToEntry(Landroid/content/Entity;)Lcom/google/api/services/calendar/model/Event;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v16

    :try_start_1
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/syncadapters/calendar/EventHandler;->mClient:Lcom/google/api/services/calendar/Calendar;

    invoke-virtual {v4}, Lcom/google/api/services/calendar/Calendar;->events()Lcom/google/api/services/calendar/Calendar$Events;

    move-result-object v4

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/google/android/syncadapters/calendar/EventHandler;->mCalendarId:Ljava/lang/String;

    move-object/from16 v0, v16

    invoke-virtual {v4, v6, v0}, Lcom/google/api/services/calendar/Calendar$Events;->insert(Ljava/lang/String;Lcom/google/api/services/calendar/model/Event;)Lcom/google/api/services/calendar/Calendar$Events$Insert;

    move-result-object v4

    const/4 v6, 0x1

    invoke-static {v6}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v6

    invoke-virtual {v4, v6}, Lcom/google/api/services/calendar/Calendar$Events$Insert;->setSendNotifications(Ljava/lang/Boolean;)Lcom/google/api/services/calendar/Calendar$Events$Insert;

    move-result-object v4

    invoke-virtual {v4}, Lcom/google/api/services/calendar/Calendar$Events$Insert;->execute()Lcom/google/api/services/calendar/model/Event;
    :try_end_1
    .catch Lcom/google/api/client/googleapis/json/GoogleJsonResponseException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result-object v8

    :goto_2
    if-eqz v24, :cond_5

    const/4 v4, 0x1

    move/from16 v0, v24

    invoke-static {v0, v4}, Landroid/net/TrafficStats;->incrementOperationCount(II)V

    :cond_5
    invoke-static {v13}, Landroid/net/TrafficStats;->setThreadStatsTag(I)V

    const/4 v10, 0x1

    const/4 v12, 0x0

    move-object/from16 v6, p0

    move-object v7, v3

    move-object/from16 v9, p1

    move-object/from16 v11, p2

    invoke-virtual/range {v6 .. v12}, Lcom/google/android/syncadapters/calendar/EventHandler;->applyItemToEntity(Ljava/util/List;Lcom/google/api/services/calendar/model/Event;Landroid/content/Entity;ZLandroid/content/SyncResult;Ljava/lang/Object;)V

    goto/16 :goto_0

    :catch_0
    move-exception v15

    :try_start_2
    invoke-virtual {v15}, Lcom/google/api/client/googleapis/json/GoogleJsonResponseException;->getStatusCode()I

    move-result v4

    const/16 v6, 0x193

    if-ne v4, v6, :cond_7

    const-string v4, "EventHandler"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "Can\'t insert event into "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/google/android/syncadapters/calendar/EventHandler;->mCalendarId:Ljava/lang/String;

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, ": Forbidden"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v4, v6}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    sget-object v4, Landroid/provider/CalendarContract$Events;->CONTENT_URI:Landroid/net/Uri;

    const-string v6, "_id"

    move-object/from16 v0, v25

    invoke-virtual {v0, v6}, Landroid/content/ContentValues;->getAsLong(Ljava/lang/String;)Ljava/lang/Long;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/Long;->longValue()J

    move-result-wide v6

    invoke-static {v4, v6, v7}, Landroid/content/ContentUris;->withAppendedId(Landroid/net/Uri;J)Landroid/net/Uri;

    move-result-object v4

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/google/android/syncadapters/calendar/EventHandler;->mAccount:Landroid/accounts/Account;

    invoke-static {v4, v6}, Lcom/google/android/syncadapters/calendar/HandlerUtils;->addQueryParameters(Landroid/net/Uri;Landroid/accounts/Account;)Landroid/net/Uri;

    move-result-object v4

    const/4 v6, 0x0

    const/4 v7, 0x0

    invoke-static {v3, v4, v6, v7}, Lcom/google/android/syncadapters/calendar/CalendarSyncAdapterApiary;->addDeleteOperation(Ljava/util/List;Landroid/net/Uri;Ljava/lang/Long;Z)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    if-eqz v24, :cond_6

    const/4 v4, 0x1

    move/from16 v0, v24

    invoke-static {v0, v4}, Landroid/net/TrafficStats;->incrementOperationCount(II)V

    :cond_6
    :goto_3
    invoke-static {v13}, Landroid/net/TrafficStats;->setThreadStatsTag(I)V

    goto/16 :goto_0

    :cond_7
    :try_start_3
    const-string v4, "EventHandler"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "Can\'t insert event in "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/google/android/syncadapters/calendar/EventHandler;->mCalendarId:Ljava/lang/String;

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v4, v6, v15}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    const/4 v3, 0x0

    if-eqz v24, :cond_6

    const/4 v4, 0x1

    move/from16 v0, v24

    invoke-static {v0, v4}, Landroid/net/TrafficStats;->incrementOperationCount(II)V

    goto :goto_3

    :cond_8
    if-eqz v19, :cond_9

    or-int/lit8 v24, v13, 0x3

    :try_start_4
    invoke-static/range {v24 .. v24}, Landroid/net/TrafficStats;->setThreadStatsTag(I)V
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    :try_start_5
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/syncadapters/calendar/EventHandler;->mClient:Lcom/google/api/services/calendar/Calendar;

    invoke-virtual {v4}, Lcom/google/api/services/calendar/Calendar;->events()Lcom/google/api/services/calendar/Calendar$Events;

    move-result-object v4

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/google/android/syncadapters/calendar/EventHandler;->mCalendarId:Ljava/lang/String;

    move-object/from16 v0, v17

    invoke-virtual {v4, v6, v0}, Lcom/google/api/services/calendar/Calendar$Events;->delete(Ljava/lang/String;Ljava/lang/String;)Lcom/google/api/services/calendar/Calendar$Events$Delete;

    move-result-object v4

    const/4 v6, 0x1

    invoke-static {v6}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v6

    invoke-virtual {v4, v6}, Lcom/google/api/services/calendar/Calendar$Events$Delete;->setSendNotifications(Ljava/lang/Boolean;)Lcom/google/api/services/calendar/Calendar$Events$Delete;

    move-result-object v4

    invoke-virtual {v4}, Lcom/google/api/services/calendar/Calendar$Events$Delete;->execute()V
    :try_end_5
    .catch Lcom/google/api/client/googleapis/json/GoogleJsonResponseException; {:try_start_5 .. :try_end_5} :catch_1
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    const/4 v8, 0x0

    goto/16 :goto_2

    :catch_1
    move-exception v15

    :try_start_6
    invoke-virtual {v15}, Lcom/google/api/client/googleapis/json/GoogleJsonResponseException;->getStatusCode()I

    move-result v14

    sparse-switch v14, :sswitch_data_0

    const-string v4, "EventHandler"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "Can\'t delete event "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/google/android/syncadapters/calendar/EventHandler;->mCalendarId:Ljava/lang/String;

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, "/"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    move-object/from16 v0, v17

    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v4, v6, v15}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_0

    const/4 v3, 0x0

    if-eqz v24, :cond_6

    const/4 v4, 0x1

    move/from16 v0, v24

    invoke-static {v0, v4}, Landroid/net/TrafficStats;->incrementOperationCount(II)V

    goto/16 :goto_3

    :sswitch_0
    :try_start_7
    const-string v4, "EventHandler"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "Can\'t delete event "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/google/android/syncadapters/calendar/EventHandler;->mCalendarId:Ljava/lang/String;

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, "/"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    move-object/from16 v0, v17

    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, ": Forbidden"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v4, v6}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    new-instance v5, Landroid/content/ContentValues;

    invoke-direct {v5}, Landroid/content/ContentValues;-><init>()V

    const-string v4, "deleted"

    const/4 v6, 0x0

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    invoke-virtual {v5, v4, v6}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    sget-object v4, Landroid/provider/CalendarContract$Events;->CONTENT_URI:Landroid/net/Uri;

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/google/android/syncadapters/calendar/EventHandler;->mAccount:Landroid/accounts/Account;

    invoke-static {v4, v6}, Lcom/google/android/syncadapters/calendar/HandlerUtils;->addQueryParameters(Landroid/net/Uri;Landroid/accounts/Account;)Landroid/net/Uri;

    move-result-object v4

    const-string v6, "_id"

    move-object/from16 v0, v25

    invoke-virtual {v0, v6}, Landroid/content/ContentValues;->getAsLong(Ljava/lang/String;)Ljava/lang/Long;

    move-result-object v6

    const/4 v7, 0x0

    const/4 v8, 0x1

    invoke-static/range {v3 .. v8}, Lcom/google/android/syncadapters/calendar/CalendarSyncAdapterApiary;->addUpdateOperation(Ljava/util/List;Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/Long;Ljava/lang/Integer;Z)V

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/syncadapters/calendar/EventHandler;->mClient:Lcom/google/api/services/calendar/Calendar;

    invoke-virtual {v4}, Lcom/google/api/services/calendar/Calendar;->events()Lcom/google/api/services/calendar/Calendar$Events;

    move-result-object v4

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/google/android/syncadapters/calendar/EventHandler;->mCalendarId:Ljava/lang/String;

    move-object/from16 v0, v17

    invoke-virtual {v4, v6, v0}, Lcom/google/api/services/calendar/Calendar$Events;->get(Ljava/lang/String;Ljava/lang/String;)Lcom/google/api/services/calendar/Calendar$Events$Get;

    move-result-object v4

    invoke-virtual {v4}, Lcom/google/api/services/calendar/Calendar$Events$Get;->execute()Lcom/google/api/services/calendar/model/Event;

    move-result-object v8

    goto/16 :goto_2

    :sswitch_1
    const-string v4, "EventHandler"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "Can\'t delete event "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/google/android/syncadapters/calendar/EventHandler;->mCalendarId:Ljava/lang/String;

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, "/"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    move-object/from16 v0, v17

    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, ": Not found"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v4, v6}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v8, 0x0

    goto/16 :goto_2

    :sswitch_2
    const-string v4, "EventHandler"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "Can\'t delete event "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/google/android/syncadapters/calendar/EventHandler;->mCalendarId:Ljava/lang/String;

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, "/"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    move-object/from16 v0, v17

    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, ": Deleted from server"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v4, v6}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v8, 0x0

    goto/16 :goto_2

    :cond_9
    or-int/lit8 v24, v13, 0x2

    invoke-static/range {v24 .. v24}, Landroid/net/TrafficStats;->setThreadStatsTag(I)V

    const-string v4, "_sync_id =? AND lastSynced =?"

    const/4 v6, 0x2

    new-array v6, v6, [Ljava/lang/String;

    const/4 v7, 0x0

    aput-object v22, v6, v7

    const/4 v7, 0x1

    const-string v9, "1"

    aput-object v9, v6, v7

    move-object/from16 v0, p0

    invoke-direct {v0, v4, v6}, Lcom/google/android/syncadapters/calendar/EventHandler;->fetchEntity(Ljava/lang/String;[Ljava/lang/String;)Landroid/content/Entity;
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_0

    move-result-object v20

    if-nez v20, :cond_c

    :try_start_8
    invoke-virtual/range {p0 .. p1}, Lcom/google/android/syncadapters/calendar/EventHandler;->convertEntityToEntry(Landroid/content/Entity;)Lcom/google/api/services/calendar/model/Event;

    move-result-object v16

    const-string v4, "hasAttendeeData"

    move-object/from16 v0, v25

    invoke-virtual {v0, v4}, Landroid/content/ContentValues;->getAsInteger(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/Integer;->intValue()I

    move-result v4

    if-nez v4, :cond_a

    const/4 v4, 0x1

    invoke-static {v4}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v4

    move-object/from16 v0, v16

    invoke-virtual {v0, v4}, Lcom/google/api/services/calendar/model/Event;->setAttendeesOmitted(Ljava/lang/Boolean;)Lcom/google/api/services/calendar/model/Event;

    :cond_a
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/syncadapters/calendar/EventHandler;->mClient:Lcom/google/api/services/calendar/Calendar;

    invoke-virtual {v4}, Lcom/google/api/services/calendar/Calendar;->events()Lcom/google/api/services/calendar/Calendar$Events;

    move-result-object v4

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/google/android/syncadapters/calendar/EventHandler;->mCalendarId:Ljava/lang/String;

    move-object/from16 v0, v17

    move-object/from16 v1, v16

    invoke-virtual {v4, v6, v0, v1}, Lcom/google/api/services/calendar/Calendar$Events;->update(Ljava/lang/String;Ljava/lang/String;Lcom/google/api/services/calendar/model/Event;)Lcom/google/api/services/calendar/Calendar$Events$Update;

    move-result-object v4

    const/4 v6, 0x1

    invoke-static {v6}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v6

    invoke-virtual {v4, v6}, Lcom/google/api/services/calendar/Calendar$Events$Update;->setSendNotifications(Ljava/lang/Boolean;)Lcom/google/api/services/calendar/Calendar$Events$Update;

    move-result-object v23

    const-string v4, "sync_data4"

    move-object/from16 v0, v25

    invoke-virtual {v0, v4}, Landroid/content/ContentValues;->containsKey(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_b

    const-string v4, "ifmatch"

    const-string v6, "sync_data4"

    move-object/from16 v0, v25

    invoke-virtual {v0, v6}, Landroid/content/ContentValues;->getAsString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    move-object/from16 v0, v23

    invoke-virtual {v0, v4, v6}, Lcom/google/api/services/calendar/Calendar$Events$Update;->set(Ljava/lang/String;Ljava/lang/Object;)V

    :cond_b
    invoke-virtual/range {v23 .. v23}, Lcom/google/api/services/calendar/Calendar$Events$Update;->execute()Lcom/google/api/services/calendar/model/Event;

    move-result-object v8

    goto/16 :goto_2

    :cond_c
    invoke-virtual/range {v20 .. v20}, Landroid/content/Entity;->getEntityValues()Landroid/content/ContentValues;

    move-result-object v21

    const-string v4, "duration"

    move-object/from16 v0, v21

    invoke-virtual {v0, v4}, Landroid/content/ContentValues;->getAsString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    move-object/from16 v0, p0

    invoke-direct {v0, v4}, Lcom/google/android/syncadapters/calendar/EventHandler;->parseDuration(Ljava/lang/String;)J

    move-result-wide v6

    move-object/from16 v0, p0

    move-object/from16 v1, v21

    invoke-direct {v0, v1, v6, v7}, Lcom/google/android/syncadapters/calendar/EventHandler;->setDtendFromDuration(Landroid/content/ContentValues;J)V

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    move-object/from16 v2, v20

    invoke-direct {v0, v1, v2}, Lcom/google/android/syncadapters/calendar/EventHandler;->convertValuesToPartialDiff(Landroid/content/Entity;Landroid/content/Entity;)Lcom/google/api/services/calendar/model/Event;

    move-result-object v16

    invoke-virtual/range {v16 .. v16}, Lcom/google/api/services/calendar/model/Event;->isEmpty()Z

    move-result v4

    if-eqz v4, :cond_d

    const-string v4, "EventHandler"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "No diffs found for event "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    move-object/from16 v0, v17

    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v4, v6}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    sget-object v4, Landroid/provider/CalendarContract$Events;->CONTENT_URI:Landroid/net/Uri;

    const-string v6, "_id"

    move-object/from16 v0, v25

    invoke-virtual {v0, v6}, Landroid/content/ContentValues;->getAsLong(Ljava/lang/String;)Ljava/lang/Long;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/Long;->longValue()J

    move-result-wide v6

    invoke-static {v4, v6, v7}, Landroid/content/ContentUris;->withAppendedId(Landroid/net/Uri;J)Landroid/net/Uri;

    move-result-object v4

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/google/android/syncadapters/calendar/EventHandler;->mAccount:Landroid/accounts/Account;

    invoke-static {v4, v6}, Lcom/google/android/syncadapters/calendar/HandlerUtils;->addQueryParameters(Landroid/net/Uri;Landroid/accounts/Account;)Landroid/net/Uri;

    move-result-object v4

    invoke-static {v4}, Landroid/content/ContentProviderOperation;->newUpdate(Landroid/net/Uri;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v4

    const-string v6, "dirty"

    const/4 v7, 0x0

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    invoke-virtual {v4, v6, v7}, Landroid/content/ContentProviderOperation$Builder;->withValue(Ljava/lang/String;Ljava/lang/Object;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v4

    invoke-virtual {v4}, Landroid/content/ContentProviderOperation$Builder;->build()Landroid/content/ContentProviderOperation;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
    :try_end_8
    .catch Lcom/google/api/client/googleapis/json/GoogleJsonResponseException; {:try_start_8 .. :try_end_8} :catch_2
    .catchall {:try_start_8 .. :try_end_8} :catchall_0

    if-eqz v24, :cond_6

    const/4 v4, 0x1

    move/from16 v0, v24

    invoke-static {v0, v4}, Landroid/net/TrafficStats;->incrementOperationCount(II)V

    goto/16 :goto_3

    :cond_d
    :try_start_9
    invoke-static/range {v16 .. v16}, Lcom/google/android/syncadapters/calendar/EventHandler;->hasOnlyUnsyncedKeys(Lcom/google/api/services/calendar/model/Event;)Z

    move-result v18

    if-eqz v18, :cond_e

    const-string v4, "description"

    move-object/from16 v0, v21

    invoke-virtual {v0, v4}, Landroid/content/ContentValues;->getAsString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    move-object/from16 v0, p0

    move-object/from16 v1, v16

    invoke-direct {v0, v1, v4}, Lcom/google/android/syncadapters/calendar/EventHandler;->touchDescription(Lcom/google/api/services/calendar/model/Event;Ljava/lang/String;)V

    :cond_e
    const-string v4, "hasAttendeeData"

    move-object/from16 v0, v25

    invoke-virtual {v0, v4}, Landroid/content/ContentValues;->getAsInteger(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/Integer;->intValue()I

    move-result v4

    if-nez v4, :cond_f

    const/4 v4, 0x1

    invoke-static {v4}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v4

    move-object/from16 v0, v16

    invoke-virtual {v0, v4}, Lcom/google/api/services/calendar/model/Event;->setAttendeesOmitted(Ljava/lang/Boolean;)Lcom/google/api/services/calendar/model/Event;

    :cond_f
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/syncadapters/calendar/EventHandler;->mClient:Lcom/google/api/services/calendar/Calendar;

    invoke-virtual {v4}, Lcom/google/api/services/calendar/Calendar;->events()Lcom/google/api/services/calendar/Calendar$Events;

    move-result-object v4

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/google/android/syncadapters/calendar/EventHandler;->mCalendarId:Ljava/lang/String;

    move-object/from16 v0, v17

    move-object/from16 v1, v16

    invoke-virtual {v4, v6, v0, v1}, Lcom/google/api/services/calendar/Calendar$Events;->patch(Ljava/lang/String;Ljava/lang/String;Lcom/google/api/services/calendar/model/Event;)Lcom/google/api/services/calendar/Calendar$Events$Patch;

    move-result-object v6

    if-nez v18, :cond_10

    const/4 v4, 0x1

    :goto_4
    invoke-static {v4}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v4

    invoke-virtual {v6, v4}, Lcom/google/api/services/calendar/Calendar$Events$Patch;->setSendNotifications(Ljava/lang/Boolean;)Lcom/google/api/services/calendar/Calendar$Events$Patch;

    move-result-object v23

    invoke-virtual/range {v23 .. v23}, Lcom/google/api/services/calendar/Calendar$Events$Patch;->execute()Lcom/google/api/services/calendar/model/Event;
    :try_end_9
    .catch Lcom/google/api/client/googleapis/json/GoogleJsonResponseException; {:try_start_9 .. :try_end_9} :catch_2
    .catchall {:try_start_9 .. :try_end_9} :catchall_0

    move-result-object v8

    goto/16 :goto_2

    :cond_10
    const/4 v4, 0x0

    goto :goto_4

    :catch_2
    move-exception v15

    :try_start_a
    invoke-virtual {v15}, Lcom/google/api/client/googleapis/json/GoogleJsonResponseException;->getStatusCode()I

    move-result v14

    sparse-switch v14, :sswitch_data_1

    const-string v4, "EventHandler"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "Can\'t update event "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/google/android/syncadapters/calendar/EventHandler;->mCalendarId:Ljava/lang/String;

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, "/"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    move-object/from16 v0, v17

    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v4, v6, v15}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
    :try_end_a
    .catchall {:try_start_a .. :try_end_a} :catchall_0

    const/4 v3, 0x0

    if-eqz v24, :cond_6

    const/4 v4, 0x1

    move/from16 v0, v24

    invoke-static {v0, v4}, Landroid/net/TrafficStats;->incrementOperationCount(II)V

    goto/16 :goto_3

    :sswitch_3
    :try_start_b
    const-string v4, "EventHandler"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "Can\'t update event "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/google/android/syncadapters/calendar/EventHandler;->mCalendarId:Ljava/lang/String;

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, "/"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    move-object/from16 v0, v17

    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, ": Forbidden"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v4, v6}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/syncadapters/calendar/EventHandler;->mClient:Lcom/google/api/services/calendar/Calendar;

    invoke-virtual {v4}, Lcom/google/api/services/calendar/Calendar;->events()Lcom/google/api/services/calendar/Calendar$Events;

    move-result-object v4

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/google/android/syncadapters/calendar/EventHandler;->mCalendarId:Ljava/lang/String;

    move-object/from16 v0, v17

    invoke-virtual {v4, v6, v0}, Lcom/google/api/services/calendar/Calendar$Events;->get(Ljava/lang/String;Ljava/lang/String;)Lcom/google/api/services/calendar/Calendar$Events$Get;

    move-result-object v4

    invoke-virtual {v4}, Lcom/google/api/services/calendar/Calendar$Events$Get;->execute()Lcom/google/api/services/calendar/model/Event;

    move-result-object v8

    goto/16 :goto_2

    :sswitch_4
    const-string v4, "EventHandler"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "Can\'t update event "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/google/android/syncadapters/calendar/EventHandler;->mCalendarId:Ljava/lang/String;

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, "/"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    move-object/from16 v0, v17

    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, ": Not found"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v4, v6}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v8, 0x0

    goto/16 :goto_2

    :sswitch_5
    const-string v4, "EventHandler"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "Can\'t update event "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/google/android/syncadapters/calendar/EventHandler;->mCalendarId:Ljava/lang/String;

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, "/"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    move-object/from16 v0, v17

    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, ": Deleted from server"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v4, v6}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v8, 0x0

    goto/16 :goto_2

    :sswitch_6
    const-string v4, "EventHandler"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "Can\'t update event "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/google/android/syncadapters/calendar/EventHandler;->mCalendarId:Ljava/lang/String;

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, "/"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    move-object/from16 v0, v17

    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, ": Conflict detected"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v4, v6}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/syncadapters/calendar/EventHandler;->mClient:Lcom/google/api/services/calendar/Calendar;

    invoke-virtual {v4}, Lcom/google/api/services/calendar/Calendar;->events()Lcom/google/api/services/calendar/Calendar$Events;

    move-result-object v4

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/google/android/syncadapters/calendar/EventHandler;->mCalendarId:Ljava/lang/String;

    move-object/from16 v0, v17

    invoke-virtual {v4, v6, v0}, Lcom/google/api/services/calendar/Calendar$Events;->get(Ljava/lang/String;Ljava/lang/String;)Lcom/google/api/services/calendar/Calendar$Events$Get;

    move-result-object v4

    invoke-virtual {v4}, Lcom/google/api/services/calendar/Calendar$Events$Get;->execute()Lcom/google/api/services/calendar/model/Event;
    :try_end_b
    .catchall {:try_start_b .. :try_end_b} :catchall_0

    move-result-object v8

    goto/16 :goto_2

    :catchall_0
    move-exception v4

    if-eqz v24, :cond_11

    const/4 v6, 0x1

    move/from16 v0, v24

    invoke-static {v0, v6}, Landroid/net/TrafficStats;->incrementOperationCount(II)V

    :cond_11
    invoke-static {v13}, Landroid/net/TrafficStats;->setThreadStatsTag(I)V

    throw v4

    nop

    :sswitch_data_0
    .sparse-switch
        0x193 -> :sswitch_0
        0x194 -> :sswitch_1
        0x19e -> :sswitch_2
    .end sparse-switch

    :sswitch_data_1
    .sparse-switch
        0x193 -> :sswitch_3
        0x194 -> :sswitch_4
        0x19c -> :sswitch_6
        0x19e -> :sswitch_5
    .end sparse-switch
.end method
