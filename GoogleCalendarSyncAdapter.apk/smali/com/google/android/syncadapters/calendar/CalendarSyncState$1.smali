.class final Lcom/google/android/syncadapters/calendar/CalendarSyncState$1;
.super Ljava/lang/Object;
.source "CalendarSyncState.java"

# interfaces
.implements Lcom/google/android/syncadapters/calendar/CalendarSyncState$IdTransformer;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/syncadapters/calendar/CalendarSyncState;->upgradeEvents(Landroid/content/ContentProviderClient;Landroid/accounts/Account;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = null
.end annotation


# direct methods
.method constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public transform(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    .locals 4
    .param p1    # Ljava/lang/String;
    .param p2    # Ljava/lang/String;

    if-eqz p2, :cond_0

    const-string v2, "http://www.google.com/calendar/feeds/"

    invoke-virtual {p2, v2}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_1

    :cond_0
    :goto_0
    return-object p2

    :cond_1
    const-string v2, "/events/"

    const-string v3, "http://www.google.com/calendar/feeds/"

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v3

    invoke-virtual {p2, v2, v3}, Ljava/lang/String;->indexOf(Ljava/lang/String;I)I

    move-result v1

    if-ltz v1, :cond_0

    const-string v2, "/events/"

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v2

    add-int/2addr v2, v1

    invoke-virtual {p2, v2}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v0

    sget-boolean v2, Lcom/google/android/syncadapters/calendar/Utils;->JELLY_BEAN_OR_HIGHER:Z

    if-eqz v2, :cond_2

    :goto_1
    move-object p2, v0

    goto :goto_0

    :cond_2
    invoke-static {v0, p1}, Lcom/google/android/syncadapters/calendar/Utils;->createEventId(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    goto :goto_1
.end method
