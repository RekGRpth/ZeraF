.class public Lcom/google/android/apiary/GoogleRequestInitializer;
.super Ljava/lang/Object;
.source "GoogleRequestInitializer.java"

# interfaces
.implements Lcom/google/api/client/http/HttpExecuteInterceptor;
.implements Lcom/google/api/client/http/HttpRequestInitializer;
.implements Lcom/google/api/client/http/HttpUnsuccessfulResponseHandler;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/apiary/GoogleRequestInitializer$1;,
        Lcom/google/android/apiary/GoogleRequestInitializer$BlockedRequestException;
    }
.end annotation


# instance fields
.field private mAccountName:Ljava/lang/String;

.field private mAuthErrorHandler:Lcom/google/android/apiary/AuthErrorHandler;

.field private mAuthToken:Ljava/lang/String;

.field private final mContext:Landroid/content/Context;

.field private final mLogTag:Ljava/lang/String;

.field private mRequestConnectTimeout:I

.field private mRequestReadTimeout:I

.field private final mScope:Ljava/lang/String;


# direct methods
.method public constructor <init>(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V
    .locals 2
    .param p1    # Landroid/content/Context;
    .param p2    # Ljava/lang/String;
    .param p3    # Ljava/lang/String;

    const/4 v1, 0x0

    const/4 v0, 0x0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object v1, p0, Lcom/google/android/apiary/GoogleRequestInitializer;->mAuthToken:Ljava/lang/String;

    iput-object v1, p0, Lcom/google/android/apiary/GoogleRequestInitializer;->mAuthErrorHandler:Lcom/google/android/apiary/AuthErrorHandler;

    iput v0, p0, Lcom/google/android/apiary/GoogleRequestInitializer;->mRequestConnectTimeout:I

    iput v0, p0, Lcom/google/android/apiary/GoogleRequestInitializer;->mRequestReadTimeout:I

    iput-object p1, p0, Lcom/google/android/apiary/GoogleRequestInitializer;->mContext:Landroid/content/Context;

    iput-object p2, p0, Lcom/google/android/apiary/GoogleRequestInitializer;->mScope:Ljava/lang/String;

    iput-object p3, p0, Lcom/google/android/apiary/GoogleRequestInitializer;->mLogTag:Ljava/lang/String;

    return-void
.end method


# virtual methods
.method public getAuthToken()Ljava/lang/String;
    .locals 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/android/apiary/AuthenticationException;
        }
    .end annotation

    iget-object v2, p0, Lcom/google/android/apiary/GoogleRequestInitializer;->mAuthToken:Ljava/lang/String;

    if-nez v2, :cond_0

    :try_start_0
    iget-object v2, p0, Lcom/google/android/apiary/GoogleRequestInitializer;->mContext:Landroid/content/Context;

    iget-object v3, p0, Lcom/google/android/apiary/GoogleRequestInitializer;->mAccountName:Ljava/lang/String;

    iget-object v4, p0, Lcom/google/android/apiary/GoogleRequestInitializer;->mScope:Ljava/lang/String;

    invoke-static {v2, v3, v4}, Lcom/google/android/gms/auth/GoogleAuthUtil;->getToken(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/google/android/apiary/GoogleRequestInitializer;->mAuthToken:Ljava/lang/String;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Lcom/google/android/gms/auth/UserRecoverableAuthException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Lcom/google/android/gms/auth/GoogleAuthException; {:try_start_0 .. :try_end_0} :catch_2

    :cond_0
    iget-object v2, p0, Lcom/google/android/apiary/GoogleRequestInitializer;->mAuthToken:Ljava/lang/String;

    return-object v2

    :catch_0
    move-exception v1

    new-instance v2, Lcom/google/android/apiary/AuthenticationException;

    const-string v3, "Could not get an auth token"

    invoke-direct {v2, v3, v1}, Lcom/google/android/apiary/AuthenticationException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v2

    :catch_1
    move-exception v1

    iget-object v2, p0, Lcom/google/android/apiary/GoogleRequestInitializer;->mAuthErrorHandler:Lcom/google/android/apiary/AuthErrorHandler;

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/google/android/apiary/GoogleRequestInitializer;->mContext:Landroid/content/Context;

    invoke-static {v2}, Lcom/google/android/gms/common/GooglePlayServicesUtil;->isGooglePlayServicesAvailable(Landroid/content/Context;)I

    move-result v0

    if-nez v0, :cond_1

    iget-object v2, p0, Lcom/google/android/apiary/GoogleRequestInitializer;->mAuthErrorHandler:Lcom/google/android/apiary/AuthErrorHandler;

    iget-object v3, p0, Lcom/google/android/apiary/GoogleRequestInitializer;->mAccountName:Ljava/lang/String;

    invoke-virtual {v1}, Lcom/google/android/gms/auth/UserRecoverableAuthException;->getIntent()Landroid/content/Intent;

    move-result-object v4

    invoke-interface {v2, v3, v4}, Lcom/google/android/apiary/AuthErrorHandler;->handleAuthError(Ljava/lang/String;Landroid/content/Intent;)V

    new-instance v2, Lcom/google/android/apiary/AuthenticationException;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Authentication error for account :"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lcom/google/android/apiary/GoogleRequestInitializer;->mAccountName:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3, v1}, Lcom/google/android/apiary/AuthenticationException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v2

    :cond_1
    iget-object v2, p0, Lcom/google/android/apiary/GoogleRequestInitializer;->mAuthErrorHandler:Lcom/google/android/apiary/AuthErrorHandler;

    const/4 v3, 0x0

    invoke-virtual {v1}, Lcom/google/android/gms/auth/UserRecoverableAuthException;->getIntent()Landroid/content/Intent;

    move-result-object v4

    invoke-interface {v2, v3, v4}, Lcom/google/android/apiary/AuthErrorHandler;->handleAuthError(Ljava/lang/String;Landroid/content/Intent;)V

    new-instance v2, Lcom/google/android/apiary/AuthenticationException;

    const-string v3, "Need to install GmsCore.apk"

    invoke-direct {v2, v3, v1}, Lcom/google/android/apiary/AuthenticationException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v2

    :catch_2
    move-exception v1

    new-instance v2, Lcom/google/android/apiary/AuthenticationException;

    const-string v3, "Could not get an auth token"

    invoke-direct {v2, v3, v1}, Lcom/google/android/apiary/AuthenticationException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v2
.end method

.method public handleResponse(Lcom/google/api/client/http/HttpRequest;Lcom/google/api/client/http/HttpResponse;Z)Z
    .locals 2
    .param p1    # Lcom/google/api/client/http/HttpRequest;
    .param p2    # Lcom/google/api/client/http/HttpResponse;
    .param p3    # Z
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-virtual {p2}, Lcom/google/api/client/http/HttpResponse;->getStatusCode()I

    move-result v0

    const/16 v1, 0x191

    if-ne v0, v1, :cond_1

    iget-object v0, p0, Lcom/google/android/apiary/GoogleRequestInitializer;->mLogTag:Ljava/lang/String;

    const-string v1, "Got a 401. Invalidating token"

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    invoke-virtual {p1}, Lcom/google/api/client/http/HttpRequest;->getNumberOfRetries()I

    move-result v0

    if-lez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apiary/GoogleRequestInitializer;->mLogTag:Ljava/lang/String;

    const-string v1, "Retrying request"

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    iget-object v0, p0, Lcom/google/android/apiary/GoogleRequestInitializer;->mContext:Landroid/content/Context;

    iget-object v1, p0, Lcom/google/android/apiary/GoogleRequestInitializer;->mAuthToken:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/google/android/gms/auth/GoogleAuthUtil;->invalidateToken(Landroid/content/Context;Ljava/lang/String;)V

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/apiary/GoogleRequestInitializer;->mAuthToken:Ljava/lang/String;

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public initialize(Lcom/google/api/client/http/HttpRequest;)V
    .locals 2
    .param p1    # Lcom/google/api/client/http/HttpRequest;

    invoke-virtual {p1, p0}, Lcom/google/api/client/http/HttpRequest;->setInterceptor(Lcom/google/api/client/http/HttpExecuteInterceptor;)Lcom/google/api/client/http/HttpRequest;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/google/api/client/http/HttpRequest;->setUnsuccessfulResponseHandler(Lcom/google/api/client/http/HttpUnsuccessfulResponseHandler;)Lcom/google/api/client/http/HttpRequest;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/google/api/client/http/HttpRequest;->setNumberOfRetries(I)Lcom/google/api/client/http/HttpRequest;

    iget v0, p0, Lcom/google/android/apiary/GoogleRequestInitializer;->mRequestConnectTimeout:I

    if-lez v0, :cond_0

    iget v0, p0, Lcom/google/android/apiary/GoogleRequestInitializer;->mRequestConnectTimeout:I

    invoke-virtual {p1, v0}, Lcom/google/api/client/http/HttpRequest;->setConnectTimeout(I)Lcom/google/api/client/http/HttpRequest;

    :cond_0
    iget v0, p0, Lcom/google/android/apiary/GoogleRequestInitializer;->mRequestReadTimeout:I

    if-lez v0, :cond_1

    iget v0, p0, Lcom/google/android/apiary/GoogleRequestInitializer;->mRequestReadTimeout:I

    invoke-virtual {p1, v0}, Lcom/google/api/client/http/HttpRequest;->setReadTimeout(I)Lcom/google/api/client/http/HttpRequest;

    :cond_1
    return-void
.end method

.method public intercept(Lcom/google/api/client/http/HttpRequest;)V
    .locals 11
    .param p1    # Lcom/google/api/client/http/HttpRequest;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-virtual {p0}, Lcom/google/android/apiary/GoogleRequestInitializer;->getAuthToken()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1}, Lcom/google/api/client/http/HttpRequest;->getHeaders()Lcom/google/api/client/http/HttpHeaders;

    move-result-object v2

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "OAuth "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v2, v8}, Lcom/google/api/client/http/HttpHeaders;->setAuthorization(Ljava/lang/String;)V

    invoke-virtual {p1}, Lcom/google/api/client/http/HttpRequest;->getUrl()Lcom/google/api/client/http/GenericUrl;

    move-result-object v7

    invoke-virtual {v7}, Lcom/google/api/client/http/GenericUrl;->build()Ljava/lang/String;

    move-result-object v3

    iget-object v8, p0, Lcom/google/android/apiary/GoogleRequestInitializer;->mContext:Landroid/content/Context;

    invoke-virtual {v8}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v8

    invoke-static {v8}, Lcom/google/android/common/http/UrlRules;->getRules(Landroid/content/ContentResolver;)Lcom/google/android/common/http/UrlRules;

    move-result-object v6

    invoke-virtual {v6, v3}, Lcom/google/android/common/http/UrlRules;->matchRule(Ljava/lang/String;)Lcom/google/android/common/http/UrlRules$Rule;

    move-result-object v5

    invoke-virtual {v5, v3}, Lcom/google/android/common/http/UrlRules$Rule;->apply(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    if-nez v4, :cond_0

    iget-object v8, p0, Lcom/google/android/apiary/GoogleRequestInitializer;->mLogTag:Ljava/lang/String;

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "Blocked by "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    iget-object v10, v5, Lcom/google/android/common/http/UrlRules$Rule;->mName:Ljava/lang/String;

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, ": "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v8, v9}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    new-instance v8, Lcom/google/android/apiary/GoogleRequestInitializer$BlockedRequestException;

    const/4 v9, 0x0

    invoke-direct {v8, v5, v9}, Lcom/google/android/apiary/GoogleRequestInitializer$BlockedRequestException;-><init>(Lcom/google/android/common/http/UrlRules$Rule;Lcom/google/android/apiary/GoogleRequestInitializer$1;)V

    throw v8

    :cond_0
    invoke-virtual {v4, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-nez v8, :cond_2

    iget-object v8, p0, Lcom/google/android/apiary/GoogleRequestInitializer;->mLogTag:Ljava/lang/String;

    const/4 v9, 0x2

    invoke-static {v8, v9}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v8

    if-eqz v8, :cond_1

    iget-object v8, p0, Lcom/google/android/apiary/GoogleRequestInitializer;->mLogTag:Ljava/lang/String;

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "Rule "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    iget-object v10, v5, Lcom/google/android/common/http/UrlRules$Rule;->mName:Ljava/lang/String;

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, ": "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, " -> "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v8, v9}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    :cond_1
    new-instance v7, Lcom/google/api/client/http/GenericUrl;

    invoke-direct {v7, v4}, Lcom/google/api/client/http/GenericUrl;-><init>(Ljava/lang/String;)V

    invoke-virtual {p1, v7}, Lcom/google/api/client/http/HttpRequest;->setUrl(Lcom/google/api/client/http/GenericUrl;)Lcom/google/api/client/http/HttpRequest;

    :cond_2
    const-string v8, "ifmatch"

    invoke-virtual {v7, v8}, Lcom/google/api/client/http/GenericUrl;->getFirst(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    if-eqz v1, :cond_3

    invoke-virtual {v2, v1}, Lcom/google/api/client/http/HttpHeaders;->setIfMatch(Ljava/lang/String;)V

    const-string v8, "ifmatch"

    invoke-virtual {v7, v8}, Lcom/google/api/client/http/GenericUrl;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    :cond_3
    return-void
.end method

.method public setAuthErrorHandler(Lcom/google/android/apiary/AuthErrorHandler;)V
    .locals 0
    .param p1    # Lcom/google/android/apiary/AuthErrorHandler;

    iput-object p1, p0, Lcom/google/android/apiary/GoogleRequestInitializer;->mAuthErrorHandler:Lcom/google/android/apiary/AuthErrorHandler;

    return-void
.end method

.method public setEmail(Ljava/lang/String;)V
    .locals 1
    .param p1    # Ljava/lang/String;

    iput-object p1, p0, Lcom/google/android/apiary/GoogleRequestInitializer;->mAccountName:Ljava/lang/String;

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/apiary/GoogleRequestInitializer;->mAuthToken:Ljava/lang/String;

    return-void
.end method
