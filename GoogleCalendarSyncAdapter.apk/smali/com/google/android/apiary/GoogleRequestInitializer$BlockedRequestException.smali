.class public Lcom/google/android/apiary/GoogleRequestInitializer$BlockedRequestException;
.super Ljava/io/IOException;
.source "GoogleRequestInitializer.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/apiary/GoogleRequestInitializer;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "BlockedRequestException"
.end annotation


# direct methods
.method private constructor <init>(Lcom/google/android/common/http/UrlRules$Rule;)V
    .locals 2
    .param p1    # Lcom/google/android/common/http/UrlRules$Rule;

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "Blocked by rule: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p1, Lcom/google/android/common/http/UrlRules$Rule;->mName:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    return-void
.end method

.method synthetic constructor <init>(Lcom/google/android/common/http/UrlRules$Rule;Lcom/google/android/apiary/GoogleRequestInitializer$1;)V
    .locals 0
    .param p1    # Lcom/google/android/common/http/UrlRules$Rule;
    .param p2    # Lcom/google/android/apiary/GoogleRequestInitializer$1;

    invoke-direct {p0, p1}, Lcom/google/android/apiary/GoogleRequestInitializer$BlockedRequestException;-><init>(Lcom/google/android/common/http/UrlRules$Rule;)V

    return-void
.end method
