.class public Lcom/google/android/auth/GoogleAuthSession;
.super Ljava/lang/Object;
.source "GoogleAuthSession.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/auth/GoogleAuthSession$1;,
        Lcom/google/android/auth/GoogleAuthSession$AuthServiceConnection;
    }
.end annotation

.annotation runtime Ljava/lang/Deprecated;
.end annotation


# instance fields
.field private mAccount:Landroid/accounts/Account;

.field mAuthServiceConnection:Lcom/google/android/auth/GoogleAuthSession$AuthServiceConnection;

.field private mException:Ljava/lang/String;

.field private mExtras:Landroid/os/Bundle;

.field private mScope:Ljava/lang/String;

.field private mSession:Ljava/lang/String;

.field private mTimeoutMs:I

.field private mToken:Ljava/lang/String;

.field private mUIIntent:Landroid/content/Intent;


# direct methods
.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;Landroid/os/Bundle;)V
    .locals 2
    .param p1    # Ljava/lang/String;
    .param p2    # Ljava/lang/String;
    .param p3    # Landroid/os/Bundle;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Lcom/google/android/auth/GoogleAuthSession$AuthServiceConnection;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/google/android/auth/GoogleAuthSession$AuthServiceConnection;-><init>(Lcom/google/android/auth/GoogleAuthSession$1;)V

    iput-object v0, p0, Lcom/google/android/auth/GoogleAuthSession;->mAuthServiceConnection:Lcom/google/android/auth/GoogleAuthSession$AuthServiceConnection;

    const/16 v0, 0x2710

    iput v0, p0, Lcom/google/android/auth/GoogleAuthSession;->mTimeoutMs:I

    if-nez p3, :cond_0

    new-instance p3, Landroid/os/Bundle;

    invoke-direct {p3}, Landroid/os/Bundle;-><init>()V

    :cond_0
    iput-object p2, p0, Lcom/google/android/auth/GoogleAuthSession;->mScope:Ljava/lang/String;

    iput-object p3, p0, Lcom/google/android/auth/GoogleAuthSession;->mExtras:Landroid/os/Bundle;

    new-instance v0, Landroid/accounts/Account;

    const-string v1, "com.google"

    invoke-direct {v0, p1, v1}, Landroid/accounts/Account;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    iput-object v0, p0, Lcom/google/android/auth/GoogleAuthSession;->mAccount:Landroid/accounts/Account;

    return-void
.end method

.method private processResult(Landroid/os/Bundle;)V
    .locals 1
    .param p1    # Landroid/os/Bundle;

    const-string v0, "session"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/auth/GoogleAuthSession;->mSession:Ljava/lang/String;

    const-string v0, "authtoken"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/auth/GoogleAuthSession;->mToken:Ljava/lang/String;

    const-string v0, "Error"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/auth/GoogleAuthSession;->mException:Ljava/lang/String;

    const-string v0, "userRecoveryIntent"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Landroid/content/Intent;

    iput-object v0, p0, Lcom/google/android/auth/GoogleAuthSession;->mUIIntent:Landroid/content/Intent;

    return-void
.end method


# virtual methods
.method public authenticate(Landroid/content/Context;)Ljava/lang/String;
    .locals 7
    .param p1    # Landroid/content/Context;

    const/4 v3, 0x0

    iget-object v4, p0, Lcom/google/android/auth/GoogleAuthSession;->mToken:Ljava/lang/String;

    if-eqz v4, :cond_0

    iget-object v3, p0, Lcom/google/android/auth/GoogleAuthSession;->mToken:Ljava/lang/String;

    :goto_0
    return-object v3

    :cond_0
    iput-object v3, p0, Lcom/google/android/auth/GoogleAuthSession;->mException:Ljava/lang/String;

    iput-object v3, p0, Lcom/google/android/auth/GoogleAuthSession;->mUIIntent:Landroid/content/Intent;

    :try_start_0
    iget-object v4, p0, Lcom/google/android/auth/GoogleAuthSession;->mAuthServiceConnection:Lcom/google/android/auth/GoogleAuthSession$AuthServiceConnection;

    iget-object v5, p0, Lcom/google/android/auth/GoogleAuthSession;->mAccount:Landroid/accounts/Account;

    iget-object v5, v5, Landroid/accounts/Account;->type:Ljava/lang/String;

    iget v6, p0, Lcom/google/android/auth/GoogleAuthSession;->mTimeoutMs:I

    invoke-virtual {v4, p1, v5, v6}, Lcom/google/android/auth/GoogleAuthSession$AuthServiceConnection;->get(Landroid/content/Context;Ljava/lang/String;I)Lcom/google/android/auth/IAuthManagerService;

    move-result-object v0

    if-nez v0, :cond_1

    const-string v4, "AppDownloadRequired"

    iput-object v4, p0, Lcom/google/android/auth/GoogleAuthSession;->mException:Ljava/lang/String;
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_1

    goto :goto_0

    :catch_0
    move-exception v1

    const-string v4, "GoogleAuthToken"

    const-string v5, "GMS remote exception "

    invoke-static {v4, v5, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    const-string v4, "InternalError"

    iput-object v4, p0, Lcom/google/android/auth/GoogleAuthSession;->mException:Ljava/lang/String;

    goto :goto_0

    :cond_1
    :try_start_1
    iget-object v4, p0, Lcom/google/android/auth/GoogleAuthSession;->mExtras:Landroid/os/Bundle;

    const-string v5, "androidPackageName"

    invoke-virtual {v4, v5}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v4

    if-nez v4, :cond_2

    iget-object v4, p0, Lcom/google/android/auth/GoogleAuthSession;->mExtras:Landroid/os/Bundle;

    const-string v5, "androidPackageName"

    invoke-virtual {p1}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v4, v5, v6}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_1
    .catch Landroid/os/RemoteException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Ljava/lang/InterruptedException; {:try_start_1 .. :try_end_1} :catch_1

    :cond_2
    :try_start_2
    iget-object v4, p0, Lcom/google/android/auth/GoogleAuthSession;->mAccount:Landroid/accounts/Account;

    iget-object v4, v4, Landroid/accounts/Account;->name:Ljava/lang/String;

    iget-object v5, p0, Lcom/google/android/auth/GoogleAuthSession;->mScope:Ljava/lang/String;

    iget-object v6, p0, Lcom/google/android/auth/GoogleAuthSession;->mExtras:Landroid/os/Bundle;

    invoke-interface {v0, v4, v5, v6}, Lcom/google/android/auth/IAuthManagerService;->getToken(Ljava/lang/String;Ljava/lang/String;Landroid/os/Bundle;)Landroid/os/Bundle;

    move-result-object v2

    invoke-direct {p0, v2}, Lcom/google/android/auth/GoogleAuthSession;->processResult(Landroid/os/Bundle;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    :try_start_3
    iget-object v4, p0, Lcom/google/android/auth/GoogleAuthSession;->mAuthServiceConnection:Lcom/google/android/auth/GoogleAuthSession$AuthServiceConnection;

    invoke-virtual {v4, p1}, Lcom/google/android/auth/GoogleAuthSession$AuthServiceConnection;->release(Landroid/content/Context;)V

    iget-object v3, p0, Lcom/google/android/auth/GoogleAuthSession;->mToken:Ljava/lang/String;

    goto :goto_0

    :catchall_0
    move-exception v4

    iget-object v5, p0, Lcom/google/android/auth/GoogleAuthSession;->mAuthServiceConnection:Lcom/google/android/auth/GoogleAuthSession$AuthServiceConnection;

    invoke-virtual {v5, p1}, Lcom/google/android/auth/GoogleAuthSession$AuthServiceConnection;->release(Landroid/content/Context;)V

    throw v4
    :try_end_3
    .catch Landroid/os/RemoteException; {:try_start_3 .. :try_end_3} :catch_0
    .catch Ljava/lang/InterruptedException; {:try_start_3 .. :try_end_3} :catch_1

    :catch_1
    move-exception v1

    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/Thread;->interrupt()V

    const-string v4, "Interrupted"

    iput-object v4, p0, Lcom/google/android/auth/GoogleAuthSession;->mException:Ljava/lang/String;

    goto :goto_0
.end method

.method public getError()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/android/auth/GoogleAuthSession;->mException:Ljava/lang/String;

    return-object v0
.end method

.method public getIntent()Landroid/content/Intent;
    .locals 4

    iget-object v1, p0, Lcom/google/android/auth/GoogleAuthSession;->mUIIntent:Landroid/content/Intent;

    if-nez v1, :cond_0

    new-instance v0, Landroid/content/ComponentName;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v2, p0, Lcom/google/android/auth/GoogleAuthSession;->mAccount:Landroid/accounts/Account;

    iget-object v2, v2, Landroid/accounts/Account;->type:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ".android.gms"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v3, p0, Lcom/google/android/auth/GoogleAuthSession;->mAccount:Landroid/accounts/Account;

    iget-object v3, v3, Landroid/accounts/Account;->type:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ".android.gms.auth.TokenActivity"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Landroid/content/ComponentName;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    new-instance v1, Landroid/content/Intent;

    invoke-direct {v1}, Landroid/content/Intent;-><init>()V

    invoke-virtual {v1, v0}, Landroid/content/Intent;->setComponent(Landroid/content/ComponentName;)Landroid/content/Intent;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/auth/GoogleAuthSession;->mUIIntent:Landroid/content/Intent;

    iget-object v1, p0, Lcom/google/android/auth/GoogleAuthSession;->mUIIntent:Landroid/content/Intent;

    iget-object v2, p0, Lcom/google/android/auth/GoogleAuthSession;->mUIIntent:Landroid/content/Intent;

    invoke-virtual {v2}, Landroid/content/Intent;->getFlags()I

    move-result v2

    const v3, -0x10000001

    and-int/2addr v2, v3

    invoke-virtual {v1, v2}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    iget-object v1, p0, Lcom/google/android/auth/GoogleAuthSession;->mUIIntent:Landroid/content/Intent;

    const-string v2, "authAccount"

    iget-object v3, p0, Lcom/google/android/auth/GoogleAuthSession;->mAccount:Landroid/accounts/Account;

    iget-object v3, v3, Landroid/accounts/Account;->name:Ljava/lang/String;

    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    iget-object v1, p0, Lcom/google/android/auth/GoogleAuthSession;->mUIIntent:Landroid/content/Intent;

    const-string v2, "service"

    iget-object v3, p0, Lcom/google/android/auth/GoogleAuthSession;->mScope:Ljava/lang/String;

    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    iget-object v1, p0, Lcom/google/android/auth/GoogleAuthSession;->mUIIntent:Landroid/content/Intent;

    const-string v2, "callerExtras"

    iget-object v3, p0, Lcom/google/android/auth/GoogleAuthSession;->mExtras:Landroid/os/Bundle;

    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Bundle;)Landroid/content/Intent;

    iget-object v1, p0, Lcom/google/android/auth/GoogleAuthSession;->mUIIntent:Landroid/content/Intent;

    const-string v2, "session"

    iget-object v3, p0, Lcom/google/android/auth/GoogleAuthSession;->mSession:Ljava/lang/String;

    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    :cond_0
    iget-object v1, p0, Lcom/google/android/auth/GoogleAuthSession;->mUIIntent:Landroid/content/Intent;

    return-object v1
.end method

.method public hasHardError()Z
    .locals 1

    iget-object v0, p0, Lcom/google/android/auth/GoogleAuthSession;->mToken:Ljava/lang/String;

    if-nez v0, :cond_0

    invoke-virtual {p0}, Lcom/google/android/auth/GoogleAuthSession;->hasUserRecoverableError()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-virtual {p0}, Lcom/google/android/auth/GoogleAuthSession;->hasSoftError()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasSoftError()Z
    .locals 3

    const/4 v0, 0x0

    iget-object v1, p0, Lcom/google/android/auth/GoogleAuthSession;->mToken:Ljava/lang/String;

    if-eqz v1, :cond_1

    :cond_0
    :goto_0
    return v0

    :cond_1
    const-string v1, "NetworkError"

    iget-object v2, p0, Lcom/google/android/auth/GoogleAuthSession;->mException:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_2

    const-string v1, "ServiceUnavailable"

    iget-object v2, p0, Lcom/google/android/auth/GoogleAuthSession;->mException:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_2

    const-string v1, "Timeout"

    iget-object v2, p0, Lcom/google/android/auth/GoogleAuthSession;->mException:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    :cond_2
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public hasUserRecoverableError()Z
    .locals 4

    const/4 v1, 0x1

    const/4 v0, 0x0

    iget-object v2, p0, Lcom/google/android/auth/GoogleAuthSession;->mToken:Ljava/lang/String;

    if-eqz v2, :cond_1

    :cond_0
    :goto_0
    return v0

    :cond_1
    iget-object v2, p0, Lcom/google/android/auth/GoogleAuthSession;->mException:Ljava/lang/String;

    if-nez v2, :cond_2

    move v0, v1

    goto :goto_0

    :cond_2
    const-string v2, "BadAuthentication"

    iget-object v3, p0, Lcom/google/android/auth/GoogleAuthSession;->mException:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_3

    const-string v2, "CaptchaRequired"

    iget-object v3, p0, Lcom/google/android/auth/GoogleAuthSession;->mException:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_3

    const-string v2, "DeviceManagementRequiredOrSyncDisabled"

    iget-object v3, p0, Lcom/google/android/auth/GoogleAuthSession;->mException:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_3

    const-string v2, "NeedPermission"

    iget-object v3, p0, Lcom/google/android/auth/GoogleAuthSession;->mException:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_3

    const-string v2, "NeedsBrowser"

    iget-object v3, p0, Lcom/google/android/auth/GoogleAuthSession;->mException:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_3

    const-string v2, "UserCancel"

    iget-object v3, p0, Lcom/google/android/auth/GoogleAuthSession;->mException:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_3

    const-string v2, "AppDownloadRequired"

    iget-object v3, p0, Lcom/google/android/auth/GoogleAuthSession;->mException:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    :cond_3
    move v0, v1

    goto :goto_0
.end method
