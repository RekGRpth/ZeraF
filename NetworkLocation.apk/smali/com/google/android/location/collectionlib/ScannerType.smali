.class public Lcom/google/android/location/collectionlib/ScannerType;
.super Ljava/lang/Object;
.source "ScannerType.java"


# static fields
.field public static final ACCELEROMETER:Lcom/google/android/location/collectionlib/ScannerType;

.field public static final ALL:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Lcom/google/android/location/collectionlib/ScannerType;",
            ">;"
        }
    .end annotation
.end field

.field public static final ALL_SENSORS:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Lcom/google/android/location/collectionlib/ScannerType;",
            ">;"
        }
    .end annotation
.end field

.field public static final BAROMETER:Lcom/google/android/location/collectionlib/ScannerType;

.field public static final CELL:Lcom/google/android/location/collectionlib/ScannerType;

.field public static final GYROSCOPE:Lcom/google/android/location/collectionlib/ScannerType;

.field public static final MAGNETIC_FIELD:Lcom/google/android/location/collectionlib/ScannerType;

.field public static final ORIENTATION:Lcom/google/android/location/collectionlib/ScannerType;

.field public static final UNKNOWN:Lcom/google/android/location/collectionlib/ScannerType;

.field public static final WIFI:Lcom/google/android/location/collectionlib/ScannerType;


# instance fields
.field private final mValue:I


# direct methods
.method static constructor <clinit>()V
    .locals 8

    const/4 v7, 0x3

    const/4 v6, 0x0

    const/4 v5, 0x4

    const/4 v4, 0x2

    const/4 v3, 0x1

    new-instance v0, Lcom/google/android/location/collectionlib/ScannerType;

    invoke-direct {v0, v3}, Lcom/google/android/location/collectionlib/ScannerType;-><init>(I)V

    sput-object v0, Lcom/google/android/location/collectionlib/ScannerType;->WIFI:Lcom/google/android/location/collectionlib/ScannerType;

    new-instance v0, Lcom/google/android/location/collectionlib/ScannerType;

    invoke-direct {v0, v4}, Lcom/google/android/location/collectionlib/ScannerType;-><init>(I)V

    sput-object v0, Lcom/google/android/location/collectionlib/ScannerType;->CELL:Lcom/google/android/location/collectionlib/ScannerType;

    new-instance v0, Lcom/google/android/location/collectionlib/ScannerType;

    invoke-direct {v0, v5}, Lcom/google/android/location/collectionlib/ScannerType;-><init>(I)V

    sput-object v0, Lcom/google/android/location/collectionlib/ScannerType;->ORIENTATION:Lcom/google/android/location/collectionlib/ScannerType;

    new-instance v0, Lcom/google/android/location/collectionlib/ScannerType;

    const/16 v1, 0x8

    invoke-direct {v0, v1}, Lcom/google/android/location/collectionlib/ScannerType;-><init>(I)V

    sput-object v0, Lcom/google/android/location/collectionlib/ScannerType;->ACCELEROMETER:Lcom/google/android/location/collectionlib/ScannerType;

    new-instance v0, Lcom/google/android/location/collectionlib/ScannerType;

    const/16 v1, 0x10

    invoke-direct {v0, v1}, Lcom/google/android/location/collectionlib/ScannerType;-><init>(I)V

    sput-object v0, Lcom/google/android/location/collectionlib/ScannerType;->GYROSCOPE:Lcom/google/android/location/collectionlib/ScannerType;

    new-instance v0, Lcom/google/android/location/collectionlib/ScannerType;

    const/16 v1, 0x20

    invoke-direct {v0, v1}, Lcom/google/android/location/collectionlib/ScannerType;-><init>(I)V

    sput-object v0, Lcom/google/android/location/collectionlib/ScannerType;->MAGNETIC_FIELD:Lcom/google/android/location/collectionlib/ScannerType;

    new-instance v0, Lcom/google/android/location/collectionlib/ScannerType;

    const/16 v1, 0x100

    invoke-direct {v0, v1}, Lcom/google/android/location/collectionlib/ScannerType;-><init>(I)V

    sput-object v0, Lcom/google/android/location/collectionlib/ScannerType;->BAROMETER:Lcom/google/android/location/collectionlib/ScannerType;

    new-instance v0, Lcom/google/android/location/collectionlib/ScannerType;

    const/high16 v1, -0x80000000

    invoke-direct {v0, v1}, Lcom/google/android/location/collectionlib/ScannerType;-><init>(I)V

    sput-object v0, Lcom/google/android/location/collectionlib/ScannerType;->UNKNOWN:Lcom/google/android/location/collectionlib/ScannerType;

    const/4 v0, 0x7

    new-array v0, v0, [Lcom/google/android/location/collectionlib/ScannerType;

    sget-object v1, Lcom/google/android/location/collectionlib/ScannerType;->WIFI:Lcom/google/android/location/collectionlib/ScannerType;

    aput-object v1, v0, v6

    sget-object v1, Lcom/google/android/location/collectionlib/ScannerType;->CELL:Lcom/google/android/location/collectionlib/ScannerType;

    aput-object v1, v0, v3

    sget-object v1, Lcom/google/android/location/collectionlib/ScannerType;->ORIENTATION:Lcom/google/android/location/collectionlib/ScannerType;

    aput-object v1, v0, v4

    sget-object v1, Lcom/google/android/location/collectionlib/ScannerType;->ACCELEROMETER:Lcom/google/android/location/collectionlib/ScannerType;

    aput-object v1, v0, v7

    sget-object v1, Lcom/google/android/location/collectionlib/ScannerType;->GYROSCOPE:Lcom/google/android/location/collectionlib/ScannerType;

    aput-object v1, v0, v5

    const/4 v1, 0x5

    sget-object v2, Lcom/google/android/location/collectionlib/ScannerType;->MAGNETIC_FIELD:Lcom/google/android/location/collectionlib/ScannerType;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, Lcom/google/android/location/collectionlib/ScannerType;->BAROMETER:Lcom/google/android/location/collectionlib/ScannerType;

    aput-object v2, v0, v1

    invoke-static {v0}, Lcom/google/android/location/collectionlib/ScannerType;->of([Lcom/google/android/location/collectionlib/ScannerType;)Ljava/util/Set;

    move-result-object v0

    invoke-static {v0}, Ljava/util/Collections;->unmodifiableSet(Ljava/util/Set;)Ljava/util/Set;

    move-result-object v0

    sput-object v0, Lcom/google/android/location/collectionlib/ScannerType;->ALL:Ljava/util/Set;

    const/4 v0, 0x5

    new-array v0, v0, [Lcom/google/android/location/collectionlib/ScannerType;

    sget-object v1, Lcom/google/android/location/collectionlib/ScannerType;->ORIENTATION:Lcom/google/android/location/collectionlib/ScannerType;

    aput-object v1, v0, v6

    sget-object v1, Lcom/google/android/location/collectionlib/ScannerType;->ACCELEROMETER:Lcom/google/android/location/collectionlib/ScannerType;

    aput-object v1, v0, v3

    sget-object v1, Lcom/google/android/location/collectionlib/ScannerType;->GYROSCOPE:Lcom/google/android/location/collectionlib/ScannerType;

    aput-object v1, v0, v4

    sget-object v1, Lcom/google/android/location/collectionlib/ScannerType;->MAGNETIC_FIELD:Lcom/google/android/location/collectionlib/ScannerType;

    aput-object v1, v0, v7

    sget-object v1, Lcom/google/android/location/collectionlib/ScannerType;->BAROMETER:Lcom/google/android/location/collectionlib/ScannerType;

    aput-object v1, v0, v5

    invoke-static {v0}, Lcom/google/android/location/collectionlib/ScannerType;->of([Lcom/google/android/location/collectionlib/ScannerType;)Ljava/util/Set;

    move-result-object v0

    invoke-static {v0}, Ljava/util/Collections;->unmodifiableSet(Ljava/util/Set;)Ljava/util/Set;

    move-result-object v0

    sput-object v0, Lcom/google/android/location/collectionlib/ScannerType;->ALL_SENSORS:Ljava/util/Set;

    return-void
.end method

.method private constructor <init>(I)V
    .locals 0
    .param p1    # I

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput p1, p0, Lcom/google/android/location/collectionlib/ScannerType;->mValue:I

    return-void
.end method

.method public static varargs of([Lcom/google/android/location/collectionlib/ScannerType;)Ljava/util/Set;
    .locals 6
    .param p0    # [Lcom/google/android/location/collectionlib/ScannerType;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "([",
            "Lcom/google/android/location/collectionlib/ScannerType;",
            ")",
            "Ljava/util/Set",
            "<",
            "Lcom/google/android/location/collectionlib/ScannerType;",
            ">;"
        }
    .end annotation

    new-instance v3, Ljava/util/HashSet;

    array-length v5, p0

    invoke-direct {v3, v5}, Ljava/util/HashSet;-><init>(I)V

    move-object v0, p0

    array-length v2, v0

    const/4 v1, 0x0

    :goto_0
    if-ge v1, v2, :cond_0

    aget-object v4, v0, v1

    invoke-interface {v3, v4}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_0
    return-object v3
.end method

.method public static parse(I)Lcom/google/android/location/collectionlib/ScannerType;
    .locals 5
    .param p0    # I

    const/4 v2, 0x0

    const/4 v1, 0x0

    sget-object v4, Lcom/google/android/location/collectionlib/ScannerType;->ALL:Ljava/util/Set;

    invoke-interface {v4}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :cond_0
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/google/android/location/collectionlib/ScannerType;

    iget v4, v3, Lcom/google/android/location/collectionlib/ScannerType;->mValue:I

    and-int/2addr v4, p0

    if-eqz v4, :cond_0

    move-object v2, v3

    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_1
    const/4 v4, 0x1

    if-ne v1, v4, :cond_2

    :goto_1
    return-object v2

    :cond_2
    const/4 v2, 0x0

    goto :goto_1
.end method


# virtual methods
.method public toString()Ljava/lang/String;
    .locals 1

    iget v0, p0, Lcom/google/android/location/collectionlib/ScannerType;->mValue:I

    invoke-static {v0}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
