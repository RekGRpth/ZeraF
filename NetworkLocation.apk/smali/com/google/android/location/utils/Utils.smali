.class public final Lcom/google/android/location/utils/Utils;
.super Ljava/lang/Object;
.source "Utils.java"


# direct methods
.method private constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static clearField(Lcom/google/gmm/common/io/protocol/ProtoBuf;I)V
    .locals 1
    .param p0    # Lcom/google/gmm/common/io/protocol/ProtoBuf;
    .param p1    # I

    :goto_0
    if-eqz p0, :cond_0

    invoke-virtual {p0, p1}, Lcom/google/gmm/common/io/protocol/ProtoBuf;->getCount(I)I

    move-result v0

    if-lez v0, :cond_0

    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, Lcom/google/gmm/common/io/protocol/ProtoBuf;->remove(II)V

    goto :goto_0

    :cond_0
    return-void
.end method

.method public static containsOneValidGLocRequestElement(Lcom/google/gmm/common/io/protocol/ProtoBuf;)Z
    .locals 5
    .param p0    # Lcom/google/gmm/common/io/protocol/ProtoBuf;

    const/4 v4, 0x2

    const/4 v2, 0x1

    const/4 v1, 0x0

    invoke-static {p0}, Lcom/google/android/location/utils/Utils;->isSuccessfulRpc(Lcom/google/gmm/common/io/protocol/ProtoBuf;)Z

    move-result v3

    if-nez v3, :cond_1

    :cond_0
    :goto_0
    return v1

    :cond_1
    invoke-virtual {p0, v4}, Lcom/google/gmm/common/io/protocol/ProtoBuf;->has(I)Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-virtual {p0, v4}, Lcom/google/gmm/common/io/protocol/ProtoBuf;->getProtoBuf(I)Lcom/google/gmm/common/io/protocol/ProtoBuf;

    move-result-object v0

    invoke-virtual {v0, v2}, Lcom/google/gmm/common/io/protocol/ProtoBuf;->getInt(I)I

    move-result v3

    if-nez v3, :cond_0

    move v1, v2

    goto :goto_0
.end method

.method public static deleteDirResursive(Ljava/io/File;)Z
    .locals 8
    .param p0    # Ljava/io/File;

    const/4 v5, 0x1

    const/4 v6, 0x0

    invoke-virtual {p0}, Ljava/io/File;->isDirectory()Z

    move-result v7

    if-nez v7, :cond_0

    invoke-virtual {p0}, Ljava/io/File;->delete()Z

    move-result v4

    :goto_0
    return v4

    :cond_0
    const/4 v4, 0x1

    invoke-virtual {p0}, Ljava/io/File;->listFiles()[Ljava/io/File;

    move-result-object v0

    array-length v3, v0

    const/4 v2, 0x0

    :goto_1
    if-ge v2, v3, :cond_2

    aget-object v1, v0, v2

    if-eqz v4, :cond_1

    invoke-static {v1}, Lcom/google/android/location/utils/Utils;->deleteDirResursive(Ljava/io/File;)Z

    move-result v7

    if-eqz v7, :cond_1

    move v4, v5

    :goto_2
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    :cond_1
    move v4, v6

    goto :goto_2

    :cond_2
    if-eqz v4, :cond_3

    invoke-virtual {p0}, Ljava/io/File;->delete()Z

    move-result v7

    if-eqz v7, :cond_3

    move v4, v5

    :goto_3
    goto :goto_0

    :cond_3
    move v4, v6

    goto :goto_3
.end method

.method public static isSuccessfulRpc(Lcom/google/gmm/common/io/protocol/ProtoBuf;)Z
    .locals 2
    .param p0    # Lcom/google/gmm/common/io/protocol/ProtoBuf;

    const/4 v0, 0x1

    if-eqz p0, :cond_0

    invoke-virtual {p0, v0}, Lcom/google/gmm/common/io/protocol/ProtoBuf;->getInt(I)I

    move-result v1

    if-nez v1, :cond_0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
