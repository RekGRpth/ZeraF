.class public abstract Lcom/google/android/location/utils/logging/Logger;
.super Ljava/lang/Object;
.source "Logger.java"


# static fields
.field private static loggerImplementation:Lcom/google/android/location/utils/logging/LoggerInterface;


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static d(Ljava/lang/String;Ljava/lang/String;)V
    .locals 1
    .param p0    # Ljava/lang/String;
    .param p1    # Ljava/lang/String;

    sget-object v0, Lcom/google/android/location/utils/logging/Logger;->loggerImplementation:Lcom/google/android/location/utils/logging/LoggerInterface;

    if-eqz v0, :cond_0

    sget-object v0, Lcom/google/android/location/utils/logging/Logger;->loggerImplementation:Lcom/google/android/location/utils/logging/LoggerInterface;

    invoke-interface {v0, p0, p1}, Lcom/google/android/location/utils/logging/LoggerInterface;->d(Ljava/lang/String;Ljava/lang/String;)V

    :cond_0
    return-void
.end method

.method public static e(Ljava/lang/String;Ljava/lang/String;)V
    .locals 1
    .param p0    # Ljava/lang/String;
    .param p1    # Ljava/lang/String;

    sget-object v0, Lcom/google/android/location/utils/logging/Logger;->loggerImplementation:Lcom/google/android/location/utils/logging/LoggerInterface;

    if-eqz v0, :cond_0

    sget-object v0, Lcom/google/android/location/utils/logging/Logger;->loggerImplementation:Lcom/google/android/location/utils/logging/LoggerInterface;

    invoke-interface {v0, p0, p1}, Lcom/google/android/location/utils/logging/LoggerInterface;->e(Ljava/lang/String;Ljava/lang/String;)V

    :cond_0
    return-void
.end method

.method public static e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V
    .locals 1
    .param p0    # Ljava/lang/String;
    .param p1    # Ljava/lang/String;
    .param p2    # Ljava/lang/Throwable;

    sget-object v0, Lcom/google/android/location/utils/logging/Logger;->loggerImplementation:Lcom/google/android/location/utils/logging/LoggerInterface;

    if-eqz v0, :cond_0

    sget-object v0, Lcom/google/android/location/utils/logging/Logger;->loggerImplementation:Lcom/google/android/location/utils/logging/LoggerInterface;

    invoke-interface {v0, p0, p1, p2}, Lcom/google/android/location/utils/logging/LoggerInterface;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    :cond_0
    return-void
.end method

.method public static i(Ljava/lang/String;Ljava/lang/String;)V
    .locals 1
    .param p0    # Ljava/lang/String;
    .param p1    # Ljava/lang/String;

    sget-object v0, Lcom/google/android/location/utils/logging/Logger;->loggerImplementation:Lcom/google/android/location/utils/logging/LoggerInterface;

    if-eqz v0, :cond_0

    sget-object v0, Lcom/google/android/location/utils/logging/Logger;->loggerImplementation:Lcom/google/android/location/utils/logging/LoggerInterface;

    invoke-interface {v0, p0, p1}, Lcom/google/android/location/utils/logging/LoggerInterface;->i(Ljava/lang/String;Ljava/lang/String;)V

    :cond_0
    return-void
.end method

.method public static isLoggable(Ljava/lang/String;I)Z
    .locals 1
    .param p0    # Ljava/lang/String;
    .param p1    # I

    sget-object v0, Lcom/google/android/location/utils/logging/Logger;->loggerImplementation:Lcom/google/android/location/utils/logging/LoggerInterface;

    if-eqz v0, :cond_0

    sget-object v0, Lcom/google/android/location/utils/logging/Logger;->loggerImplementation:Lcom/google/android/location/utils/logging/LoggerInterface;

    invoke-interface {v0, p0, p1}, Lcom/google/android/location/utils/logging/LoggerInterface;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static declared-synchronized registerLogger(Lcom/google/android/location/utils/logging/LoggerInterface;)V
    .locals 2
    .param p0    # Lcom/google/android/location/utils/logging/LoggerInterface;

    const-class v0, Lcom/google/android/location/utils/logging/Logger;

    monitor-enter v0

    :try_start_0
    sput-object p0, Lcom/google/android/location/utils/logging/Logger;->loggerImplementation:Lcom/google/android/location/utils/logging/LoggerInterface;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit v0

    return-void

    :catchall_0
    move-exception v1

    monitor-exit v0

    throw v1
.end method

.method public static w(Ljava/lang/String;Ljava/lang/String;)V
    .locals 1
    .param p0    # Ljava/lang/String;
    .param p1    # Ljava/lang/String;

    sget-object v0, Lcom/google/android/location/utils/logging/Logger;->loggerImplementation:Lcom/google/android/location/utils/logging/LoggerInterface;

    if-eqz v0, :cond_0

    sget-object v0, Lcom/google/android/location/utils/logging/Logger;->loggerImplementation:Lcom/google/android/location/utils/logging/LoggerInterface;

    invoke-interface {v0, p0, p1}, Lcom/google/android/location/utils/logging/LoggerInterface;->w(Ljava/lang/String;Ljava/lang/String;)V

    :cond_0
    return-void
.end method
