.class public Lcom/google/android/location/data/SignalStrengthBagPredictor;
.super Ljava/lang/Object;
.source "SignalStrengthBagPredictor.java"


# instance fields
.field private final bag:Lcom/google/android/location/learning/BagOfTrees;

.field private final macAddressToAttributeIndex:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/Long;",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljava/util/Map;Lcom/google/android/location/learning/BagOfTrees;)V
    .locals 0
    .param p2    # Lcom/google/android/location/learning/BagOfTrees;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/Long;",
            "Ljava/lang/Integer;",
            ">;",
            "Lcom/google/android/location/learning/BagOfTrees;",
            ")V"
        }
    .end annotation

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/google/android/location/data/SignalStrengthBagPredictor;->macAddressToAttributeIndex:Ljava/util/Map;

    iput-object p2, p0, Lcom/google/android/location/data/SignalStrengthBagPredictor;->bag:Lcom/google/android/location/learning/BagOfTrees;

    return-void
.end method


# virtual methods
.method public computePrediction(Ljava/util/Map;)[F
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/Long;",
            "Ljava/lang/Integer;",
            ">;)[F"
        }
    .end annotation

    invoke-virtual {p0, p1}, Lcom/google/android/location/data/SignalStrengthBagPredictor;->convertSignalStrengthsToAttVec(Ljava/util/Map;)[F

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/location/data/SignalStrengthBagPredictor;->bag:Lcom/google/android/location/learning/BagOfTrees;

    invoke-virtual {v1, v0}, Lcom/google/android/location/learning/BagOfTrees;->computePrediction([F)[F

    move-result-object v1

    return-object v1
.end method

.method convertSignalStrengthsToAttVec(Ljava/util/Map;)[F
    .locals 9
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/Long;",
            "Ljava/lang/Integer;",
            ">;)[F"
        }
    .end annotation

    iget-object v6, p0, Lcom/google/android/location/data/SignalStrengthBagPredictor;->bag:Lcom/google/android/location/learning/BagOfTrees;

    iget v6, v6, Lcom/google/android/location/learning/BagOfTrees;->attributeVectorSize:I

    new-array v5, v6, [F

    iget-object v6, p0, Lcom/google/android/location/data/SignalStrengthBagPredictor;->bag:Lcom/google/android/location/learning/BagOfTrees;

    iget v6, v6, Lcom/google/android/location/learning/BagOfTrees;->attributeVectorSize:I

    new-array v0, v6, [I

    invoke-interface {p1}, Ljava/util/Map;->keySet()Ljava/util/Set;

    move-result-object v6

    invoke-interface {v6}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_0
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v6

    if-eqz v6, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/Long;

    iget-object v6, p0, Lcom/google/android/location/data/SignalStrengthBagPredictor;->macAddressToAttributeIndex:Ljava/util/Map;

    invoke-interface {v6, v4}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Integer;

    if-eqz v3, :cond_0

    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    move-result v7

    aget v8, v5, v7

    invoke-interface {p1, v4}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Ljava/lang/Integer;

    invoke-virtual {v6}, Ljava/lang/Integer;->intValue()I

    move-result v6

    int-to-float v6, v6

    add-float/2addr v6, v8

    aput v6, v5, v7

    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    move-result v6

    aget v7, v0, v6

    add-int/lit8 v7, v7, 0x1

    aput v7, v0, v6

    goto :goto_0

    :cond_1
    const/4 v1, 0x0

    :goto_1
    array-length v6, v5

    if-ge v1, v6, :cond_3

    aget v6, v0, v1

    if-nez v6, :cond_2

    const/high16 v6, -0x3d380000

    aput v6, v5, v1

    :goto_2
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    :cond_2
    aget v6, v5, v1

    aget v7, v0, v1

    int-to-float v7, v7

    div-float/2addr v6, v7

    aput v6, v5, v1

    goto :goto_2

    :cond_3
    return-object v5
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 5
    .param p1    # Ljava/lang/Object;

    const/4 v1, 0x1

    const/4 v2, 0x0

    if-ne p0, p1, :cond_1

    :cond_0
    :goto_0
    return v1

    :cond_1
    instance-of v3, p1, Lcom/google/android/location/data/SignalStrengthBagPredictor;

    if-nez v3, :cond_2

    move v1, v2

    goto :goto_0

    :cond_2
    move-object v0, p1

    check-cast v0, Lcom/google/android/location/data/SignalStrengthBagPredictor;

    iget-object v3, p0, Lcom/google/android/location/data/SignalStrengthBagPredictor;->macAddressToAttributeIndex:Ljava/util/Map;

    iget-object v4, v0, Lcom/google/android/location/data/SignalStrengthBagPredictor;->macAddressToAttributeIndex:Ljava/util/Map;

    invoke-virtual {v3, v4}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    iget-object v3, p0, Lcom/google/android/location/data/SignalStrengthBagPredictor;->bag:Lcom/google/android/location/learning/BagOfTrees;

    iget-object v4, v0, Lcom/google/android/location/data/SignalStrengthBagPredictor;->bag:Lcom/google/android/location/learning/BagOfTrees;

    invoke-virtual {v3, v4}, Lcom/google/android/location/learning/BagOfTrees;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_0

    :cond_3
    move v1, v2

    goto :goto_0
.end method

.method public hashCode()I
    .locals 3

    const/16 v0, 0x11

    iget-object v1, p0, Lcom/google/android/location/data/SignalStrengthBagPredictor;->macAddressToAttributeIndex:Ljava/util/Map;

    invoke-virtual {v1}, Ljava/lang/Object;->hashCode()I

    move-result v1

    add-int/lit16 v0, v1, 0x20f

    mul-int/lit8 v1, v0, 0x1f

    iget-object v2, p0, Lcom/google/android/location/data/SignalStrengthBagPredictor;->bag:Lcom/google/android/location/learning/BagOfTrees;

    invoke-virtual {v2}, Lcom/google/android/location/learning/BagOfTrees;->hashCode()I

    move-result v2

    add-int v0, v1, v2

    return v0
.end method

.method public numTrees()I
    .locals 1

    iget-object v0, p0, Lcom/google/android/location/data/SignalStrengthBagPredictor;->bag:Lcom/google/android/location/learning/BagOfTrees;

    invoke-virtual {v0}, Lcom/google/android/location/learning/BagOfTrees;->numTrees()I

    move-result v0

    return v0
.end method
