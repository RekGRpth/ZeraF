.class public Lcom/google/android/location/data/TemporalObject;
.super Ljava/lang/Object;
.source "TemporalObject.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;"
    }
.end annotation


# instance fields
.field private final creationTimeInMillisSinceEpoch:J

.field private volatile lastUsedTimeInMillisSinceEpoch:J

.field private final value:Ljava/lang/Object;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "TT;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljava/lang/Object;J)V
    .locals 0
    .param p2    # J
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TT;J)V"
        }
    .end annotation

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/google/android/location/data/TemporalObject;->value:Ljava/lang/Object;

    iput-wide p2, p0, Lcom/google/android/location/data/TemporalObject;->creationTimeInMillisSinceEpoch:J

    iput-wide p2, p0, Lcom/google/android/location/data/TemporalObject;->lastUsedTimeInMillisSinceEpoch:J

    return-void
.end method


# virtual methods
.method public get(J)Ljava/lang/Object;
    .locals 1
    .param p1    # J
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(J)TT;"
        }
    .end annotation

    invoke-virtual {p0, p1, p2}, Lcom/google/android/location/data/TemporalObject;->updateLastUsedTime(J)V

    iget-object v0, p0, Lcom/google/android/location/data/TemporalObject;->value:Ljava/lang/Object;

    return-object v0
.end method

.method public getCreationTimeInMillisSinceEpoch()J
    .locals 2

    iget-wide v0, p0, Lcom/google/android/location/data/TemporalObject;->creationTimeInMillisSinceEpoch:J

    return-wide v0
.end method

.method public getLastUsedTimeInMillisSinceEpoch()J
    .locals 2

    iget-wide v0, p0, Lcom/google/android/location/data/TemporalObject;->lastUsedTimeInMillisSinceEpoch:J

    return-wide v0
.end method

.method public getWithoutUpdatingLastUsedTime()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()TT;"
        }
    .end annotation

    iget-object v0, p0, Lcom/google/android/location/data/TemporalObject;->value:Ljava/lang/Object;

    return-object v0
.end method

.method public updateLastUsedTime(J)V
    .locals 0
    .param p1    # J

    iput-wide p1, p0, Lcom/google/android/location/data/TemporalObject;->lastUsedTimeInMillisSinceEpoch:J

    return-void
.end method
