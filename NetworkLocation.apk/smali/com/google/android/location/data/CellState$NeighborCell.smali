.class public Lcom/google/android/location/data/CellState$NeighborCell;
.super Ljava/lang/Object;
.source "CellState.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/location/data/CellState;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "NeighborCell"
.end annotation


# instance fields
.field private final mCid:I

.field private final mLac:I

.field private final mPsc:I

.field private final mRssi:I


# direct methods
.method public constructor <init>(IIII)V
    .locals 0
    .param p1    # I
    .param p2    # I
    .param p3    # I
    .param p4    # I

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput p1, p0, Lcom/google/android/location/data/CellState$NeighborCell;->mCid:I

    iput p2, p0, Lcom/google/android/location/data/CellState$NeighborCell;->mLac:I

    iput p3, p0, Lcom/google/android/location/data/CellState$NeighborCell;->mPsc:I

    iput p4, p0, Lcom/google/android/location/data/CellState$NeighborCell;->mRssi:I

    return-void
.end method


# virtual methods
.method public gcell(Lcom/google/android/location/data/CellState;)Lcom/google/gmm/common/io/protocol/ProtoBuf;
    .locals 7
    .param p1    # Lcom/google/android/location/data/CellState;

    const/4 v6, -0x1

    new-instance v0, Lcom/google/gmm/common/io/protocol/ProtoBuf;

    sget-object v4, Lcom/google/android/location/protocol/LocserverMessageTypes;->GCELL:Lcom/google/gmm/common/io/protocol/ProtoBufType;

    invoke-direct {v0, v4}, Lcom/google/gmm/common/io/protocol/ProtoBuf;-><init>(Lcom/google/gmm/common/io/protocol/ProtoBufType;)V

    const/4 v4, 0x2

    iget v5, p0, Lcom/google/android/location/data/CellState$NeighborCell;->mCid:I

    invoke-virtual {v0, v4, v5}, Lcom/google/gmm/common/io/protocol/ProtoBuf;->setInt(II)V

    const/4 v4, 0x1

    iget v5, p0, Lcom/google/android/location/data/CellState$NeighborCell;->mLac:I

    invoke-virtual {v0, v4, v5}, Lcom/google/gmm/common/io/protocol/ProtoBuf;->setInt(II)V

    const/4 v4, 0x5

    iget v5, p0, Lcom/google/android/location/data/CellState$NeighborCell;->mRssi:I

    invoke-virtual {v0, v4, v5}, Lcom/google/gmm/common/io/protocol/ProtoBuf;->setInt(II)V

    iget v4, p0, Lcom/google/android/location/data/CellState$NeighborCell;->mPsc:I

    if-eq v4, v6, :cond_0

    const/16 v4, 0x8

    iget v5, p0, Lcom/google/android/location/data/CellState$NeighborCell;->mPsc:I

    invoke-virtual {v0, v4, v5}, Lcom/google/gmm/common/io/protocol/ProtoBuf;->setInt(II)V

    :cond_0
    invoke-virtual {p1}, Lcom/google/android/location/data/CellState;->getMcc()I

    move-result v1

    if-eq v1, v6, :cond_1

    const/4 v4, 0x4

    invoke-virtual {v0, v4, v1}, Lcom/google/gmm/common/io/protocol/ProtoBuf;->setInt(II)V

    :cond_1
    invoke-virtual {p1}, Lcom/google/android/location/data/CellState;->getMnc()I

    move-result v2

    if-eq v2, v6, :cond_2

    const/4 v4, 0x3

    invoke-virtual {v0, v4, v2}, Lcom/google/gmm/common/io/protocol/ProtoBuf;->setInt(II)V

    :cond_2
    invoke-virtual {p1}, Lcom/google/android/location/data/CellState;->getGCellRadioType()I

    move-result v3

    if-eq v3, v6, :cond_3

    const/16 v4, 0xa

    invoke-virtual {v0, v4, v3}, Lcom/google/gmm/common/io/protocol/ProtoBuf;->setInt(II)V

    :cond_3
    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    iget v0, p0, Lcom/google/android/location/data/CellState$NeighborCell;->mPsc:I

    const/4 v1, -0x1

    if-eq v0, v1, :cond_0

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iget v1, p0, Lcom/google/android/location/data/CellState$NeighborCell;->mPsc:I

    invoke-static {v1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "@"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/google/android/location/data/CellState$NeighborCell;->mRssi:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iget v1, p0, Lcom/google/android/location/data/CellState$NeighborCell;->mCid:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ":"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/google/android/location/data/CellState$NeighborCell;->mLac:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "@"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/google/android/location/data/CellState$NeighborCell;->mRssi:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method
