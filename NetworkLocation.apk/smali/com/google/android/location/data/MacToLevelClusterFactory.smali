.class public Lcom/google/android/location/data/MacToLevelClusterFactory;
.super Ljava/lang/Object;
.source "MacToLevelClusterFactory.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static create(Lcom/google/gmm/common/io/protocol/ProtoBuf;)Ljava/util/Map;
    .locals 10
    .param p0    # Lcom/google/gmm/common/io/protocol/ProtoBuf;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/gmm/common/io/protocol/ProtoBuf;",
            ")",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/Long;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    const/4 v9, 0x2

    const/4 v8, 0x1

    new-instance v6, Ljava/util/HashMap;

    invoke-direct {v6}, Ljava/util/HashMap;-><init>()V

    const/4 v2, 0x0

    :goto_0
    invoke-virtual {p0, v8}, Lcom/google/gmm/common/io/protocol/ProtoBuf;->getCount(I)I

    move-result v7

    if-ge v2, v7, :cond_1

    invoke-virtual {p0, v8, v2}, Lcom/google/gmm/common/io/protocol/ProtoBuf;->getProtoBuf(II)Lcom/google/gmm/common/io/protocol/ProtoBuf;

    move-result-object v0

    invoke-virtual {v0, v8}, Lcom/google/gmm/common/io/protocol/ProtoBuf;->getString(I)Ljava/lang/String;

    move-result-object v1

    const/4 v3, 0x0

    :goto_1
    invoke-virtual {v0, v9}, Lcom/google/gmm/common/io/protocol/ProtoBuf;->getCount(I)I

    move-result v7

    if-ge v3, v7, :cond_0

    invoke-virtual {v0, v9, v3}, Lcom/google/gmm/common/io/protocol/ProtoBuf;->getLong(II)J

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v7

    invoke-interface {v6, v7, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    add-int/lit8 v3, v3, 0x1

    goto :goto_1

    :cond_0
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    :cond_1
    return-object v6
.end method
