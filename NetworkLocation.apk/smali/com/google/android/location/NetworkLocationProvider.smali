.class Lcom/google/android/location/NetworkLocationProvider;
.super Lcom/android/location/provider/LocationProviderBase;
.source "NetworkLocationProvider.java"

# interfaces
.implements Landroid/location/LocationListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/location/NetworkLocationProvider$ProviderHandler;
    }
.end annotation


# static fields
.field private static PROPERTIES:Lcom/android/location/provider/ProviderPropertiesUnbundled;

.field private static context:Landroid/content/Context;

.field private static instance:Lcom/google/android/location/NetworkLocationProvider;


# instance fields
.field private final client:Lcom/google/android/location/internal/client/NetworkLocationClient;

.field private final lock:Ljava/lang/Object;

.field private final mHandler:Lcom/google/android/location/NetworkLocationProvider$ProviderHandler;

.field private statusUpdateTime:J


# direct methods
.method static constructor <clinit>()V
    .locals 9

    const/4 v0, 0x1

    const/4 v1, 0x0

    move v2, v0

    move v3, v1

    move v4, v1

    move v5, v1

    move v6, v1

    move v7, v0

    move v8, v0

    invoke-static/range {v0 .. v8}, Lcom/android/location/provider/ProviderPropertiesUnbundled;->create(ZZZZZZZII)Lcom/android/location/provider/ProviderPropertiesUnbundled;

    move-result-object v0

    sput-object v0, Lcom/google/android/location/NetworkLocationProvider;->PROPERTIES:Lcom/android/location/provider/ProviderPropertiesUnbundled;

    return-void
.end method

.method private constructor <init>()V
    .locals 8

    const/4 v7, -0x1

    const/4 v4, 0x1

    const-string v3, "NetworkLocationProvider"

    sget-object v5, Lcom/google/android/location/NetworkLocationProvider;->PROPERTIES:Lcom/android/location/provider/ProviderPropertiesUnbundled;

    invoke-direct {p0, v3, v5}, Lcom/android/location/provider/LocationProviderBase;-><init>(Ljava/lang/String;Lcom/android/location/provider/ProviderPropertiesUnbundled;)V

    new-instance v3, Ljava/lang/Object;

    invoke-direct {v3}, Ljava/lang/Object;-><init>()V

    iput-object v3, p0, Lcom/google/android/location/NetworkLocationProvider;->lock:Ljava/lang/Object;

    const-wide/16 v5, 0x0

    iput-wide v5, p0, Lcom/google/android/location/NetworkLocationProvider;->statusUpdateTime:J

    sget-object v3, Lcom/google/android/location/NetworkLocationProvider;->context:Landroid/content/Context;

    if-nez v3, :cond_0

    new-instance v3, Ljava/lang/RuntimeException;

    const-string v4, "must call init"

    invoke-direct {v3, v4}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v3

    :cond_0
    new-instance v3, Lcom/google/android/location/NetworkLocationProvider$ProviderHandler;

    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v5

    invoke-direct {v3, p0, v5}, Lcom/google/android/location/NetworkLocationProvider$ProviderHandler;-><init>(Lcom/google/android/location/NetworkLocationProvider;Landroid/os/Looper;)V

    iput-object v3, p0, Lcom/google/android/location/NetworkLocationProvider;->mHandler:Lcom/google/android/location/NetworkLocationProvider$ProviderHandler;

    sget-object v3, Lcom/google/android/location/NetworkLocationProvider;->context:Landroid/content/Context;

    invoke-static {v3}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v2

    const-string v3, "user-confirmed"

    invoke-interface {v2, v3, v7}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v1

    if-eq v1, v7, :cond_1

    if-ne v1, v4, :cond_2

    move v3, v4

    :goto_0
    invoke-direct {p0, v3}, Lcom/google/android/location/NetworkLocationProvider;->setUserConfirmedPreference(Z)V

    invoke-interface {v2}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->clear()Landroid/content/SharedPreferences$Editor;

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    :cond_1
    sget-object v3, Lcom/google/android/location/NetworkLocationProvider;->context:Landroid/content/Context;

    invoke-virtual {v3}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v3

    const-string v5, "location_providers_allowed"

    invoke-static {v5}, Landroid/provider/Settings$Secure;->getUriFor(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v5

    new-instance v6, Lcom/google/android/location/NetworkLocationProvider$1;

    iget-object v7, p0, Lcom/google/android/location/NetworkLocationProvider;->mHandler:Lcom/google/android/location/NetworkLocationProvider$ProviderHandler;

    invoke-direct {v6, p0, v7}, Lcom/google/android/location/NetworkLocationProvider$1;-><init>(Lcom/google/android/location/NetworkLocationProvider;Landroid/os/Handler;)V

    invoke-virtual {v3, v5, v4, v6}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;)V

    invoke-direct {p0}, Lcom/google/android/location/NetworkLocationProvider;->applySettings()V

    new-instance v3, Lcom/google/android/location/internal/client/NetworkLocationClient;

    sget-object v4, Lcom/google/android/location/NetworkLocationProvider;->context:Landroid/content/Context;

    const v5, 0x7fffffff

    iget-object v6, p0, Lcom/google/android/location/NetworkLocationProvider;->mHandler:Lcom/google/android/location/NetworkLocationProvider$ProviderHandler;

    invoke-virtual {v6}, Lcom/google/android/location/NetworkLocationProvider$ProviderHandler;->getLooper()Landroid/os/Looper;

    move-result-object v6

    invoke-direct {v3, v4, v5, p0, v6}, Lcom/google/android/location/internal/client/NetworkLocationClient;-><init>(Landroid/content/Context;ILandroid/location/LocationListener;Landroid/os/Looper;)V

    iput-object v3, p0, Lcom/google/android/location/NetworkLocationProvider;->client:Lcom/google/android/location/internal/client/NetworkLocationClient;

    return-void

    :cond_2
    const/4 v3, 0x0

    goto :goto_0
.end method

.method static synthetic access$000(Lcom/google/android/location/NetworkLocationProvider;)V
    .locals 0
    .param p0    # Lcom/google/android/location/NetworkLocationProvider;

    invoke-direct {p0}, Lcom/google/android/location/NetworkLocationProvider;->applySettings()V

    return-void
.end method

.method static synthetic access$100(Lcom/google/android/location/NetworkLocationProvider;I)V
    .locals 0
    .param p0    # Lcom/google/android/location/NetworkLocationProvider;
    .param p1    # I

    invoke-direct {p0, p1}, Lcom/google/android/location/NetworkLocationProvider;->handleSetMinTime(I)V

    return-void
.end method

.method private applySettings()V
    .locals 7

    const/4 v2, 0x0

    const/4 v6, 0x0

    sget-object v0, Lcom/google/android/location/NetworkLocationProvider;->context:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const-string v1, "network"

    invoke-static {v0, v1}, Landroid/provider/Settings$Secure;->isLocationProviderEnabled(Landroid/content/ContentResolver;Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2

    sget-object v0, Lcom/google/android/location/NetworkLocationProvider;->context:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    sget-object v1, Lcom/google/android/gsf/GoogleSettingsContract$Partner;->CONTENT_URI:Landroid/net/Uri;

    const-string v3, "(name=?)"

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/String;

    const-string v5, "network_location_opt_in"

    aput-object v5, v4, v6

    move-object v5, v2

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v0

    if-nez v0, :cond_1

    const-string v0, "NetworkLocationProvider"

    const-string v1, "applySettings(): provider not available"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    :goto_0
    return-void

    :cond_1
    invoke-interface {v0}, Landroid/database/Cursor;->close()V

    sget-object v0, Lcom/google/android/location/NetworkLocationProvider;->context:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const-string v1, "network_location_opt_in"

    invoke-static {v0, v1, v6}, Lcom/google/android/gsf/GoogleSettingsContract$Partner;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Lcom/google/android/location/NetworkLocationProvider;->context:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const-string v1, "network"

    invoke-static {v0, v1, v6}, Landroid/provider/Settings$Secure;->setLocationProviderEnabled(Landroid/content/ContentResolver;Ljava/lang/String;Z)V

    new-instance v0, Landroid/content/Intent;

    sget-object v1, Lcom/google/android/location/NetworkLocationProvider;->context:Landroid/content/Context;

    const-class v2, Lcom/google/android/location/ConfirmAlertActivity;

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const/high16 v1, 0x10000000

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    sget-object v1, Lcom/google/android/location/NetworkLocationProvider;->context:Landroid/content/Context;

    invoke-virtual {v1, v0}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    goto :goto_0

    :cond_2
    invoke-direct {p0, v6}, Lcom/google/android/location/NetworkLocationProvider;->setUserConfirmedPreference(Z)V

    goto :goto_0
.end method

.method public static declared-synchronized getInstance()Lcom/google/android/location/NetworkLocationProvider;
    .locals 2

    const-class v0, Lcom/google/android/location/NetworkLocationProvider;

    monitor-enter v0

    :try_start_0
    sget-object v1, Lcom/google/android/location/NetworkLocationProvider;->instance:Lcom/google/android/location/NetworkLocationProvider;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit v0

    return-object v1

    :catchall_0
    move-exception v1

    monitor-exit v0

    throw v1
.end method

.method private handleSetMinTime(I)V
    .locals 1
    .param p1    # I

    iget-object v0, p0, Lcom/google/android/location/NetworkLocationProvider;->client:Lcom/google/android/location/internal/client/NetworkLocationClient;

    invoke-virtual {v0, p1}, Lcom/google/android/location/internal/client/NetworkLocationClient;->changePeriod(I)V

    return-void
.end method

.method public static declared-synchronized init(Landroid/content/Context;)V
    .locals 2
    .param p0    # Landroid/content/Context;

    const-class v1, Lcom/google/android/location/NetworkLocationProvider;

    monitor-enter v1

    :try_start_0
    sput-object p0, Lcom/google/android/location/NetworkLocationProvider;->context:Landroid/content/Context;

    sget-object v0, Lcom/google/android/location/NetworkLocationProvider;->instance:Lcom/google/android/location/NetworkLocationProvider;

    if-nez v0, :cond_0

    new-instance v0, Lcom/google/android/location/NetworkLocationProvider;

    invoke-direct {v0}, Lcom/google/android/location/NetworkLocationProvider;-><init>()V

    sput-object v0, Lcom/google/android/location/NetworkLocationProvider;->instance:Lcom/google/android/location/NetworkLocationProvider;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :cond_0
    monitor-exit v1

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method private setUserConfirmedPreference(Z)V
    .locals 4
    .param p1    # Z

    sget-object v1, Lcom/google/android/location/NetworkLocationProvider;->context:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    const-string v3, "network_location_opt_in"

    if-eqz p1, :cond_1

    const-string v1, "1"

    :goto_0
    invoke-static {v2, v3, v1}, Lcom/google/android/gsf/GoogleSettingsContract$Partner;->putString(Landroid/content/ContentResolver;Ljava/lang/String;Ljava/lang/String;)Z

    if-nez p1, :cond_0

    sget-object v1, Lcom/google/android/location/internal/NlpVersionInfo$NlpApk;->ANDROID:Lcom/google/android/location/internal/NlpVersionInfo$NlpApk;

    sget-object v2, Lcom/google/android/location/NetworkLocationProvider;->context:Landroid/content/Context;

    invoke-static {v1, v2}, Lcom/google/android/location/internal/NlpVersionInfo;->getNlpVersionInfo(Lcom/google/android/location/internal/NlpVersionInfo$NlpApk;Landroid/content/Context;)Lcom/google/android/location/internal/NlpVersionInfo;

    move-result-object v0

    iget-object v1, v0, Lcom/google/android/location/internal/NlpVersionInfo;->intent:Landroid/content/Intent;

    if-eqz v1, :cond_0

    sget-object v1, Lcom/google/android/location/NetworkLocationProvider;->context:Landroid/content/Context;

    iget-object v2, v0, Lcom/google/android/location/internal/NlpVersionInfo;->intent:Landroid/content/Intent;

    invoke-virtual {v1, v2}, Landroid/content/Context;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;

    :cond_0
    return-void

    :cond_1
    const-string v1, "0"

    goto :goto_0
.end method


# virtual methods
.method public onDisable()V
    .locals 0

    return-void
.end method

.method public onDump(Ljava/io/FileDescriptor;Ljava/io/PrintWriter;[Ljava/lang/String;)V
    .locals 1
    .param p1    # Ljava/io/FileDescriptor;
    .param p2    # Ljava/io/PrintWriter;
    .param p3    # [Ljava/lang/String;

    invoke-static {}, Landroid/os/Binder;->clearCallingIdentity()J

    iget-object v0, p0, Lcom/google/android/location/NetworkLocationProvider;->client:Lcom/google/android/location/internal/client/NetworkLocationClient;

    invoke-virtual {v0}, Lcom/google/android/location/internal/client/NetworkLocationClient;->getDebugDump()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    return-void
.end method

.method public onEnable()V
    .locals 0

    return-void
.end method

.method public onGetStatus(Landroid/os/Bundle;)I
    .locals 4
    .param p1    # Landroid/os/Bundle;

    invoke-static {}, Landroid/os/Binder;->clearCallingIdentity()J

    sget-object v2, Lcom/google/android/location/NetworkLocationProvider;->context:Landroid/content/Context;

    const-string v3, "connectivity"

    invoke-virtual {v2, v3}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/net/ConnectivityManager;

    invoke-virtual {v0}, Landroid/net/ConnectivityManager;->getActiveNetworkInfo()Landroid/net/NetworkInfo;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-virtual {v1}, Landroid/net/NetworkInfo;->isAvailable()Z

    move-result v2

    if-eqz v2, :cond_0

    const/4 v2, 0x2

    :goto_0
    return v2

    :cond_0
    const/4 v2, 0x1

    goto :goto_0
.end method

.method public onGetStatusUpdateTime()J
    .locals 4

    invoke-static {}, Landroid/os/Binder;->clearCallingIdentity()J

    iget-object v1, p0, Lcom/google/android/location/NetworkLocationProvider;->lock:Ljava/lang/Object;

    monitor-enter v1

    :try_start_0
    iget-wide v2, p0, Lcom/google/android/location/NetworkLocationProvider;->statusUpdateTime:J

    monitor-exit v1

    return-wide v2

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public onLocationChanged(Landroid/location/Location;)V
    .locals 0
    .param p1    # Landroid/location/Location;

    invoke-virtual {p0, p1}, Lcom/google/android/location/NetworkLocationProvider;->reportLocation(Landroid/location/Location;)V

    return-void
.end method

.method public onProviderDisabled(Ljava/lang/String;)V
    .locals 0
    .param p1    # Ljava/lang/String;

    return-void
.end method

.method public onProviderEnabled(Ljava/lang/String;)V
    .locals 0
    .param p1    # Ljava/lang/String;

    return-void
.end method

.method public onSetRequest(Lcom/android/location/provider/ProviderRequestUnbundled;Landroid/os/WorkSource;)V
    .locals 8
    .param p1    # Lcom/android/location/provider/ProviderRequestUnbundled;
    .param p2    # Landroid/os/WorkSource;

    invoke-static {}, Landroid/os/Binder;->clearCallingIdentity()J

    invoke-virtual {p1}, Lcom/android/location/provider/ProviderRequestUnbundled;->getReportLocation()Z

    move-result v4

    if-nez v4, :cond_0

    const v1, 0x7fffffff

    :goto_0
    iget-object v4, p0, Lcom/google/android/location/NetworkLocationProvider;->mHandler:Lcom/google/android/location/NetworkLocationProvider$ProviderHandler;

    const/4 v5, 0x1

    const/4 v6, 0x0

    invoke-static {v4, v5, v1, v6}, Landroid/os/Message;->obtain(Landroid/os/Handler;III)Landroid/os/Message;

    move-result-object v0

    iget-object v4, p0, Lcom/google/android/location/NetworkLocationProvider;->mHandler:Lcom/google/android/location/NetworkLocationProvider$ProviderHandler;

    invoke-virtual {v4, v0}, Lcom/google/android/location/NetworkLocationProvider$ProviderHandler;->sendMessage(Landroid/os/Message;)Z

    return-void

    :cond_0
    invoke-virtual {p1}, Lcom/android/location/provider/ProviderRequestUnbundled;->getInterval()J

    move-result-wide v4

    const-wide/16 v6, 0x3e8

    div-long v2, v4, v6

    long-to-int v1, v2

    int-to-long v4, v1

    cmp-long v4, v4, v2

    if-eqz v4, :cond_1

    const-string v4, "NetworkLocationProvider"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "interval is too big: "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {p1}, Lcom/android/location/provider/ProviderRequestUnbundled;->getInterval()J

    move-result-wide v6

    invoke-virtual {v5, v6, v7}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    const v1, 0x7fffffff

    :cond_1
    const/16 v4, 0x14

    invoke-static {v1, v4}, Ljava/lang/Math;->max(II)I

    move-result v1

    goto :goto_0
.end method

.method public onStatusChanged(Ljava/lang/String;ILandroid/os/Bundle;)V
    .locals 0
    .param p1    # Ljava/lang/String;
    .param p2    # I
    .param p3    # Landroid/os/Bundle;

    return-void
.end method

.method userConfirmedEnable(Z)V
    .locals 2
    .param p1    # Z

    if-eqz p1, :cond_0

    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/google/android/location/NetworkLocationProvider;->setUserConfirmedPreference(Z)V

    :cond_0
    sget-object v0, Lcom/google/android/location/NetworkLocationProvider;->context:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const-string v1, "network"

    invoke-static {v0, v1, p1}, Landroid/provider/Settings$Secure;->setLocationProviderEnabled(Landroid/content/ContentResolver;Ljava/lang/String;Z)V

    return-void
.end method
