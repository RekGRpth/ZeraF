.class public Lcom/google/android/location/cache/TemporalLRUCacheSavers;
.super Ljava/lang/Object;
.source "TemporalLRUCacheSavers.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/location/cache/TemporalLRUCacheSavers$BaseTemporalLRUCacheSaver;,
        Lcom/google/android/location/cache/TemporalLRUCacheSavers$TemporalLRUCacheSaver;
    }
.end annotation


# static fields
.field public static final LONG_TO_STRING_SAVER:Lcom/google/android/location/cache/TemporalLRUCacheSavers$TemporalLRUCacheSaver;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/location/cache/TemporalLRUCacheSavers$TemporalLRUCacheSaver",
            "<",
            "Ljava/lang/Long;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public static final STRING_TO_STRING_SAVER:Lcom/google/android/location/cache/TemporalLRUCacheSavers$TemporalLRUCacheSaver;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/location/cache/TemporalLRUCacheSavers$TemporalLRUCacheSaver",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    invoke-static {}, Lcom/google/android/location/cache/TemporalLRUCacheSavers;->createLongToStringSaver()Lcom/google/android/location/cache/TemporalLRUCacheSavers$TemporalLRUCacheSaver;

    move-result-object v0

    sput-object v0, Lcom/google/android/location/cache/TemporalLRUCacheSavers;->LONG_TO_STRING_SAVER:Lcom/google/android/location/cache/TemporalLRUCacheSavers$TemporalLRUCacheSaver;

    invoke-static {}, Lcom/google/android/location/cache/TemporalLRUCacheSavers;->createStringToStringSaver()Lcom/google/android/location/cache/TemporalLRUCacheSavers$TemporalLRUCacheSaver;

    move-result-object v0

    sput-object v0, Lcom/google/android/location/cache/TemporalLRUCacheSavers;->STRING_TO_STRING_SAVER:Lcom/google/android/location/cache/TemporalLRUCacheSavers$TemporalLRUCacheSaver;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private static createLongToStringSaver()Lcom/google/android/location/cache/TemporalLRUCacheSavers$TemporalLRUCacheSaver;
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/android/location/cache/TemporalLRUCacheSavers$TemporalLRUCacheSaver",
            "<",
            "Ljava/lang/Long;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    const/4 v1, 0x1

    new-instance v0, Lcom/google/android/location/cache/TemporalLRUCacheSavers$1;

    sget-object v2, Lcom/google/android/location/protocol/LocserverMessageTypes;->NLP_TEMPORAL_LONG_TO_STRING_CACHE:Lcom/google/gmm/common/io/protocol/ProtoBufType;

    sget-object v3, Lcom/google/android/location/protocol/LocserverMessageTypes;->NLP_TEMPORAL_LONG_TO_STRING_ENTRY:Lcom/google/gmm/common/io/protocol/ProtoBufType;

    const/4 v5, 0x3

    const/4 v6, 0x4

    move v4, v1

    invoke-direct/range {v0 .. v6}, Lcom/google/android/location/cache/TemporalLRUCacheSavers$1;-><init>(ILcom/google/gmm/common/io/protocol/ProtoBufType;Lcom/google/gmm/common/io/protocol/ProtoBufType;III)V

    return-object v0
.end method

.method private static createStringToStringSaver()Lcom/google/android/location/cache/TemporalLRUCacheSavers$TemporalLRUCacheSaver;
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/android/location/cache/TemporalLRUCacheSavers$TemporalLRUCacheSaver",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    const/4 v1, 0x1

    new-instance v0, Lcom/google/android/location/cache/TemporalLRUCacheSavers$2;

    sget-object v2, Lcom/google/android/location/protocol/LocserverMessageTypes;->NLP_TEMPORAL_STRING_TO_STRING_CACHE:Lcom/google/gmm/common/io/protocol/ProtoBufType;

    sget-object v3, Lcom/google/android/location/protocol/LocserverMessageTypes;->NLP_TEMPORAL_STRING_TO_STRING_ENTRY:Lcom/google/gmm/common/io/protocol/ProtoBufType;

    const/4 v5, 0x3

    const/4 v6, 0x4

    move v4, v1

    invoke-direct/range {v0 .. v6}, Lcom/google/android/location/cache/TemporalLRUCacheSavers$2;-><init>(ILcom/google/gmm/common/io/protocol/ProtoBufType;Lcom/google/gmm/common/io/protocol/ProtoBufType;III)V

    return-object v0
.end method
