.class Lcom/google/android/location/cache/DiskTemporalCache$DiskTemporalLRUCache;
.super Lcom/google/android/location/cache/TemporalLRUCache;
.source "DiskTemporalCache.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/location/cache/DiskTemporalCache;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "DiskTemporalLRUCache"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<K:",
        "Ljava/lang/Object;",
        ">",
        "Lcom/google/android/location/cache/TemporalLRUCache",
        "<TK;",
        "Ljava/lang/String;",
        ">;"
    }
.end annotation


# instance fields
.field private final cacheDir:Ljava/io/File;

.field private final executor:Ljava/util/concurrent/ExecutorService;


# direct methods
.method constructor <init>(ILjava/util/concurrent/ExecutorService;Ljava/io/File;)V
    .locals 0
    .param p1    # I
    .param p2    # Ljava/util/concurrent/ExecutorService;
    .param p3    # Ljava/io/File;

    invoke-direct {p0, p1}, Lcom/google/android/location/cache/TemporalLRUCache;-><init>(I)V

    iput-object p2, p0, Lcom/google/android/location/cache/DiskTemporalCache$DiskTemporalLRUCache;->executor:Ljava/util/concurrent/ExecutorService;

    iput-object p3, p0, Lcom/google/android/location/cache/DiskTemporalCache$DiskTemporalLRUCache;->cacheDir:Ljava/io/File;

    return-void
.end method

.method static synthetic access$000(Lcom/google/android/location/cache/DiskTemporalCache$DiskTemporalLRUCache;)Ljava/io/File;
    .locals 1
    .param p0    # Lcom/google/android/location/cache/DiskTemporalCache$DiskTemporalLRUCache;

    iget-object v0, p0, Lcom/google/android/location/cache/DiskTemporalCache$DiskTemporalLRUCache;->cacheDir:Ljava/io/File;

    return-object v0
.end method


# virtual methods
.method protected onRemoveEntry(Ljava/util/Map$Entry;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map$Entry",
            "<TK;",
            "Lcom/google/android/location/data/TemporalObject",
            "<",
            "Ljava/lang/String;",
            ">;>;)V"
        }
    .end annotation

    invoke-interface {p1}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/location/data/TemporalObject;

    iget-object v1, p0, Lcom/google/android/location/cache/DiskTemporalCache$DiskTemporalLRUCache;->executor:Ljava/util/concurrent/ExecutorService;

    new-instance v2, Lcom/google/android/location/cache/DiskTemporalCache$DiskTemporalLRUCache$1;

    invoke-direct {v2, p0, v0}, Lcom/google/android/location/cache/DiskTemporalCache$DiskTemporalLRUCache$1;-><init>(Lcom/google/android/location/cache/DiskTemporalCache$DiskTemporalLRUCache;Lcom/google/android/location/data/TemporalObject;)V

    invoke-interface {v1, v2}, Ljava/util/concurrent/ExecutorService;->submit(Ljava/lang/Runnable;)Ljava/util/concurrent/Future;

    return-void
.end method
