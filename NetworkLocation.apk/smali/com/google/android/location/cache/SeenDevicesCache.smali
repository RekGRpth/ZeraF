.class public Lcom/google/android/location/cache/SeenDevicesCache;
.super Ljava/lang/Object;
.source "SeenDevicesCache.java"

# interfaces
.implements Lcom/google/common/base/Predicate;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/location/cache/SeenDevicesCache$LruCache;,
        Lcom/google/android/location/cache/SeenDevicesCache$SeenWifiAp;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/google/common/base/Predicate",
        "<",
        "Lcom/google/gmm/common/io/protocol/ProtoBuf;",
        ">;"
    }
.end annotation


# instance fields
.field private final dataStore:Lcom/google/android/location/os/SimpleProtoBufStore;

.field private lastRefreshIsSuccess:Z

.field private lastRefreshMillisSinceBoot:J

.field private final nlpParamsState:Lcom/google/android/location/os/NlpParametersState;

.field private final os:Lcom/google/android/location/os/Os;

.field private final wifiApCache:Lcom/google/android/location/cache/SeenDevicesCache$LruCache;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/location/cache/SeenDevicesCache$LruCache",
            "<",
            "Ljava/lang/Long;",
            "Lcom/google/android/location/cache/SeenDevicesCache$SeenWifiAp;",
            ">;"
        }
    .end annotation
.end field

.field private final wifiCacheStats:Lcom/google/android/location/cache/Stats;


# direct methods
.method public constructor <init>(Lcom/google/android/location/os/Os;Lcom/google/android/location/os/NlpParametersState;)V
    .locals 7
    .param p1    # Lcom/google/android/location/os/Os;
    .param p2    # Lcom/google/android/location/os/NlpParametersState;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Lcom/google/android/location/cache/Stats;

    invoke-direct {v0}, Lcom/google/android/location/cache/Stats;-><init>()V

    iput-object v0, p0, Lcom/google/android/location/cache/SeenDevicesCache;->wifiCacheStats:Lcom/google/android/location/cache/Stats;

    new-instance v0, Lcom/google/android/location/cache/SeenDevicesCache$LruCache;

    const/16 v1, 0x3e8

    iget-object v2, p0, Lcom/google/android/location/cache/SeenDevicesCache;->wifiCacheStats:Lcom/google/android/location/cache/Stats;

    invoke-direct {v0, v1, v2}, Lcom/google/android/location/cache/SeenDevicesCache$LruCache;-><init>(ILcom/google/android/location/cache/Stats;)V

    iput-object v0, p0, Lcom/google/android/location/cache/SeenDevicesCache;->wifiApCache:Lcom/google/android/location/cache/SeenDevicesCache$LruCache;

    iput-object p1, p0, Lcom/google/android/location/cache/SeenDevicesCache;->os:Lcom/google/android/location/os/Os;

    iput-object p2, p0, Lcom/google/android/location/cache/SeenDevicesCache;->nlpParamsState:Lcom/google/android/location/os/NlpParametersState;

    new-instance v0, Lcom/google/android/location/os/SimpleProtoBufStore;

    const/4 v1, 0x2

    sget-object v2, Lcom/google/android/location/protocol/LocserverMessageTypes;->NLP_SEEN_DEVICES_CACHE:Lcom/google/gmm/common/io/protocol/ProtoBufType;

    invoke-static {p1}, Lcom/google/android/location/cache/SeenDevicesCache;->getFile(Lcom/google/android/location/os/FileSystem;)Ljava/io/File;

    move-result-object v3

    invoke-interface {p1}, Lcom/google/android/location/os/Os;->getEncryptionKeyOrNull()Ljavax/crypto/SecretKey;

    move-result-object v6

    move-object v4, p0

    move-object v5, p1

    invoke-direct/range {v0 .. v6}, Lcom/google/android/location/os/SimpleProtoBufStore;-><init>(ILcom/google/gmm/common/io/protocol/ProtoBufType;Ljava/io/File;Lcom/google/common/base/Predicate;Lcom/google/android/location/os/FileSystem;Ljavax/crypto/SecretKey;)V

    iput-object v0, p0, Lcom/google/android/location/cache/SeenDevicesCache;->dataStore:Lcom/google/android/location/os/SimpleProtoBufStore;

    invoke-direct {p0}, Lcom/google/android/location/cache/SeenDevicesCache;->clear()V

    return-void
.end method

.method private clear()V
    .locals 8

    const/4 v6, 0x1

    invoke-static {}, Ljava/lang/Math;->random()D

    move-result-wide v2

    const-wide v4, 0x4194997000000000L

    mul-double/2addr v2, v4

    invoke-static {v2, v3}, Ljava/lang/Math;->floor(D)D

    move-result-wide v2

    double-to-long v0, v2

    iget-object v2, p0, Lcom/google/android/location/cache/SeenDevicesCache;->os:Lcom/google/android/location/os/Os;

    invoke-interface {v2}, Lcom/google/android/location/os/Os;->millisSinceBoot()J

    move-result-wide v2

    sub-long/2addr v2, v0

    invoke-virtual {p0, v2, v3, v6}, Lcom/google/android/location/cache/SeenDevicesCache;->setLastRefreshMillisSinceBoot(JZ)V

    iget-object v2, p0, Lcom/google/android/location/cache/SeenDevicesCache;->wifiApCache:Lcom/google/android/location/cache/SeenDevicesCache$LruCache;

    invoke-virtual {v2}, Lcom/google/android/location/cache/SeenDevicesCache$LruCache;->clear()V

    const-string v2, "SeenDevicesCache"

    const-string v3, "Clearing device cache, last refresh: %d"

    new-array v4, v6, [Ljava/lang/Object;

    const/4 v5, 0x0

    iget-wide v6, p0, Lcom/google/android/location/cache/SeenDevicesCache;->lastRefreshMillisSinceBoot:J

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v6

    aput-object v6, v4, v5

    invoke-static {v3, v4}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/google/android/location/utils/logging/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method private static getFile(Lcom/google/android/location/os/FileSystem;)Ljava/io/File;
    .locals 3
    .param p0    # Lcom/google/android/location/os/FileSystem;

    new-instance v0, Ljava/io/File;

    invoke-interface {p0}, Lcom/google/android/location/os/FileSystem;->seenDevicesCacheDir()Ljava/io/File;

    move-result-object v1

    const-string v2, "nlp_devices"

    invoke-direct {v0, v1, v2}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    return-object v0
.end method

.method private isValid(Lcom/google/gmm/common/io/protocol/ProtoBuf;)Z
    .locals 2
    .param p1    # Lcom/google/gmm/common/io/protocol/ProtoBuf;

    const/4 v0, 0x1

    if-eqz p1, :cond_0

    invoke-virtual {p1}, Lcom/google/gmm/common/io/protocol/ProtoBuf;->isValid()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-virtual {p1, v0}, Lcom/google/gmm/common/io/protocol/ProtoBuf;->has(I)Z

    move-result v1

    if-eqz v1, :cond_0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private packCache()Lcom/google/gmm/common/io/protocol/ProtoBuf;
    .locals 6

    new-instance v2, Lcom/google/gmm/common/io/protocol/ProtoBuf;

    sget-object v3, Lcom/google/android/location/protocol/LocserverMessageTypes;->NLP_SEEN_DEVICES_CACHE:Lcom/google/gmm/common/io/protocol/ProtoBufType;

    invoke-direct {v2, v3}, Lcom/google/gmm/common/io/protocol/ProtoBuf;-><init>(Lcom/google/gmm/common/io/protocol/ProtoBufType;)V

    iget-object v3, p0, Lcom/google/android/location/cache/SeenDevicesCache;->wifiApCache:Lcom/google/android/location/cache/SeenDevicesCache$LruCache;

    invoke-virtual {v3}, Lcom/google/android/location/cache/SeenDevicesCache$LruCache;->entrySet()Ljava/util/Set;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    const/4 v4, 0x3

    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/google/android/location/cache/SeenDevicesCache$SeenWifiAp;

    # getter for: Lcom/google/android/location/cache/SeenDevicesCache$SeenWifiAp;->device:Lcom/google/gmm/common/io/protocol/ProtoBuf;
    invoke-static {v3}, Lcom/google/android/location/cache/SeenDevicesCache$SeenWifiAp;->access$500(Lcom/google/android/location/cache/SeenDevicesCache$SeenWifiAp;)Lcom/google/gmm/common/io/protocol/ProtoBuf;

    move-result-object v3

    invoke-virtual {v2, v4, v3}, Lcom/google/gmm/common/io/protocol/ProtoBuf;->addProtoBuf(ILcom/google/gmm/common/io/protocol/ProtoBuf;)V

    goto :goto_0

    :cond_0
    const/4 v3, 0x1

    iget-object v4, p0, Lcom/google/android/location/cache/SeenDevicesCache;->os:Lcom/google/android/location/os/Os;

    invoke-interface {v4}, Lcom/google/android/location/os/Os;->bootTime()J

    move-result-wide v4

    invoke-virtual {v2, v3, v4, v5}, Lcom/google/gmm/common/io/protocol/ProtoBuf;->setLong(IJ)V

    const/4 v3, 0x2

    iget-wide v4, p0, Lcom/google/android/location/cache/SeenDevicesCache;->lastRefreshMillisSinceBoot:J

    invoke-virtual {v2, v3, v4, v5}, Lcom/google/gmm/common/io/protocol/ProtoBuf;->setLong(IJ)V

    const/4 v3, 0x4

    iget-boolean v4, p0, Lcom/google/android/location/cache/SeenDevicesCache;->lastRefreshIsSuccess:Z

    invoke-virtual {v2, v3, v4}, Lcom/google/gmm/common/io/protocol/ProtoBuf;->setBool(IZ)V

    return-object v2
.end method

.method private unpackCache(Lcom/google/gmm/common/io/protocol/ProtoBuf;)V
    .locals 22
    .param p1    # Lcom/google/gmm/common/io/protocol/ProtoBuf;

    const/16 v19, 0x1

    move-object/from16 v0, p1

    move/from16 v1, v19

    invoke-virtual {v0, v1}, Lcom/google/gmm/common/io/protocol/ProtoBuf;->getLong(I)J

    move-result-wide v5

    const/16 v19, 0x2

    move-object/from16 v0, p1

    move/from16 v1, v19

    invoke-virtual {v0, v1}, Lcom/google/gmm/common/io/protocol/ProtoBuf;->getLong(I)J

    move-result-wide v19

    add-long v12, v19, v5

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/location/cache/SeenDevicesCache;->os:Lcom/google/android/location/os/Os;

    move-object/from16 v19, v0

    invoke-interface/range {v19 .. v19}, Lcom/google/android/location/os/Os;->millisSinceEpoch()J

    move-result-wide v16

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/location/cache/SeenDevicesCache;->os:Lcom/google/android/location/os/Os;

    move-object/from16 v19, v0

    invoke-interface/range {v19 .. v19}, Lcom/google/android/location/os/Os;->millisSinceBoot()J

    move-result-wide v9

    sub-long v7, v16, v9

    cmp-long v19, v12, v16

    if-lez v19, :cond_0

    move-wide/from16 v12, v16

    :cond_0
    sub-long v19, v12, v7

    const/16 v21, 0x4

    move-object/from16 v0, p1

    move/from16 v1, v21

    invoke-virtual {v0, v1}, Lcom/google/gmm/common/io/protocol/ProtoBuf;->getBool(I)Z

    move-result v21

    move-object/from16 v0, p0

    move-wide/from16 v1, v19

    move/from16 v3, v21

    invoke-virtual {v0, v1, v2, v3}, Lcom/google/android/location/cache/SeenDevicesCache;->setLastRefreshMillisSinceBoot(JZ)V

    const/4 v11, 0x0

    :goto_0
    const/16 v19, 0x3

    move-object/from16 v0, p1

    move/from16 v1, v19

    invoke-virtual {v0, v1}, Lcom/google/gmm/common/io/protocol/ProtoBuf;->getCount(I)I

    move-result v19

    move/from16 v0, v19

    if-ge v11, v0, :cond_2

    const/16 v19, 0x3

    move-object/from16 v0, p1

    move/from16 v1, v19

    invoke-virtual {v0, v1, v11}, Lcom/google/gmm/common/io/protocol/ProtoBuf;->getProtoBuf(II)Lcom/google/gmm/common/io/protocol/ProtoBuf;

    move-result-object v18

    move-object/from16 v0, p0

    move-object/from16 v1, v18

    invoke-direct {v0, v1}, Lcom/google/android/location/cache/SeenDevicesCache;->isValid(Lcom/google/gmm/common/io/protocol/ProtoBuf;)Z

    move-result v19

    if-eqz v19, :cond_1

    new-instance v4, Lcom/google/android/location/cache/SeenDevicesCache$SeenWifiAp;

    move-object/from16 v0, v18

    invoke-direct {v4, v0}, Lcom/google/android/location/cache/SeenDevicesCache$SeenWifiAp;-><init>(Lcom/google/gmm/common/io/protocol/ProtoBuf;)V

    # invokes: Lcom/google/android/location/cache/SeenDevicesCache$SeenWifiAp;->adjustTimestamps(JJJ)V
    invoke-static/range {v4 .. v10}, Lcom/google/android/location/cache/SeenDevicesCache$SeenWifiAp;->access$400(Lcom/google/android/location/cache/SeenDevicesCache$SeenWifiAp;JJJ)V

    const/16 v19, 0x1

    invoke-virtual/range {v18 .. v19}, Lcom/google/gmm/common/io/protocol/ProtoBuf;->getLong(I)J

    move-result-wide v14

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/location/cache/SeenDevicesCache;->wifiApCache:Lcom/google/android/location/cache/SeenDevicesCache$LruCache;

    move-object/from16 v19, v0

    invoke-static {v14, v15}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v20

    move-object/from16 v0, v19

    move-object/from16 v1, v20

    invoke-virtual {v0, v1, v4}, Lcom/google/android/location/cache/SeenDevicesCache$LruCache;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    :cond_1
    add-int/lit8 v11, v11, 0x1

    goto :goto_0

    :cond_2
    return-void
.end method


# virtual methods
.method public apply(Lcom/google/gmm/common/io/protocol/ProtoBuf;)Z
    .locals 2
    .param p1    # Lcom/google/gmm/common/io/protocol/ProtoBuf;

    const/4 v0, 0x1

    invoke-virtual {p1, v0}, Lcom/google/gmm/common/io/protocol/ProtoBuf;->has(I)Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v1, 0x2

    invoke-virtual {p1, v1}, Lcom/google/gmm/common/io/protocol/ProtoBuf;->has(I)Z

    move-result v1

    if-eqz v1, :cond_0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public bridge synthetic apply(Ljava/lang/Object;)Z
    .locals 1
    .param p1    # Ljava/lang/Object;

    check-cast p1, Lcom/google/gmm/common/io/protocol/ProtoBuf;

    invoke-virtual {p0, p1}, Lcom/google/android/location/cache/SeenDevicesCache;->apply(Lcom/google/gmm/common/io/protocol/ProtoBuf;)Z

    move-result v0

    return v0
.end method

.method public discardOldEntries()V
    .locals 15

    iget-object v10, p0, Lcom/google/android/location/cache/SeenDevicesCache;->os:Lcom/google/android/location/os/Os;

    invoke-interface {v10}, Lcom/google/android/location/os/Os;->millisSinceBoot()J

    move-result-wide v8

    const-wide/32 v10, 0xa4cb800

    sub-long v4, v8, v10

    const-wide/32 v10, 0x240c8400

    sub-long v6, v8, v10

    iget-object v10, p0, Lcom/google/android/location/cache/SeenDevicesCache;->wifiApCache:Lcom/google/android/location/cache/SeenDevicesCache$LruCache;

    invoke-virtual {v10}, Lcom/google/android/location/cache/SeenDevicesCache$LruCache;->entrySet()Ljava/util/Set;

    move-result-object v10

    invoke-interface {v10}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_0
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v10

    if-eqz v10, :cond_3

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/Map$Entry;

    invoke-interface {v1}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v10

    check-cast v10, Lcom/google/android/location/cache/SeenDevicesCache$SeenWifiAp;

    invoke-virtual {v10}, Lcom/google/android/location/cache/SeenDevicesCache$SeenWifiAp;->getLastSeenDaysSinceBoot()I

    move-result v3

    invoke-interface {v1}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v10

    check-cast v10, Lcom/google/android/location/cache/SeenDevicesCache$SeenWifiAp;

    invoke-virtual {v10}, Lcom/google/android/location/cache/SeenDevicesCache$SeenWifiAp;->getDownloadDaysSinceBoot()I

    move-result v0

    const v10, 0x7fffffff

    if-ne v3, v10, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->remove()V

    goto :goto_0

    :cond_1
    int-to-long v10, v3

    const-wide/32 v12, 0x5265c00

    mul-long/2addr v10, v12

    cmp-long v10, v10, v6

    if-gez v10, :cond_2

    invoke-interface {v2}, Ljava/util/Iterator;->remove()V

    iget-object v10, p0, Lcom/google/android/location/cache/SeenDevicesCache;->wifiCacheStats:Lcom/google/android/location/cache/Stats;

    invoke-virtual {v10}, Lcom/google/android/location/cache/Stats;->addExpirationEviction()V

    const-string v10, "SeenDevicesCache"

    const-string v11, "Discarding %d because never seen for a long time."

    const/4 v12, 0x1

    new-array v12, v12, [Ljava/lang/Object;

    const/4 v13, 0x0

    invoke-interface {v1}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v14

    aput-object v14, v12, v13

    invoke-static {v11, v12}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v11

    invoke-static {v10, v11}, Lcom/google/android/location/utils/logging/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    :cond_2
    const v10, 0x7fffffff

    if-eq v0, v10, :cond_0

    int-to-long v10, v0

    const-wide/32 v12, 0x5265c00

    mul-long/2addr v10, v12

    cmp-long v10, v10, v4

    if-gez v10, :cond_0

    iget-object v10, p0, Lcom/google/android/location/cache/SeenDevicesCache;->wifiCacheStats:Lcom/google/android/location/cache/Stats;

    invoke-virtual {v10}, Lcom/google/android/location/cache/Stats;->addExpirationEviction()V

    invoke-interface {v1}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v10

    check-cast v10, Lcom/google/android/location/cache/SeenDevicesCache$SeenWifiAp;

    # invokes: Lcom/google/android/location/cache/SeenDevicesCache$SeenWifiAp;->clearGlsResult()V
    invoke-static {v10}, Lcom/google/android/location/cache/SeenDevicesCache$SeenWifiAp;->access$600(Lcom/google/android/location/cache/SeenDevicesCache$SeenWifiAp;)V

    const-string v10, "SeenDevicesCache"

    const-string v11, "Removing cache result of %d because it\'s too old."

    const/4 v12, 0x1

    new-array v12, v12, [Ljava/lang/Object;

    const/4 v13, 0x0

    invoke-interface {v1}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v14

    aput-object v14, v12, v13

    invoke-static {v11, v12}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v11

    invoke-static {v10, v11}, Lcom/google/android/location/utils/logging/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    :cond_3
    return-void
.end method

.method public entrySet()Ljava/util/Set;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Set",
            "<",
            "Ljava/util/Map$Entry",
            "<",
            "Ljava/lang/Long;",
            "Lcom/google/android/location/cache/SeenDevicesCache$SeenWifiAp;",
            ">;>;"
        }
    .end annotation

    iget-object v0, p0, Lcom/google/android/location/cache/SeenDevicesCache;->wifiApCache:Lcom/google/android/location/cache/SeenDevicesCache$LruCache;

    invoke-virtual {v0}, Lcom/google/android/location/cache/SeenDevicesCache$LruCache;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-static {v0}, Ljava/util/Collections;->unmodifiableSet(Ljava/util/Set;)Ljava/util/Set;

    move-result-object v0

    return-object v0
.end method

.method public extractStats()Lcom/google/gmm/common/io/protocol/ProtoBuf;
    .locals 3

    iget-object v0, p0, Lcom/google/android/location/cache/SeenDevicesCache;->wifiCacheStats:Lcom/google/android/location/cache/Stats;

    iget-object v1, p0, Lcom/google/android/location/cache/SeenDevicesCache;->wifiApCache:Lcom/google/android/location/cache/SeenDevicesCache$LruCache;

    # getter for: Lcom/google/android/location/cache/SeenDevicesCache$LruCache;->capacity:I
    invoke-static {v1}, Lcom/google/android/location/cache/SeenDevicesCache$LruCache;->access$300(Lcom/google/android/location/cache/SeenDevicesCache$LruCache;)I

    move-result v1

    iget-object v2, p0, Lcom/google/android/location/cache/SeenDevicesCache;->wifiApCache:Lcom/google/android/location/cache/SeenDevicesCache$LruCache;

    invoke-virtual {v2}, Lcom/google/android/location/cache/SeenDevicesCache$LruCache;->size()I

    move-result v2

    invoke-virtual {v0, v1, v2}, Lcom/google/android/location/cache/Stats;->extractAndReset(II)Lcom/google/gmm/common/io/protocol/ProtoBuf;

    move-result-object v0

    return-object v0
.end method

.method public getLastRefreshIsSuccess()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/location/cache/SeenDevicesCache;->lastRefreshIsSuccess:Z

    return v0
.end method

.method public getLastRefreshMillisSinceBoot()J
    .locals 2

    iget-wide v0, p0, Lcom/google/android/location/cache/SeenDevicesCache;->lastRefreshMillisSinceBoot:J

    return-wide v0
.end method

.method public load()V
    .locals 8

    :try_start_0
    iget-object v2, p0, Lcom/google/android/location/cache/SeenDevicesCache;->dataStore:Lcom/google/android/location/os/SimpleProtoBufStore;

    invoke-virtual {v2}, Lcom/google/android/location/os/SimpleProtoBufStore;->load()Lcom/google/gmm/common/io/protocol/ProtoBuf;

    move-result-object v1

    invoke-direct {p0, v1}, Lcom/google/android/location/cache/SeenDevicesCache;->unpackCache(Lcom/google/gmm/common/io/protocol/ProtoBuf;)V

    invoke-virtual {p0}, Lcom/google/android/location/cache/SeenDevicesCache;->discardOldEntries()V

    const-string v2, "SeenDevicesCache"

    const-string v3, "Loaded %d entries, last refresh: %d"

    const/4 v4, 0x2

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    iget-object v6, p0, Lcom/google/android/location/cache/SeenDevicesCache;->wifiApCache:Lcom/google/android/location/cache/SeenDevicesCache$LruCache;

    invoke-virtual {v6}, Lcom/google/android/location/cache/SeenDevicesCache$LruCache;->size()I

    move-result v6

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    aput-object v6, v4, v5

    const/4 v5, 0x1

    iget-wide v6, p0, Lcom/google/android/location/cache/SeenDevicesCache;->lastRefreshMillisSinceBoot:J

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v6

    aput-object v6, v4, v5

    invoke-static {v3, v4}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/google/android/location/utils/logging/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    return-void

    :catch_0
    move-exception v0

    invoke-direct {p0}, Lcom/google/android/location/cache/SeenDevicesCache;->clear()V

    const-string v2, "SeenDevicesCache"

    invoke-virtual {v0}, Ljava/io/IOException;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/google/android/location/utils/logging/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public save()V
    .locals 2

    invoke-virtual {p0}, Lcom/google/android/location/cache/SeenDevicesCache;->discardOldEntries()V

    iget-object v0, p0, Lcom/google/android/location/cache/SeenDevicesCache;->dataStore:Lcom/google/android/location/os/SimpleProtoBufStore;

    invoke-direct {p0}, Lcom/google/android/location/cache/SeenDevicesCache;->packCache()Lcom/google/gmm/common/io/protocol/ProtoBuf;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/location/os/SimpleProtoBufStore;->save(Lcom/google/gmm/common/io/protocol/ProtoBuf;)Z

    return-void
.end method

.method public setLastRefreshMillisSinceBoot(JZ)V
    .locals 0
    .param p1    # J
    .param p3    # Z

    iput-wide p1, p0, Lcom/google/android/location/cache/SeenDevicesCache;->lastRefreshMillisSinceBoot:J

    iput-boolean p3, p0, Lcom/google/android/location/cache/SeenDevicesCache;->lastRefreshIsSuccess:Z

    return-void
.end method

.method public updateCachesFromGlsQueryResponse(Lcom/google/gmm/common/io/protocol/ProtoBuf;)V
    .locals 12
    .param p1    # Lcom/google/gmm/common/io/protocol/ProtoBuf;

    invoke-static {p1}, Lcom/google/android/location/utils/Utils;->containsOneValidGLocRequestElement(Lcom/google/gmm/common/io/protocol/ProtoBuf;)Z

    move-result v10

    if-nez v10, :cond_1

    :cond_0
    return-void

    :cond_1
    iget-object v10, p0, Lcom/google/android/location/cache/SeenDevicesCache;->os:Lcom/google/android/location/os/Os;

    invoke-interface {v10}, Lcom/google/android/location/os/Os;->millisSinceBoot()J

    move-result-wide v5

    const/4 v10, 0x2

    invoke-virtual {p1, v10}, Lcom/google/gmm/common/io/protocol/ProtoBuf;->getProtoBuf(I)Lcom/google/gmm/common/io/protocol/ProtoBuf;

    move-result-object v7

    iget-object v10, p0, Lcom/google/android/location/cache/SeenDevicesCache;->nlpParamsState:Lcom/google/android/location/os/NlpParametersState;

    invoke-virtual {v10}, Lcom/google/android/location/os/NlpParametersState;->getWifiDatabaseVersion()I

    move-result v9

    const/4 v4, 0x0

    :goto_0
    const/4 v10, 0x3

    invoke-virtual {v7, v10}, Lcom/google/gmm/common/io/protocol/ProtoBuf;->getCount(I)I

    move-result v10

    if-ge v4, v10, :cond_0

    const/4 v10, 0x3

    invoke-virtual {v7, v10, v4}, Lcom/google/gmm/common/io/protocol/ProtoBuf;->getProtoBuf(II)Lcom/google/gmm/common/io/protocol/ProtoBuf;

    move-result-object v2

    const/4 v10, 0x3

    invoke-virtual {v2, v10}, Lcom/google/gmm/common/io/protocol/ProtoBuf;->has(I)Z

    move-result v10

    if-eqz v10, :cond_2

    const/4 v10, 0x3

    invoke-virtual {v2, v10}, Lcom/google/gmm/common/io/protocol/ProtoBuf;->getProtoBuf(I)Lcom/google/gmm/common/io/protocol/ProtoBuf;

    move-result-object v3

    const-wide/16 v0, -0x1

    const/16 v10, 0x8

    invoke-virtual {v3, v10}, Lcom/google/gmm/common/io/protocol/ProtoBuf;->has(I)Z

    move-result v10

    if-eqz v10, :cond_3

    const/16 v10, 0x8

    invoke-virtual {v3, v10}, Lcom/google/gmm/common/io/protocol/ProtoBuf;->getLong(I)J

    move-result-wide v0

    :goto_1
    const-wide/16 v10, -0x1

    cmp-long v10, v0, v10

    if-eqz v10, :cond_2

    iget-object v10, p0, Lcom/google/android/location/cache/SeenDevicesCache;->wifiApCache:Lcom/google/android/location/cache/SeenDevicesCache$LruCache;

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v11

    invoke-virtual {v10, v11}, Lcom/google/android/location/cache/SeenDevicesCache$LruCache;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Lcom/google/android/location/cache/SeenDevicesCache$SeenWifiAp;

    if-eqz v8, :cond_2

    const/4 v10, 0x4

    invoke-virtual {v2, v10}, Lcom/google/gmm/common/io/protocol/ProtoBuf;->has(I)Z

    move-result v10

    if-eqz v10, :cond_4

    const/4 v10, 0x4

    invoke-virtual {v2, v10}, Lcom/google/gmm/common/io/protocol/ProtoBuf;->getFloat(I)F

    move-result v10

    invoke-virtual {v8, v5, v6, v10, v9}, Lcom/google/android/location/cache/SeenDevicesCache$SeenWifiAp;->setGlsResult(JFI)V

    :cond_2
    :goto_2
    add-int/lit8 v4, v4, 0x1

    goto :goto_0

    :cond_3
    const/4 v10, 0x1

    invoke-virtual {v3, v10}, Lcom/google/gmm/common/io/protocol/ProtoBuf;->getString(I)Ljava/lang/String;

    move-result-object v10

    invoke-static {v10}, Lcom/google/android/location/data/WifiUtils;->macToLong(Ljava/lang/String;)J

    move-result-wide v0

    goto :goto_1

    :cond_4
    invoke-virtual {v8, v5, v6, v9}, Lcom/google/android/location/cache/SeenDevicesCache$SeenWifiAp;->setGlsResult(JI)V

    goto :goto_2
.end method
