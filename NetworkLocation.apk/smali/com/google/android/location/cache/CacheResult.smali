.class public final Lcom/google/android/location/cache/CacheResult;
.super Ljava/lang/Object;
.source "CacheResult.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;"
    }
.end annotation


# instance fields
.field private databaseVersion:I

.field private lastSeenTime:J

.field private position:Ljava/lang/Object;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "TT;"
        }
    .end annotation
.end field

.field private readingTime:J


# direct methods
.method constructor <init>(ILjava/lang/Object;J)V
    .locals 7
    .param p1    # I
    .param p3    # J
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(ITT;J)V"
        }
    .end annotation

    move-object v0, p0

    move v1, p1

    move-object v2, p2

    move-wide v3, p3

    move-wide v5, p3

    invoke-direct/range {v0 .. v6}, Lcom/google/android/location/cache/CacheResult;-><init>(ILjava/lang/Object;JJ)V

    return-void
.end method

.method constructor <init>(ILjava/lang/Object;JJ)V
    .locals 2
    .param p1    # I
    .param p3    # J
    .param p5    # J
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(ITT;JJ)V"
        }
    .end annotation

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/location/cache/CacheResult;->databaseVersion:I

    if-nez p2, :cond_0

    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Position many not be null."

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    iput-object p2, p0, Lcom/google/android/location/cache/CacheResult;->position:Ljava/lang/Object;

    iput p1, p0, Lcom/google/android/location/cache/CacheResult;->databaseVersion:I

    iput-wide p3, p0, Lcom/google/android/location/cache/CacheResult;->readingTime:J

    iput-wide p5, p0, Lcom/google/android/location/cache/CacheResult;->lastSeenTime:J

    return-void
.end method

.method public static load(Ljava/io/DataInput;Lcom/google/android/location/data/Persistent;)Lcom/google/android/location/cache/CacheResult;
    .locals 7
    .param p0    # Ljava/io/DataInput;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<P:",
            "Ljava/lang/Object;",
            ">(",
            "Ljava/io/DataInput;",
            "Lcom/google/android/location/data/Persistent",
            "<TP;>;)",
            "Lcom/google/android/location/cache/CacheResult",
            "<TP;>;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-interface {p0}, Ljava/io/DataInput;->readLong()J

    move-result-wide v5

    invoke-interface {p0}, Ljava/io/DataInput;->readLong()J

    move-result-wide v3

    invoke-interface {p0}, Ljava/io/DataInput;->readInt()I

    move-result v1

    invoke-interface {p1, p0}, Lcom/google/android/location/data/Persistent;->load(Ljava/io/DataInput;)Ljava/lang/Object;

    move-result-object v2

    new-instance v0, Lcom/google/android/location/cache/CacheResult;

    invoke-direct/range {v0 .. v6}, Lcom/google/android/location/cache/CacheResult;-><init>(ILjava/lang/Object;JJ)V

    iput-wide v5, v0, Lcom/google/android/location/cache/CacheResult;->lastSeenTime:J

    return-object v0
.end method

.method public static save(Lcom/google/android/location/cache/CacheResult;Ljava/io/DataOutput;Lcom/google/android/location/data/Persistent;)V
    .locals 2
    .param p1    # Ljava/io/DataOutput;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<P:",
            "Ljava/lang/Object;",
            ">(",
            "Lcom/google/android/location/cache/CacheResult",
            "<TP;>;",
            "Ljava/io/DataOutput;",
            "Lcom/google/android/location/data/Persistent",
            "<TP;>;)V"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    iget-wide v0, p0, Lcom/google/android/location/cache/CacheResult;->lastSeenTime:J

    invoke-interface {p1, v0, v1}, Ljava/io/DataOutput;->writeLong(J)V

    iget-wide v0, p0, Lcom/google/android/location/cache/CacheResult;->readingTime:J

    invoke-interface {p1, v0, v1}, Ljava/io/DataOutput;->writeLong(J)V

    iget v0, p0, Lcom/google/android/location/cache/CacheResult;->databaseVersion:I

    invoke-interface {p1, v0}, Ljava/io/DataOutput;->writeInt(I)V

    iget-object v0, p0, Lcom/google/android/location/cache/CacheResult;->position:Ljava/lang/Object;

    invoke-interface {p2, v0, p1}, Lcom/google/android/location/data/Persistent;->save(Ljava/lang/Object;Ljava/io/DataOutput;)V

    return-void
.end method


# virtual methods
.method public getDatabaseVersion()I
    .locals 1

    iget v0, p0, Lcom/google/android/location/cache/CacheResult;->databaseVersion:I

    return v0
.end method

.method public getLastSeenTime()J
    .locals 2

    iget-wide v0, p0, Lcom/google/android/location/cache/CacheResult;->lastSeenTime:J

    return-wide v0
.end method

.method public getPosition()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()TT;"
        }
    .end annotation

    iget-object v0, p0, Lcom/google/android/location/cache/CacheResult;->position:Ljava/lang/Object;

    return-object v0
.end method

.method public getReadingTime()J
    .locals 2

    iget-wide v0, p0, Lcom/google/android/location/cache/CacheResult;->readingTime:J

    return-wide v0
.end method

.method setGlsResult(ILjava/lang/Object;J)V
    .locals 2
    .param p1    # I
    .param p3    # J
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(ITT;J)V"
        }
    .end annotation

    if-nez p2, :cond_0

    new-instance v0, Ljava/lang/RuntimeException;

    const-string v1, "Position may not be null."

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    iput-wide p3, p0, Lcom/google/android/location/cache/CacheResult;->readingTime:J

    iput-object p2, p0, Lcom/google/android/location/cache/CacheResult;->position:Ljava/lang/Object;

    iput p1, p0, Lcom/google/android/location/cache/CacheResult;->databaseVersion:I

    return-void
.end method

.method public setLastSeenTime(J)V
    .locals 0
    .param p1    # J

    iput-wide p1, p0, Lcom/google/android/location/cache/CacheResult;->lastSeenTime:J

    return-void
.end method

.method public setReadingTime(J)V
    .locals 0
    .param p1    # J

    iput-wide p1, p0, Lcom/google/android/location/cache/CacheResult;->readingTime:J

    return-void
.end method

.method public toString()Ljava/lang/String;
    .locals 3

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "CacheResult ["

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/location/cache/CacheResult;->position:Ljava/lang/Object;

    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " databaseVersion="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/google/android/location/cache/CacheResult;->databaseVersion:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " readingTime="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-wide v1, p0, Lcom/google/android/location/cache/CacheResult;->readingTime:J

    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " lastSeenTime="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-wide v1, p0, Lcom/google/android/location/cache/CacheResult;->lastSeenTime:J

    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "]"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
