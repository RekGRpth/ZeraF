.class final Lcom/google/android/location/cache/PersistentState$2;
.super Ljava/lang/Object;
.source "PersistentState.java"

# interfaces
.implements Lcom/google/android/location/data/Persistent;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/location/cache/PersistentState;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/google/android/location/data/Persistent",
        "<",
        "Lcom/google/android/location/cache/CacheResult",
        "<",
        "Lcom/google/android/location/data/WifiApPosition;",
        ">;>;"
    }
.end annotation


# direct methods
.method constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public load(Ljava/io/DataInput;)Lcom/google/android/location/cache/CacheResult;
    .locals 1
    .param p1    # Ljava/io/DataInput;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/io/DataInput;",
            ")",
            "Lcom/google/android/location/cache/CacheResult",
            "<",
            "Lcom/google/android/location/data/WifiApPosition;",
            ">;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    sget-object v0, Lcom/google/android/location/data/WifiApPosition;->WIFI_AP_SAVER:Lcom/google/android/location/data/Persistent;

    invoke-static {p1, v0}, Lcom/google/android/location/cache/CacheResult;->load(Ljava/io/DataInput;Lcom/google/android/location/data/Persistent;)Lcom/google/android/location/cache/CacheResult;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic load(Ljava/io/DataInput;)Ljava/lang/Object;
    .locals 1
    .param p1    # Ljava/io/DataInput;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-virtual {p0, p1}, Lcom/google/android/location/cache/PersistentState$2;->load(Ljava/io/DataInput;)Lcom/google/android/location/cache/CacheResult;

    move-result-object v0

    return-object v0
.end method

.method public save(Lcom/google/android/location/cache/CacheResult;Ljava/io/DataOutput;)V
    .locals 1
    .param p2    # Ljava/io/DataOutput;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/location/cache/CacheResult",
            "<",
            "Lcom/google/android/location/data/WifiApPosition;",
            ">;",
            "Ljava/io/DataOutput;",
            ")V"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    sget-object v0, Lcom/google/android/location/data/WifiApPosition;->WIFI_AP_SAVER:Lcom/google/android/location/data/Persistent;

    invoke-static {p1, p2, v0}, Lcom/google/android/location/cache/CacheResult;->save(Lcom/google/android/location/cache/CacheResult;Ljava/io/DataOutput;Lcom/google/android/location/data/Persistent;)V

    return-void
.end method

.method public bridge synthetic save(Ljava/lang/Object;Ljava/io/DataOutput;)V
    .locals 0
    .param p1    # Ljava/lang/Object;
    .param p2    # Ljava/io/DataOutput;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    check-cast p1, Lcom/google/android/location/cache/CacheResult;

    invoke-virtual {p0, p1, p2}, Lcom/google/android/location/cache/PersistentState$2;->save(Lcom/google/android/location/cache/CacheResult;Ljava/io/DataOutput;)V

    return-void
.end method
