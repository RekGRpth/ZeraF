.class public Lcom/google/android/location/cache/PersistentState;
.super Ljava/lang/Object;
.source "PersistentState.java"


# static fields
.field public static final INVALID_RESULT:Lcom/google/android/location/data/LocatorResult;

.field public static final POSITION_SAVER:Lcom/google/android/location/data/Persistent;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/location/data/Persistent",
            "<",
            "Lcom/google/android/location/cache/CacheResult",
            "<",
            "Lcom/google/android/location/data/Position;",
            ">;>;"
        }
    .end annotation
.end field

.field public static final WIFI_AP_POSITION_SAVER:Lcom/google/android/location/data/Persistent;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/location/data/Persistent",
            "<",
            "Lcom/google/android/location/cache/CacheResult",
            "<",
            "Lcom/google/android/location/data/WifiApPosition;",
            ">;>;"
        }
    .end annotation
.end field


# instance fields
.field private cacheRefreshMillisSinceBoot:J

.field public final cellCache:Lcom/google/android/location/cache/TemporalCache;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/location/cache/TemporalCache",
            "<",
            "Ljava/lang/String;",
            "Lcom/google/android/location/data/Position;",
            ">;"
        }
    .end annotation
.end field

.field public lastKnown:Lcom/google/android/location/data/LocatorResult;

.field private lastRefreshIsSuccess:Z

.field public final nlpParamsState:Lcom/google/android/location/os/NlpParametersState;

.field public final wifiCache:Lcom/google/android/location/cache/TemporalCache;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/location/cache/TemporalCache",
            "<",
            "Ljava/lang/Long;",
            "Lcom/google/android/location/data/WifiApPosition;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 5

    new-instance v0, Lcom/google/android/location/data/LocatorResult;

    const/4 v1, 0x0

    sget-object v2, Lcom/google/android/location/data/LocatorResult$ResultStatus;->NO_LOCATION:Lcom/google/android/location/data/LocatorResult$ResultStatus;

    const-wide/16 v3, 0x0

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/google/android/location/data/LocatorResult;-><init>(Lcom/google/android/location/data/Position;Lcom/google/android/location/data/LocatorResult$ResultStatus;J)V

    sput-object v0, Lcom/google/android/location/cache/PersistentState;->INVALID_RESULT:Lcom/google/android/location/data/LocatorResult;

    new-instance v0, Lcom/google/android/location/cache/PersistentState$1;

    invoke-direct {v0}, Lcom/google/android/location/cache/PersistentState$1;-><init>()V

    sput-object v0, Lcom/google/android/location/cache/PersistentState;->POSITION_SAVER:Lcom/google/android/location/data/Persistent;

    new-instance v0, Lcom/google/android/location/cache/PersistentState$2;

    invoke-direct {v0}, Lcom/google/android/location/cache/PersistentState$2;-><init>()V

    sput-object v0, Lcom/google/android/location/cache/PersistentState;->WIFI_AP_POSITION_SAVER:Lcom/google/android/location/data/Persistent;

    return-void
.end method

.method public constructor <init>(Lcom/google/android/location/os/NlpParametersState;J)V
    .locals 4
    .param p1    # Lcom/google/android/location/os/NlpParametersState;
    .param p2    # J

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    sget-object v0, Lcom/google/android/location/cache/PersistentState;->INVALID_RESULT:Lcom/google/android/location/data/LocatorResult;

    iput-object v0, p0, Lcom/google/android/location/cache/PersistentState;->lastKnown:Lcom/google/android/location/data/LocatorResult;

    iput-object p1, p0, Lcom/google/android/location/cache/PersistentState;->nlpParamsState:Lcom/google/android/location/os/NlpParametersState;

    new-instance v0, Lcom/google/android/location/cache/TemporalCache;

    const/16 v1, 0x32

    sget-object v2, Lcom/google/android/location/data/Savers;->STRING_SAVER:Lcom/google/android/location/data/Persistent;

    sget-object v3, Lcom/google/android/location/cache/PersistentState;->POSITION_SAVER:Lcom/google/android/location/data/Persistent;

    invoke-direct {v0, v1, v2, v3}, Lcom/google/android/location/cache/TemporalCache;-><init>(ILcom/google/android/location/data/Persistent;Lcom/google/android/location/data/Persistent;)V

    iput-object v0, p0, Lcom/google/android/location/cache/PersistentState;->cellCache:Lcom/google/android/location/cache/TemporalCache;

    new-instance v0, Lcom/google/android/location/cache/TemporalCache;

    const/16 v1, 0x3e8

    sget-object v2, Lcom/google/android/location/data/Savers;->LONG_SAVER:Lcom/google/android/location/data/Persistent;

    sget-object v3, Lcom/google/android/location/cache/PersistentState;->WIFI_AP_POSITION_SAVER:Lcom/google/android/location/data/Persistent;

    invoke-direct {v0, v1, v2, v3}, Lcom/google/android/location/cache/TemporalCache;-><init>(ILcom/google/android/location/data/Persistent;Lcom/google/android/location/data/Persistent;)V

    iput-object v0, p0, Lcom/google/android/location/cache/PersistentState;->wifiCache:Lcom/google/android/location/cache/TemporalCache;

    invoke-direct {p0, p2, p3}, Lcom/google/android/location/cache/PersistentState;->reset(J)V

    return-void
.end method

.method public static cellCacheKeyToGCell(Ljava/lang/String;)Lcom/google/gmm/common/io/protocol/ProtoBuf;
    .locals 12
    .param p0    # Ljava/lang/String;

    const/4 v8, 0x0

    const/4 v11, -0x1

    if-nez p0, :cond_1

    move-object v7, v8

    :cond_0
    :goto_0
    return-object v7

    :cond_1
    invoke-virtual {p0}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object p0

    const-string v9, ":"

    invoke-virtual {p0, v9}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v5

    array-length v9, v5

    const/4 v10, 0x5

    if-eq v9, v10, :cond_2

    move-object v7, v8

    goto :goto_0

    :cond_2
    const/4 v9, 0x0

    :try_start_0
    aget-object v9, v5, v9

    invoke-static {v9}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v6

    const/4 v9, 0x1

    aget-object v9, v5, v9

    invoke-static {v9}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v3

    const/4 v9, 0x2

    aget-object v9, v5, v9

    invoke-static {v9}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v4

    const/4 v9, 0x3

    aget-object v9, v5, v9

    invoke-static {v9}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v2

    const/4 v9, 0x4

    aget-object v9, v5, v9

    invoke-static {v9}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v0

    new-instance v7, Lcom/google/gmm/common/io/protocol/ProtoBuf;

    sget-object v9, Lcom/google/android/location/protocol/LocserverMessageTypes;->GCELL:Lcom/google/gmm/common/io/protocol/ProtoBufType;

    invoke-direct {v7, v9}, Lcom/google/gmm/common/io/protocol/ProtoBuf;-><init>(Lcom/google/gmm/common/io/protocol/ProtoBufType;)V

    const/4 v9, 0x1

    invoke-virtual {v7, v9, v2}, Lcom/google/gmm/common/io/protocol/ProtoBuf;->setInt(II)V

    const/4 v9, 0x2

    invoke-virtual {v7, v9, v0}, Lcom/google/gmm/common/io/protocol/ProtoBuf;->setInt(II)V

    if-eq v6, v11, :cond_3

    const/16 v9, 0xa

    invoke-virtual {v7, v9, v6}, Lcom/google/gmm/common/io/protocol/ProtoBuf;->setInt(II)V

    :cond_3
    if-eq v4, v11, :cond_4

    const/4 v9, 0x3

    invoke-virtual {v7, v9, v4}, Lcom/google/gmm/common/io/protocol/ProtoBuf;->setInt(II)V

    :cond_4
    if-eq v3, v11, :cond_0

    const/4 v9, 0x4

    invoke-virtual {v7, v9, v3}, Lcom/google/gmm/common/io/protocol/ProtoBuf;->setInt(II)V
    :try_end_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v1

    move-object v7, v8

    goto :goto_0
.end method

.method public static convertTimeSinceBoot(JJJJ)J
    .locals 2
    .param p0    # J
    .param p2    # J
    .param p4    # J
    .param p6    # J

    add-long v0, p2, p0

    sub-long/2addr v0, p4

    invoke-static {p6, p7, v0, v1}, Ljava/lang/Math;->min(JJ)J

    move-result-wide v0

    return-wide v0
.end method

.method private decryptInput(Ljava/io/InputStream;Ljavax/crypto/SecretKey;)Ljava/io/DataInput;
    .locals 2
    .param p1    # Ljava/io/InputStream;
    .param p2    # Ljavax/crypto/SecretKey;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    new-instance v0, Ljava/io/DataInputStream;

    invoke-static {p1, p2}, Lcom/google/android/location/os/CipherStreams;->newCipherInputStream(Ljava/io/InputStream;Ljavax/crypto/SecretKey;)Ljava/io/InputStream;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/io/DataInputStream;-><init>(Ljava/io/InputStream;)V

    return-object v0
.end method

.method private encryptOutput(Ljava/io/OutputStream;Ljavax/crypto/SecretKey;)Ljava/io/DataOutputStream;
    .locals 2
    .param p1    # Ljava/io/OutputStream;
    .param p2    # Ljavax/crypto/SecretKey;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    new-instance v0, Ljava/io/DataOutputStream;

    invoke-static {p1, p2}, Lcom/google/android/location/os/CipherStreams;->newCipherOutputStream(Ljava/io/OutputStream;Ljavax/crypto/SecretKey;)Ljava/io/OutputStream;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/io/DataOutputStream;-><init>(Ljava/io/OutputStream;)V

    return-object v0
.end method

.method public static getCellCacheKey(IIIII)Ljava/lang/String;
    .locals 2
    .param p0    # I
    .param p1    # I
    .param p2    # I
    .param p3    # I
    .param p4    # I

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ":"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ":"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ":"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ":"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public static getCellCacheKey(Lcom/google/android/location/data/CellState;)Ljava/lang/String;
    .locals 5
    .param p0    # Lcom/google/android/location/data/CellState;

    invoke-virtual {p0}, Lcom/google/android/location/data/CellState;->getGCellRadioType()I

    move-result v0

    invoke-virtual {p0}, Lcom/google/android/location/data/CellState;->getMcc()I

    move-result v1

    invoke-virtual {p0}, Lcom/google/android/location/data/CellState;->getMnc()I

    move-result v2

    invoke-virtual {p0}, Lcom/google/android/location/data/CellState;->getLac()I

    move-result v3

    invoke-virtual {p0}, Lcom/google/android/location/data/CellState;->getCid()I

    move-result v4

    invoke-static {v0, v1, v2, v3, v4}, Lcom/google/android/location/cache/PersistentState;->getCellCacheKey(IIIII)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private reset(J)V
    .locals 8
    .param p1    # J

    const-wide/high16 v6, 0x404e000000000000L

    sget-object v2, Lcom/google/android/location/cache/PersistentState;->INVALID_RESULT:Lcom/google/android/location/data/LocatorResult;

    iput-object v2, p0, Lcom/google/android/location/cache/PersistentState;->lastKnown:Lcom/google/android/location/data/LocatorResult;

    invoke-static {}, Ljava/lang/Math;->random()D

    move-result-wide v2

    const-wide/high16 v4, 0x4038000000000000L

    mul-double/2addr v2, v4

    mul-double/2addr v2, v6

    mul-double/2addr v2, v6

    const-wide v4, 0x408f400000000000L

    mul-double/2addr v2, v4

    invoke-static {v2, v3}, Ljava/lang/Math;->floor(D)D

    move-result-wide v2

    double-to-long v0, v2

    sub-long v2, p1, v0

    const/4 v4, 0x1

    invoke-virtual {p0, v2, v3, v4}, Lcom/google/android/location/cache/PersistentState;->setCacheRefreshMillisSinceBoot(JZ)V

    iget-object v2, p0, Lcom/google/android/location/cache/PersistentState;->cellCache:Lcom/google/android/location/cache/TemporalCache;

    invoke-virtual {v2}, Lcom/google/android/location/cache/TemporalCache;->clear()V

    iget-object v2, p0, Lcom/google/android/location/cache/PersistentState;->wifiCache:Lcom/google/android/location/cache/TemporalCache;

    invoke-virtual {v2}, Lcom/google/android/location/cache/TemporalCache;->clear()V

    return-void
.end method


# virtual methods
.method public discardOldCacheEntries(J)V
    .locals 5
    .param p1    # J

    const-wide/32 v2, 0x240c8400

    sub-long v0, p1, v2

    iget-object v2, p0, Lcom/google/android/location/cache/PersistentState;->cellCache:Lcom/google/android/location/cache/TemporalCache;

    iget-object v3, p0, Lcom/google/android/location/cache/PersistentState;->nlpParamsState:Lcom/google/android/location/os/NlpParametersState;

    invoke-virtual {v3}, Lcom/google/android/location/os/NlpParametersState;->getCellCacheTtlMillis()J

    move-result-wide v3

    sub-long v3, p1, v3

    invoke-virtual {v2, v3, v4, v0, v1}, Lcom/google/android/location/cache/TemporalCache;->discardOldEntries(JJ)V

    iget-object v2, p0, Lcom/google/android/location/cache/PersistentState;->wifiCache:Lcom/google/android/location/cache/TemporalCache;

    iget-object v3, p0, Lcom/google/android/location/cache/PersistentState;->nlpParamsState:Lcom/google/android/location/os/NlpParametersState;

    invoke-virtual {v3}, Lcom/google/android/location/os/NlpParametersState;->getWifiCacheTtlMillis()J

    move-result-wide v3

    sub-long v3, p1, v3

    invoke-virtual {v2, v3, v4, v0, v1}, Lcom/google/android/location/cache/TemporalCache;->discardOldEntries(JJ)V

    return-void
.end method

.method public getCacheRefreshMillisSinceBoot()J
    .locals 2

    iget-wide v0, p0, Lcom/google/android/location/cache/PersistentState;->cacheRefreshMillisSinceBoot:J

    return-wide v0
.end method

.method public getCellCache()Lcom/google/android/location/cache/TemporalCache;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/android/location/cache/TemporalCache",
            "<",
            "Ljava/lang/String;",
            "Lcom/google/android/location/data/Position;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/google/android/location/cache/PersistentState;->cellCache:Lcom/google/android/location/cache/TemporalCache;

    return-object v0
.end method

.method public getLastRefreshIsSuccess()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/location/cache/PersistentState;->lastRefreshIsSuccess:Z

    return v0
.end method

.method public getWifiCache()Lcom/google/android/location/cache/TemporalCache;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/android/location/cache/TemporalCache",
            "<",
            "Ljava/lang/Long;",
            "Lcom/google/android/location/data/WifiApPosition;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/google/android/location/cache/PersistentState;->wifiCache:Lcom/google/android/location/cache/TemporalCache;

    return-object v0
.end method

.method public load(Lcom/google/android/location/os/Os;)V
    .locals 15
    .param p1    # Lcom/google/android/location/os/Os;

    new-instance v9, Ljava/io/File;

    invoke-interface/range {p1 .. p1}, Lcom/google/android/location/os/Os;->persistentStateDir()Ljava/io/File;

    move-result-object v0

    const-string v2, "nlp_state"

    invoke-direct {v9, v0, v2}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    invoke-interface/range {p1 .. p1}, Lcom/google/android/location/os/Os;->bootTime()J

    move-result-wide v3

    invoke-interface/range {p1 .. p1}, Lcom/google/android/location/os/Os;->millisSinceBoot()J

    move-result-wide v5

    :try_start_0
    new-instance v8, Ljava/io/FileInputStream;

    invoke-direct {v8, v9}, Ljava/io/FileInputStream;-><init>(Ljava/io/File;)V

    new-instance v1, Ljava/io/BufferedInputStream;

    invoke-direct {v1, v8}, Ljava/io/BufferedInputStream;-><init>(Ljava/io/InputStream;)V

    invoke-interface/range {p1 .. p1}, Lcom/google/android/location/os/Os;->getEncryptionKey()Ljavax/crypto/SecretKey;

    move-result-object v2

    move-object v0, p0

    invoke-virtual/range {v0 .. v6}, Lcom/google/android/location/cache/PersistentState;->load(Ljava/io/InputStream;Ljavax/crypto/SecretKey;JJ)V

    invoke-virtual {v1}, Ljava/io/BufferedInputStream;->close()V
    :try_end_0
    .catch Ljava/io/FileNotFoundException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/SecurityException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_2

    :goto_0
    return-void

    :catch_0
    move-exception v7

    invoke-direct {p0, v5, v6}, Lcom/google/android/location/cache/PersistentState;->reset(J)V

    const-string v0, "PersistentState"

    const-string v2, "Clearing persistent state, last refresh: %s"

    const/4 v10, 0x1

    new-array v10, v10, [Ljava/lang/Object;

    const/4 v11, 0x0

    new-instance v12, Ljava/util/Date;

    iget-wide v13, p0, Lcom/google/android/location/cache/PersistentState;->cacheRefreshMillisSinceBoot:J

    add-long/2addr v13, v3

    invoke-direct {v12, v13, v14}, Ljava/util/Date;-><init>(J)V

    aput-object v12, v10, v11

    invoke-static {v2, v10}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Lcom/google/android/location/utils/logging/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    const-string v0, "PersistentState"

    const-string v2, "No existing nlp persistent state."

    invoke-static {v0, v2}, Lcom/google/android/location/utils/logging/Logger;->i(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    :catch_1
    move-exception v7

    invoke-direct {p0, v5, v6}, Lcom/google/android/location/cache/PersistentState;->reset(J)V

    const-string v0, "PersistentState"

    invoke-virtual {v7}, Ljava/lang/SecurityException;->getMessage()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2, v7}, Lcom/google/android/location/utils/logging/Logger;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0

    :catch_2
    move-exception v7

    invoke-direct {p0, v5, v6}, Lcom/google/android/location/cache/PersistentState;->reset(J)V

    const-string v0, "PersistentState"

    invoke-virtual {v7}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2, v7}, Lcom/google/android/location/utils/logging/Logger;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0
.end method

.method load(Ljava/io/InputStream;Ljavax/crypto/SecretKey;JJ)V
    .locals 22
    .param p1    # Ljava/io/InputStream;
    .param p2    # Ljavax/crypto/SecretKey;
    .param p3    # J
    .param p5    # J
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    :try_start_0
    new-instance v21, Ljava/io/DataInputStream;

    move-object/from16 v0, v21

    move-object/from16 v1, p1

    invoke-direct {v0, v1}, Ljava/io/DataInputStream;-><init>(Ljava/io/InputStream;)V

    invoke-virtual/range {v21 .. v21}, Ljava/io/DataInputStream;->readUnsignedShort()I

    move-result v20

    const/16 v7, 0xa

    move/from16 v0, v20

    if-ne v0, v7, :cond_1

    invoke-direct/range {p0 .. p2}, Lcom/google/android/location/cache/PersistentState;->decryptInput(Ljava/io/InputStream;Ljavax/crypto/SecretKey;)Ljava/io/DataInput;

    move-result-object v15

    invoke-interface {v15}, Ljava/io/DataInput;->readLong()J

    move-result-wide v3

    invoke-interface {v15}, Ljava/io/DataInput;->readLong()J

    move-result-wide v5

    move-wide/from16 v7, p3

    move-wide/from16 v9, p5

    invoke-static/range {v3 .. v10}, Lcom/google/android/location/cache/PersistentState;->convertTimeSinceBoot(JJJJ)J

    move-result-wide v7

    invoke-interface {v15}, Ljava/io/DataInput;->readBoolean()Z

    move-result v9

    move-object/from16 v0, p0

    invoke-virtual {v0, v7, v8, v9}, Lcom/google/android/location/cache/PersistentState;->setCacheRefreshMillisSinceBoot(JZ)V

    invoke-interface {v15}, Ljava/io/DataInput;->readBoolean()Z

    move-result v17

    if-eqz v17, :cond_0

    sget-object v7, Lcom/google/android/location/data/LocatorResult;->SAVER:Lcom/google/android/location/data/Persistent;

    invoke-interface {v7, v15}, Lcom/google/android/location/data/Persistent;->load(Ljava/io/DataInput;)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Lcom/google/android/location/data/LocatorResult;

    move-object/from16 v0, p0

    iput-object v7, v0, Lcom/google/android/location/cache/PersistentState;->lastKnown:Lcom/google/android/location/data/LocatorResult;

    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/google/android/location/cache/PersistentState;->lastKnown:Lcom/google/android/location/data/LocatorResult;

    iget-wide v9, v7, Lcom/google/android/location/data/LocatorResult;->reportTime:J

    move-wide v7, v3

    move-wide/from16 v11, p3

    move-wide/from16 v13, p5

    invoke-static/range {v7 .. v14}, Lcom/google/android/location/cache/PersistentState;->convertTimeSinceBoot(JJJJ)J

    move-result-wide v18

    new-instance v7, Lcom/google/android/location/data/LocatorResult;

    move-object/from16 v0, p0

    iget-object v8, v0, Lcom/google/android/location/cache/PersistentState;->lastKnown:Lcom/google/android/location/data/LocatorResult;

    iget-object v8, v8, Lcom/google/android/location/data/LocatorResult;->position:Lcom/google/android/location/data/Position;

    move-object/from16 v0, p0

    iget-object v9, v0, Lcom/google/android/location/cache/PersistentState;->lastKnown:Lcom/google/android/location/data/LocatorResult;

    iget-object v9, v9, Lcom/google/android/location/data/LocatorResult;->status:Lcom/google/android/location/data/LocatorResult$ResultStatus;

    move-wide/from16 v0, v18

    invoke-direct {v7, v8, v9, v0, v1}, Lcom/google/android/location/data/LocatorResult;-><init>(Lcom/google/android/location/data/Position;Lcom/google/android/location/data/LocatorResult$ResultStatus;J)V

    move-object/from16 v0, p0

    iput-object v7, v0, Lcom/google/android/location/cache/PersistentState;->lastKnown:Lcom/google/android/location/data/LocatorResult;

    :cond_0
    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/google/android/location/cache/PersistentState;->cellCache:Lcom/google/android/location/cache/TemporalCache;

    invoke-virtual {v7, v15}, Lcom/google/android/location/cache/TemporalCache;->load(Ljava/io/DataInput;)Lcom/google/android/location/cache/TemporalCache;

    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/google/android/location/cache/PersistentState;->wifiCache:Lcom/google/android/location/cache/TemporalCache;

    invoke-virtual {v7, v15}, Lcom/google/android/location/cache/TemporalCache;->load(Ljava/io/DataInput;)Lcom/google/android/location/cache/TemporalCache;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    const-string v7, "PersistentState"

    const-string v8, "Loaded %d cell, %d wifi. Last refresh time: %s"

    const/4 v9, 0x3

    new-array v9, v9, [Ljava/lang/Object;

    const/4 v10, 0x0

    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/google/android/location/cache/PersistentState;->cellCache:Lcom/google/android/location/cache/TemporalCache;

    invoke-virtual {v11}, Lcom/google/android/location/cache/TemporalCache;->size()I

    move-result v11

    invoke-static {v11}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v11

    aput-object v11, v9, v10

    const/4 v10, 0x1

    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/google/android/location/cache/PersistentState;->wifiCache:Lcom/google/android/location/cache/TemporalCache;

    invoke-virtual {v11}, Lcom/google/android/location/cache/TemporalCache;->size()I

    move-result v11

    invoke-static {v11}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v11

    aput-object v11, v9, v10

    const/4 v10, 0x2

    new-instance v11, Ljava/util/Date;

    move-object/from16 v0, p0

    iget-wide v12, v0, Lcom/google/android/location/cache/PersistentState;->cacheRefreshMillisSinceBoot:J

    add-long v12, v12, p3

    invoke-direct {v11, v12, v13}, Ljava/util/Date;-><init>(J)V

    aput-object v11, v9, v10

    invoke-static {v8, v9}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8}, Lcom/google/android/location/utils/logging/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    :goto_0
    return-void

    :cond_1
    :try_start_1
    move-object/from16 v0, p0

    move-wide/from16 v1, p5

    invoke-direct {v0, v1, v2}, Lcom/google/android/location/cache/PersistentState;->reset(J)V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_0

    :catch_0
    move-exception v16

    move-object/from16 v0, p0

    move-wide/from16 v1, p5

    invoke-direct {v0, v1, v2}, Lcom/google/android/location/cache/PersistentState;->reset(J)V

    throw v16
.end method

.method public save(Lcom/google/android/location/os/Os;)V
    .locals 8
    .param p1    # Lcom/google/android/location/os/Os;

    invoke-interface {p1}, Lcom/google/android/location/os/Os;->persistentStateDir()Ljava/io/File;

    move-result-object v1

    if-nez v1, :cond_0

    :goto_0
    return-void

    :cond_0
    new-instance v4, Ljava/io/File;

    const-string v5, "nlp_state"

    invoke-direct {v4, v1, v5}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    :try_start_0
    new-instance v3, Ljava/io/FileOutputStream;

    invoke-direct {v3, v4}, Ljava/io/FileOutputStream;-><init>(Ljava/io/File;)V

    invoke-interface {p1, v4}, Lcom/google/android/location/os/Os;->makeFilePrivate(Ljava/io/File;)V

    new-instance v0, Ljava/io/BufferedOutputStream;

    invoke-direct {v0, v3}, Ljava/io/BufferedOutputStream;-><init>(Ljava/io/OutputStream;)V

    invoke-interface {p1}, Lcom/google/android/location/os/Os;->getEncryptionKey()Ljavax/crypto/SecretKey;

    move-result-object v5

    invoke-interface {p1}, Lcom/google/android/location/os/Os;->bootTime()J

    move-result-wide v6

    invoke-virtual {p0, v0, v5, v6, v7}, Lcom/google/android/location/cache/PersistentState;->save(Ljava/io/OutputStream;Ljavax/crypto/SecretKey;J)V
    :try_end_0
    .catch Ljava/io/FileNotFoundException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/SecurityException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_2

    goto :goto_0

    :catch_0
    move-exception v2

    const-string v5, "PersistentState"

    invoke-virtual {v2}, Ljava/io/FileNotFoundException;->getMessage()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6, v2}, Lcom/google/android/location/utils/logging/Logger;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0

    :catch_1
    move-exception v2

    const-string v5, "PersistentState"

    invoke-virtual {v2}, Ljava/lang/SecurityException;->getMessage()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6, v2}, Lcom/google/android/location/utils/logging/Logger;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0

    :catch_2
    move-exception v2

    const-string v5, "PersistentState"

    invoke-virtual {v2}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6, v2}, Lcom/google/android/location/utils/logging/Logger;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0
.end method

.method save(Ljava/io/OutputStream;Ljavax/crypto/SecretKey;J)V
    .locals 4
    .param p1    # Ljava/io/OutputStream;
    .param p2    # Ljavax/crypto/SecretKey;
    .param p3    # J
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    new-instance v1, Ljava/io/DataOutputStream;

    invoke-direct {v1, p1}, Ljava/io/DataOutputStream;-><init>(Ljava/io/OutputStream;)V

    const/16 v2, 0xa

    invoke-virtual {v1, v2}, Ljava/io/DataOutputStream;->writeShort(I)V

    invoke-virtual {v1}, Ljava/io/DataOutputStream;->flush()V

    invoke-direct {p0, p1, p2}, Lcom/google/android/location/cache/PersistentState;->encryptOutput(Ljava/io/OutputStream;Ljavax/crypto/SecretKey;)Ljava/io/DataOutputStream;

    move-result-object v0

    invoke-virtual {v0, p3, p4}, Ljava/io/DataOutputStream;->writeLong(J)V

    iget-wide v2, p0, Lcom/google/android/location/cache/PersistentState;->cacheRefreshMillisSinceBoot:J

    invoke-virtual {v0, v2, v3}, Ljava/io/DataOutputStream;->writeLong(J)V

    iget-boolean v2, p0, Lcom/google/android/location/cache/PersistentState;->lastRefreshIsSuccess:Z

    invoke-virtual {v0, v2}, Ljava/io/DataOutputStream;->writeBoolean(Z)V

    iget-object v2, p0, Lcom/google/android/location/cache/PersistentState;->lastKnown:Lcom/google/android/location/data/LocatorResult;

    sget-object v3, Lcom/google/android/location/cache/PersistentState;->INVALID_RESULT:Lcom/google/android/location/data/LocatorResult;

    if-eq v2, v3, :cond_0

    const/4 v2, 0x1

    invoke-virtual {v0, v2}, Ljava/io/DataOutputStream;->writeBoolean(Z)V

    sget-object v2, Lcom/google/android/location/data/LocatorResult;->SAVER:Lcom/google/android/location/data/Persistent;

    iget-object v3, p0, Lcom/google/android/location/cache/PersistentState;->lastKnown:Lcom/google/android/location/data/LocatorResult;

    invoke-interface {v2, v3, v0}, Lcom/google/android/location/data/Persistent;->save(Ljava/lang/Object;Ljava/io/DataOutput;)V

    :goto_0
    iget-object v2, p0, Lcom/google/android/location/cache/PersistentState;->cellCache:Lcom/google/android/location/cache/TemporalCache;

    iget-object v3, p0, Lcom/google/android/location/cache/PersistentState;->cellCache:Lcom/google/android/location/cache/TemporalCache;

    invoke-virtual {v2, v3, v0}, Lcom/google/android/location/cache/TemporalCache;->save(Lcom/google/android/location/cache/TemporalCache;Ljava/io/DataOutput;)V

    iget-object v2, p0, Lcom/google/android/location/cache/PersistentState;->wifiCache:Lcom/google/android/location/cache/TemporalCache;

    iget-object v3, p0, Lcom/google/android/location/cache/PersistentState;->wifiCache:Lcom/google/android/location/cache/TemporalCache;

    invoke-virtual {v2, v3, v0}, Lcom/google/android/location/cache/TemporalCache;->save(Lcom/google/android/location/cache/TemporalCache;Ljava/io/DataOutput;)V

    invoke-virtual {v0}, Ljava/io/DataOutputStream;->close()V

    return-void

    :cond_0
    const/4 v2, 0x0

    invoke-virtual {v0, v2}, Ljava/io/DataOutputStream;->writeBoolean(Z)V

    goto :goto_0
.end method

.method public setCacheRefreshMillisSinceBoot(JZ)V
    .locals 0
    .param p1    # J
    .param p3    # Z

    iput-wide p1, p0, Lcom/google/android/location/cache/PersistentState;->cacheRefreshMillisSinceBoot:J

    iput-boolean p3, p0, Lcom/google/android/location/cache/PersistentState;->lastRefreshIsSuccess:Z

    return-void
.end method

.method public updateCachesFromGlsQueryResponse(Lcom/google/gmm/common/io/protocol/ProtoBuf;ZJ)V
    .locals 41
    .param p1    # Lcom/google/gmm/common/io/protocol/ProtoBuf;
    .param p2    # Z
    .param p3    # J

    invoke-static/range {p1 .. p1}, Lcom/google/android/location/utils/Utils;->containsOneValidGLocRequestElement(Lcom/google/gmm/common/io/protocol/ProtoBuf;)Z

    move-result v5

    if-nez v5, :cond_1

    :cond_0
    return-void

    :cond_1
    const/4 v5, 0x2

    move-object/from16 v0, p1

    invoke-virtual {v0, v5}, Lcom/google/gmm/common/io/protocol/ProtoBuf;->getProtoBuf(I)Lcom/google/gmm/common/io/protocol/ProtoBuf;

    move-result-object v40

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/google/android/location/cache/PersistentState;->nlpParamsState:Lcom/google/android/location/os/NlpParametersState;

    invoke-virtual {v5}, Lcom/google/android/location/os/NlpParametersState;->getCellDatabaseVersion()I

    move-result v8

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/google/android/location/cache/PersistentState;->nlpParamsState:Lcom/google/android/location/os/NlpParametersState;

    invoke-virtual {v5}, Lcom/google/android/location/os/NlpParametersState;->getWifiDatabaseVersion()I

    move-result v19

    const/16 v33, 0x0

    :goto_0
    const/4 v5, 0x3

    move-object/from16 v0, v40

    invoke-virtual {v0, v5}, Lcom/google/gmm/common/io/protocol/ProtoBuf;->getCount(I)I

    move-result v5

    move/from16 v0, v33

    if-ge v0, v5, :cond_0

    const/4 v5, 0x3

    move-object/from16 v0, v40

    move/from16 v1, v33

    invoke-virtual {v0, v5, v1}, Lcom/google/gmm/common/io/protocol/ProtoBuf;->getProtoBuf(II)Lcom/google/gmm/common/io/protocol/ProtoBuf;

    move-result-object v30

    const/16 v35, 0x0

    const/4 v12, 0x0

    const/4 v13, -0x1

    const/4 v14, -0x1

    const/16 v36, -0x1

    const v15, 0x9c40

    const/4 v5, 0x1

    move-object/from16 v0, v30

    invoke-virtual {v0, v5}, Lcom/google/gmm/common/io/protocol/ProtoBuf;->has(I)Z

    move-result v5

    if-eqz v5, :cond_4

    const/4 v5, 0x1

    move-object/from16 v0, v30

    invoke-virtual {v0, v5}, Lcom/google/gmm/common/io/protocol/ProtoBuf;->getProtoBuf(I)Lcom/google/gmm/common/io/protocol/ProtoBuf;

    move-result-object v31

    const/4 v5, 0x3

    move-object/from16 v0, v31

    invoke-virtual {v0, v5}, Lcom/google/gmm/common/io/protocol/ProtoBuf;->has(I)Z

    move-result v5

    if-eqz v5, :cond_2

    const/4 v5, 0x1

    move-object/from16 v0, v31

    invoke-virtual {v0, v5}, Lcom/google/gmm/common/io/protocol/ProtoBuf;->has(I)Z

    move-result v5

    if-eqz v5, :cond_2

    const/4 v5, 0x4

    move-object/from16 v0, v31

    invoke-virtual {v0, v5}, Lcom/google/gmm/common/io/protocol/ProtoBuf;->has(I)Z

    move-result v5

    if-eqz v5, :cond_2

    const/4 v5, 0x1

    move-object/from16 v0, v31

    invoke-virtual {v0, v5}, Lcom/google/gmm/common/io/protocol/ProtoBuf;->getProtoBuf(I)Lcom/google/gmm/common/io/protocol/ProtoBuf;

    move-result-object v5

    const/4 v6, 0x1

    invoke-virtual {v5, v6}, Lcom/google/gmm/common/io/protocol/ProtoBuf;->getInt(I)I

    move-result v35

    const/4 v5, 0x1

    move-object/from16 v0, v31

    invoke-virtual {v0, v5}, Lcom/google/gmm/common/io/protocol/ProtoBuf;->getProtoBuf(I)Lcom/google/gmm/common/io/protocol/ProtoBuf;

    move-result-object v5

    const/4 v6, 0x2

    invoke-virtual {v5, v6}, Lcom/google/gmm/common/io/protocol/ProtoBuf;->getInt(I)I

    move-result v12

    const/4 v5, 0x3

    move-object/from16 v0, v31

    invoke-virtual {v0, v5}, Lcom/google/gmm/common/io/protocol/ProtoBuf;->getInt(I)I

    move-result v5

    mul-int/lit16 v13, v5, 0x3e8

    const/4 v5, 0x4

    move-object/from16 v0, v31

    invoke-virtual {v0, v5}, Lcom/google/gmm/common/io/protocol/ProtoBuf;->getInt(I)I

    move-result v14

    :cond_2
    const/16 v5, 0x8

    move-object/from16 v0, v31

    invoke-virtual {v0, v5}, Lcom/google/gmm/common/io/protocol/ProtoBuf;->has(I)Z

    move-result v5

    if-eqz v5, :cond_3

    const/16 v5, 0x8

    move-object/from16 v0, v31

    invoke-virtual {v0, v5}, Lcom/google/gmm/common/io/protocol/ProtoBuf;->getInt(I)I

    move-result v36

    :cond_3
    const/16 v5, 0x14

    move-object/from16 v0, v31

    invoke-virtual {v0, v5}, Lcom/google/gmm/common/io/protocol/ProtoBuf;->has(I)Z

    move-result v5

    if-eqz v5, :cond_4

    const/16 v5, 0x14

    move-object/from16 v0, v31

    invoke-virtual {v0, v5}, Lcom/google/gmm/common/io/protocol/ProtoBuf;->getInt(I)I

    move-result v5

    mul-int/lit16 v15, v5, 0x3e8

    :cond_4
    const/4 v5, 0x2

    move-object/from16 v0, v30

    invoke-virtual {v0, v5}, Lcom/google/gmm/common/io/protocol/ProtoBuf;->has(I)Z

    move-result v5

    if-eqz v5, :cond_8

    const/4 v5, 0x3

    move/from16 v0, v36

    if-eq v0, v5, :cond_8

    const/4 v5, 0x2

    move-object/from16 v0, v30

    invoke-virtual {v0, v5}, Lcom/google/gmm/common/io/protocol/ProtoBuf;->getProtoBuf(I)Lcom/google/gmm/common/io/protocol/ProtoBuf;

    move-result-object v29

    const/4 v5, 0x2

    move-object/from16 v0, v29

    invoke-virtual {v0, v5}, Lcom/google/gmm/common/io/protocol/ProtoBuf;->getInt(I)I

    move-result v26

    const/4 v5, 0x1

    move-object/from16 v0, v29

    invoke-virtual {v0, v5}, Lcom/google/gmm/common/io/protocol/ProtoBuf;->getInt(I)I

    move-result v34

    const/16 v37, -0x1

    const/16 v38, -0x1

    const/16 v39, -0x1

    const/4 v5, 0x4

    move-object/from16 v0, v29

    invoke-virtual {v0, v5}, Lcom/google/gmm/common/io/protocol/ProtoBuf;->has(I)Z

    move-result v5

    if-eqz v5, :cond_5

    const/4 v5, 0x4

    move-object/from16 v0, v29

    invoke-virtual {v0, v5}, Lcom/google/gmm/common/io/protocol/ProtoBuf;->getInt(I)I

    move-result v37

    :cond_5
    const/4 v5, 0x3

    move-object/from16 v0, v29

    invoke-virtual {v0, v5}, Lcom/google/gmm/common/io/protocol/ProtoBuf;->has(I)Z

    move-result v5

    if-eqz v5, :cond_6

    const/4 v5, 0x3

    move-object/from16 v0, v29

    invoke-virtual {v0, v5}, Lcom/google/gmm/common/io/protocol/ProtoBuf;->getInt(I)I

    move-result v38

    :cond_6
    const/16 v5, 0xa

    move-object/from16 v0, v29

    invoke-virtual {v0, v5}, Lcom/google/gmm/common/io/protocol/ProtoBuf;->has(I)Z

    move-result v5

    if-eqz v5, :cond_7

    const/16 v5, 0xa

    move-object/from16 v0, v29

    invoke-virtual {v0, v5}, Lcom/google/gmm/common/io/protocol/ProtoBuf;->getInt(I)I

    move-result v39

    :cond_7
    new-instance v9, Lcom/google/android/location/data/Position;

    move/from16 v0, v35

    invoke-direct {v9, v0, v12, v13, v14}, Lcom/google/android/location/data/Position;-><init>(IIII)V

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/google/android/location/cache/PersistentState;->cellCache:Lcom/google/android/location/cache/TemporalCache;

    move/from16 v0, v39

    move/from16 v1, v37

    move/from16 v2, v38

    move/from16 v3, v34

    move/from16 v4, v26

    invoke-static {v0, v1, v2, v3, v4}, Lcom/google/android/location/cache/PersistentState;->getCellCacheKey(IIIII)Ljava/lang/String;

    move-result-object v7

    move/from16 v6, p2

    move-wide/from16 v10, p3

    invoke-virtual/range {v5 .. v11}, Lcom/google/android/location/cache/TemporalCache;->insertPosition(ZLjava/lang/Object;ILjava/lang/Object;J)V

    :cond_8
    const/4 v5, 0x3

    move-object/from16 v0, v30

    invoke-virtual {v0, v5}, Lcom/google/gmm/common/io/protocol/ProtoBuf;->has(I)Z

    move-result v5

    if-eqz v5, :cond_b

    const/4 v5, 0x3

    move-object/from16 v0, v30

    invoke-virtual {v0, v5}, Lcom/google/gmm/common/io/protocol/ProtoBuf;->getProtoBuf(I)Lcom/google/gmm/common/io/protocol/ProtoBuf;

    move-result-object v32

    const-wide/16 v23, -0x1

    const/16 v5, 0x8

    move-object/from16 v0, v32

    invoke-virtual {v0, v5}, Lcom/google/gmm/common/io/protocol/ProtoBuf;->has(I)Z

    move-result v5

    if-eqz v5, :cond_c

    const/16 v5, 0x8

    move-object/from16 v0, v32

    invoke-virtual {v0, v5}, Lcom/google/gmm/common/io/protocol/ProtoBuf;->getLong(I)J

    move-result-wide v23

    :goto_1
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/google/android/location/cache/PersistentState;->wifiCache:Lcom/google/android/location/cache/TemporalCache;

    invoke-static/range {v23 .. v24}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v6

    invoke-virtual {v5, v6}, Lcom/google/android/location/cache/TemporalCache;->lookupWithoutUpdatingLastSeen(Ljava/lang/Object;)Lcom/google/android/location/cache/CacheResult;

    move-result-object v25

    if-nez v25, :cond_d

    if-eqz p2, :cond_d

    const/16 v27, 0x1

    :goto_2
    if-eqz v25, :cond_e

    if-eqz p2, :cond_9

    invoke-virtual/range {v25 .. v25}, Lcom/google/android/location/cache/CacheResult;->getPosition()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/google/android/location/data/WifiApPosition;

    invoke-virtual {v5}, Lcom/google/android/location/data/WifiApPosition;->isOutlier()Z

    move-result v5

    if-nez v5, :cond_e

    :cond_9
    const/16 v28, 0x1

    :goto_3
    if-nez v27, :cond_a

    if-eqz v28, :cond_b

    :cond_a
    new-instance v10, Lcom/google/android/location/data/WifiApPosition;

    move/from16 v11, v35

    invoke-direct/range {v10 .. v15}, Lcom/google/android/location/data/WifiApPosition;-><init>(IIIII)V

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/location/cache/PersistentState;->wifiCache:Lcom/google/android/location/cache/TemporalCache;

    move-object/from16 v16, v0

    const/16 v17, 0x1

    invoke-static/range {v23 .. v24}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v18

    move-object/from16 v20, v10

    move-wide/from16 v21, p3

    invoke-virtual/range {v16 .. v22}, Lcom/google/android/location/cache/TemporalCache;->insertPosition(ZLjava/lang/Object;ILjava/lang/Object;J)V

    :cond_b
    add-int/lit8 v33, v33, 0x1

    goto/16 :goto_0

    :cond_c
    const/4 v5, 0x1

    move-object/from16 v0, v32

    invoke-virtual {v0, v5}, Lcom/google/gmm/common/io/protocol/ProtoBuf;->getString(I)Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Lcom/google/android/location/data/WifiUtils;->macToLong(Ljava/lang/String;)J

    move-result-wide v23

    goto :goto_1

    :cond_d
    const/16 v27, 0x0

    goto :goto_2

    :cond_e
    const/16 v28, 0x0

    goto :goto_3
.end method
