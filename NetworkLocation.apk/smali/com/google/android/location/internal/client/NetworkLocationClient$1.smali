.class Lcom/google/android/location/internal/client/NetworkLocationClient$1;
.super Lcom/google/android/location/internal/ILocationListener$Stub;
.source "NetworkLocationClient.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/location/internal/client/NetworkLocationClient;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/location/internal/client/NetworkLocationClient;


# direct methods
.method constructor <init>(Lcom/google/android/location/internal/client/NetworkLocationClient;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/location/internal/client/NetworkLocationClient$1;->this$0:Lcom/google/android/location/internal/client/NetworkLocationClient;

    invoke-direct {p0}, Lcom/google/android/location/internal/ILocationListener$Stub;-><init>()V

    return-void
.end method


# virtual methods
.method public onLocationChanged(Landroid/location/Location;)V
    .locals 1
    .param p1    # Landroid/location/Location;

    invoke-static {}, Landroid/os/Binder;->clearCallingIdentity()J

    iget-object v0, p0, Lcom/google/android/location/internal/client/NetworkLocationClient$1;->this$0:Lcom/google/android/location/internal/client/NetworkLocationClient;

    # getter for: Lcom/google/android/location/internal/client/NetworkLocationClient;->clientReporter:Lcom/google/android/location/internal/client/ClientReporter;
    invoke-static {v0}, Lcom/google/android/location/internal/client/NetworkLocationClient;->access$100(Lcom/google/android/location/internal/client/NetworkLocationClient;)Lcom/google/android/location/internal/client/ClientReporter;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/google/android/location/internal/client/ClientReporter;->onLocationChanged(Landroid/location/Location;)V

    return-void
.end method

.method public onProviderDisabled()V
    .locals 1

    invoke-static {}, Landroid/os/Binder;->clearCallingIdentity()J

    iget-object v0, p0, Lcom/google/android/location/internal/client/NetworkLocationClient$1;->this$0:Lcom/google/android/location/internal/client/NetworkLocationClient;

    # getter for: Lcom/google/android/location/internal/client/NetworkLocationClient;->clientReporter:Lcom/google/android/location/internal/client/ClientReporter;
    invoke-static {v0}, Lcom/google/android/location/internal/client/NetworkLocationClient;->access$100(Lcom/google/android/location/internal/client/NetworkLocationClient;)Lcom/google/android/location/internal/client/ClientReporter;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/location/internal/client/ClientReporter;->onProviderDisabled()V

    return-void
.end method

.method public onProviderEnabled()V
    .locals 1

    invoke-static {}, Landroid/os/Binder;->clearCallingIdentity()J

    iget-object v0, p0, Lcom/google/android/location/internal/client/NetworkLocationClient$1;->this$0:Lcom/google/android/location/internal/client/NetworkLocationClient;

    # getter for: Lcom/google/android/location/internal/client/NetworkLocationClient;->clientReporter:Lcom/google/android/location/internal/client/ClientReporter;
    invoke-static {v0}, Lcom/google/android/location/internal/client/NetworkLocationClient;->access$100(Lcom/google/android/location/internal/client/NetworkLocationClient;)Lcom/google/android/location/internal/client/ClientReporter;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/location/internal/client/ClientReporter;->onProviderEnabled()V

    return-void
.end method

.method public onStatusChanged(ILandroid/os/Bundle;)V
    .locals 1
    .param p1    # I
    .param p2    # Landroid/os/Bundle;

    invoke-static {}, Landroid/os/Binder;->clearCallingIdentity()J

    iget-object v0, p0, Lcom/google/android/location/internal/client/NetworkLocationClient$1;->this$0:Lcom/google/android/location/internal/client/NetworkLocationClient;

    # getter for: Lcom/google/android/location/internal/client/NetworkLocationClient;->clientReporter:Lcom/google/android/location/internal/client/ClientReporter;
    invoke-static {v0}, Lcom/google/android/location/internal/client/NetworkLocationClient;->access$100(Lcom/google/android/location/internal/client/NetworkLocationClient;)Lcom/google/android/location/internal/client/ClientReporter;

    move-result-object v0

    invoke-virtual {v0, p1, p2}, Lcom/google/android/location/internal/client/ClientReporter;->onStatusChanged(ILandroid/os/Bundle;)V

    return-void
.end method
