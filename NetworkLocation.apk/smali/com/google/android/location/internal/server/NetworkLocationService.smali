.class public Lcom/google/android/location/internal/server/NetworkLocationService;
.super Landroid/app/Service;
.source "NetworkLocationService.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/location/internal/server/NetworkLocationService$Implementation;
    }
.end annotation


# instance fields
.field private binder:Lcom/google/android/location/internal/INetworkLocationInternal$Stub;

.field private eventLog:Lcom/google/android/location/os/EventLog;

.field private serviceThread:Lcom/google/android/location/internal/server/ServiceThread;

.field private final thread:Landroid/os/HandlerThread;

.field public final timestamper:Lcom/google/android/location/os/EventLog$Timestamper;


# direct methods
.method public constructor <init>()V
    .locals 3

    invoke-direct {p0}, Landroid/app/Service;-><init>()V

    new-instance v0, Lcom/google/android/location/internal/server/NetworkLocationService$1;

    invoke-direct {v0, p0}, Lcom/google/android/location/internal/server/NetworkLocationService$1;-><init>(Lcom/google/android/location/internal/server/NetworkLocationService;)V

    iput-object v0, p0, Lcom/google/android/location/internal/server/NetworkLocationService;->timestamper:Lcom/google/android/location/os/EventLog$Timestamper;

    new-instance v0, Landroid/os/HandlerThread;

    const-string v1, "gmmInternalNlpService"

    const/4 v2, -0x4

    invoke-direct {v0, v1, v2}, Landroid/os/HandlerThread;-><init>(Ljava/lang/String;I)V

    iput-object v0, p0, Lcom/google/android/location/internal/server/NetworkLocationService;->thread:Landroid/os/HandlerThread;

    return-void
.end method

.method static synthetic access$100(Lcom/google/android/location/internal/server/NetworkLocationService;)Lcom/google/android/location/internal/server/ServiceThread;
    .locals 1
    .param p0    # Lcom/google/android/location/internal/server/NetworkLocationService;

    iget-object v0, p0, Lcom/google/android/location/internal/server/NetworkLocationService;->serviceThread:Lcom/google/android/location/internal/server/ServiceThread;

    return-object v0
.end method

.method static synthetic access$200(Lcom/google/android/location/internal/server/NetworkLocationService;Ljava/io/PrintWriter;)V
    .locals 0
    .param p0    # Lcom/google/android/location/internal/server/NetworkLocationService;
    .param p1    # Ljava/io/PrintWriter;

    invoke-direct {p0, p1}, Lcom/google/android/location/internal/server/NetworkLocationService;->dump(Ljava/io/PrintWriter;)V

    return-void
.end method

.method private dump(Ljava/io/PrintWriter;)V
    .locals 10
    .param p1    # Ljava/io/PrintWriter;

    invoke-static {}, Lcom/google/android/location/os/real/RealOs;->sinceBootMillis()J

    move-result-wide v4

    invoke-static {}, Lcom/google/android/location/os/real/RealOs;->sinceEpochMillis()J

    move-result-wide v8

    new-instance v1, Ljava/text/SimpleDateFormat;

    const-string v0, "MM-dd HH:mm:ss.SSS"

    sget-object v2, Ljava/util/Locale;->US:Ljava/util/Locale;

    invoke-direct {v1, v0, v2}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;Ljava/util/Locale;)V

    new-instance v0, Ljava/util/Date;

    invoke-direct {v0, v8, v9}, Ljava/util/Date;-><init>(J)V

    invoke-virtual {v1, v0}, Ljava/text/SimpleDateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v7

    const-string v0, "elapsedRealtime "

    invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    invoke-virtual {p1, v4, v5}, Ljava/io/PrintWriter;->print(J)V

    const-string v0, " is time "

    invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    invoke-virtual {p1, v7}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/google/android/location/internal/server/NetworkLocationService;->serviceThread:Lcom/google/android/location/internal/server/ServiceThread;

    invoke-virtual {v0, p1}, Lcom/google/android/location/internal/server/ServiceThread;->addLocationListenerStats(Ljava/io/PrintWriter;)V

    iget-object v0, p0, Lcom/google/android/location/internal/server/NetworkLocationService;->eventLog:Lcom/google/android/location/os/EventLog;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/location/internal/server/NetworkLocationService;->eventLog:Lcom/google/android/location/os/EventLog;

    sub-long v2, v8, v4

    move-object v6, p1

    invoke-virtual/range {v0 .. v6}, Lcom/google/android/location/os/EventLog;->dump(Ljava/text/Format;JJLjava/io/PrintWriter;)V

    :cond_0
    return-void
.end method

.method private declared-synchronized init()V
    .locals 9

    monitor-enter p0

    :try_start_0
    iget-object v6, p0, Lcom/google/android/location/internal/server/NetworkLocationService;->eventLog:Lcom/google/android/location/os/EventLog;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-eqz v6, :cond_0

    :goto_0
    monitor-exit p0

    return-void

    :cond_0
    :try_start_1
    iget-object v6, p0, Lcom/google/android/location/internal/server/NetworkLocationService;->thread:Landroid/os/HandlerThread;

    invoke-virtual {v6}, Landroid/os/HandlerThread;->start()V

    new-instance v6, Lcom/google/android/location/internal/server/ServiceThread;

    iget-object v7, p0, Lcom/google/android/location/internal/server/NetworkLocationService;->thread:Landroid/os/HandlerThread;

    invoke-virtual {v7}, Landroid/os/HandlerThread;->getLooper()Landroid/os/Looper;

    move-result-object v7

    invoke-direct {v6, p0, v7}, Lcom/google/android/location/internal/server/ServiceThread;-><init>(Landroid/content/Context;Landroid/os/Looper;)V

    iput-object v6, p0, Lcom/google/android/location/internal/server/NetworkLocationService;->serviceThread:Lcom/google/android/location/internal/server/ServiceThread;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    const/4 v5, 0x0

    :try_start_2
    const-string v6, "nlp_debug_log"

    invoke-virtual {p0, v6}, Lcom/google/android/location/internal/server/NetworkLocationService;->openFileInput(Ljava/lang/String;)Ljava/io/FileInputStream;

    move-result-object v4

    const/4 v5, 0x1

    invoke-virtual {v4}, Ljava/io/FileInputStream;->close()V
    :try_end_2
    .catch Ljava/io/FileNotFoundException; {:try_start_2 .. :try_end_2} :catch_0
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_1
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    :goto_1
    const/4 v1, 0x0

    if-eqz v5, :cond_1

    :try_start_3
    new-instance v0, Ljava/io/BufferedOutputStream;

    const-string v6, "nlp_debug_log"

    const v7, 0x8000

    invoke-virtual {p0, v6, v7}, Lcom/google/android/location/internal/server/NetworkLocationService;->openFileOutput(Ljava/lang/String;I)Ljava/io/FileOutputStream;

    move-result-object v6

    invoke-direct {v0, v6}, Ljava/io/BufferedOutputStream;-><init>(Ljava/io/OutputStream;)V

    new-instance v2, Ljava/io/PrintWriter;

    invoke-direct {v2, v0}, Ljava/io/PrintWriter;-><init>(Ljava/io/OutputStream;)V
    :try_end_3
    .catch Ljava/io/FileNotFoundException; {:try_start_3 .. :try_end_3} :catch_2
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    move-object v1, v2

    :cond_1
    :goto_2
    :try_start_4
    new-instance v7, Lcom/google/android/location/os/EventLog;

    iget-object v8, p0, Lcom/google/android/location/internal/server/NetworkLocationService;->timestamper:Lcom/google/android/location/os/EventLog$Timestamper;

    invoke-static {}, Lcom/google/android/location/os/real/SdkSpecific;->getInstance()Lcom/google/android/location/os/real/SdkSpecific;

    move-result-object v6

    invoke-virtual {v6}, Lcom/google/android/location/os/real/SdkSpecific;->adbBugreportCallsDump()Z

    move-result v6

    if-eqz v6, :cond_2

    const/4 v6, 0x0

    :goto_3
    invoke-direct {v7, v8, v6, v1}, Lcom/google/android/location/os/EventLog;-><init>(Lcom/google/android/location/os/EventLog$Timestamper;Lcom/google/android/location/utils/logging/LoggerInterface;Ljava/io/PrintWriter;)V

    iput-object v7, p0, Lcom/google/android/location/internal/server/NetworkLocationService;->eventLog:Lcom/google/android/location/os/EventLog;

    iget-object v6, p0, Lcom/google/android/location/internal/server/NetworkLocationService;->serviceThread:Lcom/google/android/location/internal/server/ServiceThread;

    iget-object v7, p0, Lcom/google/android/location/internal/server/NetworkLocationService;->eventLog:Lcom/google/android/location/os/EventLog;

    invoke-virtual {v6, v7}, Lcom/google/android/location/internal/server/ServiceThread;->setEventLog(Lcom/google/android/location/os/EventLog;)V
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v6

    monitor-exit p0

    throw v6

    :catch_0
    move-exception v3

    const/4 v5, 0x0

    goto :goto_1

    :catch_1
    move-exception v3

    const/4 v5, 0x0

    goto :goto_1

    :catch_2
    move-exception v3

    :try_start_5
    const-string v6, "gmmInternalNlpService"

    const-string v7, "debug log file missing for output!?"

    invoke-static {v6, v7}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_2

    :cond_2
    new-instance v6, Lcom/google/android/location/os/real/AndroidLogger;

    invoke-direct {v6}, Lcom/google/android/location/os/real/AndroidLogger;-><init>()V
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    goto :goto_3
.end method


# virtual methods
.method protected dump(Ljava/io/FileDescriptor;Ljava/io/PrintWriter;[Ljava/lang/String;)V
    .locals 0
    .param p1    # Ljava/io/FileDescriptor;
    .param p2    # Ljava/io/PrintWriter;
    .param p3    # [Ljava/lang/String;

    invoke-direct {p0, p2}, Lcom/google/android/location/internal/server/NetworkLocationService;->dump(Ljava/io/PrintWriter;)V

    return-void
.end method

.method public onBind(Landroid/content/Intent;)Landroid/os/IBinder;
    .locals 1
    .param p1    # Landroid/content/Intent;

    iget-object v0, p0, Lcom/google/android/location/internal/server/NetworkLocationService;->binder:Lcom/google/android/location/internal/INetworkLocationInternal$Stub;

    return-object v0
.end method

.method public onCreate()V
    .locals 3

    invoke-static {}, Lcom/google/android/location/os/real/SdkSpecific;->getInstance()Lcom/google/android/location/os/real/SdkSpecific;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/location/os/real/SdkSpecific;->allowThreadDiskWrites()V

    invoke-direct {p0}, Lcom/google/android/location/internal/server/NetworkLocationService;->init()V

    new-instance v0, Lcom/google/android/location/os/real/AndroidLogger;

    iget-object v1, p0, Lcom/google/android/location/internal/server/NetworkLocationService;->eventLog:Lcom/google/android/location/os/EventLog;

    invoke-direct {v0, v1}, Lcom/google/android/location/os/real/AndroidLogger;-><init>(Lcom/google/android/location/os/EventLog;)V

    invoke-static {v0}, Lcom/google/android/location/utils/logging/Logger;->registerLogger(Lcom/google/android/location/utils/logging/LoggerInterface;)V

    const-string v0, "gmmInternalNlpService"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "onCreate "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/location/utils/logging/Logger;->i(Ljava/lang/String;Ljava/lang/String;)V

    new-instance v0, Lcom/google/android/location/internal/server/NetworkLocationService$Implementation;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/google/android/location/internal/server/NetworkLocationService$Implementation;-><init>(Lcom/google/android/location/internal/server/NetworkLocationService;Lcom/google/android/location/internal/server/NetworkLocationService$1;)V

    iput-object v0, p0, Lcom/google/android/location/internal/server/NetworkLocationService;->binder:Lcom/google/android/location/internal/INetworkLocationInternal$Stub;

    iget-object v0, p0, Lcom/google/android/location/internal/server/NetworkLocationService;->serviceThread:Lcom/google/android/location/internal/server/ServiceThread;

    invoke-virtual {v0}, Lcom/google/android/location/internal/server/ServiceThread;->createService()V

    return-void
.end method

.method public onDestroy()V
    .locals 3

    const/4 v2, 0x0

    const-string v0, "gmmInternalNlpService"

    const-string v1, "onDestroy"

    invoke-static {v0, v1}, Lcom/google/android/location/utils/logging/Logger;->i(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/google/android/location/internal/server/NetworkLocationService;->serviceThread:Lcom/google/android/location/internal/server/ServiceThread;

    invoke-virtual {v0}, Lcom/google/android/location/internal/server/ServiceThread;->destroyService()V

    iput-object v2, p0, Lcom/google/android/location/internal/server/NetworkLocationService;->binder:Lcom/google/android/location/internal/INetworkLocationInternal$Stub;

    const-string v0, "gmmInternalNlpService"

    const-string v1, "unregistering logger"

    invoke-static {v0, v1}, Lcom/google/android/location/utils/logging/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    invoke-static {v2}, Lcom/google/android/location/utils/logging/Logger;->registerLogger(Lcom/google/android/location/utils/logging/LoggerInterface;)V

    return-void
.end method

.method public onStart(Landroid/content/Intent;I)V
    .locals 10
    .param p1    # Landroid/content/Intent;
    .param p2    # I

    if-nez p1, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    const-string v7, "gmmInternalNlpService"

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "internal NLP onStart("

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {p1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, ") "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {p0}, Lcom/google/android/location/internal/server/NetworkLocationService;->getPackageName()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    const-string v7, "com.google.android.location.internal.EXTRA_PENDING_INTENT"

    invoke-virtual {p1, v7}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    move-result v7

    if-eqz v7, :cond_4

    const-string v7, "com.google.android.location.internal.EXTRA_PENDING_INTENT"

    invoke-virtual {p1, v7}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v4

    check-cast v4, Landroid/app/PendingIntent;

    const-string v7, "com.google.android.location.internal.EXTRA_PERIOD_MILLIS"

    const-wide/16 v8, -0x1

    invoke-virtual {p1, v7, v8, v9}, Landroid/content/Intent;->getLongExtra(Ljava/lang/String;J)J

    move-result-wide v5

    const-string v7, "com.google.android.location.internal.EXTRA_DEBUG_INFO"

    const/4 v8, 0x0

    invoke-virtual {p1, v7, v8}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v1

    const-wide/16 v7, -0x1

    cmp-long v7, v5, v7

    if-nez v7, :cond_2

    const-string v7, "com.google.android.location.internal.EXTRA_PERIOD_MILLIS"

    const/4 v8, -0x1

    invoke-virtual {p1, v7, v8}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v7

    int-to-long v5, v7

    :cond_2
    const-wide/32 v7, 0x7fffffff

    invoke-static {v5, v6, v7, v8}, Ljava/lang/Math;->min(JJ)J

    move-result-wide v5

    if-eqz v4, :cond_4

    const-wide/16 v7, 0x0

    cmp-long v7, v5, v7

    if-ltz v7, :cond_4

    invoke-virtual {v4}, Landroid/app/PendingIntent;->getTargetPackage()Ljava/lang/String;

    move-result-object v3

    if-eqz v1, :cond_3

    invoke-virtual {p0}, Lcom/google/android/location/internal/server/NetworkLocationService;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v2

    const-string v7, "android.permission.ACCESS_WIFI_STATE"

    invoke-virtual {v2, v7, v3}, Landroid/content/pm/PackageManager;->checkPermission(Ljava/lang/String;Ljava/lang/String;)I

    move-result v7

    if-nez v7, :cond_5

    const/4 v0, 0x1

    :goto_1
    const-string v7, "gmmInternalNlpService"

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "package "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, " hasWifi? "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8}, Lcom/google/android/location/utils/logging/Logger;->i(Ljava/lang/String;Ljava/lang/String;)V

    if-eqz v1, :cond_6

    if-eqz v0, :cond_6

    const/4 v1, 0x1

    :cond_3
    :goto_2
    const-string v7, "gmmInternalNlpService"

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "received pending intent from "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, " "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8}, Lcom/google/android/location/utils/logging/Logger;->i(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v7, p0, Lcom/google/android/location/internal/server/NetworkLocationService;->serviceThread:Lcom/google/android/location/internal/server/ServiceThread;

    const-wide/16 v8, 0x3e8

    div-long v8, v5, v8

    long-to-int v8, v8

    invoke-virtual {v7, v4, v8, v1}, Lcom/google/android/location/internal/server/ServiceThread;->addLocationPendingIntent(Landroid/app/PendingIntent;IZ)V

    :cond_4
    const-string v7, "com.google.android.location.internal.EXTRA_ACTIVITY_PENDING_INTENT"

    invoke-virtual {p1, v7}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    move-result v7

    if-eqz v7, :cond_0

    const-string v7, "com.google.android.location.internal.EXTRA_ACTIVITY_PENDING_INTENT"

    invoke-virtual {p1, v7}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v4

    check-cast v4, Landroid/app/PendingIntent;

    if-eqz v4, :cond_0

    invoke-virtual {v4}, Landroid/app/PendingIntent;->getTargetPackage()Ljava/lang/String;

    move-result-object v3

    const-string v7, "gmmInternalNlpService"

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "received activity pending intent from "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8}, Lcom/google/android/location/utils/logging/Logger;->i(Ljava/lang/String;Ljava/lang/String;)V

    invoke-static {v3}, Lcom/google/android/location/internal/server/ActivityDetectionWhitelist;->isInWhitelist(Ljava/lang/String;)Z

    move-result v7

    if-eqz v7, :cond_7

    iget-object v7, p0, Lcom/google/android/location/internal/server/NetworkLocationService;->serviceThread:Lcom/google/android/location/internal/server/ServiceThread;

    invoke-virtual {v7, v4}, Lcom/google/android/location/internal/server/ServiceThread;->addActivityPendingIntent(Landroid/app/PendingIntent;)V

    goto/16 :goto_0

    :cond_5
    const/4 v0, 0x0

    goto/16 :goto_1

    :cond_6
    const/4 v1, 0x0

    goto :goto_2

    :cond_7
    const-string v7, "gmmInternalNlpService"

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "Rejecting request for activity detection. Application not in whitelist. Please contact lbs-team. "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8}, Lcom/google/android/location/utils/logging/Logger;->i(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0
.end method
