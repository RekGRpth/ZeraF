.class public Lcom/google/android/location/NetworkLocationService;
.super Landroid/app/Service;
.source "NetworkLocationService.java"


# instance fields
.field private mProvider:Lcom/google/android/location/NetworkLocationProvider;


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Landroid/app/Service;-><init>()V

    return-void
.end method


# virtual methods
.method public onBind(Landroid/content/Intent;)Landroid/os/IBinder;
    .locals 1
    .param p1    # Landroid/content/Intent;

    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/google/android/location/NetworkLocationService;->mProvider:Lcom/google/android/location/NetworkLocationProvider;

    if-nez v0, :cond_0

    invoke-static {}, Lcom/google/android/location/NetworkLocationProvider;->getInstance()Lcom/google/android/location/NetworkLocationProvider;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/location/NetworkLocationService;->mProvider:Lcom/google/android/location/NetworkLocationProvider;

    :cond_0
    iget-object v0, p0, Lcom/google/android/location/NetworkLocationService;->mProvider:Lcom/google/android/location/NetworkLocationProvider;

    invoke-virtual {v0}, Lcom/google/android/location/NetworkLocationProvider;->getBinder()Landroid/os/IBinder;

    move-result-object v0

    monitor-exit p0

    return-object v0

    :catchall_0
    move-exception v0

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public onCreate()V
    .locals 1

    invoke-virtual {p0}, Lcom/google/android/location/NetworkLocationService;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/location/NetworkLocationProvider;->init(Landroid/content/Context;)V

    invoke-virtual {p0}, Lcom/google/android/location/NetworkLocationService;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/location/os/real/GlsClient;->init(Landroid/content/Context;)V

    return-void
.end method

.method public onDestroy()V
    .locals 1

    monitor-enter p0

    const/4 v0, 0x0

    :try_start_0
    iput-object v0, p0, Lcom/google/android/location/NetworkLocationService;->mProvider:Lcom/google/android/location/NetworkLocationProvider;

    monitor-exit p0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public onUnbind(Landroid/content/Intent;)Z
    .locals 1
    .param p1    # Landroid/content/Intent;

    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/google/android/location/NetworkLocationService;->mProvider:Lcom/google/android/location/NetworkLocationProvider;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/location/NetworkLocationService;->mProvider:Lcom/google/android/location/NetworkLocationProvider;

    invoke-virtual {v0}, Lcom/google/android/location/NetworkLocationProvider;->onDisable()V

    :cond_0
    monitor-exit p0

    const/4 v0, 0x0

    return v0

    :catchall_0
    move-exception v0

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method
