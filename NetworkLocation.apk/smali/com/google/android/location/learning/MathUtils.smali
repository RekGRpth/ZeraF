.class public Lcom/google/android/location/learning/MathUtils;
.super Ljava/lang/Object;
.source "MathUtils.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/location/learning/MathUtils$MeanStdDev;
    }
.end annotation


# direct methods
.method private constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static computeMeanStdDev([F)Lcom/google/android/location/learning/MathUtils$MeanStdDev;
    .locals 10
    .param p0    # [F

    const/4 v6, 0x0

    const/4 v7, 0x0

    array-length v4, p0

    const/4 v2, 0x0

    :goto_0
    if-ge v2, v4, :cond_0

    aget v8, p0, v2

    add-float/2addr v6, v8

    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    :cond_0
    int-to-float v8, v4

    div-float v3, v6, v8

    const/4 v1, 0x0

    const/4 v2, 0x0

    :goto_1
    if-ge v2, v4, :cond_1

    aget v8, p0, v2

    sub-float v0, v8, v3

    mul-float v8, v0, v0

    add-float/2addr v1, v8

    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    :cond_1
    const/4 v8, 0x1

    if-ne v4, v8, :cond_2

    new-instance v8, Lcom/google/android/location/learning/MathUtils$MeanStdDev;

    const/4 v9, 0x0

    invoke-direct {v8, v3, v9}, Lcom/google/android/location/learning/MathUtils$MeanStdDev;-><init>(FF)V

    :goto_2
    return-object v8

    :cond_2
    add-int/lit8 v8, v4, -0x1

    int-to-float v8, v8

    div-float v8, v1, v8

    float-to-double v8, v8

    invoke-static {v8, v9}, Ljava/lang/Math;->sqrt(D)D

    move-result-wide v8

    double-to-float v5, v8

    new-instance v8, Lcom/google/android/location/learning/MathUtils$MeanStdDev;

    invoke-direct {v8, v3, v5}, Lcom/google/android/location/learning/MathUtils$MeanStdDev;-><init>(FF)V

    goto :goto_2
.end method
