.class public Lcom/google/android/location/NetworkProvider;
.super Ljava/lang/Object;
.source "NetworkProvider.java"

# interfaces
.implements Lcom/google/android/location/os/Callbacks;


# instance fields
.field final activityDetectionScheduler:Lcom/google/android/location/activity/ActivityDetectionScheduler;

.field final cacheUpdater:Lcom/google/android/location/cache/CacheUpdater;

.field final locator:Lcom/google/android/location/NetworkLocator;

.field final modelState:Lcom/google/android/location/cache/ModelState;

.field final os:Lcom/google/android/location/os/Os;

.field final seenDevicesCache:Lcom/google/android/location/cache/SeenDevicesCache;

.field final state:Lcom/google/android/location/cache/PersistentState;

.field final stats:Lcom/google/android/location/Stats;

.field final tracker:Lcom/google/android/location/data/CellScreenTracker;


# direct methods
.method public constructor <init>(Lcom/google/android/location/os/Os;Lcom/google/android/location/os/NlpParametersState;Lcom/google/android/location/cache/ModelState;)V
    .locals 7
    .param p1    # Lcom/google/android/location/os/Os;
    .param p2    # Lcom/google/android/location/os/NlpParametersState;
    .param p3    # Lcom/google/android/location/cache/ModelState;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/google/android/location/NetworkProvider;->os:Lcom/google/android/location/os/Os;

    new-instance v0, Lcom/google/android/location/cache/PersistentState;

    invoke-interface {p1}, Lcom/google/android/location/os/Os;->millisSinceBoot()J

    move-result-wide v1

    invoke-direct {v0, p2, v1, v2}, Lcom/google/android/location/cache/PersistentState;-><init>(Lcom/google/android/location/os/NlpParametersState;J)V

    iput-object v0, p0, Lcom/google/android/location/NetworkProvider;->state:Lcom/google/android/location/cache/PersistentState;

    new-instance v0, Lcom/google/android/location/cache/SeenDevicesCache;

    invoke-direct {v0, p1, p2}, Lcom/google/android/location/cache/SeenDevicesCache;-><init>(Lcom/google/android/location/os/Os;Lcom/google/android/location/os/NlpParametersState;)V

    iput-object v0, p0, Lcom/google/android/location/NetworkProvider;->seenDevicesCache:Lcom/google/android/location/cache/SeenDevicesCache;

    new-instance v0, Lcom/google/android/location/cache/CacheUpdater;

    iget-object v1, p0, Lcom/google/android/location/NetworkProvider;->state:Lcom/google/android/location/cache/PersistentState;

    iget-object v2, p0, Lcom/google/android/location/NetworkProvider;->seenDevicesCache:Lcom/google/android/location/cache/SeenDevicesCache;

    invoke-direct {v0, p1, v1, v2, p2}, Lcom/google/android/location/cache/CacheUpdater;-><init>(Lcom/google/android/location/os/Os;Lcom/google/android/location/cache/PersistentState;Lcom/google/android/location/cache/SeenDevicesCache;Lcom/google/android/location/os/NlpParametersState;)V

    iput-object v0, p0, Lcom/google/android/location/NetworkProvider;->cacheUpdater:Lcom/google/android/location/cache/CacheUpdater;

    iput-object p3, p0, Lcom/google/android/location/NetworkProvider;->modelState:Lcom/google/android/location/cache/ModelState;

    new-instance v4, Lcom/google/android/location/localizer/ModelStateManager;

    invoke-direct {v4, p1, p3}, Lcom/google/android/location/localizer/ModelStateManager;-><init>(Lcom/google/android/location/os/Os;Lcom/google/android/location/cache/ModelState;)V

    new-instance v0, Lcom/google/android/location/Stats;

    iget-object v1, p0, Lcom/google/android/location/NetworkProvider;->state:Lcom/google/android/location/cache/PersistentState;

    iget-object v2, p0, Lcom/google/android/location/NetworkProvider;->seenDevicesCache:Lcom/google/android/location/cache/SeenDevicesCache;

    invoke-direct {v0, p1, v1, v2}, Lcom/google/android/location/Stats;-><init>(Lcom/google/android/location/os/Os;Lcom/google/android/location/cache/PersistentState;Lcom/google/android/location/cache/SeenDevicesCache;)V

    iput-object v0, p0, Lcom/google/android/location/NetworkProvider;->stats:Lcom/google/android/location/Stats;

    new-instance v0, Lcom/google/android/location/data/CellScreenTracker;

    invoke-direct {v0}, Lcom/google/android/location/data/CellScreenTracker;-><init>()V

    iput-object v0, p0, Lcom/google/android/location/NetworkProvider;->tracker:Lcom/google/android/location/data/CellScreenTracker;

    new-instance v0, Lcom/google/android/location/NetworkLocator;

    iget-object v2, p0, Lcom/google/android/location/NetworkProvider;->state:Lcom/google/android/location/cache/PersistentState;

    iget-object v3, p0, Lcom/google/android/location/NetworkProvider;->seenDevicesCache:Lcom/google/android/location/cache/SeenDevicesCache;

    iget-object v5, p0, Lcom/google/android/location/NetworkProvider;->stats:Lcom/google/android/location/Stats;

    iget-object v6, p0, Lcom/google/android/location/NetworkProvider;->tracker:Lcom/google/android/location/data/CellScreenTracker;

    move-object v1, p1

    invoke-direct/range {v0 .. v6}, Lcom/google/android/location/NetworkLocator;-><init>(Lcom/google/android/location/os/Os;Lcom/google/android/location/cache/PersistentState;Lcom/google/android/location/cache/SeenDevicesCache;Lcom/google/android/location/localizer/ModelStateManager;Lcom/google/android/location/Stats;Lcom/google/android/location/data/CellScreenTracker;)V

    iput-object v0, p0, Lcom/google/android/location/NetworkProvider;->locator:Lcom/google/android/location/NetworkLocator;

    new-instance v0, Lcom/google/android/location/activity/ActivityDetectionScheduler;

    new-instance v1, Lcom/google/android/location/activity/SensorRateUtil;

    invoke-direct {v1, p1}, Lcom/google/android/location/activity/SensorRateUtil;-><init>(Lcom/google/android/location/os/Os;)V

    invoke-direct {v0, p1, v1}, Lcom/google/android/location/activity/ActivityDetectionScheduler;-><init>(Lcom/google/android/location/os/Os;Lcom/google/android/location/activity/SensorRateUtil;)V

    iput-object v0, p0, Lcom/google/android/location/NetworkProvider;->activityDetectionScheduler:Lcom/google/android/location/activity/ActivityDetectionScheduler;

    invoke-interface {p1, p0}, Lcom/google/android/location/os/Os;->registerCallbacks(Lcom/google/android/location/os/Callbacks;)V

    return-void
.end method


# virtual methods
.method public airplaneModeChanged(Z)V
    .locals 1
    .param p1    # Z

    iget-object v0, p0, Lcom/google/android/location/NetworkProvider;->locator:Lcom/google/android/location/NetworkLocator;

    invoke-virtual {v0, p1}, Lcom/google/android/location/NetworkLocator;->airplaneModeChanged(Z)V

    return-void
.end method

.method public alarmRing(I)V
    .locals 1
    .param p1    # I

    packed-switch p1, :pswitch_data_0

    :cond_0
    :goto_0
    return-void

    :pswitch_0
    iget-object v0, p0, Lcom/google/android/location/NetworkProvider;->locator:Lcom/google/android/location/NetworkLocator;

    invoke-virtual {v0}, Lcom/google/android/location/NetworkLocator;->alarmRing()V

    goto :goto_0

    :pswitch_1
    iget-object v0, p0, Lcom/google/android/location/NetworkProvider;->cacheUpdater:Lcom/google/android/location/cache/CacheUpdater;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/location/NetworkProvider;->cacheUpdater:Lcom/google/android/location/cache/CacheUpdater;

    invoke-virtual {v0, p1}, Lcom/google/android/location/cache/CacheUpdater;->alarmRing(I)V

    goto :goto_0

    :pswitch_2
    iget-object v0, p0, Lcom/google/android/location/NetworkProvider;->activityDetectionScheduler:Lcom/google/android/location/activity/ActivityDetectionScheduler;

    invoke-virtual {v0, p1}, Lcom/google/android/location/activity/ActivityDetectionScheduler;->alarmRing(I)V

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public batteryChanged(IIZ)V
    .locals 0
    .param p1    # I
    .param p2    # I
    .param p3    # Z

    return-void
.end method

.method public cellScanResults(Lcom/google/android/location/data/CellState;)V
    .locals 3
    .param p1    # Lcom/google/android/location/data/CellState;

    iget-object v0, p0, Lcom/google/android/location/NetworkProvider;->tracker:Lcom/google/android/location/data/CellScreenTracker;

    iget-object v1, p0, Lcom/google/android/location/NetworkProvider;->os:Lcom/google/android/location/os/Os;

    invoke-interface {v1}, Lcom/google/android/location/os/Os;->millisSinceBoot()J

    move-result-wide v1

    invoke-virtual {v0, p1, v1, v2}, Lcom/google/android/location/data/CellScreenTracker;->setCurrentCellTower(Lcom/google/android/location/data/CellState;J)V

    iget-object v0, p0, Lcom/google/android/location/NetworkProvider;->locator:Lcom/google/android/location/NetworkLocator;

    invoke-virtual {v0, p1}, Lcom/google/android/location/NetworkLocator;->cellScanResults(Lcom/google/android/location/data/CellState;)V

    return-void
.end method

.method public cellSignalStrength(I)V
    .locals 3
    .param p1    # I

    iget-object v0, p0, Lcom/google/android/location/NetworkProvider;->locator:Lcom/google/android/location/NetworkLocator;

    invoke-virtual {v0, p1}, Lcom/google/android/location/NetworkLocator;->cellSignalStrength(I)V

    iget-object v0, p0, Lcom/google/android/location/NetworkProvider;->tracker:Lcom/google/android/location/data/CellScreenTracker;

    iget-object v1, p0, Lcom/google/android/location/NetworkProvider;->os:Lcom/google/android/location/os/Os;

    invoke-interface {v1}, Lcom/google/android/location/os/Os;->millisSinceBoot()J

    move-result-wide v1

    invoke-virtual {v0, p1, v1, v2}, Lcom/google/android/location/data/CellScreenTracker;->setCurrentCellTowerSignalStrength(IJ)V

    return-void
.end method

.method public dataNetworkChanged(ZZ)V
    .locals 0
    .param p1    # Z
    .param p2    # Z

    return-void
.end method

.method public glsDeviceLocationResponse(Lcom/google/gmm/common/io/protocol/ProtoBuf;)V
    .locals 1
    .param p1    # Lcom/google/gmm/common/io/protocol/ProtoBuf;

    iget-object v0, p0, Lcom/google/android/location/NetworkProvider;->cacheUpdater:Lcom/google/android/location/cache/CacheUpdater;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/location/NetworkProvider;->cacheUpdater:Lcom/google/android/location/cache/CacheUpdater;

    invoke-virtual {v0, p1}, Lcom/google/android/location/cache/CacheUpdater;->glsDeviceLocationResponse(Lcom/google/gmm/common/io/protocol/ProtoBuf;)V

    :cond_0
    return-void
.end method

.method public glsModelQueryResponse(Lcom/google/gmm/common/io/protocol/ProtoBuf;)V
    .locals 1
    .param p1    # Lcom/google/gmm/common/io/protocol/ProtoBuf;

    iget-object v0, p0, Lcom/google/android/location/NetworkProvider;->locator:Lcom/google/android/location/NetworkLocator;

    invoke-virtual {v0, p1}, Lcom/google/android/location/NetworkLocator;->glsModelQueryResponse(Lcom/google/gmm/common/io/protocol/ProtoBuf;)V

    return-void
.end method

.method public glsQueryResponse(Lcom/google/gmm/common/io/protocol/ProtoBuf;)V
    .locals 1
    .param p1    # Lcom/google/gmm/common/io/protocol/ProtoBuf;

    iget-object v0, p0, Lcom/google/android/location/NetworkProvider;->locator:Lcom/google/android/location/NetworkLocator;

    invoke-virtual {v0, p1}, Lcom/google/android/location/NetworkLocator;->glsQueryResponse(Lcom/google/gmm/common/io/protocol/ProtoBuf;)V

    return-void
.end method

.method public initialize()V
    .locals 3

    iget-object v0, p0, Lcom/google/android/location/NetworkProvider;->state:Lcom/google/android/location/cache/PersistentState;

    iget-object v1, p0, Lcom/google/android/location/NetworkProvider;->os:Lcom/google/android/location/os/Os;

    invoke-virtual {v0, v1}, Lcom/google/android/location/cache/PersistentState;->load(Lcom/google/android/location/os/Os;)V

    iget-object v0, p0, Lcom/google/android/location/NetworkProvider;->seenDevicesCache:Lcom/google/android/location/cache/SeenDevicesCache;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/location/NetworkProvider;->seenDevicesCache:Lcom/google/android/location/cache/SeenDevicesCache;

    invoke-virtual {v0}, Lcom/google/android/location/cache/SeenDevicesCache;->load()V

    :cond_0
    iget-object v0, p0, Lcom/google/android/location/NetworkProvider;->cacheUpdater:Lcom/google/android/location/cache/CacheUpdater;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/location/NetworkProvider;->cacheUpdater:Lcom/google/android/location/cache/CacheUpdater;

    invoke-virtual {v0}, Lcom/google/android/location/cache/CacheUpdater;->initialize()V

    :cond_1
    iget-object v0, p0, Lcom/google/android/location/NetworkProvider;->stats:Lcom/google/android/location/Stats;

    iget-object v1, p0, Lcom/google/android/location/NetworkProvider;->os:Lcom/google/android/location/os/Os;

    invoke-interface {v1}, Lcom/google/android/location/os/Os;->millisSinceBoot()J

    move-result-wide v1

    invoke-virtual {v0, v1, v2}, Lcom/google/android/location/Stats;->reset(J)V

    iget-object v0, p0, Lcom/google/android/location/NetworkProvider;->modelState:Lcom/google/android/location/cache/ModelState;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/google/android/location/NetworkProvider;->modelState:Lcom/google/android/location/cache/ModelState;

    invoke-virtual {v0}, Lcom/google/android/location/cache/ModelState;->load()V

    :cond_2
    return-void
.end method

.method public nlpParamtersStateChanged(Lcom/google/android/location/os/NlpParametersState;)V
    .locals 0
    .param p1    # Lcom/google/android/location/os/NlpParametersState;

    return-void
.end method

.method public onActivityDetected(Lcom/google/android/location/clientlib/NlpActivity;)V
    .locals 0
    .param p1    # Lcom/google/android/location/clientlib/NlpActivity;

    return-void
.end method

.method public onActivityDetectionEnabled(Z)V
    .locals 1
    .param p1    # Z

    iget-object v0, p0, Lcom/google/android/location/NetworkProvider;->activityDetectionScheduler:Lcom/google/android/location/activity/ActivityDetectionScheduler;

    invoke-virtual {v0, p1}, Lcom/google/android/location/activity/ActivityDetectionScheduler;->activityDetectionEnabledChanged(Z)V

    return-void
.end method

.method public onTravelModeDetected(Lcom/google/android/location/activity/TravelDetectionManager$TravelDetectionResult;)V
    .locals 1
    .param p1    # Lcom/google/android/location/activity/TravelDetectionManager$TravelDetectionResult;

    iget-object v0, p0, Lcom/google/android/location/NetworkProvider;->activityDetectionScheduler:Lcom/google/android/location/activity/ActivityDetectionScheduler;

    invoke-virtual {v0, p1}, Lcom/google/android/location/activity/ActivityDetectionScheduler;->travelModeDetected(Lcom/google/android/location/activity/TravelDetectionManager$TravelDetectionResult;)V

    return-void
.end method

.method public quit(Z)V
    .locals 2
    .param p1    # Z

    if-eqz p1, :cond_0

    iget-object v0, p0, Lcom/google/android/location/NetworkProvider;->state:Lcom/google/android/location/cache/PersistentState;

    iget-object v1, p0, Lcom/google/android/location/NetworkProvider;->locator:Lcom/google/android/location/NetworkLocator;

    iget-object v1, v1, Lcom/google/android/location/NetworkLocator;->os:Lcom/google/android/location/os/Os;

    invoke-virtual {v0, v1}, Lcom/google/android/location/cache/PersistentState;->save(Lcom/google/android/location/os/Os;)V

    iget-object v0, p0, Lcom/google/android/location/NetworkProvider;->modelState:Lcom/google/android/location/cache/ModelState;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/location/NetworkProvider;->modelState:Lcom/google/android/location/cache/ModelState;

    invoke-virtual {v0}, Lcom/google/android/location/cache/ModelState;->save()V

    :cond_0
    return-void
.end method

.method public screenStateChanged(Z)V
    .locals 3
    .param p1    # Z

    iget-object v0, p0, Lcom/google/android/location/NetworkProvider;->tracker:Lcom/google/android/location/data/CellScreenTracker;

    iget-object v1, p0, Lcom/google/android/location/NetworkProvider;->os:Lcom/google/android/location/os/Os;

    invoke-interface {v1}, Lcom/google/android/location/os/Os;->millisSinceBoot()J

    move-result-wide v1

    invoke-virtual {v0, p1, v1, v2}, Lcom/google/android/location/data/CellScreenTracker;->setScreenState(ZJ)V

    iget-object v0, p0, Lcom/google/android/location/NetworkProvider;->activityDetectionScheduler:Lcom/google/android/location/activity/ActivityDetectionScheduler;

    invoke-virtual {v0, p1}, Lcom/google/android/location/activity/ActivityDetectionScheduler;->screenStateChanged(Z)V

    return-void
.end method

.method public setPeriod(IZ)V
    .locals 1
    .param p1    # I
    .param p2    # Z

    iget-object v0, p0, Lcom/google/android/location/NetworkProvider;->locator:Lcom/google/android/location/NetworkLocator;

    invoke-virtual {v0, p1, p2}, Lcom/google/android/location/NetworkLocator;->setPeriod(IZ)V

    return-void
.end method

.method public signalRmiRequest(Lcom/google/android/location/data/NetworkLocation;)V
    .locals 0
    .param p1    # Lcom/google/android/location/data/NetworkLocation;

    return-void
.end method

.method public wifiScanResults(Lcom/google/android/location/data/WifiScan;)V
    .locals 1
    .param p1    # Lcom/google/android/location/data/WifiScan;

    iget-object v0, p0, Lcom/google/android/location/NetworkProvider;->locator:Lcom/google/android/location/NetworkLocator;

    invoke-virtual {v0, p1}, Lcom/google/android/location/NetworkLocator;->wifiScanResults(Lcom/google/android/location/data/WifiScan;)V

    return-void
.end method

.method public wifiStateChanged(Z)V
    .locals 1
    .param p1    # Z

    iget-object v0, p0, Lcom/google/android/location/NetworkProvider;->locator:Lcom/google/android/location/NetworkLocator;

    invoke-virtual {v0, p1}, Lcom/google/android/location/NetworkLocator;->wifiStateChanged(Z)V

    return-void
.end method
