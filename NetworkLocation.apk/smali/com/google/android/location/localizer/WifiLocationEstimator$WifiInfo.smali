.class Lcom/google/android/location/localizer/WifiLocationEstimator$WifiInfo;
.super Ljava/lang/Object;
.source "WifiLocationEstimator.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/location/localizer/WifiLocationEstimator;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "WifiInfo"
.end annotation


# instance fields
.field private intersectionCount:I

.field private final mac:Ljava/lang/Long;

.field private final position:Lcom/google/android/location/data/Position;


# direct methods
.method public constructor <init>(Ljava/lang/Long;Lcom/google/android/location/data/Position;)V
    .locals 1
    .param p1    # Ljava/lang/Long;
    .param p2    # Lcom/google/android/location/data/Position;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x0

    iput v0, p0, Lcom/google/android/location/localizer/WifiLocationEstimator$WifiInfo;->intersectionCount:I

    iput-object p1, p0, Lcom/google/android/location/localizer/WifiLocationEstimator$WifiInfo;->mac:Ljava/lang/Long;

    iput-object p2, p0, Lcom/google/android/location/localizer/WifiLocationEstimator$WifiInfo;->position:Lcom/google/android/location/data/Position;

    return-void
.end method


# virtual methods
.method public getIntersectionCount()I
    .locals 1

    iget v0, p0, Lcom/google/android/location/localizer/WifiLocationEstimator$WifiInfo;->intersectionCount:I

    return v0
.end method

.method public getMac()Ljava/lang/Long;
    .locals 1

    iget-object v0, p0, Lcom/google/android/location/localizer/WifiLocationEstimator$WifiInfo;->mac:Ljava/lang/Long;

    return-object v0
.end method

.method public getPosition()Lcom/google/android/location/data/Position;
    .locals 1

    iget-object v0, p0, Lcom/google/android/location/localizer/WifiLocationEstimator$WifiInfo;->position:Lcom/google/android/location/data/Position;

    return-object v0
.end method

.method public incIntersectionCount()V
    .locals 1

    iget v0, p0, Lcom/google/android/location/localizer/WifiLocationEstimator$WifiInfo;->intersectionCount:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/google/android/location/localizer/WifiLocationEstimator$WifiInfo;->intersectionCount:I

    return-void
.end method

.method public setIntersectionCount(I)V
    .locals 0
    .param p1    # I

    iput p1, p0, Lcom/google/android/location/localizer/WifiLocationEstimator$WifiInfo;->intersectionCount:I

    return-void
.end method
