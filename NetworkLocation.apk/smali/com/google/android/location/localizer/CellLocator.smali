.class public Lcom/google/android/location/localizer/CellLocator;
.super Ljava/lang/Object;
.source "CellLocator.java"


# instance fields
.field private final cache:Lcom/google/android/location/cache/TemporalCache;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/location/cache/TemporalCache",
            "<",
            "Ljava/lang/String;",
            "Lcom/google/android/location/data/Position;",
            ">;"
        }
    .end annotation
.end field

.field private final centroid:Lcom/google/android/location/localizer/LocationCentroid;

.field private final clock:Lcom/google/android/location/os/Clock;


# direct methods
.method public constructor <init>(Lcom/google/android/location/cache/TemporalCache;Lcom/google/android/location/os/Clock;)V
    .locals 1
    .param p2    # Lcom/google/android/location/os/Clock;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/location/cache/TemporalCache",
            "<",
            "Ljava/lang/String;",
            "Lcom/google/android/location/data/Position;",
            ">;",
            "Lcom/google/android/location/os/Clock;",
            ")V"
        }
    .end annotation

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Lcom/google/android/location/localizer/LocationCentroid;

    invoke-direct {v0}, Lcom/google/android/location/localizer/LocationCentroid;-><init>()V

    iput-object v0, p0, Lcom/google/android/location/localizer/CellLocator;->centroid:Lcom/google/android/location/localizer/LocationCentroid;

    iput-object p1, p0, Lcom/google/android/location/localizer/CellLocator;->cache:Lcom/google/android/location/cache/TemporalCache;

    iput-object p2, p0, Lcom/google/android/location/localizer/CellLocator;->clock:Lcom/google/android/location/os/Clock;

    return-void
.end method

.method private findInCache(Lcom/google/android/location/data/CellState;Ljava/util/Map;J)Lcom/google/android/location/cache/CacheResult;
    .locals 3
    .param p1    # Lcom/google/android/location/data/CellState;
    .param p3    # J
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/location/data/CellState;",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Lcom/google/android/location/data/Position;",
            ">;J)",
            "Lcom/google/android/location/cache/CacheResult",
            "<",
            "Lcom/google/android/location/data/Position;",
            ">;"
        }
    .end annotation

    invoke-static {p1}, Lcom/google/android/location/cache/PersistentState;->getCellCacheKey(Lcom/google/android/location/data/CellState;)Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/location/localizer/CellLocator;->cache:Lcom/google/android/location/cache/TemporalCache;

    invoke-virtual {v2, v1, p3, p4}, Lcom/google/android/location/cache/TemporalCache;->lookup(Ljava/lang/Object;J)Lcom/google/android/location/cache/CacheResult;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/google/android/location/cache/CacheResult;->getPosition()Ljava/lang/Object;

    move-result-object v2

    invoke-interface {p2, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public computeLocation(Lcom/google/android/location/data/CellStatus;)Lcom/google/android/location/data/CellLocatorResult;
    .locals 28
    .param p1    # Lcom/google/android/location/data/CellStatus;

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/location/localizer/CellLocator;->clock:Lcom/google/android/location/os/Clock;

    invoke-interface {v4}, Lcom/google/android/location/os/Clock;->millisSinceBoot()J

    move-result-wide v22

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/location/localizer/CellLocator;->clock:Lcom/google/android/location/os/Clock;

    invoke-interface {v4}, Lcom/google/android/location/os/Clock;->millisSinceEpoch()J

    move-result-wide v24

    const/16 v26, 0x0

    const/16 v19, 0x0

    if-eqz p1, :cond_0

    invoke-virtual/range {p1 .. p1}, Lcom/google/android/location/data/CellStatus;->getPrimary()Lcom/google/android/location/data/CellState;

    move-result-object v26

    invoke-virtual/range {p1 .. p1}, Lcom/google/android/location/data/CellStatus;->getHistory()Ljava/util/List;

    move-result-object v19

    :cond_0
    new-instance v10, Ljava/util/HashMap;

    invoke-direct {v10}, Ljava/util/HashMap;-><init>()V

    if-eqz v26, :cond_1

    invoke-virtual/range {v26 .. v26}, Lcom/google/android/location/data/CellState;->isValid()Z

    move-result v4

    if-nez v4, :cond_2

    :cond_1
    new-instance v4, Lcom/google/android/location/data/CellLocatorResult;

    const/4 v5, 0x0

    sget-object v6, Lcom/google/android/location/data/LocatorResult$ResultStatus;->NO_LOCATION:Lcom/google/android/location/data/LocatorResult$ResultStatus;

    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/google/android/location/localizer/CellLocator;->clock:Lcom/google/android/location/os/Clock;

    invoke-interface {v7}, Lcom/google/android/location/os/Clock;->millisSinceBoot()J

    move-result-wide v7

    move-object/from16 v9, p1

    invoke-direct/range {v4 .. v10}, Lcom/google/android/location/data/CellLocatorResult;-><init>(Lcom/google/android/location/data/Position;Lcom/google/android/location/data/LocatorResult$ResultStatus;JLcom/google/android/location/data/CellStatus;Ljava/util/Map;)V

    :goto_0
    return-object v4

    :cond_2
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/location/localizer/CellLocator;->centroid:Lcom/google/android/location/localizer/LocationCentroid;

    invoke-virtual {v4}, Lcom/google/android/location/localizer/LocationCentroid;->reset()V

    move-object/from16 v0, p0

    move-object/from16 v1, v26

    move-wide/from16 v2, v24

    invoke-direct {v0, v1, v10, v2, v3}, Lcom/google/android/location/localizer/CellLocator;->findInCache(Lcom/google/android/location/data/CellState;Ljava/util/Map;J)Lcom/google/android/location/cache/CacheResult;

    move-result-object v27

    if-nez v27, :cond_3

    const-string v4, "CellLocator"

    const-string v6, "Primary cell miss in cache. Need server request."

    invoke-static {v4, v6}, Lcom/google/android/location/utils/logging/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    new-instance v4, Lcom/google/android/location/data/CellLocatorResult;

    const/4 v5, 0x0

    sget-object v6, Lcom/google/android/location/data/LocatorResult$ResultStatus;->CACHE_MISS:Lcom/google/android/location/data/LocatorResult$ResultStatus;

    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/google/android/location/localizer/CellLocator;->clock:Lcom/google/android/location/os/Clock;

    invoke-interface {v7}, Lcom/google/android/location/os/Clock;->millisSinceBoot()J

    move-result-wide v7

    move-object/from16 v9, p1

    invoke-direct/range {v4 .. v10}, Lcom/google/android/location/data/CellLocatorResult;-><init>(Lcom/google/android/location/data/Position;Lcom/google/android/location/data/LocatorResult$ResultStatus;JLcom/google/android/location/data/CellStatus;Ljava/util/Map;)V

    goto :goto_0

    :cond_3
    invoke-virtual/range {v27 .. v27}, Lcom/google/android/location/cache/CacheResult;->getPosition()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/google/android/location/data/Position;

    invoke-virtual {v4}, Lcom/google/android/location/data/Position;->isValid()Z

    move-result v4

    if-nez v4, :cond_4

    const-string v4, "CellLocator"

    const-string v6, "Primary cell is in cache with no location."

    invoke-static {v4, v6}, Lcom/google/android/location/utils/logging/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    new-instance v4, Lcom/google/android/location/data/CellLocatorResult;

    const/4 v5, 0x0

    sget-object v6, Lcom/google/android/location/data/LocatorResult$ResultStatus;->NO_LOCATION:Lcom/google/android/location/data/LocatorResult$ResultStatus;

    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/google/android/location/localizer/CellLocator;->clock:Lcom/google/android/location/os/Clock;

    invoke-interface {v7}, Lcom/google/android/location/os/Clock;->millisSinceBoot()J

    move-result-wide v7

    move-object/from16 v9, p1

    invoke-direct/range {v4 .. v10}, Lcom/google/android/location/data/CellLocatorResult;-><init>(Lcom/google/android/location/data/Position;Lcom/google/android/location/data/LocatorResult$ResultStatus;JLcom/google/android/location/data/CellStatus;Ljava/util/Map;)V

    goto :goto_0

    :cond_4
    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/google/android/location/localizer/CellLocator;->centroid:Lcom/google/android/location/localizer/LocationCentroid;

    invoke-virtual/range {v27 .. v27}, Lcom/google/android/location/cache/CacheResult;->getPosition()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/google/android/location/data/Position;

    invoke-virtual {v6, v4}, Lcom/google/android/location/localizer/LocationCentroid;->addLocation(Lcom/google/android/location/data/Position;)V

    if-eqz v19, :cond_6

    invoke-interface/range {v19 .. v19}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v21

    :cond_5
    :goto_1
    invoke-interface/range {v21 .. v21}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_6

    invoke-interface/range {v21 .. v21}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v20

    check-cast v20, Lcom/google/android/location/data/CellState;

    invoke-virtual/range {v20 .. v20}, Lcom/google/android/location/data/CellState;->getTime()J

    move-result-wide v6

    sub-long v6, v22, v6

    const-wide/16 v8, 0x7530

    cmp-long v4, v6, v8

    if-gez v4, :cond_5

    move-object/from16 v0, p0

    move-object/from16 v1, v20

    move-wide/from16 v2, v24

    invoke-direct {v0, v1, v10, v2, v3}, Lcom/google/android/location/localizer/CellLocator;->findInCache(Lcom/google/android/location/data/CellState;Ljava/util/Map;J)Lcom/google/android/location/cache/CacheResult;

    move-result-object v18

    if-eqz v18, :cond_5

    invoke-virtual/range {v18 .. v18}, Lcom/google/android/location/cache/CacheResult;->getPosition()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/google/android/location/data/Position;

    invoke-virtual {v4}, Lcom/google/android/location/data/Position;->isValid()Z

    move-result v4

    if-eqz v4, :cond_5

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/google/android/location/localizer/CellLocator;->centroid:Lcom/google/android/location/localizer/LocationCentroid;

    invoke-virtual/range {v18 .. v18}, Lcom/google/android/location/cache/CacheResult;->getPosition()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/google/android/location/data/Position;

    invoke-virtual {v6, v4}, Lcom/google/android/location/localizer/LocationCentroid;->addLocation(Lcom/google/android/location/data/Position;)V

    goto :goto_1

    :cond_6
    new-instance v5, Lcom/google/android/location/data/Position;

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/location/localizer/CellLocator;->centroid:Lcom/google/android/location/localizer/LocationCentroid;

    invoke-virtual {v4}, Lcom/google/android/location/localizer/LocationCentroid;->getCentroidLat()D

    move-result-wide v6

    invoke-static {v6, v7}, Lcom/google/android/location/localizer/LocalizerUtil;->degreesToE7(D)I

    move-result v4

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/google/android/location/localizer/CellLocator;->centroid:Lcom/google/android/location/localizer/LocationCentroid;

    invoke-virtual {v6}, Lcom/google/android/location/localizer/LocationCentroid;->getCentroidLng()D

    move-result-wide v6

    invoke-static {v6, v7}, Lcom/google/android/location/localizer/LocalizerUtil;->degreesToE7(D)I

    move-result v6

    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/google/android/location/localizer/CellLocator;->centroid:Lcom/google/android/location/localizer/LocationCentroid;

    invoke-virtual {v7}, Lcom/google/android/location/localizer/LocationCentroid;->getAccuracy()I

    move-result v7

    invoke-static {v7}, Lcom/google/android/location/localizer/LocalizerUtil;->metersToMm(I)I

    move-result v7

    move-object/from16 v0, p0

    iget-object v8, v0, Lcom/google/android/location/localizer/CellLocator;->centroid:Lcom/google/android/location/localizer/LocationCentroid;

    invoke-virtual {v8}, Lcom/google/android/location/localizer/LocationCentroid;->getConfidence()I

    move-result v8

    invoke-direct {v5, v4, v6, v7, v8}, Lcom/google/android/location/data/Position;-><init>(IIII)V

    invoke-static {v5}, Lcom/google/android/location/localizer/LocalizerUtil;->hasSaneValues(Lcom/google/android/location/data/Position;)Z

    move-result v4

    if-eqz v4, :cond_7

    const-string v4, "CellLocator"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "Found cell location: "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v4, v6}, Lcom/google/android/location/utils/logging/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    new-instance v4, Lcom/google/android/location/data/CellLocatorResult;

    sget-object v6, Lcom/google/android/location/data/LocatorResult$ResultStatus;->OK:Lcom/google/android/location/data/LocatorResult$ResultStatus;

    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/google/android/location/localizer/CellLocator;->clock:Lcom/google/android/location/os/Clock;

    invoke-interface {v7}, Lcom/google/android/location/os/Clock;->millisSinceBoot()J

    move-result-wide v7

    move-object/from16 v9, p1

    invoke-direct/range {v4 .. v10}, Lcom/google/android/location/data/CellLocatorResult;-><init>(Lcom/google/android/location/data/Position;Lcom/google/android/location/data/LocatorResult$ResultStatus;JLcom/google/android/location/data/CellStatus;Ljava/util/Map;)V

    goto/16 :goto_0

    :cond_7
    const-string v4, "CellLocator"

    const-string v6, "Cell location had non-sane values"

    invoke-static {v4, v6}, Lcom/google/android/location/utils/logging/Logger;->w(Ljava/lang/String;Ljava/lang/String;)V

    new-instance v11, Lcom/google/android/location/data/CellLocatorResult;

    const/4 v12, 0x0

    sget-object v13, Lcom/google/android/location/data/LocatorResult$ResultStatus;->NO_LOCATION:Lcom/google/android/location/data/LocatorResult$ResultStatus;

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/location/localizer/CellLocator;->clock:Lcom/google/android/location/os/Clock;

    invoke-interface {v4}, Lcom/google/android/location/os/Clock;->millisSinceBoot()J

    move-result-wide v14

    move-object/from16 v16, p1

    move-object/from16 v17, v10

    invoke-direct/range {v11 .. v17}, Lcom/google/android/location/data/CellLocatorResult;-><init>(Lcom/google/android/location/data/Position;Lcom/google/android/location/data/LocatorResult$ResultStatus;JLcom/google/android/location/data/CellStatus;Ljava/util/Map;)V

    move-object v4, v11

    goto/16 :goto_0
.end method
