.class public Lcom/google/android/location/localizer/LocatorManager;
.super Ljava/lang/Object;
.source "LocatorManager.java"


# instance fields
.field private final cellLocator:Lcom/google/android/location/localizer/CellLocator;

.field private final wifiLocator:Lcom/google/android/location/localizer/WifiLocator;


# direct methods
.method public constructor <init>(Lcom/google/android/location/cache/TemporalCache;Lcom/google/android/location/cache/TemporalCache;Lcom/google/android/location/localizer/ModelStateManager;Lcom/google/android/location/os/Os;)V
    .locals 7
    .param p3    # Lcom/google/android/location/localizer/ModelStateManager;
    .param p4    # Lcom/google/android/location/os/Os;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/location/cache/TemporalCache",
            "<",
            "Ljava/lang/String;",
            "Lcom/google/android/location/data/Position;",
            ">;",
            "Lcom/google/android/location/cache/TemporalCache",
            "<",
            "Ljava/lang/Long;",
            "Lcom/google/android/location/data/WifiApPosition;",
            ">;",
            "Lcom/google/android/location/localizer/ModelStateManager;",
            "Lcom/google/android/location/os/Os;",
            ")V"
        }
    .end annotation

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    invoke-interface {p4}, Lcom/google/android/location/os/Os;->openMetricModelRawResource()Ljava/io/InputStream;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/location/localizer/MetricModel;->fromStream(Ljava/io/InputStream;)Lcom/google/android/location/localizer/MetricModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/location/localizer/MetricModel;->getSignalDistribution()Ljava/util/List;

    move-result-object v6

    move-object v5, p4

    new-instance v2, Lcom/google/android/location/localizer/MaxLreLocalizer;

    invoke-direct {v2, v6}, Lcom/google/android/location/localizer/MaxLreLocalizer;-><init>(Ljava/util/List;)V

    new-instance v0, Lcom/google/android/location/localizer/WifiLocator;

    new-instance v1, Lcom/google/android/location/localizer/WifiLocationEstimator;

    invoke-direct {v1}, Lcom/google/android/location/localizer/WifiLocationEstimator;-><init>()V

    new-instance v3, Lcom/google/android/location/localizer/ModelLocalizerV2;

    invoke-direct {v3, p4, p3}, Lcom/google/android/location/localizer/ModelLocalizerV2;-><init>(Lcom/google/android/location/os/Os;Lcom/google/android/location/localizer/ModelStateManager;)V

    move-object v4, p2

    invoke-direct/range {v0 .. v5}, Lcom/google/android/location/localizer/WifiLocator;-><init>(Lcom/google/android/location/localizer/WifiLocationEstimator;Lcom/google/android/location/localizer/MaxLreLocalizer;Lcom/google/android/location/localizer/ModelLocalizerV2;Lcom/google/android/location/cache/TemporalCache;Lcom/google/android/location/os/Clock;)V

    iput-object v0, p0, Lcom/google/android/location/localizer/LocatorManager;->wifiLocator:Lcom/google/android/location/localizer/WifiLocator;

    new-instance v0, Lcom/google/android/location/localizer/CellLocator;

    invoke-direct {v0, p1, v5}, Lcom/google/android/location/localizer/CellLocator;-><init>(Lcom/google/android/location/cache/TemporalCache;Lcom/google/android/location/os/Clock;)V

    iput-object v0, p0, Lcom/google/android/location/localizer/LocatorManager;->cellLocator:Lcom/google/android/location/localizer/CellLocator;

    return-void
.end method

.method private doWifiLookup(Lcom/google/android/location/data/WifiScan;Lcom/google/android/location/data/WifiScan;Lcom/google/android/location/data/Position;J)Lcom/google/android/location/data/WifiLocatorResult;
    .locals 3
    .param p1    # Lcom/google/android/location/data/WifiScan;
    .param p2    # Lcom/google/android/location/data/WifiScan;
    .param p3    # Lcom/google/android/location/data/Position;
    .param p4    # J

    iget-object v1, p0, Lcom/google/android/location/localizer/LocatorManager;->wifiLocator:Lcom/google/android/location/localizer/WifiLocator;

    invoke-static {p1}, Ljava/util/Collections;->singletonList(Ljava/lang/Object;)Ljava/util/List;

    move-result-object v2

    invoke-virtual {v1, v2, p3, p4, p5}, Lcom/google/android/location/localizer/WifiLocator;->computeLocation(Ljava/util/List;Lcom/google/android/location/data/Position;J)Lcom/google/android/location/data/WifiLocatorResult;

    move-result-object v0

    iget-object v1, v0, Lcom/google/android/location/data/WifiLocatorResult;->status:Lcom/google/android/location/data/LocatorResult$ResultStatus;

    sget-object v2, Lcom/google/android/location/data/LocatorResult$ResultStatus;->OK:Lcom/google/android/location/data/LocatorResult$ResultStatus;

    if-eq v1, v2, :cond_0

    if-eq p1, p2, :cond_0

    if-eqz p2, :cond_0

    iget-object v1, p0, Lcom/google/android/location/localizer/LocatorManager;->wifiLocator:Lcom/google/android/location/localizer/WifiLocator;

    invoke-static {p2}, Ljava/util/Collections;->singletonList(Ljava/lang/Object;)Ljava/util/List;

    move-result-object v2

    invoke-virtual {v1, v2, p3, p4, p5}, Lcom/google/android/location/localizer/WifiLocator;->computeLocation(Ljava/util/List;Lcom/google/android/location/data/Position;J)Lcom/google/android/location/data/WifiLocatorResult;

    move-result-object v0

    :cond_0
    return-object v0
.end method

.method private selectBestResult(Lcom/google/android/location/data/LocatorResult;Lcom/google/android/location/data/LocatorResult;)Lcom/google/android/location/data/LocatorResult;
    .locals 12
    .param p1    # Lcom/google/android/location/data/LocatorResult;
    .param p2    # Lcom/google/android/location/data/LocatorResult;

    const/4 v8, 0x1

    const/4 v9, 0x0

    iget-object v10, p1, Lcom/google/android/location/data/LocatorResult;->status:Lcom/google/android/location/data/LocatorResult$ResultStatus;

    sget-object v11, Lcom/google/android/location/data/LocatorResult$ResultStatus;->OK:Lcom/google/android/location/data/LocatorResult$ResultStatus;

    if-ne v10, v11, :cond_1

    move v4, v8

    :goto_0
    iget-object v10, p2, Lcom/google/android/location/data/LocatorResult;->status:Lcom/google/android/location/data/LocatorResult$ResultStatus;

    sget-object v11, Lcom/google/android/location/data/LocatorResult$ResultStatus;->OK:Lcom/google/android/location/data/LocatorResult$ResultStatus;

    if-ne v10, v11, :cond_2

    move v3, v8

    :goto_1
    if-nez v4, :cond_3

    if-nez v3, :cond_3

    const/4 p2, 0x0

    :cond_0
    :goto_2
    return-object p2

    :cond_1
    move v4, v9

    goto :goto_0

    :cond_2
    move v3, v9

    goto :goto_1

    :cond_3
    if-eqz v4, :cond_0

    if-nez v3, :cond_4

    move-object p2, p1

    goto :goto_2

    :cond_4
    iget-object v7, p1, Lcom/google/android/location/data/LocatorResult;->position:Lcom/google/android/location/data/Position;

    iget-object v0, p2, Lcom/google/android/location/data/LocatorResult;->position:Lcom/google/android/location/data/Position;

    invoke-static {v7, v0}, Lcom/google/android/location/localizer/LocalizerUtil;->computeDistance(Lcom/google/android/location/data/Position;Lcom/google/android/location/data/Position;)I

    move-result v2

    iget v10, v7, Lcom/google/android/location/data/Position;->accuracyMm:I

    iget v11, v0, Lcom/google/android/location/data/Position;->accuracyMm:I

    add-int/2addr v10, v11

    const v11, 0x3567e0

    invoke-static {v10, v11}, Ljava/lang/Math;->max(II)I

    move-result v10

    div-int/lit16 v5, v10, 0x3e8

    if-gt v2, v5, :cond_5

    move v1, v8

    :goto_3
    if-eqz v1, :cond_7

    iget v10, v7, Lcom/google/android/location/data/Position;->accuracyMm:I

    iget v11, v0, Lcom/google/android/location/data/Position;->accuracyMm:I

    if-le v10, v11, :cond_6

    move v6, v8

    :goto_4
    if-nez v6, :cond_0

    move-object p2, p1

    goto :goto_2

    :cond_5
    move v1, v9

    goto :goto_3

    :cond_6
    move v6, v9

    goto :goto_4

    :cond_7
    iget v10, v7, Lcom/google/android/location/data/Position;->confidence:I

    iget v11, v0, Lcom/google/android/location/data/Position;->confidence:I

    if-ge v10, v11, :cond_8

    move v6, v8

    :goto_5
    goto :goto_4

    :cond_8
    move v6, v9

    goto :goto_5
.end method


# virtual methods
.method public computeLocation(Lcom/google/android/location/data/CellStatus;Lcom/google/android/location/data/WifiScan;Lcom/google/android/location/data/GlsLocatorResult;Lcom/google/android/location/data/Position;J)Lcom/google/android/location/data/NetworkLocation;
    .locals 10
    .param p1    # Lcom/google/android/location/data/CellStatus;
    .param p2    # Lcom/google/android/location/data/WifiScan;
    .param p3    # Lcom/google/android/location/data/GlsLocatorResult;
    .param p4    # Lcom/google/android/location/data/Position;
    .param p5    # J

    iget-object v0, p0, Lcom/google/android/location/localizer/LocatorManager;->cellLocator:Lcom/google/android/location/localizer/CellLocator;

    invoke-virtual {v0, p1}, Lcom/google/android/location/localizer/CellLocator;->computeLocation(Lcom/google/android/location/data/CellStatus;)Lcom/google/android/location/data/CellLocatorResult;

    move-result-object v8

    if-nez p4, :cond_0

    if-eqz v8, :cond_0

    iget-object v0, v8, Lcom/google/android/location/data/CellLocatorResult;->status:Lcom/google/android/location/data/LocatorResult$ResultStatus;

    sget-object v1, Lcom/google/android/location/data/LocatorResult$ResultStatus;->OK:Lcom/google/android/location/data/LocatorResult$ResultStatus;

    if-ne v0, v1, :cond_0

    iget-object v7, v8, Lcom/google/android/location/data/CellLocatorResult;->position:Lcom/google/android/location/data/Position;

    new-instance p4, Lcom/google/android/location/data/Position;

    iget v0, v7, Lcom/google/android/location/data/Position;->latE7:I

    iget v1, v7, Lcom/google/android/location/data/Position;->lngE7:I

    iget v3, v7, Lcom/google/android/location/data/Position;->accuracyMm:I

    mul-int/lit8 v3, v3, 0x4

    invoke-direct {p4, v0, v1, v3}, Lcom/google/android/location/data/Position;-><init>(III)V

    const-string v0, "LocatorManager"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Generating cell-based aperture: "

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/location/utils/logging/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    :cond_0
    if-nez p3, :cond_2

    const/4 v2, 0x0

    :goto_0
    move-object v0, p0

    move-object v1, p2

    move-object v3, p4

    move-wide v4, p5

    invoke-direct/range {v0 .. v5}, Lcom/google/android/location/localizer/LocatorManager;->doWifiLookup(Lcom/google/android/location/data/WifiScan;Lcom/google/android/location/data/WifiScan;Lcom/google/android/location/data/Position;J)Lcom/google/android/location/data/WifiLocatorResult;

    move-result-object v9

    invoke-direct {p0, v9, v8}, Lcom/google/android/location/localizer/LocatorManager;->selectBestResult(Lcom/google/android/location/data/LocatorResult;Lcom/google/android/location/data/LocatorResult;)Lcom/google/android/location/data/LocatorResult;

    move-result-object v6

    if-nez v6, :cond_1

    if-eqz p3, :cond_1

    iget-object v0, p3, Lcom/google/android/location/data/GlsLocatorResult;->status:Lcom/google/android/location/data/LocatorResult$ResultStatus;

    sget-object v1, Lcom/google/android/location/data/LocatorResult$ResultStatus;->OK:Lcom/google/android/location/data/LocatorResult$ResultStatus;

    if-ne v0, v1, :cond_1

    move-object v6, p3

    :cond_1
    new-instance v0, Lcom/google/android/location/data/NetworkLocation;

    invoke-direct {v0, v6, v9, v8, p3}, Lcom/google/android/location/data/NetworkLocation;-><init>(Lcom/google/android/location/data/LocatorResult;Lcom/google/android/location/data/WifiLocatorResult;Lcom/google/android/location/data/CellLocatorResult;Lcom/google/android/location/data/GlsLocatorResult;)V

    return-object v0

    :cond_2
    iget-object v2, p3, Lcom/google/android/location/data/GlsLocatorResult;->wifiScan:Lcom/google/android/location/data/WifiScan;

    goto :goto_0
.end method
