.class public Lcom/google/android/location/activity/CellTravelDetector;
.super Ljava/lang/Object;
.source "CellTravelDetector.java"


# instance fields
.field private final HISTORY_SIZE:I

.field private final MIN_HISTORY_SIZE:I

.field private final cellHistory:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/android/location/data/CellLocatorResult;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 2

    const/4 v1, 0x7

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput v1, p0, Lcom/google/android/location/activity/CellTravelDetector;->HISTORY_SIZE:I

    const/4 v0, 0x4

    iput v0, p0, Lcom/google/android/location/activity/CellTravelDetector;->MIN_HISTORY_SIZE:I

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(I)V

    iput-object v0, p0, Lcom/google/android/location/activity/CellTravelDetector;->cellHistory:Ljava/util/List;

    return-void
.end method

.method private cellCirclesOverlapsLocation(Lcom/google/android/location/data/CellLocatorResult;Lcom/google/android/location/data/CellLocatorResult;)Z
    .locals 4
    .param p1    # Lcom/google/android/location/data/CellLocatorResult;
    .param p2    # Lcom/google/android/location/data/CellLocatorResult;

    iget-object v2, p1, Lcom/google/android/location/data/CellLocatorResult;->position:Lcom/google/android/location/data/Position;

    iget-object v3, p2, Lcom/google/android/location/data/CellLocatorResult;->position:Lcom/google/android/location/data/Position;

    invoke-static {v2, v3}, Lcom/google/android/location/localizer/LocalizerUtil;->computeDistance(Lcom/google/android/location/data/Position;Lcom/google/android/location/data/Position;)I

    move-result v1

    iget-object v2, p1, Lcom/google/android/location/data/CellLocatorResult;->position:Lcom/google/android/location/data/Position;

    iget v2, v2, Lcom/google/android/location/data/Position;->accuracyMm:I

    div-int/lit16 v2, v2, 0x3e8

    const/16 v3, 0xc8

    invoke-static {v2, v3}, Ljava/lang/Math;->max(II)I

    move-result v0

    if-gt v1, v0, :cond_0

    const/4 v2, 0x1

    :goto_0
    return v2

    :cond_0
    const/4 v2, 0x0

    goto :goto_0
.end method

.method private maybeUpdateHistory(Lcom/google/android/location/data/CellLocatorResult;)V
    .locals 3
    .param p1    # Lcom/google/android/location/data/CellLocatorResult;

    iget-object v0, p0, Lcom/google/android/location/activity/CellTravelDetector;->cellHistory:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/location/activity/CellTravelDetector;->cellHistory:Ljava/util/List;

    invoke-direct {p0, p1, v0}, Lcom/google/android/location/activity/CellTravelDetector;->signalIsSignificantlyNewer(Lcom/google/android/location/data/CellLocatorResult;Ljava/util/List;)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const-string v0, "TravelC"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Cell history("

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/location/activity/CellTravelDetector;->cellHistory:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ") updated with: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/location/utils/logging/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/google/android/location/activity/CellTravelDetector;->cellHistory:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    iget-object v0, p0, Lcom/google/android/location/activity/CellTravelDetector;->cellHistory:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    const/4 v1, 0x7

    if-le v0, v1, :cond_1

    iget-object v0, p0, Lcom/google/android/location/activity/CellTravelDetector;->cellHistory:Ljava/util/List;

    const/4 v1, 0x0

    invoke-interface {v0, v1}, Ljava/util/List;->remove(I)Ljava/lang/Object;

    :cond_1
    return-void
.end method

.method private signalIsSignificantlyNewer(Lcom/google/android/location/data/CellLocatorResult;Ljava/util/List;)Z
    .locals 8
    .param p1    # Lcom/google/android/location/data/CellLocatorResult;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/location/data/CellLocatorResult;",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/location/data/CellLocatorResult;",
            ">;)Z"
        }
    .end annotation

    iget-wide v0, p1, Lcom/google/android/location/data/CellLocatorResult;->reportTime:J

    invoke-interface {p2}, Ljava/util/List;->size()I

    move-result v4

    add-int/lit8 v4, v4, -0x1

    invoke-interface {p2, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/google/android/location/data/CellLocatorResult;

    iget-wide v2, v4, Lcom/google/android/location/data/CellLocatorResult;->reportTime:J

    sub-long v4, v0, v2

    const-wide/32 v6, 0xd6d8

    cmp-long v4, v4, v6

    if-ltz v4, :cond_0

    const/4 v4, 0x1

    :goto_0
    return v4

    :cond_0
    const/4 v4, 0x0

    goto :goto_0
.end method


# virtual methods
.method public detectTravelType(Lcom/google/android/location/data/CellLocatorResult;)Lcom/google/android/location/activity/TravelDetectionManager$TravelDetectionResult;
    .locals 8
    .param p1    # Lcom/google/android/location/data/CellLocatorResult;

    const-wide/high16 v6, 0x3fe0000000000000L

    if-eqz p1, :cond_0

    iget-object v4, p1, Lcom/google/android/location/data/CellLocatorResult;->position:Lcom/google/android/location/data/Position;

    if-nez v4, :cond_1

    :cond_0
    sget-object v3, Lcom/google/android/location/activity/TravelDetectionManager;->UNKNOWN_RESULT:Lcom/google/android/location/activity/TravelDetectionManager$TravelDetectionResult;

    :goto_0
    const-string v4, "TravelC"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "Travel detection result CellOnly: "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/google/android/location/utils/logging/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    return-object v3

    :cond_1
    iget-object v4, p0, Lcom/google/android/location/activity/CellTravelDetector;->cellHistory:Ljava/util/List;

    invoke-interface {v4}, Ljava/util/List;->size()I

    move-result v4

    const/4 v5, 0x4

    if-ge v4, v5, :cond_2

    invoke-direct {p0, p1}, Lcom/google/android/location/activity/CellTravelDetector;->maybeUpdateHistory(Lcom/google/android/location/data/CellLocatorResult;)V

    sget-object v3, Lcom/google/android/location/activity/TravelDetectionManager;->UNKNOWN_RESULT:Lcom/google/android/location/activity/TravelDetectionManager$TravelDetectionResult;

    goto :goto_0

    :cond_2
    const/4 v2, 0x0

    iget-object v4, p0, Lcom/google/android/location/activity/CellTravelDetector;->cellHistory:Ljava/util/List;

    invoke-interface {v4}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_3
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_4

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/location/data/CellLocatorResult;

    invoke-direct {p0, p1, v0}, Lcom/google/android/location/activity/CellTravelDetector;->cellCirclesOverlapsLocation(Lcom/google/android/location/data/CellLocatorResult;Lcom/google/android/location/data/CellLocatorResult;)Z

    move-result v4

    if-nez v4, :cond_3

    const/4 v2, 0x1

    :cond_4
    invoke-direct {p0, p1}, Lcom/google/android/location/activity/CellTravelDetector;->maybeUpdateHistory(Lcom/google/android/location/data/CellLocatorResult;)V

    if-eqz v2, :cond_5

    new-instance v3, Lcom/google/android/location/activity/TravelDetectionManager$TravelDetectionResult;

    sget-object v4, Lcom/google/android/location/data/TravelDetectionType;->MOVING:Lcom/google/android/location/data/TravelDetectionType;

    invoke-direct {v3, v4, v6, v7}, Lcom/google/android/location/activity/TravelDetectionManager$TravelDetectionResult;-><init>(Lcom/google/android/location/data/TravelDetectionType;D)V

    goto :goto_0

    :cond_5
    new-instance v3, Lcom/google/android/location/activity/TravelDetectionManager$TravelDetectionResult;

    sget-object v4, Lcom/google/android/location/data/TravelDetectionType;->STATIONARY:Lcom/google/android/location/data/TravelDetectionType;

    invoke-direct {v3, v4, v6, v7}, Lcom/google/android/location/activity/TravelDetectionManager$TravelDetectionResult;-><init>(Lcom/google/android/location/data/TravelDetectionType;D)V

    goto :goto_0
.end method
