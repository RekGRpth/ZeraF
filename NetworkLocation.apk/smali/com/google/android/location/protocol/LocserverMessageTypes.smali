.class public Lcom/google/android/location/protocol/LocserverMessageTypes;
.super Ljava/lang/Object;
.source "LocserverMessageTypes.java"


# static fields
.field public static final GACCELEROMETER_SNAPSHOT:Lcom/google/gmm/common/io/protocol/ProtoBufType;

.field public static final GADDRESS:Lcom/google/gmm/common/io/protocol/ProtoBufType;

.field public static final GADDRESS_COMPONENT:Lcom/google/gmm/common/io/protocol/ProtoBufType;

.field public static final GAPP_PROFILE:Lcom/google/gmm/common/io/protocol/ProtoBufType;

.field public static final GBAROMETER_SNAPSHOT:Lcom/google/gmm/common/io/protocol/ProtoBufType;

.field public static final GCELL:Lcom/google/gmm/common/io/protocol/ProtoBufType;

.field public static final GCELLULAR_PLATFORM_PROFILE:Lcom/google/gmm/common/io/protocol/ProtoBufType;

.field public static final GCELLULAR_PROFILE:Lcom/google/gmm/common/io/protocol/ProtoBufType;

.field public static final GCLIENT_CACHE_STATS:Lcom/google/gmm/common/io/protocol/ProtoBufType;

.field public static final GCLIENT_COLLECTOR_STATS:Lcom/google/gmm/common/io/protocol/ProtoBufType;

.field public static final GCLIENT_POLICY_STATS:Lcom/google/gmm/common/io/protocol/ProtoBufType;

.field public static final GCLIENT_SENSOR_COLLECTION_STATS:Lcom/google/gmm/common/io/protocol/ProtoBufType;

.field public static final GCLIENT_SERVER_QUERY_STATS:Lcom/google/gmm/common/io/protocol/ProtoBufType;

.field public static final GCLIENT_STATS:Lcom/google/gmm/common/io/protocol/ProtoBufType;

.field public static final GCLIENT_TOKEN_BUCKET_STATS:Lcom/google/gmm/common/io/protocol/ProtoBufType;

.field public static final GDEBUG_PROFILE:Lcom/google/gmm/common/io/protocol/ProtoBufType;

.field public static final GDEVICE_LOCATION:Lcom/google/gmm/common/io/protocol/ProtoBufType;

.field public static final GFEATURE:Lcom/google/gmm/common/io/protocol/ProtoBufType;

.field public static final GFEATURE_TYPE_RESTRICT:Lcom/google/gmm/common/io/protocol/ProtoBufType;

.field public static final GGATEWAY_DEVICE:Lcom/google/gmm/common/io/protocol/ProtoBufType;

.field public static final GGEOCODE_REQUEST:Lcom/google/gmm/common/io/protocol/ProtoBufType;

.field public static final GGPS_PROFILE:Lcom/google/gmm/common/io/protocol/ProtoBufType;

.field public static final GGPS_SATELLITE:Lcom/google/gmm/common/io/protocol/ProtoBufType;

.field public static final GGPS_STATUS_SNAPSHOT:Lcom/google/gmm/common/io/protocol/ProtoBufType;

.field public static final GGYROSCOPE_PLATFORM_PROFILE:Lcom/google/gmm/common/io/protocol/ProtoBufType;

.field public static final GGYROSCOPE_SNAPSHOT:Lcom/google/gmm/common/io/protocol/ProtoBufType;

.field public static final GINDOOR_LOCATION:Lcom/google/gmm/common/io/protocol/ProtoBufType;

.field public static final GLAT_LNG:Lcom/google/gmm/common/io/protocol/ProtoBufType;

.field public static final GLOCATION:Lcom/google/gmm/common/io/protocol/ProtoBufType;

.field public static final GLOC_REPLY:Lcom/google/gmm/common/io/protocol/ProtoBufType;

.field public static final GLOC_REPLY_ELEMENT:Lcom/google/gmm/common/io/protocol/ProtoBufType;

.field public static final GLOC_REQUEST:Lcom/google/gmm/common/io/protocol/ProtoBufType;

.field public static final GLOC_REQUEST_ELEMENT:Lcom/google/gmm/common/io/protocol/ProtoBufType;

.field public static final GMAGNETIC_FIELD_SNAPSHOT:Lcom/google/gmm/common/io/protocol/ProtoBufType;

.field public static final GORIENTATION_SNAPSHOT:Lcom/google/gmm/common/io/protocol/ProtoBufType;

.field public static final GPLATFORM_PROFILE:Lcom/google/gmm/common/io/protocol/ProtoBufType;

.field public static final GPREFETCH_MODE:Lcom/google/gmm/common/io/protocol/ProtoBufType;

.field public static final GRECTANGLE:Lcom/google/gmm/common/io/protocol/ProtoBufType;

.field public static final GSENSORS_PLATFORM_PROFILE:Lcom/google/gmm/common/io/protocol/ProtoBufType;

.field public static final GSENSORS_PROFILE:Lcom/google/gmm/common/io/protocol/ProtoBufType;

.field public static final GSESSION_PROFILE:Lcom/google/gmm/common/io/protocol/ProtoBufType;

.field public static final GSIGNAL_FINGERPRINT_MODEL:Lcom/google/gmm/common/io/protocol/ProtoBufType;

.field public static final GSIGNAL_FINGERPRINT_MODEL_CLUSTER:Lcom/google/gmm/common/io/protocol/ProtoBufType;

.field public static final GSIGNAL_FINGERPRINT_MODEL_REPLY:Lcom/google/gmm/common/io/protocol/ProtoBufType;

.field public static final GSIGNAL_FINGERPRINT_MODEL_REQUEST:Lcom/google/gmm/common/io/protocol/ProtoBufType;

.field public static final GUSER_PROFILE:Lcom/google/gmm/common/io/protocol/ProtoBufType;

.field public static final GWIFI_CLUSTER_MEMBERSHIP:Lcom/google/gmm/common/io/protocol/ProtoBufType;

.field public static final GWIFI_DEVICE:Lcom/google/gmm/common/io/protocol/ProtoBufType;

.field public static final GWIFI_PLATFORM_PROFILE:Lcom/google/gmm/common/io/protocol/ProtoBufType;

.field public static final GWIFI_PROFILE:Lcom/google/gmm/common/io/protocol/ProtoBufType;

.field public static final NLP_COLLECTION_POLICY_STATE:Lcom/google/gmm/common/io/protocol/ProtoBufType;

.field public static final NLP_COLLECTOR_STATE:Lcom/google/gmm/common/io/protocol/ProtoBufType;

.field public static final NLP_ELEMENT_APP_DATA:Lcom/google/gmm/common/io/protocol/ProtoBufType;

.field public static final NLP_IN_OUTDOOR_CLASSIFIER_RESULT:Lcom/google/gmm/common/io/protocol/ProtoBufType;

.field public static final NLP_IN_OUTDOOR_HISTORY:Lcom/google/gmm/common/io/protocol/ProtoBufType;

.field public static final NLP_IN_OUTDOOR_TRANSITION_TIME:Lcom/google/gmm/common/io/protocol/ProtoBufType;

.field public static final NLP_PARAMETERS:Lcom/google/gmm/common/io/protocol/ProtoBufType;

.field public static final NLP_SEEN_DEVICES_CACHE:Lcom/google/gmm/common/io/protocol/ProtoBufType;

.field public static final NLP_SENSOR_COLLECTION_STATE:Lcom/google/gmm/common/io/protocol/ProtoBufType;

.field public static final NLP_TEMPORAL_LONG_TO_STRING_CACHE:Lcom/google/gmm/common/io/protocol/ProtoBufType;

.field public static final NLP_TEMPORAL_LONG_TO_STRING_ENTRY:Lcom/google/gmm/common/io/protocol/ProtoBufType;

.field public static final NLP_TEMPORAL_STRING_TO_STRING_CACHE:Lcom/google/gmm/common/io/protocol/ProtoBufType;

.field public static final NLP_TEMPORAL_STRING_TO_STRING_ENTRY:Lcom/google/gmm/common/io/protocol/ProtoBufType;

.field public static final NLP_TIME_SPAN:Lcom/google/gmm/common/io/protocol/ProtoBufType;

.field public static final NLP_TIME_ZONE:Lcom/google/gmm/common/io/protocol/ProtoBufType;

.field public static final NLP_TOKEN_BUCKET_STATE:Lcom/google/gmm/common/io/protocol/ProtoBufType;

.field public static final NLP_WIFI_AP_PROFILE:Lcom/google/gmm/common/io/protocol/ProtoBufType;

.field public static final RESPONSE_CODES:Lcom/google/gmm/common/io/protocol/ProtoBufType;


# direct methods
.method static constructor <clinit>()V
    .locals 11

    const/16 v10, 0x21b

    const/4 v9, 0x2

    const/4 v8, 0x1

    const/16 v7, 0x215

    const/4 v6, 0x0

    new-instance v0, Lcom/google/gmm/common/io/protocol/ProtoBufType;

    invoke-direct {v0}, Lcom/google/gmm/common/io/protocol/ProtoBufType;-><init>()V

    sput-object v0, Lcom/google/android/location/protocol/LocserverMessageTypes;->GCLIENT_POLICY_STATS:Lcom/google/gmm/common/io/protocol/ProtoBufType;

    new-instance v0, Lcom/google/gmm/common/io/protocol/ProtoBufType;

    invoke-direct {v0}, Lcom/google/gmm/common/io/protocol/ProtoBufType;-><init>()V

    sput-object v0, Lcom/google/android/location/protocol/LocserverMessageTypes;->GFEATURE_TYPE_RESTRICT:Lcom/google/gmm/common/io/protocol/ProtoBufType;

    new-instance v0, Lcom/google/gmm/common/io/protocol/ProtoBufType;

    invoke-direct {v0}, Lcom/google/gmm/common/io/protocol/ProtoBufType;-><init>()V

    sput-object v0, Lcom/google/android/location/protocol/LocserverMessageTypes;->GLOC_REQUEST_ELEMENT:Lcom/google/gmm/common/io/protocol/ProtoBufType;

    new-instance v0, Lcom/google/gmm/common/io/protocol/ProtoBufType;

    invoke-direct {v0}, Lcom/google/gmm/common/io/protocol/ProtoBufType;-><init>()V

    sput-object v0, Lcom/google/android/location/protocol/LocserverMessageTypes;->GADDRESS:Lcom/google/gmm/common/io/protocol/ProtoBufType;

    new-instance v0, Lcom/google/gmm/common/io/protocol/ProtoBufType;

    invoke-direct {v0}, Lcom/google/gmm/common/io/protocol/ProtoBufType;-><init>()V

    sput-object v0, Lcom/google/android/location/protocol/LocserverMessageTypes;->NLP_COLLECTION_POLICY_STATE:Lcom/google/gmm/common/io/protocol/ProtoBufType;

    new-instance v0, Lcom/google/gmm/common/io/protocol/ProtoBufType;

    invoke-direct {v0}, Lcom/google/gmm/common/io/protocol/ProtoBufType;-><init>()V

    sput-object v0, Lcom/google/android/location/protocol/LocserverMessageTypes;->GSIGNAL_FINGERPRINT_MODEL:Lcom/google/gmm/common/io/protocol/ProtoBufType;

    new-instance v0, Lcom/google/gmm/common/io/protocol/ProtoBufType;

    invoke-direct {v0}, Lcom/google/gmm/common/io/protocol/ProtoBufType;-><init>()V

    sput-object v0, Lcom/google/android/location/protocol/LocserverMessageTypes;->GWIFI_CLUSTER_MEMBERSHIP:Lcom/google/gmm/common/io/protocol/ProtoBufType;

    new-instance v0, Lcom/google/gmm/common/io/protocol/ProtoBufType;

    invoke-direct {v0}, Lcom/google/gmm/common/io/protocol/ProtoBufType;-><init>()V

    sput-object v0, Lcom/google/android/location/protocol/LocserverMessageTypes;->GGPS_SATELLITE:Lcom/google/gmm/common/io/protocol/ProtoBufType;

    new-instance v0, Lcom/google/gmm/common/io/protocol/ProtoBufType;

    invoke-direct {v0}, Lcom/google/gmm/common/io/protocol/ProtoBufType;-><init>()V

    sput-object v0, Lcom/google/android/location/protocol/LocserverMessageTypes;->GGEOCODE_REQUEST:Lcom/google/gmm/common/io/protocol/ProtoBufType;

    new-instance v0, Lcom/google/gmm/common/io/protocol/ProtoBufType;

    invoke-direct {v0}, Lcom/google/gmm/common/io/protocol/ProtoBufType;-><init>()V

    sput-object v0, Lcom/google/android/location/protocol/LocserverMessageTypes;->GSENSORS_PROFILE:Lcom/google/gmm/common/io/protocol/ProtoBufType;

    new-instance v0, Lcom/google/gmm/common/io/protocol/ProtoBufType;

    invoke-direct {v0}, Lcom/google/gmm/common/io/protocol/ProtoBufType;-><init>()V

    sput-object v0, Lcom/google/android/location/protocol/LocserverMessageTypes;->NLP_SENSOR_COLLECTION_STATE:Lcom/google/gmm/common/io/protocol/ProtoBufType;

    new-instance v0, Lcom/google/gmm/common/io/protocol/ProtoBufType;

    invoke-direct {v0}, Lcom/google/gmm/common/io/protocol/ProtoBufType;-><init>()V

    sput-object v0, Lcom/google/android/location/protocol/LocserverMessageTypes;->GSENSORS_PLATFORM_PROFILE:Lcom/google/gmm/common/io/protocol/ProtoBufType;

    new-instance v0, Lcom/google/gmm/common/io/protocol/ProtoBufType;

    invoke-direct {v0}, Lcom/google/gmm/common/io/protocol/ProtoBufType;-><init>()V

    sput-object v0, Lcom/google/android/location/protocol/LocserverMessageTypes;->NLP_ELEMENT_APP_DATA:Lcom/google/gmm/common/io/protocol/ProtoBufType;

    new-instance v0, Lcom/google/gmm/common/io/protocol/ProtoBufType;

    invoke-direct {v0}, Lcom/google/gmm/common/io/protocol/ProtoBufType;-><init>()V

    sput-object v0, Lcom/google/android/location/protocol/LocserverMessageTypes;->GUSER_PROFILE:Lcom/google/gmm/common/io/protocol/ProtoBufType;

    new-instance v0, Lcom/google/gmm/common/io/protocol/ProtoBufType;

    invoke-direct {v0}, Lcom/google/gmm/common/io/protocol/ProtoBufType;-><init>()V

    sput-object v0, Lcom/google/android/location/protocol/LocserverMessageTypes;->GCLIENT_STATS:Lcom/google/gmm/common/io/protocol/ProtoBufType;

    new-instance v0, Lcom/google/gmm/common/io/protocol/ProtoBufType;

    invoke-direct {v0}, Lcom/google/gmm/common/io/protocol/ProtoBufType;-><init>()V

    sput-object v0, Lcom/google/android/location/protocol/LocserverMessageTypes;->NLP_TOKEN_BUCKET_STATE:Lcom/google/gmm/common/io/protocol/ProtoBufType;

    new-instance v0, Lcom/google/gmm/common/io/protocol/ProtoBufType;

    invoke-direct {v0}, Lcom/google/gmm/common/io/protocol/ProtoBufType;-><init>()V

    sput-object v0, Lcom/google/android/location/protocol/LocserverMessageTypes;->NLP_WIFI_AP_PROFILE:Lcom/google/gmm/common/io/protocol/ProtoBufType;

    new-instance v0, Lcom/google/gmm/common/io/protocol/ProtoBufType;

    invoke-direct {v0}, Lcom/google/gmm/common/io/protocol/ProtoBufType;-><init>()V

    sput-object v0, Lcom/google/android/location/protocol/LocserverMessageTypes;->RESPONSE_CODES:Lcom/google/gmm/common/io/protocol/ProtoBufType;

    new-instance v0, Lcom/google/gmm/common/io/protocol/ProtoBufType;

    invoke-direct {v0}, Lcom/google/gmm/common/io/protocol/ProtoBufType;-><init>()V

    sput-object v0, Lcom/google/android/location/protocol/LocserverMessageTypes;->NLP_COLLECTOR_STATE:Lcom/google/gmm/common/io/protocol/ProtoBufType;

    new-instance v0, Lcom/google/gmm/common/io/protocol/ProtoBufType;

    invoke-direct {v0}, Lcom/google/gmm/common/io/protocol/ProtoBufType;-><init>()V

    sput-object v0, Lcom/google/android/location/protocol/LocserverMessageTypes;->GSIGNAL_FINGERPRINT_MODEL_CLUSTER:Lcom/google/gmm/common/io/protocol/ProtoBufType;

    new-instance v0, Lcom/google/gmm/common/io/protocol/ProtoBufType;

    invoke-direct {v0}, Lcom/google/gmm/common/io/protocol/ProtoBufType;-><init>()V

    sput-object v0, Lcom/google/android/location/protocol/LocserverMessageTypes;->GSIGNAL_FINGERPRINT_MODEL_REQUEST:Lcom/google/gmm/common/io/protocol/ProtoBufType;

    new-instance v0, Lcom/google/gmm/common/io/protocol/ProtoBufType;

    invoke-direct {v0}, Lcom/google/gmm/common/io/protocol/ProtoBufType;-><init>()V

    sput-object v0, Lcom/google/android/location/protocol/LocserverMessageTypes;->GLAT_LNG:Lcom/google/gmm/common/io/protocol/ProtoBufType;

    new-instance v0, Lcom/google/gmm/common/io/protocol/ProtoBufType;

    invoke-direct {v0}, Lcom/google/gmm/common/io/protocol/ProtoBufType;-><init>()V

    sput-object v0, Lcom/google/android/location/protocol/LocserverMessageTypes;->GADDRESS_COMPONENT:Lcom/google/gmm/common/io/protocol/ProtoBufType;

    new-instance v0, Lcom/google/gmm/common/io/protocol/ProtoBufType;

    invoke-direct {v0}, Lcom/google/gmm/common/io/protocol/ProtoBufType;-><init>()V

    sput-object v0, Lcom/google/android/location/protocol/LocserverMessageTypes;->GGPS_PROFILE:Lcom/google/gmm/common/io/protocol/ProtoBufType;

    new-instance v0, Lcom/google/gmm/common/io/protocol/ProtoBufType;

    invoke-direct {v0}, Lcom/google/gmm/common/io/protocol/ProtoBufType;-><init>()V

    sput-object v0, Lcom/google/android/location/protocol/LocserverMessageTypes;->GRECTANGLE:Lcom/google/gmm/common/io/protocol/ProtoBufType;

    new-instance v0, Lcom/google/gmm/common/io/protocol/ProtoBufType;

    invoke-direct {v0}, Lcom/google/gmm/common/io/protocol/ProtoBufType;-><init>()V

    sput-object v0, Lcom/google/android/location/protocol/LocserverMessageTypes;->GMAGNETIC_FIELD_SNAPSHOT:Lcom/google/gmm/common/io/protocol/ProtoBufType;

    new-instance v0, Lcom/google/gmm/common/io/protocol/ProtoBufType;

    invoke-direct {v0}, Lcom/google/gmm/common/io/protocol/ProtoBufType;-><init>()V

    sput-object v0, Lcom/google/android/location/protocol/LocserverMessageTypes;->NLP_TIME_ZONE:Lcom/google/gmm/common/io/protocol/ProtoBufType;

    new-instance v0, Lcom/google/gmm/common/io/protocol/ProtoBufType;

    invoke-direct {v0}, Lcom/google/gmm/common/io/protocol/ProtoBufType;-><init>()V

    sput-object v0, Lcom/google/android/location/protocol/LocserverMessageTypes;->GCLIENT_TOKEN_BUCKET_STATS:Lcom/google/gmm/common/io/protocol/ProtoBufType;

    new-instance v0, Lcom/google/gmm/common/io/protocol/ProtoBufType;

    invoke-direct {v0}, Lcom/google/gmm/common/io/protocol/ProtoBufType;-><init>()V

    sput-object v0, Lcom/google/android/location/protocol/LocserverMessageTypes;->GLOC_REPLY_ELEMENT:Lcom/google/gmm/common/io/protocol/ProtoBufType;

    new-instance v0, Lcom/google/gmm/common/io/protocol/ProtoBufType;

    invoke-direct {v0}, Lcom/google/gmm/common/io/protocol/ProtoBufType;-><init>()V

    sput-object v0, Lcom/google/android/location/protocol/LocserverMessageTypes;->GLOC_REPLY:Lcom/google/gmm/common/io/protocol/ProtoBufType;

    new-instance v0, Lcom/google/gmm/common/io/protocol/ProtoBufType;

    invoke-direct {v0}, Lcom/google/gmm/common/io/protocol/ProtoBufType;-><init>()V

    sput-object v0, Lcom/google/android/location/protocol/LocserverMessageTypes;->GWIFI_PLATFORM_PROFILE:Lcom/google/gmm/common/io/protocol/ProtoBufType;

    new-instance v0, Lcom/google/gmm/common/io/protocol/ProtoBufType;

    invoke-direct {v0}, Lcom/google/gmm/common/io/protocol/ProtoBufType;-><init>()V

    sput-object v0, Lcom/google/android/location/protocol/LocserverMessageTypes;->GWIFI_PROFILE:Lcom/google/gmm/common/io/protocol/ProtoBufType;

    new-instance v0, Lcom/google/gmm/common/io/protocol/ProtoBufType;

    invoke-direct {v0}, Lcom/google/gmm/common/io/protocol/ProtoBufType;-><init>()V

    sput-object v0, Lcom/google/android/location/protocol/LocserverMessageTypes;->GINDOOR_LOCATION:Lcom/google/gmm/common/io/protocol/ProtoBufType;

    new-instance v0, Lcom/google/gmm/common/io/protocol/ProtoBufType;

    invoke-direct {v0}, Lcom/google/gmm/common/io/protocol/ProtoBufType;-><init>()V

    sput-object v0, Lcom/google/android/location/protocol/LocserverMessageTypes;->NLP_SEEN_DEVICES_CACHE:Lcom/google/gmm/common/io/protocol/ProtoBufType;

    new-instance v0, Lcom/google/gmm/common/io/protocol/ProtoBufType;

    invoke-direct {v0}, Lcom/google/gmm/common/io/protocol/ProtoBufType;-><init>()V

    sput-object v0, Lcom/google/android/location/protocol/LocserverMessageTypes;->NLP_IN_OUTDOOR_CLASSIFIER_RESULT:Lcom/google/gmm/common/io/protocol/ProtoBufType;

    new-instance v0, Lcom/google/gmm/common/io/protocol/ProtoBufType;

    invoke-direct {v0}, Lcom/google/gmm/common/io/protocol/ProtoBufType;-><init>()V

    sput-object v0, Lcom/google/android/location/protocol/LocserverMessageTypes;->GCLIENT_CACHE_STATS:Lcom/google/gmm/common/io/protocol/ProtoBufType;

    new-instance v0, Lcom/google/gmm/common/io/protocol/ProtoBufType;

    invoke-direct {v0}, Lcom/google/gmm/common/io/protocol/ProtoBufType;-><init>()V

    sput-object v0, Lcom/google/android/location/protocol/LocserverMessageTypes;->GLOCATION:Lcom/google/gmm/common/io/protocol/ProtoBufType;

    new-instance v0, Lcom/google/gmm/common/io/protocol/ProtoBufType;

    invoke-direct {v0}, Lcom/google/gmm/common/io/protocol/ProtoBufType;-><init>()V

    sput-object v0, Lcom/google/android/location/protocol/LocserverMessageTypes;->NLP_PARAMETERS:Lcom/google/gmm/common/io/protocol/ProtoBufType;

    new-instance v0, Lcom/google/gmm/common/io/protocol/ProtoBufType;

    invoke-direct {v0}, Lcom/google/gmm/common/io/protocol/ProtoBufType;-><init>()V

    sput-object v0, Lcom/google/android/location/protocol/LocserverMessageTypes;->NLP_TEMPORAL_STRING_TO_STRING_CACHE:Lcom/google/gmm/common/io/protocol/ProtoBufType;

    new-instance v0, Lcom/google/gmm/common/io/protocol/ProtoBufType;

    invoke-direct {v0}, Lcom/google/gmm/common/io/protocol/ProtoBufType;-><init>()V

    sput-object v0, Lcom/google/android/location/protocol/LocserverMessageTypes;->GDEBUG_PROFILE:Lcom/google/gmm/common/io/protocol/ProtoBufType;

    new-instance v0, Lcom/google/gmm/common/io/protocol/ProtoBufType;

    invoke-direct {v0}, Lcom/google/gmm/common/io/protocol/ProtoBufType;-><init>()V

    sput-object v0, Lcom/google/android/location/protocol/LocserverMessageTypes;->GGATEWAY_DEVICE:Lcom/google/gmm/common/io/protocol/ProtoBufType;

    new-instance v0, Lcom/google/gmm/common/io/protocol/ProtoBufType;

    invoke-direct {v0}, Lcom/google/gmm/common/io/protocol/ProtoBufType;-><init>()V

    sput-object v0, Lcom/google/android/location/protocol/LocserverMessageTypes;->GPLATFORM_PROFILE:Lcom/google/gmm/common/io/protocol/ProtoBufType;

    new-instance v0, Lcom/google/gmm/common/io/protocol/ProtoBufType;

    invoke-direct {v0}, Lcom/google/gmm/common/io/protocol/ProtoBufType;-><init>()V

    sput-object v0, Lcom/google/android/location/protocol/LocserverMessageTypes;->GGYROSCOPE_PLATFORM_PROFILE:Lcom/google/gmm/common/io/protocol/ProtoBufType;

    new-instance v0, Lcom/google/gmm/common/io/protocol/ProtoBufType;

    invoke-direct {v0}, Lcom/google/gmm/common/io/protocol/ProtoBufType;-><init>()V

    sput-object v0, Lcom/google/android/location/protocol/LocserverMessageTypes;->GAPP_PROFILE:Lcom/google/gmm/common/io/protocol/ProtoBufType;

    new-instance v0, Lcom/google/gmm/common/io/protocol/ProtoBufType;

    invoke-direct {v0}, Lcom/google/gmm/common/io/protocol/ProtoBufType;-><init>()V

    sput-object v0, Lcom/google/android/location/protocol/LocserverMessageTypes;->GCLIENT_SENSOR_COLLECTION_STATS:Lcom/google/gmm/common/io/protocol/ProtoBufType;

    new-instance v0, Lcom/google/gmm/common/io/protocol/ProtoBufType;

    invoke-direct {v0}, Lcom/google/gmm/common/io/protocol/ProtoBufType;-><init>()V

    sput-object v0, Lcom/google/android/location/protocol/LocserverMessageTypes;->GGYROSCOPE_SNAPSHOT:Lcom/google/gmm/common/io/protocol/ProtoBufType;

    new-instance v0, Lcom/google/gmm/common/io/protocol/ProtoBufType;

    invoke-direct {v0}, Lcom/google/gmm/common/io/protocol/ProtoBufType;-><init>()V

    sput-object v0, Lcom/google/android/location/protocol/LocserverMessageTypes;->GSIGNAL_FINGERPRINT_MODEL_REPLY:Lcom/google/gmm/common/io/protocol/ProtoBufType;

    new-instance v0, Lcom/google/gmm/common/io/protocol/ProtoBufType;

    invoke-direct {v0}, Lcom/google/gmm/common/io/protocol/ProtoBufType;-><init>()V

    sput-object v0, Lcom/google/android/location/protocol/LocserverMessageTypes;->GWIFI_DEVICE:Lcom/google/gmm/common/io/protocol/ProtoBufType;

    new-instance v0, Lcom/google/gmm/common/io/protocol/ProtoBufType;

    invoke-direct {v0}, Lcom/google/gmm/common/io/protocol/ProtoBufType;-><init>()V

    sput-object v0, Lcom/google/android/location/protocol/LocserverMessageTypes;->NLP_IN_OUTDOOR_TRANSITION_TIME:Lcom/google/gmm/common/io/protocol/ProtoBufType;

    new-instance v0, Lcom/google/gmm/common/io/protocol/ProtoBufType;

    invoke-direct {v0}, Lcom/google/gmm/common/io/protocol/ProtoBufType;-><init>()V

    sput-object v0, Lcom/google/android/location/protocol/LocserverMessageTypes;->GCELL:Lcom/google/gmm/common/io/protocol/ProtoBufType;

    new-instance v0, Lcom/google/gmm/common/io/protocol/ProtoBufType;

    invoke-direct {v0}, Lcom/google/gmm/common/io/protocol/ProtoBufType;-><init>()V

    sput-object v0, Lcom/google/android/location/protocol/LocserverMessageTypes;->GACCELEROMETER_SNAPSHOT:Lcom/google/gmm/common/io/protocol/ProtoBufType;

    new-instance v0, Lcom/google/gmm/common/io/protocol/ProtoBufType;

    invoke-direct {v0}, Lcom/google/gmm/common/io/protocol/ProtoBufType;-><init>()V

    sput-object v0, Lcom/google/android/location/protocol/LocserverMessageTypes;->GSESSION_PROFILE:Lcom/google/gmm/common/io/protocol/ProtoBufType;

    new-instance v0, Lcom/google/gmm/common/io/protocol/ProtoBufType;

    invoke-direct {v0}, Lcom/google/gmm/common/io/protocol/ProtoBufType;-><init>()V

    sput-object v0, Lcom/google/android/location/protocol/LocserverMessageTypes;->GFEATURE:Lcom/google/gmm/common/io/protocol/ProtoBufType;

    new-instance v0, Lcom/google/gmm/common/io/protocol/ProtoBufType;

    invoke-direct {v0}, Lcom/google/gmm/common/io/protocol/ProtoBufType;-><init>()V

    sput-object v0, Lcom/google/android/location/protocol/LocserverMessageTypes;->NLP_IN_OUTDOOR_HISTORY:Lcom/google/gmm/common/io/protocol/ProtoBufType;

    new-instance v0, Lcom/google/gmm/common/io/protocol/ProtoBufType;

    invoke-direct {v0}, Lcom/google/gmm/common/io/protocol/ProtoBufType;-><init>()V

    sput-object v0, Lcom/google/android/location/protocol/LocserverMessageTypes;->NLP_TEMPORAL_LONG_TO_STRING_ENTRY:Lcom/google/gmm/common/io/protocol/ProtoBufType;

    new-instance v0, Lcom/google/gmm/common/io/protocol/ProtoBufType;

    invoke-direct {v0}, Lcom/google/gmm/common/io/protocol/ProtoBufType;-><init>()V

    sput-object v0, Lcom/google/android/location/protocol/LocserverMessageTypes;->GCELLULAR_PLATFORM_PROFILE:Lcom/google/gmm/common/io/protocol/ProtoBufType;

    new-instance v0, Lcom/google/gmm/common/io/protocol/ProtoBufType;

    invoke-direct {v0}, Lcom/google/gmm/common/io/protocol/ProtoBufType;-><init>()V

    sput-object v0, Lcom/google/android/location/protocol/LocserverMessageTypes;->NLP_TIME_SPAN:Lcom/google/gmm/common/io/protocol/ProtoBufType;

    new-instance v0, Lcom/google/gmm/common/io/protocol/ProtoBufType;

    invoke-direct {v0}, Lcom/google/gmm/common/io/protocol/ProtoBufType;-><init>()V

    sput-object v0, Lcom/google/android/location/protocol/LocserverMessageTypes;->NLP_TEMPORAL_STRING_TO_STRING_ENTRY:Lcom/google/gmm/common/io/protocol/ProtoBufType;

    new-instance v0, Lcom/google/gmm/common/io/protocol/ProtoBufType;

    invoke-direct {v0}, Lcom/google/gmm/common/io/protocol/ProtoBufType;-><init>()V

    sput-object v0, Lcom/google/android/location/protocol/LocserverMessageTypes;->GCELLULAR_PROFILE:Lcom/google/gmm/common/io/protocol/ProtoBufType;

    new-instance v0, Lcom/google/gmm/common/io/protocol/ProtoBufType;

    invoke-direct {v0}, Lcom/google/gmm/common/io/protocol/ProtoBufType;-><init>()V

    sput-object v0, Lcom/google/android/location/protocol/LocserverMessageTypes;->NLP_TEMPORAL_LONG_TO_STRING_CACHE:Lcom/google/gmm/common/io/protocol/ProtoBufType;

    new-instance v0, Lcom/google/gmm/common/io/protocol/ProtoBufType;

    invoke-direct {v0}, Lcom/google/gmm/common/io/protocol/ProtoBufType;-><init>()V

    sput-object v0, Lcom/google/android/location/protocol/LocserverMessageTypes;->GDEVICE_LOCATION:Lcom/google/gmm/common/io/protocol/ProtoBufType;

    new-instance v0, Lcom/google/gmm/common/io/protocol/ProtoBufType;

    invoke-direct {v0}, Lcom/google/gmm/common/io/protocol/ProtoBufType;-><init>()V

    sput-object v0, Lcom/google/android/location/protocol/LocserverMessageTypes;->GPREFETCH_MODE:Lcom/google/gmm/common/io/protocol/ProtoBufType;

    new-instance v0, Lcom/google/gmm/common/io/protocol/ProtoBufType;

    invoke-direct {v0}, Lcom/google/gmm/common/io/protocol/ProtoBufType;-><init>()V

    sput-object v0, Lcom/google/android/location/protocol/LocserverMessageTypes;->GCLIENT_SERVER_QUERY_STATS:Lcom/google/gmm/common/io/protocol/ProtoBufType;

    new-instance v0, Lcom/google/gmm/common/io/protocol/ProtoBufType;

    invoke-direct {v0}, Lcom/google/gmm/common/io/protocol/ProtoBufType;-><init>()V

    sput-object v0, Lcom/google/android/location/protocol/LocserverMessageTypes;->GCLIENT_COLLECTOR_STATS:Lcom/google/gmm/common/io/protocol/ProtoBufType;

    new-instance v0, Lcom/google/gmm/common/io/protocol/ProtoBufType;

    invoke-direct {v0}, Lcom/google/gmm/common/io/protocol/ProtoBufType;-><init>()V

    sput-object v0, Lcom/google/android/location/protocol/LocserverMessageTypes;->GBAROMETER_SNAPSHOT:Lcom/google/gmm/common/io/protocol/ProtoBufType;

    new-instance v0, Lcom/google/gmm/common/io/protocol/ProtoBufType;

    invoke-direct {v0}, Lcom/google/gmm/common/io/protocol/ProtoBufType;-><init>()V

    sput-object v0, Lcom/google/android/location/protocol/LocserverMessageTypes;->GORIENTATION_SNAPSHOT:Lcom/google/gmm/common/io/protocol/ProtoBufType;

    new-instance v0, Lcom/google/gmm/common/io/protocol/ProtoBufType;

    invoke-direct {v0}, Lcom/google/gmm/common/io/protocol/ProtoBufType;-><init>()V

    sput-object v0, Lcom/google/android/location/protocol/LocserverMessageTypes;->GGPS_STATUS_SNAPSHOT:Lcom/google/gmm/common/io/protocol/ProtoBufType;

    new-instance v0, Lcom/google/gmm/common/io/protocol/ProtoBufType;

    invoke-direct {v0}, Lcom/google/gmm/common/io/protocol/ProtoBufType;-><init>()V

    sput-object v0, Lcom/google/android/location/protocol/LocserverMessageTypes;->GLOC_REQUEST:Lcom/google/gmm/common/io/protocol/ProtoBufType;

    sget-object v0, Lcom/google/android/location/protocol/LocserverMessageTypes;->GCLIENT_POLICY_STATS:Lcom/google/gmm/common/io/protocol/ProtoBufType;

    sget-object v1, Lcom/google/android/location/protocol/LocserverMessageTypes;->GCLIENT_TOKEN_BUCKET_STATS:Lcom/google/gmm/common/io/protocol/ProtoBufType;

    invoke-virtual {v0, v10, v8, v1}, Lcom/google/gmm/common/io/protocol/ProtoBufType;->addElement(IILjava/lang/Object;)Lcom/google/gmm/common/io/protocol/ProtoBufType;

    move-result-object v0

    sget-object v1, Lcom/google/android/location/protocol/LocserverMessageTypes;->GCLIENT_TOKEN_BUCKET_STATS:Lcom/google/gmm/common/io/protocol/ProtoBufType;

    invoke-virtual {v0, v10, v9, v1}, Lcom/google/gmm/common/io/protocol/ProtoBufType;->addElement(IILjava/lang/Object;)Lcom/google/gmm/common/io/protocol/ProtoBufType;

    move-result-object v0

    const/4 v1, 0x3

    sget-object v2, Lcom/google/android/location/protocol/LocserverMessageTypes;->GCLIENT_TOKEN_BUCKET_STATS:Lcom/google/gmm/common/io/protocol/ProtoBufType;

    invoke-virtual {v0, v10, v1, v2}, Lcom/google/gmm/common/io/protocol/ProtoBufType;->addElement(IILjava/lang/Object;)Lcom/google/gmm/common/io/protocol/ProtoBufType;

    sget-object v0, Lcom/google/android/location/protocol/LocserverMessageTypes;->GFEATURE_TYPE_RESTRICT:Lcom/google/gmm/common/io/protocol/ProtoBufType;

    const/16 v1, 0x415

    invoke-virtual {v0, v1, v8, v6}, Lcom/google/gmm/common/io/protocol/ProtoBufType;->addElement(IILjava/lang/Object;)Lcom/google/gmm/common/io/protocol/ProtoBufType;

    move-result-object v0

    const/16 v1, 0x415

    invoke-virtual {v0, v1, v9, v6}, Lcom/google/gmm/common/io/protocol/ProtoBufType;->addElement(IILjava/lang/Object;)Lcom/google/gmm/common/io/protocol/ProtoBufType;

    sget-object v0, Lcom/google/android/location/protocol/LocserverMessageTypes;->GLOC_REQUEST_ELEMENT:Lcom/google/gmm/common/io/protocol/ProtoBufType;

    invoke-virtual {v0, v10, v8, v6}, Lcom/google/gmm/common/io/protocol/ProtoBufType;->addElement(IILjava/lang/Object;)Lcom/google/gmm/common/io/protocol/ProtoBufType;

    move-result-object v0

    invoke-virtual {v0, v10, v9, v6}, Lcom/google/gmm/common/io/protocol/ProtoBufType;->addElement(IILjava/lang/Object;)Lcom/google/gmm/common/io/protocol/ProtoBufType;

    move-result-object v0

    const/4 v1, 0x3

    invoke-virtual {v0, v10, v1, v6}, Lcom/google/gmm/common/io/protocol/ProtoBufType;->addElement(IILjava/lang/Object;)Lcom/google/gmm/common/io/protocol/ProtoBufType;

    move-result-object v0

    const/4 v1, 0x4

    invoke-virtual {v0, v10, v1, v6}, Lcom/google/gmm/common/io/protocol/ProtoBufType;->addElement(IILjava/lang/Object;)Lcom/google/gmm/common/io/protocol/ProtoBufType;

    move-result-object v0

    const/4 v1, 0x5

    invoke-virtual {v0, v10, v1, v6}, Lcom/google/gmm/common/io/protocol/ProtoBufType;->addElement(IILjava/lang/Object;)Lcom/google/gmm/common/io/protocol/ProtoBufType;

    move-result-object v0

    const/16 v1, 0x415

    const/4 v2, 0x6

    invoke-virtual {v0, v1, v2, v6}, Lcom/google/gmm/common/io/protocol/ProtoBufType;->addElement(IILjava/lang/Object;)Lcom/google/gmm/common/io/protocol/ProtoBufType;

    move-result-object v0

    const/16 v1, 0x419

    const/4 v2, 0x7

    invoke-virtual {v0, v1, v2, v6}, Lcom/google/gmm/common/io/protocol/ProtoBufType;->addElement(IILjava/lang/Object;)Lcom/google/gmm/common/io/protocol/ProtoBufType;

    move-result-object v0

    const/16 v1, 0x9

    invoke-virtual {v0, v10, v1, v6}, Lcom/google/gmm/common/io/protocol/ProtoBufType;->addElement(IILjava/lang/Object;)Lcom/google/gmm/common/io/protocol/ProtoBufType;

    move-result-object v0

    const/16 v1, 0xa

    new-instance v2, Ljava/lang/Long;

    const-wide/16 v3, 0xf

    invoke-direct {v2, v3, v4}, Ljava/lang/Long;-><init>(J)V

    invoke-virtual {v0, v7, v1, v2}, Lcom/google/gmm/common/io/protocol/ProtoBufType;->addElement(IILjava/lang/Object;)Lcom/google/gmm/common/io/protocol/ProtoBufType;

    move-result-object v0

    const/16 v1, 0xb

    invoke-virtual {v0, v10, v1, v6}, Lcom/google/gmm/common/io/protocol/ProtoBufType;->addElement(IILjava/lang/Object;)Lcom/google/gmm/common/io/protocol/ProtoBufType;

    move-result-object v0

    const/16 v1, 0xc

    invoke-virtual {v0, v10, v1, v6}, Lcom/google/gmm/common/io/protocol/ProtoBufType;->addElement(IILjava/lang/Object;)Lcom/google/gmm/common/io/protocol/ProtoBufType;

    move-result-object v0

    const/16 v1, 0x63

    invoke-virtual {v0, v10, v1, v6}, Lcom/google/gmm/common/io/protocol/ProtoBufType;->addElement(IILjava/lang/Object;)Lcom/google/gmm/common/io/protocol/ProtoBufType;

    sget-object v0, Lcom/google/android/location/protocol/LocserverMessageTypes;->GADDRESS:Lcom/google/gmm/common/io/protocol/ProtoBufType;

    const/16 v1, 0x41c

    invoke-virtual {v0, v1, v8, v6}, Lcom/google/gmm/common/io/protocol/ProtoBufType;->addElement(IILjava/lang/Object;)Lcom/google/gmm/common/io/protocol/ProtoBufType;

    move-result-object v0

    const/16 v1, 0x41b

    sget-object v2, Lcom/google/android/location/protocol/LocserverMessageTypes;->GADDRESS_COMPONENT:Lcom/google/gmm/common/io/protocol/ProtoBufType;

    invoke-virtual {v0, v1, v9, v2}, Lcom/google/gmm/common/io/protocol/ProtoBufType;->addElement(IILjava/lang/Object;)Lcom/google/gmm/common/io/protocol/ProtoBufType;

    sget-object v0, Lcom/google/android/location/protocol/LocserverMessageTypes;->NLP_COLLECTION_POLICY_STATE:Lcom/google/gmm/common/io/protocol/ProtoBufType;

    const/16 v1, 0x113

    invoke-virtual {v0, v1, v8, v6}, Lcom/google/gmm/common/io/protocol/ProtoBufType;->addElement(IILjava/lang/Object;)Lcom/google/gmm/common/io/protocol/ProtoBufType;

    move-result-object v0

    const/16 v1, 0x113

    invoke-virtual {v0, v1, v9, v6}, Lcom/google/gmm/common/io/protocol/ProtoBufType;->addElement(IILjava/lang/Object;)Lcom/google/gmm/common/io/protocol/ProtoBufType;

    move-result-object v0

    const/4 v1, 0x3

    sget-object v2, Lcom/google/android/location/protocol/LocserverMessageTypes;->NLP_TOKEN_BUCKET_STATE:Lcom/google/gmm/common/io/protocol/ProtoBufType;

    invoke-virtual {v0, v10, v1, v2}, Lcom/google/gmm/common/io/protocol/ProtoBufType;->addElement(IILjava/lang/Object;)Lcom/google/gmm/common/io/protocol/ProtoBufType;

    move-result-object v0

    const/4 v1, 0x4

    sget-object v2, Lcom/google/android/location/protocol/LocserverMessageTypes;->NLP_TOKEN_BUCKET_STATE:Lcom/google/gmm/common/io/protocol/ProtoBufType;

    invoke-virtual {v0, v10, v1, v2}, Lcom/google/gmm/common/io/protocol/ProtoBufType;->addElement(IILjava/lang/Object;)Lcom/google/gmm/common/io/protocol/ProtoBufType;

    move-result-object v0

    const/4 v1, 0x5

    sget-object v2, Lcom/google/android/location/protocol/LocserverMessageTypes;->NLP_TOKEN_BUCKET_STATE:Lcom/google/gmm/common/io/protocol/ProtoBufType;

    invoke-virtual {v0, v10, v1, v2}, Lcom/google/gmm/common/io/protocol/ProtoBufType;->addElement(IILjava/lang/Object;)Lcom/google/gmm/common/io/protocol/ProtoBufType;

    sget-object v0, Lcom/google/android/location/protocol/LocserverMessageTypes;->GSIGNAL_FINGERPRINT_MODEL:Lcom/google/gmm/common/io/protocol/ProtoBufType;

    const/4 v1, 0x3

    invoke-virtual {v0, v10, v1, v6}, Lcom/google/gmm/common/io/protocol/ProtoBufType;->addElement(IILjava/lang/Object;)Lcom/google/gmm/common/io/protocol/ProtoBufType;

    sget-object v0, Lcom/google/android/location/protocol/LocserverMessageTypes;->GWIFI_CLUSTER_MEMBERSHIP:Lcom/google/gmm/common/io/protocol/ProtoBufType;

    const/16 v1, 0x21c

    invoke-virtual {v0, v1, v8, v6}, Lcom/google/gmm/common/io/protocol/ProtoBufType;->addElement(IILjava/lang/Object;)Lcom/google/gmm/common/io/protocol/ProtoBufType;

    move-result-object v0

    const/16 v1, 0x413

    invoke-virtual {v0, v1, v9, v6}, Lcom/google/gmm/common/io/protocol/ProtoBufType;->addElement(IILjava/lang/Object;)Lcom/google/gmm/common/io/protocol/ProtoBufType;

    sget-object v0, Lcom/google/android/location/protocol/LocserverMessageTypes;->GGPS_SATELLITE:Lcom/google/gmm/common/io/protocol/ProtoBufType;

    invoke-virtual {v0, v7, v8, v6}, Lcom/google/gmm/common/io/protocol/ProtoBufType;->addElement(IILjava/lang/Object;)Lcom/google/gmm/common/io/protocol/ProtoBufType;

    move-result-object v0

    const/16 v1, 0x212

    invoke-virtual {v0, v1, v9, v6}, Lcom/google/gmm/common/io/protocol/ProtoBufType;->addElement(IILjava/lang/Object;)Lcom/google/gmm/common/io/protocol/ProtoBufType;

    move-result-object v0

    const/16 v1, 0x212

    const/4 v2, 0x3

    invoke-virtual {v0, v1, v2, v6}, Lcom/google/gmm/common/io/protocol/ProtoBufType;->addElement(IILjava/lang/Object;)Lcom/google/gmm/common/io/protocol/ProtoBufType;

    move-result-object v0

    const/16 v1, 0x212

    const/4 v2, 0x4

    invoke-virtual {v0, v1, v2, v6}, Lcom/google/gmm/common/io/protocol/ProtoBufType;->addElement(IILjava/lang/Object;)Lcom/google/gmm/common/io/protocol/ProtoBufType;

    sget-object v0, Lcom/google/android/location/protocol/LocserverMessageTypes;->GGEOCODE_REQUEST:Lcom/google/gmm/common/io/protocol/ProtoBufType;

    const/16 v1, 0x217

    new-instance v2, Ljava/lang/Long;

    const-wide/16 v3, 0x1

    invoke-direct {v2, v3, v4}, Ljava/lang/Long;-><init>(J)V

    invoke-virtual {v0, v1, v8, v2}, Lcom/google/gmm/common/io/protocol/ProtoBufType;->addElement(IILjava/lang/Object;)Lcom/google/gmm/common/io/protocol/ProtoBufType;

    move-result-object v0

    const/16 v1, 0x218

    sget-object v2, Lcom/google/gmm/common/io/protocol/ProtoBuf;->FALSE:Ljava/lang/Boolean;

    invoke-virtual {v0, v1, v9, v2}, Lcom/google/gmm/common/io/protocol/ProtoBufType;->addElement(IILjava/lang/Object;)Lcom/google/gmm/common/io/protocol/ProtoBufType;

    move-result-object v0

    const/4 v1, 0x3

    invoke-virtual {v0, v10, v1, v6}, Lcom/google/gmm/common/io/protocol/ProtoBufType;->addElement(IILjava/lang/Object;)Lcom/google/gmm/common/io/protocol/ProtoBufType;

    move-result-object v0

    const/4 v1, 0x4

    new-instance v2, Ljava/lang/Long;

    const-wide/16 v3, 0x0

    invoke-direct {v2, v3, v4}, Ljava/lang/Long;-><init>(J)V

    invoke-virtual {v0, v7, v1, v2}, Lcom/google/gmm/common/io/protocol/ProtoBufType;->addElement(IILjava/lang/Object;)Lcom/google/gmm/common/io/protocol/ProtoBufType;

    move-result-object v0

    const/4 v1, 0x5

    invoke-virtual {v0, v10, v1, v6}, Lcom/google/gmm/common/io/protocol/ProtoBufType;->addElement(IILjava/lang/Object;)Lcom/google/gmm/common/io/protocol/ProtoBufType;

    move-result-object v0

    const/16 v1, 0x218

    const/4 v2, 0x6

    sget-object v3, Lcom/google/gmm/common/io/protocol/ProtoBuf;->FALSE:Ljava/lang/Boolean;

    invoke-virtual {v0, v1, v2, v3}, Lcom/google/gmm/common/io/protocol/ProtoBufType;->addElement(IILjava/lang/Object;)Lcom/google/gmm/common/io/protocol/ProtoBufType;

    sget-object v0, Lcom/google/android/location/protocol/LocserverMessageTypes;->GSENSORS_PROFILE:Lcom/google/gmm/common/io/protocol/ProtoBufType;

    const/16 v1, 0x213

    invoke-virtual {v0, v1, v8, v6}, Lcom/google/gmm/common/io/protocol/ProtoBufType;->addElement(IILjava/lang/Object;)Lcom/google/gmm/common/io/protocol/ProtoBufType;

    move-result-object v0

    const/16 v1, 0x41b

    invoke-virtual {v0, v1, v9, v6}, Lcom/google/gmm/common/io/protocol/ProtoBufType;->addElement(IILjava/lang/Object;)Lcom/google/gmm/common/io/protocol/ProtoBufType;

    move-result-object v0

    const/16 v1, 0x41b

    const/4 v2, 0x3

    invoke-virtual {v0, v1, v2, v6}, Lcom/google/gmm/common/io/protocol/ProtoBufType;->addElement(IILjava/lang/Object;)Lcom/google/gmm/common/io/protocol/ProtoBufType;

    move-result-object v0

    const/16 v1, 0x41b

    const/4 v2, 0x4

    invoke-virtual {v0, v1, v2, v6}, Lcom/google/gmm/common/io/protocol/ProtoBufType;->addElement(IILjava/lang/Object;)Lcom/google/gmm/common/io/protocol/ProtoBufType;

    move-result-object v0

    const/16 v1, 0x41b

    const/4 v2, 0x5

    invoke-virtual {v0, v1, v2, v6}, Lcom/google/gmm/common/io/protocol/ProtoBufType;->addElement(IILjava/lang/Object;)Lcom/google/gmm/common/io/protocol/ProtoBufType;

    move-result-object v0

    const/16 v1, 0x41b

    const/4 v2, 0x6

    invoke-virtual {v0, v1, v2, v6}, Lcom/google/gmm/common/io/protocol/ProtoBufType;->addElement(IILjava/lang/Object;)Lcom/google/gmm/common/io/protocol/ProtoBufType;

    move-result-object v0

    const/16 v1, 0x41b

    const/4 v2, 0x7

    invoke-virtual {v0, v1, v2, v6}, Lcom/google/gmm/common/io/protocol/ProtoBufType;->addElement(IILjava/lang/Object;)Lcom/google/gmm/common/io/protocol/ProtoBufType;

    move-result-object v0

    const/16 v1, 0x41b

    const/16 v2, 0x8

    invoke-virtual {v0, v1, v2, v6}, Lcom/google/gmm/common/io/protocol/ProtoBufType;->addElement(IILjava/lang/Object;)Lcom/google/gmm/common/io/protocol/ProtoBufType;

    move-result-object v0

    const/16 v1, 0x9

    invoke-virtual {v0, v10, v1, v6}, Lcom/google/gmm/common/io/protocol/ProtoBufType;->addElement(IILjava/lang/Object;)Lcom/google/gmm/common/io/protocol/ProtoBufType;

    move-result-object v0

    const/16 v1, 0x41b

    const/16 v2, 0xa

    invoke-virtual {v0, v1, v2, v6}, Lcom/google/gmm/common/io/protocol/ProtoBufType;->addElement(IILjava/lang/Object;)Lcom/google/gmm/common/io/protocol/ProtoBufType;

    move-result-object v0

    const/16 v1, 0x41b

    const/16 v2, 0xb

    invoke-virtual {v0, v1, v2, v6}, Lcom/google/gmm/common/io/protocol/ProtoBufType;->addElement(IILjava/lang/Object;)Lcom/google/gmm/common/io/protocol/ProtoBufType;

    sget-object v0, Lcom/google/android/location/protocol/LocserverMessageTypes;->NLP_SENSOR_COLLECTION_STATE:Lcom/google/gmm/common/io/protocol/ProtoBufType;

    const/16 v1, 0x213

    invoke-virtual {v0, v1, v8, v6}, Lcom/google/gmm/common/io/protocol/ProtoBufType;->addElement(IILjava/lang/Object;)Lcom/google/gmm/common/io/protocol/ProtoBufType;

    move-result-object v0

    const/16 v1, 0x213

    invoke-virtual {v0, v1, v9, v6}, Lcom/google/gmm/common/io/protocol/ProtoBufType;->addElement(IILjava/lang/Object;)Lcom/google/gmm/common/io/protocol/ProtoBufType;

    move-result-object v0

    const/16 v1, 0x218

    const/4 v2, 0x3

    invoke-virtual {v0, v1, v2, v6}, Lcom/google/gmm/common/io/protocol/ProtoBufType;->addElement(IILjava/lang/Object;)Lcom/google/gmm/common/io/protocol/ProtoBufType;

    move-result-object v0

    const/4 v1, 0x4

    invoke-virtual {v0, v7, v1, v6}, Lcom/google/gmm/common/io/protocol/ProtoBufType;->addElement(IILjava/lang/Object;)Lcom/google/gmm/common/io/protocol/ProtoBufType;

    move-result-object v0

    const/4 v1, 0x5

    new-instance v2, Ljava/lang/Long;

    const-wide/16 v3, 0x1

    invoke-direct {v2, v3, v4}, Ljava/lang/Long;-><init>(J)V

    invoke-virtual {v0, v7, v1, v2}, Lcom/google/gmm/common/io/protocol/ProtoBufType;->addElement(IILjava/lang/Object;)Lcom/google/gmm/common/io/protocol/ProtoBufType;

    sget-object v0, Lcom/google/android/location/protocol/LocserverMessageTypes;->GSENSORS_PLATFORM_PROFILE:Lcom/google/gmm/common/io/protocol/ProtoBufType;

    invoke-virtual {v0, v10, v8, v6}, Lcom/google/gmm/common/io/protocol/ProtoBufType;->addElement(IILjava/lang/Object;)Lcom/google/gmm/common/io/protocol/ProtoBufType;

    sget-object v0, Lcom/google/android/location/protocol/LocserverMessageTypes;->NLP_ELEMENT_APP_DATA:Lcom/google/gmm/common/io/protocol/ProtoBufType;

    sget-object v1, Lcom/google/android/location/protocol/LocserverMessageTypes;->GLAT_LNG:Lcom/google/gmm/common/io/protocol/ProtoBufType;

    invoke-virtual {v0, v10, v8, v1}, Lcom/google/gmm/common/io/protocol/ProtoBufType;->addElement(IILjava/lang/Object;)Lcom/google/gmm/common/io/protocol/ProtoBufType;

    move-result-object v0

    invoke-virtual {v0, v7, v9, v6}, Lcom/google/gmm/common/io/protocol/ProtoBufType;->addElement(IILjava/lang/Object;)Lcom/google/gmm/common/io/protocol/ProtoBufType;

    move-result-object v0

    const/16 v1, 0x21c

    const/4 v2, 0x3

    invoke-virtual {v0, v1, v2, v6}, Lcom/google/gmm/common/io/protocol/ProtoBufType;->addElement(IILjava/lang/Object;)Lcom/google/gmm/common/io/protocol/ProtoBufType;

    move-result-object v0

    const/4 v1, 0x4

    new-instance v2, Ljava/lang/Long;

    const-wide/16 v3, 0x0

    invoke-direct {v2, v3, v4}, Ljava/lang/Long;-><init>(J)V

    invoke-virtual {v0, v7, v1, v2}, Lcom/google/gmm/common/io/protocol/ProtoBufType;->addElement(IILjava/lang/Object;)Lcom/google/gmm/common/io/protocol/ProtoBufType;

    move-result-object v0

    const/16 v1, 0x21c

    const/4 v2, 0x7

    invoke-virtual {v0, v1, v2, v6}, Lcom/google/gmm/common/io/protocol/ProtoBufType;->addElement(IILjava/lang/Object;)Lcom/google/gmm/common/io/protocol/ProtoBufType;

    move-result-object v0

    const/16 v1, 0x21c

    const/4 v2, 0x5

    invoke-virtual {v0, v1, v2, v6}, Lcom/google/gmm/common/io/protocol/ProtoBufType;->addElement(IILjava/lang/Object;)Lcom/google/gmm/common/io/protocol/ProtoBufType;

    move-result-object v0

    const/4 v1, 0x6

    new-instance v2, Ljava/lang/Long;

    const-wide/16 v3, 0x0

    invoke-direct {v2, v3, v4}, Ljava/lang/Long;-><init>(J)V

    invoke-virtual {v0, v7, v1, v2}, Lcom/google/gmm/common/io/protocol/ProtoBufType;->addElement(IILjava/lang/Object;)Lcom/google/gmm/common/io/protocol/ProtoBufType;

    sget-object v0, Lcom/google/android/location/protocol/LocserverMessageTypes;->GUSER_PROFILE:Lcom/google/gmm/common/io/protocol/ProtoBufType;

    const/16 v1, 0x21c

    invoke-virtual {v0, v1, v8, v6}, Lcom/google/gmm/common/io/protocol/ProtoBufType;->addElement(IILjava/lang/Object;)Lcom/google/gmm/common/io/protocol/ProtoBufType;

    move-result-object v0

    const/16 v1, 0x21c

    invoke-virtual {v0, v1, v9, v6}, Lcom/google/gmm/common/io/protocol/ProtoBufType;->addElement(IILjava/lang/Object;)Lcom/google/gmm/common/io/protocol/ProtoBufType;

    sget-object v0, Lcom/google/android/location/protocol/LocserverMessageTypes;->GCLIENT_STATS:Lcom/google/gmm/common/io/protocol/ProtoBufType;

    const/16 v1, 0x113

    invoke-virtual {v0, v1, v8, v6}, Lcom/google/gmm/common/io/protocol/ProtoBufType;->addElement(IILjava/lang/Object;)Lcom/google/gmm/common/io/protocol/ProtoBufType;

    move-result-object v0

    const/16 v1, 0x115

    invoke-virtual {v0, v1, v9, v6}, Lcom/google/gmm/common/io/protocol/ProtoBufType;->addElement(IILjava/lang/Object;)Lcom/google/gmm/common/io/protocol/ProtoBufType;

    move-result-object v0

    const/4 v1, 0x3

    sget-object v2, Lcom/google/android/location/protocol/LocserverMessageTypes;->GCLIENT_CACHE_STATS:Lcom/google/gmm/common/io/protocol/ProtoBufType;

    invoke-virtual {v0, v10, v1, v2}, Lcom/google/gmm/common/io/protocol/ProtoBufType;->addElement(IILjava/lang/Object;)Lcom/google/gmm/common/io/protocol/ProtoBufType;

    move-result-object v0

    const/4 v1, 0x4

    sget-object v2, Lcom/google/android/location/protocol/LocserverMessageTypes;->GCLIENT_CACHE_STATS:Lcom/google/gmm/common/io/protocol/ProtoBufType;

    invoke-virtual {v0, v10, v1, v2}, Lcom/google/gmm/common/io/protocol/ProtoBufType;->addElement(IILjava/lang/Object;)Lcom/google/gmm/common/io/protocol/ProtoBufType;

    move-result-object v0

    const/16 v1, 0x41b

    const/4 v2, 0x5

    sget-object v3, Lcom/google/android/location/protocol/LocserverMessageTypes;->GCLIENT_SERVER_QUERY_STATS:Lcom/google/gmm/common/io/protocol/ProtoBufType;

    invoke-virtual {v0, v1, v2, v3}, Lcom/google/gmm/common/io/protocol/ProtoBufType;->addElement(IILjava/lang/Object;)Lcom/google/gmm/common/io/protocol/ProtoBufType;

    move-result-object v0

    const/4 v1, 0x6

    invoke-virtual {v0, v7, v1, v6}, Lcom/google/gmm/common/io/protocol/ProtoBufType;->addElement(IILjava/lang/Object;)Lcom/google/gmm/common/io/protocol/ProtoBufType;

    move-result-object v0

    const/4 v1, 0x7

    invoke-virtual {v0, v7, v1, v6}, Lcom/google/gmm/common/io/protocol/ProtoBufType;->addElement(IILjava/lang/Object;)Lcom/google/gmm/common/io/protocol/ProtoBufType;

    move-result-object v0

    const/16 v1, 0x8

    invoke-virtual {v0, v7, v1, v6}, Lcom/google/gmm/common/io/protocol/ProtoBufType;->addElement(IILjava/lang/Object;)Lcom/google/gmm/common/io/protocol/ProtoBufType;

    move-result-object v0

    const/16 v1, 0x9

    invoke-virtual {v0, v7, v1, v6}, Lcom/google/gmm/common/io/protocol/ProtoBufType;->addElement(IILjava/lang/Object;)Lcom/google/gmm/common/io/protocol/ProtoBufType;

    move-result-object v0

    const/16 v1, 0xa

    invoke-virtual {v0, v7, v1, v6}, Lcom/google/gmm/common/io/protocol/ProtoBufType;->addElement(IILjava/lang/Object;)Lcom/google/gmm/common/io/protocol/ProtoBufType;

    move-result-object v0

    const/16 v1, 0xb

    sget-object v2, Lcom/google/android/location/protocol/LocserverMessageTypes;->GCLIENT_CACHE_STATS:Lcom/google/gmm/common/io/protocol/ProtoBufType;

    invoke-virtual {v0, v10, v1, v2}, Lcom/google/gmm/common/io/protocol/ProtoBufType;->addElement(IILjava/lang/Object;)Lcom/google/gmm/common/io/protocol/ProtoBufType;

    move-result-object v0

    const/16 v1, 0xc

    sget-object v2, Lcom/google/android/location/protocol/LocserverMessageTypes;->GCLIENT_COLLECTOR_STATS:Lcom/google/gmm/common/io/protocol/ProtoBufType;

    invoke-virtual {v0, v10, v1, v2}, Lcom/google/gmm/common/io/protocol/ProtoBufType;->addElement(IILjava/lang/Object;)Lcom/google/gmm/common/io/protocol/ProtoBufType;

    move-result-object v0

    const/16 v1, 0xd

    sget-object v2, Lcom/google/android/location/protocol/LocserverMessageTypes;->GCLIENT_POLICY_STATS:Lcom/google/gmm/common/io/protocol/ProtoBufType;

    invoke-virtual {v0, v10, v1, v2}, Lcom/google/gmm/common/io/protocol/ProtoBufType;->addElement(IILjava/lang/Object;)Lcom/google/gmm/common/io/protocol/ProtoBufType;

    move-result-object v0

    const/16 v1, 0xe

    invoke-virtual {v0, v7, v1, v6}, Lcom/google/gmm/common/io/protocol/ProtoBufType;->addElement(IILjava/lang/Object;)Lcom/google/gmm/common/io/protocol/ProtoBufType;

    move-result-object v0

    const/16 v1, 0xf

    invoke-virtual {v0, v7, v1, v6}, Lcom/google/gmm/common/io/protocol/ProtoBufType;->addElement(IILjava/lang/Object;)Lcom/google/gmm/common/io/protocol/ProtoBufType;

    sget-object v0, Lcom/google/android/location/protocol/LocserverMessageTypes;->NLP_TOKEN_BUCKET_STATE:Lcom/google/gmm/common/io/protocol/ProtoBufType;

    const/16 v1, 0x113

    invoke-virtual {v0, v1, v8, v6}, Lcom/google/gmm/common/io/protocol/ProtoBufType;->addElement(IILjava/lang/Object;)Lcom/google/gmm/common/io/protocol/ProtoBufType;

    move-result-object v0

    const/16 v1, 0x113

    invoke-virtual {v0, v1, v9, v6}, Lcom/google/gmm/common/io/protocol/ProtoBufType;->addElement(IILjava/lang/Object;)Lcom/google/gmm/common/io/protocol/ProtoBufType;

    sget-object v0, Lcom/google/android/location/protocol/LocserverMessageTypes;->NLP_WIFI_AP_PROFILE:Lcom/google/gmm/common/io/protocol/ProtoBufType;

    const/16 v1, 0x213

    invoke-virtual {v0, v1, v8, v6}, Lcom/google/gmm/common/io/protocol/ProtoBufType;->addElement(IILjava/lang/Object;)Lcom/google/gmm/common/io/protocol/ProtoBufType;

    move-result-object v0

    const/16 v1, 0x221

    invoke-virtual {v0, v1, v9, v6}, Lcom/google/gmm/common/io/protocol/ProtoBufType;->addElement(IILjava/lang/Object;)Lcom/google/gmm/common/io/protocol/ProtoBufType;

    move-result-object v0

    const/16 v1, 0x221

    const/4 v2, 0x3

    invoke-virtual {v0, v1, v2, v6}, Lcom/google/gmm/common/io/protocol/ProtoBufType;->addElement(IILjava/lang/Object;)Lcom/google/gmm/common/io/protocol/ProtoBufType;

    move-result-object v0

    const/16 v1, 0x212

    const/4 v2, 0x4

    invoke-virtual {v0, v1, v2, v6}, Lcom/google/gmm/common/io/protocol/ProtoBufType;->addElement(IILjava/lang/Object;)Lcom/google/gmm/common/io/protocol/ProtoBufType;

    move-result-object v0

    const/4 v1, 0x5

    invoke-virtual {v0, v7, v1, v6}, Lcom/google/gmm/common/io/protocol/ProtoBufType;->addElement(IILjava/lang/Object;)Lcom/google/gmm/common/io/protocol/ProtoBufType;

    move-result-object v0

    const/4 v1, 0x6

    new-instance v2, Ljava/lang/Long;

    const-wide/16 v3, 0x0

    invoke-direct {v2, v3, v4}, Ljava/lang/Long;-><init>(J)V

    invoke-virtual {v0, v7, v1, v2}, Lcom/google/gmm/common/io/protocol/ProtoBufType;->addElement(IILjava/lang/Object;)Lcom/google/gmm/common/io/protocol/ProtoBufType;

    sget-object v0, Lcom/google/android/location/protocol/LocserverMessageTypes;->NLP_COLLECTOR_STATE:Lcom/google/gmm/common/io/protocol/ProtoBufType;

    const/16 v1, 0x41b

    sget-object v2, Lcom/google/android/location/protocol/LocserverMessageTypes;->NLP_SENSOR_COLLECTION_STATE:Lcom/google/gmm/common/io/protocol/ProtoBufType;

    invoke-virtual {v0, v1, v8, v2}, Lcom/google/gmm/common/io/protocol/ProtoBufType;->addElement(IILjava/lang/Object;)Lcom/google/gmm/common/io/protocol/ProtoBufType;

    move-result-object v0

    const/16 v1, 0x213

    invoke-virtual {v0, v1, v9, v6}, Lcom/google/gmm/common/io/protocol/ProtoBufType;->addElement(IILjava/lang/Object;)Lcom/google/gmm/common/io/protocol/ProtoBufType;

    move-result-object v0

    const/16 v1, 0x213

    const/4 v2, 0x3

    invoke-virtual {v0, v1, v2, v6}, Lcom/google/gmm/common/io/protocol/ProtoBufType;->addElement(IILjava/lang/Object;)Lcom/google/gmm/common/io/protocol/ProtoBufType;

    move-result-object v0

    const/4 v1, 0x4

    new-instance v2, Ljava/lang/Long;

    const-wide/16 v3, 0x1

    invoke-direct {v2, v3, v4}, Ljava/lang/Long;-><init>(J)V

    invoke-virtual {v0, v7, v1, v2}, Lcom/google/gmm/common/io/protocol/ProtoBufType;->addElement(IILjava/lang/Object;)Lcom/google/gmm/common/io/protocol/ProtoBufType;

    move-result-object v0

    const/4 v1, 0x5

    new-instance v2, Ljava/lang/Long;

    const-wide/16 v3, 0x1

    invoke-direct {v2, v3, v4}, Ljava/lang/Long;-><init>(J)V

    invoke-virtual {v0, v7, v1, v2}, Lcom/google/gmm/common/io/protocol/ProtoBufType;->addElement(IILjava/lang/Object;)Lcom/google/gmm/common/io/protocol/ProtoBufType;

    move-result-object v0

    const/16 v1, 0x213

    const/4 v2, 0x6

    invoke-virtual {v0, v1, v2, v6}, Lcom/google/gmm/common/io/protocol/ProtoBufType;->addElement(IILjava/lang/Object;)Lcom/google/gmm/common/io/protocol/ProtoBufType;

    move-result-object v0

    const/4 v1, 0x7

    invoke-virtual {v0, v7, v1, v6}, Lcom/google/gmm/common/io/protocol/ProtoBufType;->addElement(IILjava/lang/Object;)Lcom/google/gmm/common/io/protocol/ProtoBufType;

    move-result-object v0

    const/16 v1, 0x213

    const/16 v2, 0x8

    invoke-virtual {v0, v1, v2, v6}, Lcom/google/gmm/common/io/protocol/ProtoBufType;->addElement(IILjava/lang/Object;)Lcom/google/gmm/common/io/protocol/ProtoBufType;

    move-result-object v0

    const/16 v1, 0x213

    const/16 v2, 0x9

    invoke-virtual {v0, v1, v2, v6}, Lcom/google/gmm/common/io/protocol/ProtoBufType;->addElement(IILjava/lang/Object;)Lcom/google/gmm/common/io/protocol/ProtoBufType;

    move-result-object v0

    const/16 v1, 0x41b

    const/16 v2, 0xa

    sget-object v3, Lcom/google/android/location/protocol/LocserverMessageTypes;->NLP_IN_OUTDOOR_TRANSITION_TIME:Lcom/google/gmm/common/io/protocol/ProtoBufType;

    invoke-virtual {v0, v1, v2, v3}, Lcom/google/gmm/common/io/protocol/ProtoBufType;->addElement(IILjava/lang/Object;)Lcom/google/gmm/common/io/protocol/ProtoBufType;

    move-result-object v0

    const/16 v1, 0xb

    invoke-virtual {v0, v7, v1, v6}, Lcom/google/gmm/common/io/protocol/ProtoBufType;->addElement(IILjava/lang/Object;)Lcom/google/gmm/common/io/protocol/ProtoBufType;

    sget-object v0, Lcom/google/android/location/protocol/LocserverMessageTypes;->GSIGNAL_FINGERPRINT_MODEL_CLUSTER:Lcom/google/gmm/common/io/protocol/ProtoBufType;

    invoke-virtual {v0, v10, v8, v6}, Lcom/google/gmm/common/io/protocol/ProtoBufType;->addElement(IILjava/lang/Object;)Lcom/google/gmm/common/io/protocol/ProtoBufType;

    sget-object v0, Lcom/google/android/location/protocol/LocserverMessageTypes;->GSIGNAL_FINGERPRINT_MODEL_REQUEST:Lcom/google/gmm/common/io/protocol/ProtoBufType;

    invoke-virtual {v0, v7, v8, v6}, Lcom/google/gmm/common/io/protocol/ProtoBufType;->addElement(IILjava/lang/Object;)Lcom/google/gmm/common/io/protocol/ProtoBufType;

    move-result-object v0

    const/16 v1, 0x21c

    invoke-virtual {v0, v1, v9, v6}, Lcom/google/gmm/common/io/protocol/ProtoBufType;->addElement(IILjava/lang/Object;)Lcom/google/gmm/common/io/protocol/ProtoBufType;

    sget-object v0, Lcom/google/android/location/protocol/LocserverMessageTypes;->GLAT_LNG:Lcom/google/gmm/common/io/protocol/ProtoBufType;

    const/16 v1, 0x117

    invoke-virtual {v0, v1, v8, v6}, Lcom/google/gmm/common/io/protocol/ProtoBufType;->addElement(IILjava/lang/Object;)Lcom/google/gmm/common/io/protocol/ProtoBufType;

    move-result-object v0

    const/16 v1, 0x117

    invoke-virtual {v0, v1, v9, v6}, Lcom/google/gmm/common/io/protocol/ProtoBufType;->addElement(IILjava/lang/Object;)Lcom/google/gmm/common/io/protocol/ProtoBufType;

    sget-object v0, Lcom/google/android/location/protocol/LocserverMessageTypes;->GADDRESS_COMPONENT:Lcom/google/gmm/common/io/protocol/ProtoBufType;

    const/16 v1, 0x11c

    invoke-virtual {v0, v1, v8, v6}, Lcom/google/gmm/common/io/protocol/ProtoBufType;->addElement(IILjava/lang/Object;)Lcom/google/gmm/common/io/protocol/ProtoBufType;

    move-result-object v0

    const/16 v1, 0x115

    invoke-virtual {v0, v1, v9, v6}, Lcom/google/gmm/common/io/protocol/ProtoBufType;->addElement(IILjava/lang/Object;)Lcom/google/gmm/common/io/protocol/ProtoBufType;

    move-result-object v0

    const/16 v1, 0x21c

    const/4 v2, 0x3

    invoke-virtual {v0, v1, v2, v6}, Lcom/google/gmm/common/io/protocol/ProtoBufType;->addElement(IILjava/lang/Object;)Lcom/google/gmm/common/io/protocol/ProtoBufType;

    sget-object v0, Lcom/google/android/location/protocol/LocserverMessageTypes;->GGPS_PROFILE:Lcom/google/gmm/common/io/protocol/ProtoBufType;

    invoke-virtual {v0, v7, v8, v6}, Lcom/google/gmm/common/io/protocol/ProtoBufType;->addElement(IILjava/lang/Object;)Lcom/google/gmm/common/io/protocol/ProtoBufType;

    move-result-object v0

    invoke-virtual {v0, v7, v9, v6}, Lcom/google/gmm/common/io/protocol/ProtoBufType;->addElement(IILjava/lang/Object;)Lcom/google/gmm/common/io/protocol/ProtoBufType;

    move-result-object v0

    const/4 v1, 0x3

    invoke-virtual {v0, v7, v1, v6}, Lcom/google/gmm/common/io/protocol/ProtoBufType;->addElement(IILjava/lang/Object;)Lcom/google/gmm/common/io/protocol/ProtoBufType;

    move-result-object v0

    const/4 v1, 0x4

    invoke-virtual {v0, v7, v1, v6}, Lcom/google/gmm/common/io/protocol/ProtoBufType;->addElement(IILjava/lang/Object;)Lcom/google/gmm/common/io/protocol/ProtoBufType;

    sget-object v0, Lcom/google/android/location/protocol/LocserverMessageTypes;->GRECTANGLE:Lcom/google/gmm/common/io/protocol/ProtoBufType;

    const/16 v1, 0x11b

    invoke-virtual {v0, v1, v8, v6}, Lcom/google/gmm/common/io/protocol/ProtoBufType;->addElement(IILjava/lang/Object;)Lcom/google/gmm/common/io/protocol/ProtoBufType;

    move-result-object v0

    const/16 v1, 0x11b

    invoke-virtual {v0, v1, v9, v6}, Lcom/google/gmm/common/io/protocol/ProtoBufType;->addElement(IILjava/lang/Object;)Lcom/google/gmm/common/io/protocol/ProtoBufType;

    sget-object v0, Lcom/google/android/location/protocol/LocserverMessageTypes;->GMAGNETIC_FIELD_SNAPSHOT:Lcom/google/gmm/common/io/protocol/ProtoBufType;

    const/16 v1, 0x212

    invoke-virtual {v0, v1, v8, v6}, Lcom/google/gmm/common/io/protocol/ProtoBufType;->addElement(IILjava/lang/Object;)Lcom/google/gmm/common/io/protocol/ProtoBufType;

    move-result-object v0

    const/16 v1, 0x212

    invoke-virtual {v0, v1, v9, v6}, Lcom/google/gmm/common/io/protocol/ProtoBufType;->addElement(IILjava/lang/Object;)Lcom/google/gmm/common/io/protocol/ProtoBufType;

    move-result-object v0

    const/16 v1, 0x212

    const/4 v2, 0x3

    invoke-virtual {v0, v1, v2, v6}, Lcom/google/gmm/common/io/protocol/ProtoBufType;->addElement(IILjava/lang/Object;)Lcom/google/gmm/common/io/protocol/ProtoBufType;

    move-result-object v0

    const/4 v1, 0x4

    invoke-virtual {v0, v7, v1, v6}, Lcom/google/gmm/common/io/protocol/ProtoBufType;->addElement(IILjava/lang/Object;)Lcom/google/gmm/common/io/protocol/ProtoBufType;

    move-result-object v0

    const/4 v1, 0x5

    new-instance v2, Ljava/lang/Long;

    const-wide/16 v3, 0x0

    invoke-direct {v2, v3, v4}, Ljava/lang/Long;-><init>(J)V

    invoke-virtual {v0, v7, v1, v2}, Lcom/google/gmm/common/io/protocol/ProtoBufType;->addElement(IILjava/lang/Object;)Lcom/google/gmm/common/io/protocol/ProtoBufType;

    move-result-object v0

    const/4 v1, 0x6

    new-instance v2, Ljava/lang/Long;

    const-wide/16 v3, 0x0

    invoke-direct {v2, v3, v4}, Ljava/lang/Long;-><init>(J)V

    invoke-virtual {v0, v7, v1, v2}, Lcom/google/gmm/common/io/protocol/ProtoBufType;->addElement(IILjava/lang/Object;)Lcom/google/gmm/common/io/protocol/ProtoBufType;

    move-result-object v0

    const/16 v1, 0x212

    const/4 v2, 0x7

    invoke-virtual {v0, v1, v2, v6}, Lcom/google/gmm/common/io/protocol/ProtoBufType;->addElement(IILjava/lang/Object;)Lcom/google/gmm/common/io/protocol/ProtoBufType;

    move-result-object v0

    const/16 v1, 0x8

    new-instance v2, Ljava/lang/Long;

    const-wide/16 v3, 0x0

    invoke-direct {v2, v3, v4}, Ljava/lang/Long;-><init>(J)V

    invoke-virtual {v0, v7, v1, v2}, Lcom/google/gmm/common/io/protocol/ProtoBufType;->addElement(IILjava/lang/Object;)Lcom/google/gmm/common/io/protocol/ProtoBufType;

    move-result-object v0

    const/16 v1, 0x9

    new-instance v2, Ljava/lang/Long;

    const-wide/16 v3, 0x0

    invoke-direct {v2, v3, v4}, Ljava/lang/Long;-><init>(J)V

    invoke-virtual {v0, v7, v1, v2}, Lcom/google/gmm/common/io/protocol/ProtoBufType;->addElement(IILjava/lang/Object;)Lcom/google/gmm/common/io/protocol/ProtoBufType;

    sget-object v0, Lcom/google/android/location/protocol/LocserverMessageTypes;->NLP_TIME_ZONE:Lcom/google/gmm/common/io/protocol/ProtoBufType;

    const/16 v1, 0x21c

    invoke-virtual {v0, v1, v8, v6}, Lcom/google/gmm/common/io/protocol/ProtoBufType;->addElement(IILjava/lang/Object;)Lcom/google/gmm/common/io/protocol/ProtoBufType;

    move-result-object v0

    invoke-virtual {v0, v7, v9, v6}, Lcom/google/gmm/common/io/protocol/ProtoBufType;->addElement(IILjava/lang/Object;)Lcom/google/gmm/common/io/protocol/ProtoBufType;

    sget-object v0, Lcom/google/android/location/protocol/LocserverMessageTypes;->GCLIENT_TOKEN_BUCKET_STATS:Lcom/google/gmm/common/io/protocol/ProtoBufType;

    invoke-virtual {v0, v7, v8, v6}, Lcom/google/gmm/common/io/protocol/ProtoBufType;->addElement(IILjava/lang/Object;)Lcom/google/gmm/common/io/protocol/ProtoBufType;

    move-result-object v0

    invoke-virtual {v0, v7, v9, v6}, Lcom/google/gmm/common/io/protocol/ProtoBufType;->addElement(IILjava/lang/Object;)Lcom/google/gmm/common/io/protocol/ProtoBufType;

    sget-object v0, Lcom/google/android/location/protocol/LocserverMessageTypes;->GLOC_REPLY_ELEMENT:Lcom/google/gmm/common/io/protocol/ProtoBufType;

    const/16 v1, 0x115

    invoke-virtual {v0, v1, v8, v6}, Lcom/google/gmm/common/io/protocol/ProtoBufType;->addElement(IILjava/lang/Object;)Lcom/google/gmm/common/io/protocol/ProtoBufType;

    move-result-object v0

    invoke-virtual {v0, v10, v9, v6}, Lcom/google/gmm/common/io/protocol/ProtoBufType;->addElement(IILjava/lang/Object;)Lcom/google/gmm/common/io/protocol/ProtoBufType;

    move-result-object v0

    const/16 v1, 0x41b

    const/4 v2, 0x3

    invoke-virtual {v0, v1, v2, v6}, Lcom/google/gmm/common/io/protocol/ProtoBufType;->addElement(IILjava/lang/Object;)Lcom/google/gmm/common/io/protocol/ProtoBufType;

    move-result-object v0

    const/4 v1, 0x4

    invoke-virtual {v0, v7, v1, v6}, Lcom/google/gmm/common/io/protocol/ProtoBufType;->addElement(IILjava/lang/Object;)Lcom/google/gmm/common/io/protocol/ProtoBufType;

    move-result-object v0

    const/4 v1, 0x5

    invoke-virtual {v0, v10, v1, v6}, Lcom/google/gmm/common/io/protocol/ProtoBufType;->addElement(IILjava/lang/Object;)Lcom/google/gmm/common/io/protocol/ProtoBufType;

    move-result-object v0

    const/4 v1, 0x6

    invoke-virtual {v0, v10, v1, v6}, Lcom/google/gmm/common/io/protocol/ProtoBufType;->addElement(IILjava/lang/Object;)Lcom/google/gmm/common/io/protocol/ProtoBufType;

    sget-object v0, Lcom/google/android/location/protocol/LocserverMessageTypes;->GLOC_REPLY:Lcom/google/gmm/common/io/protocol/ProtoBufType;

    const/16 v1, 0x115

    invoke-virtual {v0, v1, v8, v6}, Lcom/google/gmm/common/io/protocol/ProtoBufType;->addElement(IILjava/lang/Object;)Lcom/google/gmm/common/io/protocol/ProtoBufType;

    move-result-object v0

    const/16 v1, 0x41b

    sget-object v2, Lcom/google/android/location/protocol/LocserverMessageTypes;->GLOC_REPLY_ELEMENT:Lcom/google/gmm/common/io/protocol/ProtoBufType;

    invoke-virtual {v0, v1, v9, v2}, Lcom/google/gmm/common/io/protocol/ProtoBufType;->addElement(IILjava/lang/Object;)Lcom/google/gmm/common/io/protocol/ProtoBufType;

    move-result-object v0

    const/16 v1, 0x21c

    const/4 v2, 0x3

    invoke-virtual {v0, v1, v2, v6}, Lcom/google/gmm/common/io/protocol/ProtoBufType;->addElement(IILjava/lang/Object;)Lcom/google/gmm/common/io/protocol/ProtoBufType;

    move-result-object v0

    const/4 v1, 0x4

    sget-object v2, Lcom/google/android/location/protocol/LocserverMessageTypes;->GSESSION_PROFILE:Lcom/google/gmm/common/io/protocol/ProtoBufType;

    invoke-virtual {v0, v10, v1, v2}, Lcom/google/gmm/common/io/protocol/ProtoBufType;->addElement(IILjava/lang/Object;)Lcom/google/gmm/common/io/protocol/ProtoBufType;

    move-result-object v0

    const/4 v1, 0x5

    sget-object v2, Lcom/google/android/location/protocol/LocserverMessageTypes;->NLP_PARAMETERS:Lcom/google/gmm/common/io/protocol/ProtoBufType;

    invoke-virtual {v0, v10, v1, v2}, Lcom/google/gmm/common/io/protocol/ProtoBufType;->addElement(IILjava/lang/Object;)Lcom/google/gmm/common/io/protocol/ProtoBufType;

    sget-object v0, Lcom/google/android/location/protocol/LocserverMessageTypes;->GWIFI_PLATFORM_PROFILE:Lcom/google/gmm/common/io/protocol/ProtoBufType;

    const/16 v1, 0x21c

    invoke-virtual {v0, v1, v9, v6}, Lcom/google/gmm/common/io/protocol/ProtoBufType;->addElement(IILjava/lang/Object;)Lcom/google/gmm/common/io/protocol/ProtoBufType;

    move-result-object v0

    const/4 v1, 0x3

    invoke-virtual {v0, v7, v1, v6}, Lcom/google/gmm/common/io/protocol/ProtoBufType;->addElement(IILjava/lang/Object;)Lcom/google/gmm/common/io/protocol/ProtoBufType;

    sget-object v0, Lcom/google/android/location/protocol/LocserverMessageTypes;->GWIFI_PROFILE:Lcom/google/gmm/common/io/protocol/ProtoBufType;

    const/16 v1, 0x113

    invoke-virtual {v0, v1, v8, v6}, Lcom/google/gmm/common/io/protocol/ProtoBufType;->addElement(IILjava/lang/Object;)Lcom/google/gmm/common/io/protocol/ProtoBufType;

    move-result-object v0

    const/16 v1, 0x41b

    sget-object v2, Lcom/google/android/location/protocol/LocserverMessageTypes;->GWIFI_DEVICE:Lcom/google/gmm/common/io/protocol/ProtoBufType;

    invoke-virtual {v0, v1, v9, v2}, Lcom/google/gmm/common/io/protocol/ProtoBufType;->addElement(IILjava/lang/Object;)Lcom/google/gmm/common/io/protocol/ProtoBufType;

    move-result-object v0

    const/16 v1, 0x41b

    const/4 v2, 0x4

    sget-object v3, Lcom/google/android/location/protocol/LocserverMessageTypes;->GGATEWAY_DEVICE:Lcom/google/gmm/common/io/protocol/ProtoBufType;

    invoke-virtual {v0, v1, v2, v3}, Lcom/google/gmm/common/io/protocol/ProtoBufType;->addElement(IILjava/lang/Object;)Lcom/google/gmm/common/io/protocol/ProtoBufType;

    move-result-object v0

    const/4 v1, 0x3

    new-instance v2, Ljava/lang/Long;

    const-wide/16 v3, 0x0

    invoke-direct {v2, v3, v4}, Ljava/lang/Long;-><init>(J)V

    invoke-virtual {v0, v7, v1, v2}, Lcom/google/gmm/common/io/protocol/ProtoBufType;->addElement(IILjava/lang/Object;)Lcom/google/gmm/common/io/protocol/ProtoBufType;

    sget-object v0, Lcom/google/android/location/protocol/LocserverMessageTypes;->GINDOOR_LOCATION:Lcom/google/gmm/common/io/protocol/ProtoBufType;

    const/16 v1, 0x21c

    invoke-virtual {v0, v1, v8, v6}, Lcom/google/gmm/common/io/protocol/ProtoBufType;->addElement(IILjava/lang/Object;)Lcom/google/gmm/common/io/protocol/ProtoBufType;

    move-result-object v0

    const/16 v1, 0x21c

    invoke-virtual {v0, v1, v9, v6}, Lcom/google/gmm/common/io/protocol/ProtoBufType;->addElement(IILjava/lang/Object;)Lcom/google/gmm/common/io/protocol/ProtoBufType;

    move-result-object v0

    const/16 v1, 0x21c

    const/4 v2, 0x3

    invoke-virtual {v0, v1, v2, v6}, Lcom/google/gmm/common/io/protocol/ProtoBufType;->addElement(IILjava/lang/Object;)Lcom/google/gmm/common/io/protocol/ProtoBufType;

    move-result-object v0

    const/16 v1, 0x21c

    const/4 v2, 0x4

    invoke-virtual {v0, v1, v2, v6}, Lcom/google/gmm/common/io/protocol/ProtoBufType;->addElement(IILjava/lang/Object;)Lcom/google/gmm/common/io/protocol/ProtoBufType;

    move-result-object v0

    const/16 v1, 0x21c

    const/4 v2, 0x5

    invoke-virtual {v0, v1, v2, v6}, Lcom/google/gmm/common/io/protocol/ProtoBufType;->addElement(IILjava/lang/Object;)Lcom/google/gmm/common/io/protocol/ProtoBufType;

    sget-object v0, Lcom/google/android/location/protocol/LocserverMessageTypes;->NLP_SEEN_DEVICES_CACHE:Lcom/google/gmm/common/io/protocol/ProtoBufType;

    const/16 v1, 0x213

    invoke-virtual {v0, v1, v8, v6}, Lcom/google/gmm/common/io/protocol/ProtoBufType;->addElement(IILjava/lang/Object;)Lcom/google/gmm/common/io/protocol/ProtoBufType;

    move-result-object v0

    const/16 v1, 0x213

    invoke-virtual {v0, v1, v9, v6}, Lcom/google/gmm/common/io/protocol/ProtoBufType;->addElement(IILjava/lang/Object;)Lcom/google/gmm/common/io/protocol/ProtoBufType;

    move-result-object v0

    const/16 v1, 0x41b

    const/4 v2, 0x3

    sget-object v3, Lcom/google/android/location/protocol/LocserverMessageTypes;->NLP_WIFI_AP_PROFILE:Lcom/google/gmm/common/io/protocol/ProtoBufType;

    invoke-virtual {v0, v1, v2, v3}, Lcom/google/gmm/common/io/protocol/ProtoBufType;->addElement(IILjava/lang/Object;)Lcom/google/gmm/common/io/protocol/ProtoBufType;

    move-result-object v0

    const/16 v1, 0x218

    const/4 v2, 0x4

    sget-object v3, Lcom/google/gmm/common/io/protocol/ProtoBuf;->TRUE:Ljava/lang/Boolean;

    invoke-virtual {v0, v1, v2, v3}, Lcom/google/gmm/common/io/protocol/ProtoBufType;->addElement(IILjava/lang/Object;)Lcom/google/gmm/common/io/protocol/ProtoBufType;

    sget-object v0, Lcom/google/android/location/protocol/LocserverMessageTypes;->NLP_IN_OUTDOOR_CLASSIFIER_RESULT:Lcom/google/gmm/common/io/protocol/ProtoBufType;

    const/16 v1, 0x213

    invoke-virtual {v0, v1, v8, v6}, Lcom/google/gmm/common/io/protocol/ProtoBufType;->addElement(IILjava/lang/Object;)Lcom/google/gmm/common/io/protocol/ProtoBufType;

    move-result-object v0

    invoke-virtual {v0, v7, v9, v6}, Lcom/google/gmm/common/io/protocol/ProtoBufType;->addElement(IILjava/lang/Object;)Lcom/google/gmm/common/io/protocol/ProtoBufType;

    move-result-object v0

    const/16 v1, 0x218

    const/4 v2, 0x3

    sget-object v3, Lcom/google/gmm/common/io/protocol/ProtoBuf;->TRUE:Ljava/lang/Boolean;

    invoke-virtual {v0, v1, v2, v3}, Lcom/google/gmm/common/io/protocol/ProtoBufType;->addElement(IILjava/lang/Object;)Lcom/google/gmm/common/io/protocol/ProtoBufType;

    move-result-object v0

    const/4 v1, 0x4

    invoke-virtual {v0, v7, v1, v6}, Lcom/google/gmm/common/io/protocol/ProtoBufType;->addElement(IILjava/lang/Object;)Lcom/google/gmm/common/io/protocol/ProtoBufType;

    move-result-object v0

    const/16 v1, 0x218

    const/4 v2, 0x5

    sget-object v3, Lcom/google/gmm/common/io/protocol/ProtoBuf;->FALSE:Ljava/lang/Boolean;

    invoke-virtual {v0, v1, v2, v3}, Lcom/google/gmm/common/io/protocol/ProtoBufType;->addElement(IILjava/lang/Object;)Lcom/google/gmm/common/io/protocol/ProtoBufType;

    move-result-object v0

    const/4 v1, 0x6

    invoke-virtual {v0, v7, v1, v6}, Lcom/google/gmm/common/io/protocol/ProtoBufType;->addElement(IILjava/lang/Object;)Lcom/google/gmm/common/io/protocol/ProtoBufType;

    move-result-object v0

    const/4 v1, 0x7

    new-instance v2, Ljava/lang/Long;

    const-wide/16 v3, 0x1

    invoke-direct {v2, v3, v4}, Ljava/lang/Long;-><init>(J)V

    invoke-virtual {v0, v7, v1, v2}, Lcom/google/gmm/common/io/protocol/ProtoBufType;->addElement(IILjava/lang/Object;)Lcom/google/gmm/common/io/protocol/ProtoBufType;

    sget-object v0, Lcom/google/android/location/protocol/LocserverMessageTypes;->GCLIENT_CACHE_STATS:Lcom/google/gmm/common/io/protocol/ProtoBufType;

    invoke-virtual {v0, v7, v8, v6}, Lcom/google/gmm/common/io/protocol/ProtoBufType;->addElement(IILjava/lang/Object;)Lcom/google/gmm/common/io/protocol/ProtoBufType;

    move-result-object v0

    invoke-virtual {v0, v7, v9, v6}, Lcom/google/gmm/common/io/protocol/ProtoBufType;->addElement(IILjava/lang/Object;)Lcom/google/gmm/common/io/protocol/ProtoBufType;

    move-result-object v0

    const/4 v1, 0x3

    invoke-virtual {v0, v7, v1, v6}, Lcom/google/gmm/common/io/protocol/ProtoBufType;->addElement(IILjava/lang/Object;)Lcom/google/gmm/common/io/protocol/ProtoBufType;

    move-result-object v0

    const/4 v1, 0x4

    invoke-virtual {v0, v7, v1, v6}, Lcom/google/gmm/common/io/protocol/ProtoBufType;->addElement(IILjava/lang/Object;)Lcom/google/gmm/common/io/protocol/ProtoBufType;

    move-result-object v0

    const/4 v1, 0x5

    invoke-virtual {v0, v7, v1, v6}, Lcom/google/gmm/common/io/protocol/ProtoBufType;->addElement(IILjava/lang/Object;)Lcom/google/gmm/common/io/protocol/ProtoBufType;

    move-result-object v0

    const/4 v1, 0x6

    invoke-virtual {v0, v7, v1, v6}, Lcom/google/gmm/common/io/protocol/ProtoBufType;->addElement(IILjava/lang/Object;)Lcom/google/gmm/common/io/protocol/ProtoBufType;

    move-result-object v0

    const/4 v1, 0x7

    invoke-virtual {v0, v7, v1, v6}, Lcom/google/gmm/common/io/protocol/ProtoBufType;->addElement(IILjava/lang/Object;)Lcom/google/gmm/common/io/protocol/ProtoBufType;

    sget-object v0, Lcom/google/android/location/protocol/LocserverMessageTypes;->GLOCATION:Lcom/google/gmm/common/io/protocol/ProtoBufType;

    invoke-virtual {v0, v10, v8, v6}, Lcom/google/gmm/common/io/protocol/ProtoBufType;->addElement(IILjava/lang/Object;)Lcom/google/gmm/common/io/protocol/ProtoBufType;

    move-result-object v0

    const/16 v1, 0x21c

    invoke-virtual {v0, v1, v9, v6}, Lcom/google/gmm/common/io/protocol/ProtoBufType;->addElement(IILjava/lang/Object;)Lcom/google/gmm/common/io/protocol/ProtoBufType;

    move-result-object v0

    const/4 v1, 0x3

    invoke-virtual {v0, v7, v1, v6}, Lcom/google/gmm/common/io/protocol/ProtoBufType;->addElement(IILjava/lang/Object;)Lcom/google/gmm/common/io/protocol/ProtoBufType;

    move-result-object v0

    const/4 v1, 0x4

    invoke-virtual {v0, v7, v1, v6}, Lcom/google/gmm/common/io/protocol/ProtoBufType;->addElement(IILjava/lang/Object;)Lcom/google/gmm/common/io/protocol/ProtoBufType;

    move-result-object v0

    const/16 v1, 0x41b

    const/4 v2, 0x5

    invoke-virtual {v0, v1, v2, v6}, Lcom/google/gmm/common/io/protocol/ProtoBufType;->addElement(IILjava/lang/Object;)Lcom/google/gmm/common/io/protocol/ProtoBufType;

    move-result-object v0

    const/16 v1, 0x213

    const/4 v2, 0x6

    invoke-virtual {v0, v1, v2, v6}, Lcom/google/gmm/common/io/protocol/ProtoBufType;->addElement(IILjava/lang/Object;)Lcom/google/gmm/common/io/protocol/ProtoBufType;

    move-result-object v0

    const/16 v1, 0x218

    const/4 v2, 0x7

    invoke-virtual {v0, v1, v2, v6}, Lcom/google/gmm/common/io/protocol/ProtoBufType;->addElement(IILjava/lang/Object;)Lcom/google/gmm/common/io/protocol/ProtoBufType;

    move-result-object v0

    const/16 v1, 0x8

    invoke-virtual {v0, v7, v1, v6}, Lcom/google/gmm/common/io/protocol/ProtoBufType;->addElement(IILjava/lang/Object;)Lcom/google/gmm/common/io/protocol/ProtoBufType;

    move-result-object v0

    const/16 v1, 0x21c

    const/16 v2, 0x9

    invoke-virtual {v0, v1, v2, v6}, Lcom/google/gmm/common/io/protocol/ProtoBufType;->addElement(IILjava/lang/Object;)Lcom/google/gmm/common/io/protocol/ProtoBufType;

    move-result-object v0

    const/16 v1, 0xa

    invoke-virtual {v0, v7, v1, v6}, Lcom/google/gmm/common/io/protocol/ProtoBufType;->addElement(IILjava/lang/Object;)Lcom/google/gmm/common/io/protocol/ProtoBufType;

    move-result-object v0

    const/16 v1, 0xb

    invoke-virtual {v0, v7, v1, v6}, Lcom/google/gmm/common/io/protocol/ProtoBufType;->addElement(IILjava/lang/Object;)Lcom/google/gmm/common/io/protocol/ProtoBufType;

    move-result-object v0

    const/16 v1, 0xc

    invoke-virtual {v0, v7, v1, v6}, Lcom/google/gmm/common/io/protocol/ProtoBufType;->addElement(IILjava/lang/Object;)Lcom/google/gmm/common/io/protocol/ProtoBufType;

    move-result-object v0

    const/16 v1, 0xd

    invoke-virtual {v0, v7, v1, v6}, Lcom/google/gmm/common/io/protocol/ProtoBufType;->addElement(IILjava/lang/Object;)Lcom/google/gmm/common/io/protocol/ProtoBufType;

    move-result-object v0

    const/16 v1, 0xe

    invoke-virtual {v0, v10, v1, v6}, Lcom/google/gmm/common/io/protocol/ProtoBufType;->addElement(IILjava/lang/Object;)Lcom/google/gmm/common/io/protocol/ProtoBufType;

    move-result-object v0

    const/16 v1, 0x21c

    const/16 v2, 0xf

    invoke-virtual {v0, v1, v2, v6}, Lcom/google/gmm/common/io/protocol/ProtoBufType;->addElement(IILjava/lang/Object;)Lcom/google/gmm/common/io/protocol/ProtoBufType;

    move-result-object v0

    const/16 v1, 0x212

    const/16 v2, 0x10

    invoke-virtual {v0, v1, v2, v6}, Lcom/google/gmm/common/io/protocol/ProtoBufType;->addElement(IILjava/lang/Object;)Lcom/google/gmm/common/io/protocol/ProtoBufType;

    move-result-object v0

    const/16 v1, 0x218

    const/16 v2, 0x11

    invoke-virtual {v0, v1, v2, v6}, Lcom/google/gmm/common/io/protocol/ProtoBufType;->addElement(IILjava/lang/Object;)Lcom/google/gmm/common/io/protocol/ProtoBufType;

    move-result-object v0

    const/16 v1, 0x21c

    const/16 v2, 0x12

    invoke-virtual {v0, v1, v2, v6}, Lcom/google/gmm/common/io/protocol/ProtoBufType;->addElement(IILjava/lang/Object;)Lcom/google/gmm/common/io/protocol/ProtoBufType;

    move-result-object v0

    const/16 v1, 0x13

    invoke-virtual {v0, v10, v1, v6}, Lcom/google/gmm/common/io/protocol/ProtoBufType;->addElement(IILjava/lang/Object;)Lcom/google/gmm/common/io/protocol/ProtoBufType;

    move-result-object v0

    const/16 v1, 0x14

    invoke-virtual {v0, v7, v1, v6}, Lcom/google/gmm/common/io/protocol/ProtoBufType;->addElement(IILjava/lang/Object;)Lcom/google/gmm/common/io/protocol/ProtoBufType;

    sget-object v0, Lcom/google/android/location/protocol/LocserverMessageTypes;->NLP_PARAMETERS:Lcom/google/gmm/common/io/protocol/ProtoBufType;

    new-instance v1, Ljava/lang/Long;

    const-wide/16 v2, 0x0

    invoke-direct {v1, v2, v3}, Ljava/lang/Long;-><init>(J)V

    invoke-virtual {v0, v7, v8, v1}, Lcom/google/gmm/common/io/protocol/ProtoBufType;->addElement(IILjava/lang/Object;)Lcom/google/gmm/common/io/protocol/ProtoBufType;

    move-result-object v0

    const/16 v1, 0x218

    sget-object v2, Lcom/google/gmm/common/io/protocol/ProtoBuf;->FALSE:Ljava/lang/Boolean;

    invoke-virtual {v0, v1, v9, v2}, Lcom/google/gmm/common/io/protocol/ProtoBufType;->addElement(IILjava/lang/Object;)Lcom/google/gmm/common/io/protocol/ProtoBufType;

    move-result-object v0

    const/16 v1, 0x214

    const/4 v2, 0x3

    new-instance v3, Ljava/lang/Long;

    const-wide/32 v4, 0x93a80

    invoke-direct {v3, v4, v5}, Ljava/lang/Long;-><init>(J)V

    invoke-virtual {v0, v1, v2, v3}, Lcom/google/gmm/common/io/protocol/ProtoBufType;->addElement(IILjava/lang/Object;)Lcom/google/gmm/common/io/protocol/ProtoBufType;

    move-result-object v0

    const/4 v1, 0x4

    new-instance v2, Ljava/lang/Long;

    const-wide/32 v3, 0x15180

    invoke-direct {v2, v3, v4}, Ljava/lang/Long;-><init>(J)V

    invoke-virtual {v0, v7, v1, v2}, Lcom/google/gmm/common/io/protocol/ProtoBufType;->addElement(IILjava/lang/Object;)Lcom/google/gmm/common/io/protocol/ProtoBufType;

    move-result-object v0

    const/16 v1, 0x218

    const/4 v2, 0x5

    sget-object v3, Lcom/google/gmm/common/io/protocol/ProtoBuf;->FALSE:Ljava/lang/Boolean;

    invoke-virtual {v0, v1, v2, v3}, Lcom/google/gmm/common/io/protocol/ProtoBufType;->addElement(IILjava/lang/Object;)Lcom/google/gmm/common/io/protocol/ProtoBufType;

    move-result-object v0

    const/16 v1, 0x218

    const/4 v2, 0x6

    sget-object v3, Lcom/google/gmm/common/io/protocol/ProtoBuf;->FALSE:Ljava/lang/Boolean;

    invoke-virtual {v0, v1, v2, v3}, Lcom/google/gmm/common/io/protocol/ProtoBufType;->addElement(IILjava/lang/Object;)Lcom/google/gmm/common/io/protocol/ProtoBufType;

    move-result-object v0

    const/16 v1, 0xb

    new-instance v2, Ljava/lang/Long;

    const-wide/16 v3, 0x0

    invoke-direct {v2, v3, v4}, Ljava/lang/Long;-><init>(J)V

    invoke-virtual {v0, v7, v1, v2}, Lcom/google/gmm/common/io/protocol/ProtoBufType;->addElement(IILjava/lang/Object;)Lcom/google/gmm/common/io/protocol/ProtoBufType;

    move-result-object v0

    const/16 v1, 0xc

    new-instance v2, Ljava/lang/Long;

    const-wide/16 v3, 0x0

    invoke-direct {v2, v3, v4}, Ljava/lang/Long;-><init>(J)V

    invoke-virtual {v0, v7, v1, v2}, Lcom/google/gmm/common/io/protocol/ProtoBufType;->addElement(IILjava/lang/Object;)Lcom/google/gmm/common/io/protocol/ProtoBufType;

    move-result-object v0

    const/16 v1, 0xd

    new-instance v2, Ljava/lang/Long;

    const-wide/16 v3, 0x64

    invoke-direct {v2, v3, v4}, Ljava/lang/Long;-><init>(J)V

    invoke-virtual {v0, v7, v1, v2}, Lcom/google/gmm/common/io/protocol/ProtoBufType;->addElement(IILjava/lang/Object;)Lcom/google/gmm/common/io/protocol/ProtoBufType;

    move-result-object v0

    const/16 v1, 0xe

    new-instance v2, Ljava/lang/Long;

    const-wide/16 v3, 0x12c

    invoke-direct {v2, v3, v4}, Ljava/lang/Long;-><init>(J)V

    invoke-virtual {v0, v7, v1, v2}, Lcom/google/gmm/common/io/protocol/ProtoBufType;->addElement(IILjava/lang/Object;)Lcom/google/gmm/common/io/protocol/ProtoBufType;

    move-result-object v0

    const/16 v1, 0xf

    new-instance v2, Ljava/lang/Long;

    const-wide/32 v3, 0x2a300

    invoke-direct {v2, v3, v4}, Ljava/lang/Long;-><init>(J)V

    invoke-virtual {v0, v7, v1, v2}, Lcom/google/gmm/common/io/protocol/ProtoBufType;->addElement(IILjava/lang/Object;)Lcom/google/gmm/common/io/protocol/ProtoBufType;

    move-result-object v0

    const/16 v1, 0x10

    new-instance v2, Ljava/lang/Long;

    const-wide/32 v3, 0x2a300

    invoke-direct {v2, v3, v4}, Ljava/lang/Long;-><init>(J)V

    invoke-virtual {v0, v7, v1, v2}, Lcom/google/gmm/common/io/protocol/ProtoBufType;->addElement(IILjava/lang/Object;)Lcom/google/gmm/common/io/protocol/ProtoBufType;

    move-result-object v0

    const/16 v1, 0x15

    new-instance v2, Ljava/lang/Long;

    const-wide/16 v3, 0x258

    invoke-direct {v2, v3, v4}, Ljava/lang/Long;-><init>(J)V

    invoke-virtual {v0, v7, v1, v2}, Lcom/google/gmm/common/io/protocol/ProtoBufType;->addElement(IILjava/lang/Object;)Lcom/google/gmm/common/io/protocol/ProtoBufType;

    move-result-object v0

    const/16 v1, 0x16

    new-instance v2, Ljava/lang/Long;

    const-wide/32 v3, 0x12c00

    invoke-direct {v2, v3, v4}, Ljava/lang/Long;-><init>(J)V

    invoke-virtual {v0, v7, v1, v2}, Lcom/google/gmm/common/io/protocol/ProtoBufType;->addElement(IILjava/lang/Object;)Lcom/google/gmm/common/io/protocol/ProtoBufType;

    move-result-object v0

    const/16 v1, 0x17

    new-instance v2, Ljava/lang/Long;

    const-wide/16 v3, 0x4ec

    invoke-direct {v2, v3, v4}, Ljava/lang/Long;-><init>(J)V

    invoke-virtual {v0, v7, v1, v2}, Lcom/google/gmm/common/io/protocol/ProtoBufType;->addElement(IILjava/lang/Object;)Lcom/google/gmm/common/io/protocol/ProtoBufType;

    move-result-object v0

    const/16 v1, 0x1f

    new-instance v2, Ljava/lang/Long;

    const-wide/16 v3, 0x258

    invoke-direct {v2, v3, v4}, Ljava/lang/Long;-><init>(J)V

    invoke-virtual {v0, v7, v1, v2}, Lcom/google/gmm/common/io/protocol/ProtoBufType;->addElement(IILjava/lang/Object;)Lcom/google/gmm/common/io/protocol/ProtoBufType;

    move-result-object v0

    const/16 v1, 0x20

    new-instance v2, Ljava/lang/Long;

    const-wide/32 v3, 0x15180

    invoke-direct {v2, v3, v4}, Ljava/lang/Long;-><init>(J)V

    invoke-virtual {v0, v7, v1, v2}, Lcom/google/gmm/common/io/protocol/ProtoBufType;->addElement(IILjava/lang/Object;)Lcom/google/gmm/common/io/protocol/ProtoBufType;

    move-result-object v0

    const/16 v1, 0x22

    new-instance v2, Ljava/lang/Long;

    const-wide/16 v3, 0x3200

    invoke-direct {v2, v3, v4}, Ljava/lang/Long;-><init>(J)V

    invoke-virtual {v0, v7, v1, v2}, Lcom/google/gmm/common/io/protocol/ProtoBufType;->addElement(IILjava/lang/Object;)Lcom/google/gmm/common/io/protocol/ProtoBufType;

    move-result-object v0

    const/16 v1, 0x23

    new-instance v2, Ljava/lang/Long;

    const-wide/16 v3, 0x3840

    invoke-direct {v2, v3, v4}, Ljava/lang/Long;-><init>(J)V

    invoke-virtual {v0, v7, v1, v2}, Lcom/google/gmm/common/io/protocol/ProtoBufType;->addElement(IILjava/lang/Object;)Lcom/google/gmm/common/io/protocol/ProtoBufType;

    move-result-object v0

    const/16 v1, 0x24

    new-instance v2, Ljava/lang/Long;

    const-wide/16 v3, 0x4ec

    invoke-direct {v2, v3, v4}, Ljava/lang/Long;-><init>(J)V

    invoke-virtual {v0, v7, v1, v2}, Lcom/google/gmm/common/io/protocol/ProtoBufType;->addElement(IILjava/lang/Object;)Lcom/google/gmm/common/io/protocol/ProtoBufType;

    move-result-object v0

    const/16 v1, 0x25

    new-instance v2, Ljava/lang/Long;

    const-wide/32 v3, 0x15180

    invoke-direct {v2, v3, v4}, Ljava/lang/Long;-><init>(J)V

    invoke-virtual {v0, v7, v1, v2}, Lcom/google/gmm/common/io/protocol/ProtoBufType;->addElement(IILjava/lang/Object;)Lcom/google/gmm/common/io/protocol/ProtoBufType;

    sget-object v0, Lcom/google/android/location/protocol/LocserverMessageTypes;->NLP_TEMPORAL_STRING_TO_STRING_CACHE:Lcom/google/gmm/common/io/protocol/ProtoBufType;

    const/16 v1, 0x41b

    sget-object v2, Lcom/google/android/location/protocol/LocserverMessageTypes;->NLP_TEMPORAL_STRING_TO_STRING_ENTRY:Lcom/google/gmm/common/io/protocol/ProtoBufType;

    invoke-virtual {v0, v1, v8, v2}, Lcom/google/gmm/common/io/protocol/ProtoBufType;->addElement(IILjava/lang/Object;)Lcom/google/gmm/common/io/protocol/ProtoBufType;

    sget-object v0, Lcom/google/android/location/protocol/LocserverMessageTypes;->GDEBUG_PROFILE:Lcom/google/gmm/common/io/protocol/ProtoBufType;

    invoke-virtual {v0, v7, v8, v6}, Lcom/google/gmm/common/io/protocol/ProtoBufType;->addElement(IILjava/lang/Object;)Lcom/google/gmm/common/io/protocol/ProtoBufType;

    move-result-object v0

    const/16 v1, 0x218

    sget-object v2, Lcom/google/gmm/common/io/protocol/ProtoBuf;->TRUE:Ljava/lang/Boolean;

    invoke-virtual {v0, v1, v9, v2}, Lcom/google/gmm/common/io/protocol/ProtoBufType;->addElement(IILjava/lang/Object;)Lcom/google/gmm/common/io/protocol/ProtoBufType;

    move-result-object v0

    const/16 v1, 0x41b

    const/4 v2, 0x3

    invoke-virtual {v0, v1, v2, v6}, Lcom/google/gmm/common/io/protocol/ProtoBufType;->addElement(IILjava/lang/Object;)Lcom/google/gmm/common/io/protocol/ProtoBufType;

    move-result-object v0

    const/16 v1, 0x218

    const/4 v2, 0x4

    sget-object v3, Lcom/google/gmm/common/io/protocol/ProtoBuf;->FALSE:Ljava/lang/Boolean;

    invoke-virtual {v0, v1, v2, v3}, Lcom/google/gmm/common/io/protocol/ProtoBufType;->addElement(IILjava/lang/Object;)Lcom/google/gmm/common/io/protocol/ProtoBufType;

    move-result-object v0

    const/16 v1, 0x41c

    const/4 v2, 0x5

    invoke-virtual {v0, v1, v2, v6}, Lcom/google/gmm/common/io/protocol/ProtoBufType;->addElement(IILjava/lang/Object;)Lcom/google/gmm/common/io/protocol/ProtoBufType;

    move-result-object v0

    const/16 v1, 0x218

    const/4 v2, 0x6

    sget-object v3, Lcom/google/gmm/common/io/protocol/ProtoBuf;->TRUE:Ljava/lang/Boolean;

    invoke-virtual {v0, v1, v2, v3}, Lcom/google/gmm/common/io/protocol/ProtoBufType;->addElement(IILjava/lang/Object;)Lcom/google/gmm/common/io/protocol/ProtoBufType;

    sget-object v0, Lcom/google/android/location/protocol/LocserverMessageTypes;->GGATEWAY_DEVICE:Lcom/google/gmm/common/io/protocol/ProtoBufType;

    const/16 v1, 0x11c

    invoke-virtual {v0, v1, v8, v6}, Lcom/google/gmm/common/io/protocol/ProtoBufType;->addElement(IILjava/lang/Object;)Lcom/google/gmm/common/io/protocol/ProtoBufType;

    move-result-object v0

    invoke-virtual {v0, v7, v9, v6}, Lcom/google/gmm/common/io/protocol/ProtoBufType;->addElement(IILjava/lang/Object;)Lcom/google/gmm/common/io/protocol/ProtoBufType;

    sget-object v0, Lcom/google/android/location/protocol/LocserverMessageTypes;->GPLATFORM_PROFILE:Lcom/google/gmm/common/io/protocol/ProtoBufType;

    const/16 v1, 0x21c

    invoke-virtual {v0, v1, v8, v6}, Lcom/google/gmm/common/io/protocol/ProtoBufType;->addElement(IILjava/lang/Object;)Lcom/google/gmm/common/io/protocol/ProtoBufType;

    move-result-object v0

    const/16 v1, 0x21c

    invoke-virtual {v0, v1, v9, v6}, Lcom/google/gmm/common/io/protocol/ProtoBufType;->addElement(IILjava/lang/Object;)Lcom/google/gmm/common/io/protocol/ProtoBufType;

    move-result-object v0

    const/16 v1, 0x21c

    const/4 v2, 0x3

    invoke-virtual {v0, v1, v2, v6}, Lcom/google/gmm/common/io/protocol/ProtoBufType;->addElement(IILjava/lang/Object;)Lcom/google/gmm/common/io/protocol/ProtoBufType;

    move-result-object v0

    const/16 v1, 0x21c

    const/4 v2, 0x4

    invoke-virtual {v0, v1, v2, v6}, Lcom/google/gmm/common/io/protocol/ProtoBufType;->addElement(IILjava/lang/Object;)Lcom/google/gmm/common/io/protocol/ProtoBufType;

    move-result-object v0

    const/16 v1, 0x21c

    const/4 v2, 0x5

    invoke-virtual {v0, v1, v2, v6}, Lcom/google/gmm/common/io/protocol/ProtoBufType;->addElement(IILjava/lang/Object;)Lcom/google/gmm/common/io/protocol/ProtoBufType;

    move-result-object v0

    const/4 v1, 0x6

    invoke-virtual {v0, v10, v1, v6}, Lcom/google/gmm/common/io/protocol/ProtoBufType;->addElement(IILjava/lang/Object;)Lcom/google/gmm/common/io/protocol/ProtoBufType;

    move-result-object v0

    const/4 v1, 0x7

    invoke-virtual {v0, v10, v1, v6}, Lcom/google/gmm/common/io/protocol/ProtoBufType;->addElement(IILjava/lang/Object;)Lcom/google/gmm/common/io/protocol/ProtoBufType;

    move-result-object v0

    const/16 v1, 0x21c

    const/16 v2, 0x8

    invoke-virtual {v0, v1, v2, v6}, Lcom/google/gmm/common/io/protocol/ProtoBufType;->addElement(IILjava/lang/Object;)Lcom/google/gmm/common/io/protocol/ProtoBufType;

    move-result-object v0

    const/16 v1, 0x21c

    const/16 v2, 0x9

    invoke-virtual {v0, v1, v2, v6}, Lcom/google/gmm/common/io/protocol/ProtoBufType;->addElement(IILjava/lang/Object;)Lcom/google/gmm/common/io/protocol/ProtoBufType;

    move-result-object v0

    const/16 v1, 0xa

    sget-object v2, Lcom/google/android/location/protocol/LocserverMessageTypes;->GSENSORS_PLATFORM_PROFILE:Lcom/google/gmm/common/io/protocol/ProtoBufType;

    invoke-virtual {v0, v10, v1, v2}, Lcom/google/gmm/common/io/protocol/ProtoBufType;->addElement(IILjava/lang/Object;)Lcom/google/gmm/common/io/protocol/ProtoBufType;

    sget-object v0, Lcom/google/android/location/protocol/LocserverMessageTypes;->GGYROSCOPE_PLATFORM_PROFILE:Lcom/google/gmm/common/io/protocol/ProtoBufType;

    const/16 v1, 0x212

    invoke-virtual {v0, v1, v8, v6}, Lcom/google/gmm/common/io/protocol/ProtoBufType;->addElement(IILjava/lang/Object;)Lcom/google/gmm/common/io/protocol/ProtoBufType;

    move-result-object v0

    const/16 v1, 0x212

    invoke-virtual {v0, v1, v9, v6}, Lcom/google/gmm/common/io/protocol/ProtoBufType;->addElement(IILjava/lang/Object;)Lcom/google/gmm/common/io/protocol/ProtoBufType;

    move-result-object v0

    const/16 v1, 0x212

    const/4 v2, 0x3

    invoke-virtual {v0, v1, v2, v6}, Lcom/google/gmm/common/io/protocol/ProtoBufType;->addElement(IILjava/lang/Object;)Lcom/google/gmm/common/io/protocol/ProtoBufType;

    move-result-object v0

    const/16 v1, 0x213

    const/4 v2, 0x4

    invoke-virtual {v0, v1, v2, v6}, Lcom/google/gmm/common/io/protocol/ProtoBufType;->addElement(IILjava/lang/Object;)Lcom/google/gmm/common/io/protocol/ProtoBufType;

    sget-object v0, Lcom/google/android/location/protocol/LocserverMessageTypes;->GAPP_PROFILE:Lcom/google/gmm/common/io/protocol/ProtoBufType;

    const/16 v1, 0x21c

    invoke-virtual {v0, v1, v8, v6}, Lcom/google/gmm/common/io/protocol/ProtoBufType;->addElement(IILjava/lang/Object;)Lcom/google/gmm/common/io/protocol/ProtoBufType;

    move-result-object v0

    const/16 v1, 0x21c

    invoke-virtual {v0, v1, v9, v6}, Lcom/google/gmm/common/io/protocol/ProtoBufType;->addElement(IILjava/lang/Object;)Lcom/google/gmm/common/io/protocol/ProtoBufType;

    move-result-object v0

    const/4 v1, 0x3

    invoke-virtual {v0, v7, v1, v6}, Lcom/google/gmm/common/io/protocol/ProtoBufType;->addElement(IILjava/lang/Object;)Lcom/google/gmm/common/io/protocol/ProtoBufType;

    move-result-object v0

    const/4 v1, 0x4

    invoke-virtual {v0, v7, v1, v6}, Lcom/google/gmm/common/io/protocol/ProtoBufType;->addElement(IILjava/lang/Object;)Lcom/google/gmm/common/io/protocol/ProtoBufType;

    move-result-object v0

    const/16 v1, 0x21c

    const/4 v2, 0x5

    invoke-virtual {v0, v1, v2, v6}, Lcom/google/gmm/common/io/protocol/ProtoBufType;->addElement(IILjava/lang/Object;)Lcom/google/gmm/common/io/protocol/ProtoBufType;

    move-result-object v0

    const/4 v1, 0x6

    invoke-virtual {v0, v7, v1, v6}, Lcom/google/gmm/common/io/protocol/ProtoBufType;->addElement(IILjava/lang/Object;)Lcom/google/gmm/common/io/protocol/ProtoBufType;

    sget-object v0, Lcom/google/android/location/protocol/LocserverMessageTypes;->GCLIENT_SENSOR_COLLECTION_STATS:Lcom/google/gmm/common/io/protocol/ProtoBufType;

    invoke-virtual {v0, v7, v8, v6}, Lcom/google/gmm/common/io/protocol/ProtoBufType;->addElement(IILjava/lang/Object;)Lcom/google/gmm/common/io/protocol/ProtoBufType;

    move-result-object v0

    new-instance v1, Ljava/lang/Long;

    const-wide/16 v2, 0x1

    invoke-direct {v1, v2, v3}, Ljava/lang/Long;-><init>(J)V

    invoke-virtual {v0, v7, v9, v1}, Lcom/google/gmm/common/io/protocol/ProtoBufType;->addElement(IILjava/lang/Object;)Lcom/google/gmm/common/io/protocol/ProtoBufType;

    move-result-object v0

    const/4 v1, 0x3

    invoke-virtual {v0, v7, v1, v6}, Lcom/google/gmm/common/io/protocol/ProtoBufType;->addElement(IILjava/lang/Object;)Lcom/google/gmm/common/io/protocol/ProtoBufType;

    sget-object v0, Lcom/google/android/location/protocol/LocserverMessageTypes;->GGYROSCOPE_SNAPSHOT:Lcom/google/gmm/common/io/protocol/ProtoBufType;

    const/16 v1, 0x212

    invoke-virtual {v0, v1, v8, v6}, Lcom/google/gmm/common/io/protocol/ProtoBufType;->addElement(IILjava/lang/Object;)Lcom/google/gmm/common/io/protocol/ProtoBufType;

    move-result-object v0

    const/16 v1, 0x212

    invoke-virtual {v0, v1, v9, v6}, Lcom/google/gmm/common/io/protocol/ProtoBufType;->addElement(IILjava/lang/Object;)Lcom/google/gmm/common/io/protocol/ProtoBufType;

    move-result-object v0

    const/16 v1, 0x212

    const/4 v2, 0x3

    invoke-virtual {v0, v1, v2, v6}, Lcom/google/gmm/common/io/protocol/ProtoBufType;->addElement(IILjava/lang/Object;)Lcom/google/gmm/common/io/protocol/ProtoBufType;

    move-result-object v0

    const/4 v1, 0x4

    invoke-virtual {v0, v7, v1, v6}, Lcom/google/gmm/common/io/protocol/ProtoBufType;->addElement(IILjava/lang/Object;)Lcom/google/gmm/common/io/protocol/ProtoBufType;

    move-result-object v0

    const/4 v1, 0x5

    new-instance v2, Ljava/lang/Long;

    const-wide/16 v3, 0x0

    invoke-direct {v2, v3, v4}, Ljava/lang/Long;-><init>(J)V

    invoke-virtual {v0, v7, v1, v2}, Lcom/google/gmm/common/io/protocol/ProtoBufType;->addElement(IILjava/lang/Object;)Lcom/google/gmm/common/io/protocol/ProtoBufType;

    move-result-object v0

    const/4 v1, 0x6

    new-instance v2, Ljava/lang/Long;

    const-wide/16 v3, 0x0

    invoke-direct {v2, v3, v4}, Ljava/lang/Long;-><init>(J)V

    invoke-virtual {v0, v7, v1, v2}, Lcom/google/gmm/common/io/protocol/ProtoBufType;->addElement(IILjava/lang/Object;)Lcom/google/gmm/common/io/protocol/ProtoBufType;

    move-result-object v0

    const/4 v1, 0x7

    new-instance v2, Ljava/lang/Long;

    const-wide/16 v3, 0x0

    invoke-direct {v2, v3, v4}, Ljava/lang/Long;-><init>(J)V

    invoke-virtual {v0, v7, v1, v2}, Lcom/google/gmm/common/io/protocol/ProtoBufType;->addElement(IILjava/lang/Object;)Lcom/google/gmm/common/io/protocol/ProtoBufType;

    move-result-object v0

    const/16 v1, 0x8

    new-instance v2, Ljava/lang/Long;

    const-wide/16 v3, 0x0

    invoke-direct {v2, v3, v4}, Ljava/lang/Long;-><init>(J)V

    invoke-virtual {v0, v7, v1, v2}, Lcom/google/gmm/common/io/protocol/ProtoBufType;->addElement(IILjava/lang/Object;)Lcom/google/gmm/common/io/protocol/ProtoBufType;

    sget-object v0, Lcom/google/android/location/protocol/LocserverMessageTypes;->GSIGNAL_FINGERPRINT_MODEL_REPLY:Lcom/google/gmm/common/io/protocol/ProtoBufType;

    const/16 v1, 0x41b

    sget-object v2, Lcom/google/android/location/protocol/LocserverMessageTypes;->GWIFI_CLUSTER_MEMBERSHIP:Lcom/google/gmm/common/io/protocol/ProtoBufType;

    invoke-virtual {v0, v1, v8, v2}, Lcom/google/gmm/common/io/protocol/ProtoBufType;->addElement(IILjava/lang/Object;)Lcom/google/gmm/common/io/protocol/ProtoBufType;

    move-result-object v0

    sget-object v1, Lcom/google/android/location/protocol/LocserverMessageTypes;->GSIGNAL_FINGERPRINT_MODEL_CLUSTER:Lcom/google/gmm/common/io/protocol/ProtoBufType;

    invoke-virtual {v0, v10, v9, v1}, Lcom/google/gmm/common/io/protocol/ProtoBufType;->addElement(IILjava/lang/Object;)Lcom/google/gmm/common/io/protocol/ProtoBufType;

    move-result-object v0

    const/4 v1, 0x3

    sget-object v2, Lcom/google/android/location/protocol/LocserverMessageTypes;->GSIGNAL_FINGERPRINT_MODEL:Lcom/google/gmm/common/io/protocol/ProtoBufType;

    invoke-virtual {v0, v10, v1, v2}, Lcom/google/gmm/common/io/protocol/ProtoBufType;->addElement(IILjava/lang/Object;)Lcom/google/gmm/common/io/protocol/ProtoBufType;

    sget-object v0, Lcom/google/android/location/protocol/LocserverMessageTypes;->GWIFI_DEVICE:Lcom/google/gmm/common/io/protocol/ProtoBufType;

    const/16 v1, 0x11c

    invoke-virtual {v0, v1, v8, v6}, Lcom/google/gmm/common/io/protocol/ProtoBufType;->addElement(IILjava/lang/Object;)Lcom/google/gmm/common/io/protocol/ProtoBufType;

    move-result-object v0

    const/16 v1, 0x21c

    invoke-virtual {v0, v1, v9, v6}, Lcom/google/gmm/common/io/protocol/ProtoBufType;->addElement(IILjava/lang/Object;)Lcom/google/gmm/common/io/protocol/ProtoBufType;

    move-result-object v0

    const/4 v1, 0x3

    invoke-virtual {v0, v7, v1, v6}, Lcom/google/gmm/common/io/protocol/ProtoBufType;->addElement(IILjava/lang/Object;)Lcom/google/gmm/common/io/protocol/ProtoBufType;

    move-result-object v0

    const/4 v1, 0x4

    invoke-virtual {v0, v7, v1, v6}, Lcom/google/gmm/common/io/protocol/ProtoBufType;->addElement(IILjava/lang/Object;)Lcom/google/gmm/common/io/protocol/ProtoBufType;

    move-result-object v0

    const/4 v1, 0x5

    invoke-virtual {v0, v7, v1, v6}, Lcom/google/gmm/common/io/protocol/ProtoBufType;->addElement(IILjava/lang/Object;)Lcom/google/gmm/common/io/protocol/ProtoBufType;

    move-result-object v0

    const/16 v1, 0x218

    const/4 v2, 0x6

    invoke-virtual {v0, v1, v2, v6}, Lcom/google/gmm/common/io/protocol/ProtoBufType;->addElement(IILjava/lang/Object;)Lcom/google/gmm/common/io/protocol/ProtoBufType;

    move-result-object v0

    const/4 v1, 0x7

    invoke-virtual {v0, v7, v1, v6}, Lcom/google/gmm/common/io/protocol/ProtoBufType;->addElement(IILjava/lang/Object;)Lcom/google/gmm/common/io/protocol/ProtoBufType;

    move-result-object v0

    const/16 v1, 0x213

    const/16 v2, 0x8

    invoke-virtual {v0, v1, v2, v6}, Lcom/google/gmm/common/io/protocol/ProtoBufType;->addElement(IILjava/lang/Object;)Lcom/google/gmm/common/io/protocol/ProtoBufType;

    move-result-object v0

    const/16 v1, 0x221

    const/16 v2, 0x9

    invoke-virtual {v0, v1, v2, v6}, Lcom/google/gmm/common/io/protocol/ProtoBufType;->addElement(IILjava/lang/Object;)Lcom/google/gmm/common/io/protocol/ProtoBufType;

    move-result-object v0

    const/16 v1, 0xa

    invoke-virtual {v0, v7, v1, v6}, Lcom/google/gmm/common/io/protocol/ProtoBufType;->addElement(IILjava/lang/Object;)Lcom/google/gmm/common/io/protocol/ProtoBufType;

    sget-object v0, Lcom/google/android/location/protocol/LocserverMessageTypes;->NLP_IN_OUTDOOR_TRANSITION_TIME:Lcom/google/gmm/common/io/protocol/ProtoBufType;

    const/16 v1, 0x21c

    invoke-virtual {v0, v1, v8, v6}, Lcom/google/gmm/common/io/protocol/ProtoBufType;->addElement(IILjava/lang/Object;)Lcom/google/gmm/common/io/protocol/ProtoBufType;

    move-result-object v0

    const/16 v1, 0x41b

    sget-object v2, Lcom/google/android/location/protocol/LocserverMessageTypes;->NLP_TIME_SPAN:Lcom/google/gmm/common/io/protocol/ProtoBufType;

    invoke-virtual {v0, v1, v9, v2}, Lcom/google/gmm/common/io/protocol/ProtoBufType;->addElement(IILjava/lang/Object;)Lcom/google/gmm/common/io/protocol/ProtoBufType;

    sget-object v0, Lcom/google/android/location/protocol/LocserverMessageTypes;->GCELL:Lcom/google/gmm/common/io/protocol/ProtoBufType;

    const/16 v1, 0x115

    invoke-virtual {v0, v1, v8, v6}, Lcom/google/gmm/common/io/protocol/ProtoBufType;->addElement(IILjava/lang/Object;)Lcom/google/gmm/common/io/protocol/ProtoBufType;

    move-result-object v0

    const/16 v1, 0x115

    invoke-virtual {v0, v1, v9, v6}, Lcom/google/gmm/common/io/protocol/ProtoBufType;->addElement(IILjava/lang/Object;)Lcom/google/gmm/common/io/protocol/ProtoBufType;

    move-result-object v0

    const/4 v1, 0x3

    new-instance v2, Ljava/lang/Long;

    const-wide/16 v3, -0x1

    invoke-direct {v2, v3, v4}, Ljava/lang/Long;-><init>(J)V

    invoke-virtual {v0, v7, v1, v2}, Lcom/google/gmm/common/io/protocol/ProtoBufType;->addElement(IILjava/lang/Object;)Lcom/google/gmm/common/io/protocol/ProtoBufType;

    move-result-object v0

    const/4 v1, 0x4

    new-instance v2, Ljava/lang/Long;

    const-wide/16 v3, -0x1

    invoke-direct {v2, v3, v4}, Ljava/lang/Long;-><init>(J)V

    invoke-virtual {v0, v7, v1, v2}, Lcom/google/gmm/common/io/protocol/ProtoBufType;->addElement(IILjava/lang/Object;)Lcom/google/gmm/common/io/protocol/ProtoBufType;

    move-result-object v0

    const/4 v1, 0x5

    new-instance v2, Ljava/lang/Long;

    const-wide/16 v3, -0x270f

    invoke-direct {v2, v3, v4}, Ljava/lang/Long;-><init>(J)V

    invoke-virtual {v0, v7, v1, v2}, Lcom/google/gmm/common/io/protocol/ProtoBufType;->addElement(IILjava/lang/Object;)Lcom/google/gmm/common/io/protocol/ProtoBufType;

    move-result-object v0

    const/4 v1, 0x6

    new-instance v2, Ljava/lang/Long;

    const-wide/16 v3, 0x0

    invoke-direct {v2, v3, v4}, Ljava/lang/Long;-><init>(J)V

    invoke-virtual {v0, v7, v1, v2}, Lcom/google/gmm/common/io/protocol/ProtoBufType;->addElement(IILjava/lang/Object;)Lcom/google/gmm/common/io/protocol/ProtoBufType;

    move-result-object v0

    const/4 v1, 0x7

    new-instance v2, Ljava/lang/Long;

    const-wide/16 v3, -0x1

    invoke-direct {v2, v3, v4}, Ljava/lang/Long;-><init>(J)V

    invoke-virtual {v0, v7, v1, v2}, Lcom/google/gmm/common/io/protocol/ProtoBufType;->addElement(IILjava/lang/Object;)Lcom/google/gmm/common/io/protocol/ProtoBufType;

    move-result-object v0

    const/16 v1, 0x8

    new-instance v2, Ljava/lang/Long;

    const-wide/16 v3, -0x1

    invoke-direct {v2, v3, v4}, Ljava/lang/Long;-><init>(J)V

    invoke-virtual {v0, v7, v1, v2}, Lcom/google/gmm/common/io/protocol/ProtoBufType;->addElement(IILjava/lang/Object;)Lcom/google/gmm/common/io/protocol/ProtoBufType;

    move-result-object v0

    const/16 v1, 0x9

    invoke-virtual {v0, v10, v1, v6}, Lcom/google/gmm/common/io/protocol/ProtoBufType;->addElement(IILjava/lang/Object;)Lcom/google/gmm/common/io/protocol/ProtoBufType;

    move-result-object v0

    const/16 v1, 0xa

    invoke-virtual {v0, v7, v1, v6}, Lcom/google/gmm/common/io/protocol/ProtoBufType;->addElement(IILjava/lang/Object;)Lcom/google/gmm/common/io/protocol/ProtoBufType;

    sget-object v0, Lcom/google/android/location/protocol/LocserverMessageTypes;->GACCELEROMETER_SNAPSHOT:Lcom/google/gmm/common/io/protocol/ProtoBufType;

    const/16 v1, 0x212

    invoke-virtual {v0, v1, v8, v6}, Lcom/google/gmm/common/io/protocol/ProtoBufType;->addElement(IILjava/lang/Object;)Lcom/google/gmm/common/io/protocol/ProtoBufType;

    move-result-object v0

    const/16 v1, 0x212

    invoke-virtual {v0, v1, v9, v6}, Lcom/google/gmm/common/io/protocol/ProtoBufType;->addElement(IILjava/lang/Object;)Lcom/google/gmm/common/io/protocol/ProtoBufType;

    move-result-object v0

    const/16 v1, 0x212

    const/4 v2, 0x3

    invoke-virtual {v0, v1, v2, v6}, Lcom/google/gmm/common/io/protocol/ProtoBufType;->addElement(IILjava/lang/Object;)Lcom/google/gmm/common/io/protocol/ProtoBufType;

    move-result-object v0

    const/4 v1, 0x4

    invoke-virtual {v0, v7, v1, v6}, Lcom/google/gmm/common/io/protocol/ProtoBufType;->addElement(IILjava/lang/Object;)Lcom/google/gmm/common/io/protocol/ProtoBufType;

    move-result-object v0

    const/4 v1, 0x5

    new-instance v2, Ljava/lang/Long;

    const-wide/16 v3, 0x0

    invoke-direct {v2, v3, v4}, Ljava/lang/Long;-><init>(J)V

    invoke-virtual {v0, v7, v1, v2}, Lcom/google/gmm/common/io/protocol/ProtoBufType;->addElement(IILjava/lang/Object;)Lcom/google/gmm/common/io/protocol/ProtoBufType;

    move-result-object v0

    const/4 v1, 0x6

    new-instance v2, Ljava/lang/Long;

    const-wide/16 v3, 0x0

    invoke-direct {v2, v3, v4}, Ljava/lang/Long;-><init>(J)V

    invoke-virtual {v0, v7, v1, v2}, Lcom/google/gmm/common/io/protocol/ProtoBufType;->addElement(IILjava/lang/Object;)Lcom/google/gmm/common/io/protocol/ProtoBufType;

    move-result-object v0

    const/4 v1, 0x7

    new-instance v2, Ljava/lang/Long;

    const-wide/16 v3, 0x0

    invoke-direct {v2, v3, v4}, Ljava/lang/Long;-><init>(J)V

    invoke-virtual {v0, v7, v1, v2}, Lcom/google/gmm/common/io/protocol/ProtoBufType;->addElement(IILjava/lang/Object;)Lcom/google/gmm/common/io/protocol/ProtoBufType;

    move-result-object v0

    const/16 v1, 0x8

    new-instance v2, Ljava/lang/Long;

    const-wide/16 v3, 0x0

    invoke-direct {v2, v3, v4}, Ljava/lang/Long;-><init>(J)V

    invoke-virtual {v0, v7, v1, v2}, Lcom/google/gmm/common/io/protocol/ProtoBufType;->addElement(IILjava/lang/Object;)Lcom/google/gmm/common/io/protocol/ProtoBufType;

    sget-object v0, Lcom/google/android/location/protocol/LocserverMessageTypes;->GSESSION_PROFILE:Lcom/google/gmm/common/io/protocol/ProtoBufType;

    const/16 v1, 0x213

    invoke-virtual {v0, v1, v8, v6}, Lcom/google/gmm/common/io/protocol/ProtoBufType;->addElement(IILjava/lang/Object;)Lcom/google/gmm/common/io/protocol/ProtoBufType;

    move-result-object v0

    const/16 v1, 0x21c

    invoke-virtual {v0, v1, v9, v6}, Lcom/google/gmm/common/io/protocol/ProtoBufType;->addElement(IILjava/lang/Object;)Lcom/google/gmm/common/io/protocol/ProtoBufType;

    move-result-object v0

    const/4 v1, 0x3

    invoke-virtual {v0, v7, v1, v6}, Lcom/google/gmm/common/io/protocol/ProtoBufType;->addElement(IILjava/lang/Object;)Lcom/google/gmm/common/io/protocol/ProtoBufType;

    move-result-object v0

    const/16 v1, 0x213

    const/4 v2, 0x4

    invoke-virtual {v0, v1, v2, v6}, Lcom/google/gmm/common/io/protocol/ProtoBufType;->addElement(IILjava/lang/Object;)Lcom/google/gmm/common/io/protocol/ProtoBufType;

    sget-object v0, Lcom/google/android/location/protocol/LocserverMessageTypes;->GFEATURE:Lcom/google/gmm/common/io/protocol/ProtoBufType;

    const/16 v1, 0x11c

    invoke-virtual {v0, v1, v8, v6}, Lcom/google/gmm/common/io/protocol/ProtoBufType;->addElement(IILjava/lang/Object;)Lcom/google/gmm/common/io/protocol/ProtoBufType;

    move-result-object v0

    const/16 v1, 0x115

    invoke-virtual {v0, v1, v9, v6}, Lcom/google/gmm/common/io/protocol/ProtoBufType;->addElement(IILjava/lang/Object;)Lcom/google/gmm/common/io/protocol/ProtoBufType;

    move-result-object v0

    const/16 v1, 0x11b

    const/4 v2, 0x3

    invoke-virtual {v0, v1, v2, v6}, Lcom/google/gmm/common/io/protocol/ProtoBufType;->addElement(IILjava/lang/Object;)Lcom/google/gmm/common/io/protocol/ProtoBufType;

    move-result-object v0

    const/4 v1, 0x4

    invoke-virtual {v0, v10, v1, v6}, Lcom/google/gmm/common/io/protocol/ProtoBufType;->addElement(IILjava/lang/Object;)Lcom/google/gmm/common/io/protocol/ProtoBufType;

    move-result-object v0

    const/4 v1, 0x5

    invoke-virtual {v0, v10, v1, v6}, Lcom/google/gmm/common/io/protocol/ProtoBufType;->addElement(IILjava/lang/Object;)Lcom/google/gmm/common/io/protocol/ProtoBufType;

    move-result-object v0

    const/16 v1, 0x21c

    const/4 v2, 0x6

    invoke-virtual {v0, v1, v2, v6}, Lcom/google/gmm/common/io/protocol/ProtoBufType;->addElement(IILjava/lang/Object;)Lcom/google/gmm/common/io/protocol/ProtoBufType;

    sget-object v0, Lcom/google/android/location/protocol/LocserverMessageTypes;->NLP_IN_OUTDOOR_HISTORY:Lcom/google/gmm/common/io/protocol/ProtoBufType;

    const/16 v1, 0x41b

    sget-object v2, Lcom/google/android/location/protocol/LocserverMessageTypes;->NLP_TIME_ZONE:Lcom/google/gmm/common/io/protocol/ProtoBufType;

    invoke-virtual {v0, v1, v8, v2}, Lcom/google/gmm/common/io/protocol/ProtoBufType;->addElement(IILjava/lang/Object;)Lcom/google/gmm/common/io/protocol/ProtoBufType;

    move-result-object v0

    const/16 v1, 0x41b

    sget-object v2, Lcom/google/android/location/protocol/LocserverMessageTypes;->NLP_IN_OUTDOOR_CLASSIFIER_RESULT:Lcom/google/gmm/common/io/protocol/ProtoBufType;

    invoke-virtual {v0, v1, v9, v2}, Lcom/google/gmm/common/io/protocol/ProtoBufType;->addElement(IILjava/lang/Object;)Lcom/google/gmm/common/io/protocol/ProtoBufType;

    sget-object v0, Lcom/google/android/location/protocol/LocserverMessageTypes;->NLP_TEMPORAL_LONG_TO_STRING_ENTRY:Lcom/google/gmm/common/io/protocol/ProtoBufType;

    const/16 v1, 0x213

    invoke-virtual {v0, v1, v8, v6}, Lcom/google/gmm/common/io/protocol/ProtoBufType;->addElement(IILjava/lang/Object;)Lcom/google/gmm/common/io/protocol/ProtoBufType;

    move-result-object v0

    const/16 v1, 0x21c

    invoke-virtual {v0, v1, v9, v6}, Lcom/google/gmm/common/io/protocol/ProtoBufType;->addElement(IILjava/lang/Object;)Lcom/google/gmm/common/io/protocol/ProtoBufType;

    move-result-object v0

    const/16 v1, 0x213

    const/4 v2, 0x3

    invoke-virtual {v0, v1, v2, v6}, Lcom/google/gmm/common/io/protocol/ProtoBufType;->addElement(IILjava/lang/Object;)Lcom/google/gmm/common/io/protocol/ProtoBufType;

    move-result-object v0

    const/16 v1, 0x213

    const/4 v2, 0x4

    invoke-virtual {v0, v1, v2, v6}, Lcom/google/gmm/common/io/protocol/ProtoBufType;->addElement(IILjava/lang/Object;)Lcom/google/gmm/common/io/protocol/ProtoBufType;

    sget-object v0, Lcom/google/android/location/protocol/LocserverMessageTypes;->GCELLULAR_PLATFORM_PROFILE:Lcom/google/gmm/common/io/protocol/ProtoBufType;

    invoke-virtual {v0, v7, v8, v6}, Lcom/google/gmm/common/io/protocol/ProtoBufType;->addElement(IILjava/lang/Object;)Lcom/google/gmm/common/io/protocol/ProtoBufType;

    move-result-object v0

    const/16 v1, 0x21c

    invoke-virtual {v0, v1, v9, v6}, Lcom/google/gmm/common/io/protocol/ProtoBufType;->addElement(IILjava/lang/Object;)Lcom/google/gmm/common/io/protocol/ProtoBufType;

    move-result-object v0

    const/16 v1, 0x21c

    const/4 v2, 0x3

    invoke-virtual {v0, v1, v2, v6}, Lcom/google/gmm/common/io/protocol/ProtoBufType;->addElement(IILjava/lang/Object;)Lcom/google/gmm/common/io/protocol/ProtoBufType;

    move-result-object v0

    const/4 v1, 0x4

    invoke-virtual {v0, v7, v1, v6}, Lcom/google/gmm/common/io/protocol/ProtoBufType;->addElement(IILjava/lang/Object;)Lcom/google/gmm/common/io/protocol/ProtoBufType;

    move-result-object v0

    const/4 v1, 0x5

    invoke-virtual {v0, v7, v1, v6}, Lcom/google/gmm/common/io/protocol/ProtoBufType;->addElement(IILjava/lang/Object;)Lcom/google/gmm/common/io/protocol/ProtoBufType;

    sget-object v0, Lcom/google/android/location/protocol/LocserverMessageTypes;->NLP_TIME_SPAN:Lcom/google/gmm/common/io/protocol/ProtoBufType;

    invoke-virtual {v0, v7, v8, v6}, Lcom/google/gmm/common/io/protocol/ProtoBufType;->addElement(IILjava/lang/Object;)Lcom/google/gmm/common/io/protocol/ProtoBufType;

    move-result-object v0

    invoke-virtual {v0, v7, v9, v6}, Lcom/google/gmm/common/io/protocol/ProtoBufType;->addElement(IILjava/lang/Object;)Lcom/google/gmm/common/io/protocol/ProtoBufType;

    sget-object v0, Lcom/google/android/location/protocol/LocserverMessageTypes;->NLP_TEMPORAL_STRING_TO_STRING_ENTRY:Lcom/google/gmm/common/io/protocol/ProtoBufType;

    const/16 v1, 0x21c

    invoke-virtual {v0, v1, v8, v6}, Lcom/google/gmm/common/io/protocol/ProtoBufType;->addElement(IILjava/lang/Object;)Lcom/google/gmm/common/io/protocol/ProtoBufType;

    move-result-object v0

    const/16 v1, 0x21c

    invoke-virtual {v0, v1, v9, v6}, Lcom/google/gmm/common/io/protocol/ProtoBufType;->addElement(IILjava/lang/Object;)Lcom/google/gmm/common/io/protocol/ProtoBufType;

    move-result-object v0

    const/16 v1, 0x213

    const/4 v2, 0x3

    invoke-virtual {v0, v1, v2, v6}, Lcom/google/gmm/common/io/protocol/ProtoBufType;->addElement(IILjava/lang/Object;)Lcom/google/gmm/common/io/protocol/ProtoBufType;

    move-result-object v0

    const/16 v1, 0x213

    const/4 v2, 0x4

    invoke-virtual {v0, v1, v2, v6}, Lcom/google/gmm/common/io/protocol/ProtoBufType;->addElement(IILjava/lang/Object;)Lcom/google/gmm/common/io/protocol/ProtoBufType;

    sget-object v0, Lcom/google/android/location/protocol/LocserverMessageTypes;->GCELLULAR_PROFILE:Lcom/google/gmm/common/io/protocol/ProtoBufType;

    const/16 v1, 0x11b

    sget-object v2, Lcom/google/android/location/protocol/LocserverMessageTypes;->GCELL:Lcom/google/gmm/common/io/protocol/ProtoBufType;

    invoke-virtual {v0, v1, v8, v2}, Lcom/google/gmm/common/io/protocol/ProtoBufType;->addElement(IILjava/lang/Object;)Lcom/google/gmm/common/io/protocol/ProtoBufType;

    move-result-object v0

    const/16 v1, 0x113

    invoke-virtual {v0, v1, v9, v6}, Lcom/google/gmm/common/io/protocol/ProtoBufType;->addElement(IILjava/lang/Object;)Lcom/google/gmm/common/io/protocol/ProtoBufType;

    move-result-object v0

    const/16 v1, 0x41b

    const/4 v2, 0x3

    sget-object v3, Lcom/google/android/location/protocol/LocserverMessageTypes;->GCELL:Lcom/google/gmm/common/io/protocol/ProtoBufType;

    invoke-virtual {v0, v1, v2, v3}, Lcom/google/gmm/common/io/protocol/ProtoBufType;->addElement(IILjava/lang/Object;)Lcom/google/gmm/common/io/protocol/ProtoBufType;

    move-result-object v0

    const/16 v1, 0x41b

    const/4 v2, 0x4

    sget-object v3, Lcom/google/android/location/protocol/LocserverMessageTypes;->GCELL:Lcom/google/gmm/common/io/protocol/ProtoBufType;

    invoke-virtual {v0, v1, v2, v3}, Lcom/google/gmm/common/io/protocol/ProtoBufType;->addElement(IILjava/lang/Object;)Lcom/google/gmm/common/io/protocol/ProtoBufType;

    move-result-object v0

    const/4 v1, 0x5

    new-instance v2, Ljava/lang/Long;

    const-wide/16 v3, 0x0

    invoke-direct {v2, v3, v4}, Ljava/lang/Long;-><init>(J)V

    invoke-virtual {v0, v7, v1, v2}, Lcom/google/gmm/common/io/protocol/ProtoBufType;->addElement(IILjava/lang/Object;)Lcom/google/gmm/common/io/protocol/ProtoBufType;

    sget-object v0, Lcom/google/android/location/protocol/LocserverMessageTypes;->NLP_TEMPORAL_LONG_TO_STRING_CACHE:Lcom/google/gmm/common/io/protocol/ProtoBufType;

    const/16 v1, 0x41b

    sget-object v2, Lcom/google/android/location/protocol/LocserverMessageTypes;->NLP_TEMPORAL_LONG_TO_STRING_ENTRY:Lcom/google/gmm/common/io/protocol/ProtoBufType;

    invoke-virtual {v0, v1, v8, v2}, Lcom/google/gmm/common/io/protocol/ProtoBufType;->addElement(IILjava/lang/Object;)Lcom/google/gmm/common/io/protocol/ProtoBufType;

    sget-object v0, Lcom/google/android/location/protocol/LocserverMessageTypes;->GDEVICE_LOCATION:Lcom/google/gmm/common/io/protocol/ProtoBufType;

    invoke-virtual {v0, v10, v8, v6}, Lcom/google/gmm/common/io/protocol/ProtoBufType;->addElement(IILjava/lang/Object;)Lcom/google/gmm/common/io/protocol/ProtoBufType;

    move-result-object v0

    invoke-virtual {v0, v10, v9, v6}, Lcom/google/gmm/common/io/protocol/ProtoBufType;->addElement(IILjava/lang/Object;)Lcom/google/gmm/common/io/protocol/ProtoBufType;

    move-result-object v0

    const/4 v1, 0x3

    invoke-virtual {v0, v10, v1, v6}, Lcom/google/gmm/common/io/protocol/ProtoBufType;->addElement(IILjava/lang/Object;)Lcom/google/gmm/common/io/protocol/ProtoBufType;

    move-result-object v0

    const/16 v1, 0x212

    const/4 v2, 0x4

    invoke-virtual {v0, v1, v2, v6}, Lcom/google/gmm/common/io/protocol/ProtoBufType;->addElement(IILjava/lang/Object;)Lcom/google/gmm/common/io/protocol/ProtoBufType;

    sget-object v0, Lcom/google/android/location/protocol/LocserverMessageTypes;->GCLIENT_SERVER_QUERY_STATS:Lcom/google/gmm/common/io/protocol/ProtoBufType;

    invoke-virtual {v0, v7, v8, v6}, Lcom/google/gmm/common/io/protocol/ProtoBufType;->addElement(IILjava/lang/Object;)Lcom/google/gmm/common/io/protocol/ProtoBufType;

    move-result-object v0

    invoke-virtual {v0, v7, v9, v6}, Lcom/google/gmm/common/io/protocol/ProtoBufType;->addElement(IILjava/lang/Object;)Lcom/google/gmm/common/io/protocol/ProtoBufType;

    move-result-object v0

    const/4 v1, 0x3

    invoke-virtual {v0, v7, v1, v6}, Lcom/google/gmm/common/io/protocol/ProtoBufType;->addElement(IILjava/lang/Object;)Lcom/google/gmm/common/io/protocol/ProtoBufType;

    move-result-object v0

    const/4 v1, 0x4

    invoke-virtual {v0, v7, v1, v6}, Lcom/google/gmm/common/io/protocol/ProtoBufType;->addElement(IILjava/lang/Object;)Lcom/google/gmm/common/io/protocol/ProtoBufType;

    sget-object v0, Lcom/google/android/location/protocol/LocserverMessageTypes;->GCLIENT_COLLECTOR_STATS:Lcom/google/gmm/common/io/protocol/ProtoBufType;

    const/16 v1, 0x41b

    sget-object v2, Lcom/google/android/location/protocol/LocserverMessageTypes;->GCLIENT_SENSOR_COLLECTION_STATS:Lcom/google/gmm/common/io/protocol/ProtoBufType;

    invoke-virtual {v0, v1, v8, v2}, Lcom/google/gmm/common/io/protocol/ProtoBufType;->addElement(IILjava/lang/Object;)Lcom/google/gmm/common/io/protocol/ProtoBufType;

    sget-object v0, Lcom/google/android/location/protocol/LocserverMessageTypes;->GBAROMETER_SNAPSHOT:Lcom/google/gmm/common/io/protocol/ProtoBufType;

    const/16 v1, 0x212

    invoke-virtual {v0, v1, v8, v6}, Lcom/google/gmm/common/io/protocol/ProtoBufType;->addElement(IILjava/lang/Object;)Lcom/google/gmm/common/io/protocol/ProtoBufType;

    move-result-object v0

    invoke-virtual {v0, v7, v9, v6}, Lcom/google/gmm/common/io/protocol/ProtoBufType;->addElement(IILjava/lang/Object;)Lcom/google/gmm/common/io/protocol/ProtoBufType;

    move-result-object v0

    const/4 v1, 0x3

    new-instance v2, Ljava/lang/Long;

    const-wide/16 v3, 0x0

    invoke-direct {v2, v3, v4}, Ljava/lang/Long;-><init>(J)V

    invoke-virtual {v0, v7, v1, v2}, Lcom/google/gmm/common/io/protocol/ProtoBufType;->addElement(IILjava/lang/Object;)Lcom/google/gmm/common/io/protocol/ProtoBufType;

    move-result-object v0

    const/4 v1, 0x4

    new-instance v2, Ljava/lang/Long;

    const-wide/16 v3, 0x0

    invoke-direct {v2, v3, v4}, Ljava/lang/Long;-><init>(J)V

    invoke-virtual {v0, v7, v1, v2}, Lcom/google/gmm/common/io/protocol/ProtoBufType;->addElement(IILjava/lang/Object;)Lcom/google/gmm/common/io/protocol/ProtoBufType;

    sget-object v0, Lcom/google/android/location/protocol/LocserverMessageTypes;->GORIENTATION_SNAPSHOT:Lcom/google/gmm/common/io/protocol/ProtoBufType;

    const/16 v1, 0x212

    invoke-virtual {v0, v1, v8, v6}, Lcom/google/gmm/common/io/protocol/ProtoBufType;->addElement(IILjava/lang/Object;)Lcom/google/gmm/common/io/protocol/ProtoBufType;

    move-result-object v0

    const/16 v1, 0x212

    invoke-virtual {v0, v1, v9, v6}, Lcom/google/gmm/common/io/protocol/ProtoBufType;->addElement(IILjava/lang/Object;)Lcom/google/gmm/common/io/protocol/ProtoBufType;

    move-result-object v0

    const/16 v1, 0x212

    const/4 v2, 0x3

    invoke-virtual {v0, v1, v2, v6}, Lcom/google/gmm/common/io/protocol/ProtoBufType;->addElement(IILjava/lang/Object;)Lcom/google/gmm/common/io/protocol/ProtoBufType;

    move-result-object v0

    const/4 v1, 0x4

    invoke-virtual {v0, v7, v1, v6}, Lcom/google/gmm/common/io/protocol/ProtoBufType;->addElement(IILjava/lang/Object;)Lcom/google/gmm/common/io/protocol/ProtoBufType;

    move-result-object v0

    const/4 v1, 0x5

    new-instance v2, Ljava/lang/Long;

    const-wide/16 v3, 0x0

    invoke-direct {v2, v3, v4}, Ljava/lang/Long;-><init>(J)V

    invoke-virtual {v0, v7, v1, v2}, Lcom/google/gmm/common/io/protocol/ProtoBufType;->addElement(IILjava/lang/Object;)Lcom/google/gmm/common/io/protocol/ProtoBufType;

    move-result-object v0

    const/4 v1, 0x6

    new-instance v2, Ljava/lang/Long;

    const-wide/16 v3, 0x0

    invoke-direct {v2, v3, v4}, Ljava/lang/Long;-><init>(J)V

    invoke-virtual {v0, v7, v1, v2}, Lcom/google/gmm/common/io/protocol/ProtoBufType;->addElement(IILjava/lang/Object;)Lcom/google/gmm/common/io/protocol/ProtoBufType;

    move-result-object v0

    const/4 v1, 0x7

    new-instance v2, Ljava/lang/Long;

    const-wide/16 v3, 0x0

    invoke-direct {v2, v3, v4}, Ljava/lang/Long;-><init>(J)V

    invoke-virtual {v0, v7, v1, v2}, Lcom/google/gmm/common/io/protocol/ProtoBufType;->addElement(IILjava/lang/Object;)Lcom/google/gmm/common/io/protocol/ProtoBufType;

    move-result-object v0

    const/16 v1, 0x8

    new-instance v2, Ljava/lang/Long;

    const-wide/16 v3, 0x0

    invoke-direct {v2, v3, v4}, Ljava/lang/Long;-><init>(J)V

    invoke-virtual {v0, v7, v1, v2}, Lcom/google/gmm/common/io/protocol/ProtoBufType;->addElement(IILjava/lang/Object;)Lcom/google/gmm/common/io/protocol/ProtoBufType;

    sget-object v0, Lcom/google/android/location/protocol/LocserverMessageTypes;->GGPS_STATUS_SNAPSHOT:Lcom/google/gmm/common/io/protocol/ProtoBufType;

    invoke-virtual {v0, v7, v8, v6}, Lcom/google/gmm/common/io/protocol/ProtoBufType;->addElement(IILjava/lang/Object;)Lcom/google/gmm/common/io/protocol/ProtoBufType;

    move-result-object v0

    const/16 v1, 0x41b

    sget-object v2, Lcom/google/android/location/protocol/LocserverMessageTypes;->GGPS_SATELLITE:Lcom/google/gmm/common/io/protocol/ProtoBufType;

    invoke-virtual {v0, v1, v9, v2}, Lcom/google/gmm/common/io/protocol/ProtoBufType;->addElement(IILjava/lang/Object;)Lcom/google/gmm/common/io/protocol/ProtoBufType;

    sget-object v0, Lcom/google/android/location/protocol/LocserverMessageTypes;->GLOC_REQUEST:Lcom/google/gmm/common/io/protocol/ProtoBufType;

    invoke-virtual {v0, v10, v8, v6}, Lcom/google/gmm/common/io/protocol/ProtoBufType;->addElement(IILjava/lang/Object;)Lcom/google/gmm/common/io/protocol/ProtoBufType;

    move-result-object v0

    const/16 v1, 0x41b

    invoke-virtual {v0, v1, v9, v6}, Lcom/google/gmm/common/io/protocol/ProtoBufType;->addElement(IILjava/lang/Object;)Lcom/google/gmm/common/io/protocol/ProtoBufType;

    move-result-object v0

    const/4 v1, 0x3

    invoke-virtual {v0, v10, v1, v6}, Lcom/google/gmm/common/io/protocol/ProtoBufType;->addElement(IILjava/lang/Object;)Lcom/google/gmm/common/io/protocol/ProtoBufType;

    move-result-object v0

    const/16 v1, 0x41b

    const/4 v2, 0x4

    invoke-virtual {v0, v1, v2, v6}, Lcom/google/gmm/common/io/protocol/ProtoBufType;->addElement(IILjava/lang/Object;)Lcom/google/gmm/common/io/protocol/ProtoBufType;

    move-result-object v0

    const/16 v1, 0x41b

    const/4 v2, 0x5

    invoke-virtual {v0, v1, v2, v6}, Lcom/google/gmm/common/io/protocol/ProtoBufType;->addElement(IILjava/lang/Object;)Lcom/google/gmm/common/io/protocol/ProtoBufType;

    move-result-object v0

    const/4 v1, 0x6

    invoke-virtual {v0, v10, v1, v6}, Lcom/google/gmm/common/io/protocol/ProtoBufType;->addElement(IILjava/lang/Object;)Lcom/google/gmm/common/io/protocol/ProtoBufType;

    move-result-object v0

    const/4 v1, 0x7

    invoke-virtual {v0, v7, v1, v6}, Lcom/google/gmm/common/io/protocol/ProtoBufType;->addElement(IILjava/lang/Object;)Lcom/google/gmm/common/io/protocol/ProtoBufType;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
