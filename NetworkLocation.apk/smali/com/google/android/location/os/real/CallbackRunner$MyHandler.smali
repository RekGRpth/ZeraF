.class final Lcom/google/android/location/os/real/CallbackRunner$MyHandler;
.super Landroid/os/Handler;
.source "CallbackRunner.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/location/os/real/CallbackRunner;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x12
    name = "MyHandler"
.end annotation


# instance fields
.field private final connectivityManager:Landroid/net/ConnectivityManager;

.field private lastSignalStrength:I

.field private final telephonyManager:Landroid/telephony/TelephonyManager;

.field final synthetic this$0:Lcom/google/android/location/os/real/CallbackRunner;


# direct methods
.method private constructor <init>(Lcom/google/android/location/os/real/CallbackRunner;)V
    .locals 2

    iput-object p1, p0, Lcom/google/android/location/os/real/CallbackRunner$MyHandler;->this$0:Lcom/google/android/location/os/real/CallbackRunner;

    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    iget-object v0, p0, Lcom/google/android/location/os/real/CallbackRunner$MyHandler;->this$0:Lcom/google/android/location/os/real/CallbackRunner;

    # getter for: Lcom/google/android/location/os/real/CallbackRunner;->context:Landroid/content/Context;
    invoke-static {v0}, Lcom/google/android/location/os/real/CallbackRunner;->access$000(Lcom/google/android/location/os/real/CallbackRunner;)Landroid/content/Context;

    move-result-object v0

    const-string v1, "phone"

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/telephony/TelephonyManager;

    iput-object v0, p0, Lcom/google/android/location/os/real/CallbackRunner$MyHandler;->telephonyManager:Landroid/telephony/TelephonyManager;

    iget-object v0, p0, Lcom/google/android/location/os/real/CallbackRunner$MyHandler;->this$0:Lcom/google/android/location/os/real/CallbackRunner;

    # getter for: Lcom/google/android/location/os/real/CallbackRunner;->context:Landroid/content/Context;
    invoke-static {v0}, Lcom/google/android/location/os/real/CallbackRunner;->access$000(Lcom/google/android/location/os/real/CallbackRunner;)Landroid/content/Context;

    move-result-object v0

    const-string v1, "connectivity"

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/net/ConnectivityManager;

    iput-object v0, p0, Lcom/google/android/location/os/real/CallbackRunner$MyHandler;->connectivityManager:Landroid/net/ConnectivityManager;

    const/16 v0, -0x270f

    iput v0, p0, Lcom/google/android/location/os/real/CallbackRunner$MyHandler;->lastSignalStrength:I

    return-void
.end method

.method synthetic constructor <init>(Lcom/google/android/location/os/real/CallbackRunner;Lcom/google/android/location/os/real/CallbackRunner$1;)V
    .locals 0
    .param p1    # Lcom/google/android/location/os/real/CallbackRunner;
    .param p2    # Lcom/google/android/location/os/real/CallbackRunner$1;

    invoke-direct {p0, p1}, Lcom/google/android/location/os/real/CallbackRunner$MyHandler;-><init>(Lcom/google/android/location/os/real/CallbackRunner;)V

    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 14
    .param p1    # Landroid/os/Message;

    iget v1, p1, Landroid/os/Message;->what:I

    packed-switch v1, :pswitch_data_0

    :pswitch_0
    const-string v1, "NetworkLocationCallbackRunner"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "unexpected message "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget v4, p1, Landroid/os/Message;->what:I

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v1, v3}, Lcom/google/android/location/utils/logging/Logger;->e(Ljava/lang/String;Ljava/lang/String;)V

    :goto_0
    return-void

    :pswitch_1
    iget-object v1, p0, Lcom/google/android/location/os/real/CallbackRunner$MyHandler;->this$0:Lcom/google/android/location/os/real/CallbackRunner;

    # getter for: Lcom/google/android/location/os/real/CallbackRunner;->eventLog:Lcom/google/android/location/os/EventLog;
    invoke-static {v1}, Lcom/google/android/location/os/real/CallbackRunner;->access$100(Lcom/google/android/location/os/real/CallbackRunner;)Lcom/google/android/location/os/EventLog;

    move-result-object v1

    sget-object v3, Lcom/google/android/location/os/Event;->QUIT:Lcom/google/android/location/os/Event;

    invoke-virtual {v1, v3}, Lcom/google/android/location/os/EventLog;->addEvent(Lcom/google/android/location/os/Event;)V

    iget-object v1, p0, Lcom/google/android/location/os/real/CallbackRunner$MyHandler;->this$0:Lcom/google/android/location/os/real/CallbackRunner;

    # getter for: Lcom/google/android/location/os/real/CallbackRunner;->callbacks:Lcom/google/android/location/os/Callbacks;
    invoke-static {v1}, Lcom/google/android/location/os/real/CallbackRunner;->access$200(Lcom/google/android/location/os/real/CallbackRunner;)Lcom/google/android/location/os/Callbacks;

    move-result-object v3

    iget v1, p1, Landroid/os/Message;->arg1:I

    if-eqz v1, :cond_0

    const/4 v1, 0x1

    :goto_1
    invoke-interface {v3, v1}, Lcom/google/android/location/os/Callbacks;->quit(Z)V

    iget-object v1, p0, Lcom/google/android/location/os/real/CallbackRunner$MyHandler;->telephonyManager:Landroid/telephony/TelephonyManager;

    iget-object v3, p0, Lcom/google/android/location/os/real/CallbackRunner$MyHandler;->this$0:Lcom/google/android/location/os/real/CallbackRunner;

    # getter for: Lcom/google/android/location/os/real/CallbackRunner;->phoneStateListener:Lcom/google/android/location/os/real/CallbackRunner$MyPhoneStateListener;
    invoke-static {v3}, Lcom/google/android/location/os/real/CallbackRunner;->access$300(Lcom/google/android/location/os/real/CallbackRunner;)Lcom/google/android/location/os/real/CallbackRunner$MyPhoneStateListener;

    move-result-object v3

    const/4 v4, 0x0

    invoke-virtual {v1, v3, v4}, Landroid/telephony/TelephonyManager;->listen(Landroid/telephony/PhoneStateListener;I)V

    invoke-static {}, Landroid/os/Looper;->myLooper()Landroid/os/Looper;

    move-result-object v1

    invoke-virtual {v1}, Landroid/os/Looper;->quit()V

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    goto :goto_1

    :pswitch_2
    iget-object v1, p0, Lcom/google/android/location/os/real/CallbackRunner$MyHandler;->this$0:Lcom/google/android/location/os/real/CallbackRunner;

    # getter for: Lcom/google/android/location/os/real/CallbackRunner;->eventLog:Lcom/google/android/location/os/EventLog;
    invoke-static {v1}, Lcom/google/android/location/os/real/CallbackRunner;->access$100(Lcom/google/android/location/os/real/CallbackRunner;)Lcom/google/android/location/os/EventLog;

    move-result-object v1

    sget-object v3, Lcom/google/android/location/os/Event;->GLS_QUERY_RESPONSE:Lcom/google/android/location/os/Event;

    invoke-virtual {v1, v3}, Lcom/google/android/location/os/EventLog;->addEvent(Lcom/google/android/location/os/Event;)V

    iget-object v1, p0, Lcom/google/android/location/os/real/CallbackRunner$MyHandler;->this$0:Lcom/google/android/location/os/real/CallbackRunner;

    # getter for: Lcom/google/android/location/os/real/CallbackRunner;->callbacks:Lcom/google/android/location/os/Callbacks;
    invoke-static {v1}, Lcom/google/android/location/os/real/CallbackRunner;->access$200(Lcom/google/android/location/os/real/CallbackRunner;)Lcom/google/android/location/os/Callbacks;

    move-result-object v3

    iget-object v1, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v1, Lcom/google/gmm/common/io/protocol/ProtoBuf;

    invoke-interface {v3, v1}, Lcom/google/android/location/os/Callbacks;->glsQueryResponse(Lcom/google/gmm/common/io/protocol/ProtoBuf;)V

    goto :goto_0

    :pswitch_3
    iget-object v1, p0, Lcom/google/android/location/os/real/CallbackRunner$MyHandler;->this$0:Lcom/google/android/location/os/real/CallbackRunner;

    # getter for: Lcom/google/android/location/os/real/CallbackRunner;->eventLog:Lcom/google/android/location/os/EventLog;
    invoke-static {v1}, Lcom/google/android/location/os/real/CallbackRunner;->access$100(Lcom/google/android/location/os/real/CallbackRunner;)Lcom/google/android/location/os/EventLog;

    move-result-object v3

    iget v4, p1, Landroid/os/Message;->arg1:I

    iget v1, p1, Landroid/os/Message;->arg2:I

    if-eqz v1, :cond_1

    const/4 v1, 0x1

    :goto_2
    invoke-virtual {v3, v4, v1}, Lcom/google/android/location/os/EventLog;->addSetPeriod(IZ)V

    iget-object v1, p0, Lcom/google/android/location/os/real/CallbackRunner$MyHandler;->this$0:Lcom/google/android/location/os/real/CallbackRunner;

    # getter for: Lcom/google/android/location/os/real/CallbackRunner;->callbacks:Lcom/google/android/location/os/Callbacks;
    invoke-static {v1}, Lcom/google/android/location/os/real/CallbackRunner;->access$200(Lcom/google/android/location/os/real/CallbackRunner;)Lcom/google/android/location/os/Callbacks;

    move-result-object v3

    iget v4, p1, Landroid/os/Message;->arg1:I

    iget v1, p1, Landroid/os/Message;->arg2:I

    if-eqz v1, :cond_2

    const/4 v1, 0x1

    :goto_3
    invoke-interface {v3, v4, v1}, Lcom/google/android/location/os/Callbacks;->setPeriod(IZ)V

    goto :goto_0

    :cond_1
    const/4 v1, 0x0

    goto :goto_2

    :cond_2
    const/4 v1, 0x0

    goto :goto_3

    :pswitch_4
    iget v1, p1, Landroid/os/Message;->arg1:I

    const/4 v3, 0x1

    if-ne v1, v3, :cond_3

    const/16 v1, -0x270f

    iput v1, p0, Lcom/google/android/location/os/real/CallbackRunner$MyHandler;->lastSignalStrength:I

    :cond_3
    const/4 v0, 0x0

    iget-object v1, p0, Lcom/google/android/location/os/real/CallbackRunner$MyHandler;->telephonyManager:Landroid/telephony/TelephonyManager;

    invoke-virtual {v1}, Landroid/telephony/TelephonyManager;->getCellLocation()Landroid/telephony/CellLocation;

    move-result-object v2

    if-eqz v2, :cond_4

    new-instance v0, Lcom/google/android/location/os/real/RealCellState;

    iget-object v1, p0, Lcom/google/android/location/os/real/CallbackRunner$MyHandler;->telephonyManager:Landroid/telephony/TelephonyManager;

    iget v3, p0, Lcom/google/android/location/os/real/CallbackRunner$MyHandler;->lastSignalStrength:I

    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v4

    invoke-direct/range {v0 .. v5}, Lcom/google/android/location/os/real/RealCellState;-><init>(Landroid/telephony/TelephonyManager;Landroid/telephony/CellLocation;IJ)V

    :cond_4
    iget-object v1, p0, Lcom/google/android/location/os/real/CallbackRunner$MyHandler;->this$0:Lcom/google/android/location/os/real/CallbackRunner;

    # getter for: Lcom/google/android/location/os/real/CallbackRunner;->eventLog:Lcom/google/android/location/os/EventLog;
    invoke-static {v1}, Lcom/google/android/location/os/real/CallbackRunner;->access$100(Lcom/google/android/location/os/real/CallbackRunner;)Lcom/google/android/location/os/EventLog;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/google/android/location/os/EventLog;->addCellScanResults(Lcom/google/android/location/data/CellState;)V

    iget-object v1, p0, Lcom/google/android/location/os/real/CallbackRunner$MyHandler;->this$0:Lcom/google/android/location/os/real/CallbackRunner;

    # getter for: Lcom/google/android/location/os/real/CallbackRunner;->callbacks:Lcom/google/android/location/os/Callbacks;
    invoke-static {v1}, Lcom/google/android/location/os/real/CallbackRunner;->access$200(Lcom/google/android/location/os/real/CallbackRunner;)Lcom/google/android/location/os/Callbacks;

    move-result-object v1

    invoke-interface {v1, v0}, Lcom/google/android/location/os/Callbacks;->cellScanResults(Lcom/google/android/location/data/CellState;)V

    goto/16 :goto_0

    :pswitch_5
    iget-object v12, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v12, Landroid/telephony/SignalStrength;

    invoke-virtual {v12}, Landroid/telephony/SignalStrength;->isGsm()Z

    move-result v1

    if-eqz v1, :cond_5

    invoke-virtual {v12}, Landroid/telephony/SignalStrength;->getGsmSignalStrength()I

    move-result v1

    iput v1, p0, Lcom/google/android/location/os/real/CallbackRunner$MyHandler;->lastSignalStrength:I

    :goto_4
    iget-object v1, p0, Lcom/google/android/location/os/real/CallbackRunner$MyHandler;->this$0:Lcom/google/android/location/os/real/CallbackRunner;

    # getter for: Lcom/google/android/location/os/real/CallbackRunner;->eventLog:Lcom/google/android/location/os/EventLog;
    invoke-static {v1}, Lcom/google/android/location/os/real/CallbackRunner;->access$100(Lcom/google/android/location/os/real/CallbackRunner;)Lcom/google/android/location/os/EventLog;

    move-result-object v1

    iget v3, p0, Lcom/google/android/location/os/real/CallbackRunner$MyHandler;->lastSignalStrength:I

    invoke-virtual {v1, v3}, Lcom/google/android/location/os/EventLog;->addCellSignalStrength(I)V

    iget-object v1, p0, Lcom/google/android/location/os/real/CallbackRunner$MyHandler;->this$0:Lcom/google/android/location/os/real/CallbackRunner;

    # getter for: Lcom/google/android/location/os/real/CallbackRunner;->callbacks:Lcom/google/android/location/os/Callbacks;
    invoke-static {v1}, Lcom/google/android/location/os/real/CallbackRunner;->access$200(Lcom/google/android/location/os/real/CallbackRunner;)Lcom/google/android/location/os/Callbacks;

    move-result-object v1

    iget v3, p0, Lcom/google/android/location/os/real/CallbackRunner$MyHandler;->lastSignalStrength:I

    invoke-interface {v1, v3}, Lcom/google/android/location/os/Callbacks;->cellSignalStrength(I)V

    goto/16 :goto_0

    :cond_5
    invoke-virtual {v12}, Landroid/telephony/SignalStrength;->getCdmaDbm()I

    move-result v1

    iput v1, p0, Lcom/google/android/location/os/real/CallbackRunner$MyHandler;->lastSignalStrength:I

    goto :goto_4

    :pswitch_6
    iget-object v1, p0, Lcom/google/android/location/os/real/CallbackRunner$MyHandler;->this$0:Lcom/google/android/location/os/real/CallbackRunner;

    # getter for: Lcom/google/android/location/os/real/CallbackRunner;->eventLog:Lcom/google/android/location/os/EventLog;
    invoke-static {v1}, Lcom/google/android/location/os/real/CallbackRunner;->access$100(Lcom/google/android/location/os/real/CallbackRunner;)Lcom/google/android/location/os/EventLog;

    move-result-object v1

    const/4 v3, 0x0

    invoke-virtual {v1, v3}, Lcom/google/android/location/os/EventLog;->addAlarmRing(I)V

    iget-object v1, p0, Lcom/google/android/location/os/real/CallbackRunner$MyHandler;->this$0:Lcom/google/android/location/os/real/CallbackRunner;

    # getter for: Lcom/google/android/location/os/real/CallbackRunner;->callbacks:Lcom/google/android/location/os/Callbacks;
    invoke-static {v1}, Lcom/google/android/location/os/real/CallbackRunner;->access$200(Lcom/google/android/location/os/real/CallbackRunner;)Lcom/google/android/location/os/Callbacks;

    move-result-object v1

    const/4 v3, 0x0

    invoke-interface {v1, v3}, Lcom/google/android/location/os/Callbacks;->alarmRing(I)V

    iget-object v1, p0, Lcom/google/android/location/os/real/CallbackRunner$MyHandler;->this$0:Lcom/google/android/location/os/real/CallbackRunner;

    # getter for: Lcom/google/android/location/os/real/CallbackRunner;->alarmWakeLock:Landroid/os/PowerManager$WakeLock;
    invoke-static {v1}, Lcom/google/android/location/os/real/CallbackRunner;->access$400(Lcom/google/android/location/os/real/CallbackRunner;)Landroid/os/PowerManager$WakeLock;

    move-result-object v1

    invoke-virtual {v1}, Landroid/os/PowerManager$WakeLock;->release()V

    goto/16 :goto_0

    :pswitch_7
    iget-object v1, p0, Lcom/google/android/location/os/real/CallbackRunner$MyHandler;->this$0:Lcom/google/android/location/os/real/CallbackRunner;

    # getter for: Lcom/google/android/location/os/real/CallbackRunner;->eventLog:Lcom/google/android/location/os/EventLog;
    invoke-static {v1}, Lcom/google/android/location/os/real/CallbackRunner;->access$100(Lcom/google/android/location/os/real/CallbackRunner;)Lcom/google/android/location/os/EventLog;

    move-result-object v1

    const/4 v3, 0x1

    invoke-virtual {v1, v3}, Lcom/google/android/location/os/EventLog;->addAlarmRing(I)V

    iget-object v1, p0, Lcom/google/android/location/os/real/CallbackRunner$MyHandler;->this$0:Lcom/google/android/location/os/real/CallbackRunner;

    # getter for: Lcom/google/android/location/os/real/CallbackRunner;->callbacks:Lcom/google/android/location/os/Callbacks;
    invoke-static {v1}, Lcom/google/android/location/os/real/CallbackRunner;->access$200(Lcom/google/android/location/os/real/CallbackRunner;)Lcom/google/android/location/os/Callbacks;

    move-result-object v1

    const/4 v3, 0x1

    invoke-interface {v1, v3}, Lcom/google/android/location/os/Callbacks;->alarmRing(I)V

    iget-object v1, p0, Lcom/google/android/location/os/real/CallbackRunner$MyHandler;->this$0:Lcom/google/android/location/os/real/CallbackRunner;

    # getter for: Lcom/google/android/location/os/real/CallbackRunner;->alarmWakeLock:Landroid/os/PowerManager$WakeLock;
    invoke-static {v1}, Lcom/google/android/location/os/real/CallbackRunner;->access$400(Lcom/google/android/location/os/real/CallbackRunner;)Landroid/os/PowerManager$WakeLock;

    move-result-object v1

    invoke-virtual {v1}, Landroid/os/PowerManager$WakeLock;->release()V

    goto/16 :goto_0

    :pswitch_8
    iget-object v1, p0, Lcom/google/android/location/os/real/CallbackRunner$MyHandler;->this$0:Lcom/google/android/location/os/real/CallbackRunner;

    # getter for: Lcom/google/android/location/os/real/CallbackRunner;->eventLog:Lcom/google/android/location/os/EventLog;
    invoke-static {v1}, Lcom/google/android/location/os/real/CallbackRunner;->access$100(Lcom/google/android/location/os/real/CallbackRunner;)Lcom/google/android/location/os/EventLog;

    move-result-object v1

    const/4 v3, 0x2

    invoke-virtual {v1, v3}, Lcom/google/android/location/os/EventLog;->addAlarmRing(I)V

    iget-object v1, p0, Lcom/google/android/location/os/real/CallbackRunner$MyHandler;->this$0:Lcom/google/android/location/os/real/CallbackRunner;

    # getter for: Lcom/google/android/location/os/real/CallbackRunner;->callbacks:Lcom/google/android/location/os/Callbacks;
    invoke-static {v1}, Lcom/google/android/location/os/real/CallbackRunner;->access$200(Lcom/google/android/location/os/real/CallbackRunner;)Lcom/google/android/location/os/Callbacks;

    move-result-object v1

    const/4 v3, 0x2

    invoke-interface {v1, v3}, Lcom/google/android/location/os/Callbacks;->alarmRing(I)V

    iget-object v1, p0, Lcom/google/android/location/os/real/CallbackRunner$MyHandler;->this$0:Lcom/google/android/location/os/real/CallbackRunner;

    # getter for: Lcom/google/android/location/os/real/CallbackRunner;->alarmWakeLock:Landroid/os/PowerManager$WakeLock;
    invoke-static {v1}, Lcom/google/android/location/os/real/CallbackRunner;->access$400(Lcom/google/android/location/os/real/CallbackRunner;)Landroid/os/PowerManager$WakeLock;

    move-result-object v1

    invoke-virtual {v1}, Landroid/os/PowerManager$WakeLock;->release()V

    goto/16 :goto_0

    :pswitch_9
    iget-object v13, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v13, Lcom/google/android/location/os/real/RealWifiScan;

    iget-object v1, p0, Lcom/google/android/location/os/real/CallbackRunner$MyHandler;->this$0:Lcom/google/android/location/os/real/CallbackRunner;

    # getter for: Lcom/google/android/location/os/real/CallbackRunner;->eventLog:Lcom/google/android/location/os/EventLog;
    invoke-static {v1}, Lcom/google/android/location/os/real/CallbackRunner;->access$100(Lcom/google/android/location/os/real/CallbackRunner;)Lcom/google/android/location/os/EventLog;

    move-result-object v1

    invoke-virtual {v1, v13}, Lcom/google/android/location/os/EventLog;->addWifiScanResults(Lcom/google/android/location/data/WifiScan;)V

    iget-object v1, p0, Lcom/google/android/location/os/real/CallbackRunner$MyHandler;->this$0:Lcom/google/android/location/os/real/CallbackRunner;

    # getter for: Lcom/google/android/location/os/real/CallbackRunner;->callbacks:Lcom/google/android/location/os/Callbacks;
    invoke-static {v1}, Lcom/google/android/location/os/real/CallbackRunner;->access$200(Lcom/google/android/location/os/real/CallbackRunner;)Lcom/google/android/location/os/Callbacks;

    move-result-object v1

    invoke-interface {v1, v13}, Lcom/google/android/location/os/Callbacks;->wifiScanResults(Lcom/google/android/location/data/WifiScan;)V

    goto/16 :goto_0

    :pswitch_a
    iget v1, p1, Landroid/os/Message;->arg1:I

    if-eqz v1, :cond_6

    const/4 v7, 0x1

    :goto_5
    iget-object v1, p0, Lcom/google/android/location/os/real/CallbackRunner$MyHandler;->this$0:Lcom/google/android/location/os/real/CallbackRunner;

    # getter for: Lcom/google/android/location/os/real/CallbackRunner;->eventLog:Lcom/google/android/location/os/EventLog;
    invoke-static {v1}, Lcom/google/android/location/os/real/CallbackRunner;->access$100(Lcom/google/android/location/os/real/CallbackRunner;)Lcom/google/android/location/os/EventLog;

    move-result-object v1

    invoke-virtual {v1, v7}, Lcom/google/android/location/os/EventLog;->addWifiStateChanged(Z)V

    iget-object v1, p0, Lcom/google/android/location/os/real/CallbackRunner$MyHandler;->this$0:Lcom/google/android/location/os/real/CallbackRunner;

    # getter for: Lcom/google/android/location/os/real/CallbackRunner;->callbacks:Lcom/google/android/location/os/Callbacks;
    invoke-static {v1}, Lcom/google/android/location/os/real/CallbackRunner;->access$200(Lcom/google/android/location/os/real/CallbackRunner;)Lcom/google/android/location/os/Callbacks;

    move-result-object v1

    invoke-interface {v1, v7}, Lcom/google/android/location/os/Callbacks;->wifiStateChanged(Z)V

    goto/16 :goto_0

    :cond_6
    const/4 v7, 0x0

    goto :goto_5

    :pswitch_b
    iget-object v6, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v6, Landroid/os/Bundle;

    const-string v1, "scale"

    const/16 v3, 0x64

    invoke-virtual {v6, v1, v3}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v11

    const-string v1, "level"

    const/4 v3, 0x0

    invoke-virtual {v6, v1, v3}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v8

    const-string v1, "plugged"

    const/4 v3, 0x0

    invoke-virtual {v6, v1, v3}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v1

    if-eqz v1, :cond_7

    const/4 v10, 0x1

    :goto_6
    iget-object v1, p0, Lcom/google/android/location/os/real/CallbackRunner$MyHandler;->this$0:Lcom/google/android/location/os/real/CallbackRunner;

    # getter for: Lcom/google/android/location/os/real/CallbackRunner;->eventLog:Lcom/google/android/location/os/EventLog;
    invoke-static {v1}, Lcom/google/android/location/os/real/CallbackRunner;->access$100(Lcom/google/android/location/os/real/CallbackRunner;)Lcom/google/android/location/os/EventLog;

    move-result-object v1

    invoke-virtual {v1, v11, v8, v10}, Lcom/google/android/location/os/EventLog;->addBatteryStateChanged(IIZ)V

    iget-object v1, p0, Lcom/google/android/location/os/real/CallbackRunner$MyHandler;->this$0:Lcom/google/android/location/os/real/CallbackRunner;

    # getter for: Lcom/google/android/location/os/real/CallbackRunner;->callbacks:Lcom/google/android/location/os/Callbacks;
    invoke-static {v1}, Lcom/google/android/location/os/real/CallbackRunner;->access$200(Lcom/google/android/location/os/real/CallbackRunner;)Lcom/google/android/location/os/Callbacks;

    move-result-object v1

    invoke-interface {v1, v11, v8, v10}, Lcom/google/android/location/os/Callbacks;->batteryChanged(IIZ)V

    goto/16 :goto_0

    :cond_7
    const/4 v10, 0x0

    goto :goto_6

    :pswitch_c
    iget v1, p1, Landroid/os/Message;->arg1:I

    if-eqz v1, :cond_8

    const/4 v9, 0x1

    :goto_7
    iget-object v1, p0, Lcom/google/android/location/os/real/CallbackRunner$MyHandler;->this$0:Lcom/google/android/location/os/real/CallbackRunner;

    # getter for: Lcom/google/android/location/os/real/CallbackRunner;->eventLog:Lcom/google/android/location/os/EventLog;
    invoke-static {v1}, Lcom/google/android/location/os/real/CallbackRunner;->access$100(Lcom/google/android/location/os/real/CallbackRunner;)Lcom/google/android/location/os/EventLog;

    move-result-object v1

    invoke-virtual {v1, v9}, Lcom/google/android/location/os/EventLog;->addScreenStateChanged(Z)V

    iget-object v1, p0, Lcom/google/android/location/os/real/CallbackRunner$MyHandler;->this$0:Lcom/google/android/location/os/real/CallbackRunner;

    # getter for: Lcom/google/android/location/os/real/CallbackRunner;->callbacks:Lcom/google/android/location/os/Callbacks;
    invoke-static {v1}, Lcom/google/android/location/os/real/CallbackRunner;->access$200(Lcom/google/android/location/os/real/CallbackRunner;)Lcom/google/android/location/os/Callbacks;

    move-result-object v1

    invoke-interface {v1, v9}, Lcom/google/android/location/os/Callbacks;->screenStateChanged(Z)V

    goto/16 :goto_0

    :cond_8
    const/4 v9, 0x0

    goto :goto_7

    :pswitch_d
    iget v1, p1, Landroid/os/Message;->arg1:I

    if-eqz v1, :cond_9

    const/4 v7, 0x1

    :goto_8
    iget-object v1, p0, Lcom/google/android/location/os/real/CallbackRunner$MyHandler;->this$0:Lcom/google/android/location/os/real/CallbackRunner;

    # getter for: Lcom/google/android/location/os/real/CallbackRunner;->eventLog:Lcom/google/android/location/os/EventLog;
    invoke-static {v1}, Lcom/google/android/location/os/real/CallbackRunner;->access$100(Lcom/google/android/location/os/real/CallbackRunner;)Lcom/google/android/location/os/EventLog;

    move-result-object v1

    invoke-virtual {v1, v7}, Lcom/google/android/location/os/EventLog;->addAirplaneModeChanged(Z)V

    iget-object v1, p0, Lcom/google/android/location/os/real/CallbackRunner$MyHandler;->this$0:Lcom/google/android/location/os/real/CallbackRunner;

    # getter for: Lcom/google/android/location/os/real/CallbackRunner;->callbacks:Lcom/google/android/location/os/Callbacks;
    invoke-static {v1}, Lcom/google/android/location/os/real/CallbackRunner;->access$200(Lcom/google/android/location/os/real/CallbackRunner;)Lcom/google/android/location/os/Callbacks;

    move-result-object v1

    invoke-interface {v1, v7}, Lcom/google/android/location/os/Callbacks;->airplaneModeChanged(Z)V

    goto/16 :goto_0

    :cond_9
    const/4 v7, 0x0

    goto :goto_8

    :pswitch_e
    iget-object v1, p0, Lcom/google/android/location/os/real/CallbackRunner$MyHandler;->this$0:Lcom/google/android/location/os/real/CallbackRunner;

    # getter for: Lcom/google/android/location/os/real/CallbackRunner;->eventLog:Lcom/google/android/location/os/EventLog;
    invoke-static {v1}, Lcom/google/android/location/os/real/CallbackRunner;->access$100(Lcom/google/android/location/os/real/CallbackRunner;)Lcom/google/android/location/os/EventLog;

    move-result-object v1

    sget-object v3, Lcom/google/android/location/os/Event;->GLS_MODEL_QUERY_RESPONSE:Lcom/google/android/location/os/Event;

    invoke-virtual {v1, v3}, Lcom/google/android/location/os/EventLog;->addEvent(Lcom/google/android/location/os/Event;)V

    iget-object v1, p0, Lcom/google/android/location/os/real/CallbackRunner$MyHandler;->this$0:Lcom/google/android/location/os/real/CallbackRunner;

    # getter for: Lcom/google/android/location/os/real/CallbackRunner;->callbacks:Lcom/google/android/location/os/Callbacks;
    invoke-static {v1}, Lcom/google/android/location/os/real/CallbackRunner;->access$200(Lcom/google/android/location/os/real/CallbackRunner;)Lcom/google/android/location/os/Callbacks;

    move-result-object v3

    iget-object v1, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v1, Lcom/google/gmm/common/io/protocol/ProtoBuf;

    invoke-interface {v3, v1}, Lcom/google/android/location/os/Callbacks;->glsModelQueryResponse(Lcom/google/gmm/common/io/protocol/ProtoBuf;)V

    goto/16 :goto_0

    :pswitch_f
    iget-object v1, p0, Lcom/google/android/location/os/real/CallbackRunner$MyHandler;->this$0:Lcom/google/android/location/os/real/CallbackRunner;

    # getter for: Lcom/google/android/location/os/real/CallbackRunner;->eventLog:Lcom/google/android/location/os/EventLog;
    invoke-static {v1}, Lcom/google/android/location/os/real/CallbackRunner;->access$100(Lcom/google/android/location/os/real/CallbackRunner;)Lcom/google/android/location/os/EventLog;

    move-result-object v1

    sget-object v3, Lcom/google/android/location/os/Event;->GLS_DEVICE_LOCATION_RESPONSE:Lcom/google/android/location/os/Event;

    invoke-virtual {v1, v3}, Lcom/google/android/location/os/EventLog;->addEvent(Lcom/google/android/location/os/Event;)V

    iget-object v1, p0, Lcom/google/android/location/os/real/CallbackRunner$MyHandler;->this$0:Lcom/google/android/location/os/real/CallbackRunner;

    # getter for: Lcom/google/android/location/os/real/CallbackRunner;->callbacks:Lcom/google/android/location/os/Callbacks;
    invoke-static {v1}, Lcom/google/android/location/os/real/CallbackRunner;->access$200(Lcom/google/android/location/os/real/CallbackRunner;)Lcom/google/android/location/os/Callbacks;

    move-result-object v3

    iget-object v1, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v1, Lcom/google/gmm/common/io/protocol/ProtoBuf;

    invoke-interface {v3, v1}, Lcom/google/android/location/os/Callbacks;->glsDeviceLocationResponse(Lcom/google/gmm/common/io/protocol/ProtoBuf;)V

    goto/16 :goto_0

    :pswitch_10
    iget-object v1, p0, Lcom/google/android/location/os/real/CallbackRunner$MyHandler;->this$0:Lcom/google/android/location/os/real/CallbackRunner;

    iget-object v3, p0, Lcom/google/android/location/os/real/CallbackRunner$MyHandler;->connectivityManager:Landroid/net/ConnectivityManager;

    iget-object v4, p0, Lcom/google/android/location/os/real/CallbackRunner$MyHandler;->this$0:Lcom/google/android/location/os/real/CallbackRunner;

    # getter for: Lcom/google/android/location/os/real/CallbackRunner;->callbacks:Lcom/google/android/location/os/Callbacks;
    invoke-static {v4}, Lcom/google/android/location/os/real/CallbackRunner;->access$200(Lcom/google/android/location/os/real/CallbackRunner;)Lcom/google/android/location/os/Callbacks;

    move-result-object v4

    # invokes: Lcom/google/android/location/os/real/CallbackRunner;->notifyNetworkEvent(Landroid/net/ConnectivityManager;Lcom/google/android/location/os/Callbacks;)V
    invoke-static {v1, v3, v4}, Lcom/google/android/location/os/real/CallbackRunner;->access$500(Lcom/google/android/location/os/real/CallbackRunner;Landroid/net/ConnectivityManager;Lcom/google/android/location/os/Callbacks;)V

    goto/16 :goto_0

    :pswitch_11
    iget-object v1, p0, Lcom/google/android/location/os/real/CallbackRunner$MyHandler;->this$0:Lcom/google/android/location/os/real/CallbackRunner;

    # getter for: Lcom/google/android/location/os/real/CallbackRunner;->eventLog:Lcom/google/android/location/os/EventLog;
    invoke-static {v1}, Lcom/google/android/location/os/real/CallbackRunner;->access$100(Lcom/google/android/location/os/real/CallbackRunner;)Lcom/google/android/location/os/EventLog;

    move-result-object v1

    sget-object v3, Lcom/google/android/location/os/Event;->NLP_PARAMS_CHANGED:Lcom/google/android/location/os/Event;

    invoke-virtual {v1, v3}, Lcom/google/android/location/os/EventLog;->addEvent(Lcom/google/android/location/os/Event;)V

    iget-object v1, p0, Lcom/google/android/location/os/real/CallbackRunner$MyHandler;->this$0:Lcom/google/android/location/os/real/CallbackRunner;

    # getter for: Lcom/google/android/location/os/real/CallbackRunner;->callbacks:Lcom/google/android/location/os/Callbacks;
    invoke-static {v1}, Lcom/google/android/location/os/real/CallbackRunner;->access$200(Lcom/google/android/location/os/real/CallbackRunner;)Lcom/google/android/location/os/Callbacks;

    move-result-object v3

    iget-object v1, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v1, Lcom/google/android/location/os/NlpParametersState;

    invoke-interface {v3, v1}, Lcom/google/android/location/os/Callbacks;->nlpParamtersStateChanged(Lcom/google/android/location/os/NlpParametersState;)V

    goto/16 :goto_0

    :pswitch_12
    iget-object v1, p0, Lcom/google/android/location/os/real/CallbackRunner$MyHandler;->this$0:Lcom/google/android/location/os/real/CallbackRunner;

    # getter for: Lcom/google/android/location/os/real/CallbackRunner;->eventLog:Lcom/google/android/location/os/EventLog;
    invoke-static {v1}, Lcom/google/android/location/os/real/CallbackRunner;->access$100(Lcom/google/android/location/os/real/CallbackRunner;)Lcom/google/android/location/os/EventLog;

    move-result-object v1

    sget-object v3, Lcom/google/android/location/os/Event;->USER_REPORT_MAPS_ISSUE:Lcom/google/android/location/os/Event;

    invoke-virtual {v1, v3}, Lcom/google/android/location/os/EventLog;->addEvent(Lcom/google/android/location/os/Event;)V

    iget-object v1, p0, Lcom/google/android/location/os/real/CallbackRunner$MyHandler;->this$0:Lcom/google/android/location/os/real/CallbackRunner;

    # getter for: Lcom/google/android/location/os/real/CallbackRunner;->callbacks:Lcom/google/android/location/os/Callbacks;
    invoke-static {v1}, Lcom/google/android/location/os/real/CallbackRunner;->access$200(Lcom/google/android/location/os/real/CallbackRunner;)Lcom/google/android/location/os/Callbacks;

    move-result-object v3

    iget-object v1, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v1, Lcom/google/android/location/data/NetworkLocation;

    invoke-interface {v3, v1}, Lcom/google/android/location/os/Callbacks;->signalRmiRequest(Lcom/google/android/location/data/NetworkLocation;)V

    goto/16 :goto_0

    :pswitch_13
    iget-object v1, p0, Lcom/google/android/location/os/real/CallbackRunner$MyHandler;->this$0:Lcom/google/android/location/os/real/CallbackRunner;

    # getter for: Lcom/google/android/location/os/real/CallbackRunner;->callbacks:Lcom/google/android/location/os/Callbacks;
    invoke-static {v1}, Lcom/google/android/location/os/real/CallbackRunner;->access$200(Lcom/google/android/location/os/real/CallbackRunner;)Lcom/google/android/location/os/Callbacks;

    move-result-object v3

    iget-object v1, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v1, Lcom/google/android/location/activity/TravelDetectionManager$TravelDetectionResult;

    invoke-interface {v3, v1}, Lcom/google/android/location/os/Callbacks;->onTravelModeDetected(Lcom/google/android/location/activity/TravelDetectionManager$TravelDetectionResult;)V

    goto/16 :goto_0

    :pswitch_14
    iget-object v1, p0, Lcom/google/android/location/os/real/CallbackRunner$MyHandler;->this$0:Lcom/google/android/location/os/real/CallbackRunner;

    # getter for: Lcom/google/android/location/os/real/CallbackRunner;->callbacks:Lcom/google/android/location/os/Callbacks;
    invoke-static {v1}, Lcom/google/android/location/os/real/CallbackRunner;->access$200(Lcom/google/android/location/os/real/CallbackRunner;)Lcom/google/android/location/os/Callbacks;

    move-result-object v3

    iget-object v1, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v1, Lcom/google/android/location/clientlib/NlpActivity;

    invoke-interface {v3, v1}, Lcom/google/android/location/os/Callbacks;->onActivityDetected(Lcom/google/android/location/clientlib/NlpActivity;)V

    goto/16 :goto_0

    :pswitch_15
    iget-object v1, p0, Lcom/google/android/location/os/real/CallbackRunner$MyHandler;->this$0:Lcom/google/android/location/os/real/CallbackRunner;

    # getter for: Lcom/google/android/location/os/real/CallbackRunner;->callbacks:Lcom/google/android/location/os/Callbacks;
    invoke-static {v1}, Lcom/google/android/location/os/real/CallbackRunner;->access$200(Lcom/google/android/location/os/real/CallbackRunner;)Lcom/google/android/location/os/Callbacks;

    move-result-object v3

    iget-object v1, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v1, Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    invoke-interface {v3, v1}, Lcom/google/android/location/os/Callbacks;->onActivityDetectionEnabled(Z)V

    goto/16 :goto_0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_7
        :pswitch_8
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_9
        :pswitch_a
        :pswitch_b
        :pswitch_0
        :pswitch_c
        :pswitch_0
        :pswitch_d
        :pswitch_0
        :pswitch_e
        :pswitch_10
        :pswitch_f
        :pswitch_11
        :pswitch_12
        :pswitch_14
        :pswitch_15
        :pswitch_13
    .end packed-switch
.end method
