.class Lcom/google/android/location/os/real/RealWifiScan;
.super Lcom/google/android/location/data/WifiScan;
.source "RealWifiScan.java"


# direct methods
.method private constructor <init>(JLjava/util/ArrayList;)V
    .locals 0
    .param p1    # J
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(J",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/google/android/location/data/WifiScan$Device;",
            ">;)V"
        }
    .end annotation

    invoke-direct {p0, p1, p2, p3}, Lcom/google/android/location/data/WifiScan;-><init>(JLjava/util/ArrayList;)V

    return-void
.end method

.method public static create(JLjava/util/List;)Lcom/google/android/location/os/real/RealWifiScan;
    .locals 10
    .param p0    # J
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(J",
            "Ljava/util/List",
            "<",
            "Landroid/net/wifi/ScanResult;",
            ">;)",
            "Lcom/google/android/location/os/real/RealWifiScan;"
        }
    .end annotation

    new-instance v6, Ljava/util/ArrayList;

    invoke-interface {p2}, Ljava/util/List;->size()I

    move-result v0

    invoke-direct {v6, v0}, Ljava/util/ArrayList;-><init>(I)V

    invoke-interface {p2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v7

    :cond_0
    :goto_0
    invoke-interface {v7}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v7}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Landroid/net/wifi/ScanResult;

    iget-object v0, v9, Landroid/net/wifi/ScanResult;->capabilities:Ljava/lang/String;

    if-eqz v0, :cond_1

    iget-object v0, v9, Landroid/net/wifi/ScanResult;->capabilities:Ljava/lang/String;

    const-string v3, "[IBSS]"

    invoke-virtual {v0, v3}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_1

    const/4 v8, 0x1

    :goto_1
    if-nez v8, :cond_0

    iget-object v0, v9, Landroid/net/wifi/ScanResult;->BSSID:Ljava/lang/String;

    invoke-static {v0}, Lcom/google/android/location/data/WifiUtils;->macToLong(Ljava/lang/String;)J

    move-result-wide v1

    const-wide/16 v3, -0x1

    cmp-long v0, v1, v3

    if-eqz v0, :cond_0

    new-instance v0, Lcom/google/android/location/data/WifiScan$Device;

    iget v3, v9, Landroid/net/wifi/ScanResult;->level:I

    iget-object v4, v9, Landroid/net/wifi/ScanResult;->SSID:Ljava/lang/String;

    iget v5, v9, Landroid/net/wifi/ScanResult;->frequency:I

    int-to-short v5, v5

    invoke-direct/range {v0 .. v5}, Lcom/google/android/location/data/WifiScan$Device;-><init>(JILjava/lang/String;S)V

    invoke-virtual {v6, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_1
    const/4 v8, 0x0

    goto :goto_1

    :cond_2
    new-instance v0, Lcom/google/android/location/os/real/RealWifiScan;

    invoke-direct {v0, p0, p1, v6}, Lcom/google/android/location/os/real/RealWifiScan;-><init>(JLjava/util/ArrayList;)V

    return-object v0
.end method
