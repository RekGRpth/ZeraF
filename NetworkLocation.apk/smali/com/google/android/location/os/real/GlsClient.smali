.class public Lcom/google/android/location/os/real/GlsClient;
.super Ljava/lang/Object;
.source "GlsClient.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/location/os/real/GlsClient$1;,
        Lcom/google/android/location/os/real/GlsClient$RequestListener;,
        Lcom/google/android/location/os/real/GlsClient$RequestListenerType;
    }
.end annotation


# static fields
.field static USE_GSERVICES:Z

.field private static applicationVersion:Ljava/lang/String;

.field private static cacheDir:Ljava/io/File;

.field private static platformBuild:Ljava/lang/String;

.field private static platformKey:Ljava/lang/String;


# instance fields
.field private final callbackRunner:Lcom/google/android/location/os/real/CallbackRunner;

.field private final context:Landroid/content/Context;

.field private final deviceLocationQueryListener:Lcom/google/android/location/os/real/GlsClient$RequestListener;

.field private mDeviceRestart:Z

.field private final modelQueryListener:Lcom/google/android/location/os/real/GlsClient$RequestListener;

.field private final nlpParamsState:Lcom/google/android/location/os/NlpParametersState;

.field private final queryListener:Lcom/google/android/location/os/real/GlsClient$RequestListener;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const/4 v0, 0x1

    sput-boolean v0, Lcom/google/android/location/os/real/GlsClient;->USE_GSERVICES:Z

    const-string v0, ""

    sput-object v0, Lcom/google/android/location/os/real/GlsClient;->applicationVersion:Ljava/lang/String;

    const/4 v0, 0x0

    sput-object v0, Lcom/google/android/location/os/real/GlsClient;->platformBuild:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Lcom/google/android/location/os/NlpParametersState;Lcom/google/android/location/os/real/CallbackRunner;)V
    .locals 2
    .param p1    # Landroid/content/Context;
    .param p2    # Lcom/google/android/location/os/NlpParametersState;
    .param p3    # Lcom/google/android/location/os/real/CallbackRunner;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/location/os/real/GlsClient;->mDeviceRestart:Z

    new-instance v0, Lcom/google/android/location/os/real/GlsClient$RequestListener;

    sget-object v1, Lcom/google/android/location/os/real/GlsClient$RequestListenerType;->QUERY:Lcom/google/android/location/os/real/GlsClient$RequestListenerType;

    invoke-direct {v0, p0, v1}, Lcom/google/android/location/os/real/GlsClient$RequestListener;-><init>(Lcom/google/android/location/os/real/GlsClient;Lcom/google/android/location/os/real/GlsClient$RequestListenerType;)V

    iput-object v0, p0, Lcom/google/android/location/os/real/GlsClient;->queryListener:Lcom/google/android/location/os/real/GlsClient$RequestListener;

    new-instance v0, Lcom/google/android/location/os/real/GlsClient$RequestListener;

    sget-object v1, Lcom/google/android/location/os/real/GlsClient$RequestListenerType;->MODEL_QUERY:Lcom/google/android/location/os/real/GlsClient$RequestListenerType;

    invoke-direct {v0, p0, v1}, Lcom/google/android/location/os/real/GlsClient$RequestListener;-><init>(Lcom/google/android/location/os/real/GlsClient;Lcom/google/android/location/os/real/GlsClient$RequestListenerType;)V

    iput-object v0, p0, Lcom/google/android/location/os/real/GlsClient;->modelQueryListener:Lcom/google/android/location/os/real/GlsClient$RequestListener;

    new-instance v0, Lcom/google/android/location/os/real/GlsClient$RequestListener;

    sget-object v1, Lcom/google/android/location/os/real/GlsClient$RequestListenerType;->DEVICE_LOCATION_QUERY:Lcom/google/android/location/os/real/GlsClient$RequestListenerType;

    invoke-direct {v0, p0, v1}, Lcom/google/android/location/os/real/GlsClient$RequestListener;-><init>(Lcom/google/android/location/os/real/GlsClient;Lcom/google/android/location/os/real/GlsClient$RequestListenerType;)V

    iput-object v0, p0, Lcom/google/android/location/os/real/GlsClient;->deviceLocationQueryListener:Lcom/google/android/location/os/real/GlsClient$RequestListener;

    iput-object p1, p0, Lcom/google/android/location/os/real/GlsClient;->context:Landroid/content/Context;

    iput-object p2, p0, Lcom/google/android/location/os/real/GlsClient;->nlpParamsState:Lcom/google/android/location/os/NlpParametersState;

    iput-object p3, p0, Lcom/google/android/location/os/real/GlsClient;->callbackRunner:Lcom/google/android/location/os/real/CallbackRunner;

    invoke-static {p1}, Lcom/google/android/location/os/real/GlsClient;->init(Landroid/content/Context;)V

    return-void
.end method

.method static synthetic access$000(Ljava/util/Locale;Lcom/google/gmm/common/io/protocol/ProtoBuf;)Lcom/google/gmm/common/io/protocol/ProtoBuf;
    .locals 1
    .param p0    # Ljava/util/Locale;
    .param p1    # Lcom/google/gmm/common/io/protocol/ProtoBuf;

    invoke-static {p0, p1}, Lcom/google/android/location/os/real/GlsClient;->createPlatformProfile(Ljava/util/Locale;Lcom/google/gmm/common/io/protocol/ProtoBuf;)Lcom/google/gmm/common/io/protocol/ProtoBuf;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$100(Lcom/google/android/location/os/real/GlsClient;)Lcom/google/android/location/os/NlpParametersState;
    .locals 1
    .param p0    # Lcom/google/android/location/os/real/GlsClient;

    iget-object v0, p0, Lcom/google/android/location/os/real/GlsClient;->nlpParamsState:Lcom/google/android/location/os/NlpParametersState;

    return-object v0
.end method

.method static synthetic access$200(Lcom/google/android/location/os/real/GlsClient;)Z
    .locals 1
    .param p0    # Lcom/google/android/location/os/real/GlsClient;

    invoke-direct {p0}, Lcom/google/android/location/os/real/GlsClient;->generateDebugInfo()Z

    move-result v0

    return v0
.end method

.method static synthetic access$300(Lcom/google/android/location/os/real/GlsClient;Lcom/google/gmm/common/io/protocol/ProtoBuf;)V
    .locals 0
    .param p0    # Lcom/google/android/location/os/real/GlsClient;
    .param p1    # Lcom/google/gmm/common/io/protocol/ProtoBuf;

    invoke-direct {p0, p1}, Lcom/google/android/location/os/real/GlsClient;->updateDebugProfileLocked(Lcom/google/gmm/common/io/protocol/ProtoBuf;)V

    return-void
.end method

.method static synthetic access$400(Lcom/google/android/location/os/real/GlsClient;)Lcom/google/android/location/os/real/CallbackRunner;
    .locals 1
    .param p0    # Lcom/google/android/location/os/real/GlsClient;

    iget-object v0, p0, Lcom/google/android/location/os/real/GlsClient;->callbackRunner:Lcom/google/android/location/os/real/CallbackRunner;

    return-object v0
.end method

.method static synthetic access$500(Lcom/google/gmm/common/io/protocol/ProtoBuf;)V
    .locals 0
    .param p0    # Lcom/google/gmm/common/io/protocol/ProtoBuf;

    invoke-static {p0}, Lcom/google/android/location/os/real/GlsClient;->extractPlatformKey(Lcom/google/gmm/common/io/protocol/ProtoBuf;)V

    return-void
.end method

.method private static createPlatformProfile(Ljava/util/Locale;Lcom/google/gmm/common/io/protocol/ProtoBuf;)Lcom/google/gmm/common/io/protocol/ProtoBuf;
    .locals 4
    .param p0    # Ljava/util/Locale;
    .param p1    # Lcom/google/gmm/common/io/protocol/ProtoBuf;

    new-instance v1, Lcom/google/gmm/common/io/protocol/ProtoBuf;

    sget-object v2, Lcom/google/android/location/protocol/LocserverMessageTypes;->GPLATFORM_PROFILE:Lcom/google/gmm/common/io/protocol/ProtoBufType;

    invoke-direct {v1, v2}, Lcom/google/gmm/common/io/protocol/ProtoBuf;-><init>(Lcom/google/gmm/common/io/protocol/ProtoBufType;)V

    const/4 v2, 0x1

    sget-object v3, Lcom/google/android/location/os/real/GlsClient;->applicationVersion:Ljava/lang/String;

    invoke-virtual {v1, v2, v3}, Lcom/google/gmm/common/io/protocol/ProtoBuf;->setString(ILjava/lang/String;)V

    const/4 v2, 0x2

    sget-object v3, Lcom/google/android/location/os/real/GlsClient;->platformBuild:Ljava/lang/String;

    invoke-virtual {v1, v2, v3}, Lcom/google/gmm/common/io/protocol/ProtoBuf;->setString(ILjava/lang/String;)V

    if-eqz p0, :cond_0

    invoke-virtual {p0}, Ljava/util/Locale;->toString()Ljava/lang/String;

    move-result-object v2

    if-eqz v2, :cond_0

    const/4 v2, 0x5

    invoke-virtual {p0}, Ljava/util/Locale;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Lcom/google/gmm/common/io/protocol/ProtoBuf;->setString(ILjava/lang/String;)V

    :cond_0
    invoke-static {}, Lcom/google/android/location/os/real/GlsClient;->getPlatformKey()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_1

    const/4 v2, 0x3

    invoke-virtual {v1, v2, v0}, Lcom/google/gmm/common/io/protocol/ProtoBuf;->setString(ILjava/lang/String;)V

    :cond_1
    const/4 v2, 0x6

    invoke-virtual {v1, v2, p1}, Lcom/google/gmm/common/io/protocol/ProtoBuf;->setProtoBuf(ILcom/google/gmm/common/io/protocol/ProtoBuf;)V

    return-object v1
.end method

.method public static declared-synchronized deletePlatformKey()V
    .locals 6

    const-class v3, Lcom/google/android/location/os/real/GlsClient;

    monitor-enter v3

    :try_start_0
    new-instance v1, Ljava/io/File;

    sget-object v2, Lcom/google/android/location/os/real/GlsClient;->cacheDir:Ljava/io/File;

    const-string v4, "nlp_GlsPlatformKey"

    invoke-direct {v1, v2, v4}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :try_start_1
    invoke-virtual {v1}, Ljava/io/File;->delete()Z
    :try_end_1
    .catch Ljava/lang/SecurityException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    :goto_0
    monitor-exit v3

    return-void

    :catch_0
    move-exception v0

    :try_start_2
    const-string v2, "GlsClient"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Unable to delete "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v1}, Ljava/io/File;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v2, v4, v0}, Lcom/google/android/location/utils/logging/Logger;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v2

    monitor-exit v3

    throw v2
.end method

.method private static extractPlatformKey(Lcom/google/gmm/common/io/protocol/ProtoBuf;)V
    .locals 3
    .param p0    # Lcom/google/gmm/common/io/protocol/ProtoBuf;

    const/4 v2, 0x3

    if-eqz p0, :cond_0

    const/4 v1, 0x1

    invoke-virtual {p0, v1}, Lcom/google/gmm/common/io/protocol/ProtoBuf;->getInt(I)I

    move-result v1

    if-nez v1, :cond_0

    invoke-virtual {p0, v2}, Lcom/google/gmm/common/io/protocol/ProtoBuf;->has(I)Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-virtual {p0, v2}, Lcom/google/gmm/common/io/protocol/ProtoBuf;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_0

    invoke-static {v0}, Lcom/google/android/location/os/real/GlsClient;->setPlatformKey(Ljava/lang/String;)V

    :cond_0
    return-void
.end method

.method public static forwardGeocode(Ljava/util/Locale;Ljava/lang/String;Ljava/lang/String;DDDDILjava/util/List;)V
    .locals 26
    .param p0    # Ljava/util/Locale;
    .param p1    # Ljava/lang/String;
    .param p2    # Ljava/lang/String;
    .param p3    # D
    .param p5    # D
    .param p7    # D
    .param p9    # D
    .param p11    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Locale;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "DDDDI",
            "Ljava/util/List",
            "<",
            "Landroid/location/Address;",
            ">;)V"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    new-instance v18, Lcom/google/gmm/common/io/protocol/ProtoBuf;

    sget-object v23, Lcom/google/android/location/protocol/LocserverMessageTypes;->GLOC_REQUEST_ELEMENT:Lcom/google/gmm/common/io/protocol/ProtoBufType;

    move-object/from16 v0, v18

    move-object/from16 v1, v23

    invoke-direct {v0, v1}, Lcom/google/gmm/common/io/protocol/ProtoBuf;-><init>(Lcom/google/gmm/common/io/protocol/ProtoBufType;)V

    new-instance v9, Lcom/google/gmm/common/io/protocol/ProtoBuf;

    sget-object v23, Lcom/google/android/location/protocol/LocserverMessageTypes;->GLOCATION:Lcom/google/gmm/common/io/protocol/ProtoBufType;

    move-object/from16 v0, v23

    invoke-direct {v9, v0}, Lcom/google/gmm/common/io/protocol/ProtoBuf;-><init>(Lcom/google/gmm/common/io/protocol/ProtoBufType;)V

    const/16 v23, 0x6

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v24

    move/from16 v0, v23

    move-wide/from16 v1, v24

    invoke-virtual {v9, v0, v1, v2}, Lcom/google/gmm/common/io/protocol/ProtoBuf;->setLong(IJ)V

    const/16 v23, 0xf

    move/from16 v0, v23

    move-object/from16 v1, p2

    invoke-virtual {v9, v0, v1}, Lcom/google/gmm/common/io/protocol/ProtoBuf;->setString(ILjava/lang/String;)V

    const/16 v23, 0x3

    move-object/from16 v0, v18

    move/from16 v1, v23

    invoke-virtual {v0, v1, v9}, Lcom/google/gmm/common/io/protocol/ProtoBuf;->setProtoBuf(ILcom/google/gmm/common/io/protocol/ProtoBuf;)V

    new-instance v6, Lcom/google/gmm/common/io/protocol/ProtoBuf;

    sget-object v23, Lcom/google/android/location/protocol/LocserverMessageTypes;->GGEOCODE_REQUEST:Lcom/google/gmm/common/io/protocol/ProtoBufType;

    move-object/from16 v0, v23

    invoke-direct {v6, v0}, Lcom/google/gmm/common/io/protocol/ProtoBuf;-><init>(Lcom/google/gmm/common/io/protocol/ProtoBufType;)V

    const/16 v23, 0x1

    move/from16 v0, v23

    move/from16 v1, p11

    invoke-virtual {v6, v0, v1}, Lcom/google/gmm/common/io/protocol/ProtoBuf;->setInt(II)V

    const-wide/16 v23, 0x0

    cmpl-double v23, p3, v23

    if-eqz v23, :cond_0

    const-wide/16 v23, 0x0

    cmpl-double v23, p5, v23

    if-eqz v23, :cond_0

    const-wide/16 v23, 0x0

    cmpl-double v23, p7, v23

    if-eqz v23, :cond_0

    const-wide/16 v23, 0x0

    cmpl-double v23, p9, v23

    if-eqz v23, :cond_0

    new-instance v10, Lcom/google/gmm/common/io/protocol/ProtoBuf;

    sget-object v23, Lcom/google/android/location/protocol/LocserverMessageTypes;->GLAT_LNG:Lcom/google/gmm/common/io/protocol/ProtoBufType;

    move-object/from16 v0, v23

    invoke-direct {v10, v0}, Lcom/google/gmm/common/io/protocol/ProtoBuf;-><init>(Lcom/google/gmm/common/io/protocol/ProtoBufType;)V

    const/16 v23, 0x1

    const-wide v24, 0x416312d000000000L

    mul-double v24, v24, p3

    move-wide/from16 v0, v24

    double-to-int v0, v0

    move/from16 v24, v0

    move/from16 v0, v23

    move/from16 v1, v24

    invoke-virtual {v10, v0, v1}, Lcom/google/gmm/common/io/protocol/ProtoBuf;->setInt(II)V

    const/16 v23, 0x2

    const-wide v24, 0x416312d000000000L

    mul-double v24, v24, p5

    move-wide/from16 v0, v24

    double-to-int v0, v0

    move/from16 v24, v0

    move/from16 v0, v23

    move/from16 v1, v24

    invoke-virtual {v10, v0, v1}, Lcom/google/gmm/common/io/protocol/ProtoBuf;->setInt(II)V

    new-instance v22, Lcom/google/gmm/common/io/protocol/ProtoBuf;

    sget-object v23, Lcom/google/android/location/protocol/LocserverMessageTypes;->GLAT_LNG:Lcom/google/gmm/common/io/protocol/ProtoBufType;

    invoke-direct/range {v22 .. v23}, Lcom/google/gmm/common/io/protocol/ProtoBuf;-><init>(Lcom/google/gmm/common/io/protocol/ProtoBufType;)V

    const/16 v23, 0x1

    const-wide v24, 0x416312d000000000L

    mul-double v24, v24, p7

    move-wide/from16 v0, v24

    double-to-int v0, v0

    move/from16 v24, v0

    invoke-virtual/range {v22 .. v24}, Lcom/google/gmm/common/io/protocol/ProtoBuf;->setInt(II)V

    const/16 v23, 0x2

    const-wide v24, 0x416312d000000000L

    mul-double v24, v24, p9

    move-wide/from16 v0, v24

    double-to-int v0, v0

    move/from16 v24, v0

    invoke-virtual/range {v22 .. v24}, Lcom/google/gmm/common/io/protocol/ProtoBuf;->setInt(II)V

    new-instance v4, Lcom/google/gmm/common/io/protocol/ProtoBuf;

    sget-object v23, Lcom/google/android/location/protocol/LocserverMessageTypes;->GRECTANGLE:Lcom/google/gmm/common/io/protocol/ProtoBufType;

    move-object/from16 v0, v23

    invoke-direct {v4, v0}, Lcom/google/gmm/common/io/protocol/ProtoBuf;-><init>(Lcom/google/gmm/common/io/protocol/ProtoBufType;)V

    const/16 v23, 0x1

    move/from16 v0, v23

    invoke-virtual {v4, v0, v10}, Lcom/google/gmm/common/io/protocol/ProtoBuf;->setProtoBuf(ILcom/google/gmm/common/io/protocol/ProtoBuf;)V

    const/16 v23, 0x2

    move/from16 v0, v23

    move-object/from16 v1, v22

    invoke-virtual {v4, v0, v1}, Lcom/google/gmm/common/io/protocol/ProtoBuf;->setProtoBuf(ILcom/google/gmm/common/io/protocol/ProtoBuf;)V

    const/16 v23, 0x3

    move/from16 v0, v23

    invoke-virtual {v6, v0, v4}, Lcom/google/gmm/common/io/protocol/ProtoBuf;->setProtoBuf(ILcom/google/gmm/common/io/protocol/ProtoBuf;)V

    :cond_0
    const/16 v23, 0x4

    move-object/from16 v0, v18

    move/from16 v1, v23

    invoke-virtual {v0, v1, v6}, Lcom/google/gmm/common/io/protocol/ProtoBuf;->setProtoBuf(ILcom/google/gmm/common/io/protocol/ProtoBuf;)V

    new-instance v17, Lcom/google/gmm/common/io/protocol/ProtoBuf;

    sget-object v23, Lcom/google/android/location/protocol/LocserverMessageTypes;->GLOC_REQUEST:Lcom/google/gmm/common/io/protocol/ProtoBufType;

    move-object/from16 v0, v17

    move-object/from16 v1, v23

    invoke-direct {v0, v1}, Lcom/google/gmm/common/io/protocol/ProtoBuf;-><init>(Lcom/google/gmm/common/io/protocol/ProtoBufType;)V

    const/16 v23, 0x4

    move-object/from16 v0, v17

    move/from16 v1, v23

    move-object/from16 v2, v18

    invoke-virtual {v0, v1, v2}, Lcom/google/gmm/common/io/protocol/ProtoBuf;->addProtoBuf(ILcom/google/gmm/common/io/protocol/ProtoBuf;)V

    const/16 v23, 0x0

    move-object/from16 v0, p0

    move-object/from16 v1, v23

    invoke-static {v0, v1}, Lcom/google/android/location/os/real/GlsClient;->createPlatformProfile(Ljava/util/Locale;Lcom/google/gmm/common/io/protocol/ProtoBuf;)Lcom/google/gmm/common/io/protocol/ProtoBuf;

    move-result-object v14

    const/16 v23, 0x1

    move-object/from16 v0, v17

    move/from16 v1, v23

    invoke-virtual {v0, v1, v14}, Lcom/google/gmm/common/io/protocol/ProtoBuf;->setProtoBuf(ILcom/google/gmm/common/io/protocol/ProtoBuf;)V

    if-eqz p1, :cond_1

    new-instance v3, Lcom/google/gmm/common/io/protocol/ProtoBuf;

    sget-object v23, Lcom/google/android/location/protocol/LocserverMessageTypes;->GAPP_PROFILE:Lcom/google/gmm/common/io/protocol/ProtoBufType;

    move-object/from16 v0, v23

    invoke-direct {v3, v0}, Lcom/google/gmm/common/io/protocol/ProtoBuf;-><init>(Lcom/google/gmm/common/io/protocol/ProtoBufType;)V

    const/16 v23, 0x1

    move/from16 v0, v23

    move-object/from16 v1, p1

    invoke-virtual {v3, v0, v1}, Lcom/google/gmm/common/io/protocol/ProtoBuf;->setString(ILjava/lang/String;)V

    const/16 v23, 0x2

    move-object/from16 v0, v17

    move/from16 v1, v23

    invoke-virtual {v0, v1, v3}, Lcom/google/gmm/common/io/protocol/ProtoBuf;->setProtoBuf(ILcom/google/gmm/common/io/protocol/ProtoBuf;)V

    :cond_1
    new-instance v11, Ljava/io/ByteArrayOutputStream;

    invoke-direct {v11}, Ljava/io/ByteArrayOutputStream;-><init>()V

    :try_start_0
    move-object/from16 v0, v17

    invoke-virtual {v0, v11}, Lcom/google/gmm/common/io/protocol/ProtoBuf;->outputTo(Ljava/io/OutputStream;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    new-instance v15, Lcom/google/gmm/common/io/protocol/ProtoBuf;

    sget-object v23, Lcom/google/android/location/protocol/LocserverMessageTypes;->GLOC_REPLY:Lcom/google/gmm/common/io/protocol/ProtoBufType;

    move-object/from16 v0, v23

    invoke-direct {v15, v0}, Lcom/google/gmm/common/io/protocol/ProtoBuf;-><init>(Lcom/google/gmm/common/io/protocol/ProtoBufType;)V

    new-instance v12, Lcom/google/masf/protocol/PlainRequest;

    const-string v23, "g:loc/ql"

    const/16 v24, 0x0

    invoke-virtual {v11}, Ljava/io/ByteArrayOutputStream;->toByteArray()[B

    move-result-object v25

    move-object/from16 v0, v23

    move/from16 v1, v24

    move-object/from16 v2, v25

    invoke-direct {v12, v0, v1, v2}, Lcom/google/masf/protocol/PlainRequest;-><init>(Ljava/lang/String;I[B)V

    new-instance v7, Lcom/google/android/location/os/real/ProtoRequestListener;

    const/16 v23, 0x0

    move-object/from16 v0, v23

    invoke-direct {v7, v15, v0}, Lcom/google/android/location/os/real/ProtoRequestListener;-><init>(Lcom/google/gmm/common/io/protocol/ProtoBuf;Lcom/google/masf/ServiceCallback;)V

    invoke-virtual {v12, v7}, Lcom/google/masf/protocol/Request;->setListener(Lcom/google/masf/protocol/Request$Listener;)V

    invoke-static {}, Lcom/google/android/location/net/SynchronizedMobileServiceMux;->getSingleton()Lcom/google/android/location/net/SynchronizedMobileServiceMux;

    move-result-object v23

    const/16 v24, 0x1

    move-object/from16 v0, v23

    move/from16 v1, v24

    invoke-virtual {v0, v12, v1}, Lcom/google/android/location/net/SynchronizedMobileServiceMux;->submitRequest(Lcom/google/masf/protocol/Request;Z)V

    :try_start_1
    invoke-virtual {v7}, Lcom/google/android/location/os/real/ProtoRequestListener;->getAsyncResult()Lcom/google/masf/services/AsyncResult;

    move-result-object v23

    const-wide/16 v24, 0x1388

    invoke-virtual/range {v23 .. v25}, Lcom/google/masf/services/AsyncResult;->get(J)Ljava/lang/Object;

    move-result-object v19

    check-cast v19, Lcom/google/gmm/common/io/protocol/ProtoBuf;
    :try_end_1
    .catch Ljava/lang/InterruptedException; {:try_start_1 .. :try_end_1} :catch_1

    if-nez v19, :cond_2

    new-instance v23, Ljava/io/IOException;

    const-string v24, "Unable to parse response from server"

    invoke-direct/range {v23 .. v24}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v23

    :catch_0
    move-exception v5

    const-string v23, "GlsClient"

    const-string v24, "forwardGeocode(): unable to write request to payload"

    invoke-static/range {v23 .. v24}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    throw v5

    :catch_1
    move-exception v5

    const-string v23, "GlsClient"

    const-string v24, "forwardGeocode(): response timeout"

    invoke-static/range {v23 .. v24}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    new-instance v23, Ljava/io/IOException;

    const-string v24, "response time-out"

    invoke-direct/range {v23 .. v24}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v23

    :cond_2
    const/16 v23, 0x1

    move-object/from16 v0, v19

    move/from16 v1, v23

    invoke-virtual {v0, v1}, Lcom/google/gmm/common/io/protocol/ProtoBuf;->getInt(I)I

    move-result v20

    if-eqz v20, :cond_3

    const-string v23, "GlsClient"

    new-instance v24, Ljava/lang/StringBuilder;

    invoke-direct/range {v24 .. v24}, Ljava/lang/StringBuilder;-><init>()V

    const-string v25, "forwardGeocode(): RPC failed with status "

    invoke-virtual/range {v24 .. v25}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v24

    move-object/from16 v0, v24

    move/from16 v1, v20

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v24

    invoke-virtual/range {v24 .. v24}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v24

    invoke-static/range {v23 .. v24}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    new-instance v23, Ljava/io/IOException;

    new-instance v24, Ljava/lang/StringBuilder;

    invoke-direct/range {v24 .. v24}, Ljava/lang/StringBuilder;-><init>()V

    const-string v25, "RPC failed with status "

    invoke-virtual/range {v24 .. v25}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v24

    move-object/from16 v0, v24

    move/from16 v1, v20

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v24

    invoke-virtual/range {v24 .. v24}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v24

    invoke-direct/range {v23 .. v24}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v23

    :cond_3
    const/16 v23, 0x3

    move-object/from16 v0, v19

    move/from16 v1, v23

    invoke-virtual {v0, v1}, Lcom/google/gmm/common/io/protocol/ProtoBuf;->has(I)Z

    move-result v23

    if-eqz v23, :cond_4

    const/16 v23, 0x3

    move-object/from16 v0, v19

    move/from16 v1, v23

    invoke-virtual {v0, v1}, Lcom/google/gmm/common/io/protocol/ProtoBuf;->getString(I)Ljava/lang/String;

    move-result-object v13

    invoke-static {v13}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v23

    if-nez v23, :cond_4

    invoke-static {v13}, Lcom/google/android/location/os/real/GlsClient;->setPlatformKey(Ljava/lang/String;)V

    :cond_4
    const/16 v23, 0x2

    move-object/from16 v0, v19

    move/from16 v1, v23

    invoke-virtual {v0, v1}, Lcom/google/gmm/common/io/protocol/ProtoBuf;->has(I)Z

    move-result v23

    if-nez v23, :cond_5

    const-string v23, "GlsClient"

    const-string v24, "forwardGeocode(): no ReplyElement"

    invoke-static/range {v23 .. v24}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    :goto_0
    return-void

    :cond_5
    const/16 v23, 0x2

    move-object/from16 v0, v19

    move/from16 v1, v23

    invoke-virtual {v0, v1}, Lcom/google/gmm/common/io/protocol/ProtoBuf;->getProtoBuf(I)Lcom/google/gmm/common/io/protocol/ProtoBuf;

    move-result-object v16

    const/16 v23, 0x1

    move-object/from16 v0, v16

    move/from16 v1, v23

    invoke-virtual {v0, v1}, Lcom/google/gmm/common/io/protocol/ProtoBuf;->getInt(I)I

    move-result v21

    if-eqz v21, :cond_6

    const-string v23, "GlsClient"

    new-instance v24, Ljava/lang/StringBuilder;

    invoke-direct/range {v24 .. v24}, Ljava/lang/StringBuilder;-><init>()V

    const-string v25, "forwardGeocode(): GLS failed with status "

    invoke-virtual/range {v24 .. v25}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v24

    move-object/from16 v0, v24

    move/from16 v1, v21

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v24

    invoke-virtual/range {v24 .. v24}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v24

    invoke-static/range {v23 .. v24}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    :cond_6
    const/16 v23, 0x2

    move-object/from16 v0, v16

    move/from16 v1, v23

    invoke-virtual {v0, v1}, Lcom/google/gmm/common/io/protocol/ProtoBuf;->has(I)Z

    move-result v23

    if-nez v23, :cond_7

    const-string v23, "GlsClient"

    const-string v24, "forwardGeocode(): no location in ReplyElement"

    invoke-static/range {v23 .. v24}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    :cond_7
    const/16 v23, 0x2

    move-object/from16 v0, v16

    move/from16 v1, v23

    invoke-virtual {v0, v1}, Lcom/google/gmm/common/io/protocol/ProtoBuf;->getProtoBuf(I)Lcom/google/gmm/common/io/protocol/ProtoBuf;

    move-result-object v8

    const/16 v23, 0x5

    move/from16 v0, v23

    invoke-virtual {v8, v0}, Lcom/google/gmm/common/io/protocol/ProtoBuf;->has(I)Z

    move-result v23

    if-nez v23, :cond_8

    const-string v23, "GlsClient"

    const-string v24, "forwardGeocode(): no feature in GLocation"

    invoke-static/range {v23 .. v24}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    :cond_8
    move-object/from16 v0, p0

    move-object/from16 v1, p12

    invoke-static {v8, v0, v1}, Lcom/google/android/location/os/real/GlsClient;->getAddressFromProtoBuf(Lcom/google/gmm/common/io/protocol/ProtoBuf;Ljava/util/Locale;Ljava/util/List;)V

    goto :goto_0
.end method

.method private generateDebugInfo()Z
    .locals 3

    const/4 v0, 0x0

    sget-boolean v1, Lcom/google/android/location/os/real/GlsClient;->USE_GSERVICES:Z

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/google/android/location/os/real/GlsClient;->context:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    const-string v2, "network_location_provider_debug"

    invoke-static {v1, v2, v0}, Lcom/google/android/gsf/Gservices;->getString(Landroid/content/ContentResolver;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    :cond_0
    const-string v1, "verbose"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_1

    const-string v1, "on"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    :cond_1
    const/4 v1, 0x1

    :goto_0
    return v1

    :cond_2
    const/4 v1, 0x0

    goto :goto_0
.end method

.method private static getAddressFromProtoBuf(Lcom/google/gmm/common/io/protocol/ProtoBuf;Ljava/util/Locale;Ljava/util/List;)V
    .locals 21
    .param p0    # Lcom/google/gmm/common/io/protocol/ProtoBuf;
    .param p1    # Ljava/util/Locale;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/gmm/common/io/protocol/ProtoBuf;",
            "Ljava/util/Locale;",
            "Ljava/util/List",
            "<",
            "Landroid/location/Address;",
            ">;)V"
        }
    .end annotation

    const-wide/high16 v8, -0x4010000000000000L

    const-wide/high16 v12, -0x4010000000000000L

    const/16 v17, 0x1

    move-object/from16 v0, p0

    move/from16 v1, v17

    invoke-virtual {v0, v1}, Lcom/google/gmm/common/io/protocol/ProtoBuf;->has(I)Z

    move-result v17

    if-eqz v17, :cond_0

    const/16 v17, 0x1

    move-object/from16 v0, p0

    move/from16 v1, v17

    invoke-virtual {v0, v1}, Lcom/google/gmm/common/io/protocol/ProtoBuf;->getProtoBuf(I)Lcom/google/gmm/common/io/protocol/ProtoBuf;

    move-result-object v10

    const/16 v17, 0x1

    move/from16 v0, v17

    invoke-virtual {v10, v0}, Lcom/google/gmm/common/io/protocol/ProtoBuf;->getInt(I)I

    move-result v17

    move/from16 v0, v17

    int-to-double v0, v0

    move-wide/from16 v17, v0

    const-wide v19, 0x416312d000000000L

    div-double v8, v17, v19

    const/16 v17, 0x2

    move/from16 v0, v17

    invoke-virtual {v10, v0}, Lcom/google/gmm/common/io/protocol/ProtoBuf;->getInt(I)I

    move-result v17

    move/from16 v0, v17

    int-to-double v0, v0

    move-wide/from16 v17, v0

    const-wide v19, 0x416312d000000000L

    div-double v12, v17, v19

    :cond_0
    const/4 v2, 0x0

    :goto_0
    const/16 v17, 0x5

    move-object/from16 v0, p0

    move/from16 v1, v17

    invoke-virtual {v0, v1}, Lcom/google/gmm/common/io/protocol/ProtoBuf;->getCount(I)I

    move-result v17

    move/from16 v0, v17

    if-ge v2, v0, :cond_5

    new-instance v15, Landroid/location/Address;

    move-object/from16 v0, p1

    invoke-direct {v15, v0}, Landroid/location/Address;-><init>(Ljava/util/Locale;)V

    const/16 v17, 0x5

    move-object/from16 v0, p0

    move/from16 v1, v17

    invoke-virtual {v0, v1, v2}, Lcom/google/gmm/common/io/protocol/ProtoBuf;->getProtoBuf(II)Lcom/google/gmm/common/io/protocol/ProtoBuf;

    move-result-object v6

    const/16 v17, 0x1

    move/from16 v0, v17

    invoke-virtual {v6, v0}, Lcom/google/gmm/common/io/protocol/ProtoBuf;->getString(I)Ljava/lang/String;

    move-result-object v17

    move-object/from16 v0, v17

    invoke-virtual {v15, v0}, Landroid/location/Address;->setFeatureName(Ljava/lang/String;)V

    const/16 v17, 0x5

    move/from16 v0, v17

    invoke-virtual {v6, v0}, Lcom/google/gmm/common/io/protocol/ProtoBuf;->has(I)Z

    move-result v17

    if-eqz v17, :cond_2

    const/16 v17, 0x5

    move/from16 v0, v17

    invoke-virtual {v6, v0}, Lcom/google/gmm/common/io/protocol/ProtoBuf;->getProtoBuf(I)Lcom/google/gmm/common/io/protocol/ProtoBuf;

    move-result-object v4

    const/16 v17, 0x1

    move/from16 v0, v17

    invoke-virtual {v4, v0}, Lcom/google/gmm/common/io/protocol/ProtoBuf;->getInt(I)I

    move-result v17

    move/from16 v0, v17

    int-to-double v0, v0

    move-wide/from16 v17, v0

    const-wide v19, 0x416312d000000000L

    div-double v17, v17, v19

    move-wide/from16 v0, v17

    invoke-virtual {v15, v0, v1}, Landroid/location/Address;->setLatitude(D)V

    const/16 v17, 0x2

    move/from16 v0, v17

    invoke-virtual {v4, v0}, Lcom/google/gmm/common/io/protocol/ProtoBuf;->getInt(I)I

    move-result v17

    move/from16 v0, v17

    int-to-double v0, v0

    move-wide/from16 v17, v0

    const-wide v19, 0x416312d000000000L

    div-double v17, v17, v19

    move-wide/from16 v0, v17

    invoke-virtual {v15, v0, v1}, Landroid/location/Address;->setLongitude(D)V

    :cond_1
    :goto_1
    const/16 v17, 0x3

    move/from16 v0, v17

    invoke-virtual {v6, v0}, Lcom/google/gmm/common/io/protocol/ProtoBuf;->getProtoBuf(I)Lcom/google/gmm/common/io/protocol/ProtoBuf;

    move-result-object v3

    const/4 v7, 0x0

    :goto_2
    const/16 v17, 0x1

    move/from16 v0, v17

    invoke-virtual {v3, v0}, Lcom/google/gmm/common/io/protocol/ProtoBuf;->getCount(I)I

    move-result v17

    move/from16 v0, v17

    if-ge v7, v0, :cond_3

    const/16 v17, 0x1

    move/from16 v0, v17

    invoke-virtual {v3, v0, v7}, Lcom/google/gmm/common/io/protocol/ProtoBuf;->getString(II)Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v15, v7, v11}, Landroid/location/Address;->setAddressLine(ILjava/lang/String;)V

    add-int/lit8 v7, v7, 0x1

    goto :goto_2

    :cond_2
    const/16 v17, 0x1

    move-object/from16 v0, p0

    move/from16 v1, v17

    invoke-virtual {v0, v1}, Lcom/google/gmm/common/io/protocol/ProtoBuf;->has(I)Z

    move-result v17

    if-eqz v17, :cond_1

    invoke-virtual {v15, v8, v9}, Landroid/location/Address;->setLatitude(D)V

    invoke-virtual {v15, v12, v13}, Landroid/location/Address;->setLongitude(D)V

    goto :goto_1

    :cond_3
    const/4 v7, 0x0

    :goto_3
    const/16 v17, 0x2

    move/from16 v0, v17

    invoke-virtual {v3, v0}, Lcom/google/gmm/common/io/protocol/ProtoBuf;->getCount(I)I

    move-result v17

    move/from16 v0, v17

    if-ge v7, v0, :cond_4

    const/16 v17, 0x2

    move/from16 v0, v17

    invoke-virtual {v3, v0, v7}, Lcom/google/gmm/common/io/protocol/ProtoBuf;->getProtoBuf(II)Lcom/google/gmm/common/io/protocol/ProtoBuf;

    move-result-object v5

    const/16 v17, 0x2

    move/from16 v0, v17

    invoke-virtual {v5, v0}, Lcom/google/gmm/common/io/protocol/ProtoBuf;->getInt(I)I

    move-result v16

    const/16 v17, 0x1

    move/from16 v0, v17

    invoke-virtual {v5, v0}, Lcom/google/gmm/common/io/protocol/ProtoBuf;->getString(I)Ljava/lang/String;

    move-result-object v14

    packed-switch v16, :pswitch_data_0

    :goto_4
    add-int/lit8 v7, v7, 0x1

    goto :goto_3

    :pswitch_0
    invoke-virtual {v15, v14}, Landroid/location/Address;->setAdminArea(Ljava/lang/String;)V

    goto :goto_4

    :pswitch_1
    invoke-virtual {v15, v14}, Landroid/location/Address;->setSubAdminArea(Ljava/lang/String;)V

    goto :goto_4

    :pswitch_2
    invoke-virtual {v15, v14}, Landroid/location/Address;->setLocality(Ljava/lang/String;)V

    goto :goto_4

    :pswitch_3
    invoke-virtual {v15, v14}, Landroid/location/Address;->setSubLocality(Ljava/lang/String;)V

    goto :goto_4

    :pswitch_4
    invoke-virtual {v15, v14}, Landroid/location/Address;->setThoroughfare(Ljava/lang/String;)V

    goto :goto_4

    :pswitch_5
    invoke-virtual {v15, v14}, Landroid/location/Address;->setSubThoroughfare(Ljava/lang/String;)V

    goto :goto_4

    :pswitch_6
    invoke-virtual {v15, v14}, Landroid/location/Address;->setPremises(Ljava/lang/String;)V

    goto :goto_4

    :pswitch_7
    invoke-virtual {v15, v14}, Landroid/location/Address;->setPostalCode(Ljava/lang/String;)V

    goto :goto_4

    :pswitch_8
    invoke-virtual {v15, v14}, Landroid/location/Address;->setCountryName(Ljava/lang/String;)V

    goto :goto_4

    :pswitch_9
    invoke-virtual {v15, v14}, Landroid/location/Address;->setCountryCode(Ljava/lang/String;)V

    goto :goto_4

    :cond_4
    move-object/from16 v0, p2

    invoke-interface {v0, v15}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    add-int/lit8 v2, v2, 0x1

    goto/16 :goto_0

    :cond_5
    return-void

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_8
        :pswitch_9
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_6
        :pswitch_4
        :pswitch_5
        :pswitch_7
    .end packed-switch
.end method

.method private static getMasfConfig(Landroid/content/Context;)Lcom/google/masf/MobileServiceMux$Configuration;
    .locals 8
    .param p0    # Landroid/content/Context;

    const/4 v3, 0x0

    new-instance v0, Lcom/google/gmm/common/android/AndroidConfig;

    invoke-direct {v0, p0}, Lcom/google/gmm/common/android/AndroidConfig;-><init>(Landroid/content/Context;)V

    invoke-static {v0}, Lcom/google/gmm/common/Config;->setConfig(Lcom/google/gmm/common/Config;)V

    const-string v1, "https://www.google.com/loc/m/api"

    sget-boolean v5, Lcom/google/android/location/os/real/GlsClient;->USE_GSERVICES:Z

    if-eqz v5, :cond_0

    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v5

    const-string v6, "url:google_location_server"

    invoke-static {v5, v6, v3}, Lcom/google/android/gsf/Gservices;->getString(Landroid/content/ContentResolver;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    :cond_0
    if-eqz v3, :cond_1

    const-string v5, " "

    const/4 v6, 0x4

    invoke-virtual {v3, v5, v6}, Ljava/lang/String;->split(Ljava/lang/String;I)[Ljava/lang/String;

    move-result-object v4

    array-length v5, v4

    const/4 v6, 0x3

    if-ne v5, v6, :cond_1

    const/4 v5, 0x0

    aget-object v5, v4, v5

    invoke-virtual {v5, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_1

    const/4 v5, 0x1

    aget-object v5, v4, v5

    const-string v6, "rewrite"

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_1

    const/4 v5, 0x2

    aget-object v1, v4, v5

    const-string v5, "GlsClient"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "using "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Lcom/google/android/location/utils/logging/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    :cond_1
    new-instance v2, Lcom/google/masf/MobileServiceMux$Configuration;

    invoke-direct {v2}, Lcom/google/masf/MobileServiceMux$Configuration;-><init>()V

    invoke-virtual {v2, v1}, Lcom/google/masf/MobileServiceMux$Configuration;->setServerUri(Ljava/lang/String;)V

    const-string v5, "location"

    invoke-virtual {v2, v5}, Lcom/google/masf/MobileServiceMux$Configuration;->setApplicationName(Ljava/lang/String;)V

    sget-object v5, Lcom/google/android/location/os/real/GlsClient;->applicationVersion:Ljava/lang/String;

    invoke-virtual {v2, v5}, Lcom/google/masf/MobileServiceMux$Configuration;->setApplicationVersion(Ljava/lang/String;)V

    const-string v5, "android"

    invoke-virtual {v2, v5}, Lcom/google/masf/MobileServiceMux$Configuration;->setPlatformId(Ljava/lang/String;)V

    const-string v5, "gmm"

    invoke-virtual {v2, v5}, Lcom/google/masf/MobileServiceMux$Configuration;->setDistributionChannel(Ljava/lang/String;)V

    return-object v2
.end method

.method static declared-synchronized getPlatformKey()Ljava/lang/String;
    .locals 9

    const/4 v5, 0x0

    const-class v6, Lcom/google/android/location/os/real/GlsClient;

    monitor-enter v6

    :try_start_0
    sget-object v7, Lcom/google/android/location/os/real/GlsClient;->platformKey:Ljava/lang/String;

    if-eqz v7, :cond_0

    sget-object v5, Lcom/google/android/location/os/real/GlsClient;->platformKey:Ljava/lang/String;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :goto_0
    monitor-exit v6

    return-object v5

    :cond_0
    :try_start_1
    new-instance v2, Ljava/io/File;

    sget-object v7, Lcom/google/android/location/os/real/GlsClient;->cacheDir:Ljava/io/File;

    const-string v8, "nlp_GlsPlatformKey"

    invoke-direct {v2, v7, v8}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    new-instance v3, Ljava/io/BufferedInputStream;

    new-instance v7, Ljava/io/FileInputStream;

    invoke-direct {v7, v2}, Ljava/io/FileInputStream;-><init>(Ljava/io/File;)V

    invoke-direct {v3, v7}, Ljava/io/BufferedInputStream;-><init>(Ljava/io/InputStream;)V

    new-instance v0, Ljava/io/DataInputStream;

    invoke-direct {v0, v3}, Ljava/io/DataInputStream;-><init>(Ljava/io/InputStream;)V

    invoke-virtual {v0}, Ljava/io/DataInputStream;->readUTF()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0}, Ljava/io/DataInputStream;->close()V

    sput-object v4, Lcom/google/android/location/os/real/GlsClient;->platformKey:Ljava/lang/String;

    sget-object v5, Lcom/google/android/location/os/real/GlsClient;->platformKey:Ljava/lang/String;
    :try_end_1
    .catch Ljava/io/FileNotFoundException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    :catch_0
    move-exception v1

    goto :goto_0

    :catch_1
    move-exception v1

    goto :goto_0

    :catchall_0
    move-exception v5

    monitor-exit v6

    throw v5
.end method

.method public static declared-synchronized init(Landroid/content/Context;)V
    .locals 4
    .param p0    # Landroid/content/Context;

    const-class v2, Lcom/google/android/location/os/real/GlsClient;

    monitor-enter v2

    :try_start_0
    sget-object v1, Lcom/google/android/location/internal/NlpVersionInfo$NlpApk;->ANDROID:Lcom/google/android/location/internal/NlpVersionInfo$NlpApk;

    invoke-static {v1, p0}, Lcom/google/android/location/internal/NlpVersionInfo;->getNlpVersionInfo(Lcom/google/android/location/internal/NlpVersionInfo$NlpApk;Landroid/content/Context;)Lcom/google/android/location/internal/NlpVersionInfo;

    move-result-object v0

    iget v1, v0, Lcom/google/android/location/internal/NlpVersionInfo;->releaseVersion:I

    invoke-static {v1}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v1

    sput-object v1, Lcom/google/android/location/os/real/GlsClient;->applicationVersion:Ljava/lang/String;

    invoke-static {p0}, Lcom/google/android/location/os/real/GlsClient;->getMasfConfig(Landroid/content/Context;)Lcom/google/masf/MobileServiceMux$Configuration;

    move-result-object v1

    invoke-static {p0, v1}, Lcom/google/android/location/net/SynchronizedMobileServiceMux;->mightInitialize(Landroid/content/Context;Lcom/google/masf/MobileServiceMux$Configuration;)V

    sget-object v1, Landroid/os/Build;->FINGERPRINT:Ljava/lang/String;

    if-eqz v1, :cond_0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "android/"

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    sget-object v3, Landroid/os/Build;->FINGERPRINT:Ljava/lang/String;

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    sput-object v1, Lcom/google/android/location/os/real/GlsClient;->platformBuild:Ljava/lang/String;

    :goto_0
    invoke-virtual {p0}, Landroid/content/Context;->getCacheDir()Ljava/io/File;

    move-result-object v1

    sput-object v1, Lcom/google/android/location/os/real/GlsClient;->cacheDir:Ljava/io/File;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit v2

    return-void

    :cond_0
    :try_start_1
    const-string v1, "android"

    sput-object v1, Lcom/google/android/location/os/real/GlsClient;->platformBuild:Ljava/lang/String;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v1

    monitor-exit v2

    throw v1
.end method

.method public static reverseGeocode(Ljava/util/Locale;Ljava/lang/String;DDILjava/util/List;)V
    .locals 24
    .param p0    # Ljava/util/Locale;
    .param p1    # Ljava/lang/String;
    .param p2    # D
    .param p4    # D
    .param p6    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Locale;",
            "Ljava/lang/String;",
            "DDI",
            "Ljava/util/List",
            "<",
            "Landroid/location/Address;",
            ">;)V"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    new-instance v17, Lcom/google/gmm/common/io/protocol/ProtoBuf;

    sget-object v21, Lcom/google/android/location/protocol/LocserverMessageTypes;->GLOC_REQUEST_ELEMENT:Lcom/google/gmm/common/io/protocol/ProtoBufType;

    move-object/from16 v0, v17

    move-object/from16 v1, v21

    invoke-direct {v0, v1}, Lcom/google/gmm/common/io/protocol/ProtoBuf;-><init>(Lcom/google/gmm/common/io/protocol/ProtoBufType;)V

    new-instance v6, Lcom/google/gmm/common/io/protocol/ProtoBuf;

    sget-object v21, Lcom/google/android/location/protocol/LocserverMessageTypes;->GLAT_LNG:Lcom/google/gmm/common/io/protocol/ProtoBufType;

    move-object/from16 v0, v21

    invoke-direct {v6, v0}, Lcom/google/gmm/common/io/protocol/ProtoBuf;-><init>(Lcom/google/gmm/common/io/protocol/ProtoBufType;)V

    const/16 v21, 0x1

    const-wide v22, 0x416312d000000000L

    mul-double v22, v22, p2

    move-wide/from16 v0, v22

    double-to-int v0, v0

    move/from16 v22, v0

    move/from16 v0, v21

    move/from16 v1, v22

    invoke-virtual {v6, v0, v1}, Lcom/google/gmm/common/io/protocol/ProtoBuf;->setInt(II)V

    const/16 v21, 0x2

    const-wide v22, 0x416312d000000000L

    mul-double v22, v22, p4

    move-wide/from16 v0, v22

    double-to-int v0, v0

    move/from16 v22, v0

    move/from16 v0, v21

    move/from16 v1, v22

    invoke-virtual {v6, v0, v1}, Lcom/google/gmm/common/io/protocol/ProtoBuf;->setInt(II)V

    new-instance v9, Lcom/google/gmm/common/io/protocol/ProtoBuf;

    sget-object v21, Lcom/google/android/location/protocol/LocserverMessageTypes;->GLOCATION:Lcom/google/gmm/common/io/protocol/ProtoBufType;

    move-object/from16 v0, v21

    invoke-direct {v9, v0}, Lcom/google/gmm/common/io/protocol/ProtoBuf;-><init>(Lcom/google/gmm/common/io/protocol/ProtoBufType;)V

    const/16 v21, 0x1

    move/from16 v0, v21

    invoke-virtual {v9, v0, v6}, Lcom/google/gmm/common/io/protocol/ProtoBuf;->setProtoBuf(ILcom/google/gmm/common/io/protocol/ProtoBuf;)V

    const/16 v21, 0x6

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v22

    move/from16 v0, v21

    move-wide/from16 v1, v22

    invoke-virtual {v9, v0, v1, v2}, Lcom/google/gmm/common/io/protocol/ProtoBuf;->setLong(IJ)V

    const/16 v21, 0x3

    move-object/from16 v0, v17

    move/from16 v1, v21

    invoke-virtual {v0, v1, v9}, Lcom/google/gmm/common/io/protocol/ProtoBuf;->setProtoBuf(ILcom/google/gmm/common/io/protocol/ProtoBuf;)V

    new-instance v5, Lcom/google/gmm/common/io/protocol/ProtoBuf;

    sget-object v21, Lcom/google/android/location/protocol/LocserverMessageTypes;->GGEOCODE_REQUEST:Lcom/google/gmm/common/io/protocol/ProtoBufType;

    move-object/from16 v0, v21

    invoke-direct {v5, v0}, Lcom/google/gmm/common/io/protocol/ProtoBuf;-><init>(Lcom/google/gmm/common/io/protocol/ProtoBufType;)V

    const/16 v21, 0x1

    move/from16 v0, v21

    move/from16 v1, p6

    invoke-virtual {v5, v0, v1}, Lcom/google/gmm/common/io/protocol/ProtoBuf;->setInt(II)V

    const/16 v21, 0x4

    move-object/from16 v0, v17

    move/from16 v1, v21

    invoke-virtual {v0, v1, v5}, Lcom/google/gmm/common/io/protocol/ProtoBuf;->setProtoBuf(ILcom/google/gmm/common/io/protocol/ProtoBuf;)V

    new-instance v16, Lcom/google/gmm/common/io/protocol/ProtoBuf;

    sget-object v21, Lcom/google/android/location/protocol/LocserverMessageTypes;->GLOC_REQUEST:Lcom/google/gmm/common/io/protocol/ProtoBufType;

    move-object/from16 v0, v16

    move-object/from16 v1, v21

    invoke-direct {v0, v1}, Lcom/google/gmm/common/io/protocol/ProtoBuf;-><init>(Lcom/google/gmm/common/io/protocol/ProtoBufType;)V

    const/16 v21, 0x4

    move-object/from16 v0, v16

    move/from16 v1, v21

    move-object/from16 v2, v17

    invoke-virtual {v0, v1, v2}, Lcom/google/gmm/common/io/protocol/ProtoBuf;->addProtoBuf(ILcom/google/gmm/common/io/protocol/ProtoBuf;)V

    const/16 v21, 0x0

    move-object/from16 v0, p0

    move-object/from16 v1, v21

    invoke-static {v0, v1}, Lcom/google/android/location/os/real/GlsClient;->createPlatformProfile(Ljava/util/Locale;Lcom/google/gmm/common/io/protocol/ProtoBuf;)Lcom/google/gmm/common/io/protocol/ProtoBuf;

    move-result-object v13

    const/16 v21, 0x1

    move-object/from16 v0, v16

    move/from16 v1, v21

    invoke-virtual {v0, v1, v13}, Lcom/google/gmm/common/io/protocol/ProtoBuf;->setProtoBuf(ILcom/google/gmm/common/io/protocol/ProtoBuf;)V

    if-eqz p1, :cond_0

    new-instance v3, Lcom/google/gmm/common/io/protocol/ProtoBuf;

    sget-object v21, Lcom/google/android/location/protocol/LocserverMessageTypes;->GAPP_PROFILE:Lcom/google/gmm/common/io/protocol/ProtoBufType;

    move-object/from16 v0, v21

    invoke-direct {v3, v0}, Lcom/google/gmm/common/io/protocol/ProtoBuf;-><init>(Lcom/google/gmm/common/io/protocol/ProtoBufType;)V

    const/16 v21, 0x1

    move/from16 v0, v21

    move-object/from16 v1, p1

    invoke-virtual {v3, v0, v1}, Lcom/google/gmm/common/io/protocol/ProtoBuf;->setString(ILjava/lang/String;)V

    const/16 v21, 0x2

    move-object/from16 v0, v16

    move/from16 v1, v21

    invoke-virtual {v0, v1, v3}, Lcom/google/gmm/common/io/protocol/ProtoBuf;->setProtoBuf(ILcom/google/gmm/common/io/protocol/ProtoBuf;)V

    :cond_0
    new-instance v10, Ljava/io/ByteArrayOutputStream;

    invoke-direct {v10}, Ljava/io/ByteArrayOutputStream;-><init>()V

    :try_start_0
    move-object/from16 v0, v16

    invoke-virtual {v0, v10}, Lcom/google/gmm/common/io/protocol/ProtoBuf;->outputTo(Ljava/io/OutputStream;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    new-instance v14, Lcom/google/gmm/common/io/protocol/ProtoBuf;

    sget-object v21, Lcom/google/android/location/protocol/LocserverMessageTypes;->GLOC_REPLY:Lcom/google/gmm/common/io/protocol/ProtoBufType;

    move-object/from16 v0, v21

    invoke-direct {v14, v0}, Lcom/google/gmm/common/io/protocol/ProtoBuf;-><init>(Lcom/google/gmm/common/io/protocol/ProtoBufType;)V

    new-instance v11, Lcom/google/masf/protocol/PlainRequest;

    const-string v21, "g:loc/ql"

    const/16 v22, 0x0

    invoke-virtual {v10}, Ljava/io/ByteArrayOutputStream;->toByteArray()[B

    move-result-object v23

    move-object/from16 v0, v21

    move/from16 v1, v22

    move-object/from16 v2, v23

    invoke-direct {v11, v0, v1, v2}, Lcom/google/masf/protocol/PlainRequest;-><init>(Ljava/lang/String;I[B)V

    new-instance v7, Lcom/google/android/location/os/real/ProtoRequestListener;

    const/16 v21, 0x0

    move-object/from16 v0, v21

    invoke-direct {v7, v14, v0}, Lcom/google/android/location/os/real/ProtoRequestListener;-><init>(Lcom/google/gmm/common/io/protocol/ProtoBuf;Lcom/google/masf/ServiceCallback;)V

    invoke-virtual {v11, v7}, Lcom/google/masf/protocol/Request;->setListener(Lcom/google/masf/protocol/Request$Listener;)V

    invoke-static {}, Lcom/google/android/location/net/SynchronizedMobileServiceMux;->getSingleton()Lcom/google/android/location/net/SynchronizedMobileServiceMux;

    move-result-object v21

    const/16 v22, 0x1

    move-object/from16 v0, v21

    move/from16 v1, v22

    invoke-virtual {v0, v11, v1}, Lcom/google/android/location/net/SynchronizedMobileServiceMux;->submitRequest(Lcom/google/masf/protocol/Request;Z)V

    :try_start_1
    invoke-virtual {v7}, Lcom/google/android/location/os/real/ProtoRequestListener;->getAsyncResult()Lcom/google/masf/services/AsyncResult;

    move-result-object v21

    const-wide/16 v22, 0x1388

    invoke-virtual/range {v21 .. v23}, Lcom/google/masf/services/AsyncResult;->get(J)Ljava/lang/Object;

    move-result-object v18

    check-cast v18, Lcom/google/gmm/common/io/protocol/ProtoBuf;
    :try_end_1
    .catch Ljava/lang/InterruptedException; {:try_start_1 .. :try_end_1} :catch_1

    if-nez v18, :cond_1

    new-instance v21, Ljava/io/IOException;

    const-string v22, "Unable to parse response from server"

    invoke-direct/range {v21 .. v22}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v21

    :catch_0
    move-exception v4

    const-string v21, "GlsClient"

    const-string v22, "reverseGeocode(): unable to write request to payload"

    invoke-static/range {v21 .. v22}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    throw v4

    :catch_1
    move-exception v4

    const-string v21, "GlsClient"

    const-string v22, "reverseGeocode(): response timeout"

    invoke-static/range {v21 .. v22}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    new-instance v21, Ljava/io/IOException;

    const-string v22, "response time-out"

    invoke-direct/range {v21 .. v22}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v21

    :cond_1
    const/16 v21, 0x1

    move-object/from16 v0, v18

    move/from16 v1, v21

    invoke-virtual {v0, v1}, Lcom/google/gmm/common/io/protocol/ProtoBuf;->getInt(I)I

    move-result v19

    if-eqz v19, :cond_2

    const-string v21, "GlsClient"

    new-instance v22, Ljava/lang/StringBuilder;

    invoke-direct/range {v22 .. v22}, Ljava/lang/StringBuilder;-><init>()V

    const-string v23, "reverseGeocode(): RPC failed with status "

    invoke-virtual/range {v22 .. v23}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v22

    move-object/from16 v0, v22

    move/from16 v1, v19

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v22

    invoke-virtual/range {v22 .. v22}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v22

    invoke-static/range {v21 .. v22}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    new-instance v21, Ljava/io/IOException;

    new-instance v22, Ljava/lang/StringBuilder;

    invoke-direct/range {v22 .. v22}, Ljava/lang/StringBuilder;-><init>()V

    const-string v23, "RPC failed with status "

    invoke-virtual/range {v22 .. v23}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v22

    move-object/from16 v0, v22

    move/from16 v1, v19

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v22

    invoke-virtual/range {v22 .. v22}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v22

    invoke-direct/range {v21 .. v22}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v21

    :cond_2
    const/16 v21, 0x3

    move-object/from16 v0, v18

    move/from16 v1, v21

    invoke-virtual {v0, v1}, Lcom/google/gmm/common/io/protocol/ProtoBuf;->has(I)Z

    move-result v21

    if-eqz v21, :cond_3

    const/16 v21, 0x3

    move-object/from16 v0, v18

    move/from16 v1, v21

    invoke-virtual {v0, v1}, Lcom/google/gmm/common/io/protocol/ProtoBuf;->getString(I)Ljava/lang/String;

    move-result-object v12

    invoke-static {v12}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v21

    if-nez v21, :cond_3

    invoke-static {v12}, Lcom/google/android/location/os/real/GlsClient;->setPlatformKey(Ljava/lang/String;)V

    :cond_3
    const/16 v21, 0x2

    move-object/from16 v0, v18

    move/from16 v1, v21

    invoke-virtual {v0, v1}, Lcom/google/gmm/common/io/protocol/ProtoBuf;->has(I)Z

    move-result v21

    if-nez v21, :cond_4

    const-string v21, "GlsClient"

    const-string v22, "reverseGeocode(): no ReplyElement"

    invoke-static/range {v21 .. v22}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    :goto_0
    return-void

    :cond_4
    const/16 v21, 0x2

    move-object/from16 v0, v18

    move/from16 v1, v21

    invoke-virtual {v0, v1}, Lcom/google/gmm/common/io/protocol/ProtoBuf;->getProtoBuf(I)Lcom/google/gmm/common/io/protocol/ProtoBuf;

    move-result-object v15

    const/16 v21, 0x1

    move/from16 v0, v21

    invoke-virtual {v15, v0}, Lcom/google/gmm/common/io/protocol/ProtoBuf;->getInt(I)I

    move-result v20

    if-eqz v20, :cond_5

    const-string v21, "GlsClient"

    new-instance v22, Ljava/lang/StringBuilder;

    invoke-direct/range {v22 .. v22}, Ljava/lang/StringBuilder;-><init>()V

    const-string v23, "reverseGeocode(): GLS failed with status "

    invoke-virtual/range {v22 .. v23}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v22

    move-object/from16 v0, v22

    move/from16 v1, v20

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v22

    invoke-virtual/range {v22 .. v22}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v22

    invoke-static/range {v21 .. v22}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    :cond_5
    const/16 v21, 0x2

    move/from16 v0, v21

    invoke-virtual {v15, v0}, Lcom/google/gmm/common/io/protocol/ProtoBuf;->has(I)Z

    move-result v21

    if-nez v21, :cond_6

    const-string v21, "GlsClient"

    const-string v22, "reverseGeocode(): no location in ReplyElement"

    invoke-static/range {v21 .. v22}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    :cond_6
    const/16 v21, 0x2

    move/from16 v0, v21

    invoke-virtual {v15, v0}, Lcom/google/gmm/common/io/protocol/ProtoBuf;->getProtoBuf(I)Lcom/google/gmm/common/io/protocol/ProtoBuf;

    move-result-object v8

    const/16 v21, 0x5

    move/from16 v0, v21

    invoke-virtual {v8, v0}, Lcom/google/gmm/common/io/protocol/ProtoBuf;->has(I)Z

    move-result v21

    if-nez v21, :cond_7

    const-string v21, "GlsClient"

    const-string v22, "reverseGeocode(): no feature in GLocation"

    invoke-static/range {v21 .. v22}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    :cond_7
    move-object/from16 v0, p0

    move-object/from16 v1, p7

    invoke-static {v8, v0, v1}, Lcom/google/android/location/os/real/GlsClient;->getAddressFromProtoBuf(Lcom/google/gmm/common/io/protocol/ProtoBuf;Ljava/util/Locale;Ljava/util/List;)V

    goto :goto_0
.end method

.method private static declared-synchronized setPlatformKey(Ljava/lang/String;)V
    .locals 7
    .param p0    # Ljava/lang/String;

    const-class v5, Lcom/google/android/location/os/real/GlsClient;

    monitor-enter v5

    :try_start_0
    sget-object v4, Lcom/google/android/location/os/real/GlsClient;->cacheDir:Ljava/io/File;

    invoke-virtual {v4}, Ljava/io/File;->exists()Z

    move-result v4

    if-nez v4, :cond_0

    sget-object v4, Lcom/google/android/location/os/real/GlsClient;->cacheDir:Ljava/io/File;

    invoke-virtual {v4}, Ljava/io/File;->mkdirs()Z

    move-result v4

    if-nez v4, :cond_0

    const-string v4, "GlsClient"

    const-string v6, "setPlatformKey(): couldn\'t create directory"

    invoke-static {v4, v6}, Lcom/google/android/location/utils/logging/Logger;->w(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :goto_0
    monitor-exit v5

    return-void

    :cond_0
    :try_start_1
    new-instance v2, Ljava/io/File;

    sget-object v4, Lcom/google/android/location/os/real/GlsClient;->cacheDir:Ljava/io/File;

    const-string v6, "nlp_GlsPlatformKey"

    invoke-direct {v2, v4, v6}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    new-instance v3, Ljava/io/BufferedOutputStream;

    new-instance v4, Ljava/io/FileOutputStream;

    invoke-direct {v4, v2}, Ljava/io/FileOutputStream;-><init>(Ljava/io/File;)V

    invoke-direct {v3, v4}, Ljava/io/BufferedOutputStream;-><init>(Ljava/io/OutputStream;)V

    new-instance v0, Ljava/io/DataOutputStream;

    invoke-direct {v0, v3}, Ljava/io/DataOutputStream;-><init>(Ljava/io/OutputStream;)V

    invoke-virtual {v0, p0}, Ljava/io/DataOutputStream;->writeUTF(Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/io/DataOutputStream;->close()V

    sput-object p0, Lcom/google/android/location/os/real/GlsClient;->platformKey:Ljava/lang/String;
    :try_end_1
    .catch Ljava/io/FileNotFoundException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    :catch_0
    move-exception v1

    :try_start_2
    const-string v4, "GlsClient"

    const-string v6, "setPlatformKey(): unable to create platform key file"

    invoke-static {v4, v6}, Lcom/google/android/location/utils/logging/Logger;->w(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v4

    monitor-exit v5

    throw v4

    :catch_1
    move-exception v1

    :try_start_3
    const-string v4, "GlsClient"

    const-string v6, "setPlatformKey(): unable to write to platform key"

    invoke-static {v4, v6}, Lcom/google/android/location/utils/logging/Logger;->w(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto :goto_0
.end method

.method private updateDebugProfileLocked(Lcom/google/gmm/common/io/protocol/ProtoBuf;)V
    .locals 9
    .param p1    # Lcom/google/gmm/common/io/protocol/ProtoBuf;

    const/4 v3, 0x0

    const/4 v8, 0x0

    iget-boolean v6, p0, Lcom/google/android/location/os/real/GlsClient;->mDeviceRestart:Z

    if-eqz v6, :cond_0

    const/4 v6, 0x4

    const/4 v7, 0x1

    invoke-virtual {p1, v6, v7}, Lcom/google/gmm/common/io/protocol/ProtoBuf;->setBool(IZ)V

    iput-boolean v8, p0, Lcom/google/android/location/os/real/GlsClient;->mDeviceRestart:Z

    :cond_0
    sget-boolean v6, Lcom/google/android/location/os/real/GlsClient;->USE_GSERVICES:Z

    if-eqz v6, :cond_1

    iget-object v6, p0, Lcom/google/android/location/os/real/GlsClient;->context:Landroid/content/Context;

    invoke-virtual {v6}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v6

    const-string v7, "network_location_provider_debug"

    invoke-static {v6, v7, v3}, Lcom/google/android/gsf/Gservices;->getString(Landroid/content/ContentResolver;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    :cond_1
    if-eqz v3, :cond_2

    const/4 v6, 0x6

    invoke-virtual {p1, v6, v8}, Lcom/google/gmm/common/io/protocol/ProtoBuf;->setBool(IZ)V

    iget-object v6, p0, Lcom/google/android/location/os/real/GlsClient;->context:Landroid/content/Context;

    invoke-static {v6}, Landroid/accounts/AccountManager;->get(Landroid/content/Context;)Landroid/accounts/AccountManager;

    move-result-object v1

    const-string v6, "com.google"

    invoke-virtual {v1, v6}, Landroid/accounts/AccountManager;->getAccountsByType(Ljava/lang/String;)[Landroid/accounts/Account;

    move-result-object v2

    array-length v5, v2

    const/4 v4, 0x0

    :goto_0
    if-ge v4, v5, :cond_2

    aget-object v0, v2, v4

    const/4 v6, 0x5

    iget-object v7, v0, Landroid/accounts/Account;->name:Ljava/lang/String;

    invoke-virtual {p1, v6, v7}, Lcom/google/gmm/common/io/protocol/ProtoBuf;->addString(ILjava/lang/String;)V

    const-string v6, "GlsClient"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "adding account "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    iget-object v8, v0, Landroid/accounts/Account;->name:Ljava/lang/String;

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Lcom/google/android/location/utils/logging/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    add-int/lit8 v4, v4, 0x1

    goto :goto_0

    :cond_2
    return-void
.end method


# virtual methods
.method public deviceLocationQuery(Lcom/google/gmm/common/io/protocol/ProtoBuf;)V
    .locals 2
    .param p1    # Lcom/google/gmm/common/io/protocol/ProtoBuf;

    iget-object v0, p0, Lcom/google/android/location/os/real/GlsClient;->deviceLocationQueryListener:Lcom/google/android/location/os/real/GlsClient$RequestListener;

    const/4 v1, 0x0

    invoke-virtual {v0, p1, v1}, Lcom/google/android/location/os/real/GlsClient$RequestListener;->sendRpc(Lcom/google/gmm/common/io/protocol/ProtoBuf;Lcom/google/gmm/common/io/protocol/ProtoBuf;)V

    return-void
.end method

.method public modelQuery(Lcom/google/gmm/common/io/protocol/ProtoBuf;)V
    .locals 2
    .param p1    # Lcom/google/gmm/common/io/protocol/ProtoBuf;

    iget-object v0, p0, Lcom/google/android/location/os/real/GlsClient;->modelQueryListener:Lcom/google/android/location/os/real/GlsClient$RequestListener;

    const/4 v1, 0x0

    invoke-virtual {v0, p1, v1}, Lcom/google/android/location/os/real/GlsClient$RequestListener;->sendRpc(Lcom/google/gmm/common/io/protocol/ProtoBuf;Lcom/google/gmm/common/io/protocol/ProtoBuf;)V

    return-void
.end method

.method public query(Lcom/google/gmm/common/io/protocol/ProtoBuf;Lcom/google/gmm/common/io/protocol/ProtoBuf;)V
    .locals 1
    .param p1    # Lcom/google/gmm/common/io/protocol/ProtoBuf;
    .param p2    # Lcom/google/gmm/common/io/protocol/ProtoBuf;

    iget-object v0, p0, Lcom/google/android/location/os/real/GlsClient;->queryListener:Lcom/google/android/location/os/real/GlsClient$RequestListener;

    invoke-virtual {v0, p1, p2}, Lcom/google/android/location/os/real/GlsClient$RequestListener;->sendRpc(Lcom/google/gmm/common/io/protocol/ProtoBuf;Lcom/google/gmm/common/io/protocol/ProtoBuf;)V

    return-void
.end method
