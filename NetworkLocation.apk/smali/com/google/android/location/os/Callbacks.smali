.class public interface abstract Lcom/google/android/location/os/Callbacks;
.super Ljava/lang/Object;
.source "Callbacks.java"


# virtual methods
.method public abstract airplaneModeChanged(Z)V
.end method

.method public abstract alarmRing(I)V
.end method

.method public abstract batteryChanged(IIZ)V
.end method

.method public abstract cellScanResults(Lcom/google/android/location/data/CellState;)V
.end method

.method public abstract cellSignalStrength(I)V
.end method

.method public abstract dataNetworkChanged(ZZ)V
.end method

.method public abstract glsDeviceLocationResponse(Lcom/google/gmm/common/io/protocol/ProtoBuf;)V
.end method

.method public abstract glsModelQueryResponse(Lcom/google/gmm/common/io/protocol/ProtoBuf;)V
.end method

.method public abstract glsQueryResponse(Lcom/google/gmm/common/io/protocol/ProtoBuf;)V
.end method

.method public abstract initialize()V
.end method

.method public abstract nlpParamtersStateChanged(Lcom/google/android/location/os/NlpParametersState;)V
.end method

.method public abstract onActivityDetected(Lcom/google/android/location/clientlib/NlpActivity;)V
.end method

.method public abstract onActivityDetectionEnabled(Z)V
.end method

.method public abstract onTravelModeDetected(Lcom/google/android/location/activity/TravelDetectionManager$TravelDetectionResult;)V
.end method

.method public abstract quit(Z)V
.end method

.method public abstract screenStateChanged(Z)V
.end method

.method public abstract setPeriod(IZ)V
.end method

.method public abstract signalRmiRequest(Lcom/google/android/location/data/NetworkLocation;)V
.end method

.method public abstract wifiScanResults(Lcom/google/android/location/data/WifiScan;)V
.end method

.method public abstract wifiStateChanged(Z)V
.end method
