.class public Lcom/google/android/location/os/SimpleProtoBufStore;
.super Ljava/lang/Object;
.source "SimpleProtoBufStore.java"


# instance fields
.field private final VERSION:I

.field private final file:Ljava/io/File;

.field private final fileSystem:Lcom/google/android/location/os/FileSystem;

.field private final isValidPredicate:Lcom/google/common/base/Predicate;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/common/base/Predicate",
            "<",
            "Lcom/google/gmm/common/io/protocol/ProtoBuf;",
            ">;"
        }
    .end annotation
.end field

.field private final key:Ljavax/crypto/SecretKey;

.field private final protoBufType:Lcom/google/gmm/common/io/protocol/ProtoBufType;


# direct methods
.method public constructor <init>(ILcom/google/gmm/common/io/protocol/ProtoBufType;Ljava/io/File;Lcom/google/common/base/Predicate;Lcom/google/android/location/os/FileSystem;Ljavax/crypto/SecretKey;)V
    .locals 0
    .param p1    # I
    .param p2    # Lcom/google/gmm/common/io/protocol/ProtoBufType;
    .param p3    # Ljava/io/File;
    .param p5    # Lcom/google/android/location/os/FileSystem;
    .param p6    # Ljavax/crypto/SecretKey;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Lcom/google/gmm/common/io/protocol/ProtoBufType;",
            "Ljava/io/File;",
            "Lcom/google/common/base/Predicate",
            "<",
            "Lcom/google/gmm/common/io/protocol/ProtoBuf;",
            ">;",
            "Lcom/google/android/location/os/FileSystem;",
            "Ljavax/crypto/SecretKey;",
            ")V"
        }
    .end annotation

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p2, p0, Lcom/google/android/location/os/SimpleProtoBufStore;->protoBufType:Lcom/google/gmm/common/io/protocol/ProtoBufType;

    iput-object p5, p0, Lcom/google/android/location/os/SimpleProtoBufStore;->fileSystem:Lcom/google/android/location/os/FileSystem;

    iput-object p3, p0, Lcom/google/android/location/os/SimpleProtoBufStore;->file:Ljava/io/File;

    iput p1, p0, Lcom/google/android/location/os/SimpleProtoBufStore;->VERSION:I

    iput-object p6, p0, Lcom/google/android/location/os/SimpleProtoBufStore;->key:Ljavax/crypto/SecretKey;

    iput-object p4, p0, Lcom/google/android/location/os/SimpleProtoBufStore;->isValidPredicate:Lcom/google/common/base/Predicate;

    return-void
.end method

.method private closeStream(Ljava/io/Closeable;)V
    .locals 1
    .param p1    # Ljava/io/Closeable;

    if-eqz p1, :cond_0

    :try_start_0
    invoke-interface {p1}, Ljava/io/Closeable;->close()V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    :cond_0
    :goto_0
    return-void

    :catch_0
    move-exception v0

    goto :goto_0
.end method

.method private saveTo(Lcom/google/gmm/common/io/protocol/ProtoBuf;Ljava/io/OutputStream;)Z
    .locals 5
    .param p1    # Lcom/google/gmm/common/io/protocol/ProtoBuf;
    .param p2    # Ljava/io/OutputStream;

    const/4 v1, 0x0

    const/4 v0, 0x0

    :try_start_0
    new-instance v2, Ljava/io/DataOutputStream;

    invoke-direct {v2, p2}, Ljava/io/DataOutputStream;-><init>(Ljava/io/OutputStream;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :try_start_1
    iget v4, p0, Lcom/google/android/location/os/SimpleProtoBufStore;->VERSION:I

    invoke-virtual {v2, v4}, Ljava/io/DataOutputStream;->writeShort(I)V

    invoke-virtual {v2}, Ljava/io/DataOutputStream;->flush()V

    iget-object v4, p0, Lcom/google/android/location/os/SimpleProtoBufStore;->key:Ljavax/crypto/SecretKey;

    invoke-static {v2, v4}, Lcom/google/android/location/os/CipherStreams;->newBufferedCipherOutputStream(Ljava/io/OutputStream;Ljavax/crypto/SecretKey;)Ljava/io/OutputStream;

    move-result-object v0

    invoke-virtual {p1}, Lcom/google/gmm/common/io/protocol/ProtoBuf;->toByteArray()[B

    move-result-object v4

    invoke-virtual {v0, v4}, Ljava/io/OutputStream;->write([B)V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    const/4 v4, 0x1

    invoke-direct {p0, v0}, Lcom/google/android/location/os/SimpleProtoBufStore;->closeStream(Ljava/io/Closeable;)V

    invoke-direct {p0, v2}, Lcom/google/android/location/os/SimpleProtoBufStore;->closeStream(Ljava/io/Closeable;)V

    move-object v1, v2

    :goto_0
    return v4

    :catch_0
    move-exception v3

    :goto_1
    const/4 v4, 0x0

    invoke-direct {p0, v0}, Lcom/google/android/location/os/SimpleProtoBufStore;->closeStream(Ljava/io/Closeable;)V

    invoke-direct {p0, v1}, Lcom/google/android/location/os/SimpleProtoBufStore;->closeStream(Ljava/io/Closeable;)V

    goto :goto_0

    :catchall_0
    move-exception v4

    :goto_2
    invoke-direct {p0, v0}, Lcom/google/android/location/os/SimpleProtoBufStore;->closeStream(Ljava/io/Closeable;)V

    invoke-direct {p0, v1}, Lcom/google/android/location/os/SimpleProtoBufStore;->closeStream(Ljava/io/Closeable;)V

    throw v4

    :catchall_1
    move-exception v4

    move-object v1, v2

    goto :goto_2

    :catch_1
    move-exception v3

    move-object v1, v2

    goto :goto_1
.end method


# virtual methods
.method public load()Lcom/google/gmm/common/io/protocol/ProtoBuf;
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    :try_start_0
    new-instance v1, Ljava/io/FileInputStream;

    iget-object v2, p0, Lcom/google/android/location/os/SimpleProtoBufStore;->file:Ljava/io/File;

    invoke-direct {v1, v2}, Ljava/io/FileInputStream;-><init>(Ljava/io/File;)V

    invoke-virtual {p0, v1}, Lcom/google/android/location/os/SimpleProtoBufStore;->load(Ljava/io/InputStream;)Lcom/google/gmm/common/io/protocol/ProtoBuf;
    :try_end_0
    .catch Ljava/io/FileNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    return-object v1

    :catch_0
    move-exception v0

    new-instance v1, Ljava/io/IOException;

    const-string v2, "File not found."

    invoke-direct {v1, v2}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v1
.end method

.method load(Ljava/io/InputStream;)Lcom/google/gmm/common/io/protocol/ProtoBuf;
    .locals 12
    .param p1    # Ljava/io/InputStream;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    const/4 v0, 0x0

    const/4 v1, 0x0

    :try_start_0
    new-instance v2, Ljava/io/DataInputStream;

    invoke-direct {v2, p1}, Ljava/io/DataInputStream;-><init>(Ljava/io/InputStream;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    :try_start_1
    invoke-virtual {v2}, Ljava/io/DataInputStream;->readUnsignedShort()I

    move-result v6

    iget v7, p0, Lcom/google/android/location/os/SimpleProtoBufStore;->VERSION:I

    if-eq v6, v7, :cond_0

    new-instance v7, Ljava/io/IOException;

    const-string v8, "Invalid version, desired = %d, actual = %d"

    const/4 v9, 0x2

    new-array v9, v9, [Ljava/lang/Object;

    const/4 v10, 0x0

    iget v11, p0, Lcom/google/android/location/os/SimpleProtoBufStore;->VERSION:I

    invoke-static {v11}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v11

    aput-object v11, v9, v10

    const/4 v10, 0x1

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v11

    aput-object v11, v9, v10

    invoke-static {v8, v9}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v8

    invoke-direct {v7, v8}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v7
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    :catchall_0
    move-exception v7

    move-object v1, v2

    :goto_0
    invoke-direct {p0, v0}, Lcom/google/android/location/os/SimpleProtoBufStore;->closeStream(Ljava/io/Closeable;)V

    invoke-direct {p0, v1}, Lcom/google/android/location/os/SimpleProtoBufStore;->closeStream(Ljava/io/Closeable;)V

    throw v7

    :cond_0
    :try_start_2
    iget-object v7, p0, Lcom/google/android/location/os/SimpleProtoBufStore;->key:Ljavax/crypto/SecretKey;

    invoke-static {v2, v7}, Lcom/google/android/location/os/CipherStreams;->newBufferedCipherInputStream(Ljava/io/InputStream;Ljavax/crypto/SecretKey;)Ljava/io/InputStream;

    move-result-object v0

    new-instance v4, Lcom/google/gmm/common/io/protocol/ProtoBuf;

    iget-object v7, p0, Lcom/google/android/location/os/SimpleProtoBufStore;->protoBufType:Lcom/google/gmm/common/io/protocol/ProtoBufType;

    invoke-direct {v4, v7}, Lcom/google/gmm/common/io/protocol/ProtoBuf;-><init>(Lcom/google/gmm/common/io/protocol/ProtoBufType;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    :try_start_3
    invoke-virtual {v4, v0}, Lcom/google/gmm/common/io/protocol/ProtoBuf;->parse(Ljava/io/InputStream;)Lcom/google/gmm/common/io/protocol/ProtoBuf;

    invoke-virtual {v4}, Lcom/google/gmm/common/io/protocol/ProtoBuf;->isValid()Z
    :try_end_3
    .catch Ljava/lang/Throwable; {:try_start_3 .. :try_end_3} :catch_0
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    move-result v3

    if-eqz v3, :cond_1

    :try_start_4
    iget-object v7, p0, Lcom/google/android/location/os/SimpleProtoBufStore;->isValidPredicate:Lcom/google/common/base/Predicate;

    invoke-interface {v7, v4}, Lcom/google/common/base/Predicate;->apply(Ljava/lang/Object;)Z

    move-result v7

    if-nez v7, :cond_2

    :cond_1
    new-instance v7, Ljava/io/IOException;

    const-string v8, "Invalid file format."

    invoke-direct {v7, v8}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v7

    :catch_0
    move-exception v5

    new-instance v7, Ljava/io/IOException;

    const-string v8, "Invalid file format."

    invoke-direct {v7, v8, v5}, Ljava/io/IOException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v7
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    :cond_2
    invoke-direct {p0, v0}, Lcom/google/android/location/os/SimpleProtoBufStore;->closeStream(Ljava/io/Closeable;)V

    invoke-direct {p0, v2}, Lcom/google/android/location/os/SimpleProtoBufStore;->closeStream(Ljava/io/Closeable;)V

    return-object v4

    :catchall_1
    move-exception v7

    goto :goto_0
.end method

.method public save(Lcom/google/gmm/common/io/protocol/ProtoBuf;)Z
    .locals 3
    .param p1    # Lcom/google/gmm/common/io/protocol/ProtoBuf;

    const-string v1, "protoBuf can not be null."

    invoke-static {p1, v1}, Lcom/google/common/base/Preconditions;->checkNotNull(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    :try_start_0
    iget-object v1, p0, Lcom/google/android/location/os/SimpleProtoBufStore;->file:Ljava/io/File;

    invoke-virtual {v1}, Ljava/io/File;->exists()Z

    move-result v1

    if-nez v1, :cond_0

    iget-object v1, p0, Lcom/google/android/location/os/SimpleProtoBufStore;->file:Ljava/io/File;

    invoke-virtual {v1}, Ljava/io/File;->createNewFile()Z

    :cond_0
    iget-object v1, p0, Lcom/google/android/location/os/SimpleProtoBufStore;->fileSystem:Lcom/google/android/location/os/FileSystem;

    iget-object v2, p0, Lcom/google/android/location/os/SimpleProtoBufStore;->file:Ljava/io/File;

    invoke-interface {v1, v2}, Lcom/google/android/location/os/FileSystem;->makeFilePrivate(Ljava/io/File;)V

    new-instance v1, Ljava/io/FileOutputStream;

    iget-object v2, p0, Lcom/google/android/location/os/SimpleProtoBufStore;->file:Ljava/io/File;

    invoke-direct {v1, v2}, Ljava/io/FileOutputStream;-><init>(Ljava/io/File;)V

    invoke-direct {p0, p1, v1}, Lcom/google/android/location/os/SimpleProtoBufStore;->saveTo(Lcom/google/gmm/common/io/protocol/ProtoBuf;Ljava/io/OutputStream;)Z
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    :goto_0
    return v1

    :catch_0
    move-exception v0

    const/4 v1, 0x0

    goto :goto_0
.end method
