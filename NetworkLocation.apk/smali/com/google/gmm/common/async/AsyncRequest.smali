.class public interface abstract Lcom/google/gmm/common/async/AsyncRequest;
.super Ljava/lang/Object;


# virtual methods
.method public abstract getException()Ljava/lang/Exception;
.end method

.method public abstract hasException()Z
.end method

.method public abstract isCompletedOrException()Z
.end method

.method public abstract isRunning()Z
.end method

.method public abstract submit(Lcom/google/gmm/common/task/AbstractTask;)V
.end method
