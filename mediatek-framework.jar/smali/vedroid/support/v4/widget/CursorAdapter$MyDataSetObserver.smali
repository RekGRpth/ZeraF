.class Lvedroid/support/v4/widget/CursorAdapter$MyDataSetObserver;
.super Landroid/database/DataSetObserver;
.source "CursorAdapter.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lvedroid/support/v4/widget/CursorAdapter;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "MyDataSetObserver"
.end annotation


# instance fields
.field final synthetic this$0:Lvedroid/support/v4/widget/CursorAdapter;


# direct methods
.method private constructor <init>(Lvedroid/support/v4/widget/CursorAdapter;)V
    .locals 0

    iput-object p1, p0, Lvedroid/support/v4/widget/CursorAdapter$MyDataSetObserver;->this$0:Lvedroid/support/v4/widget/CursorAdapter;

    invoke-direct {p0}, Landroid/database/DataSetObserver;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lvedroid/support/v4/widget/CursorAdapter;Lvedroid/support/v4/widget/CursorAdapter$1;)V
    .locals 0
    .param p1    # Lvedroid/support/v4/widget/CursorAdapter;
    .param p2    # Lvedroid/support/v4/widget/CursorAdapter$1;

    invoke-direct {p0, p1}, Lvedroid/support/v4/widget/CursorAdapter$MyDataSetObserver;-><init>(Lvedroid/support/v4/widget/CursorAdapter;)V

    return-void
.end method


# virtual methods
.method public onChanged()V
    .locals 2

    iget-object v0, p0, Lvedroid/support/v4/widget/CursorAdapter$MyDataSetObserver;->this$0:Lvedroid/support/v4/widget/CursorAdapter;

    const/4 v1, 0x1

    iput-boolean v1, v0, Lvedroid/support/v4/widget/CursorAdapter;->mDataValid:Z

    iget-object v0, p0, Lvedroid/support/v4/widget/CursorAdapter$MyDataSetObserver;->this$0:Lvedroid/support/v4/widget/CursorAdapter;

    invoke-virtual {v0}, Landroid/widget/BaseAdapter;->notifyDataSetChanged()V

    return-void
.end method

.method public onInvalidated()V
    .locals 2

    iget-object v0, p0, Lvedroid/support/v4/widget/CursorAdapter$MyDataSetObserver;->this$0:Lvedroid/support/v4/widget/CursorAdapter;

    const/4 v1, 0x0

    iput-boolean v1, v0, Lvedroid/support/v4/widget/CursorAdapter;->mDataValid:Z

    iget-object v0, p0, Lvedroid/support/v4/widget/CursorAdapter$MyDataSetObserver;->this$0:Lvedroid/support/v4/widget/CursorAdapter;

    invoke-virtual {v0}, Landroid/widget/BaseAdapter;->notifyDataSetInvalidated()V

    return-void
.end method
