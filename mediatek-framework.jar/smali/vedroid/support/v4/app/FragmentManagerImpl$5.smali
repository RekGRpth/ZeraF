.class Lvedroid/support/v4/app/FragmentManagerImpl$5;
.super Ljava/lang/Object;
.source "FragmentManager.java"

# interfaces
.implements Landroid/view/animation/Animation$AnimationListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lvedroid/support/v4/app/FragmentManagerImpl;->moveToState(Lvedroid/support/v4/app/Fragment;IIIZ)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lvedroid/support/v4/app/FragmentManagerImpl;

.field final synthetic val$fragment:Lvedroid/support/v4/app/Fragment;


# direct methods
.method constructor <init>(Lvedroid/support/v4/app/FragmentManagerImpl;Lvedroid/support/v4/app/Fragment;)V
    .locals 0

    iput-object p1, p0, Lvedroid/support/v4/app/FragmentManagerImpl$5;->this$0:Lvedroid/support/v4/app/FragmentManagerImpl;

    iput-object p2, p0, Lvedroid/support/v4/app/FragmentManagerImpl$5;->val$fragment:Lvedroid/support/v4/app/Fragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onAnimationEnd(Landroid/view/animation/Animation;)V
    .locals 6
    .param p1    # Landroid/view/animation/Animation;

    const/4 v3, 0x0

    iget-object v0, p0, Lvedroid/support/v4/app/FragmentManagerImpl$5;->val$fragment:Lvedroid/support/v4/app/Fragment;

    iget-object v0, v0, Lvedroid/support/v4/app/Fragment;->mAnimatingAway:Landroid/view/View;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lvedroid/support/v4/app/FragmentManagerImpl$5;->val$fragment:Lvedroid/support/v4/app/Fragment;

    const/4 v1, 0x0

    iput-object v1, v0, Lvedroid/support/v4/app/Fragment;->mAnimatingAway:Landroid/view/View;

    iget-object v0, p0, Lvedroid/support/v4/app/FragmentManagerImpl$5;->this$0:Lvedroid/support/v4/app/FragmentManagerImpl;

    iget-object v1, p0, Lvedroid/support/v4/app/FragmentManagerImpl$5;->val$fragment:Lvedroid/support/v4/app/Fragment;

    iget-object v2, p0, Lvedroid/support/v4/app/FragmentManagerImpl$5;->val$fragment:Lvedroid/support/v4/app/Fragment;

    iget v2, v2, Lvedroid/support/v4/app/Fragment;->mStateAfterAnimating:I

    move v4, v3

    move v5, v3

    invoke-virtual/range {v0 .. v5}, Lvedroid/support/v4/app/FragmentManagerImpl;->moveToState(Lvedroid/support/v4/app/Fragment;IIIZ)V

    :cond_0
    return-void
.end method

.method public onAnimationRepeat(Landroid/view/animation/Animation;)V
    .locals 0
    .param p1    # Landroid/view/animation/Animation;

    return-void
.end method

.method public onAnimationStart(Landroid/view/animation/Animation;)V
    .locals 0
    .param p1    # Landroid/view/animation/Animation;

    return-void
.end method
