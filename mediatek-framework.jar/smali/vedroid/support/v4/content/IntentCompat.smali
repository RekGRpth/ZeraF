.class public Lvedroid/support/v4/content/IntentCompat;
.super Ljava/lang/Object;
.source "IntentCompat.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lvedroid/support/v4/content/IntentCompat$IntentCompatImplIcsMr1;,
        Lvedroid/support/v4/content/IntentCompat$IntentCompatImplHC;,
        Lvedroid/support/v4/content/IntentCompat$IntentCompatImplBase;,
        Lvedroid/support/v4/content/IntentCompat$IntentCompatImpl;
    }
.end annotation


# static fields
.field public static final ACTION_EXTERNAL_APPLICATIONS_AVAILABLE:Ljava/lang/String; = "android.intent.action.EXTERNAL_APPLICATIONS_AVAILABLE"

.field public static final ACTION_EXTERNAL_APPLICATIONS_UNAVAILABLE:Ljava/lang/String; = "android.intent.action.EXTERNAL_APPLICATIONS_UNAVAILABLE"

.field public static final EXTRA_CHANGED_PACKAGE_LIST:Ljava/lang/String; = "android.intent.extra.changed_package_list"

.field public static final EXTRA_CHANGED_UID_LIST:Ljava/lang/String; = "android.intent.extra.changed_uid_list"

.field public static final EXTRA_HTML_TEXT:Ljava/lang/String; = "android.intent.extra.HTML_TEXT"

.field public static final FLAG_ACTIVITY_CLEAR_TASK:I = 0x8000

.field public static final FLAG_ACTIVITY_TASK_ON_HOME:I = 0x4000

.field private static final IMPL:Lvedroid/support/v4/content/IntentCompat$IntentCompatImpl;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0xf

    if-lt v0, v1, :cond_0

    new-instance v1, Lvedroid/support/v4/content/IntentCompat$IntentCompatImplIcsMr1;

    invoke-direct {v1}, Lvedroid/support/v4/content/IntentCompat$IntentCompatImplIcsMr1;-><init>()V

    sput-object v1, Lvedroid/support/v4/content/IntentCompat;->IMPL:Lvedroid/support/v4/content/IntentCompat$IntentCompatImpl;

    :goto_0
    return-void

    :cond_0
    const/16 v1, 0xb

    if-lt v0, v1, :cond_1

    new-instance v1, Lvedroid/support/v4/content/IntentCompat$IntentCompatImplHC;

    invoke-direct {v1}, Lvedroid/support/v4/content/IntentCompat$IntentCompatImplHC;-><init>()V

    sput-object v1, Lvedroid/support/v4/content/IntentCompat;->IMPL:Lvedroid/support/v4/content/IntentCompat$IntentCompatImpl;

    goto :goto_0

    :cond_1
    new-instance v1, Lvedroid/support/v4/content/IntentCompat$IntentCompatImplBase;

    invoke-direct {v1}, Lvedroid/support/v4/content/IntentCompat$IntentCompatImplBase;-><init>()V

    sput-object v1, Lvedroid/support/v4/content/IntentCompat;->IMPL:Lvedroid/support/v4/content/IntentCompat$IntentCompatImpl;

    goto :goto_0
.end method

.method private constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static makeMainActivity(Landroid/content/ComponentName;)Landroid/content/Intent;
    .locals 1
    .param p0    # Landroid/content/ComponentName;

    sget-object v0, Lvedroid/support/v4/content/IntentCompat;->IMPL:Lvedroid/support/v4/content/IntentCompat$IntentCompatImpl;

    invoke-interface {v0, p0}, Lvedroid/support/v4/content/IntentCompat$IntentCompatImpl;->makeMainActivity(Landroid/content/ComponentName;)Landroid/content/Intent;

    move-result-object v0

    return-object v0
.end method

.method public static makeMainSelectorActivity(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;
    .locals 1
    .param p0    # Ljava/lang/String;
    .param p1    # Ljava/lang/String;

    sget-object v0, Lvedroid/support/v4/content/IntentCompat;->IMPL:Lvedroid/support/v4/content/IntentCompat$IntentCompatImpl;

    invoke-interface {v0, p0, p1}, Lvedroid/support/v4/content/IntentCompat$IntentCompatImpl;->makeMainSelectorActivity(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    return-object v0
.end method

.method public static makeRestartActivityTask(Landroid/content/ComponentName;)Landroid/content/Intent;
    .locals 1
    .param p0    # Landroid/content/ComponentName;

    sget-object v0, Lvedroid/support/v4/content/IntentCompat;->IMPL:Lvedroid/support/v4/content/IntentCompat$IntentCompatImpl;

    invoke-interface {v0, p0}, Lvedroid/support/v4/content/IntentCompat$IntentCompatImpl;->makeRestartActivityTask(Landroid/content/ComponentName;)Landroid/content/Intent;

    move-result-object v0

    return-object v0
.end method
