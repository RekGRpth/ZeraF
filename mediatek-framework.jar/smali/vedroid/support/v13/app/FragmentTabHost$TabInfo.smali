.class final Lvedroid/support/v13/app/FragmentTabHost$TabInfo;
.super Ljava/lang/Object;
.source "FragmentTabHost.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lvedroid/support/v13/app/FragmentTabHost;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = "TabInfo"
.end annotation


# instance fields
.field private final args:Landroid/os/Bundle;

.field private final clss:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<*>;"
        }
    .end annotation
.end field

.field private fragment:Landroid/app/Fragment;

.field private final tag:Ljava/lang/String;


# direct methods
.method constructor <init>(Ljava/lang/String;Ljava/lang/Class;Landroid/os/Bundle;)V
    .locals 0
    .param p1    # Ljava/lang/String;
    .param p3    # Landroid/os/Bundle;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/Class",
            "<*>;",
            "Landroid/os/Bundle;",
            ")V"
        }
    .end annotation

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lvedroid/support/v13/app/FragmentTabHost$TabInfo;->tag:Ljava/lang/String;

    iput-object p2, p0, Lvedroid/support/v13/app/FragmentTabHost$TabInfo;->clss:Ljava/lang/Class;

    iput-object p3, p0, Lvedroid/support/v13/app/FragmentTabHost$TabInfo;->args:Landroid/os/Bundle;

    return-void
.end method

.method static synthetic access$100(Lvedroid/support/v13/app/FragmentTabHost$TabInfo;)Landroid/app/Fragment;
    .locals 1
    .param p0    # Lvedroid/support/v13/app/FragmentTabHost$TabInfo;

    iget-object v0, p0, Lvedroid/support/v13/app/FragmentTabHost$TabInfo;->fragment:Landroid/app/Fragment;

    return-object v0
.end method

.method static synthetic access$102(Lvedroid/support/v13/app/FragmentTabHost$TabInfo;Landroid/app/Fragment;)Landroid/app/Fragment;
    .locals 0
    .param p0    # Lvedroid/support/v13/app/FragmentTabHost$TabInfo;
    .param p1    # Landroid/app/Fragment;

    iput-object p1, p0, Lvedroid/support/v13/app/FragmentTabHost$TabInfo;->fragment:Landroid/app/Fragment;

    return-object p1
.end method

.method static synthetic access$200(Lvedroid/support/v13/app/FragmentTabHost$TabInfo;)Ljava/lang/String;
    .locals 1
    .param p0    # Lvedroid/support/v13/app/FragmentTabHost$TabInfo;

    iget-object v0, p0, Lvedroid/support/v13/app/FragmentTabHost$TabInfo;->tag:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$300(Lvedroid/support/v13/app/FragmentTabHost$TabInfo;)Ljava/lang/Class;
    .locals 1
    .param p0    # Lvedroid/support/v13/app/FragmentTabHost$TabInfo;

    iget-object v0, p0, Lvedroid/support/v13/app/FragmentTabHost$TabInfo;->clss:Ljava/lang/Class;

    return-object v0
.end method

.method static synthetic access$400(Lvedroid/support/v13/app/FragmentTabHost$TabInfo;)Landroid/os/Bundle;
    .locals 1
    .param p0    # Lvedroid/support/v13/app/FragmentTabHost$TabInfo;

    iget-object v0, p0, Lvedroid/support/v13/app/FragmentTabHost$TabInfo;->args:Landroid/os/Bundle;

    return-object v0
.end method
