.class public Lcom/mediatek/common/agps/MtkAgpsProfileManager;
.super Ljava/lang/Object;
.source "MtkAgpsProfileManager.java"


# instance fields
.field private mAgpsEnable:Z

.field private mAgpsProfileList:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/mediatek/common/agps/MtkAgpsProfile;",
            ">;"
        }
    .end annotation
.end field

.field private mCaEnable:Z

.field private mCdmaProfileList:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/mediatek/common/agps/MtkAgpsCdmaProfile;",
            ">;"
        }
    .end annotation
.end field

.field private mCpGeminiPrefSim:I

.field private mCpUpSelection:Z

.field private mDefaultCdmaProfile:Ljava/lang/String;

.field private mDefaultProfileName:Ljava/lang/String;

.field private mDisableAfterReboot:Z

.field private mEcidStatus:Z

.field private mEvdoPrefer:I

.field private mGpevt:Z

.field private mLogFileMaxNum:I

.field private mNiRequest:Z

.field private mNotifyTimeout:I

.field private mPmtk997_5:Z

.field private mRoamingEnable:Z

.field private mSiMode:I

.field private mSuplVersion:I

.field private mVerifyTimeout:I


# direct methods
.method public constructor <init>()V
    .locals 5

    const/4 v4, 0x0

    const/16 v3, 0x8

    const/4 v2, 0x1

    const/4 v1, 0x0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/mediatek/common/agps/MtkAgpsProfileManager;->mAgpsProfileList:Ljava/util/List;

    iput-boolean v1, p0, Lcom/mediatek/common/agps/MtkAgpsProfileManager;->mDisableAfterReboot:Z

    iput v2, p0, Lcom/mediatek/common/agps/MtkAgpsProfileManager;->mSiMode:I

    iput-boolean v2, p0, Lcom/mediatek/common/agps/MtkAgpsProfileManager;->mCaEnable:Z

    iput-boolean v2, p0, Lcom/mediatek/common/agps/MtkAgpsProfileManager;->mNiRequest:Z

    iput-boolean v2, p0, Lcom/mediatek/common/agps/MtkAgpsProfileManager;->mAgpsEnable:Z

    const/16 v0, 0xa

    iput v0, p0, Lcom/mediatek/common/agps/MtkAgpsProfileManager;->mLogFileMaxNum:I

    iput v2, p0, Lcom/mediatek/common/agps/MtkAgpsProfileManager;->mCpGeminiPrefSim:I

    iput-boolean v1, p0, Lcom/mediatek/common/agps/MtkAgpsProfileManager;->mRoamingEnable:Z

    iput-object v4, p0, Lcom/mediatek/common/agps/MtkAgpsProfileManager;->mDefaultProfileName:Ljava/lang/String;

    iput-boolean v1, p0, Lcom/mediatek/common/agps/MtkAgpsProfileManager;->mCpUpSelection:Z

    iput v3, p0, Lcom/mediatek/common/agps/MtkAgpsProfileManager;->mNotifyTimeout:I

    iput v3, p0, Lcom/mediatek/common/agps/MtkAgpsProfileManager;->mVerifyTimeout:I

    iput-boolean v1, p0, Lcom/mediatek/common/agps/MtkAgpsProfileManager;->mEcidStatus:Z

    iput-boolean v1, p0, Lcom/mediatek/common/agps/MtkAgpsProfileManager;->mPmtk997_5:Z

    iput-boolean v1, p0, Lcom/mediatek/common/agps/MtkAgpsProfileManager;->mGpevt:Z

    iput v2, p0, Lcom/mediatek/common/agps/MtkAgpsProfileManager;->mSuplVersion:I

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/mediatek/common/agps/MtkAgpsProfileManager;->mCdmaProfileList:Ljava/util/List;

    iput-object v4, p0, Lcom/mediatek/common/agps/MtkAgpsProfileManager;->mDefaultCdmaProfile:Ljava/lang/String;

    iput v1, p0, Lcom/mediatek/common/agps/MtkAgpsProfileManager;->mEvdoPrefer:I

    return-void
.end method

.method private log(Ljava/lang/String;)V
    .locals 1
    .param p1    # Ljava/lang/String;

    const-string v0, "[MtkAgpsManagerService]"

    invoke-static {v0, p1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    return-void
.end method


# virtual methods
.method public dumpFile(Ljava/lang/String;)V
    .locals 6
    .param p1    # Ljava/lang/String;

    const/4 v2, 0x0

    :try_start_0
    new-instance v3, Ljava/io/BufferedReader;

    new-instance v4, Ljava/io/FileReader;

    invoke-direct {v4, p1}, Ljava/io/FileReader;-><init>(Ljava/lang/String;)V

    invoke-direct {v3, v4}, Ljava/io/BufferedReader;-><init>(Ljava/io/Reader;)V
    :try_end_0
    .catch Ljava/io/FileNotFoundException; {:try_start_0 .. :try_end_0} :catch_7
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_3
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :try_start_1
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "==== dumpFile path="

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " ===="

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-direct {p0, v4}, Lcom/mediatek/common/agps/MtkAgpsProfileManager;->log(Ljava/lang/String;)V

    const/4 v1, 0x0

    :goto_0
    invoke-virtual {v3}, Ljava/io/BufferedReader;->readLine()Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_1

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "dumpFile="

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-direct {p0, v4}, Lcom/mediatek/common/agps/MtkAgpsProfileManager;->log(Ljava/lang/String;)V
    :try_end_1
    .catch Ljava/io/FileNotFoundException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_6
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    goto :goto_0

    :catch_0
    move-exception v0

    move-object v2, v3

    :goto_1
    :try_start_2
    invoke-virtual {v0}, Ljava/lang/Throwable;->printStackTrace()V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    if-eqz v2, :cond_0

    :try_start_3
    invoke-virtual {v2}, Ljava/io/BufferedReader;->close()V
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_2

    :cond_0
    :goto_2
    return-void

    :cond_1
    if-eqz v3, :cond_2

    :try_start_4
    invoke-virtual {v3}, Ljava/io/BufferedReader;->close()V
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_1

    :cond_2
    move-object v2, v3

    goto :goto_2

    :catch_1
    move-exception v0

    invoke-virtual {v0}, Ljava/lang/Throwable;->printStackTrace()V

    move-object v2, v3

    goto :goto_2

    :catch_2
    move-exception v0

    invoke-virtual {v0}, Ljava/lang/Throwable;->printStackTrace()V

    goto :goto_2

    :catch_3
    move-exception v0

    :goto_3
    :try_start_5
    invoke-virtual {v0}, Ljava/lang/Throwable;->printStackTrace()V
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    if-eqz v2, :cond_0

    :try_start_6
    invoke-virtual {v2}, Ljava/io/BufferedReader;->close()V
    :try_end_6
    .catch Ljava/io/IOException; {:try_start_6 .. :try_end_6} :catch_4

    goto :goto_2

    :catch_4
    move-exception v0

    invoke-virtual {v0}, Ljava/lang/Throwable;->printStackTrace()V

    goto :goto_2

    :catchall_0
    move-exception v4

    :goto_4
    if-eqz v2, :cond_3

    :try_start_7
    invoke-virtual {v2}, Ljava/io/BufferedReader;->close()V
    :try_end_7
    .catch Ljava/io/IOException; {:try_start_7 .. :try_end_7} :catch_5

    :cond_3
    :goto_5
    throw v4

    :catch_5
    move-exception v0

    invoke-virtual {v0}, Ljava/lang/Throwable;->printStackTrace()V

    goto :goto_5

    :catchall_1
    move-exception v4

    move-object v2, v3

    goto :goto_4

    :catch_6
    move-exception v0

    move-object v2, v3

    goto :goto_3

    :catch_7
    move-exception v0

    goto :goto_1
.end method

.method public getAgpsStatus()Z
    .locals 1

    iget-boolean v0, p0, Lcom/mediatek/common/agps/MtkAgpsProfileManager;->mAgpsEnable:Z

    return v0
.end method

.method public getAllCdmaProfile()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/mediatek/common/agps/MtkAgpsCdmaProfile;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/mediatek/common/agps/MtkAgpsProfileManager;->mCdmaProfileList:Ljava/util/List;

    return-object v0
.end method

.method public getAllProfile()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/mediatek/common/agps/MtkAgpsProfile;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/mediatek/common/agps/MtkAgpsProfileManager;->mAgpsProfileList:Ljava/util/List;

    return-object v0
.end method

.method public getCaStatus()Z
    .locals 1

    iget-boolean v0, p0, Lcom/mediatek/common/agps/MtkAgpsProfileManager;->mCaEnable:Z

    return v0
.end method

.method public getCpPreferSim()I
    .locals 1

    iget v0, p0, Lcom/mediatek/common/agps/MtkAgpsProfileManager;->mCpGeminiPrefSim:I

    return v0
.end method

.method public getCpUpSelection()Z
    .locals 1

    iget-boolean v0, p0, Lcom/mediatek/common/agps/MtkAgpsProfileManager;->mCpUpSelection:Z

    return v0
.end method

.method public getDefaultCdmaProfile()Lcom/mediatek/common/agps/MtkAgpsCdmaProfile;
    .locals 4

    iget-object v2, p0, Lcom/mediatek/common/agps/MtkAgpsProfileManager;->mCdmaProfileList:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :cond_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/mediatek/common/agps/MtkAgpsCdmaProfile;

    iget-object v2, v1, Lcom/mediatek/common/agps/MtkAgpsCdmaProfile;->mName:Ljava/lang/String;

    iget-object v3, p0, Lcom/mediatek/common/agps/MtkAgpsProfileManager;->mDefaultCdmaProfile:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    :goto_0
    return-object v1

    :cond_1
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public getDefaultProfile()Lcom/mediatek/common/agps/MtkAgpsProfile;
    .locals 4

    iget-object v2, p0, Lcom/mediatek/common/agps/MtkAgpsProfileManager;->mAgpsProfileList:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :cond_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/mediatek/common/agps/MtkAgpsProfile;

    iget-object v2, v1, Lcom/mediatek/common/agps/MtkAgpsProfile;->name:Ljava/lang/String;

    iget-object v3, p0, Lcom/mediatek/common/agps/MtkAgpsProfileManager;->mDefaultProfileName:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    :goto_0
    return-object v1

    :cond_1
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public getDisableAfterRebootStatus()Z
    .locals 1

    iget-boolean v0, p0, Lcom/mediatek/common/agps/MtkAgpsProfileManager;->mDisableAfterReboot:Z

    return v0
.end method

.method public getEcidStatus()Z
    .locals 1

    iget-boolean v0, p0, Lcom/mediatek/common/agps/MtkAgpsProfileManager;->mEcidStatus:Z

    return v0
.end method

.method public getEvdoPrefer()I
    .locals 1

    iget v0, p0, Lcom/mediatek/common/agps/MtkAgpsProfileManager;->mEvdoPrefer:I

    return v0
.end method

.method public getGpevt()Z
    .locals 1

    iget-boolean v0, p0, Lcom/mediatek/common/agps/MtkAgpsProfileManager;->mGpevt:Z

    return v0
.end method

.method public getNiStatus()Z
    .locals 1

    iget-boolean v0, p0, Lcom/mediatek/common/agps/MtkAgpsProfileManager;->mNiRequest:Z

    return v0
.end method

.method public getNotifyTimeout()I
    .locals 1

    iget v0, p0, Lcom/mediatek/common/agps/MtkAgpsProfileManager;->mNotifyTimeout:I

    return v0
.end method

.method public getPmtk997_5()Z
    .locals 1

    iget-boolean v0, p0, Lcom/mediatek/common/agps/MtkAgpsProfileManager;->mPmtk997_5:Z

    return v0
.end method

.method public getRoamingStatus()Z
    .locals 1

    iget-boolean v0, p0, Lcom/mediatek/common/agps/MtkAgpsProfileManager;->mRoamingEnable:Z

    return v0
.end method

.method public getSiMode()I
    .locals 1

    iget v0, p0, Lcom/mediatek/common/agps/MtkAgpsProfileManager;->mSiMode:I

    return v0
.end method

.method public getSuplVersion()I
    .locals 1

    iget v0, p0, Lcom/mediatek/common/agps/MtkAgpsProfileManager;->mSuplVersion:I

    return v0
.end method

.method public getVerifyTimeout()I
    .locals 1

    iget v0, p0, Lcom/mediatek/common/agps/MtkAgpsProfileManager;->mVerifyTimeout:I

    return v0
.end method

.method public insertProfile(Lcom/mediatek/common/agps/MtkAgpsProfile;)V
    .locals 5
    .param p1    # Lcom/mediatek/common/agps/MtkAgpsProfile;

    const/4 v0, 0x0

    const/4 v1, 0x1

    if-nez p1, :cond_2

    const-string v3, "WARNING: insertPorifle the profile is null"

    invoke-direct {p0, v3}, Lcom/mediatek/common/agps/MtkAgpsProfileManager;->log(Ljava/lang/String;)V

    :cond_0
    :goto_0
    return-void

    :cond_1
    add-int/lit8 v0, v0, 0x1

    :cond_2
    iget-object v3, p0, Lcom/mediatek/common/agps/MtkAgpsProfileManager;->mAgpsProfileList:Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v3

    if-ge v0, v3, :cond_3

    iget-object v4, p1, Lcom/mediatek/common/agps/MtkAgpsProfile;->code:Ljava/lang/String;

    iget-object v3, p0, Lcom/mediatek/common/agps/MtkAgpsProfileManager;->mAgpsProfileList:Ljava/util/List;

    invoke-interface {v3, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/mediatek/common/agps/MtkAgpsProfile;

    iget-object v3, v3, Lcom/mediatek/common/agps/MtkAgpsProfile;->code:Ljava/lang/String;

    invoke-virtual {v4, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_1

    iget-object v3, p0, Lcom/mediatek/common/agps/MtkAgpsProfileManager;->mAgpsProfileList:Ljava/util/List;

    invoke-interface {v3, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/mediatek/common/agps/MtkAgpsProfile;

    iget-object v3, p1, Lcom/mediatek/common/agps/MtkAgpsProfile;->name:Ljava/lang/String;

    iput-object v3, v2, Lcom/mediatek/common/agps/MtkAgpsProfile;->name:Ljava/lang/String;

    iget-object v3, p1, Lcom/mediatek/common/agps/MtkAgpsProfile;->addr:Ljava/lang/String;

    iput-object v3, v2, Lcom/mediatek/common/agps/MtkAgpsProfile;->addr:Ljava/lang/String;

    iget v3, p1, Lcom/mediatek/common/agps/MtkAgpsProfile;->port:I

    iput v3, v2, Lcom/mediatek/common/agps/MtkAgpsProfile;->port:I

    iget v3, p1, Lcom/mediatek/common/agps/MtkAgpsProfile;->tls:I

    iput v3, v2, Lcom/mediatek/common/agps/MtkAgpsProfile;->tls:I

    iget-object v3, p1, Lcom/mediatek/common/agps/MtkAgpsProfile;->code:Ljava/lang/String;

    iput-object v3, v2, Lcom/mediatek/common/agps/MtkAgpsProfile;->code:Ljava/lang/String;

    iget-object v3, p1, Lcom/mediatek/common/agps/MtkAgpsProfile;->backupSlpNameVar:Ljava/lang/String;

    iput-object v3, v2, Lcom/mediatek/common/agps/MtkAgpsProfile;->backupSlpNameVar:Ljava/lang/String;

    iget v3, p1, Lcom/mediatek/common/agps/MtkAgpsProfile;->showType:I

    iput v3, v2, Lcom/mediatek/common/agps/MtkAgpsProfile;->showType:I

    iget-object v3, p1, Lcom/mediatek/common/agps/MtkAgpsProfile;->addrType:Ljava/lang/String;

    iput-object v3, v2, Lcom/mediatek/common/agps/MtkAgpsProfile;->addrType:Ljava/lang/String;

    iget-object v3, p1, Lcom/mediatek/common/agps/MtkAgpsProfile;->providerId:Ljava/lang/String;

    iput-object v3, v2, Lcom/mediatek/common/agps/MtkAgpsProfile;->providerId:Ljava/lang/String;

    iget-object v3, p1, Lcom/mediatek/common/agps/MtkAgpsProfile;->defaultApn:Ljava/lang/String;

    iput-object v3, v2, Lcom/mediatek/common/agps/MtkAgpsProfile;->defaultApn:Ljava/lang/String;

    iget-object v3, p1, Lcom/mediatek/common/agps/MtkAgpsProfile;->optionApn:Ljava/lang/String;

    iput-object v3, v2, Lcom/mediatek/common/agps/MtkAgpsProfile;->optionApn:Ljava/lang/String;

    iget-object v3, p1, Lcom/mediatek/common/agps/MtkAgpsProfile;->optionApn2:Ljava/lang/String;

    iput-object v3, v2, Lcom/mediatek/common/agps/MtkAgpsProfile;->optionApn2:Ljava/lang/String;

    iget-object v3, p1, Lcom/mediatek/common/agps/MtkAgpsProfile;->appId:Ljava/lang/String;

    iput-object v3, v2, Lcom/mediatek/common/agps/MtkAgpsProfile;->appId:Ljava/lang/String;

    iget-object v3, p1, Lcom/mediatek/common/agps/MtkAgpsProfile;->mccMnc:Ljava/lang/String;

    iput-object v3, v2, Lcom/mediatek/common/agps/MtkAgpsProfile;->mccMnc:Ljava/lang/String;

    const/4 v1, 0x0

    :cond_3
    if-eqz v1, :cond_0

    iget-object v3, p0, Lcom/mediatek/common/agps/MtkAgpsProfileManager;->mAgpsProfileList:Ljava/util/List;

    invoke-interface {v3, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0
.end method

.method public toString()Ljava/lang/String;
    .locals 5

    new-instance v2, Ljava/lang/String;

    invoke-direct {v2}, Ljava/lang/String;-><init>()V

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "mDisableAfterReboot=["

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-boolean v4, p0, Lcom/mediatek/common/agps/MtkAgpsProfileManager;->mDisableAfterReboot:Z

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "] mNiRequest=["

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-boolean v4, p0, Lcom/mediatek/common/agps/MtkAgpsProfileManager;->mNiRequest:Z

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "] mAgpsEnable=["

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-boolean v4, p0, Lcom/mediatek/common/agps/MtkAgpsProfileManager;->mAgpsEnable:Z

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "] mSiMode=["

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget v4, p0, Lcom/mediatek/common/agps/MtkAgpsProfileManager;->mSiMode:I

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "] mLogFileMaxNum=["

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget v4, p0, Lcom/mediatek/common/agps/MtkAgpsProfileManager;->mLogFileMaxNum:I

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "] mCpGeminiPrefSim=["

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget v4, p0, Lcom/mediatek/common/agps/MtkAgpsProfileManager;->mCpGeminiPrefSim:I

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "] mRoamingEnable=["

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-boolean v4, p0, Lcom/mediatek/common/agps/MtkAgpsProfileManager;->mRoamingEnable:Z

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "] mCaEnable=["

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-boolean v4, p0, Lcom/mediatek/common/agps/MtkAgpsProfileManager;->mCaEnable:Z

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "] mDefaultProfileName=["

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lcom/mediatek/common/agps/MtkAgpsProfileManager;->mDefaultProfileName:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "] mCpUpSelection=["

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-boolean v4, p0, Lcom/mediatek/common/agps/MtkAgpsProfileManager;->mCpUpSelection:Z

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "] mNotifyTimeout=["

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget v4, p0, Lcom/mediatek/common/agps/MtkAgpsProfileManager;->mNotifyTimeout:I

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "] mVerifyTimeout=["

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget v4, p0, Lcom/mediatek/common/agps/MtkAgpsProfileManager;->mVerifyTimeout:I

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "] mEcidStatus=["

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-boolean v4, p0, Lcom/mediatek/common/agps/MtkAgpsProfileManager;->mEcidStatus:Z

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "] mPmtk997_5=["

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-boolean v4, p0, Lcom/mediatek/common/agps/MtkAgpsProfileManager;->mPmtk997_5:Z

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "] mGpevt=["

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-boolean v4, p0, Lcom/mediatek/common/agps/MtkAgpsProfileManager;->mGpevt:Z

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "] mSuplVersion=["

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget v4, p0, Lcom/mediatek/common/agps/MtkAgpsProfileManager;->mSuplVersion:I

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "] mEvdoPrefer=["

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget v4, p0, Lcom/mediatek/common/agps/MtkAgpsProfileManager;->mEvdoPrefer:I

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "] mDefaultCdmaProfile=["

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lcom/mediatek/common/agps/MtkAgpsProfileManager;->mDefaultCdmaProfile:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "]\n"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "============ AGPS Profile num="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lcom/mediatek/common/agps/MtkAgpsProfileManager;->mAgpsProfileList:Ljava/util/List;

    invoke-interface {v4}, Ljava/util/List;->size()I

    move-result v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " ===========\n"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, Lcom/mediatek/common/agps/MtkAgpsProfileManager;->mAgpsProfileList:Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/mediatek/common/agps/MtkAgpsProfile;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "\n"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    goto :goto_0

    :cond_0
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "============ CDMA AGPS Profile num="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lcom/mediatek/common/agps/MtkAgpsProfileManager;->mCdmaProfileList:Ljava/util/List;

    invoke-interface {v4}, Ljava/util/List;->size()I

    move-result v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " ===========\n"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, Lcom/mediatek/common/agps/MtkAgpsProfileManager;->mCdmaProfileList:Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_1
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/mediatek/common/agps/MtkAgpsCdmaProfile;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "\n"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    goto :goto_1

    :cond_1
    return-object v2
.end method

.method public updateAgpsProfile(Ljava/lang/String;)V
    .locals 14
    .param p1    # Ljava/lang/String;

    const/4 v11, 0x0

    const/4 v8, 0x0

    :try_start_0
    invoke-static {}, Landroid/util/Xml;->newPullParser()Lorg/xmlpull/v1/XmlPullParser;

    move-result-object v11

    new-instance v9, Ljava/io/FileInputStream;

    invoke-direct {v9, p1}, Ljava/io/FileInputStream;-><init>(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/io/FileNotFoundException; {:try_start_0 .. :try_end_0} :catch_d
    .catch Lorg/xmlpull/v1/XmlPullParserException; {:try_start_0 .. :try_end_0} :catch_c
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_b
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_a
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    :try_start_1
    const-string v12, "utf-8"

    invoke-interface {v11, v9, v12}, Lorg/xmlpull/v1/XmlPullParser;->setInput(Ljava/io/InputStream;Ljava/lang/String;)V

    :cond_0
    invoke-interface {v11}, Lorg/xmlpull/v1/XmlPullParser;->next()I

    invoke-interface {v11}, Lorg/xmlpull/v1/XmlPullParser;->getEventType()I

    move-result v6

    invoke-interface {v11}, Lorg/xmlpull/v1/XmlPullParser;->getName()Ljava/lang/String;

    move-result-object v10

    invoke-interface {v11}, Lorg/xmlpull/v1/XmlPullParser;->getAttributeCount()I
    :try_end_1
    .catch Ljava/io/FileNotFoundException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Lorg/xmlpull/v1/XmlPullParserException; {:try_start_1 .. :try_end_1} :catch_2
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_4
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_6
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result v4

    const/4 v12, 0x2

    if-eq v6, v12, :cond_4

    :cond_1
    :goto_0
    const/4 v12, 0x1

    if-ne v6, v12, :cond_0

    if-eqz v9, :cond_2

    :try_start_2
    invoke-virtual {v9}, Ljava/io/FileInputStream;->close()V
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_8

    :cond_2
    move-object v8, v9

    :cond_3
    :goto_1
    return-void

    :cond_4
    :try_start_3
    const-string v12, "agps_profile"

    invoke-virtual {v10, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v12

    if-eqz v12, :cond_15

    new-instance v0, Lcom/mediatek/common/agps/MtkAgpsProfile;

    invoke-direct {v0}, Lcom/mediatek/common/agps/MtkAgpsProfile;-><init>()V

    const/4 v7, 0x0

    :goto_2
    if-ge v7, v4, :cond_14

    invoke-interface {v11, v7}, Lorg/xmlpull/v1/XmlPullParser;->getAttributeName(I)Ljava/lang/String;

    move-result-object v1

    invoke-interface {v11, v7}, Lorg/xmlpull/v1/XmlPullParser;->getAttributeValue(I)Ljava/lang/String;

    move-result-object v2

    const-string v12, "address"

    invoke-virtual {v1, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v12

    if-eqz v12, :cond_6

    iput-object v2, v0, Lcom/mediatek/common/agps/MtkAgpsProfile;->addr:Ljava/lang/String;

    :cond_5
    :goto_3
    add-int/lit8 v7, v7, 0x1

    goto :goto_2

    :cond_6
    const-string v12, "slp_name"

    invoke-virtual {v1, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v12

    if-eqz v12, :cond_7

    iput-object v2, v0, Lcom/mediatek/common/agps/MtkAgpsProfile;->name:Ljava/lang/String;
    :try_end_3
    .catch Ljava/io/FileNotFoundException; {:try_start_3 .. :try_end_3} :catch_0
    .catch Lorg/xmlpull/v1/XmlPullParserException; {:try_start_3 .. :try_end_3} :catch_2
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_4
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_6
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto :goto_3

    :catch_0
    move-exception v5

    move-object v8, v9

    :goto_4
    :try_start_4
    invoke-virtual {v5}, Ljava/lang/Throwable;->printStackTrace()V

    invoke-virtual {p0, p1}, Lcom/mediatek/common/agps/MtkAgpsProfileManager;->dumpFile(Ljava/lang/String;)V
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    if-eqz v8, :cond_3

    :try_start_5
    invoke-virtual {v8}, Ljava/io/FileInputStream;->close()V
    :try_end_5
    .catch Ljava/io/IOException; {:try_start_5 .. :try_end_5} :catch_1

    goto :goto_1

    :catch_1
    move-exception v5

    invoke-virtual {v5}, Ljava/lang/Throwable;->printStackTrace()V

    goto :goto_1

    :cond_7
    :try_start_6
    const-string v12, "port"

    invoke-virtual {v1, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v12

    if-eqz v12, :cond_8

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v12

    invoke-virtual {v12}, Ljava/lang/Integer;->intValue()I

    move-result v12

    iput v12, v0, Lcom/mediatek/common/agps/MtkAgpsProfile;->port:I
    :try_end_6
    .catch Ljava/io/FileNotFoundException; {:try_start_6 .. :try_end_6} :catch_0
    .catch Lorg/xmlpull/v1/XmlPullParserException; {:try_start_6 .. :try_end_6} :catch_2
    .catch Ljava/io/IOException; {:try_start_6 .. :try_end_6} :catch_4
    .catch Ljava/lang/Exception; {:try_start_6 .. :try_end_6} :catch_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_0

    goto :goto_3

    :catch_2
    move-exception v5

    move-object v8, v9

    :goto_5
    :try_start_7
    invoke-virtual {v5}, Lorg/xmlpull/v1/XmlPullParserException;->printStackTrace()V

    invoke-virtual {p0, p1}, Lcom/mediatek/common/agps/MtkAgpsProfileManager;->dumpFile(Ljava/lang/String;)V
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_1

    if-eqz v8, :cond_3

    :try_start_8
    invoke-virtual {v8}, Ljava/io/FileInputStream;->close()V
    :try_end_8
    .catch Ljava/io/IOException; {:try_start_8 .. :try_end_8} :catch_3

    goto :goto_1

    :catch_3
    move-exception v5

    invoke-virtual {v5}, Ljava/lang/Throwable;->printStackTrace()V

    goto :goto_1

    :cond_8
    :try_start_9
    const-string v12, "tls"

    invoke-virtual {v1, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v12

    if-eqz v12, :cond_9

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v12

    invoke-virtual {v12}, Ljava/lang/Integer;->intValue()I

    move-result v12

    iput v12, v0, Lcom/mediatek/common/agps/MtkAgpsProfile;->tls:I
    :try_end_9
    .catch Ljava/io/FileNotFoundException; {:try_start_9 .. :try_end_9} :catch_0
    .catch Lorg/xmlpull/v1/XmlPullParserException; {:try_start_9 .. :try_end_9} :catch_2
    .catch Ljava/io/IOException; {:try_start_9 .. :try_end_9} :catch_4
    .catch Ljava/lang/Exception; {:try_start_9 .. :try_end_9} :catch_6
    .catchall {:try_start_9 .. :try_end_9} :catchall_0

    goto :goto_3

    :catch_4
    move-exception v5

    move-object v8, v9

    :goto_6
    :try_start_a
    invoke-virtual {v5}, Ljava/lang/Throwable;->printStackTrace()V

    invoke-virtual {p0, p1}, Lcom/mediatek/common/agps/MtkAgpsProfileManager;->dumpFile(Ljava/lang/String;)V
    :try_end_a
    .catchall {:try_start_a .. :try_end_a} :catchall_1

    if-eqz v8, :cond_3

    :try_start_b
    invoke-virtual {v8}, Ljava/io/FileInputStream;->close()V
    :try_end_b
    .catch Ljava/io/IOException; {:try_start_b .. :try_end_b} :catch_5

    goto/16 :goto_1

    :catch_5
    move-exception v5

    invoke-virtual {v5}, Ljava/lang/Throwable;->printStackTrace()V

    goto/16 :goto_1

    :cond_9
    :try_start_c
    const-string v12, "show_type"

    invoke-virtual {v1, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v12

    if-eqz v12, :cond_a

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v12

    invoke-virtual {v12}, Ljava/lang/Integer;->intValue()I

    move-result v12

    iput v12, v0, Lcom/mediatek/common/agps/MtkAgpsProfile;->showType:I
    :try_end_c
    .catch Ljava/io/FileNotFoundException; {:try_start_c .. :try_end_c} :catch_0
    .catch Lorg/xmlpull/v1/XmlPullParserException; {:try_start_c .. :try_end_c} :catch_2
    .catch Ljava/io/IOException; {:try_start_c .. :try_end_c} :catch_4
    .catch Ljava/lang/Exception; {:try_start_c .. :try_end_c} :catch_6
    .catchall {:try_start_c .. :try_end_c} :catchall_0

    goto/16 :goto_3

    :catch_6
    move-exception v5

    move-object v8, v9

    :goto_7
    :try_start_d
    invoke-virtual {v5}, Ljava/lang/Throwable;->printStackTrace()V

    invoke-virtual {p0, p1}, Lcom/mediatek/common/agps/MtkAgpsProfileManager;->dumpFile(Ljava/lang/String;)V
    :try_end_d
    .catchall {:try_start_d .. :try_end_d} :catchall_1

    if-eqz v8, :cond_3

    :try_start_e
    invoke-virtual {v8}, Ljava/io/FileInputStream;->close()V
    :try_end_e
    .catch Ljava/io/IOException; {:try_start_e .. :try_end_e} :catch_7

    goto/16 :goto_1

    :catch_7
    move-exception v5

    invoke-virtual {v5}, Ljava/lang/Throwable;->printStackTrace()V

    goto/16 :goto_1

    :cond_a
    :try_start_f
    const-string v12, "code"

    invoke-virtual {v1, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v12

    if-eqz v12, :cond_c

    iput-object v2, v0, Lcom/mediatek/common/agps/MtkAgpsProfile;->code:Ljava/lang/String;
    :try_end_f
    .catch Ljava/io/FileNotFoundException; {:try_start_f .. :try_end_f} :catch_0
    .catch Lorg/xmlpull/v1/XmlPullParserException; {:try_start_f .. :try_end_f} :catch_2
    .catch Ljava/io/IOException; {:try_start_f .. :try_end_f} :catch_4
    .catch Ljava/lang/Exception; {:try_start_f .. :try_end_f} :catch_6
    .catchall {:try_start_f .. :try_end_f} :catchall_0

    goto/16 :goto_3

    :catchall_0
    move-exception v12

    move-object v8, v9

    :goto_8
    if-eqz v8, :cond_b

    :try_start_10
    invoke-virtual {v8}, Ljava/io/FileInputStream;->close()V
    :try_end_10
    .catch Ljava/io/IOException; {:try_start_10 .. :try_end_10} :catch_9

    :cond_b
    :goto_9
    throw v12

    :cond_c
    :try_start_11
    const-string v12, "backup_slp_name_var"

    invoke-virtual {v1, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v12

    if-eqz v12, :cond_d

    iput-object v2, v0, Lcom/mediatek/common/agps/MtkAgpsProfile;->backupSlpNameVar:Ljava/lang/String;

    goto/16 :goto_3

    :cond_d
    const-string v12, "provider_id"

    invoke-virtual {v1, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v12

    if-eqz v12, :cond_e

    iput-object v2, v0, Lcom/mediatek/common/agps/MtkAgpsProfile;->providerId:Ljava/lang/String;

    goto/16 :goto_3

    :cond_e
    const-string v12, "default_apn"

    invoke-virtual {v1, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v12

    if-eqz v12, :cond_f

    iput-object v2, v0, Lcom/mediatek/common/agps/MtkAgpsProfile;->defaultApn:Ljava/lang/String;

    goto/16 :goto_3

    :cond_f
    const-string v12, "address_type"

    invoke-virtual {v1, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v12

    if-eqz v12, :cond_10

    iput-object v2, v0, Lcom/mediatek/common/agps/MtkAgpsProfile;->addrType:Ljava/lang/String;

    goto/16 :goto_3

    :cond_10
    const-string v12, "optional_apn"

    invoke-virtual {v1, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v12

    if-eqz v12, :cond_11

    iput-object v2, v0, Lcom/mediatek/common/agps/MtkAgpsProfile;->optionApn:Ljava/lang/String;

    goto/16 :goto_3

    :cond_11
    const-string v12, "optional_apn_2"

    invoke-virtual {v1, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v12

    if-eqz v12, :cond_12

    iput-object v2, v0, Lcom/mediatek/common/agps/MtkAgpsProfile;->optionApn2:Ljava/lang/String;

    goto/16 :goto_3

    :cond_12
    const-string v12, "app_id"

    invoke-virtual {v1, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v12

    if-eqz v12, :cond_13

    iput-object v2, v0, Lcom/mediatek/common/agps/MtkAgpsProfile;->appId:Ljava/lang/String;

    goto/16 :goto_3

    :cond_13
    const-string v12, "mcc_mnc"

    invoke-virtual {v1, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v12

    if-eqz v12, :cond_5

    iput-object v2, v0, Lcom/mediatek/common/agps/MtkAgpsProfile;->mccMnc:Ljava/lang/String;

    goto/16 :goto_3

    :cond_14
    iget-object v12, p0, Lcom/mediatek/common/agps/MtkAgpsProfileManager;->mAgpsProfileList:Ljava/util/List;

    invoke-interface {v12, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto/16 :goto_0

    :cond_15
    const-string v12, "cdma_profile"

    invoke-virtual {v10, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v12

    if-eqz v12, :cond_25

    new-instance v3, Lcom/mediatek/common/agps/MtkAgpsCdmaProfile;

    invoke-direct {v3}, Lcom/mediatek/common/agps/MtkAgpsCdmaProfile;-><init>()V

    const/4 v7, 0x0

    :goto_a
    if-ge v7, v4, :cond_24

    invoke-interface {v11, v7}, Lorg/xmlpull/v1/XmlPullParser;->getAttributeName(I)Ljava/lang/String;

    move-result-object v1

    invoke-interface {v11, v7}, Lorg/xmlpull/v1/XmlPullParser;->getAttributeValue(I)Ljava/lang/String;

    move-result-object v2

    const-string v12, "name"

    invoke-virtual {v1, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v12

    if-eqz v12, :cond_17

    iput-object v2, v3, Lcom/mediatek/common/agps/MtkAgpsCdmaProfile;->mName:Ljava/lang/String;

    :cond_16
    :goto_b
    add-int/lit8 v7, v7, 0x1

    goto :goto_a

    :cond_17
    const-string v12, "mcp_enable"

    invoke-virtual {v1, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v12

    if-eqz v12, :cond_19

    const-string v12, "yes"

    invoke-virtual {v2, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v12

    if-eqz v12, :cond_18

    const/4 v12, 0x1

    :goto_c
    iput v12, v3, Lcom/mediatek/common/agps/MtkAgpsCdmaProfile;->mMcpEnable:I

    goto :goto_b

    :cond_18
    const/4 v12, 0x0

    goto :goto_c

    :cond_19
    const-string v12, "mcp_addr"

    invoke-virtual {v1, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v12

    if-eqz v12, :cond_1a

    iput-object v2, v3, Lcom/mediatek/common/agps/MtkAgpsCdmaProfile;->mMcpAddr:Ljava/lang/String;

    goto :goto_b

    :cond_1a
    const-string v12, "mcp_port"

    invoke-virtual {v1, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v12

    if-eqz v12, :cond_1b

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v12

    invoke-virtual {v12}, Ljava/lang/Integer;->intValue()I

    move-result v12

    iput v12, v3, Lcom/mediatek/common/agps/MtkAgpsCdmaProfile;->mMcpPort:I

    goto :goto_b

    :cond_1b
    const-string v12, "pde_addr_valid"

    invoke-virtual {v1, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v12

    if-eqz v12, :cond_1d

    const-string v12, "yes"

    invoke-virtual {v2, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v12

    if-eqz v12, :cond_1c

    const/4 v12, 0x1

    :goto_d
    iput v12, v3, Lcom/mediatek/common/agps/MtkAgpsCdmaProfile;->mPdeAddrValid:I

    goto :goto_b

    :cond_1c
    const/4 v12, 0x0

    goto :goto_d

    :cond_1d
    const-string v12, "pde_ip_type"

    invoke-virtual {v1, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v12

    if-eqz v12, :cond_1e

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v12

    invoke-virtual {v12}, Ljava/lang/Integer;->intValue()I

    move-result v12

    iput v12, v3, Lcom/mediatek/common/agps/MtkAgpsCdmaProfile;->mPdeIpType:I

    goto :goto_b

    :cond_1e
    const-string v12, "pde_ip4_addr"

    invoke-virtual {v1, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v12

    if-eqz v12, :cond_1f

    iput-object v2, v3, Lcom/mediatek/common/agps/MtkAgpsCdmaProfile;->mPdeIp4Addr:Ljava/lang/String;

    goto :goto_b

    :cond_1f
    const-string v12, "pde_ip6_addr"

    invoke-virtual {v1, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v12

    if-eqz v12, :cond_20

    iput-object v2, v3, Lcom/mediatek/common/agps/MtkAgpsCdmaProfile;->mPdeIp6Addr:Ljava/lang/String;

    goto :goto_b

    :cond_20
    const-string v12, "pde_port"

    invoke-virtual {v1, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v12

    if-eqz v12, :cond_21

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v12

    invoke-virtual {v12}, Ljava/lang/Integer;->intValue()I

    move-result v12

    iput v12, v3, Lcom/mediatek/common/agps/MtkAgpsCdmaProfile;->mPdePort:I

    goto/16 :goto_b

    :cond_21
    const-string v12, "pde_url_valid"

    invoke-virtual {v1, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v12

    if-eqz v12, :cond_23

    const-string v12, "yes"

    invoke-virtual {v2, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v12

    if-eqz v12, :cond_22

    const/4 v12, 0x1

    :goto_e
    iput v12, v3, Lcom/mediatek/common/agps/MtkAgpsCdmaProfile;->mPdeUrlValid:I

    goto/16 :goto_b

    :cond_22
    const/4 v12, 0x0

    goto :goto_e

    :cond_23
    const-string v12, "pde_url_addr"

    invoke-virtual {v1, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v12

    if-eqz v12, :cond_16

    iput-object v2, v3, Lcom/mediatek/common/agps/MtkAgpsCdmaProfile;->mPdeUrlAddr:Ljava/lang/String;

    goto/16 :goto_b

    :cond_24
    iget-object v12, p0, Lcom/mediatek/common/agps/MtkAgpsProfileManager;->mCdmaProfileList:Ljava/util/List;

    invoke-interface {v12, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto/16 :goto_0

    :cond_25
    const-string v12, "agps_conf_para"

    invoke-virtual {v10, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v12

    if-eqz v12, :cond_1

    const/4 v7, 0x0

    :goto_f
    if-ge v7, v4, :cond_1

    invoke-interface {v11, v7}, Lorg/xmlpull/v1/XmlPullParser;->getAttributeName(I)Ljava/lang/String;

    move-result-object v1

    invoke-interface {v11, v7}, Lorg/xmlpull/v1/XmlPullParser;->getAttributeValue(I)Ljava/lang/String;

    move-result-object v2

    const-string v12, "disable_after_reboot"

    invoke-virtual {v1, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v12

    if-eqz v12, :cond_28

    const-string v12, "yes"

    invoke-virtual {v2, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v12

    if-eqz v12, :cond_27

    const/4 v12, 0x1

    :goto_10
    iput-boolean v12, p0, Lcom/mediatek/common/agps/MtkAgpsProfileManager;->mDisableAfterReboot:Z

    :cond_26
    :goto_11
    add-int/lit8 v7, v7, 0x1

    goto :goto_f

    :cond_27
    const/4 v12, 0x0

    goto :goto_10

    :cond_28
    const-string v12, "ni_request"

    invoke-virtual {v1, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v12

    if-eqz v12, :cond_2a

    const-string v12, "yes"

    invoke-virtual {v2, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v12

    if-eqz v12, :cond_29

    const/4 v12, 0x1

    :goto_12
    iput-boolean v12, p0, Lcom/mediatek/common/agps/MtkAgpsProfileManager;->mNiRequest:Z

    goto :goto_11

    :cond_29
    const/4 v12, 0x0

    goto :goto_12

    :cond_2a
    const-string v12, "agps_enable"

    invoke-virtual {v1, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v12

    if-eqz v12, :cond_2c

    const-string v12, "yes"

    invoke-virtual {v2, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v12

    if-eqz v12, :cond_2b

    const/4 v12, 0x1

    :goto_13
    iput-boolean v12, p0, Lcom/mediatek/common/agps/MtkAgpsProfileManager;->mAgpsEnable:Z

    goto :goto_11

    :cond_2b
    const/4 v12, 0x0

    goto :goto_13

    :cond_2c
    const-string v12, "log_file_max_num"

    invoke-virtual {v1, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v12

    if-eqz v12, :cond_2d

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v12

    invoke-virtual {v12}, Ljava/lang/Integer;->intValue()I

    move-result v12

    iput v12, p0, Lcom/mediatek/common/agps/MtkAgpsProfileManager;->mLogFileMaxNum:I

    goto :goto_11

    :cond_2d
    const-string v12, "cp_gemini_pref_sim"

    invoke-virtual {v1, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v12

    if-eqz v12, :cond_2f

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v12

    invoke-virtual {v12}, Ljava/lang/Integer;->intValue()I

    move-result v12

    iput v12, p0, Lcom/mediatek/common/agps/MtkAgpsProfileManager;->mCpGeminiPrefSim:I

    iget v12, p0, Lcom/mediatek/common/agps/MtkAgpsProfileManager;->mCpGeminiPrefSim:I

    if-lez v12, :cond_2e

    iget v12, p0, Lcom/mediatek/common/agps/MtkAgpsProfileManager;->mCpGeminiPrefSim:I

    const/4 v13, 0x4

    if-le v12, v13, :cond_26

    :cond_2e
    const/4 v12, 0x1

    iput v12, p0, Lcom/mediatek/common/agps/MtkAgpsProfileManager;->mCpGeminiPrefSim:I

    goto :goto_11

    :cond_2f
    const-string v12, "roaming"

    invoke-virtual {v1, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v12

    if-eqz v12, :cond_31

    const-string v12, "yes"

    invoke-virtual {v2, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v12

    if-eqz v12, :cond_30

    const/4 v12, 0x1

    :goto_14
    iput-boolean v12, p0, Lcom/mediatek/common/agps/MtkAgpsProfileManager;->mRoamingEnable:Z

    goto :goto_11

    :cond_30
    const/4 v12, 0x0

    goto :goto_14

    :cond_31
    const-string v12, "default_profile"

    invoke-virtual {v1, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v12

    if-eqz v12, :cond_32

    iput-object v2, p0, Lcom/mediatek/common/agps/MtkAgpsProfileManager;->mDefaultProfileName:Ljava/lang/String;

    goto/16 :goto_11

    :cond_32
    const-string v12, "cp_up_selection"

    invoke-virtual {v1, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v12

    if-eqz v12, :cond_34

    const-string v12, "cp"

    invoke-virtual {v2, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v12

    if-eqz v12, :cond_33

    const/4 v12, 0x1

    :goto_15
    iput-boolean v12, p0, Lcom/mediatek/common/agps/MtkAgpsProfileManager;->mCpUpSelection:Z

    goto/16 :goto_11

    :cond_33
    const/4 v12, 0x0

    goto :goto_15

    :cond_34
    const-string v12, "notify_timeout"

    invoke-virtual {v1, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v12

    if-eqz v12, :cond_36

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v12

    invoke-virtual {v12}, Ljava/lang/Integer;->intValue()I

    move-result v12

    iput v12, p0, Lcom/mediatek/common/agps/MtkAgpsProfileManager;->mNotifyTimeout:I

    iget v12, p0, Lcom/mediatek/common/agps/MtkAgpsProfileManager;->mNotifyTimeout:I

    const/16 v13, 0x14

    if-gt v12, v13, :cond_35

    iget v12, p0, Lcom/mediatek/common/agps/MtkAgpsProfileManager;->mNotifyTimeout:I

    if-gez v12, :cond_26

    :cond_35
    const/16 v12, 0x8

    iput v12, p0, Lcom/mediatek/common/agps/MtkAgpsProfileManager;->mNotifyTimeout:I

    goto/16 :goto_11

    :cond_36
    const-string v12, "verify_timeout"

    invoke-virtual {v1, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v12

    if-eqz v12, :cond_38

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v12

    invoke-virtual {v12}, Ljava/lang/Integer;->intValue()I

    move-result v12

    iput v12, p0, Lcom/mediatek/common/agps/MtkAgpsProfileManager;->mVerifyTimeout:I

    iget v12, p0, Lcom/mediatek/common/agps/MtkAgpsProfileManager;->mVerifyTimeout:I

    const/16 v13, 0x14

    if-gt v12, v13, :cond_37

    iget v12, p0, Lcom/mediatek/common/agps/MtkAgpsProfileManager;->mVerifyTimeout:I

    if-gez v12, :cond_26

    :cond_37
    const/16 v12, 0x8

    iput v12, p0, Lcom/mediatek/common/agps/MtkAgpsProfileManager;->mVerifyTimeout:I

    goto/16 :goto_11

    :cond_38
    const-string v12, "ca_enable"

    invoke-virtual {v1, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v12

    if-eqz v12, :cond_3a

    const-string v12, "yes"

    invoke-virtual {v2, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v12

    if-eqz v12, :cond_39

    const/4 v12, 0x1

    :goto_16
    iput-boolean v12, p0, Lcom/mediatek/common/agps/MtkAgpsProfileManager;->mCaEnable:Z

    goto/16 :goto_11

    :cond_39
    const/4 v12, 0x0

    goto :goto_16

    :cond_3a
    const-string v12, "si_mode"

    invoke-virtual {v1, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v12

    if-eqz v12, :cond_3c

    const-string v12, "ma"

    invoke-virtual {v2, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v12

    if-eqz v12, :cond_3b

    const/4 v12, 0x0

    iput v12, p0, Lcom/mediatek/common/agps/MtkAgpsProfileManager;->mSiMode:I

    goto/16 :goto_11

    :cond_3b
    const/4 v12, 0x1

    iput v12, p0, Lcom/mediatek/common/agps/MtkAgpsProfileManager;->mSiMode:I

    goto/16 :goto_11

    :cond_3c
    const-string v12, "ecid_enable"

    invoke-virtual {v1, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v12

    if-eqz v12, :cond_3e

    const-string v12, "yes"

    invoke-virtual {v2, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v12

    if-eqz v12, :cond_3d

    const/4 v12, 0x1

    :goto_17
    iput-boolean v12, p0, Lcom/mediatek/common/agps/MtkAgpsProfileManager;->mEcidStatus:Z

    goto/16 :goto_11

    :cond_3d
    const/4 v12, 0x0

    goto :goto_17

    :cond_3e
    const-string v12, "cp_auto_reset"

    invoke-virtual {v1, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v12

    if-eqz v12, :cond_40

    const-string v12, "yes"

    invoke-virtual {v2, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v12

    if-eqz v12, :cond_3f

    const/4 v12, 0x1

    :goto_18
    iput-boolean v12, p0, Lcom/mediatek/common/agps/MtkAgpsProfileManager;->mPmtk997_5:Z

    goto/16 :goto_11

    :cond_3f
    const/4 v12, 0x0

    goto :goto_18

    :cond_40
    const-string v12, "gpevt"

    invoke-virtual {v1, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v12

    if-eqz v12, :cond_42

    const-string v12, "yes"

    invoke-virtual {v2, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v12

    if-eqz v12, :cond_41

    const/4 v12, 0x1

    :goto_19
    iput-boolean v12, p0, Lcom/mediatek/common/agps/MtkAgpsProfileManager;->mGpevt:Z

    goto/16 :goto_11

    :cond_41
    const/4 v12, 0x0

    goto :goto_19

    :cond_42
    const-string v12, "supl_version"

    invoke-virtual {v1, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v12

    if-eqz v12, :cond_44

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v12

    invoke-virtual {v12}, Ljava/lang/Integer;->intValue()I

    move-result v12

    iput v12, p0, Lcom/mediatek/common/agps/MtkAgpsProfileManager;->mSuplVersion:I

    iget v12, p0, Lcom/mediatek/common/agps/MtkAgpsProfileManager;->mSuplVersion:I

    const/4 v13, 0x2

    if-gt v12, v13, :cond_43

    iget v12, p0, Lcom/mediatek/common/agps/MtkAgpsProfileManager;->mSuplVersion:I

    if-gtz v12, :cond_26

    :cond_43
    const/4 v12, 0x1

    iput v12, p0, Lcom/mediatek/common/agps/MtkAgpsProfileManager;->mSuplVersion:I

    goto/16 :goto_11

    :cond_44
    const-string v12, "cdma_agps_preferred"

    invoke-virtual {v1, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v12

    if-eqz v12, :cond_46

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v12

    invoke-virtual {v12}, Ljava/lang/Integer;->intValue()I

    move-result v12

    iput v12, p0, Lcom/mediatek/common/agps/MtkAgpsProfileManager;->mEvdoPrefer:I

    iget v12, p0, Lcom/mediatek/common/agps/MtkAgpsProfileManager;->mEvdoPrefer:I

    const/4 v13, 0x2

    if-gt v12, v13, :cond_45

    iget v12, p0, Lcom/mediatek/common/agps/MtkAgpsProfileManager;->mEvdoPrefer:I

    if-gtz v12, :cond_26

    :cond_45
    const/4 v12, 0x0

    iput v12, p0, Lcom/mediatek/common/agps/MtkAgpsProfileManager;->mEvdoPrefer:I

    goto/16 :goto_11

    :cond_46
    const-string v12, "default_cdma_profile"

    invoke-virtual {v1, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v12

    if-eqz v12, :cond_26

    iput-object v2, p0, Lcom/mediatek/common/agps/MtkAgpsProfileManager;->mDefaultCdmaProfile:Ljava/lang/String;
    :try_end_11
    .catch Ljava/io/FileNotFoundException; {:try_start_11 .. :try_end_11} :catch_0
    .catch Lorg/xmlpull/v1/XmlPullParserException; {:try_start_11 .. :try_end_11} :catch_2
    .catch Ljava/io/IOException; {:try_start_11 .. :try_end_11} :catch_4
    .catch Ljava/lang/Exception; {:try_start_11 .. :try_end_11} :catch_6
    .catchall {:try_start_11 .. :try_end_11} :catchall_0

    goto/16 :goto_11

    :catch_8
    move-exception v5

    invoke-virtual {v5}, Ljava/lang/Throwable;->printStackTrace()V

    move-object v8, v9

    goto/16 :goto_1

    :catch_9
    move-exception v5

    invoke-virtual {v5}, Ljava/lang/Throwable;->printStackTrace()V

    goto/16 :goto_9

    :catchall_1
    move-exception v12

    goto/16 :goto_8

    :catch_a
    move-exception v5

    goto/16 :goto_7

    :catch_b
    move-exception v5

    goto/16 :goto_6

    :catch_c
    move-exception v5

    goto/16 :goto_5

    :catch_d
    move-exception v5

    goto/16 :goto_4
.end method
