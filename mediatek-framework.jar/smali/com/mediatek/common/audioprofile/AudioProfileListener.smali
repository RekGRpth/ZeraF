.class public Lcom/mediatek/common/audioprofile/AudioProfileListener;
.super Ljava/lang/Object;
.source "AudioProfileListener.java"


# static fields
.field public static final LISTEN_AUDIOPROFILE_CHANGEG:I = 0x1

.field public static final LISTEN_NONE:I = 0x0

.field public static final LISTEN_RINGERMODE_CHANGED:I = 0x2

.field public static final LISTEN_RINGER_VOLUME_CHANGED:I = 0x4

.field public static final LISTEN_VIBRATE_SETTING_CHANGED:I = 0x8

.field private static final TAG:Ljava/lang/String; = "AudioProfileListener"


# instance fields
.field callback:Lcom/mediatek/common/audioprofile/IAudioProfileListener;

.field mHandler:Landroid/os/Handler;


# direct methods
.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Lcom/mediatek/common/audioprofile/AudioProfileListener$1;

    invoke-direct {v0, p0}, Lcom/mediatek/common/audioprofile/AudioProfileListener$1;-><init>(Lcom/mediatek/common/audioprofile/AudioProfileListener;)V

    iput-object v0, p0, Lcom/mediatek/common/audioprofile/AudioProfileListener;->callback:Lcom/mediatek/common/audioprofile/IAudioProfileListener;

    new-instance v0, Lcom/mediatek/common/audioprofile/AudioProfileListener$2;

    invoke-direct {v0, p0}, Lcom/mediatek/common/audioprofile/AudioProfileListener$2;-><init>(Lcom/mediatek/common/audioprofile/AudioProfileListener;)V

    iput-object v0, p0, Lcom/mediatek/common/audioprofile/AudioProfileListener;->mHandler:Landroid/os/Handler;

    return-void
.end method


# virtual methods
.method public getCallback()Lcom/mediatek/common/audioprofile/IAudioProfileListener;
    .locals 1

    iget-object v0, p0, Lcom/mediatek/common/audioprofile/AudioProfileListener;->callback:Lcom/mediatek/common/audioprofile/IAudioProfileListener;

    return-object v0
.end method

.method public onAudioProfileChanged(Ljava/lang/String;)V
    .locals 0
    .param p1    # Ljava/lang/String;

    return-void
.end method

.method public onRingerModeChanged(I)V
    .locals 0
    .param p1    # I

    return-void
.end method

.method public onRingerVolumeChanged(IILjava/lang/String;)V
    .locals 0
    .param p1    # I
    .param p2    # I
    .param p3    # Ljava/lang/String;

    return-void
.end method

.method public onVibrateSettingChanged(II)V
    .locals 0
    .param p1    # I
    .param p2    # I

    return-void
.end method
