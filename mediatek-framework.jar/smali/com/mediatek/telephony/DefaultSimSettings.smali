.class public Lcom/mediatek/telephony/DefaultSimSettings;
.super Ljava/lang/Object;
.source "DefaultSimSettings.java"


# static fields
.field private static final LOG_TAG:Ljava/lang/String; = "PHONE"


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private static get3GSimId()I
    .locals 4

    const/4 v1, 0x0

    const-string v2, "gsm.3gswitch"

    invoke-static {v2, v1}, Landroid/os/SystemProperties;->getInt(Ljava/lang/String;I)I

    move-result v0

    if-lez v0, :cond_0

    sget v2, Lcom/android/internal/telephony/PhoneConstants;->GEMINI_SIM_NUM:I

    if-gt v0, v2, :cond_0

    add-int/lit8 v1, v0, -0x1

    :goto_0
    return v1

    :cond_0
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "get3GSimId() got invalid property value:"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/mediatek/telephony/DefaultSimSettings;->logd(Ljava/lang/String;)V

    goto :goto_0
.end method

.method private static isSimRemoved(J[JI)Z
    .locals 4
    .param p0    # J
    .param p2    # [J
    .param p3    # I

    const-wide/16 v2, 0x0

    cmp-long v2, p0, v2

    if-gtz v2, :cond_1

    const/4 v1, 0x0

    :cond_0
    :goto_0
    return v1

    :cond_1
    const/4 v1, 0x1

    const/4 v0, 0x0

    :goto_1
    if-ge v0, p3, :cond_0

    aget-wide v2, p2, v0

    cmp-long v2, p0, v2

    if-nez v2, :cond_2

    const/4 v1, 0x0

    goto :goto_0

    :cond_2
    add-int/lit8 v0, v0, 0x1

    goto :goto_1
.end method

.method private static logd(Ljava/lang/String;)V
    .locals 3
    .param p0    # Ljava/lang/String;

    const-string v0, "PHONE"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "[DefaultSimSetting] "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    return-void
.end method

.method public static setAllDefaultSim(Landroid/content/Context;Landroid/content/ContentResolver;Ljava/util/List;[J[ZIIZ[IZ)V
    .locals 33
    .param p0    # Landroid/content/Context;
    .param p1    # Landroid/content/ContentResolver;
    .param p3    # [J
    .param p4    # [Z
    .param p5    # I
    .param p6    # I
    .param p7    # Z
    .param p8    # [I
    .param p9    # Z
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Landroid/content/ContentResolver;",
            "Ljava/util/List",
            "<",
            "Lcom/mediatek/telephony/SimInfoManager$SimInfoRecord;",
            ">;[J[ZIIZ[IZ)V"
        }
    .end annotation

    const/16 v29, 0x0

    :try_start_0
    const-class v30, Lcom/mediatek/common/telephony/ITelephonyExt;

    const/16 v31, 0x0

    move/from16 v0, v31

    new-array v0, v0, [Ljava/lang/Object;

    move-object/from16 v31, v0

    invoke-static/range {v30 .. v31}, Lcom/mediatek/common/MediatekClassFactory;->createInstance(Ljava/lang/Class;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v30

    move-object/from16 v0, v30

    check-cast v0, Lcom/mediatek/common/telephony/ITelephonyExt;

    move-object/from16 v29, v0
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    invoke-static {}, Lcom/mediatek/telephony/DefaultSimSettings;->get3GSimId()I

    move-result v15

    const-string v30, "video_call_sim_setting"

    const-wide/16 v31, -0x5

    move-object/from16 v0, p1

    move-object/from16 v1, v30

    move-wide/from16 v2, v31

    invoke-static {v0, v1, v2, v3}, Landroid/provider/Settings$System;->getLong(Landroid/content/ContentResolver;Ljava/lang/String;J)J

    move-result-wide v22

    const-string v30, "voice_call_sim_setting"

    const-wide/16 v31, -0x5

    move-object/from16 v0, p1

    move-object/from16 v1, v30

    move-wide/from16 v2, v31

    invoke-static {v0, v1, v2, v3}, Landroid/provider/Settings$System;->getLong(Landroid/content/ContentResolver;Ljava/lang/String;J)J

    move-result-wide v24

    const-string v30, "sms_sim_setting"

    const-wide/16 v31, -0x5

    move-object/from16 v0, p1

    move-object/from16 v1, v30

    move-wide/from16 v2, v31

    invoke-static {v0, v1, v2, v3}, Landroid/provider/Settings$System;->getLong(Landroid/content/ContentResolver;Ljava/lang/String;J)J

    move-result-wide v20

    const-string v30, "gprs_connection_sim_setting"

    const-wide/16 v31, -0x5

    move-object/from16 v0, p1

    move-object/from16 v1, v30

    move-wide/from16 v2, v31

    invoke-static {v0, v1, v2, v3}, Landroid/provider/Settings$System;->getLong(Landroid/content/ContentResolver;Ljava/lang/String;J)J

    move-result-wide v18

    const-wide/16 v16, -0x5

    aget-boolean v30, p4, v15

    if-eqz v30, :cond_0

    aget-wide v16, p3, v15

    :cond_0
    const-string v30, "video_call_sim_setting"

    move-object/from16 v0, p1

    move-object/from16 v1, v30

    move-wide/from16 v2, v16

    invoke-static {v0, v1, v2, v3}, Landroid/provider/Settings$System;->putLong(Landroid/content/ContentResolver;Ljava/lang/String;J)Z

    const-string v30, "connectivity"

    move-object/from16 v0, p0

    move-object/from16 v1, v30

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Landroid/net/ConnectivityManager;

    const/16 v30, 0x1

    move/from16 v0, p6

    move/from16 v1, v30

    if-le v0, v1, :cond_c

    const-wide/16 v30, -0x5

    cmp-long v30, v24, v30

    if-nez v30, :cond_1

    const-string v30, "voice_call_sim_setting"

    const-wide/16 v31, -0x1

    move-object/from16 v0, p1

    move-object/from16 v1, v30

    move-wide/from16 v2, v31

    invoke-static {v0, v1, v2, v3}, Landroid/provider/Settings$System;->putLong(Landroid/content/ContentResolver;Ljava/lang/String;J)Z

    :cond_1
    const-wide/16 v30, -0x5

    cmp-long v30, v20, v30

    if-nez v30, :cond_2

    const-string v30, "ro.operator.optr"

    invoke-static/range {v30 .. v30}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v30

    invoke-static/range {v30 .. v30}, Lcom/mediatek/telephony/DefaultSimSettings;->logd(Ljava/lang/String;)V

    const-string v30, "OP01"

    const-string v31, "ro.operator.optr"

    invoke-static/range {v31 .. v31}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v31

    invoke-virtual/range {v30 .. v31}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v30

    if-eqz v30, :cond_a

    const-string v30, "sms_sim_setting"

    const-wide/16 v31, -0x3

    move-object/from16 v0, p1

    move-object/from16 v1, v30

    move-wide/from16 v2, v31

    invoke-static {v0, v1, v2, v3}, Landroid/provider/Settings$System;->putLong(Landroid/content/ContentResolver;Ljava/lang/String;J)Z

    :cond_2
    :goto_1
    const-wide/16 v30, -0x5

    cmp-long v30, v18, v30

    if-nez v30, :cond_3

    invoke-interface/range {v29 .. v29}, Lcom/mediatek/common/telephony/ITelephonyExt;->isDefaultDataOn()Z

    move-result v30

    if-eqz v30, :cond_b

    invoke-virtual {v4, v15}, Landroid/net/ConnectivityManager;->setMobileDataEnabledGemini(I)Z

    :cond_3
    :goto_2
    const-wide/16 v5, -0x5

    const/16 v30, 0x1

    move/from16 v0, p6

    move/from16 v1, v30

    if-le v0, v1, :cond_10

    const-wide/16 v5, -0x1

    :cond_4
    :goto_3
    sget v30, Lcom/android/internal/telephony/PhoneConstants;->GEMINI_SIM_NUM:I

    move-wide/from16 v0, v24

    move-object/from16 v2, p3

    move/from16 v3, v30

    invoke-static {v0, v1, v2, v3}, Lcom/mediatek/telephony/DefaultSimSettings;->isSimRemoved(J[JI)Z

    move-result v30

    if-eqz v30, :cond_5

    const-string v30, "voice_call_sim_setting"

    move-object/from16 v0, p1

    move-object/from16 v1, v30

    invoke-static {v0, v1, v5, v6}, Landroid/provider/Settings$System;->putLong(Landroid/content/ContentResolver;Ljava/lang/String;J)Z

    :cond_5
    sget v30, Lcom/android/internal/telephony/PhoneConstants;->GEMINI_SIM_NUM:I

    move-wide/from16 v0, v20

    move-object/from16 v2, p3

    move/from16 v3, v30

    invoke-static {v0, v1, v2, v3}, Lcom/mediatek/telephony/DefaultSimSettings;->isSimRemoved(J[JI)Z

    move-result v30

    if-eqz v30, :cond_7

    move-wide v7, v5

    const-string v30, "ro.operator.optr"

    invoke-static/range {v30 .. v30}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v30

    invoke-static/range {v30 .. v30}, Lcom/mediatek/telephony/DefaultSimSettings;->logd(Ljava/lang/String;)V

    const-string v30, "OP01"

    const-string v31, "ro.operator.optr"

    invoke-static/range {v31 .. v31}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v31

    invoke-virtual/range {v30 .. v31}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v30

    if-eqz v30, :cond_6

    const/16 v30, 0x1

    move/from16 v0, p6

    move/from16 v1, v30

    if-le v0, v1, :cond_11

    const-wide/16 v7, -0x3

    :cond_6
    :goto_4
    const-string v30, "sms_sim_setting"

    move-object/from16 v0, p1

    move-object/from16 v1, v30

    invoke-static {v0, v1, v7, v8}, Landroid/provider/Settings$System;->putLong(Landroid/content/ContentResolver;Ljava/lang/String;J)Z

    :cond_7
    sget v30, Lcom/android/internal/telephony/PhoneConstants;->GEMINI_SIM_NUM:I

    move-wide/from16 v0, v18

    move-object/from16 v2, p3

    move/from16 v3, v30

    invoke-static {v0, v1, v2, v3}, Lcom/mediatek/telephony/DefaultSimSettings;->isSimRemoved(J[JI)Z

    move-result v30

    if-eqz v30, :cond_14

    invoke-interface/range {v29 .. v29}, Lcom/mediatek/common/telephony/ITelephonyExt;->isDefaultDataOn()Z

    move-result v30

    if-eqz v30, :cond_13

    if-lez p6, :cond_8

    const-string v30, "default data SIM removed and default data on, switch to 3G SIM"

    invoke-static/range {v30 .. v30}, Lcom/mediatek/telephony/DefaultSimSettings;->logd(Ljava/lang/String;)V

    aget-boolean v30, p4, v15

    if-eqz v30, :cond_12

    invoke-virtual {v4, v15}, Landroid/net/ConnectivityManager;->setMobileDataEnabledGemini(I)Z

    :cond_8
    :goto_5
    const-string v30, "gprs_connection_sim_setting"

    const-wide/16 v31, -0x5

    move-object/from16 v0, p1

    move-object/from16 v1, v30

    move-wide/from16 v2, v31

    invoke-static {v0, v1, v2, v3}, Landroid/provider/Settings$System;->getLong(Landroid/content/ContentResolver;Ljava/lang/String;J)J

    move-result-wide v10

    const-wide/16 v30, -0x5

    cmp-long v30, v10, v30

    if-eqz v30, :cond_9

    const-wide/16 v30, 0x0

    cmp-long v30, v10, v30

    if-eqz v30, :cond_9

    move-object/from16 v0, p0

    invoke-static {v0, v10, v11}, Lcom/mediatek/telephony/SimInfoManager;->getSlotById(Landroid/content/Context;J)I

    move-result v28

    const/16 v30, -0x1

    move/from16 v0, v28

    move/from16 v1, v30

    if-eq v0, v1, :cond_1c

    move/from16 v0, v28

    invoke-virtual {v4, v0}, Landroid/net/ConnectivityManager;->setMobileDataEnabledGemini(I)Z

    :cond_9
    :goto_6
    return-void

    :catch_0
    move-exception v9

    invoke-virtual {v9}, Ljava/lang/Throwable;->printStackTrace()V

    goto/16 :goto_0

    :cond_a
    const-string v30, "sms_sim_setting"

    const-wide/16 v31, -0x1

    move-object/from16 v0, p1

    move-object/from16 v1, v30

    move-wide/from16 v2, v31

    invoke-static {v0, v1, v2, v3}, Landroid/provider/Settings$System;->putLong(Landroid/content/ContentResolver;Ljava/lang/String;J)Z

    goto/16 :goto_1

    :cond_b
    const/16 v30, 0x0

    move/from16 v0, v30

    invoke-virtual {v4, v0}, Landroid/net/ConnectivityManager;->setMobileDataEnabled(Z)V

    goto/16 :goto_2

    :cond_c
    const/16 v30, 0x1

    move/from16 v0, p6

    move/from16 v1, v30

    if-ne v0, v1, :cond_3

    const/16 v30, 0x0

    move-object/from16 v0, p2

    move/from16 v1, v30

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v30

    check-cast v30, Lcom/mediatek/telephony/SimInfoManager$SimInfoRecord;

    move-object/from16 v0, v30

    iget-wide v0, v0, Lcom/mediatek/telephony/SimInfoManager$SimInfoRecord;->mSimInfoId:J

    move-wide/from16 v26, v0

    const-string v30, "enable_internet_call_value"

    const/16 v31, 0x0

    move-object/from16 v0, p1

    move-object/from16 v1, v30

    move/from16 v2, v31

    invoke-static {v0, v1, v2}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v14

    invoke-static/range {p0 .. p0}, Landroid/net/sip/SipManager;->isVoipSupported(Landroid/content/Context;)Z

    move-result v30

    if-eqz v30, :cond_d

    if-eqz v14, :cond_d

    const-wide/16 v30, -0x5

    cmp-long v30, v24, v30

    if-nez v30, :cond_e

    :cond_d
    const-string v30, "voice_call_sim_setting"

    move-object/from16 v0, p1

    move-object/from16 v1, v30

    move-wide/from16 v2, v26

    invoke-static {v0, v1, v2, v3}, Landroid/provider/Settings$System;->putLong(Landroid/content/ContentResolver;Ljava/lang/String;J)Z

    :cond_e
    const-string v30, "sms_sim_setting"

    move-object/from16 v0, p1

    move-object/from16 v1, v30

    move-wide/from16 v2, v26

    invoke-static {v0, v1, v2, v3}, Landroid/provider/Settings$System;->putLong(Landroid/content/ContentResolver;Ljava/lang/String;J)Z

    const-wide/16 v30, -0x5

    cmp-long v30, v18, v30

    if-nez v30, :cond_3

    invoke-interface/range {v29 .. v29}, Lcom/mediatek/common/telephony/ITelephonyExt;->isDefaultDataOn()Z

    move-result v30

    if-eqz v30, :cond_f

    const/16 v30, 0x0

    move-object/from16 v0, p2

    move/from16 v1, v30

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v30

    check-cast v30, Lcom/mediatek/telephony/SimInfoManager$SimInfoRecord;

    move-object/from16 v0, v30

    iget v0, v0, Lcom/mediatek/telephony/SimInfoManager$SimInfoRecord;->mSimSlotId:I

    move/from16 v30, v0

    move/from16 v0, v30

    invoke-virtual {v4, v0}, Landroid/net/ConnectivityManager;->setMobileDataEnabledGemini(I)Z

    goto/16 :goto_2

    :cond_f
    const/16 v30, 0x0

    move/from16 v0, v30

    invoke-virtual {v4, v0}, Landroid/net/ConnectivityManager;->setMobileDataEnabled(Z)V

    goto/16 :goto_2

    :cond_10
    const/16 v30, 0x1

    move/from16 v0, p6

    move/from16 v1, v30

    if-ne v0, v1, :cond_4

    const/16 v30, 0x0

    move-object/from16 v0, p2

    move/from16 v1, v30

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v30

    check-cast v30, Lcom/mediatek/telephony/SimInfoManager$SimInfoRecord;

    move-object/from16 v0, v30

    iget-wide v5, v0, Lcom/mediatek/telephony/SimInfoManager$SimInfoRecord;->mSimInfoId:J

    goto/16 :goto_3

    :cond_11
    const/16 v30, 0x1

    move/from16 v0, p6

    move/from16 v1, v30

    if-ne v0, v1, :cond_6

    const/16 v30, 0x0

    move-object/from16 v0, p2

    move/from16 v1, v30

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v30

    check-cast v30, Lcom/mediatek/telephony/SimInfoManager$SimInfoRecord;

    move-object/from16 v0, v30

    iget-wide v7, v0, Lcom/mediatek/telephony/SimInfoManager$SimInfoRecord;->mSimInfoId:J

    goto/16 :goto_4

    :cond_12
    const/16 v30, 0x0

    move-object/from16 v0, p2

    move/from16 v1, v30

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v30

    check-cast v30, Lcom/mediatek/telephony/SimInfoManager$SimInfoRecord;

    move-object/from16 v0, v30

    iget v0, v0, Lcom/mediatek/telephony/SimInfoManager$SimInfoRecord;->mSimSlotId:I

    move/from16 v30, v0

    move/from16 v0, v30

    invoke-virtual {v4, v0}, Landroid/net/ConnectivityManager;->setMobileDataEnabledGemini(I)Z

    goto/16 :goto_5

    :cond_13
    const/16 v30, 0x0

    move/from16 v0, v30

    invoke-virtual {v4, v0}, Landroid/net/ConnectivityManager;->setMobileDataEnabled(Z)V

    goto/16 :goto_5

    :cond_14
    invoke-interface/range {v29 .. v29}, Lcom/mediatek/common/telephony/ITelephonyExt;->isDefaultEnable3GSIMDataWhenNewSIMInserted()Z

    move-result v30

    if-eqz v30, :cond_8

    const-wide/16 v30, 0x0

    cmp-long v30, v18, v30

    if-lez v30, :cond_1a

    if-lez p5, :cond_16

    const-string v30, "SIM swapped and data on, default switch to 3G SIM"

    invoke-static/range {v30 .. v30}, Lcom/mediatek/telephony/DefaultSimSettings;->logd(Ljava/lang/String;)V

    aget-boolean v30, p4, v15

    if-eqz v30, :cond_15

    invoke-virtual {v4, v15}, Landroid/net/ConnectivityManager;->setMobileDataEnabledGemini(I)Z

    goto/16 :goto_5

    :cond_15
    const/16 v30, 0x0

    move-object/from16 v0, p2

    move/from16 v1, v30

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v30

    check-cast v30, Lcom/mediatek/telephony/SimInfoManager$SimInfoRecord;

    move-object/from16 v0, v30

    iget v0, v0, Lcom/mediatek/telephony/SimInfoManager$SimInfoRecord;->mSimSlotId:I

    move/from16 v30, v0

    move/from16 v0, v30

    invoke-virtual {v4, v0}, Landroid/net/ConnectivityManager;->setMobileDataEnabledGemini(I)Z

    goto/16 :goto_5

    :cond_16
    const/4 v12, 0x0

    const/4 v13, 0x0

    :goto_7
    move-object/from16 v0, p8

    array-length v0, v0

    move/from16 v30, v0

    move/from16 v0, v30

    if-ge v13, v0, :cond_17

    aget v30, p8, v13

    const/16 v31, -0x3

    move/from16 v0, v30

    move/from16 v1, v31

    if-ne v0, v1, :cond_18

    const/4 v12, 0x1

    :cond_17
    if-nez p9, :cond_8

    if-eqz v12, :cond_8

    const-string v30, "Some SIM is switched slot, default switch to 3G SIM"

    invoke-static/range {v30 .. v30}, Lcom/mediatek/telephony/DefaultSimSettings;->logd(Ljava/lang/String;)V

    aget-boolean v30, p4, v15

    if-eqz v30, :cond_19

    invoke-virtual {v4, v15}, Landroid/net/ConnectivityManager;->setMobileDataEnabledGemini(I)Z

    goto/16 :goto_5

    :cond_18
    add-int/lit8 v13, v13, 0x1

    goto :goto_7

    :cond_19
    const/16 v30, 0x0

    move-object/from16 v0, p2

    move/from16 v1, v30

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v30

    check-cast v30, Lcom/mediatek/telephony/SimInfoManager$SimInfoRecord;

    move-object/from16 v0, v30

    iget v0, v0, Lcom/mediatek/telephony/SimInfoManager$SimInfoRecord;->mSimSlotId:I

    move/from16 v30, v0

    move/from16 v0, v30

    invoke-virtual {v4, v0}, Landroid/net/ConnectivityManager;->setMobileDataEnabledGemini(I)Z

    goto/16 :goto_5

    :cond_1a
    if-lez p5, :cond_8

    move/from16 v0, p5

    move/from16 v1, p6

    if-ne v0, v1, :cond_8

    const-string v30, "All SIM new, data off and default switch data to 3G SIM"

    invoke-static/range {v30 .. v30}, Lcom/mediatek/telephony/DefaultSimSettings;->logd(Ljava/lang/String;)V

    aget-boolean v30, p4, v15

    if-eqz v30, :cond_1b

    invoke-virtual {v4, v15}, Landroid/net/ConnectivityManager;->setMobileDataEnabledGemini(I)Z

    goto/16 :goto_5

    :cond_1b
    const/16 v30, 0x0

    move-object/from16 v0, p2

    move/from16 v1, v30

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v30

    check-cast v30, Lcom/mediatek/telephony/SimInfoManager$SimInfoRecord;

    move-object/from16 v0, v30

    iget v0, v0, Lcom/mediatek/telephony/SimInfoManager$SimInfoRecord;->mSimSlotId:I

    move/from16 v30, v0

    move/from16 v0, v30

    invoke-virtual {v4, v0}, Landroid/net/ConnectivityManager;->setMobileDataEnabledGemini(I)Z

    goto/16 :goto_5

    :cond_1c
    const-string v30, "gprsDefaultSIM does not exist in slot then skip."

    invoke-static/range {v30 .. v30}, Lcom/mediatek/telephony/DefaultSimSettings;->logd(Ljava/lang/String;)V

    goto/16 :goto_6
.end method
