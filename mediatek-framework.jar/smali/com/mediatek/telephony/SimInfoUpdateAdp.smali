.class public Lcom/mediatek/telephony/SimInfoUpdateAdp;
.super Lcom/mediatek/telephony/SimInfoUpdate;
.source "SimInfoUpdateAdp.java"

# interfaces
.implements Lcom/mediatek/common/telephony/ISimInfoUpdate;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0
    .param p1    # Landroid/content/Context;

    invoke-direct {p0, p1}, Lcom/mediatek/telephony/SimInfoUpdate;-><init>(Landroid/content/Context;)V

    return-void
.end method

.method public constructor <init>(Z)V
    .locals 0
    .param p1    # Z

    invoke-direct {p0, p1}, Lcom/mediatek/telephony/SimInfoUpdate;-><init>(Z)V

    return-void
.end method


# virtual methods
.method public broadcastSimInsertedStatusAdp(I)V
    .locals 0
    .param p1    # I

    invoke-static {p1}, Lcom/mediatek/telephony/SimInfoUpdate;->broadcastSimInsertedStatus(I)V

    return-void
.end method

.method public disposeReceiverAdp()V
    .locals 0

    invoke-static {}, Lcom/mediatek/telephony/SimInfoUpdate;->disposeReceiver()V

    return-void
.end method

.method public setDefaultNameForNewSimAdp(Landroid/content/Context;Ljava/lang/String;I)V
    .locals 0
    .param p1    # Landroid/content/Context;
    .param p2    # Ljava/lang/String;
    .param p3    # I

    invoke-static {p1, p2, p3}, Lcom/mediatek/telephony/SimInfoUpdate;->setDefaultNameForNewSim(Landroid/content/Context;Ljava/lang/String;I)V

    return-void
.end method

.method public updateSimInfoByIccIdAdp(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)V
    .locals 0
    .param p1    # Ljava/lang/String;
    .param p2    # Ljava/lang/String;
    .param p3    # Ljava/lang/String;
    .param p4    # Ljava/lang/String;
    .param p5    # Z

    invoke-static {p1, p2, p3, p4, p5}, Lcom/mediatek/telephony/SimInfoUpdate;->updateSimInfoByIccId(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)V

    return-void
.end method
