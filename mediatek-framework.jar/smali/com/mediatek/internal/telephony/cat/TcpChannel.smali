.class Lcom/mediatek/internal/telephony/cat/TcpChannel;
.super Lcom/mediatek/internal/telephony/cat/Channel;
.source "Channel.java"


# static fields
.field private static final TCP_CONN_TIMEOUT:I = 0x3a98


# instance fields
.field mInput:Ljava/io/DataInputStream;

.field mOutput:Ljava/io/BufferedOutputStream;

.field mSocket:Ljava/net/Socket;

.field rt:Ljava/lang/Thread;


# direct methods
.method constructor <init>(IIILjava/net/InetAddress;IILcom/android/internal/telephony/cat/CatService;Lcom/mediatek/internal/telephony/cat/BipManager;)V
    .locals 1
    .param p1    # I
    .param p2    # I
    .param p3    # I
    .param p4    # Ljava/net/InetAddress;
    .param p5    # I
    .param p6    # I
    .param p7    # Lcom/android/internal/telephony/cat/CatService;
    .param p8    # Lcom/mediatek/internal/telephony/cat/BipManager;

    const/4 v0, 0x0

    invoke-direct/range {p0 .. p8}, Lcom/mediatek/internal/telephony/cat/Channel;-><init>(IIILjava/net/InetAddress;IILcom/android/internal/telephony/cat/CatService;Lcom/mediatek/internal/telephony/cat/BipManager;)V

    iput-object v0, p0, Lcom/mediatek/internal/telephony/cat/TcpChannel;->mSocket:Ljava/net/Socket;

    iput-object v0, p0, Lcom/mediatek/internal/telephony/cat/TcpChannel;->mInput:Ljava/io/DataInputStream;

    iput-object v0, p0, Lcom/mediatek/internal/telephony/cat/TcpChannel;->mOutput:Ljava/io/BufferedOutputStream;

    return-void
.end method

.method private onOpenChannelCompleted()V
    .locals 0

    return-void
.end method


# virtual methods
.method public closeChannel()I
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public getTxAvailBufferSize()I
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public openChannel(Lcom/android/internal/telephony/cat/CatCmdMessage;)I
    .locals 1
    .param p1    # Lcom/android/internal/telephony/cat/CatCmdMessage;

    const/4 v0, 0x0

    return v0
.end method

.method public receiveData(ILcom/mediatek/internal/telephony/cat/ReceiveDataResult;)I
    .locals 1
    .param p1    # I
    .param p2    # Lcom/mediatek/internal/telephony/cat/ReceiveDataResult;

    const/4 v0, 0x0

    return v0
.end method

.method public receiveData(I)Lcom/mediatek/internal/telephony/cat/ReceiveDataResult;
    .locals 1
    .param p1    # I

    new-instance v0, Lcom/mediatek/internal/telephony/cat/ReceiveDataResult;

    invoke-direct {v0}, Lcom/mediatek/internal/telephony/cat/ReceiveDataResult;-><init>()V

    return-object v0
.end method

.method public sendData([BI)I
    .locals 1
    .param p1    # [B
    .param p2    # I

    const/4 v0, 0x0

    return v0
.end method
