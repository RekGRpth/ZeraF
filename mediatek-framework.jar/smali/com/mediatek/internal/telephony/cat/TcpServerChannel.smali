.class Lcom/mediatek/internal/telephony/cat/TcpServerChannel;
.super Lcom/mediatek/internal/telephony/cat/Channel;
.source "Channel.java"


# instance fields
.field private mCloseBackToTcpListen:Z

.field protected mInput:Ljava/io/DataInputStream;

.field protected mOutput:Ljava/io/BufferedOutputStream;

.field protected mSSocket:Ljava/net/ServerSocket;

.field protected mSocket:Ljava/net/Socket;

.field private rt:Ljava/lang/Thread;


# direct methods
.method constructor <init>(IIIIILcom/android/internal/telephony/cat/CatService;Lcom/mediatek/internal/telephony/cat/BipManager;)V
    .locals 9
    .param p1    # I
    .param p2    # I
    .param p3    # I
    .param p4    # I
    .param p5    # I
    .param p6    # Lcom/android/internal/telephony/cat/CatService;
    .param p7    # Lcom/mediatek/internal/telephony/cat/BipManager;

    const/4 v4, 0x0

    move-object v0, p0

    move v1, p1

    move v2, p2

    move v3, p3

    move v5, p4

    move v6, p5

    move-object v7, p6

    move-object/from16 v8, p7

    invoke-direct/range {v0 .. v8}, Lcom/mediatek/internal/telephony/cat/Channel;-><init>(IIILjava/net/InetAddress;IILcom/android/internal/telephony/cat/CatService;Lcom/mediatek/internal/telephony/cat/BipManager;)V

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/mediatek/internal/telephony/cat/TcpServerChannel;->mSSocket:Ljava/net/ServerSocket;

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/mediatek/internal/telephony/cat/TcpServerChannel;->mSocket:Ljava/net/Socket;

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/mediatek/internal/telephony/cat/TcpServerChannel;->mInput:Ljava/io/DataInputStream;

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/mediatek/internal/telephony/cat/TcpServerChannel;->mOutput:Ljava/io/BufferedOutputStream;

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/mediatek/internal/telephony/cat/TcpServerChannel;->rt:Ljava/lang/Thread;

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/mediatek/internal/telephony/cat/TcpServerChannel;->mCloseBackToTcpListen:Z

    return-void
.end method


# virtual methods
.method public closeChannel()I
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public getTcpStatus()B
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public getTxAvailBufferSize()I
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public isCloseBackToTcpListen()Z
    .locals 1

    iget-boolean v0, p0, Lcom/mediatek/internal/telephony/cat/TcpServerChannel;->mCloseBackToTcpListen:Z

    return v0
.end method

.method public openChannel(Lcom/android/internal/telephony/cat/CatCmdMessage;)I
    .locals 1
    .param p1    # Lcom/android/internal/telephony/cat/CatCmdMessage;

    const/4 v0, 0x0

    return v0
.end method

.method public receiveData(ILcom/mediatek/internal/telephony/cat/ReceiveDataResult;)I
    .locals 3
    .param p1    # I
    .param p2    # Lcom/mediatek/internal/telephony/cat/ReceiveDataResult;

    const-string v1, "[BIP]"

    const-string v2, "[UICC]new receiveData method"

    invoke-static {v1, v2}, Lcom/android/internal/telephony/cat/CatLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    const/4 v0, 0x0

    return v0
.end method

.method public receiveData(I)Lcom/mediatek/internal/telephony/cat/ReceiveDataResult;
    .locals 1
    .param p1    # I

    new-instance v0, Lcom/mediatek/internal/telephony/cat/ReceiveDataResult;

    invoke-direct {v0}, Lcom/mediatek/internal/telephony/cat/ReceiveDataResult;-><init>()V

    return-object v0
.end method

.method public sendData([BI)I
    .locals 1
    .param p1    # [B
    .param p2    # I

    const/4 v0, 0x0

    return v0
.end method

.method public setCloseBackToTcpListen(Z)V
    .locals 0
    .param p1    # Z

    return-void
.end method

.method public setTcpStatus(BZ)V
    .locals 0
    .param p1    # B
    .param p2    # Z

    return-void
.end method
