.class public Lcom/mediatek/omacp/message/OmacpApplicationCapability;
.super Ljava/lang/Object;
.source "OmacpApplicationCapability.java"


# static fields
.field public static sBrowser:Z

.field public static sBrowserBookMark:Z

.field public static sBrowserBookMarkFolder:Z

.field public static sBrowserBookMarkName:Z

.field public static sBrowserHomePage:Z

.field public static sBrowserPassWord:Z

.field public static sBrowserToNapId:Z

.field public static sBrowserToProxy:Z

.field public static sBrowserUserName:Z

.field public static sDm:Z

.field public static sDmAddrType:Z

.field public static sDmAuthData:Z

.field public static sDmAuthLevel:Z

.field public static sDmAuthName:Z

.field public static sDmAuthSecret:Z

.field public static sDmAuthType:Z

.field public static sDmInit:Z

.field public static sDmPortNumber:Z

.field public static sDmProviderId:Z

.field public static sDmServerAddress:Z

.field public static sDmServerName:Z

.field public static sDmToNapid:Z

.field public static sDmToProxy:Z

.field public static sDs:Z

.field public static sDsAddressType:Z

.field public static sDsAuthData:Z

.field public static sDsAuthLevel:Z

.field public static sDsAuthName:Z

.field public static sDsAuthSecret:Z

.field public static sDsAuthType:Z

.field public static sDsClientDatabaseUrl:Z

.field public static sDsDatabaseAuthName:Z

.field public static sDsDatabaseAuthSecret:Z

.field public static sDsDatabaseAuthType:Z

.field public static sDsDatabaseContentType:Z

.field public static sDsDatabaseName:Z

.field public static sDsDatabaseUrl:Z

.field public static sDsPortNumber:Z

.field public static sDsProviderId:Z

.field public static sDsServerAddress:Z

.field public static sDsServerName:Z

.field public static sDsSyncType:Z

.field public static sDsToNapid:Z

.field public static sDsToProxy:Z

.field public static sEmail:Z

.field public static sEmailFrom:Z

.field public static sEmailInboundAddr:Z

.field public static sEmailInboundAddrType:Z

.field public static sEmailInboundAuthType:Z

.field public static sEmailInboundPassword:Z

.field public static sEmailInboundPortNumber:Z

.field public static sEmailInboundSecure:Z

.field public static sEmailInboundUserName:Z

.field public static sEmailOutboundAddr:Z

.field public static sEmailOutboundAddrType:Z

.field public static sEmailOutboundAuthType:Z

.field public static sEmailOutboundPassword:Z

.field public static sEmailOutboundPortNumber:Z

.field public static sEmailOutboundSecure:Z

.field public static sEmailOutboundUserName:Z

.field public static sEmailProviderId:Z

.field public static sEmailRtAddr:Z

.field public static sEmailSettingName:Z

.field public static sEmailToNapid:Z

.field public static sImps:Z

.field public static sImpsAddressType:Z

.field public static sImpsAuthLevel:Z

.field public static sImpsAuthName:Z

.field public static sImpsAuthSecret:Z

.field public static sImpsClientIdPrefix:Z

.field public static sImpsContentType:Z

.field public static sImpsProviderId:Z

.field public static sImpsServerAddress:Z

.field public static sImpsServerName:Z

.field public static sImpsServices:Z

.field public static sImpsToNapid:Z

.field public static sImpsToProxy:Z

.field public static sMms:Z

.field public static sMmsCm:Z

.field public static sMmsMa:Z

.field public static sMmsMmsc:Z

.field public static sMmsMmscName:Z

.field public static sMmsMs:Z

.field public static sMmsPcAddr:Z

.field public static sMmsRm:Z

.field public static sMmsToNapid:Z

.field public static sMmsToProxy:Z

.field public static sRtsp:Z

.field public static sRtspMaxBandwidth:Z

.field public static sRtspMaxUdpPort:Z

.field public static sRtspMinUdpPort:Z

.field public static sRtspName:Z

.field public static sRtspNetInfo:Z

.field public static sRtspProviderId:Z

.field public static sRtspToNapid:Z

.field public static sRtspToProxy:Z

.field public static sSupl:Z

.field public static sSuplAddrType:Z

.field public static sSuplProviderId:Z

.field public static sSuplServerAddr:Z

.field public static sSuplServerName:Z

.field public static sSuplToNapid:Z


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const/4 v0, 0x0

    sput-boolean v0, Lcom/mediatek/omacp/message/OmacpApplicationCapability;->sBrowser:Z

    sput-boolean v0, Lcom/mediatek/omacp/message/OmacpApplicationCapability;->sBrowserBookMarkFolder:Z

    sput-boolean v0, Lcom/mediatek/omacp/message/OmacpApplicationCapability;->sBrowserToProxy:Z

    sput-boolean v0, Lcom/mediatek/omacp/message/OmacpApplicationCapability;->sBrowserToNapId:Z

    sput-boolean v0, Lcom/mediatek/omacp/message/OmacpApplicationCapability;->sBrowserBookMarkName:Z

    sput-boolean v0, Lcom/mediatek/omacp/message/OmacpApplicationCapability;->sBrowserBookMark:Z

    sput-boolean v0, Lcom/mediatek/omacp/message/OmacpApplicationCapability;->sBrowserUserName:Z

    sput-boolean v0, Lcom/mediatek/omacp/message/OmacpApplicationCapability;->sBrowserPassWord:Z

    sput-boolean v0, Lcom/mediatek/omacp/message/OmacpApplicationCapability;->sBrowserHomePage:Z

    sput-boolean v0, Lcom/mediatek/omacp/message/OmacpApplicationCapability;->sMms:Z

    sput-boolean v0, Lcom/mediatek/omacp/message/OmacpApplicationCapability;->sMmsMmscName:Z

    sput-boolean v0, Lcom/mediatek/omacp/message/OmacpApplicationCapability;->sMmsToProxy:Z

    sput-boolean v0, Lcom/mediatek/omacp/message/OmacpApplicationCapability;->sMmsToNapid:Z

    sput-boolean v0, Lcom/mediatek/omacp/message/OmacpApplicationCapability;->sMmsMmsc:Z

    sput-boolean v0, Lcom/mediatek/omacp/message/OmacpApplicationCapability;->sMmsCm:Z

    sput-boolean v0, Lcom/mediatek/omacp/message/OmacpApplicationCapability;->sMmsRm:Z

    sput-boolean v0, Lcom/mediatek/omacp/message/OmacpApplicationCapability;->sMmsMs:Z

    sput-boolean v0, Lcom/mediatek/omacp/message/OmacpApplicationCapability;->sMmsPcAddr:Z

    sput-boolean v0, Lcom/mediatek/omacp/message/OmacpApplicationCapability;->sMmsMa:Z

    sput-boolean v0, Lcom/mediatek/omacp/message/OmacpApplicationCapability;->sDm:Z

    sput-boolean v0, Lcom/mediatek/omacp/message/OmacpApplicationCapability;->sDmProviderId:Z

    sput-boolean v0, Lcom/mediatek/omacp/message/OmacpApplicationCapability;->sDmServerName:Z

    sput-boolean v0, Lcom/mediatek/omacp/message/OmacpApplicationCapability;->sDmToProxy:Z

    sput-boolean v0, Lcom/mediatek/omacp/message/OmacpApplicationCapability;->sDmToNapid:Z

    sput-boolean v0, Lcom/mediatek/omacp/message/OmacpApplicationCapability;->sDmServerAddress:Z

    sput-boolean v0, Lcom/mediatek/omacp/message/OmacpApplicationCapability;->sDmAddrType:Z

    sput-boolean v0, Lcom/mediatek/omacp/message/OmacpApplicationCapability;->sDmPortNumber:Z

    sput-boolean v0, Lcom/mediatek/omacp/message/OmacpApplicationCapability;->sDmAuthLevel:Z

    sput-boolean v0, Lcom/mediatek/omacp/message/OmacpApplicationCapability;->sDmAuthType:Z

    sput-boolean v0, Lcom/mediatek/omacp/message/OmacpApplicationCapability;->sDmAuthName:Z

    sput-boolean v0, Lcom/mediatek/omacp/message/OmacpApplicationCapability;->sDmAuthSecret:Z

    sput-boolean v0, Lcom/mediatek/omacp/message/OmacpApplicationCapability;->sDmAuthData:Z

    sput-boolean v0, Lcom/mediatek/omacp/message/OmacpApplicationCapability;->sDmInit:Z

    sput-boolean v0, Lcom/mediatek/omacp/message/OmacpApplicationCapability;->sEmail:Z

    sput-boolean v0, Lcom/mediatek/omacp/message/OmacpApplicationCapability;->sEmailProviderId:Z

    sput-boolean v0, Lcom/mediatek/omacp/message/OmacpApplicationCapability;->sEmailSettingName:Z

    sput-boolean v0, Lcom/mediatek/omacp/message/OmacpApplicationCapability;->sEmailToNapid:Z

    sput-boolean v0, Lcom/mediatek/omacp/message/OmacpApplicationCapability;->sEmailOutboundAddr:Z

    sput-boolean v0, Lcom/mediatek/omacp/message/OmacpApplicationCapability;->sEmailOutboundAddrType:Z

    sput-boolean v0, Lcom/mediatek/omacp/message/OmacpApplicationCapability;->sEmailOutboundPortNumber:Z

    sput-boolean v0, Lcom/mediatek/omacp/message/OmacpApplicationCapability;->sEmailOutboundSecure:Z

    sput-boolean v0, Lcom/mediatek/omacp/message/OmacpApplicationCapability;->sEmailOutboundAuthType:Z

    sput-boolean v0, Lcom/mediatek/omacp/message/OmacpApplicationCapability;->sEmailOutboundUserName:Z

    sput-boolean v0, Lcom/mediatek/omacp/message/OmacpApplicationCapability;->sEmailOutboundPassword:Z

    sput-boolean v0, Lcom/mediatek/omacp/message/OmacpApplicationCapability;->sEmailFrom:Z

    sput-boolean v0, Lcom/mediatek/omacp/message/OmacpApplicationCapability;->sEmailRtAddr:Z

    sput-boolean v0, Lcom/mediatek/omacp/message/OmacpApplicationCapability;->sEmailInboundAddr:Z

    sput-boolean v0, Lcom/mediatek/omacp/message/OmacpApplicationCapability;->sEmailInboundAddrType:Z

    sput-boolean v0, Lcom/mediatek/omacp/message/OmacpApplicationCapability;->sEmailInboundPortNumber:Z

    sput-boolean v0, Lcom/mediatek/omacp/message/OmacpApplicationCapability;->sEmailInboundSecure:Z

    sput-boolean v0, Lcom/mediatek/omacp/message/OmacpApplicationCapability;->sEmailInboundAuthType:Z

    sput-boolean v0, Lcom/mediatek/omacp/message/OmacpApplicationCapability;->sEmailInboundUserName:Z

    sput-boolean v0, Lcom/mediatek/omacp/message/OmacpApplicationCapability;->sEmailInboundPassword:Z

    sput-boolean v0, Lcom/mediatek/omacp/message/OmacpApplicationCapability;->sRtsp:Z

    sput-boolean v0, Lcom/mediatek/omacp/message/OmacpApplicationCapability;->sRtspProviderId:Z

    sput-boolean v0, Lcom/mediatek/omacp/message/OmacpApplicationCapability;->sRtspName:Z

    sput-boolean v0, Lcom/mediatek/omacp/message/OmacpApplicationCapability;->sRtspToProxy:Z

    sput-boolean v0, Lcom/mediatek/omacp/message/OmacpApplicationCapability;->sRtspToNapid:Z

    sput-boolean v0, Lcom/mediatek/omacp/message/OmacpApplicationCapability;->sRtspMaxBandwidth:Z

    sput-boolean v0, Lcom/mediatek/omacp/message/OmacpApplicationCapability;->sRtspNetInfo:Z

    sput-boolean v0, Lcom/mediatek/omacp/message/OmacpApplicationCapability;->sRtspMinUdpPort:Z

    sput-boolean v0, Lcom/mediatek/omacp/message/OmacpApplicationCapability;->sRtspMaxUdpPort:Z

    sput-boolean v0, Lcom/mediatek/omacp/message/OmacpApplicationCapability;->sSupl:Z

    sput-boolean v0, Lcom/mediatek/omacp/message/OmacpApplicationCapability;->sSuplProviderId:Z

    sput-boolean v0, Lcom/mediatek/omacp/message/OmacpApplicationCapability;->sSuplServerName:Z

    sput-boolean v0, Lcom/mediatek/omacp/message/OmacpApplicationCapability;->sSuplToNapid:Z

    sput-boolean v0, Lcom/mediatek/omacp/message/OmacpApplicationCapability;->sSuplServerAddr:Z

    sput-boolean v0, Lcom/mediatek/omacp/message/OmacpApplicationCapability;->sSuplAddrType:Z

    sput-boolean v0, Lcom/mediatek/omacp/message/OmacpApplicationCapability;->sDs:Z

    sput-boolean v0, Lcom/mediatek/omacp/message/OmacpApplicationCapability;->sDsServerName:Z

    sput-boolean v0, Lcom/mediatek/omacp/message/OmacpApplicationCapability;->sDsToProxy:Z

    sput-boolean v0, Lcom/mediatek/omacp/message/OmacpApplicationCapability;->sDsToNapid:Z

    sput-boolean v0, Lcom/mediatek/omacp/message/OmacpApplicationCapability;->sDsProviderId:Z

    sput-boolean v0, Lcom/mediatek/omacp/message/OmacpApplicationCapability;->sDsServerAddress:Z

    sput-boolean v0, Lcom/mediatek/omacp/message/OmacpApplicationCapability;->sDsAddressType:Z

    sput-boolean v0, Lcom/mediatek/omacp/message/OmacpApplicationCapability;->sDsPortNumber:Z

    sput-boolean v0, Lcom/mediatek/omacp/message/OmacpApplicationCapability;->sDsAuthLevel:Z

    sput-boolean v0, Lcom/mediatek/omacp/message/OmacpApplicationCapability;->sDsAuthType:Z

    sput-boolean v0, Lcom/mediatek/omacp/message/OmacpApplicationCapability;->sDsAuthName:Z

    sput-boolean v0, Lcom/mediatek/omacp/message/OmacpApplicationCapability;->sDsAuthSecret:Z

    sput-boolean v0, Lcom/mediatek/omacp/message/OmacpApplicationCapability;->sDsAuthData:Z

    sput-boolean v0, Lcom/mediatek/omacp/message/OmacpApplicationCapability;->sDsDatabaseContentType:Z

    sput-boolean v0, Lcom/mediatek/omacp/message/OmacpApplicationCapability;->sDsDatabaseUrl:Z

    sput-boolean v0, Lcom/mediatek/omacp/message/OmacpApplicationCapability;->sDsDatabaseName:Z

    sput-boolean v0, Lcom/mediatek/omacp/message/OmacpApplicationCapability;->sDsDatabaseAuthType:Z

    sput-boolean v0, Lcom/mediatek/omacp/message/OmacpApplicationCapability;->sDsDatabaseAuthName:Z

    sput-boolean v0, Lcom/mediatek/omacp/message/OmacpApplicationCapability;->sDsDatabaseAuthSecret:Z

    sput-boolean v0, Lcom/mediatek/omacp/message/OmacpApplicationCapability;->sDsClientDatabaseUrl:Z

    sput-boolean v0, Lcom/mediatek/omacp/message/OmacpApplicationCapability;->sDsSyncType:Z

    sput-boolean v0, Lcom/mediatek/omacp/message/OmacpApplicationCapability;->sImps:Z

    sput-boolean v0, Lcom/mediatek/omacp/message/OmacpApplicationCapability;->sImpsProviderId:Z

    sput-boolean v0, Lcom/mediatek/omacp/message/OmacpApplicationCapability;->sImpsServerName:Z

    sput-boolean v0, Lcom/mediatek/omacp/message/OmacpApplicationCapability;->sImpsContentType:Z

    sput-boolean v0, Lcom/mediatek/omacp/message/OmacpApplicationCapability;->sImpsServerAddress:Z

    sput-boolean v0, Lcom/mediatek/omacp/message/OmacpApplicationCapability;->sImpsAddressType:Z

    sput-boolean v0, Lcom/mediatek/omacp/message/OmacpApplicationCapability;->sImpsToProxy:Z

    sput-boolean v0, Lcom/mediatek/omacp/message/OmacpApplicationCapability;->sImpsToNapid:Z

    sput-boolean v0, Lcom/mediatek/omacp/message/OmacpApplicationCapability;->sImpsAuthLevel:Z

    sput-boolean v0, Lcom/mediatek/omacp/message/OmacpApplicationCapability;->sImpsAuthName:Z

    sput-boolean v0, Lcom/mediatek/omacp/message/OmacpApplicationCapability;->sImpsAuthSecret:Z

    sput-boolean v0, Lcom/mediatek/omacp/message/OmacpApplicationCapability;->sImpsServices:Z

    sput-boolean v0, Lcom/mediatek/omacp/message/OmacpApplicationCapability;->sImpsClientIdPrefix:Z

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
