.class Lcom/mediatek/datatransfer/AppRestoreActivity$AppRestoreStatusListener$2;
.super Ljava/lang/Object;
.source "AppRestoreActivity.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/mediatek/datatransfer/AppRestoreActivity$AppRestoreStatusListener;->onProgressChanged(Lcom/mediatek/datatransfer/modules/Composer;I)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$1:Lcom/mediatek/datatransfer/AppRestoreActivity$AppRestoreStatusListener;

.field final synthetic val$composer:Lcom/mediatek/datatransfer/modules/Composer;

.field final synthetic val$progress:I


# direct methods
.method constructor <init>(Lcom/mediatek/datatransfer/AppRestoreActivity$AppRestoreStatusListener;ILcom/mediatek/datatransfer/modules/Composer;)V
    .locals 0

    iput-object p1, p0, Lcom/mediatek/datatransfer/AppRestoreActivity$AppRestoreStatusListener$2;->this$1:Lcom/mediatek/datatransfer/AppRestoreActivity$AppRestoreStatusListener;

    iput p2, p0, Lcom/mediatek/datatransfer/AppRestoreActivity$AppRestoreStatusListener$2;->val$progress:I

    iput-object p3, p0, Lcom/mediatek/datatransfer/AppRestoreActivity$AppRestoreStatusListener$2;->val$composer:Lcom/mediatek/datatransfer/modules/Composer;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 6

    iget-object v3, p0, Lcom/mediatek/datatransfer/AppRestoreActivity$AppRestoreStatusListener$2;->this$1:Lcom/mediatek/datatransfer/AppRestoreActivity$AppRestoreStatusListener;

    iget-object v3, v3, Lcom/mediatek/datatransfer/AppRestoreActivity$AppRestoreStatusListener;->this$0:Lcom/mediatek/datatransfer/AppRestoreActivity;

    iget v4, p0, Lcom/mediatek/datatransfer/AppRestoreActivity$AppRestoreStatusListener$2;->val$progress:I

    invoke-virtual {v3, v4}, Lcom/mediatek/datatransfer/AbstractRestoreActivity;->setProgressDialogProgress(I)V

    iget v3, p0, Lcom/mediatek/datatransfer/AppRestoreActivity$AppRestoreStatusListener$2;->val$progress:I

    iget-object v4, p0, Lcom/mediatek/datatransfer/AppRestoreActivity$AppRestoreStatusListener$2;->val$composer:Lcom/mediatek/datatransfer/modules/Composer;

    invoke-virtual {v4}, Lcom/mediatek/datatransfer/modules/Composer;->getCount()I

    move-result v4

    if-ge v3, v4, :cond_0

    iget-object v3, p0, Lcom/mediatek/datatransfer/AppRestoreActivity$AppRestoreStatusListener$2;->this$1:Lcom/mediatek/datatransfer/AppRestoreActivity$AppRestoreStatusListener;

    iget-object v3, v3, Lcom/mediatek/datatransfer/AppRestoreActivity$AppRestoreStatusListener;->this$0:Lcom/mediatek/datatransfer/AppRestoreActivity;

    iget-object v3, v3, Lcom/mediatek/datatransfer/AbstractRestoreActivity;->mRestoreService:Lcom/mediatek/datatransfer/RestoreService$RestoreBinder;

    const/16 v4, 0x10

    invoke-virtual {v3, v4}, Lcom/mediatek/datatransfer/RestoreService$RestoreBinder;->getRestoreItemParam(I)Ljava/util/ArrayList;

    move-result-object v2

    iget v3, p0, Lcom/mediatek/datatransfer/AppRestoreActivity$AppRestoreStatusListener$2;->val$progress:I

    invoke-virtual {v2, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iget-object v3, p0, Lcom/mediatek/datatransfer/AppRestoreActivity$AppRestoreStatusListener$2;->this$1:Lcom/mediatek/datatransfer/AppRestoreActivity$AppRestoreStatusListener;

    iget-object v3, v3, Lcom/mediatek/datatransfer/AppRestoreActivity$AppRestoreStatusListener;->this$0:Lcom/mediatek/datatransfer/AppRestoreActivity;

    invoke-static {v3}, Lcom/mediatek/datatransfer/AppRestoreActivity;->access$800(Lcom/mediatek/datatransfer/AppRestoreActivity;)Ljava/lang/String;

    move-result-object v3

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "onProgressChanged: the "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget v5, p0, Lcom/mediatek/datatransfer/AppRestoreActivity$AppRestoreStatusListener$2;->val$progress:I

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "  apkName is "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/mediatek/datatransfer/utils/MyLogger;->logV(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v3, p0, Lcom/mediatek/datatransfer/AppRestoreActivity$AppRestoreStatusListener$2;->this$1:Lcom/mediatek/datatransfer/AppRestoreActivity$AppRestoreStatusListener;

    iget-object v3, v3, Lcom/mediatek/datatransfer/AppRestoreActivity$AppRestoreStatusListener;->this$0:Lcom/mediatek/datatransfer/AppRestoreActivity;

    iget-object v4, p0, Lcom/mediatek/datatransfer/AppRestoreActivity$AppRestoreStatusListener$2;->this$1:Lcom/mediatek/datatransfer/AppRestoreActivity$AppRestoreStatusListener;

    iget-object v4, v4, Lcom/mediatek/datatransfer/AppRestoreActivity$AppRestoreStatusListener;->this$0:Lcom/mediatek/datatransfer/AppRestoreActivity;

    invoke-static {v4, v0}, Lcom/mediatek/datatransfer/AppRestoreActivity;->access$1000(Lcom/mediatek/datatransfer/AppRestoreActivity;Ljava/lang/String;)Lcom/mediatek/datatransfer/AppSnippet;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/mediatek/datatransfer/AppRestoreActivity;->access$1100(Lcom/mediatek/datatransfer/AppRestoreActivity;Lcom/mediatek/datatransfer/AppSnippet;)Ljava/lang/String;

    move-result-object v1

    iget-object v3, p0, Lcom/mediatek/datatransfer/AppRestoreActivity$AppRestoreStatusListener$2;->this$1:Lcom/mediatek/datatransfer/AppRestoreActivity$AppRestoreStatusListener;

    iget-object v3, v3, Lcom/mediatek/datatransfer/AppRestoreActivity$AppRestoreStatusListener;->this$0:Lcom/mediatek/datatransfer/AppRestoreActivity;

    invoke-virtual {v3, v1}, Lcom/mediatek/datatransfer/AbstractRestoreActivity;->setProgressDialogMessage(Ljava/lang/CharSequence;)V

    :cond_0
    return-void
.end method
