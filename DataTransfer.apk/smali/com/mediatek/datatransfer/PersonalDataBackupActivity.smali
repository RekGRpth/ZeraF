.class public Lcom/mediatek/datatransfer/PersonalDataBackupActivity;
.super Lcom/mediatek/datatransfer/AbstractBackupActivity;
.source "PersonalDataBackupActivity.java"

# interfaces
.implements Lcom/mediatek/datatransfer/CheckedListActivity$OnCheckedCountChangedListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/mediatek/datatransfer/PersonalDataBackupActivity$PersonalDataBackupStatusListener;,
        Lcom/mediatek/datatransfer/PersonalDataBackupActivity$PersonalDataBackupAdapter;,
        Lcom/mediatek/datatransfer/PersonalDataBackupActivity$InitPersonalDataTask;
    }
.end annotation


# static fields
.field private static CLASS_TAG:Ljava/lang/String; = null

.field private static final DISABLE_ALPHA:F = 0.4f

.field private static final ENABLE_ALPHA:F = 1.0f


# instance fields
.field private CONTACT_TYPE:Ljava/lang/String;

.field private MESSAGE_TYPE:Ljava/lang/String;

.field private dialogs:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Landroid/app/AlertDialog;",
            ">;"
        }
    .end annotation
.end field

.field initDataTask:Lcom/mediatek/datatransfer/PersonalDataBackupActivity$InitPersonalDataTask;

.field private mBackupFolderPath:Ljava/lang/String;

.field private mBackupItemDataList:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/mediatek/datatransfer/PersonalItemData;",
            ">;"
        }
    .end annotation
.end field

.field private mBackupListAdapter:Lcom/mediatek/datatransfer/PersonalDataBackupActivity$PersonalDataBackupAdapter;

.field private mBackupListener:Lcom/mediatek/datatransfer/BackupService$OnBackupStatusListener;

.field private mContactCheckTypes:[Z

.field private mMessageCheckTypes:[Z

.field mSimCount:I

.field mSimInfoList:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/mediatek/telephony/SimInfoManager$SimInfoRecord;",
            ">;"
        }
    .end annotation
.end field

.field messageEnable:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const-string v0, "DataTransfer/PersonalDataBackupActivity"

    sput-object v0, Lcom/mediatek/datatransfer/PersonalDataBackupActivity;->CLASS_TAG:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Lcom/mediatek/datatransfer/AbstractBackupActivity;-><init>()V

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/mediatek/datatransfer/PersonalDataBackupActivity;->mBackupItemDataList:Ljava/util/ArrayList;

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/mediatek/datatransfer/PersonalDataBackupActivity;->dialogs:Ljava/util/ArrayList;

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/mediatek/datatransfer/PersonalDataBackupActivity;->initDataTask:Lcom/mediatek/datatransfer/PersonalDataBackupActivity$InitPersonalDataTask;

    const/16 v0, 0xa

    new-array v0, v0, [Z

    iput-object v0, p0, Lcom/mediatek/datatransfer/PersonalDataBackupActivity;->mContactCheckTypes:[Z

    const/4 v0, 0x2

    new-array v0, v0, [Z

    iput-object v0, p0, Lcom/mediatek/datatransfer/PersonalDataBackupActivity;->mMessageCheckTypes:[Z

    const-string v0, "contact"

    iput-object v0, p0, Lcom/mediatek/datatransfer/PersonalDataBackupActivity;->CONTACT_TYPE:Ljava/lang/String;

    const-string v0, "message"

    iput-object v0, p0, Lcom/mediatek/datatransfer/PersonalDataBackupActivity;->MESSAGE_TYPE:Ljava/lang/String;

    const/4 v0, 0x0

    iput v0, p0, Lcom/mediatek/datatransfer/PersonalDataBackupActivity;->mSimCount:I

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/mediatek/datatransfer/PersonalDataBackupActivity;->messageEnable:Ljava/util/List;

    return-void
.end method

.method static synthetic access$100(Lcom/mediatek/datatransfer/PersonalDataBackupActivity;Lcom/mediatek/telephony/SimInfoManager$SimInfoRecord;)Lcom/mediatek/telephony/SimInfoManager$SimInfoRecord;
    .locals 1
    .param p0    # Lcom/mediatek/datatransfer/PersonalDataBackupActivity;
    .param p1    # Lcom/mediatek/telephony/SimInfoManager$SimInfoRecord;

    invoke-direct {p0, p1}, Lcom/mediatek/datatransfer/PersonalDataBackupActivity;->getNextSimCard(Lcom/mediatek/telephony/SimInfoManager$SimInfoRecord;)Lcom/mediatek/telephony/SimInfoManager$SimInfoRecord;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$1000(Lcom/mediatek/datatransfer/PersonalDataBackupActivity;Ljava/lang/String;)V
    .locals 0
    .param p0    # Lcom/mediatek/datatransfer/PersonalDataBackupActivity;
    .param p1    # Ljava/lang/String;

    invoke-direct {p0, p1}, Lcom/mediatek/datatransfer/PersonalDataBackupActivity;->startPersonalDataBackup(Ljava/lang/String;)V

    return-void
.end method

.method static synthetic access$1100(Lcom/mediatek/datatransfer/PersonalDataBackupActivity;Ljava/util/ArrayList;)V
    .locals 0
    .param p0    # Lcom/mediatek/datatransfer/PersonalDataBackupActivity;
    .param p1    # Ljava/util/ArrayList;

    invoke-direct {p0, p1}, Lcom/mediatek/datatransfer/PersonalDataBackupActivity;->updateData(Ljava/util/ArrayList;)V

    return-void
.end method

.method static synthetic access$1200(Lcom/mediatek/datatransfer/PersonalDataBackupActivity;)Lcom/mediatek/datatransfer/BackupService$OnBackupStatusListener;
    .locals 1
    .param p0    # Lcom/mediatek/datatransfer/PersonalDataBackupActivity;

    iget-object v0, p0, Lcom/mediatek/datatransfer/PersonalDataBackupActivity;->mBackupListener:Lcom/mediatek/datatransfer/BackupService$OnBackupStatusListener;

    return-object v0
.end method

.method static synthetic access$1300(Lcom/mediatek/datatransfer/PersonalDataBackupActivity;[Lcom/mediatek/datatransfer/modules/Composer;)I
    .locals 1
    .param p0    # Lcom/mediatek/datatransfer/PersonalDataBackupActivity;
    .param p1    # [Lcom/mediatek/datatransfer/modules/Composer;

    invoke-direct {p0, p1}, Lcom/mediatek/datatransfer/PersonalDataBackupActivity;->getModulesCount([Lcom/mediatek/datatransfer/modules/Composer;)I

    move-result v0

    return v0
.end method

.method static synthetic access$1400(Lcom/mediatek/datatransfer/PersonalDataBackupActivity;)V
    .locals 0
    .param p0    # Lcom/mediatek/datatransfer/PersonalDataBackupActivity;

    invoke-direct {p0}, Lcom/mediatek/datatransfer/PersonalDataBackupActivity;->getSimInfoList()V

    return-void
.end method

.method static synthetic access$1500(Lcom/mediatek/datatransfer/PersonalDataBackupActivity;)V
    .locals 0
    .param p0    # Lcom/mediatek/datatransfer/PersonalDataBackupActivity;

    invoke-direct {p0}, Lcom/mediatek/datatransfer/PersonalDataBackupActivity;->showContactConfigDialog()V

    return-void
.end method

.method static synthetic access$1600(Lcom/mediatek/datatransfer/PersonalDataBackupActivity;)V
    .locals 0
    .param p0    # Lcom/mediatek/datatransfer/PersonalDataBackupActivity;

    invoke-direct {p0}, Lcom/mediatek/datatransfer/PersonalDataBackupActivity;->showMessageConfigDialog()V

    return-void
.end method

.method static synthetic access$1700(Lcom/mediatek/datatransfer/PersonalDataBackupActivity;Lcom/mediatek/datatransfer/BackupEngine$BackupResultType;Ljava/util/ArrayList;)V
    .locals 0
    .param p0    # Lcom/mediatek/datatransfer/PersonalDataBackupActivity;
    .param p1    # Lcom/mediatek/datatransfer/BackupEngine$BackupResultType;
    .param p2    # Ljava/util/ArrayList;

    invoke-direct {p0, p1, p2}, Lcom/mediatek/datatransfer/PersonalDataBackupActivity;->showBackupResult(Lcom/mediatek/datatransfer/BackupEngine$BackupResultType;Ljava/util/ArrayList;)V

    return-void
.end method

.method static synthetic access$200()Ljava/lang/String;
    .locals 1

    sget-object v0, Lcom/mediatek/datatransfer/PersonalDataBackupActivity;->CLASS_TAG:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$300(Lcom/mediatek/datatransfer/PersonalDataBackupActivity;Lcom/mediatek/telephony/SimInfoManager$SimInfoRecord;)Z
    .locals 1
    .param p0    # Lcom/mediatek/datatransfer/PersonalDataBackupActivity;
    .param p1    # Lcom/mediatek/telephony/SimInfoManager$SimInfoRecord;

    invoke-direct {p0, p1}, Lcom/mediatek/datatransfer/PersonalDataBackupActivity;->checkSimLocked(Lcom/mediatek/telephony/SimInfoManager$SimInfoRecord;)Z

    move-result v0

    return v0
.end method

.method static synthetic access$400(Lcom/mediatek/datatransfer/PersonalDataBackupActivity;I)V
    .locals 0
    .param p0    # Lcom/mediatek/datatransfer/PersonalDataBackupActivity;
    .param p1    # I

    invoke-direct {p0, p1}, Lcom/mediatek/datatransfer/PersonalDataBackupActivity;->startUnlockSim(I)V

    return-void
.end method

.method static synthetic access$500(Lcom/mediatek/datatransfer/PersonalDataBackupActivity;)[Z
    .locals 1
    .param p0    # Lcom/mediatek/datatransfer/PersonalDataBackupActivity;

    iget-object v0, p0, Lcom/mediatek/datatransfer/PersonalDataBackupActivity;->mMessageCheckTypes:[Z

    return-object v0
.end method

.method static synthetic access$600(Lcom/mediatek/datatransfer/PersonalDataBackupActivity;[ZIZ)Z
    .locals 1
    .param p0    # Lcom/mediatek/datatransfer/PersonalDataBackupActivity;
    .param p1    # [Z
    .param p2    # I
    .param p3    # Z

    invoke-direct {p0, p1, p2, p3}, Lcom/mediatek/datatransfer/PersonalDataBackupActivity;->isAllValued([ZIZ)Z

    move-result v0

    return v0
.end method

.method static synthetic access$700(Lcom/mediatek/datatransfer/PersonalDataBackupActivity;)[Z
    .locals 1
    .param p0    # Lcom/mediatek/datatransfer/PersonalDataBackupActivity;

    iget-object v0, p0, Lcom/mediatek/datatransfer/PersonalDataBackupActivity;->mContactCheckTypes:[Z

    return-object v0
.end method

.method static synthetic access$800(Lcom/mediatek/datatransfer/PersonalDataBackupActivity;)I
    .locals 1
    .param p0    # Lcom/mediatek/datatransfer/PersonalDataBackupActivity;

    invoke-direct {p0}, Lcom/mediatek/datatransfer/PersonalDataBackupActivity;->getContactTypeNumber()I

    move-result v0

    return v0
.end method

.method static synthetic access$900(Lcom/mediatek/datatransfer/PersonalDataBackupActivity;)Ljava/lang/String;
    .locals 1
    .param p0    # Lcom/mediatek/datatransfer/PersonalDataBackupActivity;

    iget-object v0, p0, Lcom/mediatek/datatransfer/PersonalDataBackupActivity;->mBackupFolderPath:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$902(Lcom/mediatek/datatransfer/PersonalDataBackupActivity;Ljava/lang/String;)Ljava/lang/String;
    .locals 0
    .param p0    # Lcom/mediatek/datatransfer/PersonalDataBackupActivity;
    .param p1    # Ljava/lang/String;

    iput-object p1, p0, Lcom/mediatek/datatransfer/PersonalDataBackupActivity;->mBackupFolderPath:Ljava/lang/String;

    return-object p1
.end method

.method private checkSimLocked(Lcom/mediatek/telephony/SimInfoManager$SimInfoRecord;)Z
    .locals 7
    .param p1    # Lcom/mediatek/telephony/SimInfoManager$SimInfoRecord;

    const/4 v3, 0x1

    if-eqz p1, :cond_0

    invoke-direct {p0, p1}, Lcom/mediatek/datatransfer/PersonalDataBackupActivity;->showSIMLockedDialog(Lcom/mediatek/telephony/SimInfoManager$SimInfoRecord;)V

    :goto_0
    return v3

    :cond_0
    iget-object v4, p0, Lcom/mediatek/datatransfer/PersonalDataBackupActivity;->mSimInfoList:Ljava/util/List;

    if-eqz v4, :cond_2

    sget-object v4, Lcom/mediatek/datatransfer/PersonalDataBackupActivity;->CLASS_TAG:Ljava/lang/String;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "[mSimInfoList]===>mSimInfoList size = "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget-object v6, p0, Lcom/mediatek/datatransfer/PersonalDataBackupActivity;->mSimInfoList:Ljava/util/List;

    invoke-interface {v6}, Ljava/util/List;->size()I

    move-result v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/mediatek/datatransfer/utils/MyLogger;->logI(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v4, p0, Lcom/mediatek/datatransfer/PersonalDataBackupActivity;->mSimInfoList:Ljava/util/List;

    invoke-interface {v4}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :cond_1
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_2

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/mediatek/telephony/SimInfoManager$SimInfoRecord;

    iget v1, v2, Lcom/mediatek/telephony/SimInfoManager$SimInfoRecord;->mSimSlotId:I

    invoke-direct {p0, v1}, Lcom/mediatek/datatransfer/PersonalDataBackupActivity;->isSIMLocked(I)Z

    move-result v4

    if-eqz v4, :cond_1

    iget v4, v2, Lcom/mediatek/telephony/SimInfoManager$SimInfoRecord;->mSimSlotId:I

    invoke-direct {p0, v4}, Lcom/mediatek/datatransfer/PersonalDataBackupActivity;->isSimCardSelected(I)Z

    move-result v4

    if-eqz v4, :cond_1

    sget-object v4, Lcom/mediatek/datatransfer/PersonalDataBackupActivity;->CLASS_TAG:Ljava/lang/String;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "[checkSimLocked]===> isSIMLocked "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/mediatek/datatransfer/utils/MyLogger;->logD(Ljava/lang/String;Ljava/lang/String;)V

    invoke-direct {p0, v2}, Lcom/mediatek/datatransfer/PersonalDataBackupActivity;->checkSimLocked(Lcom/mediatek/telephony/SimInfoManager$SimInfoRecord;)Z

    const/4 p1, 0x0

    goto :goto_0

    :cond_2
    const/4 v3, 0x0

    goto :goto_0
.end method

.method private createFolderEditorDialog()Landroid/app/AlertDialog;
    .locals 8

    const/4 v7, 0x0

    invoke-static {p0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v2

    const v4, 0x7f030003

    invoke-virtual {v2, v4, v7}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v3

    const v4, 0x7f08000b

    invoke-virtual {v3, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/EditText;

    new-instance v4, Landroid/app/AlertDialog$Builder;

    invoke-direct {v4, p0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    const v5, 0x7f060032

    invoke-virtual {v4, v5}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    move-result-object v4

    invoke-virtual {v4, v3}, Landroid/app/AlertDialog$Builder;->setView(Landroid/view/View;)Landroid/app/AlertDialog$Builder;

    move-result-object v4

    const v5, 0x104000a

    new-instance v6, Lcom/mediatek/datatransfer/PersonalDataBackupActivity$12;

    invoke-direct {v6, p0}, Lcom/mediatek/datatransfer/PersonalDataBackupActivity$12;-><init>(Lcom/mediatek/datatransfer/PersonalDataBackupActivity;)V

    invoke-virtual {v4, v5, v6}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v4

    const/high16 v5, 0x1040000

    invoke-virtual {v4, v5, v7}, Landroid/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v4

    invoke-virtual {v4}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v0

    new-instance v4, Lcom/mediatek/datatransfer/PersonalDataBackupActivity$13;

    invoke-direct {v4, p0, v0}, Lcom/mediatek/datatransfer/PersonalDataBackupActivity$13;-><init>(Lcom/mediatek/datatransfer/PersonalDataBackupActivity;Landroid/app/AlertDialog;)V

    invoke-virtual {v1, v4}, Landroid/widget/TextView;->addTextChangedListener(Landroid/text/TextWatcher;)V

    return-object v0
.end method

.method private getContactTypeNumber()I
    .locals 3

    iget v1, p0, Lcom/mediatek/datatransfer/PersonalDataBackupActivity;->mSimCount:I

    add-int/lit8 v1, v1, 0x1

    iget-object v2, p0, Lcom/mediatek/datatransfer/PersonalDataBackupActivity;->mContactCheckTypes:[Z

    array-length v2, v2

    if-ge v1, v2, :cond_0

    iget v1, p0, Lcom/mediatek/datatransfer/PersonalDataBackupActivity;->mSimCount:I

    add-int/lit8 v0, v1, 0x1

    :goto_0
    return v0

    :cond_0
    iget-object v1, p0, Lcom/mediatek/datatransfer/PersonalDataBackupActivity;->mContactCheckTypes:[Z

    array-length v0, v1

    goto :goto_0
.end method

.method private varargs getModulesCount([Lcom/mediatek/datatransfer/modules/Composer;)I
    .locals 6
    .param p1    # [Lcom/mediatek/datatransfer/modules/Composer;

    const/4 v2, 0x0

    move-object v0, p1

    array-length v4, v0

    const/4 v3, 0x0

    :goto_0
    if-ge v3, v4, :cond_1

    aget-object v1, v0, v3

    invoke-virtual {v1}, Lcom/mediatek/datatransfer/modules/Composer;->init()Z

    move-result v5

    if-eqz v5, :cond_0

    invoke-virtual {v1}, Lcom/mediatek/datatransfer/modules/Composer;->getCount()I

    move-result v5

    add-int/2addr v2, v5

    invoke-virtual {v1}, Lcom/mediatek/datatransfer/modules/Composer;->onEnd()V

    :cond_0
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    :cond_1
    return v2
.end method

.method private getNextSimCard(Lcom/mediatek/telephony/SimInfoManager$SimInfoRecord;)Lcom/mediatek/telephony/SimInfoManager$SimInfoRecord;
    .locals 6
    .param p1    # Lcom/mediatek/telephony/SimInfoManager$SimInfoRecord;

    const/4 v2, 0x0

    if-nez p1, :cond_1

    :cond_0
    :goto_0
    return-object v2

    :cond_1
    invoke-direct {p0, p1}, Lcom/mediatek/datatransfer/PersonalDataBackupActivity;->getSimIndex(Lcom/mediatek/telephony/SimInfoManager$SimInfoRecord;)I

    move-result v0

    sget-object v3, Lcom/mediatek/datatransfer/PersonalDataBackupActivity;->CLASS_TAG:Ljava/lang/String;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "[getNextSimCard] current SIM card index is "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/mediatek/datatransfer/utils/MyLogger;->logD(Ljava/lang/String;Ljava/lang/String;)V

    add-int/lit8 v3, v0, 0x1

    iget-object v4, p0, Lcom/mediatek/datatransfer/PersonalDataBackupActivity;->mSimInfoList:Ljava/util/List;

    invoke-interface {v4}, Ljava/util/List;->size()I

    move-result v4

    if-ge v3, v4, :cond_0

    iget-object v3, p0, Lcom/mediatek/datatransfer/PersonalDataBackupActivity;->mSimInfoList:Ljava/util/List;

    add-int/lit8 v4, v0, 0x1

    invoke-interface {v3, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/mediatek/telephony/SimInfoManager$SimInfoRecord;

    sget-object v3, Lcom/mediatek/datatransfer/PersonalDataBackupActivity;->CLASS_TAG:Ljava/lang/String;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "[getNextSimCard] current SIM card is "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-object v5, p1, Lcom/mediatek/telephony/SimInfoManager$SimInfoRecord;->mDisplayName:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " Next SIM card is "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-object v5, v1, Lcom/mediatek/telephony/SimInfoManager$SimInfoRecord;->mDisplayName:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/mediatek/datatransfer/utils/MyLogger;->logD(Ljava/lang/String;Ljava/lang/String;)V

    iget v3, v1, Lcom/mediatek/telephony/SimInfoManager$SimInfoRecord;->mSimSlotId:I

    invoke-direct {p0, v3}, Lcom/mediatek/datatransfer/PersonalDataBackupActivity;->isSimCardSelected(I)Z

    move-result v3

    if-eqz v3, :cond_2

    iget v3, v1, Lcom/mediatek/telephony/SimInfoManager$SimInfoRecord;->mSimSlotId:I

    invoke-direct {p0, v3}, Lcom/mediatek/datatransfer/PersonalDataBackupActivity;->isSIMLocked(I)Z

    move-result v3

    if-eqz v3, :cond_2

    :goto_1
    move-object v2, v1

    goto :goto_0

    :cond_2
    move-object v1, v2

    goto :goto_1
.end method

.method private getSelectedItemList()Ljava/util/ArrayList;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation

    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    invoke-virtual {p0}, Lcom/mediatek/datatransfer/CheckedListActivity;->getCount()I

    move-result v0

    const/4 v3, 0x0

    :goto_0
    if-ge v3, v0, :cond_1

    invoke-virtual {p0, v3}, Lcom/mediatek/datatransfer/CheckedListActivity;->getItemByPosition(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/mediatek/datatransfer/PersonalItemData;

    invoke-virtual {p0, v3}, Lcom/mediatek/datatransfer/CheckedListActivity;->isItemCheckedByPosition(I)Z

    move-result v4

    if-eqz v4, :cond_0

    invoke-virtual {v1}, Lcom/mediatek/datatransfer/PersonalItemData;->getType()I

    move-result v4

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v2, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :cond_0
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    :cond_1
    return-object v2
.end method

.method private getSimIndex(Lcom/mediatek/telephony/SimInfoManager$SimInfoRecord;)I
    .locals 7
    .param p1    # Lcom/mediatek/telephony/SimInfoManager$SimInfoRecord;

    const/4 v1, -0x1

    iget-object v3, p0, Lcom/mediatek/datatransfer/PersonalDataBackupActivity;->mSimInfoList:Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :cond_0
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/mediatek/telephony/SimInfoManager$SimInfoRecord;

    iget-wide v3, v2, Lcom/mediatek/telephony/SimInfoManager$SimInfoRecord;->mSimInfoId:J

    iget-wide v5, p1, Lcom/mediatek/telephony/SimInfoManager$SimInfoRecord;->mSimInfoId:J

    cmp-long v3, v3, v5

    if-nez v3, :cond_0

    iget-object v3, p0, Lcom/mediatek/datatransfer/PersonalDataBackupActivity;->mSimInfoList:Ljava/util/List;

    invoke-interface {v3, v2}, Ljava/util/List;->indexOf(Ljava/lang/Object;)I

    move-result v1

    goto :goto_0

    :cond_1
    return v1
.end method

.method private getSimInfoList()V
    .locals 6

    invoke-static {p0}, Lcom/mediatek/telephony/SimInfoManager;->getInsertedSimInfoList(Landroid/content/Context;)Ljava/util/List;

    move-result-object v2

    iput-object v2, p0, Lcom/mediatek/datatransfer/PersonalDataBackupActivity;->mSimInfoList:Ljava/util/List;

    iget-object v2, p0, Lcom/mediatek/datatransfer/PersonalDataBackupActivity;->mSimInfoList:Ljava/util/List;

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/mediatek/datatransfer/PersonalDataBackupActivity;->mSimInfoList:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/mediatek/telephony/SimInfoManager$SimInfoRecord;

    sget-object v2, Lcom/mediatek/datatransfer/PersonalDataBackupActivity;->CLASS_TAG:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "sim id  = "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-wide v4, v1, Lcom/mediatek/telephony/SimInfoManager$SimInfoRecord;->mSimInfoId:J

    invoke-virtual {v3, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ", name = "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, v1, Lcom/mediatek/telephony/SimInfoManager$SimInfoRecord;->mDisplayName:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ", slot = "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget v4, v1, Lcom/mediatek/telephony/SimInfoManager$SimInfoRecord;->mSimSlotId:I

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/mediatek/datatransfer/utils/MyLogger;->logD(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    :cond_0
    sget-object v2, Lcom/mediatek/datatransfer/PersonalDataBackupActivity;->CLASS_TAG:Ljava/lang/String;

    const-string v3, "No SIM inserted!"

    invoke-static {v2, v3}, Lcom/mediatek/datatransfer/utils/MyLogger;->logD(Ljava/lang/String;Ljava/lang/String;)V

    :cond_1
    iget-object v2, p0, Lcom/mediatek/datatransfer/PersonalDataBackupActivity;->mSimInfoList:Ljava/util/List;

    if-eqz v2, :cond_2

    iget-object v2, p0, Lcom/mediatek/datatransfer/PersonalDataBackupActivity;->mSimInfoList:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->isEmpty()Z

    move-result v2

    if-eqz v2, :cond_3

    const/4 v2, 0x0

    :goto_1
    iput v2, p0, Lcom/mediatek/datatransfer/PersonalDataBackupActivity;->mSimCount:I

    :cond_2
    return-void

    :cond_3
    iget-object v2, p0, Lcom/mediatek/datatransfer/PersonalDataBackupActivity;->mSimInfoList:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    goto :goto_1
.end method

.method private init()V
    .locals 1

    invoke-direct {p0}, Lcom/mediatek/datatransfer/PersonalDataBackupActivity;->initActionBar()V

    invoke-virtual {p0}, Lcom/mediatek/datatransfer/PersonalDataBackupActivity;->updateTitle()V

    iget-object v0, p0, Lcom/mediatek/datatransfer/PersonalDataBackupActivity;->dialogs:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    return-void
.end method

.method private initActionBar()V
    .locals 2

    invoke-virtual {p0}, Landroid/app/Activity;->getActionBar()Landroid/app/ActionBar;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/app/ActionBar;->setDisplayShowHomeEnabled(Z)V

    return-void
.end method

.method private isAirModeOn(Landroid/content/Context;)Z
    .locals 4
    .param p1    # Landroid/content/Context;

    const/4 v1, 0x1

    const/4 v0, 0x0

    :try_start_0
    invoke-virtual {p0}, Landroid/content/ContextWrapper;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    const-string v3, "airplane_mode_on"

    invoke-static {v2, v3}, Landroid/provider/Settings$Global;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;)I
    :try_end_0
    .catch Landroid/provider/Settings$SettingNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v0

    :goto_0
    if-ne v0, v1, :cond_0

    :goto_1
    return v1

    :cond_0
    const/4 v1, 0x0

    goto :goto_1

    :catch_0
    move-exception v2

    goto :goto_0
.end method

.method private isAllValued([ZIZ)Z
    .locals 3
    .param p1    # [Z
    .param p2    # I
    .param p3    # Z

    const/4 v1, 0x1

    const/4 v0, 0x0

    :goto_0
    if-ge v0, p2, :cond_0

    aget-boolean v2, p1, v0

    if-eq v2, p3, :cond_1

    const/4 v1, 0x0

    :cond_0
    return v1

    :cond_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method

.method private isSIMLocked(I)Z
    .locals 5
    .param p1    # I

    invoke-static {}, Lcom/mediatek/telephony/TelephonyManagerEx;->getDefault()Lcom/mediatek/telephony/TelephonyManagerEx;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/mediatek/telephony/TelephonyManagerEx;->getSimState(I)I

    move-result v1

    sget-object v2, Lcom/mediatek/datatransfer/PersonalDataBackupActivity;->CLASS_TAG:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "SIM card slot "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " It\'s statue = "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/mediatek/datatransfer/utils/MyLogger;->logD(Ljava/lang/String;Ljava/lang/String;)V

    const/4 v2, 0x2

    if-ne v1, v2, :cond_0

    const/4 v2, 0x1

    :goto_0
    return v2

    :cond_0
    const/4 v2, 0x0

    goto :goto_0
.end method

.method private isSimCardSelected()Z
    .locals 3

    const/4 v1, 0x1

    invoke-direct {p0}, Lcom/mediatek/datatransfer/PersonalDataBackupActivity;->getSimInfoList()V

    iget v2, p0, Lcom/mediatek/datatransfer/PersonalDataBackupActivity;->mSimCount:I

    if-lt v2, v1, :cond_1

    const/4 v0, 0x1

    :goto_0
    iget v2, p0, Lcom/mediatek/datatransfer/PersonalDataBackupActivity;->mSimCount:I

    if-gt v0, v2, :cond_1

    iget-object v2, p0, Lcom/mediatek/datatransfer/PersonalDataBackupActivity;->mContactCheckTypes:[Z

    aget-boolean v2, v2, v0

    if-eqz v2, :cond_0

    :goto_1
    return v1

    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_1
    const/4 v1, 0x0

    goto :goto_1
.end method

.method private isSimCardSelected(I)Z
    .locals 6
    .param p1    # I

    sget-object v3, Lcom/mediatek/datatransfer/PersonalDataBackupActivity;->CLASS_TAG:Ljava/lang/String;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "[isSimCardSelected] slot = "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/mediatek/datatransfer/utils/MyLogger;->logD(Ljava/lang/String;Ljava/lang/String;)V

    invoke-direct {p0}, Lcom/mediatek/datatransfer/PersonalDataBackupActivity;->getSimInfoList()V

    iget v3, p0, Lcom/mediatek/datatransfer/PersonalDataBackupActivity;->mSimCount:I

    const/4 v4, 0x1

    if-lt v3, v4, :cond_2

    iget-object v3, p0, Lcom/mediatek/datatransfer/PersonalDataBackupActivity;->mSimInfoList:Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :cond_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_2

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/mediatek/telephony/SimInfoManager$SimInfoRecord;

    iget v3, v2, Lcom/mediatek/telephony/SimInfoManager$SimInfoRecord;->mSimSlotId:I

    if-ne v3, p1, :cond_0

    iget-object v3, p0, Lcom/mediatek/datatransfer/PersonalDataBackupActivity;->mSimInfoList:Ljava/util/List;

    invoke-interface {v3, v2}, Ljava/util/List;->indexOf(Ljava/lang/Object;)I

    move-result v3

    add-int/lit8 v1, v3, 0x1

    if-lez v1, :cond_0

    iget-object v3, p0, Lcom/mediatek/datatransfer/PersonalDataBackupActivity;->mContactCheckTypes:[Z

    array-length v3, v3

    if-ge v1, v3, :cond_0

    sget-object v4, Lcom/mediatek/datatransfer/PersonalDataBackupActivity;->CLASS_TAG:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "[isSimCardSelected] slot = "

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    iget-object v3, p0, Lcom/mediatek/datatransfer/PersonalDataBackupActivity;->mContactCheckTypes:[Z

    aget-boolean v3, v3, v1

    if-eqz v3, :cond_1

    const-string v3, " selected"

    :goto_0
    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v4, v3}, Lcom/mediatek/datatransfer/utils/MyLogger;->logD(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v3, p0, Lcom/mediatek/datatransfer/PersonalDataBackupActivity;->mContactCheckTypes:[Z

    aget-boolean v3, v3, v1

    :goto_1
    return v3

    :cond_1
    const-string v3, "not been selected"

    goto :goto_0

    :cond_2
    const/4 v3, 0x0

    goto :goto_1
.end method

.method private showAirModeOnDialog()V
    .locals 4

    new-instance v1, Landroid/app/AlertDialog$Builder;

    invoke-direct {v1, p0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    const v2, 0x7f060041

    invoke-virtual {v1, v2}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Landroid/app/AlertDialog$Builder;->setCancelable(Z)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    const v2, 0x7f060042

    invoke-virtual {v1, v2}, Landroid/app/AlertDialog$Builder;->setMessage(I)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    const v2, 0x104000a

    new-instance v3, Lcom/mediatek/datatransfer/PersonalDataBackupActivity$4;

    invoke-direct {v3, p0}, Lcom/mediatek/datatransfer/PersonalDataBackupActivity$4;-><init>(Lcom/mediatek/datatransfer/PersonalDataBackupActivity;)V

    invoke-virtual {v1, v2, v3}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    const/high16 v2, 0x1040000

    new-instance v3, Lcom/mediatek/datatransfer/PersonalDataBackupActivity$3;

    invoke-direct {v3, p0}, Lcom/mediatek/datatransfer/PersonalDataBackupActivity$3;-><init>(Lcom/mediatek/datatransfer/PersonalDataBackupActivity;)V

    invoke-virtual {v1, v2, v3}, Landroid/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    invoke-virtual {v1}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/Dialog;->show()V

    iget-object v1, p0, Lcom/mediatek/datatransfer/PersonalDataBackupActivity;->dialogs:Ljava/util/ArrayList;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    return-void
.end method

.method private showBackupResult(Lcom/mediatek/datatransfer/BackupEngine$BackupResultType;Ljava/util/ArrayList;)V
    .locals 2
    .param p1    # Lcom/mediatek/datatransfer/BackupEngine$BackupResultType;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/mediatek/datatransfer/BackupEngine$BackupResultType;",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/mediatek/datatransfer/ResultDialog$ResultEntity;",
            ">;)V"
        }
    .end annotation

    iget-object v1, p0, Lcom/mediatek/datatransfer/AbstractBackupActivity;->mProgressDialog:Landroid/app/ProgressDialog;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/mediatek/datatransfer/AbstractBackupActivity;->mProgressDialog:Landroid/app/ProgressDialog;

    invoke-virtual {v1}, Landroid/app/Dialog;->isShowing()Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/mediatek/datatransfer/AbstractBackupActivity;->mProgressDialog:Landroid/app/ProgressDialog;

    invoke-virtual {v1}, Landroid/app/Dialog;->dismiss()V

    :cond_0
    iget-object v1, p0, Lcom/mediatek/datatransfer/AbstractBackupActivity;->mCancelDlg:Landroid/app/ProgressDialog;

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/mediatek/datatransfer/AbstractBackupActivity;->mCancelDlg:Landroid/app/ProgressDialog;

    invoke-virtual {v1}, Landroid/app/Dialog;->isShowing()Z

    move-result v1

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/mediatek/datatransfer/AbstractBackupActivity;->mCancelDlg:Landroid/app/ProgressDialog;

    invoke-virtual {v1}, Landroid/app/Dialog;->dismiss()V

    :cond_1
    sget-object v1, Lcom/mediatek/datatransfer/BackupEngine$BackupResultType;->Cancel:Lcom/mediatek/datatransfer/BackupEngine$BackupResultType;

    if-eq p1, v1, :cond_2

    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    const-string v1, "result"

    invoke-virtual {v0, v1, p2}, Landroid/os/Bundle;->putParcelableArrayList(Ljava/lang/String;Ljava/util/ArrayList;)V

    const/16 v1, 0x7d4

    invoke-virtual {p0, v1, v0}, Landroid/app/Activity;->showDialog(ILandroid/os/Bundle;)Z

    :goto_0
    return-void

    :cond_2
    invoke-virtual {p0}, Lcom/mediatek/datatransfer/AbstractBackupActivity;->stopService()V

    goto :goto_0
.end method

.method private showContactConfigDialog()V
    .locals 10

    const/4 v9, 0x3

    const v8, 0x7f06002e

    const/4 v7, 0x2

    const/4 v6, 0x1

    const/4 v5, 0x0

    iget-object v4, p0, Lcom/mediatek/datatransfer/PersonalDataBackupActivity;->mContactCheckTypes:[Z

    array-length v4, v4

    new-array v3, v4, [Z

    const/4 v1, 0x0

    :goto_0
    iget-object v4, p0, Lcom/mediatek/datatransfer/PersonalDataBackupActivity;->mContactCheckTypes:[Z

    array-length v4, v4

    if-ge v1, v4, :cond_0

    iget-object v4, p0, Lcom/mediatek/datatransfer/PersonalDataBackupActivity;->mContactCheckTypes:[Z

    aget-boolean v4, v4, v1

    aput-boolean v4, v3, v1

    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_0
    iget v4, p0, Lcom/mediatek/datatransfer/PersonalDataBackupActivity;->mSimCount:I

    packed-switch v4, :pswitch_data_0

    new-array v2, v6, [Ljava/lang/String;

    invoke-virtual {p0, v8}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v4

    aput-object v4, v2, v5

    :goto_1
    new-instance v4, Landroid/app/AlertDialog$Builder;

    invoke-direct {v4, p0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    const v5, 0x7f060006

    invoke-virtual {v4, v5}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    move-result-object v4

    invoke-virtual {v4, v6}, Landroid/app/AlertDialog$Builder;->setCancelable(Z)Landroid/app/AlertDialog$Builder;

    move-result-object v4

    new-instance v5, Lcom/mediatek/datatransfer/PersonalDataBackupActivity$10;

    invoke-direct {v5, p0, v3}, Lcom/mediatek/datatransfer/PersonalDataBackupActivity$10;-><init>(Lcom/mediatek/datatransfer/PersonalDataBackupActivity;[Z)V

    invoke-virtual {v4, v2, v3, v5}, Landroid/app/AlertDialog$Builder;->setMultiChoiceItems([Ljava/lang/CharSequence;[ZLandroid/content/DialogInterface$OnMultiChoiceClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v4

    const v5, 0x104000a

    new-instance v6, Lcom/mediatek/datatransfer/PersonalDataBackupActivity$9;

    invoke-direct {v6, p0, v3}, Lcom/mediatek/datatransfer/PersonalDataBackupActivity$9;-><init>(Lcom/mediatek/datatransfer/PersonalDataBackupActivity;[Z)V

    invoke-virtual {v4, v5, v6}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v4

    const/high16 v5, 0x1040000

    const/4 v6, 0x0

    invoke-virtual {v4, v5, v6}, Landroid/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v4

    invoke-virtual {v4}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/Dialog;->show()V

    return-void

    :pswitch_0
    new-array v2, v7, [Ljava/lang/String;

    invoke-virtual {p0, v8}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v4

    aput-object v4, v2, v5

    iget-object v4, p0, Lcom/mediatek/datatransfer/PersonalDataBackupActivity;->mSimInfoList:Ljava/util/List;

    invoke-interface {v4, v5}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/mediatek/telephony/SimInfoManager$SimInfoRecord;

    iget-object v4, v4, Lcom/mediatek/telephony/SimInfoManager$SimInfoRecord;->mDisplayName:Ljava/lang/String;

    aput-object v4, v2, v6

    goto :goto_1

    :pswitch_1
    new-array v2, v9, [Ljava/lang/String;

    invoke-virtual {p0, v8}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v4

    aput-object v4, v2, v5

    iget-object v4, p0, Lcom/mediatek/datatransfer/PersonalDataBackupActivity;->mSimInfoList:Ljava/util/List;

    invoke-interface {v4, v5}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/mediatek/telephony/SimInfoManager$SimInfoRecord;

    iget-object v4, v4, Lcom/mediatek/telephony/SimInfoManager$SimInfoRecord;->mDisplayName:Ljava/lang/String;

    aput-object v4, v2, v6

    iget-object v4, p0, Lcom/mediatek/datatransfer/PersonalDataBackupActivity;->mSimInfoList:Ljava/util/List;

    invoke-interface {v4, v6}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/mediatek/telephony/SimInfoManager$SimInfoRecord;

    iget-object v4, v4, Lcom/mediatek/telephony/SimInfoManager$SimInfoRecord;->mDisplayName:Ljava/lang/String;

    aput-object v4, v2, v7

    goto :goto_1

    :pswitch_2
    const/4 v4, 0x4

    new-array v2, v4, [Ljava/lang/String;

    invoke-virtual {p0, v8}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v4

    aput-object v4, v2, v5

    iget-object v4, p0, Lcom/mediatek/datatransfer/PersonalDataBackupActivity;->mSimInfoList:Ljava/util/List;

    invoke-interface {v4, v5}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/mediatek/telephony/SimInfoManager$SimInfoRecord;

    iget-object v4, v4, Lcom/mediatek/telephony/SimInfoManager$SimInfoRecord;->mDisplayName:Ljava/lang/String;

    aput-object v4, v2, v6

    iget-object v4, p0, Lcom/mediatek/datatransfer/PersonalDataBackupActivity;->mSimInfoList:Ljava/util/List;

    invoke-interface {v4, v6}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/mediatek/telephony/SimInfoManager$SimInfoRecord;

    iget-object v4, v4, Lcom/mediatek/telephony/SimInfoManager$SimInfoRecord;->mDisplayName:Ljava/lang/String;

    aput-object v4, v2, v7

    iget-object v4, p0, Lcom/mediatek/datatransfer/PersonalDataBackupActivity;->mSimInfoList:Ljava/util/List;

    invoke-interface {v4, v7}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/mediatek/telephony/SimInfoManager$SimInfoRecord;

    iget-object v4, v4, Lcom/mediatek/telephony/SimInfoManager$SimInfoRecord;->mDisplayName:Ljava/lang/String;

    aput-object v4, v2, v9

    goto/16 :goto_1

    :pswitch_3
    const/4 v4, 0x5

    new-array v2, v4, [Ljava/lang/String;

    invoke-virtual {p0, v8}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v4

    aput-object v4, v2, v5

    iget-object v4, p0, Lcom/mediatek/datatransfer/PersonalDataBackupActivity;->mSimInfoList:Ljava/util/List;

    invoke-interface {v4, v5}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/mediatek/telephony/SimInfoManager$SimInfoRecord;

    iget-object v4, v4, Lcom/mediatek/telephony/SimInfoManager$SimInfoRecord;->mDisplayName:Ljava/lang/String;

    aput-object v4, v2, v6

    iget-object v4, p0, Lcom/mediatek/datatransfer/PersonalDataBackupActivity;->mSimInfoList:Ljava/util/List;

    invoke-interface {v4, v6}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/mediatek/telephony/SimInfoManager$SimInfoRecord;

    iget-object v4, v4, Lcom/mediatek/telephony/SimInfoManager$SimInfoRecord;->mDisplayName:Ljava/lang/String;

    aput-object v4, v2, v7

    iget-object v4, p0, Lcom/mediatek/datatransfer/PersonalDataBackupActivity;->mSimInfoList:Ljava/util/List;

    invoke-interface {v4, v7}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/mediatek/telephony/SimInfoManager$SimInfoRecord;

    iget-object v4, v4, Lcom/mediatek/telephony/SimInfoManager$SimInfoRecord;->mDisplayName:Ljava/lang/String;

    aput-object v4, v2, v9

    const/4 v5, 0x4

    iget-object v4, p0, Lcom/mediatek/datatransfer/PersonalDataBackupActivity;->mSimInfoList:Ljava/util/List;

    invoke-interface {v4, v9}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/mediatek/telephony/SimInfoManager$SimInfoRecord;

    iget-object v4, v4, Lcom/mediatek/telephony/SimInfoManager$SimInfoRecord;->mDisplayName:Ljava/lang/String;

    aput-object v4, v2, v5

    goto/16 :goto_1

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method private showMessageConfigDialog()V
    .locals 10

    const/4 v9, 0x2

    iget-object v6, p0, Lcom/mediatek/datatransfer/PersonalDataBackupActivity;->messageEnable:Ljava/util/List;

    invoke-interface {v6}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v6

    if-eqz v6, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/String;

    sget-object v6, Lcom/mediatek/datatransfer/PersonalDataBackupActivity;->CLASS_TAG:Ljava/lang/String;

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "messageEnable = "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Lcom/mediatek/datatransfer/utils/MyLogger;->logE(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    :cond_0
    iget-object v6, p0, Lcom/mediatek/datatransfer/PersonalDataBackupActivity;->messageEnable:Ljava/util/List;

    iget-object v7, p0, Lcom/mediatek/datatransfer/PersonalDataBackupActivity;->messageEnable:Ljava/util/List;

    invoke-interface {v7}, Ljava/util/List;->size()I

    move-result v7

    new-array v7, v7, [Ljava/lang/String;

    invoke-interface {v6, v7}, Ljava/util/List;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v6

    check-cast v6, [Ljava/lang/String;

    move-object v3, v6

    check-cast v3, [Ljava/lang/String;

    new-array v5, v9, [Z

    const/4 v2, 0x0

    :goto_1
    if-ge v2, v9, :cond_1

    iget-object v6, p0, Lcom/mediatek/datatransfer/PersonalDataBackupActivity;->mMessageCheckTypes:[Z

    aget-boolean v6, v6, v2

    aput-boolean v6, v5, v2

    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    :cond_1
    new-instance v6, Landroid/app/AlertDialog$Builder;

    invoke-direct {v6, p0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    const v7, 0x7f060007

    invoke-virtual {v6, v7}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    move-result-object v6

    const/4 v7, 0x1

    invoke-virtual {v6, v7}, Landroid/app/AlertDialog$Builder;->setCancelable(Z)Landroid/app/AlertDialog$Builder;

    move-result-object v6

    new-instance v7, Lcom/mediatek/datatransfer/PersonalDataBackupActivity$8;

    invoke-direct {v7, p0, v3, v5}, Lcom/mediatek/datatransfer/PersonalDataBackupActivity$8;-><init>(Lcom/mediatek/datatransfer/PersonalDataBackupActivity;[Ljava/lang/String;[Z)V

    invoke-virtual {v6, v3, v5, v7}, Landroid/app/AlertDialog$Builder;->setMultiChoiceItems([Ljava/lang/CharSequence;[ZLandroid/content/DialogInterface$OnMultiChoiceClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v6

    const v7, 0x104000a

    new-instance v8, Lcom/mediatek/datatransfer/PersonalDataBackupActivity$7;

    invoke-direct {v8, p0, v5}, Lcom/mediatek/datatransfer/PersonalDataBackupActivity$7;-><init>(Lcom/mediatek/datatransfer/PersonalDataBackupActivity;[Z)V

    invoke-virtual {v6, v7, v8}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v6

    const/high16 v7, 0x1040000

    const/4 v8, 0x0

    invoke-virtual {v6, v7, v8}, Landroid/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v6

    invoke-virtual {v6}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/Dialog;->show()V

    return-void
.end method

.method private showSIMLockedDialog(Lcom/mediatek/telephony/SimInfoManager$SimInfoRecord;)V
    .locals 4
    .param p1    # Lcom/mediatek/telephony/SimInfoManager$SimInfoRecord;

    new-instance v1, Landroid/app/AlertDialog$Builder;

    invoke-direct {v1, p0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    const v2, 0x7f060043

    invoke-virtual {v1, v2}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Landroid/app/AlertDialog$Builder;->setCancelable(Z)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    const v2, 0x7f060044

    invoke-virtual {v1, v2}, Landroid/app/AlertDialog$Builder;->setMessage(I)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    const v2, 0x7f060045

    new-instance v3, Lcom/mediatek/datatransfer/PersonalDataBackupActivity$6;

    invoke-direct {v3, p0, p1}, Lcom/mediatek/datatransfer/PersonalDataBackupActivity$6;-><init>(Lcom/mediatek/datatransfer/PersonalDataBackupActivity;Lcom/mediatek/telephony/SimInfoManager$SimInfoRecord;)V

    invoke-virtual {v1, v2, v3}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    const/high16 v2, 0x1040000

    new-instance v3, Lcom/mediatek/datatransfer/PersonalDataBackupActivity$5;

    invoke-direct {v3, p0, p1}, Lcom/mediatek/datatransfer/PersonalDataBackupActivity$5;-><init>(Lcom/mediatek/datatransfer/PersonalDataBackupActivity;Lcom/mediatek/telephony/SimInfoManager$SimInfoRecord;)V

    invoke-virtual {v1, v2, v3}, Landroid/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    invoke-virtual {v1}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/Dialog;->show()V

    iget-object v1, p0, Lcom/mediatek/datatransfer/PersonalDataBackupActivity;->dialogs:Ljava/util/ArrayList;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    return-void
.end method

.method private startPersonalDataBackup(Ljava/lang/String;)V
    .locals 14
    .param p1    # Ljava/lang/String;

    const/16 v13, 0x40

    const/4 v12, 0x0

    const/4 v11, 0x1

    if-eqz p1, :cond_0

    invoke-virtual {p1}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v7

    const-string v8, ""

    invoke-virtual {v7, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    invoke-virtual {p0}, Lcom/mediatek/datatransfer/AbstractBackupActivity;->startService()V

    iget-object v7, p0, Lcom/mediatek/datatransfer/AbstractBackupActivity;->mBackupService:Lcom/mediatek/datatransfer/BackupService$BackupBinder;

    if-eqz v7, :cond_c

    invoke-direct {p0}, Lcom/mediatek/datatransfer/PersonalDataBackupActivity;->getSelectedItemList()Ljava/util/ArrayList;

    move-result-object v3

    iget-object v7, p0, Lcom/mediatek/datatransfer/AbstractBackupActivity;->mBackupService:Lcom/mediatek/datatransfer/BackupService$BackupBinder;

    invoke-virtual {v7, v3}, Lcom/mediatek/datatransfer/BackupService$BackupBinder;->setBackupModelList(Ljava/util/ArrayList;)V

    invoke-static {v11}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    invoke-virtual {v3, v7}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_5

    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    const/4 v7, 0x6

    new-array v1, v7, [Ljava/lang/String;

    const-string v7, "all"

    aput-object v7, v1, v12

    const-string v7, "phone"

    aput-object v7, v1, v11

    const/4 v7, 0x2

    const-string v8, "sim1"

    aput-object v8, v1, v7

    const/4 v7, 0x3

    const-string v8, "sim2"

    aput-object v8, v1, v7

    const/4 v7, 0x4

    const-string v8, "sim3"

    aput-object v8, v1, v7

    const/4 v7, 0x5

    const-string v8, "sim4"

    aput-object v8, v1, v7

    iget-object v7, p0, Lcom/mediatek/datatransfer/PersonalDataBackupActivity;->mContactCheckTypes:[Z

    aget-boolean v7, v7, v12

    if-eqz v7, :cond_2

    const-string v7, "phone"

    invoke-virtual {v4, v7}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :cond_2
    iget v7, p0, Lcom/mediatek/datatransfer/PersonalDataBackupActivity;->mSimCount:I

    if-lt v7, v11, :cond_4

    const/4 v2, 0x1

    :goto_1
    iget v7, p0, Lcom/mediatek/datatransfer/PersonalDataBackupActivity;->mSimCount:I

    if-gt v2, v7, :cond_4

    iget-object v7, p0, Lcom/mediatek/datatransfer/PersonalDataBackupActivity;->mContactCheckTypes:[Z

    aget-boolean v7, v7, v2

    if-eqz v7, :cond_3

    iget-object v7, p0, Lcom/mediatek/datatransfer/PersonalDataBackupActivity;->mSimInfoList:Ljava/util/List;

    add-int/lit8 v8, v2, -0x1

    invoke-interface {v7, v8}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Lcom/mediatek/telephony/SimInfoManager$SimInfoRecord;

    iget-object v7, v7, Lcom/mediatek/telephony/SimInfoManager$SimInfoRecord;->mDisplayName:Ljava/lang/String;

    invoke-virtual {v4, v7}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    sget-object v8, Lcom/mediatek/datatransfer/PersonalDataBackupActivity;->CLASS_TAG:Ljava/lang/String;

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, " mDisplayName is "

    invoke-virtual {v7, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    iget-object v7, p0, Lcom/mediatek/datatransfer/PersonalDataBackupActivity;->mSimInfoList:Ljava/util/List;

    add-int/lit8 v10, v2, -0x1

    invoke-interface {v7, v10}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Lcom/mediatek/telephony/SimInfoManager$SimInfoRecord;

    iget-object v7, v7, Lcom/mediatek/telephony/SimInfoManager$SimInfoRecord;->mDisplayName:Ljava/lang/String;

    invoke-virtual {v9, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v8, v7}, Lcom/mediatek/datatransfer/utils/MyLogger;->logD(Ljava/lang/String;Ljava/lang/String;)V

    :cond_3
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    :cond_4
    iget-object v7, p0, Lcom/mediatek/datatransfer/AbstractBackupActivity;->mBackupService:Lcom/mediatek/datatransfer/BackupService$BackupBinder;

    invoke-virtual {v7, v11, v4}, Lcom/mediatek/datatransfer/BackupService$BackupBinder;->setBackupItemParam(ILjava/util/ArrayList;)V

    :cond_5
    invoke-static {v13}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    invoke-virtual {v3, v7}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_8

    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    iget-object v7, p0, Lcom/mediatek/datatransfer/PersonalDataBackupActivity;->mMessageCheckTypes:[Z

    aget-boolean v7, v7, v12

    if-eqz v7, :cond_6

    const-string v7, "sms"

    invoke-virtual {v4, v7}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :cond_6
    iget-object v7, p0, Lcom/mediatek/datatransfer/PersonalDataBackupActivity;->mMessageCheckTypes:[Z

    aget-boolean v7, v7, v11

    if-eqz v7, :cond_7

    const-string v7, "mms"

    invoke-virtual {v4, v7}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :cond_7
    iget-object v7, p0, Lcom/mediatek/datatransfer/AbstractBackupActivity;->mBackupService:Lcom/mediatek/datatransfer/BackupService$BackupBinder;

    invoke-virtual {v7, v13, v4}, Lcom/mediatek/datatransfer/BackupService$BackupBinder;->setBackupItemParam(ILjava/util/ArrayList;)V

    :cond_8
    iget-object v7, p0, Lcom/mediatek/datatransfer/AbstractBackupActivity;->mBackupService:Lcom/mediatek/datatransfer/BackupService$BackupBinder;

    invoke-virtual {v7, p1}, Lcom/mediatek/datatransfer/BackupService$BackupBinder;->startBackup(Ljava/lang/String;)Z

    move-result v6

    if-eqz v6, :cond_9

    invoke-virtual {p0}, Lcom/mediatek/datatransfer/AbstractBackupActivity;->showProgress()V

    goto/16 :goto_0

    :cond_9
    invoke-static {}, Lcom/mediatek/datatransfer/utils/SDCardUtils;->getStoragePath()Ljava/lang/String;

    move-result-object v5

    if-nez v5, :cond_a

    sget-object v7, Lcom/mediatek/datatransfer/PersonalDataBackupActivity;->CLASS_TAG:Ljava/lang/String;

    const-string v8, "SDCard is removed"

    invoke-static {v7, v8}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v6, 0x1

    iget-object v7, p0, Lcom/mediatek/datatransfer/AbstractBackupActivity;->mHandler:Landroid/os/Handler;

    new-instance v8, Lcom/mediatek/datatransfer/PersonalDataBackupActivity$1;

    invoke-direct {v8, p0}, Lcom/mediatek/datatransfer/PersonalDataBackupActivity$1;-><init>(Lcom/mediatek/datatransfer/PersonalDataBackupActivity;)V

    invoke-virtual {v7, v8}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    :goto_2
    invoke-virtual {p0}, Lcom/mediatek/datatransfer/AbstractBackupActivity;->stopService()V

    goto/16 :goto_0

    :cond_a
    invoke-static {v5}, Lcom/mediatek/datatransfer/utils/SDCardUtils;->getAvailableSize(Ljava/lang/String;)J

    move-result-wide v7

    const-wide/16 v9, 0x200

    cmp-long v7, v7, v9

    if-gtz v7, :cond_b

    sget-object v7, Lcom/mediatek/datatransfer/PersonalDataBackupActivity;->CLASS_TAG:Ljava/lang/String;

    const-string v8, "SDCard is full"

    invoke-static {v7, v8}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v6, 0x1

    iget-object v7, p0, Lcom/mediatek/datatransfer/AbstractBackupActivity;->mHandler:Landroid/os/Handler;

    new-instance v8, Lcom/mediatek/datatransfer/PersonalDataBackupActivity$2;

    invoke-direct {v8, p0}, Lcom/mediatek/datatransfer/PersonalDataBackupActivity$2;-><init>(Lcom/mediatek/datatransfer/PersonalDataBackupActivity;)V

    invoke-virtual {v7, v8}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    goto :goto_2

    :cond_b
    sget-object v7, Lcom/mediatek/datatransfer/PersonalDataBackupActivity;->CLASS_TAG:Ljava/lang/String;

    const-string v8, "unkown error"

    invoke-static {v7, v8}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    const-string v7, "name"

    const/16 v8, 0x2f

    invoke-virtual {p1, v8}, Ljava/lang/String;->lastIndexOf(I)I

    move-result v8

    add-int/lit8 v8, v8, 0x1

    invoke-virtual {p1, v8}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v0, v7, v8}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    const/16 v7, 0x7db

    invoke-virtual {p0, v7, v0}, Landroid/app/Activity;->showDialog(ILandroid/os/Bundle;)Z

    goto :goto_2

    :cond_c
    invoke-virtual {p0}, Lcom/mediatek/datatransfer/AbstractBackupActivity;->stopService()V

    sget-object v7, Lcom/mediatek/datatransfer/PersonalDataBackupActivity;->CLASS_TAG:Ljava/lang/String;

    const-string v8, "startPersonalDataBackup: error! service is null"

    invoke-static {v7, v8}, Lcom/mediatek/datatransfer/utils/MyLogger;->logE(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0
.end method

.method private startUnlockSim(I)V
    .locals 4
    .param p1    # I

    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    sget-object v1, Lcom/mediatek/datatransfer/PersonalDataBackupActivity;->CLASS_TAG:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "[startUnlockSim] slot = "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/mediatek/datatransfer/utils/MyLogger;->logD(Ljava/lang/String;Ljava/lang/String;)V

    const-string v1, "com.android.phone.ACTION_UNLOCK_SIM_LOCK"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    const-string v1, "com.android.phone.EXTRA_SIM_SLOT"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    const-string v1, "com.android.phone.EXTRA_UNLOCK_TYPE"

    const/16 v2, 0x1f5

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    invoke-virtual {p0, v0}, Landroid/content/ContextWrapper;->sendBroadcast(Landroid/content/Intent;)V

    return-void
.end method

.method private updateData(Ljava/util/ArrayList;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/mediatek/datatransfer/PersonalItemData;",
            ">;)V"
        }
    .end annotation

    iput-object p1, p0, Lcom/mediatek/datatransfer/PersonalDataBackupActivity;->mBackupItemDataList:Ljava/util/ArrayList;

    iget-object v0, p0, Lcom/mediatek/datatransfer/PersonalDataBackupActivity;->mBackupListAdapter:Lcom/mediatek/datatransfer/PersonalDataBackupActivity$PersonalDataBackupAdapter;

    iget-object v1, p0, Lcom/mediatek/datatransfer/PersonalDataBackupActivity;->mBackupItemDataList:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Lcom/mediatek/datatransfer/PersonalDataBackupActivity$PersonalDataBackupAdapter;->changeData(Ljava/util/ArrayList;)V

    invoke-virtual {p0}, Lcom/mediatek/datatransfer/CheckedListActivity;->syncUnCheckedItems()V

    iget-object v0, p0, Lcom/mediatek/datatransfer/PersonalDataBackupActivity;->mBackupListAdapter:Lcom/mediatek/datatransfer/PersonalDataBackupActivity$PersonalDataBackupAdapter;

    invoke-virtual {v0}, Landroid/widget/BaseAdapter;->notifyDataSetChanged()V

    invoke-virtual {p0}, Lcom/mediatek/datatransfer/PersonalDataBackupActivity;->updateTitle()V

    invoke-virtual {p0}, Lcom/mediatek/datatransfer/AbstractBackupActivity;->updateButtonState()V

    invoke-virtual {p0}, Lcom/mediatek/datatransfer/PersonalDataBackupActivity;->checkBackupState()V

    return-void
.end method


# virtual methods
.method protected afterServiceConnected()V
    .locals 1

    new-instance v0, Lcom/mediatek/datatransfer/PersonalDataBackupActivity$PersonalDataBackupStatusListener;

    invoke-direct {v0, p0}, Lcom/mediatek/datatransfer/PersonalDataBackupActivity$PersonalDataBackupStatusListener;-><init>(Lcom/mediatek/datatransfer/PersonalDataBackupActivity;)V

    iput-object v0, p0, Lcom/mediatek/datatransfer/PersonalDataBackupActivity;->mBackupListener:Lcom/mediatek/datatransfer/BackupService$OnBackupStatusListener;

    iget-object v0, p0, Lcom/mediatek/datatransfer/PersonalDataBackupActivity;->mBackupListener:Lcom/mediatek/datatransfer/BackupService$OnBackupStatusListener;

    invoke-virtual {p0, v0}, Lcom/mediatek/datatransfer/AbstractBackupActivity;->setOnBackupStatusListener(Lcom/mediatek/datatransfer/BackupService$OnBackupStatusListener;)V

    invoke-virtual {p0}, Lcom/mediatek/datatransfer/PersonalDataBackupActivity;->checkBackupState()V

    return-void
.end method

.method protected checkBackupState()V
    .locals 6

    iget-object v3, p0, Lcom/mediatek/datatransfer/AbstractBackupActivity;->mBackupService:Lcom/mediatek/datatransfer/BackupService$BackupBinder;

    if-eqz v3, :cond_0

    iget-object v3, p0, Lcom/mediatek/datatransfer/AbstractBackupActivity;->mBackupService:Lcom/mediatek/datatransfer/BackupService$BackupBinder;

    invoke-virtual {v3}, Lcom/mediatek/datatransfer/BackupService$BackupBinder;->getState()I

    move-result v2

    packed-switch v2, :pswitch_data_0

    :pswitch_0
    invoke-super {p0}, Lcom/mediatek/datatransfer/AbstractBackupActivity;->checkBackupState()V

    :cond_0
    :goto_0
    return-void

    :pswitch_1
    iget-object v3, p0, Lcom/mediatek/datatransfer/AbstractBackupActivity;->mBackupService:Lcom/mediatek/datatransfer/BackupService$BackupBinder;

    invoke-virtual {v3}, Lcom/mediatek/datatransfer/BackupService$BackupBinder;->getCurBackupProgress()Lcom/mediatek/datatransfer/BackupService$BackupProgress;

    move-result-object v1

    sget-object v3, Lcom/mediatek/datatransfer/PersonalDataBackupActivity;->CLASS_TAG:Ljava/lang/String;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "checkBackupState: Max = "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget v5, v1, Lcom/mediatek/datatransfer/BackupService$BackupProgress;->mMax:I

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " curprogress = "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget v5, v1, Lcom/mediatek/datatransfer/BackupService$BackupProgress;->mCurNum:I

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v3, 0x1

    if-ne v2, v3, :cond_1

    iget-object v3, p0, Lcom/mediatek/datatransfer/AbstractBackupActivity;->mProgressDialog:Landroid/app/ProgressDialog;

    invoke-virtual {v3}, Landroid/app/Dialog;->show()V

    :cond_1
    iget v3, v1, Lcom/mediatek/datatransfer/BackupService$BackupProgress;->mCurNum:I

    iget v4, v1, Lcom/mediatek/datatransfer/BackupService$BackupProgress;->mMax:I

    if-ge v3, v4, :cond_2

    iget v3, v1, Lcom/mediatek/datatransfer/BackupService$BackupProgress;->mType:I

    invoke-virtual {p0, v3}, Lcom/mediatek/datatransfer/PersonalDataBackupActivity;->getProgressDlgMessage(I)Ljava/lang/String;

    move-result-object v0

    iget-object v3, p0, Lcom/mediatek/datatransfer/AbstractBackupActivity;->mProgressDialog:Landroid/app/ProgressDialog;

    if-eqz v3, :cond_2

    iget-object v3, p0, Lcom/mediatek/datatransfer/AbstractBackupActivity;->mProgressDialog:Landroid/app/ProgressDialog;

    invoke-virtual {v3, v0}, Landroid/app/ProgressDialog;->setMessage(Ljava/lang/CharSequence;)V

    :cond_2
    iget-object v3, p0, Lcom/mediatek/datatransfer/AbstractBackupActivity;->mProgressDialog:Landroid/app/ProgressDialog;

    if-eqz v3, :cond_0

    iget-object v3, p0, Lcom/mediatek/datatransfer/AbstractBackupActivity;->mProgressDialog:Landroid/app/ProgressDialog;

    iget v4, v1, Lcom/mediatek/datatransfer/BackupService$BackupProgress;->mMax:I

    invoke-virtual {v3, v4}, Landroid/app/ProgressDialog;->setMax(I)V

    iget-object v3, p0, Lcom/mediatek/datatransfer/AbstractBackupActivity;->mProgressDialog:Landroid/app/ProgressDialog;

    iget v4, v1, Lcom/mediatek/datatransfer/BackupService$BackupProgress;->mCurNum:I

    invoke-virtual {v3, v4}, Landroid/app/ProgressDialog;->setProgress(I)V

    goto :goto_0

    :pswitch_2
    iget-object v3, p0, Lcom/mediatek/datatransfer/AbstractBackupActivity;->mBackupService:Lcom/mediatek/datatransfer/BackupService$BackupBinder;

    invoke-virtual {v3}, Lcom/mediatek/datatransfer/BackupService$BackupBinder;->getBackupResultType()Lcom/mediatek/datatransfer/BackupEngine$BackupResultType;

    move-result-object v3

    iget-object v4, p0, Lcom/mediatek/datatransfer/AbstractBackupActivity;->mBackupService:Lcom/mediatek/datatransfer/BackupService$BackupBinder;

    invoke-virtual {v4}, Lcom/mediatek/datatransfer/BackupService$BackupBinder;->getBackupResult()Ljava/util/ArrayList;

    move-result-object v4

    invoke-direct {p0, v3, v4}, Lcom/mediatek/datatransfer/PersonalDataBackupActivity;->showBackupResult(Lcom/mediatek/datatransfer/BackupEngine$BackupResultType;Ljava/util/ArrayList;)V

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_1
        :pswitch_0
        :pswitch_0
        :pswitch_2
    .end packed-switch
.end method

.method protected getProgressDlgMessage(I)Ljava/lang/String;
    .locals 2
    .param p1    # I

    new-instance v0, Ljava/lang/StringBuilder;

    const v1, 0x7f06000e

    invoke-virtual {p0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v1, "("

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-static {p0, p1}, Lcom/mediatek/datatransfer/utils/ModuleType;->getModuleStringFromType(Landroid/content/Context;I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    return-object v1
.end method

.method public initBackupAdapter()Landroid/widget/BaseAdapter;
    .locals 3

    new-instance v0, Lcom/mediatek/datatransfer/PersonalDataBackupActivity$PersonalDataBackupAdapter;

    iget-object v1, p0, Lcom/mediatek/datatransfer/PersonalDataBackupActivity;->mBackupItemDataList:Ljava/util/ArrayList;

    const v2, 0x7f030002

    invoke-direct {v0, p0, p0, v1, v2}, Lcom/mediatek/datatransfer/PersonalDataBackupActivity$PersonalDataBackupAdapter;-><init>(Lcom/mediatek/datatransfer/PersonalDataBackupActivity;Landroid/content/Context;Ljava/util/ArrayList;I)V

    iput-object v0, p0, Lcom/mediatek/datatransfer/PersonalDataBackupActivity;->mBackupListAdapter:Lcom/mediatek/datatransfer/PersonalDataBackupActivity$PersonalDataBackupAdapter;

    iget-object v0, p0, Lcom/mediatek/datatransfer/PersonalDataBackupActivity;->mBackupListAdapter:Lcom/mediatek/datatransfer/PersonalDataBackupActivity$PersonalDataBackupAdapter;

    return-object v0
.end method

.method public onCheckedCountChanged()V
    .locals 0

    invoke-super {p0}, Lcom/mediatek/datatransfer/AbstractBackupActivity;->onCheckedCountChanged()V

    invoke-virtual {p0}, Lcom/mediatek/datatransfer/PersonalDataBackupActivity;->updateTitle()V

    return-void
.end method

.method public onConfigurationChanged(Landroid/content/res/Configuration;)V
    .locals 2
    .param p1    # Landroid/content/res/Configuration;

    invoke-super {p0, p1}, Lcom/mediatek/datatransfer/AbstractBackupActivity;->onConfigurationChanged(Landroid/content/res/Configuration;)V

    sget-object v0, Lcom/mediatek/datatransfer/PersonalDataBackupActivity;->CLASS_TAG:Ljava/lang/String;

    const-string v1, "onConfigurationChanged"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    return-void
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 4
    .param p1    # Landroid/os/Bundle;

    const/4 v3, 0x0

    const/4 v2, 0x1

    const/4 v1, 0x5

    invoke-virtual {p0, v1}, Landroid/app/Activity;->requestWindowFeature(I)Z

    invoke-super {p0, p1}, Lcom/mediatek/datatransfer/AbstractBackupActivity;->onCreate(Landroid/os/Bundle;)V

    if-eqz p1, :cond_0

    iget-object v1, p0, Lcom/mediatek/datatransfer/PersonalDataBackupActivity;->CONTACT_TYPE:Ljava/lang/String;

    invoke-virtual {p1, v1}, Landroid/os/Bundle;->getBooleanArray(Ljava/lang/String;)[Z

    move-result-object v1

    iput-object v1, p0, Lcom/mediatek/datatransfer/PersonalDataBackupActivity;->mContactCheckTypes:[Z

    iget-object v1, p0, Lcom/mediatek/datatransfer/PersonalDataBackupActivity;->MESSAGE_TYPE:Ljava/lang/String;

    invoke-virtual {p1, v1}, Landroid/os/Bundle;->getBooleanArray(Ljava/lang/String;)[Z

    move-result-object v1

    iput-object v1, p0, Lcom/mediatek/datatransfer/PersonalDataBackupActivity;->mMessageCheckTypes:[Z

    :goto_0
    sget-object v1, Lcom/mediatek/datatransfer/PersonalDataBackupActivity;->CLASS_TAG:Ljava/lang/String;

    const-string v2, "onCreate"

    invoke-static {v1, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    invoke-direct {p0}, Lcom/mediatek/datatransfer/PersonalDataBackupActivity;->init()V

    invoke-virtual {p0, v3}, Landroid/app/Activity;->setProgressBarIndeterminateVisibility(Z)V

    return-void

    :cond_0
    invoke-direct {p0}, Lcom/mediatek/datatransfer/PersonalDataBackupActivity;->getSimInfoList()V

    const/4 v0, 0x0

    :goto_1
    iget-object v1, p0, Lcom/mediatek/datatransfer/PersonalDataBackupActivity;->mContactCheckTypes:[Z

    array-length v1, v1

    if-ge v0, v1, :cond_1

    iget-object v1, p0, Lcom/mediatek/datatransfer/PersonalDataBackupActivity;->mContactCheckTypes:[Z

    aput-boolean v2, v1, v0

    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    :cond_1
    iget-object v1, p0, Lcom/mediatek/datatransfer/PersonalDataBackupActivity;->mMessageCheckTypes:[Z

    aput-boolean v2, v1, v3

    iget-object v1, p0, Lcom/mediatek/datatransfer/PersonalDataBackupActivity;->mMessageCheckTypes:[Z

    aput-boolean v2, v1, v2

    goto :goto_0
.end method

.method protected onCreateDialog(ILandroid/os/Bundle;)Landroid/app/Dialog;
    .locals 3
    .param p1    # I
    .param p2    # Landroid/os/Bundle;

    const/4 v0, 0x0

    sparse-switch p1, :sswitch_data_0

    invoke-super {p0, p1, p2}, Lcom/mediatek/datatransfer/AbstractBackupActivity;->onCreateDialog(ILandroid/os/Bundle;)Landroid/app/Dialog;

    move-result-object v0

    :goto_0
    return-object v0

    :sswitch_0
    invoke-direct {p0}, Lcom/mediatek/datatransfer/PersonalDataBackupActivity;->createFolderEditorDialog()Landroid/app/AlertDialog;

    move-result-object v0

    goto :goto_0

    :sswitch_1
    new-instance v1, Lcom/mediatek/datatransfer/PersonalDataBackupActivity$11;

    invoke-direct {v1, p0}, Lcom/mediatek/datatransfer/PersonalDataBackupActivity$11;-><init>(Lcom/mediatek/datatransfer/PersonalDataBackupActivity;)V

    const v2, 0x7f060020

    invoke-static {p0, v2, p2, v1}, Lcom/mediatek/datatransfer/ResultDialog;->createResultDlg(Landroid/content/Context;ILandroid/os/Bundle;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog;

    move-result-object v0

    goto :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x7d4 -> :sswitch_1
        0x7da -> :sswitch_0
    .end sparse-switch
.end method

.method protected onDestroy()V
    .locals 4

    invoke-super {p0}, Lcom/mediatek/datatransfer/AbstractBackupActivity;->onDestroy()V

    iget-object v2, p0, Lcom/mediatek/datatransfer/PersonalDataBackupActivity;->dialogs:Ljava/util/ArrayList;

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/mediatek/datatransfer/PersonalDataBackupActivity;->dialogs:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_0

    iget-object v2, p0, Lcom/mediatek/datatransfer/PersonalDataBackupActivity;->dialogs:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/AlertDialog;

    invoke-virtual {v0}, Landroid/app/Dialog;->cancel()V

    goto :goto_0

    :cond_0
    sget-object v2, Lcom/mediatek/datatransfer/PersonalDataBackupActivity;->CLASS_TAG:Ljava/lang/String;

    const-string v3, "onDestroy"

    invoke-static {v2, v3}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    return-void
.end method

.method protected onPrepareDialog(ILandroid/app/Dialog;Landroid/os/Bundle;)V
    .locals 9
    .param p1    # I
    .param p2    # Landroid/app/Dialog;
    .param p3    # Landroid/os/Bundle;

    sparse-switch p1, :sswitch_data_0

    invoke-super {p0, p1, p2, p3}, Landroid/app/Activity;->onPrepareDialog(ILandroid/app/Dialog;Landroid/os/Bundle;)V

    :cond_0
    :goto_0
    return-void

    :sswitch_0
    move-object v3, p2

    check-cast v3, Landroid/app/AlertDialog;

    invoke-virtual {v3}, Landroid/app/AlertDialog;->getListView()Landroid/widget/ListView;

    move-result-object v5

    if-eqz v5, :cond_0

    invoke-static {p0, p3}, Lcom/mediatek/datatransfer/ResultDialog;->createResultAdapter(Landroid/content/Context;Landroid/os/Bundle;)Lcom/mediatek/datatransfer/ResultDialog$ResultDialogAdapter;

    move-result-object v0

    invoke-virtual {v5, v0}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    goto :goto_0

    :sswitch_1
    const v6, 0x7f08000b

    invoke-virtual {p2, v6}, Landroid/app/Dialog;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/EditText;

    if-eqz v4, :cond_0

    new-instance v1, Ljava/text/SimpleDateFormat;

    const-string v6, "yyyyMMddHHmmss"

    invoke-direct {v1, v6}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;)V

    new-instance v6, Ljava/util/Date;

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v7

    invoke-direct {v6, v7, v8}, Ljava/util/Date;-><init>(J)V

    invoke-virtual {v1, v6}, Ljava/text/DateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v4, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x7d4 -> :sswitch_0
        0x7da -> :sswitch_1
    .end sparse-switch
.end method

.method protected onResume()V
    .locals 2

    invoke-super {p0}, Lcom/mediatek/datatransfer/AbstractBackupActivity;->onResume()V

    sget-object v0, Lcom/mediatek/datatransfer/PersonalDataBackupActivity;->CLASS_TAG:Ljava/lang/String;

    const-string v1, "onResume"

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    return-void
.end method

.method protected onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 2
    .param p1    # Landroid/os/Bundle;

    invoke-super {p0, p1}, Lcom/mediatek/datatransfer/CheckedListActivity;->onSaveInstanceState(Landroid/os/Bundle;)V

    iget-object v0, p0, Lcom/mediatek/datatransfer/PersonalDataBackupActivity;->CONTACT_TYPE:Ljava/lang/String;

    iget-object v1, p0, Lcom/mediatek/datatransfer/PersonalDataBackupActivity;->mContactCheckTypes:[Z

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBooleanArray(Ljava/lang/String;[Z)V

    iget-object v0, p0, Lcom/mediatek/datatransfer/PersonalDataBackupActivity;->MESSAGE_TYPE:Ljava/lang/String;

    iget-object v1, p0, Lcom/mediatek/datatransfer/PersonalDataBackupActivity;->mMessageCheckTypes:[Z

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBooleanArray(Ljava/lang/String;[Z)V

    return-void
.end method

.method protected onStart()V
    .locals 2

    invoke-super {p0}, Landroid/app/Activity;->onStart()V

    sget-object v0, Lcom/mediatek/datatransfer/PersonalDataBackupActivity;->CLASS_TAG:Ljava/lang/String;

    const-string v1, "onStart"

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    invoke-static {}, Lcom/mediatek/datatransfer/utils/SDCardUtils;->getExternalStoragePath()Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_0

    sget-object v0, Lcom/mediatek/datatransfer/PersonalDataBackupActivity;->CLASS_TAG:Ljava/lang/String;

    const-string v1, "onStart NO SDCARD! cancel initDataTask "

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    invoke-virtual {p0}, Landroid/app/Activity;->finish()V

    :goto_0
    return-void

    :cond_0
    new-instance v0, Lcom/mediatek/datatransfer/PersonalDataBackupActivity$InitPersonalDataTask;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/mediatek/datatransfer/PersonalDataBackupActivity$InitPersonalDataTask;-><init>(Lcom/mediatek/datatransfer/PersonalDataBackupActivity;Lcom/mediatek/datatransfer/PersonalDataBackupActivity$1;)V

    iput-object v0, p0, Lcom/mediatek/datatransfer/PersonalDataBackupActivity;->initDataTask:Lcom/mediatek/datatransfer/PersonalDataBackupActivity$InitPersonalDataTask;

    iget-object v0, p0, Lcom/mediatek/datatransfer/PersonalDataBackupActivity;->initDataTask:Lcom/mediatek/datatransfer/PersonalDataBackupActivity$InitPersonalDataTask;

    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/Void;

    invoke-virtual {v0, v1}, Landroid/os/AsyncTask;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    goto :goto_0
.end method

.method protected onStop()V
    .locals 2

    invoke-super {p0}, Landroid/app/Activity;->onStop()V

    sget-object v0, Lcom/mediatek/datatransfer/PersonalDataBackupActivity;->CLASS_TAG:Ljava/lang/String;

    const-string v1, "onStop"

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v0, p0, Lcom/mediatek/datatransfer/PersonalDataBackupActivity;->initDataTask:Lcom/mediatek/datatransfer/PersonalDataBackupActivity$InitPersonalDataTask;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/mediatek/datatransfer/PersonalDataBackupActivity;->initDataTask:Lcom/mediatek/datatransfer/PersonalDataBackupActivity$InitPersonalDataTask;

    invoke-virtual {v0}, Landroid/os/AsyncTask;->getStatus()Landroid/os/AsyncTask$Status;

    move-result-object v0

    sget-object v1, Landroid/os/AsyncTask$Status;->RUNNING:Landroid/os/AsyncTask$Status;

    if-ne v0, v1, :cond_0

    iget-object v0, p0, Lcom/mediatek/datatransfer/PersonalDataBackupActivity;->initDataTask:Lcom/mediatek/datatransfer/PersonalDataBackupActivity$InitPersonalDataTask;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/os/AsyncTask;->cancel(Z)Z

    iget-object v0, p0, Lcom/mediatek/datatransfer/PersonalDataBackupActivity;->mBackupListAdapter:Lcom/mediatek/datatransfer/PersonalDataBackupActivity$PersonalDataBackupAdapter;

    invoke-virtual {v0}, Lcom/mediatek/datatransfer/PersonalDataBackupActivity$PersonalDataBackupAdapter;->reset()V

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/mediatek/datatransfer/PersonalDataBackupActivity;->initDataTask:Lcom/mediatek/datatransfer/PersonalDataBackupActivity$InitPersonalDataTask;

    :cond_0
    return-void
.end method

.method public setAirplaneMode(Landroid/content/Context;Z)V
    .locals 4
    .param p1    # Landroid/content/Context;
    .param p2    # Z

    const/4 v3, 0x0

    invoke-virtual {p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    const-string v2, "airplane_mode_on"

    invoke-static {v1, v2, v3}, Landroid/provider/Settings$Global;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    new-instance v0, Landroid/content/Intent;

    const-string v1, "android.intent.action.AIRPLANE_MODE"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    const-string v1, "state"

    invoke-virtual {v0, v1, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    invoke-virtual {p1, v0}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    return-void
.end method

.method public startBackup()V
    .locals 3

    const/16 v2, 0x7da

    sget-object v0, Lcom/mediatek/datatransfer/PersonalDataBackupActivity;->CLASS_TAG:Ljava/lang/String;

    const-string v1, "startBackup"

    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    invoke-direct {p0}, Lcom/mediatek/datatransfer/PersonalDataBackupActivity;->isSimCardSelected()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-direct {p0, p0}, Lcom/mediatek/datatransfer/PersonalDataBackupActivity;->isAirModeOn(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-direct {p0}, Lcom/mediatek/datatransfer/PersonalDataBackupActivity;->showAirModeOnDialog()V

    :cond_0
    :goto_0
    return-void

    :cond_1
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/mediatek/datatransfer/PersonalDataBackupActivity;->checkSimLocked(Lcom/mediatek/telephony/SimInfoManager$SimInfoRecord;)Z

    move-result v0

    if-nez v0, :cond_0

    invoke-virtual {p0, v2}, Landroid/app/Activity;->showDialog(I)V

    goto :goto_0

    :cond_2
    invoke-virtual {p0, v2}, Landroid/app/Activity;->showDialog(I)V

    goto :goto_0
.end method

.method public updateTitle()V
    .locals 5

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const v3, 0x7f060028

    invoke-virtual {p0, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Lcom/mediatek/datatransfer/CheckedListActivity;->getCount()I

    move-result v2

    invoke-virtual {p0}, Lcom/mediatek/datatransfer/CheckedListActivity;->getCheckedCount()I

    move-result v0

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "("

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "/"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ")"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v3, ""

    invoke-virtual {p0, v3}, Landroid/app/Activity;->setTitle(Ljava/lang/CharSequence;)V

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p0, v3}, Landroid/app/Activity;->setTitle(Ljava/lang/CharSequence;)V

    return-void
.end method
