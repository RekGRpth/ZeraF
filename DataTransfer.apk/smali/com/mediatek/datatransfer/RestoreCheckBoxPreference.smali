.class public Lcom/mediatek/datatransfer/RestoreCheckBoxPreference;
.super Landroid/preference/Preference;
.source "RestoreCheckBoxPreference.java"


# instance fields
.field private final CLASS_TAG:Ljava/lang/String;

.field private mAccociateFile:Ljava/io/File;

.field private mCheckBox:Landroid/widget/CheckBox;

.field private mChecked:Z

.field private mRestored:Z

.field private mTextView:Landroid/widget/TextView;

.field private mVisibility:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 2
    .param p1    # Landroid/content/Context;

    const/4 v1, 0x0

    invoke-direct {p0, p1}, Landroid/preference/Preference;-><init>(Landroid/content/Context;)V

    const-string v0, "DataTransfer/RestoreCheckBoxPreference"

    iput-object v0, p0, Lcom/mediatek/datatransfer/RestoreCheckBoxPreference;->CLASS_TAG:Ljava/lang/String;

    iput-boolean v1, p0, Lcom/mediatek/datatransfer/RestoreCheckBoxPreference;->mVisibility:Z

    iput-boolean v1, p0, Lcom/mediatek/datatransfer/RestoreCheckBoxPreference;->mChecked:Z

    iput-boolean v1, p0, Lcom/mediatek/datatransfer/RestoreCheckBoxPreference;->mRestored:Z

    const v0, 0x7f030004

    invoke-virtual {p0, v0}, Landroid/preference/Preference;->setWidgetLayoutResource(I)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 2
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/util/AttributeSet;

    const/4 v1, 0x0

    invoke-direct {p0, p1, p2}, Landroid/preference/Preference;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    const-string v0, "DataTransfer/RestoreCheckBoxPreference"

    iput-object v0, p0, Lcom/mediatek/datatransfer/RestoreCheckBoxPreference;->CLASS_TAG:Ljava/lang/String;

    iput-boolean v1, p0, Lcom/mediatek/datatransfer/RestoreCheckBoxPreference;->mVisibility:Z

    iput-boolean v1, p0, Lcom/mediatek/datatransfer/RestoreCheckBoxPreference;->mChecked:Z

    iput-boolean v1, p0, Lcom/mediatek/datatransfer/RestoreCheckBoxPreference;->mRestored:Z

    const v0, 0x7f030004

    invoke-virtual {p0, v0}, Landroid/preference/Preference;->setWidgetLayoutResource(I)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 2
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/util/AttributeSet;
    .param p3    # I

    const/4 v1, 0x0

    invoke-direct {p0, p1, p2, p3}, Landroid/preference/Preference;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    const-string v0, "DataTransfer/RestoreCheckBoxPreference"

    iput-object v0, p0, Lcom/mediatek/datatransfer/RestoreCheckBoxPreference;->CLASS_TAG:Ljava/lang/String;

    iput-boolean v1, p0, Lcom/mediatek/datatransfer/RestoreCheckBoxPreference;->mVisibility:Z

    iput-boolean v1, p0, Lcom/mediatek/datatransfer/RestoreCheckBoxPreference;->mChecked:Z

    iput-boolean v1, p0, Lcom/mediatek/datatransfer/RestoreCheckBoxPreference;->mRestored:Z

    return-void
.end method

.method static synthetic access$002(Lcom/mediatek/datatransfer/RestoreCheckBoxPreference;Z)Z
    .locals 0
    .param p0    # Lcom/mediatek/datatransfer/RestoreCheckBoxPreference;
    .param p1    # Z

    iput-boolean p1, p0, Lcom/mediatek/datatransfer/RestoreCheckBoxPreference;->mChecked:Z

    return p1
.end method

.method private showTextView(Z)V
    .locals 3
    .param p1    # Z

    const-string v0, "DataTransfer/RestoreCheckBoxPreference"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "showTextView: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/mediatek/datatransfer/utils/MyLogger;->logD(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/mediatek/datatransfer/RestoreCheckBoxPreference;->mTextView:Landroid/widget/TextView;

    if-nez v0, :cond_0

    :goto_0
    return-void

    :cond_0
    if-eqz p1, :cond_1

    iget-object v0, p0, Lcom/mediatek/datatransfer/RestoreCheckBoxPreference;->mTextView:Landroid/widget/TextView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    goto :goto_0

    :cond_1
    iget-object v0, p0, Lcom/mediatek/datatransfer/RestoreCheckBoxPreference;->mTextView:Landroid/widget/TextView;

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    goto :goto_0
.end method


# virtual methods
.method public getAccociateFile()Ljava/io/File;
    .locals 1

    iget-object v0, p0, Lcom/mediatek/datatransfer/RestoreCheckBoxPreference;->mAccociateFile:Ljava/io/File;

    return-object v0
.end method

.method public getKey()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/mediatek/datatransfer/RestoreCheckBoxPreference;->mAccociateFile:Ljava/io/File;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/mediatek/datatransfer/RestoreCheckBoxPreference;->mAccociateFile:Ljava/io/File;

    invoke-virtual {v0}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    const-string v0, ""

    goto :goto_0
.end method

.method public isChecked()Z
    .locals 1

    iget-boolean v0, p0, Lcom/mediatek/datatransfer/RestoreCheckBoxPreference;->mChecked:Z

    return v0
.end method

.method protected onBindView(Landroid/view/View;)V
    .locals 2
    .param p1    # Landroid/view/View;

    const v0, 0x7f08000c

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/mediatek/datatransfer/RestoreCheckBoxPreference;->mTextView:Landroid/widget/TextView;

    const v0, 0x7f08000d

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/CheckBox;

    iput-object v0, p0, Lcom/mediatek/datatransfer/RestoreCheckBoxPreference;->mCheckBox:Landroid/widget/CheckBox;

    iget-boolean v0, p0, Lcom/mediatek/datatransfer/RestoreCheckBoxPreference;->mRestored:Z

    invoke-direct {p0, v0}, Lcom/mediatek/datatransfer/RestoreCheckBoxPreference;->showTextView(Z)V

    iget-boolean v0, p0, Lcom/mediatek/datatransfer/RestoreCheckBoxPreference;->mVisibility:Z

    invoke-virtual {p0, v0}, Lcom/mediatek/datatransfer/RestoreCheckBoxPreference;->showCheckbox(Z)V

    iget-object v0, p0, Lcom/mediatek/datatransfer/RestoreCheckBoxPreference;->mCheckBox:Landroid/widget/CheckBox;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/mediatek/datatransfer/RestoreCheckBoxPreference;->mCheckBox:Landroid/widget/CheckBox;

    new-instance v1, Lcom/mediatek/datatransfer/RestoreCheckBoxPreference$1;

    invoke-direct {v1, p0}, Lcom/mediatek/datatransfer/RestoreCheckBoxPreference$1;-><init>(Lcom/mediatek/datatransfer/RestoreCheckBoxPreference;)V

    invoke-virtual {v0, v1}, Landroid/widget/CompoundButton;->setOnCheckedChangeListener(Landroid/widget/CompoundButton$OnCheckedChangeListener;)V

    :cond_0
    iget-boolean v0, p0, Lcom/mediatek/datatransfer/RestoreCheckBoxPreference;->mChecked:Z

    invoke-virtual {p0, v0}, Lcom/mediatek/datatransfer/RestoreCheckBoxPreference;->setChecked(Z)V

    invoke-super {p0, p1}, Landroid/preference/Preference;->onBindView(Landroid/view/View;)V

    return-void
.end method

.method public setAccociateFile(Ljava/io/File;)V
    .locals 0
    .param p1    # Ljava/io/File;

    iput-object p1, p0, Lcom/mediatek/datatransfer/RestoreCheckBoxPreference;->mAccociateFile:Ljava/io/File;

    return-void
.end method

.method public setChecked(Z)V
    .locals 1
    .param p1    # Z

    iput-boolean p1, p0, Lcom/mediatek/datatransfer/RestoreCheckBoxPreference;->mChecked:Z

    iget-object v0, p0, Lcom/mediatek/datatransfer/RestoreCheckBoxPreference;->mCheckBox:Landroid/widget/CheckBox;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/mediatek/datatransfer/RestoreCheckBoxPreference;->mCheckBox:Landroid/widget/CheckBox;

    invoke-virtual {v0, p1}, Landroid/widget/CompoundButton;->setChecked(Z)V

    :cond_0
    return-void
.end method

.method public setRestored(Z)V
    .locals 0
    .param p1    # Z

    iput-boolean p1, p0, Lcom/mediatek/datatransfer/RestoreCheckBoxPreference;->mRestored:Z

    return-void
.end method

.method public showCheckbox(Z)V
    .locals 3
    .param p1    # Z

    const-string v0, "DataTransfer/RestoreCheckBoxPreference"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "showCheckbox: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/mediatek/datatransfer/utils/MyLogger;->logD(Ljava/lang/String;Ljava/lang/String;)V

    iput-boolean p1, p0, Lcom/mediatek/datatransfer/RestoreCheckBoxPreference;->mVisibility:Z

    iget-object v0, p0, Lcom/mediatek/datatransfer/RestoreCheckBoxPreference;->mCheckBox:Landroid/widget/CheckBox;

    if-nez v0, :cond_0

    :goto_0
    return-void

    :cond_0
    if-eqz p1, :cond_1

    iget-object v0, p0, Lcom/mediatek/datatransfer/RestoreCheckBoxPreference;->mCheckBox:Landroid/widget/CheckBox;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    goto :goto_0

    :cond_1
    iget-object v0, p0, Lcom/mediatek/datatransfer/RestoreCheckBoxPreference;->mCheckBox:Landroid/widget/CheckBox;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    goto :goto_0
.end method
