.class Lcom/mediatek/datatransfer/modules/SmsRestoreComposer$SmsRestoreEntry;
.super Ljava/lang/Object;
.source "SmsRestoreComposer.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/mediatek/datatransfer/modules/SmsRestoreComposer;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = "SmsRestoreEntry"
.end annotation


# instance fields
.field private body:Ljava/lang/String;

.field private boxType:Ljava/lang/String;

.field private locked:Ljava/lang/String;

.field private readByte:Ljava/lang/String;

.field private seen:Ljava/lang/String;

.field private simCardid:Ljava/lang/String;

.field private smsAddress:Ljava/lang/String;

.field final synthetic this$0:Lcom/mediatek/datatransfer/modules/SmsRestoreComposer;

.field private timeStamp:Ljava/lang/String;


# direct methods
.method constructor <init>(Lcom/mediatek/datatransfer/modules/SmsRestoreComposer;)V
    .locals 0

    iput-object p1, p0, Lcom/mediatek/datatransfer/modules/SmsRestoreComposer$SmsRestoreEntry;->this$0:Lcom/mediatek/datatransfer/modules/SmsRestoreComposer;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public getBody()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/mediatek/datatransfer/modules/SmsRestoreComposer$SmsRestoreEntry;->body:Ljava/lang/String;

    return-object v0
.end method

.method public getBoxType()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/mediatek/datatransfer/modules/SmsRestoreComposer$SmsRestoreEntry;->boxType:Ljava/lang/String;

    return-object v0
.end method

.method public getLocked()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/mediatek/datatransfer/modules/SmsRestoreComposer$SmsRestoreEntry;->locked:Ljava/lang/String;

    return-object v0
.end method

.method public getReadByte()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/mediatek/datatransfer/modules/SmsRestoreComposer$SmsRestoreEntry;->readByte:Ljava/lang/String;

    if-nez v0, :cond_0

    const-string v0, "READ"

    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/mediatek/datatransfer/modules/SmsRestoreComposer$SmsRestoreEntry;->readByte:Ljava/lang/String;

    goto :goto_0
.end method

.method public getSeen()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/mediatek/datatransfer/modules/SmsRestoreComposer$SmsRestoreEntry;->seen:Ljava/lang/String;

    if-nez v0, :cond_0

    const-string v0, "1"

    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/mediatek/datatransfer/modules/SmsRestoreComposer$SmsRestoreEntry;->seen:Ljava/lang/String;

    goto :goto_0
.end method

.method public getSimCardid()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/mediatek/datatransfer/modules/SmsRestoreComposer$SmsRestoreEntry;->simCardid:Ljava/lang/String;

    return-object v0
.end method

.method public getSmsAddress()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/mediatek/datatransfer/modules/SmsRestoreComposer$SmsRestoreEntry;->smsAddress:Ljava/lang/String;

    return-object v0
.end method

.method public getTimeStamp()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/mediatek/datatransfer/modules/SmsRestoreComposer$SmsRestoreEntry;->timeStamp:Ljava/lang/String;

    return-object v0
.end method

.method public setBody(Ljava/lang/String;)V
    .locals 0
    .param p1    # Ljava/lang/String;

    iput-object p1, p0, Lcom/mediatek/datatransfer/modules/SmsRestoreComposer$SmsRestoreEntry;->body:Ljava/lang/String;

    return-void
.end method

.method public setBoxType(Ljava/lang/String;)V
    .locals 0
    .param p1    # Ljava/lang/String;

    iput-object p1, p0, Lcom/mediatek/datatransfer/modules/SmsRestoreComposer$SmsRestoreEntry;->boxType:Ljava/lang/String;

    return-void
.end method

.method public setLocked(Ljava/lang/String;)V
    .locals 0
    .param p1    # Ljava/lang/String;

    iput-object p1, p0, Lcom/mediatek/datatransfer/modules/SmsRestoreComposer$SmsRestoreEntry;->locked:Ljava/lang/String;

    return-void
.end method

.method public setReadByte(Ljava/lang/String;)V
    .locals 0
    .param p1    # Ljava/lang/String;

    iput-object p1, p0, Lcom/mediatek/datatransfer/modules/SmsRestoreComposer$SmsRestoreEntry;->readByte:Ljava/lang/String;

    return-void
.end method

.method public setSeen(Ljava/lang/String;)V
    .locals 0
    .param p1    # Ljava/lang/String;

    iput-object p1, p0, Lcom/mediatek/datatransfer/modules/SmsRestoreComposer$SmsRestoreEntry;->seen:Ljava/lang/String;

    return-void
.end method

.method public setSimCardid(Ljava/lang/String;)V
    .locals 0
    .param p1    # Ljava/lang/String;

    iput-object p1, p0, Lcom/mediatek/datatransfer/modules/SmsRestoreComposer$SmsRestoreEntry;->simCardid:Ljava/lang/String;

    return-void
.end method

.method public setSmsAddress(Ljava/lang/String;)V
    .locals 0
    .param p1    # Ljava/lang/String;

    iput-object p1, p0, Lcom/mediatek/datatransfer/modules/SmsRestoreComposer$SmsRestoreEntry;->smsAddress:Ljava/lang/String;

    return-void
.end method

.method public setTimeStamp(Ljava/lang/String;)V
    .locals 0
    .param p1    # Ljava/lang/String;

    iput-object p1, p0, Lcom/mediatek/datatransfer/modules/SmsRestoreComposer$SmsRestoreEntry;->timeStamp:Ljava/lang/String;

    return-void
.end method
