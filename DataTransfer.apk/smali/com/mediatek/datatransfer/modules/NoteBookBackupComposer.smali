.class public Lcom/mediatek/datatransfer/modules/NoteBookBackupComposer;
.super Lcom/mediatek/datatransfer/modules/Composer;
.source "NoteBookBackupComposer.java"


# static fields
.field private static final CLASS_TAG:Ljava/lang/String; = "DataTransfer/NoteBookBackupComposer"

.field private static final COLUMN_NAME_CREATED:Ljava/lang/String; = "created"

.field private static final COLUMN_NAME_GROUP:Ljava/lang/String; = "notegroup"

.field private static final COLUMN_NAME_MODIFIED:Ljava/lang/String; = "modified"

.field private static final COLUMN_NAME_NOTE:Ljava/lang/String; = "note"

.field private static final COLUMN_NAME_TITLE:Ljava/lang/String; = "title"


# instance fields
.field private mCursor:Landroid/database/Cursor;

.field private mXmlComposer:Lcom/mediatek/datatransfer/modules/NoteBookXmlComposer;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0
    .param p1    # Landroid/content/Context;

    invoke-direct {p0, p1}, Lcom/mediatek/datatransfer/modules/Composer;-><init>(Landroid/content/Context;)V

    return-void
.end method

.method private writeToFile(Ljava/lang/String;)V
    .locals 5
    .param p1    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    :try_start_0
    new-instance v2, Ljava/io/FileOutputStream;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v4, p0, Lcom/mediatek/datatransfer/modules/Composer;->mParentFolderPath:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    sget-object v4, Ljava/io/File;->separator:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "notebook"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    sget-object v4, Ljava/io/File;->separator:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "notebook.xml"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Ljava/io/FileOutputStream;-><init>(Ljava/lang/String;)V

    invoke-virtual {p1}, Ljava/lang/String;->getBytes()[B

    move-result-object v0

    const/4 v3, 0x0

    array-length v4, v0

    invoke-virtual {v2, v0, v3, v4}, Ljava/io/FileOutputStream;->write([BII)V

    invoke-virtual {v2}, Ljava/io/OutputStream;->flush()V

    invoke-virtual {v2}, Ljava/io/FileOutputStream;->close()V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1

    :goto_0
    return-void

    :catch_0
    move-exception v1

    throw v1

    :catch_1
    move-exception v1

    invoke-virtual {v1}, Ljava/lang/Throwable;->printStackTrace()V

    goto :goto_0
.end method


# virtual methods
.method public final getCount()I
    .locals 4

    const/4 v0, 0x0

    iget-object v1, p0, Lcom/mediatek/datatransfer/modules/NoteBookBackupComposer;->mCursor:Landroid/database/Cursor;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/mediatek/datatransfer/modules/NoteBookBackupComposer;->mCursor:Landroid/database/Cursor;

    invoke-interface {v1}, Landroid/database/Cursor;->isClosed()Z

    move-result v1

    if-nez v1, :cond_0

    iget-object v1, p0, Lcom/mediatek/datatransfer/modules/NoteBookBackupComposer;->mCursor:Landroid/database/Cursor;

    invoke-interface {v1}, Landroid/database/Cursor;->getCount()I

    move-result v0

    :cond_0
    const-string v1, "DataTransfer/NoteBookBackupComposer"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "getCount():"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/mediatek/datatransfer/utils/MyLogger;->logD(Ljava/lang/String;Ljava/lang/String;)V

    return v0
.end method

.method public final getModuleType()I
    .locals 1

    const/16 v0, 0x100

    return v0
.end method

.method public final implementComposeOneEntity()Z
    .locals 11

    const/4 v7, 0x0

    iget-object v8, p0, Lcom/mediatek/datatransfer/modules/NoteBookBackupComposer;->mCursor:Landroid/database/Cursor;

    if-eqz v8, :cond_1

    iget-object v8, p0, Lcom/mediatek/datatransfer/modules/NoteBookBackupComposer;->mCursor:Landroid/database/Cursor;

    invoke-interface {v8}, Landroid/database/Cursor;->isAfterLast()Z

    move-result v8

    if-nez v8, :cond_1

    iget-object v8, p0, Lcom/mediatek/datatransfer/modules/NoteBookBackupComposer;->mXmlComposer:Lcom/mediatek/datatransfer/modules/NoteBookXmlComposer;

    if-eqz v8, :cond_0

    :try_start_0
    iget-object v8, p0, Lcom/mediatek/datatransfer/modules/NoteBookBackupComposer;->mCursor:Landroid/database/Cursor;

    iget-object v9, p0, Lcom/mediatek/datatransfer/modules/NoteBookBackupComposer;->mCursor:Landroid/database/Cursor;

    const-string v10, "title"

    invoke-interface {v9, v10}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v9

    invoke-interface {v8, v9}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v1

    iget-object v8, p0, Lcom/mediatek/datatransfer/modules/NoteBookBackupComposer;->mCursor:Landroid/database/Cursor;

    iget-object v9, p0, Lcom/mediatek/datatransfer/modules/NoteBookBackupComposer;->mCursor:Landroid/database/Cursor;

    const-string v10, "note"

    invoke-interface {v9, v10}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v9

    invoke-interface {v8, v9}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v2

    iget-object v8, p0, Lcom/mediatek/datatransfer/modules/NoteBookBackupComposer;->mCursor:Landroid/database/Cursor;

    iget-object v9, p0, Lcom/mediatek/datatransfer/modules/NoteBookBackupComposer;->mCursor:Landroid/database/Cursor;

    const-string v10, "created"

    invoke-interface {v9, v10}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v9

    invoke-interface {v8, v9}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v3

    iget-object v8, p0, Lcom/mediatek/datatransfer/modules/NoteBookBackupComposer;->mCursor:Landroid/database/Cursor;

    iget-object v9, p0, Lcom/mediatek/datatransfer/modules/NoteBookBackupComposer;->mCursor:Landroid/database/Cursor;

    const-string v10, "modified"

    invoke-interface {v9, v10}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v9

    invoke-interface {v8, v9}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v4

    iget-object v8, p0, Lcom/mediatek/datatransfer/modules/NoteBookBackupComposer;->mCursor:Landroid/database/Cursor;

    iget-object v9, p0, Lcom/mediatek/datatransfer/modules/NoteBookBackupComposer;->mCursor:Landroid/database/Cursor;

    const-string v10, "notegroup"

    invoke-interface {v9, v10}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v9

    invoke-interface {v8, v9}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v5

    new-instance v0, Lcom/mediatek/datatransfer/modules/NoteBookXmlInfo;

    invoke-direct/range {v0 .. v5}, Lcom/mediatek/datatransfer/modules/NoteBookXmlInfo;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    iget-object v8, p0, Lcom/mediatek/datatransfer/modules/NoteBookBackupComposer;->mXmlComposer:Lcom/mediatek/datatransfer/modules/NoteBookXmlComposer;

    invoke-virtual {v8, v0}, Lcom/mediatek/datatransfer/modules/NoteBookXmlComposer;->addOneMmsRecord(Lcom/mediatek/datatransfer/modules/NoteBookXmlInfo;)Z
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0

    const/4 v7, 0x1

    :cond_0
    :goto_0
    iget-object v8, p0, Lcom/mediatek/datatransfer/modules/NoteBookBackupComposer;->mCursor:Landroid/database/Cursor;

    invoke-interface {v8}, Landroid/database/Cursor;->moveToNext()Z

    :cond_1
    return v7

    :catch_0
    move-exception v6

    invoke-virtual {v6}, Ljava/lang/Throwable;->printStackTrace()V

    goto :goto_0
.end method

.method public final init()Z
    .locals 7

    const/4 v2, 0x0

    const/4 v6, 0x0

    const-string v0, "content://com.mediatek.notebook.NotePad/notes"

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    iget-object v0, p0, Lcom/mediatek/datatransfer/modules/Composer;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    move-object v3, v2

    move-object v4, v2

    move-object v5, v2

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v0

    iput-object v0, p0, Lcom/mediatek/datatransfer/modules/NoteBookBackupComposer;->mCursor:Landroid/database/Cursor;

    iget-object v0, p0, Lcom/mediatek/datatransfer/modules/NoteBookBackupComposer;->mCursor:Landroid/database/Cursor;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/mediatek/datatransfer/modules/NoteBookBackupComposer;->mCursor:Landroid/database/Cursor;

    invoke-interface {v0}, Landroid/database/Cursor;->moveToFirst()Z

    const/4 v6, 0x1

    :cond_0
    const-string v2, "DataTransfer/NoteBookBackupComposer"

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "init():"

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v6}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v3, ",count::"

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v0, p0, Lcom/mediatek/datatransfer/modules/NoteBookBackupComposer;->mCursor:Landroid/database/Cursor;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/mediatek/datatransfer/modules/NoteBookBackupComposer;->mCursor:Landroid/database/Cursor;

    invoke-interface {v0}, Landroid/database/Cursor;->getCount()I

    move-result v0

    :goto_0
    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v2, v0}, Lcom/mediatek/datatransfer/utils/MyLogger;->logD(Ljava/lang/String;Ljava/lang/String;)V

    return v6

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final isAfterLast()Z
    .locals 4

    const/4 v0, 0x1

    iget-object v1, p0, Lcom/mediatek/datatransfer/modules/NoteBookBackupComposer;->mCursor:Landroid/database/Cursor;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/mediatek/datatransfer/modules/NoteBookBackupComposer;->mCursor:Landroid/database/Cursor;

    invoke-interface {v1}, Landroid/database/Cursor;->isAfterLast()Z

    move-result v0

    :cond_0
    const-string v1, "DataTransfer/NoteBookBackupComposer"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "isAfterLast():"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/mediatek/datatransfer/utils/MyLogger;->logD(Ljava/lang/String;Ljava/lang/String;)V

    return v0
.end method

.method public onEnd()V
    .locals 3

    invoke-super {p0}, Lcom/mediatek/datatransfer/modules/Composer;->onEnd()V

    iget-object v2, p0, Lcom/mediatek/datatransfer/modules/NoteBookBackupComposer;->mXmlComposer:Lcom/mediatek/datatransfer/modules/NoteBookXmlComposer;

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/mediatek/datatransfer/modules/NoteBookBackupComposer;->mXmlComposer:Lcom/mediatek/datatransfer/modules/NoteBookXmlComposer;

    invoke-virtual {v2}, Lcom/mediatek/datatransfer/modules/NoteBookXmlComposer;->endCompose()Z

    iget-object v2, p0, Lcom/mediatek/datatransfer/modules/NoteBookBackupComposer;->mXmlComposer:Lcom/mediatek/datatransfer/modules/NoteBookXmlComposer;

    invoke-virtual {v2}, Lcom/mediatek/datatransfer/modules/NoteBookXmlComposer;->getXmlInfo()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0}, Lcom/mediatek/datatransfer/modules/Composer;->getComposed()I

    move-result v2

    if-lez v2, :cond_0

    if-eqz v1, :cond_0

    :try_start_0
    invoke-direct {p0, v1}, Lcom/mediatek/datatransfer/modules/NoteBookBackupComposer;->writeToFile(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    :cond_0
    :goto_0
    iget-object v2, p0, Lcom/mediatek/datatransfer/modules/NoteBookBackupComposer;->mCursor:Landroid/database/Cursor;

    if-eqz v2, :cond_1

    iget-object v2, p0, Lcom/mediatek/datatransfer/modules/NoteBookBackupComposer;->mCursor:Landroid/database/Cursor;

    invoke-interface {v2}, Landroid/database/Cursor;->close()V

    const/4 v2, 0x0

    iput-object v2, p0, Lcom/mediatek/datatransfer/modules/NoteBookBackupComposer;->mCursor:Landroid/database/Cursor;

    :cond_1
    return-void

    :catch_0
    move-exception v0

    iget-object v2, p0, Lcom/mediatek/datatransfer/modules/Composer;->mReporter:Lcom/mediatek/datatransfer/ProgressReporter;

    if-eqz v2, :cond_2

    iget-object v2, p0, Lcom/mediatek/datatransfer/modules/Composer;->mReporter:Lcom/mediatek/datatransfer/ProgressReporter;

    invoke-interface {v2, v0}, Lcom/mediatek/datatransfer/ProgressReporter;->onErr(Ljava/io/IOException;)V

    :cond_2
    invoke-virtual {v0}, Ljava/lang/Throwable;->printStackTrace()V

    goto :goto_0
.end method

.method public onStart()V
    .locals 8

    invoke-super {p0}, Lcom/mediatek/datatransfer/modules/Composer;->onStart()V

    invoke-virtual {p0}, Lcom/mediatek/datatransfer/modules/NoteBookBackupComposer;->getCount()I

    move-result v6

    if-lez v6, :cond_3

    new-instance v5, Ljava/io/File;

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v7, p0, Lcom/mediatek/datatransfer/modules/Composer;->mParentFolderPath:Ljava/lang/String;

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    sget-object v7, Ljava/io/File;->separator:Ljava/lang/String;

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, "notebook"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-direct {v5, v6}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v5}, Ljava/io/File;->exists()Z

    move-result v6

    if-eqz v6, :cond_1

    invoke-virtual {v5}, Ljava/io/File;->listFiles()[Ljava/io/File;

    move-result-object v2

    move-object v0, v2

    array-length v4, v0

    const/4 v3, 0x0

    :goto_0
    if-ge v3, v4, :cond_2

    aget-object v1, v0, v3

    invoke-virtual {v1}, Ljava/io/File;->isFile()Z

    move-result v6

    if-eqz v6, :cond_0

    invoke-virtual {v1}, Ljava/io/File;->delete()Z

    :cond_0
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    :cond_1
    invoke-virtual {v5}, Ljava/io/File;->mkdirs()Z

    :cond_2
    new-instance v6, Lcom/mediatek/datatransfer/modules/NoteBookXmlComposer;

    invoke-direct {v6}, Lcom/mediatek/datatransfer/modules/NoteBookXmlComposer;-><init>()V

    iput-object v6, p0, Lcom/mediatek/datatransfer/modules/NoteBookBackupComposer;->mXmlComposer:Lcom/mediatek/datatransfer/modules/NoteBookXmlComposer;

    if-eqz v6, :cond_3

    iget-object v6, p0, Lcom/mediatek/datatransfer/modules/NoteBookBackupComposer;->mXmlComposer:Lcom/mediatek/datatransfer/modules/NoteBookXmlComposer;

    invoke-virtual {v6}, Lcom/mediatek/datatransfer/modules/NoteBookXmlComposer;->startCompose()Z

    :cond_3
    return-void
.end method
