.class public Lcom/mediatek/datatransfer/modules/MmsRestoreComposer;
.super Lcom/mediatek/datatransfer/modules/Composer;
.source "MmsRestoreComposer.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/mediatek/datatransfer/modules/MmsRestoreComposer$1;,
        Lcom/mediatek/datatransfer/modules/MmsRestoreComposer$MmsRestoreContent;,
        Lcom/mediatek/datatransfer/modules/MmsRestoreComposer$MmsRestoreThread;
    }
.end annotation


# static fields
.field private static final CLASS_TAG:Ljava/lang/String; = "DataTransfer/MmsRestoreComposer"


# instance fields
.field private mIndex:I

.field private mLock:Ljava/lang/Object;

.field private mPduList:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/mediatek/datatransfer/modules/MmsRestoreComposer$MmsRestoreContent;",
            ">;"
        }
    .end annotation
.end field

.field private mRecordList:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/mediatek/datatransfer/modules/MmsXmlInfo;",
            ">;"
        }
    .end annotation
.end field

.field private mTime:J

.field private mTmpPduList:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/mediatek/datatransfer/modules/MmsRestoreComposer$MmsRestoreContent;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 2
    .param p1    # Landroid/content/Context;

    const/4 v1, 0x0

    invoke-direct {p0, p1}, Lcom/mediatek/datatransfer/modules/Composer;-><init>(Landroid/content/Context;)V

    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lcom/mediatek/datatransfer/modules/MmsRestoreComposer;->mLock:Ljava/lang/Object;

    iput-object v1, p0, Lcom/mediatek/datatransfer/modules/MmsRestoreComposer;->mPduList:Ljava/util/ArrayList;

    iput-object v1, p0, Lcom/mediatek/datatransfer/modules/MmsRestoreComposer;->mTmpPduList:Ljava/util/ArrayList;

    return-void
.end method

.method static synthetic access$200(Lcom/mediatek/datatransfer/modules/MmsRestoreComposer;)Ljava/util/ArrayList;
    .locals 1
    .param p0    # Lcom/mediatek/datatransfer/modules/MmsRestoreComposer;

    iget-object v0, p0, Lcom/mediatek/datatransfer/modules/MmsRestoreComposer;->mPduList:Ljava/util/ArrayList;

    return-object v0
.end method

.method static synthetic access$202(Lcom/mediatek/datatransfer/modules/MmsRestoreComposer;Ljava/util/ArrayList;)Ljava/util/ArrayList;
    .locals 0
    .param p0    # Lcom/mediatek/datatransfer/modules/MmsRestoreComposer;
    .param p1    # Ljava/util/ArrayList;

    iput-object p1, p0, Lcom/mediatek/datatransfer/modules/MmsRestoreComposer;->mPduList:Ljava/util/ArrayList;

    return-object p1
.end method

.method static synthetic access$300(Lcom/mediatek/datatransfer/modules/MmsRestoreComposer;)Ljava/lang/Object;
    .locals 1
    .param p0    # Lcom/mediatek/datatransfer/modules/MmsRestoreComposer;

    iget-object v0, p0, Lcom/mediatek/datatransfer/modules/MmsRestoreComposer;->mLock:Ljava/lang/Object;

    return-object v0
.end method

.method private getMsgBoxUri(Ljava/lang/String;)Landroid/net/Uri;
    .locals 1
    .param p1    # Ljava/lang/String;

    const-string v0, "1"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    sget-object v0, Landroid/provider/Telephony$Mms$Inbox;->CONTENT_URI:Landroid/net/Uri;

    :goto_0
    return-object v0

    :cond_0
    const-string v0, "2"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    sget-object v0, Landroid/provider/Telephony$Mms$Sent;->CONTENT_URI:Landroid/net/Uri;

    goto :goto_0

    :cond_1
    const-string v0, "3"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    sget-object v0, Landroid/provider/Telephony$Mms$Draft;->CONTENT_URI:Landroid/net/Uri;

    goto :goto_0

    :cond_2
    const-string v0, "4"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    sget-object v0, Landroid/provider/Telephony$Mms$Outbox;->CONTENT_URI:Landroid/net/Uri;

    goto :goto_0

    :cond_3
    sget-object v0, Landroid/provider/Telephony$Mms$Inbox;->CONTENT_URI:Landroid/net/Uri;

    goto :goto_0
.end method

.method private getXmlInfo(Ljava/lang/String;)Ljava/lang/String;
    .locals 8
    .param p1    # Ljava/lang/String;

    const/4 v3, 0x0

    :try_start_0
    new-instance v4, Ljava/io/FileInputStream;

    invoke-direct {v4, p1}, Ljava/io/FileInputStream;-><init>(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_a
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_3
    .catch Ljava/lang/IndexOutOfBoundsException; {:try_start_0 .. :try_end_0} :catch_5
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :try_start_1
    new-instance v0, Ljava/io/ByteArrayOutputStream;

    invoke-direct {v0}, Ljava/io/ByteArrayOutputStream;-><init>()V

    const/4 v5, -0x1

    const/16 v6, 0x200

    new-array v1, v6, [B

    :goto_0
    const/4 v6, 0x0

    const/16 v7, 0x200

    invoke-virtual {v4, v1, v6, v7}, Ljava/io/FileInputStream;->read([BII)I

    move-result v5

    const/4 v6, -0x1

    if-eq v5, v6, :cond_1

    const/4 v6, 0x0

    invoke-virtual {v0, v1, v6, v5}, Ljava/io/ByteArrayOutputStream;->write([BII)V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Ljava/lang/NullPointerException; {:try_start_1 .. :try_end_1} :catch_9
    .catch Ljava/lang/IndexOutOfBoundsException; {:try_start_1 .. :try_end_1} :catch_8
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    goto :goto_0

    :catch_0
    move-exception v2

    move-object v3, v4

    :goto_1
    :try_start_2
    invoke-virtual {v2}, Ljava/lang/Throwable;->printStackTrace()V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    if-eqz v3, :cond_0

    :try_start_3
    invoke-virtual {v3}, Ljava/io/FileInputStream;->close()V
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_2

    :cond_0
    :goto_2
    const/4 v6, 0x0

    :goto_3
    return-object v6

    :cond_1
    :try_start_4
    invoke-virtual {v0}, Ljava/io/ByteArrayOutputStream;->toString()Ljava/lang/String;
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_0
    .catch Ljava/lang/NullPointerException; {:try_start_4 .. :try_end_4} :catch_9
    .catch Ljava/lang/IndexOutOfBoundsException; {:try_start_4 .. :try_end_4} :catch_8
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    move-result-object v6

    if-eqz v4, :cond_2

    :try_start_5
    invoke-virtual {v4}, Ljava/io/FileInputStream;->close()V
    :try_end_5
    .catch Ljava/io/IOException; {:try_start_5 .. :try_end_5} :catch_1

    :cond_2
    :goto_4
    move-object v3, v4

    goto :goto_3

    :catch_1
    move-exception v2

    invoke-virtual {v2}, Ljava/lang/Throwable;->printStackTrace()V

    goto :goto_4

    :catch_2
    move-exception v2

    invoke-virtual {v2}, Ljava/lang/Throwable;->printStackTrace()V

    goto :goto_2

    :catch_3
    move-exception v2

    :goto_5
    :try_start_6
    invoke-virtual {v2}, Ljava/lang/Throwable;->printStackTrace()V
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_0

    if-eqz v3, :cond_0

    :try_start_7
    invoke-virtual {v3}, Ljava/io/FileInputStream;->close()V
    :try_end_7
    .catch Ljava/io/IOException; {:try_start_7 .. :try_end_7} :catch_4

    goto :goto_2

    :catch_4
    move-exception v2

    invoke-virtual {v2}, Ljava/lang/Throwable;->printStackTrace()V

    goto :goto_2

    :catch_5
    move-exception v2

    :goto_6
    :try_start_8
    invoke-virtual {v2}, Ljava/lang/Throwable;->printStackTrace()V
    :try_end_8
    .catchall {:try_start_8 .. :try_end_8} :catchall_0

    if-eqz v3, :cond_0

    :try_start_9
    invoke-virtual {v3}, Ljava/io/FileInputStream;->close()V
    :try_end_9
    .catch Ljava/io/IOException; {:try_start_9 .. :try_end_9} :catch_6

    goto :goto_2

    :catch_6
    move-exception v2

    invoke-virtual {v2}, Ljava/lang/Throwable;->printStackTrace()V

    goto :goto_2

    :catchall_0
    move-exception v6

    :goto_7
    if-eqz v3, :cond_3

    :try_start_a
    invoke-virtual {v3}, Ljava/io/FileInputStream;->close()V
    :try_end_a
    .catch Ljava/io/IOException; {:try_start_a .. :try_end_a} :catch_7

    :cond_3
    :goto_8
    throw v6

    :catch_7
    move-exception v2

    invoke-virtual {v2}, Ljava/lang/Throwable;->printStackTrace()V

    goto :goto_8

    :catchall_1
    move-exception v6

    move-object v3, v4

    goto :goto_7

    :catch_8
    move-exception v2

    move-object v3, v4

    goto :goto_6

    :catch_9
    move-exception v2

    move-object v3, v4

    goto :goto_5

    :catch_a
    move-exception v2

    goto :goto_1
.end method

.method private readFileContent(Ljava/lang/String;)[B
    .locals 7
    .param p1    # Ljava/lang/String;

    :try_start_0
    new-instance v3, Ljava/io/FileInputStream;

    invoke-direct {v3, p1}, Ljava/io/FileInputStream;-><init>(Ljava/lang/String;)V

    new-instance v0, Ljava/io/ByteArrayOutputStream;

    invoke-direct {v0}, Ljava/io/ByteArrayOutputStream;-><init>()V

    const/4 v4, -0x1

    const/16 v5, 0x200

    new-array v1, v5, [B

    :goto_0
    const/4 v5, 0x0

    const/16 v6, 0x200

    invoke-virtual {v3, v1, v5, v6}, Ljava/io/FileInputStream;->read([BII)I

    move-result v4

    const/4 v5, -0x1

    if-eq v4, v5, :cond_0

    const/4 v5, 0x0

    invoke-virtual {v0, v1, v5, v4}, Ljava/io/ByteArrayOutputStream;->write([BII)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/IndexOutOfBoundsException; {:try_start_0 .. :try_end_0} :catch_2

    goto :goto_0

    :catch_0
    move-exception v2

    invoke-virtual {v2}, Ljava/lang/Throwable;->printStackTrace()V

    :goto_1
    const/4 v5, 0x0

    :goto_2
    return-object v5

    :cond_0
    :try_start_1
    invoke-virtual {v3}, Ljava/io/FileInputStream;->close()V

    invoke-virtual {v0}, Ljava/io/ByteArrayOutputStream;->toByteArray()[B
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Ljava/lang/NullPointerException; {:try_start_1 .. :try_end_1} :catch_1
    .catch Ljava/lang/IndexOutOfBoundsException; {:try_start_1 .. :try_end_1} :catch_2

    move-result-object v5

    goto :goto_2

    :catch_1
    move-exception v2

    invoke-virtual {v2}, Ljava/lang/Throwable;->printStackTrace()V

    goto :goto_1

    :catch_2
    move-exception v2

    invoke-virtual {v2}, Ljava/lang/Throwable;->printStackTrace()V

    goto :goto_1
.end method


# virtual methods
.method public composeOneEntity()Z
    .locals 1

    invoke-virtual {p0}, Lcom/mediatek/datatransfer/modules/MmsRestoreComposer;->implementComposeOneEntity()Z

    move-result v0

    return v0
.end method

.method public getCount()I
    .locals 4

    const/4 v0, 0x0

    iget-object v1, p0, Lcom/mediatek/datatransfer/modules/MmsRestoreComposer;->mRecordList:Ljava/util/ArrayList;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/mediatek/datatransfer/modules/MmsRestoreComposer;->mRecordList:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v0

    :cond_0
    const-string v1, "DataTransfer/MmsRestoreComposer"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "getCount():"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/mediatek/datatransfer/utils/MyLogger;->logD(Ljava/lang/String;Ljava/lang/String;)V

    return v0
.end method

.method public getModuleType()I
    .locals 1

    const/4 v0, 0x4

    return v0
.end method

.method public implementComposeOneEntity()Z
    .locals 17

    const/4 v8, 0x0

    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/mediatek/datatransfer/modules/MmsRestoreComposer;->mRecordList:Ljava/util/ArrayList;

    if-eqz v13, :cond_5

    move-object/from16 v0, p0

    iget v13, v0, Lcom/mediatek/datatransfer/modules/MmsRestoreComposer;->mIndex:I

    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/mediatek/datatransfer/modules/MmsRestoreComposer;->mRecordList:Ljava/util/ArrayList;

    invoke-virtual {v14}, Ljava/util/ArrayList;->size()I

    move-result v14

    if-ge v13, v14, :cond_5

    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/mediatek/datatransfer/modules/MmsRestoreComposer;->mRecordList:Ljava/util/ArrayList;

    move-object/from16 v0, p0

    iget v14, v0, Lcom/mediatek/datatransfer/modules/MmsRestoreComposer;->mIndex:I

    add-int/lit8 v15, v14, 0x1

    move-object/from16 v0, p0

    iput v15, v0, Lcom/mediatek/datatransfer/modules/MmsRestoreComposer;->mIndex:I

    invoke-virtual {v13, v14}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Lcom/mediatek/datatransfer/modules/MmsXmlInfo;

    invoke-virtual {v7}, Lcom/mediatek/datatransfer/modules/MmsXmlInfo;->getMsgBox()Ljava/lang/String;

    move-result-object v4

    move-object/from16 v0, p0

    invoke-direct {v0, v4}, Lcom/mediatek/datatransfer/modules/MmsRestoreComposer;->getMsgBoxUri(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v3

    const-string v10, "-1"

    invoke-virtual {v7}, Lcom/mediatek/datatransfer/modules/MmsXmlInfo;->getSimId()Ljava/lang/String;

    move-result-object v13

    invoke-static {v13}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v13

    add-int/lit8 v11, v13, -0x1

    if-gez v11, :cond_6

    const-string v10, "-1"

    :goto_0
    const-string v13, "DataTransfer/MmsRestoreComposer"

    new-instance v14, Ljava/lang/StringBuilder;

    invoke-direct {v14}, Ljava/lang/StringBuilder;-><init>()V

    const-string v15, "mIdx:"

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    move-object/from16 v0, p0

    iget v15, v0, Lcom/mediatek/datatransfer/modules/MmsRestoreComposer;->mIndex:I

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v14

    const-string v15, ",mMsgUri:"

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    invoke-virtual {v3}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v15

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    const-string v15, ",simId:"

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    invoke-virtual {v14, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    invoke-virtual {v14}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v14

    invoke-static {v13, v14}, Lcom/mediatek/datatransfer/utils/MyLogger;->logD(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v7}, Lcom/mediatek/datatransfer/modules/MmsXmlInfo;->getID()Ljava/lang/String;

    move-result-object v6

    new-instance v13, Ljava/lang/StringBuilder;

    invoke-direct {v13}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/mediatek/datatransfer/modules/Composer;->mParentFolderPath:Ljava/lang/String;

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    sget-object v14, Ljava/io/File;->separator:Ljava/lang/String;

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    const-string v14, "mms"

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    sget-object v14, Ljava/io/File;->separator:Ljava/lang/String;

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v13, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v13}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    const-string v13, "DataTransfer/MmsRestoreComposer"

    new-instance v14, Ljava/lang/StringBuilder;

    invoke-direct {v14}, Ljava/lang/StringBuilder;-><init>()V

    const-string v15, "fileName:"

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    invoke-virtual {v14, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    invoke-virtual {v14}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v14

    invoke-static {v13, v14}, Lcom/mediatek/datatransfer/utils/MyLogger;->logD(Ljava/lang/String;Ljava/lang/String;)V

    move-object/from16 v0, p0

    invoke-direct {v0, v2}, Lcom/mediatek/datatransfer/modules/MmsRestoreComposer;->readFileContent(Ljava/lang/String;)[B

    move-result-object v5

    if-eqz v5, :cond_0

    const/4 v8, 0x1

    :cond_0
    if-eqz v8, :cond_2

    const-string v13, "DataTransfer/MmsRestoreComposer"

    new-instance v14, Ljava/lang/StringBuilder;

    invoke-direct {v14}, Ljava/lang/StringBuilder;-><init>()V

    const-string v15, "readFileContent finish, result:"

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    invoke-virtual {v14, v8}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v14

    invoke-virtual {v14}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v14

    invoke-static {v13, v14}, Lcom/mediatek/datatransfer/utils/MyLogger;->logD(Ljava/lang/String;Ljava/lang/String;)V

    const-string v13, "DataTransfer/MmsRestoreComposer"

    const-string v14, "Mms: MmsRestoreThread parse begin"

    invoke-static {v13, v14}, Lcom/mediatek/datatransfer/utils/MyLogger;->logD(Ljava/lang/String;Ljava/lang/String;)V

    new-instance v12, Lcom/mediatek/datatransfer/modules/MmsRestoreComposer$MmsRestoreContent;

    const/4 v13, 0x0

    move-object/from16 v0, p0

    invoke-direct {v12, v0, v13}, Lcom/mediatek/datatransfer/modules/MmsRestoreComposer$MmsRestoreContent;-><init>(Lcom/mediatek/datatransfer/modules/MmsRestoreComposer;Lcom/mediatek/datatransfer/modules/MmsRestoreComposer$1;)V

    iput-object v3, v12, Lcom/mediatek/datatransfer/modules/MmsRestoreComposer$MmsRestoreContent;->mMsgUri:Landroid/net/Uri;

    iget-object v13, v12, Lcom/mediatek/datatransfer/modules/MmsRestoreComposer$MmsRestoreContent;->mMsgInfo:Ljava/util/HashMap;

    const-string v14, "locked"

    invoke-virtual {v7}, Lcom/mediatek/datatransfer/modules/MmsXmlInfo;->getIsLocked()Ljava/lang/String;

    move-result-object v15

    invoke-virtual {v13, v14, v15}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v13, v12, Lcom/mediatek/datatransfer/modules/MmsRestoreComposer$MmsRestoreContent;->mMsgInfo:Ljava/util/HashMap;

    const-string v14, "read"

    invoke-virtual {v7}, Lcom/mediatek/datatransfer/modules/MmsXmlInfo;->getIsRead()Ljava/lang/String;

    move-result-object v15

    invoke-virtual {v13, v14, v15}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v13, v12, Lcom/mediatek/datatransfer/modules/MmsRestoreComposer$MmsRestoreContent;->mMsgInfo:Ljava/util/HashMap;

    const-string v14, "sim_id"

    invoke-virtual {v13, v14, v10}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-object/from16 v0, p0

    iget v13, v0, Lcom/mediatek/datatransfer/modules/MmsRestoreComposer;->mIndex:I

    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/mediatek/datatransfer/modules/MmsRestoreComposer;->mRecordList:Ljava/util/ArrayList;

    invoke-virtual {v14}, Ljava/util/ArrayList;->size()I

    move-result v14

    if-ne v13, v14, :cond_8

    iget-object v13, v12, Lcom/mediatek/datatransfer/modules/MmsRestoreComposer$MmsRestoreContent;->mMsgInfo:Ljava/util/HashMap;

    const-string v14, "index"

    const-string v15, "0"

    invoke-virtual {v13, v14, v15}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const-string v13, "DataTransfer/MmsRestoreComposer"

    const-string v14, "this is last mms"

    invoke-static {v13, v14}, Lcom/mediatek/datatransfer/utils/MyLogger;->logD(Ljava/lang/String;Ljava/lang/String;)V

    :goto_1
    sget-object v13, Landroid/provider/Telephony$Mms$Inbox;->CONTENT_URI:Landroid/net/Uri;

    if-ne v3, v13, :cond_a

    :try_start_0
    new-instance v13, Lcom/google/android/mms/pdu/PduParser;

    invoke-direct {v13, v5}, Lcom/google/android/mms/pdu/PduParser;-><init>([B)V

    const/4 v14, 0x1

    invoke-virtual {v13, v14}, Lcom/google/android/mms/pdu/PduParser;->parse(Z)Lcom/google/android/mms/pdu/GenericPdu;

    move-result-object v13

    check-cast v13, Lcom/google/android/mms/pdu/RetrieveConf;

    iput-object v13, v12, Lcom/mediatek/datatransfer/modules/MmsRestoreComposer$MmsRestoreContent;->mRetrieveConf:Lcom/google/android/mms/pdu/RetrieveConf;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    iget-object v13, v12, Lcom/mediatek/datatransfer/modules/MmsRestoreComposer$MmsRestoreContent;->mRetrieveConf:Lcom/google/android/mms/pdu/RetrieveConf;

    if-nez v13, :cond_1

    new-instance v13, Lcom/google/android/mms/pdu/PduParser;

    invoke-direct {v13, v5}, Lcom/google/android/mms/pdu/PduParser;-><init>([B)V

    const/4 v14, 0x1

    invoke-virtual {v13, v14}, Lcom/google/android/mms/pdu/PduParser;->parse(Z)Lcom/google/android/mms/pdu/GenericPdu;

    move-result-object v13

    check-cast v13, Lcom/google/android/mms/pdu/NotificationInd;

    iput-object v13, v12, Lcom/mediatek/datatransfer/modules/MmsRestoreComposer$MmsRestoreContent;->mIndConf:Lcom/google/android/mms/pdu/NotificationInd;

    :cond_1
    :goto_2
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/mediatek/datatransfer/modules/MmsRestoreComposer;->mTmpPduList:Ljava/util/ArrayList;

    invoke-virtual {v13, v12}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :cond_2
    move-object/from16 v0, p0

    iget v13, v0, Lcom/mediatek/datatransfer/modules/MmsRestoreComposer;->mIndex:I

    rem-int/lit8 v13, v13, 0x5

    if-eqz v13, :cond_3

    invoke-virtual/range {p0 .. p0}, Lcom/mediatek/datatransfer/modules/MmsRestoreComposer;->isAfterLast()Z

    move-result v13

    if-eqz v13, :cond_5

    :cond_3
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/mediatek/datatransfer/modules/MmsRestoreComposer;->mPduList:Ljava/util/ArrayList;

    if-eqz v13, :cond_4

    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/mediatek/datatransfer/modules/MmsRestoreComposer;->mLock:Ljava/lang/Object;

    monitor-enter v14

    :try_start_1
    const-string v13, "DataTransfer/MmsRestoreComposer"

    const-string v15, "Mms: wait for MmsRestoreThread"

    invoke-static {v13, v15}, Lcom/mediatek/datatransfer/utils/MyLogger;->logD(Ljava/lang/String;Ljava/lang/String;)V

    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/mediatek/datatransfer/modules/MmsRestoreComposer;->mLock:Ljava/lang/Object;

    invoke-virtual {v13}, Ljava/lang/Object;->wait()V
    :try_end_1
    .catch Ljava/lang/InterruptedException; {:try_start_1 .. :try_end_1} :catch_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    :goto_3
    :try_start_2
    monitor-exit v14
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    :cond_4
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/mediatek/datatransfer/modules/MmsRestoreComposer;->mTmpPduList:Ljava/util/ArrayList;

    move-object/from16 v0, p0

    iput-object v13, v0, Lcom/mediatek/datatransfer/modules/MmsRestoreComposer;->mPduList:Ljava/util/ArrayList;

    new-instance v13, Lcom/mediatek/datatransfer/modules/MmsRestoreComposer$MmsRestoreThread;

    const/4 v14, 0x0

    move-object/from16 v0, p0

    invoke-direct {v13, v0, v14}, Lcom/mediatek/datatransfer/modules/MmsRestoreComposer$MmsRestoreThread;-><init>(Lcom/mediatek/datatransfer/modules/MmsRestoreComposer;Lcom/mediatek/datatransfer/modules/MmsRestoreComposer$1;)V

    invoke-virtual {v13}, Ljava/lang/Thread;->start()V

    invoke-virtual/range {p0 .. p0}, Lcom/mediatek/datatransfer/modules/MmsRestoreComposer;->isAfterLast()Z

    move-result v13

    if-nez v13, :cond_5

    new-instance v13, Ljava/util/ArrayList;

    invoke-direct {v13}, Ljava/util/ArrayList;-><init>()V

    move-object/from16 v0, p0

    iput-object v13, v0, Lcom/mediatek/datatransfer/modules/MmsRestoreComposer;->mTmpPduList:Ljava/util/ArrayList;

    :cond_5
    return v8

    :cond_6
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/mediatek/datatransfer/modules/Composer;->mContext:Landroid/content/Context;

    invoke-static {v13, v11}, Lcom/mediatek/telephony/SimInfoManager;->getSimInfoBySlot(Landroid/content/Context;I)Lcom/mediatek/telephony/SimInfoManager$SimInfoRecord;

    move-result-object v9

    if-nez v9, :cond_7

    const-string v10, "-1"

    goto/16 :goto_0

    :cond_7
    iget-wide v13, v9, Lcom/mediatek/telephony/SimInfoManager$SimInfoRecord;->mSimInfoId:J

    invoke-static {v13, v14}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v10

    goto/16 :goto_0

    :cond_8
    iget-object v13, v12, Lcom/mediatek/datatransfer/modules/MmsRestoreComposer$MmsRestoreContent;->mMsgInfo:Ljava/util/HashMap;

    const-string v14, "index"

    new-instance v15, Ljava/lang/StringBuilder;

    invoke-direct {v15}, Ljava/lang/StringBuilder;-><init>()V

    const-string v16, ""

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    move-object/from16 v0, p0

    iget v0, v0, Lcom/mediatek/datatransfer/modules/MmsRestoreComposer;->mIndex:I

    move/from16 v16, v0

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v15

    invoke-virtual {v15}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v15

    invoke-virtual {v13, v14, v15}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const-string v13, "DataTransfer/MmsRestoreComposer"

    new-instance v14, Ljava/lang/StringBuilder;

    invoke-direct {v14}, Ljava/lang/StringBuilder;-><init>()V

    const-string v15, "this is tmpContent.mNumber="

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    move-object/from16 v0, p0

    iget v15, v0, Lcom/mediatek/datatransfer/modules/MmsRestoreComposer;->mIndex:I

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v14

    invoke-virtual {v14}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v14

    invoke-static {v13, v14}, Lcom/mediatek/datatransfer/utils/MyLogger;->logD(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_1

    :catch_0
    move-exception v1

    :try_start_3
    invoke-virtual {v1}, Ljava/lang/Throwable;->printStackTrace()V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    iget-object v13, v12, Lcom/mediatek/datatransfer/modules/MmsRestoreComposer$MmsRestoreContent;->mRetrieveConf:Lcom/google/android/mms/pdu/RetrieveConf;

    if-nez v13, :cond_1

    new-instance v13, Lcom/google/android/mms/pdu/PduParser;

    invoke-direct {v13, v5}, Lcom/google/android/mms/pdu/PduParser;-><init>([B)V

    const/4 v14, 0x1

    invoke-virtual {v13, v14}, Lcom/google/android/mms/pdu/PduParser;->parse(Z)Lcom/google/android/mms/pdu/GenericPdu;

    move-result-object v13

    check-cast v13, Lcom/google/android/mms/pdu/NotificationInd;

    iput-object v13, v12, Lcom/mediatek/datatransfer/modules/MmsRestoreComposer$MmsRestoreContent;->mIndConf:Lcom/google/android/mms/pdu/NotificationInd;

    goto/16 :goto_2

    :catchall_0
    move-exception v13

    move-object v14, v13

    iget-object v13, v12, Lcom/mediatek/datatransfer/modules/MmsRestoreComposer$MmsRestoreContent;->mRetrieveConf:Lcom/google/android/mms/pdu/RetrieveConf;

    if-nez v13, :cond_9

    new-instance v13, Lcom/google/android/mms/pdu/PduParser;

    invoke-direct {v13, v5}, Lcom/google/android/mms/pdu/PduParser;-><init>([B)V

    const/4 v15, 0x1

    invoke-virtual {v13, v15}, Lcom/google/android/mms/pdu/PduParser;->parse(Z)Lcom/google/android/mms/pdu/GenericPdu;

    move-result-object v13

    check-cast v13, Lcom/google/android/mms/pdu/NotificationInd;

    iput-object v13, v12, Lcom/mediatek/datatransfer/modules/MmsRestoreComposer$MmsRestoreContent;->mIndConf:Lcom/google/android/mms/pdu/NotificationInd;

    :cond_9
    throw v14

    :cond_a
    new-instance v13, Lcom/google/android/mms/pdu/PduParser;

    invoke-direct {v13, v5}, Lcom/google/android/mms/pdu/PduParser;-><init>([B)V

    const/4 v14, 0x1

    invoke-virtual {v13, v14}, Lcom/google/android/mms/pdu/PduParser;->parse(Z)Lcom/google/android/mms/pdu/GenericPdu;

    move-result-object v13

    check-cast v13, Lcom/google/android/mms/pdu/SendReq;

    iput-object v13, v12, Lcom/mediatek/datatransfer/modules/MmsRestoreComposer$MmsRestoreContent;->mSendConf:Lcom/google/android/mms/pdu/SendReq;

    goto/16 :goto_2

    :catch_1
    move-exception v1

    :try_start_4
    invoke-virtual {v1}, Ljava/lang/Throwable;->printStackTrace()V

    goto/16 :goto_3

    :catchall_1
    move-exception v13

    monitor-exit v14
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    throw v13
.end method

.method public init()Z
    .locals 6

    const/4 v2, 0x0

    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    iput-object v3, p0, Lcom/mediatek/datatransfer/modules/MmsRestoreComposer;->mTmpPduList:Ljava/util/ArrayList;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v4, p0, Lcom/mediatek/datatransfer/modules/Composer;->mParentFolderPath:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    sget-object v4, Ljava/io/File;->separator:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "mms"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    sget-object v4, Ljava/io/File;->separator:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "msg_box.xml"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    const-string v3, "DataTransfer/MmsRestoreComposer"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "init():path:"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/mediatek/datatransfer/utils/MyLogger;->logD(Ljava/lang/String;Ljava/lang/String;)V

    invoke-direct {p0, v1}, Lcom/mediatek/datatransfer/modules/MmsRestoreComposer;->getXmlInfo(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-static {v0}, Lcom/mediatek/datatransfer/modules/MmsXmlParser;->parse(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v3

    iput-object v3, p0, Lcom/mediatek/datatransfer/modules/MmsRestoreComposer;->mRecordList:Ljava/util/ArrayList;

    const/4 v2, 0x1

    :goto_0
    const/4 v3, 0x0

    iput v3, p0, Lcom/mediatek/datatransfer/modules/MmsRestoreComposer;->mIndex:I

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v3

    iput-wide v3, p0, Lcom/mediatek/datatransfer/modules/MmsRestoreComposer;->mTime:J

    const-string v3, "DataTransfer/MmsRestoreComposer"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "init():"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/mediatek/datatransfer/utils/MyLogger;->logD(Ljava/lang/String;Ljava/lang/String;)V

    return v2

    :cond_0
    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    iput-object v3, p0, Lcom/mediatek/datatransfer/modules/MmsRestoreComposer;->mRecordList:Ljava/util/ArrayList;

    goto :goto_0
.end method

.method public isAfterLast()Z
    .locals 4

    const/4 v0, 0x1

    iget-object v1, p0, Lcom/mediatek/datatransfer/modules/MmsRestoreComposer;->mRecordList:Ljava/util/ArrayList;

    if-eqz v1, :cond_0

    iget v1, p0, Lcom/mediatek/datatransfer/modules/MmsRestoreComposer;->mIndex:I

    iget-object v2, p0, Lcom/mediatek/datatransfer/modules/MmsRestoreComposer;->mRecordList:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v2

    if-lt v1, v2, :cond_1

    const/4 v0, 0x1

    :cond_0
    :goto_0
    const-string v1, "DataTransfer/MmsRestoreComposer"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "isAfterLast():"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/mediatek/datatransfer/utils/MyLogger;->logD(Ljava/lang/String;Ljava/lang/String;)V

    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public onEnd()V
    .locals 4

    iget-object v1, p0, Lcom/mediatek/datatransfer/modules/MmsRestoreComposer;->mPduList:Ljava/util/ArrayList;

    if-eqz v1, :cond_0

    iget-object v2, p0, Lcom/mediatek/datatransfer/modules/MmsRestoreComposer;->mLock:Ljava/lang/Object;

    monitor-enter v2

    :try_start_0
    const-string v1, "DataTransfer/MmsRestoreComposer"

    const-string v3, "Mms: wait for MmsRestoreThread"

    invoke-static {v1, v3}, Lcom/mediatek/datatransfer/utils/MyLogger;->logD(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v1, p0, Lcom/mediatek/datatransfer/modules/MmsRestoreComposer;->mLock:Ljava/lang/Object;

    invoke-virtual {v1}, Ljava/lang/Object;->wait()V
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :goto_0
    :try_start_1
    monitor-exit v2
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    :cond_0
    invoke-super {p0}, Lcom/mediatek/datatransfer/modules/Composer;->onEnd()V

    const-string v1, "DataTransfer/MmsRestoreComposer"

    const-string v2, "onEnd()"

    invoke-static {v1, v2}, Lcom/mediatek/datatransfer/utils/MyLogger;->logD(Ljava/lang/String;Ljava/lang/String;)V

    return-void

    :catch_0
    move-exception v0

    :try_start_2
    invoke-virtual {v0}, Ljava/lang/Throwable;->printStackTrace()V

    goto :goto_0

    :catchall_0
    move-exception v1

    monitor-exit v2
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    throw v1
.end method
