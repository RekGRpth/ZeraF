.class public Lcom/mediatek/datatransfer/modules/ContactBackupComposer;
.super Lcom/mediatek/datatransfer/modules/Composer;
.source "ContactBackupComposer.java"


# static fields
.field private static final CLASS_TAG:Ljava/lang/String; = "DataTransfer/ContactBackupComposer"


# instance fields
.field private mCount:I

.field private mIndex:I

.field mOutStream:Ljava/io/FileOutputStream;

.field private mVCardComposer:Lcom/android/vcard/VCardComposer;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0
    .param p1    # Landroid/content/Context;

    invoke-direct {p0, p1}, Lcom/mediatek/datatransfer/modules/Composer;-><init>(Landroid/content/Context;)V

    return-void
.end method

.method private getCondition()Ljava/lang/String;
    .locals 9

    iget-object v7, p0, Lcom/mediatek/datatransfer/modules/Composer;->mParams:Ljava/util/List;

    if-eqz v7, :cond_4

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iget-object v7, p0, Lcom/mediatek/datatransfer/modules/Composer;->mContext:Landroid/content/Context;

    invoke-static {v7}, Lcom/mediatek/telephony/SimInfoManager;->getInsertedSimInfoList(Landroid/content/Context;)Ljava/util/List;

    move-result-object v6

    if-eqz v6, :cond_1

    invoke-interface {v6}, Ljava/util/List;->size()I

    move-result v7

    if-lez v7, :cond_1

    invoke-interface {v6}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_0
    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v7

    if-eqz v7, :cond_1

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/mediatek/telephony/SimInfoManager$SimInfoRecord;

    iget-object v7, p0, Lcom/mediatek/datatransfer/modules/Composer;->mParams:Ljava/util/List;

    iget-object v8, v5, Lcom/mediatek/telephony/SimInfoManager$SimInfoRecord;->mDisplayName:Ljava/lang/String;

    invoke-interface {v7, v8}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_0

    iget-wide v7, v5, Lcom/mediatek/telephony/SimInfoManager$SimInfoRecord;->mSimInfoId:J

    invoke-static {v7, v8}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v7

    invoke-interface {v1, v7}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_1
    iget-object v7, p0, Lcom/mediatek/datatransfer/modules/Composer;->mParams:Ljava/util/List;

    const-string v8, "phone"

    invoke-interface {v7, v8}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_2

    const-string v7, "-1"

    invoke-interface {v1, v7}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :cond_2
    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v4

    if-lez v4, :cond_4

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "indicate_phone_or_sim_contact ="

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const/4 v7, 0x0

    invoke-interface {v1, v7}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Ljava/lang/String;

    invoke-virtual {v8, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v0, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const/4 v2, 0x1

    :goto_1
    if-ge v2, v4, :cond_3

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, " OR indicate_phone_or_sim_contact ="

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-interface {v1, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Ljava/lang/String;

    invoke-virtual {v8, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v0, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    :cond_3
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    :goto_2
    return-object v7

    :cond_4
    const/4 v7, 0x0

    goto :goto_2
.end method


# virtual methods
.method public getCount()I
    .locals 3

    const-string v0, "DataTransfer/ContactBackupComposer"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "getCount():"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/mediatek/datatransfer/modules/ContactBackupComposer;->mCount:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/mediatek/datatransfer/utils/MyLogger;->logD(Ljava/lang/String;Ljava/lang/String;)V

    iget v0, p0, Lcom/mediatek/datatransfer/modules/ContactBackupComposer;->mCount:I

    return v0
.end method

.method public getModuleType()I
    .locals 1

    const/4 v0, 0x1

    return v0
.end method

.method protected implementComposeOneEntity()Z
    .locals 7

    const/4 v2, 0x0

    iget-object v4, p0, Lcom/mediatek/datatransfer/modules/ContactBackupComposer;->mVCardComposer:Lcom/android/vcard/VCardComposer;

    if-eqz v4, :cond_0

    iget-object v4, p0, Lcom/mediatek/datatransfer/modules/ContactBackupComposer;->mVCardComposer:Lcom/android/vcard/VCardComposer;

    invoke-virtual {v4}, Lcom/android/vcard/VCardComposer;->isAfterLast()Z

    move-result v4

    if-nez v4, :cond_0

    iget-object v4, p0, Lcom/mediatek/datatransfer/modules/ContactBackupComposer;->mVCardComposer:Lcom/android/vcard/VCardComposer;

    invoke-virtual {v4}, Lcom/android/vcard/VCardComposer;->createOneEntry()Ljava/lang/String;

    move-result-object v3

    if-eqz v3, :cond_0

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v4

    if-lez v4, :cond_0

    iget-object v4, p0, Lcom/mediatek/datatransfer/modules/ContactBackupComposer;->mOutStream:Ljava/io/FileOutputStream;

    if-eqz v4, :cond_0

    :try_start_0
    invoke-virtual {v3}, Ljava/lang/String;->getBytes()[B

    move-result-object v0

    iget-object v4, p0, Lcom/mediatek/datatransfer/modules/ContactBackupComposer;->mOutStream:Ljava/io/FileOutputStream;

    const/4 v5, 0x0

    array-length v6, v0

    invoke-virtual {v4, v0, v5, v6}, Ljava/io/FileOutputStream;->write([BII)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1

    const/4 v2, 0x1

    :cond_0
    :goto_0
    const-string v4, "DataTransfer/ContactBackupComposer"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "add result:"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/mediatek/datatransfer/utils/MyLogger;->logD(Ljava/lang/String;Ljava/lang/String;)V

    return v2

    :catch_0
    move-exception v1

    iget-object v4, p0, Lcom/mediatek/datatransfer/modules/Composer;->mReporter:Lcom/mediatek/datatransfer/ProgressReporter;

    if-eqz v4, :cond_0

    iget-object v4, p0, Lcom/mediatek/datatransfer/modules/Composer;->mReporter:Lcom/mediatek/datatransfer/ProgressReporter;

    invoke-interface {v4, v1}, Lcom/mediatek/datatransfer/ProgressReporter;->onErr(Ljava/io/IOException;)V

    goto :goto_0

    :catch_1
    move-exception v4

    goto :goto_0
.end method

.method public init()Z
    .locals 7

    const/4 v6, 0x0

    const/4 v1, 0x0

    const/4 v2, 0x0

    iput v2, p0, Lcom/mediatek/datatransfer/modules/ContactBackupComposer;->mCount:I

    new-instance v2, Lcom/android/vcard/VCardComposer;

    iget-object v3, p0, Lcom/mediatek/datatransfer/modules/Composer;->mContext:Landroid/content/Context;

    const/high16 v4, -0x40000000

    const/4 v5, 0x1

    invoke-direct {v2, v3, v4, v5}, Lcom/android/vcard/VCardComposer;-><init>(Landroid/content/Context;IZ)V

    iput-object v2, p0, Lcom/mediatek/datatransfer/modules/ContactBackupComposer;->mVCardComposer:Lcom/android/vcard/VCardComposer;

    invoke-direct {p0}, Lcom/mediatek/datatransfer/modules/ContactBackupComposer;->getCondition()Ljava/lang/String;

    move-result-object v0

    iget-object v2, p0, Lcom/mediatek/datatransfer/modules/ContactBackupComposer;->mVCardComposer:Lcom/android/vcard/VCardComposer;

    invoke-virtual {v2, v0, v6}, Lcom/android/vcard/VCardComposer;->init(Ljava/lang/String;[Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_0

    const/4 v1, 0x1

    iget-object v2, p0, Lcom/mediatek/datatransfer/modules/ContactBackupComposer;->mVCardComposer:Lcom/android/vcard/VCardComposer;

    invoke-virtual {v2}, Lcom/android/vcard/VCardComposer;->getCount()I

    move-result v2

    iput v2, p0, Lcom/mediatek/datatransfer/modules/ContactBackupComposer;->mCount:I

    :goto_0
    const-string v2, "DataTransfer/ContactBackupComposer"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "init():"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ",count:"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget v4, p0, Lcom/mediatek/datatransfer/modules/ContactBackupComposer;->mCount:I

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/mediatek/datatransfer/utils/MyLogger;->logD(Ljava/lang/String;Ljava/lang/String;)V

    return v1

    :cond_0
    iput-object v6, p0, Lcom/mediatek/datatransfer/modules/ContactBackupComposer;->mVCardComposer:Lcom/android/vcard/VCardComposer;

    goto :goto_0
.end method

.method public isAfterLast()Z
    .locals 4

    const/4 v0, 0x1

    iget-object v1, p0, Lcom/mediatek/datatransfer/modules/ContactBackupComposer;->mVCardComposer:Lcom/android/vcard/VCardComposer;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/mediatek/datatransfer/modules/ContactBackupComposer;->mVCardComposer:Lcom/android/vcard/VCardComposer;

    invoke-virtual {v1}, Lcom/android/vcard/VCardComposer;->isAfterLast()Z

    move-result v0

    :cond_0
    const-string v1, "DataTransfer/ContactBackupComposer"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "isAfterLast():"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/mediatek/datatransfer/utils/MyLogger;->logD(Ljava/lang/String;Ljava/lang/String;)V

    return v0
.end method

.method public onEnd()V
    .locals 1

    invoke-super {p0}, Lcom/mediatek/datatransfer/modules/Composer;->onEnd()V

    iget-object v0, p0, Lcom/mediatek/datatransfer/modules/ContactBackupComposer;->mVCardComposer:Lcom/android/vcard/VCardComposer;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/mediatek/datatransfer/modules/ContactBackupComposer;->mVCardComposer:Lcom/android/vcard/VCardComposer;

    invoke-virtual {v0}, Lcom/android/vcard/VCardComposer;->terminate()V

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/mediatek/datatransfer/modules/ContactBackupComposer;->mVCardComposer:Lcom/android/vcard/VCardComposer;

    :cond_0
    iget-object v0, p0, Lcom/mediatek/datatransfer/modules/ContactBackupComposer;->mOutStream:Ljava/io/FileOutputStream;

    if-eqz v0, :cond_1

    :try_start_0
    iget-object v0, p0, Lcom/mediatek/datatransfer/modules/ContactBackupComposer;->mOutStream:Ljava/io/FileOutputStream;

    invoke-virtual {v0}, Ljava/io/OutputStream;->flush()V

    iget-object v0, p0, Lcom/mediatek/datatransfer/modules/ContactBackupComposer;->mOutStream:Ljava/io/FileOutputStream;

    invoke-virtual {v0}, Ljava/io/FileOutputStream;->close()V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    :cond_1
    :goto_0
    return-void

    :catch_0
    move-exception v0

    goto :goto_0

    :catch_1
    move-exception v0

    goto :goto_0
.end method

.method public final onStart()V
    .locals 6

    invoke-super {p0}, Lcom/mediatek/datatransfer/modules/Composer;->onStart()V

    invoke-virtual {p0}, Lcom/mediatek/datatransfer/modules/ContactBackupComposer;->getCount()I

    move-result v4

    if-lez v4, :cond_1

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v5, p0, Lcom/mediatek/datatransfer/modules/Composer;->mParentFolderPath:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    sget-object v5, Ljava/io/File;->separator:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "Contact"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    new-instance v2, Ljava/io/File;

    invoke-direct {v2, v3}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2}, Ljava/io/File;->exists()Z

    move-result v4

    if-nez v4, :cond_0

    invoke-virtual {v2}, Ljava/io/File;->mkdirs()Z

    :cond_0
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    sget-object v5, Ljava/io/File;->separator:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "contact.vcf"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    :try_start_0
    new-instance v4, Ljava/io/FileOutputStream;

    invoke-direct {v4, v1}, Ljava/io/FileOutputStream;-><init>(Ljava/lang/String;)V

    iput-object v4, p0, Lcom/mediatek/datatransfer/modules/ContactBackupComposer;->mOutStream:Ljava/io/FileOutputStream;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1

    :cond_1
    :goto_0
    return-void

    :catch_0
    move-exception v0

    const/4 v4, 0x0

    iput-object v4, p0, Lcom/mediatek/datatransfer/modules/ContactBackupComposer;->mOutStream:Ljava/io/FileOutputStream;

    goto :goto_0

    :catch_1
    move-exception v4

    goto :goto_0
.end method
