.class Lcom/mediatek/datatransfer/modules/MmsBackupComposer$WriteFileThread;
.super Ljava/lang/Thread;
.source "MmsBackupComposer.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/mediatek/datatransfer/modules/MmsBackupComposer;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "WriteFileThread"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/mediatek/datatransfer/modules/MmsBackupComposer;


# direct methods
.method private constructor <init>(Lcom/mediatek/datatransfer/modules/MmsBackupComposer;)V
    .locals 0

    iput-object p1, p0, Lcom/mediatek/datatransfer/modules/MmsBackupComposer$WriteFileThread;->this$0:Lcom/mediatek/datatransfer/modules/MmsBackupComposer;

    invoke-direct {p0}, Ljava/lang/Thread;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/mediatek/datatransfer/modules/MmsBackupComposer;Lcom/mediatek/datatransfer/modules/MmsBackupComposer$1;)V
    .locals 0
    .param p1    # Lcom/mediatek/datatransfer/modules/MmsBackupComposer;
    .param p2    # Lcom/mediatek/datatransfer/modules/MmsBackupComposer$1;

    invoke-direct {p0, p1}, Lcom/mediatek/datatransfer/modules/MmsBackupComposer$WriteFileThread;-><init>(Lcom/mediatek/datatransfer/modules/MmsBackupComposer;)V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 7

    const/4 v2, 0x0

    :goto_0
    iget-object v4, p0, Lcom/mediatek/datatransfer/modules/MmsBackupComposer$WriteFileThread;->this$0:Lcom/mediatek/datatransfer/modules/MmsBackupComposer;

    invoke-static {v4}, Lcom/mediatek/datatransfer/modules/MmsBackupComposer;->access$200(Lcom/mediatek/datatransfer/modules/MmsBackupComposer;)Ljava/util/ArrayList;

    move-result-object v4

    if-eqz v4, :cond_3

    iget-object v4, p0, Lcom/mediatek/datatransfer/modules/MmsBackupComposer$WriteFileThread;->this$0:Lcom/mediatek/datatransfer/modules/MmsBackupComposer;

    invoke-static {v4}, Lcom/mediatek/datatransfer/modules/MmsBackupComposer;->access$200(Lcom/mediatek/datatransfer/modules/MmsBackupComposer;)Ljava/util/ArrayList;

    move-result-object v4

    invoke-virtual {v4}, Ljava/util/ArrayList;->size()I

    move-result v4

    if-ge v2, v4, :cond_3

    iget-object v4, p0, Lcom/mediatek/datatransfer/modules/MmsBackupComposer$WriteFileThread;->this$0:Lcom/mediatek/datatransfer/modules/MmsBackupComposer;

    invoke-static {v4}, Lcom/mediatek/datatransfer/modules/MmsBackupComposer;->access$200(Lcom/mediatek/datatransfer/modules/MmsBackupComposer;)Ljava/util/ArrayList;

    move-result-object v4

    invoke-virtual {v4, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/mediatek/datatransfer/modules/MmsBackupComposer$MmsBackupContent;

    iget-object v3, v4, Lcom/mediatek/datatransfer/modules/MmsBackupComposer$MmsBackupContent;->pduMid:[B

    iget-object v4, p0, Lcom/mediatek/datatransfer/modules/MmsBackupComposer$WriteFileThread;->this$0:Lcom/mediatek/datatransfer/modules/MmsBackupComposer;

    invoke-static {v4}, Lcom/mediatek/datatransfer/modules/MmsBackupComposer;->access$200(Lcom/mediatek/datatransfer/modules/MmsBackupComposer;)Ljava/util/ArrayList;

    move-result-object v4

    invoke-virtual {v4, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/mediatek/datatransfer/modules/MmsBackupComposer$MmsBackupContent;

    iget-object v1, v4, Lcom/mediatek/datatransfer/modules/MmsBackupComposer$MmsBackupContent;->fileName:Ljava/lang/String;

    if-eqz v3, :cond_1

    :try_start_0
    const-string v4, "DataTransfer/MmsBackupComposer"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "Mms: WriteFileThread() pduMid.length:"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    array-length v6, v3

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/mediatek/datatransfer/utils/MyLogger;->logD(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v4, p0, Lcom/mediatek/datatransfer/modules/MmsBackupComposer$WriteFileThread;->this$0:Lcom/mediatek/datatransfer/modules/MmsBackupComposer;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v6, p0, Lcom/mediatek/datatransfer/modules/MmsBackupComposer$WriteFileThread;->this$0:Lcom/mediatek/datatransfer/modules/MmsBackupComposer;

    iget-object v6, v6, Lcom/mediatek/datatransfer/modules/Composer;->mParentFolderPath:Ljava/lang/String;

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    sget-object v6, Ljava/io/File;->separator:Ljava/lang/String;

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "mms"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    sget-object v6, Ljava/io/File;->separator:Ljava/lang/String;

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5, v3}, Lcom/mediatek/datatransfer/modules/MmsBackupComposer;->access$300(Lcom/mediatek/datatransfer/modules/MmsBackupComposer;Ljava/lang/String;[B)V

    iget-object v4, p0, Lcom/mediatek/datatransfer/modules/MmsBackupComposer$WriteFileThread;->this$0:Lcom/mediatek/datatransfer/modules/MmsBackupComposer;

    invoke-static {v4}, Lcom/mediatek/datatransfer/modules/MmsBackupComposer;->access$400(Lcom/mediatek/datatransfer/modules/MmsBackupComposer;)Lcom/mediatek/datatransfer/modules/MmsXmlComposer;

    move-result-object v4

    if-eqz v4, :cond_0

    iget-object v4, p0, Lcom/mediatek/datatransfer/modules/MmsBackupComposer$WriteFileThread;->this$0:Lcom/mediatek/datatransfer/modules/MmsBackupComposer;

    invoke-static {v4}, Lcom/mediatek/datatransfer/modules/MmsBackupComposer;->access$400(Lcom/mediatek/datatransfer/modules/MmsBackupComposer;)Lcom/mediatek/datatransfer/modules/MmsXmlComposer;

    move-result-object v5

    iget-object v4, p0, Lcom/mediatek/datatransfer/modules/MmsBackupComposer$WriteFileThread;->this$0:Lcom/mediatek/datatransfer/modules/MmsBackupComposer;

    invoke-static {v4}, Lcom/mediatek/datatransfer/modules/MmsBackupComposer;->access$200(Lcom/mediatek/datatransfer/modules/MmsBackupComposer;)Ljava/util/ArrayList;

    move-result-object v4

    invoke-virtual {v4, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/mediatek/datatransfer/modules/MmsBackupComposer$MmsBackupContent;

    iget-object v4, v4, Lcom/mediatek/datatransfer/modules/MmsBackupComposer$MmsBackupContent;->record:Lcom/mediatek/datatransfer/modules/MmsXmlInfo;

    invoke-virtual {v5, v4}, Lcom/mediatek/datatransfer/modules/MmsXmlComposer;->addOneMmsRecord(Lcom/mediatek/datatransfer/modules/MmsXmlInfo;)Z

    :cond_0
    iget-object v4, p0, Lcom/mediatek/datatransfer/modules/MmsBackupComposer$WriteFileThread;->this$0:Lcom/mediatek/datatransfer/modules/MmsBackupComposer;

    const/4 v5, 0x1

    invoke-virtual {v4, v5}, Lcom/mediatek/datatransfer/modules/Composer;->increaseComposed(Z)V

    const-string v4, "DataTransfer/MmsBackupComposer"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "WriteFileThread() addFile:"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, " success"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/mediatek/datatransfer/utils/MyLogger;->logD(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    :cond_1
    :goto_1
    add-int/lit8 v2, v2, 0x1

    goto/16 :goto_0

    :catch_0
    move-exception v0

    iget-object v4, p0, Lcom/mediatek/datatransfer/modules/MmsBackupComposer$WriteFileThread;->this$0:Lcom/mediatek/datatransfer/modules/MmsBackupComposer;

    iget-object v4, v4, Lcom/mediatek/datatransfer/modules/Composer;->mReporter:Lcom/mediatek/datatransfer/ProgressReporter;

    if-eqz v4, :cond_2

    iget-object v4, p0, Lcom/mediatek/datatransfer/modules/MmsBackupComposer$WriteFileThread;->this$0:Lcom/mediatek/datatransfer/modules/MmsBackupComposer;

    iget-object v4, v4, Lcom/mediatek/datatransfer/modules/Composer;->mReporter:Lcom/mediatek/datatransfer/ProgressReporter;

    invoke-interface {v4, v0}, Lcom/mediatek/datatransfer/ProgressReporter;->onErr(Ljava/io/IOException;)V

    :cond_2
    const-string v4, "DataTransfer/MmsBackupComposer"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "Mms: WriteFileThread() addFile:"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, " fail"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/mediatek/datatransfer/utils/MyLogger;->logE(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1

    :cond_3
    iget-object v4, p0, Lcom/mediatek/datatransfer/modules/MmsBackupComposer$WriteFileThread;->this$0:Lcom/mediatek/datatransfer/modules/MmsBackupComposer;

    invoke-static {v4}, Lcom/mediatek/datatransfer/modules/MmsBackupComposer;->access$500(Lcom/mediatek/datatransfer/modules/MmsBackupComposer;)Ljava/lang/Object;

    move-result-object v5

    monitor-enter v5

    :try_start_1
    iget-object v4, p0, Lcom/mediatek/datatransfer/modules/MmsBackupComposer$WriteFileThread;->this$0:Lcom/mediatek/datatransfer/modules/MmsBackupComposer;

    const/4 v6, 0x0

    invoke-static {v4, v6}, Lcom/mediatek/datatransfer/modules/MmsBackupComposer;->access$202(Lcom/mediatek/datatransfer/modules/MmsBackupComposer;Ljava/util/ArrayList;)Ljava/util/ArrayList;

    iget-object v4, p0, Lcom/mediatek/datatransfer/modules/MmsBackupComposer$WriteFileThread;->this$0:Lcom/mediatek/datatransfer/modules/MmsBackupComposer;

    invoke-static {v4}, Lcom/mediatek/datatransfer/modules/MmsBackupComposer;->access$500(Lcom/mediatek/datatransfer/modules/MmsBackupComposer;)Ljava/lang/Object;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/Object;->notifyAll()V

    monitor-exit v5

    return-void

    :catchall_0
    move-exception v4

    monitor-exit v5
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v4
.end method
