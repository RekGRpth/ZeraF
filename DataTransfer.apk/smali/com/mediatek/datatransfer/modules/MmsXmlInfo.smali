.class public Lcom/mediatek/datatransfer/modules/MmsXmlInfo;
.super Ljava/lang/Object;
.source "MmsXmlInfo.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/mediatek/datatransfer/modules/MmsXmlInfo$MmsXml;
    }
.end annotation


# static fields
.field private static final CLASS_TAG:Ljava/lang/String; = "DataTransfer/RestoreService"


# instance fields
.field private mDate:Ljava/lang/String;

.field private mId:Ljava/lang/String;

.field private mIsLocked:Ljava/lang/String;

.field private mIsRead:Ljava/lang/String;

.field private mMsgBox:Ljava/lang/String;

.field private mSimId:Ljava/lang/String;

.field private mSize:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public getDate()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/mediatek/datatransfer/modules/MmsXmlInfo;->mDate:Ljava/lang/String;

    if-nez v0, :cond_0

    const-string v0, ""

    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/mediatek/datatransfer/modules/MmsXmlInfo;->mDate:Ljava/lang/String;

    goto :goto_0
.end method

.method public getID()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/mediatek/datatransfer/modules/MmsXmlInfo;->mId:Ljava/lang/String;

    if-nez v0, :cond_0

    const-string v0, ""

    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/mediatek/datatransfer/modules/MmsXmlInfo;->mId:Ljava/lang/String;

    goto :goto_0
.end method

.method public getIsLocked()Ljava/lang/String;
    .locals 2

    iget-object v0, p0, Lcom/mediatek/datatransfer/modules/MmsXmlInfo;->mIsLocked:Ljava/lang/String;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/mediatek/datatransfer/modules/MmsXmlInfo;->mIsLocked:Ljava/lang/String;

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const-string v0, "0"

    :goto_0
    return-object v0

    :cond_1
    iget-object v0, p0, Lcom/mediatek/datatransfer/modules/MmsXmlInfo;->mIsLocked:Ljava/lang/String;

    goto :goto_0
.end method

.method public getIsRead()Ljava/lang/String;
    .locals 2

    iget-object v0, p0, Lcom/mediatek/datatransfer/modules/MmsXmlInfo;->mIsRead:Ljava/lang/String;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/mediatek/datatransfer/modules/MmsXmlInfo;->mIsRead:Ljava/lang/String;

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const-string v0, "1"

    :goto_0
    return-object v0

    :cond_1
    iget-object v0, p0, Lcom/mediatek/datatransfer/modules/MmsXmlInfo;->mIsRead:Ljava/lang/String;

    goto :goto_0
.end method

.method public getMsgBox()Ljava/lang/String;
    .locals 2

    iget-object v0, p0, Lcom/mediatek/datatransfer/modules/MmsXmlInfo;->mMsgBox:Ljava/lang/String;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/mediatek/datatransfer/modules/MmsXmlInfo;->mMsgBox:Ljava/lang/String;

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const-string v0, "1"

    :goto_0
    return-object v0

    :cond_1
    iget-object v0, p0, Lcom/mediatek/datatransfer/modules/MmsXmlInfo;->mMsgBox:Ljava/lang/String;

    goto :goto_0
.end method

.method public getSimId()Ljava/lang/String;
    .locals 2

    iget-object v0, p0, Lcom/mediatek/datatransfer/modules/MmsXmlInfo;->mSimId:Ljava/lang/String;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/mediatek/datatransfer/modules/MmsXmlInfo;->mSimId:Ljava/lang/String;

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const-string v0, "0"

    :goto_0
    return-object v0

    :cond_1
    iget-object v0, p0, Lcom/mediatek/datatransfer/modules/MmsXmlInfo;->mSimId:Ljava/lang/String;

    goto :goto_0
.end method

.method public getSize()Ljava/lang/String;
    .locals 2

    iget-object v0, p0, Lcom/mediatek/datatransfer/modules/MmsXmlInfo;->mSize:Ljava/lang/String;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/mediatek/datatransfer/modules/MmsXmlInfo;->mSize:Ljava/lang/String;

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const-string v0, "0"

    :goto_0
    return-object v0

    :cond_1
    iget-object v0, p0, Lcom/mediatek/datatransfer/modules/MmsXmlInfo;->mSize:Ljava/lang/String;

    goto :goto_0
.end method

.method public setDate(Ljava/lang/String;)V
    .locals 0
    .param p1    # Ljava/lang/String;

    iput-object p1, p0, Lcom/mediatek/datatransfer/modules/MmsXmlInfo;->mDate:Ljava/lang/String;

    return-void
.end method

.method public setID(Ljava/lang/String;)V
    .locals 0
    .param p1    # Ljava/lang/String;

    iput-object p1, p0, Lcom/mediatek/datatransfer/modules/MmsXmlInfo;->mId:Ljava/lang/String;

    return-void
.end method

.method public setIsLocked(Ljava/lang/String;)V
    .locals 0
    .param p1    # Ljava/lang/String;

    iput-object p1, p0, Lcom/mediatek/datatransfer/modules/MmsXmlInfo;->mIsLocked:Ljava/lang/String;

    return-void
.end method

.method public setIsRead(Ljava/lang/String;)V
    .locals 0
    .param p1    # Ljava/lang/String;

    iput-object p1, p0, Lcom/mediatek/datatransfer/modules/MmsXmlInfo;->mIsRead:Ljava/lang/String;

    return-void
.end method

.method public setMsgBox(Ljava/lang/String;)V
    .locals 0
    .param p1    # Ljava/lang/String;

    iput-object p1, p0, Lcom/mediatek/datatransfer/modules/MmsXmlInfo;->mMsgBox:Ljava/lang/String;

    return-void
.end method

.method public setSimId(Ljava/lang/String;)V
    .locals 0
    .param p1    # Ljava/lang/String;

    iput-object p1, p0, Lcom/mediatek/datatransfer/modules/MmsXmlInfo;->mSimId:Ljava/lang/String;

    return-void
.end method

.method public setSize(Ljava/lang/String;)V
    .locals 0
    .param p1    # Ljava/lang/String;

    iput-object p1, p0, Lcom/mediatek/datatransfer/modules/MmsXmlInfo;->mSize:Ljava/lang/String;

    return-void
.end method
