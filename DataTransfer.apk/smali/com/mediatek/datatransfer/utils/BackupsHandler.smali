.class public interface abstract Lcom/mediatek/datatransfer/utils/BackupsHandler;
.super Ljava/lang/Object;
.source "BackupsHandler.java"


# static fields
.field public static final result:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/io/File;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    sput-object v0, Lcom/mediatek/datatransfer/utils/BackupsHandler;->result:Ljava/util/List;

    return-void
.end method


# virtual methods
.method public abstract cancel()V
.end method

.method public abstract getBackupType()Ljava/lang/String;
.end method

.method public abstract init()Z
.end method

.method public abstract onEnd()Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Ljava/io/File;",
            ">;"
        }
    .end annotation
.end method

.method public abstract onStart()V
.end method

.method public abstract reset()V
.end method
