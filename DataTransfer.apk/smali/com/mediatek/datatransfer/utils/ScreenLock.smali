.class public Lcom/mediatek/datatransfer/utils/ScreenLock;
.super Ljava/lang/Object;
.source "ScreenLock.java"


# static fields
.field private static final CLASS_TAG:Ljava/lang/String; = "DataTransfer/ScreenLock"

.field private static mIncetance:Lcom/mediatek/datatransfer/utils/ScreenLock;


# instance fields
.field private mWakeLock:Landroid/os/PowerManager$WakeLock;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const/4 v0, 0x0

    sput-object v0, Lcom/mediatek/datatransfer/utils/ScreenLock;->mIncetance:Lcom/mediatek/datatransfer/utils/ScreenLock;

    return-void
.end method

.method private constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static instance()Lcom/mediatek/datatransfer/utils/ScreenLock;
    .locals 1

    sget-object v0, Lcom/mediatek/datatransfer/utils/ScreenLock;->mIncetance:Lcom/mediatek/datatransfer/utils/ScreenLock;

    if-nez v0, :cond_0

    new-instance v0, Lcom/mediatek/datatransfer/utils/ScreenLock;

    invoke-direct {v0}, Lcom/mediatek/datatransfer/utils/ScreenLock;-><init>()V

    sput-object v0, Lcom/mediatek/datatransfer/utils/ScreenLock;->mIncetance:Lcom/mediatek/datatransfer/utils/ScreenLock;

    :cond_0
    sget-object v0, Lcom/mediatek/datatransfer/utils/ScreenLock;->mIncetance:Lcom/mediatek/datatransfer/utils/ScreenLock;

    return-object v0
.end method


# virtual methods
.method public acquireWakeLock(Landroid/content/Context;)V
    .locals 3
    .param p1    # Landroid/content/Context;

    iget-object v1, p0, Lcom/mediatek/datatransfer/utils/ScreenLock;->mWakeLock:Landroid/os/PowerManager$WakeLock;

    if-nez v1, :cond_0

    const-string v1, "DataTransfer/ScreenLock"

    const-string v2, "Acquiring wake lock"

    invoke-static {v1, v2}, Lcom/mediatek/datatransfer/utils/MyLogger;->logD(Ljava/lang/String;Ljava/lang/String;)V

    const-string v1, "power"

    invoke-virtual {p1, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/os/PowerManager;

    const/4 v1, 0x6

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Class;->getCanonicalName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/os/PowerManager;->newWakeLock(ILjava/lang/String;)Landroid/os/PowerManager$WakeLock;

    move-result-object v1

    iput-object v1, p0, Lcom/mediatek/datatransfer/utils/ScreenLock;->mWakeLock:Landroid/os/PowerManager$WakeLock;

    :cond_0
    iget-object v1, p0, Lcom/mediatek/datatransfer/utils/ScreenLock;->mWakeLock:Landroid/os/PowerManager$WakeLock;

    invoke-virtual {v1}, Landroid/os/PowerManager$WakeLock;->acquire()V

    return-void
.end method

.method public releaseWakeLock()V
    .locals 1

    iget-object v0, p0, Lcom/mediatek/datatransfer/utils/ScreenLock;->mWakeLock:Landroid/os/PowerManager$WakeLock;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/mediatek/datatransfer/utils/ScreenLock;->mWakeLock:Landroid/os/PowerManager$WakeLock;

    invoke-virtual {v0}, Landroid/os/PowerManager$WakeLock;->isHeld()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/mediatek/datatransfer/utils/ScreenLock;->mWakeLock:Landroid/os/PowerManager$WakeLock;

    invoke-virtual {v0}, Landroid/os/PowerManager$WakeLock;->release()V

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/mediatek/datatransfer/utils/ScreenLock;->mWakeLock:Landroid/os/PowerManager$WakeLock;

    :cond_0
    return-void
.end method
