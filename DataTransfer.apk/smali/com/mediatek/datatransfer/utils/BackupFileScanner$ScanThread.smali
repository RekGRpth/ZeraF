.class Lcom/mediatek/datatransfer/utils/BackupFileScanner$ScanThread;
.super Ljava/lang/Thread;
.source "BackupFileScanner.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/mediatek/datatransfer/utils/BackupFileScanner;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "ScanThread"
.end annotation


# instance fields
.field isCanceled:Z

.field final synthetic this$0:Lcom/mediatek/datatransfer/utils/BackupFileScanner;


# direct methods
.method private constructor <init>(Lcom/mediatek/datatransfer/utils/BackupFileScanner;)V
    .locals 1

    iput-object p1, p0, Lcom/mediatek/datatransfer/utils/BackupFileScanner$ScanThread;->this$0:Lcom/mediatek/datatransfer/utils/BackupFileScanner;

    invoke-direct {p0}, Ljava/lang/Thread;-><init>()V

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/mediatek/datatransfer/utils/BackupFileScanner$ScanThread;->isCanceled:Z

    return-void
.end method

.method synthetic constructor <init>(Lcom/mediatek/datatransfer/utils/BackupFileScanner;Lcom/mediatek/datatransfer/utils/BackupFileScanner$1;)V
    .locals 0
    .param p1    # Lcom/mediatek/datatransfer/utils/BackupFileScanner;
    .param p2    # Lcom/mediatek/datatransfer/utils/BackupFileScanner$1;

    invoke-direct {p0, p1}, Lcom/mediatek/datatransfer/utils/BackupFileScanner$ScanThread;-><init>(Lcom/mediatek/datatransfer/utils/BackupFileScanner;)V

    return-void
.end method

.method private addMD5toPreference(Ljava/util/List;)V
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    iget-object v4, p0, Lcom/mediatek/datatransfer/utils/BackupFileScanner$ScanThread;->this$0:Lcom/mediatek/datatransfer/utils/BackupFileScanner;

    invoke-static {v4}, Lcom/mediatek/datatransfer/utils/BackupFileScanner;->access$200(Lcom/mediatek/datatransfer/utils/BackupFileScanner;)Landroid/content/Context;

    move-result-object v4

    const-string v5, "md5_info"

    const/4 v6, 0x0

    invoke-virtual {v4, v5, v6}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v3

    invoke-interface {v3}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    const/4 v4, 0x1

    invoke-interface {v0, v2, v4}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    goto :goto_0

    :cond_0
    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    return-void
.end method

.method private checkMD5(Ljava/util/List;)Ljava/util/List;
    .locals 10
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Ljava/io/File;",
            ">;)",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    const/4 v9, 0x0

    iget-object v7, p0, Lcom/mediatek/datatransfer/utils/BackupFileScanner$ScanThread;->this$0:Lcom/mediatek/datatransfer/utils/BackupFileScanner;

    invoke-static {v7}, Lcom/mediatek/datatransfer/utils/BackupFileScanner;->access$200(Lcom/mediatek/datatransfer/utils/BackupFileScanner;)Landroid/content/Context;

    move-result-object v7

    const-string v8, "md5_info"

    invoke-virtual {v7, v8, v9}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v5

    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    new-instance v6, Ljava/util/ArrayList;

    invoke-direct {v6}, Ljava/util/ArrayList;-><init>()V

    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v7

    if-eqz v7, :cond_3

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/io/File;

    if-nez v0, :cond_1

    const-string v7, "DataTransfer/BackupFileScanner"

    const-string v8, "[checkMD5] file is null ,continue!"

    invoke-static {v7, v8}, Lcom/mediatek/datatransfer/utils/MyLogger;->logE(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    :cond_1
    invoke-virtual {v0}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v7

    invoke-static {v7}, Lcom/mediatek/datatransfer/utils/FileUtils;->getFileMD5(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-interface {v5, v2, v9}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v3

    if-nez v3, :cond_2

    invoke-interface {v4, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_2
    const/4 v7, 0x1

    if-ne v3, v7, :cond_0

    invoke-interface {v6, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_3
    invoke-interface {p1, v6}, Ljava/util/List;->removeAll(Ljava/util/Collection;)Z

    return-object v4
.end method

.method private detectSDcardForOtherBackups()V
    .locals 13

    const/4 v9, 0x1

    const/4 v5, 0x0

    new-instance v8, Ljava/util/ArrayList;

    invoke-direct {v8}, Ljava/util/ArrayList;-><init>()V

    iget-object v10, p0, Lcom/mediatek/datatransfer/utils/BackupFileScanner$ScanThread;->this$0:Lcom/mediatek/datatransfer/utils/BackupFileScanner;

    invoke-static {v10}, Lcom/mediatek/datatransfer/utils/BackupFileScanner;->access$100(Lcom/mediatek/datatransfer/utils/BackupFileScanner;)Ljava/util/LinkedHashSet;

    move-result-object v10

    invoke-virtual {v10}, Ljava/util/HashSet;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_0
    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v10

    if-eqz v10, :cond_3

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/mediatek/datatransfer/utils/BackupsHandler;

    iget-boolean v10, p0, Lcom/mediatek/datatransfer/utils/BackupFileScanner$ScanThread;->isCanceled:Z

    if-eqz v10, :cond_1

    invoke-interface {v1}, Lcom/mediatek/datatransfer/utils/BackupsHandler;->cancel()V

    goto :goto_0

    :cond_1
    invoke-interface {v1}, Lcom/mediatek/datatransfer/utils/BackupsHandler;->reset()V

    invoke-interface {v1}, Lcom/mediatek/datatransfer/utils/BackupsHandler;->init()Z

    move-result v10

    if-eqz v10, :cond_0

    invoke-interface {v1}, Lcom/mediatek/datatransfer/utils/BackupsHandler;->onStart()V

    invoke-interface {v1}, Lcom/mediatek/datatransfer/utils/BackupsHandler;->onEnd()Ljava/util/List;

    move-result-object v6

    if-eqz v6, :cond_2

    invoke-interface {v8, v6}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    :cond_2
    invoke-interface {v1}, Lcom/mediatek/datatransfer/utils/BackupsHandler;->getBackupType()Ljava/lang/String;

    move-result-object v0

    const-string v10, "datatransfer"

    invoke-virtual {v0, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_0

    const-string v10, "DataTransfer/BackupFileScanner"

    const-string v11, "Type is DATATRANSFER"

    invoke-static {v10, v11}, Lcom/mediatek/datatransfer/utils/MyLogger;->logV(Ljava/lang/String;Ljava/lang/String;)V

    invoke-direct {p0}, Lcom/mediatek/datatransfer/utils/BackupFileScanner$ScanThread;->isOtherBackup()I

    move-result v5

    goto :goto_0

    :cond_3
    const/4 v7, 0x0

    invoke-interface {v8}, Ljava/util/List;->isEmpty()Z

    move-result v10

    if-nez v10, :cond_4

    invoke-direct {p0, v8}, Lcom/mediatek/datatransfer/utils/BackupFileScanner$ScanThread;->checkMD5(Ljava/util/List;)Ljava/util/List;

    move-result-object v4

    if-eqz v8, :cond_4

    :try_start_0
    invoke-direct {p0, v8}, Lcom/mediatek/datatransfer/utils/BackupFileScanner$ScanThread;->makeOneBackupHistory(Ljava/util/List;)Ljava/lang/String;

    move-result-object v7

    invoke-direct {p0, v4}, Lcom/mediatek/datatransfer/utils/BackupFileScanner$ScanThread;->addMD5toPreference(Ljava/util/List;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    :cond_4
    if-eq v5, v9, :cond_5

    invoke-interface {v8}, Ljava/util/List;->isEmpty()Z

    move-result v10

    if-nez v10, :cond_6

    :cond_5
    const-string v10, "DataTransfer/BackupFileScanner"

    new-instance v11, Ljava/lang/StringBuilder;

    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    const-string v12, "notifyType = "

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v11

    const-string v12, "  path = "

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-static {v10, v11}, Lcom/mediatek/datatransfer/utils/MyLogger;->logE(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v10, p0, Lcom/mediatek/datatransfer/utils/BackupFileScanner$ScanThread;->this$0:Lcom/mediatek/datatransfer/utils/BackupFileScanner;

    invoke-static {v10}, Lcom/mediatek/datatransfer/utils/BackupFileScanner;->access$200(Lcom/mediatek/datatransfer/utils/BackupFileScanner;)Landroid/content/Context;

    move-result-object v10

    invoke-static {v10}, Lcom/mediatek/datatransfer/utils/NotifyManager;->getInstance(Landroid/content/Context;)Lcom/mediatek/datatransfer/utils/NotifyManager;

    move-result-object v10

    invoke-virtual {v10, v5, v7}, Lcom/mediatek/datatransfer/utils/NotifyManager;->showNewDetectionNotification(ILjava/lang/String;)V

    :cond_6
    iget-boolean v10, p0, Lcom/mediatek/datatransfer/utils/BackupFileScanner$ScanThread;->isCanceled:Z

    if-eqz v10, :cond_9

    iget-boolean v10, p0, Lcom/mediatek/datatransfer/utils/BackupFileScanner$ScanThread;->isCanceled:Z

    if-nez v10, :cond_8

    :goto_1
    iput-boolean v9, p0, Lcom/mediatek/datatransfer/utils/BackupFileScanner$ScanThread;->isCanceled:Z

    :cond_7
    :goto_2
    return-void

    :catch_0
    move-exception v2

    const-string v9, "DataTransfer/BackupFileScanner"

    const-string v10, "we must do some rollback option here."

    invoke-static {v9, v10}, Lcom/mediatek/datatransfer/utils/MyLogger;->logE(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v2}, Ljava/lang/Throwable;->printStackTrace()V

    if-eqz v7, :cond_7

    const-string v9, ""

    invoke-virtual {v7, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-nez v9, :cond_7

    new-instance v9, Ljava/io/File;

    invoke-direct {v9, v7}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-static {v9}, Lcom/mediatek/datatransfer/utils/FileUtils;->deleteFileOrFolder(Ljava/io/File;)Z

    goto :goto_2

    :cond_8
    const/4 v9, 0x0

    goto :goto_1

    :cond_9
    iget-boolean v9, p0, Lcom/mediatek/datatransfer/utils/BackupFileScanner$ScanThread;->isCanceled:Z

    goto :goto_1
.end method

.method private filterFile([Ljava/io/File;)[Ljava/io/File;
    .locals 7
    .param p1    # [Ljava/io/File;

    const/4 v5, 0x0

    if-nez p1, :cond_1

    :cond_0
    :goto_0
    return-object v5

    :cond_1
    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    move-object v0, p1

    array-length v3, v0

    const/4 v2, 0x0

    :goto_1
    if-ge v2, v3, :cond_2

    aget-object v1, v0, v2

    iget-boolean v6, p0, Lcom/mediatek/datatransfer/utils/BackupFileScanner$ScanThread;->isCanceled:Z

    if-eqz v6, :cond_3

    :cond_2
    iget-boolean v6, p0, Lcom/mediatek/datatransfer/utils/BackupFileScanner$ScanThread;->isCanceled:Z

    if-nez v6, :cond_0

    const/4 v5, 0x0

    new-array v5, v5, [Ljava/io/File;

    invoke-interface {v4, v5}, Ljava/util/List;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v5

    check-cast v5, [Ljava/io/File;

    check-cast v5, [Ljava/io/File;

    goto :goto_0

    :cond_3
    invoke-static {v1}, Lcom/mediatek/datatransfer/utils/FileUtils;->isEmptyFolder(Ljava/io/File;)Z

    move-result v6

    if-nez v6, :cond_4

    invoke-interface {v4, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :cond_4
    add-int/lit8 v2, v2, 0x1

    goto :goto_1
.end method

.method private generateBackupFileItems([Ljava/io/File;)Ljava/util/List;
    .locals 11
    .param p1    # [Ljava/io/File;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "([",
            "Ljava/io/File;",
            ")",
            "Ljava/util/List",
            "<",
            "Lcom/mediatek/datatransfer/utils/BackupFilePreview;",
            ">;"
        }
    .end annotation

    const/4 v6, 0x0

    if-eqz p1, :cond_0

    iget-boolean v7, p0, Lcom/mediatek/datatransfer/utils/BackupFileScanner$ScanThread;->isCanceled:Z

    if-eqz v7, :cond_1

    :cond_0
    move-object v5, v6

    :goto_0
    return-object v5

    :cond_1
    new-instance v5, Ljava/util/ArrayList;

    invoke-direct {v5}, Ljava/util/ArrayList;-><init>()V

    move-object v0, p1

    array-length v4, v0

    const/4 v3, 0x0

    :goto_1
    if-ge v3, v4, :cond_2

    aget-object v2, v0, v3

    iget-boolean v7, p0, Lcom/mediatek/datatransfer/utils/BackupFileScanner$ScanThread;->isCanceled:Z

    if-eqz v7, :cond_3

    :cond_2
    iget-boolean v7, p0, Lcom/mediatek/datatransfer/utils/BackupFileScanner$ScanThread;->isCanceled:Z

    if-nez v7, :cond_5

    invoke-direct {p0, v5}, Lcom/mediatek/datatransfer/utils/BackupFileScanner$ScanThread;->sort(Ljava/util/List;)V

    goto :goto_0

    :cond_3
    new-instance v1, Lcom/mediatek/datatransfer/utils/BackupFilePreview;

    invoke-direct {v1, v2}, Lcom/mediatek/datatransfer/utils/BackupFilePreview;-><init>(Ljava/io/File;)V

    if-eqz v1, :cond_4

    invoke-virtual {v1}, Lcom/mediatek/datatransfer/utils/BackupFilePreview;->getFileSize()J

    move-result-wide v7

    const-wide/16 v9, 0x0

    cmp-long v7, v7, v9

    if-eqz v7, :cond_4

    invoke-interface {v5, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :cond_4
    add-int/lit8 v3, v3, 0x1

    goto :goto_1

    :cond_5
    move-object v5, v6

    goto :goto_0
.end method

.method private getDataTransferBackups()Ljava/util/HashMap;
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/util/List",
            "<",
            "Lcom/mediatek/datatransfer/utils/BackupFilePreview;",
            ">;>;"
        }
    .end annotation

    invoke-direct {p0}, Lcom/mediatek/datatransfer/utils/BackupFileScanner$ScanThread;->scanPersonalBackupFiles()[Ljava/io/File;

    move-result-object v3

    new-instance v5, Ljava/util/HashMap;

    invoke-direct {v5}, Ljava/util/HashMap;-><init>()V

    invoke-direct {p0, v3}, Lcom/mediatek/datatransfer/utils/BackupFileScanner$ScanThread;->generateBackupFileItems([Ljava/io/File;)Ljava/util/List;

    move-result-object v2

    const-string v6, "personalData"

    invoke-virtual {v5, v6, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    invoke-static {}, Lcom/mediatek/datatransfer/utils/SDCardUtils;->getAppsBackupPath()Ljava/lang/String;

    move-result-object v4

    if-eqz v4, :cond_0

    invoke-virtual {v4}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v6

    const-string v7, ""

    invoke-virtual {v6, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_1

    :cond_0
    const/4 v5, 0x0

    :goto_0
    return-object v5

    :cond_1
    new-instance v1, Ljava/io/File;

    invoke-direct {v1, v4}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    const/4 v0, 0x0

    invoke-virtual {v1}, Ljava/io/File;->exists()Z

    move-result v6

    if-eqz v6, :cond_2

    new-instance v0, Lcom/mediatek/datatransfer/utils/BackupFilePreview;

    invoke-direct {v0, v1}, Lcom/mediatek/datatransfer/utils/BackupFilePreview;-><init>(Ljava/io/File;)V

    invoke-interface {v2, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :cond_2
    const-string v6, "appData"

    invoke-virtual {v5, v6, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0
.end method

.method private initDataTransferBackupForList()V
    .locals 5

    invoke-direct {p0}, Lcom/mediatek/datatransfer/utils/BackupFileScanner$ScanThread;->getDataTransferBackups()Ljava/util/HashMap;

    move-result-object v1

    iget-object v2, p0, Lcom/mediatek/datatransfer/utils/BackupFileScanner$ScanThread;->this$0:Lcom/mediatek/datatransfer/utils/BackupFileScanner;

    invoke-static {v2}, Lcom/mediatek/datatransfer/utils/BackupFileScanner;->access$300(Lcom/mediatek/datatransfer/utils/BackupFileScanner;)Ljava/lang/Object;

    move-result-object v3

    monitor-enter v3

    :try_start_0
    iget-boolean v2, p0, Lcom/mediatek/datatransfer/utils/BackupFileScanner$ScanThread;->isCanceled:Z

    if-nez v2, :cond_0

    iget-object v2, p0, Lcom/mediatek/datatransfer/utils/BackupFileScanner$ScanThread;->this$0:Lcom/mediatek/datatransfer/utils/BackupFileScanner;

    invoke-static {v2}, Lcom/mediatek/datatransfer/utils/BackupFileScanner;->access$400(Lcom/mediatek/datatransfer/utils/BackupFileScanner;)Landroid/os/Handler;

    move-result-object v2

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/mediatek/datatransfer/utils/BackupFileScanner$ScanThread;->this$0:Lcom/mediatek/datatransfer/utils/BackupFileScanner;

    invoke-static {v2}, Lcom/mediatek/datatransfer/utils/BackupFileScanner;->access$400(Lcom/mediatek/datatransfer/utils/BackupFileScanner;)Landroid/os/Handler;

    move-result-object v2

    const/16 v4, 0x502

    invoke-virtual {v2, v4, v1}, Landroid/os/Handler;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v0

    iget-object v2, p0, Lcom/mediatek/datatransfer/utils/BackupFileScanner$ScanThread;->this$0:Lcom/mediatek/datatransfer/utils/BackupFileScanner;

    invoke-static {v2}, Lcom/mediatek/datatransfer/utils/BackupFileScanner;->access$400(Lcom/mediatek/datatransfer/utils/BackupFileScanner;)Landroid/os/Handler;

    move-result-object v2

    invoke-virtual {v2, v0}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    :cond_0
    monitor-exit v3
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    iget-object v2, p0, Lcom/mediatek/datatransfer/utils/BackupFileScanner$ScanThread;->this$0:Lcom/mediatek/datatransfer/utils/BackupFileScanner;

    const/4 v3, 0x0

    iput-object v3, v2, Lcom/mediatek/datatransfer/utils/BackupFileScanner;->mScanThread:Lcom/mediatek/datatransfer/utils/BackupFileScanner$ScanThread;

    return-void

    :catchall_0
    move-exception v2

    :try_start_1
    monitor-exit v3
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v2
.end method

.method private isBootReset(Z)Z
    .locals 5
    .param p1    # Z

    const/4 v1, 0x1

    const/4 v2, 0x0

    iget-object v3, p0, Lcom/mediatek/datatransfer/utils/BackupFileScanner$ScanThread;->this$0:Lcom/mediatek/datatransfer/utils/BackupFileScanner;

    invoke-static {v3}, Lcom/mediatek/datatransfer/utils/BackupFileScanner;->access$200(Lcom/mediatek/datatransfer/utils/BackupFileScanner;)Landroid/content/Context;

    move-result-object v3

    const-string v4, "boot_reset_flag"

    invoke-virtual {v3, v4, v2}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v0

    if-eqz p1, :cond_0

    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v3

    const-string v4, "boot_reset"

    invoke-interface {v3, v4, v2}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    move-result-object v3

    invoke-interface {v3}, Landroid/content/SharedPreferences$Editor;->commit()Z

    :cond_0
    const-string v3, "boot_reset"

    invoke-interface {v0, v3, v1}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v3

    if-eqz v3, :cond_1

    :goto_0
    return v1

    :cond_1
    move v1, v2

    goto :goto_0
.end method

.method private isOtherBackup()I
    .locals 11

    const/4 v6, 0x0

    const/4 v7, 0x1

    invoke-direct {p0}, Lcom/mediatek/datatransfer/utils/BackupFileScanner$ScanThread;->scanPersonalBackupFiles()[Ljava/io/File;

    move-result-object v2

    if-eqz v2, :cond_3

    array-length v8, v2

    if-lez v8, :cond_3

    move-object v0, v2

    array-length v4, v0

    const/4 v3, 0x0

    :goto_0
    if-ge v3, v4, :cond_3

    aget-object v1, v0, v3

    new-instance v5, Lcom/mediatek/datatransfer/utils/BackupFilePreview;

    invoke-direct {v5, v1}, Lcom/mediatek/datatransfer/utils/BackupFilePreview;-><init>(Ljava/io/File;)V

    const-string v8, "DataTransfer/BackupFileScanner"

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "temPreview.isOtherDeviceBackup() = "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v5}, Lcom/mediatek/datatransfer/utils/BackupFilePreview;->isOtherDeviceBackup()Z

    move-result v10

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v8, v9}, Lcom/mediatek/datatransfer/utils/MyLogger;->logW(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v5}, Lcom/mediatek/datatransfer/utils/BackupFilePreview;->isOtherDeviceBackup()Z

    move-result v8

    if-eqz v8, :cond_0

    :goto_1
    return v7

    :cond_0
    invoke-direct {p0, v6}, Lcom/mediatek/datatransfer/utils/BackupFileScanner$ScanThread;->isBootReset(Z)Z

    move-result v8

    if-eqz v8, :cond_2

    invoke-virtual {v5}, Lcom/mediatek/datatransfer/utils/BackupFilePreview;->isSelfBackup()Z

    move-result v8

    if-nez v8, :cond_2

    const-string v8, "DataTransfer/BackupFileScanner"

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "Find other Phone\'s backup on SDCard && boot has been reseted "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v5}, Lcom/mediatek/datatransfer/utils/BackupFilePreview;->isSelfBackup()Z

    move-result v10

    if-nez v10, :cond_1

    move v6, v7

    :cond_1
    invoke-virtual {v9, v6}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v8, v6}, Lcom/mediatek/datatransfer/utils/MyLogger;->logD(Ljava/lang/String;Ljava/lang/String;)V

    invoke-direct {p0, v7}, Lcom/mediatek/datatransfer/utils/BackupFileScanner$ScanThread;->isBootReset(Z)Z

    goto :goto_1

    :cond_2
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    :cond_3
    move v7, v6

    goto :goto_1
.end method

.method private makeOneBackupHistory(Ljava/util/List;)Ljava/lang/String;
    .locals 23
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Ljava/io/File;",
            ">;)",
            "Ljava/lang/String;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-static/range {p1 .. p1}, Lcom/mediatek/datatransfer/utils/FileUtils;->getNewestFile(Ljava/util/List;)Ljava/io/File;

    move-result-object v15

    if-nez v15, :cond_0

    const/16 v16, 0x0

    :goto_0
    return-object v16

    :cond_0
    invoke-virtual {v15}, Ljava/io/File;->lastModified()J

    move-result-wide v9

    new-instance v7, Ljava/text/SimpleDateFormat;

    const-string v20, "yyyyMMddHHmmss"

    move-object/from16 v0, v20

    invoke-direct {v7, v0}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;)V

    new-instance v20, Ljava/util/Date;

    move-object/from16 v0, v20

    invoke-direct {v0, v9, v10}, Ljava/util/Date;-><init>(J)V

    move-object/from16 v0, v20

    invoke-virtual {v7, v0}, Ljava/text/DateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v8

    new-instance v20, Ljava/lang/StringBuilder;

    invoke-direct/range {v20 .. v20}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static {}, Lcom/mediatek/datatransfer/utils/SDCardUtils;->getPersonalDataBackupPath()Ljava/lang/String;

    move-result-object v21

    invoke-virtual/range {v20 .. v21}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    sget-object v21, Ljava/io/File;->separator:Ljava/lang/String;

    invoke-virtual/range {v20 .. v21}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    move-object/from16 v0, v20

    invoke-virtual {v0, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    invoke-virtual/range {v20 .. v20}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v16

    new-instance v5, Ljava/io/File;

    move-object/from16 v0, v16

    invoke-direct {v5, v0}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    const-string v20, "DataTransfer/BackupFileScanner"

    new-instance v21, Ljava/lang/StringBuilder;

    invoke-direct/range {v21 .. v21}, Ljava/lang/StringBuilder;-><init>()V

    const-string v22, "makeOneBackupHistory <=====>path  "

    invoke-virtual/range {v21 .. v22}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v21

    move-object/from16 v0, v21

    move-object/from16 v1, v16

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v21

    invoke-virtual/range {v21 .. v21}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v21

    invoke-static/range {v20 .. v21}, Lcom/mediatek/datatransfer/utils/MyLogger;->logV(Ljava/lang/String;Ljava/lang/String;)V

    if-eqz v5, :cond_1

    invoke-virtual {v5}, Ljava/io/File;->exists()Z

    move-result v20

    if-eqz v20, :cond_1

    invoke-virtual {v5}, Ljava/io/File;->isDirectory()Z

    move-result v20

    if-eqz v20, :cond_1

    invoke-static {v5}, Lcom/mediatek/datatransfer/utils/FileUtils;->isEmptyFolder(Ljava/io/File;)Z

    move-result v20

    if-nez v20, :cond_1

    const-string v20, "DataTransfer/BackupFileScanner"

    const-string v21, "makeOneBackupHistory <=====>file has been exist "

    invoke-static/range {v20 .. v21}, Lcom/mediatek/datatransfer/utils/MyLogger;->logV(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    :cond_1
    invoke-static/range {v16 .. v16}, Lcom/mediatek/datatransfer/utils/FileUtils;->createFile(Ljava/lang/String;)Ljava/io/File;

    move-result-object v5

    new-instance v20, Ljava/lang/StringBuilder;

    invoke-direct/range {v20 .. v20}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, v20

    move-object/from16 v1, v16

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    sget-object v21, Ljava/io/File;->separator:Ljava/lang/String;

    invoke-virtual/range {v20 .. v21}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    const-string v21, "Contact"

    invoke-virtual/range {v20 .. v21}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    invoke-virtual/range {v20 .. v20}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v20

    invoke-static/range {v20 .. v20}, Lcom/mediatek/datatransfer/utils/FileUtils;->createFile(Ljava/lang/String;)Ljava/io/File;

    move-result-object v4

    new-instance v20, Ljava/lang/StringBuilder;

    invoke-direct/range {v20 .. v20}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, v20

    move-object/from16 v1, v16

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    sget-object v21, Ljava/io/File;->separator:Ljava/lang/String;

    invoke-virtual/range {v20 .. v21}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    const-string v21, "mms"

    invoke-virtual/range {v20 .. v21}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    invoke-virtual/range {v20 .. v20}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v20

    invoke-static/range {v20 .. v20}, Lcom/mediatek/datatransfer/utils/FileUtils;->createFile(Ljava/lang/String;)Ljava/io/File;

    move-result-object v13

    new-instance v20, Ljava/lang/StringBuilder;

    invoke-direct/range {v20 .. v20}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, v20

    move-object/from16 v1, v16

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    sget-object v21, Ljava/io/File;->separator:Ljava/lang/String;

    invoke-virtual/range {v20 .. v21}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    const-string v21, "sms"

    invoke-virtual/range {v20 .. v21}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    invoke-virtual/range {v20 .. v20}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v20

    invoke-static/range {v20 .. v20}, Lcom/mediatek/datatransfer/utils/FileUtils;->createFile(Ljava/lang/String;)Ljava/io/File;

    move-result-object v18

    invoke-interface/range {p1 .. p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v11

    :cond_2
    :goto_1
    invoke-interface {v11}, Ljava/util/Iterator;->hasNext()Z

    move-result v20

    if-eqz v20, :cond_6

    invoke-interface {v11}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Ljava/io/File;

    invoke-virtual {v6}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v14

    const-string v20, ".vcf"

    move-object/from16 v0, v20

    invoke-virtual {v14, v0}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v20

    if-eqz v20, :cond_3

    invoke-static {v6, v4}, Lorg/apache/commons/io/FileUtils;->copyFileToDirectory(Ljava/io/File;Ljava/io/File;)V

    const-string v20, "DataTransfer/BackupFileScanner"

    new-instance v21, Ljava/lang/StringBuilder;

    invoke-direct/range {v21 .. v21}, Ljava/lang/StringBuilder;-><init>()V

    const-string v22, "makeOneBackupHistory <=====>file = "

    invoke-virtual/range {v21 .. v22}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v21

    invoke-virtual {v6}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v22

    invoke-virtual/range {v21 .. v22}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v21

    const-string v22, "TO: "

    invoke-virtual/range {v21 .. v22}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v21

    invoke-virtual {v5}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v22

    invoke-virtual/range {v21 .. v22}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v21

    invoke-virtual/range {v21 .. v21}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v21

    invoke-static/range {v20 .. v21}, Lcom/mediatek/datatransfer/utils/MyLogger;->logV(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1

    :cond_3
    const-string v20, ".s"

    move-object/from16 v0, v20

    invoke-virtual {v14, v0}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v20

    if-nez v20, :cond_4

    const-string v20, ".m"

    move-object/from16 v0, v20

    invoke-virtual {v14, v0}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v20

    if-eqz v20, :cond_5

    :cond_4
    invoke-static {v6, v13}, Lorg/apache/commons/io/FileUtils;->copyFileToDirectory(Ljava/io/File;Ljava/io/File;)V

    goto :goto_1

    :cond_5
    const-string v20, "sms.vmsg"

    move-object/from16 v0, v20

    invoke-virtual {v14, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v20

    if-eqz v20, :cond_2

    move-object/from16 v0, v18

    invoke-static {v6, v0}, Lorg/apache/commons/io/FileUtils;->copyFileToDirectory(Ljava/io/File;Ljava/io/File;)V

    goto :goto_1

    :cond_6
    invoke-virtual {v4}, Ljava/io/File;->listFiles()[Ljava/io/File;

    move-result-object v20

    invoke-static/range {v20 .. v20}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v20

    new-instance v21, Ljava/lang/StringBuilder;

    invoke-direct/range {v21 .. v21}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v4}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v22

    invoke-virtual/range {v21 .. v22}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v21

    sget-object v22, Ljava/io/File;->separator:Ljava/lang/String;

    invoke-virtual/range {v21 .. v22}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v21

    const-string v22, "contact.vcf"

    invoke-virtual/range {v21 .. v22}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v21

    invoke-virtual/range {v21 .. v21}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v21

    invoke-static/range {v20 .. v21}, Lcom/mediatek/datatransfer/utils/FileUtils;->combineFiles(Ljava/util/List;Ljava/lang/String;)V

    invoke-virtual {v4}, Ljava/io/File;->listFiles()[Ljava/io/File;

    move-result-object v2

    array-length v12, v2

    const/4 v11, 0x0

    :goto_2
    if-ge v11, v12, :cond_8

    aget-object v6, v2, v11

    invoke-virtual {v6}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v20

    const-string v21, "contact.vcf"

    invoke-virtual/range {v20 .. v21}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v20

    if-eqz v20, :cond_7

    :goto_3
    add-int/lit8 v11, v11, 0x1

    goto :goto_2

    :cond_7
    invoke-static {v6}, Lcom/mediatek/datatransfer/utils/FileUtils;->deleteFileOrFolder(Ljava/io/File;)Z

    goto :goto_3

    :cond_8
    if-eqz v13, :cond_b

    invoke-virtual {v13}, Ljava/io/File;->isDirectory()Z

    move-result v20

    if-eqz v20, :cond_b

    invoke-virtual {v13}, Ljava/io/File;->listFiles()[Ljava/io/File;

    move-result-object v20

    move-object/from16 v0, v20

    array-length v0, v0

    move/from16 v20, v0

    if-lez v20, :cond_b

    new-instance v3, Lcom/mediatek/datatransfer/modules/MmsXmlComposer;

    invoke-direct {v3}, Lcom/mediatek/datatransfer/modules/MmsXmlComposer;-><init>()V

    invoke-virtual {v3}, Lcom/mediatek/datatransfer/modules/MmsXmlComposer;->startCompose()Z

    invoke-virtual {v13}, Ljava/io/File;->listFiles()[Ljava/io/File;

    move-result-object v2

    array-length v12, v2

    const/4 v11, 0x0

    :goto_4
    if-ge v11, v12, :cond_a

    aget-object v6, v2, v11

    new-instance v17, Lcom/mediatek/datatransfer/modules/MmsXmlInfo;

    invoke-direct/range {v17 .. v17}, Lcom/mediatek/datatransfer/modules/MmsXmlInfo;-><init>()V

    invoke-virtual {v6}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v20

    move-object/from16 v0, v17

    move-object/from16 v1, v20

    invoke-virtual {v0, v1}, Lcom/mediatek/datatransfer/modules/MmsXmlInfo;->setID(Ljava/lang/String;)V

    const-string v20, "1"

    move-object/from16 v0, v17

    move-object/from16 v1, v20

    invoke-virtual {v0, v1}, Lcom/mediatek/datatransfer/modules/MmsXmlInfo;->setIsRead(Ljava/lang/String;)V

    invoke-virtual {v6}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v20

    const-string v21, ".m"

    invoke-virtual/range {v20 .. v21}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v20

    if-eqz v20, :cond_9

    const-string v20, "1"

    :goto_5
    move-object/from16 v0, v17

    move-object/from16 v1, v20

    invoke-virtual {v0, v1}, Lcom/mediatek/datatransfer/modules/MmsXmlInfo;->setMsgBox(Ljava/lang/String;)V

    new-instance v20, Ljava/lang/StringBuilder;

    invoke-direct/range {v20 .. v20}, Ljava/lang/StringBuilder;-><init>()V

    const-string v21, ""

    invoke-virtual/range {v20 .. v21}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    invoke-virtual {v6}, Ljava/io/File;->lastModified()J

    move-result-wide v21

    invoke-virtual/range {v20 .. v22}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v20

    invoke-virtual/range {v20 .. v20}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v20

    move-object/from16 v0, v17

    move-object/from16 v1, v20

    invoke-virtual {v0, v1}, Lcom/mediatek/datatransfer/modules/MmsXmlInfo;->setDate(Ljava/lang/String;)V

    new-instance v20, Ljava/lang/StringBuilder;

    invoke-direct/range {v20 .. v20}, Ljava/lang/StringBuilder;-><init>()V

    const-string v21, ""

    invoke-virtual/range {v20 .. v21}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    invoke-virtual {v6}, Ljava/io/File;->length()J

    move-result-wide v21

    invoke-virtual/range {v20 .. v22}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v20

    invoke-virtual/range {v20 .. v20}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v20

    move-object/from16 v0, v17

    move-object/from16 v1, v20

    invoke-virtual {v0, v1}, Lcom/mediatek/datatransfer/modules/MmsXmlInfo;->setSize(Ljava/lang/String;)V

    const-string v20, "0"

    move-object/from16 v0, v17

    move-object/from16 v1, v20

    invoke-virtual {v0, v1}, Lcom/mediatek/datatransfer/modules/MmsXmlInfo;->setSimId(Ljava/lang/String;)V

    const-string v20, "0"

    move-object/from16 v0, v17

    move-object/from16 v1, v20

    invoke-virtual {v0, v1}, Lcom/mediatek/datatransfer/modules/MmsXmlInfo;->setIsLocked(Ljava/lang/String;)V

    move-object/from16 v0, v17

    invoke-virtual {v3, v0}, Lcom/mediatek/datatransfer/modules/MmsXmlComposer;->addOneMmsRecord(Lcom/mediatek/datatransfer/modules/MmsXmlInfo;)Z

    add-int/lit8 v11, v11, 0x1

    goto/16 :goto_4

    :cond_9
    const-string v20, "2"

    goto :goto_5

    :cond_a
    invoke-virtual {v3}, Lcom/mediatek/datatransfer/modules/MmsXmlComposer;->endCompose()Z

    invoke-virtual {v3}, Lcom/mediatek/datatransfer/modules/MmsXmlComposer;->getXmlInfo()Ljava/lang/String;

    move-result-object v19

    new-instance v20, Ljava/lang/StringBuilder;

    invoke-direct/range {v20 .. v20}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, v20

    move-object/from16 v1, v16

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    sget-object v21, Ljava/io/File;->separator:Ljava/lang/String;

    invoke-virtual/range {v20 .. v21}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    const-string v21, "mms"

    invoke-virtual/range {v20 .. v21}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    sget-object v21, Ljava/io/File;->separator:Ljava/lang/String;

    invoke-virtual/range {v20 .. v21}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    const-string v21, "msg_box.xml"

    invoke-virtual/range {v20 .. v21}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    invoke-virtual/range {v20 .. v20}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v20

    invoke-virtual/range {v19 .. v19}, Ljava/lang/String;->getBytes()[B

    move-result-object v21

    invoke-static/range {v20 .. v21}, Lcom/mediatek/datatransfer/utils/FileUtils;->writeToFile(Ljava/lang/String;[B)V

    :cond_b
    move-object/from16 v0, p0

    move-object/from16 v1, v16

    invoke-direct {v0, v9, v10, v1, v5}, Lcom/mediatek/datatransfer/utils/BackupFileScanner$ScanThread;->makeRootRecordXml(JLjava/lang/String;Ljava/io/File;)V

    invoke-static {v5}, Lcom/mediatek/datatransfer/utils/FileUtils;->deleteEmptyFolder(Ljava/io/File;)V

    goto/16 :goto_0
.end method

.method private makeRootRecordXml(JLjava/lang/String;Ljava/io/File;)V
    .locals 5
    .param p1    # J
    .param p3    # Ljava/lang/String;
    .param p4    # Ljava/io/File;

    new-instance v0, Lcom/mediatek/datatransfer/RecordXmlInfo;

    invoke-direct {v0}, Lcom/mediatek/datatransfer/RecordXmlInfo;-><init>()V

    const/4 v2, 0x0

    invoke-virtual {v0, v2}, Lcom/mediatek/datatransfer/RecordXmlInfo;->setRestore(Z)V

    invoke-static {}, Lcom/mediatek/datatransfer/utils/Utils;->getPhoneSearialNumber()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/mediatek/datatransfer/RecordXmlInfo;->setDevice(Ljava/lang/String;)V

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, ""

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1, p2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/mediatek/datatransfer/RecordXmlInfo;->setTime(Ljava/lang/String;)V

    new-instance v1, Lcom/mediatek/datatransfer/RecordXmlComposer;

    invoke-direct {v1}, Lcom/mediatek/datatransfer/RecordXmlComposer;-><init>()V

    invoke-virtual {v1}, Lcom/mediatek/datatransfer/RecordXmlComposer;->startCompose()Z

    invoke-virtual {v1, v0}, Lcom/mediatek/datatransfer/RecordXmlComposer;->addOneRecord(Lcom/mediatek/datatransfer/RecordXmlInfo;)Z

    invoke-virtual {v1}, Lcom/mediatek/datatransfer/RecordXmlComposer;->endCompose()Z

    if-eqz p4, :cond_0

    invoke-virtual {p4}, Ljava/io/File;->canWrite()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-virtual {v1}, Lcom/mediatek/datatransfer/RecordXmlComposer;->getXmlInfo()Ljava/lang/String;

    move-result-object v2

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    sget-object v4, Ljava/io/File;->separator:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "record.xml"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/mediatek/datatransfer/utils/Utils;->writeToFile(Ljava/lang/String;Ljava/lang/String;)V

    :cond_0
    return-void
.end method

.method private scanBackupFiles(Ljava/lang/String;)[Ljava/io/File;
    .locals 1
    .param p1    # Ljava/lang/String;

    if-eqz p1, :cond_0

    iget-boolean v0, p0, Lcom/mediatek/datatransfer/utils/BackupFileScanner$ScanThread;->isCanceled:Z

    if-nez v0, :cond_0

    new-instance v0, Ljava/io/File;

    invoke-direct {v0, p1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/io/File;->listFiles()[Ljava/io/File;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/mediatek/datatransfer/utils/BackupFileScanner$ScanThread;->filterFile([Ljava/io/File;)[Ljava/io/File;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private scanPersonalBackupFiles()[Ljava/io/File;
    .locals 2

    invoke-static {}, Lcom/mediatek/datatransfer/utils/SDCardUtils;->getPersonalDataBackupPath()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/mediatek/datatransfer/utils/BackupFileScanner$ScanThread;->scanBackupFiles(Ljava/lang/String;)[Ljava/io/File;

    move-result-object v1

    return-object v1
.end method

.method private sort(Ljava/util/List;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/mediatek/datatransfer/utils/BackupFilePreview;",
            ">;)V"
        }
    .end annotation

    new-instance v0, Lcom/mediatek/datatransfer/utils/BackupFileScanner$ScanThread$1;

    invoke-direct {v0, p0}, Lcom/mediatek/datatransfer/utils/BackupFileScanner$ScanThread$1;-><init>(Lcom/mediatek/datatransfer/utils/BackupFileScanner$ScanThread;)V

    invoke-static {p1, v0}, Ljava/util/Collections;->sort(Ljava/util/List;Ljava/util/Comparator;)V

    return-void
.end method


# virtual methods
.method public cancel()V
    .locals 1

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/mediatek/datatransfer/utils/BackupFileScanner$ScanThread;->isCanceled:Z

    return-void
.end method

.method public run()V
    .locals 1

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/mediatek/datatransfer/utils/BackupFileScanner$ScanThread;->isCanceled:Z

    iget-object v0, p0, Lcom/mediatek/datatransfer/utils/BackupFileScanner$ScanThread;->this$0:Lcom/mediatek/datatransfer/utils/BackupFileScanner;

    invoke-static {v0}, Lcom/mediatek/datatransfer/utils/BackupFileScanner;->access$100(Lcom/mediatek/datatransfer/utils/BackupFileScanner;)Ljava/util/LinkedHashSet;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/mediatek/datatransfer/utils/BackupFileScanner$ScanThread;->this$0:Lcom/mediatek/datatransfer/utils/BackupFileScanner;

    invoke-static {v0}, Lcom/mediatek/datatransfer/utils/BackupFileScanner;->access$100(Lcom/mediatek/datatransfer/utils/BackupFileScanner;)Ljava/util/LinkedHashSet;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/HashSet;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-direct {p0}, Lcom/mediatek/datatransfer/utils/BackupFileScanner$ScanThread;->detectSDcardForOtherBackups()V

    :goto_0
    return-void

    :cond_0
    invoke-direct {p0}, Lcom/mediatek/datatransfer/utils/BackupFileScanner$ScanThread;->initDataTransferBackupForList()V

    goto :goto_0
.end method
