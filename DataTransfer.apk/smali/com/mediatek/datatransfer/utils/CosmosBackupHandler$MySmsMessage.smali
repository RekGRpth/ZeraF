.class Lcom/mediatek/datatransfer/utils/CosmosBackupHandler$MySmsMessage;
.super Ljava/lang/Object;
.source "CosmosBackupHandler.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/mediatek/datatransfer/utils/CosmosBackupHandler;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "MySmsMessage"
.end annotation


# instance fields
.field message:Landroid/telephony/SmsMessage;

.field final synthetic this$0:Lcom/mediatek/datatransfer/utils/CosmosBackupHandler;

.field timeStamp:J

.field type:I


# direct methods
.method public constructor <init>(Lcom/mediatek/datatransfer/utils/CosmosBackupHandler;Landroid/telephony/SmsMessage;IJ)V
    .locals 4
    .param p2    # Landroid/telephony/SmsMessage;
    .param p3    # I
    .param p4    # J

    const-wide/16 v2, 0x0

    iput-object p1, p0, Lcom/mediatek/datatransfer/utils/CosmosBackupHandler$MySmsMessage;->this$0:Lcom/mediatek/datatransfer/utils/CosmosBackupHandler;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/mediatek/datatransfer/utils/CosmosBackupHandler$MySmsMessage;->message:Landroid/telephony/SmsMessage;

    const/4 v0, -0x1

    iput v0, p0, Lcom/mediatek/datatransfer/utils/CosmosBackupHandler$MySmsMessage;->type:I

    iput-wide v2, p0, Lcom/mediatek/datatransfer/utils/CosmosBackupHandler$MySmsMessage;->timeStamp:J

    iput-object p2, p0, Lcom/mediatek/datatransfer/utils/CosmosBackupHandler$MySmsMessage;->message:Landroid/telephony/SmsMessage;

    iput p3, p0, Lcom/mediatek/datatransfer/utils/CosmosBackupHandler$MySmsMessage;->type:I

    invoke-virtual {p2}, Landroid/telephony/SmsMessage;->getTimestampMillis()J

    move-result-wide v0

    cmp-long v0, v0, v2

    if-nez v0, :cond_0

    :goto_0
    iput-wide p4, p0, Lcom/mediatek/datatransfer/utils/CosmosBackupHandler$MySmsMessage;->timeStamp:J

    const-string v0, "CosmosBackupHandler"

    invoke-virtual {p0}, Lcom/mediatek/datatransfer/utils/CosmosBackupHandler$MySmsMessage;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/mediatek/datatransfer/utils/MyLogger;->logD(Ljava/lang/String;Ljava/lang/String;)V

    return-void

    :cond_0
    invoke-virtual {p2}, Landroid/telephony/SmsMessage;->getTimestampMillis()J

    move-result-wide p4

    goto :goto_0
.end method


# virtual methods
.method public getMessage()Landroid/telephony/SmsMessage;
    .locals 1

    iget-object v0, p0, Lcom/mediatek/datatransfer/utils/CosmosBackupHandler$MySmsMessage;->message:Landroid/telephony/SmsMessage;

    return-object v0
.end method

.method public getTimeStamp()J
    .locals 2

    iget-wide v0, p0, Lcom/mediatek/datatransfer/utils/CosmosBackupHandler$MySmsMessage;->timeStamp:J

    return-wide v0
.end method

.method public getType()I
    .locals 1

    iget v0, p0, Lcom/mediatek/datatransfer/utils/CosmosBackupHandler$MySmsMessage;->type:I

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 3

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "MySmsMessage [message="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/mediatek/datatransfer/utils/CosmosBackupHandler$MySmsMessage;->message:Landroid/telephony/SmsMessage;

    invoke-virtual {v1}, Landroid/telephony/SmsMessage;->getDisplayMessageBody()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", type="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/mediatek/datatransfer/utils/CosmosBackupHandler$MySmsMessage;->type:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", timeStamp="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-wide v1, p0, Lcom/mediatek/datatransfer/utils/CosmosBackupHandler$MySmsMessage;->timeStamp:J

    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "]"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
