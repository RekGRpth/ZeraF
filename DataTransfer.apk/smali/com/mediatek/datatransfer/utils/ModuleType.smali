.class public Lcom/mediatek/datatransfer/utils/ModuleType;
.super Ljava/lang/Object;
.source "ModuleType.java"


# static fields
.field private static final CLASS_TAG:Ljava/lang/String; = "DataTransfer/ModuleType"

.field public static final TYPE_APP:I = 0x10

.field public static final TYPE_BOOKMARK:I = 0x200

.field public static final TYPE_CALENDAR:I = 0x8

.field public static final TYPE_CONTACT:I = 0x1

.field public static final TYPE_INVALID:I = 0x0

.field public static final TYPE_MESSAGE:I = 0x40

.field public static final TYPE_MMS:I = 0x4

.field public static final TYPE_MUSIC:I = 0x80

.field public static final TYPE_NOTEBOOK:I = 0x100

.field public static final TYPE_PICTURE:I = 0x20

.field public static final TYPE_SMS:I = 0x2


# direct methods
.method private constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static getModuleStringFromType(Landroid/content/Context;I)Ljava/lang/String;
    .locals 4
    .param p0    # Landroid/content/Context;
    .param p1    # I

    const/4 v0, 0x0

    sparse-switch p1, :sswitch_data_0

    :goto_0
    const-string v1, "DataTransfer/ModuleType"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "getModuleStringFromType: resId = "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/mediatek/datatransfer/utils/MyLogger;->logD(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    return-object v1

    :sswitch_0
    const v0, 0x7f060006

    goto :goto_0

    :sswitch_1
    const v0, 0x7f060007

    goto :goto_0

    :sswitch_2
    const v0, 0x7f060009

    goto :goto_0

    :sswitch_3
    const v0, 0x7f060008

    goto :goto_0

    :sswitch_4
    const v0, 0x7f06000a

    goto :goto_0

    :sswitch_5
    const v0, 0x7f06000b

    goto :goto_0

    :sswitch_6
    const v0, 0x7f06000c

    goto :goto_0

    :sswitch_7
    const v0, 0x7f06000d

    goto :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x1 -> :sswitch_0
        0x8 -> :sswitch_2
        0x10 -> :sswitch_4
        0x20 -> :sswitch_3
        0x40 -> :sswitch_1
        0x80 -> :sswitch_5
        0x100 -> :sswitch_6
        0x200 -> :sswitch_7
    .end sparse-switch
.end method
