.class public Lcom/mediatek/datatransfer/utils/BackupFileScanner;
.super Ljava/lang/Object;
.source "BackupFileScanner.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/mediatek/datatransfer/utils/BackupFileScanner$1;,
        Lcom/mediatek/datatransfer/utils/BackupFileScanner$ScanThread;
    }
.end annotation


# static fields
.field private static final CLASS_TAG:Ljava/lang/String; = "DataTransfer/BackupFileScanner"


# instance fields
.field private mContext:Landroid/content/Context;

.field private mHandler:Landroid/os/Handler;

.field mScanThread:Lcom/mediatek/datatransfer/utils/BackupFileScanner$ScanThread;

.field private object:Ljava/lang/Object;

.field private scanTaskHandlers:Ljava/util/LinkedHashSet;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/LinkedHashSet",
            "<",
            "Lcom/mediatek/datatransfer/utils/BackupsHandler;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/os/Handler;)V
    .locals 2
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/os/Handler;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lcom/mediatek/datatransfer/utils/BackupFileScanner;->object:Ljava/lang/Object;

    new-instance v0, Ljava/util/LinkedHashSet;

    invoke-direct {v0}, Ljava/util/LinkedHashSet;-><init>()V

    iput-object v0, p0, Lcom/mediatek/datatransfer/utils/BackupFileScanner;->scanTaskHandlers:Ljava/util/LinkedHashSet;

    iput-object p2, p0, Lcom/mediatek/datatransfer/utils/BackupFileScanner;->mHandler:Landroid/os/Handler;

    iput-object p1, p0, Lcom/mediatek/datatransfer/utils/BackupFileScanner;->mContext:Landroid/content/Context;

    iget-object v0, p0, Lcom/mediatek/datatransfer/utils/BackupFileScanner;->scanTaskHandlers:Ljava/util/LinkedHashSet;

    invoke-virtual {v0}, Ljava/util/HashSet;->clear()V

    iget-object v0, p0, Lcom/mediatek/datatransfer/utils/BackupFileScanner;->mHandler:Landroid/os/Handler;

    if-nez v0, :cond_0

    const-string v0, "DataTransfer/BackupFileScanner"

    const-string v1, "constuctor maybe failed!cause mHandler is null"

    invoke-static {v0, v1}, Lcom/mediatek/datatransfer/utils/MyLogger;->logE(Ljava/lang/String;Ljava/lang/String;)V

    :cond_0
    return-void
.end method

.method static synthetic access$100(Lcom/mediatek/datatransfer/utils/BackupFileScanner;)Ljava/util/LinkedHashSet;
    .locals 1
    .param p0    # Lcom/mediatek/datatransfer/utils/BackupFileScanner;

    iget-object v0, p0, Lcom/mediatek/datatransfer/utils/BackupFileScanner;->scanTaskHandlers:Ljava/util/LinkedHashSet;

    return-object v0
.end method

.method static synthetic access$200(Lcom/mediatek/datatransfer/utils/BackupFileScanner;)Landroid/content/Context;
    .locals 1
    .param p0    # Lcom/mediatek/datatransfer/utils/BackupFileScanner;

    iget-object v0, p0, Lcom/mediatek/datatransfer/utils/BackupFileScanner;->mContext:Landroid/content/Context;

    return-object v0
.end method

.method static synthetic access$300(Lcom/mediatek/datatransfer/utils/BackupFileScanner;)Ljava/lang/Object;
    .locals 1
    .param p0    # Lcom/mediatek/datatransfer/utils/BackupFileScanner;

    iget-object v0, p0, Lcom/mediatek/datatransfer/utils/BackupFileScanner;->object:Ljava/lang/Object;

    return-object v0
.end method

.method static synthetic access$400(Lcom/mediatek/datatransfer/utils/BackupFileScanner;)Landroid/os/Handler;
    .locals 1
    .param p0    # Lcom/mediatek/datatransfer/utils/BackupFileScanner;

    iget-object v0, p0, Lcom/mediatek/datatransfer/utils/BackupFileScanner;->mHandler:Landroid/os/Handler;

    return-object v0
.end method


# virtual methods
.method public addScanHandler(Lcom/mediatek/datatransfer/utils/BackupsHandler;)Z
    .locals 1
    .param p1    # Lcom/mediatek/datatransfer/utils/BackupsHandler;

    iget-object v0, p0, Lcom/mediatek/datatransfer/utils/BackupFileScanner;->scanTaskHandlers:Ljava/util/LinkedHashSet;

    invoke-virtual {v0, p1}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public isRunning()Z
    .locals 1

    iget-object v0, p0, Lcom/mediatek/datatransfer/utils/BackupFileScanner;->mScanThread:Lcom/mediatek/datatransfer/utils/BackupFileScanner$ScanThread;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/mediatek/datatransfer/utils/BackupFileScanner;->mScanThread:Lcom/mediatek/datatransfer/utils/BackupFileScanner$ScanThread;

    iget-boolean v0, v0, Lcom/mediatek/datatransfer/utils/BackupFileScanner$ScanThread;->isCanceled:Z

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public quitScan()V
    .locals 3

    iget-object v1, p0, Lcom/mediatek/datatransfer/utils/BackupFileScanner;->object:Ljava/lang/Object;

    monitor-enter v1

    :try_start_0
    iget-object v0, p0, Lcom/mediatek/datatransfer/utils/BackupFileScanner;->mScanThread:Lcom/mediatek/datatransfer/utils/BackupFileScanner$ScanThread;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/mediatek/datatransfer/utils/BackupFileScanner;->mScanThread:Lcom/mediatek/datatransfer/utils/BackupFileScanner$ScanThread;

    invoke-virtual {v0}, Lcom/mediatek/datatransfer/utils/BackupFileScanner$ScanThread;->cancel()V

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/mediatek/datatransfer/utils/BackupFileScanner;->mScanThread:Lcom/mediatek/datatransfer/utils/BackupFileScanner$ScanThread;

    const-string v0, "DataTransfer/BackupFileScanner"

    const-string v2, "quitScan"

    invoke-static {v0, v2}, Lcom/mediatek/datatransfer/utils/MyLogger;->logV(Ljava/lang/String;Ljava/lang/String;)V

    :cond_0
    monitor-exit v1

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public setHandler(Landroid/os/Handler;)V
    .locals 2
    .param p1    # Landroid/os/Handler;

    iget-object v1, p0, Lcom/mediatek/datatransfer/utils/BackupFileScanner;->object:Ljava/lang/Object;

    monitor-enter v1

    :try_start_0
    iput-object p1, p0, Lcom/mediatek/datatransfer/utils/BackupFileScanner;->mHandler:Landroid/os/Handler;

    monitor-exit v1

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public startScan()V
    .locals 2

    new-instance v0, Lcom/mediatek/datatransfer/utils/BackupFileScanner$ScanThread;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/mediatek/datatransfer/utils/BackupFileScanner$ScanThread;-><init>(Lcom/mediatek/datatransfer/utils/BackupFileScanner;Lcom/mediatek/datatransfer/utils/BackupFileScanner$1;)V

    iput-object v0, p0, Lcom/mediatek/datatransfer/utils/BackupFileScanner;->mScanThread:Lcom/mediatek/datatransfer/utils/BackupFileScanner$ScanThread;

    iget-object v0, p0, Lcom/mediatek/datatransfer/utils/BackupFileScanner;->mScanThread:Lcom/mediatek/datatransfer/utils/BackupFileScanner$ScanThread;

    invoke-virtual {v0}, Ljava/lang/Thread;->start()V

    return-void
.end method
