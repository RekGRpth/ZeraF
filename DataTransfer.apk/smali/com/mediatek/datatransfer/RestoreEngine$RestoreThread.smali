.class public Lcom/mediatek/datatransfer/RestoreEngine$RestoreThread;
.super Ljava/lang/Thread;
.source "RestoreEngine.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/mediatek/datatransfer/RestoreEngine;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1
    name = "RestoreThread"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/mediatek/datatransfer/RestoreEngine;


# direct methods
.method public constructor <init>(Lcom/mediatek/datatransfer/RestoreEngine;)V
    .locals 0

    iput-object p1, p0, Lcom/mediatek/datatransfer/RestoreEngine$RestoreThread;->this$0:Lcom/mediatek/datatransfer/RestoreEngine;

    invoke-direct {p0}, Ljava/lang/Thread;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 7

    const-string v3, "DataTransfer/RestoreEngine"

    const-string v4, "RestoreThread begin..."

    invoke-static {v3, v4}, Lcom/mediatek/datatransfer/utils/MyLogger;->logD(Ljava/lang/String;Ljava/lang/String;)V

    const-wide/16 v3, 0x3e8

    :try_start_0
    invoke-static {v3, v4}, Ljava/lang/Thread;->sleep(J)V
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    iget-object v3, p0, Lcom/mediatek/datatransfer/RestoreEngine$RestoreThread;->this$0:Lcom/mediatek/datatransfer/RestoreEngine;

    invoke-static {v3}, Lcom/mediatek/datatransfer/RestoreEngine;->access$000(Lcom/mediatek/datatransfer/RestoreEngine;)Ljava/util/List;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_3

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/mediatek/datatransfer/modules/Composer;

    const-string v3, "DataTransfer/RestoreEngine"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "RestoreThread composer:"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v0}, Lcom/mediatek/datatransfer/modules/Composer;->getModuleType()I

    move-result v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " start.."

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/mediatek/datatransfer/utils/MyLogger;->logD(Ljava/lang/String;Ljava/lang/String;)V

    const-string v3, "DataTransfer/RestoreEngine"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "begin restore:"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v5

    invoke-virtual {v4, v5, v6}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/mediatek/datatransfer/utils/MyLogger;->logD(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v0}, Lcom/mediatek/datatransfer/modules/Composer;->isCancel()Z

    move-result v3

    if-nez v3, :cond_2

    invoke-virtual {v0}, Lcom/mediatek/datatransfer/modules/Composer;->init()Z

    move-result v3

    if-nez v3, :cond_0

    invoke-virtual {v0}, Lcom/mediatek/datatransfer/modules/Composer;->onEnd()V

    goto :goto_1

    :catch_0
    move-exception v1

    invoke-virtual {v1}, Ljava/lang/Throwable;->printStackTrace()V

    goto :goto_0

    :cond_0
    const-string v3, "DataTransfer/RestoreEngine"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "RestoreThread composer:"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v0}, Lcom/mediatek/datatransfer/modules/Composer;->getModuleType()I

    move-result v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " init finish"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/mediatek/datatransfer/utils/MyLogger;->logD(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v0}, Lcom/mediatek/datatransfer/modules/Composer;->onStart()V

    :goto_2
    invoke-virtual {v0}, Lcom/mediatek/datatransfer/modules/Composer;->isAfterLast()Z

    move-result v3

    if-nez v3, :cond_2

    invoke-virtual {v0}, Lcom/mediatek/datatransfer/modules/Composer;->isCancel()Z

    move-result v3

    if-nez v3, :cond_2

    iget-object v3, p0, Lcom/mediatek/datatransfer/RestoreEngine$RestoreThread;->this$0:Lcom/mediatek/datatransfer/RestoreEngine;

    invoke-static {v3}, Lcom/mediatek/datatransfer/RestoreEngine;->access$100(Lcom/mediatek/datatransfer/RestoreEngine;)Z

    move-result v3

    if-eqz v3, :cond_1

    iget-object v3, p0, Lcom/mediatek/datatransfer/RestoreEngine$RestoreThread;->this$0:Lcom/mediatek/datatransfer/RestoreEngine;

    invoke-static {v3}, Lcom/mediatek/datatransfer/RestoreEngine;->access$200(Lcom/mediatek/datatransfer/RestoreEngine;)Ljava/lang/Object;

    move-result-object v4

    monitor-enter v4

    :try_start_1
    const-string v3, "DataTransfer/RestoreEngine"

    const-string v5, "RestoreThread wait..."

    invoke-static {v3, v5}, Lcom/mediatek/datatransfer/utils/MyLogger;->logD(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v3, p0, Lcom/mediatek/datatransfer/RestoreEngine$RestoreThread;->this$0:Lcom/mediatek/datatransfer/RestoreEngine;

    invoke-static {v3}, Lcom/mediatek/datatransfer/RestoreEngine;->access$200(Lcom/mediatek/datatransfer/RestoreEngine;)Ljava/lang/Object;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Object;->wait()V
    :try_end_1
    .catch Ljava/lang/InterruptedException; {:try_start_1 .. :try_end_1} :catch_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    :goto_3
    :try_start_2
    monitor-exit v4
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    :cond_1
    invoke-virtual {v0}, Lcom/mediatek/datatransfer/modules/Composer;->composeOneEntity()Z

    const-string v3, "DataTransfer/RestoreEngine"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "RestoreThread composer:"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v0}, Lcom/mediatek/datatransfer/modules/Composer;->getModuleType()I

    move-result v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "composer one entiry"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/mediatek/datatransfer/utils/MyLogger;->logD(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_2

    :catch_1
    move-exception v1

    :try_start_3
    invoke-virtual {v1}, Ljava/lang/Throwable;->printStackTrace()V

    goto :goto_3

    :catchall_0
    move-exception v3

    monitor-exit v4
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    throw v3

    :cond_2
    const-wide/16 v3, 0xc8

    :try_start_4
    invoke-static {v3, v4}, Lcom/mediatek/datatransfer/RestoreEngine$RestoreThread;->sleep(J)V
    :try_end_4
    .catch Ljava/lang/InterruptedException; {:try_start_4 .. :try_end_4} :catch_2

    :goto_4
    invoke-virtual {v0}, Lcom/mediatek/datatransfer/modules/Composer;->onEnd()V

    const-string v3, "DataTransfer/RestoreEngine"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "end restore:"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v5

    invoke-virtual {v4, v5, v6}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/mediatek/datatransfer/utils/MyLogger;->logD(Ljava/lang/String;Ljava/lang/String;)V

    const-string v3, "DataTransfer/RestoreEngine"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "RestoreThread composer:"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v0}, Lcom/mediatek/datatransfer/modules/Composer;->getModuleType()I

    move-result v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " composer finish"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/mediatek/datatransfer/utils/MyLogger;->logD(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_1

    :catch_2
    move-exception v1

    invoke-virtual {v1}, Ljava/lang/Throwable;->printStackTrace()V

    goto :goto_4

    :cond_3
    const-string v3, "DataTransfer/RestoreEngine"

    const-string v4, "RestoreThread run finish"

    invoke-static {v3, v4}, Lcom/mediatek/datatransfer/utils/MyLogger;->logD(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v3, p0, Lcom/mediatek/datatransfer/RestoreEngine$RestoreThread;->this$0:Lcom/mediatek/datatransfer/RestoreEngine;

    const/4 v4, 0x0

    invoke-static {v3, v4}, Lcom/mediatek/datatransfer/RestoreEngine;->access$302(Lcom/mediatek/datatransfer/RestoreEngine;Z)Z

    iget-object v3, p0, Lcom/mediatek/datatransfer/RestoreEngine$RestoreThread;->this$0:Lcom/mediatek/datatransfer/RestoreEngine;

    invoke-static {v3}, Lcom/mediatek/datatransfer/RestoreEngine;->access$400(Lcom/mediatek/datatransfer/RestoreEngine;)Lcom/mediatek/datatransfer/RestoreEngine$OnRestoreDoneListner;

    move-result-object v3

    if-eqz v3, :cond_4

    iget-object v3, p0, Lcom/mediatek/datatransfer/RestoreEngine$RestoreThread;->this$0:Lcom/mediatek/datatransfer/RestoreEngine;

    invoke-static {v3}, Lcom/mediatek/datatransfer/RestoreEngine;->access$400(Lcom/mediatek/datatransfer/RestoreEngine;)Lcom/mediatek/datatransfer/RestoreEngine$OnRestoreDoneListner;

    move-result-object v3

    const/4 v4, 0x1

    invoke-interface {v3, v4}, Lcom/mediatek/datatransfer/RestoreEngine$OnRestoreDoneListner;->onFinishRestore(Z)V

    :cond_4
    return-void
.end method
