.class public Lcom/mediatek/datatransfer/RestoreService;
.super Landroid/app/Service;
.source "RestoreService.java"

# interfaces
.implements Lcom/mediatek/datatransfer/ProgressReporter;
.implements Lcom/mediatek/datatransfer/RestoreEngine$OnRestoreDoneListner;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/mediatek/datatransfer/RestoreService$RestoreBinder;,
        Lcom/mediatek/datatransfer/RestoreService$RestoreProgress;,
        Lcom/mediatek/datatransfer/RestoreService$OnRestoreStatusListener;
    }
.end annotation


# static fields
.field private static final CLASS_TAG:Ljava/lang/String; = "DataTransfer/RestoreService"


# instance fields
.field private mAppResultList:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/mediatek/datatransfer/ResultDialog$ResultEntity;",
            ">;"
        }
    .end annotation
.end field

.field private mBinder:Lcom/mediatek/datatransfer/RestoreService$RestoreBinder;

.field private mCurrentProgress:Lcom/mediatek/datatransfer/RestoreService$RestoreProgress;

.field mParasMap:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/Integer;",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;>;"
        }
    .end annotation
.end field

.field private mRestoreEngine:Lcom/mediatek/datatransfer/RestoreEngine;

.field private mRestoreStatusListener:Lcom/mediatek/datatransfer/RestoreService$OnRestoreStatusListener;

.field private mResultList:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/mediatek/datatransfer/ResultDialog$ResultEntity;",
            ">;"
        }
    .end annotation
.end field

.field private mState:I


# direct methods
.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Landroid/app/Service;-><init>()V

    new-instance v0, Lcom/mediatek/datatransfer/RestoreService$RestoreBinder;

    invoke-direct {v0, p0}, Lcom/mediatek/datatransfer/RestoreService$RestoreBinder;-><init>(Lcom/mediatek/datatransfer/RestoreService;)V

    iput-object v0, p0, Lcom/mediatek/datatransfer/RestoreService;->mBinder:Lcom/mediatek/datatransfer/RestoreService$RestoreBinder;

    new-instance v0, Lcom/mediatek/datatransfer/RestoreService$RestoreProgress;

    invoke-direct {v0}, Lcom/mediatek/datatransfer/RestoreService$RestoreProgress;-><init>()V

    iput-object v0, p0, Lcom/mediatek/datatransfer/RestoreService;->mCurrentProgress:Lcom/mediatek/datatransfer/RestoreService$RestoreProgress;

    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/mediatek/datatransfer/RestoreService;->mParasMap:Ljava/util/HashMap;

    return-void
.end method

.method static synthetic access$000(Lcom/mediatek/datatransfer/RestoreService;)I
    .locals 1
    .param p0    # Lcom/mediatek/datatransfer/RestoreService;

    invoke-direct {p0}, Lcom/mediatek/datatransfer/RestoreService;->getRestoreState()I

    move-result v0

    return v0
.end method

.method static synthetic access$100(Lcom/mediatek/datatransfer/RestoreService;)Lcom/mediatek/datatransfer/RestoreEngine;
    .locals 1
    .param p0    # Lcom/mediatek/datatransfer/RestoreService;

    iget-object v0, p0, Lcom/mediatek/datatransfer/RestoreService;->mRestoreEngine:Lcom/mediatek/datatransfer/RestoreEngine;

    return-object v0
.end method

.method static synthetic access$102(Lcom/mediatek/datatransfer/RestoreService;Lcom/mediatek/datatransfer/RestoreEngine;)Lcom/mediatek/datatransfer/RestoreEngine;
    .locals 0
    .param p0    # Lcom/mediatek/datatransfer/RestoreService;
    .param p1    # Lcom/mediatek/datatransfer/RestoreEngine;

    iput-object p1, p0, Lcom/mediatek/datatransfer/RestoreService;->mRestoreEngine:Lcom/mediatek/datatransfer/RestoreEngine;

    return-object p1
.end method

.method static synthetic access$200(Lcom/mediatek/datatransfer/RestoreService;)Ljava/util/ArrayList;
    .locals 1
    .param p0    # Lcom/mediatek/datatransfer/RestoreService;

    iget-object v0, p0, Lcom/mediatek/datatransfer/RestoreService;->mResultList:Ljava/util/ArrayList;

    return-object v0
.end method

.method static synthetic access$300(Lcom/mediatek/datatransfer/RestoreService;)Ljava/util/ArrayList;
    .locals 1
    .param p0    # Lcom/mediatek/datatransfer/RestoreService;

    iget-object v0, p0, Lcom/mediatek/datatransfer/RestoreService;->mAppResultList:Ljava/util/ArrayList;

    return-object v0
.end method

.method static synthetic access$400(Lcom/mediatek/datatransfer/RestoreService;)Lcom/mediatek/datatransfer/RestoreService$RestoreProgress;
    .locals 1
    .param p0    # Lcom/mediatek/datatransfer/RestoreService;

    iget-object v0, p0, Lcom/mediatek/datatransfer/RestoreService;->mCurrentProgress:Lcom/mediatek/datatransfer/RestoreService$RestoreProgress;

    return-object v0
.end method

.method static synthetic access$502(Lcom/mediatek/datatransfer/RestoreService;Lcom/mediatek/datatransfer/RestoreService$OnRestoreStatusListener;)Lcom/mediatek/datatransfer/RestoreService$OnRestoreStatusListener;
    .locals 0
    .param p0    # Lcom/mediatek/datatransfer/RestoreService;
    .param p1    # Lcom/mediatek/datatransfer/RestoreService$OnRestoreStatusListener;

    iput-object p1, p0, Lcom/mediatek/datatransfer/RestoreService;->mRestoreStatusListener:Lcom/mediatek/datatransfer/RestoreService$OnRestoreStatusListener;

    return-object p1
.end method

.method private getRestoreState()I
    .locals 1

    monitor-enter p0

    :try_start_0
    iget v0, p0, Lcom/mediatek/datatransfer/RestoreService;->mState:I

    monitor-exit p0

    return v0

    :catchall_0
    move-exception v0

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method


# virtual methods
.method public moveToState(I)V
    .locals 1
    .param p1    # I

    monitor-enter p0

    :try_start_0
    iput p1, p0, Lcom/mediatek/datatransfer/RestoreService;->mState:I

    monitor-exit p0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public onBind(Landroid/content/Intent;)Landroid/os/IBinder;
    .locals 2
    .param p1    # Landroid/content/Intent;

    const-string v0, "DataTransfer/RestoreService"

    const-string v1, "onbind"

    invoke-static {v0, v1}, Lcom/mediatek/datatransfer/utils/MyLogger;->logI(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/mediatek/datatransfer/RestoreService;->mBinder:Lcom/mediatek/datatransfer/RestoreService$RestoreBinder;

    return-object v0
.end method

.method public onCreate()V
    .locals 2

    invoke-super {p0}, Landroid/app/Service;->onCreate()V

    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/mediatek/datatransfer/RestoreService;->moveToState(I)V

    const-string v0, "DataTransfer/RestoreService"

    const-string v1, "onCreate"

    invoke-static {v0, v1}, Lcom/mediatek/datatransfer/utils/MyLogger;->logI(Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method public onDestroy()V
    .locals 2

    invoke-super {p0}, Landroid/app/Service;->onDestroy()V

    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Landroid/app/Service;->stopForeground(Z)V

    const-string v0, "DataTransfer/RestoreService"

    const-string v1, "onDestroy"

    invoke-static {v0, v1}, Lcom/mediatek/datatransfer/utils/MyLogger;->logI(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/mediatek/datatransfer/RestoreService;->mRestoreEngine:Lcom/mediatek/datatransfer/RestoreEngine;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/mediatek/datatransfer/RestoreService;->mRestoreEngine:Lcom/mediatek/datatransfer/RestoreEngine;

    invoke-virtual {v0}, Lcom/mediatek/datatransfer/RestoreEngine;->isRunning()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/mediatek/datatransfer/RestoreService;->mRestoreEngine:Lcom/mediatek/datatransfer/RestoreEngine;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/mediatek/datatransfer/RestoreEngine;->setOnRestoreEndListner(Lcom/mediatek/datatransfer/RestoreEngine$OnRestoreDoneListner;)V

    iget-object v0, p0, Lcom/mediatek/datatransfer/RestoreService;->mRestoreEngine:Lcom/mediatek/datatransfer/RestoreEngine;

    invoke-virtual {v0}, Lcom/mediatek/datatransfer/RestoreEngine;->cancel()V

    :cond_0
    return-void
.end method

.method public onEnd(Lcom/mediatek/datatransfer/modules/Composer;Z)V
    .locals 3
    .param p1    # Lcom/mediatek/datatransfer/modules/Composer;
    .param p2    # Z

    iget-object v1, p0, Lcom/mediatek/datatransfer/RestoreService;->mResultList:Ljava/util/ArrayList;

    if-nez v1, :cond_0

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lcom/mediatek/datatransfer/RestoreService;->mResultList:Ljava/util/ArrayList;

    :cond_0
    new-instance v0, Lcom/mediatek/datatransfer/ResultDialog$ResultEntity;

    invoke-virtual {p1}, Lcom/mediatek/datatransfer/modules/Composer;->getModuleType()I

    move-result v2

    if-eqz p2, :cond_1

    const/4 v1, 0x0

    :goto_0
    invoke-direct {v0, v2, v1}, Lcom/mediatek/datatransfer/ResultDialog$ResultEntity;-><init>(II)V

    iget-object v1, p0, Lcom/mediatek/datatransfer/RestoreService;->mResultList:Ljava/util/ArrayList;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    return-void

    :cond_1
    const/4 v1, -0x1

    goto :goto_0
.end method

.method public onErr(Ljava/io/IOException;)V
    .locals 1
    .param p1    # Ljava/io/IOException;

    iget-object v0, p0, Lcom/mediatek/datatransfer/RestoreService;->mRestoreStatusListener:Lcom/mediatek/datatransfer/RestoreService$OnRestoreStatusListener;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/mediatek/datatransfer/RestoreService;->mRestoreStatusListener:Lcom/mediatek/datatransfer/RestoreService$OnRestoreStatusListener;

    invoke-interface {v0, p1}, Lcom/mediatek/datatransfer/RestoreService$OnRestoreStatusListener;->onRestoreErr(Ljava/io/IOException;)V

    :cond_0
    return-void
.end method

.method public onFinishRestore(Z)V
    .locals 2
    .param p1    # Z

    const/4 v0, 0x5

    invoke-virtual {p0, v0}, Lcom/mediatek/datatransfer/RestoreService;->moveToState(I)V

    iget-object v0, p0, Lcom/mediatek/datatransfer/RestoreService;->mRestoreStatusListener:Lcom/mediatek/datatransfer/RestoreService$OnRestoreStatusListener;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/mediatek/datatransfer/RestoreService;->mRestoreStatusListener:Lcom/mediatek/datatransfer/RestoreService$OnRestoreStatusListener;

    iget-object v1, p0, Lcom/mediatek/datatransfer/RestoreService;->mResultList:Ljava/util/ArrayList;

    invoke-interface {v0, p1, v1}, Lcom/mediatek/datatransfer/RestoreService$OnRestoreStatusListener;->onRestoreEnd(ZLjava/util/ArrayList;)V

    :cond_0
    invoke-static {p0}, Lcom/mediatek/datatransfer/utils/NotifyManager;->getInstance(Landroid/content/Context;)Lcom/mediatek/datatransfer/utils/NotifyManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/mediatek/datatransfer/utils/NotifyManager;->clearNotification()V

    return-void
.end method

.method public onOneFinished(Lcom/mediatek/datatransfer/modules/Composer;Z)V
    .locals 5
    .param p1    # Lcom/mediatek/datatransfer/modules/Composer;
    .param p2    # Z

    const/16 v3, 0x10

    iget-object v1, p0, Lcom/mediatek/datatransfer/RestoreService;->mCurrentProgress:Lcom/mediatek/datatransfer/RestoreService$RestoreProgress;

    iget v2, v1, Lcom/mediatek/datatransfer/RestoreService$RestoreProgress;->mCurNum:I

    add-int/lit8 v2, v2, 0x1

    iput v2, v1, Lcom/mediatek/datatransfer/RestoreService$RestoreProgress;->mCurNum:I

    invoke-virtual {p1}, Lcom/mediatek/datatransfer/modules/Composer;->getModuleType()I

    move-result v1

    if-ne v1, v3, :cond_1

    iget-object v1, p0, Lcom/mediatek/datatransfer/RestoreService;->mAppResultList:Ljava/util/ArrayList;

    if-nez v1, :cond_0

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lcom/mediatek/datatransfer/RestoreService;->mAppResultList:Ljava/util/ArrayList;

    :cond_0
    new-instance v0, Lcom/mediatek/datatransfer/ResultDialog$ResultEntity;

    if-eqz p2, :cond_4

    const/4 v1, 0x0

    :goto_0
    invoke-direct {v0, v3, v1}, Lcom/mediatek/datatransfer/ResultDialog$ResultEntity;-><init>(II)V

    iget-object v1, p0, Lcom/mediatek/datatransfer/RestoreService;->mParasMap:Ljava/util/HashMap;

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/ArrayList;

    iget-object v2, p0, Lcom/mediatek/datatransfer/RestoreService;->mCurrentProgress:Lcom/mediatek/datatransfer/RestoreService$RestoreProgress;

    iget v2, v2, Lcom/mediatek/datatransfer/RestoreService$RestoreProgress;->mCurNum:I

    add-int/lit8 v2, v2, -0x1

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/mediatek/datatransfer/ResultDialog$ResultEntity;->setKey(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/mediatek/datatransfer/RestoreService;->mAppResultList:Ljava/util/ArrayList;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :cond_1
    iget-object v1, p0, Lcom/mediatek/datatransfer/RestoreService;->mRestoreStatusListener:Lcom/mediatek/datatransfer/RestoreService$OnRestoreStatusListener;

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/mediatek/datatransfer/RestoreService;->mRestoreStatusListener:Lcom/mediatek/datatransfer/RestoreService$OnRestoreStatusListener;

    iget-object v2, p0, Lcom/mediatek/datatransfer/RestoreService;->mCurrentProgress:Lcom/mediatek/datatransfer/RestoreService$RestoreProgress;

    iget v2, v2, Lcom/mediatek/datatransfer/RestoreService$RestoreProgress;->mCurNum:I

    invoke-interface {v1, p1, v2}, Lcom/mediatek/datatransfer/RestoreService$OnRestoreStatusListener;->onProgressChanged(Lcom/mediatek/datatransfer/modules/Composer;I)V

    :cond_2
    iget-object v1, p0, Lcom/mediatek/datatransfer/RestoreService;->mCurrentProgress:Lcom/mediatek/datatransfer/RestoreService$RestoreProgress;

    iget v1, v1, Lcom/mediatek/datatransfer/RestoreService$RestoreProgress;->mMax:I

    if-eqz v1, :cond_3

    invoke-static {p0}, Lcom/mediatek/datatransfer/utils/NotifyManager;->getInstance(Landroid/content/Context;)Lcom/mediatek/datatransfer/utils/NotifyManager;

    move-result-object v1

    invoke-virtual {p1}, Lcom/mediatek/datatransfer/modules/Composer;->getModuleType()I

    move-result v2

    invoke-static {p0, v2}, Lcom/mediatek/datatransfer/utils/ModuleType;->getModuleStringFromType(Landroid/content/Context;I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1}, Lcom/mediatek/datatransfer/modules/Composer;->getModuleType()I

    move-result v3

    iget-object v4, p0, Lcom/mediatek/datatransfer/RestoreService;->mCurrentProgress:Lcom/mediatek/datatransfer/RestoreService$RestoreProgress;

    iget v4, v4, Lcom/mediatek/datatransfer/RestoreService$RestoreProgress;->mCurNum:I

    invoke-virtual {v1, v2, v3, v4}, Lcom/mediatek/datatransfer/utils/NotifyManager;->showRestoreNotification(Ljava/lang/String;II)V

    :cond_3
    return-void

    :cond_4
    const/4 v1, -0x1

    goto :goto_0
.end method

.method public onStart(Lcom/mediatek/datatransfer/modules/Composer;)V
    .locals 3
    .param p1    # Lcom/mediatek/datatransfer/modules/Composer;

    iget-object v0, p0, Lcom/mediatek/datatransfer/RestoreService;->mCurrentProgress:Lcom/mediatek/datatransfer/RestoreService$RestoreProgress;

    invoke-virtual {p1}, Lcom/mediatek/datatransfer/modules/Composer;->getModuleType()I

    move-result v1

    iput v1, v0, Lcom/mediatek/datatransfer/RestoreService$RestoreProgress;->mType:I

    iget-object v0, p0, Lcom/mediatek/datatransfer/RestoreService;->mCurrentProgress:Lcom/mediatek/datatransfer/RestoreService$RestoreProgress;

    invoke-virtual {p1}, Lcom/mediatek/datatransfer/modules/Composer;->getCount()I

    move-result v1

    iput v1, v0, Lcom/mediatek/datatransfer/RestoreService$RestoreProgress;->mMax:I

    iget-object v0, p0, Lcom/mediatek/datatransfer/RestoreService;->mCurrentProgress:Lcom/mediatek/datatransfer/RestoreService$RestoreProgress;

    const/4 v1, 0x0

    iput v1, v0, Lcom/mediatek/datatransfer/RestoreService$RestoreProgress;->mCurNum:I

    iget-object v0, p0, Lcom/mediatek/datatransfer/RestoreService;->mRestoreStatusListener:Lcom/mediatek/datatransfer/RestoreService$OnRestoreStatusListener;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/mediatek/datatransfer/RestoreService;->mRestoreStatusListener:Lcom/mediatek/datatransfer/RestoreService$OnRestoreStatusListener;

    iget-object v1, p0, Lcom/mediatek/datatransfer/RestoreService;->mCurrentProgress:Lcom/mediatek/datatransfer/RestoreService$RestoreProgress;

    iget v1, v1, Lcom/mediatek/datatransfer/RestoreService$RestoreProgress;->mType:I

    iget-object v2, p0, Lcom/mediatek/datatransfer/RestoreService;->mCurrentProgress:Lcom/mediatek/datatransfer/RestoreService$RestoreProgress;

    iget v2, v2, Lcom/mediatek/datatransfer/RestoreService$RestoreProgress;->mMax:I

    invoke-interface {v0, v1, v2}, Lcom/mediatek/datatransfer/RestoreService$OnRestoreStatusListener;->onComposerChanged(II)V

    :cond_0
    iget-object v0, p0, Lcom/mediatek/datatransfer/RestoreService;->mCurrentProgress:Lcom/mediatek/datatransfer/RestoreService$RestoreProgress;

    iget v0, v0, Lcom/mediatek/datatransfer/RestoreService$RestoreProgress;->mMax:I

    if-eqz v0, :cond_1

    invoke-static {p0}, Lcom/mediatek/datatransfer/utils/NotifyManager;->getInstance(Landroid/content/Context;)Lcom/mediatek/datatransfer/utils/NotifyManager;

    move-result-object v0

    iget-object v1, p0, Lcom/mediatek/datatransfer/RestoreService;->mCurrentProgress:Lcom/mediatek/datatransfer/RestoreService$RestoreProgress;

    iget v1, v1, Lcom/mediatek/datatransfer/RestoreService$RestoreProgress;->mMax:I

    invoke-virtual {v0, v1}, Lcom/mediatek/datatransfer/utils/NotifyManager;->setMaxPercent(I)V

    :cond_1
    return-void
.end method

.method public onStartCommand(Landroid/content/Intent;II)I
    .locals 2
    .param p1    # Landroid/content/Intent;
    .param p2    # I
    .param p3    # I

    invoke-super {p0, p1, p2, p3}, Landroid/app/Service;->onStartCommand(Landroid/content/Intent;II)I

    const-string v0, "DataTransfer/RestoreService"

    const-string v1, "onStartCommand"

    invoke-static {v0, v1}, Lcom/mediatek/datatransfer/utils/MyLogger;->logI(Ljava/lang/String;Ljava/lang/String;)V

    const/4 v0, 0x0

    return v0
.end method

.method public onUnbind(Landroid/content/Intent;)Z
    .locals 2
    .param p1    # Landroid/content/Intent;

    invoke-super {p0, p1}, Landroid/app/Service;->onUnbind(Landroid/content/Intent;)Z

    const-string v0, "DataTransfer/RestoreService"

    const-string v1, "onUnbind"

    invoke-static {v0, v1}, Lcom/mediatek/datatransfer/utils/MyLogger;->logI(Ljava/lang/String;Ljava/lang/String;)V

    const/4 v0, 0x1

    return v0
.end method
