.class Lcom/mediatek/datatransfer/AbstractRestoreActivity$9;
.super Ljava/lang/Object;
.source "AbstractRestoreActivity.java"

# interfaces
.implements Landroid/content/ServiceConnection;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/mediatek/datatransfer/AbstractRestoreActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/mediatek/datatransfer/AbstractRestoreActivity;


# direct methods
.method constructor <init>(Lcom/mediatek/datatransfer/AbstractRestoreActivity;)V
    .locals 0

    iput-object p1, p0, Lcom/mediatek/datatransfer/AbstractRestoreActivity$9;->this$0:Lcom/mediatek/datatransfer/AbstractRestoreActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onServiceConnected(Landroid/content/ComponentName;Landroid/os/IBinder;)V
    .locals 2
    .param p1    # Landroid/content/ComponentName;
    .param p2    # Landroid/os/IBinder;

    iget-object v0, p0, Lcom/mediatek/datatransfer/AbstractRestoreActivity$9;->this$0:Lcom/mediatek/datatransfer/AbstractRestoreActivity;

    check-cast p2, Lcom/mediatek/datatransfer/RestoreService$RestoreBinder;

    iput-object p2, v0, Lcom/mediatek/datatransfer/AbstractRestoreActivity;->mRestoreService:Lcom/mediatek/datatransfer/RestoreService$RestoreBinder;

    iget-object v0, p0, Lcom/mediatek/datatransfer/AbstractRestoreActivity$9;->this$0:Lcom/mediatek/datatransfer/AbstractRestoreActivity;

    iget-object v0, v0, Lcom/mediatek/datatransfer/AbstractRestoreActivity;->mRestoreService:Lcom/mediatek/datatransfer/RestoreService$RestoreBinder;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/mediatek/datatransfer/AbstractRestoreActivity$9;->this$0:Lcom/mediatek/datatransfer/AbstractRestoreActivity;

    iget-object v0, v0, Lcom/mediatek/datatransfer/AbstractRestoreActivity;->mRestoreService:Lcom/mediatek/datatransfer/RestoreService$RestoreBinder;

    iget-object v1, p0, Lcom/mediatek/datatransfer/AbstractRestoreActivity$9;->this$0:Lcom/mediatek/datatransfer/AbstractRestoreActivity;

    iget-object v1, v1, Lcom/mediatek/datatransfer/AbstractRestoreActivity;->mRestoreListener:Lcom/mediatek/datatransfer/RestoreService$OnRestoreStatusListener;

    invoke-virtual {v0, v1}, Lcom/mediatek/datatransfer/RestoreService$RestoreBinder;->setOnRestoreChangedListner(Lcom/mediatek/datatransfer/RestoreService$OnRestoreStatusListener;)V

    iget-object v0, p0, Lcom/mediatek/datatransfer/AbstractRestoreActivity$9;->this$0:Lcom/mediatek/datatransfer/AbstractRestoreActivity;

    invoke-virtual {v0}, Lcom/mediatek/datatransfer/AbstractRestoreActivity;->afterServiceConnected()V

    :cond_0
    const-string v0, "DataTransfer/AbstractRestoreActivity"

    const-string v1, " onServiceConnected"

    invoke-static {v0, v1}, Lcom/mediatek/datatransfer/utils/MyLogger;->logI(Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method public onServiceDisconnected(Landroid/content/ComponentName;)V
    .locals 2
    .param p1    # Landroid/content/ComponentName;

    iget-object v0, p0, Lcom/mediatek/datatransfer/AbstractRestoreActivity$9;->this$0:Lcom/mediatek/datatransfer/AbstractRestoreActivity;

    const/4 v1, 0x0

    iput-object v1, v0, Lcom/mediatek/datatransfer/AbstractRestoreActivity;->mRestoreService:Lcom/mediatek/datatransfer/RestoreService$RestoreBinder;

    const-string v0, "DataTransfer/AbstractRestoreActivity"

    const-string v1, " onServiceDisconnected"

    invoke-static {v0, v1}, Lcom/mediatek/datatransfer/utils/MyLogger;->logI(Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method
