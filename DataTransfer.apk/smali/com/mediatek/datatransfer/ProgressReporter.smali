.class public interface abstract Lcom/mediatek/datatransfer/ProgressReporter;
.super Ljava/lang/Object;
.source "ProgressReporter.java"


# virtual methods
.method public abstract onEnd(Lcom/mediatek/datatransfer/modules/Composer;Z)V
.end method

.method public abstract onErr(Ljava/io/IOException;)V
.end method

.method public abstract onOneFinished(Lcom/mediatek/datatransfer/modules/Composer;Z)V
.end method

.method public abstract onStart(Lcom/mediatek/datatransfer/modules/Composer;)V
.end method
