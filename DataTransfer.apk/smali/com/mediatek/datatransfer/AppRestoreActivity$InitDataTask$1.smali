.class Lcom/mediatek/datatransfer/AppRestoreActivity$InitDataTask$1;
.super Ljava/lang/Object;
.source "AppRestoreActivity.java"

# interfaces
.implements Ljava/util/Comparator;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/mediatek/datatransfer/AppRestoreActivity$InitDataTask;->doInBackground([Ljava/lang/Void;)Ljava/lang/Long;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ljava/util/Comparator",
        "<",
        "Lcom/mediatek/datatransfer/AppSnippet;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic this$1:Lcom/mediatek/datatransfer/AppRestoreActivity$InitDataTask;


# direct methods
.method constructor <init>(Lcom/mediatek/datatransfer/AppRestoreActivity$InitDataTask;)V
    .locals 0

    iput-object p1, p0, Lcom/mediatek/datatransfer/AppRestoreActivity$InitDataTask$1;->this$1:Lcom/mediatek/datatransfer/AppRestoreActivity$InitDataTask;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public compare(Lcom/mediatek/datatransfer/AppSnippet;Lcom/mediatek/datatransfer/AppSnippet;)I
    .locals 4
    .param p1    # Lcom/mediatek/datatransfer/AppSnippet;
    .param p2    # Lcom/mediatek/datatransfer/AppSnippet;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-virtual {p1}, Lcom/mediatek/datatransfer/AppSnippet;->getName()Ljava/lang/CharSequence;

    move-result-object v3

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/CharSequence;)V

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-virtual {p2}, Lcom/mediatek/datatransfer/AppSnippet;->getName()Ljava/lang/CharSequence;

    move-result-object v3

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/CharSequence;)V

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    if-eqz v0, :cond_0

    if-eqz v1, :cond_0

    invoke-virtual {v0, v1}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v2

    :goto_0
    return v2

    :cond_0
    const/4 v2, 0x0

    goto :goto_0
.end method

.method public bridge synthetic compare(Ljava/lang/Object;Ljava/lang/Object;)I
    .locals 1
    .param p1    # Ljava/lang/Object;
    .param p2    # Ljava/lang/Object;

    check-cast p1, Lcom/mediatek/datatransfer/AppSnippet;

    check-cast p2, Lcom/mediatek/datatransfer/AppSnippet;

    invoke-virtual {p0, p1, p2}, Lcom/mediatek/datatransfer/AppRestoreActivity$InitDataTask$1;->compare(Lcom/mediatek/datatransfer/AppSnippet;Lcom/mediatek/datatransfer/AppSnippet;)I

    move-result v0

    return v0
.end method
