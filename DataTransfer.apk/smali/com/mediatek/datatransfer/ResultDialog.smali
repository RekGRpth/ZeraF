.class public Lcom/mediatek/datatransfer/ResultDialog;
.super Ljava/lang/Object;
.source "ResultDialog.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/mediatek/datatransfer/ResultDialog$1;,
        Lcom/mediatek/datatransfer/ResultDialog$ResultDialogAdapter;,
        Lcom/mediatek/datatransfer/ResultDialog$ResultEntity;
    }
.end annotation


# static fields
.field public static final RESULT_TYPE_BACKUP:I = 0x1

.field public static final RESULT_TYPE_RESTRORE:I = 0x2


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static createAppResultAdapter(Ljava/util/List;Landroid/content/Context;Landroid/os/Bundle;I)Lcom/mediatek/datatransfer/ResultDialog$ResultDialogAdapter;
    .locals 16
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/os/Bundle;
    .param p3    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/mediatek/datatransfer/AppSnippet;",
            ">;",
            "Landroid/content/Context;",
            "Landroid/os/Bundle;",
            "I)",
            "Lcom/mediatek/datatransfer/ResultDialog$ResultDialogAdapter;"
        }
    .end annotation

    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    const-string v1, "result"

    move-object/from16 v0, p2

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getParcelableArrayList(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v12

    if-nez v12, :cond_0

    const-string v1, "B&R"

    const-string v2, "createAppResultAdapter: error: list is null"

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v1, 0x0

    :goto_0
    return-object v1

    :cond_0
    invoke-interface {v12}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v8

    :goto_1
    invoke-interface {v8}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_6

    invoke-interface {v8}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v10

    check-cast v10, Lcom/mediatek/datatransfer/ResultDialog$ResultEntity;

    new-instance v13, Ljava/util/HashMap;

    invoke-direct {v13}, Ljava/util/HashMap;-><init>()V

    const/4 v14, 0x0

    invoke-interface/range {p0 .. p0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v9

    :cond_1
    invoke-interface {v9}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_2

    invoke-interface {v9}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Lcom/mediatek/datatransfer/AppSnippet;

    const/4 v11, 0x0

    const/4 v1, 0x1

    move/from16 v0, p3

    if-ne v1, v0, :cond_5

    invoke-virtual {v7}, Lcom/mediatek/datatransfer/AppSnippet;->getPackageName()Ljava/lang/String;

    move-result-object v11

    :goto_2
    invoke-static {v10}, Lcom/mediatek/datatransfer/ResultDialog$ResultEntity;->access$200(Lcom/mediatek/datatransfer/ResultDialog$ResultEntity;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v11, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-virtual {v7}, Lcom/mediatek/datatransfer/AppSnippet;->getName()Ljava/lang/CharSequence;

    move-result-object v14

    :cond_2
    if-nez v14, :cond_3

    const v1, 0x7f060017

    move-object/from16 v0, p1

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v14

    :cond_3
    const-string v1, "name"

    invoke-interface {v13, v1, v14}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const v15, 0x7f060022

    invoke-static {v10}, Lcom/mediatek/datatransfer/ResultDialog$ResultEntity;->access$100(Lcom/mediatek/datatransfer/ResultDialog$ResultEntity;)I

    move-result v1

    const/4 v2, -0x1

    if-ne v1, v2, :cond_4

    const v15, 0x7f060023

    :cond_4
    const-string v1, "result"

    move-object/from16 v0, p1

    invoke-virtual {v0, v15}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-interface {v13, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    invoke-interface {v3, v13}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_1

    :cond_5
    invoke-virtual {v7}, Lcom/mediatek/datatransfer/AppSnippet;->getFileName()Ljava/lang/String;

    move-result-object v11

    goto :goto_2

    :cond_6
    const/4 v1, 0x2

    new-array v5, v1, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v2, "name"

    aput-object v2, v5, v1

    const/4 v1, 0x1

    const-string v2, "result"

    aput-object v2, v5, v1

    const/4 v1, 0x2

    new-array v6, v1, [I

    fill-array-data v6, :array_0

    new-instance v1, Lcom/mediatek/datatransfer/ResultDialog$ResultDialogAdapter;

    const v4, 0x7f030007

    move-object/from16 v2, p1

    invoke-direct/range {v1 .. v6}, Lcom/mediatek/datatransfer/ResultDialog$ResultDialogAdapter;-><init>(Landroid/content/Context;Ljava/util/List;I[Ljava/lang/String;[I)V

    goto/16 :goto_0

    :array_0
    .array-data 4
        0x7f080013
        0x7f080014
    .end array-data
.end method

.method private static createEmptyAdapter(Landroid/content/Context;)Landroid/widget/ArrayAdapter;
    .locals 3
    .param p0    # Landroid/content/Context;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            ")",
            "Landroid/widget/ArrayAdapter",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    const/4 v2, 0x0

    new-instance v0, Landroid/widget/ArrayAdapter;

    new-array v1, v2, [Ljava/lang/String;

    invoke-direct {v0, p0, v2, v1}, Landroid/widget/ArrayAdapter;-><init>(Landroid/content/Context;I[Ljava/lang/Object;)V

    return-object v0
.end method

.method public static createResultAdapter(Landroid/content/Context;Landroid/os/Bundle;)Lcom/mediatek/datatransfer/ResultDialog$ResultDialogAdapter;
    .locals 11
    .param p0    # Landroid/content/Context;
    .param p1    # Landroid/os/Bundle;

    const/4 v3, 0x2

    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    const-string v0, "result"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getParcelableArrayList(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v8

    if-nez v8, :cond_0

    const-string v0, "B&R"

    const-string v1, "ResultDialogAdapter: error: list is null"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_0
    invoke-interface {v8}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v6

    :goto_1
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Lcom/mediatek/datatransfer/ResultDialog$ResultEntity;

    new-instance v9, Ljava/util/HashMap;

    invoke-direct {v9}, Ljava/util/HashMap;-><init>()V

    const-string v0, "name"

    invoke-static {v7}, Lcom/mediatek/datatransfer/ResultDialog$ResultEntity;->access$000(Lcom/mediatek/datatransfer/ResultDialog$ResultEntity;)I

    move-result v1

    invoke-static {p0, v1}, Lcom/mediatek/datatransfer/utils/ModuleType;->getModuleStringFromType(Landroid/content/Context;I)Ljava/lang/String;

    move-result-object v1

    invoke-interface {v9, v0, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const v10, 0x7f060022

    invoke-static {v7}, Lcom/mediatek/datatransfer/ResultDialog$ResultEntity;->access$100(Lcom/mediatek/datatransfer/ResultDialog$ResultEntity;)I

    move-result v0

    packed-switch v0, :pswitch_data_0

    const v10, 0x7f060022

    :goto_2
    const-string v0, "result"

    invoke-virtual {p0, v10}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-interface {v9, v0, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    invoke-interface {v2, v9}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_1

    :pswitch_0
    const v10, 0x7f060024

    goto :goto_2

    :pswitch_1
    const v10, 0x7f060023

    goto :goto_2

    :cond_1
    new-array v4, v3, [Ljava/lang/String;

    const/4 v0, 0x0

    const-string v1, "name"

    aput-object v1, v4, v0

    const/4 v0, 0x1

    const-string v1, "result"

    aput-object v1, v4, v0

    new-array v5, v3, [I

    fill-array-data v5, :array_0

    new-instance v0, Lcom/mediatek/datatransfer/ResultDialog$ResultDialogAdapter;

    const v3, 0x7f030007

    move-object v1, p0

    invoke-direct/range {v0 .. v5}, Lcom/mediatek/datatransfer/ResultDialog$ResultDialogAdapter;-><init>(Landroid/content/Context;Ljava/util/List;I[Ljava/lang/String;[I)V

    goto :goto_0

    :pswitch_data_0
    .packed-switch -0x2
        :pswitch_0
        :pswitch_1
    .end packed-switch

    :array_0
    .array-data 4
        0x7f080013
        0x7f080014
    .end array-data
.end method

.method public static createResultDlg(Landroid/content/Context;ILandroid/os/Bundle;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog;
    .locals 4
    .param p0    # Landroid/content/Context;
    .param p1    # I
    .param p2    # Landroid/os/Bundle;
    .param p3    # Landroid/content/DialogInterface$OnClickListener;

    new-instance v1, Landroid/app/AlertDialog$Builder;

    invoke-direct {v1, p0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Landroid/app/AlertDialog$Builder;->setCancelable(Z)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    invoke-virtual {v1, p1}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    const v2, 0x104000a

    invoke-virtual {v1, v2, p3}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    invoke-static {p0}, Lcom/mediatek/datatransfer/ResultDialog;->createEmptyAdapter(Landroid/content/Context;)Landroid/widget/ArrayAdapter;

    move-result-object v2

    const/4 v3, 0x0

    invoke-virtual {v1, v2, v3}, Landroid/app/AlertDialog$Builder;->setAdapter(Landroid/widget/ListAdapter;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    invoke-virtual {v1}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v0

    return-object v0
.end method
