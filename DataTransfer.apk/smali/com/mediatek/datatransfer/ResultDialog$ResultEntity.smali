.class public Lcom/mediatek/datatransfer/ResultDialog$ResultEntity;
.super Ljava/lang/Object;
.source "ResultDialog.java"

# interfaces
.implements Landroid/os/Parcelable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/mediatek/datatransfer/ResultDialog;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "ResultEntity"
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/mediatek/datatransfer/ResultDialog$ResultEntity;",
            ">;"
        }
    .end annotation
.end field

.field public static final ENTITY_APP_ITEM:I = 0x0

.field public static final ENTITY_PERSONAL_DATA_ITEM:I = 0x0

.field public static final FAIL:I = -0x1

.field public static final NO_CONTENT:I = -0x2

.field public static final SUCCESS:I


# instance fields
.field private mKey:Ljava/lang/String;

.field private mResult:I

.field private mType:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Lcom/mediatek/datatransfer/ResultDialog$ResultEntity$1;

    invoke-direct {v0}, Lcom/mediatek/datatransfer/ResultDialog$ResultEntity$1;-><init>()V

    sput-object v0, Lcom/mediatek/datatransfer/ResultDialog$ResultEntity;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(II)V
    .locals 0
    .param p1    # I
    .param p2    # I

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput p1, p0, Lcom/mediatek/datatransfer/ResultDialog$ResultEntity;->mType:I

    iput p2, p0, Lcom/mediatek/datatransfer/ResultDialog$ResultEntity;->mResult:I

    return-void
.end method

.method private constructor <init>(Landroid/os/Parcel;)V
    .locals 1
    .param p1    # Landroid/os/Parcel;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/mediatek/datatransfer/ResultDialog$ResultEntity;->mType:I

    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/mediatek/datatransfer/ResultDialog$ResultEntity;->mResult:I

    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/mediatek/datatransfer/ResultDialog$ResultEntity;->mKey:Ljava/lang/String;

    return-void
.end method

.method synthetic constructor <init>(Landroid/os/Parcel;Lcom/mediatek/datatransfer/ResultDialog$1;)V
    .locals 0
    .param p1    # Landroid/os/Parcel;
    .param p2    # Lcom/mediatek/datatransfer/ResultDialog$1;

    invoke-direct {p0, p1}, Lcom/mediatek/datatransfer/ResultDialog$ResultEntity;-><init>(Landroid/os/Parcel;)V

    return-void
.end method

.method static synthetic access$000(Lcom/mediatek/datatransfer/ResultDialog$ResultEntity;)I
    .locals 1
    .param p0    # Lcom/mediatek/datatransfer/ResultDialog$ResultEntity;

    iget v0, p0, Lcom/mediatek/datatransfer/ResultDialog$ResultEntity;->mType:I

    return v0
.end method

.method static synthetic access$100(Lcom/mediatek/datatransfer/ResultDialog$ResultEntity;)I
    .locals 1
    .param p0    # Lcom/mediatek/datatransfer/ResultDialog$ResultEntity;

    iget v0, p0, Lcom/mediatek/datatransfer/ResultDialog$ResultEntity;->mResult:I

    return v0
.end method

.method static synthetic access$200(Lcom/mediatek/datatransfer/ResultDialog$ResultEntity;)Ljava/lang/String;
    .locals 1
    .param p0    # Lcom/mediatek/datatransfer/ResultDialog$ResultEntity;

    iget-object v0, p0, Lcom/mediatek/datatransfer/ResultDialog$ResultEntity;->mKey:Ljava/lang/String;

    return-object v0
.end method


# virtual methods
.method public describeContents()I
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public getResult()I
    .locals 1

    iget v0, p0, Lcom/mediatek/datatransfer/ResultDialog$ResultEntity;->mResult:I

    return v0
.end method

.method public setKey(Ljava/lang/String;)V
    .locals 0
    .param p1    # Ljava/lang/String;

    iput-object p1, p0, Lcom/mediatek/datatransfer/ResultDialog$ResultEntity;->mKey:Ljava/lang/String;

    return-void
.end method

.method public setResult(I)V
    .locals 0
    .param p1    # I

    iput p1, p0, Lcom/mediatek/datatransfer/ResultDialog$ResultEntity;->mResult:I

    return-void
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 1
    .param p1    # Landroid/os/Parcel;
    .param p2    # I

    iget v0, p0, Lcom/mediatek/datatransfer/ResultDialog$ResultEntity;->mType:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    iget v0, p0, Lcom/mediatek/datatransfer/ResultDialog$ResultEntity;->mResult:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    iget-object v0, p0, Lcom/mediatek/datatransfer/ResultDialog$ResultEntity;->mKey:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    return-void
.end method
