.class Lcom/mediatek/datatransfer/RestoreTabFragment$DeleteCheckItemTask;
.super Landroid/os/AsyncTask;
.source "RestoreTabFragment.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/mediatek/datatransfer/RestoreTabFragment;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "DeleteCheckItemTask"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/os/AsyncTask",
        "<",
        "Ljava/util/HashSet",
        "<",
        "Ljava/io/File;",
        ">;",
        "Ljava/lang/String;",
        "Ljava/lang/Long;",
        ">;"
    }
.end annotation


# instance fields
.field private mDeletingDialog:Landroid/app/ProgressDialog;

.field final synthetic this$0:Lcom/mediatek/datatransfer/RestoreTabFragment;


# direct methods
.method public constructor <init>(Lcom/mediatek/datatransfer/RestoreTabFragment;)V
    .locals 2

    iput-object p1, p0, Lcom/mediatek/datatransfer/RestoreTabFragment$DeleteCheckItemTask;->this$0:Lcom/mediatek/datatransfer/RestoreTabFragment;

    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    new-instance v0, Landroid/app/ProgressDialog;

    invoke-virtual {p1}, Landroid/app/Fragment;->getActivity()Landroid/app/Activity;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/app/ProgressDialog;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/mediatek/datatransfer/RestoreTabFragment$DeleteCheckItemTask;->mDeletingDialog:Landroid/app/ProgressDialog;

    iget-object v0, p0, Lcom/mediatek/datatransfer/RestoreTabFragment$DeleteCheckItemTask;->mDeletingDialog:Landroid/app/ProgressDialog;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/app/Dialog;->setCancelable(Z)V

    iget-object v0, p0, Lcom/mediatek/datatransfer/RestoreTabFragment$DeleteCheckItemTask;->mDeletingDialog:Landroid/app/ProgressDialog;

    const v1, 0x7f060030

    invoke-virtual {p1, v1}, Landroid/app/Fragment;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/app/ProgressDialog;->setMessage(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lcom/mediatek/datatransfer/RestoreTabFragment$DeleteCheckItemTask;->mDeletingDialog:Landroid/app/ProgressDialog;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/app/ProgressDialog;->setIndeterminate(Z)V

    return-void
.end method


# virtual methods
.method protected varargs doInBackground([Ljava/util/HashSet;)Ljava/lang/Long;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "([",
            "Ljava/util/HashSet",
            "<",
            "Ljava/io/File;",
            ">;)",
            "Ljava/lang/Long;"
        }
    .end annotation

    const/4 v3, 0x0

    aget-object v0, p1, v3

    invoke-virtual {v0}, Ljava/util/HashSet;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/io/File;

    invoke-static {v1}, Lcom/mediatek/datatransfer/utils/FileUtils;->deleteFileOrFolder(Ljava/io/File;)Z

    goto :goto_0

    :cond_0
    const/4 v3, 0x0

    return-object v3
.end method

.method protected bridge synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1
    .param p1    # [Ljava/lang/Object;

    check-cast p1, [Ljava/util/HashSet;

    invoke-virtual {p0, p1}, Lcom/mediatek/datatransfer/RestoreTabFragment$DeleteCheckItemTask;->doInBackground([Ljava/util/HashSet;)Ljava/lang/Long;

    move-result-object v0

    return-object v0
.end method

.method protected onPostExecute(Ljava/lang/Long;)V
    .locals 2
    .param p1    # Ljava/lang/Long;

    invoke-super {p0, p1}, Landroid/os/AsyncTask;->onPostExecute(Ljava/lang/Object;)V

    iget-object v1, p0, Lcom/mediatek/datatransfer/RestoreTabFragment$DeleteCheckItemTask;->this$0:Lcom/mediatek/datatransfer/RestoreTabFragment;

    invoke-static {v1}, Lcom/mediatek/datatransfer/RestoreTabFragment;->access$300(Lcom/mediatek/datatransfer/RestoreTabFragment;)V

    iget-object v1, p0, Lcom/mediatek/datatransfer/RestoreTabFragment$DeleteCheckItemTask;->this$0:Lcom/mediatek/datatransfer/RestoreTabFragment;

    invoke-virtual {v1}, Landroid/app/Fragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v1, p0, Lcom/mediatek/datatransfer/RestoreTabFragment$DeleteCheckItemTask;->mDeletingDialog:Landroid/app/ProgressDialog;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/mediatek/datatransfer/RestoreTabFragment$DeleteCheckItemTask;->mDeletingDialog:Landroid/app/ProgressDialog;

    invoke-virtual {v1}, Landroid/app/Dialog;->dismiss()V

    :cond_0
    return-void
.end method

.method protected bridge synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 0
    .param p1    # Ljava/lang/Object;

    check-cast p1, Ljava/lang/Long;

    invoke-virtual {p0, p1}, Lcom/mediatek/datatransfer/RestoreTabFragment$DeleteCheckItemTask;->onPostExecute(Ljava/lang/Long;)V

    return-void
.end method

.method protected onPreExecute()V
    .locals 2

    iget-object v1, p0, Lcom/mediatek/datatransfer/RestoreTabFragment$DeleteCheckItemTask;->this$0:Lcom/mediatek/datatransfer/RestoreTabFragment;

    invoke-virtual {v1}, Landroid/app/Fragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v1, p0, Lcom/mediatek/datatransfer/RestoreTabFragment$DeleteCheckItemTask;->mDeletingDialog:Landroid/app/ProgressDialog;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/mediatek/datatransfer/RestoreTabFragment$DeleteCheckItemTask;->mDeletingDialog:Landroid/app/ProgressDialog;

    invoke-virtual {v1}, Landroid/app/Dialog;->show()V

    :cond_0
    return-void
.end method
