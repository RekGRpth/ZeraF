.class Lcom/mediatek/datatransfer/AppRestoreActivity$InitDataTask;
.super Landroid/os/AsyncTask;
.source "AppRestoreActivity.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/mediatek/datatransfer/AppRestoreActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "InitDataTask"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/os/AsyncTask",
        "<",
        "Ljava/lang/Void;",
        "Ljava/lang/Void;",
        "Ljava/lang/Long;",
        ">;"
    }
.end annotation


# instance fields
.field appDatas:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/mediatek/datatransfer/AppSnippet;",
            ">;"
        }
    .end annotation
.end field

.field final synthetic this$0:Lcom/mediatek/datatransfer/AppRestoreActivity;


# direct methods
.method private constructor <init>(Lcom/mediatek/datatransfer/AppRestoreActivity;)V
    .locals 0

    iput-object p1, p0, Lcom/mediatek/datatransfer/AppRestoreActivity$InitDataTask;->this$0:Lcom/mediatek/datatransfer/AppRestoreActivity;

    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/mediatek/datatransfer/AppRestoreActivity;Lcom/mediatek/datatransfer/AppRestoreActivity$1;)V
    .locals 0
    .param p1    # Lcom/mediatek/datatransfer/AppRestoreActivity;
    .param p2    # Lcom/mediatek/datatransfer/AppRestoreActivity$1;

    invoke-direct {p0, p1}, Lcom/mediatek/datatransfer/AppRestoreActivity$InitDataTask;-><init>(Lcom/mediatek/datatransfer/AppRestoreActivity;)V

    return-void
.end method


# virtual methods
.method protected varargs doInBackground([Ljava/lang/Void;)Ljava/lang/Long;
    .locals 6
    .param p1    # [Ljava/lang/Void;

    iget-object v4, p0, Lcom/mediatek/datatransfer/AppRestoreActivity$InitDataTask;->this$0:Lcom/mediatek/datatransfer/AppRestoreActivity;

    invoke-static {v4}, Lcom/mediatek/datatransfer/AppRestoreActivity;->access$700(Lcom/mediatek/datatransfer/AppRestoreActivity;)Ljava/io/File;

    move-result-object v4

    invoke-static {v4}, Lcom/mediatek/datatransfer/utils/FileUtils;->getAllApkFileInFolder(Ljava/io/File;)Ljava/util/ArrayList;

    move-result-object v0

    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    iput-object v4, p0, Lcom/mediatek/datatransfer/AppRestoreActivity$InitDataTask;->appDatas:Ljava/util/ArrayList;

    if-eqz v0, :cond_1

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_0
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/io/File;

    iget-object v4, p0, Lcom/mediatek/datatransfer/AppRestoreActivity$InitDataTask;->this$0:Lcom/mediatek/datatransfer/AppRestoreActivity;

    invoke-virtual {v1}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/mediatek/datatransfer/utils/FileUtils;->getAppSnippet(Landroid/content/Context;Ljava/lang/String;)Lcom/mediatek/datatransfer/AppSnippet;

    move-result-object v3

    if-eqz v3, :cond_0

    iget-object v4, p0, Lcom/mediatek/datatransfer/AppRestoreActivity$InitDataTask;->appDatas:Ljava/util/ArrayList;

    invoke-virtual {v4, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_1
    iget-object v4, p0, Lcom/mediatek/datatransfer/AppRestoreActivity$InitDataTask;->appDatas:Ljava/util/ArrayList;

    new-instance v5, Lcom/mediatek/datatransfer/AppRestoreActivity$InitDataTask$1;

    invoke-direct {v5, p0}, Lcom/mediatek/datatransfer/AppRestoreActivity$InitDataTask$1;-><init>(Lcom/mediatek/datatransfer/AppRestoreActivity$InitDataTask;)V

    invoke-static {v4, v5}, Ljava/util/Collections;->sort(Ljava/util/List;Ljava/util/Comparator;)V

    const/4 v4, 0x0

    return-object v4
.end method

.method protected bridge synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1
    .param p1    # [Ljava/lang/Object;

    check-cast p1, [Ljava/lang/Void;

    invoke-virtual {p0, p1}, Lcom/mediatek/datatransfer/AppRestoreActivity$InitDataTask;->doInBackground([Ljava/lang/Void;)Ljava/lang/Long;

    move-result-object v0

    return-object v0
.end method

.method protected onPostExecute(Ljava/lang/Long;)V
    .locals 4
    .param p1    # Ljava/lang/Long;

    const/4 v2, 0x1

    invoke-super {p0, p1}, Landroid/os/AsyncTask;->onPostExecute(Ljava/lang/Object;)V

    iget-object v0, p0, Lcom/mediatek/datatransfer/AppRestoreActivity$InitDataTask;->this$0:Lcom/mediatek/datatransfer/AppRestoreActivity;

    iget-object v1, p0, Lcom/mediatek/datatransfer/AppRestoreActivity$InitDataTask;->appDatas:Ljava/util/ArrayList;

    invoke-static {v0, v1}, Lcom/mediatek/datatransfer/AppRestoreActivity;->access$102(Lcom/mediatek/datatransfer/AppRestoreActivity;Ljava/util/List;)Ljava/util/List;

    iget-object v0, p0, Lcom/mediatek/datatransfer/AppRestoreActivity$InitDataTask;->this$0:Lcom/mediatek/datatransfer/AppRestoreActivity;

    invoke-static {v0}, Lcom/mediatek/datatransfer/AppRestoreActivity;->access$200(Lcom/mediatek/datatransfer/AppRestoreActivity;)Lcom/mediatek/datatransfer/AppRestoreActivity$AppRestoreAdapter;

    move-result-object v0

    iget-object v1, p0, Lcom/mediatek/datatransfer/AppRestoreActivity$InitDataTask;->appDatas:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Lcom/mediatek/datatransfer/AppRestoreActivity$AppRestoreAdapter;->changeData(Ljava/util/List;)V

    iget-object v0, p0, Lcom/mediatek/datatransfer/AppRestoreActivity$InitDataTask;->this$0:Lcom/mediatek/datatransfer/AppRestoreActivity;

    invoke-virtual {v0}, Lcom/mediatek/datatransfer/CheckedListActivity;->syncUnCheckedItems()V

    iget-object v0, p0, Lcom/mediatek/datatransfer/AppRestoreActivity$InitDataTask;->this$0:Lcom/mediatek/datatransfer/AppRestoreActivity;

    invoke-virtual {v0, v2}, Lcom/mediatek/datatransfer/AbstractRestoreActivity;->setButtonsEnable(Z)V

    iget-object v0, p0, Lcom/mediatek/datatransfer/AppRestoreActivity$InitDataTask;->this$0:Lcom/mediatek/datatransfer/AppRestoreActivity;

    invoke-virtual {v0}, Lcom/mediatek/datatransfer/AppRestoreActivity;->notifyListItemCheckedChanged()V

    iget-object v0, p0, Lcom/mediatek/datatransfer/AppRestoreActivity$InitDataTask;->this$0:Lcom/mediatek/datatransfer/AppRestoreActivity;

    invoke-static {v0, v2}, Lcom/mediatek/datatransfer/AppRestoreActivity;->access$302(Lcom/mediatek/datatransfer/AppRestoreActivity;Z)Z

    iget-object v0, p0, Lcom/mediatek/datatransfer/AppRestoreActivity$InitDataTask;->this$0:Lcom/mediatek/datatransfer/AppRestoreActivity;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/mediatek/datatransfer/AbstractRestoreActivity;->showLoadingContent(Z)V

    iget-object v0, p0, Lcom/mediatek/datatransfer/AppRestoreActivity$InitDataTask;->this$0:Lcom/mediatek/datatransfer/AppRestoreActivity;

    invoke-static {v0}, Lcom/mediatek/datatransfer/AppRestoreActivity;->access$400(Lcom/mediatek/datatransfer/AppRestoreActivity;)Lcom/mediatek/datatransfer/AppRestoreActivity$AppRestoreStatusListener;

    move-result-object v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/mediatek/datatransfer/AppRestoreActivity$InitDataTask;->this$0:Lcom/mediatek/datatransfer/AppRestoreActivity;

    new-instance v1, Lcom/mediatek/datatransfer/AppRestoreActivity$AppRestoreStatusListener;

    iget-object v2, p0, Lcom/mediatek/datatransfer/AppRestoreActivity$InitDataTask;->this$0:Lcom/mediatek/datatransfer/AppRestoreActivity;

    const/4 v3, 0x0

    invoke-direct {v1, v2, v3}, Lcom/mediatek/datatransfer/AppRestoreActivity$AppRestoreStatusListener;-><init>(Lcom/mediatek/datatransfer/AppRestoreActivity;Lcom/mediatek/datatransfer/AppRestoreActivity$1;)V

    invoke-static {v0, v1}, Lcom/mediatek/datatransfer/AppRestoreActivity;->access$402(Lcom/mediatek/datatransfer/AppRestoreActivity;Lcom/mediatek/datatransfer/AppRestoreActivity$AppRestoreStatusListener;)Lcom/mediatek/datatransfer/AppRestoreActivity$AppRestoreStatusListener;

    :cond_0
    iget-object v0, p0, Lcom/mediatek/datatransfer/AppRestoreActivity$InitDataTask;->this$0:Lcom/mediatek/datatransfer/AppRestoreActivity;

    iget-object v1, p0, Lcom/mediatek/datatransfer/AppRestoreActivity$InitDataTask;->this$0:Lcom/mediatek/datatransfer/AppRestoreActivity;

    invoke-static {v1}, Lcom/mediatek/datatransfer/AppRestoreActivity;->access$400(Lcom/mediatek/datatransfer/AppRestoreActivity;)Lcom/mediatek/datatransfer/AppRestoreActivity$AppRestoreStatusListener;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/mediatek/datatransfer/AbstractRestoreActivity;->setOnRestoreStatusListener(Lcom/mediatek/datatransfer/RestoreService$OnRestoreStatusListener;)V

    iget-object v0, p0, Lcom/mediatek/datatransfer/AppRestoreActivity$InitDataTask;->this$0:Lcom/mediatek/datatransfer/AppRestoreActivity;

    invoke-static {v0}, Lcom/mediatek/datatransfer/AppRestoreActivity;->access$600(Lcom/mediatek/datatransfer/AppRestoreActivity;)V

    return-void
.end method

.method protected bridge synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 0
    .param p1    # Ljava/lang/Object;

    check-cast p1, Ljava/lang/Long;

    invoke-virtual {p0, p1}, Lcom/mediatek/datatransfer/AppRestoreActivity$InitDataTask;->onPostExecute(Ljava/lang/Long;)V

    return-void
.end method

.method protected onPreExecute()V
    .locals 2

    invoke-super {p0}, Landroid/os/AsyncTask;->onPreExecute()V

    iget-object v0, p0, Lcom/mediatek/datatransfer/AppRestoreActivity$InitDataTask;->this$0:Lcom/mediatek/datatransfer/AppRestoreActivity;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/mediatek/datatransfer/AbstractRestoreActivity;->setButtonsEnable(Z)V

    iget-object v0, p0, Lcom/mediatek/datatransfer/AppRestoreActivity$InitDataTask;->this$0:Lcom/mediatek/datatransfer/AppRestoreActivity;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/mediatek/datatransfer/AbstractRestoreActivity;->showLoadingContent(Z)V

    iget-object v0, p0, Lcom/mediatek/datatransfer/AppRestoreActivity$InitDataTask;->this$0:Lcom/mediatek/datatransfer/AppRestoreActivity;

    const v1, 0x7f060029

    invoke-virtual {v0, v1}, Landroid/app/Activity;->setTitle(I)V

    return-void
.end method
