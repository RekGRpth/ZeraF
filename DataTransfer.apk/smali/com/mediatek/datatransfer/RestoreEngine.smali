.class public Lcom/mediatek/datatransfer/RestoreEngine;
.super Ljava/lang/Object;
.source "RestoreEngine.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/mediatek/datatransfer/RestoreEngine$RestoreThread;,
        Lcom/mediatek/datatransfer/RestoreEngine$OnRestoreDoneListner;
    }
.end annotation


# static fields
.field private static final CLASS_TAG:Ljava/lang/String; = "DataTransfer/RestoreEngine"

.field private static sSelfInstance:Lcom/mediatek/datatransfer/RestoreEngine;


# instance fields
.field private mComposerList:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/mediatek/datatransfer/modules/Composer;",
            ">;"
        }
    .end annotation
.end field

.field private mContext:Landroid/content/Context;

.field private mIsPause:Z

.field private mIsRunning:Z

.field private mLock:Ljava/lang/Object;

.field mModuleList:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field mParasMap:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/Integer;",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;>;"
        }
    .end annotation
.end field

.field private mReporter:Lcom/mediatek/datatransfer/ProgressReporter;

.field private mRestoreDoneListner:Lcom/mediatek/datatransfer/RestoreEngine$OnRestoreDoneListner;

.field private mRestoreFolder:Ljava/lang/String;


# direct methods
.method private constructor <init>(Landroid/content/Context;Lcom/mediatek/datatransfer/ProgressReporter;)V
    .locals 1
    .param p1    # Landroid/content/Context;
    .param p2    # Lcom/mediatek/datatransfer/ProgressReporter;

    const/4 v0, 0x0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-boolean v0, p0, Lcom/mediatek/datatransfer/RestoreEngine;->mIsRunning:Z

    iput-boolean v0, p0, Lcom/mediatek/datatransfer/RestoreEngine;->mIsPause:Z

    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lcom/mediatek/datatransfer/RestoreEngine;->mLock:Ljava/lang/Object;

    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/mediatek/datatransfer/RestoreEngine;->mParasMap:Ljava/util/HashMap;

    iput-object p1, p0, Lcom/mediatek/datatransfer/RestoreEngine;->mContext:Landroid/content/Context;

    iput-object p2, p0, Lcom/mediatek/datatransfer/RestoreEngine;->mReporter:Lcom/mediatek/datatransfer/ProgressReporter;

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/mediatek/datatransfer/RestoreEngine;->mComposerList:Ljava/util/List;

    return-void
.end method

.method static synthetic access$000(Lcom/mediatek/datatransfer/RestoreEngine;)Ljava/util/List;
    .locals 1
    .param p0    # Lcom/mediatek/datatransfer/RestoreEngine;

    iget-object v0, p0, Lcom/mediatek/datatransfer/RestoreEngine;->mComposerList:Ljava/util/List;

    return-object v0
.end method

.method static synthetic access$100(Lcom/mediatek/datatransfer/RestoreEngine;)Z
    .locals 1
    .param p0    # Lcom/mediatek/datatransfer/RestoreEngine;

    iget-boolean v0, p0, Lcom/mediatek/datatransfer/RestoreEngine;->mIsPause:Z

    return v0
.end method

.method static synthetic access$200(Lcom/mediatek/datatransfer/RestoreEngine;)Ljava/lang/Object;
    .locals 1
    .param p0    # Lcom/mediatek/datatransfer/RestoreEngine;

    iget-object v0, p0, Lcom/mediatek/datatransfer/RestoreEngine;->mLock:Ljava/lang/Object;

    return-object v0
.end method

.method static synthetic access$302(Lcom/mediatek/datatransfer/RestoreEngine;Z)Z
    .locals 0
    .param p0    # Lcom/mediatek/datatransfer/RestoreEngine;
    .param p1    # Z

    iput-boolean p1, p0, Lcom/mediatek/datatransfer/RestoreEngine;->mIsRunning:Z

    return p1
.end method

.method static synthetic access$400(Lcom/mediatek/datatransfer/RestoreEngine;)Lcom/mediatek/datatransfer/RestoreEngine$OnRestoreDoneListner;
    .locals 1
    .param p0    # Lcom/mediatek/datatransfer/RestoreEngine;

    iget-object v0, p0, Lcom/mediatek/datatransfer/RestoreEngine;->mRestoreDoneListner:Lcom/mediatek/datatransfer/RestoreEngine$OnRestoreDoneListner;

    return-object v0
.end method

.method private addComposer(Lcom/mediatek/datatransfer/modules/Composer;)V
    .locals 4
    .param p1    # Lcom/mediatek/datatransfer/modules/Composer;

    if-eqz p1, :cond_1

    invoke-virtual {p1}, Lcom/mediatek/datatransfer/modules/Composer;->getModuleType()I

    move-result v1

    iget-object v2, p0, Lcom/mediatek/datatransfer/RestoreEngine;->mParasMap:Ljava/util/HashMap;

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/ArrayList;

    if-eqz v0, :cond_0

    invoke-virtual {p1, v0}, Lcom/mediatek/datatransfer/modules/Composer;->setParams(Ljava/util/List;)V

    :cond_0
    iget-object v2, p0, Lcom/mediatek/datatransfer/RestoreEngine;->mReporter:Lcom/mediatek/datatransfer/ProgressReporter;

    invoke-virtual {p1, v2}, Lcom/mediatek/datatransfer/modules/Composer;->setReporter(Lcom/mediatek/datatransfer/ProgressReporter;)V

    iget-object v2, p0, Lcom/mediatek/datatransfer/RestoreEngine;->mRestoreFolder:Ljava/lang/String;

    invoke-virtual {p1, v2}, Lcom/mediatek/datatransfer/modules/Composer;->setParentFolderPath(Ljava/lang/String;)V

    iget-object v2, p0, Lcom/mediatek/datatransfer/RestoreEngine;->mComposerList:Ljava/util/List;

    invoke-interface {v2, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :cond_1
    return-void
.end method

.method public static declared-synchronized getInstance(Landroid/content/Context;Lcom/mediatek/datatransfer/ProgressReporter;)Lcom/mediatek/datatransfer/RestoreEngine;
    .locals 2
    .param p0    # Landroid/content/Context;
    .param p1    # Lcom/mediatek/datatransfer/ProgressReporter;

    const-class v1, Lcom/mediatek/datatransfer/RestoreEngine;

    monitor-enter v1

    :try_start_0
    sget-object v0, Lcom/mediatek/datatransfer/RestoreEngine;->sSelfInstance:Lcom/mediatek/datatransfer/RestoreEngine;

    if-nez v0, :cond_0

    new-instance v0, Lcom/mediatek/datatransfer/RestoreEngine;

    invoke-direct {v0, p0, p1}, Lcom/mediatek/datatransfer/RestoreEngine;-><init>(Landroid/content/Context;Lcom/mediatek/datatransfer/ProgressReporter;)V

    sput-object v0, Lcom/mediatek/datatransfer/RestoreEngine;->sSelfInstance:Lcom/mediatek/datatransfer/RestoreEngine;

    :goto_0
    sget-object v0, Lcom/mediatek/datatransfer/RestoreEngine;->sSelfInstance:Lcom/mediatek/datatransfer/RestoreEngine;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit v1

    return-object v0

    :cond_0
    :try_start_1
    sget-object v0, Lcom/mediatek/datatransfer/RestoreEngine;->sSelfInstance:Lcom/mediatek/datatransfer/RestoreEngine;

    invoke-direct {v0, p0, p1}, Lcom/mediatek/datatransfer/RestoreEngine;->updateInfo(Landroid/content/Context;Lcom/mediatek/datatransfer/ProgressReporter;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method private reset()V
    .locals 1

    iget-object v0, p0, Lcom/mediatek/datatransfer/RestoreEngine;->mComposerList:Ljava/util/List;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/mediatek/datatransfer/RestoreEngine;->mComposerList:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    :cond_0
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/mediatek/datatransfer/RestoreEngine;->mIsPause:Z

    return-void
.end method

.method private setupComposer(Ljava/util/List;)Z
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Ljava/lang/Integer;",
            ">;)Z"
        }
    .end annotation

    const/4 v0, 0x1

    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Integer;

    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    move-result v2

    sparse-switch v2, :sswitch_data_0

    const/4 v0, 0x0

    goto :goto_0

    :sswitch_0
    new-instance v3, Lcom/mediatek/datatransfer/modules/ContactRestoreComposer;

    iget-object v4, p0, Lcom/mediatek/datatransfer/RestoreEngine;->mContext:Landroid/content/Context;

    invoke-direct {v3, v4}, Lcom/mediatek/datatransfer/modules/ContactRestoreComposer;-><init>(Landroid/content/Context;)V

    invoke-direct {p0, v3}, Lcom/mediatek/datatransfer/RestoreEngine;->addComposer(Lcom/mediatek/datatransfer/modules/Composer;)V

    goto :goto_0

    :sswitch_1
    new-instance v3, Lcom/mediatek/datatransfer/modules/MessageRestoreComposer;

    iget-object v4, p0, Lcom/mediatek/datatransfer/RestoreEngine;->mContext:Landroid/content/Context;

    invoke-direct {v3, v4}, Lcom/mediatek/datatransfer/modules/MessageRestoreComposer;-><init>(Landroid/content/Context;)V

    invoke-direct {p0, v3}, Lcom/mediatek/datatransfer/RestoreEngine;->addComposer(Lcom/mediatek/datatransfer/modules/Composer;)V

    goto :goto_0

    :sswitch_2
    new-instance v3, Lcom/mediatek/datatransfer/modules/SmsRestoreComposer;

    iget-object v4, p0, Lcom/mediatek/datatransfer/RestoreEngine;->mContext:Landroid/content/Context;

    invoke-direct {v3, v4}, Lcom/mediatek/datatransfer/modules/SmsRestoreComposer;-><init>(Landroid/content/Context;)V

    invoke-direct {p0, v3}, Lcom/mediatek/datatransfer/RestoreEngine;->addComposer(Lcom/mediatek/datatransfer/modules/Composer;)V

    goto :goto_0

    :sswitch_3
    new-instance v3, Lcom/mediatek/datatransfer/modules/MmsRestoreComposer;

    iget-object v4, p0, Lcom/mediatek/datatransfer/RestoreEngine;->mContext:Landroid/content/Context;

    invoke-direct {v3, v4}, Lcom/mediatek/datatransfer/modules/MmsRestoreComposer;-><init>(Landroid/content/Context;)V

    invoke-direct {p0, v3}, Lcom/mediatek/datatransfer/RestoreEngine;->addComposer(Lcom/mediatek/datatransfer/modules/Composer;)V

    goto :goto_0

    :sswitch_4
    new-instance v3, Lcom/mediatek/datatransfer/modules/PictureRestoreComposer;

    iget-object v4, p0, Lcom/mediatek/datatransfer/RestoreEngine;->mContext:Landroid/content/Context;

    invoke-direct {v3, v4}, Lcom/mediatek/datatransfer/modules/PictureRestoreComposer;-><init>(Landroid/content/Context;)V

    invoke-direct {p0, v3}, Lcom/mediatek/datatransfer/RestoreEngine;->addComposer(Lcom/mediatek/datatransfer/modules/Composer;)V

    goto :goto_0

    :sswitch_5
    new-instance v3, Lcom/mediatek/datatransfer/modules/CalendarRestoreComposer;

    iget-object v4, p0, Lcom/mediatek/datatransfer/RestoreEngine;->mContext:Landroid/content/Context;

    invoke-direct {v3, v4}, Lcom/mediatek/datatransfer/modules/CalendarRestoreComposer;-><init>(Landroid/content/Context;)V

    invoke-direct {p0, v3}, Lcom/mediatek/datatransfer/RestoreEngine;->addComposer(Lcom/mediatek/datatransfer/modules/Composer;)V

    goto :goto_0

    :sswitch_6
    new-instance v3, Lcom/mediatek/datatransfer/modules/AppRestoreComposer;

    iget-object v4, p0, Lcom/mediatek/datatransfer/RestoreEngine;->mContext:Landroid/content/Context;

    invoke-direct {v3, v4}, Lcom/mediatek/datatransfer/modules/AppRestoreComposer;-><init>(Landroid/content/Context;)V

    invoke-direct {p0, v3}, Lcom/mediatek/datatransfer/RestoreEngine;->addComposer(Lcom/mediatek/datatransfer/modules/Composer;)V

    goto :goto_0

    :sswitch_7
    new-instance v3, Lcom/mediatek/datatransfer/modules/MusicRestoreComposer;

    iget-object v4, p0, Lcom/mediatek/datatransfer/RestoreEngine;->mContext:Landroid/content/Context;

    invoke-direct {v3, v4}, Lcom/mediatek/datatransfer/modules/MusicRestoreComposer;-><init>(Landroid/content/Context;)V

    invoke-direct {p0, v3}, Lcom/mediatek/datatransfer/RestoreEngine;->addComposer(Lcom/mediatek/datatransfer/modules/Composer;)V

    goto :goto_0

    :sswitch_8
    new-instance v3, Lcom/mediatek/datatransfer/modules/NoteBookRestoreComposer;

    iget-object v4, p0, Lcom/mediatek/datatransfer/RestoreEngine;->mContext:Landroid/content/Context;

    invoke-direct {v3, v4}, Lcom/mediatek/datatransfer/modules/NoteBookRestoreComposer;-><init>(Landroid/content/Context;)V

    invoke-direct {p0, v3}, Lcom/mediatek/datatransfer/RestoreEngine;->addComposer(Lcom/mediatek/datatransfer/modules/Composer;)V

    goto :goto_0

    :sswitch_9
    new-instance v3, Lcom/mediatek/datatransfer/modules/BookmarkRestoreComposer;

    iget-object v4, p0, Lcom/mediatek/datatransfer/RestoreEngine;->mContext:Landroid/content/Context;

    invoke-direct {v3, v4}, Lcom/mediatek/datatransfer/modules/BookmarkRestoreComposer;-><init>(Landroid/content/Context;)V

    invoke-direct {p0, v3}, Lcom/mediatek/datatransfer/RestoreEngine;->addComposer(Lcom/mediatek/datatransfer/modules/Composer;)V

    goto/16 :goto_0

    :cond_0
    return v0

    :sswitch_data_0
    .sparse-switch
        0x1 -> :sswitch_0
        0x2 -> :sswitch_2
        0x4 -> :sswitch_3
        0x8 -> :sswitch_5
        0x10 -> :sswitch_6
        0x20 -> :sswitch_4
        0x40 -> :sswitch_1
        0x80 -> :sswitch_7
        0x100 -> :sswitch_8
        0x200 -> :sswitch_9
    .end sparse-switch
.end method

.method private final updateInfo(Landroid/content/Context;Lcom/mediatek/datatransfer/ProgressReporter;)V
    .locals 0
    .param p1    # Landroid/content/Context;
    .param p2    # Lcom/mediatek/datatransfer/ProgressReporter;

    iput-object p1, p0, Lcom/mediatek/datatransfer/RestoreEngine;->mContext:Landroid/content/Context;

    iput-object p2, p0, Lcom/mediatek/datatransfer/RestoreEngine;->mReporter:Lcom/mediatek/datatransfer/ProgressReporter;

    return-void
.end method


# virtual methods
.method public cancel()V
    .locals 0

    return-void
.end method

.method public continueRestore()V
    .locals 0

    return-void
.end method

.method public isPaused()Z
    .locals 1

    iget-boolean v0, p0, Lcom/mediatek/datatransfer/RestoreEngine;->mIsPause:Z

    return v0
.end method

.method public isRunning()Z
    .locals 1

    iget-boolean v0, p0, Lcom/mediatek/datatransfer/RestoreEngine;->mIsRunning:Z

    return v0
.end method

.method public pause()V
    .locals 1

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/mediatek/datatransfer/RestoreEngine;->mIsPause:Z

    return-void
.end method

.method public setOnRestoreEndListner(Lcom/mediatek/datatransfer/RestoreEngine$OnRestoreDoneListner;)V
    .locals 0
    .param p1    # Lcom/mediatek/datatransfer/RestoreEngine$OnRestoreDoneListner;

    iput-object p1, p0, Lcom/mediatek/datatransfer/RestoreEngine;->mRestoreDoneListner:Lcom/mediatek/datatransfer/RestoreEngine$OnRestoreDoneListner;

    return-void
.end method

.method public setRestoreItemParam(ILjava/util/ArrayList;)V
    .locals 2
    .param p1    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    iget-object v0, p0, Lcom/mediatek/datatransfer/RestoreEngine;->mParasMap:Ljava/util/HashMap;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1, p2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    return-void
.end method

.method public setRestoreModelList(Ljava/util/ArrayList;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/Integer;",
            ">;)V"
        }
    .end annotation

    invoke-direct {p0}, Lcom/mediatek/datatransfer/RestoreEngine;->reset()V

    iput-object p1, p0, Lcom/mediatek/datatransfer/RestoreEngine;->mModuleList:Ljava/util/ArrayList;

    return-void
.end method

.method public startRestore(Ljava/lang/String;)V
    .locals 1
    .param p1    # Ljava/lang/String;

    if-eqz p1, :cond_0

    iget-object v0, p0, Lcom/mediatek/datatransfer/RestoreEngine;->mModuleList:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-lez v0, :cond_0

    iput-object p1, p0, Lcom/mediatek/datatransfer/RestoreEngine;->mRestoreFolder:Ljava/lang/String;

    iget-object v0, p0, Lcom/mediatek/datatransfer/RestoreEngine;->mModuleList:Ljava/util/ArrayList;

    invoke-direct {p0, v0}, Lcom/mediatek/datatransfer/RestoreEngine;->setupComposer(Ljava/util/List;)Z

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/mediatek/datatransfer/RestoreEngine;->mIsRunning:Z

    new-instance v0, Lcom/mediatek/datatransfer/RestoreEngine$RestoreThread;

    invoke-direct {v0, p0}, Lcom/mediatek/datatransfer/RestoreEngine$RestoreThread;-><init>(Lcom/mediatek/datatransfer/RestoreEngine;)V

    invoke-virtual {v0}, Ljava/lang/Thread;->start()V

    :cond_0
    return-void
.end method

.method public startRestore(Ljava/lang/String;Ljava/util/List;)V
    .locals 1
    .param p1    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/util/List",
            "<",
            "Ljava/lang/Integer;",
            ">;)V"
        }
    .end annotation

    invoke-direct {p0}, Lcom/mediatek/datatransfer/RestoreEngine;->reset()V

    if-eqz p1, :cond_0

    invoke-interface {p2}, Ljava/util/List;->size()I

    move-result v0

    if-lez v0, :cond_0

    iput-object p1, p0, Lcom/mediatek/datatransfer/RestoreEngine;->mRestoreFolder:Ljava/lang/String;

    invoke-direct {p0, p2}, Lcom/mediatek/datatransfer/RestoreEngine;->setupComposer(Ljava/util/List;)Z

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/mediatek/datatransfer/RestoreEngine;->mIsRunning:Z

    new-instance v0, Lcom/mediatek/datatransfer/RestoreEngine$RestoreThread;

    invoke-direct {v0, p0}, Lcom/mediatek/datatransfer/RestoreEngine$RestoreThread;-><init>(Lcom/mediatek/datatransfer/RestoreEngine;)V

    invoke-virtual {v0}, Ljava/lang/Thread;->start()V

    :cond_0
    return-void
.end method
