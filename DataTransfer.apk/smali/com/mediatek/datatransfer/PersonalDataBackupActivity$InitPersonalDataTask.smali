.class Lcom/mediatek/datatransfer/PersonalDataBackupActivity$InitPersonalDataTask;
.super Landroid/os/AsyncTask;
.source "PersonalDataBackupActivity.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/mediatek/datatransfer/PersonalDataBackupActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "InitPersonalDataTask"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/os/AsyncTask",
        "<",
        "Ljava/lang/Void;",
        "Ljava/lang/Void;",
        "Ljava/lang/Long;",
        ">;"
    }
.end annotation


# static fields
.field private static final TASK_TAG:Ljava/lang/String; = "InitPersonalDataTask"


# instance fields
.field mBackupDataList:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/mediatek/datatransfer/PersonalItemData;",
            ">;"
        }
    .end annotation
.end field

.field final synthetic this$0:Lcom/mediatek/datatransfer/PersonalDataBackupActivity;


# direct methods
.method private constructor <init>(Lcom/mediatek/datatransfer/PersonalDataBackupActivity;)V
    .locals 0

    iput-object p1, p0, Lcom/mediatek/datatransfer/PersonalDataBackupActivity$InitPersonalDataTask;->this$0:Lcom/mediatek/datatransfer/PersonalDataBackupActivity;

    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/mediatek/datatransfer/PersonalDataBackupActivity;Lcom/mediatek/datatransfer/PersonalDataBackupActivity$1;)V
    .locals 0
    .param p1    # Lcom/mediatek/datatransfer/PersonalDataBackupActivity;
    .param p2    # Lcom/mediatek/datatransfer/PersonalDataBackupActivity$1;

    invoke-direct {p0, p1}, Lcom/mediatek/datatransfer/PersonalDataBackupActivity$InitPersonalDataTask;-><init>(Lcom/mediatek/datatransfer/PersonalDataBackupActivity;)V

    return-void
.end method


# virtual methods
.method protected varargs doInBackground([Ljava/lang/Void;)Ljava/lang/Long;
    .locals 14
    .param p1    # [Ljava/lang/Void;

    iget-object v9, p0, Lcom/mediatek/datatransfer/PersonalDataBackupActivity$InitPersonalDataTask;->this$0:Lcom/mediatek/datatransfer/PersonalDataBackupActivity;

    iget-object v9, v9, Lcom/mediatek/datatransfer/PersonalDataBackupActivity;->messageEnable:Ljava/util/List;

    invoke-interface {v9}, Ljava/util/List;->clear()V

    new-instance v9, Ljava/util/ArrayList;

    invoke-direct {v9}, Ljava/util/ArrayList;-><init>()V

    iput-object v9, p0, Lcom/mediatek/datatransfer/PersonalDataBackupActivity$InitPersonalDataTask;->mBackupDataList:Ljava/util/ArrayList;

    const/4 v9, 0x6

    new-array v8, v9, [I

    fill-array-data v8, :array_0

    array-length v7, v8

    const/4 v5, 0x0

    :goto_0
    if-ge v5, v7, :cond_5

    const/4 v0, 0x1

    const/4 v2, 0x0

    aget v9, v8, v5

    sparse-switch v9, :sswitch_data_0

    :cond_0
    :goto_1
    const/4 v1, 0x0

    if-eqz v2, :cond_4

    const/4 v0, 0x1

    :goto_2
    new-instance v6, Lcom/mediatek/datatransfer/PersonalItemData;

    aget v9, v8, v5

    invoke-direct {v6, v9, v0}, Lcom/mediatek/datatransfer/PersonalItemData;-><init>(IZ)V

    iget-object v9, p0, Lcom/mediatek/datatransfer/PersonalDataBackupActivity$InitPersonalDataTask;->mBackupDataList:Ljava/util/ArrayList;

    invoke-virtual {v9, v6}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    add-int/lit8 v5, v5, 0x1

    goto :goto_0

    :sswitch_0
    iget-object v9, p0, Lcom/mediatek/datatransfer/PersonalDataBackupActivity$InitPersonalDataTask;->this$0:Lcom/mediatek/datatransfer/PersonalDataBackupActivity;

    const/4 v10, 0x1

    new-array v10, v10, [Lcom/mediatek/datatransfer/modules/Composer;

    const/4 v11, 0x0

    new-instance v12, Lcom/mediatek/datatransfer/modules/ContactBackupComposer;

    iget-object v13, p0, Lcom/mediatek/datatransfer/PersonalDataBackupActivity$InitPersonalDataTask;->this$0:Lcom/mediatek/datatransfer/PersonalDataBackupActivity;

    invoke-direct {v12, v13}, Lcom/mediatek/datatransfer/modules/ContactBackupComposer;-><init>(Landroid/content/Context;)V

    aput-object v12, v10, v11

    invoke-static {v9, v10}, Lcom/mediatek/datatransfer/PersonalDataBackupActivity;->access$1300(Lcom/mediatek/datatransfer/PersonalDataBackupActivity;[Lcom/mediatek/datatransfer/modules/Composer;)I

    move-result v2

    goto :goto_1

    :sswitch_1
    const/4 v4, 0x0

    const/4 v3, 0x0

    new-instance v1, Lcom/mediatek/datatransfer/modules/SmsBackupComposer;

    iget-object v9, p0, Lcom/mediatek/datatransfer/PersonalDataBackupActivity$InitPersonalDataTask;->this$0:Lcom/mediatek/datatransfer/PersonalDataBackupActivity;

    invoke-direct {v1, v9}, Lcom/mediatek/datatransfer/modules/SmsBackupComposer;-><init>(Landroid/content/Context;)V

    invoke-virtual {v1}, Lcom/mediatek/datatransfer/modules/SmsBackupComposer;->init()Z

    move-result v9

    if-eqz v9, :cond_1

    invoke-virtual {v1}, Lcom/mediatek/datatransfer/modules/SmsBackupComposer;->getCount()I

    move-result v4

    invoke-virtual {v1}, Lcom/mediatek/datatransfer/modules/SmsBackupComposer;->onEnd()V

    :cond_1
    if-eqz v4, :cond_2

    iget-object v9, p0, Lcom/mediatek/datatransfer/PersonalDataBackupActivity$InitPersonalDataTask;->this$0:Lcom/mediatek/datatransfer/PersonalDataBackupActivity;

    iget-object v9, v9, Lcom/mediatek/datatransfer/PersonalDataBackupActivity;->messageEnable:Ljava/util/List;

    iget-object v10, p0, Lcom/mediatek/datatransfer/PersonalDataBackupActivity$InitPersonalDataTask;->this$0:Lcom/mediatek/datatransfer/PersonalDataBackupActivity;

    const v11, 0x7f06003c

    invoke-virtual {v10, v11}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v10

    invoke-interface {v9, v10}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :cond_2
    new-instance v1, Lcom/mediatek/datatransfer/modules/MmsBackupComposer;

    iget-object v9, p0, Lcom/mediatek/datatransfer/PersonalDataBackupActivity$InitPersonalDataTask;->this$0:Lcom/mediatek/datatransfer/PersonalDataBackupActivity;

    invoke-direct {v1, v9}, Lcom/mediatek/datatransfer/modules/MmsBackupComposer;-><init>(Landroid/content/Context;)V

    invoke-virtual {v1}, Lcom/mediatek/datatransfer/modules/MmsBackupComposer;->init()Z

    move-result v9

    if-eqz v9, :cond_3

    invoke-virtual {v1}, Lcom/mediatek/datatransfer/modules/MmsBackupComposer;->getCount()I

    move-result v3

    invoke-virtual {v1}, Lcom/mediatek/datatransfer/modules/MmsBackupComposer;->onEnd()V

    :cond_3
    add-int v2, v4, v3

    invoke-static {}, Lcom/mediatek/datatransfer/PersonalDataBackupActivity;->access$200()Ljava/lang/String;

    move-result-object v9

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string v11, "countSMS = "

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v10

    const-string v11, "countMMS"

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-static {v9, v10}, Lcom/mediatek/datatransfer/utils/MyLogger;->logE(Ljava/lang/String;Ljava/lang/String;)V

    if-eqz v3, :cond_0

    iget-object v9, p0, Lcom/mediatek/datatransfer/PersonalDataBackupActivity$InitPersonalDataTask;->this$0:Lcom/mediatek/datatransfer/PersonalDataBackupActivity;

    iget-object v9, v9, Lcom/mediatek/datatransfer/PersonalDataBackupActivity;->messageEnable:Ljava/util/List;

    iget-object v10, p0, Lcom/mediatek/datatransfer/PersonalDataBackupActivity$InitPersonalDataTask;->this$0:Lcom/mediatek/datatransfer/PersonalDataBackupActivity;

    const v11, 0x7f06003d

    invoke-virtual {v10, v11}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v10

    invoke-interface {v9, v10}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto/16 :goto_1

    :sswitch_2
    iget-object v9, p0, Lcom/mediatek/datatransfer/PersonalDataBackupActivity$InitPersonalDataTask;->this$0:Lcom/mediatek/datatransfer/PersonalDataBackupActivity;

    const/4 v10, 0x1

    new-array v10, v10, [Lcom/mediatek/datatransfer/modules/Composer;

    const/4 v11, 0x0

    new-instance v12, Lcom/mediatek/datatransfer/modules/PictureBackupComposer;

    iget-object v13, p0, Lcom/mediatek/datatransfer/PersonalDataBackupActivity$InitPersonalDataTask;->this$0:Lcom/mediatek/datatransfer/PersonalDataBackupActivity;

    invoke-direct {v12, v13}, Lcom/mediatek/datatransfer/modules/PictureBackupComposer;-><init>(Landroid/content/Context;)V

    aput-object v12, v10, v11

    invoke-static {v9, v10}, Lcom/mediatek/datatransfer/PersonalDataBackupActivity;->access$1300(Lcom/mediatek/datatransfer/PersonalDataBackupActivity;[Lcom/mediatek/datatransfer/modules/Composer;)I

    move-result v2

    goto/16 :goto_1

    :sswitch_3
    iget-object v9, p0, Lcom/mediatek/datatransfer/PersonalDataBackupActivity$InitPersonalDataTask;->this$0:Lcom/mediatek/datatransfer/PersonalDataBackupActivity;

    const/4 v10, 0x1

    new-array v10, v10, [Lcom/mediatek/datatransfer/modules/Composer;

    const/4 v11, 0x0

    new-instance v12, Lcom/mediatek/datatransfer/modules/CalendarBackupComposer;

    iget-object v13, p0, Lcom/mediatek/datatransfer/PersonalDataBackupActivity$InitPersonalDataTask;->this$0:Lcom/mediatek/datatransfer/PersonalDataBackupActivity;

    invoke-direct {v12, v13}, Lcom/mediatek/datatransfer/modules/CalendarBackupComposer;-><init>(Landroid/content/Context;)V

    aput-object v12, v10, v11

    invoke-static {v9, v10}, Lcom/mediatek/datatransfer/PersonalDataBackupActivity;->access$1300(Lcom/mediatek/datatransfer/PersonalDataBackupActivity;[Lcom/mediatek/datatransfer/modules/Composer;)I

    move-result v2

    goto/16 :goto_1

    :sswitch_4
    iget-object v9, p0, Lcom/mediatek/datatransfer/PersonalDataBackupActivity$InitPersonalDataTask;->this$0:Lcom/mediatek/datatransfer/PersonalDataBackupActivity;

    const/4 v10, 0x1

    new-array v10, v10, [Lcom/mediatek/datatransfer/modules/Composer;

    const/4 v11, 0x0

    new-instance v12, Lcom/mediatek/datatransfer/modules/AppBackupComposer;

    iget-object v13, p0, Lcom/mediatek/datatransfer/PersonalDataBackupActivity$InitPersonalDataTask;->this$0:Lcom/mediatek/datatransfer/PersonalDataBackupActivity;

    invoke-direct {v12, v13}, Lcom/mediatek/datatransfer/modules/AppBackupComposer;-><init>(Landroid/content/Context;)V

    aput-object v12, v10, v11

    invoke-static {v9, v10}, Lcom/mediatek/datatransfer/PersonalDataBackupActivity;->access$1300(Lcom/mediatek/datatransfer/PersonalDataBackupActivity;[Lcom/mediatek/datatransfer/modules/Composer;)I

    move-result v2

    goto/16 :goto_1

    :sswitch_5
    iget-object v9, p0, Lcom/mediatek/datatransfer/PersonalDataBackupActivity$InitPersonalDataTask;->this$0:Lcom/mediatek/datatransfer/PersonalDataBackupActivity;

    const/4 v10, 0x1

    new-array v10, v10, [Lcom/mediatek/datatransfer/modules/Composer;

    const/4 v11, 0x0

    new-instance v12, Lcom/mediatek/datatransfer/modules/MusicBackupComposer;

    iget-object v13, p0, Lcom/mediatek/datatransfer/PersonalDataBackupActivity$InitPersonalDataTask;->this$0:Lcom/mediatek/datatransfer/PersonalDataBackupActivity;

    invoke-direct {v12, v13}, Lcom/mediatek/datatransfer/modules/MusicBackupComposer;-><init>(Landroid/content/Context;)V

    aput-object v12, v10, v11

    invoke-static {v9, v10}, Lcom/mediatek/datatransfer/PersonalDataBackupActivity;->access$1300(Lcom/mediatek/datatransfer/PersonalDataBackupActivity;[Lcom/mediatek/datatransfer/modules/Composer;)I

    move-result v2

    goto/16 :goto_1

    :sswitch_6
    iget-object v9, p0, Lcom/mediatek/datatransfer/PersonalDataBackupActivity$InitPersonalDataTask;->this$0:Lcom/mediatek/datatransfer/PersonalDataBackupActivity;

    const/4 v10, 0x1

    new-array v10, v10, [Lcom/mediatek/datatransfer/modules/Composer;

    const/4 v11, 0x0

    new-instance v12, Lcom/mediatek/datatransfer/modules/NoteBookBackupComposer;

    iget-object v13, p0, Lcom/mediatek/datatransfer/PersonalDataBackupActivity$InitPersonalDataTask;->this$0:Lcom/mediatek/datatransfer/PersonalDataBackupActivity;

    invoke-direct {v12, v13}, Lcom/mediatek/datatransfer/modules/NoteBookBackupComposer;-><init>(Landroid/content/Context;)V

    aput-object v12, v10, v11

    invoke-static {v9, v10}, Lcom/mediatek/datatransfer/PersonalDataBackupActivity;->access$1300(Lcom/mediatek/datatransfer/PersonalDataBackupActivity;[Lcom/mediatek/datatransfer/modules/Composer;)I

    move-result v2

    goto/16 :goto_1

    :sswitch_7
    iget-object v9, p0, Lcom/mediatek/datatransfer/PersonalDataBackupActivity$InitPersonalDataTask;->this$0:Lcom/mediatek/datatransfer/PersonalDataBackupActivity;

    const/4 v10, 0x1

    new-array v10, v10, [Lcom/mediatek/datatransfer/modules/Composer;

    const/4 v11, 0x0

    new-instance v12, Lcom/mediatek/datatransfer/modules/BookmarkBackupComposer;

    iget-object v13, p0, Lcom/mediatek/datatransfer/PersonalDataBackupActivity$InitPersonalDataTask;->this$0:Lcom/mediatek/datatransfer/PersonalDataBackupActivity;

    invoke-direct {v12, v13}, Lcom/mediatek/datatransfer/modules/BookmarkBackupComposer;-><init>(Landroid/content/Context;)V

    aput-object v12, v10, v11

    invoke-static {v9, v10}, Lcom/mediatek/datatransfer/PersonalDataBackupActivity;->access$1300(Lcom/mediatek/datatransfer/PersonalDataBackupActivity;[Lcom/mediatek/datatransfer/modules/Composer;)I

    move-result v2

    goto/16 :goto_1

    :cond_4
    const/4 v0, 0x0

    goto/16 :goto_2

    :cond_5
    const/4 v9, 0x0

    return-object v9

    nop

    :array_0
    .array-data 4
        0x1
        0x40
        0x20
        0x8
        0x80
        0x200
    .end array-data

    :sswitch_data_0
    .sparse-switch
        0x1 -> :sswitch_0
        0x8 -> :sswitch_3
        0x10 -> :sswitch_4
        0x20 -> :sswitch_2
        0x40 -> :sswitch_1
        0x80 -> :sswitch_5
        0x100 -> :sswitch_6
        0x200 -> :sswitch_7
    .end sparse-switch
.end method

.method protected bridge synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1
    .param p1    # [Ljava/lang/Object;

    check-cast p1, [Ljava/lang/Void;

    invoke-virtual {p0, p1}, Lcom/mediatek/datatransfer/PersonalDataBackupActivity$InitPersonalDataTask;->doInBackground([Ljava/lang/Void;)Ljava/lang/Long;

    move-result-object v0

    return-object v0
.end method

.method protected onPostExecute(Ljava/lang/Long;)V
    .locals 3
    .param p1    # Ljava/lang/Long;

    iget-object v0, p0, Lcom/mediatek/datatransfer/PersonalDataBackupActivity$InitPersonalDataTask;->this$0:Lcom/mediatek/datatransfer/PersonalDataBackupActivity;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/mediatek/datatransfer/AbstractBackupActivity;->showLoadingContent(Z)V

    iget-object v0, p0, Lcom/mediatek/datatransfer/PersonalDataBackupActivity$InitPersonalDataTask;->this$0:Lcom/mediatek/datatransfer/PersonalDataBackupActivity;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/mediatek/datatransfer/AbstractBackupActivity;->setButtonsEnable(Z)V

    iget-object v0, p0, Lcom/mediatek/datatransfer/PersonalDataBackupActivity$InitPersonalDataTask;->this$0:Lcom/mediatek/datatransfer/PersonalDataBackupActivity;

    iget-object v1, p0, Lcom/mediatek/datatransfer/PersonalDataBackupActivity$InitPersonalDataTask;->mBackupDataList:Ljava/util/ArrayList;

    invoke-static {v0, v1}, Lcom/mediatek/datatransfer/PersonalDataBackupActivity;->access$1100(Lcom/mediatek/datatransfer/PersonalDataBackupActivity;Ljava/util/ArrayList;)V

    iget-object v0, p0, Lcom/mediatek/datatransfer/PersonalDataBackupActivity$InitPersonalDataTask;->this$0:Lcom/mediatek/datatransfer/PersonalDataBackupActivity;

    iget-object v1, p0, Lcom/mediatek/datatransfer/PersonalDataBackupActivity$InitPersonalDataTask;->this$0:Lcom/mediatek/datatransfer/PersonalDataBackupActivity;

    invoke-static {v1}, Lcom/mediatek/datatransfer/PersonalDataBackupActivity;->access$1200(Lcom/mediatek/datatransfer/PersonalDataBackupActivity;)Lcom/mediatek/datatransfer/BackupService$OnBackupStatusListener;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/mediatek/datatransfer/AbstractBackupActivity;->setOnBackupStatusListener(Lcom/mediatek/datatransfer/BackupService$OnBackupStatusListener;)V

    invoke-static {}, Lcom/mediatek/datatransfer/PersonalDataBackupActivity;->access$200()Ljava/lang/String;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "---onPostExecute----getTitle"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/mediatek/datatransfer/PersonalDataBackupActivity$InitPersonalDataTask;->this$0:Lcom/mediatek/datatransfer/PersonalDataBackupActivity;

    invoke-virtual {v2}, Landroid/app/Activity;->getTitle()Ljava/lang/CharSequence;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/mediatek/datatransfer/utils/MyLogger;->logD(Ljava/lang/String;Ljava/lang/String;)V

    invoke-super {p0, p1}, Landroid/os/AsyncTask;->onPostExecute(Ljava/lang/Object;)V

    return-void
.end method

.method protected bridge synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 0
    .param p1    # Ljava/lang/Object;

    check-cast p1, Ljava/lang/Long;

    invoke-virtual {p0, p1}, Lcom/mediatek/datatransfer/PersonalDataBackupActivity$InitPersonalDataTask;->onPostExecute(Ljava/lang/Long;)V

    return-void
.end method

.method protected onPreExecute()V
    .locals 2

    invoke-super {p0}, Landroid/os/AsyncTask;->onPreExecute()V

    invoke-static {}, Lcom/mediatek/datatransfer/PersonalDataBackupActivity;->access$200()Ljava/lang/String;

    move-result-object v0

    const-string v1, "InitPersonalDataTask---onPreExecute"

    invoke-static {v0, v1}, Lcom/mediatek/datatransfer/utils/MyLogger;->logD(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/mediatek/datatransfer/PersonalDataBackupActivity$InitPersonalDataTask;->this$0:Lcom/mediatek/datatransfer/PersonalDataBackupActivity;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/mediatek/datatransfer/AbstractBackupActivity;->showLoadingContent(Z)V

    iget-object v0, p0, Lcom/mediatek/datatransfer/PersonalDataBackupActivity$InitPersonalDataTask;->this$0:Lcom/mediatek/datatransfer/PersonalDataBackupActivity;

    const v1, 0x7f060028

    invoke-virtual {v0, v1}, Landroid/app/Activity;->setTitle(I)V

    iget-object v0, p0, Lcom/mediatek/datatransfer/PersonalDataBackupActivity$InitPersonalDataTask;->this$0:Lcom/mediatek/datatransfer/PersonalDataBackupActivity;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/mediatek/datatransfer/AbstractBackupActivity;->setButtonsEnable(Z)V

    return-void
.end method
