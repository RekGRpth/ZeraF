.class Lcom/mediatek/datatransfer/AbstractBackupActivity$7;
.super Ljava/lang/Object;
.source "AbstractBackupActivity.java"

# interfaces
.implements Landroid/content/DialogInterface$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/mediatek/datatransfer/AbstractBackupActivity;->onCreateDialog(ILandroid/os/Bundle;)Landroid/app/Dialog;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/mediatek/datatransfer/AbstractBackupActivity;


# direct methods
.method constructor <init>(Lcom/mediatek/datatransfer/AbstractBackupActivity;)V
    .locals 0

    iput-object p1, p0, Lcom/mediatek/datatransfer/AbstractBackupActivity$7;->this$0:Lcom/mediatek/datatransfer/AbstractBackupActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/content/DialogInterface;I)V
    .locals 2
    .param p1    # Landroid/content/DialogInterface;
    .param p2    # I

    iget-object v0, p0, Lcom/mediatek/datatransfer/AbstractBackupActivity$7;->this$0:Lcom/mediatek/datatransfer/AbstractBackupActivity;

    iget-object v0, v0, Lcom/mediatek/datatransfer/AbstractBackupActivity;->mBackupService:Lcom/mediatek/datatransfer/BackupService$BackupBinder;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/mediatek/datatransfer/AbstractBackupActivity$7;->this$0:Lcom/mediatek/datatransfer/AbstractBackupActivity;

    iget-object v0, v0, Lcom/mediatek/datatransfer/AbstractBackupActivity;->mBackupService:Lcom/mediatek/datatransfer/BackupService$BackupBinder;

    invoke-virtual {v0}, Lcom/mediatek/datatransfer/BackupService$BackupBinder;->getState()I

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/mediatek/datatransfer/AbstractBackupActivity$7;->this$0:Lcom/mediatek/datatransfer/AbstractBackupActivity;

    iget-object v0, v0, Lcom/mediatek/datatransfer/AbstractBackupActivity;->mBackupService:Lcom/mediatek/datatransfer/BackupService$BackupBinder;

    invoke-virtual {v0}, Lcom/mediatek/datatransfer/BackupService$BackupBinder;->getState()I

    move-result v0

    const/4 v1, 0x5

    if-eq v0, v1, :cond_1

    iget-object v0, p0, Lcom/mediatek/datatransfer/AbstractBackupActivity$7;->this$0:Lcom/mediatek/datatransfer/AbstractBackupActivity;

    iget-object v0, v0, Lcom/mediatek/datatransfer/AbstractBackupActivity;->mCancelDlg:Landroid/app/ProgressDialog;

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/mediatek/datatransfer/AbstractBackupActivity$7;->this$0:Lcom/mediatek/datatransfer/AbstractBackupActivity;

    iget-object v1, p0, Lcom/mediatek/datatransfer/AbstractBackupActivity$7;->this$0:Lcom/mediatek/datatransfer/AbstractBackupActivity;

    invoke-static {v1}, Lcom/mediatek/datatransfer/AbstractBackupActivity;->access$100(Lcom/mediatek/datatransfer/AbstractBackupActivity;)Landroid/app/ProgressDialog;

    move-result-object v1

    iput-object v1, v0, Lcom/mediatek/datatransfer/AbstractBackupActivity;->mCancelDlg:Landroid/app/ProgressDialog;

    :cond_0
    iget-object v0, p0, Lcom/mediatek/datatransfer/AbstractBackupActivity$7;->this$0:Lcom/mediatek/datatransfer/AbstractBackupActivity;

    iget-object v0, v0, Lcom/mediatek/datatransfer/AbstractBackupActivity;->mCancelDlg:Landroid/app/ProgressDialog;

    invoke-virtual {v0}, Landroid/app/Dialog;->show()V

    iget-object v0, p0, Lcom/mediatek/datatransfer/AbstractBackupActivity$7;->this$0:Lcom/mediatek/datatransfer/AbstractBackupActivity;

    iget-object v0, v0, Lcom/mediatek/datatransfer/AbstractBackupActivity;->mBackupService:Lcom/mediatek/datatransfer/BackupService$BackupBinder;

    invoke-virtual {v0}, Lcom/mediatek/datatransfer/BackupService$BackupBinder;->cancelBackup()V

    :cond_1
    return-void
.end method
