.class Lcom/mediatek/datatransfer/PersonalDataBackupActivity$12;
.super Ljava/lang/Object;
.source "PersonalDataBackupActivity.java"

# interfaces
.implements Landroid/content/DialogInterface$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/mediatek/datatransfer/PersonalDataBackupActivity;->createFolderEditorDialog()Landroid/app/AlertDialog;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/mediatek/datatransfer/PersonalDataBackupActivity;


# direct methods
.method constructor <init>(Lcom/mediatek/datatransfer/PersonalDataBackupActivity;)V
    .locals 0

    iput-object p1, p0, Lcom/mediatek/datatransfer/PersonalDataBackupActivity$12;->this$0:Lcom/mediatek/datatransfer/PersonalDataBackupActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private hideKeyboard(Landroid/widget/EditText;)V
    .locals 3
    .param p1    # Landroid/widget/EditText;

    iget-object v1, p0, Lcom/mediatek/datatransfer/PersonalDataBackupActivity$12;->this$0:Lcom/mediatek/datatransfer/PersonalDataBackupActivity;

    const-string v2, "input_method"

    invoke-virtual {v1, v2}, Landroid/app/Activity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/inputmethod/InputMethodManager;

    invoke-virtual {p1}, Landroid/view/View;->getWindowToken()Landroid/os/IBinder;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/view/inputmethod/InputMethodManager;->hideSoftInputFromWindow(Landroid/os/IBinder;I)Z

    return-void
.end method


# virtual methods
.method public onClick(Landroid/content/DialogInterface;I)V
    .locals 7
    .param p1    # Landroid/content/DialogInterface;
    .param p2    # I

    move-object v1, p1

    check-cast v1, Landroid/app/AlertDialog;

    const v5, 0x7f08000b

    invoke-virtual {v1, v5}, Landroid/app/Dialog;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/EditText;

    if-eqz v2, :cond_0

    invoke-virtual {v2}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v3

    invoke-static {}, Lcom/mediatek/datatransfer/utils/SDCardUtils;->getPersonalDataBackupPath()Ljava/lang/String;

    move-result-object v4

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    sget-object v5, Ljava/io/File;->separator:Ljava/lang/String;

    invoke-virtual {v0, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/CharSequence;)Ljava/lang/StringBuilder;

    iget-object v5, p0, Lcom/mediatek/datatransfer/PersonalDataBackupActivity$12;->this$0:Lcom/mediatek/datatransfer/PersonalDataBackupActivity;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Lcom/mediatek/datatransfer/PersonalDataBackupActivity;->access$902(Lcom/mediatek/datatransfer/PersonalDataBackupActivity;Ljava/lang/String;)Ljava/lang/String;

    invoke-direct {p0, v2}, Lcom/mediatek/datatransfer/PersonalDataBackupActivity$12;->hideKeyboard(Landroid/widget/EditText;)V

    const-string v5, ""

    invoke-virtual {v2, v5}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v5, p0, Lcom/mediatek/datatransfer/PersonalDataBackupActivity$12;->this$0:Lcom/mediatek/datatransfer/PersonalDataBackupActivity;

    iget-object v6, p0, Lcom/mediatek/datatransfer/PersonalDataBackupActivity$12;->this$0:Lcom/mediatek/datatransfer/PersonalDataBackupActivity;

    invoke-static {v6}, Lcom/mediatek/datatransfer/PersonalDataBackupActivity;->access$900(Lcom/mediatek/datatransfer/PersonalDataBackupActivity;)Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Lcom/mediatek/datatransfer/PersonalDataBackupActivity;->access$1000(Lcom/mediatek/datatransfer/PersonalDataBackupActivity;Ljava/lang/String;)V

    :goto_0
    return-void

    :cond_0
    invoke-static {}, Lcom/mediatek/datatransfer/PersonalDataBackupActivity;->access$200()Ljava/lang/String;

    move-result-object v5

    const-string v6, " can not get folder name"

    invoke-static {v5, v6}, Lcom/mediatek/datatransfer/utils/MyLogger;->logE(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method
