.class Lcom/mediatek/datatransfer/PersonalDataBackupActivity$PersonalDataBackupStatusListener$2;
.super Ljava/lang/Object;
.source "PersonalDataBackupActivity.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/mediatek/datatransfer/PersonalDataBackupActivity$PersonalDataBackupStatusListener;->onComposerChanged(Lcom/mediatek/datatransfer/modules/Composer;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$1:Lcom/mediatek/datatransfer/PersonalDataBackupActivity$PersonalDataBackupStatusListener;

.field final synthetic val$composer:Lcom/mediatek/datatransfer/modules/Composer;


# direct methods
.method constructor <init>(Lcom/mediatek/datatransfer/PersonalDataBackupActivity$PersonalDataBackupStatusListener;Lcom/mediatek/datatransfer/modules/Composer;)V
    .locals 0

    iput-object p1, p0, Lcom/mediatek/datatransfer/PersonalDataBackupActivity$PersonalDataBackupStatusListener$2;->this$1:Lcom/mediatek/datatransfer/PersonalDataBackupActivity$PersonalDataBackupStatusListener;

    iput-object p2, p0, Lcom/mediatek/datatransfer/PersonalDataBackupActivity$PersonalDataBackupStatusListener$2;->val$composer:Lcom/mediatek/datatransfer/modules/Composer;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 3

    iget-object v1, p0, Lcom/mediatek/datatransfer/PersonalDataBackupActivity$PersonalDataBackupStatusListener$2;->this$1:Lcom/mediatek/datatransfer/PersonalDataBackupActivity$PersonalDataBackupStatusListener;

    iget-object v1, v1, Lcom/mediatek/datatransfer/PersonalDataBackupActivity$PersonalDataBackupStatusListener;->this$0:Lcom/mediatek/datatransfer/PersonalDataBackupActivity;

    iget-object v2, p0, Lcom/mediatek/datatransfer/PersonalDataBackupActivity$PersonalDataBackupStatusListener$2;->val$composer:Lcom/mediatek/datatransfer/modules/Composer;

    invoke-virtual {v2}, Lcom/mediatek/datatransfer/modules/Composer;->getModuleType()I

    move-result v2

    invoke-virtual {v1, v2}, Lcom/mediatek/datatransfer/PersonalDataBackupActivity;->getProgressDlgMessage(I)Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/mediatek/datatransfer/PersonalDataBackupActivity$PersonalDataBackupStatusListener$2;->this$1:Lcom/mediatek/datatransfer/PersonalDataBackupActivity$PersonalDataBackupStatusListener;

    iget-object v1, v1, Lcom/mediatek/datatransfer/PersonalDataBackupActivity$PersonalDataBackupStatusListener;->this$0:Lcom/mediatek/datatransfer/PersonalDataBackupActivity;

    iget-object v1, v1, Lcom/mediatek/datatransfer/AbstractBackupActivity;->mProgressDialog:Landroid/app/ProgressDialog;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/mediatek/datatransfer/PersonalDataBackupActivity$PersonalDataBackupStatusListener$2;->this$1:Lcom/mediatek/datatransfer/PersonalDataBackupActivity$PersonalDataBackupStatusListener;

    iget-object v1, v1, Lcom/mediatek/datatransfer/PersonalDataBackupActivity$PersonalDataBackupStatusListener;->this$0:Lcom/mediatek/datatransfer/PersonalDataBackupActivity;

    iget-object v1, v1, Lcom/mediatek/datatransfer/AbstractBackupActivity;->mProgressDialog:Landroid/app/ProgressDialog;

    invoke-virtual {v1, v0}, Landroid/app/ProgressDialog;->setMessage(Ljava/lang/CharSequence;)V

    iget-object v1, p0, Lcom/mediatek/datatransfer/PersonalDataBackupActivity$PersonalDataBackupStatusListener$2;->this$1:Lcom/mediatek/datatransfer/PersonalDataBackupActivity$PersonalDataBackupStatusListener;

    iget-object v1, v1, Lcom/mediatek/datatransfer/PersonalDataBackupActivity$PersonalDataBackupStatusListener;->this$0:Lcom/mediatek/datatransfer/PersonalDataBackupActivity;

    iget-object v1, v1, Lcom/mediatek/datatransfer/AbstractBackupActivity;->mProgressDialog:Landroid/app/ProgressDialog;

    iget-object v2, p0, Lcom/mediatek/datatransfer/PersonalDataBackupActivity$PersonalDataBackupStatusListener$2;->val$composer:Lcom/mediatek/datatransfer/modules/Composer;

    invoke-virtual {v2}, Lcom/mediatek/datatransfer/modules/Composer;->getCount()I

    move-result v2

    invoke-virtual {v1, v2}, Landroid/app/ProgressDialog;->setMax(I)V

    iget-object v1, p0, Lcom/mediatek/datatransfer/PersonalDataBackupActivity$PersonalDataBackupStatusListener$2;->this$1:Lcom/mediatek/datatransfer/PersonalDataBackupActivity$PersonalDataBackupStatusListener;

    iget-object v1, v1, Lcom/mediatek/datatransfer/PersonalDataBackupActivity$PersonalDataBackupStatusListener;->this$0:Lcom/mediatek/datatransfer/PersonalDataBackupActivity;

    iget-object v1, v1, Lcom/mediatek/datatransfer/AbstractBackupActivity;->mProgressDialog:Landroid/app/ProgressDialog;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Landroid/app/ProgressDialog;->setProgress(I)V

    :cond_0
    return-void
.end method
