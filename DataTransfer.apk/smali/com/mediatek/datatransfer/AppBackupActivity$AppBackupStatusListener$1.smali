.class Lcom/mediatek/datatransfer/AppBackupActivity$AppBackupStatusListener$1;
.super Ljava/lang/Object;
.source "AppBackupActivity.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/mediatek/datatransfer/AppBackupActivity$AppBackupStatusListener;->onComposerChanged(Lcom/mediatek/datatransfer/modules/Composer;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$1:Lcom/mediatek/datatransfer/AppBackupActivity$AppBackupStatusListener;

.field final synthetic val$composer:Lcom/mediatek/datatransfer/modules/Composer;


# direct methods
.method constructor <init>(Lcom/mediatek/datatransfer/AppBackupActivity$AppBackupStatusListener;Lcom/mediatek/datatransfer/modules/Composer;)V
    .locals 0

    iput-object p1, p0, Lcom/mediatek/datatransfer/AppBackupActivity$AppBackupStatusListener$1;->this$1:Lcom/mediatek/datatransfer/AppBackupActivity$AppBackupStatusListener;

    iput-object p2, p0, Lcom/mediatek/datatransfer/AppBackupActivity$AppBackupStatusListener$1;->val$composer:Lcom/mediatek/datatransfer/modules/Composer;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 7

    const/4 v6, 0x0

    iget-object v3, p0, Lcom/mediatek/datatransfer/AppBackupActivity$AppBackupStatusListener$1;->this$1:Lcom/mediatek/datatransfer/AppBackupActivity$AppBackupStatusListener;

    iget-object v3, v3, Lcom/mediatek/datatransfer/AppBackupActivity$AppBackupStatusListener;->this$0:Lcom/mediatek/datatransfer/AppBackupActivity;

    iget-object v3, v3, Lcom/mediatek/datatransfer/AbstractBackupActivity;->mBackupService:Lcom/mediatek/datatransfer/BackupService$BackupBinder;

    const/16 v4, 0x10

    invoke-virtual {v3, v4}, Lcom/mediatek/datatransfer/BackupService$BackupBinder;->getBackupItemParam(I)Ljava/util/ArrayList;

    move-result-object v2

    invoke-virtual {v2, v6}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    iget-object v3, p0, Lcom/mediatek/datatransfer/AppBackupActivity$AppBackupStatusListener$1;->this$1:Lcom/mediatek/datatransfer/AppBackupActivity$AppBackupStatusListener;

    iget-object v3, v3, Lcom/mediatek/datatransfer/AppBackupActivity$AppBackupStatusListener;->this$0:Lcom/mediatek/datatransfer/AppBackupActivity;

    invoke-static {v3}, Lcom/mediatek/datatransfer/AppBackupActivity;->access$400(Lcom/mediatek/datatransfer/AppBackupActivity;)Ljava/lang/String;

    move-result-object v3

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "onComposerChanged, first packageName is "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/mediatek/datatransfer/utils/MyLogger;->logV(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v3, p0, Lcom/mediatek/datatransfer/AppBackupActivity$AppBackupStatusListener$1;->this$1:Lcom/mediatek/datatransfer/AppBackupActivity$AppBackupStatusListener;

    iget-object v3, v3, Lcom/mediatek/datatransfer/AppBackupActivity$AppBackupStatusListener;->this$0:Lcom/mediatek/datatransfer/AppBackupActivity;

    iget-object v4, p0, Lcom/mediatek/datatransfer/AppBackupActivity$AppBackupStatusListener$1;->this$1:Lcom/mediatek/datatransfer/AppBackupActivity$AppBackupStatusListener;

    iget-object v4, v4, Lcom/mediatek/datatransfer/AppBackupActivity$AppBackupStatusListener;->this$0:Lcom/mediatek/datatransfer/AppBackupActivity;

    invoke-static {v4, v1}, Lcom/mediatek/datatransfer/AppBackupActivity;->access$500(Lcom/mediatek/datatransfer/AppBackupActivity;Ljava/lang/String;)Lcom/mediatek/datatransfer/AppSnippet;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/mediatek/datatransfer/AppBackupActivity;->access$600(Lcom/mediatek/datatransfer/AppBackupActivity;Lcom/mediatek/datatransfer/AppSnippet;)Ljava/lang/String;

    move-result-object v0

    iget-object v3, p0, Lcom/mediatek/datatransfer/AppBackupActivity$AppBackupStatusListener$1;->this$1:Lcom/mediatek/datatransfer/AppBackupActivity$AppBackupStatusListener;

    iget-object v3, v3, Lcom/mediatek/datatransfer/AppBackupActivity$AppBackupStatusListener;->this$0:Lcom/mediatek/datatransfer/AppBackupActivity;

    iget-object v3, v3, Lcom/mediatek/datatransfer/AbstractBackupActivity;->mProgressDialog:Landroid/app/ProgressDialog;

    if-eqz v3, :cond_0

    iget-object v3, p0, Lcom/mediatek/datatransfer/AppBackupActivity$AppBackupStatusListener$1;->this$1:Lcom/mediatek/datatransfer/AppBackupActivity$AppBackupStatusListener;

    iget-object v3, v3, Lcom/mediatek/datatransfer/AppBackupActivity$AppBackupStatusListener;->this$0:Lcom/mediatek/datatransfer/AppBackupActivity;

    iget-object v3, v3, Lcom/mediatek/datatransfer/AbstractBackupActivity;->mProgressDialog:Landroid/app/ProgressDialog;

    invoke-virtual {v3, v0}, Landroid/app/ProgressDialog;->setMessage(Ljava/lang/CharSequence;)V

    iget-object v3, p0, Lcom/mediatek/datatransfer/AppBackupActivity$AppBackupStatusListener$1;->this$1:Lcom/mediatek/datatransfer/AppBackupActivity$AppBackupStatusListener;

    iget-object v3, v3, Lcom/mediatek/datatransfer/AppBackupActivity$AppBackupStatusListener;->this$0:Lcom/mediatek/datatransfer/AppBackupActivity;

    iget-object v3, v3, Lcom/mediatek/datatransfer/AbstractBackupActivity;->mProgressDialog:Landroid/app/ProgressDialog;

    iget-object v4, p0, Lcom/mediatek/datatransfer/AppBackupActivity$AppBackupStatusListener$1;->val$composer:Lcom/mediatek/datatransfer/modules/Composer;

    invoke-virtual {v4}, Lcom/mediatek/datatransfer/modules/Composer;->getCount()I

    move-result v4

    invoke-virtual {v3, v4}, Landroid/app/ProgressDialog;->setMax(I)V

    iget-object v3, p0, Lcom/mediatek/datatransfer/AppBackupActivity$AppBackupStatusListener$1;->this$1:Lcom/mediatek/datatransfer/AppBackupActivity$AppBackupStatusListener;

    iget-object v3, v3, Lcom/mediatek/datatransfer/AppBackupActivity$AppBackupStatusListener;->this$0:Lcom/mediatek/datatransfer/AppBackupActivity;

    iget-object v3, v3, Lcom/mediatek/datatransfer/AbstractBackupActivity;->mProgressDialog:Landroid/app/ProgressDialog;

    invoke-virtual {v3, v6}, Landroid/app/ProgressDialog;->setProgress(I)V

    :cond_0
    return-void
.end method
