.class Lcom/mediatek/datatransfer/AppRestoreActivity$AppRestoreAdapter;
.super Landroid/widget/BaseAdapter;
.source "AppRestoreActivity.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/mediatek/datatransfer/AppRestoreActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "AppRestoreAdapter"
.end annotation


# instance fields
.field private mInflater:Landroid/view/LayoutInflater;

.field private mLayoutId:I

.field private mList:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/mediatek/datatransfer/AppSnippet;",
            ">;"
        }
    .end annotation
.end field

.field final synthetic this$0:Lcom/mediatek/datatransfer/AppRestoreActivity;


# direct methods
.method public constructor <init>(Lcom/mediatek/datatransfer/AppRestoreActivity;Landroid/content/Context;Ljava/util/List;I)V
    .locals 1
    .param p2    # Landroid/content/Context;
    .param p4    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Ljava/util/List",
            "<",
            "Lcom/mediatek/datatransfer/AppSnippet;",
            ">;I)V"
        }
    .end annotation

    iput-object p1, p0, Lcom/mediatek/datatransfer/AppRestoreActivity$AppRestoreAdapter;->this$0:Lcom/mediatek/datatransfer/AppRestoreActivity;

    invoke-direct {p0}, Landroid/widget/BaseAdapter;-><init>()V

    iput-object p3, p0, Lcom/mediatek/datatransfer/AppRestoreActivity$AppRestoreAdapter;->mList:Ljava/util/List;

    iput p4, p0, Lcom/mediatek/datatransfer/AppRestoreActivity$AppRestoreAdapter;->mLayoutId:I

    invoke-static {p2}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    iput-object v0, p0, Lcom/mediatek/datatransfer/AppRestoreActivity$AppRestoreAdapter;->mInflater:Landroid/view/LayoutInflater;

    return-void
.end method


# virtual methods
.method public changeData(Ljava/util/List;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/mediatek/datatransfer/AppSnippet;",
            ">;)V"
        }
    .end annotation

    iput-object p1, p0, Lcom/mediatek/datatransfer/AppRestoreActivity$AppRestoreAdapter;->mList:Ljava/util/List;

    return-void
.end method

.method public getCount()I
    .locals 1

    iget-object v0, p0, Lcom/mediatek/datatransfer/AppRestoreActivity$AppRestoreAdapter;->mList:Ljava/util/List;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lcom/mediatek/datatransfer/AppRestoreActivity$AppRestoreAdapter;->mList:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    goto :goto_0
.end method

.method public getItem(I)Ljava/lang/Object;
    .locals 1
    .param p1    # I

    iget-object v0, p0, Lcom/mediatek/datatransfer/AppRestoreActivity$AppRestoreAdapter;->mList:Ljava/util/List;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/mediatek/datatransfer/AppRestoreActivity$AppRestoreAdapter;->mList:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    goto :goto_0
.end method

.method public getItemId(I)J
    .locals 2
    .param p1    # I

    iget-object v0, p0, Lcom/mediatek/datatransfer/AppRestoreActivity$AppRestoreAdapter;->mList:Ljava/util/List;

    if-nez v0, :cond_0

    const-wide/16 v0, 0x0

    :goto_0
    return-wide v0

    :cond_0
    iget-object v0, p0, Lcom/mediatek/datatransfer/AppRestoreActivity$AppRestoreAdapter;->mList:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/mediatek/datatransfer/AppSnippet;

    invoke-virtual {v0}, Lcom/mediatek/datatransfer/AppSnippet;->getPackageName()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    int-to-long v0, v0

    goto :goto_0
.end method

.method public getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 8
    .param p1    # I
    .param p2    # Landroid/view/View;
    .param p3    # Landroid/view/ViewGroup;

    iget-object v5, p0, Lcom/mediatek/datatransfer/AppRestoreActivity$AppRestoreAdapter;->mList:Ljava/util/List;

    if-nez v5, :cond_0

    const/4 v4, 0x0

    :goto_0
    return-object v4

    :cond_0
    move-object v4, p2

    if-nez v4, :cond_1

    iget-object v5, p0, Lcom/mediatek/datatransfer/AppRestoreActivity$AppRestoreAdapter;->mInflater:Landroid/view/LayoutInflater;

    iget v6, p0, Lcom/mediatek/datatransfer/AppRestoreActivity$AppRestoreAdapter;->mLayoutId:I

    const/4 v7, 0x0

    invoke-virtual {v5, v6, p3, v7}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v4

    :cond_1
    iget-object v5, p0, Lcom/mediatek/datatransfer/AppRestoreActivity$AppRestoreAdapter;->mList:Ljava/util/List;

    invoke-interface {v5, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/mediatek/datatransfer/AppSnippet;

    const/high16 v5, 0x7f080000

    invoke-virtual {v4, v5}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ImageView;

    const v5, 0x7f080001

    invoke-virtual {v4, v5}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/TextView;

    const v5, 0x7f080002

    invoke-virtual {v4, v5}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/CheckBox;

    invoke-virtual {v2}, Lcom/mediatek/datatransfer/AppSnippet;->getIcon()Landroid/graphics/drawable/Drawable;

    move-result-object v5

    invoke-virtual {v1, v5}, Landroid/view/View;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    invoke-virtual {v2}, Lcom/mediatek/datatransfer/AppSnippet;->getName()Ljava/lang/CharSequence;

    move-result-object v5

    invoke-virtual {v3, v5}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v5, p0, Lcom/mediatek/datatransfer/AppRestoreActivity$AppRestoreAdapter;->this$0:Lcom/mediatek/datatransfer/AppRestoreActivity;

    invoke-virtual {v5, p1}, Lcom/mediatek/datatransfer/CheckedListActivity;->isItemCheckedByPosition(I)Z

    move-result v5

    invoke-virtual {v0, v5}, Landroid/widget/CompoundButton;->setChecked(Z)V

    goto :goto_0
.end method
