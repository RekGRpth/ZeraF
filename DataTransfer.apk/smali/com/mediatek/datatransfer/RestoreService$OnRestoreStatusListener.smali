.class public interface abstract Lcom/mediatek/datatransfer/RestoreService$OnRestoreStatusListener;
.super Ljava/lang/Object;
.source "RestoreService.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/mediatek/datatransfer/RestoreService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "OnRestoreStatusListener"
.end annotation


# virtual methods
.method public abstract onComposerChanged(II)V
.end method

.method public abstract onProgressChanged(Lcom/mediatek/datatransfer/modules/Composer;I)V
.end method

.method public abstract onRestoreEnd(ZLjava/util/ArrayList;)V
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(Z",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/mediatek/datatransfer/ResultDialog$ResultEntity;",
            ">;)V"
        }
    .end annotation
.end method

.method public abstract onRestoreErr(Ljava/io/IOException;)V
.end method
