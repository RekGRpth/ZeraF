.class Lcom/mediatek/datatransfer/PersonalDataRestoreActivity$FilePreviewTask;
.super Landroid/os/AsyncTask;
.source "PersonalDataRestoreActivity.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/mediatek/datatransfer/PersonalDataRestoreActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "FilePreviewTask"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/os/AsyncTask",
        "<",
        "Ljava/lang/Void;",
        "Ljava/lang/Void;",
        "Ljava/lang/Long;",
        ">;"
    }
.end annotation


# instance fields
.field private mModule:I

.field final synthetic this$0:Lcom/mediatek/datatransfer/PersonalDataRestoreActivity;


# direct methods
.method private constructor <init>(Lcom/mediatek/datatransfer/PersonalDataRestoreActivity;)V
    .locals 1

    iput-object p1, p0, Lcom/mediatek/datatransfer/PersonalDataRestoreActivity$FilePreviewTask;->this$0:Lcom/mediatek/datatransfer/PersonalDataRestoreActivity;

    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    const/4 v0, 0x0

    iput v0, p0, Lcom/mediatek/datatransfer/PersonalDataRestoreActivity$FilePreviewTask;->mModule:I

    return-void
.end method

.method synthetic constructor <init>(Lcom/mediatek/datatransfer/PersonalDataRestoreActivity;Lcom/mediatek/datatransfer/PersonalDataRestoreActivity$1;)V
    .locals 0
    .param p1    # Lcom/mediatek/datatransfer/PersonalDataRestoreActivity;
    .param p2    # Lcom/mediatek/datatransfer/PersonalDataRestoreActivity$1;

    invoke-direct {p0, p1}, Lcom/mediatek/datatransfer/PersonalDataRestoreActivity$FilePreviewTask;-><init>(Lcom/mediatek/datatransfer/PersonalDataRestoreActivity;)V

    return-void
.end method


# virtual methods
.method protected varargs doInBackground([Ljava/lang/Void;)Ljava/lang/Long;
    .locals 3
    .param p1    # [Ljava/lang/Void;

    iget-object v0, p0, Lcom/mediatek/datatransfer/PersonalDataRestoreActivity$FilePreviewTask;->this$0:Lcom/mediatek/datatransfer/PersonalDataRestoreActivity;

    new-instance v1, Lcom/mediatek/datatransfer/utils/BackupFilePreview;

    iget-object v2, p0, Lcom/mediatek/datatransfer/PersonalDataRestoreActivity$FilePreviewTask;->this$0:Lcom/mediatek/datatransfer/PersonalDataRestoreActivity;

    invoke-static {v2}, Lcom/mediatek/datatransfer/PersonalDataRestoreActivity;->access$800(Lcom/mediatek/datatransfer/PersonalDataRestoreActivity;)Ljava/io/File;

    move-result-object v2

    invoke-direct {v1, v2}, Lcom/mediatek/datatransfer/utils/BackupFilePreview;-><init>(Ljava/io/File;)V

    iput-object v1, v0, Lcom/mediatek/datatransfer/PersonalDataRestoreActivity;->mPreview:Lcom/mediatek/datatransfer/utils/BackupFilePreview;

    iget-object v0, p0, Lcom/mediatek/datatransfer/PersonalDataRestoreActivity$FilePreviewTask;->this$0:Lcom/mediatek/datatransfer/PersonalDataRestoreActivity;

    iget-object v0, v0, Lcom/mediatek/datatransfer/PersonalDataRestoreActivity;->mPreview:Lcom/mediatek/datatransfer/utils/BackupFilePreview;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/mediatek/datatransfer/PersonalDataRestoreActivity$FilePreviewTask;->this$0:Lcom/mediatek/datatransfer/PersonalDataRestoreActivity;

    iget-object v0, v0, Lcom/mediatek/datatransfer/PersonalDataRestoreActivity;->mPreview:Lcom/mediatek/datatransfer/utils/BackupFilePreview;

    iget-object v1, p0, Lcom/mediatek/datatransfer/PersonalDataRestoreActivity$FilePreviewTask;->this$0:Lcom/mediatek/datatransfer/PersonalDataRestoreActivity;

    invoke-virtual {v0, v1}, Lcom/mediatek/datatransfer/utils/BackupFilePreview;->getBackupModules(Landroid/content/Context;)I

    move-result v0

    iput v0, p0, Lcom/mediatek/datatransfer/PersonalDataRestoreActivity$FilePreviewTask;->mModule:I

    :cond_0
    const/4 v0, 0x0

    return-object v0
.end method

.method protected bridge synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1
    .param p1    # [Ljava/lang/Object;

    check-cast p1, [Ljava/lang/Void;

    invoke-virtual {p0, p1}, Lcom/mediatek/datatransfer/PersonalDataRestoreActivity$FilePreviewTask;->doInBackground([Ljava/lang/Void;)Ljava/lang/Long;

    move-result-object v0

    return-object v0
.end method

.method protected onPostExecute(Ljava/lang/Long;)V
    .locals 11
    .param p1    # Ljava/lang/Long;

    const/4 v8, 0x1

    invoke-super {p0, p1}, Landroid/os/AsyncTask;->onPostExecute(Ljava/lang/Object;)V

    const/4 v7, 0x6

    new-array v6, v7, [I

    fill-array-data v6, :array_0

    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    move-object v0, v6

    array-length v3, v0

    const/4 v1, 0x0

    :goto_0
    if-ge v1, v3, :cond_1

    aget v5, v0, v1

    iget v7, p0, Lcom/mediatek/datatransfer/PersonalDataRestoreActivity$FilePreviewTask;->mModule:I

    and-int/2addr v7, v5

    if-eqz v7, :cond_0

    new-instance v2, Lcom/mediatek/datatransfer/PersonalItemData;

    invoke-direct {v2, v5, v8}, Lcom/mediatek/datatransfer/PersonalItemData;-><init>(IZ)V

    invoke-virtual {v4, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_1
    iget-object v7, p0, Lcom/mediatek/datatransfer/PersonalDataRestoreActivity$FilePreviewTask;->this$0:Lcom/mediatek/datatransfer/PersonalDataRestoreActivity;

    invoke-static {v7}, Lcom/mediatek/datatransfer/PersonalDataRestoreActivity;->access$100(Lcom/mediatek/datatransfer/PersonalDataRestoreActivity;)Lcom/mediatek/datatransfer/PersonalDataRestoreActivity$PersonalDataRestoreAdapter;

    move-result-object v7

    invoke-virtual {v7, v4}, Lcom/mediatek/datatransfer/PersonalDataRestoreActivity$PersonalDataRestoreAdapter;->changeData(Ljava/util/ArrayList;)V

    iget-object v7, p0, Lcom/mediatek/datatransfer/PersonalDataRestoreActivity$FilePreviewTask;->this$0:Lcom/mediatek/datatransfer/PersonalDataRestoreActivity;

    invoke-virtual {v7}, Lcom/mediatek/datatransfer/CheckedListActivity;->syncUnCheckedItems()V

    iget-object v7, p0, Lcom/mediatek/datatransfer/PersonalDataRestoreActivity$FilePreviewTask;->this$0:Lcom/mediatek/datatransfer/PersonalDataRestoreActivity;

    invoke-virtual {v7, v8}, Lcom/mediatek/datatransfer/AbstractRestoreActivity;->setButtonsEnable(Z)V

    iget-object v7, p0, Lcom/mediatek/datatransfer/PersonalDataRestoreActivity$FilePreviewTask;->this$0:Lcom/mediatek/datatransfer/PersonalDataRestoreActivity;

    invoke-virtual {v7}, Lcom/mediatek/datatransfer/PersonalDataRestoreActivity;->notifyListItemCheckedChanged()V

    iget-object v7, p0, Lcom/mediatek/datatransfer/PersonalDataRestoreActivity$FilePreviewTask;->this$0:Lcom/mediatek/datatransfer/PersonalDataRestoreActivity;

    invoke-static {v7, v8}, Lcom/mediatek/datatransfer/PersonalDataRestoreActivity;->access$202(Lcom/mediatek/datatransfer/PersonalDataRestoreActivity;Z)Z

    iget-object v7, p0, Lcom/mediatek/datatransfer/PersonalDataRestoreActivity$FilePreviewTask;->this$0:Lcom/mediatek/datatransfer/PersonalDataRestoreActivity;

    const/4 v8, 0x0

    invoke-virtual {v7, v8}, Lcom/mediatek/datatransfer/AbstractRestoreActivity;->showLoadingContent(Z)V

    iget-object v7, p0, Lcom/mediatek/datatransfer/PersonalDataRestoreActivity$FilePreviewTask;->this$0:Lcom/mediatek/datatransfer/PersonalDataRestoreActivity;

    invoke-static {v7}, Lcom/mediatek/datatransfer/PersonalDataRestoreActivity;->access$300(Lcom/mediatek/datatransfer/PersonalDataRestoreActivity;)V

    iget-object v7, p0, Lcom/mediatek/datatransfer/PersonalDataRestoreActivity$FilePreviewTask;->this$0:Lcom/mediatek/datatransfer/PersonalDataRestoreActivity;

    invoke-static {v7}, Lcom/mediatek/datatransfer/PersonalDataRestoreActivity;->access$400(Lcom/mediatek/datatransfer/PersonalDataRestoreActivity;)Lcom/mediatek/datatransfer/PersonalDataRestoreActivity$PersonalDataRestoreStatusListener;

    move-result-object v7

    if-nez v7, :cond_2

    iget-object v7, p0, Lcom/mediatek/datatransfer/PersonalDataRestoreActivity$FilePreviewTask;->this$0:Lcom/mediatek/datatransfer/PersonalDataRestoreActivity;

    new-instance v8, Lcom/mediatek/datatransfer/PersonalDataRestoreActivity$PersonalDataRestoreStatusListener;

    iget-object v9, p0, Lcom/mediatek/datatransfer/PersonalDataRestoreActivity$FilePreviewTask;->this$0:Lcom/mediatek/datatransfer/PersonalDataRestoreActivity;

    const/4 v10, 0x0

    invoke-direct {v8, v9, v10}, Lcom/mediatek/datatransfer/PersonalDataRestoreActivity$PersonalDataRestoreStatusListener;-><init>(Lcom/mediatek/datatransfer/PersonalDataRestoreActivity;Lcom/mediatek/datatransfer/PersonalDataRestoreActivity$1;)V

    invoke-static {v7, v8}, Lcom/mediatek/datatransfer/PersonalDataRestoreActivity;->access$402(Lcom/mediatek/datatransfer/PersonalDataRestoreActivity;Lcom/mediatek/datatransfer/PersonalDataRestoreActivity$PersonalDataRestoreStatusListener;)Lcom/mediatek/datatransfer/PersonalDataRestoreActivity$PersonalDataRestoreStatusListener;

    :cond_2
    iget-object v7, p0, Lcom/mediatek/datatransfer/PersonalDataRestoreActivity$FilePreviewTask;->this$0:Lcom/mediatek/datatransfer/PersonalDataRestoreActivity;

    iget-object v8, p0, Lcom/mediatek/datatransfer/PersonalDataRestoreActivity$FilePreviewTask;->this$0:Lcom/mediatek/datatransfer/PersonalDataRestoreActivity;

    invoke-static {v8}, Lcom/mediatek/datatransfer/PersonalDataRestoreActivity;->access$400(Lcom/mediatek/datatransfer/PersonalDataRestoreActivity;)Lcom/mediatek/datatransfer/PersonalDataRestoreActivity$PersonalDataRestoreStatusListener;

    move-result-object v8

    invoke-virtual {v7, v8}, Lcom/mediatek/datatransfer/AbstractRestoreActivity;->setOnRestoreStatusListener(Lcom/mediatek/datatransfer/RestoreService$OnRestoreStatusListener;)V

    iget-object v7, p0, Lcom/mediatek/datatransfer/PersonalDataRestoreActivity$FilePreviewTask;->this$0:Lcom/mediatek/datatransfer/PersonalDataRestoreActivity;

    invoke-static {v7}, Lcom/mediatek/datatransfer/PersonalDataRestoreActivity;->access$600(Lcom/mediatek/datatransfer/PersonalDataRestoreActivity;)Ljava/lang/String;

    move-result-object v7

    const-string v8, "mIsDataInitialed is ok"

    invoke-static {v7, v8}, Lcom/mediatek/datatransfer/utils/MyLogger;->logD(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v7, p0, Lcom/mediatek/datatransfer/PersonalDataRestoreActivity$FilePreviewTask;->this$0:Lcom/mediatek/datatransfer/PersonalDataRestoreActivity;

    invoke-static {v7}, Lcom/mediatek/datatransfer/PersonalDataRestoreActivity;->access$700(Lcom/mediatek/datatransfer/PersonalDataRestoreActivity;)V

    return-void

    nop

    :array_0
    .array-data 4
        0x1
        0x40
        0x20
        0x8
        0x80
        0x200
    .end array-data
.end method

.method protected bridge synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 0
    .param p1    # Ljava/lang/Object;

    check-cast p1, Ljava/lang/Long;

    invoke-virtual {p0, p1}, Lcom/mediatek/datatransfer/PersonalDataRestoreActivity$FilePreviewTask;->onPostExecute(Ljava/lang/Long;)V

    return-void
.end method

.method protected onPreExecute()V
    .locals 2

    invoke-super {p0}, Landroid/os/AsyncTask;->onPreExecute()V

    iget-object v0, p0, Lcom/mediatek/datatransfer/PersonalDataRestoreActivity$FilePreviewTask;->this$0:Lcom/mediatek/datatransfer/PersonalDataRestoreActivity;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/mediatek/datatransfer/AbstractRestoreActivity;->setButtonsEnable(Z)V

    iget-object v0, p0, Lcom/mediatek/datatransfer/PersonalDataRestoreActivity$FilePreviewTask;->this$0:Lcom/mediatek/datatransfer/PersonalDataRestoreActivity;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/mediatek/datatransfer/AbstractRestoreActivity;->showLoadingContent(Z)V

    iget-object v0, p0, Lcom/mediatek/datatransfer/PersonalDataRestoreActivity$FilePreviewTask;->this$0:Lcom/mediatek/datatransfer/PersonalDataRestoreActivity;

    invoke-static {v0}, Lcom/mediatek/datatransfer/PersonalDataRestoreActivity;->access$300(Lcom/mediatek/datatransfer/PersonalDataRestoreActivity;)V

    return-void
.end method
