.class Lcom/mediatek/datatransfer/PersonalDataBackupActivity$10;
.super Ljava/lang/Object;
.source "PersonalDataBackupActivity.java"

# interfaces
.implements Landroid/content/DialogInterface$OnMultiChoiceClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/mediatek/datatransfer/PersonalDataBackupActivity;->showContactConfigDialog()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/mediatek/datatransfer/PersonalDataBackupActivity;

.field final synthetic val$temp:[Z


# direct methods
.method constructor <init>(Lcom/mediatek/datatransfer/PersonalDataBackupActivity;[Z)V
    .locals 0

    iput-object p1, p0, Lcom/mediatek/datatransfer/PersonalDataBackupActivity$10;->this$0:Lcom/mediatek/datatransfer/PersonalDataBackupActivity;

    iput-object p2, p0, Lcom/mediatek/datatransfer/PersonalDataBackupActivity$10;->val$temp:[Z

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/content/DialogInterface;IZ)V
    .locals 7
    .param p1    # Landroid/content/DialogInterface;
    .param p2    # I
    .param p3    # Z

    const/4 v6, 0x0

    const/4 v5, -0x1

    invoke-static {}, Lcom/mediatek/datatransfer/PersonalDataBackupActivity;->access$200()Ljava/lang/String;

    move-result-object v2

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "DialogID.DLG_CONTACT_CONFIG: the number "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " is checked("

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ")"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/mediatek/datatransfer/utils/MyLogger;->logD(Ljava/lang/String;Ljava/lang/String;)V

    move-object v1, p1

    check-cast v1, Landroid/app/AlertDialog;

    iget-object v2, p0, Lcom/mediatek/datatransfer/PersonalDataBackupActivity$10;->this$0:Lcom/mediatek/datatransfer/PersonalDataBackupActivity;

    iget v2, v2, Lcom/mediatek/datatransfer/PersonalDataBackupActivity;->mSimCount:I

    add-int/lit8 v0, v2, 0x1

    iget-object v2, p0, Lcom/mediatek/datatransfer/PersonalDataBackupActivity$10;->this$0:Lcom/mediatek/datatransfer/PersonalDataBackupActivity;

    iget-object v3, p0, Lcom/mediatek/datatransfer/PersonalDataBackupActivity$10;->val$temp:[Z

    invoke-static {v2, v3, v0, v6}, Lcom/mediatek/datatransfer/PersonalDataBackupActivity;->access$600(Lcom/mediatek/datatransfer/PersonalDataBackupActivity;[ZIZ)Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-virtual {v1, v5}, Landroid/app/AlertDialog;->getButton(I)Landroid/widget/Button;

    move-result-object v2

    invoke-virtual {v2, v6}, Landroid/widget/TextView;->setEnabled(Z)V

    :goto_0
    return-void

    :cond_0
    invoke-virtual {v1, v5}, Landroid/app/AlertDialog;->getButton(I)Landroid/widget/Button;

    move-result-object v2

    const/4 v3, 0x1

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setEnabled(Z)V

    goto :goto_0
.end method
