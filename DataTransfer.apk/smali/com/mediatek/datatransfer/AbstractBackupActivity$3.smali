.class Lcom/mediatek/datatransfer/AbstractBackupActivity$3;
.super Ljava/lang/Object;
.source "AbstractBackupActivity.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/mediatek/datatransfer/AbstractBackupActivity;->initButton()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/mediatek/datatransfer/AbstractBackupActivity;


# direct methods
.method constructor <init>(Lcom/mediatek/datatransfer/AbstractBackupActivity;)V
    .locals 0

    iput-object p1, p0, Lcom/mediatek/datatransfer/AbstractBackupActivity$3;->this$0:Lcom/mediatek/datatransfer/AbstractBackupActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 3
    .param p1    # Landroid/view/View;

    iget-object v1, p0, Lcom/mediatek/datatransfer/AbstractBackupActivity$3;->this$0:Lcom/mediatek/datatransfer/AbstractBackupActivity;

    iget-object v1, v1, Lcom/mediatek/datatransfer/AbstractBackupActivity;->mBackupService:Lcom/mediatek/datatransfer/BackupService$BackupBinder;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/mediatek/datatransfer/AbstractBackupActivity$3;->this$0:Lcom/mediatek/datatransfer/AbstractBackupActivity;

    iget-object v1, v1, Lcom/mediatek/datatransfer/AbstractBackupActivity;->mBackupService:Lcom/mediatek/datatransfer/BackupService$BackupBinder;

    invoke-virtual {v1}, Lcom/mediatek/datatransfer/BackupService$BackupBinder;->getState()I

    move-result v1

    if-eqz v1, :cond_1

    :cond_0
    iget-object v1, p0, Lcom/mediatek/datatransfer/AbstractBackupActivity$3;->this$0:Lcom/mediatek/datatransfer/AbstractBackupActivity;

    invoke-static {v1}, Lcom/mediatek/datatransfer/AbstractBackupActivity;->access$000(Lcom/mediatek/datatransfer/AbstractBackupActivity;)Ljava/lang/String;

    move-result-object v1

    const-string v2, "Can not to start. BackupService not ready or BackupService is ruuning"

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    :goto_0
    return-void

    :cond_1
    iget-object v1, p0, Lcom/mediatek/datatransfer/AbstractBackupActivity$3;->this$0:Lcom/mediatek/datatransfer/AbstractBackupActivity;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Lcom/mediatek/datatransfer/CheckedListActivity;->isAllChecked(Z)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/mediatek/datatransfer/AbstractBackupActivity$3;->this$0:Lcom/mediatek/datatransfer/AbstractBackupActivity;

    invoke-static {v1}, Lcom/mediatek/datatransfer/AbstractBackupActivity;->access$000(Lcom/mediatek/datatransfer/AbstractBackupActivity;)Ljava/lang/String;

    move-result-object v1

    const-string v2, "to Backup List is null or empty"

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    :cond_2
    invoke-static {}, Lcom/mediatek/datatransfer/utils/SDCardUtils;->getStoragePath()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_3

    iget-object v1, p0, Lcom/mediatek/datatransfer/AbstractBackupActivity$3;->this$0:Lcom/mediatek/datatransfer/AbstractBackupActivity;

    invoke-virtual {v1}, Lcom/mediatek/datatransfer/AbstractBackupActivity;->startBackup()V

    goto :goto_0

    :cond_3
    iget-object v1, p0, Lcom/mediatek/datatransfer/AbstractBackupActivity$3;->this$0:Lcom/mediatek/datatransfer/AbstractBackupActivity;

    const/16 v2, 0x7d7

    invoke-virtual {v1, v2}, Landroid/app/Activity;->showDialog(I)V

    goto :goto_0
.end method
