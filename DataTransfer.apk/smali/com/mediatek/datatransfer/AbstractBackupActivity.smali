.class public abstract Lcom/mediatek/datatransfer/AbstractBackupActivity;
.super Lcom/mediatek/datatransfer/CheckedListActivity;
.source "AbstractBackupActivity.java"

# interfaces
.implements Lcom/mediatek/datatransfer/CheckedListActivity$OnCheckedCountChangedListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/mediatek/datatransfer/AbstractBackupActivity$NomalBackupStatusListener;
    }
.end annotation


# instance fields
.field private CLASS_TAG:Ljava/lang/String;

.field loadingContent:Landroid/widget/LinearLayout;

.field protected mAdapter:Landroid/widget/BaseAdapter;

.field private mBackupListener:Lcom/mediatek/datatransfer/BackupService$OnBackupStatusListener;

.field protected mBackupService:Lcom/mediatek/datatransfer/BackupService$BackupBinder;

.field private mButtonBackup:Landroid/widget/Button;

.field protected mCancelDlg:Landroid/app/ProgressDialog;

.field private mCheckBoxSelect:Landroid/widget/CheckBox;

.field private mDivider:Landroid/view/View;

.field protected mHandler:Landroid/os/Handler;

.field protected mProgressDialog:Landroid/app/ProgressDialog;

.field private mServiceCon:Landroid/content/ServiceConnection;


# direct methods
.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Lcom/mediatek/datatransfer/CheckedListActivity;-><init>()V

    const-string v0, "DataTransfer/AbstractBackupActivity"

    iput-object v0, p0, Lcom/mediatek/datatransfer/AbstractBackupActivity;->CLASS_TAG:Ljava/lang/String;

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/mediatek/datatransfer/AbstractBackupActivity;->loadingContent:Landroid/widget/LinearLayout;

    new-instance v0, Lcom/mediatek/datatransfer/AbstractBackupActivity$10;

    invoke-direct {v0, p0}, Lcom/mediatek/datatransfer/AbstractBackupActivity$10;-><init>(Lcom/mediatek/datatransfer/AbstractBackupActivity;)V

    iput-object v0, p0, Lcom/mediatek/datatransfer/AbstractBackupActivity;->mServiceCon:Landroid/content/ServiceConnection;

    return-void
.end method

.method static synthetic access$000(Lcom/mediatek/datatransfer/AbstractBackupActivity;)Ljava/lang/String;
    .locals 1
    .param p0    # Lcom/mediatek/datatransfer/AbstractBackupActivity;

    iget-object v0, p0, Lcom/mediatek/datatransfer/AbstractBackupActivity;->CLASS_TAG:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$100(Lcom/mediatek/datatransfer/AbstractBackupActivity;)Landroid/app/ProgressDialog;
    .locals 1
    .param p0    # Lcom/mediatek/datatransfer/AbstractBackupActivity;

    invoke-direct {p0}, Lcom/mediatek/datatransfer/AbstractBackupActivity;->createCancelDlg()Landroid/app/ProgressDialog;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$200(Lcom/mediatek/datatransfer/AbstractBackupActivity;)Lcom/mediatek/datatransfer/BackupService$OnBackupStatusListener;
    .locals 1
    .param p0    # Lcom/mediatek/datatransfer/AbstractBackupActivity;

    iget-object v0, p0, Lcom/mediatek/datatransfer/AbstractBackupActivity;->mBackupListener:Lcom/mediatek/datatransfer/BackupService$OnBackupStatusListener;

    return-object v0
.end method

.method private bindService()V
    .locals 4

    invoke-virtual {p0}, Landroid/content/ContextWrapper;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    new-instance v1, Landroid/content/Intent;

    const-class v2, Lcom/mediatek/datatransfer/BackupService;

    invoke-direct {v1, p0, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    iget-object v2, p0, Lcom/mediatek/datatransfer/AbstractBackupActivity;->mServiceCon:Landroid/content/ServiceConnection;

    const/4 v3, 0x1

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/Context;->bindService(Landroid/content/Intent;Landroid/content/ServiceConnection;I)Z

    return-void
.end method

.method private createCancelDlg()Landroid/app/ProgressDialog;
    .locals 2

    iget-object v0, p0, Lcom/mediatek/datatransfer/AbstractBackupActivity;->mCancelDlg:Landroid/app/ProgressDialog;

    if-nez v0, :cond_0

    new-instance v0, Landroid/app/ProgressDialog;

    invoke-direct {v0, p0}, Landroid/app/ProgressDialog;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/mediatek/datatransfer/AbstractBackupActivity;->mCancelDlg:Landroid/app/ProgressDialog;

    iget-object v0, p0, Lcom/mediatek/datatransfer/AbstractBackupActivity;->mCancelDlg:Landroid/app/ProgressDialog;

    const v1, 0x7f060012

    invoke-virtual {p0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/app/ProgressDialog;->setMessage(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lcom/mediatek/datatransfer/AbstractBackupActivity;->mCancelDlg:Landroid/app/ProgressDialog;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/app/Dialog;->setCancelable(Z)V

    :cond_0
    iget-object v0, p0, Lcom/mediatek/datatransfer/AbstractBackupActivity;->mCancelDlg:Landroid/app/ProgressDialog;

    return-object v0
.end method

.method private createProgressDlg()Landroid/app/ProgressDialog;
    .locals 3

    const/4 v2, 0x1

    iget-object v0, p0, Lcom/mediatek/datatransfer/AbstractBackupActivity;->mProgressDialog:Landroid/app/ProgressDialog;

    if-nez v0, :cond_0

    new-instance v0, Landroid/app/ProgressDialog;

    invoke-direct {v0, p0}, Landroid/app/ProgressDialog;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/mediatek/datatransfer/AbstractBackupActivity;->mProgressDialog:Landroid/app/ProgressDialog;

    iget-object v0, p0, Lcom/mediatek/datatransfer/AbstractBackupActivity;->mProgressDialog:Landroid/app/ProgressDialog;

    invoke-virtual {v0, v2}, Landroid/app/ProgressDialog;->setProgressStyle(I)V

    iget-object v0, p0, Lcom/mediatek/datatransfer/AbstractBackupActivity;->mProgressDialog:Landroid/app/ProgressDialog;

    const v1, 0x7f06000e

    invoke-virtual {p0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/app/ProgressDialog;->setMessage(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lcom/mediatek/datatransfer/AbstractBackupActivity;->mProgressDialog:Landroid/app/ProgressDialog;

    invoke-virtual {v0, v2}, Landroid/app/Dialog;->setCancelable(Z)V

    iget-object v0, p0, Lcom/mediatek/datatransfer/AbstractBackupActivity;->mProgressDialog:Landroid/app/ProgressDialog;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/app/Dialog;->setCanceledOnTouchOutside(Z)V

    iget-object v0, p0, Lcom/mediatek/datatransfer/AbstractBackupActivity;->mProgressDialog:Landroid/app/ProgressDialog;

    iget-object v1, p0, Lcom/mediatek/datatransfer/AbstractBackupActivity;->mHandler:Landroid/os/Handler;

    const/16 v2, 0x501

    invoke-virtual {v1, v2}, Landroid/os/Handler;->obtainMessage(I)Landroid/os/Message;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/app/Dialog;->setCancelMessage(Landroid/os/Message;)V

    :cond_0
    iget-object v0, p0, Lcom/mediatek/datatransfer/AbstractBackupActivity;->mProgressDialog:Landroid/app/ProgressDialog;

    return-object v0
.end method

.method private init()V
    .locals 0

    invoke-direct {p0}, Lcom/mediatek/datatransfer/AbstractBackupActivity;->bindService()V

    invoke-virtual {p0, p0}, Lcom/mediatek/datatransfer/CheckedListActivity;->registerOnCheckedCountChangedListener(Lcom/mediatek/datatransfer/CheckedListActivity$OnCheckedCountChangedListener;)V

    invoke-direct {p0}, Lcom/mediatek/datatransfer/AbstractBackupActivity;->initButton()V

    invoke-virtual {p0}, Lcom/mediatek/datatransfer/AbstractBackupActivity;->initHandler()V

    invoke-direct {p0}, Lcom/mediatek/datatransfer/AbstractBackupActivity;->initLoadingView()V

    invoke-direct {p0}, Lcom/mediatek/datatransfer/AbstractBackupActivity;->createProgressDlg()Landroid/app/ProgressDialog;

    return-void
.end method

.method private initButton()V
    .locals 2

    const v0, 0x7f080006

    invoke-virtual {p0, v0}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/mediatek/datatransfer/AbstractBackupActivity;->mDivider:Landroid/view/View;

    iget-object v0, p0, Lcom/mediatek/datatransfer/AbstractBackupActivity;->mDivider:Landroid/view/View;

    invoke-virtual {p0}, Landroid/app/ListActivity;->getListView()Landroid/widget/ListView;

    move-result-object v1

    invoke-virtual {v1}, Landroid/widget/ListView;->getDivider()Landroid/graphics/drawable/Drawable;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/view/View;->setBackground(Landroid/graphics/drawable/Drawable;)V

    const v0, 0x7f080007

    invoke-virtual {p0, v0}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lcom/mediatek/datatransfer/AbstractBackupActivity;->mButtonBackup:Landroid/widget/Button;

    iget-object v0, p0, Lcom/mediatek/datatransfer/AbstractBackupActivity;->mButtonBackup:Landroid/widget/Button;

    new-instance v1, Lcom/mediatek/datatransfer/AbstractBackupActivity$3;

    invoke-direct {v1, p0}, Lcom/mediatek/datatransfer/AbstractBackupActivity$3;-><init>(Lcom/mediatek/datatransfer/AbstractBackupActivity;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    const v0, 0x7f080005

    invoke-virtual {p0, v0}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/CheckBox;

    iput-object v0, p0, Lcom/mediatek/datatransfer/AbstractBackupActivity;->mCheckBoxSelect:Landroid/widget/CheckBox;

    iget-object v0, p0, Lcom/mediatek/datatransfer/AbstractBackupActivity;->mCheckBoxSelect:Landroid/widget/CheckBox;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/widget/CompoundButton;->setChecked(Z)V

    iget-object v0, p0, Lcom/mediatek/datatransfer/AbstractBackupActivity;->mCheckBoxSelect:Landroid/widget/CheckBox;

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    iget-object v0, p0, Lcom/mediatek/datatransfer/AbstractBackupActivity;->mCheckBoxSelect:Landroid/widget/CheckBox;

    new-instance v1, Lcom/mediatek/datatransfer/AbstractBackupActivity$4;

    invoke-direct {v1, p0}, Lcom/mediatek/datatransfer/AbstractBackupActivity$4;-><init>(Lcom/mediatek/datatransfer/AbstractBackupActivity;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    return-void
.end method

.method private initLoadingView()V
    .locals 1

    const v0, 0x7f080003

    invoke-virtual {p0, v0}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/mediatek/datatransfer/AbstractBackupActivity;->loadingContent:Landroid/widget/LinearLayout;

    return-void
.end method

.method private unBindService()V
    .locals 2

    iget-object v0, p0, Lcom/mediatek/datatransfer/AbstractBackupActivity;->mBackupService:Lcom/mediatek/datatransfer/BackupService$BackupBinder;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/mediatek/datatransfer/AbstractBackupActivity;->mBackupService:Lcom/mediatek/datatransfer/BackupService$BackupBinder;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/mediatek/datatransfer/BackupService$BackupBinder;->setOnBackupChangedListner(Lcom/mediatek/datatransfer/BackupService$OnBackupStatusListener;)V

    :cond_0
    invoke-virtual {p0}, Landroid/content/ContextWrapper;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    iget-object v1, p0, Lcom/mediatek/datatransfer/AbstractBackupActivity;->mServiceCon:Landroid/content/ServiceConnection;

    invoke-virtual {v0, v1}, Landroid/content/Context;->unbindService(Landroid/content/ServiceConnection;)V

    return-void
.end method


# virtual methods
.method protected abstract afterServiceConnected()V
.end method

.method protected checkBackupState()V
    .locals 2

    iget-object v1, p0, Lcom/mediatek/datatransfer/AbstractBackupActivity;->mBackupService:Lcom/mediatek/datatransfer/BackupService$BackupBinder;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/mediatek/datatransfer/AbstractBackupActivity;->mBackupService:Lcom/mediatek/datatransfer/BackupService$BackupBinder;

    invoke-virtual {v1}, Lcom/mediatek/datatransfer/BackupService$BackupBinder;->getState()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    :cond_0
    :goto_0
    return-void

    :pswitch_0
    invoke-virtual {p0}, Lcom/mediatek/datatransfer/AbstractBackupActivity;->errChecked()Z

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x6
        :pswitch_0
    .end packed-switch
.end method

.method protected errChecked()Z
    .locals 6

    const/4 v1, 0x0

    invoke-static {}, Lcom/mediatek/datatransfer/utils/SDCardUtils;->getStoragePath()Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_1

    iget-object v2, p0, Lcom/mediatek/datatransfer/AbstractBackupActivity;->CLASS_TAG:Ljava/lang/String;

    const-string v3, "SDCard is removed"

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v1, 0x1

    iget-object v2, p0, Lcom/mediatek/datatransfer/AbstractBackupActivity;->mHandler:Landroid/os/Handler;

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/mediatek/datatransfer/AbstractBackupActivity;->mHandler:Landroid/os/Handler;

    new-instance v3, Lcom/mediatek/datatransfer/AbstractBackupActivity$1;

    invoke-direct {v3, p0}, Lcom/mediatek/datatransfer/AbstractBackupActivity$1;-><init>(Lcom/mediatek/datatransfer/AbstractBackupActivity;)V

    invoke-virtual {v2, v3}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    :cond_0
    :goto_0
    return v1

    :cond_1
    invoke-static {v0}, Lcom/mediatek/datatransfer/utils/SDCardUtils;->getAvailableSize(Ljava/lang/String;)J

    move-result-wide v2

    const-wide/16 v4, 0x200

    cmp-long v2, v2, v4

    if-gtz v2, :cond_2

    iget-object v2, p0, Lcom/mediatek/datatransfer/AbstractBackupActivity;->CLASS_TAG:Ljava/lang/String;

    const-string v3, "SDCard is full"

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v1, 0x1

    iget-object v2, p0, Lcom/mediatek/datatransfer/AbstractBackupActivity;->mHandler:Landroid/os/Handler;

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/mediatek/datatransfer/AbstractBackupActivity;->mHandler:Landroid/os/Handler;

    new-instance v3, Lcom/mediatek/datatransfer/AbstractBackupActivity$2;

    invoke-direct {v3, p0}, Lcom/mediatek/datatransfer/AbstractBackupActivity$2;-><init>(Lcom/mediatek/datatransfer/AbstractBackupActivity;)V

    invoke-virtual {v2, v3}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    goto :goto_0

    :cond_2
    iget-object v2, p0, Lcom/mediatek/datatransfer/AbstractBackupActivity;->CLASS_TAG:Ljava/lang/String;

    const-string v3, "unkown error"

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method public abstract initBackupAdapter()Landroid/widget/BaseAdapter;
.end method

.method protected final initHandler()V
    .locals 1

    new-instance v0, Lcom/mediatek/datatransfer/AbstractBackupActivity$5;

    invoke-direct {v0, p0}, Lcom/mediatek/datatransfer/AbstractBackupActivity$5;-><init>(Lcom/mediatek/datatransfer/AbstractBackupActivity;)V

    iput-object v0, p0, Lcom/mediatek/datatransfer/AbstractBackupActivity;->mHandler:Landroid/os/Handler;

    return-void
.end method

.method public onCheckedCountChanged()V
    .locals 1

    iget-object v0, p0, Lcom/mediatek/datatransfer/AbstractBackupActivity;->mAdapter:Landroid/widget/BaseAdapter;

    invoke-virtual {v0}, Landroid/widget/BaseAdapter;->notifyDataSetChanged()V

    invoke-virtual {p0}, Lcom/mediatek/datatransfer/AbstractBackupActivity;->updateButtonState()V

    return-void
.end method

.method public onConfigurationChanged(Landroid/content/res/Configuration;)V
    .locals 2
    .param p1    # Landroid/content/res/Configuration;

    invoke-super {p0, p1}, Landroid/app/Activity;->onConfigurationChanged(Landroid/content/res/Configuration;)V

    iget-object v0, p0, Lcom/mediatek/datatransfer/AbstractBackupActivity;->CLASS_TAG:Ljava/lang/String;

    const-string v1, "onConfigurationChanged"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    return-void
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 3
    .param p1    # Landroid/os/Bundle;

    invoke-super {p0, p1}, Lcom/mediatek/datatransfer/CheckedListActivity;->onCreate(Landroid/os/Bundle;)V

    const v1, 0x7f030001

    invoke-virtual {p0, v1}, Landroid/app/Activity;->setContentView(I)V

    invoke-virtual {p0}, Landroid/app/Activity;->getActionBar()Landroid/app/ActionBar;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/app/ActionBar;->setDisplayHomeAsUpEnabled(Z)V

    invoke-direct {p0}, Lcom/mediatek/datatransfer/AbstractBackupActivity;->init()V

    iget-object v1, p0, Lcom/mediatek/datatransfer/AbstractBackupActivity;->CLASS_TAG:Ljava/lang/String;

    const-string v2, "onCreate"

    invoke-static {v1, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    if-eqz p1, :cond_0

    invoke-virtual {p0}, Lcom/mediatek/datatransfer/AbstractBackupActivity;->updateButtonState()V

    :cond_0
    return-void
.end method

.method protected onCreateDialog(ILandroid/os/Bundle;)Landroid/app/Dialog;
    .locals 7
    .param p1    # I
    .param p2    # Landroid/os/Bundle;

    const v4, 0x7f060010

    const v5, 0x104000a

    const/4 v6, 0x0

    const/4 v0, 0x0

    sparse-switch p1, :sswitch_data_0

    :goto_0
    return-object v0

    :sswitch_0
    new-instance v3, Landroid/app/AlertDialog$Builder;

    invoke-direct {v3, p0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    invoke-virtual {v3, v4}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    move-result-object v3

    const v4, 0x7f060011

    invoke-virtual {v3, v4}, Landroid/app/AlertDialog$Builder;->setMessage(I)Landroid/app/AlertDialog$Builder;

    move-result-object v3

    new-instance v4, Lcom/mediatek/datatransfer/AbstractBackupActivity$7;

    invoke-direct {v4, p0}, Lcom/mediatek/datatransfer/AbstractBackupActivity$7;-><init>(Lcom/mediatek/datatransfer/AbstractBackupActivity;)V

    invoke-virtual {v3, v5, v4}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v3

    const/high16 v4, 0x1040000

    new-instance v5, Lcom/mediatek/datatransfer/AbstractBackupActivity$6;

    invoke-direct {v5, p0}, Lcom/mediatek/datatransfer/AbstractBackupActivity$6;-><init>(Lcom/mediatek/datatransfer/AbstractBackupActivity;)V

    invoke-virtual {v3, v4, v5}, Landroid/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v3

    invoke-virtual {v3, v6}, Landroid/app/AlertDialog$Builder;->setCancelable(Z)Landroid/app/AlertDialog$Builder;

    move-result-object v3

    invoke-virtual {v3}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v0

    goto :goto_0

    :sswitch_1
    new-instance v3, Landroid/app/AlertDialog$Builder;

    invoke-direct {v3, p0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    invoke-virtual {v3, v4}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    move-result-object v3

    const v4, 0x7f06001e

    invoke-virtual {v3, v4}, Landroid/app/AlertDialog$Builder;->setMessage(I)Landroid/app/AlertDialog$Builder;

    move-result-object v3

    new-instance v4, Lcom/mediatek/datatransfer/AbstractBackupActivity$8;

    invoke-direct {v4, p0}, Lcom/mediatek/datatransfer/AbstractBackupActivity$8;-><init>(Lcom/mediatek/datatransfer/AbstractBackupActivity;)V

    invoke-virtual {v3, v5, v4}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v3

    invoke-virtual {v3, v6}, Landroid/app/AlertDialog$Builder;->setCancelable(Z)Landroid/app/AlertDialog$Builder;

    move-result-object v3

    invoke-virtual {v3}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v0

    goto :goto_0

    :sswitch_2
    new-instance v3, Landroid/app/AlertDialog$Builder;

    invoke-direct {v3, p0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    invoke-virtual {v3, v4}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    move-result-object v3

    const v4, 0x7f06001f

    invoke-virtual {v3, v4}, Landroid/app/AlertDialog$Builder;->setMessage(I)Landroid/app/AlertDialog$Builder;

    move-result-object v3

    new-instance v4, Lcom/mediatek/datatransfer/AbstractBackupActivity$9;

    invoke-direct {v4, p0}, Lcom/mediatek/datatransfer/AbstractBackupActivity$9;-><init>(Lcom/mediatek/datatransfer/AbstractBackupActivity;)V

    invoke-virtual {v3, v5, v4}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v3

    invoke-virtual {v3, v6}, Landroid/app/AlertDialog$Builder;->setCancelable(Z)Landroid/app/AlertDialog$Builder;

    move-result-object v3

    invoke-virtual {v3}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v0

    goto :goto_0

    :sswitch_3
    const-string v3, "name"

    invoke-virtual {p2, v3}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    const v3, 0x7f060036

    invoke-virtual {p0, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/Object;

    aput-object v2, v4, v6

    invoke-static {v3, v4}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    new-instance v3, Landroid/app/AlertDialog$Builder;

    invoke-direct {v3, p0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    const v4, 0x1010355

    invoke-virtual {v3, v4}, Landroid/app/AlertDialog$Builder;->setIconAttribute(I)Landroid/app/AlertDialog$Builder;

    move-result-object v3

    const v4, 0x7f06001a

    invoke-virtual {v3, v4}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    move-result-object v3

    invoke-virtual {v3, v1}, Landroid/app/AlertDialog$Builder;->setMessage(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    move-result-object v3

    const/4 v4, 0x0

    invoke-virtual {v3, v5, v4}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v3

    invoke-virtual {v3}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v0

    goto/16 :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x7d1 -> :sswitch_1
        0x7d2 -> :sswitch_2
        0x7d8 -> :sswitch_0
        0x7db -> :sswitch_3
    .end sparse-switch
.end method

.method protected onDestroy()V
    .locals 3

    const/4 v2, 0x0

    invoke-super {p0}, Lcom/mediatek/datatransfer/CheckedListActivity;->onDestroy()V

    iget-object v0, p0, Lcom/mediatek/datatransfer/AbstractBackupActivity;->CLASS_TAG:Ljava/lang/String;

    const-string v1, "onDestroy"

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v0, p0, Lcom/mediatek/datatransfer/AbstractBackupActivity;->mProgressDialog:Landroid/app/ProgressDialog;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/mediatek/datatransfer/AbstractBackupActivity;->mProgressDialog:Landroid/app/ProgressDialog;

    invoke-virtual {v0}, Landroid/app/Dialog;->isShowing()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/mediatek/datatransfer/AbstractBackupActivity;->mProgressDialog:Landroid/app/ProgressDialog;

    invoke-virtual {v0}, Landroid/app/Dialog;->dismiss()V

    :cond_0
    iget-object v0, p0, Lcom/mediatek/datatransfer/AbstractBackupActivity;->mBackupService:Lcom/mediatek/datatransfer/BackupService$BackupBinder;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/mediatek/datatransfer/AbstractBackupActivity;->mBackupService:Lcom/mediatek/datatransfer/BackupService$BackupBinder;

    invoke-virtual {v0}, Lcom/mediatek/datatransfer/BackupService$BackupBinder;->getState()I

    move-result v0

    if-nez v0, :cond_1

    invoke-virtual {p0}, Lcom/mediatek/datatransfer/AbstractBackupActivity;->stopService()V

    :cond_1
    iget-object v0, p0, Lcom/mediatek/datatransfer/AbstractBackupActivity;->mBackupService:Lcom/mediatek/datatransfer/BackupService$BackupBinder;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/mediatek/datatransfer/AbstractBackupActivity;->mBackupService:Lcom/mediatek/datatransfer/BackupService$BackupBinder;

    invoke-virtual {v0, v2}, Lcom/mediatek/datatransfer/BackupService$BackupBinder;->setOnBackupChangedListner(Lcom/mediatek/datatransfer/BackupService$OnBackupStatusListener;)V

    :cond_2
    invoke-direct {p0}, Lcom/mediatek/datatransfer/AbstractBackupActivity;->unBindService()V

    iput-object v2, p0, Lcom/mediatek/datatransfer/AbstractBackupActivity;->mHandler:Landroid/os/Handler;

    return-void
.end method

.method public onKeyDown(ILandroid/view/KeyEvent;)Z
    .locals 1
    .param p1    # I
    .param p2    # Landroid/view/KeyEvent;

    const/16 v0, 0x52

    if-ne p1, v0, :cond_0

    invoke-virtual {p2}, Landroid/view/KeyEvent;->isLongPress()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    invoke-super {p0, p1, p2}, Landroid/app/Activity;->onKeyDown(ILandroid/view/KeyEvent;)Z

    move-result v0

    goto :goto_0
.end method

.method public onOptionsItemSelected(Landroid/view/MenuItem;)Z
    .locals 3
    .param p1    # Landroid/view/MenuItem;

    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v1

    packed-switch v1, :pswitch_data_0

    invoke-super {p0, p1}, Landroid/app/Activity;->onOptionsItemSelected(Landroid/view/MenuItem;)Z

    move-result v1

    :goto_0
    return v1

    :pswitch_0
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/mediatek/datatransfer/MainActivity;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const-string v1, "action_where"

    const-string v2, "action_backup"

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const/high16 v1, 0x4000000

    invoke-virtual {v0, v1}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    invoke-virtual {p0, v0}, Landroid/app/Activity;->startActivity(Landroid/content/Intent;)V

    const/4 v1, 0x1

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x102002c
        :pswitch_0
    .end packed-switch
.end method

.method protected onResume()V
    .locals 2

    invoke-super {p0}, Lcom/mediatek/datatransfer/CheckedListActivity;->onResume()V

    iget-object v0, p0, Lcom/mediatek/datatransfer/AbstractBackupActivity;->CLASS_TAG:Ljava/lang/String;

    const-string v1, "onResume"

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    invoke-virtual {p0}, Lcom/mediatek/datatransfer/AbstractBackupActivity;->initBackupAdapter()Landroid/widget/BaseAdapter;

    move-result-object v0

    iput-object v0, p0, Lcom/mediatek/datatransfer/AbstractBackupActivity;->mAdapter:Landroid/widget/BaseAdapter;

    iget-object v0, p0, Lcom/mediatek/datatransfer/AbstractBackupActivity;->mAdapter:Landroid/widget/BaseAdapter;

    invoke-virtual {p0, v0}, Landroid/app/ListActivity;->setListAdapter(Landroid/widget/ListAdapter;)V

    return-void
.end method

.method protected setButtonsEnable(Z)V
    .locals 3
    .param p1    # Z

    iget-object v0, p0, Lcom/mediatek/datatransfer/AbstractBackupActivity;->CLASS_TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "setButtonsEnable - "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/mediatek/datatransfer/utils/MyLogger;->logD(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/mediatek/datatransfer/AbstractBackupActivity;->mButtonBackup:Landroid/widget/Button;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/mediatek/datatransfer/AbstractBackupActivity;->mButtonBackup:Landroid/widget/Button;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setEnabled(Z)V

    :cond_0
    iget-object v0, p0, Lcom/mediatek/datatransfer/AbstractBackupActivity;->mCheckBoxSelect:Landroid/widget/CheckBox;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/mediatek/datatransfer/AbstractBackupActivity;->mCheckBoxSelect:Landroid/widget/CheckBox;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setEnabled(Z)V

    :cond_1
    return-void
.end method

.method public setOnBackupStatusListener(Lcom/mediatek/datatransfer/BackupService$OnBackupStatusListener;)V
    .locals 2
    .param p1    # Lcom/mediatek/datatransfer/BackupService$OnBackupStatusListener;

    iput-object p1, p0, Lcom/mediatek/datatransfer/AbstractBackupActivity;->mBackupListener:Lcom/mediatek/datatransfer/BackupService$OnBackupStatusListener;

    iget-object v0, p0, Lcom/mediatek/datatransfer/AbstractBackupActivity;->mBackupListener:Lcom/mediatek/datatransfer/BackupService$OnBackupStatusListener;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/mediatek/datatransfer/AbstractBackupActivity;->mBackupService:Lcom/mediatek/datatransfer/BackupService$BackupBinder;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/mediatek/datatransfer/AbstractBackupActivity;->mBackupService:Lcom/mediatek/datatransfer/BackupService$BackupBinder;

    iget-object v1, p0, Lcom/mediatek/datatransfer/AbstractBackupActivity;->mBackupListener:Lcom/mediatek/datatransfer/BackupService$OnBackupStatusListener;

    invoke-virtual {v0, v1}, Lcom/mediatek/datatransfer/BackupService$BackupBinder;->setOnBackupChangedListner(Lcom/mediatek/datatransfer/BackupService$OnBackupStatusListener;)V

    :cond_0
    return-void
.end method

.method protected showLoadingContent(Z)V
    .locals 4
    .param p1    # Z

    const/16 v2, 0x8

    const/4 v1, 0x0

    const v0, 0x7f080003

    invoke-virtual {p0, v0}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v3

    if-eqz p1, :cond_0

    move v0, v1

    :goto_0
    invoke-virtual {v3, v0}, Landroid/view/View;->setVisibility(I)V

    const v0, 0x7f080004

    invoke-virtual {p0, v0}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    if-nez p1, :cond_1

    :goto_1
    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    return-void

    :cond_0
    move v0, v2

    goto :goto_0

    :cond_1
    move v1, v2

    goto :goto_1
.end method

.method protected showProgress()V
    .locals 1

    iget-object v0, p0, Lcom/mediatek/datatransfer/AbstractBackupActivity;->mProgressDialog:Landroid/app/ProgressDialog;

    if-nez v0, :cond_0

    invoke-direct {p0}, Lcom/mediatek/datatransfer/AbstractBackupActivity;->createProgressDlg()Landroid/app/ProgressDialog;

    move-result-object v0

    iput-object v0, p0, Lcom/mediatek/datatransfer/AbstractBackupActivity;->mProgressDialog:Landroid/app/ProgressDialog;

    :cond_0
    iget-object v0, p0, Lcom/mediatek/datatransfer/AbstractBackupActivity;->mProgressDialog:Landroid/app/ProgressDialog;

    invoke-virtual {v0}, Landroid/app/Dialog;->show()V

    return-void
.end method

.method public abstract startBackup()V
.end method

.method protected startService()V
    .locals 2

    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/mediatek/datatransfer/BackupService;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    invoke-virtual {p0, v0}, Landroid/content/ContextWrapper;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;

    return-void
.end method

.method protected stopService()V
    .locals 2

    iget-object v0, p0, Lcom/mediatek/datatransfer/AbstractBackupActivity;->mBackupService:Lcom/mediatek/datatransfer/BackupService$BackupBinder;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/mediatek/datatransfer/AbstractBackupActivity;->mBackupService:Lcom/mediatek/datatransfer/BackupService$BackupBinder;

    invoke-virtual {v0}, Lcom/mediatek/datatransfer/BackupService$BackupBinder;->reset()V

    :cond_0
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/mediatek/datatransfer/BackupService;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    invoke-virtual {p0, v0}, Landroid/content/ContextWrapper;->stopService(Landroid/content/Intent;)Z

    return-void
.end method

.method protected updateButtonState()V
    .locals 5

    const/4 v4, 0x1

    const/4 v3, 0x0

    iget-object v0, p0, Lcom/mediatek/datatransfer/AbstractBackupActivity;->mCheckBoxSelect:Landroid/widget/CheckBox;

    invoke-virtual {v0, v3}, Landroid/view/View;->setVisibility(I)V

    iget-object v0, p0, Lcom/mediatek/datatransfer/AbstractBackupActivity;->mCheckBoxSelect:Landroid/widget/CheckBox;

    invoke-virtual {p0}, Landroid/app/Activity;->getApplication()Landroid/app/Application;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/ContextWrapper;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f060004

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lcom/mediatek/datatransfer/AbstractBackupActivity;->mDivider:Landroid/view/View;

    invoke-virtual {v0, v3}, Landroid/view/View;->setVisibility(I)V

    invoke-virtual {p0, v3}, Lcom/mediatek/datatransfer/CheckedListActivity;->isAllChecked(Z)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/mediatek/datatransfer/AbstractBackupActivity;->mButtonBackup:Landroid/widget/Button;

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setEnabled(Z)V

    iget-object v0, p0, Lcom/mediatek/datatransfer/AbstractBackupActivity;->mCheckBoxSelect:Landroid/widget/CheckBox;

    invoke-virtual {v0, v3}, Landroid/widget/CompoundButton;->setChecked(Z)V

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/mediatek/datatransfer/AbstractBackupActivity;->mButtonBackup:Landroid/widget/Button;

    invoke-virtual {v0, v4}, Landroid/widget/TextView;->setEnabled(Z)V

    iget-object v0, p0, Lcom/mediatek/datatransfer/AbstractBackupActivity;->mCheckBoxSelect:Landroid/widget/CheckBox;

    invoke-virtual {p0, v4}, Lcom/mediatek/datatransfer/CheckedListActivity;->isAllChecked(Z)Z

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/CompoundButton;->setChecked(Z)V

    goto :goto_0
.end method
