.class public abstract Lcom/mediatek/datatransfer/AbstractRestoreActivity;
.super Lcom/mediatek/datatransfer/CheckedListActivity;
.source "AbstractRestoreActivity.java"

# interfaces
.implements Lcom/mediatek/datatransfer/CheckedListActivity$OnCheckedCountChangedListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/mediatek/datatransfer/AbstractRestoreActivity$NormalRestoreStatusListener;
    }
.end annotation


# instance fields
.field private final CLASS_TAG:Ljava/lang/String;

.field loadingContent:Landroid/widget/LinearLayout;

.field mAdapter:Landroid/widget/BaseAdapter;

.field private mBtRestore:Landroid/widget/Button;

.field private mChboxSelect:Landroid/widget/CheckBox;

.field private mDivider:Landroid/view/View;

.field protected mHandler:Landroid/os/Handler;

.field private mProgressDialog:Landroid/app/ProgressDialog;

.field mRestoreListener:Lcom/mediatek/datatransfer/RestoreService$OnRestoreStatusListener;

.field protected mRestoreService:Lcom/mediatek/datatransfer/RestoreService$RestoreBinder;

.field mSDCardListener:Lcom/mediatek/datatransfer/SDCardReceiver$OnSDCardStatusChangedListener;

.field mServiceCon:Landroid/content/ServiceConnection;

.field protected mUnCheckedList:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 2

    const/4 v1, 0x0

    invoke-direct {p0}, Lcom/mediatek/datatransfer/CheckedListActivity;-><init>()V

    const-string v0, "DataTransfer/AbstractRestoreActivity"

    iput-object v0, p0, Lcom/mediatek/datatransfer/AbstractRestoreActivity;->CLASS_TAG:Ljava/lang/String;

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/mediatek/datatransfer/AbstractRestoreActivity;->mUnCheckedList:Ljava/util/ArrayList;

    iput-object v1, p0, Lcom/mediatek/datatransfer/AbstractRestoreActivity;->mBtRestore:Landroid/widget/Button;

    iput-object v1, p0, Lcom/mediatek/datatransfer/AbstractRestoreActivity;->mChboxSelect:Landroid/widget/CheckBox;

    iput-object v1, p0, Lcom/mediatek/datatransfer/AbstractRestoreActivity;->loadingContent:Landroid/widget/LinearLayout;

    new-instance v0, Lcom/mediatek/datatransfer/AbstractRestoreActivity$9;

    invoke-direct {v0, p0}, Lcom/mediatek/datatransfer/AbstractRestoreActivity$9;-><init>(Lcom/mediatek/datatransfer/AbstractRestoreActivity;)V

    iput-object v0, p0, Lcom/mediatek/datatransfer/AbstractRestoreActivity;->mServiceCon:Landroid/content/ServiceConnection;

    return-void
.end method

.method static synthetic access$000(Lcom/mediatek/datatransfer/AbstractRestoreActivity;)Landroid/app/ProgressDialog;
    .locals 1
    .param p0    # Lcom/mediatek/datatransfer/AbstractRestoreActivity;

    iget-object v0, p0, Lcom/mediatek/datatransfer/AbstractRestoreActivity;->mProgressDialog:Landroid/app/ProgressDialog;

    return-object v0
.end method

.method private bindService()V
    .locals 4

    invoke-virtual {p0}, Landroid/content/ContextWrapper;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    new-instance v1, Landroid/content/Intent;

    const-class v2, Lcom/mediatek/datatransfer/RestoreService;

    invoke-direct {v1, p0, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    iget-object v2, p0, Lcom/mediatek/datatransfer/AbstractRestoreActivity;->mServiceCon:Landroid/content/ServiceConnection;

    const/4 v3, 0x1

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/Context;->bindService(Landroid/content/Intent;Landroid/content/ServiceConnection;I)Z

    return-void
.end method

.method private init()V
    .locals 0

    invoke-virtual {p0, p0}, Lcom/mediatek/datatransfer/CheckedListActivity;->registerOnCheckedCountChangedListener(Lcom/mediatek/datatransfer/CheckedListActivity$OnCheckedCountChangedListener;)V

    invoke-direct {p0}, Lcom/mediatek/datatransfer/AbstractRestoreActivity;->registerSDCardListener()V

    invoke-direct {p0}, Lcom/mediatek/datatransfer/AbstractRestoreActivity;->initButton()V

    invoke-direct {p0}, Lcom/mediatek/datatransfer/AbstractRestoreActivity;->initHandler()V

    invoke-direct {p0}, Lcom/mediatek/datatransfer/AbstractRestoreActivity;->initLoadingView()V

    invoke-direct {p0}, Lcom/mediatek/datatransfer/AbstractRestoreActivity;->initDetailList()V

    invoke-virtual {p0}, Lcom/mediatek/datatransfer/AbstractRestoreActivity;->createProgressDlg()Landroid/app/ProgressDialog;

    return-void
.end method

.method private initButton()V
    .locals 2

    const v0, 0x7f080010

    invoke-virtual {p0, v0}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/mediatek/datatransfer/AbstractRestoreActivity;->mDivider:Landroid/view/View;

    iget-object v0, p0, Lcom/mediatek/datatransfer/AbstractRestoreActivity;->mDivider:Landroid/view/View;

    invoke-virtual {p0}, Landroid/app/ListActivity;->getListView()Landroid/widget/ListView;

    move-result-object v1

    invoke-virtual {v1}, Landroid/widget/ListView;->getDivider()Landroid/graphics/drawable/Drawable;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/view/View;->setBackground(Landroid/graphics/drawable/Drawable;)V

    const v0, 0x7f080011

    invoke-virtual {p0, v0}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lcom/mediatek/datatransfer/AbstractRestoreActivity;->mBtRestore:Landroid/widget/Button;

    iget-object v0, p0, Lcom/mediatek/datatransfer/AbstractRestoreActivity;->mBtRestore:Landroid/widget/Button;

    new-instance v1, Lcom/mediatek/datatransfer/AbstractRestoreActivity$5;

    invoke-direct {v1, p0}, Lcom/mediatek/datatransfer/AbstractRestoreActivity$5;-><init>(Lcom/mediatek/datatransfer/AbstractRestoreActivity;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    const v0, 0x7f08000f

    invoke-virtual {p0, v0}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/CheckBox;

    iput-object v0, p0, Lcom/mediatek/datatransfer/AbstractRestoreActivity;->mChboxSelect:Landroid/widget/CheckBox;

    iget-object v0, p0, Lcom/mediatek/datatransfer/AbstractRestoreActivity;->mChboxSelect:Landroid/widget/CheckBox;

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    iget-object v0, p0, Lcom/mediatek/datatransfer/AbstractRestoreActivity;->mChboxSelect:Landroid/widget/CheckBox;

    new-instance v1, Lcom/mediatek/datatransfer/AbstractRestoreActivity$6;

    invoke-direct {v1, p0}, Lcom/mediatek/datatransfer/AbstractRestoreActivity$6;-><init>(Lcom/mediatek/datatransfer/AbstractRestoreActivity;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    return-void
.end method

.method private initDetailList()V
    .locals 1

    invoke-virtual {p0}, Lcom/mediatek/datatransfer/AbstractRestoreActivity;->initAdapter()Landroid/widget/BaseAdapter;

    move-result-object v0

    iput-object v0, p0, Lcom/mediatek/datatransfer/AbstractRestoreActivity;->mAdapter:Landroid/widget/BaseAdapter;

    iget-object v0, p0, Lcom/mediatek/datatransfer/AbstractRestoreActivity;->mAdapter:Landroid/widget/BaseAdapter;

    invoke-virtual {p0, v0}, Landroid/app/ListActivity;->setListAdapter(Landroid/widget/ListAdapter;)V

    invoke-virtual {p0}, Lcom/mediatek/datatransfer/AbstractRestoreActivity;->updateButtonState()V

    return-void
.end method

.method private initHandler()V
    .locals 1

    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    iput-object v0, p0, Lcom/mediatek/datatransfer/AbstractRestoreActivity;->mHandler:Landroid/os/Handler;

    return-void
.end method

.method private initLoadingView()V
    .locals 1

    const v0, 0x7f080003

    invoke-virtual {p0, v0}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/mediatek/datatransfer/AbstractRestoreActivity;->loadingContent:Landroid/widget/LinearLayout;

    return-void
.end method

.method private registerSDCardListener()V
    .locals 2

    new-instance v1, Lcom/mediatek/datatransfer/AbstractRestoreActivity$4;

    invoke-direct {v1, p0}, Lcom/mediatek/datatransfer/AbstractRestoreActivity$4;-><init>(Lcom/mediatek/datatransfer/AbstractRestoreActivity;)V

    iput-object v1, p0, Lcom/mediatek/datatransfer/AbstractRestoreActivity;->mSDCardListener:Lcom/mediatek/datatransfer/SDCardReceiver$OnSDCardStatusChangedListener;

    invoke-static {}, Lcom/mediatek/datatransfer/SDCardReceiver;->getInstance()Lcom/mediatek/datatransfer/SDCardReceiver;

    move-result-object v0

    iget-object v1, p0, Lcom/mediatek/datatransfer/AbstractRestoreActivity;->mSDCardListener:Lcom/mediatek/datatransfer/SDCardReceiver$OnSDCardStatusChangedListener;

    invoke-virtual {v0, v1}, Lcom/mediatek/datatransfer/SDCardReceiver;->registerOnSDCardChangedListener(Lcom/mediatek/datatransfer/SDCardReceiver$OnSDCardStatusChangedListener;)V

    return-void
.end method

.method private unBindService()V
    .locals 2

    iget-object v0, p0, Lcom/mediatek/datatransfer/AbstractRestoreActivity;->mRestoreService:Lcom/mediatek/datatransfer/RestoreService$RestoreBinder;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/mediatek/datatransfer/AbstractRestoreActivity;->mRestoreService:Lcom/mediatek/datatransfer/RestoreService$RestoreBinder;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/mediatek/datatransfer/RestoreService$RestoreBinder;->setOnRestoreChangedListner(Lcom/mediatek/datatransfer/RestoreService$OnRestoreStatusListener;)V

    :cond_0
    invoke-virtual {p0}, Landroid/content/ContextWrapper;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    iget-object v1, p0, Lcom/mediatek/datatransfer/AbstractRestoreActivity;->mServiceCon:Landroid/content/ServiceConnection;

    invoke-virtual {v0, v1}, Landroid/content/Context;->unbindService(Landroid/content/ServiceConnection;)V

    return-void
.end method

.method private unRegisteSDCardListener()V
    .locals 2

    iget-object v1, p0, Lcom/mediatek/datatransfer/AbstractRestoreActivity;->mSDCardListener:Lcom/mediatek/datatransfer/SDCardReceiver$OnSDCardStatusChangedListener;

    if-eqz v1, :cond_0

    invoke-static {}, Lcom/mediatek/datatransfer/SDCardReceiver;->getInstance()Lcom/mediatek/datatransfer/SDCardReceiver;

    move-result-object v0

    iget-object v1, p0, Lcom/mediatek/datatransfer/AbstractRestoreActivity;->mSDCardListener:Lcom/mediatek/datatransfer/SDCardReceiver$OnSDCardStatusChangedListener;

    invoke-virtual {v0, v1}, Lcom/mediatek/datatransfer/SDCardReceiver;->unRegisterOnSDCardChangedListener(Lcom/mediatek/datatransfer/SDCardReceiver$OnSDCardStatusChangedListener;)V

    :cond_0
    return-void
.end method


# virtual methods
.method protected abstract afterServiceConnected()V
.end method

.method protected createProgressDlg()Landroid/app/ProgressDialog;
    .locals 2

    iget-object v0, p0, Lcom/mediatek/datatransfer/AbstractRestoreActivity;->mProgressDialog:Landroid/app/ProgressDialog;

    if-nez v0, :cond_0

    new-instance v0, Landroid/app/ProgressDialog;

    invoke-direct {v0, p0}, Landroid/app/ProgressDialog;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/mediatek/datatransfer/AbstractRestoreActivity;->mProgressDialog:Landroid/app/ProgressDialog;

    iget-object v0, p0, Lcom/mediatek/datatransfer/AbstractRestoreActivity;->mProgressDialog:Landroid/app/ProgressDialog;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/app/ProgressDialog;->setProgressStyle(I)V

    iget-object v0, p0, Lcom/mediatek/datatransfer/AbstractRestoreActivity;->mProgressDialog:Landroid/app/ProgressDialog;

    const v1, 0x7f06000f

    invoke-virtual {p0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/app/ProgressDialog;->setMessage(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lcom/mediatek/datatransfer/AbstractRestoreActivity;->mProgressDialog:Landroid/app/ProgressDialog;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/app/Dialog;->setCancelable(Z)V

    :cond_0
    iget-object v0, p0, Lcom/mediatek/datatransfer/AbstractRestoreActivity;->mProgressDialog:Landroid/app/ProgressDialog;

    return-object v0
.end method

.method protected dismissProgressDialog()V
    .locals 1

    iget-object v0, p0, Lcom/mediatek/datatransfer/AbstractRestoreActivity;->mProgressDialog:Landroid/app/ProgressDialog;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/mediatek/datatransfer/AbstractRestoreActivity;->mProgressDialog:Landroid/app/ProgressDialog;

    invoke-virtual {v0}, Landroid/app/Dialog;->isShowing()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/mediatek/datatransfer/AbstractRestoreActivity;->mProgressDialog:Landroid/app/ProgressDialog;

    invoke-virtual {v0}, Landroid/app/Dialog;->dismiss()V

    :cond_0
    return-void
.end method

.method protected errChecked()Z
    .locals 6

    const/4 v1, 0x0

    invoke-static {}, Lcom/mediatek/datatransfer/utils/SDCardUtils;->getStoragePath()Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_1

    const/4 v1, 0x1

    iget-object v2, p0, Lcom/mediatek/datatransfer/AbstractRestoreActivity;->mHandler:Landroid/os/Handler;

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/mediatek/datatransfer/AbstractRestoreActivity;->mHandler:Landroid/os/Handler;

    new-instance v3, Lcom/mediatek/datatransfer/AbstractRestoreActivity$7;

    invoke-direct {v3, p0}, Lcom/mediatek/datatransfer/AbstractRestoreActivity$7;-><init>(Lcom/mediatek/datatransfer/AbstractRestoreActivity;)V

    invoke-virtual {v2, v3}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    :cond_0
    :goto_0
    return v1

    :cond_1
    invoke-static {v0}, Lcom/mediatek/datatransfer/utils/SDCardUtils;->getAvailableSize(Ljava/lang/String;)J

    move-result-wide v2

    const-wide/16 v4, 0x200

    cmp-long v2, v2, v4

    if-gtz v2, :cond_0

    const/4 v1, 0x1

    iget-object v2, p0, Lcom/mediatek/datatransfer/AbstractRestoreActivity;->mHandler:Landroid/os/Handler;

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/mediatek/datatransfer/AbstractRestoreActivity;->mHandler:Landroid/os/Handler;

    new-instance v3, Lcom/mediatek/datatransfer/AbstractRestoreActivity$8;

    invoke-direct {v3, p0}, Lcom/mediatek/datatransfer/AbstractRestoreActivity$8;-><init>(Lcom/mediatek/datatransfer/AbstractRestoreActivity;)V

    invoke-virtual {v2, v3}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    goto :goto_0
.end method

.method protected abstract initAdapter()Landroid/widget/BaseAdapter;
.end method

.method protected isCanStartRestore()Z
    .locals 3

    const/4 v0, 0x0

    iget-object v1, p0, Lcom/mediatek/datatransfer/AbstractRestoreActivity;->mRestoreService:Lcom/mediatek/datatransfer/RestoreService$RestoreBinder;

    if-nez v1, :cond_0

    const-string v1, "DataTransfer/AbstractRestoreActivity"

    const-string v2, " isCanStartRestore : mRestoreService is null"

    invoke-static {v1, v2}, Lcom/mediatek/datatransfer/utils/MyLogger;->logE(Ljava/lang/String;Ljava/lang/String;)V

    :goto_0
    return v0

    :cond_0
    iget-object v1, p0, Lcom/mediatek/datatransfer/AbstractRestoreActivity;->mRestoreService:Lcom/mediatek/datatransfer/RestoreService$RestoreBinder;

    invoke-virtual {v1}, Lcom/mediatek/datatransfer/RestoreService$RestoreBinder;->getState()I

    move-result v1

    if-eqz v1, :cond_1

    const-string v1, "DataTransfer/AbstractRestoreActivity"

    const-string v2, " isCanStartRestore :Can not to start Restore. Restore Service is ruuning"

    invoke-static {v1, v2}, Lcom/mediatek/datatransfer/utils/MyLogger;->logE(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    :cond_1
    const/4 v0, 0x1

    goto :goto_0
.end method

.method protected isProgressDialogShowing()Z
    .locals 1

    iget-object v0, p0, Lcom/mediatek/datatransfer/AbstractRestoreActivity;->mProgressDialog:Landroid/app/ProgressDialog;

    invoke-virtual {v0}, Landroid/app/Dialog;->isShowing()Z

    move-result v0

    return v0
.end method

.method protected notifyListItemCheckedChanged()V
    .locals 1

    iget-object v0, p0, Lcom/mediatek/datatransfer/AbstractRestoreActivity;->mAdapter:Landroid/widget/BaseAdapter;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/mediatek/datatransfer/AbstractRestoreActivity;->mAdapter:Landroid/widget/BaseAdapter;

    invoke-virtual {v0}, Landroid/widget/BaseAdapter;->notifyDataSetChanged()V

    :cond_0
    invoke-virtual {p0}, Lcom/mediatek/datatransfer/AbstractRestoreActivity;->updateButtonState()V

    return-void
.end method

.method public onCheckedCountChanged()V
    .locals 1

    iget-object v0, p0, Lcom/mediatek/datatransfer/AbstractRestoreActivity;->mAdapter:Landroid/widget/BaseAdapter;

    invoke-virtual {v0}, Landroid/widget/BaseAdapter;->notifyDataSetChanged()V

    invoke-virtual {p0}, Lcom/mediatek/datatransfer/AbstractRestoreActivity;->updateButtonState()V

    return-void
.end method

.method public onConfigurationChanged(Landroid/content/res/Configuration;)V
    .locals 2
    .param p1    # Landroid/content/res/Configuration;

    invoke-super {p0, p1}, Landroid/app/Activity;->onConfigurationChanged(Landroid/content/res/Configuration;)V

    const-string v0, "DataTransfer/AbstractRestoreActivity"

    const-string v1, " onConfigurationChanged"

    invoke-static {v0, v1}, Lcom/mediatek/datatransfer/utils/MyLogger;->logI(Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 3
    .param p1    # Landroid/os/Bundle;

    const/4 v1, 0x5

    invoke-virtual {p0, v1}, Landroid/app/Activity;->requestWindowFeature(I)Z

    invoke-super {p0, p1}, Lcom/mediatek/datatransfer/CheckedListActivity;->onCreate(Landroid/os/Bundle;)V

    const/4 v1, 0x0

    invoke-virtual {p0, v1}, Landroid/app/Activity;->setProgressBarIndeterminateVisibility(Z)V

    const v1, 0x7f030005

    invoke-virtual {p0, v1}, Landroid/app/Activity;->setContentView(I)V

    invoke-virtual {p0}, Landroid/app/Activity;->getActionBar()Landroid/app/ActionBar;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/app/ActionBar;->setDisplayHomeAsUpEnabled(Z)V

    invoke-direct {p0}, Lcom/mediatek/datatransfer/AbstractRestoreActivity;->init()V

    invoke-direct {p0}, Lcom/mediatek/datatransfer/AbstractRestoreActivity;->bindService()V

    const-string v1, "DataTransfer/AbstractRestoreActivity"

    const-string v2, " onCreate"

    invoke-static {v1, v2}, Lcom/mediatek/datatransfer/utils/MyLogger;->logI(Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method protected onCreateDialog(I)Landroid/app/Dialog;
    .locals 1
    .param p1    # I

    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, Lcom/mediatek/datatransfer/AbstractRestoreActivity;->onCreateDialog(ILandroid/os/Bundle;)Landroid/app/Dialog;

    move-result-object v0

    return-object v0
.end method

.method protected onCreateDialog(ILandroid/os/Bundle;)Landroid/app/Dialog;
    .locals 8
    .param p1    # I
    .param p2    # Landroid/os/Bundle;

    const v7, 0x7f060025

    const v6, 0x1080027

    const v5, 0x104000a

    const/4 v4, 0x0

    const/4 v0, 0x0

    const-string v1, "DataTransfer/AbstractRestoreActivity"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, " oncreateDialog, id = "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/mediatek/datatransfer/utils/MyLogger;->logI(Ljava/lang/String;Ljava/lang/String;)V

    packed-switch p1, :pswitch_data_0

    :goto_0
    return-object v0

    :pswitch_0
    new-instance v1, Landroid/app/AlertDialog$Builder;

    invoke-direct {v1, p0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    invoke-virtual {v1, v6}, Landroid/app/AlertDialog$Builder;->setIcon(I)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    const v2, 0x7f06001a

    invoke-virtual {v1, v2}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    const v2, 0x7f06001c

    invoke-virtual {v1, v2}, Landroid/app/AlertDialog$Builder;->setMessage(I)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    const/high16 v2, 0x1040000

    const/4 v3, 0x0

    invoke-virtual {v1, v2, v3}, Landroid/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    new-instance v2, Lcom/mediatek/datatransfer/AbstractRestoreActivity$1;

    invoke-direct {v2, p0}, Lcom/mediatek/datatransfer/AbstractRestoreActivity$1;-><init>(Lcom/mediatek/datatransfer/AbstractRestoreActivity;)V

    invoke-virtual {v1, v5, v2}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    invoke-virtual {v1, v4}, Landroid/app/AlertDialog$Builder;->setCancelable(Z)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    invoke-virtual {v1}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v0

    goto :goto_0

    :pswitch_1
    new-instance v1, Landroid/app/AlertDialog$Builder;

    invoke-direct {v1, p0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    invoke-virtual {v1, v7}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    invoke-virtual {v1, v6}, Landroid/app/AlertDialog$Builder;->setIcon(I)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    const v2, 0x7f06001e

    invoke-virtual {v1, v2}, Landroid/app/AlertDialog$Builder;->setMessage(I)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    new-instance v2, Lcom/mediatek/datatransfer/AbstractRestoreActivity$2;

    invoke-direct {v2, p0}, Lcom/mediatek/datatransfer/AbstractRestoreActivity$2;-><init>(Lcom/mediatek/datatransfer/AbstractRestoreActivity;)V

    invoke-virtual {v1, v5, v2}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    invoke-virtual {v1, v4}, Landroid/app/AlertDialog$Builder;->setCancelable(Z)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    invoke-virtual {v1}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v0

    goto :goto_0

    :pswitch_2
    new-instance v1, Landroid/app/AlertDialog$Builder;

    invoke-direct {v1, p0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    invoke-virtual {v1, v7}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    invoke-virtual {v1, v6}, Landroid/app/AlertDialog$Builder;->setIcon(I)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    const v2, 0x7f06001f

    invoke-virtual {v1, v2}, Landroid/app/AlertDialog$Builder;->setMessage(I)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    new-instance v2, Lcom/mediatek/datatransfer/AbstractRestoreActivity$3;

    invoke-direct {v2, p0}, Lcom/mediatek/datatransfer/AbstractRestoreActivity$3;-><init>(Lcom/mediatek/datatransfer/AbstractRestoreActivity;)V

    invoke-virtual {v1, v5, v2}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    invoke-virtual {v1, v4}, Landroid/app/AlertDialog$Builder;->setCancelable(Z)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    invoke-virtual {v1}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v0

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x7d0
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method protected onDestroy()V
    .locals 3

    const/4 v2, 0x0

    invoke-super {p0}, Lcom/mediatek/datatransfer/CheckedListActivity;->onDestroy()V

    const-string v0, "DataTransfer/AbstractRestoreActivity"

    const-string v1, " onDestroy"

    invoke-static {v0, v1}, Lcom/mediatek/datatransfer/utils/MyLogger;->logI(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/mediatek/datatransfer/AbstractRestoreActivity;->mProgressDialog:Landroid/app/ProgressDialog;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/mediatek/datatransfer/AbstractRestoreActivity;->mProgressDialog:Landroid/app/ProgressDialog;

    invoke-virtual {v0}, Landroid/app/Dialog;->isShowing()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/mediatek/datatransfer/AbstractRestoreActivity;->mProgressDialog:Landroid/app/ProgressDialog;

    invoke-virtual {v0}, Landroid/app/Dialog;->dismiss()V

    :cond_0
    invoke-direct {p0}, Lcom/mediatek/datatransfer/AbstractRestoreActivity;->unRegisteSDCardListener()V

    iget-object v0, p0, Lcom/mediatek/datatransfer/AbstractRestoreActivity;->mRestoreService:Lcom/mediatek/datatransfer/RestoreService$RestoreBinder;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/mediatek/datatransfer/AbstractRestoreActivity;->mRestoreService:Lcom/mediatek/datatransfer/RestoreService$RestoreBinder;

    invoke-virtual {v0}, Lcom/mediatek/datatransfer/RestoreService$RestoreBinder;->getState()I

    move-result v0

    if-nez v0, :cond_1

    invoke-virtual {p0}, Lcom/mediatek/datatransfer/AbstractRestoreActivity;->stopService()V

    :cond_1
    iget-object v0, p0, Lcom/mediatek/datatransfer/AbstractRestoreActivity;->mRestoreService:Lcom/mediatek/datatransfer/RestoreService$RestoreBinder;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/mediatek/datatransfer/AbstractRestoreActivity;->mRestoreService:Lcom/mediatek/datatransfer/RestoreService$RestoreBinder;

    invoke-virtual {v0, v2}, Lcom/mediatek/datatransfer/RestoreService$RestoreBinder;->setOnRestoreChangedListner(Lcom/mediatek/datatransfer/RestoreService$OnRestoreStatusListener;)V

    :cond_2
    invoke-direct {p0}, Lcom/mediatek/datatransfer/AbstractRestoreActivity;->unBindService()V

    iput-object v2, p0, Lcom/mediatek/datatransfer/AbstractRestoreActivity;->mHandler:Landroid/os/Handler;

    return-void
.end method

.method public onKeyDown(ILandroid/view/KeyEvent;)Z
    .locals 1
    .param p1    # I
    .param p2    # Landroid/view/KeyEvent;

    const/16 v0, 0x52

    if-ne p1, v0, :cond_0

    invoke-virtual {p2}, Landroid/view/KeyEvent;->isLongPress()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    invoke-super {p0, p1, p2}, Landroid/app/Activity;->onKeyDown(ILandroid/view/KeyEvent;)Z

    move-result v0

    goto :goto_0
.end method

.method public onOptionsItemSelected(Landroid/view/MenuItem;)Z
    .locals 3
    .param p1    # Landroid/view/MenuItem;

    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v1

    packed-switch v1, :pswitch_data_0

    invoke-super {p0, p1}, Landroid/app/Activity;->onOptionsItemSelected(Landroid/view/MenuItem;)Z

    move-result v1

    :goto_0
    return v1

    :pswitch_0
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/mediatek/datatransfer/MainActivity;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const-string v1, "action_where"

    const-string v2, "ction_restore"

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const/high16 v1, 0x4000000

    invoke-virtual {v0, v1}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    invoke-virtual {p0, v0}, Landroid/app/Activity;->startActivity(Landroid/content/Intent;)V

    const/4 v1, 0x1

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x102002c
        :pswitch_0
    .end packed-switch
.end method

.method public onPause()V
    .locals 2

    invoke-super {p0}, Landroid/app/Activity;->onPause()V

    const-string v0, "DataTransfer/AbstractRestoreActivity"

    const-string v1, " onPasue"

    invoke-static {v0, v1}, Lcom/mediatek/datatransfer/utils/MyLogger;->logI(Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method protected onStart()V
    .locals 2

    const-string v0, "DataTransfer/AbstractRestoreActivity"

    const-string v1, " onStart"

    invoke-static {v0, v1}, Lcom/mediatek/datatransfer/utils/MyLogger;->logI(Ljava/lang/String;Ljava/lang/String;)V

    invoke-super {p0}, Landroid/app/Activity;->onStart()V

    return-void
.end method

.method public onStop()V
    .locals 2

    invoke-super {p0}, Landroid/app/Activity;->onStop()V

    const-string v0, "DataTransfer/AbstractRestoreActivity"

    const-string v1, " onStop"

    invoke-static {v0, v1}, Lcom/mediatek/datatransfer/utils/MyLogger;->logI(Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method protected setButtonsEnable(Z)V
    .locals 1
    .param p1    # Z

    iget-object v0, p0, Lcom/mediatek/datatransfer/AbstractRestoreActivity;->mChboxSelect:Landroid/widget/CheckBox;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/mediatek/datatransfer/AbstractRestoreActivity;->mChboxSelect:Landroid/widget/CheckBox;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setEnabled(Z)V

    :cond_0
    iget-object v0, p0, Lcom/mediatek/datatransfer/AbstractRestoreActivity;->mBtRestore:Landroid/widget/Button;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/mediatek/datatransfer/AbstractRestoreActivity;->mBtRestore:Landroid/widget/Button;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setEnabled(Z)V

    :cond_1
    return-void
.end method

.method public setOnRestoreStatusListener(Lcom/mediatek/datatransfer/RestoreService$OnRestoreStatusListener;)V
    .locals 2
    .param p1    # Lcom/mediatek/datatransfer/RestoreService$OnRestoreStatusListener;

    iput-object p1, p0, Lcom/mediatek/datatransfer/AbstractRestoreActivity;->mRestoreListener:Lcom/mediatek/datatransfer/RestoreService$OnRestoreStatusListener;

    iget-object v0, p0, Lcom/mediatek/datatransfer/AbstractRestoreActivity;->mRestoreService:Lcom/mediatek/datatransfer/RestoreService$RestoreBinder;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/mediatek/datatransfer/AbstractRestoreActivity;->mRestoreService:Lcom/mediatek/datatransfer/RestoreService$RestoreBinder;

    iget-object v1, p0, Lcom/mediatek/datatransfer/AbstractRestoreActivity;->mRestoreListener:Lcom/mediatek/datatransfer/RestoreService$OnRestoreStatusListener;

    invoke-virtual {v0, v1}, Lcom/mediatek/datatransfer/RestoreService$RestoreBinder;->setOnRestoreChangedListner(Lcom/mediatek/datatransfer/RestoreService$OnRestoreStatusListener;)V

    :cond_0
    return-void
.end method

.method protected setProgressDialogMax(I)V
    .locals 1
    .param p1    # I

    iget-object v0, p0, Lcom/mediatek/datatransfer/AbstractRestoreActivity;->mProgressDialog:Landroid/app/ProgressDialog;

    if-nez v0, :cond_0

    invoke-virtual {p0}, Lcom/mediatek/datatransfer/AbstractRestoreActivity;->createProgressDlg()Landroid/app/ProgressDialog;

    move-result-object v0

    iput-object v0, p0, Lcom/mediatek/datatransfer/AbstractRestoreActivity;->mProgressDialog:Landroid/app/ProgressDialog;

    :cond_0
    iget-object v0, p0, Lcom/mediatek/datatransfer/AbstractRestoreActivity;->mProgressDialog:Landroid/app/ProgressDialog;

    invoke-virtual {v0, p1}, Landroid/app/ProgressDialog;->setMax(I)V

    return-void
.end method

.method protected setProgressDialogMessage(Ljava/lang/CharSequence;)V
    .locals 1
    .param p1    # Ljava/lang/CharSequence;

    iget-object v0, p0, Lcom/mediatek/datatransfer/AbstractRestoreActivity;->mProgressDialog:Landroid/app/ProgressDialog;

    if-nez v0, :cond_0

    invoke-virtual {p0}, Lcom/mediatek/datatransfer/AbstractRestoreActivity;->createProgressDlg()Landroid/app/ProgressDialog;

    move-result-object v0

    iput-object v0, p0, Lcom/mediatek/datatransfer/AbstractRestoreActivity;->mProgressDialog:Landroid/app/ProgressDialog;

    :cond_0
    iget-object v0, p0, Lcom/mediatek/datatransfer/AbstractRestoreActivity;->mProgressDialog:Landroid/app/ProgressDialog;

    invoke-virtual {v0, p1}, Landroid/app/ProgressDialog;->setMessage(Ljava/lang/CharSequence;)V

    return-void
.end method

.method protected setProgressDialogProgress(I)V
    .locals 1
    .param p1    # I

    iget-object v0, p0, Lcom/mediatek/datatransfer/AbstractRestoreActivity;->mProgressDialog:Landroid/app/ProgressDialog;

    if-nez v0, :cond_0

    invoke-virtual {p0}, Lcom/mediatek/datatransfer/AbstractRestoreActivity;->createProgressDlg()Landroid/app/ProgressDialog;

    move-result-object v0

    iput-object v0, p0, Lcom/mediatek/datatransfer/AbstractRestoreActivity;->mProgressDialog:Landroid/app/ProgressDialog;

    :cond_0
    iget-object v0, p0, Lcom/mediatek/datatransfer/AbstractRestoreActivity;->mProgressDialog:Landroid/app/ProgressDialog;

    invoke-virtual {v0, p1}, Landroid/app/ProgressDialog;->setProgress(I)V

    return-void
.end method

.method protected showLoadingContent(Z)V
    .locals 4
    .param p1    # Z

    const/16 v2, 0x8

    const/4 v1, 0x0

    const v0, 0x7f080003

    invoke-virtual {p0, v0}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v3

    if-eqz p1, :cond_0

    move v0, v1

    :goto_0
    invoke-virtual {v3, v0}, Landroid/view/View;->setVisibility(I)V

    const v0, 0x7f08000e

    invoke-virtual {p0, v0}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    if-nez p1, :cond_1

    :goto_1
    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    return-void

    :cond_0
    move v0, v2

    goto :goto_0

    :cond_1
    move v1, v2

    goto :goto_1
.end method

.method protected showProgressDialog()V
    .locals 1

    iget-object v0, p0, Lcom/mediatek/datatransfer/AbstractRestoreActivity;->mProgressDialog:Landroid/app/ProgressDialog;

    if-nez v0, :cond_0

    invoke-virtual {p0}, Lcom/mediatek/datatransfer/AbstractRestoreActivity;->createProgressDlg()Landroid/app/ProgressDialog;

    move-result-object v0

    iput-object v0, p0, Lcom/mediatek/datatransfer/AbstractRestoreActivity;->mProgressDialog:Landroid/app/ProgressDialog;

    :cond_0
    iget-object v0, p0, Lcom/mediatek/datatransfer/AbstractRestoreActivity;->mProgressDialog:Landroid/app/ProgressDialog;

    invoke-virtual {v0}, Landroid/app/Dialog;->show()V

    return-void
.end method

.method protected abstract startRestore()V
.end method

.method protected startService()V
    .locals 2

    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/mediatek/datatransfer/RestoreService;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    invoke-virtual {p0, v0}, Landroid/content/ContextWrapper;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;

    return-void
.end method

.method protected stopService()V
    .locals 2

    iget-object v0, p0, Lcom/mediatek/datatransfer/AbstractRestoreActivity;->mRestoreService:Lcom/mediatek/datatransfer/RestoreService$RestoreBinder;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/mediatek/datatransfer/AbstractRestoreActivity;->mRestoreService:Lcom/mediatek/datatransfer/RestoreService$RestoreBinder;

    invoke-virtual {v0}, Lcom/mediatek/datatransfer/RestoreService$RestoreBinder;->reset()V

    :cond_0
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/mediatek/datatransfer/RestoreService;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    invoke-virtual {p0, v0}, Landroid/content/ContextWrapper;->stopService(Landroid/content/Intent;)Z

    return-void
.end method

.method protected updateButtonState()V
    .locals 3

    const/4 v1, 0x1

    const/4 v2, 0x0

    invoke-virtual {p0}, Lcom/mediatek/datatransfer/CheckedListActivity;->getCount()I

    move-result v0

    if-nez v0, :cond_0

    invoke-virtual {p0, v2}, Lcom/mediatek/datatransfer/AbstractRestoreActivity;->setButtonsEnable(Z)V

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/mediatek/datatransfer/AbstractRestoreActivity;->mDivider:Landroid/view/View;

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    invoke-virtual {p0, v2}, Lcom/mediatek/datatransfer/CheckedListActivity;->isAllChecked(Z)Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/mediatek/datatransfer/AbstractRestoreActivity;->mBtRestore:Landroid/widget/Button;

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setEnabled(Z)V

    iget-object v0, p0, Lcom/mediatek/datatransfer/AbstractRestoreActivity;->mChboxSelect:Landroid/widget/CheckBox;

    invoke-virtual {v0, v2}, Landroid/widget/CompoundButton;->setChecked(Z)V

    goto :goto_0

    :cond_1
    iget-object v0, p0, Lcom/mediatek/datatransfer/AbstractRestoreActivity;->mBtRestore:Landroid/widget/Button;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setEnabled(Z)V

    iget-object v0, p0, Lcom/mediatek/datatransfer/AbstractRestoreActivity;->mChboxSelect:Landroid/widget/CheckBox;

    invoke-virtual {p0, v1}, Lcom/mediatek/datatransfer/CheckedListActivity;->isAllChecked(Z)Z

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/CompoundButton;->setChecked(Z)V

    iget-object v0, p0, Lcom/mediatek/datatransfer/AbstractRestoreActivity;->mChboxSelect:Landroid/widget/CheckBox;

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    iget-object v0, p0, Lcom/mediatek/datatransfer/AbstractRestoreActivity;->mChboxSelect:Landroid/widget/CheckBox;

    invoke-virtual {p0}, Landroid/app/Activity;->getApplication()Landroid/app/Application;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/ContextWrapper;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f060004

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0
.end method
