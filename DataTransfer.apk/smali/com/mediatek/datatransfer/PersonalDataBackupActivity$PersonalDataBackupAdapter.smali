.class Lcom/mediatek/datatransfer/PersonalDataBackupActivity$PersonalDataBackupAdapter;
.super Landroid/widget/BaseAdapter;
.source "PersonalDataBackupActivity.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/mediatek/datatransfer/PersonalDataBackupActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "PersonalDataBackupAdapter"
.end annotation


# instance fields
.field private mDataList:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/mediatek/datatransfer/PersonalItemData;",
            ">;"
        }
    .end annotation
.end field

.field private mInflater:Landroid/view/LayoutInflater;

.field private mLayoutId:I

.field final synthetic this$0:Lcom/mediatek/datatransfer/PersonalDataBackupActivity;


# direct methods
.method public constructor <init>(Lcom/mediatek/datatransfer/PersonalDataBackupActivity;Landroid/content/Context;Ljava/util/ArrayList;I)V
    .locals 1
    .param p2    # Landroid/content/Context;
    .param p4    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/mediatek/datatransfer/PersonalItemData;",
            ">;I)V"
        }
    .end annotation

    iput-object p1, p0, Lcom/mediatek/datatransfer/PersonalDataBackupActivity$PersonalDataBackupAdapter;->this$0:Lcom/mediatek/datatransfer/PersonalDataBackupActivity;

    invoke-direct {p0}, Landroid/widget/BaseAdapter;-><init>()V

    iput-object p3, p0, Lcom/mediatek/datatransfer/PersonalDataBackupActivity$PersonalDataBackupAdapter;->mDataList:Ljava/util/ArrayList;

    iput p4, p0, Lcom/mediatek/datatransfer/PersonalDataBackupActivity$PersonalDataBackupAdapter;->mLayoutId:I

    invoke-static {p2}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    iput-object v0, p0, Lcom/mediatek/datatransfer/PersonalDataBackupActivity$PersonalDataBackupAdapter;->mInflater:Landroid/view/LayoutInflater;

    return-void
.end method


# virtual methods
.method public changeData(Ljava/util/ArrayList;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/mediatek/datatransfer/PersonalItemData;",
            ">;)V"
        }
    .end annotation

    iput-object p1, p0, Lcom/mediatek/datatransfer/PersonalDataBackupActivity$PersonalDataBackupAdapter;->mDataList:Ljava/util/ArrayList;

    return-void
.end method

.method public getCount()I
    .locals 1

    iget-object v0, p0, Lcom/mediatek/datatransfer/PersonalDataBackupActivity$PersonalDataBackupAdapter;->mDataList:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    return v0
.end method

.method public getItem(I)Ljava/lang/Object;
    .locals 1
    .param p1    # I

    iget-object v0, p0, Lcom/mediatek/datatransfer/PersonalDataBackupActivity$PersonalDataBackupAdapter;->mDataList:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public getItemId(I)J
    .locals 2
    .param p1    # I

    iget-object v0, p0, Lcom/mediatek/datatransfer/PersonalDataBackupActivity$PersonalDataBackupAdapter;->mDataList:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/mediatek/datatransfer/PersonalItemData;

    invoke-virtual {v0}, Lcom/mediatek/datatransfer/PersonalItemData;->getType()I

    move-result v0

    int-to-long v0, v0

    return-wide v0
.end method

.method public getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 17
    .param p1    # I
    .param p2    # Landroid/view/View;
    .param p3    # Landroid/view/ViewGroup;

    move-object/from16 v13, p2

    if-nez v13, :cond_0

    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/mediatek/datatransfer/PersonalDataBackupActivity$PersonalDataBackupAdapter;->mInflater:Landroid/view/LayoutInflater;

    move-object/from16 v0, p0

    iget v15, v0, Lcom/mediatek/datatransfer/PersonalDataBackupActivity$PersonalDataBackupAdapter;->mLayoutId:I

    const/16 v16, 0x0

    move-object/from16 v0, p3

    move/from16 v1, v16

    invoke-virtual {v14, v15, v0, v1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v13

    :cond_0
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/mediatek/datatransfer/PersonalDataBackupActivity$PersonalDataBackupAdapter;->mDataList:Ljava/util/ArrayList;

    move/from16 v0, p1

    invoke-virtual {v14, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v11

    check-cast v11, Lcom/mediatek/datatransfer/PersonalItemData;

    const v14, 0x7f080008

    invoke-virtual {v13, v14}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v6

    const v14, 0x7f080009

    invoke-virtual {v13, v14}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v5

    const/high16 v14, 0x7f080000

    invoke-virtual {v13, v14}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v9

    check-cast v9, Landroid/widget/ImageView;

    const v14, 0x7f080001

    invoke-virtual {v13, v14}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v12

    check-cast v12, Landroid/widget/TextView;

    const v14, 0x7f080002

    invoke-virtual {v13, v14}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/CheckBox;

    invoke-virtual {v11}, Lcom/mediatek/datatransfer/PersonalItemData;->isEnable()Z

    move-result v3

    invoke-virtual {v9, v3}, Landroid/view/View;->setEnabled(Z)V

    invoke-virtual {v12, v3}, Landroid/widget/TextView;->setEnabled(Z)V

    if-eqz v3, :cond_3

    const/high16 v14, 0x3f800000

    :goto_0
    invoke-virtual {v6, v14}, Landroid/view/View;->setAlpha(F)V

    invoke-virtual {v4, v3}, Landroid/widget/TextView;->setEnabled(Z)V

    invoke-virtual {v11}, Lcom/mediatek/datatransfer/PersonalItemData;->getType()I

    move-result v14

    const/4 v15, 0x1

    if-ne v14, v15, :cond_5

    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/mediatek/datatransfer/PersonalDataBackupActivity$PersonalDataBackupAdapter;->this$0:Lcom/mediatek/datatransfer/PersonalDataBackupActivity;

    move/from16 v0, p1

    invoke-virtual {v14, v0}, Lcom/mediatek/datatransfer/CheckedListActivity;->isItemCheckedByPosition(I)Z

    move-result v10

    if-eqz v10, :cond_4

    const/high16 v2, 0x3f800000

    :goto_1
    invoke-virtual {v5, v10}, Landroid/view/View;->setEnabled(Z)V

    invoke-virtual {v5, v2}, Landroid/view/View;->setAlpha(F)V

    const/4 v14, 0x0

    invoke-virtual {v5, v14}, Landroid/view/View;->setVisibility(I)V

    new-instance v14, Lcom/mediatek/datatransfer/PersonalDataBackupActivity$PersonalDataBackupAdapter$1;

    move-object/from16 v0, p0

    invoke-direct {v14, v0}, Lcom/mediatek/datatransfer/PersonalDataBackupActivity$PersonalDataBackupAdapter$1;-><init>(Lcom/mediatek/datatransfer/PersonalDataBackupActivity$PersonalDataBackupAdapter;)V

    invoke-virtual {v5, v14}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    :goto_2
    new-instance v14, Lcom/mediatek/datatransfer/PersonalDataBackupActivity$PersonalDataBackupAdapter$3;

    move-object/from16 v0, p0

    move/from16 v1, p1

    invoke-direct {v14, v0, v4, v1}, Lcom/mediatek/datatransfer/PersonalDataBackupActivity$PersonalDataBackupAdapter$3;-><init>(Lcom/mediatek/datatransfer/PersonalDataBackupActivity$PersonalDataBackupAdapter;Landroid/widget/CheckBox;I)V

    invoke-virtual {v6, v14}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    if-nez v3, :cond_1

    const/4 v14, 0x0

    invoke-virtual {v4, v14}, Landroid/widget/CompoundButton;->setChecked(Z)V

    :cond_1
    invoke-virtual/range {p0 .. p1}, Lcom/mediatek/datatransfer/PersonalDataBackupActivity$PersonalDataBackupAdapter;->getItemId(I)J

    move-result-wide v7

    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/mediatek/datatransfer/PersonalDataBackupActivity$PersonalDataBackupAdapter;->this$0:Lcom/mediatek/datatransfer/PersonalDataBackupActivity;

    if-nez v3, :cond_8

    const/4 v14, 0x1

    :goto_3
    invoke-virtual {v15, v7, v8, v14}, Lcom/mediatek/datatransfer/CheckedListActivity;->setItemDisabledById(JZ)V

    invoke-virtual {v11}, Lcom/mediatek/datatransfer/PersonalItemData;->getIconId()I

    move-result v14

    invoke-virtual {v9, v14}, Landroid/widget/ImageView;->setImageResource(I)V

    invoke-virtual {v11}, Lcom/mediatek/datatransfer/PersonalItemData;->getTextId()I

    move-result v14

    invoke-virtual {v12, v14}, Landroid/widget/TextView;->setText(I)V

    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/mediatek/datatransfer/PersonalDataBackupActivity$PersonalDataBackupAdapter;->this$0:Lcom/mediatek/datatransfer/PersonalDataBackupActivity;

    move/from16 v0, p1

    invoke-virtual {v14, v0}, Lcom/mediatek/datatransfer/CheckedListActivity;->isItemCheckedByPosition(I)Z

    move-result v14

    if-eqz v14, :cond_9

    invoke-virtual {v4}, Landroid/view/View;->isEnabled()Z

    move-result v14

    if-eqz v14, :cond_2

    const/4 v14, 0x1

    invoke-virtual {v4, v14}, Landroid/widget/CompoundButton;->setChecked(Z)V

    :cond_2
    :goto_4
    return-object v13

    :cond_3
    const v14, 0x3ecccccd

    goto :goto_0

    :cond_4
    const v2, 0x3ecccccd

    goto :goto_1

    :cond_5
    invoke-virtual {v11}, Lcom/mediatek/datatransfer/PersonalItemData;->getType()I

    move-result v14

    const/16 v15, 0x40

    if-ne v14, v15, :cond_7

    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/mediatek/datatransfer/PersonalDataBackupActivity$PersonalDataBackupAdapter;->this$0:Lcom/mediatek/datatransfer/PersonalDataBackupActivity;

    move/from16 v0, p1

    invoke-virtual {v14, v0}, Lcom/mediatek/datatransfer/CheckedListActivity;->isItemCheckedByPosition(I)Z

    move-result v10

    if-eqz v10, :cond_6

    const/high16 v2, 0x3f800000

    :goto_5
    invoke-virtual {v5, v10}, Landroid/view/View;->setEnabled(Z)V

    invoke-virtual {v5, v2}, Landroid/view/View;->setAlpha(F)V

    const/4 v14, 0x0

    invoke-virtual {v5, v14}, Landroid/view/View;->setVisibility(I)V

    new-instance v14, Lcom/mediatek/datatransfer/PersonalDataBackupActivity$PersonalDataBackupAdapter$2;

    move-object/from16 v0, p0

    invoke-direct {v14, v0}, Lcom/mediatek/datatransfer/PersonalDataBackupActivity$PersonalDataBackupAdapter$2;-><init>(Lcom/mediatek/datatransfer/PersonalDataBackupActivity$PersonalDataBackupAdapter;)V

    invoke-virtual {v5, v14}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    goto :goto_2

    :cond_6
    const v2, 0x3ecccccd

    goto :goto_5

    :cond_7
    const/16 v14, 0x8

    invoke-virtual {v5, v14}, Landroid/view/View;->setVisibility(I)V

    const/4 v14, 0x0

    invoke-virtual {v5, v14}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    goto/16 :goto_2

    :cond_8
    const/4 v14, 0x0

    goto :goto_3

    :cond_9
    invoke-virtual {v4}, Landroid/view/View;->isEnabled()Z

    move-result v14

    if-eqz v14, :cond_2

    const/4 v14, 0x0

    invoke-virtual {v4, v14}, Landroid/widget/CompoundButton;->setChecked(Z)V

    goto :goto_4
.end method

.method public reset()V
    .locals 1

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/mediatek/datatransfer/PersonalDataBackupActivity$PersonalDataBackupAdapter;->mDataList:Ljava/util/ArrayList;

    return-void
.end method
