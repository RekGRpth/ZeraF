.class Lcom/mediatek/datatransfer/MainActivity$MainOnSDCardStatusChangedListener;
.super Ljava/lang/Object;
.source "MainActivity.java"

# interfaces
.implements Lcom/mediatek/datatransfer/SDCardReceiver$OnSDCardStatusChangedListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/mediatek/datatransfer/MainActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = "MainOnSDCardStatusChangedListener"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/mediatek/datatransfer/MainActivity;


# direct methods
.method constructor <init>(Lcom/mediatek/datatransfer/MainActivity;)V
    .locals 0

    iput-object p1, p0, Lcom/mediatek/datatransfer/MainActivity$MainOnSDCardStatusChangedListener;->this$0:Lcom/mediatek/datatransfer/MainActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onSDCardStatusChanged(Z)V
    .locals 3
    .param p1    # Z

    const-string v0, "DataTransfer/MainActivity"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "onSDCardStatusChanged - "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/mediatek/datatransfer/utils/MyLogger;->logD(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/mediatek/datatransfer/MainActivity$MainOnSDCardStatusChangedListener;->this$0:Lcom/mediatek/datatransfer/MainActivity;

    invoke-static {v0}, Lcom/mediatek/datatransfer/MainActivity;->access$000(Lcom/mediatek/datatransfer/MainActivity;)V

    return-void
.end method
