.class public Lcom/mediatek/datatransfer/BackupEngine;
.super Ljava/lang/Object;
.source "BackupEngine.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/mediatek/datatransfer/BackupEngine$1;,
        Lcom/mediatek/datatransfer/BackupEngine$BackupThread;,
        Lcom/mediatek/datatransfer/BackupEngine$BackupResultType;,
        Lcom/mediatek/datatransfer/BackupEngine$OnBackupDoneListner;
    }
.end annotation


# static fields
.field private static mSelfInstance:Lcom/mediatek/datatransfer/BackupEngine;


# instance fields
.field private mBackupDoneListner:Lcom/mediatek/datatransfer/BackupEngine$OnBackupDoneListner;

.field private mBackupFolder:Ljava/lang/String;

.field private mComposerList:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/mediatek/datatransfer/modules/Composer;",
            ">;"
        }
    .end annotation
.end field

.field private mContext:Landroid/content/Context;

.field private mIsCancel:Z

.field private mIsPause:Z

.field private mIsRunning:Z

.field private mLock:Ljava/lang/Object;

.field mModuleList:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field mParasMap:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/Integer;",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;>;"
        }
    .end annotation
.end field

.field private mProgressReporter:Lcom/mediatek/datatransfer/ProgressReporter;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/mediatek/datatransfer/ProgressReporter;)V
    .locals 1
    .param p1    # Landroid/content/Context;
    .param p2    # Lcom/mediatek/datatransfer/ProgressReporter;

    const/4 v0, 0x0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-boolean v0, p0, Lcom/mediatek/datatransfer/BackupEngine;->mIsRunning:Z

    iput-boolean v0, p0, Lcom/mediatek/datatransfer/BackupEngine;->mIsPause:Z

    iput-boolean v0, p0, Lcom/mediatek/datatransfer/BackupEngine;->mIsCancel:Z

    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lcom/mediatek/datatransfer/BackupEngine;->mLock:Ljava/lang/Object;

    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/mediatek/datatransfer/BackupEngine;->mParasMap:Ljava/util/HashMap;

    iput-object p1, p0, Lcom/mediatek/datatransfer/BackupEngine;->mContext:Landroid/content/Context;

    iput-object p2, p0, Lcom/mediatek/datatransfer/BackupEngine;->mProgressReporter:Lcom/mediatek/datatransfer/ProgressReporter;

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/mediatek/datatransfer/BackupEngine;->mComposerList:Ljava/util/List;

    sput-object p0, Lcom/mediatek/datatransfer/BackupEngine;->mSelfInstance:Lcom/mediatek/datatransfer/BackupEngine;

    return-void
.end method

.method static synthetic access$100(Lcom/mediatek/datatransfer/BackupEngine;)Ljava/util/List;
    .locals 1
    .param p0    # Lcom/mediatek/datatransfer/BackupEngine;

    iget-object v0, p0, Lcom/mediatek/datatransfer/BackupEngine;->mComposerList:Ljava/util/List;

    return-object v0
.end method

.method static synthetic access$200(Lcom/mediatek/datatransfer/BackupEngine;)Z
    .locals 1
    .param p0    # Lcom/mediatek/datatransfer/BackupEngine;

    iget-boolean v0, p0, Lcom/mediatek/datatransfer/BackupEngine;->mIsPause:Z

    return v0
.end method

.method static synthetic access$300(Lcom/mediatek/datatransfer/BackupEngine;)Ljava/lang/Object;
    .locals 1
    .param p0    # Lcom/mediatek/datatransfer/BackupEngine;

    iget-object v0, p0, Lcom/mediatek/datatransfer/BackupEngine;->mLock:Ljava/lang/Object;

    return-object v0
.end method

.method static synthetic access$402(Lcom/mediatek/datatransfer/BackupEngine;Z)Z
    .locals 0
    .param p0    # Lcom/mediatek/datatransfer/BackupEngine;
    .param p1    # Z

    iput-boolean p1, p0, Lcom/mediatek/datatransfer/BackupEngine;->mIsRunning:Z

    return p1
.end method

.method static synthetic access$500(Lcom/mediatek/datatransfer/BackupEngine;)Z
    .locals 1
    .param p0    # Lcom/mediatek/datatransfer/BackupEngine;

    iget-boolean v0, p0, Lcom/mediatek/datatransfer/BackupEngine;->mIsCancel:Z

    return v0
.end method

.method static synthetic access$600(Lcom/mediatek/datatransfer/BackupEngine;)Ljava/lang/String;
    .locals 1
    .param p0    # Lcom/mediatek/datatransfer/BackupEngine;

    iget-object v0, p0, Lcom/mediatek/datatransfer/BackupEngine;->mBackupFolder:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$700(Lcom/mediatek/datatransfer/BackupEngine;Ljava/io/File;)V
    .locals 0
    .param p0    # Lcom/mediatek/datatransfer/BackupEngine;
    .param p1    # Ljava/io/File;

    invoke-direct {p0, p1}, Lcom/mediatek/datatransfer/BackupEngine;->deleteFolder(Ljava/io/File;)V

    return-void
.end method

.method static synthetic access$800(Lcom/mediatek/datatransfer/BackupEngine;)Lcom/mediatek/datatransfer/BackupEngine$OnBackupDoneListner;
    .locals 1
    .param p0    # Lcom/mediatek/datatransfer/BackupEngine;

    iget-object v0, p0, Lcom/mediatek/datatransfer/BackupEngine;->mBackupDoneListner:Lcom/mediatek/datatransfer/BackupEngine$OnBackupDoneListner;

    return-object v0
.end method

.method private addComposer(Lcom/mediatek/datatransfer/modules/Composer;)V
    .locals 4
    .param p1    # Lcom/mediatek/datatransfer/modules/Composer;

    if-eqz p1, :cond_1

    invoke-virtual {p1}, Lcom/mediatek/datatransfer/modules/Composer;->getModuleType()I

    move-result v1

    iget-object v2, p0, Lcom/mediatek/datatransfer/BackupEngine;->mParasMap:Ljava/util/HashMap;

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/ArrayList;

    if-eqz v0, :cond_0

    invoke-virtual {p1, v0}, Lcom/mediatek/datatransfer/modules/Composer;->setParams(Ljava/util/List;)V

    :cond_0
    iget-object v2, p0, Lcom/mediatek/datatransfer/BackupEngine;->mProgressReporter:Lcom/mediatek/datatransfer/ProgressReporter;

    invoke-virtual {p1, v2}, Lcom/mediatek/datatransfer/modules/Composer;->setReporter(Lcom/mediatek/datatransfer/ProgressReporter;)V

    iget-object v2, p0, Lcom/mediatek/datatransfer/BackupEngine;->mBackupFolder:Ljava/lang/String;

    invoke-virtual {p1, v2}, Lcom/mediatek/datatransfer/modules/Composer;->setParentFolderPath(Ljava/lang/String;)V

    iget-object v2, p0, Lcom/mediatek/datatransfer/BackupEngine;->mComposerList:Ljava/util/List;

    invoke-interface {v2, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :cond_1
    return-void
.end method

.method private deleteFolder(Ljava/io/File;)V
    .locals 3
    .param p1    # Ljava/io/File;

    invoke-virtual {p1}, Ljava/io/File;->exists()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-virtual {p1}, Ljava/io/File;->isFile()Z

    move-result v2

    if-eqz v2, :cond_2

    invoke-virtual {p1}, Ljava/io/File;->delete()Z

    :cond_0
    invoke-virtual {p1}, Ljava/io/File;->delete()Z

    :cond_1
    return-void

    :cond_2
    invoke-virtual {p1}, Ljava/io/File;->isDirectory()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-virtual {p1}, Ljava/io/File;->listFiles()[Ljava/io/File;

    move-result-object v0

    const/4 v1, 0x0

    :goto_0
    array-length v2, v0

    if-ge v1, v2, :cond_0

    aget-object v2, v0, v1

    invoke-direct {p0, v2}, Lcom/mediatek/datatransfer/BackupEngine;->deleteFolder(Ljava/io/File;)V

    add-int/lit8 v1, v1, 0x1

    goto :goto_0
.end method

.method public static getInstance(Landroid/content/Context;Lcom/mediatek/datatransfer/ProgressReporter;)Lcom/mediatek/datatransfer/BackupEngine;
    .locals 1
    .param p0    # Landroid/content/Context;
    .param p1    # Lcom/mediatek/datatransfer/ProgressReporter;

    sget-object v0, Lcom/mediatek/datatransfer/BackupEngine;->mSelfInstance:Lcom/mediatek/datatransfer/BackupEngine;

    if-nez v0, :cond_0

    new-instance v0, Lcom/mediatek/datatransfer/BackupEngine;

    invoke-direct {v0, p0, p1}, Lcom/mediatek/datatransfer/BackupEngine;-><init>(Landroid/content/Context;Lcom/mediatek/datatransfer/ProgressReporter;)V

    :goto_0
    sget-object v0, Lcom/mediatek/datatransfer/BackupEngine;->mSelfInstance:Lcom/mediatek/datatransfer/BackupEngine;

    return-object v0

    :cond_0
    sget-object v0, Lcom/mediatek/datatransfer/BackupEngine;->mSelfInstance:Lcom/mediatek/datatransfer/BackupEngine;

    invoke-direct {v0, p0, p1}, Lcom/mediatek/datatransfer/BackupEngine;->updateInfo(Landroid/content/Context;Lcom/mediatek/datatransfer/ProgressReporter;)V

    goto :goto_0
.end method

.method private reset()V
    .locals 2

    const/4 v1, 0x0

    iget-object v0, p0, Lcom/mediatek/datatransfer/BackupEngine;->mComposerList:Ljava/util/List;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/mediatek/datatransfer/BackupEngine;->mComposerList:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    :cond_0
    iget-object v0, p0, Lcom/mediatek/datatransfer/BackupEngine;->mParasMap:Ljava/util/HashMap;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/mediatek/datatransfer/BackupEngine;->mParasMap:Ljava/util/HashMap;

    invoke-virtual {v0}, Ljava/util/HashMap;->clear()V

    :cond_1
    iput-boolean v1, p0, Lcom/mediatek/datatransfer/BackupEngine;->mIsPause:Z

    iput-boolean v1, p0, Lcom/mediatek/datatransfer/BackupEngine;->mIsCancel:Z

    return-void
.end method

.method private setupComposer(Ljava/util/ArrayList;)Z
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/Integer;",
            ">;)Z"
        }
    .end annotation

    const-string v4, "B&R"

    const-string v5, "backupEnginesetupComposer begin..."

    invoke-static {v4, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v2, 0x1

    new-instance v1, Ljava/io/File;

    iget-object v4, p0, Lcom/mediatek/datatransfer/BackupEngine;->mBackupFolder:Ljava/lang/String;

    invoke-direct {v1, v4}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1}, Ljava/io/File;->exists()Z

    move-result v4

    if-nez v4, :cond_0

    invoke-virtual {v1}, Ljava/io/File;->mkdirs()Z

    move-result v2

    :cond_0
    if-eqz v2, :cond_2

    const-string v4, "B&R"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "backupEnginecreate folder "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget-object v6, p0, Lcom/mediatek/datatransfer/BackupEngine;->mBackupFolder:Ljava/lang/String;

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, " success"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    invoke-virtual {p1}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/Integer;

    invoke-virtual {v4}, Ljava/lang/Integer;->intValue()I

    move-result v3

    sparse-switch v3, :sswitch_data_0

    const/4 v2, 0x0

    goto :goto_0

    :sswitch_0
    new-instance v4, Lcom/mediatek/datatransfer/modules/ContactBackupComposer;

    iget-object v5, p0, Lcom/mediatek/datatransfer/BackupEngine;->mContext:Landroid/content/Context;

    invoke-direct {v4, v5}, Lcom/mediatek/datatransfer/modules/ContactBackupComposer;-><init>(Landroid/content/Context;)V

    invoke-direct {p0, v4}, Lcom/mediatek/datatransfer/BackupEngine;->addComposer(Lcom/mediatek/datatransfer/modules/Composer;)V

    goto :goto_0

    :sswitch_1
    new-instance v4, Lcom/mediatek/datatransfer/modules/CalendarBackupComposer;

    iget-object v5, p0, Lcom/mediatek/datatransfer/BackupEngine;->mContext:Landroid/content/Context;

    invoke-direct {v4, v5}, Lcom/mediatek/datatransfer/modules/CalendarBackupComposer;-><init>(Landroid/content/Context;)V

    invoke-direct {p0, v4}, Lcom/mediatek/datatransfer/BackupEngine;->addComposer(Lcom/mediatek/datatransfer/modules/Composer;)V

    goto :goto_0

    :sswitch_2
    new-instance v4, Lcom/mediatek/datatransfer/modules/SmsBackupComposer;

    iget-object v5, p0, Lcom/mediatek/datatransfer/BackupEngine;->mContext:Landroid/content/Context;

    invoke-direct {v4, v5}, Lcom/mediatek/datatransfer/modules/SmsBackupComposer;-><init>(Landroid/content/Context;)V

    invoke-direct {p0, v4}, Lcom/mediatek/datatransfer/BackupEngine;->addComposer(Lcom/mediatek/datatransfer/modules/Composer;)V

    goto :goto_0

    :sswitch_3
    new-instance v4, Lcom/mediatek/datatransfer/modules/MmsBackupComposer;

    iget-object v5, p0, Lcom/mediatek/datatransfer/BackupEngine;->mContext:Landroid/content/Context;

    invoke-direct {v4, v5}, Lcom/mediatek/datatransfer/modules/MmsBackupComposer;-><init>(Landroid/content/Context;)V

    invoke-direct {p0, v4}, Lcom/mediatek/datatransfer/BackupEngine;->addComposer(Lcom/mediatek/datatransfer/modules/Composer;)V

    goto :goto_0

    :sswitch_4
    new-instance v4, Lcom/mediatek/datatransfer/modules/MessageBackupComposer;

    iget-object v5, p0, Lcom/mediatek/datatransfer/BackupEngine;->mContext:Landroid/content/Context;

    invoke-direct {v4, v5}, Lcom/mediatek/datatransfer/modules/MessageBackupComposer;-><init>(Landroid/content/Context;)V

    invoke-direct {p0, v4}, Lcom/mediatek/datatransfer/BackupEngine;->addComposer(Lcom/mediatek/datatransfer/modules/Composer;)V

    goto :goto_0

    :sswitch_5
    new-instance v4, Lcom/mediatek/datatransfer/modules/AppBackupComposer;

    iget-object v5, p0, Lcom/mediatek/datatransfer/BackupEngine;->mContext:Landroid/content/Context;

    invoke-direct {v4, v5}, Lcom/mediatek/datatransfer/modules/AppBackupComposer;-><init>(Landroid/content/Context;)V

    invoke-direct {p0, v4}, Lcom/mediatek/datatransfer/BackupEngine;->addComposer(Lcom/mediatek/datatransfer/modules/Composer;)V

    goto :goto_0

    :sswitch_6
    new-instance v4, Lcom/mediatek/datatransfer/modules/PictureBackupComposer;

    iget-object v5, p0, Lcom/mediatek/datatransfer/BackupEngine;->mContext:Landroid/content/Context;

    invoke-direct {v4, v5}, Lcom/mediatek/datatransfer/modules/PictureBackupComposer;-><init>(Landroid/content/Context;)V

    invoke-direct {p0, v4}, Lcom/mediatek/datatransfer/BackupEngine;->addComposer(Lcom/mediatek/datatransfer/modules/Composer;)V

    goto :goto_0

    :sswitch_7
    new-instance v4, Lcom/mediatek/datatransfer/modules/MusicBackupComposer;

    iget-object v5, p0, Lcom/mediatek/datatransfer/BackupEngine;->mContext:Landroid/content/Context;

    invoke-direct {v4, v5}, Lcom/mediatek/datatransfer/modules/MusicBackupComposer;-><init>(Landroid/content/Context;)V

    invoke-direct {p0, v4}, Lcom/mediatek/datatransfer/BackupEngine;->addComposer(Lcom/mediatek/datatransfer/modules/Composer;)V

    goto :goto_0

    :sswitch_8
    new-instance v4, Lcom/mediatek/datatransfer/modules/NoteBookBackupComposer;

    iget-object v5, p0, Lcom/mediatek/datatransfer/BackupEngine;->mContext:Landroid/content/Context;

    invoke-direct {v4, v5}, Lcom/mediatek/datatransfer/modules/NoteBookBackupComposer;-><init>(Landroid/content/Context;)V

    invoke-direct {p0, v4}, Lcom/mediatek/datatransfer/BackupEngine;->addComposer(Lcom/mediatek/datatransfer/modules/Composer;)V

    goto :goto_0

    :sswitch_9
    new-instance v4, Lcom/mediatek/datatransfer/modules/BookmarkBackupComposer;

    iget-object v5, p0, Lcom/mediatek/datatransfer/BackupEngine;->mContext:Landroid/content/Context;

    invoke-direct {v4, v5}, Lcom/mediatek/datatransfer/modules/BookmarkBackupComposer;-><init>(Landroid/content/Context;)V

    invoke-direct {p0, v4}, Lcom/mediatek/datatransfer/BackupEngine;->addComposer(Lcom/mediatek/datatransfer/modules/Composer;)V

    goto/16 :goto_0

    :cond_1
    const-string v4, "B&R"

    const-string v5, "backupEnginesetupComposer finish"

    invoke-static {v4, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :goto_1
    return v2

    :cond_2
    const-string v4, "B&R"

    const-string v5, "backupEnginesetupComposer failed"

    invoke-static {v4, v5}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v2, 0x0

    goto :goto_1

    :sswitch_data_0
    .sparse-switch
        0x1 -> :sswitch_0
        0x2 -> :sswitch_2
        0x4 -> :sswitch_3
        0x8 -> :sswitch_1
        0x10 -> :sswitch_5
        0x20 -> :sswitch_6
        0x40 -> :sswitch_4
        0x80 -> :sswitch_7
        0x100 -> :sswitch_8
        0x200 -> :sswitch_9
    .end sparse-switch
.end method

.method private final updateInfo(Landroid/content/Context;Lcom/mediatek/datatransfer/ProgressReporter;)V
    .locals 0
    .param p1    # Landroid/content/Context;
    .param p2    # Lcom/mediatek/datatransfer/ProgressReporter;

    iput-object p1, p0, Lcom/mediatek/datatransfer/BackupEngine;->mContext:Landroid/content/Context;

    iput-object p2, p0, Lcom/mediatek/datatransfer/BackupEngine;->mProgressReporter:Lcom/mediatek/datatransfer/ProgressReporter;

    return-void
.end method


# virtual methods
.method public final cancel()V
    .locals 4

    const/4 v3, 0x1

    iget-object v2, p0, Lcom/mediatek/datatransfer/BackupEngine;->mComposerList:Ljava/util/List;

    if-eqz v2, :cond_1

    iget-object v2, p0, Lcom/mediatek/datatransfer/BackupEngine;->mComposerList:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    if-lez v2, :cond_1

    iget-object v2, p0, Lcom/mediatek/datatransfer/BackupEngine;->mComposerList:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/mediatek/datatransfer/modules/Composer;

    invoke-virtual {v0, v3}, Lcom/mediatek/datatransfer/modules/Composer;->setCancel(Z)V

    goto :goto_0

    :cond_0
    iput-boolean v3, p0, Lcom/mediatek/datatransfer/BackupEngine;->mIsCancel:Z

    invoke-virtual {p0}, Lcom/mediatek/datatransfer/BackupEngine;->continueBackup()V

    :cond_1
    return-void
.end method

.method public final continueBackup()V
    .locals 2

    iget-boolean v0, p0, Lcom/mediatek/datatransfer/BackupEngine;->mIsPause:Z

    if-eqz v0, :cond_0

    iget-object v1, p0, Lcom/mediatek/datatransfer/BackupEngine;->mLock:Ljava/lang/Object;

    monitor-enter v1

    const/4 v0, 0x0

    :try_start_0
    iput-boolean v0, p0, Lcom/mediatek/datatransfer/BackupEngine;->mIsPause:Z

    iget-object v0, p0, Lcom/mediatek/datatransfer/BackupEngine;->mLock:Ljava/lang/Object;

    invoke-virtual {v0}, Ljava/lang/Object;->notify()V

    monitor-exit v1

    :cond_0
    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public final isPaused()Z
    .locals 1

    iget-boolean v0, p0, Lcom/mediatek/datatransfer/BackupEngine;->mIsPause:Z

    return v0
.end method

.method public final isRunning()Z
    .locals 1

    iget-boolean v0, p0, Lcom/mediatek/datatransfer/BackupEngine;->mIsRunning:Z

    return v0
.end method

.method public final pause()V
    .locals 1

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/mediatek/datatransfer/BackupEngine;->mIsPause:Z

    return-void
.end method

.method public setBackupItemParam(ILjava/util/ArrayList;)V
    .locals 2
    .param p1    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    iget-object v0, p0, Lcom/mediatek/datatransfer/BackupEngine;->mParasMap:Ljava/util/HashMap;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1, p2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    return-void
.end method

.method public setBackupModelList(Ljava/util/ArrayList;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/Integer;",
            ">;)V"
        }
    .end annotation

    invoke-direct {p0}, Lcom/mediatek/datatransfer/BackupEngine;->reset()V

    iput-object p1, p0, Lcom/mediatek/datatransfer/BackupEngine;->mModuleList:Ljava/util/ArrayList;

    return-void
.end method

.method public final setOnBackupDoneListner(Lcom/mediatek/datatransfer/BackupEngine$OnBackupDoneListner;)V
    .locals 0
    .param p1    # Lcom/mediatek/datatransfer/BackupEngine$OnBackupDoneListner;

    iput-object p1, p0, Lcom/mediatek/datatransfer/BackupEngine;->mBackupDoneListner:Lcom/mediatek/datatransfer/BackupEngine$OnBackupDoneListner;

    return-void
.end method

.method public startBackup(Ljava/lang/String;)Z
    .locals 4
    .param p1    # Ljava/lang/String;

    const/4 v0, 0x1

    iput-object p1, p0, Lcom/mediatek/datatransfer/BackupEngine;->mBackupFolder:Ljava/lang/String;

    const-string v1, "B&R"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "backupEnginestartBackup():"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v1, p0, Lcom/mediatek/datatransfer/BackupEngine;->mModuleList:Ljava/util/ArrayList;

    invoke-direct {p0, v1}, Lcom/mediatek/datatransfer/BackupEngine;->setupComposer(Ljava/util/ArrayList;)Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v1, 0x1

    iput-boolean v1, p0, Lcom/mediatek/datatransfer/BackupEngine;->mIsRunning:Z

    new-instance v1, Lcom/mediatek/datatransfer/BackupEngine$BackupThread;

    const/4 v2, 0x0

    invoke-direct {v1, p0, v2}, Lcom/mediatek/datatransfer/BackupEngine$BackupThread;-><init>(Lcom/mediatek/datatransfer/BackupEngine;Lcom/mediatek/datatransfer/BackupEngine$1;)V

    invoke-virtual {v1}, Ljava/lang/Thread;->start()V

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
