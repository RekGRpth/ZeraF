.class Lcom/mediatek/datatransfer/PersonalDataRestoreActivity$PersonalDataRestoreStatusListener;
.super Lcom/mediatek/datatransfer/AbstractRestoreActivity$NormalRestoreStatusListener;
.source "PersonalDataRestoreActivity.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/mediatek/datatransfer/PersonalDataRestoreActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "PersonalDataRestoreStatusListener"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/mediatek/datatransfer/PersonalDataRestoreActivity;


# direct methods
.method private constructor <init>(Lcom/mediatek/datatransfer/PersonalDataRestoreActivity;)V
    .locals 0

    iput-object p1, p0, Lcom/mediatek/datatransfer/PersonalDataRestoreActivity$PersonalDataRestoreStatusListener;->this$0:Lcom/mediatek/datatransfer/PersonalDataRestoreActivity;

    invoke-direct {p0, p1}, Lcom/mediatek/datatransfer/AbstractRestoreActivity$NormalRestoreStatusListener;-><init>(Lcom/mediatek/datatransfer/AbstractRestoreActivity;)V

    return-void
.end method

.method synthetic constructor <init>(Lcom/mediatek/datatransfer/PersonalDataRestoreActivity;Lcom/mediatek/datatransfer/PersonalDataRestoreActivity$1;)V
    .locals 0
    .param p1    # Lcom/mediatek/datatransfer/PersonalDataRestoreActivity;
    .param p2    # Lcom/mediatek/datatransfer/PersonalDataRestoreActivity$1;

    invoke-direct {p0, p1}, Lcom/mediatek/datatransfer/PersonalDataRestoreActivity$PersonalDataRestoreStatusListener;-><init>(Lcom/mediatek/datatransfer/PersonalDataRestoreActivity;)V

    return-void
.end method


# virtual methods
.method public onComposerChanged(II)V
    .locals 3
    .param p1    # I
    .param p2    # I

    iget-object v0, p0, Lcom/mediatek/datatransfer/PersonalDataRestoreActivity$PersonalDataRestoreStatusListener;->this$0:Lcom/mediatek/datatransfer/PersonalDataRestoreActivity;

    invoke-static {v0}, Lcom/mediatek/datatransfer/PersonalDataRestoreActivity;->access$600(Lcom/mediatek/datatransfer/PersonalDataRestoreActivity;)Ljava/lang/String;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "RestoreDetailActivity: onComposerChanged type = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v0, p0, Lcom/mediatek/datatransfer/PersonalDataRestoreActivity$PersonalDataRestoreStatusListener;->this$0:Lcom/mediatek/datatransfer/PersonalDataRestoreActivity;

    iget-object v0, v0, Lcom/mediatek/datatransfer/AbstractRestoreActivity;->mHandler:Landroid/os/Handler;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/mediatek/datatransfer/PersonalDataRestoreActivity$PersonalDataRestoreStatusListener;->this$0:Lcom/mediatek/datatransfer/PersonalDataRestoreActivity;

    iget-object v0, v0, Lcom/mediatek/datatransfer/AbstractRestoreActivity;->mHandler:Landroid/os/Handler;

    new-instance v1, Lcom/mediatek/datatransfer/PersonalDataRestoreActivity$PersonalDataRestoreStatusListener$1;

    invoke-direct {v1, p0, p1, p2}, Lcom/mediatek/datatransfer/PersonalDataRestoreActivity$PersonalDataRestoreStatusListener$1;-><init>(Lcom/mediatek/datatransfer/PersonalDataRestoreActivity$PersonalDataRestoreStatusListener;II)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    :cond_0
    return-void
.end method

.method public onRestoreEnd(ZLjava/util/ArrayList;)V
    .locals 13
    .param p1    # Z
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(Z",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/mediatek/datatransfer/ResultDialog$ResultEntity;",
            ">;)V"
        }
    .end annotation

    move-object v4, p2

    iget-object v11, p0, Lcom/mediatek/datatransfer/PersonalDataRestoreActivity$PersonalDataRestoreStatusListener;->this$0:Lcom/mediatek/datatransfer/PersonalDataRestoreActivity;

    invoke-static {v11}, Lcom/mediatek/datatransfer/PersonalDataRestoreActivity;->access$600(Lcom/mediatek/datatransfer/PersonalDataRestoreActivity;)Ljava/lang/String;

    move-result-object v11

    const-string v12, "onRestoreEnd"

    invoke-static {v11, v12}, Lcom/mediatek/datatransfer/utils/MyLogger;->logD(Ljava/lang/String;Ljava/lang/String;)V

    const/4 v2, 0x0

    invoke-virtual {p2}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v11

    if-eqz v11, :cond_1

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Lcom/mediatek/datatransfer/ResultDialog$ResultEntity;

    invoke-virtual {v9}, Lcom/mediatek/datatransfer/ResultDialog$ResultEntity;->getResult()I

    move-result v11

    if-nez v11, :cond_0

    const/4 v2, 0x1

    :cond_1
    if-eqz v2, :cond_6

    new-instance v11, Ljava/lang/StringBuilder;

    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v12, p0, Lcom/mediatek/datatransfer/PersonalDataRestoreActivity$PersonalDataRestoreStatusListener;->this$0:Lcom/mediatek/datatransfer/PersonalDataRestoreActivity;

    invoke-static {v12}, Lcom/mediatek/datatransfer/PersonalDataRestoreActivity;->access$1000(Lcom/mediatek/datatransfer/PersonalDataRestoreActivity;)Ljava/lang/String;

    move-result-object v12

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    sget-object v12, Ljava/io/File;->separator:Ljava/lang/String;

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    const-string v12, "record.xml"

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v7}, Lcom/mediatek/datatransfer/utils/Utils;->readFromFile(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    new-instance v6, Ljava/util/ArrayList;

    invoke-direct {v6}, Ljava/util/ArrayList;-><init>()V

    if-eqz v1, :cond_2

    invoke-virtual {v1}, Ljava/lang/String;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-static {v11}, Lcom/mediatek/datatransfer/RecordXmlParser;->parse(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v6

    :cond_2
    new-instance v10, Lcom/mediatek/datatransfer/RecordXmlComposer;

    invoke-direct {v10}, Lcom/mediatek/datatransfer/RecordXmlComposer;-><init>()V

    invoke-virtual {v10}, Lcom/mediatek/datatransfer/RecordXmlComposer;->startCompose()Z

    new-instance v8, Lcom/mediatek/datatransfer/RecordXmlInfo;

    invoke-direct {v8}, Lcom/mediatek/datatransfer/RecordXmlInfo;-><init>()V

    const/4 v11, 0x1

    invoke-virtual {v8, v11}, Lcom/mediatek/datatransfer/RecordXmlInfo;->setRestore(Z)V

    invoke-static {}, Lcom/mediatek/datatransfer/utils/Utils;->getPhoneSearialNumber()Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v8, v11}, Lcom/mediatek/datatransfer/RecordXmlInfo;->setDevice(Ljava/lang/String;)V

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v11

    invoke-static {v11, v12}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v8, v11}, Lcom/mediatek/datatransfer/RecordXmlInfo;->setTime(Ljava/lang/String;)V

    const/4 v0, 0x0

    invoke-virtual {v6}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v11

    if-eqz v11, :cond_4

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/mediatek/datatransfer/RecordXmlInfo;

    invoke-virtual {v5}, Lcom/mediatek/datatransfer/RecordXmlInfo;->getDevice()Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v8}, Lcom/mediatek/datatransfer/RecordXmlInfo;->getDevice()Ljava/lang/String;

    move-result-object v12

    invoke-virtual {v11, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_3

    invoke-virtual {v10, v8}, Lcom/mediatek/datatransfer/RecordXmlComposer;->addOneRecord(Lcom/mediatek/datatransfer/RecordXmlInfo;)Z

    const/4 v0, 0x1

    goto :goto_0

    :cond_3
    invoke-virtual {v10, v5}, Lcom/mediatek/datatransfer/RecordXmlComposer;->addOneRecord(Lcom/mediatek/datatransfer/RecordXmlInfo;)Z

    goto :goto_0

    :cond_4
    if-nez v0, :cond_5

    invoke-virtual {v10, v8}, Lcom/mediatek/datatransfer/RecordXmlComposer;->addOneRecord(Lcom/mediatek/datatransfer/RecordXmlInfo;)Z

    :cond_5
    invoke-virtual {v10}, Lcom/mediatek/datatransfer/RecordXmlComposer;->endCompose()Z

    invoke-virtual {v10}, Lcom/mediatek/datatransfer/RecordXmlComposer;->getXmlInfo()Ljava/lang/String;

    move-result-object v11

    invoke-static {v11, v7}, Lcom/mediatek/datatransfer/utils/Utils;->writeToFile(Ljava/lang/String;Ljava/lang/String;)V

    :cond_6
    iget-object v11, p0, Lcom/mediatek/datatransfer/PersonalDataRestoreActivity$PersonalDataRestoreStatusListener;->this$0:Lcom/mediatek/datatransfer/PersonalDataRestoreActivity;

    iget-object v11, v11, Lcom/mediatek/datatransfer/AbstractRestoreActivity;->mHandler:Landroid/os/Handler;

    if-eqz v11, :cond_7

    iget-object v11, p0, Lcom/mediatek/datatransfer/PersonalDataRestoreActivity$PersonalDataRestoreStatusListener;->this$0:Lcom/mediatek/datatransfer/PersonalDataRestoreActivity;

    iget-object v11, v11, Lcom/mediatek/datatransfer/AbstractRestoreActivity;->mHandler:Landroid/os/Handler;

    new-instance v12, Lcom/mediatek/datatransfer/PersonalDataRestoreActivity$PersonalDataRestoreStatusListener$2;

    invoke-direct {v12, p0, v4}, Lcom/mediatek/datatransfer/PersonalDataRestoreActivity$PersonalDataRestoreStatusListener$2;-><init>(Lcom/mediatek/datatransfer/PersonalDataRestoreActivity$PersonalDataRestoreStatusListener;Ljava/util/ArrayList;)V

    invoke-virtual {v11, v12}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    :cond_7
    return-void
.end method
