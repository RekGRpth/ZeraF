.class public Lcom/mediatek/datatransfer/RecordXmlComposer;
.super Ljava/lang/Object;
.source "RecordXmlComposer.java"


# instance fields
.field private mSerializer:Lorg/xmlpull/v1/XmlSerializer;

.field private mStringWriter:Ljava/io/StringWriter;


# direct methods
.method public constructor <init>()V
    .locals 1

    const/4 v0, 0x0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lcom/mediatek/datatransfer/RecordXmlComposer;->mSerializer:Lorg/xmlpull/v1/XmlSerializer;

    iput-object v0, p0, Lcom/mediatek/datatransfer/RecordXmlComposer;->mStringWriter:Ljava/io/StringWriter;

    return-void
.end method


# virtual methods
.method public addOneRecord(Lcom/mediatek/datatransfer/RecordXmlInfo;)Z
    .locals 6
    .param p1    # Lcom/mediatek/datatransfer/RecordXmlInfo;

    const/4 v1, 0x0

    :try_start_0
    invoke-virtual {p1}, Lcom/mediatek/datatransfer/RecordXmlInfo;->isRestore()Z

    move-result v2

    if-eqz v2, :cond_2

    iget-object v2, p0, Lcom/mediatek/datatransfer/RecordXmlComposer;->mSerializer:Lorg/xmlpull/v1/XmlSerializer;

    const-string v3, ""

    const-string v4, "restore"

    invoke-interface {v2, v3, v4}, Lorg/xmlpull/v1/XmlSerializer;->startTag(Ljava/lang/String;Ljava/lang/String;)Lorg/xmlpull/v1/XmlSerializer;

    :goto_0
    invoke-virtual {p1}, Lcom/mediatek/datatransfer/RecordXmlInfo;->getDevice()Ljava/lang/String;

    move-result-object v2

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/mediatek/datatransfer/RecordXmlComposer;->mSerializer:Lorg/xmlpull/v1/XmlSerializer;

    const-string v3, ""

    const-string v4, "device"

    invoke-virtual {p1}, Lcom/mediatek/datatransfer/RecordXmlInfo;->getDevice()Ljava/lang/String;

    move-result-object v5

    invoke-interface {v2, v3, v4, v5}, Lorg/xmlpull/v1/XmlSerializer;->attribute(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lorg/xmlpull/v1/XmlSerializer;

    :cond_0
    invoke-virtual {p1}, Lcom/mediatek/datatransfer/RecordXmlInfo;->getTime()Ljava/lang/String;

    move-result-object v2

    if-eqz v2, :cond_1

    iget-object v2, p0, Lcom/mediatek/datatransfer/RecordXmlComposer;->mSerializer:Lorg/xmlpull/v1/XmlSerializer;

    const-string v3, ""

    const-string v4, "time"

    invoke-virtual {p1}, Lcom/mediatek/datatransfer/RecordXmlInfo;->getTime()Ljava/lang/String;

    move-result-object v5

    invoke-interface {v2, v3, v4, v5}, Lorg/xmlpull/v1/XmlSerializer;->attribute(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lorg/xmlpull/v1/XmlSerializer;

    :cond_1
    invoke-virtual {p1}, Lcom/mediatek/datatransfer/RecordXmlInfo;->isRestore()Z

    move-result v2

    if-eqz v2, :cond_3

    iget-object v2, p0, Lcom/mediatek/datatransfer/RecordXmlComposer;->mSerializer:Lorg/xmlpull/v1/XmlSerializer;

    const-string v3, ""

    const-string v4, "restore"

    invoke-interface {v2, v3, v4}, Lorg/xmlpull/v1/XmlSerializer;->endTag(Ljava/lang/String;Ljava/lang/String;)Lorg/xmlpull/v1/XmlSerializer;

    :goto_1
    const/4 v1, 0x1

    :goto_2
    return v1

    :cond_2
    iget-object v2, p0, Lcom/mediatek/datatransfer/RecordXmlComposer;->mSerializer:Lorg/xmlpull/v1/XmlSerializer;

    const-string v3, ""

    const-string v4, "backup"

    invoke-interface {v2, v3, v4}, Lorg/xmlpull/v1/XmlSerializer;->startTag(Ljava/lang/String;Ljava/lang/String;)Lorg/xmlpull/v1/XmlSerializer;
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/IllegalStateException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_2

    goto :goto_0

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Ljava/lang/Throwable;->printStackTrace()V

    goto :goto_2

    :cond_3
    :try_start_1
    iget-object v2, p0, Lcom/mediatek/datatransfer/RecordXmlComposer;->mSerializer:Lorg/xmlpull/v1/XmlSerializer;

    const-string v3, ""

    const-string v4, "backup"

    invoke-interface {v2, v3, v4}, Lorg/xmlpull/v1/XmlSerializer;->endTag(Ljava/lang/String;Ljava/lang/String;)Lorg/xmlpull/v1/XmlSerializer;
    :try_end_1
    .catch Ljava/lang/IllegalArgumentException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Ljava/lang/IllegalStateException; {:try_start_1 .. :try_end_1} :catch_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_2

    goto :goto_1

    :catch_1
    move-exception v0

    invoke-virtual {v0}, Ljava/lang/Throwable;->printStackTrace()V

    goto :goto_2

    :catch_2
    move-exception v0

    invoke-virtual {v0}, Ljava/lang/Throwable;->printStackTrace()V

    goto :goto_2
.end method

.method public endCompose()Z
    .locals 5

    const/4 v1, 0x0

    :try_start_0
    iget-object v2, p0, Lcom/mediatek/datatransfer/RecordXmlComposer;->mSerializer:Lorg/xmlpull/v1/XmlSerializer;

    const-string v3, ""

    const-string v4, "root"

    invoke-interface {v2, v3, v4}, Lorg/xmlpull/v1/XmlSerializer;->endTag(Ljava/lang/String;Ljava/lang/String;)Lorg/xmlpull/v1/XmlSerializer;

    iget-object v2, p0, Lcom/mediatek/datatransfer/RecordXmlComposer;->mSerializer:Lorg/xmlpull/v1/XmlSerializer;

    invoke-interface {v2}, Lorg/xmlpull/v1/XmlSerializer;->endDocument()V
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/IllegalStateException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_2

    const/4 v1, 0x1

    :goto_0
    return v1

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Ljava/lang/Throwable;->printStackTrace()V

    goto :goto_0

    :catch_1
    move-exception v0

    invoke-virtual {v0}, Ljava/lang/Throwable;->printStackTrace()V

    goto :goto_0

    :catch_2
    move-exception v0

    invoke-virtual {v0}, Ljava/lang/Throwable;->printStackTrace()V

    goto :goto_0
.end method

.method public getXmlInfo()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/mediatek/datatransfer/RecordXmlComposer;->mStringWriter:Ljava/io/StringWriter;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/mediatek/datatransfer/RecordXmlComposer;->mStringWriter:Ljava/io/StringWriter;

    invoke-virtual {v0}, Ljava/io/StringWriter;->toString()Ljava/lang/String;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public startCompose()Z
    .locals 5

    const/4 v1, 0x0

    invoke-static {}, Landroid/util/Xml;->newSerializer()Lorg/xmlpull/v1/XmlSerializer;

    move-result-object v2

    iput-object v2, p0, Lcom/mediatek/datatransfer/RecordXmlComposer;->mSerializer:Lorg/xmlpull/v1/XmlSerializer;

    new-instance v2, Ljava/io/StringWriter;

    invoke-direct {v2}, Ljava/io/StringWriter;-><init>()V

    iput-object v2, p0, Lcom/mediatek/datatransfer/RecordXmlComposer;->mStringWriter:Ljava/io/StringWriter;

    :try_start_0
    iget-object v2, p0, Lcom/mediatek/datatransfer/RecordXmlComposer;->mSerializer:Lorg/xmlpull/v1/XmlSerializer;

    iget-object v3, p0, Lcom/mediatek/datatransfer/RecordXmlComposer;->mStringWriter:Ljava/io/StringWriter;

    invoke-interface {v2, v3}, Lorg/xmlpull/v1/XmlSerializer;->setOutput(Ljava/io/Writer;)V

    iget-object v2, p0, Lcom/mediatek/datatransfer/RecordXmlComposer;->mSerializer:Lorg/xmlpull/v1/XmlSerializer;

    const/4 v3, 0x0

    const/4 v4, 0x0

    invoke-static {v4}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v4

    invoke-interface {v2, v3, v4}, Lorg/xmlpull/v1/XmlSerializer;->startDocument(Ljava/lang/String;Ljava/lang/Boolean;)V

    iget-object v2, p0, Lcom/mediatek/datatransfer/RecordXmlComposer;->mSerializer:Lorg/xmlpull/v1/XmlSerializer;

    const-string v3, ""

    const-string v4, "root"

    invoke-interface {v2, v3, v4}, Lorg/xmlpull/v1/XmlSerializer;->startTag(Ljava/lang/String;Ljava/lang/String;)Lorg/xmlpull/v1/XmlSerializer;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    const/4 v1, 0x1

    :goto_0
    return v1

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Ljava/lang/Throwable;->printStackTrace()V

    goto :goto_0
.end method
