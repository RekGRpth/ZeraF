.class public Lcom/mediatek/datatransfer/PersonalDataRestoreActivity;
.super Lcom/mediatek/datatransfer/AbstractRestoreActivity;
.source "PersonalDataRestoreActivity.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/mediatek/datatransfer/PersonalDataRestoreActivity$PersonalDataRestoreStatusListener;,
        Lcom/mediatek/datatransfer/PersonalDataRestoreActivity$FilePreviewTask;,
        Lcom/mediatek/datatransfer/PersonalDataRestoreActivity$PersonalDataRestoreAdapter;
    }
.end annotation


# instance fields
.field private CLASS_TAG:Ljava/lang/String;

.field private mFile:Ljava/io/File;

.field private mIsCheckedRestoreStatus:Z

.field private mIsDataInitialed:Z

.field private mIsStoped:Z

.field private mNeedUpdateResult:Z

.field mPreview:Lcom/mediatek/datatransfer/utils/BackupFilePreview;

.field private mRestoreAdapter:Lcom/mediatek/datatransfer/PersonalDataRestoreActivity$PersonalDataRestoreAdapter;

.field private mRestoreFolderPath:Ljava/lang/String;

.field private mRestoreStoreStatusListener:Lcom/mediatek/datatransfer/PersonalDataRestoreActivity$PersonalDataRestoreStatusListener;


# direct methods
.method public constructor <init>()V
    .locals 2

    const/4 v1, 0x0

    invoke-direct {p0}, Lcom/mediatek/datatransfer/AbstractRestoreActivity;-><init>()V

    const-string v0, "DataTransfer/PersonalDataRestoreActivity"

    iput-object v0, p0, Lcom/mediatek/datatransfer/PersonalDataRestoreActivity;->CLASS_TAG:Ljava/lang/String;

    iput-boolean v1, p0, Lcom/mediatek/datatransfer/PersonalDataRestoreActivity;->mIsStoped:Z

    iput-boolean v1, p0, Lcom/mediatek/datatransfer/PersonalDataRestoreActivity;->mNeedUpdateResult:Z

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/mediatek/datatransfer/PersonalDataRestoreActivity;->mPreview:Lcom/mediatek/datatransfer/utils/BackupFilePreview;

    iput-boolean v1, p0, Lcom/mediatek/datatransfer/PersonalDataRestoreActivity;->mIsCheckedRestoreStatus:Z

    return-void
.end method

.method static synthetic access$100(Lcom/mediatek/datatransfer/PersonalDataRestoreActivity;)Lcom/mediatek/datatransfer/PersonalDataRestoreActivity$PersonalDataRestoreAdapter;
    .locals 1
    .param p0    # Lcom/mediatek/datatransfer/PersonalDataRestoreActivity;

    iget-object v0, p0, Lcom/mediatek/datatransfer/PersonalDataRestoreActivity;->mRestoreAdapter:Lcom/mediatek/datatransfer/PersonalDataRestoreActivity$PersonalDataRestoreAdapter;

    return-object v0
.end method

.method static synthetic access$1000(Lcom/mediatek/datatransfer/PersonalDataRestoreActivity;)Ljava/lang/String;
    .locals 1
    .param p0    # Lcom/mediatek/datatransfer/PersonalDataRestoreActivity;

    iget-object v0, p0, Lcom/mediatek/datatransfer/PersonalDataRestoreActivity;->mRestoreFolderPath:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$1100(Lcom/mediatek/datatransfer/PersonalDataRestoreActivity;)Z
    .locals 1
    .param p0    # Lcom/mediatek/datatransfer/PersonalDataRestoreActivity;

    iget-boolean v0, p0, Lcom/mediatek/datatransfer/PersonalDataRestoreActivity;->mIsStoped:Z

    return v0
.end method

.method static synthetic access$1202(Lcom/mediatek/datatransfer/PersonalDataRestoreActivity;Z)Z
    .locals 0
    .param p0    # Lcom/mediatek/datatransfer/PersonalDataRestoreActivity;
    .param p1    # Z

    iput-boolean p1, p0, Lcom/mediatek/datatransfer/PersonalDataRestoreActivity;->mNeedUpdateResult:Z

    return p1
.end method

.method static synthetic access$1300(Lcom/mediatek/datatransfer/PersonalDataRestoreActivity;Ljava/util/ArrayList;)V
    .locals 0
    .param p0    # Lcom/mediatek/datatransfer/PersonalDataRestoreActivity;
    .param p1    # Ljava/util/ArrayList;

    invoke-direct {p0, p1}, Lcom/mediatek/datatransfer/PersonalDataRestoreActivity;->showRestoreResult(Ljava/util/ArrayList;)V

    return-void
.end method

.method static synthetic access$202(Lcom/mediatek/datatransfer/PersonalDataRestoreActivity;Z)Z
    .locals 0
    .param p0    # Lcom/mediatek/datatransfer/PersonalDataRestoreActivity;
    .param p1    # Z

    iput-boolean p1, p0, Lcom/mediatek/datatransfer/PersonalDataRestoreActivity;->mIsDataInitialed:Z

    return p1
.end method

.method static synthetic access$300(Lcom/mediatek/datatransfer/PersonalDataRestoreActivity;)V
    .locals 0
    .param p0    # Lcom/mediatek/datatransfer/PersonalDataRestoreActivity;

    invoke-direct {p0}, Lcom/mediatek/datatransfer/PersonalDataRestoreActivity;->initActionBar()V

    return-void
.end method

.method static synthetic access$400(Lcom/mediatek/datatransfer/PersonalDataRestoreActivity;)Lcom/mediatek/datatransfer/PersonalDataRestoreActivity$PersonalDataRestoreStatusListener;
    .locals 1
    .param p0    # Lcom/mediatek/datatransfer/PersonalDataRestoreActivity;

    iget-object v0, p0, Lcom/mediatek/datatransfer/PersonalDataRestoreActivity;->mRestoreStoreStatusListener:Lcom/mediatek/datatransfer/PersonalDataRestoreActivity$PersonalDataRestoreStatusListener;

    return-object v0
.end method

.method static synthetic access$402(Lcom/mediatek/datatransfer/PersonalDataRestoreActivity;Lcom/mediatek/datatransfer/PersonalDataRestoreActivity$PersonalDataRestoreStatusListener;)Lcom/mediatek/datatransfer/PersonalDataRestoreActivity$PersonalDataRestoreStatusListener;
    .locals 0
    .param p0    # Lcom/mediatek/datatransfer/PersonalDataRestoreActivity;
    .param p1    # Lcom/mediatek/datatransfer/PersonalDataRestoreActivity$PersonalDataRestoreStatusListener;

    iput-object p1, p0, Lcom/mediatek/datatransfer/PersonalDataRestoreActivity;->mRestoreStoreStatusListener:Lcom/mediatek/datatransfer/PersonalDataRestoreActivity$PersonalDataRestoreStatusListener;

    return-object p1
.end method

.method static synthetic access$600(Lcom/mediatek/datatransfer/PersonalDataRestoreActivity;)Ljava/lang/String;
    .locals 1
    .param p0    # Lcom/mediatek/datatransfer/PersonalDataRestoreActivity;

    iget-object v0, p0, Lcom/mediatek/datatransfer/PersonalDataRestoreActivity;->CLASS_TAG:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$700(Lcom/mediatek/datatransfer/PersonalDataRestoreActivity;)V
    .locals 0
    .param p0    # Lcom/mediatek/datatransfer/PersonalDataRestoreActivity;

    invoke-direct {p0}, Lcom/mediatek/datatransfer/PersonalDataRestoreActivity;->checkRestoreState()V

    return-void
.end method

.method static synthetic access$800(Lcom/mediatek/datatransfer/PersonalDataRestoreActivity;)Ljava/io/File;
    .locals 1
    .param p0    # Lcom/mediatek/datatransfer/PersonalDataRestoreActivity;

    iget-object v0, p0, Lcom/mediatek/datatransfer/PersonalDataRestoreActivity;->mFile:Ljava/io/File;

    return-object v0
.end method

.method static synthetic access$900(Lcom/mediatek/datatransfer/PersonalDataRestoreActivity;I)Ljava/lang/String;
    .locals 1
    .param p0    # Lcom/mediatek/datatransfer/PersonalDataRestoreActivity;
    .param p1    # I

    invoke-direct {p0, p1}, Lcom/mediatek/datatransfer/PersonalDataRestoreActivity;->getProgressDlgMessage(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private checkRestoreState()V
    .locals 7

    const/4 v6, 0x1

    iget-boolean v3, p0, Lcom/mediatek/datatransfer/PersonalDataRestoreActivity;->mIsCheckedRestoreStatus:Z

    if-eqz v3, :cond_1

    iget-object v3, p0, Lcom/mediatek/datatransfer/PersonalDataRestoreActivity;->CLASS_TAG:Ljava/lang/String;

    const-string v4, "can not checkRestoreState, as it has been checked"

    invoke-static {v3, v4}, Lcom/mediatek/datatransfer/utils/MyLogger;->logD(Ljava/lang/String;Ljava/lang/String;)V

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-boolean v3, p0, Lcom/mediatek/datatransfer/PersonalDataRestoreActivity;->mIsDataInitialed:Z

    if-nez v3, :cond_2

    iget-object v3, p0, Lcom/mediatek/datatransfer/PersonalDataRestoreActivity;->CLASS_TAG:Ljava/lang/String;

    const-string v4, "can not checkRestoreState, wait data to initialed"

    invoke-static {v3, v4}, Lcom/mediatek/datatransfer/utils/MyLogger;->logD(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    :cond_2
    iget-object v3, p0, Lcom/mediatek/datatransfer/PersonalDataRestoreActivity;->CLASS_TAG:Ljava/lang/String;

    const-string v4, "all ready. to checkRestoreState"

    invoke-static {v3, v4}, Lcom/mediatek/datatransfer/utils/MyLogger;->logD(Ljava/lang/String;Ljava/lang/String;)V

    iput-boolean v6, p0, Lcom/mediatek/datatransfer/PersonalDataRestoreActivity;->mIsCheckedRestoreStatus:Z

    iget-object v3, p0, Lcom/mediatek/datatransfer/AbstractRestoreActivity;->mRestoreService:Lcom/mediatek/datatransfer/RestoreService$RestoreBinder;

    if-eqz v3, :cond_0

    iget-object v3, p0, Lcom/mediatek/datatransfer/AbstractRestoreActivity;->mRestoreService:Lcom/mediatek/datatransfer/RestoreService$RestoreBinder;

    invoke-virtual {v3}, Lcom/mediatek/datatransfer/RestoreService$RestoreBinder;->getState()I

    move-result v2

    iget-object v3, p0, Lcom/mediatek/datatransfer/PersonalDataRestoreActivity;->CLASS_TAG:Ljava/lang/String;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "checkRestoreState: state = "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/mediatek/datatransfer/utils/MyLogger;->logD(Ljava/lang/String;Ljava/lang/String;)V

    if-eq v2, v6, :cond_3

    const/4 v3, 0x2

    if-ne v2, v3, :cond_5

    :cond_3
    iget-object v3, p0, Lcom/mediatek/datatransfer/AbstractRestoreActivity;->mRestoreService:Lcom/mediatek/datatransfer/RestoreService$RestoreBinder;

    invoke-virtual {v3}, Lcom/mediatek/datatransfer/RestoreService$RestoreBinder;->getCurRestoreProgress()Lcom/mediatek/datatransfer/RestoreService$RestoreProgress;

    move-result-object v1

    iget-object v3, p0, Lcom/mediatek/datatransfer/PersonalDataRestoreActivity;->CLASS_TAG:Ljava/lang/String;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "checkRestoreState: Max = "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget v5, v1, Lcom/mediatek/datatransfer/RestoreService$RestoreProgress;->mMax:I

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " curprogress = "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget v5, v1, Lcom/mediatek/datatransfer/RestoreService$RestoreProgress;->mCurNum:I

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/mediatek/datatransfer/utils/MyLogger;->logD(Ljava/lang/String;Ljava/lang/String;)V

    iget v3, v1, Lcom/mediatek/datatransfer/RestoreService$RestoreProgress;->mType:I

    invoke-direct {p0, v3}, Lcom/mediatek/datatransfer/PersonalDataRestoreActivity;->getProgressDlgMessage(I)Ljava/lang/String;

    move-result-object v0

    if-ne v2, v6, :cond_4

    invoke-virtual {p0}, Lcom/mediatek/datatransfer/AbstractRestoreActivity;->showProgressDialog()V

    :cond_4
    iget v3, v1, Lcom/mediatek/datatransfer/RestoreService$RestoreProgress;->mMax:I

    invoke-virtual {p0, v3}, Lcom/mediatek/datatransfer/AbstractRestoreActivity;->setProgressDialogMax(I)V

    iget v3, v1, Lcom/mediatek/datatransfer/RestoreService$RestoreProgress;->mCurNum:I

    invoke-virtual {p0, v3}, Lcom/mediatek/datatransfer/AbstractRestoreActivity;->setProgressDialogProgress(I)V

    invoke-virtual {p0, v0}, Lcom/mediatek/datatransfer/AbstractRestoreActivity;->setProgressDialogMessage(Ljava/lang/CharSequence;)V

    goto/16 :goto_0

    :cond_5
    const/4 v3, 0x5

    if-ne v2, v3, :cond_7

    iget-boolean v3, p0, Lcom/mediatek/datatransfer/PersonalDataRestoreActivity;->mIsStoped:Z

    if-eqz v3, :cond_6

    iput-boolean v6, p0, Lcom/mediatek/datatransfer/PersonalDataRestoreActivity;->mNeedUpdateResult:Z

    goto/16 :goto_0

    :cond_6
    iget-object v3, p0, Lcom/mediatek/datatransfer/AbstractRestoreActivity;->mRestoreService:Lcom/mediatek/datatransfer/RestoreService$RestoreBinder;

    invoke-virtual {v3}, Lcom/mediatek/datatransfer/RestoreService$RestoreBinder;->getRestoreResult()Ljava/util/ArrayList;

    move-result-object v3

    invoke-direct {p0, v3}, Lcom/mediatek/datatransfer/PersonalDataRestoreActivity;->showRestoreResult(Ljava/util/ArrayList;)V

    goto/16 :goto_0

    :cond_7
    const/4 v3, 0x6

    if-ne v2, v3, :cond_0

    invoke-virtual {p0}, Lcom/mediatek/datatransfer/AbstractRestoreActivity;->errChecked()Z

    goto/16 :goto_0
.end method

.method private getProgressDlgMessage(I)Ljava/lang/String;
    .locals 3
    .param p1    # I

    new-instance v0, Ljava/lang/StringBuilder;

    const v1, 0x7f06000f

    invoke-virtual {p0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v1, "("

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-static {p0, p1}, Lcom/mediatek/datatransfer/utils/ModuleType;->getModuleStringFromType(Landroid/content/Context;I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ")"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    return-object v1
.end method

.method private getSelectedItemList()Ljava/util/ArrayList;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation

    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    invoke-virtual {p0}, Lcom/mediatek/datatransfer/CheckedListActivity;->getCount()I

    move-result v0

    const/4 v3, 0x0

    :goto_0
    if-ge v3, v0, :cond_1

    invoke-virtual {p0, v3}, Lcom/mediatek/datatransfer/CheckedListActivity;->getItemByPosition(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/mediatek/datatransfer/PersonalItemData;

    invoke-virtual {p0, v3}, Lcom/mediatek/datatransfer/CheckedListActivity;->isItemCheckedByPosition(I)Z

    move-result v4

    if-eqz v4, :cond_0

    invoke-virtual {v1}, Lcom/mediatek/datatransfer/PersonalItemData;->getType()I

    move-result v4

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v2, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :cond_0
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    :cond_1
    return-object v2
.end method

.method private init()V
    .locals 0

    invoke-direct {p0}, Lcom/mediatek/datatransfer/PersonalDataRestoreActivity;->initActionBar()V

    invoke-virtual {p0}, Lcom/mediatek/datatransfer/PersonalDataRestoreActivity;->updateTitle()V

    return-void
.end method

.method private initActionBar()V
    .locals 5

    invoke-virtual {p0}, Landroid/app/Activity;->getActionBar()Landroid/app/ActionBar;

    move-result-object v0

    const/4 v4, 0x0

    invoke-virtual {v0, v4}, Landroid/app/ActionBar;->setDisplayShowHomeEnabled(Z)V

    iget-object v4, p0, Lcom/mediatek/datatransfer/PersonalDataRestoreActivity;->mFile:Ljava/io/File;

    invoke-virtual {v4}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0, v4}, Landroid/app/ActionBar;->setTitle(Ljava/lang/CharSequence;)V

    new-instance v1, Ljava/lang/StringBuilder;

    const v4, 0x7f060018

    invoke-virtual {p0, v4}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-direct {v1, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v4, " "

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v4, p0, Lcom/mediatek/datatransfer/PersonalDataRestoreActivity;->mFile:Ljava/io/File;

    invoke-static {v4}, Lcom/mediatek/datatransfer/utils/FileUtils;->computeAllFileSizeInFolder(Ljava/io/File;)J

    move-result-wide v2

    invoke-static {v2, v3, p0}, Lcom/mediatek/datatransfer/utils/FileUtils;->getDisplaySize(JLandroid/content/Context;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0, v4}, Landroid/app/ActionBar;->setSubtitle(Ljava/lang/CharSequence;)V

    return-void
.end method

.method private showRestoreResult(Ljava/util/ArrayList;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/mediatek/datatransfer/ResultDialog$ResultEntity;",
            ">;)V"
        }
    .end annotation

    invoke-virtual {p0}, Lcom/mediatek/datatransfer/AbstractRestoreActivity;->dismissProgressDialog()V

    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    const-string v1, "result"

    invoke-virtual {v0, v1, p1}, Landroid/os/Bundle;->putParcelableArrayList(Ljava/lang/String;Ljava/util/ArrayList;)V

    const/16 v1, 0x7d4

    invoke-virtual {p0, v1, v0}, Landroid/app/Activity;->showDialog(ILandroid/os/Bundle;)Z

    return-void
.end method

.method private showUpdatingTitle()V
    .locals 2

    invoke-virtual {p0}, Landroid/app/Activity;->getActionBar()Landroid/app/ActionBar;

    move-result-object v0

    const v1, 0x7f060035

    invoke-virtual {v0, v1}, Landroid/app/ActionBar;->setTitle(I)V

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/app/ActionBar;->setSubtitle(Ljava/lang/CharSequence;)V

    return-void
.end method

.method private updateResultIfneed()V
    .locals 3

    iget-object v1, p0, Lcom/mediatek/datatransfer/AbstractRestoreActivity;->mRestoreService:Lcom/mediatek/datatransfer/RestoreService$RestoreBinder;

    if-eqz v1, :cond_0

    iget-boolean v1, p0, Lcom/mediatek/datatransfer/PersonalDataRestoreActivity;->mNeedUpdateResult:Z

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/mediatek/datatransfer/AbstractRestoreActivity;->mRestoreService:Lcom/mediatek/datatransfer/RestoreService$RestoreBinder;

    invoke-virtual {v1}, Lcom/mediatek/datatransfer/RestoreService$RestoreBinder;->getState()I

    move-result v0

    const/4 v1, 0x5

    if-ne v0, v1, :cond_0

    iget-object v1, p0, Lcom/mediatek/datatransfer/PersonalDataRestoreActivity;->CLASS_TAG:Ljava/lang/String;

    const-string v2, "updateResult because of finish when onStop"

    invoke-static {v1, v2}, Lcom/mediatek/datatransfer/utils/MyLogger;->logV(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v1, p0, Lcom/mediatek/datatransfer/AbstractRestoreActivity;->mRestoreService:Lcom/mediatek/datatransfer/RestoreService$RestoreBinder;

    invoke-virtual {v1}, Lcom/mediatek/datatransfer/RestoreService$RestoreBinder;->getRestoreResult()Ljava/util/ArrayList;

    move-result-object v1

    invoke-direct {p0, v1}, Lcom/mediatek/datatransfer/PersonalDataRestoreActivity;->showRestoreResult(Ljava/util/ArrayList;)V

    :cond_0
    const/4 v1, 0x0

    iput-boolean v1, p0, Lcom/mediatek/datatransfer/PersonalDataRestoreActivity;->mNeedUpdateResult:Z

    return-void
.end method


# virtual methods
.method protected afterServiceConnected()V
    .locals 2

    iget-object v0, p0, Lcom/mediatek/datatransfer/PersonalDataRestoreActivity;->CLASS_TAG:Ljava/lang/String;

    const-string v1, "afterServiceConnected, to checkRestorestate"

    invoke-static {v0, v1}, Lcom/mediatek/datatransfer/utils/MyLogger;->logD(Ljava/lang/String;Ljava/lang/String;)V

    invoke-direct {p0}, Lcom/mediatek/datatransfer/PersonalDataRestoreActivity;->checkRestoreState()V

    return-void
.end method

.method protected initAdapter()Landroid/widget/BaseAdapter;
    .locals 3

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    new-instance v1, Lcom/mediatek/datatransfer/PersonalDataRestoreActivity$PersonalDataRestoreAdapter;

    const v2, 0x7f030006

    invoke-direct {v1, p0, p0, v0, v2}, Lcom/mediatek/datatransfer/PersonalDataRestoreActivity$PersonalDataRestoreAdapter;-><init>(Lcom/mediatek/datatransfer/PersonalDataRestoreActivity;Landroid/content/Context;Ljava/util/ArrayList;I)V

    iput-object v1, p0, Lcom/mediatek/datatransfer/PersonalDataRestoreActivity;->mRestoreAdapter:Lcom/mediatek/datatransfer/PersonalDataRestoreActivity$PersonalDataRestoreAdapter;

    iget-object v1, p0, Lcom/mediatek/datatransfer/PersonalDataRestoreActivity;->mRestoreAdapter:Lcom/mediatek/datatransfer/PersonalDataRestoreActivity$PersonalDataRestoreAdapter;

    return-object v1
.end method

.method protected notifyListItemCheckedChanged()V
    .locals 0

    invoke-super {p0}, Lcom/mediatek/datatransfer/AbstractRestoreActivity;->notifyListItemCheckedChanged()V

    invoke-virtual {p0}, Lcom/mediatek/datatransfer/PersonalDataRestoreActivity;->updateTitle()V

    return-void
.end method

.method public onCheckedCountChanged()V
    .locals 0

    invoke-super {p0}, Lcom/mediatek/datatransfer/AbstractRestoreActivity;->onCheckedCountChanged()V

    invoke-virtual {p0}, Lcom/mediatek/datatransfer/PersonalDataRestoreActivity;->updateTitle()V

    return-void
.end method

.method public onConfigurationChanged(Landroid/content/res/Configuration;)V
    .locals 2
    .param p1    # Landroid/content/res/Configuration;

    invoke-super {p0, p1}, Lcom/mediatek/datatransfer/AbstractRestoreActivity;->onConfigurationChanged(Landroid/content/res/Configuration;)V

    iget-object v0, p0, Lcom/mediatek/datatransfer/PersonalDataRestoreActivity;->CLASS_TAG:Ljava/lang/String;

    const-string v1, "onConfigurationChanged"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    return-void
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 3
    .param p1    # Landroid/os/Bundle;

    invoke-super {p0, p1}, Lcom/mediatek/datatransfer/AbstractRestoreActivity;->onCreate(Landroid/os/Bundle;)V

    invoke-virtual {p0}, Landroid/app/Activity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    if-eqz v0, :cond_0

    const-string v1, "filename"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    if-nez v1, :cond_1

    :cond_0
    invoke-virtual {p0}, Landroid/app/Activity;->finish()V

    :goto_0
    return-void

    :cond_1
    new-instance v1, Ljava/io/File;

    const-string v2, "filename"

    invoke-virtual {v0, v2}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    iput-object v1, p0, Lcom/mediatek/datatransfer/PersonalDataRestoreActivity;->mFile:Ljava/io/File;

    iget-object v1, p0, Lcom/mediatek/datatransfer/PersonalDataRestoreActivity;->mFile:Ljava/io/File;

    invoke-virtual {v1}, Ljava/io/File;->exists()Z

    move-result v1

    if-nez v1, :cond_2

    const-string v1, "file don\'t exist"

    const/4 v2, 0x1

    invoke-static {p0, v1, v2}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    invoke-virtual {p0}, Landroid/app/Activity;->finish()V

    goto :goto_0

    :cond_2
    iget-object v1, p0, Lcom/mediatek/datatransfer/PersonalDataRestoreActivity;->CLASS_TAG:Ljava/lang/String;

    const-string v2, "onCreate"

    invoke-static {v1, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    invoke-direct {p0}, Lcom/mediatek/datatransfer/PersonalDataRestoreActivity;->init()V

    goto :goto_0
.end method

.method protected onCreateDialog(ILandroid/os/Bundle;)Landroid/app/Dialog;
    .locals 3
    .param p1    # I
    .param p2    # Landroid/os/Bundle;

    const/4 v0, 0x0

    packed-switch p1, :pswitch_data_0

    invoke-super {p0, p1, p2}, Lcom/mediatek/datatransfer/AbstractRestoreActivity;->onCreateDialog(ILandroid/os/Bundle;)Landroid/app/Dialog;

    move-result-object v0

    :goto_0
    return-object v0

    :pswitch_0
    const v1, 0x7f060021

    new-instance v2, Lcom/mediatek/datatransfer/PersonalDataRestoreActivity$1;

    invoke-direct {v2, p0}, Lcom/mediatek/datatransfer/PersonalDataRestoreActivity$1;-><init>(Lcom/mediatek/datatransfer/PersonalDataRestoreActivity;)V

    invoke-static {p0, v1, p2, v2}, Lcom/mediatek/datatransfer/ResultDialog;->createResultDlg(Landroid/content/Context;ILandroid/os/Bundle;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog;

    move-result-object v0

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x7d4
        :pswitch_0
    .end packed-switch
.end method

.method public onDestroy()V
    .locals 1

    invoke-super {p0}, Lcom/mediatek/datatransfer/AbstractRestoreActivity;->onDestroy()V

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/mediatek/datatransfer/AbstractRestoreActivity;->mHandler:Landroid/os/Handler;

    return-void
.end method

.method protected onPrepareDialog(ILandroid/app/Dialog;Landroid/os/Bundle;)V
    .locals 3
    .param p1    # I
    .param p2    # Landroid/app/Dialog;
    .param p3    # Landroid/os/Bundle;

    packed-switch p1, :pswitch_data_0

    :cond_0
    :goto_0
    return-void

    :pswitch_0
    move-object v1, p2

    check-cast v1, Landroid/app/AlertDialog;

    invoke-virtual {v1}, Landroid/app/AlertDialog;->getListView()Landroid/widget/ListView;

    move-result-object v2

    if-eqz v2, :cond_0

    invoke-static {p0, p3}, Lcom/mediatek/datatransfer/ResultDialog;->createResultAdapter(Landroid/content/Context;Landroid/os/Bundle;)Lcom/mediatek/datatransfer/ResultDialog$ResultDialogAdapter;

    move-result-object v0

    invoke-virtual {v2, v0}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x7d4
        :pswitch_0
    .end packed-switch
.end method

.method protected onResume()V
    .locals 2

    invoke-super {p0}, Lcom/mediatek/datatransfer/CheckedListActivity;->onResume()V

    iget-object v0, p0, Lcom/mediatek/datatransfer/PersonalDataRestoreActivity;->CLASS_TAG:Ljava/lang/String;

    const-string v1, "onResume"

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v0, p0, Lcom/mediatek/datatransfer/PersonalDataRestoreActivity;->mFile:Ljava/io/File;

    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v0

    if-nez v0, :cond_0

    const v0, 0x7f060033

    const/4 v1, 0x1

    invoke-static {p0, v0, v1}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    invoke-virtual {p0}, Landroid/app/Activity;->finish()V

    :cond_0
    return-void
.end method

.method protected onStart()V
    .locals 3

    const/4 v2, 0x0

    invoke-super {p0}, Lcom/mediatek/datatransfer/AbstractRestoreActivity;->onStart()V

    iget-object v0, p0, Lcom/mediatek/datatransfer/PersonalDataRestoreActivity;->mFile:Ljava/io/File;

    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v0

    if-eqz v0, :cond_0

    new-instance v0, Lcom/mediatek/datatransfer/PersonalDataRestoreActivity$FilePreviewTask;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/mediatek/datatransfer/PersonalDataRestoreActivity$FilePreviewTask;-><init>(Lcom/mediatek/datatransfer/PersonalDataRestoreActivity;Lcom/mediatek/datatransfer/PersonalDataRestoreActivity$1;)V

    new-array v1, v2, [Ljava/lang/Void;

    invoke-virtual {v0, v1}, Landroid/os/AsyncTask;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    invoke-direct {p0}, Lcom/mediatek/datatransfer/PersonalDataRestoreActivity;->updateResultIfneed()V

    iput-boolean v2, p0, Lcom/mediatek/datatransfer/PersonalDataRestoreActivity;->mIsStoped:Z

    :goto_0
    return-void

    :cond_0
    const v0, 0x7f060033

    invoke-static {p0, v0, v2}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    invoke-virtual {p0}, Landroid/app/Activity;->finish()V

    goto :goto_0
.end method

.method public onStop()V
    .locals 1

    invoke-super {p0}, Lcom/mediatek/datatransfer/AbstractRestoreActivity;->onStop()V

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/mediatek/datatransfer/PersonalDataRestoreActivity;->mIsStoped:Z

    return-void
.end method

.method public startRestore()V
    .locals 7

    const/4 v6, 0x0

    invoke-virtual {p0}, Lcom/mediatek/datatransfer/AbstractRestoreActivity;->isCanStartRestore()Z

    move-result v4

    if-nez v4, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    invoke-virtual {p0}, Lcom/mediatek/datatransfer/AbstractRestoreActivity;->startService()V

    iget-object v4, p0, Lcom/mediatek/datatransfer/PersonalDataRestoreActivity;->CLASS_TAG:Ljava/lang/String;

    const-string v5, "startRestore"

    invoke-static {v4, v5}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    invoke-direct {p0}, Lcom/mediatek/datatransfer/PersonalDataRestoreActivity;->getSelectedItemList()Ljava/util/ArrayList;

    move-result-object v1

    iget-object v4, p0, Lcom/mediatek/datatransfer/AbstractRestoreActivity;->mRestoreService:Lcom/mediatek/datatransfer/RestoreService$RestoreBinder;

    invoke-virtual {v4, v1}, Lcom/mediatek/datatransfer/RestoreService$RestoreBinder;->setRestoreModelList(Ljava/util/ArrayList;)V

    iget-object v4, p0, Lcom/mediatek/datatransfer/PersonalDataRestoreActivity;->mFile:Ljava/io/File;

    invoke-virtual {v4}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v4

    iput-object v4, p0, Lcom/mediatek/datatransfer/PersonalDataRestoreActivity;->mRestoreFolderPath:Ljava/lang/String;

    iget-object v4, p0, Lcom/mediatek/datatransfer/AbstractRestoreActivity;->mRestoreService:Lcom/mediatek/datatransfer/RestoreService$RestoreBinder;

    iget-object v5, p0, Lcom/mediatek/datatransfer/PersonalDataRestoreActivity;->mRestoreFolderPath:Ljava/lang/String;

    invoke-virtual {v4, v5}, Lcom/mediatek/datatransfer/RestoreService$RestoreBinder;->startRestore(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_2

    invoke-virtual {p0}, Lcom/mediatek/datatransfer/AbstractRestoreActivity;->showProgressDialog()V

    invoke-virtual {v1, v6}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/Integer;

    invoke-virtual {v4}, Ljava/lang/Integer;->intValue()I

    move-result v4

    invoke-direct {p0, v4}, Lcom/mediatek/datatransfer/PersonalDataRestoreActivity;->getProgressDlgMessage(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0, v2}, Lcom/mediatek/datatransfer/AbstractRestoreActivity;->setProgressDialogMessage(Ljava/lang/CharSequence;)V

    invoke-virtual {p0, v6}, Lcom/mediatek/datatransfer/AbstractRestoreActivity;->setProgressDialogProgress(I)V

    invoke-virtual {p0, v6}, Landroid/app/Activity;->setProgress(I)V

    iget-object v4, p0, Lcom/mediatek/datatransfer/PersonalDataRestoreActivity;->mPreview:Lcom/mediatek/datatransfer/utils/BackupFilePreview;

    if-eqz v4, :cond_0

    iget-object v5, p0, Lcom/mediatek/datatransfer/PersonalDataRestoreActivity;->mPreview:Lcom/mediatek/datatransfer/utils/BackupFilePreview;

    invoke-virtual {v1, v6}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/Integer;

    invoke-virtual {v4}, Ljava/lang/Integer;->intValue()I

    move-result v4

    invoke-virtual {v5, v4}, Lcom/mediatek/datatransfer/utils/BackupFilePreview;->getItemCount(I)I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/mediatek/datatransfer/AbstractRestoreActivity;->setProgressDialogMax(I)V

    goto :goto_0

    :cond_2
    invoke-virtual {p0}, Lcom/mediatek/datatransfer/AbstractRestoreActivity;->stopService()V

    goto :goto_0
.end method

.method public updateTitle()V
    .locals 5

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const v3, 0x7f060028

    invoke-virtual {p0, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Lcom/mediatek/datatransfer/CheckedListActivity;->getCount()I

    move-result v2

    invoke-virtual {p0}, Lcom/mediatek/datatransfer/CheckedListActivity;->getCheckedCount()I

    move-result v1

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "("

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "/"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ")"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p0, v3}, Landroid/app/Activity;->setTitle(Ljava/lang/CharSequence;)V

    return-void
.end method
