.class public Lcom/mediatek/datatransfer/RestoreService$RestoreBinder;
.super Landroid/os/Binder;
.source "RestoreService.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/mediatek/datatransfer/RestoreService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1
    name = "RestoreBinder"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/mediatek/datatransfer/RestoreService;


# direct methods
.method public constructor <init>(Lcom/mediatek/datatransfer/RestoreService;)V
    .locals 0

    iput-object p1, p0, Lcom/mediatek/datatransfer/RestoreService$RestoreBinder;->this$0:Lcom/mediatek/datatransfer/RestoreService;

    invoke-direct {p0}, Landroid/os/Binder;-><init>()V

    return-void
.end method


# virtual methods
.method public cancelRestore()V
    .locals 2

    iget-object v0, p0, Lcom/mediatek/datatransfer/RestoreService$RestoreBinder;->this$0:Lcom/mediatek/datatransfer/RestoreService;

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Lcom/mediatek/datatransfer/RestoreService;->moveToState(I)V

    iget-object v0, p0, Lcom/mediatek/datatransfer/RestoreService$RestoreBinder;->this$0:Lcom/mediatek/datatransfer/RestoreService;

    invoke-static {v0}, Lcom/mediatek/datatransfer/RestoreService;->access$100(Lcom/mediatek/datatransfer/RestoreService;)Lcom/mediatek/datatransfer/RestoreEngine;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/mediatek/datatransfer/RestoreService$RestoreBinder;->this$0:Lcom/mediatek/datatransfer/RestoreService;

    invoke-static {v0}, Lcom/mediatek/datatransfer/RestoreService;->access$100(Lcom/mediatek/datatransfer/RestoreService;)Lcom/mediatek/datatransfer/RestoreEngine;

    move-result-object v0

    invoke-virtual {v0}, Lcom/mediatek/datatransfer/RestoreEngine;->cancel()V

    :cond_0
    const-string v0, "DataTransfer/RestoreService"

    const-string v1, "cancelRestore"

    invoke-static {v0, v1}, Lcom/mediatek/datatransfer/utils/MyLogger;->logD(Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method public continueRestore()V
    .locals 2

    iget-object v0, p0, Lcom/mediatek/datatransfer/RestoreService$RestoreBinder;->this$0:Lcom/mediatek/datatransfer/RestoreService;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/mediatek/datatransfer/RestoreService;->moveToState(I)V

    iget-object v0, p0, Lcom/mediatek/datatransfer/RestoreService$RestoreBinder;->this$0:Lcom/mediatek/datatransfer/RestoreService;

    invoke-static {v0}, Lcom/mediatek/datatransfer/RestoreService;->access$100(Lcom/mediatek/datatransfer/RestoreService;)Lcom/mediatek/datatransfer/RestoreEngine;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/mediatek/datatransfer/RestoreService$RestoreBinder;->this$0:Lcom/mediatek/datatransfer/RestoreService;

    invoke-static {v0}, Lcom/mediatek/datatransfer/RestoreService;->access$100(Lcom/mediatek/datatransfer/RestoreService;)Lcom/mediatek/datatransfer/RestoreEngine;

    move-result-object v0

    invoke-virtual {v0}, Lcom/mediatek/datatransfer/RestoreEngine;->continueRestore()V

    :cond_0
    const-string v0, "DataTransfer/RestoreService"

    const-string v1, "continueRestore"

    invoke-static {v0, v1}, Lcom/mediatek/datatransfer/utils/MyLogger;->logD(Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method public getAppRestoreResult()Ljava/util/ArrayList;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/mediatek/datatransfer/ResultDialog$ResultEntity;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/mediatek/datatransfer/RestoreService$RestoreBinder;->this$0:Lcom/mediatek/datatransfer/RestoreService;

    invoke-static {v0}, Lcom/mediatek/datatransfer/RestoreService;->access$300(Lcom/mediatek/datatransfer/RestoreService;)Ljava/util/ArrayList;

    move-result-object v0

    return-object v0
.end method

.method public getCurRestoreProgress()Lcom/mediatek/datatransfer/RestoreService$RestoreProgress;
    .locals 1

    iget-object v0, p0, Lcom/mediatek/datatransfer/RestoreService$RestoreBinder;->this$0:Lcom/mediatek/datatransfer/RestoreService;

    invoke-static {v0}, Lcom/mediatek/datatransfer/RestoreService;->access$400(Lcom/mediatek/datatransfer/RestoreService;)Lcom/mediatek/datatransfer/RestoreService$RestoreProgress;

    move-result-object v0

    return-object v0
.end method

.method public getRestoreItemParam(I)Ljava/util/ArrayList;
    .locals 2
    .param p1    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/mediatek/datatransfer/RestoreService$RestoreBinder;->this$0:Lcom/mediatek/datatransfer/RestoreService;

    iget-object v0, v0, Lcom/mediatek/datatransfer/RestoreService;->mParasMap:Ljava/util/HashMap;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/ArrayList;

    return-object v0
.end method

.method public getRestoreResult()Ljava/util/ArrayList;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/mediatek/datatransfer/ResultDialog$ResultEntity;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/mediatek/datatransfer/RestoreService$RestoreBinder;->this$0:Lcom/mediatek/datatransfer/RestoreService;

    invoke-static {v0}, Lcom/mediatek/datatransfer/RestoreService;->access$200(Lcom/mediatek/datatransfer/RestoreService;)Ljava/util/ArrayList;

    move-result-object v0

    return-object v0
.end method

.method public getState()I
    .locals 1

    iget-object v0, p0, Lcom/mediatek/datatransfer/RestoreService$RestoreBinder;->this$0:Lcom/mediatek/datatransfer/RestoreService;

    invoke-static {v0}, Lcom/mediatek/datatransfer/RestoreService;->access$000(Lcom/mediatek/datatransfer/RestoreService;)I

    move-result v0

    return v0
.end method

.method public pauseRestore()V
    .locals 2

    iget-object v0, p0, Lcom/mediatek/datatransfer/RestoreService$RestoreBinder;->this$0:Lcom/mediatek/datatransfer/RestoreService;

    const/4 v1, 0x2

    invoke-virtual {v0, v1}, Lcom/mediatek/datatransfer/RestoreService;->moveToState(I)V

    iget-object v0, p0, Lcom/mediatek/datatransfer/RestoreService$RestoreBinder;->this$0:Lcom/mediatek/datatransfer/RestoreService;

    invoke-static {v0}, Lcom/mediatek/datatransfer/RestoreService;->access$100(Lcom/mediatek/datatransfer/RestoreService;)Lcom/mediatek/datatransfer/RestoreEngine;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/mediatek/datatransfer/RestoreService$RestoreBinder;->this$0:Lcom/mediatek/datatransfer/RestoreService;

    invoke-static {v0}, Lcom/mediatek/datatransfer/RestoreService;->access$100(Lcom/mediatek/datatransfer/RestoreService;)Lcom/mediatek/datatransfer/RestoreEngine;

    move-result-object v0

    invoke-virtual {v0}, Lcom/mediatek/datatransfer/RestoreEngine;->pause()V

    :cond_0
    const-string v0, "DataTransfer/RestoreService"

    const-string v1, "pauseRestore"

    invoke-static {v0, v1}, Lcom/mediatek/datatransfer/utils/MyLogger;->logD(Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method public reset()V
    .locals 2

    iget-object v0, p0, Lcom/mediatek/datatransfer/RestoreService$RestoreBinder;->this$0:Lcom/mediatek/datatransfer/RestoreService;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/mediatek/datatransfer/RestoreService;->moveToState(I)V

    iget-object v0, p0, Lcom/mediatek/datatransfer/RestoreService$RestoreBinder;->this$0:Lcom/mediatek/datatransfer/RestoreService;

    invoke-static {v0}, Lcom/mediatek/datatransfer/RestoreService;->access$200(Lcom/mediatek/datatransfer/RestoreService;)Ljava/util/ArrayList;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/mediatek/datatransfer/RestoreService$RestoreBinder;->this$0:Lcom/mediatek/datatransfer/RestoreService;

    invoke-static {v0}, Lcom/mediatek/datatransfer/RestoreService;->access$200(Lcom/mediatek/datatransfer/RestoreService;)Ljava/util/ArrayList;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    :cond_0
    iget-object v0, p0, Lcom/mediatek/datatransfer/RestoreService$RestoreBinder;->this$0:Lcom/mediatek/datatransfer/RestoreService;

    invoke-static {v0}, Lcom/mediatek/datatransfer/RestoreService;->access$300(Lcom/mediatek/datatransfer/RestoreService;)Ljava/util/ArrayList;

    move-result-object v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/mediatek/datatransfer/RestoreService$RestoreBinder;->this$0:Lcom/mediatek/datatransfer/RestoreService;

    invoke-static {v0}, Lcom/mediatek/datatransfer/RestoreService;->access$300(Lcom/mediatek/datatransfer/RestoreService;)Ljava/util/ArrayList;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    :cond_1
    iget-object v0, p0, Lcom/mediatek/datatransfer/RestoreService$RestoreBinder;->this$0:Lcom/mediatek/datatransfer/RestoreService;

    iget-object v0, v0, Lcom/mediatek/datatransfer/RestoreService;->mParasMap:Ljava/util/HashMap;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/mediatek/datatransfer/RestoreService$RestoreBinder;->this$0:Lcom/mediatek/datatransfer/RestoreService;

    iget-object v0, v0, Lcom/mediatek/datatransfer/RestoreService;->mParasMap:Ljava/util/HashMap;

    invoke-virtual {v0}, Ljava/util/HashMap;->clear()V

    :cond_2
    return-void
.end method

.method public setOnRestoreChangedListner(Lcom/mediatek/datatransfer/RestoreService$OnRestoreStatusListener;)V
    .locals 1
    .param p1    # Lcom/mediatek/datatransfer/RestoreService$OnRestoreStatusListener;

    iget-object v0, p0, Lcom/mediatek/datatransfer/RestoreService$RestoreBinder;->this$0:Lcom/mediatek/datatransfer/RestoreService;

    invoke-static {v0, p1}, Lcom/mediatek/datatransfer/RestoreService;->access$502(Lcom/mediatek/datatransfer/RestoreService;Lcom/mediatek/datatransfer/RestoreService$OnRestoreStatusListener;)Lcom/mediatek/datatransfer/RestoreService$OnRestoreStatusListener;

    return-void
.end method

.method public setRestoreItemParam(ILjava/util/ArrayList;)V
    .locals 2
    .param p1    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    iget-object v0, p0, Lcom/mediatek/datatransfer/RestoreService$RestoreBinder;->this$0:Lcom/mediatek/datatransfer/RestoreService;

    iget-object v0, v0, Lcom/mediatek/datatransfer/RestoreService;->mParasMap:Ljava/util/HashMap;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1, p2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v0, p0, Lcom/mediatek/datatransfer/RestoreService$RestoreBinder;->this$0:Lcom/mediatek/datatransfer/RestoreService;

    invoke-static {v0}, Lcom/mediatek/datatransfer/RestoreService;->access$100(Lcom/mediatek/datatransfer/RestoreService;)Lcom/mediatek/datatransfer/RestoreEngine;

    move-result-object v0

    invoke-virtual {v0, p1, p2}, Lcom/mediatek/datatransfer/RestoreEngine;->setRestoreItemParam(ILjava/util/ArrayList;)V

    return-void
.end method

.method public setRestoreModelList(Ljava/util/ArrayList;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/Integer;",
            ">;)V"
        }
    .end annotation

    invoke-virtual {p0}, Lcom/mediatek/datatransfer/RestoreService$RestoreBinder;->reset()V

    iget-object v0, p0, Lcom/mediatek/datatransfer/RestoreService$RestoreBinder;->this$0:Lcom/mediatek/datatransfer/RestoreService;

    invoke-static {v0}, Lcom/mediatek/datatransfer/RestoreService;->access$100(Lcom/mediatek/datatransfer/RestoreService;)Lcom/mediatek/datatransfer/RestoreEngine;

    move-result-object v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/mediatek/datatransfer/RestoreService$RestoreBinder;->this$0:Lcom/mediatek/datatransfer/RestoreService;

    iget-object v1, p0, Lcom/mediatek/datatransfer/RestoreService$RestoreBinder;->this$0:Lcom/mediatek/datatransfer/RestoreService;

    iget-object v2, p0, Lcom/mediatek/datatransfer/RestoreService$RestoreBinder;->this$0:Lcom/mediatek/datatransfer/RestoreService;

    invoke-static {v1, v2}, Lcom/mediatek/datatransfer/RestoreEngine;->getInstance(Landroid/content/Context;Lcom/mediatek/datatransfer/ProgressReporter;)Lcom/mediatek/datatransfer/RestoreEngine;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/mediatek/datatransfer/RestoreService;->access$102(Lcom/mediatek/datatransfer/RestoreService;Lcom/mediatek/datatransfer/RestoreEngine;)Lcom/mediatek/datatransfer/RestoreEngine;

    :cond_0
    iget-object v0, p0, Lcom/mediatek/datatransfer/RestoreService$RestoreBinder;->this$0:Lcom/mediatek/datatransfer/RestoreService;

    invoke-static {v0}, Lcom/mediatek/datatransfer/RestoreService;->access$100(Lcom/mediatek/datatransfer/RestoreService;)Lcom/mediatek/datatransfer/RestoreEngine;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/mediatek/datatransfer/RestoreEngine;->setRestoreModelList(Ljava/util/ArrayList;)V

    return-void
.end method

.method public startRestore(Ljava/lang/String;)Z
    .locals 3
    .param p1    # Ljava/lang/String;

    const/4 v0, 0x1

    iget-object v1, p0, Lcom/mediatek/datatransfer/RestoreService$RestoreBinder;->this$0:Lcom/mediatek/datatransfer/RestoreService;

    new-instance v2, Landroid/app/Notification;

    invoke-direct {v2}, Landroid/app/Notification;-><init>()V

    invoke-virtual {v1, v0, v2}, Landroid/app/Service;->startForeground(ILandroid/app/Notification;)V

    iget-object v1, p0, Lcom/mediatek/datatransfer/RestoreService$RestoreBinder;->this$0:Lcom/mediatek/datatransfer/RestoreService;

    invoke-static {v1}, Lcom/mediatek/datatransfer/RestoreService;->access$100(Lcom/mediatek/datatransfer/RestoreService;)Lcom/mediatek/datatransfer/RestoreEngine;

    move-result-object v1

    if-nez v1, :cond_0

    const-string v0, "DataTransfer/RestoreService"

    const-string v1, "startRestore Error: engine is not initialed"

    invoke-static {v0, v1}, Lcom/mediatek/datatransfer/utils/MyLogger;->logE(Ljava/lang/String;Ljava/lang/String;)V

    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_0
    iget-object v1, p0, Lcom/mediatek/datatransfer/RestoreService$RestoreBinder;->this$0:Lcom/mediatek/datatransfer/RestoreService;

    invoke-static {v1}, Lcom/mediatek/datatransfer/RestoreService;->access$100(Lcom/mediatek/datatransfer/RestoreService;)Lcom/mediatek/datatransfer/RestoreEngine;

    move-result-object v1

    iget-object v2, p0, Lcom/mediatek/datatransfer/RestoreService$RestoreBinder;->this$0:Lcom/mediatek/datatransfer/RestoreService;

    invoke-virtual {v1, v2}, Lcom/mediatek/datatransfer/RestoreEngine;->setOnRestoreEndListner(Lcom/mediatek/datatransfer/RestoreEngine$OnRestoreDoneListner;)V

    iget-object v1, p0, Lcom/mediatek/datatransfer/RestoreService$RestoreBinder;->this$0:Lcom/mediatek/datatransfer/RestoreService;

    invoke-static {v1}, Lcom/mediatek/datatransfer/RestoreService;->access$100(Lcom/mediatek/datatransfer/RestoreService;)Lcom/mediatek/datatransfer/RestoreEngine;

    move-result-object v1

    invoke-virtual {v1, p1}, Lcom/mediatek/datatransfer/RestoreEngine;->startRestore(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/mediatek/datatransfer/RestoreService$RestoreBinder;->this$0:Lcom/mediatek/datatransfer/RestoreService;

    invoke-virtual {v1, v0}, Lcom/mediatek/datatransfer/RestoreService;->moveToState(I)V

    goto :goto_0
.end method
