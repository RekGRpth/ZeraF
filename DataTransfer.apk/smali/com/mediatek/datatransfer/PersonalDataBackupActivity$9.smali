.class Lcom/mediatek/datatransfer/PersonalDataBackupActivity$9;
.super Ljava/lang/Object;
.source "PersonalDataBackupActivity.java"

# interfaces
.implements Landroid/content/DialogInterface$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/mediatek/datatransfer/PersonalDataBackupActivity;->showContactConfigDialog()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/mediatek/datatransfer/PersonalDataBackupActivity;

.field final synthetic val$temp:[Z


# direct methods
.method constructor <init>(Lcom/mediatek/datatransfer/PersonalDataBackupActivity;[Z)V
    .locals 0

    iput-object p1, p0, Lcom/mediatek/datatransfer/PersonalDataBackupActivity$9;->this$0:Lcom/mediatek/datatransfer/PersonalDataBackupActivity;

    iput-object p2, p0, Lcom/mediatek/datatransfer/PersonalDataBackupActivity$9;->val$temp:[Z

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/content/DialogInterface;I)V
    .locals 7
    .param p1    # Landroid/content/DialogInterface;
    .param p2    # I

    const/4 v6, 0x0

    const/4 v1, 0x1

    const/4 v2, 0x0

    :goto_0
    iget-object v3, p0, Lcom/mediatek/datatransfer/PersonalDataBackupActivity$9;->this$0:Lcom/mediatek/datatransfer/PersonalDataBackupActivity;

    invoke-static {v3}, Lcom/mediatek/datatransfer/PersonalDataBackupActivity;->access$700(Lcom/mediatek/datatransfer/PersonalDataBackupActivity;)[Z

    move-result-object v3

    array-length v3, v3

    if-ge v2, v3, :cond_0

    iget-object v3, p0, Lcom/mediatek/datatransfer/PersonalDataBackupActivity$9;->this$0:Lcom/mediatek/datatransfer/PersonalDataBackupActivity;

    invoke-static {v3}, Lcom/mediatek/datatransfer/PersonalDataBackupActivity;->access$700(Lcom/mediatek/datatransfer/PersonalDataBackupActivity;)[Z

    move-result-object v3

    iget-object v4, p0, Lcom/mediatek/datatransfer/PersonalDataBackupActivity$9;->val$temp:[Z

    aget-boolean v4, v4, v2

    aput-boolean v4, v3, v2

    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    :cond_0
    iget-object v3, p0, Lcom/mediatek/datatransfer/PersonalDataBackupActivity$9;->this$0:Lcom/mediatek/datatransfer/PersonalDataBackupActivity;

    invoke-static {v3}, Lcom/mediatek/datatransfer/PersonalDataBackupActivity;->access$800(Lcom/mediatek/datatransfer/PersonalDataBackupActivity;)I

    move-result v0

    const/4 v2, 0x0

    :goto_1
    if-ge v2, v0, :cond_1

    iget-object v3, p0, Lcom/mediatek/datatransfer/PersonalDataBackupActivity$9;->this$0:Lcom/mediatek/datatransfer/PersonalDataBackupActivity;

    invoke-static {v3}, Lcom/mediatek/datatransfer/PersonalDataBackupActivity;->access$700(Lcom/mediatek/datatransfer/PersonalDataBackupActivity;)[Z

    move-result-object v3

    aget-boolean v3, v3, v2

    if-eqz v3, :cond_2

    const/4 v1, 0x0

    :cond_1
    invoke-static {}, Lcom/mediatek/datatransfer/PersonalDataBackupActivity;->access$200()Ljava/lang/String;

    move-result-object v3

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "mContactCheckTypes = "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-object v5, p0, Lcom/mediatek/datatransfer/PersonalDataBackupActivity$9;->this$0:Lcom/mediatek/datatransfer/PersonalDataBackupActivity;

    invoke-static {v5}, Lcom/mediatek/datatransfer/PersonalDataBackupActivity;->access$700(Lcom/mediatek/datatransfer/PersonalDataBackupActivity;)[Z

    move-result-object v5

    invoke-static {v5}, Ljava/util/Arrays;->toString([Z)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/mediatek/datatransfer/utils/MyLogger;->logD(Ljava/lang/String;Ljava/lang/String;)V

    if-eqz v1, :cond_3

    iget-object v3, p0, Lcom/mediatek/datatransfer/PersonalDataBackupActivity$9;->this$0:Lcom/mediatek/datatransfer/PersonalDataBackupActivity;

    invoke-virtual {v3, v6, v6}, Lcom/mediatek/datatransfer/CheckedListActivity;->setItemCheckedByPosition(IZ)V

    :goto_2
    return-void

    :cond_2
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    :cond_3
    iget-object v3, p0, Lcom/mediatek/datatransfer/PersonalDataBackupActivity$9;->this$0:Lcom/mediatek/datatransfer/PersonalDataBackupActivity;

    const/4 v4, 0x1

    invoke-virtual {v3, v6, v4}, Lcom/mediatek/datatransfer/CheckedListActivity;->setItemCheckedByPosition(IZ)V

    goto :goto_2
.end method
