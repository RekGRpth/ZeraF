.class Lcom/mediatek/datatransfer/BackupEngine$BackupThread;
.super Ljava/lang/Thread;
.source "BackupEngine.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/mediatek/datatransfer/BackupEngine;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "BackupThread"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/mediatek/datatransfer/BackupEngine;


# direct methods
.method private constructor <init>(Lcom/mediatek/datatransfer/BackupEngine;)V
    .locals 0

    iput-object p1, p0, Lcom/mediatek/datatransfer/BackupEngine$BackupThread;->this$0:Lcom/mediatek/datatransfer/BackupEngine;

    invoke-direct {p0}, Ljava/lang/Thread;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/mediatek/datatransfer/BackupEngine;Lcom/mediatek/datatransfer/BackupEngine$1;)V
    .locals 0
    .param p1    # Lcom/mediatek/datatransfer/BackupEngine;
    .param p2    # Lcom/mediatek/datatransfer/BackupEngine$1;

    invoke-direct {p0, p1}, Lcom/mediatek/datatransfer/BackupEngine$BackupThread;-><init>(Lcom/mediatek/datatransfer/BackupEngine;)V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 7

    sget-object v3, Lcom/mediatek/datatransfer/BackupEngine$BackupResultType;->Fail:Lcom/mediatek/datatransfer/BackupEngine$BackupResultType;

    const-string v4, "B&R"

    const-string v5, "backupEngineBackupThread begin..."

    invoke-static {v4, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v4, p0, Lcom/mediatek/datatransfer/BackupEngine$BackupThread;->this$0:Lcom/mediatek/datatransfer/BackupEngine;

    invoke-static {v4}, Lcom/mediatek/datatransfer/BackupEngine;->access$100(Lcom/mediatek/datatransfer/BackupEngine;)Ljava/util/List;

    move-result-object v4

    invoke-interface {v4}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_2

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/mediatek/datatransfer/modules/Composer;

    const-string v4, "B&R"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "backupEngineBackupThread->composer:"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v0}, Lcom/mediatek/datatransfer/modules/Composer;->getModuleType()I

    move-result v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, " start..."

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    invoke-virtual {v0}, Lcom/mediatek/datatransfer/modules/Composer;->isCancel()Z

    move-result v4

    if-nez v4, :cond_1

    invoke-virtual {v0}, Lcom/mediatek/datatransfer/modules/Composer;->init()Z

    invoke-virtual {v0}, Lcom/mediatek/datatransfer/modules/Composer;->onStart()V

    const-string v4, "B&R"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "backupEngineBackupThread-> composer:"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v0}, Lcom/mediatek/datatransfer/modules/Composer;->getModuleType()I

    move-result v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, " init finish"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :goto_1
    invoke-virtual {v0}, Lcom/mediatek/datatransfer/modules/Composer;->isAfterLast()Z

    move-result v4

    if-nez v4, :cond_1

    invoke-virtual {v0}, Lcom/mediatek/datatransfer/modules/Composer;->isCancel()Z

    move-result v4

    if-nez v4, :cond_1

    iget-object v4, p0, Lcom/mediatek/datatransfer/BackupEngine$BackupThread;->this$0:Lcom/mediatek/datatransfer/BackupEngine;

    invoke-static {v4}, Lcom/mediatek/datatransfer/BackupEngine;->access$200(Lcom/mediatek/datatransfer/BackupEngine;)Z

    move-result v4

    if-eqz v4, :cond_0

    iget-object v4, p0, Lcom/mediatek/datatransfer/BackupEngine$BackupThread;->this$0:Lcom/mediatek/datatransfer/BackupEngine;

    invoke-static {v4}, Lcom/mediatek/datatransfer/BackupEngine;->access$300(Lcom/mediatek/datatransfer/BackupEngine;)Ljava/lang/Object;

    move-result-object v5

    monitor-enter v5

    :try_start_0
    const-string v4, "B&R"

    const-string v6, "backupEngineBackupThread wait..."

    invoke-static {v4, v6}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v4, p0, Lcom/mediatek/datatransfer/BackupEngine$BackupThread;->this$0:Lcom/mediatek/datatransfer/BackupEngine;

    invoke-static {v4}, Lcom/mediatek/datatransfer/BackupEngine;->access$300(Lcom/mediatek/datatransfer/BackupEngine;)Ljava/lang/Object;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/Object;->wait()V
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :goto_2
    :try_start_1
    monitor-exit v5
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    :cond_0
    invoke-virtual {v0}, Lcom/mediatek/datatransfer/modules/Composer;->composeOneEntity()Z

    const-string v4, "B&R"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "backupEngineBackupThread->composer:"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v0}, Lcom/mediatek/datatransfer/modules/Composer;->getModuleType()I

    move-result v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, " compose one entiry"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1

    :catch_0
    move-exception v1

    :try_start_2
    invoke-virtual {v1}, Ljava/lang/Throwable;->printStackTrace()V

    goto :goto_2

    :catchall_0
    move-exception v4

    monitor-exit v5
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    throw v4

    :cond_1
    const-wide/16 v4, 0xc8

    :try_start_3
    invoke-static {v4, v5}, Lcom/mediatek/datatransfer/BackupEngine$BackupThread;->sleep(J)V
    :try_end_3
    .catch Ljava/lang/InterruptedException; {:try_start_3 .. :try_end_3} :catch_1

    :goto_3
    invoke-virtual {v0}, Lcom/mediatek/datatransfer/modules/Composer;->onEnd()V

    const-string v4, "B&R"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "backupEngineBackupThread-> composer:"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v0}, Lcom/mediatek/datatransfer/modules/Composer;->getModuleType()I

    move-result v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, " finish"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0

    :catch_1
    move-exception v1

    invoke-virtual {v1}, Ljava/lang/Throwable;->printStackTrace()V

    goto :goto_3

    :cond_2
    const-string v4, "B&R"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "backupEngineBackupThread run finish, result:"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v4, p0, Lcom/mediatek/datatransfer/BackupEngine$BackupThread;->this$0:Lcom/mediatek/datatransfer/BackupEngine;

    const/4 v5, 0x0

    invoke-static {v4, v5}, Lcom/mediatek/datatransfer/BackupEngine;->access$402(Lcom/mediatek/datatransfer/BackupEngine;Z)Z

    iget-object v4, p0, Lcom/mediatek/datatransfer/BackupEngine$BackupThread;->this$0:Lcom/mediatek/datatransfer/BackupEngine;

    invoke-static {v4}, Lcom/mediatek/datatransfer/BackupEngine;->access$500(Lcom/mediatek/datatransfer/BackupEngine;)Z

    move-result v4

    if-eqz v4, :cond_6

    sget-object v3, Lcom/mediatek/datatransfer/BackupEngine$BackupResultType;->Cancel:Lcom/mediatek/datatransfer/BackupEngine$BackupResultType;

    iget-object v4, p0, Lcom/mediatek/datatransfer/BackupEngine$BackupThread;->this$0:Lcom/mediatek/datatransfer/BackupEngine;

    iget-object v4, v4, Lcom/mediatek/datatransfer/BackupEngine;->mModuleList:Ljava/util/ArrayList;

    const/16 v5, 0x10

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_3

    iget-object v4, p0, Lcom/mediatek/datatransfer/BackupEngine$BackupThread;->this$0:Lcom/mediatek/datatransfer/BackupEngine;

    new-instance v5, Ljava/io/File;

    iget-object v6, p0, Lcom/mediatek/datatransfer/BackupEngine$BackupThread;->this$0:Lcom/mediatek/datatransfer/BackupEngine;

    invoke-static {v6}, Lcom/mediatek/datatransfer/BackupEngine;->access$600(Lcom/mediatek/datatransfer/BackupEngine;)Ljava/lang/String;

    move-result-object v6

    invoke-direct {v5, v6}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-static {v4, v5}, Lcom/mediatek/datatransfer/BackupEngine;->access$700(Lcom/mediatek/datatransfer/BackupEngine;Ljava/io/File;)V

    :cond_3
    :goto_4
    iget-object v4, p0, Lcom/mediatek/datatransfer/BackupEngine$BackupThread;->this$0:Lcom/mediatek/datatransfer/BackupEngine;

    invoke-static {v4}, Lcom/mediatek/datatransfer/BackupEngine;->access$800(Lcom/mediatek/datatransfer/BackupEngine;)Lcom/mediatek/datatransfer/BackupEngine$OnBackupDoneListner;

    move-result-object v4

    if-eqz v4, :cond_5

    iget-object v4, p0, Lcom/mediatek/datatransfer/BackupEngine$BackupThread;->this$0:Lcom/mediatek/datatransfer/BackupEngine;

    invoke-static {v4}, Lcom/mediatek/datatransfer/BackupEngine;->access$200(Lcom/mediatek/datatransfer/BackupEngine;)Z

    move-result v4

    if-eqz v4, :cond_4

    iget-object v4, p0, Lcom/mediatek/datatransfer/BackupEngine$BackupThread;->this$0:Lcom/mediatek/datatransfer/BackupEngine;

    invoke-static {v4}, Lcom/mediatek/datatransfer/BackupEngine;->access$300(Lcom/mediatek/datatransfer/BackupEngine;)Ljava/lang/Object;

    move-result-object v5

    monitor-enter v5

    :try_start_4
    const-string v4, "B&R"

    const-string v6, "backupEngineBackupThread wait before end..."

    invoke-static {v4, v6}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v4, p0, Lcom/mediatek/datatransfer/BackupEngine$BackupThread;->this$0:Lcom/mediatek/datatransfer/BackupEngine;

    invoke-static {v4}, Lcom/mediatek/datatransfer/BackupEngine;->access$300(Lcom/mediatek/datatransfer/BackupEngine;)Ljava/lang/Object;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/Object;->wait()V
    :try_end_4
    .catch Ljava/lang/InterruptedException; {:try_start_4 .. :try_end_4} :catch_2
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    :goto_5
    :try_start_5
    monitor-exit v5
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_1

    :cond_4
    iget-object v4, p0, Lcom/mediatek/datatransfer/BackupEngine$BackupThread;->this$0:Lcom/mediatek/datatransfer/BackupEngine;

    invoke-static {v4}, Lcom/mediatek/datatransfer/BackupEngine;->access$800(Lcom/mediatek/datatransfer/BackupEngine;)Lcom/mediatek/datatransfer/BackupEngine$OnBackupDoneListner;

    move-result-object v4

    invoke-interface {v4, v3}, Lcom/mediatek/datatransfer/BackupEngine$OnBackupDoneListner;->onFinishBackup(Lcom/mediatek/datatransfer/BackupEngine$BackupResultType;)V

    :cond_5
    return-void

    :cond_6
    sget-object v3, Lcom/mediatek/datatransfer/BackupEngine$BackupResultType;->Success:Lcom/mediatek/datatransfer/BackupEngine$BackupResultType;

    goto :goto_4

    :catch_2
    move-exception v1

    :try_start_6
    invoke-virtual {v1}, Ljava/lang/Throwable;->printStackTrace()V

    goto :goto_5

    :catchall_1
    move-exception v4

    monitor-exit v5
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_1

    throw v4
.end method
