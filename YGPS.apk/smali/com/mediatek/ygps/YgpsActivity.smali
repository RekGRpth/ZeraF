.class public Lcom/mediatek/ygps/YgpsActivity;
.super Landroid/app/TabActivity;
.source "YgpsActivity.java"

# interfaces
.implements Lcom/mediatek/ygps/SatelliteDataProvider;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/mediatek/ygps/YgpsActivity$YgpsWakeLock;,
        Lcom/mediatek/ygps/YgpsActivity$NmeaListenClass;,
        Lcom/mediatek/ygps/YgpsActivity$AutoTestThread;
    }
.end annotation


# static fields
.field private static final COMMAND_END:Ljava/lang/String; = "*"

.field private static final COMMAND_START:Ljava/lang/String; = "$"

.field private static final COUNT_PRECISION:I = 0x1f4

.field private static final DATE_FORMAT:Ljava/text/DateFormat;

.field private static final DIALOG_WAITING_FOR_STOP:I = 0x0

.field private static final EXCEED_SECOND:I = 0x3e7

.field private static final FIRST_TIME:Ljava/lang/String; = "first.time"

.field private static final GPS_EXTRA_A1LMANAC:Ljava/lang/String; = "almanac"

.field private static final GPS_EXTRA_ALL:Ljava/lang/String; = "all"

.field private static final GPS_EXTRA_EPHEMERIS:Ljava/lang/String; = "ephemeris"

.field private static final GPS_EXTRA_HEALTH:Ljava/lang/String; = "health"

.field private static final GPS_EXTRA_IONO:Ljava/lang/String; = "iono"

.field private static final GPS_EXTRA_POSITION:Ljava/lang/String; = "position"

.field private static final GPS_EXTRA_RTI:Ljava/lang/String; = "rti"

.field private static final GPS_EXTRA_TIME:Ljava/lang/String; = "time"

.field private static final GPS_EXTRA_UTC:Ljava/lang/String; = "utc"

.field private static final GRAVITE_Y_OFFSET:I = 0x96

.field private static final HANDLE_CHECK_SATEREPORT:I = 0x3eb

.field private static final HANDLE_CLEAR:I = 0x3ea

.field private static final HANDLE_COMMAND_GETVERSION:I = 0x44e

.field private static final HANDLE_COMMAND_JAMMINGSCAN:I = 0x44d

.field private static final HANDLE_COMMAND_OTHERS:I = 0x44f

.field private static final HANDLE_COUNTER:I = 0x3e8

.field private static final HANDLE_EXCEED_PERIOD:I = 0x438

.field private static final HANDLE_MSG_DELAY:I = 0xc8

.field private static final HANDLE_SET_COUNTDOWN:I = 0x41a

.field private static final HANDLE_SET_CURRENT_TIMES:I = 0x406

.field private static final HANDLE_SET_MEANTTFF:I = 0x42e

.field private static final HANDLE_SET_PARAM_RECONNECT:I = 0x442

.field private static final HANDLE_START_BUTTON_UPDATE:I = 0x410

.field private static final HANDLE_UPDATE_RESULT:I = 0x3e9

.field private static final INPUT_VALUE_MAX:I = 0x3e7

.field private static final INPUT_VALUE_MIN:I = 0x0

.field private static final INTENT_ACTION_SCREEN_OFF:Ljava/lang/String; = "android.intent.action.SCREEN_OFF"

.field private static final LOCATION_MAX_LENGTH:I = 0xc

.field private static final MESSAGE_ARG_0:I = 0x0

.field private static final MESSAGE_ARG_1:I = 0x1

.field private static final NMEALOG_PATH:Ljava/lang/String; = "/data/misc/nmea_log"

.field private static final NMEALOG_SD:Z = true

.field private static final NMEA_LOG_PREX:Ljava/lang/String; = "Nmealog"

.field private static final NMEA_LOG_SUFX:Ljava/lang/String; = ".txt"

.field private static final ONE_SECOND:I = 0x3e8

.field private static final RESPONSE_ARRAY_LENGTH:I = 0x4

.field private static final SATE_RATE_TIMEOUT:I = 0x3

.field private static final SHARED_PREF_KEY_BG:Ljava/lang/String; = "RunInBG"

.field private static final TAG:Ljava/lang/String; = "YGPS/Activity"

.field private static final TAG_BG:Ljava/lang/String; = "EM/YGPS_BG"

.field private static final YEAR_START:I = 0x76c


# instance fields
.field private mAutoTestHandler:Landroid/os/Handler;

.field private mAutoTestThread:Lcom/mediatek/ygps/YgpsActivity$AutoTestThread;

.field private mAzimuth:[F

.field public final mBtnClickListener:Landroid/view/View$OnClickListener;

.field private mBtnColdStart:Landroid/widget/Button;

.field private mBtnFullStart:Landroid/widget/Button;

.field private mBtnGpsHwTest:Landroid/widget/Button;

.field private mBtnGpsJamming:Landroid/widget/Button;

.field private mBtnGpsTestStart:Landroid/widget/Button;

.field private mBtnGpsTestStop:Landroid/widget/Button;

.field private mBtnHotStart:Landroid/widget/Button;

.field private mBtnHotStill:Landroid/widget/Button;

.field private mBtnNMEADbgDbg:Landroid/widget/Button;

.field private mBtnNMEAStop:Landroid/widget/Button;

.field private mBtnNmeaClear:Landroid/widget/Button;

.field private mBtnNmeaDbgDbgFile:Landroid/widget/Button;

.field private mBtnNmeaDbgNmea:Landroid/widget/Button;

.field private mBtnNmeaDbgNmeaDdms:Landroid/widget/Button;

.field private mBtnNmeaSave:Landroid/widget/Button;

.field private mBtnNmeaStart:Landroid/widget/Button;

.field private mBtnReStart:Landroid/widget/Button;

.field private mBtnSuplLog:Landroid/widget/Button;

.field private mBtnWarmStart:Landroid/widget/Button;

.field private mCbNeed3DFix:Landroid/widget/CheckBox;

.field private mCurrentTimes:I

.field private mElevation:[F

.field private mEtGpsJammingTimes:Landroid/widget/EditText;

.field private mEtTestInterval:Landroid/widget/EditText;

.field private mEtTestTimes:Landroid/widget/EditText;

.field private mFirstFix:Z

.field public final mGpsListener:Landroid/location/GpsStatus$Listener;

.field private mHandler:Landroid/os/Handler;

.field private mIsExit:Z

.field private mIsNeed3DFix:Z

.field private mIsRunInBg:Z

.field private mIsShowVersion:Z

.field private mIsTestRunning:Z

.field private mLastLocation:Landroid/location/Location;

.field private mLastTimestamp:J

.field public final mLocListener:Landroid/location/LocationListener;

.field private mLocationManager:Landroid/location/LocationManager;

.field private mMeanTTFF:F

.field private mNmeaListener:Lcom/mediatek/ygps/YgpsActivity$NmeaListenClass;

.field private mOutputNMEALog:Ljava/io/FileOutputStream;

.field private mPowerKeyFilter:Landroid/content/IntentFilter;

.field private mPowerKeyReceiver:Landroid/content/BroadcastReceiver;

.field private mPrns:[I

.field private mPrompt:Landroid/widget/Toast;

.field private mProvider:Ljava/lang/String;

.field private mSateReportTimeOut:I

.field private mSatelliteView:Lcom/mediatek/ygps/SatelliteSkyView;

.field private mSatellites:I

.field private mShowFirstFixLocate:Z

.field private mShowLoc:Z

.field private mSignalView:Lcom/mediatek/ygps/SatelliteSignalView;

.field private mSnrs:[F

.field private mSocketClient:Lcom/mediatek/ygps/ClientSocket;

.field private mStartNmeaRecord:Z

.field private mStartPressedHandling:Z

.field private mStatus:Ljava/lang/String;

.field private mStatusPrompt:Landroid/widget/Toast;

.field private mStopPressedHandling:Z

.field private mTVNMEAHint:Landroid/widget/TextView;

.field private mTestInterval:I

.field private mTotalTimes:I

.field private mTtffValue:I

.field private mTvNmeaLog:Landroid/widget/TextView;

.field private mUsedInFixMask:[I

.field private mYgpsWakeLock:Lcom/mediatek/ygps/YgpsActivity$YgpsWakeLock;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    new-instance v0, Ljava/text/SimpleDateFormat;

    const-string v1, "yyyyMMddhhmmss"

    invoke-direct {v0, v1}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;)V

    sput-object v0, Lcom/mediatek/ygps/YgpsActivity;->DATE_FORMAT:Ljava/text/DateFormat;

    return-void
.end method

.method public constructor <init>()V
    .locals 4

    const/16 v3, 0x100

    const/4 v2, 0x0

    const/4 v1, 0x0

    invoke-direct {p0}, Landroid/app/TabActivity;-><init>()V

    const/16 v0, 0x8

    new-array v0, v0, [I

    iput-object v0, p0, Lcom/mediatek/ygps/YgpsActivity;->mUsedInFixMask:[I

    iput v2, p0, Lcom/mediatek/ygps/YgpsActivity;->mTtffValue:I

    iput v2, p0, Lcom/mediatek/ygps/YgpsActivity;->mSatellites:I

    iput v2, p0, Lcom/mediatek/ygps/YgpsActivity;->mTotalTimes:I

    iput v2, p0, Lcom/mediatek/ygps/YgpsActivity;->mCurrentTimes:I

    iput v2, p0, Lcom/mediatek/ygps/YgpsActivity;->mTestInterval:I

    const/4 v0, 0x0

    iput v0, p0, Lcom/mediatek/ygps/YgpsActivity;->mMeanTTFF:F

    new-array v0, v3, [I

    iput-object v0, p0, Lcom/mediatek/ygps/YgpsActivity;->mPrns:[I

    new-array v0, v3, [F

    iput-object v0, p0, Lcom/mediatek/ygps/YgpsActivity;->mSnrs:[F

    new-array v0, v3, [F

    iput-object v0, p0, Lcom/mediatek/ygps/YgpsActivity;->mElevation:[F

    new-array v0, v3, [F

    iput-object v0, p0, Lcom/mediatek/ygps/YgpsActivity;->mAzimuth:[F

    iput-boolean v2, p0, Lcom/mediatek/ygps/YgpsActivity;->mShowLoc:Z

    iput-boolean v2, p0, Lcom/mediatek/ygps/YgpsActivity;->mStartNmeaRecord:Z

    iput-boolean v2, p0, Lcom/mediatek/ygps/YgpsActivity;->mFirstFix:Z

    iput-boolean v2, p0, Lcom/mediatek/ygps/YgpsActivity;->mIsNeed3DFix:Z

    iput-boolean v2, p0, Lcom/mediatek/ygps/YgpsActivity;->mIsTestRunning:Z

    iput-boolean v2, p0, Lcom/mediatek/ygps/YgpsActivity;->mIsExit:Z

    iput-boolean v2, p0, Lcom/mediatek/ygps/YgpsActivity;->mIsRunInBg:Z

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/mediatek/ygps/YgpsActivity;->mShowFirstFixLocate:Z

    iput-boolean v2, p0, Lcom/mediatek/ygps/YgpsActivity;->mIsShowVersion:Z

    iput v2, p0, Lcom/mediatek/ygps/YgpsActivity;->mSateReportTimeOut:I

    iput-object v1, p0, Lcom/mediatek/ygps/YgpsActivity;->mSocketClient:Lcom/mediatek/ygps/ClientSocket;

    iput-object v1, p0, Lcom/mediatek/ygps/YgpsActivity;->mSatelliteView:Lcom/mediatek/ygps/SatelliteSkyView;

    iput-object v1, p0, Lcom/mediatek/ygps/YgpsActivity;->mSignalView:Lcom/mediatek/ygps/SatelliteSignalView;

    iput-object v1, p0, Lcom/mediatek/ygps/YgpsActivity;->mNmeaListener:Lcom/mediatek/ygps/YgpsActivity$NmeaListenClass;

    iput-object v1, p0, Lcom/mediatek/ygps/YgpsActivity;->mLocationManager:Landroid/location/LocationManager;

    iput-object v1, p0, Lcom/mediatek/ygps/YgpsActivity;->mYgpsWakeLock:Lcom/mediatek/ygps/YgpsActivity$YgpsWakeLock;

    iput-object v1, p0, Lcom/mediatek/ygps/YgpsActivity;->mLastLocation:Landroid/location/Location;

    iput-object v1, p0, Lcom/mediatek/ygps/YgpsActivity;->mBtnColdStart:Landroid/widget/Button;

    iput-object v1, p0, Lcom/mediatek/ygps/YgpsActivity;->mBtnWarmStart:Landroid/widget/Button;

    iput-object v1, p0, Lcom/mediatek/ygps/YgpsActivity;->mBtnHotStart:Landroid/widget/Button;

    iput-object v1, p0, Lcom/mediatek/ygps/YgpsActivity;->mBtnFullStart:Landroid/widget/Button;

    iput-object v1, p0, Lcom/mediatek/ygps/YgpsActivity;->mBtnReStart:Landroid/widget/Button;

    iput-object v1, p0, Lcom/mediatek/ygps/YgpsActivity;->mBtnHotStill:Landroid/widget/Button;

    iput-object v1, p0, Lcom/mediatek/ygps/YgpsActivity;->mBtnNmeaStart:Landroid/widget/Button;

    iput-object v1, p0, Lcom/mediatek/ygps/YgpsActivity;->mBtnNMEAStop:Landroid/widget/Button;

    iput-object v1, p0, Lcom/mediatek/ygps/YgpsActivity;->mBtnNMEADbgDbg:Landroid/widget/Button;

    iput-object v1, p0, Lcom/mediatek/ygps/YgpsActivity;->mBtnNmeaDbgNmea:Landroid/widget/Button;

    iput-object v1, p0, Lcom/mediatek/ygps/YgpsActivity;->mBtnNmeaDbgDbgFile:Landroid/widget/Button;

    iput-object v1, p0, Lcom/mediatek/ygps/YgpsActivity;->mBtnNmeaDbgNmeaDdms:Landroid/widget/Button;

    iput-object v1, p0, Lcom/mediatek/ygps/YgpsActivity;->mBtnNmeaClear:Landroid/widget/Button;

    iput-object v1, p0, Lcom/mediatek/ygps/YgpsActivity;->mBtnNmeaSave:Landroid/widget/Button;

    iput-object v1, p0, Lcom/mediatek/ygps/YgpsActivity;->mBtnGpsTestStart:Landroid/widget/Button;

    iput-object v1, p0, Lcom/mediatek/ygps/YgpsActivity;->mBtnGpsTestStop:Landroid/widget/Button;

    iput-object v1, p0, Lcom/mediatek/ygps/YgpsActivity;->mBtnSuplLog:Landroid/widget/Button;

    iput-object v1, p0, Lcom/mediatek/ygps/YgpsActivity;->mEtTestTimes:Landroid/widget/EditText;

    iput-object v1, p0, Lcom/mediatek/ygps/YgpsActivity;->mCbNeed3DFix:Landroid/widget/CheckBox;

    iput-object v1, p0, Lcom/mediatek/ygps/YgpsActivity;->mEtTestInterval:Landroid/widget/EditText;

    iput-object v1, p0, Lcom/mediatek/ygps/YgpsActivity;->mAutoTestThread:Lcom/mediatek/ygps/YgpsActivity$AutoTestThread;

    const-string v0, ""

    iput-object v0, p0, Lcom/mediatek/ygps/YgpsActivity;->mProvider:Ljava/lang/String;

    const-string v0, ""

    iput-object v0, p0, Lcom/mediatek/ygps/YgpsActivity;->mStatus:Ljava/lang/String;

    iput-boolean v2, p0, Lcom/mediatek/ygps/YgpsActivity;->mStopPressedHandling:Z

    iput-boolean v2, p0, Lcom/mediatek/ygps/YgpsActivity;->mStartPressedHandling:Z

    iput-object v1, p0, Lcom/mediatek/ygps/YgpsActivity;->mTvNmeaLog:Landroid/widget/TextView;

    iput-object v1, p0, Lcom/mediatek/ygps/YgpsActivity;->mTVNMEAHint:Landroid/widget/TextView;

    iput-object v1, p0, Lcom/mediatek/ygps/YgpsActivity;->mOutputNMEALog:Ljava/io/FileOutputStream;

    iput-object v1, p0, Lcom/mediatek/ygps/YgpsActivity;->mBtnGpsHwTest:Landroid/widget/Button;

    iput-object v1, p0, Lcom/mediatek/ygps/YgpsActivity;->mBtnGpsJamming:Landroid/widget/Button;

    iput-object v1, p0, Lcom/mediatek/ygps/YgpsActivity;->mEtGpsJammingTimes:Landroid/widget/EditText;

    iput-object v1, p0, Lcom/mediatek/ygps/YgpsActivity;->mPowerKeyFilter:Landroid/content/IntentFilter;

    iput-object v1, p0, Lcom/mediatek/ygps/YgpsActivity;->mPowerKeyReceiver:Landroid/content/BroadcastReceiver;

    iput-object v1, p0, Lcom/mediatek/ygps/YgpsActivity;->mPrompt:Landroid/widget/Toast;

    iput-object v1, p0, Lcom/mediatek/ygps/YgpsActivity;->mStatusPrompt:Landroid/widget/Toast;

    new-instance v0, Lcom/mediatek/ygps/YgpsActivity$1;

    invoke-direct {v0, p0}, Lcom/mediatek/ygps/YgpsActivity$1;-><init>(Lcom/mediatek/ygps/YgpsActivity;)V

    iput-object v0, p0, Lcom/mediatek/ygps/YgpsActivity;->mAutoTestHandler:Landroid/os/Handler;

    const-wide/16 v0, -0x1

    iput-wide v0, p0, Lcom/mediatek/ygps/YgpsActivity;->mLastTimestamp:J

    new-instance v0, Lcom/mediatek/ygps/YgpsActivity$4;

    invoke-direct {v0, p0}, Lcom/mediatek/ygps/YgpsActivity$4;-><init>(Lcom/mediatek/ygps/YgpsActivity;)V

    iput-object v0, p0, Lcom/mediatek/ygps/YgpsActivity;->mLocListener:Landroid/location/LocationListener;

    new-instance v0, Lcom/mediatek/ygps/YgpsActivity$5;

    invoke-direct {v0, p0}, Lcom/mediatek/ygps/YgpsActivity$5;-><init>(Lcom/mediatek/ygps/YgpsActivity;)V

    iput-object v0, p0, Lcom/mediatek/ygps/YgpsActivity;->mGpsListener:Landroid/location/GpsStatus$Listener;

    new-instance v0, Lcom/mediatek/ygps/YgpsActivity$6;

    invoke-direct {v0, p0}, Lcom/mediatek/ygps/YgpsActivity$6;-><init>(Lcom/mediatek/ygps/YgpsActivity;)V

    iput-object v0, p0, Lcom/mediatek/ygps/YgpsActivity;->mBtnClickListener:Landroid/view/View$OnClickListener;

    new-instance v0, Lcom/mediatek/ygps/YgpsActivity$7;

    invoke-direct {v0, p0}, Lcom/mediatek/ygps/YgpsActivity$7;-><init>(Lcom/mediatek/ygps/YgpsActivity;)V

    iput-object v0, p0, Lcom/mediatek/ygps/YgpsActivity;->mHandler:Landroid/os/Handler;

    return-void
.end method

.method static synthetic access$100(Lcom/mediatek/ygps/YgpsActivity;)Landroid/widget/Button;
    .locals 1
    .param p0    # Lcom/mediatek/ygps/YgpsActivity;

    iget-object v0, p0, Lcom/mediatek/ygps/YgpsActivity;->mBtnGpsTestStart:Landroid/widget/Button;

    return-object v0
.end method

.method static synthetic access$1000(Lcom/mediatek/ygps/YgpsActivity;)V
    .locals 0
    .param p0    # Lcom/mediatek/ygps/YgpsActivity;

    invoke-direct {p0}, Lcom/mediatek/ygps/YgpsActivity;->showVersion()V

    return-void
.end method

.method static synthetic access$1100(Lcom/mediatek/ygps/YgpsActivity;)Z
    .locals 1
    .param p0    # Lcom/mediatek/ygps/YgpsActivity;

    iget-boolean v0, p0, Lcom/mediatek/ygps/YgpsActivity;->mStartNmeaRecord:Z

    return v0
.end method

.method static synthetic access$1102(Lcom/mediatek/ygps/YgpsActivity;Z)Z
    .locals 0
    .param p0    # Lcom/mediatek/ygps/YgpsActivity;
    .param p1    # Z

    iput-boolean p1, p0, Lcom/mediatek/ygps/YgpsActivity;->mStartNmeaRecord:Z

    return p1
.end method

.method static synthetic access$1200(Lcom/mediatek/ygps/YgpsActivity;Ljava/lang/String;)V
    .locals 0
    .param p0    # Lcom/mediatek/ygps/YgpsActivity;
    .param p1    # Ljava/lang/String;

    invoke-direct {p0, p1}, Lcom/mediatek/ygps/YgpsActivity;->saveNMEALog(Ljava/lang/String;)V

    return-void
.end method

.method static synthetic access$1300(Lcom/mediatek/ygps/YgpsActivity;)Landroid/widget/TextView;
    .locals 1
    .param p0    # Lcom/mediatek/ygps/YgpsActivity;

    iget-object v0, p0, Lcom/mediatek/ygps/YgpsActivity;->mTvNmeaLog:Landroid/widget/TextView;

    return-object v0
.end method

.method static synthetic access$1400(Lcom/mediatek/ygps/YgpsActivity;)Z
    .locals 1
    .param p0    # Lcom/mediatek/ygps/YgpsActivity;

    iget-boolean v0, p0, Lcom/mediatek/ygps/YgpsActivity;->mFirstFix:Z

    return v0
.end method

.method static synthetic access$1402(Lcom/mediatek/ygps/YgpsActivity;Z)Z
    .locals 0
    .param p0    # Lcom/mediatek/ygps/YgpsActivity;
    .param p1    # Z

    iput-boolean p1, p0, Lcom/mediatek/ygps/YgpsActivity;->mFirstFix:Z

    return p1
.end method

.method static synthetic access$1500(Lcom/mediatek/ygps/YgpsActivity;)Z
    .locals 1
    .param p0    # Lcom/mediatek/ygps/YgpsActivity;

    iget-boolean v0, p0, Lcom/mediatek/ygps/YgpsActivity;->mShowLoc:Z

    return v0
.end method

.method static synthetic access$1600(Lcom/mediatek/ygps/YgpsActivity;)Landroid/widget/Toast;
    .locals 1
    .param p0    # Lcom/mediatek/ygps/YgpsActivity;

    iget-object v0, p0, Lcom/mediatek/ygps/YgpsActivity;->mPrompt:Landroid/widget/Toast;

    return-object v0
.end method

.method static synthetic access$1602(Lcom/mediatek/ygps/YgpsActivity;Landroid/widget/Toast;)Landroid/widget/Toast;
    .locals 0
    .param p0    # Lcom/mediatek/ygps/YgpsActivity;
    .param p1    # Landroid/widget/Toast;

    iput-object p1, p0, Lcom/mediatek/ygps/YgpsActivity;->mPrompt:Landroid/widget/Toast;

    return-object p1
.end method

.method static synthetic access$1700(Lcom/mediatek/ygps/YgpsActivity;)Z
    .locals 1
    .param p0    # Lcom/mediatek/ygps/YgpsActivity;

    iget-boolean v0, p0, Lcom/mediatek/ygps/YgpsActivity;->mShowFirstFixLocate:Z

    return v0
.end method

.method static synthetic access$1702(Lcom/mediatek/ygps/YgpsActivity;Z)Z
    .locals 0
    .param p0    # Lcom/mediatek/ygps/YgpsActivity;
    .param p1    # Z

    iput-boolean p1, p0, Lcom/mediatek/ygps/YgpsActivity;->mShowFirstFixLocate:Z

    return p1
.end method

.method static synthetic access$1800(Lcom/mediatek/ygps/YgpsActivity;)Landroid/location/Location;
    .locals 1
    .param p0    # Lcom/mediatek/ygps/YgpsActivity;

    iget-object v0, p0, Lcom/mediatek/ygps/YgpsActivity;->mLastLocation:Landroid/location/Location;

    return-object v0
.end method

.method static synthetic access$1802(Lcom/mediatek/ygps/YgpsActivity;Landroid/location/Location;)Landroid/location/Location;
    .locals 0
    .param p0    # Lcom/mediatek/ygps/YgpsActivity;
    .param p1    # Landroid/location/Location;

    iput-object p1, p0, Lcom/mediatek/ygps/YgpsActivity;->mLastLocation:Landroid/location/Location;

    return-object p1
.end method

.method static synthetic access$1900(Lcom/mediatek/ygps/YgpsActivity;)I
    .locals 1
    .param p0    # Lcom/mediatek/ygps/YgpsActivity;

    iget v0, p0, Lcom/mediatek/ygps/YgpsActivity;->mTtffValue:I

    return v0
.end method

.method static synthetic access$1902(Lcom/mediatek/ygps/YgpsActivity;I)I
    .locals 0
    .param p0    # Lcom/mediatek/ygps/YgpsActivity;
    .param p1    # I

    iput p1, p0, Lcom/mediatek/ygps/YgpsActivity;->mTtffValue:I

    return p1
.end method

.method static synthetic access$1912(Lcom/mediatek/ygps/YgpsActivity;I)I
    .locals 1
    .param p0    # Lcom/mediatek/ygps/YgpsActivity;
    .param p1    # I

    iget v0, p0, Lcom/mediatek/ygps/YgpsActivity;->mTtffValue:I

    add-int/2addr v0, p1

    iput v0, p0, Lcom/mediatek/ygps/YgpsActivity;->mTtffValue:I

    return v0
.end method

.method static synthetic access$200(Lcom/mediatek/ygps/YgpsActivity;)Landroid/widget/Button;
    .locals 1
    .param p0    # Lcom/mediatek/ygps/YgpsActivity;

    iget-object v0, p0, Lcom/mediatek/ygps/YgpsActivity;->mBtnGpsTestStop:Landroid/widget/Button;

    return-object v0
.end method

.method static synthetic access$2000(Lcom/mediatek/ygps/YgpsActivity;)Ljava/lang/String;
    .locals 1
    .param p0    # Lcom/mediatek/ygps/YgpsActivity;

    iget-object v0, p0, Lcom/mediatek/ygps/YgpsActivity;->mProvider:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$2002(Lcom/mediatek/ygps/YgpsActivity;Ljava/lang/String;)Ljava/lang/String;
    .locals 0
    .param p0    # Lcom/mediatek/ygps/YgpsActivity;
    .param p1    # Ljava/lang/String;

    iput-object p1, p0, Lcom/mediatek/ygps/YgpsActivity;->mProvider:Ljava/lang/String;

    return-object p1
.end method

.method static synthetic access$2100(Lcom/mediatek/ygps/YgpsActivity;)Ljava/lang/String;
    .locals 1
    .param p0    # Lcom/mediatek/ygps/YgpsActivity;

    iget-object v0, p0, Lcom/mediatek/ygps/YgpsActivity;->mStatus:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$2102(Lcom/mediatek/ygps/YgpsActivity;Ljava/lang/String;)Ljava/lang/String;
    .locals 0
    .param p0    # Lcom/mediatek/ygps/YgpsActivity;
    .param p1    # Ljava/lang/String;

    iput-object p1, p0, Lcom/mediatek/ygps/YgpsActivity;->mStatus:Ljava/lang/String;

    return-object p1
.end method

.method static synthetic access$2200(Lcom/mediatek/ygps/YgpsActivity;)I
    .locals 1
    .param p0    # Lcom/mediatek/ygps/YgpsActivity;

    iget v0, p0, Lcom/mediatek/ygps/YgpsActivity;->mCurrentTimes:I

    return v0
.end method

.method static synthetic access$2300(Lcom/mediatek/ygps/YgpsActivity;)Landroid/os/Handler;
    .locals 1
    .param p0    # Lcom/mediatek/ygps/YgpsActivity;

    iget-object v0, p0, Lcom/mediatek/ygps/YgpsActivity;->mHandler:Landroid/os/Handler;

    return-object v0
.end method

.method static synthetic access$2400(Lcom/mediatek/ygps/YgpsActivity;)Z
    .locals 1
    .param p0    # Lcom/mediatek/ygps/YgpsActivity;

    iget-boolean v0, p0, Lcom/mediatek/ygps/YgpsActivity;->mIsTestRunning:Z

    return v0
.end method

.method static synthetic access$2402(Lcom/mediatek/ygps/YgpsActivity;Z)Z
    .locals 0
    .param p0    # Lcom/mediatek/ygps/YgpsActivity;
    .param p1    # Z

    iput-boolean p1, p0, Lcom/mediatek/ygps/YgpsActivity;->mIsTestRunning:Z

    return p1
.end method

.method static synthetic access$2500(Lcom/mediatek/ygps/YgpsActivity;I)F
    .locals 1
    .param p0    # Lcom/mediatek/ygps/YgpsActivity;
    .param p1    # I

    invoke-direct {p0, p1}, Lcom/mediatek/ygps/YgpsActivity;->meanTTFF(I)F

    move-result v0

    return v0
.end method

.method static synthetic access$2600(Lcom/mediatek/ygps/YgpsActivity;)Landroid/location/LocationManager;
    .locals 1
    .param p0    # Lcom/mediatek/ygps/YgpsActivity;

    iget-object v0, p0, Lcom/mediatek/ygps/YgpsActivity;->mLocationManager:Landroid/location/LocationManager;

    return-object v0
.end method

.method static synthetic access$2700(Lcom/mediatek/ygps/YgpsActivity;)I
    .locals 1
    .param p0    # Lcom/mediatek/ygps/YgpsActivity;

    iget v0, p0, Lcom/mediatek/ygps/YgpsActivity;->mSateReportTimeOut:I

    return v0
.end method

.method static synthetic access$2702(Lcom/mediatek/ygps/YgpsActivity;I)I
    .locals 0
    .param p0    # Lcom/mediatek/ygps/YgpsActivity;
    .param p1    # I

    iput p1, p0, Lcom/mediatek/ygps/YgpsActivity;->mSateReportTimeOut:I

    return p1
.end method

.method static synthetic access$2708(Lcom/mediatek/ygps/YgpsActivity;)I
    .locals 2
    .param p0    # Lcom/mediatek/ygps/YgpsActivity;

    iget v0, p0, Lcom/mediatek/ygps/YgpsActivity;->mSateReportTimeOut:I

    add-int/lit8 v1, v0, 0x1

    iput v1, p0, Lcom/mediatek/ygps/YgpsActivity;->mSateReportTimeOut:I

    return v0
.end method

.method static synthetic access$2800(Lcom/mediatek/ygps/YgpsActivity;)V
    .locals 0
    .param p0    # Lcom/mediatek/ygps/YgpsActivity;

    invoke-direct {p0}, Lcom/mediatek/ygps/YgpsActivity;->clearLayout()V

    return-void
.end method

.method static synthetic access$2900(Lcom/mediatek/ygps/YgpsActivity;)Landroid/widget/Toast;
    .locals 1
    .param p0    # Lcom/mediatek/ygps/YgpsActivity;

    iget-object v0, p0, Lcom/mediatek/ygps/YgpsActivity;->mStatusPrompt:Landroid/widget/Toast;

    return-object v0
.end method

.method static synthetic access$300(Lcom/mediatek/ygps/YgpsActivity;)V
    .locals 0
    .param p0    # Lcom/mediatek/ygps/YgpsActivity;

    invoke-direct {p0}, Lcom/mediatek/ygps/YgpsActivity;->setViewToStopState()V

    return-void
.end method

.method static synthetic access$3000(Lcom/mediatek/ygps/YgpsActivity;)V
    .locals 0
    .param p0    # Lcom/mediatek/ygps/YgpsActivity;

    invoke-direct {p0}, Lcom/mediatek/ygps/YgpsActivity;->startGPSAutoTest()V

    return-void
.end method

.method static synthetic access$3100(Lcom/mediatek/ygps/YgpsActivity;)Z
    .locals 1
    .param p0    # Lcom/mediatek/ygps/YgpsActivity;

    iget-boolean v0, p0, Lcom/mediatek/ygps/YgpsActivity;->mStopPressedHandling:Z

    return v0
.end method

.method static synthetic access$3102(Lcom/mediatek/ygps/YgpsActivity;Z)Z
    .locals 0
    .param p0    # Lcom/mediatek/ygps/YgpsActivity;
    .param p1    # Z

    iput-boolean p1, p0, Lcom/mediatek/ygps/YgpsActivity;->mStopPressedHandling:Z

    return p1
.end method

.method static synthetic access$3200(Lcom/mediatek/ygps/YgpsActivity;)Landroid/widget/Button;
    .locals 1
    .param p0    # Lcom/mediatek/ygps/YgpsActivity;

    iget-object v0, p0, Lcom/mediatek/ygps/YgpsActivity;->mBtnHotStart:Landroid/widget/Button;

    return-object v0
.end method

.method static synthetic access$3300(Lcom/mediatek/ygps/YgpsActivity;)Z
    .locals 1
    .param p0    # Lcom/mediatek/ygps/YgpsActivity;

    invoke-direct {p0}, Lcom/mediatek/ygps/YgpsActivity;->gpsTestRunning()Z

    move-result v0

    return v0
.end method

.method static synthetic access$3400(Lcom/mediatek/ygps/YgpsActivity;Z)V
    .locals 0
    .param p0    # Lcom/mediatek/ygps/YgpsActivity;
    .param p1    # Z

    invoke-direct {p0, p1}, Lcom/mediatek/ygps/YgpsActivity;->enableBtns(Z)V

    return-void
.end method

.method static synthetic access$3500(Lcom/mediatek/ygps/YgpsActivity;)Landroid/widget/Button;
    .locals 1
    .param p0    # Lcom/mediatek/ygps/YgpsActivity;

    iget-object v0, p0, Lcom/mediatek/ygps/YgpsActivity;->mBtnWarmStart:Landroid/widget/Button;

    return-object v0
.end method

.method static synthetic access$3600(Lcom/mediatek/ygps/YgpsActivity;)Landroid/widget/Button;
    .locals 1
    .param p0    # Lcom/mediatek/ygps/YgpsActivity;

    iget-object v0, p0, Lcom/mediatek/ygps/YgpsActivity;->mBtnColdStart:Landroid/widget/Button;

    return-object v0
.end method

.method static synthetic access$3700(Lcom/mediatek/ygps/YgpsActivity;)Landroid/widget/Button;
    .locals 1
    .param p0    # Lcom/mediatek/ygps/YgpsActivity;

    iget-object v0, p0, Lcom/mediatek/ygps/YgpsActivity;->mBtnFullStart:Landroid/widget/Button;

    return-object v0
.end method

.method static synthetic access$3800(Lcom/mediatek/ygps/YgpsActivity;)Landroid/widget/Button;
    .locals 1
    .param p0    # Lcom/mediatek/ygps/YgpsActivity;

    iget-object v0, p0, Lcom/mediatek/ygps/YgpsActivity;->mBtnReStart:Landroid/widget/Button;

    return-object v0
.end method

.method static synthetic access$3900(Lcom/mediatek/ygps/YgpsActivity;)Landroid/widget/Button;
    .locals 1
    .param p0    # Lcom/mediatek/ygps/YgpsActivity;

    iget-object v0, p0, Lcom/mediatek/ygps/YgpsActivity;->mBtnNmeaStart:Landroid/widget/Button;

    return-object v0
.end method

.method static synthetic access$400(Lcom/mediatek/ygps/YgpsActivity;)F
    .locals 1
    .param p0    # Lcom/mediatek/ygps/YgpsActivity;

    iget v0, p0, Lcom/mediatek/ygps/YgpsActivity;->mMeanTTFF:F

    return v0
.end method

.method static synthetic access$4000(Lcom/mediatek/ygps/YgpsActivity;)Z
    .locals 1
    .param p0    # Lcom/mediatek/ygps/YgpsActivity;

    invoke-direct {p0}, Lcom/mediatek/ygps/YgpsActivity;->createFileForSavingNMEALog()Z

    move-result v0

    return v0
.end method

.method static synthetic access$402(Lcom/mediatek/ygps/YgpsActivity;F)F
    .locals 0
    .param p0    # Lcom/mediatek/ygps/YgpsActivity;
    .param p1    # F

    iput p1, p0, Lcom/mediatek/ygps/YgpsActivity;->mMeanTTFF:F

    return p1
.end method

.method static synthetic access$4100(Lcom/mediatek/ygps/YgpsActivity;)Landroid/widget/Button;
    .locals 1
    .param p0    # Lcom/mediatek/ygps/YgpsActivity;

    iget-object v0, p0, Lcom/mediatek/ygps/YgpsActivity;->mBtnNMEAStop:Landroid/widget/Button;

    return-object v0
.end method

.method static synthetic access$4200(Lcom/mediatek/ygps/YgpsActivity;)V
    .locals 0
    .param p0    # Lcom/mediatek/ygps/YgpsActivity;

    invoke-direct {p0}, Lcom/mediatek/ygps/YgpsActivity;->finishSavingNMEALog()V

    return-void
.end method

.method static synthetic access$4300(Lcom/mediatek/ygps/YgpsActivity;)Landroid/widget/Button;
    .locals 1
    .param p0    # Lcom/mediatek/ygps/YgpsActivity;

    iget-object v0, p0, Lcom/mediatek/ygps/YgpsActivity;->mBtnNMEADbgDbg:Landroid/widget/Button;

    return-object v0
.end method

.method static synthetic access$4400(Lcom/mediatek/ygps/YgpsActivity;)Landroid/widget/Button;
    .locals 1
    .param p0    # Lcom/mediatek/ygps/YgpsActivity;

    iget-object v0, p0, Lcom/mediatek/ygps/YgpsActivity;->mBtnNmeaDbgNmea:Landroid/widget/Button;

    return-object v0
.end method

.method static synthetic access$4500(Lcom/mediatek/ygps/YgpsActivity;)Landroid/widget/Button;
    .locals 1
    .param p0    # Lcom/mediatek/ygps/YgpsActivity;

    iget-object v0, p0, Lcom/mediatek/ygps/YgpsActivity;->mBtnNmeaDbgDbgFile:Landroid/widget/Button;

    return-object v0
.end method

.method static synthetic access$4600(Lcom/mediatek/ygps/YgpsActivity;)Landroid/widget/Button;
    .locals 1
    .param p0    # Lcom/mediatek/ygps/YgpsActivity;

    iget-object v0, p0, Lcom/mediatek/ygps/YgpsActivity;->mBtnNmeaDbgNmeaDdms:Landroid/widget/Button;

    return-object v0
.end method

.method static synthetic access$4700(Lcom/mediatek/ygps/YgpsActivity;)Landroid/widget/Button;
    .locals 1
    .param p0    # Lcom/mediatek/ygps/YgpsActivity;

    iget-object v0, p0, Lcom/mediatek/ygps/YgpsActivity;->mBtnHotStill:Landroid/widget/Button;

    return-object v0
.end method

.method static synthetic access$4800(Lcom/mediatek/ygps/YgpsActivity;)Landroid/widget/Button;
    .locals 1
    .param p0    # Lcom/mediatek/ygps/YgpsActivity;

    iget-object v0, p0, Lcom/mediatek/ygps/YgpsActivity;->mBtnSuplLog:Landroid/widget/Button;

    return-object v0
.end method

.method static synthetic access$4900(Lcom/mediatek/ygps/YgpsActivity;)Landroid/widget/Button;
    .locals 1
    .param p0    # Lcom/mediatek/ygps/YgpsActivity;

    iget-object v0, p0, Lcom/mediatek/ygps/YgpsActivity;->mBtnNmeaClear:Landroid/widget/Button;

    return-object v0
.end method

.method static synthetic access$500(Lcom/mediatek/ygps/YgpsActivity;Landroid/os/Bundle;Z)V
    .locals 0
    .param p0    # Lcom/mediatek/ygps/YgpsActivity;
    .param p1    # Landroid/os/Bundle;
    .param p2    # Z

    invoke-direct {p0, p1, p2}, Lcom/mediatek/ygps/YgpsActivity;->resetParam(Landroid/os/Bundle;Z)V

    return-void
.end method

.method static synthetic access$5000(Lcom/mediatek/ygps/YgpsActivity;)Landroid/widget/Button;
    .locals 1
    .param p0    # Lcom/mediatek/ygps/YgpsActivity;

    iget-object v0, p0, Lcom/mediatek/ygps/YgpsActivity;->mBtnNmeaSave:Landroid/widget/Button;

    return-object v0
.end method

.method static synthetic access$5100(Lcom/mediatek/ygps/YgpsActivity;)V
    .locals 0
    .param p0    # Lcom/mediatek/ygps/YgpsActivity;

    invoke-direct {p0}, Lcom/mediatek/ygps/YgpsActivity;->saveNMEALog()V

    return-void
.end method

.method static synthetic access$5200(Lcom/mediatek/ygps/YgpsActivity;)Landroid/widget/Button;
    .locals 1
    .param p0    # Lcom/mediatek/ygps/YgpsActivity;

    iget-object v0, p0, Lcom/mediatek/ygps/YgpsActivity;->mBtnGpsHwTest:Landroid/widget/Button;

    return-object v0
.end method

.method static synthetic access$5300(Lcom/mediatek/ygps/YgpsActivity;)V
    .locals 0
    .param p0    # Lcom/mediatek/ygps/YgpsActivity;

    invoke-direct {p0}, Lcom/mediatek/ygps/YgpsActivity;->onGpsHwTestClicked()V

    return-void
.end method

.method static synthetic access$5400(Lcom/mediatek/ygps/YgpsActivity;)Landroid/widget/Button;
    .locals 1
    .param p0    # Lcom/mediatek/ygps/YgpsActivity;

    iget-object v0, p0, Lcom/mediatek/ygps/YgpsActivity;->mBtnGpsJamming:Landroid/widget/Button;

    return-object v0
.end method

.method static synthetic access$5500(Lcom/mediatek/ygps/YgpsActivity;)V
    .locals 0
    .param p0    # Lcom/mediatek/ygps/YgpsActivity;

    invoke-direct {p0}, Lcom/mediatek/ygps/YgpsActivity;->onGpsJammingScanClicked()V

    return-void
.end method

.method static synthetic access$600(Lcom/mediatek/ygps/YgpsActivity;Z)V
    .locals 0
    .param p0    # Lcom/mediatek/ygps/YgpsActivity;
    .param p1    # Z

    invoke-direct {p0, p1}, Lcom/mediatek/ygps/YgpsActivity;->setStartButtonEnable(Z)V

    return-void
.end method

.method static synthetic access$700(Lcom/mediatek/ygps/YgpsActivity;)V
    .locals 0
    .param p0    # Lcom/mediatek/ygps/YgpsActivity;

    invoke-direct {p0}, Lcom/mediatek/ygps/YgpsActivity;->reconnectTest()V

    return-void
.end method

.method static synthetic access$800(Lcom/mediatek/ygps/YgpsActivity;)Z
    .locals 1
    .param p0    # Lcom/mediatek/ygps/YgpsActivity;

    iget-boolean v0, p0, Lcom/mediatek/ygps/YgpsActivity;->mIsShowVersion:Z

    return v0
.end method

.method static synthetic access$802(Lcom/mediatek/ygps/YgpsActivity;Z)Z
    .locals 0
    .param p0    # Lcom/mediatek/ygps/YgpsActivity;
    .param p1    # Z

    iput-boolean p1, p0, Lcom/mediatek/ygps/YgpsActivity;->mIsShowVersion:Z

    return p1
.end method

.method static synthetic access$900(Lcom/mediatek/ygps/YgpsActivity;)J
    .locals 2
    .param p0    # Lcom/mediatek/ygps/YgpsActivity;

    iget-wide v0, p0, Lcom/mediatek/ygps/YgpsActivity;->mLastTimestamp:J

    return-wide v0
.end method

.method static synthetic access$902(Lcom/mediatek/ygps/YgpsActivity;J)J
    .locals 0
    .param p0    # Lcom/mediatek/ygps/YgpsActivity;
    .param p1    # J

    iput-wide p1, p0, Lcom/mediatek/ygps/YgpsActivity;->mLastTimestamp:J

    return-wide p1
.end method

.method private clearLayout()V
    .locals 2

    const v1, 0x7f050002

    const v0, 0x7f07000e

    invoke-virtual {p0, v0}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    const v0, 0x7f07000f

    invoke-virtual {p0, v0}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    const v0, 0x7f070012

    invoke-virtual {p0, v0}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    const v0, 0x7f070013

    invoke-virtual {p0, v0}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    const v0, 0x7f070014

    invoke-virtual {p0, v0}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    const v0, 0x7f070015

    invoke-virtual {p0, v0}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    const v0, 0x7f070016

    invoke-virtual {p0, v0}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    const v0, 0x7f070017

    invoke-virtual {p0, v0}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    const v0, 0x7f070018

    invoke-virtual {p0, v0}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    iget-boolean v0, p0, Lcom/mediatek/ygps/YgpsActivity;->mShowFirstFixLocate:Z

    if-eqz v0, :cond_0

    const v0, 0x7f070011

    invoke-virtual {p0, v0}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    const v0, 0x7f070010

    invoke-virtual {p0, v0}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    :cond_0
    return-void
.end method

.method private createFileForSavingNMEALog()Z
    .locals 10

    const/4 v5, 0x1

    const/4 v4, 0x0

    const-string v6, "YGPS/Activity"

    const-string v7, "Enter startSavingNMEALog function"

    invoke-static {v6, v7}, Lcom/mediatek/xlog/Xlog;->v(Ljava/lang/String;Ljava/lang/String;)I

    invoke-static {}, Landroid/os/Environment;->getExternalStorageState()Ljava/lang/String;

    move-result-object v6

    const-string v7, "mounted"

    invoke-virtual {v6, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-nez v6, :cond_0

    const-string v6, "YGPS/Activity"

    const-string v7, "saveNMEALog function: No SD card"

    invoke-static {v6, v7}, Lcom/mediatek/xlog/Xlog;->v(Ljava/lang/String;Ljava/lang/String;)I

    const v6, 0x7f05003d

    invoke-static {p0, v6, v5}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v5

    invoke-virtual {v5}, Landroid/widget/Toast;->show()V

    :goto_0
    return v4

    :cond_0
    sget-object v6, Lcom/mediatek/ygps/YgpsActivity;->DATE_FORMAT:Ljava/text/DateFormat;

    new-instance v7, Ljava/util/Date;

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v8

    invoke-direct {v7, v8, v9}, Ljava/util/Date;-><init>(J)V

    invoke-virtual {v6, v7}, Ljava/text/DateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v3

    const/4 v2, 0x0

    new-instance v2, Ljava/io/File;

    invoke-static {}, Landroid/os/Environment;->getExternalStorageDirectory()Ljava/io/File;

    move-result-object v6

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "Nmealog"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, ".txt"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-direct {v2, v6, v7}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    if-eqz v2, :cond_1

    :try_start_0
    invoke-virtual {v2}, Ljava/io/File;->createNewFile()Z
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    :cond_1
    :try_start_1
    new-instance v6, Ljava/io/FileOutputStream;

    invoke-direct {v6, v2}, Ljava/io/FileOutputStream;-><init>(Ljava/io/File;)V

    iput-object v6, p0, Lcom/mediatek/ygps/YgpsActivity;->mOutputNMEALog:Ljava/io/FileOutputStream;
    :try_end_1
    .catch Ljava/io/FileNotFoundException; {:try_start_1 .. :try_end_1} :catch_1

    iget-object v4, p0, Lcom/mediatek/ygps/YgpsActivity;->mTVNMEAHint:Landroid/widget/TextView;

    const v6, 0x7f050039

    invoke-virtual {v4, v6}, Landroid/widget/TextView;->setText(I)V

    move v4, v5

    goto :goto_0

    :catch_0
    move-exception v0

    const-string v6, "YGPS/Activity"

    const-string v7, "create new file failed!"

    invoke-static {v6, v7}, Lcom/mediatek/xlog/Xlog;->w(Ljava/lang/String;Ljava/lang/String;)I

    const v6, 0x7f05005f

    invoke-static {p0, v6, v5}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v5

    invoke-virtual {v5}, Landroid/widget/Toast;->show()V

    goto :goto_0

    :catch_1
    move-exception v1

    const-string v5, "YGPS/Activity"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "output stream FileNotFoundException: "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v1}, Ljava/lang/Throwable;->getMessage()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Lcom/mediatek/xlog/Xlog;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method private emptyArray()V
    .locals 4

    const/4 v3, 0x0

    const/4 v2, 0x0

    iput v3, p0, Lcom/mediatek/ygps/YgpsActivity;->mSatellites:I

    const/4 v0, 0x0

    :goto_0
    const/16 v1, 0x100

    if-ge v0, v1, :cond_1

    iget-object v1, p0, Lcom/mediatek/ygps/YgpsActivity;->mPrns:[I

    aput v3, v1, v0

    iget-object v1, p0, Lcom/mediatek/ygps/YgpsActivity;->mSnrs:[F

    aput v2, v1, v0

    iget-object v1, p0, Lcom/mediatek/ygps/YgpsActivity;->mElevation:[F

    aput v2, v1, v0

    iget-object v1, p0, Lcom/mediatek/ygps/YgpsActivity;->mAzimuth:[F

    aput v2, v1, v0

    const/16 v1, 0x8

    if-ge v0, v1, :cond_0

    iget-object v1, p0, Lcom/mediatek/ygps/YgpsActivity;->mUsedInFixMask:[I

    aput v3, v1, v0

    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_1
    return-void
.end method

.method private enableBtns(Z)V
    .locals 1
    .param p1    # Z

    iget-object v0, p0, Lcom/mediatek/ygps/YgpsActivity;->mBtnHotStart:Landroid/widget/Button;

    invoke-virtual {v0, p1}, Landroid/view/View;->setClickable(Z)V

    iget-object v0, p0, Lcom/mediatek/ygps/YgpsActivity;->mBtnWarmStart:Landroid/widget/Button;

    invoke-virtual {v0, p1}, Landroid/view/View;->setClickable(Z)V

    iget-object v0, p0, Lcom/mediatek/ygps/YgpsActivity;->mBtnColdStart:Landroid/widget/Button;

    invoke-virtual {v0, p1}, Landroid/view/View;->setClickable(Z)V

    iget-object v0, p0, Lcom/mediatek/ygps/YgpsActivity;->mBtnFullStart:Landroid/widget/Button;

    invoke-virtual {v0, p1}, Landroid/view/View;->setClickable(Z)V

    iget-object v0, p0, Lcom/mediatek/ygps/YgpsActivity;->mBtnReStart:Landroid/widget/Button;

    invoke-virtual {v0, p1}, Landroid/view/View;->setClickable(Z)V

    iget-object v0, p0, Lcom/mediatek/ygps/YgpsActivity;->mBtnHotStill:Landroid/widget/Button;

    invoke-virtual {v0, p1}, Landroid/view/View;->setClickable(Z)V

    return-void
.end method

.method private finishSavingNMEALog()V
    .locals 5

    const/4 v1, 0x0

    :try_start_0
    iput-boolean v1, p0, Lcom/mediatek/ygps/YgpsActivity;->mStartNmeaRecord:Z

    iget-object v1, p0, Lcom/mediatek/ygps/YgpsActivity;->mBtnNMEAStop:Landroid/widget/Button;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setEnabled(Z)V

    iget-object v1, p0, Lcom/mediatek/ygps/YgpsActivity;->mBtnNmeaStart:Landroid/widget/Button;

    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setEnabled(Z)V

    iget-object v1, p0, Lcom/mediatek/ygps/YgpsActivity;->mTVNMEAHint:Landroid/widget/TextView;

    const v2, 0x7f050002

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(I)V

    iget-object v1, p0, Lcom/mediatek/ygps/YgpsActivity;->mTvNmeaLog:Landroid/widget/TextView;

    const v2, 0x7f050002

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(I)V

    iget-object v1, p0, Lcom/mediatek/ygps/YgpsActivity;->mOutputNMEALog:Ljava/io/FileOutputStream;

    invoke-virtual {v1}, Ljava/io/FileOutputStream;->close()V

    const/4 v1, 0x0

    iput-object v1, p0, Lcom/mediatek/ygps/YgpsActivity;->mOutputNMEALog:Ljava/io/FileOutputStream;

    const v1, 0x7f050060

    invoke-virtual {p0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    invoke-static {}, Landroid/os/Environment;->getExternalStorageDirectory()Ljava/io/File;

    move-result-object v4

    invoke-virtual {v4}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x1

    invoke-static {p0, v1, v2}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v1

    invoke-virtual {v1}, Landroid/widget/Toast;->show()V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    return-void

    :catch_0
    move-exception v0

    const-string v1, "YGPS/Activity"

    const-string v2, "Close file failed!"

    invoke-static {v1, v2}, Lcom/mediatek/xlog/Xlog;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method private gpsTestRunning()Z
    .locals 2

    const/4 v0, 0x1

    iget-boolean v1, p0, Lcom/mediatek/ygps/YgpsActivity;->mIsTestRunning:Z

    if-eqz v1, :cond_0

    const v1, 0x7f050077

    invoke-static {p0, v1, v0}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v1

    invoke-virtual {v1}, Landroid/widget/Toast;->show()V

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private meanTTFF(I)F
    .locals 2
    .param p1    # I

    iget v0, p0, Lcom/mediatek/ygps/YgpsActivity;->mMeanTTFF:F

    add-int/lit8 v1, p1, -0x1

    int-to-float v1, v1

    mul-float/2addr v0, v1

    iget v1, p0, Lcom/mediatek/ygps/YgpsActivity;->mTtffValue:I

    int-to-float v1, v1

    add-float/2addr v0, v1

    int-to-float v1, p1

    div-float/2addr v0, v1

    return v0
.end method

.method private onGpsHwTestClicked()V
    .locals 5

    const-string v2, "test.mode"

    const-string v3, "0"

    invoke-static {v2, v3}, Lcom/mediatek/ygps/GpsMnlSetting;->getMnlProp(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    const-string v2, "0"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/mediatek/ygps/YgpsActivity;->mBtnGpsHwTest:Landroid/widget/Button;

    const v3, 0x7f05005b

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(I)V

    const-string v2, "test.mode"

    const-string v3, "1"

    invoke-static {v2, v3}, Lcom/mediatek/ygps/GpsMnlSetting;->setMnlProp(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v2, p0, Lcom/mediatek/ygps/YgpsActivity;->mBtnNmeaDbgDbgFile:Landroid/widget/Button;

    const v3, 0x7f050055

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(I)V

    const-string v2, "debug.dbg2file"

    const-string v3, "1"

    invoke-static {v2, v3}, Lcom/mediatek/ygps/GpsMnlSetting;->setMnlProp(Ljava/lang/String;Ljava/lang/String;)V

    :goto_0
    const-string v2, "first.time"

    const/4 v3, 0x0

    invoke-virtual {p0, v2, v3}, Landroid/content/ContextWrapper;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v2

    const-string v3, "first.time"

    const-string v4, "1"

    invoke-interface {v2, v3, v4}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    move-result-object v2

    invoke-interface {v2}, Landroid/content/SharedPreferences$Editor;->commit()Z

    return-void

    :cond_0
    iget-object v2, p0, Lcom/mediatek/ygps/YgpsActivity;->mBtnGpsHwTest:Landroid/widget/Button;

    const v3, 0x7f05005a

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(I)V

    const-string v2, "test.mode"

    const-string v3, "0"

    invoke-static {v2, v3}, Lcom/mediatek/ygps/GpsMnlSetting;->setMnlProp(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method private onGpsJammingScanClicked()V
    .locals 4

    const/4 v3, 0x1

    iget-object v1, p0, Lcom/mediatek/ygps/YgpsActivity;->mEtGpsJammingTimes:Landroid/widget/EditText;

    invoke-virtual {v1}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v1

    invoke-interface {v1}, Landroid/text/Editable;->length()I

    move-result v1

    if-nez v1, :cond_0

    const-string v1, "Please input Jamming scan times"

    invoke-static {p0, v1, v3}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v1

    invoke-virtual {v1}, Landroid/widget/Toast;->show()V

    :goto_0
    return-void

    :cond_0
    iget-object v1, p0, Lcom/mediatek/ygps/YgpsActivity;->mEtGpsJammingTimes:Landroid/widget/EditText;

    invoke-virtual {v1}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v1

    if-lez v1, :cond_1

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v1

    const/16 v2, 0x3e7

    if-le v1, v2, :cond_2

    :cond_1
    const-string v1, "Jamming scan times error"

    invoke-static {p0, v1, v3}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v1

    invoke-virtual {v1}, Landroid/widget/Toast;->show()V

    goto :goto_0

    :cond_2
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "PMTK837,1,"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {p0, v1}, Lcom/mediatek/ygps/YgpsActivity;->sendCommand(Ljava/lang/String;)V

    goto :goto_0
.end method

.method private reconnectTest()V
    .locals 11

    const/4 v10, 0x1

    const/4 v0, 0x0

    new-instance v3, Landroid/os/Bundle;

    invoke-direct {v3}, Landroid/os/Bundle;-><init>()V

    const-string v6, "ephemeris"

    invoke-virtual {v3, v6, v10}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    const/4 v4, 0x1

    :goto_0
    :try_start_0
    iget v6, p0, Lcom/mediatek/ygps/YgpsActivity;->mTotalTimes:I

    if-gt v4, v6, :cond_2

    iget-boolean v6, p0, Lcom/mediatek/ygps/YgpsActivity;->mIsTestRunning:Z

    if-eqz v6, :cond_2

    iput v4, p0, Lcom/mediatek/ygps/YgpsActivity;->mCurrentTimes:I

    const-string v6, "YGPS/Activity"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "reconnectTest function: "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    iget v8, p0, Lcom/mediatek/ygps/YgpsActivity;->mCurrentTimes:I

    invoke-static {v8}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/Integer;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Lcom/mediatek/xlog/Xlog;->v(Ljava/lang/String;Ljava/lang/String;)I

    invoke-direct {p0, v4}, Lcom/mediatek/ygps/YgpsActivity;->setCurrentTimes(I)V

    const/4 v6, 0x1

    invoke-direct {p0, v3, v6}, Lcom/mediatek/ygps/YgpsActivity;->resetParam(Landroid/os/Bundle;Z)V

    iget-boolean v6, p0, Lcom/mediatek/ygps/YgpsActivity;->mIsNeed3DFix:Z

    if-eqz v6, :cond_4

    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v6

    invoke-virtual {v6}, Ljava/util/Calendar;->getTime()Ljava/util/Date;

    move-result-object v6

    invoke-virtual {v6}, Ljava/util/Date;->getTime()J

    move-result-wide v6

    const-wide/16 v8, 0x3e8

    div-long/2addr v6, v8

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    :cond_0
    iget-boolean v6, p0, Lcom/mediatek/ygps/YgpsActivity;->mIsTestRunning:Z

    if-eqz v6, :cond_1

    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v6

    invoke-virtual {v6}, Ljava/util/Calendar;->getTime()Ljava/util/Date;

    move-result-object v6

    invoke-virtual {v6}, Ljava/util/Date;->getTime()J

    move-result-wide v6

    const-wide/16 v8, 0x3e8

    div-long/2addr v6, v8

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v5

    iget-boolean v6, p0, Lcom/mediatek/ygps/YgpsActivity;->mFirstFix:Z

    if-eqz v6, :cond_3

    :cond_1
    :goto_1
    if-eqz v0, :cond_5

    :cond_2
    const-wide/16 v6, 0x3e8

    invoke-static {v6, v7}, Ljava/lang/Thread;->sleep(J)V

    invoke-direct {p0}, Lcom/mediatek/ygps/YgpsActivity;->stopGPSAutoTest()V

    :goto_2
    return-void

    :cond_3
    invoke-virtual {v5}, Ljava/lang/Long;->longValue()J

    move-result-wide v6

    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v8

    sub-long/2addr v6, v8

    const-wide/16 v8, 0x3e7

    cmp-long v6, v6, v8

    if-lez v6, :cond_0

    const/4 v0, 0x1

    const/16 v6, 0x3e7

    invoke-direct {p0, v6}, Lcom/mediatek/ygps/YgpsActivity;->showExceedPeriod(I)V
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_1

    :catch_0
    move-exception v2

    const-string v6, "YGPS/Activity"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "GPS auto test thread interrupted: "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v2}, Ljava/lang/Throwable;->getMessage()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Lcom/mediatek/xlog/Xlog;->w(Ljava/lang/String;Ljava/lang/String;)I

    const v6, 0x7f050063

    invoke-static {p0, v6, v10}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v6

    invoke-virtual {v6}, Landroid/widget/Toast;->show()V

    goto :goto_2

    :cond_4
    const-wide/16 v6, 0x7d0

    :try_start_1
    invoke-static {v6, v7}, Ljava/lang/Thread;->sleep(J)V
    :try_end_1
    .catch Ljava/lang/InterruptedException; {:try_start_1 .. :try_end_1} :catch_0

    :cond_5
    add-int/lit8 v4, v4, 0x1

    goto/16 :goto_0
.end method

.method private resetParam(Landroid/os/Bundle;Z)V
    .locals 9
    .param p1    # Landroid/os/Bundle;
    .param p2    # Z

    const/16 v3, 0x3e8

    const/4 v8, 0x0

    const-string v0, "YGPS/Activity"

    const-string v1, "Enter resetParam function"

    invoke-static {v0, v1}, Lcom/mediatek/xlog/Xlog;->v(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v0, p0, Lcom/mediatek/ygps/YgpsActivity;->mLocationManager:Landroid/location/LocationManager;

    iget-object v1, p0, Lcom/mediatek/ygps/YgpsActivity;->mLocListener:Landroid/location/LocationListener;

    invoke-virtual {v0, v1}, Landroid/location/LocationManager;->removeUpdates(Landroid/location/LocationListener;)V

    const-wide/16 v0, 0x1f4

    :try_start_0
    invoke-static {v0, v1}, Ljava/lang/Thread;->sleep(J)V
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    if-nez p2, :cond_0

    iget-object v0, p0, Lcom/mediatek/ygps/YgpsActivity;->mLocationManager:Landroid/location/LocationManager;

    const-string v1, "gps"

    const-string v2, "delete_aiding_data"

    invoke-virtual {v0, v1, v2, p1}, Landroid/location/LocationManager;->sendExtraCommand(Ljava/lang/String;Ljava/lang/String;Landroid/os/Bundle;)Z

    :cond_0
    if-nez p2, :cond_1

    invoke-direct {p0}, Lcom/mediatek/ygps/YgpsActivity;->clearLayout()V

    :cond_1
    iput-boolean v8, p0, Lcom/mediatek/ygps/YgpsActivity;->mFirstFix:Z

    iput v8, p0, Lcom/mediatek/ygps/YgpsActivity;->mTtffValue:I

    iget-object v0, p0, Lcom/mediatek/ygps/YgpsActivity;->mHandler:Landroid/os/Handler;

    invoke-virtual {v0, v3}, Landroid/os/Handler;->hasMessages(I)Z

    move-result v0

    if-nez v0, :cond_2

    iget-object v0, p0, Lcom/mediatek/ygps/YgpsActivity;->mHandler:Landroid/os/Handler;

    invoke-virtual {v0, v3}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    :cond_2
    if-eqz p2, :cond_7

    :try_start_1
    iget v0, p0, Lcom/mediatek/ygps/YgpsActivity;->mTestInterval:I

    if-eqz v0, :cond_7

    iget v7, p0, Lcom/mediatek/ygps/YgpsActivity;->mTestInterval:I

    :goto_1
    if-ltz v7, :cond_3

    iget-boolean v0, p0, Lcom/mediatek/ygps/YgpsActivity;->mIsTestRunning:Z

    if-eqz v0, :cond_3

    invoke-direct {p0, v7}, Lcom/mediatek/ygps/YgpsActivity;->setCountDown(I)V

    const-wide/16 v0, 0x3e8

    invoke-static {v0, v1}, Ljava/lang/Thread;->sleep(J)V
    :try_end_1
    .catch Ljava/lang/InterruptedException; {:try_start_1 .. :try_end_1} :catch_1

    add-int/lit8 v7, v7, -0x1

    goto :goto_1

    :catch_0
    move-exception v6

    const-string v0, "YGPS/Activity"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "resetParam InterruptedException: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v6}, Ljava/lang/Throwable;->getMessage()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/mediatek/xlog/Xlog;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    :cond_3
    :try_start_2
    iget-boolean v0, p0, Lcom/mediatek/ygps/YgpsActivity;->mIsTestRunning:Z

    if-nez v0, :cond_4

    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/mediatek/ygps/YgpsActivity;->setCountDown(I)V
    :try_end_2
    .catch Ljava/lang/InterruptedException; {:try_start_2 .. :try_end_2} :catch_1

    :cond_4
    :goto_2
    iget-boolean v0, p0, Lcom/mediatek/ygps/YgpsActivity;->mIsExit:Z

    if-nez v0, :cond_5

    iget-object v0, p0, Lcom/mediatek/ygps/YgpsActivity;->mLocationManager:Landroid/location/LocationManager;

    const-string v1, "gps"

    const-wide/16 v2, 0x0

    const/4 v4, 0x0

    iget-object v5, p0, Lcom/mediatek/ygps/YgpsActivity;->mLocListener:Landroid/location/LocationListener;

    invoke-virtual/range {v0 .. v5}, Landroid/location/LocationManager;->requestLocationUpdates(Ljava/lang/String;JFLandroid/location/LocationListener;)V

    :cond_5
    if-nez p2, :cond_6

    iget-object v0, p0, Lcom/mediatek/ygps/YgpsActivity;->mBtnGpsTestStart:Landroid/widget/Button;

    invoke-virtual {v0}, Landroid/view/View;->isEnabled()Z

    move-result v0

    if-nez v0, :cond_6

    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/mediatek/ygps/YgpsActivity;->setStartButtonEnable(Z)V

    invoke-virtual {p0, v8}, Landroid/app/Activity;->removeDialog(I)V

    iput-boolean v8, p0, Lcom/mediatek/ygps/YgpsActivity;->mStopPressedHandling:Z

    iput-boolean v8, p0, Lcom/mediatek/ygps/YgpsActivity;->mStartPressedHandling:Z

    :cond_6
    return-void

    :cond_7
    const-wide/16 v0, 0x1f4

    :try_start_3
    invoke-static {v0, v1}, Ljava/lang/Thread;->sleep(J)V
    :try_end_3
    .catch Ljava/lang/InterruptedException; {:try_start_3 .. :try_end_3} :catch_1

    goto :goto_2

    :catch_1
    move-exception v6

    const-string v0, "YGPS/Activity"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "resetParam InterruptedException: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v6}, Ljava/lang/Throwable;->getMessage()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/mediatek/xlog/Xlog;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_2
.end method

.method private resetTestParam()V
    .locals 2

    const/4 v1, 0x0

    iput-boolean v1, p0, Lcom/mediatek/ygps/YgpsActivity;->mIsNeed3DFix:Z

    iput v1, p0, Lcom/mediatek/ygps/YgpsActivity;->mTotalTimes:I

    iput v1, p0, Lcom/mediatek/ygps/YgpsActivity;->mCurrentTimes:I

    iput v1, p0, Lcom/mediatek/ygps/YgpsActivity;->mTestInterval:I

    const/4 v0, 0x0

    iput v0, p0, Lcom/mediatek/ygps/YgpsActivity;->mMeanTTFF:F

    iput-boolean v1, p0, Lcom/mediatek/ygps/YgpsActivity;->mIsTestRunning:Z

    return-void
.end method

.method private resetTestView()V
    .locals 2

    const v0, 0x7f070008

    invoke-virtual {p0, v0}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    const-string v1, ""

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    const v0, 0x7f070007

    invoke-virtual {p0, v0}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    const-string v1, ""

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    return-void
.end method

.method private saveNMEALog()V
    .locals 13

    const/4 v12, 0x0

    const/4 v11, 0x1

    invoke-static {}, Landroid/os/Environment;->getExternalStorageState()Ljava/lang/String;

    move-result-object v7

    const-string v8, "mounted"

    invoke-virtual {v7, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_6

    sget-object v7, Lcom/mediatek/ygps/YgpsActivity;->DATE_FORMAT:Ljava/text/DateFormat;

    new-instance v8, Ljava/util/Date;

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v9

    invoke-direct {v8, v9, v10}, Ljava/util/Date;-><init>(J)V

    invoke-virtual {v7, v8}, Ljava/text/DateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v6

    new-instance v1, Ljava/io/File;

    invoke-static {}, Landroid/os/Environment;->getExternalStorageDirectory()Ljava/io/File;

    move-result-object v7

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "Nmealog"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, ".txt"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-direct {v1, v7, v8}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    const/4 v2, 0x0

    const/4 v4, 0x1

    :try_start_0
    invoke-virtual {v1}, Ljava/io/File;->createNewFile()Z

    move-result v7

    if-nez v7, :cond_1

    const v7, 0x7f05005f

    const/4 v8, 0x1

    invoke-static {p0, v7, v8}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v7

    invoke-virtual {v7}, Landroid/widget/Toast;->show()V
    :try_end_0
    .catch Landroid/content/res/Resources$NotFoundException; {:try_start_0 .. :try_end_0} :catch_3
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_5
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-eqz v2, :cond_0

    :try_start_1
    #Replaced unresolvable odex instruction with a throw
    throw v2
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_0

    :cond_0
    :goto_0
    return-void

    :catch_0
    move-exception v0

    const-string v7, "YGPS/Activity"

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "Save nmealog exception in finally: "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v0}, Ljava/lang/Throwable;->getMessage()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8}, Lcom/mediatek/xlog/Xlog;->v(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v4, 0x0

    goto :goto_0

    :cond_1
    :try_start_2
    new-instance v3, Ljava/io/FileOutputStream;

    invoke-direct {v3, v1}, Ljava/io/FileOutputStream;-><init>(Ljava/io/File;)V
    :try_end_2
    .catch Landroid/content/res/Resources$NotFoundException; {:try_start_2 .. :try_end_2} :catch_3
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_5
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    const v7, 0x7f070024

    :try_start_3
    invoke-virtual {p0, v7}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v7

    check-cast v7, Landroid/widget/TextView;

    invoke-virtual {v7}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/String;->getBytes()[B

    move-result-object v7

    array-length v7, v7

    if-nez v7, :cond_2

    const v7, 0x7f050061

    const/4 v8, 0x1

    invoke-static {p0, v7, v8}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v7

    invoke-virtual {v7}, Landroid/widget/Toast;->show()V
    :try_end_3
    .catch Landroid/content/res/Resources$NotFoundException; {:try_start_3 .. :try_end_3} :catch_9
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_8
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    if-eqz v3, :cond_0

    :try_start_4
    invoke-virtual {v3}, Ljava/io/FileOutputStream;->close()V
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_1

    goto :goto_0

    :catch_1
    move-exception v0

    const-string v7, "YGPS/Activity"

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "Save nmealog exception in finally: "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v0}, Ljava/lang/Throwable;->getMessage()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8}, Lcom/mediatek/xlog/Xlog;->v(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v4, 0x0

    goto :goto_0

    :cond_2
    :try_start_5
    invoke-virtual {v5}, Ljava/lang/String;->getBytes()[B

    move-result-object v7

    const/4 v8, 0x0

    invoke-virtual {v5}, Ljava/lang/String;->getBytes()[B

    move-result-object v9

    array-length v9, v9

    invoke-virtual {v3, v7, v8, v9}, Ljava/io/FileOutputStream;->write([BII)V

    invoke-virtual {v3}, Ljava/io/OutputStream;->flush()V

    invoke-virtual {v3}, Ljava/io/FileOutputStream;->close()V
    :try_end_5
    .catch Landroid/content/res/Resources$NotFoundException; {:try_start_5 .. :try_end_5} :catch_9
    .catch Ljava/io/IOException; {:try_start_5 .. :try_end_5} :catch_8
    .catchall {:try_start_5 .. :try_end_5} :catchall_1

    if-eqz v3, :cond_7

    :try_start_6
    invoke-virtual {v3}, Ljava/io/FileOutputStream;->close()V
    :try_end_6
    .catch Ljava/io/IOException; {:try_start_6 .. :try_end_6} :catch_2

    move-object v2, v3

    :cond_3
    :goto_1
    if-eqz v4, :cond_5

    const-string v7, "YGPS/Activity"

    const-string v8, "Save Nmealog to file Finished"

    invoke-static {v7, v8}, Lcom/mediatek/xlog/Xlog;->v(Ljava/lang/String;Ljava/lang/String;)I

    const v7, 0x7f05005c

    invoke-virtual {p0, v7}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v7

    new-array v8, v11, [Ljava/lang/Object;

    invoke-static {}, Landroid/os/Environment;->getExternalStorageDirectory()Ljava/io/File;

    move-result-object v9

    invoke-virtual {v9}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v9

    aput-object v9, v8, v12

    invoke-static {v7, v8}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v7

    invoke-static {p0, v7, v11}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v7

    invoke-virtual {v7}, Landroid/widget/Toast;->show()V

    goto/16 :goto_0

    :catch_2
    move-exception v0

    const-string v7, "YGPS/Activity"

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "Save nmealog exception in finally: "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v0}, Ljava/lang/Throwable;->getMessage()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8}, Lcom/mediatek/xlog/Xlog;->v(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v4, 0x0

    move-object v2, v3

    goto :goto_1

    :catch_3
    move-exception v0

    :goto_2
    :try_start_7
    const-string v7, "YGPS/Activity"

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "Save nmealog NotFoundException: "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v0}, Ljava/lang/Throwable;->getMessage()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8}, Lcom/mediatek/xlog/Xlog;->v(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_0

    const/4 v4, 0x0

    if-eqz v2, :cond_3

    :try_start_8
    invoke-virtual {v2}, Ljava/io/FileOutputStream;->close()V
    :try_end_8
    .catch Ljava/io/IOException; {:try_start_8 .. :try_end_8} :catch_4

    goto :goto_1

    :catch_4
    move-exception v0

    const-string v7, "YGPS/Activity"

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "Save nmealog exception in finally: "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v0}, Ljava/lang/Throwable;->getMessage()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8}, Lcom/mediatek/xlog/Xlog;->v(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v4, 0x0

    goto/16 :goto_1

    :catch_5
    move-exception v0

    :goto_3
    :try_start_9
    const-string v7, "YGPS/Activity"

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "Save nmealog IOException: "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v0}, Ljava/lang/Throwable;->getMessage()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8}, Lcom/mediatek/xlog/Xlog;->v(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_9
    .catchall {:try_start_9 .. :try_end_9} :catchall_0

    const/4 v4, 0x0

    if-eqz v2, :cond_3

    :try_start_a
    invoke-virtual {v2}, Ljava/io/FileOutputStream;->close()V
    :try_end_a
    .catch Ljava/io/IOException; {:try_start_a .. :try_end_a} :catch_6

    goto/16 :goto_1

    :catch_6
    move-exception v0

    const-string v7, "YGPS/Activity"

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "Save nmealog exception in finally: "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v0}, Ljava/lang/Throwable;->getMessage()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8}, Lcom/mediatek/xlog/Xlog;->v(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v4, 0x0

    goto/16 :goto_1

    :catchall_0
    move-exception v7

    :goto_4
    if-eqz v2, :cond_4

    :try_start_b
    invoke-virtual {v2}, Ljava/io/FileOutputStream;->close()V
    :try_end_b
    .catch Ljava/io/IOException; {:try_start_b .. :try_end_b} :catch_7

    :cond_4
    :goto_5
    throw v7

    :catch_7
    move-exception v0

    const-string v8, "YGPS/Activity"

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "Save nmealog exception in finally: "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v0}, Ljava/lang/Throwable;->getMessage()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v8, v9}, Lcom/mediatek/xlog/Xlog;->v(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v4, 0x0

    goto :goto_5

    :cond_5
    const-string v7, "YGPS/Activity"

    const-string v8, "Save Nmealog Failed"

    invoke-static {v7, v8}, Lcom/mediatek/xlog/Xlog;->w(Ljava/lang/String;Ljava/lang/String;)I

    const v7, 0x7f05005e

    invoke-static {p0, v7, v11}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v7

    invoke-virtual {v7}, Landroid/widget/Toast;->show()V

    goto/16 :goto_0

    :cond_6
    const-string v7, "YGPS/Activity"

    const-string v8, "saveNMEALog function: No SD card"

    invoke-static {v7, v8}, Lcom/mediatek/xlog/Xlog;->v(Ljava/lang/String;Ljava/lang/String;)I

    const v7, 0x7f05003d

    invoke-static {p0, v7, v11}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v7

    invoke-virtual {v7}, Landroid/widget/Toast;->show()V

    goto/16 :goto_0

    :catchall_1
    move-exception v7

    move-object v2, v3

    goto :goto_4

    :catch_8
    move-exception v0

    move-object v2, v3

    goto/16 :goto_3

    :catch_9
    move-exception v0

    move-object v2, v3

    goto/16 :goto_2

    :cond_7
    move-object v2, v3

    goto/16 :goto_1
.end method

.method private saveNMEALog(Ljava/lang/String;)V
    .locals 7
    .param p1    # Ljava/lang/String;

    const/4 v6, 0x1

    const/4 v0, 0x1

    :try_start_0
    iget-object v2, p0, Lcom/mediatek/ygps/YgpsActivity;->mOutputNMEALog:Ljava/io/FileOutputStream;

    invoke-virtual {p1}, Ljava/lang/String;->getBytes()[B

    move-result-object v3

    const/4 v4, 0x0

    invoke-virtual {p1}, Ljava/lang/String;->getBytes()[B

    move-result-object v5

    array-length v5, v5

    invoke-virtual {v2, v3, v4, v5}, Ljava/io/FileOutputStream;->write([BII)V

    iget-object v2, p0, Lcom/mediatek/ygps/YgpsActivity;->mOutputNMEALog:Ljava/io/FileOutputStream;

    invoke-virtual {v2}, Ljava/io/OutputStream;->flush()V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-nez v0, :cond_0

    invoke-direct {p0}, Lcom/mediatek/ygps/YgpsActivity;->finishSavingNMEALog()V

    const-string v2, "Please check your SD card"

    invoke-static {p0, v2, v6}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v2

    invoke-virtual {v2}, Landroid/widget/Toast;->show()V

    :cond_0
    :goto_0
    return-void

    :catch_0
    move-exception v1

    const/4 v0, 0x0

    :try_start_1
    const-string v2, "YGPS/Activity"

    const-string v3, "write NMEA log to file failed!"

    invoke-static {v2, v3}, Lcom/mediatek/xlog/Xlog;->v(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    if-nez v0, :cond_0

    invoke-direct {p0}, Lcom/mediatek/ygps/YgpsActivity;->finishSavingNMEALog()V

    const-string v2, "Please check your SD card"

    invoke-static {p0, v2, v6}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v2

    invoke-virtual {v2}, Landroid/widget/Toast;->show()V

    goto :goto_0

    :catchall_0
    move-exception v2

    if-nez v0, :cond_1

    invoke-direct {p0}, Lcom/mediatek/ygps/YgpsActivity;->finishSavingNMEALog()V

    const-string v3, "Please check your SD card"

    invoke-static {p0, v3, v6}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v3

    invoke-virtual {v3}, Landroid/widget/Toast;->show()V

    :cond_1
    throw v2
.end method

.method private sendCommand(Ljava/lang/String;)V
    .locals 9
    .param p1    # Ljava/lang/String;

    const v8, 0x7f050076

    const/4 v7, 0x1

    const/4 v6, -0x1

    const-string v3, "YGPS/Activity"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "GPS Command is "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/mediatek/xlog/Xlog;->v(Ljava/lang/String;Ljava/lang/String;)I

    if-eqz p1, :cond_0

    invoke-virtual {p1}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v3

    if-nez v3, :cond_1

    :cond_0
    invoke-static {p0, v8, v7}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v3

    invoke-virtual {v3}, Landroid/widget/Toast;->show()V

    :goto_0
    return-void

    :cond_1
    const-string v3, "$"

    invoke-virtual {p1, v3}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v1

    const-string v3, "*"

    invoke-virtual {p1, v3}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v2

    move-object v0, p1

    if-eq v1, v6, :cond_4

    if-eq v2, v6, :cond_4

    if-ge v2, v1, :cond_2

    invoke-static {p0, v8, v7}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v3

    invoke-virtual {v3}, Landroid/widget/Toast;->show()V

    goto :goto_0

    :cond_2
    add-int/lit8 v3, v1, 0x1

    invoke-virtual {v0, v3, v2}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v0

    :cond_3
    :goto_1
    iget-object v3, p0, Lcom/mediatek/ygps/YgpsActivity;->mSocketClient:Lcom/mediatek/ygps/ClientSocket;

    invoke-virtual {v0}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Lcom/mediatek/ygps/ClientSocket;->sendCommand(Ljava/lang/String;)V

    goto :goto_0

    :cond_4
    if-eq v1, v6, :cond_5

    add-int/lit8 v3, v1, 0x1

    invoke-virtual {v0, v3}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_1

    :cond_5
    if-eq v2, v6, :cond_3

    const/4 v3, 0x0

    invoke-virtual {v0, v3, v2}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v0

    goto :goto_1
.end method

.method private setCountDown(I)V
    .locals 3
    .param p1    # I

    iget-object v1, p0, Lcom/mediatek/ygps/YgpsActivity;->mAutoTestHandler:Landroid/os/Handler;

    const/16 v2, 0x41a

    invoke-virtual {v1, v2}, Landroid/os/Handler;->obtainMessage(I)Landroid/os/Message;

    move-result-object v0

    iput p1, v0, Landroid/os/Message;->arg1:I

    iget-object v1, p0, Lcom/mediatek/ygps/YgpsActivity;->mAutoTestHandler:Landroid/os/Handler;

    invoke-virtual {v1, v0}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    return-void
.end method

.method private setCurrentTimes(I)V
    .locals 3
    .param p1    # I

    iget-object v1, p0, Lcom/mediatek/ygps/YgpsActivity;->mAutoTestHandler:Landroid/os/Handler;

    const/16 v2, 0x406

    invoke-virtual {v1, v2}, Landroid/os/Handler;->obtainMessage(I)Landroid/os/Message;

    move-result-object v0

    iput p1, v0, Landroid/os/Message;->arg1:I

    iget-object v1, p0, Lcom/mediatek/ygps/YgpsActivity;->mAutoTestHandler:Landroid/os/Handler;

    invoke-virtual {v1, v0}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    return-void
.end method

.method private setLayout()V
    .locals 8

    const v7, 0x7f05005a

    const/4 v6, 0x0

    const/16 v5, 0x8

    const v3, 0x7f070031

    invoke-virtual {p0, v3}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Lcom/mediatek/ygps/SatelliteSkyView;

    iput-object v3, p0, Lcom/mediatek/ygps/YgpsActivity;->mSatelliteView:Lcom/mediatek/ygps/SatelliteSkyView;

    iget-object v3, p0, Lcom/mediatek/ygps/YgpsActivity;->mSatelliteView:Lcom/mediatek/ygps/SatelliteSkyView;

    invoke-virtual {v3, p0}, Lcom/mediatek/ygps/SatelliteSkyView;->setDataProvider(Lcom/mediatek/ygps/SatelliteDataProvider;)V

    const v3, 0x7f070032

    invoke-virtual {p0, v3}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Lcom/mediatek/ygps/SatelliteSignalView;

    iput-object v3, p0, Lcom/mediatek/ygps/YgpsActivity;->mSignalView:Lcom/mediatek/ygps/SatelliteSignalView;

    iget-object v3, p0, Lcom/mediatek/ygps/YgpsActivity;->mSignalView:Lcom/mediatek/ygps/SatelliteSignalView;

    invoke-virtual {v3, p0}, Lcom/mediatek/ygps/SatelliteSignalView;->setDataProvider(Lcom/mediatek/ygps/SatelliteDataProvider;)V

    const v3, 0x7f07001d

    invoke-virtual {p0, v3}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/Button;

    iput-object v3, p0, Lcom/mediatek/ygps/YgpsActivity;->mBtnColdStart:Landroid/widget/Button;

    iget-object v3, p0, Lcom/mediatek/ygps/YgpsActivity;->mBtnColdStart:Landroid/widget/Button;

    iget-object v4, p0, Lcom/mediatek/ygps/YgpsActivity;->mBtnClickListener:Landroid/view/View$OnClickListener;

    invoke-virtual {v3, v4}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    const v3, 0x7f07001c

    invoke-virtual {p0, v3}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/Button;

    iput-object v3, p0, Lcom/mediatek/ygps/YgpsActivity;->mBtnWarmStart:Landroid/widget/Button;

    iget-object v3, p0, Lcom/mediatek/ygps/YgpsActivity;->mBtnWarmStart:Landroid/widget/Button;

    iget-object v4, p0, Lcom/mediatek/ygps/YgpsActivity;->mBtnClickListener:Landroid/view/View$OnClickListener;

    invoke-virtual {v3, v4}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    const v3, 0x7f07001b

    invoke-virtual {p0, v3}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/Button;

    iput-object v3, p0, Lcom/mediatek/ygps/YgpsActivity;->mBtnHotStart:Landroid/widget/Button;

    iget-object v3, p0, Lcom/mediatek/ygps/YgpsActivity;->mBtnHotStart:Landroid/widget/Button;

    iget-object v4, p0, Lcom/mediatek/ygps/YgpsActivity;->mBtnClickListener:Landroid/view/View$OnClickListener;

    invoke-virtual {v3, v4}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    const v3, 0x7f07001e

    invoke-virtual {p0, v3}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/Button;

    iput-object v3, p0, Lcom/mediatek/ygps/YgpsActivity;->mBtnFullStart:Landroid/widget/Button;

    iget-object v3, p0, Lcom/mediatek/ygps/YgpsActivity;->mBtnFullStart:Landroid/widget/Button;

    iget-object v4, p0, Lcom/mediatek/ygps/YgpsActivity;->mBtnClickListener:Landroid/view/View$OnClickListener;

    invoke-virtual {v3, v4}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    const v3, 0x7f070020

    invoke-virtual {p0, v3}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/Button;

    iput-object v3, p0, Lcom/mediatek/ygps/YgpsActivity;->mBtnReStart:Landroid/widget/Button;

    iget-object v3, p0, Lcom/mediatek/ygps/YgpsActivity;->mBtnReStart:Landroid/widget/Button;

    iget-object v4, p0, Lcom/mediatek/ygps/YgpsActivity;->mBtnClickListener:Landroid/view/View$OnClickListener;

    invoke-virtual {v3, v4}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    const v3, 0x7f07001f

    invoke-virtual {p0, v3}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/Button;

    iput-object v3, p0, Lcom/mediatek/ygps/YgpsActivity;->mBtnHotStill:Landroid/widget/Button;

    iget-object v3, p0, Lcom/mediatek/ygps/YgpsActivity;->mBtnHotStill:Landroid/widget/Button;

    iget-object v4, p0, Lcom/mediatek/ygps/YgpsActivity;->mBtnClickListener:Landroid/view/View$OnClickListener;

    invoke-virtual {v3, v4}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    const v3, 0x7f070021

    invoke-virtual {p0, v3}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/Button;

    iput-object v3, p0, Lcom/mediatek/ygps/YgpsActivity;->mBtnSuplLog:Landroid/widget/Button;

    iget-object v3, p0, Lcom/mediatek/ygps/YgpsActivity;->mBtnSuplLog:Landroid/widget/Button;

    iget-object v4, p0, Lcom/mediatek/ygps/YgpsActivity;->mBtnClickListener:Landroid/view/View$OnClickListener;

    invoke-virtual {v3, v4}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    const v3, 0x7f070024

    invoke-virtual {p0, v3}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/TextView;

    iput-object v3, p0, Lcom/mediatek/ygps/YgpsActivity;->mTvNmeaLog:Landroid/widget/TextView;

    const v3, 0x7f070023

    invoke-virtual {p0, v3}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/TextView;

    iput-object v3, p0, Lcom/mediatek/ygps/YgpsActivity;->mTVNMEAHint:Landroid/widget/TextView;

    const v3, 0x7f070025

    invoke-virtual {p0, v3}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/Button;

    iput-object v3, p0, Lcom/mediatek/ygps/YgpsActivity;->mBtnNmeaStart:Landroid/widget/Button;

    iget-object v3, p0, Lcom/mediatek/ygps/YgpsActivity;->mBtnNmeaStart:Landroid/widget/Button;

    iget-object v4, p0, Lcom/mediatek/ygps/YgpsActivity;->mBtnClickListener:Landroid/view/View$OnClickListener;

    invoke-virtual {v3, v4}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    const v3, 0x7f070026

    invoke-virtual {p0, v3}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/Button;

    iput-object v3, p0, Lcom/mediatek/ygps/YgpsActivity;->mBtnNMEAStop:Landroid/widget/Button;

    iget-object v3, p0, Lcom/mediatek/ygps/YgpsActivity;->mBtnNMEAStop:Landroid/widget/Button;

    iget-object v4, p0, Lcom/mediatek/ygps/YgpsActivity;->mBtnClickListener:Landroid/view/View$OnClickListener;

    invoke-virtual {v3, v4}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v3, p0, Lcom/mediatek/ygps/YgpsActivity;->mBtnNMEAStop:Landroid/widget/Button;

    invoke-virtual {v3, v6}, Landroid/widget/TextView;->setEnabled(Z)V

    const v3, 0x7f070029

    invoke-virtual {p0, v3}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/Button;

    iput-object v3, p0, Lcom/mediatek/ygps/YgpsActivity;->mBtnNMEADbgDbg:Landroid/widget/Button;

    iget-object v3, p0, Lcom/mediatek/ygps/YgpsActivity;->mBtnNMEADbgDbg:Landroid/widget/Button;

    iget-object v4, p0, Lcom/mediatek/ygps/YgpsActivity;->mBtnClickListener:Landroid/view/View$OnClickListener;

    invoke-virtual {v3, v4}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    const v3, 0x7f07002a

    invoke-virtual {p0, v3}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/Button;

    iput-object v3, p0, Lcom/mediatek/ygps/YgpsActivity;->mBtnNmeaDbgNmea:Landroid/widget/Button;

    iget-object v3, p0, Lcom/mediatek/ygps/YgpsActivity;->mBtnNmeaDbgNmea:Landroid/widget/Button;

    iget-object v4, p0, Lcom/mediatek/ygps/YgpsActivity;->mBtnClickListener:Landroid/view/View$OnClickListener;

    invoke-virtual {v3, v4}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    const v3, 0x7f07002b

    invoke-virtual {p0, v3}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/Button;

    iput-object v3, p0, Lcom/mediatek/ygps/YgpsActivity;->mBtnNmeaDbgDbgFile:Landroid/widget/Button;

    iget-object v3, p0, Lcom/mediatek/ygps/YgpsActivity;->mBtnNmeaDbgDbgFile:Landroid/widget/Button;

    iget-object v4, p0, Lcom/mediatek/ygps/YgpsActivity;->mBtnClickListener:Landroid/view/View$OnClickListener;

    invoke-virtual {v3, v4}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    const v3, 0x7f07002c

    invoke-virtual {p0, v3}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/Button;

    iput-object v3, p0, Lcom/mediatek/ygps/YgpsActivity;->mBtnNmeaDbgNmeaDdms:Landroid/widget/Button;

    iget-object v3, p0, Lcom/mediatek/ygps/YgpsActivity;->mBtnNmeaDbgNmeaDdms:Landroid/widget/Button;

    iget-object v4, p0, Lcom/mediatek/ygps/YgpsActivity;->mBtnClickListener:Landroid/view/View$OnClickListener;

    invoke-virtual {v3, v4}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    const v3, 0x7f070027

    invoke-virtual {p0, v3}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/Button;

    iput-object v3, p0, Lcom/mediatek/ygps/YgpsActivity;->mBtnNmeaClear:Landroid/widget/Button;

    iget-object v3, p0, Lcom/mediatek/ygps/YgpsActivity;->mBtnNmeaClear:Landroid/widget/Button;

    iget-object v4, p0, Lcom/mediatek/ygps/YgpsActivity;->mBtnClickListener:Landroid/view/View$OnClickListener;

    invoke-virtual {v3, v4}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    const v3, 0x7f070028

    invoke-virtual {p0, v3}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/Button;

    iput-object v3, p0, Lcom/mediatek/ygps/YgpsActivity;->mBtnNmeaSave:Landroid/widget/Button;

    iget-object v3, p0, Lcom/mediatek/ygps/YgpsActivity;->mBtnNmeaSave:Landroid/widget/Button;

    iget-object v4, p0, Lcom/mediatek/ygps/YgpsActivity;->mBtnClickListener:Landroid/view/View$OnClickListener;

    invoke-virtual {v3, v4}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    const v3, 0x7f070004

    invoke-virtual {p0, v3}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/Button;

    iput-object v3, p0, Lcom/mediatek/ygps/YgpsActivity;->mBtnGpsTestStart:Landroid/widget/Button;

    iget-object v3, p0, Lcom/mediatek/ygps/YgpsActivity;->mBtnGpsTestStart:Landroid/widget/Button;

    iget-object v4, p0, Lcom/mediatek/ygps/YgpsActivity;->mBtnClickListener:Landroid/view/View$OnClickListener;

    invoke-virtual {v3, v4}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    const v3, 0x7f070005

    invoke-virtual {p0, v3}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/Button;

    iput-object v3, p0, Lcom/mediatek/ygps/YgpsActivity;->mBtnGpsTestStop:Landroid/widget/Button;

    iget-object v3, p0, Lcom/mediatek/ygps/YgpsActivity;->mBtnGpsTestStop:Landroid/widget/Button;

    iget-object v4, p0, Lcom/mediatek/ygps/YgpsActivity;->mBtnClickListener:Landroid/view/View$OnClickListener;

    invoke-virtual {v3, v4}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v3, p0, Lcom/mediatek/ygps/YgpsActivity;->mBtnGpsTestStop:Landroid/widget/Button;

    invoke-virtual {v3, v6}, Landroid/widget/TextView;->setEnabled(Z)V

    const v3, 0x7f070001

    invoke-virtual {p0, v3}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/EditText;

    iput-object v3, p0, Lcom/mediatek/ygps/YgpsActivity;->mEtTestTimes:Landroid/widget/EditText;

    const v3, 0x7f070003

    invoke-virtual {p0, v3}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/CheckBox;

    iput-object v3, p0, Lcom/mediatek/ygps/YgpsActivity;->mCbNeed3DFix:Landroid/widget/CheckBox;

    const v3, 0x7f070002

    invoke-virtual {p0, v3}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/EditText;

    iput-object v3, p0, Lcom/mediatek/ygps/YgpsActivity;->mEtTestInterval:Landroid/widget/EditText;

    const v3, 0x7f07002d

    invoke-virtual {p0, v3}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/Button;

    iput-object v3, p0, Lcom/mediatek/ygps/YgpsActivity;->mBtnGpsHwTest:Landroid/widget/Button;

    iget-object v3, p0, Lcom/mediatek/ygps/YgpsActivity;->mBtnGpsHwTest:Landroid/widget/Button;

    iget-object v4, p0, Lcom/mediatek/ygps/YgpsActivity;->mBtnClickListener:Landroid/view/View$OnClickListener;

    invoke-virtual {v3, v4}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    const v3, 0x7f07002e

    invoke-virtual {p0, v3}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/Button;

    iput-object v3, p0, Lcom/mediatek/ygps/YgpsActivity;->mBtnGpsJamming:Landroid/widget/Button;

    iget-object v3, p0, Lcom/mediatek/ygps/YgpsActivity;->mBtnGpsJamming:Landroid/widget/Button;

    iget-object v4, p0, Lcom/mediatek/ygps/YgpsActivity;->mBtnClickListener:Landroid/view/View$OnClickListener;

    invoke-virtual {v3, v4}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    const v3, 0x7f07002f

    invoke-virtual {p0, v3}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/EditText;

    iput-object v3, p0, Lcom/mediatek/ygps/YgpsActivity;->mEtGpsJammingTimes:Landroid/widget/EditText;

    iget-object v3, p0, Lcom/mediatek/ygps/YgpsActivity;->mEtGpsJammingTimes:Landroid/widget/EditText;

    const v4, 0x7f05004b

    invoke-virtual {p0, v4}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v3, p0, Lcom/mediatek/ygps/YgpsActivity;->mEtGpsJammingTimes:Landroid/widget/EditText;

    iget-object v4, p0, Lcom/mediatek/ygps/YgpsActivity;->mEtGpsJammingTimes:Landroid/widget/EditText;

    invoke-virtual {v4}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v4

    invoke-interface {v4}, Landroid/text/Editable;->length()I

    move-result v4

    invoke-virtual {v3, v4}, Landroid/widget/EditText;->setSelection(I)V

    iget-object v3, p0, Lcom/mediatek/ygps/YgpsActivity;->mBtnNMEADbgDbg:Landroid/widget/Button;

    invoke-virtual {v3, v5}, Landroid/view/View;->setVisibility(I)V

    iget-object v3, p0, Lcom/mediatek/ygps/YgpsActivity;->mBtnNmeaDbgNmea:Landroid/widget/Button;

    invoke-virtual {v3, v5}, Landroid/view/View;->setVisibility(I)V

    iget-object v3, p0, Lcom/mediatek/ygps/YgpsActivity;->mBtnNmeaSave:Landroid/widget/Button;

    invoke-virtual {v3, v5}, Landroid/view/View;->setVisibility(I)V

    iget-object v3, p0, Lcom/mediatek/ygps/YgpsActivity;->mBtnNmeaClear:Landroid/widget/Button;

    invoke-virtual {v3, v5}, Landroid/view/View;->setVisibility(I)V

    const-string v3, "debug.dbg2socket"

    const-string v4, "0"

    invoke-static {v3, v4}, Lcom/mediatek/ygps/GpsMnlSetting;->getMnlProp(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    const-string v3, "0"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_1

    iget-object v3, p0, Lcom/mediatek/ygps/YgpsActivity;->mBtnNMEADbgDbg:Landroid/widget/Button;

    const v4, 0x7f050050

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setText(I)V

    :goto_0
    const-string v3, "debug.nmea2socket"

    const-string v4, "0"

    invoke-static {v3, v4}, Lcom/mediatek/ygps/GpsMnlSetting;->getMnlProp(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    const-string v3, "0"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_2

    iget-object v3, p0, Lcom/mediatek/ygps/YgpsActivity;->mBtnNmeaDbgNmea:Landroid/widget/Button;

    const v4, 0x7f050052

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setText(I)V

    :goto_1
    const-string v3, "debug.dbg2file"

    const-string v4, "0"

    invoke-static {v3, v4}, Lcom/mediatek/ygps/GpsMnlSetting;->getMnlProp(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    const-string v3, "0"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    iget-object v3, p0, Lcom/mediatek/ygps/YgpsActivity;->mBtnNmeaDbgDbgFile:Landroid/widget/Button;

    const v4, 0x7f050054

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setText(I)V

    :goto_2
    const-string v3, "debug.debug_nmea"

    const-string v4, "1"

    invoke-static {v3, v4}, Lcom/mediatek/ygps/GpsMnlSetting;->getMnlProp(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    const-string v3, "1"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_4

    iget-object v3, p0, Lcom/mediatek/ygps/YgpsActivity;->mBtnNmeaDbgNmeaDdms:Landroid/widget/Button;

    const v4, 0x7f050056

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setText(I)V

    :goto_3
    const-string v3, "BEE_enabled"

    const-string v4, "1"

    invoke-static {v3, v4}, Lcom/mediatek/ygps/GpsMnlSetting;->getMnlProp(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    const-string v3, "1"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_5

    iget-object v3, p0, Lcom/mediatek/ygps/YgpsActivity;->mBtnHotStill:Landroid/widget/Button;

    const v4, 0x7f050058

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setText(I)V

    :goto_4
    const-string v3, "SUPPLOG_enabled"

    const-string v4, "0"

    invoke-static {v3, v4}, Lcom/mediatek/ygps/GpsMnlSetting;->getMnlProp(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    const-string v3, "1"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_6

    iget-object v3, p0, Lcom/mediatek/ygps/YgpsActivity;->mBtnSuplLog:Landroid/widget/Button;

    const v4, 0x7f050079

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setText(I)V

    :goto_5
    const/4 v0, 0x0

    const-string v3, "first.time"

    invoke-virtual {p0, v3, v6}, Landroid/content/ContextWrapper;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v1

    const-string v3, "first.time"

    const/4 v4, 0x0

    invoke-interface {v1, v3, v4}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    if-eqz v2, :cond_0

    const-string v3, "1"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_7

    invoke-interface {v1}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v3

    const-string v4, "first.time"

    const-string v5, "2"

    invoke-interface {v3, v4, v5}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    move-result-object v3

    invoke-interface {v3}, Landroid/content/SharedPreferences$Editor;->commit()Z

    :cond_0
    :goto_6
    const-string v3, "test.mode"

    const-string v4, "0"

    invoke-static {v3, v4}, Lcom/mediatek/ygps/GpsMnlSetting;->getMnlProp(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    const-string v3, "0"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_8

    iget-object v3, p0, Lcom/mediatek/ygps/YgpsActivity;->mBtnGpsHwTest:Landroid/widget/Button;

    invoke-virtual {v3, v7}, Landroid/widget/TextView;->setText(I)V

    :goto_7
    return-void

    :cond_1
    iget-object v3, p0, Lcom/mediatek/ygps/YgpsActivity;->mBtnNMEADbgDbg:Landroid/widget/Button;

    const v4, 0x7f050051

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setText(I)V

    goto/16 :goto_0

    :cond_2
    iget-object v3, p0, Lcom/mediatek/ygps/YgpsActivity;->mBtnNmeaDbgNmea:Landroid/widget/Button;

    const v4, 0x7f050053

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setText(I)V

    goto/16 :goto_1

    :cond_3
    iget-object v3, p0, Lcom/mediatek/ygps/YgpsActivity;->mBtnNmeaDbgDbgFile:Landroid/widget/Button;

    const v4, 0x7f050055

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setText(I)V

    goto/16 :goto_2

    :cond_4
    iget-object v3, p0, Lcom/mediatek/ygps/YgpsActivity;->mBtnNmeaDbgNmeaDdms:Landroid/widget/Button;

    const v4, 0x7f050057

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setText(I)V

    goto/16 :goto_3

    :cond_5
    iget-object v3, p0, Lcom/mediatek/ygps/YgpsActivity;->mBtnHotStill:Landroid/widget/Button;

    const v4, 0x7f050059

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setText(I)V

    goto/16 :goto_4

    :cond_6
    iget-object v3, p0, Lcom/mediatek/ygps/YgpsActivity;->mBtnSuplLog:Landroid/widget/Button;

    const v4, 0x7f05007a

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setText(I)V

    goto :goto_5

    :cond_7
    const-string v3, "2"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    const/4 v0, 0x1

    goto :goto_6

    :cond_8
    if-eqz v0, :cond_9

    const-string v3, "test.mode"

    const-string v4, "0"

    invoke-static {v3, v4}, Lcom/mediatek/ygps/GpsMnlSetting;->setMnlProp(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v3, p0, Lcom/mediatek/ygps/YgpsActivity;->mBtnGpsHwTest:Landroid/widget/Button;

    invoke-virtual {v3, v7}, Landroid/widget/TextView;->setText(I)V

    goto :goto_7

    :cond_9
    iget-object v3, p0, Lcom/mediatek/ygps/YgpsActivity;->mBtnGpsHwTest:Landroid/widget/Button;

    const v4, 0x7f05005b

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setText(I)V

    goto :goto_7
.end method

.method private setStartButtonEnable(Z)V
    .locals 3
    .param p1    # Z

    iget-object v1, p0, Lcom/mediatek/ygps/YgpsActivity;->mAutoTestHandler:Landroid/os/Handler;

    const/16 v2, 0x410

    invoke-virtual {v1, v2}, Landroid/os/Handler;->obtainMessage(I)Landroid/os/Message;

    move-result-object v0

    if-eqz p1, :cond_0

    const/4 v1, 0x1

    :goto_0
    iput v1, v0, Landroid/os/Message;->arg1:I

    iget-object v1, p0, Lcom/mediatek/ygps/YgpsActivity;->mAutoTestHandler:Landroid/os/Handler;

    invoke-virtual {v1, v0}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    return-void

    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method private setTestParam()V
    .locals 3

    iget-object v1, p0, Lcom/mediatek/ygps/YgpsActivity;->mAutoTestHandler:Landroid/os/Handler;

    const/16 v2, 0x442

    invoke-virtual {v1, v2}, Landroid/os/Handler;->obtainMessage(I)Landroid/os/Message;

    move-result-object v0

    iget-object v1, p0, Lcom/mediatek/ygps/YgpsActivity;->mAutoTestHandler:Landroid/os/Handler;

    invoke-virtual {v1, v0}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    return-void
.end method

.method private setViewToStartState()V
    .locals 2

    const/4 v1, 0x0

    iget-object v0, p0, Lcom/mediatek/ygps/YgpsActivity;->mBtnGpsTestStart:Landroid/widget/Button;

    invoke-virtual {v0, v1}, Landroid/view/View;->setFocusableInTouchMode(Z)V

    iget-object v0, p0, Lcom/mediatek/ygps/YgpsActivity;->mBtnGpsTestStart:Landroid/widget/Button;

    invoke-virtual {v0}, Landroid/view/View;->refreshDrawableState()V

    iget-object v0, p0, Lcom/mediatek/ygps/YgpsActivity;->mBtnGpsTestStart:Landroid/widget/Button;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setEnabled(Z)V

    iget-object v0, p0, Lcom/mediatek/ygps/YgpsActivity;->mEtTestTimes:Landroid/widget/EditText;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/mediatek/ygps/YgpsActivity;->mEtTestTimes:Landroid/widget/EditText;

    invoke-virtual {v0, v1}, Landroid/view/View;->setFocusable(Z)V

    iget-object v0, p0, Lcom/mediatek/ygps/YgpsActivity;->mEtTestTimes:Landroid/widget/EditText;

    invoke-virtual {v0}, Landroid/view/View;->refreshDrawableState()V

    iget-object v0, p0, Lcom/mediatek/ygps/YgpsActivity;->mEtTestTimes:Landroid/widget/EditText;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setEnabled(Z)V

    :cond_0
    iget-object v0, p0, Lcom/mediatek/ygps/YgpsActivity;->mCbNeed3DFix:Landroid/widget/CheckBox;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/mediatek/ygps/YgpsActivity;->mCbNeed3DFix:Landroid/widget/CheckBox;

    invoke-virtual {v0, v1}, Landroid/view/View;->setFocusable(Z)V

    iget-object v0, p0, Lcom/mediatek/ygps/YgpsActivity;->mCbNeed3DFix:Landroid/widget/CheckBox;

    invoke-virtual {v0}, Landroid/view/View;->refreshDrawableState()V

    iget-object v0, p0, Lcom/mediatek/ygps/YgpsActivity;->mCbNeed3DFix:Landroid/widget/CheckBox;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setEnabled(Z)V

    :cond_1
    iget-object v0, p0, Lcom/mediatek/ygps/YgpsActivity;->mEtTestInterval:Landroid/widget/EditText;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/mediatek/ygps/YgpsActivity;->mEtTestInterval:Landroid/widget/EditText;

    invoke-virtual {v0, v1}, Landroid/view/View;->setFocusable(Z)V

    iget-object v0, p0, Lcom/mediatek/ygps/YgpsActivity;->mEtTestInterval:Landroid/widget/EditText;

    invoke-virtual {v0}, Landroid/view/View;->refreshDrawableState()V

    iget-object v0, p0, Lcom/mediatek/ygps/YgpsActivity;->mEtTestInterval:Landroid/widget/EditText;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setEnabled(Z)V

    :cond_2
    iget-object v0, p0, Lcom/mediatek/ygps/YgpsActivity;->mBtnGpsTestStop:Landroid/widget/Button;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setEnabled(Z)V

    invoke-direct {p0}, Lcom/mediatek/ygps/YgpsActivity;->clearLayout()V

    return-void
.end method

.method private setViewToStopState()V
    .locals 3

    const/4 v2, 0x0

    const/4 v1, 0x1

    iget-object v0, p0, Lcom/mediatek/ygps/YgpsActivity;->mBtnGpsTestStop:Landroid/widget/Button;

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setEnabled(Z)V

    iget-object v0, p0, Lcom/mediatek/ygps/YgpsActivity;->mEtTestTimes:Landroid/widget/EditText;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/mediatek/ygps/YgpsActivity;->mEtTestTimes:Landroid/widget/EditText;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setEnabled(Z)V

    iget-object v0, p0, Lcom/mediatek/ygps/YgpsActivity;->mEtTestTimes:Landroid/widget/EditText;

    invoke-virtual {v0, v1}, Landroid/view/View;->setFocusableInTouchMode(Z)V

    iget-object v0, p0, Lcom/mediatek/ygps/YgpsActivity;->mEtTestTimes:Landroid/widget/EditText;

    invoke-virtual {v0}, Landroid/view/View;->refreshDrawableState()V

    :cond_0
    iget-object v0, p0, Lcom/mediatek/ygps/YgpsActivity;->mCbNeed3DFix:Landroid/widget/CheckBox;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/mediatek/ygps/YgpsActivity;->mCbNeed3DFix:Landroid/widget/CheckBox;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setEnabled(Z)V

    iget-object v0, p0, Lcom/mediatek/ygps/YgpsActivity;->mCbNeed3DFix:Landroid/widget/CheckBox;

    invoke-virtual {v0}, Landroid/view/View;->refreshDrawableState()V

    :cond_1
    iget-object v0, p0, Lcom/mediatek/ygps/YgpsActivity;->mEtTestInterval:Landroid/widget/EditText;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/mediatek/ygps/YgpsActivity;->mEtTestInterval:Landroid/widget/EditText;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setEnabled(Z)V

    iget-object v0, p0, Lcom/mediatek/ygps/YgpsActivity;->mEtTestInterval:Landroid/widget/EditText;

    invoke-virtual {v0, v1}, Landroid/view/View;->setFocusableInTouchMode(Z)V

    iget-object v0, p0, Lcom/mediatek/ygps/YgpsActivity;->mEtTestInterval:Landroid/widget/EditText;

    invoke-virtual {v0}, Landroid/view/View;->refreshDrawableState()V

    :cond_2
    iget-object v0, p0, Lcom/mediatek/ygps/YgpsActivity;->mBtnGpsTestStart:Landroid/widget/Button;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setEnabled(Z)V

    iget-object v0, p0, Lcom/mediatek/ygps/YgpsActivity;->mBtnGpsTestStart:Landroid/widget/Button;

    invoke-virtual {v0, v2}, Landroid/view/View;->setFocusableInTouchMode(Z)V

    iget-object v0, p0, Lcom/mediatek/ygps/YgpsActivity;->mBtnGpsTestStart:Landroid/widget/Button;

    invoke-virtual {v0}, Landroid/view/View;->refreshDrawableState()V

    return-void
.end method

.method private showExceedPeriod(I)V
    .locals 3
    .param p1    # I

    iget-object v1, p0, Lcom/mediatek/ygps/YgpsActivity;->mAutoTestHandler:Landroid/os/Handler;

    const/16 v2, 0x438

    invoke-virtual {v1, v2}, Landroid/os/Handler;->obtainMessage(I)Landroid/os/Message;

    move-result-object v0

    iput p1, v0, Landroid/os/Message;->arg1:I

    iget-object v1, p0, Lcom/mediatek/ygps/YgpsActivity;->mAutoTestHandler:Landroid/os/Handler;

    invoke-virtual {v1, v0}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    return-void
.end method

.method private showVersion()V
    .locals 3

    const-string v1, "YGPS/Activity"

    const-string v2, "Enter show version"

    invoke-static {v1, v2}, Lcom/mediatek/xlog/Xlog;->v(Ljava/lang/String;Ljava/lang/String;)I

    iget-boolean v1, p0, Lcom/mediatek/ygps/YgpsActivity;->mIsExit:Z

    if-eqz v1, :cond_0

    :goto_0
    return-void

    :cond_0
    const v1, 0x7f070019

    invoke-virtual {p0, v1}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    const v1, 0x7f050070

    invoke-virtual {p0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/mediatek/ygps/GpsMnlSetting;->getChipVersion(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    const-string v1, "PMTK605"

    invoke-direct {p0, v1}, Lcom/mediatek/ygps/YgpsActivity;->sendCommand(Ljava/lang/String;)V

    goto :goto_0
.end method

.method private startGPSAutoTest()V
    .locals 6

    const v5, 0x7f05004e

    const/16 v4, 0x3e7

    const/4 v3, 0x1

    iget-object v2, p0, Lcom/mediatek/ygps/YgpsActivity;->mEtTestTimes:Landroid/widget/EditText;

    if-eqz v2, :cond_3

    iget-object v2, p0, Lcom/mediatek/ygps/YgpsActivity;->mEtTestTimes:Landroid/widget/EditText;

    invoke-virtual {v2}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v2

    invoke-interface {v2}, Landroid/text/Editable;->length()I

    move-result v2

    if-nez v2, :cond_0

    const v2, 0x7f05004d

    invoke-static {p0, v2, v3}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v2

    invoke-virtual {v2}, Landroid/widget/Toast;->show()V

    iget-object v2, p0, Lcom/mediatek/ygps/YgpsActivity;->mBtnGpsTestStart:Landroid/widget/Button;

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setEnabled(Z)V

    :goto_0
    return-void

    :cond_0
    iget-object v2, p0, Lcom/mediatek/ygps/YgpsActivity;->mEtTestTimes:Landroid/widget/EditText;

    invoke-virtual {v2}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v2

    if-ltz v2, :cond_1

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v2

    if-le v2, v4, :cond_2

    :cond_1
    invoke-static {p0, v5, v3}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v2

    invoke-virtual {v2}, Landroid/widget/Toast;->show()V

    iget-object v2, p0, Lcom/mediatek/ygps/YgpsActivity;->mBtnGpsTestStart:Landroid/widget/Button;

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setEnabled(Z)V

    goto :goto_0

    :cond_2
    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v2

    iput v2, p0, Lcom/mediatek/ygps/YgpsActivity;->mTotalTimes:I

    :cond_3
    iget-object v2, p0, Lcom/mediatek/ygps/YgpsActivity;->mEtTestInterval:Landroid/widget/EditText;

    if-eqz v2, :cond_7

    iget-object v2, p0, Lcom/mediatek/ygps/YgpsActivity;->mEtTestInterval:Landroid/widget/EditText;

    invoke-virtual {v2}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v2

    invoke-interface {v2}, Landroid/text/Editable;->length()I

    move-result v2

    if-nez v2, :cond_4

    const v2, 0x7f05004f

    invoke-static {p0, v2, v3}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v2

    invoke-virtual {v2}, Landroid/widget/Toast;->show()V

    iget-object v2, p0, Lcom/mediatek/ygps/YgpsActivity;->mBtnGpsTestStart:Landroid/widget/Button;

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setEnabled(Z)V

    goto :goto_0

    :cond_4
    iget-object v2, p0, Lcom/mediatek/ygps/YgpsActivity;->mEtTestInterval:Landroid/widget/EditText;

    invoke-virtual {v2}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v2

    if-ltz v2, :cond_5

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v2

    if-le v2, v4, :cond_6

    :cond_5
    invoke-static {p0, v5, v3}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v2

    invoke-virtual {v2}, Landroid/widget/Toast;->show()V

    iget-object v2, p0, Lcom/mediatek/ygps/YgpsActivity;->mBtnGpsTestStart:Landroid/widget/Button;

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setEnabled(Z)V

    goto :goto_0

    :cond_6
    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v2

    iput v2, p0, Lcom/mediatek/ygps/YgpsActivity;->mTestInterval:I

    :cond_7
    iget-object v2, p0, Lcom/mediatek/ygps/YgpsActivity;->mCbNeed3DFix:Landroid/widget/CheckBox;

    if-eqz v2, :cond_8

    iget-object v2, p0, Lcom/mediatek/ygps/YgpsActivity;->mCbNeed3DFix:Landroid/widget/CheckBox;

    invoke-virtual {v2}, Landroid/widget/CompoundButton;->isChecked()Z

    move-result v2

    iput-boolean v2, p0, Lcom/mediatek/ygps/YgpsActivity;->mIsNeed3DFix:Z

    :cond_8
    iput-boolean v3, p0, Lcom/mediatek/ygps/YgpsActivity;->mIsTestRunning:Z

    invoke-direct {p0}, Lcom/mediatek/ygps/YgpsActivity;->resetTestView()V

    iget-boolean v2, p0, Lcom/mediatek/ygps/YgpsActivity;->mStartPressedHandling:Z

    if-nez v2, :cond_a

    iput-boolean v3, p0, Lcom/mediatek/ygps/YgpsActivity;->mStartPressedHandling:Z

    invoke-direct {p0}, Lcom/mediatek/ygps/YgpsActivity;->setViewToStartState()V

    new-instance v2, Lcom/mediatek/ygps/YgpsActivity$AutoTestThread;

    const/4 v3, 0x0

    invoke-direct {v2, p0, v3}, Lcom/mediatek/ygps/YgpsActivity$AutoTestThread;-><init>(Lcom/mediatek/ygps/YgpsActivity;Lcom/mediatek/ygps/YgpsActivity$1;)V

    iput-object v2, p0, Lcom/mediatek/ygps/YgpsActivity;->mAutoTestThread:Lcom/mediatek/ygps/YgpsActivity$AutoTestThread;

    iget-object v2, p0, Lcom/mediatek/ygps/YgpsActivity;->mAutoTestThread:Lcom/mediatek/ygps/YgpsActivity$AutoTestThread;

    if-eqz v2, :cond_9

    iget-object v2, p0, Lcom/mediatek/ygps/YgpsActivity;->mAutoTestThread:Lcom/mediatek/ygps/YgpsActivity$AutoTestThread;

    invoke-virtual {v2}, Ljava/lang/Thread;->start()V

    goto/16 :goto_0

    :cond_9
    const-string v2, "YGPS/Activity"

    const-string v3, "new matThread failed"

    invoke-static {v2, v3}, Lcom/mediatek/xlog/Xlog;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0

    :cond_a
    const-string v2, "YGPS/Activity"

    const-string v3, "start button has been pushed."

    invoke-static {v2, v3}, Lcom/mediatek/xlog/Xlog;->w(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v2, p0, Lcom/mediatek/ygps/YgpsActivity;->mBtnGpsTestStart:Landroid/widget/Button;

    invoke-virtual {v2}, Landroid/view/View;->refreshDrawableState()V

    iget-object v2, p0, Lcom/mediatek/ygps/YgpsActivity;->mBtnGpsTestStart:Landroid/widget/Button;

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setEnabled(Z)V

    goto/16 :goto_0
.end method

.method private stopGPSAutoTest()V
    .locals 0

    invoke-direct {p0}, Lcom/mediatek/ygps/YgpsActivity;->resetTestParam()V

    invoke-direct {p0}, Lcom/mediatek/ygps/YgpsActivity;->setTestParam()V

    return-void
.end method

.method private toString([FI)Ljava/lang/String;
    .locals 3
    .param p1    # [F
    .param p2    # I

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "("

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const/4 v0, 0x0

    :goto_0
    if-ge v0, p2, :cond_0

    aget v2, p1, v0

    invoke-static {v2}, Ljava/lang/Float;->toString(F)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v2, ","

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_0
    const-string v2, ")"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    return-object v2
.end method

.method private toString([II)Ljava/lang/String;
    .locals 3
    .param p1    # [I
    .param p2    # I

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "("

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const/4 v0, 0x0

    :goto_0
    if-ge v0, p2, :cond_0

    aget v2, p1, v0

    invoke-static {v2}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v2, ","

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_0
    const-string v2, ")"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    return-object v2
.end method


# virtual methods
.method public getSatelliteStatus([I[F[F[FII[I)I
    .locals 4
    .param p1    # [I
    .param p2    # [F
    .param p3    # [F
    .param p4    # [F
    .param p5    # I
    .param p6    # I
    .param p7    # [I

    monitor-enter p0

    if-eqz p1, :cond_0

    :try_start_0
    iget-object v0, p0, Lcom/mediatek/ygps/YgpsActivity;->mPrns:[I

    const/4 v1, 0x0

    const/4 v2, 0x0

    iget v3, p0, Lcom/mediatek/ygps/YgpsActivity;->mSatellites:I

    invoke-static {v0, v1, p1, v2, v3}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    :cond_0
    if-eqz p2, :cond_1

    iget-object v0, p0, Lcom/mediatek/ygps/YgpsActivity;->mSnrs:[F

    const/4 v1, 0x0

    const/4 v2, 0x0

    iget v3, p0, Lcom/mediatek/ygps/YgpsActivity;->mSatellites:I

    invoke-static {v0, v1, p2, v2, v3}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    :cond_1
    if-eqz p4, :cond_2

    iget-object v0, p0, Lcom/mediatek/ygps/YgpsActivity;->mAzimuth:[F

    const/4 v1, 0x0

    const/4 v2, 0x0

    iget v3, p0, Lcom/mediatek/ygps/YgpsActivity;->mSatellites:I

    invoke-static {v0, v1, p4, v2, v3}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    :cond_2
    if-eqz p3, :cond_3

    iget-object v0, p0, Lcom/mediatek/ygps/YgpsActivity;->mElevation:[F

    const/4 v1, 0x0

    const/4 v2, 0x0

    iget v3, p0, Lcom/mediatek/ygps/YgpsActivity;->mSatellites:I

    invoke-static {v0, v1, p3, v2, v3}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    :cond_3
    if-eqz p7, :cond_4

    iget-object v0, p0, Lcom/mediatek/ygps/YgpsActivity;->mUsedInFixMask:[I

    const/4 v1, 0x0

    const/4 v2, 0x0

    const/16 v3, 0x8

    invoke-static {v0, v1, p7, v2, v3}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    :cond_4
    iget v0, p0, Lcom/mediatek/ygps/YgpsActivity;->mSatellites:I

    monitor-exit p0

    return v0

    :catchall_0
    move-exception v0

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 12
    .param p1    # Landroid/os/Bundle;

    const v5, 0x7f050033

    const v4, 0x7f050032

    const v3, 0x7f050031

    const/4 v11, 0x1

    const/4 v10, 0x0

    const-string v0, "YGPS/Activity"

    const-string v1, "Enter onCreate  function of Main Activity"

    invoke-static {v0, v1}, Lcom/mediatek/xlog/Xlog;->v(Ljava/lang/String;Ljava/lang/String;)I

    invoke-super {p0, p1}, Landroid/app/ActivityGroup;->onCreate(Landroid/os/Bundle;)V

    new-instance v0, Landroid/os/StrictMode$ThreadPolicy$Builder;

    invoke-direct {v0}, Landroid/os/StrictMode$ThreadPolicy$Builder;-><init>()V

    invoke-virtual {v0}, Landroid/os/StrictMode$ThreadPolicy$Builder;->detectNetwork()Landroid/os/StrictMode$ThreadPolicy$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/os/StrictMode$ThreadPolicy$Builder;->build()Landroid/os/StrictMode$ThreadPolicy;

    move-result-object v0

    invoke-static {v0}, Landroid/os/StrictMode;->setThreadPolicy(Landroid/os/StrictMode$ThreadPolicy;)V

    invoke-virtual {p0}, Landroid/app/TabActivity;->getTabHost()Landroid/widget/TabHost;

    move-result-object v9

    invoke-static {p0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    const v1, 0x7f030005

    invoke-virtual {v9}, Landroid/widget/TabHost;->getTabContentView()Landroid/widget/FrameLayout;

    move-result-object v2

    invoke-virtual {v0, v1, v2, v11}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    invoke-virtual {p0, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v9, v0}, Landroid/widget/TabHost;->newTabSpec(Ljava/lang/String;)Landroid/widget/TabHost$TabSpec;

    move-result-object v0

    invoke-virtual {p0, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TabHost$TabSpec;->setIndicator(Ljava/lang/CharSequence;)Landroid/widget/TabHost$TabSpec;

    move-result-object v0

    const v1, 0x7f070030

    invoke-virtual {v0, v1}, Landroid/widget/TabHost$TabSpec;->setContent(I)Landroid/widget/TabHost$TabSpec;

    move-result-object v0

    invoke-virtual {v9, v0}, Landroid/widget/TabHost;->addTab(Landroid/widget/TabHost$TabSpec;)V

    invoke-virtual {p0, v4}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v9, v0}, Landroid/widget/TabHost;->newTabSpec(Ljava/lang/String;)Landroid/widget/TabHost$TabSpec;

    move-result-object v0

    invoke-virtual {p0, v4}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TabHost$TabSpec;->setIndicator(Ljava/lang/CharSequence;)Landroid/widget/TabHost$TabSpec;

    move-result-object v0

    const v1, 0x7f07000a

    invoke-virtual {v0, v1}, Landroid/widget/TabHost$TabSpec;->setContent(I)Landroid/widget/TabHost$TabSpec;

    move-result-object v0

    invoke-virtual {v9, v0}, Landroid/widget/TabHost;->addTab(Landroid/widget/TabHost$TabSpec;)V

    const v0, 0x7f050034

    invoke-virtual {p0, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v9, v0}, Landroid/widget/TabHost;->newTabSpec(Ljava/lang/String;)Landroid/widget/TabHost$TabSpec;

    move-result-object v0

    const v1, 0x7f050034

    invoke-virtual {p0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TabHost$TabSpec;->setIndicator(Ljava/lang/CharSequence;)Landroid/widget/TabHost$TabSpec;

    move-result-object v0

    const v1, 0x7f070022

    invoke-virtual {v0, v1}, Landroid/widget/TabHost$TabSpec;->setContent(I)Landroid/widget/TabHost$TabSpec;

    move-result-object v0

    invoke-virtual {v9, v0}, Landroid/widget/TabHost;->addTab(Landroid/widget/TabHost$TabSpec;)V

    invoke-virtual {p0, v5}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v9, v0}, Landroid/widget/TabHost;->newTabSpec(Ljava/lang/String;)Landroid/widget/TabHost$TabSpec;

    move-result-object v0

    invoke-virtual {p0, v5}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TabHost$TabSpec;->setIndicator(Ljava/lang/CharSequence;)Landroid/widget/TabHost$TabSpec;

    move-result-object v0

    const/high16 v1, 0x7f070000

    invoke-virtual {v0, v1}, Landroid/widget/TabHost$TabSpec;->setContent(I)Landroid/widget/TabHost$TabSpec;

    move-result-object v0

    invoke-virtual {v9, v0}, Landroid/widget/TabHost;->addTab(Landroid/widget/TabHost$TabSpec;)V

    new-instance v0, Lcom/mediatek/ygps/YgpsActivity$2;

    invoke-direct {v0, p0}, Lcom/mediatek/ygps/YgpsActivity$2;-><init>(Lcom/mediatek/ygps/YgpsActivity;)V

    invoke-virtual {v9, v0}, Landroid/widget/TabHost;->setOnTabChangedListener(Landroid/widget/TabHost$OnTabChangeListener;)V

    invoke-direct {p0}, Lcom/mediatek/ygps/YgpsActivity;->setLayout()V

    new-instance v7, Landroid/content/Intent;

    const-string v0, "com.mediatek.ygps.YgpsService"

    invoke-direct {v7, v0}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0, v7}, Landroid/content/ContextWrapper;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;

    const-string v0, "YGPS/Activity"

    const-string v1, "START service"

    invoke-static {v0, v1}, Lcom/mediatek/xlog/Xlog;->v(Ljava/lang/String;Ljava/lang/String;)I

    new-instance v0, Lcom/mediatek/ygps/YgpsActivity$YgpsWakeLock;

    invoke-direct {v0, p0}, Lcom/mediatek/ygps/YgpsActivity$YgpsWakeLock;-><init>(Lcom/mediatek/ygps/YgpsActivity;)V

    iput-object v0, p0, Lcom/mediatek/ygps/YgpsActivity;->mYgpsWakeLock:Lcom/mediatek/ygps/YgpsActivity$YgpsWakeLock;

    iget-object v0, p0, Lcom/mediatek/ygps/YgpsActivity;->mYgpsWakeLock:Lcom/mediatek/ygps/YgpsActivity$YgpsWakeLock;

    invoke-virtual {v0, p0}, Lcom/mediatek/ygps/YgpsActivity$YgpsWakeLock;->acquireScreenWakeLock(Landroid/content/Context;)V

    new-instance v0, Lcom/mediatek/ygps/YgpsActivity$NmeaListenClass;

    invoke-direct {v0, p0}, Lcom/mediatek/ygps/YgpsActivity$NmeaListenClass;-><init>(Lcom/mediatek/ygps/YgpsActivity;)V

    iput-object v0, p0, Lcom/mediatek/ygps/YgpsActivity;->mNmeaListener:Lcom/mediatek/ygps/YgpsActivity$NmeaListenClass;

    :try_start_0
    const-string v0, "location"

    invoke-virtual {p0, v0}, Landroid/app/Activity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/location/LocationManager;

    iput-object v0, p0, Lcom/mediatek/ygps/YgpsActivity;->mLocationManager:Landroid/location/LocationManager;

    iget-object v0, p0, Lcom/mediatek/ygps/YgpsActivity;->mLocationManager:Landroid/location/LocationManager;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/mediatek/ygps/YgpsActivity;->mLocationManager:Landroid/location/LocationManager;

    const-string v1, "gps"

    const-wide/16 v2, 0x0

    const/4 v4, 0x0

    iget-object v5, p0, Lcom/mediatek/ygps/YgpsActivity;->mLocListener:Landroid/location/LocationListener;

    invoke-virtual/range {v0 .. v5}, Landroid/location/LocationManager;->requestLocationUpdates(Ljava/lang/String;JFLandroid/location/LocationListener;)V

    iget-object v0, p0, Lcom/mediatek/ygps/YgpsActivity;->mLocationManager:Landroid/location/LocationManager;

    iget-object v1, p0, Lcom/mediatek/ygps/YgpsActivity;->mGpsListener:Landroid/location/GpsStatus$Listener;

    invoke-virtual {v0, v1}, Landroid/location/LocationManager;->addGpsStatusListener(Landroid/location/GpsStatus$Listener;)Z

    iget-object v0, p0, Lcom/mediatek/ygps/YgpsActivity;->mLocationManager:Landroid/location/LocationManager;

    iget-object v1, p0, Lcom/mediatek/ygps/YgpsActivity;->mNmeaListener:Lcom/mediatek/ygps/YgpsActivity$NmeaListenClass;

    invoke-virtual {v0, v1}, Landroid/location/LocationManager;->addNmeaListener(Landroid/location/GpsStatus$NmeaListener;)Z

    iget-object v0, p0, Lcom/mediatek/ygps/YgpsActivity;->mLocationManager:Landroid/location/LocationManager;

    const-string v1, "gps"

    invoke-virtual {v0, v1}, Landroid/location/LocationManager;->isProviderEnabled(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    const v0, 0x7f050069

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    const-string v3, "gps"

    aput-object v3, v1, v2

    invoke-virtual {p0, v0, v1}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/Object;

    invoke-static {v0, v1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/mediatek/ygps/YgpsActivity;->mProvider:Ljava/lang/String;

    :goto_0
    const v0, 0x7f050070

    invoke-virtual {p0, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/mediatek/ygps/YgpsActivity;->mStatus:Ljava/lang/String;
    :try_end_0
    .catch Ljava/lang/SecurityException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_1

    :goto_1
    iget-object v0, p0, Lcom/mediatek/ygps/YgpsActivity;->mHandler:Landroid/os/Handler;

    const/16 v1, 0x3e8

    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    const-string v0, "RunInBG"

    invoke-virtual {p0, v0, v10}, Landroid/content/ContextWrapper;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v8

    const-string v0, "RunInBG"

    invoke-interface {v8, v0, v10}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    if-eqz v0, :cond_2

    iput-boolean v11, p0, Lcom/mediatek/ygps/YgpsActivity;->mIsRunInBg:Z

    :goto_2
    iput-boolean v11, p0, Lcom/mediatek/ygps/YgpsActivity;->mShowFirstFixLocate:Z

    new-instance v0, Landroid/content/IntentFilter;

    const-string v1, "android.intent.action.SCREEN_OFF"

    invoke-direct {v0, v1}, Landroid/content/IntentFilter;-><init>(Ljava/lang/String;)V

    iput-object v0, p0, Lcom/mediatek/ygps/YgpsActivity;->mPowerKeyFilter:Landroid/content/IntentFilter;

    new-instance v0, Lcom/mediatek/ygps/YgpsActivity$3;

    invoke-direct {v0, p0}, Lcom/mediatek/ygps/YgpsActivity$3;-><init>(Lcom/mediatek/ygps/YgpsActivity;)V

    iput-object v0, p0, Lcom/mediatek/ygps/YgpsActivity;->mPowerKeyReceiver:Landroid/content/BroadcastReceiver;

    iget-object v0, p0, Lcom/mediatek/ygps/YgpsActivity;->mPowerKeyReceiver:Landroid/content/BroadcastReceiver;

    iget-object v1, p0, Lcom/mediatek/ygps/YgpsActivity;->mPowerKeyFilter:Landroid/content/IntentFilter;

    invoke-virtual {p0, v0, v1}, Landroid/content/ContextWrapper;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    const-string v0, "YGPS/Activity"

    const-string v1, "registerReceiver powerKeyReceiver"

    invoke-static {v0, v1}, Lcom/mediatek/xlog/Xlog;->v(Ljava/lang/String;Ljava/lang/String;)I

    new-instance v0, Lcom/mediatek/ygps/ClientSocket;

    invoke-direct {v0, p0}, Lcom/mediatek/ygps/ClientSocket;-><init>(Lcom/mediatek/ygps/YgpsActivity;)V

    iput-object v0, p0, Lcom/mediatek/ygps/YgpsActivity;->mSocketClient:Lcom/mediatek/ygps/ClientSocket;

    iget-object v0, p0, Lcom/mediatek/ygps/YgpsActivity;->mHandler:Landroid/os/Handler;

    const/16 v1, 0x3eb

    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    return-void

    :cond_0
    const v0, 0x7f05006a

    const/4 v1, 0x1

    :try_start_1
    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    const-string v3, "gps"

    aput-object v3, v1, v2

    invoke-virtual {p0, v0, v1}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/Object;

    invoke-static {v0, v1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/mediatek/ygps/YgpsActivity;->mProvider:Ljava/lang/String;
    :try_end_1
    .catch Ljava/lang/SecurityException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_1 .. :try_end_1} :catch_1

    goto :goto_0

    :catch_0
    move-exception v6

    const-string v0, "security exception"

    invoke-static {p0, v0, v11}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    const-string v0, "YGPS/Activity"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Exception: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v6}, Ljava/lang/Throwable;->getMessage()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/mediatek/xlog/Xlog;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_1

    :cond_1
    :try_start_2
    const-string v0, "YGPS/Activity"

    const-string v1, "new mLocationManager failed"

    invoke-static {v0, v1}, Lcom/mediatek/xlog/Xlog;->w(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_2
    .catch Ljava/lang/SecurityException; {:try_start_2 .. :try_end_2} :catch_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_2 .. :try_end_2} :catch_1

    goto/16 :goto_1

    :catch_1
    move-exception v6

    const-string v0, "YGPS/Activity"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Exception: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v6}, Ljava/lang/Throwable;->getMessage()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/mediatek/xlog/Xlog;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_1

    :cond_2
    iput-boolean v10, p0, Lcom/mediatek/ygps/YgpsActivity;->mIsRunInBg:Z

    goto/16 :goto_2
.end method

.method protected onCreateDialog(I)Landroid/app/Dialog;
    .locals 3
    .param p1    # I

    const/4 v2, 0x0

    const/4 v0, 0x0

    if-nez p1, :cond_0

    new-instance v0, Landroid/app/ProgressDialog;

    invoke-direct {v0, p0}, Landroid/app/ProgressDialog;-><init>(Landroid/content/Context;)V

    const v1, 0x7f050072

    invoke-virtual {v0, v1}, Landroid/app/Dialog;->setTitle(I)V

    const v1, 0x7f050073

    invoke-virtual {p0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/app/ProgressDialog;->setMessage(Ljava/lang/CharSequence;)V

    invoke-virtual {v0, v2}, Landroid/app/ProgressDialog;->setProgressStyle(I)V

    invoke-virtual {v0, v2}, Landroid/app/Dialog;->setCancelable(Z)V

    invoke-virtual {v0, v2}, Landroid/app/ProgressDialog;->setProgress(I)V

    :goto_0
    return-object v0

    :cond_0
    new-instance v0, Landroid/app/ProgressDialog;

    invoke-direct {v0, p0}, Landroid/app/ProgressDialog;-><init>(Landroid/content/Context;)V

    const v1, 0x7f050074

    invoke-virtual {v0, v1}, Landroid/app/Dialog;->setTitle(I)V

    const v1, 0x7f050075

    invoke-virtual {p0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/app/ProgressDialog;->setMessage(Ljava/lang/CharSequence;)V

    goto :goto_0
.end method

.method public onCreateOptionsMenu(Landroid/view/Menu;)Z
    .locals 6
    .param p1    # Landroid/view/Menu;

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    invoke-super {p0, p1}, Landroid/app/Activity;->onCreateOptionsMenu(Landroid/view/Menu;)Z

    move-result v1

    iget-boolean v2, p0, Lcom/mediatek/ygps/YgpsActivity;->mShowLoc:Z

    if-eqz v2, :cond_0

    const v2, 0x7f050007

    invoke-interface {p1, v3, v4, v3, v2}, Landroid/view/Menu;->add(IIII)Landroid/view/MenuItem;

    :goto_0
    const-string v2, "RunInBG"

    invoke-virtual {p0, v2, v3}, Landroid/content/ContextWrapper;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v0

    const-string v2, "RunInBG"

    invoke-interface {v0, v2, v3}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v2

    if-eqz v2, :cond_1

    const v2, 0x7f050067

    invoke-interface {p1, v3, v5, v3, v2}, Landroid/view/Menu;->add(IIII)Landroid/view/MenuItem;

    :goto_1
    return v1

    :cond_0
    const v2, 0x7f050006

    invoke-interface {p1, v3, v4, v3, v2}, Landroid/view/Menu;->add(IIII)Landroid/view/MenuItem;

    goto :goto_0

    :cond_1
    const v2, 0x7f050068

    invoke-interface {p1, v3, v5, v3, v2}, Landroid/view/Menu;->add(IIII)Landroid/view/MenuItem;

    goto :goto_1
.end method

.method protected onDestroy()V
    .locals 5

    const-string v3, "YGPS/Activity"

    const-string v4, "enter onDestroy function"

    invoke-static {v3, v4}, Lcom/mediatek/xlog/Xlog;->v(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v3, p0, Lcom/mediatek/ygps/YgpsActivity;->mLocationManager:Landroid/location/LocationManager;

    iget-object v4, p0, Lcom/mediatek/ygps/YgpsActivity;->mLocListener:Landroid/location/LocationListener;

    invoke-virtual {v3, v4}, Landroid/location/LocationManager;->removeUpdates(Landroid/location/LocationListener;)V

    iget-object v3, p0, Lcom/mediatek/ygps/YgpsActivity;->mLocationManager:Landroid/location/LocationManager;

    iget-object v4, p0, Lcom/mediatek/ygps/YgpsActivity;->mGpsListener:Landroid/location/GpsStatus$Listener;

    invoke-virtual {v3, v4}, Landroid/location/LocationManager;->removeGpsStatusListener(Landroid/location/GpsStatus$Listener;)V

    iget-object v3, p0, Lcom/mediatek/ygps/YgpsActivity;->mLocationManager:Landroid/location/LocationManager;

    iget-object v4, p0, Lcom/mediatek/ygps/YgpsActivity;->mNmeaListener:Lcom/mediatek/ygps/YgpsActivity$NmeaListenClass;

    invoke-virtual {v3, v4}, Landroid/location/LocationManager;->removeNmeaListener(Landroid/location/GpsStatus$NmeaListener;)V

    iget-object v3, p0, Lcom/mediatek/ygps/YgpsActivity;->mHandler:Landroid/os/Handler;

    const/16 v4, 0x3e9

    invoke-virtual {v3, v4}, Landroid/os/Handler;->removeMessages(I)V

    iget-object v3, p0, Lcom/mediatek/ygps/YgpsActivity;->mHandler:Landroid/os/Handler;

    const/16 v4, 0x3e8

    invoke-virtual {v3, v4}, Landroid/os/Handler;->removeMessages(I)V

    iget-object v3, p0, Lcom/mediatek/ygps/YgpsActivity;->mHandler:Landroid/os/Handler;

    const/16 v4, 0x3eb

    invoke-virtual {v3, v4}, Landroid/os/Handler;->removeMessages(I)V

    const/4 v3, 0x1

    iput-boolean v3, p0, Lcom/mediatek/ygps/YgpsActivity;->mIsExit:Z

    iget-object v3, p0, Lcom/mediatek/ygps/YgpsActivity;->mOutputNMEALog:Ljava/io/FileOutputStream;

    if-eqz v3, :cond_0

    invoke-direct {p0}, Lcom/mediatek/ygps/YgpsActivity;->finishSavingNMEALog()V

    :cond_0
    new-instance v0, Landroid/content/Intent;

    const-string v3, "com.mediatek.ygps.YgpsService"

    invoke-direct {v0, v3}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0}, Landroid/content/ContextWrapper;->getBaseContext()Landroid/content/Context;

    move-result-object v3

    invoke-virtual {v3, v0}, Landroid/content/Context;->stopService(Landroid/content/Intent;)Z

    const-string v3, "YGPS/Activity"

    const-string v4, "STOP service"

    invoke-static {v3, v4}, Lcom/mediatek/xlog/Xlog;->v(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v3, p0, Lcom/mediatek/ygps/YgpsActivity;->mPowerKeyReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {p0, v3}, Landroid/content/ContextWrapper;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    const-string v3, "YGPS/Activity"

    const-string v4, "unregisterReceiver powerKeyReceiver"

    invoke-static {v3, v4}, Lcom/mediatek/xlog/Xlog;->v(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v3, p0, Lcom/mediatek/ygps/YgpsActivity;->mSocketClient:Lcom/mediatek/ygps/ClientSocket;

    invoke-virtual {v3}, Lcom/mediatek/ygps/ClientSocket;->endClient()V

    const-string v3, "first.time"

    const/4 v4, 0x0

    invoke-virtual {p0, v3, v4}, Landroid/content/ContextWrapper;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v1

    const-string v3, "first.time"

    const/4 v4, 0x0

    invoke-interface {v1, v3, v4}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    if-eqz v2, :cond_1

    const-string v3, "2"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_1

    const-string v3, "test.mode"

    const-string v4, "0"

    invoke-static {v3, v4}, Lcom/mediatek/ygps/GpsMnlSetting;->setMnlProp(Ljava/lang/String;Ljava/lang/String;)V

    :cond_1
    invoke-super {p0}, Landroid/app/ActivityGroup;->onDestroy()V

    return-void
.end method

.method public onOptionsItemSelected(Landroid/view/MenuItem;)Z
    .locals 5
    .param p1    # Landroid/view/MenuItem;

    const/4 v1, 0x1

    const/4 v2, 0x0

    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v3

    packed-switch v3, :pswitch_data_0

    move v1, v2

    :goto_0
    return v1

    :pswitch_0
    new-instance v2, Landroid/content/Intent;

    const-class v3, Lcom/mediatek/ygps/CopyrightInfo;

    invoke-direct {v2, p0, v3}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    invoke-virtual {p0, v2}, Landroid/app/Activity;->startActivity(Landroid/content/Intent;)V

    goto :goto_0

    :pswitch_1
    iget-boolean v3, p0, Lcom/mediatek/ygps/YgpsActivity;->mShowLoc:Z

    if-eqz v3, :cond_1

    iput-boolean v2, p0, Lcom/mediatek/ygps/YgpsActivity;->mShowLoc:Z

    iget-object v2, p0, Lcom/mediatek/ygps/YgpsActivity;->mPrompt:Landroid/widget/Toast;

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/mediatek/ygps/YgpsActivity;->mPrompt:Landroid/widget/Toast;

    invoke-virtual {v2}, Landroid/widget/Toast;->cancel()V

    :cond_0
    const v2, 0x7f050006

    invoke-interface {p1, v2}, Landroid/view/MenuItem;->setTitle(I)Landroid/view/MenuItem;

    goto :goto_0

    :cond_1
    iput-boolean v1, p0, Lcom/mediatek/ygps/YgpsActivity;->mShowLoc:Z

    const v2, 0x7f050007

    invoke-interface {p1, v2}, Landroid/view/MenuItem;->setTitle(I)Landroid/view/MenuItem;

    goto :goto_0

    :pswitch_2
    const-string v3, "RunInBG"

    invoke-virtual {p0, v3, v2}, Landroid/content/ContextWrapper;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v0

    const-string v3, "RunInBG"

    invoke-interface {v0, v3, v2}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v3

    if-eqz v3, :cond_2

    const v3, 0x7f050068

    invoke-interface {p1, v3}, Landroid/view/MenuItem;->setTitle(I)Landroid/view/MenuItem;

    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v3

    const-string v4, "RunInBG"

    invoke-interface {v3, v4, v2}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    move-result-object v2

    invoke-interface {v2}, Landroid/content/SharedPreferences$Editor;->commit()Z

    const-string v2, "EM/YGPS_BG"

    const-string v3, "now should *not* be in bg."

    invoke-static {v2, v3}, Lcom/mediatek/xlog/Xlog;->v(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    :cond_2
    const v2, 0x7f050067

    invoke-interface {p1, v2}, Landroid/view/MenuItem;->setTitle(I)Landroid/view/MenuItem;

    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v2

    const-string v3, "RunInBG"

    invoke-interface {v2, v3, v1}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    move-result-object v2

    invoke-interface {v2}, Landroid/content/SharedPreferences$Editor;->commit()Z

    const-string v2, "EM/YGPS_BG"

    const-string v3, "now should be in bg."

    invoke-static {v2, v3}, Lcom/mediatek/xlog/Xlog;->v(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public onPause()V
    .locals 2

    invoke-super {p0}, Landroid/app/ActivityGroup;->onPause()V

    const-string v0, "YGPS/Activity"

    const-string v1, "Enter onPause function"

    invoke-static {v0, v1}, Lcom/mediatek/xlog/Xlog;->v(Ljava/lang/String;Ljava/lang/String;)I

    return-void
.end method

.method public onResponse(Ljava/lang/String;)V
    .locals 4
    .param p1    # Ljava/lang/String;

    const-string v1, "YGPS/Activity"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Enter getResponse: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/mediatek/xlog/Xlog;->v(Ljava/lang/String;Ljava/lang/String;)I

    if-eqz p1, :cond_0

    invoke-virtual {p1}, Ljava/lang/String;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v1, p0, Lcom/mediatek/ygps/YgpsActivity;->mHandler:Landroid/os/Handler;

    const/16 v2, 0x3e9

    invoke-virtual {v1, v2}, Landroid/os/Handler;->obtainMessage(I)Landroid/os/Message;

    move-result-object v0

    const-string v1, "$PMTK705"

    invoke-virtual {p1, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_2

    const/16 v1, 0x44e

    iput v1, v0, Landroid/os/Message;->arg1:I

    :goto_1
    iput-object p1, v0, Landroid/os/Message;->obj:Ljava/lang/Object;

    iget-object v1, p0, Lcom/mediatek/ygps/YgpsActivity;->mHandler:Landroid/os/Handler;

    invoke-virtual {v1, v0}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    goto :goto_0

    :cond_2
    const-string v1, "PMTK001"

    invoke-virtual {p1, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_3

    const/16 v1, 0x44d

    iput v1, v0, Landroid/os/Message;->arg1:I

    goto :goto_1

    :cond_3
    const/16 v1, 0x44f

    iput v1, v0, Landroid/os/Message;->arg1:I

    goto :goto_1
.end method

.method protected onRestart()V
    .locals 6

    const/4 v4, 0x1

    const/4 v3, 0x0

    const-string v0, "YGPS/Activity"

    const-string v1, "Enter onRestart function"

    invoke-static {v0, v1}, Lcom/mediatek/xlog/Xlog;->v(Ljava/lang/String;Ljava/lang/String;)I

    const-string v0, "EM/YGPS_BG"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "mbRunInBG "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-boolean v2, p0, Lcom/mediatek/ygps/YgpsActivity;->mIsRunInBg:Z

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/mediatek/xlog/Xlog;->v(Ljava/lang/String;Ljava/lang/String;)I

    iget-boolean v0, p0, Lcom/mediatek/ygps/YgpsActivity;->mIsRunInBg:Z

    if-nez v0, :cond_0

    iput-boolean v3, p0, Lcom/mediatek/ygps/YgpsActivity;->mFirstFix:Z

    iget-object v0, p0, Lcom/mediatek/ygps/YgpsActivity;->mLocationManager:Landroid/location/LocationManager;

    const-string v1, "gps"

    invoke-virtual {v0, v1}, Landroid/location/LocationManager;->isProviderEnabled(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    const v0, 0x7f050069

    new-array v1, v4, [Ljava/lang/Object;

    const-string v2, "gps"

    aput-object v2, v1, v3

    invoke-virtual {p0, v0, v1}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    new-array v1, v3, [Ljava/lang/Object;

    invoke-static {v0, v1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/mediatek/ygps/YgpsActivity;->mProvider:Ljava/lang/String;

    :goto_0
    const v0, 0x7f050070

    invoke-virtual {p0, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/mediatek/ygps/YgpsActivity;->mStatus:Ljava/lang/String;

    iget-object v0, p0, Lcom/mediatek/ygps/YgpsActivity;->mLocationManager:Landroid/location/LocationManager;

    const-string v1, "gps"

    const-wide/16 v2, 0x0

    const/4 v4, 0x0

    iget-object v5, p0, Lcom/mediatek/ygps/YgpsActivity;->mLocListener:Landroid/location/LocationListener;

    invoke-virtual/range {v0 .. v5}, Landroid/location/LocationManager;->requestLocationUpdates(Ljava/lang/String;JFLandroid/location/LocationListener;)V

    iget-object v0, p0, Lcom/mediatek/ygps/YgpsActivity;->mLocationManager:Landroid/location/LocationManager;

    iget-object v1, p0, Lcom/mediatek/ygps/YgpsActivity;->mGpsListener:Landroid/location/GpsStatus$Listener;

    invoke-virtual {v0, v1}, Landroid/location/LocationManager;->addGpsStatusListener(Landroid/location/GpsStatus$Listener;)Z

    :cond_0
    iget-object v0, p0, Lcom/mediatek/ygps/YgpsActivity;->mYgpsWakeLock:Lcom/mediatek/ygps/YgpsActivity$YgpsWakeLock;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/mediatek/ygps/YgpsActivity;->mYgpsWakeLock:Lcom/mediatek/ygps/YgpsActivity$YgpsWakeLock;

    invoke-virtual {v0, p0}, Lcom/mediatek/ygps/YgpsActivity$YgpsWakeLock;->acquireScreenWakeLock(Landroid/content/Context;)V

    :goto_1
    invoke-super {p0}, Landroid/app/Activity;->onRestart()V

    return-void

    :cond_1
    const v0, 0x7f05006a

    new-array v1, v4, [Ljava/lang/Object;

    const-string v2, "gps"

    aput-object v2, v1, v3

    invoke-virtual {p0, v0, v1}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    new-array v1, v3, [Ljava/lang/Object;

    invoke-static {v0, v1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/mediatek/ygps/YgpsActivity;->mProvider:Ljava/lang/String;

    goto :goto_0

    :cond_2
    const-string v0, "YGPS/Activity"

    const-string v1, "mYGPSWakeLock is null"

    invoke-static {v0, v1}, Lcom/mediatek/xlog/Xlog;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1
.end method

.method protected onResume()V
    .locals 4

    invoke-super {p0}, Landroid/app/ActivityGroup;->onResume()V

    const-string v2, "YGPS/Activity"

    const-string v3, "Enter onResume function"

    invoke-static {v2, v3}, Lcom/mediatek/xlog/Xlog;->v(Ljava/lang/String;Ljava/lang/String;)I

    const v2, 0x7f07000b

    invoke-virtual {p0, v2}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iget-object v2, p0, Lcom/mediatek/ygps/YgpsActivity;->mProvider:Ljava/lang/String;

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    const v2, 0x7f07000c

    invoke-virtual {p0, v2}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iget-object v2, p0, Lcom/mediatek/ygps/YgpsActivity;->mStatus:Ljava/lang/String;

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    return-void
.end method

.method protected onStop()V
    .locals 3

    invoke-super {p0}, Landroid/app/ActivityGroup;->onStop()V

    const-string v0, "YGPS/Activity"

    const-string v1, "Enter onStop function"

    invoke-static {v0, v1}, Lcom/mediatek/xlog/Xlog;->v(Ljava/lang/String;Ljava/lang/String;)I

    const-string v0, "EM/YGPS_BG"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "mbRunInBG "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-boolean v2, p0, Lcom/mediatek/ygps/YgpsActivity;->mIsRunInBg:Z

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/mediatek/xlog/Xlog;->v(Ljava/lang/String;Ljava/lang/String;)I

    iget-boolean v0, p0, Lcom/mediatek/ygps/YgpsActivity;->mIsRunInBg:Z

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/mediatek/ygps/YgpsActivity;->mLocationManager:Landroid/location/LocationManager;

    iget-object v1, p0, Lcom/mediatek/ygps/YgpsActivity;->mLocListener:Landroid/location/LocationListener;

    invoke-virtual {v0, v1}, Landroid/location/LocationManager;->removeUpdates(Landroid/location/LocationListener;)V

    iget-object v0, p0, Lcom/mediatek/ygps/YgpsActivity;->mLocationManager:Landroid/location/LocationManager;

    iget-object v1, p0, Lcom/mediatek/ygps/YgpsActivity;->mGpsListener:Landroid/location/GpsStatus$Listener;

    invoke-virtual {v0, v1}, Landroid/location/LocationManager;->removeGpsStatusListener(Landroid/location/GpsStatus$Listener;)V

    :cond_0
    iget-object v0, p0, Lcom/mediatek/ygps/YgpsActivity;->mYgpsWakeLock:Lcom/mediatek/ygps/YgpsActivity$YgpsWakeLock;

    invoke-virtual {v0}, Lcom/mediatek/ygps/YgpsActivity$YgpsWakeLock;->release()V

    iget-object v0, p0, Lcom/mediatek/ygps/YgpsActivity;->mPrompt:Landroid/widget/Toast;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/mediatek/ygps/YgpsActivity;->mPrompt:Landroid/widget/Toast;

    invoke-virtual {v0}, Landroid/widget/Toast;->cancel()V

    :cond_1
    iget-object v0, p0, Lcom/mediatek/ygps/YgpsActivity;->mStatusPrompt:Landroid/widget/Toast;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/mediatek/ygps/YgpsActivity;->mStatusPrompt:Landroid/widget/Toast;

    invoke-virtual {v0}, Landroid/widget/Toast;->cancel()V

    :cond_2
    return-void
.end method

.method public setSatelliteStatus(I[I[F[F[FII[I)V
    .locals 4
    .param p1    # I
    .param p2    # [I
    .param p3    # [F
    .param p4    # [F
    .param p5    # [F
    .param p6    # I
    .param p7    # I
    .param p8    # [I

    const-string v0, "YGPS/Activity"

    const-string v1, "Enter setSatelliteStatus function"

    invoke-static {v0, v1}, Lcom/mediatek/xlog/Xlog;->v(Ljava/lang/String;Ljava/lang/String;)I

    monitor-enter p0

    :try_start_0
    invoke-direct {p0}, Lcom/mediatek/ygps/YgpsActivity;->emptyArray()V

    iput p1, p0, Lcom/mediatek/ygps/YgpsActivity;->mSatellites:I

    const/4 v0, 0x0

    iget-object v1, p0, Lcom/mediatek/ygps/YgpsActivity;->mPrns:[I

    const/4 v2, 0x0

    iget v3, p0, Lcom/mediatek/ygps/YgpsActivity;->mSatellites:I

    invoke-static {p2, v0, v1, v2, v3}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    const/4 v0, 0x0

    iget-object v1, p0, Lcom/mediatek/ygps/YgpsActivity;->mSnrs:[F

    const/4 v2, 0x0

    iget v3, p0, Lcom/mediatek/ygps/YgpsActivity;->mSatellites:I

    invoke-static {p3, v0, v1, v2, v3}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    const/4 v0, 0x0

    iget-object v1, p0, Lcom/mediatek/ygps/YgpsActivity;->mElevation:[F

    const/4 v2, 0x0

    iget v3, p0, Lcom/mediatek/ygps/YgpsActivity;->mSatellites:I

    invoke-static {p4, v0, v1, v2, v3}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    const/4 v0, 0x0

    iget-object v1, p0, Lcom/mediatek/ygps/YgpsActivity;->mAzimuth:[F

    const/4 v2, 0x0

    iget v3, p0, Lcom/mediatek/ygps/YgpsActivity;->mSatellites:I

    invoke-static {p5, v0, v1, v2, v3}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    const/4 v0, 0x0

    iget-object v1, p0, Lcom/mediatek/ygps/YgpsActivity;->mUsedInFixMask:[I

    const/4 v2, 0x0

    const/16 v3, 0x8

    invoke-static {p8, v0, v1, v2, v3}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    iget-object v0, p0, Lcom/mediatek/ygps/YgpsActivity;->mSatelliteView:Lcom/mediatek/ygps/SatelliteSkyView;

    invoke-virtual {v0}, Landroid/view/View;->postInvalidate()V

    iget-object v0, p0, Lcom/mediatek/ygps/YgpsActivity;->mSignalView:Lcom/mediatek/ygps/SatelliteSignalView;

    invoke-virtual {v0}, Landroid/view/View;->postInvalidate()V

    return-void

    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method

.method public setSatelliteStatus(Ljava/lang/Iterable;)V
    .locals 9
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Iterable",
            "<",
            "Landroid/location/GpsSatellite;",
            ">;)V"
        }
    .end annotation

    const-string v4, "YGPS/Activity"

    const-string v5, "Enter setSatelliteStatus function"

    invoke-static {v4, v5}, Lcom/mediatek/xlog/Xlog;->v(Ljava/lang/String;Ljava/lang/String;)I

    if-nez p1, :cond_0

    invoke-direct {p0}, Lcom/mediatek/ygps/YgpsActivity;->emptyArray()V

    :goto_0
    const-string v4, "YGPS/Activity"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "Found "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget v6, p0, Lcom/mediatek/ygps/YgpsActivity;->mSatellites:I

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, " Satellites:"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget-object v6, p0, Lcom/mediatek/ygps/YgpsActivity;->mPrns:[I

    iget v7, p0, Lcom/mediatek/ygps/YgpsActivity;->mSatellites:I

    invoke-direct {p0, v6, v7}, Lcom/mediatek/ygps/YgpsActivity;->toString([II)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, ","

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget-object v6, p0, Lcom/mediatek/ygps/YgpsActivity;->mSnrs:[F

    iget v7, p0, Lcom/mediatek/ygps/YgpsActivity;->mSatellites:I

    invoke-direct {p0, v6, v7}, Lcom/mediatek/ygps/YgpsActivity;->toString([FI)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/mediatek/xlog/Xlog;->v(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v0, 0x0

    :goto_1
    iget-object v4, p0, Lcom/mediatek/ygps/YgpsActivity;->mUsedInFixMask:[I

    array-length v4, v4

    if-ge v0, v4, :cond_3

    const-string v4, "YGPS/Activity"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "Satellites Masks "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, ": 0x"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget-object v6, p0, Lcom/mediatek/ygps/YgpsActivity;->mUsedInFixMask:[I

    aget v6, v6, v0

    invoke-static {v6}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/mediatek/xlog/Xlog;->v(Ljava/lang/String;Ljava/lang/String;)I

    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    :cond_0
    monitor-enter p0

    :try_start_0
    invoke-direct {p0}, Lcom/mediatek/ygps/YgpsActivity;->emptyArray()V

    const/4 v2, 0x0

    invoke-interface {p1}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_2
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_2

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/location/GpsSatellite;

    iget-object v4, p0, Lcom/mediatek/ygps/YgpsActivity;->mPrns:[I

    invoke-virtual {v3}, Landroid/location/GpsSatellite;->getPrn()I

    move-result v5

    aput v5, v4, v2

    iget-object v4, p0, Lcom/mediatek/ygps/YgpsActivity;->mSnrs:[F

    invoke-virtual {v3}, Landroid/location/GpsSatellite;->getSnr()F

    move-result v5

    aput v5, v4, v2

    iget-object v4, p0, Lcom/mediatek/ygps/YgpsActivity;->mElevation:[F

    invoke-virtual {v3}, Landroid/location/GpsSatellite;->getElevation()F

    move-result v5

    aput v5, v4, v2

    iget-object v4, p0, Lcom/mediatek/ygps/YgpsActivity;->mAzimuth:[F

    invoke-virtual {v3}, Landroid/location/GpsSatellite;->getAzimuth()F

    move-result v5

    aput v5, v4, v2

    invoke-virtual {v3}, Landroid/location/GpsSatellite;->usedInFix()Z

    move-result v4

    if-eqz v4, :cond_1

    iget-object v4, p0, Lcom/mediatek/ygps/YgpsActivity;->mPrns:[I

    aget v4, v4, v2

    add-int/lit8 v0, v4, -0x1

    iget-object v4, p0, Lcom/mediatek/ygps/YgpsActivity;->mUsedInFixMask:[I

    div-int/lit8 v5, v0, 0x20

    aget v6, v4, v5

    const/4 v7, 0x1

    rem-int/lit8 v8, v0, 0x20

    shl-int/2addr v7, v8

    or-int/2addr v6, v7

    aput v6, v4, v5

    :cond_1
    add-int/lit8 v2, v2, 0x1

    goto :goto_2

    :cond_2
    iput v2, p0, Lcom/mediatek/ygps/YgpsActivity;->mSatellites:I

    monitor-exit p0

    goto/16 :goto_0

    :catchall_0
    move-exception v4

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v4

    :cond_3
    iget-object v4, p0, Lcom/mediatek/ygps/YgpsActivity;->mSatelliteView:Lcom/mediatek/ygps/SatelliteSkyView;

    invoke-virtual {v4}, Landroid/view/View;->postInvalidate()V

    iget-object v4, p0, Lcom/mediatek/ygps/YgpsActivity;->mSignalView:Lcom/mediatek/ygps/SatelliteSignalView;

    invoke-virtual {v4}, Landroid/view/View;->postInvalidate()V

    return-void
.end method
