.class Lcom/mediatek/ygps/YgpsActivity$7;
.super Landroid/os/Handler;
.source "YgpsActivity.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/mediatek/ygps/YgpsActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/mediatek/ygps/YgpsActivity;


# direct methods
.method constructor <init>(Lcom/mediatek/ygps/YgpsActivity;)V
    .locals 0

    iput-object p1, p0, Lcom/mediatek/ygps/YgpsActivity$7;->this$0:Lcom/mediatek/ygps/YgpsActivity;

    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 8
    .param p1    # Landroid/os/Message;

    const/4 v7, 0x3

    const/4 v6, 0x1

    iget v4, p1, Landroid/os/Message;->what:I

    packed-switch v4, :pswitch_data_0

    :cond_0
    :goto_0
    :pswitch_0
    invoke-super {p0, p1}, Landroid/os/Handler;->handleMessage(Landroid/os/Message;)V

    return-void

    :pswitch_1
    iget-object v4, p0, Lcom/mediatek/ygps/YgpsActivity$7;->this$0:Lcom/mediatek/ygps/YgpsActivity;

    invoke-static {v4}, Lcom/mediatek/ygps/YgpsActivity;->access$1400(Lcom/mediatek/ygps/YgpsActivity;)Z

    move-result v4

    if-nez v4, :cond_0

    iget-object v4, p0, Lcom/mediatek/ygps/YgpsActivity$7;->this$0:Lcom/mediatek/ygps/YgpsActivity;

    const/16 v5, 0x1f4

    invoke-static {v4, v5}, Lcom/mediatek/ygps/YgpsActivity;->access$1912(Lcom/mediatek/ygps/YgpsActivity;I)I

    iget-object v4, p0, Lcom/mediatek/ygps/YgpsActivity$7;->this$0:Lcom/mediatek/ygps/YgpsActivity;

    const v5, 0x7f07000d

    invoke-virtual {v4, v5}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/TextView;

    iget-object v4, p0, Lcom/mediatek/ygps/YgpsActivity$7;->this$0:Lcom/mediatek/ygps/YgpsActivity;

    invoke-static {v4}, Lcom/mediatek/ygps/YgpsActivity;->access$1900(Lcom/mediatek/ygps/YgpsActivity;)I

    move-result v4

    rem-int/lit16 v4, v4, 0x3e8

    if-nez v4, :cond_1

    const-string v4, "Counting"

    :goto_1
    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    const/16 v4, 0x3e8

    const-wide/16 v5, 0x1f4

    invoke-virtual {p0, v4, v5, v6}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    goto :goto_0

    :cond_1
    const-string v4, ""

    goto :goto_1

    :pswitch_2
    iget-object v4, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    invoke-virtual {v4}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    iget v4, p1, Landroid/os/Message;->arg1:I

    packed-switch v4, :pswitch_data_1

    goto :goto_0

    :pswitch_3
    const-string v4, "PMTK001,837"

    invoke-virtual {v0, v4}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v4

    if-eqz v4, :cond_0

    iget-object v4, p0, Lcom/mediatek/ygps/YgpsActivity$7;->this$0:Lcom/mediatek/ygps/YgpsActivity;

    const v5, 0x7f050064

    invoke-static {v4, v5, v6}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v4

    invoke-virtual {v4}, Landroid/widget/Toast;->show()V

    goto :goto_0

    :pswitch_4
    const-string v4, "$PMTK705"

    invoke-virtual {v0, v4}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_0

    const-string v4, ","

    invoke-virtual {v0, v4}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v1

    array-length v4, v1

    const/4 v5, 0x4

    if-lt v4, v5, :cond_0

    iget-object v4, p0, Lcom/mediatek/ygps/YgpsActivity$7;->this$0:Lcom/mediatek/ygps/YgpsActivity;

    const v5, 0x7f07001a

    invoke-virtual {v4, v5}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    if-eqz v2, :cond_2

    invoke-virtual {v2}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v4

    const-string v5, "MNL"

    invoke-virtual {v4, v5}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v4

    if-nez v4, :cond_0

    aget-object v4, v1, v7

    invoke-virtual {v2, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v4, p0, Lcom/mediatek/ygps/YgpsActivity$7;->this$0:Lcom/mediatek/ygps/YgpsActivity;

    invoke-static {v4, v6}, Lcom/mediatek/ygps/YgpsActivity;->access$802(Lcom/mediatek/ygps/YgpsActivity;Z)Z

    goto/16 :goto_0

    :cond_2
    const-string v4, "YGPS/Activity"

    const-string v5, "txt_mnl_version is null"

    invoke-static {v4, v5}, Lcom/mediatek/xlog/Xlog;->v(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0

    :pswitch_5
    iget-object v4, p0, Lcom/mediatek/ygps/YgpsActivity$7;->this$0:Lcom/mediatek/ygps/YgpsActivity;

    const/4 v5, 0x0

    invoke-virtual {v4, v5}, Lcom/mediatek/ygps/YgpsActivity;->setSatelliteStatus(Ljava/lang/Iterable;)V

    iget-object v4, p0, Lcom/mediatek/ygps/YgpsActivity$7;->this$0:Lcom/mediatek/ygps/YgpsActivity;

    invoke-static {v4}, Lcom/mediatek/ygps/YgpsActivity;->access$2800(Lcom/mediatek/ygps/YgpsActivity;)V

    iget-object v4, p0, Lcom/mediatek/ygps/YgpsActivity$7;->this$0:Lcom/mediatek/ygps/YgpsActivity;

    invoke-static {v4, v6}, Lcom/mediatek/ygps/YgpsActivity;->access$3400(Lcom/mediatek/ygps/YgpsActivity;Z)V

    goto/16 :goto_0

    :pswitch_6
    iget-object v4, p0, Lcom/mediatek/ygps/YgpsActivity$7;->this$0:Lcom/mediatek/ygps/YgpsActivity;

    invoke-static {v4}, Lcom/mediatek/ygps/YgpsActivity;->access$2708(Lcom/mediatek/ygps/YgpsActivity;)I

    iget-object v4, p0, Lcom/mediatek/ygps/YgpsActivity$7;->this$0:Lcom/mediatek/ygps/YgpsActivity;

    invoke-static {v4}, Lcom/mediatek/ygps/YgpsActivity;->access$2700(Lcom/mediatek/ygps/YgpsActivity;)I

    move-result v4

    if-ge v7, v4, :cond_3

    iget-object v4, p0, Lcom/mediatek/ygps/YgpsActivity$7;->this$0:Lcom/mediatek/ygps/YgpsActivity;

    const/4 v5, 0x0

    invoke-static {v4, v5}, Lcom/mediatek/ygps/YgpsActivity;->access$2702(Lcom/mediatek/ygps/YgpsActivity;I)I

    const/16 v4, 0x3ea

    invoke-virtual {p0, v4}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    :cond_3
    const/16 v4, 0x3eb

    const-wide/16 v5, 0x3e8

    invoke-virtual {p0, v4, v5, v6}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    goto/16 :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x3e8
        :pswitch_1
        :pswitch_2
        :pswitch_5
        :pswitch_6
    .end packed-switch

    :pswitch_data_1
    .packed-switch 0x44d
        :pswitch_3
        :pswitch_4
        :pswitch_0
    .end packed-switch
.end method
