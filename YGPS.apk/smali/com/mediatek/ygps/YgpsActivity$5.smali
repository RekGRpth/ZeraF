.class Lcom/mediatek/ygps/YgpsActivity$5;
.super Ljava/lang/Object;
.source "YgpsActivity.java"

# interfaces
.implements Landroid/location/GpsStatus$Listener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/mediatek/ygps/YgpsActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/mediatek/ygps/YgpsActivity;


# direct methods
.method constructor <init>(Lcom/mediatek/ygps/YgpsActivity;)V
    .locals 0

    iput-object p1, p0, Lcom/mediatek/ygps/YgpsActivity$5;->this$0:Lcom/mediatek/ygps/YgpsActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private isLocationFixed(Ljava/lang/Iterable;)Z
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Iterable",
            "<",
            "Landroid/location/GpsSatellite;",
            ">;)Z"
        }
    .end annotation

    const/4 v0, 0x0

    monitor-enter p0

    :try_start_0
    invoke-interface {p1}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/location/GpsSatellite;

    invoke-virtual {v2}, Landroid/location/GpsSatellite;->usedInFix()Z

    move-result v3

    if-eqz v3, :cond_0

    const/4 v0, 0x1

    :cond_1
    monitor-exit p0

    return v0

    :catchall_0
    move-exception v3

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v3
.end method

.method private onFirstFix(I)V
    .locals 10
    .param p1    # I

    const/4 v9, 0x1

    const v8, 0x7f050071

    const-string v3, "YGPS/Activity"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Enter onFirstFix function: ttff = "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/mediatek/xlog/Xlog;->v(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v3, p0, Lcom/mediatek/ygps/YgpsActivity$5;->this$0:Lcom/mediatek/ygps/YgpsActivity;

    invoke-static {v3}, Lcom/mediatek/ygps/YgpsActivity;->access$2200(Lcom/mediatek/ygps/YgpsActivity;)I

    move-result v0

    iget-object v3, p0, Lcom/mediatek/ygps/YgpsActivity$5;->this$0:Lcom/mediatek/ygps/YgpsActivity;

    invoke-static {v3}, Lcom/mediatek/ygps/YgpsActivity;->access$2300(Lcom/mediatek/ygps/YgpsActivity;)Landroid/os/Handler;

    move-result-object v3

    const/16 v4, 0x3e8

    invoke-virtual {v3, v4}, Landroid/os/Handler;->removeMessages(I)V

    iget-object v3, p0, Lcom/mediatek/ygps/YgpsActivity$5;->this$0:Lcom/mediatek/ygps/YgpsActivity;

    invoke-static {v3, p1}, Lcom/mediatek/ygps/YgpsActivity;->access$1902(Lcom/mediatek/ygps/YgpsActivity;I)I

    iget-object v3, p0, Lcom/mediatek/ygps/YgpsActivity$5;->this$0:Lcom/mediatek/ygps/YgpsActivity;

    invoke-static {v3}, Lcom/mediatek/ygps/YgpsActivity;->access$1900(Lcom/mediatek/ygps/YgpsActivity;)I

    move-result v3

    if-eq p1, v3, :cond_0

    const-string v3, "YGPS/Activity"

    const-string v4, "ttff != mTTFF"

    invoke-static {v3, v4}, Lcom/mediatek/xlog/Xlog;->w(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v3, p0, Lcom/mediatek/ygps/YgpsActivity$5;->this$0:Lcom/mediatek/ygps/YgpsActivity;

    invoke-static {v3, p1}, Lcom/mediatek/ygps/YgpsActivity;->access$1902(Lcom/mediatek/ygps/YgpsActivity;I)I

    :cond_0
    iget-object v3, p0, Lcom/mediatek/ygps/YgpsActivity$5;->this$0:Lcom/mediatek/ygps/YgpsActivity;

    invoke-static {v3, v9}, Lcom/mediatek/ygps/YgpsActivity;->access$1402(Lcom/mediatek/ygps/YgpsActivity;Z)Z

    iget-object v3, p0, Lcom/mediatek/ygps/YgpsActivity$5;->this$0:Lcom/mediatek/ygps/YgpsActivity;

    iget-object v4, p0, Lcom/mediatek/ygps/YgpsActivity$5;->this$0:Lcom/mediatek/ygps/YgpsActivity;

    const v5, 0x7f050066

    invoke-virtual {v4, v5}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v4

    const/4 v5, 0x2

    new-array v5, v5, [Ljava/lang/Object;

    const/4 v6, 0x0

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    aput-object v7, v5, v6

    iget-object v6, p0, Lcom/mediatek/ygps/YgpsActivity$5;->this$0:Lcom/mediatek/ygps/YgpsActivity;

    invoke-virtual {v6, v8}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v6

    aput-object v6, v5, v9

    invoke-static {v4, v5}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4, v9}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v3

    invoke-virtual {v3}, Landroid/widget/Toast;->show()V

    iget-object v3, p0, Lcom/mediatek/ygps/YgpsActivity$5;->this$0:Lcom/mediatek/ygps/YgpsActivity;

    const v4, 0x7f07000d

    invoke-virtual {v3, v4}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v4, p0, Lcom/mediatek/ygps/YgpsActivity$5;->this$0:Lcom/mediatek/ygps/YgpsActivity;

    invoke-static {v4}, Lcom/mediatek/ygps/YgpsActivity;->access$1900(Lcom/mediatek/ygps/YgpsActivity;)I

    move-result v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lcom/mediatek/ygps/YgpsActivity$5;->this$0:Lcom/mediatek/ygps/YgpsActivity;

    invoke-virtual {v4, v8}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v3, p0, Lcom/mediatek/ygps/YgpsActivity$5;->this$0:Lcom/mediatek/ygps/YgpsActivity;

    invoke-static {v3}, Lcom/mediatek/ygps/YgpsActivity;->access$2400(Lcom/mediatek/ygps/YgpsActivity;)Z

    move-result v3

    if-eqz v3, :cond_1

    iget-object v3, p0, Lcom/mediatek/ygps/YgpsActivity$5;->this$0:Lcom/mediatek/ygps/YgpsActivity;

    const v4, 0x7f070007

    invoke-virtual {v3, v4}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v4, p0, Lcom/mediatek/ygps/YgpsActivity$5;->this$0:Lcom/mediatek/ygps/YgpsActivity;

    invoke-static {v4}, Lcom/mediatek/ygps/YgpsActivity;->access$1900(Lcom/mediatek/ygps/YgpsActivity;)I

    move-result v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lcom/mediatek/ygps/YgpsActivity$5;->this$0:Lcom/mediatek/ygps/YgpsActivity;

    invoke-virtual {v4, v8}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v3, p0, Lcom/mediatek/ygps/YgpsActivity$5;->this$0:Lcom/mediatek/ygps/YgpsActivity;

    iget-object v4, p0, Lcom/mediatek/ygps/YgpsActivity$5;->this$0:Lcom/mediatek/ygps/YgpsActivity;

    invoke-static {v4, v0}, Lcom/mediatek/ygps/YgpsActivity;->access$2500(Lcom/mediatek/ygps/YgpsActivity;I)F

    move-result v4

    invoke-static {v3, v4}, Lcom/mediatek/ygps/YgpsActivity;->access$402(Lcom/mediatek/ygps/YgpsActivity;F)F

    iget-object v3, p0, Lcom/mediatek/ygps/YgpsActivity$5;->this$0:Lcom/mediatek/ygps/YgpsActivity;

    const v4, 0x7f070008

    invoke-virtual {v3, v4}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/TextView;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v5, p0, Lcom/mediatek/ygps/YgpsActivity$5;->this$0:Lcom/mediatek/ygps/YgpsActivity;

    invoke-static {v5}, Lcom/mediatek/ygps/YgpsActivity;->access$400(Lcom/mediatek/ygps/YgpsActivity;)F

    move-result v5

    invoke-static {v5}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/Float;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-object v5, p0, Lcom/mediatek/ygps/YgpsActivity$5;->this$0:Lcom/mediatek/ygps/YgpsActivity;

    invoke-virtual {v5, v8}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    :cond_1
    return-void
.end method

.method private onPreFix(I)V
    .locals 5
    .param p1    # I

    const-string v2, "YGPS/Activity"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Enter onPreFix function: ttff = "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/mediatek/xlog/Xlog;->v(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v2, p0, Lcom/mediatek/ygps/YgpsActivity$5;->this$0:Lcom/mediatek/ygps/YgpsActivity;

    invoke-static {v2}, Lcom/mediatek/ygps/YgpsActivity;->access$2200(Lcom/mediatek/ygps/YgpsActivity;)I

    move-result v0

    iget-object v2, p0, Lcom/mediatek/ygps/YgpsActivity$5;->this$0:Lcom/mediatek/ygps/YgpsActivity;

    invoke-static {v2}, Lcom/mediatek/ygps/YgpsActivity;->access$2300(Lcom/mediatek/ygps/YgpsActivity;)Landroid/os/Handler;

    move-result-object v2

    const/16 v3, 0x3e8

    invoke-virtual {v2, v3}, Landroid/os/Handler;->removeMessages(I)V

    iget-object v2, p0, Lcom/mediatek/ygps/YgpsActivity$5;->this$0:Lcom/mediatek/ygps/YgpsActivity;

    invoke-static {v2, p1}, Lcom/mediatek/ygps/YgpsActivity;->access$1902(Lcom/mediatek/ygps/YgpsActivity;I)I

    iget-object v2, p0, Lcom/mediatek/ygps/YgpsActivity$5;->this$0:Lcom/mediatek/ygps/YgpsActivity;

    const/4 v3, 0x1

    invoke-static {v2, v3}, Lcom/mediatek/ygps/YgpsActivity;->access$1402(Lcom/mediatek/ygps/YgpsActivity;Z)Z

    iget-object v2, p0, Lcom/mediatek/ygps/YgpsActivity$5;->this$0:Lcom/mediatek/ygps/YgpsActivity;

    const v3, 0x7f07000d

    invoke-virtual {v2, v3}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v3, p0, Lcom/mediatek/ygps/YgpsActivity$5;->this$0:Lcom/mediatek/ygps/YgpsActivity;

    invoke-static {v3}, Lcom/mediatek/ygps/YgpsActivity;->access$1900(Lcom/mediatek/ygps/YgpsActivity;)I

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/mediatek/ygps/YgpsActivity$5;->this$0:Lcom/mediatek/ygps/YgpsActivity;

    const v4, 0x7f050071

    invoke-virtual {v3, v4}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    return-void
.end method


# virtual methods
.method public onGpsStatusChanged(I)V
    .locals 5
    .param p1    # I

    const-string v2, "YGPS/Activity"

    const-string v3, "Enter onGpsStatusChanged function"

    invoke-static {v2, v3}, Lcom/mediatek/xlog/Xlog;->v(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v2, p0, Lcom/mediatek/ygps/YgpsActivity$5;->this$0:Lcom/mediatek/ygps/YgpsActivity;

    invoke-static {v2}, Lcom/mediatek/ygps/YgpsActivity;->access$2600(Lcom/mediatek/ygps/YgpsActivity;)Landroid/location/LocationManager;

    move-result-object v2

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Landroid/location/LocationManager;->getGpsStatus(Landroid/location/GpsStatus;)Landroid/location/GpsStatus;

    move-result-object v0

    packed-switch p1, :pswitch_data_0

    :cond_0
    :goto_0
    iget-object v2, p0, Lcom/mediatek/ygps/YgpsActivity$5;->this$0:Lcom/mediatek/ygps/YgpsActivity;

    const v3, 0x7f07000c

    invoke-virtual {v2, v3}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iget-object v2, p0, Lcom/mediatek/ygps/YgpsActivity$5;->this$0:Lcom/mediatek/ygps/YgpsActivity;

    invoke-static {v2}, Lcom/mediatek/ygps/YgpsActivity;->access$2100(Lcom/mediatek/ygps/YgpsActivity;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    const-string v2, "YGPS/Activity"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "onGpsStatusChanged:"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " Status:"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lcom/mediatek/ygps/YgpsActivity$5;->this$0:Lcom/mediatek/ygps/YgpsActivity;

    invoke-static {v4}, Lcom/mediatek/ygps/YgpsActivity;->access$2100(Lcom/mediatek/ygps/YgpsActivity;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/mediatek/xlog/Xlog;->v(Ljava/lang/String;Ljava/lang/String;)I

    return-void

    :pswitch_0
    iget-object v2, p0, Lcom/mediatek/ygps/YgpsActivity$5;->this$0:Lcom/mediatek/ygps/YgpsActivity;

    iget-object v3, p0, Lcom/mediatek/ygps/YgpsActivity$5;->this$0:Lcom/mediatek/ygps/YgpsActivity;

    const v4, 0x7f05006b

    invoke-virtual {v3, v4}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/mediatek/ygps/YgpsActivity;->access$2102(Lcom/mediatek/ygps/YgpsActivity;Ljava/lang/String;)Ljava/lang/String;

    goto :goto_0

    :pswitch_1
    iget-object v2, p0, Lcom/mediatek/ygps/YgpsActivity$5;->this$0:Lcom/mediatek/ygps/YgpsActivity;

    iget-object v3, p0, Lcom/mediatek/ygps/YgpsActivity$5;->this$0:Lcom/mediatek/ygps/YgpsActivity;

    const v4, 0x7f05006c

    invoke-virtual {v3, v4}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/mediatek/ygps/YgpsActivity;->access$2102(Lcom/mediatek/ygps/YgpsActivity;Ljava/lang/String;)Ljava/lang/String;

    goto :goto_0

    :pswitch_2
    invoke-virtual {v0}, Landroid/location/GpsStatus;->getTimeToFirstFix()I

    move-result v2

    invoke-direct {p0, v2}, Lcom/mediatek/ygps/YgpsActivity$5;->onFirstFix(I)V

    iget-object v2, p0, Lcom/mediatek/ygps/YgpsActivity$5;->this$0:Lcom/mediatek/ygps/YgpsActivity;

    iget-object v3, p0, Lcom/mediatek/ygps/YgpsActivity$5;->this$0:Lcom/mediatek/ygps/YgpsActivity;

    const v4, 0x7f05006d

    invoke-virtual {v3, v4}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/mediatek/ygps/YgpsActivity;->access$2102(Lcom/mediatek/ygps/YgpsActivity;Ljava/lang/String;)Ljava/lang/String;

    goto :goto_0

    :pswitch_3
    iget-object v2, p0, Lcom/mediatek/ygps/YgpsActivity$5;->this$0:Lcom/mediatek/ygps/YgpsActivity;

    const/4 v3, 0x0

    invoke-static {v2, v3}, Lcom/mediatek/ygps/YgpsActivity;->access$2702(Lcom/mediatek/ygps/YgpsActivity;I)I

    iget-object v2, p0, Lcom/mediatek/ygps/YgpsActivity$5;->this$0:Lcom/mediatek/ygps/YgpsActivity;

    invoke-virtual {v0}, Landroid/location/GpsStatus;->getSatellites()Ljava/lang/Iterable;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/mediatek/ygps/YgpsActivity;->setSatelliteStatus(Ljava/lang/Iterable;)V

    invoke-virtual {v0}, Landroid/location/GpsStatus;->getSatellites()Ljava/lang/Iterable;

    move-result-object v2

    invoke-direct {p0, v2}, Lcom/mediatek/ygps/YgpsActivity$5;->isLocationFixed(Ljava/lang/Iterable;)Z

    move-result v2

    if-nez v2, :cond_2

    iget-object v2, p0, Lcom/mediatek/ygps/YgpsActivity$5;->this$0:Lcom/mediatek/ygps/YgpsActivity;

    invoke-static {v2}, Lcom/mediatek/ygps/YgpsActivity;->access$2800(Lcom/mediatek/ygps/YgpsActivity;)V

    iget-object v2, p0, Lcom/mediatek/ygps/YgpsActivity$5;->this$0:Lcom/mediatek/ygps/YgpsActivity;

    iget-object v3, p0, Lcom/mediatek/ygps/YgpsActivity$5;->this$0:Lcom/mediatek/ygps/YgpsActivity;

    const v4, 0x7f05006e

    invoke-virtual {v3, v4}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/mediatek/ygps/YgpsActivity;->access$2102(Lcom/mediatek/ygps/YgpsActivity;Ljava/lang/String;)Ljava/lang/String;

    :cond_1
    :goto_1
    iget-object v2, p0, Lcom/mediatek/ygps/YgpsActivity$5;->this$0:Lcom/mediatek/ygps/YgpsActivity;

    invoke-static {v2}, Lcom/mediatek/ygps/YgpsActivity;->access$800(Lcom/mediatek/ygps/YgpsActivity;)Z

    move-result v2

    if-nez v2, :cond_0

    iget-object v2, p0, Lcom/mediatek/ygps/YgpsActivity$5;->this$0:Lcom/mediatek/ygps/YgpsActivity;

    invoke-static {v2}, Lcom/mediatek/ygps/YgpsActivity;->access$1000(Lcom/mediatek/ygps/YgpsActivity;)V

    goto/16 :goto_0

    :cond_2
    iget-object v2, p0, Lcom/mediatek/ygps/YgpsActivity$5;->this$0:Lcom/mediatek/ygps/YgpsActivity;

    iget-object v3, p0, Lcom/mediatek/ygps/YgpsActivity$5;->this$0:Lcom/mediatek/ygps/YgpsActivity;

    const v4, 0x7f05006f

    invoke-virtual {v3, v4}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/mediatek/ygps/YgpsActivity;->access$2102(Lcom/mediatek/ygps/YgpsActivity;Ljava/lang/String;)Ljava/lang/String;

    iget-object v2, p0, Lcom/mediatek/ygps/YgpsActivity$5;->this$0:Lcom/mediatek/ygps/YgpsActivity;

    invoke-static {v2}, Lcom/mediatek/ygps/YgpsActivity;->access$1400(Lcom/mediatek/ygps/YgpsActivity;)Z

    move-result v2

    if-nez v2, :cond_1

    invoke-virtual {v0}, Landroid/location/GpsStatus;->getTimeToFirstFix()I

    move-result v2

    invoke-direct {p0, v2}, Lcom/mediatek/ygps/YgpsActivity$5;->onPreFix(I)V

    goto :goto_1

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method
