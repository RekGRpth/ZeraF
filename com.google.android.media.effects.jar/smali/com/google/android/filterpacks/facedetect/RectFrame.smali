.class public Lcom/google/android/filterpacks/facedetect/RectFrame;
.super Landroid/filterfw/core/NativeBuffer;
.source "RectFrame.java"


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const-string v0, "filterpack_facedetect"

    invoke-static {v0}, Ljava/lang/System;->loadLibrary(Ljava/lang/String;)V

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Landroid/filterfw/core/NativeBuffer;-><init>()V

    return-void
.end method

.method public constructor <init>(I)V
    .locals 0
    .param p1    # I

    invoke-direct {p0, p1}, Landroid/filterfw/core/NativeBuffer;-><init>(I)V

    return-void
.end method

.method private native nativeGetHeight(I)F
.end method

.method private native nativeGetWidth(I)F
.end method

.method private native nativeGetX(I)F
.end method

.method private native nativeGetY(I)F
.end method

.method private native nativeSetHeight(IF)Z
.end method

.method private native nativeSetWidth(IF)Z
.end method

.method private native nativeSetX(IF)Z
.end method

.method private native nativeSetY(IF)Z
.end method


# virtual methods
.method public native getElementSize()I
.end method

.method public getHeight(I)F
    .locals 1
    .param p1    # I

    invoke-virtual {p0}, Lcom/google/android/filterpacks/facedetect/RectFrame;->assertReadable()V

    invoke-direct {p0, p1}, Lcom/google/android/filterpacks/facedetect/RectFrame;->nativeGetHeight(I)F

    move-result v0

    return v0
.end method

.method public getWidth(I)F
    .locals 1
    .param p1    # I

    invoke-virtual {p0}, Lcom/google/android/filterpacks/facedetect/RectFrame;->assertReadable()V

    invoke-direct {p0, p1}, Lcom/google/android/filterpacks/facedetect/RectFrame;->nativeGetWidth(I)F

    move-result v0

    return v0
.end method

.method public getX(I)F
    .locals 1
    .param p1    # I

    invoke-virtual {p0}, Lcom/google/android/filterpacks/facedetect/RectFrame;->assertReadable()V

    invoke-direct {p0, p1}, Lcom/google/android/filterpacks/facedetect/RectFrame;->nativeGetX(I)F

    move-result v0

    return v0
.end method

.method public getY(I)F
    .locals 1
    .param p1    # I

    invoke-virtual {p0}, Lcom/google/android/filterpacks/facedetect/RectFrame;->assertReadable()V

    invoke-direct {p0, p1}, Lcom/google/android/filterpacks/facedetect/RectFrame;->nativeGetY(I)F

    move-result v0

    return v0
.end method

.method public setHeight(IF)V
    .locals 0
    .param p1    # I
    .param p2    # F

    invoke-virtual {p0}, Lcom/google/android/filterpacks/facedetect/RectFrame;->assertWritable()V

    invoke-direct {p0, p1, p2}, Lcom/google/android/filterpacks/facedetect/RectFrame;->nativeSetHeight(IF)Z

    return-void
.end method

.method public setWidth(IF)V
    .locals 0
    .param p1    # I
    .param p2    # F

    invoke-virtual {p0}, Lcom/google/android/filterpacks/facedetect/RectFrame;->assertWritable()V

    invoke-direct {p0, p1, p2}, Lcom/google/android/filterpacks/facedetect/RectFrame;->nativeSetWidth(IF)Z

    return-void
.end method

.method public setX(IF)V
    .locals 0
    .param p1    # I
    .param p2    # F

    invoke-virtual {p0}, Lcom/google/android/filterpacks/facedetect/RectFrame;->assertWritable()V

    invoke-direct {p0, p1, p2}, Lcom/google/android/filterpacks/facedetect/RectFrame;->nativeSetX(IF)Z

    return-void
.end method

.method public setY(IF)V
    .locals 0
    .param p1    # I
    .param p2    # F

    invoke-virtual {p0}, Lcom/google/android/filterpacks/facedetect/RectFrame;->assertWritable()V

    invoke-direct {p0, p1, p2}, Lcom/google/android/filterpacks/facedetect/RectFrame;->nativeSetY(IF)Z

    return-void
.end method
