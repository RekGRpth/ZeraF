.class public Lorg/jivesoftware/smack/packet/BatchPresence;
.super Lorg/jivesoftware/smack/packet/Packet;
.source "BatchPresence.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lorg/jivesoftware/smack/packet/BatchPresence$Type;
    }
.end annotation


# instance fields
.field private mPresenceList:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lorg/jivesoftware/smack/packet/Presence;",
            ">;"
        }
    .end annotation
.end field

.field private mType:Lorg/jivesoftware/smack/packet/BatchPresence$Type;


# direct methods
.method public constructor <init>(Lorg/jivesoftware/smack/packet/BatchPresence$Type;)V
    .locals 1
    .param p1    # Lorg/jivesoftware/smack/packet/BatchPresence$Type;

    invoke-direct {p0}, Lorg/jivesoftware/smack/packet/Packet;-><init>()V

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lorg/jivesoftware/smack/packet/BatchPresence;->mPresenceList:Ljava/util/ArrayList;

    invoke-virtual {p0, p1}, Lorg/jivesoftware/smack/packet/BatchPresence;->setType(Lorg/jivesoftware/smack/packet/BatchPresence$Type;)V

    return-void
.end method


# virtual methods
.method public addPresenceStanza(Lorg/jivesoftware/smack/packet/Presence;)V
    .locals 1
    .param p1    # Lorg/jivesoftware/smack/packet/Presence;

    iget-object v0, p0, Lorg/jivesoftware/smack/packet/BatchPresence;->mPresenceList:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    return-void
.end method

.method public getNumPresenceStanzas()I
    .locals 1

    iget-object v0, p0, Lorg/jivesoftware/smack/packet/BatchPresence;->mPresenceList:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    return v0
.end method

.method public getPresenceStanzaList()Ljava/util/ArrayList;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList",
            "<",
            "Lorg/jivesoftware/smack/packet/Presence;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lorg/jivesoftware/smack/packet/BatchPresence;->mPresenceList:Ljava/util/ArrayList;

    return-object v0
.end method

.method public setType(Lorg/jivesoftware/smack/packet/BatchPresence$Type;)V
    .locals 0
    .param p1    # Lorg/jivesoftware/smack/packet/BatchPresence$Type;

    iput-object p1, p0, Lorg/jivesoftware/smack/packet/BatchPresence;->mType:Lorg/jivesoftware/smack/packet/BatchPresence$Type;

    return-void
.end method

.method public toProtoBuf()Lcom/google/common/io/protocol/ProtoBuf;
    .locals 8

    new-instance v2, Lcom/google/common/io/protocol/ProtoBuf;

    sget-object v5, Lcom/google/android/gsf/gtalkservice/proto/GtalkCoreMessageTypes;->BATCH_PRESENCE_STANZA:Lcom/google/common/io/protocol/ProtoBufType;

    invoke-direct {v2, v5}, Lcom/google/common/io/protocol/ProtoBuf;-><init>(Lcom/google/common/io/protocol/ProtoBufType;)V

    invoke-virtual {p0}, Lorg/jivesoftware/smack/packet/BatchPresence;->getPacketID()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v5

    if-nez v5, :cond_0

    const-string v5, "ID_NOT_AVAILABLE"

    invoke-virtual {v5, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-nez v5, :cond_0

    const/4 v5, 0x1

    invoke-virtual {v2, v5, v0}, Lcom/google/common/io/protocol/ProtoBuf;->setString(ILjava/lang/String;)V

    :cond_0
    invoke-virtual {p0}, Lorg/jivesoftware/smack/packet/BatchPresence;->getTo()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v5

    if-nez v5, :cond_1

    const/4 v5, 0x2

    invoke-virtual {v2, v5, v4}, Lcom/google/common/io/protocol/ProtoBuf;->setString(ILjava/lang/String;)V

    :cond_1
    const/16 v5, 0x8

    iget-object v6, p0, Lorg/jivesoftware/smack/packet/BatchPresence;->mType:Lorg/jivesoftware/smack/packet/BatchPresence$Type;

    invoke-virtual {v6}, Lorg/jivesoftware/smack/packet/BatchPresence$Type;->ordinal()I

    move-result v6

    invoke-virtual {v2, v5, v6}, Lcom/google/common/io/protocol/ProtoBuf;->setInt(II)V

    const/4 v5, 0x7

    invoke-virtual {p0}, Lorg/jivesoftware/smack/packet/BatchPresence;->getAccountId()J

    move-result-wide v6

    invoke-virtual {v2, v5, v6, v7}, Lcom/google/common/io/protocol/ProtoBuf;->setLong(IJ)V

    invoke-virtual {p0}, Lorg/jivesoftware/smack/packet/BatchPresence;->getRmq2Id()Ljava/lang/String;

    move-result-object v3

    if-eqz v3, :cond_2

    const/4 v5, 0x4

    invoke-virtual {v2, v5, v3}, Lcom/google/common/io/protocol/ProtoBuf;->setString(ILjava/lang/String;)V

    :cond_2
    invoke-virtual {p0}, Lorg/jivesoftware/smack/packet/BatchPresence;->getLastStreamId()I

    move-result v1

    const/4 v5, -0x1

    if-eq v1, v5, :cond_3

    const/4 v5, 0x6

    invoke-virtual {v2, v5, v1}, Lcom/google/common/io/protocol/ProtoBuf;->setInt(II)V

    :cond_3
    return-object v2
.end method

.method public toXML()Ljava/lang/String;
    .locals 14

    const/4 v13, -0x1

    const/16 v12, 0x20

    new-instance v0, Ljava/lang/StringBuffer;

    invoke-direct {v0}, Ljava/lang/StringBuffer;-><init>()V

    const-string v9, "<iq"

    invoke-virtual {v0, v9}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    invoke-virtual {p0}, Lorg/jivesoftware/smack/packet/BatchPresence;->getPacketID()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p0}, Lorg/jivesoftware/smack/packet/BatchPresence;->getTo()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {p0}, Lorg/jivesoftware/smack/packet/BatchPresence;->getRmq2Id()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {p0}, Lorg/jivesoftware/smack/packet/BatchPresence;->getLastStreamId()I

    move-result v3

    invoke-virtual {p0}, Lorg/jivesoftware/smack/packet/BatchPresence;->getStreamId()I

    move-result v7

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, " type="

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    iget-object v9, p0, Lorg/jivesoftware/smack/packet/BatchPresence;->mType:Lorg/jivesoftware/smack/packet/BatchPresence$Type;

    sget-object v11, Lorg/jivesoftware/smack/packet/BatchPresence$Type;->GET:Lorg/jivesoftware/smack/packet/BatchPresence$Type;

    if-ne v9, v11, :cond_5

    const-string v9, "GET"

    :goto_0
    invoke-virtual {v10, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v0, v9}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    if-eqz v4, :cond_0

    const-string v9, " id=\""

    invoke-virtual {v0, v9}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v9

    invoke-virtual {v9, v4}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v9

    const-string v10, "\""

    invoke-virtual {v9, v10}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    :cond_0
    if-eqz v8, :cond_1

    const-string v9, " to=\""

    invoke-virtual {v0, v9}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v9

    invoke-virtual {v9, v8}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v9

    const-string v10, "\""

    invoke-virtual {v9, v10}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    :cond_1
    const-string v9, " account-id=\""

    invoke-virtual {v0, v9}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v9

    invoke-virtual {p0}, Lorg/jivesoftware/smack/packet/BatchPresence;->getAccountId()J

    move-result-wide v10

    invoke-virtual {v9, v10, v11}, Ljava/lang/StringBuffer;->append(J)Ljava/lang/StringBuffer;

    move-result-object v9

    const-string v10, "\" "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    if-eqz v6, :cond_2

    invoke-virtual {v0, v12}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    move-result-object v9

    const-string v10, "persistent_id=\""

    invoke-virtual {v9, v10}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v9

    invoke-virtual {v9, v6}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v9

    const-string v10, "\""

    invoke-virtual {v9, v10}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    :cond_2
    if-eq v3, v13, :cond_3

    invoke-virtual {v0, v12}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    move-result-object v9

    const-string v10, "last_stream_id=\""

    invoke-virtual {v9, v10}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v9

    invoke-virtual {v9, v3}, Ljava/lang/StringBuffer;->append(I)Ljava/lang/StringBuffer;

    move-result-object v9

    const-string v10, "\""

    invoke-virtual {v9, v10}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    :cond_3
    if-eq v7, v13, :cond_4

    invoke-virtual {v0, v12}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    move-result-object v9

    const-string v10, "stream_id=\""

    invoke-virtual {v9, v10}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v9

    invoke-virtual {v9, v7}, Ljava/lang/StringBuffer;->append(I)Ljava/lang/StringBuffer;

    move-result-object v9

    const-string v10, "\""

    invoke-virtual {v9, v10}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    :cond_4
    const-string v9, "><mp:batch xmlns:mp=\"google:mobile:presence\">"

    invoke-virtual {v0, v9}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    invoke-virtual {p0}, Lorg/jivesoftware/smack/packet/BatchPresence;->getNumPresenceStanzas()I

    move-result v1

    const/4 v2, 0x0

    :goto_1
    if-ge v2, v1, :cond_6

    iget-object v9, p0, Lorg/jivesoftware/smack/packet/BatchPresence;->mPresenceList:Ljava/util/ArrayList;

    invoke-virtual {v9, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lorg/jivesoftware/smack/packet/Presence;

    invoke-virtual {v5}, Lorg/jivesoftware/smack/packet/Presence;->toXML()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v0, v9}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    :cond_5
    const-string v9, "SET"

    goto/16 :goto_0

    :cond_6
    const-string v9, "</mp:batch></iq>"

    invoke-virtual {v0, v9}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    invoke-virtual {v0}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v9

    return-object v9
.end method
