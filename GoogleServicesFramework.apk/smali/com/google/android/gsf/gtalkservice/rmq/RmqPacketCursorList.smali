.class public Lcom/google/android/gsf/gtalkservice/rmq/RmqPacketCursorList;
.super Lcom/google/android/gsf/gtalkservice/rmq/RmqPacketList;
.source "RmqPacketCursorList.java"


# instance fields
.field private mAccountidColumnIndex:I

.field private final mCursor:Landroid/database/Cursor;

.field private mDataColumnIndex:I

.field private mPacketIdColumnIndex:I

.field private mProtobufTagIndex:I

.field private mRmqIdColumnIndex:I


# direct methods
.method public constructor <init>(Landroid/database/Cursor;)V
    .locals 2
    .param p1    # Landroid/database/Cursor;

    invoke-direct {p0}, Lcom/google/android/gsf/gtalkservice/rmq/RmqPacketList;-><init>()V

    iput-object p1, p0, Lcom/google/android/gsf/gtalkservice/rmq/RmqPacketCursorList;->mCursor:Landroid/database/Cursor;

    iget-object v0, p0, Lcom/google/android/gsf/gtalkservice/rmq/RmqPacketCursorList;->mCursor:Landroid/database/Cursor;

    const-string v1, "data"

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lcom/google/android/gsf/gtalkservice/rmq/RmqPacketCursorList;->mDataColumnIndex:I

    iget-object v0, p0, Lcom/google/android/gsf/gtalkservice/rmq/RmqPacketCursorList;->mCursor:Landroid/database/Cursor;

    const-string v1, "type"

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lcom/google/android/gsf/gtalkservice/rmq/RmqPacketCursorList;->mProtobufTagIndex:I

    iget-object v0, p0, Lcom/google/android/gsf/gtalkservice/rmq/RmqPacketCursorList;->mCursor:Landroid/database/Cursor;

    const-string v1, "rmq_id"

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lcom/google/android/gsf/gtalkservice/rmq/RmqPacketCursorList;->mRmqIdColumnIndex:I

    iget-object v0, p0, Lcom/google/android/gsf/gtalkservice/rmq/RmqPacketCursorList;->mCursor:Landroid/database/Cursor;

    const-string v1, "packet_id"

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lcom/google/android/gsf/gtalkservice/rmq/RmqPacketCursorList;->mPacketIdColumnIndex:I

    iget-object v0, p0, Lcom/google/android/gsf/gtalkservice/rmq/RmqPacketCursorList;->mCursor:Landroid/database/Cursor;

    const-string v1, "account"

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lcom/google/android/gsf/gtalkservice/rmq/RmqPacketCursorList;->mAccountidColumnIndex:I

    return-void
.end method


# virtual methods
.method public close()V
    .locals 1

    iget-object v0, p0, Lcom/google/android/gsf/gtalkservice/rmq/RmqPacketCursorList;->mCursor:Landroid/database/Cursor;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/gsf/gtalkservice/rmq/RmqPacketCursorList;->mCursor:Landroid/database/Cursor;

    invoke-interface {v0}, Landroid/database/Cursor;->close()V

    :cond_0
    return-void
.end method

.method public getPacketAt(I)Lorg/jivesoftware/smack/packet/Packet;
    .locals 10
    .param p1    # I

    const/4 v3, 0x0

    iget-object v8, p0, Lcom/google/android/gsf/gtalkservice/rmq/RmqPacketCursorList;->mCursor:Landroid/database/Cursor;

    if-nez v8, :cond_1

    :cond_0
    :goto_0
    return-object v3

    :cond_1
    iget-object v8, p0, Lcom/google/android/gsf/gtalkservice/rmq/RmqPacketCursorList;->mCursor:Landroid/database/Cursor;

    invoke-interface {v8, p1}, Landroid/database/Cursor;->moveToPosition(I)Z

    iget-object v8, p0, Lcom/google/android/gsf/gtalkservice/rmq/RmqPacketCursorList;->mCursor:Landroid/database/Cursor;

    iget v9, p0, Lcom/google/android/gsf/gtalkservice/rmq/RmqPacketCursorList;->mRmqIdColumnIndex:I

    invoke-interface {v8, v9}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v5

    iget-object v8, p0, Lcom/google/android/gsf/gtalkservice/rmq/RmqPacketCursorList;->mCursor:Landroid/database/Cursor;

    iget v9, p0, Lcom/google/android/gsf/gtalkservice/rmq/RmqPacketCursorList;->mProtobufTagIndex:I

    invoke-interface {v8, v9}, Landroid/database/Cursor;->getInt(I)I

    move-result v7

    iget-object v8, p0, Lcom/google/android/gsf/gtalkservice/rmq/RmqPacketCursorList;->mCursor:Landroid/database/Cursor;

    iget v9, p0, Lcom/google/android/gsf/gtalkservice/rmq/RmqPacketCursorList;->mDataColumnIndex:I

    invoke-interface {v8, v9}, Landroid/database/Cursor;->getBlob(I)[B

    move-result-object v2

    iget-object v8, p0, Lcom/google/android/gsf/gtalkservice/rmq/RmqPacketCursorList;->mCursor:Landroid/database/Cursor;

    iget v9, p0, Lcom/google/android/gsf/gtalkservice/rmq/RmqPacketCursorList;->mPacketIdColumnIndex:I

    invoke-interface {v8, v9}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v4

    iget-object v8, p0, Lcom/google/android/gsf/gtalkservice/rmq/RmqPacketCursorList;->mCursor:Landroid/database/Cursor;

    iget v9, p0, Lcom/google/android/gsf/gtalkservice/rmq/RmqPacketCursorList;->mAccountidColumnIndex:I

    invoke-interface {v8, v9}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v0

    if-eqz v2, :cond_0

    new-instance v3, Lcom/google/android/gsf/gtalkservice/rmq/RmqPacket;

    int-to-byte v8, v7

    invoke-direct {v3, v5, v6, v8, v2}, Lcom/google/android/gsf/gtalkservice/rmq/RmqPacket;-><init>(JB[B)V

    invoke-virtual {v3, v0, v1}, Lorg/jivesoftware/smack/packet/Packet;->setAccountId(J)V

    invoke-virtual {v3, v4}, Lorg/jivesoftware/smack/packet/Packet;->setPacketID(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public size()I
    .locals 1

    iget-object v0, p0, Lcom/google/android/gsf/gtalkservice/rmq/RmqPacketCursorList;->mCursor:Landroid/database/Cursor;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lcom/google/android/gsf/gtalkservice/rmq/RmqPacketCursorList;->mCursor:Landroid/database/Cursor;

    invoke-interface {v0}, Landroid/database/Cursor;->getCount()I

    move-result v0

    goto :goto_0
.end method
