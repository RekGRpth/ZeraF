.class public Lcom/google/android/gsf/gtalkservice/gtalk/GTalkConnectionContext;
.super Ljava/lang/Object;
.source "GTalkConnectionContext.java"


# instance fields
.field private mChatMgr:Lcom/google/android/gsf/gtalkservice/gtalk/ChatMgr;

.field private mConnection:Lcom/google/android/gsf/gtalkservice/gtalk/GTalkConnection;

.field private mGTalkService:Lcom/google/android/gsf/gtalkservice/service/GTalkService;

.field private mOtrMgr:Lcom/google/android/gsf/gtalkservice/gtalk/OtrManager;

.field private mRosterListenerImpl:Lcom/google/android/gsf/gtalkservice/gtalk/RosterListenerImpl;

.field private mRosterMgr:Lcom/google/android/gsf/gtalkservice/gtalk/RosterManager;

.field private mSubscriptionMgr:Lcom/google/android/gsf/gtalkservice/gtalk/SubscriptionManager;

.field private mVCardMgr:Lcom/google/android/gsf/gtalkservice/gtalk/VCardMgr;

.field private mVideoChatSessionMgr:Lcom/google/android/gsf/gtalkservice/gtalk/VideoChatSessionManager;


# direct methods
.method public constructor <init>(Lcom/google/android/gsf/gtalkservice/service/GTalkService;Lcom/google/android/gsf/gtalkservice/gtalk/GTalkConnection;)V
    .locals 1
    .param p1    # Lcom/google/android/gsf/gtalkservice/service/GTalkService;
    .param p2    # Lcom/google/android/gsf/gtalkservice/gtalk/GTalkConnection;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/google/android/gsf/gtalkservice/gtalk/GTalkConnectionContext;->mGTalkService:Lcom/google/android/gsf/gtalkservice/service/GTalkService;

    iput-object p2, p0, Lcom/google/android/gsf/gtalkservice/gtalk/GTalkConnectionContext;->mConnection:Lcom/google/android/gsf/gtalkservice/gtalk/GTalkConnection;

    new-instance v0, Lcom/google/android/gsf/gtalkservice/gtalk/RosterManager;

    invoke-direct {v0}, Lcom/google/android/gsf/gtalkservice/gtalk/RosterManager;-><init>()V

    iput-object v0, p0, Lcom/google/android/gsf/gtalkservice/gtalk/GTalkConnectionContext;->mRosterMgr:Lcom/google/android/gsf/gtalkservice/gtalk/RosterManager;

    new-instance v0, Lcom/google/android/gsf/gtalkservice/gtalk/ChatMgr;

    invoke-direct {v0}, Lcom/google/android/gsf/gtalkservice/gtalk/ChatMgr;-><init>()V

    iput-object v0, p0, Lcom/google/android/gsf/gtalkservice/gtalk/GTalkConnectionContext;->mChatMgr:Lcom/google/android/gsf/gtalkservice/gtalk/ChatMgr;

    new-instance v0, Lcom/google/android/gsf/gtalkservice/gtalk/SubscriptionManager;

    invoke-direct {v0}, Lcom/google/android/gsf/gtalkservice/gtalk/SubscriptionManager;-><init>()V

    iput-object v0, p0, Lcom/google/android/gsf/gtalkservice/gtalk/GTalkConnectionContext;->mSubscriptionMgr:Lcom/google/android/gsf/gtalkservice/gtalk/SubscriptionManager;

    new-instance v0, Lcom/google/android/gsf/gtalkservice/gtalk/VCardMgr;

    invoke-direct {v0}, Lcom/google/android/gsf/gtalkservice/gtalk/VCardMgr;-><init>()V

    iput-object v0, p0, Lcom/google/android/gsf/gtalkservice/gtalk/GTalkConnectionContext;->mVCardMgr:Lcom/google/android/gsf/gtalkservice/gtalk/VCardMgr;

    new-instance v0, Lcom/google/android/gsf/gtalkservice/gtalk/OtrManager;

    invoke-direct {v0}, Lcom/google/android/gsf/gtalkservice/gtalk/OtrManager;-><init>()V

    iput-object v0, p0, Lcom/google/android/gsf/gtalkservice/gtalk/GTalkConnectionContext;->mOtrMgr:Lcom/google/android/gsf/gtalkservice/gtalk/OtrManager;

    new-instance v0, Lcom/google/android/gsf/gtalkservice/gtalk/RosterListenerImpl;

    invoke-direct {v0}, Lcom/google/android/gsf/gtalkservice/gtalk/RosterListenerImpl;-><init>()V

    iput-object v0, p0, Lcom/google/android/gsf/gtalkservice/gtalk/GTalkConnectionContext;->mRosterListenerImpl:Lcom/google/android/gsf/gtalkservice/gtalk/RosterListenerImpl;

    new-instance v0, Lcom/google/android/gsf/gtalkservice/gtalk/VideoChatSessionManager;

    invoke-direct {v0}, Lcom/google/android/gsf/gtalkservice/gtalk/VideoChatSessionManager;-><init>()V

    iput-object v0, p0, Lcom/google/android/gsf/gtalkservice/gtalk/GTalkConnectionContext;->mVideoChatSessionMgr:Lcom/google/android/gsf/gtalkservice/gtalk/VideoChatSessionManager;

    return-void
.end method


# virtual methods
.method public getChatManager()Lcom/google/android/gsf/gtalkservice/gtalk/ChatMgr;
    .locals 1

    iget-object v0, p0, Lcom/google/android/gsf/gtalkservice/gtalk/GTalkConnectionContext;->mChatMgr:Lcom/google/android/gsf/gtalkservice/gtalk/ChatMgr;

    return-object v0
.end method

.method public getContext()Landroid/content/Context;
    .locals 1

    iget-object v0, p0, Lcom/google/android/gsf/gtalkservice/gtalk/GTalkConnectionContext;->mGTalkService:Lcom/google/android/gsf/gtalkservice/service/GTalkService;

    return-object v0
.end method

.method public getGTalkConnection()Lcom/google/android/gsf/gtalkservice/gtalk/GTalkConnection;
    .locals 1

    iget-object v0, p0, Lcom/google/android/gsf/gtalkservice/gtalk/GTalkConnectionContext;->mConnection:Lcom/google/android/gsf/gtalkservice/gtalk/GTalkConnection;

    return-object v0
.end method

.method public getGTalkConnectionAccountIdFilter()Lorg/jivesoftware/smack/filter/AccountIdFilter;
    .locals 1

    iget-object v0, p0, Lcom/google/android/gsf/gtalkservice/gtalk/GTalkConnectionContext;->mConnection:Lcom/google/android/gsf/gtalkservice/gtalk/GTalkConnection;

    invoke-virtual {v0}, Lcom/google/android/gsf/gtalkservice/gtalk/GTalkConnection;->getAccountIdFilter()Lorg/jivesoftware/smack/filter/AccountIdFilter;

    move-result-object v0

    return-object v0
.end method

.method public getGtalkAccountId()J
    .locals 2

    iget-object v0, p0, Lcom/google/android/gsf/gtalkservice/gtalk/GTalkConnectionContext;->mConnection:Lcom/google/android/gsf/gtalkservice/gtalk/GTalkConnection;

    invoke-virtual {v0}, Lcom/google/android/gsf/gtalkservice/gtalk/GTalkConnection;->getAccountId()J

    move-result-wide v0

    return-wide v0
.end method

.method public getIQPacketManager()Lcom/google/android/gsf/gtalkservice/IQPacketManager;
    .locals 1

    iget-object v0, p0, Lcom/google/android/gsf/gtalkservice/gtalk/GTalkConnectionContext;->mConnection:Lcom/google/android/gsf/gtalkservice/gtalk/GTalkConnection;

    invoke-virtual {v0}, Lcom/google/android/gsf/gtalkservice/gtalk/GTalkConnection;->getIQPacketManager()Lcom/google/android/gsf/gtalkservice/IQPacketManager;

    move-result-object v0

    return-object v0
.end method

.method public getOtrManager()Lcom/google/android/gsf/gtalkservice/gtalk/OtrManager;
    .locals 1

    iget-object v0, p0, Lcom/google/android/gsf/gtalkservice/gtalk/GTalkConnectionContext;->mOtrMgr:Lcom/google/android/gsf/gtalkservice/gtalk/OtrManager;

    return-object v0
.end method

.method public getRawStanzaSendReceiveManager()Lcom/google/android/gsf/gtalkservice/RawStanzaSendReceiveManager;
    .locals 1

    iget-object v0, p0, Lcom/google/android/gsf/gtalkservice/gtalk/GTalkConnectionContext;->mConnection:Lcom/google/android/gsf/gtalkservice/gtalk/GTalkConnection;

    invoke-virtual {v0}, Lcom/google/android/gsf/gtalkservice/gtalk/GTalkConnection;->getRawStanzaSendReceiveManager()Lcom/google/android/gsf/gtalkservice/RawStanzaSendReceiveManager;

    move-result-object v0

    return-object v0
.end method

.method public getRosterHandler()Lcom/google/android/gsf/gtalkservice/gtalk/RosterListenerImpl;
    .locals 1

    iget-object v0, p0, Lcom/google/android/gsf/gtalkservice/gtalk/GTalkConnectionContext;->mRosterListenerImpl:Lcom/google/android/gsf/gtalkservice/gtalk/RosterListenerImpl;

    return-object v0
.end method

.method public getRosterManager()Lcom/google/android/gsf/gtalkservice/gtalk/RosterManager;
    .locals 1

    iget-object v0, p0, Lcom/google/android/gsf/gtalkservice/gtalk/GTalkConnectionContext;->mRosterMgr:Lcom/google/android/gsf/gtalkservice/gtalk/RosterManager;

    return-object v0
.end method

.method public getService()Lcom/google/android/gsf/gtalkservice/service/GTalkService;
    .locals 1

    iget-object v0, p0, Lcom/google/android/gsf/gtalkservice/gtalk/GTalkConnectionContext;->mGTalkService:Lcom/google/android/gsf/gtalkservice/service/GTalkService;

    return-object v0
.end method

.method public getSubscriptionManager()Lcom/google/android/gsf/gtalkservice/gtalk/SubscriptionManager;
    .locals 1

    iget-object v0, p0, Lcom/google/android/gsf/gtalkservice/gtalk/GTalkConnectionContext;->mSubscriptionMgr:Lcom/google/android/gsf/gtalkservice/gtalk/SubscriptionManager;

    return-object v0
.end method

.method public getVCardManager()Lcom/google/android/gsf/gtalkservice/gtalk/VCardMgr;
    .locals 1

    iget-object v0, p0, Lcom/google/android/gsf/gtalkservice/gtalk/GTalkConnectionContext;->mVCardMgr:Lcom/google/android/gsf/gtalkservice/gtalk/VCardMgr;

    return-object v0
.end method

.method public getVideoChatSessionManager()Lcom/google/android/gsf/gtalkservice/gtalk/VideoChatSessionManager;
    .locals 1

    iget-object v0, p0, Lcom/google/android/gsf/gtalkservice/gtalk/GTalkConnectionContext;->mVideoChatSessionMgr:Lcom/google/android/gsf/gtalkservice/gtalk/VideoChatSessionManager;

    return-object v0
.end method

.method public init(Landroid/os/Handler;)V
    .locals 3
    .param p1    # Landroid/os/Handler;

    iget-object v1, p0, Lcom/google/android/gsf/gtalkservice/gtalk/GTalkConnectionContext;->mGTalkService:Lcom/google/android/gsf/gtalkservice/service/GTalkService;

    invoke-virtual {v1}, Lcom/google/android/gsf/gtalkservice/service/GTalkService;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/gsf/gtalkservice/gtalk/GTalkConnectionContext;->mRosterMgr:Lcom/google/android/gsf/gtalkservice/gtalk/RosterManager;

    iget-object v2, p0, Lcom/google/android/gsf/gtalkservice/gtalk/GTalkConnectionContext;->mGTalkService:Lcom/google/android/gsf/gtalkservice/service/GTalkService;

    invoke-virtual {v1, v2, v0, p0}, Lcom/google/android/gsf/gtalkservice/gtalk/RosterManager;->init(Landroid/content/Context;Landroid/content/ContentResolver;Lcom/google/android/gsf/gtalkservice/gtalk/GTalkConnectionContext;)V

    iget-object v1, p0, Lcom/google/android/gsf/gtalkservice/gtalk/GTalkConnectionContext;->mChatMgr:Lcom/google/android/gsf/gtalkservice/gtalk/ChatMgr;

    iget-object v2, p0, Lcom/google/android/gsf/gtalkservice/gtalk/GTalkConnectionContext;->mGTalkService:Lcom/google/android/gsf/gtalkservice/service/GTalkService;

    invoke-virtual {v1, v2, v0, p0, p1}, Lcom/google/android/gsf/gtalkservice/gtalk/ChatMgr;->init(Landroid/content/Context;Landroid/content/ContentResolver;Lcom/google/android/gsf/gtalkservice/gtalk/GTalkConnectionContext;Landroid/os/Handler;)V

    iget-object v1, p0, Lcom/google/android/gsf/gtalkservice/gtalk/GTalkConnectionContext;->mSubscriptionMgr:Lcom/google/android/gsf/gtalkservice/gtalk/SubscriptionManager;

    invoke-virtual {v1, v0, p0, p1}, Lcom/google/android/gsf/gtalkservice/gtalk/SubscriptionManager;->init(Landroid/content/ContentResolver;Lcom/google/android/gsf/gtalkservice/gtalk/GTalkConnectionContext;Landroid/os/Handler;)V

    iget-object v1, p0, Lcom/google/android/gsf/gtalkservice/gtalk/GTalkConnectionContext;->mVCardMgr:Lcom/google/android/gsf/gtalkservice/gtalk/VCardMgr;

    iget-object v2, p0, Lcom/google/android/gsf/gtalkservice/gtalk/GTalkConnectionContext;->mGTalkService:Lcom/google/android/gsf/gtalkservice/service/GTalkService;

    invoke-virtual {v1, v2, p0}, Lcom/google/android/gsf/gtalkservice/gtalk/VCardMgr;->init(Landroid/content/Context;Lcom/google/android/gsf/gtalkservice/gtalk/GTalkConnectionContext;)V

    iget-object v1, p0, Lcom/google/android/gsf/gtalkservice/gtalk/GTalkConnectionContext;->mOtrMgr:Lcom/google/android/gsf/gtalkservice/gtalk/OtrManager;

    iget-object v2, p0, Lcom/google/android/gsf/gtalkservice/gtalk/GTalkConnectionContext;->mGTalkService:Lcom/google/android/gsf/gtalkservice/service/GTalkService;

    invoke-virtual {v1, v2, v0, p0}, Lcom/google/android/gsf/gtalkservice/gtalk/OtrManager;->init(Landroid/content/Context;Landroid/content/ContentResolver;Lcom/google/android/gsf/gtalkservice/gtalk/GTalkConnectionContext;)V

    iget-object v1, p0, Lcom/google/android/gsf/gtalkservice/gtalk/GTalkConnectionContext;->mRosterListenerImpl:Lcom/google/android/gsf/gtalkservice/gtalk/RosterListenerImpl;

    invoke-virtual {v1, v0, p0, p1}, Lcom/google/android/gsf/gtalkservice/gtalk/RosterListenerImpl;->init(Landroid/content/ContentResolver;Lcom/google/android/gsf/gtalkservice/gtalk/GTalkConnectionContext;Landroid/os/Handler;)V

    iget-object v1, p0, Lcom/google/android/gsf/gtalkservice/gtalk/GTalkConnectionContext;->mVideoChatSessionMgr:Lcom/google/android/gsf/gtalkservice/gtalk/VideoChatSessionManager;

    iget-object v2, p0, Lcom/google/android/gsf/gtalkservice/gtalk/GTalkConnectionContext;->mGTalkService:Lcom/google/android/gsf/gtalkservice/service/GTalkService;

    invoke-virtual {v1, v2, p0}, Lcom/google/android/gsf/gtalkservice/gtalk/VideoChatSessionManager;->init(Landroid/content/Context;Lcom/google/android/gsf/gtalkservice/gtalk/GTalkConnectionContext;)V

    return-void
.end method
