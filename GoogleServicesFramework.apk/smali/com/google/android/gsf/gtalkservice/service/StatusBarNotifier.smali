.class public Lcom/google/android/gsf/gtalkservice/service/StatusBarNotifier;
.super Ljava/lang/Object;
.source "StatusBarNotifier.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/gsf/gtalkservice/service/StatusBarNotifier$NotificationInfo;
    }
.end annotation


# static fields
.field private static final ACCOUNT_NAME_PROJECTION:[Ljava/lang/String;

.field private static final AVATAR_PROJECTION:[Ljava/lang/String;


# instance fields
.field private mContext:Landroid/content/Context;

.field private mGenericAvatar:Landroid/graphics/Bitmap;

.field private mLastSoundPlayedMs:J

.field private mNotificationInfo:Lcom/google/android/gsf/gtalkservice/service/StatusBarNotifier$NotificationInfo;

.field private final mNotificationLock:Ljava/lang/Object;

.field private mNotificationMgr:Landroid/app/NotificationManager;


# direct methods
.method static constructor <clinit>()V
    .locals 4

    const/4 v3, 0x1

    const/4 v2, 0x0

    new-array v0, v3, [Ljava/lang/String;

    const-string v1, "data"

    aput-object v1, v0, v2

    sput-object v0, Lcom/google/android/gsf/gtalkservice/service/StatusBarNotifier;->AVATAR_PROJECTION:[Ljava/lang/String;

    new-array v0, v3, [Ljava/lang/String;

    const-string v1, "name"

    aput-object v1, v0, v2

    sput-object v0, Lcom/google/android/gsf/gtalkservice/service/StatusBarNotifier;->ACCOUNT_NAME_PROJECTION:[Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1    # Landroid/content/Context;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lcom/google/android/gsf/gtalkservice/service/StatusBarNotifier;->mNotificationLock:Ljava/lang/Object;

    iput-object p1, p0, Lcom/google/android/gsf/gtalkservice/service/StatusBarNotifier;->mContext:Landroid/content/Context;

    const-string v0, "notification"

    invoke-virtual {p1, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/NotificationManager;

    iput-object v0, p0, Lcom/google/android/gsf/gtalkservice/service/StatusBarNotifier;->mNotificationMgr:Landroid/app/NotificationManager;

    return-void
.end method

.method static synthetic access$002(Lcom/google/android/gsf/gtalkservice/service/StatusBarNotifier;Landroid/content/Context;)Landroid/content/Context;
    .locals 0
    .param p0    # Lcom/google/android/gsf/gtalkservice/service/StatusBarNotifier;
    .param p1    # Landroid/content/Context;

    iput-object p1, p0, Lcom/google/android/gsf/gtalkservice/service/StatusBarNotifier;->mContext:Landroid/content/Context;

    return-object p1
.end method

.method static synthetic access$100(Lcom/google/android/gsf/gtalkservice/service/StatusBarNotifier;)Landroid/app/NotificationManager;
    .locals 1
    .param p0    # Lcom/google/android/gsf/gtalkservice/service/StatusBarNotifier;

    iget-object v0, p0, Lcom/google/android/gsf/gtalkservice/service/StatusBarNotifier;->mNotificationMgr:Landroid/app/NotificationManager;

    return-object v0
.end method

.method private cancelNotify(Ljava/lang/String;JLcom/google/android/gsf/TalkContract$AccountSettings$QueryMap;)V
    .locals 4
    .param p1    # Ljava/lang/String;
    .param p2    # J
    .param p4    # Lcom/google/android/gsf/TalkContract$AccountSettings$QueryMap;

    const/4 v3, 0x3

    invoke-static {p1}, Lorg/jivesoftware/smack/util/StringUtils;->parseBareAddress(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    const-string v1, "GTalkService"

    invoke-static {v1, v3}, Lcom/google/android/gsf/gtalkservice/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "cancelNotify for "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", accountId = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2, p3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {p0, v1}, Lcom/google/android/gsf/gtalkservice/service/StatusBarNotifier;->log(Ljava/lang/String;)V

    :cond_0
    const/4 v0, 0x0

    iget-object v1, p0, Lcom/google/android/gsf/gtalkservice/service/StatusBarNotifier;->mNotificationInfo:Lcom/google/android/gsf/gtalkservice/service/StatusBarNotifier$NotificationInfo;

    if-nez v1, :cond_2

    const-string v1, "GTalkService"

    invoke-static {v1, v3}, Lcom/google/android/gsf/gtalkservice/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_1

    const-string v1, "cancelNotify: mNotificationInfo is null, cancel all"

    invoke-direct {p0, v1}, Lcom/google/android/gsf/gtalkservice/service/StatusBarNotifier;->log(Ljava/lang/String;)V

    :cond_1
    const/4 v0, 0x1

    :cond_2
    if-nez p1, :cond_4

    const-string v1, "GTalkService"

    invoke-static {v1, v3}, Lcom/google/android/gsf/gtalkservice/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_3

    const-string v1, "cancelNotify: jid is null, cancel all"

    invoke-direct {p0, v1}, Lcom/google/android/gsf/gtalkservice/service/StatusBarNotifier;->log(Ljava/lang/String;)V

    :cond_3
    const/4 v0, 0x1

    :cond_4
    if-eqz v0, :cond_5

    iget-object v1, p0, Lcom/google/android/gsf/gtalkservice/service/StatusBarNotifier;->mNotificationMgr:Landroid/app/NotificationManager;

    long-to-int v2, p2

    invoke-virtual {v1, v2}, Landroid/app/NotificationManager;->cancel(I)V

    :goto_0
    return-void

    :cond_5
    iget-object v2, p0, Lcom/google/android/gsf/gtalkservice/service/StatusBarNotifier;->mNotificationLock:Ljava/lang/Object;

    monitor-enter v2

    :try_start_0
    iget-object v1, p0, Lcom/google/android/gsf/gtalkservice/service/StatusBarNotifier;->mNotificationInfo:Lcom/google/android/gsf/gtalkservice/service/StatusBarNotifier$NotificationInfo;

    invoke-virtual {v1, p1, p2, p3}, Lcom/google/android/gsf/gtalkservice/service/StatusBarNotifier$NotificationInfo;->removeForUser(Ljava/lang/String;J)Z

    move-result v1

    if-eqz v1, :cond_7

    iget-object v1, p0, Lcom/google/android/gsf/gtalkservice/service/StatusBarNotifier;->mNotificationMgr:Landroid/app/NotificationManager;

    long-to-int v3, p2

    invoke-virtual {v1, v3}, Landroid/app/NotificationManager;->cancel(I)V

    :cond_6
    :goto_1
    monitor-exit v2

    goto :goto_0

    :catchall_0
    move-exception v1

    monitor-exit v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1

    :cond_7
    :try_start_1
    const-string v1, "GTalkService"

    const/4 v3, 0x3

    invoke-static {v1, v3}, Lcom/google/android/gsf/gtalkservice/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_6

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "cancelNotify: cannot find notification for "

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {p0, v1}, Lcom/google/android/gsf/gtalkservice/service/StatusBarNotifier;->log(Ljava/lang/String;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_1
.end method

.method private createNotification(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Landroid/app/TaskStackBuilder;ZLcom/google/android/gsf/TalkContract$AccountSettings$QueryMap;Ljava/lang/String;JLandroid/graphics/Bitmap;IZZ)Landroid/app/Notification;
    .locals 12
    .param p1    # Ljava/lang/String;
    .param p2    # Ljava/lang/String;
    .param p3    # Ljava/lang/String;
    .param p4    # Landroid/app/TaskStackBuilder;
    .param p5    # Z
    .param p6    # Lcom/google/android/gsf/TalkContract$AccountSettings$QueryMap;
    .param p7    # Ljava/lang/String;
    .param p8    # J
    .param p10    # Landroid/graphics/Bitmap;
    .param p11    # I
    .param p12    # Z
    .param p13    # Z

    new-instance v9, Landroid/app/Notification$Builder;

    iget-object v10, p0, Lcom/google/android/gsf/gtalkservice/service/StatusBarNotifier;->mContext:Landroid/content/Context;

    invoke-direct {v9, v10}, Landroid/app/Notification$Builder;-><init>(Landroid/content/Context;)V

    if-eqz p5, :cond_0

    const/4 p1, 0x0

    :cond_0
    invoke-virtual {v9, p1}, Landroid/app/Notification$Builder;->setTicker(Ljava/lang/CharSequence;)Landroid/app/Notification$Builder;

    move-result-object v9

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v10

    invoke-virtual {v9, v10, v11}, Landroid/app/Notification$Builder;->setWhen(J)Landroid/app/Notification$Builder;

    move-result-object v9

    invoke-virtual {v9, p2}, Landroid/app/Notification$Builder;->setContentTitle(Ljava/lang/CharSequence;)Landroid/app/Notification$Builder;

    move-result-object v9

    invoke-virtual {v9, p3}, Landroid/app/Notification$Builder;->setContentText(Ljava/lang/CharSequence;)Landroid/app/Notification$Builder;

    move-result-object v9

    const/4 v10, 0x0

    const/high16 v11, 0x8000000

    move-object/from16 v0, p4

    invoke-virtual {v0, v10, v11}, Landroid/app/TaskStackBuilder;->getPendingIntent(II)Landroid/app/PendingIntent;

    move-result-object v10

    invoke-virtual {v9, v10}, Landroid/app/Notification$Builder;->setContentIntent(Landroid/app/PendingIntent;)Landroid/app/Notification$Builder;

    move-result-object v9

    const v10, 0x7f020009

    invoke-virtual {v9, v10}, Landroid/app/Notification$Builder;->setSmallIcon(I)Landroid/app/Notification$Builder;

    move-result-object v7

    const/4 v9, 0x1

    move/from16 v0, p11

    if-le v0, v9, :cond_1

    move/from16 v0, p11

    invoke-virtual {v7, v0}, Landroid/app/Notification$Builder;->setNumber(I)Landroid/app/Notification$Builder;

    :cond_1
    if-eqz p10, :cond_2

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "createNotification: set notification\'s icon to the avatar "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    move-object/from16 v0, p10

    invoke-virtual {v9, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-direct {p0, v9}, Lcom/google/android/gsf/gtalkservice/service/StatusBarNotifier;->log(Ljava/lang/String;)V

    move-object/from16 v0, p10

    invoke-virtual {v7, v0}, Landroid/app/Notification$Builder;->setLargeIcon(Landroid/graphics/Bitmap;)Landroid/app/Notification$Builder;

    :cond_2
    if-nez p5, :cond_3

    if-eqz p7, :cond_3

    const-wide/16 v9, 0x0

    cmp-long v9, p8, v9

    if-lez v9, :cond_3

    if-eqz p12, :cond_3

    const-string v9, "popup"

    invoke-virtual/range {p6 .. p6}, Lcom/google/android/gsf/TalkContract$AccountSettings$QueryMap;->getTextNotification()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v9, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_3

    new-instance v4, Landroid/content/Intent;

    const-string v9, "com.google.android.talk.RECEIVE_MESSAGE"

    invoke-direct {v4, v9}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    const-string v9, "message"

    invoke-virtual {v4, v9, p3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const-string v9, "from"

    move-object/from16 v0, p7

    invoke-virtual {v4, v9, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const-string v9, "accountId"

    move-wide/from16 v0, p8

    invoke-virtual {v4, v9, v0, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;J)Landroid/content/Intent;

    const-string v9, "timestamp"

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v10

    invoke-virtual {v4, v9, v10, v11}, Landroid/content/Intent;->putExtra(Ljava/lang/String;J)Landroid/content/Intent;

    invoke-static/range {p7 .. p7}, Lorg/jivesoftware/smack/util/StringUtils;->parseBareAddress(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    const-string v9, "username"

    invoke-virtual {v4, v9, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const-string v9, "android.intent.extra.INTENT"

    invoke-virtual/range {p4 .. p4}, Landroid/app/TaskStackBuilder;->getIntentCount()I

    move-result v10

    add-int/lit8 v10, v10, -0x1

    move-object/from16 v0, p4

    invoke-virtual {v0, v10}, Landroid/app/TaskStackBuilder;->editIntentAt(I)Landroid/content/Intent;

    move-result-object v10

    invoke-virtual {v4, v9, v10}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    iget-object v9, p0, Lcom/google/android/gsf/gtalkservice/service/StatusBarNotifier;->mContext:Landroid/content/Context;

    const/4 v10, 0x0

    const/high16 v11, 0x8000000

    invoke-static {v9, v10, v4, v11}, Landroid/app/PendingIntent;->getBroadcast(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v9

    const/4 v10, 0x0

    invoke-virtual {v7, v9, v10}, Landroid/app/Notification$Builder;->setFullScreenIntent(Landroid/app/PendingIntent;Z)Landroid/app/Notification$Builder;

    :cond_3
    if-eqz p13, :cond_4

    if-eqz p7, :cond_4

    const-wide/16 v9, 0x0

    cmp-long v9, p8, v9

    if-lez v9, :cond_4

    if-eqz p3, :cond_4

    new-instance v9, Landroid/app/Notification$BigTextStyle;

    invoke-direct {v9}, Landroid/app/Notification$BigTextStyle;-><init>()V

    invoke-virtual {v9, p3}, Landroid/app/Notification$BigTextStyle;->bigText(Ljava/lang/CharSequence;)Landroid/app/Notification$BigTextStyle;

    move-result-object v9

    move-wide/from16 v0, p8

    invoke-direct {p0, v0, v1}, Lcom/google/android/gsf/gtalkservice/service/StatusBarNotifier;->getNameForAccount(J)Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v9, v10}, Landroid/app/Notification$BigTextStyle;->setSummaryText(Ljava/lang/CharSequence;)Landroid/app/Notification$BigTextStyle;

    move-result-object v9

    invoke-virtual {v7, v9}, Landroid/app/Notification$Builder;->setStyle(Landroid/app/Notification$Style;)Landroid/app/Notification$Builder;

    :cond_4
    const/4 v8, 0x0

    if-nez p5, :cond_7

    invoke-direct {p0}, Lcom/google/android/gsf/gtalkservice/service/StatusBarNotifier;->shouldSuppressSoundNotification()Z

    move-result v9

    if-nez v9, :cond_7

    move-object/from16 v0, p6

    invoke-direct {p0, v7, v0}, Lcom/google/android/gsf/gtalkservice/service/StatusBarNotifier;->setRinger(Landroid/app/Notification$Builder;Lcom/google/android/gsf/TalkContract$AccountSettings$QueryMap;)Z

    move-result v8

    :cond_5
    :goto_0
    const/4 v3, 0x4

    if-eqz v8, :cond_6

    or-int/lit8 v3, v3, 0x2

    :cond_6
    invoke-virtual {v7, v3}, Landroid/app/Notification$Builder;->setDefaults(I)Landroid/app/Notification$Builder;

    invoke-virtual {v7}, Landroid/app/Notification$Builder;->getNotification()Landroid/app/Notification;

    move-result-object v6

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "create notification returning: "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v6}, Landroid/app/Notification;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-direct {p0, v9}, Lcom/google/android/gsf/gtalkservice/service/StatusBarNotifier;->log(Ljava/lang/String;)V

    return-object v6

    :cond_7
    sget-boolean v9, Lcom/google/android/gsf/gtalkservice/LogTag;->sDebug:Z

    if-eqz v9, :cond_5

    const-string v9, "createNotification: suppress sound notification"

    invoke-direct {p0, v9}, Lcom/google/android/gsf/gtalkservice/service/StatusBarNotifier;->log(Ljava/lang/String;)V

    goto :goto_0
.end method

.method private decodeAvatar([B)Landroid/graphics/Bitmap;
    .locals 13
    .param p1    # [B

    const/4 v9, 0x0

    const/4 v1, 0x0

    if-nez p1, :cond_1

    move-object v0, v9

    :cond_0
    :goto_0
    return-object v0

    :cond_1
    array-length v2, p1

    invoke-static {p1, v1, v2}, Landroid/graphics/BitmapFactory;->decodeByteArray([BII)Landroid/graphics/Bitmap;

    move-result-object v0

    if-nez v0, :cond_2

    move-object v0, v9

    goto :goto_0

    :cond_2
    invoke-virtual {v0}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v3

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v4

    iget-object v2, p0, Lcom/google/android/gsf/gtalkservice/service/StatusBarNotifier;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v7

    const v2, 0x1050005

    invoke-virtual {v7, v2}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v11

    const v2, 0x1050006

    invoke-virtual {v7, v2}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v10

    if-gt v3, v11, :cond_3

    if-le v4, v10, :cond_0

    :cond_3
    int-to-float v2, v11

    int-to-float v6, v3

    div-float/2addr v2, v6

    int-to-float v6, v10

    int-to-float v12, v4

    div-float/2addr v6, v12

    invoke-static {v2, v6}, Ljava/lang/Math;->max(FF)F

    move-result v8

    new-instance v5, Landroid/graphics/Matrix;

    invoke-direct {v5}, Landroid/graphics/Matrix;-><init>()V

    invoke-virtual {v5, v8, v8}, Landroid/graphics/Matrix;->setScale(FF)V

    const/4 v6, 0x1

    move v2, v1

    invoke-static/range {v0 .. v6}, Landroid/graphics/Bitmap;->createBitmap(Landroid/graphics/Bitmap;IIIILandroid/graphics/Matrix;Z)Landroid/graphics/Bitmap;

    move-result-object v9

    if-eq v9, v0, :cond_4

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->recycle()V

    :cond_4
    move-object v0, v9

    goto :goto_0
.end method

.method private getAvatarFromCursor(Landroid/database/Cursor;I)Landroid/graphics/Bitmap;
    .locals 2
    .param p1    # Landroid/database/Cursor;
    .param p2    # I

    invoke-static {p1, p2}, Lcom/google/android/gsf/gtalkservice/service/StatusBarNotifier;->loadAvatarData(Landroid/database/Cursor;I)[B

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/google/android/gsf/gtalkservice/service/StatusBarNotifier;->decodeAvatar([B)Landroid/graphics/Bitmap;

    move-result-object v1

    return-object v1
.end method

.method private getGenericAvatar()Landroid/graphics/Bitmap;
    .locals 3

    iget-object v1, p0, Lcom/google/android/gsf/gtalkservice/service/StatusBarNotifier;->mGenericAvatar:Landroid/graphics/Bitmap;

    if-nez v1, :cond_0

    iget-object v1, p0, Lcom/google/android/gsf/gtalkservice/service/StatusBarNotifier;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f020001

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    check-cast v0, Landroid/graphics/drawable/BitmapDrawable;

    invoke-virtual {v0}, Landroid/graphics/drawable/BitmapDrawable;->getBitmap()Landroid/graphics/Bitmap;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/gsf/gtalkservice/service/StatusBarNotifier;->mGenericAvatar:Landroid/graphics/Bitmap;

    :cond_0
    iget-object v1, p0, Lcom/google/android/gsf/gtalkservice/service/StatusBarNotifier;->mGenericAvatar:Landroid/graphics/Bitmap;

    return-object v1
.end method

.method private getNameForAccount(J)Ljava/lang/String;
    .locals 8
    .param p1    # J

    const/4 v4, 0x0

    const/4 v7, 0x0

    iget-object v0, p0, Lcom/google/android/gsf/gtalkservice/service/StatusBarNotifier;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    sget-object v1, Lcom/google/android/gsf/TalkContract$Account;->CONTENT_URI:Landroid/net/Uri;

    sget-object v2, Lcom/google/android/gsf/gtalkservice/service/StatusBarNotifier;->ACCOUNT_NAME_PROJECTION:[Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "_id="

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1, p2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    move-object v5, v4

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v6

    if-nez v6, :cond_0

    :goto_0
    return-object v4

    :cond_0
    :try_start_0
    invoke-interface {v6}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    if-eqz v0, :cond_1

    const/4 v0, 0x0

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v7

    :cond_1
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    move-object v4, v7

    goto :goto_0

    :catchall_0
    move-exception v0

    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    throw v0
.end method

.method private internalRemoveAllNotifications()V
    .locals 2

    iget-object v0, p0, Lcom/google/android/gsf/gtalkservice/service/StatusBarNotifier;->mNotificationInfo:Lcom/google/android/gsf/gtalkservice/service/StatusBarNotifier$NotificationInfo;

    if-nez v0, :cond_0

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/google/android/gsf/gtalkservice/service/StatusBarNotifier;->mNotificationInfo:Lcom/google/android/gsf/gtalkservice/service/StatusBarNotifier$NotificationInfo;

    invoke-virtual {v0}, Lcom/google/android/gsf/gtalkservice/service/StatusBarNotifier$NotificationInfo;->cancelNotifications()V

    iget-object v1, p0, Lcom/google/android/gsf/gtalkservice/service/StatusBarNotifier;->mNotificationLock:Ljava/lang/Object;

    monitor-enter v1

    :try_start_0
    iget-object v0, p0, Lcom/google/android/gsf/gtalkservice/service/StatusBarNotifier;->mNotificationInfo:Lcom/google/android/gsf/gtalkservice/service/StatusBarNotifier$NotificationInfo;

    invoke-virtual {v0}, Lcom/google/android/gsf/gtalkservice/service/StatusBarNotifier$NotificationInfo;->removeAll()V

    monitor-exit v1

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method private static loadAvatarData(Landroid/database/Cursor;I)[B
    .locals 1
    .param p0    # Landroid/database/Cursor;
    .param p1    # I

    invoke-interface {p0, p1}, Landroid/database/Cursor;->getBlob(I)[B

    move-result-object v0

    return-object v0
.end method

.method private log(Ljava/lang/String;)V
    .locals 3
    .param p1    # Ljava/lang/String;

    const-string v0, "GTalkService"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "[StatusBarNotify] "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/gsf/gtalkservice/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method private logEmptyCursor(Ljava/lang/String;)V
    .locals 3
    .param p1    # Ljava/lang/String;

    const-string v0, "GTalkService"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "[StatusBarNotify] "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ": empty cursor, possibly low memory"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/gsf/gtalkservice/Log;->e(Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method private notify(JLjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Landroid/app/TaskStackBuilder;ZLcom/google/android/gsf/TalkContract$AccountSettings$QueryMap;ZZ)V
    .locals 20
    .param p1    # J
    .param p3    # Ljava/lang/String;
    .param p4    # Ljava/lang/String;
    .param p5    # Ljava/lang/String;
    .param p6    # Ljava/lang/String;
    .param p7    # Ljava/lang/String;
    .param p8    # Landroid/app/TaskStackBuilder;
    .param p9    # Z
    .param p10    # Lcom/google/android/gsf/TalkContract$AccountSettings$QueryMap;
    .param p11    # Z
    .param p12    # Z

    const/16 v17, 0x0

    const/16 v16, 0x0

    const/4 v13, 0x0

    const/4 v12, 0x0

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/gsf/gtalkservice/service/StatusBarNotifier;->mNotificationInfo:Lcom/google/android/gsf/gtalkservice/service/StatusBarNotifier$NotificationInfo;

    if-nez v2, :cond_0

    new-instance v2, Lcom/google/android/gsf/gtalkservice/service/StatusBarNotifier$NotificationInfo;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/gsf/gtalkservice/service/StatusBarNotifier;->mContext:Landroid/content/Context;

    move-object/from16 v0, p0

    invoke-direct {v2, v0, v3}, Lcom/google/android/gsf/gtalkservice/service/StatusBarNotifier$NotificationInfo;-><init>(Lcom/google/android/gsf/gtalkservice/service/StatusBarNotifier;Landroid/content/Context;)V

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/google/android/gsf/gtalkservice/service/StatusBarNotifier;->mNotificationInfo:Lcom/google/android/gsf/gtalkservice/service/StatusBarNotifier$NotificationInfo;

    :cond_0
    invoke-static/range {p3 .. p3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_3

    move-object/from16 v0, p0

    iget-object v10, v0, Lcom/google/android/gsf/gtalkservice/service/StatusBarNotifier;->mNotificationLock:Ljava/lang/Object;

    monitor-enter v10

    :try_start_0
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/gsf/gtalkservice/service/StatusBarNotifier;->mNotificationInfo:Lcom/google/android/gsf/gtalkservice/service/StatusBarNotifier$NotificationInfo;

    move-wide/from16 v3, p1

    move-object/from16 v5, p3

    move-object/from16 v6, p4

    move-object/from16 v7, p5

    move-object/from16 v8, p7

    move-object/from16 v9, p8

    invoke-virtual/range {v2 .. v9}, Lcom/google/android/gsf/gtalkservice/service/StatusBarNotifier$NotificationInfo;->add(JLjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Landroid/app/TaskStackBuilder;)Z

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/gsf/gtalkservice/service/StatusBarNotifier;->mNotificationInfo:Lcom/google/android/gsf/gtalkservice/service/StatusBarNotifier$NotificationInfo;

    move-wide/from16 v0, p1

    invoke-virtual {v2, v0, v1}, Lcom/google/android/gsf/gtalkservice/service/StatusBarNotifier$NotificationInfo;->getItemForAccount(J)Lcom/google/android/gsf/gtalkservice/service/StatusBarNotifier$NotificationInfo$Item;

    move-result-object v18

    move-object/from16 v0, v18

    iget-object v4, v0, Lcom/google/android/gsf/gtalkservice/service/StatusBarNotifier$NotificationInfo$Item;->mTitle:Ljava/lang/String;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :try_start_1
    move-object/from16 v0, v18

    iget-object v5, v0, Lcom/google/android/gsf/gtalkservice/service/StatusBarNotifier$NotificationInfo$Item;->mMessage:Ljava/lang/String;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    :try_start_2
    move-object/from16 v0, v18

    iget-object v0, v0, Lcom/google/android/gsf/gtalkservice/service/StatusBarNotifier$NotificationInfo$Item;->mTaskStackBuilder:Landroid/app/TaskStackBuilder;

    move-object/from16 p8, v0

    move-object/from16 v0, v18

    iget v13, v0, Lcom/google/android/gsf/gtalkservice/service/StatusBarNotifier$NotificationInfo$Item;->mUnreadCount:I

    move-object/from16 v0, v18

    iget-object v12, v0, Lcom/google/android/gsf/gtalkservice/service/StatusBarNotifier$NotificationInfo$Item;->mAvatar:Landroid/graphics/Bitmap;

    monitor-exit v10
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_2

    :goto_0
    move-object/from16 v2, p0

    move-object/from16 v3, p6

    move-object/from16 v6, p8

    move/from16 v7, p9

    move-object/from16 v8, p10

    move-object/from16 v9, p3

    move-wide/from16 v10, p1

    move/from16 v14, p11

    move/from16 v15, p12

    invoke-direct/range {v2 .. v15}, Lcom/google/android/gsf/gtalkservice/service/StatusBarNotifier;->createNotification(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Landroid/app/TaskStackBuilder;ZLcom/google/android/gsf/TalkContract$AccountSettings$QueryMap;Ljava/lang/String;JLandroid/graphics/Bitmap;IZZ)Landroid/app/Notification;

    move-result-object v19

    if-eqz v19, :cond_2

    move-object/from16 v0, v19

    iget-object v2, v0, Landroid/app/Notification;->fullScreenIntent:Landroid/app/PendingIntent;

    if-eqz v2, :cond_1

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/gsf/gtalkservice/service/StatusBarNotifier;->mNotificationMgr:Landroid/app/NotificationManager;

    move-wide/from16 v0, p1

    long-to-int v3, v0

    invoke-virtual {v2, v3}, Landroid/app/NotificationManager;->cancel(I)V

    :cond_1
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/gsf/gtalkservice/service/StatusBarNotifier;->mNotificationMgr:Landroid/app/NotificationManager;

    move-wide/from16 v0, p1

    long-to-int v3, v0

    move-object/from16 v0, v19

    invoke-virtual {v2, v3, v0}, Landroid/app/NotificationManager;->notify(ILandroid/app/Notification;)V

    :cond_2
    return-void

    :catchall_0
    move-exception v2

    move-object/from16 v5, v16

    move-object/from16 v4, v17

    :goto_1
    :try_start_3
    monitor-exit v10
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_2

    throw v2

    :catchall_1
    move-exception v2

    move-object/from16 v5, v16

    goto :goto_1

    :catchall_2
    move-exception v2

    goto :goto_1

    :cond_3
    move-object/from16 v5, v16

    move-object/from16 v4, v17

    goto :goto_0
.end method

.method private setRinger(Landroid/app/Notification$Builder;Lcom/google/android/gsf/TalkContract$AccountSettings$QueryMap;)Z
    .locals 5
    .param p1    # Landroid/app/Notification$Builder;
    .param p2    # Lcom/google/android/gsf/TalkContract$AccountSettings$QueryMap;

    invoke-virtual {p2}, Lcom/google/android/gsf/TalkContract$AccountSettings$QueryMap;->getTextRingtoneURI()Ljava/lang/String;

    move-result-object v1

    const-string v3, "always"

    invoke-virtual {p2}, Lcom/google/android/gsf/TalkContract$AccountSettings$QueryMap;->getTextVibrateWhen()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-eqz v3, :cond_2

    const/4 v0, 0x0

    :goto_0
    invoke-virtual {p1, v0}, Landroid/app/Notification$Builder;->setSound(Landroid/net/Uri;)Landroid/app/Notification$Builder;

    if-eqz v0, :cond_0

    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v3

    iput-wide v3, p0, Lcom/google/android/gsf/gtalkservice/service/StatusBarNotifier;->mLastSoundPlayedMs:J

    :cond_0
    sget-boolean v3, Lcom/google/android/gsf/gtalkservice/LogTag;->sVerbose:Z

    if-eqz v3, :cond_1

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "setRinger: notificationSoundUri = "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " vibrate = "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-direct {p0, v3}, Lcom/google/android/gsf/gtalkservice/service/StatusBarNotifier;->log(Ljava/lang/String;)V

    :cond_1
    return v2

    :cond_2
    invoke-static {v1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    goto :goto_0
.end method

.method private shouldSuppressSoundNotification()Z
    .locals 4

    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v0

    iget-wide v2, p0, Lcom/google/android/gsf/gtalkservice/service/StatusBarNotifier;->mLastSoundPlayedMs:J

    sub-long/2addr v0, v2

    const-wide/16 v2, 0xbb8

    cmp-long v0, v0, v2

    if-gez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public dismissAllNotifications()V
    .locals 0

    invoke-direct {p0}, Lcom/google/android/gsf/gtalkservice/service/StatusBarNotifier;->internalRemoveAllNotifications()V

    return-void
.end method

.method public dismissNotificationFor(Ljava/lang/String;JLcom/google/android/gsf/TalkContract$AccountSettings$QueryMap;)V
    .locals 0
    .param p1    # Ljava/lang/String;
    .param p2    # J
    .param p4    # Lcom/google/android/gsf/TalkContract$AccountSettings$QueryMap;

    invoke-direct {p0, p1, p2, p3, p4}, Lcom/google/android/gsf/gtalkservice/service/StatusBarNotifier;->cancelNotify(Ljava/lang/String;JLcom/google/android/gsf/TalkContract$AccountSettings$QueryMap;)V

    return-void
.end method

.method public dismissNotificationsForAccount(JLcom/google/android/gsf/TalkContract$AccountSettings$QueryMap;)V
    .locals 2
    .param p1    # J
    .param p3    # Lcom/google/android/gsf/TalkContract$AccountSettings$QueryMap;

    const-string v0, "GTalkService"

    const/4 v1, 0x3

    invoke-static {v0, v1}, Lcom/google/android/gsf/gtalkservice/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "dismissNotificationsForAccount: account="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1, p2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/google/android/gsf/gtalkservice/service/StatusBarNotifier;->log(Ljava/lang/String;)V

    :cond_0
    iget-object v0, p0, Lcom/google/android/gsf/gtalkservice/service/StatusBarNotifier;->mNotificationMgr:Landroid/app/NotificationManager;

    long-to-int v1, p1

    invoke-virtual {v0, v1}, Landroid/app/NotificationManager;->cancel(I)V

    iget-object v0, p0, Lcom/google/android/gsf/gtalkservice/service/StatusBarNotifier;->mNotificationInfo:Lcom/google/android/gsf/gtalkservice/service/StatusBarNotifier$NotificationInfo;

    if-nez v0, :cond_1

    :goto_0
    return-void

    :cond_1
    iget-object v1, p0, Lcom/google/android/gsf/gtalkservice/service/StatusBarNotifier;->mNotificationLock:Ljava/lang/Object;

    monitor-enter v1

    :try_start_0
    iget-object v0, p0, Lcom/google/android/gsf/gtalkservice/service/StatusBarNotifier;->mNotificationInfo:Lcom/google/android/gsf/gtalkservice/service/StatusBarNotifier$NotificationInfo;

    invoke-virtual {v0, p1, p2}, Lcom/google/android/gsf/gtalkservice/service/StatusBarNotifier$NotificationInfo;->removeForAccount(J)Z

    monitor-exit v1

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public getAvatarForContact(Ljava/lang/String;J)Landroid/graphics/Bitmap;
    .locals 12
    .param p1    # Ljava/lang/String;
    .param p2    # J

    const/4 v5, 0x0

    const/4 v1, 0x1

    const/4 v0, 0x0

    if-nez p1, :cond_0

    const-string v0, "GTalkService"

    const-string v1, "getAvatarForContact: null contact!"

    invoke-static {v0, v1}, Lcom/google/android/gsf/gtalkservice/Log;->e(Ljava/lang/String;Ljava/lang/String;)V

    :goto_0
    return-object v5

    :cond_0
    new-array v4, v1, [Ljava/lang/String;

    aput-object p1, v4, v0

    sget-object v0, Lcom/google/android/gsf/TalkContract$Avatars;->CONTENT_URI_AVATARS_BY:Landroid/net/Uri;

    invoke-virtual {v0}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v7

    invoke-static {v7, p2, p3}, Landroid/content/ContentUris;->appendId(Landroid/net/Uri$Builder;J)Landroid/net/Uri$Builder;

    iget-object v0, p0, Lcom/google/android/gsf/gtalkservice/service/StatusBarNotifier;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    invoke-virtual {v7}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v1

    sget-object v2, Lcom/google/android/gsf/gtalkservice/service/StatusBarNotifier;->AVATAR_PROJECTION:[Ljava/lang/String;

    const-string v3, "contact=?"

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v8

    const/4 v6, 0x0

    if-eqz v8, :cond_3

    :try_start_0
    invoke-interface {v8}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    if-eqz v0, :cond_1

    const/4 v0, 0x0

    invoke-direct {p0, v8, v0}, Lcom/google/android/gsf/gtalkservice/service/StatusBarNotifier;->getAvatarFromCursor(Landroid/database/Cursor;I)Landroid/graphics/Bitmap;

    move-result-object v6

    if-eqz v6, :cond_1

    iget-object v0, p0, Lcom/google/android/gsf/gtalkservice/service/StatusBarNotifier;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v11

    const v0, 0x1050006

    invoke-virtual {v11, v0}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v9

    const v0, 0x1050005

    invoke-virtual {v11, v0}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v10

    const/4 v0, 0x1

    invoke-static {v6, v10, v9, v0}, Landroid/graphics/Bitmap;->createScaledBitmap(Landroid/graphics/Bitmap;IIZ)Landroid/graphics/Bitmap;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v6

    :cond_1
    invoke-interface {v8}, Landroid/database/Cursor;->close()V

    :goto_1
    if-nez v6, :cond_2

    invoke-direct {p0}, Lcom/google/android/gsf/gtalkservice/service/StatusBarNotifier;->getGenericAvatar()Landroid/graphics/Bitmap;

    move-result-object v6

    :cond_2
    move-object v5, v6

    goto :goto_0

    :catchall_0
    move-exception v0

    invoke-interface {v8}, Landroid/database/Cursor;->close()V

    throw v0

    :cond_3
    const-string v0, "getAvatarForContact"

    invoke-direct {p0, v0}, Lcom/google/android/gsf/gtalkservice/service/StatusBarNotifier;->logEmptyCursor(Ljava/lang/String;)V

    goto :goto_1
.end method

.method public notifyAuthError(Ljava/lang/String;JLcom/google/android/gsf/TalkContract$AccountSettings$QueryMap;)V
    .locals 5

    const/4 v1, 0x0

    new-instance v0, Landroid/app/Notification$Builder;

    iget-object v2, p0, Lcom/google/android/gsf/gtalkservice/service/StatusBarNotifier;->mContext:Landroid/content/Context;

    invoke-direct {v0, v2}, Landroid/app/Notification$Builder;-><init>(Landroid/content/Context;)V

    const v2, 0x108008a

    invoke-virtual {v0, v2}, Landroid/app/Notification$Builder;->setSmallIcon(I)Landroid/app/Notification$Builder;

    move-result-object v0

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    invoke-virtual {v0, v2, v3}, Landroid/app/Notification$Builder;->setWhen(J)Landroid/app/Notification$Builder;

    move-result-object v0

    iget-object v2, p0, Lcom/google/android/gsf/gtalkservice/service/StatusBarNotifier;->mContext:Landroid/content/Context;

    const v3, 0x7f06005c

    invoke-virtual {v2, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/app/Notification$Builder;->setTicker(Ljava/lang/CharSequence;)Landroid/app/Notification$Builder;

    move-result-object v0

    const/4 v2, 0x1

    invoke-virtual {v0, v2}, Landroid/app/Notification$Builder;->setAutoCancel(Z)Landroid/app/Notification$Builder;

    move-result-object v2

    invoke-direct {p0}, Lcom/google/android/gsf/gtalkservice/service/StatusBarNotifier;->shouldSuppressSoundNotification()Z

    move-result v0

    if-nez v0, :cond_1

    invoke-direct {p0, v2, p4}, Lcom/google/android/gsf/gtalkservice/service/StatusBarNotifier;->setRinger(Landroid/app/Notification$Builder;Lcom/google/android/gsf/TalkContract$AccountSettings$QueryMap;)Z

    move-result v0

    :goto_0
    if-eqz v0, :cond_0

    const/4 v0, 0x2

    invoke-virtual {v2, v0}, Landroid/app/Notification$Builder;->setDefaults(I)Landroid/app/Notification$Builder;

    :cond_0
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    iget-object v3, p0, Lcom/google/android/gsf/gtalkservice/service/StatusBarNotifier;->mContext:Landroid/content/Context;

    const-class v4, Lcom/google/android/gsf/gtalkservice/ConnectionAuthErrorDialog;

    invoke-virtual {v0, v3, v4}, Landroid/content/Intent;->setClass(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    const-string v3, "accountId"

    invoke-virtual {v0, v3, p2, p3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;J)Landroid/content/Intent;

    const-string v3, "username"

    invoke-virtual {v0, v3, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    iget-object v3, p0, Lcom/google/android/gsf/gtalkservice/service/StatusBarNotifier;->mContext:Landroid/content/Context;

    const v4, 0x7f06005d

    invoke-virtual {v3, v4}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/app/Notification$Builder;->setContentTitle(Ljava/lang/CharSequence;)Landroid/app/Notification$Builder;

    move-result-object v3

    invoke-virtual {v3, p1}, Landroid/app/Notification$Builder;->setContentText(Ljava/lang/CharSequence;)Landroid/app/Notification$Builder;

    move-result-object v3

    iget-object v4, p0, Lcom/google/android/gsf/gtalkservice/service/StatusBarNotifier;->mContext:Landroid/content/Context;

    invoke-static {v4, v1, v0, v1}, Landroid/app/PendingIntent;->getActivity(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v0

    invoke-virtual {v3, v0}, Landroid/app/Notification$Builder;->setContentIntent(Landroid/app/PendingIntent;)Landroid/app/Notification$Builder;

    iget-object v0, p0, Lcom/google/android/gsf/gtalkservice/service/StatusBarNotifier;->mNotificationMgr:Landroid/app/NotificationManager;

    long-to-int v1, p2

    invoke-virtual {v2}, Landroid/app/Notification$Builder;->getNotification()Landroid/app/Notification;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/app/NotificationManager;->notify(ILandroid/app/Notification;)V

    return-void

    :cond_1
    move v0, v1

    goto :goto_0
.end method

.method public notifyChat(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;JLjava/lang/String;JZLcom/google/android/gsf/TalkContract$AccountSettings$QueryMap;Z)V
    .locals 18
    .param p1    # Ljava/lang/String;
    .param p2    # Ljava/lang/String;
    .param p3    # Ljava/lang/String;
    .param p4    # J
    .param p6    # Ljava/lang/String;
    .param p7    # J
    .param p9    # Z
    .param p10    # Lcom/google/android/gsf/TalkContract$AccountSettings$QueryMap;
    .param p11    # Z

    const-string v2, "off"

    invoke-virtual/range {p10 .. p10}, Lcom/google/android/gsf/TalkContract$AccountSettings$QueryMap;->getTextNotification()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    const-string v2, "GTalkService"

    const/4 v3, 0x3

    invoke-static {v2, v3}, Lcom/google/android/gsf/gtalkservice/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v2

    if-eqz v2, :cond_0

    const-string v2, "notifyChat: notification not enabled"

    move-object/from16 v0, p0

    invoke-direct {v0, v2}, Lcom/google/android/gsf/gtalkservice/service/StatusBarNotifier;->log(Ljava/lang/String;)V

    :cond_0
    :goto_0
    return-void

    :cond_1
    if-eqz p9, :cond_2

    const-string v2, "GTalkService"

    const/4 v3, 0x3

    invoke-static {v2, v3}, Lcom/google/android/gsf/gtalkservice/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v2

    if-eqz v2, :cond_0

    const-string v2, "notifyChat: lightWeightNotify"

    move-object/from16 v0, p0

    invoke-direct {v0, v2}, Lcom/google/android/gsf/gtalkservice/service/StatusBarNotifier;->log(Ljava/lang/String;)V

    goto :goto_0

    :cond_2
    invoke-static/range {p1 .. p1}, Lorg/jivesoftware/smack/util/StringUtils;->parseBareAddress(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    const-wide/16 v2, 0x0

    cmp-long v2, p7, v2

    if-eqz v2, :cond_4

    invoke-static/range {p7 .. p8}, Lcom/google/android/gsf/TalkContract$Messages;->getContentUriByThreadId(J)Landroid/net/Uri;

    move-result-object v17

    :goto_1
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/gsf/gtalkservice/service/StatusBarNotifier;->mContext:Landroid/content/Context;

    invoke-static {v2}, Landroid/app/TaskStackBuilder;->create(Landroid/content/Context;)Landroid/app/TaskStackBuilder;

    move-result-object v10

    new-instance v15, Landroid/content/Intent;

    const-string v2, "android.intent.action.VIEW"

    move-object/from16 v0, v17

    invoke-direct {v15, v2, v0}, Landroid/content/Intent;-><init>(Ljava/lang/String;Landroid/net/Uri;)V

    const/high16 v2, 0x34000000

    invoke-virtual {v15, v2}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    const-string v2, "from"

    move-object/from16 v0, p1

    invoke-virtual {v15, v2, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const-string v2, "accountId"

    move-wide/from16 v0, p4

    invoke-virtual {v15, v2, v0, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;J)Landroid/content/Intent;

    const-string v2, "from_notify"

    const/4 v3, 0x1

    invoke-virtual {v15, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    const-string v2, "username"

    move-object/from16 v0, p6

    invoke-virtual {v15, v2, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const-wide/16 v2, 0x0

    cmp-long v2, p7, v2

    if-eqz v2, :cond_3

    const-string v2, "is_muc"

    const/4 v3, 0x1

    invoke-virtual {v15, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    :cond_3
    invoke-virtual {v10, v15}, Landroid/app/TaskStackBuilder;->addNextIntent(Landroid/content/Intent;)Landroid/app/TaskStackBuilder;

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/gsf/gtalkservice/service/StatusBarNotifier;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v16

    if-eqz p11, :cond_5

    const v2, 0x7f060047

    const/4 v3, 0x2

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    aput-object p2, v3, v4

    const/4 v4, 0x1

    aput-object p6, v3, v4

    move-object/from16 v0, v16

    invoke-virtual {v0, v2, v3}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v7

    :goto_2
    const v2, 0x7f060046

    const/4 v3, 0x2

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    aput-object p2, v3, v4

    const/4 v4, 0x1

    aput-object p3, v3, v4

    move-object/from16 v0, v16

    invoke-virtual {v0, v2, v3}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v8

    const/4 v13, 0x1

    const/4 v14, 0x1

    move-object/from16 v2, p0

    move-wide/from16 v3, p4

    move-object/from16 v6, p2

    move-object/from16 v9, p3

    move/from16 v11, p9

    move-object/from16 v12, p10

    invoke-direct/range {v2 .. v14}, Lcom/google/android/gsf/gtalkservice/service/StatusBarNotifier;->notify(JLjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Landroid/app/TaskStackBuilder;ZLcom/google/android/gsf/TalkContract$AccountSettings$QueryMap;ZZ)V

    goto/16 :goto_0

    :cond_4
    invoke-static/range {p4 .. p5}, Lcom/google/android/gsf/TalkContract$Messages;->getContentUriByAccount(J)Landroid/net/Uri;

    move-result-object v17

    goto/16 :goto_1

    :cond_5
    move-object/from16 v7, p2

    goto :goto_2
.end method

.method public notifyGroupChatInvitation(Lcom/google/android/gtalkservice/GroupChatInvitation;JLcom/google/android/gsf/TalkContract$AccountSettings$QueryMap;)V
    .locals 18
    .param p1    # Lcom/google/android/gtalkservice/GroupChatInvitation;
    .param p2    # J
    .param p4    # Lcom/google/android/gsf/TalkContract$AccountSettings$QueryMap;

    invoke-virtual/range {p1 .. p1}, Lcom/google/android/gtalkservice/GroupChatInvitation;->getGroupContactId()J

    move-result-wide v2

    invoke-static {v2, v3}, Lcom/google/android/gsf/TalkContract$Messages;->getContentUriByThreadId(J)Landroid/net/Uri;

    move-result-object v17

    new-instance v15, Landroid/content/Intent;

    const-string v2, "android.intent.action.VIEW"

    move-object/from16 v0, v17

    invoke-direct {v15, v2, v0}, Landroid/content/Intent;-><init>(Ljava/lang/String;Landroid/net/Uri;)V

    invoke-virtual/range {p1 .. p1}, Lcom/google/android/gtalkservice/GroupChatInvitation;->getRoomAddress()Ljava/lang/String;

    move-result-object v16

    const-string v2, "from"

    move-object/from16 v0, v16

    invoke-virtual {v15, v2, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const-string v2, "accountId"

    move-wide/from16 v0, p2

    invoke-virtual {v15, v2, v0, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;J)Landroid/content/Intent;

    const-string v2, "is_muc"

    const/4 v3, 0x1

    invoke-virtual {v15, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    const-string v2, "muc_inviter"

    invoke-virtual/range {p1 .. p1}, Lcom/google/android/gtalkservice/GroupChatInvitation;->getInviter()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v15, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const-string v2, "muc_password"

    invoke-virtual/range {p1 .. p1}, Lcom/google/android/gtalkservice/GroupChatInvitation;->getPassword()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v15, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const-string v2, "from_notify"

    const/4 v3, 0x1

    invoke-virtual {v15, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    const-string v2, "state"

    const/4 v3, 0x1

    invoke-virtual {v15, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/gsf/gtalkservice/service/StatusBarNotifier;->mContext:Landroid/content/Context;

    invoke-static {v2}, Landroid/app/TaskStackBuilder;->create(Landroid/content/Context;)Landroid/app/TaskStackBuilder;

    move-result-object v10

    invoke-virtual {v10, v15}, Landroid/app/TaskStackBuilder;->addNextIntent(Landroid/content/Intent;)Landroid/app/TaskStackBuilder;

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/gsf/gtalkservice/service/StatusBarNotifier;->mContext:Landroid/content/Context;

    const v3, 0x7f060045

    invoke-virtual {v2, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v7

    invoke-virtual/range {p1 .. p1}, Lcom/google/android/gtalkservice/GroupChatInvitation;->getInviter()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lorg/jivesoftware/smack/util/StringUtils;->parseBareAddress(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/gsf/gtalkservice/service/StatusBarNotifier;->mContext:Landroid/content/Context;

    const v3, 0x7f060049

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v6, 0x0

    aput-object v5, v4, v6

    invoke-virtual {v2, v3, v4}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v8

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/gsf/gtalkservice/service/StatusBarNotifier;->mContext:Landroid/content/Context;

    const v3, 0x7f06004a

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v6, 0x0

    aput-object v5, v4, v6

    invoke-virtual {v2, v3, v4}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v9

    const/4 v11, 0x0

    const/4 v13, 0x0

    const/4 v14, 0x0

    move-object/from16 v2, p0

    move-wide/from16 v3, p2

    move-object v6, v5

    move-object/from16 v12, p4

    invoke-direct/range {v2 .. v14}, Lcom/google/android/gsf/gtalkservice/service/StatusBarNotifier;->notify(JLjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Landroid/app/TaskStackBuilder;ZLcom/google/android/gsf/TalkContract$AccountSettings$QueryMap;ZZ)V

    return-void
.end method

.method public notifySubscriptionRequest(Ljava/lang/String;Ljava/lang/String;JLcom/google/android/gsf/TalkContract$AccountSettings$QueryMap;)V
    .locals 16
    .param p1    # Ljava/lang/String;
    .param p2    # Ljava/lang/String;
    .param p3    # J
    .param p5    # Lcom/google/android/gsf/TalkContract$AccountSettings$QueryMap;

    invoke-virtual/range {p5 .. p5}, Lcom/google/android/gsf/TalkContract$AccountSettings$QueryMap;->getNotifyFriendInvitation()Z

    move-result v2

    if-nez v2, :cond_0

    const-string v2, "notifySubscriptionRequest: setting says no notify for invite"

    move-object/from16 v0, p0

    invoke-direct {v0, v2}, Lcom/google/android/gsf/gtalkservice/service/StatusBarNotifier;->log(Ljava/lang/String;)V

    :goto_0
    return-void

    :cond_0
    const-string v2, "GTalkService"

    const/4 v3, 0x3

    invoke-static {v2, v3}, Lcom/google/android/gsf/gtalkservice/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v2

    if-eqz v2, :cond_1

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "notifyInvite: contact="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    move-object/from16 v0, p1

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ", msg="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    move-object/from16 v0, p2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, p0

    invoke-direct {v0, v2}, Lcom/google/android/gsf/gtalkservice/service/StatusBarNotifier;->log(Ljava/lang/String;)V

    :cond_1
    invoke-static/range {p1 .. p1}, Lorg/jivesoftware/smack/util/StringUtils;->parseBareAddress(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    new-instance v15, Landroid/content/Intent;

    const-string v2, "android.intent.action.GTALK_MANAGE_SUBSCRIPTION"

    const/4 v3, 0x0

    invoke-direct {v15, v2, v3}, Landroid/content/Intent;-><init>(Ljava/lang/String;Landroid/net/Uri;)V

    const-string v2, "vnd.android.cursor.item/gtalk-contacts"

    invoke-virtual {v15, v2}, Landroid/content/Intent;->setType(Ljava/lang/String;)Landroid/content/Intent;

    const-string v2, "from"

    move-object/from16 v0, p1

    invoke-virtual {v15, v2, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const-string v2, "accountId"

    move-wide/from16 v0, p3

    invoke-virtual {v15, v2, v0, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;J)Landroid/content/Intent;

    const-string v2, "from_notify"

    const/4 v3, 0x1

    invoke-virtual {v15, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    const-string v2, "username"

    invoke-virtual {v15, v2, v5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/gsf/gtalkservice/service/StatusBarNotifier;->mContext:Landroid/content/Context;

    invoke-static {v2}, Landroid/app/TaskStackBuilder;->create(Landroid/content/Context;)Landroid/app/TaskStackBuilder;

    move-result-object v10

    invoke-virtual {v10, v15}, Landroid/app/TaskStackBuilder;->addNextIntent(Landroid/content/Intent;)Landroid/app/TaskStackBuilder;

    move-object/from16 v7, p1

    const/4 v11, 0x0

    const/4 v13, 0x0

    const/4 v14, 0x0

    move-object/from16 v2, p0

    move-wide/from16 v3, p3

    move-object v6, v5

    move-object/from16 v8, p2

    move-object/from16 v9, p2

    move-object/from16 v12, p5

    invoke-direct/range {v2 .. v14}, Lcom/google/android/gsf/gtalkservice/service/StatusBarNotifier;->notify(JLjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Landroid/app/TaskStackBuilder;ZLcom/google/android/gsf/TalkContract$AccountSettings$QueryMap;ZZ)V

    goto :goto_0
.end method

.method public onServiceDestroyed()V
    .locals 0

    return-void
.end method

.method public removeChatNotificationFor(Ljava/lang/String;JLcom/google/android/gsf/TalkContract$AccountSettings$QueryMap;)V
    .locals 0
    .param p1    # Ljava/lang/String;
    .param p2    # J
    .param p4    # Lcom/google/android/gsf/TalkContract$AccountSettings$QueryMap;

    invoke-direct {p0, p1, p2, p3, p4}, Lcom/google/android/gsf/gtalkservice/service/StatusBarNotifier;->cancelNotify(Ljava/lang/String;JLcom/google/android/gsf/TalkContract$AccountSettings$QueryMap;)V

    return-void
.end method

.method public removeSubscriptionNotificationFor(Ljava/lang/String;JLcom/google/android/gsf/TalkContract$AccountSettings$QueryMap;)V
    .locals 1
    .param p1    # Ljava/lang/String;
    .param p2    # J
    .param p4    # Lcom/google/android/gsf/TalkContract$AccountSettings$QueryMap;

    invoke-static {p1}, Lorg/jivesoftware/smack/util/StringUtils;->parseBareAddress(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0, p2, p3, p4}, Lcom/google/android/gsf/gtalkservice/service/StatusBarNotifier;->cancelNotify(Ljava/lang/String;JLcom/google/android/gsf/TalkContract$AccountSettings$QueryMap;)V

    return-void
.end method
