.class public Lcom/google/android/gsf/gtalkservice/extensions/OtrQueryProvider;
.super Ljava/lang/Object;
.source "OtrQueryProvider.java"

# interfaces
.implements Lorg/jivesoftware/smack/provider/IQProvider;


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public getProtoBufType()Lcom/google/common/io/protocol/ProtoBufType;
    .locals 2

    sget-boolean v0, Lcom/google/android/gsf/gtalkservice/extensions/OtrQuery;->USE_XML_EXTENSION:Z

    if-eqz v0, :cond_0

    new-instance v0, Ljava/lang/UnsupportedOperationException;

    const-string v1, "Should not get called."

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    sget-object v0, Lcom/google/android/gsf/gtalkservice/proto/GtalkExtensionsMessageTypes;->OTR_QUERY:Lcom/google/common/io/protocol/ProtoBufType;

    return-object v0
.end method

.method public getTag()I
    .locals 1

    sget-boolean v0, Lcom/google/android/gsf/gtalkservice/extensions/OtrQuery;->USE_XML_EXTENSION:Z

    if-eqz v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_0
    const/16 v0, 0x9

    goto :goto_0
.end method

.method public parseIQ(Lcom/google/common/io/protocol/ProtoBuf;)Lorg/jivesoftware/smack/packet/IQ;
    .locals 13
    .param p1    # Lcom/google/common/io/protocol/ProtoBuf;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    const/4 v12, 0x4

    const/4 v11, 0x2

    const/4 v10, 0x3

    const/4 v9, 0x1

    sget-boolean v8, Lcom/google/android/gsf/gtalkservice/extensions/OtrQuery;->USE_XML_EXTENSION:Z

    if-eqz v8, :cond_0

    new-instance v8, Ljava/lang/UnsupportedOperationException;

    const-string v9, "should not get called."

    invoke-direct {v8, v9}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v8

    :cond_0
    new-instance v6, Lcom/google/android/gsf/gtalkservice/extensions/OtrQuery;

    invoke-direct {v6}, Lcom/google/android/gsf/gtalkservice/extensions/OtrQuery;-><init>()V

    invoke-virtual {p1, v10}, Lcom/google/common/io/protocol/ProtoBuf;->has(I)Z

    move-result v8

    if-eqz v8, :cond_3

    invoke-virtual {p1, v10}, Lcom/google/common/io/protocol/ProtoBuf;->getString(I)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v6, v8}, Lcom/google/android/gsf/gtalkservice/extensions/OtrQuery;->setEtag(Ljava/lang/String;)V

    :cond_1
    invoke-virtual {p1, v9}, Lcom/google/common/io/protocol/ProtoBuf;->has(I)Z

    move-result v8

    if-eqz v8, :cond_6

    invoke-virtual {p1, v9}, Lcom/google/common/io/protocol/ProtoBuf;->getBool(I)Z

    move-result v8

    if-eqz v8, :cond_5

    sget-object v8, Lcom/google/android/gsf/gtalkservice/extensions/OtrQuery$DefaultValue;->ENABLED:Lcom/google/android/gsf/gtalkservice/extensions/OtrQuery$DefaultValue;

    :goto_0
    invoke-virtual {v6, v8}, Lcom/google/android/gsf/gtalkservice/extensions/OtrQuery;->setDefaultValue(Lcom/google/android/gsf/gtalkservice/extensions/OtrQuery$DefaultValue;)V

    :goto_1
    invoke-virtual {p1, v11}, Lcom/google/common/io/protocol/ProtoBuf;->getCount(I)I

    move-result v5

    const/4 v1, 0x0

    :goto_2
    if-ge v1, v5, :cond_4

    invoke-virtual {p1, v11, v1}, Lcom/google/common/io/protocol/ProtoBuf;->getProtoBuf(II)Lcom/google/common/io/protocol/ProtoBuf;

    move-result-object v3

    invoke-virtual {v3, v9}, Lcom/google/common/io/protocol/ProtoBuf;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v11}, Lcom/google/common/io/protocol/ProtoBuf;->getBool(I)Z

    move-result v7

    const/4 v0, 0x0

    invoke-virtual {v3, v10}, Lcom/google/common/io/protocol/ProtoBuf;->has(I)Z

    move-result v8

    if-eqz v8, :cond_2

    invoke-virtual {v3, v10}, Lcom/google/common/io/protocol/ProtoBuf;->getBool(I)Z

    move-result v0

    :cond_2
    new-instance v2, Lcom/google/android/gsf/gtalkservice/extensions/OtrQuery$Item;

    invoke-direct {v2, v4, v7, v0}, Lcom/google/android/gsf/gtalkservice/extensions/OtrQuery$Item;-><init>(Ljava/lang/String;ZZ)V

    invoke-virtual {v6, v2}, Lcom/google/android/gsf/gtalkservice/extensions/OtrQuery;->addItem(Lcom/google/android/gsf/gtalkservice/extensions/OtrQuery$Item;)V

    add-int/lit8 v1, v1, 0x1

    goto :goto_2

    :cond_3
    invoke-virtual {p1, v12}, Lcom/google/common/io/protocol/ProtoBuf;->has(I)Z

    move-result v8

    if-eqz v8, :cond_1

    invoke-virtual {p1, v12}, Lcom/google/common/io/protocol/ProtoBuf;->getBool(I)Z

    move-result v8

    if-eqz v8, :cond_1

    invoke-static {v9}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v8

    invoke-virtual {v6, v8}, Lcom/google/android/gsf/gtalkservice/extensions/OtrQuery;->setNotModified(Ljava/lang/Boolean;)V

    :cond_4
    return-object v6

    :cond_5
    sget-object v8, Lcom/google/android/gsf/gtalkservice/extensions/OtrQuery$DefaultValue;->DISABLED:Lcom/google/android/gsf/gtalkservice/extensions/OtrQuery$DefaultValue;

    goto :goto_0

    :cond_6
    sget-object v8, Lcom/google/android/gsf/gtalkservice/extensions/OtrQuery$DefaultValue;->NONE:Lcom/google/android/gsf/gtalkservice/extensions/OtrQuery$DefaultValue;

    invoke-virtual {v6, v8}, Lcom/google/android/gsf/gtalkservice/extensions/OtrQuery;->setDefaultValue(Lcom/google/android/gsf/gtalkservice/extensions/OtrQuery$DefaultValue;)V

    goto :goto_1
.end method

.method public parseIQ(Lorg/xmlpull/v1/XmlPullParser;)Lorg/jivesoftware/smack/packet/IQ;
    .locals 11
    .param p1    # Lorg/xmlpull/v1/XmlPullParser;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    new-instance v6, Lcom/google/android/gsf/gtalkservice/extensions/OtrQuery;

    invoke-direct {v6}, Lcom/google/android/gsf/gtalkservice/extensions/OtrQuery;-><init>()V

    const/4 v4, 0x0

    const/4 v1, 0x0

    :cond_0
    :goto_0
    if-nez v1, :cond_5

    invoke-interface {p1}, Lorg/xmlpull/v1/XmlPullParser;->next()I

    move-result v3

    const/4 v9, 0x2

    if-ne v3, v9, :cond_3

    invoke-interface {p1}, Lorg/xmlpull/v1/XmlPullParser;->getName()Ljava/lang/String;

    move-result-object v9

    const-string v10, "item"

    invoke-virtual {v9, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_0

    const-string v9, ""

    const-string v10, "jid"

    invoke-interface {p1, v9, v10}, Lorg/xmlpull/v1/XmlPullParser;->getAttributeValue(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    const-string v9, ""

    const-string v10, "value"

    invoke-interface {p1, v9, v10}, Lorg/xmlpull/v1/XmlPullParser;->getAttributeValue(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    const-string v9, ""

    const-string v10, "source"

    invoke-interface {p1, v9, v10}, Lorg/xmlpull/v1/XmlPullParser;->getAttributeValue(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    const/4 v2, 0x0

    const/4 v0, 0x0

    const-string v9, "enabled"

    invoke-virtual {v9, v8}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v9

    if-eqz v9, :cond_1

    const/4 v2, 0x1

    :cond_1
    invoke-virtual {v5, v7}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v9

    if-eqz v9, :cond_2

    const/4 v0, 0x1

    :cond_2
    new-instance v4, Lcom/google/android/gsf/gtalkservice/extensions/OtrQuery$Item;

    invoke-direct {v4, v5, v2, v0}, Lcom/google/android/gsf/gtalkservice/extensions/OtrQuery$Item;-><init>(Ljava/lang/String;ZZ)V

    goto :goto_0

    :cond_3
    const/4 v9, 0x3

    if-ne v3, v9, :cond_0

    invoke-interface {p1}, Lorg/xmlpull/v1/XmlPullParser;->getName()Ljava/lang/String;

    move-result-object v9

    const-string v10, "item"

    invoke-virtual {v9, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_4

    invoke-virtual {v6, v4}, Lcom/google/android/gsf/gtalkservice/extensions/OtrQuery;->addItem(Lcom/google/android/gsf/gtalkservice/extensions/OtrQuery$Item;)V

    :cond_4
    invoke-interface {p1}, Lorg/xmlpull/v1/XmlPullParser;->getName()Ljava/lang/String;

    move-result-object v9

    const-string v10, "query"

    invoke-virtual {v9, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_0

    const/4 v1, 0x1

    goto :goto_0

    :cond_5
    return-object v6
.end method
