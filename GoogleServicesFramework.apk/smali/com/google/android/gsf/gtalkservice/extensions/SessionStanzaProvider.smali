.class public Lcom/google/android/gsf/gtalkservice/extensions/SessionStanzaProvider;
.super Lorg/jivesoftware/smack/provider/RawXmlIQProvider;
.source "SessionStanzaProvider.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Lorg/jivesoftware/smack/provider/RawXmlIQProvider;-><init>()V

    return-void
.end method


# virtual methods
.method public parseRawXml(Ljava/lang/String;Ljava/lang/String;[B)Lorg/jivesoftware/smack/packet/IQ;
    .locals 2
    .param p1    # Ljava/lang/String;
    .param p2    # Ljava/lang/String;
    .param p3    # [B

    new-instance v0, Lcom/google/android/gsf/gtalkservice/extensions/SessionStanza;

    invoke-direct {v0}, Lcom/google/android/gsf/gtalkservice/extensions/SessionStanza;-><init>()V

    new-instance v1, Ljava/lang/String;

    invoke-direct {v1, p3}, Ljava/lang/String;-><init>([B)V

    invoke-virtual {v0, v1}, Lcom/google/android/gsf/gtalkservice/extensions/SessionStanza;->setSessionRawXml(Ljava/lang/String;)V

    return-object v0
.end method
