.class public Lcom/google/android/gsf/checkin/CheckinTask;
.super Landroid/os/AsyncTask;
.source "CheckinTask.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/gsf/checkin/CheckinTask$Params;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/os/AsyncTask",
        "<",
        "Lcom/google/android/gsf/checkin/CheckinTask$Params;",
        "Ljava/lang/Void;",
        "Lcom/google/android/gsf/checkin/proto/Checkin$AndroidCheckinResponse;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    return-void
.end method

.method private static combineResponses(Lcom/google/android/gsf/checkin/proto/Checkin$AndroidCheckinResponse;Lcom/google/android/gsf/checkin/proto/Checkin$AndroidCheckinResponse;)Lcom/google/android/gsf/checkin/proto/Checkin$AndroidCheckinResponse;
    .locals 9
    .param p0    # Lcom/google/android/gsf/checkin/proto/Checkin$AndroidCheckinResponse;
    .param p1    # Lcom/google/android/gsf/checkin/proto/Checkin$AndroidCheckinResponse;

    invoke-virtual {p0}, Lcom/google/android/gsf/checkin/proto/Checkin$AndroidCheckinResponse;->getIntentCount()I

    move-result v8

    invoke-virtual {p1}, Lcom/google/android/gsf/checkin/proto/Checkin$AndroidCheckinResponse;->getIntentCount()I

    move-result v5

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v8, :cond_2

    invoke-virtual {p0, v0}, Lcom/google/android/gsf/checkin/proto/Checkin$AndroidCheckinResponse;->getIntent(I)Lcom/google/android/gsf/checkin/proto/Logs$AndroidIntentProto;

    move-result-object v7

    invoke-virtual {v7}, Lcom/google/android/gsf/checkin/proto/Logs$AndroidIntentProto;->getAction()Ljava/lang/String;

    move-result-object v6

    const/4 v1, 0x0

    const/4 v2, 0x0

    :goto_1
    if-ge v2, v5, :cond_0

    if-nez v1, :cond_0

    invoke-virtual {p1, v2}, Lcom/google/android/gsf/checkin/proto/Checkin$AndroidCheckinResponse;->getIntent(I)Lcom/google/android/gsf/checkin/proto/Logs$AndroidIntentProto;

    move-result-object v4

    invoke-virtual {v4}, Lcom/google/android/gsf/checkin/proto/Logs$AndroidIntentProto;->getAction()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v6, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    :cond_0
    if-nez v1, :cond_1

    invoke-virtual {p1, v7}, Lcom/google/android/gsf/checkin/proto/Checkin$AndroidCheckinResponse;->addIntent(Lcom/google/android/gsf/checkin/proto/Logs$AndroidIntentProto;)Lcom/google/android/gsf/checkin/proto/Checkin$AndroidCheckinResponse;

    :cond_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_2
    return-object p1
.end method

.method private static makeRequest(Lcom/google/android/gsf/checkin/CheckinTask$Params;Lcom/google/android/gsf/checkin/proto/Checkin$AndroidCheckinRequest;J)J
    .locals 16
    .param p0    # Lcom/google/android/gsf/checkin/CheckinTask$Params;
    .param p1    # Lcom/google/android/gsf/checkin/proto/Checkin$AndroidCheckinRequest;
    .param p2    # J

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/google/android/gsf/checkin/CheckinTask$Params;->context:Landroid/content/Context;

    const-string v2, "connectivity"

    invoke-virtual {v1, v2}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Landroid/net/ConnectivityManager;

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/google/android/gsf/checkin/CheckinTask$Params;->context:Landroid/content/Context;

    const-string v2, "phone"

    invoke-virtual {v1, v2}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v14

    check-cast v14, Landroid/telephony/TelephonyManager;

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/google/android/gsf/checkin/CheckinTask$Params;->context:Landroid/content/Context;

    const-string v2, "wifi"

    invoke-virtual {v1, v2}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v15

    check-cast v15, Landroid/net/wifi/WifiManager;

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/google/android/gsf/checkin/CheckinTask$Params;->storage:Landroid/content/SharedPreferences;

    move-object/from16 v0, p1

    invoke-static {v1, v0}, Lcom/google/android/gsf/checkin/CheckinRequestBuilder;->addBuildProperties(Landroid/content/SharedPreferences;Lcom/google/android/gsf/checkin/proto/Checkin$AndroidCheckinRequest;)V

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/google/android/gsf/checkin/CheckinTask$Params;->context:Landroid/content/Context;

    move-object/from16 v0, p1

    invoke-static {v1, v0}, Lcom/google/android/gsf/checkin/CheckinRequestBuilder;->addPackageProperties(Landroid/content/Context;Lcom/google/android/gsf/checkin/proto/Checkin$AndroidCheckinRequest;)V

    move-object/from16 v0, p1

    invoke-static {v14, v15, v8, v0}, Lcom/google/android/gsf/checkin/CheckinRequestBuilder;->addNetworkProperties(Landroid/telephony/TelephonyManager;Landroid/net/wifi/WifiManager;Landroid/net/ConnectivityManager;Lcom/google/android/gsf/checkin/proto/Checkin$AndroidCheckinRequest;)V

    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v1

    move-object/from16 v0, p1

    invoke-static {v1, v0}, Lcom/google/android/gsf/checkin/CheckinRequestBuilder;->addLocaleProperty(Ljava/util/Locale;Lcom/google/android/gsf/checkin/proto/Checkin$AndroidCheckinRequest;)V

    const-wide/16 v12, 0x0

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/google/android/gsf/checkin/CheckinTask$Params;->storage:Landroid/content/SharedPreferences;

    if-eqz v1, :cond_0

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/google/android/gsf/checkin/CheckinTask$Params;->storage:Landroid/content/SharedPreferences;

    const-string v2, "CheckinTask_securityToken"

    const-wide/16 v3, 0x0

    invoke-interface {v1, v2, v3, v4}, Landroid/content/SharedPreferences;->getLong(Ljava/lang/String;J)J

    move-result-wide v12

    :cond_0
    const-wide/16 v1, 0x0

    cmp-long v1, v12, v1

    if-nez v1, :cond_1

    :try_start_0
    new-instance v10, Ljava/io/DataInputStream;

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/google/android/gsf/checkin/CheckinTask$Params;->context:Landroid/content/Context;

    const-string v2, "security_token"

    invoke-virtual {v1, v2}, Landroid/content/Context;->openFileInput(Ljava/lang/String;)Ljava/io/FileInputStream;

    move-result-object v1

    invoke-direct {v10, v1}, Ljava/io/DataInputStream;-><init>(Ljava/io/InputStream;)V

    invoke-virtual {v10}, Ljava/io/DataInputStream;->readLong()J

    move-result-wide v12

    invoke-virtual {v10}, Ljava/io/DataInputStream;->close()V
    :try_end_0
    .catch Ljava/io/FileNotFoundException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    :cond_1
    :goto_0
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/google/android/gsf/checkin/CheckinTask$Params;->context:Landroid/content/Context;

    move-object/from16 v0, p1

    invoke-static {v1, v12, v13, v0}, Lcom/google/android/gsf/checkin/CheckinRequestBuilder;->addIdProperties(Landroid/content/Context;JLcom/google/android/gsf/checkin/proto/Checkin$AndroidCheckinRequest;)V

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/google/android/gsf/checkin/CheckinTask$Params;->context:Landroid/content/Context;

    move-object/from16 v0, p1

    invoke-static {v1, v0}, Lcom/google/android/gsf/checkin/CheckinRequestBuilder;->addAccountInfo(Landroid/content/Context;Lcom/google/android/gsf/checkin/proto/Checkin$AndroidCheckinRequest;)V

    invoke-static {}, Ljava/util/TimeZone;->getDefault()Ljava/util/TimeZone;

    move-result-object v1

    move-object/from16 v0, p1

    invoke-static {v1, v0}, Lcom/google/android/gsf/checkin/CheckinRequestBuilder;->addTimeZone(Ljava/util/TimeZone;Lcom/google/android/gsf/checkin/proto/Checkin$AndroidCheckinRequest;)V

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/google/android/gsf/checkin/CheckinTask$Params;->context:Landroid/content/Context;

    move-object/from16 v0, p1

    invoke-static {v1, v0}, Lcom/google/android/gsf/checkin/CheckinRequestBuilder;->addDeviceConfiguration(Landroid/content/Context;Lcom/google/android/gsf/checkin/proto/Checkin$AndroidCheckinRequest;)V

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/google/android/gsf/checkin/CheckinTask$Params;->context:Landroid/content/Context;

    move-object/from16 v0, p1

    invoke-static {v1, v0}, Lcom/google/android/gsf/checkin/CheckinRequestBuilder;->addRequestedGroups(Landroid/content/Context;Lcom/google/android/gsf/checkin/proto/Checkin$AndroidCheckinRequest;)V

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/google/android/gsf/checkin/CheckinTask$Params;->dropbox:Landroid/os/DropBoxManager;

    if-eqz v1, :cond_2

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/google/android/gsf/checkin/CheckinTask$Params;->context:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v9

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/google/android/gsf/checkin/CheckinTask$Params;->dropbox:Landroid/os/DropBoxManager;

    move-object/from16 v0, p0

    iget v2, v0, Lcom/google/android/gsf/checkin/CheckinTask$Params;->maxRequestBytes:I

    move-object/from16 v0, p0

    iget v3, v0, Lcom/google/android/gsf/checkin/CheckinTask$Params;->maxEventBytes:I

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/String;

    const/4 v5, 0x0

    const-string v6, "checkin_dropbox_upload"

    aput-object v6, v4, v5

    invoke-static {v9, v4}, Lcom/google/android/gsf/Gservices;->getStringsByPrefix(Landroid/content/ContentResolver;[Ljava/lang/String;)Ljava/util/Map;

    move-result-object v4

    move-wide/from16 v5, p2

    move-object/from16 v7, p1

    invoke-static/range {v1 .. v7}, Lcom/google/android/gsf/checkin/CheckinRequestBuilder;->addEvents(Landroid/os/DropBoxManager;IILjava/util/Map;JLcom/google/android/gsf/checkin/proto/Checkin$AndroidCheckinRequest;)J

    move-result-wide p2

    :cond_2
    return-wide p2

    :catch_0
    move-exception v11

    const-string v1, "CheckinTask"

    const-string v2, "Error reading backup security token file"

    invoke-static {v1, v2, v11}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0

    :catch_1
    move-exception v1

    goto :goto_0
.end method

.method private static maybeSetTime(Lorg/apache/http/client/HttpClient;Lcom/google/android/gsf/checkin/CheckinTask$Params;)Z
    .locals 12
    .param p0    # Lorg/apache/http/client/HttpClient;
    .param p1    # Lcom/google/android/gsf/checkin/CheckinTask$Params;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    const/4 v7, 0x0

    iget-object v8, p1, Lcom/google/android/gsf/checkin/CheckinTask$Params;->serverUrl:Ljava/lang/String;

    const-string v9, "https:"

    invoke-virtual {v8, v9}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v8

    if-nez v8, :cond_0

    :goto_0
    return v7

    :cond_0
    new-instance v1, Lorg/apache/http/client/methods/HttpPost;

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "http:"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    iget-object v9, p1, Lcom/google/android/gsf/checkin/CheckinTask$Params;->serverUrl:Ljava/lang/String;

    const/4 v10, 0x6

    invoke-virtual {v9, v10}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-direct {v1, v8}, Lorg/apache/http/client/methods/HttpPost;-><init>(Ljava/lang/String;)V

    const-string v8, "Content-type"

    const-string v9, "application/x-protobuffer"

    invoke-virtual {v1, v8, v9}, Lorg/apache/http/client/methods/HttpPost;->setHeader(Ljava/lang/String;Ljava/lang/String;)V

    new-instance v8, Lorg/apache/http/entity/ByteArrayEntity;

    invoke-static {v7}, Lcom/google/android/gsf/checkin/CheckinRequestBuilder;->newRequest(I)Lcom/google/android/gsf/checkin/proto/Checkin$AndroidCheckinRequest;

    move-result-object v9

    invoke-virtual {v9}, Lcom/google/android/gsf/checkin/proto/Checkin$AndroidCheckinRequest;->toByteArray()[B

    move-result-object v9

    invoke-direct {v8, v9}, Lorg/apache/http/entity/ByteArrayEntity;-><init>([B)V

    invoke-virtual {v1, v8}, Lorg/apache/http/client/methods/HttpPost;->setEntity(Lorg/apache/http/HttpEntity;)V

    invoke-interface {p0, v1}, Lorg/apache/http/client/HttpClient;->execute(Lorg/apache/http/client/methods/HttpUriRequest;)Lorg/apache/http/HttpResponse;

    move-result-object v8

    invoke-static {p1, v8}, Lcom/google/android/gsf/checkin/CheckinTask;->parseResponse(Lcom/google/android/gsf/checkin/CheckinTask$Params;Lorg/apache/http/HttpResponse;)Lcom/google/android/gsf/checkin/proto/Checkin$AndroidCheckinResponse;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/android/gsf/checkin/proto/Checkin$AndroidCheckinResponse;->hasTimeMsec()Z

    move-result v8

    if-nez v8, :cond_1

    const-string v8, "CheckinTask"

    const-string v9, "No time of day in checkin server response"

    invoke-static {v8, v9}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    :cond_1
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v5

    invoke-virtual {v2}, Lcom/google/android/gsf/checkin/proto/Checkin$AndroidCheckinResponse;->getTimeMsec()J

    move-result-wide v3

    sub-long v8, v3, v5

    invoke-static {v8, v9}, Ljava/lang/Math;->abs(J)J

    move-result-wide v8

    iget-wide v10, p1, Lcom/google/android/gsf/checkin/CheckinTask$Params;->minTimeAdjustmentMillis:J

    cmp-long v8, v8, v10

    if-gez v8, :cond_2

    const-string v8, "CheckinTask"

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "Server time agrees: delta "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    sub-long v10, v3, v5

    invoke-static {v10, v11}, Ljava/lang/Math;->abs(J)J

    move-result-wide v10

    invoke-virtual {v9, v10, v11}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, " msec"

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v8, v9}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0

    :cond_2
    iget-wide v8, p1, Lcom/google/android/gsf/checkin/CheckinTask$Params;->minTimeSettingMillis:J

    cmp-long v8, v3, v8

    if-gez v8, :cond_3

    const-string v8, "CheckinTask"

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "Server time is curiously old: "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, v3, v4}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v8, v9}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0

    :cond_3
    const-string v7, "CheckinTask"

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "Setting time from "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v5, v6}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, " to "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v3, v4}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v7, p1, Lcom/google/android/gsf/checkin/CheckinTask$Params;->context:Landroid/content/Context;

    const-string v8, "alarm"

    invoke-virtual {v7, v8}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/AlarmManager;

    invoke-virtual {v0, v3, v4}, Landroid/app/AlarmManager;->setTime(J)V

    const/4 v7, 0x1

    goto/16 :goto_0
.end method

.method private static parseResponse(Lcom/google/android/gsf/checkin/CheckinTask$Params;Lorg/apache/http/HttpResponse;)Lcom/google/android/gsf/checkin/proto/Checkin$AndroidCheckinResponse;
    .locals 11
    .param p0    # Lcom/google/android/gsf/checkin/CheckinTask$Params;
    .param p1    # Lorg/apache/http/HttpResponse;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    const-string v8, "Retry-After"

    invoke-interface {p1, v8}, Lorg/apache/http/HttpResponse;->getFirstHeader(Ljava/lang/String;)Lorg/apache/http/Header;

    move-result-object v6

    if-eqz v6, :cond_0

    iget-object v8, p0, Lcom/google/android/gsf/checkin/CheckinTask$Params;->scheduler:Lcom/android/common/OperationScheduler;

    if-eqz v8, :cond_0

    iget-object v8, p0, Lcom/google/android/gsf/checkin/CheckinTask$Params;->scheduler:Lcom/android/common/OperationScheduler;

    invoke-interface {v6}, Lorg/apache/http/Header;->getValue()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Lcom/android/common/OperationScheduler;->setMoratoriumTimeHttp(Ljava/lang/String;)Z

    move-result v8

    if-eqz v8, :cond_2

    const-string v8, "CheckinTask"

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "Got Retry-After: "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-interface {v6}, Lorg/apache/http/Header;->getValue()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v8, v9}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    :goto_0
    const-string v8, "Content-Type"

    invoke-interface {p1, v8}, Lorg/apache/http/HttpResponse;->getFirstHeader(Ljava/lang/String;)Lorg/apache/http/Header;

    move-result-object v0

    if-eqz v0, :cond_1

    invoke-interface {v0}, Lorg/apache/http/Header;->getValue()Ljava/lang/String;

    move-result-object v8

    if-nez v8, :cond_3

    :cond_1
    new-instance v8, Ljava/io/IOException;

    const-string v9, "No Content-Type header"

    invoke-direct {v8, v9}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v8

    :cond_2
    const-string v8, "CheckinTask"

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "Can\'t parse Retry-After: "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-interface {v6}, Lorg/apache/http/Header;->getValue()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v8, v9}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    :cond_3
    invoke-interface {v0}, Lorg/apache/http/Header;->getValue()Ljava/lang/String;

    move-result-object v8

    const-string v9, "application/x-protobuffer"

    invoke-virtual {v8, v9}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v8

    if-nez v8, :cond_4

    new-instance v8, Ljava/io/IOException;

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "Bad Content-Type: "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-interface {v0}, Lorg/apache/http/Header;->getValue()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-direct {v8, v9}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v8

    :cond_4
    invoke-interface {p1}, Lorg/apache/http/HttpResponse;->getStatusLine()Lorg/apache/http/StatusLine;

    move-result-object v7

    invoke-interface {p1}, Lorg/apache/http/HttpResponse;->getEntity()Lorg/apache/http/HttpEntity;

    move-result-object v2

    invoke-interface {v7}, Lorg/apache/http/StatusLine;->getStatusCode()I

    move-result v8

    const/16 v9, 0xc8

    if-eq v8, v9, :cond_6

    if-eqz v2, :cond_5

    invoke-interface {v2}, Lorg/apache/http/HttpEntity;->consumeContent()V

    :cond_5
    new-instance v8, Ljava/io/IOException;

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "Rejected response from server: "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-direct {v8, v9}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v8

    :cond_6
    if-nez v2, :cond_7

    new-instance v8, Ljava/io/IOException;

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "Empty response from server: "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-direct {v8, v9}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v8

    :cond_7
    invoke-interface {v2}, Lorg/apache/http/HttpEntity;->getContent()Ljava/io/InputStream;

    move-result-object v3

    invoke-interface {v2}, Lorg/apache/http/HttpEntity;->getContentEncoding()Lorg/apache/http/Header;

    move-result-object v1

    if-eqz v1, :cond_8

    invoke-interface {v1}, Lorg/apache/http/Header;->getValue()Ljava/lang/String;

    move-result-object v8

    const-string v9, "gzip"

    invoke-virtual {v8, v9}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v8

    if-eqz v8, :cond_8

    new-instance v4, Ljava/util/zip/GZIPInputStream;

    invoke-direct {v4, v3}, Ljava/util/zip/GZIPInputStream;-><init>(Ljava/io/InputStream;)V

    move-object v3, v4

    :cond_8
    new-instance v5, Lcom/google/android/gsf/checkin/proto/Checkin$AndroidCheckinResponse;

    invoke-direct {v5}, Lcom/google/android/gsf/checkin/proto/Checkin$AndroidCheckinResponse;-><init>()V

    :try_start_0
    invoke-static {v3}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->newInstance(Ljava/io/InputStream;)Lcom/google/protobuf/micro/CodedInputStreamMicro;

    move-result-object v8

    invoke-virtual {v5, v8}, Lcom/google/android/gsf/checkin/proto/Checkin$AndroidCheckinResponse;->mergeFrom(Lcom/google/protobuf/micro/CodedInputStreamMicro;)Lcom/google/android/gsf/checkin/proto/Checkin$AndroidCheckinResponse;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    invoke-virtual {v3}, Ljava/io/InputStream;->close()V

    invoke-virtual {v5}, Lcom/google/android/gsf/checkin/proto/Checkin$AndroidCheckinResponse;->hasStatsOk()Z

    move-result v8

    if-eqz v8, :cond_9

    invoke-virtual {v5}, Lcom/google/android/gsf/checkin/proto/Checkin$AndroidCheckinResponse;->getStatsOk()Z

    move-result v8

    if-nez v8, :cond_a

    :cond_9
    new-instance v8, Ljava/io/IOException;

    const-string v9, "Server refused checkin"

    invoke-direct {v8, v9}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v8

    :catchall_0
    move-exception v8

    invoke-virtual {v3}, Ljava/io/InputStream;->close()V

    throw v8

    :cond_a
    return-object v5
.end method

.method private static sendRequest(Lcom/google/android/gsf/checkin/CheckinTask$Params;Lorg/apache/http/client/HttpClient;Lcom/google/android/gsf/checkin/proto/Checkin$AndroidCheckinRequest;)Lcom/google/android/gsf/checkin/proto/Checkin$AndroidCheckinResponse;
    .locals 11
    .param p0    # Lcom/google/android/gsf/checkin/CheckinTask$Params;
    .param p1    # Lorg/apache/http/client/HttpClient;
    .param p2    # Lcom/google/android/gsf/checkin/proto/Checkin$AndroidCheckinRequest;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    new-instance v4, Lorg/apache/http/client/methods/HttpPost;

    iget-object v7, p0, Lcom/google/android/gsf/checkin/CheckinTask$Params;->serverUrl:Ljava/lang/String;

    invoke-direct {v4, v7}, Lorg/apache/http/client/methods/HttpPost;-><init>(Ljava/lang/String;)V

    const-string v7, "Content-type"

    const-string v8, "application/x-protobuffer"

    invoke-virtual {v4, v7, v8}, Lorg/apache/http/client/methods/HttpPost;->setHeader(Ljava/lang/String;Ljava/lang/String;)V

    new-instance v0, Ljava/io/ByteArrayOutputStream;

    invoke-direct {v0}, Ljava/io/ByteArrayOutputStream;-><init>()V

    new-instance v3, Ljava/util/zip/GZIPOutputStream;

    invoke-direct {v3, v0}, Ljava/util/zip/GZIPOutputStream;-><init>(Ljava/io/OutputStream;)V

    invoke-static {v3}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->newInstance(Ljava/io/OutputStream;)Lcom/google/protobuf/micro/CodedOutputStreamMicro;

    move-result-object v6

    invoke-virtual {p2, v6}, Lcom/google/android/gsf/checkin/proto/Checkin$AndroidCheckinRequest;->writeTo(Lcom/google/protobuf/micro/CodedOutputStreamMicro;)V

    invoke-virtual {v6}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->flush()V

    invoke-virtual {v3}, Ljava/util/zip/GZIPOutputStream;->close()V

    new-instance v2, Lorg/apache/http/entity/ByteArrayEntity;

    invoke-virtual {v0}, Ljava/io/ByteArrayOutputStream;->toByteArray()[B

    move-result-object v7

    invoke-direct {v2, v7}, Lorg/apache/http/entity/ByteArrayEntity;-><init>([B)V

    const-string v7, "gzip"

    invoke-virtual {v2, v7}, Lorg/apache/http/entity/ByteArrayEntity;->setContentEncoding(Ljava/lang/String;)V

    invoke-virtual {v4, v2}, Lorg/apache/http/client/methods/HttpPost;->setEntity(Lorg/apache/http/HttpEntity;)V

    const-string v7, "Accept-Encoding"

    const-string v8, "gzip"

    invoke-virtual {v4, v7, v8}, Lorg/apache/http/client/methods/HttpPost;->addHeader(Ljava/lang/String;Ljava/lang/String;)V

    :try_start_0
    const-string v7, "CheckinTask"

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "Sending checkin request ("

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v2}, Lorg/apache/http/entity/ByteArrayEntity;->getContentLength()J

    move-result-wide v9

    invoke-virtual {v8, v9, v10}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, " bytes)"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    invoke-interface {p1, v4}, Lorg/apache/http/client/HttpClient;->execute(Lorg/apache/http/client/methods/HttpUriRequest;)Lorg/apache/http/HttpResponse;
    :try_end_0
    .catch Ljavax/net/ssl/SSLException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v5

    :goto_0
    invoke-static {p0, v5}, Lcom/google/android/gsf/checkin/CheckinTask;->parseResponse(Lcom/google/android/gsf/checkin/CheckinTask$Params;Lorg/apache/http/HttpResponse;)Lcom/google/android/gsf/checkin/proto/Checkin$AndroidCheckinResponse;

    move-result-object v7

    return-object v7

    :catch_0
    move-exception v1

    const-string v7, "CheckinTask"

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "SSL error, attempting time correction: "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    invoke-static {p1, p0}, Lcom/google/android/gsf/checkin/CheckinTask;->maybeSetTime(Lorg/apache/http/client/HttpClient;Lcom/google/android/gsf/checkin/CheckinTask$Params;)Z

    move-result v7

    if-nez v7, :cond_0

    throw v1

    :cond_0
    invoke-interface {p1, v4}, Lorg/apache/http/client/HttpClient;->execute(Lorg/apache/http/client/methods/HttpUriRequest;)Lorg/apache/http/HttpResponse;

    move-result-object v5

    goto :goto_0
.end method


# virtual methods
.method protected varargs doInBackground([Lcom/google/android/gsf/checkin/CheckinTask$Params;)Lcom/google/android/gsf/checkin/proto/Checkin$AndroidCheckinResponse;
    .locals 25
    .param p1    # [Lcom/google/android/gsf/checkin/CheckinTask$Params;

    move-object/from16 v0, p1

    array-length v0, v0

    move/from16 v21, v0

    const/16 v22, 0x1

    move/from16 v0, v21

    move/from16 v1, v22

    if-eq v0, v1, :cond_0

    new-instance v21, Ljava/lang/IllegalArgumentException;

    const-string v22, "Must be one Params object"

    invoke-direct/range {v21 .. v22}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v21

    :cond_0
    const/16 v21, 0x0

    aget-object v15, p1, v21

    const/4 v5, 0x0

    const/4 v7, 0x0

    const/4 v8, 0x0

    const-wide/16 v3, 0x0

    :try_start_0
    iget-object v0, v15, Lcom/google/android/gsf/checkin/CheckinTask$Params;->storage:Landroid/content/SharedPreferences;

    move-object/from16 v21, v0

    if-eqz v21, :cond_1

    iget-object v0, v15, Lcom/google/android/gsf/checkin/CheckinTask$Params;->storage:Landroid/content/SharedPreferences;

    move-object/from16 v21, v0

    const-string v22, "CheckinTask_bookmark"

    const-wide/16 v23, 0x0

    invoke-interface/range {v21 .. v24}, Landroid/content/SharedPreferences;->getLong(Ljava/lang/String;J)J

    move-result-wide v3

    :cond_1
    new-instance v6, Lcom/google/android/common/http/GoogleHttpClient;

    iget-object v0, v15, Lcom/google/android/gsf/checkin/CheckinTask$Params;->context:Landroid/content/Context;

    move-object/from16 v21, v0

    const-string v22, "Android-Checkin/2.0"

    const/16 v23, 0x1

    move-object/from16 v0, v21

    move-object/from16 v1, v22

    move/from16 v2, v23

    invoke-direct {v6, v0, v1, v2}, Lcom/google/android/common/http/GoogleHttpClient;-><init>(Landroid/content/Context;Ljava/lang/String;Z)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :goto_0
    :try_start_1
    iget v0, v15, Lcom/google/android/gsf/checkin/CheckinTask$Params;->maxRequests:I

    move/from16 v21, v0

    move/from16 v0, v21

    if-ge v8, v0, :cond_2

    move-wide v13, v3

    invoke-static {v8}, Lcom/google/android/gsf/checkin/CheckinRequestBuilder;->newRequest(I)Lcom/google/android/gsf/checkin/proto/Checkin$AndroidCheckinRequest;

    move-result-object v16

    move-object/from16 v0, v16

    invoke-static {v15, v0, v3, v4}, Lcom/google/android/gsf/checkin/CheckinTask;->makeRequest(Lcom/google/android/gsf/checkin/CheckinTask$Params;Lcom/google/android/gsf/checkin/proto/Checkin$AndroidCheckinRequest;J)J

    move-result-wide v3

    cmp-long v21, v13, v3

    if-nez v21, :cond_5

    if-lez v8, :cond_5

    :cond_2
    const-string v21, "CheckinTask"

    new-instance v22, Ljava/lang/StringBuilder;

    invoke-direct/range {v22 .. v22}, Ljava/lang/StringBuilder;-><init>()V

    const-string v23, "Checkin success: "

    invoke-virtual/range {v22 .. v23}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v22

    iget-object v0, v15, Lcom/google/android/gsf/checkin/CheckinTask$Params;->serverUrl:Ljava/lang/String;

    move-object/from16 v23, v0

    invoke-virtual/range {v22 .. v23}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v22

    const-string v23, " ("

    invoke-virtual/range {v22 .. v23}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v22

    move-object/from16 v0, v22

    invoke-virtual {v0, v8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v22

    const-string v23, " requests sent)"

    invoke-virtual/range {v22 .. v23}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v22

    invoke-virtual/range {v22 .. v22}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v22

    invoke-static/range {v21 .. v22}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v0, v15, Lcom/google/android/gsf/checkin/CheckinTask$Params;->scheduler:Lcom/android/common/OperationScheduler;

    move-object/from16 v21, v0

    if-eqz v21, :cond_3

    iget-object v0, v15, Lcom/google/android/gsf/checkin/CheckinTask$Params;->scheduler:Lcom/android/common/OperationScheduler;

    move-object/from16 v21, v0

    invoke-virtual/range {v21 .. v21}, Lcom/android/common/OperationScheduler;->onSuccess()V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    :cond_3
    if-eqz v6, :cond_e

    invoke-virtual {v6}, Lcom/google/android/common/http/GoogleHttpClient;->close()V

    move-object v5, v6

    :cond_4
    :goto_1
    return-object v7

    :cond_5
    :try_start_2
    move-object/from16 v0, v16

    invoke-static {v15, v6, v0}, Lcom/google/android/gsf/checkin/CheckinTask;->sendRequest(Lcom/google/android/gsf/checkin/CheckinTask$Params;Lorg/apache/http/client/HttpClient;Lcom/google/android/gsf/checkin/proto/Checkin$AndroidCheckinRequest;)Lcom/google/android/gsf/checkin/proto/Checkin$AndroidCheckinResponse;

    move-result-object v17

    iget-object v0, v15, Lcom/google/android/gsf/checkin/CheckinTask$Params;->scheduler:Lcom/android/common/OperationScheduler;

    move-object/from16 v21, v0

    if-eqz v21, :cond_6

    iget-object v0, v15, Lcom/google/android/gsf/checkin/CheckinTask$Params;->scheduler:Lcom/android/common/OperationScheduler;

    move-object/from16 v21, v0

    invoke-virtual/range {v21 .. v21}, Lcom/android/common/OperationScheduler;->resetTransientError()V

    :cond_6
    if-nez v7, :cond_b

    move-object/from16 v7, v17

    :goto_2
    iget-object v0, v15, Lcom/google/android/gsf/checkin/CheckinTask$Params;->storage:Landroid/content/SharedPreferences;

    move-object/from16 v21, v0

    if-eqz v21, :cond_9

    iget-object v0, v15, Lcom/google/android/gsf/checkin/CheckinTask$Params;->storage:Landroid/content/SharedPreferences;

    move-object/from16 v21, v0

    invoke-interface/range {v21 .. v21}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v12

    const-string v21, "CheckinTask_bookmark"

    move-object/from16 v0, v21

    invoke-interface {v12, v0, v3, v4}, Landroid/content/SharedPreferences$Editor;->putLong(Ljava/lang/String;J)Landroid/content/SharedPreferences$Editor;

    if-eqz v17, :cond_8

    invoke-virtual/range {v17 .. v17}, Lcom/google/android/gsf/checkin/proto/Checkin$AndroidCheckinResponse;->hasSecurityToken()Z

    move-result v21

    if-eqz v21, :cond_8

    invoke-virtual/range {v17 .. v17}, Lcom/google/android/gsf/checkin/proto/Checkin$AndroidCheckinResponse;->getSecurityToken()J

    move-result-wide v19

    const-wide/16 v21, 0x0

    cmp-long v21, v19, v21

    if-eqz v21, :cond_8

    iget-object v0, v15, Lcom/google/android/gsf/checkin/CheckinTask$Params;->storage:Landroid/content/SharedPreferences;

    move-object/from16 v21, v0

    const-string v22, "CheckinTask_securityToken"

    const-wide/16 v23, 0x0

    invoke-interface/range {v21 .. v24}, Landroid/content/SharedPreferences;->getLong(Ljava/lang/String;J)J

    move-result-wide v21

    cmp-long v21, v19, v21

    if-eqz v21, :cond_7

    const-string v21, "CheckinTask_securityToken"

    move-object/from16 v0, v21

    move-wide/from16 v1, v19

    invoke-interface {v12, v0, v1, v2}, Landroid/content/SharedPreferences$Editor;->putLong(Ljava/lang/String;J)Landroid/content/SharedPreferences$Editor;

    :cond_7
    iget-object v0, v15, Lcom/google/android/gsf/checkin/CheckinTask$Params;->context:Landroid/content/Context;

    move-object/from16 v21, v0

    invoke-virtual/range {v21 .. v21}, Landroid/content/Context;->getFilesDir()Ljava/io/File;

    move-result-object v9

    new-instance v18, Ljava/io/File;

    const-string v21, "security_token"

    move-object/from16 v0, v18

    move-object/from16 v1, v21

    invoke-direct {v0, v9, v1}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    invoke-virtual/range {v18 .. v18}, Ljava/io/File;->exists()Z

    move-result v21

    if-nez v21, :cond_8

    new-instance v10, Ljava/io/DataOutputStream;

    iget-object v0, v15, Lcom/google/android/gsf/checkin/CheckinTask$Params;->context:Landroid/content/Context;

    move-object/from16 v21, v0

    const-string v22, "security_token"

    const/16 v23, 0x0

    invoke-virtual/range {v21 .. v23}, Landroid/content/Context;->openFileOutput(Ljava/lang/String;I)Ljava/io/FileOutputStream;

    move-result-object v21

    move-object/from16 v0, v21

    invoke-direct {v10, v0}, Ljava/io/DataOutputStream;-><init>(Ljava/io/OutputStream;)V

    move-wide/from16 v0, v19

    invoke-virtual {v10, v0, v1}, Ljava/io/DataOutputStream;->writeLong(J)V

    invoke-virtual {v10}, Ljava/io/DataOutputStream;->close()V

    :cond_8
    invoke-interface {v12}, Landroid/content/SharedPreferences$Editor;->apply()V

    :cond_9
    if-eqz v17, :cond_a

    iget-object v0, v15, Lcom/google/android/gsf/checkin/CheckinTask$Params;->context:Landroid/content/Context;

    move-object/from16 v21, v0

    invoke-virtual/range {v21 .. v21}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v21

    move-object/from16 v0, v17

    move-object/from16 v1, v21

    invoke-static {v0, v1}, Lcom/google/android/gsf/checkin/CheckinResponseProcessor;->updateGservices(Lcom/google/android/gsf/checkin/proto/Checkin$AndroidCheckinResponse;Landroid/content/ContentResolver;)V

    :cond_a
    add-int/lit8 v8, v8, 0x1

    goto/16 :goto_0

    :cond_b
    move-object/from16 v0, v17

    invoke-static {v7, v0}, Lcom/google/android/gsf/checkin/CheckinTask;->combineResponses(Lcom/google/android/gsf/checkin/proto/Checkin$AndroidCheckinResponse;Lcom/google/android/gsf/checkin/proto/Checkin$AndroidCheckinResponse;)Lcom/google/android/gsf/checkin/proto/Checkin$AndroidCheckinResponse;
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_1
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    move-result-object v7

    goto/16 :goto_2

    :catch_0
    move-exception v11

    :goto_3
    :try_start_3
    const-string v21, "CheckinTask"

    new-instance v22, Ljava/lang/StringBuilder;

    invoke-direct/range {v22 .. v22}, Ljava/lang/StringBuilder;-><init>()V

    const-string v23, "Checkin failed: "

    invoke-virtual/range {v22 .. v23}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v22

    iget-object v0, v15, Lcom/google/android/gsf/checkin/CheckinTask$Params;->serverUrl:Ljava/lang/String;

    move-object/from16 v23, v0

    invoke-virtual/range {v22 .. v23}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v22

    const-string v23, " (request #"

    invoke-virtual/range {v22 .. v23}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v22

    move-object/from16 v0, v22

    invoke-virtual {v0, v8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v22

    const-string v23, "): "

    invoke-virtual/range {v22 .. v23}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v22

    move-object/from16 v0, v22

    invoke-virtual {v0, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v22

    invoke-virtual/range {v22 .. v22}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v22

    invoke-static/range {v21 .. v22}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v0, v15, Lcom/google/android/gsf/checkin/CheckinTask$Params;->scheduler:Lcom/android/common/OperationScheduler;

    move-object/from16 v21, v0

    if-eqz v21, :cond_c

    iget-object v0, v15, Lcom/google/android/gsf/checkin/CheckinTask$Params;->scheduler:Lcom/android/common/OperationScheduler;

    move-object/from16 v21, v0

    invoke-virtual/range {v21 .. v21}, Lcom/android/common/OperationScheduler;->onTransientError()V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    :cond_c
    if-eqz v5, :cond_4

    invoke-virtual {v5}, Lcom/google/android/common/http/GoogleHttpClient;->close()V

    goto/16 :goto_1

    :catchall_0
    move-exception v21

    :goto_4
    if-eqz v5, :cond_d

    invoke-virtual {v5}, Lcom/google/android/common/http/GoogleHttpClient;->close()V

    :cond_d
    throw v21

    :catchall_1
    move-exception v21

    move-object v5, v6

    goto :goto_4

    :catch_1
    move-exception v11

    move-object v5, v6

    goto :goto_3

    :cond_e
    move-object v5, v6

    goto/16 :goto_1
.end method

.method protected bridge synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1
    .param p1    # [Ljava/lang/Object;

    check-cast p1, [Lcom/google/android/gsf/checkin/CheckinTask$Params;

    invoke-virtual {p0, p1}, Lcom/google/android/gsf/checkin/CheckinTask;->doInBackground([Lcom/google/android/gsf/checkin/CheckinTask$Params;)Lcom/google/android/gsf/checkin/proto/Checkin$AndroidCheckinResponse;

    move-result-object v0

    return-object v0
.end method
