.class Lcom/google/android/gsf/settings/GoogleLocationSettings$3;
.super Ljava/lang/Object;
.source "GoogleLocationSettings.java"

# interfaces
.implements Landroid/content/DialogInterface$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/gsf/settings/GoogleLocationSettings;->showAllowHistoryDialog()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/gsf/settings/GoogleLocationSettings;


# direct methods
.method constructor <init>(Lcom/google/android/gsf/settings/GoogleLocationSettings;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/gsf/settings/GoogleLocationSettings$3;->this$0:Lcom/google/android/gsf/settings/GoogleLocationSettings;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/content/DialogInterface;I)V
    .locals 2
    .param p1    # Landroid/content/DialogInterface;
    .param p2    # I

    iget-object v1, p0, Lcom/google/android/gsf/settings/GoogleLocationSettings$3;->this$0:Lcom/google/android/gsf/settings/GoogleLocationSettings;

    iget-object v0, p0, Lcom/google/android/gsf/settings/GoogleLocationSettings$3;->this$0:Lcom/google/android/gsf/settings/GoogleLocationSettings;

    # getter for: Lcom/google/android/gsf/settings/GoogleLocationSettings;->mAllowHistory:Landroid/widget/CheckBox;
    invoke-static {v0}, Lcom/google/android/gsf/settings/GoogleLocationSettings;->access$300(Lcom/google/android/gsf/settings/GoogleLocationSettings;)Landroid/widget/CheckBox;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/CheckBox;->isChecked()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    # invokes: Lcom/google/android/gsf/settings/GoogleLocationSettings;->setAllowHistoryQuietly(Z)V
    invoke-static {v1, v0}, Lcom/google/android/gsf/settings/GoogleLocationSettings;->access$400(Lcom/google/android/gsf/settings/GoogleLocationSettings;Z)V

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
