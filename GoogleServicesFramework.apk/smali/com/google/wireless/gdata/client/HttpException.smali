.class public Lcom/google/wireless/gdata/client/HttpException;
.super Ljava/lang/Exception;
.source "HttpException.java"


# instance fields
.field private final responseStream:Ljava/io/InputStream;

.field private final statusCode:I


# direct methods
.method public constructor <init>(Ljava/lang/String;ILjava/io/InputStream;)V
    .locals 0
    .param p1    # Ljava/lang/String;
    .param p2    # I
    .param p3    # Ljava/io/InputStream;

    invoke-direct {p0, p1}, Ljava/lang/Exception;-><init>(Ljava/lang/String;)V

    iput p2, p0, Lcom/google/wireless/gdata/client/HttpException;->statusCode:I

    iput-object p3, p0, Lcom/google/wireless/gdata/client/HttpException;->responseStream:Ljava/io/InputStream;

    return-void
.end method


# virtual methods
.method public getStatusCode()I
    .locals 1

    iget v0, p0, Lcom/google/wireless/gdata/client/HttpException;->statusCode:I

    return v0
.end method
