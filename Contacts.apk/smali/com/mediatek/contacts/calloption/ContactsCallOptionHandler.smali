.class public Lcom/mediatek/contacts/calloption/ContactsCallOptionHandler;
.super Lcom/mediatek/calloption/CallOptionHandler;
.source "ContactsCallOptionHandler.java"

# interfaces
.implements Lcom/mediatek/calloption/CallOptionBaseHandler$ICallOptionResultHandle;


# static fields
.field private static final TAG:Ljava/lang/String; = "ContactsCallOptionHandler"


# instance fields
.field private mActivityContext:Landroid/content/Context;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/mediatek/calloption/CallOptionHandlerFactory;)V
    .locals 0
    .param p1    # Landroid/content/Context;
    .param p2    # Lcom/mediatek/calloption/CallOptionHandlerFactory;

    invoke-direct {p0, p2}, Lcom/mediatek/calloption/CallOptionHandler;-><init>(Lcom/mediatek/calloption/CallOptionHandlerFactory;)V

    iput-object p1, p0, Lcom/mediatek/contacts/calloption/ContactsCallOptionHandler;->mActivityContext:Landroid/content/Context;

    return-void
.end method

.method private static log(Ljava/lang/String;)V
    .locals 1
    .param p0    # Ljava/lang/String;

    const-string v0, "ContactsCallOptionHandler"

    invoke-static {v0, p0}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    return-void
.end method


# virtual methods
.method public doCallOptionHandle(Landroid/content/Intent;)V
    .locals 9
    .param p1    # Landroid/content/Intent;

    const-string v0, "phone"

    invoke-static {v0}, Landroid/os/ServiceManager;->getService(Ljava/lang/String;)Landroid/os/IBinder;

    move-result-object v0

    invoke-static {v0}, Lcom/android/internal/telephony/ITelephony$Stub;->asInterface(Landroid/os/IBinder;)Lcom/android/internal/telephony/ITelephony;

    move-result-object v6

    if-nez v6, :cond_0

    const-string v0, "Can not get telephony service object"

    invoke-static {v0}, Lcom/mediatek/contacts/calloption/ContactsCallOptionHandler;->log(Ljava/lang/String;)V

    :goto_0
    return-void

    :cond_0
    invoke-static {}, Lcom/android/contacts/ContactsApplication;->getInstance()Lcom/android/contacts/ContactsApplication;

    move-result-object v0

    invoke-static {v0}, Lcom/mediatek/calloption/SimAssociateHandler;->getInstance(Landroid/content/Context;)Lcom/mediatek/calloption/SimAssociateHandler;

    move-result-object v0

    invoke-virtual {v0}, Lcom/mediatek/calloption/SimAssociateHandler;->load()V

    iget-object v1, p0, Lcom/mediatek/contacts/calloption/ContactsCallOptionHandler;->mActivityContext:Landroid/content/Context;

    invoke-static {}, Lcom/android/contacts/ContactsApplication;->getInstance()Lcom/android/contacts/ContactsApplication;

    move-result-object v2

    invoke-static {}, Lcom/android/contacts/ContactsApplication;->getInstance()Lcom/android/contacts/ContactsApplication;

    move-result-object v0

    iget-object v5, v0, Lcom/android/contacts/ContactsApplication;->cellConnMgr:Lcom/mediatek/CellConnService/CellConnMgr;

    invoke-static {}, Lcom/mediatek/contacts/simcontact/SlotUtils;->isGeminiEnabled()Z

    move-result v7

    const/4 v8, 0x1

    move-object v0, p0

    move-object v3, p1

    move-object v4, p0

    invoke-super/range {v0 .. v8}, Lcom/mediatek/calloption/CallOptionHandler;->doCallOptionHandle(Landroid/content/Context;Landroid/content/Context;Landroid/content/Intent;Lcom/mediatek/calloption/CallOptionBaseHandler$ICallOptionResultHandle;Lcom/mediatek/CellConnService/CellConnMgr;Lcom/android/internal/telephony/ITelephony;ZZ)V

    goto :goto_0
.end method

.method public onContinueCallProcess(Landroid/content/Intent;)V
    .locals 2
    .param p1    # Landroid/content/Intent;

    invoke-virtual {p0}, Lcom/mediatek/calloption/CallOptionHandler;->dismissDialogs()V

    const-string v0, "com.mediatek.phone.OutgoingCallReceiver"

    invoke-virtual {p1, v0}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    const-string v0, "com.android.phone"

    const-string v1, "com.mediatek.phone.OutgoingCallReceiver"

    invoke-virtual {p1, v0, v1}, Landroid/content/Intent;->setClassName(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    invoke-static {}, Lcom/android/contacts/ContactsApplication;->getInstance()Lcom/android/contacts/ContactsApplication;

    move-result-object v0

    invoke-virtual {v0, p1}, Landroid/content/ContextWrapper;->sendBroadcast(Landroid/content/Intent;)V

    return-void
.end method

.method public onHandlingFinish()V
    .locals 0

    return-void
.end method
