.class Lcom/mediatek/contacts/simcontact/AbstractStartSIMService$ImportAllSimContactsThread;
.super Ljava/lang/Thread;
.source "AbstractStartSIMService.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/mediatek/contacts/simcontact/AbstractStartSIMService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "ImportAllSimContactsThread"
.end annotation


# instance fields
.field mSimCursor:Landroid/database/Cursor;

.field mSimId:I

.field mSimType:I

.field mSlot:I

.field final synthetic this$0:Lcom/mediatek/contacts/simcontact/AbstractStartSIMService;


# direct methods
.method public constructor <init>(Lcom/mediatek/contacts/simcontact/AbstractStartSIMService;Lcom/mediatek/contacts/simcontact/AbstractStartSIMService$ServiceWorkData;)V
    .locals 2
    .param p2    # Lcom/mediatek/contacts/simcontact/AbstractStartSIMService$ServiceWorkData;

    const/4 v1, 0x0

    iput-object p1, p0, Lcom/mediatek/contacts/simcontact/AbstractStartSIMService$ImportAllSimContactsThread;->this$0:Lcom/mediatek/contacts/simcontact/AbstractStartSIMService;

    const-string v0, "ImportAllSimContactsThread"

    invoke-direct {p0, v0}, Ljava/lang/Thread;-><init>(Ljava/lang/String;)V

    iput v1, p0, Lcom/mediatek/contacts/simcontact/AbstractStartSIMService$ImportAllSimContactsThread;->mSlot:I

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/mediatek/contacts/simcontact/AbstractStartSIMService$ImportAllSimContactsThread;->mSimCursor:Landroid/database/Cursor;

    const/4 v0, -0x1

    iput v0, p0, Lcom/mediatek/contacts/simcontact/AbstractStartSIMService$ImportAllSimContactsThread;->mSimType:I

    iput v1, p0, Lcom/mediatek/contacts/simcontact/AbstractStartSIMService$ImportAllSimContactsThread;->mSimId:I

    iget v0, p2, Lcom/mediatek/contacts/simcontact/AbstractStartSIMService$ServiceWorkData;->mSlotId:I

    iput v0, p0, Lcom/mediatek/contacts/simcontact/AbstractStartSIMService$ImportAllSimContactsThread;->mSlot:I

    iget-object v0, p2, Lcom/mediatek/contacts/simcontact/AbstractStartSIMService$ServiceWorkData;->mSimCursor:Landroid/database/Cursor;

    iput-object v0, p0, Lcom/mediatek/contacts/simcontact/AbstractStartSIMService$ImportAllSimContactsThread;->mSimCursor:Landroid/database/Cursor;

    iget v0, p2, Lcom/mediatek/contacts/simcontact/AbstractStartSIMService$ServiceWorkData;->mSimType:I

    iput v0, p0, Lcom/mediatek/contacts/simcontact/AbstractStartSIMService$ImportAllSimContactsThread;->mSimType:I

    iget v0, p2, Lcom/mediatek/contacts/simcontact/AbstractStartSIMService$ServiceWorkData;->mSimId:I

    iput v0, p0, Lcom/mediatek/contacts/simcontact/AbstractStartSIMService$ImportAllSimContactsThread;->mSimId:I

    return-void
.end method


# virtual methods
.method public run()V
    .locals 18

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/mediatek/contacts/simcontact/AbstractStartSIMService$ImportAllSimContactsThread;->this$0:Lcom/mediatek/contacts/simcontact/AbstractStartSIMService;

    invoke-virtual {v1}, Landroid/content/ContextWrapper;->getApplicationContext()Landroid/content/Context;

    move-result-object v14

    invoke-virtual {v14}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v3

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/mediatek/contacts/simcontact/AbstractStartSIMService$ImportAllSimContactsThread;->this$0:Lcom/mediatek/contacts/simcontact/AbstractStartSIMService;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "importing thread for SIM "

    invoke-virtual {v2, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    move-object/from16 v0, p0

    iget v5, v0, Lcom/mediatek/contacts/simcontact/AbstractStartSIMService$ImportAllSimContactsThread;->mSlot:I

    invoke-virtual {v2, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/mediatek/contacts/simcontact/AbstractStartSIMService;->access$000(Lcom/mediatek/contacts/simcontact/AbstractStartSIMService;Ljava/lang/String;)V

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/mediatek/contacts/simcontact/AbstractStartSIMService$ImportAllSimContactsThread;->mSimCursor:Landroid/database/Cursor;

    if-eqz v1, :cond_3

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/mediatek/contacts/simcontact/AbstractStartSIMService$ImportAllSimContactsThread;->this$0:Lcom/mediatek/contacts/simcontact/AbstractStartSIMService;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "1begin insert slot:"

    invoke-virtual {v2, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    move-object/from16 v0, p0

    iget v5, v0, Lcom/mediatek/contacts/simcontact/AbstractStartSIMService$ImportAllSimContactsThread;->mSlot:I

    invoke-virtual {v2, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v5, " contact|mSimId:"

    invoke-virtual {v2, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    move-object/from16 v0, p0

    iget v5, v0, Lcom/mediatek/contacts/simcontact/AbstractStartSIMService$ImportAllSimContactsThread;->mSimId:I

    invoke-virtual {v2, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/mediatek/contacts/simcontact/AbstractStartSIMService;->access$000(Lcom/mediatek/contacts/simcontact/AbstractStartSIMService;Ljava/lang/String;)V

    move-object/from16 v0, p0

    iget v1, v0, Lcom/mediatek/contacts/simcontact/AbstractStartSIMService$ImportAllSimContactsThread;->mSimId:I

    const/4 v2, 0x1

    if-ge v1, v2, :cond_0

    move-object/from16 v0, p0

    iget v1, v0, Lcom/mediatek/contacts/simcontact/AbstractStartSIMService$ImportAllSimContactsThread;->mSlot:I

    invoke-static {v14, v1}, Landroid/provider/Telephony$SIMInfo;->getSIMInfoBySlot(Landroid/content/Context;I)Landroid/provider/Telephony$SIMInfo;

    move-result-object v17

    if-eqz v17, :cond_0

    move-object/from16 v0, v17

    iget-wide v1, v0, Landroid/provider/Telephony$SIMInfo;->mSimId:J

    long-to-int v1, v1

    move-object/from16 v0, p0

    iput v1, v0, Lcom/mediatek/contacts/simcontact/AbstractStartSIMService$ImportAllSimContactsThread;->mSimId:I

    :cond_0
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/mediatek/contacts/simcontact/AbstractStartSIMService$ImportAllSimContactsThread;->this$0:Lcom/mediatek/contacts/simcontact/AbstractStartSIMService;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "2begin insert slot:"

    invoke-virtual {v2, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    move-object/from16 v0, p0

    iget v5, v0, Lcom/mediatek/contacts/simcontact/AbstractStartSIMService$ImportAllSimContactsThread;->mSlot:I

    invoke-virtual {v2, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v5, " contact|mSimId:"

    invoke-virtual {v2, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    move-object/from16 v0, p0

    iget v5, v0, Lcom/mediatek/contacts/simcontact/AbstractStartSIMService$ImportAllSimContactsThread;->mSimId:I

    invoke-virtual {v2, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/mediatek/contacts/simcontact/AbstractStartSIMService;->access$000(Lcom/mediatek/contacts/simcontact/AbstractStartSIMService;Ljava/lang/String;)V

    move-object/from16 v0, p0

    iget v1, v0, Lcom/mediatek/contacts/simcontact/AbstractStartSIMService$ImportAllSimContactsThread;->mSimId:I

    if-lez v1, :cond_1

    monitor-enter p0

    :try_start_0
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/mediatek/contacts/simcontact/AbstractStartSIMService$ImportAllSimContactsThread;->this$0:Lcom/mediatek/contacts/simcontact/AbstractStartSIMService;

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/mediatek/contacts/simcontact/AbstractStartSIMService$ImportAllSimContactsThread;->mSimCursor:Landroid/database/Cursor;

    move-object/from16 v0, p0

    iget v4, v0, Lcom/mediatek/contacts/simcontact/AbstractStartSIMService$ImportAllSimContactsThread;->mSlot:I

    move-object/from16 v0, p0

    iget v5, v0, Lcom/mediatek/contacts/simcontact/AbstractStartSIMService$ImportAllSimContactsThread;->mSimId:I

    int-to-long v5, v5

    move-object/from16 v0, p0

    iget v7, v0, Lcom/mediatek/contacts/simcontact/AbstractStartSIMService$ImportAllSimContactsThread;->mSimType:I

    const/4 v8, 0x0

    const/4 v9, 0x0

    invoke-static/range {v1 .. v9}, Lcom/mediatek/contacts/simcontact/AbstractStartSIMService;->access$400(Lcom/mediatek/contacts/simcontact/AbstractStartSIMService;Landroid/database/Cursor;Landroid/content/ContentResolver;IJILjava/util/HashSet;Z)V

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :cond_1
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/mediatek/contacts/simcontact/AbstractStartSIMService$ImportAllSimContactsThread;->this$0:Lcom/mediatek/contacts/simcontact/AbstractStartSIMService;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "end insert slot"

    invoke-virtual {v2, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    move-object/from16 v0, p0

    iget v5, v0, Lcom/mediatek/contacts/simcontact/AbstractStartSIMService$ImportAllSimContactsThread;->mSlot:I

    invoke-virtual {v2, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v5, " contact"

    invoke-virtual {v2, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/mediatek/contacts/simcontact/AbstractStartSIMService;->access$000(Lcom/mediatek/contacts/simcontact/AbstractStartSIMService;Ljava/lang/String;)V

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/mediatek/contacts/simcontact/AbstractStartSIMService$ImportAllSimContactsThread;->mSimCursor:Landroid/database/Cursor;

    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/mediatek/contacts/simcontact/AbstractStartSIMService$ImportAllSimContactsThread;->this$0:Lcom/mediatek/contacts/simcontact/AbstractStartSIMService;

    move-object/from16 v0, p0

    iget v2, v0, Lcom/mediatek/contacts/simcontact/AbstractStartSIMService$ImportAllSimContactsThread;->mSlot:I

    invoke-virtual {v1, v2}, Lcom/mediatek/contacts/simcontact/AbstractStartSIMService;->checkSimState(I)Z

    move-result v1

    if-eqz v1, :cond_3

    move-object/from16 v0, p0

    iget v1, v0, Lcom/mediatek/contacts/simcontact/AbstractStartSIMService$ImportAllSimContactsThread;->mSimId:I

    if-lez v1, :cond_3

    const/4 v6, 0x0

    move-object/from16 v0, p0

    iget v1, v0, Lcom/mediatek/contacts/simcontact/AbstractStartSIMService$ImportAllSimContactsThread;->mSlot:I

    invoke-static {v1}, Lcom/mediatek/contacts/simcontact/SimCardUtils$SimUri;->getSimSdnUri(I)Landroid/net/Uri;

    move-result-object v4

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/mediatek/contacts/simcontact/AbstractStartSIMService$ImportAllSimContactsThread;->this$0:Lcom/mediatek/contacts/simcontact/AbstractStartSIMService;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "iccSdnUri"

    invoke-virtual {v2, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/mediatek/contacts/simcontact/AbstractStartSIMService;->access$000(Lcom/mediatek/contacts/simcontact/AbstractStartSIMService;Ljava/lang/String;)V

    invoke-static {}, Lcom/mediatek/contacts/simcontact/AbstractStartSIMService;->access$200()[Ljava/lang/String;

    move-result-object v5

    const/4 v6, 0x0

    const/4 v7, 0x0

    const/4 v8, 0x0

    invoke-virtual/range {v3 .. v8}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v6

    if-eqz v6, :cond_3

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/mediatek/contacts/simcontact/AbstractStartSIMService$ImportAllSimContactsThread;->this$0:Lcom/mediatek/contacts/simcontact/AbstractStartSIMService;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "sdnCursor.getCount() = "

    invoke-virtual {v2, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-interface {v6}, Landroid/database/Cursor;->getCount()I

    move-result v5

    invoke-virtual {v2, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/mediatek/contacts/simcontact/AbstractStartSIMService;->access$000(Lcom/mediatek/contacts/simcontact/AbstractStartSIMService;Ljava/lang/String;)V

    :try_start_1
    invoke-interface {v6}, Landroid/database/Cursor;->getCount()I

    move-result v1

    if-lez v1, :cond_2

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/mediatek/contacts/simcontact/AbstractStartSIMService$ImportAllSimContactsThread;->this$0:Lcom/mediatek/contacts/simcontact/AbstractStartSIMService;

    move-object/from16 v0, p0

    iget v8, v0, Lcom/mediatek/contacts/simcontact/AbstractStartSIMService$ImportAllSimContactsThread;->mSlot:I

    move-object/from16 v0, p0

    iget v1, v0, Lcom/mediatek/contacts/simcontact/AbstractStartSIMService$ImportAllSimContactsThread;->mSimId:I

    int-to-long v9, v1

    move-object/from16 v0, p0

    iget v11, v0, Lcom/mediatek/contacts/simcontact/AbstractStartSIMService$ImportAllSimContactsThread;->mSimType:I

    const/4 v12, 0x0

    const/4 v13, 0x1

    move-object v7, v3

    invoke-static/range {v5 .. v13}, Lcom/mediatek/contacts/simcontact/AbstractStartSIMService;->access$400(Lcom/mediatek/contacts/simcontact/AbstractStartSIMService;Landroid/database/Cursor;Landroid/content/ContentResolver;IJILjava/util/HashSet;Z)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    :cond_2
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    :cond_3
    :goto_0
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/mediatek/contacts/simcontact/AbstractStartSIMService$ImportAllSimContactsThread;->this$0:Lcom/mediatek/contacts/simcontact/AbstractStartSIMService;

    const-string v2, "[ImportAllSimContactsThread]send finish msg"

    invoke-static {v1, v2}, Lcom/mediatek/contacts/simcontact/AbstractStartSIMService;->access$000(Lcom/mediatek/contacts/simcontact/AbstractStartSIMService;Ljava/lang/String;)V

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/mediatek/contacts/simcontact/AbstractStartSIMService$ImportAllSimContactsThread;->this$0:Lcom/mediatek/contacts/simcontact/AbstractStartSIMService;

    invoke-static {v1}, Lcom/mediatek/contacts/simcontact/AbstractStartSIMService;->access$500(Lcom/mediatek/contacts/simcontact/AbstractStartSIMService;)Lcom/mediatek/contacts/simcontact/AbstractStartSIMService$SimHandler;

    move-result-object v1

    const/16 v2, 0x1f4

    invoke-virtual {v1, v2}, Landroid/os/Handler;->obtainMessage(I)Landroid/os/Message;

    move-result-object v16

    move-object/from16 v0, p0

    iget v1, v0, Lcom/mediatek/contacts/simcontact/AbstractStartSIMService$ImportAllSimContactsThread;->mSlot:I

    move-object/from16 v0, v16

    iput v1, v0, Landroid/os/Message;->arg2:I

    invoke-virtual/range {v16 .. v16}, Landroid/os/Message;->sendToTarget()V

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/mediatek/contacts/simcontact/AbstractStartSIMService$ImportAllSimContactsThread;->this$0:Lcom/mediatek/contacts/simcontact/AbstractStartSIMService;

    const-string v2, "[ImportAllSimContactsThread]send finish end"

    invoke-static {v1, v2}, Lcom/mediatek/contacts/simcontact/AbstractStartSIMService;->access$000(Lcom/mediatek/contacts/simcontact/AbstractStartSIMService;Ljava/lang/String;)V

    return-void

    :catchall_0
    move-exception v1

    :try_start_2
    monitor-exit p0
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    throw v1

    :catch_0
    move-exception v15

    :try_start_3
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/mediatek/contacts/simcontact/AbstractStartSIMService$ImportAllSimContactsThread;->this$0:Lcom/mediatek/contacts/simcontact/AbstractStartSIMService;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "actuallyImportOneSimContact exception"

    invoke-virtual {v2, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v15}, Ljava/lang/Throwable;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v2, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/mediatek/contacts/simcontact/AbstractStartSIMService;->access$000(Lcom/mediatek/contacts/simcontact/AbstractStartSIMService;Ljava/lang/String;)V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    goto :goto_0

    :catchall_1
    move-exception v1

    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    throw v1
.end method
