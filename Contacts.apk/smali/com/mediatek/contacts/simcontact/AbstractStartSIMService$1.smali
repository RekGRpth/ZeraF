.class Lcom/mediatek/contacts/simcontact/AbstractStartSIMService$1;
.super Ljava/lang/Object;
.source "AbstractStartSIMService.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/mediatek/contacts/simcontact/AbstractStartSIMService;->deleteSimContact(Lcom/mediatek/contacts/simcontact/AbstractStartSIMService$ServiceWorkData;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/mediatek/contacts/simcontact/AbstractStartSIMService;

.field final synthetic val$slotId:I

.field final synthetic val$workData:Lcom/mediatek/contacts/simcontact/AbstractStartSIMService$ServiceWorkData;


# direct methods
.method constructor <init>(Lcom/mediatek/contacts/simcontact/AbstractStartSIMService;ILcom/mediatek/contacts/simcontact/AbstractStartSIMService$ServiceWorkData;)V
    .locals 0

    iput-object p1, p0, Lcom/mediatek/contacts/simcontact/AbstractStartSIMService$1;->this$0:Lcom/mediatek/contacts/simcontact/AbstractStartSIMService;

    iput p2, p0, Lcom/mediatek/contacts/simcontact/AbstractStartSIMService$1;->val$slotId:I

    iput-object p3, p0, Lcom/mediatek/contacts/simcontact/AbstractStartSIMService$1;->val$workData:Lcom/mediatek/contacts/simcontact/AbstractStartSIMService$ServiceWorkData;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 21

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/mediatek/contacts/simcontact/AbstractStartSIMService$1;->this$0:Lcom/mediatek/contacts/simcontact/AbstractStartSIMService;

    move-object/from16 v17, v0

    new-instance v18, Ljava/lang/StringBuilder;

    invoke-direct/range {v18 .. v18}, Ljava/lang/StringBuilder;-><init>()V

    const-string v19, "[deleteSimContact] begin. slotId: "

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    move-object/from16 v0, p0

    iget v0, v0, Lcom/mediatek/contacts/simcontact/AbstractStartSIMService$1;->val$slotId:I

    move/from16 v19, v0

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v18

    invoke-virtual/range {v18 .. v18}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v18

    invoke-static/range {v17 .. v18}, Lcom/mediatek/contacts/simcontact/AbstractStartSIMService;->access$000(Lcom/mediatek/contacts/simcontact/AbstractStartSIMService;Ljava/lang/String;)V

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/mediatek/contacts/simcontact/AbstractStartSIMService$1;->this$0:Lcom/mediatek/contacts/simcontact/AbstractStartSIMService;

    move-object/from16 v17, v0

    invoke-virtual/range {v17 .. v17}, Landroid/content/ContextWrapper;->getApplicationContext()Landroid/content/Context;

    move-result-object v2

    const/16 v9, 0xa

    invoke-static {}, Lcom/mediatek/contacts/simcontact/SimCardUtils;->isSimInfoReady()Z

    move-result v13

    :goto_0
    if-lez v9, :cond_0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/mediatek/contacts/simcontact/AbstractStartSIMService$1;->this$0:Lcom/mediatek/contacts/simcontact/AbstractStartSIMService;

    move-object/from16 v17, v0

    new-instance v18, Ljava/lang/StringBuilder;

    invoke-direct/range {v18 .. v18}, Ljava/lang/StringBuilder;-><init>()V

    const-string v19, "[deleteSimContact] simInfoReady: "

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    move-object/from16 v0, v18

    invoke-virtual {v0, v13}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v18

    invoke-virtual/range {v18 .. v18}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v18

    invoke-static/range {v17 .. v18}, Lcom/mediatek/contacts/simcontact/AbstractStartSIMService;->access$000(Lcom/mediatek/contacts/simcontact/AbstractStartSIMService;Ljava/lang/String;)V

    if-nez v13, :cond_0

    const-wide/16 v17, 0x3e8

    :try_start_0
    invoke-static/range {v17 .. v18}, Ljava/lang/Thread;->sleep(J)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    :goto_1
    invoke-static {}, Lcom/mediatek/contacts/simcontact/SimCardUtils;->isSimInfoReady()Z

    move-result v13

    add-int/lit8 v9, v9, -0x1

    goto :goto_0

    :catch_0
    move-exception v6

    const-string v17, "AbstractStartSIMService"

    const-string v18, "catched excepiotn."

    invoke-static/range {v17 .. v18}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1

    :cond_0
    const/4 v4, 0x0

    invoke-static {v2}, Landroid/provider/Telephony$SIMInfo;->getAllSIMList(Landroid/content/Context;)Ljava/util/List;

    move-result-object v14

    const/4 v11, 0x0

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    invoke-interface {v14}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v8

    :cond_1
    :goto_2
    invoke-interface {v8}, Ljava/util/Iterator;->hasNext()Z

    move-result v17

    if-eqz v17, :cond_3

    invoke-interface {v8}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v12

    check-cast v12, Landroid/provider/Telephony$SIMInfo;

    iget v0, v12, Landroid/provider/Telephony$SIMInfo;->mSlot:I

    move/from16 v17, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/mediatek/contacts/simcontact/AbstractStartSIMService$1;->val$slotId:I

    move/from16 v18, v0

    move/from16 v0, v17

    move/from16 v1, v18

    if-ne v0, v1, :cond_2

    iget-wide v0, v12, Landroid/provider/Telephony$SIMInfo;->mSimId:J

    move-wide/from16 v17, v0

    move-wide/from16 v0, v17

    long-to-int v4, v0

    :cond_2
    iget v0, v12, Landroid/provider/Telephony$SIMInfo;->mSlot:I

    move/from16 v17, v0

    const/16 v18, -0x1

    move/from16 v0, v17

    move/from16 v1, v18

    if-ne v0, v1, :cond_1

    iget-wide v0, v12, Landroid/provider/Telephony$SIMInfo;->mSimId:J

    move-wide/from16 v17, v0

    move-wide/from16 v0, v17

    invoke-virtual {v5, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v17

    const-string v18, ","

    invoke-virtual/range {v17 .. v18}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_2

    :cond_3
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/mediatek/contacts/simcontact/AbstractStartSIMService$1;->val$workData:Lcom/mediatek/contacts/simcontact/AbstractStartSIMService$ServiceWorkData;

    move-object/from16 v17, v0

    move-object/from16 v0, v17

    iput v4, v0, Lcom/mediatek/contacts/simcontact/AbstractStartSIMService$ServiceWorkData;->mSimId:I

    if-lez v4, :cond_4

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/mediatek/contacts/simcontact/AbstractStartSIMService$1;->val$workData:Lcom/mediatek/contacts/simcontact/AbstractStartSIMService$ServiceWorkData;

    move-object/from16 v17, v0

    move-object/from16 v0, v17

    iget v0, v0, Lcom/mediatek/contacts/simcontact/AbstractStartSIMService$ServiceWorkData;->mSimType:I

    move/from16 v17, v0

    const/16 v18, -0x1

    move/from16 v0, v17

    move/from16 v1, v18

    if-ne v0, v1, :cond_4

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/mediatek/contacts/simcontact/AbstractStartSIMService$1;->val$workData:Lcom/mediatek/contacts/simcontact/AbstractStartSIMService$ServiceWorkData;

    move-object/from16 v17, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/mediatek/contacts/simcontact/AbstractStartSIMService$1;->val$slotId:I

    move/from16 v18, v0

    invoke-static/range {v18 .. v18}, Lcom/mediatek/contacts/simcontact/SimCardUtils;->getSimTypeBySlot(I)I

    move-result v18

    move/from16 v0, v18

    move-object/from16 v1, v17

    iput v0, v1, Lcom/mediatek/contacts/simcontact/AbstractStartSIMService$ServiceWorkData;->mSimType:I

    :cond_4
    invoke-virtual {v5}, Ljava/lang/StringBuilder;->length()I

    move-result v17

    if-lez v17, :cond_5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->length()I

    move-result v17

    add-int/lit8 v17, v17, -0x1

    move/from16 v0, v17

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->deleteCharAt(I)Ljava/lang/StringBuilder;

    :cond_5
    if-nez v4, :cond_b

    move-object/from16 v0, p0

    iget v0, v0, Lcom/mediatek/contacts/simcontact/AbstractStartSIMService$1;->val$slotId:I

    move/from16 v17, v0

    if-ltz v17, :cond_b

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->length()I

    move-result v17

    if-lez v17, :cond_6

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    :cond_6
    :goto_3
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/mediatek/contacts/simcontact/AbstractStartSIMService$1;->this$0:Lcom/mediatek/contacts/simcontact/AbstractStartSIMService;

    move-object/from16 v17, v0

    new-instance v18, Ljava/lang/StringBuilder;

    invoke-direct/range {v18 .. v18}, Ljava/lang/StringBuilder;-><init>()V

    const-string v19, "[deleteSimContact]slotId:"

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    move-object/from16 v0, p0

    iget v0, v0, Lcom/mediatek/contacts/simcontact/AbstractStartSIMService$1;->val$slotId:I

    move/from16 v19, v0

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v18

    const-string v19, "|selection:"

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    move-object/from16 v0, v18

    invoke-virtual {v0, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    invoke-virtual/range {v18 .. v18}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v18

    invoke-static/range {v17 .. v18}, Lcom/mediatek/contacts/simcontact/AbstractStartSIMService;->access$000(Lcom/mediatek/contacts/simcontact/AbstractStartSIMService;Ljava/lang/String;)V

    invoke-static {v11}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v17

    if-nez v17, :cond_7

    invoke-virtual {v2}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v17

    sget-object v18, Landroid/provider/ContactsContract$RawContacts;->CONTENT_URI:Landroid/net/Uri;

    invoke-virtual/range {v18 .. v18}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v18

    const-string v19, "sim"

    const-string v20, "true"

    invoke-virtual/range {v18 .. v20}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v18

    invoke-virtual/range {v18 .. v18}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v18

    new-instance v19, Ljava/lang/StringBuilder;

    invoke-direct/range {v19 .. v19}, Ljava/lang/StringBuilder;-><init>()V

    const-string v20, "indicate_phone_or_sim_contact IN ("

    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    move-object/from16 v0, v19

    invoke-virtual {v0, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    const-string v20, ")"

    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    invoke-virtual/range {v19 .. v19}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v19

    const/16 v20, 0x0

    invoke-virtual/range {v17 .. v20}, Landroid/content/ContentResolver;->delete(Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v3

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/mediatek/contacts/simcontact/AbstractStartSIMService$1;->this$0:Lcom/mediatek/contacts/simcontact/AbstractStartSIMService;

    move-object/from16 v17, v0

    new-instance v18, Ljava/lang/StringBuilder;

    invoke-direct/range {v18 .. v18}, Ljava/lang/StringBuilder;-><init>()V

    const-string v19, "[deleteSimContact]slotId:"

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    move-object/from16 v0, p0

    iget v0, v0, Lcom/mediatek/contacts/simcontact/AbstractStartSIMService$1;->val$slotId:I

    move/from16 v19, v0

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v18

    const-string v19, "|count:"

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    move-object/from16 v0, v18

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v18

    invoke-virtual/range {v18 .. v18}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v18

    invoke-static/range {v17 .. v18}, Lcom/mediatek/contacts/simcontact/AbstractStartSIMService;->access$000(Lcom/mediatek/contacts/simcontact/AbstractStartSIMService;Ljava/lang/String;)V

    :cond_7
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/mediatek/contacts/simcontact/AbstractStartSIMService$1;->val$workData:Lcom/mediatek/contacts/simcontact/AbstractStartSIMService$ServiceWorkData;

    move-object/from16 v17, v0

    move-object/from16 v0, v17

    iget-object v0, v0, Lcom/mediatek/contacts/simcontact/AbstractStartSIMService$ServiceWorkData;->mSimCursor:Landroid/database/Cursor;

    move-object/from16 v17, v0

    if-eqz v17, :cond_8

    if-nez v4, :cond_9

    :cond_8
    invoke-virtual {v2}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v17

    sget-object v18, Landroid/provider/ContactsContract$Groups;->CONTENT_URI:Landroid/net/Uri;

    new-instance v19, Ljava/lang/StringBuilder;

    invoke-direct/range {v19 .. v19}, Ljava/lang/StringBuilder;-><init>()V

    const-string v20, "account_name=\'USIM"

    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    move-object/from16 v0, p0

    iget v0, v0, Lcom/mediatek/contacts/simcontact/AbstractStartSIMService$1;->val$slotId:I

    move/from16 v20, v0

    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v19

    const-string v20, "\' AND "

    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    const-string v20, "account_type"

    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    const-string v20, "=\'USIM Account\'"

    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    invoke-virtual/range {v19 .. v19}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v19

    const/16 v20, 0x0

    invoke-virtual/range {v17 .. v20}, Landroid/content/ContentResolver;->delete(Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)I

    :cond_9
    invoke-static {}, Lcom/mediatek/contacts/simcontact/SlotUtils;->getAllSlotIds()Ljava/util/List;

    move-result-object v17

    invoke-interface/range {v17 .. v17}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v8

    :cond_a
    :goto_4
    invoke-interface {v8}, Ljava/util/Iterator;->hasNext()Z

    move-result v17

    if-eqz v17, :cond_d

    invoke-interface {v8}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v17

    check-cast v17, Ljava/lang/Integer;

    invoke-virtual/range {v17 .. v17}, Ljava/lang/Integer;->intValue()I

    move-result v10

    move-object/from16 v0, p0

    iget v0, v0, Lcom/mediatek/contacts/simcontact/AbstractStartSIMService$1;->val$slotId:I

    move/from16 v17, v0

    move/from16 v0, v17

    if-eq v10, v0, :cond_a

    invoke-static {v10}, Lcom/mediatek/contacts/simcontact/SimCardUtils;->isSimInserted(I)Z

    move-result v17

    if-nez v17, :cond_a

    invoke-virtual {v2}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v17

    sget-object v18, Landroid/provider/ContactsContract$Groups;->CONTENT_URI:Landroid/net/Uri;

    new-instance v19, Ljava/lang/StringBuilder;

    invoke-direct/range {v19 .. v19}, Ljava/lang/StringBuilder;-><init>()V

    const-string v20, "account_name=\'USIM"

    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    move-object/from16 v0, v19

    invoke-virtual {v0, v10}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v19

    const-string v20, "\' AND "

    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    const-string v20, "account_type"

    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    const-string v20, "=\'USIM Account\'"

    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    invoke-virtual/range {v19 .. v19}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v19

    const/16 v20, 0x0

    invoke-virtual/range {v17 .. v20}, Landroid/content/ContentResolver;->delete(Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)I

    goto :goto_4

    :cond_b
    new-instance v17, Ljava/lang/StringBuilder;

    invoke-direct/range {v17 .. v17}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, v17

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v18

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->length()I

    move-result v17

    if-lez v17, :cond_c

    new-instance v17, Ljava/lang/StringBuilder;

    invoke-direct/range {v17 .. v17}, Ljava/lang/StringBuilder;-><init>()V

    const-string v19, ","

    move-object/from16 v0, v17

    move-object/from16 v1, v19

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v17

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v19

    move-object/from16 v0, v17

    move-object/from16 v1, v19

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v17

    invoke-virtual/range {v17 .. v17}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v17

    :goto_5
    move-object/from16 v0, v18

    move-object/from16 v1, v17

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v17

    invoke-virtual/range {v17 .. v17}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    goto/16 :goto_3

    :cond_c
    const-string v17, ""

    goto :goto_5

    :cond_d
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/mediatek/contacts/simcontact/AbstractStartSIMService$1;->this$0:Lcom/mediatek/contacts/simcontact/AbstractStartSIMService;

    move-object/from16 v17, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/mediatek/contacts/simcontact/AbstractStartSIMService$1;->val$slotId:I

    move/from16 v18, v0

    invoke-virtual/range {v17 .. v18}, Lcom/mediatek/contacts/simcontact/AbstractStartSIMService;->checkSimState(I)Z

    move-result v17

    if-eqz v17, :cond_e

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/mediatek/contacts/simcontact/AbstractStartSIMService$1;->this$0:Lcom/mediatek/contacts/simcontact/AbstractStartSIMService;

    move-object/from16 v17, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/mediatek/contacts/simcontact/AbstractStartSIMService$1;->val$slotId:I

    move/from16 v18, v0

    invoke-virtual/range {v17 .. v18}, Lcom/mediatek/contacts/simcontact/AbstractStartSIMService;->checkPhoneBookState(I)Z

    move-result v17

    if-eqz v17, :cond_e

    const/4 v15, 0x1

    :goto_6
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/mediatek/contacts/simcontact/AbstractStartSIMService$1;->val$workData:Lcom/mediatek/contacts/simcontact/AbstractStartSIMService$ServiceWorkData;

    move-object/from16 v17, v0

    move-object/from16 v0, v17

    iget v0, v0, Lcom/mediatek/contacts/simcontact/AbstractStartSIMService$ServiceWorkData;->mWorkType:I

    move/from16 v16, v0

    const/16 v7, 0xa

    :goto_7
    if-lez v7, :cond_10

    if-nez v15, :cond_10

    const/16 v17, 0x1

    move/from16 v0, v16

    move/from16 v1, v17

    if-ne v0, v1, :cond_10

    const-wide/16 v17, 0x3e8

    :try_start_1
    invoke-static/range {v17 .. v18}, Ljava/lang/Thread;->sleep(J)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1

    :goto_8
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/mediatek/contacts/simcontact/AbstractStartSIMService$1;->this$0:Lcom/mediatek/contacts/simcontact/AbstractStartSIMService;

    move-object/from16 v17, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/mediatek/contacts/simcontact/AbstractStartSIMService$1;->val$slotId:I

    move/from16 v18, v0

    invoke-virtual/range {v17 .. v18}, Lcom/mediatek/contacts/simcontact/AbstractStartSIMService;->checkSimState(I)Z

    move-result v17

    if-eqz v17, :cond_f

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/mediatek/contacts/simcontact/AbstractStartSIMService$1;->this$0:Lcom/mediatek/contacts/simcontact/AbstractStartSIMService;

    move-object/from16 v17, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/mediatek/contacts/simcontact/AbstractStartSIMService$1;->val$slotId:I

    move/from16 v18, v0

    invoke-virtual/range {v17 .. v18}, Lcom/mediatek/contacts/simcontact/AbstractStartSIMService;->checkPhoneBookState(I)Z

    move-result v17

    if-eqz v17, :cond_f

    const/4 v15, 0x1

    :goto_9
    add-int/lit8 v7, v7, -0x1

    goto :goto_7

    :cond_e
    const/4 v15, 0x0

    goto :goto_6

    :catch_1
    move-exception v6

    const-string v17, "AbstractStartSIMService"

    const-string v18, "catched excepiotn."

    invoke-static/range {v17 .. v18}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_8

    :cond_f
    const/4 v15, 0x0

    goto :goto_9

    :cond_10
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/mediatek/contacts/simcontact/AbstractStartSIMService$1;->val$workData:Lcom/mediatek/contacts/simcontact/AbstractStartSIMService$ServiceWorkData;

    move-object/from16 v17, v0

    const/16 v18, -0x1

    move/from16 v0, v18

    move-object/from16 v1, v17

    iput v0, v1, Lcom/mediatek/contacts/simcontact/AbstractStartSIMService$ServiceWorkData;->mSimId:I

    if-eqz v15, :cond_11

    move-object/from16 v0, p0

    iget v0, v0, Lcom/mediatek/contacts/simcontact/AbstractStartSIMService$1;->val$slotId:I

    move/from16 v17, v0

    move/from16 v0, v17

    invoke-static {v2, v0}, Landroid/provider/Telephony$SIMInfo;->getSIMInfoBySlot(Landroid/content/Context;I)Landroid/provider/Telephony$SIMInfo;

    move-result-object v12

    if-eqz v12, :cond_11

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/mediatek/contacts/simcontact/AbstractStartSIMService$1;->val$workData:Lcom/mediatek/contacts/simcontact/AbstractStartSIMService$ServiceWorkData;

    move-object/from16 v17, v0

    iget-wide v0, v12, Landroid/provider/Telephony$SIMInfo;->mSimId:J

    move-wide/from16 v18, v0

    move-wide/from16 v0, v18

    long-to-int v0, v0

    move/from16 v18, v0

    move/from16 v0, v18

    move-object/from16 v1, v17

    iput v0, v1, Lcom/mediatek/contacts/simcontact/AbstractStartSIMService$ServiceWorkData;->mSimId:I

    :cond_11
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/mediatek/contacts/simcontact/AbstractStartSIMService$1;->this$0:Lcom/mediatek/contacts/simcontact/AbstractStartSIMService;

    move-object/from16 v17, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/mediatek/contacts/simcontact/AbstractStartSIMService$1;->val$workData:Lcom/mediatek/contacts/simcontact/AbstractStartSIMService$ServiceWorkData;

    move-object/from16 v18, v0

    invoke-static/range {v17 .. v18}, Lcom/mediatek/contacts/simcontact/AbstractStartSIMService;->access$100(Lcom/mediatek/contacts/simcontact/AbstractStartSIMService;Lcom/mediatek/contacts/simcontact/AbstractStartSIMService$ServiceWorkData;)V

    return-void
.end method
