.class Lcom/mediatek/contacts/simcontact/AbstractStartSIMService$SimHandler;
.super Landroid/os/Handler;
.source "AbstractStartSIMService.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/mediatek/contacts/simcontact/AbstractStartSIMService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "SimHandler"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/mediatek/contacts/simcontact/AbstractStartSIMService;


# direct methods
.method public constructor <init>(Lcom/mediatek/contacts/simcontact/AbstractStartSIMService;)V
    .locals 0

    iput-object p1, p0, Lcom/mediatek/contacts/simcontact/AbstractStartSIMService$SimHandler;->this$0:Lcom/mediatek/contacts/simcontact/AbstractStartSIMService;

    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 19
    .param p1    # Landroid/os/Message;

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/mediatek/contacts/simcontact/AbstractStartSIMService$SimHandler;->this$0:Lcom/mediatek/contacts/simcontact/AbstractStartSIMService;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "[handleMessage] msg.what = "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    move-object/from16 v0, p1

    iget v6, v0, Landroid/os/Message;->what:I

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, ", slotId = "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    move-object/from16 v0, p1

    iget v6, v0, Landroid/os/Message;->arg2:I

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/mediatek/contacts/simcontact/AbstractStartSIMService;->access$000(Lcom/mediatek/contacts/simcontact/AbstractStartSIMService;Ljava/lang/String;)V

    move-object/from16 v0, p1

    iget v3, v0, Landroid/os/Message;->arg2:I

    move-object/from16 v0, p1

    iget v4, v0, Landroid/os/Message;->what:I

    sparse-switch v4, :sswitch_data_0

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/mediatek/contacts/simcontact/AbstractStartSIMService$SimHandler;->this$0:Lcom/mediatek/contacts/simcontact/AbstractStartSIMService;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "[handleMessage] unknown msg.what received: "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    move-object/from16 v0, p1

    iget v6, v0, Landroid/os/Message;->what:I

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/mediatek/contacts/simcontact/AbstractStartSIMService;->access$000(Lcom/mediatek/contacts/simcontact/AbstractStartSIMService;Ljava/lang/String;)V

    :cond_0
    :goto_0
    return-void

    :sswitch_0
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/mediatek/contacts/simcontact/AbstractStartSIMService$SimHandler;->this$0:Lcom/mediatek/contacts/simcontact/AbstractStartSIMService;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "[handleMessage]process MSG SLOT"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, " || sServiceState:"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-static {v3}, Lcom/mediatek/contacts/simcontact/AbstractStartSIMService;->getServiceState(I)I

    move-result v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/mediatek/contacts/simcontact/AbstractStartSIMService;->access$000(Lcom/mediatek/contacts/simcontact/AbstractStartSIMService;Ljava/lang/String;)V

    move-object/from16 v0, p1

    iget-object v1, v0, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v1, Lcom/mediatek/contacts/simcontact/AbstractStartSIMService$ServiceWorkData;

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/mediatek/contacts/simcontact/AbstractStartSIMService$SimHandler;->this$0:Lcom/mediatek/contacts/simcontact/AbstractStartSIMService;

    invoke-static {v4, v1}, Lcom/mediatek/contacts/simcontact/AbstractStartSIMService;->access$700(Lcom/mediatek/contacts/simcontact/AbstractStartSIMService;Lcom/mediatek/contacts/simcontact/AbstractStartSIMService$ServiceWorkData;)V

    goto :goto_0

    :sswitch_1
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/mediatek/contacts/simcontact/AbstractStartSIMService$SimHandler;->this$0:Lcom/mediatek/contacts/simcontact/AbstractStartSIMService;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "[handleMessage]LOAD SLOT"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "|| sServiceState:"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-static {v3}, Lcom/mediatek/contacts/simcontact/AbstractStartSIMService;->getServiceState(I)I

    move-result v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/mediatek/contacts/simcontact/AbstractStartSIMService;->access$000(Lcom/mediatek/contacts/simcontact/AbstractStartSIMService;Ljava/lang/String;)V

    invoke-static {v3}, Lcom/mediatek/contacts/simcontact/AbstractStartSIMService;->getServiceState(I)I

    move-result v4

    const/4 v5, 0x2

    if-ge v4, v5, :cond_0

    const/4 v4, 0x2

    invoke-static {v3, v4}, Lcom/mediatek/contacts/simcontact/AbstractStartSIMService;->access$800(II)V

    move-object/from16 v0, p1

    iget-object v1, v0, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v1, Lcom/mediatek/contacts/simcontact/AbstractStartSIMService$ServiceWorkData;

    iget v0, v1, Lcom/mediatek/contacts/simcontact/AbstractStartSIMService$ServiceWorkData;->mWorkType:I

    move/from16 v18, v0

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/mediatek/contacts/simcontact/AbstractStartSIMService$SimHandler;->this$0:Lcom/mediatek/contacts/simcontact/AbstractStartSIMService;

    invoke-virtual {v4, v3}, Lcom/mediatek/contacts/simcontact/AbstractStartSIMService;->checkSimState(I)Z

    move-result v4

    if-eqz v4, :cond_1

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/mediatek/contacts/simcontact/AbstractStartSIMService$SimHandler;->this$0:Lcom/mediatek/contacts/simcontact/AbstractStartSIMService;

    invoke-virtual {v4, v3}, Lcom/mediatek/contacts/simcontact/AbstractStartSIMService;->checkPhoneBookState(I)Z

    move-result v4

    if-eqz v4, :cond_1

    const/16 v16, 0x1

    :goto_1
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/mediatek/contacts/simcontact/AbstractStartSIMService$SimHandler;->this$0:Lcom/mediatek/contacts/simcontact/AbstractStartSIMService;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "[handleMessage]LOAD SLOT"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "|| simStateReady:"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    move/from16 v0, v16

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/mediatek/contacts/simcontact/AbstractStartSIMService;->access$000(Lcom/mediatek/contacts/simcontact/AbstractStartSIMService;Ljava/lang/String;)V

    if-eqz v16, :cond_2

    const/4 v4, 0x1

    move/from16 v0, v18

    if-ne v0, v4, :cond_2

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/mediatek/contacts/simcontact/AbstractStartSIMService$SimHandler;->this$0:Lcom/mediatek/contacts/simcontact/AbstractStartSIMService;

    move-object/from16 v0, p1

    iget-object v4, v0, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v4, Lcom/mediatek/contacts/simcontact/AbstractStartSIMService$ServiceWorkData;

    invoke-static {v5, v4}, Lcom/mediatek/contacts/simcontact/AbstractStartSIMService;->access$900(Lcom/mediatek/contacts/simcontact/AbstractStartSIMService;Lcom/mediatek/contacts/simcontact/AbstractStartSIMService$ServiceWorkData;)V

    goto/16 :goto_0

    :cond_1
    const/16 v16, 0x0

    goto :goto_1

    :cond_2
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/mediatek/contacts/simcontact/AbstractStartSIMService$SimHandler;->this$0:Lcom/mediatek/contacts/simcontact/AbstractStartSIMService;

    invoke-static {v4}, Lcom/mediatek/contacts/simcontact/AbstractStartSIMService;->access$500(Lcom/mediatek/contacts/simcontact/AbstractStartSIMService;)Lcom/mediatek/contacts/simcontact/AbstractStartSIMService$SimHandler;

    move-result-object v4

    const/16 v5, 0x1f4

    invoke-virtual {v4, v5}, Landroid/os/Handler;->obtainMessage(I)Landroid/os/Message;

    move-result-object v13

    iput v3, v13, Landroid/os/Message;->arg2:I

    invoke-virtual {v13}, Landroid/os/Message;->sendToTarget()V

    goto/16 :goto_0

    :sswitch_2
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/mediatek/contacts/simcontact/AbstractStartSIMService$SimHandler;->this$0:Lcom/mediatek/contacts/simcontact/AbstractStartSIMService;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "[handleMessage]REMOVE SLOT"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "|| sServiceState:"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-static {v3}, Lcom/mediatek/contacts/simcontact/AbstractStartSIMService;->getServiceState(I)I

    move-result v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/mediatek/contacts/simcontact/AbstractStartSIMService;->access$000(Lcom/mediatek/contacts/simcontact/AbstractStartSIMService;Ljava/lang/String;)V

    invoke-static {v3}, Lcom/mediatek/contacts/simcontact/AbstractStartSIMService;->getServiceState(I)I

    move-result v4

    const/4 v5, 0x1

    if-ge v4, v5, :cond_0

    const/4 v4, 0x1

    invoke-static {v3, v4}, Lcom/mediatek/contacts/simcontact/AbstractStartSIMService;->access$800(II)V

    move-object/from16 v0, p1

    iget-object v1, v0, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v1, Lcom/mediatek/contacts/simcontact/AbstractStartSIMService$ServiceWorkData;

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/mediatek/contacts/simcontact/AbstractStartSIMService$SimHandler;->this$0:Lcom/mediatek/contacts/simcontact/AbstractStartSIMService;

    invoke-virtual {v4, v1}, Lcom/mediatek/contacts/simcontact/AbstractStartSIMService;->deleteSimContact(Lcom/mediatek/contacts/simcontact/AbstractStartSIMService$ServiceWorkData;)V

    goto/16 :goto_0

    :sswitch_3
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/mediatek/contacts/simcontact/AbstractStartSIMService$SimHandler;->this$0:Lcom/mediatek/contacts/simcontact/AbstractStartSIMService;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "[handleMessage]import SLOT"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "|| sServiceState:"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-static {v3}, Lcom/mediatek/contacts/simcontact/AbstractStartSIMService;->getServiceState(I)I

    move-result v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/mediatek/contacts/simcontact/AbstractStartSIMService;->access$000(Lcom/mediatek/contacts/simcontact/AbstractStartSIMService;Ljava/lang/String;)V

    invoke-static {v3}, Lcom/mediatek/contacts/simcontact/AbstractStartSIMService;->getServiceState(I)I

    move-result v4

    const/4 v5, 0x3

    if-ge v4, v5, :cond_0

    const/4 v4, 0x3

    invoke-static {v3, v4}, Lcom/mediatek/contacts/simcontact/AbstractStartSIMService;->access$800(II)V

    move-object/from16 v0, p1

    iget-object v1, v0, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v1, Lcom/mediatek/contacts/simcontact/AbstractStartSIMService$ServiceWorkData;

    new-instance v4, Lcom/mediatek/contacts/simcontact/AbstractStartSIMService$ImportAllSimContactsThread;

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/mediatek/contacts/simcontact/AbstractStartSIMService$SimHandler;->this$0:Lcom/mediatek/contacts/simcontact/AbstractStartSIMService;

    invoke-direct {v4, v5, v1}, Lcom/mediatek/contacts/simcontact/AbstractStartSIMService$ImportAllSimContactsThread;-><init>(Lcom/mediatek/contacts/simcontact/AbstractStartSIMService;Lcom/mediatek/contacts/simcontact/AbstractStartSIMService$ServiceWorkData;)V

    invoke-virtual {v4}, Ljava/lang/Thread;->start()V

    goto/16 :goto_0

    :sswitch_4
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/mediatek/contacts/simcontact/AbstractStartSIMService$SimHandler;->this$0:Lcom/mediatek/contacts/simcontact/AbstractStartSIMService;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "[handleMessage]finish SLOT"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "|| sServiceState:"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-static {v3}, Lcom/mediatek/contacts/simcontact/AbstractStartSIMService;->getServiceState(I)I

    move-result v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/mediatek/contacts/simcontact/AbstractStartSIMService;->access$000(Lcom/mediatek/contacts/simcontact/AbstractStartSIMService;Ljava/lang/String;)V

    const/4 v14, 0x1

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/mediatek/contacts/simcontact/AbstractStartSIMService$SimHandler;->this$0:Lcom/mediatek/contacts/simcontact/AbstractStartSIMService;

    invoke-static {v4}, Lcom/mediatek/contacts/simcontact/AbstractStartSIMService;->access$1000(Lcom/mediatek/contacts/simcontact/AbstractStartSIMService;)Landroid/util/SparseArray;

    move-result-object v4

    invoke-virtual {v4, v3}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v17

    check-cast v17, Ljava/util/ArrayList;

    invoke-virtual/range {v17 .. v17}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v4

    if-nez v4, :cond_4

    const-string v4, "AbstractStartSIMService"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "SLOT"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, " simWorkQueue: "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual/range {v17 .. v17}, Ljava/util/AbstractCollection;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    invoke-virtual/range {v17 .. v17}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v4

    if-eqz v4, :cond_8

    const/4 v9, -0x1

    :goto_2
    const-string v4, "AbstractStartSIMService"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "SLOT"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, " DoneWorkType: "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v9}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v4, 0x1

    if-ne v9, v4, :cond_3

    invoke-static {v3}, Lcom/mediatek/contacts/simcontact/AbstractStartSIMService;->getServiceState(I)I

    move-result v4

    const/4 v5, 0x3

    if-ne v4, v5, :cond_3

    const/4 v4, 0x0

    invoke-static {v3, v4}, Lcom/mediatek/contacts/simcontact/SlotUtils;->updateSimServiceRunningStateForSlot(IZ)V

    :cond_3
    invoke-virtual/range {v17 .. v17}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v4

    if-nez v4, :cond_4

    invoke-virtual/range {v17 .. v17}, Ljava/util/ArrayList;->size()I

    move-result v4

    add-int/lit8 v4, v4, -0x1

    move-object/from16 v0, v17

    invoke-virtual {v0, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/Integer;

    invoke-virtual {v4}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-virtual/range {v17 .. v17}, Ljava/util/ArrayList;->clear()V

    const-string v4, "AbstractStartSIMService"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "SLOT"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, " LastWorkType: "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    if-eq v9, v2, :cond_4

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    move-object/from16 v0, v17

    invoke-virtual {v0, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    new-instance v1, Lcom/mediatek/contacts/simcontact/AbstractStartSIMService$ServiceWorkData;

    const/4 v4, -0x1

    const/4 v5, -0x1

    const/4 v6, 0x0

    const/4 v7, 0x0

    invoke-direct/range {v1 .. v7}, Lcom/mediatek/contacts/simcontact/AbstractStartSIMService$ServiceWorkData;-><init>(IIIILandroid/database/Cursor;I)V

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/mediatek/contacts/simcontact/AbstractStartSIMService$SimHandler;->this$0:Lcom/mediatek/contacts/simcontact/AbstractStartSIMService;

    invoke-static {v4}, Lcom/mediatek/contacts/simcontact/AbstractStartSIMService;->access$500(Lcom/mediatek/contacts/simcontact/AbstractStartSIMService;)Lcom/mediatek/contacts/simcontact/AbstractStartSIMService$SimHandler;

    move-result-object v4

    const/16 v5, 0x64

    invoke-virtual {v4, v5}, Landroid/os/Handler;->obtainMessage(I)Landroid/os/Message;

    move-result-object v15

    iput v2, v15, Landroid/os/Message;->arg1:I

    iput v3, v15, Landroid/os/Message;->arg2:I

    iput-object v1, v15, Landroid/os/Message;->obj:Ljava/lang/Object;

    invoke-virtual {v15}, Landroid/os/Message;->sendToTarget()V

    const/4 v14, 0x0

    const/4 v4, 0x1

    invoke-static {v3, v4}, Lcom/mediatek/contacts/simcontact/SlotUtils;->updateSimServiceRunningStateForSlot(IZ)V

    :cond_4
    const-string v4, "AbstractStartSIMService"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "SLOT"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, " reallyFinished: "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v14}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v4, 0x0

    invoke-static {v3, v4}, Lcom/mediatek/contacts/simcontact/AbstractStartSIMService;->access$800(II)V

    if-eqz v14, :cond_7

    new-instance v12, Landroid/content/Intent;

    const-string v4, "com.android.contacts.ACTION_PHB_LOAD_FINISHED"

    invoke-direct {v12, v4}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    const-string v4, "simId"

    invoke-virtual {v12, v4, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    const-string v4, "slotId"

    invoke-virtual {v12, v4, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/mediatek/contacts/simcontact/AbstractStartSIMService$SimHandler;->this$0:Lcom/mediatek/contacts/simcontact/AbstractStartSIMService;

    invoke-virtual {v4, v12}, Landroid/content/ContextWrapper;->sendBroadcast(Landroid/content/Intent;)V

    const/4 v8, 0x1

    invoke-static {}, Lcom/mediatek/contacts/simcontact/SlotUtils;->getAllSlotIds()Ljava/util/List;

    move-result-object v4

    invoke-interface {v4}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v10

    :cond_5
    invoke-interface {v10}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_6

    invoke-interface {v10}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/Integer;

    invoke-virtual {v4}, Ljava/lang/Integer;->intValue()I

    move-result v11

    invoke-static {v11}, Lcom/mediatek/contacts/simcontact/AbstractStartSIMService;->isServiceRunning(I)Z

    move-result v4

    if-eqz v4, :cond_5

    const/4 v8, 0x0

    :cond_6
    if-eqz v8, :cond_7

    const-string v4, "AbstractStartSIMService"

    const-string v5, "Will call stopSelf here."

    invoke-static {v4, v5}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/mediatek/contacts/simcontact/AbstractStartSIMService$SimHandler;->this$0:Lcom/mediatek/contacts/simcontact/AbstractStartSIMService;

    invoke-virtual {v4}, Landroid/app/Service;->stopSelf()V

    :cond_7
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/mediatek/contacts/simcontact/AbstractStartSIMService$SimHandler;->this$0:Lcom/mediatek/contacts/simcontact/AbstractStartSIMService;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "After stopSelf SLOT"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/mediatek/contacts/simcontact/AbstractStartSIMService;->access$000(Lcom/mediatek/contacts/simcontact/AbstractStartSIMService;Ljava/lang/String;)V

    goto/16 :goto_0

    :cond_8
    const/4 v4, 0x0

    move-object/from16 v0, v17

    invoke-virtual {v0, v4}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/Integer;

    invoke-virtual {v4}, Ljava/lang/Integer;->intValue()I

    move-result v9

    goto/16 :goto_2

    :sswitch_data_0
    .sparse-switch
        0x64 -> :sswitch_0
        0xc8 -> :sswitch_1
        0x12c -> :sswitch_2
        0x190 -> :sswitch_3
        0x1f4 -> :sswitch_4
    .end sparse-switch
.end method
