.class public abstract Lcom/mediatek/contacts/list/AbstractPickerFragment;
.super Lcom/android/contacts/list/ContactEntryListFragment;
.source "AbstractPickerFragment.java"

# interfaces
.implements Lcom/mediatek/contacts/list/ContactListMultiChoiceListener;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/android/contacts/list/ContactEntryListFragment",
        "<",
        "Lcom/android/contacts/list/ContactEntryListAdapter;",
        ">;",
        "Lcom/mediatek/contacts/list/ContactListMultiChoiceListener;"
    }
.end annotation


# static fields
.field public static final ALLOWED_ITEMS_MAX:I = 0xdac

.field private static final KEY_CHECKEDIDS:Ljava/lang/String; = "checkedids"

.field private static final TAG:Ljava/lang/String;


# instance fields
.field private mAccountFilterHeader:Landroid/view/View;

.field private mCheckedIds:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation
.end field

.field private mEmptyView:Landroid/widget/TextView;

.field private mIsSelectedAll:Z

.field private mIsSelectedNone:Z

.field private mPushedIds:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation
.end field

.field private mSearchString:Ljava/lang/String;

.field private mSlectedItemsFormater:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const-class v0, Lcom/mediatek/contacts/list/AbstractPickerFragment;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/mediatek/contacts/list/AbstractPickerFragment;->TAG:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Lcom/android/contacts/list/ContactEntryListFragment;-><init>()V

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/mediatek/contacts/list/AbstractPickerFragment;->mIsSelectedAll:Z

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/mediatek/contacts/list/AbstractPickerFragment;->mIsSelectedNone:Z

    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, Lcom/mediatek/contacts/list/AbstractPickerFragment;->mCheckedIds:Ljava/util/Set;

    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, Lcom/mediatek/contacts/list/AbstractPickerFragment;->mPushedIds:Ljava/util/Set;

    return-void
.end method

.method private static convertSetToPrimitive(Ljava/util/Set;)[J
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Set",
            "<",
            "Ljava/lang/Long;",
            ">;)[J"
        }
    .end annotation

    if-nez p0, :cond_1

    const/4 v5, 0x0

    :cond_0
    return-object v5

    :cond_1
    invoke-interface {p0}, Ljava/util/Set;->size()I

    move-result v0

    new-array v5, v0, [J

    const/4 v3, 0x0

    invoke-interface {p0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v6

    if-eqz v6, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Long;

    if-ge v3, v0, :cond_0

    add-int/lit8 v4, v3, 0x1

    invoke-virtual {v2}, Ljava/lang/Long;->longValue()J

    move-result-wide v6

    aput-wide v6, v5, v3

    move v3, v4

    goto :goto_0
.end method

.method private setOkButtonStatus(Z)V
    .locals 3
    .param p1    # Z

    invoke-virtual {p0}, Landroid/app/Fragment;->getActivity()Landroid/app/Activity;

    move-result-object v1

    invoke-virtual {v1}, Landroid/app/Activity;->getActionBar()Landroid/app/ActionBar;

    move-result-object v1

    invoke-virtual {v1}, Landroid/app/ActionBar;->getCustomView()Landroid/view/View;

    move-result-object v1

    const v2, 0x7f07016b

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    if-eqz v0, :cond_0

    if-eqz p1, :cond_1

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setEnabled(Z)V

    const/4 v1, -0x1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTextColor(I)V

    :cond_0
    :goto_0
    return-void

    :cond_1
    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setEnabled(Z)V

    const v1, -0x333334

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTextColor(I)V

    goto :goto_0
.end method

.method private showSelectedCount(I)V
    .locals 5
    .param p1    # I

    invoke-virtual {p0}, Landroid/app/Fragment;->getActivity()Landroid/app/Activity;

    move-result-object v1

    invoke-virtual {v1}, Landroid/app/Activity;->getActionBar()Landroid/app/ActionBar;

    move-result-object v1

    invoke-virtual {v1}, Landroid/app/ActionBar;->getCustomView()Landroid/view/View;

    move-result-object v1

    const v2, 0x7f07009a

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    if-nez v0, :cond_0

    sget-object v1, Lcom/mediatek/contacts/list/AbstractPickerFragment;->TAG:Ljava/lang/String;

    const-string v2, "Load view resource error!"

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    :goto_0
    return-void

    :cond_0
    iget-object v1, p0, Lcom/mediatek/contacts/list/AbstractPickerFragment;->mSlectedItemsFormater:Ljava/lang/String;

    if-nez v1, :cond_1

    sget-object v1, Lcom/mediatek/contacts/list/AbstractPickerFragment;->TAG:Ljava/lang/String;

    const-string v2, "Load string resource error!"

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    :cond_1
    iget-object v1, p0, Lcom/mediatek/contacts/list/AbstractPickerFragment;->mSlectedItemsFormater:Ljava/lang/String;

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    invoke-static {p1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0
.end method

.method private updateCheckBoxState(Z)V
    .locals 7
    .param p1    # Z

    invoke-virtual {p0}, Lcom/android/contacts/list/ContactEntryListFragment;->getListView()Landroid/widget/ListView;

    move-result-object v4

    invoke-virtual {v4}, Landroid/widget/ListView;->getAdapter()Landroid/widget/ListAdapter;

    move-result-object v4

    invoke-interface {v4}, Landroid/widget/ListAdapter;->getCount()I

    move-result v0

    const-wide/16 v1, -0x1

    const/4 v3, 0x0

    :goto_0
    if-ge v3, v0, :cond_0

    if-eqz p1, :cond_3

    iget-object v4, p0, Lcom/mediatek/contacts/list/AbstractPickerFragment;->mCheckedIds:Ljava/util/Set;

    invoke-interface {v4}, Ljava/util/Set;->size()I

    move-result v4

    const/16 v5, 0xdac

    if-lt v4, v5, :cond_1

    invoke-virtual {p0}, Landroid/app/Fragment;->getActivity()Landroid/app/Activity;

    move-result-object v4

    const v5, 0x7f0c0090

    const/4 v6, 0x0

    invoke-static {v4, v5, v6}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v4

    invoke-virtual {v4}, Landroid/widget/Toast;->show()V

    invoke-virtual {p0}, Lcom/android/contacts/list/ContactEntryListFragment;->getListView()Landroid/widget/ListView;

    move-result-object v4

    const/16 v5, 0xdab

    invoke-virtual {v4, v5}, Landroid/widget/ListView;->setSelection(I)V

    :cond_0
    iget-object v4, p0, Lcom/mediatek/contacts/list/AbstractPickerFragment;->mCheckedIds:Ljava/util/Set;

    invoke-interface {v4}, Ljava/util/Set;->size()I

    move-result v4

    invoke-direct {p0, v4}, Lcom/mediatek/contacts/list/AbstractPickerFragment;->updateSelectedItemsView(I)V

    return-void

    :cond_1
    invoke-virtual {p0}, Lcom/android/contacts/list/ContactEntryListFragment;->getListView()Landroid/widget/ListView;

    move-result-object v4

    invoke-virtual {v4, v3}, Landroid/widget/AbsListView;->isItemChecked(I)Z

    move-result v4

    if-nez v4, :cond_2

    invoke-virtual {p0}, Lcom/android/contacts/list/ContactEntryListFragment;->getListView()Landroid/widget/ListView;

    move-result-object v4

    invoke-virtual {v4, v3, p1}, Landroid/widget/AbsListView;->setItemChecked(IZ)V

    invoke-virtual {p0, v3}, Lcom/mediatek/contacts/list/AbstractPickerFragment;->getListItemDataId(I)J

    move-result-wide v1

    iget-object v4, p0, Lcom/mediatek/contacts/list/AbstractPickerFragment;->mCheckedIds:Ljava/util/Set;

    invoke-static {v1, v2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v5

    invoke-interface {v4, v5}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    :cond_2
    :goto_1
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    :cond_3
    iget-object v4, p0, Lcom/mediatek/contacts/list/AbstractPickerFragment;->mCheckedIds:Ljava/util/Set;

    invoke-interface {v4}, Ljava/util/Set;->clear()V

    invoke-virtual {p0}, Lcom/android/contacts/list/ContactEntryListFragment;->getListView()Landroid/widget/ListView;

    move-result-object v4

    invoke-virtual {v4, v3, p1}, Landroid/widget/AbsListView;->setItemChecked(IZ)V

    goto :goto_1
.end method

.method private updateSelectedItemsView(I)V
    .locals 5
    .param p1    # I

    const/4 v1, 0x1

    const/4 v2, 0x0

    invoke-virtual {p0}, Landroid/app/Fragment;->getActivity()Landroid/app/Activity;

    move-result-object v3

    invoke-virtual {v3}, Landroid/app/Activity;->getActionBar()Landroid/app/ActionBar;

    move-result-object v3

    invoke-virtual {v3}, Landroid/app/ActionBar;->getCustomView()Landroid/view/View;

    move-result-object v3

    const v4, 0x7f07016b

    invoke-virtual {v3, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    if-nez p1, :cond_0

    iput-boolean v1, p0, Lcom/mediatek/contacts/list/AbstractPickerFragment;->mIsSelectedNone:Z

    :goto_0
    iget-boolean v3, p0, Lcom/mediatek/contacts/list/AbstractPickerFragment;->mIsSelectedNone:Z

    if-nez v3, :cond_1

    :goto_1
    invoke-direct {p0, v1}, Lcom/mediatek/contacts/list/AbstractPickerFragment;->setOkButtonStatus(Z)V

    invoke-virtual {p0}, Lcom/android/contacts/list/ContactEntryListFragment;->getAdapter()Lcom/android/contacts/list/ContactEntryListAdapter;

    move-result-object v1

    invoke-virtual {v1}, Lcom/android/contacts/list/ContactEntryListAdapter;->isSearchMode()Z

    move-result v1

    if-eqz v1, :cond_2

    sget-object v1, Lcom/mediatek/contacts/list/AbstractPickerFragment;->TAG:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "#updateSelectedItemsView(),isSearchMonde,don\'t showSelectedCount:"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/mediatek/contacts/util/LogUtils;->w(Ljava/lang/String;Ljava/lang/String;)V

    :goto_2
    return-void

    :cond_0
    iput-boolean v2, p0, Lcom/mediatek/contacts/list/AbstractPickerFragment;->mIsSelectedNone:Z

    goto :goto_0

    :cond_1
    move v1, v2

    goto :goto_1

    :cond_2
    invoke-direct {p0, p1}, Lcom/mediatek/contacts/list/AbstractPickerFragment;->showSelectedCount(I)V

    goto :goto_2
.end method


# virtual methods
.method protected configureAdapter()V
    .locals 7

    const/4 v6, 0x0

    const/4 v4, 0x1

    invoke-super {p0}, Lcom/android/contacts/list/ContactEntryListFragment;->configureAdapter()V

    invoke-virtual {p0}, Lcom/android/contacts/list/ContactEntryListFragment;->getAdapter()Lcom/android/contacts/list/ContactEntryListAdapter;

    move-result-object v0

    if-nez v0, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    invoke-virtual {v0, v4}, Lcom/android/contacts/list/ContactEntryListAdapter;->setEmptyListEnabled(Z)V

    invoke-virtual {v0, v4}, Lcom/android/contacts/widget/IndexerListAdapter;->setSectionHeaderDisplayEnabled(Z)V

    invoke-virtual {v0, v4}, Lcom/android/contacts/list/ContactEntryListAdapter;->setDisplayPhotos(Z)V

    invoke-virtual {v0, v6}, Lcom/android/contacts/list/ContactEntryListAdapter;->setQuickContactEnabled(Z)V

    invoke-super {p0, v4}, Lcom/android/contacts/list/ContactEntryListFragment;->setPhotoLoaderEnabled(Z)V

    iget-object v4, p0, Lcom/mediatek/contacts/list/AbstractPickerFragment;->mSearchString:Ljava/lang/String;

    invoke-virtual {v0, v4}, Lcom/android/contacts/list/ContactEntryListAdapter;->setQueryString(Ljava/lang/String;)V

    invoke-virtual {v0, v6}, Lcom/android/contacts/list/ContactEntryListAdapter;->setIncludeProfile(Z)V

    iget-object v4, p0, Lcom/mediatek/contacts/list/AbstractPickerFragment;->mAccountFilterHeader:Landroid/view/View;

    if-eqz v4, :cond_0

    iget-object v4, p0, Lcom/mediatek/contacts/list/AbstractPickerFragment;->mAccountFilterHeader:Landroid/view/View;

    const v5, 0x7f070030

    invoke-virtual {v4, v5}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    invoke-virtual {p0}, Lcom/android/contacts/list/ContactEntryListFragment;->getContext()Landroid/content/Context;

    move-result-object v4

    invoke-virtual {v4}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/res/Resources;->getThemeMainColor()I

    move-result v3

    if-eqz v3, :cond_2

    invoke-virtual {v1, v3}, Landroid/widget/TextView;->setTextColor(I)V

    :cond_2
    if-eqz v1, :cond_0

    const v4, 0x7f0c027c

    invoke-virtual {v1, v4}, Landroid/widget/TextView;->setText(I)V

    iget-object v4, p0, Lcom/mediatek/contacts/list/AbstractPickerFragment;->mAccountFilterHeader:Landroid/view/View;

    invoke-virtual {v4, v6}, Landroid/view/View;->setVisibility(I)V

    goto :goto_0
.end method

.method public getCheckedItemIds()[J
    .locals 1

    iget-object v0, p0, Lcom/mediatek/contacts/list/AbstractPickerFragment;->mCheckedIds:Ljava/util/Set;

    invoke-static {v0}, Lcom/mediatek/contacts/list/AbstractPickerFragment;->convertSetToPrimitive(Ljava/util/Set;)[J

    move-result-object v0

    return-object v0
.end method

.method public abstract getListItemDataId(I)J
.end method

.method public handleCursorItem(Landroid/database/Cursor;)V
    .locals 0
    .param p1    # Landroid/database/Cursor;

    return-void
.end method

.method protected inflateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 2
    .param p1    # Landroid/view/LayoutInflater;
    .param p2    # Landroid/view/ViewGroup;

    const v0, 0x7f040095

    const/4 v1, 0x0

    invoke-virtual {p1, v0, v1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    return-object v0
.end method

.method public isSelectedAll()Z
    .locals 1

    iget-boolean v0, p0, Lcom/mediatek/contacts/list/AbstractPickerFragment;->mIsSelectedAll:Z

    return v0
.end method

.method public isSelectedNone()Z
    .locals 1

    iget-boolean v0, p0, Lcom/mediatek/contacts/list/AbstractPickerFragment;->mIsSelectedNone:Z

    return v0
.end method

.method public markItemsAsSelectedForCheckedGroups([J)V
    .locals 9
    .param p1    # [J

    new-instance v5, Ljava/util/HashSet;

    invoke-direct {v5}, Ljava/util/HashSet;-><init>()V

    iget-object v6, p0, Lcom/mediatek/contacts/list/AbstractPickerFragment;->mCheckedIds:Ljava/util/Set;

    invoke-interface {v5, v6}, Ljava/util/Set;->addAll(Ljava/util/Collection;)Z

    move-object v0, p1

    array-length v4, v0

    const/4 v1, 0x0

    :goto_0
    if-ge v1, v4, :cond_0

    aget-wide v2, v0, v1

    invoke-interface {v5}, Ljava/util/Set;->size()I

    move-result v6

    const/16 v7, 0xdac

    if-lt v6, v7, :cond_1

    invoke-virtual {p0}, Landroid/app/Fragment;->getActivity()Landroid/app/Activity;

    move-result-object v6

    const v7, 0x7f0c0090

    const/4 v8, 0x0

    invoke-static {v6, v7, v8}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v6

    invoke-virtual {v6}, Landroid/widget/Toast;->show()V

    :cond_0
    return-void

    :cond_1
    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v6

    invoke-interface {v5, v6}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    iget-object v6, p0, Lcom/mediatek/contacts/list/AbstractPickerFragment;->mPushedIds:Ljava/util/Set;

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v7

    invoke-interface {v6, v7}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    add-int/lit8 v1, v1, 0x1

    goto :goto_0
.end method

.method public onActivityCreated(Landroid/os/Bundle;)V
    .locals 2
    .param p1    # Landroid/os/Bundle;

    invoke-super {p0, p1}, Landroid/app/Fragment;->onActivityCreated(Landroid/os/Bundle;)V

    invoke-virtual {p0}, Landroid/app/Fragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    const v1, 0x7f0c003e

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/mediatek/contacts/list/AbstractPickerFragment;->mSlectedItemsFormater:Ljava/lang/String;

    iget-object v0, p0, Lcom/mediatek/contacts/list/AbstractPickerFragment;->mCheckedIds:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->size()I

    move-result v0

    invoke-direct {p0, v0}, Lcom/mediatek/contacts/list/AbstractPickerFragment;->showSelectedCount(I)V

    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/mediatek/contacts/list/AbstractPickerFragment;->setOkButtonStatus(Z)V

    invoke-virtual {p0}, Lcom/android/contacts/list/ContactEntryListFragment;->getListView()Landroid/widget/ListView;

    move-result-object v0

    const/4 v1, 0x2

    invoke-virtual {v0, v1}, Landroid/widget/AbsListView;->setChoiceMode(I)V

    return-void
.end method

.method public onClearSelect()V
    .locals 1

    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/mediatek/contacts/list/AbstractPickerFragment;->updateCheckBoxState(Z)V

    return-void
.end method

.method protected onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;)V
    .locals 2
    .param p1    # Landroid/view/LayoutInflater;
    .param p2    # Landroid/view/ViewGroup;

    invoke-super {p0, p1, p2}, Lcom/android/contacts/list/ContactEntryListFragment;->onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;)V

    invoke-virtual {p0}, Lcom/android/contacts/list/ContactEntryListFragment;->getView()Landroid/view/View;

    move-result-object v0

    const v1, 0x7f07002f

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/mediatek/contacts/list/AbstractPickerFragment;->mAccountFilterHeader:Landroid/view/View;

    invoke-virtual {p0}, Lcom/android/contacts/list/ContactEntryListFragment;->getView()Landroid/view/View;

    move-result-object v0

    const v1, 0x7f0700ef

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/mediatek/contacts/list/AbstractPickerFragment;->mEmptyView:Landroid/widget/TextView;

    iget-object v0, p0, Lcom/mediatek/contacts/list/AbstractPickerFragment;->mEmptyView:Landroid/widget/TextView;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/mediatek/contacts/list/AbstractPickerFragment;->mEmptyView:Landroid/widget/TextView;

    const v1, 0x7f0c0137

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    :cond_0
    return-void
.end method

.method protected onItemClick(IJ)V
    .locals 0
    .param p1    # I
    .param p2    # J

    return-void
.end method

.method public onItemClick(Landroid/widget/AdapterView;Landroid/view/View;IJ)V
    .locals 3
    .param p2    # Landroid/view/View;
    .param p3    # I
    .param p4    # J
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/widget/AdapterView",
            "<*>;",
            "Landroid/view/View;",
            "IJ)V"
        }
    .end annotation

    const/4 v2, 0x0

    sget-object v0, Lcom/mediatek/contacts/list/AbstractPickerFragment;->TAG:Ljava/lang/String;

    const-string v1, "onItemClick with adapterView"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    invoke-super/range {p0 .. p5}, Lcom/android/contacts/list/ContactEntryListFragment;->onItemClick(Landroid/widget/AdapterView;Landroid/view/View;IJ)V

    invoke-virtual {p0}, Lcom/android/contacts/list/ContactEntryListFragment;->getListView()Landroid/widget/ListView;

    move-result-object v0

    invoke-virtual {v0, p3}, Landroid/widget/AbsListView;->isItemChecked(I)Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/mediatek/contacts/list/AbstractPickerFragment;->mCheckedIds:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->size()I

    move-result v0

    const/16 v1, 0xdac

    if-lt v0, v1, :cond_0

    invoke-virtual {p0}, Landroid/app/Fragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    const v1, 0x7f0c0090

    invoke-static {v0, v1, v2}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    invoke-virtual {p0}, Lcom/android/contacts/list/ContactEntryListFragment;->getListView()Landroid/widget/ListView;

    move-result-object v0

    invoke-virtual {v0, p3, v2}, Landroid/widget/AbsListView;->setItemChecked(IZ)V

    invoke-virtual {p0}, Lcom/android/contacts/list/ContactEntryListFragment;->getListView()Landroid/widget/ListView;

    move-result-object v0

    const/16 v1, 0xdab

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setSelection(I)V

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/mediatek/contacts/list/AbstractPickerFragment;->mCheckedIds:Ljava/util/Set;

    invoke-static {p4, p5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    :goto_1
    invoke-virtual {p0}, Lcom/android/contacts/list/ContactEntryListFragment;->getListView()Landroid/widget/ListView;

    move-result-object v0

    invoke-virtual {p0}, Lcom/android/contacts/list/ContactEntryListFragment;->getListView()Landroid/widget/ListView;

    move-result-object v1

    invoke-virtual {v1, p3}, Landroid/widget/AbsListView;->isItemChecked(I)Z

    move-result v1

    invoke-virtual {v0, p3, v1}, Landroid/widget/AbsListView;->setItemChecked(IZ)V

    iget-object v0, p0, Lcom/mediatek/contacts/list/AbstractPickerFragment;->mCheckedIds:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->size()I

    move-result v0

    invoke-direct {p0, v0}, Lcom/mediatek/contacts/list/AbstractPickerFragment;->updateSelectedItemsView(I)V

    goto :goto_0

    :cond_1
    iget-object v0, p0, Lcom/mediatek/contacts/list/AbstractPickerFragment;->mCheckedIds:Ljava/util/Set;

    invoke-static {p4, p5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Set;->remove(Ljava/lang/Object;)Z

    goto :goto_1
.end method

.method public onLoadFinished(Landroid/content/Loader;Landroid/database/Cursor;)V
    .locals 13
    .param p2    # Landroid/database/Cursor;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Loader",
            "<",
            "Landroid/database/Cursor;",
            ">;",
            "Landroid/database/Cursor;",
            ")V"
        }
    .end annotation

    sget-object v11, Lcom/mediatek/contacts/list/AbstractPickerFragment;->TAG:Ljava/lang/String;

    const-string v12, "onLoadFinished"

    invoke-static {v11, v12}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    invoke-virtual {p0}, Lcom/android/contacts/list/ContactEntryListFragment;->getAdapter()Lcom/android/contacts/list/ContactEntryListAdapter;

    move-result-object v11

    invoke-virtual {v11}, Lcom/android/contacts/list/ContactEntryListAdapter;->isSearchMode()Z

    move-result v11

    if-eqz v11, :cond_3

    sget-object v11, Lcom/mediatek/contacts/list/AbstractPickerFragment;->TAG:Ljava/lang/String;

    const-string v12, "SearchMode"

    invoke-static {v11, v12}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    invoke-interface {p2}, Landroid/database/Cursor;->getExtras()Landroid/os/Bundle;

    move-result-object v11

    const-string v12, "non_filter_ids"

    invoke-virtual {v11, v12}, Landroid/os/Bundle;->getIntArray(Ljava/lang/String;)[I

    move-result-object v5

    if-eqz v5, :cond_2

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string v11, "onLoadFinished ids: "

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-object v0, v5

    array-length v7, v0

    const/4 v3, 0x0

    :goto_0
    if-ge v3, v7, :cond_0

    aget v4, v0, v3

    new-instance v11, Ljava/lang/StringBuilder;

    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static {v4}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v12

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    const-string v12, ","

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    :cond_0
    sget-object v11, Lcom/mediatek/contacts/list/AbstractPickerFragment;->TAG:Ljava/lang/String;

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v12

    invoke-static {v11, v12}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v11, p0, Lcom/mediatek/contacts/list/AbstractPickerFragment;->mCheckedIds:Ljava/util/Set;

    invoke-interface {v11}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v6

    :cond_1
    :goto_1
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v11

    if-eqz v11, :cond_2

    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/Long;

    invoke-virtual {v4}, Ljava/lang/Long;->intValue()I

    move-result v11

    invoke-static {v5, v11}, Ljava/util/Arrays;->binarySearch([II)I

    move-result v11

    if-gez v11, :cond_1

    invoke-interface {v6}, Ljava/util/Iterator;->remove()V

    goto :goto_1

    :cond_2
    invoke-virtual {p0}, Lcom/android/contacts/list/ContactEntryListFragment;->getListView()Landroid/widget/ListView;

    move-result-object v11

    const/4 v12, 0x0

    invoke-virtual {v11, v12}, Landroid/widget/AbsListView;->setFastScrollEnabled(Z)V

    invoke-virtual {p0}, Lcom/android/contacts/list/ContactEntryListFragment;->getListView()Landroid/widget/ListView;

    move-result-object v11

    const/4 v12, 0x0

    invoke-virtual {v11, v12}, Landroid/widget/AbsListView;->setFastScrollAlwaysVisible(Z)V

    :cond_3
    if-eqz p2, :cond_4

    if-eqz p2, :cond_a

    invoke-interface {p2}, Landroid/database/Cursor;->getCount()I

    move-result v11

    if-nez v11, :cond_a

    :cond_4
    iget-object v11, p0, Lcom/mediatek/contacts/list/AbstractPickerFragment;->mEmptyView:Landroid/widget/TextView;

    if-eqz v11, :cond_5

    invoke-virtual {p0}, Lcom/android/contacts/list/ContactEntryListFragment;->getAdapter()Lcom/android/contacts/list/ContactEntryListAdapter;

    move-result-object v11

    invoke-virtual {v11}, Lcom/android/contacts/list/ContactEntryListAdapter;->isSearchMode()Z

    move-result v11

    if-eqz v11, :cond_9

    iget-object v11, p0, Lcom/mediatek/contacts/list/AbstractPickerFragment;->mEmptyView:Landroid/widget/TextView;

    const v12, 0x7f0c014d

    invoke-virtual {v11, v12}, Landroid/widget/TextView;->setText(I)V

    :goto_2
    iget-object v11, p0, Lcom/mediatek/contacts/list/AbstractPickerFragment;->mEmptyView:Landroid/widget/TextView;

    const/4 v12, 0x0

    invoke-virtual {v11, v12}, Landroid/view/View;->setVisibility(I)V

    :cond_5
    invoke-virtual {p0}, Lcom/android/contacts/list/ContactEntryListFragment;->getListView()Landroid/widget/ListView;

    move-result-object v11

    const/4 v12, 0x0

    invoke-virtual {v11, v12}, Landroid/widget/AbsListView;->setFastScrollEnabled(Z)V

    invoke-virtual {p0}, Lcom/android/contacts/list/ContactEntryListFragment;->getListView()Landroid/widget/ListView;

    move-result-object v11

    const/4 v12, 0x0

    invoke-virtual {v11, v12}, Landroid/widget/AbsListView;->setFastScrollAlwaysVisible(Z)V

    :cond_6
    :goto_3
    invoke-virtual {p0}, Lcom/android/contacts/list/ContactEntryListFragment;->getListView()Landroid/widget/ListView;

    move-result-object v11

    invoke-virtual {v11}, Landroid/widget/AbsListView;->clearChoices()V

    new-instance v8, Ljava/util/HashSet;

    invoke-direct {v8}, Ljava/util/HashSet;-><init>()V

    const-wide/16 v1, -0x1

    const/4 v9, 0x0

    if-eqz p2, :cond_d

    const/4 v11, -0x1

    invoke-interface {p2, v11}, Landroid/database/Cursor;->moveToPosition(I)Z

    :goto_4
    invoke-interface {p2}, Landroid/database/Cursor;->moveToNext()Z

    move-result v11

    if-eqz v11, :cond_d

    const-wide/16 v1, -0x1

    const/4 v11, 0x0

    invoke-interface {p2, v11}, Landroid/database/Cursor;->getInt(I)I

    move-result v11

    int-to-long v1, v11

    invoke-static {v1, v2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v11

    invoke-interface {v8, v11}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    iget-object v11, p0, Lcom/mediatek/contacts/list/AbstractPickerFragment;->mCheckedIds:Ljava/util/Set;

    invoke-static {v1, v2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v12

    invoke-interface {v11, v12}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v11

    if-nez v11, :cond_7

    iget-object v11, p0, Lcom/mediatek/contacts/list/AbstractPickerFragment;->mPushedIds:Ljava/util/Set;

    invoke-static {v1, v2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v12

    invoke-interface {v11, v12}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_8

    :cond_7
    invoke-virtual {p0}, Lcom/android/contacts/list/ContactEntryListFragment;->getListView()Landroid/widget/ListView;

    move-result-object v11

    const/4 v12, 0x1

    invoke-virtual {v11, v9, v12}, Landroid/widget/AbsListView;->setItemChecked(IZ)V

    :cond_8
    add-int/lit8 v9, v9, 0x1

    invoke-virtual {p0, p2}, Lcom/mediatek/contacts/list/AbstractPickerFragment;->handleCursorItem(Landroid/database/Cursor;)V

    goto :goto_4

    :cond_9
    iget-object v11, p0, Lcom/mediatek/contacts/list/AbstractPickerFragment;->mEmptyView:Landroid/widget/TextView;

    const v12, 0x7f0c0137

    invoke-virtual {v11, v12}, Landroid/widget/TextView;->setText(I)V

    goto :goto_2

    :cond_a
    iget-object v11, p0, Lcom/mediatek/contacts/list/AbstractPickerFragment;->mEmptyView:Landroid/widget/TextView;

    if-eqz v11, :cond_b

    invoke-virtual {p0}, Lcom/android/contacts/list/ContactEntryListFragment;->getAdapter()Lcom/android/contacts/list/ContactEntryListAdapter;

    move-result-object v11

    invoke-virtual {v11}, Lcom/android/contacts/list/ContactEntryListAdapter;->isSearchMode()Z

    move-result v11

    if-eqz v11, :cond_c

    iget-object v11, p0, Lcom/mediatek/contacts/list/AbstractPickerFragment;->mEmptyView:Landroid/widget/TextView;

    const v12, 0x7f0c014d

    invoke-virtual {v11, v12}, Landroid/widget/TextView;->setText(I)V

    :goto_5
    iget-object v11, p0, Lcom/mediatek/contacts/list/AbstractPickerFragment;->mEmptyView:Landroid/widget/TextView;

    const/16 v12, 0x8

    invoke-virtual {v11, v12}, Landroid/view/View;->setVisibility(I)V

    :cond_b
    invoke-virtual {p0}, Lcom/android/contacts/list/ContactEntryListFragment;->getAdapter()Lcom/android/contacts/list/ContactEntryListAdapter;

    move-result-object v11

    invoke-virtual {v11}, Lcom/android/contacts/list/ContactEntryListAdapter;->isSearchMode()Z

    move-result v11

    if-nez v11, :cond_6

    invoke-virtual {p0}, Lcom/android/contacts/list/ContactEntryListFragment;->getListView()Landroid/widget/ListView;

    move-result-object v11

    const/4 v12, 0x1

    invoke-virtual {v11, v12}, Landroid/widget/AbsListView;->setFastScrollEnabled(Z)V

    invoke-virtual {p0}, Lcom/android/contacts/list/ContactEntryListFragment;->getListView()Landroid/widget/ListView;

    move-result-object v11

    const/4 v12, 0x1

    invoke-virtual {v11, v12}, Landroid/widget/AbsListView;->setFastScrollAlwaysVisible(Z)V

    goto/16 :goto_3

    :cond_c
    iget-object v11, p0, Lcom/mediatek/contacts/list/AbstractPickerFragment;->mEmptyView:Landroid/widget/TextView;

    const v12, 0x7f0c0137

    invoke-virtual {v11, v12}, Landroid/widget/TextView;->setText(I)V

    goto :goto_5

    :cond_d
    invoke-virtual {p0}, Lcom/android/contacts/list/ContactEntryListFragment;->getAdapter()Lcom/android/contacts/list/ContactEntryListAdapter;

    move-result-object v11

    invoke-virtual {v11}, Lcom/android/contacts/list/ContactEntryListAdapter;->isSearchMode()Z

    move-result v11

    if-nez v11, :cond_f

    iget-object v11, p0, Lcom/mediatek/contacts/list/AbstractPickerFragment;->mCheckedIds:Ljava/util/Set;

    invoke-interface {v11}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v6

    :cond_e
    :goto_6
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v11

    if-eqz v11, :cond_f

    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/Long;

    invoke-interface {v8, v4}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v11

    if-nez v11, :cond_e

    invoke-interface {v6}, Ljava/util/Iterator;->remove()V

    goto :goto_6

    :cond_f
    iget-object v11, p0, Lcom/mediatek/contacts/list/AbstractPickerFragment;->mPushedIds:Ljava/util/Set;

    invoke-interface {v11}, Ljava/util/Set;->isEmpty()Z

    move-result v11

    if-nez v11, :cond_10

    iget-object v11, p0, Lcom/mediatek/contacts/list/AbstractPickerFragment;->mCheckedIds:Ljava/util/Set;

    iget-object v12, p0, Lcom/mediatek/contacts/list/AbstractPickerFragment;->mPushedIds:Ljava/util/Set;

    invoke-interface {v11, v12}, Ljava/util/Set;->addAll(Ljava/util/Collection;)Z

    iget-object v11, p0, Lcom/mediatek/contacts/list/AbstractPickerFragment;->mPushedIds:Ljava/util/Set;

    invoke-interface {v11}, Ljava/util/Set;->clear()V

    :cond_10
    iget-object v11, p0, Lcom/mediatek/contacts/list/AbstractPickerFragment;->mCheckedIds:Ljava/util/Set;

    invoke-interface {v11}, Ljava/util/Set;->size()I

    move-result v11

    invoke-direct {p0, v11}, Lcom/mediatek/contacts/list/AbstractPickerFragment;->updateSelectedItemsView(I)V

    invoke-virtual {p0}, Lcom/android/contacts/list/ContactEntryListFragment;->clearListViewLastState()V

    invoke-super {p0, p1, p2}, Lcom/android/contacts/list/ContactEntryListFragment;->onLoadFinished(Landroid/content/Loader;Landroid/database/Cursor;)V

    return-void
.end method

.method public bridge synthetic onLoadFinished(Landroid/content/Loader;Ljava/lang/Object;)V
    .locals 0
    .param p1    # Landroid/content/Loader;
    .param p2    # Ljava/lang/Object;

    check-cast p2, Landroid/database/Cursor;

    invoke-virtual {p0, p1, p2}, Lcom/mediatek/contacts/list/AbstractPickerFragment;->onLoadFinished(Landroid/content/Loader;Landroid/database/Cursor;)V

    return-void
.end method

.method public onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 8
    .param p1    # Landroid/os/Bundle;

    invoke-super {p0, p1}, Lcom/android/contacts/list/ContactEntryListFragment;->onSaveInstanceState(Landroid/os/Bundle;)V

    iget-object v6, p0, Lcom/mediatek/contacts/list/AbstractPickerFragment;->mCheckedIds:Ljava/util/Set;

    invoke-interface {v6}, Ljava/util/Set;->size()I

    move-result v1

    new-array v0, v1, [J

    const/4 v4, 0x0

    iget-object v6, p0, Lcom/mediatek/contacts/list/AbstractPickerFragment;->mCheckedIds:Ljava/util/Set;

    invoke-interface {v6}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v6

    if-eqz v6, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Long;

    add-int/lit8 v5, v4, 0x1

    invoke-virtual {v3}, Ljava/lang/Long;->longValue()J

    move-result-wide v6

    aput-wide v6, v0, v4

    move v4, v5

    goto :goto_0

    :cond_0
    const-string v6, "checkedids"

    invoke-virtual {p1, v6, v0}, Landroid/os/Bundle;->putLongArray(Ljava/lang/String;[J)V

    return-void
.end method

.method public onSelectAll()V
    .locals 1

    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/mediatek/contacts/list/AbstractPickerFragment;->updateCheckBoxState(Z)V

    return-void
.end method

.method public restoreSavedState(Landroid/os/Bundle;)V
    .locals 6
    .param p1    # Landroid/os/Bundle;

    invoke-super {p0, p1}, Lcom/android/contacts/list/ContactEntryListFragment;->restoreSavedState(Landroid/os/Bundle;)V

    if-nez p1, :cond_1

    :cond_0
    return-void

    :cond_1
    iget-object v3, p0, Lcom/mediatek/contacts/list/AbstractPickerFragment;->mCheckedIds:Ljava/util/Set;

    if-nez v3, :cond_2

    new-instance v3, Ljava/util/HashSet;

    invoke-direct {v3}, Ljava/util/HashSet;-><init>()V

    iput-object v3, p0, Lcom/mediatek/contacts/list/AbstractPickerFragment;->mCheckedIds:Ljava/util/Set;

    :cond_2
    iget-object v3, p0, Lcom/mediatek/contacts/list/AbstractPickerFragment;->mCheckedIds:Ljava/util/Set;

    invoke-interface {v3}, Ljava/util/Set;->clear()V

    const-string v3, "checkedids"

    invoke-virtual {p1, v3}, Landroid/os/Bundle;->getLongArray(Ljava/lang/String;)[J

    move-result-object v1

    array-length v0, v1

    const/4 v2, 0x0

    :goto_0
    if-ge v2, v0, :cond_0

    iget-object v3, p0, Lcom/mediatek/contacts/list/AbstractPickerFragment;->mCheckedIds:Ljava/util/Set;

    aget-wide v4, v1, v2

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    invoke-interface {v3, v4}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    add-int/lit8 v2, v2, 0x1

    goto :goto_0
.end method

.method public startSearch(Ljava/lang/String;)V
    .locals 2
    .param p1    # Ljava/lang/String;

    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 p1, 0x0

    :cond_0
    invoke-virtual {p0}, Lcom/android/contacts/list/ContactEntryListFragment;->getAdapter()Lcom/android/contacts/list/ContactEntryListAdapter;

    move-result-object v0

    if-nez p1, :cond_2

    if-eqz v0, :cond_1

    const/4 v1, 0x0

    iput-object v1, p0, Lcom/mediatek/contacts/list/AbstractPickerFragment;->mSearchString:Ljava/lang/String;

    invoke-virtual {v0, p1}, Lcom/android/contacts/list/ContactEntryListAdapter;->setQueryString(Ljava/lang/String;)V

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/android/contacts/list/ContactEntryListAdapter;->setSearchMode(Z)V

    invoke-virtual {p0}, Lcom/android/contacts/list/ContactEntryListFragment;->reloadData()V

    :cond_1
    :goto_0
    return-void

    :cond_2
    iget-object v1, p0, Lcom/mediatek/contacts/list/AbstractPickerFragment;->mSearchString:Ljava/lang/String;

    invoke-static {v1, p1}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_1

    iput-object p1, p0, Lcom/mediatek/contacts/list/AbstractPickerFragment;->mSearchString:Ljava/lang/String;

    if-eqz v0, :cond_1

    invoke-virtual {v0, p1}, Lcom/android/contacts/list/ContactEntryListAdapter;->setQueryString(Ljava/lang/String;)V

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/android/contacts/list/ContactEntryListAdapter;->setSearchMode(Z)V

    invoke-virtual {p0}, Lcom/android/contacts/list/ContactEntryListFragment;->reloadData()V

    goto :goto_0
.end method

.method public updateSelectedItemsView()V
    .locals 4

    invoke-virtual {p0}, Lcom/android/contacts/list/ContactEntryListFragment;->getAdapter()Lcom/android/contacts/list/ContactEntryListAdapter;

    move-result-object v0

    invoke-virtual {p0}, Lcom/android/contacts/list/ContactEntryListFragment;->getListView()Landroid/widget/ListView;

    move-result-object v3

    invoke-virtual {v3}, Landroid/widget/ListView;->getAdapter()Landroid/widget/ListAdapter;

    move-result-object v3

    invoke-interface {v3}, Landroid/widget/ListAdapter;->getCount()I

    move-result v2

    iget-object v3, p0, Lcom/mediatek/contacts/list/AbstractPickerFragment;->mCheckedIds:Ljava/util/Set;

    invoke-interface {v3}, Ljava/util/Set;->size()I

    move-result v1

    invoke-direct {p0, v1}, Lcom/mediatek/contacts/list/AbstractPickerFragment;->updateSelectedItemsView(I)V

    invoke-virtual {p0}, Lcom/android/contacts/list/ContactEntryListFragment;->getListView()Landroid/widget/ListView;

    move-result-object v3

    invoke-virtual {v3}, Landroid/widget/AbsListView;->getCheckedItemCount()I

    move-result v3

    if-eq v2, v3, :cond_0

    const/16 v3, 0xdac

    if-lt v1, v3, :cond_1

    :cond_0
    const/4 v3, 0x1

    iput-boolean v3, p0, Lcom/mediatek/contacts/list/AbstractPickerFragment;->mIsSelectedAll:Z

    :goto_0
    return-void

    :cond_1
    const/4 v3, 0x0

    iput-boolean v3, p0, Lcom/mediatek/contacts/list/AbstractPickerFragment;->mIsSelectedAll:Z

    goto :goto_0
.end method
