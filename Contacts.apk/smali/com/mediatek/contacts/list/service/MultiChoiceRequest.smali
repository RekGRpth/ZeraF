.class public Lcom/mediatek/contacts/list/service/MultiChoiceRequest;
.super Ljava/lang/Object;
.source "MultiChoiceRequest.java"


# instance fields
.field public mAccountDst:Landroid/accounts/Account;

.field public mAccountSrc:Landroid/accounts/Account;

.field public mContactId:I

.field public mContactName:Ljava/lang/String;

.field public mIndicator:J

.field public mSimIndex:I

.field public mTargetAccount:Landroid/accounts/Account;


# direct methods
.method public constructor <init>(JIILjava/lang/String;)V
    .locals 0
    .param p1    # J
    .param p3    # I
    .param p4    # I
    .param p5    # Ljava/lang/String;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-wide p1, p0, Lcom/mediatek/contacts/list/service/MultiChoiceRequest;->mIndicator:J

    iput p3, p0, Lcom/mediatek/contacts/list/service/MultiChoiceRequest;->mSimIndex:I

    iput p4, p0, Lcom/mediatek/contacts/list/service/MultiChoiceRequest;->mContactId:I

    iput-object p5, p0, Lcom/mediatek/contacts/list/service/MultiChoiceRequest;->mContactName:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(JIILjava/lang/String;Landroid/accounts/Account;)V
    .locals 0
    .param p1    # J
    .param p3    # I
    .param p4    # I
    .param p5    # Ljava/lang/String;
    .param p6    # Landroid/accounts/Account;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-wide p1, p0, Lcom/mediatek/contacts/list/service/MultiChoiceRequest;->mIndicator:J

    iput p3, p0, Lcom/mediatek/contacts/list/service/MultiChoiceRequest;->mSimIndex:I

    iput p4, p0, Lcom/mediatek/contacts/list/service/MultiChoiceRequest;->mContactId:I

    iput-object p5, p0, Lcom/mediatek/contacts/list/service/MultiChoiceRequest;->mContactName:Ljava/lang/String;

    iput-object p6, p0, Lcom/mediatek/contacts/list/service/MultiChoiceRequest;->mTargetAccount:Landroid/accounts/Account;

    return-void
.end method

.method public constructor <init>(JIILjava/lang/String;Landroid/accounts/Account;Landroid/accounts/Account;)V
    .locals 0
    .param p1    # J
    .param p3    # I
    .param p4    # I
    .param p5    # Ljava/lang/String;
    .param p6    # Landroid/accounts/Account;
    .param p7    # Landroid/accounts/Account;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-wide p1, p0, Lcom/mediatek/contacts/list/service/MultiChoiceRequest;->mIndicator:J

    iput p3, p0, Lcom/mediatek/contacts/list/service/MultiChoiceRequest;->mSimIndex:I

    iput p4, p0, Lcom/mediatek/contacts/list/service/MultiChoiceRequest;->mContactId:I

    iput-object p5, p0, Lcom/mediatek/contacts/list/service/MultiChoiceRequest;->mContactName:Ljava/lang/String;

    iput-object p6, p0, Lcom/mediatek/contacts/list/service/MultiChoiceRequest;->mAccountSrc:Landroid/accounts/Account;

    iput-object p7, p0, Lcom/mediatek/contacts/list/service/MultiChoiceRequest;->mAccountDst:Landroid/accounts/Account;

    return-void
.end method
