.class public Lcom/mediatek/contacts/list/service/CopyProcessor;
.super Lcom/android/contacts/vcard/ProcessorBase;
.source "CopyProcessor.java"


# static fields
.field private static final DATA_ALLCOLUMNS:[Ljava/lang/String;

.field private static final DEBUG:Z = true

.field private static final LOG_TAG:Ljava/lang/String; = "CopyMultiContacts"

.field private static final MAX_OP_COUNT_IN_ONE_BATCH:I = 0x190

.field private static final RETRYCOUNT:I = 0x14


# instance fields
.field private final mAccountDst:Landroid/accounts/Account;

.field private final mAccountSrc:Landroid/accounts/Account;

.field private volatile mCanceled:Z

.field private volatile mDone:Z

.field private volatile mIsRunning:Z

.field private final mJobId:I

.field private final mListener:Lcom/mediatek/contacts/list/service/MultiChoiceHandlerListener;

.field private final mRequests:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/mediatek/contacts/list/service/MultiChoiceRequest;",
            ">;"
        }
    .end annotation
.end field

.field private final mResolver:Landroid/content/ContentResolver;

.field private final mService:Lcom/mediatek/contacts/list/service/MultiChoiceService;

.field private mWakeLock:Landroid/os/PowerManager$WakeLock;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    const/16 v0, 0x18

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v2, "_id"

    aput-object v2, v0, v1

    const/4 v1, 0x1

    const-string v2, "mimetype"

    aput-object v2, v0, v1

    const/4 v1, 0x2

    const-string v2, "is_primary"

    aput-object v2, v0, v1

    const/4 v1, 0x3

    const-string v2, "is_super_primary"

    aput-object v2, v0, v1

    const/4 v1, 0x4

    const-string v2, "data1"

    aput-object v2, v0, v1

    const/4 v1, 0x5

    const-string v2, "data2"

    aput-object v2, v0, v1

    const/4 v1, 0x6

    const-string v2, "data3"

    aput-object v2, v0, v1

    const/4 v1, 0x7

    const-string v2, "data4"

    aput-object v2, v0, v1

    const/16 v1, 0x8

    const-string v2, "data5"

    aput-object v2, v0, v1

    const/16 v1, 0x9

    const-string v2, "data6"

    aput-object v2, v0, v1

    const/16 v1, 0xa

    const-string v2, "data7"

    aput-object v2, v0, v1

    const/16 v1, 0xb

    const-string v2, "data8"

    aput-object v2, v0, v1

    const/16 v1, 0xc

    const-string v2, "data9"

    aput-object v2, v0, v1

    const/16 v1, 0xd

    const-string v2, "data10"

    aput-object v2, v0, v1

    const/16 v1, 0xe

    const-string v2, "data11"

    aput-object v2, v0, v1

    const/16 v1, 0xf

    const-string v2, "data12"

    aput-object v2, v0, v1

    const/16 v1, 0x10

    const-string v2, "data13"

    aput-object v2, v0, v1

    const/16 v1, 0x11

    const-string v2, "data14"

    aput-object v2, v0, v1

    const/16 v1, 0x12

    const-string v2, "data15"

    aput-object v2, v0, v1

    const/16 v1, 0x13

    const-string v2, "data_sync1"

    aput-object v2, v0, v1

    const/16 v1, 0x14

    const-string v2, "data_sync2"

    aput-object v2, v0, v1

    const/16 v1, 0x15

    const-string v2, "data_sync3"

    aput-object v2, v0, v1

    const/16 v1, 0x16

    const-string v2, "data_sync4"

    aput-object v2, v0, v1

    const/16 v1, 0x17

    const-string v2, "is_additional_number"

    aput-object v2, v0, v1

    sput-object v0, Lcom/mediatek/contacts/list/service/CopyProcessor;->DATA_ALLCOLUMNS:[Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Lcom/mediatek/contacts/list/service/MultiChoiceService;Lcom/mediatek/contacts/list/service/MultiChoiceHandlerListener;Ljava/util/List;ILandroid/accounts/Account;Landroid/accounts/Account;)V
    .locals 3
    .param p1    # Lcom/mediatek/contacts/list/service/MultiChoiceService;
    .param p2    # Lcom/mediatek/contacts/list/service/MultiChoiceHandlerListener;
    .param p4    # I
    .param p5    # Landroid/accounts/Account;
    .param p6    # Landroid/accounts/Account;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/mediatek/contacts/list/service/MultiChoiceService;",
            "Lcom/mediatek/contacts/list/service/MultiChoiceHandlerListener;",
            "Ljava/util/List",
            "<",
            "Lcom/mediatek/contacts/list/service/MultiChoiceRequest;",
            ">;I",
            "Landroid/accounts/Account;",
            "Landroid/accounts/Account;",
            ")V"
        }
    .end annotation

    invoke-direct {p0}, Lcom/android/contacts/vcard/ProcessorBase;-><init>()V

    iput-object p1, p0, Lcom/mediatek/contacts/list/service/CopyProcessor;->mService:Lcom/mediatek/contacts/list/service/MultiChoiceService;

    iget-object v1, p0, Lcom/mediatek/contacts/list/service/CopyProcessor;->mService:Lcom/mediatek/contacts/list/service/MultiChoiceService;

    invoke-virtual {v1}, Landroid/content/ContextWrapper;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    iput-object v1, p0, Lcom/mediatek/contacts/list/service/CopyProcessor;->mResolver:Landroid/content/ContentResolver;

    iput-object p2, p0, Lcom/mediatek/contacts/list/service/CopyProcessor;->mListener:Lcom/mediatek/contacts/list/service/MultiChoiceHandlerListener;

    iput-object p3, p0, Lcom/mediatek/contacts/list/service/CopyProcessor;->mRequests:Ljava/util/List;

    iput p4, p0, Lcom/mediatek/contacts/list/service/CopyProcessor;->mJobId:I

    iput-object p5, p0, Lcom/mediatek/contacts/list/service/CopyProcessor;->mAccountSrc:Landroid/accounts/Account;

    iput-object p6, p0, Lcom/mediatek/contacts/list/service/CopyProcessor;->mAccountDst:Landroid/accounts/Account;

    iget-object v1, p0, Lcom/mediatek/contacts/list/service/CopyProcessor;->mService:Lcom/mediatek/contacts/list/service/MultiChoiceService;

    invoke-virtual {v1}, Landroid/content/ContextWrapper;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    const-string v2, "power"

    invoke-virtual {v1, v2}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/os/PowerManager;

    const v1, 0x20000006

    const-string v2, "CopyMultiContacts"

    invoke-virtual {v0, v1, v2}, Landroid/os/PowerManager;->newWakeLock(ILjava/lang/String;)Landroid/os/PowerManager$WakeLock;

    move-result-object v1

    iput-object v1, p0, Lcom/mediatek/contacts/list/service/CopyProcessor;->mWakeLock:Landroid/os/PowerManager$WakeLock;

    return-void
.end method

.method private copyContactsToAccount()V
    .locals 29

    const-string v1, "CopyMultiContacts"

    const-string v2, "copyContactsToAccount"

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    move-object/from16 v0, p0

    iget-boolean v1, v0, Lcom/mediatek/contacts/list/service/CopyProcessor;->mCanceled:Z

    if-eqz v1, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    const/16 v17, 0x0

    const/16 v20, 0x0

    new-instance v28, Ljava/lang/StringBuilder;

    invoke-direct/range {v28 .. v28}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "("

    move-object/from16 v0, v28

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/mediatek/contacts/list/service/CopyProcessor;->mRequests:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v22

    :goto_1
    invoke-interface/range {v22 .. v22}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_2

    invoke-interface/range {v22 .. v22}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v27

    check-cast v27, Lcom/mediatek/contacts/list/service/MultiChoiceRequest;

    move-object/from16 v0, v27

    iget v1, v0, Lcom/mediatek/contacts/list/service/MultiChoiceRequest;->mContactId:I

    invoke-static {v1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v1

    move-object/from16 v0, v28

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, ","

    move-object/from16 v0, v28

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_1

    :cond_2
    invoke-virtual/range {v28 .. v28}, Ljava/lang/StringBuilder;->length()I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    move-object/from16 v0, v28

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->deleteCharAt(I)Ljava/lang/StringBuilder;

    const-string v1, ")"

    move-object/from16 v0, v28

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, "CopyMultiContacts"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "copyContactsToAccount contactIds "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual/range {v28 .. v28}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/mediatek/contacts/list/service/CopyProcessor;->mResolver:Landroid/content/ContentResolver;

    sget-object v2, Landroid/provider/ContactsContract$RawContacts;->CONTENT_URI:Landroid/net/Uri;

    const/4 v3, 0x2

    new-array v3, v3, [Ljava/lang/String;

    const/4 v7, 0x0

    const-string v13, "_id"

    aput-object v13, v3, v7

    const/4 v7, 0x1

    const-string v13, "display_name"

    aput-object v13, v3, v7

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v13, "contact_id IN "

    invoke-virtual {v7, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual/range {v28 .. v28}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v13

    invoke-virtual {v7, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    const/4 v5, 0x0

    const/4 v6, 0x0

    invoke-virtual/range {v1 .. v6}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v26

    if-nez v26, :cond_6

    const/4 v5, 0x0

    :goto_2
    new-instance v25, Ljava/util/ArrayList;

    invoke-direct/range {v25 .. v25}, Ljava/util/ArrayList;-><init>()V

    if-eqz v26, :cond_c

    const-string v1, "CopyMultiContacts"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "copyContactsToAccount: rawContactsCursor.size = "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-interface/range {v26 .. v26}, Landroid/database/Cursor;->getCount()I

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    move/from16 v4, v20

    :cond_3
    :goto_3
    invoke-interface/range {v26 .. v26}, Landroid/database/Cursor;->moveToNext()Z

    move-result v1

    if-eqz v1, :cond_4

    move-object/from16 v0, p0

    iget-boolean v1, v0, Lcom/mediatek/contacts/list/service/CopyProcessor;->mCanceled:Z

    if-eqz v1, :cond_7

    const-string v1, "CopyMultiContacts"

    const-string v2, "runInternal run: mCanceled = true"

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_4
    invoke-interface/range {v26 .. v26}, Landroid/database/Cursor;->close()V

    invoke-virtual/range {v25 .. v25}, Ljava/util/ArrayList;->size()I

    move-result v1

    if-lez v1, :cond_5

    :try_start_0
    const-string v1, "CopyMultiContacts"

    const-string v2, "Before end applyBatch. "

    invoke-static {v1, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/mediatek/contacts/list/service/CopyProcessor;->mResolver:Landroid/content/ContentResolver;

    const-string v2, "com.android.contacts"

    move-object/from16 v0, v25

    invoke-virtual {v1, v2, v0}, Landroid/content/ContentResolver;->applyBatch(Ljava/lang/String;Ljava/util/ArrayList;)[Landroid/content/ContentProviderResult;

    const-string v1, "CopyMultiContacts"

    const-string v2, "After end applyBatch "

    invoke-static {v1, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_2
    .catch Landroid/content/OperationApplicationException; {:try_start_0 .. :try_end_0} :catch_3

    :goto_4
    invoke-virtual/range {v25 .. v25}, Ljava/util/ArrayList;->clear()V

    :cond_5
    move-object/from16 v0, p0

    iget-boolean v1, v0, Lcom/mediatek/contacts/list/service/CopyProcessor;->mCanceled:Z

    if-eqz v1, :cond_d

    const-string v1, "CopyMultiContacts"

    const-string v2, "runInternal run: mCanceled = true"

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/mediatek/contacts/list/service/CopyProcessor;->mService:Lcom/mediatek/contacts/list/service/MultiChoiceService;

    move-object/from16 v0, p0

    iget v2, v0, Lcom/mediatek/contacts/list/service/CopyProcessor;->mJobId:I

    const/4 v3, 0x0

    invoke-virtual {v1, v2, v3}, Lcom/mediatek/contacts/list/service/MultiChoiceService;->handleFinishNotification(IZ)V

    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/mediatek/contacts/list/service/CopyProcessor;->mListener:Lcom/mediatek/contacts/list/service/MultiChoiceHandlerListener;

    const/4 v14, 0x1

    move-object/from16 v0, p0

    iget v15, v0, Lcom/mediatek/contacts/list/service/CopyProcessor;->mJobId:I

    sub-int v18, v5, v17

    move/from16 v16, v5

    invoke-virtual/range {v13 .. v18}, Lcom/mediatek/contacts/list/service/MultiChoiceHandlerListener;->onCanceled(IIIII)V

    if-eqz v26, :cond_0

    invoke-interface/range {v26 .. v26}, Landroid/database/Cursor;->isClosed()Z

    move-result v1

    if-nez v1, :cond_0

    invoke-interface/range {v26 .. v26}, Landroid/database/Cursor;->close()V

    goto/16 :goto_0

    :cond_6
    invoke-interface/range {v26 .. v26}, Landroid/database/Cursor;->getCount()I

    move-result v5

    goto/16 :goto_2

    :cond_7
    add-int/lit8 v4, v4, 0x1

    const/4 v1, 0x1

    move-object/from16 v0, v26

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v6

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/mediatek/contacts/list/service/CopyProcessor;->mListener:Lcom/mediatek/contacts/list/service/MultiChoiceHandlerListener;

    const/4 v2, 0x1

    move-object/from16 v0, p0

    iget v3, v0, Lcom/mediatek/contacts/list/service/CopyProcessor;->mJobId:I

    invoke-virtual/range {v1 .. v6}, Lcom/mediatek/contacts/list/service/MultiChoiceHandlerListener;->onProcessed(IIIILjava/lang/String;)V

    const/4 v1, 0x0

    move-object/from16 v0, v26

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v23

    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/mediatek/contacts/list/service/CopyProcessor;->mResolver:Landroid/content/ContentResolver;

    sget-object v8, Landroid/provider/ContactsContract$Data;->CONTENT_URI:Landroid/net/Uri;

    sget-object v9, Lcom/mediatek/contacts/list/service/CopyProcessor;->DATA_ALLCOLUMNS:[Ljava/lang/String;

    const-string v10, "raw_contact_id=? "

    const/4 v1, 0x1

    new-array v11, v1, [Ljava/lang/String;

    const/4 v1, 0x0

    invoke-static/range {v23 .. v24}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v2

    aput-object v2, v11, v1

    const/4 v12, 0x0

    invoke-virtual/range {v7 .. v12}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v8

    if-eqz v8, :cond_3

    invoke-interface {v8}, Landroid/database/Cursor;->getCount()I

    move-result v1

    if-gtz v1, :cond_8

    const-string v1, "CopyMultiContacts"

    const-string v2, "dataCursor is empty"

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    invoke-interface {v8}, Landroid/database/Cursor;->close()V

    goto/16 :goto_3

    :cond_8
    invoke-virtual/range {v25 .. v25}, Ljava/util/ArrayList;->size()I

    move-result v19

    sget-object v1, Landroid/provider/ContactsContract$RawContacts;->CONTENT_URI:Landroid/net/Uri;

    invoke-static {v1}, Landroid/content/ContentProviderOperation;->newInsert(Landroid/net/Uri;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v9

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/mediatek/contacts/list/service/CopyProcessor;->mAccountDst:Landroid/accounts/Account;

    iget-object v1, v1, Landroid/accounts/Account;->name:Ljava/lang/String;

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_a

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/mediatek/contacts/list/service/CopyProcessor;->mAccountDst:Landroid/accounts/Account;

    iget-object v1, v1, Landroid/accounts/Account;->type:Ljava/lang/String;

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_a

    const-string v1, "account_name"

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/mediatek/contacts/list/service/CopyProcessor;->mAccountDst:Landroid/accounts/Account;

    iget-object v2, v2, Landroid/accounts/Account;->name:Ljava/lang/String;

    invoke-virtual {v9, v1, v2}, Landroid/content/ContentProviderOperation$Builder;->withValue(Ljava/lang/String;Ljava/lang/Object;)Landroid/content/ContentProviderOperation$Builder;

    const-string v1, "account_type"

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/mediatek/contacts/list/service/CopyProcessor;->mAccountDst:Landroid/accounts/Account;

    iget-object v2, v2, Landroid/accounts/Account;->type:Ljava/lang/String;

    invoke-virtual {v9, v1, v2}, Landroid/content/ContentProviderOperation$Builder;->withValue(Ljava/lang/String;Ljava/lang/Object;)Landroid/content/ContentProviderOperation$Builder;

    :goto_5
    const-string v1, "aggregation_mode"

    const/4 v2, 0x3

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v9, v1, v2}, Landroid/content/ContentProviderOperation$Builder;->withValue(Ljava/lang/String;Ljava/lang/Object;)Landroid/content/ContentProviderOperation$Builder;

    invoke-virtual {v9}, Landroid/content/ContentProviderOperation$Builder;->build()Landroid/content/ContentProviderOperation;

    move-result-object v1

    move-object/from16 v0, v25

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    const/4 v1, -0x1

    invoke-interface {v8, v1}, Landroid/database/Cursor;->moveToPosition(I)Z

    invoke-interface {v8}, Landroid/database/Cursor;->getColumnNames()[Ljava/lang/String;

    move-result-object v10

    :cond_9
    :goto_6
    invoke-interface {v8}, Landroid/database/Cursor;->moveToNext()Z

    move-result v1

    if-eqz v1, :cond_b

    const-string v1, "mimetype"

    invoke-interface {v8, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    invoke-interface {v8, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v11

    const-string v1, "CopyMultiContacts"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "mimeType:"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    const-string v1, "vnd.android.cursor.item/group_membership"

    invoke-virtual {v1, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_9

    sget-object v1, Landroid/provider/ContactsContract$Data;->CONTENT_URI:Landroid/net/Uri;

    invoke-static {v1}, Landroid/content/ContentProviderOperation;->newInsert(Landroid/net/Uri;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v9

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/mediatek/contacts/list/service/CopyProcessor;->mAccountDst:Landroid/accounts/Account;

    check-cast v1, Lcom/mediatek/contacts/model/AccountWithDataSetEx;

    invoke-virtual {v1}, Lcom/mediatek/contacts/model/AccountWithDataSetEx;->getSlotId()I

    move-result v12

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/mediatek/contacts/list/service/CopyProcessor;->mAccountSrc:Landroid/accounts/Account;

    iget-object v13, v1, Landroid/accounts/Account;->type:Ljava/lang/String;

    move-object/from16 v7, p0

    invoke-direct/range {v7 .. v13}, Lcom/mediatek/contacts/list/service/CopyProcessor;->generateDataBuilder(Landroid/database/Cursor;Landroid/content/ContentProviderOperation$Builder;[Ljava/lang/String;Ljava/lang/String;ILjava/lang/String;)V

    const-string v1, "raw_contact_id"

    move/from16 v0, v19

    invoke-virtual {v9, v1, v0}, Landroid/content/ContentProviderOperation$Builder;->withValueBackReference(Ljava/lang/String;I)Landroid/content/ContentProviderOperation$Builder;

    invoke-virtual {v9}, Landroid/content/ContentProviderOperation$Builder;->build()Landroid/content/ContentProviderOperation;

    move-result-object v1

    move-object/from16 v0, v25

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_6

    :cond_a
    new-instance v1, Landroid/content/ContentValues;

    invoke-direct {v1}, Landroid/content/ContentValues;-><init>()V

    invoke-virtual {v9, v1}, Landroid/content/ContentProviderOperation$Builder;->withValues(Landroid/content/ContentValues;)Landroid/content/ContentProviderOperation$Builder;

    goto :goto_5

    :cond_b
    invoke-interface {v8}, Landroid/database/Cursor;->close()V

    add-int/lit8 v17, v17, 0x1

    invoke-virtual/range {v25 .. v25}, Ljava/util/ArrayList;->size()I

    move-result v1

    const/16 v2, 0x190

    if-le v1, v2, :cond_3

    :try_start_1
    const-string v1, "CopyMultiContacts"

    const-string v2, "Before applyBatch. "

    invoke-static {v1, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/mediatek/contacts/list/service/CopyProcessor;->mResolver:Landroid/content/ContentResolver;

    const-string v2, "com.android.contacts"

    move-object/from16 v0, v25

    invoke-virtual {v1, v2, v0}, Landroid/content/ContentResolver;->applyBatch(Ljava/lang/String;Ljava/util/ArrayList;)[Landroid/content/ContentProviderResult;

    const-string v1, "CopyMultiContacts"

    const-string v2, "After applyBatch "

    invoke-static {v1, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_1
    .catch Landroid/os/RemoteException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Landroid/content/OperationApplicationException; {:try_start_1 .. :try_end_1} :catch_1

    :goto_7
    invoke-virtual/range {v25 .. v25}, Ljava/util/ArrayList;->clear()V

    goto/16 :goto_3

    :catch_0
    move-exception v21

    const-string v1, "CopyMultiContacts"

    const-string v2, "%s: %s"

    const/4 v3, 0x2

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v7, 0x0

    invoke-virtual/range {v21 .. v21}, Ljava/lang/Throwable;->toString()Ljava/lang/String;

    move-result-object v13

    aput-object v13, v3, v7

    const/4 v7, 0x1

    invoke-virtual/range {v21 .. v21}, Ljava/lang/Throwable;->getMessage()Ljava/lang/String;

    move-result-object v13

    aput-object v13, v3, v7

    invoke-static {v2, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_7

    :catch_1
    move-exception v21

    const-string v1, "CopyMultiContacts"

    const-string v2, "%s: %s"

    const/4 v3, 0x2

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v7, 0x0

    invoke-virtual/range {v21 .. v21}, Ljava/lang/Throwable;->toString()Ljava/lang/String;

    move-result-object v13

    aput-object v13, v3, v7

    const/4 v7, 0x1

    invoke-virtual/range {v21 .. v21}, Ljava/lang/Throwable;->getMessage()Ljava/lang/String;

    move-result-object v13

    aput-object v13, v3, v7

    invoke-static {v2, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_7

    :catch_2
    move-exception v21

    const-string v1, "CopyMultiContacts"

    const-string v2, "%s: %s"

    const/4 v3, 0x2

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v7, 0x0

    invoke-virtual/range {v21 .. v21}, Ljava/lang/Throwable;->toString()Ljava/lang/String;

    move-result-object v13

    aput-object v13, v3, v7

    const/4 v7, 0x1

    invoke-virtual/range {v21 .. v21}, Ljava/lang/Throwable;->getMessage()Ljava/lang/String;

    move-result-object v13

    aput-object v13, v3, v7

    invoke-static {v2, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_4

    :catch_3
    move-exception v21

    const-string v1, "CopyMultiContacts"

    const-string v2, "%s: %s"

    const/4 v3, 0x2

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v7, 0x0

    invoke-virtual/range {v21 .. v21}, Ljava/lang/Throwable;->toString()Ljava/lang/String;

    move-result-object v13

    aput-object v13, v3, v7

    const/4 v7, 0x1

    invoke-virtual/range {v21 .. v21}, Ljava/lang/Throwable;->getMessage()Ljava/lang/String;

    move-result-object v13

    aput-object v13, v3, v7

    invoke-static {v2, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_4

    :cond_c
    move/from16 v4, v20

    :cond_d
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/mediatek/contacts/list/service/CopyProcessor;->mService:Lcom/mediatek/contacts/list/service/MultiChoiceService;

    move-object/from16 v0, p0

    iget v3, v0, Lcom/mediatek/contacts/list/service/CopyProcessor;->mJobId:I

    move/from16 v0, v17

    if-ne v0, v5, :cond_e

    const/4 v1, 0x1

    :goto_8
    invoke-virtual {v2, v3, v1}, Lcom/mediatek/contacts/list/service/MultiChoiceService;->handleFinishNotification(IZ)V

    move/from16 v0, v17

    if-ne v0, v5, :cond_f

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/mediatek/contacts/list/service/CopyProcessor;->mListener:Lcom/mediatek/contacts/list/service/MultiChoiceHandlerListener;

    const/4 v2, 0x1

    move-object/from16 v0, p0

    iget v3, v0, Lcom/mediatek/contacts/list/service/CopyProcessor;->mJobId:I

    invoke-virtual {v1, v2, v3, v5}, Lcom/mediatek/contacts/list/service/MultiChoiceHandlerListener;->onFinished(III)V

    :goto_9
    const-string v1, "CopyMultiContacts"

    const-string v2, "copyContactsToAccount: end"

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0

    :cond_e
    const/4 v1, 0x0

    goto :goto_8

    :cond_f
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/mediatek/contacts/list/service/CopyProcessor;->mListener:Lcom/mediatek/contacts/list/service/MultiChoiceHandlerListener;

    const/4 v14, 0x1

    move-object/from16 v0, p0

    iget v15, v0, Lcom/mediatek/contacts/list/service/CopyProcessor;->mJobId:I

    sub-int v18, v5, v17

    move/from16 v16, v5

    invoke-virtual/range {v13 .. v18}, Lcom/mediatek/contacts/list/service/MultiChoiceHandlerListener;->onFailed(IIIII)V

    goto :goto_9
.end method

.method private copyContactsToSim()V
    .locals 91

    const/4 v8, 0x0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/mediatek/contacts/list/service/CopyProcessor;->mAccountDst:Landroid/accounts/Account;

    move-object/from16 v52, v0

    check-cast v52, Lcom/mediatek/contacts/model/AccountWithDataSetEx;

    const-string v2, "CopyMultiContacts"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "[copyContactsToSim]AccountName:"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    move-object/from16 v0, v52

    iget-object v4, v0, Landroid/accounts/Account;->name:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "|accountType:"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    move-object/from16 v0, v52

    iget-object v4, v0, Landroid/accounts/Account;->type:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    invoke-virtual/range {v52 .. v52}, Lcom/mediatek/contacts/model/AccountWithDataSetEx;->getSlotId()I

    move-result v19

    invoke-static {}, Lcom/mediatek/phone/SIMInfoWrapper;->getDefault()Lcom/mediatek/phone/SIMInfoWrapper;

    move-result-object v2

    move/from16 v0, v19

    invoke-virtual {v2, v0}, Lcom/mediatek/phone/SIMInfoWrapper;->getSimInfoBySlot(I)Landroid/provider/Telephony$SIMInfo;

    move-result-object v57

    if-eqz v57, :cond_1

    move-object/from16 v0, v57

    iget-wide v0, v0, Landroid/provider/Telephony$SIMInfo;->mSimId:J

    move-wide/from16 v40, v0

    :goto_0
    const-string v2, "CopyMultiContacts"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "[copyContactsToSim]dstSlotId:"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    move/from16 v0, v19

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "|dstSimId:"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    move-wide/from16 v0, v40

    invoke-virtual {v3, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    invoke-static/range {v19 .. v19}, Lcom/mediatek/contacts/simcontact/SimCardUtils;->isSimUsimType(I)Z

    move-result v70

    if-eqz v70, :cond_2

    const-string v42, "USIM"

    :goto_1
    const-string v2, "CopyMultiContacts"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "[copyContactsToSim]dstSimType:"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    move-object/from16 v0, v42

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    move-object/from16 v0, p0

    move/from16 v1, v19

    invoke-direct {v0, v1}, Lcom/mediatek/contacts/list/service/CopyProcessor;->isSimReady(I)Z

    move-result v2

    if-eqz v2, :cond_0

    move-object/from16 v0, p0

    move/from16 v1, v19

    invoke-direct {v0, v1}, Lcom/mediatek/contacts/list/service/CopyProcessor;->isPhoneBookReady(I)Z

    move-result v2

    if-nez v2, :cond_3

    :cond_0
    const/4 v8, 0x3

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/mediatek/contacts/list/service/CopyProcessor;->mService:Lcom/mediatek/contacts/list/service/MultiChoiceService;

    move-object/from16 v0, p0

    iget v3, v0, Lcom/mediatek/contacts/list/service/CopyProcessor;->mJobId:I

    const/4 v4, 0x0

    invoke-virtual {v2, v3, v4}, Lcom/mediatek/contacts/list/service/MultiChoiceService;->handleFinishNotification(IZ)V

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/mediatek/contacts/list/service/CopyProcessor;->mListener:Lcom/mediatek/contacts/list/service/MultiChoiceHandlerListener;

    const/4 v3, 0x1

    move-object/from16 v0, p0

    iget v4, v0, Lcom/mediatek/contacts/list/service/CopyProcessor;->mJobId:I

    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/mediatek/contacts/list/service/CopyProcessor;->mRequests:Ljava/util/List;

    invoke-interface {v7}, Ljava/util/List;->size()I

    move-result v5

    const/4 v6, 0x0

    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/mediatek/contacts/list/service/CopyProcessor;->mRequests:Ljava/util/List;

    invoke-interface {v7}, Ljava/util/List;->size()I

    move-result v7

    invoke-virtual/range {v2 .. v8}, Lcom/mediatek/contacts/list/service/MultiChoiceHandlerListener;->onFailed(IIIIII)V

    :goto_2
    return-void

    :cond_1
    const-wide/16 v40, -0x1

    goto/16 :goto_0

    :cond_2
    const-string v42, "SIM"

    goto :goto_1

    :cond_3
    new-instance v25, Ljava/util/ArrayList;

    invoke-direct/range {v25 .. v25}, Ljava/util/ArrayList;-><init>()V

    new-instance v16, Ljava/util/ArrayList;

    invoke-direct/range {v16 .. v16}, Ljava/util/ArrayList;-><init>()V

    new-instance v61, Ljava/util/ArrayList;

    invoke-direct/range {v61 .. v61}, Ljava/util/ArrayList;-><init>()V

    new-instance v26, Ljava/util/ArrayList;

    invoke-direct/range {v26 .. v26}, Ljava/util/ArrayList;-><init>()V

    new-instance v17, Ljava/util/ArrayList;

    invoke-direct/range {v17 .. v17}, Ljava/util/ArrayList;-><init>()V

    new-instance v77, Ljava/util/ArrayList;

    invoke-direct/range {v77 .. v77}, Ljava/util/ArrayList;-><init>()V

    const/16 v89, 0x0

    move-object/from16 v0, p0

    iget-object v9, v0, Lcom/mediatek/contacts/list/service/CopyProcessor;->mResolver:Landroid/content/ContentResolver;

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/mediatek/contacts/list/service/CopyProcessor;->mRequests:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v6

    const/16 v49, 0x0

    const/4 v5, 0x0

    invoke-static/range {v19 .. v19}, Lcom/mediatek/contacts/simcontact/SimCardUtils;->getIccCardEmailCount(I)I

    move-result v66

    const/16 v69, 0x0

    new-instance v81, Ljava/util/ArrayList;

    invoke-direct/range {v81 .. v81}, Ljava/util/ArrayList;-><init>()V

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/mediatek/contacts/list/service/CopyProcessor;->mRequests:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v65

    :cond_4
    :goto_3
    invoke-interface/range {v65 .. v65}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_5

    invoke-interface/range {v65 .. v65}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v84

    check-cast v84, Lcom/mediatek/contacts/list/service/MultiChoiceRequest;

    move-object/from16 v0, p0

    iget-boolean v2, v0, Lcom/mediatek/contacts/list/service/CopyProcessor;->mCanceled:Z

    if-eqz v2, :cond_8

    :cond_5
    :goto_4
    invoke-virtual/range {v81 .. v81}, Ljava/util/ArrayList;->size()I

    move-result v2

    if-lez v2, :cond_7

    :try_start_0
    const-string v2, "CopyMultiContacts"

    const-string v3, "Before end applyBatch. "

    invoke-static {v2, v3}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    move-object/from16 v0, p0

    move/from16 v1, v19

    invoke-direct {v0, v1}, Lcom/mediatek/contacts/list/service/CopyProcessor;->isSimReady(I)Z

    move-result v2

    if-eqz v2, :cond_6

    move-object/from16 v0, p0

    move/from16 v1, v19

    invoke-direct {v0, v1}, Lcom/mediatek/contacts/list/service/CopyProcessor;->isPhoneBookReady(I)Z

    move-result v2

    if-eqz v2, :cond_6

    const-string v2, "com.android.contacts"

    move-object/from16 v0, v81

    invoke-virtual {v9, v2, v0}, Landroid/content/ContentResolver;->applyBatch(Ljava/lang/String;Ljava/util/ArrayList;)[Landroid/content/ContentProviderResult;

    :cond_6
    const-string v2, "CopyMultiContacts"

    const-string v3, "After end applyBatch "

    invoke-static {v2, v3}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_2
    .catch Landroid/content/OperationApplicationException; {:try_start_0 .. :try_end_0} :catch_3

    :goto_5
    invoke-virtual/range {v81 .. v81}, Ljava/util/ArrayList;->clear()V

    :cond_7
    move-object/from16 v0, p0

    iget-boolean v2, v0, Lcom/mediatek/contacts/list/service/CopyProcessor;->mCanceled:Z

    if-eqz v2, :cond_37

    const-string v2, "CopyMultiContacts"

    const-string v3, "copyContactsToSim run: mCanceled = true"

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v8, 0x2

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/mediatek/contacts/list/service/CopyProcessor;->mService:Lcom/mediatek/contacts/list/service/MultiChoiceService;

    move-object/from16 v0, p0

    iget v3, v0, Lcom/mediatek/contacts/list/service/CopyProcessor;->mJobId:I

    const/4 v4, 0x0

    invoke-virtual {v2, v3, v4}, Lcom/mediatek/contacts/list/service/MultiChoiceService;->handleFinishNotification(IZ)V

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/mediatek/contacts/list/service/CopyProcessor;->mListener:Lcom/mediatek/contacts/list/service/MultiChoiceHandlerListener;

    move-object/from16 v45, v0

    const/16 v46, 0x1

    move-object/from16 v0, p0

    iget v0, v0, Lcom/mediatek/contacts/list/service/CopyProcessor;->mJobId:I

    move/from16 v47, v0

    sub-int v50, v6, v49

    move/from16 v48, v6

    invoke-virtual/range {v45 .. v50}, Lcom/mediatek/contacts/list/service/MultiChoiceHandlerListener;->onCanceled(IIIII)V

    goto/16 :goto_2

    :cond_8
    move-object/from16 v0, p0

    move/from16 v1, v19

    invoke-direct {v0, v1}, Lcom/mediatek/contacts/list/service/CopyProcessor;->isSimReady(I)Z

    move-result v2

    if-eqz v2, :cond_9

    move-object/from16 v0, p0

    move/from16 v1, v19

    invoke-direct {v0, v1}, Lcom/mediatek/contacts/list/service/CopyProcessor;->isPhoneBookReady(I)Z

    move-result v2

    if-nez v2, :cond_a

    :cond_9
    const-string v2, "CopyMultiContacts"

    const-string v3, "copyContactsToSim run: sim not ready"

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v8, 0x1

    invoke-virtual/range {v81 .. v81}, Ljava/util/ArrayList;->clear()V

    goto/16 :goto_4

    :cond_a
    add-int/lit8 v5, v5, 0x1

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/mediatek/contacts/list/service/CopyProcessor;->mListener:Lcom/mediatek/contacts/list/service/MultiChoiceHandlerListener;

    const/4 v3, 0x1

    move-object/from16 v0, p0

    iget v4, v0, Lcom/mediatek/contacts/list/service/CopyProcessor;->mJobId:I

    move-object/from16 v0, v84

    iget-object v7, v0, Lcom/mediatek/contacts/list/service/MultiChoiceRequest;->mContactName:Ljava/lang/String;

    invoke-virtual/range {v2 .. v7}, Lcom/mediatek/contacts/list/service/MultiChoiceHandlerListener;->onProcessed(IIIILjava/lang/String;)V

    invoke-virtual/range {v25 .. v25}, Ljava/util/ArrayList;->clear()V

    invoke-virtual/range {v16 .. v16}, Ljava/util/ArrayList;->clear()V

    invoke-virtual/range {v26 .. v26}, Ljava/util/ArrayList;->clear()V

    invoke-virtual/range {v17 .. v17}, Ljava/util/ArrayList;->clear()V

    invoke-virtual/range {v61 .. v61}, Ljava/util/ArrayList;->clear()V

    const/16 v89, 0x0

    move-object/from16 v0, v84

    iget v0, v0, Lcom/mediatek/contacts/list/service/MultiChoiceRequest;->mContactId:I

    move/from16 v56, v0

    sget-object v2, Landroid/provider/ContactsContract$Contacts;->CONTENT_URI:Landroid/net/Uri;

    move/from16 v0, v56

    int-to-long v3, v0

    invoke-static {v2, v3, v4}, Landroid/content/ContentUris;->withAppendedId(Landroid/net/Uri;J)Landroid/net/Uri;

    move-result-object v2

    const-string v3, "data"

    invoke-static {v2, v3}, Landroid/net/Uri;->withAppendedPath(Landroid/net/Uri;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v10

    const/4 v2, 0x4

    new-array v0, v2, [Ljava/lang/String;

    move-object/from16 v83, v0

    const/4 v2, 0x0

    const-string v3, "_id"

    aput-object v3, v83, v2

    const/4 v2, 0x1

    const-string v3, "mimetype"

    aput-object v3, v83, v2

    const/4 v2, 0x2

    const-string v3, "data1"

    aput-object v3, v83, v2

    const/4 v2, 0x3

    const-string v3, "is_additional_number"

    aput-object v3, v83, v2

    invoke-static {}, Lcom/mediatek/contacts/ExtensionManager;->getInstance()Lcom/mediatek/contacts/ExtensionManager;

    move-result-object v2

    invoke-virtual {v2}, Lcom/mediatek/contacts/ExtensionManager;->getContactAccountExtension()Lcom/android/contacts/ext/ContactAccountExtension;

    move-result-object v2

    const/4 v3, 0x1

    const-string v4, "ExtensionForAAS"

    move-object/from16 v0, v83

    invoke-virtual {v2, v3, v0, v4}, Lcom/android/contacts/ext/ContactAccountExtension;->getProjection(I[Ljava/lang/String;Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v82

    move-object/from16 v0, v82

    array-length v2, v0

    add-int/lit8 v2, v2, 0x2

    new-array v11, v2, [Ljava/lang/String;

    const/4 v2, 0x0

    const/4 v3, 0x0

    move-object/from16 v0, v82

    array-length v4, v0

    move-object/from16 v0, v82

    invoke-static {v0, v2, v11, v3, v4}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/String;

    const/4 v3, 0x0

    const-string v4, "name_raw_contact_id"

    aput-object v4, v2, v3

    const/4 v3, 0x1

    const-string v4, "raw_contact_id"

    aput-object v4, v2, v3

    const/4 v3, 0x0

    move-object/from16 v0, v82

    array-length v4, v0

    const/4 v7, 0x2

    invoke-static {v2, v3, v11, v4, v7}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    const/4 v12, 0x0

    const/4 v13, 0x0

    const/4 v14, 0x0

    invoke-virtual/range {v9 .. v14}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v54

    if-eqz v54, :cond_e

    invoke-interface/range {v54 .. v54}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v2

    if-eqz v2, :cond_e

    :cond_b
    const/4 v2, 0x1

    move-object/from16 v0, v54

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v75

    const-string v2, "vnd.android.cursor.item/phone_v2"

    move-object/from16 v0, v75

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_13

    const/4 v2, 0x2

    move-object/from16 v0, v54

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v78

    const/4 v2, 0x3

    move-object/from16 v0, v54

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getInt(I)I

    move-result v68

    invoke-static {}, Lcom/mediatek/contacts/ExtensionManager;->getInstance()Lcom/mediatek/contacts/ExtensionManager;

    move-result-object v2

    invoke-virtual {v2}, Lcom/mediatek/contacts/ExtensionManager;->getContactAccountExtension()Lcom/android/contacts/ext/ContactAccountExtension;

    move-result-object v2

    const-string v3, "ExtensionForAAS"

    invoke-virtual {v2, v3}, Lcom/android/contacts/ext/ContactAccountExtension;->isFeatureEnabled(Ljava/lang/String;)Z

    move-result v67

    const/4 v2, 0x1

    move/from16 v0, v68

    if-ne v0, v2, :cond_12

    move-object/from16 v0, v16

    move-object/from16 v1, v78

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    if-eqz v67, :cond_c

    const/4 v2, 0x4

    move-object/from16 v0, v54

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getInt(I)I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    move-object/from16 v0, v17

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :cond_c
    :goto_6
    if-eqz v70, :cond_d

    const-string v2, "vnd.android.cursor.item/email_v2"

    move-object/from16 v0, v75

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_14

    if-lez v66, :cond_14

    const/4 v2, 0x2

    move-object/from16 v0, v54

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v60

    move-object/from16 v0, v61

    move-object/from16 v1, v60

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :cond_d
    :goto_7
    invoke-interface/range {v54 .. v54}, Landroid/database/Cursor;->moveToNext()Z

    move-result v2

    if-nez v2, :cond_b

    :cond_e
    if-eqz v54, :cond_f

    invoke-interface/range {v54 .. v54}, Landroid/database/Cursor;->close()V

    :cond_f
    invoke-static/range {v19 .. v19}, Lcom/mediatek/contacts/simcontact/SimCardUtils$SimUri;->getSimUri(I)Landroid/net/Uri;

    move-result-object v58

    invoke-static/range {v89 .. v89}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_15

    const/16 v74, 0x0

    :goto_8
    invoke-static {}, Lcom/mediatek/contacts/ExtensionManager;->getInstance()Lcom/mediatek/contacts/ExtensionManager;

    move-result-object v2

    invoke-virtual {v2}, Lcom/mediatek/contacts/ExtensionManager;->getContactDetailExtension()Lcom/android/contacts/ext/ContactDetailExtension;

    move-result-object v2

    const-string v3, "ExtensionForAAS"

    move/from16 v0, v19

    invoke-virtual {v2, v0, v3}, Lcom/android/contacts/ext/ContactDetailExtension;->getAdditionNumberCount(ILjava/lang/String;)I

    move-result v18

    invoke-static/range {v19 .. v19}, Lcom/mediatek/contacts/simcontact/SimCardUtils;->getAnrCount(I)I

    move-result v90

    if-eqz v70, :cond_1a

    invoke-virtual/range {v25 .. v25}, Ljava/util/ArrayList;->size()I

    move-result v79

    invoke-virtual/range {v16 .. v16}, Ljava/util/ArrayList;->size()I

    move-result v53

    invoke-virtual/range {v61 .. v61}, Ljava/util/ArrayList;->size()I

    move-result v62

    move/from16 v0, v74

    move/from16 v1, v53

    if-le v0, v1, :cond_16

    :goto_9
    move/from16 v0, v74

    move/from16 v1, v62

    if-le v0, v1, :cond_17

    :goto_a
    invoke-static {}, Lcom/mediatek/contacts/ExtensionManager;->getInstance()Lcom/mediatek/contacts/ExtensionManager;

    move-result-object v2

    invoke-virtual {v2}, Lcom/mediatek/contacts/ExtensionManager;->getContactAccountExtension()Lcom/android/contacts/ext/ContactAccountExtension;

    move-result-object v2

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/mediatek/contacts/list/service/CopyProcessor;->mAccountDst:Landroid/accounts/Account;

    iget-object v3, v3, Landroid/accounts/Account;->type:Ljava/lang/String;

    const-string v4, "ExtensionForAAS"

    invoke-virtual {v2, v3, v4}, Lcom/android/contacts/ext/ContactAccountExtension;->isFeatureAccount(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_18

    add-int v2, v79, v53

    int-to-double v2, v2

    const-wide/high16 v12, 0x3ff0000000000000L

    move/from16 v0, v18

    int-to-double v0, v0

    move-wide/from16 v21, v0

    add-double v12, v12, v21

    div-double/2addr v2, v12

    move/from16 v0, v18

    int-to-float v4, v0

    float-to-double v12, v4

    const-wide/high16 v21, 0x3ff0000000000000L

    move/from16 v0, v18

    int-to-double v0, v0

    move-wide/from16 v23, v0

    add-double v21, v21, v23

    div-double v12, v12, v21

    add-double/2addr v2, v12

    double-to-int v0, v2

    move/from16 v80, v0

    const-string v2, "CopyMultiContacts"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "maxAnr="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    move/from16 v0, v18

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "; numberQuota="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    move/from16 v0, v80

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    :goto_b
    move/from16 v0, v74

    move/from16 v1, v80

    if-le v0, v1, :cond_19

    :goto_c
    const/16 v86, 0x0

    new-instance v15, Landroid/content/ContentValues;

    invoke-direct {v15}, Landroid/content/ContentValues;-><init>()V

    const/16 v35, 0x0

    const/16 v36, 0x0

    const/16 v38, 0x0

    const/16 v87, 0x0

    const/16 v32, 0x0

    if-lez v86, :cond_1c

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, v89

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    move/from16 v0, v86

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v35

    :goto_d
    invoke-static/range {v35 .. v35}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_10

    const-string v35, ""

    :cond_10
    if-eqz v35, :cond_11

    invoke-virtual/range {v35 .. v35}, Ljava/lang/String;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_11

    invoke-virtual/range {v35 .. v35}, Ljava/lang/String;->length()I

    move-result v2

    if-nez v2, :cond_1d

    :cond_11
    invoke-virtual/range {v25 .. v25}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v2

    if-eqz v2, :cond_1d

    const-string v2, "CopyMultiContacts"

    const-string v3, " name and number are empty"

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v8, 0x1

    goto/16 :goto_3

    :cond_12
    move-object/from16 v0, v25

    move-object/from16 v1, v78

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    if-eqz v67, :cond_c

    const/4 v2, 0x4

    move-object/from16 v0, v54

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getInt(I)I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    move-object/from16 v0, v26

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto/16 :goto_6

    :cond_13
    const-string v2, "vnd.android.cursor.item/name"

    move-object/from16 v0, v75

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_c

    const-string v2, "name_raw_contact_id"

    move-object/from16 v0, v54

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v2

    move-object/from16 v0, v54

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getInt(I)I

    move-result v2

    const-string v3, "raw_contact_id"

    move-object/from16 v0, v54

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v3

    move-object/from16 v0, v54

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getInt(I)I

    move-result v3

    if-ne v2, v3, :cond_c

    const/4 v2, 0x2

    move-object/from16 v0, v54

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v89

    goto/16 :goto_6

    :cond_14
    invoke-static/range {v75 .. v75}, Lcom/mediatek/contacts/extension/aassne/SneExt;->isNickname(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_d

    const/4 v2, 0x2

    move-object/from16 v0, v54

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v76

    move-object/from16 v0, v77

    move-object/from16 v1, v76

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto/16 :goto_7

    :cond_15
    const/16 v74, 0x1

    goto/16 :goto_8

    :cond_16
    move/from16 v74, v53

    goto/16 :goto_9

    :cond_17
    move/from16 v74, v62

    goto/16 :goto_a

    :cond_18
    add-int v2, v79, v53

    int-to-double v2, v2

    const-wide/high16 v12, 0x3ff0000000000000L

    move/from16 v0, v90

    int-to-double v0, v0

    move-wide/from16 v21, v0

    add-double v12, v12, v21

    div-double/2addr v2, v12

    move/from16 v0, v90

    int-to-float v4, v0

    float-to-double v12, v4

    const-wide/high16 v21, 0x3ff0000000000000L

    move/from16 v0, v90

    int-to-double v0, v0

    move-wide/from16 v23, v0

    add-double v21, v21, v23

    div-double v12, v12, v21

    add-double/2addr v2, v12

    double-to-int v0, v2

    move/from16 v80, v0

    goto/16 :goto_b

    :cond_19
    move/from16 v74, v80

    goto/16 :goto_c

    :cond_1a
    move-object/from16 v0, v25

    move-object/from16 v1, v16

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->addAll(Ljava/util/Collection;)Z

    move-object/from16 v0, v26

    move-object/from16 v1, v17

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->addAll(Ljava/util/Collection;)Z

    invoke-virtual/range {v16 .. v16}, Ljava/util/ArrayList;->clear()V

    invoke-virtual/range {v25 .. v25}, Ljava/util/ArrayList;->size()I

    move-result v79

    move/from16 v0, v74

    move/from16 v1, v79

    if-le v0, v1, :cond_1b

    :goto_e
    goto/16 :goto_c

    :cond_1b
    move/from16 v74, v79

    goto :goto_e

    :cond_1c
    move-object/from16 v35, v89

    goto/16 :goto_d

    :cond_1d
    const/16 v88, 0x0

    new-instance v20, Ljava/util/ArrayList;

    invoke-direct/range {v20 .. v20}, Ljava/util/ArrayList;-><init>()V

    const/16 v64, 0x0

    :goto_f
    move/from16 v0, v64

    move/from16 v1, v74

    if-ge v0, v1, :cond_27

    invoke-virtual {v15}, Landroid/content/ContentValues;->clear()V

    const-string v2, "tag"

    move-object/from16 v0, v35

    invoke-virtual {v15, v2, v0}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    const-string v2, "CopyMultiContacts"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "copyContactsToSim tag is "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    move-object/from16 v0, v35

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    const/16 v36, 0x0

    const/16 v38, 0x0

    const/16 v87, 0x0

    invoke-virtual/range {v25 .. v25}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_1e

    const/4 v2, 0x0

    move-object/from16 v0, v25

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;

    move-result-object v36

    check-cast v36, Ljava/lang/String;

    invoke-static/range {v36 .. v36}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_1f

    const-string v36, ""

    :goto_10
    const-string v2, "number"

    invoke-static/range {v36 .. v36}, Landroid/telephony/PhoneNumberUtils;->stripSeparators(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v15, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    const-string v2, "CopyMultiContacts"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "copyContactsToSim number is "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    move-object/from16 v0, v36

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    invoke-static {}, Lcom/mediatek/contacts/ExtensionManager;->getInstance()Lcom/mediatek/contacts/ExtensionManager;

    move-result-object v2

    invoke-virtual {v2}, Lcom/mediatek/contacts/ExtensionManager;->getContactListExtension()Lcom/android/contacts/ext/ContactListExtension;

    move-result-object v2

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/mediatek/contacts/list/service/CopyProcessor;->mAccountDst:Landroid/accounts/Account;

    iget-object v3, v3, Landroid/accounts/Account;->type:Ljava/lang/String;

    const-string v4, "ExtensionForAAS"

    move-object/from16 v0, v26

    invoke-virtual {v2, v3, v0, v4}, Lcom/android/contacts/ext/ContactListExtension;->checkPhoneTypeArray(Ljava/lang/String;Ljava/util/ArrayList;Ljava/lang/String;)V

    :cond_1e
    invoke-virtual/range {v20 .. v20}, Ljava/util/ArrayList;->clear()V

    if-eqz v70, :cond_26

    const-string v2, "CopyMultiContacts"

    const-string v3, "copyContactsToSim copy to USIM"

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    invoke-virtual/range {v16 .. v16}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_29

    invoke-static {}, Lcom/mediatek/contacts/ExtensionManager;->getInstance()Lcom/mediatek/contacts/ExtensionManager;

    move-result-object v2

    invoke-virtual {v2}, Lcom/mediatek/contacts/ExtensionManager;->getContactAccountExtension()Lcom/android/contacts/ext/ContactAccountExtension;

    move-result-object v12

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/mediatek/contacts/list/service/CopyProcessor;->mAccountDst:Landroid/accounts/Account;

    iget-object v13, v2, Landroid/accounts/Account;->type:Ljava/lang/String;

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/mediatek/contacts/list/service/CopyProcessor;->mService:Lcom/mediatek/contacts/list/service/MultiChoiceService;

    invoke-virtual {v2}, Landroid/content/ContextWrapper;->getApplicationContext()Landroid/content/Context;

    move-result-object v14

    const-string v21, "ExtensionForAAS"

    invoke-virtual/range {v12 .. v21}, Lcom/android/contacts/ext/ContactAccountExtension;->buildValuesForSim(Ljava/lang/String;Landroid/content/Context;Landroid/content/ContentValues;Ljava/util/ArrayList;Ljava/util/ArrayList;IILjava/util/ArrayList;Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_23

    invoke-virtual/range {v16 .. v16}, Ljava/util/ArrayList;->size()I

    move-result v2

    move/from16 v0, v90

    if-ge v2, v0, :cond_20

    invoke-virtual/range {v16 .. v16}, Ljava/util/ArrayList;->size()I

    move-result v73

    :goto_11
    const/16 v71, 0x0

    :goto_12
    move/from16 v0, v71

    move/from16 v1, v73

    if-ge v0, v1, :cond_22

    const/4 v2, 0x0

    move-object/from16 v0, v16

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;

    move-result-object v38

    check-cast v38, Ljava/lang/String;

    invoke-static/range {v38 .. v38}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_21

    const-string v38, ""

    :goto_13
    const-string v2, "anr"

    invoke-static/range {v38 .. v38}, Landroid/telephony/PhoneNumberUtils;->stripSeparators(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v15, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    add-int/lit8 v71, v71, 0x1

    goto :goto_12

    :cond_1f
    const-string v2, "-"

    const-string v3, ""

    move-object/from16 v0, v36

    invoke-virtual {v0, v2, v3}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v36

    goto/16 :goto_10

    :cond_20
    move/from16 v73, v90

    goto :goto_11

    :cond_21
    const-string v2, "-"

    const-string v3, ""

    move-object/from16 v0, v38

    invoke-virtual {v0, v2, v3}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v38

    goto :goto_13

    :cond_22
    invoke-virtual/range {v16 .. v16}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_23

    move-object/from16 v0, v25

    move-object/from16 v1, v16

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->addAll(Ljava/util/Collection;)Z

    invoke-virtual/range {v16 .. v16}, Ljava/util/ArrayList;->clear()V

    :cond_23
    invoke-virtual/range {v61 .. v61}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_25

    const/4 v2, 0x0

    move-object/from16 v0, v61

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;

    move-result-object v87

    check-cast v87, Ljava/lang/String;

    invoke-static/range {v87 .. v87}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_24

    const-string v87, ""

    :cond_24
    const-string v2, "emails"

    move-object/from16 v0, v87

    invoke-virtual {v15, v2, v0}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    const-string v2, "CopyMultiContacts"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "copyContactsToSim emails is "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    move-object/from16 v0, v87

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_25
    invoke-static {}, Lcom/mediatek/contacts/ExtensionManager;->getInstance()Lcom/mediatek/contacts/ExtensionManager;

    move-result-object v2

    invoke-virtual {v2}, Lcom/mediatek/contacts/ExtensionManager;->getContactListExtension()Lcom/android/contacts/ext/ContactListExtension;

    move-result-object v27

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/mediatek/contacts/list/service/CopyProcessor;->mAccountDst:Landroid/accounts/Account;

    iget-object v0, v2, Landroid/accounts/Account;->type:Ljava/lang/String;

    move-object/from16 v28, v0

    const-string v33, "ExtensionForSNE"

    move-object/from16 v29, v15

    move-object/from16 v30, v77

    move/from16 v31, v19

    invoke-virtual/range {v27 .. v33}, Lcom/android/contacts/ext/ContactListExtension;->buildSimNickname(Ljava/lang/String;Landroid/content/ContentValues;Ljava/util/ArrayList;ILjava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v32

    :cond_26
    move-object/from16 v0, p0

    move/from16 v1, v19

    invoke-direct {v0, v1}, Lcom/mediatek/contacts/list/service/CopyProcessor;->isSimReady(I)Z

    move-result v2

    if-eqz v2, :cond_27

    move-object/from16 v0, p0

    move/from16 v1, v19

    invoke-direct {v0, v1}, Lcom/mediatek/contacts/list/service/CopyProcessor;->isPhoneBookReady(I)Z

    move-result v2

    if-nez v2, :cond_2c

    :cond_27
    :goto_14
    if-lez v88, :cond_28

    add-int/lit8 v49, v49, 0x1

    :cond_28
    if-eqz v69, :cond_4

    goto/16 :goto_4

    :cond_29
    invoke-virtual/range {v25 .. v25}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_23

    invoke-static {}, Lcom/mediatek/contacts/ExtensionManager;->getInstance()Lcom/mediatek/contacts/ExtensionManager;

    move-result-object v2

    invoke-virtual {v2}, Lcom/mediatek/contacts/ExtensionManager;->getContactAccountExtension()Lcom/android/contacts/ext/ContactAccountExtension;

    move-result-object v21

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/mediatek/contacts/list/service/CopyProcessor;->mAccountDst:Landroid/accounts/Account;

    iget-object v0, v2, Landroid/accounts/Account;->type:Ljava/lang/String;

    move-object/from16 v22, v0

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/mediatek/contacts/list/service/CopyProcessor;->mService:Lcom/mediatek/contacts/list/service/MultiChoiceService;

    invoke-virtual {v2}, Landroid/content/ContextWrapper;->getApplicationContext()Landroid/content/Context;

    move-result-object v23

    const-string v30, "ExtensionForAAS"

    move-object/from16 v24, v15

    move/from16 v27, v18

    move/from16 v28, v19

    move-object/from16 v29, v20

    invoke-virtual/range {v21 .. v30}, Lcom/android/contacts/ext/ContactAccountExtension;->buildValuesForSim(Ljava/lang/String;Landroid/content/Context;Landroid/content/ContentValues;Ljava/util/ArrayList;Ljava/util/ArrayList;IILjava/util/ArrayList;Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_23

    invoke-virtual/range {v25 .. v25}, Ljava/util/ArrayList;->size()I

    move-result v2

    move/from16 v0, v90

    if-ge v2, v0, :cond_2a

    invoke-virtual/range {v25 .. v25}, Ljava/util/ArrayList;->size()I

    move-result v73

    :goto_15
    const/16 v72, 0x0

    :goto_16
    move/from16 v0, v72

    move/from16 v1, v73

    if-ge v0, v1, :cond_23

    const/4 v2, 0x0

    move-object/from16 v0, v25

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;

    move-result-object v38

    check-cast v38, Ljava/lang/String;

    invoke-static/range {v38 .. v38}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_2b

    const-string v38, ""

    :goto_17
    const-string v2, "anr"

    invoke-static/range {v38 .. v38}, Landroid/telephony/PhoneNumberUtils;->stripSeparators(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v15, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    add-int/lit8 v72, v72, 0x1

    goto :goto_16

    :cond_2a
    move/from16 v73, v90

    goto :goto_15

    :cond_2b
    const-string v2, "-"

    const-string v3, ""

    move-object/from16 v0, v38

    invoke-virtual {v0, v2, v3}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v38

    goto :goto_17

    :cond_2c
    const-string v2, "CopyMultiContacts"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Before insert Sim card. values="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    move-object/from16 v0, v58

    invoke-virtual {v9, v0, v15}, Landroid/content/ContentResolver;->insert(Landroid/net/Uri;Landroid/content/ContentValues;)Landroid/net/Uri;

    move-result-object v85

    const-string v2, "CopyMultiContacts"

    const-string v3, "After insert Sim card."

    invoke-static {v2, v3}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    const-string v2, "CopyMultiContacts"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "retUri is "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    move-object/from16 v0, v85

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    if-eqz v85, :cond_36

    invoke-virtual/range {v85 .. v85}, Landroid/net/Uri;->getPathSegments()Ljava/util/List;

    move-result-object v55

    const-string v2, "error"

    const/4 v3, 0x0

    move-object/from16 v0, v55

    invoke-interface {v0, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_34

    const/4 v2, 0x1

    move-object/from16 v0, v55

    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v63

    check-cast v63, Ljava/lang/String;

    const-string v2, "CopyMultiContacts"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "error code = "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    move-object/from16 v0, v63

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    move-object/from16 v0, p0

    move-object/from16 v1, v63

    invoke-direct {v0, v1}, Lcom/mediatek/contacts/list/service/CopyProcessor;->printSimErrorDetails(Ljava/lang/String;)V

    const/4 v2, 0x6

    if-eq v8, v2, :cond_2d

    const/4 v8, 0x1

    :cond_2d
    const-string v2, "-3"

    const/4 v3, 0x1

    move-object/from16 v0, v55

    invoke-interface {v0, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2e

    const/4 v8, -0x3

    const/16 v69, 0x1

    const-string v2, "CopyMultiContacts"

    const-string v3, "Fail to insert sim contacts fail because sim storage is full."

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_14

    :cond_2e
    const-string v2, "-12"

    const/4 v3, 0x1

    move-object/from16 v0, v55

    invoke-interface {v0, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_31

    const/4 v8, 0x6

    const-string v2, "CopyMultiContacts"

    const-string v3, "Fail to save USIM email  because emial slot is full in USIM."

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    const-string v2, "CopyMultiContacts"

    const-string v3, "Ignore this error and remove the email address to save this item again"

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    const-string v2, "emails"

    invoke-virtual {v15, v2}, Landroid/content/ContentValues;->remove(Ljava/lang/String;)V

    move-object/from16 v0, v58

    invoke-virtual {v9, v0, v15}, Landroid/content/ContentResolver;->insert(Landroid/net/Uri;Landroid/content/ContentValues;)Landroid/net/Uri;

    move-result-object v85

    const-string v2, "CopyMultiContacts"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "[Save Again]The retUri is "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    move-object/from16 v0, v85

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    if-eqz v85, :cond_2f

    const-string v2, "error"

    invoke-virtual/range {v85 .. v85}, Landroid/net/Uri;->getPathSegments()Ljava/util/List;

    move-result-object v3

    const/4 v4, 0x0

    invoke-interface {v3, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2f

    const-string v2, "-3"

    invoke-virtual/range {v85 .. v85}, Landroid/net/Uri;->getPathSegments()Ljava/util/List;

    move-result-object v3

    const/4 v4, 0x1

    invoke-interface {v3, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2f

    const/4 v8, -0x3

    const/16 v69, 0x1

    const-string v2, "CopyMultiContacts"

    const-string v3, "Fail to insert sim contacts fail because sim storage is full."

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_14

    :cond_2f
    if-eqz v85, :cond_31

    const-string v2, "error"

    invoke-virtual/range {v85 .. v85}, Landroid/net/Uri;->getPathSegments()Ljava/util/List;

    move-result-object v3

    const/4 v4, 0x0

    invoke-interface {v3, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_31

    invoke-static/range {v85 .. v85}, Landroid/content/ContentUris;->parseId(Landroid/net/Uri;)J

    move-result-wide v43

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/mediatek/contacts/list/service/CopyProcessor;->mAccountDst:Landroid/accounts/Account;

    move-object/from16 v34, v0

    const/16 v37, 0x0

    const/16 v45, 0x0

    move-object/from16 v33, v81

    move-object/from16 v39, v9

    move-object/from16 v46, v20

    move-object/from16 v47, v32

    invoke-static/range {v33 .. v47}, Lcom/mediatek/contacts/extension/aassne/SimUtils;->buildInsertOperation(Ljava/util/ArrayList;Landroid/accounts/Account;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Landroid/content/ContentResolver;JLjava/lang/String;JLjava/util/Set;Ljava/util/ArrayList;Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_30

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/mediatek/contacts/list/service/CopyProcessor;->mAccountDst:Landroid/accounts/Account;

    move-object/from16 v34, v0

    const/16 v37, 0x0

    const/16 v45, 0x0

    move-object/from16 v33, v81

    move-object/from16 v39, v9

    invoke-static/range {v33 .. v45}, Lcom/mediatek/contacts/SubContactsUtils;->buildInsertOperation(Ljava/util/ArrayList;Landroid/accounts/Account;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Landroid/content/ContentResolver;JLjava/lang/String;JLjava/util/Set;)V

    :cond_30
    add-int/lit8 v88, v88, 0x1

    :cond_31
    :goto_18
    invoke-virtual/range {v81 .. v81}, Ljava/util/ArrayList;->size()I

    move-result v2

    const/16 v3, 0x190

    if-le v2, v3, :cond_33

    :try_start_1
    const-string v2, "CopyMultiContacts"

    const-string v3, "Before applyBatch. "

    invoke-static {v2, v3}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    move-object/from16 v0, p0

    move/from16 v1, v19

    invoke-direct {v0, v1}, Lcom/mediatek/contacts/list/service/CopyProcessor;->isSimReady(I)Z

    move-result v2

    if-eqz v2, :cond_32

    move-object/from16 v0, p0

    move/from16 v1, v19

    invoke-direct {v0, v1}, Lcom/mediatek/contacts/list/service/CopyProcessor;->isPhoneBookReady(I)Z

    move-result v2

    if-eqz v2, :cond_32

    const-string v2, "com.android.contacts"

    move-object/from16 v0, v81

    invoke-virtual {v9, v2, v0}, Landroid/content/ContentResolver;->applyBatch(Ljava/lang/String;Ljava/util/ArrayList;)[Landroid/content/ContentProviderResult;

    :cond_32
    const-string v2, "CopyMultiContacts"

    const-string v3, "After applyBatch "

    invoke-static {v2, v3}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_1
    .catch Landroid/os/RemoteException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Landroid/content/OperationApplicationException; {:try_start_1 .. :try_end_1} :catch_1

    :goto_19
    invoke-virtual/range {v81 .. v81}, Ljava/util/ArrayList;->clear()V

    :cond_33
    add-int/lit8 v64, v64, 0x1

    goto/16 :goto_f

    :cond_34
    const-string v2, "CopyMultiContacts"

    const-string v3, "insertUsimFlag = true"

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    invoke-static/range {v85 .. v85}, Landroid/content/ContentUris;->parseId(Landroid/net/Uri;)J

    move-result-wide v43

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/mediatek/contacts/list/service/CopyProcessor;->mAccountDst:Landroid/accounts/Account;

    move-object/from16 v34, v0

    const/16 v45, 0x0

    move-object/from16 v33, v81

    move-object/from16 v37, v87

    move-object/from16 v39, v9

    move-object/from16 v46, v20

    move-object/from16 v47, v32

    invoke-static/range {v33 .. v47}, Lcom/mediatek/contacts/extension/aassne/SimUtils;->buildInsertOperation(Ljava/util/ArrayList;Landroid/accounts/Account;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Landroid/content/ContentResolver;JLjava/lang/String;JLjava/util/Set;Ljava/util/ArrayList;Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_35

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/mediatek/contacts/list/service/CopyProcessor;->mAccountDst:Landroid/accounts/Account;

    move-object/from16 v34, v0

    const/16 v45, 0x0

    move-object/from16 v33, v81

    move-object/from16 v37, v87

    move-object/from16 v39, v9

    invoke-static/range {v33 .. v45}, Lcom/mediatek/contacts/SubContactsUtils;->buildInsertOperation(Ljava/util/ArrayList;Landroid/accounts/Account;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Landroid/content/ContentResolver;JLjava/lang/String;JLjava/util/Set;)V

    :cond_35
    add-int/lit8 v88, v88, 0x1

    goto :goto_18

    :cond_36
    const/4 v8, 0x1

    goto :goto_18

    :catch_0
    move-exception v59

    const-string v2, "CopyMultiContacts"

    const-string v3, "%s: %s"

    const/4 v4, 0x2

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v7, 0x0

    invoke-virtual/range {v59 .. v59}, Ljava/lang/Throwable;->toString()Ljava/lang/String;

    move-result-object v12

    aput-object v12, v4, v7

    const/4 v7, 0x1

    invoke-virtual/range {v59 .. v59}, Ljava/lang/Throwable;->getMessage()Ljava/lang/String;

    move-result-object v12

    aput-object v12, v4, v7

    invoke-static {v3, v4}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_19

    :catch_1
    move-exception v59

    const-string v2, "CopyMultiContacts"

    const-string v3, "%s: %s"

    const/4 v4, 0x2

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v7, 0x0

    invoke-virtual/range {v59 .. v59}, Ljava/lang/Throwable;->toString()Ljava/lang/String;

    move-result-object v12

    aput-object v12, v4, v7

    const/4 v7, 0x1

    invoke-virtual/range {v59 .. v59}, Ljava/lang/Throwable;->getMessage()Ljava/lang/String;

    move-result-object v12

    aput-object v12, v4, v7

    invoke-static {v3, v4}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_19

    :catch_2
    move-exception v59

    const-string v2, "CopyMultiContacts"

    const-string v3, "%s: %s"

    const/4 v4, 0x2

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v7, 0x0

    invoke-virtual/range {v59 .. v59}, Ljava/lang/Throwable;->toString()Ljava/lang/String;

    move-result-object v12

    aput-object v12, v4, v7

    const/4 v7, 0x1

    invoke-virtual/range {v59 .. v59}, Ljava/lang/Throwable;->getMessage()Ljava/lang/String;

    move-result-object v12

    aput-object v12, v4, v7

    invoke-static {v3, v4}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_5

    :catch_3
    move-exception v59

    const-string v2, "CopyMultiContacts"

    const-string v3, "%s: %s"

    const/4 v4, 0x2

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v7, 0x0

    invoke-virtual/range {v59 .. v59}, Ljava/lang/Throwable;->toString()Ljava/lang/String;

    move-result-object v12

    aput-object v12, v4, v7

    const/4 v7, 0x1

    invoke-virtual/range {v59 .. v59}, Ljava/lang/Throwable;->getMessage()Ljava/lang/String;

    move-result-object v12

    aput-object v12, v4, v7

    invoke-static {v3, v4}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_5

    :cond_37
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/mediatek/contacts/list/service/CopyProcessor;->mService:Lcom/mediatek/contacts/list/service/MultiChoiceService;

    move-object/from16 v0, p0

    iget v4, v0, Lcom/mediatek/contacts/list/service/CopyProcessor;->mJobId:I

    if-nez v8, :cond_38

    const/4 v2, 0x1

    :goto_1a
    invoke-virtual {v3, v4, v2}, Lcom/mediatek/contacts/list/service/MultiChoiceService;->handleFinishNotification(IZ)V

    if-nez v8, :cond_39

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/mediatek/contacts/list/service/CopyProcessor;->mListener:Lcom/mediatek/contacts/list/service/MultiChoiceHandlerListener;

    const/4 v3, 0x1

    move-object/from16 v0, p0

    iget v4, v0, Lcom/mediatek/contacts/list/service/CopyProcessor;->mJobId:I

    invoke-virtual {v2, v3, v4, v6}, Lcom/mediatek/contacts/list/service/MultiChoiceHandlerListener;->onFinished(III)V

    goto/16 :goto_2

    :cond_38
    const/4 v2, 0x0

    goto :goto_1a

    :cond_39
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/mediatek/contacts/list/service/CopyProcessor;->mListener:Lcom/mediatek/contacts/list/service/MultiChoiceHandlerListener;

    move-object/from16 v45, v0

    const/16 v46, 0x1

    move-object/from16 v0, p0

    iget v0, v0, Lcom/mediatek/contacts/list/service/CopyProcessor;->mJobId:I

    move/from16 v47, v0

    sub-int v50, v6, v49

    move/from16 v48, v6

    move/from16 v51, v8

    invoke-virtual/range {v45 .. v51}, Lcom/mediatek/contacts/list/service/MultiChoiceHandlerListener;->onFailed(IIIIII)V

    goto/16 :goto_2
.end method

.method private copyContactsToSimWithRadioStateCheck()V
    .locals 12

    const/4 v1, 0x1

    const/4 v4, 0x0

    iget-boolean v0, p0, Lcom/mediatek/contacts/list/service/CopyProcessor;->mCanceled:Z

    if-eqz v0, :cond_0

    :goto_0
    return-void

    :cond_0
    const/4 v6, 0x0

    iget-object v7, p0, Lcom/mediatek/contacts/list/service/CopyProcessor;->mAccountDst:Landroid/accounts/Account;

    check-cast v7, Lcom/mediatek/contacts/model/AccountWithDataSetEx;

    const-string v0, "CopyMultiContacts"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "[copyContactsToSimWithRadioCheck]AccountName: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, v7, Landroid/accounts/Account;->name:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " | accountType: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, v7, Landroid/accounts/Account;->type:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    invoke-virtual {v7}, Lcom/mediatek/contacts/model/AccountWithDataSetEx;->getSlotId()I

    move-result v8

    invoke-direct {p0, v8}, Lcom/mediatek/contacts/list/service/CopyProcessor;->isSimReady(I)Z

    move-result v0

    if-nez v0, :cond_1

    const/4 v6, 0x3

    iget-object v0, p0, Lcom/mediatek/contacts/list/service/CopyProcessor;->mService:Lcom/mediatek/contacts/list/service/MultiChoiceService;

    iget v2, p0, Lcom/mediatek/contacts/list/service/CopyProcessor;->mJobId:I

    invoke-virtual {v0, v2, v4}, Lcom/mediatek/contacts/list/service/MultiChoiceService;->handleFinishNotification(IZ)V

    iget-object v0, p0, Lcom/mediatek/contacts/list/service/CopyProcessor;->mListener:Lcom/mediatek/contacts/list/service/MultiChoiceHandlerListener;

    iget v2, p0, Lcom/mediatek/contacts/list/service/CopyProcessor;->mJobId:I

    iget-object v3, p0, Lcom/mediatek/contacts/list/service/CopyProcessor;->mRequests:Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v3

    iget-object v5, p0, Lcom/mediatek/contacts/list/service/CopyProcessor;->mRequests:Ljava/util/List;

    invoke-interface {v5}, Ljava/util/List;->size()I

    move-result v5

    invoke-virtual/range {v0 .. v6}, Lcom/mediatek/contacts/list/service/MultiChoiceHandlerListener;->onFailed(IIIIII)V

    goto :goto_0

    :cond_1
    invoke-direct {p0, v8}, Lcom/mediatek/contacts/list/service/CopyProcessor;->isPhoneBookReady(I)Z

    move-result v0

    if-nez v0, :cond_2

    const/4 v10, 0x0

    move v11, v10

    :goto_1
    add-int/lit8 v10, v11, 0x1

    const/16 v0, 0x14

    if-ge v11, v0, :cond_2

    const-wide/16 v2, 0x1f4

    :try_start_0
    invoke-static {v2, v3}, Ljava/lang/Thread;->sleep(J)V
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_2
    invoke-direct {p0, v8}, Lcom/mediatek/contacts/list/service/CopyProcessor;->isPhoneBookReady(I)Z

    move-result v0

    if-eqz v0, :cond_4

    :cond_2
    invoke-direct {p0, v8}, Lcom/mediatek/contacts/list/service/CopyProcessor;->isPhoneBookReady(I)Z

    move-result v0

    if-nez v0, :cond_3

    const/4 v6, 0x3

    iget-object v0, p0, Lcom/mediatek/contacts/list/service/CopyProcessor;->mService:Lcom/mediatek/contacts/list/service/MultiChoiceService;

    iget v2, p0, Lcom/mediatek/contacts/list/service/CopyProcessor;->mJobId:I

    invoke-virtual {v0, v2, v4}, Lcom/mediatek/contacts/list/service/MultiChoiceService;->handleFinishNotification(IZ)V

    iget-object v0, p0, Lcom/mediatek/contacts/list/service/CopyProcessor;->mListener:Lcom/mediatek/contacts/list/service/MultiChoiceHandlerListener;

    iget v2, p0, Lcom/mediatek/contacts/list/service/CopyProcessor;->mJobId:I

    iget-object v3, p0, Lcom/mediatek/contacts/list/service/CopyProcessor;->mRequests:Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v3

    iget-object v5, p0, Lcom/mediatek/contacts/list/service/CopyProcessor;->mRequests:Ljava/util/List;

    invoke-interface {v5}, Ljava/util/List;->size()I

    move-result v5

    invoke-virtual/range {v0 .. v6}, Lcom/mediatek/contacts/list/service/MultiChoiceHandlerListener;->onFailed(IIIIII)V

    goto/16 :goto_0

    :catch_0
    move-exception v9

    invoke-virtual {v9}, Ljava/lang/Throwable;->printStackTrace()V

    goto :goto_2

    :cond_3
    invoke-direct {p0}, Lcom/mediatek/contacts/list/service/CopyProcessor;->copyContactsToSim()V

    goto/16 :goto_0

    :cond_4
    move v11, v10

    goto :goto_1
.end method

.method private cursorColumnToBuilder(Landroid/database/Cursor;[Ljava/lang/String;ILandroid/content/ContentProviderOperation$Builder;)V
    .locals 3
    .param p1    # Landroid/database/Cursor;
    .param p2    # [Ljava/lang/String;
    .param p3    # I
    .param p4    # Landroid/content/ContentProviderOperation$Builder;

    invoke-interface {p1, p3}, Landroid/database/Cursor;->getType(I)I

    move-result v0

    packed-switch v0, :pswitch_data_0

    :pswitch_0
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Invalid or unhandled data type"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    :pswitch_1
    aget-object v0, p2, p3

    invoke-interface {p1, p3}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v1

    invoke-static {v1, v2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-virtual {p4, v0, v1}, Landroid/content/ContentProviderOperation$Builder;->withValue(Ljava/lang/String;Ljava/lang/Object;)Landroid/content/ContentProviderOperation$Builder;

    :goto_0
    :pswitch_2
    return-void

    :pswitch_3
    aget-object v0, p2, p3

    invoke-interface {p1, p3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p4, v0, v1}, Landroid/content/ContentProviderOperation$Builder;->withValue(Ljava/lang/String;Ljava/lang/Object;)Landroid/content/ContentProviderOperation$Builder;

    goto :goto_0

    :pswitch_4
    aget-object v0, p2, p3

    invoke-interface {p1, p3}, Landroid/database/Cursor;->getBlob(I)[B

    move-result-object v1

    invoke-virtual {p4, v0, v1}, Landroid/content/ContentProviderOperation$Builder;->withValue(Ljava/lang/String;Ljava/lang/Object;)Landroid/content/ContentProviderOperation$Builder;

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_2
        :pswitch_1
        :pswitch_0
        :pswitch_3
        :pswitch_4
    .end packed-switch
.end method

.method private generateDataBuilder(Landroid/database/Cursor;Landroid/content/ContentProviderOperation$Builder;[Ljava/lang/String;Ljava/lang/String;ILjava/lang/String;)V
    .locals 10
    .param p1    # Landroid/database/Cursor;
    .param p2    # Landroid/content/ContentProviderOperation$Builder;
    .param p3    # [Ljava/lang/String;
    .param p4    # Ljava/lang/String;
    .param p5    # I
    .param p6    # Ljava/lang/String;

    const/4 v8, 0x1

    :goto_0
    array-length v0, p3

    if-ge v8, v0, :cond_1

    invoke-static {}, Lcom/mediatek/contacts/ExtensionManager;->getInstance()Lcom/mediatek/contacts/ExtensionManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/mediatek/contacts/ExtensionManager;->getContactListExtension()Lcom/android/contacts/ext/ContactListExtension;

    move-result-object v0

    iget-object v1, p0, Lcom/mediatek/contacts/list/service/CopyProcessor;->mService:Lcom/mediatek/contacts/list/service/MultiChoiceService;

    invoke-virtual {v1}, Landroid/content/ContextWrapper;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    const-string v9, "ExtensionForAAS"

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object/from16 v5, p6

    move-object v6, p4

    move v7, p5

    invoke-virtual/range {v0 .. v9}, Lcom/android/contacts/ext/ContactListExtension;->generateDataBuilder(Landroid/content/Context;Landroid/database/Cursor;Landroid/content/ContentProviderOperation$Builder;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;IILjava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    :goto_1
    add-int/lit8 v8, v8, 0x1

    goto :goto_0

    :cond_0
    invoke-direct {p0, p1, p3, v8, p2}, Lcom/mediatek/contacts/list/service/CopyProcessor;->cursorColumnToBuilder(Landroid/database/Cursor;[Ljava/lang/String;ILandroid/content/ContentProviderOperation$Builder;)V

    goto :goto_1

    :cond_1
    return-void
.end method

.method private isPhoneBookReady(I)Z
    .locals 3
    .param p1    # I

    const-string v0, "CopyMultiContacts"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "isPhoneBookReady "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-static {p1}, Lcom/mediatek/contacts/simcontact/SimCardUtils;->isPhoneBookReady(I)Z

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    invoke-static {p1}, Lcom/mediatek/contacts/simcontact/SimCardUtils;->isPhoneBookReady(I)Z

    move-result v0

    return v0
.end method

.method private isSimReady(I)Z
    .locals 9
    .param p1    # I

    invoke-static {p1}, Lcom/mediatek/contacts/simcontact/SimCardUtils;->isSimPukRequest(I)Z

    move-result v5

    invoke-static {p1}, Lcom/mediatek/contacts/simcontact/SimCardUtils;->isSimPinRequest(I)Z

    move-result v4

    invoke-static {p1}, Lcom/mediatek/contacts/simcontact/SimCardUtils;->isSimInserted(I)Z

    move-result v3

    iget-object v6, p0, Lcom/mediatek/contacts/list/service/CopyProcessor;->mResolver:Landroid/content/ContentResolver;

    invoke-static {v6, p1}, Lcom/mediatek/contacts/simcontact/SimCardUtils;->isSetRadioOn(Landroid/content/ContentResolver;I)Z

    move-result v1

    invoke-static {p1}, Lcom/mediatek/contacts/simcontact/SimCardUtils;->isFdnEnabed(I)Z

    move-result v0

    invoke-static {}, Lcom/mediatek/contacts/simcontact/SimCardUtils;->isSimInfoReady()Z

    move-result v2

    const-string v6, "CopyMultiContacts"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "[checkSimState]slotId:"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, "||simPUKReq: "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v5}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, "||simPINReq: "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v4}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, "||isRadioOn: "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, "||isFdnEnabled: "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, "||simInserted: "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, "||isSimInfoReady:"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    if-nez v5, :cond_0

    if-eqz v1, :cond_0

    if-nez v0, :cond_0

    if-nez v4, :cond_0

    if-eqz v3, :cond_0

    if-eqz v2, :cond_0

    const/4 v6, 0x1

    :goto_0
    return v6

    :cond_0
    const/4 v6, 0x0

    goto :goto_0
.end method

.method private printSimErrorDetails(Ljava/lang/String;)V
    .locals 3
    .param p1    # Ljava/lang/String;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    :pswitch_0
    const-string v1, "CopyMultiContacts"

    const-string v2, "ERROR ICC UNKNOW"

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :goto_0
    return-void

    :pswitch_1
    const-string v1, "CopyMultiContacts"

    const-string v2, "ERROR PHONE NUMBER TOO LONG"

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    :pswitch_2
    const-string v1, "CopyMultiContacts"

    const-string v2, "ERROR NAME TOO LONG"

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    :pswitch_3
    const-string v1, "CopyMultiContacts"

    const-string v2, "ERROR STORAGE FULL"

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    :pswitch_4
    const-string v1, "CopyMultiContacts"

    const-string v2, "ERROR ICC NOT READY"

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    :pswitch_5
    const-string v1, "CopyMultiContacts"

    const-string v2, "ERROR ICC PASSWORD ERROR"

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    :pswitch_6
    const-string v1, "CopyMultiContacts"

    const-string v2, "ERROR ICC ANR TOO LONG"

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    :pswitch_7
    const-string v1, "CopyMultiContacts"

    const-string v2, "ERROR ICC GENERIC FAILURE"

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    :pswitch_8
    const-string v1, "CopyMultiContacts"

    const-string v2, "ERROR ICC ADN LIST NOT EXIST"

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    :pswitch_9
    const-string v1, "CopyMultiContacts"

    const-string v2, "ERROR ICC USIM EMAIL LOST"

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch -0xb
        :pswitch_8
        :pswitch_7
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_6
        :pswitch_5
        :pswitch_4
        :pswitch_3
        :pswitch_2
        :pswitch_1
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_9
    .end packed-switch
.end method


# virtual methods
.method public declared-synchronized cancel(Z)Z
    .locals 7
    .param p1    # Z

    const/4 v0, 0x0

    const/4 v6, 0x1

    monitor-enter p0

    :try_start_0
    const-string v1, "CopyMultiContacts"

    const-string v2, "CopyProcessor received cancel request"

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-boolean v1, p0, Lcom/mediatek/contacts/list/service/CopyProcessor;->mDone:Z

    if-nez v1, :cond_0

    iget-boolean v1, p0, Lcom/mediatek/contacts/list/service/CopyProcessor;->mCanceled:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-eqz v1, :cond_1

    :cond_0
    :goto_0
    monitor-exit p0

    return v0

    :cond_1
    const/4 v0, 0x1

    :try_start_1
    iput-boolean v0, p0, Lcom/mediatek/contacts/list/service/CopyProcessor;->mCanceled:Z

    iget-boolean v0, p0, Lcom/mediatek/contacts/list/service/CopyProcessor;->mIsRunning:Z

    if-nez v0, :cond_2

    iget-object v0, p0, Lcom/mediatek/contacts/list/service/CopyProcessor;->mService:Lcom/mediatek/contacts/list/service/MultiChoiceService;

    iget v1, p0, Lcom/mediatek/contacts/list/service/CopyProcessor;->mJobId:I

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Lcom/mediatek/contacts/list/service/MultiChoiceService;->handleFinishNotification(IZ)V

    iget-object v0, p0, Lcom/mediatek/contacts/list/service/CopyProcessor;->mListener:Lcom/mediatek/contacts/list/service/MultiChoiceHandlerListener;

    const/4 v1, 0x1

    iget v2, p0, Lcom/mediatek/contacts/list/service/CopyProcessor;->mJobId:I

    const/4 v3, -0x1

    const/4 v4, -0x1

    const/4 v5, -0x1

    invoke-virtual/range {v0 .. v5}, Lcom/mediatek/contacts/list/service/MultiChoiceHandlerListener;->onCanceled(IIIII)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    :cond_2
    move v0, v6

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public getType()I
    .locals 1

    const/4 v0, 0x1

    return v0
.end method

.method public declared-synchronized isCancelled()Z
    .locals 1

    monitor-enter p0

    :try_start_0
    iget-boolean v0, p0, Lcom/mediatek/contacts/list/service/CopyProcessor;->mCanceled:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized isDone()Z
    .locals 1

    monitor-enter p0

    :try_start_0
    iget-boolean v0, p0, Lcom/mediatek/contacts/list/service/CopyProcessor;->mDone:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public run()V
    .locals 2

    const/4 v0, 0x1

    :try_start_0
    iput-boolean v0, p0, Lcom/mediatek/contacts/list/service/CopyProcessor;->mIsRunning:Z

    iget-object v0, p0, Lcom/mediatek/contacts/list/service/CopyProcessor;->mWakeLock:Landroid/os/PowerManager$WakeLock;

    invoke-virtual {v0}, Landroid/os/PowerManager$WakeLock;->acquire()V

    const-string v0, "SIM Account"

    iget-object v1, p0, Lcom/mediatek/contacts/list/service/CopyProcessor;->mAccountDst:Landroid/accounts/Account;

    iget-object v1, v1, Landroid/accounts/Account;->type:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    const-string v0, "USIM Account"

    iget-object v1, p0, Lcom/mediatek/contacts/list/service/CopyProcessor;->mAccountDst:Landroid/accounts/Account;

    iget-object v1, v1, Landroid/accounts/Account;->type:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    const-string v0, "UIM Account"

    iget-object v1, p0, Lcom/mediatek/contacts/list/service/CopyProcessor;->mAccountDst:Landroid/accounts/Account;

    iget-object v1, v1, Landroid/accounts/Account;->type:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    :cond_0
    invoke-direct {p0}, Lcom/mediatek/contacts/list/service/CopyProcessor;->copyContactsToSimWithRadioStateCheck()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :goto_0
    monitor-enter p0

    const/4 v0, 0x1

    :try_start_1
    iput-boolean v0, p0, Lcom/mediatek/contacts/list/service/CopyProcessor;->mDone:Z

    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    iget-object v0, p0, Lcom/mediatek/contacts/list/service/CopyProcessor;->mWakeLock:Landroid/os/PowerManager$WakeLock;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/mediatek/contacts/list/service/CopyProcessor;->mWakeLock:Landroid/os/PowerManager$WakeLock;

    invoke-virtual {v0}, Landroid/os/PowerManager$WakeLock;->isHeld()Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/mediatek/contacts/list/service/CopyProcessor;->mWakeLock:Landroid/os/PowerManager$WakeLock;

    invoke-virtual {v0}, Landroid/os/PowerManager$WakeLock;->release()V

    :cond_1
    return-void

    :cond_2
    :try_start_2
    invoke-direct {p0}, Lcom/mediatek/contacts/list/service/CopyProcessor;->copyContactsToAccount()V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-enter p0

    const/4 v1, 0x1

    :try_start_3
    iput-boolean v1, p0, Lcom/mediatek/contacts/list/service/CopyProcessor;->mDone:Z

    monitor-exit p0
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_2

    iget-object v1, p0, Lcom/mediatek/contacts/list/service/CopyProcessor;->mWakeLock:Landroid/os/PowerManager$WakeLock;

    if-eqz v1, :cond_3

    iget-object v1, p0, Lcom/mediatek/contacts/list/service/CopyProcessor;->mWakeLock:Landroid/os/PowerManager$WakeLock;

    invoke-virtual {v1}, Landroid/os/PowerManager$WakeLock;->isHeld()Z

    move-result v1

    if-eqz v1, :cond_3

    iget-object v1, p0, Lcom/mediatek/contacts/list/service/CopyProcessor;->mWakeLock:Landroid/os/PowerManager$WakeLock;

    invoke-virtual {v1}, Landroid/os/PowerManager$WakeLock;->release()V

    :cond_3
    throw v0

    :catchall_1
    move-exception v0

    :try_start_4
    monitor-exit p0
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    throw v0

    :catchall_2
    move-exception v0

    :try_start_5
    monitor-exit p0
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_2

    throw v0
.end method
