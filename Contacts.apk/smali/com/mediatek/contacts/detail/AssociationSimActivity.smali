.class public Lcom/mediatek/contacts/detail/AssociationSimActivity;
.super Lcom/android/contacts/ContactsActivity;
.source "AssociationSimActivity.java"

# interfaces
.implements Landroid/view/View$OnClickListener;
.implements Lcom/android/contacts/util/NotifyingAsyncQueryHandler$AsyncQueryListener;
.implements Lcom/android/contacts/util/NotifyingAsyncQueryHandler$AsyncUpdateListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/mediatek/contacts/detail/AssociationSimActivity$ContactDetailInfo;,
        Lcom/mediatek/contacts/detail/AssociationSimActivity$SimInfoMgr;,
        Lcom/mediatek/contacts/detail/AssociationSimActivity$NumberInfoMgr;,
        Lcom/mediatek/contacts/detail/AssociationSimActivity$NumberInfoAdapter;,
        Lcom/mediatek/contacts/detail/AssociationSimActivity$ListViewAdapter;
    }
.end annotation


# static fields
.field public static final INTENT_DATA_KEY_CONTACT_DETAIL_INFO:Ljava/lang/String; = "contact_detail_info"

.field public static final INTENT_DATA_KEY_SEL_DATA_ID:Ljava/lang/String; = "sel_data_id"

.field public static final INTENT_DATA_KEY_SEL_SIM_ID:Ljava/lang/String; = "sel_sim_id"

.field private static final TAG:Ljava/lang/String; = "AssociationSimActivity"


# instance fields
.field private mActionBar:Landroid/app/ActionBar;

.field private mAdapterChildSize:I

.field private mBtnCancel:Landroid/widget/Button;

.field private mBtnSave:Landroid/widget/Button;

.field private mContactDetailInfo:Lcom/mediatek/contacts/detail/AssociationSimActivity$ContactDetailInfo;

.field private mContactPhoto:Landroid/widget/ImageView;

.field private mHandler:Lcom/android/contacts/util/NotifyingAsyncQueryHandler;

.field private mInitDataId:J

.field private mInitSimId:I

.field private mListView:Landroid/widget/ListView;

.field private mNumberInfoMgr:Lcom/mediatek/contacts/detail/AssociationSimActivity$NumberInfoMgr;

.field private mOnListViewItemClick:Landroid/widget/AdapterView$OnItemClickListener;

.field private mSelectDialog:Landroid/app/AlertDialog;

.field private mSelectDialogType:I

.field private mSimInfoMgr:Lcom/mediatek/contacts/detail/AssociationSimActivity$SimInfoMgr;


# direct methods
.method public constructor <init>()V
    .locals 3

    const/4 v2, 0x0

    invoke-direct {p0}, Lcom/android/contacts/ContactsActivity;-><init>()V

    iput-object v2, p0, Lcom/mediatek/contacts/detail/AssociationSimActivity;->mActionBar:Landroid/app/ActionBar;

    iput-object v2, p0, Lcom/mediatek/contacts/detail/AssociationSimActivity;->mContactPhoto:Landroid/widget/ImageView;

    iput-object v2, p0, Lcom/mediatek/contacts/detail/AssociationSimActivity;->mListView:Landroid/widget/ListView;

    iput-object v2, p0, Lcom/mediatek/contacts/detail/AssociationSimActivity;->mBtnSave:Landroid/widget/Button;

    iput-object v2, p0, Lcom/mediatek/contacts/detail/AssociationSimActivity;->mBtnCancel:Landroid/widget/Button;

    iput-object v2, p0, Lcom/mediatek/contacts/detail/AssociationSimActivity;->mHandler:Lcom/android/contacts/util/NotifyingAsyncQueryHandler;

    new-instance v0, Lcom/mediatek/contacts/detail/AssociationSimActivity$SimInfoMgr;

    invoke-direct {v0, p0}, Lcom/mediatek/contacts/detail/AssociationSimActivity$SimInfoMgr;-><init>(Lcom/mediatek/contacts/detail/AssociationSimActivity;)V

    iput-object v0, p0, Lcom/mediatek/contacts/detail/AssociationSimActivity;->mSimInfoMgr:Lcom/mediatek/contacts/detail/AssociationSimActivity$SimInfoMgr;

    new-instance v0, Lcom/mediatek/contacts/detail/AssociationSimActivity$NumberInfoMgr;

    invoke-direct {v0, p0}, Lcom/mediatek/contacts/detail/AssociationSimActivity$NumberInfoMgr;-><init>(Lcom/mediatek/contacts/detail/AssociationSimActivity;)V

    iput-object v0, p0, Lcom/mediatek/contacts/detail/AssociationSimActivity;->mNumberInfoMgr:Lcom/mediatek/contacts/detail/AssociationSimActivity$NumberInfoMgr;

    const-wide/16 v0, -0x1

    iput-wide v0, p0, Lcom/mediatek/contacts/detail/AssociationSimActivity;->mInitDataId:J

    const/4 v0, -0x1

    iput v0, p0, Lcom/mediatek/contacts/detail/AssociationSimActivity;->mInitSimId:I

    const/4 v0, 0x2

    iput v0, p0, Lcom/mediatek/contacts/detail/AssociationSimActivity;->mAdapterChildSize:I

    iput-object v2, p0, Lcom/mediatek/contacts/detail/AssociationSimActivity;->mSelectDialog:Landroid/app/AlertDialog;

    const/4 v0, 0x0

    iput v0, p0, Lcom/mediatek/contacts/detail/AssociationSimActivity;->mSelectDialogType:I

    new-instance v0, Lcom/mediatek/contacts/detail/AssociationSimActivity$1;

    invoke-direct {v0, p0}, Lcom/mediatek/contacts/detail/AssociationSimActivity$1;-><init>(Lcom/mediatek/contacts/detail/AssociationSimActivity;)V

    iput-object v0, p0, Lcom/mediatek/contacts/detail/AssociationSimActivity;->mOnListViewItemClick:Landroid/widget/AdapterView$OnItemClickListener;

    return-void
.end method

.method static synthetic access$000(Lcom/mediatek/contacts/detail/AssociationSimActivity;)Lcom/mediatek/contacts/detail/AssociationSimActivity$SimInfoMgr;
    .locals 1
    .param p0    # Lcom/mediatek/contacts/detail/AssociationSimActivity;

    iget-object v0, p0, Lcom/mediatek/contacts/detail/AssociationSimActivity;->mSimInfoMgr:Lcom/mediatek/contacts/detail/AssociationSimActivity$SimInfoMgr;

    return-object v0
.end method

.method static synthetic access$100(Lcom/mediatek/contacts/detail/AssociationSimActivity;)I
    .locals 1
    .param p0    # Lcom/mediatek/contacts/detail/AssociationSimActivity;

    iget v0, p0, Lcom/mediatek/contacts/detail/AssociationSimActivity;->mSelectDialogType:I

    return v0
.end method

.method static synthetic access$1000(Lcom/mediatek/contacts/detail/AssociationSimActivity;)I
    .locals 1
    .param p0    # Lcom/mediatek/contacts/detail/AssociationSimActivity;

    iget v0, p0, Lcom/mediatek/contacts/detail/AssociationSimActivity;->mAdapterChildSize:I

    return v0
.end method

.method static synthetic access$102(Lcom/mediatek/contacts/detail/AssociationSimActivity;I)I
    .locals 0
    .param p0    # Lcom/mediatek/contacts/detail/AssociationSimActivity;
    .param p1    # I

    iput p1, p0, Lcom/mediatek/contacts/detail/AssociationSimActivity;->mSelectDialogType:I

    return p1
.end method

.method static synthetic access$200(Lcom/mediatek/contacts/detail/AssociationSimActivity;)Lcom/mediatek/contacts/detail/AssociationSimActivity$NumberInfoMgr;
    .locals 1
    .param p0    # Lcom/mediatek/contacts/detail/AssociationSimActivity;

    iget-object v0, p0, Lcom/mediatek/contacts/detail/AssociationSimActivity;->mNumberInfoMgr:Lcom/mediatek/contacts/detail/AssociationSimActivity$NumberInfoMgr;

    return-object v0
.end method

.method static synthetic access$302(Lcom/mediatek/contacts/detail/AssociationSimActivity;J)J
    .locals 0
    .param p0    # Lcom/mediatek/contacts/detail/AssociationSimActivity;
    .param p1    # J

    iput-wide p1, p0, Lcom/mediatek/contacts/detail/AssociationSimActivity;->mInitDataId:J

    return-wide p1
.end method

.method static synthetic access$400(Lcom/mediatek/contacts/detail/AssociationSimActivity;)Landroid/app/AlertDialog;
    .locals 1
    .param p0    # Lcom/mediatek/contacts/detail/AssociationSimActivity;

    iget-object v0, p0, Lcom/mediatek/contacts/detail/AssociationSimActivity;->mSelectDialog:Landroid/app/AlertDialog;

    return-object v0
.end method

.method static synthetic access$402(Lcom/mediatek/contacts/detail/AssociationSimActivity;Landroid/app/AlertDialog;)Landroid/app/AlertDialog;
    .locals 0
    .param p0    # Lcom/mediatek/contacts/detail/AssociationSimActivity;
    .param p1    # Landroid/app/AlertDialog;

    iput-object p1, p0, Lcom/mediatek/contacts/detail/AssociationSimActivity;->mSelectDialog:Landroid/app/AlertDialog;

    return-object p1
.end method

.method static synthetic access$502(Lcom/mediatek/contacts/detail/AssociationSimActivity;I)I
    .locals 0
    .param p0    # Lcom/mediatek/contacts/detail/AssociationSimActivity;
    .param p1    # I

    iput p1, p0, Lcom/mediatek/contacts/detail/AssociationSimActivity;->mInitSimId:I

    return p1
.end method

.method static synthetic access$600(Lcom/mediatek/contacts/detail/AssociationSimActivity;)Landroid/widget/ListView;
    .locals 1
    .param p0    # Lcom/mediatek/contacts/detail/AssociationSimActivity;

    iget-object v0, p0, Lcom/mediatek/contacts/detail/AssociationSimActivity;->mListView:Landroid/widget/ListView;

    return-object v0
.end method

.method static synthetic access$700(Lcom/mediatek/contacts/detail/AssociationSimActivity;)Z
    .locals 1
    .param p0    # Lcom/mediatek/contacts/detail/AssociationSimActivity;

    invoke-direct {p0}, Lcom/mediatek/contacts/detail/AssociationSimActivity;->isSimInfoChanged()Z

    move-result v0

    return v0
.end method

.method static synthetic access$800(Lcom/mediatek/contacts/detail/AssociationSimActivity;)Landroid/widget/Button;
    .locals 1
    .param p0    # Lcom/mediatek/contacts/detail/AssociationSimActivity;

    iget-object v0, p0, Lcom/mediatek/contacts/detail/AssociationSimActivity;->mBtnSave:Landroid/widget/Button;

    return-object v0
.end method

.method static synthetic access$900(Lcom/mediatek/contacts/detail/AssociationSimActivity;)Lcom/mediatek/contacts/detail/AssociationSimActivity$ContactDetailInfo;
    .locals 1
    .param p0    # Lcom/mediatek/contacts/detail/AssociationSimActivity;

    iget-object v0, p0, Lcom/mediatek/contacts/detail/AssociationSimActivity;->mContactDetailInfo:Lcom/mediatek/contacts/detail/AssociationSimActivity$ContactDetailInfo;

    return-object v0
.end method

.method private isSimInfoChanged()Z
    .locals 3

    const/4 v0, 0x0

    iget-object v1, p0, Lcom/mediatek/contacts/detail/AssociationSimActivity;->mNumberInfoMgr:Lcom/mediatek/contacts/detail/AssociationSimActivity$NumberInfoMgr;

    iget-object v1, v1, Lcom/mediatek/contacts/detail/AssociationSimActivity$NumberInfoMgr;->mNumberInfoList:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    if-lez v1, :cond_0

    iget-object v1, p0, Lcom/mediatek/contacts/detail/AssociationSimActivity;->mSimInfoMgr:Lcom/mediatek/contacts/detail/AssociationSimActivity$SimInfoMgr;

    iget-object v1, v1, Lcom/mediatek/contacts/detail/AssociationSimActivity$SimInfoMgr;->mSimInfoList:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    if-lez v1, :cond_0

    iget-object v1, p0, Lcom/mediatek/contacts/detail/AssociationSimActivity;->mNumberInfoMgr:Lcom/mediatek/contacts/detail/AssociationSimActivity$NumberInfoMgr;

    invoke-virtual {v1}, Lcom/mediatek/contacts/detail/AssociationSimActivity$NumberInfoMgr;->getShowingNumberSimId()I

    move-result v1

    iget-object v2, p0, Lcom/mediatek/contacts/detail/AssociationSimActivity;->mSimInfoMgr:Lcom/mediatek/contacts/detail/AssociationSimActivity$SimInfoMgr;

    invoke-virtual {v2}, Lcom/mediatek/contacts/detail/AssociationSimActivity$SimInfoMgr;->getShowingSimId()I

    move-result v2

    if-eq v1, v2, :cond_0

    const/4 v0, 0x1

    :cond_0
    return v0
.end method

.method private returnToContactDetail(Z)V
    .locals 3
    .param p1    # Z

    const-string v0, "AssociationSimActivity"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "returnToContactDetail : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    invoke-virtual {p0}, Landroid/app/Activity;->finish()V

    return-void
.end method


# virtual methods
.method public closeSelectDialog()V
    .locals 1

    iget-object v0, p0, Lcom/mediatek/contacts/detail/AssociationSimActivity;->mSelectDialog:Landroid/app/AlertDialog;

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Landroid/app/Activity;->isFinishing()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/mediatek/contacts/detail/AssociationSimActivity;->mSelectDialog:Landroid/app/AlertDialog;

    invoke-virtual {v0}, Landroid/app/Dialog;->dismiss()V

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/mediatek/contacts/detail/AssociationSimActivity;->mSelectDialog:Landroid/app/AlertDialog;

    :cond_0
    return-void
.end method

.method public initView()V
    .locals 3

    invoke-virtual {p0}, Landroid/app/Activity;->getActionBar()Landroid/app/ActionBar;

    move-result-object v0

    iput-object v0, p0, Lcom/mediatek/contacts/detail/AssociationSimActivity;->mActionBar:Landroid/app/ActionBar;

    iget-object v0, p0, Lcom/mediatek/contacts/detail/AssociationSimActivity;->mActionBar:Landroid/app/ActionBar;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/mediatek/contacts/detail/AssociationSimActivity;->mActionBar:Landroid/app/ActionBar;

    const/16 v1, 0xc

    const/16 v2, 0xe

    invoke-virtual {v0, v1, v2}, Landroid/app/ActionBar;->setDisplayOptions(II)V

    iget-object v0, p0, Lcom/mediatek/contacts/detail/AssociationSimActivity;->mActionBar:Landroid/app/ActionBar;

    iget-object v1, p0, Lcom/mediatek/contacts/detail/AssociationSimActivity;->mContactDetailInfo:Lcom/mediatek/contacts/detail/AssociationSimActivity$ContactDetailInfo;

    iget-object v1, v1, Lcom/mediatek/contacts/detail/AssociationSimActivity$ContactDetailInfo;->mDisplayTitle:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/app/ActionBar;->setTitle(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lcom/mediatek/contacts/detail/AssociationSimActivity;->mContactDetailInfo:Lcom/mediatek/contacts/detail/AssociationSimActivity$ContactDetailInfo;

    iget-object v0, v0, Lcom/mediatek/contacts/detail/AssociationSimActivity$ContactDetailInfo;->mDisplaySubtitle:Ljava/lang/String;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/mediatek/contacts/detail/AssociationSimActivity;->mContactDetailInfo:Lcom/mediatek/contacts/detail/AssociationSimActivity$ContactDetailInfo;

    iget-object v0, v0, Lcom/mediatek/contacts/detail/AssociationSimActivity$ContactDetailInfo;->mDisplaySubtitle:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/mediatek/contacts/detail/AssociationSimActivity;->mActionBar:Landroid/app/ActionBar;

    iget-object v1, p0, Lcom/mediatek/contacts/detail/AssociationSimActivity;->mContactDetailInfo:Lcom/mediatek/contacts/detail/AssociationSimActivity$ContactDetailInfo;

    iget-object v1, v1, Lcom/mediatek/contacts/detail/AssociationSimActivity$ContactDetailInfo;->mDisplaySubtitle:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/app/ActionBar;->setSubtitle(Ljava/lang/CharSequence;)V

    :cond_0
    const v0, 0x7f070038

    invoke-virtual {p0, v0}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/mediatek/contacts/detail/AssociationSimActivity;->mContactPhoto:Landroid/widget/ImageView;

    iget-object v0, p0, Lcom/mediatek/contacts/detail/AssociationSimActivity;->mContactPhoto:Landroid/widget/ImageView;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/mediatek/contacts/detail/AssociationSimActivity;->mContactDetailInfo:Lcom/mediatek/contacts/detail/AssociationSimActivity$ContactDetailInfo;

    iget-object v1, p0, Lcom/mediatek/contacts/detail/AssociationSimActivity;->mContactPhoto:Landroid/widget/ImageView;

    invoke-virtual {v0, v1}, Lcom/mediatek/contacts/detail/AssociationSimActivity$ContactDetailInfo;->setPhoto(Landroid/widget/ImageView;)V

    iget-object v0, p0, Lcom/mediatek/contacts/detail/AssociationSimActivity;->mContactPhoto:Landroid/widget/ImageView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setClickable(Z)V

    :cond_1
    const v0, 0x7f070039

    invoke-virtual {p0, v0}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ListView;

    iput-object v0, p0, Lcom/mediatek/contacts/detail/AssociationSimActivity;->mListView:Landroid/widget/ListView;

    const v0, 0x7f07003b

    invoke-virtual {p0, v0}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lcom/mediatek/contacts/detail/AssociationSimActivity;->mBtnSave:Landroid/widget/Button;

    const v0, 0x7f07003a

    invoke-virtual {p0, v0}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lcom/mediatek/contacts/detail/AssociationSimActivity;->mBtnCancel:Landroid/widget/Button;

    iget-object v0, p0, Lcom/mediatek/contacts/detail/AssociationSimActivity;->mBtnSave:Landroid/widget/Button;

    invoke-virtual {v0, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v0, p0, Lcom/mediatek/contacts/detail/AssociationSimActivity;->mBtnCancel:Landroid/widget/Button;

    invoke-virtual {v0, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    return-void
.end method

.method public onClick(Landroid/view/View;)V
    .locals 1
    .param p1    # Landroid/view/View;

    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    :goto_0
    return-void

    :pswitch_0
    invoke-virtual {p0}, Lcom/mediatek/contacts/detail/AssociationSimActivity;->saveAssociationSimInfo()V

    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/mediatek/contacts/detail/AssociationSimActivity;->returnToContactDetail(Z)V

    goto :goto_0

    :pswitch_1
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/mediatek/contacts/detail/AssociationSimActivity;->returnToContactDetail(Z)V

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x7f07003a
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 4
    .param p1    # Landroid/os/Bundle;

    invoke-super {p0, p1}, Lcom/android/contacts/ContactsActivity;->onCreate(Landroid/os/Bundle;)V

    invoke-virtual {p0}, Landroid/app/Activity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v1, "sel_data_id"

    const-wide/16 v2, -0x1

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/Intent;->getLongExtra(Ljava/lang/String;J)J

    move-result-wide v1

    iput-wide v1, p0, Lcom/mediatek/contacts/detail/AssociationSimActivity;->mInitDataId:J

    const-string v1, "sel_sim_id"

    const/4 v2, -0x1

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v1

    iput v1, p0, Lcom/mediatek/contacts/detail/AssociationSimActivity;->mInitSimId:I

    const-string v1, "contact_detail_info"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v1

    check-cast v1, Lcom/mediatek/contacts/detail/AssociationSimActivity$ContactDetailInfo;

    iput-object v1, p0, Lcom/mediatek/contacts/detail/AssociationSimActivity;->mContactDetailInfo:Lcom/mediatek/contacts/detail/AssociationSimActivity$ContactDetailInfo;

    iget-object v1, p0, Lcom/mediatek/contacts/detail/AssociationSimActivity;->mContactDetailInfo:Lcom/mediatek/contacts/detail/AssociationSimActivity$ContactDetailInfo;

    if-nez v1, :cond_0

    const-string v1, "AssociationSimActivity"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "[onCreate]intent did not carry ContactDetailInfo: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    new-instance v1, Ljava/lang/IllegalArgumentException;

    const-string v2, "A ContactDetailInfo object should be put in the intent to launch this Activity"

    invoke-direct {v1, v2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v1

    :cond_0
    const v1, 0x7f040007

    invoke-virtual {p0, v1}, Landroid/app/Activity;->setContentView(I)V

    invoke-virtual {p0}, Lcom/mediatek/contacts/detail/AssociationSimActivity;->initView()V

    new-instance v1, Lcom/android/contacts/util/NotifyingAsyncQueryHandler;

    invoke-direct {v1, p0, p0}, Lcom/android/contacts/util/NotifyingAsyncQueryHandler;-><init>(Landroid/content/Context;Lcom/android/contacts/util/NotifyingAsyncQueryHandler$AsyncQueryListener;)V

    iput-object v1, p0, Lcom/mediatek/contacts/detail/AssociationSimActivity;->mHandler:Lcom/android/contacts/util/NotifyingAsyncQueryHandler;

    iget-object v1, p0, Lcom/mediatek/contacts/detail/AssociationSimActivity;->mHandler:Lcom/android/contacts/util/NotifyingAsyncQueryHandler;

    invoke-virtual {v1, p0}, Lcom/android/contacts/util/NotifyingAsyncQueryHandler;->setUpdateListener(Lcom/android/contacts/util/NotifyingAsyncQueryHandler$AsyncUpdateListener;)V

    return-void
.end method

.method protected onDestroy()V
    .locals 4

    invoke-virtual {p0}, Landroid/app/Activity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v1, "sel_data_id"

    iget-wide v2, p0, Lcom/mediatek/contacts/detail/AssociationSimActivity;->mInitDataId:J

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;J)Landroid/content/Intent;

    const-string v1, "sel_sim_id"

    iget v2, p0, Lcom/mediatek/contacts/detail/AssociationSimActivity;->mInitSimId:I

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    invoke-super {p0}, Lcom/android/contacts/ContactsActivity;->onDestroy()V

    return-void
.end method

.method public onOptionsItemSelected(Landroid/view/MenuItem;)Z
    .locals 2
    .param p1    # Landroid/view/MenuItem;

    const/4 v0, 0x1

    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v1

    packed-switch v1, :pswitch_data_0

    invoke-super {p0, p1}, Landroid/app/Activity;->onOptionsItemSelected(Landroid/view/MenuItem;)Z

    move-result v0

    :goto_0
    return v0

    :pswitch_0
    invoke-direct {p0, v0}, Lcom/mediatek/contacts/detail/AssociationSimActivity;->returnToContactDetail(Z)V

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x102002c
        :pswitch_0
    .end packed-switch
.end method

.method public onQueryComplete(ILjava/lang/Object;Landroid/database/Cursor;)V
    .locals 0
    .param p1    # I
    .param p2    # Ljava/lang/Object;
    .param p3    # Landroid/database/Cursor;

    return-void
.end method

.method protected onResume()V
    .locals 3

    invoke-super {p0}, Lcom/android/contacts/activities/TransactionSafeActivity;->onResume()V

    iget-object v0, p0, Lcom/mediatek/contacts/detail/AssociationSimActivity;->mNumberInfoMgr:Lcom/mediatek/contacts/detail/AssociationSimActivity$NumberInfoMgr;

    invoke-virtual {v0}, Lcom/mediatek/contacts/detail/AssociationSimActivity$NumberInfoMgr;->initNumberInfo()Z

    iget-object v0, p0, Lcom/mediatek/contacts/detail/AssociationSimActivity;->mSimInfoMgr:Lcom/mediatek/contacts/detail/AssociationSimActivity$SimInfoMgr;

    invoke-virtual {v0, p0}, Lcom/mediatek/contacts/detail/AssociationSimActivity$SimInfoMgr;->initSimInfo(Landroid/content/Context;)Z

    iget-object v0, p0, Lcom/mediatek/contacts/detail/AssociationSimActivity;->mNumberInfoMgr:Lcom/mediatek/contacts/detail/AssociationSimActivity$NumberInfoMgr;

    iget-wide v1, p0, Lcom/mediatek/contacts/detail/AssociationSimActivity;->mInitDataId:J

    invoke-virtual {v0, v1, v2}, Lcom/mediatek/contacts/detail/AssociationSimActivity$NumberInfoMgr;->setShowingNumberNameByDataId(J)Z

    iget-object v0, p0, Lcom/mediatek/contacts/detail/AssociationSimActivity;->mSimInfoMgr:Lcom/mediatek/contacts/detail/AssociationSimActivity$SimInfoMgr;

    iget v1, p0, Lcom/mediatek/contacts/detail/AssociationSimActivity;->mInitSimId:I

    invoke-virtual {v0, v1}, Lcom/mediatek/contacts/detail/AssociationSimActivity$SimInfoMgr;->setShowingIndexBySimId(I)Z

    iget-object v0, p0, Lcom/mediatek/contacts/detail/AssociationSimActivity;->mListView:Landroid/widget/ListView;

    new-instance v1, Lcom/mediatek/contacts/detail/AssociationSimActivity$ListViewAdapter;

    invoke-direct {v1, p0, p0}, Lcom/mediatek/contacts/detail/AssociationSimActivity$ListViewAdapter;-><init>(Lcom/mediatek/contacts/detail/AssociationSimActivity;Landroid/content/Context;)V

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    iget-object v0, p0, Lcom/mediatek/contacts/detail/AssociationSimActivity;->mListView:Landroid/widget/ListView;

    iget-object v1, p0, Lcom/mediatek/contacts/detail/AssociationSimActivity;->mOnListViewItemClick:Landroid/widget/AdapterView$OnItemClickListener;

    invoke-virtual {v0, v1}, Landroid/widget/AdapterView;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    iget-object v0, p0, Lcom/mediatek/contacts/detail/AssociationSimActivity;->mBtnSave:Landroid/widget/Button;

    invoke-direct {p0}, Lcom/mediatek/contacts/detail/AssociationSimActivity;->isSimInfoChanged()Z

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setEnabled(Z)V

    return-void
.end method

.method public onUpdateComplete(ILjava/lang/Object;I)V
    .locals 2
    .param p1    # I
    .param p2    # Ljava/lang/Object;
    .param p3    # I

    if-nez p1, :cond_0

    new-instance v0, Landroid/content/Intent;

    const-string v1, "com.android.contacts.associate_changed"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0, v0}, Landroid/content/ContextWrapper;->sendBroadcast(Landroid/content/Intent;)V

    :cond_0
    return-void
.end method

.method public saveAssociationSimInfo()V
    .locals 9

    const/4 v1, 0x0

    new-instance v4, Landroid/content/ContentValues;

    invoke-direct {v4}, Landroid/content/ContentValues;-><init>()V

    const-string v0, "sim_id"

    iget-object v2, p0, Lcom/mediatek/contacts/detail/AssociationSimActivity;->mSimInfoMgr:Lcom/mediatek/contacts/detail/AssociationSimActivity$SimInfoMgr;

    invoke-virtual {v2}, Lcom/mediatek/contacts/detail/AssociationSimActivity$SimInfoMgr;->getShowingSimId()I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v4, v0, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    iget-object v0, p0, Lcom/mediatek/contacts/detail/AssociationSimActivity;->mHandler:Lcom/android/contacts/util/NotifyingAsyncQueryHandler;

    sget-object v3, Landroid/provider/ContactsContract$Data;->CONTENT_URI:Landroid/net/Uri;

    const-string v5, "_id=? "

    const/4 v2, 0x1

    new-array v6, v2, [Ljava/lang/String;

    iget-object v2, p0, Lcom/mediatek/contacts/detail/AssociationSimActivity;->mNumberInfoMgr:Lcom/mediatek/contacts/detail/AssociationSimActivity$NumberInfoMgr;

    invoke-virtual {v2}, Lcom/mediatek/contacts/detail/AssociationSimActivity$NumberInfoMgr;->getShowingNumberDataId()J

    move-result-wide v7

    invoke-static {v7, v8}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v2

    aput-object v2, v6, v1

    move-object v2, p0

    invoke-virtual/range {v0 .. v6}, Landroid/content/AsyncQueryHandler;->startUpdate(ILjava/lang/Object;Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)V

    return-void
.end method

.method public setListViewChildText(Landroid/view/View;Ljava/lang/String;Ljava/lang/String;)V
    .locals 3
    .param p1    # Landroid/view/View;
    .param p2    # Ljava/lang/String;
    .param p3    # Ljava/lang/String;

    const v2, 0x7f07003d

    invoke-virtual {p1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    invoke-virtual {v1, p2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    const v2, 0x7f07003e

    invoke-virtual {p1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    invoke-virtual {v0, p3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    return-void
.end method
