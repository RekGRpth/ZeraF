.class Lcom/mediatek/contacts/detail/AssociationSimActivity$1;
.super Ljava/lang/Object;
.source "AssociationSimActivity.java"

# interfaces
.implements Landroid/widget/AdapterView$OnItemClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/mediatek/contacts/detail/AssociationSimActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/mediatek/contacts/detail/AssociationSimActivity;


# direct methods
.method constructor <init>(Lcom/mediatek/contacts/detail/AssociationSimActivity;)V
    .locals 0

    iput-object p1, p0, Lcom/mediatek/contacts/detail/AssociationSimActivity$1;->this$0:Lcom/mediatek/contacts/detail/AssociationSimActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onItemClick(Landroid/widget/AdapterView;Landroid/view/View;IJ)V
    .locals 8
    .param p2    # Landroid/view/View;
    .param p3    # I
    .param p4    # J
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/widget/AdapterView",
            "<*>;",
            "Landroid/view/View;",
            "IJ)V"
        }
    .end annotation

    const/4 v7, 0x0

    const/4 v3, 0x2

    if-ge p3, v3, :cond_0

    iget-object v3, p0, Lcom/mediatek/contacts/detail/AssociationSimActivity$1;->this$0:Lcom/mediatek/contacts/detail/AssociationSimActivity;

    invoke-virtual {v3}, Lcom/mediatek/contacts/detail/AssociationSimActivity;->closeSelectDialog()V

    const/4 v3, 0x1

    if-ne p3, v3, :cond_1

    iget-object v3, p0, Lcom/mediatek/contacts/detail/AssociationSimActivity$1;->this$0:Lcom/mediatek/contacts/detail/AssociationSimActivity;

    invoke-static {v3}, Lcom/mediatek/contacts/detail/AssociationSimActivity;->access$000(Lcom/mediatek/contacts/detail/AssociationSimActivity;)Lcom/mediatek/contacts/detail/AssociationSimActivity$SimInfoMgr;

    move-result-object v3

    iget-object v3, v3, Lcom/mediatek/contacts/detail/AssociationSimActivity$SimInfoMgr;->mSimInfoList:Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v3

    if-nez v3, :cond_1

    const-string v3, "AssociationSimActivity"

    const-string v4, "[onListViewItemClick]: mSimInfoList.size() = 0"

    invoke-static {v3, v4}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v3, p0, Lcom/mediatek/contacts/detail/AssociationSimActivity$1;->this$0:Lcom/mediatek/contacts/detail/AssociationSimActivity;

    invoke-static {v3, p3}, Lcom/mediatek/contacts/detail/AssociationSimActivity;->access$102(Lcom/mediatek/contacts/detail/AssociationSimActivity;I)I

    new-instance v2, Lcom/mediatek/contacts/detail/AssociationSimActivity$1$1;

    invoke-direct {v2, p0}, Lcom/mediatek/contacts/detail/AssociationSimActivity$1$1;-><init>(Lcom/mediatek/contacts/detail/AssociationSimActivity$1;)V

    if-nez p3, :cond_2

    new-instance v0, Lcom/mediatek/contacts/detail/AssociationSimActivity$NumberInfoAdapter;

    iget-object v3, p0, Lcom/mediatek/contacts/detail/AssociationSimActivity$1;->this$0:Lcom/mediatek/contacts/detail/AssociationSimActivity;

    iget-object v4, p0, Lcom/mediatek/contacts/detail/AssociationSimActivity$1;->this$0:Lcom/mediatek/contacts/detail/AssociationSimActivity;

    invoke-direct {v0, v3, v4}, Lcom/mediatek/contacts/detail/AssociationSimActivity$NumberInfoAdapter;-><init>(Lcom/mediatek/contacts/detail/AssociationSimActivity;Landroid/content/Context;)V

    new-instance v1, Landroid/app/AlertDialog$Builder;

    iget-object v3, p0, Lcom/mediatek/contacts/detail/AssociationSimActivity$1;->this$0:Lcom/mediatek/contacts/detail/AssociationSimActivity;

    invoke-direct {v1, v3}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    const/4 v3, -0x1

    invoke-virtual {v1, v0, v3, v2}, Landroid/app/AlertDialog$Builder;->setSingleChoiceItems(Landroid/widget/ListAdapter;ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v3

    iget-object v4, p0, Lcom/mediatek/contacts/detail/AssociationSimActivity$1;->this$0:Lcom/mediatek/contacts/detail/AssociationSimActivity;

    invoke-static {v4}, Lcom/mediatek/contacts/detail/AssociationSimActivity;->access$900(Lcom/mediatek/contacts/detail/AssociationSimActivity;)Lcom/mediatek/contacts/detail/AssociationSimActivity$ContactDetailInfo;

    move-result-object v4

    iget-object v4, v4, Lcom/mediatek/contacts/detail/AssociationSimActivity$ContactDetailInfo;->mDisplayTitle:Ljava/lang/String;

    invoke-virtual {v3, v4}, Landroid/app/AlertDialog$Builder;->setTitle(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    move-result-object v3

    const v4, 0x1080045

    invoke-virtual {v3, v4}, Landroid/app/AlertDialog$Builder;->setIcon(I)Landroid/app/AlertDialog$Builder;

    iget-object v3, p0, Lcom/mediatek/contacts/detail/AssociationSimActivity$1;->this$0:Lcom/mediatek/contacts/detail/AssociationSimActivity;

    invoke-virtual {v1}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/mediatek/contacts/detail/AssociationSimActivity;->access$402(Lcom/mediatek/contacts/detail/AssociationSimActivity;Landroid/app/AlertDialog;)Landroid/app/AlertDialog;

    :goto_1
    iget-object v3, p0, Lcom/mediatek/contacts/detail/AssociationSimActivity$1;->this$0:Lcom/mediatek/contacts/detail/AssociationSimActivity;

    invoke-static {v3}, Lcom/mediatek/contacts/detail/AssociationSimActivity;->access$400(Lcom/mediatek/contacts/detail/AssociationSimActivity;)Landroid/app/AlertDialog;

    move-result-object v3

    invoke-virtual {v3}, Landroid/app/Dialog;->show()V

    goto :goto_0

    :cond_2
    iget-object v3, p0, Lcom/mediatek/contacts/detail/AssociationSimActivity$1;->this$0:Lcom/mediatek/contacts/detail/AssociationSimActivity;

    iget-object v4, p0, Lcom/mediatek/contacts/detail/AssociationSimActivity$1;->this$0:Lcom/mediatek/contacts/detail/AssociationSimActivity;

    iget-object v5, p0, Lcom/mediatek/contacts/detail/AssociationSimActivity$1;->this$0:Lcom/mediatek/contacts/detail/AssociationSimActivity;

    invoke-virtual {v5}, Landroid/view/ContextThemeWrapper;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    const v6, 0x7f0c003b

    invoke-virtual {v5, v6}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5, v7, v2, v7}, Lcom/mediatek/contacts/widget/SimPickerDialog;->create(Landroid/content/Context;Ljava/lang/String;ZLandroid/content/DialogInterface$OnClickListener;Z)Landroid/app/AlertDialog;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/mediatek/contacts/detail/AssociationSimActivity;->access$402(Lcom/mediatek/contacts/detail/AssociationSimActivity;Landroid/app/AlertDialog;)Landroid/app/AlertDialog;

    goto :goto_1
.end method
