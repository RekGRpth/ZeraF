.class public final Lcom/mediatek/contacts/util/ContactsIntent$LIST;
.super Ljava/lang/Object;
.source "ContactsIntent.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/mediatek/contacts/util/ContactsIntent;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "LIST"
.end annotation


# static fields
.field public static final ACTION_DELETE_MULTICONTACTS:Ljava/lang/String; = "android.intent.action.contacts.list.DELETEMULTICONTACTS"

.field public static final ACTION_GROUP_ADD_MULTICONTACTS:Ljava/lang/String; = "android.intent.action.contacts.list.group.ADDMULTICONTACTS"

.field public static final ACTION_GROUP_MOVE_MULTICONTACTS:Ljava/lang/String; = "android.intent.action.contacts.list.group.MOVEMULTICONTACTS"

.field public static final ACTION_PICK_MULTICONTACTS:Ljava/lang/String; = "android.intent.action.contacts.list.PICKMULTICONTACTS"

.field public static final ACTION_PICK_MULTIDATAS:Ljava/lang/String; = "android.intent.action.contacts.list.PICKMULTIDATAS"

.field public static final ACTION_PICK_MULTIEMAILS:Ljava/lang/String; = "android.intent.action.contacts.list.PICKMULTIEMAILS"

.field public static final ACTION_PICK_MULTIPHONEANDEMAILS:Ljava/lang/String; = "android.intent.action.contacts.list.PICKMULTIPHONEANDEMAILS"

.field public static final ACTION_PICK_MULTIPHONES:Ljava/lang/String; = "android.intent.action.contacts.list.PICKMULTIPHONES"

.field public static final ACTION_SHARE_MULTICONTACTS:Ljava/lang/String; = "android.intent.action.contacts.list.SHAREMULTICONTACTS"


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
