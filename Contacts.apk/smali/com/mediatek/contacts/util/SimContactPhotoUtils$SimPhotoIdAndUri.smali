.class public interface abstract Lcom/mediatek/contacts/util/SimContactPhotoUtils$SimPhotoIdAndUri;
.super Ljava/lang/Object;
.source "SimContactPhotoUtils.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/mediatek/contacts/util/SimContactPhotoUtils;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "SimPhotoIdAndUri"
.end annotation


# static fields
.field public static final DEFAULT_SIM_PHOTO_ID:I = -0x1

.field public static final DEFAULT_SIM_PHOTO_ID_SDN:I = -0x9

.field public static final DEFAULT_SIM_PHOTO_URI:Ljava/lang/String; = "content://sim"

.field public static final DEFAULT_SIM_PHOTO_URI_SDN:Ljava/lang/String; = "content://sdn"

.field public static final SIM_PHOTO_ID_BLUE:I = -0xa

.field public static final SIM_PHOTO_ID_BLUE_SDN:I = -0x5

.field public static final SIM_PHOTO_ID_GREEN:I = -0xc

.field public static final SIM_PHOTO_ID_GREEN_SDN:I = -0x7

.field public static final SIM_PHOTO_ID_ORANGE:I = -0xb

.field public static final SIM_PHOTO_ID_ORANGE_SDN:I = -0x6

.field public static final SIM_PHOTO_ID_PURPLE:I = -0xd

.field public static final SIM_PHOTO_ID_PURPLE_SDN:I = -0x8

.field public static final SIM_PHOTO_URI_BLUE:Ljava/lang/String; = "content://sim-10"

.field public static final SIM_PHOTO_URI_BLUE_SDN:Ljava/lang/String; = "content://sdn-5"

.field public static final SIM_PHOTO_URI_GREEN:Ljava/lang/String; = "content://sim-12"

.field public static final SIM_PHOTO_URI_GREEN_SDN:Ljava/lang/String; = "content://sdn-7"

.field public static final SIM_PHOTO_URI_ORANGE:Ljava/lang/String; = "content://sim-11"

.field public static final SIM_PHOTO_URI_ORANGE_SDN:Ljava/lang/String; = "content://sdn-6"

.field public static final SIM_PHOTO_URI_PURPLE:Ljava/lang/String; = "content://sim-13"

.field public static final SIM_PHOTO_URI_PURPLE_SDN:Ljava/lang/String; = "content://sdn-8"
