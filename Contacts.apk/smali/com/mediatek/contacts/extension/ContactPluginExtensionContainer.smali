.class public Lcom/mediatek/contacts/extension/ContactPluginExtensionContainer;
.super Ljava/lang/Object;
.source "ContactPluginExtensionContainer.java"


# static fields
.field private static final TAG:Ljava/lang/String; = "ContactPluginExtensionContainer"


# instance fields
.field private mCallDetailExtensionContainer:Lcom/mediatek/contacts/extension/CallDetailExtensionContainer;

.field private mCallListExtensionContainer:Lcom/mediatek/contacts/extension/CallListExtensionContainer;

.field private mContactAccountExtensionContainer:Lcom/mediatek/contacts/extension/ContactAccountExtensionContainer;

.field private mContactDetailExtensionContainer:Lcom/mediatek/contacts/extension/ContactDetailExtensionContainer;

.field private mContactListExtensionContainer:Lcom/mediatek/contacts/extension/ContactListExtensionContainer;

.field private mDialPadExtensionContainer:Lcom/mediatek/contacts/extension/DialPadExtensionContainer;

.field private mDialtactsExtensionContainer:Lcom/mediatek/contacts/extension/DialtactsExtensionContainer;

.field private mQuickContactExtensionContainer:Lcom/mediatek/contacts/extension/QuickContactExtensionContainer;

.field private mSimPickExtensionContainer:Lcom/mediatek/contacts/extension/SimPickExtensionContainer;

.field private mSpeedDialExtensionContainer:Lcom/mediatek/contacts/extension/SpeedDialExtensionContainer;


# direct methods
.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Lcom/mediatek/contacts/extension/CallDetailExtensionContainer;

    invoke-direct {v0}, Lcom/mediatek/contacts/extension/CallDetailExtensionContainer;-><init>()V

    iput-object v0, p0, Lcom/mediatek/contacts/extension/ContactPluginExtensionContainer;->mCallDetailExtensionContainer:Lcom/mediatek/contacts/extension/CallDetailExtensionContainer;

    new-instance v0, Lcom/mediatek/contacts/extension/CallListExtensionContainer;

    invoke-direct {v0}, Lcom/mediatek/contacts/extension/CallListExtensionContainer;-><init>()V

    iput-object v0, p0, Lcom/mediatek/contacts/extension/ContactPluginExtensionContainer;->mCallListExtensionContainer:Lcom/mediatek/contacts/extension/CallListExtensionContainer;

    new-instance v0, Lcom/mediatek/contacts/extension/ContactAccountExtensionContainer;

    invoke-direct {v0}, Lcom/mediatek/contacts/extension/ContactAccountExtensionContainer;-><init>()V

    iput-object v0, p0, Lcom/mediatek/contacts/extension/ContactPluginExtensionContainer;->mContactAccountExtensionContainer:Lcom/mediatek/contacts/extension/ContactAccountExtensionContainer;

    new-instance v0, Lcom/mediatek/contacts/extension/ContactDetailExtensionContainer;

    invoke-direct {v0}, Lcom/mediatek/contacts/extension/ContactDetailExtensionContainer;-><init>()V

    iput-object v0, p0, Lcom/mediatek/contacts/extension/ContactPluginExtensionContainer;->mContactDetailExtensionContainer:Lcom/mediatek/contacts/extension/ContactDetailExtensionContainer;

    new-instance v0, Lcom/mediatek/contacts/extension/ContactListExtensionContainer;

    invoke-direct {v0}, Lcom/mediatek/contacts/extension/ContactListExtensionContainer;-><init>()V

    iput-object v0, p0, Lcom/mediatek/contacts/extension/ContactPluginExtensionContainer;->mContactListExtensionContainer:Lcom/mediatek/contacts/extension/ContactListExtensionContainer;

    new-instance v0, Lcom/mediatek/contacts/extension/DialPadExtensionContainer;

    invoke-direct {v0}, Lcom/mediatek/contacts/extension/DialPadExtensionContainer;-><init>()V

    iput-object v0, p0, Lcom/mediatek/contacts/extension/ContactPluginExtensionContainer;->mDialPadExtensionContainer:Lcom/mediatek/contacts/extension/DialPadExtensionContainer;

    new-instance v0, Lcom/mediatek/contacts/extension/DialtactsExtensionContainer;

    invoke-direct {v0}, Lcom/mediatek/contacts/extension/DialtactsExtensionContainer;-><init>()V

    iput-object v0, p0, Lcom/mediatek/contacts/extension/ContactPluginExtensionContainer;->mDialtactsExtensionContainer:Lcom/mediatek/contacts/extension/DialtactsExtensionContainer;

    new-instance v0, Lcom/mediatek/contacts/extension/SpeedDialExtensionContainer;

    invoke-direct {v0}, Lcom/mediatek/contacts/extension/SpeedDialExtensionContainer;-><init>()V

    iput-object v0, p0, Lcom/mediatek/contacts/extension/ContactPluginExtensionContainer;->mSpeedDialExtensionContainer:Lcom/mediatek/contacts/extension/SpeedDialExtensionContainer;

    new-instance v0, Lcom/mediatek/contacts/extension/SimPickExtensionContainer;

    invoke-direct {v0}, Lcom/mediatek/contacts/extension/SimPickExtensionContainer;-><init>()V

    iput-object v0, p0, Lcom/mediatek/contacts/extension/ContactPluginExtensionContainer;->mSimPickExtensionContainer:Lcom/mediatek/contacts/extension/SimPickExtensionContainer;

    new-instance v0, Lcom/mediatek/contacts/extension/QuickContactExtensionContainer;

    invoke-direct {v0}, Lcom/mediatek/contacts/extension/QuickContactExtensionContainer;-><init>()V

    iput-object v0, p0, Lcom/mediatek/contacts/extension/ContactPluginExtensionContainer;->mQuickContactExtensionContainer:Lcom/mediatek/contacts/extension/QuickContactExtensionContainer;

    return-void
.end method


# virtual methods
.method public addExtensions(Lcom/android/contacts/ext/IContactPlugin;)V
    .locals 3
    .param p1    # Lcom/android/contacts/ext/IContactPlugin;

    const-string v0, "ContactPluginExtensionContainer"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "contactPlugin : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v0, p0, Lcom/mediatek/contacts/extension/ContactPluginExtensionContainer;->mCallDetailExtensionContainer:Lcom/mediatek/contacts/extension/CallDetailExtensionContainer;

    invoke-interface {p1}, Lcom/android/contacts/ext/IContactPlugin;->createCallDetailExtension()Lcom/android/contacts/ext/CallDetailExtension;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/mediatek/contacts/extension/CallDetailExtensionContainer;->add(Lcom/android/contacts/ext/CallDetailExtension;)V

    iget-object v0, p0, Lcom/mediatek/contacts/extension/ContactPluginExtensionContainer;->mCallListExtensionContainer:Lcom/mediatek/contacts/extension/CallListExtensionContainer;

    invoke-interface {p1}, Lcom/android/contacts/ext/IContactPlugin;->createCallListExtension()Lcom/android/contacts/ext/CallListExtension;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/mediatek/contacts/extension/CallListExtensionContainer;->add(Lcom/android/contacts/ext/CallListExtension;)V

    iget-object v0, p0, Lcom/mediatek/contacts/extension/ContactPluginExtensionContainer;->mContactAccountExtensionContainer:Lcom/mediatek/contacts/extension/ContactAccountExtensionContainer;

    invoke-interface {p1}, Lcom/android/contacts/ext/IContactPlugin;->createContactAccountExtension()Lcom/android/contacts/ext/ContactAccountExtension;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/mediatek/contacts/extension/ContactAccountExtensionContainer;->add(Lcom/android/contacts/ext/ContactAccountExtension;)V

    iget-object v0, p0, Lcom/mediatek/contacts/extension/ContactPluginExtensionContainer;->mContactDetailExtensionContainer:Lcom/mediatek/contacts/extension/ContactDetailExtensionContainer;

    invoke-interface {p1}, Lcom/android/contacts/ext/IContactPlugin;->createContactDetailExtension()Lcom/android/contacts/ext/ContactDetailExtension;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/mediatek/contacts/extension/ContactDetailExtensionContainer;->add(Lcom/android/contacts/ext/ContactDetailExtension;)V

    iget-object v0, p0, Lcom/mediatek/contacts/extension/ContactPluginExtensionContainer;->mContactListExtensionContainer:Lcom/mediatek/contacts/extension/ContactListExtensionContainer;

    invoke-interface {p1}, Lcom/android/contacts/ext/IContactPlugin;->createContactListExtension()Lcom/android/contacts/ext/ContactListExtension;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/mediatek/contacts/extension/ContactListExtensionContainer;->add(Lcom/android/contacts/ext/ContactListExtension;)V

    iget-object v0, p0, Lcom/mediatek/contacts/extension/ContactPluginExtensionContainer;->mDialPadExtensionContainer:Lcom/mediatek/contacts/extension/DialPadExtensionContainer;

    invoke-interface {p1}, Lcom/android/contacts/ext/IContactPlugin;->createDialPadExtension()Lcom/android/contacts/ext/DialPadExtension;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/mediatek/contacts/extension/DialPadExtensionContainer;->add(Lcom/android/contacts/ext/DialPadExtension;)V

    iget-object v0, p0, Lcom/mediatek/contacts/extension/ContactPluginExtensionContainer;->mDialtactsExtensionContainer:Lcom/mediatek/contacts/extension/DialtactsExtensionContainer;

    invoke-interface {p1}, Lcom/android/contacts/ext/IContactPlugin;->createDialtactsExtension()Lcom/android/contacts/ext/DialtactsExtension;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/mediatek/contacts/extension/DialtactsExtensionContainer;->add(Lcom/android/contacts/ext/DialtactsExtension;)V

    iget-object v0, p0, Lcom/mediatek/contacts/extension/ContactPluginExtensionContainer;->mSpeedDialExtensionContainer:Lcom/mediatek/contacts/extension/SpeedDialExtensionContainer;

    invoke-interface {p1}, Lcom/android/contacts/ext/IContactPlugin;->createSpeedDialExtension()Lcom/android/contacts/ext/SpeedDialExtension;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/mediatek/contacts/extension/SpeedDialExtensionContainer;->add(Lcom/android/contacts/ext/SpeedDialExtension;)V

    iget-object v0, p0, Lcom/mediatek/contacts/extension/ContactPluginExtensionContainer;->mSimPickExtensionContainer:Lcom/mediatek/contacts/extension/SimPickExtensionContainer;

    invoke-interface {p1}, Lcom/android/contacts/ext/IContactPlugin;->createSimPickExtension()Lcom/android/contacts/ext/SimPickExtension;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/mediatek/contacts/extension/SimPickExtensionContainer;->add(Lcom/android/contacts/ext/SimPickExtension;)V

    iget-object v0, p0, Lcom/mediatek/contacts/extension/ContactPluginExtensionContainer;->mQuickContactExtensionContainer:Lcom/mediatek/contacts/extension/QuickContactExtensionContainer;

    invoke-interface {p1}, Lcom/android/contacts/ext/IContactPlugin;->createQuickContactExtension()Lcom/android/contacts/ext/QuickContactExtension;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/mediatek/contacts/extension/QuickContactExtensionContainer;->add(Lcom/android/contacts/ext/QuickContactExtension;)V

    return-void
.end method

.method public getCallDetailExtension()Lcom/android/contacts/ext/CallDetailExtension;
    .locals 2

    const-string v0, "ContactPluginExtensionContainer"

    const-string v1, "return CallDetailExtension "

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v0, p0, Lcom/mediatek/contacts/extension/ContactPluginExtensionContainer;->mCallDetailExtensionContainer:Lcom/mediatek/contacts/extension/CallDetailExtensionContainer;

    return-object v0
.end method

.method public getCallListExtension()Lcom/android/contacts/ext/CallListExtension;
    .locals 2

    const-string v0, "ContactPluginExtensionContainer"

    const-string v1, "return CallListExtension "

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v0, p0, Lcom/mediatek/contacts/extension/ContactPluginExtensionContainer;->mCallListExtensionContainer:Lcom/mediatek/contacts/extension/CallListExtensionContainer;

    return-object v0
.end method

.method public getContactAccountExtension()Lcom/android/contacts/ext/ContactAccountExtension;
    .locals 3

    const-string v0, "ContactPluginExtensionContainer"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "return ContactAccountExtension "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/mediatek/contacts/extension/ContactPluginExtensionContainer;->mContactAccountExtensionContainer:Lcom/mediatek/contacts/extension/ContactAccountExtensionContainer;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v0, p0, Lcom/mediatek/contacts/extension/ContactPluginExtensionContainer;->mContactAccountExtensionContainer:Lcom/mediatek/contacts/extension/ContactAccountExtensionContainer;

    return-object v0
.end method

.method public getContactDetailExtension()Lcom/android/contacts/ext/ContactDetailExtension;
    .locals 2

    const-string v0, "ContactPluginExtensionContainer"

    const-string v1, "return ContactDetailExtension "

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v0, p0, Lcom/mediatek/contacts/extension/ContactPluginExtensionContainer;->mContactDetailExtensionContainer:Lcom/mediatek/contacts/extension/ContactDetailExtensionContainer;

    return-object v0
.end method

.method public getContactListExtension()Lcom/android/contacts/ext/ContactListExtension;
    .locals 2

    const-string v0, "ContactPluginExtensionContainer"

    const-string v1, "return ContactListExtension "

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v0, p0, Lcom/mediatek/contacts/extension/ContactPluginExtensionContainer;->mContactListExtensionContainer:Lcom/mediatek/contacts/extension/ContactListExtensionContainer;

    return-object v0
.end method

.method public getDialPadExtension()Lcom/android/contacts/ext/DialPadExtension;
    .locals 2

    const-string v0, "ContactPluginExtensionContainer"

    const-string v1, "return DialPadExtension "

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v0, p0, Lcom/mediatek/contacts/extension/ContactPluginExtensionContainer;->mDialPadExtensionContainer:Lcom/mediatek/contacts/extension/DialPadExtensionContainer;

    return-object v0
.end method

.method public getDialtactsExtension()Lcom/android/contacts/ext/DialtactsExtension;
    .locals 2

    const-string v0, "ContactPluginExtensionContainer"

    const-string v1, "return DialtactsExtension "

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v0, p0, Lcom/mediatek/contacts/extension/ContactPluginExtensionContainer;->mDialtactsExtensionContainer:Lcom/mediatek/contacts/extension/DialtactsExtensionContainer;

    return-object v0
.end method

.method public getQuickContactExtension()Lcom/android/contacts/ext/QuickContactExtension;
    .locals 2

    const-string v0, "ContactPluginExtensionContainer"

    const-string v1, "return QuickContactExtension"

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v0, p0, Lcom/mediatek/contacts/extension/ContactPluginExtensionContainer;->mQuickContactExtensionContainer:Lcom/mediatek/contacts/extension/QuickContactExtensionContainer;

    return-object v0
.end method

.method public getSimPickExtension()Lcom/android/contacts/ext/SimPickExtension;
    .locals 2

    const-string v0, "ContactPluginExtensionContainer"

    const-string v1, "return SimPickExtension "

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v0, p0, Lcom/mediatek/contacts/extension/ContactPluginExtensionContainer;->mSimPickExtensionContainer:Lcom/mediatek/contacts/extension/SimPickExtensionContainer;

    return-object v0
.end method

.method public getSpeedDialExtension()Lcom/android/contacts/ext/SpeedDialExtension;
    .locals 2

    const-string v0, "ContactPluginExtensionContainer"

    const-string v1, "return SpeedDialExtension "

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v0, p0, Lcom/mediatek/contacts/extension/ContactPluginExtensionContainer;->mSpeedDialExtensionContainer:Lcom/mediatek/contacts/extension/SpeedDialExtensionContainer;

    return-object v0
.end method
