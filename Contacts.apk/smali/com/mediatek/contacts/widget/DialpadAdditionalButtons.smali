.class public Lcom/mediatek/contacts/widget/DialpadAdditionalButtons;
.super Landroid/widget/FrameLayout;
.source "DialpadAdditionalButtons.java"


# static fields
.field private static final TAG:Ljava/lang/String; = "DialpadAdditionalButtons"


# instance fields
.field private mButtonHeight:I

.field private mButtonWidth:I

.field private mDialButton:Landroid/widget/ImageButton;

.field private mDivider1:Landroid/view/View;

.field private mDivider2:Landroid/view/View;

.field private mDividerHeight:I

.field private mDividerVertical:Landroid/graphics/drawable/Drawable;

.field private mDividerWidth:I

.field private mHoloButton:Landroid/widget/ImageButton;

.field private mLayouted:Z

.field private mMenuButton:Landroid/widget/ImageButton;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 2
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/util/AttributeSet;

    invoke-direct {p0, p1, p2}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    const/4 v1, 0x0

    iput-boolean v1, p0, Lcom/mediatek/contacts/widget/DialpadAdditionalButtons;->mLayouted:Z

    invoke-virtual {p0}, Landroid/view/View;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f090073

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    iput v1, p0, Lcom/mediatek/contacts/widget/DialpadAdditionalButtons;->mButtonWidth:I

    const v1, 0x7f0900a0

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    iput v1, p0, Lcom/mediatek/contacts/widget/DialpadAdditionalButtons;->mButtonHeight:I

    const v1, 0x7f090079

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    iput v1, p0, Lcom/mediatek/contacts/widget/DialpadAdditionalButtons;->mDividerHeight:I

    const v1, 0x7f09007a

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    iput v1, p0, Lcom/mediatek/contacts/widget/DialpadAdditionalButtons;->mDividerWidth:I

    return-void
.end method

.method private calculatePositionByOrientation(II)I
    .locals 2
    .param p1    # I
    .param p2    # I

    invoke-virtual {p0}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v0

    iget v0, v0, Landroid/content/res/Configuration;->orientation:I

    const/4 v1, 0x2

    if-ne v0, v1, :cond_0

    sub-int/2addr p1, p2

    :cond_0
    return p1
.end method


# virtual methods
.method protected init()V
    .locals 9

    const/4 v8, 0x1

    const/4 v7, 0x0

    invoke-virtual {p0}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v4

    invoke-virtual {v4}, Landroid/content/Context;->getTheme()Landroid/content/res/Resources$Theme;

    move-result-object v4

    new-array v5, v8, [I

    const v6, 0x101030e

    aput v6, v5, v7

    invoke-virtual {v4, v5}, Landroid/content/res/Resources$Theme;->obtainStyledAttributes([I)Landroid/content/res/TypedArray;

    move-result-object v3

    invoke-virtual {v3, v7}, Landroid/content/res/TypedArray;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    invoke-virtual {p0}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v4

    invoke-virtual {v4}, Landroid/content/Context;->getTheme()Landroid/content/res/Resources$Theme;

    move-result-object v4

    new-array v5, v8, [I

    const v6, 0x101030a

    aput v6, v5, v7

    invoke-virtual {v4, v5}, Landroid/content/res/Resources$Theme;->obtainStyledAttributes([I)Landroid/content/res/TypedArray;

    move-result-object v3

    invoke-virtual {v3, v7}, Landroid/content/res/TypedArray;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v4

    iput-object v4, p0, Lcom/mediatek/contacts/widget/DialpadAdditionalButtons;->mDividerVertical:Landroid/graphics/drawable/Drawable;

    new-instance v4, Landroid/widget/ImageButton;

    invoke-virtual {p0}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v5

    invoke-direct {v4, v5}, Landroid/widget/ImageButton;-><init>(Landroid/content/Context;)V

    iput-object v4, p0, Lcom/mediatek/contacts/widget/DialpadAdditionalButtons;->mDialButton:Landroid/widget/ImageButton;

    iget-object v4, p0, Lcom/mediatek/contacts/widget/DialpadAdditionalButtons;->mDialButton:Landroid/widget/ImageButton;

    const v5, 0x7f02007c # R.drawable.ic_dial_action_call

    invoke-virtual {v4, v5}, Landroid/widget/ImageView;->setImageResource(I)V

    iget-object v4, p0, Lcom/mediatek/contacts/widget/DialpadAdditionalButtons;->mDialButton:Landroid/widget/ImageButton;

    invoke-static {}, Lcom/mediatek/phone/SIMInfoWrapper;->getDefault()Lcom/mediatek/phone/SIMInfoWrapper;

    move-result-object v2

    invoke-virtual {v2, v7}, Lcom/mediatek/phone/SIMInfoWrapper;->getSimIdBySlotId(I)I

    move-result v0

    invoke-virtual {v2, v0}, Lcom/mediatek/phone/SIMInfoWrapper;->getInsertedSimColorById(I)I

    move-result v0

    invoke-virtual {v2, v0}, Lcom/mediatek/phone/SIMInfoWrapper;->getSimBackgroundResByColorId(I)I

    move-result v0

    invoke-virtual {v4, v0}, Landroid/view/View;->setBackgroundResource(I)V

    iget-object v4, p0, Lcom/mediatek/contacts/widget/DialpadAdditionalButtons;->mDialButton:Landroid/widget/ImageButton;

    const v5, 0x7f07002b # id dialButton

    invoke-virtual {v4, v5}, Landroid/view/View;->setId(I)V

    iget-object v4, p0, Lcom/mediatek/contacts/widget/DialpadAdditionalButtons;->mDialButton:Landroid/widget/ImageButton;

    invoke-virtual {p0, v4}, Landroid/view/ViewGroup;->addView(Landroid/view/View;)V

    new-instance v4, Landroid/view/View;

    invoke-virtual {p0}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v5

    invoke-direct {v4, v5}, Landroid/view/View;-><init>(Landroid/content/Context;)V

    iput-object v4, p0, Lcom/mediatek/contacts/widget/DialpadAdditionalButtons;->mDivider1:Landroid/view/View;

    iget-object v4, p0, Lcom/mediatek/contacts/widget/DialpadAdditionalButtons;->mDivider1:Landroid/view/View;

    iget-object v5, p0, Lcom/mediatek/contacts/widget/DialpadAdditionalButtons;->mDividerVertical:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v4, v5}, Landroid/view/View;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    iget-object v4, p0, Lcom/mediatek/contacts/widget/DialpadAdditionalButtons;->mDivider1:Landroid/view/View;

    invoke-virtual {p0, v4}, Landroid/view/ViewGroup;->addView(Landroid/view/View;)V

    new-instance v4, Landroid/widget/ImageButton;

    invoke-virtual {p0}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v5

    invoke-direct {v4, v5}, Landroid/widget/ImageButton;-><init>(Landroid/content/Context;)V

    iput-object v4, p0, Lcom/mediatek/contacts/widget/DialpadAdditionalButtons;->mHoloButton:Landroid/widget/ImageButton;

    iget-object v4, p0, Lcom/mediatek/contacts/widget/DialpadAdditionalButtons;->mHoloButton:Landroid/widget/ImageButton;

    const v5, 0x7f020083 # R.drawable.ic_dialpad_holo_dark

    invoke-virtual {v4, v5}, Landroid/widget/ImageView;->setImageResource(I)V

    iget-object v4, p0, Lcom/mediatek/contacts/widget/DialpadAdditionalButtons;->mHoloButton:Landroid/widget/ImageButton;

    invoke-virtual {v4, v1}, Landroid/view/View;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    iget-object v4, p0, Lcom/mediatek/contacts/widget/DialpadAdditionalButtons;->mHoloButton:Landroid/widget/ImageButton;

    const v5, 0x7f07002a

    invoke-virtual {v4, v5}, Landroid/view/View;->setId(I)V

    iget-object v4, p0, Lcom/mediatek/contacts/widget/DialpadAdditionalButtons;->mHoloButton:Landroid/widget/ImageButton;

    invoke-virtual {p0, v4}, Landroid/view/ViewGroup;->addView(Landroid/view/View;)V

    new-instance v4, Landroid/view/View;

    invoke-virtual {p0}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v5

    invoke-direct {v4, v5}, Landroid/view/View;-><init>(Landroid/content/Context;)V

    iput-object v4, p0, Lcom/mediatek/contacts/widget/DialpadAdditionalButtons;->mDivider2:Landroid/view/View;

    iget-object v4, p0, Lcom/mediatek/contacts/widget/DialpadAdditionalButtons;->mDivider2:Landroid/view/View;

    iget-object v5, p0, Lcom/mediatek/contacts/widget/DialpadAdditionalButtons;->mDividerVertical:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v4, v5}, Landroid/view/View;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    iget-object v4, p0, Lcom/mediatek/contacts/widget/DialpadAdditionalButtons;->mDivider2:Landroid/view/View;

    invoke-virtual {p0, v4}, Landroid/view/ViewGroup;->addView(Landroid/view/View;)V

    new-instance v4, Landroid/widget/ImageButton;

    invoke-virtual {p0}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v5

    invoke-direct {v4, v5}, Landroid/widget/ImageButton;-><init>(Landroid/content/Context;)V

    iput-object v4, p0, Lcom/mediatek/contacts/widget/DialpadAdditionalButtons;->mMenuButton:Landroid/widget/ImageButton;

    iget-object v4, p0, Lcom/mediatek/contacts/widget/DialpadAdditionalButtons;->mMenuButton:Landroid/widget/ImageButton;

    invoke-static {}, Lcom/mediatek/phone/SIMInfoWrapper;->getDefault()Lcom/mediatek/phone/SIMInfoWrapper;

    move-result-object v2

    invoke-virtual {v2, v8}, Lcom/mediatek/phone/SIMInfoWrapper;->getSimIdBySlotId(I)I

    move-result v0

    invoke-virtual {v2, v0}, Lcom/mediatek/phone/SIMInfoWrapper;->getInsertedSimColorById(I)I

    move-result v0

    invoke-virtual {v2, v0}, Lcom/mediatek/phone/SIMInfoWrapper;->getSimBackgroundResByColorId(I)I

    move-result v0

    invoke-virtual {v4, v0}, Landroid/view/View;->setBackgroundResource(I)V

    const v0, 0x7f07002c

    const v2, 0x7f020099 # R.drawable.ic_menu_overflow

    invoke-virtual {p0}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v4

    invoke-static {v4}, Landroid/view/ViewConfiguration;->get(Landroid/content/Context;)Landroid/view/ViewConfiguration;

    move-result-object v4

    invoke-virtual {v4}, Landroid/view/ViewConfiguration;->hasPermanentMenuKey()Z

    move-result v4

    if-eqz v4, :cond_0

    const v0, 0x7f07002d # id videoDialButton

    const v2, 0x7f02007f # R.drawable.ic_dial_action_video_call
#    const v2, 0x7f02007c # R.drawable.ic_dial_action_call

    :cond_0
    iget-object v4, p0, Lcom/mediatek/contacts/widget/DialpadAdditionalButtons;->mMenuButton:Landroid/widget/ImageButton;

    invoke-virtual {v4, v0}, Landroid/view/View;->setId(I)V

    iget-object v4, p0, Lcom/mediatek/contacts/widget/DialpadAdditionalButtons;->mMenuButton:Landroid/widget/ImageButton;

    invoke-virtual {v4, v2}, Landroid/widget/ImageView;->setImageResource(I)V

    iget-object v4, p0, Lcom/mediatek/contacts/widget/DialpadAdditionalButtons;->mMenuButton:Landroid/widget/ImageButton;

    invoke-virtual {p0, v4}, Landroid/view/ViewGroup;->addView(Landroid/view/View;)V

    invoke-virtual {p0}, Lcom/mediatek/contacts/widget/DialpadAdditionalButtons;->setButtonVisibility()V

    return-void
.end method

.method protected onFinishInflate()V
    .locals 0

    invoke-super {p0}, Landroid/view/View;->onFinishInflate()V

    invoke-virtual {p0}, Lcom/mediatek/contacts/widget/DialpadAdditionalButtons;->init()V

    return-void
.end method

.method protected onLayout(ZIIII)V
    .locals 9
    .param p1    # Z
    .param p2    # I
    .param p3    # I
    .param p4    # I
    .param p5    # I

    const/4 v7, 0x1

    const/4 v8, 0x0

    iget-boolean v5, p0, Lcom/mediatek/contacts/widget/DialpadAdditionalButtons;->mLayouted:Z

    if-eqz v5, :cond_0

    :goto_0
    return-void

    :cond_0
    iput-boolean v7, p0, Lcom/mediatek/contacts/widget/DialpadAdditionalButtons;->mLayouted:Z

    invoke-virtual {p0, v8}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    iget v5, p0, Lcom/mediatek/contacts/widget/DialpadAdditionalButtons;->mButtonWidth:I

    iget v6, p0, Lcom/mediatek/contacts/widget/DialpadAdditionalButtons;->mButtonHeight:I

    invoke-virtual {v0, v8, v8, v5, v6}, Landroid/view/View;->layout(IIII)V

    iget v5, p0, Lcom/mediatek/contacts/widget/DialpadAdditionalButtons;->mButtonHeight:I

    iget v6, p0, Lcom/mediatek/contacts/widget/DialpadAdditionalButtons;->mDividerHeight:I

    sub-int/2addr v5, v6

    shr-int/lit8 v4, v5, 0x1

    invoke-virtual {p0, v7}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    iget v5, p0, Lcom/mediatek/contacts/widget/DialpadAdditionalButtons;->mButtonWidth:I

    iget v6, p0, Lcom/mediatek/contacts/widget/DialpadAdditionalButtons;->mDividerHeight:I

    invoke-direct {p0, v5, v6}, Lcom/mediatek/contacts/widget/DialpadAdditionalButtons;->calculatePositionByOrientation(II)I

    move-result v1

    iget v5, p0, Lcom/mediatek/contacts/widget/DialpadAdditionalButtons;->mButtonWidth:I

    iget v6, p0, Lcom/mediatek/contacts/widget/DialpadAdditionalButtons;->mDividerWidth:I

    add-int/2addr v5, v6

    iget v6, p0, Lcom/mediatek/contacts/widget/DialpadAdditionalButtons;->mDividerHeight:I

    invoke-direct {p0, v5, v6}, Lcom/mediatek/contacts/widget/DialpadAdditionalButtons;->calculatePositionByOrientation(II)I

    move-result v2

    iget v5, p0, Lcom/mediatek/contacts/widget/DialpadAdditionalButtons;->mDividerHeight:I

    add-int/2addr v5, v4

    invoke-virtual {v0, v1, v4, v2, v5}, Landroid/view/View;->layout(IIII)V

    const/4 v5, 0x2

    invoke-virtual {p0, v5}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    iget v5, p0, Lcom/mediatek/contacts/widget/DialpadAdditionalButtons;->mButtonWidth:I

    iget v6, p0, Lcom/mediatek/contacts/widget/DialpadAdditionalButtons;->mDividerHeight:I

    invoke-direct {p0, v5, v6}, Lcom/mediatek/contacts/widget/DialpadAdditionalButtons;->calculatePositionByOrientation(II)I

    move-result v3

    iget v5, p0, Lcom/mediatek/contacts/widget/DialpadAdditionalButtons;->mButtonWidth:I

    shl-int/lit8 v5, v5, 0x1

    iget v6, p0, Lcom/mediatek/contacts/widget/DialpadAdditionalButtons;->mButtonHeight:I

    invoke-virtual {v0, v3, v8, v5, v6}, Landroid/view/View;->layout(IIII)V

    const/4 v5, 0x3

    invoke-virtual {p0, v5}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    iget v5, p0, Lcom/mediatek/contacts/widget/DialpadAdditionalButtons;->mButtonWidth:I

    shl-int/lit8 v5, v5, 0x1

    iget v6, p0, Lcom/mediatek/contacts/widget/DialpadAdditionalButtons;->mButtonWidth:I

    shl-int/lit8 v6, v6, 0x1

    iget v7, p0, Lcom/mediatek/contacts/widget/DialpadAdditionalButtons;->mDividerWidth:I

    add-int/2addr v6, v7

    iget v7, p0, Lcom/mediatek/contacts/widget/DialpadAdditionalButtons;->mDividerHeight:I

    add-int/2addr v7, v4

    invoke-virtual {v0, v5, v4, v6, v7}, Landroid/view/View;->layout(IIII)V

    const/4 v5, 0x4

    invoke-virtual {p0, v5}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    iget v5, p0, Lcom/mediatek/contacts/widget/DialpadAdditionalButtons;->mButtonWidth:I

    shl-int/lit8 v5, v5, 0x1

    iget v6, p0, Lcom/mediatek/contacts/widget/DialpadAdditionalButtons;->mButtonWidth:I

    shl-int/lit8 v6, v6, 0x1

    iget v7, p0, Lcom/mediatek/contacts/widget/DialpadAdditionalButtons;->mButtonWidth:I

    add-int/2addr v6, v7

    iget v7, p0, Lcom/mediatek/contacts/widget/DialpadAdditionalButtons;->mButtonHeight:I

    invoke-virtual {v0, v5, v8, v6, v7}, Landroid/view/View;->layout(IIII)V

    goto :goto_0
.end method

.method protected setButtonVisibility()V
    .locals 3

    const/4 v2, 0x4

    invoke-virtual {p0}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v0

    iget v0, v0, Landroid/content/res/Configuration;->orientation:I

    const/4 v1, 0x2

    if-ne v0, v1, :cond_0

    iget-object v0, p0, Lcom/mediatek/contacts/widget/DialpadAdditionalButtons;->mHoloButton:Landroid/widget/ImageButton;

    invoke-virtual {v0, v2}, Landroid/widget/ImageView;->setVisibility(I)V

    iget-object v0, p0, Lcom/mediatek/contacts/widget/DialpadAdditionalButtons;->mMenuButton:Landroid/widget/ImageButton;

    invoke-virtual {v0, v2}, Landroid/widget/ImageView;->setVisibility(I)V

    iget-object v0, p0, Lcom/mediatek/contacts/widget/DialpadAdditionalButtons;->mDivider1:Landroid/view/View;

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    iget-object v0, p0, Lcom/mediatek/contacts/widget/DialpadAdditionalButtons;->mDivider2:Landroid/view/View;

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    :cond_0
    return-void
.end method
