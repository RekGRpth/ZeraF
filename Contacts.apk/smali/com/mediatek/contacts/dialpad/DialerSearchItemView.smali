.class public Lcom/mediatek/contacts/dialpad/DialerSearchItemView;
.super Landroid/widget/FrameLayout;
.source "DialerSearchItemView.java"


# static fields
.field private static final DBG:Z = true

.field private static final TAG:Ljava/lang/String; = "DialerSearchListItem"

.field private static sDialerSearchItemViewHeight:I

.field private static sListItemQuickContactPaddingBottom:I

.field private static sListItemQuickContactPaddingTop:I


# instance fields
.field protected mCall:Landroid/widget/ImageButton;

.field protected mCallType:Landroid/widget/ImageView;

.field protected mDate:Landroid/widget/TextView;

.field protected mDivider:Landroid/view/View;

.field protected mLabelAndNumber:Landroid/widget/TextView;

.field protected mName:Landroid/widget/TextView;

.field protected mOperator:Landroid/widget/TextView;

.field protected mQuickContactBadge:Landroid/widget/QuickContactBadge;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 2
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/util/AttributeSet;

    invoke-direct {p0, p1, p2}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    iget-object v0, p0, Landroid/view/View;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f090077

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    sput v0, Lcom/mediatek/contacts/dialpad/DialerSearchItemView;->sDialerSearchItemViewHeight:I

    iget-object v0, p0, Landroid/view/View;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f090080

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    sput v0, Lcom/mediatek/contacts/dialpad/DialerSearchItemView;->sListItemQuickContactPaddingTop:I

    iget-object v0, p0, Landroid/view/View;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f090081

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    sput v0, Lcom/mediatek/contacts/dialpad/DialerSearchItemView;->sListItemQuickContactPaddingBottom:I

    return-void
.end method

.method private getLineMaxHeight(Landroid/view/View;I)I
    .locals 1
    .param p1    # Landroid/view/View;
    .param p2    # I

    invoke-virtual {p1}, Landroid/view/View;->getMeasuredHeight()I

    move-result v0

    invoke-static {p2, v0}, Ljava/lang/Math;->max(II)I

    move-result v0

    return v0
.end method

.method private onSubLayout(Landroid/view/View;II)I
    .locals 7
    .param p1    # Landroid/view/View;
    .param p2    # I
    .param p3    # I

    invoke-virtual {p1}, Landroid/view/View;->getMeasuredWidth()I

    move-result v4

    invoke-virtual {p1}, Landroid/view/View;->getMeasuredHeight()I

    move-result v2

    invoke-virtual {p1}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v3

    check-cast v3, Landroid/widget/FrameLayout$LayoutParams;

    instance-of v5, p1, Landroid/widget/ImageView;

    if-eqz v5, :cond_0

    sub-int v5, p3, v2

    add-int/lit8 v1, v5, -0x3

    :goto_0
    iget v5, v3, Landroid/view/ViewGroup$MarginLayoutParams;->leftMargin:I

    add-int v0, p2, v5

    add-int v5, v0, v4

    add-int v6, v1, v2

    invoke-virtual {p1, v0, v1, v5, v6}, Landroid/view/View;->layout(IIII)V

    add-int v5, v0, v4

    return v5

    :cond_0
    sub-int v1, p3, v2

    goto :goto_0
.end method


# virtual methods
.method log(Ljava/lang/String;)V
    .locals 1
    .param p1    # Ljava/lang/String;

    const-string v0, "DialerSearchListItem"

    invoke-static {v0, p1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    return-void
.end method

.method protected onFinishInflate()V
    .locals 1

    const v0, 0x7f07008a

    invoke-virtual {p0, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/QuickContactBadge;

    iput-object v0, p0, Lcom/mediatek/contacts/dialpad/DialerSearchItemView;->mQuickContactBadge:Landroid/widget/QuickContactBadge;

    const v0, 0x7f07010d

    invoke-virtual {p0, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/mediatek/contacts/dialpad/DialerSearchItemView;->mLabelAndNumber:Landroid/widget/TextView;

    const v0, 0x7f07008b

    invoke-virtual {p0, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/mediatek/contacts/dialpad/DialerSearchItemView;->mName:Landroid/widget/TextView;

    const v0, 0x7f07010e

    invoke-virtual {p0, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/mediatek/contacts/dialpad/DialerSearchItemView;->mCallType:Landroid/widget/ImageView;

    const v0, 0x7f07010f

    invoke-virtual {p0, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/mediatek/contacts/dialpad/DialerSearchItemView;->mOperator:Landroid/widget/TextView;

    const v0, 0x7f070057

    invoke-virtual {p0, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/mediatek/contacts/dialpad/DialerSearchItemView;->mDate:Landroid/widget/TextView;

    const v0, 0x7f070091

    invoke-virtual {p0, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/mediatek/contacts/dialpad/DialerSearchItemView;->mDivider:Landroid/view/View;

    const v0, 0x7f070110

    invoke-virtual {p0, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageButton;

    iput-object v0, p0, Lcom/mediatek/contacts/dialpad/DialerSearchItemView;->mCall:Landroid/widget/ImageButton;

    return-void
.end method

.method protected onLayout(ZIIII)V
    .locals 28
    .param p1    # Z
    .param p2    # I
    .param p3    # I
    .param p4    # I
    .param p5    # I

    new-instance v25, Ljava/lang/StringBuilder;

    invoke-direct/range {v25 .. v25}, Ljava/lang/StringBuilder;-><init>()V

    const-string v26, "changed = "

    invoke-virtual/range {v25 .. v26}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v25

    move-object/from16 v0, v25

    move/from16 v1, p1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v25

    const-string v26, " left = "

    invoke-virtual/range {v25 .. v26}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v25

    move-object/from16 v0, v25

    move/from16 v1, p2

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v25

    const-string v26, " top = "

    invoke-virtual/range {v25 .. v26}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v25

    move-object/from16 v0, v25

    move/from16 v1, p3

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v25

    const-string v26, " right = "

    invoke-virtual/range {v25 .. v26}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v25

    move-object/from16 v0, v25

    move/from16 v1, p4

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v25

    const-string v26, " bottom = "

    invoke-virtual/range {v25 .. v26}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v25

    move-object/from16 v0, v25

    move/from16 v1, p5

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v25

    invoke-virtual/range {v25 .. v25}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v25

    move-object/from16 v0, p0

    move-object/from16 v1, v25

    invoke-virtual {v0, v1}, Lcom/mediatek/contacts/dialpad/DialerSearchItemView;->log(Ljava/lang/String;)V

    move-object/from16 v0, p0

    iget v0, v0, Landroid/view/View;->mPaddingLeft:I

    move/from16 v18, v0

    sub-int v25, p4, p2

    move-object/from16 v0, p0

    iget v0, v0, Landroid/view/View;->mPaddingRight:I

    move/from16 v26, v0

    sub-int v19, v25, v26

    sget v20, Lcom/mediatek/contacts/dialpad/DialerSearchItemView;->sListItemQuickContactPaddingTop:I

    sub-int v25, p5, p3

    sget v26, Lcom/mediatek/contacts/dialpad/DialerSearchItemView;->sListItemQuickContactPaddingBottom:I

    sub-int v17, v25, v26

    new-instance v25, Ljava/lang/StringBuilder;

    invoke-direct/range {v25 .. v25}, Ljava/lang/StringBuilder;-><init>()V

    const-string v26, "mPaddingTop = "

    invoke-virtual/range {v25 .. v26}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v25

    move-object/from16 v0, p0

    iget v0, v0, Landroid/view/View;->mPaddingTop:I

    move/from16 v26, v0

    invoke-virtual/range {v25 .. v26}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v25

    const-string v26, " mPaddingBottom = "

    invoke-virtual/range {v25 .. v26}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v25

    move-object/from16 v0, p0

    iget v0, v0, Landroid/view/View;->mPaddingBottom:I

    move/from16 v26, v0

    invoke-virtual/range {v25 .. v26}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v25

    const-string v26, " mPaddingLeft = "

    invoke-virtual/range {v25 .. v26}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v25

    move-object/from16 v0, p0

    iget v0, v0, Landroid/view/View;->mPaddingLeft:I

    move/from16 v26, v0

    invoke-virtual/range {v25 .. v26}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v25

    const-string v26, " mPaddingRight = "

    invoke-virtual/range {v25 .. v26}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v25

    move-object/from16 v0, p0

    iget v0, v0, Landroid/view/View;->mPaddingRight:I

    move/from16 v26, v0

    invoke-virtual/range {v25 .. v26}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v25

    invoke-virtual/range {v25 .. v25}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v25

    move-object/from16 v0, p0

    move-object/from16 v1, v25

    invoke-virtual {v0, v1}, Lcom/mediatek/contacts/dialpad/DialerSearchItemView;->log(Ljava/lang/String;)V

    new-instance v25, Ljava/lang/StringBuilder;

    invoke-direct/range {v25 .. v25}, Ljava/lang/StringBuilder;-><init>()V

    const-string v26, "parentTop = "

    invoke-virtual/range {v25 .. v26}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v25

    move-object/from16 v0, v25

    move/from16 v1, v20

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v25

    const-string v26, " parentBottom = "

    invoke-virtual/range {v25 .. v26}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v25

    move-object/from16 v0, v25

    move/from16 v1, v17

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v25

    invoke-virtual/range {v25 .. v25}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v25

    move-object/from16 v0, p0

    move-object/from16 v1, v25

    invoke-virtual {v0, v1}, Lcom/mediatek/contacts/dialpad/DialerSearchItemView;->log(Ljava/lang/String;)V

    move/from16 v6, p2

    move/from16 v7, p3

    const/4 v9, 0x0

    const/4 v10, 0x0

    const/4 v11, 0x0

    const/4 v14, 0x0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/mediatek/contacts/dialpad/DialerSearchItemView;->mQuickContactBadge:Landroid/widget/QuickContactBadge;

    move-object/from16 v25, v0

    invoke-virtual/range {v25 .. v25}, Landroid/view/View;->getMeasuredWidth()I

    move-result v24

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/mediatek/contacts/dialpad/DialerSearchItemView;->mQuickContactBadge:Landroid/widget/QuickContactBadge;

    move-object/from16 v25, v0

    invoke-virtual/range {v25 .. v25}, Landroid/view/View;->getMeasuredHeight()I

    move-result v8

    sub-int v25, v17, v20

    sub-int v25, v25, v8

    div-int/lit8 v25, v25, 0x2

    add-int v7, v20, v25

    move/from16 v6, v18

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/mediatek/contacts/dialpad/DialerSearchItemView;->mQuickContactBadge:Landroid/widget/QuickContactBadge;

    move-object/from16 v25, v0

    add-int v26, v6, v24

    add-int v27, v7, v8

    move-object/from16 v0, v25

    move/from16 v1, v26

    move/from16 v2, v27

    invoke-virtual {v0, v6, v7, v1, v2}, Landroid/view/View;->layout(IIII)V

    add-int v21, v6, v24

    sget v25, Lcom/mediatek/contacts/dialpad/DialerSearchItemView;->sListItemQuickContactPaddingTop:I

    add-int v25, v25, v8

    sget v26, Lcom/mediatek/contacts/dialpad/DialerSearchItemView;->sListItemQuickContactPaddingBottom:I

    add-int v25, v25, v26

    sput v25, Lcom/mediatek/contacts/dialpad/DialerSearchItemView;->sDialerSearchItemViewHeight:I

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/mediatek/contacts/dialpad/DialerSearchItemView;->mCall:Landroid/widget/ImageButton;

    move-object/from16 v25, v0

    invoke-virtual/range {v25 .. v25}, Landroid/view/View;->getMeasuredWidth()I

    move-result v25

    move-object/from16 v0, p0

    iget v0, v0, Landroid/view/View;->mPaddingRight:I

    move/from16 v26, v0

    add-int v24, v25, v26

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/mediatek/contacts/dialpad/DialerSearchItemView;->mCall:Landroid/widget/ImageButton;

    move-object/from16 v25, v0

    invoke-virtual/range {v25 .. v25}, Landroid/view/View;->getMeasuredHeight()I

    move-result v8

    sget v25, Lcom/mediatek/contacts/dialpad/DialerSearchItemView;->sDialerSearchItemViewHeight:I

    sub-int v25, v25, v8

    div-int/lit8 v7, v25, 0x2

    sub-int v6, v19, v24

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/mediatek/contacts/dialpad/DialerSearchItemView;->mCall:Landroid/widget/ImageButton;

    move-object/from16 v25, v0

    add-int v26, v6, v24

    add-int v27, v7, v8

    move-object/from16 v0, v25

    move/from16 v1, v26

    move/from16 v2, v27

    invoke-virtual {v0, v6, v7, v1, v2}, Landroid/view/View;->layout(IIII)V

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/mediatek/contacts/dialpad/DialerSearchItemView;->mDivider:Landroid/view/View;

    move-object/from16 v25, v0

    invoke-virtual/range {v25 .. v25}, Landroid/view/View;->getMeasuredWidth()I

    move-result v24

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/mediatek/contacts/dialpad/DialerSearchItemView;->mDivider:Landroid/view/View;

    move-object/from16 v25, v0

    invoke-virtual/range {v25 .. v25}, Landroid/view/View;->getMeasuredHeight()I

    move-result v8

    sub-int v25, v17, v20

    sub-int v25, v25, v8

    div-int/lit8 v25, v25, 0x2

    add-int v7, v20, v25

    sub-int v6, v6, v24

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/mediatek/contacts/dialpad/DialerSearchItemView;->mDivider:Landroid/view/View;

    move-object/from16 v25, v0

    add-int v26, v6, v24

    add-int v27, v7, v8

    move-object/from16 v0, v25

    move/from16 v1, v26

    move/from16 v2, v27

    invoke-virtual {v0, v6, v7, v1, v2}, Landroid/view/View;->layout(IIII)V

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/mediatek/contacts/dialpad/DialerSearchItemView;->mName:Landroid/widget/TextView;

    move-object/from16 v25, v0

    invoke-virtual/range {v25 .. v25}, Landroid/view/View;->getMeasuredWidth()I

    move-result v24

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/mediatek/contacts/dialpad/DialerSearchItemView;->mName:Landroid/widget/TextView;

    move-object/from16 v25, v0

    invoke-virtual/range {v25 .. v25}, Landroid/view/View;->getMeasuredHeight()I

    move-result v8

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/mediatek/contacts/dialpad/DialerSearchItemView;->mName:Landroid/widget/TextView;

    move-object/from16 v25, v0

    invoke-virtual/range {v25 .. v25}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v13

    check-cast v13, Landroid/widget/FrameLayout$LayoutParams;

    move/from16 v7, v20

    iget v0, v13, Landroid/view/ViewGroup$MarginLayoutParams;->leftMargin:I

    move/from16 v25, v0

    add-int v6, v21, v25

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/mediatek/contacts/dialpad/DialerSearchItemView;->mName:Landroid/widget/TextView;

    move-object/from16 v25, v0

    add-int v26, v6, v24

    add-int v27, v7, v8

    move-object/from16 v0, v25

    move/from16 v1, v26

    move/from16 v2, v27

    invoke-virtual {v0, v6, v7, v1, v2}, Landroid/view/View;->layout(IIII)V

    add-int v15, v7, v8

    move v9, v8

    add-int/2addr v14, v9

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/mediatek/contacts/dialpad/DialerSearchItemView;->mCallType:Landroid/widget/ImageView;

    move-object/from16 v25, v0

    invoke-virtual/range {v25 .. v25}, Landroid/view/View;->getVisibility()I

    move-result v25

    if-nez v25, :cond_4

    const/4 v5, 0x1

    :goto_0
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/mediatek/contacts/dialpad/DialerSearchItemView;->mCallType:Landroid/widget/ImageView;

    move-object/from16 v25, v0

    invoke-virtual/range {v25 .. v25}, Landroid/view/View;->getVisibility()I

    move-result v25

    if-nez v25, :cond_2

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/mediatek/contacts/dialpad/DialerSearchItemView;->mCallType:Landroid/widget/ImageView;

    move-object/from16 v25, v0

    move-object/from16 v0, p0

    move-object/from16 v1, v25

    move/from16 v2, v21

    move/from16 v3, v17

    invoke-direct {v0, v1, v2, v3}, Lcom/mediatek/contacts/dialpad/DialerSearchItemView;->onSubLayout(Landroid/view/View;II)I

    move-result v4

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/mediatek/contacts/dialpad/DialerSearchItemView;->mCallType:Landroid/widget/ImageView;

    move-object/from16 v25, v0

    move-object/from16 v0, p0

    move-object/from16 v1, v25

    invoke-direct {v0, v1, v11}, Lcom/mediatek/contacts/dialpad/DialerSearchItemView;->getLineMaxHeight(Landroid/view/View;I)I

    move-result v11

    move/from16 v23, v4

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/mediatek/contacts/dialpad/DialerSearchItemView;->mOperator:Landroid/widget/TextView;

    move-object/from16 v25, v0

    invoke-virtual/range {v25 .. v25}, Landroid/view/View;->getVisibility()I

    move-result v25

    if-nez v25, :cond_0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/mediatek/contacts/dialpad/DialerSearchItemView;->mOperator:Landroid/widget/TextView;

    move-object/from16 v25, v0

    move-object/from16 v0, p0

    move-object/from16 v1, v25

    move/from16 v2, v17

    invoke-direct {v0, v1, v4, v2}, Lcom/mediatek/contacts/dialpad/DialerSearchItemView;->onSubLayout(Landroid/view/View;II)I

    move-result v23

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/mediatek/contacts/dialpad/DialerSearchItemView;->mOperator:Landroid/widget/TextView;

    move-object/from16 v25, v0

    move-object/from16 v0, p0

    move-object/from16 v1, v25

    invoke-direct {v0, v1, v11}, Lcom/mediatek/contacts/dialpad/DialerSearchItemView;->getLineMaxHeight(Landroid/view/View;I)I

    move-result v11

    :cond_0
    move/from16 v16, v23

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/mediatek/contacts/dialpad/DialerSearchItemView;->mDate:Landroid/widget/TextView;

    move-object/from16 v25, v0

    invoke-virtual/range {v25 .. v25}, Landroid/view/View;->getVisibility()I

    move-result v25

    if-nez v25, :cond_1

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/mediatek/contacts/dialpad/DialerSearchItemView;->mDate:Landroid/widget/TextView;

    move-object/from16 v25, v0

    move-object/from16 v0, p0

    move-object/from16 v1, v25

    move/from16 v2, v16

    move/from16 v3, v17

    invoke-direct {v0, v1, v2, v3}, Lcom/mediatek/contacts/dialpad/DialerSearchItemView;->onSubLayout(Landroid/view/View;II)I

    move-result v22

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/mediatek/contacts/dialpad/DialerSearchItemView;->mDate:Landroid/widget/TextView;

    move-object/from16 v25, v0

    move-object/from16 v0, p0

    move-object/from16 v1, v25

    invoke-direct {v0, v1, v11}, Lcom/mediatek/contacts/dialpad/DialerSearchItemView;->getLineMaxHeight(Landroid/view/View;I)I

    move-result v11

    :cond_1
    add-int/2addr v14, v11

    :cond_2
    move/from16 v12, v21

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/mediatek/contacts/dialpad/DialerSearchItemView;->mLabelAndNumber:Landroid/widget/TextView;

    move-object/from16 v25, v0

    invoke-virtual/range {v25 .. v25}, Landroid/view/View;->getVisibility()I

    move-result v25

    if-nez v25, :cond_3

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/mediatek/contacts/dialpad/DialerSearchItemView;->mLabelAndNumber:Landroid/widget/TextView;

    move-object/from16 v25, v0

    invoke-virtual/range {v25 .. v25}, Landroid/view/View;->getMeasuredWidth()I

    move-result v24

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/mediatek/contacts/dialpad/DialerSearchItemView;->mLabelAndNumber:Landroid/widget/TextView;

    move-object/from16 v25, v0

    invoke-virtual/range {v25 .. v25}, Landroid/view/View;->getMeasuredHeight()I

    move-result v8

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/mediatek/contacts/dialpad/DialerSearchItemView;->mLabelAndNumber:Landroid/widget/TextView;

    move-object/from16 v25, v0

    invoke-virtual/range {v25 .. v25}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v13

    check-cast v13, Landroid/widget/FrameLayout$LayoutParams;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/mediatek/contacts/dialpad/DialerSearchItemView;->mCallType:Landroid/widget/ImageView;

    move-object/from16 v25, v0

    invoke-virtual/range {v25 .. v25}, Landroid/view/View;->getVisibility()I

    move-result v25

    if-nez v25, :cond_5

    move v7, v15

    :goto_1
    iget v0, v13, Landroid/view/ViewGroup$MarginLayoutParams;->leftMargin:I

    move/from16 v25, v0

    add-int v6, v21, v25

    add-int v12, v6, v24

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/mediatek/contacts/dialpad/DialerSearchItemView;->mLabelAndNumber:Landroid/widget/TextView;

    move-object/from16 v25, v0

    add-int v26, v6, v24

    add-int v27, v7, v8

    move-object/from16 v0, v25

    move/from16 v1, v26

    move/from16 v2, v27

    invoke-virtual {v0, v6, v7, v1, v2}, Landroid/view/View;->layout(IIII)V

    invoke-static {v10, v8}, Ljava/lang/Math;->max(II)I

    move-result v10

    :cond_3
    add-int/2addr v14, v10

    sget v25, Lcom/mediatek/contacts/dialpad/DialerSearchItemView;->sDialerSearchItemViewHeight:I

    move/from16 v0, v25

    invoke-static {v14, v0}, Ljava/lang/Math;->max(II)I

    move-result v25

    sput v25, Lcom/mediatek/contacts/dialpad/DialerSearchItemView;->sDialerSearchItemViewHeight:I

    return-void

    :cond_4
    const/4 v5, 0x0

    goto/16 :goto_0

    :cond_5
    sub-int v7, v17, v8

    goto :goto_1
.end method

.method protected onMeasure(II)V
    .locals 3
    .param p1    # I
    .param p2    # I

    invoke-virtual {p0}, Landroid/view/View;->getSuggestedMinimumWidth()I

    move-result v0

    invoke-super {p0, p1, p2}, Landroid/widget/FrameLayout;->onMeasure(II)V

    const/4 v1, 0x0

    invoke-static {v0, p1, v1}, Lcom/mediatek/contacts/dialpad/DialerSearchItemView;->resolveSizeAndState(III)I

    move-result v1

    sget v2, Lcom/mediatek/contacts/dialpad/DialerSearchItemView;->sDialerSearchItemViewHeight:I

    invoke-virtual {p0, v1, v2}, Landroid/view/View;->setMeasuredDimension(II)V

    return-void
.end method
