.class public Lcom/mediatek/contacts/dialpad/DialerSearchController;
.super Landroid/content/AsyncQueryHandler;
.source "DialerSearchController.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/mediatek/contacts/dialpad/DialerSearchController$DsTextWatcher;,
        Lcom/mediatek/contacts/dialpad/DialerSearchController$DataChangeObserver;,
        Lcom/mediatek/contacts/dialpad/DialerSearchController$DialerSearchResult;,
        Lcom/mediatek/contacts/dialpad/DialerSearchController$OnDialerSearchResult;
    }
.end annotation


# static fields
.field private static final DBG:Z = true

.field public static final DIALER_SEARCH_MODE_ALL:I = 0x0

.field public static final DIALER_SEARCH_MODE_NUMBER:I = 0x1

.field public static final DIALER_SEARCH_PROJECTION:[Ljava/lang/String;

.field private static final QUERY_TOKEN_INCREMENT:I = 0x32

.field private static final QUERY_TOKEN_INIT:I = 0x1e

.field private static final QUERY_TOKEN_NULL:I = 0x28

.field private static final QUERY_TOKEN_SIMPLE:I = 0x3c

.field private static final TAG:Ljava/lang/String; = "DialerSearchController"


# instance fields
.field protected mActivity:Landroid/app/Activity;

.field protected mAdapter:Lcom/mediatek/contacts/dialpad/DialerSearchAdapter;

.field mCallLogContentObserver:Lcom/mediatek/contacts/dialpad/DialerSearchController$DataChangeObserver;

.field private mClearDigitsOnStop:Z

.field private mConfigFromIntent:Z

.field protected mContactsPrefs:Lcom/android/contacts/preference/ContactsPreferences;

.field private mDataChanged:Z

.field protected mDialerSearchCursorCount:I

.field protected mDigitString:Ljava/lang/String;

.field protected mDigits:Landroid/widget/EditText;

.field private mDigitsFromIntent:Ljava/lang/String;

.field private mDsTextWatcher:Lcom/mediatek/contacts/dialpad/DialerSearchController$DsTextWatcher;

.field mFilterContentObserver:Lcom/mediatek/contacts/dialpad/DialerSearchController$DataChangeObserver;

.field private mFragmentView:Landroid/view/View;

.field private mIsFirstLaunched:Z

.field private mIsForeground:Z

.field private mIsLocaleChanging:Z

.field private mIsShowLoadingTip:Z

.field protected mListView:Landroid/widget/ListView;

.field private mLoadTipsContainer:Landroid/widget/LinearLayout;

.field protected mNoResultDigCnt:I

.field protected mOnDialerSearchResult:Lcom/mediatek/contacts/dialpad/DialerSearchController$OnDialerSearchResult;

.field protected mPrevQueryDigCnt:I

.field protected mSearchNumCntQ:Ljava/util/Queue;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Queue",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field protected mSearchNumberOnly:Z

.field protected mSearchTitle:Landroid/widget/TextView;

.field protected mSelectedContactUri:Landroid/net/Uri;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    const/16 v0, 0x10

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v2, "_id"

    aput-object v2, v0, v1

    const/4 v1, 0x1

    const-string v2, "vds_contact_id"

    aput-object v2, v0, v1

    const/4 v1, 0x2

    const-string v2, "vds_call_date"

    aput-object v2, v0, v1

    const/4 v1, 0x3

    const-string v2, "vds_call_log_id"

    aput-object v2, v0, v1

    const/4 v1, 0x4

    const-string v2, "vds_call_type"

    aput-object v2, v0, v1

    const/4 v1, 0x5

    const-string v2, "vds_sim_id"

    aput-object v2, v0, v1

    const/4 v1, 0x6

    const-string v2, "vds_indicate_phone_sim"

    aput-object v2, v0, v1

    const/4 v1, 0x7

    const-string v2, "vds_starred"

    aput-object v2, v0, v1

    const/16 v1, 0x8

    const-string v2, "vds_photo_id"

    aput-object v2, v0, v1

    const/16 v1, 0x9

    const-string v2, "vds_phone_type"

    aput-object v2, v0, v1

    const/16 v1, 0xa

    const-string v2, "vds_name"

    aput-object v2, v0, v1

    const/16 v1, 0xb

    const-string v2, "vds_phone_number"

    aput-object v2, v0, v1

    const/16 v1, 0xc

    const-string v2, "vds_lookup"

    aput-object v2, v0, v1

    const/16 v1, 0xd

    const-string v2, "matched_data_offsets"

    aput-object v2, v0, v1

    const/16 v1, 0xe

    const-string v2, "matched_name_offsets"

    aput-object v2, v0, v1

    const/16 v1, 0xf

    const-string v2, "vds_is_sdn_contact"

    aput-object v2, v0, v1

    sput-object v0, Lcom/mediatek/contacts/dialpad/DialerSearchController;->DIALER_SEARCH_PROJECTION:[Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Landroid/app/Activity;Landroid/view/View;Landroid/widget/ListView;Lcom/mediatek/contacts/dialpad/DialerSearchAdapter$Listener;Lcom/mediatek/contacts/calloption/ContactsCallOptionHandler;)V
    .locals 4
    .param p1    # Landroid/app/Activity;
    .param p2    # Landroid/view/View;
    .param p3    # Landroid/widget/ListView;
    .param p4    # Lcom/mediatek/contacts/dialpad/DialerSearchAdapter$Listener;
    .param p5    # Lcom/mediatek/contacts/calloption/ContactsCallOptionHandler;

    const/4 v3, 0x1

    invoke-virtual {p1}, Landroid/content/ContextWrapper;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    invoke-direct {p0, v0}, Landroid/content/AsyncQueryHandler;-><init>(Landroid/content/ContentResolver;)V

    new-instance v0, Ljava/util/LinkedList;

    invoke-direct {v0}, Ljava/util/LinkedList;-><init>()V

    iput-object v0, p0, Lcom/mediatek/contacts/dialpad/DialerSearchController;->mSearchNumCntQ:Ljava/util/Queue;

    iput-object p1, p0, Lcom/mediatek/contacts/dialpad/DialerSearchController;->mActivity:Landroid/app/Activity;

    iput-object p2, p0, Lcom/mediatek/contacts/dialpad/DialerSearchController;->mFragmentView:Landroid/view/View;

    iget-object v0, p0, Lcom/mediatek/contacts/dialpad/DialerSearchController;->mFragmentView:Landroid/view/View;

    const v1, 0x7f070119

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/EditText;

    iput-object v0, p0, Lcom/mediatek/contacts/dialpad/DialerSearchController;->mDigits:Landroid/widget/EditText;

    iput-object p3, p0, Lcom/mediatek/contacts/dialpad/DialerSearchController;->mListView:Landroid/widget/ListView;

    new-instance v0, Lcom/mediatek/contacts/dialpad/DialerSearchAdapter;

    invoke-direct {v0, p1, p4, p5}, Lcom/mediatek/contacts/dialpad/DialerSearchAdapter;-><init>(Landroid/content/Context;Lcom/mediatek/contacts/dialpad/DialerSearchAdapter$Listener;Lcom/mediatek/contacts/calloption/ContactsCallOptionHandler;)V

    iput-object v0, p0, Lcom/mediatek/contacts/dialpad/DialerSearchController;->mAdapter:Lcom/mediatek/contacts/dialpad/DialerSearchAdapter;

    iget-object v0, p0, Lcom/mediatek/contacts/dialpad/DialerSearchController;->mListView:Landroid/widget/ListView;

    iget-object v1, p0, Lcom/mediatek/contacts/dialpad/DialerSearchController;->mAdapter:Lcom/mediatek/contacts/dialpad/DialerSearchAdapter;

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    new-instance v0, Lcom/mediatek/contacts/dialpad/DialerSearchController$DataChangeObserver;

    invoke-direct {v0, p0}, Lcom/mediatek/contacts/dialpad/DialerSearchController$DataChangeObserver;-><init>(Lcom/mediatek/contacts/dialpad/DialerSearchController;)V

    iput-object v0, p0, Lcom/mediatek/contacts/dialpad/DialerSearchController;->mCallLogContentObserver:Lcom/mediatek/contacts/dialpad/DialerSearchController$DataChangeObserver;

    new-instance v0, Lcom/mediatek/contacts/dialpad/DialerSearchController$DataChangeObserver;

    invoke-direct {v0, p0}, Lcom/mediatek/contacts/dialpad/DialerSearchController$DataChangeObserver;-><init>(Lcom/mediatek/contacts/dialpad/DialerSearchController;)V

    iput-object v0, p0, Lcom/mediatek/contacts/dialpad/DialerSearchController;->mFilterContentObserver:Lcom/mediatek/contacts/dialpad/DialerSearchController$DataChangeObserver;

    iget-object v0, p0, Lcom/mediatek/contacts/dialpad/DialerSearchController;->mActivity:Landroid/app/Activity;

    invoke-virtual {v0}, Landroid/content/ContextWrapper;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const-string v1, "content://com.android.contacts.dialer_search/callLog/"

    invoke-static {v1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    iget-object v2, p0, Lcom/mediatek/contacts/dialpad/DialerSearchController;->mCallLogContentObserver:Lcom/mediatek/contacts/dialpad/DialerSearchController$DataChangeObserver;

    invoke-virtual {v0, v1, v3, v2}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;)V

    iget-object v0, p0, Lcom/mediatek/contacts/dialpad/DialerSearchController;->mActivity:Landroid/app/Activity;

    invoke-virtual {v0}, Landroid/content/ContextWrapper;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const-string v1, "content://com.android.contacts/dialer_search/filter/"

    invoke-static {v1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    iget-object v2, p0, Lcom/mediatek/contacts/dialpad/DialerSearchController;->mFilterContentObserver:Lcom/mediatek/contacts/dialpad/DialerSearchController$DataChangeObserver;

    invoke-virtual {v0, v1, v3, v2}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;)V

    new-instance v0, Lcom/android/contacts/preference/ContactsPreferences;

    invoke-direct {v0, p1}, Lcom/android/contacts/preference/ContactsPreferences;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/mediatek/contacts/dialpad/DialerSearchController;->mContactsPrefs:Lcom/android/contacts/preference/ContactsPreferences;

    iget-object v0, p0, Lcom/mediatek/contacts/dialpad/DialerSearchController;->mContactsPrefs:Lcom/android/contacts/preference/ContactsPreferences;

    new-instance v1, Lcom/mediatek/contacts/dialpad/DialerSearchController$1;

    invoke-direct {v1, p0}, Lcom/mediatek/contacts/dialpad/DialerSearchController$1;-><init>(Lcom/mediatek/contacts/dialpad/DialerSearchController;)V

    invoke-virtual {v0, v1}, Lcom/android/contacts/preference/ContactsPreferences;->registerChangeListener(Lcom/android/contacts/preference/ContactsPreferences$ChangeListener;)V

    invoke-virtual {p0}, Lcom/mediatek/contacts/dialpad/DialerSearchController;->setDialerSearchTextWatcher()V

    iput-boolean v3, p0, Lcom/mediatek/contacts/dialpad/DialerSearchController;->mIsFirstLaunched:Z

    return-void
.end method

.method static synthetic access$000(Lcom/mediatek/contacts/dialpad/DialerSearchController;)Z
    .locals 1
    .param p0    # Lcom/mediatek/contacts/dialpad/DialerSearchController;

    iget-boolean v0, p0, Lcom/mediatek/contacts/dialpad/DialerSearchController;->mIsForeground:Z

    return v0
.end method

.method static synthetic access$100(Lcom/mediatek/contacts/dialpad/DialerSearchController;)V
    .locals 0
    .param p0    # Lcom/mediatek/contacts/dialpad/DialerSearchController;

    invoke-direct {p0}, Lcom/mediatek/contacts/dialpad/DialerSearchController;->forceQueryIfNeeded()V

    return-void
.end method

.method static synthetic access$200(Lcom/mediatek/contacts/dialpad/DialerSearchController;)Z
    .locals 1
    .param p0    # Lcom/mediatek/contacts/dialpad/DialerSearchController;

    iget-boolean v0, p0, Lcom/mediatek/contacts/dialpad/DialerSearchController;->mDataChanged:Z

    return v0
.end method

.method static synthetic access$202(Lcom/mediatek/contacts/dialpad/DialerSearchController;Z)Z
    .locals 0
    .param p0    # Lcom/mediatek/contacts/dialpad/DialerSearchController;
    .param p1    # Z

    iput-boolean p1, p0, Lcom/mediatek/contacts/dialpad/DialerSearchController;->mDataChanged:Z

    return p1
.end method

.method static synthetic access$300(Lcom/mediatek/contacts/dialpad/DialerSearchController;)Z
    .locals 1
    .param p0    # Lcom/mediatek/contacts/dialpad/DialerSearchController;

    iget-boolean v0, p0, Lcom/mediatek/contacts/dialpad/DialerSearchController;->mIsFirstLaunched:Z

    return v0
.end method

.method static synthetic access$400(Lcom/mediatek/contacts/dialpad/DialerSearchController;)Landroid/view/View;
    .locals 1
    .param p0    # Lcom/mediatek/contacts/dialpad/DialerSearchController;

    iget-object v0, p0, Lcom/mediatek/contacts/dialpad/DialerSearchController;->mFragmentView:Landroid/view/View;

    return-object v0
.end method

.method private forceLoadAll()V
    .locals 2

    iget-object v0, p0, Lcom/mediatek/contacts/dialpad/DialerSearchController;->mDigits:Landroid/widget/EditText;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/mediatek/contacts/dialpad/DialerSearchController;->mDataChanged:Z

    iget-object v0, p0, Lcom/mediatek/contacts/dialpad/DialerSearchController;->mDigits:Landroid/widget/EditText;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    :cond_0
    return-void
.end method

.method private forceQueryIfNeeded()V
    .locals 1

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/mediatek/contacts/dialpad/DialerSearchController;->mDataChanged:Z

    iget-object v0, p0, Lcom/mediatek/contacts/dialpad/DialerSearchController;->mDigits:Landroid/widget/EditText;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/mediatek/contacts/dialpad/DialerSearchController;->mDigits:Landroid/widget/EditText;

    invoke-virtual {v0}, Landroid/widget/TextView;->length()I

    move-result v0

    if-nez v0, :cond_0

    invoke-direct {p0}, Lcom/mediatek/contacts/dialpad/DialerSearchController;->forceLoadAll()V

    :cond_0
    return-void
.end method


# virtual methods
.method public clearDialerSearchTextWatcher()V
    .locals 2

    iget-object v0, p0, Lcom/mediatek/contacts/dialpad/DialerSearchController;->mDsTextWatcher:Lcom/mediatek/contacts/dialpad/DialerSearchController$DsTextWatcher;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/mediatek/contacts/dialpad/DialerSearchController;->mDigits:Landroid/widget/EditText;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/mediatek/contacts/dialpad/DialerSearchController;->mDigits:Landroid/widget/EditText;

    iget-object v1, p0, Lcom/mediatek/contacts/dialpad/DialerSearchController;->mDsTextWatcher:Lcom/mediatek/contacts/dialpad/DialerSearchController$DsTextWatcher;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->removeTextChangedListener(Landroid/text/TextWatcher;)V

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/mediatek/contacts/dialpad/DialerSearchController;->mDsTextWatcher:Lcom/mediatek/contacts/dialpad/DialerSearchController$DsTextWatcher;

    :cond_0
    return-void
.end method

.method public configureFromIntent(Z)V
    .locals 2
    .param p1    # Z

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "[configureFromIntent]digitsFilled:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/mediatek/contacts/dialpad/DialerSearchController;->log(Ljava/lang/String;)V

    iput-boolean p1, p0, Lcom/mediatek/contacts/dialpad/DialerSearchController;->mConfigFromIntent:Z

    iget-boolean v0, p0, Lcom/mediatek/contacts/dialpad/DialerSearchController;->mConfigFromIntent:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/mediatek/contacts/dialpad/DialerSearchController;->mDigits:Landroid/widget/EditText;

    invoke-virtual {v0}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/mediatek/contacts/dialpad/DialerSearchController;->mDigitsFromIntent:Ljava/lang/String;

    :cond_0
    return-void
.end method

.method public forceUpdate()V
    .locals 0

    invoke-direct {p0}, Lcom/mediatek/contacts/dialpad/DialerSearchController;->forceLoadAll()V

    return-void
.end method

.method public hasDialerSearchTextWatcher()Z
    .locals 1

    iget-object v0, p0, Lcom/mediatek/contacts/dialpad/DialerSearchController;->mDsTextWatcher:Lcom/mediatek/contacts/dialpad/DialerSearchController$DsTextWatcher;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isSearchNumberOnly()Z
    .locals 1

    iget-boolean v0, p0, Lcom/mediatek/contacts/dialpad/DialerSearchController;->mSearchNumberOnly:Z

    return v0
.end method

.method log(Ljava/lang/String;)V
    .locals 1
    .param p1    # Ljava/lang/String;

    const-string v0, "DialerSearchController"

    invoke-static {v0, p1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    return-void
.end method

.method obtainDialerSearchResult(I)Lcom/mediatek/contacts/dialpad/DialerSearchController$DialerSearchResult;
    .locals 1
    .param p1    # I

    new-instance v0, Lcom/mediatek/contacts/dialpad/DialerSearchController$DialerSearchResult;

    invoke-direct {v0}, Lcom/mediatek/contacts/dialpad/DialerSearchController$DialerSearchResult;-><init>()V

    iput p1, v0, Lcom/mediatek/contacts/dialpad/DialerSearchController$DialerSearchResult;->mCount:I

    return-object v0
.end method

.method public onActivityCreated(Landroid/os/Bundle;)V
    .locals 2
    .param p1    # Landroid/os/Bundle;

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "[onActivityCreated]savedInstance null:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    if-nez p1, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/mediatek/contacts/dialpad/DialerSearchController;->log(Ljava/lang/String;)V

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public onDestroy()V
    .locals 2

    const-string v0, "[onDestroy]"

    invoke-virtual {p0, v0}, Lcom/mediatek/contacts/dialpad/DialerSearchController;->log(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/mediatek/contacts/dialpad/DialerSearchController;->mCallLogContentObserver:Lcom/mediatek/contacts/dialpad/DialerSearchController$DataChangeObserver;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/mediatek/contacts/dialpad/DialerSearchController;->mActivity:Landroid/app/Activity;

    invoke-virtual {v0}, Landroid/content/ContextWrapper;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    iget-object v1, p0, Lcom/mediatek/contacts/dialpad/DialerSearchController;->mCallLogContentObserver:Lcom/mediatek/contacts/dialpad/DialerSearchController$DataChangeObserver;

    invoke-virtual {v0, v1}, Landroid/content/ContentResolver;->unregisterContentObserver(Landroid/database/ContentObserver;)V

    :cond_0
    iget-object v0, p0, Lcom/mediatek/contacts/dialpad/DialerSearchController;->mFilterContentObserver:Lcom/mediatek/contacts/dialpad/DialerSearchController$DataChangeObserver;

    if-eqz v0, :cond_1

    const-string v0, "DialerSearchController onDestroy : unregister the filter observer."

    invoke-virtual {p0, v0}, Lcom/mediatek/contacts/dialpad/DialerSearchController;->log(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/mediatek/contacts/dialpad/DialerSearchController;->mActivity:Landroid/app/Activity;

    invoke-virtual {v0}, Landroid/content/ContextWrapper;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    iget-object v1, p0, Lcom/mediatek/contacts/dialpad/DialerSearchController;->mFilterContentObserver:Lcom/mediatek/contacts/dialpad/DialerSearchController$DataChangeObserver;

    invoke-virtual {v0, v1}, Landroid/content/ContentResolver;->unregisterContentObserver(Landroid/database/ContentObserver;)V

    :cond_1
    invoke-virtual {p0}, Lcom/mediatek/contacts/dialpad/DialerSearchController;->clearDialerSearchTextWatcher()V

    iget-object v0, p0, Lcom/mediatek/contacts/dialpad/DialerSearchController;->mContactsPrefs:Lcom/android/contacts/preference/ContactsPreferences;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/mediatek/contacts/dialpad/DialerSearchController;->mContactsPrefs:Lcom/android/contacts/preference/ContactsPreferences;

    invoke-virtual {v0}, Lcom/android/contacts/preference/ContactsPreferences;->unregisterChangeListener()V

    :cond_2
    return-void
.end method

.method public onPause()V
    .locals 1

    const-string v0, "[onPause]"

    invoke-virtual {p0, v0}, Lcom/mediatek/contacts/dialpad/DialerSearchController;->log(Ljava/lang/String;)V

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/mediatek/contacts/dialpad/DialerSearchController;->mIsForeground:Z

    return-void
.end method

.method protected onQueryComplete(ILjava/lang/Object;Landroid/database/Cursor;)V
    .locals 7
    .param p1    # I
    .param p2    # Ljava/lang/Object;
    .param p3    # Landroid/database/Cursor;

    const/4 v6, 0x0

    const/4 v5, 0x0

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "[onQueryComplete]mIsShowLoadingTip:"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-boolean v4, p0, Lcom/mediatek/contacts/dialpad/DialerSearchController;->mIsShowLoadingTip:Z

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p0, v3}, Lcom/mediatek/contacts/dialpad/DialerSearchController;->log(Ljava/lang/String;)V

    iget-boolean v3, p0, Lcom/mediatek/contacts/dialpad/DialerSearchController;->mIsShowLoadingTip:Z

    if-eqz v3, :cond_0

    iget-object v3, p0, Lcom/mediatek/contacts/dialpad/DialerSearchController;->mFragmentView:Landroid/view/View;

    invoke-virtual {p0, v3, v5, v6, v5}, Lcom/mediatek/contacts/dialpad/DialerSearchController;->showLoadingTips(Landroid/view/View;ZLjava/lang/String;Z)V

    :cond_0
    iget-object v3, p0, Lcom/mediatek/contacts/dialpad/DialerSearchController;->mSearchNumCntQ:Ljava/util/Queue;

    invoke-interface {v3}, Ljava/util/Queue;->poll()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    if-eqz v0, :cond_1

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v3

    iput v3, p0, Lcom/mediatek/contacts/dialpad/DialerSearchController;->mPrevQueryDigCnt:I

    :cond_1
    const-string v3, "+onQueryComplete"

    invoke-virtual {p0, v3}, Lcom/mediatek/contacts/dialpad/DialerSearchController;->log(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/mediatek/contacts/dialpad/DialerSearchController;->mAdapter:Lcom/mediatek/contacts/dialpad/DialerSearchAdapter;

    if-eqz p3, :cond_5

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "[onQueryComplete]mIsLocaleChanging"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-boolean v4, p0, Lcom/mediatek/contacts/dialpad/DialerSearchController;->mIsLocaleChanging:Z

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p0, v3}, Lcom/mediatek/contacts/dialpad/DialerSearchController;->log(Ljava/lang/String;)V

    iget-boolean v3, p0, Lcom/mediatek/contacts/dialpad/DialerSearchController;->mIsLocaleChanging:Z

    if-eqz v3, :cond_2

    invoke-interface {p3}, Landroid/database/Cursor;->close()V

    :goto_0
    return-void

    :cond_2
    invoke-interface {p3}, Landroid/database/Cursor;->getCount()I

    move-result v3

    iput v3, p0, Lcom/mediatek/contacts/dialpad/DialerSearchController;->mDialerSearchCursorCount:I

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "[onQueryComplete]cursor count: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget v4, p0, Lcom/mediatek/contacts/dialpad/DialerSearchController;->mDialerSearchCursorCount:I

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p0, v3}, Lcom/mediatek/contacts/dialpad/DialerSearchController;->log(Ljava/lang/String;)V

    iget-object v3, p0, Lcom/mediatek/contacts/dialpad/DialerSearchController;->mDigits:Landroid/widget/EditText;

    invoke-virtual {v3}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v2

    if-eqz v2, :cond_7

    iget v3, p0, Lcom/mediatek/contacts/dialpad/DialerSearchController;->mDialerSearchCursorCount:I

    if-lez v3, :cond_7

    iput v5, p0, Lcom/mediatek/contacts/dialpad/DialerSearchController;->mNoResultDigCnt:I

    iget-object v3, p0, Lcom/mediatek/contacts/dialpad/DialerSearchController;->mSearchTitle:Landroid/widget/TextView;

    if-eqz v3, :cond_3

    iget-object v3, p0, Lcom/mediatek/contacts/dialpad/DialerSearchController;->mSearchTitle:Landroid/widget/TextView;

    const/16 v4, 0x8

    invoke-virtual {v3, v4}, Landroid/view/View;->setVisibility(I)V

    :cond_3
    invoke-static {v2}, Lcom/mediatek/contacts/dialpad/DialerSearchUtils;->tripHyphen(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v3

    iget v4, p0, Lcom/mediatek/contacts/dialpad/DialerSearchController;->mPrevQueryDigCnt:I

    if-ne v3, v4, :cond_6

    iget-object v3, p0, Lcom/mediatek/contacts/dialpad/DialerSearchController;->mOnDialerSearchResult:Lcom/mediatek/contacts/dialpad/DialerSearchController$OnDialerSearchResult;

    if-eqz v3, :cond_4

    iget-object v3, p0, Lcom/mediatek/contacts/dialpad/DialerSearchController;->mOnDialerSearchResult:Lcom/mediatek/contacts/dialpad/DialerSearchController$OnDialerSearchResult;

    iget v4, p0, Lcom/mediatek/contacts/dialpad/DialerSearchController;->mDialerSearchCursorCount:I

    invoke-virtual {p0, v4}, Lcom/mediatek/contacts/dialpad/DialerSearchController;->obtainDialerSearchResult(I)Lcom/mediatek/contacts/dialpad/DialerSearchController$DialerSearchResult;

    move-result-object v4

    invoke-interface {v3, v4}, Lcom/mediatek/contacts/dialpad/DialerSearchController$OnDialerSearchResult;->onDialerSearchResult(Lcom/mediatek/contacts/dialpad/DialerSearchController$DialerSearchResult;)V

    :cond_4
    invoke-virtual {v1, p3}, Lcom/mediatek/contacts/dialpad/DialerSearchAdapter;->setResultCursor(Landroid/database/Cursor;)V

    invoke-virtual {v1, p3}, Landroid/widget/CursorAdapter;->changeCursor(Landroid/database/Cursor;)V

    :cond_5
    :goto_1
    const-string v3, "-onQueryComplete"

    invoke-virtual {p0, v3}, Lcom/mediatek/contacts/dialpad/DialerSearchController;->log(Ljava/lang/String;)V

    goto :goto_0

    :cond_6
    invoke-interface {p3}, Landroid/database/Cursor;->close()V

    goto :goto_1

    :cond_7
    iget-object v3, p0, Lcom/mediatek/contacts/dialpad/DialerSearchController;->mOnDialerSearchResult:Lcom/mediatek/contacts/dialpad/DialerSearchController$OnDialerSearchResult;

    if-eqz v3, :cond_8

    iget-object v3, p0, Lcom/mediatek/contacts/dialpad/DialerSearchController;->mOnDialerSearchResult:Lcom/mediatek/contacts/dialpad/DialerSearchController$OnDialerSearchResult;

    iget v4, p0, Lcom/mediatek/contacts/dialpad/DialerSearchController;->mDialerSearchCursorCount:I

    invoke-virtual {p0, v4}, Lcom/mediatek/contacts/dialpad/DialerSearchController;->obtainDialerSearchResult(I)Lcom/mediatek/contacts/dialpad/DialerSearchController$DialerSearchResult;

    move-result-object v4

    invoke-interface {v3, v4}, Lcom/mediatek/contacts/dialpad/DialerSearchController$OnDialerSearchResult;->onDialerSearchResult(Lcom/mediatek/contacts/dialpad/DialerSearchController$DialerSearchResult;)V

    :cond_8
    iget-object v3, p0, Lcom/mediatek/contacts/dialpad/DialerSearchController;->mDigits:Landroid/widget/EditText;

    invoke-virtual {v3}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v3

    invoke-interface {v3}, Landroid/text/Editable;->length()I

    move-result v3

    iput v3, p0, Lcom/mediatek/contacts/dialpad/DialerSearchController;->mNoResultDigCnt:I

    invoke-interface {p3}, Landroid/database/Cursor;->close()V

    invoke-virtual {v1, v6}, Lcom/mediatek/contacts/dialpad/DialerSearchAdapter;->setResultCursor(Landroid/database/Cursor;)V

    iget-object v3, p0, Lcom/mediatek/contacts/dialpad/DialerSearchController;->mSearchTitle:Landroid/widget/TextView;

    if-eqz v3, :cond_5

    iget-object v3, p0, Lcom/mediatek/contacts/dialpad/DialerSearchController;->mDigits:Landroid/widget/EditText;

    invoke-virtual {v3}, Landroid/widget/TextView;->length()I

    move-result v3

    if-lez v3, :cond_9

    iget-object v3, p0, Lcom/mediatek/contacts/dialpad/DialerSearchController;->mSearchTitle:Landroid/widget/TextView;

    const v4, 0x7f0c00da

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setText(I)V

    :goto_2
    iget-object v3, p0, Lcom/mediatek/contacts/dialpad/DialerSearchController;->mSearchTitle:Landroid/widget/TextView;

    invoke-virtual {v3, v5}, Landroid/view/View;->setVisibility(I)V

    goto :goto_1

    :cond_9
    iget-object v3, p0, Lcom/mediatek/contacts/dialpad/DialerSearchController;->mSearchTitle:Landroid/widget/TextView;

    const v4, 0x7f0c00db

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setText(I)V

    goto :goto_2
.end method

.method public onResume()V
    .locals 4

    const/4 v2, 0x0

    const/4 v1, 0x1

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "[onResume]mAdapter is null:"

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v0, p0, Lcom/mediatek/contacts/dialpad/DialerSearchController;->mAdapter:Lcom/mediatek/contacts/dialpad/DialerSearchAdapter;

    if-nez v0, :cond_2

    move v0, v1

    :goto_0
    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/mediatek/contacts/dialpad/DialerSearchController;->log(Ljava/lang/String;)V

    iget-boolean v0, p0, Lcom/mediatek/contacts/dialpad/DialerSearchController;->mConfigFromIntent:Z

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/mediatek/contacts/dialpad/DialerSearchController;->mDigits:Landroid/widget/EditText;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/mediatek/contacts/dialpad/DialerSearchController;->mDigitsFromIntent:Ljava/lang/String;

    if-eqz v0, :cond_0

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "[configureFromIntent]current Text:"

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v3, p0, Lcom/mediatek/contacts/dialpad/DialerSearchController;->mDigits:Landroid/widget/EditText;

    invoke-virtual {v3}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v3

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/mediatek/contacts/dialpad/DialerSearchController;->log(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/mediatek/contacts/dialpad/DialerSearchController;->mDigits:Landroid/widget/EditText;

    iget-object v3, p0, Lcom/mediatek/contacts/dialpad/DialerSearchController;->mDigitsFromIntent:Ljava/lang/String;

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lcom/mediatek/contacts/dialpad/DialerSearchController;->mDigits:Landroid/widget/EditText;

    iget-object v3, p0, Lcom/mediatek/contacts/dialpad/DialerSearchController;->mDigits:Landroid/widget/EditText;

    invoke-virtual {v3}, Landroid/widget/TextView;->length()I

    move-result v3

    invoke-virtual {v0, v3}, Landroid/widget/EditText;->setSelection(I)V

    :cond_0
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/mediatek/contacts/dialpad/DialerSearchController;->mDigitsFromIntent:Ljava/lang/String;

    iput-boolean v2, p0, Lcom/mediatek/contacts/dialpad/DialerSearchController;->mConfigFromIntent:Z

    :cond_1
    :goto_1
    iput-boolean v1, p0, Lcom/mediatek/contacts/dialpad/DialerSearchController;->mIsForeground:Z

    return-void

    :cond_2
    move v0, v2

    goto :goto_0

    :cond_3
    iget-object v0, p0, Lcom/mediatek/contacts/dialpad/DialerSearchController;->mAdapter:Lcom/mediatek/contacts/dialpad/DialerSearchAdapter;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/mediatek/contacts/dialpad/DialerSearchController;->mAdapter:Lcom/mediatek/contacts/dialpad/DialerSearchAdapter;

    invoke-virtual {v0}, Lcom/mediatek/contacts/dialpad/DialerSearchAdapter;->onResume()V

    iget-object v0, p0, Lcom/mediatek/contacts/dialpad/DialerSearchController;->mAdapter:Lcom/mediatek/contacts/dialpad/DialerSearchAdapter;

    invoke-virtual {v0}, Lcom/mediatek/contacts/dialpad/DialerSearchAdapter;->isDigitsCleared()Z

    move-result v0

    if-eqz v0, :cond_4

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "[onResume]mAdapter isDigitsCleared:"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v2, p0, Lcom/mediatek/contacts/dialpad/DialerSearchController;->mAdapter:Lcom/mediatek/contacts/dialpad/DialerSearchAdapter;

    invoke-virtual {v2}, Lcom/mediatek/contacts/dialpad/DialerSearchAdapter;->isDigitsCleared()Z

    move-result v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/mediatek/contacts/dialpad/DialerSearchController;->log(Ljava/lang/String;)V

    invoke-direct {p0}, Lcom/mediatek/contacts/dialpad/DialerSearchController;->forceLoadAll()V

    iget-object v0, p0, Lcom/mediatek/contacts/dialpad/DialerSearchController;->mAdapter:Lcom/mediatek/contacts/dialpad/DialerSearchAdapter;

    invoke-virtual {v0}, Lcom/mediatek/contacts/dialpad/DialerSearchAdapter;->resetDigitsState()V

    goto :goto_1

    :cond_4
    iget-boolean v0, p0, Lcom/mediatek/contacts/dialpad/DialerSearchController;->mDataChanged:Z

    if-eqz v0, :cond_5

    invoke-direct {p0}, Lcom/mediatek/contacts/dialpad/DialerSearchController;->forceLoadAll()V

    goto :goto_1

    :cond_5
    iget-object v0, p0, Lcom/mediatek/contacts/dialpad/DialerSearchController;->mDigits:Landroid/widget/EditText;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/mediatek/contacts/dialpad/DialerSearchController;->mDigits:Landroid/widget/EditText;

    invoke-virtual {v0}, Landroid/widget/TextView;->length()I

    move-result v0

    if-lez v0, :cond_1

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "[onResume] Query Dialersearch DB with tel: "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v2, p0, Lcom/mediatek/contacts/dialpad/DialerSearchController;->mDigits:Landroid/widget/EditText;

    invoke-virtual {v2}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/mediatek/contacts/dialpad/DialerSearchController;->log(Ljava/lang/String;)V

    invoke-virtual {p0, v1}, Lcom/mediatek/contacts/dialpad/DialerSearchController;->setSearchNumberMode(Z)V

    iget-object v0, p0, Lcom/mediatek/contacts/dialpad/DialerSearchController;->mDigits:Landroid/widget/EditText;

    iget-object v2, p0, Lcom/mediatek/contacts/dialpad/DialerSearchController;->mDigits:Landroid/widget/EditText;

    invoke-virtual {v2}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lcom/mediatek/contacts/dialpad/DialerSearchController;->mDigits:Landroid/widget/EditText;

    iget-object v2, p0, Lcom/mediatek/contacts/dialpad/DialerSearchController;->mDigits:Landroid/widget/EditText;

    invoke-virtual {v2}, Landroid/widget/TextView;->length()I

    move-result v2

    invoke-virtual {v0, v2}, Landroid/widget/EditText;->setSelection(I)V

    goto/16 :goto_1
.end method

.method public onScrollStateChanged(Landroid/widget/AbsListView;I)V
    .locals 1
    .param p1    # Landroid/widget/AbsListView;
    .param p2    # I

    iget-object v0, p0, Lcom/mediatek/contacts/dialpad/DialerSearchController;->mAdapter:Lcom/mediatek/contacts/dialpad/DialerSearchAdapter;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/mediatek/contacts/dialpad/DialerSearchController;->mAdapter:Lcom/mediatek/contacts/dialpad/DialerSearchAdapter;

    invoke-virtual {v0, p1, p2}, Lcom/mediatek/contacts/dialpad/DialerSearchAdapter;->onScrollStateChanged(Landroid/widget/AbsListView;I)V

    :cond_0
    return-void
.end method

.method public onStart()V
    .locals 2

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "[onStart]mIsFirstLaunched:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-boolean v1, p0, Lcom/mediatek/contacts/dialpad/DialerSearchController;->mIsFirstLaunched:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "|mDataChanged:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-boolean v1, p0, Lcom/mediatek/contacts/dialpad/DialerSearchController;->mDataChanged:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/mediatek/contacts/dialpad/DialerSearchController;->log(Ljava/lang/String;)V

    iget-boolean v0, p0, Lcom/mediatek/contacts/dialpad/DialerSearchController;->mIsFirstLaunched:Z

    if-eqz v0, :cond_1

    invoke-direct {p0}, Lcom/mediatek/contacts/dialpad/DialerSearchController;->forceLoadAll()V

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/mediatek/contacts/dialpad/DialerSearchController;->mIsFirstLaunched:Z

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-boolean v0, p0, Lcom/mediatek/contacts/dialpad/DialerSearchController;->mDataChanged:Z

    if-eqz v0, :cond_0

    invoke-direct {p0}, Lcom/mediatek/contacts/dialpad/DialerSearchController;->forceQueryIfNeeded()V

    goto :goto_0
.end method

.method public onStop()V
    .locals 2

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "[onStop]mClearDigitsOnStop:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-boolean v1, p0, Lcom/mediatek/contacts/dialpad/DialerSearchController;->mClearDigitsOnStop:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/mediatek/contacts/dialpad/DialerSearchController;->log(Ljava/lang/String;)V

    const-string v0, "[onStop] Reserve tel number as the dialpad is stopping!"

    invoke-virtual {p0, v0}, Lcom/mediatek/contacts/dialpad/DialerSearchController;->log(Ljava/lang/String;)V

    return-void
.end method

.method public setClearDigitsOnStop(Z)V
    .locals 0
    .param p1    # Z

    iput-boolean p1, p0, Lcom/mediatek/contacts/dialpad/DialerSearchController;->mClearDigitsOnStop:Z

    return-void
.end method

.method public setDialerSearchTextWatcher()V
    .locals 2

    iget-object v0, p0, Lcom/mediatek/contacts/dialpad/DialerSearchController;->mDsTextWatcher:Lcom/mediatek/contacts/dialpad/DialerSearchController$DsTextWatcher;

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/mediatek/contacts/dialpad/DialerSearchController;->mDigits:Landroid/widget/EditText;

    if-eqz v0, :cond_0

    new-instance v0, Lcom/mediatek/contacts/dialpad/DialerSearchController$DsTextWatcher;

    invoke-direct {v0, p0}, Lcom/mediatek/contacts/dialpad/DialerSearchController$DsTextWatcher;-><init>(Lcom/mediatek/contacts/dialpad/DialerSearchController;)V

    iput-object v0, p0, Lcom/mediatek/contacts/dialpad/DialerSearchController;->mDsTextWatcher:Lcom/mediatek/contacts/dialpad/DialerSearchController$DsTextWatcher;

    iget-object v0, p0, Lcom/mediatek/contacts/dialpad/DialerSearchController;->mDigits:Landroid/widget/EditText;

    iget-object v1, p0, Lcom/mediatek/contacts/dialpad/DialerSearchController;->mDsTextWatcher:Lcom/mediatek/contacts/dialpad/DialerSearchController$DsTextWatcher;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->addTextChangedListener(Landroid/text/TextWatcher;)V

    :cond_0
    return-void
.end method

.method public setDialerSearchTitle(Landroid/widget/TextView;)V
    .locals 0
    .param p1    # Landroid/widget/TextView;

    iput-object p1, p0, Lcom/mediatek/contacts/dialpad/DialerSearchController;->mSearchTitle:Landroid/widget/TextView;

    return-void
.end method

.method public setOnDialerSearchResult(Lcom/mediatek/contacts/dialpad/DialerSearchController$OnDialerSearchResult;)V
    .locals 0
    .param p1    # Lcom/mediatek/contacts/dialpad/DialerSearchController$OnDialerSearchResult;

    iput-object p1, p0, Lcom/mediatek/contacts/dialpad/DialerSearchController;->mOnDialerSearchResult:Lcom/mediatek/contacts/dialpad/DialerSearchController$OnDialerSearchResult;

    return-void
.end method

.method public setSearchNumberMode(Z)V
    .locals 0
    .param p1    # Z

    iput-boolean p1, p0, Lcom/mediatek/contacts/dialpad/DialerSearchController;->mSearchNumberOnly:Z

    return-void
.end method

.method public showLoadingTips(Landroid/view/View;ZLjava/lang/String;Z)V
    .locals 22
    .param p1    # Landroid/view/View;
    .param p2    # Z
    .param p3    # Ljava/lang/String;
    .param p4    # Z

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/mediatek/contacts/dialpad/DialerSearchController;->mActivity:Landroid/app/Activity;

    move-object/from16 v19, v0

    invoke-virtual/range {v19 .. v19}, Landroid/view/ContextThemeWrapper;->getResources()Landroid/content/res/Resources;

    move-result-object v19

    invoke-virtual/range {v19 .. v19}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v19

    move-object/from16 v0, v19

    iget v0, v0, Landroid/content/res/Configuration;->orientation:I

    move/from16 v19, v0

    const/16 v20, 0x2

    move/from16 v0, v19

    move/from16 v1, v20

    if-ne v0, v1, :cond_1

    const-string v19, "[showLocaleChangeTips] return lanscape."

    move-object/from16 v0, p0

    move-object/from16 v1, v19

    invoke-virtual {v0, v1}, Lcom/mediatek/contacts/dialpad/DialerSearchController;->log(Ljava/lang/String;)V

    :cond_0
    :goto_0
    return-void

    :cond_1
    const v19, 0x7f070115

    move-object/from16 v0, p1

    move/from16 v1, v19

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v16

    const v19, 0x7f07002a

    move-object/from16 v0, p1

    move/from16 v1, v19

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v5

    if-eqz v16, :cond_2

    move-object/from16 v0, v16

    instance-of v0, v0, Landroid/widget/RelativeLayout;

    move/from16 v19, v0

    if-eqz v19, :cond_2

    if-eqz v5, :cond_2

    instance-of v0, v5, Landroid/widget/ImageButton;

    move/from16 v19, v0

    if-nez v19, :cond_3

    :cond_2
    const-string v19, "[showLocaleChangeTips] return due to invalid layout."

    move-object/from16 v0, p0

    move-object/from16 v1, v19

    invoke-virtual {v0, v1}, Lcom/mediatek/contacts/dialpad/DialerSearchController;->log(Ljava/lang/String;)V

    goto :goto_0

    :cond_3
    const-class v19, Landroid/widget/RelativeLayout;

    move-object/from16 v0, v19

    move-object/from16 v1, v16

    invoke-virtual {v0, v1}, Ljava/lang/Class;->cast(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v15

    check-cast v15, Landroid/widget/RelativeLayout;

    const-class v19, Landroid/widget/ImageButton;

    move-object/from16 v0, v19

    invoke-virtual {v0, v5}, Ljava/lang/Class;->cast(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Landroid/widget/ImageButton;

    new-instance v19, Ljava/lang/StringBuilder;

    invoke-direct/range {v19 .. v19}, Ljava/lang/StringBuilder;-><init>()V

    const-string v20, "[showLocaleChangeTips]isShowTips:"

    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    move-object/from16 v0, v19

    move/from16 v1, p2

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v19

    const-string v20, "||mLoadTipsContainer is null:"

    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/mediatek/contacts/dialpad/DialerSearchController;->mLoadTipsContainer:Landroid/widget/LinearLayout;

    move-object/from16 v19, v0

    if-nez v19, :cond_4

    const/16 v19, 0x1

    :goto_1
    move-object/from16 v0, v20

    move/from16 v1, v19

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v19

    invoke-virtual/range {v19 .. v19}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v19

    move-object/from16 v0, p0

    move-object/from16 v1, v19

    invoke-virtual {v0, v1}, Lcom/mediatek/contacts/dialpad/DialerSearchController;->log(Ljava/lang/String;)V

    if-eqz p2, :cond_7

    move/from16 v0, p4

    move-object/from16 v1, p0

    iput-boolean v0, v1, Lcom/mediatek/contacts/dialpad/DialerSearchController;->mIsLocaleChanging:Z

    if-nez p4, :cond_5

    const/16 v19, 0x1

    :goto_2
    move/from16 v0, v19

    move-object/from16 v1, p0

    iput-boolean v0, v1, Lcom/mediatek/contacts/dialpad/DialerSearchController;->mIsShowLoadingTip:Z

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/mediatek/contacts/dialpad/DialerSearchController;->mLoadTipsContainer:Landroid/widget/LinearLayout;

    move-object/from16 v19, v0

    if-eqz v19, :cond_6

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/mediatek/contacts/dialpad/DialerSearchController;->mLoadTipsContainer:Landroid/widget/LinearLayout;

    move-object/from16 v19, v0

    const/16 v20, 0x1

    invoke-virtual/range {v19 .. v20}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v17

    check-cast v17, Landroid/widget/TextView;

    if-eqz v17, :cond_0

    move-object/from16 v0, v17

    move-object/from16 v1, p3

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_0

    :cond_4
    const/16 v19, 0x0

    goto :goto_1

    :cond_5
    const/16 v19, 0x0

    goto :goto_2

    :cond_6
    new-instance v6, Landroid/util/DisplayMetrics;

    invoke-direct {v6}, Landroid/util/DisplayMetrics;-><init>()V

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/mediatek/contacts/dialpad/DialerSearchController;->mActivity:Landroid/app/Activity;

    move-object/from16 v19, v0

    invoke-virtual/range {v19 .. v19}, Landroid/app/Activity;->getWindowManager()Landroid/view/WindowManager;

    move-result-object v19

    invoke-interface/range {v19 .. v19}, Landroid/view/WindowManager;->getDefaultDisplay()Landroid/view/Display;

    move-result-object v19

    move-object/from16 v0, v19

    invoke-virtual {v0, v6}, Landroid/view/Display;->getMetrics(Landroid/util/DisplayMetrics;)V

    iget v0, v6, Landroid/util/DisplayMetrics;->widthPixels:I

    move/from16 v18, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/mediatek/contacts/dialpad/DialerSearchController;->mActivity:Landroid/app/Activity;

    move-object/from16 v19, v0

    invoke-virtual/range {v19 .. v19}, Landroid/view/ContextThemeWrapper;->getResources()Landroid/content/res/Resources;

    move-result-object v19

    const v20, 0x7f090072

    invoke-virtual/range {v19 .. v20}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v13

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/mediatek/contacts/dialpad/DialerSearchController;->mActivity:Landroid/app/Activity;

    move-object/from16 v19, v0

    invoke-virtual/range {v19 .. v19}, Landroid/view/ContextThemeWrapper;->getResources()Landroid/content/res/Resources;

    move-result-object v19

    const v20, 0x7f0900a0

    invoke-virtual/range {v19 .. v20}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v11

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/mediatek/contacts/dialpad/DialerSearchController;->mActivity:Landroid/app/Activity;

    move-object/from16 v19, v0

    invoke-virtual/range {v19 .. v19}, Landroid/view/ContextThemeWrapper;->getResources()Landroid/content/res/Resources;

    move-result-object v19

    const v20, 0x7f090033

    invoke-virtual/range {v19 .. v20}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v12

    iget v0, v6, Landroid/util/DisplayMetrics;->density:F

    move/from16 v19, v0

    const/high16 v20, 0x42600000

    mul-float v19, v19, v20

    move/from16 v0, v19

    float-to-int v7, v0

    iget v0, v6, Landroid/util/DisplayMetrics;->heightPixels:I

    move/from16 v19, v0

    sub-int v19, v19, v11

    mul-int/lit8 v20, v13, 0x4

    sub-int v19, v19, v20

    mul-int/lit8 v20, v12, 0x2

    sub-int v19, v19, v20

    sub-int v19, v19, v7

    add-int/lit8 v8, v19, -0x6e

    new-instance v19, Landroid/widget/LinearLayout;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/mediatek/contacts/dialpad/DialerSearchController;->mActivity:Landroid/app/Activity;

    move-object/from16 v20, v0

    invoke-direct/range {v19 .. v20}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;)V

    move-object/from16 v0, v19

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/mediatek/contacts/dialpad/DialerSearchController;->mLoadTipsContainer:Landroid/widget/LinearLayout;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/mediatek/contacts/dialpad/DialerSearchController;->mLoadTipsContainer:Landroid/widget/LinearLayout;

    move-object/from16 v19, v0

    const/16 v20, 0x1

    invoke-virtual/range {v19 .. v20}, Landroid/widget/LinearLayout;->setOrientation(I)V

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/mediatek/contacts/dialpad/DialerSearchController;->mLoadTipsContainer:Landroid/widget/LinearLayout;

    move-object/from16 v19, v0

    const/16 v20, 0x11

    invoke-virtual/range {v19 .. v20}, Landroid/widget/LinearLayout;->setGravity(I)V

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/mediatek/contacts/dialpad/DialerSearchController;->mLoadTipsContainer:Landroid/widget/LinearLayout;

    move-object/from16 v19, v0

    const/16 v20, 0x0

    invoke-virtual/range {v19 .. v20}, Landroid/view/View;->setClickable(Z)V

    new-instance v10, Landroid/widget/ProgressBar;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/mediatek/contacts/dialpad/DialerSearchController;->mActivity:Landroid/app/Activity;

    move-object/from16 v19, v0

    const/16 v20, 0x0

    const v21, 0x101007a

    move-object/from16 v0, v19

    move-object/from16 v1, v20

    move/from16 v2, v21

    invoke-direct {v10, v0, v1, v2}, Landroid/widget/ProgressBar;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    new-instance v19, Landroid/view/ViewGroup$LayoutParams;

    const/16 v20, -0x2

    const/16 v21, -0x2

    invoke-direct/range {v19 .. v21}, Landroid/view/ViewGroup$LayoutParams;-><init>(II)V

    move-object/from16 v0, v19

    invoke-virtual {v10, v0}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/mediatek/contacts/dialpad/DialerSearchController;->mLoadTipsContainer:Landroid/widget/LinearLayout;

    move-object/from16 v19, v0

    move-object/from16 v0, v19

    invoke-virtual {v0, v10}, Landroid/view/ViewGroup;->addView(Landroid/view/View;)V

    new-instance v17, Landroid/widget/TextView;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/mediatek/contacts/dialpad/DialerSearchController;->mActivity:Landroid/app/Activity;

    move-object/from16 v19, v0

    move-object/from16 v0, v17

    move-object/from16 v1, v19

    invoke-direct {v0, v1}, Landroid/widget/TextView;-><init>(Landroid/content/Context;)V

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/mediatek/contacts/dialpad/DialerSearchController;->mActivity:Landroid/app/Activity;

    move-object/from16 v19, v0

    const v20, 0x1030044

    move-object/from16 v0, v17

    move-object/from16 v1, v19

    move/from16 v2, v20

    invoke-virtual {v0, v1, v2}, Landroid/widget/TextView;->setTextAppearance(Landroid/content/Context;I)V

    const/high16 v19, 0x40800000

    iget v0, v6, Landroid/util/DisplayMetrics;->density:F

    move/from16 v20, v0

    mul-float v19, v19, v20

    move/from16 v0, v19

    float-to-int v14, v0

    const/16 v19, 0x0

    move-object/from16 v0, v17

    move/from16 v1, v19

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setSingleLine(Z)V

    const/16 v19, 0xa

    const/16 v20, 0xa

    const/16 v21, 0x0

    move-object/from16 v0, v17

    move/from16 v1, v19

    move/from16 v2, v20

    move/from16 v3, v21

    invoke-virtual {v0, v1, v14, v2, v3}, Landroid/widget/TextView;->setPadding(IIII)V

    const/16 v19, 0x1

    move-object/from16 v0, v17

    move/from16 v1, v19

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setGravity(I)V

    move-object/from16 v0, v17

    move-object/from16 v1, p3

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    new-instance v19, Landroid/view/ViewGroup$LayoutParams;

    const/16 v20, -0x2

    const/16 v21, -0x2

    invoke-direct/range {v19 .. v21}, Landroid/view/ViewGroup$LayoutParams;-><init>(II)V

    move-object/from16 v0, v17

    move-object/from16 v1, v19

    invoke-virtual {v0, v1}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/mediatek/contacts/dialpad/DialerSearchController;->mLoadTipsContainer:Landroid/widget/LinearLayout;

    move-object/from16 v19, v0

    move-object/from16 v0, v19

    move-object/from16 v1, v17

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->addView(Landroid/view/View;)V

    new-instance v9, Landroid/widget/RelativeLayout$LayoutParams;

    move/from16 v0, v18

    invoke-direct {v9, v0, v8}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/mediatek/contacts/dialpad/DialerSearchController;->mLoadTipsContainer:Landroid/widget/LinearLayout;

    move-object/from16 v19, v0

    invoke-virtual {v15}, Landroid/view/ViewGroup;->getChildCount()I

    move-result v20

    move-object/from16 v0, v19

    move/from16 v1, v20

    invoke-virtual {v15, v0, v1, v9}, Landroid/view/ViewGroup;->addView(Landroid/view/View;ILandroid/view/ViewGroup$LayoutParams;)V

    if-eqz p4, :cond_0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/mediatek/contacts/dialpad/DialerSearchController;->mAdapter:Lcom/mediatek/contacts/dialpad/DialerSearchAdapter;

    move-object/from16 v19, v0

    const/16 v20, 0x0

    invoke-virtual/range {v19 .. v20}, Lcom/mediatek/contacts/dialpad/DialerSearchAdapter;->setResultCursor(Landroid/database/Cursor;)V

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/mediatek/contacts/dialpad/DialerSearchController;->mAdapter:Lcom/mediatek/contacts/dialpad/DialerSearchAdapter;

    move-object/from16 v19, v0

    const/16 v20, 0x0

    invoke-virtual/range {v19 .. v20}, Landroid/widget/CursorAdapter;->changeCursor(Landroid/database/Cursor;)V

    invoke-virtual/range {p0 .. p0}, Lcom/mediatek/contacts/dialpad/DialerSearchController;->clearDialerSearchTextWatcher()V

    if-eqz v4, :cond_0

    const/16 v19, 0x0

    move/from16 v0, v19

    invoke-virtual {v4, v0}, Landroid/view/View;->setClickable(Z)V

    goto/16 :goto_0

    :cond_7
    const/16 v19, 0x0

    move/from16 v0, v19

    move-object/from16 v1, p0

    iput-boolean v0, v1, Lcom/mediatek/contacts/dialpad/DialerSearchController;->mIsLocaleChanging:Z

    const/16 v19, 0x0

    move/from16 v0, v19

    move-object/from16 v1, p0

    iput-boolean v0, v1, Lcom/mediatek/contacts/dialpad/DialerSearchController;->mIsShowLoadingTip:Z

    const/16 v19, 0x1

    move/from16 v0, v19

    invoke-virtual {v4, v0}, Landroid/view/View;->setClickable(Z)V

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/mediatek/contacts/dialpad/DialerSearchController;->mLoadTipsContainer:Landroid/widget/LinearLayout;

    move-object/from16 v19, v0

    if-eqz v19, :cond_0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/mediatek/contacts/dialpad/DialerSearchController;->mLoadTipsContainer:Landroid/widget/LinearLayout;

    move-object/from16 v19, v0

    const/16 v20, 0x8

    invoke-virtual/range {v19 .. v20}, Landroid/view/View;->setVisibility(I)V

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/mediatek/contacts/dialpad/DialerSearchController;->mLoadTipsContainer:Landroid/widget/LinearLayout;

    move-object/from16 v19, v0

    move-object/from16 v0, v19

    invoke-virtual {v15, v0}, Landroid/view/ViewGroup;->removeView(Landroid/view/View;)V

    if-eqz p4, :cond_8

    invoke-virtual/range {p0 .. p0}, Lcom/mediatek/contacts/dialpad/DialerSearchController;->hasDialerSearchTextWatcher()Z

    move-result v19

    if-nez v19, :cond_8

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/mediatek/contacts/dialpad/DialerSearchController;->mDigits:Landroid/widget/EditText;

    move-object/from16 v19, v0

    if-eqz v19, :cond_8

    invoke-virtual/range {p0 .. p0}, Lcom/mediatek/contacts/dialpad/DialerSearchController;->setDialerSearchTextWatcher()V

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/mediatek/contacts/dialpad/DialerSearchController;->mDigits:Landroid/widget/EditText;

    move-object/from16 v19, v0

    invoke-virtual/range {v19 .. v19}, Landroid/widget/TextView;->length()I

    move-result v19

    if-lez v19, :cond_9

    const/16 v19, 0x1

    move-object/from16 v0, p0

    move/from16 v1, v19

    invoke-virtual {v0, v1}, Lcom/mediatek/contacts/dialpad/DialerSearchController;->setSearchNumberMode(Z)V

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/mediatek/contacts/dialpad/DialerSearchController;->mDigits:Landroid/widget/EditText;

    move-object/from16 v19, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/mediatek/contacts/dialpad/DialerSearchController;->mDigits:Landroid/widget/EditText;

    move-object/from16 v20, v0

    invoke-virtual/range {v20 .. v20}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v20

    invoke-virtual/range {v19 .. v20}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/mediatek/contacts/dialpad/DialerSearchController;->mDigits:Landroid/widget/EditText;

    move-object/from16 v19, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/mediatek/contacts/dialpad/DialerSearchController;->mDigits:Landroid/widget/EditText;

    move-object/from16 v20, v0

    invoke-virtual/range {v20 .. v20}, Landroid/widget/TextView;->length()I

    move-result v20

    invoke-virtual/range {v19 .. v20}, Landroid/widget/EditText;->setSelection(I)V

    :cond_8
    :goto_3
    const/16 v19, 0x0

    move-object/from16 v0, v19

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/mediatek/contacts/dialpad/DialerSearchController;->mLoadTipsContainer:Landroid/widget/LinearLayout;

    goto/16 :goto_0

    :cond_9
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/mediatek/contacts/dialpad/DialerSearchController;->mDigits:Landroid/widget/EditText;

    move-object/from16 v19, v0

    const/16 v20, 0x0

    invoke-virtual/range {v19 .. v20}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_3
.end method

.method public updateDialerSearch()V
    .locals 0

    invoke-direct {p0}, Lcom/mediatek/contacts/dialpad/DialerSearchController;->forceQueryIfNeeded()V

    return-void
.end method
