.class public Lcom/mediatek/contacts/calllog/CallLogListItemView;
.super Landroid/view/ViewGroup;
.source "CallLogListItemView.java"

# interfaces
.implements Landroid/widget/AbsListView$SelectionBoundsAdjuster;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/mediatek/contacts/calllog/CallLogListItemView$DontPressWithParentImageView;
    }
.end annotation


# static fields
.field private static final ADJUST_NUMBER:F = 0.01f

.field private static final DEFAULT_CALLLOG_SECONDARY_TEXT_COLOR:I = 0x999999

.field private static final DEFAULT_DATE_DIVIDER_HEIGHT:I = 0x3

.field private static final DEFAULT_ITEM_CALL_COUNT_TEXT_SIZE:I = 0xc

.field private static final DEFAULT_ITEM_CALL_TIME_TEXT_SIZE:I = 0xc

.field private static final DEFAULT_ITEM_DATE_BACKGROUND_COLOR:I = 0x55ffffff

.field private static final DEFAULT_ITEM_NAME_TEXT_SIZE:I = 0x12

.field private static final DEFAULT_ITEM_NUMBER_TEXT_SIZE:I = 0xe

.field private static final DEFAULT_ITEM_TEXT_COLOR:I = -0x1

.field private static final FONT_SIZE_EXTRA_LARGE:F = 1.15f

.field private static final FONT_SIZE_LARGE:F = 1.1f

.field private static final NAME_TOP_TO_ADD:I = 0x25

.field private static final NUMBER_TOP_TO_ADD:I = 0x6

.field private static final QUICK_CONTACT_BADGE_STYLE:I = 0x10102af

.field private static final TAG:Ljava/lang/String; = "CallLogListItemView"

.field private static final VERTICAL_DIVIDER_LEN:I = 0x1

.field private static sCallLogInnerMarginDim:I

.field private static sCheckBoxMultiSelWidthDim:I

.field private static sImageViewCallHeightDim:I

.field private static sImageViewCallWidthDim:I

.field private static sListItemPaddingLeftDim:I

.field private static sListItemPaddingRightDim:I

.field private static sListItemQuickContactPaddingBottom:I

.field private static sListItemQuickContactPaddingTop:I

.field private static sNameWidthMaxDim:I

.field private static sSimNameWidthMaxDim:I

.field private static sVerticalDividerHeightDim:I


# instance fields
.field private mActivatedBgDrawable:Landroid/graphics/drawable/Drawable;

.field private mActivatedSupported:Z

.field private mBackground:Landroid/widget/ImageView;

.field private mBoundsWithoutHeader:Landroid/graphics/Rect;

.field private mCallButtonClickListener:Landroid/view/View$OnClickListener;

.field private mCallLogDateBackgroundHeight:I

.field private mCallLogDateVisible:Z

.field private mCallTypeIcon:Lcom/android/contacts/calllog/CallTypeIconsView;

.field private mCallTypeIconHeight:I

.field private mCallTypeIconSimNameMaxHeight:I

.field private mCheckBoxMultiSel:Landroid/widget/CheckBox;

.field private mCheckBoxMultiSelHeight:I

.field protected final mContext:Landroid/content/Context;

.field private mDrawableVertialDivider:Landroid/graphics/drawable/Drawable;

.field private mExtentionIcon:Landroid/widget/ImageView;

.field private final mGapBetweenImageAndText:I

.field private mHorizontalDateDividerColor:I

.field private mHorizontalDateDividerHeight:I

.field private mHorizontalDividerHeight:I

.field private mHorizontalDividerVisible:Z

.field private mImageViewCall:Lcom/mediatek/contacts/calllog/CallLogListItemView$DontPressWithParentImageView;

.field private mPhotoViewHeight:I

.field private mPhotoViewWidth:I

.field private final mPreferredHeight:I

.field private mQuickContactPhoto:Lcom/mediatek/contacts/widget/QuickContactBadgeWithPhoneNumber;

.field private final mSecondaryTextColor:I

.field private mSelectIcon:Landroid/widget/ImageView;

.field private final mSelectableItemBackgroundDrawable:Landroid/graphics/drawable/Drawable;

.field private mSelectionBoundsMarginLeft:I

.field private mSelectionBoundsMarginRight:I

.field private mTagId:I

.field private mTextViewCallCount:Landroid/widget/TextView;

.field private mTextViewCallCountHeight:I

.field private mTextViewCallLogDate:Landroid/widget/TextView;

.field private mTextViewCallLogDateHeight:I

.field private mTextViewCallTime:Landroid/widget/TextView;

.field private mTextViewCallTimeHeight:I

.field private mTextViewName:Landroid/widget/TextView;

.field private mTextViewNameHeight:I

.field private mTextViewNumber:Landroid/widget/TextView;

.field private mTextViewNumberHeight:I

.field private mTextViewSimName:Landroid/widget/TextView;

.field private mTextViewSimNameHeight:I

.field private mVerticalDividerBeVisible:Z

.field private mVerticalDividerWidth:I

.field private mViewHorizontalDateDivider:Landroid/view/View;

.field private mViewHorizontalDateDividerHeight:I

.field private mViewHorizontalDivider:Landroid/view/View;

.field private mViewVertialDivider:Landroid/view/View;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 10
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/util/AttributeSet;

    const/4 v9, 0x2

    const/4 v8, -0x2

    const/4 v7, 0x0

    const/4 v6, 0x1

    invoke-direct {p0, p1, p2}, Landroid/view/ViewGroup;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    const/4 v2, -0x1

    iput v2, p0, Lcom/mediatek/contacts/calllog/CallLogListItemView;->mTagId:I

    iput-boolean v6, p0, Lcom/mediatek/contacts/calllog/CallLogListItemView;->mHorizontalDividerVisible:Z

    iput v6, p0, Lcom/mediatek/contacts/calllog/CallLogListItemView;->mHorizontalDividerHeight:I

    iput v6, p0, Lcom/mediatek/contacts/calllog/CallLogListItemView;->mVerticalDividerWidth:I

    const/4 v2, 0x3

    iput v2, p0, Lcom/mediatek/contacts/calllog/CallLogListItemView;->mHorizontalDateDividerHeight:I

    const v2, 0x55ffffff

    iput v2, p0, Lcom/mediatek/contacts/calllog/CallLogListItemView;->mHorizontalDateDividerColor:I

    new-instance v2, Landroid/graphics/Rect;

    invoke-direct {v2}, Landroid/graphics/Rect;-><init>()V

    iput-object v2, p0, Lcom/mediatek/contacts/calllog/CallLogListItemView;->mBoundsWithoutHeader:Landroid/graphics/Rect;

    iput-object p1, p0, Lcom/mediatek/contacts/calllog/CallLogListItemView;->mContext:Landroid/content/Context;

    const/4 v1, 0x0

    invoke-virtual {p0}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v2

    sget-object v3, Lcom/android/contacts/R$styleable;->ContactListItemView:[I

    invoke-virtual {v2, p2, v3}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[I)Landroid/content/res/TypedArray;

    move-result-object v0

    invoke-virtual {v0, v7, v1}, Landroid/content/res/TypedArray;->getDimensionPixelSize(II)I

    move-result v2

    iput v2, p0, Lcom/mediatek/contacts/calllog/CallLogListItemView;->mPreferredHeight:I

    invoke-virtual {v0, v9}, Landroid/content/res/TypedArray;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v2

    iput-object v2, p0, Lcom/mediatek/contacts/calllog/CallLogListItemView;->mActivatedBgDrawable:Landroid/graphics/drawable/Drawable;

    const/16 v2, 0x9

    invoke-virtual {v0, v2, v1}, Landroid/content/res/TypedArray;->getDimensionPixelOffset(II)I

    move-result v2

    iput v2, p0, Lcom/mediatek/contacts/calllog/CallLogListItemView;->mGapBetweenImageAndText:I

    const/16 v2, 0x8

    invoke-virtual {v0, v2, v1}, Landroid/content/res/TypedArray;->getDimensionPixelOffset(II)I

    move-result v2

    const/4 v3, 0x5

    invoke-virtual {v0, v3, v1}, Landroid/content/res/TypedArray;->getDimensionPixelOffset(II)I

    move-result v3

    const/4 v4, 0x6

    invoke-virtual {v0, v4, v1}, Landroid/content/res/TypedArray;->getDimensionPixelOffset(II)I

    move-result v4

    const/4 v5, 0x7

    invoke-virtual {v0, v5, v1}, Landroid/content/res/TypedArray;->getDimensionPixelOffset(II)I

    move-result v5

    invoke-virtual {p0, v2, v3, v4, v5}, Landroid/view/View;->setPadding(IIII)V

    invoke-virtual {v0}, Landroid/content/res/TypedArray;->recycle()V

    iget-object v2, p0, Lcom/mediatek/contacts/calllog/CallLogListItemView;->mContext:Landroid/content/Context;

    const/4 v3, 0x0

    sget-object v4, Lcom/android/internal/R$styleable;->ViewGroup_Layout:[I

    const v5, 0x10102af

    invoke-virtual {v2, v3, v4, v5, v1}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[III)Landroid/content/res/TypedArray;

    move-result-object v0

    invoke-virtual {v0, v7, v8}, Landroid/content/res/TypedArray;->getLayoutDimension(II)I

    move-result v2

    iput v2, p0, Lcom/mediatek/contacts/calllog/CallLogListItemView;->mPhotoViewWidth:I

    invoke-virtual {v0, v6, v8}, Landroid/content/res/TypedArray;->getLayoutDimension(II)I

    move-result v2

    iput v2, p0, Lcom/mediatek/contacts/calllog/CallLogListItemView;->mPhotoViewHeight:I

    invoke-virtual {v0}, Landroid/content/res/TypedArray;->recycle()V

    iget-object v2, p0, Lcom/mediatek/contacts/calllog/CallLogListItemView;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getTheme()Landroid/content/res/Resources$Theme;

    move-result-object v2

    new-array v3, v6, [I

    const v4, 0x101030e

    aput v4, v3, v7

    invoke-virtual {v2, v3}, Landroid/content/res/Resources$Theme;->obtainStyledAttributes([I)Landroid/content/res/TypedArray;

    move-result-object v0

    invoke-virtual {v0, v7}, Landroid/content/res/TypedArray;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v2

    iput-object v2, p0, Lcom/mediatek/contacts/calllog/CallLogListItemView;->mSelectableItemBackgroundDrawable:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v0}, Landroid/content/res/TypedArray;->recycle()V

    invoke-virtual {p0}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v2

    sget-object v3, Lcom/android/contacts/R$styleable;->CallLog:[I

    invoke-virtual {v2, p2, v3}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[I)Landroid/content/res/TypedArray;

    move-result-object v0

    const v2, 0x999999

    invoke-virtual {v0, v9, v2}, Landroid/content/res/TypedArray;->getInteger(II)I

    move-result v2

    iput v2, p0, Lcom/mediatek/contacts/calllog/CallLogListItemView;->mSecondaryTextColor:I

    invoke-virtual {v0}, Landroid/content/res/TypedArray;->recycle()V

    iget-object v2, p0, Lcom/mediatek/contacts/calllog/CallLogListItemView;->mActivatedBgDrawable:Landroid/graphics/drawable/Drawable;

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/mediatek/contacts/calllog/CallLogListItemView;->mActivatedBgDrawable:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v2, p0}, Landroid/graphics/drawable/Drawable;->setCallback(Landroid/graphics/drawable/Drawable$Callback;)V

    :cond_0
    invoke-direct {p0}, Lcom/mediatek/contacts/calllog/CallLogListItemView;->initPredefinedData()V

    return-void
.end method

.method private ensureVerticalDivider()V
    .locals 2

    iget-object v0, p0, Lcom/mediatek/contacts/calllog/CallLogListItemView;->mDrawableVertialDivider:Landroid/graphics/drawable/Drawable;

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/mediatek/contacts/calllog/CallLogListItemView;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f020084

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    iput-object v0, p0, Lcom/mediatek/contacts/calllog/CallLogListItemView;->mDrawableVertialDivider:Landroid/graphics/drawable/Drawable;

    const/4 v0, 0x1

    iput v0, p0, Lcom/mediatek/contacts/calllog/CallLogListItemView;->mVerticalDividerWidth:I

    :cond_0
    return-void
.end method

.method public static getFontSize()F
    .locals 5

    new-instance v1, Landroid/content/res/Configuration;

    invoke-direct {v1}, Landroid/content/res/Configuration;-><init>()V

    :try_start_0
    invoke-static {}, Landroid/app/ActivityManagerNative;->getDefault()Landroid/app/IActivityManager;

    move-result-object v2

    invoke-interface {v2}, Landroid/app/IActivityManager;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/content/res/Configuration;->updateFrom(Landroid/content/res/Configuration;)I
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    const-string v2, "CallLogListItemView"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "getFontSize(), Font size is "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget v4, v1, Landroid/content/res/Configuration;->fontScale:F

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    iget v2, v1, Landroid/content/res/Configuration;->fontScale:F

    return v2

    :catch_0
    move-exception v0

    const-string v2, "CallLogListItemView"

    const-string v3, "Unable to retrieve font size"

    invoke-static {v2, v3}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method private initPredefinedData()V
    .locals 2

    const/4 v0, 0x0

    sput v0, Lcom/mediatek/contacts/calllog/CallLogListItemView;->sCallLogInnerMarginDim:I

    sput v0, Lcom/mediatek/contacts/calllog/CallLogListItemView;->sImageViewCallWidthDim:I

    sput v0, Lcom/mediatek/contacts/calllog/CallLogListItemView;->sImageViewCallHeightDim:I

    sput v0, Lcom/mediatek/contacts/calllog/CallLogListItemView;->sVerticalDividerHeightDim:I

    sput v0, Lcom/mediatek/contacts/calllog/CallLogListItemView;->sSimNameWidthMaxDim:I

    sput v0, Lcom/mediatek/contacts/calllog/CallLogListItemView;->sListItemQuickContactPaddingTop:I

    sput v0, Lcom/mediatek/contacts/calllog/CallLogListItemView;->sListItemQuickContactPaddingBottom:I

    sput v0, Lcom/mediatek/contacts/calllog/CallLogListItemView;->sCheckBoxMultiSelWidthDim:I

    sput v0, Lcom/mediatek/contacts/calllog/CallLogListItemView;->sListItemPaddingLeftDim:I

    sput v0, Lcom/mediatek/contacts/calllog/CallLogListItemView;->sListItemPaddingRightDim:I

    sput v0, Lcom/mediatek/contacts/calllog/CallLogListItemView;->sNameWidthMaxDim:I

    iget-object v0, p0, Lcom/mediatek/contacts/calllog/CallLogListItemView;->mContext:Landroid/content/Context;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/mediatek/contacts/calllog/CallLogListItemView;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f09004a

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    sput v0, Lcom/mediatek/contacts/calllog/CallLogListItemView;->sCallLogInnerMarginDim:I

    iget-object v0, p0, Lcom/mediatek/contacts/calllog/CallLogListItemView;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f090048

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    sput v0, Lcom/mediatek/contacts/calllog/CallLogListItemView;->sImageViewCallWidthDim:I

    iget-object v0, p0, Lcom/mediatek/contacts/calllog/CallLogListItemView;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f090070

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    sput v0, Lcom/mediatek/contacts/calllog/CallLogListItemView;->sImageViewCallHeightDim:I

    iget-object v0, p0, Lcom/mediatek/contacts/calllog/CallLogListItemView;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f090047

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    sput v0, Lcom/mediatek/contacts/calllog/CallLogListItemView;->sVerticalDividerHeightDim:I

    iget-object v0, p0, Lcom/mediatek/contacts/calllog/CallLogListItemView;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f09007f

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    sput v0, Lcom/mediatek/contacts/calllog/CallLogListItemView;->sSimNameWidthMaxDim:I

    iget-object v0, p0, Lcom/mediatek/contacts/calllog/CallLogListItemView;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f090080

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    sput v0, Lcom/mediatek/contacts/calllog/CallLogListItemView;->sListItemQuickContactPaddingTop:I

    iget-object v0, p0, Lcom/mediatek/contacts/calllog/CallLogListItemView;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f090081

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    sput v0, Lcom/mediatek/contacts/calllog/CallLogListItemView;->sListItemQuickContactPaddingBottom:I

    iget-object v0, p0, Lcom/mediatek/contacts/calllog/CallLogListItemView;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f090082

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    sput v0, Lcom/mediatek/contacts/calllog/CallLogListItemView;->sCheckBoxMultiSelWidthDim:I

    iget-object v0, p0, Lcom/mediatek/contacts/calllog/CallLogListItemView;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f090084

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    sput v0, Lcom/mediatek/contacts/calllog/CallLogListItemView;->sListItemPaddingLeftDim:I

    iget-object v0, p0, Lcom/mediatek/contacts/calllog/CallLogListItemView;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f090085

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    sput v0, Lcom/mediatek/contacts/calllog/CallLogListItemView;->sListItemPaddingRightDim:I

    iget-object v0, p0, Lcom/mediatek/contacts/calllog/CallLogListItemView;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f090086

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    sput v0, Lcom/mediatek/contacts/calllog/CallLogListItemView;->sNameWidthMaxDim:I

    :goto_0
    return-void

    :cond_0
    const-string v0, "CallLogListItemView"

    const-string v1, "Error!!! - initPredefinedData() mContext is null!"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method


# virtual methods
.method public adjustListItemSelectionBounds(Landroid/graphics/Rect;)V
    .locals 2
    .param p1    # Landroid/graphics/Rect;

    iget v0, p1, Landroid/graphics/Rect;->top:I

    iget-object v1, p0, Lcom/mediatek/contacts/calllog/CallLogListItemView;->mBoundsWithoutHeader:Landroid/graphics/Rect;

    iget v1, v1, Landroid/graphics/Rect;->top:I

    add-int/2addr v0, v1

    iput v0, p1, Landroid/graphics/Rect;->top:I

    iget v0, p1, Landroid/graphics/Rect;->top:I

    iget-object v1, p0, Lcom/mediatek/contacts/calllog/CallLogListItemView;->mBoundsWithoutHeader:Landroid/graphics/Rect;

    invoke-virtual {v1}, Landroid/graphics/Rect;->height()I

    move-result v1

    add-int/2addr v0, v1

    iput v0, p1, Landroid/graphics/Rect;->bottom:I

    iget v0, p1, Landroid/graphics/Rect;->left:I

    iget v1, p0, Lcom/mediatek/contacts/calllog/CallLogListItemView;->mSelectionBoundsMarginLeft:I

    add-int/2addr v0, v1

    iput v0, p1, Landroid/graphics/Rect;->left:I

    iget v0, p1, Landroid/graphics/Rect;->right:I

    iget v1, p0, Lcom/mediatek/contacts/calllog/CallLogListItemView;->mSelectionBoundsMarginRight:I

    sub-int/2addr v0, v1

    iput v0, p1, Landroid/graphics/Rect;->right:I

    return-void
.end method

.method public dispatchDraw(Landroid/graphics/Canvas;)V
    .locals 1
    .param p1    # Landroid/graphics/Canvas;

    iget-boolean v0, p0, Lcom/mediatek/contacts/calllog/CallLogListItemView;->mActivatedSupported:Z

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Landroid/view/View;->isActivated()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/mediatek/contacts/calllog/CallLogListItemView;->mActivatedBgDrawable:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v0, p1}, Landroid/graphics/drawable/Drawable;->draw(Landroid/graphics/Canvas;)V

    :cond_0
    iget-boolean v0, p0, Lcom/mediatek/contacts/calllog/CallLogListItemView;->mVerticalDividerBeVisible:Z

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/mediatek/contacts/calllog/CallLogListItemView;->mDrawableVertialDivider:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v0, p1}, Landroid/graphics/drawable/Drawable;->draw(Landroid/graphics/Canvas;)V

    :cond_1
    invoke-super {p0, p1}, Landroid/view/ViewGroup;->dispatchDraw(Landroid/graphics/Canvas;)V

    return-void
.end method

.method protected drawableStateChanged()V
    .locals 2

    invoke-super {p0}, Landroid/view/ViewGroup;->drawableStateChanged()V

    iget-boolean v0, p0, Lcom/mediatek/contacts/calllog/CallLogListItemView;->mActivatedSupported:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/mediatek/contacts/calllog/CallLogListItemView;->mActivatedBgDrawable:Landroid/graphics/drawable/Drawable;

    invoke-virtual {p0}, Landroid/view/View;->getDrawableState()[I

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/graphics/drawable/Drawable;->setState([I)Z

    :cond_0
    return-void
.end method

.method public getBackgroundView()Landroid/widget/ImageView;
    .locals 2

    iget-object v0, p0, Lcom/mediatek/contacts/calllog/CallLogListItemView;->mBackground:Landroid/widget/ImageView;

    if-nez v0, :cond_0

    new-instance v0, Landroid/widget/ImageView;

    iget-object v1, p0, Lcom/mediatek/contacts/calllog/CallLogListItemView;->mContext:Landroid/content/Context;

    invoke-direct {v0, v1}, Landroid/widget/ImageView;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/mediatek/contacts/calllog/CallLogListItemView;->mBackground:Landroid/widget/ImageView;

    iget-object v0, p0, Lcom/mediatek/contacts/calllog/CallLogListItemView;->mBackground:Landroid/widget/ImageView;

    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, Landroid/view/ViewGroup;->addView(Landroid/view/View;I)V

    :cond_0
    iget-object v0, p0, Lcom/mediatek/contacts/calllog/CallLogListItemView;->mBackground:Landroid/widget/ImageView;

    return-object v0
.end method

.method public getCallButton()Landroid/widget/ImageView;
    .locals 6

    const/4 v5, 0x0

    iget-object v0, p0, Lcom/mediatek/contacts/calllog/CallLogListItemView;->mImageViewCall:Lcom/mediatek/contacts/calllog/CallLogListItemView$DontPressWithParentImageView;

    if-nez v0, :cond_1

    new-instance v0, Lcom/mediatek/contacts/calllog/CallLogListItemView$DontPressWithParentImageView;

    iget-object v1, p0, Lcom/mediatek/contacts/calllog/CallLogListItemView;->mContext:Landroid/content/Context;

    const/4 v2, 0x0

    invoke-direct {v0, v1, v2}, Lcom/mediatek/contacts/calllog/CallLogListItemView$DontPressWithParentImageView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    iput-object v0, p0, Lcom/mediatek/contacts/calllog/CallLogListItemView;->mImageViewCall:Lcom/mediatek/contacts/calllog/CallLogListItemView$DontPressWithParentImageView;

    iget-object v0, p0, Lcom/mediatek/contacts/calllog/CallLogListItemView;->mImageViewCall:Lcom/mediatek/contacts/calllog/CallLogListItemView$DontPressWithParentImageView;

    iget-object v1, p0, Lcom/mediatek/contacts/calllog/CallLogListItemView;->mCallButtonClickListener:Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v0, p0, Lcom/mediatek/contacts/calllog/CallLogListItemView;->mImageViewCall:Lcom/mediatek/contacts/calllog/CallLogListItemView$DontPressWithParentImageView;

    iget-object v1, p0, Lcom/mediatek/contacts/calllog/CallLogListItemView;->mSelectableItemBackgroundDrawable:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v0, v1}, Landroid/view/View;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    iget-object v0, p0, Lcom/mediatek/contacts/calllog/CallLogListItemView;->mImageViewCall:Lcom/mediatek/contacts/calllog/CallLogListItemView$DontPressWithParentImageView;

    sget v1, Lcom/mediatek/contacts/calllog/CallLogListItemView;->sCallLogInnerMarginDim:I

    sget v2, Lcom/mediatek/contacts/calllog/CallLogListItemView;->sCallLogInnerMarginDim:I

    sget v3, Lcom/mediatek/contacts/calllog/CallLogListItemView;->sCallLogInnerMarginDim:I

    sget v4, Lcom/mediatek/contacts/calllog/CallLogListItemView;->sCallLogInnerMarginDim:I

    invoke-virtual {v0, v1, v2, v3, v4}, Landroid/view/View;->setPadding(IIII)V

    iget-object v0, p0, Lcom/mediatek/contacts/calllog/CallLogListItemView;->mImageViewCall:Lcom/mediatek/contacts/calllog/CallLogListItemView$DontPressWithParentImageView;

    sget-object v1, Landroid/widget/ImageView$ScaleType;->CENTER:Landroid/widget/ImageView$ScaleType;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setScaleType(Landroid/widget/ImageView$ScaleType;)V

    iget-object v0, p0, Lcom/mediatek/contacts/calllog/CallLogListItemView;->mImageViewCall:Lcom/mediatek/contacts/calllog/CallLogListItemView$DontPressWithParentImageView;

    invoke-virtual {p0, v0}, Landroid/view/ViewGroup;->addView(Landroid/view/View;)V

    iget-object v0, p0, Lcom/mediatek/contacts/calllog/CallLogListItemView;->mViewVertialDivider:Landroid/view/View;

    if-nez v0, :cond_0

    new-instance v0, Landroid/view/View;

    iget-object v1, p0, Lcom/mediatek/contacts/calllog/CallLogListItemView;->mContext:Landroid/content/Context;

    invoke-direct {v0, v1}, Landroid/view/View;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/mediatek/contacts/calllog/CallLogListItemView;->mViewVertialDivider:Landroid/view/View;

    iget-object v0, p0, Lcom/mediatek/contacts/calllog/CallLogListItemView;->mViewVertialDivider:Landroid/view/View;

    const v1, 0x7f020084

    invoke-virtual {v0, v1}, Landroid/view/View;->setBackgroundResource(I)V

    iget-object v0, p0, Lcom/mediatek/contacts/calllog/CallLogListItemView;->mViewVertialDivider:Landroid/view/View;

    invoke-virtual {p0, v0}, Landroid/view/ViewGroup;->addView(Landroid/view/View;)V

    :cond_0
    iget-object v0, p0, Lcom/mediatek/contacts/calllog/CallLogListItemView;->mImageViewCall:Lcom/mediatek/contacts/calllog/CallLogListItemView$DontPressWithParentImageView;

    invoke-virtual {v0, v5}, Landroid/widget/ImageView;->setVisibility(I)V

    iget-object v0, p0, Lcom/mediatek/contacts/calllog/CallLogListItemView;->mViewVertialDivider:Landroid/view/View;

    invoke-virtual {v0, v5}, Landroid/view/View;->setVisibility(I)V

    :cond_1
    iget-object v0, p0, Lcom/mediatek/contacts/calllog/CallLogListItemView;->mImageViewCall:Lcom/mediatek/contacts/calllog/CallLogListItemView$DontPressWithParentImageView;

    return-object v0
.end method

.method public getCallCountTextView()Landroid/widget/TextView;
    .locals 2

    iget-object v0, p0, Lcom/mediatek/contacts/calllog/CallLogListItemView;->mTextViewCallCount:Landroid/widget/TextView;

    if-nez v0, :cond_0

    new-instance v0, Landroid/widget/TextView;

    iget-object v1, p0, Lcom/mediatek/contacts/calllog/CallLogListItemView;->mContext:Landroid/content/Context;

    invoke-direct {v0, v1}, Landroid/widget/TextView;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/mediatek/contacts/calllog/CallLogListItemView;->mTextViewCallCount:Landroid/widget/TextView;

    iget-object v0, p0, Lcom/mediatek/contacts/calllog/CallLogListItemView;->mTextViewCallCount:Landroid/widget/TextView;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setSingleLine(Z)V

    iget-object v0, p0, Lcom/mediatek/contacts/calllog/CallLogListItemView;->mTextViewCallCount:Landroid/widget/TextView;

    const/high16 v1, 0x41400000

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTextSize(F)V

    iget-object v0, p0, Lcom/mediatek/contacts/calllog/CallLogListItemView;->mTextViewCallCount:Landroid/widget/TextView;

    iget v1, p0, Lcom/mediatek/contacts/calllog/CallLogListItemView;->mSecondaryTextColor:I

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTextColor(I)V

    iget-object v0, p0, Lcom/mediatek/contacts/calllog/CallLogListItemView;->mTextViewCallCount:Landroid/widget/TextView;

    const/16 v1, 0x10

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setGravity(I)V

    iget-object v0, p0, Lcom/mediatek/contacts/calllog/CallLogListItemView;->mTextViewCallCount:Landroid/widget/TextView;

    invoke-virtual {p0}, Landroid/view/View;->isActivated()Z

    move-result v1

    invoke-virtual {v0, v1}, Landroid/view/View;->setActivated(Z)V

    iget-object v0, p0, Lcom/mediatek/contacts/calllog/CallLogListItemView;->mTextViewCallCount:Landroid/widget/TextView;

    const-string v1, "100000"

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lcom/mediatek/contacts/calllog/CallLogListItemView;->mTextViewCallCount:Landroid/widget/TextView;

    invoke-virtual {p0, v0}, Landroid/view/ViewGroup;->addView(Landroid/view/View;)V

    :cond_0
    iget-object v0, p0, Lcom/mediatek/contacts/calllog/CallLogListItemView;->mTextViewCallCount:Landroid/widget/TextView;

    return-object v0
.end method

.method public getCallLogNameTextView()Landroid/widget/TextView;
    .locals 2

    iget-object v0, p0, Lcom/mediatek/contacts/calllog/CallLogListItemView;->mTextViewName:Landroid/widget/TextView;

    if-nez v0, :cond_0

    new-instance v0, Landroid/widget/TextView;

    iget-object v1, p0, Lcom/mediatek/contacts/calllog/CallLogListItemView;->mContext:Landroid/content/Context;

    invoke-direct {v0, v1}, Landroid/widget/TextView;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/mediatek/contacts/calllog/CallLogListItemView;->mTextViewName:Landroid/widget/TextView;

    iget-object v0, p0, Lcom/mediatek/contacts/calllog/CallLogListItemView;->mTextViewName:Landroid/widget/TextView;

    const/4 v1, 0x2

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setMaxLines(I)V

    iget-object v0, p0, Lcom/mediatek/contacts/calllog/CallLogListItemView;->mTextViewName:Landroid/widget/TextView;

    const/high16 v1, 0x41900000

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTextSize(F)V

    iget-object v0, p0, Lcom/mediatek/contacts/calllog/CallLogListItemView;->mTextViewName:Landroid/widget/TextView;

    const/4 v1, -0x1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTextColor(I)V

    iget-object v0, p0, Lcom/mediatek/contacts/calllog/CallLogListItemView;->mTextViewName:Landroid/widget/TextView;

    sget-object v1, Landroid/text/TextUtils$TruncateAt;->END:Landroid/text/TextUtils$TruncateAt;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setEllipsize(Landroid/text/TextUtils$TruncateAt;)V

    iget-object v0, p0, Lcom/mediatek/contacts/calllog/CallLogListItemView;->mTextViewName:Landroid/widget/TextView;

    sget v1, Lcom/mediatek/contacts/calllog/CallLogListItemView;->sNameWidthMaxDim:I

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setMaxWidth(I)V

    iget-object v0, p0, Lcom/mediatek/contacts/calllog/CallLogListItemView;->mTextViewName:Landroid/widget/TextView;

    invoke-virtual {p0}, Landroid/view/View;->isActivated()Z

    move-result v1

    invoke-virtual {v0, v1}, Landroid/view/View;->setActivated(Z)V

    iget-object v0, p0, Lcom/mediatek/contacts/calllog/CallLogListItemView;->mTextViewName:Landroid/widget/TextView;

    const/16 v1, 0x10

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setGravity(I)V

    iget-object v0, p0, Lcom/mediatek/contacts/calllog/CallLogListItemView;->mTextViewName:Landroid/widget/TextView;

    invoke-virtual {p0, v0}, Landroid/view/ViewGroup;->addView(Landroid/view/View;)V

    :cond_0
    iget-object v0, p0, Lcom/mediatek/contacts/calllog/CallLogListItemView;->mTextViewName:Landroid/widget/TextView;

    return-object v0
.end method

.method public getCallTimeTextView()Landroid/widget/TextView;
    .locals 2

    iget-object v0, p0, Lcom/mediatek/contacts/calllog/CallLogListItemView;->mTextViewCallTime:Landroid/widget/TextView;

    if-nez v0, :cond_0

    new-instance v0, Landroid/widget/TextView;

    iget-object v1, p0, Lcom/mediatek/contacts/calllog/CallLogListItemView;->mContext:Landroid/content/Context;

    invoke-direct {v0, v1}, Landroid/widget/TextView;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/mediatek/contacts/calllog/CallLogListItemView;->mTextViewCallTime:Landroid/widget/TextView;

    iget-object v0, p0, Lcom/mediatek/contacts/calllog/CallLogListItemView;->mTextViewCallTime:Landroid/widget/TextView;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setSingleLine(Z)V

    iget-object v0, p0, Lcom/mediatek/contacts/calllog/CallLogListItemView;->mTextViewCallTime:Landroid/widget/TextView;

    const/high16 v1, 0x41400000

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTextSize(F)V

    iget-object v0, p0, Lcom/mediatek/contacts/calllog/CallLogListItemView;->mTextViewCallTime:Landroid/widget/TextView;

    iget v1, p0, Lcom/mediatek/contacts/calllog/CallLogListItemView;->mSecondaryTextColor:I

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTextColor(I)V

    iget-object v0, p0, Lcom/mediatek/contacts/calllog/CallLogListItemView;->mTextViewCallTime:Landroid/widget/TextView;

    const/16 v1, 0x10

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setGravity(I)V

    iget-object v0, p0, Lcom/mediatek/contacts/calllog/CallLogListItemView;->mTextViewCallTime:Landroid/widget/TextView;

    invoke-virtual {p0}, Landroid/view/View;->isActivated()Z

    move-result v1

    invoke-virtual {v0, v1}, Landroid/view/View;->setActivated(Z)V

    iget-object v0, p0, Lcom/mediatek/contacts/calllog/CallLogListItemView;->mTextViewCallTime:Landroid/widget/TextView;

    const-string v1, "9:00 pm"

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lcom/mediatek/contacts/calllog/CallLogListItemView;->mTextViewCallTime:Landroid/widget/TextView;

    invoke-virtual {p0, v0}, Landroid/view/ViewGroup;->addView(Landroid/view/View;)V

    :cond_0
    iget-object v0, p0, Lcom/mediatek/contacts/calllog/CallLogListItemView;->mTextViewCallTime:Landroid/widget/TextView;

    return-object v0
.end method

.method public getCallTypeIconView()Lcom/android/contacts/calllog/CallTypeIconsView;
    .locals 2

    iget-object v0, p0, Lcom/mediatek/contacts/calllog/CallLogListItemView;->mCallTypeIcon:Lcom/android/contacts/calllog/CallTypeIconsView;

    if-nez v0, :cond_0

    new-instance v0, Lcom/android/contacts/calllog/CallTypeIconsView;

    iget-object v1, p0, Lcom/mediatek/contacts/calllog/CallLogListItemView;->mContext:Landroid/content/Context;

    invoke-direct {v0, v1}, Lcom/android/contacts/calllog/CallTypeIconsView;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/mediatek/contacts/calllog/CallLogListItemView;->mCallTypeIcon:Lcom/android/contacts/calllog/CallTypeIconsView;

    iget-object v0, p0, Lcom/mediatek/contacts/calllog/CallLogListItemView;->mCallTypeIcon:Lcom/android/contacts/calllog/CallTypeIconsView;

    invoke-virtual {p0}, Landroid/view/View;->isActivated()Z

    move-result v1

    invoke-virtual {v0, v1}, Landroid/view/View;->setActivated(Z)V

    iget-object v0, p0, Lcom/mediatek/contacts/calllog/CallLogListItemView;->mCallTypeIcon:Lcom/android/contacts/calllog/CallTypeIconsView;

    invoke-virtual {p0, v0}, Landroid/view/ViewGroup;->addView(Landroid/view/View;)V

    :cond_0
    iget-object v0, p0, Lcom/mediatek/contacts/calllog/CallLogListItemView;->mCallTypeIcon:Lcom/android/contacts/calllog/CallTypeIconsView;

    return-object v0
.end method

.method public getCheckBoxMultiSel()Landroid/widget/CheckBox;
    .locals 2

    iget-object v0, p0, Lcom/mediatek/contacts/calllog/CallLogListItemView;->mCheckBoxMultiSel:Landroid/widget/CheckBox;

    if-nez v0, :cond_0

    new-instance v0, Landroid/widget/CheckBox;

    iget-object v1, p0, Lcom/mediatek/contacts/calllog/CallLogListItemView;->mContext:Landroid/content/Context;

    invoke-direct {v0, v1}, Landroid/widget/CheckBox;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/mediatek/contacts/calllog/CallLogListItemView;->mCheckBoxMultiSel:Landroid/widget/CheckBox;

    iget-object v0, p0, Lcom/mediatek/contacts/calllog/CallLogListItemView;->mCheckBoxMultiSel:Landroid/widget/CheckBox;

    invoke-virtual {p0}, Landroid/view/View;->isActivated()Z

    move-result v1

    invoke-virtual {v0, v1}, Landroid/view/View;->setActivated(Z)V

    iget-object v0, p0, Lcom/mediatek/contacts/calllog/CallLogListItemView;->mCheckBoxMultiSel:Landroid/widget/CheckBox;

    invoke-virtual {p0, v0}, Landroid/view/ViewGroup;->addView(Landroid/view/View;)V

    :cond_0
    iget-object v0, p0, Lcom/mediatek/contacts/calllog/CallLogListItemView;->mCheckBoxMultiSel:Landroid/widget/CheckBox;

    return-object v0
.end method

.method public getExtentionIconView()V
    .locals 3

    iget-object v0, p0, Lcom/mediatek/contacts/calllog/CallLogListItemView;->mExtentionIcon:Landroid/widget/ImageView;

    if-nez v0, :cond_0

    new-instance v0, Landroid/widget/ImageView;

    iget-object v1, p0, Lcom/mediatek/contacts/calllog/CallLogListItemView;->mContext:Landroid/content/Context;

    invoke-direct {v0, v1}, Landroid/widget/ImageView;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/mediatek/contacts/calllog/CallLogListItemView;->mExtentionIcon:Landroid/widget/ImageView;

    :cond_0
    iget-object v0, p0, Lcom/mediatek/contacts/calllog/CallLogListItemView;->mExtentionIcon:Landroid/widget/ImageView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    invoke-static {}, Lcom/mediatek/contacts/ExtensionManager;->getInstance()Lcom/mediatek/contacts/ExtensionManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/mediatek/contacts/ExtensionManager;->getCallListExtension()Lcom/android/contacts/ext/CallListExtension;

    move-result-object v0

    iget-object v1, p0, Lcom/mediatek/contacts/calllog/CallLogListItemView;->mExtentionIcon:Landroid/widget/ImageView;

    const-string v2, "ExtenstionForRCS"

    invoke-virtual {v0, v1, v2}, Lcom/android/contacts/ext/CallListExtension;->setExtentionImageView(Landroid/widget/ImageView;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/mediatek/contacts/calllog/CallLogListItemView;->mExtentionIcon:Landroid/widget/ImageView;

    invoke-virtual {p0, v0}, Landroid/view/ViewGroup;->addView(Landroid/view/View;)V

    return-void
.end method

.method public getHorizontalDivider()Landroid/view/View;
    .locals 2

    iget-object v0, p0, Lcom/mediatek/contacts/calllog/CallLogListItemView;->mViewHorizontalDivider:Landroid/view/View;

    if-nez v0, :cond_0

    new-instance v0, Landroid/view/View;

    iget-object v1, p0, Lcom/mediatek/contacts/calllog/CallLogListItemView;->mContext:Landroid/content/Context;

    invoke-direct {v0, v1}, Landroid/view/View;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/mediatek/contacts/calllog/CallLogListItemView;->mViewHorizontalDivider:Landroid/view/View;

    iget-object v0, p0, Lcom/mediatek/contacts/calllog/CallLogListItemView;->mViewHorizontalDivider:Landroid/view/View;

    iget v1, p0, Lcom/mediatek/contacts/calllog/CallLogListItemView;->mHorizontalDateDividerColor:I

    invoke-virtual {v0, v1}, Landroid/view/View;->setBackgroundColor(I)V

    iget-object v0, p0, Lcom/mediatek/contacts/calllog/CallLogListItemView;->mViewHorizontalDivider:Landroid/view/View;

    invoke-virtual {p0, v0}, Landroid/view/ViewGroup;->addView(Landroid/view/View;)V

    :cond_0
    iget-object v0, p0, Lcom/mediatek/contacts/calllog/CallLogListItemView;->mViewHorizontalDivider:Landroid/view/View;

    return-object v0
.end method

.method public getNumberTextView()Landroid/widget/TextView;
    .locals 2

    iget-object v0, p0, Lcom/mediatek/contacts/calllog/CallLogListItemView;->mTextViewNumber:Landroid/widget/TextView;

    if-nez v0, :cond_0

    new-instance v0, Landroid/widget/TextView;

    iget-object v1, p0, Lcom/mediatek/contacts/calllog/CallLogListItemView;->mContext:Landroid/content/Context;

    invoke-direct {v0, v1}, Landroid/widget/TextView;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/mediatek/contacts/calllog/CallLogListItemView;->mTextViewNumber:Landroid/widget/TextView;

    iget-object v0, p0, Lcom/mediatek/contacts/calllog/CallLogListItemView;->mTextViewNumber:Landroid/widget/TextView;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setSingleLine(Z)V

    iget-object v0, p0, Lcom/mediatek/contacts/calllog/CallLogListItemView;->mTextViewNumber:Landroid/widget/TextView;

    const/high16 v1, 0x41600000

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTextSize(F)V

    iget-object v0, p0, Lcom/mediatek/contacts/calllog/CallLogListItemView;->mTextViewNumber:Landroid/widget/TextView;

    iget v1, p0, Lcom/mediatek/contacts/calllog/CallLogListItemView;->mSecondaryTextColor:I

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTextColor(I)V

    iget-object v0, p0, Lcom/mediatek/contacts/calllog/CallLogListItemView;->mTextViewNumber:Landroid/widget/TextView;

    sget-object v1, Landroid/text/TextUtils$TruncateAt;->END:Landroid/text/TextUtils$TruncateAt;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setEllipsize(Landroid/text/TextUtils$TruncateAt;)V

    iget-object v0, p0, Lcom/mediatek/contacts/calllog/CallLogListItemView;->mTextViewNumber:Landroid/widget/TextView;

    sget v1, Lcom/mediatek/contacts/calllog/CallLogListItemView;->sNameWidthMaxDim:I

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setMaxWidth(I)V

    iget-object v0, p0, Lcom/mediatek/contacts/calllog/CallLogListItemView;->mTextViewNumber:Landroid/widget/TextView;

    invoke-virtual {p0}, Landroid/view/View;->isActivated()Z

    move-result v1

    invoke-virtual {v0, v1}, Landroid/view/View;->setActivated(Z)V

    iget-object v0, p0, Lcom/mediatek/contacts/calllog/CallLogListItemView;->mTextViewNumber:Landroid/widget/TextView;

    invoke-virtual {p0, v0}, Landroid/view/ViewGroup;->addView(Landroid/view/View;)V

    :cond_0
    iget-object v0, p0, Lcom/mediatek/contacts/calllog/CallLogListItemView;->mTextViewNumber:Landroid/widget/TextView;

    return-object v0
.end method

.method public getQuickContact()Lcom/mediatek/contacts/widget/QuickContactBadgeWithPhoneNumber;
    .locals 4

    iget-object v0, p0, Lcom/mediatek/contacts/calllog/CallLogListItemView;->mQuickContactPhoto:Lcom/mediatek/contacts/widget/QuickContactBadgeWithPhoneNumber;

    if-nez v0, :cond_0

    new-instance v0, Lcom/mediatek/contacts/widget/QuickContactBadgeWithPhoneNumber;

    iget-object v1, p0, Lcom/mediatek/contacts/calllog/CallLogListItemView;->mContext:Landroid/content/Context;

    const/4 v2, 0x0

    const v3, 0x10102af

    invoke-direct {v0, v1, v2, v3}, Lcom/mediatek/contacts/widget/QuickContactBadgeWithPhoneNumber;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    iput-object v0, p0, Lcom/mediatek/contacts/calllog/CallLogListItemView;->mQuickContactPhoto:Lcom/mediatek/contacts/widget/QuickContactBadgeWithPhoneNumber;

    iget-object v0, p0, Lcom/mediatek/contacts/calllog/CallLogListItemView;->mQuickContactPhoto:Lcom/mediatek/contacts/widget/QuickContactBadgeWithPhoneNumber;

    invoke-virtual {p0, v0}, Landroid/view/ViewGroup;->addView(Landroid/view/View;)V

    :cond_0
    iget-object v0, p0, Lcom/mediatek/contacts/calllog/CallLogListItemView;->mQuickContactPhoto:Lcom/mediatek/contacts/widget/QuickContactBadgeWithPhoneNumber;

    return-object v0
.end method

.method public getSectionDate()Landroid/widget/TextView;
    .locals 4

    const/4 v3, 0x1

    iget-object v0, p0, Lcom/mediatek/contacts/calllog/CallLogListItemView;->mTextViewCallLogDate:Landroid/widget/TextView;

    if-nez v0, :cond_0

    new-instance v0, Landroid/widget/TextView;

    iget-object v1, p0, Lcom/mediatek/contacts/calllog/CallLogListItemView;->mContext:Landroid/content/Context;

    invoke-direct {v0, v1}, Landroid/widget/TextView;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/mediatek/contacts/calllog/CallLogListItemView;->mTextViewCallLogDate:Landroid/widget/TextView;

    iget-object v0, p0, Lcom/mediatek/contacts/calllog/CallLogListItemView;->mTextViewCallLogDate:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/mediatek/contacts/calllog/CallLogListItemView;->mContext:Landroid/content/Context;

    const v2, 0x1030046

    invoke-virtual {v0, v1, v2}, Landroid/widget/TextView;->setTextAppearance(Landroid/content/Context;I)V

    iget-object v0, p0, Lcom/mediatek/contacts/calllog/CallLogListItemView;->mTextViewCallLogDate:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/mediatek/contacts/calllog/CallLogListItemView;->mTextViewCallLogDate:Landroid/widget/TextView;

    invoke-virtual {v1}, Landroid/widget/TextView;->getTypeface()Landroid/graphics/Typeface;

    move-result-object v1

    invoke-virtual {v0, v1, v3}, Landroid/widget/TextView;->setTypeface(Landroid/graphics/Typeface;I)V

    iget-object v0, p0, Lcom/mediatek/contacts/calllog/CallLogListItemView;->mTextViewCallLogDate:Landroid/widget/TextView;

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setSingleLine(Z)V

    iget-object v0, p0, Lcom/mediatek/contacts/calllog/CallLogListItemView;->mTextViewCallLogDate:Landroid/widget/TextView;

    const/16 v1, 0x10

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setGravity(I)V

    iget-object v0, p0, Lcom/mediatek/contacts/calllog/CallLogListItemView;->mTextViewCallLogDate:Landroid/widget/TextView;

    invoke-virtual {p0, v0}, Landroid/view/ViewGroup;->addView(Landroid/view/View;)V

    :cond_0
    iget-object v0, p0, Lcom/mediatek/contacts/calllog/CallLogListItemView;->mViewHorizontalDateDivider:Landroid/view/View;

    if-nez v0, :cond_1

    new-instance v0, Landroid/view/View;

    iget-object v1, p0, Lcom/mediatek/contacts/calllog/CallLogListItemView;->mContext:Landroid/content/Context;

    invoke-direct {v0, v1}, Landroid/view/View;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/mediatek/contacts/calllog/CallLogListItemView;->mViewHorizontalDateDivider:Landroid/view/View;

    iget-object v0, p0, Lcom/mediatek/contacts/calllog/CallLogListItemView;->mViewHorizontalDateDivider:Landroid/view/View;

    iget v1, p0, Lcom/mediatek/contacts/calllog/CallLogListItemView;->mHorizontalDateDividerColor:I

    invoke-virtual {v0, v1}, Landroid/view/View;->setBackgroundColor(I)V

    iget-object v0, p0, Lcom/mediatek/contacts/calllog/CallLogListItemView;->mViewHorizontalDateDivider:Landroid/view/View;

    invoke-virtual {p0, v0}, Landroid/view/ViewGroup;->addView(Landroid/view/View;)V

    :cond_1
    iget-object v0, p0, Lcom/mediatek/contacts/calllog/CallLogListItemView;->mTextViewCallLogDate:Landroid/widget/TextView;

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setAllCaps(Z)V

    iput-boolean v3, p0, Lcom/mediatek/contacts/calllog/CallLogListItemView;->mCallLogDateVisible:Z

    iget-object v0, p0, Lcom/mediatek/contacts/calllog/CallLogListItemView;->mTextViewCallLogDate:Landroid/widget/TextView;

    return-object v0
.end method

.method public getSelectImageView()Landroid/widget/ImageView;
    .locals 2

    iget-object v0, p0, Lcom/mediatek/contacts/calllog/CallLogListItemView;->mSelectIcon:Landroid/widget/ImageView;

    if-nez v0, :cond_0

    new-instance v0, Landroid/widget/ImageView;

    iget-object v1, p0, Lcom/mediatek/contacts/calllog/CallLogListItemView;->mContext:Landroid/content/Context;

    invoke-direct {v0, v1}, Landroid/widget/ImageView;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/mediatek/contacts/calllog/CallLogListItemView;->mSelectIcon:Landroid/widget/ImageView;

    iget-object v0, p0, Lcom/mediatek/contacts/calllog/CallLogListItemView;->mSelectIcon:Landroid/widget/ImageView;

    const v1, 0x7f0200c6

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    iget-object v0, p0, Lcom/mediatek/contacts/calllog/CallLogListItemView;->mSelectIcon:Landroid/widget/ImageView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    iget-object v0, p0, Lcom/mediatek/contacts/calllog/CallLogListItemView;->mSelectIcon:Landroid/widget/ImageView;

    invoke-virtual {p0, v0}, Landroid/view/ViewGroup;->addView(Landroid/view/View;)V

    :cond_0
    iget-object v0, p0, Lcom/mediatek/contacts/calllog/CallLogListItemView;->mSelectIcon:Landroid/widget/ImageView;

    return-object v0
.end method

.method public getSimNameTextView()Landroid/widget/TextView;
    .locals 4

    iget-object v0, p0, Lcom/mediatek/contacts/calllog/CallLogListItemView;->mTextViewSimName:Landroid/widget/TextView;

    if-nez v0, :cond_0

    new-instance v0, Landroid/widget/TextView;

    iget-object v1, p0, Lcom/mediatek/contacts/calllog/CallLogListItemView;->mContext:Landroid/content/Context;

    invoke-direct {v0, v1}, Landroid/widget/TextView;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/mediatek/contacts/calllog/CallLogListItemView;->mTextViewSimName:Landroid/widget/TextView;

    iget-object v0, p0, Lcom/mediatek/contacts/calllog/CallLogListItemView;->mTextViewSimName:Landroid/widget/TextView;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setSingleLine(Z)V

    iget-object v0, p0, Lcom/mediatek/contacts/calllog/CallLogListItemView;->mTextViewSimName:Landroid/widget/TextView;

    const/4 v1, 0x0

    iget-object v2, p0, Lcom/mediatek/contacts/calllog/CallLogListItemView;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f09007b

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v2

    int-to-float v2, v2

    invoke-virtual {v0, v1, v2}, Landroid/widget/TextView;->setTextSize(IF)V

    iget-object v0, p0, Lcom/mediatek/contacts/calllog/CallLogListItemView;->mTextViewSimName:Landroid/widget/TextView;

    const/4 v1, -0x1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTextColor(I)V

    iget-object v0, p0, Lcom/mediatek/contacts/calllog/CallLogListItemView;->mTextViewSimName:Landroid/widget/TextView;

    const/16 v1, 0x10

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setGravity(I)V

    iget-object v0, p0, Lcom/mediatek/contacts/calllog/CallLogListItemView;->mTextViewSimName:Landroid/widget/TextView;

    sget-object v1, Landroid/text/TextUtils$TruncateAt;->MIDDLE:Landroid/text/TextUtils$TruncateAt;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setEllipsize(Landroid/text/TextUtils$TruncateAt;)V

    iget-object v0, p0, Lcom/mediatek/contacts/calllog/CallLogListItemView;->mTextViewSimName:Landroid/widget/TextView;

    sget v1, Lcom/mediatek/contacts/calllog/CallLogListItemView;->sSimNameWidthMaxDim:I

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setMaxWidth(I)V

    iget-object v0, p0, Lcom/mediatek/contacts/calllog/CallLogListItemView;->mTextViewSimName:Landroid/widget/TextView;

    invoke-virtual {p0}, Landroid/view/View;->isActivated()Z

    move-result v1

    invoke-virtual {v0, v1}, Landroid/view/View;->setActivated(Z)V

    iget-object v0, p0, Lcom/mediatek/contacts/calllog/CallLogListItemView;->mTextViewSimName:Landroid/widget/TextView;

    const-string v1, "China Mobile Realy?"

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lcom/mediatek/contacts/calllog/CallLogListItemView;->mTextViewSimName:Landroid/widget/TextView;

    invoke-virtual {p0, v0}, Landroid/view/ViewGroup;->addView(Landroid/view/View;)V

    :cond_0
    iget-object v0, p0, Lcom/mediatek/contacts/calllog/CallLogListItemView;->mTextViewSimName:Landroid/widget/TextView;

    return-object v0
.end method

.method public getTagId()I
    .locals 1

    iget v0, p0, Lcom/mediatek/contacts/calllog/CallLogListItemView;->mTagId:I

    return v0
.end method

.method protected isVisible(Landroid/view/View;)Z
    .locals 1
    .param p1    # Landroid/view/View;

    if-eqz p1, :cond_0

    invoke-virtual {p1}, Landroid/view/View;->getVisibility()I

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public jumpDrawablesToCurrentState()V
    .locals 1

    invoke-super {p0}, Landroid/view/ViewGroup;->jumpDrawablesToCurrentState()V

    iget-boolean v0, p0, Lcom/mediatek/contacts/calllog/CallLogListItemView;->mActivatedSupported:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/mediatek/contacts/calllog/CallLogListItemView;->mActivatedBgDrawable:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v0}, Landroid/graphics/drawable/Drawable;->jumpToCurrentState()V

    :cond_0
    return-void
.end method

.method protected layoutRightSide(IIII)I
    .locals 5
    .param p1    # I
    .param p2    # I
    .param p3    # I
    .param p4    # I

    iget-object v2, p0, Lcom/mediatek/contacts/calllog/CallLogListItemView;->mImageViewCall:Lcom/mediatek/contacts/calllog/CallLogListItemView$DontPressWithParentImageView;

    invoke-virtual {p0, v2}, Lcom/mediatek/contacts/calllog/CallLogListItemView;->isVisible(Landroid/view/View;)Z

    move-result v2

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/mediatek/contacts/calllog/CallLogListItemView;->mImageViewCall:Lcom/mediatek/contacts/calllog/CallLogListItemView$DontPressWithParentImageView;

    invoke-virtual {v2}, Landroid/view/View;->getMeasuredWidth()I

    move-result v0

    sub-int/2addr p4, v0

    iget-object v2, p0, Lcom/mediatek/contacts/calllog/CallLogListItemView;->mImageViewCall:Lcom/mediatek/contacts/calllog/CallLogListItemView$DontPressWithParentImageView;

    add-int v3, p4, v0

    iget v4, p0, Lcom/mediatek/contacts/calllog/CallLogListItemView;->mHorizontalDividerHeight:I

    sub-int v4, p1, v4

    invoke-virtual {v2, p4, p2, v3, v4}, Landroid/view/View;->layout(IIII)V

    const/4 v2, 0x1

    iput-boolean v2, p0, Lcom/mediatek/contacts/calllog/CallLogListItemView;->mVerticalDividerBeVisible:Z

    invoke-direct {p0}, Lcom/mediatek/contacts/calllog/CallLogListItemView;->ensureVerticalDivider()V

    iget v2, p0, Lcom/mediatek/contacts/calllog/CallLogListItemView;->mVerticalDividerWidth:I

    sub-int/2addr p4, v2

    add-int v2, p2, p1

    sget v3, Lcom/mediatek/contacts/calllog/CallLogListItemView;->sVerticalDividerHeightDim:I

    sub-int/2addr v2, v3

    div-int/lit8 v1, v2, 0x2

    iget-object v2, p0, Lcom/mediatek/contacts/calllog/CallLogListItemView;->mDrawableVertialDivider:Landroid/graphics/drawable/Drawable;

    iget v3, p0, Lcom/mediatek/contacts/calllog/CallLogListItemView;->mVerticalDividerWidth:I

    add-int/2addr v3, p4

    sget v4, Lcom/mediatek/contacts/calllog/CallLogListItemView;->sVerticalDividerHeightDim:I

    add-int/2addr v4, v1

    invoke-virtual {v2, p4, v1, v3, v4}, Landroid/graphics/drawable/Drawable;->setBounds(IIII)V

    sget v2, Lcom/mediatek/contacts/calllog/CallLogListItemView;->sCallLogInnerMarginDim:I

    sub-int/2addr p4, v2

    :goto_0
    return p4

    :cond_0
    const/4 v2, 0x0

    iput-boolean v2, p0, Lcom/mediatek/contacts/calllog/CallLogListItemView;->mVerticalDividerBeVisible:Z

    goto :goto_0
.end method

.method protected onLayout(ZIIII)V
    .locals 25
    .param p1    # Z
    .param p2    # I
    .param p3    # I
    .param p4    # I
    .param p5    # I

    sub-int v11, p5, p3

    sub-int v24, p4, p2

    invoke-virtual/range {p0 .. p0}, Landroid/view/View;->getPaddingRight()I

    move-result v12

    const/4 v4, 0x0

    move v5, v11

    if-nez v12, :cond_d

    sget v3, Lcom/mediatek/contacts/calllog/CallLogListItemView;->sListItemPaddingLeftDim:I

    :goto_0
    if-nez v12, :cond_e

    sget v2, Lcom/mediatek/contacts/calllog/CallLogListItemView;->sListItemPaddingRightDim:I

    :goto_1
    sub-int v6, v24, v2

    move-object/from16 v0, p0

    iget-boolean v2, v0, Lcom/mediatek/contacts/calllog/CallLogListItemView;->mCallLogDateVisible:Z

    if-eqz v2, :cond_0

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/mediatek/contacts/calllog/CallLogListItemView;->mTextViewCallLogDate:Landroid/widget/TextView;

    move-object/from16 v0, p0

    iget v7, v0, Lcom/mediatek/contacts/calllog/CallLogListItemView;->mTextViewCallLogDateHeight:I

    invoke-virtual {v2, v3, v4, v6, v7}, Landroid/view/View;->layout(IIII)V

    move-object/from16 v0, p0

    iget v2, v0, Lcom/mediatek/contacts/calllog/CallLogListItemView;->mTextViewCallLogDateHeight:I

    add-int/2addr v4, v2

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/mediatek/contacts/calllog/CallLogListItemView;->mViewHorizontalDateDivider:Landroid/view/View;

    move-object/from16 v0, p0

    iget v7, v0, Lcom/mediatek/contacts/calllog/CallLogListItemView;->mViewHorizontalDateDividerHeight:I

    add-int/2addr v7, v4

    invoke-virtual {v2, v3, v4, v6, v7}, Landroid/view/View;->layout(IIII)V

    move-object/from16 v0, p0

    iget v2, v0, Lcom/mediatek/contacts/calllog/CallLogListItemView;->mViewHorizontalDateDividerHeight:I

    add-int/2addr v4, v2

    :cond_0
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/mediatek/contacts/calllog/CallLogListItemView;->mBackground:Landroid/widget/ImageView;

    const/4 v7, 0x0

    move/from16 v0, v24

    invoke-virtual {v2, v7, v4, v0, v11}, Landroid/view/View;->layout(IIII)V

    move-object/from16 v0, p0

    iget-boolean v2, v0, Lcom/mediatek/contacts/calllog/CallLogListItemView;->mHorizontalDividerVisible:Z

    if-eqz v2, :cond_2

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/mediatek/contacts/calllog/CallLogListItemView;->mViewHorizontalDivider:Landroid/view/View;

    if-nez v2, :cond_1

    invoke-virtual/range {p0 .. p0}, Lcom/mediatek/contacts/calllog/CallLogListItemView;->getHorizontalDivider()Landroid/view/View;

    :cond_1
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/mediatek/contacts/calllog/CallLogListItemView;->mViewHorizontalDivider:Landroid/view/View;

    move-object/from16 v0, p0

    iget v7, v0, Lcom/mediatek/contacts/calllog/CallLogListItemView;->mHorizontalDividerHeight:I

    sub-int v7, v11, v7

    invoke-virtual {v2, v3, v7, v6, v11}, Landroid/view/View;->layout(IIII)V

    move-object/from16 v0, p0

    iget v2, v0, Lcom/mediatek/contacts/calllog/CallLogListItemView;->mHorizontalDividerHeight:I

    sub-int/2addr v5, v2

    :cond_2
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/mediatek/contacts/calllog/CallLogListItemView;->mBoundsWithoutHeader:Landroid/graphics/Rect;

    const/4 v7, 0x0

    move/from16 v0, v24

    invoke-virtual {v2, v7, v4, v0, v5}, Landroid/graphics/Rect;->set(IIII)V

    move-object/from16 v0, p0

    iget-boolean v2, v0, Lcom/mediatek/contacts/calllog/CallLogListItemView;->mActivatedSupported:Z

    if-eqz v2, :cond_3

    invoke-virtual/range {p0 .. p0}, Landroid/view/View;->isActivated()Z

    move-result v2

    if-eqz v2, :cond_3

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/mediatek/contacts/calllog/CallLogListItemView;->mActivatedBgDrawable:Landroid/graphics/drawable/Drawable;

    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/mediatek/contacts/calllog/CallLogListItemView;->mBoundsWithoutHeader:Landroid/graphics/Rect;

    invoke-virtual {v2, v7}, Landroid/graphics/drawable/Drawable;->setBounds(Landroid/graphics/Rect;)V

    :cond_3
    sget v2, Lcom/mediatek/contacts/calllog/CallLogListItemView;->sListItemQuickContactPaddingTop:I

    add-int/2addr v4, v2

    sget v2, Lcom/mediatek/contacts/calllog/CallLogListItemView;->sListItemQuickContactPaddingBottom:I

    sub-int/2addr v5, v2

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/mediatek/contacts/calllog/CallLogListItemView;->mCheckBoxMultiSel:Landroid/widget/CheckBox;

    move-object/from16 v0, p0

    invoke-virtual {v0, v2}, Lcom/mediatek/contacts/calllog/CallLogListItemView;->isVisible(Landroid/view/View;)Z

    move-result v2

    if-eqz v2, :cond_4

    sub-int v2, v5, v4

    move-object/from16 v0, p0

    iget v7, v0, Lcom/mediatek/contacts/calllog/CallLogListItemView;->mCheckBoxMultiSelHeight:I

    sub-int/2addr v2, v7

    div-int/lit8 v2, v2, 0x2

    add-int v10, v4, v2

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/mediatek/contacts/calllog/CallLogListItemView;->mCheckBoxMultiSel:Landroid/widget/CheckBox;

    sget v7, Lcom/mediatek/contacts/calllog/CallLogListItemView;->sCheckBoxMultiSelWidthDim:I

    add-int/2addr v7, v3

    move-object/from16 v0, p0

    iget v8, v0, Lcom/mediatek/contacts/calllog/CallLogListItemView;->mCheckBoxMultiSelHeight:I

    add-int/2addr v8, v10

    invoke-virtual {v2, v3, v10, v7, v8}, Landroid/view/View;->layout(IIII)V

    sget v2, Lcom/mediatek/contacts/calllog/CallLogListItemView;->sCheckBoxMultiSelWidthDim:I

    add-int/2addr v3, v2

    :cond_4
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/mediatek/contacts/calllog/CallLogListItemView;->mQuickContactPhoto:Lcom/mediatek/contacts/widget/QuickContactBadgeWithPhoneNumber;

    move-object/from16 v0, p0

    invoke-virtual {v0, v2}, Lcom/mediatek/contacts/calllog/CallLogListItemView;->isVisible(Landroid/view/View;)Z

    move-result v2

    if-eqz v2, :cond_5

    sub-int v2, v5, v4

    move-object/from16 v0, p0

    iget v7, v0, Lcom/mediatek/contacts/calllog/CallLogListItemView;->mPhotoViewHeight:I

    sub-int/2addr v2, v7

    div-int/lit8 v2, v2, 0x2

    add-int v15, v4, v2

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/mediatek/contacts/calllog/CallLogListItemView;->mQuickContactPhoto:Lcom/mediatek/contacts/widget/QuickContactBadgeWithPhoneNumber;

    move-object/from16 v0, p0

    iget v7, v0, Lcom/mediatek/contacts/calllog/CallLogListItemView;->mPhotoViewWidth:I

    add-int/2addr v7, v3

    move-object/from16 v0, p0

    iget v8, v0, Lcom/mediatek/contacts/calllog/CallLogListItemView;->mPhotoViewHeight:I

    add-int/2addr v8, v15

    invoke-virtual {v2, v3, v15, v7, v8}, Landroid/view/View;->layout(IIII)V

    move-object/from16 v0, p0

    iget v2, v0, Lcom/mediatek/contacts/calllog/CallLogListItemView;->mPhotoViewWidth:I

    sget v7, Lcom/mediatek/contacts/calllog/CallLogListItemView;->sCallLogInnerMarginDim:I

    add-int/2addr v2, v7

    add-int/2addr v3, v2

    :cond_5
    move-object/from16 v0, p0

    invoke-virtual {v0, v11, v4, v5, v6}, Lcom/mediatek/contacts/calllog/CallLogListItemView;->layoutRightSide(IIII)I

    move-result v6

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/mediatek/contacts/calllog/CallLogListItemView;->mSelectIcon:Landroid/widget/ImageView;

    invoke-virtual {v2}, Landroid/view/View;->getMeasuredWidth()I

    move-result v14

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/mediatek/contacts/calllog/CallLogListItemView;->mSelectIcon:Landroid/widget/ImageView;

    invoke-virtual {v2}, Landroid/view/View;->getMeasuredWidth()I

    move-result v13

    sub-int v16, v24, v13

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/mediatek/contacts/calllog/CallLogListItemView;->mSelectIcon:Landroid/widget/ImageView;

    add-int/lit8 v7, v16, -0xa

    move-object/from16 v0, p0

    iget v8, v0, Lcom/mediatek/contacts/calllog/CallLogListItemView;->mHorizontalDividerHeight:I

    sub-int v8, v11, v8

    move/from16 v0, v24

    invoke-virtual {v2, v7, v4, v0, v8}, Landroid/view/View;->layout(IIII)V

    invoke-static {}, Lcom/mediatek/contacts/ExtensionManager;->getInstance()Lcom/mediatek/contacts/ExtensionManager;

    move-result-object v2

    invoke-virtual {v2}, Lcom/mediatek/contacts/ExtensionManager;->getCallListExtension()Lcom/android/contacts/ext/CallListExtension;

    move-result-object v2

    const-string v7, "ExtenstionForRCS"

    invoke-virtual {v2, v7}, Lcom/android/contacts/ext/CallListExtension;->checkPluginSupport(Ljava/lang/String;)Z

    move-result v17

    if-eqz v17, :cond_6

    invoke-static {}, Lcom/mediatek/contacts/ExtensionManager;->getInstance()Lcom/mediatek/contacts/ExtensionManager;

    move-result-object v2

    invoke-virtual {v2}, Lcom/mediatek/contacts/ExtensionManager;->getCallListExtension()Lcom/android/contacts/ext/CallListExtension;

    move-result-object v2

    move-object/from16 v0, p0

    iget v7, v0, Lcom/mediatek/contacts/calllog/CallLogListItemView;->mGapBetweenImageAndText:I

    move-object/from16 v0, p0

    iget-object v8, v0, Lcom/mediatek/contacts/calllog/CallLogListItemView;->mExtentionIcon:Landroid/widget/ImageView;

    const-string v9, "ExtenstionForRCS"

    invoke-virtual/range {v2 .. v9}, Lcom/android/contacts/ext/CallListExtension;->layoutExtentionIcon(IIIIILandroid/widget/ImageView;Ljava/lang/String;)I

    move-result v6

    :cond_6
    move-object/from16 v0, p0

    iget v2, v0, Lcom/mediatek/contacts/calllog/CallLogListItemView;->mTextViewNameHeight:I

    move-object/from16 v0, p0

    iget v7, v0, Lcom/mediatek/contacts/calllog/CallLogListItemView;->mTextViewNumberHeight:I

    add-int/2addr v2, v7

    move-object/from16 v0, p0

    iget v7, v0, Lcom/mediatek/contacts/calllog/CallLogListItemView;->mCallTypeIconSimNameMaxHeight:I

    add-int v22, v2, v7

    add-int v2, v5, v4

    sub-int v2, v2, v22

    div-int/lit8 v18, v2, 0x2

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/mediatek/contacts/calllog/CallLogListItemView;->mTextViewName:Landroid/widget/TextView;

    move-object/from16 v0, p0

    invoke-virtual {v0, v2}, Lcom/mediatek/contacts/calllog/CallLogListItemView;->isVisible(Landroid/view/View;)Z

    move-result v2

    if-eqz v2, :cond_7

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/mediatek/contacts/calllog/CallLogListItemView;->mTextViewName:Landroid/widget/TextView;

    move-object/from16 v0, p0

    iget v7, v0, Lcom/mediatek/contacts/calllog/CallLogListItemView;->mTextViewNameHeight:I

    add-int v7, v7, v18

    move/from16 v0, v18

    invoke-virtual {v2, v3, v0, v6, v7}, Landroid/view/View;->layout(IIII)V

    :cond_7
    move-object/from16 v0, p0

    iget v2, v0, Lcom/mediatek/contacts/calllog/CallLogListItemView;->mTextViewNameHeight:I

    add-int v18, v18, v2

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/mediatek/contacts/calllog/CallLogListItemView;->mTextViewNumber:Landroid/widget/TextView;

    move-object/from16 v0, p0

    invoke-virtual {v0, v2}, Lcom/mediatek/contacts/calllog/CallLogListItemView;->isVisible(Landroid/view/View;)Z

    move-result v2

    if-eqz v2, :cond_8

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/mediatek/contacts/calllog/CallLogListItemView;->mTextViewNumber:Landroid/widget/TextView;

    move-object/from16 v0, p0

    iget v7, v0, Lcom/mediatek/contacts/calllog/CallLogListItemView;->mTextViewNumberHeight:I

    add-int v7, v7, v18

    move/from16 v0, v18

    invoke-virtual {v2, v3, v0, v6, v7}, Landroid/view/View;->layout(IIII)V

    move-object/from16 v0, p0

    iget v2, v0, Lcom/mediatek/contacts/calllog/CallLogListItemView;->mTextViewNumberHeight:I

    add-int/lit8 v2, v2, 0x6

    add-int v18, v18, v2

    :cond_8
    move/from16 v19, v3

    const/16 v23, 0x0

    add-int v20, v18, v5

    move-object/from16 v0, p0

    iget v2, v0, Lcom/mediatek/contacts/calllog/CallLogListItemView;->mCallTypeIconHeight:I

    sub-int v2, v20, v2

    div-int/lit8 v2, v2, 0x2

    add-int/lit8 v21, v2, 0x2

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/mediatek/contacts/calllog/CallLogListItemView;->mCallTypeIcon:Lcom/android/contacts/calllog/CallTypeIconsView;

    move-object/from16 v0, p0

    invoke-virtual {v0, v2}, Lcom/mediatek/contacts/calllog/CallLogListItemView;->isVisible(Landroid/view/View;)Z

    move-result v2

    if-eqz v2, :cond_9

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/mediatek/contacts/calllog/CallLogListItemView;->mCallTypeIcon:Lcom/android/contacts/calllog/CallTypeIconsView;

    invoke-virtual {v2}, Landroid/view/View;->getMeasuredWidth()I

    move-result v23

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/mediatek/contacts/calllog/CallLogListItemView;->mCallTypeIcon:Lcom/android/contacts/calllog/CallTypeIconsView;

    add-int v7, v19, v23

    move-object/from16 v0, p0

    iget v8, v0, Lcom/mediatek/contacts/calllog/CallLogListItemView;->mCallTypeIconHeight:I

    add-int v8, v8, v21

    move/from16 v0, v19

    move/from16 v1, v21

    invoke-virtual {v2, v0, v1, v7, v8}, Landroid/view/View;->layout(IIII)V

    sget v2, Lcom/mediatek/contacts/calllog/CallLogListItemView;->sCallLogInnerMarginDim:I

    add-int v2, v2, v23

    add-int v19, v19, v2

    :cond_9
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/mediatek/contacts/calllog/CallLogListItemView;->mTextViewCallCount:Landroid/widget/TextView;

    move-object/from16 v0, p0

    invoke-virtual {v0, v2}, Lcom/mediatek/contacts/calllog/CallLogListItemView;->isVisible(Landroid/view/View;)Z

    move-result v2

    if-eqz v2, :cond_a

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/mediatek/contacts/calllog/CallLogListItemView;->mTextViewCallCount:Landroid/widget/TextView;

    invoke-virtual {v2}, Landroid/view/View;->getMeasuredWidth()I

    move-result v23

    move-object/from16 v0, p0

    iget v2, v0, Lcom/mediatek/contacts/calllog/CallLogListItemView;->mTextViewCallCountHeight:I

    sub-int v2, v20, v2

    div-int/lit8 v21, v2, 0x2

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/mediatek/contacts/calllog/CallLogListItemView;->mTextViewCallCount:Landroid/widget/TextView;

    add-int v7, v19, v23

    move-object/from16 v0, p0

    iget v8, v0, Lcom/mediatek/contacts/calllog/CallLogListItemView;->mTextViewCallCountHeight:I

    add-int v8, v8, v21

    move/from16 v0, v19

    move/from16 v1, v21

    invoke-virtual {v2, v0, v1, v7, v8}, Landroid/view/View;->layout(IIII)V

    sget v2, Lcom/mediatek/contacts/calllog/CallLogListItemView;->sCallLogInnerMarginDim:I

    add-int v2, v2, v23

    add-int v19, v19, v2

    :cond_a
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/mediatek/contacts/calllog/CallLogListItemView;->mTextViewSimName:Landroid/widget/TextView;

    move-object/from16 v0, p0

    invoke-virtual {v0, v2}, Lcom/mediatek/contacts/calllog/CallLogListItemView;->isVisible(Landroid/view/View;)Z

    move-result v2

    if-eqz v2, :cond_b

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/mediatek/contacts/calllog/CallLogListItemView;->mTextViewSimName:Landroid/widget/TextView;

    invoke-virtual {v2}, Landroid/view/View;->getMeasuredWidth()I

    move-result v23

    sget v2, Lcom/mediatek/contacts/calllog/CallLogListItemView;->sSimNameWidthMaxDim:I

    move/from16 v0, v23

    invoke-static {v0, v2}, Ljava/lang/Math;->min(II)I

    move-result v23

    move-object/from16 v0, p0

    iget v2, v0, Lcom/mediatek/contacts/calllog/CallLogListItemView;->mTextViewSimNameHeight:I

    sub-int v2, v20, v2

    div-int/lit8 v21, v2, 0x2

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/mediatek/contacts/calllog/CallLogListItemView;->mTextViewSimName:Landroid/widget/TextView;

    add-int v7, v19, v23

    move-object/from16 v0, p0

    iget v8, v0, Lcom/mediatek/contacts/calllog/CallLogListItemView;->mTextViewSimNameHeight:I

    add-int v8, v8, v21

    move/from16 v0, v19

    move/from16 v1, v21

    invoke-virtual {v2, v0, v1, v7, v8}, Landroid/view/View;->layout(IIII)V

    sget v2, Lcom/mediatek/contacts/calllog/CallLogListItemView;->sCallLogInnerMarginDim:I

    add-int v2, v2, v23

    add-int v19, v19, v2

    :cond_b
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/mediatek/contacts/calllog/CallLogListItemView;->mTextViewCallTime:Landroid/widget/TextView;

    move-object/from16 v0, p0

    invoke-virtual {v0, v2}, Lcom/mediatek/contacts/calllog/CallLogListItemView;->isVisible(Landroid/view/View;)Z

    move-result v2

    if-eqz v2, :cond_c

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/mediatek/contacts/calllog/CallLogListItemView;->mTextViewCallTime:Landroid/widget/TextView;

    invoke-virtual {v2}, Landroid/view/View;->getMeasuredWidth()I

    move-result v23

    move-object/from16 v0, p0

    iget v2, v0, Lcom/mediatek/contacts/calllog/CallLogListItemView;->mTextViewCallTimeHeight:I

    sub-int v2, v20, v2

    div-int/lit8 v21, v2, 0x2

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/mediatek/contacts/calllog/CallLogListItemView;->mTextViewCallTime:Landroid/widget/TextView;

    add-int v7, v19, v23

    move-object/from16 v0, p0

    iget v8, v0, Lcom/mediatek/contacts/calllog/CallLogListItemView;->mTextViewCallTimeHeight:I

    add-int v8, v8, v21

    move/from16 v0, v19

    move/from16 v1, v21

    invoke-virtual {v2, v0, v1, v7, v8}, Landroid/view/View;->layout(IIII)V

    :cond_c
    return-void

    :cond_d
    const/4 v3, 0x0

    goto/16 :goto_0

    :cond_e
    const/4 v2, 0x0

    goto/16 :goto_1
.end method

.method protected onMeasure(II)V
    .locals 11
    .param p1    # I
    .param p2    # I

    const/high16 v10, -0x80000000

    const/4 v9, 0x0

    invoke-static {v9, p1}, Lcom/mediatek/contacts/calllog/CallLogListItemView;->resolveSize(II)I

    move-result v5

    iget-boolean v6, p0, Lcom/mediatek/contacts/calllog/CallLogListItemView;->mCallLogDateVisible:Z

    if-eqz v6, :cond_9

    iget v6, p0, Lcom/mediatek/contacts/calllog/CallLogListItemView;->mPreferredHeight:I

    iget v7, p0, Lcom/mediatek/contacts/calllog/CallLogListItemView;->mCallLogDateBackgroundHeight:I

    add-int/2addr v6, v7

    iget v7, p0, Lcom/mediatek/contacts/calllog/CallLogListItemView;->mHorizontalDateDividerHeight:I

    add-int v4, v6, v7

    :goto_0
    iput v9, p0, Lcom/mediatek/contacts/calllog/CallLogListItemView;->mTextViewCallLogDateHeight:I

    iput v9, p0, Lcom/mediatek/contacts/calllog/CallLogListItemView;->mViewHorizontalDateDividerHeight:I

    iput v9, p0, Lcom/mediatek/contacts/calllog/CallLogListItemView;->mTextViewNameHeight:I

    iput v9, p0, Lcom/mediatek/contacts/calllog/CallLogListItemView;->mTextViewNumberHeight:I

    iput v9, p0, Lcom/mediatek/contacts/calllog/CallLogListItemView;->mCallTypeIconHeight:I

    iput v9, p0, Lcom/mediatek/contacts/calllog/CallLogListItemView;->mTextViewCallCountHeight:I

    iput v9, p0, Lcom/mediatek/contacts/calllog/CallLogListItemView;->mTextViewSimNameHeight:I

    iput v9, p0, Lcom/mediatek/contacts/calllog/CallLogListItemView;->mTextViewCallTimeHeight:I

    iget-object v6, p0, Lcom/mediatek/contacts/calllog/CallLogListItemView;->mTextViewCallLogDate:Landroid/widget/TextView;

    invoke-virtual {p0, v6}, Lcom/mediatek/contacts/calllog/CallLogListItemView;->isVisible(Landroid/view/View;)Z

    move-result v6

    if-eqz v6, :cond_0

    iget-object v6, p0, Lcom/mediatek/contacts/calllog/CallLogListItemView;->mTextViewCallLogDate:Landroid/widget/TextView;

    const/high16 v7, 0x40000000

    invoke-static {v5, v7}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v7

    invoke-static {v9, v9}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v8

    invoke-virtual {v6, v7, v8}, Landroid/view/View;->measure(II)V

    iget-object v6, p0, Lcom/mediatek/contacts/calllog/CallLogListItemView;->mTextViewCallLogDate:Landroid/widget/TextView;

    invoke-virtual {v6}, Landroid/view/View;->getMeasuredHeight()I

    move-result v6

    iput v6, p0, Lcom/mediatek/contacts/calllog/CallLogListItemView;->mTextViewCallLogDateHeight:I

    const/4 v6, 0x3

    iput v6, p0, Lcom/mediatek/contacts/calllog/CallLogListItemView;->mViewHorizontalDateDividerHeight:I

    :cond_0
    sget v6, Lcom/mediatek/contacts/calllog/CallLogListItemView;->sImageViewCallWidthDim:I

    sub-int v6, v5, v6

    iget v7, p0, Lcom/mediatek/contacts/calllog/CallLogListItemView;->mVerticalDividerWidth:I

    sub-int/2addr v6, v7

    sget v7, Lcom/mediatek/contacts/calllog/CallLogListItemView;->sCallLogInnerMarginDim:I

    sub-int v3, v6, v7

    iget-object v6, p0, Lcom/mediatek/contacts/calllog/CallLogListItemView;->mTextViewName:Landroid/widget/TextView;

    invoke-virtual {p0, v6}, Lcom/mediatek/contacts/calllog/CallLogListItemView;->isVisible(Landroid/view/View;)Z

    move-result v6

    if-eqz v6, :cond_a

    iget-object v6, p0, Lcom/mediatek/contacts/calllog/CallLogListItemView;->mTextViewName:Landroid/widget/TextView;

    invoke-static {v3, v10}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v7

    invoke-static {v9, v9}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v8

    invoke-virtual {v6, v7, v8}, Landroid/view/View;->measure(II)V

    iget-object v6, p0, Lcom/mediatek/contacts/calllog/CallLogListItemView;->mTextViewName:Landroid/widget/TextView;

    invoke-virtual {v6}, Landroid/view/View;->getMeasuredHeight()I

    move-result v6

    iput v6, p0, Lcom/mediatek/contacts/calllog/CallLogListItemView;->mTextViewNameHeight:I

    :goto_1
    iget-object v6, p0, Lcom/mediatek/contacts/calllog/CallLogListItemView;->mTextViewNumber:Landroid/widget/TextView;

    invoke-virtual {p0, v6}, Lcom/mediatek/contacts/calllog/CallLogListItemView;->isVisible(Landroid/view/View;)Z

    move-result v6

    if-eqz v6, :cond_1

    iget-object v6, p0, Lcom/mediatek/contacts/calllog/CallLogListItemView;->mTextViewNumber:Landroid/widget/TextView;

    invoke-static {v3, v10}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v7

    invoke-static {v9, v9}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v8

    invoke-virtual {v6, v7, v8}, Landroid/view/View;->measure(II)V

    iget-object v6, p0, Lcom/mediatek/contacts/calllog/CallLogListItemView;->mTextViewNumber:Landroid/widget/TextView;

    invoke-virtual {v6}, Landroid/view/View;->getMeasuredHeight()I

    move-result v6

    iput v6, p0, Lcom/mediatek/contacts/calllog/CallLogListItemView;->mTextViewNumberHeight:I

    :cond_1
    iget-object v6, p0, Lcom/mediatek/contacts/calllog/CallLogListItemView;->mCallTypeIcon:Lcom/android/contacts/calllog/CallTypeIconsView;

    invoke-virtual {p0, v6}, Lcom/mediatek/contacts/calllog/CallLogListItemView;->isVisible(Landroid/view/View;)Z

    move-result v6

    if-eqz v6, :cond_2

    iget-object v6, p0, Lcom/mediatek/contacts/calllog/CallLogListItemView;->mCallTypeIcon:Lcom/android/contacts/calllog/CallTypeIconsView;

    invoke-static {v9, v9}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v7

    invoke-static {v9, v9}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v8

    invoke-virtual {v6, v7, v8}, Landroid/view/View;->measure(II)V

    iget-object v6, p0, Lcom/mediatek/contacts/calllog/CallLogListItemView;->mCallTypeIcon:Lcom/android/contacts/calllog/CallTypeIconsView;

    invoke-virtual {v6}, Landroid/view/View;->getMeasuredHeight()I

    move-result v6

    iput v6, p0, Lcom/mediatek/contacts/calllog/CallLogListItemView;->mCallTypeIconHeight:I

    :cond_2
    iget-object v6, p0, Lcom/mediatek/contacts/calllog/CallLogListItemView;->mTextViewCallCount:Landroid/widget/TextView;

    invoke-virtual {p0, v6}, Lcom/mediatek/contacts/calllog/CallLogListItemView;->isVisible(Landroid/view/View;)Z

    move-result v6

    if-eqz v6, :cond_3

    iget-object v6, p0, Lcom/mediatek/contacts/calllog/CallLogListItemView;->mTextViewCallCount:Landroid/widget/TextView;

    invoke-static {v9, v9}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v7

    invoke-static {v9, v9}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v8

    invoke-virtual {v6, v7, v8}, Landroid/view/View;->measure(II)V

    iget-object v6, p0, Lcom/mediatek/contacts/calllog/CallLogListItemView;->mTextViewCallCount:Landroid/widget/TextView;

    invoke-virtual {v6}, Landroid/view/View;->getMeasuredHeight()I

    move-result v6

    iput v6, p0, Lcom/mediatek/contacts/calllog/CallLogListItemView;->mTextViewCallCountHeight:I

    :cond_3
    iget v6, p0, Lcom/mediatek/contacts/calllog/CallLogListItemView;->mCallTypeIconHeight:I

    iget v7, p0, Lcom/mediatek/contacts/calllog/CallLogListItemView;->mTextViewCallCountHeight:I

    invoke-static {v6, v7}, Ljava/lang/Math;->max(II)I

    move-result v6

    iput v6, p0, Lcom/mediatek/contacts/calllog/CallLogListItemView;->mCallTypeIconSimNameMaxHeight:I

    iget-object v6, p0, Lcom/mediatek/contacts/calllog/CallLogListItemView;->mTextViewSimName:Landroid/widget/TextView;

    invoke-virtual {p0, v6}, Lcom/mediatek/contacts/calllog/CallLogListItemView;->isVisible(Landroid/view/View;)Z

    move-result v6

    if-eqz v6, :cond_4

    iget-object v6, p0, Lcom/mediatek/contacts/calllog/CallLogListItemView;->mTextViewSimName:Landroid/widget/TextView;

    invoke-static {v9, v9}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v7

    invoke-static {v9, v9}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v8

    invoke-virtual {v6, v7, v8}, Landroid/view/View;->measure(II)V

    iget-object v6, p0, Lcom/mediatek/contacts/calllog/CallLogListItemView;->mTextViewSimName:Landroid/widget/TextView;

    invoke-virtual {v6}, Landroid/view/View;->getMeasuredHeight()I

    move-result v6

    iput v6, p0, Lcom/mediatek/contacts/calllog/CallLogListItemView;->mTextViewSimNameHeight:I

    :cond_4
    iget v6, p0, Lcom/mediatek/contacts/calllog/CallLogListItemView;->mCallTypeIconSimNameMaxHeight:I

    iget v7, p0, Lcom/mediatek/contacts/calllog/CallLogListItemView;->mTextViewSimNameHeight:I

    invoke-static {v6, v7}, Ljava/lang/Math;->max(II)I

    move-result v6

    iput v6, p0, Lcom/mediatek/contacts/calllog/CallLogListItemView;->mCallTypeIconSimNameMaxHeight:I

    iget-object v6, p0, Lcom/mediatek/contacts/calllog/CallLogListItemView;->mTextViewCallTime:Landroid/widget/TextView;

    invoke-virtual {p0, v6}, Lcom/mediatek/contacts/calllog/CallLogListItemView;->isVisible(Landroid/view/View;)Z

    move-result v6

    if-eqz v6, :cond_5

    iget-object v6, p0, Lcom/mediatek/contacts/calllog/CallLogListItemView;->mTextViewCallTime:Landroid/widget/TextView;

    invoke-static {v9, v9}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v7

    invoke-static {v9, v9}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v8

    invoke-virtual {v6, v7, v8}, Landroid/view/View;->measure(II)V

    iget-object v6, p0, Lcom/mediatek/contacts/calllog/CallLogListItemView;->mTextViewCallTime:Landroid/widget/TextView;

    invoke-virtual {v6}, Landroid/view/View;->getMeasuredHeight()I

    move-result v6

    iput v6, p0, Lcom/mediatek/contacts/calllog/CallLogListItemView;->mTextViewCallTimeHeight:I

    :cond_5
    iget v6, p0, Lcom/mediatek/contacts/calllog/CallLogListItemView;->mCallTypeIconSimNameMaxHeight:I

    iget v7, p0, Lcom/mediatek/contacts/calllog/CallLogListItemView;->mTextViewCallTimeHeight:I

    invoke-static {v6, v7}, Ljava/lang/Math;->max(II)I

    move-result v6

    iput v6, p0, Lcom/mediatek/contacts/calllog/CallLogListItemView;->mCallTypeIconSimNameMaxHeight:I

    iget-object v6, p0, Lcom/mediatek/contacts/calllog/CallLogListItemView;->mImageViewCall:Lcom/mediatek/contacts/calllog/CallLogListItemView$DontPressWithParentImageView;

    invoke-virtual {p0, v6}, Lcom/mediatek/contacts/calllog/CallLogListItemView;->isVisible(Landroid/view/View;)Z

    move-result v6

    if-eqz v6, :cond_6

    iget-object v6, p0, Lcom/mediatek/contacts/calllog/CallLogListItemView;->mImageViewCall:Lcom/mediatek/contacts/calllog/CallLogListItemView$DontPressWithParentImageView;

    sget v7, Lcom/mediatek/contacts/calllog/CallLogListItemView;->sImageViewCallWidthDim:I

    sget v8, Lcom/mediatek/contacts/calllog/CallLogListItemView;->sImageViewCallHeightDim:I

    invoke-virtual {v6, v7, v8}, Landroid/view/View;->measure(II)V

    :cond_6
    iget-object v6, p0, Lcom/mediatek/contacts/calllog/CallLogListItemView;->mSelectIcon:Landroid/widget/ImageView;

    sget v7, Lcom/mediatek/contacts/calllog/CallLogListItemView;->sImageViewCallWidthDim:I

    mul-int/lit8 v7, v7, 0x2

    sget v8, Lcom/mediatek/contacts/calllog/CallLogListItemView;->sImageViewCallHeightDim:I

    mul-int/lit8 v8, v8, 0x2

    invoke-virtual {v6, v7, v8}, Landroid/view/View;->measure(II)V

    iget-object v6, p0, Lcom/mediatek/contacts/calllog/CallLogListItemView;->mCheckBoxMultiSel:Landroid/widget/CheckBox;

    invoke-virtual {p0, v6}, Lcom/mediatek/contacts/calllog/CallLogListItemView;->isVisible(Landroid/view/View;)Z

    move-result v6

    if-eqz v6, :cond_7

    iget-object v6, p0, Lcom/mediatek/contacts/calllog/CallLogListItemView;->mCheckBoxMultiSel:Landroid/widget/CheckBox;

    sget v7, Lcom/mediatek/contacts/calllog/CallLogListItemView;->sCheckBoxMultiSelWidthDim:I

    sget v8, Lcom/mediatek/contacts/calllog/CallLogListItemView;->sCheckBoxMultiSelWidthDim:I

    invoke-virtual {v6, v7, v8}, Landroid/view/View;->measure(II)V

    iget-object v6, p0, Lcom/mediatek/contacts/calllog/CallLogListItemView;->mCheckBoxMultiSel:Landroid/widget/CheckBox;

    invoke-virtual {v6}, Landroid/view/View;->getMeasuredHeight()I

    move-result v6

    iput v6, p0, Lcom/mediatek/contacts/calllog/CallLogListItemView;->mCheckBoxMultiSelHeight:I

    :cond_7
    invoke-virtual {p0}, Landroid/view/View;->getPaddingTop()I

    move-result v2

    invoke-virtual {p0}, Landroid/view/View;->getPaddingBottom()I

    move-result v1

    if-nez v2, :cond_b

    sget v2, Lcom/mediatek/contacts/calllog/CallLogListItemView;->sListItemQuickContactPaddingTop:I

    :goto_2
    if-nez v1, :cond_c

    sget v2, Lcom/mediatek/contacts/calllog/CallLogListItemView;->sListItemQuickContactPaddingBottom:I

    :goto_3
    iget v6, p0, Lcom/mediatek/contacts/calllog/CallLogListItemView;->mTextViewCallLogDateHeight:I

    iget v7, p0, Lcom/mediatek/contacts/calllog/CallLogListItemView;->mViewHorizontalDateDividerHeight:I

    add-int/2addr v6, v7

    iget v7, p0, Lcom/mediatek/contacts/calllog/CallLogListItemView;->mTextViewNameHeight:I

    add-int/2addr v6, v7

    iget v7, p0, Lcom/mediatek/contacts/calllog/CallLogListItemView;->mTextViewNumberHeight:I

    add-int/2addr v6, v7

    iget v7, p0, Lcom/mediatek/contacts/calllog/CallLogListItemView;->mCallTypeIconSimNameMaxHeight:I

    add-int/2addr v6, v7

    add-int/2addr v6, v1

    add-int v0, v6, v2

    iget v6, p0, Lcom/mediatek/contacts/calllog/CallLogListItemView;->mPhotoViewHeight:I

    add-int/2addr v6, v2

    add-int/2addr v6, v1

    invoke-static {v0, v6}, Ljava/lang/Math;->max(II)I

    move-result v0

    iget-boolean v6, p0, Lcom/mediatek/contacts/calllog/CallLogListItemView;->mHorizontalDividerVisible:Z

    if-eqz v6, :cond_8

    iget v6, p0, Lcom/mediatek/contacts/calllog/CallLogListItemView;->mHorizontalDividerHeight:I

    add-int/2addr v0, v6

    :cond_8
    invoke-static {v0, v4}, Ljava/lang/Math;->max(II)I

    move-result v0

    invoke-virtual {p0, v5, v0}, Landroid/view/View;->setMeasuredDimension(II)V

    invoke-static {}, Lcom/mediatek/contacts/ExtensionManager;->getInstance()Lcom/mediatek/contacts/ExtensionManager;

    move-result-object v6

    invoke-virtual {v6}, Lcom/mediatek/contacts/ExtensionManager;->getCallListExtension()Lcom/android/contacts/ext/CallListExtension;

    move-result-object v6

    iget-object v7, p0, Lcom/mediatek/contacts/calllog/CallLogListItemView;->mExtentionIcon:Landroid/widget/ImageView;

    const-string v8, "ExtenstionForRCS"

    invoke-virtual {v6, v7, v8}, Lcom/android/contacts/ext/CallListExtension;->measureExtention(Landroid/widget/ImageView;Ljava/lang/String;)V

    return-void

    :cond_9
    iget v4, p0, Lcom/mediatek/contacts/calllog/CallLogListItemView;->mPreferredHeight:I

    goto/16 :goto_0

    :cond_a
    const/16 v6, 0x25

    iput v6, p0, Lcom/mediatek/contacts/calllog/CallLogListItemView;->mTextViewNameHeight:I

    goto/16 :goto_1

    :cond_b
    sput v2, Lcom/mediatek/contacts/calllog/CallLogListItemView;->sListItemQuickContactPaddingTop:I

    goto :goto_2

    :cond_c
    sput v2, Lcom/mediatek/contacts/calllog/CallLogListItemView;->sListItemQuickContactPaddingBottom:I

    goto :goto_3
.end method

.method public onTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 6
    .param p1    # Landroid/view/MotionEvent;

    invoke-virtual {p1}, Landroid/view/MotionEvent;->isTouchEvent()Z

    move-result v3

    if-eqz v3, :cond_5

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v3

    if-nez v3, :cond_5

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v0

    const/4 v1, -0x1

    const/4 v2, -0x1

    iget-object v3, p0, Lcom/mediatek/contacts/calllog/CallLogListItemView;->mImageViewCall:Lcom/mediatek/contacts/calllog/CallLogListItemView$DontPressWithParentImageView;

    if-eqz v3, :cond_0

    iget-object v3, p0, Lcom/mediatek/contacts/calllog/CallLogListItemView;->mImageViewCall:Lcom/mediatek/contacts/calllog/CallLogListItemView$DontPressWithParentImageView;

    invoke-virtual {v3}, Landroid/view/View;->getLeft()I

    move-result v2

    :cond_0
    iget-object v3, p0, Lcom/mediatek/contacts/calllog/CallLogListItemView;->mCheckBoxMultiSel:Landroid/widget/CheckBox;

    if-eqz v3, :cond_4

    iget-object v3, p0, Lcom/mediatek/contacts/calllog/CallLogListItemView;->mCheckBoxMultiSel:Landroid/widget/CheckBox;

    invoke-virtual {v3}, Landroid/view/View;->getLeft()I

    move-result v1

    :cond_1
    :goto_0
    const-string v3, "CallLogListItemView"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "onTouchEvent, rightSide="

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ", leftSide="

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    if-ltz v2, :cond_2

    if-eqz v2, :cond_2

    int-to-float v3, v2

    cmpg-float v3, v0, v3

    if-gez v3, :cond_5

    :cond_2
    if-ltz v1, :cond_3

    int-to-float v3, v1

    cmpl-float v3, v0, v3

    if-lez v3, :cond_5

    :cond_3
    invoke-super {p0, p1}, Landroid/view/View;->onTouchEvent(Landroid/view/MotionEvent;)Z

    move-result v3

    :goto_1
    return v3

    :cond_4
    iget-object v3, p0, Lcom/mediatek/contacts/calllog/CallLogListItemView;->mQuickContactPhoto:Lcom/mediatek/contacts/widget/QuickContactBadgeWithPhoneNumber;

    if-eqz v3, :cond_1

    iget-object v3, p0, Lcom/mediatek/contacts/calllog/CallLogListItemView;->mQuickContactPhoto:Lcom/mediatek/contacts/widget/QuickContactBadgeWithPhoneNumber;

    invoke-virtual {v3}, Landroid/view/View;->getRight()I

    move-result v1

    goto :goto_0

    :cond_5
    const/4 v3, 0x1

    goto :goto_1
.end method

.method public removeExtentionIconView()V
    .locals 1

    iget-object v0, p0, Lcom/mediatek/contacts/calllog/CallLogListItemView;->mExtentionIcon:Landroid/widget/ImageView;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/mediatek/contacts/calllog/CallLogListItemView;->mExtentionIcon:Landroid/widget/ImageView;

    invoke-virtual {p0, v0}, Landroid/view/ViewGroup;->removeView(Landroid/view/View;)V

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/mediatek/contacts/calllog/CallLogListItemView;->mExtentionIcon:Landroid/widget/ImageView;

    :cond_0
    return-void
.end method

.method public requestLayout()V
    .locals 0

    invoke-virtual {p0}, Landroid/view/View;->forceLayout()V

    return-void
.end method

.method public setCallLogName(Ljava/lang/String;)V
    .locals 2
    .param p1    # Ljava/lang/String;

    if-eqz p1, :cond_0

    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v0

    if-nez v0, :cond_2

    :cond_0
    iget-object v0, p0, Lcom/mediatek/contacts/calllog/CallLogListItemView;->mTextViewName:Landroid/widget/TextView;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/mediatek/contacts/calllog/CallLogListItemView;->mTextViewName:Landroid/widget/TextView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    :cond_1
    :goto_0
    return-void

    :cond_2
    invoke-virtual {p0}, Lcom/mediatek/contacts/calllog/CallLogListItemView;->getCallLogNameTextView()Landroid/widget/TextView;

    iget-object v0, p0, Lcom/mediatek/contacts/calllog/CallLogListItemView;->mTextViewName:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lcom/mediatek/contacts/calllog/CallLogListItemView;->mTextViewName:Landroid/widget/TextView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    goto :goto_0
.end method

.method public setCallType(II)V
    .locals 2
    .param p1    # I
    .param p2    # I

    iget-object v0, p0, Lcom/mediatek/contacts/calllog/CallLogListItemView;->mCallTypeIcon:Lcom/android/contacts/calllog/CallTypeIconsView;

    if-nez v0, :cond_0

    invoke-virtual {p0}, Lcom/mediatek/contacts/calllog/CallLogListItemView;->getCallTypeIconView()Lcom/android/contacts/calllog/CallTypeIconsView;

    :cond_0
    iget-object v0, p0, Lcom/mediatek/contacts/calllog/CallLogListItemView;->mCallTypeIcon:Lcom/android/contacts/calllog/CallTypeIconsView;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/mediatek/contacts/calllog/CallLogListItemView;->mCallTypeIcon:Lcom/android/contacts/calllog/CallTypeIconsView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    iget-object v0, p0, Lcom/mediatek/contacts/calllog/CallLogListItemView;->mCallTypeIcon:Lcom/android/contacts/calllog/CallTypeIconsView;

    invoke-virtual {v0, p1, p2}, Lcom/android/contacts/calllog/CallTypeIconsView;->set(II)V

    :goto_0
    return-void

    :cond_1
    const-string v0, "CallLogListItemView"

    const-string v1, "Error!!! - setCallType() mCallTypeIcon is null!"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method public setCheckBoxMultiSel(ZZ)V
    .locals 2
    .param p1    # Z
    .param p2    # Z

    invoke-virtual {p0}, Lcom/mediatek/contacts/calllog/CallLogListItemView;->getCheckBoxMultiSel()Landroid/widget/CheckBox;

    iget-object v0, p0, Lcom/mediatek/contacts/calllog/CallLogListItemView;->mCheckBoxMultiSel:Landroid/widget/CheckBox;

    invoke-virtual {v0, p1}, Landroid/view/View;->setFocusable(Z)V

    iget-object v0, p0, Lcom/mediatek/contacts/calllog/CallLogListItemView;->mCheckBoxMultiSel:Landroid/widget/CheckBox;

    invoke-virtual {v0, p2}, Landroid/view/View;->setClickable(Z)V

    iget-object v0, p0, Lcom/mediatek/contacts/calllog/CallLogListItemView;->mCheckBoxMultiSel:Landroid/widget/CheckBox;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    return-void
.end method

.method public setExtentionIcon(Z)V
    .locals 2
    .param p1    # Z

    if-eqz p1, :cond_2

    iget-object v0, p0, Lcom/mediatek/contacts/calllog/CallLogListItemView;->mExtentionIcon:Landroid/widget/ImageView;

    if-nez v0, :cond_0

    invoke-virtual {p0}, Lcom/mediatek/contacts/calllog/CallLogListItemView;->getExtentionIconView()V

    :cond_0
    iget-object v0, p0, Lcom/mediatek/contacts/calllog/CallLogListItemView;->mExtentionIcon:Landroid/widget/ImageView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    :cond_1
    :goto_0
    return-void

    :cond_2
    iget-object v0, p0, Lcom/mediatek/contacts/calllog/CallLogListItemView;->mExtentionIcon:Landroid/widget/ImageView;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/mediatek/contacts/calllog/CallLogListItemView;->mExtentionIcon:Landroid/widget/ImageView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    goto :goto_0
.end method

.method public setNumber(Ljava/lang/String;)V
    .locals 2
    .param p1    # Ljava/lang/String;

    if-eqz p1, :cond_0

    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v0

    if-nez v0, :cond_2

    :cond_0
    iget-object v0, p0, Lcom/mediatek/contacts/calllog/CallLogListItemView;->mTextViewNumber:Landroid/widget/TextView;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/mediatek/contacts/calllog/CallLogListItemView;->mTextViewNumber:Landroid/widget/TextView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    :cond_1
    :goto_0
    return-void

    :cond_2
    invoke-virtual {p0}, Lcom/mediatek/contacts/calllog/CallLogListItemView;->getNumberTextView()Landroid/widget/TextView;

    iget-object v0, p0, Lcom/mediatek/contacts/calllog/CallLogListItemView;->mTextViewNumber:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lcom/mediatek/contacts/calllog/CallLogListItemView;->mTextViewNumber:Landroid/widget/TextView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    goto :goto_0
.end method

.method public setOnCallButtonClickListener(Landroid/view/View$OnClickListener;)V
    .locals 0
    .param p1    # Landroid/view/View$OnClickListener;

    iput-object p1, p0, Lcom/mediatek/contacts/calllog/CallLogListItemView;->mCallButtonClickListener:Landroid/view/View$OnClickListener;

    return-void
.end method

.method public setSectionDate(Ljava/lang/String;)V
    .locals 3
    .param p1    # Ljava/lang/String;

    const/16 v2, 0x8

    const/4 v1, 0x0

    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    invoke-virtual {p0}, Lcom/mediatek/contacts/calllog/CallLogListItemView;->getSectionDate()Landroid/widget/TextView;

    iget-object v0, p0, Lcom/mediatek/contacts/calllog/CallLogListItemView;->mTextViewCallLogDate:Landroid/widget/TextView;

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    iget-object v0, p0, Lcom/mediatek/contacts/calllog/CallLogListItemView;->mViewHorizontalDateDivider:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    iget-object v0, p0, Lcom/mediatek/contacts/calllog/CallLogListItemView;->mTextViewCallLogDate:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/mediatek/contacts/calllog/CallLogListItemView;->mCallLogDateVisible:Z

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/mediatek/contacts/calllog/CallLogListItemView;->mTextViewCallLogDate:Landroid/widget/TextView;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/mediatek/contacts/calllog/CallLogListItemView;->mTextViewCallLogDate:Landroid/widget/TextView;

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    :cond_1
    iget-object v0, p0, Lcom/mediatek/contacts/calllog/CallLogListItemView;->mViewHorizontalDateDivider:Landroid/view/View;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/mediatek/contacts/calllog/CallLogListItemView;->mViewHorizontalDateDivider:Landroid/view/View;

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    :cond_2
    iput-boolean v1, p0, Lcom/mediatek/contacts/calllog/CallLogListItemView;->mCallLogDateVisible:Z

    goto :goto_0
.end method

.method public setTagId(I)V
    .locals 0
    .param p1    # I

    iput p1, p0, Lcom/mediatek/contacts/calllog/CallLogListItemView;->mTagId:I

    return-void
.end method

.method protected verifyDrawable(Landroid/graphics/drawable/Drawable;)Z
    .locals 1
    .param p1    # Landroid/graphics/drawable/Drawable;

    iget-object v0, p0, Lcom/mediatek/contacts/calllog/CallLogListItemView;->mActivatedBgDrawable:Landroid/graphics/drawable/Drawable;

    if-eq p1, v0, :cond_0

    invoke-super {p0, p1}, Landroid/view/View;->verifyDrawable(Landroid/graphics/drawable/Drawable;)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method
