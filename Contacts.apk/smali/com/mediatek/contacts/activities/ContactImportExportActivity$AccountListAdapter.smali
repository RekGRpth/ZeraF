.class Lcom/mediatek/contacts/activities/ContactImportExportActivity$AccountListAdapter;
.super Landroid/widget/BaseAdapter;
.source "ContactImportExportActivity.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/mediatek/contacts/activities/ContactImportExportActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "AccountListAdapter"
.end annotation


# instance fields
.field private mAccountTypes:Lcom/android/contacts/model/AccountTypeManager;

.field private mContext:Landroid/content/Context;

.field private final mLayoutInflater:Landroid/view/LayoutInflater;

.field final synthetic this$0:Lcom/mediatek/contacts/activities/ContactImportExportActivity;


# direct methods
.method public constructor <init>(Lcom/mediatek/contacts/activities/ContactImportExportActivity;Landroid/content/Context;)V
    .locals 1
    .param p2    # Landroid/content/Context;

    iput-object p1, p0, Lcom/mediatek/contacts/activities/ContactImportExportActivity$AccountListAdapter;->this$0:Lcom/mediatek/contacts/activities/ContactImportExportActivity;

    invoke-direct {p0}, Landroid/widget/BaseAdapter;-><init>()V

    iput-object p2, p0, Lcom/mediatek/contacts/activities/ContactImportExportActivity$AccountListAdapter;->mContext:Landroid/content/Context;

    invoke-static {p2}, Lcom/android/contacts/model/AccountTypeManager;->getInstance(Landroid/content/Context;)Lcom/android/contacts/model/AccountTypeManager;

    move-result-object v0

    iput-object v0, p0, Lcom/mediatek/contacts/activities/ContactImportExportActivity$AccountListAdapter;->mAccountTypes:Lcom/android/contacts/model/AccountTypeManager;

    const-string v0, "layout_inflater"

    invoke-virtual {p2, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/LayoutInflater;

    iput-object v0, p0, Lcom/mediatek/contacts/activities/ContactImportExportActivity$AccountListAdapter;->mLayoutInflater:Landroid/view/LayoutInflater;

    return-void
.end method


# virtual methods
.method public getCount()I
    .locals 1

    iget-object v0, p0, Lcom/mediatek/contacts/activities/ContactImportExportActivity$AccountListAdapter;->this$0:Lcom/mediatek/contacts/activities/ContactImportExportActivity;

    invoke-static {v0}, Lcom/mediatek/contacts/activities/ContactImportExportActivity;->access$1200(Lcom/mediatek/contacts/activities/ContactImportExportActivity;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    return v0
.end method

.method public getItem(I)Lcom/mediatek/contacts/model/AccountWithDataSetEx;
    .locals 1
    .param p1    # I

    const/4 v0, 0x0

    return-object v0
.end method

.method public bridge synthetic getItem(I)Ljava/lang/Object;
    .locals 1
    .param p1    # I

    invoke-virtual {p0, p1}, Lcom/mediatek/contacts/activities/ContactImportExportActivity$AccountListAdapter;->getItem(I)Lcom/mediatek/contacts/model/AccountWithDataSetEx;

    move-result-object v0

    return-object v0
.end method

.method public getItemId(I)J
    .locals 2
    .param p1    # I

    int-to-long v0, p1

    return-wide v0
.end method

.method public getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 10
    .param p1    # I
    .param p2    # Landroid/view/View;
    .param p3    # Landroid/view/ViewGroup;

    const/4 v6, 0x0

    if-eqz p2, :cond_1

    move-object v5, p2

    check-cast v5, Lcom/mediatek/contacts/widget/ImportExportItem;

    :goto_0
    iget-object v7, p0, Lcom/mediatek/contacts/activities/ContactImportExportActivity$AccountListAdapter;->this$0:Lcom/mediatek/contacts/activities/ContactImportExportActivity;

    invoke-static {v7}, Lcom/mediatek/contacts/activities/ContactImportExportActivity;->access$1200(Lcom/mediatek/contacts/activities/ContactImportExportActivity;)Ljava/util/List;

    move-result-object v7

    invoke-interface {v7, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/mediatek/contacts/activities/ContactImportExportActivity$ListViewItemObject;

    iput-object v5, v3, Lcom/mediatek/contacts/activities/ContactImportExportActivity$ListViewItemObject;->mView:Lcom/mediatek/contacts/widget/ImportExportItem;

    iget-object v0, v3, Lcom/mediatek/contacts/activities/ContactImportExportActivity$ListViewItemObject;->mAccount:Lcom/mediatek/contacts/model/AccountWithDataSetEx;

    iget-object v7, p0, Lcom/mediatek/contacts/activities/ContactImportExportActivity$AccountListAdapter;->mAccountTypes:Lcom/android/contacts/model/AccountTypeManager;

    iget-object v8, v0, Landroid/accounts/Account;->type:Ljava/lang/String;

    iget-object v9, v0, Lcom/android/contacts/model/account/AccountWithDataSet;->dataSet:Ljava/lang/String;

    invoke-virtual {v7, v8, v9}, Lcom/android/contacts/model/AccountTypeManager;->getAccountType(Ljava/lang/String;Ljava/lang/String;)Lcom/android/contacts/model/account/AccountType;

    move-result-object v1

    iget-object v7, v3, Lcom/mediatek/contacts/activities/ContactImportExportActivity$ListViewItemObject;->mAccount:Lcom/mediatek/contacts/model/AccountWithDataSetEx;

    invoke-virtual {v7}, Lcom/mediatek/contacts/model/AccountWithDataSetEx;->getSlotId()I

    move-result v4

    invoke-static {}, Lcom/mediatek/contacts/activities/ContactImportExportActivity;->access$000()Ljava/lang/String;

    move-result-object v7

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "dataSet: "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    iget-object v9, v0, Lcom/android/contacts/model/account/AccountWithDataSet;->dataSet:Ljava/lang/String;

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, " slotId: "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    if-eqz v1, :cond_2

    invoke-virtual {v1}, Lcom/android/contacts/model/account/AccountType;->isIccCardAccount()Z

    move-result v7

    if-eqz v7, :cond_2

    iget-object v7, p0, Lcom/mediatek/contacts/activities/ContactImportExportActivity$AccountListAdapter;->mContext:Landroid/content/Context;

    invoke-virtual {v1, v7, v4}, Lcom/android/contacts/model/account/AccountType;->getDisplayIconBySlotId(Landroid/content/Context;I)Landroid/graphics/drawable/Drawable;

    move-result-object v2

    :goto_1
    invoke-virtual {v3}, Lcom/mediatek/contacts/activities/ContactImportExportActivity$ListViewItemObject;->getName()Ljava/lang/String;

    move-result-object v7

    iget-object v8, v0, Lcom/android/contacts/model/account/AccountWithDataSet;->dataSet:Ljava/lang/String;

    invoke-virtual {v5, v2, v7, v8}, Lcom/mediatek/contacts/widget/ImportExportItem;->bindView(Landroid/graphics/drawable/Drawable;Ljava/lang/String;Ljava/lang/String;)V

    iget-object v7, p0, Lcom/mediatek/contacts/activities/ContactImportExportActivity$AccountListAdapter;->this$0:Lcom/mediatek/contacts/activities/ContactImportExportActivity;

    invoke-static {v7}, Lcom/mediatek/contacts/activities/ContactImportExportActivity;->access$900(Lcom/mediatek/contacts/activities/ContactImportExportActivity;)I

    move-result v7

    if-ne v7, p1, :cond_0

    const/4 v6, 0x1

    :cond_0
    invoke-virtual {v5, v6}, Lcom/mediatek/contacts/widget/ImportExportItem;->setActivated(Z)V

    return-object v5

    :cond_1
    iget-object v7, p0, Lcom/mediatek/contacts/activities/ContactImportExportActivity$AccountListAdapter;->mLayoutInflater:Landroid/view/LayoutInflater;

    const v8, 0x7f040034

    invoke-virtual {v7, v8, p3, v6}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v5

    check-cast v5, Lcom/mediatek/contacts/widget/ImportExportItem;

    goto :goto_0

    :cond_2
    iget-object v7, p0, Lcom/mediatek/contacts/activities/ContactImportExportActivity$AccountListAdapter;->mContext:Landroid/content/Context;

    invoke-virtual {v1, v7}, Lcom/android/contacts/model/account/AccountType;->getDisplayIcon(Landroid/content/Context;)Landroid/graphics/drawable/Drawable;

    move-result-object v2

    goto :goto_1
.end method
