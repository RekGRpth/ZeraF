.class Lcom/mediatek/contacts/activities/ContactImportExportActivity$1;
.super Ljava/lang/Object;
.source "ContactImportExportActivity.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/mediatek/contacts/activities/ContactImportExportActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/mediatek/contacts/activities/ContactImportExportActivity;


# direct methods
.method constructor <init>(Lcom/mediatek/contacts/activities/ContactImportExportActivity;)V
    .locals 0

    iput-object p1, p0, Lcom/mediatek/contacts/activities/ContactImportExportActivity$1;->this$0:Lcom/mediatek/contacts/activities/ContactImportExportActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 4

    # getter for: Lcom/mediatek/contacts/activities/ContactImportExportActivity;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/mediatek/contacts/activities/ContactImportExportActivity;->access$000()Ljava/lang/String;

    move-result-object v1

    const-string v2, "mServiceComplete run"

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v1, p0, Lcom/mediatek/contacts/activities/ContactImportExportActivity$1;->this$0:Lcom/mediatek/contacts/activities/ContactImportExportActivity;

    # getter for: Lcom/mediatek/contacts/activities/ContactImportExportActivity;->mWaitingDialog:Landroid/app/ProgressDialog;
    invoke-static {v1}, Lcom/mediatek/contacts/activities/ContactImportExportActivity;->access$100(Lcom/mediatek/contacts/activities/ContactImportExportActivity;)Landroid/app/ProgressDialog;

    move-result-object v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/mediatek/contacts/activities/ContactImportExportActivity$1;->this$0:Lcom/mediatek/contacts/activities/ContactImportExportActivity;

    # getter for: Lcom/mediatek/contacts/activities/ContactImportExportActivity;->mWaitingDialog:Landroid/app/ProgressDialog;
    invoke-static {v1}, Lcom/mediatek/contacts/activities/ContactImportExportActivity;->access$100(Lcom/mediatek/contacts/activities/ContactImportExportActivity;)Landroid/app/ProgressDialog;

    move-result-object v1

    invoke-virtual {v1}, Landroid/app/ProgressDialog;->dismiss()V

    iget-object v1, p0, Lcom/mediatek/contacts/activities/ContactImportExportActivity$1;->this$0:Lcom/mediatek/contacts/activities/ContactImportExportActivity;

    const/4 v2, 0x0

    # setter for: Lcom/mediatek/contacts/activities/ContactImportExportActivity;->mWaitingDialog:Landroid/app/ProgressDialog;
    invoke-static {v1, v2}, Lcom/mediatek/contacts/activities/ContactImportExportActivity;->access$102(Lcom/mediatek/contacts/activities/ContactImportExportActivity;Landroid/app/ProgressDialog;)Landroid/app/ProgressDialog;

    :cond_0
    iget-object v1, p0, Lcom/mediatek/contacts/activities/ContactImportExportActivity$1;->this$0:Lcom/mediatek/contacts/activities/ContactImportExportActivity;

    # getter for: Lcom/mediatek/contacts/activities/ContactImportExportActivity;->mCellMgr:Lcom/mediatek/CellConnService/CellConnMgr;
    invoke-static {v1}, Lcom/mediatek/contacts/activities/ContactImportExportActivity;->access$200(Lcom/mediatek/contacts/activities/ContactImportExportActivity;)Lcom/mediatek/CellConnService/CellConnMgr;

    move-result-object v1

    invoke-virtual {v1}, Lcom/mediatek/CellConnService/CellConnMgr;->getResult()I

    move-result v0

    # getter for: Lcom/mediatek/contacts/activities/ContactImportExportActivity;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/mediatek/contacts/activities/ContactImportExportActivity;->access$000()Ljava/lang/String;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "mServiceComplete result = "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-static {v0}, Lcom/mediatek/CellConnService/CellConnMgr;->resultToString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v1, 0x2

    if-eq v0, v1, :cond_1

    iget-object v1, p0, Lcom/mediatek/contacts/activities/ContactImportExportActivity$1;->this$0:Lcom/mediatek/contacts/activities/ContactImportExportActivity;

    # invokes: Lcom/mediatek/contacts/activities/ContactImportExportActivity;->handleImportExportAction()V
    invoke-static {v1}, Lcom/mediatek/contacts/activities/ContactImportExportActivity;->access$300(Lcom/mediatek/contacts/activities/ContactImportExportActivity;)V

    :cond_1
    return-void
.end method
