.class public Lcom/mediatek/contacts/activities/ContactImportExportActivity;
.super Landroid/app/Activity;
.source "ContactImportExportActivity.java"

# interfaces
.implements Landroid/view/View$OnClickListener;
.implements Landroid/widget/AdapterView$OnItemClickListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/mediatek/contacts/activities/ContactImportExportActivity$AccountListAdapter;,
        Lcom/mediatek/contacts/activities/ContactImportExportActivity$MyLoaderCallbacks;,
        Lcom/mediatek/contacts/activities/ContactImportExportActivity$AccountsLoader;,
        Lcom/mediatek/contacts/activities/ContactImportExportActivity$ListViewItemObject;
    }
.end annotation


# static fields
.field private static final ACCOUNT_LOADER_ID:I = 0x0

.field public static final REQUEST_CODE:I = 0x1b207

.field public static final RESULT_CODE:I = 0x1b208

.field private static final SELECTION_VIEW_STEP_NONE:I = 0x0

.field private static final SELECTION_VIEW_STEP_ONE:I = 0x1

.field private static final SELECTION_VIEW_STEP_TWO:I = 0x2

.field public static final STORAGE_ACCOUNT_TYPE:Ljava/lang/String; = "_STORAGE_ACCOUNT"

.field private static final TAG:Ljava/lang/String;


# instance fields
.field private mAccounts:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/mediatek/contacts/model/AccountWithDataSetEx;",
            ">;"
        }
    .end annotation
.end field

.field private mAdapter:Lcom/mediatek/contacts/activities/ContactImportExportActivity$AccountListAdapter;

.field private mCellMgr:Lcom/mediatek/CellConnService/CellConnMgr;

.field private mCheckedAccount1:Lcom/mediatek/contacts/model/AccountWithDataSetEx;

.field private mCheckedAccount2:Lcom/mediatek/contacts/model/AccountWithDataSetEx;

.field private mCheckedPosition:I

.field private mIsFirstEntry:Z

.field private mListItemObjectList:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/mediatek/contacts/activities/ContactImportExportActivity$ListViewItemObject;",
            ">;"
        }
    .end annotation
.end field

.field private mListView:Landroid/widget/ListView;

.field private mServiceComplete:Ljava/lang/Runnable;

.field private mShowingStep:I

.field private mWaitingDialog:Landroid/app/ProgressDialog;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const-class v0, Lcom/mediatek/contacts/activities/ContactImportExportActivity;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/mediatek/contacts/activities/ContactImportExportActivity;->TAG:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 2

    const/4 v0, 0x0

    const/4 v1, 0x0

    invoke-direct {p0}, Landroid/app/Activity;-><init>()V

    iput-object v1, p0, Lcom/mediatek/contacts/activities/ContactImportExportActivity;->mAccounts:Ljava/util/List;

    iput v0, p0, Lcom/mediatek/contacts/activities/ContactImportExportActivity;->mShowingStep:I

    iput v0, p0, Lcom/mediatek/contacts/activities/ContactImportExportActivity;->mCheckedPosition:I

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/mediatek/contacts/activities/ContactImportExportActivity;->mIsFirstEntry:Z

    iput-object v1, p0, Lcom/mediatek/contacts/activities/ContactImportExportActivity;->mCheckedAccount1:Lcom/mediatek/contacts/model/AccountWithDataSetEx;

    iput-object v1, p0, Lcom/mediatek/contacts/activities/ContactImportExportActivity;->mCheckedAccount2:Lcom/mediatek/contacts/model/AccountWithDataSetEx;

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/mediatek/contacts/activities/ContactImportExportActivity;->mListItemObjectList:Ljava/util/List;

    iput-object v1, p0, Lcom/mediatek/contacts/activities/ContactImportExportActivity;->mAdapter:Lcom/mediatek/contacts/activities/ContactImportExportActivity$AccountListAdapter;

    new-instance v0, Lcom/mediatek/contacts/activities/ContactImportExportActivity$1;

    invoke-direct {v0, p0}, Lcom/mediatek/contacts/activities/ContactImportExportActivity$1;-><init>(Lcom/mediatek/contacts/activities/ContactImportExportActivity;)V

    iput-object v0, p0, Lcom/mediatek/contacts/activities/ContactImportExportActivity;->mServiceComplete:Ljava/lang/Runnable;

    new-instance v0, Lcom/mediatek/CellConnService/CellConnMgr;

    iget-object v1, p0, Lcom/mediatek/contacts/activities/ContactImportExportActivity;->mServiceComplete:Ljava/lang/Runnable;

    invoke-direct {v0, v1}, Lcom/mediatek/CellConnService/CellConnMgr;-><init>(Ljava/lang/Runnable;)V

    iput-object v0, p0, Lcom/mediatek/contacts/activities/ContactImportExportActivity;->mCellMgr:Lcom/mediatek/CellConnService/CellConnMgr;

    return-void
.end method

.method static synthetic access$000()Ljava/lang/String;
    .locals 1

    sget-object v0, Lcom/mediatek/contacts/activities/ContactImportExportActivity;->TAG:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$100(Lcom/mediatek/contacts/activities/ContactImportExportActivity;)Landroid/app/ProgressDialog;
    .locals 1
    .param p0    # Lcom/mediatek/contacts/activities/ContactImportExportActivity;

    iget-object v0, p0, Lcom/mediatek/contacts/activities/ContactImportExportActivity;->mWaitingDialog:Landroid/app/ProgressDialog;

    return-object v0
.end method

.method static synthetic access$1000(Lcom/mediatek/contacts/activities/ContactImportExportActivity;I)V
    .locals 0
    .param p0    # Lcom/mediatek/contacts/activities/ContactImportExportActivity;
    .param p1    # I

    invoke-direct {p0, p1}, Lcom/mediatek/contacts/activities/ContactImportExportActivity;->setCheckedAccount(I)V

    return-void
.end method

.method static synthetic access$102(Lcom/mediatek/contacts/activities/ContactImportExportActivity;Landroid/app/ProgressDialog;)Landroid/app/ProgressDialog;
    .locals 0
    .param p0    # Lcom/mediatek/contacts/activities/ContactImportExportActivity;
    .param p1    # Landroid/app/ProgressDialog;

    iput-object p1, p0, Lcom/mediatek/contacts/activities/ContactImportExportActivity;->mWaitingDialog:Landroid/app/ProgressDialog;

    return-object p1
.end method

.method static synthetic access$1100(Lcom/mediatek/contacts/activities/ContactImportExportActivity;)V
    .locals 0
    .param p0    # Lcom/mediatek/contacts/activities/ContactImportExportActivity;

    invoke-direct {p0}, Lcom/mediatek/contacts/activities/ContactImportExportActivity;->updateUi()V

    return-void
.end method

.method static synthetic access$1200(Lcom/mediatek/contacts/activities/ContactImportExportActivity;)Ljava/util/List;
    .locals 1
    .param p0    # Lcom/mediatek/contacts/activities/ContactImportExportActivity;

    iget-object v0, p0, Lcom/mediatek/contacts/activities/ContactImportExportActivity;->mListItemObjectList:Ljava/util/List;

    return-object v0
.end method

.method static synthetic access$200(Lcom/mediatek/contacts/activities/ContactImportExportActivity;)Lcom/mediatek/CellConnService/CellConnMgr;
    .locals 1
    .param p0    # Lcom/mediatek/contacts/activities/ContactImportExportActivity;

    iget-object v0, p0, Lcom/mediatek/contacts/activities/ContactImportExportActivity;->mCellMgr:Lcom/mediatek/CellConnService/CellConnMgr;

    return-object v0
.end method

.method static synthetic access$300(Lcom/mediatek/contacts/activities/ContactImportExportActivity;)V
    .locals 0
    .param p0    # Lcom/mediatek/contacts/activities/ContactImportExportActivity;

    invoke-direct {p0}, Lcom/mediatek/contacts/activities/ContactImportExportActivity;->handleImportExportAction()V

    return-void
.end method

.method static synthetic access$500(Landroid/content/Context;)Ljava/util/List;
    .locals 1
    .param p0    # Landroid/content/Context;

    invoke-static {p0}, Lcom/mediatek/contacts/activities/ContactImportExportActivity;->loadAccountFilters(Landroid/content/Context;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$600(Lcom/mediatek/contacts/activities/ContactImportExportActivity;)Ljava/util/List;
    .locals 1
    .param p0    # Lcom/mediatek/contacts/activities/ContactImportExportActivity;

    iget-object v0, p0, Lcom/mediatek/contacts/activities/ContactImportExportActivity;->mAccounts:Ljava/util/List;

    return-object v0
.end method

.method static synthetic access$602(Lcom/mediatek/contacts/activities/ContactImportExportActivity;Ljava/util/List;)Ljava/util/List;
    .locals 0
    .param p0    # Lcom/mediatek/contacts/activities/ContactImportExportActivity;
    .param p1    # Ljava/util/List;

    iput-object p1, p0, Lcom/mediatek/contacts/activities/ContactImportExportActivity;->mAccounts:Ljava/util/List;

    return-object p1
.end method

.method static synthetic access$700(Lcom/mediatek/contacts/activities/ContactImportExportActivity;)I
    .locals 1
    .param p0    # Lcom/mediatek/contacts/activities/ContactImportExportActivity;

    iget v0, p0, Lcom/mediatek/contacts/activities/ContactImportExportActivity;->mShowingStep:I

    return v0
.end method

.method static synthetic access$800(Lcom/mediatek/contacts/activities/ContactImportExportActivity;I)V
    .locals 0
    .param p0    # Lcom/mediatek/contacts/activities/ContactImportExportActivity;
    .param p1    # I

    invoke-direct {p0, p1}, Lcom/mediatek/contacts/activities/ContactImportExportActivity;->setShowingStep(I)V

    return-void
.end method

.method static synthetic access$900(Lcom/mediatek/contacts/activities/ContactImportExportActivity;)I
    .locals 1
    .param p0    # Lcom/mediatek/contacts/activities/ContactImportExportActivity;

    iget v0, p0, Lcom/mediatek/contacts/activities/ContactImportExportActivity;->mCheckedPosition:I

    return v0
.end method

.method private checkSDCardAvaliable(Ljava/lang/String;)Z
    .locals 5
    .param p1    # Ljava/lang/String;

    if-nez p1, :cond_0

    const/4 v3, 0x0

    :goto_0
    return v3

    :cond_0
    const-string v2, ""

    :try_start_0
    const-string v3, "mount"

    invoke-static {v3}, Landroid/os/ServiceManager;->getService(Ljava/lang/String;)Landroid/os/IBinder;

    move-result-object v3

    invoke-static {v3}, Landroid/os/storage/IMountService$Stub;->asInterface(Landroid/os/IBinder;)Landroid/os/storage/IMountService;

    move-result-object v0

    invoke-interface {v0, p1}, Landroid/os/storage/IMountService;->getVolumeState(Ljava/lang/String;)Ljava/lang/String;
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v2

    :goto_1
    const-string v3, "mounted"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    goto :goto_0

    :catch_0
    move-exception v1

    sget-object v3, Lcom/mediatek/contacts/activities/ContactImportExportActivity;->TAG:Ljava/lang/String;

    invoke-virtual {v1}, Landroid/os/RemoteException;->getStackTrace()[Ljava/lang/StackTraceElement;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1
.end method

.method private getCheckedAccountPosition(Lcom/mediatek/contacts/model/AccountWithDataSetEx;)I
    .locals 3
    .param p1    # Lcom/mediatek/contacts/model/AccountWithDataSetEx;

    const/4 v0, 0x0

    :goto_0
    iget-object v2, p0, Lcom/mediatek/contacts/activities/ContactImportExportActivity;->mListItemObjectList:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    if-ge v0, v2, :cond_1

    iget-object v2, p0, Lcom/mediatek/contacts/activities/ContactImportExportActivity;->mListItemObjectList:Ljava/util/List;

    invoke-interface {v2, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/mediatek/contacts/activities/ContactImportExportActivity$ListViewItemObject;

    iget-object v2, v1, Lcom/mediatek/contacts/activities/ContactImportExportActivity$ListViewItemObject;->mAccount:Lcom/mediatek/contacts/model/AccountWithDataSetEx;

    if-ne v2, p1, :cond_0

    :goto_1
    return v0

    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_1
    const/4 v0, 0x0

    goto :goto_1
.end method

.method private handleImportExportAction()V
    .locals 8

    const v7, 0x7f0c004d

    const v6, 0x104000a

    const v5, 0x1010355

    const v4, 0x1b207

    const/4 v3, 0x1

    iget-object v1, p0, Lcom/mediatek/contacts/activities/ContactImportExportActivity;->mCheckedAccount1:Lcom/mediatek/contacts/model/AccountWithDataSetEx;

    invoke-static {v1}, Lcom/mediatek/contacts/activities/ContactImportExportActivity;->isStorageAccount(Landroid/accounts/Account;)Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/mediatek/contacts/activities/ContactImportExportActivity;->mCheckedAccount1:Lcom/mediatek/contacts/model/AccountWithDataSetEx;

    iget-object v1, v1, Lcom/mediatek/contacts/model/AccountWithDataSetEx;->dataSet:Ljava/lang/String;

    invoke-direct {p0, v1}, Lcom/mediatek/contacts/activities/ContactImportExportActivity;->checkSDCardAvaliable(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_1

    :cond_0
    iget-object v1, p0, Lcom/mediatek/contacts/activities/ContactImportExportActivity;->mCheckedAccount2:Lcom/mediatek/contacts/model/AccountWithDataSetEx;

    invoke-static {v1}, Lcom/mediatek/contacts/activities/ContactImportExportActivity;->isStorageAccount(Landroid/accounts/Account;)Z

    move-result v1

    if-eqz v1, :cond_3

    iget-object v1, p0, Lcom/mediatek/contacts/activities/ContactImportExportActivity;->mCheckedAccount2:Lcom/mediatek/contacts/model/AccountWithDataSetEx;

    iget-object v1, v1, Lcom/mediatek/contacts/model/AccountWithDataSetEx;->dataSet:Ljava/lang/String;

    invoke-direct {p0, v1}, Lcom/mediatek/contacts/activities/ContactImportExportActivity;->checkSDCardAvaliable(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_3

    :cond_1
    new-instance v1, Landroid/app/AlertDialog$Builder;

    invoke-direct {v1, p0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    const v2, 0x7f0c019a

    invoke-virtual {v1, v2}, Landroid/app/AlertDialog$Builder;->setMessage(I)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    const v2, 0x7f0c000d

    invoke-virtual {v1, v2}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    invoke-virtual {v1, v5}, Landroid/app/AlertDialog$Builder;->setIconAttribute(I)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    new-instance v2, Lcom/mediatek/contacts/activities/ContactImportExportActivity$2;

    invoke-direct {v2, p0}, Lcom/mediatek/contacts/activities/ContactImportExportActivity$2;-><init>(Lcom/mediatek/contacts/activities/ContactImportExportActivity;)V

    invoke-virtual {v1, v6, v2}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    invoke-virtual {v1}, Landroid/app/AlertDialog$Builder;->show()Landroid/app/AlertDialog;

    :cond_2
    :goto_0
    return-void

    :cond_3
    iget-object v1, p0, Lcom/mediatek/contacts/activities/ContactImportExportActivity;->mCheckedAccount1:Lcom/mediatek/contacts/model/AccountWithDataSetEx;

    invoke-static {v1}, Lcom/mediatek/contacts/activities/ContactImportExportActivity;->isStorageAccount(Landroid/accounts/Account;)Z

    move-result v1

    if-eqz v1, :cond_4

    iget-object v1, p0, Lcom/mediatek/contacts/activities/ContactImportExportActivity;->mCheckedAccount2:Lcom/mediatek/contacts/model/AccountWithDataSetEx;

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/mediatek/contacts/activities/ContactImportExportActivity;->mCheckedAccount1:Lcom/mediatek/contacts/model/AccountWithDataSetEx;

    iget-object v1, v1, Lcom/mediatek/contacts/model/AccountWithDataSetEx;->dataSet:Ljava/lang/String;

    iget-object v2, p0, Lcom/mediatek/contacts/activities/ContactImportExportActivity;->mCheckedAccount2:Lcom/mediatek/contacts/model/AccountWithDataSetEx;

    invoke-static {p0, v1, v2}, Lcom/android/contacts/util/AccountSelectionUtil;->doImportFromSdCard(Landroid/content/Context;Ljava/lang/String;Lcom/android/contacts/model/account/AccountWithDataSet;)V

    goto :goto_0

    :cond_4
    iget-object v1, p0, Lcom/mediatek/contacts/activities/ContactImportExportActivity;->mCheckedAccount2:Lcom/mediatek/contacts/model/AccountWithDataSetEx;

    invoke-static {v1}, Lcom/mediatek/contacts/activities/ContactImportExportActivity;->isStorageAccount(Landroid/accounts/Account;)Z

    move-result v1

    if-eqz v1, :cond_6

    iget-object v1, p0, Lcom/mediatek/contacts/activities/ContactImportExportActivity;->mCheckedAccount2:Lcom/mediatek/contacts/model/AccountWithDataSetEx;

    iget-object v1, v1, Lcom/mediatek/contacts/model/AccountWithDataSetEx;->dataSet:Ljava/lang/String;

    invoke-direct {p0, v1}, Lcom/mediatek/contacts/activities/ContactImportExportActivity;->isSDCardFull(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_5

    sget-object v1, Lcom/mediatek/contacts/activities/ContactImportExportActivity;->TAG:Ljava/lang/String;

    const-string v2, "[handleImportExportAction] isSDCardFull"

    invoke-static {v1, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    new-instance v1, Landroid/app/AlertDialog$Builder;

    invoke-direct {v1, p0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    invoke-virtual {v1, v7}, Landroid/app/AlertDialog$Builder;->setMessage(I)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    invoke-virtual {v1, v7}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    invoke-virtual {v1, v5}, Landroid/app/AlertDialog$Builder;->setIconAttribute(I)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    new-instance v2, Lcom/mediatek/contacts/activities/ContactImportExportActivity$3;

    invoke-direct {v2, p0}, Lcom/mediatek/contacts/activities/ContactImportExportActivity$3;-><init>(Lcom/mediatek/contacts/activities/ContactImportExportActivity;)V

    invoke-virtual {v1, v6, v2}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    invoke-virtual {v1}, Landroid/app/AlertDialog$Builder;->show()Landroid/app/AlertDialog;

    goto :goto_0

    :cond_5
    new-instance v1, Landroid/content/Intent;

    const-class v2, Lcom/mediatek/contacts/list/ContactListMultiChoiceActivity;

    invoke-direct {v1, p0, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const-string v2, "android.intent.action.contacts.list.PICKMULTICONTACTS"

    invoke-virtual {v1, v2}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v1

    const-string v2, "request_type"

    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    move-result-object v1

    const-string v2, "toSDCard"

    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    move-result-object v1

    const-string v2, "fromaccount"

    iget-object v3, p0, Lcom/mediatek/contacts/activities/ContactImportExportActivity;->mCheckedAccount1:Lcom/mediatek/contacts/model/AccountWithDataSetEx;

    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    move-result-object v1

    const-string v2, "toaccount"

    iget-object v3, p0, Lcom/mediatek/contacts/activities/ContactImportExportActivity;->mCheckedAccount2:Lcom/mediatek/contacts/model/AccountWithDataSetEx;

    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {p0, v0, v4}, Lcom/mediatek/contacts/activities/ContactImportExportActivity;->startActivityForResult(Landroid/content/Intent;I)V

    goto :goto_0

    :cond_6
    new-instance v1, Landroid/content/Intent;

    const-class v2, Lcom/mediatek/contacts/list/ContactListMultiChoiceActivity;

    invoke-direct {v1, p0, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const-string v2, "android.intent.action.contacts.list.PICKMULTICONTACTS"

    invoke-virtual {v1, v2}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v1

    const-string v2, "request_type"

    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    move-result-object v1

    const-string v2, "toSDCard"

    const/4 v3, 0x0

    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    move-result-object v1

    const-string v2, "fromaccount"

    iget-object v3, p0, Lcom/mediatek/contacts/activities/ContactImportExportActivity;->mCheckedAccount1:Lcom/mediatek/contacts/model/AccountWithDataSetEx;

    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    move-result-object v1

    const-string v2, "toaccount"

    iget-object v3, p0, Lcom/mediatek/contacts/activities/ContactImportExportActivity;->mCheckedAccount2:Lcom/mediatek/contacts/model/AccountWithDataSetEx;

    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {p0, v0, v4}, Lcom/mediatek/contacts/activities/ContactImportExportActivity;->startActivityForResult(Landroid/content/Intent;I)V

    goto/16 :goto_0
.end method

.method private isSDCardFull(Ljava/lang/String;)Z
    .locals 9
    .param p1    # Ljava/lang/String;

    const/4 v4, 0x1

    const/4 v5, 0x0

    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v6

    if-eqz v6, :cond_0

    :goto_0
    return v5

    :cond_0
    sget-object v6, Lcom/mediatek/contacts/activities/ContactImportExportActivity;->TAG:Ljava/lang/String;

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "isSDCardFull storage path is "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    invoke-direct {p0, p1}, Lcom/mediatek/contacts/activities/ContactImportExportActivity;->checkSDCardAvaliable(Ljava/lang/String;)Z

    move-result v6

    if-eqz v6, :cond_3

    const/4 v3, 0x0

    :try_start_0
    new-instance v3, Landroid/os/StatFs;

    invoke-direct {v3, p1}, Landroid/os/StatFs;-><init>(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0

    if-nez v3, :cond_1

    sget-object v4, Lcom/mediatek/contacts/activities/ContactImportExportActivity;->TAG:Ljava/lang/String;

    const-string v6, "isSDCardFull sf is null "

    invoke-static {v4, v6}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    :catch_0
    move-exception v2

    invoke-virtual {v2}, Ljava/lang/IllegalArgumentException;->printStackTrace()V

    sget-object v4, Lcom/mediatek/contacts/activities/ContactImportExportActivity;->TAG:Ljava/lang/String;

    invoke-virtual {v2}, Ljava/lang/IllegalArgumentException;->getMessage()Ljava/lang/String;

    move-result-object v6

    invoke-static {v4, v6}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    :cond_1
    invoke-virtual {v3}, Landroid/os/StatFs;->getAvailableBlocks()I

    move-result v6

    int-to-long v0, v6

    const-wide/16 v6, 0x0

    cmp-long v6, v0, v6

    if-gtz v6, :cond_2

    :goto_1
    move v5, v4

    goto :goto_0

    :cond_2
    move v4, v5

    goto :goto_1

    :cond_3
    move v5, v4

    goto :goto_0
.end method

.method private static isStorageAccount(Landroid/accounts/Account;)Z
    .locals 2
    .param p0    # Landroid/accounts/Account;

    if-eqz p0, :cond_0

    iget-object v0, p0, Landroid/accounts/Account;->type:Ljava/lang/String;

    const-string v1, "_STORAGE_ACCOUNT"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private static loadAccountFilters(Landroid/content/Context;)Ljava/util/List;
    .locals 10
    .param p0    # Landroid/content/Context;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            ")",
            "Ljava/util/List",
            "<",
            "Lcom/mediatek/contacts/model/AccountWithDataSetEx;",
            ">;"
        }
    .end annotation

    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    invoke-static {p0}, Lcom/android/contacts/model/AccountTypeManager;->getInstance(Landroid/content/Context;)Lcom/android/contacts/model/AccountTypeManager;

    move-result-object v2

    const/4 v7, 0x1

    invoke-virtual {v2, v7}, Lcom/android/contacts/model/AccountTypeManager;->getAccounts(Z)Ljava/util/List;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v5

    :cond_0
    :goto_0
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v7

    if-eqz v7, :cond_3

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/contacts/model/account/AccountWithDataSet;

    iget-object v7, v0, Lcom/android/contacts/model/account/AccountWithDataSet;->type:Ljava/lang/String;

    iget-object v8, v0, Lcom/android/contacts/model/account/AccountWithDataSet;->dataSet:Ljava/lang/String;

    invoke-virtual {v2, v7, v8}, Lcom/android/contacts/model/AccountTypeManager;->getAccountType(Ljava/lang/String;Ljava/lang/String;)Lcom/android/contacts/model/account/AccountType;

    move-result-object v1

    invoke-virtual {v1}, Lcom/android/contacts/model/account/AccountType;->isExtension()Z

    move-result v7

    if-eqz v7, :cond_1

    invoke-virtual {v0, p0}, Lcom/android/contacts/model/account/AccountWithDataSet;->hasData(Landroid/content/Context;)Z

    move-result v7

    if-eqz v7, :cond_0

    :cond_1
    invoke-static {}, Lcom/mediatek/contacts/simcontact/SlotUtils;->getFirstSlotId()I

    move-result v6

    instance-of v7, v0, Lcom/mediatek/contacts/model/AccountWithDataSetEx;

    if-eqz v7, :cond_2

    move-object v7, v0

    check-cast v7, Lcom/mediatek/contacts/model/AccountWithDataSetEx;

    invoke-virtual {v7}, Lcom/mediatek/contacts/model/AccountWithDataSetEx;->getSlotId()I

    move-result v6

    :cond_2
    new-instance v7, Lcom/mediatek/contacts/model/AccountWithDataSetEx;

    iget-object v8, v0, Lcom/android/contacts/model/account/AccountWithDataSet;->name:Ljava/lang/String;

    iget-object v9, v0, Lcom/android/contacts/model/account/AccountWithDataSet;->type:Ljava/lang/String;

    invoke-direct {v7, v8, v9, v6}, Lcom/mediatek/contacts/model/AccountWithDataSetEx;-><init>(Ljava/lang/String;Ljava/lang/String;I)V

    invoke-interface {v4, v7}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_3
    return-object v4
.end method

.method private onBackAction()V
    .locals 2

    const/4 v0, 0x0

    const/4 v1, 0x1

    invoke-direct {p0, v1}, Lcom/mediatek/contacts/activities/ContactImportExportActivity;->setShowingStep(I)V

    iget-object v1, p0, Lcom/mediatek/contacts/activities/ContactImportExportActivity;->mCheckedAccount1:Lcom/mediatek/contacts/model/AccountWithDataSetEx;

    invoke-direct {p0, v1}, Lcom/mediatek/contacts/activities/ContactImportExportActivity;->getCheckedAccountPosition(Lcom/mediatek/contacts/model/AccountWithDataSetEx;)I

    move-result v0

    iput v0, p0, Lcom/mediatek/contacts/activities/ContactImportExportActivity;->mCheckedPosition:I

    iget v1, p0, Lcom/mediatek/contacts/activities/ContactImportExportActivity;->mCheckedPosition:I

    invoke-direct {p0, v1}, Lcom/mediatek/contacts/activities/ContactImportExportActivity;->setCheckedAccount(I)V

    invoke-direct {p0}, Lcom/mediatek/contacts/activities/ContactImportExportActivity;->updateUi()V

    return-void
.end method

.method private onNextAction()V
    .locals 3

    const/4 v2, 0x2

    const/4 v0, 0x0

    iget v1, p0, Lcom/mediatek/contacts/activities/ContactImportExportActivity;->mShowingStep:I

    if-lt v1, v2, :cond_0

    invoke-virtual {p0}, Lcom/mediatek/contacts/activities/ContactImportExportActivity;->doImportExport()V

    :goto_0
    return-void

    :cond_0
    invoke-direct {p0, v2}, Lcom/mediatek/contacts/activities/ContactImportExportActivity;->setShowingStep(I)V

    iget-boolean v1, p0, Lcom/mediatek/contacts/activities/ContactImportExportActivity;->mIsFirstEntry:Z

    if-nez v1, :cond_1

    iget-object v1, p0, Lcom/mediatek/contacts/activities/ContactImportExportActivity;->mCheckedAccount1:Lcom/mediatek/contacts/model/AccountWithDataSetEx;

    if-nez v1, :cond_2

    iget-object v1, p0, Lcom/mediatek/contacts/activities/ContactImportExportActivity;->mCheckedAccount2:Lcom/mediatek/contacts/model/AccountWithDataSetEx;

    if-nez v1, :cond_2

    :cond_1
    const/4 v0, 0x0

    :goto_1
    const/4 v1, 0x0

    iput-boolean v1, p0, Lcom/mediatek/contacts/activities/ContactImportExportActivity;->mIsFirstEntry:Z

    iput v0, p0, Lcom/mediatek/contacts/activities/ContactImportExportActivity;->mCheckedPosition:I

    iget v1, p0, Lcom/mediatek/contacts/activities/ContactImportExportActivity;->mCheckedPosition:I

    invoke-direct {p0, v1}, Lcom/mediatek/contacts/activities/ContactImportExportActivity;->setCheckedAccount(I)V

    invoke-direct {p0}, Lcom/mediatek/contacts/activities/ContactImportExportActivity;->updateUi()V

    goto :goto_0

    :cond_2
    iget-object v1, p0, Lcom/mediatek/contacts/activities/ContactImportExportActivity;->mCheckedAccount2:Lcom/mediatek/contacts/model/AccountWithDataSetEx;

    invoke-direct {p0, v1}, Lcom/mediatek/contacts/activities/ContactImportExportActivity;->getCheckedAccountPosition(Lcom/mediatek/contacts/model/AccountWithDataSetEx;)I

    move-result v0

    goto :goto_1
.end method

.method private setButtonState(Z)V
    .locals 4
    .param p1    # Z

    const/4 v2, 0x1

    const/4 v1, 0x0

    const v0, 0x7f07014b

    invoke-virtual {p0, v0}, Lcom/mediatek/contacts/activities/ContactImportExportActivity;->findViewById(I)Landroid/view/View;

    move-result-object v3

    if-eqz p1, :cond_1

    iget v0, p0, Lcom/mediatek/contacts/activities/ContactImportExportActivity;->mShowingStep:I

    if-le v0, v2, :cond_1

    move v0, v1

    :goto_0
    invoke-virtual {v3, v0}, Landroid/view/View;->setVisibility(I)V

    const v0, 0x7f07014c

    invoke-virtual {p0, v0}, Lcom/mediatek/contacts/activities/ContactImportExportActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    if-eqz p1, :cond_0

    iget v3, p0, Lcom/mediatek/contacts/activities/ContactImportExportActivity;->mShowingStep:I

    if-lez v3, :cond_0

    move v1, v2

    :cond_0
    invoke-virtual {v0, v1}, Landroid/view/View;->setEnabled(Z)V

    return-void

    :cond_1
    const/16 v0, 0x8

    goto :goto_0
.end method

.method private setCheckedAccount(I)V
    .locals 2
    .param p1    # I

    iget v0, p0, Lcom/mediatek/contacts/activities/ContactImportExportActivity;->mShowingStep:I

    const/4 v1, 0x1

    if-ne v0, v1, :cond_1

    iget-object v0, p0, Lcom/mediatek/contacts/activities/ContactImportExportActivity;->mListItemObjectList:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/mediatek/contacts/activities/ContactImportExportActivity$ListViewItemObject;

    iget-object v0, v0, Lcom/mediatek/contacts/activities/ContactImportExportActivity$ListViewItemObject;->mAccount:Lcom/mediatek/contacts/model/AccountWithDataSetEx;

    iput-object v0, p0, Lcom/mediatek/contacts/activities/ContactImportExportActivity;->mCheckedAccount1:Lcom/mediatek/contacts/model/AccountWithDataSetEx;

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget v0, p0, Lcom/mediatek/contacts/activities/ContactImportExportActivity;->mShowingStep:I

    const/4 v1, 0x2

    if-ne v0, v1, :cond_0

    iget-object v0, p0, Lcom/mediatek/contacts/activities/ContactImportExportActivity;->mListItemObjectList:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/mediatek/contacts/activities/ContactImportExportActivity$ListViewItemObject;

    iget-object v0, v0, Lcom/mediatek/contacts/activities/ContactImportExportActivity$ListViewItemObject;->mAccount:Lcom/mediatek/contacts/model/AccountWithDataSetEx;

    iput-object v0, p0, Lcom/mediatek/contacts/activities/ContactImportExportActivity;->mCheckedAccount2:Lcom/mediatek/contacts/model/AccountWithDataSetEx;

    goto :goto_0
.end method

.method private setCheckedPosition(I)V
    .locals 2
    .param p1    # I

    iget v0, p0, Lcom/mediatek/contacts/activities/ContactImportExportActivity;->mCheckedPosition:I

    if-eq v0, p1, :cond_0

    iget v0, p0, Lcom/mediatek/contacts/activities/ContactImportExportActivity;->mCheckedPosition:I

    const/4 v1, 0x0

    invoke-direct {p0, v0, v1}, Lcom/mediatek/contacts/activities/ContactImportExportActivity;->setListViewItemChecked(IZ)V

    iput p1, p0, Lcom/mediatek/contacts/activities/ContactImportExportActivity;->mCheckedPosition:I

    iget v0, p0, Lcom/mediatek/contacts/activities/ContactImportExportActivity;->mCheckedPosition:I

    const/4 v1, 0x1

    invoke-direct {p0, v0, v1}, Lcom/mediatek/contacts/activities/ContactImportExportActivity;->setListViewItemChecked(IZ)V

    :cond_0
    return-void
.end method

.method private setListViewItemChecked(IZ)V
    .locals 2
    .param p1    # I
    .param p2    # Z

    const/4 v1, -0x1

    if-le p1, v1, :cond_0

    iget-object v1, p0, Lcom/mediatek/contacts/activities/ContactImportExportActivity;->mListItemObjectList:Ljava/util/List;

    invoke-interface {v1, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/mediatek/contacts/activities/ContactImportExportActivity$ListViewItemObject;

    iget-object v1, v0, Lcom/mediatek/contacts/activities/ContactImportExportActivity$ListViewItemObject;->mView:Lcom/mediatek/contacts/widget/ImportExportItem;

    if-eqz v1, :cond_0

    iget-object v1, v0, Lcom/mediatek/contacts/activities/ContactImportExportActivity$ListViewItemObject;->mView:Lcom/mediatek/contacts/widget/ImportExportItem;

    invoke-virtual {v1, p2}, Lcom/mediatek/contacts/widget/ImportExportItem;->setActivated(Z)V

    :cond_0
    return-void
.end method

.method private setShowingStep(I)V
    .locals 8
    .param p1    # I

    const v7, 0x7f07014a

    iput p1, p0, Lcom/mediatek/contacts/activities/ContactImportExportActivity;->mShowingStep:I

    iget-object v5, p0, Lcom/mediatek/contacts/activities/ContactImportExportActivity;->mListItemObjectList:Ljava/util/List;

    invoke-interface {v5}, Ljava/util/List;->clear()V

    const v5, 0x7f0700df

    invoke-virtual {p0, v5}, Lcom/mediatek/contacts/activities/ContactImportExportActivity;->findViewById(I)Landroid/view/View;

    move-result-object v5

    check-cast v5, Landroid/widget/LinearLayout;

    const/4 v6, 0x0

    invoke-virtual {v5, v6}, Landroid/widget/LinearLayout;->setVisibility(I)V

    iget v5, p0, Lcom/mediatek/contacts/activities/ContactImportExportActivity;->mShowingStep:I

    const/4 v6, 0x1

    if-ne v5, v6, :cond_0

    invoke-virtual {p0, v7}, Lcom/mediatek/contacts/activities/ContactImportExportActivity;->findViewById(I)Landroid/view/View;

    move-result-object v5

    check-cast v5, Landroid/widget/TextView;

    const v6, 0x7f0c0071

    invoke-virtual {v5, v6}, Landroid/widget/TextView;->setText(I)V

    iget-object v5, p0, Lcom/mediatek/contacts/activities/ContactImportExportActivity;->mAccounts:Ljava/util/List;

    invoke-interface {v5}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :goto_0
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_5

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/mediatek/contacts/model/AccountWithDataSetEx;

    iget-object v5, p0, Lcom/mediatek/contacts/activities/ContactImportExportActivity;->mListItemObjectList:Ljava/util/List;

    new-instance v6, Lcom/mediatek/contacts/activities/ContactImportExportActivity$ListViewItemObject;

    invoke-direct {v6, p0, v0}, Lcom/mediatek/contacts/activities/ContactImportExportActivity$ListViewItemObject;-><init>(Lcom/mediatek/contacts/activities/ContactImportExportActivity;Lcom/mediatek/contacts/model/AccountWithDataSetEx;)V

    invoke-interface {v5, v6}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_0
    iget v5, p0, Lcom/mediatek/contacts/activities/ContactImportExportActivity;->mShowingStep:I

    const/4 v6, 0x2

    if-ne v5, v6, :cond_5

    invoke-virtual {p0, v7}, Lcom/mediatek/contacts/activities/ContactImportExportActivity;->findViewById(I)Landroid/view/View;

    move-result-object v5

    check-cast v5, Landroid/widget/TextView;

    const v6, 0x7f0c0072

    invoke-virtual {v5, v6}, Landroid/widget/TextView;->setText(I)V

    iget-object v5, p0, Lcom/mediatek/contacts/activities/ContactImportExportActivity;->mAccounts:Ljava/util/List;

    invoke-interface {v5}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :cond_1
    :goto_1
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_5

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/mediatek/contacts/model/AccountWithDataSetEx;

    iget-object v5, p0, Lcom/mediatek/contacts/activities/ContactImportExportActivity;->mCheckedAccount1:Lcom/mediatek/contacts/model/AccountWithDataSetEx;

    if-eq v5, v0, :cond_1

    invoke-static {p0}, Lcom/android/contacts/model/AccountTypeManager;->getInstance(Landroid/content/Context;)Lcom/android/contacts/model/AccountTypeManager;

    move-result-object v2

    invoke-virtual {v0}, Lcom/mediatek/contacts/model/AccountWithDataSetEx;->getAccountTypeWithDataSet()Lcom/android/contacts/model/account/AccountTypeWithDataSet;

    move-result-object v5

    invoke-virtual {v2, v5}, Lcom/android/contacts/model/AccountTypeManager;->getAccountType(Lcom/android/contacts/model/account/AccountTypeWithDataSet;)Lcom/android/contacts/model/account/AccountType;

    move-result-object v1

    iget-object v5, p0, Lcom/mediatek/contacts/activities/ContactImportExportActivity;->mCheckedAccount1:Lcom/mediatek/contacts/model/AccountWithDataSetEx;

    invoke-virtual {v5}, Lcom/mediatek/contacts/model/AccountWithDataSetEx;->getAccountTypeWithDataSet()Lcom/android/contacts/model/account/AccountTypeWithDataSet;

    move-result-object v5

    invoke-virtual {v2, v5}, Lcom/android/contacts/model/AccountTypeManager;->getAccountType(Lcom/android/contacts/model/account/AccountTypeWithDataSet;)Lcom/android/contacts/model/account/AccountType;

    move-result-object v3

    iget-object v5, p0, Lcom/mediatek/contacts/activities/ContactImportExportActivity;->mCheckedAccount1:Lcom/mediatek/contacts/model/AccountWithDataSetEx;

    invoke-static {v5}, Lcom/mediatek/contacts/activities/ContactImportExportActivity;->isStorageAccount(Landroid/accounts/Account;)Z

    move-result v5

    if-eqz v5, :cond_2

    invoke-virtual {v1}, Lcom/android/contacts/model/account/AccountType;->isIccCardAccount()Z

    move-result v5

    if-nez v5, :cond_1

    :cond_2
    invoke-virtual {v3}, Lcom/android/contacts/model/account/AccountType;->isIccCardAccount()Z

    move-result v5

    if-eqz v5, :cond_3

    invoke-static {v0}, Lcom/mediatek/contacts/activities/ContactImportExportActivity;->isStorageAccount(Landroid/accounts/Account;)Z

    move-result v5

    if-nez v5, :cond_1

    :cond_3
    iget-object v5, p0, Lcom/mediatek/contacts/activities/ContactImportExportActivity;->mCheckedAccount1:Lcom/mediatek/contacts/model/AccountWithDataSetEx;

    invoke-static {v5}, Lcom/mediatek/contacts/activities/ContactImportExportActivity;->isStorageAccount(Landroid/accounts/Account;)Z

    move-result v5

    if-eqz v5, :cond_4

    invoke-static {v0}, Lcom/mediatek/contacts/activities/ContactImportExportActivity;->isStorageAccount(Landroid/accounts/Account;)Z

    move-result v5

    if-nez v5, :cond_1

    :cond_4
    iget-object v5, p0, Lcom/mediatek/contacts/activities/ContactImportExportActivity;->mListItemObjectList:Ljava/util/List;

    new-instance v6, Lcom/mediatek/contacts/activities/ContactImportExportActivity$ListViewItemObject;

    invoke-direct {v6, p0, v0}, Lcom/mediatek/contacts/activities/ContactImportExportActivity$ListViewItemObject;-><init>(Lcom/mediatek/contacts/activities/ContactImportExportActivity;Lcom/mediatek/contacts/model/AccountWithDataSetEx;)V

    invoke-interface {v5, v6}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_1

    :cond_5
    return-void
.end method

.method private updateUi()V
    .locals 2

    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/mediatek/contacts/activities/ContactImportExportActivity;->setButtonState(Z)V

    iget-object v0, p0, Lcom/mediatek/contacts/activities/ContactImportExportActivity;->mListView:Landroid/widget/ListView;

    iget-object v1, p0, Lcom/mediatek/contacts/activities/ContactImportExportActivity;->mAdapter:Lcom/mediatek/contacts/activities/ContactImportExportActivity$AccountListAdapter;

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    return-void
.end method


# virtual methods
.method public doImportExport()V
    .locals 6

    const/4 v5, 0x1

    invoke-static {}, Lcom/mediatek/contacts/simcontact/SlotUtils;->getNonSlotId()I

    move-result v1

    iget-object v2, p0, Lcom/mediatek/contacts/activities/ContactImportExportActivity;->mCheckedAccount1:Lcom/mediatek/contacts/model/AccountWithDataSetEx;

    iget-object v2, v2, Lcom/mediatek/contacts/model/AccountWithDataSetEx;->type:Ljava/lang/String;

    invoke-static {v2}, Lcom/android/contacts/model/account/AccountType;->isAccountTypeIccCard(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_1

    iget-object v2, p0, Lcom/mediatek/contacts/activities/ContactImportExportActivity;->mCheckedAccount1:Lcom/mediatek/contacts/model/AccountWithDataSetEx;

    invoke-virtual {v2}, Lcom/mediatek/contacts/model/AccountWithDataSetEx;->getSlotId()I

    move-result v1

    iget-object v2, p0, Lcom/mediatek/contacts/activities/ContactImportExportActivity;->mCellMgr:Lcom/mediatek/CellConnService/CellConnMgr;

    const/16 v3, 0x130

    invoke-virtual {v2, v1, v3}, Lcom/mediatek/CellConnService/CellConnMgr;->handleCellConn(II)I

    move-result v0

    sget-object v2, Lcom/mediatek/contacts/activities/ContactImportExportActivity;->TAG:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "[doImportExport] slot : "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ", nRet is "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-static {v0}, Lcom/mediatek/CellConnService/CellConnMgr;->resultToString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    if-ne v0, v5, :cond_0

    iget-object v2, p0, Lcom/mediatek/contacts/activities/ContactImportExportActivity;->mWaitingDialog:Landroid/app/ProgressDialog;

    if-nez v2, :cond_0

    const-string v2, ""

    const v3, 0x7f0c005b

    invoke-virtual {p0, v3}, Lcom/mediatek/contacts/activities/ContactImportExportActivity;->getString(I)Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x0

    invoke-static {p0, v2, v3, v5, v4}, Landroid/app/ProgressDialog;->show(Landroid/content/Context;Ljava/lang/CharSequence;Ljava/lang/CharSequence;ZZ)Landroid/app/ProgressDialog;

    move-result-object v2

    iput-object v2, p0, Lcom/mediatek/contacts/activities/ContactImportExportActivity;->mWaitingDialog:Landroid/app/ProgressDialog;

    :cond_0
    :goto_0
    return-void

    :cond_1
    invoke-direct {p0}, Lcom/mediatek/contacts/activities/ContactImportExportActivity;->handleImportExportAction()V

    goto :goto_0
.end method

.method public getDirectory(Ljava/lang/String;Ljava/lang/String;)Ljava/io/File;
    .locals 3
    .param p1    # Ljava/lang/String;
    .param p2    # Ljava/lang/String;

    const-string v0, "getDirectory"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "path : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    if-nez p1, :cond_0

    new-instance v0, Ljava/io/File;

    invoke-direct {v0, p2}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    :goto_0
    return-object v0

    :cond_0
    new-instance v0, Ljava/io/File;

    invoke-direct {v0, p1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public getStorageAccounts()Ljava/util/List;
    .locals 11
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/mediatek/contacts/model/AccountWithDataSetEx;",
            ">;"
        }
    .end annotation

    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    invoke-virtual {p0}, Lcom/mediatek/contacts/activities/ContactImportExportActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v7

    const-string v8, "storage"

    invoke-virtual {v7, v8}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/os/storage/StorageManager;

    if-nez v3, :cond_1

    :cond_0
    return-object v4

    :cond_1
    invoke-virtual {v3}, Landroid/os/storage/StorageManager;->getVolumeList()[Landroid/os/storage/StorageVolume;

    move-result-object v6

    if-eqz v6, :cond_0

    sget-object v7, Lcom/mediatek/contacts/activities/ContactImportExportActivity;->TAG:Ljava/lang/String;

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "volumes are "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    move-object v0, v6

    array-length v2, v0

    const/4 v1, 0x0

    :goto_0
    if-ge v1, v2, :cond_0

    aget-object v5, v0, v1

    sget-object v7, Lcom/mediatek/contacts/activities/ContactImportExportActivity;->TAG:Ljava/lang/String;

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "volume is "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    invoke-virtual {v5}, Landroid/os/storage/StorageVolume;->getPath()Ljava/lang/String;

    move-result-object v7

    const-string v8, "/mnt/usbotg"

    invoke-virtual {v7, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-nez v7, :cond_2

    invoke-virtual {v5}, Landroid/os/storage/StorageVolume;->getPath()Ljava/lang/String;

    move-result-object v7

    invoke-direct {p0, v7}, Lcom/mediatek/contacts/activities/ContactImportExportActivity;->checkSDCardAvaliable(Ljava/lang/String;)Z

    move-result v7

    if-nez v7, :cond_3

    :cond_2
    :goto_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_3
    new-instance v7, Lcom/mediatek/contacts/model/AccountWithDataSetEx;

    invoke-virtual {v5, p0}, Landroid/os/storage/StorageVolume;->getDescription(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v8

    const-string v9, "_STORAGE_ACCOUNT"

    invoke-virtual {v5}, Landroid/os/storage/StorageVolume;->getPath()Ljava/lang/String;

    move-result-object v10

    invoke-direct {v7, v8, v9, v10}, Lcom/mediatek/contacts/model/AccountWithDataSetEx;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v4, v7}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_1
.end method

.method protected onActivityResult(IILandroid/content/Intent;)V
    .locals 1
    .param p1    # I
    .param p2    # I
    .param p3    # Landroid/content/Intent;

    invoke-super {p0, p1, p2, p3}, Landroid/app/Activity;->onActivityResult(IILandroid/content/Intent;)V

    const v0, 0x1b207

    if-ne p1, v0, :cond_0

    const v0, 0x1b208

    if-ne p2, v0, :cond_0

    invoke-virtual {p0}, Lcom/mediatek/contacts/activities/ContactImportExportActivity;->finish()V

    :cond_0
    return-void
.end method

.method public onBackPressed()V
    .locals 2

    iget v0, p0, Lcom/mediatek/contacts/activities/ContactImportExportActivity;->mShowingStep:I

    const/4 v1, 0x1

    if-le v0, v1, :cond_0

    invoke-direct {p0}, Lcom/mediatek/contacts/activities/ContactImportExportActivity;->onBackAction()V

    :goto_0
    return-void

    :cond_0
    invoke-super {p0}, Landroid/app/Activity;->onBackPressed()V

    goto :goto_0
.end method

.method public onClick(Landroid/view/View;)V
    .locals 3
    .param p1    # Landroid/view/View;

    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v1

    packed-switch v1, :pswitch_data_0

    :goto_0
    return-void

    :pswitch_0
    const/4 v0, 0x0

    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v1

    const v2, 0x7f07014c

    if-ne v1, v2, :cond_0

    invoke-direct {p0}, Lcom/mediatek/contacts/activities/ContactImportExportActivity;->onNextAction()V

    goto :goto_0

    :cond_0
    invoke-direct {p0}, Lcom/mediatek/contacts/activities/ContactImportExportActivity;->onBackAction()V

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x7f07014b
        :pswitch_0
        :pswitch_0
    .end packed-switch
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 5
    .param p1    # Landroid/os/Bundle;

    const/4 v4, 0x0

    invoke-super {p0, p1}, Landroid/app/Activity;->onCreate(Landroid/os/Bundle;)V

    const v1, 0x7f04007c

    invoke-virtual {p0, v1}, Lcom/mediatek/contacts/activities/ContactImportExportActivity;->setContentView(I)V

    const v1, 0x7f07014c

    invoke-virtual {p0, v1}, Lcom/mediatek/contacts/activities/ContactImportExportActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/Button;

    invoke-virtual {v1, p0}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    const v1, 0x7f07014b

    invoke-virtual {p0, v1}, Lcom/mediatek/contacts/activities/ContactImportExportActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/Button;

    invoke-virtual {v1, p0}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    const v1, 0x7f0700df

    invoke-virtual {p0, v1}, Lcom/mediatek/contacts/activities/ContactImportExportActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/LinearLayout;

    const/16 v2, 0x8

    invoke-virtual {v1, v2}, Landroid/widget/LinearLayout;->setVisibility(I)V

    const v1, 0x7f070117

    invoke-virtual {p0, v1}, Lcom/mediatek/contacts/activities/ContactImportExportActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ListView;

    iput-object v1, p0, Lcom/mediatek/contacts/activities/ContactImportExportActivity;->mListView:Landroid/widget/ListView;

    iget-object v1, p0, Lcom/mediatek/contacts/activities/ContactImportExportActivity;->mListView:Landroid/widget/ListView;

    invoke-virtual {v1, p0}, Landroid/widget/ListView;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    invoke-virtual {p0}, Lcom/mediatek/contacts/activities/ContactImportExportActivity;->getActionBar()Landroid/app/ActionBar;

    move-result-object v0

    if-eqz v0, :cond_0

    const/16 v1, 0xc

    const/16 v2, 0xe

    invoke-virtual {v0, v1, v2}, Landroid/app/ActionBar;->setDisplayOptions(II)V

    const v1, 0x7f0c0074

    invoke-virtual {v0, v1}, Landroid/app/ActionBar;->setTitle(I)V

    :cond_0
    new-instance v1, Lcom/mediatek/contacts/activities/ContactImportExportActivity$AccountListAdapter;

    invoke-direct {v1, p0, p0}, Lcom/mediatek/contacts/activities/ContactImportExportActivity$AccountListAdapter;-><init>(Lcom/mediatek/contacts/activities/ContactImportExportActivity;Landroid/content/Context;)V

    iput-object v1, p0, Lcom/mediatek/contacts/activities/ContactImportExportActivity;->mAdapter:Lcom/mediatek/contacts/activities/ContactImportExportActivity$AccountListAdapter;

    invoke-virtual {p0}, Lcom/mediatek/contacts/activities/ContactImportExportActivity;->getLoaderManager()Landroid/app/LoaderManager;

    move-result-object v1

    const/4 v2, 0x0

    new-instance v3, Lcom/mediatek/contacts/activities/ContactImportExportActivity$MyLoaderCallbacks;

    invoke-direct {v3, p0, v4}, Lcom/mediatek/contacts/activities/ContactImportExportActivity$MyLoaderCallbacks;-><init>(Lcom/mediatek/contacts/activities/ContactImportExportActivity;Lcom/mediatek/contacts/activities/ContactImportExportActivity$1;)V

    invoke-virtual {v1, v2, v4, v3}, Landroid/app/LoaderManager;->restartLoader(ILandroid/os/Bundle;Landroid/app/LoaderManager$LoaderCallbacks;)Landroid/content/Loader;

    iget-object v1, p0, Lcom/mediatek/contacts/activities/ContactImportExportActivity;->mCellMgr:Lcom/mediatek/CellConnService/CellConnMgr;

    invoke-virtual {v1, p0}, Lcom/mediatek/CellConnService/CellConnMgr;->register(Landroid/content/Context;)V

    return-void
.end method

.method protected onDestroy()V
    .locals 2

    iget-object v0, p0, Lcom/mediatek/contacts/activities/ContactImportExportActivity;->mCellMgr:Lcom/mediatek/CellConnService/CellConnMgr;

    invoke-virtual {v0}, Lcom/mediatek/CellConnService/CellConnMgr;->unregister()V

    iget-object v0, p0, Lcom/mediatek/contacts/activities/ContactImportExportActivity;->mWaitingDialog:Landroid/app/ProgressDialog;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/mediatek/contacts/activities/ContactImportExportActivity;->mWaitingDialog:Landroid/app/ProgressDialog;

    invoke-virtual {v0}, Landroid/app/ProgressDialog;->dismiss()V

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/mediatek/contacts/activities/ContactImportExportActivity;->mWaitingDialog:Landroid/app/ProgressDialog;

    :cond_0
    invoke-super {p0}, Landroid/app/Activity;->onDestroy()V

    sget-object v0, Lcom/mediatek/contacts/activities/ContactImportExportActivity;->TAG:Ljava/lang/String;

    const-string v1, "[onDestroy]"

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    return-void
.end method

.method public onItemClick(Landroid/widget/AdapterView;Landroid/view/View;IJ)V
    .locals 0
    .param p2    # Landroid/view/View;
    .param p3    # I
    .param p4    # J
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/widget/AdapterView",
            "<*>;",
            "Landroid/view/View;",
            "IJ)V"
        }
    .end annotation

    invoke-direct {p0, p3}, Lcom/mediatek/contacts/activities/ContactImportExportActivity;->setCheckedPosition(I)V

    invoke-direct {p0, p3}, Lcom/mediatek/contacts/activities/ContactImportExportActivity;->setCheckedAccount(I)V

    return-void
.end method

.method public onOptionsItemSelected(Landroid/view/MenuItem;)Z
    .locals 1
    .param p1    # Landroid/view/MenuItem;

    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    invoke-super {p0, p1}, Landroid/app/Activity;->onOptionsItemSelected(Landroid/view/MenuItem;)Z

    move-result v0

    :goto_0
    return v0

    :pswitch_0
    invoke-virtual {p0}, Lcom/mediatek/contacts/activities/ContactImportExportActivity;->finish()V

    const/4 v0, 0x1

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x102002c
        :pswitch_0
    .end packed-switch
.end method
