.class public Lcom/mediatek/phone/provider/CallHistoryAsync$UpdateConfirmFlagArgs;
.super Ljava/lang/Object;
.source "CallHistoryAsync.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/mediatek/phone/provider/CallHistoryAsync;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "UpdateConfirmFlagArgs"
.end annotation


# instance fields
.field public final mConfirm:J

.field public final mContext:Landroid/content/Context;

.field public final mNumber:Ljava/lang/String;


# direct methods
.method public constructor <init>(Landroid/content/Context;Ljava/lang/String;J)V
    .locals 0
    .param p1    # Landroid/content/Context;
    .param p2    # Ljava/lang/String;
    .param p3    # J

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/mediatek/phone/provider/CallHistoryAsync$UpdateConfirmFlagArgs;->mContext:Landroid/content/Context;

    iput-object p2, p0, Lcom/mediatek/phone/provider/CallHistoryAsync$UpdateConfirmFlagArgs;->mNumber:Ljava/lang/String;

    iput-wide p3, p0, Lcom/mediatek/phone/provider/CallHistoryAsync$UpdateConfirmFlagArgs;->mConfirm:J

    return-void
.end method
