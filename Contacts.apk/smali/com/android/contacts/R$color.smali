.class public final Lcom/android/contacts/R$color;
.super Ljava/lang/Object;
.source "R.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/contacts/R;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "color"
.end annotation


# static fields
.field public static final action_bar_button_text_color:I = 0x7f080013

.field public static final background_primary:I = 0x7f080006

.field public static final background_social_updates:I = 0x7f080007

.field public static final call_log_detail:I = 0x7f08001a

.field public static final call_log_missed_call_highlight_color:I = 0x7f08000a

.field public static final call_log_voicemail_highlight_color:I = 0x7f08000b

.field public static final calllog_list_item_simname_font_color:I = 0x7f080019

.field public static final contact_count_text_color:I = 0x7f080011

.field public static final contact_name_searched_color:I = 0x7f08001b

.field public static final contact_tile_shadow_box_color:I = 0x7f080015

.field public static final detail_tab_carousel_tab_label_color:I = 0x7f080012

.field public static final detail_update_tab_text_color:I = 0x7f080009

.field public static final dialer_button_text:I = 0x7f08001c

.field public static final dialtacts_secondary_text_color:I = 0x7f080010

.field public static final image_placeholder:I = 0x7f080017

.field public static final item_selected:I = 0x7f080018

.field public static final kind_title:I = 0x7f08001d

.field public static final multiple_choice_text_color:I = 0x7f08001e

.field public static final people_app_theme_color:I = 0x7f08000f

.field public static final people_contact_tile_status_color:I = 0x7f080014

.field public static final primary_text_color:I = 0x7f08001f

.field public static final quickcontact_list_background:I = 0x7f080004

.field public static final quickcontact_list_divider:I = 0x7f080003

.field public static final quickcontact_tab_indicator:I = 0x7f080005

.field public static final secondary_text_color:I = 0x7f080020

.field public static final section_header_text_color:I = 0x7f080008

.field public static final shortcut_overlay_text_background:I = 0x7f080002

.field public static final stream_item_stripe_color:I = 0x7f080016

.field public static final textColorIconOverlay:I = 0x7f080000

.field public static final textColorIconOverlayShadow:I = 0x7f080001

.field public static final voicemail_playback_seek_bar_already_played:I = 0x7f08000e

.field public static final voicemail_playback_seek_bar_yet_to_play:I = 0x7f08000d

.field public static final voicemail_playback_ui_background:I = 0x7f08000c


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
