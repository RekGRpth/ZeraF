.class public final Lcom/android/contacts/R$string;
.super Ljava/lang/Object;
.source "R.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/contacts/R;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "string"
.end annotation


# static fields
.field public static final AirPlane_mode_on:I = 0x7f0c0043

.field public static final AirPlane_mode_on_edit:I = 0x7f0c0049

.field public static final FDNEnabled:I = 0x7f0c0044

.field public static final account_phone:I = 0x7f0c01f2

.field public static final account_phone_only:I = 0x7f0c00d6

.field public static final account_sim_only:I = 0x7f0c00d7

.field public static final account_type_format:I = 0x7f0c0249

.field public static final account_type_local_phone:I = 0x7f0c0056

.field public static final account_type_sim:I = 0x7f0c0054

.field public static final account_type_sim_label:I = 0x7f0c0059

.field public static final account_type_uim_label:I = 0x7f0c00b0

.field public static final account_type_usim:I = 0x7f0c0055

.field public static final account_type_usim_label:I = 0x7f0c005a

.field public static final account_uim_only:I = 0x7f0c00d8

.field public static final account_usim_only:I = 0x7f0c00d9

.field public static final actionIncomingCall:I = 0x7f0c017d

.field public static final action_menu_overflow_description:I = 0x7f0c02da

.field public static final activity_title_confirm_add_detail:I = 0x7f0c0286

.field public static final activity_title_contacts_filter:I = 0x7f0c027e

.field public static final activity_title_settings:I = 0x7f0c027d

.field public static final addPeopleToGroup:I = 0x7f0c013d

.field public static final add_2sec_pause:I = 0x7f0c01d2

.field public static final add_SIP_to_sim:I = 0x7f0c0093

.field public static final add_account:I = 0x7f0c02d5

.field public static final add_connection_button:I = 0x7f0c0246

.field public static final add_contact_dlg_message_fmt:I = 0x7f0c0184

.field public static final add_contact_dlg_title:I = 0x7f0c00b9

.field public static final add_field:I = 0x7f0c0263

.field public static final add_new_account:I = 0x7f0c02d6

.field public static final add_new_entry_for_section:I = 0x7f0c0264

.field public static final add_organization:I = 0x7f0c0265

.field public static final add_speed_dial:I = 0x7f0c0020

.field public static final add_to_my_contacts:I = 0x7f0c0270

.field public static final add_vm_number_str:I = 0x7f0c00a0

.field public static final add_wait:I = 0x7f0c01d3

.field public static final aggregation_suggestion_edit_dialog_message:I = 0x7f0c026e

.field public static final aggregation_suggestion_edit_dialog_title:I = 0x7f0c00e6

.field public static final aggregation_suggestion_join_dialog_message:I = 0x7f0c026d

.field public static final aggregation_suggestion_join_dialog_title:I = 0x7f0c00e5

.field public static final all_calls:I = 0x7f0c02e4

.field public static final all_resources:I = 0x7f0c0014

.field public static final all_tab_label:I = 0x7f0c02e1

.field public static final all_tab_label1:I = 0x7f0c0016

.field public static final already_has_SIP:I = 0x7f0c0094

.field public static final assign_to_speed_dial:I = 0x7f0c02f2

.field public static final assigned_key:I = 0x7f0c02f4

.field public static final associate_SIM:I = 0x7f0c003b

.field public static final associate_sim_missing_message:I = 0x7f0c002a

.field public static final associate_sim_missing_other:I = 0x7f0c002b

.field public static final associatiated_SIM:I = 0x7f0c0039

.field public static final attach_photo_dialog_title:I = 0x7f0c0133

.field public static final audio_chat:I = 0x7f0c0231

.field public static final available:I = 0x7f0c02f6

.field public static final back:I = 0x7f0c0073

.field public static final batchContactDeleteProgress:I = 0x7f0c02ec

.field public static final batch_delete_error:I = 0x7f0c02df

.field public static final block_incoming_call:I = 0x7f0c003c

.field public static final block_video_incoming_call:I = 0x7f0c003d

.field public static final block_voice_incoming_call:I = 0x7f0c0303

.field public static final blurbJoinContactDataWith:I = 0x7f0c0119

.field public static final both_sims:I = 0x7f0c02e3

.field public static final caching_vcard_message:I = 0x7f0c01ae

.field public static final caching_vcard_title:I = 0x7f0c01ad

.field public static final callAgain:I = 0x7f0c017f

.field public static final callBack:I = 0x7f0c017e

.field public static final callDetailTitle:I = 0x7f0c0177

.field public static final callDetailsDurationFormat:I = 0x7f0c0181

.field public static final callFailed_cdma_notEmergency:I = 0x7f0c00c1

.field public static final callFailed_simError:I = 0x7f0c002d

.field public static final callShortcutActivityTitle:I = 0x7f0c00f5

.field public static final call_assistant:I = 0x7f0c0206

.field public static final call_callback:I = 0x7f0c01fb

.field public static final call_car:I = 0x7f0c01fc

.field public static final call_company_main:I = 0x7f0c01fd

.field public static final call_custom:I = 0x7f0c01f3

.field public static final call_disambig_title:I = 0x7f0c01d4

.field public static final call_fax_home:I = 0x7f0c01f8

.field public static final call_fax_work:I = 0x7f0c01f7

.field public static final call_ghost_data_phone:I = 0x7f0c02e0

.field public static final call_home:I = 0x7f0c01f4

.field public static final call_indicator_3g:I = 0x7f0c0018

.field public static final call_ip_dial:I = 0x7f0c001c

.field public static final call_isdn:I = 0x7f0c01fe

.field public static final call_log_auto_rejected_label:I = 0x7f0c00b1

.field public static final call_log_empty_gecode:I = 0x7f0c02cb

.field public static final call_log_filter_all_resources:I = 0x7f0c0028

.field public static final call_log_incoming_header:I = 0x7f0c02a5

.field public static final call_log_item_count:I = 0x7f0c0017

.field public static final call_log_item_count_and_date:I = 0x7f0c02b3

.field public static final call_log_missed_header:I = 0x7f0c02a7

.field public static final call_log_new_header:I = 0x7f0c02a2

.field public static final call_log_old_header:I = 0x7f0c02a3

.field public static final call_log_outgoing_header:I = 0x7f0c02a6

.field public static final call_log_voicemail_header:I = 0x7f0c02a4

.field public static final call_main:I = 0x7f0c01ff

.field public static final call_mms:I = 0x7f0c0207

.field public static final call_mobile:I = 0x7f0c01f5

.field public static final call_other:I = 0x7f0c01fa

.field public static final call_other_fax:I = 0x7f0c0200

.field public static final call_pager:I = 0x7f0c01f9

.field public static final call_people:I = 0x7f0c001b

.field public static final call_pin_dialog_title:I = 0x7f0c001a

.field public static final call_radio:I = 0x7f0c0201

.field public static final call_settings:I = 0x7f0c01d5

.field public static final call_sipcall:I = 0x7f0c0019

.field public static final call_sms:I = 0x7f0c00ae

.field public static final call_speed_dial:I = 0x7f0c001d

.field public static final call_suggested:I = 0x7f0c02f8

.field public static final call_telex:I = 0x7f0c0202

.field public static final call_tty_tdd:I = 0x7f0c0203

.field public static final call_type_and_date:I = 0x7f0c0298

.field public static final call_via_sim1:I = 0x7f0c02e8

.field public static final call_via_sim2:I = 0x7f0c02e9

.field public static final call_video_call:I = 0x7f0c001f

.field public static final call_work:I = 0x7f0c01f6

.field public static final call_work_mobile:I = 0x7f0c0204

.field public static final call_work_pager:I = 0x7f0c0205

.field public static final calllogHint:I = 0x7f0c0098

.field public static final calllogList:I = 0x7f0c0097

.field public static final calllog_description:I = 0x7f0c0099

.field public static final calllog_today:I = 0x7f0c0032

.field public static final calllog_yesterday:I = 0x7f0c0033

.field public static final cancel_confirmation_dialog_message:I = 0x7f0c0297

.field public static final cancel_confirmation_dialog_title:I = 0x7f0c00e7

.field public static final cancel_export_confirmation_message:I = 0x7f0c01cf

.field public static final cancel_export_confirmation_title:I = 0x7f0c00e2

.field public static final cancel_import_confirmation_message:I = 0x7f0c01ce

.field public static final cancel_import_confirmation_title:I = 0x7f0c00e1

.field public static final cancel_vcard_import_or_export_failed:I = 0x7f0c01d0

.field public static final cannot_insert_null_number:I = 0x7f0c0042

.field public static final chat:I = 0x7f0c0230

.field public static final chat_aim:I = 0x7f0c0228

.field public static final chat_gtalk:I = 0x7f0c022d

.field public static final chat_icq:I = 0x7f0c022e

.field public static final chat_jabber:I = 0x7f0c022f

.field public static final chat_msn:I = 0x7f0c0229

.field public static final chat_qq:I = 0x7f0c022c

.field public static final chat_skype:I = 0x7f0c022b

.field public static final chat_yahoo:I = 0x7f0c022a

.field public static final choose_call_type_header:I = 0x7f0c02eb

.field public static final choose_resources_header:I = 0x7f0c0015

.field public static final choose_sim_header:I = 0x7f0c02ea

.field public static final clearCallLogConfirmation:I = 0x7f0c015e

.field public static final clearCallLogConfirmation_title:I = 0x7f0c015d

.field public static final clearCallLogProgress_title:I = 0x7f0c015f

.field public static final clearFrequentsConfirmation:I = 0x7f0c0161

.field public static final clearFrequentsConfirmation_title:I = 0x7f0c0160

.field public static final clearFrequentsProgress_title:I = 0x7f0c0162

.field public static final clear_default:I = 0x7f0c0295

.field public static final composer_failed_to_get_database_infomation:I = 0x7f0c01c9

.field public static final composer_has_no_exportable_contact:I = 0x7f0c01ca

.field public static final composer_not_initialized:I = 0x7f0c01cb

.field public static final config_export_extensions_to_consider:I = 0x7f0c0005

.field public static final config_export_file_extension:I = 0x7f0c0004

.field public static final config_export_file_prefix:I = 0x7f0c0002

.field public static final config_export_file_suffix:I = 0x7f0c0003

.field public static final config_export_vcard_type:I = 0x7f0c0001

.field public static final config_import_vcard_type:I = 0x7f0c0000

.field public static final config_prohibited_phone_number_regexp:I = 0x7f0c0006

.field public static final confirm_export_message:I = 0x7f0c01bd

.field public static final confirm_export_title:I = 0x7f0c01bc

.field public static final connections:I = 0x7f0c0245

.field public static final contactDetailAbout:I = 0x7f0c0101

.field public static final contactDetailUpdates:I = 0x7f0c0102

.field public static final contactPickerActivityTitle:I = 0x7f0c00f7

.field public static final contactSavedErrorToast:I = 0x7f0c0141

.field public static final contactSavedToast:I = 0x7f0c0140

.field public static final contact_delete_all_tips:I = 0x7f0c00a4

.field public static final contact_directory_description:I = 0x7f0c0271

.field public static final contact_editor_prompt_multiple_accounts:I = 0x7f0c02d3

.field public static final contact_editor_prompt_one_account:I = 0x7f0c02d2

.field public static final contact_editor_prompt_zero_accounts:I = 0x7f0c02d1

.field public static final contact_group_list_title:I = 0x7f0c00b5

.field public static final contact_list_loading:I = 0x7f0c027c

.field public static final contact_read_only:I = 0x7f0c024b

.field public static final contact_status_update_attribution:I = 0x7f0c0268

.field public static final contact_status_update_attribution_with_date:I = 0x7f0c0269

.field public static final contactsAllLabel:I = 0x7f0c014e

.field public static final contactsFavoritesLabel:I = 0x7f0c0150

.field public static final contactsGroupsLabel:I = 0x7f0c014f

.field public static final contactsJoinedMessage:I = 0x7f0c011d

.field public static final contactsList:I = 0x7f0c00f0

.field public static final contacts_unavailable_add_account:I = 0x7f0c028d

.field public static final contacts_unavailable_create_contact:I = 0x7f0c028c

.field public static final contacts_unavailable_import_contacts:I = 0x7f0c028e

.field public static final copy_text:I = 0x7f0c0293

.field public static final copy_text_to_dialer:I = 0x7f0c00c0

.field public static final createContactShortcutSuccessful:I = 0x7f0c00e9

.field public static final create_group_dialog_title:I = 0x7f0c028f

.field public static final create_group_item_label:I = 0x7f0c0290

.field public static final customLabelPickerTitle:I = 0x7f0c0134

.field public static final custom_list_filter:I = 0x7f0c027b

.field public static final date_year_toggle:I = 0x7f0c0289

.field public static final deleteCallLogConfirmation_message:I = 0x7f0c00a7

.field public static final deleteCallLogConfirmation_title:I = 0x7f0c00a6

.field public static final deleteConfirmation:I = 0x7f0c0123

.field public static final deleteConfirmation_title:I = 0x7f0c00ba

.field public static final delete_error:I = 0x7f0c02de

.field public static final delete_group_dialog_message:I = 0x7f0c0291

.field public static final delete_group_dialog_title:I = 0x7f0c00eb

.field public static final delete_group_failure:I = 0x7f0c00af

.field public static final deleting_call_log:I = 0x7f0c009d

.field public static final description_add_contact:I = 0x7f0c02c3

.field public static final description_call:I = 0x7f0c02c5

.field public static final description_call_log_incoming_call:I = 0x7f0c02bf

.field public static final description_call_log_missed_call:I = 0x7f0c02c1

.field public static final description_call_log_outgoing_call:I = 0x7f0c02c0

.field public static final description_call_log_play_button:I = 0x7f0c02be

.field public static final description_call_log_unheard_voicemail:I = 0x7f0c02c7

.field public static final description_call_log_voicemail:I = 0x7f0c02c2

.field public static final description_contact_photo:I = 0x7f0c0196

.field public static final description_delete_button:I = 0x7f0c0194

.field public static final description_dial_button:I = 0x7f0c0193

.field public static final description_dial_phone_number:I = 0x7f0c02c9

.field public static final description_digits_edittext:I = 0x7f0c0195

.field public static final description_image_button_eight:I = 0x7f0c018c

.field public static final description_image_button_five:I = 0x7f0c0189

.field public static final description_image_button_four:I = 0x7f0c0188

.field public static final description_image_button_nine:I = 0x7f0c018d

.field public static final description_image_button_one:I = 0x7f0c0185

.field public static final description_image_button_pound:I = 0x7f0c0190

.field public static final description_image_button_seven:I = 0x7f0c018b

.field public static final description_image_button_six:I = 0x7f0c018a

.field public static final description_image_button_star:I = 0x7f0c018e

.field public static final description_image_button_three:I = 0x7f0c0187

.field public static final description_image_button_two:I = 0x7f0c0186

.field public static final description_image_button_zero:I = 0x7f0c018f

.field public static final description_minus_button:I = 0x7f0c0197

.field public static final description_plus_button:I = 0x7f0c0198

.field public static final description_quick_contact_for:I = 0x7f0c02ca

.field public static final description_search_button:I = 0x7f0c0192

.field public static final description_send_message:I = 0x7f0c02c8

.field public static final description_send_text_message:I = 0x7f0c02c6

.field public static final description_star:I = 0x7f0c026a

.field public static final description_view_contact:I = 0x7f0c02c4

.field public static final description_view_contact_detail:I = 0x7f0c0199

.field public static final description_voicemail_button:I = 0x7f0c0191

.field public static final dialerAllContactsLabel:I = 0x7f0c0151

.field public static final dialerDialpadHintText:I = 0x7f0c016a

.field public static final dialerIconLabel:I = 0x7f0c0152

.field public static final dialerKeyboardHintText:I = 0x7f0c0169

.field public static final dialer_addAnotherCall:I = 0x7f0c0176

.field public static final dialer_returnToInCallScreen:I = 0x7f0c0175

.field public static final dialer_useDtmfDialpad:I = 0x7f0c0174

.field public static final dialog_import:I = 0x7f0c01df

.field public static final dialog_import_export:I = 0x7f0c01de

.field public static final dialog_new_contact_account:I = 0x7f0c01ea

.field public static final dialog_new_group_account:I = 0x7f0c01eb

.field public static final dialog_phone_call_prohibited_message:I = 0x7f0c00ed

.field public static final dialog_phone_call_prohibited_title:I = 0x7f0c00bb

.field public static final dialog_sync_add:I = 0x7f0c01ed

.field public static final dialog_voicemail_airplane_mode_message:I = 0x7f0c02d8

.field public static final dialog_voicemail_not_ready_message:I = 0x7f0c02d7

.field public static final dialog_voicemail_not_ready_title:I = 0x7f0c00bc

.field public static final directory_search_label:I = 0x7f0c0272

.field public static final display_all_contacts:I = 0x7f0c01f0

.field public static final display_more_groups:I = 0x7f0c01ee

.field public static final display_options_sort_by_family_name:I = 0x7f0c0250

.field public static final display_options_sort_by_given_name:I = 0x7f0c024f

.field public static final display_options_sort_list_by:I = 0x7f0c024e

.field public static final display_options_view_family_name_first:I = 0x7f0c0253

.field public static final display_options_view_given_name_first:I = 0x7f0c0252

.field public static final display_options_view_names_as:I = 0x7f0c0251

.field public static final display_ungrouped:I = 0x7f0c01ef

.field public static final display_warn_remove_ungrouped:I = 0x7f0c01f1

.field public static final dlg_simstorage_content:I = 0x7f0c00b4

.field public static final editContactDescription:I = 0x7f0c00fd

.field public static final editGroupDescription:I = 0x7f0c00ff

.field public static final edit_contact:I = 0x7f0c026b

.field public static final email:I = 0x7f0c0223

.field public static final emailLabelsGroup:I = 0x7f0c012f

.field public static final email_2g_invalid:I = 0x7f0c0047

.field public static final email_custom:I = 0x7f0c0222

.field public static final email_error:I = 0x7f0c0066

.field public static final email_home:I = 0x7f0c021e

.field public static final email_invalid:I = 0x7f0c0046

.field public static final email_mobile:I = 0x7f0c021f

.field public static final email_other:I = 0x7f0c0221

.field public static final email_too_long:I = 0x7f0c0050

.field public static final email_work:I = 0x7f0c0220

.field public static final emergency_number:I = 0x7f0c0302

.field public static final emergencycall:I = 0x7f0c0026

.field public static final emptyGroup:I = 0x7f0c013c

.field public static final enable_sip_dialog_message:I = 0x7f0c00a2

.field public static final enter_contact_name:I = 0x7f0c029a

.field public static final err_icc_no_phone_book:I = 0x7f0c00df

.field public static final error_import_usim_contact_email_lost:I = 0x7f0c00ac

.field public static final error_save_usim_contact_email_lost:I = 0x7f0c00ab

.field public static final eventLabelsGroup:I = 0x7f0c01e7

.field public static final event_edit_field_hint_text:I = 0x7f0c0266

.field public static final expand_collapse_name_fields_description:I = 0x7f0c02db

.field public static final export_to_sdcard:I = 0x7f0c019d

.field public static final exporting_contact_failed_message:I = 0x7f0c01bf

.field public static final exporting_contact_failed_title:I = 0x7f0c01be

.field public static final exporting_contact_list_message:I = 0x7f0c01c6

.field public static final exporting_contact_list_progress:I = 0x7f0c01cd

.field public static final exporting_contact_list_title:I = 0x7f0c01c5

.field public static final exporting_vcard_canceled_title:I = 0x7f0c01c4

.field public static final exporting_vcard_finished_title:I = 0x7f0c01c3

.field public static final external_profile_title:I = 0x7f0c02ce

.field public static final fail_reason_could_not_initialize_exporter:I = 0x7f0c01c7

.field public static final fail_reason_could_not_open_file:I = 0x7f0c01cc

.field public static final fail_reason_error_occurred_during_export:I = 0x7f0c01c8

.field public static final fail_reason_failed_to_collect_vcard_meta_info:I = 0x7f0c01aa

.field public static final fail_reason_failed_to_read_files:I = 0x7f0c00ec

.field public static final fail_reason_io_error:I = 0x7f0c01a4

.field public static final fail_reason_low_memory_during_import:I = 0x7f0c01a5

.field public static final fail_reason_no_exportable_contact:I = 0x7f0c01c0

.field public static final fail_reason_not_supported:I = 0x7f0c01a7

.field public static final fail_reason_too_long_filename:I = 0x7f0c01c2

.field public static final fail_reason_too_many_vcard:I = 0x7f0c01c1

.field public static final fail_reason_unknown:I = 0x7f0c01ab

.field public static final fail_reason_vcard_parse_error:I = 0x7f0c01a6

.field public static final favoritesFrequentCalled:I = 0x7f0c0183

.field public static final favoritesFrequentContacted:I = 0x7f0c0182

.field public static final file_already_on_sd_card:I = 0x7f0c00a8

.field public static final fix_number_too_long:I = 0x7f0c004b

.field public static final foundTooManyContacts:I = 0x7f0c014c

.field public static final frequentList:I = 0x7f0c00f9

.field public static final from_account_format:I = 0x7f0c00e8

.field public static final full_name:I = 0x7f0c023b

.field public static final generic_failure:I = 0x7f0c004f

.field public static final generic_no_account_prompt:I = 0x7f0c02dc

.field public static final generic_no_account_prompt_title:I = 0x7f0c02dd

.field public static final ghostData_company:I = 0x7f0c0129

.field public static final ghostData_title:I = 0x7f0c012a

.field public static final groupSavedErrorToast:I = 0x7f0c0143

.field public static final groupSavedToast:I = 0x7f0c0142

.field public static final group_discard_member:I = 0x7f0c006c

.field public static final group_discard_member_reason:I = 0x7f0c006d

.field public static final group_edit_field_hint_text:I = 0x7f0c0267

.field public static final group_name_exists:I = 0x7f0c006b

.field public static final group_name_hint:I = 0x7f0c02b4

.field public static final group_read_only:I = 0x7f0c024d

.field public static final groupsLabel:I = 0x7f0c01e9

.field public static final has_double_phone_number:I = 0x7f0c003f

.field public static final help_url_people_add:I = 0x7f0c0008

.field public static final help_url_people_edit:I = 0x7f0c0009

.field public static final help_url_people_main:I = 0x7f0c0007

.field public static final hint_findContacts:I = 0x7f0c0283

.field public static final imLabelsGroup:I = 0x7f0c0130

.field public static final imei:I = 0x7f0c0163

.field public static final imei_invalid:I = 0x7f0c0027

.field public static final imexport_bridge_sd_card:I = 0x7f0c0070

.field public static final imexport_title:I = 0x7f0c0074

.field public static final import_all_vcard_string:I = 0x7f0c01a1

.field public static final import_export_title:I = 0x7f0c006e

.field public static final import_failure_no_vcard_file:I = 0x7f0c01a9

.field public static final import_from_sdcard:I = 0x7f0c019c

.field public static final import_from_sim:I = 0x7f0c019b

.field public static final import_multiple_vcard_string:I = 0x7f0c01a0

.field public static final import_no_vcard_dialog_text:I = 0x7f0c00b2

.field public static final import_no_vcard_dialog_title:I = 0x7f0c0304

.field public static final import_one_vcard_string:I = 0x7f0c019f

.field public static final importing_vcard_canceled_title:I = 0x7f0c01b4

.field public static final importing_vcard_description:I = 0x7f0c01b0

.field public static final importing_vcard_finished_title:I = 0x7f0c01b3

.field public static final insertContactDescription:I = 0x7f0c00fe

.field public static final insertGroupDescription:I = 0x7f0c0100

.field public static final international_dialing_add_country_code:I = 0x7f0c00c4

.field public static final international_dialing_add_country_code_and_area_code:I = 0x7f0c00c5

.field public static final international_dialing_add_country_code_and_area_code_without_prefix:I = 0x7f0c00d4

.field public static final international_dialing_add_country_code_default_input_area_code:I = 0x7f0c00c8

.field public static final international_dialing_add_country_code_input_area_code:I = 0x7f0c00c7

.field public static final international_dialing_area_code:I = 0x7f0c00d2

.field public static final international_dialing_call_button:I = 0x7f0c00d1

.field public static final international_dialing_change_country_prefix:I = 0x7f0c00c6

.field public static final international_dialing_click_below_button:I = 0x7f0c00ce

.field public static final international_dialing_confirm_area_code:I = 0x7f0c00cc

.field public static final international_dialing_dial_with_country_code:I = 0x7f0c00cd

.field public static final international_dialing_dialed_number:I = 0x7f0c00cb

.field public static final international_dialing_input_area_code:I = 0x7f0c00ca

.field public static final international_dialing_invalid_number:I = 0x7f0c00d3

.field public static final international_dialing_need_area_code:I = 0x7f0c00c9

.field public static final international_dialing_no:I = 0x7f0c00d0

.field public static final international_dialing_select_country:I = 0x7f0c00c3

.field public static final international_dialing_title:I = 0x7f0c00c2

.field public static final international_dialing_yes:I = 0x7f0c00cf

.field public static final invalidContactMessage:I = 0x7f0c012b

.field public static final ip_dial_error_toast_for_no_ip_prefix_number:I = 0x7f0c002e

.field public static final ip_dial_error_toast_for_sip_call_selected:I = 0x7f0c002f

.field public static final keep_local:I = 0x7f0c02d4

.field public static final label_notes:I = 0x7f0c0127

.field public static final label_sip_address:I = 0x7f0c0128

.field public static final launcherDialer:I = 0x7f0c00ee

.field public static final listAllContactsInAccount:I = 0x7f0c0149

.field public static final listCustomView:I = 0x7f0c014b

.field public static final listFoundAllCalllogZero:I = 0x7f0c0096

.field public static final listFoundAllContactsZero:I = 0x7f0c014d

.field public static final listSingleContact:I = 0x7f0c014a

.field public static final listTotalAllContactsZero:I = 0x7f0c0145

.field public static final listTotalAllContactsZeroCustom:I = 0x7f0c0146

.field public static final listTotalAllContactsZeroGroup:I = 0x7f0c0148

.field public static final listTotalAllContactsZeroStarred:I = 0x7f0c0147

.field public static final listTotalPhoneContactsZero:I = 0x7f0c0144

.field public static final list_filter_all_accounts:I = 0x7f0c0275

.field public static final list_filter_all_starred:I = 0x7f0c0276

.field public static final list_filter_custom:I = 0x7f0c0277

.field public static final list_filter_customize:I = 0x7f0c0278

.field public static final list_filter_phones:I = 0x7f0c0279

.field public static final list_filter_single:I = 0x7f0c027a

.field public static final local_account_label:I = 0x7f0c0058

.field public static final local_account_type:I = 0x7f0c0057

.field public static final local_device_account_name:I = 0x7f0c0305

.field public static final local_invisible_directory:I = 0x7f0c026c

.field public static final local_profile_title:I = 0x7f0c02cd

.field public static final local_search_label:I = 0x7f0c0273

.field public static final locale_change_in_progress:I = 0x7f0c0258

.field public static final look_simstorage:I = 0x7f0c00b3

.field public static final make_primary:I = 0x7f0c01d7

.field public static final map_custom:I = 0x7f0c0227

.field public static final map_home:I = 0x7f0c0224

.field public static final map_other:I = 0x7f0c0226

.field public static final map_work:I = 0x7f0c0225

.field public static final meid:I = 0x7f0c0164

.field public static final menu_accounts:I = 0x7f0c01da

.field public static final menu_actionbar_selected_items:I = 0x7f0c003e

.field public static final menu_addStar:I = 0x7f0c0108

.field public static final menu_all_contacts:I = 0x7f0c00e3

.field public static final menu_association:I = 0x7f0c0034

.field public static final menu_call:I = 0x7f0c010e

.field public static final menu_callNumber:I = 0x7f0c0107

.field public static final menu_clearAll:I = 0x7f0c0011

.field public static final menu_clear_frequents:I = 0x7f0c01db

.field public static final menu_contacts_filter:I = 0x7f0c01dc

.field public static final menu_copy:I = 0x7f0c010c

.field public static final menu_copyContact:I = 0x7f0c026f

.field public static final menu_create_contact_shortcut:I = 0x7f0c010d

.field public static final menu_deleteContact:I = 0x7f0c010b

.field public static final menu_deleteGroup:I = 0x7f0c0112

.field public static final menu_delete_contact:I = 0x7f0c0077

.field public static final menu_discard:I = 0x7f0c0126

.field public static final menu_display_all:I = 0x7f0c025f

.field public static final menu_display_selected:I = 0x7f0c025e

.field public static final menu_doNotSave:I = 0x7f0c0125

.field public static final menu_done:I = 0x7f0c0124

.field public static final menu_editContact:I = 0x7f0c010a

.field public static final menu_editGroup:I = 0x7f0c0111

.field public static final menu_email_group:I = 0x7f0c005f

.field public static final menu_export_database:I = 0x7f0c02d9

.field public static final menu_help:I = 0x7f0c0280

.field public static final menu_import_export:I = 0x7f0c01dd

.field public static final menu_ipcallNumber:I = 0x7f0c009c

.field public static final menu_joinAggregate:I = 0x7f0c0117

.field public static final menu_message_group:I = 0x7f0c005e

.field public static final menu_move_group:I = 0x7f0c005d

.field public static final menu_multichoice_clear_select:I = 0x7f0c02fc

.field public static final menu_multichoice_select_all:I = 0x7f0c02fb

.field public static final menu_multichoice_title:I = 0x7f0c02e2

.field public static final menu_newContact:I = 0x7f0c0105

.field public static final menu_new_contact_action_bar:I = 0x7f0c0113

.field public static final menu_new_group_action_bar:I = 0x7f0c0114

.field public static final menu_print:I = 0x7f0c000a

.field public static final menu_redirect_calls_to_vm:I = 0x7f0c011f

.field public static final menu_removeStar:I = 0x7f0c0109

.field public static final menu_remove_association:I = 0x7f0c0035

.field public static final menu_search:I = 0x7f0c0104

.field public static final menu_selectAll:I = 0x7f0c0010

.field public static final menu_select_all:I = 0x7f0c0260

.field public static final menu_select_none:I = 0x7f0c0261

.field public static final menu_sendSMS:I = 0x7f0c010f

.field public static final menu_sendTextMessage:I = 0x7f0c0154

.field public static final menu_set_ring_tone:I = 0x7f0c011e

.field public static final menu_settings:I = 0x7f0c027f

.field public static final menu_share:I = 0x7f0c01e0

.field public static final menu_show_all_calls:I = 0x7f0c02ba

.field public static final menu_show_incoming_only:I = 0x7f0c02b7

.field public static final menu_show_missed_only:I = 0x7f0c02b8

.field public static final menu_show_outgoing_only:I = 0x7f0c02b6

.field public static final menu_show_voicemails_only:I = 0x7f0c02b9

.field public static final menu_speed_dial:I = 0x7f0c02ef

.field public static final menu_splitAggregate:I = 0x7f0c0110

.field public static final menu_sync_remove:I = 0x7f0c01ec

.field public static final menu_videocallNumber:I = 0x7f0c009b

.field public static final menu_viewContact:I = 0x7f0c0106

.field public static final messageShortcutActivityTitle:I = 0x7f0c00f6

.field public static final missed_calls:I = 0x7f0c02e7

.field public static final missing_name:I = 0x7f0c01d9

.field public static final move:I = 0x7f0c02ee

.field public static final move_contacts_to:I = 0x7f0c005c

.field public static final moving_group_members:I = 0x7f0c0060

.field public static final moving_group_members_fail:I = 0x7f0c0063

.field public static final moving_group_members_sucess:I = 0x7f0c0062

.field public static final msg_loading_sim_contacts_toast:I = 0x7f0c004a

.field public static final multichoice_confirmation_message_copy:I = 0x7f0c008c

.field public static final multichoice_confirmation_message_copy_with_account:I = 0x7f0c0301

.field public static final multichoice_confirmation_message_delete:I = 0x7f0c008a

.field public static final multichoice_confirmation_message_delete_with_account:I = 0x7f0c0300

.field public static final multichoice_confirmation_title_copy:I = 0x7f0c008b

.field public static final multichoice_confirmation_title_delete:I = 0x7f0c0089

.field public static final multichoice_contacts_limit:I = 0x7f0c0090

.field public static final multichoice_delete_confirm_message:I = 0x7f0c008e

.field public static final multichoice_delete_confirm_title:I = 0x7f0c008f

.field public static final multichoice_no_select_alert:I = 0x7f0c008d

.field public static final multipleContactDeleteConfirmation:I = 0x7f0c0122

.field public static final nameLabelsGroup:I = 0x7f0c01e3

.field public static final name_family:I = 0x7f0c023d

.field public static final name_given:I = 0x7f0c023c

.field public static final name_middle:I = 0x7f0c023f

.field public static final name_needed:I = 0x7f0c0069

.field public static final name_phonetic:I = 0x7f0c0244

.field public static final name_phonetic_family:I = 0x7f0c0243

.field public static final name_phonetic_given:I = 0x7f0c0241

.field public static final name_phonetic_middle:I = 0x7f0c0242

.field public static final name_prefix:I = 0x7f0c023e

.field public static final name_suffix:I = 0x7f0c0240

.field public static final name_too_long:I = 0x7f0c004c

.field public static final next:I = 0x7f0c006f

.field public static final nfc_vcard_file_name:I = 0x7f0c02b5

.field public static final nicknameLabelsGroup:I = 0x7f0c01e4

.field public static final nickname_too_long:I = 0x7f0c00be

.field public static final noAccounts:I = 0x7f0c0139

.field public static final noContacts:I = 0x7f0c0137

.field public static final noContactsHelpText:I = 0x7f0c016f

.field public static final noContactsHelpTextForCreateShortcut:I = 0x7f0c016e

.field public static final noContactsHelpTextWithSync:I = 0x7f0c0170

.field public static final noContactsHelpTextWithSyncForCreateShortcut:I = 0x7f0c016d

.field public static final noContactsNoSimHelpText:I = 0x7f0c0171

.field public static final noContactsNoSimHelpTextWithSync:I = 0x7f0c0172

.field public static final noContactsWithPhoneNumbers:I = 0x7f0c013b

.field public static final noFavoritesHelpText:I = 0x7f0c0173

.field public static final noGroups:I = 0x7f0c0138

.field public static final noMatchingCalllogs:I = 0x7f0c009a

.field public static final noMatchingContacts:I = 0x7f0c013a

.field public static final no_account_prompt:I = 0x7f0c02d0

.field public static final no_call_log:I = 0x7f0c00db

.field public static final no_contact_details:I = 0x7f0c024c

.field public static final no_contacts_selected:I = 0x7f0c0262

.field public static final no_match_call_log:I = 0x7f0c00da

.field public static final no_sdcard_message:I = 0x7f0c019a

.field public static final no_sdcard_title:I = 0x7f0c000d

.field public static final no_search_result:I = 0x7f0c00d5

.field public static final no_valid_email_in_group:I = 0x7f0c0065

.field public static final no_valid_number_in_group:I = 0x7f0c0064

.field public static final no_vm_number:I = 0x7f0c009e

.field public static final no_vm_number_msg:I = 0x7f0c009f

.field public static final no_way_to_print:I = 0x7f0c000c

.field public static final non_phone_add_to_contacts:I = 0x7f0c0285

.field public static final non_phone_caption:I = 0x7f0c0284

.field public static final non_phone_close:I = 0x7f0c0287

.field public static final notification_action_voicemail_play:I = 0x7f0c00ea

.field public static final notification_new_voicemail_ticker:I = 0x7f0c029d

.field public static final notification_voicemail_callers_list:I = 0x7f0c029c

.field public static final notifier_cancel_copy_title:I = 0x7f0c0081

.field public static final notifier_cancel_delete_title:I = 0x7f0c007f

.field public static final notifier_fail_copy_title:I = 0x7f0c0082

.field public static final notifier_fail_delete_title:I = 0x7f0c0080

.field public static final notifier_failure_by_sim_full:I = 0x7f0c007d

.field public static final notifier_failure_sim_notready:I = 0x7f0c007e

.field public static final notifier_finish_copy_content:I = 0x7f0c007b

.field public static final notifier_finish_copy_title:I = 0x7f0c007a

.field public static final notifier_finish_delete_content:I = 0x7f0c0079

.field public static final notifier_finish_delete_title:I = 0x7f0c0078

.field public static final notifier_multichoice_process_report:I = 0x7f0c007c

.field public static final notifier_progress__copy_will_start_message:I = 0x7f0c0086

.field public static final notifier_progress__delete_will_start_message:I = 0x7f0c0083

.field public static final notifier_progress_copy_description:I = 0x7f0c0088

.field public static final notifier_progress_copy_message:I = 0x7f0c0087

.field public static final notifier_progress_delete_description:I = 0x7f0c0085

.field public static final notifier_progress_delete_message:I = 0x7f0c0084

.field public static final number_too_long:I = 0x7f0c004e

.field public static final organizationLabelsGroup:I = 0x7f0c01e5

.field public static final organization_company_and_title:I = 0x7f0c0282

.field public static final outgoing_calls:I = 0x7f0c02e6

.field public static final payphone:I = 0x7f0c0168

.field public static final people:I = 0x7f0c00ef

.field public static final percentage:I = 0x7f0c01bb

.field public static final phoneLabelsGroup:I = 0x7f0c012e

.field public static final phone_book_busy:I = 0x7f0c0040

.field public static final phone_storage_full_create:I = 0x7f0c0091

.field public static final phone_storage_full_edit:I = 0x7f0c0092

.field public static final photoPickerNotFoundText:I = 0x7f0c0132

.field public static final pick_new_photo:I = 0x7f0c0257

.field public static final pick_new_photo_from_gallery:I = 0x7f0c00bf

.field public static final pick_photo:I = 0x7f0c0256

.field public static final pickerNewContactHeader:I = 0x7f0c012c

.field public static final pickerNewContactText:I = 0x7f0c012d

.field public static final please_wait:I = 0x7f0c005b

.field public static final postalLabelsGroup:I = 0x7f0c0131

.field public static final postal_address:I = 0x7f0c0233

.field public static final postal_city:I = 0x7f0c0237

.field public static final postal_country:I = 0x7f0c023a

.field public static final postal_neighborhood:I = 0x7f0c0236

.field public static final postal_pobox:I = 0x7f0c0235

.field public static final postal_postcode:I = 0x7f0c0239

.field public static final postal_region:I = 0x7f0c0238

.field public static final postal_street:I = 0x7f0c0234

.field public static final preference_displayOptions:I = 0x7f0c0281

.field public static final printContact:I = 0x7f0c000b

.field public static final private_num:I = 0x7f0c0167

.field public static final profile_display_name:I = 0x7f0c0299

.field public static final progress_notifier_message:I = 0x7f0c01af

.field public static final quickcontact_missing_app:I = 0x7f0c01d8

.field public static final readOnlyContactDeleteConfirmation:I = 0x7f0c0121

.field public static final readOnlyContactWarning:I = 0x7f0c0120

.field public static final reading_vcard_canceled_title:I = 0x7f0c01b2

.field public static final reading_vcard_failed_title:I = 0x7f0c01b1

.field public static final received_calls:I = 0x7f0c02e5

.field public static final recent:I = 0x7f0c0247

.field public static final recentCallsIconLabel:I = 0x7f0c0153

.field public static final recentCalls_addToContact:I = 0x7f0c0157

.field public static final recentCalls_callNumber:I = 0x7f0c0155

.field public static final recentCalls_delete:I = 0x7f0c000f

.field public static final recentCalls_deleteAll:I = 0x7f0c0159

.field public static final recentCalls_editNumberBeforeCall:I = 0x7f0c0156

.field public static final recentCalls_empty:I = 0x7f0c015c

.field public static final recentCalls_removeFromRecentList:I = 0x7f0c0158

.field public static final recentCalls_shareVoicemail:I = 0x7f0c015b

.field public static final recentCalls_trashVoicemail:I = 0x7f0c015a

.field public static final recent_updates:I = 0x7f0c0248

.field public static final relationLabelsGroup:I = 0x7f0c01e8

.field public static final reminder:I = 0x7f0c00a1

.field public static final removePhoto:I = 0x7f0c0136

.field public static final remove_association_message:I = 0x7f0c0036

.field public static final remove_number_title:I = 0x7f0c0037

.field public static final remove_sd_confirm_1:I = 0x7f0c0024

.field public static final remove_sd_confirm_2:I = 0x7f0c0025

.field public static final remove_speed_dial:I = 0x7f0c0023

.field public static final removing_group_member:I = 0x7f0c02f9

.field public static final removing_group_members_fail:I = 0x7f0c02fa

.field public static final removing_group_members_sucess:I = 0x7f0c0061

.field public static final reselect_key:I = 0x7f0c02f7

.field public static final reselect_number:I = 0x7f0c0031

.field public static final returnCall:I = 0x7f0c0180

.field public static final save_group_fail:I = 0x7f0c006a

.field public static final savingContact:I = 0x7f0c013e

.field public static final savingDisplayGroups:I = 0x7f0c013f

.field public static final scanning_sdcard_failed_message:I = 0x7f0c01a3

.field public static final scanning_sdcard_failed_title:I = 0x7f0c00bd

.field public static final sd_add:I = 0x7f0c0030

.field public static final sd_cancel:I = 0x7f0c02f3

.field public static final searchHint:I = 0x7f0c0103

.field public static final search_bar_hint:I = 0x7f0c00e4

.field public static final search_results_for:I = 0x7f0c0095

.field public static final search_results_searching:I = 0x7f0c025d

.field public static final search_settings_description:I = 0x7f0c01d1

.field public static final searching_vcard_message:I = 0x7f0c01a2

.field public static final searching_vcard_title:I = 0x7f0c00e0

.field public static final select_vcard_title:I = 0x7f0c01ac

.field public static final selected_item_count:I = 0x7f0c000e

.field public static final send_file_sms_error:I = 0x7f0c00a9

.field public static final send_groupemail_no_number_1:I = 0x7f0c00b7

.field public static final send_groupemail_no_number_2:I = 0x7f0c00b8

.field public static final send_groupemail_no_number_more:I = 0x7f0c00b6

.field public static final send_groupsms_no_number_1:I = 0x7f0c0052

.field public static final send_groupsms_no_number_2:I = 0x7f0c0053

.field public static final send_groupsms_no_number_more:I = 0x7f0c0051

.field public static final send_message:I = 0x7f0c001e

.field public static final send_to_voicemail_checkbox:I = 0x7f0c0135

.field public static final separatorJoinAggregateAll:I = 0x7f0c011c

.field public static final separatorJoinAggregateSuggestions:I = 0x7f0c011b

.field public static final set_default:I = 0x7f0c0294

.field public static final share_contacts_limit:I = 0x7f0c00a3

.field public static final share_error:I = 0x7f0c01e2

.field public static final share_too_large:I = 0x7f0c00dc

.field public static final share_via:I = 0x7f0c01e1

.field public static final share_visible_contacts:I = 0x7f0c019e

.field public static final shortcutActivityTitle:I = 0x7f0c00f4

.field public static final shortcutContact:I = 0x7f0c00f1

.field public static final shortcutDialContact:I = 0x7f0c00f2

.field public static final shortcutMessageContact:I = 0x7f0c00f3

.field public static final showAllContactsJoinItem:I = 0x7f0c011a

.field public static final sim1:I = 0x7f0c0012

.field public static final sim2:I = 0x7f0c0013

.field public static final sim3:I = 0x7f0c00dd

.field public static final sim4:I = 0x7f0c00de

.field public static final simContacts_emptyLoading:I = 0x7f0c016b

.field public static final simContacts_title:I = 0x7f0c016c

.field public static final sim_card_state_illegal:I = 0x7f0c02ed

.field public static final sim_invalid:I = 0x7f0c0041

.field public static final sim_invalid_fix_number:I = 0x7f0c0048

.field public static final sim_invalid_number:I = 0x7f0c0045

.field public static final sim_manage_call_via:I = 0x7f0c002c

.field public static final sms:I = 0x7f0c021d

.field public static final sms_assistant:I = 0x7f0c021b

.field public static final sms_callback:I = 0x7f0c0210

.field public static final sms_car:I = 0x7f0c0211

.field public static final sms_company_main:I = 0x7f0c0212

.field public static final sms_custom:I = 0x7f0c0208

.field public static final sms_disambig_title:I = 0x7f0c01d6

.field public static final sms_fax_home:I = 0x7f0c020d

.field public static final sms_fax_work:I = 0x7f0c020c

.field public static final sms_home:I = 0x7f0c0209

.field public static final sms_isdn:I = 0x7f0c0213

.field public static final sms_main:I = 0x7f0c0214

.field public static final sms_mms:I = 0x7f0c021c

.field public static final sms_mobile:I = 0x7f0c020a

.field public static final sms_other:I = 0x7f0c020f

.field public static final sms_other_fax:I = 0x7f0c0215

.field public static final sms_pager:I = 0x7f0c020e

.field public static final sms_radio:I = 0x7f0c0216

.field public static final sms_telex:I = 0x7f0c0217

.field public static final sms_tty_tdd:I = 0x7f0c0218

.field public static final sms_work:I = 0x7f0c020b

.field public static final sms_work_mobile:I = 0x7f0c0219

.field public static final sms_work_pager:I = 0x7f0c021a

.field public static final social_widget_label:I = 0x7f0c028a

.field public static final social_widget_loading:I = 0x7f0c028b

.field public static final speed_dial_added:I = 0x7f0c0021

.field public static final speed_dial_added2:I = 0x7f0c0022

.field public static final speed_dial_manage:I = 0x7f0c02f0

.field public static final speed_dial_view:I = 0x7f0c02f1

.field public static final splitConfirmation:I = 0x7f0c0116

.field public static final splitConfirmation_title:I = 0x7f0c0115

.field public static final starredList:I = 0x7f0c00f8

.field public static final status_available:I = 0x7f0c02bb

.field public static final status_away:I = 0x7f0c02bc

.field public static final status_busy:I = 0x7f0c02bd

.field public static final storage_full:I = 0x7f0c004d

.field public static final store_contact_to:I = 0x7f0c0076

.field public static final strequentList:I = 0x7f0c00fa

.field public static final sum_search_networks:I = 0x7f0c00a5

.field public static final take_new_photo:I = 0x7f0c0255

.field public static final take_photo:I = 0x7f0c0254

.field public static final tips_source:I = 0x7f0c0071

.field public static final tips_target:I = 0x7f0c0072

.field public static final titleJoinContactDataWith:I = 0x7f0c0118

.field public static final toast_call_detail_error:I = 0x7f0c0178

.field public static final toast_displaying_all_contacts:I = 0x7f0c02cf

.field public static final toast_join_with_empty_contact:I = 0x7f0c0292

.field public static final toast_making_personal_copy:I = 0x7f0c0274

.field public static final toast_text_copied:I = 0x7f0c0296

.field public static final turn_on_3g_service_message:I = 0x7f0c0029

.field public static final type_incoming:I = 0x7f0c0179

.field public static final type_ip_outgoing:I = 0x7f0c00ad

.field public static final type_missed:I = 0x7f0c017b

.field public static final type_outgoing:I = 0x7f0c017a

.field public static final type_voicemail:I = 0x7f0c017c

.field public static final unassigned:I = 0x7f0c02f5

.field public static final unassociated:I = 0x7f0c003a

.field public static final unknown:I = 0x7f0c0166

.field public static final upgrade_in_progress:I = 0x7f0c0259

.field public static final upgrade_out_of_memory:I = 0x7f0c025a

.field public static final upgrade_out_of_memory_retry:I = 0x7f0c025c

.field public static final upgrade_out_of_memory_uninstall:I = 0x7f0c025b

.field public static final use_photo_as_primary:I = 0x7f0c024a

.field public static final user_profile_cannot_sd_card:I = 0x7f0c00aa

.field public static final user_profile_contacts_list_header:I = 0x7f0c02cc

.field public static final usim_group_count_exceed_limit:I = 0x7f0c0068

.field public static final usim_group_count_exceed_limit_export:I = 0x7f0c02fe

.field public static final usim_group_exceed_limit_export:I = 0x7f0c02ff

.field public static final usim_group_name_exceed_limit:I = 0x7f0c0067

.field public static final usim_group_name_exceed_limit_export:I = 0x7f0c02fd

.field public static final vcard_export_request_rejected_message:I = 0x7f0c01b9

.field public static final vcard_export_will_start_message:I = 0x7f0c01b8

.field public static final vcard_import_failed:I = 0x7f0c01a8

.field public static final vcard_import_request_rejected_message:I = 0x7f0c01b7

.field public static final vcard_import_will_start_message:I = 0x7f0c01b5

.field public static final vcard_import_will_start_message_with_default_name:I = 0x7f0c01b6

.field public static final vcard_unknown_filename:I = 0x7f0c01ba

.field public static final video_chat:I = 0x7f0c0232

.field public static final viewContactDesription:I = 0x7f0c00fc

.field public static final viewContactTitle:I = 0x7f0c00fb

.field public static final view_updates_from_group:I = 0x7f0c029b

.field public static final voicemail:I = 0x7f0c0165

.field public static final voicemail_buffering:I = 0x7f0c029f

.field public static final voicemail_fetching_content:I = 0x7f0c02a0

.field public static final voicemail_fetching_timout:I = 0x7f0c02a1

.field public static final voicemail_playback_error:I = 0x7f0c029e

.field public static final voicemail_speed_faster:I = 0x7f0c02b1

.field public static final voicemail_speed_fastest:I = 0x7f0c02b2

.field public static final voicemail_speed_normal:I = 0x7f0c02b0

.field public static final voicemail_speed_slower:I = 0x7f0c02af

.field public static final voicemail_speed_slowest:I = 0x7f0c02ae

.field public static final voicemail_status_action_call_server:I = 0x7f0c02ad

.field public static final voicemail_status_action_configure:I = 0x7f0c02ac

.field public static final voicemail_status_audio_not_available:I = 0x7f0c02ab

.field public static final voicemail_status_configure_voicemail:I = 0x7f0c02aa

.field public static final voicemail_status_messages_waiting:I = 0x7f0c02a9

.field public static final voicemail_status_voicemail_not_available:I = 0x7f0c02a8

.field public static final warning_detail:I = 0x7f0c0038

.field public static final websiteLabelsGroup:I = 0x7f0c01e6

.field public static final widget_name_and_phonetic:I = 0x7f0c0288

.field public static final xport_error_one_account:I = 0x7f0c0075


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
