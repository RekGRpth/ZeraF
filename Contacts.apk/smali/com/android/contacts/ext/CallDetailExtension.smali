.class public Lcom/android/contacts/ext/CallDetailExtension;
.super Ljava/lang/Object;
.source "CallDetailExtension.java"


# static fields
.field private static final TAG:Ljava/lang/String; = "CallDetailExtension"


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public getCommand()Ljava/lang/String;
    .locals 1

    const-string v0, ""

    return-object v0
.end method

.method public isNeedAutoRejectedMenu(ZLjava/lang/String;)Z
    .locals 1
    .param p1    # Z
    .param p2    # Ljava/lang/String;

    const/4 v0, 0x0

    return v0
.end method

.method public setChar(ZLjava/lang/String;Ljava/lang/String;IZLjava/lang/String;)Ljava/lang/String;
    .locals 1
    .param p1    # Z
    .param p2    # Ljava/lang/String;
    .param p3    # Ljava/lang/String;
    .param p4    # I
    .param p5    # Z
    .param p6    # Ljava/lang/String;

    const/4 v0, 0x0

    return-object v0
.end method

.method public setTextView(ILandroid/widget/TextView;Ljava/lang/String;Ljava/lang/String;)V
    .locals 2
    .param p1    # I
    .param p2    # Landroid/widget/TextView;
    .param p3    # Ljava/lang/String;
    .param p4    # Ljava/lang/String;

    const/4 v0, 0x3

    if-eq p1, v0, :cond_0

    const/4 v0, 0x4

    if-ne p1, v0, :cond_1

    :cond_0
    const-string v0, "CallDetailExtension"

    const-string v1, "[setTextView] is gone"

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    const/16 v0, 0x8

    invoke-virtual {p2, v0}, Landroid/view/View;->setVisibility(I)V

    :goto_0
    return-void

    :cond_1
    const-string v0, "CallDetailExtension"

    const-string v1, "[setTextView] is visible"

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v0, 0x0

    invoke-virtual {p2, v0}, Landroid/view/View;->setVisibility(I)V

    invoke-virtual {p2, p3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0
.end method

.method public setViewVisible(Landroid/view/View;Ljava/lang/String;Ljava/lang/String;IIIIIII)V
    .locals 0
    .param p1    # Landroid/view/View;
    .param p2    # Ljava/lang/String;
    .param p3    # Ljava/lang/String;
    .param p4    # I
    .param p5    # I
    .param p6    # I
    .param p7    # I
    .param p8    # I
    .param p9    # I
    .param p10    # I

    return-void
.end method

.method public setViewVisibleByActivity(Landroid/app/Activity;Ljava/lang/String;Ljava/lang/String;IIIIIIILjava/lang/String;)V
    .locals 0
    .param p1    # Landroid/app/Activity;
    .param p2    # Ljava/lang/String;
    .param p3    # Ljava/lang/String;
    .param p4    # I
    .param p5    # I
    .param p6    # I
    .param p7    # I
    .param p8    # I
    .param p9    # I
    .param p10    # I
    .param p11    # Ljava/lang/String;

    return-void
.end method
