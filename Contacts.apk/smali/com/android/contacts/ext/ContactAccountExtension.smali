.class public Lcom/android/contacts/ext/ContactAccountExtension;
.super Ljava/lang/Object;
.source "ContactAccountExtension.java"


# static fields
.field public static final CONTENTVALUE_ANR_INSERT:I = 0x1

.field public static final CONTENTVALUE_ANR_UPDATE:I = 0x2

.field public static final CONTENTVALUE_INSERT_SIM:I = 0x3

.field public static final CONTENTVALUE_NICKNAME:I = 0x0

.field public static final DB_UPDATE_ANR:I = 0x1

.field public static final DB_UPDATE_NICKNAME:I = 0x0

.field public static final FEATURE_AAS:Ljava/lang/String; = "AAS"

.field public static final FEATURE_SNE:Ljava/lang/String; = "SNE"

.field public static final PROJECTION_ADDRESS_BOOK:I = 0x3

.field public static final PROJECTION_COPY_TO_SIM:I = 0x1

.field public static final PROJECTION_LOAD_DATA:I = 0x2

.field public static final TYPE_OPERATION_AAS:I = 0x0

.field public static final TYPE_OPERATION_INSERT:I = 0x2

.field public static final TYPE_OPERATION_SNE:I = 0x1


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public buildOperation(Ljava/lang/String;Ljava/util/ArrayList;Ljava/util/ArrayList;Ljava/lang/String;IILjava/lang/String;)Z
    .locals 1
    .param p1    # Ljava/lang/String;
    .param p3    # Ljava/util/ArrayList;
    .param p4    # Ljava/lang/String;
    .param p5    # I
    .param p6    # I
    .param p7    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/util/ArrayList",
            "<",
            "Landroid/content/ContentProviderOperation;",
            ">;",
            "Ljava/util/ArrayList;",
            "Ljava/lang/String;",
            "II",
            "Ljava/lang/String;",
            ")Z"
        }
    .end annotation

    const/4 v0, 0x0

    return v0
.end method

.method public buildOperationFromCursor(Ljava/lang/String;Ljava/util/ArrayList;Landroid/database/Cursor;ILjava/lang/String;)Z
    .locals 1
    .param p1    # Ljava/lang/String;
    .param p3    # Landroid/database/Cursor;
    .param p4    # I
    .param p5    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/util/ArrayList",
            "<",
            "Landroid/content/ContentProviderOperation;",
            ">;",
            "Landroid/database/Cursor;",
            "I",
            "Ljava/lang/String;",
            ")Z"
        }
    .end annotation

    const/4 v0, 0x0

    return v0
.end method

.method public buildValuesForSim(Ljava/lang/String;Landroid/content/Context;Landroid/content/ContentValues;Ljava/util/ArrayList;Ljava/util/ArrayList;IILjava/util/ArrayList;Ljava/lang/String;)Z
    .locals 1
    .param p1    # Ljava/lang/String;
    .param p2    # Landroid/content/Context;
    .param p3    # Landroid/content/ContentValues;
    .param p6    # I
    .param p7    # I
    .param p8    # Ljava/util/ArrayList;
    .param p9    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Landroid/content/Context;",
            "Landroid/content/ContentValues;",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/Integer;",
            ">;II",
            "Ljava/util/ArrayList;",
            "Ljava/lang/String;",
            ")Z"
        }
    .end annotation

    const/4 v0, 0x0

    return v0
.end method

.method public checkOperationBuilder(Ljava/lang/String;Landroid/content/ContentProviderOperation$Builder;Landroid/database/Cursor;ILjava/lang/String;)Z
    .locals 2
    .param p1    # Ljava/lang/String;
    .param p2    # Landroid/content/ContentProviderOperation$Builder;
    .param p3    # Landroid/database/Cursor;
    .param p4    # I
    .param p5    # Ljava/lang/String;

    const/4 v1, 0x2

    if-ne p4, v1, :cond_0

    const-string v0, "data2"

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {p2, v0, v1}, Landroid/content/ContentProviderOperation$Builder;->withValue(Ljava/lang/String;Ljava/lang/Object;)Landroid/content/ContentProviderOperation$Builder;

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getCurrentSlot(Ljava/lang/String;)I
    .locals 1
    .param p1    # Ljava/lang/String;

    const/4 v0, -0x1

    return v0
.end method

.method public getCustomTypeLabel(ILjava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    .locals 1
    .param p1    # I
    .param p2    # Ljava/lang/String;
    .param p3    # Ljava/lang/String;

    const/4 v0, 0x0

    return-object v0
.end method

.method public getProjection(I[Ljava/lang/String;Ljava/lang/String;)[Ljava/lang/String;
    .locals 0
    .param p1    # I
    .param p2    # [Ljava/lang/String;
    .param p3    # Ljava/lang/String;

    return-object p2
.end method

.method public getTypeLabel(Landroid/content/res/Resources;ILjava/lang/CharSequence;ILjava/lang/String;)Ljava/lang/CharSequence;
    .locals 1
    .param p1    # Landroid/content/res/Resources;
    .param p2    # I
    .param p3    # Ljava/lang/CharSequence;
    .param p4    # I
    .param p5    # Ljava/lang/String;

    invoke-static {p1, p2, p3}, Landroid/provider/ContactsContract$CommonDataKinds$Phone;->getTypeLabel(Landroid/content/res/Resources;ILjava/lang/CharSequence;)Ljava/lang/CharSequence;

    move-result-object v0

    return-object v0
.end method

.method public hidePhoneLabel(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Z
    .locals 1
    .param p1    # Ljava/lang/String;
    .param p2    # Ljava/lang/String;
    .param p3    # Ljava/lang/String;
    .param p4    # Ljava/lang/String;

    const/4 v0, 0x0

    return v0
.end method

.method public isFeatureAccount(Ljava/lang/String;Ljava/lang/String;)Z
    .locals 1
    .param p1    # Ljava/lang/String;
    .param p2    # Ljava/lang/String;

    const/4 v0, 0x0

    return v0
.end method

.method public isFeatureEnabled(Ljava/lang/String;)Z
    .locals 1
    .param p1    # Ljava/lang/String;

    const/4 v0, 0x0

    return v0
.end method

.method public isPhone(Ljava/lang/String;Ljava/lang/String;)Z
    .locals 1
    .param p1    # Ljava/lang/String;
    .param p2    # Ljava/lang/String;

    const/4 v0, 0x0

    return v0
.end method

.method public isTextValid(Ljava/lang/String;IILjava/lang/String;)Z
    .locals 1
    .param p1    # Ljava/lang/String;
    .param p2    # I
    .param p3    # I
    .param p4    # Ljava/lang/String;

    const/4 v0, 0x1

    return v0
.end method

.method public needNewDataKind(Ljava/lang/String;)Z
    .locals 1
    .param p1    # Ljava/lang/String;

    const/4 v0, 0x0

    return v0
.end method

.method public setCurrentSlot(ILjava/lang/String;)V
    .locals 0
    .param p1    # I
    .param p2    # Ljava/lang/String;

    return-void
.end method

.method public switchSimGuide(Landroid/app/Activity;Ljava/lang/String;Ljava/lang/String;)V
    .locals 0
    .param p1    # Landroid/app/Activity;
    .param p2    # Ljava/lang/String;
    .param p3    # Ljava/lang/String;

    return-void
.end method

.method public updateContentValues(Ljava/lang/String;Landroid/content/ContentValues;Ljava/util/ArrayList;Ljava/lang/String;ILjava/lang/String;)Z
    .locals 2
    .param p1    # Ljava/lang/String;
    .param p2    # Landroid/content/ContentValues;
    .param p3    # Ljava/util/ArrayList;
    .param p4    # Ljava/lang/String;
    .param p5    # I
    .param p6    # Ljava/lang/String;

    const/4 v1, 0x2

    if-ne p5, v1, :cond_1

    invoke-static {p4}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_0

    const-string v0, ""

    :goto_0
    const-string v1, "newAnr"

    invoke-virtual {p2, v1, v0}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    const/4 v1, 0x1

    :goto_1
    return v1

    :cond_0
    move-object v0, p4

    goto :goto_0

    :cond_1
    const/4 v1, 0x0

    goto :goto_1
.end method

.method public updateDataToDb(Ljava/lang/String;Landroid/content/ContentResolver;Ljava/util/ArrayList;Ljava/util/ArrayList;JILjava/lang/String;)Z
    .locals 1
    .param p1    # Ljava/lang/String;
    .param p2    # Landroid/content/ContentResolver;
    .param p3    # Ljava/util/ArrayList;
    .param p4    # Ljava/util/ArrayList;
    .param p5    # J
    .param p7    # I
    .param p8    # Ljava/lang/String;

    const/4 v0, 0x0

    return v0
.end method
