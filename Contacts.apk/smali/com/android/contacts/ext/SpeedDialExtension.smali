.class public Lcom/android/contacts/ext/SpeedDialExtension;
.super Ljava/lang/Object;
.source "SpeedDialExtension.java"


# static fields
.field private static final TAG:Ljava/lang/String; = "SpeedDialExtension"


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public clearPrefStateIfNecessary(Ljava/lang/String;)Z
    .locals 2
    .param p1    # Ljava/lang/String;

    const-string v0, "SpeedDialExtension"

    const-string v1, "SpeedDialManageActivity: [clearPrefStateIfNecessary]"

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v0, 0x1

    return v0
.end method

.method public needCheckContacts(Ljava/lang/String;)Z
    .locals 2
    .param p1    # Ljava/lang/String;

    const-string v0, "SpeedDialExtension"

    const-string v1, "SpeedDialManageActivity: [needCheckContacts()]"

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v0, 0x1

    return v0
.end method

.method public needClearPreState(Ljava/lang/String;)Z
    .locals 1
    .param p1    # Ljava/lang/String;

    const/4 v0, 0x1

    return v0
.end method

.method public needClearSharedPreferences(Ljava/lang/String;)Z
    .locals 1
    .param p1    # Ljava/lang/String;

    const/4 v0, 0x1

    return v0
.end method

.method public setAddPosition(IZLjava/lang/String;)I
    .locals 0
    .param p1    # I
    .param p2    # Z
    .param p3    # Ljava/lang/String;

    return p1
.end method

.method public setView(Landroid/view/View;IZILjava/lang/String;)V
    .locals 0
    .param p1    # Landroid/view/View;
    .param p2    # I
    .param p3    # Z
    .param p4    # I
    .param p5    # Ljava/lang/String;

    return-void
.end method

.method public showSpeedInputDialog(Ljava/lang/String;)Z
    .locals 1
    .param p1    # Ljava/lang/String;

    const/4 v0, 0x0

    return v0
.end method
