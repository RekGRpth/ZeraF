.class Lcom/android/contacts/list/PhoneFavoriteFragment$2;
.super Ljava/lang/Object;
.source "PhoneFavoriteFragment.java"

# interfaces
.implements Landroid/widget/AdapterView$OnItemClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/android/contacts/list/PhoneFavoriteFragment;->onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/contacts/list/PhoneFavoriteFragment;


# direct methods
.method constructor <init>(Lcom/android/contacts/list/PhoneFavoriteFragment;)V
    .locals 0

    iput-object p1, p0, Lcom/android/contacts/list/PhoneFavoriteFragment$2;->this$0:Lcom/android/contacts/list/PhoneFavoriteFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onItemClick(Landroid/widget/AdapterView;Landroid/view/View;IJ)V
    .locals 3
    .param p2    # Landroid/view/View;
    .param p3    # I
    .param p4    # J
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/widget/AdapterView",
            "<*>;",
            "Landroid/view/View;",
            "IJ)V"
        }
    .end annotation

    add-int/lit8 v0, p3, -0x1

    iget-object v2, p0, Lcom/android/contacts/list/PhoneFavoriteFragment$2;->this$0:Lcom/android/contacts/list/PhoneFavoriteFragment;

    invoke-static {v2}, Lcom/android/contacts/list/PhoneFavoriteFragment;->access$1700(Lcom/android/contacts/list/PhoneFavoriteFragment;)Lcom/android/contacts/list/PhoneFavoriteFragment$Listener;

    move-result-object v2

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/android/contacts/list/PhoneFavoriteFragment$2;->this$0:Lcom/android/contacts/list/PhoneFavoriteFragment;

    invoke-static {v2}, Lcom/android/contacts/list/PhoneFavoriteFragment;->access$400(Lcom/android/contacts/list/PhoneFavoriteFragment;)Lcom/android/contacts/list/PhoneNumberListAdapter;

    move-result-object v2

    invoke-virtual {v2, v0}, Lcom/android/contacts/list/PhoneNumberListAdapter;->getDataUri(I)Landroid/net/Uri;

    move-result-object v1

    if-eqz v1, :cond_0

    iget-object v2, p0, Lcom/android/contacts/list/PhoneFavoriteFragment$2;->this$0:Lcom/android/contacts/list/PhoneFavoriteFragment;

    invoke-static {v2}, Lcom/android/contacts/list/PhoneFavoriteFragment;->access$1700(Lcom/android/contacts/list/PhoneFavoriteFragment;)Lcom/android/contacts/list/PhoneFavoriteFragment$Listener;

    move-result-object v2

    invoke-interface {v2, v1}, Lcom/android/contacts/list/PhoneFavoriteFragment$Listener;->onContactSelected(Landroid/net/Uri;)V

    :cond_0
    return-void
.end method
