.class Lcom/android/contacts/list/PhoneFavoriteFragment$ContactTileLoaderListener;
.super Ljava/lang/Object;
.source "PhoneFavoriteFragment.java"

# interfaces
.implements Landroid/app/LoaderManager$LoaderCallbacks;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/contacts/list/PhoneFavoriteFragment;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "ContactTileLoaderListener"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Landroid/app/LoaderManager$LoaderCallbacks",
        "<",
        "Landroid/database/Cursor;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/contacts/list/PhoneFavoriteFragment;


# direct methods
.method private constructor <init>(Lcom/android/contacts/list/PhoneFavoriteFragment;)V
    .locals 0

    iput-object p1, p0, Lcom/android/contacts/list/PhoneFavoriteFragment$ContactTileLoaderListener;->this$0:Lcom/android/contacts/list/PhoneFavoriteFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/android/contacts/list/PhoneFavoriteFragment;Lcom/android/contacts/list/PhoneFavoriteFragment$1;)V
    .locals 0
    .param p1    # Lcom/android/contacts/list/PhoneFavoriteFragment;
    .param p2    # Lcom/android/contacts/list/PhoneFavoriteFragment$1;

    invoke-direct {p0, p1}, Lcom/android/contacts/list/PhoneFavoriteFragment$ContactTileLoaderListener;-><init>(Lcom/android/contacts/list/PhoneFavoriteFragment;)V

    return-void
.end method


# virtual methods
.method public onCreateLoader(ILandroid/os/Bundle;)Landroid/content/CursorLoader;
    .locals 1
    .param p1    # I
    .param p2    # Landroid/os/Bundle;

    iget-object v0, p0, Lcom/android/contacts/list/PhoneFavoriteFragment$ContactTileLoaderListener;->this$0:Lcom/android/contacts/list/PhoneFavoriteFragment;

    invoke-virtual {v0}, Landroid/app/Fragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    invoke-static {v0}, Lcom/android/contacts/ContactTileLoaderFactory;->createStrequentPhoneOnlyLoader(Landroid/content/Context;)Landroid/content/CursorLoader;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic onCreateLoader(ILandroid/os/Bundle;)Landroid/content/Loader;
    .locals 1
    .param p1    # I
    .param p2    # Landroid/os/Bundle;

    invoke-virtual {p0, p1, p2}, Lcom/android/contacts/list/PhoneFavoriteFragment$ContactTileLoaderListener;->onCreateLoader(ILandroid/os/Bundle;)Landroid/content/CursorLoader;

    move-result-object v0

    return-object v0
.end method

.method public onLoadFinished(Landroid/content/Loader;Landroid/database/Cursor;)V
    .locals 5
    .param p2    # Landroid/database/Cursor;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Loader",
            "<",
            "Landroid/database/Cursor;",
            ">;",
            "Landroid/database/Cursor;",
            ")V"
        }
    .end annotation

    const/4 v4, 0x0

    const/4 v3, 0x0

    iget-object v0, p0, Lcom/android/contacts/list/PhoneFavoriteFragment$ContactTileLoaderListener;->this$0:Lcom/android/contacts/list/PhoneFavoriteFragment;

    invoke-virtual {v0}, Landroid/app/Fragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    invoke-static {v0}, Lcom/android/contacts/util/PhoneCapabilityTester;->isUsingTwoPanes(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/android/contacts/list/PhoneFavoriteFragment$ContactTileLoaderListener;->this$0:Lcom/android/contacts/list/PhoneFavoriteFragment;

    invoke-virtual {v0}, Landroid/app/Fragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/ContextThemeWrapper;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v0

    iget v0, v0, Landroid/content/res/Configuration;->orientation:I

    const/4 v1, 0x2

    if-ne v0, v1, :cond_1

    if-eqz p2, :cond_0

    invoke-interface {p2}, Landroid/database/Cursor;->getCount()I

    move-result v0

    if-nez v0, :cond_1

    :cond_0
    iget-object v0, p0, Lcom/android/contacts/list/PhoneFavoriteFragment$ContactTileLoaderListener;->this$0:Lcom/android/contacts/list/PhoneFavoriteFragment;

    invoke-static {v0}, Lcom/android/contacts/list/PhoneFavoriteFragment;->access$000(Lcom/android/contacts/list/PhoneFavoriteFragment;)Landroid/widget/TextView;

    move-result-object v0

    const v1, 0x7f0c0173

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    iget-object v0, p0, Lcom/android/contacts/list/PhoneFavoriteFragment$ContactTileLoaderListener;->this$0:Lcom/android/contacts/list/PhoneFavoriteFragment;

    invoke-static {v0}, Lcom/android/contacts/list/PhoneFavoriteFragment;->access$100(Lcom/android/contacts/list/PhoneFavoriteFragment;)Landroid/widget/ListView;

    move-result-object v0

    iget-object v1, p0, Lcom/android/contacts/list/PhoneFavoriteFragment$ContactTileLoaderListener;->this$0:Lcom/android/contacts/list/PhoneFavoriteFragment;

    invoke-static {v1}, Lcom/android/contacts/list/PhoneFavoriteFragment;->access$000(Lcom/android/contacts/list/PhoneFavoriteFragment;)Landroid/widget/TextView;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/AdapterView;->setEmptyView(Landroid/view/View;)V

    :cond_1
    iget-object v0, p0, Lcom/android/contacts/list/PhoneFavoriteFragment$ContactTileLoaderListener;->this$0:Lcom/android/contacts/list/PhoneFavoriteFragment;

    invoke-static {v0}, Lcom/android/contacts/list/PhoneFavoriteFragment;->access$200(Lcom/android/contacts/list/PhoneFavoriteFragment;)Lcom/android/contacts/list/ContactTileAdapter;

    move-result-object v0

    invoke-virtual {v0, p2}, Lcom/android/contacts/list/ContactTileAdapter;->setContactCursor(Landroid/database/Cursor;)V

    iget-object v0, p0, Lcom/android/contacts/list/PhoneFavoriteFragment$ContactTileLoaderListener;->this$0:Lcom/android/contacts/list/PhoneFavoriteFragment;

    invoke-static {v0}, Lcom/android/contacts/list/PhoneFavoriteFragment;->access$300(Lcom/android/contacts/list/PhoneFavoriteFragment;)Z

    move-result v0

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/android/contacts/list/PhoneFavoriteFragment$ContactTileLoaderListener;->this$0:Lcom/android/contacts/list/PhoneFavoriteFragment;

    invoke-static {v0}, Lcom/android/contacts/list/PhoneFavoriteFragment;->access$400(Lcom/android/contacts/list/PhoneFavoriteFragment;)Lcom/android/contacts/list/PhoneNumberListAdapter;

    move-result-object v0

    invoke-virtual {v0}, Lcom/android/contacts/list/ContactEntryListAdapter;->onDataReload()V

    iget-object v0, p0, Lcom/android/contacts/list/PhoneFavoriteFragment$ContactTileLoaderListener;->this$0:Lcom/android/contacts/list/PhoneFavoriteFragment;

    invoke-virtual {v0}, Landroid/app/Fragment;->getLoaderManager()Landroid/app/LoaderManager;

    move-result-object v0

    invoke-static {}, Lcom/android/contacts/list/PhoneFavoriteFragment;->access$500()I

    move-result v1

    iget-object v2, p0, Lcom/android/contacts/list/PhoneFavoriteFragment$ContactTileLoaderListener;->this$0:Lcom/android/contacts/list/PhoneFavoriteFragment;

    invoke-static {v2}, Lcom/android/contacts/list/PhoneFavoriteFragment;->access$600(Lcom/android/contacts/list/PhoneFavoriteFragment;)Landroid/app/LoaderManager$LoaderCallbacks;

    move-result-object v2

    invoke-virtual {v0, v1, v4, v2}, Landroid/app/LoaderManager;->restartLoader(ILandroid/os/Bundle;Landroid/app/LoaderManager$LoaderCallbacks;)Landroid/content/Loader;

    :cond_2
    :goto_0
    iget-object v0, p0, Lcom/android/contacts/list/PhoneFavoriteFragment$ContactTileLoaderListener;->this$0:Lcom/android/contacts/list/PhoneFavoriteFragment;

    invoke-static {v0, v3}, Lcom/android/contacts/list/PhoneFavoriteFragment;->access$302(Lcom/android/contacts/list/PhoneFavoriteFragment;Z)Z

    iget-object v0, p0, Lcom/android/contacts/list/PhoneFavoriteFragment$ContactTileLoaderListener;->this$0:Lcom/android/contacts/list/PhoneFavoriteFragment;

    const/4 v1, 0x1

    invoke-static {v0, v1}, Lcom/android/contacts/list/PhoneFavoriteFragment;->access$702(Lcom/android/contacts/list/PhoneFavoriteFragment;Z)Z

    iget-object v0, p0, Lcom/android/contacts/list/PhoneFavoriteFragment$ContactTileLoaderListener;->this$0:Lcom/android/contacts/list/PhoneFavoriteFragment;

    invoke-static {v0}, Lcom/android/contacts/list/PhoneFavoriteFragment;->access$800(Lcom/android/contacts/list/PhoneFavoriteFragment;)V

    iget-object v0, p0, Lcom/android/contacts/list/PhoneFavoriteFragment$ContactTileLoaderListener;->this$0:Lcom/android/contacts/list/PhoneFavoriteFragment;

    invoke-static {v0}, Lcom/android/contacts/list/PhoneFavoriteFragment;->access$900(Lcom/android/contacts/list/PhoneFavoriteFragment;)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v3}, Landroid/view/View;->setVisibility(I)V

    iget-object v0, p0, Lcom/android/contacts/list/PhoneFavoriteFragment$ContactTileLoaderListener;->this$0:Lcom/android/contacts/list/PhoneFavoriteFragment;

    invoke-static {v0}, Lcom/android/contacts/list/PhoneFavoriteFragment;->access$1000(Lcom/android/contacts/list/PhoneFavoriteFragment;)V

    return-void

    :cond_3
    iget-object v0, p0, Lcom/android/contacts/list/PhoneFavoriteFragment$ContactTileLoaderListener;->this$0:Lcom/android/contacts/list/PhoneFavoriteFragment;

    invoke-static {v0}, Lcom/android/contacts/list/PhoneFavoriteFragment;->access$700(Lcom/android/contacts/list/PhoneFavoriteFragment;)Z

    move-result v0

    if-nez v0, :cond_2

    iget-object v0, p0, Lcom/android/contacts/list/PhoneFavoriteFragment$ContactTileLoaderListener;->this$0:Lcom/android/contacts/list/PhoneFavoriteFragment;

    invoke-virtual {v0}, Landroid/app/Fragment;->getLoaderManager()Landroid/app/LoaderManager;

    move-result-object v0

    invoke-static {}, Lcom/android/contacts/list/PhoneFavoriteFragment;->access$500()I

    move-result v1

    iget-object v2, p0, Lcom/android/contacts/list/PhoneFavoriteFragment$ContactTileLoaderListener;->this$0:Lcom/android/contacts/list/PhoneFavoriteFragment;

    invoke-static {v2}, Lcom/android/contacts/list/PhoneFavoriteFragment;->access$600(Lcom/android/contacts/list/PhoneFavoriteFragment;)Landroid/app/LoaderManager$LoaderCallbacks;

    move-result-object v2

    invoke-virtual {v0, v1, v4, v2}, Landroid/app/LoaderManager;->initLoader(ILandroid/os/Bundle;Landroid/app/LoaderManager$LoaderCallbacks;)Landroid/content/Loader;

    goto :goto_0
.end method

.method public bridge synthetic onLoadFinished(Landroid/content/Loader;Ljava/lang/Object;)V
    .locals 0
    .param p1    # Landroid/content/Loader;
    .param p2    # Ljava/lang/Object;

    check-cast p2, Landroid/database/Cursor;

    invoke-virtual {p0, p1, p2}, Lcom/android/contacts/list/PhoneFavoriteFragment$ContactTileLoaderListener;->onLoadFinished(Landroid/content/Loader;Landroid/database/Cursor;)V

    return-void
.end method

.method public onLoaderReset(Landroid/content/Loader;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Loader",
            "<",
            "Landroid/database/Cursor;",
            ">;)V"
        }
    .end annotation

    return-void
.end method
