.class public Lcom/android/contacts/list/ContactNameHighlightingAnimation;
.super Lcom/android/contacts/widget/TextHighlightingAnimation;
.source "ContactNameHighlightingAnimation.java"


# instance fields
.field private final mListView:Landroid/widget/ListView;

.field private mSavedScrollingCacheEnabledFlag:Z


# direct methods
.method public constructor <init>(Landroid/widget/ListView;I)V
    .locals 0
    .param p1    # Landroid/widget/ListView;
    .param p2    # I

    invoke-direct {p0, p2}, Lcom/android/contacts/widget/TextHighlightingAnimation;-><init>(I)V

    iput-object p1, p0, Lcom/android/contacts/list/ContactNameHighlightingAnimation;->mListView:Landroid/widget/ListView;

    return-void
.end method


# virtual methods
.method protected invalidate()V
    .locals 5

    iget-object v4, p0, Lcom/android/contacts/list/ContactNameHighlightingAnimation;->mListView:Landroid/widget/ListView;

    invoke-virtual {v4}, Landroid/view/ViewGroup;->getChildCount()I

    move-result v0

    const/4 v1, 0x0

    :goto_0
    if-ge v1, v0, :cond_1

    iget-object v4, p0, Lcom/android/contacts/list/ContactNameHighlightingAnimation;->mListView:Landroid/widget/ListView;

    invoke-virtual {v4, v1}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v2

    instance-of v4, v2, Lcom/android/contacts/list/ContactListItemView;

    if-eqz v4, :cond_0

    move-object v3, v2

    check-cast v3, Lcom/android/contacts/list/ContactListItemView;

    invoke-virtual {v3}, Lcom/android/contacts/list/ContactListItemView;->getNameTextView()Landroid/widget/TextView;

    move-result-object v4

    invoke-virtual {v4}, Landroid/view/View;->invalidate()V

    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_1
    return-void
.end method

.method protected onAnimationEnded()V
    .locals 2

    iget-object v0, p0, Lcom/android/contacts/list/ContactNameHighlightingAnimation;->mListView:Landroid/widget/ListView;

    iget-boolean v1, p0, Lcom/android/contacts/list/ContactNameHighlightingAnimation;->mSavedScrollingCacheEnabledFlag:Z

    invoke-virtual {v0, v1}, Landroid/widget/AbsListView;->setScrollingCacheEnabled(Z)V

    return-void
.end method

.method protected onAnimationStarted()V
    .locals 2

    iget-object v0, p0, Lcom/android/contacts/list/ContactNameHighlightingAnimation;->mListView:Landroid/widget/ListView;

    invoke-virtual {v0}, Landroid/widget/AbsListView;->isScrollingCacheEnabled()Z

    move-result v0

    iput-boolean v0, p0, Lcom/android/contacts/list/ContactNameHighlightingAnimation;->mSavedScrollingCacheEnabledFlag:Z

    iget-object v0, p0, Lcom/android/contacts/list/ContactNameHighlightingAnimation;->mListView:Landroid/widget/ListView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/AbsListView;->setScrollingCacheEnabled(Z)V

    return-void
.end method
