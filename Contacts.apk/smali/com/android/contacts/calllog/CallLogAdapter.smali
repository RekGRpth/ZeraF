.class public Lcom/android/contacts/calllog/CallLogAdapter;
.super Lcom/mediatek/contacts/widget/GroupingListAdapterWithHeader;
.source "CallLogAdapter.java"

# interfaces
.implements Landroid/widget/AbsListView$OnScrollListener;
.implements Lcom/android/contacts/calllog/CallLogGroupBuilder$GroupCreator;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/contacts/calllog/CallLogAdapter$ContactInfoRequest;,
        Lcom/android/contacts/calllog/CallLogAdapter$NumberWithCountryIso;,
        Lcom/android/contacts/calllog/CallLogAdapter$CallFetcher;
    }
.end annotation


# static fields
.field private static final CONTACT_INFO_CACHE_SIZE:I = 0x64

.field private static final REDRAW:I = 0x1

.field private static final START_PROCESSING_REQUESTS_DELAY_MILLIS:I = 0x3e8

.field private static final START_THREAD:I = 0x2

.field private static final TAG:Ljava/lang/String; = "CallLogAdapter"


# instance fields
.field private final mCallFetcher:Lcom/android/contacts/calllog/CallLogAdapter$CallFetcher;

.field private final mCallLogGroupBuilder:Lcom/android/contacts/calllog/CallLogGroupBuilder;

.field private final mCallLogSimInfoHelper:Lcom/mediatek/contacts/calllog/CallLogSimInfoHelper;

.field private final mCallLogViewsHelper:Lcom/android/contacts/calllog/CallLogListItemHelper;

.field private mContactInfoCache:Lcom/android/contacts/util/ExpirableCache;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/android/contacts/util/ExpirableCache",
            "<",
            "Lcom/android/contacts/calllog/CallLogAdapter$NumberWithCountryIso;",
            "Lcom/android/contacts/calllog/ContactInfo;",
            ">;"
        }
    .end annotation
.end field

.field private final mContactInfoHelper:Lcom/android/contacts/calllog/ContactInfoHelper;

.field private mContactInfoMap:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Lcom/android/contacts/calllog/ContactInfo;",
            ">;"
        }
    .end annotation
.end field

.field private final mContactPhotoManager:Lcom/android/contacts/ContactPhotoManager;

.field private final mContext:Landroid/content/Context;

.field private mCursor:Landroid/database/Cursor;

.field private mLoading:Z

.field protected final mPhoneCallDetailsHelper:Lcom/android/contacts/PhoneCallDetailsHelper;

.field private mPhoneNumberHelper:Lcom/android/contacts/calllog/PhoneNumberHelper;

.field private volatile mRequestProcessingDisabled:Z

.field private final mRequests:Ljava/util/LinkedList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/LinkedList",
            "<",
            "Lcom/android/contacts/calllog/CallLogAdapter$ContactInfoRequest;",
            ">;"
        }
    .end annotation
.end field

.field private final mSecondaryActionListener:Landroid/view/View$OnClickListener;

.field private mSimInfoWrapper:Lcom/mediatek/phone/SIMInfoWrapper;

.field private mViewTreeObserver:Landroid/view/ViewTreeObserver;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/android/contacts/calllog/CallLogAdapter$CallFetcher;Lcom/android/contacts/calllog/ContactInfoHelper;)V
    .locals 6
    .param p1    # Landroid/content/Context;
    .param p2    # Lcom/android/contacts/calllog/CallLogAdapter$CallFetcher;
    .param p3    # Lcom/android/contacts/calllog/ContactInfoHelper;

    invoke-direct {p0, p1}, Lcom/mediatek/contacts/widget/GroupingListAdapterWithHeader;-><init>(Landroid/content/Context;)V

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/android/contacts/calllog/CallLogAdapter;->mViewTreeObserver:Landroid/view/ViewTreeObserver;

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/android/contacts/calllog/CallLogAdapter;->mLoading:Z

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/android/contacts/calllog/CallLogAdapter;->mRequestProcessingDisabled:Z

    new-instance v0, Lcom/android/contacts/calllog/CallLogAdapter$1;

    invoke-direct {v0, p0}, Lcom/android/contacts/calllog/CallLogAdapter$1;-><init>(Lcom/android/contacts/calllog/CallLogAdapter;)V

    iput-object v0, p0, Lcom/android/contacts/calllog/CallLogAdapter;->mSecondaryActionListener:Landroid/view/View$OnClickListener;

    iput-object p1, p0, Lcom/android/contacts/calllog/CallLogAdapter;->mContext:Landroid/content/Context;

    iput-object p2, p0, Lcom/android/contacts/calllog/CallLogAdapter;->mCallFetcher:Lcom/android/contacts/calllog/CallLogAdapter$CallFetcher;

    iput-object p3, p0, Lcom/android/contacts/calllog/CallLogAdapter;->mContactInfoHelper:Lcom/android/contacts/calllog/ContactInfoHelper;

    const/16 v0, 0x64

    invoke-static {v0}, Lcom/android/contacts/util/ExpirableCache;->create(I)Lcom/android/contacts/util/ExpirableCache;

    move-result-object v0

    iput-object v0, p0, Lcom/android/contacts/calllog/CallLogAdapter;->mContactInfoCache:Lcom/android/contacts/util/ExpirableCache;

    new-instance v0, Ljava/util/LinkedList;

    invoke-direct {v0}, Ljava/util/LinkedList;-><init>()V

    iput-object v0, p0, Lcom/android/contacts/calllog/CallLogAdapter;->mRequests:Ljava/util/LinkedList;

    iget-object v0, p0, Lcom/android/contacts/calllog/CallLogAdapter;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    new-instance v2, Lcom/android/contacts/calllog/CallTypeHelper;

    invoke-direct {v2, v1}, Lcom/android/contacts/calllog/CallTypeHelper;-><init>(Landroid/content/res/Resources;)V

    iget-object v0, p0, Lcom/android/contacts/calllog/CallLogAdapter;->mContext:Landroid/content/Context;

    invoke-static {v0}, Lcom/android/contacts/ContactPhotoManager;->getInstance(Landroid/content/Context;)Lcom/android/contacts/ContactPhotoManager;

    move-result-object v0

    iput-object v0, p0, Lcom/android/contacts/calllog/CallLogAdapter;->mContactPhotoManager:Lcom/android/contacts/ContactPhotoManager;

    new-instance v0, Lcom/android/contacts/calllog/PhoneNumberHelper;

    invoke-direct {v0, v1}, Lcom/android/contacts/calllog/PhoneNumberHelper;-><init>(Landroid/content/res/Resources;)V

    iput-object v0, p0, Lcom/android/contacts/calllog/CallLogAdapter;->mPhoneNumberHelper:Lcom/android/contacts/calllog/PhoneNumberHelper;

    new-instance v0, Lcom/mediatek/contacts/calllog/CallLogSimInfoHelper;

    invoke-direct {v0, v1}, Lcom/mediatek/contacts/calllog/CallLogSimInfoHelper;-><init>(Landroid/content/res/Resources;)V

    iput-object v0, p0, Lcom/android/contacts/calllog/CallLogAdapter;->mCallLogSimInfoHelper:Lcom/mediatek/contacts/calllog/CallLogSimInfoHelper;

    new-instance v0, Lcom/android/contacts/PhoneCallDetailsHelper;

    iget-object v3, p0, Lcom/android/contacts/calllog/CallLogAdapter;->mPhoneNumberHelper:Lcom/android/contacts/calllog/PhoneNumberHelper;

    iget-object v4, p0, Lcom/android/contacts/calllog/CallLogAdapter;->mCallLogSimInfoHelper:Lcom/mediatek/contacts/calllog/CallLogSimInfoHelper;

    iget-object v5, p0, Lcom/android/contacts/calllog/CallLogAdapter;->mContext:Landroid/content/Context;

    invoke-direct/range {v0 .. v5}, Lcom/android/contacts/PhoneCallDetailsHelper;-><init>(Landroid/content/res/Resources;Lcom/android/contacts/calllog/CallTypeHelper;Lcom/android/contacts/calllog/PhoneNumberHelper;Lcom/mediatek/contacts/calllog/CallLogSimInfoHelper;Landroid/content/Context;)V

    iput-object v0, p0, Lcom/android/contacts/calllog/CallLogAdapter;->mPhoneCallDetailsHelper:Lcom/android/contacts/PhoneCallDetailsHelper;

    new-instance v0, Lcom/android/contacts/calllog/CallLogListItemHelper;

    iget-object v3, p0, Lcom/android/contacts/calllog/CallLogAdapter;->mPhoneCallDetailsHelper:Lcom/android/contacts/PhoneCallDetailsHelper;

    iget-object v4, p0, Lcom/android/contacts/calllog/CallLogAdapter;->mPhoneNumberHelper:Lcom/android/contacts/calllog/PhoneNumberHelper;

    invoke-direct {v0, v3, v4, v1}, Lcom/android/contacts/calllog/CallLogListItemHelper;-><init>(Lcom/android/contacts/PhoneCallDetailsHelper;Lcom/android/contacts/calllog/PhoneNumberHelper;Landroid/content/res/Resources;)V

    iput-object v0, p0, Lcom/android/contacts/calllog/CallLogAdapter;->mCallLogViewsHelper:Lcom/android/contacts/calllog/CallLogListItemHelper;

    new-instance v0, Lcom/android/contacts/calllog/CallLogGroupBuilder;

    invoke-direct {v0, p0}, Lcom/android/contacts/calllog/CallLogGroupBuilder;-><init>(Lcom/android/contacts/calllog/CallLogGroupBuilder$GroupCreator;)V

    iput-object v0, p0, Lcom/android/contacts/calllog/CallLogAdapter;->mCallLogGroupBuilder:Lcom/android/contacts/calllog/CallLogGroupBuilder;

    iget-object v0, p0, Lcom/android/contacts/calllog/CallLogAdapter;->mContactInfoMap:Ljava/util/HashMap;

    if-nez v0, :cond_0

    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/android/contacts/calllog/CallLogAdapter;->mContactInfoMap:Ljava/util/HashMap;

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/android/contacts/calllog/CallLogAdapter;->mContactInfoMap:Ljava/util/HashMap;

    invoke-virtual {v0}, Ljava/util/HashMap;->clear()V

    goto :goto_0
.end method

.method static synthetic access$000(Lcom/android/contacts/calllog/CallLogAdapter;)Landroid/content/Context;
    .locals 1
    .param p0    # Lcom/android/contacts/calllog/CallLogAdapter;

    iget-object v0, p0, Lcom/android/contacts/calllog/CallLogAdapter;->mContext:Landroid/content/Context;

    return-object v0
.end method

.method private configureCallSecondaryAction(Lcom/mediatek/contacts/calllog/CallLogListItemView;Lcom/android/contacts/PhoneCallDetails;)V
    .locals 2
    .param p1    # Lcom/mediatek/contacts/calllog/CallLogListItemView;
    .param p2    # Lcom/android/contacts/PhoneCallDetails;

    invoke-virtual {p1}, Lcom/mediatek/contacts/calllog/CallLogListItemView;->getCallButton()Landroid/widget/ImageView;

    move-result-object v0

    invoke-direct {p0, p2}, Lcom/android/contacts/calllog/CallLogAdapter;->getCallActionDescription(Lcom/android/contacts/PhoneCallDetails;)Ljava/lang/CharSequence;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/view/View;->setContentDescription(Ljava/lang/CharSequence;)V

    return-void
.end method

.method private getCallActionDescription(Lcom/android/contacts/PhoneCallDetails;)Ljava/lang/CharSequence;
    .locals 5
    .param p1    # Lcom/android/contacts/PhoneCallDetails;

    iget-object v1, p1, Lcom/android/contacts/PhoneCallDetails;->name:Ljava/lang/CharSequence;

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_0

    iget-object v0, p1, Lcom/android/contacts/PhoneCallDetails;->name:Ljava/lang/CharSequence;

    :goto_0
    iget-object v1, p0, Lcom/android/contacts/calllog/CallLogAdapter;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0c02c5

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    aput-object v0, v3, v4

    invoke-virtual {v1, v2, v3}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    return-object v1

    :cond_0
    iget-object v1, p0, Lcom/android/contacts/calllog/CallLogAdapter;->mPhoneNumberHelper:Lcom/android/contacts/calllog/PhoneNumberHelper;

    iget-object v2, p1, Lcom/android/contacts/PhoneCallDetails;->number:Ljava/lang/CharSequence;

    iget-object v3, p1, Lcom/android/contacts/PhoneCallDetails;->formattedNumber:Ljava/lang/CharSequence;

    invoke-virtual {v1, v2, v3}, Lcom/android/contacts/calllog/PhoneNumberHelper;->getDisplayNumber(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/CharSequence;

    move-result-object v0

    goto :goto_0
.end method

.method private log(Ljava/lang/String;)V
    .locals 1
    .param p1    # Ljava/lang/String;

    const-string v0, "CallLogAdapter"

    invoke-static {v0, p1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    return-void
.end method


# virtual methods
.method public addGroup(IIZ)V
    .locals 0
    .param p1    # I
    .param p2    # I
    .param p3    # Z

    invoke-super {p0, p1, p2, p3}, Lcom/android/common/widget/GroupingListAdapter;->addGroup(IIZ)V

    return-void
.end method

.method protected addGroups(Landroid/database/Cursor;)V
    .locals 1
    .param p1    # Landroid/database/Cursor;
    .annotation build Lcom/google/common/annotations/VisibleForTesting;
    .end annotation

    iget-object v0, p0, Lcom/android/contacts/calllog/CallLogAdapter;->mCallLogGroupBuilder:Lcom/android/contacts/calllog/CallLogGroupBuilder;

    invoke-virtual {v0, p1}, Lcom/android/contacts/calllog/CallLogGroupBuilder;->addGroups(Landroid/database/Cursor;)V

    return-void
.end method

.method protected bindCallButtonView(Lcom/mediatek/contacts/calllog/CallLogListItemView;Lcom/android/contacts/PhoneCallDetails;)V
    .locals 6
    .param p1    # Lcom/mediatek/contacts/calllog/CallLogListItemView;
    .param p2    # Lcom/android/contacts/PhoneCallDetails;

    const/4 v5, 0x0

    invoke-virtual {p1}, Lcom/mediatek/contacts/calllog/CallLogListItemView;->getCallButton()Landroid/widget/ImageView;

    move-result-object v1

    const v2, 0x7f020040

    invoke-virtual {v1, v2}, Landroid/widget/ImageView;->setImageResource(I)V

    invoke-virtual {p1}, Lcom/mediatek/contacts/calllog/CallLogListItemView;->getCallButton()Landroid/widget/ImageView;

    move-result-object v1

    invoke-virtual {v1, v5}, Landroid/widget/ImageView;->setColorFilter(I)V

    iget-object v1, p2, Lcom/android/contacts/PhoneCallDetails;->number:Ljava/lang/CharSequence;

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_0

    invoke-virtual {p1}, Lcom/mediatek/contacts/calllog/CallLogListItemView;->getCallButton()Landroid/widget/ImageView;

    move-result-object v2

    iget-object v1, p2, Lcom/android/contacts/PhoneCallDetails;->number:Ljava/lang/CharSequence;

    check-cast v1, Ljava/lang/String;

    iget v3, p2, Lcom/android/contacts/PhoneCallDetails;->simId:I

    int-to-long v3, v3

    invoke-static {v1, v3, v4}, Lcom/android/contacts/calllog/IntentProvider;->getReturnCallIntentProvider(Ljava/lang/String;J)Lcom/android/contacts/calllog/IntentProvider;

    move-result-object v1

    invoke-virtual {v2, v1}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    :goto_0
    iget-object v1, p0, Lcom/android/contacts/calllog/CallLogAdapter;->mPhoneNumberHelper:Lcom/android/contacts/calllog/PhoneNumberHelper;

    iget-object v2, p2, Lcom/android/contacts/PhoneCallDetails;->number:Ljava/lang/CharSequence;

    invoke-virtual {v1, v2}, Lcom/android/contacts/calllog/PhoneNumberHelper;->canPlaceCallsTo(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-direct {p0, p1, p2}, Lcom/android/contacts/calllog/CallLogAdapter;->configureCallSecondaryAction(Lcom/mediatek/contacts/calllog/CallLogListItemView;Lcom/android/contacts/PhoneCallDetails;)V

    invoke-virtual {p1}, Lcom/mediatek/contacts/calllog/CallLogListItemView;->getCallButton()Landroid/widget/ImageView;

    move-result-object v1

    invoke-virtual {v1, v5}, Landroid/widget/ImageView;->setVisibility(I)V

    :goto_1
    return-void

    :cond_0
    invoke-virtual {p1}, Lcom/mediatek/contacts/calllog/CallLogListItemView;->getCallButton()Landroid/widget/ImageView;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    goto :goto_0

    :cond_1
    invoke-virtual {p1}, Lcom/mediatek/contacts/calllog/CallLogListItemView;->getCallButton()Landroid/widget/ImageView;

    move-result-object v1

    const/16 v2, 0x8

    invoke-virtual {v1, v2}, Landroid/widget/ImageView;->setVisibility(I)V

    goto :goto_1
.end method

.method protected bindChildView(Landroid/view/View;Landroid/content/Context;Landroid/database/Cursor;)V
    .locals 1
    .param p1    # Landroid/view/View;
    .param p2    # Landroid/content/Context;
    .param p3    # Landroid/database/Cursor;

    const/4 v0, 0x1

    invoke-virtual {p0, p1, p3, v0}, Lcom/android/contacts/calllog/CallLogAdapter;->bindView(Landroid/view/View;Landroid/database/Cursor;I)V

    return-void
.end method

.method protected bindGroupView(Landroid/view/View;Landroid/content/Context;Landroid/database/Cursor;IZ)V
    .locals 0
    .param p1    # Landroid/view/View;
    .param p2    # Landroid/content/Context;
    .param p3    # Landroid/database/Cursor;
    .param p4    # I
    .param p5    # Z

    invoke-virtual {p0, p1, p3, p4}, Lcom/android/contacts/calllog/CallLogAdapter;->bindView(Landroid/view/View;Landroid/database/Cursor;I)V

    return-void
.end method

.method protected bindPlayButtonView(Lcom/mediatek/contacts/calllog/CallLogListItemView;)V
    .locals 2
    .param p1    # Lcom/mediatek/contacts/calllog/CallLogListItemView;

    invoke-virtual {p1}, Lcom/mediatek/contacts/calllog/CallLogListItemView;->getCallButton()Landroid/widget/ImageView;

    move-result-object v0

    const v1, 0x7f0200a6

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    invoke-virtual {p1}, Lcom/mediatek/contacts/calllog/CallLogListItemView;->getCallButton()Landroid/widget/ImageView;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setColorFilter(I)V

    return-void
.end method

.method protected bindStandAloneView(Landroid/view/View;Landroid/content/Context;Landroid/database/Cursor;)V
    .locals 1
    .param p1    # Landroid/view/View;
    .param p2    # Landroid/content/Context;
    .param p3    # Landroid/database/Cursor;

    const/4 v0, 0x1

    invoke-virtual {p0, p1, p3, v0}, Lcom/android/contacts/calllog/CallLogAdapter;->bindView(Landroid/view/View;Landroid/database/Cursor;I)V

    return-void
.end method

.method protected bindView(Landroid/view/View;Landroid/database/Cursor;I)V
    .locals 34
    .param p1    # Landroid/view/View;
    .param p2    # Landroid/database/Cursor;
    .param p3    # I

    move-object/from16 v0, p1

    instance-of v4, v0, Lcom/mediatek/contacts/calllog/CallLogListItemView;

    if-nez v4, :cond_0

    const-string v4, "Error!!! - bindView(): view is not CallLogListItemView!"

    move-object/from16 v0, p0

    invoke-direct {v0, v4}, Lcom/android/contacts/calllog/CallLogAdapter;->log(Ljava/lang/String;)V

    :goto_0
    return-void

    :cond_0
    move-object/from16 v25, p1

    check-cast v25, Lcom/mediatek/contacts/calllog/CallLogListItemView;

    move-object/from16 v0, p0

    move-object/from16 v1, p2

    invoke-virtual {v0, v1}, Lcom/android/contacts/calllog/CallLogAdapter;->getContactInfo(Landroid/database/Cursor;)Lcom/android/contacts/calllog/ContactInfo;

    move-result-object v22

    invoke-interface/range {p2 .. p2}, Landroid/database/Cursor;->getPosition()I

    move-result v4

    const/4 v5, 0x0

    move-object/from16 v0, p2

    invoke-interface {v0, v5}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v5

    move-object/from16 v0, p0

    move/from16 v1, p3

    invoke-static {v0, v4, v5, v6, v1}, Lcom/android/contacts/calllog/IntentProvider;->getCallDetailIntentProvider(Lcom/android/contacts/calllog/CallLogAdapter;IJI)Lcom/android/contacts/calllog/IntentProvider;

    move-result-object v4

    move-object/from16 v0, v25

    invoke-virtual {v0, v4}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    const/16 v30, 0x0

    invoke-interface/range {p2 .. p2}, Landroid/database/Cursor;->getPosition()I

    move-result v4

    move-object/from16 v0, p0

    invoke-virtual {v0, v4}, Lcom/mediatek/contacts/widget/GroupingListAdapterWithHeader;->isDateGroupHeader(I)Z

    move-result v4

    if-eqz v4, :cond_4

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/contacts/calllog/CallLogAdapter;->mContext:Landroid/content/Context;

    move-object/from16 v0, v22

    iget-wide v5, v0, Lcom/android/contacts/calllog/ContactInfo;->date:J

    invoke-static {v4, v5, v6}, Lcom/mediatek/contacts/calllog/CallLogDateFormatHelper;->getFormatedDateText(Landroid/content/Context;J)Ljava/lang/String;

    move-result-object v30

    move-object/from16 v0, v25

    move-object/from16 v1, v30

    invoke-virtual {v0, v1}, Lcom/mediatek/contacts/calllog/CallLogListItemView;->setSectionDate(Ljava/lang/String;)V

    :goto_1
    move-object/from16 v0, v22

    iget-object v4, v0, Lcom/android/contacts/calllog/ContactInfo;->name:Ljava/lang/String;

    invoke-static {v4}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-eqz v4, :cond_5

    new-instance v3, Lcom/android/contacts/PhoneCallDetails;

    move-object/from16 v0, v22

    iget-object v4, v0, Lcom/android/contacts/calllog/ContactInfo;->number:Ljava/lang/String;

    move-object/from16 v0, v22

    iget-object v5, v0, Lcom/android/contacts/calllog/ContactInfo;->formattedNumber:Ljava/lang/String;

    move-object/from16 v0, v22

    iget-object v6, v0, Lcom/android/contacts/calllog/ContactInfo;->countryIso:Ljava/lang/String;

    move-object/from16 v0, v22

    iget-object v7, v0, Lcom/android/contacts/calllog/ContactInfo;->geocode:Ljava/lang/String;

    move-object/from16 v0, v22

    iget v8, v0, Lcom/android/contacts/calllog/ContactInfo;->type:I

    move-object/from16 v0, v22

    iget-wide v9, v0, Lcom/android/contacts/calllog/ContactInfo;->date:J

    move-object/from16 v0, v22

    iget-wide v11, v0, Lcom/android/contacts/calllog/ContactInfo;->duration:J

    move-object/from16 v0, v22

    iget v13, v0, Lcom/android/contacts/calllog/ContactInfo;->simId:I

    move-object/from16 v0, v22

    iget v14, v0, Lcom/android/contacts/calllog/ContactInfo;->vtCall:I

    move-object/from16 v0, v22

    iget-object v0, v0, Lcom/android/contacts/calllog/ContactInfo;->ipPrefix:Ljava/lang/String;

    move-object/from16 v16, v0

    move/from16 v15, p3

    invoke-direct/range {v3 .. v16}, Lcom/android/contacts/PhoneCallDetails;-><init>(Ljava/lang/CharSequence;Ljava/lang/CharSequence;Ljava/lang/String;Ljava/lang/String;IJJIIILjava/lang/String;)V

    :goto_2
    move-object/from16 v0, v22

    iget v4, v0, Lcom/android/contacts/calllog/ContactInfo;->isRead:I

    if-nez v4, :cond_6

    const/4 v7, 0x1

    :goto_3
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/contacts/calllog/CallLogAdapter;->mPhoneNumberHelper:Lcom/android/contacts/calllog/PhoneNumberHelper;

    iget-object v5, v3, Lcom/android/contacts/PhoneCallDetails;->number:Ljava/lang/CharSequence;

    invoke-virtual {v4, v5}, Lcom/android/contacts/calllog/PhoneNumberHelper;->isEmergencyNumber(Ljava/lang/CharSequence;)Z

    move-result v8

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/contacts/calllog/CallLogAdapter;->mPhoneNumberHelper:Lcom/android/contacts/calllog/PhoneNumberHelper;

    iget-object v5, v3, Lcom/android/contacts/PhoneCallDetails;->number:Ljava/lang/CharSequence;

    iget v6, v3, Lcom/android/contacts/PhoneCallDetails;->simId:I

    invoke-virtual {v4, v5, v6}, Lcom/android/contacts/calllog/PhoneNumberHelper;->isVoiceMailNumberForMtk(Ljava/lang/CharSequence;I)Z

    move-result v9

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/contacts/calllog/CallLogAdapter;->mPhoneNumberHelper:Lcom/android/contacts/calllog/PhoneNumberHelper;

    iget-object v5, v3, Lcom/android/contacts/PhoneCallDetails;->number:Ljava/lang/CharSequence;

    invoke-virtual {v4, v5}, Lcom/android/contacts/calllog/PhoneNumberHelper;->isSipNumber(Ljava/lang/CharSequence;)Z

    move-result v15

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/contacts/calllog/CallLogAdapter;->mPhoneCallDetailsHelper:Lcom/android/contacts/PhoneCallDetailsHelper;

    move-object/from16 v5, v25

    move-object v6, v3

    invoke-virtual/range {v4 .. v9}, Lcom/android/contacts/PhoneCallDetailsHelper;->setPhoneCallDetails(Lcom/mediatek/contacts/calllog/CallLogListItemView;Lcom/android/contacts/PhoneCallDetails;ZZZ)V

    invoke-static {}, Lcom/mediatek/contacts/util/VvmUtils;->isVvmEnabled()Z

    move-result v4

    if-eqz v4, :cond_7

    iget v4, v3, Lcom/android/contacts/PhoneCallDetails;->callType:I

    const/4 v5, 0x4

    if-ne v4, v5, :cond_7

    const-string v4, "voicemail_uri"

    move-object/from16 v0, p2

    invoke-interface {v0, v4}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v4

    move-object/from16 v0, p2

    invoke-interface {v0, v4}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v33

    const/4 v4, 0x0

    move-object/from16 v0, p2

    invoke-interface {v0, v4}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v28

    move-object/from16 v0, p0

    move-object/from16 v1, v25

    invoke-virtual {v0, v1}, Lcom/android/contacts/calllog/CallLogAdapter;->bindPlayButtonView(Lcom/mediatek/contacts/calllog/CallLogListItemView;)V

    invoke-virtual/range {v25 .. v25}, Lcom/mediatek/contacts/calllog/CallLogListItemView;->getCallButton()Landroid/widget/ImageView;

    move-result-object v4

    move-wide/from16 v0, v28

    move-object/from16 v2, v33

    invoke-static {v0, v1, v2}, Lcom/android/contacts/calllog/IntentProvider;->getPlayVoicemailIntentProvider(JLjava/lang/String;)Lcom/android/contacts/calllog/IntentProvider;

    move-result-object v5

    invoke-virtual {v4, v5}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    :goto_4
    invoke-virtual/range {v25 .. v25}, Lcom/mediatek/contacts/calllog/CallLogListItemView;->getSelectImageView()Landroid/widget/ImageView;

    move-result-object v4

    const/16 v5, 0x8

    invoke-virtual {v4, v5}, Landroid/widget/ImageView;->setVisibility(I)V

    invoke-virtual/range {v25 .. v25}, Lcom/mediatek/contacts/calllog/CallLogListItemView;->getBackgroundView()Landroid/widget/ImageView;

    move-result-object v4

    const/4 v5, 0x0

    invoke-virtual {v4, v5}, Landroid/widget/ImageView;->setVisibility(I)V

    if-nez v8, :cond_1

    if-eqz v9, :cond_8

    :cond_1
    const-wide/16 v4, 0x0

    move-object/from16 v0, v22

    iput-wide v4, v0, Lcom/android/contacts/calllog/ContactInfo;->photoId:J

    const/4 v4, 0x0

    move-object/from16 v0, v22

    iput-object v4, v0, Lcom/android/contacts/calllog/ContactInfo;->lookupUri:Landroid/net/Uri;

    :cond_2
    :goto_5
    move-object/from16 v0, v22

    iget-object v4, v0, Lcom/android/contacts/calllog/ContactInfo;->lookupUri:Landroid/net/Uri;

    if-eqz v4, :cond_c

    invoke-virtual/range {v25 .. v25}, Lcom/mediatek/contacts/calllog/CallLogListItemView;->getQuickContact()Lcom/mediatek/contacts/widget/QuickContactBadgeWithPhoneNumber;

    move-result-object v4

    move-object/from16 v0, v22

    iget-wide v5, v0, Lcom/android/contacts/calllog/ContactInfo;->photoId:J

    move-object/from16 v0, v22

    iget-object v10, v0, Lcom/android/contacts/calllog/ContactInfo;->lookupUri:Landroid/net/Uri;

    move-object/from16 v0, p0

    invoke-virtual {v0, v4, v5, v6, v10}, Lcom/android/contacts/calllog/CallLogAdapter;->setPhoto(Lcom/mediatek/contacts/widget/QuickContactBadgeWithPhoneNumber;JLandroid/net/Uri;)V

    :goto_6
    const/16 v26, 0x0

    if-eqz v3, :cond_3

    iget-object v4, v3, Lcom/android/contacts/PhoneCallDetails;->number:Ljava/lang/CharSequence;

    if-eqz v4, :cond_3

    iget-object v4, v3, Lcom/android/contacts/PhoneCallDetails;->number:Ljava/lang/CharSequence;

    invoke-virtual {v4}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v26

    :cond_3
    invoke-static {}, Lcom/mediatek/contacts/ExtensionManager;->getInstance()Lcom/mediatek/contacts/ExtensionManager;

    move-result-object v4

    invoke-virtual {v4}, Lcom/mediatek/contacts/ExtensionManager;->getCallListExtension()Lcom/android/contacts/ext/CallListExtension;

    move-result-object v4

    const-string v5, "ExtenstionForRCS"

    move-object/from16 v0, v26

    invoke-virtual {v4, v0, v5}, Lcom/android/contacts/ext/CallListExtension;->setExtentionIcon(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v27

    invoke-virtual/range {v25 .. v25}, Lcom/mediatek/contacts/calllog/CallLogListItemView;->removeExtentionIconView()V

    move-object/from16 v0, v25

    move/from16 v1, v27

    invoke-virtual {v0, v1}, Lcom/mediatek/contacts/calllog/CallLogListItemView;->setExtentionIcon(Z)V

    goto/16 :goto_0

    :cond_4
    const/4 v4, 0x0

    move-object/from16 v0, v25

    invoke-virtual {v0, v4}, Lcom/mediatek/contacts/calllog/CallLogListItemView;->setSectionDate(Ljava/lang/String;)V

    goto/16 :goto_1

    :cond_5
    new-instance v3, Lcom/android/contacts/PhoneCallDetails;

    move-object/from16 v0, v22

    iget-object v4, v0, Lcom/android/contacts/calllog/ContactInfo;->number:Ljava/lang/String;

    move-object/from16 v0, v22

    iget-object v5, v0, Lcom/android/contacts/calllog/ContactInfo;->formattedNumber:Ljava/lang/String;

    move-object/from16 v0, v22

    iget-object v6, v0, Lcom/android/contacts/calllog/ContactInfo;->countryIso:Ljava/lang/String;

    move-object/from16 v0, v22

    iget-object v7, v0, Lcom/android/contacts/calllog/ContactInfo;->geocode:Ljava/lang/String;

    move-object/from16 v0, v22

    iget v8, v0, Lcom/android/contacts/calllog/ContactInfo;->type:I

    move-object/from16 v0, v22

    iget-wide v9, v0, Lcom/android/contacts/calllog/ContactInfo;->date:J

    move-object/from16 v0, v22

    iget-wide v11, v0, Lcom/android/contacts/calllog/ContactInfo;->duration:J

    move-object/from16 v0, v22

    iget-object v13, v0, Lcom/android/contacts/calllog/ContactInfo;->name:Ljava/lang/String;

    move-object/from16 v0, v22

    iget v14, v0, Lcom/android/contacts/calllog/ContactInfo;->nNumberTypeId:I

    move-object/from16 v0, v22

    iget-object v15, v0, Lcom/android/contacts/calllog/ContactInfo;->label:Ljava/lang/String;

    move-object/from16 v0, v22

    iget-object v0, v0, Lcom/android/contacts/calllog/ContactInfo;->lookupUri:Landroid/net/Uri;

    move-object/from16 v16, v0

    const/16 v17, 0x0

    move-object/from16 v0, v22

    iget v0, v0, Lcom/android/contacts/calllog/ContactInfo;->simId:I

    move/from16 v18, v0

    move-object/from16 v0, v22

    iget v0, v0, Lcom/android/contacts/calllog/ContactInfo;->vtCall:I

    move/from16 v19, v0

    move-object/from16 v0, v22

    iget-object v0, v0, Lcom/android/contacts/calllog/ContactInfo;->ipPrefix:Ljava/lang/String;

    move-object/from16 v21, v0

    move/from16 v20, p3

    invoke-direct/range {v3 .. v21}, Lcom/android/contacts/PhoneCallDetails;-><init>(Ljava/lang/CharSequence;Ljava/lang/CharSequence;Ljava/lang/String;Ljava/lang/String;IJJLjava/lang/CharSequence;ILjava/lang/CharSequence;Landroid/net/Uri;Landroid/net/Uri;IIILjava/lang/String;)V

    goto/16 :goto_2

    :cond_6
    const/4 v7, 0x0

    goto/16 :goto_3

    :cond_7
    move-object/from16 v0, p0

    move-object/from16 v1, v25

    invoke-virtual {v0, v1, v3}, Lcom/android/contacts/calllog/CallLogAdapter;->bindCallButtonView(Lcom/mediatek/contacts/calllog/CallLogListItemView;Lcom/android/contacts/PhoneCallDetails;)V

    goto/16 :goto_4

    :cond_8
    move-object/from16 v0, v22

    iget v4, v0, Lcom/android/contacts/calllog/ContactInfo;->contactSimId:I

    if-lez v4, :cond_2

    invoke-static {}, Lcom/mediatek/phone/SIMInfoWrapper;->getDefault()Lcom/mediatek/phone/SIMInfoWrapper;

    move-result-object v4

    move-object/from16 v0, v22

    iget v5, v0, Lcom/android/contacts/calllog/ContactInfo;->contactSimId:I

    invoke-virtual {v4, v5}, Lcom/mediatek/phone/SIMInfoWrapper;->getSlotIdBySimId(I)I

    move-result v32

    const/16 v23, -0x1

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/contacts/calllog/CallLogAdapter;->mSimInfoWrapper:Lcom/mediatek/phone/SIMInfoWrapper;

    if-nez v4, :cond_9

    invoke-static {}, Lcom/mediatek/phone/SIMInfoWrapper;->getDefault()Lcom/mediatek/phone/SIMInfoWrapper;

    move-result-object v4

    move-object/from16 v0, p0

    iput-object v4, v0, Lcom/android/contacts/calllog/CallLogAdapter;->mSimInfoWrapper:Lcom/mediatek/phone/SIMInfoWrapper;

    :cond_9
    const/16 v24, -0x1

    if-eqz p2, :cond_a

    const/16 v4, 0x16

    move-object/from16 v0, p2

    invoke-interface {v0, v4}, Landroid/database/Cursor;->getInt(I)I

    move-result v24

    :cond_a
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/contacts/calllog/CallLogAdapter;->mSimInfoWrapper:Lcom/mediatek/phone/SIMInfoWrapper;

    move/from16 v0, v32

    invoke-virtual {v4, v0}, Lcom/mediatek/phone/SIMInfoWrapper;->getSimInfoBySlot(I)Landroid/provider/Telephony$SIMInfo;

    move-result-object v31

    if-eqz v31, :cond_b

    move-object/from16 v0, v31

    iget v0, v0, Landroid/provider/Telephony$SIMInfo;->mColor:I

    move/from16 v23, v0

    :cond_b
    new-instance v4, Lcom/mediatek/contacts/util/SimContactPhotoUtils;

    invoke-direct {v4}, Lcom/mediatek/contacts/util/SimContactPhotoUtils;-><init>()V

    move/from16 v0, v24

    move/from16 v1, v23

    invoke-virtual {v4, v0, v1}, Lcom/mediatek/contacts/util/SimContactPhotoUtils;->getPhotoId(II)J

    move-result-wide v4

    move-object/from16 v0, v22

    iput-wide v4, v0, Lcom/android/contacts/calllog/ContactInfo;->photoId:J

    goto/16 :goto_5

    :cond_c
    invoke-virtual/range {v25 .. v25}, Lcom/mediatek/contacts/calllog/CallLogListItemView;->getQuickContact()Lcom/mediatek/contacts/widget/QuickContactBadgeWithPhoneNumber;

    move-result-object v11

    move-object/from16 v0, v22

    iget-wide v12, v0, Lcom/android/contacts/calllog/ContactInfo;->photoId:J

    move-object/from16 v0, v22

    iget-object v14, v0, Lcom/android/contacts/calllog/ContactInfo;->number:Ljava/lang/String;

    move-object/from16 v10, p0

    invoke-virtual/range {v10 .. v15}, Lcom/android/contacts/calllog/CallLogAdapter;->setPhoto(Lcom/mediatek/contacts/widget/QuickContactBadgeWithPhoneNumber;JLjava/lang/String;Z)V

    goto/16 :goto_6
.end method

.method public changeCursor(Landroid/database/Cursor;)V
    .locals 2
    .param p1    # Landroid/database/Cursor;

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "changeCursor(), cursor = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/android/contacts/calllog/CallLogAdapter;->log(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/android/contacts/calllog/CallLogAdapter;->mCursor:Landroid/database/Cursor;

    if-eq v0, p1, :cond_0

    iput-object p1, p0, Lcom/android/contacts/calllog/CallLogAdapter;->mCursor:Landroid/database/Cursor;

    iget-object v0, p0, Lcom/android/contacts/calllog/CallLogAdapter;->mContactInfoMap:Ljava/util/HashMap;

    invoke-virtual {v0}, Ljava/util/HashMap;->clear()V

    :cond_0
    invoke-super {p0, p1}, Lcom/mediatek/contacts/widget/GroupingListAdapterWithHeader;->changeCursor(Landroid/database/Cursor;)V

    return-void
.end method

.method public clearCachedContactInfo()V
    .locals 1

    iget-object v0, p0, Lcom/android/contacts/calllog/CallLogAdapter;->mContactInfoMap:Ljava/util/HashMap;

    invoke-virtual {v0}, Ljava/util/HashMap;->clear()V

    return-void
.end method

.method disableRequestProcessingForTest()V
    .locals 1
    .annotation build Lcom/google/common/annotations/VisibleForTesting;
    .end annotation

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/android/contacts/calllog/CallLogAdapter;->mRequestProcessingDisabled:Z

    return-void
.end method

.method protected getContactInfo(Landroid/database/Cursor;)Lcom/android/contacts/calllog/ContactInfo;
    .locals 4
    .param p1    # Landroid/database/Cursor;

    const/4 v1, 0x0

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const/4 v3, 0x1

    invoke-interface {p1, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const/4 v3, 0x2

    invoke-interface {p1, v3}, Landroid/database/Cursor;->getInt(I)I

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    iget-object v2, p0, Lcom/android/contacts/calllog/CallLogAdapter;->mContactInfoMap:Ljava/util/HashMap;

    invoke-virtual {v2, v0}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/android/contacts/calllog/ContactInfo;

    if-nez v1, :cond_0

    invoke-virtual {p0, p1}, Lcom/android/contacts/calllog/CallLogAdapter;->getContactInfoFromCallLog(Landroid/database/Cursor;)Lcom/android/contacts/calllog/ContactInfo;

    move-result-object v1

    iget-object v2, p0, Lcom/android/contacts/calllog/CallLogAdapter;->mContactInfoMap:Ljava/util/HashMap;

    invoke-virtual {v2, v0, v1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    :cond_0
    return-object v1
.end method

.method protected getContactInfoFromCallLog(Landroid/database/Cursor;)Lcom/android/contacts/calllog/ContactInfo;
    .locals 1
    .param p1    # Landroid/database/Cursor;

    invoke-static {p1}, Lcom/android/contacts/calllog/ContactInfo;->fromCursor(Landroid/database/Cursor;)Lcom/android/contacts/calllog/ContactInfo;

    move-result-object v0

    return-object v0
.end method

.method injectContactInfoForTest(Ljava/lang/String;Ljava/lang/String;Lcom/android/contacts/calllog/ContactInfo;)V
    .locals 2
    .param p1    # Ljava/lang/String;
    .param p2    # Ljava/lang/String;
    .param p3    # Lcom/android/contacts/calllog/ContactInfo;
    .annotation build Lcom/google/common/annotations/VisibleForTesting;
    .end annotation

    new-instance v0, Lcom/android/contacts/calllog/CallLogAdapter$NumberWithCountryIso;

    invoke-direct {v0, p1, p2}, Lcom/android/contacts/calllog/CallLogAdapter$NumberWithCountryIso;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v1, p0, Lcom/android/contacts/calllog/CallLogAdapter;->mContactInfoCache:Lcom/android/contacts/util/ExpirableCache;

    invoke-virtual {v1, v0, p3}, Lcom/android/contacts/util/ExpirableCache;->put(Ljava/lang/Object;Ljava/lang/Object;)V

    return-void
.end method

.method public isEmpty()Z
    .locals 1

    iget-boolean v0, p0, Lcom/android/contacts/calllog/CallLogAdapter;->mLoading:Z

    if-eqz v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_0
    invoke-super {p0}, Landroid/widget/BaseAdapter;->isEmpty()Z

    move-result v0

    goto :goto_0
.end method

.method protected newCallLogItemView(Landroid/content/Context;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 2
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/view/ViewGroup;

    new-instance v0, Lcom/mediatek/contacts/calllog/CallLogListItemView;

    const/4 v1, 0x0

    invoke-direct {v0, p1, v1}, Lcom/mediatek/contacts/calllog/CallLogListItemView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    iget-object v1, p0, Lcom/android/contacts/calllog/CallLogAdapter;->mSecondaryActionListener:Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v1}, Lcom/mediatek/contacts/calllog/CallLogListItemView;->setOnCallButtonClickListener(Landroid/view/View$OnClickListener;)V

    return-object v0
.end method

.method protected newChildView(Landroid/content/Context;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 1
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/view/ViewGroup;

    invoke-virtual {p0, p1, p2}, Lcom/android/contacts/calllog/CallLogAdapter;->newCallLogItemView(Landroid/content/Context;Landroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    return-object v0
.end method

.method protected newGroupView(Landroid/content/Context;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 1
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/view/ViewGroup;

    invoke-virtual {p0, p1, p2}, Lcom/android/contacts/calllog/CallLogAdapter;->newCallLogItemView(Landroid/content/Context;Landroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    return-object v0
.end method

.method protected newStandAloneView(Landroid/content/Context;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 1
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/view/ViewGroup;

    invoke-virtual {p0, p1, p2}, Lcom/android/contacts/calllog/CallLogAdapter;->newCallLogItemView(Landroid/content/Context;Landroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    return-object v0
.end method

.method protected onContentChanged()V
    .locals 1

    iget-object v0, p0, Lcom/android/contacts/calllog/CallLogAdapter;->mCallFetcher:Lcom/android/contacts/calllog/CallLogAdapter$CallFetcher;

    invoke-interface {v0}, Lcom/android/contacts/calllog/CallLogAdapter$CallFetcher;->fetchCalls()V

    return-void
.end method

.method public onScroll(Landroid/widget/AbsListView;III)V
    .locals 0
    .param p1    # Landroid/widget/AbsListView;
    .param p2    # I
    .param p3    # I
    .param p4    # I

    return-void
.end method

.method public onScrollStateChanged(Landroid/widget/AbsListView;I)V
    .locals 1
    .param p1    # Landroid/widget/AbsListView;
    .param p2    # I

    if-eqz p2, :cond_0

    const/4 v0, 0x1

    if-ne p2, v0, :cond_1

    :cond_0
    iget-object v0, p0, Lcom/android/contacts/calllog/CallLogAdapter;->mContactPhotoManager:Lcom/android/contacts/ContactPhotoManager;

    invoke-virtual {v0}, Lcom/android/contacts/ContactPhotoManager;->resume()V

    :goto_0
    return-void

    :cond_1
    iget-object v0, p0, Lcom/android/contacts/calllog/CallLogAdapter;->mContactPhotoManager:Lcom/android/contacts/ContactPhotoManager;

    invoke-virtual {v0}, Lcom/android/contacts/ContactPhotoManager;->pause()V

    goto :goto_0
.end method

.method public setGroupHeaderPosition(I)V
    .locals 0
    .param p1    # I

    invoke-super {p0, p1}, Lcom/mediatek/contacts/widget/GroupingListAdapterWithHeader;->setGroupHeaderPosition(I)V

    return-void
.end method

.method public setLoading(Z)V
    .locals 0
    .param p1    # Z

    iput-boolean p1, p0, Lcom/android/contacts/calllog/CallLogAdapter;->mLoading:Z

    return-void
.end method

.method protected setPhoto(Lcom/mediatek/contacts/widget/QuickContactBadgeWithPhoneNumber;JLandroid/net/Uri;)V
    .locals 2
    .param p1    # Lcom/mediatek/contacts/widget/QuickContactBadgeWithPhoneNumber;
    .param p2    # J
    .param p4    # Landroid/net/Uri;

    const/4 v0, 0x0

    const/4 v1, 0x0

    invoke-virtual {p1, v0, v1}, Lcom/mediatek/contacts/widget/QuickContactBadgeWithPhoneNumber;->assignPhoneNumber(Ljava/lang/String;Z)V

    invoke-virtual {p1, p4}, Landroid/widget/QuickContactBadge;->assignContactUri(Landroid/net/Uri;)V

    iget-object v0, p0, Lcom/android/contacts/calllog/CallLogAdapter;->mContactPhotoManager:Lcom/android/contacts/ContactPhotoManager;

    const/4 v1, 0x1

    invoke-virtual {v0, p1, p2, p3, v1}, Lcom/android/contacts/ContactPhotoManager;->loadThumbnail(Landroid/widget/ImageView;JZ)V

    return-void
.end method

.method protected setPhoto(Lcom/mediatek/contacts/widget/QuickContactBadgeWithPhoneNumber;JLjava/lang/String;Z)V
    .locals 2
    .param p1    # Lcom/mediatek/contacts/widget/QuickContactBadgeWithPhoneNumber;
    .param p2    # J
    .param p4    # Ljava/lang/String;
    .param p5    # Z

    const/4 v0, 0x0

    invoke-virtual {p1, v0}, Landroid/widget/QuickContactBadge;->assignContactUri(Landroid/net/Uri;)V

    invoke-virtual {p1, p4, p5}, Lcom/mediatek/contacts/widget/QuickContactBadgeWithPhoneNumber;->assignPhoneNumber(Ljava/lang/String;Z)V

    iget-object v0, p0, Lcom/android/contacts/calllog/CallLogAdapter;->mContactPhotoManager:Lcom/android/contacts/ContactPhotoManager;

    const/4 v1, 0x1

    invoke-virtual {v0, p1, p2, p3, v1}, Lcom/android/contacts/ContactPhotoManager;->loadThumbnail(Landroid/widget/ImageView;JZ)V

    return-void
.end method
