.class public Lcom/android/contacts/activities/PeopleActivity$AccountCategoryInfo;
.super Ljava/lang/Object;
.source "PeopleActivity.java"

# interfaces
.implements Landroid/os/Parcelable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/contacts/activities/PeopleActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "AccountCategoryInfo"
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/android/contacts/activities/PeopleActivity$AccountCategoryInfo;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public mAccountCategory:Ljava/lang/String;

.field public mSimId:I

.field public mSimName:Ljava/lang/String;

.field public mSlotId:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Lcom/android/contacts/activities/PeopleActivity$AccountCategoryInfo$1;

    invoke-direct {v0}, Lcom/android/contacts/activities/PeopleActivity$AccountCategoryInfo$1;-><init>()V

    sput-object v0, Lcom/android/contacts/activities/PeopleActivity$AccountCategoryInfo;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method private constructor <init>(Landroid/os/Parcel;)V
    .locals 1
    .param p1    # Landroid/os/Parcel;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/android/contacts/activities/PeopleActivity$AccountCategoryInfo;->mAccountCategory:Ljava/lang/String;

    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/android/contacts/activities/PeopleActivity$AccountCategoryInfo;->mSlotId:I

    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/android/contacts/activities/PeopleActivity$AccountCategoryInfo;->mSimId:I

    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/android/contacts/activities/PeopleActivity$AccountCategoryInfo;->mSimName:Ljava/lang/String;

    return-void
.end method

.method synthetic constructor <init>(Landroid/os/Parcel;Lcom/android/contacts/activities/PeopleActivity$1;)V
    .locals 0
    .param p1    # Landroid/os/Parcel;
    .param p2    # Lcom/android/contacts/activities/PeopleActivity$1;

    invoke-direct {p0, p1}, Lcom/android/contacts/activities/PeopleActivity$AccountCategoryInfo;-><init>(Landroid/os/Parcel;)V

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;IILjava/lang/String;)V
    .locals 0
    .param p1    # Ljava/lang/String;
    .param p2    # I
    .param p3    # I
    .param p4    # Ljava/lang/String;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/android/contacts/activities/PeopleActivity$AccountCategoryInfo;->mAccountCategory:Ljava/lang/String;

    iput p2, p0, Lcom/android/contacts/activities/PeopleActivity$AccountCategoryInfo;->mSlotId:I

    iput p3, p0, Lcom/android/contacts/activities/PeopleActivity$AccountCategoryInfo;->mSimId:I

    iput-object p4, p0, Lcom/android/contacts/activities/PeopleActivity$AccountCategoryInfo;->mSimName:Ljava/lang/String;

    return-void
.end method


# virtual methods
.method public describeContents()I
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 1
    .param p1    # Landroid/os/Parcel;
    .param p2    # I

    iget-object v0, p0, Lcom/android/contacts/activities/PeopleActivity$AccountCategoryInfo;->mAccountCategory:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    iget v0, p0, Lcom/android/contacts/activities/PeopleActivity$AccountCategoryInfo;->mSlotId:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    iget v0, p0, Lcom/android/contacts/activities/PeopleActivity$AccountCategoryInfo;->mSimId:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    iget-object v0, p0, Lcom/android/contacts/activities/PeopleActivity$AccountCategoryInfo;->mSimName:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    return-void
.end method
