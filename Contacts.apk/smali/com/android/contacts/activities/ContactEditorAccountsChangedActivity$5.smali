.class Lcom/android/contacts/activities/ContactEditorAccountsChangedActivity$5;
.super Ljava/lang/Object;
.source "ContactEditorAccountsChangedActivity.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/contacts/activities/ContactEditorAccountsChangedActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/contacts/activities/ContactEditorAccountsChangedActivity;


# direct methods
.method constructor <init>(Lcom/android/contacts/activities/ContactEditorAccountsChangedActivity;)V
    .locals 0

    iput-object p1, p0, Lcom/android/contacts/activities/ContactEditorAccountsChangedActivity$5;->this$0:Lcom/android/contacts/activities/ContactEditorAccountsChangedActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 9

    const/4 v8, 0x1

    # getter for: Lcom/android/contacts/activities/ContactEditorAccountsChangedActivity;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/android/contacts/activities/ContactEditorAccountsChangedActivity;->access$300()Ljava/lang/String;

    move-result-object v4

    const-string v5, "mServiceComplete run"

    invoke-static {v4, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v4, p0, Lcom/android/contacts/activities/ContactEditorAccountsChangedActivity$5;->this$0:Lcom/android/contacts/activities/ContactEditorAccountsChangedActivity;

    # getter for: Lcom/android/contacts/activities/ContactEditorAccountsChangedActivity;->mCellMgr:Lcom/mediatek/CellConnService/CellConnMgr;
    invoke-static {v4}, Lcom/android/contacts/activities/ContactEditorAccountsChangedActivity;->access$200(Lcom/android/contacts/activities/ContactEditorAccountsChangedActivity;)Lcom/mediatek/CellConnService/CellConnMgr;

    move-result-object v4

    invoke-virtual {v4}, Lcom/mediatek/CellConnService/CellConnMgr;->getResult()I

    move-result v2

    # getter for: Lcom/android/contacts/activities/ContactEditorAccountsChangedActivity;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/android/contacts/activities/ContactEditorAccountsChangedActivity;->access$300()Ljava/lang/String;

    move-result-object v4

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "mServiceComplete result = "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-static {v2}, Lcom/mediatek/CellConnService/CellConnMgr;->resultToString(I)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v4, p0, Lcom/android/contacts/activities/ContactEditorAccountsChangedActivity$5;->this$0:Lcom/android/contacts/activities/ContactEditorAccountsChangedActivity;

    # getter for: Lcom/android/contacts/activities/ContactEditorAccountsChangedActivity;->mCellMgr:Lcom/mediatek/CellConnService/CellConnMgr;
    invoke-static {v4}, Lcom/android/contacts/activities/ContactEditorAccountsChangedActivity;->access$200(Lcom/android/contacts/activities/ContactEditorAccountsChangedActivity;)Lcom/mediatek/CellConnService/CellConnMgr;

    const/4 v4, 0x2

    if-ne v4, v2, :cond_0

    iget-object v4, p0, Lcom/android/contacts/activities/ContactEditorAccountsChangedActivity$5;->this$0:Lcom/android/contacts/activities/ContactEditorAccountsChangedActivity;

    invoke-virtual {v4}, Lcom/android/contacts/activities/ContactEditorAccountsChangedActivity;->finish()V

    :goto_0
    return-void

    :cond_0
    const/4 v0, -0x1

    const/4 v1, 0x0

    iget-object v4, p0, Lcom/android/contacts/activities/ContactEditorAccountsChangedActivity$5;->this$0:Lcom/android/contacts/activities/ContactEditorAccountsChangedActivity;

    # getter for: Lcom/android/contacts/activities/ContactEditorAccountsChangedActivity;->mSlotId:I
    invoke-static {v4}, Lcom/android/contacts/activities/ContactEditorAccountsChangedActivity;->access$100(Lcom/android/contacts/activities/ContactEditorAccountsChangedActivity;)I

    move-result v4

    invoke-static {v4}, Lcom/mediatek/contacts/simcontact/SimCardUtils;->isPhoneBookReady(I)Z

    move-result v4

    if-nez v4, :cond_2

    const/4 v1, 0x1

    const v0, 0x7f0c0040

    :cond_1
    :goto_1
    if-eqz v1, :cond_3

    iget-object v4, p0, Lcom/android/contacts/activities/ContactEditorAccountsChangedActivity$5;->this$0:Lcom/android/contacts/activities/ContactEditorAccountsChangedActivity;

    invoke-static {v4, v0, v8}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v4

    invoke-virtual {v4}, Landroid/widget/Toast;->show()V

    goto :goto_0

    :cond_2
    iget-object v4, p0, Lcom/android/contacts/activities/ContactEditorAccountsChangedActivity$5;->this$0:Lcom/android/contacts/activities/ContactEditorAccountsChangedActivity;

    # getter for: Lcom/android/contacts/activities/ContactEditorAccountsChangedActivity;->mSlotId:I
    invoke-static {v4}, Lcom/android/contacts/activities/ContactEditorAccountsChangedActivity;->access$100(Lcom/android/contacts/activities/ContactEditorAccountsChangedActivity;)I

    move-result v4

    invoke-static {v4}, Lcom/mediatek/contacts/simcontact/SimCardUtils$ShowSimCardStorageInfoTask;->getSurplugCount(I)I

    move-result v4

    if-nez v4, :cond_1

    const/4 v1, 0x1

    const v0, 0x7f0c004d

    goto :goto_1

    :cond_3
    iget-object v4, p0, Lcom/android/contacts/activities/ContactEditorAccountsChangedActivity$5;->this$0:Lcom/android/contacts/activities/ContactEditorAccountsChangedActivity;

    iget-object v5, p0, Lcom/android/contacts/activities/ContactEditorAccountsChangedActivity$5;->this$0:Lcom/android/contacts/activities/ContactEditorAccountsChangedActivity;

    # getter for: Lcom/android/contacts/activities/ContactEditorAccountsChangedActivity;->mSlotId:I
    invoke-static {v5}, Lcom/android/contacts/activities/ContactEditorAccountsChangedActivity;->access$100(Lcom/android/contacts/activities/ContactEditorAccountsChangedActivity;)I

    move-result v5

    invoke-static {v4, v5}, Landroid/provider/Telephony$SIMInfo;->getSIMInfoBySlot(Landroid/content/Context;I)Landroid/provider/Telephony$SIMInfo;

    move-result-object v3

    if-eqz v3, :cond_4

    iget-object v4, p0, Lcom/android/contacts/activities/ContactEditorAccountsChangedActivity$5;->this$0:Lcom/android/contacts/activities/ContactEditorAccountsChangedActivity;

    iget-wide v5, v3, Landroid/provider/Telephony$SIMInfo;->mSimId:J

    # setter for: Lcom/android/contacts/activities/ContactEditorAccountsChangedActivity;->mSimId:J
    invoke-static {v4, v5, v6}, Lcom/android/contacts/activities/ContactEditorAccountsChangedActivity;->access$602(Lcom/android/contacts/activities/ContactEditorAccountsChangedActivity;J)J

    :cond_4
    # getter for: Lcom/android/contacts/activities/ContactEditorAccountsChangedActivity;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/android/contacts/activities/ContactEditorAccountsChangedActivity;->access$300()Ljava/lang/String;

    move-result-object v4

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "mSimSelectionDialog mSimId is "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget-object v6, p0, Lcom/android/contacts/activities/ContactEditorAccountsChangedActivity$5;->this$0:Lcom/android/contacts/activities/ContactEditorAccountsChangedActivity;

    # getter for: Lcom/android/contacts/activities/ContactEditorAccountsChangedActivity;->mSimId:J
    invoke-static {v6}, Lcom/android/contacts/activities/ContactEditorAccountsChangedActivity;->access$600(Lcom/android/contacts/activities/ContactEditorAccountsChangedActivity;)J

    move-result-wide v6

    invoke-virtual {v5, v6, v7}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v4, p0, Lcom/android/contacts/activities/ContactEditorAccountsChangedActivity$5;->this$0:Lcom/android/contacts/activities/ContactEditorAccountsChangedActivity;

    # setter for: Lcom/android/contacts/activities/ContactEditorAccountsChangedActivity;->mNewSimType:Z
    invoke-static {v4, v8}, Lcom/android/contacts/activities/ContactEditorAccountsChangedActivity;->access$702(Lcom/android/contacts/activities/ContactEditorAccountsChangedActivity;Z)Z

    iget-object v4, p0, Lcom/android/contacts/activities/ContactEditorAccountsChangedActivity$5;->this$0:Lcom/android/contacts/activities/ContactEditorAccountsChangedActivity;

    iget-object v5, p0, Lcom/android/contacts/activities/ContactEditorAccountsChangedActivity$5;->this$0:Lcom/android/contacts/activities/ContactEditorAccountsChangedActivity;

    # getter for: Lcom/android/contacts/activities/ContactEditorAccountsChangedActivity;->mAccountListAdapter:Lcom/android/contacts/util/AccountsListAdapter;
    invoke-static {v5}, Lcom/android/contacts/activities/ContactEditorAccountsChangedActivity;->access$000(Lcom/android/contacts/activities/ContactEditorAccountsChangedActivity;)Lcom/android/contacts/util/AccountsListAdapter;

    move-result-object v5

    iget-object v6, p0, Lcom/android/contacts/activities/ContactEditorAccountsChangedActivity$5;->this$0:Lcom/android/contacts/activities/ContactEditorAccountsChangedActivity;

    # getter for: Lcom/android/contacts/activities/ContactEditorAccountsChangedActivity;->mPosition:I
    invoke-static {v6}, Lcom/android/contacts/activities/ContactEditorAccountsChangedActivity;->access$800(Lcom/android/contacts/activities/ContactEditorAccountsChangedActivity;)I

    move-result v6

    invoke-virtual {v5, v6}, Lcom/android/contacts/util/AccountsListAdapter;->getItem(I)Lcom/android/contacts/model/account/AccountWithDataSet;

    move-result-object v5

    # invokes: Lcom/android/contacts/activities/ContactEditorAccountsChangedActivity;->saveAccountAndReturnResult(Lcom/android/contacts/model/account/AccountWithDataSet;)V
    invoke-static {v4, v5}, Lcom/android/contacts/activities/ContactEditorAccountsChangedActivity;->access$400(Lcom/android/contacts/activities/ContactEditorAccountsChangedActivity;Lcom/android/contacts/model/account/AccountWithDataSet;)V

    goto :goto_0
.end method
