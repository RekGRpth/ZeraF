.class public final Lcom/android/contacts/R$dimen;
.super Ljava/lang/Object;
.source "R.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/contacts/R;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "dimen"
.end annotation


# static fields
.field public static final account_container_left_padding:I = 0x7f09002a

.field public static final account_selector_popup_width:I = 0x7f090000

.field public static final association_sim_leftright_padding:I = 0x7f09006e

.field public static final button_grid_layout_button_height:I = 0x7f090072

.field public static final button_grid_layout_button_width:I = 0x7f090071

.field public static final call_button_height:I = 0x7f090039

.field public static final call_detail_button_spacing:I = 0x7f090050

.field public static final call_detail_contact_name_margin:I = 0x7f09004f

.field public static final call_detail_simname_text_size:I = 0x7f09007c

.field public static final call_detail_without_voicemail_sim_name_max_width:I = 0x7f09006f

.field public static final call_log_call_action_height:I = 0x7f090070

.field public static final call_log_call_action_size:I = 0x7f090047

.field public static final call_log_call_action_width:I = 0x7f090048

.field public static final call_log_icon_margin:I = 0x7f090049

.field public static final call_log_indent_margin:I = 0x7f09004c

.field public static final call_log_inner_margin:I = 0x7f09004a

.field public static final call_log_list_contact_photo_size:I = 0x7f09004e

.field public static final call_log_list_item_height:I = 0x7f09004d

.field public static final call_log_outer_margin:I = 0x7f09004b

.field public static final calllog_auto_rejected_cluster_height:I = 0x7f09008b

.field public static final calllog_list_item_calltype_simname_padding:I = 0x7f09006a

.field public static final calllog_list_item_label_simname_padding:I = 0x7f090069

.field public static final calllog_list_item_name_max_length:I = 0x7f090086

.field public static final calllog_list_item_number_max_length:I = 0x7f090087

.field public static final calllog_list_item_quick_contact_padding_bottom:I = 0x7f090081

.field public static final calllog_list_item_quick_contact_padding_top:I = 0x7f090080

.field public static final calllog_list_item_simname_bottom_margin:I = 0x7f09006b

.field public static final calllog_list_item_simname_font_size:I = 0x7f09006c

.field public static final calllog_list_item_simname_height:I = 0x7f090067

.field public static final calllog_list_item_simname_max_length:I = 0x7f09006d

.field public static final calllog_list_item_simname_max_length1:I = 0x7f09007d

.field public static final calllog_list_item_simname_max_length2:I = 0x7f09007e

.field public static final calllog_list_item_simname_max_length3:I = 0x7f09007f

.field public static final calllog_list_item_simname_text_size:I = 0x7f09007b

.field public static final calllog_list_item_simname_top_padding:I = 0x7f090068

.field public static final calllog_list_margin_left:I = 0x7f090084

.field public static final calllog_list_margin_right:I = 0x7f090085

.field public static final calllog_list_padding_top:I = 0x7f090088

.field public static final calllog_multi_select_list_item_checkbox_height:I = 0x7f090083

.field public static final calllog_multi_select_list_item_checkbox_width:I = 0x7f090082

.field public static final calllog_search_button_all_tab_text_height:I = 0x7f090097

.field public static final calllog_search_button_all_tab_text_margin_left:I = 0x7f090099

.field public static final calllog_search_button_all_tab_text_size:I = 0x7f09009a

.field public static final calllog_search_button_all_tab_text_width:I = 0x7f090098

.field public static final calllog_search_button_all_width:I = 0x7f09008d

.field public static final calllog_search_button_divider_height:I = 0x7f090095

.field public static final calllog_search_button_divider_margin_left:I = 0x7f090096

.field public static final calllog_search_button_divider_width:I = 0x7f090094

.field public static final calllog_search_button_height:I = 0x7f09008c

.field public static final calllog_search_button_incoming_margin_left:I = 0x7f09008f

.field public static final calllog_search_button_incoming_width:I = 0x7f09008e

.field public static final calllog_search_button_missed_margin_left:I = 0x7f090093

.field public static final calllog_search_button_missed_width:I = 0x7f090092

.field public static final calllog_search_button_outgoing_margin_left:I = 0x7f090091

.field public static final calllog_search_button_outgoing_width:I = 0x7f090090

.field public static final calllog_search_image_icon_height:I = 0x7f09009c

.field public static final calllog_search_image_icon_margin_left:I = 0x7f09009e

.field public static final calllog_search_image_icon_margin_top:I = 0x7f09009d

.field public static final calllog_search_image_icon_width:I = 0x7f09009b

.field public static final contact_browser_list_header_left_margin:I = 0x7f09003b

.field public static final contact_browser_list_header_right_margin:I = 0x7f09003c

.field public static final contact_browser_list_item_photo_size:I = 0x7f09003d

.field public static final contact_browser_list_item_text_indent:I = 0x7f09003e

.field public static final contact_browser_list_top_margin:I = 0x7f09003f

.field public static final contact_detail_list_top_padding:I = 0x7f0900a1

.field public static final contact_filter_header_min_height:I = 0x7f090058

.field public static final contact_filter_icon_size:I = 0x7f090057

.field public static final contact_filter_item_min_height:I = 0x7f090056

.field public static final contact_filter_left_margin:I = 0x7f090054

.field public static final contact_filter_right_margin:I = 0x7f090055

.field public static final contact_import_export_icon_size:I = 0x7f090059

.field public static final contact_import_export_item_min_height:I = 0x7f09005a

.field public static final contact_picker_contact_list_min_height:I = 0x7f09005f

.field public static final contact_picker_search_view_max_width:I = 0x7f09005e

.field public static final contact_tile_divider_padding:I = 0x7f09005b

.field public static final contact_tile_list_padding_top:I = 0x7f090046

.field public static final contact_tile_shadowbox_height:I = 0x7f090045

.field public static final detail_contact_photo_expanded_size:I = 0x7f090018

.field public static final detail_contact_photo_margin:I = 0x7f090016

.field public static final detail_contact_photo_shadow_height:I = 0x7f09000c

.field public static final detail_contact_photo_size:I = 0x7f090017

.field public static final detail_header_name_text_size:I = 0x7f090020

.field public static final detail_item_icon_margin:I = 0x7f09001b

.field public static final detail_item_side_margin:I = 0x7f09001c

.field public static final detail_item_vertical_margin:I = 0x7f09001d

.field public static final detail_min_line_item_height:I = 0x7f09001e

.field public static final detail_network_icon_size:I = 0x7f09001f

.field public static final detail_tab_carousel_tab_label_height:I = 0x7f09000d

.field public static final detail_tab_carousel_tab_label_indent:I = 0x7f09000e

.field public static final detail_update_section_attribution_comments_padding:I = 0x7f090015

.field public static final detail_update_section_between_items_padding:I = 0x7f090014

.field public static final detail_update_section_between_items_vertical_padding:I = 0x7f090012

.field public static final detail_update_section_item_horizontal_padding:I = 0x7f090013

.field public static final detail_update_section_item_vertical_padding:I = 0x7f090011

.field public static final detail_update_section_side_padding:I = 0x7f090010

.field public static final detail_update_tab_side_padding:I = 0x7f09000f

.field public static final detail_vertical_divider_vertical_margin:I = 0x7f090021

.field public static final dialer_search_item_view_initial_height:I = 0x7f090077

.field public static final dialer_search_list_view_max_height:I = 0x7f090076

.field public static final dialer_search_list_view_min_height:I = 0x7f090075

.field public static final dialer_search_outer_margin:I = 0x7f090051

.field public static final dialpad_additioanl_button_height:I = 0x7f090074

.field public static final dialpad_additional_button_height:I = 0x7f0900a0

.field public static final dialpad_additional_button_width:I = 0x7f090073

.field public static final dialpad_button_height:I = 0x7f0900a2

.field public static final dialpad_button_margin:I = 0x7f090038

.field public static final dialpad_button_width:I = 0x7f090078

.field public static final dialpad_center_margin:I = 0x7f090037

.field public static final dialpad_digits_height:I = 0x7f090035

.field public static final dialpad_digits_margin_bottom:I = 0x7f090036

.field public static final dialpad_digits_text_size:I = 0x7f090034

.field public static final dialpad_digits_text_size_delta:I = 0x7f090062

.field public static final dialpad_digits_text_size_min:I = 0x7f090061

.field public static final dialpad_digits_width:I = 0x7f090063

.field public static final dialpad_divider_height:I = 0x7f090079

.field public static final dialpad_divider_width:I = 0x7f09007a

.field public static final dialpad_horizontal_margin:I = 0x7f090032

.field public static final dialpad_operator_horizontal_padding_left:I = 0x7f090066

.field public static final dialpad_operator_horizontal_padding_right:I = 0x7f090065

.field public static final dialpad_operator_name_text_size:I = 0x7f09009f

.field public static final dialpad_operator_vertical_padding:I = 0x7f090064

.field public static final dialpad_vertical_margin:I = 0x7f090033

.field public static final editor_add_field_label_left_padding:I = 0x7f090007

.field public static final editor_field_left_padding:I = 0x7f090009

.field public static final editor_field_right_padding:I = 0x7f09000a

.field public static final editor_min_line_item_height:I = 0x7f09000b

.field public static final editor_padding_top:I = 0x7f090002

.field public static final editor_round_button_padding_bottom:I = 0x7f090006

.field public static final editor_round_button_padding_left:I = 0x7f090003

.field public static final editor_round_button_padding_right:I = 0x7f090004

.field public static final editor_round_button_padding_top:I = 0x7f090005

.field public static final editor_type_label_width:I = 0x7f090008

.field public static final empty_message_top_margin:I = 0x7f090052

.field public static final expanded_photo_height_offset:I = 0x7f090019

.field public static final fake_menu_button_min_width:I = 0x7f090060

.field public static final group_account_header_vertical_account_type_text_size:I = 0x7f09008a

.field public static final group_detail_border_padding:I = 0x7f090030

.field public static final group_editor_autocomplete_left_padding:I = 0x7f09002f

.field public static final group_editor_member_list_left_margin:I = 0x7f09002d

.field public static final group_editor_member_list_right_margin:I = 0x7f09002e

.field public static final group_editor_side_padding:I = 0x7f09002c

.field public static final group_member_item_left_padding:I = 0x7f09002b

.field public static final join_header_bottom_margin:I = 0x7f090043

.field public static final join_header_left_margin:I = 0x7f090040

.field public static final join_header_right_margin:I = 0x7f090041

.field public static final join_header_top_margin:I = 0x7f090042

.field public static final list_header_extra_top_padding:I = 0x7f090044

.field public static final list_section_divider_min_height:I = 0x7f09005d

.field public static final list_section_height:I = 0x7f090029

.field public static final list_visible_scrollbar_padding:I = 0x7f090022

.field public static final no_accounts_message_margin:I = 0x7f090053

.field public static final photo_action_popup_min_width:I = 0x7f09001a

.field public static final quick_contact_photo_container_height:I = 0x7f090031

.field public static final quick_contact_top_position:I = 0x7f090001

.field public static final quickcontact_item_name_width:I = 0x7f090089

.field public static final search_view_width:I = 0x7f09003a

.field public static final shortcut_icon_border_width:I = 0x7f090026

.field public static final shortcut_icon_size:I = 0x7f090025

.field public static final shortcut_overlay_text_background_padding:I = 0x7f090028

.field public static final shortcut_overlay_text_size:I = 0x7f090027

.field public static final stream_item_stripe_width:I = 0x7f09005c

.field public static final widget_text_size_name:I = 0x7f090023

.field public static final widget_text_size_snippet:I = 0x7f090024


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
