.class public final Lcom/android/contacts/R$drawable;
.super Ljava/lang/Object;
.source "R.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/contacts/R;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "drawable"
.end annotation


# static fields
.field public static final ab_bottom_opaque_dark_holo:I = 0x7f020000

.field public static final ab_dropdown_navigation_item_background:I = 0x7f020001

.field public static final ab_dropdown_navigation_item_background_focused:I = 0x7f020002

.field public static final ab_dropdown_navigation_item_background_pressed:I = 0x7f020003

.field public static final ab_solid_custom_blue_inverse_holo:I = 0x7f020004

.field public static final ab_stacked_opaque_dark_holo:I = 0x7f020005

.field public static final ab_stacked_solid_inverse_holo:I = 0x7f020006

.field public static final account_spinner_icon:I = 0x7f020007

.field public static final action_bar_divider:I = 0x7f020008

.field public static final action_bar_item_background:I = 0x7f020009

.field public static final action_bar_item_focused:I = 0x7f02000a

.field public static final action_bar_item_pressed:I = 0x7f02000b

.field public static final action_bar_tab:I = 0x7f02000c

.field public static final action_bar_tab_ext:I = 0x7f02000d

.field public static final aggregation_suggestions_bg:I = 0x7f02000e

.field public static final aggregation_suggestions_bg_light_holo:I = 0x7f02000f

.field public static final background_dial_holo_dark:I = 0x7f020010

.field public static final badge_action_call:I = 0x7f020011

.field public static final badge_action_sms:I = 0x7f020012

.field public static final bg_people_updates_holo:I = 0x7f020013

.field public static final bg_status_contact_widget:I = 0x7f020014

.field public static final btn_call:I = 0x7f020015

.field public static final btn_call_pressed:I = 0x7f020016

.field public static final btn_calllog_all:I = 0x7f020017

.field public static final btn_calllog_all_sel:I = 0x7f020018

.field public static final btn_calllog_incoming:I = 0x7f020019

.field public static final btn_calllog_incoming_sel:I = 0x7f02001a

.field public static final btn_calllog_missed:I = 0x7f02001b

.field public static final btn_calllog_missed_sel:I = 0x7f02001c

.field public static final btn_circle:I = 0x7f02001d

.field public static final btn_circle_disable:I = 0x7f02001e

.field public static final btn_circle_disable_focused:I = 0x7f02001f

.field public static final btn_circle_normal:I = 0x7f020020

.field public static final btn_minus:I = 0x7f020021

.field public static final btn_star_holo_dark:I = 0x7f020022

.field public static final btn_star_off_normal_holo_dark:I = 0x7f020023

.field public static final btn_star_off_normal_holo_light:I = 0x7f020024

.field public static final btn_star_on_normal_holo_dark:I = 0x7f020025

.field public static final btn_star_on_normal_holo_light:I = 0x7f020026

.field public static final call_background:I = 0x7f020027

.field public static final contact_account_phone:I = 0x7f020028

.field public static final contact_phone_storage:I = 0x7f020029

.field public static final contact_sd_card_icon:I = 0x7f02002a

.field public static final contacts_widget_preview:I = 0x7f02002b

.field public static final dial_background_texture:I = 0x7f02002c

.field public static final dial_num_0_wht:I = 0x7f02002d

.field public static final dial_num_1_wht:I = 0x7f02002e

.field public static final dial_num_2_wht:I = 0x7f02002f

.field public static final dial_num_3_wht:I = 0x7f020030

.field public static final dial_num_4_wht:I = 0x7f020031

.field public static final dial_num_5_wht:I = 0x7f020032

.field public static final dial_num_6_wht:I = 0x7f020033

.field public static final dial_num_7_wht:I = 0x7f020034

.field public static final dial_num_8_wht:I = 0x7f020035

.field public static final dial_num_9_wht:I = 0x7f020036

.field public static final dial_num_pound_wht:I = 0x7f020037

.field public static final dial_num_star_wht:I = 0x7f020038

.field public static final dialpad_background:I = 0x7f020039

.field public static final divider_vertical_dark:I = 0x7f02003a

.field public static final dropdown_normal_holo_dark:I = 0x7f02003b

.field public static final frame_thumbnail_contact_widget_holo:I = 0x7f02003c

.field public static final gray_action_bar_background:I = 0x7f02003d

.field public static final group_list_item_background:I = 0x7f02003e

.field public static final group_source_button_background:I = 0x7f02003f

.field public static final ic_ab_dialer_holo_dark:I = 0x7f020040

.field public static final ic_ab_favourites_holo_dark:I = 0x7f020041

.field public static final ic_ab_history_holo_dark:I = 0x7f020042

.field public static final ic_ab_search_holo_dark:I = 0x7f020043

.field public static final ic_add_contact_holo_dark:I = 0x7f020044

.field public static final ic_add_contact_holo_light:I = 0x7f020045

.field public static final ic_add_group_holo_dark:I = 0x7f020046

.field public static final ic_association:I = 0x7f020047

.field public static final ic_btn_incoming:I = 0x7f020048

.field public static final ic_btn_minus_down:I = 0x7f020049

.field public static final ic_btn_minus_focus:I = 0x7f02004a

.field public static final ic_btn_minus_up:I = 0x7f02004b

.field public static final ic_btn_missed:I = 0x7f02004c

.field public static final ic_btn_outgoing:I = 0x7f02004d

.field public static final ic_btn_resource:I = 0x7f02004e

.field public static final ic_btn_round_more_normal:I = 0x7f02004f

.field public static final ic_call_autorejected_holo_dark:I = 0x7f020050

.field public static final ic_call_incoming_holo_dark:I = 0x7f020051

.field public static final ic_call_log_list_details_background:I = 0x7f020052

.field public static final ic_call_missed_holo_dark:I = 0x7f020053

.field public static final ic_call_outgoing_holo_dark:I = 0x7f020054

.field public static final ic_call_voicemail_holo_dark:I = 0x7f020055

.field public static final ic_call_voicemail_holo_dark_red:I = 0x7f020056

.field public static final ic_contact_account_phone:I = 0x7f020057

.field public static final ic_contact_account_phone_minitab:I = 0x7f020058

.field public static final ic_contact_account_sim:I = 0x7f020059

.field public static final ic_contact_account_sim_blue:I = 0x7f02005a

.field public static final ic_contact_account_sim_green:I = 0x7f02005b

.field public static final ic_contact_account_sim_lock:I = 0x7f02005c

.field public static final ic_contact_account_sim_minitab:I = 0x7f02005d

.field public static final ic_contact_account_sim_orange:I = 0x7f02005e

.field public static final ic_contact_account_sim_purple:I = 0x7f02005f

.field public static final ic_contact_account_usim:I = 0x7f020060

.field public static final ic_contact_account_usim_minitab:I = 0x7f020061

.field public static final ic_contact_picture_180_holo_dark:I = 0x7f020062

.field public static final ic_contact_picture_180_holo_light:I = 0x7f020063

.field public static final ic_contact_picture_holo_dark:I = 0x7f020064

.field public static final ic_contact_picture_holo_light:I = 0x7f020065

.field public static final ic_contact_picture_sdn_contact:I = 0x7f020066

.field public static final ic_contact_picture_sdn_contact_blue:I = 0x7f020067

.field public static final ic_contact_picture_sdn_contact_green:I = 0x7f020068

.field public static final ic_contact_picture_sdn_contact_orange:I = 0x7f020069

.field public static final ic_contact_picture_sdn_contact_purple:I = 0x7f02006a

.field public static final ic_contact_picture_sdn_holo_dark:I = 0x7f02006b

.field public static final ic_contact_picture_sdn_holo_dark_blue:I = 0x7f02006c

.field public static final ic_contact_picture_sdn_holo_dark_green:I = 0x7f02006d

.field public static final ic_contact_picture_sdn_holo_dark_orange:I = 0x7f02006e

.field public static final ic_contact_picture_sdn_holo_dark_purple:I = 0x7f02006f

.field public static final ic_contact_picture_sim_contact:I = 0x7f020070

.field public static final ic_contact_picture_sim_contact_blue:I = 0x7f020071

.field public static final ic_contact_picture_sim_contact_green:I = 0x7f020072

.field public static final ic_contact_picture_sim_contact_orange:I = 0x7f020073

.field public static final ic_contact_picture_sim_contact_purple:I = 0x7f020074

.field public static final ic_contact_picture_sim_holo_dark:I = 0x7f020075

.field public static final ic_contact_picture_sim_holo_dark_blue:I = 0x7f020076

.field public static final ic_contact_picture_sim_holo_dark_green:I = 0x7f020077

.field public static final ic_contact_picture_sim_holo_dark_orange:I = 0x7f020078

.field public static final ic_contact_picture_sim_holo_dark_purple:I = 0x7f020079

.field public static final ic_contact_picture_usim_contact:I = 0x7f02007a

.field public static final ic_contacts_holo_dark:I = 0x7f02007b

.field public static final ic_dial_action_call:I = 0x7f02007c

.field public static final ic_dial_action_delete:I = 0x7f02007d

.field public static final ic_dial_action_search:I = 0x7f02007e

.field public static final ic_dial_action_video_call:I = 0x7f02007f

.field public static final ic_dialer_fork_add_call:I = 0x7f020080

.field public static final ic_dialer_fork_current_call:I = 0x7f020081

.field public static final ic_dialer_fork_tt_keypad:I = 0x7f020082

.field public static final ic_dialpad_holo_dark:I = 0x7f020083

.field public static final ic_divider_dashed_holo_dark:I = 0x7f020084

.field public static final ic_groups_holo_dark:I = 0x7f020085

.field public static final ic_hold_pause:I = 0x7f020086

.field public static final ic_hold_pause_disabled_holo_dark:I = 0x7f020087

.field public static final ic_hold_pause_holo_dark:I = 0x7f020088

.field public static final ic_launcher_contacts:I = 0x7f020089

.field public static final ic_launcher_phone:I = 0x7f02008a

.field public static final ic_launcher_smsmms:I = 0x7f02008b

.field public static final ic_list_default_mime_holo_dark:I = 0x7f02008c

.field public static final ic_menu_add_field_holo_light:I = 0x7f02008d

.field public static final ic_menu_cancel_holo_dark:I = 0x7f02008e

.field public static final ic_menu_compose_holo_dark:I = 0x7f02008f

.field public static final ic_menu_contact_clear_select:I = 0x7f020090

.field public static final ic_menu_contact_select_all:I = 0x7f020091

.field public static final ic_menu_contact_trash:I = 0x7f020092

.field public static final ic_menu_copy_holo_dark:I = 0x7f020093

.field public static final ic_menu_done_holo_dark:I = 0x7f020094

.field public static final ic_menu_expander_maximized_holo_light:I = 0x7f020095

.field public static final ic_menu_expander_minimized_holo_light:I = 0x7f020096

.field public static final ic_menu_look_simstorage_holo_light:I = 0x7f020097

.field public static final ic_menu_mark:I = 0x7f020098

.field public static final ic_menu_overflow:I = 0x7f020099

.field public static final ic_menu_print:I = 0x7f02009a

.field public static final ic_menu_remove_field_holo_light:I = 0x7f02009b

.field public static final ic_menu_settings_holo_light:I = 0x7f02009c

.field public static final ic_menu_share_holo_light:I = 0x7f02009d

.field public static final ic_menu_star_holo_light:I = 0x7f02009e

.field public static final ic_menu_trash_holo_light:I = 0x7f02009f

.field public static final ic_minus:I = 0x7f0200a0

.field public static final ic_minus_disabled_holo_dark:I = 0x7f0200a1

.field public static final ic_minus_holo_dark:I = 0x7f0200a2

.field public static final ic_play:I = 0x7f0200a3

.field public static final ic_play_active_holo_dark:I = 0x7f0200a4

.field public static final ic_play_disabled_holo_dark:I = 0x7f0200a5

.field public static final ic_play_holo_dark:I = 0x7f0200a6

.field public static final ic_plus:I = 0x7f0200a7

.field public static final ic_plus_disabled_holo_dark:I = 0x7f0200a8

.field public static final ic_plus_holo_dark:I = 0x7f0200a9

.field public static final ic_show_dialpad_holo_dark:I = 0x7f0200aa

.field public static final ic_sound_off_speakerphone_disabled_holo_dark:I = 0x7f0200ab

.field public static final ic_sound_off_speakerphone_holo_dark:I = 0x7f0200ac

.field public static final ic_sound_speakerphone_disabled_holo_dark:I = 0x7f0200ad

.field public static final ic_sound_speakerphone_holo_dark:I = 0x7f0200ae

.field public static final ic_speakerphone_off:I = 0x7f0200af

.field public static final ic_speakerphone_on:I = 0x7f0200b0

.field public static final ic_tab_all:I = 0x7f0200b1

.field public static final ic_tab_calllog:I = 0x7f0200b2

.field public static final ic_tab_dialer:I = 0x7f0200b3

.field public static final ic_tab_groups:I = 0x7f0200b4

.field public static final ic_tab_recent:I = 0x7f0200b5

.field public static final ic_tab_selected_focused_holo:I = 0x7f0200b6

.field public static final ic_tab_selected_holo:I = 0x7f0200b7

.field public static final ic_tab_selected_pressed_holo:I = 0x7f0200b8

.field public static final ic_tab_starred:I = 0x7f0200b9

.field public static final ic_tab_unselected_focused_holo:I = 0x7f0200ba

.field public static final ic_tab_unselected_holo:I = 0x7f0200bb

.field public static final ic_tab_unselected_pressed_holo:I = 0x7f0200bc

.field public static final ic_text_holo_dark:I = 0x7f0200bd

.field public static final ic_text_holo_light:I = 0x7f0200be

.field public static final ic_trash_holo_dark:I = 0x7f0200bf

.field public static final ic_video_call:I = 0x7f0200c0

.field public static final ic_video_call_autorejected_holo_dark:I = 0x7f0200c1

.field public static final ic_video_call_incoming_holo_dark:I = 0x7f0200c2

.field public static final ic_video_call_light:I = 0x7f0200c3

.field public static final ic_video_call_missed_holo_dark:I = 0x7f0200c4

.field public static final ic_video_call_outgoing_holo_dark:I = 0x7f0200c5

.field public static final item_select:I = 0x7f0200c6

.field public static final list_activated_holo:I = 0x7f0200c7

.field public static final list_background_holo:I = 0x7f0200c8

.field public static final list_divider:I = 0x7f0200c9

.field public static final list_focused_holo:I = 0x7f0200ca

.field public static final list_item_activated_background:I = 0x7f0200cb

.field public static final list_pressed_holo:I = 0x7f0200cc

.field public static final list_pressed_holo_light:I = 0x7f0200cd

.field public static final list_section_divider_holo_custom:I = 0x7f0200ce

.field public static final list_selector_disabled_holo_dark:I = 0x7f0200cf

.field public static final list_title_holo:I = 0x7f0200d0

.field public static final panel_content:I = 0x7f0200d1

.field public static final panel_favorites_holo_light:I = 0x7f0200d2

.field public static final panel_message:I = 0x7f0200d3

.field public static final quickcon_background_texture:I = 0x7f0200d4

.field public static final quickcontact_badge_overlay_normal_light:I = 0x7f0200d5

.field public static final quickcontact_badge_overlay_pressed_light:I = 0x7f0200d6

.field public static final quickcontact_list_item_background:I = 0x7f0200d7

.field public static final quickcontact_list_item_divider:I = 0x7f0200d8

.field public static final quickcontact_track_background:I = 0x7f0200d9

.field public static final seek_bar_thumb:I = 0x7f0200da

.field public static final seekbar_drawable:I = 0x7f0200db

.field public static final sim_dark_internet_call:I = 0x7f0200dc

.field public static final sim_dark_not_activated:I = 0x7f0200dd

.field public static final sim_suggested:I = 0x7f0200de

.field public static final spinner_default_holo_dark:I = 0x7f0200df

.field public static final stat_sys_upload_done:I = 0x7f0200e0

.field public static final sym_action_audiochat_holo_light:I = 0x7f0200e1

.field public static final sym_action_videochat_holo_light:I = 0x7f0200e2

.field public static final tab_selected:I = 0x7f0200e3

.field public static final tab_selected_focused:I = 0x7f0200e4

.field public static final tab_selected_focused_holo:I = 0x7f0200e5

.field public static final tab_selected_holo:I = 0x7f0200e6

.field public static final tab_selected_pressed:I = 0x7f0200e7

.field public static final tab_selected_pressed_holo:I = 0x7f0200e8

.field public static final tab_unselected_focused:I = 0x7f0200e9

.field public static final tab_unselected_focused_holo:I = 0x7f0200ea

.field public static final tab_unselected_holo:I = 0x7f0200eb

.field public static final tab_unselected_pressed:I = 0x7f0200ec

.field public static final tab_unselected_pressed_holo:I = 0x7f0200ed

.field public static final unknown_source:I = 0x7f0200ee


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
