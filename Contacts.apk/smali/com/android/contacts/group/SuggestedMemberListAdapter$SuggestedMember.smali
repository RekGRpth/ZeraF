.class public Lcom/android/contacts/group/SuggestedMemberListAdapter$SuggestedMember;
.super Ljava/lang/Object;
.source "SuggestedMemberListAdapter.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/contacts/group/SuggestedMemberListAdapter;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1
    name = "SuggestedMember"
.end annotation


# instance fields
.field private mContactId:J

.field private mDisplayName:Ljava/lang/String;

.field private mExtraInfo:Ljava/lang/String;

.field private mFixExtraInfo:Z

.field private mIsSdnContact:I

.field private mPhoto:[B

.field private mRawContactId:J

.field private mSimId:I

.field private mSimType:I

.field private mSlotId:I

.field final synthetic this$0:Lcom/android/contacts/group/SuggestedMemberListAdapter;


# direct methods
.method public constructor <init>(Lcom/android/contacts/group/SuggestedMemberListAdapter;JLjava/lang/String;J)V
    .locals 2
    .param p2    # J
    .param p4    # Ljava/lang/String;
    .param p5    # J

    const/4 v1, 0x0

    const/4 v0, -0x1

    iput-object p1, p0, Lcom/android/contacts/group/SuggestedMemberListAdapter$SuggestedMember;->this$0:Lcom/android/contacts/group/SuggestedMemberListAdapter;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput v0, p0, Lcom/android/contacts/group/SuggestedMemberListAdapter$SuggestedMember;->mSimId:I

    iput v0, p0, Lcom/android/contacts/group/SuggestedMemberListAdapter$SuggestedMember;->mSimType:I

    iput v0, p0, Lcom/android/contacts/group/SuggestedMemberListAdapter$SuggestedMember;->mSlotId:I

    iput v1, p0, Lcom/android/contacts/group/SuggestedMemberListAdapter$SuggestedMember;->mIsSdnContact:I

    iput-boolean v1, p0, Lcom/android/contacts/group/SuggestedMemberListAdapter$SuggestedMember;->mFixExtraInfo:Z

    iput-wide p2, p0, Lcom/android/contacts/group/SuggestedMemberListAdapter$SuggestedMember;->mRawContactId:J

    iput-object p4, p0, Lcom/android/contacts/group/SuggestedMemberListAdapter$SuggestedMember;->mDisplayName:Ljava/lang/String;

    iput-wide p5, p0, Lcom/android/contacts/group/SuggestedMemberListAdapter$SuggestedMember;->mContactId:J

    return-void
.end method

.method public constructor <init>(Lcom/android/contacts/group/SuggestedMemberListAdapter;Lcom/android/contacts/group/SuggestedMemberListAdapter$SuggestedMember;)V
    .locals 2
    .param p2    # Lcom/android/contacts/group/SuggestedMemberListAdapter$SuggestedMember;

    const/4 v1, 0x0

    const/4 v0, -0x1

    iput-object p1, p0, Lcom/android/contacts/group/SuggestedMemberListAdapter$SuggestedMember;->this$0:Lcom/android/contacts/group/SuggestedMemberListAdapter;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput v0, p0, Lcom/android/contacts/group/SuggestedMemberListAdapter$SuggestedMember;->mSimId:I

    iput v0, p0, Lcom/android/contacts/group/SuggestedMemberListAdapter$SuggestedMember;->mSimType:I

    iput v0, p0, Lcom/android/contacts/group/SuggestedMemberListAdapter$SuggestedMember;->mSlotId:I

    iput v1, p0, Lcom/android/contacts/group/SuggestedMemberListAdapter$SuggestedMember;->mIsSdnContact:I

    iput-boolean v1, p0, Lcom/android/contacts/group/SuggestedMemberListAdapter$SuggestedMember;->mFixExtraInfo:Z

    invoke-virtual {p2}, Lcom/android/contacts/group/SuggestedMemberListAdapter$SuggestedMember;->getRawContactId()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/android/contacts/group/SuggestedMemberListAdapter$SuggestedMember;->mRawContactId:J

    invoke-virtual {p2}, Lcom/android/contacts/group/SuggestedMemberListAdapter$SuggestedMember;->getDisplayName()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/android/contacts/group/SuggestedMemberListAdapter$SuggestedMember;->mDisplayName:Ljava/lang/String;

    invoke-virtual {p2}, Lcom/android/contacts/group/SuggestedMemberListAdapter$SuggestedMember;->getContactId()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/android/contacts/group/SuggestedMemberListAdapter$SuggestedMember;->mContactId:J

    invoke-virtual {p2}, Lcom/android/contacts/group/SuggestedMemberListAdapter$SuggestedMember;->getPhotoByteArray()[B

    move-result-object v0

    iput-object v0, p0, Lcom/android/contacts/group/SuggestedMemberListAdapter$SuggestedMember;->mPhoto:[B

    invoke-virtual {p2}, Lcom/android/contacts/group/SuggestedMemberListAdapter$SuggestedMember;->getSimId()I

    move-result v0

    iput v0, p0, Lcom/android/contacts/group/SuggestedMemberListAdapter$SuggestedMember;->mSimId:I

    invoke-virtual {p2}, Lcom/android/contacts/group/SuggestedMemberListAdapter$SuggestedMember;->getSimType()I

    move-result v0

    iput v0, p0, Lcom/android/contacts/group/SuggestedMemberListAdapter$SuggestedMember;->mSimType:I

    invoke-virtual {p2}, Lcom/android/contacts/group/SuggestedMemberListAdapter$SuggestedMember;->getSlotId()I

    move-result v0

    iput v0, p0, Lcom/android/contacts/group/SuggestedMemberListAdapter$SuggestedMember;->mSlotId:I

    invoke-virtual {p2}, Lcom/android/contacts/group/SuggestedMemberListAdapter$SuggestedMember;->getIsSdnContact()I

    move-result v0

    iput v0, p0, Lcom/android/contacts/group/SuggestedMemberListAdapter$SuggestedMember;->mIsSdnContact:I

    return-void
.end method


# virtual methods
.method public getContactId()J
    .locals 2

    iget-wide v0, p0, Lcom/android/contacts/group/SuggestedMemberListAdapter$SuggestedMember;->mContactId:J

    return-wide v0
.end method

.method public getDisplayName()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/android/contacts/group/SuggestedMemberListAdapter$SuggestedMember;->mDisplayName:Ljava/lang/String;

    return-object v0
.end method

.method public getExtraInfo()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/android/contacts/group/SuggestedMemberListAdapter$SuggestedMember;->mExtraInfo:Ljava/lang/String;

    return-object v0
.end method

.method public getIsSdnContact()I
    .locals 1

    iget v0, p0, Lcom/android/contacts/group/SuggestedMemberListAdapter$SuggestedMember;->mIsSdnContact:I

    return v0
.end method

.method public getPhotoByteArray()[B
    .locals 1

    iget-object v0, p0, Lcom/android/contacts/group/SuggestedMemberListAdapter$SuggestedMember;->mPhoto:[B

    return-object v0
.end method

.method public getRawContactId()J
    .locals 2

    iget-wide v0, p0, Lcom/android/contacts/group/SuggestedMemberListAdapter$SuggestedMember;->mRawContactId:J

    return-wide v0
.end method

.method public getSimId()I
    .locals 1

    iget v0, p0, Lcom/android/contacts/group/SuggestedMemberListAdapter$SuggestedMember;->mSimId:I

    return v0
.end method

.method public getSimType()I
    .locals 1

    iget v0, p0, Lcom/android/contacts/group/SuggestedMemberListAdapter$SuggestedMember;->mSimType:I

    return v0
.end method

.method public getSlotId()I
    .locals 1

    iget v0, p0, Lcom/android/contacts/group/SuggestedMemberListAdapter$SuggestedMember;->mSlotId:I

    return v0
.end method

.method public hasExtraInfo()Z
    .locals 1

    iget-object v0, p0, Lcom/android/contacts/group/SuggestedMemberListAdapter$SuggestedMember;->mExtraInfo:Ljava/lang/String;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasFixedExtrasInfo()Z
    .locals 1

    iget-boolean v0, p0, Lcom/android/contacts/group/SuggestedMemberListAdapter$SuggestedMember;->mFixExtraInfo:Z

    return v0
.end method

.method public setExtraInfo(Ljava/lang/String;)V
    .locals 0
    .param p1    # Ljava/lang/String;

    iput-object p1, p0, Lcom/android/contacts/group/SuggestedMemberListAdapter$SuggestedMember;->mExtraInfo:Ljava/lang/String;

    return-void
.end method

.method public setFixExtrasInfo(Z)V
    .locals 0
    .param p1    # Z

    iput-boolean p1, p0, Lcom/android/contacts/group/SuggestedMemberListAdapter$SuggestedMember;->mFixExtraInfo:Z

    return-void
.end method

.method public setIsSdnContact(I)V
    .locals 0
    .param p1    # I

    iput p1, p0, Lcom/android/contacts/group/SuggestedMemberListAdapter$SuggestedMember;->mIsSdnContact:I

    return-void
.end method

.method public setPhotoByteArray([B)V
    .locals 0
    .param p1    # [B

    iput-object p1, p0, Lcom/android/contacts/group/SuggestedMemberListAdapter$SuggestedMember;->mPhoto:[B

    return-void
.end method

.method public setRawContactId(J)V
    .locals 0
    .param p1    # J

    iput-wide p1, p0, Lcom/android/contacts/group/SuggestedMemberListAdapter$SuggestedMember;->mRawContactId:J

    return-void
.end method

.method public setSimId(I)V
    .locals 0
    .param p1    # I

    iput p1, p0, Lcom/android/contacts/group/SuggestedMemberListAdapter$SuggestedMember;->mSimId:I

    return-void
.end method

.method public setSimType(I)V
    .locals 0
    .param p1    # I

    iput p1, p0, Lcom/android/contacts/group/SuggestedMemberListAdapter$SuggestedMember;->mSimType:I

    return-void
.end method

.method public setSlotId(I)V
    .locals 0
    .param p1    # I

    iput p1, p0, Lcom/android/contacts/group/SuggestedMemberListAdapter$SuggestedMember;->mSlotId:I

    return-void
.end method

.method public toString()Ljava/lang/String;
    .locals 1

    invoke-virtual {p0}, Lcom/android/contacts/group/SuggestedMemberListAdapter$SuggestedMember;->getDisplayName()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
