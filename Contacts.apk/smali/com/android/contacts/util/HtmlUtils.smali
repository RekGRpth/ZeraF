.class public Lcom/android/contacts/util/HtmlUtils;
.super Ljava/lang/Object;
.source "HtmlUtils.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/contacts/util/HtmlUtils$StreamItemQuoteSpan;
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static fromHtml(Landroid/content/Context;Ljava/lang/String;)Landroid/text/Spanned;
    .locals 2
    .param p0    # Landroid/content/Context;
    .param p1    # Ljava/lang/String;

    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v1, 0x0

    :goto_0
    return-object v1

    :cond_0
    invoke-static {p1}, Landroid/text/Html;->fromHtml(Ljava/lang/String;)Landroid/text/Spanned;

    move-result-object v0

    invoke-static {p0, v0}, Lcom/android/contacts/util/HtmlUtils;->postprocess(Landroid/content/Context;Landroid/text/Spanned;)Landroid/text/Spanned;

    move-result-object v1

    goto :goto_0
.end method

.method public static fromHtml(Landroid/content/Context;Ljava/lang/String;Landroid/text/Html$ImageGetter;Landroid/text/Html$TagHandler;)Ljava/lang/CharSequence;
    .locals 1
    .param p0    # Landroid/content/Context;
    .param p1    # Ljava/lang/String;
    .param p2    # Landroid/text/Html$ImageGetter;
    .param p3    # Landroid/text/Html$TagHandler;

    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_0
    invoke-static {p1, p2, p3}, Landroid/text/Html;->fromHtml(Ljava/lang/String;Landroid/text/Html$ImageGetter;Landroid/text/Html$TagHandler;)Landroid/text/Spanned;

    move-result-object v0

    invoke-static {p0, v0}, Lcom/android/contacts/util/HtmlUtils;->postprocess(Landroid/content/Context;Landroid/text/Spanned;)Landroid/text/Spanned;

    move-result-object v0

    goto :goto_0
.end method

.method static postprocess(Landroid/content/Context;Landroid/text/Spanned;)Landroid/text/Spanned;
    .locals 14
    .param p0    # Landroid/content/Context;
    .param p1    # Landroid/text/Spanned;
    .annotation build Lcom/google/common/annotations/VisibleForTesting;
    .end annotation

    const/4 v13, 0x0

    if-nez p1, :cond_1

    const/4 p1, 0x0

    :cond_0
    :goto_0
    return-object p1

    :cond_1
    invoke-interface {p1}, Landroid/text/Spanned;->length()I

    move-result v5

    if-eqz v5, :cond_0

    instance-of v10, p1, Landroid/text/SpannableStringBuilder;

    if-eqz v10, :cond_2

    move-object v0, p1

    check-cast v0, Landroid/text/SpannableStringBuilder;

    :goto_1
    const-class v10, Landroid/text/style/QuoteSpan;

    invoke-virtual {v0, v13, v5, v10}, Landroid/text/SpannableStringBuilder;->getSpans(IILjava/lang/Class;)[Ljava/lang/Object;

    move-result-object v6

    check-cast v6, [Landroid/text/style/QuoteSpan;

    if-eqz v6, :cond_3

    array-length v10, v6

    if-eqz v10, :cond_3

    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v7

    const v10, 0x7f080016

    invoke-virtual {v7, v10}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    const v10, 0x7f09005c

    invoke-virtual {v7, v10}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v9

    const/4 v3, 0x0

    :goto_2
    array-length v10, v6

    if-ge v3, v10, :cond_3

    aget-object v10, v6, v3

    new-instance v11, Lcom/android/contacts/util/HtmlUtils$StreamItemQuoteSpan;

    invoke-direct {v11, v1, v9}, Lcom/android/contacts/util/HtmlUtils$StreamItemQuoteSpan;-><init>(II)V

    invoke-static {v0, v10, v11}, Lcom/android/contacts/util/HtmlUtils;->replaceSpan(Landroid/text/SpannableStringBuilder;Ljava/lang/Object;Ljava/lang/Object;)V

    add-int/lit8 v3, v3, 0x1

    goto :goto_2

    :cond_2
    new-instance v0, Landroid/text/SpannableStringBuilder;

    invoke-direct {v0, p1}, Landroid/text/SpannableStringBuilder;-><init>(Ljava/lang/CharSequence;)V

    goto :goto_1

    :cond_3
    const-class v10, Landroid/text/style/ImageSpan;

    invoke-virtual {v0, v13, v5, v10}, Landroid/text/SpannableStringBuilder;->getSpans(IILjava/lang/Class;)[Ljava/lang/Object;

    move-result-object v4

    check-cast v4, [Landroid/text/style/ImageSpan;

    if-eqz v4, :cond_4

    const/4 v3, 0x0

    :goto_3
    array-length v10, v4

    if-ge v3, v10, :cond_4

    aget-object v8, v4, v3

    new-instance v10, Landroid/text/style/ImageSpan;

    invoke-virtual {v8}, Landroid/text/style/ImageSpan;->getDrawable()Landroid/graphics/drawable/Drawable;

    move-result-object v11

    const/4 v12, 0x1

    invoke-direct {v10, v11, v12}, Landroid/text/style/ImageSpan;-><init>(Landroid/graphics/drawable/Drawable;I)V

    invoke-static {v0, v8, v10}, Lcom/android/contacts/util/HtmlUtils;->replaceSpan(Landroid/text/SpannableStringBuilder;Ljava/lang/Object;Ljava/lang/Object;)V

    add-int/lit8 v3, v3, 0x1

    goto :goto_3

    :cond_4
    move v2, v5

    invoke-virtual {v0}, Landroid/text/SpannableStringBuilder;->length()I

    move-result v10

    add-int/lit8 v3, v10, -0x1

    :goto_4
    if-ltz v3, :cond_5

    invoke-virtual {v0, v3}, Landroid/text/SpannableStringBuilder;->charAt(I)C

    move-result v10

    const/16 v11, 0xa

    if-eq v10, v11, :cond_6

    :cond_5
    if-ne v2, v5, :cond_7

    move-object p1, v0

    goto :goto_0

    :cond_6
    move v2, v3

    add-int/lit8 v3, v3, -0x1

    goto :goto_4

    :cond_7
    new-instance p1, Landroid/text/SpannableStringBuilder;

    invoke-direct {p1, v0, v13, v2}, Landroid/text/SpannableStringBuilder;-><init>(Ljava/lang/CharSequence;II)V

    goto/16 :goto_0
.end method

.method private static replaceSpan(Landroid/text/SpannableStringBuilder;Ljava/lang/Object;Ljava/lang/Object;)V
    .locals 3
    .param p0    # Landroid/text/SpannableStringBuilder;
    .param p1    # Ljava/lang/Object;
    .param p2    # Ljava/lang/Object;

    invoke-virtual {p0, p1}, Landroid/text/SpannableStringBuilder;->getSpanStart(Ljava/lang/Object;)I

    move-result v0

    invoke-virtual {p0, p1}, Landroid/text/SpannableStringBuilder;->getSpanEnd(Ljava/lang/Object;)I

    move-result v1

    invoke-virtual {p0, p1}, Landroid/text/SpannableStringBuilder;->getSpanFlags(Ljava/lang/Object;)I

    move-result v2

    invoke-virtual {p0, p2, v0, v1, v2}, Landroid/text/SpannableStringBuilder;->setSpan(Ljava/lang/Object;III)V

    invoke-virtual {p0, p1}, Landroid/text/SpannableStringBuilder;->removeSpan(Ljava/lang/Object;)V

    return-void
.end method
