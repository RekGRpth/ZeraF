.class public Lcom/android/contacts/util/StopWatch;
.super Ljava/lang/Object;
.source "StopWatch.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/contacts/util/StopWatch$1;,
        Lcom/android/contacts/util/StopWatch$NullStopWatch;
    }
.end annotation


# instance fields
.field private final mLabel:Ljava/lang/String;

.field private final mLapLabels:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private final mTimes:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method private constructor <init>(Ljava/lang/String;)V
    .locals 1
    .param p1    # Ljava/lang/String;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    invoke-static {}, Lcom/google/common/collect/Lists;->newArrayList()Ljava/util/ArrayList;

    move-result-object v0

    iput-object v0, p0, Lcom/android/contacts/util/StopWatch;->mTimes:Ljava/util/ArrayList;

    invoke-static {}, Lcom/google/common/collect/Lists;->newArrayList()Ljava/util/ArrayList;

    move-result-object v0

    iput-object v0, p0, Lcom/android/contacts/util/StopWatch;->mLapLabels:Ljava/util/ArrayList;

    iput-object p1, p0, Lcom/android/contacts/util/StopWatch;->mLabel:Ljava/lang/String;

    const-string v0, ""

    invoke-virtual {p0, v0}, Lcom/android/contacts/util/StopWatch;->lap(Ljava/lang/String;)V

    return-void
.end method

.method synthetic constructor <init>(Ljava/lang/String;Lcom/android/contacts/util/StopWatch$1;)V
    .locals 0
    .param p1    # Ljava/lang/String;
    .param p2    # Lcom/android/contacts/util/StopWatch$1;

    invoke-direct {p0, p1}, Lcom/android/contacts/util/StopWatch;-><init>(Ljava/lang/String;)V

    return-void
.end method

.method public static getNullStopWatch()Lcom/android/contacts/util/StopWatch;
    .locals 1

    sget-object v0, Lcom/android/contacts/util/StopWatch$NullStopWatch;->INSTANCE:Lcom/android/contacts/util/StopWatch$NullStopWatch;

    return-object v0
.end method

.method public static start(Ljava/lang/String;)Lcom/android/contacts/util/StopWatch;
    .locals 1
    .param p0    # Ljava/lang/String;

    new-instance v0, Lcom/android/contacts/util/StopWatch;

    invoke-direct {v0, p0}, Lcom/android/contacts/util/StopWatch;-><init>(Ljava/lang/String;)V

    return-object v0
.end method


# virtual methods
.method public lap(Ljava/lang/String;)V
    .locals 3
    .param p1    # Ljava/lang/String;

    iget-object v0, p0, Lcom/android/contacts/util/StopWatch;->mTimes:Ljava/util/ArrayList;

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v1

    invoke-static {v1, v2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    iget-object v0, p0, Lcom/android/contacts/util/StopWatch;->mLapLabels:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    return-void
.end method

.method public stopAndLog(Ljava/lang/String;I)V
    .locals 15
    .param p1    # Ljava/lang/String;
    .param p2    # I

    const-string v13, ""

    invoke-virtual {p0, v13}, Lcom/android/contacts/util/StopWatch;->lap(Ljava/lang/String;)V

    iget-object v13, p0, Lcom/android/contacts/util/StopWatch;->mTimes:Ljava/util/ArrayList;

    const/4 v14, 0x0

    invoke-virtual {v13, v14}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v13

    check-cast v13, Ljava/lang/Long;

    invoke-virtual {v13}, Ljava/lang/Long;->longValue()J

    move-result-wide v7

    iget-object v13, p0, Lcom/android/contacts/util/StopWatch;->mTimes:Ljava/util/ArrayList;

    iget-object v14, p0, Lcom/android/contacts/util/StopWatch;->mTimes:Ljava/util/ArrayList;

    invoke-virtual {v14}, Ljava/util/ArrayList;->size()I

    move-result v14

    add-int/lit8 v14, v14, -0x1

    invoke-virtual {v13, v14}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v13

    check-cast v13, Ljava/lang/Long;

    invoke-virtual {v13}, Ljava/lang/Long;->longValue()J

    move-result-wide v9

    sub-long v11, v9, v7

    move/from16 v0, p2

    int-to-long v13, v0

    cmp-long v13, v11, v13

    if-gez v13, :cond_0

    :goto_0
    return-void

    :cond_0
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v13, p0, Lcom/android/contacts/util/StopWatch;->mLabel:Ljava/lang/String;

    invoke-virtual {v6, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v13, ","

    invoke-virtual {v6, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v6, v11, v12}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    const-string v13, ": "

    invoke-virtual {v6, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-wide v4, v7

    const/4 v3, 0x1

    :goto_1
    iget-object v13, p0, Lcom/android/contacts/util/StopWatch;->mTimes:Ljava/util/ArrayList;

    invoke-virtual {v13}, Ljava/util/ArrayList;->size()I

    move-result v13

    if-ge v3, v13, :cond_1

    iget-object v13, p0, Lcom/android/contacts/util/StopWatch;->mTimes:Ljava/util/ArrayList;

    invoke-virtual {v13, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v13

    check-cast v13, Ljava/lang/Long;

    invoke-virtual {v13}, Ljava/lang/Long;->longValue()J

    move-result-wide v1

    iget-object v13, p0, Lcom/android/contacts/util/StopWatch;->mLapLabels:Ljava/util/ArrayList;

    invoke-virtual {v13, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v13

    check-cast v13, Ljava/lang/String;

    invoke-virtual {v6, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v13, ","

    invoke-virtual {v6, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    sub-long v13, v1, v4

    invoke-virtual {v6, v13, v14}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    const-string v13, " "

    invoke-virtual {v6, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-wide v4, v1

    add-int/lit8 v3, v3, 0x1

    goto :goto_1

    :cond_1
    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v13

    move-object/from16 v0, p1

    invoke-static {v0, v13}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method
