.class public Lcom/android/contacts/util/PhoneNumberFormatter$PhoneNumberFormattingTextWatcherEx;
.super Landroid/telephony/PhoneNumberFormattingTextWatcher;
.source "PhoneNumberFormatter.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/contacts/util/PhoneNumberFormatter;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "PhoneNumberFormattingTextWatcherEx"
.end annotation


# static fields
.field protected static mSelfChanged:Z


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const/4 v0, 0x0

    sput-boolean v0, Lcom/android/contacts/util/PhoneNumberFormatter$PhoneNumberFormattingTextWatcherEx;->mSelfChanged:Z

    return-void
.end method

.method protected constructor <init>()V
    .locals 0

    invoke-direct {p0}, Landroid/telephony/PhoneNumberFormattingTextWatcher;-><init>()V

    return-void
.end method

.method constructor <init>(Ljava/lang/String;)V
    .locals 0
    .param p1    # Ljava/lang/String;

    invoke-direct {p0, p1}, Landroid/telephony/PhoneNumberFormattingTextWatcher;-><init>(Ljava/lang/String;)V

    return-void
.end method


# virtual methods
.method public afterTextChanged(Landroid/text/Editable;)V
    .locals 1
    .param p1    # Landroid/text/Editable;

    const/4 v0, 0x1

    sput-boolean v0, Lcom/android/contacts/util/PhoneNumberFormatter$PhoneNumberFormattingTextWatcherEx;->mSelfChanged:Z

    invoke-super {p0, p1}, Landroid/telephony/PhoneNumberFormattingTextWatcher;->afterTextChanged(Landroid/text/Editable;)V

    const/4 v0, 0x0

    sput-boolean v0, Lcom/android/contacts/util/PhoneNumberFormatter$PhoneNumberFormattingTextWatcherEx;->mSelfChanged:Z

    return-void
.end method
