.class public Lcom/android/contacts/model/Contact;
.super Ljava/lang/Object;
.source "Contact.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/contacts/model/Contact$Status;
    }
.end annotation


# static fields
.field private static final TAG:Ljava/lang/String;


# instance fields
.field private final mAltDisplayName:Ljava/lang/String;

.field private mBlockVideoCall:Z

.field private final mCustomRingtone:Ljava/lang/String;

.field private mDirectoryAccountName:Ljava/lang/String;

.field private mDirectoryAccountType:Ljava/lang/String;

.field private mDirectoryDisplayName:Ljava/lang/String;

.field private mDirectoryExportSupport:I

.field private final mDirectoryId:J

.field private mDirectoryType:Ljava/lang/String;

.field private final mDisplayName:Ljava/lang/String;

.field private final mDisplayNameSource:I

.field private final mException:Ljava/lang/Exception;

.field private mGroups:Lcom/google/common/collect/ImmutableList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/common/collect/ImmutableList",
            "<",
            "Lcom/android/contacts/GroupMetaData;",
            ">;"
        }
    .end annotation
.end field

.field private final mId:J

.field private mIndicatePhoneOrSimContact:I

.field private mInvitableAccountTypes:Lcom/google/common/collect/ImmutableList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/common/collect/ImmutableList",
            "<",
            "Lcom/android/contacts/model/account/AccountType;",
            ">;"
        }
    .end annotation
.end field

.field private mIsSdnContact:I

.field private final mIsUserProfile:Z

.field private final mLookupKey:Ljava/lang/String;

.field private final mLookupUri:Landroid/net/Uri;

.field private final mNameRawContactId:J

.field private final mPhoneticName:Ljava/lang/String;

.field private mPhotoBinaryData:[B

.field private final mPhotoId:J

.field private final mPhotoUri:Ljava/lang/String;

.field private final mPresence:Ljava/lang/Integer;

.field private mRawContacts:Lcom/google/common/collect/ImmutableList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/common/collect/ImmutableList",
            "<",
            "Lcom/android/contacts/model/RawContact;",
            ">;"
        }
    .end annotation
.end field

.field private final mRequestedUri:Landroid/net/Uri;

.field private final mSendToVoicemail:Z

.field private mSimIndex:I

.field private mSlot:I

.field private final mStarred:Z

.field private final mStatus:Lcom/android/contacts/model/Contact$Status;

.field private mStatuses:Lcom/google/common/collect/ImmutableMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/common/collect/ImmutableMap",
            "<",
            "Ljava/lang/Long;",
            "Lcom/android/contacts/util/DataStatus;",
            ">;"
        }
    .end annotation
.end field

.field private mStreamItems:Lcom/google/common/collect/ImmutableList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/common/collect/ImmutableList",
            "<",
            "Lcom/android/contacts/util/StreamItemEntry;",
            ">;"
        }
    .end annotation
.end field

.field private final mUri:Landroid/net/Uri;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const-class v0, Lcom/android/contacts/model/Contact;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/android/contacts/model/Contact;->TAG:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Landroid/net/Uri;Landroid/net/Uri;Landroid/net/Uri;JLjava/lang/String;JJIJLjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ZLjava/lang/Integer;ZLjava/lang/String;Z)V
    .locals 2
    .param p1    # Landroid/net/Uri;
    .param p2    # Landroid/net/Uri;
    .param p3    # Landroid/net/Uri;
    .param p4    # J
    .param p6    # Ljava/lang/String;
    .param p7    # J
    .param p9    # J
    .param p11    # I
    .param p12    # J
    .param p14    # Ljava/lang/String;
    .param p15    # Ljava/lang/String;
    .param p16    # Ljava/lang/String;
    .param p17    # Ljava/lang/String;
    .param p18    # Z
    .param p19    # Ljava/lang/Integer;
    .param p20    # Z
    .param p21    # Ljava/lang/String;
    .param p22    # Z

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v1, -0x1

    iput v1, p0, Lcom/android/contacts/model/Contact;->mSlot:I

    const/4 v1, -0x1

    iput v1, p0, Lcom/android/contacts/model/Contact;->mIndicatePhoneOrSimContact:I

    const/4 v1, 0x0

    iput-boolean v1, p0, Lcom/android/contacts/model/Contact;->mBlockVideoCall:Z

    const/4 v1, -0x1

    iput v1, p0, Lcom/android/contacts/model/Contact;->mSimIndex:I

    const/4 v1, 0x0

    iput v1, p0, Lcom/android/contacts/model/Contact;->mIsSdnContact:I

    sget-object v1, Lcom/android/contacts/model/Contact$Status;->LOADED:Lcom/android/contacts/model/Contact$Status;

    iput-object v1, p0, Lcom/android/contacts/model/Contact;->mStatus:Lcom/android/contacts/model/Contact$Status;

    const/4 v1, 0x0

    iput-object v1, p0, Lcom/android/contacts/model/Contact;->mException:Ljava/lang/Exception;

    iput-object p1, p0, Lcom/android/contacts/model/Contact;->mRequestedUri:Landroid/net/Uri;

    iput-object p3, p0, Lcom/android/contacts/model/Contact;->mLookupUri:Landroid/net/Uri;

    iput-object p2, p0, Lcom/android/contacts/model/Contact;->mUri:Landroid/net/Uri;

    iput-wide p4, p0, Lcom/android/contacts/model/Contact;->mDirectoryId:J

    iput-object p6, p0, Lcom/android/contacts/model/Contact;->mLookupKey:Ljava/lang/String;

    iput-wide p7, p0, Lcom/android/contacts/model/Contact;->mId:J

    const/4 v1, 0x0

    iput-object v1, p0, Lcom/android/contacts/model/Contact;->mRawContacts:Lcom/google/common/collect/ImmutableList;

    const/4 v1, 0x0

    iput-object v1, p0, Lcom/android/contacts/model/Contact;->mStreamItems:Lcom/google/common/collect/ImmutableList;

    const/4 v1, 0x0

    iput-object v1, p0, Lcom/android/contacts/model/Contact;->mStatuses:Lcom/google/common/collect/ImmutableMap;

    iput-wide p9, p0, Lcom/android/contacts/model/Contact;->mNameRawContactId:J

    iput p11, p0, Lcom/android/contacts/model/Contact;->mDisplayNameSource:I

    iput-wide p12, p0, Lcom/android/contacts/model/Contact;->mPhotoId:J

    move-object/from16 v0, p14

    iput-object v0, p0, Lcom/android/contacts/model/Contact;->mPhotoUri:Ljava/lang/String;

    move-object/from16 v0, p15

    iput-object v0, p0, Lcom/android/contacts/model/Contact;->mDisplayName:Ljava/lang/String;

    move-object/from16 v0, p16

    iput-object v0, p0, Lcom/android/contacts/model/Contact;->mAltDisplayName:Ljava/lang/String;

    move-object/from16 v0, p17

    iput-object v0, p0, Lcom/android/contacts/model/Contact;->mPhoneticName:Ljava/lang/String;

    move/from16 v0, p18

    iput-boolean v0, p0, Lcom/android/contacts/model/Contact;->mStarred:Z

    move-object/from16 v0, p19

    iput-object v0, p0, Lcom/android/contacts/model/Contact;->mPresence:Ljava/lang/Integer;

    const/4 v1, 0x0

    iput-object v1, p0, Lcom/android/contacts/model/Contact;->mInvitableAccountTypes:Lcom/google/common/collect/ImmutableList;

    move/from16 v0, p20

    iput-boolean v0, p0, Lcom/android/contacts/model/Contact;->mSendToVoicemail:Z

    move-object/from16 v0, p21

    iput-object v0, p0, Lcom/android/contacts/model/Contact;->mCustomRingtone:Ljava/lang/String;

    move/from16 v0, p22

    iput-boolean v0, p0, Lcom/android/contacts/model/Contact;->mIsUserProfile:Z

    return-void
.end method

.method public constructor <init>(Landroid/net/Uri;Landroid/net/Uri;Landroid/net/Uri;JLjava/lang/String;JJIJLjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ZLjava/lang/Integer;ZLjava/lang/String;ZIZ)V
    .locals 1
    .param p1    # Landroid/net/Uri;
    .param p2    # Landroid/net/Uri;
    .param p3    # Landroid/net/Uri;
    .param p4    # J
    .param p6    # Ljava/lang/String;
    .param p7    # J
    .param p9    # J
    .param p11    # I
    .param p12    # J
    .param p14    # Ljava/lang/String;
    .param p15    # Ljava/lang/String;
    .param p16    # Ljava/lang/String;
    .param p17    # Ljava/lang/String;
    .param p18    # Z
    .param p19    # Ljava/lang/Integer;
    .param p20    # Z
    .param p21    # Ljava/lang/String;
    .param p22    # Z
    .param p23    # I
    .param p24    # Z

    invoke-direct/range {p0 .. p22}, Lcom/android/contacts/model/Contact;-><init>(Landroid/net/Uri;Landroid/net/Uri;Landroid/net/Uri;JLjava/lang/String;JJIJLjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ZLjava/lang/Integer;ZLjava/lang/String;Z)V

    move/from16 v0, p23

    iput v0, p0, Lcom/android/contacts/model/Contact;->mIsSdnContact:I

    move/from16 v0, p24

    iput-boolean v0, p0, Lcom/android/contacts/model/Contact;->mBlockVideoCall:Z

    return-void
.end method

.method private constructor <init>(Landroid/net/Uri;Lcom/android/contacts/model/Contact$Status;Ljava/lang/Exception;)V
    .locals 5
    .param p1    # Landroid/net/Uri;
    .param p2    # Lcom/android/contacts/model/Contact$Status;
    .param p3    # Ljava/lang/Exception;

    const/4 v0, -0x1

    const-wide/16 v3, -0x1

    const/4 v2, 0x0

    const/4 v1, 0x0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput v0, p0, Lcom/android/contacts/model/Contact;->mSlot:I

    iput v0, p0, Lcom/android/contacts/model/Contact;->mIndicatePhoneOrSimContact:I

    iput-boolean v2, p0, Lcom/android/contacts/model/Contact;->mBlockVideoCall:Z

    iput v0, p0, Lcom/android/contacts/model/Contact;->mSimIndex:I

    iput v2, p0, Lcom/android/contacts/model/Contact;->mIsSdnContact:I

    sget-object v0, Lcom/android/contacts/model/Contact$Status;->ERROR:Lcom/android/contacts/model/Contact$Status;

    if-ne p2, v0, :cond_0

    if-nez p3, :cond_0

    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "ERROR result must have exception"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    iput-object p2, p0, Lcom/android/contacts/model/Contact;->mStatus:Lcom/android/contacts/model/Contact$Status;

    iput-object p3, p0, Lcom/android/contacts/model/Contact;->mException:Ljava/lang/Exception;

    iput-object p1, p0, Lcom/android/contacts/model/Contact;->mRequestedUri:Landroid/net/Uri;

    iput-object v1, p0, Lcom/android/contacts/model/Contact;->mLookupUri:Landroid/net/Uri;

    iput-object v1, p0, Lcom/android/contacts/model/Contact;->mUri:Landroid/net/Uri;

    iput-wide v3, p0, Lcom/android/contacts/model/Contact;->mDirectoryId:J

    iput-object v1, p0, Lcom/android/contacts/model/Contact;->mLookupKey:Ljava/lang/String;

    iput-wide v3, p0, Lcom/android/contacts/model/Contact;->mId:J

    iput-object v1, p0, Lcom/android/contacts/model/Contact;->mRawContacts:Lcom/google/common/collect/ImmutableList;

    iput-object v1, p0, Lcom/android/contacts/model/Contact;->mStreamItems:Lcom/google/common/collect/ImmutableList;

    iput-object v1, p0, Lcom/android/contacts/model/Contact;->mStatuses:Lcom/google/common/collect/ImmutableMap;

    iput-wide v3, p0, Lcom/android/contacts/model/Contact;->mNameRawContactId:J

    iput v2, p0, Lcom/android/contacts/model/Contact;->mDisplayNameSource:I

    iput-wide v3, p0, Lcom/android/contacts/model/Contact;->mPhotoId:J

    iput-object v1, p0, Lcom/android/contacts/model/Contact;->mPhotoUri:Ljava/lang/String;

    iput-object v1, p0, Lcom/android/contacts/model/Contact;->mDisplayName:Ljava/lang/String;

    iput-object v1, p0, Lcom/android/contacts/model/Contact;->mAltDisplayName:Ljava/lang/String;

    iput-object v1, p0, Lcom/android/contacts/model/Contact;->mPhoneticName:Ljava/lang/String;

    iput-boolean v2, p0, Lcom/android/contacts/model/Contact;->mStarred:Z

    iput-object v1, p0, Lcom/android/contacts/model/Contact;->mPresence:Ljava/lang/Integer;

    iput-object v1, p0, Lcom/android/contacts/model/Contact;->mInvitableAccountTypes:Lcom/google/common/collect/ImmutableList;

    iput-boolean v2, p0, Lcom/android/contacts/model/Contact;->mSendToVoicemail:Z

    iput-object v1, p0, Lcom/android/contacts/model/Contact;->mCustomRingtone:Ljava/lang/String;

    iput-boolean v2, p0, Lcom/android/contacts/model/Contact;->mIsUserProfile:Z

    invoke-direct {p0}, Lcom/android/contacts/model/Contact;->initSIMMembers()V

    return-void
.end method

.method public constructor <init>(Landroid/net/Uri;Lcom/android/contacts/model/Contact;)V
    .locals 2
    .param p1    # Landroid/net/Uri;
    .param p2    # Lcom/android/contacts/model/Contact;

    const/4 v1, 0x0

    const/4 v0, -0x1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput v0, p0, Lcom/android/contacts/model/Contact;->mSlot:I

    iput v0, p0, Lcom/android/contacts/model/Contact;->mIndicatePhoneOrSimContact:I

    iput-boolean v1, p0, Lcom/android/contacts/model/Contact;->mBlockVideoCall:Z

    iput v0, p0, Lcom/android/contacts/model/Contact;->mSimIndex:I

    iput v1, p0, Lcom/android/contacts/model/Contact;->mIsSdnContact:I

    iput-object p1, p0, Lcom/android/contacts/model/Contact;->mRequestedUri:Landroid/net/Uri;

    iget-object v0, p2, Lcom/android/contacts/model/Contact;->mStatus:Lcom/android/contacts/model/Contact$Status;

    iput-object v0, p0, Lcom/android/contacts/model/Contact;->mStatus:Lcom/android/contacts/model/Contact$Status;

    iget-object v0, p2, Lcom/android/contacts/model/Contact;->mException:Ljava/lang/Exception;

    iput-object v0, p0, Lcom/android/contacts/model/Contact;->mException:Ljava/lang/Exception;

    iget-object v0, p2, Lcom/android/contacts/model/Contact;->mLookupUri:Landroid/net/Uri;

    iput-object v0, p0, Lcom/android/contacts/model/Contact;->mLookupUri:Landroid/net/Uri;

    iget-object v0, p2, Lcom/android/contacts/model/Contact;->mUri:Landroid/net/Uri;

    iput-object v0, p0, Lcom/android/contacts/model/Contact;->mUri:Landroid/net/Uri;

    iget-wide v0, p2, Lcom/android/contacts/model/Contact;->mDirectoryId:J

    iput-wide v0, p0, Lcom/android/contacts/model/Contact;->mDirectoryId:J

    iget-object v0, p2, Lcom/android/contacts/model/Contact;->mLookupKey:Ljava/lang/String;

    iput-object v0, p0, Lcom/android/contacts/model/Contact;->mLookupKey:Ljava/lang/String;

    iget-wide v0, p2, Lcom/android/contacts/model/Contact;->mId:J

    iput-wide v0, p0, Lcom/android/contacts/model/Contact;->mId:J

    iget-wide v0, p2, Lcom/android/contacts/model/Contact;->mNameRawContactId:J

    iput-wide v0, p0, Lcom/android/contacts/model/Contact;->mNameRawContactId:J

    iget v0, p2, Lcom/android/contacts/model/Contact;->mDisplayNameSource:I

    iput v0, p0, Lcom/android/contacts/model/Contact;->mDisplayNameSource:I

    iget-wide v0, p2, Lcom/android/contacts/model/Contact;->mPhotoId:J

    iput-wide v0, p0, Lcom/android/contacts/model/Contact;->mPhotoId:J

    iget-object v0, p2, Lcom/android/contacts/model/Contact;->mPhotoUri:Ljava/lang/String;

    iput-object v0, p0, Lcom/android/contacts/model/Contact;->mPhotoUri:Ljava/lang/String;

    iget-object v0, p2, Lcom/android/contacts/model/Contact;->mDisplayName:Ljava/lang/String;

    iput-object v0, p0, Lcom/android/contacts/model/Contact;->mDisplayName:Ljava/lang/String;

    iget-object v0, p2, Lcom/android/contacts/model/Contact;->mAltDisplayName:Ljava/lang/String;

    iput-object v0, p0, Lcom/android/contacts/model/Contact;->mAltDisplayName:Ljava/lang/String;

    iget-object v0, p2, Lcom/android/contacts/model/Contact;->mPhoneticName:Ljava/lang/String;

    iput-object v0, p0, Lcom/android/contacts/model/Contact;->mPhoneticName:Ljava/lang/String;

    iget-boolean v0, p2, Lcom/android/contacts/model/Contact;->mStarred:Z

    iput-boolean v0, p0, Lcom/android/contacts/model/Contact;->mStarred:Z

    iget-object v0, p2, Lcom/android/contacts/model/Contact;->mPresence:Ljava/lang/Integer;

    iput-object v0, p0, Lcom/android/contacts/model/Contact;->mPresence:Ljava/lang/Integer;

    iget-object v0, p2, Lcom/android/contacts/model/Contact;->mRawContacts:Lcom/google/common/collect/ImmutableList;

    iput-object v0, p0, Lcom/android/contacts/model/Contact;->mRawContacts:Lcom/google/common/collect/ImmutableList;

    iget-object v0, p2, Lcom/android/contacts/model/Contact;->mStreamItems:Lcom/google/common/collect/ImmutableList;

    iput-object v0, p0, Lcom/android/contacts/model/Contact;->mStreamItems:Lcom/google/common/collect/ImmutableList;

    iget-object v0, p2, Lcom/android/contacts/model/Contact;->mStatuses:Lcom/google/common/collect/ImmutableMap;

    iput-object v0, p0, Lcom/android/contacts/model/Contact;->mStatuses:Lcom/google/common/collect/ImmutableMap;

    iget-object v0, p2, Lcom/android/contacts/model/Contact;->mInvitableAccountTypes:Lcom/google/common/collect/ImmutableList;

    iput-object v0, p0, Lcom/android/contacts/model/Contact;->mInvitableAccountTypes:Lcom/google/common/collect/ImmutableList;

    iget-object v0, p2, Lcom/android/contacts/model/Contact;->mDirectoryDisplayName:Ljava/lang/String;

    iput-object v0, p0, Lcom/android/contacts/model/Contact;->mDirectoryDisplayName:Ljava/lang/String;

    iget-object v0, p2, Lcom/android/contacts/model/Contact;->mDirectoryType:Ljava/lang/String;

    iput-object v0, p0, Lcom/android/contacts/model/Contact;->mDirectoryType:Ljava/lang/String;

    iget-object v0, p2, Lcom/android/contacts/model/Contact;->mDirectoryAccountType:Ljava/lang/String;

    iput-object v0, p0, Lcom/android/contacts/model/Contact;->mDirectoryAccountType:Ljava/lang/String;

    iget-object v0, p2, Lcom/android/contacts/model/Contact;->mDirectoryAccountName:Ljava/lang/String;

    iput-object v0, p0, Lcom/android/contacts/model/Contact;->mDirectoryAccountName:Ljava/lang/String;

    iget v0, p2, Lcom/android/contacts/model/Contact;->mDirectoryExportSupport:I

    iput v0, p0, Lcom/android/contacts/model/Contact;->mDirectoryExportSupport:I

    iget-object v0, p2, Lcom/android/contacts/model/Contact;->mGroups:Lcom/google/common/collect/ImmutableList;

    iput-object v0, p0, Lcom/android/contacts/model/Contact;->mGroups:Lcom/google/common/collect/ImmutableList;

    iget-object v0, p2, Lcom/android/contacts/model/Contact;->mPhotoBinaryData:[B

    iput-object v0, p0, Lcom/android/contacts/model/Contact;->mPhotoBinaryData:[B

    iget-boolean v0, p2, Lcom/android/contacts/model/Contact;->mSendToVoicemail:Z

    iput-boolean v0, p0, Lcom/android/contacts/model/Contact;->mSendToVoicemail:Z

    iget-object v0, p2, Lcom/android/contacts/model/Contact;->mCustomRingtone:Ljava/lang/String;

    iput-object v0, p0, Lcom/android/contacts/model/Contact;->mCustomRingtone:Ljava/lang/String;

    iget-boolean v0, p2, Lcom/android/contacts/model/Contact;->mIsUserProfile:Z

    iput-boolean v0, p0, Lcom/android/contacts/model/Contact;->mIsUserProfile:Z

    invoke-direct {p0, p2}, Lcom/android/contacts/model/Contact;->initSIMMembers(Lcom/android/contacts/model/Contact;)V

    return-void
.end method

.method public static forError(Landroid/net/Uri;Ljava/lang/Exception;)Lcom/android/contacts/model/Contact;
    .locals 2
    .param p0    # Landroid/net/Uri;
    .param p1    # Ljava/lang/Exception;

    new-instance v0, Lcom/android/contacts/model/Contact;

    sget-object v1, Lcom/android/contacts/model/Contact$Status;->ERROR:Lcom/android/contacts/model/Contact$Status;

    invoke-direct {v0, p0, v1, p1}, Lcom/android/contacts/model/Contact;-><init>(Landroid/net/Uri;Lcom/android/contacts/model/Contact$Status;Ljava/lang/Exception;)V

    return-object v0
.end method

.method public static forNotFound(Landroid/net/Uri;)Lcom/android/contacts/model/Contact;
    .locals 3
    .param p0    # Landroid/net/Uri;

    new-instance v0, Lcom/android/contacts/model/Contact;

    sget-object v1, Lcom/android/contacts/model/Contact$Status;->NOT_FOUND:Lcom/android/contacts/model/Contact$Status;

    const/4 v2, 0x0

    invoke-direct {v0, p0, v1, v2}, Lcom/android/contacts/model/Contact;-><init>(Landroid/net/Uri;Lcom/android/contacts/model/Contact$Status;Ljava/lang/Exception;)V

    return-object v0
.end method

.method private initSIMMembers()V
    .locals 2

    const/4 v1, 0x0

    const/4 v0, -0x1

    iput v0, p0, Lcom/android/contacts/model/Contact;->mIndicatePhoneOrSimContact:I

    iput v0, p0, Lcom/android/contacts/model/Contact;->mSimIndex:I

    iput-boolean v1, p0, Lcom/android/contacts/model/Contact;->mBlockVideoCall:Z

    iput v1, p0, Lcom/android/contacts/model/Contact;->mIsSdnContact:I

    return-void
.end method

.method private initSIMMembers(Lcom/android/contacts/model/Contact;)V
    .locals 3
    .param p1    # Lcom/android/contacts/model/Contact;

    sget-object v0, Lcom/android/contacts/model/Contact;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "initSIMMembers begin mIndicatePhoneOrSimContact , mSimIndex , mBlockVideoCall, mIsSdnContact : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/android/contacts/model/Contact;->mIndicatePhoneOrSimContact:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " , "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/android/contacts/model/Contact;->mSimIndex:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " , "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-boolean v2, p0, Lcom/android/contacts/model/Contact;->mBlockVideoCall:Z

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " , "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/android/contacts/model/Contact;->mIsSdnContact:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    iget v0, p1, Lcom/android/contacts/model/Contact;->mIndicatePhoneOrSimContact:I

    iput v0, p0, Lcom/android/contacts/model/Contact;->mIndicatePhoneOrSimContact:I

    iget v0, p1, Lcom/android/contacts/model/Contact;->mSimIndex:I

    iput v0, p0, Lcom/android/contacts/model/Contact;->mSimIndex:I

    iget-boolean v0, p1, Lcom/android/contacts/model/Contact;->mBlockVideoCall:Z

    iput-boolean v0, p0, Lcom/android/contacts/model/Contact;->mBlockVideoCall:Z

    iget v0, p1, Lcom/android/contacts/model/Contact;->mIsSdnContact:I

    iput v0, p0, Lcom/android/contacts/model/Contact;->mIsSdnContact:I

    sget-object v0, Lcom/android/contacts/model/Contact;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "initSIMMembers end mIndicatePhoneOrSimContact , mSimIndex , mBlockVideoCall, mIsSdnContact : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/android/contacts/model/Contact;->mIndicatePhoneOrSimContact:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " , "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/android/contacts/model/Contact;->mSimIndex:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " , "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-boolean v2, p0, Lcom/android/contacts/model/Contact;->mBlockVideoCall:Z

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " , "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/android/contacts/model/Contact;->mIsSdnContact:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    return-void
.end method


# virtual methods
.method public createRawContactDeltaList()Lcom/android/contacts/model/RawContactDeltaList;
    .locals 1

    invoke-virtual {p0}, Lcom/android/contacts/model/Contact;->getRawContacts()Lcom/google/common/collect/ImmutableList;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/common/collect/ImmutableList;->iterator()Lcom/google/common/collect/UnmodifiableIterator;

    move-result-object v0

    invoke-static {v0}, Lcom/android/contacts/model/RawContactDeltaList;->fromIterator(Ljava/util/Iterator;)Lcom/android/contacts/model/RawContactDeltaList;

    move-result-object v0

    return-object v0
.end method

.method public getAltDisplayName()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/android/contacts/model/Contact;->mAltDisplayName:Ljava/lang/String;

    return-object v0
.end method

.method public getContactId()J
    .locals 2

    iget-wide v0, p0, Lcom/android/contacts/model/Contact;->mId:J

    return-wide v0
.end method

.method public getContentValues()Ljava/util/ArrayList;
    .locals 9
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList",
            "<",
            "Landroid/content/ContentValues;",
            ">;"
        }
    .end annotation

    iget-object v5, p0, Lcom/android/contacts/model/Contact;->mRawContacts:Lcom/google/common/collect/ImmutableList;

    invoke-virtual {v5}, Lcom/google/common/collect/ImmutableCollection;->size()I

    move-result v5

    const/4 v6, 0x1

    if-eq v5, v6, :cond_0

    new-instance v5, Ljava/lang/IllegalStateException;

    const-string v6, "Cannot extract content values from an aggregated contact"

    invoke-direct {v5, v6}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v5

    :cond_0
    iget-object v5, p0, Lcom/android/contacts/model/Contact;->mRawContacts:Lcom/google/common/collect/ImmutableList;

    const/4 v6, 0x0

    invoke-virtual {v5, v6}, Lcom/google/common/collect/ImmutableList;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/android/contacts/model/RawContact;

    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    invoke-virtual {v3}, Lcom/android/contacts/model/RawContact;->getDataItems()Ljava/util/List;

    move-result-object v5

    invoke-interface {v5}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/contacts/model/dataitem/DataItem;

    invoke-virtual {v0}, Lcom/android/contacts/model/dataitem/DataItem;->getContentValues()Landroid/content/ContentValues;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_1
    iget-wide v5, p0, Lcom/android/contacts/model/Contact;->mPhotoId:J

    const-wide/16 v7, 0x0

    cmp-long v5, v5, v7

    if-nez v5, :cond_2

    iget-object v5, p0, Lcom/android/contacts/model/Contact;->mPhotoBinaryData:[B

    if-eqz v5, :cond_2

    new-instance v2, Landroid/content/ContentValues;

    invoke-direct {v2}, Landroid/content/ContentValues;-><init>()V

    const-string v5, "mimetype"

    const-string v6, "vnd.android.cursor.item/photo"

    invoke-virtual {v2, v5, v6}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    const-string v5, "data15"

    iget-object v6, p0, Lcom/android/contacts/model/Contact;->mPhotoBinaryData:[B

    invoke-virtual {v2, v5, v6}, Landroid/content/ContentValues;->put(Ljava/lang/String;[B)V

    invoke-virtual {v4, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :cond_2
    return-object v4
.end method

.method public getCustomRingtone()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/android/contacts/model/Contact;->mCustomRingtone:Ljava/lang/String;

    return-object v0
.end method

.method public getDirectoryAccountName()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/android/contacts/model/Contact;->mDirectoryAccountName:Ljava/lang/String;

    return-object v0
.end method

.method public getDirectoryAccountType()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/android/contacts/model/Contact;->mDirectoryAccountType:Ljava/lang/String;

    return-object v0
.end method

.method public getDirectoryDisplayName()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/android/contacts/model/Contact;->mDirectoryDisplayName:Ljava/lang/String;

    return-object v0
.end method

.method public getDirectoryExportSupport()I
    .locals 1

    iget v0, p0, Lcom/android/contacts/model/Contact;->mDirectoryExportSupport:I

    return v0
.end method

.method public getDirectoryId()J
    .locals 2

    iget-wide v0, p0, Lcom/android/contacts/model/Contact;->mDirectoryId:J

    return-wide v0
.end method

.method public getDirectoryType()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/android/contacts/model/Contact;->mDirectoryType:Ljava/lang/String;

    return-object v0
.end method

.method public getDisplayName()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/android/contacts/model/Contact;->mDisplayName:Ljava/lang/String;

    return-object v0
.end method

.method public getDisplayNameSource()I
    .locals 1

    iget v0, p0, Lcom/android/contacts/model/Contact;->mDisplayNameSource:I

    return v0
.end method

.method public getException()Ljava/lang/Exception;
    .locals 1

    iget-object v0, p0, Lcom/android/contacts/model/Contact;->mException:Ljava/lang/Exception;

    return-object v0
.end method

.method public getFirstWritableRawContactId(Landroid/content/Context;)J
    .locals 6
    .param p1    # Landroid/content/Context;

    const-wide/16 v3, -0x1

    invoke-virtual {p0}, Lcom/android/contacts/model/Contact;->isDirectoryEntry()Z

    move-result v5

    if-eqz v5, :cond_1

    :cond_0
    :goto_0
    return-wide v3

    :cond_1
    invoke-virtual {p0}, Lcom/android/contacts/model/Contact;->getRawContacts()Lcom/google/common/collect/ImmutableList;

    move-result-object v5

    invoke-virtual {v5}, Lcom/google/common/collect/ImmutableList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_2
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/android/contacts/model/RawContact;

    invoke-virtual {v2}, Lcom/android/contacts/model/RawContact;->getAccountType()Lcom/android/contacts/model/account/AccountType;

    move-result-object v0

    if-eqz v0, :cond_2

    invoke-virtual {v0}, Lcom/android/contacts/model/account/AccountType;->areContactsWritable()Z

    move-result v5

    if-eqz v5, :cond_2

    invoke-virtual {v2}, Lcom/android/contacts/model/RawContact;->getId()Ljava/lang/Long;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Long;->longValue()J

    move-result-wide v3

    goto :goto_0
.end method

.method public getGroupMetaData()Lcom/google/common/collect/ImmutableList;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/common/collect/ImmutableList",
            "<",
            "Lcom/android/contacts/GroupMetaData;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/android/contacts/model/Contact;->mGroups:Lcom/google/common/collect/ImmutableList;

    return-object v0
.end method

.method public getId()J
    .locals 2
    .annotation build Lcom/google/common/annotations/VisibleForTesting;
    .end annotation

    iget-wide v0, p0, Lcom/android/contacts/model/Contact;->mId:J

    return-wide v0
.end method

.method public getIndicate()I
    .locals 1

    iget v0, p0, Lcom/android/contacts/model/Contact;->mIndicatePhoneOrSimContact:I

    return v0
.end method

.method public getInvitableAccountTypes()Lcom/google/common/collect/ImmutableList;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/common/collect/ImmutableList",
            "<",
            "Lcom/android/contacts/model/account/AccountType;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/android/contacts/model/Contact;->mInvitableAccountTypes:Lcom/google/common/collect/ImmutableList;

    return-object v0
.end method

.method public getLookupKey()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/android/contacts/model/Contact;->mLookupKey:Ljava/lang/String;

    return-object v0
.end method

.method public getLookupUri()Landroid/net/Uri;
    .locals 1

    iget-object v0, p0, Lcom/android/contacts/model/Contact;->mLookupUri:Landroid/net/Uri;

    return-object v0
.end method

.method public getNameRawContactId()J
    .locals 2

    iget-wide v0, p0, Lcom/android/contacts/model/Contact;->mNameRawContactId:J

    return-wide v0
.end method

.method public getPhoneticName()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/android/contacts/model/Contact;->mPhoneticName:Ljava/lang/String;

    return-object v0
.end method

.method public getPhotoBinaryData()[B
    .locals 1

    iget-object v0, p0, Lcom/android/contacts/model/Contact;->mPhotoBinaryData:[B

    return-object v0
.end method

.method public getPhotoId()J
    .locals 2

    iget-wide v0, p0, Lcom/android/contacts/model/Contact;->mPhotoId:J

    return-wide v0
.end method

.method public getPhotoUri()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/android/contacts/model/Contact;->mPhotoUri:Ljava/lang/String;

    return-object v0
.end method

.method public getPresence()Ljava/lang/Integer;
    .locals 1

    iget-object v0, p0, Lcom/android/contacts/model/Contact;->mPresence:Ljava/lang/Integer;

    return-object v0
.end method

.method public getRawContacts()Lcom/google/common/collect/ImmutableList;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/common/collect/ImmutableList",
            "<",
            "Lcom/android/contacts/model/RawContact;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/android/contacts/model/Contact;->mRawContacts:Lcom/google/common/collect/ImmutableList;

    return-object v0
.end method

.method public getRequestedUri()Landroid/net/Uri;
    .locals 1

    iget-object v0, p0, Lcom/android/contacts/model/Contact;->mRequestedUri:Landroid/net/Uri;

    return-object v0
.end method

.method public getSimIndex()I
    .locals 1

    iget v0, p0, Lcom/android/contacts/model/Contact;->mSimIndex:I

    return v0
.end method

.method public getSlot()I
    .locals 1

    iget v0, p0, Lcom/android/contacts/model/Contact;->mSlot:I

    return v0
.end method

.method public getStarred()Z
    .locals 1

    iget-boolean v0, p0, Lcom/android/contacts/model/Contact;->mStarred:Z

    return v0
.end method

.method public getStatuses()Lcom/google/common/collect/ImmutableMap;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/common/collect/ImmutableMap",
            "<",
            "Ljava/lang/Long;",
            "Lcom/android/contacts/util/DataStatus;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/android/contacts/model/Contact;->mStatuses:Lcom/google/common/collect/ImmutableMap;

    return-object v0
.end method

.method public getStreamItems()Lcom/google/common/collect/ImmutableList;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/common/collect/ImmutableList",
            "<",
            "Lcom/android/contacts/util/StreamItemEntry;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/android/contacts/model/Contact;->mStreamItems:Lcom/google/common/collect/ImmutableList;

    return-object v0
.end method

.method public getUri()Landroid/net/Uri;
    .locals 1

    iget-object v0, p0, Lcom/android/contacts/model/Contact;->mUri:Landroid/net/Uri;

    return-object v0
.end method

.method public isBlockVideoCall()Z
    .locals 1

    iget-boolean v0, p0, Lcom/android/contacts/model/Contact;->mBlockVideoCall:Z

    return v0
.end method

.method public isDirectoryEntry()Z
    .locals 4

    iget-wide v0, p0, Lcom/android/contacts/model/Contact;->mDirectoryId:J

    const-wide/16 v2, -0x1

    cmp-long v0, v0, v2

    if-eqz v0, :cond_0

    iget-wide v0, p0, Lcom/android/contacts/model/Contact;->mDirectoryId:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-eqz v0, :cond_0

    iget-wide v0, p0, Lcom/android/contacts/model/Contact;->mDirectoryId:J

    const-wide/16 v2, 0x1

    cmp-long v0, v0, v2

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isError()Z
    .locals 2

    iget-object v0, p0, Lcom/android/contacts/model/Contact;->mStatus:Lcom/android/contacts/model/Contact$Status;

    sget-object v1, Lcom/android/contacts/model/Contact$Status;->ERROR:Lcom/android/contacts/model/Contact$Status;

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isLoaded()Z
    .locals 2

    iget-object v0, p0, Lcom/android/contacts/model/Contact;->mStatus:Lcom/android/contacts/model/Contact$Status;

    sget-object v1, Lcom/android/contacts/model/Contact$Status;->LOADED:Lcom/android/contacts/model/Contact$Status;

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isNotFound()Z
    .locals 2

    iget-object v0, p0, Lcom/android/contacts/model/Contact;->mStatus:Lcom/android/contacts/model/Contact$Status;

    sget-object v1, Lcom/android/contacts/model/Contact$Status;->NOT_FOUND:Lcom/android/contacts/model/Contact$Status;

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isSdnContacts()Z
    .locals 2

    const/4 v0, 0x1

    iget v1, p0, Lcom/android/contacts/model/Contact;->mIsSdnContact:I

    if-ne v1, v0, :cond_0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isSendToVoicemail()Z
    .locals 1

    iget-boolean v0, p0, Lcom/android/contacts/model/Contact;->mSendToVoicemail:Z

    return v0
.end method

.method public isUserProfile()Z
    .locals 1

    iget-boolean v0, p0, Lcom/android/contacts/model/Contact;->mIsUserProfile:Z

    return v0
.end method

.method public isWritableContact(Landroid/content/Context;)Z
    .locals 4
    .param p1    # Landroid/content/Context;

    invoke-virtual {p0, p1}, Lcom/android/contacts/model/Contact;->getFirstWritableRawContactId(Landroid/content/Context;)J

    move-result-wide v0

    const-wide/16 v2, -0x1

    cmp-long v0, v0, v2

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public setDirectoryMetaData(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;I)V
    .locals 0
    .param p1    # Ljava/lang/String;
    .param p2    # Ljava/lang/String;
    .param p3    # Ljava/lang/String;
    .param p4    # Ljava/lang/String;
    .param p5    # I

    iput-object p1, p0, Lcom/android/contacts/model/Contact;->mDirectoryDisplayName:Ljava/lang/String;

    iput-object p2, p0, Lcom/android/contacts/model/Contact;->mDirectoryType:Ljava/lang/String;

    iput-object p3, p0, Lcom/android/contacts/model/Contact;->mDirectoryAccountType:Ljava/lang/String;

    iput-object p4, p0, Lcom/android/contacts/model/Contact;->mDirectoryAccountName:Ljava/lang/String;

    iput p5, p0, Lcom/android/contacts/model/Contact;->mDirectoryExportSupport:I

    return-void
.end method

.method setGroupMetaData(Lcom/google/common/collect/ImmutableList;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/common/collect/ImmutableList",
            "<",
            "Lcom/android/contacts/GroupMetaData;",
            ">;)V"
        }
    .end annotation

    iput-object p1, p0, Lcom/android/contacts/model/Contact;->mGroups:Lcom/google/common/collect/ImmutableList;

    return-void
.end method

.method public setIndicate(I)V
    .locals 0
    .param p1    # I

    iput p1, p0, Lcom/android/contacts/model/Contact;->mIndicatePhoneOrSimContact:I

    return-void
.end method

.method setInvitableAccountTypes(Lcom/google/common/collect/ImmutableList;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/common/collect/ImmutableList",
            "<",
            "Lcom/android/contacts/model/account/AccountType;",
            ">;)V"
        }
    .end annotation

    iput-object p1, p0, Lcom/android/contacts/model/Contact;->mInvitableAccountTypes:Lcom/google/common/collect/ImmutableList;

    return-void
.end method

.method setPhotoBinaryData([B)V
    .locals 0
    .param p1    # [B

    iput-object p1, p0, Lcom/android/contacts/model/Contact;->mPhotoBinaryData:[B

    return-void
.end method

.method setRawContacts(Lcom/google/common/collect/ImmutableList;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/common/collect/ImmutableList",
            "<",
            "Lcom/android/contacts/model/RawContact;",
            ">;)V"
        }
    .end annotation

    iput-object p1, p0, Lcom/android/contacts/model/Contact;->mRawContacts:Lcom/google/common/collect/ImmutableList;

    return-void
.end method

.method public setSimIndex(I)V
    .locals 0
    .param p1    # I

    iput p1, p0, Lcom/android/contacts/model/Contact;->mSimIndex:I

    return-void
.end method

.method public setSlot(I)V
    .locals 0
    .param p1    # I

    iput p1, p0, Lcom/android/contacts/model/Contact;->mSlot:I

    return-void
.end method

.method setStatuses(Lcom/google/common/collect/ImmutableMap;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/common/collect/ImmutableMap",
            "<",
            "Ljava/lang/Long;",
            "Lcom/android/contacts/util/DataStatus;",
            ">;)V"
        }
    .end annotation

    iput-object p1, p0, Lcom/android/contacts/model/Contact;->mStatuses:Lcom/google/common/collect/ImmutableMap;

    return-void
.end method

.method setStreamItems(Lcom/google/common/collect/ImmutableList;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/common/collect/ImmutableList",
            "<",
            "Lcom/android/contacts/util/StreamItemEntry;",
            ">;)V"
        }
    .end annotation

    iput-object p1, p0, Lcom/android/contacts/model/Contact;->mStreamItems:Lcom/google/common/collect/ImmutableList;

    return-void
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "{requested="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/android/contacts/model/Contact;->mRequestedUri:Landroid/net/Uri;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ",lookupkey="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/android/contacts/model/Contact;->mLookupKey:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ",uri="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/android/contacts/model/Contact;->mUri:Landroid/net/Uri;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ",status="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/android/contacts/model/Contact;->mStatus:Lcom/android/contacts/model/Contact$Status;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "}"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
