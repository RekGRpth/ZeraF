.class Lcom/android/settings/autostarts/LewaManageAutostarts$1;
.super Ljava/lang/Object;
.source "LewaManageAutostarts.java"

# interfaces
.implements Landroid/widget/AdapterView$OnItemClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/android/settings/autostarts/LewaManageAutostarts;->onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/settings/autostarts/LewaManageAutostarts;


# direct methods
.method constructor <init>(Lcom/android/settings/autostarts/LewaManageAutostarts;)V
    .locals 0

    iput-object p1, p0, Lcom/android/settings/autostarts/LewaManageAutostarts$1;->this$0:Lcom/android/settings/autostarts/LewaManageAutostarts;

    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onItemClick(Landroid/widget/AdapterView;Landroid/view/View;IJ)V
    .locals 3
    .param p2    # Landroid/view/View;
    .param p3    # I
    .param p4    # J
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/widget/AdapterView",
            "<*>;",
            "Landroid/view/View;",
            "IJ)V"
        }
    .end annotation

    const/4 v2, 0x1

    iget-object v1, p0, Lcom/android/settings/autostarts/LewaManageAutostarts$1;->this$0:Lcom/android/settings/autostarts/LewaManageAutostarts;

    # getter for: Lcom/android/settings/autostarts/LewaManageAutostarts;->mAppAdapter:Lcom/android/settings/autostarts/LewaManageAutostarts$AppAdapter;
    invoke-static {v1}, Lcom/android/settings/autostarts/LewaManageAutostarts;->access$000(Lcom/android/settings/autostarts/LewaManageAutostarts;)Lcom/android/settings/autostarts/LewaManageAutostarts$AppAdapter;

    move-result-object v1

    invoke-virtual {v1, p3}, Lcom/android/settings/autostarts/LewaManageAutostarts$AppAdapter;->getItem(I)Lcom/android/settings/autostarts/AppItem;

    move-result-object v0

    iget-boolean v1, v0, Lcom/android/settings/autostarts/AppItem;->checked:Z

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/android/settings/autostarts/LewaManageAutostarts$1;->this$0:Lcom/android/settings/autostarts/LewaManageAutostarts;

    invoke-virtual {v1, v0}, Lcom/android/settings/autostarts/LewaManageAutostarts;->closeAutoStartApp(Lcom/android/settings/autostarts/AppItem;)V

    const/4 v1, 0x0

    iput-boolean v1, v0, Lcom/android/settings/autostarts/AppItem;->checked:Z

    iget-object v1, p0, Lcom/android/settings/autostarts/LewaManageAutostarts$1;->this$0:Lcom/android/settings/autostarts/LewaManageAutostarts;

    # getter for: Lcom/android/settings/autostarts/LewaManageAutostarts;->mOptimizedSize:I
    invoke-static {v1}, Lcom/android/settings/autostarts/LewaManageAutostarts;->access$100(Lcom/android/settings/autostarts/LewaManageAutostarts;)I

    move-result v1

    if-lez v1, :cond_0

    iget-object v1, p0, Lcom/android/settings/autostarts/LewaManageAutostarts$1;->this$0:Lcom/android/settings/autostarts/LewaManageAutostarts;

    # operator-- for: Lcom/android/settings/autostarts/LewaManageAutostarts;->mOptimizedSize:I
    invoke-static {v1}, Lcom/android/settings/autostarts/LewaManageAutostarts;->access$110(Lcom/android/settings/autostarts/LewaManageAutostarts;)I

    :cond_0
    iget-object v1, p0, Lcom/android/settings/autostarts/LewaManageAutostarts$1;->this$0:Lcom/android/settings/autostarts/LewaManageAutostarts;

    iget-object v1, v1, Lcom/android/settings/autostarts/LewaManageAutostarts;->handler:Landroid/os/Handler;

    invoke-virtual {v1, v2}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    :goto_0
    return-void

    :cond_1
    iget-object v1, p0, Lcom/android/settings/autostarts/LewaManageAutostarts$1;->this$0:Lcom/android/settings/autostarts/LewaManageAutostarts;

    invoke-virtual {v1, v0}, Lcom/android/settings/autostarts/LewaManageAutostarts;->startAutoStartApp(Lcom/android/settings/autostarts/AppItem;)V

    iput-boolean v2, v0, Lcom/android/settings/autostarts/AppItem;->checked:Z

    iget-object v1, p0, Lcom/android/settings/autostarts/LewaManageAutostarts$1;->this$0:Lcom/android/settings/autostarts/LewaManageAutostarts;

    # operator++ for: Lcom/android/settings/autostarts/LewaManageAutostarts;->mOptimizedSize:I
    invoke-static {v1}, Lcom/android/settings/autostarts/LewaManageAutostarts;->access$108(Lcom/android/settings/autostarts/LewaManageAutostarts;)I

    iget-object v1, p0, Lcom/android/settings/autostarts/LewaManageAutostarts$1;->this$0:Lcom/android/settings/autostarts/LewaManageAutostarts;

    iget-object v1, v1, Lcom/android/settings/autostarts/LewaManageAutostarts;->handler:Landroid/os/Handler;

    invoke-virtual {v1, v2}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    goto :goto_0
.end method
