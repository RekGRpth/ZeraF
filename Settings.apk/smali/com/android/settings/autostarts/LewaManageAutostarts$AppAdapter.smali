.class Lcom/android/settings/autostarts/LewaManageAutostarts$AppAdapter;
.super Landroid/widget/BaseAdapter;
.source "LewaManageAutostarts.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/settings/autostarts/LewaManageAutostarts;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = "AppAdapter"
.end annotation


# instance fields
.field protected context:Landroid/content/Context;

.field protected inflater:Landroid/view/LayoutInflater;

.field protected list:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/android/settings/autostarts/AppItem;",
            ">;"
        }
    .end annotation
.end field

.field final synthetic this$0:Lcom/android/settings/autostarts/LewaManageAutostarts;


# direct methods
.method public constructor <init>(Lcom/android/settings/autostarts/LewaManageAutostarts;Landroid/view/LayoutInflater;Ljava/util/List;)V
    .locals 0
    .param p2    # Landroid/view/LayoutInflater;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/view/LayoutInflater;",
            "Ljava/util/List",
            "<",
            "Lcom/android/settings/autostarts/AppItem;",
            ">;)V"
        }
    .end annotation

    iput-object p1, p0, Lcom/android/settings/autostarts/LewaManageAutostarts$AppAdapter;->this$0:Lcom/android/settings/autostarts/LewaManageAutostarts;

    invoke-direct {p0}, Landroid/widget/BaseAdapter;-><init>()V

    iput-object p3, p0, Lcom/android/settings/autostarts/LewaManageAutostarts$AppAdapter;->list:Ljava/util/List;

    return-void
.end method


# virtual methods
.method public add(Lcom/android/settings/autostarts/AppItem;)V
    .locals 1
    .param p1    # Lcom/android/settings/autostarts/AppItem;

    iget-object v0, p0, Lcom/android/settings/autostarts/LewaManageAutostarts$AppAdapter;->list:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    return-void
.end method

.method public clear()V
    .locals 1

    iget-object v0, p0, Lcom/android/settings/autostarts/LewaManageAutostarts$AppAdapter;->list:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    return-void
.end method

.method public getCount()I
    .locals 1

    iget-object v0, p0, Lcom/android/settings/autostarts/LewaManageAutostarts$AppAdapter;->list:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    return v0
.end method

.method public getItem(I)Lcom/android/settings/autostarts/AppItem;
    .locals 1
    .param p1    # I

    iget-object v0, p0, Lcom/android/settings/autostarts/LewaManageAutostarts$AppAdapter;->list:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/settings/autostarts/AppItem;

    return-object v0
.end method

.method public bridge synthetic getItem(I)Ljava/lang/Object;
    .locals 1
    .param p1    # I

    invoke-virtual {p0, p1}, Lcom/android/settings/autostarts/LewaManageAutostarts$AppAdapter;->getItem(I)Lcom/android/settings/autostarts/AppItem;

    move-result-object v0

    return-object v0
.end method

.method public getItemId(I)J
    .locals 2
    .param p1    # I

    int-to-long v0, p1

    return-wide v0
.end method

.method public getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 13
    .param p1    # I
    .param p2    # Landroid/view/View;
    .param p3    # Landroid/view/ViewGroup;

    if-nez p2, :cond_0

    iget-object v10, p0, Lcom/android/settings/autostarts/LewaManageAutostarts$AppAdapter;->this$0:Lcom/android/settings/autostarts/LewaManageAutostarts;

    # getter for: Lcom/android/settings/autostarts/LewaManageAutostarts;->mLayoutInflater:Landroid/view/LayoutInflater;
    invoke-static {v10}, Lcom/android/settings/autostarts/LewaManageAutostarts;->access$500(Lcom/android/settings/autostarts/LewaManageAutostarts;)Landroid/view/LayoutInflater;

    move-result-object v10

    const v11, 0x7f0400b4

    const/4 v12, 0x0

    invoke-virtual {v10, v11, v12}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object p2

    :cond_0
    const v10, 0x7f080276

    invoke-virtual {p2, v10}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v9

    const v10, 0x7f0200de

    invoke-virtual {v9, v10}, Landroid/view/View;->setBackgroundResource(I)V

    const v10, 0x7f080023

    invoke-virtual {p2, v10}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v6

    check-cast v6, Landroid/widget/ImageView;

    const v10, 0x7f080279

    invoke-virtual {p2, v10}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v7

    check-cast v7, Landroid/widget/TextView;

    const v10, 0x7f08027a

    invoke-virtual {p2, v10}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v8

    check-cast v8, Landroid/widget/TextView;

    const/4 v10, 0x1

    invoke-virtual {v7, v10}, Landroid/widget/TextView;->setSingleLine(Z)V

    const v10, 0x7f08027b

    invoke-virtual {p2, v10}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v5

    check-cast v5, Landroid/widget/CheckBox;

    const v10, 0x7f080277

    invoke-virtual {p2, v10}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    const v10, 0x7f080278

    invoke-virtual {p2, v10}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    const-string v1, ""

    if-nez p1, :cond_2

    iget-object v10, p0, Lcom/android/settings/autostarts/LewaManageAutostarts$AppAdapter;->this$0:Lcom/android/settings/autostarts/LewaManageAutostarts;

    # getter for: Lcom/android/settings/autostarts/LewaManageAutostarts;->mList:Ljava/util/List;
    invoke-static {v10}, Lcom/android/settings/autostarts/LewaManageAutostarts;->access$600(Lcom/android/settings/autostarts/LewaManageAutostarts;)Ljava/util/List;

    move-result-object v10

    invoke-interface {v10}, Ljava/util/List;->size()I

    move-result v10

    if-eqz v10, :cond_2

    iget-object v10, p0, Lcom/android/settings/autostarts/LewaManageAutostarts$AppAdapter;->this$0:Lcom/android/settings/autostarts/LewaManageAutostarts;

    invoke-virtual {v10}, Lcom/android/settings/autostarts/LewaManageAutostarts;->getResources()Landroid/content/res/Resources;

    move-result-object v10

    const v11, 0x7f0b0974

    invoke-virtual {v10, v11}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    const/4 v10, 0x1

    new-array v10, v10, [Ljava/lang/Object;

    const/4 v11, 0x0

    iget-object v12, p0, Lcom/android/settings/autostarts/LewaManageAutostarts$AppAdapter;->this$0:Lcom/android/settings/autostarts/LewaManageAutostarts;

    # getter for: Lcom/android/settings/autostarts/LewaManageAutostarts;->mOptimizedSize:I
    invoke-static {v12}, Lcom/android/settings/autostarts/LewaManageAutostarts;->access$100(Lcom/android/settings/autostarts/LewaManageAutostarts;)I

    move-result v12

    invoke-static {v12}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v12

    aput-object v12, v10, v11

    invoke-static {v1, v10}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v0, v10}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    const/4 v10, 0x0

    invoke-virtual {v2, v10}, Landroid/view/View;->setVisibility(I)V

    :goto_0
    invoke-virtual {p0, p1}, Lcom/android/settings/autostarts/LewaManageAutostarts$AppAdapter;->getItem(I)Lcom/android/settings/autostarts/AppItem;

    move-result-object v10

    iget-object v10, v10, Lcom/android/settings/autostarts/AppItem;->appName:Ljava/lang/String;

    invoke-virtual {v10}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v7, v10}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    invoke-virtual {p0, p1}, Lcom/android/settings/autostarts/LewaManageAutostarts$AppAdapter;->getItem(I)Lcom/android/settings/autostarts/AppItem;

    move-result-object v10

    iget-object v10, v10, Lcom/android/settings/autostarts/AppItem;->icon:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v6, v10}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    invoke-virtual {p0, p1}, Lcom/android/settings/autostarts/LewaManageAutostarts$AppAdapter;->getItem(I)Lcom/android/settings/autostarts/AppItem;

    move-result-object v10

    iget-boolean v4, v10, Lcom/android/settings/autostarts/AppItem;->checked:Z

    if-eqz v5, :cond_1

    invoke-virtual {v5, v4}, Landroid/widget/CheckBox;->setChecked(Z)V

    :cond_1
    invoke-virtual {p0, p1}, Lcom/android/settings/autostarts/LewaManageAutostarts$AppAdapter;->getItem(I)Lcom/android/settings/autostarts/AppItem;

    move-result-object v10

    iget-boolean v3, v10, Lcom/android/settings/autostarts/AppItem;->bootStartupStatus:Z

    if-eqz v3, :cond_3

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v11, p0, Lcom/android/settings/autostarts/LewaManageAutostarts$AppAdapter;->this$0:Lcom/android/settings/autostarts/LewaManageAutostarts;

    const v12, 0x7f0b0979

    invoke-virtual {v11, v12}, Lcom/android/settings/autostarts/LewaManageAutostarts;->getString(I)Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    const-string v11, " "

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    iget-object v11, p0, Lcom/android/settings/autostarts/LewaManageAutostarts$AppAdapter;->this$0:Lcom/android/settings/autostarts/LewaManageAutostarts;

    const v12, 0x7f0b097a

    invoke-virtual {v11, v12}, Lcom/android/settings/autostarts/LewaManageAutostarts;->getString(I)Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v8, v10}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    :goto_1
    return-object p2

    :cond_2
    const/16 v10, 0x8

    invoke-virtual {v2, v10}, Landroid/view/View;->setVisibility(I)V

    goto :goto_0

    :cond_3
    iget-object v10, p0, Lcom/android/settings/autostarts/LewaManageAutostarts$AppAdapter;->this$0:Lcom/android/settings/autostarts/LewaManageAutostarts;

    const v11, 0x7f0b097a

    invoke-virtual {v10, v11}, Lcom/android/settings/autostarts/LewaManageAutostarts;->getString(I)Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v8, v10}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_1
.end method
