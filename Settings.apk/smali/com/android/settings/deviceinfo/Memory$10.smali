.class Lcom/android/settings/deviceinfo/Memory$10;
.super Ljava/lang/Object;
.source "Memory.java"

# interfaces
.implements Landroid/content/DialogInterface$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/android/settings/deviceinfo/Memory;->onPreferenceChange(Landroid/preference/Preference;Ljava/lang/Object;)Z
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/settings/deviceinfo/Memory;

.field final synthetic val$preference:Landroid/preference/Preference;


# direct methods
.method constructor <init>(Lcom/android/settings/deviceinfo/Memory;Landroid/preference/Preference;)V
    .locals 0

    iput-object p1, p0, Lcom/android/settings/deviceinfo/Memory$10;->this$0:Lcom/android/settings/deviceinfo/Memory;

    iput-object p2, p0, Lcom/android/settings/deviceinfo/Memory$10;->val$preference:Landroid/preference/Preference;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/content/DialogInterface;I)V
    .locals 4
    .param p1    # Landroid/content/DialogInterface;
    .param p2    # I

    invoke-interface {p1}, Landroid/content/DialogInterface;->cancel()V

    iget-object v0, p0, Lcom/android/settings/deviceinfo/Memory$10;->val$preference:Landroid/preference/Preference;

    check-cast v0, Lcom/android/settings/deviceinfo/RadioButtonPreference;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/android/settings/deviceinfo/RadioButtonPreference;->setChecked(Z)Z

    iget-object v1, p0, Lcom/android/settings/deviceinfo/Memory$10;->this$0:Lcom/android/settings/deviceinfo/Memory;

    invoke-static {v1}, Lcom/android/settings/deviceinfo/Memory;->access$1400(Lcom/android/settings/deviceinfo/Memory;)Lcom/android/settings/deviceinfo/RadioButtonPreference;

    move-result-object v1

    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Lcom/android/settings/deviceinfo/RadioButtonPreference;->setChecked(Z)Z

    iget-object v1, p0, Lcom/android/settings/deviceinfo/Memory$10;->this$0:Lcom/android/settings/deviceinfo/Memory;

    invoke-static {v1}, Lcom/android/settings/deviceinfo/Memory;->access$1400(Lcom/android/settings/deviceinfo/Memory;)Lcom/android/settings/deviceinfo/RadioButtonPreference;

    move-result-object v1

    if-eqz v1, :cond_0

    const-string v1, "MemorySettings"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "mDeafultWritePathPref = "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/android/settings/deviceinfo/Memory$10;->this$0:Lcom/android/settings/deviceinfo/Memory;

    invoke-static {v3}, Lcom/android/settings/deviceinfo/Memory;->access$1400(Lcom/android/settings/deviceinfo/Memory;)Lcom/android/settings/deviceinfo/RadioButtonPreference;

    move-result-object v3

    invoke-virtual {v3}, Landroid/preference/Preference;->getKey()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    return-void
.end method
