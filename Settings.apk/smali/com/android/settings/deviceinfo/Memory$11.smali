.class Lcom/android/settings/deviceinfo/Memory$11;
.super Ljava/lang/Object;
.source "Memory.java"

# interfaces
.implements Landroid/content/DialogInterface$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/android/settings/deviceinfo/Memory;->onPreferenceChange(Landroid/preference/Preference;Ljava/lang/Object;)Z
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/settings/deviceinfo/Memory;

.field final synthetic val$preference:Landroid/preference/Preference;


# direct methods
.method constructor <init>(Lcom/android/settings/deviceinfo/Memory;Landroid/preference/Preference;)V
    .locals 0

    iput-object p1, p0, Lcom/android/settings/deviceinfo/Memory$11;->this$0:Lcom/android/settings/deviceinfo/Memory;

    iput-object p2, p0, Lcom/android/settings/deviceinfo/Memory$11;->val$preference:Landroid/preference/Preference;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/content/DialogInterface;I)V
    .locals 5
    .param p1    # Landroid/content/DialogInterface;
    .param p2    # I

    iget-object v2, p0, Lcom/android/settings/deviceinfo/Memory$11;->val$preference:Landroid/preference/Preference;

    invoke-virtual {v2}, Landroid/preference/Preference;->getKey()Ljava/lang/String;

    move-result-object v2

    if-nez v2, :cond_0

    const-string v2, "MemorySettings"

    const-string v3, "setDefaultPath error! path=null"

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    :goto_0
    return-void

    :cond_0
    :try_start_0
    const-string v2, "persist.sys.sd.defaultpath"

    iget-object v3, p0, Lcom/android/settings/deviceinfo/Memory$11;->val$preference:Landroid/preference/Preference;

    invoke-virtual {v3}, Landroid/preference/Preference;->getKey()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/os/SystemProperties;->set(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_1
    iget-object v3, p0, Lcom/android/settings/deviceinfo/Memory$11;->this$0:Lcom/android/settings/deviceinfo/Memory;

    iget-object v2, p0, Lcom/android/settings/deviceinfo/Memory$11;->val$preference:Landroid/preference/Preference;

    check-cast v2, Lcom/android/settings/deviceinfo/RadioButtonPreference;

    invoke-static {v3, v2}, Lcom/android/settings/deviceinfo/Memory;->access$1402(Lcom/android/settings/deviceinfo/Memory;Lcom/android/settings/deviceinfo/RadioButtonPreference;)Lcom/android/settings/deviceinfo/RadioButtonPreference;

    iget-object v2, p0, Lcom/android/settings/deviceinfo/Memory$11;->this$0:Lcom/android/settings/deviceinfo/Memory;

    invoke-static {v2}, Lcom/android/settings/deviceinfo/Memory;->access$1400(Lcom/android/settings/deviceinfo/Memory;)Lcom/android/settings/deviceinfo/RadioButtonPreference;

    move-result-object v2

    const/4 v3, 0x1

    invoke-virtual {v2, v3}, Lcom/android/settings/deviceinfo/RadioButtonPreference;->setChecked(Z)Z

    const-string v2, "MemorySettings"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "will reboot! mDeafultWritePathPref = "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lcom/android/settings/deviceinfo/Memory$11;->this$0:Lcom/android/settings/deviceinfo/Memory;

    invoke-static {v4}, Lcom/android/settings/deviceinfo/Memory;->access$1400(Lcom/android/settings/deviceinfo/Memory;)Lcom/android/settings/deviceinfo/RadioButtonPreference;

    move-result-object v4

    invoke-virtual {v4}, Landroid/preference/Preference;->getKey()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v2, p0, Lcom/android/settings/deviceinfo/Memory$11;->this$0:Lcom/android/settings/deviceinfo/Memory;

    const-string v3, "power"

    invoke-static {v2, v3}, Lcom/android/settings/deviceinfo/Memory;->access$1500(Lcom/android/settings/deviceinfo/Memory;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/os/PowerManager;

    const-string v2, "reboot"

    invoke-virtual {v1, v2}, Landroid/os/PowerManager;->reboot(Ljava/lang/String;)V

    sget-object v2, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v3, "execute cmd--> reboot\nreboot"

    invoke-virtual {v2, v3}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    goto :goto_0

    :catch_0
    move-exception v0

    const-string v2, "MemorySettings"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "IllegalArgumentException when set default path:"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1
.end method
