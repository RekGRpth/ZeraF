.class public Lcom/mediatek/settings/ext/DefaultStatusExt;
.super Ljava/lang/Object;
.source "DefaultStatusExt.java"

# interfaces
.implements Lcom/mediatek/settings/ext/IStatusExt;


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public addAction(Landroid/content/IntentFilter;Ljava/lang/String;)V
    .locals 0
    .param p1    # Landroid/content/IntentFilter;
    .param p2    # Ljava/lang/String;

    return-void
.end method

.method public updateOpNameFromRec(Landroid/preference/Preference;Ljava/lang/String;)V
    .locals 0
    .param p1    # Landroid/preference/Preference;
    .param p2    # Ljava/lang/String;

    return-void
.end method

.method public updateServiceState(Landroid/preference/Preference;Ljava/lang/String;)V
    .locals 0
    .param p1    # Landroid/preference/Preference;
    .param p2    # Ljava/lang/String;

    invoke-virtual {p1, p2}, Landroid/preference/Preference;->setSummary(Ljava/lang/CharSequence;)V

    return-void
.end method
