.class public Lcom/mediatek/voicecommand/business/VoiceServiceInternal;
.super Lcom/mediatek/voicecommand/business/VoiceCommandBusiness;
.source "VoiceServiceInternal.java"


# instance fields
.field private mJniAdapter:Lcom/mediatek/voicecommand/adapter/IVoiceAdapter;


# direct methods
.method public constructor <init>(Lcom/mediatek/voicecommand/mgr/IMessageDispatcher;Lcom/mediatek/voicecommand/mgr/ConfigurationManager;Landroid/os/Handler;Lcom/mediatek/voicecommand/adapter/IVoiceAdapter;)V
    .locals 0
    .param p1    # Lcom/mediatek/voicecommand/mgr/IMessageDispatcher;
    .param p2    # Lcom/mediatek/voicecommand/mgr/ConfigurationManager;
    .param p3    # Landroid/os/Handler;
    .param p4    # Lcom/mediatek/voicecommand/adapter/IVoiceAdapter;

    invoke-direct {p0, p1, p2, p3}, Lcom/mediatek/voicecommand/business/VoiceCommandBusiness;-><init>(Lcom/mediatek/voicecommand/mgr/IMessageDispatcher;Lcom/mediatek/voicecommand/mgr/ConfigurationManager;Landroid/os/Handler;)V

    iput-object p4, p0, Lcom/mediatek/voicecommand/business/VoiceServiceInternal;->mJniAdapter:Lcom/mediatek/voicecommand/adapter/IVoiceAdapter;

    return-void
.end method

.method private handleHeadsetPlugEvent(Lcom/mediatek/voicecommand/mgr/VoiceMessage;)V
    .locals 2
    .param p1    # Lcom/mediatek/voicecommand/mgr/VoiceMessage;

    const/4 v1, 0x1

    iget v0, p1, Lcom/mediatek/voicecommand/mgr/VoiceMessage;->mSubAction:I

    if-ne v0, v1, :cond_0

    iget-object v0, p0, Lcom/mediatek/voicecommand/business/VoiceServiceInternal;->mJniAdapter:Lcom/mediatek/voicecommand/adapter/IVoiceAdapter;

    invoke-interface {v0, v1}, Lcom/mediatek/voicecommand/adapter/IVoiceAdapter;->setCurHeadsetMode(Z)V

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/mediatek/voicecommand/business/VoiceServiceInternal;->mJniAdapter:Lcom/mediatek/voicecommand/adapter/IVoiceAdapter;

    const/4 v1, 0x0

    invoke-interface {v0, v1}, Lcom/mediatek/voicecommand/adapter/IVoiceAdapter;->setCurHeadsetMode(Z)V

    goto :goto_0
.end method

.method private handleProcessExit(Lcom/mediatek/voicecommand/mgr/VoiceMessage;)V
    .locals 3
    .param p1    # Lcom/mediatek/voicecommand/mgr/VoiceMessage;

    iget-object v0, p0, Lcom/mediatek/voicecommand/business/VoiceServiceInternal;->mJniAdapter:Lcom/mediatek/voicecommand/adapter/IVoiceAdapter;

    iget-object v1, p1, Lcom/mediatek/voicecommand/mgr/VoiceMessage;->mPkgName:Ljava/lang/String;

    iget v2, p1, Lcom/mediatek/voicecommand/mgr/VoiceMessage;->pid:I

    invoke-interface {v0, v1, v2}, Lcom/mediatek/voicecommand/adapter/IVoiceAdapter;->stopCurMode(Ljava/lang/String;I)V

    return-void
.end method


# virtual methods
.method public handleAsyncVoiceMessage(Lcom/mediatek/voicecommand/mgr/VoiceMessage;)I
    .locals 3
    .param p1    # Lcom/mediatek/voicecommand/mgr/VoiceMessage;

    const/4 v0, 0x0

    iget v1, p1, Lcom/mediatek/voicecommand/mgr/VoiceMessage;->mMainAction:I

    packed-switch v1, :pswitch_data_0

    :cond_0
    :goto_0
    return v0

    :pswitch_0
    iget v1, p1, Lcom/mediatek/voicecommand/mgr/VoiceMessage;->mSubAction:I

    const/4 v2, 0x1

    if-ne v1, v2, :cond_0

    invoke-direct {p0, p1}, Lcom/mediatek/voicecommand/business/VoiceServiceInternal;->handleProcessExit(Lcom/mediatek/voicecommand/mgr/VoiceMessage;)V

    goto :goto_0

    :pswitch_1
    invoke-direct {p0, p1}, Lcom/mediatek/voicecommand/business/VoiceServiceInternal;->handleHeadsetPlugEvent(Lcom/mediatek/voicecommand/mgr/VoiceMessage;)V

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x2710
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public handleDataRelease()V
    .locals 1

    iget-object v0, p0, Lcom/mediatek/voicecommand/business/VoiceServiceInternal;->mJniAdapter:Lcom/mediatek/voicecommand/adapter/IVoiceAdapter;

    invoke-interface {v0}, Lcom/mediatek/voicecommand/adapter/IVoiceAdapter;->release()V

    return-void
.end method

.method public handleSyncVoiceMessage(Lcom/mediatek/voicecommand/mgr/VoiceMessage;)I
    .locals 3
    .param p1    # Lcom/mediatek/voicecommand/mgr/VoiceMessage;

    const/16 v2, 0x2711

    const/4 v0, 0x0

    iget v1, p1, Lcom/mediatek/voicecommand/mgr/VoiceMessage;->mMainAction:I

    packed-switch v1, :pswitch_data_0

    :cond_0
    :goto_0
    return v0

    :pswitch_0
    iget v1, p1, Lcom/mediatek/voicecommand/mgr/VoiceMessage;->mSubAction:I

    const/4 v2, 0x1

    if-ne v1, v2, :cond_1

    invoke-virtual {p0, p1}, Lcom/mediatek/voicecommand/business/VoiceServiceInternal;->sendMessageToHandler(Lcom/mediatek/voicecommand/mgr/VoiceMessage;)I

    goto :goto_0

    :cond_1
    iget v1, p1, Lcom/mediatek/voicecommand/mgr/VoiceMessage;->mSubAction:I

    const/4 v2, 0x2

    if-ne v1, v2, :cond_0

    invoke-virtual {p0}, Lcom/mediatek/voicecommand/business/VoiceServiceInternal;->handleDataRelease()V

    goto :goto_0

    :pswitch_1
    iget-object v1, p0, Lcom/mediatek/voicecommand/business/VoiceServiceInternal;->mHandler:Landroid/os/Handler;

    invoke-virtual {v1, v2}, Landroid/os/Handler;->hasMessages(I)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/mediatek/voicecommand/business/VoiceServiceInternal;->mHandler:Landroid/os/Handler;

    invoke-virtual {v1, v2}, Landroid/os/Handler;->removeMessages(I)V

    :cond_2
    invoke-virtual {p0, p1}, Lcom/mediatek/voicecommand/business/VoiceServiceInternal;->sendMessageToHandler(Lcom/mediatek/voicecommand/mgr/VoiceMessage;)I

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x2710
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method
