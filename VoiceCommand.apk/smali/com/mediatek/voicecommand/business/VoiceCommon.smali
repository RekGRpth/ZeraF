.class public Lcom/mediatek/voicecommand/business/VoiceCommon;
.super Lcom/mediatek/voicecommand/business/VoiceCommandBusiness;
.source "VoiceCommon.java"


# direct methods
.method public constructor <init>(Lcom/mediatek/voicecommand/mgr/IMessageDispatcher;Lcom/mediatek/voicecommand/mgr/ConfigurationManager;Landroid/os/Handler;)V
    .locals 0
    .param p1    # Lcom/mediatek/voicecommand/mgr/IMessageDispatcher;
    .param p2    # Lcom/mediatek/voicecommand/mgr/ConfigurationManager;
    .param p3    # Landroid/os/Handler;

    invoke-direct {p0, p1, p2, p3}, Lcom/mediatek/voicecommand/business/VoiceCommandBusiness;-><init>(Lcom/mediatek/voicecommand/mgr/IMessageDispatcher;Lcom/mediatek/voicecommand/mgr/ConfigurationManager;Landroid/os/Handler;)V

    return-void
.end method

.method private sendProcessKeyWord(Lcom/mediatek/voicecommand/mgr/VoiceMessage;)I
    .locals 4
    .param p1    # Lcom/mediatek/voicecommand/mgr/VoiceMessage;

    const/4 v0, 0x0

    iget-object v2, p0, Lcom/mediatek/voicecommand/business/VoiceCommon;->mCfgMgr:Lcom/mediatek/voicecommand/mgr/ConfigurationManager;

    iget-object v3, p1, Lcom/mediatek/voicecommand/mgr/VoiceMessage;->mPkgName:Ljava/lang/String;

    invoke-virtual {v2, v3}, Lcom/mediatek/voicecommand/mgr/ConfigurationManager;->getKeyWord(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x1

    const/4 v3, 0x0

    invoke-static {v2, v1, v3}, Lcom/mediatek/voicecommand/data/DataPackage;->packageResultInfo(I[Ljava/lang/String;[I)Landroid/os/Bundle;

    move-result-object v2

    iput-object v2, p1, Lcom/mediatek/voicecommand/mgr/VoiceMessage;->mExtraData:Landroid/os/Bundle;

    iget-object v2, p0, Lcom/mediatek/voicecommand/business/VoiceCommon;->mDispatcher:Lcom/mediatek/voicecommand/mgr/IMessageDispatcher;

    invoke-interface {v2, p1}, Lcom/mediatek/voicecommand/mgr/IMessageDispatcher;->dispatchMessageUp(Lcom/mediatek/voicecommand/mgr/VoiceMessage;)I

    return v0
.end method

.method private sendProcessState(Lcom/mediatek/voicecommand/mgr/VoiceMessage;)I
    .locals 4
    .param p1    # Lcom/mediatek/voicecommand/mgr/VoiceMessage;

    const/4 v0, 0x0

    iget-object v2, p0, Lcom/mediatek/voicecommand/business/VoiceCommon;->mCfgMgr:Lcom/mediatek/voicecommand/mgr/ConfigurationManager;

    iget-object v3, p1, Lcom/mediatek/voicecommand/mgr/VoiceMessage;->mPkgName:Ljava/lang/String;

    invoke-virtual {v2, v3}, Lcom/mediatek/voicecommand/mgr/ConfigurationManager;->isProcessEnable(Ljava/lang/String;)Z

    move-result v1

    const/4 v2, 0x1

    const/4 v3, 0x0

    invoke-static {v2, v1, v3}, Lcom/mediatek/voicecommand/data/DataPackage;->packageResultInfo(IZLjava/lang/String;)Landroid/os/Bundle;

    move-result-object v2

    iput-object v2, p1, Lcom/mediatek/voicecommand/mgr/VoiceMessage;->mExtraData:Landroid/os/Bundle;

    iget-object v2, p0, Lcom/mediatek/voicecommand/business/VoiceCommon;->mDispatcher:Lcom/mediatek/voicecommand/mgr/IMessageDispatcher;

    invoke-interface {v2, p1}, Lcom/mediatek/voicecommand/mgr/IMessageDispatcher;->dispatchMessageUp(Lcom/mediatek/voicecommand/mgr/VoiceMessage;)I

    return v0
.end method


# virtual methods
.method public handleAsyncVoiceMessage(Lcom/mediatek/voicecommand/mgr/VoiceMessage;)I
    .locals 1
    .param p1    # Lcom/mediatek/voicecommand/mgr/VoiceMessage;

    const/4 v0, 0x0

    return v0
.end method

.method public handleSyncVoiceMessage(Lcom/mediatek/voicecommand/mgr/VoiceMessage;)I
    .locals 2
    .param p1    # Lcom/mediatek/voicecommand/mgr/VoiceMessage;

    const/4 v0, 0x0

    iget v1, p1, Lcom/mediatek/voicecommand/mgr/VoiceMessage;->mSubAction:I

    packed-switch v1, :pswitch_data_0

    :goto_0
    return v0

    :pswitch_0
    invoke-direct {p0, p1}, Lcom/mediatek/voicecommand/business/VoiceCommon;->sendProcessKeyWord(Lcom/mediatek/voicecommand/mgr/VoiceMessage;)I

    move-result v0

    goto :goto_0

    :pswitch_1
    invoke-direct {p0, p1}, Lcom/mediatek/voicecommand/business/VoiceCommon;->sendProcessState(Lcom/mediatek/voicecommand/mgr/VoiceMessage;)I

    move-result v0

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method
