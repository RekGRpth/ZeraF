.class public Lcom/mediatek/voicecommand/mgr/ServiceDataManager;
.super Lcom/mediatek/voicecommand/mgr/VoiceDataManager;
.source "ServiceDataManager.java"

# interfaces
.implements Lcom/mediatek/voicecommand/mgr/IMessageDispatcher;


# instance fields
.field private handler:Landroid/os/Handler;

.field private mDownDispatcher:Lcom/mediatek/voicecommand/mgr/IMessageDispatcher;

.field private mUpDispatcher:Lcom/mediatek/voicecommand/mgr/IMessageDispatcher;

.field private voiceCommon:Lcom/mediatek/voicecommand/business/VoiceCommandBusiness;

.field private voiceSetting:Lcom/mediatek/voicecommand/business/VoiceCommandBusiness;


# direct methods
.method public constructor <init>(Lcom/mediatek/voicecommand/service/VoiceCommandManagerStub;)V
    .locals 3
    .param p1    # Lcom/mediatek/voicecommand/service/VoiceCommandManagerStub;

    invoke-direct {p0, p1}, Lcom/mediatek/voicecommand/mgr/VoiceDataManager;-><init>(Lcom/mediatek/voicecommand/service/VoiceCommandManagerStub;)V

    new-instance v0, Lcom/mediatek/voicecommand/mgr/ServiceDataManager$1;

    invoke-direct {v0, p0}, Lcom/mediatek/voicecommand/mgr/ServiceDataManager$1;-><init>(Lcom/mediatek/voicecommand/mgr/ServiceDataManager;)V

    iput-object v0, p0, Lcom/mediatek/voicecommand/mgr/ServiceDataManager;->handler:Landroid/os/Handler;

    new-instance v0, Lcom/mediatek/voicecommand/business/VoiceSetting;

    iget-object v1, p1, Lcom/mediatek/voicecommand/service/VoiceCommandManagerStub;->mConfigManager:Lcom/mediatek/voicecommand/mgr/ConfigurationManager;

    iget-object v2, p0, Lcom/mediatek/voicecommand/mgr/ServiceDataManager;->handler:Landroid/os/Handler;

    invoke-direct {v0, p0, v1, v2}, Lcom/mediatek/voicecommand/business/VoiceSetting;-><init>(Lcom/mediatek/voicecommand/mgr/IMessageDispatcher;Lcom/mediatek/voicecommand/mgr/ConfigurationManager;Landroid/os/Handler;)V

    iput-object v0, p0, Lcom/mediatek/voicecommand/mgr/ServiceDataManager;->voiceSetting:Lcom/mediatek/voicecommand/business/VoiceCommandBusiness;

    new-instance v0, Lcom/mediatek/voicecommand/business/VoiceCommon;

    iget-object v1, p1, Lcom/mediatek/voicecommand/service/VoiceCommandManagerStub;->mConfigManager:Lcom/mediatek/voicecommand/mgr/ConfigurationManager;

    iget-object v2, p0, Lcom/mediatek/voicecommand/mgr/ServiceDataManager;->handler:Landroid/os/Handler;

    invoke-direct {v0, p0, v1, v2}, Lcom/mediatek/voicecommand/business/VoiceCommon;-><init>(Lcom/mediatek/voicecommand/mgr/IMessageDispatcher;Lcom/mediatek/voicecommand/mgr/ConfigurationManager;Landroid/os/Handler;)V

    iput-object v0, p0, Lcom/mediatek/voicecommand/mgr/ServiceDataManager;->voiceCommon:Lcom/mediatek/voicecommand/business/VoiceCommandBusiness;

    return-void
.end method

.method static synthetic access$000(Lcom/mediatek/voicecommand/mgr/ServiceDataManager;)Lcom/mediatek/voicecommand/business/VoiceCommandBusiness;
    .locals 1
    .param p0    # Lcom/mediatek/voicecommand/mgr/ServiceDataManager;

    iget-object v0, p0, Lcom/mediatek/voicecommand/mgr/ServiceDataManager;->voiceSetting:Lcom/mediatek/voicecommand/business/VoiceCommandBusiness;

    return-object v0
.end method


# virtual methods
.method public dispatchMessageDown(Lcom/mediatek/voicecommand/mgr/VoiceMessage;)I
    .locals 2
    .param p1    # Lcom/mediatek/voicecommand/mgr/VoiceMessage;

    const/4 v0, 0x0

    iget v1, p1, Lcom/mediatek/voicecommand/mgr/VoiceMessage;->mMainAction:I

    sparse-switch v1, :sswitch_data_0

    iget-object v1, p0, Lcom/mediatek/voicecommand/mgr/ServiceDataManager;->mDownDispatcher:Lcom/mediatek/voicecommand/mgr/IMessageDispatcher;

    invoke-interface {v1, p1}, Lcom/mediatek/voicecommand/mgr/IMessageDispatcher;->dispatchMessageDown(Lcom/mediatek/voicecommand/mgr/VoiceMessage;)I

    move-result v0

    :goto_0
    return v0

    :sswitch_0
    iget-object v1, p0, Lcom/mediatek/voicecommand/mgr/ServiceDataManager;->voiceSetting:Lcom/mediatek/voicecommand/business/VoiceCommandBusiness;

    invoke-virtual {v1, p1}, Lcom/mediatek/voicecommand/business/VoiceCommandBusiness;->handleSyncVoiceMessage(Lcom/mediatek/voicecommand/mgr/VoiceMessage;)I

    move-result v0

    goto :goto_0

    :sswitch_1
    iget-object v1, p0, Lcom/mediatek/voicecommand/mgr/ServiceDataManager;->voiceCommon:Lcom/mediatek/voicecommand/business/VoiceCommandBusiness;

    invoke-virtual {v1, p1}, Lcom/mediatek/voicecommand/business/VoiceCommandBusiness;->handleSyncVoiceMessage(Lcom/mediatek/voicecommand/mgr/VoiceMessage;)I

    move-result v0

    goto :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x1 -> :sswitch_1
        0x5 -> :sswitch_0
    .end sparse-switch
.end method

.method public dispatchMessageUp(Lcom/mediatek/voicecommand/mgr/VoiceMessage;)I
    .locals 2
    .param p1    # Lcom/mediatek/voicecommand/mgr/VoiceMessage;

    const/4 v0, 0x0

    if-nez v0, :cond_0

    iget-object v1, p0, Lcom/mediatek/voicecommand/mgr/ServiceDataManager;->mUpDispatcher:Lcom/mediatek/voicecommand/mgr/IMessageDispatcher;

    invoke-interface {v1, p1}, Lcom/mediatek/voicecommand/mgr/IMessageDispatcher;->dispatchMessageUp(Lcom/mediatek/voicecommand/mgr/VoiceMessage;)I

    move-result v0

    :cond_0
    return v0
.end method

.method public setDownDispatcher(Lcom/mediatek/voicecommand/mgr/IMessageDispatcher;)V
    .locals 0
    .param p1    # Lcom/mediatek/voicecommand/mgr/IMessageDispatcher;

    iput-object p1, p0, Lcom/mediatek/voicecommand/mgr/ServiceDataManager;->mDownDispatcher:Lcom/mediatek/voicecommand/mgr/IMessageDispatcher;

    return-void
.end method

.method public setUpDispatcher(Lcom/mediatek/voicecommand/mgr/IMessageDispatcher;)V
    .locals 0
    .param p1    # Lcom/mediatek/voicecommand/mgr/IMessageDispatcher;

    iput-object p1, p0, Lcom/mediatek/voicecommand/mgr/ServiceDataManager;->mUpDispatcher:Lcom/mediatek/voicecommand/mgr/IMessageDispatcher;

    return-void
.end method
