.class public final Lcom/mediatek/voicecommand/service/VoiceCommandManagerStub;
.super Lcom/mediatek/common/voicecommand/IVoiceCommandManagerService$Stub;
.source "VoiceCommandManagerStub.java"

# interfaces
.implements Lcom/mediatek/common/voicecommand/IVoiceCommandManagerService;


# static fields
.field static final DBG:Z = false

.field public static final TAG:Ljava/lang/String; = "VCmdMgrService"


# instance fields
.field mAppDataManager:Lcom/mediatek/voicecommand/mgr/AppDataManager;

.field public mConfigManager:Lcom/mediatek/voicecommand/mgr/ConfigurationManager;

.field public final mContext:Landroid/content/Context;

.field mNativeDataManager:Lcom/mediatek/voicecommand/mgr/NativeDataManager;

.field mServiceDataManager:Lcom/mediatek/voicecommand/mgr/ServiceDataManager;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 3
    .param p1    # Landroid/content/Context;

    invoke-direct {p0}, Lcom/mediatek/common/voicecommand/IVoiceCommandManagerService$Stub;-><init>()V

    const-string v0, "VCmdMgrService"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "VoiceCommandManagerService Constructor start !"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p1}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    iput-object p1, p0, Lcom/mediatek/voicecommand/service/VoiceCommandManagerStub;->mContext:Landroid/content/Context;

    invoke-static {p1}, Lcom/mediatek/voicecommand/mgr/ConfigurationManager;->getInstance(Landroid/content/Context;)Lcom/mediatek/voicecommand/mgr/ConfigurationManager;

    move-result-object v0

    iput-object v0, p0, Lcom/mediatek/voicecommand/service/VoiceCommandManagerStub;->mConfigManager:Lcom/mediatek/voicecommand/mgr/ConfigurationManager;

    new-instance v0, Lcom/mediatek/voicecommand/mgr/AppDataManager;

    invoke-direct {v0, p0}, Lcom/mediatek/voicecommand/mgr/AppDataManager;-><init>(Lcom/mediatek/voicecommand/service/VoiceCommandManagerStub;)V

    iput-object v0, p0, Lcom/mediatek/voicecommand/service/VoiceCommandManagerStub;->mAppDataManager:Lcom/mediatek/voicecommand/mgr/AppDataManager;

    new-instance v0, Lcom/mediatek/voicecommand/mgr/ServiceDataManager;

    invoke-direct {v0, p0}, Lcom/mediatek/voicecommand/mgr/ServiceDataManager;-><init>(Lcom/mediatek/voicecommand/service/VoiceCommandManagerStub;)V

    iput-object v0, p0, Lcom/mediatek/voicecommand/service/VoiceCommandManagerStub;->mServiceDataManager:Lcom/mediatek/voicecommand/mgr/ServiceDataManager;

    new-instance v0, Lcom/mediatek/voicecommand/mgr/NativeDataManager;

    invoke-direct {v0, p0}, Lcom/mediatek/voicecommand/mgr/NativeDataManager;-><init>(Lcom/mediatek/voicecommand/service/VoiceCommandManagerStub;)V

    iput-object v0, p0, Lcom/mediatek/voicecommand/service/VoiceCommandManagerStub;->mNativeDataManager:Lcom/mediatek/voicecommand/mgr/NativeDataManager;

    iget-object v0, p0, Lcom/mediatek/voicecommand/service/VoiceCommandManagerStub;->mAppDataManager:Lcom/mediatek/voicecommand/mgr/AppDataManager;

    iget-object v1, p0, Lcom/mediatek/voicecommand/service/VoiceCommandManagerStub;->mServiceDataManager:Lcom/mediatek/voicecommand/mgr/ServiceDataManager;

    invoke-virtual {v0, v1}, Lcom/mediatek/voicecommand/mgr/AppDataManager;->setDownDispatcher(Lcom/mediatek/voicecommand/mgr/IMessageDispatcher;)V

    iget-object v0, p0, Lcom/mediatek/voicecommand/service/VoiceCommandManagerStub;->mServiceDataManager:Lcom/mediatek/voicecommand/mgr/ServiceDataManager;

    iget-object v1, p0, Lcom/mediatek/voicecommand/service/VoiceCommandManagerStub;->mAppDataManager:Lcom/mediatek/voicecommand/mgr/AppDataManager;

    invoke-virtual {v0, v1}, Lcom/mediatek/voicecommand/mgr/ServiceDataManager;->setUpDispatcher(Lcom/mediatek/voicecommand/mgr/IMessageDispatcher;)V

    iget-object v0, p0, Lcom/mediatek/voicecommand/service/VoiceCommandManagerStub;->mServiceDataManager:Lcom/mediatek/voicecommand/mgr/ServiceDataManager;

    iget-object v1, p0, Lcom/mediatek/voicecommand/service/VoiceCommandManagerStub;->mNativeDataManager:Lcom/mediatek/voicecommand/mgr/NativeDataManager;

    invoke-virtual {v0, v1}, Lcom/mediatek/voicecommand/mgr/ServiceDataManager;->setDownDispatcher(Lcom/mediatek/voicecommand/mgr/IMessageDispatcher;)V

    iget-object v0, p0, Lcom/mediatek/voicecommand/service/VoiceCommandManagerStub;->mNativeDataManager:Lcom/mediatek/voicecommand/mgr/NativeDataManager;

    iget-object v1, p0, Lcom/mediatek/voicecommand/service/VoiceCommandManagerStub;->mServiceDataManager:Lcom/mediatek/voicecommand/mgr/ServiceDataManager;

    invoke-virtual {v0, v1}, Lcom/mediatek/voicecommand/mgr/NativeDataManager;->setUpDispatcher(Lcom/mediatek/voicecommand/mgr/IMessageDispatcher;)V

    const-string v0, "VCmdMgrService"

    const-string v1, "VoiceCommandManagerService Constructor End !"

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    return-void
.end method


# virtual methods
.method public asBinder()Landroid/os/IBinder;
    .locals 0

    return-object p0
.end method

.method public registerListener(Ljava/lang/String;Lcom/mediatek/common/voicecommand/IVoiceCommandListener;)I
    .locals 3
    .param p1    # Ljava/lang/String;
    .param p2    # Lcom/mediatek/common/voicecommand/IVoiceCommandListener;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    const-string v0, "VCmdMgrService"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " register listener "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v0, p0, Lcom/mediatek/voicecommand/service/VoiceCommandManagerStub;->mAppDataManager:Lcom/mediatek/voicecommand/mgr/AppDataManager;

    invoke-static {}, Landroid/os/Binder;->getCallingUid()I

    move-result v1

    invoke-static {}, Landroid/os/Binder;->getCallingPid()I

    move-result v2

    invoke-virtual {v0, p1, v1, v2, p2}, Lcom/mediatek/voicecommand/mgr/AppDataManager;->registerListener(Ljava/lang/String;IILcom/mediatek/common/voicecommand/IVoiceCommandListener;)I

    move-result v0

    return v0
.end method

.method public release()V
    .locals 2

    new-instance v0, Lcom/mediatek/voicecommand/mgr/VoiceMessage;

    invoke-direct {v0}, Lcom/mediatek/voicecommand/mgr/VoiceMessage;-><init>()V

    const/16 v1, 0x2710

    iput v1, v0, Lcom/mediatek/voicecommand/mgr/VoiceMessage;->mMainAction:I

    const/4 v1, 0x2

    iput v1, v0, Lcom/mediatek/voicecommand/mgr/VoiceMessage;->mSubAction:I

    iget-object v1, p0, Lcom/mediatek/voicecommand/service/VoiceCommandManagerStub;->mNativeDataManager:Lcom/mediatek/voicecommand/mgr/NativeDataManager;

    invoke-virtual {v1, v0}, Lcom/mediatek/voicecommand/mgr/NativeDataManager;->dispatchMessageUp(Lcom/mediatek/voicecommand/mgr/VoiceMessage;)I

    return-void
.end method

.method public sendCommand(Ljava/lang/String;IILandroid/os/Bundle;)I
    .locals 4
    .param p1    # Ljava/lang/String;
    .param p2    # I
    .param p3    # I
    .param p4    # Landroid/os/Bundle;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    const-string v1, "VCmdMgrService"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " sendCommand "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "mainAction="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " subAction="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    new-instance v0, Lcom/mediatek/voicecommand/mgr/VoiceMessage;

    invoke-direct {v0}, Lcom/mediatek/voicecommand/mgr/VoiceMessage;-><init>()V

    iput-object p1, v0, Lcom/mediatek/voicecommand/mgr/VoiceMessage;->mPkgName:Ljava/lang/String;

    iput p2, v0, Lcom/mediatek/voicecommand/mgr/VoiceMessage;->mMainAction:I

    iput p3, v0, Lcom/mediatek/voicecommand/mgr/VoiceMessage;->mSubAction:I

    iput-object p4, v0, Lcom/mediatek/voicecommand/mgr/VoiceMessage;->mExtraData:Landroid/os/Bundle;

    invoke-static {}, Landroid/os/Binder;->getCallingPid()I

    move-result v1

    iput v1, v0, Lcom/mediatek/voicecommand/mgr/VoiceMessage;->pid:I

    invoke-static {}, Landroid/os/Binder;->getCallingUid()I

    move-result v1

    iput v1, v0, Lcom/mediatek/voicecommand/mgr/VoiceMessage;->uid:I

    iget-object v1, p0, Lcom/mediatek/voicecommand/service/VoiceCommandManagerStub;->mAppDataManager:Lcom/mediatek/voicecommand/mgr/AppDataManager;

    invoke-virtual {v1, v0}, Lcom/mediatek/voicecommand/mgr/AppDataManager;->dispatchMessageDown(Lcom/mediatek/voicecommand/mgr/VoiceMessage;)I

    move-result v1

    return v1
.end method

.method public unregisterListener(Ljava/lang/String;Lcom/mediatek/common/voicecommand/IVoiceCommandListener;)I
    .locals 6
    .param p1    # Ljava/lang/String;
    .param p2    # Lcom/mediatek/common/voicecommand/IVoiceCommandListener;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    const-string v0, "VCmdMgrService"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " unregister listener "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v0, p0, Lcom/mediatek/voicecommand/service/VoiceCommandManagerStub;->mAppDataManager:Lcom/mediatek/voicecommand/mgr/AppDataManager;

    invoke-static {}, Landroid/os/Binder;->getCallingUid()I

    move-result v2

    invoke-static {}, Landroid/os/Binder;->getCallingPid()I

    move-result v3

    const/4 v5, 0x0

    move-object v1, p1

    move-object v4, p2

    invoke-virtual/range {v0 .. v5}, Lcom/mediatek/voicecommand/mgr/AppDataManager;->unRegisterListener(Ljava/lang/String;IILcom/mediatek/common/voicecommand/IVoiceCommandListener;Z)I

    move-result v0

    return v0
.end method
