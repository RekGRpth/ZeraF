.class public Lcom/mediatek/voicecommand/service/VoiceCommandApp;
.super Landroid/app/Application;
.source "VoiceCommandApp.java"


# instance fields
.field private mVoiceServiceIntent:Landroid/content/Intent;


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Landroid/app/Application;-><init>()V

    return-void
.end method


# virtual methods
.method public onCreate()V
    .locals 2

    invoke-super {p0}, Landroid/app/Application;->onCreate()V

    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    iput-object v0, p0, Lcom/mediatek/voicecommand/service/VoiceCommandApp;->mVoiceServiceIntent:Landroid/content/Intent;

    iget-object v0, p0, Lcom/mediatek/voicecommand/service/VoiceCommandApp;->mVoiceServiceIntent:Landroid/content/Intent;

    const-string v1, "android.mediatek.voicecommand"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    iget-object v0, p0, Lcom/mediatek/voicecommand/service/VoiceCommandApp;->mVoiceServiceIntent:Landroid/content/Intent;

    const-string v1, "android.mediatek.NativeService"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->addCategory(Ljava/lang/String;)Landroid/content/Intent;

    iget-object v0, p0, Lcom/mediatek/voicecommand/service/VoiceCommandApp;->mVoiceServiceIntent:Landroid/content/Intent;

    invoke-virtual {p0, v0}, Lcom/mediatek/voicecommand/service/VoiceCommandApp;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;

    return-void
.end method
