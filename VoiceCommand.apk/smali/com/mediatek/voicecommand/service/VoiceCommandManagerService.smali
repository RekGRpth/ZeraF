.class public Lcom/mediatek/voicecommand/service/VoiceCommandManagerService;
.super Landroid/app/Service;
.source "VoiceCommandManagerService.java"


# instance fields
.field private mServiceStub:Lcom/mediatek/voicecommand/service/VoiceCommandManagerStub;


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Landroid/app/Service;-><init>()V

    return-void
.end method


# virtual methods
.method public onBind(Landroid/content/Intent;)Landroid/os/IBinder;
    .locals 2
    .param p1    # Landroid/content/Intent;

    const-string v0, "VCmdMgrService"

    const-string v1, "VoiceCommandNativeService onBind"

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v0, p0, Lcom/mediatek/voicecommand/service/VoiceCommandManagerService;->mServiceStub:Lcom/mediatek/voicecommand/service/VoiceCommandManagerStub;

    return-object v0
.end method

.method public onCreate()V
    .locals 2

    const-string v0, "VCmdMgrService"

    const-string v1, "VoiceCommandNativeService onCreate"

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    new-instance v0, Lcom/mediatek/voicecommand/service/VoiceCommandManagerStub;

    invoke-direct {v0, p0}, Lcom/mediatek/voicecommand/service/VoiceCommandManagerStub;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/mediatek/voicecommand/service/VoiceCommandManagerService;->mServiceStub:Lcom/mediatek/voicecommand/service/VoiceCommandManagerStub;

    return-void
.end method

.method public onDestroy()V
    .locals 2

    const-string v0, "VCmdMgrService"

    const-string v1, "VoiceCommandNativeService onDestroy"

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v0, p0, Lcom/mediatek/voicecommand/service/VoiceCommandManagerService;->mServiceStub:Lcom/mediatek/voicecommand/service/VoiceCommandManagerStub;

    invoke-virtual {v0}, Lcom/mediatek/voicecommand/service/VoiceCommandManagerStub;->release()V

    return-void
.end method
