.class Lcom/zte/smssecurity/SMSSecurityService$2;
.super Landroid/content/BroadcastReceiver;
.source "SMSSecurityService.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/zte/smssecurity/SMSSecurityService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/zte/smssecurity/SMSSecurityService;


# direct methods
.method constructor <init>(Lcom/zte/smssecurity/SMSSecurityService;)V
    .locals 0

    iput-object p1, p0, Lcom/zte/smssecurity/SMSSecurityService$2;->this$0:Lcom/zte/smssecurity/SMSSecurityService;

    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 13
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/content/Intent;

    const/4 v12, 0x0

    const/4 v4, 0x0

    const/4 v11, 0x1

    invoke-virtual {p0}, Landroid/content/BroadcastReceiver;->getResultCode()I

    move-result v1

    packed-switch v1, :pswitch_data_0

    const-string v1, "SMSSecurityService"

    const-string v2, "SMS Send failed"

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :goto_0
    return-void

    :pswitch_0
    invoke-static {v11}, Lcom/zte/smssecurity/SMSSecurityService;->access$902(Z)Z

    const-string v1, "SMSSecurityService"

    const-string v2, "SMS Send Succeed"

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v1, p0, Lcom/zte/smssecurity/SMSSecurityService$2;->this$0:Lcom/zte/smssecurity/SMSSecurityService;

    invoke-static {v1}, Lcom/zte/smssecurity/SMSSecurityService;->access$1100(Lcom/zte/smssecurity/SMSSecurityService;)Lcom/zte/smssecurity/SMSSecurityDatabaseHelper;

    move-result-object v1

    invoke-virtual {v1}, Landroid/database/sqlite/SQLiteOpenHelper;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    const-string v1, "SMSSecurityDatabase"

    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/String;

    const-string v3, "TYPE"

    aput-object v3, v2, v12

    const-string v3, "VALUE"

    aput-object v3, v2, v11

    const-string v3, "TYPE=\"SHOWREGISTERED\""

    move-object v5, v4

    move-object v6, v4

    move-object v7, v4

    invoke-virtual/range {v0 .. v7}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v8

    const/4 v10, 0x0

    :cond_0
    :goto_1
    invoke-interface {v8}, Landroid/database/Cursor;->moveToNext()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-interface {v8, v12}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v1

    const-string v2, "SHOWREGISTERED"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {v8, v11}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v10

    goto :goto_1

    :cond_1
    if-ne v10, v11, :cond_2

    const-string v1, "SMSSecurityService"

    const-string v2, "showRegistered == 1"

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    new-instance v9, Landroid/content/Intent;

    const-string v1, "android.intent.action.showSmsSecurity"

    invoke-direct {v9, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    const-string v1, "showRegistered"

    const-string v2, "yes"

    invoke-virtual {v9, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    iget-object v1, p0, Lcom/zte/smssecurity/SMSSecurityService$2;->this$0:Lcom/zte/smssecurity/SMSSecurityService;

    invoke-virtual {v1, v9}, Landroid/content/ContextWrapper;->sendBroadcast(Landroid/content/Intent;)V

    :cond_2
    invoke-interface {v8}, Landroid/database/Cursor;->close()V

    goto :goto_0

    :pswitch_data_0
    .packed-switch -0x1
        :pswitch_0
    .end packed-switch
.end method
