.class Lcom/zte/smssecurity/SMSSecurityService$DetectToShowDialog;
.super Ljava/lang/Thread;
.source "SMSSecurityService.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/zte/smssecurity/SMSSecurityService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "DetectToShowDialog"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/zte/smssecurity/SMSSecurityService;


# direct methods
.method private constructor <init>(Lcom/zte/smssecurity/SMSSecurityService;)V
    .locals 0

    iput-object p1, p0, Lcom/zte/smssecurity/SMSSecurityService$DetectToShowDialog;->this$0:Lcom/zte/smssecurity/SMSSecurityService;

    invoke-direct {p0}, Ljava/lang/Thread;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/zte/smssecurity/SMSSecurityService;Lcom/zte/smssecurity/SMSSecurityService$1;)V
    .locals 0
    .param p1    # Lcom/zte/smssecurity/SMSSecurityService;
    .param p2    # Lcom/zte/smssecurity/SMSSecurityService$1;

    invoke-direct {p0, p1}, Lcom/zte/smssecurity/SMSSecurityService$DetectToShowDialog;-><init>(Lcom/zte/smssecurity/SMSSecurityService;)V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 7

    const/4 v6, 0x1

    const-string v3, "SMSSecurityService"

    const-string v4, "DetectToShowDialog >>>>"

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v3, p0, Lcom/zte/smssecurity/SMSSecurityService$DetectToShowDialog;->this$0:Lcom/zte/smssecurity/SMSSecurityService;

    const-string v4, "phone"

    invoke-virtual {v3, v4}, Landroid/content/ContextWrapper;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/telephony/TelephonyManager;

    iget-object v3, p0, Lcom/zte/smssecurity/SMSSecurityService$DetectToShowDialog;->this$0:Lcom/zte/smssecurity/SMSSecurityService;

    invoke-static {v3}, Lcom/zte/smssecurity/SMSSecurityService;->access$200(Lcom/zte/smssecurity/SMSSecurityService;)Landroid/telephony/PhoneStateListener;

    move-result-object v3

    invoke-static {}, Landroid/telephony/TelephonyManager;->getDefault()Landroid/telephony/TelephonyManager;

    move-result-object v4

    invoke-virtual {v4}, Landroid/telephony/TelephonyManager;->getSmsDefaultSim()I

    move-result v4

    invoke-virtual {v2, v3, v6, v4}, Landroid/telephony/TelephonyManager;->listenGemini(Landroid/telephony/PhoneStateListener;II)V

    iget-object v3, p0, Lcom/zte/smssecurity/SMSSecurityService$DetectToShowDialog;->this$0:Lcom/zte/smssecurity/SMSSecurityService;

    const-string v4, "keyguard"

    invoke-virtual {v3, v4}, Landroid/content/ContextWrapper;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/app/KeyguardManager;

    :cond_0
    :goto_0
    invoke-static {}, Lcom/zte/smssecurity/SMSSecurityService;->access$300()Z

    move-result v3

    if-eqz v3, :cond_1

    :try_start_0
    const-string v3, "SMSSecurityService"

    const-string v4, "State is out of home"

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    const-wide/16 v3, 0x2710

    invoke-static {v3, v4}, Ljava/lang/Thread;->sleep(J)V
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_1
    invoke-virtual {v1}, Landroid/app/KeyguardManager;->inKeyguardRestrictedInputMode()Z

    move-result v3

    if-nez v3, :cond_0

    iget-object v3, p0, Lcom/zte/smssecurity/SMSSecurityService$DetectToShowDialog;->this$0:Lcom/zte/smssecurity/SMSSecurityService;

    invoke-static {v3}, Lcom/zte/smssecurity/SMSSecurityService;->access$400(Lcom/zte/smssecurity/SMSSecurityService;)Z

    move-result v3

    if-eqz v3, :cond_0

    const-string v3, "SMSSecurityService"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "mbPhoneServiceState = "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-static {}, Lcom/zte/smssecurity/SMSSecurityService;->access$500()Z

    move-result v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    invoke-static {}, Lcom/zte/smssecurity/SMSSecurityService;->access$500()Z

    move-result v3

    if-eqz v3, :cond_0

    const/4 v3, 0x0

    invoke-static {v3}, Lcom/zte/smssecurity/SMSSecurityService;->access$302(Z)Z

    goto :goto_0

    :cond_1
    new-instance v0, Landroid/content/Intent;

    const-string v3, "android.intent.action.showSmsSecurity"

    invoke-direct {v0, v3}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    iget-object v3, p0, Lcom/zte/smssecurity/SMSSecurityService$DetectToShowDialog;->this$0:Lcom/zte/smssecurity/SMSSecurityService;

    invoke-virtual {v3, v0}, Landroid/content/ContextWrapper;->sendBroadcast(Landroid/content/Intent;)V

    invoke-static {v6}, Lcom/zte/smssecurity/SMSSecurityService;->access$302(Z)Z

    return-void

    :catch_0
    move-exception v3

    goto :goto_1
.end method
