.class Lcom/zte/smssecurity/SMSSecuritySettings$1;
.super Ljava/lang/Object;
.source "SMSSecuritySettings.java"

# interfaces
.implements Landroid/content/DialogInterface$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/zte/smssecurity/SMSSecuritySettings;->onPreferenceChange(Landroid/preference/Preference;Ljava/lang/Object;)Z
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/zte/smssecurity/SMSSecuritySettings;

.field final synthetic val$value:Ljava/lang/Object;


# direct methods
.method constructor <init>(Lcom/zte/smssecurity/SMSSecuritySettings;Ljava/lang/Object;)V
    .locals 0

    iput-object p1, p0, Lcom/zte/smssecurity/SMSSecuritySettings$1;->this$0:Lcom/zte/smssecurity/SMSSecuritySettings;

    iput-object p2, p0, Lcom/zte/smssecurity/SMSSecuritySettings$1;->val$value:Ljava/lang/Object;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/content/DialogInterface;I)V
    .locals 5
    .param p1    # Landroid/content/DialogInterface;
    .param p2    # I

    const/4 v4, 0x0

    invoke-interface {p1}, Landroid/content/DialogInterface;->dismiss()V

    iget-object v2, p0, Lcom/zte/smssecurity/SMSSecuritySettings$1;->val$value:Ljava/lang/Object;

    check-cast v2, Ljava/lang/Boolean;

    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v2

    if-nez v2, :cond_0

    new-instance v1, Landroid/content/ContentValues;

    invoke-direct {v1}, Landroid/content/ContentValues;-><init>()V

    const-string v2, "VALUE"

    const-string v3, "0"

    invoke-virtual {v1, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v2, p0, Lcom/zte/smssecurity/SMSSecuritySettings$1;->this$0:Lcom/zte/smssecurity/SMSSecuritySettings;

    invoke-static {v2}, Lcom/zte/smssecurity/SMSSecuritySettings;->access$000(Lcom/zte/smssecurity/SMSSecuritySettings;)Lcom/zte/smssecurity/SMSSecurityDatabaseHelper;

    move-result-object v2

    invoke-virtual {v2}, Landroid/database/sqlite/SQLiteOpenHelper;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    const-string v2, "SMSSecurityDatabase"

    const-string v3, "TYPE=\"SERVICE_STATE\""

    invoke-virtual {v0, v2, v1, v3, v4}, Landroid/database/sqlite/SQLiteDatabase;->update(Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    invoke-virtual {v0}, Landroid/database/sqlite/SQLiteClosable;->close()V

    iget-object v2, p0, Lcom/zte/smssecurity/SMSSecuritySettings$1;->this$0:Lcom/zte/smssecurity/SMSSecuritySettings;

    invoke-static {v2}, Lcom/zte/smssecurity/SMSSecuritySettings;->access$100(Lcom/zte/smssecurity/SMSSecuritySettings;)Landroid/preference/CheckBoxPreference;

    move-result-object v2

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Landroid/preference/TwoStatePreference;->setChecked(Z)V

    iget-object v2, p0, Lcom/zte/smssecurity/SMSSecuritySettings$1;->this$0:Lcom/zte/smssecurity/SMSSecuritySettings;

    new-instance v3, Landroid/content/Intent;

    const-string v4, "com.zte.smssecurity.action.stopservice"

    invoke-direct {v3, v4}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v3}, Landroid/content/ContextWrapper;->sendBroadcast(Landroid/content/Intent;)V

    const-string v2, "SMSSecuritySettings"

    const-string v3, "send stopService"

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :goto_0
    return-void

    :cond_0
    new-instance v1, Landroid/content/ContentValues;

    invoke-direct {v1}, Landroid/content/ContentValues;-><init>()V

    const-string v2, "VALUE"

    const-string v3, "1"

    invoke-virtual {v1, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v2, p0, Lcom/zte/smssecurity/SMSSecuritySettings$1;->this$0:Lcom/zte/smssecurity/SMSSecuritySettings;

    invoke-static {v2}, Lcom/zte/smssecurity/SMSSecuritySettings;->access$000(Lcom/zte/smssecurity/SMSSecuritySettings;)Lcom/zte/smssecurity/SMSSecurityDatabaseHelper;

    move-result-object v2

    invoke-virtual {v2}, Landroid/database/sqlite/SQLiteOpenHelper;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    const-string v2, "SMSSecurityDatabase"

    const-string v3, "TYPE=\"SERVICE_STATE\""

    invoke-virtual {v0, v2, v1, v3, v4}, Landroid/database/sqlite/SQLiteDatabase;->update(Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    invoke-virtual {v0}, Landroid/database/sqlite/SQLiteClosable;->close()V

    iget-object v2, p0, Lcom/zte/smssecurity/SMSSecuritySettings$1;->this$0:Lcom/zte/smssecurity/SMSSecuritySettings;

    invoke-static {v2}, Lcom/zte/smssecurity/SMSSecuritySettings;->access$100(Lcom/zte/smssecurity/SMSSecuritySettings;)Landroid/preference/CheckBoxPreference;

    move-result-object v2

    const/4 v3, 0x1

    invoke-virtual {v2, v3}, Landroid/preference/TwoStatePreference;->setChecked(Z)V

    iget-object v2, p0, Lcom/zte/smssecurity/SMSSecuritySettings$1;->this$0:Lcom/zte/smssecurity/SMSSecuritySettings;

    new-instance v3, Landroid/content/Intent;

    const-string v4, "com.zte.smssecurity.action.startservice"

    invoke-direct {v3, v4}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v3}, Landroid/content/ContextWrapper;->sendBroadcast(Landroid/content/Intent;)V

    const-string v2, "SMSSecuritySettings"

    const-string v3, "send StartService"

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method
