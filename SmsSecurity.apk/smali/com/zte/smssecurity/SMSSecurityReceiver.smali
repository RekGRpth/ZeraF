.class public Lcom/zte/smssecurity/SMSSecurityReceiver;
.super Landroid/content/BroadcastReceiver;
.source "SMSSecurityReceiver.java"


# static fields
.field private static final BACK_ACTION:Ljava/lang/String; = "android.intent.action.showSmsSecurityBack"

.field private static final BOOTTIME_TRIGGER:Ljava/lang/String; = "3"

.field private static final BOOT_ACTION:Ljava/lang/String; = "android.intent.action.BOOT_COMPLETED"

.field private static final OPTION:Ljava/lang/String; = "option"

.field private static final SERVICESTATE:I = 0x2

.field private static final START_ACTION:Ljava/lang/String; = "com.zte.smssecurity.action.startservice"

.field private static final STOP_ACTION:Ljava/lang/String; = "com.zte.smssecurity.action.stopservice"

.field private static final TAG:Ljava/lang/String; = "SMSSecurityReceiver"

.field public static backCount:I

.field private static mbStart:Z


# instance fields
.field private cursor:Landroid/database/Cursor;

.field private db:Landroid/database/sqlite/SQLiteDatabase;

.field pendingIntent:Landroid/app/PendingIntent;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const/4 v0, 0x0

    sput-boolean v0, Lcom/zte/smssecurity/SMSSecurityReceiver;->mbStart:Z

    sput v0, Lcom/zte/smssecurity/SMSSecurityReceiver;->backCount:I

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    const/4 v0, 0x0

    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    iput-object v0, p0, Lcom/zte/smssecurity/SMSSecurityReceiver;->db:Landroid/database/sqlite/SQLiteDatabase;

    iput-object v0, p0, Lcom/zte/smssecurity/SMSSecurityReceiver;->cursor:Landroid/database/Cursor;

    iput-object v0, p0, Lcom/zte/smssecurity/SMSSecurityReceiver;->pendingIntent:Landroid/app/PendingIntent;

    return-void
.end method

.method private showSetting(Landroid/content/Context;)V
    .locals 3
    .param p1    # Landroid/content/Context;

    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    const-string v1, "com.zte.smssecurity"

    const-string v2, "com.zte.smssecurity.SMSSecuritySettings"

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->setClassName(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const-string v1, "option"

    const/4 v2, 0x2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    const/high16 v1, 0x10000000

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    invoke-virtual {p1, v0}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    return-void
.end method

.method private startService(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 14
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/content/Intent;

    const-string v0, "SMSSecurityReceiver"

    const-string v1, "Start Service"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    invoke-virtual {p1}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v13

    new-instance v11, Landroid/content/ComponentName;

    const-string v0, "com.zte.smssecurity"

    const-string v1, "com.zte.smssecurity.PhoneStateReceiver"

    invoke-direct {v11, v0, v1}, Landroid/content/ComponentName;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    const/4 v0, 0x1

    const/4 v1, 0x1

    invoke-virtual {v13, v11, v0, v1}, Landroid/content/pm/PackageManager;->setComponentEnabledSetting(Landroid/content/ComponentName;II)V

    iget-object v0, p0, Lcom/zte/smssecurity/SMSSecurityReceiver;->db:Landroid/database/sqlite/SQLiteDatabase;

    const-string v1, "SMSSecurityDatabase"

    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/String;

    const/4 v3, 0x0

    const-string v4, "TYPE"

    aput-object v4, v2, v3

    const/4 v3, 0x1

    const-string v4, "VALUE"

    aput-object v4, v2, v3

    const-string v3, "TYPE=\"BOOTTIME\""

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    invoke-virtual/range {v0 .. v7}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v12

    const-wide/16 v9, 0x0

    :cond_0
    invoke-interface {v12}, Landroid/database/Cursor;->moveToNext()Z

    move-result v0

    if-eqz v0, :cond_1

    const/4 v0, 0x0

    invoke-interface {v12, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    const-string v1, "BOOTTIME"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    invoke-interface {v12, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v9

    const-string v0, "SMSSecurityReceiver"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "boot time"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v9, v10}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_1
    invoke-interface {v12}, Landroid/database/Cursor;->close()V

    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v0

    add-long/2addr v9, v0

    const-string v0, "alarm"

    invoke-virtual {p1, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Landroid/app/AlarmManager;

    const/4 v0, 0x2

    iget-object v1, p0, Lcom/zte/smssecurity/SMSSecurityReceiver;->pendingIntent:Landroid/app/PendingIntent;

    invoke-virtual {v8, v0, v9, v10, v1}, Landroid/app/AlarmManager;->set(IJLandroid/app/PendingIntent;)V

    const-string v0, "SMSSecurityReceiver"

    const-string v1, "register alarm"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v0, 0x1

    sput-boolean v0, Lcom/zte/smssecurity/SMSSecurityReceiver;->mbStart:Z

    return-void
.end method

.method private startServiceDelay(Landroid/content/Context;Landroid/content/Intent;I)V
    .locals 15
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/content/Intent;
    .param p3    # I

    const-string v1, "SMSSecurityReceiver"

    const-string v2, "Start Service"

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    invoke-virtual/range {p1 .. p1}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v14

    new-instance v12, Landroid/content/ComponentName;

    const-string v1, "com.zte.smssecurity"

    const-string v2, "com.zte.smssecurity.PhoneStateReceiver"

    invoke-direct {v12, v1, v2}, Landroid/content/ComponentName;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    const/4 v1, 0x1

    const/4 v2, 0x1

    invoke-virtual {v14, v12, v1, v2}, Landroid/content/pm/PackageManager;->setComponentEnabledSetting(Landroid/content/ComponentName;II)V

    iget-object v1, p0, Lcom/zte/smssecurity/SMSSecurityReceiver;->db:Landroid/database/sqlite/SQLiteDatabase;

    const-string v2, "SMSSecurityDatabase"

    const/4 v3, 0x2

    new-array v3, v3, [Ljava/lang/String;

    const/4 v4, 0x0

    const-string v5, "TYPE"

    aput-object v5, v3, v4

    const/4 v4, 0x1

    const-string v5, "VALUE"

    aput-object v5, v3, v4

    const-string v4, "TYPE=\"BOOTTIME\""

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    const/4 v8, 0x0

    invoke-virtual/range {v1 .. v8}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v13

    move/from16 v0, p3

    int-to-long v10, v0

    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v1

    add-long/2addr v10, v1

    const-string v1, "alarm"

    move-object/from16 v0, p1

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Landroid/app/AlarmManager;

    const/4 v1, 0x2

    iget-object v2, p0, Lcom/zte/smssecurity/SMSSecurityReceiver;->pendingIntent:Landroid/app/PendingIntent;

    invoke-virtual {v9, v1, v10, v11, v2}, Landroid/app/AlarmManager;->set(IJLandroid/app/PendingIntent;)V

    const-string v1, "SMSSecurityReceiver"

    const-string v2, "register alarm"

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v1, 0x1

    sput-boolean v1, Lcom/zte/smssecurity/SMSSecurityReceiver;->mbStart:Z

    return-void
.end method

.method private stopService(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 8
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/content/Intent;

    const/4 v7, 0x2

    const/4 v6, 0x1

    const-string v4, "SMSSecurityReceiver"

    const-string v5, "Stop Service"

    invoke-static {v4, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    invoke-static {}, Lcom/zte/smssecurity/Util;->writeCloseFlag()V

    invoke-virtual {p1}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v3

    new-instance v1, Landroid/content/ComponentName;

    const-string v4, "com.zte.smssecurity"

    const-string v5, "com.zte.smssecurity.PhoneStateReceiver"

    invoke-direct {v1, v4, v5}, Landroid/content/ComponentName;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v3, v1, v7, v6}, Landroid/content/pm/PackageManager;->setComponentEnabledSetting(Landroid/content/ComponentName;II)V

    const-string v4, "SMSSecurityReceiver"

    const-string v5, "Disable PhoneStateReceiver!"

    invoke-static {v4, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    new-instance v2, Landroid/content/ComponentName;

    const-string v4, "com.zte.smssecurity"

    const-string v5, "com.zte.smssecurity.SMSSecurityReceiver"

    invoke-direct {v2, v4, v5}, Landroid/content/ComponentName;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v3, v2, v7, v6}, Landroid/content/pm/PackageManager;->setComponentEnabledSetting(Landroid/content/ComponentName;II)V

    const-string v4, "alarm"

    invoke-virtual {p1, v4}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/AlarmManager;

    iget-object v4, p0, Lcom/zte/smssecurity/SMSSecurityReceiver;->pendingIntent:Landroid/app/PendingIntent;

    invoke-virtual {v0, v4}, Landroid/app/AlarmManager;->cancel(Landroid/app/PendingIntent;)V

    const-string v4, "SMSSecurityReceiver"

    const-string v5, "Remove alarm"

    invoke-static {v4, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v4, 0x0

    sput-boolean v4, Lcom/zte/smssecurity/SMSSecurityReceiver;->mbStart:Z

    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 12
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/content/Intent;

    const/4 v3, 0x1

    const/4 v11, 0x0

    const/4 v4, 0x0

    new-instance v0, Lcom/zte/smssecurity/SMSSecurityDatabaseHelper;

    const-string v1, "SMSSecurityDatabase"

    invoke-direct {v0, p1, v1, v4, v3}, Lcom/zte/smssecurity/SMSSecurityDatabaseHelper;-><init>(Landroid/content/Context;Ljava/lang/String;Landroid/database/sqlite/SQLiteDatabase$CursorFactory;I)V

    invoke-virtual {v0}, Landroid/database/sqlite/SQLiteOpenHelper;->getReadableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    iput-object v0, p0, Lcom/zte/smssecurity/SMSSecurityReceiver;->db:Landroid/database/sqlite/SQLiteDatabase;

    new-instance v8, Landroid/content/Intent;

    const-class v0, Lcom/zte/smssecurity/SMSSecurityService;

    invoke-direct {v8, p1, v0}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const-string v0, "TriggerType"

    const-string v1, "3"

    invoke-virtual {v8, v0, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    invoke-static {p1, v11, v8, v11}, Landroid/app/PendingIntent;->getService(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v0

    iput-object v0, p0, Lcom/zte/smssecurity/SMSSecurityReceiver;->pendingIntent:Landroid/app/PendingIntent;

    invoke-static {}, Lcom/zte/smssecurity/Util;->getServiceState()Z

    move-result v9

    if-eqz v9, :cond_4

    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    const-string v1, "android.intent.action.BOOT_COMPLETED"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    const-string v0, "SMSSecurityReceiver"

    const-string v1, "Boot  completed.."

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    new-instance v10, Landroid/content/ContentValues;

    invoke-direct {v10}, Landroid/content/ContentValues;-><init>()V

    const-string v0, "VALUE"

    const-string v1, "0"

    invoke-virtual {v10, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/zte/smssecurity/SMSSecurityReceiver;->db:Landroid/database/sqlite/SQLiteDatabase;

    const-string v1, "SMSSecurityDatabase"

    const-string v2, "TYPE=\"SENDSMSNOW\""

    invoke-virtual {v0, v1, v10, v2, v4}, Landroid/database/sqlite/SQLiteDatabase;->update(Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    iget-object v0, p0, Lcom/zte/smssecurity/SMSSecurityReceiver;->db:Landroid/database/sqlite/SQLiteDatabase;

    const-string v1, "SMSSecurityDatabase"

    new-array v2, v3, [Ljava/lang/String;

    const-string v3, "VALUE"

    aput-object v3, v2, v11

    const-string v3, "TYPE=\"SERVICE_STATE\""

    move-object v5, v4

    move-object v6, v4

    move-object v7, v4

    invoke-virtual/range {v0 .. v7}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v0

    iput-object v0, p0, Lcom/zte/smssecurity/SMSSecurityReceiver;->cursor:Landroid/database/Cursor;

    :cond_0
    iget-object v0, p0, Lcom/zte/smssecurity/SMSSecurityReceiver;->cursor:Landroid/database/Cursor;

    invoke-interface {v0}, Landroid/database/Cursor;->moveToNext()Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/zte/smssecurity/SMSSecurityReceiver;->cursor:Landroid/database/Cursor;

    invoke-interface {v0, v11}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    const-string v1, "1"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/zte/smssecurity/SMSSecurityReceiver;->cursor:Landroid/database/Cursor;

    invoke-interface {v0}, Landroid/database/Cursor;->close()V

    invoke-direct {p0, p1, p2}, Lcom/zte/smssecurity/SMSSecurityReceiver;->startService(Landroid/content/Context;Landroid/content/Intent;)V

    :cond_1
    :goto_0
    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    const-string v1, "com.zte.smssecurity.action.startservice"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_5

    sget-boolean v0, Lcom/zte/smssecurity/SMSSecurityReceiver;->mbStart:Z

    if-nez v0, :cond_2

    new-instance v10, Landroid/content/ContentValues;

    invoke-direct {v10}, Landroid/content/ContentValues;-><init>()V

    const-string v0, "VALUE"

    const-string v1, "1"

    invoke-virtual {v10, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/zte/smssecurity/SMSSecurityReceiver;->db:Landroid/database/sqlite/SQLiteDatabase;

    const-string v1, "SMSSecurityDatabase"

    const-string v2, "TYPE=\"SERVICE_STATE\""

    invoke-virtual {v0, v1, v10, v2, v4}, Landroid/database/sqlite/SQLiteDatabase;->update(Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    invoke-virtual {v10}, Landroid/content/ContentValues;->clear()V

    const-string v0, "VALUE"

    const-string v1, "0"

    invoke-virtual {v10, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/zte/smssecurity/SMSSecurityReceiver;->db:Landroid/database/sqlite/SQLiteDatabase;

    const-string v1, "SMSSecurityDatabase"

    const-string v2, "TYPE=\"USER_CALLTIME\""

    invoke-virtual {v0, v1, v10, v2, v4}, Landroid/database/sqlite/SQLiteDatabase;->update(Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    invoke-virtual {v10}, Landroid/content/ContentValues;->clear()V

    const-string v0, "VALUE"

    const-string v1, "0"

    invoke-virtual {v10, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/zte/smssecurity/SMSSecurityReceiver;->db:Landroid/database/sqlite/SQLiteDatabase;

    const-string v1, "SMSSecurityDatabase"

    const-string v2, "TYPE=\"LONGTIMEBOOT\""

    invoke-virtual {v0, v1, v10, v2, v4}, Landroid/database/sqlite/SQLiteDatabase;->update(Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    invoke-virtual {v10}, Landroid/content/ContentValues;->clear()V

    const-string v0, "VALUE"

    const-string v1, "0"

    invoke-virtual {v10, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/zte/smssecurity/SMSSecurityReceiver;->db:Landroid/database/sqlite/SQLiteDatabase;

    const-string v1, "SMSSecurityDatabase"

    const-string v2, "TYPE=\"SENDSMSNOW\""

    invoke-virtual {v0, v1, v10, v2, v4}, Landroid/database/sqlite/SQLiteDatabase;->update(Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    invoke-direct {p0, p1, p2}, Lcom/zte/smssecurity/SMSSecurityReceiver;->startService(Landroid/content/Context;Landroid/content/Intent;)V

    :cond_2
    invoke-direct {p0, p1}, Lcom/zte/smssecurity/SMSSecurityReceiver;->showSetting(Landroid/content/Context;)V

    :cond_3
    :goto_1
    iget-object v0, p0, Lcom/zte/smssecurity/SMSSecurityReceiver;->db:Landroid/database/sqlite/SQLiteDatabase;

    invoke-virtual {v0}, Landroid/database/sqlite/SQLiteClosable;->close()V

    return-void

    :cond_4
    const-string v0, "SMSSecurityReceiver"

    const-string v1, "SmsSecurity has closed"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    invoke-direct {p0, p1, p2}, Lcom/zte/smssecurity/SMSSecurityReceiver;->stopService(Landroid/content/Context;Landroid/content/Intent;)V

    goto :goto_0

    :cond_5
    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    const-string v1, "com.zte.smssecurity.action.stopservice"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_6

    new-instance v10, Landroid/content/ContentValues;

    invoke-direct {v10}, Landroid/content/ContentValues;-><init>()V

    const-string v0, "VALUE"

    const-string v1, "0"

    invoke-virtual {v10, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/zte/smssecurity/SMSSecurityReceiver;->db:Landroid/database/sqlite/SQLiteDatabase;

    const-string v1, "SMSSecurityDatabase"

    const-string v2, "TYPE=\"SERVICE_STATE\""

    invoke-virtual {v0, v1, v10, v2, v4}, Landroid/database/sqlite/SQLiteDatabase;->update(Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    invoke-direct {p0, p1, p2}, Lcom/zte/smssecurity/SMSSecurityReceiver;->stopService(Landroid/content/Context;Landroid/content/Intent;)V

    invoke-direct {p0, p1}, Lcom/zte/smssecurity/SMSSecurityReceiver;->showSetting(Landroid/content/Context;)V

    goto :goto_1

    :cond_6
    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    const-string v1, "android.intent.action.showSmsSecurityBack"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    const-string v0, "back"

    invoke-virtual {p2, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    const-string v1, "ok"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_7

    const-string v0, "SMSSecurityReceiver"

    const-string v1, "back ok"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    new-instance v10, Landroid/content/ContentValues;

    invoke-direct {v10}, Landroid/content/ContentValues;-><init>()V

    const-string v0, "VALUE"

    const-string v1, "1"

    invoke-virtual {v10, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/zte/smssecurity/SMSSecurityReceiver;->db:Landroid/database/sqlite/SQLiteDatabase;

    const-string v1, "SMSSecurityDatabase"

    const-string v2, "TYPE=\"SENDSMSNOW\""

    invoke-virtual {v0, v1, v10, v2, v4}, Landroid/database/sqlite/SQLiteDatabase;->update(Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    invoke-virtual {v10}, Landroid/content/ContentValues;->clear()V

    const-string v0, "VALUE"

    const-string v1, "1"

    invoke-virtual {v10, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/zte/smssecurity/SMSSecurityReceiver;->db:Landroid/database/sqlite/SQLiteDatabase;

    const-string v1, "SMSSecurityDatabase"

    const-string v2, "TYPE=\"SHOWREGISTERED\""

    invoke-virtual {v0, v1, v10, v2, v4}, Landroid/database/sqlite/SQLiteDatabase;->update(Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    const/16 v0, 0x1f4

    invoke-direct {p0, p1, p2, v0}, Lcom/zte/smssecurity/SMSSecurityReceiver;->startServiceDelay(Landroid/content/Context;Landroid/content/Intent;I)V

    goto/16 :goto_1

    :cond_7
    const-string v0, "back"

    invoke-virtual {p2, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    const-string v1, "cancel"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    sget v0, Lcom/zte/smssecurity/SMSSecurityReceiver;->backCount:I

    add-int/lit8 v0, v0, 0x1

    sput v0, Lcom/zte/smssecurity/SMSSecurityReceiver;->backCount:I

    const-string v0, "SMSSecurityReceiver"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "backCount = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    sget v2, Lcom/zte/smssecurity/SMSSecurityReceiver;->backCount:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    sget v0, Lcom/zte/smssecurity/SMSSecurityReceiver;->backCount:I

    packed-switch v0, :pswitch_data_0

    goto/16 :goto_1

    :pswitch_0
    const v0, 0x36ee80

    invoke-direct {p0, p1, p2, v0}, Lcom/zte/smssecurity/SMSSecurityReceiver;->startServiceDelay(Landroid/content/Context;Landroid/content/Intent;I)V

    goto/16 :goto_1

    :pswitch_1
    new-instance v10, Landroid/content/ContentValues;

    invoke-direct {v10}, Landroid/content/ContentValues;-><init>()V

    const-string v0, "VALUE"

    const-string v1, "1"

    invoke-virtual {v10, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/zte/smssecurity/SMSSecurityReceiver;->db:Landroid/database/sqlite/SQLiteDatabase;

    const-string v1, "SMSSecurityDatabase"

    const-string v2, "TYPE=\"SENDSMSNOW\""

    invoke-virtual {v0, v1, v10, v2, v4}, Landroid/database/sqlite/SQLiteDatabase;->update(Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    invoke-virtual {v10}, Landroid/content/ContentValues;->clear()V

    const-string v0, "VALUE"

    const-string v1, "0"

    invoke-virtual {v10, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/zte/smssecurity/SMSSecurityReceiver;->db:Landroid/database/sqlite/SQLiteDatabase;

    const-string v1, "SMSSecurityDatabase"

    const-string v2, "TYPE=\"SHOWREGISTERED\""

    invoke-virtual {v0, v1, v10, v2, v4}, Landroid/database/sqlite/SQLiteDatabase;->update(Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    invoke-virtual {v10}, Landroid/content/ContentValues;->clear()V

    const v0, 0x6ddd00

    invoke-direct {p0, p1, p2, v0}, Lcom/zte/smssecurity/SMSSecurityReceiver;->startServiceDelay(Landroid/content/Context;Landroid/content/Intent;I)V

    goto/16 :goto_1

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method
