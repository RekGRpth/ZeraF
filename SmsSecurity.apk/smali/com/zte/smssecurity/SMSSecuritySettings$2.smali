.class Lcom/zte/smssecurity/SMSSecuritySettings$2;
.super Ljava/lang/Object;
.source "SMSSecuritySettings.java"

# interfaces
.implements Landroid/content/DialogInterface$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/zte/smssecurity/SMSSecuritySettings;->onPreferenceChange(Landroid/preference/Preference;Ljava/lang/Object;)Z
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/zte/smssecurity/SMSSecuritySettings;

.field final synthetic val$value:Ljava/lang/Object;


# direct methods
.method constructor <init>(Lcom/zte/smssecurity/SMSSecuritySettings;Ljava/lang/Object;)V
    .locals 0

    iput-object p1, p0, Lcom/zte/smssecurity/SMSSecuritySettings$2;->this$0:Lcom/zte/smssecurity/SMSSecuritySettings;

    iput-object p2, p0, Lcom/zte/smssecurity/SMSSecuritySettings$2;->val$value:Ljava/lang/Object;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/content/DialogInterface;I)V
    .locals 2
    .param p1    # Landroid/content/DialogInterface;
    .param p2    # I

    invoke-interface {p1}, Landroid/content/DialogInterface;->dismiss()V

    iget-object v0, p0, Lcom/zte/smssecurity/SMSSecuritySettings$2;->val$value:Ljava/lang/Object;

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/zte/smssecurity/SMSSecuritySettings$2;->this$0:Lcom/zte/smssecurity/SMSSecuritySettings;

    invoke-static {v0}, Lcom/zte/smssecurity/SMSSecuritySettings;->access$100(Lcom/zte/smssecurity/SMSSecuritySettings;)Landroid/preference/CheckBoxPreference;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/preference/TwoStatePreference;->setChecked(Z)V

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/zte/smssecurity/SMSSecuritySettings$2;->this$0:Lcom/zte/smssecurity/SMSSecuritySettings;

    invoke-static {v0}, Lcom/zte/smssecurity/SMSSecuritySettings;->access$100(Lcom/zte/smssecurity/SMSSecuritySettings;)Landroid/preference/CheckBoxPreference;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/preference/TwoStatePreference;->setChecked(Z)V

    goto :goto_0
.end method
