.class public Lcom/zte/smssecurity/PhoneTimeHandleService;
.super Landroid/app/Service;
.source "PhoneTimeHandleService.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/zte/smssecurity/PhoneTimeHandleService$1;,
        Lcom/zte/smssecurity/PhoneTimeHandleService$HandleThread;
    }
.end annotation


# static fields
.field private static final CALLTIME_TRIGGER:Ljava/lang/String; = "2"

.field private static final ONCECALLTIME_TRIGGER:Ljava/lang/String; = "4"

.field private static final SERVICE_ACTION:Ljava/lang/String; = "com.zte.smssecurity.SMSSecurityService"

.field private static final TAG:Ljava/lang/String; = "PhoneStateHandleService"


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Landroid/app/Service;-><init>()V

    return-void
.end method


# virtual methods
.method public onBind(Landroid/content/Intent;)Landroid/os/IBinder;
    .locals 1
    .param p1    # Landroid/content/Intent;

    const/4 v0, 0x0

    return-object v0
.end method

.method public onStartCommand(Landroid/content/Intent;II)I
    .locals 2
    .param p1    # Landroid/content/Intent;
    .param p2    # I
    .param p3    # I

    new-instance v0, Lcom/zte/smssecurity/PhoneTimeHandleService$HandleThread;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/zte/smssecurity/PhoneTimeHandleService$HandleThread;-><init>(Lcom/zte/smssecurity/PhoneTimeHandleService;Lcom/zte/smssecurity/PhoneTimeHandleService$1;)V

    invoke-virtual {v0}, Ljava/lang/Thread;->start()V

    invoke-super {p0, p1, p2, p3}, Landroid/app/Service;->onStartCommand(Landroid/content/Intent;II)I

    move-result v0

    return v0
.end method
