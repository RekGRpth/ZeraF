.class Lcom/zte/smssecurity/SMSSecurityService$SendSMS;
.super Ljava/lang/Thread;
.source "SMSSecurityService.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/zte/smssecurity/SMSSecurityService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "SendSMS"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/zte/smssecurity/SMSSecurityService;


# direct methods
.method private constructor <init>(Lcom/zte/smssecurity/SMSSecurityService;)V
    .locals 0

    iput-object p1, p0, Lcom/zte/smssecurity/SMSSecurityService$SendSMS;->this$0:Lcom/zte/smssecurity/SMSSecurityService;

    invoke-direct {p0}, Ljava/lang/Thread;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/zte/smssecurity/SMSSecurityService;Lcom/zte/smssecurity/SMSSecurityService$1;)V
    .locals 0
    .param p1    # Lcom/zte/smssecurity/SMSSecurityService;
    .param p2    # Lcom/zte/smssecurity/SMSSecurityService$1;

    invoke-direct {p0, p1}, Lcom/zte/smssecurity/SMSSecurityService$SendSMS;-><init>(Lcom/zte/smssecurity/SMSSecurityService;)V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 15

    const/4 v2, 0x0

    const/4 v3, 0x1

    const/4 v5, 0x0

    const-string v0, "SMSSecurityService"

    const-string v1, "SendSMS >>>>"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v0, p0, Lcom/zte/smssecurity/SMSSecurityService$SendSMS;->this$0:Lcom/zte/smssecurity/SMSSecurityService;

    const-string v1, "phone"

    invoke-virtual {v0, v1}, Landroid/content/ContextWrapper;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v14

    check-cast v14, Landroid/telephony/TelephonyManager;

    iget-object v0, p0, Lcom/zte/smssecurity/SMSSecurityService$SendSMS;->this$0:Lcom/zte/smssecurity/SMSSecurityService;

    invoke-static {v0}, Lcom/zte/smssecurity/SMSSecurityService;->access$200(Lcom/zte/smssecurity/SMSSecurityService;)Landroid/telephony/PhoneStateListener;

    move-result-object v0

    invoke-static {}, Landroid/telephony/TelephonyManager;->getDefault()Landroid/telephony/TelephonyManager;

    move-result-object v1

    invoke-virtual {v1}, Landroid/telephony/TelephonyManager;->getSmsDefaultSim()I

    move-result v1

    invoke-virtual {v14, v0, v3, v1}, Landroid/telephony/TelephonyManager;->listenGemini(Landroid/telephony/PhoneStateListener;II)V

    const-string v13, ""

    invoke-static {}, Landroid/telephony/TelephonyManager;->getDefault()Landroid/telephony/TelephonyManager;

    move-result-object v0

    invoke-virtual {v0}, Landroid/telephony/TelephonyManager;->getSmsDefaultSim()I

    move-result v0

    if-nez v0, :cond_3

    const-string v0, "SMSSecurityService"

    const-string v1, "getSmsDefaultSim = 0"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    const-string v0, "gsm.sim.operator.numeric"

    const-string v1, "-1"

    invoke-static {v0, v1}, Landroid/os/SystemProperties;->get(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v13

    const-string v0, "46001"

    invoke-virtual {v13, v0}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2

    const-string v0, "SMSSecurityService"

    const-string v1, "46001 >>>>"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    const-string v0, "10655057897"

    invoke-static {v0}, Lcom/zte/smssecurity/SMSSecurityService;->access$602(Ljava/lang/String;)Ljava/lang/String;

    :cond_0
    :goto_0
    const-string v0, "ro.build.date"

    const-string v1, "20120401"

    invoke-static {v0, v1}, Landroid/os/SystemProperties;->get(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    const-string v0, "SMSSecurityService"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "ro.build.date = "

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    new-instance v10, Ljava/lang/StringBuffer;

    invoke-direct {v10}, Ljava/lang/StringBuffer;-><init>()V

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const/4 v1, 0x4

    invoke-virtual {v7, v5, v1}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const/4 v1, 0x6

    const/16 v3, 0x8

    invoke-virtual {v7, v1, v3}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const/16 v1, 0xa

    const/16 v3, 0xc

    invoke-virtual {v7, v1, v3}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v10, v0}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    const-string v0, "SMSSecurityService"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "ro.build.date = "

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v10}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v6, 0x0

    const-string v0, "ro.build.date"

    const-string v1, "20120401"

    invoke-static {v0, v1}, Landroid/os/SystemProperties;->get(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    const/4 v8, 0x0

    invoke-virtual {v14, v5}, Landroid/telephony/TelephonyManager;->getDeviceIdGemini(I)Ljava/lang/String;

    move-result-object v8

    iget-object v0, p0, Lcom/zte/smssecurity/SMSSecurityService$SendSMS;->this$0:Lcom/zte/smssecurity/SMSSecurityService;

    invoke-virtual {v0}, Landroid/content/ContextWrapper;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    new-instance v1, Landroid/content/Intent;

    const-string v3, "SEND_INTENTACTION"

    invoke-direct {v1, v3}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    invoke-static {v0, v5, v1, v5}, Landroid/app/PendingIntent;->getBroadcast(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v4

    iget-object v0, p0, Lcom/zte/smssecurity/SMSSecurityService$SendSMS;->this$0:Lcom/zte/smssecurity/SMSSecurityService;

    iget-object v1, p0, Lcom/zte/smssecurity/SMSSecurityService$SendSMS;->this$0:Lcom/zte/smssecurity/SMSSecurityService;

    invoke-static {v1}, Lcom/zte/smssecurity/SMSSecurityService;->access$700(Lcom/zte/smssecurity/SMSSecurityService;)Landroid/content/BroadcastReceiver;

    move-result-object v1

    new-instance v3, Landroid/content/IntentFilter;

    const-string v5, "SEND_INTENTACTION"

    invoke-direct {v3, v5}, Landroid/content/IntentFilter;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, v1, v3}, Landroid/content/ContextWrapper;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    new-instance v9, Ljava/lang/StringBuffer;

    invoke-direct {v9}, Ljava/lang/StringBuffer;-><init>()V

    const-string v0, "IMEI:"

    invoke-virtual {v9, v0}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    if-nez v8, :cond_5

    const-string v0, ""

    :goto_1
    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v9, v0}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    const-string v0, "IMEI_BK:"

    invoke-virtual {v9, v0}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    if-nez v8, :cond_1

    const-string v8, ""

    :cond_1
    invoke-virtual {v0, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v9, v0}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    const-string v0, "NAME:"

    invoke-virtual {v9, v0}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "ro.build.display.id"

    invoke-static {v1}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "_"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v10}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v9, v0}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    const/4 v12, 0x0

    :goto_2
    invoke-static {}, Lcom/zte/smssecurity/SMSSecurityService;->access$800()Z

    move-result v0

    if-nez v0, :cond_8

    invoke-static {}, Lcom/zte/smssecurity/SMSSecurityService;->access$900()Z

    move-result v0

    if-nez v0, :cond_8

    const/4 v0, 0x3

    if-lt v12, v0, :cond_6

    const-string v0, "SMSSecurityService"

    const-string v1, "Three Times Over!"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v0, p0, Lcom/zte/smssecurity/SMSSecurityService$SendSMS;->this$0:Lcom/zte/smssecurity/SMSSecurityService;

    invoke-static {v0}, Lcom/zte/smssecurity/SMSSecurityService;->access$1000(Lcom/zte/smssecurity/SMSSecurityService;)V

    :goto_3
    return-void

    :cond_2
    const-string v0, "10657525749421"

    invoke-static {v0}, Lcom/zte/smssecurity/SMSSecurityService;->access$602(Ljava/lang/String;)Ljava/lang/String;

    const-string v0, "SMSSecurityService"

    const-string v1, "46000 >>>>"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0

    :cond_3
    invoke-static {}, Landroid/telephony/TelephonyManager;->getDefault()Landroid/telephony/TelephonyManager;

    move-result-object v0

    invoke-virtual {v0}, Landroid/telephony/TelephonyManager;->getSmsDefaultSim()I

    move-result v0

    if-ne v0, v3, :cond_0

    const-string v0, "gsm.sim.operator.numeric.2"

    const-string v1, "-1"

    invoke-static {v0, v1}, Landroid/os/SystemProperties;->get(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v13

    const-string v0, "46001"

    invoke-virtual {v13, v0}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_4

    const-string v0, "SMSSecurityService"

    const-string v1, "46001 >>>>"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    const-string v0, "10655057897"

    invoke-static {v0}, Lcom/zte/smssecurity/SMSSecurityService;->access$602(Ljava/lang/String;)Ljava/lang/String;

    goto/16 :goto_0

    :cond_4
    const-string v0, "10657525749421"

    invoke-static {v0}, Lcom/zte/smssecurity/SMSSecurityService;->access$602(Ljava/lang/String;)Ljava/lang/String;

    const-string v0, "SMSSecurityService"

    const-string v1, "46000 >>>>"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0

    :cond_5
    move-object v0, v8

    goto/16 :goto_1

    :cond_6
    :goto_4
    invoke-static {}, Lcom/zte/smssecurity/SMSSecurityService;->access$500()Z

    move-result v0

    if-nez v0, :cond_7

    :try_start_0
    const-string v0, "SMSSecurityService"

    const-string v1, "TelePhony State is out of service"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    const-wide/16 v0, 0x2710

    invoke-static {v0, v1}, Ljava/lang/Thread;->sleep(J)V
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_4

    :catch_0
    move-exception v0

    goto :goto_4

    :cond_7
    const-string v0, "SMSSecurityService"

    const-string v1, "Send SMS"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    invoke-static {}, Landroid/telephony/SmsManager;->getDefault()Landroid/telephony/SmsManager;

    move-result-object v0

    invoke-static {}, Lcom/zte/smssecurity/SMSSecurityService;->access$600()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v9}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v3

    move-object v5, v2

    invoke-virtual/range {v0 .. v5}, Landroid/telephony/SmsManager;->sendTextMessage(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Landroid/app/PendingIntent;Landroid/app/PendingIntent;)V

    add-int/lit8 v12, v12, 0x1

    const-wide/16 v0, 0x7530

    :try_start_1
    invoke-static {v0, v1}, Ljava/lang/Thread;->sleep(J)V
    :try_end_1
    .catch Ljava/lang/InterruptedException; {:try_start_1 .. :try_end_1} :catch_1

    goto/16 :goto_2

    :catch_1
    move-exception v11

    invoke-virtual {v11}, Ljava/lang/Throwable;->printStackTrace()V

    goto/16 :goto_2

    :cond_8
    iget-object v0, p0, Lcom/zte/smssecurity/SMSSecurityService$SendSMS;->this$0:Lcom/zte/smssecurity/SMSSecurityService;

    invoke-static {v0}, Lcom/zte/smssecurity/SMSSecurityService;->access$1000(Lcom/zte/smssecurity/SMSSecurityService;)V

    iget-object v0, p0, Lcom/zte/smssecurity/SMSSecurityService$SendSMS;->this$0:Lcom/zte/smssecurity/SMSSecurityService;

    invoke-virtual {v0}, Landroid/app/Service;->stopSelf()V

    goto/16 :goto_3
.end method
