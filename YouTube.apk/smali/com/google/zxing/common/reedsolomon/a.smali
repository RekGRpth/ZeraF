.class public final Lcom/google/zxing/common/reedsolomon/a;
.super Ljava/lang/Object;
.source "SourceFile"


# static fields
.field public static final a:Lcom/google/zxing/common/reedsolomon/a;

.field public static final b:Lcom/google/zxing/common/reedsolomon/a;

.field public static final c:Lcom/google/zxing/common/reedsolomon/a;

.field public static final d:Lcom/google/zxing/common/reedsolomon/a;

.field public static final e:Lcom/google/zxing/common/reedsolomon/a;

.field public static final f:Lcom/google/zxing/common/reedsolomon/a;

.field public static final g:Lcom/google/zxing/common/reedsolomon/a;

.field public static final h:Lcom/google/zxing/common/reedsolomon/a;


# instance fields
.field private i:[I

.field private j:[I

.field private k:Lcom/google/zxing/common/reedsolomon/b;

.field private l:Lcom/google/zxing/common/reedsolomon/b;

.field private final m:I

.field private final n:I

.field private o:Z


# direct methods
.method static constructor <clinit>()V
    .locals 4

    const/16 v3, 0x100

    new-instance v0, Lcom/google/zxing/common/reedsolomon/a;

    const/16 v1, 0x1069

    const/16 v2, 0x1000

    invoke-direct {v0, v1, v2}, Lcom/google/zxing/common/reedsolomon/a;-><init>(II)V

    sput-object v0, Lcom/google/zxing/common/reedsolomon/a;->a:Lcom/google/zxing/common/reedsolomon/a;

    new-instance v0, Lcom/google/zxing/common/reedsolomon/a;

    const/16 v1, 0x409

    const/16 v2, 0x400

    invoke-direct {v0, v1, v2}, Lcom/google/zxing/common/reedsolomon/a;-><init>(II)V

    sput-object v0, Lcom/google/zxing/common/reedsolomon/a;->b:Lcom/google/zxing/common/reedsolomon/a;

    new-instance v0, Lcom/google/zxing/common/reedsolomon/a;

    const/16 v1, 0x43

    const/16 v2, 0x40

    invoke-direct {v0, v1, v2}, Lcom/google/zxing/common/reedsolomon/a;-><init>(II)V

    sput-object v0, Lcom/google/zxing/common/reedsolomon/a;->c:Lcom/google/zxing/common/reedsolomon/a;

    new-instance v0, Lcom/google/zxing/common/reedsolomon/a;

    const/16 v1, 0x13

    const/16 v2, 0x10

    invoke-direct {v0, v1, v2}, Lcom/google/zxing/common/reedsolomon/a;-><init>(II)V

    sput-object v0, Lcom/google/zxing/common/reedsolomon/a;->d:Lcom/google/zxing/common/reedsolomon/a;

    new-instance v0, Lcom/google/zxing/common/reedsolomon/a;

    const/16 v1, 0x11d

    invoke-direct {v0, v1, v3}, Lcom/google/zxing/common/reedsolomon/a;-><init>(II)V

    sput-object v0, Lcom/google/zxing/common/reedsolomon/a;->e:Lcom/google/zxing/common/reedsolomon/a;

    new-instance v0, Lcom/google/zxing/common/reedsolomon/a;

    const/16 v1, 0x12d

    invoke-direct {v0, v1, v3}, Lcom/google/zxing/common/reedsolomon/a;-><init>(II)V

    sput-object v0, Lcom/google/zxing/common/reedsolomon/a;->f:Lcom/google/zxing/common/reedsolomon/a;

    sput-object v0, Lcom/google/zxing/common/reedsolomon/a;->g:Lcom/google/zxing/common/reedsolomon/a;

    sget-object v0, Lcom/google/zxing/common/reedsolomon/a;->c:Lcom/google/zxing/common/reedsolomon/a;

    sput-object v0, Lcom/google/zxing/common/reedsolomon/a;->h:Lcom/google/zxing/common/reedsolomon/a;

    return-void
.end method

.method private constructor <init>(II)V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/zxing/common/reedsolomon/a;->o:Z

    iput p1, p0, Lcom/google/zxing/common/reedsolomon/a;->n:I

    iput p2, p0, Lcom/google/zxing/common/reedsolomon/a;->m:I

    if-gtz p2, :cond_0

    invoke-direct {p0}, Lcom/google/zxing/common/reedsolomon/a;->d()V

    :cond_0
    return-void
.end method

.method static b(II)I
    .locals 1

    xor-int v0, p0, p1

    return v0
.end method

.method private d()V
    .locals 5

    const/4 v3, 0x1

    const/4 v1, 0x0

    iget v0, p0, Lcom/google/zxing/common/reedsolomon/a;->m:I

    new-array v0, v0, [I

    iput-object v0, p0, Lcom/google/zxing/common/reedsolomon/a;->i:[I

    iget v0, p0, Lcom/google/zxing/common/reedsolomon/a;->m:I

    new-array v0, v0, [I

    iput-object v0, p0, Lcom/google/zxing/common/reedsolomon/a;->j:[I

    move v0, v1

    move v2, v3

    :goto_0
    iget v4, p0, Lcom/google/zxing/common/reedsolomon/a;->m:I

    if-ge v0, v4, :cond_1

    iget-object v4, p0, Lcom/google/zxing/common/reedsolomon/a;->i:[I

    aput v2, v4, v0

    shl-int/lit8 v2, v2, 0x1

    iget v4, p0, Lcom/google/zxing/common/reedsolomon/a;->m:I

    if-lt v2, v4, :cond_0

    iget v4, p0, Lcom/google/zxing/common/reedsolomon/a;->n:I

    xor-int/2addr v2, v4

    iget v4, p0, Lcom/google/zxing/common/reedsolomon/a;->m:I

    add-int/lit8 v4, v4, -0x1

    and-int/2addr v2, v4

    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_1
    move v0, v1

    :goto_1
    iget v2, p0, Lcom/google/zxing/common/reedsolomon/a;->m:I

    add-int/lit8 v2, v2, -0x1

    if-ge v0, v2, :cond_2

    iget-object v2, p0, Lcom/google/zxing/common/reedsolomon/a;->j:[I

    iget-object v4, p0, Lcom/google/zxing/common/reedsolomon/a;->i:[I

    aget v4, v4, v0

    aput v0, v2, v4

    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    :cond_2
    new-instance v0, Lcom/google/zxing/common/reedsolomon/b;

    new-array v2, v3, [I

    aput v1, v2, v1

    invoke-direct {v0, p0, v2}, Lcom/google/zxing/common/reedsolomon/b;-><init>(Lcom/google/zxing/common/reedsolomon/a;[I)V

    iput-object v0, p0, Lcom/google/zxing/common/reedsolomon/a;->k:Lcom/google/zxing/common/reedsolomon/b;

    new-instance v0, Lcom/google/zxing/common/reedsolomon/b;

    new-array v2, v3, [I

    aput v3, v2, v1

    invoke-direct {v0, p0, v2}, Lcom/google/zxing/common/reedsolomon/b;-><init>(Lcom/google/zxing/common/reedsolomon/a;[I)V

    iput-object v0, p0, Lcom/google/zxing/common/reedsolomon/a;->l:Lcom/google/zxing/common/reedsolomon/b;

    iput-boolean v3, p0, Lcom/google/zxing/common/reedsolomon/a;->o:Z

    return-void
.end method

.method private e()V
    .locals 1

    iget-boolean v0, p0, Lcom/google/zxing/common/reedsolomon/a;->o:Z

    if-nez v0, :cond_0

    invoke-direct {p0}, Lcom/google/zxing/common/reedsolomon/a;->d()V

    :cond_0
    return-void
.end method


# virtual methods
.method final a(I)I
    .locals 1

    invoke-direct {p0}, Lcom/google/zxing/common/reedsolomon/a;->e()V

    iget-object v0, p0, Lcom/google/zxing/common/reedsolomon/a;->i:[I

    aget v0, v0, p1

    return v0
.end method

.method final a()Lcom/google/zxing/common/reedsolomon/b;
    .locals 1

    invoke-direct {p0}, Lcom/google/zxing/common/reedsolomon/a;->e()V

    iget-object v0, p0, Lcom/google/zxing/common/reedsolomon/a;->k:Lcom/google/zxing/common/reedsolomon/b;

    return-object v0
.end method

.method final a(II)Lcom/google/zxing/common/reedsolomon/b;
    .locals 2

    invoke-direct {p0}, Lcom/google/zxing/common/reedsolomon/a;->e()V

    if-gez p1, :cond_0

    new-instance v0, Ljava/lang/IllegalArgumentException;

    invoke-direct {v0}, Ljava/lang/IllegalArgumentException;-><init>()V

    throw v0

    :cond_0
    if-nez p2, :cond_1

    iget-object v0, p0, Lcom/google/zxing/common/reedsolomon/a;->k:Lcom/google/zxing/common/reedsolomon/b;

    :goto_0
    return-object v0

    :cond_1
    add-int/lit8 v0, p1, 0x1

    new-array v1, v0, [I

    const/4 v0, 0x0

    aput p2, v1, v0

    new-instance v0, Lcom/google/zxing/common/reedsolomon/b;

    invoke-direct {v0, p0, v1}, Lcom/google/zxing/common/reedsolomon/b;-><init>(Lcom/google/zxing/common/reedsolomon/a;[I)V

    goto :goto_0
.end method

.method final b(I)I
    .locals 1

    invoke-direct {p0}, Lcom/google/zxing/common/reedsolomon/a;->e()V

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/IllegalArgumentException;

    invoke-direct {v0}, Ljava/lang/IllegalArgumentException;-><init>()V

    throw v0

    :cond_0
    iget-object v0, p0, Lcom/google/zxing/common/reedsolomon/a;->j:[I

    aget v0, v0, p1

    return v0
.end method

.method final b()Lcom/google/zxing/common/reedsolomon/b;
    .locals 1

    invoke-direct {p0}, Lcom/google/zxing/common/reedsolomon/a;->e()V

    iget-object v0, p0, Lcom/google/zxing/common/reedsolomon/a;->l:Lcom/google/zxing/common/reedsolomon/b;

    return-object v0
.end method

.method public final c()I
    .locals 1

    iget v0, p0, Lcom/google/zxing/common/reedsolomon/a;->m:I

    return v0
.end method

.method final c(I)I
    .locals 3

    invoke-direct {p0}, Lcom/google/zxing/common/reedsolomon/a;->e()V

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/ArithmeticException;

    invoke-direct {v0}, Ljava/lang/ArithmeticException;-><init>()V

    throw v0

    :cond_0
    iget-object v0, p0, Lcom/google/zxing/common/reedsolomon/a;->i:[I

    iget v1, p0, Lcom/google/zxing/common/reedsolomon/a;->m:I

    iget-object v2, p0, Lcom/google/zxing/common/reedsolomon/a;->j:[I

    aget v2, v2, p1

    sub-int/2addr v1, v2

    add-int/lit8 v1, v1, -0x1

    aget v0, v0, v1

    return v0
.end method

.method final c(II)I
    .locals 3

    invoke-direct {p0}, Lcom/google/zxing/common/reedsolomon/a;->e()V

    if-eqz p1, :cond_0

    if-nez p2, :cond_1

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_1
    iget-object v0, p0, Lcom/google/zxing/common/reedsolomon/a;->i:[I

    iget-object v1, p0, Lcom/google/zxing/common/reedsolomon/a;->j:[I

    aget v1, v1, p1

    iget-object v2, p0, Lcom/google/zxing/common/reedsolomon/a;->j:[I

    aget v2, v2, p2

    add-int/2addr v1, v2

    iget v2, p0, Lcom/google/zxing/common/reedsolomon/a;->m:I

    add-int/lit8 v2, v2, -0x1

    rem-int/2addr v1, v2

    aget v0, v0, v1

    goto :goto_0
.end method
