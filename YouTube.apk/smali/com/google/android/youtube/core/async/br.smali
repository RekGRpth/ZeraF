.class final Lcom/google/android/youtube/core/async/br;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/youtube/core/async/n;


# instance fields
.field final synthetic a:Lcom/google/android/youtube/core/model/UserAuth;

.field final synthetic b:Landroid/app/Activity;

.field final synthetic c:Lcom/google/android/youtube/core/async/UserDelegator;


# direct methods
.method constructor <init>(Lcom/google/android/youtube/core/async/UserDelegator;Lcom/google/android/youtube/core/model/UserAuth;Landroid/app/Activity;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/youtube/core/async/br;->c:Lcom/google/android/youtube/core/async/UserDelegator;

    iput-object p2, p0, Lcom/google/android/youtube/core/async/br;->a:Lcom/google/android/youtube/core/model/UserAuth;

    iput-object p3, p0, Lcom/google/android/youtube/core/async/br;->b:Landroid/app/Activity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final bridge synthetic a(Ljava/lang/Object;Ljava/lang/Exception;)V
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/core/async/br;->c:Lcom/google/android/youtube/core/async/UserDelegator;

    invoke-static {v0, p2}, Lcom/google/android/youtube/core/async/UserDelegator;->a(Lcom/google/android/youtube/core/async/UserDelegator;Ljava/lang/Exception;)V

    return-void
.end method

.method public final bridge synthetic a(Ljava/lang/Object;Ljava/lang/Object;)V
    .locals 4

    check-cast p2, Lcom/google/android/youtube/core/model/Page;

    iget v0, p2, Lcom/google/android/youtube/core/model/Page;->totalResults:I

    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    iget-object v0, p0, Lcom/google/android/youtube/core/async/br;->c:Lcom/google/android/youtube/core/async/UserDelegator;

    const-string v1, "No +Page Delegate"

    invoke-static {v0, v1}, Lcom/google/android/youtube/core/async/UserDelegator;->a(Lcom/google/android/youtube/core/async/UserDelegator;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/google/android/youtube/core/async/br;->c:Lcom/google/android/youtube/core/async/UserDelegator;

    iget-object v1, p0, Lcom/google/android/youtube/core/async/br;->a:Lcom/google/android/youtube/core/model/UserAuth;

    invoke-static {v0, v1}, Lcom/google/android/youtube/core/async/UserDelegator;->a(Lcom/google/android/youtube/core/async/UserDelegator;Lcom/google/android/youtube/core/model/UserAuth;)V

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/google/android/youtube/core/async/br;->c:Lcom/google/android/youtube/core/async/UserDelegator;

    iget-object v1, p0, Lcom/google/android/youtube/core/async/br;->b:Landroid/app/Activity;

    iget-object v2, p0, Lcom/google/android/youtube/core/async/br;->a:Lcom/google/android/youtube/core/model/UserAuth;

    iget-object v3, p2, Lcom/google/android/youtube/core/model/Page;->entries:Ljava/util/List;

    invoke-static {v0, v1, v2, v3}, Lcom/google/android/youtube/core/async/UserDelegator;->a(Lcom/google/android/youtube/core/async/UserDelegator;Landroid/app/Activity;Lcom/google/android/youtube/core/model/UserAuth;Ljava/util/List;)V

    goto :goto_0
.end method
