.class final Lcom/google/android/youtube/core/async/bz;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/youtube/core/async/cc;


# instance fields
.field final synthetic a:Lcom/google/android/youtube/core/async/a;

.field final synthetic b:Lcom/google/android/youtube/core/async/by;


# direct methods
.method constructor <init>(Lcom/google/android/youtube/core/async/by;Lcom/google/android/youtube/core/async/a;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/youtube/core/async/bz;->b:Lcom/google/android/youtube/core/async/by;

    iput-object p2, p0, Lcom/google/android/youtube/core/async/bz;->a:Lcom/google/android/youtube/core/async/a;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a()V
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/core/async/bz;->b:Lcom/google/android/youtube/core/async/by;

    invoke-static {v0}, Lcom/google/android/youtube/core/async/by;->b(Lcom/google/android/youtube/core/async/by;)V

    return-void
.end method

.method public final a(Landroid/app/Activity;Lcom/google/android/youtube/core/model/UserAuth;)V
    .locals 4

    iget-object v0, p0, Lcom/google/android/youtube/core/async/bz;->b:Lcom/google/android/youtube/core/async/by;

    invoke-static {v0}, Lcom/google/android/youtube/core/async/by;->a(Lcom/google/android/youtube/core/async/by;)Ljava/util/List;

    move-result-object v1

    monitor-enter v1

    :try_start_0
    iget-object v0, p0, Lcom/google/android/youtube/core/async/bz;->a:Lcom/google/android/youtube/core/async/a;

    invoke-virtual {v0, p2}, Lcom/google/android/youtube/core/async/a;->a(Lcom/google/android/youtube/core/model/UserAuth;)V

    iget-object v0, p0, Lcom/google/android/youtube/core/async/bz;->b:Lcom/google/android/youtube/core/async/by;

    invoke-static {v0}, Lcom/google/android/youtube/core/async/by;->a(Lcom/google/android/youtube/core/async/by;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/youtube/core/async/bk;

    iget-object v3, p0, Lcom/google/android/youtube/core/async/bz;->b:Lcom/google/android/youtube/core/async/by;

    invoke-virtual {v3, p1, v0}, Lcom/google/android/youtube/core/async/by;->a(Landroid/app/Activity;Lcom/google/android/youtube/core/async/bk;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0

    :cond_0
    :try_start_1
    iget-object v0, p0, Lcom/google/android/youtube/core/async/bz;->b:Lcom/google/android/youtube/core/async/by;

    invoke-static {v0}, Lcom/google/android/youtube/core/async/by;->a(Lcom/google/android/youtube/core/async/by;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->clear()V

    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    return-void
.end method

.method public final a(Ljava/lang/String;Ljava/lang/Exception;)V
    .locals 3

    iget-object v0, p0, Lcom/google/android/youtube/core/async/bz;->b:Lcom/google/android/youtube/core/async/by;

    invoke-static {v0}, Lcom/google/android/youtube/core/async/by;->a(Lcom/google/android/youtube/core/async/by;)Ljava/util/List;

    move-result-object v1

    monitor-enter v1

    :try_start_0
    iget-object v0, p0, Lcom/google/android/youtube/core/async/bz;->b:Lcom/google/android/youtube/core/async/by;

    invoke-static {v0}, Lcom/google/android/youtube/core/async/by;->a(Lcom/google/android/youtube/core/async/by;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/youtube/core/async/bk;

    invoke-interface {v0, p1, p2}, Lcom/google/android/youtube/core/async/bk;->a(Ljava/lang/String;Ljava/lang/Exception;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0

    :cond_0
    :try_start_1
    iget-object v0, p0, Lcom/google/android/youtube/core/async/bz;->b:Lcom/google/android/youtube/core/async/by;

    invoke-static {v0}, Lcom/google/android/youtube/core/async/by;->a(Lcom/google/android/youtube/core/async/by;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->clear()V

    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    return-void
.end method
