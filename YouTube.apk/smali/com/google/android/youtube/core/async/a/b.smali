.class public final Lcom/google/android/youtube/core/async/a/b;
.super Ljava/lang/Object;
.source "SourceFile"


# direct methods
.method public static a(Lcom/google/android/youtube/core/client/bc;Ljava/lang/String;I)Lcom/google/android/youtube/core/async/a/c;
    .locals 3

    invoke-interface {p0}, Lcom/google/android/youtube/core/client/bc;->b()Lcom/google/android/youtube/core/async/au;

    move-result-object v0

    invoke-interface {p0}, Lcom/google/android/youtube/core/client/bc;->a()Lcom/google/android/youtube/core/async/GDataRequestFactory;

    move-result-object v1

    invoke-virtual {v1, p1}, Lcom/google/android/youtube/core/async/GDataRequestFactory;->i(Ljava/lang/String;)Lcom/google/android/youtube/core/async/GDataRequest;

    move-result-object v1

    new-instance v2, Lcom/google/android/youtube/core/async/a/g;

    invoke-direct {v2, v0, v1, p2}, Lcom/google/android/youtube/core/async/a/g;-><init>(Lcom/google/android/youtube/core/async/au;Lcom/google/android/youtube/core/async/GDataRequest;I)V

    return-object v2
.end method

.method public static a(Lcom/google/android/youtube/core/client/bc;Ljava/lang/String;Lcom/google/android/youtube/core/model/UserAuth;)Lcom/google/android/youtube/core/async/a/c;
    .locals 2

    invoke-static {p1}, Ljava/util/Collections;->singletonList(Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    const/4 v1, 0x0

    invoke-static {p0, v0, p2, v1}, Lcom/google/android/youtube/core/async/a/b;->a(Lcom/google/android/youtube/core/client/bc;Ljava/util/List;Lcom/google/android/youtube/core/model/UserAuth;I)Lcom/google/android/youtube/core/async/a/c;

    move-result-object v0

    return-object v0
.end method

.method public static a(Lcom/google/android/youtube/core/client/bc;Ljava/util/List;Lcom/google/android/youtube/core/model/UserAuth;I)Lcom/google/android/youtube/core/async/a/c;
    .locals 1

    new-instance v0, Lcom/google/android/youtube/core/async/a/d;

    invoke-direct {v0, p1, p0, p2, p3}, Lcom/google/android/youtube/core/async/a/d;-><init>(Ljava/util/List;Lcom/google/android/youtube/core/client/bc;Lcom/google/android/youtube/core/model/UserAuth;I)V

    return-object v0
.end method
