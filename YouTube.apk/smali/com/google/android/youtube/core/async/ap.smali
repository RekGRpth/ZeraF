.class public Lcom/google/android/youtube/core/async/ap;
.super Ljava/lang/Object;
.source "SourceFile"


# static fields
.field public static final b:Lcom/google/android/youtube/core/async/m;


# instance fields
.field private volatile a:I

.field public final c:Landroid/net/Uri;

.field public final d:Lcom/google/android/youtube/core/model/UserAuth;

.field public final e:Ljava/util/Map;

.field public final f:[B


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Lcom/google/android/youtube/core/async/aq;

    invoke-direct {v0}, Lcom/google/android/youtube/core/async/aq;-><init>()V

    sput-object v0, Lcom/google/android/youtube/core/async/ap;->b:Lcom/google/android/youtube/core/async/m;

    return-void
.end method

.method public constructor <init>(Landroid/net/Uri;Lcom/google/android/youtube/core/model/UserAuth;Ljava/util/Map;[B)V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const-string v0, "uri can\'t be null"

    invoke-static {p1, v0}, Lcom/google/android/youtube/core/utils/s;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/net/Uri;

    iput-object v0, p0, Lcom/google/android/youtube/core/async/ap;->c:Landroid/net/Uri;

    iput-object p2, p0, Lcom/google/android/youtube/core/async/ap;->d:Lcom/google/android/youtube/core/model/UserAuth;

    if-nez p3, :cond_0

    invoke-static {}, Ljava/util/Collections;->emptyMap()Ljava/util/Map;

    move-result-object v0

    :goto_0
    iput-object v0, p0, Lcom/google/android/youtube/core/async/ap;->e:Ljava/util/Map;

    iput-object p4, p0, Lcom/google/android/youtube/core/async/ap;->f:[B

    return-void

    :cond_0
    invoke-static {p3}, Ljava/util/Collections;->unmodifiableMap(Ljava/util/Map;)Ljava/util/Map;

    move-result-object v0

    goto :goto_0
.end method

.method protected static a(Lcom/google/android/youtube/core/async/ap;)[B
    .locals 4

    const/4 v3, 0x0

    const/4 v0, 0x0

    iget-object v1, p0, Lcom/google/android/youtube/core/async/ap;->f:[B

    if-eqz v1, :cond_0

    iget-object v0, p0, Lcom/google/android/youtube/core/async/ap;->f:[B

    array-length v1, v0

    new-array v0, v1, [B

    iget-object v2, p0, Lcom/google/android/youtube/core/async/ap;->f:[B

    invoke-static {v2, v3, v0, v3, v1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    :cond_0
    return-object v0
.end method

.method public static b(Landroid/net/Uri;Lcom/google/android/youtube/core/model/UserAuth;)Lcom/google/android/youtube/core/async/ap;
    .locals 2

    const/4 v1, 0x0

    const-string v0, "userAuth can\'t be null"

    invoke-static {p1, v0}, Lcom/google/android/youtube/core/utils/s;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    new-instance v0, Lcom/google/android/youtube/core/async/ap;

    invoke-direct {v0, p0, p1, v1, v1}, Lcom/google/android/youtube/core/async/ap;-><init>(Landroid/net/Uri;Lcom/google/android/youtube/core/model/UserAuth;Ljava/util/Map;[B)V

    return-object v0
.end method

.method public static d(Landroid/net/Uri;)Lcom/google/android/youtube/core/async/ap;
    .locals 2

    const/4 v1, 0x0

    new-instance v0, Lcom/google/android/youtube/core/async/ap;

    invoke-direct {v0, p0, v1, v1, v1}, Lcom/google/android/youtube/core/async/ap;-><init>(Landroid/net/Uri;Lcom/google/android/youtube/core/model/UserAuth;Ljava/util/Map;[B)V

    return-object v0
.end method


# virtual methods
.method public final b(Lcom/google/android/youtube/core/async/ap;)Z
    .locals 2

    iget-object v0, p1, Lcom/google/android/youtube/core/async/ap;->c:Landroid/net/Uri;

    invoke-virtual {v0}, Landroid/net/Uri;->getPath()Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/youtube/core/async/ap;->c:Landroid/net/Uri;

    invoke-virtual {v1}, Landroid/net/Uri;->getPath()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 4

    const/4 v0, 0x1

    const/4 v1, 0x0

    if-ne p0, p1, :cond_1

    :cond_0
    :goto_0
    return v0

    :cond_1
    instance-of v2, p1, Lcom/google/android/youtube/core/async/ap;

    if-nez v2, :cond_2

    move v0, v1

    goto :goto_0

    :cond_2
    check-cast p1, Lcom/google/android/youtube/core/async/ap;

    iget-object v2, p0, Lcom/google/android/youtube/core/async/ap;->c:Landroid/net/Uri;

    iget-object v3, p1, Lcom/google/android/youtube/core/async/ap;->c:Landroid/net/Uri;

    invoke-virtual {v2, v3}, Landroid/net/Uri;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    iget-object v2, p0, Lcom/google/android/youtube/core/async/ap;->d:Lcom/google/android/youtube/core/model/UserAuth;

    if-nez v2, :cond_4

    iget-object v2, p1, Lcom/google/android/youtube/core/async/ap;->d:Lcom/google/android/youtube/core/model/UserAuth;

    if-nez v2, :cond_3

    :goto_1
    iget-object v2, p0, Lcom/google/android/youtube/core/async/ap;->e:Ljava/util/Map;

    iget-object v3, p1, Lcom/google/android/youtube/core/async/ap;->e:Ljava/util/Map;

    invoke-virtual {v2, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    iget-object v2, p0, Lcom/google/android/youtube/core/async/ap;->f:[B

    iget-object v3, p1, Lcom/google/android/youtube/core/async/ap;->f:[B

    invoke-static {v2, v3}, Ljava/util/Arrays;->equals([B[B)Z

    move-result v2

    if-nez v2, :cond_0

    :cond_3
    move v0, v1

    goto :goto_0

    :cond_4
    iget-object v2, p0, Lcom/google/android/youtube/core/async/ap;->d:Lcom/google/android/youtube/core/model/UserAuth;

    iget-object v3, p1, Lcom/google/android/youtube/core/async/ap;->d:Lcom/google/android/youtube/core/model/UserAuth;

    invoke-virtual {v2, v3}, Lcom/google/android/youtube/core/model/UserAuth;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    goto :goto_1
.end method

.method public hashCode()I
    .locals 3

    const/4 v1, 0x0

    iget v0, p0, Lcom/google/android/youtube/core/async/ap;->a:I

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/youtube/core/async/ap;->c:Landroid/net/Uri;

    invoke-virtual {v0}, Landroid/net/Uri;->hashCode()I

    move-result v0

    add-int/lit16 v0, v0, 0x20f

    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lcom/google/android/youtube/core/async/ap;->d:Lcom/google/android/youtube/core/model/UserAuth;

    if-nez v0, :cond_1

    move v0, v1

    :goto_0
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/google/android/youtube/core/async/ap;->e:Ljava/util/Map;

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v2

    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/google/android/youtube/core/async/ap;->f:[B

    if-nez v2, :cond_2

    :goto_1
    add-int/2addr v0, v1

    iput v0, p0, Lcom/google/android/youtube/core/async/ap;->a:I

    :cond_0
    return v0

    :cond_1
    iget-object v0, p0, Lcom/google/android/youtube/core/async/ap;->d:Lcom/google/android/youtube/core/model/UserAuth;

    invoke-virtual {v0}, Lcom/google/android/youtube/core/model/UserAuth;->hashCode()I

    move-result v0

    goto :goto_0

    :cond_2
    iget-object v1, p0, Lcom/google/android/youtube/core/async/ap;->f:[B

    invoke-static {v1}, Ljava/util/Arrays;->hashCode([B)I

    move-result v1

    goto :goto_1
.end method

.method public toString()Ljava/lang/String;
    .locals 3

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "{uri=\'"

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/google/android/youtube/core/async/ap;->c:Landroid/net/Uri;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "\', userAuth=\'"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/youtube/core/async/ap;->d:Lcom/google/android/youtube/core/model/UserAuth;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "\', headers=\'"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/youtube/core/async/ap;->e:Ljava/util/Map;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "\', content=\'"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v0, p0, Lcom/google/android/youtube/core/async/ap;->f:[B

    if-eqz v0, :cond_0

    new-instance v0, Ljava/lang/String;

    iget-object v2, p0, Lcom/google/android/youtube/core/async/ap;->f:[B

    invoke-direct {v0, v2}, Ljava/lang/String;-><init>([B)V

    :goto_0
    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "\'}"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0

    :cond_0
    const-string v0, "null"

    goto :goto_0
.end method
