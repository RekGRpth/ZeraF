.class final Lcom/google/android/youtube/core/async/bf;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field private a:Lcom/google/android/youtube/core/async/n;

.field private b:Ljava/lang/Object;

.field private c:Ljava/lang/Object;

.field private d:Ljava/lang/Exception;

.field private e:Z


# direct methods
.method private constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(B)V
    .locals 0

    invoke-direct {p0}, Lcom/google/android/youtube/core/async/bf;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Lcom/google/android/youtube/core/async/n;Ljava/lang/Object;Ljava/lang/Exception;)V
    .locals 1

    iput-object p1, p0, Lcom/google/android/youtube/core/async/bf;->a:Lcom/google/android/youtube/core/async/n;

    iput-object p2, p0, Lcom/google/android/youtube/core/async/bf;->b:Ljava/lang/Object;

    iput-object p3, p0, Lcom/google/android/youtube/core/async/bf;->d:Ljava/lang/Exception;

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/youtube/core/async/bf;->c:Ljava/lang/Object;

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/youtube/core/async/bf;->e:Z

    return-void
.end method

.method public final a(Lcom/google/android/youtube/core/async/n;Ljava/lang/Object;Ljava/lang/Object;)V
    .locals 1

    iput-object p1, p0, Lcom/google/android/youtube/core/async/bf;->a:Lcom/google/android/youtube/core/async/n;

    iput-object p2, p0, Lcom/google/android/youtube/core/async/bf;->b:Ljava/lang/Object;

    iput-object p3, p0, Lcom/google/android/youtube/core/async/bf;->c:Ljava/lang/Object;

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/youtube/core/async/bf;->d:Ljava/lang/Exception;

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/youtube/core/async/bf;->e:Z

    return-void
.end method

.method public final run()V
    .locals 4

    const/4 v3, 0x0

    iget-boolean v0, p0, Lcom/google/android/youtube/core/async/bf;->e:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/youtube/core/async/bf;->a:Lcom/google/android/youtube/core/async/n;

    iget-object v1, p0, Lcom/google/android/youtube/core/async/bf;->b:Ljava/lang/Object;

    iget-object v2, p0, Lcom/google/android/youtube/core/async/bf;->c:Ljava/lang/Object;

    invoke-interface {v0, v1, v2}, Lcom/google/android/youtube/core/async/n;->a(Ljava/lang/Object;Ljava/lang/Object;)V

    :goto_0
    iput-object v3, p0, Lcom/google/android/youtube/core/async/bf;->a:Lcom/google/android/youtube/core/async/n;

    iput-object v3, p0, Lcom/google/android/youtube/core/async/bf;->b:Ljava/lang/Object;

    iput-object v3, p0, Lcom/google/android/youtube/core/async/bf;->c:Ljava/lang/Object;

    iput-object v3, p0, Lcom/google/android/youtube/core/async/bf;->d:Ljava/lang/Exception;

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/youtube/core/async/bf;->e:Z

    invoke-static {p0}, Lcom/google/android/youtube/core/async/be;->a(Lcom/google/android/youtube/core/async/bf;)V

    return-void

    :cond_0
    iget-object v0, p0, Lcom/google/android/youtube/core/async/bf;->a:Lcom/google/android/youtube/core/async/n;

    iget-object v1, p0, Lcom/google/android/youtube/core/async/bf;->b:Ljava/lang/Object;

    iget-object v2, p0, Lcom/google/android/youtube/core/async/bf;->d:Ljava/lang/Exception;

    invoke-interface {v0, v1, v2}, Lcom/google/android/youtube/core/async/n;->a(Ljava/lang/Object;Ljava/lang/Exception;)V

    goto :goto_0
.end method
