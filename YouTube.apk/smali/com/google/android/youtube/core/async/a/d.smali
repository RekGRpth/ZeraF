.class public final Lcom/google/android/youtube/core/async/a/d;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/youtube/core/async/a/c;


# instance fields
.field private final a:Lcom/google/android/youtube/core/client/bc;

.field private final b:Lcom/google/android/youtube/core/model/UserAuth;

.field private final c:Lcom/google/android/youtube/core/async/a/f;

.field private final d:Lcom/google/android/youtube/core/async/a/e;

.field private final e:Landroid/os/Handler;

.field private f:[Ljava/lang/String;

.field private g:Lcom/google/android/youtube/core/async/n;

.field private h:I

.field private i:I

.field private j:Lcom/google/android/youtube/core/async/p;


# direct methods
.method private constructor <init>(Ljava/util/List;Lcom/google/android/youtube/core/client/bc;Lcom/google/android/youtube/core/model/UserAuth;)V
    .locals 1

    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, p3, v0}, Lcom/google/android/youtube/core/async/a/d;-><init>(Ljava/util/List;Lcom/google/android/youtube/core/client/bc;Lcom/google/android/youtube/core/model/UserAuth;I)V

    return-void
.end method

.method public constructor <init>(Ljava/util/List;Lcom/google/android/youtube/core/client/bc;Lcom/google/android/youtube/core/model/UserAuth;I)V
    .locals 4

    const/4 v1, 0x0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    if-ltz p4, :cond_0

    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v0

    if-lt p4, v0, :cond_1

    :cond_0
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v0

    if-nez v0, :cond_2

    :cond_1
    const/4 v0, 0x1

    :goto_0
    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "startIndex="

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, p4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " must be >= 0 and < videoIds.size()="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Lcom/google/android/youtube/core/utils/s;->a(ZLjava/lang/Object;)V

    const-string v0, "gdataClient cannot be null"

    invoke-static {p2, v0}, Lcom/google/android/youtube/core/utils/s;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/youtube/core/client/bc;

    iput-object v0, p0, Lcom/google/android/youtube/core/async/a/d;->a:Lcom/google/android/youtube/core/client/bc;

    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v0

    new-array v0, v0, [Ljava/lang/String;

    invoke-interface {p1, v0}, Ljava/util/List;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Ljava/lang/String;

    iput-object v0, p0, Lcom/google/android/youtube/core/async/a/d;->f:[Ljava/lang/String;

    iput-object p3, p0, Lcom/google/android/youtube/core/async/a/d;->b:Lcom/google/android/youtube/core/model/UserAuth;

    new-instance v0, Lcom/google/android/youtube/core/async/a/f;

    invoke-direct {v0, p0, v1}, Lcom/google/android/youtube/core/async/a/f;-><init>(Lcom/google/android/youtube/core/async/a/d;B)V

    iput-object v0, p0, Lcom/google/android/youtube/core/async/a/d;->c:Lcom/google/android/youtube/core/async/a/f;

    new-instance v0, Lcom/google/android/youtube/core/async/a/e;

    invoke-direct {v0, p0, v1}, Lcom/google/android/youtube/core/async/a/e;-><init>(Lcom/google/android/youtube/core/async/a/d;B)V

    iput-object v0, p0, Lcom/google/android/youtube/core/async/a/d;->d:Lcom/google/android/youtube/core/async/a/e;

    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    iput-object v0, p0, Lcom/google/android/youtube/core/async/a/d;->e:Landroid/os/Handler;

    add-int/lit8 v0, p4, -0x1

    iput v0, p0, Lcom/google/android/youtube/core/async/a/d;->h:I

    add-int/lit8 v0, p4, -0x1

    iput v0, p0, Lcom/google/android/youtube/core/async/a/d;->i:I

    return-void

    :cond_2
    move v0, v1

    goto :goto_0
.end method

.method static synthetic a(Lcom/google/android/youtube/core/async/a/d;)I
    .locals 1

    iget v0, p0, Lcom/google/android/youtube/core/async/a/d;->i:I

    return v0
.end method

.method static synthetic a(Lcom/google/android/youtube/core/async/a/d;I)I
    .locals 0

    iput p1, p0, Lcom/google/android/youtube/core/async/a/d;->h:I

    return p1
.end method

.method static synthetic a(Lcom/google/android/youtube/core/async/a/d;Lcom/google/android/youtube/core/async/p;)Lcom/google/android/youtube/core/async/p;
    .locals 1

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/youtube/core/async/a/d;->j:Lcom/google/android/youtube/core/async/p;

    return-object v0
.end method

.method private a(I)V
    .locals 4

    const/4 v0, -0x1

    iget-object v1, p0, Lcom/google/android/youtube/core/async/a/d;->f:[Ljava/lang/String;

    array-length v1, v1

    invoke-static {p1, v1}, Ljava/lang/Math;->min(II)I

    move-result v1

    invoke-static {v0, v1}, Ljava/lang/Math;->max(II)I

    move-result v0

    iput v0, p0, Lcom/google/android/youtube/core/async/a/d;->i:I

    iget v0, p0, Lcom/google/android/youtube/core/async/a/d;->i:I

    if-ltz v0, :cond_0

    iget v0, p0, Lcom/google/android/youtube/core/async/a/d;->i:I

    iget-object v1, p0, Lcom/google/android/youtube/core/async/a/d;->f:[Ljava/lang/String;

    array-length v1, v1

    add-int/lit8 v1, v1, -0x1

    if-le v0, v1, :cond_1

    :cond_0
    iget-object v0, p0, Lcom/google/android/youtube/core/async/a/d;->e:Landroid/os/Handler;

    iget-object v1, p0, Lcom/google/android/youtube/core/async/a/d;->d:Lcom/google/android/youtube/core/async/a/e;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    :goto_0
    return-void

    :cond_1
    iget-object v0, p0, Lcom/google/android/youtube/core/async/a/d;->c:Lcom/google/android/youtube/core/async/a/f;

    invoke-static {v0}, Lcom/google/android/youtube/core/async/p;->a(Lcom/google/android/youtube/core/async/n;)Lcom/google/android/youtube/core/async/p;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/youtube/core/async/a/d;->j:Lcom/google/android/youtube/core/async/p;

    iget-object v0, p0, Lcom/google/android/youtube/core/async/a/d;->e:Landroid/os/Handler;

    iget-object v1, p0, Lcom/google/android/youtube/core/async/a/d;->j:Lcom/google/android/youtube/core/async/p;

    invoke-static {v0, v1}, Lcom/google/android/youtube/core/async/am;->a(Landroid/os/Handler;Lcom/google/android/youtube/core/async/n;)Lcom/google/android/youtube/core/async/am;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/youtube/core/async/a/d;->b:Lcom/google/android/youtube/core/model/UserAuth;

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/google/android/youtube/core/async/a/d;->a:Lcom/google/android/youtube/core/client/bc;

    iget-object v2, p0, Lcom/google/android/youtube/core/async/a/d;->f:[Ljava/lang/String;

    iget v3, p0, Lcom/google/android/youtube/core/async/a/d;->i:I

    aget-object v2, v2, v3

    iget-object v3, p0, Lcom/google/android/youtube/core/async/a/d;->b:Lcom/google/android/youtube/core/model/UserAuth;

    invoke-interface {v1, v2, v3, v0}, Lcom/google/android/youtube/core/client/bc;->a(Ljava/lang/String;Lcom/google/android/youtube/core/model/UserAuth;Lcom/google/android/youtube/core/async/n;)V

    goto :goto_0

    :cond_2
    iget-object v1, p0, Lcom/google/android/youtube/core/async/a/d;->a:Lcom/google/android/youtube/core/client/bc;

    iget-object v2, p0, Lcom/google/android/youtube/core/async/a/d;->f:[Ljava/lang/String;

    iget v3, p0, Lcom/google/android/youtube/core/async/a/d;->i:I

    aget-object v2, v2, v3

    invoke-interface {v1, v2, v0}, Lcom/google/android/youtube/core/client/bc;->a(Ljava/lang/String;Lcom/google/android/youtube/core/async/n;)V

    goto :goto_0
.end method

.method static synthetic b(Lcom/google/android/youtube/core/async/a/d;)Lcom/google/android/youtube/core/async/n;
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/core/async/a/d;->g:Lcom/google/android/youtube/core/async/n;

    return-object v0
.end method

.method private i()V
    .locals 2

    invoke-direct {p0}, Lcom/google/android/youtube/core/async/a/d;->j()V

    iget-object v0, p0, Lcom/google/android/youtube/core/async/a/d;->g:Lcom/google/android/youtube/core/async/n;

    const-string v1, "Callback must be set first"

    invoke-static {v0, v1}, Lcom/google/android/youtube/core/utils/s;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    return-void
.end method

.method private j()V
    .locals 2

    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/youtube/core/async/a/d;->e:Landroid/os/Handler;

    invoke-virtual {v1}, Landroid/os/Handler;->getLooper()Landroid/os/Looper;

    move-result-object v1

    invoke-virtual {v1}, Landroid/os/Looper;->getThread()Ljava/lang/Thread;

    move-result-object v1

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    const-string v1, "All iterator invocations must be done on the same thread"

    invoke-static {v0, v1}, Lcom/google/android/youtube/core/utils/s;->b(ZLjava/lang/Object;)V

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public final a()V
    .locals 1

    invoke-direct {p0}, Lcom/google/android/youtube/core/async/a/d;->i()V

    invoke-virtual {p0}, Lcom/google/android/youtube/core/async/a/d;->f()V

    iget v0, p0, Lcom/google/android/youtube/core/async/a/d;->h:I

    add-int/lit8 v0, v0, 0x1

    invoke-direct {p0, v0}, Lcom/google/android/youtube/core/async/a/d;->a(I)V

    return-void
.end method

.method public final a(Lcom/google/android/youtube/core/async/n;)V
    .locals 1

    invoke-direct {p0}, Lcom/google/android/youtube/core/async/a/d;->j()V

    const-string v0, "Callback cannot be null"

    invoke-static {p1, v0}, Lcom/google/android/youtube/core/utils/s;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/youtube/core/async/n;

    iput-object v0, p0, Lcom/google/android/youtube/core/async/a/d;->g:Lcom/google/android/youtube/core/async/n;

    return-void
.end method

.method public final b()V
    .locals 1

    invoke-direct {p0}, Lcom/google/android/youtube/core/async/a/d;->i()V

    invoke-virtual {p0}, Lcom/google/android/youtube/core/async/a/d;->f()V

    iget v0, p0, Lcom/google/android/youtube/core/async/a/d;->h:I

    add-int/lit8 v0, v0, -0x1

    invoke-direct {p0, v0}, Lcom/google/android/youtube/core/async/a/d;->a(I)V

    return-void
.end method

.method public final c()V
    .locals 1

    invoke-direct {p0}, Lcom/google/android/youtube/core/async/a/d;->i()V

    invoke-virtual {p0}, Lcom/google/android/youtube/core/async/a/d;->f()V

    iget v0, p0, Lcom/google/android/youtube/core/async/a/d;->i:I

    invoke-direct {p0, v0}, Lcom/google/android/youtube/core/async/a/d;->a(I)V

    return-void
.end method

.method public final d()Z
    .locals 2

    invoke-direct {p0}, Lcom/google/android/youtube/core/async/a/d;->j()V

    iget v0, p0, Lcom/google/android/youtube/core/async/a/d;->h:I

    iget-object v1, p0, Lcom/google/android/youtube/core/async/a/d;->f:[Ljava/lang/String;

    array-length v1, v1

    add-int/lit8 v1, v1, -0x1

    if-ge v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final e()Z
    .locals 1

    invoke-direct {p0}, Lcom/google/android/youtube/core/async/a/d;->j()V

    iget v0, p0, Lcom/google/android/youtube/core/async/a/d;->h:I

    if-lez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final f()V
    .locals 2

    const/4 v1, 0x0

    invoke-direct {p0}, Lcom/google/android/youtube/core/async/a/d;->j()V

    iget-object v0, p0, Lcom/google/android/youtube/core/async/a/d;->j:Lcom/google/android/youtube/core/async/p;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/youtube/core/async/a/d;->j:Lcom/google/android/youtube/core/async/p;

    invoke-virtual {v0}, Lcom/google/android/youtube/core/async/p;->a()V

    iput-object v1, p0, Lcom/google/android/youtube/core/async/a/d;->j:Lcom/google/android/youtube/core/async/p;

    :cond_0
    iget-object v0, p0, Lcom/google/android/youtube/core/async/a/d;->e:Landroid/os/Handler;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacksAndMessages(Ljava/lang/Object;)V

    return-void
.end method

.method public final synthetic g()Lcom/google/android/youtube/core/async/a/a;
    .locals 4

    new-instance v0, Lcom/google/android/youtube/core/async/a/d;

    iget-object v1, p0, Lcom/google/android/youtube/core/async/a/d;->f:[Ljava/lang/String;

    invoke-static {v1}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/youtube/core/async/a/d;->a:Lcom/google/android/youtube/core/client/bc;

    iget-object v3, p0, Lcom/google/android/youtube/core/async/a/d;->b:Lcom/google/android/youtube/core/model/UserAuth;

    invoke-direct {v0, v1, v2, v3}, Lcom/google/android/youtube/core/async/a/d;-><init>(Ljava/util/List;Lcom/google/android/youtube/core/client/bc;Lcom/google/android/youtube/core/model/UserAuth;)V

    return-object v0
.end method

.method public final h()I
    .locals 1

    invoke-direct {p0}, Lcom/google/android/youtube/core/async/a/d;->i()V

    iget v0, p0, Lcom/google/android/youtube/core/async/a/d;->i:I

    return v0
.end method
