.class public final Lcom/google/android/youtube/core/async/w;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/youtube/core/async/au;


# instance fields
.field private final a:J

.field private final b:Lcom/google/android/youtube/core/async/au;

.field private final c:Ljava/util/concurrent/atomic/AtomicLong;

.field private final d:Lcom/google/android/youtube/core/utils/e;

.field private final e:Ljava/util/concurrent/ScheduledExecutorService;


# direct methods
.method public constructor <init>(Lcom/google/android/youtube/core/async/au;Lcom/google/android/youtube/core/utils/e;Ljava/util/concurrent/atomic/AtomicLong;JLjava/util/concurrent/ScheduledExecutorService;)V
    .locals 4

    const-wide/16 v2, 0x1388

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const-string v0, "target may not be null"

    invoke-static {p1, v0}, Lcom/google/android/youtube/core/utils/s;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/youtube/core/async/au;

    iput-object v0, p0, Lcom/google/android/youtube/core/async/w;->b:Lcom/google/android/youtube/core/async/au;

    const-string v0, "baseTime may not be null"

    invoke-static {p3, v0}, Lcom/google/android/youtube/core/utils/s;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/concurrent/atomic/AtomicLong;

    iput-object v0, p0, Lcom/google/android/youtube/core/async/w;->c:Ljava/util/concurrent/atomic/AtomicLong;

    const-wide/16 v0, 0x0

    cmp-long v0, v2, v0

    if-lez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    const-string v1, "delay cannot be negative or zero"

    invoke-static {v0, v1}, Lcom/google/android/youtube/core/utils/s;->a(ZLjava/lang/Object;)V

    iput-wide v2, p0, Lcom/google/android/youtube/core/async/w;->a:J

    const-string v0, "clock may not be null"

    invoke-static {p2, v0}, Lcom/google/android/youtube/core/utils/s;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/youtube/core/utils/e;

    iput-object v0, p0, Lcom/google/android/youtube/core/async/w;->d:Lcom/google/android/youtube/core/utils/e;

    const-string v0, "scheduler may not be null"

    invoke-static {p6, v0}, Lcom/google/android/youtube/core/utils/s;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/concurrent/ScheduledExecutorService;

    iput-object v0, p0, Lcom/google/android/youtube/core/async/w;->e:Ljava/util/concurrent/ScheduledExecutorService;

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method static synthetic a(Lcom/google/android/youtube/core/async/w;)Lcom/google/android/youtube/core/async/au;
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/core/async/w;->b:Lcom/google/android/youtube/core/async/au;

    return-object v0
.end method


# virtual methods
.method public final a(Ljava/lang/Object;Lcom/google/android/youtube/core/async/n;)V
    .locals 6

    const-wide/16 v2, 0x0

    iget-object v0, p0, Lcom/google/android/youtube/core/async/w;->d:Lcom/google/android/youtube/core/utils/e;

    invoke-interface {v0}, Lcom/google/android/youtube/core/utils/e;->a()J

    move-result-wide v0

    iget-object v4, p0, Lcom/google/android/youtube/core/async/w;->c:Ljava/util/concurrent/atomic/AtomicLong;

    invoke-virtual {v4}, Ljava/util/concurrent/atomic/AtomicLong;->get()J

    move-result-wide v4

    sub-long/2addr v0, v4

    iget-wide v4, p0, Lcom/google/android/youtube/core/async/w;->a:J

    cmp-long v4, v0, v4

    if-ltz v4, :cond_0

    move-wide v0, v2

    :goto_0
    cmp-long v2, v0, v2

    if-lez v2, :cond_1

    iget-object v2, p0, Lcom/google/android/youtube/core/async/w;->e:Ljava/util/concurrent/ScheduledExecutorService;

    new-instance v3, Lcom/google/android/youtube/core/async/x;

    invoke-direct {v3, p0, p1, p2}, Lcom/google/android/youtube/core/async/x;-><init>(Lcom/google/android/youtube/core/async/w;Ljava/lang/Object;Lcom/google/android/youtube/core/async/n;)V

    sget-object v4, Ljava/util/concurrent/TimeUnit;->MILLISECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-interface {v2, v3, v0, v1, v4}, Ljava/util/concurrent/ScheduledExecutorService;->schedule(Ljava/lang/Runnable;JLjava/util/concurrent/TimeUnit;)Ljava/util/concurrent/ScheduledFuture;

    :goto_1
    return-void

    :cond_0
    iget-wide v4, p0, Lcom/google/android/youtube/core/async/w;->a:J

    sub-long v0, v4, v0

    goto :goto_0

    :cond_1
    iget-object v0, p0, Lcom/google/android/youtube/core/async/w;->b:Lcom/google/android/youtube/core/async/au;

    invoke-interface {v0, p1, p2}, Lcom/google/android/youtube/core/async/au;->a(Ljava/lang/Object;Lcom/google/android/youtube/core/async/n;)V

    goto :goto_1
.end method
