.class final Lcom/google/android/youtube/core/async/bb;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/youtube/core/async/n;


# instance fields
.field final synthetic a:Lcom/google/android/youtube/core/async/az;

.field private final b:Lcom/google/android/youtube/core/async/n;

.field private c:Z


# direct methods
.method public constructor <init>(Lcom/google/android/youtube/core/async/az;Lcom/google/android/youtube/core/async/n;Z)V
    .locals 1

    iput-object p1, p0, Lcom/google/android/youtube/core/async/bb;->a:Lcom/google/android/youtube/core/async/az;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    invoke-static {p2}, Lcom/google/android/youtube/core/utils/s;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/youtube/core/async/n;

    iput-object v0, p0, Lcom/google/android/youtube/core/async/bb;->b:Lcom/google/android/youtube/core/async/n;

    iput-boolean p3, p0, Lcom/google/android/youtube/core/async/bb;->c:Z

    return-void
.end method


# virtual methods
.method public final bridge synthetic a(Ljava/lang/Object;Ljava/lang/Exception;)V
    .locals 1

    check-cast p1, Landroid/net/Uri;

    iget-object v0, p0, Lcom/google/android/youtube/core/async/bb;->b:Lcom/google/android/youtube/core/async/n;

    invoke-interface {v0, p1, p2}, Lcom/google/android/youtube/core/async/n;->a(Ljava/lang/Object;Ljava/lang/Exception;)V

    return-void
.end method

.method public final synthetic a(Ljava/lang/Object;Ljava/lang/Object;)V
    .locals 4

    check-cast p1, Landroid/net/Uri;

    check-cast p2, Ljava/lang/Long;

    iget-boolean v0, p0, Lcom/google/android/youtube/core/async/bb;->c:Z

    if-eqz v0, :cond_0

    invoke-virtual {p2}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-gtz v0, :cond_0

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/youtube/core/async/bb;->c:Z

    iget-object v0, p0, Lcom/google/android/youtube/core/async/bb;->a:Lcom/google/android/youtube/core/async/az;

    const/4 v1, 0x1

    invoke-static {v0, v1}, Lcom/google/android/youtube/core/async/az;->a(Lcom/google/android/youtube/core/async/az;Z)Z

    iget-object v0, p0, Lcom/google/android/youtube/core/async/bb;->a:Lcom/google/android/youtube/core/async/az;

    invoke-static {v0}, Lcom/google/android/youtube/core/async/az;->a(Lcom/google/android/youtube/core/async/az;)Lcom/google/android/youtube/core/async/au;

    move-result-object v0

    invoke-interface {v0, p1, p0}, Lcom/google/android/youtube/core/async/au;->a(Ljava/lang/Object;Lcom/google/android/youtube/core/async/n;)V

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/google/android/youtube/core/async/bb;->b:Lcom/google/android/youtube/core/async/n;

    invoke-interface {v0, p1, p2}, Lcom/google/android/youtube/core/async/n;->a(Ljava/lang/Object;Ljava/lang/Object;)V

    goto :goto_0
.end method
