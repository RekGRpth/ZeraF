.class public final Lcom/google/android/youtube/core/async/a/g;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/youtube/core/async/a/c;


# instance fields
.field private final a:Lcom/google/android/youtube/core/async/au;

.field private final b:Ljava/util/List;

.field private final c:Lcom/google/android/youtube/core/async/GDataRequest;

.field private final d:Lcom/google/android/youtube/core/async/a/j;

.field private final e:Lcom/google/android/youtube/core/async/a/i;

.field private final f:Landroid/os/Handler;

.field private g:Lcom/google/android/youtube/core/async/n;

.field private h:Lcom/google/android/youtube/core/async/GDataRequest;

.field private i:Lcom/google/android/youtube/core/async/p;

.field private j:I

.field private k:I


# direct methods
.method private constructor <init>(Lcom/google/android/youtube/core/async/au;Lcom/google/android/youtube/core/async/GDataRequest;)V
    .locals 1

    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/google/android/youtube/core/async/a/g;-><init>(Lcom/google/android/youtube/core/async/au;Lcom/google/android/youtube/core/async/GDataRequest;I)V

    return-void
.end method

.method public constructor <init>(Lcom/google/android/youtube/core/async/au;Lcom/google/android/youtube/core/async/GDataRequest;I)V
    .locals 4

    const/4 v1, 0x0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    if-ltz p3, :cond_0

    const/4 v0, 0x1

    :goto_0
    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "startIndex="

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " must be >= 0"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Lcom/google/android/youtube/core/utils/s;->a(ZLjava/lang/Object;)V

    const-string v0, "firstPageRequest cannot be null"

    invoke-static {p2, v0}, Lcom/google/android/youtube/core/utils/s;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/youtube/core/async/GDataRequest;

    iput-object v0, p0, Lcom/google/android/youtube/core/async/a/g;->c:Lcom/google/android/youtube/core/async/GDataRequest;

    iput-object p2, p0, Lcom/google/android/youtube/core/async/a/g;->h:Lcom/google/android/youtube/core/async/GDataRequest;

    const-string v0, "requester cannot be null"

    invoke-static {p1, v0}, Lcom/google/android/youtube/core/utils/s;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/youtube/core/async/au;

    iput-object v0, p0, Lcom/google/android/youtube/core/async/a/g;->a:Lcom/google/android/youtube/core/async/au;

    new-instance v0, Lcom/google/android/youtube/core/async/a/j;

    invoke-direct {v0, p0, v1}, Lcom/google/android/youtube/core/async/a/j;-><init>(Lcom/google/android/youtube/core/async/a/g;B)V

    iput-object v0, p0, Lcom/google/android/youtube/core/async/a/g;->d:Lcom/google/android/youtube/core/async/a/j;

    new-instance v0, Lcom/google/android/youtube/core/async/a/i;

    invoke-direct {v0, p0, v1}, Lcom/google/android/youtube/core/async/a/i;-><init>(Lcom/google/android/youtube/core/async/a/g;B)V

    iput-object v0, p0, Lcom/google/android/youtube/core/async/a/g;->e:Lcom/google/android/youtube/core/async/a/i;

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/android/youtube/core/async/a/g;->b:Ljava/util/List;

    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    iput-object v0, p0, Lcom/google/android/youtube/core/async/a/g;->f:Landroid/os/Handler;

    add-int/lit8 v0, p3, -0x1

    iput v0, p0, Lcom/google/android/youtube/core/async/a/g;->j:I

    add-int/lit8 v0, p3, -0x1

    iput v0, p0, Lcom/google/android/youtube/core/async/a/g;->k:I

    return-void

    :cond_0
    move v0, v1

    goto :goto_0
.end method

.method static synthetic a(Lcom/google/android/youtube/core/async/a/g;)I
    .locals 1

    iget v0, p0, Lcom/google/android/youtube/core/async/a/g;->k:I

    return v0
.end method

.method static synthetic a(Lcom/google/android/youtube/core/async/a/g;I)I
    .locals 0

    iput p1, p0, Lcom/google/android/youtube/core/async/a/g;->j:I

    return p1
.end method

.method static synthetic a(Lcom/google/android/youtube/core/async/a/g;Lcom/google/android/youtube/core/async/GDataRequest;)Lcom/google/android/youtube/core/async/GDataRequest;
    .locals 0

    iput-object p1, p0, Lcom/google/android/youtube/core/async/a/g;->h:Lcom/google/android/youtube/core/async/GDataRequest;

    return-object p1
.end method

.method static synthetic a(Lcom/google/android/youtube/core/async/a/g;Lcom/google/android/youtube/core/async/p;)Lcom/google/android/youtube/core/async/p;
    .locals 1

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/youtube/core/async/a/g;->i:Lcom/google/android/youtube/core/async/p;

    return-object v0
.end method

.method private a(I)V
    .locals 2

    iget-object v0, p0, Lcom/google/android/youtube/core/async/a/g;->h:Lcom/google/android/youtube/core/async/GDataRequest;

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/google/android/youtube/core/async/a/g;->b:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    :goto_0
    const/4 v1, -0x1

    invoke-static {p1, v0}, Ljava/lang/Math;->min(II)I

    move-result v0

    invoke-static {v1, v0}, Ljava/lang/Math;->max(II)I

    move-result v0

    iput v0, p0, Lcom/google/android/youtube/core/async/a/g;->k:I

    iget v0, p0, Lcom/google/android/youtube/core/async/a/g;->k:I

    iget-object v1, p0, Lcom/google/android/youtube/core/async/a/g;->b:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    if-lt v0, v1, :cond_0

    iget-object v0, p0, Lcom/google/android/youtube/core/async/a/g;->h:Lcom/google/android/youtube/core/async/GDataRequest;

    if-nez v0, :cond_2

    :cond_0
    iget-object v0, p0, Lcom/google/android/youtube/core/async/a/g;->f:Landroid/os/Handler;

    iget-object v1, p0, Lcom/google/android/youtube/core/async/a/g;->d:Lcom/google/android/youtube/core/async/a/j;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    :goto_1
    return-void

    :cond_1
    const v0, 0x7fffffff

    goto :goto_0

    :cond_2
    invoke-direct {p0}, Lcom/google/android/youtube/core/async/a/g;->k()V

    goto :goto_1
.end method

.method static synthetic b(Lcom/google/android/youtube/core/async/a/g;)Ljava/util/List;
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/core/async/a/g;->b:Ljava/util/List;

    return-object v0
.end method

.method static synthetic c(Lcom/google/android/youtube/core/async/a/g;)Lcom/google/android/youtube/core/async/n;
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/core/async/a/g;->g:Lcom/google/android/youtube/core/async/n;

    return-object v0
.end method

.method static synthetic d(Lcom/google/android/youtube/core/async/a/g;)Lcom/google/android/youtube/core/async/GDataRequest;
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/core/async/a/g;->h:Lcom/google/android/youtube/core/async/GDataRequest;

    return-object v0
.end method

.method static synthetic e(Lcom/google/android/youtube/core/async/a/g;)V
    .locals 0

    invoke-direct {p0}, Lcom/google/android/youtube/core/async/a/g;->k()V

    return-void
.end method

.method static synthetic f(Lcom/google/android/youtube/core/async/a/g;)Lcom/google/android/youtube/core/async/a/j;
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/core/async/a/g;->d:Lcom/google/android/youtube/core/async/a/j;

    return-object v0
.end method

.method static synthetic g(Lcom/google/android/youtube/core/async/a/g;)Landroid/os/Handler;
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/core/async/a/g;->f:Landroid/os/Handler;

    return-object v0
.end method

.method private i()V
    .locals 2

    invoke-direct {p0}, Lcom/google/android/youtube/core/async/a/g;->j()V

    iget-object v0, p0, Lcom/google/android/youtube/core/async/a/g;->g:Lcom/google/android/youtube/core/async/n;

    const-string v1, "Callback must be set first"

    invoke-static {v0, v1}, Lcom/google/android/youtube/core/utils/s;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    return-void
.end method

.method private j()V
    .locals 2

    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/youtube/core/async/a/g;->f:Landroid/os/Handler;

    invoke-virtual {v1}, Landroid/os/Handler;->getLooper()Landroid/os/Looper;

    move-result-object v1

    invoke-virtual {v1}, Landroid/os/Looper;->getThread()Ljava/lang/Thread;

    move-result-object v1

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    const-string v1, "All iterator invocations must be done on the same thread"

    invoke-static {v0, v1}, Lcom/google/android/youtube/core/utils/s;->b(ZLjava/lang/Object;)V

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private k()V
    .locals 4

    iget-object v0, p0, Lcom/google/android/youtube/core/async/a/g;->e:Lcom/google/android/youtube/core/async/a/i;

    invoke-static {v0}, Lcom/google/android/youtube/core/async/p;->a(Lcom/google/android/youtube/core/async/n;)Lcom/google/android/youtube/core/async/p;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/youtube/core/async/a/g;->i:Lcom/google/android/youtube/core/async/p;

    iget-object v0, p0, Lcom/google/android/youtube/core/async/a/g;->a:Lcom/google/android/youtube/core/async/au;

    iget-object v1, p0, Lcom/google/android/youtube/core/async/a/g;->h:Lcom/google/android/youtube/core/async/GDataRequest;

    iget-object v2, p0, Lcom/google/android/youtube/core/async/a/g;->f:Landroid/os/Handler;

    iget-object v3, p0, Lcom/google/android/youtube/core/async/a/g;->i:Lcom/google/android/youtube/core/async/p;

    invoke-static {v2, v3}, Lcom/google/android/youtube/core/async/am;->a(Landroid/os/Handler;Lcom/google/android/youtube/core/async/n;)Lcom/google/android/youtube/core/async/am;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Lcom/google/android/youtube/core/async/au;->a(Ljava/lang/Object;Lcom/google/android/youtube/core/async/n;)V

    return-void
.end method


# virtual methods
.method public final a()V
    .locals 1

    invoke-direct {p0}, Lcom/google/android/youtube/core/async/a/g;->i()V

    invoke-virtual {p0}, Lcom/google/android/youtube/core/async/a/g;->f()V

    iget v0, p0, Lcom/google/android/youtube/core/async/a/g;->j:I

    add-int/lit8 v0, v0, 0x1

    invoke-direct {p0, v0}, Lcom/google/android/youtube/core/async/a/g;->a(I)V

    return-void
.end method

.method public final a(Lcom/google/android/youtube/core/async/n;)V
    .locals 1

    invoke-direct {p0}, Lcom/google/android/youtube/core/async/a/g;->j()V

    const-string v0, "Callback cannot be null"

    invoke-static {p1, v0}, Lcom/google/android/youtube/core/utils/s;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/youtube/core/async/n;

    iput-object v0, p0, Lcom/google/android/youtube/core/async/a/g;->g:Lcom/google/android/youtube/core/async/n;

    return-void
.end method

.method public final b()V
    .locals 1

    invoke-direct {p0}, Lcom/google/android/youtube/core/async/a/g;->i()V

    invoke-virtual {p0}, Lcom/google/android/youtube/core/async/a/g;->f()V

    iget v0, p0, Lcom/google/android/youtube/core/async/a/g;->j:I

    add-int/lit8 v0, v0, -0x1

    invoke-direct {p0, v0}, Lcom/google/android/youtube/core/async/a/g;->a(I)V

    return-void
.end method

.method public final c()V
    .locals 1

    invoke-direct {p0}, Lcom/google/android/youtube/core/async/a/g;->i()V

    invoke-virtual {p0}, Lcom/google/android/youtube/core/async/a/g;->f()V

    iget v0, p0, Lcom/google/android/youtube/core/async/a/g;->k:I

    invoke-direct {p0, v0}, Lcom/google/android/youtube/core/async/a/g;->a(I)V

    return-void
.end method

.method public final d()Z
    .locals 2

    invoke-direct {p0}, Lcom/google/android/youtube/core/async/a/g;->j()V

    iget v0, p0, Lcom/google/android/youtube/core/async/a/g;->j:I

    iget-object v1, p0, Lcom/google/android/youtube/core/async/a/g;->b:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    if-lt v0, v1, :cond_0

    iget-object v0, p0, Lcom/google/android/youtube/core/async/a/g;->h:Lcom/google/android/youtube/core/async/GDataRequest;

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final e()Z
    .locals 1

    invoke-direct {p0}, Lcom/google/android/youtube/core/async/a/g;->j()V

    iget v0, p0, Lcom/google/android/youtube/core/async/a/g;->j:I

    if-lez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final f()V
    .locals 2

    const/4 v1, 0x0

    invoke-direct {p0}, Lcom/google/android/youtube/core/async/a/g;->j()V

    iget-object v0, p0, Lcom/google/android/youtube/core/async/a/g;->i:Lcom/google/android/youtube/core/async/p;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/youtube/core/async/a/g;->i:Lcom/google/android/youtube/core/async/p;

    invoke-virtual {v0}, Lcom/google/android/youtube/core/async/p;->a()V

    iput-object v1, p0, Lcom/google/android/youtube/core/async/a/g;->i:Lcom/google/android/youtube/core/async/p;

    :cond_0
    iget-object v0, p0, Lcom/google/android/youtube/core/async/a/g;->f:Landroid/os/Handler;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacksAndMessages(Ljava/lang/Object;)V

    return-void
.end method

.method public final synthetic g()Lcom/google/android/youtube/core/async/a/a;
    .locals 3

    new-instance v0, Lcom/google/android/youtube/core/async/a/g;

    iget-object v1, p0, Lcom/google/android/youtube/core/async/a/g;->a:Lcom/google/android/youtube/core/async/au;

    iget-object v2, p0, Lcom/google/android/youtube/core/async/a/g;->c:Lcom/google/android/youtube/core/async/GDataRequest;

    invoke-direct {v0, v1, v2}, Lcom/google/android/youtube/core/async/a/g;-><init>(Lcom/google/android/youtube/core/async/au;Lcom/google/android/youtube/core/async/GDataRequest;)V

    return-object v0
.end method

.method public final h()I
    .locals 1

    invoke-direct {p0}, Lcom/google/android/youtube/core/async/a/g;->i()V

    iget v0, p0, Lcom/google/android/youtube/core/async/a/g;->k:I

    return v0
.end method
