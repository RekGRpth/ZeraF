.class public final Lcom/google/android/youtube/core/converter/http/cz;
.super Lcom/google/android/youtube/core/converter/http/bc;
.source "SourceFile"


# instance fields
.field private final c:Lcom/google/android/youtube/core/converter/d;


# direct methods
.method public constructor <init>(Lcom/google/android/youtube/core/converter/m;)V
    .locals 5

    invoke-direct {p0, p1}, Lcom/google/android/youtube/core/converter/http/bc;-><init>(Lcom/google/android/youtube/core/converter/m;)V

    new-instance v0, Lcom/google/android/youtube/core/converter/e;

    invoke-direct {v0}, Lcom/google/android/youtube/core/converter/e;-><init>()V

    const-string v1, "/feed/entry"

    const-string v2, "/feed"

    new-instance v3, Lcom/google/android/youtube/core/converter/http/de;

    invoke-direct {v3, p0}, Lcom/google/android/youtube/core/converter/http/de;-><init>(Lcom/google/android/youtube/core/converter/http/cz;)V

    invoke-virtual {v0, v2, v3}, Lcom/google/android/youtube/core/converter/e;->a(Ljava/lang/String;Lcom/google/android/youtube/core/converter/q;)Lcom/google/android/youtube/core/converter/e;

    move-result-object v2

    new-instance v3, Lcom/google/android/youtube/core/converter/http/dd;

    invoke-direct {v3, p0}, Lcom/google/android/youtube/core/converter/http/dd;-><init>(Lcom/google/android/youtube/core/converter/http/cz;)V

    invoke-virtual {v2, v1, v3}, Lcom/google/android/youtube/core/converter/e;->a(Ljava/lang/String;Lcom/google/android/youtube/core/converter/q;)Lcom/google/android/youtube/core/converter/e;

    move-result-object v2

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "/category"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    new-instance v4, Lcom/google/android/youtube/core/converter/http/dc;

    invoke-direct {v4, p0}, Lcom/google/android/youtube/core/converter/http/dc;-><init>(Lcom/google/android/youtube/core/converter/http/cz;)V

    invoke-virtual {v2, v3, v4}, Lcom/google/android/youtube/core/converter/e;->a(Ljava/lang/String;Lcom/google/android/youtube/core/converter/q;)Lcom/google/android/youtube/core/converter/e;

    move-result-object v2

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "/yt:connectedAccount"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    new-instance v4, Lcom/google/android/youtube/core/converter/http/db;

    invoke-direct {v4, p0}, Lcom/google/android/youtube/core/converter/http/db;-><init>(Lcom/google/android/youtube/core/converter/http/cz;)V

    invoke-virtual {v2, v3, v4}, Lcom/google/android/youtube/core/converter/e;->a(Ljava/lang/String;Lcom/google/android/youtube/core/converter/q;)Lcom/google/android/youtube/core/converter/e;

    move-result-object v2

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v3, "/yt:connectedAccount/yt:accessControl"

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    new-instance v3, Lcom/google/android/youtube/core/converter/http/da;

    invoke-direct {v3, p0}, Lcom/google/android/youtube/core/converter/http/da;-><init>(Lcom/google/android/youtube/core/converter/http/cz;)V

    invoke-virtual {v2, v1, v3}, Lcom/google/android/youtube/core/converter/e;->a(Ljava/lang/String;Lcom/google/android/youtube/core/converter/q;)Lcom/google/android/youtube/core/converter/e;

    invoke-virtual {v0}, Lcom/google/android/youtube/core/converter/e;->a()Lcom/google/android/youtube/core/converter/d;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/youtube/core/converter/http/cz;->c:Lcom/google/android/youtube/core/converter/d;

    return-void
.end method


# virtual methods
.method protected final a()Lcom/google/android/youtube/core/converter/d;
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/core/converter/http/cz;->c:Lcom/google/android/youtube/core/converter/d;

    return-object v0
.end method
