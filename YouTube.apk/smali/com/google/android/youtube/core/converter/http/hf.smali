.class final Lcom/google/android/youtube/core/converter/http/hf;
.super Lcom/google/android/youtube/core/converter/n;
.source "SourceFile"


# instance fields
.field final synthetic a:Lcom/google/android/youtube/core/converter/http/hc;


# direct methods
.method constructor <init>(Lcom/google/android/youtube/core/converter/http/hc;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/youtube/core/converter/http/hf;->a:Lcom/google/android/youtube/core/converter/http/hc;

    invoke-direct {p0}, Lcom/google/android/youtube/core/converter/n;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Lcom/google/android/youtube/core/utils/ae;Lorg/xml/sax/Attributes;Ljava/lang/String;)V
    .locals 3

    const-class v0, Lcom/google/android/youtube/core/model/am;

    invoke-virtual {p1, v0}, Lcom/google/android/youtube/core/utils/ae;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/youtube/core/model/am;

    iget-object v1, p0, Lcom/google/android/youtube/core/converter/http/hf;->a:Lcom/google/android/youtube/core/converter/http/hc;

    const-string v2, "event"

    invoke-interface {p2, v2}, Lorg/xml/sax/Attributes;->getValue(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/google/android/youtube/core/converter/http/hc;->a(Lcom/google/android/youtube/core/converter/http/hc;Ljava/lang/String;)Lcom/google/android/youtube/core/model/VmapAdBreak$TrackingEventType;

    move-result-object v1

    if-nez v1, :cond_0

    :goto_0
    return-void

    :cond_0
    :try_start_0
    invoke-virtual {p3}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/google/android/youtube/core/utils/Util;->d(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/google/android/youtube/core/model/am;->a(Lcom/google/android/youtube/core/model/VmapAdBreak$TrackingEventType;Landroid/net/Uri;)Lcom/google/android/youtube/core/model/am;
    :try_end_0
    .catch Ljava/net/MalformedURLException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v0

    const-string v0, "Badly formed AdBreak tracking uri - ignoring"

    invoke-static {v0}, Lcom/google/android/youtube/core/L;->c(Ljava/lang/String;)V

    goto :goto_0
.end method
