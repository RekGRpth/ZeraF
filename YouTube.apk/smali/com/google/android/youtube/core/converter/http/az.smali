.class final Lcom/google/android/youtube/core/converter/http/az;
.super Lcom/google/android/youtube/core/converter/n;
.source "SourceFile"


# instance fields
.field final synthetic a:Lcom/google/android/youtube/core/converter/http/ar;


# direct methods
.method constructor <init>(Lcom/google/android/youtube/core/converter/http/ar;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/youtube/core/converter/http/az;->a:Lcom/google/android/youtube/core/converter/http/ar;

    invoke-direct {p0}, Lcom/google/android/youtube/core/converter/n;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Lcom/google/android/youtube/core/utils/ae;Lorg/xml/sax/Attributes;)V
    .locals 1

    new-instance v0, Lcom/google/android/youtube/core/model/Event$Builder;

    invoke-direct {v0}, Lcom/google/android/youtube/core/model/Event$Builder;-><init>()V

    invoke-virtual {p1, v0}, Lcom/google/android/youtube/core/utils/ae;->offer(Ljava/lang/Object;)Z

    return-void
.end method

.method public final a(Lcom/google/android/youtube/core/utils/ae;Lorg/xml/sax/Attributes;Ljava/lang/String;)V
    .locals 2

    const-class v0, Lcom/google/android/youtube/core/model/Event$Builder;

    invoke-virtual {p1, v0}, Lcom/google/android/youtube/core/utils/ae;->b(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/youtube/core/model/Event$Builder;

    invoke-virtual {v0}, Lcom/google/android/youtube/core/model/Event$Builder;->build()Lcom/google/android/youtube/core/model/Event;

    move-result-object v1

    const-class v0, Lcom/google/android/youtube/core/model/Page$Builder;

    invoke-virtual {p1, v0}, Lcom/google/android/youtube/core/utils/ae;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/youtube/core/model/Page$Builder;

    invoke-virtual {v0, v1}, Lcom/google/android/youtube/core/model/Page$Builder;->addEntry(Ljava/lang/Object;)Lcom/google/android/youtube/core/model/Page$Builder;

    return-void
.end method
