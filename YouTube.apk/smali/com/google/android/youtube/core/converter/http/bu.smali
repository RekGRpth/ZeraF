.class final Lcom/google/android/youtube/core/converter/http/bu;
.super Lcom/google/android/youtube/core/converter/n;
.source "SourceFile"


# direct methods
.method constructor <init>()V
    .locals 0

    invoke-direct {p0}, Lcom/google/android/youtube/core/converter/n;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Lcom/google/android/youtube/core/utils/ae;Lorg/xml/sax/Attributes;)V
    .locals 1

    new-instance v0, Lcom/google/android/youtube/core/model/LiveEvent$Builder;

    invoke-direct {v0}, Lcom/google/android/youtube/core/model/LiveEvent$Builder;-><init>()V

    invoke-virtual {p1, v0}, Lcom/google/android/youtube/core/utils/ae;->offer(Ljava/lang/Object;)Z

    return-void
.end method

.method public final a(Lcom/google/android/youtube/core/utils/ae;Lorg/xml/sax/Attributes;Ljava/lang/String;)V
    .locals 5

    const-class v0, Lcom/google/android/youtube/core/model/LiveEvent$Builder;

    invoke-virtual {p1, v0}, Lcom/google/android/youtube/core/utils/ae;->b(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/youtube/core/model/LiveEvent$Builder;

    invoke-virtual {v0}, Lcom/google/android/youtube/core/model/LiveEvent$Builder;->build()Lcom/google/android/youtube/core/model/LiveEvent;

    move-result-object v2

    const-class v0, Lcom/google/android/youtube/core/model/Page$Builder;

    invoke-virtual {p1, v0}, Lcom/google/android/youtube/core/utils/ae;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/youtube/core/model/Page$Builder;

    invoke-virtual {v2}, Lcom/google/android/youtube/core/model/LiveEvent;->isLiveNow()Z

    move-result v1

    if-eqz v1, :cond_1

    iget-object v1, v2, Lcom/google/android/youtube/core/model/LiveEvent;->video:Lcom/google/android/youtube/core/model/Video;

    iget-object v1, v1, Lcom/google/android/youtube/core/model/Video;->streams:Ljava/util/Set;

    invoke-interface {v1}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_3

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/youtube/core/model/Stream;

    iget v1, v1, Lcom/google/android/youtube/core/model/Stream;->gdataFormat:I

    const/16 v4, 0x1c

    if-ne v1, v4, :cond_0

    const/4 v1, 0x1

    :goto_0
    if-eqz v1, :cond_2

    :cond_1
    invoke-virtual {v0, v2}, Lcom/google/android/youtube/core/model/Page$Builder;->addEntry(Ljava/lang/Object;)Lcom/google/android/youtube/core/model/Page$Builder;

    :cond_2
    return-void

    :cond_3
    const/4 v1, 0x0

    goto :goto_0
.end method
