.class final Lcom/google/android/youtube/core/converter/http/fg;
.super Lcom/google/android/youtube/core/converter/n;
.source "SourceFile"


# direct methods
.method constructor <init>()V
    .locals 0

    invoke-direct {p0}, Lcom/google/android/youtube/core/converter/n;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Lcom/google/android/youtube/core/utils/ae;Lorg/xml/sax/Attributes;Ljava/lang/String;)V
    .locals 4

    const-class v0, Lcom/google/android/youtube/core/model/ae;

    invoke-virtual {p1, v0}, Lcom/google/android/youtube/core/utils/ae;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/youtube/core/model/ae;

    const-string v1, "event"

    invoke-interface {p2, v1}, Lorg/xml/sax/Attributes;->getValue(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    if-nez v1, :cond_1

    const-string v0, "Badly formed tracking event - ignoring"

    invoke-static {v0}, Lcom/google/android/youtube/core/L;->c(Ljava/lang/String;)V

    :cond_0
    :goto_0
    return-void

    :cond_1
    :try_start_0
    invoke-virtual {p3}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/google/android/youtube/core/utils/Util;->d(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v2

    const-string v3, "start"

    invoke-virtual {v3, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_2

    invoke-virtual {v0, v2}, Lcom/google/android/youtube/core/model/ae;->b(Landroid/net/Uri;)Lcom/google/android/youtube/core/model/ae;
    :try_end_0
    .catch Ljava/net/MalformedURLException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v0

    const-string v0, "Badly formed tracking uri - ignoring"

    invoke-static {v0}, Lcom/google/android/youtube/core/L;->c(Ljava/lang/String;)V

    goto :goto_0

    :cond_2
    :try_start_1
    const-string v3, "creativeView"

    invoke-virtual {v3, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    invoke-virtual {v0, v2}, Lcom/google/android/youtube/core/model/ae;->b(Landroid/net/Uri;)Lcom/google/android/youtube/core/model/ae;

    goto :goto_0

    :cond_3
    const-string v3, "firstQuartile"

    invoke-virtual {v3, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_4

    invoke-virtual {v0, v2}, Lcom/google/android/youtube/core/model/ae;->c(Landroid/net/Uri;)Lcom/google/android/youtube/core/model/ae;

    goto :goto_0

    :cond_4
    const-string v3, "midpoint"

    invoke-virtual {v3, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_5

    invoke-virtual {v0, v2}, Lcom/google/android/youtube/core/model/ae;->d(Landroid/net/Uri;)Lcom/google/android/youtube/core/model/ae;

    goto :goto_0

    :cond_5
    const-string v3, "thirdQuartile"

    invoke-virtual {v3, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_6

    invoke-virtual {v0, v2}, Lcom/google/android/youtube/core/model/ae;->e(Landroid/net/Uri;)Lcom/google/android/youtube/core/model/ae;

    goto :goto_0

    :cond_6
    const-string v3, "complete"

    invoke-virtual {v3, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_7

    invoke-virtual {v0, v2}, Lcom/google/android/youtube/core/model/ae;->i(Landroid/net/Uri;)Lcom/google/android/youtube/core/model/ae;

    goto :goto_0

    :cond_7
    const-string v3, "pause"

    invoke-virtual {v3, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_8

    invoke-virtual {v0, v2}, Lcom/google/android/youtube/core/model/ae;->k(Landroid/net/Uri;)Lcom/google/android/youtube/core/model/ae;

    goto :goto_0

    :cond_8
    const-string v3, "resume"

    invoke-virtual {v3, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_9

    invoke-virtual {v0, v2}, Lcom/google/android/youtube/core/model/ae;->l(Landroid/net/Uri;)Lcom/google/android/youtube/core/model/ae;

    goto :goto_0

    :cond_9
    const-string v3, "mute"

    invoke-virtual {v3, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_a

    invoke-virtual {v0, v2}, Lcom/google/android/youtube/core/model/ae;->m(Landroid/net/Uri;)Lcom/google/android/youtube/core/model/ae;

    goto :goto_0

    :cond_a
    const-string v3, "fullscreen"

    invoke-virtual {v3, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_b

    invoke-virtual {v0, v2}, Lcom/google/android/youtube/core/model/ae;->n(Landroid/net/Uri;)Lcom/google/android/youtube/core/model/ae;

    goto/16 :goto_0

    :cond_b
    const-string v3, "close"

    invoke-virtual {v3, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-virtual {v0, v2}, Lcom/google/android/youtube/core/model/ae;->j(Landroid/net/Uri;)Lcom/google/android/youtube/core/model/ae;
    :try_end_1
    .catch Ljava/net/MalformedURLException; {:try_start_1 .. :try_end_1} :catch_0

    goto/16 :goto_0
.end method
