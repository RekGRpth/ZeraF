.class public final Lcom/google/android/youtube/core/converter/http/el;
.super Ljava/lang/Object;
.source "SourceFile"


# static fields
.field private static final a:Ljava/util/Set;


# direct methods
.method static constructor <clinit>()V
    .locals 4

    new-instance v0, Ljava/util/HashSet;

    const/4 v1, 0x7

    new-array v1, v1, [Ljava/lang/String;

    const/4 v2, 0x0

    const-string v3, "http://gdata.youtube.com/schemas/2007#user.uploads"

    aput-object v3, v1, v2

    const/4 v2, 0x1

    const-string v3, "http://gdata.youtube.com/schemas/2007#user.favorites"

    aput-object v3, v1, v2

    const/4 v2, 0x2

    const-string v3, "http://gdata.youtube.com/schemas/2007#user.subscriptions"

    aput-object v3, v1, v2

    const/4 v2, 0x3

    const-string v3, "http://gdata.youtube.com/schemas/2007#user.watchhistory"

    aput-object v3, v1, v2

    const/4 v2, 0x4

    const-string v3, "http://gdata.youtube.com/schemas/2007#user.watchlater"

    aput-object v3, v1, v2

    const/4 v2, 0x5

    const-string v3, "http://gdata.youtube.com/schemas/2007#user.playlists"

    aput-object v3, v1, v2

    const/4 v2, 0x6

    const-string v3, "http://gdata.youtube.com/schemas/2007#user.recentactivity"

    aput-object v3, v1, v2

    invoke-static {v1}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/util/HashSet;-><init>(Ljava/util/Collection;)V

    invoke-static {v0}, Ljava/util/Collections;->unmodifiableSet(Ljava/util/Set;)Ljava/util/Set;

    move-result-object v0

    sput-object v0, Lcom/google/android/youtube/core/converter/http/el;->a:Ljava/util/Set;

    return-void
.end method

.method static synthetic a()Ljava/util/Set;
    .locals 1

    sget-object v0, Lcom/google/android/youtube/core/converter/http/el;->a:Ljava/util/Set;

    return-object v0
.end method

.method public static a(Lcom/google/android/youtube/core/converter/e;)V
    .locals 1

    invoke-static {p0}, Lcom/google/android/youtube/core/converter/http/ed;->a(Lcom/google/android/youtube/core/converter/e;)V

    const-string v0, ""

    invoke-static {p0, v0}, Lcom/google/android/youtube/core/converter/http/el;->b(Lcom/google/android/youtube/core/converter/e;Ljava/lang/String;)V

    return-void
.end method

.method public static a(Lcom/google/android/youtube/core/converter/e;Ljava/lang/String;)V
    .locals 0

    invoke-static {p0, p1}, Lcom/google/android/youtube/core/converter/http/ed;->a(Lcom/google/android/youtube/core/converter/e;Ljava/lang/String;)V

    invoke-static {p0, p1}, Lcom/google/android/youtube/core/converter/http/el;->b(Lcom/google/android/youtube/core/converter/e;Ljava/lang/String;)V

    return-void
.end method

.method private static b(Lcom/google/android/youtube/core/converter/e;Ljava/lang/String;)V
    .locals 3

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "/entry/yt:username"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    new-instance v1, Lcom/google/android/youtube/core/converter/http/eo;

    invoke-direct {v1}, Lcom/google/android/youtube/core/converter/http/eo;-><init>()V

    invoke-virtual {p0, v0, v1}, Lcom/google/android/youtube/core/converter/e;->a(Ljava/lang/String;Lcom/google/android/youtube/core/converter/q;)Lcom/google/android/youtube/core/converter/e;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "/entry/yt:channelId"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    new-instance v2, Lcom/google/android/youtube/core/converter/http/en;

    invoke-direct {v2}, Lcom/google/android/youtube/core/converter/http/en;-><init>()V

    invoke-virtual {v0, v1, v2}, Lcom/google/android/youtube/core/converter/e;->a(Ljava/lang/String;Lcom/google/android/youtube/core/converter/q;)Lcom/google/android/youtube/core/converter/e;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "/entry/yt:googlePlusUserId"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    new-instance v2, Lcom/google/android/youtube/core/converter/http/ew;

    invoke-direct {v2}, Lcom/google/android/youtube/core/converter/http/ew;-><init>()V

    invoke-virtual {v0, v1, v2}, Lcom/google/android/youtube/core/converter/e;->a(Ljava/lang/String;Lcom/google/android/youtube/core/converter/q;)Lcom/google/android/youtube/core/converter/e;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "/entry/author/email"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    new-instance v2, Lcom/google/android/youtube/core/converter/http/ev;

    invoke-direct {v2}, Lcom/google/android/youtube/core/converter/http/ev;-><init>()V

    invoke-virtual {v0, v1, v2}, Lcom/google/android/youtube/core/converter/e;->a(Ljava/lang/String;Lcom/google/android/youtube/core/converter/q;)Lcom/google/android/youtube/core/converter/e;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "/entry/yt:age"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    new-instance v2, Lcom/google/android/youtube/core/converter/http/eu;

    invoke-direct {v2}, Lcom/google/android/youtube/core/converter/http/eu;-><init>()V

    invoke-virtual {v0, v1, v2}, Lcom/google/android/youtube/core/converter/e;->a(Ljava/lang/String;Lcom/google/android/youtube/core/converter/q;)Lcom/google/android/youtube/core/converter/e;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "/entry/yt:gender"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    new-instance v2, Lcom/google/android/youtube/core/converter/http/et;

    invoke-direct {v2}, Lcom/google/android/youtube/core/converter/http/et;-><init>()V

    invoke-virtual {v0, v1, v2}, Lcom/google/android/youtube/core/converter/e;->a(Ljava/lang/String;Lcom/google/android/youtube/core/converter/q;)Lcom/google/android/youtube/core/converter/e;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "/entry/media:thumbnail"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    new-instance v2, Lcom/google/android/youtube/core/converter/http/es;

    invoke-direct {v2}, Lcom/google/android/youtube/core/converter/http/es;-><init>()V

    invoke-virtual {v0, v1, v2}, Lcom/google/android/youtube/core/converter/e;->a(Ljava/lang/String;Lcom/google/android/youtube/core/converter/q;)Lcom/google/android/youtube/core/converter/e;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "/entry/yt:incomplete"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    new-instance v2, Lcom/google/android/youtube/core/converter/http/er;

    invoke-direct {v2}, Lcom/google/android/youtube/core/converter/http/er;-><init>()V

    invoke-virtual {v0, v1, v2}, Lcom/google/android/youtube/core/converter/e;->a(Ljava/lang/String;Lcom/google/android/youtube/core/converter/q;)Lcom/google/android/youtube/core/converter/e;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "/entry/yt:eligibleForChannel"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    new-instance v2, Lcom/google/android/youtube/core/converter/http/eq;

    invoke-direct {v2}, Lcom/google/android/youtube/core/converter/http/eq;-><init>()V

    invoke-virtual {v0, v1, v2}, Lcom/google/android/youtube/core/converter/e;->a(Ljava/lang/String;Lcom/google/android/youtube/core/converter/q;)Lcom/google/android/youtube/core/converter/e;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "/entry/yt:statistics"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    new-instance v2, Lcom/google/android/youtube/core/converter/http/ep;

    invoke-direct {v2}, Lcom/google/android/youtube/core/converter/http/ep;-><init>()V

    invoke-virtual {v0, v1, v2}, Lcom/google/android/youtube/core/converter/e;->a(Ljava/lang/String;Lcom/google/android/youtube/core/converter/q;)Lcom/google/android/youtube/core/converter/e;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "/entry/gd:feedLink"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    new-instance v2, Lcom/google/android/youtube/core/converter/http/em;

    invoke-direct {v2}, Lcom/google/android/youtube/core/converter/http/em;-><init>()V

    invoke-virtual {v0, v1, v2}, Lcom/google/android/youtube/core/converter/e;->a(Ljava/lang/String;Lcom/google/android/youtube/core/converter/q;)Lcom/google/android/youtube/core/converter/e;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/youtube/core/converter/e;->a()Lcom/google/android/youtube/core/converter/d;

    return-void
.end method
