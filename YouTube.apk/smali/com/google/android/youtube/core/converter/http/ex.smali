.class public final Lcom/google/android/youtube/core/converter/http/ex;
.super Lcom/google/android/youtube/core/converter/http/hh;
.source "SourceFile"


# instance fields
.field private final c:Lcom/google/android/youtube/core/converter/d;


# direct methods
.method public constructor <init>(Lcom/google/android/youtube/core/converter/m;Lcom/google/android/youtube/core/utils/e;)V
    .locals 3

    invoke-direct {p0, p1}, Lcom/google/android/youtube/core/converter/http/hh;-><init>(Lcom/google/android/youtube/core/converter/m;)V

    const-string v0, "clock cannot be null"

    invoke-static {p2, v0}, Lcom/google/android/youtube/core/utils/s;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    new-instance v0, Lcom/google/android/youtube/core/converter/e;

    invoke-direct {v0}, Lcom/google/android/youtube/core/converter/e;-><init>()V

    const-string v1, ""

    const/4 v2, 0x0

    invoke-static {v1, p2, v0, v2}, Lcom/google/android/youtube/core/converter/http/ex;->a(Ljava/lang/String;Lcom/google/android/youtube/core/utils/e;Lcom/google/android/youtube/core/converter/e;Lcom/google/android/youtube/core/converter/http/fn;)V

    invoke-virtual {v0}, Lcom/google/android/youtube/core/converter/e;->a()Lcom/google/android/youtube/core/converter/d;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/youtube/core/converter/http/ex;->c:Lcom/google/android/youtube/core/converter/d;

    return-void
.end method

.method static synthetic a(Ljava/lang/String;)Lcom/google/android/youtube/core/model/Stream$Quality;
    .locals 1

    invoke-static {p0}, Lcom/google/android/youtube/core/converter/http/ex;->b(Ljava/lang/String;)Lcom/google/android/youtube/core/model/Stream$Quality;

    move-result-object v0

    return-object v0
.end method

.method protected static a(Ljava/lang/String;Lcom/google/android/youtube/core/utils/e;Lcom/google/android/youtube/core/converter/e;Lcom/google/android/youtube/core/converter/http/fn;)V
    .locals 10

    const-string v0, "prefix cannot be null"

    invoke-static {p0, v0}, Lcom/google/android/youtube/core/utils/s;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const-string v0, "clock cannot be null"

    invoke-static {p1, v0}, Lcom/google/android/youtube/core/utils/s;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const-string v0, "builder cannot be null"

    invoke-static {p2, v0}, Lcom/google/android/youtube/core/utils/s;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    new-instance v0, Lcom/google/android/youtube/core/converter/http/ey;

    invoke-direct {v0}, Lcom/google/android/youtube/core/converter/http/ey;-><init>()V

    new-instance v1, Lcom/google/android/youtube/core/converter/http/ff;

    invoke-direct {v1}, Lcom/google/android/youtube/core/converter/http/ff;-><init>()V

    new-instance v2, Lcom/google/android/youtube/core/converter/http/fg;

    invoke-direct {v2}, Lcom/google/android/youtube/core/converter/http/fg;-><init>()V

    new-instance v3, Lcom/google/android/youtube/core/converter/http/fh;

    invoke-direct {v3}, Lcom/google/android/youtube/core/converter/http/fh;-><init>()V

    new-instance v4, Lcom/google/android/youtube/core/converter/http/fi;

    invoke-direct {v4}, Lcom/google/android/youtube/core/converter/http/fi;-><init>()V

    new-instance v5, Lcom/google/android/youtube/core/converter/http/fj;

    invoke-direct {v5}, Lcom/google/android/youtube/core/converter/http/fj;-><init>()V

    new-instance v6, Lcom/google/android/youtube/core/converter/http/fk;

    invoke-direct {v6}, Lcom/google/android/youtube/core/converter/http/fk;-><init>()V

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v7, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, "/VAST"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    new-instance v8, Lcom/google/android/youtube/core/converter/http/fe;

    invoke-direct {v8, p3}, Lcom/google/android/youtube/core/converter/http/fe;-><init>(Lcom/google/android/youtube/core/converter/http/fn;)V

    invoke-virtual {p2, v7, v8}, Lcom/google/android/youtube/core/converter/e;->a(Ljava/lang/String;Lcom/google/android/youtube/core/converter/q;)Lcom/google/android/youtube/core/converter/e;

    move-result-object v7

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v8, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, "/VAST/Ad"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    new-instance v9, Lcom/google/android/youtube/core/converter/http/fd;

    invoke-direct {v9, p1}, Lcom/google/android/youtube/core/converter/http/fd;-><init>(Lcom/google/android/youtube/core/utils/e;)V

    invoke-virtual {v7, v8, v9}, Lcom/google/android/youtube/core/converter/e;->a(Ljava/lang/String;Lcom/google/android/youtube/core/converter/q;)Lcom/google/android/youtube/core/converter/e;

    move-result-object v7

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v8, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, "/VAST/Ad/InLine/Creatives/Creative/Linear/Duration"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    new-instance v9, Lcom/google/android/youtube/core/converter/http/fc;

    invoke-direct {v9}, Lcom/google/android/youtube/core/converter/http/fc;-><init>()V

    invoke-virtual {v7, v8, v9}, Lcom/google/android/youtube/core/converter/e;->a(Ljava/lang/String;Lcom/google/android/youtube/core/converter/q;)Lcom/google/android/youtube/core/converter/e;

    move-result-object v7

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v8, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, "/VAST/Ad/InLine/Creatives/Creative/Linear/MediaFiles/MediaFile"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    new-instance v9, Lcom/google/android/youtube/core/converter/http/fb;

    invoke-direct {v9}, Lcom/google/android/youtube/core/converter/http/fb;-><init>()V

    invoke-virtual {v7, v8, v9}, Lcom/google/android/youtube/core/converter/e;->a(Ljava/lang/String;Lcom/google/android/youtube/core/converter/q;)Lcom/google/android/youtube/core/converter/e;

    move-result-object v7

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v8, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, "/VAST/Ad/InLine/Creatives/Creative/NonLinearAds/NonLinear/AdParameters"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    new-instance v9, Lcom/google/android/youtube/core/converter/http/fa;

    invoke-direct {v9}, Lcom/google/android/youtube/core/converter/http/fa;-><init>()V

    invoke-virtual {v7, v8, v9}, Lcom/google/android/youtube/core/converter/e;->a(Ljava/lang/String;Lcom/google/android/youtube/core/converter/q;)Lcom/google/android/youtube/core/converter/e;

    move-result-object v7

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v8, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, "/VAST/Ad/InLine/Extensions/Extension/CustomTracking/Tracking"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    new-instance v9, Lcom/google/android/youtube/core/converter/http/ez;

    invoke-direct {v9}, Lcom/google/android/youtube/core/converter/http/ez;-><init>()V

    invoke-virtual {v7, v8, v9}, Lcom/google/android/youtube/core/converter/e;->a(Ljava/lang/String;Lcom/google/android/youtube/core/converter/q;)Lcom/google/android/youtube/core/converter/e;

    move-result-object v7

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v8, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, "/VAST/Ad/InLine/Extensions/Extension/SkippableAdType"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    new-instance v9, Lcom/google/android/youtube/core/converter/http/fm;

    invoke-direct {v9}, Lcom/google/android/youtube/core/converter/http/fm;-><init>()V

    invoke-virtual {v7, v8, v9}, Lcom/google/android/youtube/core/converter/e;->a(Ljava/lang/String;Lcom/google/android/youtube/core/converter/q;)Lcom/google/android/youtube/core/converter/e;

    move-result-object v7

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v8, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, "/VAST/Ad/InLine/AdSystem"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8, v0}, Lcom/google/android/youtube/core/converter/e;->a(Ljava/lang/String;Lcom/google/android/youtube/core/converter/q;)Lcom/google/android/youtube/core/converter/e;

    move-result-object v7

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v8, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, "/VAST/Ad/InLine/Impression"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8, v1}, Lcom/google/android/youtube/core/converter/e;->a(Ljava/lang/String;Lcom/google/android/youtube/core/converter/q;)Lcom/google/android/youtube/core/converter/e;

    move-result-object v7

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v8, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, "/VAST/Ad/InLine/Creatives/Creative/Linear/TrackingEvents/Tracking"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8, v2}, Lcom/google/android/youtube/core/converter/e;->a(Ljava/lang/String;Lcom/google/android/youtube/core/converter/q;)Lcom/google/android/youtube/core/converter/e;

    move-result-object v7

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v8, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, "/VAST/Ad/InLine/Error"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8, v3}, Lcom/google/android/youtube/core/converter/e;->a(Ljava/lang/String;Lcom/google/android/youtube/core/converter/q;)Lcom/google/android/youtube/core/converter/e;

    move-result-object v7

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v8, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, "/VAST/Ad/InLine/Creatives/Creative/Linear/VideoClicks/ClickThrough"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8, v4}, Lcom/google/android/youtube/core/converter/e;->a(Ljava/lang/String;Lcom/google/android/youtube/core/converter/q;)Lcom/google/android/youtube/core/converter/e;

    move-result-object v7

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v8, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, "/VAST/Ad/InLine/Creatives/Creative/Linear/VideoClicks/ClickTracking"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8, v5}, Lcom/google/android/youtube/core/converter/e;->a(Ljava/lang/String;Lcom/google/android/youtube/core/converter/q;)Lcom/google/android/youtube/core/converter/e;

    move-result-object v7

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v8, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, "/VAST/Ad/InLine/Creatives/Creative/Linear/VideoClicks/CustomClick"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8, v6}, Lcom/google/android/youtube/core/converter/e;->a(Ljava/lang/String;Lcom/google/android/youtube/core/converter/q;)Lcom/google/android/youtube/core/converter/e;

    move-result-object v7

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v8, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, "/VAST/Ad/Wrapper/Error"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8, v3}, Lcom/google/android/youtube/core/converter/e;->a(Ljava/lang/String;Lcom/google/android/youtube/core/converter/q;)Lcom/google/android/youtube/core/converter/e;

    move-result-object v3

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v7, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, "/VAST/Ad/Wrapper/AdSystem"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v3, v7, v0}, Lcom/google/android/youtube/core/converter/e;->a(Ljava/lang/String;Lcom/google/android/youtube/core/converter/q;)Lcom/google/android/youtube/core/converter/e;

    move-result-object v0

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v7, "/VAST/Ad/Wrapper/Impression"

    invoke-virtual {v3, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3, v1}, Lcom/google/android/youtube/core/converter/e;->a(Ljava/lang/String;Lcom/google/android/youtube/core/converter/q;)Lcom/google/android/youtube/core/converter/e;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v3, "/VAST/Ad/Wrapper/Creatives/Creative/Linear/TrackingEvents/Tracking"

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1, v2}, Lcom/google/android/youtube/core/converter/e;->a(Ljava/lang/String;Lcom/google/android/youtube/core/converter/q;)Lcom/google/android/youtube/core/converter/e;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "/VAST/Ad/Wrapper/Creatives/Creative/Linear/VideoClicks/ClickThrough"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1, v4}, Lcom/google/android/youtube/core/converter/e;->a(Ljava/lang/String;Lcom/google/android/youtube/core/converter/q;)Lcom/google/android/youtube/core/converter/e;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "/VAST/Ad/Wrapper/Creatives/Creative/Linear/VideoClicks/ClickTracking"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1, v5}, Lcom/google/android/youtube/core/converter/e;->a(Ljava/lang/String;Lcom/google/android/youtube/core/converter/q;)Lcom/google/android/youtube/core/converter/e;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "/VAST/Ad/Wrapper/Creatives/Creative/Linear/VideoClicks/CustomClick"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1, v6}, Lcom/google/android/youtube/core/converter/e;->a(Ljava/lang/String;Lcom/google/android/youtube/core/converter/q;)Lcom/google/android/youtube/core/converter/e;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "/VAST/Ad/Wrapper/VASTAdTagURI"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    new-instance v2, Lcom/google/android/youtube/core/converter/http/fl;

    invoke-direct {v2}, Lcom/google/android/youtube/core/converter/http/fl;-><init>()V

    invoke-virtual {v0, v1, v2}, Lcom/google/android/youtube/core/converter/e;->a(Ljava/lang/String;Lcom/google/android/youtube/core/converter/q;)Lcom/google/android/youtube/core/converter/e;

    return-void
.end method

.method private static b(Ljava/lang/String;)Lcom/google/android/youtube/core/model/Stream$Quality;
    .locals 2

    const/4 v0, 0x0

    :try_start_0
    invoke-static {p0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_0

    invoke-virtual {p0}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v0

    :cond_0
    :goto_0
    invoke-static {v0}, Lcom/google/android/youtube/core/model/Stream;->inferQuality(I)Lcom/google/android/youtube/core/model/Stream$Quality;

    move-result-object v0

    return-object v0

    :catch_0
    move-exception v1

    const-string v1, "Badly formed video size - using UNKNOWN"

    invoke-static {v1}, Lcom/google/android/youtube/core/L;->c(Ljava/lang/String;)V

    goto :goto_0
.end method


# virtual methods
.method protected final a()Lcom/google/android/youtube/core/converter/d;
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/core/converter/http/ex;->c:Lcom/google/android/youtube/core/converter/d;

    return-object v0
.end method
