.class public final Lcom/google/android/youtube/core/converter/http/eb;
.super Lcom/google/android/youtube/core/converter/http/bn;
.source "SourceFile"


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Lcom/google/android/youtube/core/converter/http/bn;-><init>()V

    return-void
.end method

.method private e(Lorg/apache/http/HttpResponse;)Landroid/net/Uri;
    .locals 2

    invoke-virtual {p0, p1}, Lcom/google/android/youtube/core/converter/http/eb;->c(Lorg/apache/http/HttpResponse;)V

    const-string v0, "Location"

    invoke-interface {p1, v0}, Lorg/apache/http/HttpResponse;->getFirstHeader(Ljava/lang/String;)Lorg/apache/http/Header;

    move-result-object v0

    if-nez v0, :cond_0

    new-instance v0, Lcom/google/android/youtube/core/converter/ConverterException;

    const-string v1, "Location header not present"

    invoke-direct {v0, v1}, Lcom/google/android/youtube/core/converter/ConverterException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    invoke-interface {v0}, Lorg/apache/http/Header;->getValue()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public final synthetic a_(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    check-cast p1, Lorg/apache/http/HttpResponse;

    invoke-direct {p0, p1}, Lcom/google/android/youtube/core/converter/http/eb;->e(Lorg/apache/http/HttpResponse;)Landroid/net/Uri;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic b(Lorg/apache/http/HttpResponse;)Ljava/lang/Object;
    .locals 1

    invoke-direct {p0, p1}, Lcom/google/android/youtube/core/converter/http/eb;->e(Lorg/apache/http/HttpResponse;)Landroid/net/Uri;

    move-result-object v0

    return-object v0
.end method
