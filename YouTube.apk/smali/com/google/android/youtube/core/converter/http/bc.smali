.class public abstract Lcom/google/android/youtube/core/converter/http/bc;
.super Lcom/google/android/youtube/core/converter/http/hh;
.source "SourceFile"


# static fields
.field private static final c:Lcom/google/android/youtube/core/converter/d;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    new-instance v0, Lcom/google/android/youtube/core/converter/e;

    invoke-direct {v0}, Lcom/google/android/youtube/core/converter/e;-><init>()V

    const-string v1, "/errors"

    new-instance v2, Lcom/google/android/youtube/core/converter/http/bi;

    invoke-direct {v2}, Lcom/google/android/youtube/core/converter/http/bi;-><init>()V

    invoke-virtual {v0, v1, v2}, Lcom/google/android/youtube/core/converter/e;->a(Ljava/lang/String;Lcom/google/android/youtube/core/converter/q;)Lcom/google/android/youtube/core/converter/e;

    move-result-object v0

    const-string v1, "/errors/error"

    new-instance v2, Lcom/google/android/youtube/core/converter/http/bh;

    invoke-direct {v2}, Lcom/google/android/youtube/core/converter/http/bh;-><init>()V

    invoke-virtual {v0, v1, v2}, Lcom/google/android/youtube/core/converter/e;->a(Ljava/lang/String;Lcom/google/android/youtube/core/converter/q;)Lcom/google/android/youtube/core/converter/e;

    move-result-object v0

    const-string v1, "/errors/error/domain"

    new-instance v2, Lcom/google/android/youtube/core/converter/http/bg;

    invoke-direct {v2}, Lcom/google/android/youtube/core/converter/http/bg;-><init>()V

    invoke-virtual {v0, v1, v2}, Lcom/google/android/youtube/core/converter/e;->a(Ljava/lang/String;Lcom/google/android/youtube/core/converter/q;)Lcom/google/android/youtube/core/converter/e;

    move-result-object v0

    const-string v1, "/errors/error/code"

    new-instance v2, Lcom/google/android/youtube/core/converter/http/bf;

    invoke-direct {v2}, Lcom/google/android/youtube/core/converter/http/bf;-><init>()V

    invoke-virtual {v0, v1, v2}, Lcom/google/android/youtube/core/converter/e;->a(Ljava/lang/String;Lcom/google/android/youtube/core/converter/q;)Lcom/google/android/youtube/core/converter/e;

    move-result-object v0

    const-string v1, "/errors/error/location"

    new-instance v2, Lcom/google/android/youtube/core/converter/http/be;

    invoke-direct {v2}, Lcom/google/android/youtube/core/converter/http/be;-><init>()V

    invoke-virtual {v0, v1, v2}, Lcom/google/android/youtube/core/converter/e;->a(Ljava/lang/String;Lcom/google/android/youtube/core/converter/q;)Lcom/google/android/youtube/core/converter/e;

    move-result-object v0

    const-string v1, "/errors/error/internalReason"

    new-instance v2, Lcom/google/android/youtube/core/converter/http/bd;

    invoke-direct {v2}, Lcom/google/android/youtube/core/converter/http/bd;-><init>()V

    invoke-virtual {v0, v1, v2}, Lcom/google/android/youtube/core/converter/e;->a(Ljava/lang/String;Lcom/google/android/youtube/core/converter/q;)Lcom/google/android/youtube/core/converter/e;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/youtube/core/converter/e;->a()Lcom/google/android/youtube/core/converter/d;

    move-result-object v0

    sput-object v0, Lcom/google/android/youtube/core/converter/http/bc;->c:Lcom/google/android/youtube/core/converter/d;

    return-void
.end method

.method public constructor <init>(Lcom/google/android/youtube/core/converter/m;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/google/android/youtube/core/converter/http/hh;-><init>(Lcom/google/android/youtube/core/converter/m;)V

    return-void
.end method

.method public static a(Lcom/google/android/youtube/core/converter/m;)Lcom/google/android/youtube/core/converter/http/bc;
    .locals 1

    new-instance v0, Lcom/google/android/youtube/core/converter/http/bj;

    invoke-direct {v0, p0}, Lcom/google/android/youtube/core/converter/http/bj;-><init>(Lcom/google/android/youtube/core/converter/m;)V

    return-object v0
.end method


# virtual methods
.method protected final a(Lorg/apache/http/HttpResponse;)Lorg/apache/http/client/HttpResponseException;
    .locals 6

    const/16 v5, 0x191

    invoke-interface {p1}, Lorg/apache/http/HttpResponse;->getStatusLine()Lorg/apache/http/StatusLine;

    move-result-object v0

    invoke-interface {v0}, Lorg/apache/http/StatusLine;->getStatusCode()I

    move-result v2

    invoke-interface {v0}, Lorg/apache/http/StatusLine;->getReasonPhrase()Ljava/lang/String;

    move-result-object v3

    const-string v0, "Content-Type"

    invoke-interface {p1, v0}, Lorg/apache/http/HttpResponse;->getFirstHeader(Ljava/lang/String;)Lorg/apache/http/Header;

    move-result-object v0

    const/16 v1, 0x190

    if-eq v2, v1, :cond_0

    if-eq v2, v5, :cond_0

    const/16 v1, 0x193

    if-eq v2, v1, :cond_0

    const/16 v1, 0x1f7

    if-ne v2, v1, :cond_1

    :cond_0
    if-eqz v0, :cond_1

    invoke-interface {v0}, Lorg/apache/http/Header;->getValue()Ljava/lang/String;

    move-result-object v0

    const-string v1, "xml"

    invoke-virtual {v0, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_1

    const/4 v0, 0x1

    :goto_0
    if-eqz v0, :cond_2

    :try_start_0
    invoke-interface {p1}, Lorg/apache/http/HttpResponse;->getEntity()Lorg/apache/http/HttpEntity;

    move-result-object v0

    invoke-interface {v0}, Lorg/apache/http/HttpEntity;->getContent()Ljava/io/InputStream;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/youtube/core/converter/http/bc;->a:Lcom/google/android/youtube/core/converter/m;

    sget-object v4, Lcom/google/android/youtube/core/converter/http/bc;->c:Lcom/google/android/youtube/core/converter/d;

    invoke-virtual {v1, v0, v4}, Lcom/google/android/youtube/core/converter/m;->a(Ljava/io/InputStream;Lcom/google/android/youtube/core/converter/d;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/ArrayList;

    if-eqz v0, :cond_2

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v1

    if-eqz v1, :cond_2

    new-instance v1, Lcom/google/android/youtube/core/async/GDataResponseException;

    invoke-direct {v1, v2, v3, v0}, Lcom/google/android/youtube/core/async/GDataResponseException;-><init>(ILjava/lang/String;Ljava/util/List;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-object v0, v1

    :goto_1
    return-object v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0

    :catch_0
    move-exception v0

    :cond_2
    if-ne v2, v5, :cond_3

    const-string v0, "NoLinkedYouTubeAccount"

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-static {v2, v3}, Lcom/google/android/youtube/core/async/GDataResponseException;->createYouTubeSignupRequired(ILjava/lang/String;)Lcom/google/android/youtube/core/async/GDataResponseException;

    move-result-object v0

    goto :goto_1

    :cond_3
    invoke-super {p0, p1}, Lcom/google/android/youtube/core/converter/http/hh;->a(Lorg/apache/http/HttpResponse;)Lorg/apache/http/client/HttpResponseException;

    move-result-object v0

    goto :goto_1
.end method
