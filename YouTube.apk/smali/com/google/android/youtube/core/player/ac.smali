.class public final Lcom/google/android/youtube/core/player/ac;
.super Landroid/app/Dialog;
.source "SourceFile"


# instance fields
.field private final a:Landroid/app/Activity;

.field private final b:Z

.field private c:Lcom/google/android/youtube/core/player/Director;


# direct methods
.method public constructor <init>(Landroid/app/Activity;Landroid/os/Bundle;Landroid/view/View;Lcom/google/android/youtube/core/player/Director;Lcom/google/android/youtube/core/client/bc;)V
    .locals 13

    const-string v0, "video_id"

    invoke-virtual {p2, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    const-string v0, "playlist_id"

    invoke-virtual {p2, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    const-string v0, "video_ids"

    invoke-virtual {p2, v0}, Landroid/os/Bundle;->getStringArrayList(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v7

    const-string v0, "current_index"

    const/4 v1, 0x0

    invoke-virtual {p2, v0, v1}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v8

    const-string v0, "start_time_millis"

    const/4 v1, 0x0

    invoke-virtual {p2, v0, v1}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v9

    const/4 v10, 0x1

    const-string v0, "lightbox_mode"

    invoke-virtual {p2, v0}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v11

    const-string v0, "window_has_status_bar"

    invoke-virtual {p2, v0}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v12

    move-object v0, p0

    move-object v1, p1

    move-object/from16 v2, p3

    move-object/from16 v3, p4

    move-object/from16 v4, p5

    invoke-direct/range {v0 .. v12}, Lcom/google/android/youtube/core/player/ac;-><init>(Landroid/app/Activity;Landroid/view/View;Lcom/google/android/youtube/core/player/Director;Lcom/google/android/youtube/core/client/bc;Ljava/lang/String;Ljava/lang/String;Ljava/util/ArrayList;IIZZZ)V

    return-void
.end method

.method public constructor <init>(Landroid/app/Activity;Landroid/view/View;Lcom/google/android/youtube/core/player/Director;Lcom/google/android/youtube/core/client/bc;Ljava/lang/String;Ljava/lang/String;Ljava/util/ArrayList;IIZZZ)V
    .locals 8

    invoke-static/range {p11 .. p12}, Lcom/google/android/youtube/core/player/ac;->a(ZZ)I

    move-result v1

    invoke-direct {p0, p1, v1}, Landroid/app/Dialog;-><init>(Landroid/content/Context;I)V

    invoke-static {p5}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-static {p6}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_0

    if-eqz p7, :cond_2

    invoke-virtual {p7}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_2

    :cond_0
    const/4 v1, 0x1

    :goto_0
    const-string v2, "One of videoId, playlistId or videoIds must not be null or empty"

    invoke-static {v1, v2}, Lcom/google/android/youtube/core/utils/s;->a(ZLjava/lang/Object;)V

    const-string v1, "activity cannot be null"

    invoke-static {p1, v1}, Lcom/google/android/youtube/core/utils/s;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/app/Activity;

    iput-object v1, p0, Lcom/google/android/youtube/core/player/ac;->a:Landroid/app/Activity;

    const-string v1, "playerView cannot be null"

    invoke-static {p2, v1}, Lcom/google/android/youtube/core/utils/s;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const-string v1, "director cannot be null"

    invoke-static {p3, v1}, Lcom/google/android/youtube/core/utils/s;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/youtube/core/player/Director;

    iput-object v1, p0, Lcom/google/android/youtube/core/player/ac;->c:Lcom/google/android/youtube/core/player/Director;

    const-string v1, "gdataClient cannot be null"

    invoke-static {p4, v1}, Lcom/google/android/youtube/core/utils/s;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move/from16 v0, p11

    iput-boolean v0, p0, Lcom/google/android/youtube/core/player/ac;->b:Z

    if-eqz p11, :cond_3

    new-instance v1, Landroid/widget/FrameLayout;

    invoke-virtual {p0}, Lcom/google/android/youtube/core/player/ac;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-direct {v1, v2}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;)V

    const v2, 0x1080011

    invoke-virtual {v1, v2}, Landroid/widget/FrameLayout;->setBackgroundResource(I)V

    new-instance v2, Landroid/widget/FrameLayout$LayoutParams;

    const/4 v3, -0x1

    const/4 v4, -0x2

    const/16 v5, 0x11

    invoke-direct {v2, v3, v4, v5}, Landroid/widget/FrameLayout$LayoutParams;-><init>(III)V

    invoke-virtual {v1, p2, v2}, Landroid/widget/FrameLayout;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    invoke-virtual {p1}, Landroid/app/Activity;->getWindow()Landroid/view/Window;

    move-result-object v2

    const v3, 0x7f090009

    invoke-virtual {v2, v3}, Landroid/view/Window;->setBackgroundDrawableResource(I)V

    move-object p2, v1

    :goto_1
    new-instance v2, Landroid/widget/FrameLayout;

    invoke-virtual {p0}, Lcom/google/android/youtube/core/player/ac;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v2, v1}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;)V

    new-instance v3, Landroid/widget/FrameLayout$LayoutParams;

    const/4 v4, -0x1

    if-eqz p11, :cond_4

    const/4 v1, -0x2

    :goto_2
    const/16 v5, 0x11

    invoke-direct {v3, v4, v1, v5}, Landroid/widget/FrameLayout$LayoutParams;-><init>(III)V

    invoke-virtual {v2, p2, v3}, Landroid/widget/FrameLayout;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    invoke-virtual {p0, v2}, Lcom/google/android/youtube/core/player/ac;->setContentView(Landroid/view/View;)V

    const/4 v2, 0x0

    if-eqz p7, :cond_5

    const/4 v1, 0x0

    move/from16 v0, p8

    invoke-static {p4, p7, v1, v0}, Lcom/google/android/youtube/core/async/a/b;->a(Lcom/google/android/youtube/core/client/bc;Ljava/util/List;Lcom/google/android/youtube/core/model/UserAuth;I)Lcom/google/android/youtube/core/async/a/c;

    move-result-object v2

    :cond_1
    :goto_3
    sget-object v3, Lcom/google/android/youtube/core/client/VideoStats2Client$Feature;->NO_FEATURE:Lcom/google/android/youtube/core/client/VideoStats2Client$Feature;

    if-nez p10, :cond_7

    const/4 v4, 0x1

    :goto_4
    const/4 v6, 0x0

    if-nez p10, :cond_8

    const/4 v7, 0x1

    :goto_5
    move-object v1, p3

    move/from16 v5, p9

    invoke-virtual/range {v1 .. v7}, Lcom/google/android/youtube/core/player/Director;->a(Lcom/google/android/youtube/core/async/a/a;Lcom/google/android/youtube/core/client/VideoStats2Client$Feature;ZILjava/util/Map;Z)V

    return-void

    :cond_2
    const/4 v1, 0x0

    goto/16 :goto_0

    :cond_3
    const/high16 v1, -0x1000000

    invoke-virtual {p2, v1}, Landroid/view/View;->setBackgroundColor(I)V

    goto :goto_1

    :cond_4
    const/4 v1, -0x1

    goto :goto_2

    :cond_5
    if-eqz p6, :cond_6

    move/from16 v0, p8

    invoke-static {p4, p6, v0}, Lcom/google/android/youtube/core/async/a/b;->a(Lcom/google/android/youtube/core/client/bc;Ljava/lang/String;I)Lcom/google/android/youtube/core/async/a/c;

    move-result-object v2

    goto :goto_3

    :cond_6
    if-eqz p5, :cond_1

    const/4 v1, 0x0

    invoke-static {p4, p5, v1}, Lcom/google/android/youtube/core/async/a/b;->a(Lcom/google/android/youtube/core/client/bc;Ljava/lang/String;Lcom/google/android/youtube/core/model/UserAuth;)Lcom/google/android/youtube/core/async/a/c;

    move-result-object v2

    goto :goto_3

    :cond_7
    const/4 v4, 0x0

    goto :goto_4

    :cond_8
    const/4 v7, 0x0

    goto :goto_5
.end method

.method public static a(ZZ)I
    .locals 1

    if-eqz p0, :cond_1

    if-eqz p1, :cond_0

    const v0, 0x7f0d000b

    :goto_0
    return v0

    :cond_0
    const v0, 0x7f0d000a

    goto :goto_0

    :cond_1
    const v0, 0x7f0d0009

    goto :goto_0
.end method


# virtual methods
.method public final a()V
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/core/player/ac;->c:Lcom/google/android/youtube/core/player/Director;

    invoke-virtual {v0}, Lcom/google/android/youtube/core/player/Director;->o()V

    return-void
.end method

.method public final b()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/youtube/core/player/ac;->b:Z

    return v0
.end method

.method public final onKeyDown(ILandroid/view/KeyEvent;)Z
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/core/player/ac;->c:Lcom/google/android/youtube/core/player/Director;

    invoke-virtual {v0, p1, p2}, Lcom/google/android/youtube/core/player/Director;->a(ILandroid/view/KeyEvent;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    invoke-super {p0, p1, p2}, Landroid/app/Dialog;->onKeyDown(ILandroid/view/KeyEvent;)Z

    move-result v0

    goto :goto_0
.end method

.method public final onKeyUp(ILandroid/view/KeyEvent;)Z
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/core/player/ac;->c:Lcom/google/android/youtube/core/player/Director;

    invoke-virtual {v0, p1, p2}, Lcom/google/android/youtube/core/player/Director;->b(ILandroid/view/KeyEvent;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    invoke-super {p0, p1, p2}, Landroid/app/Dialog;->onKeyUp(ILandroid/view/KeyEvent;)Z

    move-result v0

    goto :goto_0
.end method

.method protected final onStop()V
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/core/player/ac;->a:Landroid/app/Activity;

    invoke-virtual {v0}, Landroid/app/Activity;->isFinishing()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/youtube/core/player/ac;->a:Landroid/app/Activity;

    invoke-virtual {v0}, Landroid/app/Activity;->finish()V

    :cond_0
    invoke-super {p0}, Landroid/app/Dialog;->onStop()V

    return-void
.end method
