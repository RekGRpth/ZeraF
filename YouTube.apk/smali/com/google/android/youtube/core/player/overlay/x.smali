.class public final Lcom/google/android/youtube/core/player/overlay/x;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field private final a:Landroid/content/Context;

.field private b:Landroid/app/Dialog;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const-string v0, "context cannot be null"

    invoke-static {p1, v0}, Lcom/google/android/youtube/core/utils/s;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/Context;

    iput-object v0, p0, Lcom/google/android/youtube/core/player/overlay/x;->a:Landroid/content/Context;

    return-void
.end method


# virtual methods
.method public final a()V
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/core/player/overlay/x;->b:Landroid/app/Dialog;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/youtube/core/player/overlay/x;->b:Landroid/app/Dialog;

    invoke-virtual {v0}, Landroid/app/Dialog;->isShowing()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/youtube/core/player/overlay/x;->b:Landroid/app/Dialog;

    invoke-virtual {v0}, Landroid/app/Dialog;->dismiss()V

    :cond_0
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/youtube/core/player/overlay/x;->b:Landroid/app/Dialog;

    return-void
.end method

.method public final a(Ljava/util/List;Lcom/google/android/youtube/core/player/overlay/z;)V
    .locals 4

    const-string v0, "listener cannot be null"

    invoke-static {p2, v0}, Lcom/google/android/youtube/core/utils/s;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    invoke-virtual {p0}, Lcom/google/android/youtube/core/player/overlay/x;->a()V

    new-instance v0, Landroid/widget/ArrayAdapter;

    iget-object v1, p0, Lcom/google/android/youtube/core/player/overlay/x;->a:Landroid/content/Context;

    const v2, 0x1090011

    invoke-direct {v0, v1, v2, p1}, Landroid/widget/ArrayAdapter;-><init>(Landroid/content/Context;ILjava/util/List;)V

    new-instance v1, Lcom/google/android/youtube/core/player/overlay/y;

    invoke-direct {v1, p0, v0, p2}, Lcom/google/android/youtube/core/player/overlay/y;-><init>(Lcom/google/android/youtube/core/player/overlay/x;Landroid/widget/ArrayAdapter;Lcom/google/android/youtube/core/player/overlay/z;)V

    new-instance v2, Lcom/google/android/youtube/core/ui/w;

    iget-object v3, p0, Lcom/google/android/youtube/core/player/overlay/x;->a:Landroid/content/Context;

    invoke-direct {v2, v3}, Lcom/google/android/youtube/core/ui/w;-><init>(Landroid/content/Context;)V

    const v3, 0x7f0b002f

    invoke-virtual {v2, v3}, Lcom/google/android/youtube/core/ui/w;->setTitle(I)Landroid/app/AlertDialog$Builder;

    move-result-object v2

    const/4 v3, 0x0

    invoke-virtual {v2, v0, v3, v1}, Landroid/app/AlertDialog$Builder;->setSingleChoiceItems(Landroid/widget/ListAdapter;ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/youtube/core/player/overlay/x;->b:Landroid/app/Dialog;

    iget-object v0, p0, Lcom/google/android/youtube/core/player/overlay/x;->b:Landroid/app/Dialog;

    invoke-virtual {v0}, Landroid/app/Dialog;->show()V

    return-void
.end method
