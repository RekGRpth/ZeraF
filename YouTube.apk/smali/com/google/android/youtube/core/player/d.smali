.class public final Lcom/google/android/youtube/core/player/d;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/youtube/core/player/overlay/d;


# instance fields
.field private final a:Lcom/google/android/youtube/core/client/bc;

.field private final b:Lcom/google/android/youtube/core/client/be;

.field private final c:Lcom/google/android/youtube/core/player/f;

.field private final d:Landroid/os/Handler;

.field private final e:Lcom/google/android/youtube/core/player/overlay/c;

.field private final f:Lcom/google/android/youtube/core/player/e;

.field private g:Lcom/google/android/youtube/core/async/p;

.field private h:Lcom/google/android/youtube/core/async/p;

.field private i:Lcom/google/android/youtube/core/async/p;

.field private j:Landroid/net/Uri;


# direct methods
.method public constructor <init>(Lcom/google/android/youtube/core/client/bc;Lcom/google/android/youtube/core/client/be;Lcom/google/android/youtube/core/player/overlay/c;Lcom/google/android/youtube/core/player/f;Landroid/os/Handler;)V
    .locals 2

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const-string v0, "gdataClient cannot be null"

    invoke-static {p1, v0}, Lcom/google/android/youtube/core/utils/s;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/youtube/core/client/bc;

    iput-object v0, p0, Lcom/google/android/youtube/core/player/d;->a:Lcom/google/android/youtube/core/client/bc;

    const-string v0, "imageClient cannot be null"

    invoke-static {p2, v0}, Lcom/google/android/youtube/core/utils/s;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/youtube/core/client/be;

    iput-object v0, p0, Lcom/google/android/youtube/core/player/d;->b:Lcom/google/android/youtube/core/client/be;

    const-string v0, "brandingOverlay cannot be null"

    invoke-static {p3, v0}, Lcom/google/android/youtube/core/utils/s;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/youtube/core/player/overlay/c;

    iput-object v0, p0, Lcom/google/android/youtube/core/player/d;->e:Lcom/google/android/youtube/core/player/overlay/c;

    const-string v0, "listener cannot be null"

    invoke-static {p4, v0}, Lcom/google/android/youtube/core/utils/s;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/youtube/core/player/f;

    iput-object v0, p0, Lcom/google/android/youtube/core/player/d;->c:Lcom/google/android/youtube/core/player/f;

    const-string v0, "uiHandler cannot be null"

    invoke-static {p5, v0}, Lcom/google/android/youtube/core/utils/s;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/os/Handler;

    iput-object v0, p0, Lcom/google/android/youtube/core/player/d;->d:Landroid/os/Handler;

    invoke-interface {p3, p0}, Lcom/google/android/youtube/core/player/overlay/c;->setListener(Lcom/google/android/youtube/core/player/overlay/d;)V

    new-instance v0, Lcom/google/android/youtube/core/player/e;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/google/android/youtube/core/player/e;-><init>(Lcom/google/android/youtube/core/player/d;B)V

    iput-object v0, p0, Lcom/google/android/youtube/core/player/d;->f:Lcom/google/android/youtube/core/player/e;

    return-void
.end method

.method static synthetic a(Lcom/google/android/youtube/core/player/d;Landroid/net/Uri;)Landroid/net/Uri;
    .locals 0

    iput-object p1, p0, Lcom/google/android/youtube/core/player/d;->j:Landroid/net/Uri;

    return-object p1
.end method

.method static synthetic a(Lcom/google/android/youtube/core/player/d;)Landroid/os/Handler;
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/core/player/d;->d:Landroid/os/Handler;

    return-object v0
.end method

.method static synthetic a(Lcom/google/android/youtube/core/player/d;Lcom/google/android/youtube/core/async/p;)Lcom/google/android/youtube/core/async/p;
    .locals 0

    iput-object p1, p0, Lcom/google/android/youtube/core/player/d;->h:Lcom/google/android/youtube/core/async/p;

    return-object p1
.end method

.method static synthetic b(Lcom/google/android/youtube/core/player/d;)Lcom/google/android/youtube/core/async/p;
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/core/player/d;->h:Lcom/google/android/youtube/core/async/p;

    return-object v0
.end method

.method static synthetic c(Lcom/google/android/youtube/core/player/d;)Lcom/google/android/youtube/core/client/be;
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/core/player/d;->b:Lcom/google/android/youtube/core/client/be;

    return-object v0
.end method

.method static synthetic d(Lcom/google/android/youtube/core/player/d;)Lcom/google/android/youtube/core/player/overlay/c;
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/core/player/d;->e:Lcom/google/android/youtube/core/player/overlay/c;

    return-object v0
.end method

.method static synthetic e(Lcom/google/android/youtube/core/player/d;)Lcom/google/android/youtube/core/player/f;
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/core/player/d;->c:Lcom/google/android/youtube/core/player/f;

    return-object v0
.end method


# virtual methods
.method public final a()V
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/core/player/d;->e:Lcom/google/android/youtube/core/player/overlay/c;

    invoke-interface {v0}, Lcom/google/android/youtube/core/player/overlay/c;->d()V

    return-void
.end method

.method public final a(Lcom/google/android/youtube/core/model/Video;)V
    .locals 4

    invoke-virtual {p0}, Lcom/google/android/youtube/core/player/d;->c()V

    invoke-virtual {p1}, Lcom/google/android/youtube/core/model/Video;->couldHaveBranding()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/youtube/core/player/d;->f:Lcom/google/android/youtube/core/player/e;

    invoke-static {v0}, Lcom/google/android/youtube/core/async/p;->a(Lcom/google/android/youtube/core/async/n;)Lcom/google/android/youtube/core/async/p;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/youtube/core/player/d;->g:Lcom/google/android/youtube/core/async/p;

    iget-object v0, p0, Lcom/google/android/youtube/core/player/d;->a:Lcom/google/android/youtube/core/client/bc;

    iget-object v1, p1, Lcom/google/android/youtube/core/model/Video;->owner:Ljava/lang/String;

    iget-object v2, p0, Lcom/google/android/youtube/core/player/d;->d:Landroid/os/Handler;

    iget-object v3, p0, Lcom/google/android/youtube/core/player/d;->g:Lcom/google/android/youtube/core/async/p;

    invoke-static {v2, v3}, Lcom/google/android/youtube/core/async/am;->a(Landroid/os/Handler;Lcom/google/android/youtube/core/async/n;)Lcom/google/android/youtube/core/async/am;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Lcom/google/android/youtube/core/client/bc;->b(Ljava/lang/String;Lcom/google/android/youtube/core/async/n;)V

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/google/android/youtube/core/player/d;->c:Lcom/google/android/youtube/core/player/f;

    const/4 v1, 0x0

    invoke-interface {v0, v1}, Lcom/google/android/youtube/core/player/f;->a(Lcom/google/android/youtube/core/model/Branding;)V

    goto :goto_0
.end method

.method public final b()V
    .locals 0

    return-void
.end method

.method public final c()V
    .locals 2

    const/4 v1, 0x0

    iget-object v0, p0, Lcom/google/android/youtube/core/player/d;->g:Lcom/google/android/youtube/core/async/p;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/youtube/core/player/d;->g:Lcom/google/android/youtube/core/async/p;

    invoke-virtual {v0}, Lcom/google/android/youtube/core/async/p;->a()V

    iput-object v1, p0, Lcom/google/android/youtube/core/player/d;->g:Lcom/google/android/youtube/core/async/p;

    :cond_0
    iget-object v0, p0, Lcom/google/android/youtube/core/player/d;->h:Lcom/google/android/youtube/core/async/p;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/youtube/core/player/d;->h:Lcom/google/android/youtube/core/async/p;

    invoke-virtual {v0}, Lcom/google/android/youtube/core/async/p;->a()V

    iput-object v1, p0, Lcom/google/android/youtube/core/player/d;->h:Lcom/google/android/youtube/core/async/p;

    :cond_1
    iget-object v0, p0, Lcom/google/android/youtube/core/player/d;->i:Lcom/google/android/youtube/core/async/p;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/google/android/youtube/core/player/d;->i:Lcom/google/android/youtube/core/async/p;

    invoke-virtual {v0}, Lcom/google/android/youtube/core/async/p;->a()V

    iput-object v1, p0, Lcom/google/android/youtube/core/player/d;->i:Lcom/google/android/youtube/core/async/p;

    :cond_2
    iget-object v0, p0, Lcom/google/android/youtube/core/player/d;->e:Lcom/google/android/youtube/core/player/overlay/c;

    invoke-interface {v0}, Lcom/google/android/youtube/core/player/overlay/c;->e()V

    iput-object v1, p0, Lcom/google/android/youtube/core/player/d;->j:Landroid/net/Uri;

    return-void
.end method
