.class final Lcom/google/android/youtube/core/player/t;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/youtube/core/player/overlay/e;


# instance fields
.field final synthetic a:Lcom/google/android/youtube/core/player/Director;

.field private b:Z


# direct methods
.method public constructor <init>(Lcom/google/android/youtube/core/player/Director;)V
    .locals 1

    iput-object p1, p0, Lcom/google/android/youtube/core/player/t;->a:Lcom/google/android/youtube/core/player/Director;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/youtube/core/player/t;->b:Z

    return-void
.end method


# virtual methods
.method public final a()V
    .locals 2

    iget-object v0, p0, Lcom/google/android/youtube/core/player/t;->a:Lcom/google/android/youtube/core/player/Director;

    iget-boolean v1, p0, Lcom/google/android/youtube/core/player/t;->b:Z

    invoke-virtual {v0, v1}, Lcom/google/android/youtube/core/player/Director;->b(Z)V

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/youtube/core/player/t;->b:Z

    return-void
.end method

.method public final a(I)V
    .locals 2

    iget-object v0, p0, Lcom/google/android/youtube/core/player/t;->a:Lcom/google/android/youtube/core/player/Director;

    invoke-virtual {v0, p1}, Lcom/google/android/youtube/core/player/Director;->a(I)V

    iget-object v0, p0, Lcom/google/android/youtube/core/player/t;->a:Lcom/google/android/youtube/core/player/Director;

    iget-boolean v1, p0, Lcom/google/android/youtube/core/player/t;->b:Z

    invoke-virtual {v0, v1}, Lcom/google/android/youtube/core/player/Director;->b(Z)V

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/youtube/core/player/t;->b:Z

    return-void
.end method

.method public final a_(Lcom/google/android/youtube/core/model/SubtitleTrack;)V
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/core/player/t;->a:Lcom/google/android/youtube/core/player/Director;

    invoke-static {v0}, Lcom/google/android/youtube/core/player/Director;->A(Lcom/google/android/youtube/core/player/Director;)Lcom/google/android/youtube/core/player/an;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/google/android/youtube/core/player/an;->a(Lcom/google/android/youtube/core/model/SubtitleTrack;)V

    iget-object v0, p0, Lcom/google/android/youtube/core/player/t;->a:Lcom/google/android/youtube/core/player/Director;

    invoke-static {v0}, Lcom/google/android/youtube/core/player/Director;->o(Lcom/google/android/youtube/core/player/Director;)Lcom/google/android/youtube/core/player/ap;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/google/android/youtube/core/player/ap;->a(Lcom/google/android/youtube/core/model/SubtitleTrack;)V

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/youtube/core/player/t;->b:Z

    return-void
.end method

.method public final b()V
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/core/player/t;->a:Lcom/google/android/youtube/core/player/Director;

    invoke-virtual {v0}, Lcom/google/android/youtube/core/player/Director;->i()V

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/youtube/core/player/t;->b:Z

    return-void
.end method

.method public final b(Z)V
    .locals 4

    iget-object v0, p0, Lcom/google/android/youtube/core/player/t;->a:Lcom/google/android/youtube/core/player/Director;

    invoke-static {v0}, Lcom/google/android/youtube/core/player/Director;->a(Lcom/google/android/youtube/core/player/Director;)Lcom/google/android/youtube/core/Analytics;

    move-result-object v1

    const-string v2, "Fullscreen"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v0, "Button"

    invoke-direct {v3, v0}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    if-eqz p1, :cond_0

    const-string v0, "On"

    :goto_0
    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-interface {v1, v2, v0}, Lcom/google/android/youtube/core/Analytics;->a(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/google/android/youtube/core/player/t;->a:Lcom/google/android/youtube/core/player/Director;

    invoke-virtual {v0, p1}, Lcom/google/android/youtube/core/player/Director;->g(Z)V

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/youtube/core/player/t;->b:Z

    return-void

    :cond_0
    const-string v0, "Off"

    goto :goto_0
.end method

.method public final c()V
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/core/player/t;->a:Lcom/google/android/youtube/core/player/Director;

    invoke-virtual {v0}, Lcom/google/android/youtube/core/player/Director;->i()V

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/youtube/core/player/t;->b:Z

    return-void
.end method

.method public final d()V
    .locals 2

    iget-object v0, p0, Lcom/google/android/youtube/core/player/t;->a:Lcom/google/android/youtube/core/player/Director;

    iget-boolean v1, p0, Lcom/google/android/youtube/core/player/t;->b:Z

    invoke-virtual {v0, v1}, Lcom/google/android/youtube/core/player/Director;->c(Z)V

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/youtube/core/player/t;->b:Z

    return-void
.end method

.method public final e()V
    .locals 2

    iget-object v0, p0, Lcom/google/android/youtube/core/player/t;->a:Lcom/google/android/youtube/core/player/Director;

    iget-boolean v1, p0, Lcom/google/android/youtube/core/player/t;->b:Z

    invoke-virtual {v0, v1}, Lcom/google/android/youtube/core/player/Director;->d(Z)V

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/youtube/core/player/t;->b:Z

    return-void
.end method

.method public final f()V
    .locals 2

    iget-object v0, p0, Lcom/google/android/youtube/core/player/t;->a:Lcom/google/android/youtube/core/player/Director;

    iget-boolean v1, p0, Lcom/google/android/youtube/core/player/t;->b:Z

    invoke-virtual {v0, v1}, Lcom/google/android/youtube/core/player/Director;->e(Z)V

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/youtube/core/player/t;->b:Z

    return-void
.end method

.method public final g()V
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/core/player/t;->a:Lcom/google/android/youtube/core/player/Director;

    invoke-virtual {v0}, Lcom/google/android/youtube/core/player/Director;->m()V

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/youtube/core/player/t;->b:Z

    return-void
.end method

.method public final h()V
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/core/player/t;->a:Lcom/google/android/youtube/core/player/Director;

    invoke-static {v0}, Lcom/google/android/youtube/core/player/Director;->v(Lcom/google/android/youtube/core/player/Director;)Lcom/google/android/youtube/core/player/v;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/youtube/core/player/v;->i_()V

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/youtube/core/player/t;->b:Z

    return-void
.end method

.method public final i()V
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/core/player/t;->a:Lcom/google/android/youtube/core/player/Director;

    invoke-static {v0}, Lcom/google/android/youtube/core/player/Director;->v(Lcom/google/android/youtube/core/player/Director;)Lcom/google/android/youtube/core/player/v;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/youtube/core/player/v;->f()V

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/youtube/core/player/t;->b:Z

    return-void
.end method

.method public final j()V
    .locals 2

    iget-object v0, p0, Lcom/google/android/youtube/core/player/t;->a:Lcom/google/android/youtube/core/player/Director;

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lcom/google/android/youtube/core/player/Director;->b(Lcom/google/android/youtube/core/player/Director;Lcom/google/android/youtube/core/player/DirectorException;)Lcom/google/android/youtube/core/player/DirectorException;

    iget-object v0, p0, Lcom/google/android/youtube/core/player/t;->a:Lcom/google/android/youtube/core/player/Director;

    sget-object v1, Lcom/google/android/youtube/core/player/Director$PlaybackStage;->LOADED:Lcom/google/android/youtube/core/player/Director$PlaybackStage;

    invoke-static {v0, v1}, Lcom/google/android/youtube/core/player/Director;->b(Lcom/google/android/youtube/core/player/Director;Lcom/google/android/youtube/core/player/Director$PlaybackStage;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/youtube/core/player/t;->a:Lcom/google/android/youtube/core/player/Director;

    iget-boolean v1, p0, Lcom/google/android/youtube/core/player/t;->b:Z

    invoke-virtual {v0, v1}, Lcom/google/android/youtube/core/player/Director;->a(Z)V

    :goto_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/youtube/core/player/t;->b:Z

    return-void

    :cond_0
    iget-object v0, p0, Lcom/google/android/youtube/core/player/t;->a:Lcom/google/android/youtube/core/player/Director;

    iget-boolean v1, p0, Lcom/google/android/youtube/core/player/t;->b:Z

    invoke-virtual {v0, v1}, Lcom/google/android/youtube/core/player/Director;->b(Z)V

    goto :goto_0
.end method

.method public final k()V
    .locals 3

    iget-object v0, p0, Lcom/google/android/youtube/core/player/t;->a:Lcom/google/android/youtube/core/player/Director;

    invoke-static {v0}, Lcom/google/android/youtube/core/player/Director;->a(Lcom/google/android/youtube/core/player/Director;)Lcom/google/android/youtube/core/Analytics;

    move-result-object v0

    const-string v1, "Replay"

    invoke-interface {v0, v1}, Lcom/google/android/youtube/core/Analytics;->b(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/google/android/youtube/core/player/t;->a:Lcom/google/android/youtube/core/player/Director;

    invoke-static {v0}, Lcom/google/android/youtube/core/player/Director;->f(Lcom/google/android/youtube/core/player/Director;)Lcom/google/android/youtube/core/player/overlay/ControllerOverlay;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/youtube/core/player/overlay/ControllerOverlay;->f()V

    iget-object v0, p0, Lcom/google/android/youtube/core/player/t;->a:Lcom/google/android/youtube/core/player/Director;

    invoke-static {v0}, Lcom/google/android/youtube/core/player/Director;->z(Lcom/google/android/youtube/core/player/Director;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/youtube/core/player/t;->a:Lcom/google/android/youtube/core/player/Director;

    const-string v1, "PlayStarted"

    const/4 v2, -0x1

    invoke-static {v0, v1, v2}, Lcom/google/android/youtube/core/player/Director;->a(Lcom/google/android/youtube/core/player/Director;Ljava/lang/String;I)V

    :cond_0
    iget-object v0, p0, Lcom/google/android/youtube/core/player/t;->a:Lcom/google/android/youtube/core/player/Director;

    iget-boolean v1, p0, Lcom/google/android/youtube/core/player/t;->b:Z

    invoke-virtual {v0, v1}, Lcom/google/android/youtube/core/player/Director;->b(Z)V

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/youtube/core/player/t;->b:Z

    return-void
.end method

.method public final l()V
    .locals 1

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/youtube/core/player/t;->b:Z

    return-void
.end method
