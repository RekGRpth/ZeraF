.class public final Lcom/google/android/youtube/core/player/k;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/youtube/core/player/ak;


# instance fields
.field private final a:Lcom/google/android/youtube/core/utils/p;

.field private final b:Z

.field private final c:Z

.field private final d:Z

.field private final e:Z

.field private final f:Z


# direct methods
.method public constructor <init>(Lcom/google/android/youtube/core/utils/p;ZZZZ)V
    .locals 7

    const/4 v6, 0x0

    move-object v0, p0

    move-object v1, p1

    move v2, p2

    move v3, p3

    move v4, p4

    move v5, p5

    invoke-direct/range {v0 .. v6}, Lcom/google/android/youtube/core/player/k;-><init>(Lcom/google/android/youtube/core/utils/p;ZZZZZ)V

    return-void
.end method

.method private constructor <init>(Lcom/google/android/youtube/core/utils/p;ZZZZZ)V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const-string v0, "networkStatus cannot be null"

    invoke-static {p1, v0}, Lcom/google/android/youtube/core/utils/s;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/youtube/core/utils/p;

    iput-object v0, p0, Lcom/google/android/youtube/core/player/k;->a:Lcom/google/android/youtube/core/utils/p;

    iput-boolean p2, p0, Lcom/google/android/youtube/core/player/k;->b:Z

    iput-boolean p3, p0, Lcom/google/android/youtube/core/player/k;->c:Z

    iput-boolean p4, p0, Lcom/google/android/youtube/core/player/k;->d:Z

    iput-boolean p5, p0, Lcom/google/android/youtube/core/player/k;->e:Z

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/youtube/core/player/k;->f:Z

    return-void
.end method

.method private a(Ljava/util/Collection;Ljava/util/Set;ZZ)Lcom/google/android/youtube/core/model/ak;
    .locals 4

    new-instance v1, Ljava/util/HashSet;

    invoke-direct {v1, p1}, Ljava/util/HashSet;-><init>(Ljava/util/Collection;)V

    if-eqz p3, :cond_1

    sget-object v0, Lcom/google/android/youtube/core/model/Stream$Quality;->STREAM_HLS:Lcom/google/android/youtube/core/model/Stream$Quality;

    invoke-static {v1, v0}, Lcom/google/android/youtube/core/utils/af;->a(Ljava/util/Set;Lcom/google/android/youtube/core/model/Stream$Quality;)Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_0

    new-instance v0, Lcom/google/android/youtube/core/player/MissingStreamException;

    invoke-direct {v0}, Lcom/google/android/youtube/core/player/MissingStreamException;-><init>()V

    throw v0

    :cond_0
    new-instance v1, Lcom/google/android/youtube/core/model/ak;

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/youtube/core/model/Stream;

    invoke-direct {v1, v0}, Lcom/google/android/youtube/core/model/ak;-><init>(Lcom/google/android/youtube/core/model/Stream;)V

    move-object v0, v1

    :goto_0
    return-object v0

    :cond_1
    iget-boolean v0, p0, Lcom/google/android/youtube/core/player/k;->e:Z

    if-eqz v0, :cond_2

    if-eqz p4, :cond_2

    invoke-static {v1}, Lcom/google/android/youtube/core/utils/af;->a(Ljava/util/Set;)Z

    move-result v0

    if-eqz v0, :cond_2

    const/4 v0, 0x1

    :goto_1
    invoke-static {v1, v0}, Lcom/google/android/youtube/core/utils/af;->a(Ljava/util/Set;Z)V

    invoke-direct {p0, v1, p2}, Lcom/google/android/youtube/core/player/k;->a(Ljava/util/Set;Ljava/util/Set;)V

    invoke-interface {v1}, Ljava/util/Set;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_3

    new-instance v0, Lcom/google/android/youtube/core/player/MissingStreamException;

    invoke-direct {v0}, Lcom/google/android/youtube/core/player/MissingStreamException;-><init>()V

    throw v0

    :cond_2
    const/4 v0, 0x0

    goto :goto_1

    :cond_3
    iget-boolean v0, p0, Lcom/google/android/youtube/core/player/k;->f:Z

    if-nez v0, :cond_4

    sget-object v0, Lcom/google/android/youtube/core/model/Stream$Quality;->STREAM_HLS:Lcom/google/android/youtube/core/model/Stream$Quality;

    invoke-static {v1, v0}, Lcom/google/android/youtube/core/utils/af;->a(Ljava/util/Set;Lcom/google/android/youtube/core/model/Stream$Quality;)Ljava/util/Set;

    :cond_4
    invoke-interface {v1}, Ljava/util/Set;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_5

    new-instance v0, Lcom/google/android/youtube/core/player/MissingStreamException;

    invoke-direct {v0}, Lcom/google/android/youtube/core/player/MissingStreamException;-><init>()V

    throw v0

    :cond_5
    invoke-direct {p0}, Lcom/google/android/youtube/core/player/k;->b()[Lcom/google/android/youtube/core/model/Stream$Quality;

    move-result-object v0

    invoke-direct {p0}, Lcom/google/android/youtube/core/player/k;->c()[Lcom/google/android/youtube/core/model/Stream$Quality;

    move-result-object v2

    invoke-static {v1, v0}, Lcom/google/android/youtube/core/utils/af;->a(Ljava/util/Set;[Lcom/google/android/youtube/core/model/Stream$Quality;)Lcom/google/android/youtube/core/model/Stream;

    move-result-object v3

    if-eqz v3, :cond_6

    iget-object v0, v3, Lcom/google/android/youtube/core/model/Stream;->quality:Lcom/google/android/youtube/core/model/Stream$Quality;

    invoke-static {v1, v0}, Lcom/google/android/youtube/core/utils/af;->a(Ljava/util/Set;Lcom/google/android/youtube/core/model/Stream$Quality;)Ljava/util/Set;

    :cond_6
    invoke-static {v1, v2}, Lcom/google/android/youtube/core/utils/af;->a(Ljava/util/Set;[Lcom/google/android/youtube/core/model/Stream$Quality;)Lcom/google/android/youtube/core/model/Stream;

    move-result-object v0

    if-nez v0, :cond_7

    if-nez v3, :cond_7

    invoke-interface {v1}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/youtube/core/model/Stream;

    :cond_7
    new-instance v1, Lcom/google/android/youtube/core/model/ak;

    invoke-direct {v1, v3, v0}, Lcom/google/android/youtube/core/model/ak;-><init>(Lcom/google/android/youtube/core/model/Stream;Lcom/google/android/youtube/core/model/Stream;)V

    move-object v0, v1

    goto :goto_0
.end method

.method private a(Ljava/util/Set;Ljava/util/Set;)V
    .locals 7

    const/4 v2, 0x1

    const/4 v3, 0x0

    invoke-interface {p1}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :cond_0
    :goto_0
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_7

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/youtube/core/model/Stream;

    iget-boolean v1, p0, Lcom/google/android/youtube/core/player/k;->b:Z

    if-nez v1, :cond_2

    iget-object v1, v0, Lcom/google/android/youtube/core/model/Stream;->quality:Lcom/google/android/youtube/core/model/Stream$Quality;

    sget-object v5, Lcom/google/android/youtube/core/model/Stream$Quality;->STREAM_720P:Lcom/google/android/youtube/core/model/Stream$Quality;

    if-ne v1, v5, :cond_2

    move v1, v2

    :goto_1
    or-int/lit8 v5, v1, 0x0

    iget-boolean v1, p0, Lcom/google/android/youtube/core/player/k;->c:Z

    if-eqz v1, :cond_3

    iget-object v1, v0, Lcom/google/android/youtube/core/model/Stream;->quality:Lcom/google/android/youtube/core/model/Stream$Quality;

    sget-object v6, Lcom/google/android/youtube/core/model/Stream$Quality;->STREAM_720P:Lcom/google/android/youtube/core/model/Stream$Quality;

    if-eq v1, v6, :cond_1

    iget-object v1, v0, Lcom/google/android/youtube/core/model/Stream;->quality:Lcom/google/android/youtube/core/model/Stream$Quality;

    sget-object v6, Lcom/google/android/youtube/core/model/Stream$Quality;->STREAM_480P:Lcom/google/android/youtube/core/model/Stream$Quality;

    if-eq v1, v6, :cond_1

    iget-object v1, v0, Lcom/google/android/youtube/core/model/Stream;->quality:Lcom/google/android/youtube/core/model/Stream$Quality;

    sget-object v6, Lcom/google/android/youtube/core/model/Stream$Quality;->STREAM_360P:Lcom/google/android/youtube/core/model/Stream$Quality;

    if-ne v1, v6, :cond_3

    :cond_1
    move v1, v2

    :goto_2
    or-int/2addr v5, v1

    iget-boolean v1, p0, Lcom/google/android/youtube/core/player/k;->e:Z

    if-nez v1, :cond_4

    iget-boolean v1, v0, Lcom/google/android/youtube/core/model/Stream;->is3D:Z

    if-eqz v1, :cond_4

    move v1, v2

    :goto_3
    or-int/2addr v5, v1

    if-eqz p2, :cond_5

    iget-object v1, v0, Lcom/google/android/youtube/core/model/Stream;->mimeType:Ljava/lang/String;

    invoke-interface {p2, v1}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_5

    move v1, v2

    :goto_4
    or-int/2addr v1, v5

    iget-boolean v5, v0, Lcom/google/android/youtube/core/model/Stream;->is3D:Z

    if-eqz v5, :cond_6

    iget-object v5, v0, Lcom/google/android/youtube/core/model/Stream;->quality:Lcom/google/android/youtube/core/model/Stream$Quality;

    sget-object v6, Lcom/google/android/youtube/core/model/Stream$Quality;->STREAM_720P:Lcom/google/android/youtube/core/model/Stream$Quality;

    if-eq v5, v6, :cond_6

    iget-object v0, v0, Lcom/google/android/youtube/core/model/Stream;->quality:Lcom/google/android/youtube/core/model/Stream$Quality;

    sget-object v5, Lcom/google/android/youtube/core/model/Stream$Quality;->STREAM_360P:Lcom/google/android/youtube/core/model/Stream$Quality;

    if-eq v0, v5, :cond_6

    move v0, v2

    :goto_5
    or-int/2addr v0, v1

    if-eqz v0, :cond_0

    invoke-interface {v4}, Ljava/util/Iterator;->remove()V

    goto :goto_0

    :cond_2
    move v1, v3

    goto :goto_1

    :cond_3
    move v1, v3

    goto :goto_2

    :cond_4
    move v1, v3

    goto :goto_3

    :cond_5
    move v1, v3

    goto :goto_4

    :cond_6
    move v0, v3

    goto :goto_5

    :cond_7
    return-void
.end method

.method private b()[Lcom/google/android/youtube/core/model/Stream$Quality;
    .locals 4

    const/4 v3, 0x1

    const/4 v2, 0x0

    iget-object v0, p0, Lcom/google/android/youtube/core/player/k;->a:Lcom/google/android/youtube/core/utils/p;

    invoke-interface {v0}, Lcom/google/android/youtube/core/utils/p;->h()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x5

    new-array v0, v0, [Lcom/google/android/youtube/core/model/Stream$Quality;

    sget-object v1, Lcom/google/android/youtube/core/model/Stream$Quality;->STREAM_HLS:Lcom/google/android/youtube/core/model/Stream$Quality;

    aput-object v1, v0, v2

    sget-object v1, Lcom/google/android/youtube/core/model/Stream$Quality;->STREAM_720P:Lcom/google/android/youtube/core/model/Stream$Quality;

    aput-object v1, v0, v3

    const/4 v1, 0x2

    sget-object v2, Lcom/google/android/youtube/core/model/Stream$Quality;->STREAM_480P:Lcom/google/android/youtube/core/model/Stream$Quality;

    aput-object v2, v0, v1

    const/4 v1, 0x3

    sget-object v2, Lcom/google/android/youtube/core/model/Stream$Quality;->STREAM_405P:Lcom/google/android/youtube/core/model/Stream$Quality;

    aput-object v2, v0, v1

    const/4 v1, 0x4

    sget-object v2, Lcom/google/android/youtube/core/model/Stream$Quality;->STREAM_360P:Lcom/google/android/youtube/core/model/Stream$Quality;

    aput-object v2, v0, v1

    :goto_0
    return-object v0

    :cond_0
    new-array v0, v3, [Lcom/google/android/youtube/core/model/Stream$Quality;

    sget-object v1, Lcom/google/android/youtube/core/model/Stream$Quality;->STREAM_360P:Lcom/google/android/youtube/core/model/Stream$Quality;

    aput-object v1, v0, v2

    goto :goto_0
.end method

.method private c()[Lcom/google/android/youtube/core/model/Stream$Quality;
    .locals 5

    const/4 v1, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    iget-object v0, p0, Lcom/google/android/youtube/core/player/k;->a:Lcom/google/android/youtube/core/utils/p;

    invoke-interface {v0}, Lcom/google/android/youtube/core/utils/p;->h()Z

    move-result v0

    if-eqz v0, :cond_0

    new-array v0, v1, [Lcom/google/android/youtube/core/model/Stream$Quality;

    sget-object v1, Lcom/google/android/youtube/core/model/Stream$Quality;->STREAM_360P:Lcom/google/android/youtube/core/model/Stream$Quality;

    aput-object v1, v0, v2

    sget-object v1, Lcom/google/android/youtube/core/model/Stream$Quality;->STREAM_240P:Lcom/google/android/youtube/core/model/Stream$Quality;

    aput-object v1, v0, v3

    sget-object v1, Lcom/google/android/youtube/core/model/Stream$Quality;->STREAM_144P:Lcom/google/android/youtube/core/model/Stream$Quality;

    aput-object v1, v0, v4

    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/google/android/youtube/core/player/k;->a:Lcom/google/android/youtube/core/utils/p;

    invoke-interface {v0}, Lcom/google/android/youtube/core/utils/p;->e()Z

    move-result v0

    if-eqz v0, :cond_1

    new-array v0, v1, [Lcom/google/android/youtube/core/model/Stream$Quality;

    sget-object v1, Lcom/google/android/youtube/core/model/Stream$Quality;->STREAM_144P:Lcom/google/android/youtube/core/model/Stream$Quality;

    aput-object v1, v0, v2

    sget-object v1, Lcom/google/android/youtube/core/model/Stream$Quality;->STREAM_240P:Lcom/google/android/youtube/core/model/Stream$Quality;

    aput-object v1, v0, v3

    sget-object v1, Lcom/google/android/youtube/core/model/Stream$Quality;->STREAM_360P:Lcom/google/android/youtube/core/model/Stream$Quality;

    aput-object v1, v0, v4

    goto :goto_0

    :cond_1
    new-array v0, v1, [Lcom/google/android/youtube/core/model/Stream$Quality;

    sget-object v1, Lcom/google/android/youtube/core/model/Stream$Quality;->STREAM_240P:Lcom/google/android/youtube/core/model/Stream$Quality;

    aput-object v1, v0, v2

    sget-object v1, Lcom/google/android/youtube/core/model/Stream$Quality;->STREAM_144P:Lcom/google/android/youtube/core/model/Stream$Quality;

    aput-object v1, v0, v3

    sget-object v1, Lcom/google/android/youtube/core/model/Stream$Quality;->STREAM_360P:Lcom/google/android/youtube/core/model/Stream$Quality;

    aput-object v1, v0, v4

    goto :goto_0
.end method


# virtual methods
.method public final a(Ljava/util/Set;Z)Lcom/google/android/youtube/core/model/Stream$Quality;
    .locals 6

    if-eqz p2, :cond_1

    invoke-direct {p0}, Lcom/google/android/youtube/core/player/k;->b()[Lcom/google/android/youtube/core/model/Stream$Quality;

    move-result-object v0

    move-object v1, v0

    :goto_0
    array-length v3, v1

    const/4 v0, 0x0

    move v2, v0

    :goto_1
    if-ge v2, v3, :cond_3

    aget-object v4, v1, v2

    invoke-interface {p1}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v5

    :cond_0
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/youtube/core/model/Stream$Quality;

    if-ne v4, v0, :cond_0

    :goto_2
    return-object v0

    :cond_1
    invoke-direct {p0}, Lcom/google/android/youtube/core/player/k;->c()[Lcom/google/android/youtube/core/model/Stream$Quality;

    move-result-object v0

    move-object v1, v0

    goto :goto_0

    :cond_2
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_1

    :cond_3
    const/4 v0, 0x0

    goto :goto_2
.end method

.method public final a(Lcom/google/android/youtube/core/model/Video;Ljava/util/Set;Lcom/google/android/youtube/core/player/al;)V
    .locals 6

    const/4 v0, 0x1

    const/4 v1, 0x0

    :try_start_0
    iget-object v2, p1, Lcom/google/android/youtube/core/model/Video;->threeDSource:Lcom/google/android/youtube/core/model/Video$ThreeDSource;

    sget-object v3, Lcom/google/android/youtube/core/model/Video$ThreeDSource;->DECLARED:Lcom/google/android/youtube/core/model/Video$ThreeDSource;

    if-eq v2, v3, :cond_0

    iget-object v2, p1, Lcom/google/android/youtube/core/model/Video;->threeDSource:Lcom/google/android/youtube/core/model/Video$ThreeDSource;

    sget-object v3, Lcom/google/android/youtube/core/model/Video$ThreeDSource;->UPLOADED:Lcom/google/android/youtube/core/model/Video$ThreeDSource;

    if-ne v2, v3, :cond_1

    :cond_0
    move v2, v0

    :goto_0
    iget-object v3, p1, Lcom/google/android/youtube/core/model/Video;->streams:Ljava/util/Set;

    invoke-virtual {p1}, Lcom/google/android/youtube/core/model/Video;->isLive()Z

    move-result v4

    iget-boolean v5, p1, Lcom/google/android/youtube/core/model/Video;->is3d:Z

    if-eqz v5, :cond_2

    if-eqz v2, :cond_2

    :goto_1
    invoke-direct {p0, v3, p2, v4, v0}, Lcom/google/android/youtube/core/player/k;->a(Ljava/util/Collection;Ljava/util/Set;ZZ)Lcom/google/android/youtube/core/model/ak;

    move-result-object v0

    invoke-interface {p3, v0}, Lcom/google/android/youtube/core/player/al;->a(Lcom/google/android/youtube/core/model/ak;)V
    :try_end_0
    .catch Lcom/google/android/youtube/core/player/MissingStreamException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_2
    return-void

    :cond_1
    move v2, v1

    goto :goto_0

    :cond_2
    move v0, v1

    goto :goto_1

    :catch_0
    move-exception v0

    invoke-interface {p3, v0}, Lcom/google/android/youtube/core/player/al;->a(Ljava/lang/Exception;)V

    goto :goto_2
.end method

.method public final a(Ljava/util/Collection;Ljava/util/Set;Lcom/google/android/youtube/core/player/al;)V
    .locals 2

    const/4 v0, 0x0

    const/4 v1, 0x0

    :try_start_0
    invoke-direct {p0, p1, p2, v0, v1}, Lcom/google/android/youtube/core/player/k;->a(Ljava/util/Collection;Ljava/util/Set;ZZ)Lcom/google/android/youtube/core/model/ak;

    move-result-object v0

    invoke-interface {p3, v0}, Lcom/google/android/youtube/core/player/al;->a(Lcom/google/android/youtube/core/model/ak;)V
    :try_end_0
    .catch Lcom/google/android/youtube/core/player/MissingStreamException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    return-void

    :catch_0
    move-exception v0

    invoke-interface {p3, v0}, Lcom/google/android/youtube/core/player/al;->a(Ljava/lang/Exception;)V

    goto :goto_0
.end method

.method public final a()Z
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/core/player/k;->a:Lcom/google/android/youtube/core/utils/p;

    invoke-interface {v0}, Lcom/google/android/youtube/core/utils/p;->h()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/google/android/youtube/core/player/k;->d:Z

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/youtube/core/player/k;->a:Lcom/google/android/youtube/core/utils/p;

    invoke-interface {v0}, Lcom/google/android/youtube/core/utils/p;->b()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
