.class public final Lcom/google/android/youtube/core/player/overlay/DefaultAdOverlay;
.super Landroid/widget/RelativeLayout;
.source "SourceFile"

# interfaces
.implements Landroid/view/View$OnClickListener;
.implements Lcom/google/android/youtube/core/player/overlay/a;


# instance fields
.field private final a:Z

.field private final b:Landroid/widget/TextView;

.field private final c:Landroid/view/View;

.field private final d:Landroid/widget/TextView;

.field private final e:Landroid/widget/FrameLayout;

.field private final f:Landroid/widget/TextView;

.field private final g:F

.field private final h:I

.field private i:Lcom/google/android/youtube/core/player/overlay/b;

.field private j:Lcom/google/android/youtube/core/Analytics;

.field private k:Z

.field private l:Ljava/lang/String;

.field private m:Ljava/lang/String;

.field private n:Lcom/google/android/youtube/core/player/overlay/DefaultAdOverlay$State;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/google/android/youtube/core/Analytics;I)V
    .locals 1

    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, p3, v0}, Lcom/google/android/youtube/core/player/overlay/DefaultAdOverlay;-><init>(Landroid/content/Context;Lcom/google/android/youtube/core/Analytics;IZ)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Lcom/google/android/youtube/core/Analytics;IZ)V
    .locals 9

    const/4 v8, -0x1

    const/high16 v7, -0x78000000

    const v6, -0x333334

    const/4 v5, 0x0

    const/4 v4, -0x2

    invoke-direct {p0, p1}, Landroid/widget/RelativeLayout;-><init>(Landroid/content/Context;)V

    iput-object p2, p0, Lcom/google/android/youtube/core/player/overlay/DefaultAdOverlay;->j:Lcom/google/android/youtube/core/Analytics;

    iput-boolean p4, p0, Lcom/google/android/youtube/core/player/overlay/DefaultAdOverlay;->a:Z

    iput p3, p0, Lcom/google/android/youtube/core/player/overlay/DefaultAdOverlay;->h:I

    invoke-virtual {p0}, Lcom/google/android/youtube/core/player/overlay/DefaultAdOverlay;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v0

    iget v0, v0, Landroid/util/DisplayMetrics;->density:F

    iput v0, p0, Lcom/google/android/youtube/core/player/overlay/DefaultAdOverlay;->g:F

    new-instance v0, Landroid/widget/TextView;

    invoke-direct {v0, p1}, Landroid/widget/TextView;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/google/android/youtube/core/player/overlay/DefaultAdOverlay;->b:Landroid/widget/TextView;

    iget-object v0, p0, Lcom/google/android/youtube/core/player/overlay/DefaultAdOverlay;->b:Landroid/widget/TextView;

    invoke-virtual {v0, v6}, Landroid/widget/TextView;->setTextColor(I)V

    iget-object v0, p0, Lcom/google/android/youtube/core/player/overlay/DefaultAdOverlay;->b:Landroid/widget/TextView;

    invoke-virtual {v0, v5}, Landroid/widget/TextView;->setIncludeFontPadding(Z)V

    iget-object v0, p0, Lcom/google/android/youtube/core/player/overlay/DefaultAdOverlay;->b:Landroid/widget/TextView;

    const/4 v1, 0x3

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setGravity(I)V

    iget-object v0, p0, Lcom/google/android/youtube/core/player/overlay/DefaultAdOverlay;->b:Landroid/widget/TextView;

    sget-object v1, Landroid/text/TextUtils$TruncateAt;->END:Landroid/text/TextUtils$TruncateAt;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setEllipsize(Landroid/text/TextUtils$TruncateAt;)V

    iget-object v0, p0, Lcom/google/android/youtube/core/player/overlay/DefaultAdOverlay;->b:Landroid/widget/TextView;

    invoke-virtual {v0}, Landroid/widget/TextView;->setSingleLine()V

    const/4 v0, 0x4

    iget v1, p0, Lcom/google/android/youtube/core/player/overlay/DefaultAdOverlay;->g:F

    invoke-static {v0, v1}, Lcom/google/android/youtube/core/player/overlay/DefaultAdOverlay;->a(IF)I

    move-result v0

    if-nez p4, :cond_0

    new-instance v1, Landroid/widget/LinearLayout;

    invoke-direct {v1, p1}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;)V

    iget-object v2, p0, Lcom/google/android/youtube/core/player/overlay/DefaultAdOverlay;->b:Landroid/widget/TextView;

    new-instance v3, Landroid/widget/RelativeLayout$LayoutParams;

    invoke-direct {v3, v4, v4}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    invoke-virtual {v1, v2, v3}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    new-instance v2, Landroid/widget/TextView;

    invoke-direct {v2, p1}, Landroid/widget/TextView;-><init>(Landroid/content/Context;)V

    iput-object v2, p0, Lcom/google/android/youtube/core/player/overlay/DefaultAdOverlay;->d:Landroid/widget/TextView;

    iget-object v2, p0, Lcom/google/android/youtube/core/player/overlay/DefaultAdOverlay;->d:Landroid/widget/TextView;

    invoke-virtual {v2, v6}, Landroid/widget/TextView;->setTextColor(I)V

    iget-object v2, p0, Lcom/google/android/youtube/core/player/overlay/DefaultAdOverlay;->d:Landroid/widget/TextView;

    invoke-virtual {v2, v5}, Landroid/widget/TextView;->setIncludeFontPadding(Z)V

    iget-object v2, p0, Lcom/google/android/youtube/core/player/overlay/DefaultAdOverlay;->d:Landroid/widget/TextView;

    const/4 v3, 0x5

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setGravity(I)V

    iget-object v2, p0, Lcom/google/android/youtube/core/player/overlay/DefaultAdOverlay;->d:Landroid/widget/TextView;

    invoke-virtual {v2, p0}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v2, p0, Lcom/google/android/youtube/core/player/overlay/DefaultAdOverlay;->d:Landroid/widget/TextView;

    new-instance v3, Landroid/widget/RelativeLayout$LayoutParams;

    invoke-direct {v3, v8, v4}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    invoke-virtual {v1, v2, v3}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    invoke-virtual {v1, v0, v0, v0, v0}, Landroid/widget/LinearLayout;->setPadding(IIII)V

    invoke-virtual {v1, v7}, Landroid/widget/LinearLayout;->setBackgroundColor(I)V

    new-instance v0, Landroid/widget/RelativeLayout$LayoutParams;

    invoke-direct {v0, v8, v4}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    const/16 v2, 0xa

    invoke-virtual {v0, v2}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(I)V

    invoke-virtual {v1, v0}, Landroid/widget/LinearLayout;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    invoke-virtual {p0, v1}, Lcom/google/android/youtube/core/player/overlay/DefaultAdOverlay;->addView(Landroid/view/View;)V

    iput-object v1, p0, Lcom/google/android/youtube/core/player/overlay/DefaultAdOverlay;->c:Landroid/view/View;

    iget-object v0, p0, Lcom/google/android/youtube/core/player/overlay/DefaultAdOverlay;->b:Landroid/widget/TextView;

    invoke-virtual {v0, p0}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    :goto_0
    new-instance v0, Landroid/widget/FrameLayout;

    invoke-direct {v0, p1}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/google/android/youtube/core/player/overlay/DefaultAdOverlay;->e:Landroid/widget/FrameLayout;

    new-instance v0, Landroid/widget/TextView;

    invoke-direct {v0, p1}, Landroid/widget/TextView;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/google/android/youtube/core/player/overlay/DefaultAdOverlay;->f:Landroid/widget/TextView;

    iget-object v0, p0, Lcom/google/android/youtube/core/player/overlay/DefaultAdOverlay;->f:Landroid/widget/TextView;

    invoke-virtual {v0, v6}, Landroid/widget/TextView;->setTextColor(I)V

    iget-object v0, p0, Lcom/google/android/youtube/core/player/overlay/DefaultAdOverlay;->f:Landroid/widget/TextView;

    invoke-virtual {v0, v5}, Landroid/widget/TextView;->setIncludeFontPadding(Z)V

    iget-object v0, p0, Lcom/google/android/youtube/core/player/overlay/DefaultAdOverlay;->f:Landroid/widget/TextView;

    const/16 v1, 0x11

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setGravity(I)V

    iget-object v0, p0, Lcom/google/android/youtube/core/player/overlay/DefaultAdOverlay;->e:Landroid/widget/FrameLayout;

    iget-object v1, p0, Lcom/google/android/youtube/core/player/overlay/DefaultAdOverlay;->f:Landroid/widget/TextView;

    new-instance v2, Landroid/widget/RelativeLayout$LayoutParams;

    invoke-direct {v2, v4, v4}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    invoke-virtual {v0, v1, v2}, Landroid/widget/FrameLayout;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    const/16 v0, 0xf

    iget v1, p0, Lcom/google/android/youtube/core/player/overlay/DefaultAdOverlay;->g:F

    invoke-static {v0, v1}, Lcom/google/android/youtube/core/player/overlay/DefaultAdOverlay;->a(IF)I

    move-result v0

    iget-object v1, p0, Lcom/google/android/youtube/core/player/overlay/DefaultAdOverlay;->e:Landroid/widget/FrameLayout;

    invoke-virtual {v1, v0, v0, v0, v0}, Landroid/widget/FrameLayout;->setPadding(IIII)V

    iget-object v0, p0, Lcom/google/android/youtube/core/player/overlay/DefaultAdOverlay;->e:Landroid/widget/FrameLayout;

    const v1, 0x7f02007a

    invoke-virtual {v0, v1}, Landroid/widget/FrameLayout;->setBackgroundResource(I)V

    new-instance v0, Landroid/widget/RelativeLayout$LayoutParams;

    invoke-direct {v0, v4, v4}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    const/16 v1, 0xc

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(I)V

    const/16 v1, 0xb

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(I)V

    iget-object v1, p0, Lcom/google/android/youtube/core/player/overlay/DefaultAdOverlay;->e:Landroid/widget/FrameLayout;

    invoke-virtual {v1, v0}, Landroid/widget/FrameLayout;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    iget-object v0, p0, Lcom/google/android/youtube/core/player/overlay/DefaultAdOverlay;->e:Landroid/widget/FrameLayout;

    invoke-virtual {v0, p0}, Landroid/widget/FrameLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v0, p0, Lcom/google/android/youtube/core/player/overlay/DefaultAdOverlay;->e:Landroid/widget/FrameLayout;

    invoke-virtual {p0, v0}, Lcom/google/android/youtube/core/player/overlay/DefaultAdOverlay;->addView(Landroid/view/View;)V

    invoke-virtual {p0, v5}, Lcom/google/android/youtube/core/player/overlay/DefaultAdOverlay;->setFullscreen(Z)V

    const/4 v0, 0x4

    invoke-virtual {p0, v0}, Lcom/google/android/youtube/core/player/overlay/DefaultAdOverlay;->setVisibility(I)V

    return-void

    :cond_0
    iget-object v1, p0, Lcom/google/android/youtube/core/player/overlay/DefaultAdOverlay;->b:Landroid/widget/TextView;

    invoke-virtual {v1, v0, v0, v0, v0}, Landroid/widget/TextView;->setPadding(IIII)V

    iget-object v0, p0, Lcom/google/android/youtube/core/player/overlay/DefaultAdOverlay;->b:Landroid/widget/TextView;

    invoke-virtual {v0, v7}, Landroid/widget/TextView;->setBackgroundColor(I)V

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/youtube/core/player/overlay/DefaultAdOverlay;->d:Landroid/widget/TextView;

    new-instance v0, Landroid/widget/RelativeLayout$LayoutParams;

    invoke-direct {v0, v4, v4}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    const/16 v1, 0xa

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(I)V

    iget-object v1, p0, Lcom/google/android/youtube/core/player/overlay/DefaultAdOverlay;->b:Landroid/widget/TextView;

    invoke-virtual {p0, v1, v0}, Lcom/google/android/youtube/core/player/overlay/DefaultAdOverlay;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    iget-object v0, p0, Lcom/google/android/youtube/core/player/overlay/DefaultAdOverlay;->b:Landroid/widget/TextView;

    iput-object v0, p0, Lcom/google/android/youtube/core/player/overlay/DefaultAdOverlay;->c:Landroid/view/View;

    goto/16 :goto_0
.end method

.method private static a(IF)I
    .locals 2

    int-to-float v0, p0

    mul-float/2addr v0, p1

    const/high16 v1, 0x3f000000

    add-float/2addr v0, v1

    float-to-int v0, v0

    return v0
.end method

.method private a()V
    .locals 6

    const/4 v2, 0x1

    const/4 v1, 0x0

    iget-boolean v0, p0, Lcom/google/android/youtube/core/player/overlay/DefaultAdOverlay;->a:Z

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/google/android/youtube/core/player/overlay/DefaultAdOverlay;->k:Z

    if-nez v0, :cond_2

    :cond_0
    move v0, v2

    :goto_0
    iget-object v4, p0, Lcom/google/android/youtube/core/player/overlay/DefaultAdOverlay;->c:Landroid/view/View;

    if-eqz v0, :cond_3

    move v3, v1

    :goto_1
    invoke-virtual {v4, v3}, Landroid/view/View;->setVisibility(I)V

    if-eqz v0, :cond_1

    iget-boolean v0, p0, Lcom/google/android/youtube/core/player/overlay/DefaultAdOverlay;->k:Z

    if-nez v0, :cond_4

    iget-object v0, p0, Lcom/google/android/youtube/core/player/overlay/DefaultAdOverlay;->b:Landroid/widget/TextView;

    invoke-virtual {p0}, Lcom/google/android/youtube/core/player/overlay/DefaultAdOverlay;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f0b0077

    new-array v2, v2, [Ljava/lang/Object;

    iget-object v5, p0, Lcom/google/android/youtube/core/player/overlay/DefaultAdOverlay;->m:Ljava/lang/String;

    aput-object v5, v2, v1

    invoke-virtual {v3, v4, v2}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    :cond_1
    :goto_2
    return-void

    :cond_2
    move v0, v1

    goto :goto_0

    :cond_3
    const/16 v3, 0x8

    goto :goto_1

    :cond_4
    iget-object v0, p0, Lcom/google/android/youtube/core/player/overlay/DefaultAdOverlay;->l:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_5

    iget-object v0, p0, Lcom/google/android/youtube/core/player/overlay/DefaultAdOverlay;->b:Landroid/widget/TextView;

    invoke-virtual {p0}, Lcom/google/android/youtube/core/player/overlay/DefaultAdOverlay;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f0b0078

    new-array v2, v2, [Ljava/lang/Object;

    iget-object v5, p0, Lcom/google/android/youtube/core/player/overlay/DefaultAdOverlay;->l:Ljava/lang/String;

    aput-object v5, v2, v1

    invoke-virtual {v3, v4, v2}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_2

    :cond_5
    iget-object v0, p0, Lcom/google/android/youtube/core/player/overlay/DefaultAdOverlay;->b:Landroid/widget/TextView;

    invoke-virtual {p0}, Lcom/google/android/youtube/core/player/overlay/DefaultAdOverlay;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0b0079

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_2
.end method


# virtual methods
.method public final a(II)V
    .locals 6

    sub-int v0, p2, p1

    div-int/lit16 v0, v0, 0x3e8

    invoke-static {v0}, Lcom/google/android/youtube/core/utils/Util;->a(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/youtube/core/player/overlay/DefaultAdOverlay;->m:Ljava/lang/String;

    invoke-direct {p0}, Lcom/google/android/youtube/core/player/overlay/DefaultAdOverlay;->a()V

    iget-object v0, p0, Lcom/google/android/youtube/core/player/overlay/DefaultAdOverlay;->n:Lcom/google/android/youtube/core/player/overlay/DefaultAdOverlay$State;

    sget-object v1, Lcom/google/android/youtube/core/player/overlay/DefaultAdOverlay$State;->WAITING_TO_SKIP:Lcom/google/android/youtube/core/player/overlay/DefaultAdOverlay$State;

    if-ne v0, v1, :cond_0

    div-int/lit16 v0, p1, 0x3e8

    rsub-int/lit8 v0, v0, 0x5

    if-gtz v0, :cond_1

    sget-object v0, Lcom/google/android/youtube/core/player/overlay/DefaultAdOverlay$State;->SKIPPABLE:Lcom/google/android/youtube/core/player/overlay/DefaultAdOverlay$State;

    iput-object v0, p0, Lcom/google/android/youtube/core/player/overlay/DefaultAdOverlay;->n:Lcom/google/android/youtube/core/player/overlay/DefaultAdOverlay$State;

    iget-object v0, p0, Lcom/google/android/youtube/core/player/overlay/DefaultAdOverlay;->f:Landroid/widget/TextView;

    invoke-virtual {p0}, Lcom/google/android/youtube/core/player/overlay/DefaultAdOverlay;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0b007c

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lcom/google/android/youtube/core/player/overlay/DefaultAdOverlay;->i:Lcom/google/android/youtube/core/player/overlay/b;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/youtube/core/player/overlay/DefaultAdOverlay;->i:Lcom/google/android/youtube/core/player/overlay/b;

    invoke-interface {v0}, Lcom/google/android/youtube/core/player/overlay/b;->c()V

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v1, p0, Lcom/google/android/youtube/core/player/overlay/DefaultAdOverlay;->f:Landroid/widget/TextView;

    invoke-virtual {p0}, Lcom/google/android/youtube/core/player/overlay/DefaultAdOverlay;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0b007b

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    aput-object v0, v4, v5

    invoke-virtual {v2, v3, v4}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0
.end method

.method public final a(Ljava/lang/String;ZZLjava/lang/String;)V
    .locals 6

    const/4 v5, 0x0

    iput-object p1, p0, Lcom/google/android/youtube/core/player/overlay/DefaultAdOverlay;->l:Ljava/lang/String;

    const-string v0, ""

    iput-object v0, p0, Lcom/google/android/youtube/core/player/overlay/DefaultAdOverlay;->m:Ljava/lang/String;

    invoke-direct {p0}, Lcom/google/android/youtube/core/player/overlay/DefaultAdOverlay;->a()V

    if-eqz p2, :cond_2

    sget-object v0, Lcom/google/android/youtube/core/player/overlay/DefaultAdOverlay$State;->WAITING_TO_SKIP:Lcom/google/android/youtube/core/player/overlay/DefaultAdOverlay$State;

    iput-object v0, p0, Lcom/google/android/youtube/core/player/overlay/DefaultAdOverlay;->n:Lcom/google/android/youtube/core/player/overlay/DefaultAdOverlay$State;

    iget-object v0, p0, Lcom/google/android/youtube/core/player/overlay/DefaultAdOverlay;->e:Landroid/widget/FrameLayout;

    invoke-virtual {v0, v5}, Landroid/widget/FrameLayout;->setVisibility(I)V

    iget-object v0, p0, Lcom/google/android/youtube/core/player/overlay/DefaultAdOverlay;->f:Landroid/widget/TextView;

    invoke-virtual {p0}, Lcom/google/android/youtube/core/player/overlay/DefaultAdOverlay;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0b007b

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x5

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v3, v5

    invoke-virtual {v1, v2, v3}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    :cond_0
    :goto_0
    iget-boolean v0, p0, Lcom/google/android/youtube/core/player/overlay/DefaultAdOverlay;->a:Z

    if-nez v0, :cond_1

    iget-object v1, p0, Lcom/google/android/youtube/core/player/overlay/DefaultAdOverlay;->d:Landroid/widget/TextView;

    if-eqz p3, :cond_3

    invoke-virtual {p0}, Lcom/google/android/youtube/core/player/overlay/DefaultAdOverlay;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v2, 0x7f0b007a

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    :goto_1
    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    :cond_1
    invoke-virtual {p0, v5}, Lcom/google/android/youtube/core/player/overlay/DefaultAdOverlay;->setVisibility(I)V

    return-void

    :cond_2
    sget-object v0, Lcom/google/android/youtube/core/player/overlay/DefaultAdOverlay$State;->NOT_SKIPPABLE:Lcom/google/android/youtube/core/player/overlay/DefaultAdOverlay$State;

    iput-object v0, p0, Lcom/google/android/youtube/core/player/overlay/DefaultAdOverlay;->n:Lcom/google/android/youtube/core/player/overlay/DefaultAdOverlay$State;

    iget-object v0, p0, Lcom/google/android/youtube/core/player/overlay/DefaultAdOverlay;->e:Landroid/widget/FrameLayout;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/youtube/core/player/overlay/DefaultAdOverlay;->e:Landroid/widget/FrameLayout;

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Landroid/widget/FrameLayout;->setVisibility(I)V

    goto :goto_0

    :cond_3
    const/4 v0, 0x0

    goto :goto_1
.end method

.method public final b()Landroid/view/View;
    .locals 0

    return-object p0
.end method

.method public final c()Lcom/google/android/youtube/core/player/overlay/PlayerOverlaysLayout$LayoutParams;
    .locals 3

    const/4 v2, -0x1

    new-instance v0, Lcom/google/android/youtube/core/player/overlay/PlayerOverlaysLayout$LayoutParams;

    const/4 v1, 0x1

    invoke-direct {v0, v2, v2, v1}, Lcom/google/android/youtube/core/player/overlay/PlayerOverlaysLayout$LayoutParams;-><init>(IIZ)V

    return-object v0
.end method

.method public final d()V
    .locals 1

    const/4 v0, 0x4

    invoke-virtual {p0, v0}, Lcom/google/android/youtube/core/player/overlay/DefaultAdOverlay;->setVisibility(I)V

    return-void
.end method

.method public final onClick(Landroid/view/View;)V
    .locals 3

    iget-object v0, p0, Lcom/google/android/youtube/core/player/overlay/DefaultAdOverlay;->i:Lcom/google/android/youtube/core/player/overlay/b;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/youtube/core/player/overlay/DefaultAdOverlay;->d:Landroid/widget/TextView;

    if-ne p1, v0, :cond_1

    iget-object v0, p0, Lcom/google/android/youtube/core/player/overlay/DefaultAdOverlay;->j:Lcom/google/android/youtube/core/Analytics;

    const-string v1, "LearnMore"

    const-string v2, "Overlay"

    invoke-interface {v0, v1, v2}, Lcom/google/android/youtube/core/Analytics;->a(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/google/android/youtube/core/player/overlay/DefaultAdOverlay;->i:Lcom/google/android/youtube/core/player/overlay/b;

    invoke-interface {v0}, Lcom/google/android/youtube/core/player/overlay/b;->b()V

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v0, p0, Lcom/google/android/youtube/core/player/overlay/DefaultAdOverlay;->e:Landroid/widget/FrameLayout;

    if-ne p1, v0, :cond_2

    iget-object v0, p0, Lcom/google/android/youtube/core/player/overlay/DefaultAdOverlay;->n:Lcom/google/android/youtube/core/player/overlay/DefaultAdOverlay$State;

    sget-object v1, Lcom/google/android/youtube/core/player/overlay/DefaultAdOverlay$State;->SKIPPABLE:Lcom/google/android/youtube/core/player/overlay/DefaultAdOverlay$State;

    if-ne v0, v1, :cond_2

    iget-object v0, p0, Lcom/google/android/youtube/core/player/overlay/DefaultAdOverlay;->j:Lcom/google/android/youtube/core/Analytics;

    const-string v1, "SkipAd"

    const-string v2, "Overlay"

    invoke-interface {v0, v1, v2}, Lcom/google/android/youtube/core/Analytics;->a(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/google/android/youtube/core/player/overlay/DefaultAdOverlay;->i:Lcom/google/android/youtube/core/player/overlay/b;

    invoke-interface {v0}, Lcom/google/android/youtube/core/player/overlay/b;->d()V

    goto :goto_0

    :cond_2
    iget-object v0, p0, Lcom/google/android/youtube/core/player/overlay/DefaultAdOverlay;->b:Landroid/widget/TextView;

    if-ne p1, v0, :cond_0

    iget-boolean v0, p0, Lcom/google/android/youtube/core/player/overlay/DefaultAdOverlay;->k:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/youtube/core/player/overlay/DefaultAdOverlay;->j:Lcom/google/android/youtube/core/Analytics;

    const-string v1, "GotoAd"

    const-string v2, "Overlay"

    invoke-interface {v0, v1, v2}, Lcom/google/android/youtube/core/Analytics;->a(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/google/android/youtube/core/player/overlay/DefaultAdOverlay;->i:Lcom/google/android/youtube/core/player/overlay/b;

    invoke-interface {v0}, Lcom/google/android/youtube/core/player/overlay/b;->e()V

    goto :goto_0
.end method

.method public final setFullscreen(Z)V
    .locals 4

    const/4 v2, 0x0

    iput-boolean p1, p0, Lcom/google/android/youtube/core/player/overlay/DefaultAdOverlay;->k:Z

    iget-object v0, p0, Lcom/google/android/youtube/core/player/overlay/DefaultAdOverlay;->e:Landroid/widget/FrameLayout;

    invoke-virtual {v0}, Landroid/widget/FrameLayout;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/widget/RelativeLayout$LayoutParams;

    const/16 v1, 0x19

    iget v3, p0, Lcom/google/android/youtube/core/player/overlay/DefaultAdOverlay;->g:F

    invoke-static {v1, v3}, Lcom/google/android/youtube/core/player/overlay/DefaultAdOverlay;->a(IF)I

    move-result v3

    if-eqz p1, :cond_0

    iget v1, p0, Lcom/google/android/youtube/core/player/overlay/DefaultAdOverlay;->h:I

    :goto_0
    add-int/2addr v1, v3

    invoke-virtual {v0, v2, v2, v2, v1}, Landroid/widget/RelativeLayout$LayoutParams;->setMargins(IIII)V

    invoke-direct {p0}, Lcom/google/android/youtube/core/player/overlay/DefaultAdOverlay;->a()V

    return-void

    :cond_0
    move v1, v2

    goto :goto_0
.end method

.method public final setListener(Lcom/google/android/youtube/core/player/overlay/b;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/youtube/core/player/overlay/DefaultAdOverlay;->i:Lcom/google/android/youtube/core/player/overlay/b;

    return-void
.end method
