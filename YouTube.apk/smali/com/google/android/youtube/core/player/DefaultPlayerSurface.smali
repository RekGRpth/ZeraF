.class public Lcom/google/android/youtube/core/player/DefaultPlayerSurface;
.super Landroid/view/ViewGroup;
.source "SourceFile"

# interfaces
.implements Landroid/view/SurfaceHolder$Callback;
.implements Lcom/google/android/youtube/core/player/af;


# instance fields
.field private final a:Lcom/google/android/youtube/core/player/i;

.field private final b:Landroid/view/View;

.field private final c:Ljava/lang/Runnable;

.field private d:Lcom/google/android/youtube/core/player/ah;

.field private e:Lcom/google/android/youtube/core/player/ag;

.field private f:I

.field private g:I

.field private h:Z

.field private i:I

.field private j:I

.field private k:I

.field private l:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 2

    invoke-direct {p0, p1}, Landroid/view/ViewGroup;-><init>(Landroid/content/Context;)V

    new-instance v0, Lcom/google/android/youtube/core/player/i;

    invoke-direct {v0, p0, p1}, Lcom/google/android/youtube/core/player/i;-><init>(Lcom/google/android/youtube/core/player/DefaultPlayerSurface;Landroid/content/Context;)V

    iput-object v0, p0, Lcom/google/android/youtube/core/player/DefaultPlayerSurface;->a:Lcom/google/android/youtube/core/player/i;

    iget-object v0, p0, Lcom/google/android/youtube/core/player/DefaultPlayerSurface;->a:Lcom/google/android/youtube/core/player/i;

    invoke-virtual {v0}, Lcom/google/android/youtube/core/player/i;->getHolder()Landroid/view/SurfaceHolder;

    move-result-object v0

    const/4 v1, 0x3

    invoke-interface {v0, v1}, Landroid/view/SurfaceHolder;->setType(I)V

    invoke-interface {v0, p0}, Landroid/view/SurfaceHolder;->addCallback(Landroid/view/SurfaceHolder$Callback;)V

    new-instance v0, Landroid/view/View;

    invoke-direct {v0, p1}, Landroid/view/View;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/google/android/youtube/core/player/DefaultPlayerSurface;->b:Landroid/view/View;

    iget-object v0, p0, Lcom/google/android/youtube/core/player/DefaultPlayerSurface;->b:Landroid/view/View;

    const/high16 v1, -0x1000000

    invoke-virtual {v0, v1}, Landroid/view/View;->setBackgroundColor(I)V

    new-instance v0, Lcom/google/android/youtube/core/player/h;

    invoke-direct {v0, p0}, Lcom/google/android/youtube/core/player/h;-><init>(Lcom/google/android/youtube/core/player/DefaultPlayerSurface;)V

    iput-object v0, p0, Lcom/google/android/youtube/core/player/DefaultPlayerSurface;->c:Ljava/lang/Runnable;

    iget-object v0, p0, Lcom/google/android/youtube/core/player/DefaultPlayerSurface;->a:Lcom/google/android/youtube/core/player/i;

    invoke-virtual {p0, v0}, Lcom/google/android/youtube/core/player/DefaultPlayerSurface;->addView(Landroid/view/View;)V

    iget-object v0, p0, Lcom/google/android/youtube/core/player/DefaultPlayerSurface;->b:Landroid/view/View;

    invoke-virtual {p0, v0}, Lcom/google/android/youtube/core/player/DefaultPlayerSurface;->addView(Landroid/view/View;)V

    sget v0, Lcom/google/android/youtube/core/utils/Util;->a:I

    const/16 v1, 0x10

    if-lt v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    iput-boolean v0, p0, Lcom/google/android/youtube/core/player/DefaultPlayerSurface;->h:Z

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method static synthetic a(Lcom/google/android/youtube/core/player/DefaultPlayerSurface;I)I
    .locals 0

    iput p1, p0, Lcom/google/android/youtube/core/player/DefaultPlayerSurface;->j:I

    return p1
.end method

.method static synthetic a(Lcom/google/android/youtube/core/player/DefaultPlayerSurface;)Landroid/view/View;
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/core/player/DefaultPlayerSurface;->b:Landroid/view/View;

    return-object v0
.end method

.method static synthetic b(Lcom/google/android/youtube/core/player/DefaultPlayerSurface;I)I
    .locals 0

    iput p1, p0, Lcom/google/android/youtube/core/player/DefaultPlayerSurface;->k:I

    return p1
.end method

.method static synthetic b(Lcom/google/android/youtube/core/player/DefaultPlayerSurface;)Lcom/google/android/youtube/core/player/ah;
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/core/player/DefaultPlayerSurface;->d:Lcom/google/android/youtube/core/player/ah;

    return-object v0
.end method

.method static synthetic c(Lcom/google/android/youtube/core/player/DefaultPlayerSurface;)I
    .locals 1

    iget v0, p0, Lcom/google/android/youtube/core/player/DefaultPlayerSurface;->f:I

    return v0
.end method

.method static synthetic d(Lcom/google/android/youtube/core/player/DefaultPlayerSurface;)I
    .locals 1

    iget v0, p0, Lcom/google/android/youtube/core/player/DefaultPlayerSurface;->g:I

    return v0
.end method

.method static synthetic e(Lcom/google/android/youtube/core/player/DefaultPlayerSurface;)Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/youtube/core/player/DefaultPlayerSurface;->h:Z

    return v0
.end method

.method static synthetic f(Lcom/google/android/youtube/core/player/DefaultPlayerSurface;)I
    .locals 1

    iget v0, p0, Lcom/google/android/youtube/core/player/DefaultPlayerSurface;->i:I

    return v0
.end method


# virtual methods
.method public final a(Lcom/google/android/youtube/core/player/ad;)V
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/core/player/DefaultPlayerSurface;->a:Lcom/google/android/youtube/core/player/i;

    invoke-virtual {v0}, Lcom/google/android/youtube/core/player/i;->getHolder()Landroid/view/SurfaceHolder;

    move-result-object v0

    invoke-interface {p1, v0}, Lcom/google/android/youtube/core/player/ad;->a(Landroid/view/SurfaceHolder;)V

    return-void
.end method

.method public final a(Z)Z
    .locals 2

    iget-boolean v0, p0, Lcom/google/android/youtube/core/player/DefaultPlayerSurface;->h:Z

    if-eqz v0, :cond_1

    if-eqz p1, :cond_0

    sget v0, Lcom/google/android/youtube/core/utils/Util;->a:I

    const/16 v1, 0x11

    if-lt v0, v1, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final b()V
    .locals 4

    iget-object v0, p0, Lcom/google/android/youtube/core/player/DefaultPlayerSurface;->b:Landroid/view/View;

    iget-object v1, p0, Lcom/google/android/youtube/core/player/DefaultPlayerSurface;->c:Ljava/lang/Runnable;

    const-wide/16 v2, 0x1f4

    invoke-virtual {v0, v1, v2, v3}, Landroid/view/View;->postDelayed(Ljava/lang/Runnable;J)Z

    return-void
.end method

.method public final c()V
    .locals 2

    iget-object v0, p0, Lcom/google/android/youtube/core/player/DefaultPlayerSurface;->b:Landroid/view/View;

    iget-object v1, p0, Lcom/google/android/youtube/core/player/DefaultPlayerSurface;->c:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Landroid/view/View;->removeCallbacks(Ljava/lang/Runnable;)Z

    iget-object v0, p0, Lcom/google/android/youtube/core/player/DefaultPlayerSurface;->b:Landroid/view/View;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    return-void
.end method

.method public final d()V
    .locals 2

    iget-object v0, p0, Lcom/google/android/youtube/core/player/DefaultPlayerSurface;->a:Lcom/google/android/youtube/core/player/i;

    invoke-virtual {p0, v0}, Lcom/google/android/youtube/core/player/DefaultPlayerSurface;->removeView(Landroid/view/View;)V

    iget-object v0, p0, Lcom/google/android/youtube/core/player/DefaultPlayerSurface;->a:Lcom/google/android/youtube/core/player/i;

    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, Lcom/google/android/youtube/core/player/DefaultPlayerSurface;->addView(Landroid/view/View;I)V

    return-void
.end method

.method public final e()V
    .locals 2

    sget v0, Lcom/google/android/youtube/core/utils/Util;->a:I

    const/16 v1, 0xe

    if-lt v0, v1, :cond_0

    iget-object v0, p0, Lcom/google/android/youtube/core/player/DefaultPlayerSurface;->a:Lcom/google/android/youtube/core/player/i;

    invoke-virtual {v0}, Lcom/google/android/youtube/core/player/i;->getHolder()Landroid/view/SurfaceHolder;

    move-result-object v0

    invoke-interface {v0}, Landroid/view/SurfaceHolder;->getSurface()Landroid/view/Surface;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Landroid/view/Surface;->release()V

    :cond_0
    return-void
.end method

.method public final f()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/youtube/core/player/DefaultPlayerSurface;->l:Z

    return v0
.end method

.method protected onLayout(ZIIII)V
    .locals 4

    const/4 v3, 0x0

    iget-object v0, p0, Lcom/google/android/youtube/core/player/DefaultPlayerSurface;->a:Lcom/google/android/youtube/core/player/i;

    iget-object v1, p0, Lcom/google/android/youtube/core/player/DefaultPlayerSurface;->a:Lcom/google/android/youtube/core/player/i;

    invoke-virtual {v1}, Lcom/google/android/youtube/core/player/i;->getMeasuredWidth()I

    move-result v1

    iget-object v2, p0, Lcom/google/android/youtube/core/player/DefaultPlayerSurface;->a:Lcom/google/android/youtube/core/player/i;

    invoke-virtual {v2}, Lcom/google/android/youtube/core/player/i;->getMeasuredHeight()I

    move-result v2

    invoke-virtual {v0, v3, v3, v1, v2}, Lcom/google/android/youtube/core/player/i;->layout(IIII)V

    iget-object v0, p0, Lcom/google/android/youtube/core/player/DefaultPlayerSurface;->b:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getVisibility()I

    move-result v0

    const/16 v1, 0x8

    if-eq v0, v1, :cond_0

    iget-object v0, p0, Lcom/google/android/youtube/core/player/DefaultPlayerSurface;->b:Landroid/view/View;

    iget-object v1, p0, Lcom/google/android/youtube/core/player/DefaultPlayerSurface;->b:Landroid/view/View;

    invoke-virtual {v1}, Landroid/view/View;->getMeasuredWidth()I

    move-result v1

    iget-object v2, p0, Lcom/google/android/youtube/core/player/DefaultPlayerSurface;->b:Landroid/view/View;

    invoke-virtual {v2}, Landroid/view/View;->getMeasuredHeight()I

    move-result v2

    invoke-virtual {v0, v3, v3, v1, v2}, Landroid/view/View;->layout(IIII)V

    :cond_0
    return-void
.end method

.method protected onMeasure(II)V
    .locals 5

    const/high16 v4, 0x40000000

    iget-object v0, p0, Lcom/google/android/youtube/core/player/DefaultPlayerSurface;->a:Lcom/google/android/youtube/core/player/i;

    invoke-virtual {v0, p1, p2}, Lcom/google/android/youtube/core/player/i;->measure(II)V

    iget-object v0, p0, Lcom/google/android/youtube/core/player/DefaultPlayerSurface;->a:Lcom/google/android/youtube/core/player/i;

    invoke-virtual {v0}, Lcom/google/android/youtube/core/player/i;->getMeasuredWidth()I

    move-result v0

    iget-object v1, p0, Lcom/google/android/youtube/core/player/DefaultPlayerSurface;->a:Lcom/google/android/youtube/core/player/i;

    invoke-virtual {v1}, Lcom/google/android/youtube/core/player/i;->getMeasuredHeight()I

    move-result v1

    iget-object v2, p0, Lcom/google/android/youtube/core/player/DefaultPlayerSurface;->b:Landroid/view/View;

    invoke-virtual {v2}, Landroid/view/View;->getVisibility()I

    move-result v2

    const/16 v3, 0x8

    if-eq v2, v3, :cond_0

    iget-object v2, p0, Lcom/google/android/youtube/core/player/DefaultPlayerSurface;->b:Landroid/view/View;

    invoke-static {v0, v4}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v3

    invoke-static {v1, v4}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v4

    invoke-virtual {v2, v3, v4}, Landroid/view/View;->measure(II)V

    :cond_0
    invoke-virtual {p0, v0, v1}, Lcom/google/android/youtube/core/player/DefaultPlayerSurface;->setMeasuredDimension(II)V

    return-void
.end method

.method public setListener(Lcom/google/android/youtube/core/player/ag;)V
    .locals 1

    const-string v0, "listener cannot be null"

    invoke-static {p1, v0}, Lcom/google/android/youtube/core/utils/s;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/youtube/core/player/ag;

    iput-object v0, p0, Lcom/google/android/youtube/core/player/DefaultPlayerSurface;->e:Lcom/google/android/youtube/core/player/ag;

    return-void
.end method

.method public setOnLetterboxChangedListener(Lcom/google/android/youtube/core/player/ah;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/youtube/core/player/DefaultPlayerSurface;->d:Lcom/google/android/youtube/core/player/ah;

    return-void
.end method

.method public setVideoSize(II)V
    .locals 1

    iput p1, p0, Lcom/google/android/youtube/core/player/DefaultPlayerSurface;->f:I

    iput p2, p0, Lcom/google/android/youtube/core/player/DefaultPlayerSurface;->g:I

    iget-object v0, p0, Lcom/google/android/youtube/core/player/DefaultPlayerSurface;->a:Lcom/google/android/youtube/core/player/i;

    invoke-virtual {v0}, Lcom/google/android/youtube/core/player/i;->requestLayout()V

    return-void
.end method

.method public setZoom(I)V
    .locals 1

    iget v0, p0, Lcom/google/android/youtube/core/player/DefaultPlayerSurface;->i:I

    if-eq v0, p1, :cond_0

    iput p1, p0, Lcom/google/android/youtube/core/player/DefaultPlayerSurface;->i:I

    invoke-virtual {p0}, Lcom/google/android/youtube/core/player/DefaultPlayerSurface;->requestLayout()V

    :cond_0
    return-void
.end method

.method public surfaceChanged(Landroid/view/SurfaceHolder;III)V
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/core/player/DefaultPlayerSurface;->e:Lcom/google/android/youtube/core/player/ag;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/youtube/core/player/DefaultPlayerSurface;->e:Lcom/google/android/youtube/core/player/ag;

    invoke-interface {v0}, Lcom/google/android/youtube/core/player/ag;->b()V

    :cond_0
    return-void
.end method

.method public surfaceCreated(Landroid/view/SurfaceHolder;)V
    .locals 1

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/youtube/core/player/DefaultPlayerSurface;->l:Z

    iget-object v0, p0, Lcom/google/android/youtube/core/player/DefaultPlayerSurface;->e:Lcom/google/android/youtube/core/player/ag;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/youtube/core/player/DefaultPlayerSurface;->e:Lcom/google/android/youtube/core/player/ag;

    invoke-interface {v0}, Lcom/google/android/youtube/core/player/ag;->a()V

    :cond_0
    return-void
.end method

.method public surfaceDestroyed(Landroid/view/SurfaceHolder;)V
    .locals 1

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/youtube/core/player/DefaultPlayerSurface;->l:Z

    iget-object v0, p0, Lcom/google/android/youtube/core/player/DefaultPlayerSurface;->e:Lcom/google/android/youtube/core/player/ag;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/youtube/core/player/DefaultPlayerSurface;->e:Lcom/google/android/youtube/core/player/ag;

    invoke-interface {v0}, Lcom/google/android/youtube/core/player/ag;->c()V

    :cond_0
    invoke-virtual {p0}, Lcom/google/android/youtube/core/player/DefaultPlayerSurface;->c()V

    return-void
.end method
