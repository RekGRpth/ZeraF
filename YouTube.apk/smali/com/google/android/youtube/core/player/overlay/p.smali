.class public final Lcom/google/android/youtube/core/player/overlay/p;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field private final a:Lcom/google/android/youtube/core/player/overlay/q;

.field private b:I

.field private c:I

.field private d:I

.field private e:I


# direct methods
.method public constructor <init>(Lcom/google/android/youtube/core/player/overlay/q;)V
    .locals 2

    const/high16 v1, -0x80000000

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    invoke-static {p1}, Lcom/google/android/youtube/core/utils/s;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/youtube/core/player/overlay/q;

    iput-object v0, p0, Lcom/google/android/youtube/core/player/overlay/p;->a:Lcom/google/android/youtube/core/player/overlay/q;

    iput v1, p0, Lcom/google/android/youtube/core/player/overlay/p;->b:I

    iput v1, p0, Lcom/google/android/youtube/core/player/overlay/p;->c:I

    iput v1, p0, Lcom/google/android/youtube/core/player/overlay/p;->d:I

    const/4 v0, 0x0

    iput v0, p0, Lcom/google/android/youtube/core/player/overlay/p;->e:I

    return-void
.end method

.method public static b(I)Z
    .locals 1

    const/16 v0, 0x5a

    if-eq p0, v0, :cond_0

    const/16 v0, 0x57

    if-eq p0, v0, :cond_0

    const/16 v0, 0x55

    if-eq p0, v0, :cond_0

    const/16 v0, 0x58

    if-eq p0, v0, :cond_0

    const/16 v0, 0x59

    if-eq p0, v0, :cond_0

    const/16 v0, 0x56

    if-eq p0, v0, :cond_0

    const/16 v0, 0x7e

    if-eq p0, v0, :cond_0

    const/16 v0, 0x7f

    if-eq p0, v0, :cond_0

    const/16 v0, 0x82

    if-eq p0, v0, :cond_0

    const/16 v0, 0x4f

    if-eq p0, v0, :cond_0

    const/16 v0, 0xaf

    if-ne p0, v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private static c(I)Z
    .locals 1

    const/16 v0, 0x4f

    if-eq p0, v0, :cond_0

    const/16 v0, 0x55

    if-eq p0, v0, :cond_0

    const/16 v0, 0x56

    if-eq p0, v0, :cond_0

    const/16 v0, 0x7e

    if-eq p0, v0, :cond_0

    const/16 v0, 0x7f

    if-eq p0, v0, :cond_0

    const/16 v0, 0xaf

    if-ne p0, v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public final a(II)V
    .locals 0

    iput p1, p0, Lcom/google/android/youtube/core/player/overlay/p;->b:I

    iput p2, p0, Lcom/google/android/youtube/core/player/overlay/p;->c:I

    return-void
.end method

.method public final a(I)Z
    .locals 5

    const/4 v0, 0x1

    const/4 v1, 0x0

    const/high16 v4, -0x80000000

    const/16 v2, 0x59

    if-eq p1, v2, :cond_0

    const/16 v2, 0x5a

    if-ne p1, v2, :cond_2

    :cond_0
    iget v2, p0, Lcom/google/android/youtube/core/player/overlay/p;->d:I

    if-eq v2, v4, :cond_1

    iget v2, p0, Lcom/google/android/youtube/core/player/overlay/p;->e:I

    if-ne p1, v2, :cond_1

    iget-object v2, p0, Lcom/google/android/youtube/core/player/overlay/p;->a:Lcom/google/android/youtube/core/player/overlay/q;

    iget v3, p0, Lcom/google/android/youtube/core/player/overlay/p;->d:I

    invoke-interface {v2, v3}, Lcom/google/android/youtube/core/player/overlay/q;->b(I)V

    iput v4, p0, Lcom/google/android/youtube/core/player/overlay/p;->d:I

    iput v1, p0, Lcom/google/android/youtube/core/player/overlay/p;->e:I

    :cond_1
    :goto_0
    return v0

    :cond_2
    invoke-static {p1}, Lcom/google/android/youtube/core/player/overlay/p;->c(I)Z

    move-result v2

    if-nez v2, :cond_1

    move v0, v1

    goto :goto_0
.end method

.method public final a(ILandroid/view/KeyEvent;)Z
    .locals 5

    const/16 v4, 0x59

    const/4 v2, 0x0

    const/high16 v3, -0x80000000

    const/4 v1, 0x1

    iget v0, p0, Lcom/google/android/youtube/core/player/overlay/p;->d:I

    if-eq v0, v3, :cond_0

    iget v0, p0, Lcom/google/android/youtube/core/player/overlay/p;->e:I

    if-eq p1, v0, :cond_0

    move v0, v1

    :goto_0
    return v0

    :cond_0
    if-eq p1, v4, :cond_1

    const/16 v0, 0x5a

    if-ne p1, v0, :cond_5

    :cond_1
    iget v0, p0, Lcom/google/android/youtube/core/player/overlay/p;->b:I

    if-eq v0, v3, :cond_3

    iget v0, p0, Lcom/google/android/youtube/core/player/overlay/p;->c:I

    if-eq v0, v3, :cond_3

    invoke-virtual {p2}, Landroid/view/KeyEvent;->getRepeatCount()I

    move-result v0

    if-nez v0, :cond_2

    iget v0, p0, Lcom/google/android/youtube/core/player/overlay/p;->b:I

    iput v0, p0, Lcom/google/android/youtube/core/player/overlay/p;->d:I

    iput p1, p0, Lcom/google/android/youtube/core/player/overlay/p;->e:I

    iget-object v0, p0, Lcom/google/android/youtube/core/player/overlay/p;->a:Lcom/google/android/youtube/core/player/overlay/q;

    invoke-interface {v0}, Lcom/google/android/youtube/core/player/overlay/q;->a()V

    :cond_2
    iget v3, p0, Lcom/google/android/youtube/core/player/overlay/p;->d:I

    if-ne p1, v4, :cond_4

    const/16 v0, -0x4e20

    :goto_1
    add-int/2addr v0, v3

    iput v0, p0, Lcom/google/android/youtube/core/player/overlay/p;->d:I

    iget v0, p0, Lcom/google/android/youtube/core/player/overlay/p;->d:I

    iget v3, p0, Lcom/google/android/youtube/core/player/overlay/p;->c:I

    invoke-static {v0, v3}, Ljava/lang/Math;->min(II)I

    move-result v0

    invoke-static {v2, v0}, Ljava/lang/Math;->max(II)I

    move-result v0

    iput v0, p0, Lcom/google/android/youtube/core/player/overlay/p;->d:I

    iget-object v0, p0, Lcom/google/android/youtube/core/player/overlay/p;->a:Lcom/google/android/youtube/core/player/overlay/q;

    iget v2, p0, Lcom/google/android/youtube/core/player/overlay/p;->d:I

    invoke-interface {v0, v2}, Lcom/google/android/youtube/core/player/overlay/q;->a(I)V

    :cond_3
    move v0, v1

    goto :goto_0

    :cond_4
    const/16 v0, 0x4e20

    goto :goto_1

    :cond_5
    invoke-static {p1}, Lcom/google/android/youtube/core/player/overlay/p;->c(I)Z

    move-result v0

    if-eqz v0, :cond_7

    invoke-virtual {p2}, Landroid/view/KeyEvent;->getRepeatCount()I

    move-result v0

    if-lez v0, :cond_6

    move v0, v1

    goto :goto_0

    :cond_6
    sparse-switch p1, :sswitch_data_0

    :goto_2
    move v0, v1

    goto :goto_0

    :sswitch_0
    iget-object v0, p0, Lcom/google/android/youtube/core/player/overlay/p;->a:Lcom/google/android/youtube/core/player/overlay/q;

    invoke-interface {v0}, Lcom/google/android/youtube/core/player/overlay/q;->b()V

    goto :goto_2

    :sswitch_1
    iget-object v0, p0, Lcom/google/android/youtube/core/player/overlay/p;->a:Lcom/google/android/youtube/core/player/overlay/q;

    invoke-interface {v0}, Lcom/google/android/youtube/core/player/overlay/q;->c()V

    goto :goto_2

    :sswitch_2
    iget-object v0, p0, Lcom/google/android/youtube/core/player/overlay/p;->a:Lcom/google/android/youtube/core/player/overlay/q;

    invoke-interface {v0}, Lcom/google/android/youtube/core/player/overlay/q;->d()V

    goto :goto_2

    :sswitch_3
    iget-object v0, p0, Lcom/google/android/youtube/core/player/overlay/p;->a:Lcom/google/android/youtube/core/player/overlay/q;

    invoke-interface {v0}, Lcom/google/android/youtube/core/player/overlay/q;->e()V

    goto :goto_2

    :cond_7
    move v0, v2

    goto :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x4f -> :sswitch_0
        0x55 -> :sswitch_0
        0x56 -> :sswitch_2
        0x7e -> :sswitch_1
        0x7f -> :sswitch_2
        0xaf -> :sswitch_3
    .end sparse-switch
.end method
