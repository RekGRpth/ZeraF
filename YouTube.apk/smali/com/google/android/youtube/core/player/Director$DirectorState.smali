.class public final Lcom/google/android/youtube/core/player/Director$DirectorState;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;


# instance fields
.field public final ad:Lcom/google/android/youtube/core/model/VastAd;

.field public final adStartPositionMillis:I

.field public final adStatsClientState:Lcom/google/android/youtube/core/client/AdStatsClient$AdStatsClientState;

.field public final feature:Lcom/google/android/youtube/core/client/VideoStats2Client$Feature;

.field public final hq:Z

.field public final isPlaying:Z

.field public final qoeStatsClientState:Lcom/google/android/youtube/core/client/QoeStatsClient$QoeStatsClientState;

.field public final statsClientState:Lcom/google/android/youtube/core/client/VideoStats2Client$VideoStats2ClientState;

.field public final streamParams:Ljava/util/Map;

.field public final userInitiatedPlayback:Z

.field public final videoStartPositionMillis:I

.field public final wasEnded:Z


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Lcom/google/android/youtube/core/player/u;

    invoke-direct {v0}, Lcom/google/android/youtube/core/player/u;-><init>()V

    sput-object v0, Lcom/google/android/youtube/core/player/Director$DirectorState;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 4

    const/4 v2, 0x0

    const/4 v1, 0x1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    invoke-static {}, Lcom/google/android/youtube/core/client/VideoStats2Client$Feature;->values()[Lcom/google/android/youtube/core/client/VideoStats2Client$Feature;

    move-result-object v0

    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v3

    aget-object v0, v0, v3

    iput-object v0, p0, Lcom/google/android/youtube/core/player/Director$DirectorState;->feature:Lcom/google/android/youtube/core/client/VideoStats2Client$Feature;

    const-class v0, Lcom/google/android/youtube/core/model/VastAd;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/google/android/youtube/core/model/VastAd;

    iput-object v0, p0, Lcom/google/android/youtube/core/player/Director$DirectorState;->ad:Lcom/google/android/youtube/core/model/VastAd;

    const-class v0, Lcom/google/android/youtube/core/client/AdStatsClient$AdStatsClientState;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/google/android/youtube/core/client/AdStatsClient$AdStatsClientState;

    iput-object v0, p0, Lcom/google/android/youtube/core/player/Director$DirectorState;->adStatsClientState:Lcom/google/android/youtube/core/client/AdStatsClient$AdStatsClientState;

    const-class v0, Lcom/google/android/youtube/core/client/VideoStats2Client$VideoStats2ClientState;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/google/android/youtube/core/client/VideoStats2Client$VideoStats2ClientState;

    iput-object v0, p0, Lcom/google/android/youtube/core/player/Director$DirectorState;->statsClientState:Lcom/google/android/youtube/core/client/VideoStats2Client$VideoStats2ClientState;

    const-class v0, Lcom/google/android/youtube/core/client/QoeStatsClient$QoeStatsClientState;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/google/android/youtube/core/client/QoeStatsClient$QoeStatsClientState;

    iput-object v0, p0, Lcom/google/android/youtube/core/player/Director$DirectorState;->qoeStatsClientState:Lcom/google/android/youtube/core/client/QoeStatsClient$QoeStatsClientState;

    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-ne v0, v1, :cond_0

    move v0, v1

    :goto_0
    iput-boolean v0, p0, Lcom/google/android/youtube/core/player/Director$DirectorState;->isPlaying:Z

    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-ne v0, v1, :cond_1

    move v0, v1

    :goto_1
    iput-boolean v0, p0, Lcom/google/android/youtube/core/player/Director$DirectorState;->wasEnded:Z

    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/google/android/youtube/core/player/Director$DirectorState;->adStartPositionMillis:I

    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/google/android/youtube/core/player/Director$DirectorState;->videoStartPositionMillis:I

    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-ne v0, v1, :cond_2

    move v0, v1

    :goto_2
    iput-boolean v0, p0, Lcom/google/android/youtube/core/player/Director$DirectorState;->hq:Z

    invoke-virtual {p1}, Landroid/os/Parcel;->readBundle()Landroid/os/Bundle;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/youtube/core/utils/Util;->a(Landroid/os/Bundle;)Ljava/util/Map;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/youtube/core/player/Director$DirectorState;->streamParams:Ljava/util/Map;

    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-ne v0, v1, :cond_3

    :goto_3
    iput-boolean v1, p0, Lcom/google/android/youtube/core/player/Director$DirectorState;->userInitiatedPlayback:Z

    return-void

    :cond_0
    move v0, v2

    goto :goto_0

    :cond_1
    move v0, v2

    goto :goto_1

    :cond_2
    move v0, v2

    goto :goto_2

    :cond_3
    move v1, v2

    goto :goto_3
.end method

.method public constructor <init>(Lcom/google/android/youtube/core/client/VideoStats2Client$Feature;Lcom/google/android/youtube/core/model/VastAd;ZZZIILcom/google/android/youtube/core/client/VideoStats2Client$VideoStats2ClientState;Lcom/google/android/youtube/core/client/AdStatsClient$AdStatsClientState;Lcom/google/android/youtube/core/client/QoeStatsClient$QoeStatsClientState;Ljava/util/Map;Z)V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const-string v0, "feature cannot be null"

    invoke-static {p1, v0}, Lcom/google/android/youtube/core/utils/s;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/youtube/core/client/VideoStats2Client$Feature;

    iput-object v0, p0, Lcom/google/android/youtube/core/player/Director$DirectorState;->feature:Lcom/google/android/youtube/core/client/VideoStats2Client$Feature;

    iput-object p2, p0, Lcom/google/android/youtube/core/player/Director$DirectorState;->ad:Lcom/google/android/youtube/core/model/VastAd;

    iput-boolean p3, p0, Lcom/google/android/youtube/core/player/Director$DirectorState;->isPlaying:Z

    iput-boolean p4, p0, Lcom/google/android/youtube/core/player/Director$DirectorState;->wasEnded:Z

    iput-boolean p5, p0, Lcom/google/android/youtube/core/player/Director$DirectorState;->hq:Z

    iput p6, p0, Lcom/google/android/youtube/core/player/Director$DirectorState;->adStartPositionMillis:I

    iput p7, p0, Lcom/google/android/youtube/core/player/Director$DirectorState;->videoStartPositionMillis:I

    iput-object p8, p0, Lcom/google/android/youtube/core/player/Director$DirectorState;->statsClientState:Lcom/google/android/youtube/core/client/VideoStats2Client$VideoStats2ClientState;

    iput-object p9, p0, Lcom/google/android/youtube/core/player/Director$DirectorState;->adStatsClientState:Lcom/google/android/youtube/core/client/AdStatsClient$AdStatsClientState;

    iput-object p10, p0, Lcom/google/android/youtube/core/player/Director$DirectorState;->qoeStatsClientState:Lcom/google/android/youtube/core/client/QoeStatsClient$QoeStatsClientState;

    iput-object p11, p0, Lcom/google/android/youtube/core/player/Director$DirectorState;->streamParams:Ljava/util/Map;

    iput-boolean p12, p0, Lcom/google/android/youtube/core/player/Director$DirectorState;->userInitiatedPlayback:Z

    return-void
.end method


# virtual methods
.method public final describeContents()I
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public final toString()Ljava/lang/String;
    .locals 2

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "Director.DirectorState{"

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-static {p0}, Ljava/lang/System;->identityHashCode(Ljava/lang/Object;)I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " feature="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/youtube/core/player/Director$DirectorState;->feature:Lcom/google/android/youtube/core/client/VideoStats2Client$Feature;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " ad="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/youtube/core/player/Director$DirectorState;->ad:Lcom/google/android/youtube/core/model/VastAd;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " adStatsClientState="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/youtube/core/player/Director$DirectorState;->adStatsClientState:Lcom/google/android/youtube/core/client/AdStatsClient$AdStatsClientState;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " statsClientState="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/youtube/core/player/Director$DirectorState;->statsClientState:Lcom/google/android/youtube/core/client/VideoStats2Client$VideoStats2ClientState;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " qoeStatsClientState="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/youtube/core/player/Director$DirectorState;->qoeStatsClientState:Lcom/google/android/youtube/core/client/QoeStatsClient$QoeStatsClientState;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " isPlaying="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-boolean v1, p0, Lcom/google/android/youtube/core/player/Director$DirectorState;->isPlaying:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " wasEnded="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-boolean v1, p0, Lcom/google/android/youtube/core/player/Director$DirectorState;->wasEnded:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " hq="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-boolean v1, p0, Lcom/google/android/youtube/core/player/Director$DirectorState;->hq:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " adStartPositionMillis="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/google/android/youtube/core/player/Director$DirectorState;->adStartPositionMillis:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " videoStartPositionMillis="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/google/android/youtube/core/player/Director$DirectorState;->videoStartPositionMillis:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " streamParams="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/youtube/core/player/Director$DirectorState;->streamParams:Ljava/util/Map;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " userInitiatedPlayback="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-boolean v1, p0, Lcom/google/android/youtube/core/player/Director$DirectorState;->userInitiatedPlayback:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "}"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 3

    const/4 v1, 0x1

    const/4 v2, 0x0

    iget-object v0, p0, Lcom/google/android/youtube/core/player/Director$DirectorState;->feature:Lcom/google/android/youtube/core/client/VideoStats2Client$Feature;

    invoke-virtual {v0}, Lcom/google/android/youtube/core/client/VideoStats2Client$Feature;->ordinal()I

    move-result v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    iget-object v0, p0, Lcom/google/android/youtube/core/player/Director$DirectorState;->ad:Lcom/google/android/youtube/core/model/VastAd;

    invoke-virtual {p1, v0, v2}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    iget-object v0, p0, Lcom/google/android/youtube/core/player/Director$DirectorState;->adStatsClientState:Lcom/google/android/youtube/core/client/AdStatsClient$AdStatsClientState;

    invoke-virtual {p1, v0, v2}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    iget-object v0, p0, Lcom/google/android/youtube/core/player/Director$DirectorState;->statsClientState:Lcom/google/android/youtube/core/client/VideoStats2Client$VideoStats2ClientState;

    invoke-virtual {p1, v0, v2}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    iget-object v0, p0, Lcom/google/android/youtube/core/player/Director$DirectorState;->qoeStatsClientState:Lcom/google/android/youtube/core/client/QoeStatsClient$QoeStatsClientState;

    invoke-virtual {p1, v0, v2}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    iget-boolean v0, p0, Lcom/google/android/youtube/core/player/Director$DirectorState;->isPlaying:Z

    if-eqz v0, :cond_0

    move v0, v1

    :goto_0
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    iget-boolean v0, p0, Lcom/google/android/youtube/core/player/Director$DirectorState;->wasEnded:Z

    if-eqz v0, :cond_1

    move v0, v1

    :goto_1
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    iget v0, p0, Lcom/google/android/youtube/core/player/Director$DirectorState;->adStartPositionMillis:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    iget v0, p0, Lcom/google/android/youtube/core/player/Director$DirectorState;->videoStartPositionMillis:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    iget-boolean v0, p0, Lcom/google/android/youtube/core/player/Director$DirectorState;->hq:Z

    if-eqz v0, :cond_2

    move v0, v1

    :goto_2
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    iget-object v0, p0, Lcom/google/android/youtube/core/player/Director$DirectorState;->streamParams:Ljava/util/Map;

    invoke-static {v0}, Lcom/google/android/youtube/core/utils/Util;->a(Ljava/util/Map;)Landroid/os/Bundle;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeBundle(Landroid/os/Bundle;)V

    iget-boolean v0, p0, Lcom/google/android/youtube/core/player/Director$DirectorState;->userInitiatedPlayback:Z

    if-eqz v0, :cond_3

    :goto_3
    invoke-virtual {p1, v1}, Landroid/os/Parcel;->writeInt(I)V

    return-void

    :cond_0
    move v0, v2

    goto :goto_0

    :cond_1
    move v0, v2

    goto :goto_1

    :cond_2
    move v0, v2

    goto :goto_2

    :cond_3
    move v1, v2

    goto :goto_3
.end method
