.class public final Lcom/google/android/youtube/core/model/ab;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/youtube/core/model/i;


# instance fields
.field private final a:Landroid/util/SparseArray;


# direct methods
.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Landroid/util/SparseArray;

    invoke-direct {v0}, Landroid/util/SparseArray;-><init>()V

    iput-object v0, p0, Lcom/google/android/youtube/core/model/ab;->a:Landroid/util/SparseArray;

    return-void
.end method

.method private a(I)Lcom/google/android/youtube/core/model/r;
    .locals 2

    iget-object v0, p0, Lcom/google/android/youtube/core/model/ab;->a:Landroid/util/SparseArray;

    invoke-virtual {v0, p1}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/youtube/core/model/r;

    if-nez v0, :cond_0

    new-instance v0, Lcom/google/android/youtube/core/model/r;

    invoke-direct {v0, p1}, Lcom/google/android/youtube/core/model/r;-><init>(I)V

    iget-object v1, p0, Lcom/google/android/youtube/core/model/ab;->a:Landroid/util/SparseArray;

    invoke-virtual {v1, p1, v0}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    :cond_0
    return-object v0
.end method


# virtual methods
.method public final a(IILcom/google/android/youtube/core/model/SubtitleWindowSettings;)Lcom/google/android/youtube/core/model/ab;
    .locals 1

    invoke-direct {p0, p1}, Lcom/google/android/youtube/core/model/ab;->a(I)Lcom/google/android/youtube/core/model/r;

    move-result-object v0

    invoke-virtual {v0, p2, p3}, Lcom/google/android/youtube/core/model/r;->a(ILcom/google/android/youtube/core/model/SubtitleWindowSettings;)Lcom/google/android/youtube/core/model/r;

    return-object p0
.end method

.method public final a(ILjava/lang/String;II)Lcom/google/android/youtube/core/model/ab;
    .locals 1

    invoke-direct {p0, p1}, Lcom/google/android/youtube/core/model/ab;->a(I)Lcom/google/android/youtube/core/model/r;

    move-result-object v0

    invoke-virtual {v0, p2, p3, p4}, Lcom/google/android/youtube/core/model/r;->a(Ljava/lang/String;II)Lcom/google/android/youtube/core/model/r;

    return-object p0
.end method

.method public final b(ILjava/lang/String;II)Lcom/google/android/youtube/core/model/ab;
    .locals 1

    invoke-direct {p0, p1}, Lcom/google/android/youtube/core/model/ab;->a(I)Lcom/google/android/youtube/core/model/r;

    move-result-object v0

    invoke-virtual {v0, p2, p3, p4}, Lcom/google/android/youtube/core/model/r;->b(Ljava/lang/String;II)Lcom/google/android/youtube/core/model/r;

    return-object p0
.end method

.method public final synthetic build()Ljava/lang/Object;
    .locals 3

    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    const/4 v0, 0x0

    move v1, v0

    :goto_0
    iget-object v0, p0, Lcom/google/android/youtube/core/model/ab;->a:Landroid/util/SparseArray;

    invoke-virtual {v0}, Landroid/util/SparseArray;->size()I

    move-result v0

    if-ge v1, v0, :cond_0

    iget-object v0, p0, Lcom/google/android/youtube/core/model/ab;->a:Landroid/util/SparseArray;

    invoke-virtual {v0, v1}, Landroid/util/SparseArray;->valueAt(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/youtube/core/model/r;

    invoke-virtual {v0}, Lcom/google/android/youtube/core/model/r;->a()Lcom/google/android/youtube/core/model/SubtitleWindow;

    move-result-object v0

    invoke-interface {v2, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    :cond_0
    new-instance v0, Lcom/google/android/youtube/core/model/Subtitles;

    const/4 v1, 0x0

    invoke-direct {v0, v2, v1}, Lcom/google/android/youtube/core/model/Subtitles;-><init>(Ljava/util/List;Lcom/google/android/youtube/core/model/aa;)V

    return-object v0
.end method
