.class public Lcom/google/android/youtube/core/model/VastAd;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;

.field public static final EMPTY_AD:Lcom/google/android/youtube/core/model/VastAd;


# instance fields
.field public final adFormat:Ljava/lang/String;

.field public final adSystem:Ljava/lang/String;

.field public final adVideoId:Ljava/lang/String;

.field public final adWrapperUri:Landroid/net/Uri;

.field public final clickthroughPingUris:Ljava/util/List;

.field public final clickthroughUri:Landroid/net/Uri;

.field public final closePingUris:Ljava/util/List;

.field public final completePingUris:Ljava/util/List;

.field public final duration:I

.field public final engagedViewPingUris:Ljava/util/List;

.field public final errorPingUris:Ljava/util/List;

.field public final expiryTimeMillis:J

.field public final fallbackHint:Z

.field public final firstQuartilePingUris:Ljava/util/List;

.field public final fullscreenPingUris:Ljava/util/List;

.field public final impressionUris:Ljava/util/List;

.field public final isPublicVideo:Z

.field public final isVastWrapper:Z

.field public final midpointPingUris:Ljava/util/List;

.field public final mutePingUris:Ljava/util/List;

.field public final originalVideoId:Ljava/lang/String;

.field public final pausePingUris:Ljava/util/List;

.field public final resumePingUris:Ljava/util/List;

.field public final shouldPingVssOnEngaged:Z

.field public final skipPingUris:Ljava/util/List;

.field public final skipShownPingUris:Ljava/util/List;

.field public final startPingUris:Ljava/util/List;

.field public final streams:Ljava/util/List;

.field public final thirdQuartilePingUris:Ljava/util/List;

.field public final title:Ljava/lang/String;

.field public final videoTitleClickedPingUris:Ljava/util/List;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Lcom/google/android/youtube/core/model/VastAd;

    invoke-direct {v0}, Lcom/google/android/youtube/core/model/VastAd;-><init>()V

    sput-object v0, Lcom/google/android/youtube/core/model/VastAd;->EMPTY_AD:Lcom/google/android/youtube/core/model/VastAd;

    new-instance v0, Lcom/google/android/youtube/core/model/ad;

    invoke-direct {v0}, Lcom/google/android/youtube/core/model/ad;-><init>()V

    sput-object v0, Lcom/google/android/youtube/core/model/VastAd;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method private constructor <init>()V
    .locals 4

    const/4 v3, 0x0

    const/4 v2, 0x0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/youtube/core/model/VastAd;->impressionUris:Ljava/util/List;

    iput-object v2, p0, Lcom/google/android/youtube/core/model/VastAd;->adVideoId:Ljava/lang/String;

    iput-object v2, p0, Lcom/google/android/youtube/core/model/VastAd;->originalVideoId:Ljava/lang/String;

    iput-object v2, p0, Lcom/google/android/youtube/core/model/VastAd;->title:Ljava/lang/String;

    iput-object v2, p0, Lcom/google/android/youtube/core/model/VastAd;->adSystem:Ljava/lang/String;

    iput-object v2, p0, Lcom/google/android/youtube/core/model/VastAd;->adFormat:Ljava/lang/String;

    iput v3, p0, Lcom/google/android/youtube/core/model/VastAd;->duration:I

    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/youtube/core/model/VastAd;->streams:Ljava/util/List;

    iput-object v2, p0, Lcom/google/android/youtube/core/model/VastAd;->clickthroughUri:Landroid/net/Uri;

    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/youtube/core/model/VastAd;->startPingUris:Ljava/util/List;

    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/youtube/core/model/VastAd;->firstQuartilePingUris:Ljava/util/List;

    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/youtube/core/model/VastAd;->midpointPingUris:Ljava/util/List;

    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/youtube/core/model/VastAd;->thirdQuartilePingUris:Ljava/util/List;

    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/youtube/core/model/VastAd;->skipPingUris:Ljava/util/List;

    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/youtube/core/model/VastAd;->skipShownPingUris:Ljava/util/List;

    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/youtube/core/model/VastAd;->engagedViewPingUris:Ljava/util/List;

    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/youtube/core/model/VastAd;->completePingUris:Ljava/util/List;

    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/youtube/core/model/VastAd;->closePingUris:Ljava/util/List;

    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/youtube/core/model/VastAd;->pausePingUris:Ljava/util/List;

    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/youtube/core/model/VastAd;->resumePingUris:Ljava/util/List;

    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/youtube/core/model/VastAd;->mutePingUris:Ljava/util/List;

    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/youtube/core/model/VastAd;->fullscreenPingUris:Ljava/util/List;

    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/youtube/core/model/VastAd;->clickthroughPingUris:Ljava/util/List;

    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/youtube/core/model/VastAd;->videoTitleClickedPingUris:Ljava/util/List;

    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/youtube/core/model/VastAd;->errorPingUris:Ljava/util/List;

    iput-boolean v3, p0, Lcom/google/android/youtube/core/model/VastAd;->shouldPingVssOnEngaged:Z

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/youtube/core/model/VastAd;->fallbackHint:Z

    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/google/android/youtube/core/model/VastAd;->expiryTimeMillis:J

    iput-boolean v3, p0, Lcom/google/android/youtube/core/model/VastAd;->isPublicVideo:Z

    iput-object v2, p0, Lcom/google/android/youtube/core/model/VastAd;->adWrapperUri:Landroid/net/Uri;

    iput-boolean v3, p0, Lcom/google/android/youtube/core/model/VastAd;->isVastWrapper:Z

    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 33

    invoke-static/range {p1 .. p1}, Lcom/google/android/youtube/core/model/VastAd;->readUriList(Landroid/os/Parcel;)Ljava/util/List;

    move-result-object v2

    invoke-virtual/range {p1 .. p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual/range {p1 .. p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual/range {p1 .. p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual/range {p1 .. p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual/range {p1 .. p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v7

    invoke-virtual/range {p1 .. p1}, Landroid/os/Parcel;->readInt()I

    move-result v8

    invoke-static/range {p1 .. p1}, Lcom/google/android/youtube/core/model/VastAd;->readStreamList(Landroid/os/Parcel;)Ljava/util/List;

    move-result-object v9

    const-class v1, Landroid/net/Uri;

    invoke-virtual {v1}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v1

    move-object/from16 v0, p1

    invoke-virtual {v0, v1}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object v10

    check-cast v10, Landroid/net/Uri;

    invoke-static/range {p1 .. p1}, Lcom/google/android/youtube/core/model/VastAd;->readUriList(Landroid/os/Parcel;)Ljava/util/List;

    move-result-object v11

    invoke-static/range {p1 .. p1}, Lcom/google/android/youtube/core/model/VastAd;->readUriList(Landroid/os/Parcel;)Ljava/util/List;

    move-result-object v12

    invoke-static/range {p1 .. p1}, Lcom/google/android/youtube/core/model/VastAd;->readUriList(Landroid/os/Parcel;)Ljava/util/List;

    move-result-object v13

    invoke-static/range {p1 .. p1}, Lcom/google/android/youtube/core/model/VastAd;->readUriList(Landroid/os/Parcel;)Ljava/util/List;

    move-result-object v14

    invoke-static/range {p1 .. p1}, Lcom/google/android/youtube/core/model/VastAd;->readUriList(Landroid/os/Parcel;)Ljava/util/List;

    move-result-object v15

    invoke-static/range {p1 .. p1}, Lcom/google/android/youtube/core/model/VastAd;->readUriList(Landroid/os/Parcel;)Ljava/util/List;

    move-result-object v16

    invoke-static/range {p1 .. p1}, Lcom/google/android/youtube/core/model/VastAd;->readUriList(Landroid/os/Parcel;)Ljava/util/List;

    move-result-object v17

    invoke-static/range {p1 .. p1}, Lcom/google/android/youtube/core/model/VastAd;->readUriList(Landroid/os/Parcel;)Ljava/util/List;

    move-result-object v18

    invoke-static/range {p1 .. p1}, Lcom/google/android/youtube/core/model/VastAd;->readUriList(Landroid/os/Parcel;)Ljava/util/List;

    move-result-object v19

    invoke-static/range {p1 .. p1}, Lcom/google/android/youtube/core/model/VastAd;->readUriList(Landroid/os/Parcel;)Ljava/util/List;

    move-result-object v20

    invoke-static/range {p1 .. p1}, Lcom/google/android/youtube/core/model/VastAd;->readUriList(Landroid/os/Parcel;)Ljava/util/List;

    move-result-object v21

    invoke-static/range {p1 .. p1}, Lcom/google/android/youtube/core/model/VastAd;->readUriList(Landroid/os/Parcel;)Ljava/util/List;

    move-result-object v22

    invoke-static/range {p1 .. p1}, Lcom/google/android/youtube/core/model/VastAd;->readUriList(Landroid/os/Parcel;)Ljava/util/List;

    move-result-object v23

    invoke-static/range {p1 .. p1}, Lcom/google/android/youtube/core/model/VastAd;->readUriList(Landroid/os/Parcel;)Ljava/util/List;

    move-result-object v24

    invoke-static/range {p1 .. p1}, Lcom/google/android/youtube/core/model/VastAd;->readUriList(Landroid/os/Parcel;)Ljava/util/List;

    move-result-object v25

    invoke-static/range {p1 .. p1}, Lcom/google/android/youtube/core/model/VastAd;->readUriList(Landroid/os/Parcel;)Ljava/util/List;

    move-result-object v26

    invoke-virtual/range {p1 .. p1}, Landroid/os/Parcel;->readInt()I

    move-result v1

    const/16 v27, 0x1

    move/from16 v0, v27

    if-ne v1, v0, :cond_0

    const/16 v27, 0x1

    :goto_0
    invoke-virtual/range {p1 .. p1}, Landroid/os/Parcel;->readInt()I

    move-result v1

    const/16 v28, 0x1

    move/from16 v0, v28

    if-ne v1, v0, :cond_1

    const/16 v28, 0x1

    :goto_1
    invoke-virtual/range {p1 .. p1}, Landroid/os/Parcel;->readLong()J

    move-result-wide v29

    invoke-virtual/range {p1 .. p1}, Landroid/os/Parcel;->readInt()I

    move-result v1

    const/16 v31, 0x1

    move/from16 v0, v31

    if-ne v1, v0, :cond_2

    const/16 v31, 0x1

    :goto_2
    const-class v1, Landroid/net/Uri;

    invoke-virtual {v1}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v1

    move-object/from16 v0, p1

    invoke-virtual {v0, v1}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object v32

    check-cast v32, Landroid/net/Uri;

    move-object/from16 v1, p0

    invoke-direct/range {v1 .. v32}, Lcom/google/android/youtube/core/model/VastAd;-><init>(Ljava/util/List;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ILjava/util/List;Landroid/net/Uri;Ljava/util/List;Ljava/util/List;Ljava/util/List;Ljava/util/List;Ljava/util/List;Ljava/util/List;Ljava/util/List;Ljava/util/List;Ljava/util/List;Ljava/util/List;Ljava/util/List;Ljava/util/List;Ljava/util/List;Ljava/util/List;Ljava/util/List;Ljava/util/List;ZZJZLandroid/net/Uri;)V

    return-void

    :cond_0
    const/16 v27, 0x0

    goto :goto_0

    :cond_1
    const/16 v28, 0x0

    goto :goto_1

    :cond_2
    const/16 v31, 0x0

    goto :goto_2
.end method

.method public constructor <init>(Ljava/util/List;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ILjava/util/List;Landroid/net/Uri;Ljava/util/List;Ljava/util/List;Ljava/util/List;Ljava/util/List;Ljava/util/List;Ljava/util/List;Ljava/util/List;Ljava/util/List;Ljava/util/List;Ljava/util/List;Ljava/util/List;Ljava/util/List;Ljava/util/List;Ljava/util/List;Ljava/util/List;Ljava/util/List;ZZJZLandroid/net/Uri;)V
    .locals 4

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const-string v2, "Impression uris cannot be null"

    invoke-static {p1, v2}, Lcom/google/android/youtube/core/utils/s;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v2

    if-lez v2, :cond_0

    const/4 v2, 0x1

    :goto_0
    const-string v3, "Impression uris cannot be empty"

    invoke-static {v2, v3}, Lcom/google/android/youtube/core/utils/s;->a(ZLjava/lang/Object;)V

    invoke-static {p1}, Lcom/google/android/youtube/core/utils/Util;->a(Ljava/util/List;)Ljava/util/List;

    move-result-object v2

    iput-object v2, p0, Lcom/google/android/youtube/core/model/VastAd;->impressionUris:Ljava/util/List;

    iput-object p2, p0, Lcom/google/android/youtube/core/model/VastAd;->adVideoId:Ljava/lang/String;

    iput-object p3, p0, Lcom/google/android/youtube/core/model/VastAd;->originalVideoId:Ljava/lang/String;

    iput-object p4, p0, Lcom/google/android/youtube/core/model/VastAd;->title:Ljava/lang/String;

    iput-object p5, p0, Lcom/google/android/youtube/core/model/VastAd;->adSystem:Ljava/lang/String;

    iput-object p6, p0, Lcom/google/android/youtube/core/model/VastAd;->adFormat:Ljava/lang/String;

    iput p7, p0, Lcom/google/android/youtube/core/model/VastAd;->duration:I

    invoke-static {p8}, Lcom/google/android/youtube/core/utils/Util;->a(Ljava/util/List;)Ljava/util/List;

    move-result-object v2

    iput-object v2, p0, Lcom/google/android/youtube/core/model/VastAd;->streams:Ljava/util/List;

    iput-object p9, p0, Lcom/google/android/youtube/core/model/VastAd;->clickthroughUri:Landroid/net/Uri;

    invoke-static {p10}, Lcom/google/android/youtube/core/utils/Util;->a(Ljava/util/List;)Ljava/util/List;

    move-result-object v2

    iput-object v2, p0, Lcom/google/android/youtube/core/model/VastAd;->startPingUris:Ljava/util/List;

    invoke-static {p11}, Lcom/google/android/youtube/core/utils/Util;->a(Ljava/util/List;)Ljava/util/List;

    move-result-object v2

    iput-object v2, p0, Lcom/google/android/youtube/core/model/VastAd;->firstQuartilePingUris:Ljava/util/List;

    invoke-static/range {p12 .. p12}, Lcom/google/android/youtube/core/utils/Util;->a(Ljava/util/List;)Ljava/util/List;

    move-result-object v2

    iput-object v2, p0, Lcom/google/android/youtube/core/model/VastAd;->midpointPingUris:Ljava/util/List;

    invoke-static/range {p13 .. p13}, Lcom/google/android/youtube/core/utils/Util;->a(Ljava/util/List;)Ljava/util/List;

    move-result-object v2

    iput-object v2, p0, Lcom/google/android/youtube/core/model/VastAd;->thirdQuartilePingUris:Ljava/util/List;

    invoke-static/range {p14 .. p14}, Lcom/google/android/youtube/core/utils/Util;->a(Ljava/util/List;)Ljava/util/List;

    move-result-object v2

    iput-object v2, p0, Lcom/google/android/youtube/core/model/VastAd;->skipPingUris:Ljava/util/List;

    invoke-static/range {p15 .. p15}, Lcom/google/android/youtube/core/utils/Util;->a(Ljava/util/List;)Ljava/util/List;

    move-result-object v2

    iput-object v2, p0, Lcom/google/android/youtube/core/model/VastAd;->skipShownPingUris:Ljava/util/List;

    invoke-static/range {p16 .. p16}, Lcom/google/android/youtube/core/utils/Util;->a(Ljava/util/List;)Ljava/util/List;

    move-result-object v2

    iput-object v2, p0, Lcom/google/android/youtube/core/model/VastAd;->engagedViewPingUris:Ljava/util/List;

    invoke-static/range {p17 .. p17}, Lcom/google/android/youtube/core/utils/Util;->a(Ljava/util/List;)Ljava/util/List;

    move-result-object v2

    iput-object v2, p0, Lcom/google/android/youtube/core/model/VastAd;->completePingUris:Ljava/util/List;

    invoke-static/range {p18 .. p18}, Lcom/google/android/youtube/core/utils/Util;->a(Ljava/util/List;)Ljava/util/List;

    move-result-object v2

    iput-object v2, p0, Lcom/google/android/youtube/core/model/VastAd;->closePingUris:Ljava/util/List;

    invoke-static/range {p19 .. p19}, Lcom/google/android/youtube/core/utils/Util;->a(Ljava/util/List;)Ljava/util/List;

    move-result-object v2

    iput-object v2, p0, Lcom/google/android/youtube/core/model/VastAd;->pausePingUris:Ljava/util/List;

    invoke-static/range {p20 .. p20}, Lcom/google/android/youtube/core/utils/Util;->a(Ljava/util/List;)Ljava/util/List;

    move-result-object v2

    iput-object v2, p0, Lcom/google/android/youtube/core/model/VastAd;->resumePingUris:Ljava/util/List;

    invoke-static/range {p21 .. p21}, Lcom/google/android/youtube/core/utils/Util;->a(Ljava/util/List;)Ljava/util/List;

    move-result-object v2

    iput-object v2, p0, Lcom/google/android/youtube/core/model/VastAd;->mutePingUris:Ljava/util/List;

    invoke-static/range {p22 .. p22}, Lcom/google/android/youtube/core/utils/Util;->a(Ljava/util/List;)Ljava/util/List;

    move-result-object v2

    iput-object v2, p0, Lcom/google/android/youtube/core/model/VastAd;->fullscreenPingUris:Ljava/util/List;

    invoke-static/range {p23 .. p23}, Lcom/google/android/youtube/core/utils/Util;->a(Ljava/util/List;)Ljava/util/List;

    move-result-object v2

    iput-object v2, p0, Lcom/google/android/youtube/core/model/VastAd;->clickthroughPingUris:Ljava/util/List;

    invoke-static/range {p24 .. p24}, Lcom/google/android/youtube/core/utils/Util;->a(Ljava/util/List;)Ljava/util/List;

    move-result-object v2

    iput-object v2, p0, Lcom/google/android/youtube/core/model/VastAd;->videoTitleClickedPingUris:Ljava/util/List;

    invoke-static/range {p25 .. p25}, Lcom/google/android/youtube/core/utils/Util;->a(Ljava/util/List;)Ljava/util/List;

    move-result-object v2

    iput-object v2, p0, Lcom/google/android/youtube/core/model/VastAd;->errorPingUris:Ljava/util/List;

    move/from16 v0, p26

    iput-boolean v0, p0, Lcom/google/android/youtube/core/model/VastAd;->shouldPingVssOnEngaged:Z

    move/from16 v0, p27

    iput-boolean v0, p0, Lcom/google/android/youtube/core/model/VastAd;->fallbackHint:Z

    move-wide/from16 v0, p28

    iput-wide v0, p0, Lcom/google/android/youtube/core/model/VastAd;->expiryTimeMillis:J

    move/from16 v0, p30

    iput-boolean v0, p0, Lcom/google/android/youtube/core/model/VastAd;->isPublicVideo:Z

    move-object/from16 v0, p31

    iput-object v0, p0, Lcom/google/android/youtube/core/model/VastAd;->adWrapperUri:Landroid/net/Uri;

    if-eqz p31, :cond_1

    const/4 v2, 0x1

    :goto_1
    iput-boolean v2, p0, Lcom/google/android/youtube/core/model/VastAd;->isVastWrapper:Z

    return-void

    :cond_0
    const/4 v2, 0x0

    goto/16 :goto_0

    :cond_1
    const/4 v2, 0x0

    goto :goto_1
.end method

.method private static readStreamList(Landroid/os/Parcel;)Ljava/util/List;
    .locals 2

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    sget-object v1, Lcom/google/android/youtube/core/model/Stream;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-virtual {p0, v0, v1}, Landroid/os/Parcel;->readTypedList(Ljava/util/List;Landroid/os/Parcelable$Creator;)V

    invoke-static {v0}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method private static readUriList(Landroid/os/Parcel;)Ljava/util/List;
    .locals 2

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    sget-object v1, Landroid/net/Uri;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-virtual {p0, v0, v1}, Landroid/os/Parcel;->readTypedList(Ljava/util/List;Landroid/os/Parcelable$Creator;)V

    invoke-static {v0}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public buildUpon()Lcom/google/android/youtube/core/model/ae;
    .locals 3

    new-instance v0, Lcom/google/android/youtube/core/model/ae;

    invoke-direct {v0}, Lcom/google/android/youtube/core/model/ae;-><init>()V

    new-instance v1, Ljava/util/ArrayList;

    iget-object v2, p0, Lcom/google/android/youtube/core/model/VastAd;->impressionUris:Ljava/util/List;

    invoke-direct {v1, v2}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    invoke-virtual {v0, v1}, Lcom/google/android/youtube/core/model/ae;->b(Ljava/util/List;)Lcom/google/android/youtube/core/model/ae;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/youtube/core/model/VastAd;->adVideoId:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/google/android/youtube/core/model/ae;->a(Ljava/lang/String;)Lcom/google/android/youtube/core/model/ae;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/youtube/core/model/VastAd;->originalVideoId:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/google/android/youtube/core/model/ae;->b(Ljava/lang/String;)Lcom/google/android/youtube/core/model/ae;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/youtube/core/model/VastAd;->title:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/google/android/youtube/core/model/ae;->c(Ljava/lang/String;)Lcom/google/android/youtube/core/model/ae;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/youtube/core/model/VastAd;->adSystem:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/google/android/youtube/core/model/ae;->d(Ljava/lang/String;)Lcom/google/android/youtube/core/model/ae;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/youtube/core/model/VastAd;->adFormat:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/google/android/youtube/core/model/ae;->e(Ljava/lang/String;)Lcom/google/android/youtube/core/model/ae;

    move-result-object v0

    iget v1, p0, Lcom/google/android/youtube/core/model/VastAd;->duration:I

    invoke-virtual {v0, v1}, Lcom/google/android/youtube/core/model/ae;->a(I)Lcom/google/android/youtube/core/model/ae;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/youtube/core/model/VastAd;->streams:Ljava/util/List;

    invoke-virtual {v0, v1}, Lcom/google/android/youtube/core/model/ae;->a(Ljava/util/Collection;)Lcom/google/android/youtube/core/model/ae;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/youtube/core/model/VastAd;->clickthroughUri:Landroid/net/Uri;

    invoke-virtual {v0, v1}, Lcom/google/android/youtube/core/model/ae;->r(Landroid/net/Uri;)Lcom/google/android/youtube/core/model/ae;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/youtube/core/model/VastAd;->startPingUris:Ljava/util/List;

    invoke-virtual {v0, v1}, Lcom/google/android/youtube/core/model/ae;->c(Ljava/util/List;)Lcom/google/android/youtube/core/model/ae;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/youtube/core/model/VastAd;->firstQuartilePingUris:Ljava/util/List;

    invoke-virtual {v0, v1}, Lcom/google/android/youtube/core/model/ae;->d(Ljava/util/List;)Lcom/google/android/youtube/core/model/ae;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/youtube/core/model/VastAd;->midpointPingUris:Ljava/util/List;

    invoke-virtual {v0, v1}, Lcom/google/android/youtube/core/model/ae;->e(Ljava/util/List;)Lcom/google/android/youtube/core/model/ae;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/youtube/core/model/VastAd;->thirdQuartilePingUris:Ljava/util/List;

    invoke-virtual {v0, v1}, Lcom/google/android/youtube/core/model/ae;->f(Ljava/util/List;)Lcom/google/android/youtube/core/model/ae;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/youtube/core/model/VastAd;->skipPingUris:Ljava/util/List;

    invoke-virtual {v0, v1}, Lcom/google/android/youtube/core/model/ae;->g(Ljava/util/List;)Lcom/google/android/youtube/core/model/ae;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/youtube/core/model/VastAd;->skipShownPingUris:Ljava/util/List;

    invoke-virtual {v0, v1}, Lcom/google/android/youtube/core/model/ae;->h(Ljava/util/List;)Lcom/google/android/youtube/core/model/ae;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/youtube/core/model/VastAd;->engagedViewPingUris:Ljava/util/List;

    invoke-virtual {v0, v1}, Lcom/google/android/youtube/core/model/ae;->i(Ljava/util/List;)Lcom/google/android/youtube/core/model/ae;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/youtube/core/model/VastAd;->completePingUris:Ljava/util/List;

    invoke-virtual {v0, v1}, Lcom/google/android/youtube/core/model/ae;->j(Ljava/util/List;)Lcom/google/android/youtube/core/model/ae;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/youtube/core/model/VastAd;->closePingUris:Ljava/util/List;

    invoke-virtual {v0, v1}, Lcom/google/android/youtube/core/model/ae;->k(Ljava/util/List;)Lcom/google/android/youtube/core/model/ae;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/youtube/core/model/VastAd;->pausePingUris:Ljava/util/List;

    invoke-virtual {v0, v1}, Lcom/google/android/youtube/core/model/ae;->l(Ljava/util/List;)Lcom/google/android/youtube/core/model/ae;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/youtube/core/model/VastAd;->resumePingUris:Ljava/util/List;

    invoke-virtual {v0, v1}, Lcom/google/android/youtube/core/model/ae;->m(Ljava/util/List;)Lcom/google/android/youtube/core/model/ae;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/youtube/core/model/VastAd;->mutePingUris:Ljava/util/List;

    invoke-virtual {v0, v1}, Lcom/google/android/youtube/core/model/ae;->n(Ljava/util/List;)Lcom/google/android/youtube/core/model/ae;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/youtube/core/model/VastAd;->fullscreenPingUris:Ljava/util/List;

    invoke-virtual {v0, v1}, Lcom/google/android/youtube/core/model/ae;->o(Ljava/util/List;)Lcom/google/android/youtube/core/model/ae;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/youtube/core/model/VastAd;->clickthroughPingUris:Ljava/util/List;

    invoke-virtual {v0, v1}, Lcom/google/android/youtube/core/model/ae;->p(Ljava/util/List;)Lcom/google/android/youtube/core/model/ae;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/youtube/core/model/VastAd;->videoTitleClickedPingUris:Ljava/util/List;

    invoke-virtual {v0, v1}, Lcom/google/android/youtube/core/model/ae;->q(Ljava/util/List;)Lcom/google/android/youtube/core/model/ae;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/youtube/core/model/VastAd;->errorPingUris:Ljava/util/List;

    invoke-virtual {v0, v1}, Lcom/google/android/youtube/core/model/ae;->r(Ljava/util/List;)Lcom/google/android/youtube/core/model/ae;

    move-result-object v0

    iget-boolean v1, p0, Lcom/google/android/youtube/core/model/VastAd;->shouldPingVssOnEngaged:Z

    invoke-virtual {v0, v1}, Lcom/google/android/youtube/core/model/ae;->a(Z)Lcom/google/android/youtube/core/model/ae;

    move-result-object v0

    iget-boolean v1, p0, Lcom/google/android/youtube/core/model/VastAd;->fallbackHint:Z

    invoke-virtual {v0, v1}, Lcom/google/android/youtube/core/model/ae;->b(Z)Lcom/google/android/youtube/core/model/ae;

    move-result-object v0

    iget-wide v1, p0, Lcom/google/android/youtube/core/model/VastAd;->expiryTimeMillis:J

    invoke-virtual {v0, v1, v2}, Lcom/google/android/youtube/core/model/ae;->a(J)Lcom/google/android/youtube/core/model/ae;

    move-result-object v0

    iget-boolean v1, p0, Lcom/google/android/youtube/core/model/VastAd;->isPublicVideo:Z

    invoke-virtual {v0, v1}, Lcom/google/android/youtube/core/model/ae;->c(Z)Lcom/google/android/youtube/core/model/ae;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/youtube/core/model/VastAd;->adWrapperUri:Landroid/net/Uri;

    invoke-virtual {v0, v1}, Lcom/google/android/youtube/core/model/ae;->s(Landroid/net/Uri;)Lcom/google/android/youtube/core/model/ae;

    move-result-object v0

    return-object v0
.end method

.method public describeContents()I
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 5

    const/4 v0, 0x0

    if-nez p1, :cond_1

    :cond_0
    :goto_0
    return v0

    :cond_1
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    check-cast p1, Lcom/google/android/youtube/core/model/VastAd;

    iget-object v1, p0, Lcom/google/android/youtube/core/model/VastAd;->adVideoId:Ljava/lang/String;

    iget-object v2, p1, Lcom/google/android/youtube/core/model/VastAd;->adVideoId:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/google/android/youtube/core/utils/Util;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/google/android/youtube/core/model/VastAd;->originalVideoId:Ljava/lang/String;

    iget-object v2, p1, Lcom/google/android/youtube/core/model/VastAd;->originalVideoId:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/google/android/youtube/core/utils/Util;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/google/android/youtube/core/model/VastAd;->title:Ljava/lang/String;

    iget-object v2, p1, Lcom/google/android/youtube/core/model/VastAd;->title:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/google/android/youtube/core/utils/Util;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/google/android/youtube/core/model/VastAd;->adSystem:Ljava/lang/String;

    iget-object v2, p1, Lcom/google/android/youtube/core/model/VastAd;->adSystem:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/google/android/youtube/core/utils/Util;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/google/android/youtube/core/model/VastAd;->adFormat:Ljava/lang/String;

    iget-object v2, p1, Lcom/google/android/youtube/core/model/VastAd;->adFormat:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/google/android/youtube/core/utils/Util;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/google/android/youtube/core/model/VastAd;->streams:Ljava/util/List;

    iget-object v2, p1, Lcom/google/android/youtube/core/model/VastAd;->streams:Ljava/util/List;

    invoke-static {v1, v2}, Lcom/google/android/youtube/core/utils/Util;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/google/android/youtube/core/model/VastAd;->clickthroughUri:Landroid/net/Uri;

    iget-object v2, p1, Lcom/google/android/youtube/core/model/VastAd;->clickthroughUri:Landroid/net/Uri;

    invoke-static {v1, v2}, Lcom/google/android/youtube/core/utils/Util;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    iget v1, p0, Lcom/google/android/youtube/core/model/VastAd;->duration:I

    iget v2, p1, Lcom/google/android/youtube/core/model/VastAd;->duration:I

    if-ne v1, v2, :cond_0

    iget-boolean v1, p0, Lcom/google/android/youtube/core/model/VastAd;->shouldPingVssOnEngaged:Z

    iget-boolean v2, p1, Lcom/google/android/youtube/core/model/VastAd;->shouldPingVssOnEngaged:Z

    if-ne v1, v2, :cond_0

    iget-boolean v1, p0, Lcom/google/android/youtube/core/model/VastAd;->fallbackHint:Z

    iget-boolean v2, p1, Lcom/google/android/youtube/core/model/VastAd;->fallbackHint:Z

    if-ne v1, v2, :cond_0

    iget-wide v1, p0, Lcom/google/android/youtube/core/model/VastAd;->expiryTimeMillis:J

    iget-wide v3, p1, Lcom/google/android/youtube/core/model/VastAd;->expiryTimeMillis:J

    cmp-long v1, v1, v3

    if-nez v1, :cond_0

    iget-object v1, p0, Lcom/google/android/youtube/core/model/VastAd;->impressionUris:Ljava/util/List;

    iget-object v2, p1, Lcom/google/android/youtube/core/model/VastAd;->impressionUris:Ljava/util/List;

    invoke-static {v1, v2}, Lcom/google/android/youtube/core/utils/Util;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/google/android/youtube/core/model/VastAd;->startPingUris:Ljava/util/List;

    iget-object v2, p1, Lcom/google/android/youtube/core/model/VastAd;->startPingUris:Ljava/util/List;

    invoke-static {v1, v2}, Lcom/google/android/youtube/core/utils/Util;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/google/android/youtube/core/model/VastAd;->firstQuartilePingUris:Ljava/util/List;

    iget-object v2, p1, Lcom/google/android/youtube/core/model/VastAd;->firstQuartilePingUris:Ljava/util/List;

    invoke-static {v1, v2}, Lcom/google/android/youtube/core/utils/Util;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/google/android/youtube/core/model/VastAd;->midpointPingUris:Ljava/util/List;

    iget-object v2, p1, Lcom/google/android/youtube/core/model/VastAd;->midpointPingUris:Ljava/util/List;

    invoke-static {v1, v2}, Lcom/google/android/youtube/core/utils/Util;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/google/android/youtube/core/model/VastAd;->thirdQuartilePingUris:Ljava/util/List;

    iget-object v2, p1, Lcom/google/android/youtube/core/model/VastAd;->thirdQuartilePingUris:Ljava/util/List;

    invoke-static {v1, v2}, Lcom/google/android/youtube/core/utils/Util;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/google/android/youtube/core/model/VastAd;->skipPingUris:Ljava/util/List;

    iget-object v2, p1, Lcom/google/android/youtube/core/model/VastAd;->skipPingUris:Ljava/util/List;

    invoke-static {v1, v2}, Lcom/google/android/youtube/core/utils/Util;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/google/android/youtube/core/model/VastAd;->skipShownPingUris:Ljava/util/List;

    iget-object v2, p1, Lcom/google/android/youtube/core/model/VastAd;->skipShownPingUris:Ljava/util/List;

    invoke-static {v1, v2}, Lcom/google/android/youtube/core/utils/Util;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/google/android/youtube/core/model/VastAd;->engagedViewPingUris:Ljava/util/List;

    iget-object v2, p1, Lcom/google/android/youtube/core/model/VastAd;->engagedViewPingUris:Ljava/util/List;

    invoke-static {v1, v2}, Lcom/google/android/youtube/core/utils/Util;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/google/android/youtube/core/model/VastAd;->completePingUris:Ljava/util/List;

    iget-object v2, p1, Lcom/google/android/youtube/core/model/VastAd;->completePingUris:Ljava/util/List;

    invoke-static {v1, v2}, Lcom/google/android/youtube/core/utils/Util;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/google/android/youtube/core/model/VastAd;->closePingUris:Ljava/util/List;

    iget-object v2, p1, Lcom/google/android/youtube/core/model/VastAd;->closePingUris:Ljava/util/List;

    invoke-static {v1, v2}, Lcom/google/android/youtube/core/utils/Util;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/google/android/youtube/core/model/VastAd;->pausePingUris:Ljava/util/List;

    iget-object v2, p1, Lcom/google/android/youtube/core/model/VastAd;->pausePingUris:Ljava/util/List;

    invoke-static {v1, v2}, Lcom/google/android/youtube/core/utils/Util;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/google/android/youtube/core/model/VastAd;->resumePingUris:Ljava/util/List;

    iget-object v2, p1, Lcom/google/android/youtube/core/model/VastAd;->resumePingUris:Ljava/util/List;

    invoke-static {v1, v2}, Lcom/google/android/youtube/core/utils/Util;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/google/android/youtube/core/model/VastAd;->mutePingUris:Ljava/util/List;

    iget-object v2, p1, Lcom/google/android/youtube/core/model/VastAd;->mutePingUris:Ljava/util/List;

    invoke-static {v1, v2}, Lcom/google/android/youtube/core/utils/Util;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/google/android/youtube/core/model/VastAd;->fullscreenPingUris:Ljava/util/List;

    iget-object v2, p1, Lcom/google/android/youtube/core/model/VastAd;->fullscreenPingUris:Ljava/util/List;

    invoke-static {v1, v2}, Lcom/google/android/youtube/core/utils/Util;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/google/android/youtube/core/model/VastAd;->clickthroughPingUris:Ljava/util/List;

    iget-object v2, p1, Lcom/google/android/youtube/core/model/VastAd;->clickthroughPingUris:Ljava/util/List;

    invoke-static {v1, v2}, Lcom/google/android/youtube/core/utils/Util;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/google/android/youtube/core/model/VastAd;->videoTitleClickedPingUris:Ljava/util/List;

    iget-object v2, p1, Lcom/google/android/youtube/core/model/VastAd;->videoTitleClickedPingUris:Ljava/util/List;

    invoke-static {v1, v2}, Lcom/google/android/youtube/core/utils/Util;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/google/android/youtube/core/model/VastAd;->errorPingUris:Ljava/util/List;

    iget-object v2, p1, Lcom/google/android/youtube/core/model/VastAd;->errorPingUris:Ljava/util/List;

    invoke-static {v1, v2}, Lcom/google/android/youtube/core/utils/Util;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/google/android/youtube/core/model/VastAd;->adWrapperUri:Landroid/net/Uri;

    iget-object v2, p1, Lcom/google/android/youtube/core/model/VastAd;->adWrapperUri:Landroid/net/Uri;

    invoke-static {v1, v2}, Lcom/google/android/youtube/core/utils/Util;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v0, 0x1

    goto/16 :goto_0
.end method

.method public firstStreamUri()Landroid/net/Uri;
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/core/model/VastAd;->streams:Ljava/util/List;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/youtube/core/model/VastAd;->streams:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-nez v0, :cond_1

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_1
    iget-object v0, p0, Lcom/google/android/youtube/core/model/VastAd;->streams:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/youtube/core/model/Stream;

    iget-object v0, v0, Lcom/google/android/youtube/core/model/Stream;->uri:Landroid/net/Uri;

    goto :goto_0
.end method

.method public hasExpired(Lcom/google/android/youtube/core/utils/e;)Z
    .locals 4

    invoke-interface {p1}, Lcom/google/android/youtube/core/utils/e;->a()J

    move-result-wide v0

    iget-wide v2, p0, Lcom/google/android/youtube/core/model/VastAd;->expiryTimeMillis:J

    cmp-long v0, v0, v2

    if-ltz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isDummy()Z
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/core/model/VastAd;->streams:Ljava/util/List;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/youtube/core/model/VastAd;->streams:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-nez v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isEmpty()Z
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/core/model/VastAd;->impressionUris:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    return v0
.end method

.method public isSkippable()Z
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/core/model/VastAd;->skipPingUris:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    iget-boolean v0, p0, Lcom/google/android/youtube/core/model/VastAd;->isVastWrapper:Z

    if-eqz v0, :cond_0

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "VastAd Wrapper: [wrapperUri="

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/google/android/youtube/core/model/VastAd;->adWrapperUri:Landroid/net/Uri;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "]"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "VastAd: [adVideoId="

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/google/android/youtube/core/model/VastAd;->adVideoId:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", videoTitle= "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/youtube/core/model/VastAd;->title:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", adSystem = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/youtube/core/model/VastAd;->adSystem:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "]"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 5

    const/4 v1, 0x1

    const/4 v2, 0x0

    iget-object v0, p0, Lcom/google/android/youtube/core/model/VastAd;->impressionUris:Ljava/util/List;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeTypedList(Ljava/util/List;)V

    iget-object v0, p0, Lcom/google/android/youtube/core/model/VastAd;->adVideoId:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/google/android/youtube/core/model/VastAd;->originalVideoId:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/google/android/youtube/core/model/VastAd;->title:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/google/android/youtube/core/model/VastAd;->adSystem:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/google/android/youtube/core/model/VastAd;->adFormat:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    iget v0, p0, Lcom/google/android/youtube/core/model/VastAd;->duration:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    iget-object v0, p0, Lcom/google/android/youtube/core/model/VastAd;->streams:Ljava/util/List;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeTypedList(Ljava/util/List;)V

    iget-object v0, p0, Lcom/google/android/youtube/core/model/VastAd;->clickthroughUri:Landroid/net/Uri;

    invoke-virtual {p1, v0, v2}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    iget-object v0, p0, Lcom/google/android/youtube/core/model/VastAd;->startPingUris:Ljava/util/List;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeTypedList(Ljava/util/List;)V

    iget-object v0, p0, Lcom/google/android/youtube/core/model/VastAd;->firstQuartilePingUris:Ljava/util/List;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeTypedList(Ljava/util/List;)V

    iget-object v0, p0, Lcom/google/android/youtube/core/model/VastAd;->midpointPingUris:Ljava/util/List;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeTypedList(Ljava/util/List;)V

    iget-object v0, p0, Lcom/google/android/youtube/core/model/VastAd;->thirdQuartilePingUris:Ljava/util/List;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeTypedList(Ljava/util/List;)V

    iget-object v0, p0, Lcom/google/android/youtube/core/model/VastAd;->skipPingUris:Ljava/util/List;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeTypedList(Ljava/util/List;)V

    iget-object v0, p0, Lcom/google/android/youtube/core/model/VastAd;->skipShownPingUris:Ljava/util/List;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeTypedList(Ljava/util/List;)V

    iget-object v0, p0, Lcom/google/android/youtube/core/model/VastAd;->engagedViewPingUris:Ljava/util/List;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeTypedList(Ljava/util/List;)V

    iget-object v0, p0, Lcom/google/android/youtube/core/model/VastAd;->completePingUris:Ljava/util/List;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeTypedList(Ljava/util/List;)V

    iget-object v0, p0, Lcom/google/android/youtube/core/model/VastAd;->closePingUris:Ljava/util/List;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeTypedList(Ljava/util/List;)V

    iget-object v0, p0, Lcom/google/android/youtube/core/model/VastAd;->pausePingUris:Ljava/util/List;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeTypedList(Ljava/util/List;)V

    iget-object v0, p0, Lcom/google/android/youtube/core/model/VastAd;->resumePingUris:Ljava/util/List;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeTypedList(Ljava/util/List;)V

    iget-object v0, p0, Lcom/google/android/youtube/core/model/VastAd;->mutePingUris:Ljava/util/List;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeTypedList(Ljava/util/List;)V

    iget-object v0, p0, Lcom/google/android/youtube/core/model/VastAd;->fullscreenPingUris:Ljava/util/List;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeTypedList(Ljava/util/List;)V

    iget-object v0, p0, Lcom/google/android/youtube/core/model/VastAd;->clickthroughPingUris:Ljava/util/List;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeTypedList(Ljava/util/List;)V

    iget-object v0, p0, Lcom/google/android/youtube/core/model/VastAd;->videoTitleClickedPingUris:Ljava/util/List;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeTypedList(Ljava/util/List;)V

    iget-object v0, p0, Lcom/google/android/youtube/core/model/VastAd;->errorPingUris:Ljava/util/List;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeTypedList(Ljava/util/List;)V

    iget-boolean v0, p0, Lcom/google/android/youtube/core/model/VastAd;->shouldPingVssOnEngaged:Z

    if-eqz v0, :cond_0

    move v0, v1

    :goto_0
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    iget-boolean v0, p0, Lcom/google/android/youtube/core/model/VastAd;->fallbackHint:Z

    if-eqz v0, :cond_1

    move v0, v1

    :goto_1
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    iget-wide v3, p0, Lcom/google/android/youtube/core/model/VastAd;->expiryTimeMillis:J

    invoke-virtual {p1, v3, v4}, Landroid/os/Parcel;->writeLong(J)V

    iget-boolean v0, p0, Lcom/google/android/youtube/core/model/VastAd;->isPublicVideo:Z

    if-eqz v0, :cond_2

    :goto_2
    invoke-virtual {p1, v1}, Landroid/os/Parcel;->writeInt(I)V

    iget-object v0, p0, Lcom/google/android/youtube/core/model/VastAd;->adWrapperUri:Landroid/net/Uri;

    invoke-virtual {p1, v0, v2}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    return-void

    :cond_0
    move v0, v2

    goto :goto_0

    :cond_1
    move v0, v2

    goto :goto_1

    :cond_2
    move v1, v2

    goto :goto_2
.end method
