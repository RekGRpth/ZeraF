.class public final Lcom/google/android/youtube/core/model/SocialSettings$Action;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field public final a:Lcom/google/android/youtube/core/model/SocialSettings$Action$ActionType;

.field public final b:Z


# direct methods
.method public constructor <init>(Lcom/google/android/youtube/core/model/SocialSettings$Action$ActionType;Z)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/google/android/youtube/core/model/SocialSettings$Action;->a:Lcom/google/android/youtube/core/model/SocialSettings$Action$ActionType;

    iput-boolean p2, p0, Lcom/google/android/youtube/core/model/SocialSettings$Action;->b:Z

    return-void
.end method


# virtual methods
.method public final equals(Ljava/lang/Object;)Z
    .locals 3

    const/4 v0, 0x0

    instance-of v1, p1, Lcom/google/android/youtube/core/model/SocialSettings$Action;

    if-eqz v1, :cond_0

    check-cast p1, Lcom/google/android/youtube/core/model/SocialSettings$Action;

    iget-object v1, p1, Lcom/google/android/youtube/core/model/SocialSettings$Action;->a:Lcom/google/android/youtube/core/model/SocialSettings$Action$ActionType;

    iget-object v2, p0, Lcom/google/android/youtube/core/model/SocialSettings$Action;->a:Lcom/google/android/youtube/core/model/SocialSettings$Action$ActionType;

    if-ne v1, v2, :cond_0

    iget-boolean v1, p1, Lcom/google/android/youtube/core/model/SocialSettings$Action;->b:Z

    iget-boolean v2, p0, Lcom/google/android/youtube/core/model/SocialSettings$Action;->b:Z

    if-ne v1, v2, :cond_0

    const/4 v0, 0x1

    :cond_0
    return v0
.end method

.method public final hashCode()I
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/core/model/SocialSettings$Action;->a:Lcom/google/android/youtube/core/model/SocialSettings$Action$ActionType;

    invoke-virtual {v0}, Lcom/google/android/youtube/core/model/SocialSettings$Action$ActionType;->ordinal()I

    move-result v0

    return v0
.end method
