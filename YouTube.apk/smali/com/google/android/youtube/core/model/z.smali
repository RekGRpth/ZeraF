.class public final Lcom/google/android/youtube/core/model/z;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/youtube/core/model/i;


# instance fields
.field private final a:Ljava/util/List;

.field private final b:Ljava/util/List;

.field private final c:Ljava/util/List;


# direct methods
.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/android/youtube/core/model/z;->a:Ljava/util/List;

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/android/youtube/core/model/z;->b:Ljava/util/List;

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/android/youtube/core/model/z;->c:Ljava/util/List;

    return-void
.end method


# virtual methods
.method public final a()Lcom/google/android/youtube/core/model/SubtitleWindowTextTimeline;
    .locals 5

    new-instance v0, Lcom/google/android/youtube/core/model/SubtitleWindowTextTimeline;

    iget-object v1, p0, Lcom/google/android/youtube/core/model/z;->a:Ljava/util/List;

    iget-object v2, p0, Lcom/google/android/youtube/core/model/z;->b:Ljava/util/List;

    iget-object v3, p0, Lcom/google/android/youtube/core/model/z;->c:Ljava/util/List;

    const/4 v4, 0x0

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/google/android/youtube/core/model/SubtitleWindowTextTimeline;-><init>(Ljava/util/List;Ljava/util/List;Ljava/util/List;Lcom/google/android/youtube/core/model/y;)V

    return-object v0
.end method

.method public final a(Ljava/lang/String;II)Lcom/google/android/youtube/core/model/z;
    .locals 2

    iget-object v0, p0, Lcom/google/android/youtube/core/model/z;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/youtube/core/model/z;->a:Ljava/util/List;

    iget-object v1, p0, Lcom/google/android/youtube/core/model/z;->a:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    if-ge p2, v0, :cond_0

    const-string v0, "subtitles are not given in non-decreasing start time order"

    invoke-static {v0}, Lcom/google/android/youtube/core/L;->c(Ljava/lang/String;)V

    :cond_0
    iget-object v0, p0, Lcom/google/android/youtube/core/model/z;->a:Ljava/util/List;

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    iget-object v0, p0, Lcom/google/android/youtube/core/model/z;->b:Ljava/util/List;

    invoke-static {p3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    iget-object v0, p0, Lcom/google/android/youtube/core/model/z;->c:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    return-object p0
.end method

.method public final b(Ljava/lang/String;II)Lcom/google/android/youtube/core/model/z;
    .locals 3

    iget-object v0, p0, Lcom/google/android/youtube/core/model/z;->c:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v0, p0, Lcom/google/android/youtube/core/model/z;->c:Ljava/util/List;

    iget-object v2, p0, Lcom/google/android/youtube/core/model/z;->c:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    add-int/lit8 v2, v2, -0x1

    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    :cond_0
    invoke-virtual {p0, p1, p2, p3}, Lcom/google/android/youtube/core/model/z;->a(Ljava/lang/String;II)Lcom/google/android/youtube/core/model/z;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic build()Ljava/lang/Object;
    .locals 1

    invoke-virtual {p0}, Lcom/google/android/youtube/core/model/z;->a()Lcom/google/android/youtube/core/model/SubtitleWindowTextTimeline;

    move-result-object v0

    return-object v0
.end method
