.class public final Lcom/google/android/youtube/core/model/Show$Builder;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/youtube/core/model/i;
.implements Ljava/io/Serializable;


# instance fields
.field private credits:Ljava/util/Map;

.field private description:Ljava/lang/String;

.field private hqPosterUri:Landroid/net/Uri;

.field private posterUri:Landroid/net/Uri;

.field private seasonsUri:Landroid/net/Uri;

.field private selfUri:Landroid/net/Uri;

.field private showGenres:Ljava/util/List;

.field private title:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/android/youtube/core/model/Show$Builder;->showGenres:Ljava/util/List;

    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/google/android/youtube/core/model/Show$Builder;->credits:Ljava/util/Map;

    return-void
.end method

.method private readObject(Ljava/io/ObjectInputStream;)V
    .locals 1

    invoke-virtual {p1}, Ljava/io/ObjectInputStream;->readObject()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iput-object v0, p0, Lcom/google/android/youtube/core/model/Show$Builder;->title:Ljava/lang/String;

    invoke-virtual {p1}, Ljava/io/ObjectInputStream;->readObject()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iput-object v0, p0, Lcom/google/android/youtube/core/model/Show$Builder;->description:Ljava/lang/String;

    invoke-virtual {p1}, Ljava/io/ObjectInputStream;->readObject()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/List;

    iput-object v0, p0, Lcom/google/android/youtube/core/model/Show$Builder;->showGenres:Ljava/util/List;

    invoke-virtual {p1}, Ljava/io/ObjectInputStream;->readObject()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/android/youtube/core/utils/Util;->a(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/youtube/core/model/Show$Builder;->selfUri:Landroid/net/Uri;

    invoke-virtual {p1}, Ljava/io/ObjectInputStream;->readObject()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/android/youtube/core/utils/Util;->a(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/youtube/core/model/Show$Builder;->seasonsUri:Landroid/net/Uri;

    invoke-virtual {p1}, Ljava/io/ObjectInputStream;->readObject()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map;

    iput-object v0, p0, Lcom/google/android/youtube/core/model/Show$Builder;->credits:Ljava/util/Map;

    invoke-virtual {p1}, Ljava/io/ObjectInputStream;->readObject()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/android/youtube/core/utils/Util;->a(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/youtube/core/model/Show$Builder;->posterUri:Landroid/net/Uri;

    invoke-virtual {p1}, Ljava/io/ObjectInputStream;->readObject()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/android/youtube/core/utils/Util;->a(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/youtube/core/model/Show$Builder;->hqPosterUri:Landroid/net/Uri;

    return-void
.end method

.method private readResolve()Ljava/lang/Object;
    .locals 1

    invoke-virtual {p0}, Lcom/google/android/youtube/core/model/Show$Builder;->build()Lcom/google/android/youtube/core/model/Show;

    move-result-object v0

    return-object v0
.end method

.method private writeObject(Ljava/io/ObjectOutputStream;)V
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/core/model/Show$Builder;->title:Ljava/lang/String;

    invoke-virtual {p1, v0}, Ljava/io/ObjectOutputStream;->writeObject(Ljava/lang/Object;)V

    iget-object v0, p0, Lcom/google/android/youtube/core/model/Show$Builder;->description:Ljava/lang/String;

    invoke-virtual {p1, v0}, Ljava/io/ObjectOutputStream;->writeObject(Ljava/lang/Object;)V

    iget-object v0, p0, Lcom/google/android/youtube/core/model/Show$Builder;->showGenres:Ljava/util/List;

    invoke-virtual {p1, v0}, Ljava/io/ObjectOutputStream;->writeObject(Ljava/lang/Object;)V

    iget-object v0, p0, Lcom/google/android/youtube/core/model/Show$Builder;->selfUri:Landroid/net/Uri;

    invoke-static {v0}, Lcom/google/android/youtube/core/utils/Util;->a(Landroid/net/Uri;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/io/ObjectOutputStream;->writeObject(Ljava/lang/Object;)V

    iget-object v0, p0, Lcom/google/android/youtube/core/model/Show$Builder;->seasonsUri:Landroid/net/Uri;

    invoke-static {v0}, Lcom/google/android/youtube/core/utils/Util;->a(Landroid/net/Uri;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/io/ObjectOutputStream;->writeObject(Ljava/lang/Object;)V

    iget-object v0, p0, Lcom/google/android/youtube/core/model/Show$Builder;->credits:Ljava/util/Map;

    invoke-virtual {p1, v0}, Ljava/io/ObjectOutputStream;->writeObject(Ljava/lang/Object;)V

    iget-object v0, p0, Lcom/google/android/youtube/core/model/Show$Builder;->posterUri:Landroid/net/Uri;

    invoke-static {v0}, Lcom/google/android/youtube/core/utils/Util;->a(Landroid/net/Uri;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/io/ObjectOutputStream;->writeObject(Ljava/lang/Object;)V

    iget-object v0, p0, Lcom/google/android/youtube/core/model/Show$Builder;->hqPosterUri:Landroid/net/Uri;

    invoke-static {v0}, Lcom/google/android/youtube/core/utils/Util;->a(Landroid/net/Uri;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/io/ObjectOutputStream;->writeObject(Ljava/lang/Object;)V

    return-void
.end method


# virtual methods
.method public final addCredit(Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/youtube/core/model/Show$Builder;
    .locals 2

    const-string v0, "role may not be null"

    invoke-static {p1, v0}, Lcom/google/android/youtube/core/utils/s;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const-string v0, "name may not be null"

    invoke-static {p2, v0}, Lcom/google/android/youtube/core/utils/s;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v0, p0, Lcom/google/android/youtube/core/model/Show$Builder;->credits:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/List;

    if-nez v0, :cond_0

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iget-object v1, p0, Lcom/google/android/youtube/core/model/Show$Builder;->credits:Ljava/util/Map;

    invoke-interface {v1, p1, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    :cond_0
    invoke-interface {v0, p2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    return-object p0
.end method

.method public final addShowGenre(Lcom/google/android/youtube/core/model/Video$Episode$EpisodeGenre;)Lcom/google/android/youtube/core/model/Show$Builder;
    .locals 1

    const-string v0, "showGenre may not be null"

    invoke-static {p1, v0}, Lcom/google/android/youtube/core/utils/s;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v0, p0, Lcom/google/android/youtube/core/model/Show$Builder;->showGenres:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    return-object p0
.end method

.method public final build()Lcom/google/android/youtube/core/model/Show;
    .locals 9

    new-instance v0, Lcom/google/android/youtube/core/model/Show;

    iget-object v1, p0, Lcom/google/android/youtube/core/model/Show$Builder;->title:Ljava/lang/String;

    iget-object v2, p0, Lcom/google/android/youtube/core/model/Show$Builder;->description:Ljava/lang/String;

    iget-object v3, p0, Lcom/google/android/youtube/core/model/Show$Builder;->showGenres:Ljava/util/List;

    iget-object v4, p0, Lcom/google/android/youtube/core/model/Show$Builder;->selfUri:Landroid/net/Uri;

    iget-object v5, p0, Lcom/google/android/youtube/core/model/Show$Builder;->seasonsUri:Landroid/net/Uri;

    iget-object v6, p0, Lcom/google/android/youtube/core/model/Show$Builder;->credits:Ljava/util/Map;

    iget-object v7, p0, Lcom/google/android/youtube/core/model/Show$Builder;->posterUri:Landroid/net/Uri;

    iget-object v8, p0, Lcom/google/android/youtube/core/model/Show$Builder;->hqPosterUri:Landroid/net/Uri;

    invoke-direct/range {v0 .. v8}, Lcom/google/android/youtube/core/model/Show;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/util/List;Landroid/net/Uri;Landroid/net/Uri;Ljava/util/Map;Landroid/net/Uri;Landroid/net/Uri;)V

    return-object v0
.end method

.method public final bridge synthetic build()Ljava/lang/Object;
    .locals 1

    invoke-virtual {p0}, Lcom/google/android/youtube/core/model/Show$Builder;->build()Lcom/google/android/youtube/core/model/Show;

    move-result-object v0

    return-object v0
.end method

.method public final credits(Ljava/util/Map;)Lcom/google/android/youtube/core/model/Show$Builder;
    .locals 5

    iget-object v0, p0, Lcom/google/android/youtube/core/model/Show$Builder;->credits:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->clear()V

    if-eqz p1, :cond_0

    invoke-interface {p1}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    iget-object v2, p0, Lcom/google/android/youtube/core/model/Show$Builder;->credits:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v3

    new-instance v4, Ljava/util/ArrayList;

    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Collection;

    invoke-direct {v4, v0}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    invoke-interface {v2, v3, v4}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    :cond_0
    return-object p0
.end method

.method public final description(Ljava/lang/String;)Lcom/google/android/youtube/core/model/Show$Builder;
    .locals 0

    iput-object p1, p0, Lcom/google/android/youtube/core/model/Show$Builder;->description:Ljava/lang/String;

    return-object p0
.end method

.method public final hqPosterUri(Landroid/net/Uri;)Lcom/google/android/youtube/core/model/Show$Builder;
    .locals 0

    iput-object p1, p0, Lcom/google/android/youtube/core/model/Show$Builder;->hqPosterUri:Landroid/net/Uri;

    return-object p0
.end method

.method public final posterUri(Landroid/net/Uri;)Lcom/google/android/youtube/core/model/Show$Builder;
    .locals 0

    iput-object p1, p0, Lcom/google/android/youtube/core/model/Show$Builder;->posterUri:Landroid/net/Uri;

    return-object p0
.end method

.method public final seasonsUri(Landroid/net/Uri;)Lcom/google/android/youtube/core/model/Show$Builder;
    .locals 0

    iput-object p1, p0, Lcom/google/android/youtube/core/model/Show$Builder;->seasonsUri:Landroid/net/Uri;

    return-object p0
.end method

.method public final selfUri(Landroid/net/Uri;)Lcom/google/android/youtube/core/model/Show$Builder;
    .locals 0

    iput-object p1, p0, Lcom/google/android/youtube/core/model/Show$Builder;->selfUri:Landroid/net/Uri;

    return-object p0
.end method

.method public final showGenres(Ljava/util/List;)Lcom/google/android/youtube/core/model/Show$Builder;
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/core/model/Show$Builder;->showGenres:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    iget-object v0, p0, Lcom/google/android/youtube/core/model/Show$Builder;->showGenres:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    return-object p0
.end method

.method public final title(Ljava/lang/String;)Lcom/google/android/youtube/core/model/Show$Builder;
    .locals 0

    iput-object p1, p0, Lcom/google/android/youtube/core/model/Show$Builder;->title:Ljava/lang/String;

    return-object p0
.end method
