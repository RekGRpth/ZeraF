.class public Lcom/google/android/youtube/core/model/ah;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field public final f:Ljava/util/List;

.field public final g:Ljava/util/List;

.field public final h:Ljava/util/Map;

.field public final i:Ljava/util/Date;

.field public final j:Ljava/util/Date;

.field public final k:Ljava/util/Date;

.field public final l:Landroid/net/Uri;

.field public final m:Landroid/net/Uri;


# direct methods
.method public constructor <init>(Ljava/util/List;Ljava/util/List;Ljava/util/Map;Ljava/util/Date;Ljava/util/Date;Ljava/util/Date;Landroid/net/Uri;Landroid/net/Uri;)V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const-string v0, "releaseMediums may not be null"

    invoke-static {p1, v0}, Lcom/google/android/youtube/core/utils/s;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/List;

    invoke-static {v0}, Lcom/google/android/youtube/core/utils/Util;->a(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/youtube/core/model/ah;->f:Ljava/util/List;

    const-string v0, "genres may not be null"

    invoke-static {p2, v0}, Lcom/google/android/youtube/core/utils/s;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/List;

    invoke-static {v0}, Lcom/google/android/youtube/core/utils/Util;->a(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/youtube/core/model/ah;->g:Ljava/util/List;

    const-string v0, "credits may not be null"

    invoke-static {p3, v0}, Lcom/google/android/youtube/core/utils/s;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map;

    invoke-static {v0}, Lcom/google/android/youtube/core/utils/Util;->b(Ljava/util/Map;)Ljava/util/Map;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/youtube/core/model/ah;->h:Ljava/util/Map;

    iput-object p4, p0, Lcom/google/android/youtube/core/model/ah;->i:Ljava/util/Date;

    iput-object p5, p0, Lcom/google/android/youtube/core/model/ah;->j:Ljava/util/Date;

    iput-object p6, p0, Lcom/google/android/youtube/core/model/ah;->k:Ljava/util/Date;

    iput-object p7, p0, Lcom/google/android/youtube/core/model/ah;->l:Landroid/net/Uri;

    iput-object p8, p0, Lcom/google/android/youtube/core/model/ah;->m:Landroid/net/Uri;

    return-void
.end method
