.class public final Lcom/google/android/youtube/core/model/Branding;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Ljava/io/Serializable;


# instance fields
.field public final bannerTargetUri:Landroid/net/Uri;

.field public final bannerUri:Landroid/net/Uri;

.field public final channelBannerMobileExtraHdUri:Landroid/net/Uri;

.field public final channelBannerMobileHdUri:Landroid/net/Uri;

.field public final channelBannerMobileLowUri:Landroid/net/Uri;

.field public final channelBannerMobileMediumHdUri:Landroid/net/Uri;

.field public final channelBannerMobileMediumUri:Landroid/net/Uri;

.field public final channelBannerTabletExtraHdUri:Landroid/net/Uri;

.field public final channelBannerTabletHdUri:Landroid/net/Uri;

.field public final channelBannerTabletLowUri:Landroid/net/Uri;

.field public final channelBannerTabletMediumUri:Landroid/net/Uri;

.field public final channelBannerTvUri:Landroid/net/Uri;

.field public final description:Ljava/lang/String;

.field public final featuredPlaylistId:Ljava/lang/String;

.field public final interstitialTargetUri:Landroid/net/Uri;

.field public final interstitialUri:Landroid/net/Uri;

.field public final keywords:Ljava/lang/String;

.field public final largeBannerUri:Landroid/net/Uri;

.field public final title:Ljava/lang/String;

.field public final tvBannerUri:Landroid/net/Uri;

.field public final watermarkTargetUri:Landroid/net/Uri;

.field public final watermarkUri:Landroid/net/Uri;


# direct methods
.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Landroid/net/Uri;Landroid/net/Uri;Landroid/net/Uri;Landroid/net/Uri;Landroid/net/Uri;Landroid/net/Uri;Landroid/net/Uri;Ljava/lang/String;Landroid/net/Uri;Landroid/net/Uri;Landroid/net/Uri;Landroid/net/Uri;Landroid/net/Uri;Landroid/net/Uri;Landroid/net/Uri;Landroid/net/Uri;Landroid/net/Uri;Landroid/net/Uri;Landroid/net/Uri;)V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/google/android/youtube/core/model/Branding;->title:Ljava/lang/String;

    iput-object p2, p0, Lcom/google/android/youtube/core/model/Branding;->description:Ljava/lang/String;

    iput-object p3, p0, Lcom/google/android/youtube/core/model/Branding;->keywords:Ljava/lang/String;

    iput-object p4, p0, Lcom/google/android/youtube/core/model/Branding;->bannerUri:Landroid/net/Uri;

    iput-object p5, p0, Lcom/google/android/youtube/core/model/Branding;->largeBannerUri:Landroid/net/Uri;

    iput-object p6, p0, Lcom/google/android/youtube/core/model/Branding;->bannerTargetUri:Landroid/net/Uri;

    iput-object p7, p0, Lcom/google/android/youtube/core/model/Branding;->watermarkUri:Landroid/net/Uri;

    iput-object p8, p0, Lcom/google/android/youtube/core/model/Branding;->watermarkTargetUri:Landroid/net/Uri;

    iput-object p9, p0, Lcom/google/android/youtube/core/model/Branding;->interstitialUri:Landroid/net/Uri;

    iput-object p10, p0, Lcom/google/android/youtube/core/model/Branding;->interstitialTargetUri:Landroid/net/Uri;

    iput-object p11, p0, Lcom/google/android/youtube/core/model/Branding;->featuredPlaylistId:Ljava/lang/String;

    iput-object p12, p0, Lcom/google/android/youtube/core/model/Branding;->tvBannerUri:Landroid/net/Uri;

    iput-object p13, p0, Lcom/google/android/youtube/core/model/Branding;->channelBannerMobileLowUri:Landroid/net/Uri;

    iput-object p14, p0, Lcom/google/android/youtube/core/model/Branding;->channelBannerMobileMediumUri:Landroid/net/Uri;

    move-object/from16 v0, p15

    iput-object v0, p0, Lcom/google/android/youtube/core/model/Branding;->channelBannerMobileMediumHdUri:Landroid/net/Uri;

    move-object/from16 v0, p16

    iput-object v0, p0, Lcom/google/android/youtube/core/model/Branding;->channelBannerMobileHdUri:Landroid/net/Uri;

    move-object/from16 v0, p17

    iput-object v0, p0, Lcom/google/android/youtube/core/model/Branding;->channelBannerMobileExtraHdUri:Landroid/net/Uri;

    move-object/from16 v0, p18

    iput-object v0, p0, Lcom/google/android/youtube/core/model/Branding;->channelBannerTabletLowUri:Landroid/net/Uri;

    move-object/from16 v0, p19

    iput-object v0, p0, Lcom/google/android/youtube/core/model/Branding;->channelBannerTabletMediumUri:Landroid/net/Uri;

    move-object/from16 v0, p20

    iput-object v0, p0, Lcom/google/android/youtube/core/model/Branding;->channelBannerTabletHdUri:Landroid/net/Uri;

    move-object/from16 v0, p21

    iput-object v0, p0, Lcom/google/android/youtube/core/model/Branding;->channelBannerTabletExtraHdUri:Landroid/net/Uri;

    move-object/from16 v0, p22

    iput-object v0, p0, Lcom/google/android/youtube/core/model/Branding;->channelBannerTvUri:Landroid/net/Uri;

    return-void
.end method

.method private readObject(Ljava/io/ObjectInputStream;)V
    .locals 2

    new-instance v0, Ljava/io/InvalidObjectException;

    const-string v1, "builder required"

    invoke-direct {v0, v1}, Ljava/io/InvalidObjectException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method private writeReplace()Ljava/lang/Object;
    .locals 1

    invoke-virtual {p0}, Lcom/google/android/youtube/core/model/Branding;->buildUpon()Lcom/google/android/youtube/core/model/Branding$Builder;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public final buildUpon()Lcom/google/android/youtube/core/model/Branding$Builder;
    .locals 2

    new-instance v0, Lcom/google/android/youtube/core/model/Branding$Builder;

    invoke-direct {v0}, Lcom/google/android/youtube/core/model/Branding$Builder;-><init>()V

    iget-object v1, p0, Lcom/google/android/youtube/core/model/Branding;->title:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/google/android/youtube/core/model/Branding$Builder;->title(Ljava/lang/String;)Lcom/google/android/youtube/core/model/Branding$Builder;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/youtube/core/model/Branding;->description:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/google/android/youtube/core/model/Branding$Builder;->description(Ljava/lang/String;)Lcom/google/android/youtube/core/model/Branding$Builder;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/youtube/core/model/Branding;->keywords:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/google/android/youtube/core/model/Branding$Builder;->keywords(Ljava/lang/String;)Lcom/google/android/youtube/core/model/Branding$Builder;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/youtube/core/model/Branding;->bannerUri:Landroid/net/Uri;

    invoke-virtual {v0, v1}, Lcom/google/android/youtube/core/model/Branding$Builder;->bannerUri(Landroid/net/Uri;)Lcom/google/android/youtube/core/model/Branding$Builder;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/youtube/core/model/Branding;->largeBannerUri:Landroid/net/Uri;

    invoke-virtual {v0, v1}, Lcom/google/android/youtube/core/model/Branding$Builder;->largeBannerUri(Landroid/net/Uri;)Lcom/google/android/youtube/core/model/Branding$Builder;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/youtube/core/model/Branding;->bannerTargetUri:Landroid/net/Uri;

    invoke-virtual {v0, v1}, Lcom/google/android/youtube/core/model/Branding$Builder;->bannerTargetUri(Landroid/net/Uri;)Lcom/google/android/youtube/core/model/Branding$Builder;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/youtube/core/model/Branding;->watermarkUri:Landroid/net/Uri;

    invoke-virtual {v0, v1}, Lcom/google/android/youtube/core/model/Branding$Builder;->watermarkUri(Landroid/net/Uri;)Lcom/google/android/youtube/core/model/Branding$Builder;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/youtube/core/model/Branding;->watermarkTargetUri:Landroid/net/Uri;

    invoke-virtual {v0, v1}, Lcom/google/android/youtube/core/model/Branding$Builder;->watermarkTargetUri(Landroid/net/Uri;)Lcom/google/android/youtube/core/model/Branding$Builder;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/youtube/core/model/Branding;->interstitialUri:Landroid/net/Uri;

    invoke-virtual {v0, v1}, Lcom/google/android/youtube/core/model/Branding$Builder;->interstitialUri(Landroid/net/Uri;)Lcom/google/android/youtube/core/model/Branding$Builder;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/youtube/core/model/Branding;->interstitialTargetUri:Landroid/net/Uri;

    invoke-virtual {v0, v1}, Lcom/google/android/youtube/core/model/Branding$Builder;->interstitialTargetUri(Landroid/net/Uri;)Lcom/google/android/youtube/core/model/Branding$Builder;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/youtube/core/model/Branding;->featuredPlaylistId:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/google/android/youtube/core/model/Branding$Builder;->featuredPlaylistId(Ljava/lang/String;)Lcom/google/android/youtube/core/model/Branding$Builder;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/youtube/core/model/Branding;->tvBannerUri:Landroid/net/Uri;

    invoke-virtual {v0, v1}, Lcom/google/android/youtube/core/model/Branding$Builder;->tvBannerUri(Landroid/net/Uri;)Lcom/google/android/youtube/core/model/Branding$Builder;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/youtube/core/model/Branding;->channelBannerMobileLowUri:Landroid/net/Uri;

    invoke-virtual {v0, v1}, Lcom/google/android/youtube/core/model/Branding$Builder;->channelBannerMobileLowUri(Landroid/net/Uri;)Lcom/google/android/youtube/core/model/Branding$Builder;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/youtube/core/model/Branding;->channelBannerMobileMediumUri:Landroid/net/Uri;

    invoke-virtual {v0, v1}, Lcom/google/android/youtube/core/model/Branding$Builder;->channelBannerMobileMediumUri(Landroid/net/Uri;)Lcom/google/android/youtube/core/model/Branding$Builder;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/youtube/core/model/Branding;->channelBannerMobileMediumHdUri:Landroid/net/Uri;

    invoke-virtual {v0, v1}, Lcom/google/android/youtube/core/model/Branding$Builder;->channelBannerMobileMediumHdUri(Landroid/net/Uri;)Lcom/google/android/youtube/core/model/Branding$Builder;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/youtube/core/model/Branding;->channelBannerMobileHdUri:Landroid/net/Uri;

    invoke-virtual {v0, v1}, Lcom/google/android/youtube/core/model/Branding$Builder;->channelBannerMobileHdUri(Landroid/net/Uri;)Lcom/google/android/youtube/core/model/Branding$Builder;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/youtube/core/model/Branding;->channelBannerMobileExtraHdUri:Landroid/net/Uri;

    invoke-virtual {v0, v1}, Lcom/google/android/youtube/core/model/Branding$Builder;->channelBannerMobileExtraHdUri(Landroid/net/Uri;)Lcom/google/android/youtube/core/model/Branding$Builder;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/youtube/core/model/Branding;->channelBannerTabletLowUri:Landroid/net/Uri;

    invoke-virtual {v0, v1}, Lcom/google/android/youtube/core/model/Branding$Builder;->channelBannerTabletLowUri(Landroid/net/Uri;)Lcom/google/android/youtube/core/model/Branding$Builder;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/youtube/core/model/Branding;->channelBannerTabletMediumUri:Landroid/net/Uri;

    invoke-virtual {v0, v1}, Lcom/google/android/youtube/core/model/Branding$Builder;->channelBannerTabletMediumUri(Landroid/net/Uri;)Lcom/google/android/youtube/core/model/Branding$Builder;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/youtube/core/model/Branding;->channelBannerTabletHdUri:Landroid/net/Uri;

    invoke-virtual {v0, v1}, Lcom/google/android/youtube/core/model/Branding$Builder;->channelBannerTabletHdUri(Landroid/net/Uri;)Lcom/google/android/youtube/core/model/Branding$Builder;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/youtube/core/model/Branding;->channelBannerTabletExtraHdUri:Landroid/net/Uri;

    invoke-virtual {v0, v1}, Lcom/google/android/youtube/core/model/Branding$Builder;->channelBannerTabletExtraHdUri(Landroid/net/Uri;)Lcom/google/android/youtube/core/model/Branding$Builder;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/youtube/core/model/Branding;->channelBannerTvUri:Landroid/net/Uri;

    invoke-virtual {v0, v1}, Lcom/google/android/youtube/core/model/Branding$Builder;->channelBannerTvUri(Landroid/net/Uri;)Lcom/google/android/youtube/core/model/Branding$Builder;

    move-result-object v0

    return-object v0
.end method
