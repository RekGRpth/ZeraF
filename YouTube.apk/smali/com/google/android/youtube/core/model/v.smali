.class public final Lcom/google/android/youtube/core/model/v;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/youtube/core/model/i;


# instance fields
.field private final a:Ljava/util/List;

.field private final b:Ljava/util/List;


# direct methods
.method public constructor <init>()V
    .locals 2

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/android/youtube/core/model/v;->a:Ljava/util/List;

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/android/youtube/core/model/v;->b:Ljava/util/List;

    const/4 v0, 0x0

    sget-object v1, Lcom/google/android/youtube/core/model/SubtitleWindowSettings;->DEFAULT_SUBTITLE_WINDOW_SETTINGS:Lcom/google/android/youtube/core/model/SubtitleWindowSettings;

    invoke-virtual {p0, v0, v1}, Lcom/google/android/youtube/core/model/v;->a(ILcom/google/android/youtube/core/model/SubtitleWindowSettings;)Lcom/google/android/youtube/core/model/v;

    return-void
.end method


# virtual methods
.method public final a()Lcom/google/android/youtube/core/model/SubtitleWindowSettingsTimeline;
    .locals 4

    new-instance v0, Lcom/google/android/youtube/core/model/SubtitleWindowSettingsTimeline;

    iget-object v1, p0, Lcom/google/android/youtube/core/model/v;->a:Ljava/util/List;

    iget-object v2, p0, Lcom/google/android/youtube/core/model/v;->b:Ljava/util/List;

    const/4 v3, 0x0

    invoke-direct {v0, v1, v2, v3}, Lcom/google/android/youtube/core/model/SubtitleWindowSettingsTimeline;-><init>(Ljava/util/List;Ljava/util/List;Lcom/google/android/youtube/core/model/u;)V

    return-object v0
.end method

.method public final a(ILcom/google/android/youtube/core/model/SubtitleWindowSettings;)Lcom/google/android/youtube/core/model/v;
    .locals 2

    iget-object v0, p0, Lcom/google/android/youtube/core/model/v;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/youtube/core/model/v;->a:Ljava/util/List;

    iget-object v1, p0, Lcom/google/android/youtube/core/model/v;->a:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    if-ge p1, v0, :cond_0

    const-string v0, "subtitle settings are not given in non-decreasing start time order"

    invoke-static {v0}, Lcom/google/android/youtube/core/L;->c(Ljava/lang/String;)V

    :cond_0
    iget-object v0, p0, Lcom/google/android/youtube/core/model/v;->a:Ljava/util/List;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    iget-object v0, p0, Lcom/google/android/youtube/core/model/v;->b:Ljava/util/List;

    invoke-interface {v0, p2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    return-object p0
.end method

.method public final synthetic build()Ljava/lang/Object;
    .locals 1

    invoke-virtual {p0}, Lcom/google/android/youtube/core/model/v;->a()Lcom/google/android/youtube/core/model/SubtitleWindowSettingsTimeline;

    move-result-object v0

    return-object v0
.end method
