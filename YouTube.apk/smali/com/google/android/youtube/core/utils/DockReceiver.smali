.class public final Lcom/google/android/youtube/core/utils/DockReceiver;
.super Landroid/content/BroadcastReceiver;
.source "SourceFile"


# instance fields
.field private final a:Landroid/content/IntentFilter;

.field private final b:Landroid/content/Context;

.field private final c:Lcom/google/android/youtube/core/utils/k;

.field private d:Z

.field private e:I


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/google/android/youtube/core/utils/k;)V
    .locals 3

    const/4 v2, 0x0

    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    new-instance v0, Landroid/content/IntentFilter;

    const-string v1, "android.intent.action.DOCK_EVENT"

    invoke-direct {v0, v1}, Landroid/content/IntentFilter;-><init>(Ljava/lang/String;)V

    iput-object v0, p0, Lcom/google/android/youtube/core/utils/DockReceiver;->a:Landroid/content/IntentFilter;

    iput-boolean v2, p0, Lcom/google/android/youtube/core/utils/DockReceiver;->d:Z

    const-string v0, "context cannot be null"

    invoke-static {p1, v0}, Lcom/google/android/youtube/core/utils/s;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/Context;

    iput-object v0, p0, Lcom/google/android/youtube/core/utils/DockReceiver;->b:Landroid/content/Context;

    const-string v0, "listener cannot be null"

    invoke-static {p2, v0}, Lcom/google/android/youtube/core/utils/s;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/youtube/core/utils/k;

    iput-object v0, p0, Lcom/google/android/youtube/core/utils/DockReceiver;->c:Lcom/google/android/youtube/core/utils/k;

    iput v2, p0, Lcom/google/android/youtube/core/utils/DockReceiver;->e:I

    return-void
.end method


# virtual methods
.method public final a()V
    .locals 3

    const/4 v0, 0x0

    iget-boolean v1, p0, Lcom/google/android/youtube/core/utils/DockReceiver;->d:Z

    if-nez v1, :cond_1

    const/4 v1, 0x1

    iput-boolean v1, p0, Lcom/google/android/youtube/core/utils/DockReceiver;->d:Z

    iget-object v1, p0, Lcom/google/android/youtube/core/utils/DockReceiver;->b:Landroid/content/Context;

    iget-object v2, p0, Lcom/google/android/youtube/core/utils/DockReceiver;->a:Landroid/content/IntentFilter;

    invoke-virtual {v1, p0, v2}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    move-result-object v1

    if-eqz v1, :cond_0

    const-string v2, "android.intent.extra.DOCK_STATE"

    invoke-virtual {v1, v2, v0}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v0

    iput v0, p0, Lcom/google/android/youtube/core/utils/DockReceiver;->e:I

    :cond_0
    iput v0, p0, Lcom/google/android/youtube/core/utils/DockReceiver;->e:I

    iget-object v0, p0, Lcom/google/android/youtube/core/utils/DockReceiver;->c:Lcom/google/android/youtube/core/utils/k;

    iget v1, p0, Lcom/google/android/youtube/core/utils/DockReceiver;->e:I

    invoke-interface {v0}, Lcom/google/android/youtube/core/utils/k;->v()V

    :cond_1
    return-void
.end method

.method public final b()V
    .locals 2

    const/4 v1, 0x0

    iget-boolean v0, p0, Lcom/google/android/youtube/core/utils/DockReceiver;->d:Z

    if-eqz v0, :cond_0

    iput-boolean v1, p0, Lcom/google/android/youtube/core/utils/DockReceiver;->d:Z

    iget-object v0, p0, Lcom/google/android/youtube/core/utils/DockReceiver;->b:Landroid/content/Context;

    invoke-virtual {v0, p0}, Landroid/content/Context;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    iput v1, p0, Lcom/google/android/youtube/core/utils/DockReceiver;->e:I

    :cond_0
    return-void
.end method

.method public final c()I
    .locals 1

    iget v0, p0, Lcom/google/android/youtube/core/utils/DockReceiver;->e:I

    return v0
.end method

.method public final onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 2

    invoke-virtual {p0}, Lcom/google/android/youtube/core/utils/DockReceiver;->isInitialStickyBroadcast()Z

    const-string v0, "android.intent.action.DOCK_EVENT"

    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v0, "android.intent.extra.DOCK_STATE"

    const/4 v1, 0x0

    invoke-virtual {p2, v0, v1}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v0

    iput v0, p0, Lcom/google/android/youtube/core/utils/DockReceiver;->e:I

    iget-object v0, p0, Lcom/google/android/youtube/core/utils/DockReceiver;->c:Lcom/google/android/youtube/core/utils/k;

    iget v1, p0, Lcom/google/android/youtube/core/utils/DockReceiver;->e:I

    invoke-interface {v0}, Lcom/google/android/youtube/core/utils/k;->v()V

    :goto_0
    return-void

    :cond_0
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "unexpected intent: "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/youtube/core/L;->c(Ljava/lang/String;)V

    goto :goto_0
.end method
