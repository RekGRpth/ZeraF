.class final Lcom/google/android/youtube/core/client/ar;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/youtube/core/async/n;


# static fields
.field private static final a:Landroid/net/Uri;


# instance fields
.field private final b:Lcom/google/android/youtube/core/async/au;

.field private final c:Ljava/lang/String;

.field private final d:Ljava/lang/String;

.field private e:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const-string v0, "http://s2.youtube.com/s?ns=yt"

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    sput-object v0, Lcom/google/android/youtube/core/client/ar;->a:Landroid/net/Uri;

    return-void
.end method

.method public constructor <init>(Lcom/google/android/youtube/core/async/au;Ljava/lang/String;Ljava/lang/String;)V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/google/android/youtube/core/client/ar;->b:Lcom/google/android/youtube/core/async/au;

    iput-object p3, p0, Lcom/google/android/youtube/core/client/ar;->d:Ljava/lang/String;

    iput-object p2, p0, Lcom/google/android/youtube/core/client/ar;->c:Ljava/lang/String;

    const/4 v0, 0x0

    iput v0, p0, Lcom/google/android/youtube/core/client/ar;->e:I

    return-void
.end method

.method static synthetic a(Lcom/google/android/youtube/core/client/ar;)Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/core/client/ar;->d:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic b(Lcom/google/android/youtube/core/client/ar;)I
    .locals 1

    iget v0, p0, Lcom/google/android/youtube/core/client/ar;->e:I

    return v0
.end method


# virtual methods
.method public final a(I)V
    .locals 0

    iput p1, p0, Lcom/google/android/youtube/core/client/ar;->e:I

    return-void
.end method

.method public final a(J)V
    .locals 4

    iget-object v0, p0, Lcom/google/android/youtube/core/client/ar;->d:Ljava/lang/String;

    if-eqz v0, :cond_2

    iget v0, p0, Lcom/google/android/youtube/core/client/ar;->e:I

    if-lez v0, :cond_1

    iget v0, p0, Lcom/google/android/youtube/core/client/ar;->e:I

    const/4 v1, 0x2

    if-ge v0, v1, :cond_0

    const-wide/16 v0, 0x4e20

    cmp-long v0, p1, v0

    if-gez v0, :cond_1

    :cond_0
    iget v0, p0, Lcom/google/android/youtube/core/client/ar;->e:I

    const/4 v1, 0x3

    if-ge v0, v1, :cond_2

    const-wide/16 v0, 0x7530

    cmp-long v0, p1, v0

    if-ltz v0, :cond_2

    :cond_1
    iget v0, p0, Lcom/google/android/youtube/core/client/ar;->e:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/google/android/youtube/core/client/ar;->e:I

    invoke-static {p1, p2}, Lcom/google/android/youtube/core/client/aq;->c(J)Ljava/lang/String;

    move-result-object v0

    sget-object v1, Lcom/google/android/youtube/core/client/ar;->a:Landroid/net/Uri;

    invoke-virtual {v1}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v1

    const-string v2, "yttk"

    const-string v3, "1"

    invoke-virtual {v1, v2, v3}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v1

    const-string v2, "ps"

    const-string v3, "android"

    invoke-virtual {v1, v2, v3}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v1

    const-string v2, "ctp"

    iget v3, p0, Lcom/google/android/youtube/core/client/ar;->e:I

    invoke-static {v3}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v1

    const-string v2, "docid"

    iget-object v3, p0, Lcom/google/android/youtube/core/client/ar;->c:Ljava/lang/String;

    invoke-virtual {v1, v2, v3}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v1

    const-string v2, "plid"

    iget-object v3, p0, Lcom/google/android/youtube/core/client/ar;->d:Ljava/lang/String;

    invoke-virtual {v1, v2, v3}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v1

    const-string v2, "st"

    invoke-virtual {v1, v2, v0}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v1

    const-string v2, "et"

    invoke-virtual {v1, v2, v0}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    const-string v1, "el"

    const-string v2, "detailpage"

    invoke-virtual {v0, v1, v2}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Pinging "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    invoke-static {}, Lcom/google/android/youtube/core/L;->b()V

    iget-object v1, p0, Lcom/google/android/youtube/core/client/ar;->b:Lcom/google/android/youtube/core/async/au;

    invoke-static {v0}, Lcom/google/android/youtube/core/async/ap;->d(Landroid/net/Uri;)Lcom/google/android/youtube/core/async/ap;

    move-result-object v0

    invoke-interface {v1, v0, p0}, Lcom/google/android/youtube/core/async/au;->a(Ljava/lang/Object;Lcom/google/android/youtube/core/async/n;)V

    :cond_2
    return-void
.end method

.method public final bridge synthetic a(Ljava/lang/Object;Ljava/lang/Exception;)V
    .locals 0

    return-void
.end method

.method public final bridge synthetic a(Ljava/lang/Object;Ljava/lang/Object;)V
    .locals 0

    return-void
.end method
