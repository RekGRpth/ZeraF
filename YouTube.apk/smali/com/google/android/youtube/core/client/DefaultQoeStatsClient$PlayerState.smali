.class final enum Lcom/google/android/youtube/core/client/DefaultQoeStatsClient$PlayerState;
.super Ljava/lang/Enum;
.source "SourceFile"


# static fields
.field private static final synthetic $VALUES:[Lcom/google/android/youtube/core/client/DefaultQoeStatsClient$PlayerState;

.field public static final enum BUFFERING:Lcom/google/android/youtube/core/client/DefaultQoeStatsClient$PlayerState;

.field public static final enum ENDED:Lcom/google/android/youtube/core/client/DefaultQoeStatsClient$PlayerState;

.field public static final enum ERROR:Lcom/google/android/youtube/core/client/DefaultQoeStatsClient$PlayerState;

.field public static final enum NOT_PLAYING:Lcom/google/android/youtube/core/client/DefaultQoeStatsClient$PlayerState;

.field public static final enum NOT_VALID:Lcom/google/android/youtube/core/client/DefaultQoeStatsClient$PlayerState;

.field public static final enum PAUSED:Lcom/google/android/youtube/core/client/DefaultQoeStatsClient$PlayerState;

.field public static final enum PLAYING:Lcom/google/android/youtube/core/client/DefaultQoeStatsClient$PlayerState;

.field public static final enum SEEKING:Lcom/google/android/youtube/core/client/DefaultQoeStatsClient$PlayerState;


# instance fields
.field private final value:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 9

    const/4 v8, 0x4

    const/4 v7, 0x3

    const/4 v6, 0x2

    const/4 v5, 0x1

    const/4 v4, 0x0

    new-instance v0, Lcom/google/android/youtube/core/client/DefaultQoeStatsClient$PlayerState;

    const-string v1, "BUFFERING"

    const-string v2, "B"

    invoke-direct {v0, v1, v4, v2}, Lcom/google/android/youtube/core/client/DefaultQoeStatsClient$PlayerState;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/google/android/youtube/core/client/DefaultQoeStatsClient$PlayerState;->BUFFERING:Lcom/google/android/youtube/core/client/DefaultQoeStatsClient$PlayerState;

    new-instance v0, Lcom/google/android/youtube/core/client/DefaultQoeStatsClient$PlayerState;

    const-string v1, "ERROR"

    const-string v2, "ER"

    invoke-direct {v0, v1, v5, v2}, Lcom/google/android/youtube/core/client/DefaultQoeStatsClient$PlayerState;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/google/android/youtube/core/client/DefaultQoeStatsClient$PlayerState;->ERROR:Lcom/google/android/youtube/core/client/DefaultQoeStatsClient$PlayerState;

    new-instance v0, Lcom/google/android/youtube/core/client/DefaultQoeStatsClient$PlayerState;

    const-string v1, "ENDED"

    const-string v2, "EN"

    invoke-direct {v0, v1, v6, v2}, Lcom/google/android/youtube/core/client/DefaultQoeStatsClient$PlayerState;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/google/android/youtube/core/client/DefaultQoeStatsClient$PlayerState;->ENDED:Lcom/google/android/youtube/core/client/DefaultQoeStatsClient$PlayerState;

    new-instance v0, Lcom/google/android/youtube/core/client/DefaultQoeStatsClient$PlayerState;

    const-string v1, "NOT_PLAYING"

    const-string v2, "N"

    invoke-direct {v0, v1, v7, v2}, Lcom/google/android/youtube/core/client/DefaultQoeStatsClient$PlayerState;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/google/android/youtube/core/client/DefaultQoeStatsClient$PlayerState;->NOT_PLAYING:Lcom/google/android/youtube/core/client/DefaultQoeStatsClient$PlayerState;

    new-instance v0, Lcom/google/android/youtube/core/client/DefaultQoeStatsClient$PlayerState;

    const-string v1, "PAUSED"

    const-string v2, "PA"

    invoke-direct {v0, v1, v8, v2}, Lcom/google/android/youtube/core/client/DefaultQoeStatsClient$PlayerState;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/google/android/youtube/core/client/DefaultQoeStatsClient$PlayerState;->PAUSED:Lcom/google/android/youtube/core/client/DefaultQoeStatsClient$PlayerState;

    new-instance v0, Lcom/google/android/youtube/core/client/DefaultQoeStatsClient$PlayerState;

    const-string v1, "PLAYING"

    const/4 v2, 0x5

    const-string v3, "PL"

    invoke-direct {v0, v1, v2, v3}, Lcom/google/android/youtube/core/client/DefaultQoeStatsClient$PlayerState;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/google/android/youtube/core/client/DefaultQoeStatsClient$PlayerState;->PLAYING:Lcom/google/android/youtube/core/client/DefaultQoeStatsClient$PlayerState;

    new-instance v0, Lcom/google/android/youtube/core/client/DefaultQoeStatsClient$PlayerState;

    const-string v1, "SEEKING"

    const/4 v2, 0x6

    const-string v3, "S"

    invoke-direct {v0, v1, v2, v3}, Lcom/google/android/youtube/core/client/DefaultQoeStatsClient$PlayerState;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/google/android/youtube/core/client/DefaultQoeStatsClient$PlayerState;->SEEKING:Lcom/google/android/youtube/core/client/DefaultQoeStatsClient$PlayerState;

    new-instance v0, Lcom/google/android/youtube/core/client/DefaultQoeStatsClient$PlayerState;

    const-string v1, "NOT_VALID"

    const/4 v2, 0x7

    const-string v3, "X"

    invoke-direct {v0, v1, v2, v3}, Lcom/google/android/youtube/core/client/DefaultQoeStatsClient$PlayerState;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/google/android/youtube/core/client/DefaultQoeStatsClient$PlayerState;->NOT_VALID:Lcom/google/android/youtube/core/client/DefaultQoeStatsClient$PlayerState;

    const/16 v0, 0x8

    new-array v0, v0, [Lcom/google/android/youtube/core/client/DefaultQoeStatsClient$PlayerState;

    sget-object v1, Lcom/google/android/youtube/core/client/DefaultQoeStatsClient$PlayerState;->BUFFERING:Lcom/google/android/youtube/core/client/DefaultQoeStatsClient$PlayerState;

    aput-object v1, v0, v4

    sget-object v1, Lcom/google/android/youtube/core/client/DefaultQoeStatsClient$PlayerState;->ERROR:Lcom/google/android/youtube/core/client/DefaultQoeStatsClient$PlayerState;

    aput-object v1, v0, v5

    sget-object v1, Lcom/google/android/youtube/core/client/DefaultQoeStatsClient$PlayerState;->ENDED:Lcom/google/android/youtube/core/client/DefaultQoeStatsClient$PlayerState;

    aput-object v1, v0, v6

    sget-object v1, Lcom/google/android/youtube/core/client/DefaultQoeStatsClient$PlayerState;->NOT_PLAYING:Lcom/google/android/youtube/core/client/DefaultQoeStatsClient$PlayerState;

    aput-object v1, v0, v7

    sget-object v1, Lcom/google/android/youtube/core/client/DefaultQoeStatsClient$PlayerState;->PAUSED:Lcom/google/android/youtube/core/client/DefaultQoeStatsClient$PlayerState;

    aput-object v1, v0, v8

    const/4 v1, 0x5

    sget-object v2, Lcom/google/android/youtube/core/client/DefaultQoeStatsClient$PlayerState;->PLAYING:Lcom/google/android/youtube/core/client/DefaultQoeStatsClient$PlayerState;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, Lcom/google/android/youtube/core/client/DefaultQoeStatsClient$PlayerState;->SEEKING:Lcom/google/android/youtube/core/client/DefaultQoeStatsClient$PlayerState;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, Lcom/google/android/youtube/core/client/DefaultQoeStatsClient$PlayerState;->NOT_VALID:Lcom/google/android/youtube/core/client/DefaultQoeStatsClient$PlayerState;

    aput-object v2, v0, v1

    sput-object v0, Lcom/google/android/youtube/core/client/DefaultQoeStatsClient$PlayerState;->$VALUES:[Lcom/google/android/youtube/core/client/DefaultQoeStatsClient$PlayerState;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILjava/lang/String;)V
    .locals 0

    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    iput-object p3, p0, Lcom/google/android/youtube/core/client/DefaultQoeStatsClient$PlayerState;->value:Ljava/lang/String;

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/google/android/youtube/core/client/DefaultQoeStatsClient$PlayerState;
    .locals 1

    const-class v0, Lcom/google/android/youtube/core/client/DefaultQoeStatsClient$PlayerState;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/google/android/youtube/core/client/DefaultQoeStatsClient$PlayerState;

    return-object v0
.end method

.method public static values()[Lcom/google/android/youtube/core/client/DefaultQoeStatsClient$PlayerState;
    .locals 1

    sget-object v0, Lcom/google/android/youtube/core/client/DefaultQoeStatsClient$PlayerState;->$VALUES:[Lcom/google/android/youtube/core/client/DefaultQoeStatsClient$PlayerState;

    invoke-virtual {v0}, [Lcom/google/android/youtube/core/client/DefaultQoeStatsClient$PlayerState;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/google/android/youtube/core/client/DefaultQoeStatsClient$PlayerState;

    return-object v0
.end method


# virtual methods
.method public final toString()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/core/client/DefaultQoeStatsClient$PlayerState;->value:Ljava/lang/String;

    return-object v0
.end method
