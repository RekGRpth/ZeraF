.class public abstract Lcom/google/android/youtube/core/client/h;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field protected final b:Ljava/util/concurrent/Executor;

.field protected final c:Lorg/apache/http/client/HttpClient;

.field protected final d:Lcom/google/android/youtube/core/utils/e;

.field protected final e:Ljava/lang/String;

.field protected final f:Lcom/google/android/youtube/core/converter/http/ec;

.field protected final g:Lcom/google/android/youtube/core/converter/m;


# direct methods
.method public constructor <init>(Ljava/util/concurrent/Executor;Lorg/apache/http/client/HttpClient;)V
    .locals 3

    const/4 v2, 0x0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const-string v0, "executor can\'t be null"

    invoke-static {p1, v0}, Lcom/google/android/youtube/core/utils/s;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/concurrent/Executor;

    iput-object v0, p0, Lcom/google/android/youtube/core/client/h;->b:Ljava/util/concurrent/Executor;

    const-string v0, "httpClient can\'t be null"

    invoke-static {p2, v0}, Lcom/google/android/youtube/core/utils/s;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/apache/http/client/HttpClient;

    iput-object v0, p0, Lcom/google/android/youtube/core/client/h;->c:Lorg/apache/http/client/HttpClient;

    new-instance v0, Lcom/google/android/youtube/core/converter/http/ec;

    sget-object v1, Lcom/google/android/youtube/core/converter/http/HttpMethod;->GET:Lcom/google/android/youtube/core/converter/http/HttpMethod;

    invoke-direct {v0, v1}, Lcom/google/android/youtube/core/converter/http/ec;-><init>(Lcom/google/android/youtube/core/converter/http/HttpMethod;)V

    iput-object v0, p0, Lcom/google/android/youtube/core/client/h;->f:Lcom/google/android/youtube/core/converter/http/ec;

    iput-object v2, p0, Lcom/google/android/youtube/core/client/h;->g:Lcom/google/android/youtube/core/converter/m;

    iput-object v2, p0, Lcom/google/android/youtube/core/client/h;->e:Ljava/lang/String;

    iput-object v2, p0, Lcom/google/android/youtube/core/client/h;->d:Lcom/google/android/youtube/core/utils/e;

    return-void
.end method

.method public constructor <init>(Ljava/util/concurrent/Executor;Lorg/apache/http/client/HttpClient;Lcom/google/android/youtube/core/converter/m;Lcom/google/android/youtube/core/utils/e;)V
    .locals 2

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const-string v0, "executor can\'t be null"

    invoke-static {p1, v0}, Lcom/google/android/youtube/core/utils/s;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/concurrent/Executor;

    iput-object v0, p0, Lcom/google/android/youtube/core/client/h;->b:Ljava/util/concurrent/Executor;

    const-string v0, "httpClient can\'t be null"

    invoke-static {p2, v0}, Lcom/google/android/youtube/core/utils/s;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/apache/http/client/HttpClient;

    iput-object v0, p0, Lcom/google/android/youtube/core/client/h;->c:Lorg/apache/http/client/HttpClient;

    const-string v0, "xmlParser cannot be null"

    invoke-static {p3, v0}, Lcom/google/android/youtube/core/utils/s;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/youtube/core/converter/m;

    iput-object v0, p0, Lcom/google/android/youtube/core/client/h;->g:Lcom/google/android/youtube/core/converter/m;

    const-string v0, "clock cannot be null"

    invoke-static {p4, v0}, Lcom/google/android/youtube/core/utils/s;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/youtube/core/utils/e;

    iput-object v0, p0, Lcom/google/android/youtube/core/client/h;->d:Lcom/google/android/youtube/core/utils/e;

    new-instance v0, Lcom/google/android/youtube/core/converter/http/ec;

    sget-object v1, Lcom/google/android/youtube/core/converter/http/HttpMethod;->GET:Lcom/google/android/youtube/core/converter/http/HttpMethod;

    invoke-direct {v0, v1}, Lcom/google/android/youtube/core/converter/http/ec;-><init>(Lcom/google/android/youtube/core/converter/http/HttpMethod;)V

    iput-object v0, p0, Lcom/google/android/youtube/core/client/h;->f:Lcom/google/android/youtube/core/converter/http/ec;

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/youtube/core/client/h;->e:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Ljava/util/concurrent/Executor;Lorg/apache/http/client/HttpClient;Lcom/google/android/youtube/core/converter/m;Ljava/lang/String;Lcom/google/android/youtube/core/utils/e;)V
    .locals 2

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const-string v0, "executor can\'t be null"

    invoke-static {p1, v0}, Lcom/google/android/youtube/core/utils/s;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/concurrent/Executor;

    iput-object v0, p0, Lcom/google/android/youtube/core/client/h;->b:Ljava/util/concurrent/Executor;

    const-string v0, "httpClient can\'t be null"

    invoke-static {p2, v0}, Lcom/google/android/youtube/core/utils/s;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/apache/http/client/HttpClient;

    iput-object v0, p0, Lcom/google/android/youtube/core/client/h;->c:Lorg/apache/http/client/HttpClient;

    const-string v0, "xmlParser can\'t be null"

    invoke-static {p3, v0}, Lcom/google/android/youtube/core/utils/s;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/youtube/core/converter/m;

    iput-object v0, p0, Lcom/google/android/youtube/core/client/h;->g:Lcom/google/android/youtube/core/converter/m;

    const-string v0, "cachePath can\'t be null"

    invoke-static {p4, v0}, Lcom/google/android/youtube/core/utils/s;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iput-object v0, p0, Lcom/google/android/youtube/core/client/h;->e:Ljava/lang/String;

    const-string v0, "clock can\'t be null"

    invoke-static {p5, v0}, Lcom/google/android/youtube/core/utils/s;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/youtube/core/utils/e;

    iput-object v0, p0, Lcom/google/android/youtube/core/client/h;->d:Lcom/google/android/youtube/core/utils/e;

    new-instance v0, Lcom/google/android/youtube/core/converter/http/ec;

    sget-object v1, Lcom/google/android/youtube/core/converter/http/HttpMethod;->GET:Lcom/google/android/youtube/core/converter/http/HttpMethod;

    invoke-direct {v0, v1}, Lcom/google/android/youtube/core/converter/http/ec;-><init>(Lcom/google/android/youtube/core/converter/http/HttpMethod;)V

    iput-object v0, p0, Lcom/google/android/youtube/core/client/h;->f:Lcom/google/android/youtube/core/converter/http/ec;

    return-void
.end method

.method public constructor <init>(Ljava/util/concurrent/Executor;Lorg/apache/http/client/HttpClient;Lcom/google/android/youtube/core/utils/e;)V
    .locals 3

    const/4 v2, 0x0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const-string v0, "executor can\'t be null"

    invoke-static {p1, v0}, Lcom/google/android/youtube/core/utils/s;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/concurrent/Executor;

    iput-object v0, p0, Lcom/google/android/youtube/core/client/h;->b:Ljava/util/concurrent/Executor;

    const-string v0, "httpClient can\'t be null"

    invoke-static {p2, v0}, Lcom/google/android/youtube/core/utils/s;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/apache/http/client/HttpClient;

    iput-object v0, p0, Lcom/google/android/youtube/core/client/h;->c:Lorg/apache/http/client/HttpClient;

    const-string v0, "clock can\'t be null"

    invoke-static {p3, v0}, Lcom/google/android/youtube/core/utils/s;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/youtube/core/utils/e;

    iput-object v0, p0, Lcom/google/android/youtube/core/client/h;->d:Lcom/google/android/youtube/core/utils/e;

    new-instance v0, Lcom/google/android/youtube/core/converter/http/ec;

    sget-object v1, Lcom/google/android/youtube/core/converter/http/HttpMethod;->GET:Lcom/google/android/youtube/core/converter/http/HttpMethod;

    invoke-direct {v0, v1}, Lcom/google/android/youtube/core/converter/http/ec;-><init>(Lcom/google/android/youtube/core/converter/http/HttpMethod;)V

    iput-object v0, p0, Lcom/google/android/youtube/core/client/h;->f:Lcom/google/android/youtube/core/converter/http/ec;

    iput-object v2, p0, Lcom/google/android/youtube/core/client/h;->g:Lcom/google/android/youtube/core/converter/m;

    iput-object v2, p0, Lcom/google/android/youtube/core/client/h;->e:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Ljava/util/concurrent/Executor;Lorg/apache/http/client/HttpClient;Ljava/lang/String;Lcom/google/android/youtube/core/utils/e;)V
    .locals 2

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const-string v0, "executor can\'t be null"

    invoke-static {p1, v0}, Lcom/google/android/youtube/core/utils/s;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/concurrent/Executor;

    iput-object v0, p0, Lcom/google/android/youtube/core/client/h;->b:Ljava/util/concurrent/Executor;

    const-string v0, "httpClient can\'t be null"

    invoke-static {p2, v0}, Lcom/google/android/youtube/core/utils/s;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/apache/http/client/HttpClient;

    iput-object v0, p0, Lcom/google/android/youtube/core/client/h;->c:Lorg/apache/http/client/HttpClient;

    const-string v0, "clock can\'t be null"

    invoke-static {p4, v0}, Lcom/google/android/youtube/core/utils/s;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/youtube/core/utils/e;

    iput-object v0, p0, Lcom/google/android/youtube/core/client/h;->d:Lcom/google/android/youtube/core/utils/e;

    iput-object p3, p0, Lcom/google/android/youtube/core/client/h;->e:Ljava/lang/String;

    new-instance v0, Lcom/google/android/youtube/core/converter/http/ec;

    sget-object v1, Lcom/google/android/youtube/core/converter/http/HttpMethod;->GET:Lcom/google/android/youtube/core/converter/http/HttpMethod;

    invoke-direct {v0, v1}, Lcom/google/android/youtube/core/converter/http/ec;-><init>(Lcom/google/android/youtube/core/converter/http/HttpMethod;)V

    iput-object v0, p0, Lcom/google/android/youtube/core/client/h;->f:Lcom/google/android/youtube/core/converter/http/ec;

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/youtube/core/client/h;->g:Lcom/google/android/youtube/core/converter/m;

    return-void
.end method

.method protected static a(Lcom/google/android/youtube/core/utils/t;Lcom/google/android/youtube/core/cache/a;Lcom/google/android/youtube/core/async/au;)Lcom/google/android/youtube/core/async/y;
    .locals 1

    new-instance v0, Lcom/google/android/youtube/core/async/y;

    invoke-direct {v0, p0, p1, p2}, Lcom/google/android/youtube/core/async/y;-><init>(Lcom/google/android/youtube/core/utils/t;Lcom/google/android/youtube/core/cache/a;Lcom/google/android/youtube/core/async/au;)V

    return-object v0
.end method

.method protected static a(I)Lcom/google/android/youtube/core/cache/b;
    .locals 1

    new-instance v0, Lcom/google/android/youtube/core/cache/b;

    invoke-direct {v0, p0}, Lcom/google/android/youtube/core/cache/b;-><init>(I)V

    return-object v0
.end method

.method protected static b(I)Lcom/google/android/youtube/core/cache/j;
    .locals 1

    new-instance v0, Lcom/google/android/youtube/core/cache/j;

    invoke-direct {v0, p0}, Lcom/google/android/youtube/core/cache/j;-><init>(I)V

    return-object v0
.end method


# virtual methods
.method protected final a(Lcom/google/android/youtube/core/converter/b;Lcom/google/android/youtube/core/converter/http/bn;)Lcom/google/android/youtube/core/async/an;
    .locals 2

    new-instance v0, Lcom/google/android/youtube/core/async/an;

    iget-object v1, p0, Lcom/google/android/youtube/core/client/h;->c:Lorg/apache/http/client/HttpClient;

    invoke-direct {v0, v1, p1, p2}, Lcom/google/android/youtube/core/async/an;-><init>(Lorg/apache/http/client/HttpClient;Lcom/google/android/youtube/core/converter/b;Lcom/google/android/youtube/core/converter/http/bn;)V

    return-object v0
.end method

.method protected final a(Lcom/google/android/youtube/core/cache/a;Lcom/google/android/youtube/core/async/au;J)Lcom/google/android/youtube/core/async/bg;
    .locals 2

    iget-object v0, p0, Lcom/google/android/youtube/core/client/h;->d:Lcom/google/android/youtube/core/utils/e;

    const-string v1, "this instance does not contain a clock"

    invoke-static {v0, v1}, Lcom/google/android/youtube/core/utils/s;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v0, p0, Lcom/google/android/youtube/core/client/h;->d:Lcom/google/android/youtube/core/utils/e;

    invoke-static {p1, p2, v0, p3, p4}, Lcom/google/android/youtube/core/async/bg;->a(Lcom/google/android/youtube/core/cache/a;Lcom/google/android/youtube/core/async/au;Lcom/google/android/youtube/core/utils/e;J)Lcom/google/android/youtube/core/async/bg;

    move-result-object v0

    return-object v0
.end method

.method protected final a(Lcom/google/android/youtube/core/async/au;)Lcom/google/android/youtube/core/async/i;
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/core/client/h;->b:Ljava/util/concurrent/Executor;

    invoke-static {v0, p1}, Lcom/google/android/youtube/core/async/i;->a(Ljava/util/concurrent/Executor;Lcom/google/android/youtube/core/async/au;)Lcom/google/android/youtube/core/async/i;

    move-result-object v0

    return-object v0
.end method

.method protected final c()V
    .locals 4

    iget-object v0, p0, Lcom/google/android/youtube/core/client/h;->e:Ljava/lang/String;

    const-string v1, "this instance does not support persistent caching"

    invoke-static {v0, v1}, Lcom/google/android/youtube/core/utils/s;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v0, p0, Lcom/google/android/youtube/core/client/h;->b:Ljava/util/concurrent/Executor;

    iget-object v1, p0, Lcom/google/android/youtube/core/client/h;->e:Ljava/lang/String;

    const-wide/32 v2, 0x1400000

    invoke-static {v0, v1, v2, v3}, Lcom/google/android/youtube/core/cache/c;->a(Ljava/util/concurrent/Executor;Ljava/lang/String;J)V

    return-void
.end method

.method protected final d()Lcom/google/android/youtube/core/cache/c;
    .locals 2

    iget-object v0, p0, Lcom/google/android/youtube/core/client/h;->e:Ljava/lang/String;

    const-string v1, "this instance does not support persistent caching"

    invoke-static {v0, v1}, Lcom/google/android/youtube/core/utils/s;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    new-instance v0, Lcom/google/android/youtube/core/cache/i;

    iget-object v1, p0, Lcom/google/android/youtube/core/client/h;->e:Ljava/lang/String;

    invoke-direct {v0, v1}, Lcom/google/android/youtube/core/cache/i;-><init>(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/google/android/youtube/core/client/h;->b:Ljava/util/concurrent/Executor;

    invoke-virtual {v0, v1}, Lcom/google/android/youtube/core/cache/i;->a(Ljava/util/concurrent/Executor;)Lcom/google/android/youtube/core/cache/c;

    move-result-object v0

    return-object v0
.end method
