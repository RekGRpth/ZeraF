.class public final Lcom/google/android/youtube/core/client/k;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/youtube/core/client/b;


# instance fields
.field private final a:Lcom/google/android/youtube/core/async/au;


# direct methods
.method private constructor <init>(Lcom/google/android/youtube/core/async/au;)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/google/android/youtube/core/client/k;->a:Lcom/google/android/youtube/core/async/au;

    return-void
.end method

.method public constructor <init>(Ljava/util/concurrent/Executor;Lorg/apache/http/client/HttpClient;)V
    .locals 3

    new-instance v0, Lcom/google/android/youtube/core/async/an;

    sget-object v1, Lcom/google/android/youtube/core/converter/http/ec;->a:Lcom/google/android/youtube/core/converter/http/ec;

    sget-object v2, Lcom/google/android/youtube/core/converter/http/bn;->b:Lcom/google/android/youtube/core/converter/http/bn;

    invoke-direct {v0, p2, v1, v2}, Lcom/google/android/youtube/core/async/an;-><init>(Lorg/apache/http/client/HttpClient;Lcom/google/android/youtube/core/converter/b;Lcom/google/android/youtube/core/converter/http/bn;)V

    invoke-static {p1, v0}, Lcom/google/android/youtube/core/async/i;->a(Ljava/util/concurrent/Executor;Lcom/google/android/youtube/core/async/au;)Lcom/google/android/youtube/core/async/i;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/google/android/youtube/core/client/k;-><init>(Lcom/google/android/youtube/core/async/au;)V

    return-void
.end method


# virtual methods
.method public final a(Lcom/google/android/youtube/core/model/VastAd;)Lcom/google/android/youtube/core/client/AdStatsClient;
    .locals 3

    new-instance v0, Lcom/google/android/youtube/core/client/j;

    iget-object v1, p0, Lcom/google/android/youtube/core/client/k;->a:Lcom/google/android/youtube/core/async/au;

    const/4 v2, 0x0

    invoke-direct {v0, v1, p1, v2}, Lcom/google/android/youtube/core/client/j;-><init>(Lcom/google/android/youtube/core/async/au;Lcom/google/android/youtube/core/model/VastAd;B)V

    return-object v0
.end method

.method public final a(Lcom/google/android/youtube/core/model/VastAd;Lcom/google/android/youtube/core/client/AdStatsClient$AdStatsClientState;)Lcom/google/android/youtube/core/client/AdStatsClient;
    .locals 7

    new-instance v0, Lcom/google/android/youtube/core/client/j;

    iget-object v1, p0, Lcom/google/android/youtube/core/client/k;->a:Lcom/google/android/youtube/core/async/au;

    iget v3, p2, Lcom/google/android/youtube/core/client/AdStatsClient$AdStatsClientState;->nextQuartile:I

    iget-boolean v4, p2, Lcom/google/android/youtube/core/client/AdStatsClient$AdStatsClientState;->engagedViewPinged:Z

    iget-boolean v5, p2, Lcom/google/android/youtube/core/client/AdStatsClient$AdStatsClientState;->impressionPinged:Z

    iget-boolean v6, p2, Lcom/google/android/youtube/core/client/AdStatsClient$AdStatsClientState;->skipAdShownPinged:Z

    move-object v2, p1

    invoke-direct/range {v0 .. v6}, Lcom/google/android/youtube/core/client/j;-><init>(Lcom/google/android/youtube/core/async/au;Lcom/google/android/youtube/core/model/VastAd;IZZZ)V

    return-object v0
.end method
