.class public final Lcom/google/android/youtube/core/client/u;
.super Lcom/google/android/youtube/core/client/h;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/youtube/core/client/as;


# instance fields
.field private final a:Lcom/google/android/youtube/core/async/au;

.field private final h:Lcom/google/android/youtube/core/utils/aj;

.field private final i:Ljava/lang/String;

.field private final j:Ljava/lang/String;


# direct methods
.method public constructor <init>(Ljava/util/concurrent/Executor;Lorg/apache/http/client/HttpClient;Lcom/google/android/youtube/core/utils/aj;[B[BLjava/lang/String;)V
    .locals 2

    invoke-direct {p0, p1, p2}, Lcom/google/android/youtube/core/client/h;-><init>(Ljava/util/concurrent/Executor;Lorg/apache/http/client/HttpClient;)V

    :try_start_0
    new-instance v0, Ljava/lang/String;

    const-string v1, "UTF-8"

    invoke-direct {v0, p4, v1}, Ljava/lang/String;-><init>([BLjava/lang/String;)V

    const-string v1, "developerKey cannot be null or empty"

    invoke-static {v0, v1}, Lcom/google/android/youtube/core/utils/s;->a(Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/youtube/core/client/u;->i:Ljava/lang/String;
    :try_end_0
    .catch Ljava/io/UnsupportedEncodingException; {:try_start_0 .. :try_end_0} :catch_0

    const-string v0, "serial cannot be null or empty"

    invoke-static {p6, v0}, Lcom/google/android/youtube/core/utils/s;->a(Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/youtube/core/client/u;->j:Ljava/lang/String;

    iput-object p3, p0, Lcom/google/android/youtube/core/client/u;->h:Lcom/google/android/youtube/core/utils/aj;

    new-instance v0, Lcom/google/android/youtube/core/converter/http/ec;

    sget-object v1, Lcom/google/android/youtube/core/converter/http/HttpMethod;->POST:Lcom/google/android/youtube/core/converter/http/HttpMethod;

    invoke-direct {v0, v1}, Lcom/google/android/youtube/core/converter/http/ec;-><init>(Lcom/google/android/youtube/core/converter/http/HttpMethod;)V

    new-instance v1, Lcom/google/android/youtube/core/converter/http/aq;

    invoke-direct {v1, p5}, Lcom/google/android/youtube/core/converter/http/aq;-><init>([B)V

    invoke-virtual {p0, v0, v1}, Lcom/google/android/youtube/core/client/u;->a(Lcom/google/android/youtube/core/converter/b;Lcom/google/android/youtube/core/converter/http/bn;)Lcom/google/android/youtube/core/async/an;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/youtube/core/client/u;->a:Lcom/google/android/youtube/core/async/au;

    return-void

    :catch_0
    move-exception v0

    new-instance v1, Ljava/lang/RuntimeException;

    invoke-direct {v1, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/Throwable;)V

    throw v1
.end method


# virtual methods
.method public final a(Lcom/google/android/youtube/core/async/n;)V
    .locals 4

    iget-object v0, p0, Lcom/google/android/youtube/core/client/u;->h:Lcom/google/android/youtube/core/utils/aj;

    if-nez v0, :cond_0

    const-string v0, "https://www.google.com/youtube/accounts/registerDevice"

    :goto_0
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "?developer=%s&serialNumber=%s"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x2

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    iget-object v3, p0, Lcom/google/android/youtube/core/client/u;->i:Ljava/lang/String;

    aput-object v3, v1, v2

    const/4 v2, 0x1

    iget-object v3, p0, Lcom/google/android/youtube/core/client/u;->j:Ljava/lang/String;

    aput-object v3, v1, v2

    invoke-static {v0, v1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/youtube/core/client/u;->a:Lcom/google/android/youtube/core/async/au;

    invoke-interface {v1, v0, p1}, Lcom/google/android/youtube/core/async/au;->a(Ljava/lang/Object;Lcom/google/android/youtube/core/async/n;)V

    return-void

    :cond_0
    iget-object v0, p0, Lcom/google/android/youtube/core/client/u;->h:Lcom/google/android/youtube/core/utils/aj;

    const-string v1, "https://www.google.com/youtube/accounts/registerDevice"

    invoke-virtual {v0, v1}, Lcom/google/android/youtube/core/utils/aj;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method
