.class public final Lcom/google/android/youtube/core/client/ap;
.super Lcom/google/android/youtube/core/client/h;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/youtube/core/client/bl;


# instance fields
.field private final a:Lcom/google/android/youtube/core/async/au;

.field private final h:Lcom/google/android/youtube/core/async/au;


# direct methods
.method public constructor <init>(Ljava/util/concurrent/Executor;Lorg/apache/http/client/HttpClient;Lcom/google/android/youtube/core/converter/m;Lcom/google/android/youtube/core/utils/e;)V
    .locals 1

    invoke-direct {p0, p1, p2, p3, p4}, Lcom/google/android/youtube/core/client/h;-><init>(Ljava/util/concurrent/Executor;Lorg/apache/http/client/HttpClient;Lcom/google/android/youtube/core/converter/m;Lcom/google/android/youtube/core/utils/e;)V

    invoke-direct {p0}, Lcom/google/android/youtube/core/client/ap;->a()Lcom/google/android/youtube/core/async/au;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/youtube/core/client/ap;->a:Lcom/google/android/youtube/core/async/au;

    invoke-direct {p0}, Lcom/google/android/youtube/core/client/ap;->b()Lcom/google/android/youtube/core/async/au;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/youtube/core/client/ap;->h:Lcom/google/android/youtube/core/async/au;

    return-void
.end method

.method public constructor <init>(Ljava/util/concurrent/Executor;Lorg/apache/http/client/HttpClient;Lcom/google/android/youtube/core/converter/m;Ljava/lang/String;Lcom/google/android/youtube/core/utils/e;)V
    .locals 1

    invoke-direct/range {p0 .. p5}, Lcom/google/android/youtube/core/client/h;-><init>(Ljava/util/concurrent/Executor;Lorg/apache/http/client/HttpClient;Lcom/google/android/youtube/core/converter/m;Ljava/lang/String;Lcom/google/android/youtube/core/utils/e;)V

    invoke-direct {p0}, Lcom/google/android/youtube/core/client/ap;->a()Lcom/google/android/youtube/core/async/au;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/youtube/core/client/ap;->a:Lcom/google/android/youtube/core/async/au;

    invoke-direct {p0}, Lcom/google/android/youtube/core/client/ap;->b()Lcom/google/android/youtube/core/async/au;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/youtube/core/client/ap;->h:Lcom/google/android/youtube/core/async/au;

    return-void
.end method

.method private a()Lcom/google/android/youtube/core/async/au;
    .locals 5

    new-instance v0, Lcom/google/android/youtube/core/converter/http/dt;

    invoke-direct {v0}, Lcom/google/android/youtube/core/converter/http/dt;-><init>()V

    const/16 v1, 0x14

    invoke-static {v1}, Lcom/google/android/youtube/core/client/ap;->a(I)Lcom/google/android/youtube/core/cache/b;

    move-result-object v1

    invoke-virtual {p0, v0, v0}, Lcom/google/android/youtube/core/client/ap;->a(Lcom/google/android/youtube/core/converter/b;Lcom/google/android/youtube/core/converter/http/bn;)Lcom/google/android/youtube/core/async/an;

    move-result-object v0

    iget-object v2, p0, Lcom/google/android/youtube/core/client/ap;->e:Ljava/lang/String;

    if-eqz v2, :cond_0

    invoke-virtual {p0}, Lcom/google/android/youtube/core/client/ap;->d()Lcom/google/android/youtube/core/cache/c;

    move-result-object v2

    const-wide/32 v3, 0x240c8400

    invoke-virtual {p0, v2, v0, v3, v4}, Lcom/google/android/youtube/core/client/ap;->a(Lcom/google/android/youtube/core/cache/a;Lcom/google/android/youtube/core/async/au;J)Lcom/google/android/youtube/core/async/bg;

    move-result-object v0

    :cond_0
    invoke-virtual {p0, v0}, Lcom/google/android/youtube/core/client/ap;->a(Lcom/google/android/youtube/core/async/au;)Lcom/google/android/youtube/core/async/i;

    move-result-object v0

    const-wide/32 v2, 0x6ddd00

    invoke-virtual {p0, v1, v0, v2, v3}, Lcom/google/android/youtube/core/client/ap;->a(Lcom/google/android/youtube/core/cache/a;Lcom/google/android/youtube/core/async/au;J)Lcom/google/android/youtube/core/async/bg;

    move-result-object v0

    return-object v0
.end method

.method private b()Lcom/google/android/youtube/core/async/au;
    .locals 5

    new-instance v0, Lcom/google/android/youtube/core/converter/http/dv;

    iget-object v1, p0, Lcom/google/android/youtube/core/client/ap;->g:Lcom/google/android/youtube/core/converter/m;

    invoke-direct {v0, v1}, Lcom/google/android/youtube/core/converter/http/dv;-><init>(Lcom/google/android/youtube/core/converter/m;)V

    const/16 v1, 0x14

    invoke-static {v1}, Lcom/google/android/youtube/core/client/ap;->a(I)Lcom/google/android/youtube/core/cache/b;

    move-result-object v1

    invoke-virtual {p0, v0, v0}, Lcom/google/android/youtube/core/client/ap;->a(Lcom/google/android/youtube/core/converter/b;Lcom/google/android/youtube/core/converter/http/bn;)Lcom/google/android/youtube/core/async/an;

    move-result-object v0

    iget-object v2, p0, Lcom/google/android/youtube/core/client/ap;->e:Ljava/lang/String;

    if-eqz v2, :cond_0

    invoke-virtual {p0}, Lcom/google/android/youtube/core/client/ap;->d()Lcom/google/android/youtube/core/cache/c;

    move-result-object v2

    const-wide/32 v3, 0x240c8400

    invoke-virtual {p0, v2, v0, v3, v4}, Lcom/google/android/youtube/core/client/ap;->a(Lcom/google/android/youtube/core/cache/a;Lcom/google/android/youtube/core/async/au;J)Lcom/google/android/youtube/core/async/bg;

    move-result-object v0

    :cond_0
    invoke-virtual {p0, v0}, Lcom/google/android/youtube/core/client/ap;->a(Lcom/google/android/youtube/core/async/au;)Lcom/google/android/youtube/core/async/i;

    move-result-object v0

    const-wide/32 v2, 0x6ddd00

    invoke-virtual {p0, v1, v0, v2, v3}, Lcom/google/android/youtube/core/client/ap;->a(Lcom/google/android/youtube/core/cache/a;Lcom/google/android/youtube/core/async/au;J)Lcom/google/android/youtube/core/async/bg;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public final a(Lcom/google/android/youtube/core/model/SubtitleTrack;Lcom/google/android/youtube/core/async/n;)V
    .locals 2

    iget-object v0, p1, Lcom/google/android/youtube/core/model/SubtitleTrack;->videoId:Ljava/lang/String;

    const-string v1, "subtitleTrack must have videoId set"

    invoke-static {v0, v1}, Lcom/google/android/youtube/core/utils/s;->a(Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/String;

    iget-object v0, p0, Lcom/google/android/youtube/core/client/ap;->h:Lcom/google/android/youtube/core/async/au;

    invoke-interface {v0, p1, p2}, Lcom/google/android/youtube/core/async/au;->a(Ljava/lang/Object;Lcom/google/android/youtube/core/async/n;)V

    return-void
.end method

.method public final a(Ljava/lang/String;Lcom/google/android/youtube/core/async/n;)V
    .locals 2

    const-string v0, "videoId cannot be empty"

    invoke-static {p1, v0}, Lcom/google/android/youtube/core/utils/s;->a(Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/String;

    new-instance v0, Lcom/google/android/youtube/core/converter/http/du;

    invoke-direct {v0, p2}, Lcom/google/android/youtube/core/converter/http/du;-><init>(Lcom/google/android/youtube/core/async/n;)V

    iget-object v1, p0, Lcom/google/android/youtube/core/client/ap;->a:Lcom/google/android/youtube/core/async/au;

    invoke-interface {v1, p1, v0}, Lcom/google/android/youtube/core/async/au;->a(Ljava/lang/Object;Lcom/google/android/youtube/core/async/n;)V

    return-void
.end method
