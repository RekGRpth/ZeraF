.class public abstract Lcom/google/android/youtube/core/ui/AbstractWorkspace;
.super Landroid/view/ViewGroup;
.source "SourceFile"


# instance fields
.field a:Ljava/lang/Runnable;

.field private b:I

.field private c:Z

.field private d:I

.field private e:I

.field private f:Landroid/widget/Scroller;

.field private g:Landroid/view/VelocityTracker;

.field private h:F

.field private i:F

.field private j:F

.field private k:I

.field private l:Z

.field private m:I

.field private n:I

.field private o:I

.field private p:I

.field private q:Landroid/graphics/drawable/Drawable;

.field private r:Lcom/google/android/youtube/core/ui/b;

.field private s:Lcom/google/android/youtube/core/ui/AbstractWorkspace$InteractionMode;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 4

    const/4 v1, 0x1

    const/4 v0, -0x1

    const/4 v3, 0x0

    invoke-direct {p0, p1, p2}, Landroid/view/ViewGroup;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    iput-boolean v1, p0, Lcom/google/android/youtube/core/ui/AbstractWorkspace;->c:Z

    iput v0, p0, Lcom/google/android/youtube/core/ui/AbstractWorkspace;->e:I

    iput v3, p0, Lcom/google/android/youtube/core/ui/AbstractWorkspace;->k:I

    iput-boolean v1, p0, Lcom/google/android/youtube/core/ui/AbstractWorkspace;->l:Z

    iput v0, p0, Lcom/google/android/youtube/core/ui/AbstractWorkspace;->p:I

    new-instance v0, Lcom/google/android/youtube/core/ui/a;

    invoke-direct {v0, p0}, Lcom/google/android/youtube/core/ui/a;-><init>(Lcom/google/android/youtube/core/ui/AbstractWorkspace;)V

    iput-object v0, p0, Lcom/google/android/youtube/core/ui/AbstractWorkspace;->a:Ljava/lang/Runnable;

    iput v3, p0, Lcom/google/android/youtube/core/ui/AbstractWorkspace;->b:I

    invoke-virtual {p0, v3}, Lcom/google/android/youtube/core/ui/AbstractWorkspace;->setHapticFeedbackEnabled(Z)V

    invoke-virtual {p0, v3}, Lcom/google/android/youtube/core/ui/AbstractWorkspace;->setHorizontalFadingEdgeEnabled(Z)V

    sget-object v0, Lcom/google/android/youtube/c;->l:[I

    const v1, 0x7f0d0006

    invoke-virtual {p1, p2, v0, v3, v1}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[III)Landroid/content/res/TypedArray;

    move-result-object v0

    invoke-static {}, Lcom/google/android/youtube/core/ui/AbstractWorkspace$InteractionMode;->values()[Lcom/google/android/youtube/core/ui/AbstractWorkspace$InteractionMode;

    move-result-object v1

    const/4 v2, 0x2

    invoke-virtual {v0, v3, v2}, Landroid/content/res/TypedArray;->getInt(II)I

    move-result v2

    aget-object v1, v1, v2

    invoke-virtual {p0, v1}, Lcom/google/android/youtube/core/ui/AbstractWorkspace;->setInteractionMode(Lcom/google/android/youtube/core/ui/AbstractWorkspace$InteractionMode;)V

    invoke-virtual {v0}, Landroid/content/res/TypedArray;->recycle()V

    new-instance v0, Landroid/widget/Scroller;

    invoke-virtual {p0}, Lcom/google/android/youtube/core/ui/AbstractWorkspace;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/widget/Scroller;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/google/android/youtube/core/ui/AbstractWorkspace;->f:Landroid/widget/Scroller;

    iget v0, p0, Lcom/google/android/youtube/core/ui/AbstractWorkspace;->b:I

    iput v0, p0, Lcom/google/android/youtube/core/ui/AbstractWorkspace;->d:I

    invoke-virtual {p0}, Lcom/google/android/youtube/core/ui/AbstractWorkspace;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Landroid/view/ViewConfiguration;->get(Landroid/content/Context;)Landroid/view/ViewConfiguration;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/ViewConfiguration;->getScaledTouchSlop()I

    move-result v1

    iput v1, p0, Lcom/google/android/youtube/core/ui/AbstractWorkspace;->m:I

    const/16 v1, 0x32

    iput v1, p0, Lcom/google/android/youtube/core/ui/AbstractWorkspace;->n:I

    invoke-virtual {v0}, Landroid/view/ViewConfiguration;->getScaledMaximumFlingVelocity()I

    move-result v0

    iput v0, p0, Lcom/google/android/youtube/core/ui/AbstractWorkspace;->o:I

    return-void
.end method

.method static synthetic a(Lcom/google/android/youtube/core/ui/AbstractWorkspace;)I
    .locals 1

    iget v0, p0, Lcom/google/android/youtube/core/ui/AbstractWorkspace;->d:I

    return v0
.end method

.method private a(ILcom/google/android/youtube/core/ui/AbstractWorkspace$SelectionMethod;)V
    .locals 8

    const/4 v2, 0x0

    invoke-direct {p0}, Lcom/google/android/youtube/core/ui/AbstractWorkspace;->b()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    invoke-static {p1, v0}, Ljava/lang/Math;->min(II)I

    move-result v0

    invoke-static {v2, v0}, Ljava/lang/Math;->max(II)I

    move-result v4

    iget v0, p0, Lcom/google/android/youtube/core/ui/AbstractWorkspace;->d:I

    sub-int v0, v4, v0

    invoke-static {v0}, Ljava/lang/Math;->abs(I)I

    move-result v0

    iput v4, p0, Lcom/google/android/youtube/core/ui/AbstractWorkspace;->e:I

    invoke-virtual {p0}, Lcom/google/android/youtube/core/ui/AbstractWorkspace;->getFocusedChild()Landroid/view/View;

    move-result-object v1

    if-eqz v1, :cond_0

    if-eqz v0, :cond_0

    iget v3, p0, Lcom/google/android/youtube/core/ui/AbstractWorkspace;->d:I

    invoke-virtual {p0, v3}, Lcom/google/android/youtube/core/ui/AbstractWorkspace;->b(I)Landroid/view/View;

    move-result-object v3

    if-ne v1, v3, :cond_0

    invoke-virtual {v1}, Landroid/view/View;->clearFocus()V

    :cond_0
    invoke-direct {p0}, Lcom/google/android/youtube/core/ui/AbstractWorkspace;->c()I

    move-result v1

    mul-int v3, v4, v1

    invoke-virtual {p0}, Lcom/google/android/youtube/core/ui/AbstractWorkspace;->getScrollX()I

    move-result v1

    sub-int/2addr v3, v1

    iget-object v5, p0, Lcom/google/android/youtube/core/ui/AbstractWorkspace;->s:Lcom/google/android/youtube/core/ui/AbstractWorkspace$InteractionMode;

    iget-boolean v5, v5, Lcom/google/android/youtube/core/ui/AbstractWorkspace$InteractionMode;->enableSmoothScroll:Z

    if-eqz v5, :cond_5

    mul-int/lit16 v0, v0, 0x12c

    if-nez v0, :cond_1

    invoke-static {v3}, Ljava/lang/Math;->abs(I)I

    move-result v0

    :cond_1
    invoke-virtual {p0, v0}, Lcom/google/android/youtube/core/ui/AbstractWorkspace;->awakenScrollBars(I)Z

    move v5, v0

    :goto_0
    iget v0, p0, Lcom/google/android/youtube/core/ui/AbstractWorkspace;->e:I

    iget v6, p0, Lcom/google/android/youtube/core/ui/AbstractWorkspace;->d:I

    if-eq v0, v6, :cond_2

    iget v0, p0, Lcom/google/android/youtube/core/ui/AbstractWorkspace;->d:I

    invoke-virtual {p0, v0}, Lcom/google/android/youtube/core/ui/AbstractWorkspace;->b(I)Landroid/view/View;

    move-result-object v0

    const/4 v6, 0x4

    invoke-virtual {v0, v6}, Landroid/view/View;->dispatchDisplayHint(I)V

    iget-object v0, p0, Lcom/google/android/youtube/core/ui/AbstractWorkspace;->a:Ljava/lang/Runnable;

    invoke-virtual {p0, v0}, Lcom/google/android/youtube/core/ui/AbstractWorkspace;->removeCallbacks(Ljava/lang/Runnable;)Z

    iget-object v0, p0, Lcom/google/android/youtube/core/ui/AbstractWorkspace;->a:Ljava/lang/Runnable;

    add-int/lit8 v6, v5, 0xa

    int-to-long v6, v6

    invoke-virtual {p0, v0, v6, v7}, Lcom/google/android/youtube/core/ui/AbstractWorkspace;->postDelayed(Ljava/lang/Runnable;J)Z

    :cond_2
    invoke-virtual {p0, v4}, Lcom/google/android/youtube/core/ui/AbstractWorkspace;->a(I)V

    iget-object v0, p0, Lcom/google/android/youtube/core/ui/AbstractWorkspace;->r:Lcom/google/android/youtube/core/ui/b;

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/google/android/youtube/core/ui/AbstractWorkspace;->r:Lcom/google/android/youtube/core/ui/b;

    :cond_3
    iget-object v0, p0, Lcom/google/android/youtube/core/ui/AbstractWorkspace;->f:Landroid/widget/Scroller;

    invoke-virtual {v0}, Landroid/widget/Scroller;->isFinished()Z

    move-result v0

    if-nez v0, :cond_4

    iget-object v0, p0, Lcom/google/android/youtube/core/ui/AbstractWorkspace;->f:Landroid/widget/Scroller;

    invoke-virtual {v0}, Landroid/widget/Scroller;->abortAnimation()V

    :cond_4
    iget-object v0, p0, Lcom/google/android/youtube/core/ui/AbstractWorkspace;->f:Landroid/widget/Scroller;

    move v4, v2

    invoke-virtual/range {v0 .. v5}, Landroid/widget/Scroller;->startScroll(IIIII)V

    invoke-virtual {p0}, Lcom/google/android/youtube/core/ui/AbstractWorkspace;->invalidate()V

    return-void

    :cond_5
    move v5, v2

    goto :goto_0
.end method

.method private a(Landroid/view/MotionEvent;)V
    .locals 3

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v0

    const v1, 0xff00

    and-int/2addr v0, v1

    shr-int/lit8 v0, v0, 0x8

    invoke-virtual {p1, v0}, Landroid/view/MotionEvent;->getPointerId(I)I

    move-result v1

    iget v2, p0, Lcom/google/android/youtube/core/ui/AbstractWorkspace;->p:I

    if-ne v1, v2, :cond_0

    if-nez v0, :cond_1

    const/4 v0, 0x1

    :goto_0
    invoke-virtual {p1, v0}, Landroid/view/MotionEvent;->getX(I)F

    move-result v1

    iput v1, p0, Lcom/google/android/youtube/core/ui/AbstractWorkspace;->h:F

    iput v1, p0, Lcom/google/android/youtube/core/ui/AbstractWorkspace;->i:F

    invoke-virtual {p1, v0}, Landroid/view/MotionEvent;->getY(I)F

    move-result v1

    iput v1, p0, Lcom/google/android/youtube/core/ui/AbstractWorkspace;->j:F

    invoke-virtual {p1, v0}, Landroid/view/MotionEvent;->getPointerId(I)I

    move-result v0

    iput v0, p0, Lcom/google/android/youtube/core/ui/AbstractWorkspace;->p:I

    iget-object v0, p0, Lcom/google/android/youtube/core/ui/AbstractWorkspace;->g:Landroid/view/VelocityTracker;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/youtube/core/ui/AbstractWorkspace;->g:Landroid/view/VelocityTracker;

    invoke-virtual {v0}, Landroid/view/VelocityTracker;->clear()V

    :cond_0
    return-void

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private b()I
    .locals 2

    invoke-virtual {p0}, Lcom/google/android/youtube/core/ui/AbstractWorkspace;->getChildCount()I

    move-result v0

    iget-object v1, p0, Lcom/google/android/youtube/core/ui/AbstractWorkspace;->q:Landroid/graphics/drawable/Drawable;

    if-eqz v1, :cond_0

    add-int/lit8 v0, v0, 0x1

    div-int/lit8 v0, v0, 0x2

    :cond_0
    return v0
.end method

.method private c()I
    .locals 2

    invoke-virtual {p0}, Lcom/google/android/youtube/core/ui/AbstractWorkspace;->getWidth()I

    move-result v0

    iget-object v1, p0, Lcom/google/android/youtube/core/ui/AbstractWorkspace;->q:Landroid/graphics/drawable/Drawable;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/google/android/youtube/core/ui/AbstractWorkspace;->q:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v1}, Landroid/graphics/drawable/Drawable;->getIntrinsicWidth()I

    move-result v1

    add-int/2addr v0, v1

    :cond_0
    return v0
.end method

.method private d(I)V
    .locals 1

    sget-object v0, Lcom/google/android/youtube/core/ui/AbstractWorkspace$SelectionMethod;->FLING:Lcom/google/android/youtube/core/ui/AbstractWorkspace$SelectionMethod;

    invoke-direct {p0, p1, v0}, Lcom/google/android/youtube/core/ui/AbstractWorkspace;->a(ILcom/google/android/youtube/core/ui/AbstractWorkspace$SelectionMethod;)V

    return-void
.end method


# virtual methods
.method public final a()I
    .locals 1

    iget v0, p0, Lcom/google/android/youtube/core/ui/AbstractWorkspace;->d:I

    return v0
.end method

.method protected abstract a(I)V
.end method

.method public addFocusables(Ljava/util/ArrayList;II)V
    .locals 2

    iget v0, p0, Lcom/google/android/youtube/core/ui/AbstractWorkspace;->d:I

    invoke-virtual {p0, v0}, Lcom/google/android/youtube/core/ui/AbstractWorkspace;->b(I)Landroid/view/View;

    move-result-object v0

    if-nez v0, :cond_1

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "Tab index "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v1, p0, Lcom/google/android/youtube/core/ui/AbstractWorkspace;->d:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " does not exist"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/youtube/core/L;->c(Ljava/lang/String;)V

    :cond_0
    :goto_0
    return-void

    :cond_1
    invoke-virtual {v0, p1, p2}, Landroid/view/View;->addFocusables(Ljava/util/ArrayList;I)V

    const/16 v0, 0x11

    if-ne p2, v0, :cond_2

    iget v0, p0, Lcom/google/android/youtube/core/ui/AbstractWorkspace;->d:I

    if-lez v0, :cond_0

    iget v0, p0, Lcom/google/android/youtube/core/ui/AbstractWorkspace;->d:I

    add-int/lit8 v0, v0, -0x1

    invoke-virtual {p0, v0}, Lcom/google/android/youtube/core/ui/AbstractWorkspace;->b(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, p1, p2}, Landroid/view/View;->addFocusables(Ljava/util/ArrayList;I)V

    goto :goto_0

    :cond_2
    const/16 v0, 0x42

    if-ne p2, v0, :cond_0

    iget v0, p0, Lcom/google/android/youtube/core/ui/AbstractWorkspace;->d:I

    invoke-direct {p0}, Lcom/google/android/youtube/core/ui/AbstractWorkspace;->b()I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    if-ge v0, v1, :cond_0

    iget v0, p0, Lcom/google/android/youtube/core/ui/AbstractWorkspace;->d:I

    add-int/lit8 v0, v0, 0x1

    invoke-virtual {p0, v0}, Lcom/google/android/youtube/core/ui/AbstractWorkspace;->b(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, p1, p2}, Landroid/view/View;->addFocusables(Ljava/util/ArrayList;I)V

    goto :goto_0
.end method

.method public addView(Landroid/view/View;)V
    .locals 1

    const/4 v0, 0x1

    invoke-virtual {p1, v0}, Landroid/view/View;->setClickable(Z)V

    invoke-super {p0, p1}, Landroid/view/ViewGroup;->addView(Landroid/view/View;)V

    return-void
.end method

.method public addView(Landroid/view/View;I)V
    .locals 1

    const/4 v0, 0x1

    invoke-virtual {p1, v0}, Landroid/view/View;->setClickable(Z)V

    invoke-super {p0, p1, p2}, Landroid/view/ViewGroup;->addView(Landroid/view/View;I)V

    return-void
.end method

.method public addView(Landroid/view/View;II)V
    .locals 1

    const/4 v0, 0x1

    invoke-virtual {p1, v0}, Landroid/view/View;->setClickable(Z)V

    invoke-super {p0, p1, p2, p3}, Landroid/view/ViewGroup;->addView(Landroid/view/View;II)V

    return-void
.end method

.method public addView(Landroid/view/View;ILandroid/view/ViewGroup$LayoutParams;)V
    .locals 1

    const/4 v0, 0x1

    invoke-virtual {p1, v0}, Landroid/view/View;->setClickable(Z)V

    invoke-super {p0, p1, p2, p3}, Landroid/view/ViewGroup;->addView(Landroid/view/View;ILandroid/view/ViewGroup$LayoutParams;)V

    return-void
.end method

.method public addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V
    .locals 1

    const/4 v0, 0x1

    invoke-virtual {p1, v0}, Landroid/view/View;->setClickable(Z)V

    invoke-super {p0, p1, p2}, Landroid/view/ViewGroup;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    return-void
.end method

.method final b(I)Landroid/view/View;
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/core/ui/AbstractWorkspace;->q:Landroid/graphics/drawable/Drawable;

    if-nez v0, :cond_0

    invoke-virtual {p0, p1}, Lcom/google/android/youtube/core/ui/AbstractWorkspace;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    mul-int/lit8 v0, p1, 0x2

    invoke-virtual {p0, v0}, Lcom/google/android/youtube/core/ui/AbstractWorkspace;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    goto :goto_0
.end method

.method protected final c(I)V
    .locals 1

    sget-object v0, Lcom/google/android/youtube/core/ui/AbstractWorkspace$SelectionMethod;->TAP:Lcom/google/android/youtube/core/ui/AbstractWorkspace$SelectionMethod;

    invoke-direct {p0, p1, v0}, Lcom/google/android/youtube/core/ui/AbstractWorkspace;->a(ILcom/google/android/youtube/core/ui/AbstractWorkspace$SelectionMethod;)V

    return-void
.end method

.method public computeScroll()V
    .locals 4

    const/4 v3, -0x1

    iget-object v0, p0, Lcom/google/android/youtube/core/ui/AbstractWorkspace;->f:Landroid/widget/Scroller;

    invoke-virtual {v0}, Landroid/widget/Scroller;->computeScrollOffset()Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/youtube/core/ui/AbstractWorkspace;->f:Landroid/widget/Scroller;

    invoke-virtual {v0}, Landroid/widget/Scroller;->getCurrX()I

    move-result v0

    iget-object v1, p0, Lcom/google/android/youtube/core/ui/AbstractWorkspace;->f:Landroid/widget/Scroller;

    invoke-virtual {v1}, Landroid/widget/Scroller;->getCurrY()I

    move-result v1

    invoke-virtual {p0, v0, v1}, Lcom/google/android/youtube/core/ui/AbstractWorkspace;->scrollTo(II)V

    invoke-virtual {p0}, Lcom/google/android/youtube/core/ui/AbstractWorkspace;->postInvalidate()V

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget v0, p0, Lcom/google/android/youtube/core/ui/AbstractWorkspace;->e:I

    if-eq v0, v3, :cond_0

    const/4 v0, 0x0

    iget v1, p0, Lcom/google/android/youtube/core/ui/AbstractWorkspace;->e:I

    invoke-direct {p0}, Lcom/google/android/youtube/core/ui/AbstractWorkspace;->b()I

    move-result v2

    add-int/lit8 v2, v2, -0x1

    invoke-static {v1, v2}, Ljava/lang/Math;->min(II)I

    move-result v1

    invoke-static {v0, v1}, Ljava/lang/Math;->max(II)I

    move-result v0

    iput v0, p0, Lcom/google/android/youtube/core/ui/AbstractWorkspace;->d:I

    iput v3, p0, Lcom/google/android/youtube/core/ui/AbstractWorkspace;->e:I

    iget-object v0, p0, Lcom/google/android/youtube/core/ui/AbstractWorkspace;->r:Lcom/google/android/youtube/core/ui/b;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/youtube/core/ui/AbstractWorkspace;->r:Lcom/google/android/youtube/core/ui/b;

    iget v0, p0, Lcom/google/android/youtube/core/ui/AbstractWorkspace;->d:I

    goto :goto_0
.end method

.method protected dispatchDraw(Landroid/graphics/Canvas;)V
    .locals 6

    const/4 v0, 0x0

    const/4 v2, 0x1

    iget v1, p0, Lcom/google/android/youtube/core/ui/AbstractWorkspace;->k:I

    if-eq v1, v2, :cond_1

    iget v1, p0, Lcom/google/android/youtube/core/ui/AbstractWorkspace;->e:I

    const/4 v3, -0x1

    if-ne v1, v3, :cond_1

    move v1, v2

    :goto_0
    if-eqz v1, :cond_2

    iget v0, p0, Lcom/google/android/youtube/core/ui/AbstractWorkspace;->d:I

    invoke-virtual {p0, v0}, Lcom/google/android/youtube/core/ui/AbstractWorkspace;->b(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {p0}, Lcom/google/android/youtube/core/ui/AbstractWorkspace;->getDrawingTime()J

    move-result-wide v1

    invoke-virtual {p0, p1, v0, v1, v2}, Lcom/google/android/youtube/core/ui/AbstractWorkspace;->drawChild(Landroid/graphics/Canvas;Landroid/view/View;J)Z

    :cond_0
    :goto_1
    return-void

    :cond_1
    move v1, v0

    goto :goto_0

    :cond_2
    invoke-virtual {p0}, Lcom/google/android/youtube/core/ui/AbstractWorkspace;->getDrawingTime()J

    move-result-wide v3

    iget v1, p0, Lcom/google/android/youtube/core/ui/AbstractWorkspace;->e:I

    if-ltz v1, :cond_3

    iget v1, p0, Lcom/google/android/youtube/core/ui/AbstractWorkspace;->e:I

    invoke-direct {p0}, Lcom/google/android/youtube/core/ui/AbstractWorkspace;->b()I

    move-result v5

    if-ge v1, v5, :cond_3

    iget v1, p0, Lcom/google/android/youtube/core/ui/AbstractWorkspace;->d:I

    iget v5, p0, Lcom/google/android/youtube/core/ui/AbstractWorkspace;->e:I

    sub-int/2addr v1, v5

    invoke-static {v1}, Ljava/lang/Math;->abs(I)I

    move-result v1

    if-ne v1, v2, :cond_3

    iget v0, p0, Lcom/google/android/youtube/core/ui/AbstractWorkspace;->d:I

    invoke-virtual {p0, v0}, Lcom/google/android/youtube/core/ui/AbstractWorkspace;->b(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {p0, p1, v0, v3, v4}, Lcom/google/android/youtube/core/ui/AbstractWorkspace;->drawChild(Landroid/graphics/Canvas;Landroid/view/View;J)Z

    iget v0, p0, Lcom/google/android/youtube/core/ui/AbstractWorkspace;->e:I

    invoke-virtual {p0, v0}, Lcom/google/android/youtube/core/ui/AbstractWorkspace;->b(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {p0, p1, v0, v3, v4}, Lcom/google/android/youtube/core/ui/AbstractWorkspace;->drawChild(Landroid/graphics/Canvas;Landroid/view/View;J)Z

    goto :goto_1

    :cond_3
    invoke-virtual {p0}, Lcom/google/android/youtube/core/ui/AbstractWorkspace;->getChildCount()I

    move-result v1

    :goto_2
    if-ge v0, v1, :cond_0

    invoke-virtual {p0, v0}, Lcom/google/android/youtube/core/ui/AbstractWorkspace;->getChildAt(I)Landroid/view/View;

    move-result-object v2

    invoke-virtual {p0, p1, v2, v3, v4}, Lcom/google/android/youtube/core/ui/AbstractWorkspace;->drawChild(Landroid/graphics/Canvas;Landroid/view/View;J)Z

    add-int/lit8 v0, v0, 0x1

    goto :goto_2
.end method

.method public dispatchUnhandledMove(Landroid/view/View;I)Z
    .locals 3

    const/4 v0, 0x1

    const/16 v1, 0x11

    if-ne p2, v1, :cond_0

    iget v1, p0, Lcom/google/android/youtube/core/ui/AbstractWorkspace;->d:I

    if-lez v1, :cond_1

    iget v1, p0, Lcom/google/android/youtube/core/ui/AbstractWorkspace;->d:I

    add-int/lit8 v1, v1, -0x1

    invoke-direct {p0, v1}, Lcom/google/android/youtube/core/ui/AbstractWorkspace;->d(I)V

    :goto_0
    return v0

    :cond_0
    const/16 v1, 0x42

    if-ne p2, v1, :cond_1

    iget v1, p0, Lcom/google/android/youtube/core/ui/AbstractWorkspace;->d:I

    invoke-direct {p0}, Lcom/google/android/youtube/core/ui/AbstractWorkspace;->b()I

    move-result v2

    add-int/lit8 v2, v2, -0x1

    if-ge v1, v2, :cond_1

    iget v1, p0, Lcom/google/android/youtube/core/ui/AbstractWorkspace;->d:I

    add-int/lit8 v1, v1, 0x1

    invoke-direct {p0, v1}, Lcom/google/android/youtube/core/ui/AbstractWorkspace;->d(I)V

    goto :goto_0

    :cond_1
    invoke-super {p0, p1, p2}, Landroid/view/ViewGroup;->dispatchUnhandledMove(Landroid/view/View;I)Z

    move-result v0

    goto :goto_0
.end method

.method public focusableViewAvailable(Landroid/view/View;)V
    .locals 3

    iget v0, p0, Lcom/google/android/youtube/core/ui/AbstractWorkspace;->d:I

    invoke-virtual {p0, v0}, Lcom/google/android/youtube/core/ui/AbstractWorkspace;->b(I)Landroid/view/View;

    move-result-object v1

    move-object v0, p1

    :goto_0
    if-ne v0, v1, :cond_1

    invoke-super {p0, p1}, Landroid/view/ViewGroup;->focusableViewAvailable(Landroid/view/View;)V

    :cond_0
    return-void

    :cond_1
    if-eq v0, p0, :cond_0

    invoke-virtual {v0}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    move-result-object v2

    instance-of v2, v2, Landroid/view/View;

    if-eqz v2, :cond_0

    invoke-virtual {v0}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    goto :goto_0
.end method

.method public onInterceptTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 7

    const/4 v2, 0x1

    const/4 v1, 0x0

    iget-object v0, p0, Lcom/google/android/youtube/core/ui/AbstractWorkspace;->s:Lcom/google/android/youtube/core/ui/AbstractWorkspace$InteractionMode;

    iget-boolean v0, v0, Lcom/google/android/youtube/core/ui/AbstractWorkspace$InteractionMode;->enableFling:Z

    if-nez v0, :cond_1

    :cond_0
    :goto_0
    return v1

    :cond_1
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v0

    const/4 v3, 0x2

    if-ne v0, v3, :cond_2

    iget v3, p0, Lcom/google/android/youtube/core/ui/AbstractWorkspace;->k:I

    if-eqz v3, :cond_2

    move v1, v2

    goto :goto_0

    :cond_2
    and-int/lit16 v0, v0, 0xff

    packed-switch v0, :pswitch_data_0

    :cond_3
    :goto_1
    :pswitch_0
    iget v0, p0, Lcom/google/android/youtube/core/ui/AbstractWorkspace;->k:I

    if-eqz v0, :cond_0

    move v1, v2

    goto :goto_0

    :pswitch_1
    iget v0, p0, Lcom/google/android/youtube/core/ui/AbstractWorkspace;->p:I

    invoke-virtual {p1, v0}, Landroid/view/MotionEvent;->findPointerIndex(I)I

    move-result v0

    invoke-virtual {p1, v0}, Landroid/view/MotionEvent;->getX(I)F

    move-result v5

    invoke-virtual {p1, v0}, Landroid/view/MotionEvent;->getY(I)F

    move-result v0

    iget v3, p0, Lcom/google/android/youtube/core/ui/AbstractWorkspace;->i:F

    sub-float v3, v5, v3

    invoke-static {v3}, Ljava/lang/Math;->abs(F)F

    move-result v3

    float-to-int v3, v3

    iget v4, p0, Lcom/google/android/youtube/core/ui/AbstractWorkspace;->j:F

    sub-float/2addr v0, v4

    invoke-static {v0}, Ljava/lang/Math;->abs(F)F

    move-result v0

    float-to-int v0, v0

    iget v6, p0, Lcom/google/android/youtube/core/ui/AbstractWorkspace;->m:I

    iget v4, p0, Lcom/google/android/youtube/core/ui/AbstractWorkspace;->n:I

    if-le v3, v4, :cond_6

    move v4, v2

    :goto_2
    if-le v3, v6, :cond_7

    move v3, v2

    :goto_3
    if-le v0, v6, :cond_8

    move v0, v2

    :goto_4
    if-nez v3, :cond_4

    if-eqz v0, :cond_3

    :cond_4
    if-eqz v4, :cond_5

    iput v2, p0, Lcom/google/android/youtube/core/ui/AbstractWorkspace;->k:I

    iput v5, p0, Lcom/google/android/youtube/core/ui/AbstractWorkspace;->i:F

    :cond_5
    iget-boolean v0, p0, Lcom/google/android/youtube/core/ui/AbstractWorkspace;->l:Z

    if-eqz v0, :cond_3

    iput-boolean v1, p0, Lcom/google/android/youtube/core/ui/AbstractWorkspace;->l:Z

    iget v0, p0, Lcom/google/android/youtube/core/ui/AbstractWorkspace;->d:I

    invoke-virtual {p0, v0}, Lcom/google/android/youtube/core/ui/AbstractWorkspace;->b(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/View;->cancelLongPress()V

    goto :goto_1

    :cond_6
    move v4, v1

    goto :goto_2

    :cond_7
    move v3, v1

    goto :goto_3

    :cond_8
    move v0, v1

    goto :goto_4

    :pswitch_2
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v0

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v3

    iput v0, p0, Lcom/google/android/youtube/core/ui/AbstractWorkspace;->h:F

    iput v0, p0, Lcom/google/android/youtube/core/ui/AbstractWorkspace;->i:F

    iput v3, p0, Lcom/google/android/youtube/core/ui/AbstractWorkspace;->j:F

    invoke-virtual {p1, v1}, Landroid/view/MotionEvent;->getPointerId(I)I

    move-result v0

    iput v0, p0, Lcom/google/android/youtube/core/ui/AbstractWorkspace;->p:I

    iput-boolean v2, p0, Lcom/google/android/youtube/core/ui/AbstractWorkspace;->l:Z

    iget-object v0, p0, Lcom/google/android/youtube/core/ui/AbstractWorkspace;->f:Landroid/widget/Scroller;

    invoke-virtual {v0}, Landroid/widget/Scroller;->isFinished()Z

    move-result v0

    if-eqz v0, :cond_9

    move v0, v1

    :goto_5
    iput v0, p0, Lcom/google/android/youtube/core/ui/AbstractWorkspace;->k:I

    goto :goto_1

    :cond_9
    move v0, v2

    goto :goto_5

    :pswitch_3
    iput v1, p0, Lcom/google/android/youtube/core/ui/AbstractWorkspace;->k:I

    iput-boolean v1, p0, Lcom/google/android/youtube/core/ui/AbstractWorkspace;->l:Z

    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/youtube/core/ui/AbstractWorkspace;->p:I

    goto :goto_1

    :pswitch_4
    invoke-direct {p0, p1}, Lcom/google/android/youtube/core/ui/AbstractWorkspace;->a(Landroid/view/MotionEvent;)V

    goto/16 :goto_1

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_2
        :pswitch_3
        :pswitch_1
        :pswitch_3
        :pswitch_0
        :pswitch_0
        :pswitch_4
    .end packed-switch
.end method

.method protected onLayout(ZIIII)V
    .locals 8

    const/4 v2, 0x0

    invoke-virtual {p0}, Lcom/google/android/youtube/core/ui/AbstractWorkspace;->getChildCount()I

    move-result v3

    move v1, v2

    move v0, v2

    :goto_0
    if-ge v1, v3, :cond_1

    invoke-virtual {p0, v1}, Lcom/google/android/youtube/core/ui/AbstractWorkspace;->getChildAt(I)Landroid/view/View;

    move-result-object v4

    invoke-virtual {v4}, Landroid/view/View;->getVisibility()I

    move-result v5

    const/16 v6, 0x8

    if-eq v5, v6, :cond_0

    invoke-virtual {v4}, Landroid/view/View;->getMeasuredWidth()I

    move-result v5

    add-int v6, v0, v5

    invoke-virtual {v4}, Landroid/view/View;->getMeasuredHeight()I

    move-result v7

    invoke-virtual {v4, v0, v2, v6, v7}, Landroid/view/View;->layout(IIII)V

    add-int/2addr v0, v5

    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_1
    return-void
.end method

.method protected onMeasure(II)V
    .locals 6

    const/high16 v2, 0x40000000

    const/4 v5, 0x1

    const/4 v1, 0x0

    invoke-super {p0, p1, p2}, Landroid/view/ViewGroup;->onMeasure(II)V

    invoke-static {p1}, Landroid/view/View$MeasureSpec;->getMode(I)I

    move-result v0

    if-eq v0, v2, :cond_0

    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Workspace can only be used in EXACTLY mode."

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    invoke-static {p2}, Landroid/view/View$MeasureSpec;->getMode(I)I

    move-result v0

    if-eq v0, v2, :cond_1

    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Workspace can only be used in EXACTLY mode."

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_1
    invoke-virtual {p0}, Lcom/google/android/youtube/core/ui/AbstractWorkspace;->getChildCount()I

    move-result v2

    move v0, v1

    :goto_0
    if-ge v0, v2, :cond_3

    iget-object v3, p0, Lcom/google/android/youtube/core/ui/AbstractWorkspace;->q:Landroid/graphics/drawable/Drawable;

    if-eqz v3, :cond_2

    and-int/lit8 v3, v0, 0x1

    if-ne v3, v5, :cond_2

    invoke-virtual {p0, v0}, Lcom/google/android/youtube/core/ui/AbstractWorkspace;->getChildAt(I)Landroid/view/View;

    move-result-object v3

    iget-object v4, p0, Lcom/google/android/youtube/core/ui/AbstractWorkspace;->q:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v4}, Landroid/graphics/drawable/Drawable;->getIntrinsicWidth()I

    move-result v4

    invoke-virtual {v3, v4, p2}, Landroid/view/View;->measure(II)V

    :goto_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_2
    invoke-virtual {p0, v0}, Lcom/google/android/youtube/core/ui/AbstractWorkspace;->getChildAt(I)Landroid/view/View;

    move-result-object v3

    invoke-virtual {v3, p1, p2}, Landroid/view/View;->measure(II)V

    goto :goto_1

    :cond_3
    iget-boolean v0, p0, Lcom/google/android/youtube/core/ui/AbstractWorkspace;->c:Z

    if-eqz v0, :cond_5

    invoke-virtual {p0, v1}, Lcom/google/android/youtube/core/ui/AbstractWorkspace;->setHorizontalScrollBarEnabled(Z)V

    invoke-static {p1}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v0

    iget-object v2, p0, Lcom/google/android/youtube/core/ui/AbstractWorkspace;->q:Landroid/graphics/drawable/Drawable;

    if-eqz v2, :cond_4

    iget-object v2, p0, Lcom/google/android/youtube/core/ui/AbstractWorkspace;->q:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v2}, Landroid/graphics/drawable/Drawable;->getIntrinsicWidth()I

    move-result v2

    add-int/2addr v0, v2

    :cond_4
    iget v2, p0, Lcom/google/android/youtube/core/ui/AbstractWorkspace;->d:I

    mul-int/2addr v0, v2

    invoke-virtual {p0, v0, v1}, Lcom/google/android/youtube/core/ui/AbstractWorkspace;->scrollTo(II)V

    invoke-virtual {p0, v5}, Lcom/google/android/youtube/core/ui/AbstractWorkspace;->setHorizontalScrollBarEnabled(Z)V

    iput-boolean v1, p0, Lcom/google/android/youtube/core/ui/AbstractWorkspace;->c:Z

    :cond_5
    return-void
.end method

.method protected onRequestFocusInDescendants(ILandroid/graphics/Rect;)Z
    .locals 2

    iget v0, p0, Lcom/google/android/youtube/core/ui/AbstractWorkspace;->e:I

    const/4 v1, -0x1

    if-eq v0, v1, :cond_1

    iget v0, p0, Lcom/google/android/youtube/core/ui/AbstractWorkspace;->e:I

    :goto_0
    invoke-virtual {p0, v0}, Lcom/google/android/youtube/core/ui/AbstractWorkspace;->b(I)Landroid/view/View;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {v0, p1, p2}, Landroid/view/View;->requestFocus(ILandroid/graphics/Rect;)Z

    :cond_0
    const/4 v0, 0x0

    return v0

    :cond_1
    iget v0, p0, Lcom/google/android/youtube/core/ui/AbstractWorkspace;->d:I

    goto :goto_0
.end method

.method protected onRestoreInstanceState(Landroid/os/Parcelable;)V
    .locals 2

    check-cast p1, Lcom/google/android/youtube/core/ui/AbstractWorkspace$SavedState;

    invoke-virtual {p1}, Lcom/google/android/youtube/core/ui/AbstractWorkspace$SavedState;->getSuperState()Landroid/os/Parcelable;

    move-result-object v0

    invoke-super {p0, v0}, Landroid/view/ViewGroup;->onRestoreInstanceState(Landroid/os/Parcelable;)V

    iget v0, p1, Lcom/google/android/youtube/core/ui/AbstractWorkspace$SavedState;->currentScreen:I

    iput v0, p0, Lcom/google/android/youtube/core/ui/AbstractWorkspace;->d:I

    iget v0, p0, Lcom/google/android/youtube/core/ui/AbstractWorkspace;->d:I

    if-ltz v0, :cond_0

    iget v0, p0, Lcom/google/android/youtube/core/ui/AbstractWorkspace;->d:I

    invoke-virtual {p0}, Lcom/google/android/youtube/core/ui/AbstractWorkspace;->getChildCount()I

    move-result v1

    if-le v0, v1, :cond_1

    :cond_0
    const/4 v0, 0x0

    iput v0, p0, Lcom/google/android/youtube/core/ui/AbstractWorkspace;->d:I

    :cond_1
    iget v0, p0, Lcom/google/android/youtube/core/ui/AbstractWorkspace;->d:I

    invoke-virtual {p0, v0}, Lcom/google/android/youtube/core/ui/AbstractWorkspace;->a(I)V

    return-void
.end method

.method protected onSaveInstanceState()Landroid/os/Parcelable;
    .locals 2

    new-instance v0, Lcom/google/android/youtube/core/ui/AbstractWorkspace$SavedState;

    invoke-super {p0}, Landroid/view/ViewGroup;->onSaveInstanceState()Landroid/os/Parcelable;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/google/android/youtube/core/ui/AbstractWorkspace$SavedState;-><init>(Landroid/os/Parcelable;)V

    iget v1, p0, Lcom/google/android/youtube/core/ui/AbstractWorkspace;->d:I

    iput v1, v0, Lcom/google/android/youtube/core/ui/AbstractWorkspace$SavedState;->currentScreen:I

    return-object v0
.end method

.method protected onSizeChanged(IIII)V
    .locals 1

    invoke-super {p0, p1, p2, p3, p4}, Landroid/view/ViewGroup;->onSizeChanged(IIII)V

    iget v0, p0, Lcom/google/android/youtube/core/ui/AbstractWorkspace;->d:I

    invoke-virtual {p0, v0}, Lcom/google/android/youtube/core/ui/AbstractWorkspace;->setCurrentScreen(I)V

    iget v0, p0, Lcom/google/android/youtube/core/ui/AbstractWorkspace;->d:I

    invoke-virtual {p0, v0}, Lcom/google/android/youtube/core/ui/AbstractWorkspace;->a(I)V

    return-void
.end method

.method public onTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 8

    const/4 v7, -0x1

    const/4 v1, 0x1

    const/4 v2, 0x0

    iget-object v0, p0, Lcom/google/android/youtube/core/ui/AbstractWorkspace;->g:Landroid/view/VelocityTracker;

    if-nez v0, :cond_0

    invoke-static {}, Landroid/view/VelocityTracker;->obtain()Landroid/view/VelocityTracker;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/youtube/core/ui/AbstractWorkspace;->g:Landroid/view/VelocityTracker;

    :cond_0
    iget-object v0, p0, Lcom/google/android/youtube/core/ui/AbstractWorkspace;->g:Landroid/view/VelocityTracker;

    invoke-virtual {v0, p1}, Landroid/view/VelocityTracker;->addMovement(Landroid/view/MotionEvent;)V

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v0

    and-int/lit16 v0, v0, 0xff

    packed-switch v0, :pswitch_data_0

    :cond_1
    :goto_0
    :pswitch_0
    return v1

    :pswitch_1
    iget-object v0, p0, Lcom/google/android/youtube/core/ui/AbstractWorkspace;->f:Landroid/widget/Scroller;

    invoke-virtual {v0}, Landroid/widget/Scroller;->isFinished()Z

    move-result v0

    if-nez v0, :cond_2

    iget-object v0, p0, Lcom/google/android/youtube/core/ui/AbstractWorkspace;->f:Landroid/widget/Scroller;

    invoke-virtual {v0}, Landroid/widget/Scroller;->abortAnimation()V

    :cond_2
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v0

    iput v0, p0, Lcom/google/android/youtube/core/ui/AbstractWorkspace;->i:F

    iput v0, p0, Lcom/google/android/youtube/core/ui/AbstractWorkspace;->h:F

    invoke-virtual {p1, v2}, Landroid/view/MotionEvent;->getPointerId(I)I

    move-result v0

    iput v0, p0, Lcom/google/android/youtube/core/ui/AbstractWorkspace;->p:I

    goto :goto_0

    :pswitch_2
    iget v0, p0, Lcom/google/android/youtube/core/ui/AbstractWorkspace;->k:I

    if-ne v0, v1, :cond_1

    iget v0, p0, Lcom/google/android/youtube/core/ui/AbstractWorkspace;->p:I

    invoke-virtual {p1, v0}, Landroid/view/MotionEvent;->findPointerIndex(I)I

    move-result v0

    invoke-virtual {p1, v0}, Landroid/view/MotionEvent;->getX(I)F

    move-result v0

    iget v3, p0, Lcom/google/android/youtube/core/ui/AbstractWorkspace;->i:F

    sub-float/2addr v3, v0

    float-to-int v3, v3

    iput v0, p0, Lcom/google/android/youtube/core/ui/AbstractWorkspace;->i:F

    invoke-virtual {p0}, Lcom/google/android/youtube/core/ui/AbstractWorkspace;->getScrollX()I

    move-result v0

    if-gez v3, :cond_3

    if-lez v0, :cond_1

    neg-int v0, v0

    invoke-static {v0, v3}, Ljava/lang/Math;->max(II)I

    move-result v0

    invoke-virtual {p0, v0, v2}, Lcom/google/android/youtube/core/ui/AbstractWorkspace;->scrollBy(II)V

    goto :goto_0

    :cond_3
    if-lez v3, :cond_4

    invoke-virtual {p0}, Lcom/google/android/youtube/core/ui/AbstractWorkspace;->getChildCount()I

    move-result v4

    add-int/lit8 v4, v4, -0x1

    invoke-virtual {p0, v4}, Lcom/google/android/youtube/core/ui/AbstractWorkspace;->getChildAt(I)Landroid/view/View;

    move-result-object v4

    invoke-virtual {v4}, Landroid/view/View;->getRight()I

    move-result v4

    sub-int v0, v4, v0

    invoke-virtual {p0}, Lcom/google/android/youtube/core/ui/AbstractWorkspace;->getWidth()I

    move-result v4

    sub-int/2addr v0, v4

    if-lez v0, :cond_1

    invoke-static {v0, v3}, Ljava/lang/Math;->min(II)I

    move-result v0

    invoke-virtual {p0, v0, v2}, Lcom/google/android/youtube/core/ui/AbstractWorkspace;->scrollBy(II)V

    goto :goto_0

    :cond_4
    invoke-virtual {p0}, Lcom/google/android/youtube/core/ui/AbstractWorkspace;->awakenScrollBars()Z

    goto :goto_0

    :pswitch_3
    iget v0, p0, Lcom/google/android/youtube/core/ui/AbstractWorkspace;->k:I

    if-ne v0, v1, :cond_5

    iget v0, p0, Lcom/google/android/youtube/core/ui/AbstractWorkspace;->p:I

    invoke-virtual {p1, v0}, Landroid/view/MotionEvent;->findPointerIndex(I)I

    move-result v3

    invoke-virtual {p1, v3}, Landroid/view/MotionEvent;->getX(I)F

    move-result v3

    iget-object v4, p0, Lcom/google/android/youtube/core/ui/AbstractWorkspace;->g:Landroid/view/VelocityTracker;

    const/16 v5, 0x3e8

    iget v6, p0, Lcom/google/android/youtube/core/ui/AbstractWorkspace;->o:I

    int-to-float v6, v6

    invoke-virtual {v4, v5, v6}, Landroid/view/VelocityTracker;->computeCurrentVelocity(IF)V

    invoke-virtual {v4, v0}, Landroid/view/VelocityTracker;->getXVelocity(I)F

    move-result v0

    float-to-int v4, v0

    iget v0, p0, Lcom/google/android/youtube/core/ui/AbstractWorkspace;->h:F

    sub-float/2addr v0, v3

    invoke-static {v0}, Ljava/lang/Math;->abs(F)F

    move-result v0

    const/high16 v3, 0x42c80000

    cmpl-float v0, v0, v3

    if-lez v0, :cond_6

    move v0, v1

    :goto_1
    invoke-virtual {p0}, Lcom/google/android/youtube/core/ui/AbstractWorkspace;->getWidth()I

    move-result v3

    invoke-virtual {p0}, Lcom/google/android/youtube/core/ui/AbstractWorkspace;->getScrollX()I

    move-result v5

    div-int/lit8 v6, v3, 0x2

    add-int/2addr v5, v6

    div-int v3, v5, v3

    if-eqz v0, :cond_7

    const/16 v5, 0x1f4

    if-le v4, v5, :cond_7

    iget v5, p0, Lcom/google/android/youtube/core/ui/AbstractWorkspace;->d:I

    if-lez v5, :cond_7

    iget v0, p0, Lcom/google/android/youtube/core/ui/AbstractWorkspace;->d:I

    add-int/lit8 v0, v0, -0x1

    invoke-static {v3, v0}, Ljava/lang/Math;->min(II)I

    move-result v0

    invoke-direct {p0, v0}, Lcom/google/android/youtube/core/ui/AbstractWorkspace;->d(I)V

    :goto_2
    iget-object v0, p0, Lcom/google/android/youtube/core/ui/AbstractWorkspace;->g:Landroid/view/VelocityTracker;

    if-eqz v0, :cond_5

    iget-object v0, p0, Lcom/google/android/youtube/core/ui/AbstractWorkspace;->g:Landroid/view/VelocityTracker;

    invoke-virtual {v0}, Landroid/view/VelocityTracker;->recycle()V

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/youtube/core/ui/AbstractWorkspace;->g:Landroid/view/VelocityTracker;

    :cond_5
    iput v2, p0, Lcom/google/android/youtube/core/ui/AbstractWorkspace;->k:I

    iput v7, p0, Lcom/google/android/youtube/core/ui/AbstractWorkspace;->p:I

    goto/16 :goto_0

    :cond_6
    move v0, v2

    goto :goto_1

    :cond_7
    if-eqz v0, :cond_8

    const/16 v0, -0x1f4

    if-ge v4, v0, :cond_8

    iget v0, p0, Lcom/google/android/youtube/core/ui/AbstractWorkspace;->d:I

    invoke-virtual {p0}, Lcom/google/android/youtube/core/ui/AbstractWorkspace;->getChildCount()I

    move-result v4

    add-int/lit8 v4, v4, -0x1

    if-ge v0, v4, :cond_8

    iget v0, p0, Lcom/google/android/youtube/core/ui/AbstractWorkspace;->d:I

    add-int/lit8 v0, v0, 0x1

    invoke-static {v3, v0}, Ljava/lang/Math;->max(II)I

    move-result v0

    invoke-direct {p0, v0}, Lcom/google/android/youtube/core/ui/AbstractWorkspace;->d(I)V

    goto :goto_2

    :cond_8
    invoke-direct {p0}, Lcom/google/android/youtube/core/ui/AbstractWorkspace;->c()I

    move-result v0

    invoke-virtual {p0}, Lcom/google/android/youtube/core/ui/AbstractWorkspace;->getScrollX()I

    move-result v3

    div-int/lit8 v4, v0, 0x2

    add-int/2addr v3, v4

    div-int v0, v3, v0

    invoke-direct {p0, v0}, Lcom/google/android/youtube/core/ui/AbstractWorkspace;->d(I)V

    goto :goto_2

    :pswitch_4
    iput v2, p0, Lcom/google/android/youtube/core/ui/AbstractWorkspace;->k:I

    iput v7, p0, Lcom/google/android/youtube/core/ui/AbstractWorkspace;->p:I

    goto/16 :goto_0

    :pswitch_5
    invoke-direct {p0, p1}, Lcom/google/android/youtube/core/ui/AbstractWorkspace;->a(Landroid/view/MotionEvent;)V

    goto/16 :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_3
        :pswitch_2
        :pswitch_4
        :pswitch_0
        :pswitch_0
        :pswitch_5
    .end packed-switch
.end method

.method public requestChildFocus(Landroid/view/View;Landroid/view/View;)V
    .locals 2

    invoke-super {p0, p1, p2}, Landroid/view/ViewGroup;->requestChildFocus(Landroid/view/View;Landroid/view/View;)V

    invoke-virtual {p0, p1}, Lcom/google/android/youtube/core/ui/AbstractWorkspace;->indexOfChild(Landroid/view/View;)I

    move-result v0

    if-ltz v0, :cond_0

    invoke-virtual {p0}, Lcom/google/android/youtube/core/ui/AbstractWorkspace;->isInTouchMode()Z

    move-result v1

    if-nez v1, :cond_0

    invoke-direct {p0, v0}, Lcom/google/android/youtube/core/ui/AbstractWorkspace;->d(I)V

    :cond_0
    return-void
.end method

.method public requestChildRectangleOnScreen(Landroid/view/View;Landroid/graphics/Rect;Z)Z
    .locals 2

    invoke-virtual {p0, p1}, Lcom/google/android/youtube/core/ui/AbstractWorkspace;->indexOfChild(Landroid/view/View;)I

    move-result v0

    iget v1, p0, Lcom/google/android/youtube/core/ui/AbstractWorkspace;->d:I

    if-ne v0, v1, :cond_0

    iget-object v1, p0, Lcom/google/android/youtube/core/ui/AbstractWorkspace;->f:Landroid/widget/Scroller;

    invoke-virtual {v1}, Landroid/widget/Scroller;->isFinished()Z

    move-result v1

    if-nez v1, :cond_1

    :cond_0
    invoke-direct {p0, v0}, Lcom/google/android/youtube/core/ui/AbstractWorkspace;->d(I)V

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public setCurrentScreen(I)V
    .locals 3

    const/4 v2, 0x0

    iget-object v0, p0, Lcom/google/android/youtube/core/ui/AbstractWorkspace;->f:Landroid/widget/Scroller;

    invoke-virtual {v0}, Landroid/widget/Scroller;->isFinished()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/youtube/core/ui/AbstractWorkspace;->f:Landroid/widget/Scroller;

    invoke-virtual {v0}, Landroid/widget/Scroller;->abortAnimation()V

    :cond_0
    invoke-direct {p0}, Lcom/google/android/youtube/core/ui/AbstractWorkspace;->b()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    invoke-static {p1, v0}, Ljava/lang/Math;->min(II)I

    move-result v0

    invoke-static {v2, v0}, Ljava/lang/Math;->max(II)I

    move-result v0

    iput v0, p0, Lcom/google/android/youtube/core/ui/AbstractWorkspace;->d:I

    iget v0, p0, Lcom/google/android/youtube/core/ui/AbstractWorkspace;->d:I

    invoke-direct {p0}, Lcom/google/android/youtube/core/ui/AbstractWorkspace;->c()I

    move-result v1

    mul-int/2addr v0, v1

    invoke-virtual {p0, v0, v2}, Lcom/google/android/youtube/core/ui/AbstractWorkspace;->scrollTo(II)V

    invoke-virtual {p0, p1}, Lcom/google/android/youtube/core/ui/AbstractWorkspace;->a(I)V

    invoke-virtual {p0}, Lcom/google/android/youtube/core/ui/AbstractWorkspace;->invalidate()V

    return-void
.end method

.method public setInteractionMode(Lcom/google/android/youtube/core/ui/AbstractWorkspace$InteractionMode;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/youtube/core/ui/AbstractWorkspace;->s:Lcom/google/android/youtube/core/ui/AbstractWorkspace$InteractionMode;

    return-void
.end method

.method public setOnLongClickListener(Landroid/view/View$OnLongClickListener;)V
    .locals 3

    invoke-direct {p0}, Lcom/google/android/youtube/core/ui/AbstractWorkspace;->b()I

    move-result v1

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v1, :cond_0

    invoke-virtual {p0, v0}, Lcom/google/android/youtube/core/ui/AbstractWorkspace;->b(I)Landroid/view/View;

    move-result-object v2

    invoke-virtual {v2, p1}, Landroid/view/View;->setOnLongClickListener(Landroid/view/View$OnLongClickListener;)V

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_0
    return-void
.end method

.method public setOnTabSelectedListener(Lcom/google/android/youtube/core/ui/b;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/youtube/core/ui/AbstractWorkspace;->r:Lcom/google/android/youtube/core/ui/b;

    return-void
.end method

.method public setSeparator(I)V
    .locals 7

    const/4 v0, 0x1

    iget-object v1, p0, Lcom/google/android/youtube/core/ui/AbstractWorkspace;->q:Landroid/graphics/drawable/Drawable;

    if-eqz v1, :cond_2

    if-nez p1, :cond_2

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/youtube/core/ui/AbstractWorkspace;->q:Landroid/graphics/drawable/Drawable;

    invoke-virtual {p0}, Lcom/google/android/youtube/core/ui/AbstractWorkspace;->getChildCount()I

    move-result v0

    add-int/lit8 v0, v0, -0x2

    :goto_0
    if-lez v0, :cond_0

    invoke-virtual {p0, v0}, Lcom/google/android/youtube/core/ui/AbstractWorkspace;->removeViewAt(I)V

    add-int/lit8 v0, v0, -0x2

    goto :goto_0

    :cond_0
    invoke-virtual {p0}, Lcom/google/android/youtube/core/ui/AbstractWorkspace;->requestLayout()V

    :cond_1
    :goto_1
    return-void

    :cond_2
    if-eqz p1, :cond_1

    iget-object v1, p0, Lcom/google/android/youtube/core/ui/AbstractWorkspace;->q:Landroid/graphics/drawable/Drawable;

    if-nez v1, :cond_4

    invoke-virtual {p0}, Lcom/google/android/youtube/core/ui/AbstractWorkspace;->getChildCount()I

    move-result v2

    invoke-virtual {p0}, Lcom/google/android/youtube/core/ui/AbstractWorkspace;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1, p1}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/youtube/core/ui/AbstractWorkspace;->q:Landroid/graphics/drawable/Drawable;

    move v1, v0

    :goto_2
    if-ge v1, v2, :cond_3

    new-instance v3, Landroid/view/View;

    invoke-virtual {p0}, Lcom/google/android/youtube/core/ui/AbstractWorkspace;->getContext()Landroid/content/Context;

    move-result-object v4

    invoke-direct {v3, v4}, Landroid/view/View;-><init>(Landroid/content/Context;)V

    iget-object v4, p0, Lcom/google/android/youtube/core/ui/AbstractWorkspace;->q:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v3, v4}, Landroid/view/View;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    new-instance v4, Landroid/view/ViewGroup$LayoutParams;

    const/4 v5, -0x2

    const/4 v6, -0x1

    invoke-direct {v4, v5, v6}, Landroid/view/ViewGroup$LayoutParams;-><init>(II)V

    invoke-virtual {v3, v4}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    invoke-virtual {p0, v3, v0}, Lcom/google/android/youtube/core/ui/AbstractWorkspace;->addView(Landroid/view/View;I)V

    add-int/lit8 v0, v0, 0x2

    add-int/lit8 v1, v1, 0x1

    goto :goto_2

    :cond_3
    invoke-virtual {p0}, Lcom/google/android/youtube/core/ui/AbstractWorkspace;->requestLayout()V

    goto :goto_1

    :cond_4
    invoke-virtual {p0}, Lcom/google/android/youtube/core/ui/AbstractWorkspace;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0, p1}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/youtube/core/ui/AbstractWorkspace;->q:Landroid/graphics/drawable/Drawable;

    invoke-virtual {p0}, Lcom/google/android/youtube/core/ui/AbstractWorkspace;->getChildCount()I

    move-result v0

    add-int/lit8 v0, v0, -0x2

    :goto_3
    if-lez v0, :cond_5

    invoke-virtual {p0, v0}, Lcom/google/android/youtube/core/ui/AbstractWorkspace;->getChildAt(I)Landroid/view/View;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/youtube/core/ui/AbstractWorkspace;->q:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v1, v2}, Landroid/view/View;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    add-int/lit8 v0, v0, -0x2

    goto :goto_3

    :cond_5
    invoke-virtual {p0}, Lcom/google/android/youtube/core/ui/AbstractWorkspace;->requestLayout()V

    goto :goto_1
.end method
