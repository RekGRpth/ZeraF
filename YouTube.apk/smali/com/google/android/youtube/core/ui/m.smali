.class final Lcom/google/android/youtube/core/ui/m;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/youtube/core/async/n;


# instance fields
.field final synthetic a:Lcom/google/android/youtube/core/ui/l;


# direct methods
.method constructor <init>(Lcom/google/android/youtube/core/ui/l;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/youtube/core/ui/m;->a:Lcom/google/android/youtube/core/ui/l;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final synthetic a(Ljava/lang/Object;Ljava/lang/Exception;)V
    .locals 3

    iget-object v0, p0, Lcom/google/android/youtube/core/ui/m;->a:Lcom/google/android/youtube/core/ui/l;

    invoke-virtual {v0}, Lcom/google/android/youtube/core/ui/l;->dismiss()V

    iget-object v0, p0, Lcom/google/android/youtube/core/ui/m;->a:Lcom/google/android/youtube/core/ui/l;

    invoke-static {v0}, Lcom/google/android/youtube/core/ui/l;->f(Lcom/google/android/youtube/core/ui/l;)Lcom/google/android/youtube/core/Analytics;

    move-result-object v0

    const-string v1, "ChannelCreationError"

    invoke-virtual {p2}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Lcom/google/android/youtube/core/Analytics;->a(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/google/android/youtube/core/ui/m;->a:Lcom/google/android/youtube/core/ui/l;

    invoke-static {v0}, Lcom/google/android/youtube/core/ui/l;->h(Lcom/google/android/youtube/core/ui/l;)Lcom/google/android/youtube/core/async/cc;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/youtube/core/ui/m;->a:Lcom/google/android/youtube/core/ui/l;

    invoke-static {v1}, Lcom/google/android/youtube/core/ui/l;->g(Lcom/google/android/youtube/core/ui/l;)Lcom/google/android/youtube/core/model/UserAuth;

    move-result-object v1

    iget-object v1, v1, Lcom/google/android/youtube/core/model/UserAuth;->account:Ljava/lang/String;

    invoke-interface {v0, v1, p2}, Lcom/google/android/youtube/core/async/cc;->a(Ljava/lang/String;Ljava/lang/Exception;)V

    return-void
.end method

.method public final synthetic a(Ljava/lang/Object;Ljava/lang/Object;)V
    .locals 4

    check-cast p2, Lcom/google/android/youtube/core/model/plus/Person;

    iget-object v0, p0, Lcom/google/android/youtube/core/ui/m;->a:Lcom/google/android/youtube/core/ui/l;

    const/4 v1, 0x1

    invoke-static {v0, v1}, Lcom/google/android/youtube/core/ui/l;->a(Lcom/google/android/youtube/core/ui/l;Z)V

    iget-object v0, p0, Lcom/google/android/youtube/core/ui/m;->a:Lcom/google/android/youtube/core/ui/l;

    invoke-static {v0}, Lcom/google/android/youtube/core/ui/l;->a(Lcom/google/android/youtube/core/ui/l;)Landroid/view/View;

    move-result-object v0

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    iget-object v0, p0, Lcom/google/android/youtube/core/ui/m;->a:Lcom/google/android/youtube/core/ui/l;

    invoke-static {v0}, Lcom/google/android/youtube/core/ui/l;->b(Lcom/google/android/youtube/core/ui/l;)Landroid/widget/TextView;

    move-result-object v0

    iget-object v1, p2, Lcom/google/android/youtube/core/model/plus/Person;->displayName:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lcom/google/android/youtube/core/ui/m;->a:Lcom/google/android/youtube/core/ui/l;

    invoke-static {v0}, Lcom/google/android/youtube/core/ui/l;->e(Lcom/google/android/youtube/core/ui/l;)Lcom/google/android/youtube/core/client/be;

    move-result-object v0

    iget-object v1, p2, Lcom/google/android/youtube/core/model/plus/Person;->imageUri:Landroid/net/Uri;

    iget-object v2, p0, Lcom/google/android/youtube/core/ui/m;->a:Lcom/google/android/youtube/core/ui/l;

    invoke-static {v2}, Lcom/google/android/youtube/core/ui/l;->c(Lcom/google/android/youtube/core/ui/l;)Landroid/app/Activity;

    move-result-object v2

    new-instance v3, Lcom/google/android/youtube/core/ui/n;

    invoke-direct {v3, p0}, Lcom/google/android/youtube/core/ui/n;-><init>(Lcom/google/android/youtube/core/ui/m;)V

    invoke-static {v2, v3}, Lcom/google/android/youtube/core/async/h;->a(Landroid/app/Activity;Lcom/google/android/youtube/core/async/n;)Lcom/google/android/youtube/core/async/h;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Lcom/google/android/youtube/core/client/be;->a(Landroid/net/Uri;Lcom/google/android/youtube/core/async/n;)V

    return-void
.end method
