.class public abstract Lcom/google/android/youtube/core/ui/BasePagedView;
.super Landroid/widget/LinearLayout;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/youtube/core/ui/PagedView;


# instance fields
.field protected final a:Landroid/view/ViewGroup;

.field protected final b:Landroid/view/ViewGroup;

.field protected final c:Landroid/widget/TextView;

.field protected final d:Landroid/view/View;

.field protected final e:Landroid/view/ViewGroup;

.field protected final f:Landroid/widget/TextView;

.field protected final g:Landroid/widget/Button;

.field protected h:Landroid/view/View;

.field protected i:Landroid/widget/ListAdapter;

.field protected final j:Lcom/google/android/youtube/core/ui/i;

.field protected k:Ljava/lang/String;

.field protected l:Lcom/google/android/youtube/core/ui/h;

.field protected m:Landroid/widget/AdapterView$OnItemClickListener;

.field protected n:Landroid/widget/AdapterView$OnItemLongClickListener;

.field private final o:Ljava/util/List;

.field private final p:Z

.field private q:Z

.field private final r:I

.field private final s:I

.field private t:Landroid/widget/FrameLayout;

.field private u:Lcom/google/android/youtube/core/ui/d;

.field private v:Lcom/google/android/youtube/core/ui/g;

.field private w:Lcom/google/android/youtube/core/ui/PagedView$State;


# direct methods
.method public constructor <init>(ILandroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 7

    const v1, 0x7f040060

    const v4, 0x7f0d0002

    const/4 v5, -0x1

    const/4 v6, 0x0

    move-object v0, p0

    move-object v2, p2

    move-object v3, p3

    invoke-direct/range {v0 .. v6}, Lcom/google/android/youtube/core/ui/BasePagedView;-><init>(ILandroid/content/Context;Landroid/util/AttributeSet;IILjava/lang/String;)V

    return-void
.end method

.method public constructor <init>(ILandroid/content/Context;Landroid/util/AttributeSet;IILjava/lang/String;)V
    .locals 9

    const/4 v8, 0x3

    const/4 v7, 0x2

    const/4 v6, -0x1

    const/4 v1, 0x1

    const/4 v2, 0x0

    invoke-direct {p0, p2, p3}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    sget-object v0, Lcom/google/android/youtube/core/ui/PagedView$State;->NEW:Lcom/google/android/youtube/core/ui/PagedView$State;

    iput-object v0, p0, Lcom/google/android/youtube/core/ui/BasePagedView;->w:Lcom/google/android/youtube/core/ui/PagedView$State;

    sget-object v0, Lcom/google/android/youtube/c;->b:[I

    invoke-virtual {p2, p3, v0, v2, p4}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[III)Landroid/content/res/TypedArray;

    move-result-object v3

    new-instance v0, Ljava/util/LinkedList;

    invoke-direct {v0}, Ljava/util/LinkedList;-><init>()V

    iput-object v0, p0, Lcom/google/android/youtube/core/ui/BasePagedView;->o:Ljava/util/List;

    invoke-virtual {p0}, Lcom/google/android/youtube/core/ui/BasePagedView;->g()Z

    move-result v0

    iput-boolean v0, p0, Lcom/google/android/youtube/core/ui/BasePagedView;->p:Z

    if-nez p6, :cond_1

    invoke-virtual {v3, v7}, Landroid/content/res/TypedArray;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/youtube/core/ui/BasePagedView;->k:Ljava/lang/String;

    :goto_0
    invoke-virtual {p0, v1}, Lcom/google/android/youtube/core/ui/BasePagedView;->setOrientation(I)V

    new-instance v0, Landroid/widget/FrameLayout;

    invoke-direct {v0, p2}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/google/android/youtube/core/ui/BasePagedView;->t:Landroid/widget/FrameLayout;

    new-instance v0, Landroid/view/ViewGroup$LayoutParams;

    invoke-direct {v0, v6, v6}, Landroid/view/ViewGroup$LayoutParams;-><init>(II)V

    iget-object v4, p0, Lcom/google/android/youtube/core/ui/BasePagedView;->t:Landroid/widget/FrameLayout;

    invoke-virtual {p0, v4, v0}, Lcom/google/android/youtube/core/ui/BasePagedView;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    invoke-static {p2}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v4

    const/4 v0, 0x0

    invoke-virtual {v4, p1, v0}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    iput-object v0, p0, Lcom/google/android/youtube/core/ui/BasePagedView;->a:Landroid/view/ViewGroup;

    iget-object v0, p0, Lcom/google/android/youtube/core/ui/BasePagedView;->t:Landroid/widget/FrameLayout;

    iget-object v5, p0, Lcom/google/android/youtube/core/ui/BasePagedView;->a:Landroid/view/ViewGroup;

    invoke-virtual {v0, v5}, Landroid/widget/FrameLayout;->addView(Landroid/view/View;)V

    if-gez p5, :cond_0

    invoke-virtual {v3, v8, v6}, Landroid/content/res/TypedArray;->getResourceId(II)I

    move-result p5

    :cond_0
    if-lez p5, :cond_2

    move v0, v1

    :goto_1
    const-string v5, "no statusView provided"

    invoke-static {v0, v5}, Lcom/google/android/youtube/core/utils/s;->b(ZLjava/lang/Object;)V

    new-instance v0, Lcom/google/android/youtube/core/ui/f;

    const/4 v5, 0x0

    invoke-virtual {v4, p5, v5}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v4

    invoke-direct {v0, v4}, Lcom/google/android/youtube/core/ui/f;-><init>(Landroid/view/View;)V

    iput-object v0, p0, Lcom/google/android/youtube/core/ui/BasePagedView;->j:Lcom/google/android/youtube/core/ui/i;

    iget-object v0, p0, Lcom/google/android/youtube/core/ui/BasePagedView;->a:Landroid/view/ViewGroup;

    invoke-virtual {v0, v2}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    iput-object v0, p0, Lcom/google/android/youtube/core/ui/BasePagedView;->b:Landroid/view/ViewGroup;

    iget-object v0, p0, Lcom/google/android/youtube/core/ui/BasePagedView;->b:Landroid/view/ViewGroup;

    invoke-virtual {v0, v2}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/google/android/youtube/core/ui/BasePagedView;->c:Landroid/widget/TextView;

    iget-object v0, p0, Lcom/google/android/youtube/core/ui/BasePagedView;->a:Landroid/view/ViewGroup;

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/youtube/core/ui/BasePagedView;->d:Landroid/view/View;

    iget-object v0, p0, Lcom/google/android/youtube/core/ui/BasePagedView;->a:Landroid/view/ViewGroup;

    const-string v4, "error_message_view"

    invoke-virtual {v0, v4}, Landroid/view/ViewGroup;->findViewWithTag(Ljava/lang/Object;)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    if-nez v0, :cond_3

    iget-object v0, p0, Lcom/google/android/youtube/core/ui/BasePagedView;->a:Landroid/view/ViewGroup;

    invoke-virtual {v0, v7}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    iput-object v0, p0, Lcom/google/android/youtube/core/ui/BasePagedView;->e:Landroid/view/ViewGroup;

    iget-object v0, p0, Lcom/google/android/youtube/core/ui/BasePagedView;->e:Landroid/view/ViewGroup;

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/google/android/youtube/core/ui/BasePagedView;->f:Landroid/widget/TextView;

    iget-object v0, p0, Lcom/google/android/youtube/core/ui/BasePagedView;->e:Landroid/view/ViewGroup;

    invoke-virtual {v0, v7}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lcom/google/android/youtube/core/ui/BasePagedView;->g:Landroid/widget/Button;

    :goto_2
    iget-object v0, p0, Lcom/google/android/youtube/core/ui/BasePagedView;->a:Landroid/view/ViewGroup;

    invoke-virtual {v0, v8}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/youtube/core/ui/BasePagedView;->h:Landroid/view/View;

    const v0, 0x7fffffff

    invoke-virtual {v3, v2, v0}, Landroid/content/res/TypedArray;->getInt(II)I

    move-result v0

    iput v0, p0, Lcom/google/android/youtube/core/ui/BasePagedView;->r:I

    const v0, 0x7fffffff

    invoke-virtual {v3, v1, v0}, Landroid/content/res/TypedArray;->getInt(II)I

    move-result v0

    iput v0, p0, Lcom/google/android/youtube/core/ui/BasePagedView;->s:I

    invoke-virtual {p0}, Lcom/google/android/youtube/core/ui/BasePagedView;->getPaddingLeft()I

    move-result v0

    invoke-virtual {p0}, Lcom/google/android/youtube/core/ui/BasePagedView;->getPaddingTop()I

    move-result v1

    invoke-virtual {p0}, Lcom/google/android/youtube/core/ui/BasePagedView;->getPaddingRight()I

    move-result v4

    invoke-virtual {p0}, Lcom/google/android/youtube/core/ui/BasePagedView;->getPaddingBottom()I

    move-result v5

    iget-object v6, p0, Lcom/google/android/youtube/core/ui/BasePagedView;->b:Landroid/view/ViewGroup;

    invoke-static {v6, v0, v1, v4, v5}, Lcom/google/android/youtube/core/ui/BasePagedView;->a(Landroid/view/View;IIII)V

    iget-object v6, p0, Lcom/google/android/youtube/core/ui/BasePagedView;->d:Landroid/view/View;

    invoke-static {v6, v0, v1, v4, v5}, Lcom/google/android/youtube/core/ui/BasePagedView;->a(Landroid/view/View;IIII)V

    iget-object v6, p0, Lcom/google/android/youtube/core/ui/BasePagedView;->h:Landroid/view/View;

    invoke-static {v6, v0, v1, v4, v5}, Lcom/google/android/youtube/core/ui/BasePagedView;->a(Landroid/view/View;IIII)V

    invoke-virtual {p0, v2, v2, v2, v2}, Lcom/google/android/youtube/core/ui/BasePagedView;->setPadding(IIII)V

    invoke-virtual {v3}, Landroid/content/res/TypedArray;->recycle()V

    new-instance v0, Lcom/google/android/youtube/core/ui/d;

    invoke-direct {v0, p0, v2}, Lcom/google/android/youtube/core/ui/d;-><init>(Lcom/google/android/youtube/core/ui/BasePagedView;B)V

    iput-object v0, p0, Lcom/google/android/youtube/core/ui/BasePagedView;->u:Lcom/google/android/youtube/core/ui/d;

    return-void

    :cond_1
    iput-object p6, p0, Lcom/google/android/youtube/core/ui/BasePagedView;->k:Ljava/lang/String;

    goto/16 :goto_0

    :cond_2
    move v0, v2

    goto/16 :goto_1

    :cond_3
    iput-object v0, p0, Lcom/google/android/youtube/core/ui/BasePagedView;->e:Landroid/view/ViewGroup;

    iget-object v0, p0, Lcom/google/android/youtube/core/ui/BasePagedView;->e:Landroid/view/ViewGroup;

    const-string v4, "error_message"

    invoke-virtual {v0, v4}, Landroid/view/ViewGroup;->findViewWithTag(Ljava/lang/Object;)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/google/android/youtube/core/ui/BasePagedView;->f:Landroid/widget/TextView;

    iget-object v0, p0, Lcom/google/android/youtube/core/ui/BasePagedView;->e:Landroid/view/ViewGroup;

    const-string v4, "retry_button"

    invoke-virtual {v0, v4}, Landroid/view/ViewGroup;->findViewWithTag(Ljava/lang/Object;)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lcom/google/android/youtube/core/ui/BasePagedView;->g:Landroid/widget/Button;

    goto :goto_2
.end method

.method private static a(Landroid/view/View;IIII)V
    .locals 4

    invoke-virtual {p0}, Landroid/view/View;->getPaddingLeft()I

    move-result v0

    add-int/2addr v0, p1

    invoke-virtual {p0}, Landroid/view/View;->getPaddingTop()I

    move-result v1

    add-int/2addr v1, p2

    invoke-virtual {p0}, Landroid/view/View;->getPaddingRight()I

    move-result v2

    add-int/2addr v2, p3

    invoke-virtual {p0}, Landroid/view/View;->getPaddingBottom()I

    move-result v3

    add-int/2addr v3, p4

    invoke-virtual {p0, v0, v1, v2, v3}, Landroid/view/View;->setPadding(IIII)V

    return-void
.end method

.method private a(Lcom/google/android/youtube/core/ui/PagedView$State;)V
    .locals 4

    const/16 v2, 0x8

    const/4 v1, 0x0

    iget-object v0, p0, Lcom/google/android/youtube/core/ui/BasePagedView;->w:Lcom/google/android/youtube/core/ui/PagedView$State;

    if-eq v0, p1, :cond_0

    iget-object v3, p0, Lcom/google/android/youtube/core/ui/BasePagedView;->b:Landroid/view/ViewGroup;

    sget-object v0, Lcom/google/android/youtube/core/ui/PagedView$State;->EMPTY:Lcom/google/android/youtube/core/ui/PagedView$State;

    if-ne p1, v0, :cond_1

    move v0, v1

    :goto_0
    invoke-virtual {v3, v0}, Landroid/view/ViewGroup;->setVisibility(I)V

    iget-object v3, p0, Lcom/google/android/youtube/core/ui/BasePagedView;->d:Landroid/view/View;

    sget-object v0, Lcom/google/android/youtube/core/ui/PagedView$State;->LOADING:Lcom/google/android/youtube/core/ui/PagedView$State;

    if-ne p1, v0, :cond_2

    move v0, v1

    :goto_1
    invoke-virtual {v3, v0}, Landroid/view/View;->setVisibility(I)V

    iget-object v3, p0, Lcom/google/android/youtube/core/ui/BasePagedView;->e:Landroid/view/ViewGroup;

    sget-object v0, Lcom/google/android/youtube/core/ui/PagedView$State;->ERROR:Lcom/google/android/youtube/core/ui/PagedView$State;

    if-ne p1, v0, :cond_3

    move v0, v1

    :goto_2
    invoke-virtual {v3, v0}, Landroid/view/ViewGroup;->setVisibility(I)V

    iget-object v0, p0, Lcom/google/android/youtube/core/ui/BasePagedView;->h:Landroid/view/View;

    sget-object v3, Lcom/google/android/youtube/core/ui/PagedView$State;->ITEMS:Lcom/google/android/youtube/core/ui/PagedView$State;

    if-ne p1, v3, :cond_4

    :goto_3
    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    iput-object p1, p0, Lcom/google/android/youtube/core/ui/BasePagedView;->w:Lcom/google/android/youtube/core/ui/PagedView$State;

    iget-object v0, p0, Lcom/google/android/youtube/core/ui/BasePagedView;->v:Lcom/google/android/youtube/core/ui/g;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/youtube/core/ui/BasePagedView;->v:Lcom/google/android/youtube/core/ui/g;

    iget-object v1, p0, Lcom/google/android/youtube/core/ui/BasePagedView;->w:Lcom/google/android/youtube/core/ui/PagedView$State;

    invoke-interface {v0, v1}, Lcom/google/android/youtube/core/ui/g;->a(Lcom/google/android/youtube/core/ui/PagedView$State;)V

    :cond_0
    return-void

    :cond_1
    move v0, v2

    goto :goto_0

    :cond_2
    move v0, v2

    goto :goto_1

    :cond_3
    move v0, v2

    goto :goto_2

    :cond_4
    move v1, v2

    goto :goto_3
.end method

.method private k()V
    .locals 6

    iget-boolean v0, p0, Lcom/google/android/youtube/core/ui/BasePagedView;->q:Z

    if-nez v0, :cond_0

    iget-boolean v0, p0, Lcom/google/android/youtube/core/ui/BasePagedView;->p:Z

    if-nez v0, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    const/4 v0, 0x0

    iget-object v1, p0, Lcom/google/android/youtube/core/ui/BasePagedView;->o:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    move v1, v0

    :goto_1
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    invoke-virtual {p0, v0}, Lcom/google/android/youtube/core/ui/BasePagedView;->c(Landroid/view/View;)V

    add-int/lit8 v2, v1, 0x1

    new-instance v4, Landroid/widget/LinearLayout$LayoutParams;

    invoke-virtual {v0}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v5

    invoke-direct {v4, v5}, Landroid/widget/LinearLayout$LayoutParams;-><init>(Landroid/view/ViewGroup$LayoutParams;)V

    invoke-virtual {p0, v0, v1, v4}, Lcom/google/android/youtube/core/ui/BasePagedView;->addView(Landroid/view/View;ILandroid/view/ViewGroup$LayoutParams;)V

    move v1, v2

    goto :goto_1

    :cond_2
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/youtube/core/ui/BasePagedView;->q:Z

    goto :goto_0
.end method


# virtual methods
.method public final a()I
    .locals 1

    iget v0, p0, Lcom/google/android/youtube/core/ui/BasePagedView;->r:I

    return v0
.end method

.method public final a(Landroid/view/View;)V
    .locals 3

    invoke-virtual {p1}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    const-string v1, "header view doesn\'t specify any layout params, did you inflate with a null parent?"

    invoke-static {v0, v1}, Lcom/google/android/youtube/core/utils/s;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v0, p0, Lcom/google/android/youtube/core/ui/BasePagedView;->o:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    iget-boolean v0, p0, Lcom/google/android/youtube/core/ui/BasePagedView;->q:Z

    if-nez v0, :cond_0

    iget-boolean v0, p0, Lcom/google/android/youtube/core/ui/BasePagedView;->p:Z

    if-nez v0, :cond_1

    :cond_0
    iget-object v0, p0, Lcom/google/android/youtube/core/ui/BasePagedView;->o:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    new-instance v1, Landroid/widget/LinearLayout$LayoutParams;

    invoke-virtual {p1}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v2

    invoke-direct {v1, v2}, Landroid/widget/LinearLayout$LayoutParams;-><init>(Landroid/view/ViewGroup$LayoutParams;)V

    invoke-virtual {p0, p1, v0, v1}, Lcom/google/android/youtube/core/ui/BasePagedView;->addView(Landroid/view/View;ILandroid/view/ViewGroup$LayoutParams;)V

    :goto_0
    return-void

    :cond_1
    invoke-virtual {p0, p1}, Lcom/google/android/youtube/core/ui/BasePagedView;->b(Landroid/view/View;)V

    goto :goto_0
.end method

.method public final a(Ljava/lang/String;)V
    .locals 2

    iget-object v0, p0, Lcom/google/android/youtube/core/ui/BasePagedView;->f:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lcom/google/android/youtube/core/ui/BasePagedView;->g:Landroid/widget/Button;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setVisibility(I)V

    sget-object v0, Lcom/google/android/youtube/core/ui/PagedView$State;->ERROR:Lcom/google/android/youtube/core/ui/PagedView$State;

    invoke-direct {p0, v0}, Lcom/google/android/youtube/core/ui/BasePagedView;->a(Lcom/google/android/youtube/core/ui/PagedView$State;)V

    invoke-direct {p0}, Lcom/google/android/youtube/core/ui/BasePagedView;->k()V

    return-void
.end method

.method public final b()I
    .locals 1

    iget v0, p0, Lcom/google/android/youtube/core/ui/BasePagedView;->s:I

    return v0
.end method

.method protected abstract b(Landroid/view/View;)V
.end method

.method public final b(Ljava/lang/String;)V
    .locals 2

    iget-object v0, p0, Lcom/google/android/youtube/core/ui/BasePagedView;->f:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lcom/google/android/youtube/core/ui/BasePagedView;->g:Landroid/widget/Button;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setVisibility(I)V

    sget-object v0, Lcom/google/android/youtube/core/ui/PagedView$State;->ERROR:Lcom/google/android/youtube/core/ui/PagedView$State;

    invoke-direct {p0, v0}, Lcom/google/android/youtube/core/ui/BasePagedView;->a(Lcom/google/android/youtube/core/ui/PagedView$State;)V

    invoke-direct {p0}, Lcom/google/android/youtube/core/ui/BasePagedView;->k()V

    return-void
.end method

.method public final c()V
    .locals 2

    iget-object v0, p0, Lcom/google/android/youtube/core/ui/BasePagedView;->c:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/google/android/youtube/core/ui/BasePagedView;->k:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    sget-object v0, Lcom/google/android/youtube/core/ui/PagedView$State;->EMPTY:Lcom/google/android/youtube/core/ui/PagedView$State;

    invoke-direct {p0, v0}, Lcom/google/android/youtube/core/ui/BasePagedView;->a(Lcom/google/android/youtube/core/ui/PagedView$State;)V

    invoke-direct {p0}, Lcom/google/android/youtube/core/ui/BasePagedView;->k()V

    return-void
.end method

.method protected abstract c(Landroid/view/View;)V
.end method

.method public final d()V
    .locals 2

    sget-object v0, Lcom/google/android/youtube/core/ui/PagedView$State;->ITEMS:Lcom/google/android/youtube/core/ui/PagedView$State;

    invoke-direct {p0, v0}, Lcom/google/android/youtube/core/ui/BasePagedView;->a(Lcom/google/android/youtube/core/ui/PagedView$State;)V

    iget-boolean v0, p0, Lcom/google/android/youtube/core/ui/BasePagedView;->q:Z

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/google/android/youtube/core/ui/BasePagedView;->p:Z

    if-nez v0, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v0, p0, Lcom/google/android/youtube/core/ui/BasePagedView;->o:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    invoke-virtual {p0, v0}, Lcom/google/android/youtube/core/ui/BasePagedView;->removeView(Landroid/view/View;)V

    invoke-virtual {p0, v0}, Lcom/google/android/youtube/core/ui/BasePagedView;->b(Landroid/view/View;)V

    goto :goto_1

    :cond_2
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/youtube/core/ui/BasePagedView;->q:Z

    goto :goto_0
.end method

.method public final e()V
    .locals 1

    sget-object v0, Lcom/google/android/youtube/core/ui/PagedView$State;->LOADING:Lcom/google/android/youtube/core/ui/PagedView$State;

    invoke-direct {p0, v0}, Lcom/google/android/youtube/core/ui/BasePagedView;->a(Lcom/google/android/youtube/core/ui/PagedView$State;)V

    invoke-direct {p0}, Lcom/google/android/youtube/core/ui/BasePagedView;->k()V

    return-void
.end method

.method public final f()Lcom/google/android/youtube/core/ui/PagedView$State;
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/core/ui/BasePagedView;->w:Lcom/google/android/youtube/core/ui/PagedView$State;

    return-object v0
.end method

.method protected abstract g()Z
.end method

.method public final h()Lcom/google/android/youtube/core/ui/i;
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/core/ui/BasePagedView;->j:Lcom/google/android/youtube/core/ui/i;

    return-object v0
.end method

.method public final i()Landroid/view/View;
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/core/ui/BasePagedView;->h:Landroid/view/View;

    return-object v0
.end method

.method public j()V
    .locals 0

    return-void
.end method

.method public setAdapter(Landroid/widget/ListAdapter;)V
    .locals 2

    iput-object p1, p0, Lcom/google/android/youtube/core/ui/BasePagedView;->i:Landroid/widget/ListAdapter;

    iget-object v0, p0, Lcom/google/android/youtube/core/ui/BasePagedView;->i:Landroid/widget/ListAdapter;

    iget-object v1, p0, Lcom/google/android/youtube/core/ui/BasePagedView;->u:Lcom/google/android/youtube/core/ui/d;

    invoke-interface {v0, v1}, Landroid/widget/ListAdapter;->registerDataSetObserver(Landroid/database/DataSetObserver;)V

    return-void
.end method

.method public setEmptyText(I)V
    .locals 1

    invoke-virtual {p0}, Lcom/google/android/youtube/core/ui/BasePagedView;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0, p1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/youtube/core/ui/BasePagedView;->k:Ljava/lang/String;

    iget-object v0, p0, Lcom/google/android/youtube/core/ui/BasePagedView;->c:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(I)V

    return-void
.end method

.method public setEmptyText(Ljava/lang/CharSequence;)V
    .locals 1

    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/youtube/core/ui/BasePagedView;->k:Ljava/lang/String;

    iget-object v0, p0, Lcom/google/android/youtube/core/ui/BasePagedView;->c:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    return-void
.end method

.method public setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/youtube/core/ui/BasePagedView;->m:Landroid/widget/AdapterView$OnItemClickListener;

    return-void
.end method

.method public setOnItemLongClickListener(Landroid/widget/AdapterView$OnItemLongClickListener;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/youtube/core/ui/BasePagedView;->n:Landroid/widget/AdapterView$OnItemLongClickListener;

    return-void
.end method

.method public setOnKeyListener(Landroid/view/View$OnKeyListener;)V
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/core/ui/BasePagedView;->h:Landroid/view/View;

    invoke-virtual {v0, p1}, Landroid/view/View;->setOnKeyListener(Landroid/view/View$OnKeyListener;)V

    return-void
.end method

.method public final setOnPagedViewStateChangeListener(Lcom/google/android/youtube/core/ui/g;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/youtube/core/ui/BasePagedView;->v:Lcom/google/android/youtube/core/ui/g;

    return-void
.end method

.method public final setOnRetryClickListener(Landroid/view/View$OnClickListener;)V
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/core/ui/BasePagedView;->g:Landroid/widget/Button;

    invoke-virtual {v0, p1}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    return-void
.end method

.method public final setOnScrollListener(Lcom/google/android/youtube/core/ui/h;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/youtube/core/ui/BasePagedView;->l:Lcom/google/android/youtube/core/ui/h;

    return-void
.end method
