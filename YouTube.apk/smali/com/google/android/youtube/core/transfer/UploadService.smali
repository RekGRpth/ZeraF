.class public Lcom/google/android/youtube/core/transfer/UploadService;
.super Lcom/google/android/youtube/core/transfer/TransferService;
.source "SourceFile"


# instance fields
.field private a:Ljava/util/concurrent/Executor;

.field private b:Lorg/apache/http/client/HttpClient;

.field private c:Lcom/google/android/youtube/core/client/bc;

.field private d:Lcom/google/android/youtube/core/async/UserAuthorizer;

.field private e:Lcom/google/android/youtube/core/converter/http/fq;


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Lcom/google/android/youtube/core/transfer/TransferService;-><init>()V

    return-void
.end method

.method public static a(Landroid/content/Context;)Landroid/content/Intent;
    .locals 1

    const-class v0, Lcom/google/android/youtube/core/transfer/UploadService;

    invoke-static {p0, v0}, Lcom/google/android/youtube/core/transfer/TransferService;->a(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    move-result-object v0

    return-object v0
.end method

.method public static a(Landroid/content/Context;Lcom/google/android/youtube/core/transfer/i;)Lcom/google/android/youtube/core/utils/aa;
    .locals 1

    const-class v0, Lcom/google/android/youtube/core/transfer/UploadService;

    invoke-static {p0, v0, p1}, Lcom/google/android/youtube/core/transfer/TransferService;->a(Landroid/content/Context;Ljava/lang/Class;Lcom/google/android/youtube/core/transfer/i;)Lcom/google/android/youtube/core/utils/aa;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public final a(Lcom/google/android/youtube/core/transfer/Transfer;Lcom/google/android/youtube/core/transfer/n;)Lcom/google/android/youtube/core/transfer/m;
    .locals 10

    new-instance v0, Lcom/google/android/youtube/core/transfer/ab;

    iget-object v2, p0, Lcom/google/android/youtube/core/transfer/UploadService;->a:Ljava/util/concurrent/Executor;

    iget-object v3, p0, Lcom/google/android/youtube/core/transfer/UploadService;->b:Lorg/apache/http/client/HttpClient;

    iget-object v4, p0, Lcom/google/android/youtube/core/transfer/UploadService;->c:Lcom/google/android/youtube/core/client/bc;

    iget-object v5, p0, Lcom/google/android/youtube/core/transfer/UploadService;->d:Lcom/google/android/youtube/core/async/UserAuthorizer;

    invoke-virtual {p0}, Lcom/google/android/youtube/core/transfer/UploadService;->getApplication()Landroid/app/Application;

    move-result-object v1

    check-cast v1, Lcom/google/android/youtube/core/BaseApplication;

    invoke-virtual {v1}, Lcom/google/android/youtube/core/BaseApplication;->i()Lcom/google/android/youtube/core/Analytics;

    move-result-object v6

    iget-object v7, p0, Lcom/google/android/youtube/core/transfer/UploadService;->e:Lcom/google/android/youtube/core/converter/http/fq;

    move-object v1, p0

    move-object v8, p1

    move-object v9, p2

    invoke-direct/range {v0 .. v9}, Lcom/google/android/youtube/core/transfer/ab;-><init>(Landroid/content/Context;Ljava/util/concurrent/Executor;Lorg/apache/http/client/HttpClient;Lcom/google/android/youtube/core/client/bc;Lcom/google/android/youtube/core/async/UserAuthorizer;Lcom/google/android/youtube/core/Analytics;Lcom/google/android/youtube/core/converter/http/fq;Lcom/google/android/youtube/core/transfer/Transfer;Lcom/google/android/youtube/core/transfer/n;)V

    return-object v0
.end method

.method public final a(Lcom/google/android/youtube/core/transfer/Transfer;)Ljava/lang/Runnable;
    .locals 1

    const/4 v0, 0x0

    return-object v0
.end method

.method protected final a()Ljava/lang/String;
    .locals 1

    const-string v0, "uploads.db"

    return-object v0
.end method

.method protected final b()Ljava/lang/String;
    .locals 1

    const-string v0, "upload_policy"

    return-object v0
.end method

.method protected final c()Ljava/lang/String;
    .locals 1

    const/4 v0, 0x0

    return-object v0
.end method

.method protected final d()Ljava/lang/String;
    .locals 1

    const/4 v0, 0x0

    return-object v0
.end method

.method protected final e()Ljava/lang/String;
    .locals 1

    const/4 v0, 0x0

    return-object v0
.end method

.method public onCreate()V
    .locals 4

    invoke-super {p0}, Lcom/google/android/youtube/core/transfer/TransferService;->onCreate()V

    invoke-virtual {p0}, Lcom/google/android/youtube/core/transfer/UploadService;->getApplication()Landroid/app/Application;

    move-result-object v0

    check-cast v0, Lcom/google/android/youtube/core/BaseApplication;

    invoke-virtual {v0}, Lcom/google/android/youtube/core/BaseApplication;->N()Ljava/util/concurrent/Executor;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/youtube/core/transfer/UploadService;->a:Ljava/util/concurrent/Executor;

    invoke-virtual {v0}, Lcom/google/android/youtube/core/BaseApplication;->P()Lorg/apache/http/client/HttpClient;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/youtube/core/transfer/UploadService;->b:Lorg/apache/http/client/HttpClient;

    invoke-virtual {v0}, Lcom/google/android/youtube/core/BaseApplication;->U()Lcom/google/android/youtube/core/async/UserAuthorizer;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/youtube/core/transfer/UploadService;->d:Lcom/google/android/youtube/core/async/UserAuthorizer;

    invoke-virtual {v0}, Lcom/google/android/youtube/core/BaseApplication;->R()Lcom/google/android/youtube/core/converter/m;

    move-result-object v1

    new-instance v2, Lcom/google/android/youtube/core/converter/http/fq;

    const/4 v3, 0x0

    invoke-direct {v2, v1, v3}, Lcom/google/android/youtube/core/converter/http/fq;-><init>(Lcom/google/android/youtube/core/converter/m;Z)V

    iput-object v2, p0, Lcom/google/android/youtube/core/transfer/UploadService;->e:Lcom/google/android/youtube/core/converter/http/fq;

    check-cast v0, Lcom/google/android/youtube/core/client/bd;

    invoke-interface {v0}, Lcom/google/android/youtube/core/client/bd;->a()Lcom/google/android/youtube/core/client/bc;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/youtube/core/transfer/UploadService;->c:Lcom/google/android/youtube/core/client/bc;

    return-void
.end method
