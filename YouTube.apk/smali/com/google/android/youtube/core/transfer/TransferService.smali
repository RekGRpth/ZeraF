.class public abstract Lcom/google/android/youtube/core/transfer/TransferService;
.super Landroid/app/Service;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/youtube/core/transfer/s;
.implements Lcom/google/android/youtube/core/transfer/w;


# instance fields
.field private a:Lcom/google/android/youtube/core/transfer/j;

.field private b:Ljava/util/Map;

.field private c:Z

.field private d:Lcom/google/android/youtube/core/transfer/l;

.field private e:Ljava/util/Set;

.field private f:Lcom/google/android/youtube/core/transfer/TransfersExecutor;

.field private g:I

.field private h:Lcom/google/android/youtube/core/transfer/e;

.field private i:Landroid/content/SharedPreferences;

.field private j:Landroid/content/SharedPreferences$OnSharedPreferenceChangeListener;


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Landroid/app/Service;-><init>()V

    return-void
.end method

.method static synthetic a(Lcom/google/android/youtube/core/transfer/TransferService;I)I
    .locals 0

    iput p1, p0, Lcom/google/android/youtube/core/transfer/TransferService;->g:I

    return p1
.end method

.method protected static a(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;
    .locals 1

    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0, p0, p1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    return-object v0
.end method

.method static synthetic a(Lcom/google/android/youtube/core/transfer/TransferService;)Lcom/google/android/youtube/core/transfer/TransfersExecutor;
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/core/transfer/TransferService;->f:Lcom/google/android/youtube/core/transfer/TransfersExecutor;

    return-object v0
.end method

.method protected static a(Landroid/content/Context;Ljava/lang/Class;Lcom/google/android/youtube/core/transfer/i;)Lcom/google/android/youtube/core/utils/aa;
    .locals 1

    const-string v0, "context may not be null"

    invoke-static {p0, v0}, Lcom/google/android/youtube/core/utils/s;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    new-instance v0, Lcom/google/android/youtube/core/transfer/h;

    invoke-direct {v0, p1, p2}, Lcom/google/android/youtube/core/transfer/h;-><init>(Ljava/lang/Class;Lcom/google/android/youtube/core/transfer/i;)V

    invoke-virtual {v0, p0}, Lcom/google/android/youtube/core/utils/aa;->a(Landroid/content/Context;)V

    return-object v0
.end method

.method static synthetic a(Lcom/google/android/youtube/core/transfer/TransferService;Ljava/util/Map;)Ljava/util/Map;
    .locals 0

    iput-object p1, p0, Lcom/google/android/youtube/core/transfer/TransferService;->b:Ljava/util/Map;

    return-object p1
.end method

.method static synthetic a(Lcom/google/android/youtube/core/transfer/TransferService;Z)Z
    .locals 1

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/youtube/core/transfer/TransferService;->c:Z

    return v0
.end method

.method static synthetic b(Lcom/google/android/youtube/core/transfer/TransferService;)Ljava/util/Set;
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/core/transfer/TransferService;->e:Ljava/util/Set;

    return-object v0
.end method

.method static synthetic c(Lcom/google/android/youtube/core/transfer/TransferService;)Ljava/util/Map;
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/core/transfer/TransferService;->b:Ljava/util/Map;

    return-object v0
.end method

.method static synthetic d(Lcom/google/android/youtube/core/transfer/TransferService;)I
    .locals 1

    iget v0, p0, Lcom/google/android/youtube/core/transfer/TransferService;->g:I

    return v0
.end method

.method static synthetic e(Lcom/google/android/youtube/core/transfer/TransferService;)V
    .locals 0

    invoke-direct {p0}, Lcom/google/android/youtube/core/transfer/TransferService;->h()V

    return-void
.end method

.method static synthetic f(Lcom/google/android/youtube/core/transfer/TransferService;)V
    .locals 0

    invoke-direct {p0}, Lcom/google/android/youtube/core/transfer/TransferService;->i()V

    return-void
.end method

.method static synthetic g(Lcom/google/android/youtube/core/transfer/TransferService;)V
    .locals 0

    invoke-direct {p0}, Lcom/google/android/youtube/core/transfer/TransferService;->j()V

    return-void
.end method

.method private h()V
    .locals 3

    const/4 v0, 0x0

    invoke-virtual {p0}, Lcom/google/android/youtube/core/transfer/TransferService;->b()Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_0

    const v0, 0x7f0b0028

    invoke-virtual {p0, v0}, Lcom/google/android/youtube/core/transfer/TransferService;->getString(I)Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/youtube/core/transfer/TransferService;->i:Landroid/content/SharedPreferences;

    invoke-virtual {p0}, Lcom/google/android/youtube/core/transfer/TransferService;->b()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v1, v2, v0}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    :cond_0
    iget-object v1, p0, Lcom/google/android/youtube/core/transfer/TransferService;->f:Lcom/google/android/youtube/core/transfer/TransfersExecutor;

    invoke-virtual {v1, v0}, Lcom/google/android/youtube/core/transfer/TransfersExecutor;->b(Z)I

    move-result v0

    iput v0, p0, Lcom/google/android/youtube/core/transfer/TransferService;->g:I

    return-void
.end method

.method static synthetic h(Lcom/google/android/youtube/core/transfer/TransferService;)V
    .locals 0

    invoke-direct {p0}, Lcom/google/android/youtube/core/transfer/TransferService;->k()V

    return-void
.end method

.method private i()V
    .locals 3

    const/4 v0, 0x0

    invoke-virtual {p0}, Lcom/google/android/youtube/core/transfer/TransferService;->c()Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_0

    iget-object v2, p0, Lcom/google/android/youtube/core/transfer/TransferService;->i:Landroid/content/SharedPreferences;

    invoke-interface {v2, v1, v0}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    :cond_0
    iget-object v1, p0, Lcom/google/android/youtube/core/transfer/TransferService;->f:Lcom/google/android/youtube/core/transfer/TransfersExecutor;

    invoke-virtual {v1, v0}, Lcom/google/android/youtube/core/transfer/TransfersExecutor;->a(Z)I

    move-result v0

    iput v0, p0, Lcom/google/android/youtube/core/transfer/TransferService;->g:I

    return-void
.end method

.method private j()V
    .locals 3

    const v0, 0x7fffffff

    invoke-virtual {p0}, Lcom/google/android/youtube/core/transfer/TransferService;->d()Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_0

    iget-object v2, p0, Lcom/google/android/youtube/core/transfer/TransferService;->i:Landroid/content/SharedPreferences;

    invoke-interface {v2, v1, v0}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v0

    :cond_0
    iget-object v1, p0, Lcom/google/android/youtube/core/transfer/TransferService;->f:Lcom/google/android/youtube/core/transfer/TransfersExecutor;

    invoke-virtual {v1, v0}, Lcom/google/android/youtube/core/transfer/TransfersExecutor;->a(I)I

    move-result v0

    iput v0, p0, Lcom/google/android/youtube/core/transfer/TransferService;->g:I

    return-void
.end method

.method private k()V
    .locals 3

    invoke-virtual {p0}, Lcom/google/android/youtube/core/transfer/TransferService;->e()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_0

    iget-object v1, p0, Lcom/google/android/youtube/core/transfer/TransferService;->i:Landroid/content/SharedPreferences;

    const v2, 0x7fffffff

    invoke-interface {v1, v0, v2}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v0

    iget-object v1, p0, Lcom/google/android/youtube/core/transfer/TransferService;->h:Lcom/google/android/youtube/core/transfer/e;

    invoke-virtual {v1, v0}, Lcom/google/android/youtube/core/transfer/e;->a(I)V

    :cond_0
    return-void
.end method


# virtual methods
.method protected abstract a()Ljava/lang/String;
.end method

.method public final a(I)V
    .locals 4

    iget-object v0, p0, Lcom/google/android/youtube/core/transfer/TransferService;->a:Lcom/google/android/youtube/core/transfer/j;

    iget-object v1, p0, Lcom/google/android/youtube/core/transfer/TransferService;->a:Lcom/google/android/youtube/core/transfer/j;

    const/4 v2, 0x2

    const/4 v3, 0x0

    invoke-virtual {v1, v2, p1, v3}, Lcom/google/android/youtube/core/transfer/j;->obtainMessage(III)Landroid/os/Message;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/youtube/core/transfer/j;->sendMessage(Landroid/os/Message;)Z

    return-void
.end method

.method public final a(Ljava/util/Map;)V
    .locals 3

    iget-object v0, p0, Lcom/google/android/youtube/core/transfer/TransferService;->a:Lcom/google/android/youtube/core/transfer/j;

    iget-object v1, p0, Lcom/google/android/youtube/core/transfer/TransferService;->a:Lcom/google/android/youtube/core/transfer/j;

    const/4 v2, 0x1

    invoke-virtual {v1, v2, p1}, Lcom/google/android/youtube/core/transfer/j;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/youtube/core/transfer/j;->sendMessage(Landroid/os/Message;)Z

    return-void
.end method

.method protected final a(Lcom/google/android/youtube/core/transfer/i;)Z
    .locals 2

    iget-object v0, p0, Lcom/google/android/youtube/core/transfer/TransferService;->e:Ljava/util/Set;

    invoke-static {p1}, Lcom/google/android/youtube/core/utils/s;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    iget-boolean v0, p0, Lcom/google/android/youtube/core/transfer/TransferService;->c:Z

    if-eqz v0, :cond_0

    invoke-interface {p1}, Lcom/google/android/youtube/core/transfer/i;->t_()V

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected abstract b()Ljava/lang/String;
.end method

.method public final b(Lcom/google/android/youtube/core/transfer/Transfer;)V
    .locals 3

    iget-object v0, p0, Lcom/google/android/youtube/core/transfer/TransferService;->a:Lcom/google/android/youtube/core/transfer/j;

    iget-object v1, p0, Lcom/google/android/youtube/core/transfer/TransferService;->a:Lcom/google/android/youtube/core/transfer/j;

    const/4 v2, 0x3

    invoke-virtual {v1, v2, p1}, Lcom/google/android/youtube/core/transfer/j;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/youtube/core/transfer/j;->sendMessage(Landroid/os/Message;)Z

    return-void
.end method

.method protected final b(Lcom/google/android/youtube/core/transfer/i;)Z
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/core/transfer/TransferService;->e:Ljava/util/Set;

    invoke-interface {v0, p1}, Ljava/util/Set;->remove(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method protected abstract c()Ljava/lang/String;
.end method

.method public final c(Lcom/google/android/youtube/core/transfer/Transfer;)V
    .locals 3

    iget-object v0, p0, Lcom/google/android/youtube/core/transfer/TransferService;->a:Lcom/google/android/youtube/core/transfer/j;

    iget-object v1, p0, Lcom/google/android/youtube/core/transfer/TransferService;->a:Lcom/google/android/youtube/core/transfer/j;

    const/4 v2, 0x4

    invoke-virtual {v1, v2, p1}, Lcom/google/android/youtube/core/transfer/j;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/youtube/core/transfer/j;->sendMessage(Landroid/os/Message;)Z

    return-void
.end method

.method protected abstract d()Ljava/lang/String;
.end method

.method public final d(Lcom/google/android/youtube/core/transfer/Transfer;)V
    .locals 3

    iget-object v0, p0, Lcom/google/android/youtube/core/transfer/TransferService;->a:Lcom/google/android/youtube/core/transfer/j;

    iget-object v1, p0, Lcom/google/android/youtube/core/transfer/TransferService;->a:Lcom/google/android/youtube/core/transfer/j;

    const/4 v2, 0x5

    invoke-virtual {v1, v2, p1}, Lcom/google/android/youtube/core/transfer/j;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/youtube/core/transfer/j;->sendMessage(Landroid/os/Message;)Z

    return-void
.end method

.method protected abstract e()Ljava/lang/String;
.end method

.method public final e(Lcom/google/android/youtube/core/transfer/Transfer;)V
    .locals 3

    iget-object v0, p0, Lcom/google/android/youtube/core/transfer/TransferService;->a:Lcom/google/android/youtube/core/transfer/j;

    iget-object v1, p0, Lcom/google/android/youtube/core/transfer/TransferService;->a:Lcom/google/android/youtube/core/transfer/j;

    const/4 v2, 0x6

    invoke-virtual {v1, v2, p1}, Lcom/google/android/youtube/core/transfer/j;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/youtube/core/transfer/j;->sendMessage(Landroid/os/Message;)Z

    return-void
.end method

.method protected final f()Ljava/util/Map;
    .locals 2

    new-instance v0, Ljava/util/HashMap;

    iget-object v1, p0, Lcom/google/android/youtube/core/transfer/TransferService;->b:Ljava/util/Map;

    invoke-direct {v0, v1}, Ljava/util/HashMap;-><init>(Ljava/util/Map;)V

    return-object v0
.end method

.method public final f(Lcom/google/android/youtube/core/transfer/Transfer;)V
    .locals 3

    iget-object v0, p0, Lcom/google/android/youtube/core/transfer/TransferService;->a:Lcom/google/android/youtube/core/transfer/j;

    iget-object v1, p0, Lcom/google/android/youtube/core/transfer/TransferService;->a:Lcom/google/android/youtube/core/transfer/j;

    const/4 v2, 0x7

    invoke-virtual {v1, v2, p1}, Lcom/google/android/youtube/core/transfer/j;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/youtube/core/transfer/j;->sendMessage(Landroid/os/Message;)Z

    return-void
.end method

.method protected final g()Lcom/google/android/youtube/core/transfer/e;
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/core/transfer/TransferService;->h:Lcom/google/android/youtube/core/transfer/e;

    return-object v0
.end method

.method public onBind(Landroid/content/Intent;)Landroid/os/IBinder;
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/core/transfer/TransferService;->d:Lcom/google/android/youtube/core/transfer/l;

    return-object v0
.end method

.method public onCreate()V
    .locals 2

    invoke-super {p0}, Landroid/app/Service;->onCreate()V

    invoke-static {}, Lcom/google/android/youtube/core/L;->b()V

    new-instance v0, Lcom/google/android/youtube/core/transfer/j;

    invoke-direct {v0, p0}, Lcom/google/android/youtube/core/transfer/j;-><init>(Lcom/google/android/youtube/core/transfer/TransferService;)V

    iput-object v0, p0, Lcom/google/android/youtube/core/transfer/TransferService;->a:Lcom/google/android/youtube/core/transfer/j;

    new-instance v0, Lcom/google/android/youtube/core/transfer/TransfersExecutor;

    invoke-virtual {p0}, Lcom/google/android/youtube/core/transfer/TransferService;->a()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, p0, p0, p0, v1}, Lcom/google/android/youtube/core/transfer/TransfersExecutor;-><init>(Landroid/content/Context;Lcom/google/android/youtube/core/transfer/s;Lcom/google/android/youtube/core/transfer/w;Ljava/lang/String;)V

    iput-object v0, p0, Lcom/google/android/youtube/core/transfer/TransferService;->f:Lcom/google/android/youtube/core/transfer/TransfersExecutor;

    new-instance v0, Ljava/util/concurrent/CopyOnWriteArraySet;

    invoke-direct {v0}, Ljava/util/concurrent/CopyOnWriteArraySet;-><init>()V

    iput-object v0, p0, Lcom/google/android/youtube/core/transfer/TransferService;->e:Ljava/util/Set;

    new-instance v0, Lcom/google/android/youtube/core/transfer/l;

    invoke-direct {v0, p0}, Lcom/google/android/youtube/core/transfer/l;-><init>(Lcom/google/android/youtube/core/transfer/TransferService;)V

    iput-object v0, p0, Lcom/google/android/youtube/core/transfer/TransferService;->d:Lcom/google/android/youtube/core/transfer/l;

    invoke-virtual {p0}, Lcom/google/android/youtube/core/transfer/TransferService;->e()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    new-instance v0, Lcom/google/android/youtube/core/transfer/e;

    invoke-direct {v0}, Lcom/google/android/youtube/core/transfer/e;-><init>()V

    iput-object v0, p0, Lcom/google/android/youtube/core/transfer/TransferService;->h:Lcom/google/android/youtube/core/transfer/e;

    :cond_0
    invoke-virtual {p0}, Lcom/google/android/youtube/core/transfer/TransferService;->b()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-virtual {p0}, Lcom/google/android/youtube/core/transfer/TransferService;->c()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-virtual {p0}, Lcom/google/android/youtube/core/transfer/TransferService;->d()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-virtual {p0}, Lcom/google/android/youtube/core/transfer/TransferService;->e()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_2

    :cond_1
    invoke-virtual {p0}, Lcom/google/android/youtube/core/transfer/TransferService;->getApplication()Landroid/app/Application;

    move-result-object v0

    check-cast v0, Lcom/google/android/youtube/core/BaseApplication;

    invoke-virtual {v0}, Lcom/google/android/youtube/core/BaseApplication;->S()Landroid/content/SharedPreferences;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/youtube/core/transfer/TransferService;->i:Landroid/content/SharedPreferences;

    new-instance v0, Lcom/google/android/youtube/core/transfer/k;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/google/android/youtube/core/transfer/k;-><init>(Lcom/google/android/youtube/core/transfer/TransferService;B)V

    iput-object v0, p0, Lcom/google/android/youtube/core/transfer/TransferService;->j:Landroid/content/SharedPreferences$OnSharedPreferenceChangeListener;

    iget-object v0, p0, Lcom/google/android/youtube/core/transfer/TransferService;->i:Landroid/content/SharedPreferences;

    iget-object v1, p0, Lcom/google/android/youtube/core/transfer/TransferService;->j:Landroid/content/SharedPreferences$OnSharedPreferenceChangeListener;

    invoke-interface {v0, v1}, Landroid/content/SharedPreferences;->registerOnSharedPreferenceChangeListener(Landroid/content/SharedPreferences$OnSharedPreferenceChangeListener;)V

    :cond_2
    invoke-direct {p0}, Lcom/google/android/youtube/core/transfer/TransferService;->h()V

    invoke-direct {p0}, Lcom/google/android/youtube/core/transfer/TransferService;->i()V

    invoke-direct {p0}, Lcom/google/android/youtube/core/transfer/TransferService;->j()V

    invoke-direct {p0}, Lcom/google/android/youtube/core/transfer/TransferService;->k()V

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-static {p0, v0}, Lcom/google/android/youtube/core/transfer/TransferService;->a(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/android/youtube/core/transfer/TransferService;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;

    iget-object v0, p0, Lcom/google/android/youtube/core/transfer/TransferService;->f:Lcom/google/android/youtube/core/transfer/TransfersExecutor;

    invoke-virtual {v0}, Lcom/google/android/youtube/core/transfer/TransfersExecutor;->a()I

    move-result v0

    iput v0, p0, Lcom/google/android/youtube/core/transfer/TransferService;->g:I

    return-void
.end method

.method public onDestroy()V
    .locals 2

    iget-object v0, p0, Lcom/google/android/youtube/core/transfer/TransferService;->j:Landroid/content/SharedPreferences$OnSharedPreferenceChangeListener;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/youtube/core/transfer/TransferService;->i:Landroid/content/SharedPreferences;

    iget-object v1, p0, Lcom/google/android/youtube/core/transfer/TransferService;->j:Landroid/content/SharedPreferences$OnSharedPreferenceChangeListener;

    invoke-interface {v0, v1}, Landroid/content/SharedPreferences;->unregisterOnSharedPreferenceChangeListener(Landroid/content/SharedPreferences$OnSharedPreferenceChangeListener;)V

    :cond_0
    iget-object v0, p0, Lcom/google/android/youtube/core/transfer/TransferService;->f:Lcom/google/android/youtube/core/transfer/TransfersExecutor;

    invoke-virtual {v0}, Lcom/google/android/youtube/core/transfer/TransfersExecutor;->b()V

    invoke-super {p0}, Landroid/app/Service;->onDestroy()V

    return-void
.end method

.method public onStartCommand(Landroid/content/Intent;II)I
    .locals 1

    const/4 v0, 0x1

    return v0
.end method
