.class final Lcom/google/android/youtube/core/a/n;
.super Lcom/google/android/youtube/core/a/m;
.source "SourceFile"


# instance fields
.field private final a:Landroid/view/View;

.field private final c:Z


# direct methods
.method public constructor <init>(Landroid/view/View;Z)V
    .locals 1

    sget-object v0, Lcom/google/android/youtube/core/a/e;->b:Lcom/google/android/youtube/core/a/g;

    invoke-direct {p0, v0}, Lcom/google/android/youtube/core/a/m;-><init>(Lcom/google/android/youtube/core/a/g;)V

    const-string v0, "view cannot be null"

    invoke-static {p1, v0}, Lcom/google/android/youtube/core/utils/s;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    iput-object v0, p0, Lcom/google/android/youtube/core/a/n;->a:Landroid/view/View;

    iput-boolean p2, p0, Lcom/google/android/youtube/core/a/n;->c:Z

    return-void
.end method


# virtual methods
.method public final a(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 3

    if-nez p1, :cond_0

    const/4 v0, 0x1

    :goto_0
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "position must be zero and was "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/youtube/core/utils/s;->a(ZLjava/lang/Object;)V

    iget-object v0, p0, Lcom/google/android/youtube/core/a/n;->a:Landroid/view/View;

    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final d(I)Z
    .locals 3

    if-nez p1, :cond_0

    const/4 v0, 0x1

    :goto_0
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "position must be zero and was "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/youtube/core/utils/s;->a(ZLjava/lang/Object;)V

    iget-boolean v0, p0, Lcom/google/android/youtube/core/a/n;->c:Z

    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
