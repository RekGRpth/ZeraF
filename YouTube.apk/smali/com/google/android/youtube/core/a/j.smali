.class public final Lcom/google/android/youtube/core/a/j;
.super Landroid/widget/BaseAdapter;
.source "SourceFile"

# interfaces
.implements Landroid/widget/WrapperListAdapter;


# instance fields
.field private final a:Lcom/google/android/youtube/core/a/a;

.field private final b:Landroid/view/View;

.field private final c:Z

.field private d:Z


# direct methods
.method private constructor <init>(Lcom/google/android/youtube/core/a/a;ZLandroid/view/View;Z)V
    .locals 2

    const/4 v1, 0x0

    invoke-direct {p0}, Landroid/widget/BaseAdapter;-><init>()V

    const-string v0, "target cannot be null"

    invoke-static {p1, v0}, Lcom/google/android/youtube/core/utils/s;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/youtube/core/a/a;

    iput-object v0, p0, Lcom/google/android/youtube/core/a/j;->a:Lcom/google/android/youtube/core/a/a;

    const-string v0, "paddingView cannot be null"

    invoke-static {p3, v0}, Lcom/google/android/youtube/core/utils/s;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    iput-object v0, p0, Lcom/google/android/youtube/core/a/j;->b:Landroid/view/View;

    iput-boolean v1, p0, Lcom/google/android/youtube/core/a/j;->c:Z

    new-instance v0, Lcom/google/android/youtube/core/a/k;

    invoke-direct {v0, p0, v1}, Lcom/google/android/youtube/core/a/k;-><init>(Lcom/google/android/youtube/core/a/j;B)V

    invoke-virtual {p1, v0}, Lcom/google/android/youtube/core/a/a;->registerDataSetObserver(Landroid/database/DataSetObserver;)V

    if-eqz p4, :cond_0

    invoke-virtual {p0}, Lcom/google/android/youtube/core/a/j;->b()V

    :goto_0
    return-void

    :cond_0
    invoke-virtual {p0}, Lcom/google/android/youtube/core/a/j;->c()V

    goto :goto_0
.end method

.method private a(I)I
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/youtube/core/a/j;->d:Z

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/google/android/youtube/core/a/j;->c:Z

    if-eqz v0, :cond_0

    add-int/lit8 p1, p1, -0x1

    :cond_0
    return p1
.end method

.method public static a(Lcom/google/android/youtube/core/a/a;Landroid/view/View;Z)Lcom/google/android/youtube/core/a/j;
    .locals 2

    const/4 v1, 0x0

    new-instance v0, Lcom/google/android/youtube/core/a/j;

    invoke-direct {v0, p0, v1, p1, v1}, Lcom/google/android/youtube/core/a/j;-><init>(Lcom/google/android/youtube/core/a/a;ZLandroid/view/View;Z)V

    return-object v0
.end method

.method private b(I)Z
    .locals 2

    const/4 v0, 0x0

    iget-boolean v1, p0, Lcom/google/android/youtube/core/a/j;->d:Z

    if-eqz v1, :cond_0

    iget-boolean v1, p0, Lcom/google/android/youtube/core/a/j;->c:Z

    if-eqz v1, :cond_1

    move v1, v0

    :goto_0
    if-ne p1, v1, :cond_0

    const/4 v0, 0x1

    :cond_0
    return v0

    :cond_1
    iget-object v1, p0, Lcom/google/android/youtube/core/a/j;->a:Lcom/google/android/youtube/core/a/a;

    invoke-virtual {v1}, Lcom/google/android/youtube/core/a/a;->getCount()I

    move-result v1

    goto :goto_0
.end method


# virtual methods
.method public final a()V
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/core/a/j;->a:Lcom/google/android/youtube/core/a/a;

    invoke-virtual {v0}, Lcom/google/android/youtube/core/a/a;->a()V

    return-void
.end method

.method public final a(ILjava/lang/Iterable;)V
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/core/a/j;->a:Lcom/google/android/youtube/core/a/a;

    invoke-virtual {v0, p1, p2}, Lcom/google/android/youtube/core/a/a;->a(ILjava/lang/Iterable;)V

    return-void
.end method

.method public final a(ILjava/lang/Object;)V
    .locals 2

    iget-object v0, p0, Lcom/google/android/youtube/core/a/j;->a:Lcom/google/android/youtube/core/a/a;

    const/4 v1, 0x0

    invoke-virtual {v0, v1, p2}, Lcom/google/android/youtube/core/a/a;->c(ILjava/lang/Object;)V

    return-void
.end method

.method public final a(Ljava/lang/Iterable;)V
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/core/a/j;->a:Lcom/google/android/youtube/core/a/a;

    invoke-virtual {v0, p1}, Lcom/google/android/youtube/core/a/a;->b(Ljava/lang/Iterable;)V

    return-void
.end method

.method public final a(Ljava/util/Collection;)V
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/core/a/j;->a:Lcom/google/android/youtube/core/a/a;

    invoke-virtual {v0, p1}, Lcom/google/android/youtube/core/a/a;->a(Ljava/util/Collection;)V

    return-void
.end method

.method public final areAllItemsEnabled()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/youtube/core/a/j;->d:Z

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/youtube/core/a/j;->a:Lcom/google/android/youtube/core/a/a;

    invoke-virtual {v0}, Lcom/google/android/youtube/core/a/a;->areAllItemsEnabled()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final b()V
    .locals 2

    iget-boolean v0, p0, Lcom/google/android/youtube/core/a/j;->d:Z

    const/4 v1, 0x1

    iput-boolean v1, p0, Lcom/google/android/youtube/core/a/j;->d:Z

    if-nez v0, :cond_0

    invoke-virtual {p0}, Lcom/google/android/youtube/core/a/j;->notifyDataSetChanged()V

    :cond_0
    return-void
.end method

.method public final c()V
    .locals 2

    iget-boolean v0, p0, Lcom/google/android/youtube/core/a/j;->d:Z

    const/4 v1, 0x0

    iput-boolean v1, p0, Lcom/google/android/youtube/core/a/j;->d:Z

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/google/android/youtube/core/a/j;->notifyDataSetChanged()V

    :cond_0
    return-void
.end method

.method public final getCount()I
    .locals 2

    iget-object v0, p0, Lcom/google/android/youtube/core/a/j;->a:Lcom/google/android/youtube/core/a/a;

    invoke-virtual {v0}, Lcom/google/android/youtube/core/a/a;->getCount()I

    move-result v1

    iget-boolean v0, p0, Lcom/google/android/youtube/core/a/j;->d:Z

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    add-int/2addr v0, v1

    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final getItem(I)Ljava/lang/Object;
    .locals 2

    invoke-direct {p0, p1}, Lcom/google/android/youtube/core/a/j;->b(I)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/google/android/youtube/core/a/j;->a:Lcom/google/android/youtube/core/a/a;

    invoke-direct {p0, p1}, Lcom/google/android/youtube/core/a/j;->a(I)I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/google/android/youtube/core/a/a;->getItem(I)Ljava/lang/Object;

    move-result-object v0

    goto :goto_0
.end method

.method public final getItemId(I)J
    .locals 2

    invoke-direct {p0, p1}, Lcom/google/android/youtube/core/a/j;->b(I)Z

    move-result v0

    if-eqz v0, :cond_0

    const-wide/16 v0, -0x1

    :goto_0
    return-wide v0

    :cond_0
    iget-object v0, p0, Lcom/google/android/youtube/core/a/j;->a:Lcom/google/android/youtube/core/a/a;

    invoke-direct {p0, p1}, Lcom/google/android/youtube/core/a/j;->a(I)I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/google/android/youtube/core/a/a;->getItemId(I)J

    move-result-wide v0

    goto :goto_0
.end method

.method public final getItemViewType(I)I
    .locals 2

    invoke-direct {p0, p1}, Lcom/google/android/youtube/core/a/j;->b(I)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/youtube/core/a/j;->a:Lcom/google/android/youtube/core/a/a;

    invoke-virtual {v0}, Lcom/google/android/youtube/core/a/a;->getViewTypeCount()I

    move-result v0

    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lcom/google/android/youtube/core/a/j;->a:Lcom/google/android/youtube/core/a/a;

    invoke-direct {p0, p1}, Lcom/google/android/youtube/core/a/j;->a(I)I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/google/android/youtube/core/a/a;->getItemViewType(I)I

    move-result v0

    goto :goto_0
.end method

.method public final getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 2

    invoke-direct {p0, p1}, Lcom/google/android/youtube/core/a/j;->b(I)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/youtube/core/a/j;->b:Landroid/view/View;

    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/google/android/youtube/core/a/j;->a:Lcom/google/android/youtube/core/a/a;

    invoke-direct {p0, p1}, Lcom/google/android/youtube/core/a/j;->a(I)I

    move-result v1

    invoke-virtual {v0, v1, p2, p3}, Lcom/google/android/youtube/core/a/a;->getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    goto :goto_0
.end method

.method public final getViewTypeCount()I
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/core/a/j;->a:Lcom/google/android/youtube/core/a/a;

    invoke-virtual {v0}, Lcom/google/android/youtube/core/a/a;->getViewTypeCount()I

    move-result v0

    add-int/lit8 v0, v0, 0x1

    return v0
.end method

.method public final getWrappedAdapter()Landroid/widget/ListAdapter;
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/core/a/j;->a:Lcom/google/android/youtube/core/a/a;

    return-object v0
.end method

.method public final hasStableIds()Z
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/core/a/j;->a:Lcom/google/android/youtube/core/a/a;

    invoke-virtual {v0}, Lcom/google/android/youtube/core/a/a;->hasStableIds()Z

    move-result v0

    return v0
.end method

.method public final isEnabled(I)Z
    .locals 2

    invoke-direct {p0, p1}, Lcom/google/android/youtube/core/a/j;->b(I)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lcom/google/android/youtube/core/a/j;->a:Lcom/google/android/youtube/core/a/a;

    invoke-direct {p0, p1}, Lcom/google/android/youtube/core/a/j;->a(I)I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/google/android/youtube/core/a/a;->isEnabled(I)Z

    move-result v0

    goto :goto_0
.end method
