.class public final Lcom/google/android/youtube/core/a/i;
.super Landroid/widget/BaseAdapter;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/youtube/core/a/f;


# instance fields
.field private final a:Ljava/util/Map;

.field private final b:Z

.field private c:Lcom/google/android/youtube/core/a/e;

.field private d:Z


# direct methods
.method public constructor <init>()V
    .locals 1

    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/google/android/youtube/core/a/i;-><init>(Z)V

    return-void
.end method

.method private constructor <init>(Z)V
    .locals 3

    invoke-direct {p0}, Landroid/widget/BaseAdapter;-><init>()V

    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/google/android/youtube/core/a/i;->a:Ljava/util/Map;

    iget-object v0, p0, Lcom/google/android/youtube/core/a/i;->a:Ljava/util/Map;

    sget-object v1, Lcom/google/android/youtube/core/a/e;->b:Lcom/google/android/youtube/core/a/g;

    const/4 v2, -0x1

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/youtube/core/a/i;->b:Z

    return-void
.end method

.method private a(Ljava/util/Set;)V
    .locals 5

    const/4 v1, 0x1

    iget-boolean v0, p0, Lcom/google/android/youtube/core/a/i;->d:Z

    if-nez v0, :cond_1

    move v0, v1

    :goto_0
    const-string v2, "setViewTypes called after view types are finalized"

    invoke-static {v0, v2}, Lcom/google/android/youtube/core/utils/s;->b(ZLjava/lang/Object;)V

    invoke-interface {p1}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_0
    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/youtube/core/a/g;

    if-eqz v0, :cond_0

    iget-object v3, p0, Lcom/google/android/youtube/core/a/i;->a:Ljava/util/Map;

    invoke-interface {v3, v0}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_0

    iget-object v3, p0, Lcom/google/android/youtube/core/a/i;->a:Ljava/util/Map;

    iget-object v4, p0, Lcom/google/android/youtube/core/a/i;->a:Ljava/util/Map;

    invoke-interface {v4}, Ljava/util/Map;->size()I

    move-result v4

    add-int/lit8 v4, v4, -0x1

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-interface {v3, v0, v4}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_1

    :cond_1
    const/4 v0, 0x0

    goto :goto_0

    :cond_2
    iput-boolean v1, p0, Lcom/google/android/youtube/core/a/i;->d:Z

    return-void
.end method


# virtual methods
.method public final a(Lcom/google/android/youtube/core/a/e;)V
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/core/a/i;->c:Lcom/google/android/youtube/core/a/e;

    if-ne v0, p1, :cond_0

    invoke-virtual {p0}, Lcom/google/android/youtube/core/a/i;->notifyDataSetChanged()V

    :cond_0
    return-void
.end method

.method public final areAllItemsEnabled()Z
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public final b(Lcom/google/android/youtube/core/a/e;)V
    .locals 2

    if-eqz p1, :cond_1

    iget-boolean v0, p0, Lcom/google/android/youtube/core/a/i;->b:Z

    if-eqz v0, :cond_0

    invoke-virtual {p1}, Lcom/google/android/youtube/core/a/e;->s_()Z

    move-result v0

    if-eqz v0, :cond_3

    :cond_0
    const/4 v0, 0x1

    :goto_0
    const-string v1, "outline must have stable IDs as required by this OutlinerAdapter"

    invoke-static {v0, v1}, Lcom/google/android/youtube/core/utils/s;->a(ZLjava/lang/Object;)V

    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    invoke-virtual {p1, v0}, Lcom/google/android/youtube/core/a/e;->a(Ljava/util/Set;)V

    iget-boolean v1, p0, Lcom/google/android/youtube/core/a/i;->d:Z

    if-eqz v1, :cond_4

    iget-object v1, p0, Lcom/google/android/youtube/core/a/i;->a:Ljava/util/Map;

    invoke-interface {v1}, Ljava/util/Map;->keySet()Ljava/util/Set;

    move-result-object v1

    invoke-interface {v1, v0}, Ljava/util/Set;->containsAll(Ljava/util/Collection;)Z

    move-result v0

    const-string v1, "outline must not use unknown view types"

    invoke-static {v0, v1}, Lcom/google/android/youtube/core/utils/s;->a(ZLjava/lang/Object;)V

    :goto_1
    invoke-virtual {p1, p0}, Lcom/google/android/youtube/core/a/e;->a(Lcom/google/android/youtube/core/a/f;)V

    :cond_1
    iget-object v0, p0, Lcom/google/android/youtube/core/a/i;->c:Lcom/google/android/youtube/core/a/e;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/google/android/youtube/core/a/i;->c:Lcom/google/android/youtube/core/a/e;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/google/android/youtube/core/a/e;->a(Lcom/google/android/youtube/core/a/f;)V

    :cond_2
    iput-object p1, p0, Lcom/google/android/youtube/core/a/i;->c:Lcom/google/android/youtube/core/a/e;

    invoke-virtual {p0}, Lcom/google/android/youtube/core/a/i;->notifyDataSetChanged()V

    return-void

    :cond_3
    const/4 v0, 0x0

    goto :goto_0

    :cond_4
    invoke-direct {p0, v0}, Lcom/google/android/youtube/core/a/i;->a(Ljava/util/Set;)V

    goto :goto_1
.end method

.method public final getCount()I
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/core/a/i;->c:Lcom/google/android/youtube/core/a/e;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lcom/google/android/youtube/core/a/i;->c:Lcom/google/android/youtube/core/a/e;

    invoke-virtual {v0}, Lcom/google/android/youtube/core/a/e;->n()I

    move-result v0

    goto :goto_0
.end method

.method public final getItem(I)Ljava/lang/Object;
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/core/a/i;->c:Lcom/google/android/youtube/core/a/e;

    invoke-virtual {v0, p1}, Lcom/google/android/youtube/core/a/e;->b(I)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public final getItemId(I)J
    .locals 2

    invoke-virtual {p0}, Lcom/google/android/youtube/core/a/i;->getCount()I

    move-result v0

    if-lt p1, v0, :cond_0

    const-wide/16 v0, -0x1

    :goto_0
    return-wide v0

    :cond_0
    iget-object v0, p0, Lcom/google/android/youtube/core/a/i;->c:Lcom/google/android/youtube/core/a/e;

    invoke-virtual {v0, p1}, Lcom/google/android/youtube/core/a/e;->c(I)J

    move-result-wide v0

    goto :goto_0
.end method

.method public final getItemViewType(I)I
    .locals 4

    iget-object v0, p0, Lcom/google/android/youtube/core/a/i;->c:Lcom/google/android/youtube/core/a/e;

    invoke-virtual {v0, p1}, Lcom/google/android/youtube/core/a/e;->a(I)Lcom/google/android/youtube/core/a/g;

    move-result-object v1

    iget-object v0, p0, Lcom/google/android/youtube/core/a/i;->a:Ljava/util/Map;

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/IllegalStateException;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Unknown view type \""

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "\" at position "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    return v0
.end method

.method public final getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/core/a/i;->c:Lcom/google/android/youtube/core/a/e;

    invoke-virtual {v0, p1, p2, p3}, Lcom/google/android/youtube/core/a/e;->a(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    return-object v0
.end method

.method public final getViewTypeCount()I
    .locals 2

    iget-boolean v0, p0, Lcom/google/android/youtube/core/a/i;->d:Z

    const-string v1, "OutlinerAdapter used before view types are known"

    invoke-static {v0, v1}, Lcom/google/android/youtube/core/utils/s;->b(ZLjava/lang/Object;)V

    const/4 v0, 0x1

    iget-object v1, p0, Lcom/google/android/youtube/core/a/i;->a:Ljava/util/Map;

    invoke-interface {v1}, Ljava/util/Map;->size()I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    invoke-static {v0, v1}, Ljava/lang/Math;->max(II)I

    move-result v0

    return v0
.end method

.method public final hasStableIds()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/youtube/core/a/i;->b:Z

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/youtube/core/a/i;->c:Lcom/google/android/youtube/core/a/e;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/youtube/core/a/i;->c:Lcom/google/android/youtube/core/a/e;

    invoke-virtual {v0}, Lcom/google/android/youtube/core/a/e;->s_()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final isEnabled(I)Z
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/core/a/i;->c:Lcom/google/android/youtube/core/a/e;

    invoke-virtual {v0, p1}, Lcom/google/android/youtube/core/a/e;->d(I)Z

    move-result v0

    return v0
.end method
