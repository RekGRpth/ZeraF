.class public final Lcom/google/android/youtube/core/v14/SafeTexturePlayerSurface;
.super Landroid/view/ViewGroup;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/youtube/core/player/af;


# instance fields
.field private a:Lcom/google/android/youtube/core/player/af;

.field private b:Z

.field private c:Lcom/google/android/youtube/core/player/ag;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0

    invoke-direct {p0, p1}, Landroid/view/ViewGroup;-><init>(Landroid/content/Context;)V

    return-void
.end method

.method private a()V
    .locals 2

    iget-object v0, p0, Lcom/google/android/youtube/core/v14/SafeTexturePlayerSurface;->a:Lcom/google/android/youtube/core/player/af;

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "PlayerSurface method called before surface created"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    return-void
.end method


# virtual methods
.method public final a(Lcom/google/android/youtube/core/player/ad;)V
    .locals 1

    invoke-direct {p0}, Lcom/google/android/youtube/core/v14/SafeTexturePlayerSurface;->a()V

    iget-object v0, p0, Lcom/google/android/youtube/core/v14/SafeTexturePlayerSurface;->a:Lcom/google/android/youtube/core/player/af;

    invoke-interface {v0, p1}, Lcom/google/android/youtube/core/player/af;->a(Lcom/google/android/youtube/core/player/ad;)V

    return-void
.end method

.method public final a(Z)Z
    .locals 1

    invoke-direct {p0}, Lcom/google/android/youtube/core/v14/SafeTexturePlayerSurface;->a()V

    iget-object v0, p0, Lcom/google/android/youtube/core/v14/SafeTexturePlayerSurface;->a:Lcom/google/android/youtube/core/player/af;

    invoke-interface {v0, p1}, Lcom/google/android/youtube/core/player/af;->a(Z)Z

    move-result v0

    return v0
.end method

.method public final b()V
    .locals 1

    invoke-direct {p0}, Lcom/google/android/youtube/core/v14/SafeTexturePlayerSurface;->a()V

    iget-object v0, p0, Lcom/google/android/youtube/core/v14/SafeTexturePlayerSurface;->a:Lcom/google/android/youtube/core/player/af;

    invoke-interface {v0}, Lcom/google/android/youtube/core/player/af;->b()V

    return-void
.end method

.method public final c()V
    .locals 1

    invoke-direct {p0}, Lcom/google/android/youtube/core/v14/SafeTexturePlayerSurface;->a()V

    iget-object v0, p0, Lcom/google/android/youtube/core/v14/SafeTexturePlayerSurface;->a:Lcom/google/android/youtube/core/player/af;

    invoke-interface {v0}, Lcom/google/android/youtube/core/player/af;->c()V

    return-void
.end method

.method public final d()V
    .locals 0

    return-void
.end method

.method public final e()V
    .locals 1

    invoke-direct {p0}, Lcom/google/android/youtube/core/v14/SafeTexturePlayerSurface;->a()V

    iget-object v0, p0, Lcom/google/android/youtube/core/v14/SafeTexturePlayerSurface;->a:Lcom/google/android/youtube/core/player/af;

    invoke-interface {v0}, Lcom/google/android/youtube/core/player/af;->e()V

    return-void
.end method

.method public final f()Z
    .locals 1

    invoke-direct {p0}, Lcom/google/android/youtube/core/v14/SafeTexturePlayerSurface;->a()V

    iget-object v0, p0, Lcom/google/android/youtube/core/v14/SafeTexturePlayerSurface;->a:Lcom/google/android/youtube/core/player/af;

    invoke-interface {v0}, Lcom/google/android/youtube/core/player/af;->f()Z

    move-result v0

    return v0
.end method

.method protected final onAttachedToWindow()V
    .locals 2

    invoke-super {p0}, Landroid/view/ViewGroup;->onAttachedToWindow()V

    invoke-virtual {p0}, Lcom/google/android/youtube/core/v14/SafeTexturePlayerSurface;->isHardwareAccelerated()Z

    move-result v0

    if-eqz v0, :cond_1

    new-instance v0, Lcom/google/android/youtube/core/v14/TexturePlayerSurface;

    invoke-virtual {p0}, Lcom/google/android/youtube/core/v14/SafeTexturePlayerSurface;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/google/android/youtube/core/v14/TexturePlayerSurface;-><init>(Landroid/content/Context;)V

    invoke-virtual {p0, v0}, Lcom/google/android/youtube/core/v14/SafeTexturePlayerSurface;->addView(Landroid/view/View;)V

    iput-object v0, p0, Lcom/google/android/youtube/core/v14/SafeTexturePlayerSurface;->a:Lcom/google/android/youtube/core/player/af;

    :goto_0
    iget-boolean v0, p0, Lcom/google/android/youtube/core/v14/SafeTexturePlayerSurface;->b:Z

    if-eqz v0, :cond_0

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/youtube/core/v14/SafeTexturePlayerSurface;->b:Z

    iget-object v0, p0, Lcom/google/android/youtube/core/v14/SafeTexturePlayerSurface;->a:Lcom/google/android/youtube/core/player/af;

    iget-object v1, p0, Lcom/google/android/youtube/core/v14/SafeTexturePlayerSurface;->c:Lcom/google/android/youtube/core/player/ag;

    invoke-interface {v0, v1}, Lcom/google/android/youtube/core/player/af;->setListener(Lcom/google/android/youtube/core/player/ag;)V

    :cond_0
    return-void

    :cond_1
    new-instance v0, Lcom/google/android/youtube/core/player/DefaultPlayerSurface;

    invoke-virtual {p0}, Lcom/google/android/youtube/core/v14/SafeTexturePlayerSurface;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/google/android/youtube/core/player/DefaultPlayerSurface;-><init>(Landroid/content/Context;)V

    invoke-virtual {p0, v0}, Lcom/google/android/youtube/core/v14/SafeTexturePlayerSurface;->addView(Landroid/view/View;)V

    iput-object v0, p0, Lcom/google/android/youtube/core/v14/SafeTexturePlayerSurface;->a:Lcom/google/android/youtube/core/player/af;

    goto :goto_0
.end method

.method protected final onLayout(ZIIII)V
    .locals 4

    const/4 v3, 0x0

    invoke-virtual {p0}, Lcom/google/android/youtube/core/v14/SafeTexturePlayerSurface;->getChildCount()I

    move-result v0

    if-lez v0, :cond_0

    invoke-virtual {p0, v3}, Lcom/google/android/youtube/core/v14/SafeTexturePlayerSurface;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/View;->getMeasuredWidth()I

    move-result v1

    invoke-virtual {v0}, Landroid/view/View;->getMeasuredHeight()I

    move-result v2

    invoke-virtual {v0, v3, v3, v1, v2}, Landroid/view/View;->layout(IIII)V

    :cond_0
    return-void
.end method

.method protected final onMeasure(II)V
    .locals 2

    const/4 v1, 0x0

    invoke-virtual {p0}, Lcom/google/android/youtube/core/v14/SafeTexturePlayerSurface;->getChildCount()I

    move-result v0

    if-lez v0, :cond_0

    invoke-virtual {p0, v1}, Lcom/google/android/youtube/core/v14/SafeTexturePlayerSurface;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, p1, p2}, Landroid/view/View;->measure(II)V

    invoke-virtual {v0}, Landroid/view/View;->getMeasuredWidth()I

    move-result v1

    invoke-virtual {v0}, Landroid/view/View;->getMeasuredHeight()I

    move-result v0

    invoke-virtual {p0, v1, v0}, Lcom/google/android/youtube/core/v14/SafeTexturePlayerSurface;->setMeasuredDimension(II)V

    :goto_0
    return-void

    :cond_0
    invoke-virtual {p0, v1, v1}, Lcom/google/android/youtube/core/v14/SafeTexturePlayerSurface;->setMeasuredDimension(II)V

    goto :goto_0
.end method

.method public final setListener(Lcom/google/android/youtube/core/player/ag;)V
    .locals 1

    iput-object p1, p0, Lcom/google/android/youtube/core/v14/SafeTexturePlayerSurface;->c:Lcom/google/android/youtube/core/player/ag;

    iget-object v0, p0, Lcom/google/android/youtube/core/v14/SafeTexturePlayerSurface;->a:Lcom/google/android/youtube/core/player/af;

    if-eqz v0, :cond_0

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/youtube/core/v14/SafeTexturePlayerSurface;->b:Z

    iget-object v0, p0, Lcom/google/android/youtube/core/v14/SafeTexturePlayerSurface;->a:Lcom/google/android/youtube/core/player/af;

    invoke-interface {v0, p1}, Lcom/google/android/youtube/core/player/af;->setListener(Lcom/google/android/youtube/core/player/ag;)V

    :goto_0
    return-void

    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/youtube/core/v14/SafeTexturePlayerSurface;->b:Z

    goto :goto_0
.end method

.method public final setOnLetterboxChangedListener(Lcom/google/android/youtube/core/player/ah;)V
    .locals 1

    invoke-direct {p0}, Lcom/google/android/youtube/core/v14/SafeTexturePlayerSurface;->a()V

    iget-object v0, p0, Lcom/google/android/youtube/core/v14/SafeTexturePlayerSurface;->a:Lcom/google/android/youtube/core/player/af;

    invoke-interface {v0, p1}, Lcom/google/android/youtube/core/player/af;->setOnLetterboxChangedListener(Lcom/google/android/youtube/core/player/ah;)V

    return-void
.end method

.method public final setVideoSize(II)V
    .locals 1

    invoke-direct {p0}, Lcom/google/android/youtube/core/v14/SafeTexturePlayerSurface;->a()V

    iget-object v0, p0, Lcom/google/android/youtube/core/v14/SafeTexturePlayerSurface;->a:Lcom/google/android/youtube/core/player/af;

    invoke-interface {v0, p1, p2}, Lcom/google/android/youtube/core/player/af;->setVideoSize(II)V

    return-void
.end method

.method public final setZoom(I)V
    .locals 1

    invoke-direct {p0}, Lcom/google/android/youtube/core/v14/SafeTexturePlayerSurface;->a()V

    iget-object v0, p0, Lcom/google/android/youtube/core/v14/SafeTexturePlayerSurface;->a:Lcom/google/android/youtube/core/player/af;

    invoke-interface {v0, p1}, Lcom/google/android/youtube/core/player/af;->setZoom(I)V

    return-void
.end method
