.class public abstract Lcom/google/android/youtube/core/v11/ControllerActivity;
.super Landroid/app/Activity;
.source "SourceFile"


# instance fields
.field private final a:Ljava/util/List;

.field private b:Lcom/google/android/youtube/core/v11/Controller;

.field private c:Lcom/google/android/youtube/core/v11/Controller$LifecycleState;

.field private d:Z


# direct methods
.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Landroid/app/Activity;-><init>()V

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/android/youtube/core/v11/ControllerActivity;->a:Ljava/util/List;

    sget-object v0, Lcom/google/android/youtube/core/v11/Controller$LifecycleState;->STOPPED:Lcom/google/android/youtube/core/v11/Controller$LifecycleState;

    iput-object v0, p0, Lcom/google/android/youtube/core/v11/ControllerActivity;->c:Lcom/google/android/youtube/core/v11/Controller$LifecycleState;

    return-void
.end method

.method private b(Lcom/google/android/youtube/core/v11/Controller;)V
    .locals 4

    invoke-static {p1}, Lcom/google/android/youtube/core/utils/s;->a(Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v0, p0, Lcom/google/android/youtube/core/v11/ControllerActivity;->a:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v0

    invoke-static {v0}, Lcom/google/android/youtube/core/utils/s;->a(Z)V

    iget-object v0, p0, Lcom/google/android/youtube/core/v11/ControllerActivity;->b:Lcom/google/android/youtube/core/v11/Controller;

    if-eq p1, v0, :cond_2

    iget-object v0, p0, Lcom/google/android/youtube/core/v11/ControllerActivity;->b:Lcom/google/android/youtube/core/v11/Controller;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/youtube/core/v11/ControllerActivity;->b:Lcom/google/android/youtube/core/v11/Controller;

    sget-object v1, Lcom/google/android/youtube/core/v11/Controller$LifecycleState;->STOPPED:Lcom/google/android/youtube/core/v11/Controller$LifecycleState;

    const-string v2, "controller cannot be null"

    invoke-static {v0, v2}, Lcom/google/android/youtube/core/utils/s;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const-string v2, "newState cannot be null"

    invoke-static {v1, v2}, Lcom/google/android/youtube/core/utils/s;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v2, p0, Lcom/google/android/youtube/core/v11/ControllerActivity;->a:Ljava/util/List;

    invoke-interface {v2, v0}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v2

    const-string v3, "given controller not managed by this activity"

    invoke-static {v2, v3}, Lcom/google/android/youtube/core/utils/s;->a(ZLjava/lang/Object;)V

    invoke-virtual {v0, v1}, Lcom/google/android/youtube/core/v11/Controller;->a(Lcom/google/android/youtube/core/v11/Controller$LifecycleState;)V

    :cond_0
    iput-object p1, p0, Lcom/google/android/youtube/core/v11/ControllerActivity;->b:Lcom/google/android/youtube/core/v11/Controller;

    invoke-virtual {p0}, Lcom/google/android/youtube/core/v11/ControllerActivity;->invalidateOptionsMenu()V

    invoke-virtual {p1}, Lcom/google/android/youtube/core/v11/Controller;->b()Landroid/view/View;

    move-result-object v0

    if-eqz v0, :cond_1

    invoke-virtual {p1}, Lcom/google/android/youtube/core/v11/Controller;->b()Landroid/view/View;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/android/youtube/core/v11/ControllerActivity;->setContentView(Landroid/view/View;)V

    :cond_1
    iget-object v0, p0, Lcom/google/android/youtube/core/v11/ControllerActivity;->c:Lcom/google/android/youtube/core/v11/Controller$LifecycleState;

    invoke-virtual {p1, v0}, Lcom/google/android/youtube/core/v11/Controller;->a(Lcom/google/android/youtube/core/v11/Controller$LifecycleState;)V

    :cond_2
    return-void
.end method


# virtual methods
.method protected final a(Lcom/google/android/youtube/core/v11/Controller;)I
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/core/v11/ControllerActivity;->a:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->indexOf(Ljava/lang/Object;)I

    move-result v0

    return v0
.end method

.method protected final a()Lcom/google/android/youtube/core/v11/Controller;
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/core/v11/ControllerActivity;->b:Lcom/google/android/youtube/core/v11/Controller;

    return-object v0
.end method

.method protected final a(Lcom/google/android/youtube/core/v11/Controller;Landroid/os/Bundle;)V
    .locals 3

    invoke-static {p1}, Lcom/google/android/youtube/core/utils/s;->a(Ljava/lang/Object;)Ljava/lang/Object;

    const/4 v0, 0x0

    if-eqz p2, :cond_0

    iget-object v0, p0, Lcom/google/android/youtube/core/v11/ControllerActivity;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "controller_state_"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2, v0}, Landroid/os/Bundle;->getBundle(Ljava/lang/String;)Landroid/os/Bundle;

    move-result-object v0

    :cond_0
    invoke-virtual {p0}, Lcom/google/android/youtube/core/v11/ControllerActivity;->getLayoutInflater()Landroid/view/LayoutInflater;

    move-result-object v1

    invoke-virtual {p1, v1, v0}, Lcom/google/android/youtube/core/v11/Controller;->a(Landroid/view/LayoutInflater;Landroid/os/Bundle;)V

    iget-object v0, p0, Lcom/google/android/youtube/core/v11/ControllerActivity;->a:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    iget-object v0, p0, Lcom/google/android/youtube/core/v11/ControllerActivity;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    const/4 v1, 0x1

    if-ne v0, v1, :cond_1

    invoke-direct {p0, p1}, Lcom/google/android/youtube/core/v11/ControllerActivity;->b(Lcom/google/android/youtube/core/v11/Controller;)V

    :cond_1
    return-void
.end method

.method public getActionBar()Landroid/app/ActionBar;
    .locals 1

    :try_start_0
    invoke-super {p0}, Landroid/app/Activity;->getActionBar()Landroid/app/ActionBar;
    :try_end_0
    .catch Ljava/lang/IllegalStateException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    :goto_0
    return-object v0

    :catch_0
    move-exception v0

    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected onActivityResult(IILandroid/content/Intent;)V
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/core/v11/ControllerActivity;->b:Lcom/google/android/youtube/core/v11/Controller;

    return-void
.end method

.method public final onBackPressed()V
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/core/v11/ControllerActivity;->b:Lcom/google/android/youtube/core/v11/Controller;

    invoke-super {p0}, Landroid/app/Activity;->onBackPressed()V

    return-void
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 2

    invoke-super {p0, p1}, Landroid/app/Activity;->onCreate(Landroid/os/Bundle;)V

    invoke-virtual {p0}, Lcom/google/android/youtube/core/v11/ControllerActivity;->getWindow()Landroid/view/Window;

    move-result-object v0

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/Window;->requestFeature(I)Z

    const/4 v0, 0x3

    invoke-virtual {p0, v0}, Lcom/google/android/youtube/core/v11/ControllerActivity;->setVolumeControlStream(I)V

    return-void
.end method

.method protected final onCreateDialog(I)Landroid/app/Dialog;
    .locals 1

    invoke-virtual {p0}, Lcom/google/android/youtube/core/v11/ControllerActivity;->isFinishing()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/youtube/core/v11/ControllerActivity;->b:Lcom/google/android/youtube/core/v11/Controller;

    :cond_0
    const/4 v0, 0x0

    return-object v0
.end method

.method protected final onCreateDialog(ILandroid/os/Bundle;)Landroid/app/Dialog;
    .locals 2

    const/4 v1, 0x0

    invoke-virtual {p0}, Lcom/google/android/youtube/core/v11/ControllerActivity;->isFinishing()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/youtube/core/v11/ControllerActivity;->b:Lcom/google/android/youtube/core/v11/Controller;

    :cond_0
    return-object v1
.end method

.method public onCreateOptionsMenu(Landroid/view/Menu;)Z
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/core/v11/ControllerActivity;->b:Lcom/google/android/youtube/core/v11/Controller;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/youtube/core/v11/ControllerActivity;->b:Lcom/google/android/youtube/core/v11/Controller;

    invoke-virtual {p0}, Lcom/google/android/youtube/core/v11/ControllerActivity;->getMenuInflater()Landroid/view/MenuInflater;

    :cond_0
    invoke-super {p0, p1}, Landroid/app/Activity;->onCreateOptionsMenu(Landroid/view/Menu;)Z

    const/4 v0, 0x1

    return v0
.end method

.method protected final onDestroy()V
    .locals 2

    iget-object v0, p0, Lcom/google/android/youtube/core/v11/ControllerActivity;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    goto :goto_0

    :cond_0
    invoke-super {p0}, Landroid/app/Activity;->onDestroy()V

    return-void
.end method

.method public final onKeyDown(ILandroid/view/KeyEvent;)Z
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/core/v11/ControllerActivity;->b:Lcom/google/android/youtube/core/v11/Controller;

    invoke-super {p0, p1, p2}, Landroid/app/Activity;->onKeyDown(ILandroid/view/KeyEvent;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final onKeyUp(ILandroid/view/KeyEvent;)Z
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/core/v11/ControllerActivity;->b:Lcom/google/android/youtube/core/v11/Controller;

    invoke-super {p0, p1, p2}, Landroid/app/Activity;->onKeyUp(ILandroid/view/KeyEvent;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public onOptionsItemSelected(Landroid/view/MenuItem;)Z
    .locals 2

    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v0

    const v1, 0x102002c

    if-ne v0, v1, :cond_0

    iget-boolean v0, p0, Lcom/google/android/youtube/core/v11/ControllerActivity;->d:Z

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lcom/google/android/youtube/core/v11/ControllerActivity;->b:Lcom/google/android/youtube/core/v11/Controller;

    const/4 v0, 0x0

    goto :goto_0
.end method

.method public onOptionsMenuClosed(Landroid/view/Menu;)V
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/core/v11/ControllerActivity;->b:Lcom/google/android/youtube/core/v11/Controller;

    return-void
.end method

.method protected final onPause()V
    .locals 2

    sget-object v0, Lcom/google/android/youtube/core/v11/Controller$LifecycleState;->PAUSED:Lcom/google/android/youtube/core/v11/Controller$LifecycleState;

    iput-object v0, p0, Lcom/google/android/youtube/core/v11/ControllerActivity;->c:Lcom/google/android/youtube/core/v11/Controller$LifecycleState;

    iget-object v0, p0, Lcom/google/android/youtube/core/v11/ControllerActivity;->b:Lcom/google/android/youtube/core/v11/Controller;

    sget-object v1, Lcom/google/android/youtube/core/v11/Controller$LifecycleState;->PAUSED:Lcom/google/android/youtube/core/v11/Controller$LifecycleState;

    invoke-virtual {v0, v1}, Lcom/google/android/youtube/core/v11/Controller;->a(Lcom/google/android/youtube/core/v11/Controller$LifecycleState;)V

    invoke-super {p0}, Landroid/app/Activity;->onPause()V

    return-void
.end method

.method protected final onPrepareDialog(ILandroid/app/Dialog;Landroid/os/Bundle;)V
    .locals 1

    invoke-super {p0, p1, p2, p3}, Landroid/app/Activity;->onPrepareDialog(ILandroid/app/Dialog;Landroid/os/Bundle;)V

    iget-object v0, p0, Lcom/google/android/youtube/core/v11/ControllerActivity;->b:Lcom/google/android/youtube/core/v11/Controller;

    return-void
.end method

.method public onPrepareOptionsMenu(Landroid/view/Menu;)Z
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/core/v11/ControllerActivity;->b:Lcom/google/android/youtube/core/v11/Controller;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/youtube/core/v11/ControllerActivity;->b:Lcom/google/android/youtube/core/v11/Controller;

    :cond_0
    invoke-super {p0, p1}, Landroid/app/Activity;->onPrepareOptionsMenu(Landroid/view/Menu;)Z

    const/4 v0, 0x1

    return v0
.end method

.method protected onRestoreInstanceState(Landroid/os/Bundle;)V
    .locals 5

    const/4 v2, 0x0

    move v1, v2

    :goto_0
    iget-object v0, p0, Lcom/google/android/youtube/core/v11/ControllerActivity;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_1

    iget-object v0, p0, Lcom/google/android/youtube/core/v11/ControllerActivity;->a:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/youtube/core/v11/Controller;

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "controller_state_"

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1, v3}, Landroid/os/Bundle;->getBundle(Ljava/lang/String;)Landroid/os/Bundle;

    move-result-object v3

    const-string v4, "controller_ui_state"

    invoke-virtual {v3, v4}, Landroid/os/Bundle;->getSparseParcelableArray(Ljava/lang/String;)Landroid/util/SparseArray;

    move-result-object v3

    invoke-virtual {v0}, Lcom/google/android/youtube/core/v11/Controller;->b()Landroid/view/View;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {v0, v3}, Landroid/view/View;->restoreHierarchyState(Landroid/util/SparseArray;)V

    :cond_0
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    :cond_1
    const-string v0, "selected_controller_index"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v0

    if-ltz v0, :cond_2

    iget-object v1, p0, Lcom/google/android/youtube/core/v11/ControllerActivity;->a:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    if-ge v0, v1, :cond_2

    const/4 v2, 0x1

    :cond_2
    const-string v1, "controller index out of bound"

    invoke-static {v2, v1}, Lcom/google/android/youtube/core/utils/s;->a(ZLjava/lang/Object;)V

    iget-object v1, p0, Lcom/google/android/youtube/core/v11/ControllerActivity;->a:Ljava/util/List;

    invoke-interface {v1, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/youtube/core/v11/Controller;

    invoke-direct {p0, v0}, Lcom/google/android/youtube/core/v11/ControllerActivity;->b(Lcom/google/android/youtube/core/v11/Controller;)V

    return-void
.end method

.method protected onResume()V
    .locals 2

    invoke-super {p0}, Landroid/app/Activity;->onResume()V

    sget-object v0, Lcom/google/android/youtube/core/v11/Controller$LifecycleState;->RESUMED:Lcom/google/android/youtube/core/v11/Controller$LifecycleState;

    iput-object v0, p0, Lcom/google/android/youtube/core/v11/ControllerActivity;->c:Lcom/google/android/youtube/core/v11/Controller$LifecycleState;

    iget-object v0, p0, Lcom/google/android/youtube/core/v11/ControllerActivity;->b:Lcom/google/android/youtube/core/v11/Controller;

    sget-object v1, Lcom/google/android/youtube/core/v11/Controller$LifecycleState;->RESUMED:Lcom/google/android/youtube/core/v11/Controller$LifecycleState;

    invoke-virtual {v0, v1}, Lcom/google/android/youtube/core/v11/Controller;->a(Lcom/google/android/youtube/core/v11/Controller$LifecycleState;)V

    return-void
.end method

.method protected onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 4

    const/4 v0, 0x0

    move v1, v0

    :goto_0
    iget-object v0, p0, Lcom/google/android/youtube/core/v11/ControllerActivity;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_1

    iget-object v0, p0, Lcom/google/android/youtube/core/v11/ControllerActivity;->a:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/youtube/core/v11/Controller;

    new-instance v2, Landroid/os/Bundle;

    invoke-direct {v2}, Landroid/os/Bundle;-><init>()V

    new-instance v3, Landroid/util/SparseArray;

    invoke-direct {v3}, Landroid/util/SparseArray;-><init>()V

    invoke-virtual {v0}, Lcom/google/android/youtube/core/v11/Controller;->b()Landroid/view/View;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {v0, v3}, Landroid/view/View;->saveHierarchyState(Landroid/util/SparseArray;)V

    :cond_0
    const-string v0, "controller_ui_state"

    invoke-virtual {v2, v0, v3}, Landroid/os/Bundle;->putSparseParcelableArray(Ljava/lang/String;Landroid/util/SparseArray;)V

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v3, "controller_state_"

    invoke-direct {v0, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0, v2}, Landroid/os/Bundle;->putBundle(Ljava/lang/String;Landroid/os/Bundle;)V

    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    :cond_1
    const-string v0, "selected_controller_index"

    iget-object v1, p0, Lcom/google/android/youtube/core/v11/ControllerActivity;->a:Ljava/util/List;

    iget-object v2, p0, Lcom/google/android/youtube/core/v11/ControllerActivity;->b:Lcom/google/android/youtube/core/v11/Controller;

    invoke-interface {v1, v2}, Ljava/util/List;->indexOf(Ljava/lang/Object;)I

    move-result v1

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    return-void
.end method

.method public final onSearchRequested()Z
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/core/v11/ControllerActivity;->b:Lcom/google/android/youtube/core/v11/Controller;

    const/4 v0, 0x0

    return v0
.end method

.method protected onStart()V
    .locals 2

    invoke-super {p0}, Landroid/app/Activity;->onStart()V

    sget-object v0, Lcom/google/android/youtube/core/v11/Controller$LifecycleState;->PAUSED:Lcom/google/android/youtube/core/v11/Controller$LifecycleState;

    iput-object v0, p0, Lcom/google/android/youtube/core/v11/ControllerActivity;->c:Lcom/google/android/youtube/core/v11/Controller$LifecycleState;

    iget-object v0, p0, Lcom/google/android/youtube/core/v11/ControllerActivity;->b:Lcom/google/android/youtube/core/v11/Controller;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/youtube/core/v11/ControllerActivity;->b:Lcom/google/android/youtube/core/v11/Controller;

    sget-object v1, Lcom/google/android/youtube/core/v11/Controller$LifecycleState;->PAUSED:Lcom/google/android/youtube/core/v11/Controller$LifecycleState;

    invoke-virtual {v0, v1}, Lcom/google/android/youtube/core/v11/Controller;->a(Lcom/google/android/youtube/core/v11/Controller$LifecycleState;)V

    :cond_0
    return-void
.end method

.method protected onStop()V
    .locals 3

    sget-object v0, Lcom/google/android/youtube/core/v11/Controller$LifecycleState;->STOPPED:Lcom/google/android/youtube/core/v11/Controller$LifecycleState;

    iput-object v0, p0, Lcom/google/android/youtube/core/v11/ControllerActivity;->c:Lcom/google/android/youtube/core/v11/Controller$LifecycleState;

    iget-object v0, p0, Lcom/google/android/youtube/core/v11/ControllerActivity;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/youtube/core/v11/Controller;

    sget-object v2, Lcom/google/android/youtube/core/v11/Controller$LifecycleState;->STOPPED:Lcom/google/android/youtube/core/v11/Controller$LifecycleState;

    invoke-virtual {v0, v2}, Lcom/google/android/youtube/core/v11/Controller;->a(Lcom/google/android/youtube/core/v11/Controller$LifecycleState;)V

    goto :goto_0

    :cond_0
    invoke-super {p0}, Landroid/app/Activity;->onStop()V

    return-void
.end method
