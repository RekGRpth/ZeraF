.class public Lcom/google/android/youtube/gmsplus1/i;
.super Lvedroid/support/v4/app/Fragment;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/gms/common/c;
.implements Lcom/google/android/gms/common/d;


# instance fields
.field private a:Lcom/google/android/gms/plus/a;

.field private b:Z


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Lvedroid/support/v4/app/Fragment;-><init>()V

    return-void
.end method

.method private E()V
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/gmsplus1/i;->a:Lcom/google/android/gms/plus/a;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/youtube/gmsplus1/i;->a:Lcom/google/android/gms/plus/a;

    invoke-virtual {v0, p0}, Lcom/google/android/gms/plus/a;->c(Lcom/google/android/gms/common/c;)V

    iget-object v0, p0, Lcom/google/android/youtube/gmsplus1/i;->a:Lcom/google/android/gms/plus/a;

    invoke-virtual {v0, p0}, Lcom/google/android/gms/plus/a;->c(Lcom/google/android/gms/common/d;)V

    iget-boolean v0, p0, Lcom/google/android/youtube/gmsplus1/i;->b:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/youtube/gmsplus1/i;->a:Lcom/google/android/gms/plus/a;

    invoke-virtual {v0}, Lcom/google/android/gms/plus/a;->d()V

    :cond_0
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/youtube/gmsplus1/i;->a:Lcom/google/android/gms/plus/a;

    :cond_1
    return-void
.end method


# virtual methods
.method public final D()Lcom/google/android/gms/plus/a;
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/gmsplus1/i;->a:Lcom/google/android/gms/plus/a;

    return-object v0
.end method

.method public final a()V
    .locals 0

    return-void
.end method

.method public final a(Lcom/google/android/youtube/gmsplus1/f;Ljava/lang/String;)V
    .locals 2

    const-string v0, "gmsPlusOneClient cannot be null"

    invoke-static {p1, v0}, Lcom/google/android/youtube/core/utils/s;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const-string v0, "accountName cannot be empty"

    invoke-static {p2, v0}, Lcom/google/android/youtube/core/utils/s;->a(Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/String;

    invoke-virtual {p0}, Lcom/google/android/youtube/gmsplus1/i;->l()Z

    move-result v0

    const-string v1, "this fragment must be attached to an activity"

    invoke-static {v0, v1}, Lcom/google/android/youtube/core/utils/s;->b(ZLjava/lang/Object;)V

    invoke-direct {p0}, Lcom/google/android/youtube/gmsplus1/i;->E()V

    invoke-virtual {p0}, Lcom/google/android/youtube/gmsplus1/i;->j()Lvedroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-virtual {v0}, Lvedroid/support/v4/app/FragmentActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {p1, v0, p2, p0, p0}, Lcom/google/android/youtube/gmsplus1/f;->a(Landroid/content/Context;Ljava/lang/String;Lcom/google/android/gms/common/c;Lcom/google/android/gms/common/d;)Lcom/google/android/gms/plus/a;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/youtube/gmsplus1/i;->a:Lcom/google/android/gms/plus/a;

    iget-boolean v0, p0, Lcom/google/android/youtube/gmsplus1/i;->b:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/youtube/gmsplus1/i;->a:Lcom/google/android/gms/plus/a;

    invoke-virtual {v0}, Lcom/google/android/gms/plus/a;->b()V

    :cond_0
    return-void
.end method

.method public final b()V
    .locals 0

    return-void
.end method

.method public final c()V
    .locals 0

    return-void
.end method

.method public final d()V
    .locals 0

    invoke-direct {p0}, Lcom/google/android/youtube/gmsplus1/i;->E()V

    invoke-super {p0}, Lvedroid/support/v4/app/Fragment;->d()V

    return-void
.end method

.method public final e()V
    .locals 1

    invoke-super {p0}, Lvedroid/support/v4/app/Fragment;->e()V

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/youtube/gmsplus1/i;->b:Z

    iget-object v0, p0, Lcom/google/android/youtube/gmsplus1/i;->a:Lcom/google/android/gms/plus/a;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/youtube/gmsplus1/i;->a:Lcom/google/android/gms/plus/a;

    invoke-virtual {v0}, Lcom/google/android/gms/plus/a;->b()V

    :cond_0
    return-void
.end method

.method public final f()V
    .locals 1

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/youtube/gmsplus1/i;->b:Z

    iget-object v0, p0, Lcom/google/android/youtube/gmsplus1/i;->a:Lcom/google/android/gms/plus/a;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/youtube/gmsplus1/i;->a:Lcom/google/android/gms/plus/a;

    invoke-virtual {v0}, Lcom/google/android/gms/plus/a;->d()V

    :cond_0
    invoke-super {p0}, Lvedroid/support/v4/app/Fragment;->f()V

    return-void
.end method
