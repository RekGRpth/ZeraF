.class public final Lcom/google/android/youtube/gmsplus1/a;
.super Lcom/google/android/youtube/core/async/a;
.source "SourceFile"


# instance fields
.field private final e:Landroid/content/Context;

.field private final f:Ljava/util/concurrent/Executor;

.field private g:Lcom/google/android/youtube/gmsplus1/d;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/google/android/youtube/core/model/UserAuth$AuthMethod;Ljava/util/concurrent/Executor;)V
    .locals 2

    invoke-static {p1}, Landroid/accounts/AccountManager;->get(Landroid/content/Context;)Landroid/accounts/AccountManager;

    move-result-object v0

    const-string v1, "com.google"

    invoke-direct {p0, v0, p2, v1}, Lcom/google/android/youtube/core/async/a;-><init>(Landroid/accounts/AccountManager;Lcom/google/android/youtube/core/model/UserAuth$AuthMethod;Ljava/lang/String;)V

    const-string v0, "applicationContext cannot be null"

    invoke-static {p1, v0}, Lcom/google/android/youtube/core/utils/s;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/Context;

    iput-object v0, p0, Lcom/google/android/youtube/gmsplus1/a;->e:Landroid/content/Context;

    const-string v0, "executor cannot be null"

    invoke-static {p3, v0}, Lcom/google/android/youtube/core/utils/s;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/concurrent/Executor;

    iput-object v0, p0, Lcom/google/android/youtube/gmsplus1/a;->f:Ljava/util/concurrent/Executor;

    return-void
.end method

.method static synthetic a(Lcom/google/android/youtube/gmsplus1/a;)Landroid/content/Context;
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/gmsplus1/a;->e:Landroid/content/Context;

    return-object v0
.end method

.method private a(Lvedroid/support/v4/app/FragmentActivity;Landroid/accounts/Account;Lcom/google/android/youtube/core/async/bk;Z)V
    .locals 7

    iget-object v6, p0, Lcom/google/android/youtube/gmsplus1/a;->f:Ljava/util/concurrent/Executor;

    new-instance v0, Lcom/google/android/youtube/gmsplus1/c;

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move v5, p4

    invoke-direct/range {v0 .. v5}, Lcom/google/android/youtube/gmsplus1/c;-><init>(Lcom/google/android/youtube/gmsplus1/a;Lvedroid/support/v4/app/FragmentActivity;Landroid/accounts/Account;Lcom/google/android/youtube/core/async/bk;Z)V

    invoke-interface {v6, v0}, Ljava/util/concurrent/Executor;->execute(Ljava/lang/Runnable;)V

    return-void
.end method

.method static synthetic a(Lcom/google/android/youtube/gmsplus1/a;Lcom/google/android/youtube/gmsplus1/d;)V
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/gmsplus1/a;->g:Lcom/google/android/youtube/gmsplus1/d;

    iput-object p1, p0, Lcom/google/android/youtube/gmsplus1/a;->g:Lcom/google/android/youtube/gmsplus1/d;

    if-eqz v0, :cond_0

    invoke-static {v0}, Lcom/google/android/youtube/gmsplus1/d;->a(Lcom/google/android/youtube/gmsplus1/d;)Lcom/google/android/youtube/core/async/bk;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/youtube/core/async/bk;->g_()V

    :cond_0
    return-void
.end method


# virtual methods
.method protected final a(Landroid/accounts/Account;)Lcom/google/android/youtube/core/model/UserAuth;
    .locals 4

    const/4 v0, 0x0

    :try_start_0
    iget-object v1, p0, Lcom/google/android/youtube/gmsplus1/a;->e:Landroid/content/Context;

    iget-object v2, p1, Landroid/accounts/Account;->name:Ljava/lang/String;

    iget-object v3, p0, Lcom/google/android/youtube/gmsplus1/a;->c:Lcom/google/android/youtube/core/model/UserAuth$AuthMethod;

    iget-object v3, v3, Lcom/google/android/youtube/core/model/UserAuth$AuthMethod;->scope:Ljava/lang/String;

    invoke-static {v1, v2, v3}, Lcom/google/android/gms/auth/a;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Lcom/google/android/gms/auth/GoogleAuthException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_2

    move-result-object v1

    if-eqz v1, :cond_0

    new-instance v0, Lcom/google/android/youtube/core/model/UserAuth;

    iget-object v2, p1, Landroid/accounts/Account;->name:Ljava/lang/String;

    iget-object v3, p0, Lcom/google/android/youtube/gmsplus1/a;->c:Lcom/google/android/youtube/core/model/UserAuth$AuthMethod;

    invoke-direct {v0, v2, v3, v1}, Lcom/google/android/youtube/core/model/UserAuth;-><init>(Ljava/lang/String;Lcom/google/android/youtube/core/model/UserAuth$AuthMethod;Ljava/lang/String;)V

    :goto_0
    return-object v0

    :catch_0
    move-exception v1

    const-string v2, "Cannot get user auth; network error"

    invoke-static {v2, v1}, Lcom/google/android/youtube/core/L;->a(Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0

    :catch_1
    move-exception v1

    const-string v2, "Cannot get user auth"

    invoke-static {v2, v1}, Lcom/google/android/youtube/core/L;->a(Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0

    :catch_2
    move-exception v1

    const-string v2, "Cannot get user auth"

    invoke-static {v2, v1}, Lcom/google/android/youtube/core/L;->a(Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0

    :cond_0
    const-string v1, "got null authToken for the selected account"

    invoke-static {v1}, Lcom/google/android/youtube/core/L;->b(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public final a(Landroid/accounts/Account;Lvedroid/support/v4/app/FragmentActivity;Lcom/google/android/youtube/core/async/bk;Z)V
    .locals 1

    const-string v0, "account cannot be null"

    invoke-static {p1, v0}, Lcom/google/android/youtube/core/utils/s;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const-string v0, "activity cannot be null"

    invoke-static {p2, v0}, Lcom/google/android/youtube/core/utils/s;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const-string v0, "callback cannot be null"

    invoke-static {p3, v0}, Lcom/google/android/youtube/core/utils/s;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    invoke-direct {p0, p2, p1, p3, p4}, Lcom/google/android/youtube/gmsplus1/a;->a(Lvedroid/support/v4/app/FragmentActivity;Landroid/accounts/Account;Lcom/google/android/youtube/core/async/bk;Z)V

    return-void
.end method

.method protected final a(Landroid/accounts/Account;Lcom/google/android/youtube/core/async/bk;)V
    .locals 2

    iget-object v0, p0, Lcom/google/android/youtube/gmsplus1/a;->f:Ljava/util/concurrent/Executor;

    new-instance v1, Lcom/google/android/youtube/gmsplus1/b;

    invoke-direct {v1, p0, p1, p2}, Lcom/google/android/youtube/gmsplus1/b;-><init>(Lcom/google/android/youtube/gmsplus1/a;Landroid/accounts/Account;Lcom/google/android/youtube/core/async/bk;)V

    invoke-interface {v0, v1}, Ljava/util/concurrent/Executor;->execute(Ljava/lang/Runnable;)V

    return-void
.end method

.method public final a(Lcom/google/android/youtube/core/model/UserAuth;)V
    .locals 3

    iget-object v0, p0, Lcom/google/android/youtube/gmsplus1/a;->e:Landroid/content/Context;

    iget-object v1, p1, Lcom/google/android/youtube/core/model/UserAuth;->authToken:Ljava/lang/String;

    invoke-static {v0}, Landroid/accounts/AccountManager;->get(Landroid/content/Context;)Landroid/accounts/AccountManager;

    move-result-object v0

    const-string v2, "com.google"

    invoke-virtual {v0, v2, v1}, Landroid/accounts/AccountManager;->invalidateAuthToken(Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method public final a(IILandroid/content/Intent;)Z
    .locals 3

    const/4 v0, 0x0

    const/16 v1, 0x388

    if-eq p1, v1, :cond_1

    :cond_0
    :goto_0
    return v0

    :cond_1
    iget-object v1, p0, Lcom/google/android/youtube/gmsplus1/a;->g:Lcom/google/android/youtube/gmsplus1/d;

    const/4 v2, 0x0

    iput-object v2, p0, Lcom/google/android/youtube/gmsplus1/a;->g:Lcom/google/android/youtube/gmsplus1/d;

    if-eqz v1, :cond_0

    invoke-virtual {v1, p2}, Lcom/google/android/youtube/gmsplus1/d;->a(I)V

    const/4 v0, 0x1

    goto :goto_0
.end method

.method protected final b(Landroid/accounts/Account;Landroid/app/Activity;Lcom/google/android/youtube/core/async/bk;)V
    .locals 2

    instance-of v0, p2, Lvedroid/support/v4/app/FragmentActivity;

    const-string v1, "activity must be a FragmentActivity"

    invoke-static {v0, v1}, Lcom/google/android/youtube/core/utils/s;->a(ZLjava/lang/Object;)V

    check-cast p2, Lvedroid/support/v4/app/FragmentActivity;

    const/4 v0, 0x0

    invoke-direct {p0, p2, p1, p3, v0}, Lcom/google/android/youtube/gmsplus1/a;->a(Lvedroid/support/v4/app/FragmentActivity;Landroid/accounts/Account;Lcom/google/android/youtube/core/async/bk;Z)V

    return-void
.end method
