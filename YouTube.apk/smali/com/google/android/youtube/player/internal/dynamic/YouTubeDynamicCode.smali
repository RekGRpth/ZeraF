.class public final Lcom/google/android/youtube/player/internal/dynamic/YouTubeDynamicCode;
.super Ljava/lang/Object;
.source "SourceFile"


# direct methods
.method private static a(Ljava/lang/Class;Landroid/os/IBinder;Landroid/os/IBinder;Z)Landroid/os/IBinder;
    .locals 4

    const/4 v0, 0x3

    :try_start_0
    new-array v0, v0, [Ljava/lang/Class;

    const/4 v1, 0x0

    const-class v2, Landroid/os/IBinder;

    aput-object v2, v0, v1

    const/4 v1, 0x1

    const-class v2, Landroid/os/IBinder;

    aput-object v2, v0, v1

    const/4 v1, 0x2

    sget-object v2, Ljava/lang/Boolean;->TYPE:Ljava/lang/Class;

    aput-object v2, v0, v1

    invoke-virtual {p0, v0}, Ljava/lang/Class;->getConstructor([Ljava/lang/Class;)Ljava/lang/reflect/Constructor;

    move-result-object v0

    const/4 v1, 0x3

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    aput-object p1, v1, v2

    const/4 v2, 0x1

    aput-object p2, v1, v2

    const/4 v2, 0x2

    invoke-static {p3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v3

    aput-object v3, v1, v2

    invoke-virtual {v0, v1}, Ljava/lang/reflect/Constructor;->newInstance([Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/os/IBinder;
    :try_end_0
    .catch Ljava/lang/NoSuchMethodException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/reflect/InvocationTargetException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/InstantiationException; {:try_start_0 .. :try_end_0} :catch_2
    .catch Ljava/lang/IllegalAccessException; {:try_start_0 .. :try_end_0} :catch_3

    return-object v0

    :catch_0
    move-exception v0

    new-instance v1, Lcom/google/android/youtube/player/internal/dynamic/YouTubeDynamicCode$DynamicCodeException;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Could not find the right constructor for "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2, v0}, Lcom/google/android/youtube/player/internal/dynamic/YouTubeDynamicCode$DynamicCodeException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v1

    :catch_1
    move-exception v0

    new-instance v1, Lcom/google/android/youtube/player/internal/dynamic/YouTubeDynamicCode$DynamicCodeException;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Exception thrown by invoked constructor in "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2, v0}, Lcom/google/android/youtube/player/internal/dynamic/YouTubeDynamicCode$DynamicCodeException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v1

    :catch_2
    move-exception v0

    new-instance v1, Lcom/google/android/youtube/player/internal/dynamic/YouTubeDynamicCode$DynamicCodeException;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Unable to instantiate the dynamic class "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2, v0}, Lcom/google/android/youtube/player/internal/dynamic/YouTubeDynamicCode$DynamicCodeException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v1

    :catch_3
    move-exception v0

    new-instance v1, Lcom/google/android/youtube/player/internal/dynamic/YouTubeDynamicCode$DynamicCodeException;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Unable to call the default constructor of "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2, v0}, Lcom/google/android/youtube/player/internal/dynamic/YouTubeDynamicCode$DynamicCodeException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v1
.end method

.method private static a(Ljava/lang/ClassLoader;Ljava/lang/String;Landroid/os/IBinder;Landroid/os/IBinder;Z)Landroid/os/IBinder;
    .locals 4

    :try_start_0
    invoke-virtual {p0, p1}, Ljava/lang/ClassLoader;->loadClass(Ljava/lang/String;)Ljava/lang/Class;

    move-result-object v0

    invoke-static {v0, p2, p3, p4}, Lcom/google/android/youtube/player/internal/dynamic/YouTubeDynamicCode;->a(Ljava/lang/Class;Landroid/os/IBinder;Landroid/os/IBinder;Z)Landroid/os/IBinder;
    :try_end_0
    .catch Ljava/lang/ClassNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    return-object v0

    :catch_0
    move-exception v0

    new-instance v1, Lcom/google/android/youtube/player/internal/dynamic/YouTubeDynamicCode$DynamicCodeException;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Unable to find dynamic class "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2, v0}, Lcom/google/android/youtube/player/internal/dynamic/YouTubeDynamicCode$DynamicCodeException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v1
.end method

.method public static a(Landroid/app/Activity;Landroid/os/IBinder;Z)Lcom/google/android/youtube/player/internal/g;
    .locals 5

    const-string v0, "activity cannot be null"

    invoke-static {p0, v0}, Lcom/google/android/youtube/player/internal/util/g;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const-string v0, "serviceBinder cannot be null"

    invoke-static {p1, v0}, Lcom/google/android/youtube/player/internal/util/g;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    invoke-static {p0}, Lcom/google/android/youtube/player/internal/util/b;->b(Landroid/content/Context;)Landroid/content/Context;

    move-result-object v0

    if-nez v0, :cond_0

    new-instance v0, Lcom/google/android/youtube/player/internal/dynamic/YouTubeDynamicCode$DynamicCodeException;

    const-string v1, "Could not create remote context"

    invoke-direct {v0, v1}, Lcom/google/android/youtube/player/internal/dynamic/YouTubeDynamicCode$DynamicCodeException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    new-instance v1, Lcom/google/android/youtube/player/internal/util/DynamicCodeActivityWrapper;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {v0}, Landroid/content/Context;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v3

    invoke-static {p0, v0}, Lcom/google/android/youtube/player/internal/util/b;->a(Landroid/content/Context;Landroid/content/Context;)I

    move-result v4

    invoke-direct {v1, p0, v2, v3, v4}, Lcom/google/android/youtube/player/internal/util/DynamicCodeActivityWrapper;-><init>(Landroid/app/Activity;Landroid/content/res/Resources;Ljava/lang/ClassLoader;I)V

    invoke-virtual {v0}, Landroid/content/Context;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    const-string v2, "com.google.android.youtube.api.jar.client.RemoteEmbeddedPlayer"

    invoke-static {v1}, Lcom/google/android/youtube/player/internal/dynamic/d;->a(Ljava/lang/Object;)Lcom/google/android/youtube/player/internal/dynamic/a;

    move-result-object v1

    invoke-interface {v1}, Lcom/google/android/youtube/player/internal/dynamic/a;->asBinder()Landroid/os/IBinder;

    move-result-object v1

    invoke-static {v0, v2, v1, p1, p2}, Lcom/google/android/youtube/player/internal/dynamic/YouTubeDynamicCode;->a(Ljava/lang/ClassLoader;Ljava/lang/String;Landroid/os/IBinder;Landroid/os/IBinder;Z)Landroid/os/IBinder;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/youtube/player/internal/h;->a(Landroid/os/IBinder;)Lcom/google/android/youtube/player/internal/g;

    move-result-object v0

    return-object v0
.end method
