.class public abstract Lcom/google/android/youtube/player/internal/b;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/youtube/player/YouTubeThumbnailLoader;


# instance fields
.field private a:Lcom/google/android/youtube/player/m;

.field private b:Z


# direct methods
.method private b()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/youtube/player/internal/b;->b:Z

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public final a()V
    .locals 2

    invoke-direct {p0}, Lcom/google/android/youtube/player/internal/b;->b()Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v0, "The finalize() method for a YouTubeThumbnailLoader has work to do. You should have called release()."

    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/Object;

    invoke-static {v0, v1}, Lcom/google/android/youtube/player/internal/util/a;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    invoke-direct {p0}, Lcom/google/android/youtube/player/internal/b;->b()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/youtube/player/internal/b;->b:Z

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/youtube/player/internal/b;->a:Lcom/google/android/youtube/player/m;

    :cond_0
    return-void
.end method
