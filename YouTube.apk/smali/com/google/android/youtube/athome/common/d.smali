.class public final Lcom/google/android/youtube/athome/common/d;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field private final a:Landroid/support/place/rpc/RpcData;


# direct methods
.method public constructor <init>(Landroid/support/place/rpc/RpcData;)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/google/android/youtube/athome/common/d;->a:Landroid/support/place/rpc/RpcData;

    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/String;I)I
    .locals 1

    :try_start_0
    iget-object v0, p0, Lcom/google/android/youtube/athome/common/d;->a:Landroid/support/place/rpc/RpcData;

    invoke-virtual {v0, p1}, Landroid/support/place/rpc/RpcData;->getInteger(Ljava/lang/String;)I
    :try_end_0
    .catch Landroid/support/place/rpc/RpcData$KeyNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    move-result p2

    :goto_0
    return p2

    :catch_0
    move-exception v0

    goto :goto_0
.end method

.method public final a(Ljava/lang/String;Landroid/support/place/rpc/Flattenable$Creator;)Landroid/support/place/rpc/Flattenable;
    .locals 1

    :try_start_0
    iget-object v0, p0, Lcom/google/android/youtube/athome/common/d;->a:Landroid/support/place/rpc/RpcData;

    invoke-virtual {v0, p1, p2}, Landroid/support/place/rpc/RpcData;->getFlattenable(Ljava/lang/String;Landroid/support/place/rpc/Flattenable$Creator;)Landroid/support/place/rpc/Flattenable;
    :try_end_0
    .catch Landroid/support/place/rpc/RpcData$KeyNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    :goto_0
    return-object v0

    :catch_0
    move-exception v0

    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final a(Ljava/lang/String;)Ljava/lang/String;
    .locals 1

    :try_start_0
    iget-object v0, p0, Lcom/google/android/youtube/athome/common/d;->a:Landroid/support/place/rpc/RpcData;

    invoke-virtual {v0, p1}, Landroid/support/place/rpc/RpcData;->getString(Ljava/lang/String;)Ljava/lang/String;
    :try_end_0
    .catch Landroid/support/place/rpc/RpcData$KeyNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    :goto_0
    return-object v0

    :catch_0
    move-exception v0

    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final a(Ljava/lang/String;Z)Z
    .locals 1

    :try_start_0
    iget-object v0, p0, Lcom/google/android/youtube/athome/common/d;->a:Landroid/support/place/rpc/RpcData;

    invoke-virtual {v0, p1}, Landroid/support/place/rpc/RpcData;->getBoolean(Ljava/lang/String;)Z
    :try_end_0
    .catch Landroid/support/place/rpc/RpcData$KeyNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v0

    :goto_0
    return v0

    :catch_0
    move-exception v0

    const/4 v0, 0x0

    goto :goto_0
.end method
