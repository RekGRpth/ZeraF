.class final Lcom/google/android/youtube/api/r;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/youtube/core/player/v;


# instance fields
.field final synthetic a:Lcom/google/android/youtube/api/ApiPlayer;

.field private b:Z

.field private c:Z


# direct methods
.method private constructor <init>(Lcom/google/android/youtube/api/ApiPlayer;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/youtube/api/r;->a:Lcom/google/android/youtube/api/ApiPlayer;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/google/android/youtube/api/ApiPlayer;B)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/google/android/youtube/api/r;-><init>(Lcom/google/android/youtube/api/ApiPlayer;)V

    return-void
.end method


# virtual methods
.method public final a()V
    .locals 2

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/youtube/api/r;->b:Z

    iput-boolean v0, p0, Lcom/google/android/youtube/api/r;->c:Z

    iget-object v0, p0, Lcom/google/android/youtube/api/r;->a:Lcom/google/android/youtube/api/ApiPlayer;

    sget-object v1, Lcom/google/android/youtube/api/ApiPlayer$State;->LOADING:Lcom/google/android/youtube/api/ApiPlayer$State;

    invoke-static {v0, v1}, Lcom/google/android/youtube/api/ApiPlayer;->a(Lcom/google/android/youtube/api/ApiPlayer;Lcom/google/android/youtube/api/ApiPlayer$State;)V

    iget-object v0, p0, Lcom/google/android/youtube/api/r;->a:Lcom/google/android/youtube/api/ApiPlayer;

    invoke-static {v0}, Lcom/google/android/youtube/api/ApiPlayer;->d(Lcom/google/android/youtube/api/ApiPlayer;)Lcom/google/android/youtube/api/s;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/youtube/api/s;->d()V

    return-void
.end method

.method public final a(Lcom/google/android/youtube/core/model/Branding;)V
    .locals 0

    return-void
.end method

.method public final a(Lcom/google/android/youtube/core/model/VastAd;)V
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/youtube/api/r;->b:Z

    if-nez v0, :cond_0

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/youtube/api/r;->b:Z

    iget-object v0, p0, Lcom/google/android/youtube/api/r;->a:Lcom/google/android/youtube/api/ApiPlayer;

    invoke-static {v0}, Lcom/google/android/youtube/api/ApiPlayer;->d(Lcom/google/android/youtube/api/ApiPlayer;)Lcom/google/android/youtube/api/s;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/youtube/api/s;->e()V

    :cond_0
    iget-object v0, p0, Lcom/google/android/youtube/api/r;->a:Lcom/google/android/youtube/api/ApiPlayer;

    invoke-static {v0}, Lcom/google/android/youtube/api/ApiPlayer;->d(Lcom/google/android/youtube/api/ApiPlayer;)Lcom/google/android/youtube/api/s;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/youtube/api/s;->i()V

    return-void
.end method

.method public final a(Lcom/google/android/youtube/core/model/Video;)V
    .locals 2

    iget-object v0, p0, Lcom/google/android/youtube/api/r;->a:Lcom/google/android/youtube/api/ApiPlayer;

    iget-object v1, p1, Lcom/google/android/youtube/core/model/Video;->id:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/google/android/youtube/api/ApiPlayer;->a(Lcom/google/android/youtube/api/ApiPlayer;Ljava/lang/String;)Ljava/lang/String;

    iget-object v0, p0, Lcom/google/android/youtube/api/r;->a:Lcom/google/android/youtube/api/ApiPlayer;

    iget-object v1, p1, Lcom/google/android/youtube/core/model/Video;->title:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/google/android/youtube/api/ApiPlayer;->b(Lcom/google/android/youtube/api/ApiPlayer;Ljava/lang/String;)Ljava/lang/String;

    return-void
.end method

.method public final a(Lcom/google/android/youtube/core/player/Director$StopReason;)V
    .locals 2

    sget-object v0, Lcom/google/android/youtube/api/o;->b:[I

    invoke-virtual {p1}, Lcom/google/android/youtube/core/player/Director$StopReason;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    const-string v0, "Unhandled StopReason in OnPlaybackStopped"

    invoke-static {v0}, Lcom/google/android/youtube/core/L;->b(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/google/android/youtube/api/r;->a:Lcom/google/android/youtube/api/ApiPlayer;

    invoke-static {v0}, Lcom/google/android/youtube/api/ApiPlayer;->d(Lcom/google/android/youtube/api/ApiPlayer;)Lcom/google/android/youtube/api/s;

    move-result-object v0

    sget-object v1, Lcom/google/android/youtube/player/YouTubePlayer$ErrorReason;->UNKNOWN:Lcom/google/android/youtube/player/YouTubePlayer$ErrorReason;

    invoke-interface {v0, v1}, Lcom/google/android/youtube/api/s;->a(Lcom/google/android/youtube/player/YouTubePlayer$ErrorReason;)V

    :goto_0
    iget-object v0, p0, Lcom/google/android/youtube/api/r;->a:Lcom/google/android/youtube/api/ApiPlayer;

    invoke-static {v0}, Lcom/google/android/youtube/api/ApiPlayer;->e(Lcom/google/android/youtube/api/ApiPlayer;)V

    return-void

    :pswitch_0
    iget-object v0, p0, Lcom/google/android/youtube/api/r;->a:Lcom/google/android/youtube/api/ApiPlayer;

    invoke-static {v0}, Lcom/google/android/youtube/api/ApiPlayer;->d(Lcom/google/android/youtube/api/ApiPlayer;)Lcom/google/android/youtube/api/s;

    move-result-object v0

    sget-object v1, Lcom/google/android/youtube/player/YouTubePlayer$ErrorReason;->USER_DECLINED_RESTRICTED_CONTENT:Lcom/google/android/youtube/player/YouTubePlayer$ErrorReason;

    invoke-interface {v0, v1}, Lcom/google/android/youtube/api/s;->a(Lcom/google/android/youtube/player/YouTubePlayer$ErrorReason;)V

    goto :goto_0

    :pswitch_1
    iget-object v0, p0, Lcom/google/android/youtube/api/r;->a:Lcom/google/android/youtube/api/ApiPlayer;

    invoke-static {v0}, Lcom/google/android/youtube/api/ApiPlayer;->d(Lcom/google/android/youtube/api/ApiPlayer;)Lcom/google/android/youtube/api/s;

    move-result-object v0

    sget-object v1, Lcom/google/android/youtube/player/YouTubePlayer$ErrorReason;->EMPTY_PLAYLIST:Lcom/google/android/youtube/player/YouTubePlayer$ErrorReason;

    invoke-interface {v0, v1}, Lcom/google/android/youtube/api/s;->a(Lcom/google/android/youtube/player/YouTubePlayer$ErrorReason;)V

    goto :goto_0

    :pswitch_2
    iget-object v0, p0, Lcom/google/android/youtube/api/r;->a:Lcom/google/android/youtube/api/ApiPlayer;

    invoke-static {v0}, Lcom/google/android/youtube/api/ApiPlayer;->d(Lcom/google/android/youtube/api/ApiPlayer;)Lcom/google/android/youtube/api/s;

    move-result-object v0

    sget-object v1, Lcom/google/android/youtube/player/YouTubePlayer$ErrorReason;->AUTOPLAY_DISABLED:Lcom/google/android/youtube/player/YouTubePlayer$ErrorReason;

    invoke-interface {v0, v1}, Lcom/google/android/youtube/api/s;->a(Lcom/google/android/youtube/player/YouTubePlayer$ErrorReason;)V

    goto :goto_0

    :pswitch_3
    iget-object v0, p0, Lcom/google/android/youtube/api/r;->a:Lcom/google/android/youtube/api/ApiPlayer;

    invoke-static {v0}, Lcom/google/android/youtube/api/ApiPlayer;->d(Lcom/google/android/youtube/api/ApiPlayer;)Lcom/google/android/youtube/api/s;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/youtube/api/s;->c()V

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method public final a(Lcom/google/android/youtube/core/player/DirectorException;)V
    .locals 2

    iget-object v0, p0, Lcom/google/android/youtube/api/r;->a:Lcom/google/android/youtube/api/ApiPlayer;

    sget-object v1, Lcom/google/android/youtube/api/ApiPlayer$State;->UNINITIALIZED:Lcom/google/android/youtube/api/ApiPlayer$State;

    invoke-static {v0, v1}, Lcom/google/android/youtube/api/ApiPlayer;->a(Lcom/google/android/youtube/api/ApiPlayer;Lcom/google/android/youtube/api/ApiPlayer$State;)V

    sget-object v0, Lcom/google/android/youtube/api/o;->a:[I

    iget-object v1, p1, Lcom/google/android/youtube/core/player/DirectorException;->reason:Lcom/google/android/youtube/core/player/DirectorException$ErrorReason;

    invoke-virtual {v1}, Lcom/google/android/youtube/core/player/DirectorException$ErrorReason;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    const-string v0, "Unhandled ErrorReason in onError"

    invoke-static {v0}, Lcom/google/android/youtube/core/L;->b(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/google/android/youtube/api/r;->a:Lcom/google/android/youtube/api/ApiPlayer;

    invoke-static {v0}, Lcom/google/android/youtube/api/ApiPlayer;->d(Lcom/google/android/youtube/api/ApiPlayer;)Lcom/google/android/youtube/api/s;

    move-result-object v0

    sget-object v1, Lcom/google/android/youtube/player/YouTubePlayer$ErrorReason;->UNKNOWN:Lcom/google/android/youtube/player/YouTubePlayer$ErrorReason;

    invoke-interface {v0, v1}, Lcom/google/android/youtube/api/s;->a(Lcom/google/android/youtube/player/YouTubePlayer$ErrorReason;)V

    :goto_0
    return-void

    :pswitch_0
    iget-object v0, p0, Lcom/google/android/youtube/api/r;->a:Lcom/google/android/youtube/api/ApiPlayer;

    invoke-static {v0}, Lcom/google/android/youtube/api/ApiPlayer;->d(Lcom/google/android/youtube/api/ApiPlayer;)Lcom/google/android/youtube/api/s;

    move-result-object v0

    sget-object v1, Lcom/google/android/youtube/player/YouTubePlayer$ErrorReason;->EMBEDDING_DISABLED:Lcom/google/android/youtube/player/YouTubePlayer$ErrorReason;

    invoke-interface {v0, v1}, Lcom/google/android/youtube/api/s;->a(Lcom/google/android/youtube/player/YouTubePlayer$ErrorReason;)V

    goto :goto_0

    :pswitch_1
    iget-object v0, p0, Lcom/google/android/youtube/api/r;->a:Lcom/google/android/youtube/api/ApiPlayer;

    invoke-static {v0}, Lcom/google/android/youtube/api/ApiPlayer;->d(Lcom/google/android/youtube/api/ApiPlayer;)Lcom/google/android/youtube/api/s;

    move-result-object v0

    sget-object v1, Lcom/google/android/youtube/player/YouTubePlayer$ErrorReason;->BLOCKED_FOR_APP:Lcom/google/android/youtube/player/YouTubePlayer$ErrorReason;

    invoke-interface {v0, v1}, Lcom/google/android/youtube/api/s;->a(Lcom/google/android/youtube/player/YouTubePlayer$ErrorReason;)V

    goto :goto_0

    :pswitch_2
    iget-object v0, p0, Lcom/google/android/youtube/api/r;->a:Lcom/google/android/youtube/api/ApiPlayer;

    invoke-static {v0}, Lcom/google/android/youtube/api/ApiPlayer;->f(Lcom/google/android/youtube/api/ApiPlayer;)Lcom/google/android/youtube/api/j;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/youtube/api/j;->l()Lcom/google/android/youtube/core/utils/p;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/youtube/core/utils/p;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/youtube/api/r;->a:Lcom/google/android/youtube/api/ApiPlayer;

    invoke-static {v0}, Lcom/google/android/youtube/api/ApiPlayer;->d(Lcom/google/android/youtube/api/ApiPlayer;)Lcom/google/android/youtube/api/s;

    move-result-object v0

    sget-object v1, Lcom/google/android/youtube/player/YouTubePlayer$ErrorReason;->NOT_PLAYABLE:Lcom/google/android/youtube/player/YouTubePlayer$ErrorReason;

    invoke-interface {v0, v1}, Lcom/google/android/youtube/api/s;->a(Lcom/google/android/youtube/player/YouTubePlayer$ErrorReason;)V

    goto :goto_0

    :cond_0
    iget-object v0, p0, Lcom/google/android/youtube/api/r;->a:Lcom/google/android/youtube/api/ApiPlayer;

    invoke-static {v0}, Lcom/google/android/youtube/api/ApiPlayer;->d(Lcom/google/android/youtube/api/ApiPlayer;)Lcom/google/android/youtube/api/s;

    move-result-object v0

    sget-object v1, Lcom/google/android/youtube/player/YouTubePlayer$ErrorReason;->NETWORK_ERROR:Lcom/google/android/youtube/player/YouTubePlayer$ErrorReason;

    invoke-interface {v0, v1}, Lcom/google/android/youtube/api/s;->a(Lcom/google/android/youtube/player/YouTubePlayer$ErrorReason;)V

    goto :goto_0

    :pswitch_3
    iget-object v0, p0, Lcom/google/android/youtube/api/r;->a:Lcom/google/android/youtube/api/ApiPlayer;

    invoke-static {v0}, Lcom/google/android/youtube/api/ApiPlayer;->f(Lcom/google/android/youtube/api/ApiPlayer;)Lcom/google/android/youtube/api/j;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/youtube/api/j;->l()Lcom/google/android/youtube/core/utils/p;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/youtube/core/utils/p;->a()Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/youtube/api/r;->a:Lcom/google/android/youtube/api/ApiPlayer;

    invoke-static {v0}, Lcom/google/android/youtube/api/ApiPlayer;->d(Lcom/google/android/youtube/api/ApiPlayer;)Lcom/google/android/youtube/api/s;

    move-result-object v0

    sget-object v1, Lcom/google/android/youtube/player/YouTubePlayer$ErrorReason;->INTERNAL_ERROR:Lcom/google/android/youtube/player/YouTubePlayer$ErrorReason;

    invoke-interface {v0, v1}, Lcom/google/android/youtube/api/s;->a(Lcom/google/android/youtube/player/YouTubePlayer$ErrorReason;)V

    goto :goto_0

    :cond_1
    iget-object v0, p0, Lcom/google/android/youtube/api/r;->a:Lcom/google/android/youtube/api/ApiPlayer;

    invoke-static {v0}, Lcom/google/android/youtube/api/ApiPlayer;->d(Lcom/google/android/youtube/api/ApiPlayer;)Lcom/google/android/youtube/api/s;

    move-result-object v0

    sget-object v1, Lcom/google/android/youtube/player/YouTubePlayer$ErrorReason;->NETWORK_ERROR:Lcom/google/android/youtube/player/YouTubePlayer$ErrorReason;

    invoke-interface {v0, v1}, Lcom/google/android/youtube/api/s;->a(Lcom/google/android/youtube/player/YouTubePlayer$ErrorReason;)V

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_3
        :pswitch_3
    .end packed-switch
.end method

.method public final a_(Z)V
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/api/r;->a:Lcom/google/android/youtube/api/ApiPlayer;

    invoke-static {v0}, Lcom/google/android/youtube/api/ApiPlayer;->d(Lcom/google/android/youtube/api/ApiPlayer;)Lcom/google/android/youtube/api/s;

    move-result-object v0

    invoke-interface {v0, p1}, Lcom/google/android/youtube/api/s;->a(Z)V

    return-void
.end method

.method public final b()V
    .locals 7

    iget-object v0, p0, Lcom/google/android/youtube/api/r;->a:Lcom/google/android/youtube/api/ApiPlayer;

    sget-object v1, Lcom/google/android/youtube/api/ApiPlayer$State;->LOADED:Lcom/google/android/youtube/api/ApiPlayer$State;

    invoke-static {v0, v1}, Lcom/google/android/youtube/api/ApiPlayer;->a(Lcom/google/android/youtube/api/ApiPlayer;Lcom/google/android/youtube/api/ApiPlayer$State;)V

    iget-object v0, p0, Lcom/google/android/youtube/api/r;->a:Lcom/google/android/youtube/api/ApiPlayer;

    invoke-static {v0}, Lcom/google/android/youtube/api/ApiPlayer;->d(Lcom/google/android/youtube/api/ApiPlayer;)Lcom/google/android/youtube/api/s;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/youtube/api/r;->a:Lcom/google/android/youtube/api/ApiPlayer;

    invoke-static {v1}, Lcom/google/android/youtube/api/ApiPlayer;->g(Lcom/google/android/youtube/api/ApiPlayer;)Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/youtube/api/r;->a:Lcom/google/android/youtube/api/ApiPlayer;

    invoke-static {v2}, Lcom/google/android/youtube/api/ApiPlayer;->h(Lcom/google/android/youtube/api/ApiPlayer;)Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, Lcom/google/android/youtube/api/r;->a:Lcom/google/android/youtube/api/ApiPlayer;

    invoke-static {v3}, Lcom/google/android/youtube/api/ApiPlayer;->c(Lcom/google/android/youtube/api/ApiPlayer;)Lcom/google/android/youtube/core/player/Director;

    move-result-object v3

    invoke-virtual {v3}, Lcom/google/android/youtube/core/player/Director;->r()I

    move-result v3

    iget-object v4, p0, Lcom/google/android/youtube/api/r;->a:Lcom/google/android/youtube/api/ApiPlayer;

    invoke-static {v4}, Lcom/google/android/youtube/api/ApiPlayer;->c(Lcom/google/android/youtube/api/ApiPlayer;)Lcom/google/android/youtube/core/player/Director;

    move-result-object v4

    invoke-virtual {v4}, Lcom/google/android/youtube/core/player/Director;->s()I

    move-result v4

    iget-object v5, p0, Lcom/google/android/youtube/api/r;->a:Lcom/google/android/youtube/api/ApiPlayer;

    invoke-static {v5}, Lcom/google/android/youtube/api/ApiPlayer;->c(Lcom/google/android/youtube/api/ApiPlayer;)Lcom/google/android/youtube/core/player/Director;

    move-result-object v5

    invoke-virtual {v5}, Lcom/google/android/youtube/core/player/Director;->l()Z

    move-result v5

    iget-object v6, p0, Lcom/google/android/youtube/api/r;->a:Lcom/google/android/youtube/api/ApiPlayer;

    invoke-static {v6}, Lcom/google/android/youtube/api/ApiPlayer;->c(Lcom/google/android/youtube/api/ApiPlayer;)Lcom/google/android/youtube/core/player/Director;

    move-result-object v6

    invoke-virtual {v6}, Lcom/google/android/youtube/core/player/Director;->k()Z

    move-result v6

    invoke-interface/range {v0 .. v6}, Lcom/google/android/youtube/api/s;->a(Ljava/lang/String;Ljava/lang/String;IIZZ)V

    return-void
.end method

.method public final b(Lcom/google/android/youtube/core/model/Video;)V
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/youtube/api/r;->c:Z

    if-nez v0, :cond_0

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/youtube/api/r;->c:Z

    iget-object v0, p0, Lcom/google/android/youtube/api/r;->a:Lcom/google/android/youtube/api/ApiPlayer;

    invoke-static {v0}, Lcom/google/android/youtube/api/ApiPlayer;->d(Lcom/google/android/youtube/api/ApiPlayer;)Lcom/google/android/youtube/api/s;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/youtube/api/s;->f()V

    :cond_0
    return-void
.end method

.method public final f()V
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/api/r;->a:Lcom/google/android/youtube/api/ApiPlayer;

    invoke-static {v0}, Lcom/google/android/youtube/api/ApiPlayer;->d(Lcom/google/android/youtube/api/ApiPlayer;)Lcom/google/android/youtube/api/s;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/youtube/api/s;->k()V

    return-void
.end method

.method public final g()V
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/api/r;->a:Lcom/google/android/youtube/api/ApiPlayer;

    invoke-static {v0}, Lcom/google/android/youtube/api/ApiPlayer;->d(Lcom/google/android/youtube/api/ApiPlayer;)Lcom/google/android/youtube/api/s;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/youtube/api/s;->b()V

    return-void
.end method

.method public final h()V
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/api/r;->a:Lcom/google/android/youtube/api/ApiPlayer;

    invoke-static {v0}, Lcom/google/android/youtube/api/ApiPlayer;->d(Lcom/google/android/youtube/api/ApiPlayer;)Lcom/google/android/youtube/api/s;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/youtube/api/s;->a()V

    return-void
.end method

.method public final h_()V
    .locals 0

    return-void
.end method

.method public final i_()V
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/api/r;->a:Lcom/google/android/youtube/api/ApiPlayer;

    invoke-static {v0}, Lcom/google/android/youtube/api/ApiPlayer;->d(Lcom/google/android/youtube/api/ApiPlayer;)Lcom/google/android/youtube/api/s;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/youtube/api/s;->j()V

    return-void
.end method

.method public final j_()V
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/api/r;->a:Lcom/google/android/youtube/api/ApiPlayer;

    invoke-static {v0}, Lcom/google/android/youtube/api/ApiPlayer;->d(Lcom/google/android/youtube/api/ApiPlayer;)Lcom/google/android/youtube/api/s;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/youtube/api/s;->g()V

    return-void
.end method
