.class public final Lcom/google/android/youtube/api/service/a/de;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/youtube/core/player/overlay/ab;


# instance fields
.field private a:Lcom/google/android/youtube/api/jar/client/cn;


# direct methods
.method public constructor <init>(Lcom/google/android/youtube/api/jar/client/cn;)V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const-string v0, "client cannot be null"

    invoke-static {p1, v0}, Lcom/google/android/youtube/core/utils/s;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/youtube/api/jar/client/cn;

    iput-object v0, p0, Lcom/google/android/youtube/api/service/a/de;->a:Lcom/google/android/youtube/api/jar/client/cn;

    return-void
.end method


# virtual methods
.method public final a()V
    .locals 1

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/youtube/api/service/a/de;->a:Lcom/google/android/youtube/api/jar/client/cn;

    return-void
.end method

.method public final b()Landroid/view/View;
    .locals 1

    const/4 v0, 0x0

    return-object v0
.end method

.method public final c()Lcom/google/android/youtube/core/player/overlay/PlayerOverlaysLayout$LayoutParams;
    .locals 1

    const/4 v0, 0x0

    return-object v0
.end method

.method public final d()V
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/api/service/a/de;->a:Lcom/google/android/youtube/api/jar/client/cn;

    if-eqz v0, :cond_0

    :try_start_0
    iget-object v0, p0, Lcom/google/android/youtube/api/service/a/de;->a:Lcom/google/android/youtube/api/jar/client/cn;

    invoke-interface {v0}, Lcom/google/android/youtube/api/jar/client/cn;->a()V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    :cond_0
    :goto_0
    return-void

    :catch_0
    move-exception v0

    goto :goto_0
.end method

.method public final e()V
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/api/service/a/de;->a:Lcom/google/android/youtube/api/jar/client/cn;

    if-eqz v0, :cond_0

    :try_start_0
    iget-object v0, p0, Lcom/google/android/youtube/api/service/a/de;->a:Lcom/google/android/youtube/api/jar/client/cn;

    invoke-interface {v0}, Lcom/google/android/youtube/api/jar/client/cn;->b()V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    :cond_0
    :goto_0
    return-void

    :catch_0
    move-exception v0

    goto :goto_0
.end method

.method public final f()V
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/api/service/a/de;->a:Lcom/google/android/youtube/api/jar/client/cn;

    if-eqz v0, :cond_0

    :try_start_0
    iget-object v0, p0, Lcom/google/android/youtube/api/service/a/de;->a:Lcom/google/android/youtube/api/jar/client/cn;

    invoke-interface {v0}, Lcom/google/android/youtube/api/jar/client/cn;->c()V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    :cond_0
    :goto_0
    return-void

    :catch_0
    move-exception v0

    goto :goto_0
.end method

.method public final setThumbnail(Landroid/graphics/Bitmap;)V
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/api/service/a/de;->a:Lcom/google/android/youtube/api/jar/client/cn;

    if-eqz v0, :cond_0

    :try_start_0
    iget-object v0, p0, Lcom/google/android/youtube/api/service/a/de;->a:Lcom/google/android/youtube/api/jar/client/cn;

    invoke-interface {v0, p1}, Lcom/google/android/youtube/api/jar/client/cn;->a(Landroid/graphics/Bitmap;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    :cond_0
    :goto_0
    return-void

    :catch_0
    move-exception v0

    goto :goto_0
.end method
