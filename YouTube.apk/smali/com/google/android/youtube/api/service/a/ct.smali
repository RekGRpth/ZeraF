.class final Lcom/google/android/youtube/api/service/a/ct;
.super Lcom/google/android/youtube/api/service/a/co;
.source "SourceFile"


# instance fields
.field final synthetic b:Lcom/google/android/youtube/api/service/a/cn;

.field private final c:Ljava/lang/reflect/Constructor;

.field private final d:Ljava/lang/reflect/Constructor;

.field private final e:Ljava/lang/reflect/Method;


# direct methods
.method public constructor <init>(Lcom/google/android/youtube/api/service/a/cn;)V
    .locals 7

    const/4 v6, 0x0

    const/4 v5, 0x1

    iput-object p1, p0, Lcom/google/android/youtube/api/service/a/ct;->b:Lcom/google/android/youtube/api/service/a/cn;

    invoke-direct {p0, p1, v6}, Lcom/google/android/youtube/api/service/a/co;-><init>(Lcom/google/android/youtube/api/service/a/cn;B)V

    const-string v0, "android.view.IWindow"

    invoke-static {v0}, Ljava/lang/Class;->forName(Ljava/lang/String;)Ljava/lang/Class;

    move-result-object v0

    const-string v1, "android.view.IWindowSession$Stub$Proxy"

    invoke-static {v1}, Ljava/lang/Class;->forName(Ljava/lang/String;)Ljava/lang/Class;

    move-result-object v1

    const-string v2, "android.view.IWindow$Stub$Proxy"

    invoke-static {v2}, Ljava/lang/Class;->forName(Ljava/lang/String;)Ljava/lang/Class;

    move-result-object v2

    new-array v3, v5, [Ljava/lang/Class;

    const-string v4, "android.os.IBinder"

    invoke-static {v4}, Ljava/lang/Class;->forName(Ljava/lang/String;)Ljava/lang/Class;

    move-result-object v4

    aput-object v4, v3, v6

    invoke-virtual {v2, v3}, Ljava/lang/Class;->getDeclaredConstructor([Ljava/lang/Class;)Ljava/lang/reflect/Constructor;

    move-result-object v2

    iput-object v2, p0, Lcom/google/android/youtube/api/service/a/ct;->d:Ljava/lang/reflect/Constructor;

    iget-object v2, p0, Lcom/google/android/youtube/api/service/a/ct;->d:Ljava/lang/reflect/Constructor;

    invoke-virtual {v2, v5}, Ljava/lang/reflect/Constructor;->setAccessible(Z)V

    new-array v2, v5, [Ljava/lang/Class;

    const-string v3, "android.os.IBinder"

    invoke-static {v3}, Ljava/lang/Class;->forName(Ljava/lang/String;)Ljava/lang/Class;

    move-result-object v3

    aput-object v3, v2, v6

    invoke-virtual {v1, v2}, Ljava/lang/Class;->getDeclaredConstructor([Ljava/lang/Class;)Ljava/lang/reflect/Constructor;

    move-result-object v2

    iput-object v2, p0, Lcom/google/android/youtube/api/service/a/ct;->c:Ljava/lang/reflect/Constructor;

    iget-object v2, p0, Lcom/google/android/youtube/api/service/a/ct;->c:Ljava/lang/reflect/Constructor;

    invoke-virtual {v2, v5}, Ljava/lang/reflect/Constructor;->setAccessible(Z)V

    const-string v2, "relayout"

    const/16 v3, 0xb

    new-array v3, v3, [Ljava/lang/Class;

    aput-object v0, v3, v6

    const-class v0, Landroid/view/WindowManager$LayoutParams;

    aput-object v0, v3, v5

    const/4 v0, 0x2

    sget-object v4, Ljava/lang/Integer;->TYPE:Ljava/lang/Class;

    aput-object v4, v3, v0

    const/4 v0, 0x3

    sget-object v4, Ljava/lang/Integer;->TYPE:Ljava/lang/Class;

    aput-object v4, v3, v0

    const/4 v0, 0x4

    sget-object v4, Ljava/lang/Integer;->TYPE:Ljava/lang/Class;

    aput-object v4, v3, v0

    const/4 v0, 0x5

    sget-object v4, Ljava/lang/Boolean;->TYPE:Ljava/lang/Class;

    aput-object v4, v3, v0

    const/4 v0, 0x6

    const-class v4, Landroid/graphics/Rect;

    aput-object v4, v3, v0

    const/4 v0, 0x7

    const-class v4, Landroid/graphics/Rect;

    aput-object v4, v3, v0

    const/16 v0, 0x8

    const-class v4, Landroid/graphics/Rect;

    aput-object v4, v3, v0

    const/16 v0, 0x9

    const-class v4, Landroid/content/res/Configuration;

    aput-object v4, v3, v0

    const/16 v0, 0xa

    const-class v4, Landroid/view/Surface;

    aput-object v4, v3, v0

    invoke-virtual {v1, v2, v3}, Ljava/lang/Class;->getDeclaredMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/youtube/api/service/a/ct;->e:Ljava/lang/reflect/Method;

    iget-object v0, p0, Lcom/google/android/youtube/api/service/a/ct;->e:Ljava/lang/reflect/Method;

    invoke-virtual {v0, v5}, Ljava/lang/reflect/Method;->setAccessible(Z)V

    return-void
.end method

.method static synthetic a(Lcom/google/android/youtube/api/service/a/ct;)Ljava/lang/reflect/Constructor;
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/api/service/a/ct;->d:Ljava/lang/reflect/Constructor;

    return-object v0
.end method

.method static synthetic b(Lcom/google/android/youtube/api/service/a/ct;)Ljava/lang/reflect/Constructor;
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/api/service/a/ct;->c:Ljava/lang/reflect/Constructor;

    return-object v0
.end method

.method static synthetic c(Lcom/google/android/youtube/api/service/a/ct;)Ljava/lang/reflect/Method;
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/api/service/a/ct;->e:Ljava/lang/reflect/Method;

    return-object v0
.end method


# virtual methods
.method public final a(Landroid/os/IBinder;Landroid/os/IBinder;Landroid/view/WindowManager$LayoutParams;IIIZLandroid/graphics/Rect;Landroid/graphics/Rect;Landroid/graphics/Rect;Landroid/content/res/Configuration;Landroid/view/Surface;)I
    .locals 18

    new-instance v16, Landroid/os/ConditionVariable;

    invoke-direct/range {v16 .. v16}, Landroid/os/ConditionVariable;-><init>()V

    new-instance v5, Ljava/util/concurrent/atomic/AtomicInteger;

    invoke-direct {v5}, Ljava/util/concurrent/atomic/AtomicInteger;-><init>()V

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/google/android/youtube/api/service/a/ct;->b:Lcom/google/android/youtube/api/service/a/cn;

    invoke-static {v1}, Lcom/google/android/youtube/api/service/a/cn;->a(Lcom/google/android/youtube/api/service/a/cn;)Landroid/os/Handler;

    move-result-object v17

    new-instance v1, Lcom/google/android/youtube/api/service/a/cv;

    move-object/from16 v2, p0

    move-object/from16 v3, p2

    move-object/from16 v4, p1

    move-object/from16 v6, p3

    move/from16 v7, p4

    move/from16 v8, p5

    move/from16 v9, p6

    move/from16 v10, p7

    move-object/from16 v11, p8

    move-object/from16 v12, p9

    move-object/from16 v13, p10

    move-object/from16 v14, p11

    move-object/from16 v15, p12

    invoke-direct/range {v1 .. v16}, Lcom/google/android/youtube/api/service/a/cv;-><init>(Lcom/google/android/youtube/api/service/a/ct;Landroid/os/IBinder;Landroid/os/IBinder;Ljava/util/concurrent/atomic/AtomicInteger;Landroid/view/WindowManager$LayoutParams;IIIZLandroid/graphics/Rect;Landroid/graphics/Rect;Landroid/graphics/Rect;Landroid/content/res/Configuration;Landroid/view/Surface;Landroid/os/ConditionVariable;)V

    move-object/from16 v0, v17

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    invoke-virtual/range {v16 .. v16}, Landroid/os/ConditionVariable;->block()V

    invoke-virtual {v5}, Ljava/util/concurrent/atomic/AtomicInteger;->get()I

    move-result v1

    return v1
.end method

.method public final a(Landroid/view/Surface;)V
    .locals 2

    iget-object v0, p0, Lcom/google/android/youtube/api/service/a/ct;->b:Lcom/google/android/youtube/api/service/a/cn;

    invoke-static {v0}, Lcom/google/android/youtube/api/service/a/cn;->a(Lcom/google/android/youtube/api/service/a/cn;)Landroid/os/Handler;

    move-result-object v0

    new-instance v1, Lcom/google/android/youtube/api/service/a/cu;

    invoke-direct {v1, p0}, Lcom/google/android/youtube/api/service/a/cu;-><init>(Lcom/google/android/youtube/api/service/a/ct;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    return-void
.end method
