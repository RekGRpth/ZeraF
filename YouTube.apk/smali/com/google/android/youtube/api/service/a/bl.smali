.class public final Lcom/google/android/youtube/api/service/a/bl;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/youtube/core/player/overlay/c;


# instance fields
.field private final a:Lcom/google/android/youtube/api/service/a/bm;

.field private b:Lcom/google/android/youtube/api/jar/client/bp;


# direct methods
.method public constructor <init>(Landroid/os/Handler;Lcom/google/android/youtube/api/jar/client/bp;)V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const-string v0, "client cannot be null"

    invoke-static {p2, v0}, Lcom/google/android/youtube/core/utils/s;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/youtube/api/jar/client/bp;

    iput-object v0, p0, Lcom/google/android/youtube/api/service/a/bl;->b:Lcom/google/android/youtube/api/jar/client/bp;

    new-instance v0, Lcom/google/android/youtube/api/service/a/bm;

    invoke-direct {v0, p1}, Lcom/google/android/youtube/api/service/a/bm;-><init>(Landroid/os/Handler;)V

    iput-object v0, p0, Lcom/google/android/youtube/api/service/a/bl;->a:Lcom/google/android/youtube/api/service/a/bm;

    :try_start_0
    iget-object v0, p0, Lcom/google/android/youtube/api/service/a/bl;->a:Lcom/google/android/youtube/api/service/a/bm;

    invoke-interface {p2, v0}, Lcom/google/android/youtube/api/jar/client/bp;->a(Lcom/google/android/youtube/api/service/a/an;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    return-void

    :catch_0
    move-exception v0

    goto :goto_0
.end method


# virtual methods
.method public final a()V
    .locals 1

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/youtube/api/service/a/bl;->b:Lcom/google/android/youtube/api/jar/client/bp;

    return-void
.end method

.method public final b()Landroid/view/View;
    .locals 1

    const/4 v0, 0x0

    return-object v0
.end method

.method public final c()Lcom/google/android/youtube/core/player/overlay/PlayerOverlaysLayout$LayoutParams;
    .locals 1

    const/4 v0, 0x0

    return-object v0
.end method

.method public final d()V
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/api/service/a/bl;->b:Lcom/google/android/youtube/api/jar/client/bp;

    if-eqz v0, :cond_0

    :try_start_0
    iget-object v0, p0, Lcom/google/android/youtube/api/service/a/bl;->b:Lcom/google/android/youtube/api/jar/client/bp;

    invoke-interface {v0}, Lcom/google/android/youtube/api/jar/client/bp;->b()V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    :cond_0
    :goto_0
    return-void

    :catch_0
    move-exception v0

    goto :goto_0
.end method

.method public final e()V
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/api/service/a/bl;->b:Lcom/google/android/youtube/api/jar/client/bp;

    if-eqz v0, :cond_0

    :try_start_0
    iget-object v0, p0, Lcom/google/android/youtube/api/service/a/bl;->b:Lcom/google/android/youtube/api/jar/client/bp;

    invoke-interface {v0}, Lcom/google/android/youtube/api/jar/client/bp;->c()V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    :cond_0
    :goto_0
    return-void

    :catch_0
    move-exception v0

    goto :goto_0
.end method

.method public final setListener(Lcom/google/android/youtube/core/player/overlay/d;)V
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/api/service/a/bl;->a:Lcom/google/android/youtube/api/service/a/bm;

    invoke-virtual {v0, p1}, Lcom/google/android/youtube/api/service/a/bm;->a(Lcom/google/android/youtube/core/player/overlay/d;)V

    return-void
.end method

.method public final setWatermark(Landroid/graphics/Bitmap;Z)V
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/api/service/a/bl;->b:Lcom/google/android/youtube/api/jar/client/bp;

    if-eqz v0, :cond_0

    :try_start_0
    iget-object v0, p0, Lcom/google/android/youtube/api/service/a/bl;->b:Lcom/google/android/youtube/api/jar/client/bp;

    invoke-interface {v0, p1, p2}, Lcom/google/android/youtube/api/jar/client/bp;->a(Landroid/graphics/Bitmap;Z)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    :cond_0
    :goto_0
    return-void

    :catch_0
    move-exception v0

    goto :goto_0
.end method
