.class public final Lcom/google/android/youtube/api/service/a/cw;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field private final a:Lcom/google/android/youtube/api/service/a/cy;

.field private b:Lcom/google/android/youtube/api/jar/client/ck;


# direct methods
.method public constructor <init>(Landroid/os/Handler;Lcom/google/android/youtube/api/jar/client/ck;)V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const-string v0, "client cannot be null"

    invoke-static {p2, v0}, Lcom/google/android/youtube/core/utils/s;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/youtube/api/jar/client/ck;

    iput-object v0, p0, Lcom/google/android/youtube/api/service/a/cw;->b:Lcom/google/android/youtube/api/jar/client/ck;

    new-instance v0, Lcom/google/android/youtube/api/service/a/cy;

    invoke-direct {v0, p0, p1}, Lcom/google/android/youtube/api/service/a/cy;-><init>(Lcom/google/android/youtube/api/service/a/cw;Landroid/os/Handler;)V

    iput-object v0, p0, Lcom/google/android/youtube/api/service/a/cw;->a:Lcom/google/android/youtube/api/service/a/cy;

    :try_start_0
    iget-object v0, p0, Lcom/google/android/youtube/api/service/a/cw;->a:Lcom/google/android/youtube/api/service/a/cy;

    invoke-interface {p2, v0}, Lcom/google/android/youtube/api/jar/client/ck;->a(Lcom/google/android/youtube/api/service/a/bc;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    return-void

    :catch_0
    move-exception v0

    goto :goto_0
.end method


# virtual methods
.method public final a()V
    .locals 1

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/youtube/api/service/a/cw;->b:Lcom/google/android/youtube/api/jar/client/ck;

    return-void
.end method

.method public final a(Lcom/google/android/youtube/api/service/a/cx;)V
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/api/service/a/cw;->a:Lcom/google/android/youtube/api/service/a/cy;

    invoke-virtual {v0, p1}, Lcom/google/android/youtube/api/service/a/cy;->a(Lcom/google/android/youtube/api/service/a/cx;)V

    return-void
.end method
