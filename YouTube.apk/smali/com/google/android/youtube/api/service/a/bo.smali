.class public final Lcom/google/android/youtube/api/service/a/bo;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/youtube/core/player/overlay/ControllerOverlay;


# instance fields
.field private final a:Landroid/content/Context;

.field private final b:Lcom/google/android/youtube/api/service/a/bp;

.field private c:Lcom/google/android/youtube/api/jar/client/bs;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/os/Handler;Lcom/google/android/youtube/api/jar/client/bs;)V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const-string v0, "context cannot be null"

    invoke-static {p1, v0}, Lcom/google/android/youtube/core/utils/s;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/Context;

    iput-object v0, p0, Lcom/google/android/youtube/api/service/a/bo;->a:Landroid/content/Context;

    const-string v0, "uiHandler cannot be null"

    invoke-static {p2, v0}, Lcom/google/android/youtube/core/utils/s;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const-string v0, "client cannot be null"

    invoke-static {p3, v0}, Lcom/google/android/youtube/core/utils/s;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/youtube/api/jar/client/bs;

    iput-object v0, p0, Lcom/google/android/youtube/api/service/a/bo;->c:Lcom/google/android/youtube/api/jar/client/bs;

    new-instance v0, Lcom/google/android/youtube/api/service/a/bp;

    invoke-direct {v0, p2}, Lcom/google/android/youtube/api/service/a/bp;-><init>(Landroid/os/Handler;)V

    iput-object v0, p0, Lcom/google/android/youtube/api/service/a/bo;->b:Lcom/google/android/youtube/api/service/a/bp;

    :try_start_0
    iget-object v0, p0, Lcom/google/android/youtube/api/service/a/bo;->b:Lcom/google/android/youtube/api/service/a/bp;

    invoke-interface {p3, v0}, Lcom/google/android/youtube/api/jar/client/bs;->a(Lcom/google/android/youtube/api/service/a/aq;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    return-void

    :catch_0
    move-exception v0

    goto :goto_0
.end method


# virtual methods
.method public final a()V
    .locals 1

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/youtube/api/service/a/bo;->c:Lcom/google/android/youtube/api/jar/client/bs;

    return-void
.end method

.method public final a(Ljava/lang/String;Z)V
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/api/service/a/bo;->c:Lcom/google/android/youtube/api/jar/client/bs;

    if-eqz v0, :cond_0

    :try_start_0
    iget-object v0, p0, Lcom/google/android/youtube/api/service/a/bo;->c:Lcom/google/android/youtube/api/jar/client/bs;

    invoke-interface {v0, p1, p2}, Lcom/google/android/youtube/api/jar/client/bs;->a(Ljava/lang/String;Z)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    :cond_0
    :goto_0
    return-void

    :catch_0
    move-exception v0

    goto :goto_0
.end method

.method public final a(Ljava/util/List;)V
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/api/service/a/bo;->c:Lcom/google/android/youtube/api/jar/client/bs;

    if-eqz v0, :cond_0

    :try_start_0
    iget-object v0, p0, Lcom/google/android/youtube/api/service/a/bo;->c:Lcom/google/android/youtube/api/jar/client/bs;

    invoke-interface {v0, p1}, Lcom/google/android/youtube/api/jar/client/bs;->a(Ljava/util/List;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    :cond_0
    :goto_0
    return-void

    :catch_0
    move-exception v0

    goto :goto_0
.end method

.method public final b()Landroid/view/View;
    .locals 1

    const/4 v0, 0x0

    return-object v0
.end method

.method public final c()Lcom/google/android/youtube/core/player/overlay/PlayerOverlaysLayout$LayoutParams;
    .locals 1

    const/4 v0, 0x0

    return-object v0
.end method

.method public final d()V
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/api/service/a/bo;->c:Lcom/google/android/youtube/api/jar/client/bs;

    if-eqz v0, :cond_0

    :try_start_0
    iget-object v0, p0, Lcom/google/android/youtube/api/service/a/bo;->c:Lcom/google/android/youtube/api/jar/client/bs;

    invoke-interface {v0}, Lcom/google/android/youtube/api/jar/client/bs;->f()V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    :cond_0
    :goto_0
    return-void

    :catch_0
    move-exception v0

    goto :goto_0
.end method

.method public final e()V
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/api/service/a/bo;->c:Lcom/google/android/youtube/api/jar/client/bs;

    if-eqz v0, :cond_0

    :try_start_0
    iget-object v0, p0, Lcom/google/android/youtube/api/service/a/bo;->c:Lcom/google/android/youtube/api/jar/client/bs;

    invoke-interface {v0}, Lcom/google/android/youtube/api/jar/client/bs;->g()V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    :cond_0
    :goto_0
    return-void

    :catch_0
    move-exception v0

    goto :goto_0
.end method

.method public final f()V
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/api/service/a/bo;->c:Lcom/google/android/youtube/api/jar/client/bs;

    if-eqz v0, :cond_0

    :try_start_0
    iget-object v0, p0, Lcom/google/android/youtube/api/service/a/bo;->c:Lcom/google/android/youtube/api/jar/client/bs;

    invoke-interface {v0}, Lcom/google/android/youtube/api/jar/client/bs;->h()V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    :cond_0
    :goto_0
    return-void

    :catch_0
    move-exception v0

    goto :goto_0
.end method

.method public final g()V
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/api/service/a/bo;->c:Lcom/google/android/youtube/api/jar/client/bs;

    if-eqz v0, :cond_0

    :try_start_0
    iget-object v0, p0, Lcom/google/android/youtube/api/service/a/bo;->c:Lcom/google/android/youtube/api/jar/client/bs;

    invoke-interface {v0}, Lcom/google/android/youtube/api/jar/client/bs;->b()V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    :cond_0
    :goto_0
    return-void

    :catch_0
    move-exception v0

    goto :goto_0
.end method

.method public final h()V
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/api/service/a/bo;->c:Lcom/google/android/youtube/api/jar/client/bs;

    if-eqz v0, :cond_0

    :try_start_0
    iget-object v0, p0, Lcom/google/android/youtube/api/service/a/bo;->c:Lcom/google/android/youtube/api/jar/client/bs;

    invoke-interface {v0}, Lcom/google/android/youtube/api/jar/client/bs;->c()V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    :cond_0
    :goto_0
    return-void

    :catch_0
    move-exception v0

    goto :goto_0
.end method

.method public final i()V
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/api/service/a/bo;->c:Lcom/google/android/youtube/api/jar/client/bs;

    if-eqz v0, :cond_0

    :try_start_0
    iget-object v0, p0, Lcom/google/android/youtube/api/service/a/bo;->c:Lcom/google/android/youtube/api/jar/client/bs;

    invoke-interface {v0}, Lcom/google/android/youtube/api/jar/client/bs;->i()V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    :cond_0
    :goto_0
    return-void

    :catch_0
    move-exception v0

    goto :goto_0
.end method

.method public final onKeyDown(ILandroid/view/KeyEvent;)Z
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/api/service/a/bo;->c:Lcom/google/android/youtube/api/jar/client/bs;

    if-eqz v0, :cond_0

    :try_start_0
    iget-object v0, p0, Lcom/google/android/youtube/api/service/a/bo;->c:Lcom/google/android/youtube/api/jar/client/bs;

    invoke-interface {v0, p1, p2}, Lcom/google/android/youtube/api/jar/client/bs;->a(ILandroid/view/KeyEvent;)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    :cond_0
    :goto_0
    const/4 v0, 0x1

    return v0

    :catch_0
    move-exception v0

    goto :goto_0
.end method

.method public final onKeyUp(ILandroid/view/KeyEvent;)Z
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/api/service/a/bo;->c:Lcom/google/android/youtube/api/jar/client/bs;

    if-eqz v0, :cond_0

    :try_start_0
    iget-object v0, p0, Lcom/google/android/youtube/api/service/a/bo;->c:Lcom/google/android/youtube/api/jar/client/bs;

    invoke-interface {v0, p1, p2}, Lcom/google/android/youtube/api/jar/client/bs;->b(ILandroid/view/KeyEvent;)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    :cond_0
    :goto_0
    const/4 v0, 0x1

    return v0

    :catch_0
    move-exception v0

    goto :goto_0
.end method

.method public final setCcEnabled(Z)V
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/api/service/a/bo;->c:Lcom/google/android/youtube/api/jar/client/bs;

    if-eqz v0, :cond_0

    :try_start_0
    iget-object v0, p0, Lcom/google/android/youtube/api/service/a/bo;->c:Lcom/google/android/youtube/api/jar/client/bs;

    invoke-interface {v0, p1}, Lcom/google/android/youtube/api/jar/client/bs;->j(Z)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    :cond_0
    :goto_0
    return-void

    :catch_0
    move-exception v0

    goto :goto_0
.end method

.method public final setFullscreen(Z)V
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/api/service/a/bo;->c:Lcom/google/android/youtube/api/jar/client/bs;

    if-eqz v0, :cond_0

    :try_start_0
    iget-object v0, p0, Lcom/google/android/youtube/api/service/a/bo;->c:Lcom/google/android/youtube/api/jar/client/bs;

    invoke-interface {v0, p1}, Lcom/google/android/youtube/api/jar/client/bs;->d(Z)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    :cond_0
    :goto_0
    return-void

    :catch_0
    move-exception v0

    goto :goto_0
.end method

.method public final setHQ(Z)V
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/api/service/a/bo;->c:Lcom/google/android/youtube/api/jar/client/bs;

    if-eqz v0, :cond_0

    :try_start_0
    iget-object v0, p0, Lcom/google/android/youtube/api/service/a/bo;->c:Lcom/google/android/youtube/api/jar/client/bs;

    invoke-interface {v0, p1}, Lcom/google/android/youtube/api/jar/client/bs;->b(Z)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    :cond_0
    :goto_0
    return-void

    :catch_0
    move-exception v0

    goto :goto_0
.end method

.method public final setHQisHD(Z)V
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/api/service/a/bo;->c:Lcom/google/android/youtube/api/jar/client/bs;

    if-eqz v0, :cond_0

    :try_start_0
    iget-object v0, p0, Lcom/google/android/youtube/api/service/a/bo;->c:Lcom/google/android/youtube/api/jar/client/bs;

    invoke-interface {v0, p1}, Lcom/google/android/youtube/api/jar/client/bs;->c(Z)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    :cond_0
    :goto_0
    return-void

    :catch_0
    move-exception v0

    goto :goto_0
.end method

.method public final setHasCc(Z)V
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/api/service/a/bo;->c:Lcom/google/android/youtube/api/jar/client/bs;

    if-eqz v0, :cond_0

    :try_start_0
    iget-object v0, p0, Lcom/google/android/youtube/api/service/a/bo;->c:Lcom/google/android/youtube/api/jar/client/bs;

    invoke-interface {v0, p1}, Lcom/google/android/youtube/api/jar/client/bs;->i(Z)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    :cond_0
    :goto_0
    return-void

    :catch_0
    move-exception v0

    goto :goto_0
.end method

.method public final setHasNext(Z)V
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/api/service/a/bo;->c:Lcom/google/android/youtube/api/jar/client/bs;

    if-eqz v0, :cond_0

    :try_start_0
    iget-object v0, p0, Lcom/google/android/youtube/api/service/a/bo;->c:Lcom/google/android/youtube/api/jar/client/bs;

    invoke-interface {v0, p1}, Lcom/google/android/youtube/api/jar/client/bs;->f(Z)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    :cond_0
    :goto_0
    return-void

    :catch_0
    move-exception v0

    goto :goto_0
.end method

.method public final setHasPrevious(Z)V
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/api/service/a/bo;->c:Lcom/google/android/youtube/api/jar/client/bs;

    if-eqz v0, :cond_0

    :try_start_0
    iget-object v0, p0, Lcom/google/android/youtube/api/service/a/bo;->c:Lcom/google/android/youtube/api/jar/client/bs;

    invoke-interface {v0, p1}, Lcom/google/android/youtube/api/jar/client/bs;->g(Z)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    :cond_0
    :goto_0
    return-void

    :catch_0
    move-exception v0

    goto :goto_0
.end method

.method public final setListener(Lcom/google/android/youtube/core/player/overlay/e;)V
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/api/service/a/bo;->b:Lcom/google/android/youtube/api/service/a/bp;

    invoke-virtual {v0, p1}, Lcom/google/android/youtube/api/service/a/bp;->a(Lcom/google/android/youtube/core/player/overlay/e;)V

    return-void
.end method

.method public final setLoading()V
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/api/service/a/bo;->c:Lcom/google/android/youtube/api/jar/client/bs;

    if-eqz v0, :cond_0

    :try_start_0
    iget-object v0, p0, Lcom/google/android/youtube/api/service/a/bo;->c:Lcom/google/android/youtube/api/jar/client/bs;

    invoke-interface {v0}, Lcom/google/android/youtube/api/jar/client/bs;->e()V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    :cond_0
    :goto_0
    return-void

    :catch_0
    move-exception v0

    goto :goto_0
.end method

.method public final setPlaying()V
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/api/service/a/bo;->c:Lcom/google/android/youtube/api/jar/client/bs;

    if-eqz v0, :cond_0

    :try_start_0
    iget-object v0, p0, Lcom/google/android/youtube/api/service/a/bo;->c:Lcom/google/android/youtube/api/jar/client/bs;

    invoke-interface {v0}, Lcom/google/android/youtube/api/jar/client/bs;->d()V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    :cond_0
    :goto_0
    return-void

    :catch_0
    move-exception v0

    goto :goto_0
.end method

.method public final setScrubbingEnabled(Z)V
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/api/service/a/bo;->c:Lcom/google/android/youtube/api/jar/client/bs;

    if-eqz v0, :cond_0

    :try_start_0
    iget-object v0, p0, Lcom/google/android/youtube/api/service/a/bo;->c:Lcom/google/android/youtube/api/jar/client/bs;

    invoke-interface {v0, p1}, Lcom/google/android/youtube/api/jar/client/bs;->h(Z)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    :cond_0
    :goto_0
    return-void

    :catch_0
    move-exception v0

    goto :goto_0
.end method

.method public final setShowFullscreen(Z)V
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/api/service/a/bo;->c:Lcom/google/android/youtube/api/jar/client/bs;

    if-eqz v0, :cond_0

    :try_start_0
    iget-object v0, p0, Lcom/google/android/youtube/api/service/a/bo;->c:Lcom/google/android/youtube/api/jar/client/bs;

    invoke-interface {v0, p1}, Lcom/google/android/youtube/api/jar/client/bs;->e(Z)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    :cond_0
    :goto_0
    return-void

    :catch_0
    move-exception v0

    goto :goto_0
.end method

.method public final setStyle(Lcom/google/android/youtube/core/player/overlay/ControllerOverlay$Style;)V
    .locals 2

    iget-object v0, p0, Lcom/google/android/youtube/api/service/a/bo;->c:Lcom/google/android/youtube/api/jar/client/bs;

    if-eqz v0, :cond_0

    :try_start_0
    iget-object v0, p0, Lcom/google/android/youtube/api/service/a/bo;->c:Lcom/google/android/youtube/api/jar/client/bs;

    invoke-virtual {p1}, Lcom/google/android/youtube/core/player/overlay/ControllerOverlay$Style;->name()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/google/android/youtube/api/jar/client/bs;->a(Ljava/lang/String;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    :cond_0
    :goto_0
    return-void

    :catch_0
    move-exception v0

    goto :goto_0
.end method

.method public final setSupportsQualityToggle(Z)V
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/api/service/a/bo;->c:Lcom/google/android/youtube/api/jar/client/bs;

    if-eqz v0, :cond_0

    :try_start_0
    iget-object v0, p0, Lcom/google/android/youtube/api/service/a/bo;->c:Lcom/google/android/youtube/api/jar/client/bs;

    invoke-interface {v0, p1}, Lcom/google/android/youtube/api/jar/client/bs;->a(Z)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    :cond_0
    :goto_0
    return-void

    :catch_0
    move-exception v0

    goto :goto_0
.end method

.method public final setTimes(III)V
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/api/service/a/bo;->c:Lcom/google/android/youtube/api/jar/client/bs;

    if-eqz v0, :cond_0

    :try_start_0
    iget-object v0, p0, Lcom/google/android/youtube/api/service/a/bo;->c:Lcom/google/android/youtube/api/jar/client/bs;

    invoke-interface {v0, p1, p2, p3}, Lcom/google/android/youtube/api/jar/client/bs;->a(III)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    :cond_0
    :goto_0
    return-void

    :catch_0
    move-exception v0

    goto :goto_0
.end method
