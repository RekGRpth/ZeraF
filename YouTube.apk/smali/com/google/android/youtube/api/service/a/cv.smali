.class final Lcom/google/android/youtube/api/service/a/cv;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field final synthetic a:Landroid/os/IBinder;

.field final synthetic b:Landroid/os/IBinder;

.field final synthetic c:Ljava/util/concurrent/atomic/AtomicInteger;

.field final synthetic d:Landroid/view/WindowManager$LayoutParams;

.field final synthetic e:I

.field final synthetic f:I

.field final synthetic g:I

.field final synthetic h:Z

.field final synthetic i:Landroid/graphics/Rect;

.field final synthetic j:Landroid/graphics/Rect;

.field final synthetic k:Landroid/graphics/Rect;

.field final synthetic l:Landroid/content/res/Configuration;

.field final synthetic m:Landroid/view/Surface;

.field final synthetic n:Landroid/os/ConditionVariable;

.field final synthetic o:Lcom/google/android/youtube/api/service/a/ct;


# direct methods
.method constructor <init>(Lcom/google/android/youtube/api/service/a/ct;Landroid/os/IBinder;Landroid/os/IBinder;Ljava/util/concurrent/atomic/AtomicInteger;Landroid/view/WindowManager$LayoutParams;IIIZLandroid/graphics/Rect;Landroid/graphics/Rect;Landroid/graphics/Rect;Landroid/content/res/Configuration;Landroid/view/Surface;Landroid/os/ConditionVariable;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/youtube/api/service/a/cv;->o:Lcom/google/android/youtube/api/service/a/ct;

    iput-object p2, p0, Lcom/google/android/youtube/api/service/a/cv;->a:Landroid/os/IBinder;

    iput-object p3, p0, Lcom/google/android/youtube/api/service/a/cv;->b:Landroid/os/IBinder;

    iput-object p4, p0, Lcom/google/android/youtube/api/service/a/cv;->c:Ljava/util/concurrent/atomic/AtomicInteger;

    iput-object p5, p0, Lcom/google/android/youtube/api/service/a/cv;->d:Landroid/view/WindowManager$LayoutParams;

    iput p6, p0, Lcom/google/android/youtube/api/service/a/cv;->e:I

    iput p7, p0, Lcom/google/android/youtube/api/service/a/cv;->f:I

    iput p8, p0, Lcom/google/android/youtube/api/service/a/cv;->g:I

    iput-boolean p9, p0, Lcom/google/android/youtube/api/service/a/cv;->h:Z

    iput-object p10, p0, Lcom/google/android/youtube/api/service/a/cv;->i:Landroid/graphics/Rect;

    iput-object p11, p0, Lcom/google/android/youtube/api/service/a/cv;->j:Landroid/graphics/Rect;

    iput-object p12, p0, Lcom/google/android/youtube/api/service/a/cv;->k:Landroid/graphics/Rect;

    iput-object p13, p0, Lcom/google/android/youtube/api/service/a/cv;->l:Landroid/content/res/Configuration;

    iput-object p14, p0, Lcom/google/android/youtube/api/service/a/cv;->m:Landroid/view/Surface;

    iput-object p15, p0, Lcom/google/android/youtube/api/service/a/cv;->n:Landroid/os/ConditionVariable;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final run()V
    .locals 6

    :try_start_0
    iget-object v0, p0, Lcom/google/android/youtube/api/service/a/cv;->o:Lcom/google/android/youtube/api/service/a/ct;

    invoke-static {v0}, Lcom/google/android/youtube/api/service/a/ct;->a(Lcom/google/android/youtube/api/service/a/ct;)Ljava/lang/reflect/Constructor;

    move-result-object v0

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    iget-object v3, p0, Lcom/google/android/youtube/api/service/a/cv;->a:Landroid/os/IBinder;

    aput-object v3, v1, v2

    invoke-virtual {v0, v1}, Ljava/lang/reflect/Constructor;->newInstance([Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/youtube/api/service/a/cv;->o:Lcom/google/android/youtube/api/service/a/ct;

    invoke-static {v1}, Lcom/google/android/youtube/api/service/a/ct;->b(Lcom/google/android/youtube/api/service/a/ct;)Ljava/lang/reflect/Constructor;

    move-result-object v1

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    iget-object v4, p0, Lcom/google/android/youtube/api/service/a/cv;->b:Landroid/os/IBinder;

    aput-object v4, v2, v3

    invoke-virtual {v1, v2}, Ljava/lang/reflect/Constructor;->newInstance([Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/youtube/api/service/a/cv;->c:Ljava/util/concurrent/atomic/AtomicInteger;

    iget-object v3, p0, Lcom/google/android/youtube/api/service/a/cv;->o:Lcom/google/android/youtube/api/service/a/ct;

    invoke-static {v3}, Lcom/google/android/youtube/api/service/a/ct;->c(Lcom/google/android/youtube/api/service/a/ct;)Ljava/lang/reflect/Method;

    move-result-object v3

    const/16 v4, 0xb

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    aput-object v0, v4, v5

    const/4 v0, 0x1

    iget-object v5, p0, Lcom/google/android/youtube/api/service/a/cv;->d:Landroid/view/WindowManager$LayoutParams;

    aput-object v5, v4, v0

    const/4 v0, 0x2

    iget v5, p0, Lcom/google/android/youtube/api/service/a/cv;->e:I

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v4, v0

    const/4 v0, 0x3

    iget v5, p0, Lcom/google/android/youtube/api/service/a/cv;->f:I

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v4, v0

    const/4 v0, 0x4

    iget v5, p0, Lcom/google/android/youtube/api/service/a/cv;->g:I

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v4, v0

    const/4 v0, 0x5

    iget-boolean v5, p0, Lcom/google/android/youtube/api/service/a/cv;->h:Z

    invoke-static {v5}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v5

    aput-object v5, v4, v0

    const/4 v0, 0x6

    iget-object v5, p0, Lcom/google/android/youtube/api/service/a/cv;->i:Landroid/graphics/Rect;

    aput-object v5, v4, v0

    const/4 v0, 0x7

    iget-object v5, p0, Lcom/google/android/youtube/api/service/a/cv;->j:Landroid/graphics/Rect;

    aput-object v5, v4, v0

    const/16 v0, 0x8

    iget-object v5, p0, Lcom/google/android/youtube/api/service/a/cv;->k:Landroid/graphics/Rect;

    aput-object v5, v4, v0

    const/16 v0, 0x9

    iget-object v5, p0, Lcom/google/android/youtube/api/service/a/cv;->l:Landroid/content/res/Configuration;

    aput-object v5, v4, v0

    const/16 v0, 0xa

    iget-object v5, p0, Lcom/google/android/youtube/api/service/a/cv;->m:Landroid/view/Surface;

    aput-object v5, v4, v0

    invoke-virtual {v3, v1, v4}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-virtual {v2, v0}, Ljava/util/concurrent/atomic/AtomicInteger;->set(I)V

    iget-object v0, p0, Lcom/google/android/youtube/api/service/a/cv;->o:Lcom/google/android/youtube/api/service/a/ct;

    iget-object v0, v0, Lcom/google/android/youtube/api/service/a/ct;->b:Lcom/google/android/youtube/api/service/a/cn;

    iget-object v1, p0, Lcom/google/android/youtube/api/service/a/cv;->m:Landroid/view/Surface;

    invoke-static {v0, v1}, Lcom/google/android/youtube/api/service/a/cn;->a(Lcom/google/android/youtube/api/service/a/cn;Landroid/view/Surface;)Landroid/view/Surface;

    iget-object v0, p0, Lcom/google/android/youtube/api/service/a/cv;->n:Landroid/os/ConditionVariable;

    invoke-virtual {v0}, Landroid/os/ConditionVariable;->open()V
    :try_end_0
    .catch Ljava/lang/reflect/InvocationTargetException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/IllegalAccessException; {:try_start_0 .. :try_end_0} :catch_2
    .catch Ljava/lang/InstantiationException; {:try_start_0 .. :try_end_0} :catch_3

    return-void

    :catch_0
    move-exception v0

    new-instance v1, Ljava/lang/IllegalStateException;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Error invoking relayout method: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v1

    :catch_1
    move-exception v0

    new-instance v1, Ljava/lang/IllegalStateException;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Error invoking relayout method: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v1

    :catch_2
    move-exception v0

    new-instance v1, Ljava/lang/IllegalStateException;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Error invoking relayout method: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v1

    :catch_3
    move-exception v0

    new-instance v1, Ljava/lang/IllegalStateException;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Error invoking relayout method: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v1
.end method
