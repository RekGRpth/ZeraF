.class public final Lcom/google/android/youtube/api/service/a/cn;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Landroid/view/SurfaceHolder;


# instance fields
.field private final a:Landroid/os/Handler;

.field private final b:Lcom/google/android/youtube/api/service/a/co;

.field private final c:Ljava/util/List;

.field private d:Lcom/google/android/youtube/api/jar/client/ch;

.field private e:Landroid/view/Surface;


# direct methods
.method public constructor <init>(Landroid/os/Handler;Lcom/google/android/youtube/api/jar/client/ch;Z)V
    .locals 2

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const-string v0, "uiHandler cannot be null"

    invoke-static {p1, v0}, Lcom/google/android/youtube/core/utils/s;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/os/Handler;

    iput-object v0, p0, Lcom/google/android/youtube/api/service/a/cn;->a:Landroid/os/Handler;

    const-string v0, "client cannot be null"

    invoke-static {p2, v0}, Lcom/google/android/youtube/core/utils/s;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/youtube/api/jar/client/ch;

    iput-object v0, p0, Lcom/google/android/youtube/api/service/a/cn;->d:Lcom/google/android/youtube/api/jar/client/ch;

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/android/youtube/api/service/a/cn;->c:Ljava/util/List;

    invoke-static {p3}, Lcom/google/android/youtube/api/jar/client/dy;->b(Z)Z

    move-result v0

    if-eqz v0, :cond_0

    :try_start_0
    new-instance v0, Lcom/google/android/youtube/api/service/a/ct;

    invoke-direct {v0, p0}, Lcom/google/android/youtube/api/service/a/ct;-><init>(Lcom/google/android/youtube/api/service/a/cn;)V

    iput-object v0, p0, Lcom/google/android/youtube/api/service/a/cn;->b:Lcom/google/android/youtube/api/service/a/co;
    :try_end_0
    .catch Ljava/lang/ClassNotFoundException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/NoSuchMethodException; {:try_start_0 .. :try_end_0} :catch_1

    :goto_0
    :try_start_1
    iget-object v0, p0, Lcom/google/android/youtube/api/service/a/cn;->b:Lcom/google/android/youtube/api/service/a/co;

    invoke-interface {p2, v0}, Lcom/google/android/youtube/api/jar/client/ch;->a(Lcom/google/android/youtube/api/service/a/az;)V
    :try_end_1
    .catch Landroid/os/RemoteException; {:try_start_1 .. :try_end_1} :catch_2

    :goto_1
    return-void

    :catch_0
    move-exception v0

    new-instance v1, Ljava/lang/IllegalStateException;

    invoke-direct {v1, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/Throwable;)V

    throw v1

    :catch_1
    move-exception v0

    new-instance v1, Ljava/lang/IllegalStateException;

    invoke-direct {v1, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/Throwable;)V

    throw v1

    :cond_0
    new-instance v0, Lcom/google/android/youtube/api/service/a/cr;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/google/android/youtube/api/service/a/cr;-><init>(Lcom/google/android/youtube/api/service/a/cn;B)V

    iput-object v0, p0, Lcom/google/android/youtube/api/service/a/cn;->b:Lcom/google/android/youtube/api/service/a/co;

    goto :goto_0

    :catch_2
    move-exception v0

    goto :goto_1
.end method

.method static synthetic a(Lcom/google/android/youtube/api/service/a/cn;)Landroid/os/Handler;
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/api/service/a/cn;->a:Landroid/os/Handler;

    return-object v0
.end method

.method static synthetic a(Lcom/google/android/youtube/api/service/a/cn;Landroid/view/Surface;)Landroid/view/Surface;
    .locals 0

    iput-object p1, p0, Lcom/google/android/youtube/api/service/a/cn;->e:Landroid/view/Surface;

    return-object p1
.end method

.method static synthetic a(Lcom/google/android/youtube/api/service/a/cn;III)V
    .locals 2

    iget-object v0, p0, Lcom/google/android/youtube/api/service/a/cn;->c:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/SurfaceHolder$Callback;

    invoke-interface {v0, p0, p1, p2, p3}, Landroid/view/SurfaceHolder$Callback;->surfaceChanged(Landroid/view/SurfaceHolder;III)V

    goto :goto_0

    :cond_0
    return-void
.end method

.method static synthetic b(Lcom/google/android/youtube/api/service/a/cn;)V
    .locals 2

    iget-object v0, p0, Lcom/google/android/youtube/api/service/a/cn;->c:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/SurfaceHolder$Callback;

    invoke-interface {v0, p0}, Landroid/view/SurfaceHolder$Callback;->surfaceDestroyed(Landroid/view/SurfaceHolder;)V

    goto :goto_0

    :cond_0
    return-void
.end method

.method static synthetic c(Lcom/google/android/youtube/api/service/a/cn;)V
    .locals 2

    iget-object v0, p0, Lcom/google/android/youtube/api/service/a/cn;->c:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/SurfaceHolder$Callback;

    invoke-interface {v0, p0}, Landroid/view/SurfaceHolder$Callback;->surfaceCreated(Landroid/view/SurfaceHolder;)V

    goto :goto_0

    :cond_0
    return-void
.end method

.method static synthetic d(Lcom/google/android/youtube/api/service/a/cn;)Landroid/view/Surface;
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/api/service/a/cn;->e:Landroid/view/Surface;

    return-object v0
.end method


# virtual methods
.method public final a()V
    .locals 1

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/youtube/api/service/a/cn;->d:Lcom/google/android/youtube/api/jar/client/ch;

    return-void
.end method

.method public final addCallback(Landroid/view/SurfaceHolder$Callback;)V
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/api/service/a/cn;->c:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/youtube/api/service/a/cn;->c:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :cond_0
    return-void
.end method

.method public final getSurface()Landroid/view/Surface;
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/api/service/a/cn;->e:Landroid/view/Surface;

    return-object v0
.end method

.method public final getSurfaceFrame()Landroid/graphics/Rect;
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/api/service/a/cn;->d:Lcom/google/android/youtube/api/jar/client/ch;

    if-eqz v0, :cond_0

    :try_start_0
    iget-object v0, p0, Lcom/google/android/youtube/api/service/a/cn;->d:Lcom/google/android/youtube/api/jar/client/ch;

    invoke-interface {v0}, Lcom/google/android/youtube/api/jar/client/ch;->b()Landroid/graphics/Rect;
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    :goto_0
    return-object v0

    :catch_0
    move-exception v0

    :cond_0
    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    goto :goto_0
.end method

.method public final isCreating()Z
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/api/service/a/cn;->d:Lcom/google/android/youtube/api/jar/client/ch;

    if-eqz v0, :cond_0

    :try_start_0
    iget-object v0, p0, Lcom/google/android/youtube/api/service/a/cn;->d:Lcom/google/android/youtube/api/jar/client/ch;

    invoke-interface {v0}, Lcom/google/android/youtube/api/jar/client/ch;->a()Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v0

    :goto_0
    return v0

    :catch_0
    move-exception v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final lockCanvas()Landroid/graphics/Canvas;
    .locals 2

    new-instance v0, Ljava/lang/UnsupportedOperationException;

    const-string v1, "Unsupported call to lockCanvas"

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public final lockCanvas(Landroid/graphics/Rect;)Landroid/graphics/Canvas;
    .locals 2

    new-instance v0, Ljava/lang/UnsupportedOperationException;

    const-string v1, "Unsupported call to lockCanvas"

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public final removeCallback(Landroid/view/SurfaceHolder$Callback;)V
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/api/service/a/cn;->c:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->remove(Ljava/lang/Object;)Z

    return-void
.end method

.method public final setFixedSize(II)V
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/api/service/a/cn;->d:Lcom/google/android/youtube/api/jar/client/ch;

    if-eqz v0, :cond_0

    :try_start_0
    iget-object v0, p0, Lcom/google/android/youtube/api/service/a/cn;->d:Lcom/google/android/youtube/api/jar/client/ch;

    invoke-interface {v0, p1, p2}, Lcom/google/android/youtube/api/jar/client/ch;->a(II)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    :cond_0
    :goto_0
    return-void

    :catch_0
    move-exception v0

    goto :goto_0
.end method

.method public final setFormat(I)V
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/api/service/a/cn;->d:Lcom/google/android/youtube/api/jar/client/ch;

    if-eqz v0, :cond_0

    :try_start_0
    iget-object v0, p0, Lcom/google/android/youtube/api/service/a/cn;->d:Lcom/google/android/youtube/api/jar/client/ch;

    invoke-interface {v0, p1}, Lcom/google/android/youtube/api/jar/client/ch;->a(I)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    :cond_0
    :goto_0
    return-void

    :catch_0
    move-exception v0

    goto :goto_0
.end method

.method public final setKeepScreenOn(Z)V
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/api/service/a/cn;->d:Lcom/google/android/youtube/api/jar/client/ch;

    if-eqz v0, :cond_0

    :try_start_0
    iget-object v0, p0, Lcom/google/android/youtube/api/service/a/cn;->d:Lcom/google/android/youtube/api/jar/client/ch;

    invoke-interface {v0, p1}, Lcom/google/android/youtube/api/jar/client/ch;->a(Z)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    :cond_0
    :goto_0
    return-void

    :catch_0
    move-exception v0

    goto :goto_0
.end method

.method public final setSizeFromLayout()V
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/api/service/a/cn;->d:Lcom/google/android/youtube/api/jar/client/ch;

    if-eqz v0, :cond_0

    :try_start_0
    iget-object v0, p0, Lcom/google/android/youtube/api/service/a/cn;->d:Lcom/google/android/youtube/api/jar/client/ch;

    invoke-interface {v0}, Lcom/google/android/youtube/api/jar/client/ch;->c()V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    :cond_0
    :goto_0
    return-void

    :catch_0
    move-exception v0

    goto :goto_0
.end method

.method public final setType(I)V
    .locals 1
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    iget-object v0, p0, Lcom/google/android/youtube/api/service/a/cn;->d:Lcom/google/android/youtube/api/jar/client/ch;

    if-eqz v0, :cond_0

    :try_start_0
    iget-object v0, p0, Lcom/google/android/youtube/api/service/a/cn;->d:Lcom/google/android/youtube/api/jar/client/ch;

    invoke-interface {v0, p1}, Lcom/google/android/youtube/api/jar/client/ch;->b(I)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    :cond_0
    :goto_0
    return-void

    :catch_0
    move-exception v0

    goto :goto_0
.end method

.method public final unlockCanvasAndPost(Landroid/graphics/Canvas;)V
    .locals 2

    new-instance v0, Ljava/lang/UnsupportedOperationException;

    const-string v1, "Unsupported call to unlockCanvasAndPost"

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0
.end method
