.class public final Lcom/google/android/youtube/api/ApiPlayer;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field private final a:Landroid/content/Context;

.field private final b:Lcom/google/android/youtube/api/s;

.field private final c:Lcom/google/android/youtube/api/j;

.field private final d:Lcom/google/android/youtube/core/player/Director;

.field private final e:Lcom/google/android/youtube/core/player/as;

.field private final f:Lcom/google/android/youtube/core/player/overlay/ControllerOverlay;

.field private final g:Landroid/media/AudioManager;

.field private final h:Lcom/google/android/youtube/api/r;

.field private final i:Landroid/content/BroadcastReceiver;

.field private final j:Landroid/media/AudioManager$OnAudioFocusChangeListener;

.field private final k:Lcom/google/android/youtube/api/t;

.field private final l:Landroid/os/Handler;

.field private m:Lcom/google/android/youtube/core/async/a/c;

.field private n:Ljava/util/List;

.field private o:Ljava/lang/String;

.field private p:Lcom/google/android/youtube/api/ApiPlayer$State;

.field private q:Z

.field private r:Ljava/lang/String;

.field private s:Ljava/lang/String;

.field private t:Z

.field private u:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/google/android/youtube/api/s;Lcom/google/android/youtube/api/j;Lcom/google/android/youtube/core/player/ai;Lcom/google/android/youtube/core/player/af;Lcom/google/android/youtube/core/player/overlay/a;Lcom/google/android/youtube/core/player/overlay/c;Lcom/google/android/youtube/core/player/overlay/ControllerOverlay;Lcom/google/android/youtube/core/player/overlay/i;Lcom/google/android/youtube/core/player/overlay/aa;Lcom/google/android/youtube/core/player/overlay/ab;)V
    .locals 26

    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    const-string v2, "playerSurface cannot be null"

    move-object/from16 v0, p5

    invoke-static {v0, v2}, Lcom/google/android/youtube/core/utils/s;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const-string v2, "context cannot be null"

    move-object/from16 v0, p1

    invoke-static {v0, v2}, Lcom/google/android/youtube/core/utils/s;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/content/Context;

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/google/android/youtube/api/ApiPlayer;->a:Landroid/content/Context;

    const-string v2, "listener cannot be null"

    move-object/from16 v0, p2

    invoke-static {v0, v2}, Lcom/google/android/youtube/core/utils/s;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/android/youtube/api/s;

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/google/android/youtube/api/ApiPlayer;->b:Lcom/google/android/youtube/api/s;

    const-string v2, "context cannot be null"

    move-object/from16 v0, p3

    invoke-static {v0, v2}, Lcom/google/android/youtube/core/utils/s;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/android/youtube/api/j;

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/google/android/youtube/api/ApiPlayer;->c:Lcom/google/android/youtube/api/j;

    const-string v2, "playerUi cannot be null"

    move-object/from16 v0, p4

    invoke-static {v0, v2}, Lcom/google/android/youtube/core/utils/s;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const-string v2, "adOverlay cannot be null"

    move-object/from16 v0, p6

    invoke-static {v0, v2}, Lcom/google/android/youtube/core/utils/s;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const-string v2, "brandingOverlay cannot be null"

    move-object/from16 v0, p7

    invoke-static {v0, v2}, Lcom/google/android/youtube/core/utils/s;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const-string v2, "controllerOverlay cannot be null"

    move-object/from16 v0, p8

    invoke-static {v0, v2}, Lcom/google/android/youtube/core/utils/s;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/android/youtube/core/player/overlay/ControllerOverlay;

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/google/android/youtube/api/ApiPlayer;->f:Lcom/google/android/youtube/core/player/overlay/ControllerOverlay;

    const-string v2, "liveOverlay cannot be null"

    move-object/from16 v0, p9

    invoke-static {v0, v2}, Lcom/google/android/youtube/core/utils/s;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const-string v2, "subtitlesOverlay cannot be null"

    move-object/from16 v0, p10

    invoke-static {v0, v2}, Lcom/google/android/youtube/core/utils/s;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const-string v2, "thumbnailOverlay cannot be null"

    move-object/from16 v0, p11

    invoke-static {v0, v2}, Lcom/google/android/youtube/core/utils/s;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    new-instance v2, Lcom/google/android/youtube/core/player/as;

    move-object/from16 v0, p1

    move-object/from16 v1, p5

    invoke-direct {v2, v0, v1}, Lcom/google/android/youtube/core/player/as;-><init>(Landroid/content/Context;Lcom/google/android/youtube/core/player/af;)V

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/google/android/youtube/api/ApiPlayer;->e:Lcom/google/android/youtube/core/player/as;

    new-instance v2, Landroid/os/Handler;

    invoke-virtual/range {p1 .. p1}, Landroid/content/Context;->getMainLooper()Landroid/os/Looper;

    move-result-object v3

    invoke-direct {v2, v3}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/google/android/youtube/api/ApiPlayer;->l:Landroid/os/Handler;

    sget-object v2, Lcom/google/android/youtube/api/ApiPlayer$State;->UNINITIALIZED:Lcom/google/android/youtube/api/ApiPlayer$State;

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/google/android/youtube/api/ApiPlayer;->p:Lcom/google/android/youtube/api/ApiPlayer$State;

    new-instance v2, Lcom/google/android/youtube/api/r;

    const/4 v3, 0x0

    move-object/from16 v0, p0

    invoke-direct {v2, v0, v3}, Lcom/google/android/youtube/api/r;-><init>(Lcom/google/android/youtube/api/ApiPlayer;B)V

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/google/android/youtube/api/ApiPlayer;->h:Lcom/google/android/youtube/api/r;

    const-string v2, "audio"

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/media/AudioManager;

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/google/android/youtube/api/ApiPlayer;->g:Landroid/media/AudioManager;

    new-instance v2, Lcom/google/android/youtube/api/p;

    const/4 v3, 0x0

    move-object/from16 v0, p0

    invoke-direct {v2, v0, v3}, Lcom/google/android/youtube/api/p;-><init>(Lcom/google/android/youtube/api/ApiPlayer;B)V

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/google/android/youtube/api/ApiPlayer;->i:Landroid/content/BroadcastReceiver;

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/youtube/api/ApiPlayer;->i:Landroid/content/BroadcastReceiver;

    new-instance v3, Landroid/content/IntentFilter;

    const-string v4, "android.media.AUDIO_BECOMING_NOISY"

    invoke-direct {v3, v4}, Landroid/content/IntentFilter;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v3}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    new-instance v2, Lcom/google/android/youtube/api/q;

    const/4 v3, 0x0

    move-object/from16 v0, p0

    invoke-direct {v2, v0, v3}, Lcom/google/android/youtube/api/q;-><init>(Lcom/google/android/youtube/api/ApiPlayer;B)V

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/google/android/youtube/api/ApiPlayer;->j:Landroid/media/AudioManager$OnAudioFocusChangeListener;

    const/4 v2, 0x1

    move-object/from16 v0, p0

    iput-boolean v2, v0, Lcom/google/android/youtube/api/ApiPlayer;->t:Z

    new-instance v2, Lcom/google/android/youtube/api/t;

    const/4 v3, 0x0

    move-object/from16 v0, p0

    invoke-direct {v2, v0, v3}, Lcom/google/android/youtube/api/t;-><init>(Lcom/google/android/youtube/api/ApiPlayer;B)V

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/google/android/youtube/api/ApiPlayer;->k:Lcom/google/android/youtube/api/t;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/youtube/api/ApiPlayer;->e:Lcom/google/android/youtube/core/player/as;

    invoke-virtual/range {p3 .. p3}, Lcom/google/android/youtube/api/j;->k()Landroid/content/SharedPreferences;

    move-result-object v5

    invoke-virtual/range {p3 .. p3}, Lcom/google/android/youtube/api/j;->a()Lcom/google/android/youtube/core/client/bc;

    move-result-object v6

    invoke-virtual/range {p3 .. p3}, Lcom/google/android/youtube/api/j;->e_()Lcom/google/android/youtube/core/client/be;

    move-result-object v7

    invoke-virtual/range {p3 .. p3}, Lcom/google/android/youtube/api/j;->b()Lcom/google/android/youtube/core/client/d;

    move-result-object v8

    invoke-virtual/range {p3 .. p3}, Lcom/google/android/youtube/api/j;->d()Lcom/google/android/youtube/core/client/bo;

    move-result-object v9

    invoke-virtual/range {p3 .. p3}, Lcom/google/android/youtube/api/j;->g()Lcom/google/android/youtube/core/client/bl;

    move-result-object v10

    invoke-virtual/range {p3 .. p3}, Lcom/google/android/youtube/api/j;->o()Lcom/google/android/youtube/core/player/a;

    move-result-object v11

    invoke-virtual/range {p3 .. p3}, Lcom/google/android/youtube/api/j;->i()Lcom/google/android/youtube/core/Analytics;

    move-result-object v12

    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/google/android/youtube/api/ApiPlayer;->h:Lcom/google/android/youtube/api/r;

    invoke-virtual/range {p3 .. p3}, Lcom/google/android/youtube/api/j;->j()Lcom/google/android/youtube/core/e;

    move-result-object v20

    invoke-virtual/range {p3 .. p3}, Lcom/google/android/youtube/api/j;->l()Lcom/google/android/youtube/core/utils/p;

    move-result-object v21

    invoke-virtual/range {p3 .. p3}, Lcom/google/android/youtube/api/j;->h()Lcom/google/android/youtube/core/player/ak;

    move-result-object v22

    invoke-virtual/range {p3 .. p3}, Lcom/google/android/youtube/api/j;->m()Lcom/google/android/youtube/core/player/c;

    move-result-object v23

    invoke-virtual/range {p3 .. p3}, Lcom/google/android/youtube/api/j;->e()Lcom/google/android/youtube/core/client/b;

    move-result-object v24

    invoke-virtual/range {p3 .. p3}, Lcom/google/android/youtube/api/j;->f()Lcom/google/android/youtube/core/client/bj;

    move-result-object v25

    move-object/from16 v2, p4

    move-object/from16 v4, p1

    move-object/from16 v14, p8

    move-object/from16 v15, p7

    move-object/from16 v16, p11

    move-object/from16 v17, p6

    move-object/from16 v18, p9

    move-object/from16 v19, p10

    invoke-static/range {v2 .. v25}, Lcom/google/android/youtube/core/player/Director;->a(Lcom/google/android/youtube/core/player/ai;Lcom/google/android/youtube/core/player/as;Landroid/content/Context;Landroid/content/SharedPreferences;Lcom/google/android/youtube/core/client/bc;Lcom/google/android/youtube/core/client/be;Lcom/google/android/youtube/core/client/d;Lcom/google/android/youtube/core/client/bo;Lcom/google/android/youtube/core/client/bl;Lcom/google/android/youtube/core/player/a;Lcom/google/android/youtube/core/Analytics;Lcom/google/android/youtube/core/player/v;Lcom/google/android/youtube/core/player/overlay/ControllerOverlay;Lcom/google/android/youtube/core/player/overlay/c;Lcom/google/android/youtube/core/player/overlay/ab;Lcom/google/android/youtube/core/player/overlay/a;Lcom/google/android/youtube/core/player/overlay/i;Lcom/google/android/youtube/core/player/overlay/aa;Lcom/google/android/youtube/core/e;Lcom/google/android/youtube/core/utils/p;Lcom/google/android/youtube/core/player/ak;Lcom/google/android/youtube/core/player/c;Lcom/google/android/youtube/core/client/b;Lcom/google/android/youtube/core/client/bj;)Lcom/google/android/youtube/core/player/Director;

    move-result-object v2

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/google/android/youtube/api/ApiPlayer;->d:Lcom/google/android/youtube/core/player/Director;

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/youtube/api/ApiPlayer;->e:Lcom/google/android/youtube/core/player/as;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/youtube/api/ApiPlayer;->k:Lcom/google/android/youtube/api/t;

    invoke-virtual {v2, v3}, Lcom/google/android/youtube/core/player/as;->a(Landroid/os/Handler;)V

    return-void
.end method

.method static synthetic a(Lcom/google/android/youtube/api/ApiPlayer;)Lcom/google/android/youtube/api/ApiPlayer$State;
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/api/ApiPlayer;->p:Lcom/google/android/youtube/api/ApiPlayer$State;

    return-object v0
.end method

.method static synthetic a(Lcom/google/android/youtube/api/ApiPlayer;Ljava/lang/String;)Ljava/lang/String;
    .locals 0

    iput-object p1, p0, Lcom/google/android/youtube/api/ApiPlayer;->r:Ljava/lang/String;

    return-object p1
.end method

.method private a(Lcom/google/android/youtube/api/ApiPlayer$State;)V
    .locals 2

    iget-object v0, p0, Lcom/google/android/youtube/api/ApiPlayer;->p:Lcom/google/android/youtube/api/ApiPlayer$State;

    sget-object v1, Lcom/google/android/youtube/api/ApiPlayer$State;->DESTROYED:Lcom/google/android/youtube/api/ApiPlayer$State;

    if-eq v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    const-string v1, "Can not leave the DESTROYED state"

    invoke-static {v0, v1}, Lcom/google/android/youtube/core/utils/s;->b(ZLjava/lang/Object;)V

    iput-object p1, p0, Lcom/google/android/youtube/api/ApiPlayer;->p:Lcom/google/android/youtube/api/ApiPlayer$State;

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method static synthetic a(Lcom/google/android/youtube/api/ApiPlayer;Lcom/google/android/youtube/api/ApiPlayer$State;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/google/android/youtube/api/ApiPlayer;->a(Lcom/google/android/youtube/api/ApiPlayer$State;)V

    return-void
.end method

.method static synthetic b(Lcom/google/android/youtube/api/ApiPlayer;Ljava/lang/String;)Ljava/lang/String;
    .locals 0

    iput-object p1, p0, Lcom/google/android/youtube/api/ApiPlayer;->s:Ljava/lang/String;

    return-object p1
.end method

.method static synthetic b(Lcom/google/android/youtube/api/ApiPlayer;)V
    .locals 4

    const/4 v3, 0x1

    iput-boolean v3, p0, Lcom/google/android/youtube/api/ApiPlayer;->q:Z

    iget-boolean v0, p0, Lcom/google/android/youtube/api/ApiPlayer;->t:Z

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/google/android/youtube/api/ApiPlayer;->u:Z

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/youtube/api/ApiPlayer;->g:Landroid/media/AudioManager;

    iget-object v1, p0, Lcom/google/android/youtube/api/ApiPlayer;->j:Landroid/media/AudioManager$OnAudioFocusChangeListener;

    const/4 v2, 0x3

    invoke-virtual {v0, v1, v2, v3}, Landroid/media/AudioManager;->requestAudioFocus(Landroid/media/AudioManager$OnAudioFocusChangeListener;II)I

    iput-boolean v3, p0, Lcom/google/android/youtube/api/ApiPlayer;->u:Z

    :cond_0
    return-void
.end method

.method static synthetic c(Lcom/google/android/youtube/api/ApiPlayer;)Lcom/google/android/youtube/core/player/Director;
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/api/ApiPlayer;->d:Lcom/google/android/youtube/core/player/Director;

    return-object v0
.end method

.method static synthetic d(Lcom/google/android/youtube/api/ApiPlayer;)Lcom/google/android/youtube/api/s;
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/api/ApiPlayer;->b:Lcom/google/android/youtube/api/s;

    return-object v0
.end method

.method static synthetic e(Lcom/google/android/youtube/api/ApiPlayer;)V
    .locals 0

    invoke-direct {p0}, Lcom/google/android/youtube/api/ApiPlayer;->s()V

    return-void
.end method

.method static synthetic f(Lcom/google/android/youtube/api/ApiPlayer;)Lcom/google/android/youtube/api/j;
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/api/ApiPlayer;->c:Lcom/google/android/youtube/api/j;

    return-object v0
.end method

.method static synthetic g(Lcom/google/android/youtube/api/ApiPlayer;)Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/api/ApiPlayer;->r:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic h(Lcom/google/android/youtube/api/ApiPlayer;)Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/api/ApiPlayer;->s:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic i(Lcom/google/android/youtube/api/ApiPlayer;)Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/youtube/api/ApiPlayer;->t:Z

    return v0
.end method

.method static synthetic j(Lcom/google/android/youtube/api/ApiPlayer;)Lcom/google/android/youtube/core/player/as;
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/api/ApiPlayer;->e:Lcom/google/android/youtube/core/player/as;

    return-object v0
.end method

.method private r()Z
    .locals 3

    const/4 v0, 0x0

    iget-object v1, p0, Lcom/google/android/youtube/api/ApiPlayer;->p:Lcom/google/android/youtube/api/ApiPlayer$State;

    sget-object v2, Lcom/google/android/youtube/api/ApiPlayer$State;->DESTROYED:Lcom/google/android/youtube/api/ApiPlayer$State;

    invoke-virtual {v1, v2}, Lcom/google/android/youtube/api/ApiPlayer$State;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    const-string v1, "This YouTubePlayer has been released - ignoring command."

    new-array v0, v0, [Ljava/lang/Object;

    invoke-static {v1, v0}, Lcom/google/android/youtube/player/internal/util/a;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    const/4 v0, 0x1

    :cond_0
    return v0
.end method

.method private s()V
    .locals 2

    const/4 v1, 0x0

    iput-boolean v1, p0, Lcom/google/android/youtube/api/ApiPlayer;->q:Z

    iget-boolean v0, p0, Lcom/google/android/youtube/api/ApiPlayer;->t:Z

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/google/android/youtube/api/ApiPlayer;->u:Z

    if-eqz v0, :cond_0

    iput-boolean v1, p0, Lcom/google/android/youtube/api/ApiPlayer;->u:Z

    iget-object v0, p0, Lcom/google/android/youtube/api/ApiPlayer;->g:Landroid/media/AudioManager;

    iget-object v1, p0, Lcom/google/android/youtube/api/ApiPlayer;->j:Landroid/media/AudioManager$OnAudioFocusChangeListener;

    invoke-virtual {v0, v1}, Landroid/media/AudioManager;->abandonAudioFocus(Landroid/media/AudioManager$OnAudioFocusChangeListener;)I

    :cond_0
    return-void
.end method


# virtual methods
.method public final a()V
    .locals 2

    invoke-direct {p0}, Lcom/google/android/youtube/api/ApiPlayer;->r()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v0, p0, Lcom/google/android/youtube/api/ApiPlayer;->p:Lcom/google/android/youtube/api/ApiPlayer$State;

    sget-object v1, Lcom/google/android/youtube/api/ApiPlayer$State;->LOADED:Lcom/google/android/youtube/api/ApiPlayer$State;

    if-ne v0, v1, :cond_0

    iget-object v0, p0, Lcom/google/android/youtube/api/ApiPlayer;->d:Lcom/google/android/youtube/core/player/Director;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/google/android/youtube/core/player/Director;->b(Z)V

    goto :goto_0
.end method

.method public final a(I)V
    .locals 2

    invoke-direct {p0}, Lcom/google/android/youtube/api/ApiPlayer;->r()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v0, p0, Lcom/google/android/youtube/api/ApiPlayer;->p:Lcom/google/android/youtube/api/ApiPlayer$State;

    sget-object v1, Lcom/google/android/youtube/api/ApiPlayer$State;->LOADED:Lcom/google/android/youtube/api/ApiPlayer$State;

    if-ne v0, v1, :cond_0

    iget-object v0, p0, Lcom/google/android/youtube/api/ApiPlayer;->d:Lcom/google/android/youtube/core/player/Director;

    invoke-virtual {v0, p1}, Lcom/google/android/youtube/core/player/Director;->a(I)V

    goto :goto_0
.end method

.method public final a(Lcom/google/android/youtube/api/ApiPlayer$PlayerState;)V
    .locals 4

    const/4 v3, 0x0

    iget-boolean v0, p1, Lcom/google/android/youtube/api/ApiPlayer$PlayerState;->manageAudioFocus:Z

    iput-boolean v0, p0, Lcom/google/android/youtube/api/ApiPlayer;->t:Z

    iget v0, p1, Lcom/google/android/youtube/api/ApiPlayer$PlayerState;->iteratorType:I

    const/4 v1, -0x1

    if-ne v0, v1, :cond_0

    iput-object v3, p0, Lcom/google/android/youtube/api/ApiPlayer;->n:Ljava/util/List;

    iput-object v3, p0, Lcom/google/android/youtube/api/ApiPlayer;->o:Ljava/lang/String;

    iput-object v3, p0, Lcom/google/android/youtube/api/ApiPlayer;->m:Lcom/google/android/youtube/core/async/a/c;

    :goto_0
    return-void

    :cond_0
    iget v0, p1, Lcom/google/android/youtube/api/ApiPlayer$PlayerState;->iteratorType:I

    const/4 v1, 0x1

    if-ne v0, v1, :cond_1

    iget-object v0, p1, Lcom/google/android/youtube/api/ApiPlayer$PlayerState;->videoIds:Ljava/util/List;

    iput-object v0, p0, Lcom/google/android/youtube/api/ApiPlayer;->n:Ljava/util/List;

    iput-object v3, p0, Lcom/google/android/youtube/api/ApiPlayer;->o:Ljava/lang/String;

    iget-object v0, p0, Lcom/google/android/youtube/api/ApiPlayer;->c:Lcom/google/android/youtube/api/j;

    invoke-virtual {v0}, Lcom/google/android/youtube/api/j;->a()Lcom/google/android/youtube/core/client/bc;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/youtube/api/ApiPlayer;->n:Ljava/util/List;

    iget v2, p1, Lcom/google/android/youtube/api/ApiPlayer$PlayerState;->iteratorPosition:I

    invoke-static {v0, v1, v3, v2}, Lcom/google/android/youtube/core/async/a/b;->a(Lcom/google/android/youtube/core/client/bc;Ljava/util/List;Lcom/google/android/youtube/core/model/UserAuth;I)Lcom/google/android/youtube/core/async/a/c;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/youtube/api/ApiPlayer;->m:Lcom/google/android/youtube/core/async/a/c;

    iget-object v0, p0, Lcom/google/android/youtube/api/ApiPlayer;->d:Lcom/google/android/youtube/core/player/Director;

    iget-object v1, p0, Lcom/google/android/youtube/api/ApiPlayer;->m:Lcom/google/android/youtube/core/async/a/c;

    iget-object v2, p1, Lcom/google/android/youtube/api/ApiPlayer$PlayerState;->directorState:Lcom/google/android/youtube/core/player/Director$DirectorState;

    invoke-virtual {v0, v1, v2}, Lcom/google/android/youtube/core/player/Director;->a(Lcom/google/android/youtube/core/async/a/a;Lcom/google/android/youtube/core/player/Director$DirectorState;)V

    goto :goto_0

    :cond_1
    iget v0, p1, Lcom/google/android/youtube/api/ApiPlayer$PlayerState;->iteratorType:I

    if-nez v0, :cond_2

    iput-object v3, p0, Lcom/google/android/youtube/api/ApiPlayer;->n:Ljava/util/List;

    iget-object v0, p1, Lcom/google/android/youtube/api/ApiPlayer$PlayerState;->playlistId:Ljava/lang/String;

    iput-object v0, p0, Lcom/google/android/youtube/api/ApiPlayer;->o:Ljava/lang/String;

    iget-object v0, p0, Lcom/google/android/youtube/api/ApiPlayer;->c:Lcom/google/android/youtube/api/j;

    invoke-virtual {v0}, Lcom/google/android/youtube/api/j;->a()Lcom/google/android/youtube/core/client/bc;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/youtube/api/ApiPlayer;->o:Ljava/lang/String;

    iget v2, p1, Lcom/google/android/youtube/api/ApiPlayer$PlayerState;->iteratorPosition:I

    invoke-static {v0, v1, v2}, Lcom/google/android/youtube/core/async/a/b;->a(Lcom/google/android/youtube/core/client/bc;Ljava/lang/String;I)Lcom/google/android/youtube/core/async/a/c;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/youtube/api/ApiPlayer;->m:Lcom/google/android/youtube/core/async/a/c;

    iget-object v0, p0, Lcom/google/android/youtube/api/ApiPlayer;->d:Lcom/google/android/youtube/core/player/Director;

    iget-object v1, p0, Lcom/google/android/youtube/api/ApiPlayer;->m:Lcom/google/android/youtube/core/async/a/c;

    iget-object v2, p1, Lcom/google/android/youtube/api/ApiPlayer$PlayerState;->directorState:Lcom/google/android/youtube/core/player/Director$DirectorState;

    invoke-virtual {v0, v1, v2}, Lcom/google/android/youtube/core/player/Director;->a(Lcom/google/android/youtube/core/async/a/a;Lcom/google/android/youtube/core/player/Director$DirectorState;)V

    goto :goto_0

    :cond_2
    new-instance v0, Ljava/lang/RuntimeException;

    const-string v1, "Unknown iteratorType in saved state"

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public final a(Ljava/lang/String;I)V
    .locals 4

    const/4 v3, 0x0

    invoke-direct {p0}, Lcom/google/android/youtube/api/ApiPlayer;->r()Z

    move-result v0

    if-eqz v0, :cond_0

    :goto_0
    return-void

    :cond_0
    invoke-static {p1}, Ljava/util/Collections;->singletonList(Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/youtube/api/ApiPlayer;->n:Ljava/util/List;

    iput-object v3, p0, Lcom/google/android/youtube/api/ApiPlayer;->o:Ljava/lang/String;

    iget-object v0, p0, Lcom/google/android/youtube/api/ApiPlayer;->c:Lcom/google/android/youtube/api/j;

    invoke-virtual {v0}, Lcom/google/android/youtube/api/j;->a()Lcom/google/android/youtube/core/client/bc;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/youtube/api/ApiPlayer;->n:Ljava/util/List;

    const/4 v2, 0x0

    invoke-static {v0, v1, v3, v2}, Lcom/google/android/youtube/core/async/a/b;->a(Lcom/google/android/youtube/core/client/bc;Ljava/util/List;Lcom/google/android/youtube/core/model/UserAuth;I)Lcom/google/android/youtube/core/async/a/c;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/youtube/api/ApiPlayer;->m:Lcom/google/android/youtube/core/async/a/c;

    iget-object v0, p0, Lcom/google/android/youtube/api/ApiPlayer;->d:Lcom/google/android/youtube/core/player/Director;

    iget-object v1, p0, Lcom/google/android/youtube/api/ApiPlayer;->m:Lcom/google/android/youtube/core/async/a/c;

    sget-object v2, Lcom/google/android/youtube/core/client/VideoStats2Client$Feature;->PLAYER_EMBEDDED:Lcom/google/android/youtube/core/client/VideoStats2Client$Feature;

    const/4 v3, 0x1

    invoke-virtual {v0, v1, v2, v3, p2}, Lcom/google/android/youtube/core/player/Director;->a(Lcom/google/android/youtube/core/async/a/a;Lcom/google/android/youtube/core/client/VideoStats2Client$Feature;ZI)V

    goto :goto_0
.end method

.method public final a(Ljava/lang/String;II)V
    .locals 4

    invoke-direct {p0}, Lcom/google/android/youtube/api/ApiPlayer;->r()Z

    move-result v0

    if-eqz v0, :cond_0

    :goto_0
    return-void

    :cond_0
    iput-object p1, p0, Lcom/google/android/youtube/api/ApiPlayer;->o:Ljava/lang/String;

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/youtube/api/ApiPlayer;->n:Ljava/util/List;

    iget-object v0, p0, Lcom/google/android/youtube/api/ApiPlayer;->c:Lcom/google/android/youtube/api/j;

    invoke-virtual {v0}, Lcom/google/android/youtube/api/j;->a()Lcom/google/android/youtube/core/client/bc;

    move-result-object v0

    invoke-static {v0, p1, p2}, Lcom/google/android/youtube/core/async/a/b;->a(Lcom/google/android/youtube/core/client/bc;Ljava/lang/String;I)Lcom/google/android/youtube/core/async/a/c;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/youtube/api/ApiPlayer;->m:Lcom/google/android/youtube/core/async/a/c;

    iget-object v0, p0, Lcom/google/android/youtube/api/ApiPlayer;->d:Lcom/google/android/youtube/core/player/Director;

    iget-object v1, p0, Lcom/google/android/youtube/api/ApiPlayer;->m:Lcom/google/android/youtube/core/async/a/c;

    sget-object v2, Lcom/google/android/youtube/core/client/VideoStats2Client$Feature;->PLAYER_EMBEDDED:Lcom/google/android/youtube/core/client/VideoStats2Client$Feature;

    const/4 v3, 0x1

    invoke-virtual {v0, v1, v2, v3, p3}, Lcom/google/android/youtube/core/player/Director;->a(Lcom/google/android/youtube/core/async/a/a;Lcom/google/android/youtube/core/client/VideoStats2Client$Feature;ZI)V

    goto :goto_0
.end method

.method public final a(Ljava/util/List;II)V
    .locals 4

    const/4 v1, 0x0

    invoke-direct {p0}, Lcom/google/android/youtube/api/ApiPlayer;->r()Z

    move-result v0

    if-eqz v0, :cond_0

    :goto_0
    return-void

    :cond_0
    iput-object p1, p0, Lcom/google/android/youtube/api/ApiPlayer;->n:Ljava/util/List;

    iput-object v1, p0, Lcom/google/android/youtube/api/ApiPlayer;->o:Ljava/lang/String;

    iget-object v0, p0, Lcom/google/android/youtube/api/ApiPlayer;->c:Lcom/google/android/youtube/api/j;

    invoke-virtual {v0}, Lcom/google/android/youtube/api/j;->a()Lcom/google/android/youtube/core/client/bc;

    move-result-object v0

    invoke-static {v0, p1, v1, p2}, Lcom/google/android/youtube/core/async/a/b;->a(Lcom/google/android/youtube/core/client/bc;Ljava/util/List;Lcom/google/android/youtube/core/model/UserAuth;I)Lcom/google/android/youtube/core/async/a/c;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/youtube/api/ApiPlayer;->m:Lcom/google/android/youtube/core/async/a/c;

    iget-object v0, p0, Lcom/google/android/youtube/api/ApiPlayer;->d:Lcom/google/android/youtube/core/player/Director;

    iget-object v1, p0, Lcom/google/android/youtube/api/ApiPlayer;->m:Lcom/google/android/youtube/core/async/a/c;

    sget-object v2, Lcom/google/android/youtube/core/client/VideoStats2Client$Feature;->PLAYER_EMBEDDED:Lcom/google/android/youtube/core/client/VideoStats2Client$Feature;

    const/4 v3, 0x1

    invoke-virtual {v0, v1, v2, v3, p3}, Lcom/google/android/youtube/core/player/Director;->a(Lcom/google/android/youtube/core/async/a/a;Lcom/google/android/youtube/core/client/VideoStats2Client$Feature;ZI)V

    goto :goto_0
.end method

.method public final a(Z)V
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/api/ApiPlayer;->d:Lcom/google/android/youtube/core/player/Director;

    invoke-virtual {v0, p1}, Lcom/google/android/youtube/core/player/Director;->g(Z)V

    return-void
.end method

.method public final a(ILandroid/view/KeyEvent;)Z
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/api/ApiPlayer;->d:Lcom/google/android/youtube/core/player/Director;

    invoke-virtual {v0, p1, p2}, Lcom/google/android/youtube/core/player/Director;->a(ILandroid/view/KeyEvent;)Z

    move-result v0

    return v0
.end method

.method public final b()V
    .locals 2

    invoke-direct {p0}, Lcom/google/android/youtube/api/ApiPlayer;->r()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v0, p0, Lcom/google/android/youtube/api/ApiPlayer;->p:Lcom/google/android/youtube/api/ApiPlayer$State;

    sget-object v1, Lcom/google/android/youtube/api/ApiPlayer$State;->LOADED:Lcom/google/android/youtube/api/ApiPlayer$State;

    if-ne v0, v1, :cond_0

    iget-object v0, p0, Lcom/google/android/youtube/api/ApiPlayer;->d:Lcom/google/android/youtube/core/player/Director;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/google/android/youtube/core/player/Director;->b(Z)V

    goto :goto_0
.end method

.method public final b(I)V
    .locals 2

    invoke-direct {p0}, Lcom/google/android/youtube/api/ApiPlayer;->r()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v0, p0, Lcom/google/android/youtube/api/ApiPlayer;->p:Lcom/google/android/youtube/api/ApiPlayer$State;

    sget-object v1, Lcom/google/android/youtube/api/ApiPlayer$State;->LOADED:Lcom/google/android/youtube/api/ApiPlayer$State;

    if-ne v0, v1, :cond_0

    iget-object v0, p0, Lcom/google/android/youtube/api/ApiPlayer;->d:Lcom/google/android/youtube/core/player/Director;

    invoke-virtual {v0}, Lcom/google/android/youtube/core/player/Director;->r()I

    move-result v1

    add-int/2addr v1, p1

    invoke-virtual {v0, v1}, Lcom/google/android/youtube/core/player/Director;->a(I)V

    goto :goto_0
.end method

.method public final b(Ljava/lang/String;I)V
    .locals 7

    const/4 v5, 0x0

    const/4 v3, 0x0

    invoke-direct {p0}, Lcom/google/android/youtube/api/ApiPlayer;->r()Z

    move-result v0

    if-eqz v0, :cond_0

    :goto_0
    return-void

    :cond_0
    invoke-static {p1}, Ljava/util/Collections;->singletonList(Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/youtube/api/ApiPlayer;->n:Ljava/util/List;

    iput-object v5, p0, Lcom/google/android/youtube/api/ApiPlayer;->o:Ljava/lang/String;

    iget-object v0, p0, Lcom/google/android/youtube/api/ApiPlayer;->c:Lcom/google/android/youtube/api/j;

    invoke-virtual {v0}, Lcom/google/android/youtube/api/j;->a()Lcom/google/android/youtube/core/client/bc;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/youtube/api/ApiPlayer;->n:Ljava/util/List;

    invoke-static {v0, v1, v5, v3}, Lcom/google/android/youtube/core/async/a/b;->a(Lcom/google/android/youtube/core/client/bc;Ljava/util/List;Lcom/google/android/youtube/core/model/UserAuth;I)Lcom/google/android/youtube/core/async/a/c;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/youtube/api/ApiPlayer;->m:Lcom/google/android/youtube/core/async/a/c;

    iget-object v0, p0, Lcom/google/android/youtube/api/ApiPlayer;->d:Lcom/google/android/youtube/core/player/Director;

    iget-object v1, p0, Lcom/google/android/youtube/api/ApiPlayer;->m:Lcom/google/android/youtube/core/async/a/c;

    sget-object v2, Lcom/google/android/youtube/core/client/VideoStats2Client$Feature;->PLAYER_EMBEDDED:Lcom/google/android/youtube/core/client/VideoStats2Client$Feature;

    move v4, p2

    move v6, v3

    invoke-virtual/range {v0 .. v6}, Lcom/google/android/youtube/core/player/Director;->a(Lcom/google/android/youtube/core/async/a/a;Lcom/google/android/youtube/core/client/VideoStats2Client$Feature;ZILjava/util/Map;Z)V

    goto :goto_0
.end method

.method public final b(Ljava/lang/String;II)V
    .locals 7

    const/4 v5, 0x0

    const/4 v3, 0x0

    invoke-direct {p0}, Lcom/google/android/youtube/api/ApiPlayer;->r()Z

    move-result v0

    if-eqz v0, :cond_0

    :goto_0
    return-void

    :cond_0
    iput-object p1, p0, Lcom/google/android/youtube/api/ApiPlayer;->o:Ljava/lang/String;

    iput-object v5, p0, Lcom/google/android/youtube/api/ApiPlayer;->n:Ljava/util/List;

    iget-object v0, p0, Lcom/google/android/youtube/api/ApiPlayer;->c:Lcom/google/android/youtube/api/j;

    invoke-virtual {v0}, Lcom/google/android/youtube/api/j;->a()Lcom/google/android/youtube/core/client/bc;

    move-result-object v0

    invoke-static {v0, p1, p2}, Lcom/google/android/youtube/core/async/a/b;->a(Lcom/google/android/youtube/core/client/bc;Ljava/lang/String;I)Lcom/google/android/youtube/core/async/a/c;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/youtube/api/ApiPlayer;->m:Lcom/google/android/youtube/core/async/a/c;

    iget-object v0, p0, Lcom/google/android/youtube/api/ApiPlayer;->d:Lcom/google/android/youtube/core/player/Director;

    iget-object v1, p0, Lcom/google/android/youtube/api/ApiPlayer;->m:Lcom/google/android/youtube/core/async/a/c;

    sget-object v2, Lcom/google/android/youtube/core/client/VideoStats2Client$Feature;->PLAYER_EMBEDDED:Lcom/google/android/youtube/core/client/VideoStats2Client$Feature;

    move v4, p3

    move v6, v3

    invoke-virtual/range {v0 .. v6}, Lcom/google/android/youtube/core/player/Director;->a(Lcom/google/android/youtube/core/async/a/a;Lcom/google/android/youtube/core/client/VideoStats2Client$Feature;ZILjava/util/Map;Z)V

    goto :goto_0
.end method

.method public final b(Ljava/util/List;II)V
    .locals 7

    const/4 v3, 0x0

    const/4 v5, 0x0

    invoke-direct {p0}, Lcom/google/android/youtube/api/ApiPlayer;->r()Z

    move-result v0

    if-eqz v0, :cond_0

    :goto_0
    return-void

    :cond_0
    iput-object p1, p0, Lcom/google/android/youtube/api/ApiPlayer;->n:Ljava/util/List;

    iput-object v5, p0, Lcom/google/android/youtube/api/ApiPlayer;->o:Ljava/lang/String;

    iget-object v0, p0, Lcom/google/android/youtube/api/ApiPlayer;->c:Lcom/google/android/youtube/api/j;

    invoke-virtual {v0}, Lcom/google/android/youtube/api/j;->a()Lcom/google/android/youtube/core/client/bc;

    move-result-object v0

    invoke-static {v0, p1, v5, p2}, Lcom/google/android/youtube/core/async/a/b;->a(Lcom/google/android/youtube/core/client/bc;Ljava/util/List;Lcom/google/android/youtube/core/model/UserAuth;I)Lcom/google/android/youtube/core/async/a/c;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/youtube/api/ApiPlayer;->m:Lcom/google/android/youtube/core/async/a/c;

    iget-object v0, p0, Lcom/google/android/youtube/api/ApiPlayer;->d:Lcom/google/android/youtube/core/player/Director;

    iget-object v1, p0, Lcom/google/android/youtube/api/ApiPlayer;->m:Lcom/google/android/youtube/core/async/a/c;

    sget-object v2, Lcom/google/android/youtube/core/client/VideoStats2Client$Feature;->PLAYER_EMBEDDED:Lcom/google/android/youtube/core/client/VideoStats2Client$Feature;

    move v4, p3

    move v6, v3

    invoke-virtual/range {v0 .. v6}, Lcom/google/android/youtube/core/player/Director;->a(Lcom/google/android/youtube/core/async/a/a;Lcom/google/android/youtube/core/client/VideoStats2Client$Feature;ZILjava/util/Map;Z)V

    goto :goto_0
.end method

.method public final b(Z)V
    .locals 0

    iput-boolean p1, p0, Lcom/google/android/youtube/api/ApiPlayer;->t:Z

    return-void
.end method

.method public final b(ILandroid/view/KeyEvent;)Z
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/api/ApiPlayer;->d:Lcom/google/android/youtube/core/player/Director;

    invoke-virtual {v0, p1, p2}, Lcom/google/android/youtube/core/player/Director;->b(ILandroid/view/KeyEvent;)Z

    move-result v0

    return v0
.end method

.method public final c()V
    .locals 2

    invoke-direct {p0}, Lcom/google/android/youtube/api/ApiPlayer;->r()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v0, p0, Lcom/google/android/youtube/api/ApiPlayer;->p:Lcom/google/android/youtube/api/ApiPlayer$State;

    sget-object v1, Lcom/google/android/youtube/api/ApiPlayer$State;->LOADED:Lcom/google/android/youtube/api/ApiPlayer$State;

    if-ne v0, v1, :cond_0

    iget-object v0, p0, Lcom/google/android/youtube/api/ApiPlayer;->d:Lcom/google/android/youtube/core/player/Director;

    invoke-virtual {v0}, Lcom/google/android/youtube/core/player/Director;->i()V

    goto :goto_0
.end method

.method public final c(Z)V
    .locals 3

    const/4 v2, 0x0

    iget-object v0, p0, Lcom/google/android/youtube/api/ApiPlayer;->p:Lcom/google/android/youtube/api/ApiPlayer$State;

    sget-object v1, Lcom/google/android/youtube/api/ApiPlayer$State;->DESTROYED:Lcom/google/android/youtube/api/ApiPlayer$State;

    if-eq v0, v1, :cond_0

    invoke-direct {p0}, Lcom/google/android/youtube/api/ApiPlayer;->s()V

    iget-object v0, p0, Lcom/google/android/youtube/api/ApiPlayer;->d:Lcom/google/android/youtube/core/player/Director;

    invoke-virtual {v0, p1}, Lcom/google/android/youtube/core/player/Director;->f(Z)V

    iget-object v0, p0, Lcom/google/android/youtube/api/ApiPlayer;->e:Lcom/google/android/youtube/core/player/as;

    iget-object v1, p0, Lcom/google/android/youtube/api/ApiPlayer;->k:Lcom/google/android/youtube/api/t;

    invoke-virtual {v0, v1}, Lcom/google/android/youtube/core/player/as;->b(Landroid/os/Handler;)Z

    iget-object v0, p0, Lcom/google/android/youtube/api/ApiPlayer;->k:Lcom/google/android/youtube/api/t;

    invoke-virtual {v0, v2}, Lcom/google/android/youtube/api/t;->removeCallbacksAndMessages(Ljava/lang/Object;)V

    iget-object v0, p0, Lcom/google/android/youtube/api/ApiPlayer;->l:Landroid/os/Handler;

    invoke-virtual {v0, v2}, Landroid/os/Handler;->removeCallbacksAndMessages(Ljava/lang/Object;)V

    iget-object v0, p0, Lcom/google/android/youtube/api/ApiPlayer;->a:Landroid/content/Context;

    iget-object v1, p0, Lcom/google/android/youtube/api/ApiPlayer;->i:Landroid/content/BroadcastReceiver;

    invoke-virtual {v0, v1}, Landroid/content/Context;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    sget-object v0, Lcom/google/android/youtube/api/ApiPlayer$State;->DESTROYED:Lcom/google/android/youtube/api/ApiPlayer$State;

    invoke-direct {p0, v0}, Lcom/google/android/youtube/api/ApiPlayer;->a(Lcom/google/android/youtube/api/ApiPlayer$State;)V

    :cond_0
    return-void
.end method

.method public final d()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/youtube/api/ApiPlayer;->q:Z

    return v0
.end method

.method public final e()Z
    .locals 2

    iget-object v0, p0, Lcom/google/android/youtube/api/ApiPlayer;->p:Lcom/google/android/youtube/api/ApiPlayer$State;

    sget-object v1, Lcom/google/android/youtube/api/ApiPlayer$State;->LOADED:Lcom/google/android/youtube/api/ApiPlayer$State;

    if-eq v0, v1, :cond_0

    iget-object v0, p0, Lcom/google/android/youtube/api/ApiPlayer;->p:Lcom/google/android/youtube/api/ApiPlayer$State;

    sget-object v1, Lcom/google/android/youtube/api/ApiPlayer$State;->LOADING:Lcom/google/android/youtube/api/ApiPlayer$State;

    if-ne v0, v1, :cond_1

    :cond_0
    iget-object v0, p0, Lcom/google/android/youtube/api/ApiPlayer;->d:Lcom/google/android/youtube/core/player/Director;

    invoke-virtual {v0}, Lcom/google/android/youtube/core/player/Director;->k()Z

    move-result v0

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final f()Z
    .locals 2

    iget-object v0, p0, Lcom/google/android/youtube/api/ApiPlayer;->p:Lcom/google/android/youtube/api/ApiPlayer$State;

    sget-object v1, Lcom/google/android/youtube/api/ApiPlayer$State;->LOADED:Lcom/google/android/youtube/api/ApiPlayer$State;

    if-eq v0, v1, :cond_0

    iget-object v0, p0, Lcom/google/android/youtube/api/ApiPlayer;->p:Lcom/google/android/youtube/api/ApiPlayer$State;

    sget-object v1, Lcom/google/android/youtube/api/ApiPlayer$State;->LOADING:Lcom/google/android/youtube/api/ApiPlayer$State;

    if-ne v0, v1, :cond_1

    :cond_0
    iget-object v0, p0, Lcom/google/android/youtube/api/ApiPlayer;->d:Lcom/google/android/youtube/core/player/Director;

    invoke-virtual {v0}, Lcom/google/android/youtube/core/player/Director;->l()Z

    move-result v0

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final g()V
    .locals 3

    const/4 v2, 0x0

    invoke-direct {p0}, Lcom/google/android/youtube/api/ApiPlayer;->r()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-virtual {p0}, Lcom/google/android/youtube/api/ApiPlayer;->e()Z

    move-result v0

    if-nez v0, :cond_2

    :cond_0
    iget-object v0, p0, Lcom/google/android/youtube/api/ApiPlayer;->p:Lcom/google/android/youtube/api/ApiPlayer$State;

    sget-object v1, Lcom/google/android/youtube/api/ApiPlayer$State;->DESTROYED:Lcom/google/android/youtube/api/ApiPlayer$State;

    invoke-virtual {v0, v1}, Lcom/google/android/youtube/api/ApiPlayer$State;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    const-string v0, "Ignoring call to next() on YouTubePlayer as already at end of playlist."

    new-array v1, v2, [Ljava/lang/Object;

    invoke-static {v0, v1}, Lcom/google/android/youtube/player/internal/util/a;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    :cond_1
    :goto_0
    return-void

    :cond_2
    sget-object v0, Lcom/google/android/youtube/api/ApiPlayer$State;->LOADING:Lcom/google/android/youtube/api/ApiPlayer$State;

    invoke-direct {p0, v0}, Lcom/google/android/youtube/api/ApiPlayer;->a(Lcom/google/android/youtube/api/ApiPlayer$State;)V

    iget-object v0, p0, Lcom/google/android/youtube/api/ApiPlayer;->d:Lcom/google/android/youtube/core/player/Director;

    invoke-virtual {v0, v2}, Lcom/google/android/youtube/core/player/Director;->c(Z)V

    goto :goto_0
.end method

.method public final h()V
    .locals 3

    const/4 v2, 0x0

    invoke-direct {p0}, Lcom/google/android/youtube/api/ApiPlayer;->r()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-virtual {p0}, Lcom/google/android/youtube/api/ApiPlayer;->f()Z

    move-result v0

    if-nez v0, :cond_2

    :cond_0
    iget-object v0, p0, Lcom/google/android/youtube/api/ApiPlayer;->p:Lcom/google/android/youtube/api/ApiPlayer$State;

    sget-object v1, Lcom/google/android/youtube/api/ApiPlayer$State;->DESTROYED:Lcom/google/android/youtube/api/ApiPlayer$State;

    invoke-virtual {v0, v1}, Lcom/google/android/youtube/api/ApiPlayer$State;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    const-string v0, "Ignoring call to next() on YouTubePlayer as already at end of playlist."

    new-array v1, v2, [Ljava/lang/Object;

    invoke-static {v0, v1}, Lcom/google/android/youtube/player/internal/util/a;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    :cond_1
    :goto_0
    return-void

    :cond_2
    sget-object v0, Lcom/google/android/youtube/api/ApiPlayer$State;->LOADING:Lcom/google/android/youtube/api/ApiPlayer$State;

    invoke-direct {p0, v0}, Lcom/google/android/youtube/api/ApiPlayer;->a(Lcom/google/android/youtube/api/ApiPlayer$State;)V

    iget-object v0, p0, Lcom/google/android/youtube/api/ApiPlayer;->d:Lcom/google/android/youtube/core/player/Director;

    invoke-virtual {v0, v2}, Lcom/google/android/youtube/core/player/Director;->d(Z)V

    goto :goto_0
.end method

.method public final i()I
    .locals 2

    iget-object v0, p0, Lcom/google/android/youtube/api/ApiPlayer;->p:Lcom/google/android/youtube/api/ApiPlayer$State;

    sget-object v1, Lcom/google/android/youtube/api/ApiPlayer$State;->LOADED:Lcom/google/android/youtube/api/ApiPlayer$State;

    if-ne v0, v1, :cond_0

    iget-object v0, p0, Lcom/google/android/youtube/api/ApiPlayer;->d:Lcom/google/android/youtube/core/player/Director;

    invoke-virtual {v0}, Lcom/google/android/youtube/core/player/Director;->r()I

    move-result v0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final j()I
    .locals 2

    iget-object v0, p0, Lcom/google/android/youtube/api/ApiPlayer;->p:Lcom/google/android/youtube/api/ApiPlayer$State;

    sget-object v1, Lcom/google/android/youtube/api/ApiPlayer$State;->LOADED:Lcom/google/android/youtube/api/ApiPlayer$State;

    if-ne v0, v1, :cond_0

    iget-object v0, p0, Lcom/google/android/youtube/api/ApiPlayer;->d:Lcom/google/android/youtube/core/player/Director;

    invoke-virtual {v0}, Lcom/google/android/youtube/core/player/Director;->s()I

    move-result v0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final k()V
    .locals 2

    invoke-direct {p0}, Lcom/google/android/youtube/api/ApiPlayer;->r()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v0, p0, Lcom/google/android/youtube/api/ApiPlayer;->p:Lcom/google/android/youtube/api/ApiPlayer$State;

    sget-object v1, Lcom/google/android/youtube/api/ApiPlayer$State;->LOADED:Lcom/google/android/youtube/api/ApiPlayer$State;

    if-ne v0, v1, :cond_0

    iget-object v0, p0, Lcom/google/android/youtube/api/ApiPlayer;->d:Lcom/google/android/youtube/core/player/Director;

    invoke-virtual {v0}, Lcom/google/android/youtube/core/player/Director;->j()V

    goto :goto_0
.end method

.method public final l()Z
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/api/ApiPlayer;->d:Lcom/google/android/youtube/core/player/Director;

    invoke-virtual {v0}, Lcom/google/android/youtube/core/player/Director;->t()Z

    move-result v0

    return v0
.end method

.method public final m()V
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/api/ApiPlayer;->f:Lcom/google/android/youtube/core/player/overlay/ControllerOverlay;

    invoke-interface {v0}, Lcom/google/android/youtube/core/player/overlay/ControllerOverlay;->g()V

    return-void
.end method

.method public final n()V
    .locals 2

    iget-object v0, p0, Lcom/google/android/youtube/api/ApiPlayer;->p:Lcom/google/android/youtube/api/ApiPlayer$State;

    sget-object v1, Lcom/google/android/youtube/api/ApiPlayer$State;->DESTROYED:Lcom/google/android/youtube/api/ApiPlayer$State;

    if-eq v0, v1, :cond_0

    iget-object v0, p0, Lcom/google/android/youtube/api/ApiPlayer;->d:Lcom/google/android/youtube/core/player/Director;

    invoke-virtual {v0}, Lcom/google/android/youtube/core/player/Director;->n()V

    :cond_0
    return-void
.end method

.method public final o()V
    .locals 2

    iget-object v0, p0, Lcom/google/android/youtube/api/ApiPlayer;->p:Lcom/google/android/youtube/api/ApiPlayer$State;

    sget-object v1, Lcom/google/android/youtube/api/ApiPlayer$State;->DESTROYED:Lcom/google/android/youtube/api/ApiPlayer$State;

    if-eq v0, v1, :cond_0

    iget-object v0, p0, Lcom/google/android/youtube/api/ApiPlayer;->b:Lcom/google/android/youtube/api/s;

    iget-object v1, p0, Lcom/google/android/youtube/api/ApiPlayer;->d:Lcom/google/android/youtube/core/player/Director;

    invoke-virtual {v1}, Lcom/google/android/youtube/core/player/Director;->r()I

    move-result v1

    invoke-interface {v0, v1}, Lcom/google/android/youtube/api/s;->a(I)V

    iget-object v0, p0, Lcom/google/android/youtube/api/ApiPlayer;->d:Lcom/google/android/youtube/core/player/Director;

    invoke-virtual {v0}, Lcom/google/android/youtube/core/player/Director;->o()V

    :cond_0
    return-void
.end method

.method public final p()Lcom/google/android/youtube/core/player/Director;
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/api/ApiPlayer;->d:Lcom/google/android/youtube/core/player/Director;

    return-object v0
.end method

.method public final q()Lcom/google/android/youtube/api/ApiPlayer$PlayerState;
    .locals 7

    const/4 v4, 0x0

    iget-object v0, p0, Lcom/google/android/youtube/api/ApiPlayer;->m:Lcom/google/android/youtube/core/async/a/c;

    if-nez v0, :cond_0

    const/4 v1, -0x1

    :goto_0
    new-instance v0, Lcom/google/android/youtube/api/ApiPlayer$PlayerState;

    iget-object v2, p0, Lcom/google/android/youtube/api/ApiPlayer;->o:Ljava/lang/String;

    iget-object v3, p0, Lcom/google/android/youtube/api/ApiPlayer;->n:Ljava/util/List;

    iget-boolean v5, p0, Lcom/google/android/youtube/api/ApiPlayer;->t:Z

    iget-object v6, p0, Lcom/google/android/youtube/api/ApiPlayer;->d:Lcom/google/android/youtube/core/player/Director;

    invoke-virtual {v6}, Lcom/google/android/youtube/core/player/Director;->u()Lcom/google/android/youtube/core/player/Director$DirectorState;

    move-result-object v6

    invoke-direct/range {v0 .. v6}, Lcom/google/android/youtube/api/ApiPlayer$PlayerState;-><init>(ILjava/lang/String;Ljava/util/List;IZLcom/google/android/youtube/core/player/Director$DirectorState;)V

    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/google/android/youtube/api/ApiPlayer;->m:Lcom/google/android/youtube/core/async/a/c;

    instance-of v0, v0, Lcom/google/android/youtube/core/async/a/d;

    if-eqz v0, :cond_1

    const/4 v1, 0x1

    iget-object v0, p0, Lcom/google/android/youtube/api/ApiPlayer;->m:Lcom/google/android/youtube/core/async/a/c;

    invoke-interface {v0}, Lcom/google/android/youtube/core/async/a/c;->h()I

    move-result v4

    goto :goto_0

    :cond_1
    iget-object v0, p0, Lcom/google/android/youtube/api/ApiPlayer;->m:Lcom/google/android/youtube/core/async/a/c;

    instance-of v0, v0, Lcom/google/android/youtube/core/async/a/g;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/google/android/youtube/api/ApiPlayer;->m:Lcom/google/android/youtube/core/async/a/c;

    invoke-interface {v0}, Lcom/google/android/youtube/core/async/a/c;->h()I

    move-result v0

    move v1, v4

    move v4, v0

    goto :goto_0

    :cond_2
    new-instance v0, Ljava/lang/RuntimeException;

    const-string v1, "Failed to save PlayerState"

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0
.end method
