.class final Lcom/google/android/youtube/api/q;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Landroid/media/AudioManager$OnAudioFocusChangeListener;


# instance fields
.field final synthetic a:Lcom/google/android/youtube/api/ApiPlayer;

.field private b:Z


# direct methods
.method private constructor <init>(Lcom/google/android/youtube/api/ApiPlayer;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/youtube/api/q;->a:Lcom/google/android/youtube/api/ApiPlayer;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/google/android/youtube/api/ApiPlayer;B)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/google/android/youtube/api/q;-><init>(Lcom/google/android/youtube/api/ApiPlayer;)V

    return-void
.end method


# virtual methods
.method public final onAudioFocusChange(I)V
    .locals 3

    const/high16 v2, 0x3f800000

    const v1, 0x3dcccccd

    packed-switch p1, :pswitch_data_0

    :cond_0
    :goto_0
    :pswitch_0
    return-void

    :pswitch_1
    iget-object v0, p0, Lcom/google/android/youtube/api/q;->a:Lcom/google/android/youtube/api/ApiPlayer;

    invoke-static {v0}, Lcom/google/android/youtube/api/ApiPlayer;->j(Lcom/google/android/youtube/api/ApiPlayer;)Lcom/google/android/youtube/core/player/as;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/youtube/core/player/as;->b()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/youtube/api/q;->b:Z

    iget-object v0, p0, Lcom/google/android/youtube/api/q;->a:Lcom/google/android/youtube/api/ApiPlayer;

    invoke-virtual {v0}, Lcom/google/android/youtube/api/ApiPlayer;->c()V

    goto :goto_0

    :pswitch_2
    iget-object v0, p0, Lcom/google/android/youtube/api/q;->a:Lcom/google/android/youtube/api/ApiPlayer;

    invoke-static {v0}, Lcom/google/android/youtube/api/ApiPlayer;->j(Lcom/google/android/youtube/api/ApiPlayer;)Lcom/google/android/youtube/core/player/as;

    move-result-object v0

    invoke-virtual {v0, v1, v1}, Lcom/google/android/youtube/core/player/as;->a(FF)V

    goto :goto_0

    :pswitch_3
    iget-object v0, p0, Lcom/google/android/youtube/api/q;->a:Lcom/google/android/youtube/api/ApiPlayer;

    invoke-static {v0}, Lcom/google/android/youtube/api/ApiPlayer;->j(Lcom/google/android/youtube/api/ApiPlayer;)Lcom/google/android/youtube/core/player/as;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/youtube/core/player/as;->b()Z

    move-result v0

    if-nez v0, :cond_1

    iget-boolean v0, p0, Lcom/google/android/youtube/api/q;->b:Z

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/youtube/api/q;->a:Lcom/google/android/youtube/api/ApiPlayer;

    invoke-virtual {v0}, Lcom/google/android/youtube/api/ApiPlayer;->a()V

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/youtube/api/q;->b:Z

    :cond_1
    iget-object v0, p0, Lcom/google/android/youtube/api/q;->a:Lcom/google/android/youtube/api/ApiPlayer;

    invoke-static {v0}, Lcom/google/android/youtube/api/ApiPlayer;->j(Lcom/google/android/youtube/api/ApiPlayer;)Lcom/google/android/youtube/core/player/as;

    move-result-object v0

    invoke-virtual {v0, v2, v2}, Lcom/google/android/youtube/core/player/as;->a(FF)V

    goto :goto_0

    :pswitch_data_0
    .packed-switch -0x3
        :pswitch_2
        :pswitch_1
        :pswitch_1
        :pswitch_0
        :pswitch_3
        :pswitch_3
        :pswitch_3
    .end packed-switch
.end method
