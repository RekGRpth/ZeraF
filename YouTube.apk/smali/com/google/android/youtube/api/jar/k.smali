.class final Lcom/google/android/youtube/api/jar/k;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/youtube/core/player/overlay/o;


# instance fields
.field final synthetic a:Lcom/google/android/youtube/api/jar/ApiTvControllerOverlay;


# direct methods
.method private constructor <init>(Lcom/google/android/youtube/api/jar/ApiTvControllerOverlay;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/youtube/api/jar/k;->a:Lcom/google/android/youtube/api/jar/ApiTvControllerOverlay;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/google/android/youtube/api/jar/ApiTvControllerOverlay;B)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/google/android/youtube/api/jar/k;-><init>(Lcom/google/android/youtube/api/jar/ApiTvControllerOverlay;)V

    return-void
.end method


# virtual methods
.method public final a()V
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/api/jar/k;->a:Lcom/google/android/youtube/api/jar/ApiTvControllerOverlay;

    invoke-static {v0}, Lcom/google/android/youtube/api/jar/ApiTvControllerOverlay;->b(Lcom/google/android/youtube/api/jar/ApiTvControllerOverlay;)Lcom/google/android/youtube/core/player/overlay/e;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/youtube/api/jar/k;->a:Lcom/google/android/youtube/api/jar/ApiTvControllerOverlay;

    invoke-static {v0}, Lcom/google/android/youtube/api/jar/ApiTvControllerOverlay;->b(Lcom/google/android/youtube/api/jar/ApiTvControllerOverlay;)Lcom/google/android/youtube/core/player/overlay/e;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/youtube/core/player/overlay/e;->e()V

    :cond_0
    return-void
.end method

.method public final a(D)V
    .locals 6

    iget-object v0, p0, Lcom/google/android/youtube/api/jar/k;->a:Lcom/google/android/youtube/api/jar/ApiTvControllerOverlay;

    invoke-static {v0}, Lcom/google/android/youtube/api/jar/ApiTvControllerOverlay;->b(Lcom/google/android/youtube/api/jar/ApiTvControllerOverlay;)Lcom/google/android/youtube/core/player/overlay/e;

    move-result-object v0

    if-eqz v0, :cond_0

    const-wide/high16 v0, 0x3ff0000000000000L

    invoke-static {v0, v1, p1, p2}, Ljava/lang/Math;->max(DD)D

    move-result-wide v0

    iget-object v2, p0, Lcom/google/android/youtube/api/jar/k;->a:Lcom/google/android/youtube/api/jar/ApiTvControllerOverlay;

    invoke-static {v2}, Lcom/google/android/youtube/api/jar/ApiTvControllerOverlay;->b(Lcom/google/android/youtube/api/jar/ApiTvControllerOverlay;)Lcom/google/android/youtube/core/player/overlay/e;

    move-result-object v2

    iget-object v3, p0, Lcom/google/android/youtube/api/jar/k;->a:Lcom/google/android/youtube/api/jar/ApiTvControllerOverlay;

    invoke-static {v3}, Lcom/google/android/youtube/api/jar/ApiTvControllerOverlay;->c(Lcom/google/android/youtube/api/jar/ApiTvControllerOverlay;)I

    move-result v3

    const-wide v4, 0x40d3880000000000L

    mul-double/2addr v0, v4

    double-to-int v0, v0

    sub-int v0, v3, v0

    invoke-interface {v2, v0}, Lcom/google/android/youtube/core/player/overlay/e;->a(I)V

    :cond_0
    return-void
.end method

.method public final a(I)V
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/api/jar/k;->a:Lcom/google/android/youtube/api/jar/ApiTvControllerOverlay;

    invoke-static {v0}, Lcom/google/android/youtube/api/jar/ApiTvControllerOverlay;->b(Lcom/google/android/youtube/api/jar/ApiTvControllerOverlay;)Lcom/google/android/youtube/core/player/overlay/e;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/youtube/api/jar/k;->a:Lcom/google/android/youtube/api/jar/ApiTvControllerOverlay;

    invoke-static {v0}, Lcom/google/android/youtube/api/jar/ApiTvControllerOverlay;->b(Lcom/google/android/youtube/api/jar/ApiTvControllerOverlay;)Lcom/google/android/youtube/core/player/overlay/e;

    move-result-object v0

    invoke-interface {v0, p1}, Lcom/google/android/youtube/core/player/overlay/e;->a(I)V

    :cond_0
    return-void
.end method

.method public final b()V
    .locals 2

    iget-object v0, p0, Lcom/google/android/youtube/api/jar/k;->a:Lcom/google/android/youtube/api/jar/ApiTvControllerOverlay;

    invoke-static {v0}, Lcom/google/android/youtube/api/jar/ApiTvControllerOverlay;->a(Lcom/google/android/youtube/api/jar/ApiTvControllerOverlay;)Lcom/google/android/youtube/api/jar/ApiTvControllerOverlay$State;

    move-result-object v0

    sget-object v1, Lcom/google/android/youtube/api/jar/ApiTvControllerOverlay$State;->PLAYING:Lcom/google/android/youtube/api/jar/ApiTvControllerOverlay$State;

    if-ne v0, v1, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v0, p0, Lcom/google/android/youtube/api/jar/k;->a:Lcom/google/android/youtube/api/jar/ApiTvControllerOverlay;

    invoke-static {v0}, Lcom/google/android/youtube/api/jar/ApiTvControllerOverlay;->b(Lcom/google/android/youtube/api/jar/ApiTvControllerOverlay;)Lcom/google/android/youtube/core/player/overlay/e;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/youtube/api/jar/k;->a:Lcom/google/android/youtube/api/jar/ApiTvControllerOverlay;

    invoke-static {v0}, Lcom/google/android/youtube/api/jar/ApiTvControllerOverlay;->b(Lcom/google/android/youtube/api/jar/ApiTvControllerOverlay;)Lcom/google/android/youtube/core/player/overlay/e;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/youtube/core/player/overlay/e;->a()V

    goto :goto_0
.end method

.method public final b(D)V
    .locals 6

    iget-object v0, p0, Lcom/google/android/youtube/api/jar/k;->a:Lcom/google/android/youtube/api/jar/ApiTvControllerOverlay;

    invoke-static {v0}, Lcom/google/android/youtube/api/jar/ApiTvControllerOverlay;->b(Lcom/google/android/youtube/api/jar/ApiTvControllerOverlay;)Lcom/google/android/youtube/core/player/overlay/e;

    move-result-object v0

    if-eqz v0, :cond_0

    const-wide/high16 v0, 0x3ff0000000000000L

    invoke-static {v0, v1, p1, p2}, Ljava/lang/Math;->max(DD)D

    move-result-wide v0

    iget-object v2, p0, Lcom/google/android/youtube/api/jar/k;->a:Lcom/google/android/youtube/api/jar/ApiTvControllerOverlay;

    invoke-static {v2}, Lcom/google/android/youtube/api/jar/ApiTvControllerOverlay;->b(Lcom/google/android/youtube/api/jar/ApiTvControllerOverlay;)Lcom/google/android/youtube/core/player/overlay/e;

    move-result-object v2

    iget-object v3, p0, Lcom/google/android/youtube/api/jar/k;->a:Lcom/google/android/youtube/api/jar/ApiTvControllerOverlay;

    invoke-static {v3}, Lcom/google/android/youtube/api/jar/ApiTvControllerOverlay;->c(Lcom/google/android/youtube/api/jar/ApiTvControllerOverlay;)I

    move-result v3

    const-wide v4, 0x40d3880000000000L

    mul-double/2addr v0, v4

    double-to-int v0, v0

    add-int/2addr v0, v3

    invoke-interface {v2, v0}, Lcom/google/android/youtube/core/player/overlay/e;->a(I)V

    :cond_0
    return-void
.end method

.method public final c()V
    .locals 2

    iget-object v0, p0, Lcom/google/android/youtube/api/jar/k;->a:Lcom/google/android/youtube/api/jar/ApiTvControllerOverlay;

    invoke-static {v0}, Lcom/google/android/youtube/api/jar/ApiTvControllerOverlay;->a(Lcom/google/android/youtube/api/jar/ApiTvControllerOverlay;)Lcom/google/android/youtube/api/jar/ApiTvControllerOverlay$State;

    move-result-object v0

    sget-object v1, Lcom/google/android/youtube/api/jar/ApiTvControllerOverlay$State;->PAUSED:Lcom/google/android/youtube/api/jar/ApiTvControllerOverlay$State;

    if-ne v0, v1, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v0, p0, Lcom/google/android/youtube/api/jar/k;->a:Lcom/google/android/youtube/api/jar/ApiTvControllerOverlay;

    invoke-static {v0}, Lcom/google/android/youtube/api/jar/ApiTvControllerOverlay;->b(Lcom/google/android/youtube/api/jar/ApiTvControllerOverlay;)Lcom/google/android/youtube/core/player/overlay/e;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/youtube/api/jar/k;->a:Lcom/google/android/youtube/api/jar/ApiTvControllerOverlay;

    invoke-static {v0}, Lcom/google/android/youtube/api/jar/ApiTvControllerOverlay;->b(Lcom/google/android/youtube/api/jar/ApiTvControllerOverlay;)Lcom/google/android/youtube/core/player/overlay/e;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/youtube/core/player/overlay/e;->b()V

    goto :goto_0
.end method

.method public final d()V
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/api/jar/k;->a:Lcom/google/android/youtube/api/jar/ApiTvControllerOverlay;

    invoke-static {v0}, Lcom/google/android/youtube/api/jar/ApiTvControllerOverlay;->b(Lcom/google/android/youtube/api/jar/ApiTvControllerOverlay;)Lcom/google/android/youtube/core/player/overlay/e;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/youtube/api/jar/k;->a:Lcom/google/android/youtube/api/jar/ApiTvControllerOverlay;

    invoke-static {v0}, Lcom/google/android/youtube/api/jar/ApiTvControllerOverlay;->b(Lcom/google/android/youtube/api/jar/ApiTvControllerOverlay;)Lcom/google/android/youtube/core/player/overlay/e;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/youtube/core/player/overlay/e;->d()V

    :cond_0
    return-void
.end method

.method public final e()V
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/api/jar/k;->a:Lcom/google/android/youtube/api/jar/ApiTvControllerOverlay;

    invoke-static {v0}, Lcom/google/android/youtube/api/jar/ApiTvControllerOverlay;->b(Lcom/google/android/youtube/api/jar/ApiTvControllerOverlay;)Lcom/google/android/youtube/core/player/overlay/e;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/youtube/api/jar/k;->a:Lcom/google/android/youtube/api/jar/ApiTvControllerOverlay;

    invoke-static {v0}, Lcom/google/android/youtube/api/jar/ApiTvControllerOverlay;->b(Lcom/google/android/youtube/api/jar/ApiTvControllerOverlay;)Lcom/google/android/youtube/core/player/overlay/e;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/youtube/core/player/overlay/e;->g()V

    :cond_0
    return-void
.end method
