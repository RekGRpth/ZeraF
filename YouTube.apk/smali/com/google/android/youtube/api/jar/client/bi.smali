.class final Lcom/google/android/youtube/api/jar/client/bi;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/youtube/core/player/overlay/e;


# instance fields
.field private a:Lcom/google/android/youtube/api/service/a/aq;


# direct methods
.method public constructor <init>(Lcom/google/android/youtube/api/service/a/aq;)V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const-string v0, "service cannot be null"

    invoke-static {p1, v0}, Lcom/google/android/youtube/core/utils/s;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/youtube/api/service/a/aq;

    iput-object v0, p0, Lcom/google/android/youtube/api/jar/client/bi;->a:Lcom/google/android/youtube/api/service/a/aq;

    return-void
.end method


# virtual methods
.method public final a()V
    .locals 2

    iget-object v0, p0, Lcom/google/android/youtube/api/jar/client/bi;->a:Lcom/google/android/youtube/api/service/a/aq;

    if-eqz v0, :cond_0

    :try_start_0
    iget-object v0, p0, Lcom/google/android/youtube/api/jar/client/bi;->a:Lcom/google/android/youtube/api/service/a/aq;

    invoke-interface {v0}, Lcom/google/android/youtube/api/service/a/aq;->a()V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    :cond_0
    return-void

    :catch_0
    move-exception v0

    new-instance v1, Ljava/lang/IllegalStateException;

    invoke-direct {v1, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/Throwable;)V

    throw v1
.end method

.method public final a(I)V
    .locals 2

    iget-object v0, p0, Lcom/google/android/youtube/api/jar/client/bi;->a:Lcom/google/android/youtube/api/service/a/aq;

    if-eqz v0, :cond_0

    :try_start_0
    iget-object v0, p0, Lcom/google/android/youtube/api/jar/client/bi;->a:Lcom/google/android/youtube/api/service/a/aq;

    invoke-interface {v0, p1}, Lcom/google/android/youtube/api/service/a/aq;->a(I)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    :cond_0
    return-void

    :catch_0
    move-exception v0

    new-instance v1, Ljava/lang/IllegalStateException;

    invoke-direct {v1, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/Throwable;)V

    throw v1
.end method

.method public final a_(Lcom/google/android/youtube/core/model/SubtitleTrack;)V
    .locals 2

    iget-object v0, p0, Lcom/google/android/youtube/api/jar/client/bi;->a:Lcom/google/android/youtube/api/service/a/aq;

    if-eqz v0, :cond_0

    :try_start_0
    iget-object v0, p0, Lcom/google/android/youtube/api/jar/client/bi;->a:Lcom/google/android/youtube/api/service/a/aq;

    invoke-interface {v0, p1}, Lcom/google/android/youtube/api/service/a/aq;->a(Lcom/google/android/youtube/core/model/SubtitleTrack;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    :cond_0
    return-void

    :catch_0
    move-exception v0

    new-instance v1, Ljava/lang/IllegalStateException;

    invoke-direct {v1, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/Throwable;)V

    throw v1
.end method

.method public final b()V
    .locals 2

    iget-object v0, p0, Lcom/google/android/youtube/api/jar/client/bi;->a:Lcom/google/android/youtube/api/service/a/aq;

    if-eqz v0, :cond_0

    :try_start_0
    iget-object v0, p0, Lcom/google/android/youtube/api/jar/client/bi;->a:Lcom/google/android/youtube/api/service/a/aq;

    invoke-interface {v0}, Lcom/google/android/youtube/api/service/a/aq;->b()V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    :cond_0
    return-void

    :catch_0
    move-exception v0

    new-instance v1, Ljava/lang/IllegalStateException;

    invoke-direct {v1, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/Throwable;)V

    throw v1
.end method

.method public final b(Z)V
    .locals 2

    iget-object v0, p0, Lcom/google/android/youtube/api/jar/client/bi;->a:Lcom/google/android/youtube/api/service/a/aq;

    if-eqz v0, :cond_0

    :try_start_0
    iget-object v0, p0, Lcom/google/android/youtube/api/jar/client/bi;->a:Lcom/google/android/youtube/api/service/a/aq;

    invoke-interface {v0, p1}, Lcom/google/android/youtube/api/service/a/aq;->a(Z)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    :cond_0
    return-void

    :catch_0
    move-exception v0

    new-instance v1, Ljava/lang/IllegalStateException;

    invoke-direct {v1, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/Throwable;)V

    throw v1
.end method

.method public final c()V
    .locals 2

    iget-object v0, p0, Lcom/google/android/youtube/api/jar/client/bi;->a:Lcom/google/android/youtube/api/service/a/aq;

    if-eqz v0, :cond_0

    :try_start_0
    iget-object v0, p0, Lcom/google/android/youtube/api/jar/client/bi;->a:Lcom/google/android/youtube/api/service/a/aq;

    invoke-interface {v0}, Lcom/google/android/youtube/api/service/a/aq;->c()V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    :cond_0
    return-void

    :catch_0
    move-exception v0

    new-instance v1, Ljava/lang/IllegalStateException;

    invoke-direct {v1, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/Throwable;)V

    throw v1
.end method

.method public final d()V
    .locals 2

    iget-object v0, p0, Lcom/google/android/youtube/api/jar/client/bi;->a:Lcom/google/android/youtube/api/service/a/aq;

    if-eqz v0, :cond_0

    :try_start_0
    iget-object v0, p0, Lcom/google/android/youtube/api/jar/client/bi;->a:Lcom/google/android/youtube/api/service/a/aq;

    invoke-interface {v0}, Lcom/google/android/youtube/api/service/a/aq;->d()V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    :cond_0
    return-void

    :catch_0
    move-exception v0

    new-instance v1, Ljava/lang/IllegalStateException;

    invoke-direct {v1, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/Throwable;)V

    throw v1
.end method

.method public final e()V
    .locals 2

    iget-object v0, p0, Lcom/google/android/youtube/api/jar/client/bi;->a:Lcom/google/android/youtube/api/service/a/aq;

    if-eqz v0, :cond_0

    :try_start_0
    iget-object v0, p0, Lcom/google/android/youtube/api/jar/client/bi;->a:Lcom/google/android/youtube/api/service/a/aq;

    invoke-interface {v0}, Lcom/google/android/youtube/api/service/a/aq;->e()V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    :cond_0
    return-void

    :catch_0
    move-exception v0

    new-instance v1, Ljava/lang/IllegalStateException;

    invoke-direct {v1, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/Throwable;)V

    throw v1
.end method

.method public final f()V
    .locals 2

    iget-object v0, p0, Lcom/google/android/youtube/api/jar/client/bi;->a:Lcom/google/android/youtube/api/service/a/aq;

    if-eqz v0, :cond_0

    :try_start_0
    iget-object v0, p0, Lcom/google/android/youtube/api/jar/client/bi;->a:Lcom/google/android/youtube/api/service/a/aq;

    invoke-interface {v0}, Lcom/google/android/youtube/api/service/a/aq;->f()V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    :cond_0
    return-void

    :catch_0
    move-exception v0

    new-instance v1, Ljava/lang/IllegalStateException;

    invoke-direct {v1, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/Throwable;)V

    throw v1
.end method

.method public final g()V
    .locals 2

    iget-object v0, p0, Lcom/google/android/youtube/api/jar/client/bi;->a:Lcom/google/android/youtube/api/service/a/aq;

    if-eqz v0, :cond_0

    :try_start_0
    iget-object v0, p0, Lcom/google/android/youtube/api/jar/client/bi;->a:Lcom/google/android/youtube/api/service/a/aq;

    invoke-interface {v0}, Lcom/google/android/youtube/api/service/a/aq;->g()V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    :cond_0
    return-void

    :catch_0
    move-exception v0

    new-instance v1, Ljava/lang/IllegalStateException;

    invoke-direct {v1, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/Throwable;)V

    throw v1
.end method

.method public final h()V
    .locals 2

    iget-object v0, p0, Lcom/google/android/youtube/api/jar/client/bi;->a:Lcom/google/android/youtube/api/service/a/aq;

    if-eqz v0, :cond_0

    :try_start_0
    iget-object v0, p0, Lcom/google/android/youtube/api/jar/client/bi;->a:Lcom/google/android/youtube/api/service/a/aq;

    invoke-interface {v0}, Lcom/google/android/youtube/api/service/a/aq;->h()V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    :cond_0
    return-void

    :catch_0
    move-exception v0

    new-instance v1, Ljava/lang/IllegalStateException;

    invoke-direct {v1, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/Throwable;)V

    throw v1
.end method

.method public final i()V
    .locals 2

    iget-object v0, p0, Lcom/google/android/youtube/api/jar/client/bi;->a:Lcom/google/android/youtube/api/service/a/aq;

    if-eqz v0, :cond_0

    :try_start_0
    iget-object v0, p0, Lcom/google/android/youtube/api/jar/client/bi;->a:Lcom/google/android/youtube/api/service/a/aq;

    invoke-interface {v0}, Lcom/google/android/youtube/api/service/a/aq;->i()V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    :cond_0
    return-void

    :catch_0
    move-exception v0

    new-instance v1, Ljava/lang/IllegalStateException;

    invoke-direct {v1, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/Throwable;)V

    throw v1
.end method

.method public final j()V
    .locals 2

    iget-object v0, p0, Lcom/google/android/youtube/api/jar/client/bi;->a:Lcom/google/android/youtube/api/service/a/aq;

    if-eqz v0, :cond_0

    :try_start_0
    iget-object v0, p0, Lcom/google/android/youtube/api/jar/client/bi;->a:Lcom/google/android/youtube/api/service/a/aq;

    invoke-interface {v0}, Lcom/google/android/youtube/api/service/a/aq;->j()V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    :cond_0
    return-void

    :catch_0
    move-exception v0

    new-instance v1, Ljava/lang/IllegalStateException;

    invoke-direct {v1, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/Throwable;)V

    throw v1
.end method

.method public final k()V
    .locals 2

    iget-object v0, p0, Lcom/google/android/youtube/api/jar/client/bi;->a:Lcom/google/android/youtube/api/service/a/aq;

    if-eqz v0, :cond_0

    :try_start_0
    iget-object v0, p0, Lcom/google/android/youtube/api/jar/client/bi;->a:Lcom/google/android/youtube/api/service/a/aq;

    invoke-interface {v0}, Lcom/google/android/youtube/api/service/a/aq;->k()V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    :cond_0
    return-void

    :catch_0
    move-exception v0

    new-instance v1, Ljava/lang/IllegalStateException;

    invoke-direct {v1, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/Throwable;)V

    throw v1
.end method

.method public final l()V
    .locals 2

    iget-object v0, p0, Lcom/google/android/youtube/api/jar/client/bi;->a:Lcom/google/android/youtube/api/service/a/aq;

    if-eqz v0, :cond_0

    :try_start_0
    iget-object v0, p0, Lcom/google/android/youtube/api/jar/client/bi;->a:Lcom/google/android/youtube/api/service/a/aq;

    invoke-interface {v0}, Lcom/google/android/youtube/api/service/a/aq;->l()V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    :cond_0
    return-void

    :catch_0
    move-exception v0

    new-instance v1, Ljava/lang/IllegalStateException;

    invoke-direct {v1, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/Throwable;)V

    throw v1
.end method

.method public final m()V
    .locals 1

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/youtube/api/jar/client/bi;->a:Lcom/google/android/youtube/api/service/a/aq;

    return-void
.end method
