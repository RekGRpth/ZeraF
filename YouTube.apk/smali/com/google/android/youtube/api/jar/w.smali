.class final Lcom/google/android/youtube/api/jar/w;
.super Lcom/google/android/youtube/api/jar/v;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/youtube/core/v11/ui/c;


# instance fields
.field private final e:Lcom/google/android/youtube/core/v11/ui/b;

.field private f:Z

.field private g:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/google/android/youtube/api/jar/a;Lcom/google/android/youtube/api/jar/y;Lcom/google/android/youtube/core/player/overlay/PlayerOverlaysLayout;)V
    .locals 3

    invoke-direct {p0, p1, p2, p3}, Lcom/google/android/youtube/api/jar/v;-><init>(Landroid/content/Context;Lcom/google/android/youtube/api/jar/a;Lcom/google/android/youtube/api/jar/y;)V

    const-string v0, "playerOverlaysLayout cannot be null"

    invoke-static {p4, v0}, Lcom/google/android/youtube/core/utils/s;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    new-instance v0, Lcom/google/android/youtube/core/v11/ui/d;

    invoke-virtual {p2}, Lcom/google/android/youtube/api/jar/a;->d()Landroid/view/Window;

    move-result-object v1

    invoke-virtual {p2}, Lcom/google/android/youtube/api/jar/a;->a()Landroid/app/ActionBar;

    move-result-object v2

    invoke-direct {v0, v1, v2, p4, p0}, Lcom/google/android/youtube/core/v11/ui/d;-><init>(Landroid/view/Window;Landroid/app/ActionBar;Lcom/google/android/youtube/core/player/overlay/PlayerOverlaysLayout;Lcom/google/android/youtube/core/v11/ui/c;)V

    iput-object v0, p0, Lcom/google/android/youtube/api/jar/w;->e:Lcom/google/android/youtube/core/v11/ui/b;

    return-void
.end method

.method private j()V
    .locals 2

    iget-boolean v0, p0, Lcom/google/android/youtube/api/jar/w;->d:Z

    if-eqz v0, :cond_0

    iget-object v1, p0, Lcom/google/android/youtube/api/jar/w;->e:Lcom/google/android/youtube/core/v11/ui/b;

    iget-boolean v0, p0, Lcom/google/android/youtube/api/jar/w;->f:Z

    if-nez v0, :cond_1

    iget-boolean v0, p0, Lcom/google/android/youtube/api/jar/w;->g:Z

    if-nez v0, :cond_1

    const/4 v0, 0x1

    :goto_0
    invoke-interface {v1, v0}, Lcom/google/android/youtube/core/v11/ui/b;->b(Z)V

    :cond_0
    return-void

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public final a()V
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/api/jar/w;->e:Lcom/google/android/youtube/core/v11/ui/b;

    invoke-interface {v0}, Lcom/google/android/youtube/core/v11/ui/b;->a()V

    invoke-super {p0}, Lcom/google/android/youtube/api/jar/v;->a()V

    return-void
.end method

.method public final d(Z)V
    .locals 0

    iput-boolean p1, p0, Lcom/google/android/youtube/api/jar/w;->g:Z

    invoke-direct {p0}, Lcom/google/android/youtube/api/jar/w;->j()V

    return-void
.end method

.method public final f()V
    .locals 1

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/youtube/api/jar/w;->f:Z

    invoke-direct {p0}, Lcom/google/android/youtube/api/jar/w;->j()V

    return-void
.end method

.method public final g()V
    .locals 1

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/youtube/api/jar/w;->f:Z

    invoke-direct {p0}, Lcom/google/android/youtube/api/jar/w;->j()V

    return-void
.end method

.method final h()V
    .locals 2

    iget-object v0, p0, Lcom/google/android/youtube/api/jar/w;->e:Lcom/google/android/youtube/core/v11/ui/b;

    const/4 v1, 0x1

    invoke-interface {v0, v1}, Lcom/google/android/youtube/core/v11/ui/b;->a(Z)V

    invoke-direct {p0}, Lcom/google/android/youtube/api/jar/w;->j()V

    return-void
.end method

.method final i()V
    .locals 2

    iget-object v0, p0, Lcom/google/android/youtube/api/jar/w;->e:Lcom/google/android/youtube/core/v11/ui/b;

    const/4 v1, 0x0

    invoke-interface {v0, v1}, Lcom/google/android/youtube/core/v11/ui/b;->a(Z)V

    return-void
.end method
