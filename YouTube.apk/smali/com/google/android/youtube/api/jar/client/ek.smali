.class public final Lcom/google/android/youtube/api/jar/client/ek;
.super Lcom/google/android/youtube/api/jar/client/cl;
.source "SourceFile"


# instance fields
.field private final a:Lcom/google/android/youtube/api/jar/client/en;

.field private final b:Landroid/content/Context;

.field private final c:Landroid/os/Handler;

.field private d:Lcom/google/android/youtube/api/jar/client/em;


# direct methods
.method public constructor <init>(Lcom/google/android/youtube/api/jar/client/en;Landroid/content/Context;Landroid/os/Handler;)V
    .locals 1

    invoke-direct {p0}, Lcom/google/android/youtube/api/jar/client/cl;-><init>()V

    const-string v0, "listener cannot be null"

    invoke-static {p1, v0}, Lcom/google/android/youtube/core/utils/s;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/youtube/api/jar/client/en;

    iput-object v0, p0, Lcom/google/android/youtube/api/jar/client/ek;->a:Lcom/google/android/youtube/api/jar/client/en;

    const-string v0, "context cannot be null"

    invoke-static {p2, v0}, Lcom/google/android/youtube/core/utils/s;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/Context;

    iput-object v0, p0, Lcom/google/android/youtube/api/jar/client/ek;->b:Landroid/content/Context;

    const-string v0, "uiHandler cannot be null"

    invoke-static {p3, v0}, Lcom/google/android/youtube/core/utils/s;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/os/Handler;

    iput-object v0, p0, Lcom/google/android/youtube/api/jar/client/ek;->c:Landroid/os/Handler;

    return-void
.end method

.method static synthetic a(Lcom/google/android/youtube/api/jar/client/ek;)Landroid/content/Context;
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/api/jar/client/ek;->b:Landroid/content/Context;

    return-object v0
.end method

.method static synthetic a(Lcom/google/android/youtube/api/jar/client/ek;Lcom/google/android/youtube/api/jar/client/em;)Lcom/google/android/youtube/api/jar/client/em;
    .locals 0

    iput-object p1, p0, Lcom/google/android/youtube/api/jar/client/ek;->d:Lcom/google/android/youtube/api/jar/client/em;

    return-object p1
.end method

.method static synthetic b(Lcom/google/android/youtube/api/jar/client/ek;)Lcom/google/android/youtube/api/jar/client/em;
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/api/jar/client/ek;->d:Lcom/google/android/youtube/api/jar/client/em;

    return-object v0
.end method

.method static synthetic c(Lcom/google/android/youtube/api/jar/client/ek;)Lcom/google/android/youtube/api/jar/client/en;
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/api/jar/client/ek;->a:Lcom/google/android/youtube/api/jar/client/en;

    return-object v0
.end method


# virtual methods
.method public final a()V
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/api/jar/client/ek;->a:Lcom/google/android/youtube/api/jar/client/en;

    invoke-interface {v0}, Lcom/google/android/youtube/api/jar/client/en;->e()V

    iget-object v0, p0, Lcom/google/android/youtube/api/jar/client/ek;->d:Lcom/google/android/youtube/api/jar/client/em;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/youtube/api/jar/client/ek;->d:Lcom/google/android/youtube/api/jar/client/em;

    invoke-virtual {v0}, Lcom/google/android/youtube/api/jar/client/em;->a()V

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/youtube/api/jar/client/ek;->d:Lcom/google/android/youtube/api/jar/client/em;

    :cond_0
    return-void
.end method

.method public final a(Lcom/google/android/youtube/api/service/a/bc;)V
    .locals 2

    iget-object v0, p0, Lcom/google/android/youtube/api/jar/client/ek;->c:Landroid/os/Handler;

    new-instance v1, Lcom/google/android/youtube/api/jar/client/el;

    invoke-direct {v1, p0, p1}, Lcom/google/android/youtube/api/jar/client/el;-><init>(Lcom/google/android/youtube/api/jar/client/ek;Lcom/google/android/youtube/api/service/a/bc;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    return-void
.end method
