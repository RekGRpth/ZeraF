.class public final Lcom/google/android/youtube/api/jar/z;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field private final a:Landroid/view/View;

.field private final b:Lcom/google/android/youtube/api/jar/a;

.field private final c:Lcom/google/android/youtube/api/jar/ab;

.field private final d:Ljava/util/Random;

.field private final e:Z

.field private final f:Z

.field private final g:Z

.field private final h:I

.field private final i:I

.field private final j:I

.field private final k:I

.field private final l:Landroid/os/Handler;

.field private final m:Landroid/graphics/Rect;

.field private final n:Landroid/graphics/Rect;

.field private final o:Landroid/graphics/Rect;

.field private final p:Landroid/graphics/Rect;

.field private q:I

.field private r:I

.field private s:I

.field private t:Ljava/lang/String;

.field private u:Ljava/lang/String;

.field private v:Ljava/lang/String;


# direct methods
.method public constructor <init>(Landroid/view/View;Lcom/google/android/youtube/api/jar/a;Lcom/google/android/youtube/api/jar/ab;)V
    .locals 13

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getMainLooper()Landroid/os/Looper;

    move-result-object v4

    const/4 v5, 0x1

    const/4 v6, 0x1

    const/4 v7, 0x1

    new-instance v8, Ljava/util/Random;

    invoke-direct {v8}, Ljava/util/Random;-><init>()V

    const/16 v9, 0x3e8

    const/16 v10, 0xbb8

    const/16 v11, 0x12c

    const/16 v12, 0x258

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object/from16 v3, p3

    invoke-direct/range {v0 .. v12}, Lcom/google/android/youtube/api/jar/z;-><init>(Landroid/view/View;Lcom/google/android/youtube/api/jar/a;Lcom/google/android/youtube/api/jar/ab;Landroid/os/Looper;ZZZLjava/util/Random;IIII)V

    return-void
.end method

.method private constructor <init>(Landroid/view/View;Lcom/google/android/youtube/api/jar/a;Lcom/google/android/youtube/api/jar/ab;Landroid/os/Looper;ZZZLjava/util/Random;IIII)V
    .locals 3

    const/4 v1, 0x1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const-string v0, "playerView cannot be null"

    invoke-static {p1, v0}, Lcom/google/android/youtube/core/utils/s;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    iput-object v0, p0, Lcom/google/android/youtube/api/jar/z;->a:Landroid/view/View;

    const-string v0, "activityProxy cannot be null"

    invoke-static {p2, v0}, Lcom/google/android/youtube/core/utils/s;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/youtube/api/jar/a;

    iput-object v0, p0, Lcom/google/android/youtube/api/jar/z;->b:Lcom/google/android/youtube/api/jar/a;

    const-string v0, "listener cannot be null"

    invoke-static {p3, v0}, Lcom/google/android/youtube/core/utils/s;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/youtube/api/jar/ab;

    iput-object v0, p0, Lcom/google/android/youtube/api/jar/z;->c:Lcom/google/android/youtube/api/jar/ab;

    const-string v0, "random cannot be null"

    invoke-static {p8, v0}, Lcom/google/android/youtube/core/utils/s;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Random;

    iput-object v0, p0, Lcom/google/android/youtube/api/jar/z;->d:Ljava/util/Random;

    iput-boolean v1, p0, Lcom/google/android/youtube/api/jar/z;->e:Z

    iput-boolean v1, p0, Lcom/google/android/youtube/api/jar/z;->f:Z

    iput-boolean v1, p0, Lcom/google/android/youtube/api/jar/z;->g:Z

    const-string v0, "normalMinimumPeriod must be >= 0"

    invoke-static {v1, v0}, Lcom/google/android/youtube/core/utils/s;->a(ZLjava/lang/Object;)V

    const-string v0, "normalMaximumPeriod must be >= 0"

    invoke-static {v1, v0}, Lcom/google/android/youtube/core/utils/s;->a(ZLjava/lang/Object;)V

    const-string v0, "recheckMinimumPeriod must be >= 0"

    invoke-static {v1, v0}, Lcom/google/android/youtube/core/utils/s;->a(ZLjava/lang/Object;)V

    const-string v0, "recheckMaximumPeriod must be >= 0"

    invoke-static {v1, v0}, Lcom/google/android/youtube/core/utils/s;->a(ZLjava/lang/Object;)V

    const/16 v0, 0x3e8

    iput v0, p0, Lcom/google/android/youtube/api/jar/z;->h:I

    const/16 v0, 0xbb8

    iput v0, p0, Lcom/google/android/youtube/api/jar/z;->i:I

    const/16 v0, 0x12c

    iput v0, p0, Lcom/google/android/youtube/api/jar/z;->j:I

    const/16 v0, 0x258

    iput v0, p0, Lcom/google/android/youtube/api/jar/z;->k:I

    new-instance v0, Landroid/os/Handler;

    new-instance v1, Lcom/google/android/youtube/api/jar/aa;

    const/4 v2, 0x0

    invoke-direct {v1, p0, v2}, Lcom/google/android/youtube/api/jar/aa;-><init>(Lcom/google/android/youtube/api/jar/z;B)V

    invoke-direct {v0, p4, v1}, Landroid/os/Handler;-><init>(Landroid/os/Looper;Landroid/os/Handler$Callback;)V

    iput-object v0, p0, Lcom/google/android/youtube/api/jar/z;->l:Landroid/os/Handler;

    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    iput-object v0, p0, Lcom/google/android/youtube/api/jar/z;->m:Landroid/graphics/Rect;

    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    iput-object v0, p0, Lcom/google/android/youtube/api/jar/z;->n:Landroid/graphics/Rect;

    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    iput-object v0, p0, Lcom/google/android/youtube/api/jar/z;->o:Landroid/graphics/Rect;

    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    iput-object v0, p0, Lcom/google/android/youtube/api/jar/z;->p:Landroid/graphics/Rect;

    return-void
.end method

.method private static a(Landroid/graphics/Rect;Landroid/graphics/Rect;)Ljava/lang/String;
    .locals 5

    const-string v0, "left: %d, top: %d, right: %d, bottom: %d"

    const/4 v1, 0x4

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    iget v3, p1, Landroid/graphics/Rect;->left:I

    iget v4, p0, Landroid/graphics/Rect;->left:I

    sub-int/2addr v3, v4

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v1, v2

    const/4 v2, 0x1

    iget v3, p1, Landroid/graphics/Rect;->top:I

    iget v4, p0, Landroid/graphics/Rect;->top:I

    sub-int/2addr v3, v4

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v1, v2

    const/4 v2, 0x2

    iget v3, p0, Landroid/graphics/Rect;->right:I

    iget v4, p1, Landroid/graphics/Rect;->right:I

    sub-int/2addr v3, v4

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v1, v2

    const/4 v2, 0x3

    iget v3, p0, Landroid/graphics/Rect;->bottom:I

    iget v4, p1, Landroid/graphics/Rect;->bottom:I

    sub-int/2addr v3, v4

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v1, v2

    invoke-static {v0, v1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private a(Landroid/view/View;Landroid/graphics/Rect;)V
    .locals 2

    sget v0, Lcom/google/android/youtube/core/utils/Util;->a:I

    const/16 v1, 0xb

    if-lt v0, v1, :cond_0

    invoke-static {p1, p2}, Lcom/google/android/youtube/api/jar/z;->b(Landroid/view/View;Landroid/graphics/Rect;)V

    invoke-virtual {p1}, Landroid/view/View;->getTranslationX()F

    move-result v0

    float-to-int v0, v0

    invoke-virtual {p1}, Landroid/view/View;->getTranslationY()F

    move-result v1

    float-to-int v1, v1

    invoke-virtual {p2, v0, v1}, Landroid/graphics/Rect;->offset(II)V

    :goto_0
    return-void

    :cond_0
    invoke-static {p1, p2}, Lcom/google/android/youtube/api/jar/z;->b(Landroid/view/View;Landroid/graphics/Rect;)V

    goto :goto_0
.end method

.method private a(Landroid/view/View;Landroid/view/ViewGroup;Landroid/graphics/Rect;)V
    .locals 2

    sget v0, Lcom/google/android/youtube/core/utils/Util;->a:I

    const/16 v1, 0xb

    if-lt v0, v1, :cond_0

    invoke-static {p1, p2, p3}, Lcom/google/android/youtube/api/jar/z;->b(Landroid/view/View;Landroid/view/ViewGroup;Landroid/graphics/Rect;)V

    invoke-virtual {p2}, Landroid/view/ViewGroup;->getTranslationX()F

    move-result v0

    float-to-int v0, v0

    invoke-virtual {p2}, Landroid/view/ViewGroup;->getTranslationY()F

    move-result v1

    float-to-int v1, v1

    invoke-virtual {p3, v0, v1}, Landroid/graphics/Rect;->offset(II)V

    :goto_0
    return-void

    :cond_0
    invoke-static {p1, p2, p3}, Lcom/google/android/youtube/api/jar/z;->b(Landroid/view/View;Landroid/view/ViewGroup;Landroid/graphics/Rect;)V

    goto :goto_0
.end method

.method private a(Landroid/view/ViewGroup;Landroid/graphics/Rect;)V
    .locals 3

    const/4 v2, 0x0

    invoke-virtual {p1}, Landroid/view/ViewGroup;->getWidth()I

    move-result v0

    invoke-virtual {p1}, Landroid/view/ViewGroup;->getHeight()I

    move-result v1

    invoke-virtual {p2, v2, v2, v0, v1}, Landroid/graphics/Rect;->set(IIII)V

    iget-boolean v0, p0, Lcom/google/android/youtube/api/jar/z;->f:Z

    if-eqz v0, :cond_0

    iget v0, p2, Landroid/graphics/Rect;->left:I

    invoke-virtual {p1}, Landroid/view/ViewGroup;->getPaddingLeft()I

    move-result v1

    add-int/2addr v0, v1

    iput v0, p2, Landroid/graphics/Rect;->left:I

    iget v0, p2, Landroid/graphics/Rect;->top:I

    invoke-virtual {p1}, Landroid/view/ViewGroup;->getPaddingTop()I

    move-result v1

    add-int/2addr v0, v1

    iput v0, p2, Landroid/graphics/Rect;->top:I

    iget v0, p2, Landroid/graphics/Rect;->right:I

    invoke-virtual {p1}, Landroid/view/ViewGroup;->getPaddingRight()I

    move-result v1

    sub-int/2addr v0, v1

    iput v0, p2, Landroid/graphics/Rect;->right:I

    iget v0, p2, Landroid/graphics/Rect;->bottom:I

    invoke-virtual {p1}, Landroid/view/ViewGroup;->getPaddingBottom()I

    move-result v1

    sub-int/2addr v0, v1

    iput v0, p2, Landroid/graphics/Rect;->bottom:I

    :cond_0
    return-void
.end method

.method static synthetic a(Lcom/google/android/youtube/api/jar/z;)V
    .locals 9

    const/4 v7, 0x2

    const/high16 v6, 0x3f000000

    const/4 v8, 0x3

    const/4 v2, 0x1

    const/4 v3, 0x0

    invoke-virtual {p0}, Lcom/google/android/youtube/api/jar/z;->c()Z

    move-result v4

    iget-object v0, p0, Lcom/google/android/youtube/api/jar/z;->a:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v0

    iget v0, v0, Landroid/util/DisplayMetrics;->density:F

    iget-object v1, p0, Lcom/google/android/youtube/api/jar/z;->a:Landroid/view/View;

    invoke-virtual {v1}, Landroid/view/View;->getWidth()I

    move-result v1

    int-to-float v1, v1

    div-float/2addr v1, v0

    add-float/2addr v1, v6

    float-to-int v1, v1

    iget-object v5, p0, Lcom/google/android/youtube/api/jar/z;->a:Landroid/view/View;

    invoke-virtual {v5}, Landroid/view/View;->getHeight()I

    move-result v5

    int-to-float v5, v5

    div-float v0, v5, v0

    add-float/2addr v0, v6

    float-to-int v0, v0

    const/16 v5, 0xc3

    if-lt v1, v5, :cond_0

    const/16 v5, 0x69

    if-ge v0, v5, :cond_6

    :cond_0
    const-string v5, "The YouTubePlayerView is %ddp wide (minimum is %ddp) and %ddp high (minimum is %ddp)."

    const/4 v6, 0x4

    new-array v6, v6, [Ljava/lang/Object;

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    aput-object v1, v6, v3

    const/16 v1, 0xc8

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    aput-object v1, v6, v2

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    aput-object v0, v6, v7

    const/16 v0, 0x6e

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    aput-object v0, v6, v8

    invoke-static {v5, v6}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/youtube/api/jar/z;->u:Ljava/lang/String;

    move v1, v3

    :goto_0
    iget-object v0, p0, Lcom/google/android/youtube/api/jar/z;->a:Landroid/view/View;

    :cond_1
    invoke-virtual {v0}, Landroid/view/View;->getVisibility()I

    move-result v5

    if-eqz v5, :cond_7

    const-string v6, "The view %s has visibility \"%s\"."

    new-array v7, v7, [Ljava/lang/Object;

    aput-object v0, v7, v3

    sparse-switch v5, :sswitch_data_0

    const-string v0, "UNKNOWN"

    :goto_1
    aput-object v0, v7, v2

    invoke-static {v6, v7}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/youtube/api/jar/z;->v:Ljava/lang/String;

    move v2, v3

    :goto_2
    if-nez v4, :cond_2

    iget v0, p0, Lcom/google/android/youtube/api/jar/z;->q:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/google/android/youtube/api/jar/z;->q:I

    :cond_2
    if-nez v1, :cond_3

    iget v0, p0, Lcom/google/android/youtube/api/jar/z;->r:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/google/android/youtube/api/jar/z;->r:I

    :cond_3
    if-nez v2, :cond_4

    iget v0, p0, Lcom/google/android/youtube/api/jar/z;->s:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/google/android/youtube/api/jar/z;->s:I

    :cond_4
    iget v0, p0, Lcom/google/android/youtube/api/jar/z;->q:I

    if-lt v0, v8, :cond_9

    iget-object v0, p0, Lcom/google/android/youtube/api/jar/z;->c:Lcom/google/android/youtube/api/jar/ab;

    iget-object v2, p0, Lcom/google/android/youtube/api/jar/z;->t:Ljava/lang/String;

    invoke-interface {v0, v2}, Lcom/google/android/youtube/api/jar/ab;->a(Ljava/lang/String;)V

    iput v3, p0, Lcom/google/android/youtube/api/jar/z;->q:I

    :cond_5
    :goto_3
    if-eqz v4, :cond_b

    if-eqz v1, :cond_b

    iget v0, p0, Lcom/google/android/youtube/api/jar/z;->h:I

    iget-object v1, p0, Lcom/google/android/youtube/api/jar/z;->d:Ljava/util/Random;

    iget v2, p0, Lcom/google/android/youtube/api/jar/z;->i:I

    iget v4, p0, Lcom/google/android/youtube/api/jar/z;->h:I

    sub-int/2addr v2, v4

    invoke-virtual {v1, v2}, Ljava/util/Random;->nextInt(I)I

    move-result v1

    add-int/2addr v0, v1

    :goto_4
    iget-object v1, p0, Lcom/google/android/youtube/api/jar/z;->l:Landroid/os/Handler;

    int-to-long v4, v0

    invoke-virtual {v1, v3, v4, v5}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    return-void

    :cond_6
    move v1, v2

    goto :goto_0

    :sswitch_0
    const-string v0, "VISIBLE"

    goto :goto_1

    :sswitch_1
    const-string v0, "INVISIBLE"

    goto :goto_1

    :sswitch_2
    const-string v0, "GONE"

    goto :goto_1

    :cond_7
    invoke-virtual {v0}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    move-result-object v5

    instance-of v5, v5, Landroid/view/View;

    if-eqz v5, :cond_8

    invoke-virtual {v0}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    :goto_5
    if-nez v0, :cond_1

    goto :goto_2

    :cond_8
    const/4 v0, 0x0

    goto :goto_5

    :cond_9
    iget v0, p0, Lcom/google/android/youtube/api/jar/z;->r:I

    if-lt v0, v8, :cond_a

    iget-object v0, p0, Lcom/google/android/youtube/api/jar/z;->c:Lcom/google/android/youtube/api/jar/ab;

    iget-object v2, p0, Lcom/google/android/youtube/api/jar/z;->u:Ljava/lang/String;

    invoke-interface {v0, v2}, Lcom/google/android/youtube/api/jar/ab;->b(Ljava/lang/String;)V

    iput v3, p0, Lcom/google/android/youtube/api/jar/z;->r:I

    goto :goto_3

    :cond_a
    iget v0, p0, Lcom/google/android/youtube/api/jar/z;->s:I

    if-lt v0, v8, :cond_5

    iget-object v0, p0, Lcom/google/android/youtube/api/jar/z;->c:Lcom/google/android/youtube/api/jar/ab;

    iget-object v2, p0, Lcom/google/android/youtube/api/jar/z;->v:Ljava/lang/String;

    invoke-interface {v0, v2}, Lcom/google/android/youtube/api/jar/ab;->c(Ljava/lang/String;)V

    iput v3, p0, Lcom/google/android/youtube/api/jar/z;->s:I

    goto :goto_3

    :cond_b
    iget v0, p0, Lcom/google/android/youtube/api/jar/z;->j:I

    iget-object v1, p0, Lcom/google/android/youtube/api/jar/z;->d:Ljava/util/Random;

    iget v2, p0, Lcom/google/android/youtube/api/jar/z;->k:I

    iget v4, p0, Lcom/google/android/youtube/api/jar/z;->j:I

    sub-int/2addr v2, v4

    invoke-virtual {v1, v2}, Ljava/util/Random;->nextInt(I)I

    move-result v1

    add-int/2addr v0, v1

    goto :goto_4

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x4 -> :sswitch_1
        0x8 -> :sswitch_2
    .end sparse-switch
.end method

.method private a(Landroid/view/View;)Z
    .locals 11

    const/4 v8, 0x1

    const/4 v7, 0x0

    iget-object v0, p0, Lcom/google/android/youtube/api/jar/z;->a:Landroid/view/View;

    iget-object v1, p0, Lcom/google/android/youtube/api/jar/z;->m:Landroid/graphics/Rect;

    invoke-direct {p0, v0, v1}, Lcom/google/android/youtube/api/jar/z;->a(Landroid/view/View;Landroid/graphics/Rect;)V

    iget-object v2, p0, Lcom/google/android/youtube/api/jar/z;->a:Landroid/view/View;

    move-object v0, v2

    :goto_0
    if-eq v0, p1, :cond_4

    invoke-virtual {v0}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    move-result-object v1

    instance-of v1, v1, Landroid/view/ViewGroup;

    if-eqz v1, :cond_4

    invoke-virtual {v0}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    move-result-object v2

    check-cast v2, Landroid/view/ViewGroup;

    iget-object v1, p0, Lcom/google/android/youtube/api/jar/z;->m:Landroid/graphics/Rect;

    invoke-direct {p0, v0, v2, v1}, Lcom/google/android/youtube/api/jar/z;->a(Landroid/view/View;Landroid/view/ViewGroup;Landroid/graphics/Rect;)V

    iget-boolean v1, p0, Lcom/google/android/youtube/api/jar/z;->e:Z

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/google/android/youtube/api/jar/z;->n:Landroid/graphics/Rect;

    sget v3, Lcom/google/android/youtube/core/utils/Util;->a:I

    const/16 v4, 0xb

    if-lt v3, v4, :cond_0

    invoke-direct {p0, v2, v1}, Lcom/google/android/youtube/api/jar/z;->a(Landroid/view/ViewGroup;Landroid/graphics/Rect;)V

    invoke-virtual {v2}, Landroid/view/ViewGroup;->getTranslationX()F

    move-result v3

    float-to-int v3, v3

    invoke-virtual {v2}, Landroid/view/ViewGroup;->getTranslationY()F

    move-result v4

    float-to-int v4, v4

    invoke-virtual {v1, v3, v4}, Landroid/graphics/Rect;->offset(II)V

    :goto_1
    iget-object v1, p0, Lcom/google/android/youtube/api/jar/z;->n:Landroid/graphics/Rect;

    iget-object v3, p0, Lcom/google/android/youtube/api/jar/z;->m:Landroid/graphics/Rect;

    invoke-virtual {v1, v3}, Landroid/graphics/Rect;->contains(Landroid/graphics/Rect;)Z

    move-result v1

    if-nez v1, :cond_1

    const-string v0, "The YouTubePlayerView is not contained inside its ancestor %s. The distances between the ancestor\'s edges and that of the YouTubePlayerView is: %s (these should all be positive)."

    const/4 v1, 0x2

    new-array v1, v1, [Ljava/lang/Object;

    aput-object v2, v1, v7

    iget-object v2, p0, Lcom/google/android/youtube/api/jar/z;->n:Landroid/graphics/Rect;

    iget-object v3, p0, Lcom/google/android/youtube/api/jar/z;->m:Landroid/graphics/Rect;

    invoke-static {v2, v3}, Lcom/google/android/youtube/api/jar/z;->a(Landroid/graphics/Rect;Landroid/graphics/Rect;)Ljava/lang/String;

    move-result-object v2

    aput-object v2, v1, v8

    invoke-static {v0, v1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/youtube/api/jar/z;->t:Ljava/lang/String;

    move v0, v7

    :goto_2
    return v0

    :cond_0
    invoke-direct {p0, v2, v1}, Lcom/google/android/youtube/api/jar/z;->a(Landroid/view/ViewGroup;Landroid/graphics/Rect;)V

    goto :goto_1

    :cond_1
    invoke-virtual {v2}, Landroid/view/ViewGroup;->getChildCount()I

    move-result v10

    invoke-virtual {v2, v0}, Landroid/view/ViewGroup;->indexOfChild(Landroid/view/View;)I

    move-result v0

    add-int/lit8 v0, v0, 0x1

    move v9, v0

    :goto_3
    if-ge v9, v10, :cond_3

    invoke-virtual {v2, v9}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v1

    invoke-virtual {v1}, Landroid/view/View;->getVisibility()I

    move-result v0

    if-nez v0, :cond_2

    iget-object v0, p0, Lcom/google/android/youtube/api/jar/z;->m:Landroid/graphics/Rect;

    iget v3, v0, Landroid/graphics/Rect;->left:I

    iget-object v0, p0, Lcom/google/android/youtube/api/jar/z;->m:Landroid/graphics/Rect;

    iget v4, v0, Landroid/graphics/Rect;->top:I

    iget-object v0, p0, Lcom/google/android/youtube/api/jar/z;->m:Landroid/graphics/Rect;

    iget v5, v0, Landroid/graphics/Rect;->right:I

    iget-object v0, p0, Lcom/google/android/youtube/api/jar/z;->m:Landroid/graphics/Rect;

    iget v6, v0, Landroid/graphics/Rect;->bottom:I

    move-object v0, p0

    invoke-direct/range {v0 .. v6}, Lcom/google/android/youtube/api/jar/z;->a(Landroid/view/View;Landroid/view/ViewGroup;IIII)Z

    move-result v0

    if-nez v0, :cond_2

    move v0, v7

    goto :goto_2

    :cond_2
    add-int/lit8 v0, v9, 0x1

    move v9, v0

    goto :goto_3

    :cond_3
    move-object v0, v2

    goto/16 :goto_0

    :cond_4
    move v0, v8

    goto :goto_2
.end method

.method private a(Landroid/view/View;Landroid/view/ViewGroup;IIII)Z
    .locals 15

    iget-object v4, p0, Lcom/google/android/youtube/api/jar/z;->o:Landroid/graphics/Rect;

    invoke-virtual {v4}, Landroid/graphics/Rect;->setEmpty()V

    iget-object v4, p0, Lcom/google/android/youtube/api/jar/z;->o:Landroid/graphics/Rect;

    move-object/from16 v0, p1

    move-object/from16 v1, p2

    invoke-direct {p0, v0, v1, v4}, Lcom/google/android/youtube/api/jar/z;->a(Landroid/view/View;Landroid/view/ViewGroup;Landroid/graphics/Rect;)V

    iget-object v4, p0, Lcom/google/android/youtube/api/jar/z;->p:Landroid/graphics/Rect;

    move-object/from16 v0, p1

    invoke-direct {p0, v0, v4}, Lcom/google/android/youtube/api/jar/z;->a(Landroid/view/View;Landroid/graphics/Rect;)V

    iget-object v4, p0, Lcom/google/android/youtube/api/jar/z;->p:Landroid/graphics/Rect;

    iget-object v5, p0, Lcom/google/android/youtube/api/jar/z;->o:Landroid/graphics/Rect;

    iget v5, v5, Landroid/graphics/Rect;->left:I

    iget-object v6, p0, Lcom/google/android/youtube/api/jar/z;->o:Landroid/graphics/Rect;

    iget v6, v6, Landroid/graphics/Rect;->top:I

    invoke-virtual {v4, v5, v6}, Landroid/graphics/Rect;->offset(II)V

    iget-object v4, p0, Lcom/google/android/youtube/api/jar/z;->p:Landroid/graphics/Rect;

    move/from16 v0, p3

    move/from16 v1, p4

    move/from16 v2, p5

    move/from16 v3, p6

    invoke-virtual {v4, v0, v1, v2, v3}, Landroid/graphics/Rect;->intersects(IIII)Z

    move-result v4

    if-eqz v4, :cond_6

    iget-object v4, p0, Lcom/google/android/youtube/api/jar/z;->o:Landroid/graphics/Rect;

    move/from16 v0, p3

    move/from16 v1, p4

    move/from16 v2, p5

    move/from16 v3, p6

    invoke-virtual {v4, v0, v1, v2, v3}, Landroid/graphics/Rect;->set(IIII)V

    const-string v5, "The YouTubePlayerView is obscured by %s. %s."

    const/4 v4, 0x2

    new-array v6, v4, [Ljava/lang/Object;

    const/4 v4, 0x0

    aput-object p1, v6, v4

    const/4 v7, 0x1

    iget-object v8, p0, Lcom/google/android/youtube/api/jar/z;->o:Landroid/graphics/Rect;

    iget-object v9, p0, Lcom/google/android/youtube/api/jar/z;->p:Landroid/graphics/Rect;

    invoke-virtual {v9, v8}, Landroid/graphics/Rect;->contains(Landroid/graphics/Rect;)Z

    move-result v4

    if-eqz v4, :cond_1

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v10, "YouTubePlayerView is completely covered, with the distance in px between each edge of the obscuring view and the YouTubePlayerView being: "

    invoke-direct {v4, v10}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-static {v9, v8}, Lcom/google/android/youtube/api/jar/z;->a(Landroid/graphics/Rect;Landroid/graphics/Rect;)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v4, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v8, "."

    invoke-virtual {v4, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    :cond_0
    :goto_0
    aput-object v4, v6, v7

    invoke-static {v5, v6}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    iput-object v4, p0, Lcom/google/android/youtube/api/jar/z;->t:Ljava/lang/String;

    const/4 v4, 0x0

    :goto_1
    return v4

    :cond_1
    invoke-virtual {v8, v9}, Landroid/graphics/Rect;->contains(Landroid/graphics/Rect;)Z

    move-result v4

    if-eqz v4, :cond_2

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v10, "The view is inside the YouTubePlayerView, with the distance in px between each edge of the obscuring view and the YouTubePlayerView being: "

    invoke-direct {v4, v10}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-static {v8, v9}, Lcom/google/android/youtube/api/jar/z;->a(Landroid/graphics/Rect;Landroid/graphics/Rect;)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v4, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v8, "."

    invoke-virtual {v4, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    goto :goto_0

    :cond_2
    const-string v4, ""

    iget v10, v8, Landroid/graphics/Rect;->left:I

    iget v11, v9, Landroid/graphics/Rect;->left:I

    if-ge v10, v11, :cond_4

    iget v10, v9, Landroid/graphics/Rect;->left:I

    iget v11, v8, Landroid/graphics/Rect;->right:I

    if-ge v10, v11, :cond_4

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v10, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v10, "Left edge %d px left of YouTubePlayerView\'s right edge. "

    const/4 v11, 0x1

    new-array v11, v11, [Ljava/lang/Object;

    const/4 v12, 0x0

    iget v13, v8, Landroid/graphics/Rect;->right:I

    iget v14, v9, Landroid/graphics/Rect;->left:I

    sub-int/2addr v13, v14

    invoke-static {v13}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v13

    aput-object v13, v11, v12

    invoke-static {v10, v11}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v4, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    :cond_3
    :goto_2
    iget v10, v8, Landroid/graphics/Rect;->top:I

    iget v11, v9, Landroid/graphics/Rect;->top:I

    if-ge v10, v11, :cond_5

    iget v10, v9, Landroid/graphics/Rect;->top:I

    iget v11, v8, Landroid/graphics/Rect;->bottom:I

    if-ge v10, v11, :cond_5

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v10, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v10, "Top edge %d px above YouTubePlayerView\'s bottom edge. "

    const/4 v11, 0x1

    new-array v11, v11, [Ljava/lang/Object;

    const/4 v12, 0x0

    iget v8, v8, Landroid/graphics/Rect;->bottom:I

    iget v9, v9, Landroid/graphics/Rect;->top:I

    sub-int/2addr v8, v9

    invoke-static {v8}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    aput-object v8, v11, v12

    invoke-static {v10, v11}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v4, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    goto/16 :goto_0

    :cond_4
    iget v10, v8, Landroid/graphics/Rect;->left:I

    iget v11, v9, Landroid/graphics/Rect;->right:I

    if-ge v10, v11, :cond_3

    iget v10, v9, Landroid/graphics/Rect;->right:I

    iget v11, v8, Landroid/graphics/Rect;->right:I

    if-ge v10, v11, :cond_3

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v10, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v10, "Right edge %d px right of YouTubePlayerView\'s left edge. "

    const/4 v11, 0x1

    new-array v11, v11, [Ljava/lang/Object;

    const/4 v12, 0x0

    iget v13, v9, Landroid/graphics/Rect;->right:I

    iget v14, v8, Landroid/graphics/Rect;->left:I

    sub-int/2addr v13, v14

    invoke-static {v13}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v13

    aput-object v13, v11, v12

    invoke-static {v10, v11}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v4, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    goto :goto_2

    :cond_5
    iget v10, v8, Landroid/graphics/Rect;->top:I

    iget v11, v9, Landroid/graphics/Rect;->bottom:I

    if-ge v10, v11, :cond_0

    iget v10, v9, Landroid/graphics/Rect;->bottom:I

    iget v11, v8, Landroid/graphics/Rect;->bottom:I

    if-ge v10, v11, :cond_0

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v10, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v10, "Bottom edge %d px below YouTubePlayerView\'s top edge. "

    const/4 v11, 0x1

    new-array v11, v11, [Ljava/lang/Object;

    const/4 v12, 0x0

    iget v9, v9, Landroid/graphics/Rect;->bottom:I

    iget v8, v8, Landroid/graphics/Rect;->top:I

    sub-int v8, v9, v8

    invoke-static {v8}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    aput-object v8, v11, v12

    invoke-static {v10, v11}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v4, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    goto/16 :goto_0

    :cond_6
    iget-boolean v4, p0, Lcom/google/android/youtube/api/jar/z;->g:Z

    if-nez v4, :cond_8

    move-object/from16 v0, p1

    instance-of v4, v0, Landroid/view/ViewGroup;

    if-eqz v4, :cond_8

    move-object/from16 v6, p1

    check-cast v6, Landroid/view/ViewGroup;

    iget-object v4, p0, Lcom/google/android/youtube/api/jar/z;->o:Landroid/graphics/Rect;

    iget v4, v4, Landroid/graphics/Rect;->left:I

    sub-int v7, p3, v4

    iget-object v4, p0, Lcom/google/android/youtube/api/jar/z;->o:Landroid/graphics/Rect;

    iget v4, v4, Landroid/graphics/Rect;->top:I

    sub-int v8, p4, v4

    iget-object v4, p0, Lcom/google/android/youtube/api/jar/z;->o:Landroid/graphics/Rect;

    iget v4, v4, Landroid/graphics/Rect;->left:I

    sub-int v9, p5, v4

    iget-object v4, p0, Lcom/google/android/youtube/api/jar/z;->o:Landroid/graphics/Rect;

    iget v4, v4, Landroid/graphics/Rect;->top:I

    sub-int v10, p6, v4

    const/4 v4, 0x0

    invoke-virtual {v6}, Landroid/view/ViewGroup;->getChildCount()I

    move-result v12

    move v11, v4

    :goto_3
    if-ge v11, v12, :cond_8

    invoke-virtual {v6, v11}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v5

    move-object v4, p0

    invoke-direct/range {v4 .. v10}, Lcom/google/android/youtube/api/jar/z;->a(Landroid/view/View;Landroid/view/ViewGroup;IIII)Z

    move-result v4

    if-nez v4, :cond_7

    const/4 v4, 0x0

    goto/16 :goto_1

    :cond_7
    add-int/lit8 v4, v11, 0x1

    move v11, v4

    goto :goto_3

    :cond_8
    const/4 v4, 0x1

    goto/16 :goto_1
.end method

.method private static b(Landroid/view/View;Landroid/graphics/Rect;)V
    .locals 1

    const/4 v0, 0x0

    iput v0, p1, Landroid/graphics/Rect;->left:I

    iput v0, p1, Landroid/graphics/Rect;->top:I

    invoke-virtual {p0}, Landroid/view/View;->getWidth()I

    move-result v0

    iput v0, p1, Landroid/graphics/Rect;->right:I

    invoke-virtual {p0}, Landroid/view/View;->getHeight()I

    move-result v0

    iput v0, p1, Landroid/graphics/Rect;->bottom:I

    return-void
.end method

.method private static b(Landroid/view/View;Landroid/view/ViewGroup;Landroid/graphics/Rect;)V
    .locals 2

    invoke-virtual {p0}, Landroid/view/View;->getLeft()I

    move-result v0

    invoke-virtual {p0}, Landroid/view/View;->getTop()I

    move-result v1

    invoke-virtual {p2, v0, v1}, Landroid/graphics/Rect;->offset(II)V

    invoke-virtual {p1}, Landroid/view/ViewGroup;->getScrollX()I

    move-result v0

    neg-int v0, v0

    invoke-virtual {p1}, Landroid/view/ViewGroup;->getScrollY()I

    move-result v1

    neg-int v1, v1

    invoke-virtual {p2, v0, v1}, Landroid/graphics/Rect;->offset(II)V

    return-void
.end method


# virtual methods
.method public final a()V
    .locals 2

    const/4 v1, 0x0

    iget-object v0, p0, Lcom/google/android/youtube/api/jar/z;->l:Landroid/os/Handler;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeMessages(I)V

    iget-object v0, p0, Lcom/google/android/youtube/api/jar/z;->l:Landroid/os/Handler;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    return-void
.end method

.method public final b()V
    .locals 2

    iget-object v0, p0, Lcom/google/android/youtube/api/jar/z;->l:Landroid/os/Handler;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeMessages(I)V

    return-void
.end method

.method public final c()Z
    .locals 3

    const/4 v2, 0x0

    sget v0, Lcom/google/android/youtube/core/utils/Util;->a:I

    const/16 v1, 0xb

    if-lt v0, v1, :cond_2

    iget-object v0, p0, Lcom/google/android/youtube/api/jar/z;->b:Lcom/google/android/youtube/api/jar/a;

    invoke-virtual {v0}, Lcom/google/android/youtube/api/jar/a;->d()Landroid/view/Window;

    move-result-object v0

    const/16 v1, 0x9

    invoke-virtual {v0, v1}, Landroid/view/Window;->hasFeature(I)Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-virtual {v0}, Landroid/view/Window;->getDecorView()Landroid/view/View;

    move-result-object v0

    const v1, 0x1020002

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-direct {p0, v0}, Lcom/google/android/youtube/api/jar/z;->a(Landroid/view/View;)Z

    move-result v0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x1

    goto :goto_0

    :cond_1
    invoke-direct {p0, v2}, Lcom/google/android/youtube/api/jar/z;->a(Landroid/view/View;)Z

    move-result v0

    goto :goto_0

    :cond_2
    invoke-direct {p0, v2}, Lcom/google/android/youtube/api/jar/z;->a(Landroid/view/View;)Z

    move-result v0

    goto :goto_0
.end method
