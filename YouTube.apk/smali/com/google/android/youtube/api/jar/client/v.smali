.class public final Lcom/google/android/youtube/api/jar/client/v;
.super Lcom/google/android/youtube/api/jar/client/bk;
.source "SourceFile"


# instance fields
.field private final a:Lcom/google/android/youtube/core/player/overlay/a;

.field private final b:Landroid/os/Handler;

.field private c:Lcom/google/android/youtube/api/jar/client/ab;


# direct methods
.method public constructor <init>(Lcom/google/android/youtube/core/player/overlay/a;Landroid/os/Handler;)V
    .locals 1

    invoke-direct {p0}, Lcom/google/android/youtube/api/jar/client/bk;-><init>()V

    const-string v0, "target cannot be null"

    invoke-static {p1, v0}, Lcom/google/android/youtube/core/utils/s;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/youtube/core/player/overlay/a;

    iput-object v0, p0, Lcom/google/android/youtube/api/jar/client/v;->a:Lcom/google/android/youtube/core/player/overlay/a;

    const-string v0, "uiHandler cannot be null"

    invoke-static {p2, v0}, Lcom/google/android/youtube/core/utils/s;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/os/Handler;

    iput-object v0, p0, Lcom/google/android/youtube/api/jar/client/v;->b:Landroid/os/Handler;

    return-void
.end method

.method static synthetic a(Lcom/google/android/youtube/api/jar/client/v;)Lcom/google/android/youtube/api/jar/client/ab;
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/api/jar/client/v;->c:Lcom/google/android/youtube/api/jar/client/ab;

    return-object v0
.end method

.method static synthetic a(Lcom/google/android/youtube/api/jar/client/v;Lcom/google/android/youtube/api/jar/client/ab;)Lcom/google/android/youtube/api/jar/client/ab;
    .locals 0

    iput-object p1, p0, Lcom/google/android/youtube/api/jar/client/v;->c:Lcom/google/android/youtube/api/jar/client/ab;

    return-object p1
.end method

.method static synthetic b(Lcom/google/android/youtube/api/jar/client/v;)Lcom/google/android/youtube/core/player/overlay/a;
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/api/jar/client/v;->a:Lcom/google/android/youtube/core/player/overlay/a;

    return-object v0
.end method


# virtual methods
.method public final a()V
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/api/jar/client/v;->c:Lcom/google/android/youtube/api/jar/client/ab;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/youtube/api/jar/client/v;->c:Lcom/google/android/youtube/api/jar/client/ab;

    invoke-virtual {v0}, Lcom/google/android/youtube/api/jar/client/ab;->a()V

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/youtube/api/jar/client/v;->c:Lcom/google/android/youtube/api/jar/client/ab;

    :cond_0
    return-void
.end method

.method public final a(II)V
    .locals 2

    iget-object v0, p0, Lcom/google/android/youtube/api/jar/client/v;->b:Landroid/os/Handler;

    new-instance v1, Lcom/google/android/youtube/api/jar/client/aa;

    invoke-direct {v1, p0, p1, p2}, Lcom/google/android/youtube/api/jar/client/aa;-><init>(Lcom/google/android/youtube/api/jar/client/v;II)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    return-void
.end method

.method public final a(Lcom/google/android/youtube/api/service/a/ae;)V
    .locals 2

    iget-object v0, p0, Lcom/google/android/youtube/api/jar/client/v;->b:Landroid/os/Handler;

    new-instance v1, Lcom/google/android/youtube/api/jar/client/w;

    invoke-direct {v1, p0, p1}, Lcom/google/android/youtube/api/jar/client/w;-><init>(Lcom/google/android/youtube/api/jar/client/v;Lcom/google/android/youtube/api/service/a/ae;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    return-void
.end method

.method public final a(Ljava/lang/String;ZZLjava/lang/String;)V
    .locals 7

    iget-object v6, p0, Lcom/google/android/youtube/api/jar/client/v;->b:Landroid/os/Handler;

    new-instance v0, Lcom/google/android/youtube/api/jar/client/x;

    move-object v1, p0

    move-object v2, p1

    move v3, p2

    move v4, p3

    move-object v5, p4

    invoke-direct/range {v0 .. v5}, Lcom/google/android/youtube/api/jar/client/x;-><init>(Lcom/google/android/youtube/api/jar/client/v;Ljava/lang/String;ZZLjava/lang/String;)V

    invoke-virtual {v6, v0}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    return-void
.end method

.method public final a(Z)V
    .locals 2

    iget-object v0, p0, Lcom/google/android/youtube/api/jar/client/v;->b:Landroid/os/Handler;

    new-instance v1, Lcom/google/android/youtube/api/jar/client/z;

    invoke-direct {v1, p0, p1}, Lcom/google/android/youtube/api/jar/client/z;-><init>(Lcom/google/android/youtube/api/jar/client/v;Z)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    return-void
.end method

.method public final b()V
    .locals 2

    iget-object v0, p0, Lcom/google/android/youtube/api/jar/client/v;->b:Landroid/os/Handler;

    new-instance v1, Lcom/google/android/youtube/api/jar/client/y;

    invoke-direct {v1, p0}, Lcom/google/android/youtube/api/jar/client/y;-><init>(Lcom/google/android/youtube/api/jar/client/v;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    return-void
.end method
