.class public final Lcom/google/android/youtube/api/jar/client/dy;
.super Lcom/google/android/youtube/api/jar/client/ci;
.source "SourceFile"


# instance fields
.field private final a:Lcom/google/android/youtube/api/jar/client/ei;

.field private final b:Landroid/content/Context;

.field private final c:Landroid/os/Handler;

.field private d:Lcom/google/android/youtube/api/jar/client/eh;

.field private e:Landroid/view/SurfaceHolder;


# direct methods
.method public constructor <init>(Lcom/google/android/youtube/api/jar/client/ei;Landroid/content/Context;Landroid/os/Handler;)V
    .locals 1

    invoke-direct {p0}, Lcom/google/android/youtube/api/jar/client/ci;-><init>()V

    const-string v0, "listener cannot be null"

    invoke-static {p1, v0}, Lcom/google/android/youtube/core/utils/s;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/youtube/api/jar/client/ei;

    iput-object v0, p0, Lcom/google/android/youtube/api/jar/client/dy;->a:Lcom/google/android/youtube/api/jar/client/ei;

    const-string v0, "context cannot be null"

    invoke-static {p2, v0}, Lcom/google/android/youtube/core/utils/s;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/Context;

    iput-object v0, p0, Lcom/google/android/youtube/api/jar/client/dy;->b:Landroid/content/Context;

    const-string v0, "uiHandler cannot be null"

    invoke-static {p3, v0}, Lcom/google/android/youtube/core/utils/s;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/os/Handler;

    iput-object v0, p0, Lcom/google/android/youtube/api/jar/client/dy;->c:Landroid/os/Handler;

    return-void
.end method

.method static synthetic a(Lcom/google/android/youtube/api/jar/client/dy;)Landroid/content/Context;
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/api/jar/client/dy;->b:Landroid/content/Context;

    return-object v0
.end method

.method static synthetic a(Lcom/google/android/youtube/api/jar/client/dy;Landroid/view/SurfaceHolder;)Landroid/view/SurfaceHolder;
    .locals 0

    iput-object p1, p0, Lcom/google/android/youtube/api/jar/client/dy;->e:Landroid/view/SurfaceHolder;

    return-object p1
.end method

.method static synthetic a(Lcom/google/android/youtube/api/jar/client/dy;Lcom/google/android/youtube/api/jar/client/eh;)Lcom/google/android/youtube/api/jar/client/eh;
    .locals 0

    iput-object p1, p0, Lcom/google/android/youtube/api/jar/client/dy;->d:Lcom/google/android/youtube/api/jar/client/eh;

    return-object p1
.end method

.method static synthetic b(Lcom/google/android/youtube/api/jar/client/dy;)Lcom/google/android/youtube/api/jar/client/eh;
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/api/jar/client/dy;->d:Lcom/google/android/youtube/api/jar/client/eh;

    return-object v0
.end method

.method public static b(Z)Z
    .locals 4

    const/4 v1, 0x1

    const/4 v0, 0x0

    if-eqz p0, :cond_2

    sget v2, Lcom/google/android/youtube/core/utils/Util;->a:I

    const/16 v3, 0xd

    if-lt v2, v3, :cond_0

    const-string v2, "ka"

    sget-object v3, Landroid/os/Build;->DEVICE:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    const-string v2, "eagle"

    sget-object v3, Landroid/os/Build;->DEVICE:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    const-string v2, "asura"

    sget-object v3, Landroid/os/Build;->DEVICE:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    :cond_0
    move v0, v1

    :cond_1
    :goto_0
    return v0

    :cond_2
    sget v2, Lcom/google/android/youtube/core/utils/Util;->a:I

    const/16 v3, 0xe

    if-ge v2, v3, :cond_1

    move v0, v1

    goto :goto_0
.end method

.method static synthetic c(Lcom/google/android/youtube/api/jar/client/dy;)Landroid/view/SurfaceHolder;
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/api/jar/client/dy;->e:Landroid/view/SurfaceHolder;

    return-object v0
.end method

.method static synthetic d(Lcom/google/android/youtube/api/jar/client/dy;)Lcom/google/android/youtube/api/jar/client/ei;
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/api/jar/client/dy;->a:Lcom/google/android/youtube/api/jar/client/ei;

    return-object v0
.end method


# virtual methods
.method public final a(I)V
    .locals 2

    iget-object v0, p0, Lcom/google/android/youtube/api/jar/client/dy;->c:Landroid/os/Handler;

    new-instance v1, Lcom/google/android/youtube/api/jar/client/ee;

    invoke-direct {v1, p0, p1}, Lcom/google/android/youtube/api/jar/client/ee;-><init>(Lcom/google/android/youtube/api/jar/client/dy;I)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    return-void
.end method

.method public final a(II)V
    .locals 2

    iget-object v0, p0, Lcom/google/android/youtube/api/jar/client/dy;->c:Landroid/os/Handler;

    new-instance v1, Lcom/google/android/youtube/api/jar/client/ed;

    invoke-direct {v1, p0, p1, p2}, Lcom/google/android/youtube/api/jar/client/ed;-><init>(Lcom/google/android/youtube/api/jar/client/dy;II)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    return-void
.end method

.method public final a(Lcom/google/android/youtube/api/service/a/az;)V
    .locals 2

    iget-object v0, p0, Lcom/google/android/youtube/api/jar/client/dy;->c:Landroid/os/Handler;

    new-instance v1, Lcom/google/android/youtube/api/jar/client/dz;

    invoke-direct {v1, p0, p1}, Lcom/google/android/youtube/api/jar/client/dz;-><init>(Lcom/google/android/youtube/api/jar/client/dy;Lcom/google/android/youtube/api/service/a/az;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    return-void
.end method

.method public final a(Z)V
    .locals 2

    iget-object v0, p0, Lcom/google/android/youtube/api/jar/client/dy;->c:Landroid/os/Handler;

    new-instance v1, Lcom/google/android/youtube/api/jar/client/ea;

    invoke-direct {v1, p0, p1}, Lcom/google/android/youtube/api/jar/client/ea;-><init>(Lcom/google/android/youtube/api/jar/client/dy;Z)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    return-void
.end method

.method public final a()Z
    .locals 4

    new-instance v0, Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-direct {v0}, Ljava/util/concurrent/atomic/AtomicBoolean;-><init>()V

    new-instance v1, Landroid/os/ConditionVariable;

    invoke-direct {v1}, Landroid/os/ConditionVariable;-><init>()V

    iget-object v2, p0, Lcom/google/android/youtube/api/jar/client/dy;->c:Landroid/os/Handler;

    new-instance v3, Lcom/google/android/youtube/api/jar/client/ec;

    invoke-direct {v3, p0, v0, v1}, Lcom/google/android/youtube/api/jar/client/ec;-><init>(Lcom/google/android/youtube/api/jar/client/dy;Ljava/util/concurrent/atomic/AtomicBoolean;Landroid/os/ConditionVariable;)V

    invoke-virtual {v2, v3}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    invoke-virtual {v1}, Landroid/os/ConditionVariable;->block()V

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicBoolean;->get()Z

    move-result v0

    return v0
.end method

.method public final b()Landroid/graphics/Rect;
    .locals 4

    new-instance v0, Ljava/util/concurrent/atomic/AtomicReference;

    invoke-direct {v0}, Ljava/util/concurrent/atomic/AtomicReference;-><init>()V

    new-instance v1, Landroid/os/ConditionVariable;

    invoke-direct {v1}, Landroid/os/ConditionVariable;-><init>()V

    iget-object v2, p0, Lcom/google/android/youtube/api/jar/client/dy;->c:Landroid/os/Handler;

    new-instance v3, Lcom/google/android/youtube/api/jar/client/eb;

    invoke-direct {v3, p0, v0, v1}, Lcom/google/android/youtube/api/jar/client/eb;-><init>(Lcom/google/android/youtube/api/jar/client/dy;Ljava/util/concurrent/atomic/AtomicReference;Landroid/os/ConditionVariable;)V

    invoke-virtual {v2, v3}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    invoke-virtual {v1}, Landroid/os/ConditionVariable;->block()V

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/graphics/Rect;

    return-object v0
.end method

.method public final b(I)V
    .locals 2

    iget-object v0, p0, Lcom/google/android/youtube/api/jar/client/dy;->c:Landroid/os/Handler;

    new-instance v1, Lcom/google/android/youtube/api/jar/client/eg;

    invoke-direct {v1, p0, p1}, Lcom/google/android/youtube/api/jar/client/eg;-><init>(Lcom/google/android/youtube/api/jar/client/dy;I)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    return-void
.end method

.method public final c()V
    .locals 2

    iget-object v0, p0, Lcom/google/android/youtube/api/jar/client/dy;->c:Landroid/os/Handler;

    new-instance v1, Lcom/google/android/youtube/api/jar/client/ef;

    invoke-direct {v1, p0}, Lcom/google/android/youtube/api/jar/client/ef;-><init>(Lcom/google/android/youtube/api/jar/client/dy;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    return-void
.end method

.method public final d()V
    .locals 2

    const/4 v1, 0x0

    iget-object v0, p0, Lcom/google/android/youtube/api/jar/client/dy;->a:Lcom/google/android/youtube/api/jar/client/ei;

    invoke-interface {v0}, Lcom/google/android/youtube/api/jar/client/ei;->e()V

    iget-object v0, p0, Lcom/google/android/youtube/api/jar/client/dy;->d:Lcom/google/android/youtube/api/jar/client/eh;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/youtube/api/jar/client/dy;->d:Lcom/google/android/youtube/api/jar/client/eh;

    invoke-virtual {v0}, Lcom/google/android/youtube/api/jar/client/eh;->a()V

    iput-object v1, p0, Lcom/google/android/youtube/api/jar/client/dy;->d:Lcom/google/android/youtube/api/jar/client/eh;

    :cond_0
    iput-object v1, p0, Lcom/google/android/youtube/api/jar/client/dy;->e:Landroid/view/SurfaceHolder;

    return-void
.end method
