.class public final Lcom/google/android/youtube/api/jar/ControllerOptionsView;
.super Landroid/view/ViewGroup;
.source "SourceFile"

# interfaces
.implements Landroid/view/View$OnClickListener;
.implements Landroid/view/animation/Animation$AnimationListener;


# instance fields
.field private final a:F

.field private final b:Landroid/widget/ImageButton;

.field private final c:Landroid/widget/ImageButton;

.field private final d:Landroid/widget/ImageButton;

.field private final e:Landroid/widget/TextView;

.field private final f:Landroid/graphics/drawable/StateListDrawable;

.field private final g:Landroid/graphics/drawable/StateListDrawable;

.field private final h:Landroid/view/animation/AnimationSet;

.field private final i:Landroid/view/animation/AnimationSet;

.field private final j:Lcom/google/android/youtube/api/jar/q;

.field private k:Lcom/google/android/youtube/core/player/overlay/e;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/google/android/youtube/api/jar/q;)V
    .locals 10

    const-string v0, "context cannot be null"

    invoke-static {p1, v0}, Lcom/google/android/youtube/core/utils/s;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/Context;

    invoke-direct {p0, v0}, Landroid/view/ViewGroup;-><init>(Landroid/content/Context;)V

    const-string v0, "optionsViewListener cannot be null"

    invoke-static {p2, v0}, Lcom/google/android/youtube/core/utils/s;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/youtube/api/jar/q;

    iput-object v0, p0, Lcom/google/android/youtube/api/jar/ControllerOptionsView;->j:Lcom/google/android/youtube/api/jar/q;

    invoke-virtual {p0}, Lcom/google/android/youtube/api/jar/ControllerOptionsView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v1

    iget v1, v1, Landroid/util/DisplayMetrics;->density:F

    iput v1, p0, Lcom/google/android/youtube/api/jar/ControllerOptionsView;->a:F

    const/high16 v1, 0x41200000

    iget v2, p0, Lcom/google/android/youtube/api/jar/ControllerOptionsView;->a:F

    mul-float/2addr v1, v2

    float-to-int v1, v1

    new-instance v2, Landroid/widget/ImageButton;

    invoke-direct {v2, p1}, Landroid/widget/ImageButton;-><init>(Landroid/content/Context;)V

    iput-object v2, p0, Lcom/google/android/youtube/api/jar/ControllerOptionsView;->b:Landroid/widget/ImageButton;

    new-instance v2, Landroid/widget/ImageButton;

    invoke-direct {v2, p1}, Landroid/widget/ImageButton;-><init>(Landroid/content/Context;)V

    iput-object v2, p0, Lcom/google/android/youtube/api/jar/ControllerOptionsView;->c:Landroid/widget/ImageButton;

    new-instance v2, Landroid/widget/ImageButton;

    invoke-direct {v2, p1}, Landroid/widget/ImageButton;-><init>(Landroid/content/Context;)V

    iput-object v2, p0, Lcom/google/android/youtube/api/jar/ControllerOptionsView;->d:Landroid/widget/ImageButton;

    new-instance v2, Landroid/widget/TextView;

    invoke-direct {v2, p1}, Landroid/widget/TextView;-><init>(Landroid/content/Context;)V

    iput-object v2, p0, Lcom/google/android/youtube/api/jar/ControllerOptionsView;->e:Landroid/widget/TextView;

    new-instance v2, Landroid/graphics/drawable/StateListDrawable;

    invoke-direct {v2}, Landroid/graphics/drawable/StateListDrawable;-><init>()V

    iput-object v2, p0, Lcom/google/android/youtube/api/jar/ControllerOptionsView;->f:Landroid/graphics/drawable/StateListDrawable;

    iget-object v2, p0, Lcom/google/android/youtube/api/jar/ControllerOptionsView;->f:Landroid/graphics/drawable/StateListDrawable;

    sget-object v3, Landroid/view/View;->SELECTED_STATE_SET:[I

    const v4, 0x7f02000a

    invoke-virtual {v0, v4}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Landroid/graphics/drawable/StateListDrawable;->addState([ILandroid/graphics/drawable/Drawable;)V

    iget-object v2, p0, Lcom/google/android/youtube/api/jar/ControllerOptionsView;->f:Landroid/graphics/drawable/StateListDrawable;

    sget-object v3, Landroid/view/View;->EMPTY_STATE_SET:[I

    const v4, 0x7f020009

    invoke-virtual {v0, v4}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Landroid/graphics/drawable/StateListDrawable;->addState([ILandroid/graphics/drawable/Drawable;)V

    new-instance v2, Landroid/graphics/drawable/StateListDrawable;

    invoke-direct {v2}, Landroid/graphics/drawable/StateListDrawable;-><init>()V

    iput-object v2, p0, Lcom/google/android/youtube/api/jar/ControllerOptionsView;->g:Landroid/graphics/drawable/StateListDrawable;

    iget-object v2, p0, Lcom/google/android/youtube/api/jar/ControllerOptionsView;->g:Landroid/graphics/drawable/StateListDrawable;

    sget-object v3, Landroid/view/View;->SELECTED_STATE_SET:[I

    const v4, 0x7f020008

    invoke-virtual {v0, v4}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Landroid/graphics/drawable/StateListDrawable;->addState([ILandroid/graphics/drawable/Drawable;)V

    iget-object v2, p0, Lcom/google/android/youtube/api/jar/ControllerOptionsView;->g:Landroid/graphics/drawable/StateListDrawable;

    sget-object v3, Landroid/view/View;->EMPTY_STATE_SET:[I

    const v4, 0x7f020007

    invoke-virtual {v0, v4}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Landroid/graphics/drawable/StateListDrawable;->addState([ILandroid/graphics/drawable/Drawable;)V

    iget-object v2, p0, Lcom/google/android/youtube/api/jar/ControllerOptionsView;->b:Landroid/widget/ImageButton;

    iget-object v3, p0, Lcom/google/android/youtube/api/jar/ControllerOptionsView;->f:Landroid/graphics/drawable/StateListDrawable;

    invoke-virtual {v2, v3}, Landroid/widget/ImageButton;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    iget-object v2, p0, Lcom/google/android/youtube/api/jar/ControllerOptionsView;->b:Landroid/widget/ImageButton;

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Landroid/widget/ImageButton;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    iget-object v2, p0, Lcom/google/android/youtube/api/jar/ControllerOptionsView;->b:Landroid/widget/ImageButton;

    const/4 v3, 0x0

    invoke-virtual {v2, v1, v1, v3, v1}, Landroid/widget/ImageButton;->setPadding(IIII)V

    iget-object v2, p0, Lcom/google/android/youtube/api/jar/ControllerOptionsView;->b:Landroid/widget/ImageButton;

    const v3, 0x7f0b0090

    invoke-virtual {p1, v3}, Landroid/content/Context;->getText(I)Ljava/lang/CharSequence;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/widget/ImageButton;->setContentDescription(Ljava/lang/CharSequence;)V

    iget-object v2, p0, Lcom/google/android/youtube/api/jar/ControllerOptionsView;->b:Landroid/widget/ImageButton;

    invoke-virtual {v2, p0}, Landroid/widget/ImageButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v2, p0, Lcom/google/android/youtube/api/jar/ControllerOptionsView;->b:Landroid/widget/ImageButton;

    const/16 v3, 0x8

    invoke-virtual {v2, v3}, Landroid/widget/ImageButton;->setVisibility(I)V

    new-instance v2, Landroid/graphics/drawable/StateListDrawable;

    invoke-direct {v2}, Landroid/graphics/drawable/StateListDrawable;-><init>()V

    sget-object v3, Landroid/view/View;->SELECTED_STATE_SET:[I

    const v4, 0x7f020006

    invoke-virtual {v0, v4}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Landroid/graphics/drawable/StateListDrawable;->addState([ILandroid/graphics/drawable/Drawable;)V

    sget-object v3, Landroid/view/View;->EMPTY_STATE_SET:[I

    const v4, 0x7f020005

    invoke-virtual {v0, v4}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    invoke-virtual {v2, v3, v0}, Landroid/graphics/drawable/StateListDrawable;->addState([ILandroid/graphics/drawable/Drawable;)V

    iget-object v0, p0, Lcom/google/android/youtube/api/jar/ControllerOptionsView;->c:Landroid/widget/ImageButton;

    invoke-virtual {v0, v2}, Landroid/widget/ImageButton;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    iget-object v0, p0, Lcom/google/android/youtube/api/jar/ControllerOptionsView;->c:Landroid/widget/ImageButton;

    const/4 v2, 0x0

    invoke-virtual {v0, v2}, Landroid/widget/ImageButton;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    iget-object v0, p0, Lcom/google/android/youtube/api/jar/ControllerOptionsView;->c:Landroid/widget/ImageButton;

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v1, v2, v1}, Landroid/widget/ImageButton;->setPadding(IIII)V

    iget-object v0, p0, Lcom/google/android/youtube/api/jar/ControllerOptionsView;->c:Landroid/widget/ImageButton;

    const v2, 0x7f0b008c

    invoke-virtual {p1, v2}, Landroid/content/Context;->getText(I)Ljava/lang/CharSequence;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/widget/ImageButton;->setContentDescription(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lcom/google/android/youtube/api/jar/ControllerOptionsView;->c:Landroid/widget/ImageButton;

    invoke-virtual {v0, p0}, Landroid/widget/ImageButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v0, p0, Lcom/google/android/youtube/api/jar/ControllerOptionsView;->c:Landroid/widget/ImageButton;

    const/16 v2, 0x8

    invoke-virtual {v0, v2}, Landroid/widget/ImageButton;->setVisibility(I)V

    iget-object v0, p0, Lcom/google/android/youtube/api/jar/ControllerOptionsView;->d:Landroid/widget/ImageButton;

    const v2, 0x7f020018

    invoke-virtual {v0, v2}, Landroid/widget/ImageButton;->setImageResource(I)V

    iget-object v0, p0, Lcom/google/android/youtube/api/jar/ControllerOptionsView;->d:Landroid/widget/ImageButton;

    const/4 v2, 0x0

    invoke-virtual {v0, v2}, Landroid/widget/ImageButton;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    iget-object v0, p0, Lcom/google/android/youtube/api/jar/ControllerOptionsView;->d:Landroid/widget/ImageButton;

    invoke-virtual {v0, v1, v1, v1, v1}, Landroid/widget/ImageButton;->setPadding(IIII)V

    invoke-static {p1}, Lcom/google/android/youtube/player/e;->a(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/youtube/api/jar/ControllerOptionsView;->d:Landroid/widget/ImageButton;

    const v2, 0x7f0b0095

    invoke-virtual {p1, v2}, Landroid/content/Context;->getText(I)Ljava/lang/CharSequence;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/widget/ImageButton;->setContentDescription(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lcom/google/android/youtube/api/jar/ControllerOptionsView;->d:Landroid/widget/ImageButton;

    invoke-virtual {v0, p0}, Landroid/widget/ImageButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    :cond_0
    iget-object v0, p0, Lcom/google/android/youtube/api/jar/ControllerOptionsView;->e:Landroid/widget/TextView;

    invoke-virtual {v0}, Landroid/widget/TextView;->setSingleLine()V

    iget-object v0, p0, Lcom/google/android/youtube/api/jar/ControllerOptionsView;->e:Landroid/widget/TextView;

    const/4 v2, 0x1

    const/high16 v3, 0x41900000

    invoke-virtual {v0, v2, v3}, Landroid/widget/TextView;->setTextSize(IF)V

    iget-object v0, p0, Lcom/google/android/youtube/api/jar/ControllerOptionsView;->e:Landroid/widget/TextView;

    sget-object v2, Landroid/text/TextUtils$TruncateAt;->END:Landroid/text/TextUtils$TruncateAt;

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setEllipsize(Landroid/text/TextUtils$TruncateAt;)V

    iget-object v0, p0, Lcom/google/android/youtube/api/jar/ControllerOptionsView;->e:Landroid/widget/TextView;

    const/4 v2, -0x1

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setTextColor(I)V

    iget-object v0, p0, Lcom/google/android/youtube/api/jar/ControllerOptionsView;->e:Landroid/widget/TextView;

    const/16 v2, 0x11

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setGravity(I)V

    iget-object v0, p0, Lcom/google/android/youtube/api/jar/ControllerOptionsView;->e:Landroid/widget/TextView;

    mul-int/lit8 v2, v1, 0x2

    invoke-virtual {v0, v2, v1, v1, v1}, Landroid/widget/TextView;->setPadding(IIII)V

    new-instance v0, Landroid/view/animation/AnimationSet;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Landroid/view/animation/AnimationSet;-><init>(Z)V

    iput-object v0, p0, Lcom/google/android/youtube/api/jar/ControllerOptionsView;->h:Landroid/view/animation/AnimationSet;

    iget-object v0, p0, Lcom/google/android/youtube/api/jar/ControllerOptionsView;->h:Landroid/view/animation/AnimationSet;

    new-instance v1, Landroid/view/animation/DecelerateInterpolator;

    invoke-direct {v1}, Landroid/view/animation/DecelerateInterpolator;-><init>()V

    invoke-virtual {v0, v1}, Landroid/view/animation/AnimationSet;->setInterpolator(Landroid/view/animation/Interpolator;)V

    iget-object v0, p0, Lcom/google/android/youtube/api/jar/ControllerOptionsView;->h:Landroid/view/animation/AnimationSet;

    const-wide/16 v1, 0xc8

    invoke-virtual {v0, v1, v2}, Landroid/view/animation/AnimationSet;->setDuration(J)V

    iget-object v0, p0, Lcom/google/android/youtube/api/jar/ControllerOptionsView;->h:Landroid/view/animation/AnimationSet;

    new-instance v1, Landroid/view/animation/AlphaAnimation;

    const/4 v2, 0x0

    const/high16 v3, 0x3f800000

    invoke-direct {v1, v2, v3}, Landroid/view/animation/AlphaAnimation;-><init>(FF)V

    invoke-virtual {v0, v1}, Landroid/view/animation/AnimationSet;->addAnimation(Landroid/view/animation/Animation;)V

    iget-object v9, p0, Lcom/google/android/youtube/api/jar/ControllerOptionsView;->h:Landroid/view/animation/AnimationSet;

    new-instance v0, Landroid/view/animation/TranslateAnimation;

    const/4 v1, 0x1

    const v2, 0x3ecccccd

    const/4 v3, 0x1

    const/4 v4, 0x0

    const/4 v5, 0x1

    const/4 v6, 0x0

    const/4 v7, 0x1

    const/4 v8, 0x0

    invoke-direct/range {v0 .. v8}, Landroid/view/animation/TranslateAnimation;-><init>(IFIFIFIF)V

    invoke-virtual {v9, v0}, Landroid/view/animation/AnimationSet;->addAnimation(Landroid/view/animation/Animation;)V

    iget-object v0, p0, Lcom/google/android/youtube/api/jar/ControllerOptionsView;->h:Landroid/view/animation/AnimationSet;

    invoke-virtual {v0, p0}, Landroid/view/animation/AnimationSet;->setAnimationListener(Landroid/view/animation/Animation$AnimationListener;)V

    new-instance v0, Landroid/view/animation/AnimationSet;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Landroid/view/animation/AnimationSet;-><init>(Z)V

    iput-object v0, p0, Lcom/google/android/youtube/api/jar/ControllerOptionsView;->i:Landroid/view/animation/AnimationSet;

    iget-object v0, p0, Lcom/google/android/youtube/api/jar/ControllerOptionsView;->i:Landroid/view/animation/AnimationSet;

    new-instance v1, Landroid/view/animation/AccelerateInterpolator;

    invoke-direct {v1}, Landroid/view/animation/AccelerateInterpolator;-><init>()V

    invoke-virtual {v0, v1}, Landroid/view/animation/AnimationSet;->setInterpolator(Landroid/view/animation/Interpolator;)V

    iget-object v0, p0, Lcom/google/android/youtube/api/jar/ControllerOptionsView;->i:Landroid/view/animation/AnimationSet;

    const-wide/16 v1, 0xc8

    invoke-virtual {v0, v1, v2}, Landroid/view/animation/AnimationSet;->setDuration(J)V

    iget-object v0, p0, Lcom/google/android/youtube/api/jar/ControllerOptionsView;->i:Landroid/view/animation/AnimationSet;

    new-instance v1, Landroid/view/animation/AlphaAnimation;

    const/high16 v2, 0x3f800000

    const/4 v3, 0x0

    invoke-direct {v1, v2, v3}, Landroid/view/animation/AlphaAnimation;-><init>(FF)V

    invoke-virtual {v0, v1}, Landroid/view/animation/AnimationSet;->addAnimation(Landroid/view/animation/Animation;)V

    iget-object v9, p0, Lcom/google/android/youtube/api/jar/ControllerOptionsView;->i:Landroid/view/animation/AnimationSet;

    new-instance v0, Landroid/view/animation/TranslateAnimation;

    const/4 v1, 0x1

    const/4 v2, 0x0

    const/4 v3, 0x1

    const v4, 0x3ecccccd

    const/4 v5, 0x1

    const/4 v6, 0x0

    const/4 v7, 0x1

    const/4 v8, 0x0

    invoke-direct/range {v0 .. v8}, Landroid/view/animation/TranslateAnimation;-><init>(IFIFIFIF)V

    invoke-virtual {v9, v0}, Landroid/view/animation/AnimationSet;->addAnimation(Landroid/view/animation/Animation;)V

    iget-object v0, p0, Lcom/google/android/youtube/api/jar/ControllerOptionsView;->i:Landroid/view/animation/AnimationSet;

    invoke-virtual {v0, p0}, Landroid/view/animation/AnimationSet;->setAnimationListener(Landroid/view/animation/Animation$AnimationListener;)V

    iget-object v0, p0, Lcom/google/android/youtube/api/jar/ControllerOptionsView;->b:Landroid/widget/ImageButton;

    invoke-virtual {p0, v0}, Lcom/google/android/youtube/api/jar/ControllerOptionsView;->addView(Landroid/view/View;)V

    iget-object v0, p0, Lcom/google/android/youtube/api/jar/ControllerOptionsView;->c:Landroid/widget/ImageButton;

    invoke-virtual {p0, v0}, Lcom/google/android/youtube/api/jar/ControllerOptionsView;->addView(Landroid/view/View;)V

    iget-object v0, p0, Lcom/google/android/youtube/api/jar/ControllerOptionsView;->e:Landroid/widget/TextView;

    invoke-virtual {p0, v0}, Lcom/google/android/youtube/api/jar/ControllerOptionsView;->addView(Landroid/view/View;)V

    iget-object v0, p0, Lcom/google/android/youtube/api/jar/ControllerOptionsView;->d:Landroid/widget/ImageButton;

    invoke-virtual {p0, v0}, Lcom/google/android/youtube/api/jar/ControllerOptionsView;->addView(Landroid/view/View;)V

    const v0, 0x7f02001b

    invoke-virtual {p0, v0}, Lcom/google/android/youtube/api/jar/ControllerOptionsView;->setBackgroundResource(I)V

    const/4 v0, 0x4

    invoke-super {p0, v0}, Landroid/view/ViewGroup;->setVisibility(I)V

    return-void
.end method

.method private static a(Landroid/view/View;II)I
    .locals 3

    invoke-virtual {p0}, Landroid/view/View;->getMeasuredHeight()I

    move-result v0

    sub-int v0, p2, v0

    div-int/lit8 v0, v0, 0x2

    invoke-virtual {p0}, Landroid/view/View;->getMeasuredWidth()I

    move-result v1

    add-int/2addr v1, p1

    invoke-virtual {p0}, Landroid/view/View;->getMeasuredHeight()I

    move-result v2

    add-int/2addr v2, v0

    invoke-virtual {p0, p1, v0, v1, v2}, Landroid/view/View;->layout(IIII)V

    invoke-virtual {p0}, Landroid/view/View;->getMeasuredWidth()I

    move-result v0

    return v0
.end method


# virtual methods
.method public final a()V
    .locals 1

    invoke-virtual {p0}, Lcom/google/android/youtube/api/jar/ControllerOptionsView;->getVisibility()I

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/youtube/api/jar/ControllerOptionsView;->h:Landroid/view/animation/AnimationSet;

    invoke-virtual {v0}, Landroid/view/animation/AnimationSet;->cancel()V

    iget-object v0, p0, Lcom/google/android/youtube/api/jar/ControllerOptionsView;->i:Landroid/view/animation/AnimationSet;

    invoke-virtual {v0}, Landroid/view/animation/AnimationSet;->reset()V

    iget-object v0, p0, Lcom/google/android/youtube/api/jar/ControllerOptionsView;->i:Landroid/view/animation/AnimationSet;

    invoke-virtual {p0, v0}, Lcom/google/android/youtube/api/jar/ControllerOptionsView;->startAnimation(Landroid/view/animation/Animation;)V

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/google/android/youtube/api/jar/ControllerOptionsView;->i:Landroid/view/animation/AnimationSet;

    invoke-virtual {v0}, Landroid/view/animation/AnimationSet;->cancel()V

    iget-object v0, p0, Lcom/google/android/youtube/api/jar/ControllerOptionsView;->h:Landroid/view/animation/AnimationSet;

    invoke-virtual {v0}, Landroid/view/animation/AnimationSet;->reset()V

    iget-object v0, p0, Lcom/google/android/youtube/api/jar/ControllerOptionsView;->h:Landroid/view/animation/AnimationSet;

    invoke-virtual {p0, v0}, Lcom/google/android/youtube/api/jar/ControllerOptionsView;->startAnimation(Landroid/view/animation/Animation;)V

    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/google/android/youtube/api/jar/ControllerOptionsView;->setVisibility(I)V

    goto :goto_0
.end method

.method public final onAnimationEnd(Landroid/view/animation/Animation;)V
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/api/jar/ControllerOptionsView;->i:Landroid/view/animation/AnimationSet;

    if-ne p1, v0, :cond_0

    const/4 v0, 0x4

    invoke-virtual {p0, v0}, Lcom/google/android/youtube/api/jar/ControllerOptionsView;->setVisibility(I)V

    :cond_0
    return-void
.end method

.method public final onAnimationRepeat(Landroid/view/animation/Animation;)V
    .locals 0

    return-void
.end method

.method public final onAnimationStart(Landroid/view/animation/Animation;)V
    .locals 0

    return-void
.end method

.method public final onClick(Landroid/view/View;)V
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/api/jar/ControllerOptionsView;->b:Landroid/widget/ImageButton;

    if-ne p1, v0, :cond_1

    iget-object v0, p0, Lcom/google/android/youtube/api/jar/ControllerOptionsView;->k:Lcom/google/android/youtube/core/player/overlay/e;

    invoke-interface {v0}, Lcom/google/android/youtube/core/player/overlay/e;->f()V

    iget-object v0, p0, Lcom/google/android/youtube/api/jar/ControllerOptionsView;->j:Lcom/google/android/youtube/api/jar/q;

    invoke-interface {v0}, Lcom/google/android/youtube/api/jar/q;->b()V

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v0, p0, Lcom/google/android/youtube/api/jar/ControllerOptionsView;->c:Landroid/widget/ImageButton;

    if-ne p1, v0, :cond_2

    iget-object v0, p0, Lcom/google/android/youtube/api/jar/ControllerOptionsView;->k:Lcom/google/android/youtube/core/player/overlay/e;

    invoke-interface {v0}, Lcom/google/android/youtube/core/player/overlay/e;->g()V

    iget-object v0, p0, Lcom/google/android/youtube/api/jar/ControllerOptionsView;->j:Lcom/google/android/youtube/api/jar/q;

    invoke-interface {v0}, Lcom/google/android/youtube/api/jar/q;->b()V

    goto :goto_0

    :cond_2
    iget-object v0, p0, Lcom/google/android/youtube/api/jar/ControllerOptionsView;->d:Landroid/widget/ImageButton;

    if-ne p1, v0, :cond_0

    iget-object v0, p0, Lcom/google/android/youtube/api/jar/ControllerOptionsView;->j:Lcom/google/android/youtube/api/jar/q;

    invoke-interface {v0}, Lcom/google/android/youtube/api/jar/q;->a()V

    goto :goto_0
.end method

.method protected final onLayout(ZIIII)V
    .locals 5

    const/4 v0, 0x0

    const/16 v4, 0x8

    sub-int v1, p5, p3

    sub-int v2, p4, p2

    iget-object v3, p0, Lcom/google/android/youtube/api/jar/ControllerOptionsView;->b:Landroid/widget/ImageButton;

    invoke-virtual {v3}, Landroid/widget/ImageButton;->getVisibility()I

    move-result v3

    if-eq v3, v4, :cond_0

    iget-object v3, p0, Lcom/google/android/youtube/api/jar/ControllerOptionsView;->b:Landroid/widget/ImageButton;

    invoke-static {v3, v0, v1}, Lcom/google/android/youtube/api/jar/ControllerOptionsView;->a(Landroid/view/View;II)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    :cond_0
    iget-object v3, p0, Lcom/google/android/youtube/api/jar/ControllerOptionsView;->c:Landroid/widget/ImageButton;

    invoke-virtual {v3}, Landroid/widget/ImageButton;->getVisibility()I

    move-result v3

    if-eq v3, v4, :cond_1

    iget-object v3, p0, Lcom/google/android/youtube/api/jar/ControllerOptionsView;->c:Landroid/widget/ImageButton;

    invoke-static {v3, v0, v1}, Lcom/google/android/youtube/api/jar/ControllerOptionsView;->a(Landroid/view/View;II)I

    move-result v3

    add-int/2addr v0, v3

    :cond_1
    iget-object v3, p0, Lcom/google/android/youtube/api/jar/ControllerOptionsView;->e:Landroid/widget/TextView;

    invoke-virtual {v3}, Landroid/widget/TextView;->getVisibility()I

    move-result v3

    if-eq v3, v4, :cond_2

    iget-object v3, p0, Lcom/google/android/youtube/api/jar/ControllerOptionsView;->e:Landroid/widget/TextView;

    invoke-static {v3, v0, v1}, Lcom/google/android/youtube/api/jar/ControllerOptionsView;->a(Landroid/view/View;II)I

    :cond_2
    iget-object v0, p0, Lcom/google/android/youtube/api/jar/ControllerOptionsView;->d:Landroid/widget/ImageButton;

    iget-object v3, p0, Lcom/google/android/youtube/api/jar/ControllerOptionsView;->d:Landroid/widget/ImageButton;

    invoke-virtual {v3}, Landroid/widget/ImageButton;->getMeasuredWidth()I

    move-result v3

    sub-int/2addr v2, v3

    invoke-static {v0, v2, v1}, Lcom/google/android/youtube/api/jar/ControllerOptionsView;->a(Landroid/view/View;II)I

    return-void
.end method

.method protected final onMeasure(II)V
    .locals 6

    const/4 v1, 0x0

    const/16 v5, 0x8

    const/high16 v4, -0x80000000

    invoke-static {v1, p1}, Lcom/google/android/youtube/api/jar/ControllerOptionsView;->getDefaultSize(II)I

    move-result v0

    invoke-static {v1, p2}, Lcom/google/android/youtube/api/jar/ControllerOptionsView;->getDefaultSize(II)I

    move-result v1

    invoke-virtual {p0, v0, v1}, Lcom/google/android/youtube/api/jar/ControllerOptionsView;->setMeasuredDimension(II)V

    invoke-static {v1, v4}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v1

    iget-object v2, p0, Lcom/google/android/youtube/api/jar/ControllerOptionsView;->d:Landroid/widget/ImageButton;

    invoke-static {v0, v4}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v3

    invoke-virtual {v2, v3, v1}, Landroid/widget/ImageButton;->measure(II)V

    iget-object v2, p0, Lcom/google/android/youtube/api/jar/ControllerOptionsView;->d:Landroid/widget/ImageButton;

    invoke-virtual {v2}, Landroid/widget/ImageButton;->getMeasuredWidth()I

    move-result v2

    sub-int/2addr v0, v2

    iget-object v2, p0, Lcom/google/android/youtube/api/jar/ControllerOptionsView;->b:Landroid/widget/ImageButton;

    invoke-virtual {v2}, Landroid/widget/ImageButton;->getVisibility()I

    move-result v2

    if-eq v2, v5, :cond_0

    iget-object v2, p0, Lcom/google/android/youtube/api/jar/ControllerOptionsView;->b:Landroid/widget/ImageButton;

    invoke-static {v0, v4}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v3

    invoke-virtual {v2, v3, v1}, Landroid/widget/ImageButton;->measure(II)V

    iget-object v2, p0, Lcom/google/android/youtube/api/jar/ControllerOptionsView;->b:Landroid/widget/ImageButton;

    invoke-virtual {v2}, Landroid/widget/ImageButton;->getMeasuredWidth()I

    move-result v2

    sub-int/2addr v0, v2

    :cond_0
    iget-object v2, p0, Lcom/google/android/youtube/api/jar/ControllerOptionsView;->c:Landroid/widget/ImageButton;

    invoke-virtual {v2}, Landroid/widget/ImageButton;->getVisibility()I

    move-result v2

    if-eq v2, v5, :cond_1

    iget-object v2, p0, Lcom/google/android/youtube/api/jar/ControllerOptionsView;->c:Landroid/widget/ImageButton;

    invoke-static {v0, v4}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v3

    invoke-virtual {v2, v3, v1}, Landroid/widget/ImageButton;->measure(II)V

    iget-object v2, p0, Lcom/google/android/youtube/api/jar/ControllerOptionsView;->c:Landroid/widget/ImageButton;

    invoke-virtual {v2}, Landroid/widget/ImageButton;->getMeasuredWidth()I

    move-result v2

    sub-int/2addr v0, v2

    :cond_1
    iget-object v2, p0, Lcom/google/android/youtube/api/jar/ControllerOptionsView;->e:Landroid/widget/TextView;

    invoke-virtual {v2}, Landroid/widget/TextView;->getVisibility()I

    move-result v2

    if-eq v2, v5, :cond_2

    iget-object v2, p0, Lcom/google/android/youtube/api/jar/ControllerOptionsView;->e:Landroid/widget/TextView;

    const/high16 v3, 0x40000000

    invoke-static {v0, v3}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v0

    invoke-virtual {v2, v0, v1}, Landroid/widget/TextView;->measure(II)V

    :cond_2
    return-void
.end method

.method public final setCcEnabled(Z)V
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/api/jar/ControllerOptionsView;->c:Landroid/widget/ImageButton;

    invoke-virtual {v0, p1}, Landroid/widget/ImageButton;->setSelected(Z)V

    return-void
.end method

.method public final setControllerListener(Lcom/google/android/youtube/core/player/overlay/e;)V
    .locals 1

    const-string v0, "controllerListener cannot be null"

    invoke-static {p1, v0}, Lcom/google/android/youtube/core/utils/s;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/youtube/core/player/overlay/e;

    iput-object v0, p0, Lcom/google/android/youtube/api/jar/ControllerOptionsView;->k:Lcom/google/android/youtube/core/player/overlay/e;

    return-void
.end method

.method public final setHQisHD(Z)V
    .locals 2

    iget-object v1, p0, Lcom/google/android/youtube/api/jar/ControllerOptionsView;->b:Landroid/widget/ImageButton;

    if-eqz p1, :cond_0

    iget-object v0, p0, Lcom/google/android/youtube/api/jar/ControllerOptionsView;->g:Landroid/graphics/drawable/StateListDrawable;

    :goto_0
    invoke-virtual {v1, v0}, Landroid/widget/ImageButton;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    return-void

    :cond_0
    iget-object v0, p0, Lcom/google/android/youtube/api/jar/ControllerOptionsView;->f:Landroid/graphics/drawable/StateListDrawable;

    goto :goto_0
.end method

.method public final setHasCc(Z)V
    .locals 2

    iget-object v1, p0, Lcom/google/android/youtube/api/jar/ControllerOptionsView;->c:Landroid/widget/ImageButton;

    if-eqz p1, :cond_0

    const/4 v0, 0x0

    :goto_0
    invoke-virtual {v1, v0}, Landroid/widget/ImageButton;->setVisibility(I)V

    return-void

    :cond_0
    const/16 v0, 0x8

    goto :goto_0
.end method

.method public final setHq(Z)V
    .locals 3

    iget-object v0, p0, Lcom/google/android/youtube/api/jar/ControllerOptionsView;->b:Landroid/widget/ImageButton;

    invoke-virtual {v0, p1}, Landroid/widget/ImageButton;->setSelected(Z)V

    if-eqz p1, :cond_0

    const v0, 0x7f0b0091

    :goto_0
    iget-object v1, p0, Lcom/google/android/youtube/api/jar/ControllerOptionsView;->b:Landroid/widget/ImageButton;

    invoke-virtual {p0}, Lcom/google/android/youtube/api/jar/ControllerOptionsView;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-virtual {v2, v0}, Landroid/content/Context;->getText(I)Ljava/lang/CharSequence;

    move-result-object v0

    invoke-virtual {v1, v0}, Landroid/widget/ImageButton;->setContentDescription(Ljava/lang/CharSequence;)V

    return-void

    :cond_0
    const v0, 0x7f0b0090

    goto :goto_0
.end method

.method public final setSmallMode(Z)V
    .locals 2

    iget-object v1, p0, Lcom/google/android/youtube/api/jar/ControllerOptionsView;->e:Landroid/widget/TextView;

    if-eqz p1, :cond_0

    const/16 v0, 0x8

    :goto_0
    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setVisibility(I)V

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final setSupportsQualityToggle(Z)V
    .locals 2

    iget-object v1, p0, Lcom/google/android/youtube/api/jar/ControllerOptionsView;->b:Landroid/widget/ImageButton;

    if-eqz p1, :cond_0

    const/4 v0, 0x0

    :goto_0
    invoke-virtual {v1, v0}, Landroid/widget/ImageButton;->setVisibility(I)V

    return-void

    :cond_0
    const/16 v0, 0x8

    goto :goto_0
.end method

.method public final setVideoTitle(Ljava/lang/String;)V
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/api/jar/ControllerOptionsView;->e:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    return-void
.end method

.method public final setVisibility(I)V
    .locals 2

    invoke-super {p0, p1}, Landroid/view/ViewGroup;->setVisibility(I)V

    iget-object v1, p0, Lcom/google/android/youtube/api/jar/ControllerOptionsView;->j:Lcom/google/android/youtube/api/jar/q;

    if-nez p1, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-interface {v1, v0}, Lcom/google/android/youtube/api/jar/q;->a(Z)V

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
