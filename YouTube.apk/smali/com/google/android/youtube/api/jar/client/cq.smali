.class public final Lcom/google/android/youtube/api/jar/client/cq;
.super Lcom/google/android/youtube/api/jar/client/bw;
.source "SourceFile"


# instance fields
.field private final a:Lcom/google/android/youtube/core/player/overlay/i;

.field private final b:Landroid/os/Handler;

.field private c:Lcom/google/android/youtube/api/jar/client/cu;


# direct methods
.method public constructor <init>(Lcom/google/android/youtube/core/player/overlay/i;Landroid/os/Handler;)V
    .locals 1

    invoke-direct {p0}, Lcom/google/android/youtube/api/jar/client/bw;-><init>()V

    const-string v0, "target cannot be null"

    invoke-static {p1, v0}, Lcom/google/android/youtube/core/utils/s;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/youtube/core/player/overlay/i;

    iput-object v0, p0, Lcom/google/android/youtube/api/jar/client/cq;->a:Lcom/google/android/youtube/core/player/overlay/i;

    const-string v0, "uiHandler cannot be null"

    invoke-static {p2, v0}, Lcom/google/android/youtube/core/utils/s;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/os/Handler;

    iput-object v0, p0, Lcom/google/android/youtube/api/jar/client/cq;->b:Landroid/os/Handler;

    return-void
.end method

.method static synthetic a(Lcom/google/android/youtube/api/jar/client/cq;)Lcom/google/android/youtube/api/jar/client/cu;
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/api/jar/client/cq;->c:Lcom/google/android/youtube/api/jar/client/cu;

    return-object v0
.end method

.method static synthetic a(Lcom/google/android/youtube/api/jar/client/cq;Lcom/google/android/youtube/api/jar/client/cu;)Lcom/google/android/youtube/api/jar/client/cu;
    .locals 0

    iput-object p1, p0, Lcom/google/android/youtube/api/jar/client/cq;->c:Lcom/google/android/youtube/api/jar/client/cu;

    return-object p1
.end method

.method static synthetic b(Lcom/google/android/youtube/api/jar/client/cq;)Lcom/google/android/youtube/core/player/overlay/i;
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/api/jar/client/cq;->a:Lcom/google/android/youtube/core/player/overlay/i;

    return-object v0
.end method


# virtual methods
.method public final a()V
    .locals 2

    iget-object v0, p0, Lcom/google/android/youtube/api/jar/client/cq;->b:Landroid/os/Handler;

    new-instance v1, Lcom/google/android/youtube/api/jar/client/ct;

    invoke-direct {v1, p0}, Lcom/google/android/youtube/api/jar/client/ct;-><init>(Lcom/google/android/youtube/api/jar/client/cq;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    return-void
.end method

.method public final a(JZZ)V
    .locals 7

    iget-object v6, p0, Lcom/google/android/youtube/api/jar/client/cq;->b:Landroid/os/Handler;

    new-instance v0, Lcom/google/android/youtube/api/jar/client/cs;

    move-object v1, p0

    move-wide v2, p1

    move v4, p3

    move v5, p4

    invoke-direct/range {v0 .. v5}, Lcom/google/android/youtube/api/jar/client/cs;-><init>(Lcom/google/android/youtube/api/jar/client/cq;JZZ)V

    invoke-virtual {v6, v0}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    return-void
.end method

.method public final a(Lcom/google/android/youtube/api/service/a/at;)V
    .locals 2

    iget-object v0, p0, Lcom/google/android/youtube/api/jar/client/cq;->b:Landroid/os/Handler;

    new-instance v1, Lcom/google/android/youtube/api/jar/client/cr;

    invoke-direct {v1, p0, p1}, Lcom/google/android/youtube/api/jar/client/cr;-><init>(Lcom/google/android/youtube/api/jar/client/cq;Lcom/google/android/youtube/api/service/a/at;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    return-void
.end method

.method public final b()V
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/api/jar/client/cq;->c:Lcom/google/android/youtube/api/jar/client/cu;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/youtube/api/jar/client/cq;->c:Lcom/google/android/youtube/api/jar/client/cu;

    invoke-virtual {v0}, Lcom/google/android/youtube/api/jar/client/cu;->a()V

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/youtube/api/jar/client/cq;->c:Lcom/google/android/youtube/api/jar/client/cu;

    :cond_0
    return-void
.end method
