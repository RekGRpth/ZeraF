.class public final Lcom/google/android/youtube/api/jar/client/ai;
.super Lcom/google/android/youtube/api/jar/client/bt;
.source "SourceFile"


# instance fields
.field private final a:Lcom/google/android/youtube/core/player/overlay/ControllerOverlay;

.field private final b:Landroid/os/Handler;

.field private c:Lcom/google/android/youtube/api/jar/client/bi;


# direct methods
.method public constructor <init>(Lcom/google/android/youtube/core/player/overlay/ControllerOverlay;Landroid/os/Handler;)V
    .locals 1

    invoke-direct {p0}, Lcom/google/android/youtube/api/jar/client/bt;-><init>()V

    const-string v0, "target cannot be null"

    invoke-static {p1, v0}, Lcom/google/android/youtube/core/utils/s;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/youtube/core/player/overlay/ControllerOverlay;

    iput-object v0, p0, Lcom/google/android/youtube/api/jar/client/ai;->a:Lcom/google/android/youtube/core/player/overlay/ControllerOverlay;

    const-string v0, "uiHandler cannot be null"

    invoke-static {p2, v0}, Lcom/google/android/youtube/core/utils/s;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/os/Handler;

    iput-object v0, p0, Lcom/google/android/youtube/api/jar/client/ai;->b:Landroid/os/Handler;

    return-void
.end method

.method static synthetic a(Lcom/google/android/youtube/api/jar/client/ai;)Lcom/google/android/youtube/api/jar/client/bi;
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/api/jar/client/ai;->c:Lcom/google/android/youtube/api/jar/client/bi;

    return-object v0
.end method

.method static synthetic a(Lcom/google/android/youtube/api/jar/client/ai;Lcom/google/android/youtube/api/jar/client/bi;)Lcom/google/android/youtube/api/jar/client/bi;
    .locals 0

    iput-object p1, p0, Lcom/google/android/youtube/api/jar/client/ai;->c:Lcom/google/android/youtube/api/jar/client/bi;

    return-object p1
.end method

.method static synthetic b(Lcom/google/android/youtube/api/jar/client/ai;)Lcom/google/android/youtube/core/player/overlay/ControllerOverlay;
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/api/jar/client/ai;->a:Lcom/google/android/youtube/core/player/overlay/ControllerOverlay;

    return-object v0
.end method


# virtual methods
.method public final a()V
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/api/jar/client/ai;->c:Lcom/google/android/youtube/api/jar/client/bi;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/youtube/api/jar/client/ai;->c:Lcom/google/android/youtube/api/jar/client/bi;

    invoke-virtual {v0}, Lcom/google/android/youtube/api/jar/client/bi;->m()V

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/youtube/api/jar/client/ai;->c:Lcom/google/android/youtube/api/jar/client/bi;

    :cond_0
    return-void
.end method

.method public final a(III)V
    .locals 2

    iget-object v0, p0, Lcom/google/android/youtube/api/jar/client/ai;->b:Landroid/os/Handler;

    new-instance v1, Lcom/google/android/youtube/api/jar/client/as;

    invoke-direct {v1, p0, p1, p2, p3}, Lcom/google/android/youtube/api/jar/client/as;-><init>(Lcom/google/android/youtube/api/jar/client/ai;III)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    return-void
.end method

.method public final a(Lcom/google/android/youtube/api/service/a/aq;)V
    .locals 2

    iget-object v0, p0, Lcom/google/android/youtube/api/jar/client/ai;->b:Landroid/os/Handler;

    new-instance v1, Lcom/google/android/youtube/api/jar/client/aj;

    invoke-direct {v1, p0, p1}, Lcom/google/android/youtube/api/jar/client/aj;-><init>(Lcom/google/android/youtube/api/jar/client/ai;Lcom/google/android/youtube/api/service/a/aq;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    return-void
.end method

.method public final a(Ljava/lang/String;)V
    .locals 3

    invoke-static {p1}, Lcom/google/android/youtube/core/player/overlay/ControllerOverlay$Style;->valueOf(Ljava/lang/String;)Lcom/google/android/youtube/core/player/overlay/ControllerOverlay$Style;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/youtube/api/jar/client/ai;->b:Landroid/os/Handler;

    new-instance v2, Lcom/google/android/youtube/api/jar/client/au;

    invoke-direct {v2, p0, v0}, Lcom/google/android/youtube/api/jar/client/au;-><init>(Lcom/google/android/youtube/api/jar/client/ai;Lcom/google/android/youtube/core/player/overlay/ControllerOverlay$Style;)V

    invoke-virtual {v1, v2}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    return-void
.end method

.method public final a(Ljava/lang/String;Z)V
    .locals 2

    iget-object v0, p0, Lcom/google/android/youtube/api/jar/client/ai;->b:Landroid/os/Handler;

    new-instance v1, Lcom/google/android/youtube/api/jar/client/aq;

    invoke-direct {v1, p0, p1, p2}, Lcom/google/android/youtube/api/jar/client/aq;-><init>(Lcom/google/android/youtube/api/jar/client/ai;Ljava/lang/String;Z)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    return-void
.end method

.method public final a(Ljava/util/List;)V
    .locals 2

    iget-object v0, p0, Lcom/google/android/youtube/api/jar/client/ai;->b:Landroid/os/Handler;

    new-instance v1, Lcom/google/android/youtube/api/jar/client/ba;

    invoke-direct {v1, p0, p1}, Lcom/google/android/youtube/api/jar/client/ba;-><init>(Lcom/google/android/youtube/api/jar/client/ai;Ljava/util/List;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    return-void
.end method

.method public final a(Z)V
    .locals 2

    iget-object v0, p0, Lcom/google/android/youtube/api/jar/client/ai;->b:Landroid/os/Handler;

    new-instance v1, Lcom/google/android/youtube/api/jar/client/bb;

    invoke-direct {v1, p0, p1}, Lcom/google/android/youtube/api/jar/client/bb;-><init>(Lcom/google/android/youtube/api/jar/client/ai;Z)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    return-void
.end method

.method public final a(ILandroid/view/KeyEvent;)Z
    .locals 2

    iget-object v0, p0, Lcom/google/android/youtube/api/jar/client/ai;->b:Landroid/os/Handler;

    new-instance v1, Lcom/google/android/youtube/api/jar/client/aw;

    invoke-direct {v1, p0, p1, p2}, Lcom/google/android/youtube/api/jar/client/aw;-><init>(Lcom/google/android/youtube/api/jar/client/ai;ILandroid/view/KeyEvent;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    const/4 v0, 0x1

    return v0
.end method

.method public final b()V
    .locals 2

    iget-object v0, p0, Lcom/google/android/youtube/api/jar/client/ai;->b:Landroid/os/Handler;

    new-instance v1, Lcom/google/android/youtube/api/jar/client/ak;

    invoke-direct {v1, p0}, Lcom/google/android/youtube/api/jar/client/ak;-><init>(Lcom/google/android/youtube/api/jar/client/ai;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    return-void
.end method

.method public final b(Z)V
    .locals 2

    iget-object v0, p0, Lcom/google/android/youtube/api/jar/client/ai;->b:Landroid/os/Handler;

    new-instance v1, Lcom/google/android/youtube/api/jar/client/bc;

    invoke-direct {v1, p0, p1}, Lcom/google/android/youtube/api/jar/client/bc;-><init>(Lcom/google/android/youtube/api/jar/client/ai;Z)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    return-void
.end method

.method public final b(ILandroid/view/KeyEvent;)Z
    .locals 2

    iget-object v0, p0, Lcom/google/android/youtube/api/jar/client/ai;->b:Landroid/os/Handler;

    new-instance v1, Lcom/google/android/youtube/api/jar/client/ax;

    invoke-direct {v1, p0, p1, p2}, Lcom/google/android/youtube/api/jar/client/ax;-><init>(Lcom/google/android/youtube/api/jar/client/ai;ILandroid/view/KeyEvent;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    const/4 v0, 0x1

    return v0
.end method

.method public final c()V
    .locals 2

    iget-object v0, p0, Lcom/google/android/youtube/api/jar/client/ai;->b:Landroid/os/Handler;

    new-instance v1, Lcom/google/android/youtube/api/jar/client/al;

    invoke-direct {v1, p0}, Lcom/google/android/youtube/api/jar/client/al;-><init>(Lcom/google/android/youtube/api/jar/client/ai;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    return-void
.end method

.method public final c(Z)V
    .locals 2

    iget-object v0, p0, Lcom/google/android/youtube/api/jar/client/ai;->b:Landroid/os/Handler;

    new-instance v1, Lcom/google/android/youtube/api/jar/client/bd;

    invoke-direct {v1, p0, p1}, Lcom/google/android/youtube/api/jar/client/bd;-><init>(Lcom/google/android/youtube/api/jar/client/ai;Z)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    return-void
.end method

.method public final d()V
    .locals 2

    iget-object v0, p0, Lcom/google/android/youtube/api/jar/client/ai;->b:Landroid/os/Handler;

    new-instance v1, Lcom/google/android/youtube/api/jar/client/am;

    invoke-direct {v1, p0}, Lcom/google/android/youtube/api/jar/client/am;-><init>(Lcom/google/android/youtube/api/jar/client/ai;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    return-void
.end method

.method public final d(Z)V
    .locals 2

    iget-object v0, p0, Lcom/google/android/youtube/api/jar/client/ai;->b:Landroid/os/Handler;

    new-instance v1, Lcom/google/android/youtube/api/jar/client/be;

    invoke-direct {v1, p0, p1}, Lcom/google/android/youtube/api/jar/client/be;-><init>(Lcom/google/android/youtube/api/jar/client/ai;Z)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    return-void
.end method

.method public final e()V
    .locals 2

    iget-object v0, p0, Lcom/google/android/youtube/api/jar/client/ai;->b:Landroid/os/Handler;

    new-instance v1, Lcom/google/android/youtube/api/jar/client/an;

    invoke-direct {v1, p0}, Lcom/google/android/youtube/api/jar/client/an;-><init>(Lcom/google/android/youtube/api/jar/client/ai;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    return-void
.end method

.method public final e(Z)V
    .locals 2

    iget-object v0, p0, Lcom/google/android/youtube/api/jar/client/ai;->b:Landroid/os/Handler;

    new-instance v1, Lcom/google/android/youtube/api/jar/client/bf;

    invoke-direct {v1, p0, p1}, Lcom/google/android/youtube/api/jar/client/bf;-><init>(Lcom/google/android/youtube/api/jar/client/ai;Z)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    return-void
.end method

.method public final f()V
    .locals 2

    iget-object v0, p0, Lcom/google/android/youtube/api/jar/client/ai;->b:Landroid/os/Handler;

    new-instance v1, Lcom/google/android/youtube/api/jar/client/ao;

    invoke-direct {v1, p0}, Lcom/google/android/youtube/api/jar/client/ao;-><init>(Lcom/google/android/youtube/api/jar/client/ai;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    return-void
.end method

.method public final f(Z)V
    .locals 2

    iget-object v0, p0, Lcom/google/android/youtube/api/jar/client/ai;->b:Landroid/os/Handler;

    new-instance v1, Lcom/google/android/youtube/api/jar/client/bg;

    invoke-direct {v1, p0, p1}, Lcom/google/android/youtube/api/jar/client/bg;-><init>(Lcom/google/android/youtube/api/jar/client/ai;Z)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    return-void
.end method

.method public final g()V
    .locals 2

    iget-object v0, p0, Lcom/google/android/youtube/api/jar/client/ai;->b:Landroid/os/Handler;

    new-instance v1, Lcom/google/android/youtube/api/jar/client/ap;

    invoke-direct {v1, p0}, Lcom/google/android/youtube/api/jar/client/ap;-><init>(Lcom/google/android/youtube/api/jar/client/ai;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    return-void
.end method

.method public final g(Z)V
    .locals 2

    iget-object v0, p0, Lcom/google/android/youtube/api/jar/client/ai;->b:Landroid/os/Handler;

    new-instance v1, Lcom/google/android/youtube/api/jar/client/bh;

    invoke-direct {v1, p0, p1}, Lcom/google/android/youtube/api/jar/client/bh;-><init>(Lcom/google/android/youtube/api/jar/client/ai;Z)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    return-void
.end method

.method public final h()V
    .locals 2

    iget-object v0, p0, Lcom/google/android/youtube/api/jar/client/ai;->b:Landroid/os/Handler;

    new-instance v1, Lcom/google/android/youtube/api/jar/client/at;

    invoke-direct {v1, p0}, Lcom/google/android/youtube/api/jar/client/at;-><init>(Lcom/google/android/youtube/api/jar/client/ai;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    return-void
.end method

.method public final h(Z)V
    .locals 2

    iget-object v0, p0, Lcom/google/android/youtube/api/jar/client/ai;->b:Landroid/os/Handler;

    new-instance v1, Lcom/google/android/youtube/api/jar/client/ar;

    invoke-direct {v1, p0, p1}, Lcom/google/android/youtube/api/jar/client/ar;-><init>(Lcom/google/android/youtube/api/jar/client/ai;Z)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    return-void
.end method

.method public final i()V
    .locals 2

    iget-object v0, p0, Lcom/google/android/youtube/api/jar/client/ai;->b:Landroid/os/Handler;

    new-instance v1, Lcom/google/android/youtube/api/jar/client/av;

    invoke-direct {v1, p0}, Lcom/google/android/youtube/api/jar/client/av;-><init>(Lcom/google/android/youtube/api/jar/client/ai;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    return-void
.end method

.method public final i(Z)V
    .locals 2

    iget-object v0, p0, Lcom/google/android/youtube/api/jar/client/ai;->b:Landroid/os/Handler;

    new-instance v1, Lcom/google/android/youtube/api/jar/client/ay;

    invoke-direct {v1, p0, p1}, Lcom/google/android/youtube/api/jar/client/ay;-><init>(Lcom/google/android/youtube/api/jar/client/ai;Z)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    return-void
.end method

.method public final j(Z)V
    .locals 2

    iget-object v0, p0, Lcom/google/android/youtube/api/jar/client/ai;->b:Landroid/os/Handler;

    new-instance v1, Lcom/google/android/youtube/api/jar/client/az;

    invoke-direct {v1, p0, p1}, Lcom/google/android/youtube/api/jar/client/az;-><init>(Lcom/google/android/youtube/api/jar/client/ai;Z)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    return-void
.end method
