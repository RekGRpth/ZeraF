.class public Lcom/google/android/youtube/api/jar/MinimalTimeBar;
.super Landroid/view/View;
.source "SourceFile"


# instance fields
.field private final a:Lcom/google/android/youtube/api/jar/ac;

.field private b:I

.field private c:I

.field private d:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 2

    invoke-direct {p0, p1}, Landroid/view/View;-><init>(Landroid/content/Context;)V

    new-instance v0, Lcom/google/android/youtube/api/jar/ac;

    invoke-virtual {p0}, Lcom/google/android/youtube/api/jar/MinimalTimeBar;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/google/android/youtube/api/jar/ac;-><init>(Landroid/content/res/Resources;)V

    iput-object v0, p0, Lcom/google/android/youtube/api/jar/MinimalTimeBar;->a:Lcom/google/android/youtube/api/jar/ac;

    return-void
.end method


# virtual methods
.method protected onDraw(Landroid/graphics/Canvas;)V
    .locals 1

    invoke-super {p0, p1}, Landroid/view/View;->onDraw(Landroid/graphics/Canvas;)V

    iget-object v0, p0, Lcom/google/android/youtube/api/jar/MinimalTimeBar;->a:Lcom/google/android/youtube/api/jar/ac;

    invoke-virtual {v0, p1}, Lcom/google/android/youtube/api/jar/ac;->draw(Landroid/graphics/Canvas;)V

    return-void
.end method

.method protected onMeasure(II)V
    .locals 2

    const/4 v0, 0x0

    invoke-static {v0, p1}, Lcom/google/android/youtube/api/jar/MinimalTimeBar;->getDefaultSize(II)I

    move-result v0

    iget-object v1, p0, Lcom/google/android/youtube/api/jar/MinimalTimeBar;->a:Lcom/google/android/youtube/api/jar/ac;

    invoke-virtual {v1}, Lcom/google/android/youtube/api/jar/ac;->getIntrinsicHeight()I

    move-result v1

    invoke-static {v1, p2}, Lcom/google/android/youtube/api/jar/MinimalTimeBar;->resolveSize(II)I

    move-result v1

    invoke-virtual {p0, v0, v1}, Lcom/google/android/youtube/api/jar/MinimalTimeBar;->setMeasuredDimension(II)V

    return-void
.end method

.method protected onSizeChanged(IIII)V
    .locals 2

    const/4 v1, 0x0

    invoke-super {p0, p1, p2, p3, p4}, Landroid/view/View;->onSizeChanged(IIII)V

    iget-object v0, p0, Lcom/google/android/youtube/api/jar/MinimalTimeBar;->a:Lcom/google/android/youtube/api/jar/ac;

    invoke-virtual {v0, v1, v1, p1, p2}, Lcom/google/android/youtube/api/jar/ac;->setBounds(IIII)V

    return-void
.end method

.method public setScrubberTime(I)V
    .locals 4

    iget v0, p0, Lcom/google/android/youtube/api/jar/MinimalTimeBar;->b:I

    if-nez v0, :cond_0

    const-wide/16 v0, 0x0

    :goto_0
    long-to-int v0, v0

    iget-object v1, p0, Lcom/google/android/youtube/api/jar/MinimalTimeBar;->a:Lcom/google/android/youtube/api/jar/ac;

    iget v2, p0, Lcom/google/android/youtube/api/jar/MinimalTimeBar;->c:I

    mul-int/lit16 v2, v2, 0x3e8

    add-int/2addr v0, v2

    invoke-virtual {v1, v0}, Lcom/google/android/youtube/api/jar/ac;->setLevel(I)Z

    invoke-virtual {p0}, Lcom/google/android/youtube/api/jar/MinimalTimeBar;->invalidate()V

    return-void

    :cond_0
    int-to-long v0, p1

    const-wide/16 v2, 0x64

    mul-long/2addr v0, v2

    iget v2, p0, Lcom/google/android/youtube/api/jar/MinimalTimeBar;->b:I

    int-to-long v2, v2

    div-long/2addr v0, v2

    goto :goto_0
.end method

.method public setScrubbing(Z)V
    .locals 0

    iput-boolean p1, p0, Lcom/google/android/youtube/api/jar/MinimalTimeBar;->d:Z

    return-void
.end method

.method public setStyle(Lcom/google/android/youtube/core/player/overlay/ControllerOverlay$Style;)V
    .locals 2

    sget-object v0, Lcom/google/android/youtube/core/player/overlay/ControllerOverlay$Style;->AD:Lcom/google/android/youtube/core/player/overlay/ControllerOverlay$Style;

    if-ne p1, v0, :cond_0

    sget-object v0, Lcom/google/android/youtube/api/jar/af;->b:[I

    :goto_0
    iget-object v1, p0, Lcom/google/android/youtube/api/jar/MinimalTimeBar;->a:Lcom/google/android/youtube/api/jar/ac;

    invoke-virtual {v1, v0}, Lcom/google/android/youtube/api/jar/ac;->setState([I)Z

    invoke-virtual {p0}, Lcom/google/android/youtube/api/jar/MinimalTimeBar;->invalidate()V

    return-void

    :cond_0
    sget-object v0, Lcom/google/android/youtube/core/player/overlay/ControllerOverlay$Style;->LIVE:Lcom/google/android/youtube/core/player/overlay/ControllerOverlay$Style;

    if-ne p1, v0, :cond_1

    sget-object v0, Lcom/google/android/youtube/api/jar/af;->c:[I

    goto :goto_0

    :cond_1
    sget-object v0, Lcom/google/android/youtube/api/jar/af;->a:[I

    goto :goto_0
.end method

.method public setTimes(III)V
    .locals 4

    if-nez p2, :cond_1

    const-wide/16 v0, 0x0

    :goto_0
    long-to-int v0, v0

    iget-boolean v1, p0, Lcom/google/android/youtube/api/jar/MinimalTimeBar;->d:Z

    if-nez v1, :cond_0

    iget-object v1, p0, Lcom/google/android/youtube/api/jar/MinimalTimeBar;->a:Lcom/google/android/youtube/api/jar/ac;

    mul-int/lit16 v2, p3, 0x3e8

    add-int/2addr v0, v2

    invoke-virtual {v1, v0}, Lcom/google/android/youtube/api/jar/ac;->setLevel(I)Z

    :cond_0
    iput p3, p0, Lcom/google/android/youtube/api/jar/MinimalTimeBar;->c:I

    iput p2, p0, Lcom/google/android/youtube/api/jar/MinimalTimeBar;->b:I

    invoke-virtual {p0}, Lcom/google/android/youtube/api/jar/MinimalTimeBar;->invalidate()V

    return-void

    :cond_1
    int-to-long v0, p1

    const-wide/16 v2, 0x64

    mul-long/2addr v0, v2

    int-to-long v2, p2

    div-long/2addr v0, v2

    goto :goto_0
.end method
