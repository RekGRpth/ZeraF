.class final Lcom/google/android/youtube/api/jar/client/ab;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/youtube/core/player/overlay/b;


# instance fields
.field private a:Lcom/google/android/youtube/api/service/a/ae;


# direct methods
.method public constructor <init>(Lcom/google/android/youtube/api/service/a/ae;)V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const-string v0, "service cannot be null"

    invoke-static {p1, v0}, Lcom/google/android/youtube/core/utils/s;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/youtube/api/service/a/ae;

    iput-object v0, p0, Lcom/google/android/youtube/api/jar/client/ab;->a:Lcom/google/android/youtube/api/service/a/ae;

    return-void
.end method


# virtual methods
.method public final a()V
    .locals 1

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/youtube/api/jar/client/ab;->a:Lcom/google/android/youtube/api/service/a/ae;

    return-void
.end method

.method public final b()V
    .locals 2

    iget-object v0, p0, Lcom/google/android/youtube/api/jar/client/ab;->a:Lcom/google/android/youtube/api/service/a/ae;

    if-eqz v0, :cond_0

    :try_start_0
    iget-object v0, p0, Lcom/google/android/youtube/api/jar/client/ab;->a:Lcom/google/android/youtube/api/service/a/ae;

    invoke-interface {v0}, Lcom/google/android/youtube/api/service/a/ae;->b()V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    :cond_0
    return-void

    :catch_0
    move-exception v0

    new-instance v1, Ljava/lang/IllegalStateException;

    invoke-direct {v1, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/Throwable;)V

    throw v1
.end method

.method public final c()V
    .locals 2

    iget-object v0, p0, Lcom/google/android/youtube/api/jar/client/ab;->a:Lcom/google/android/youtube/api/service/a/ae;

    if-eqz v0, :cond_0

    :try_start_0
    iget-object v0, p0, Lcom/google/android/youtube/api/jar/client/ab;->a:Lcom/google/android/youtube/api/service/a/ae;

    invoke-interface {v0}, Lcom/google/android/youtube/api/service/a/ae;->c()V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    :cond_0
    return-void

    :catch_0
    move-exception v0

    new-instance v1, Ljava/lang/IllegalStateException;

    invoke-direct {v1, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/Throwable;)V

    throw v1
.end method

.method public final d()V
    .locals 2

    iget-object v0, p0, Lcom/google/android/youtube/api/jar/client/ab;->a:Lcom/google/android/youtube/api/service/a/ae;

    if-eqz v0, :cond_0

    :try_start_0
    iget-object v0, p0, Lcom/google/android/youtube/api/jar/client/ab;->a:Lcom/google/android/youtube/api/service/a/ae;

    invoke-interface {v0}, Lcom/google/android/youtube/api/service/a/ae;->d()V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    :cond_0
    return-void

    :catch_0
    move-exception v0

    new-instance v1, Ljava/lang/IllegalStateException;

    invoke-direct {v1, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/Throwable;)V

    throw v1
.end method

.method public final e()V
    .locals 2

    iget-object v0, p0, Lcom/google/android/youtube/api/jar/client/ab;->a:Lcom/google/android/youtube/api/service/a/ae;

    if-eqz v0, :cond_0

    :try_start_0
    iget-object v0, p0, Lcom/google/android/youtube/api/jar/client/ab;->a:Lcom/google/android/youtube/api/service/a/ae;

    invoke-interface {v0}, Lcom/google/android/youtube/api/service/a/ae;->e()V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    :cond_0
    return-void

    :catch_0
    move-exception v0

    new-instance v1, Ljava/lang/IllegalStateException;

    invoke-direct {v1, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/Throwable;)V

    throw v1
.end method
