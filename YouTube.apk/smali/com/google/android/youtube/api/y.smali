.class final Lcom/google/android/youtube/api/y;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/youtube/core/async/n;


# instance fields
.field final synthetic a:Lcom/google/android/youtube/api/v;


# direct methods
.method private constructor <init>(Lcom/google/android/youtube/api/v;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/youtube/api/y;->a:Lcom/google/android/youtube/api/v;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/google/android/youtube/api/v;B)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/google/android/youtube/api/y;-><init>(Lcom/google/android/youtube/api/v;)V

    return-void
.end method


# virtual methods
.method public final bridge synthetic a(Ljava/lang/Object;Ljava/lang/Exception;)V
    .locals 1

    const-string v0, "Error loading DefaultThumbnailLoader"

    invoke-static {v0, p2}, Lcom/google/android/youtube/core/L;->a(Ljava/lang/String;Ljava/lang/Throwable;)V

    iget-object v0, p0, Lcom/google/android/youtube/api/y;->a:Lcom/google/android/youtube/api/v;

    invoke-static {v0}, Lcom/google/android/youtube/api/v;->a(Lcom/google/android/youtube/api/v;)V

    return-void
.end method

.method public final synthetic a(Ljava/lang/Object;Ljava/lang/Object;)V
    .locals 4

    check-cast p2, Lcom/google/android/youtube/core/model/Video;

    if-eqz p2, :cond_0

    iget-object v0, p2, Lcom/google/android/youtube/core/model/Video;->hqThumbnailUri:Landroid/net/Uri;

    if-nez v0, :cond_1

    :cond_0
    iget-object v0, p0, Lcom/google/android/youtube/api/y;->a:Lcom/google/android/youtube/api/v;

    invoke-static {v0}, Lcom/google/android/youtube/api/v;->a(Lcom/google/android/youtube/api/v;)V

    :goto_0
    return-void

    :cond_1
    new-instance v0, Lcom/google/android/youtube/api/x;

    iget-object v1, p0, Lcom/google/android/youtube/api/y;->a:Lcom/google/android/youtube/api/v;

    iget-object v2, p2, Lcom/google/android/youtube/core/model/Video;->id:Ljava/lang/String;

    invoke-direct {v0, v1, v2}, Lcom/google/android/youtube/api/x;-><init>(Lcom/google/android/youtube/api/v;Ljava/lang/String;)V

    invoke-static {v0}, Lcom/google/android/youtube/core/async/p;->a(Lcom/google/android/youtube/core/async/n;)Lcom/google/android/youtube/core/async/p;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/youtube/api/y;->a:Lcom/google/android/youtube/api/v;

    invoke-static {v1, v0}, Lcom/google/android/youtube/api/v;->a(Lcom/google/android/youtube/api/v;Lcom/google/android/youtube/core/async/p;)Lcom/google/android/youtube/core/async/p;

    iget-object v1, p0, Lcom/google/android/youtube/api/y;->a:Lcom/google/android/youtube/api/v;

    invoke-static {v1}, Lcom/google/android/youtube/api/v;->c(Lcom/google/android/youtube/api/v;)Lcom/google/android/youtube/core/client/be;

    move-result-object v1

    iget-object v2, p2, Lcom/google/android/youtube/core/model/Video;->hqThumbnailUri:Landroid/net/Uri;

    iget-object v3, p0, Lcom/google/android/youtube/api/y;->a:Lcom/google/android/youtube/api/v;

    invoke-static {v3}, Lcom/google/android/youtube/api/v;->b(Lcom/google/android/youtube/api/v;)Landroid/os/Handler;

    move-result-object v3

    invoke-static {v3, v0}, Lcom/google/android/youtube/core/async/am;->a(Landroid/os/Handler;Lcom/google/android/youtube/core/async/n;)Lcom/google/android/youtube/core/async/am;

    move-result-object v0

    invoke-interface {v1, v2, v0}, Lcom/google/android/youtube/core/client/be;->c(Landroid/net/Uri;Lcom/google/android/youtube/core/async/n;)V

    goto :goto_0
.end method
