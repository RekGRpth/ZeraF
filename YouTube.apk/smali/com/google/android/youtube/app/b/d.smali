.class final Lcom/google/android/youtube/app/b/d;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/youtube/core/async/n;


# instance fields
.field final synthetic a:Lcom/google/android/youtube/app/b/b;

.field private final b:Lcom/google/android/youtube/core/async/GDataRequest;

.field private final c:Lcom/google/android/youtube/core/async/n;

.field private final d:Ljava/util/concurrent/atomic/AtomicInteger;

.field private final e:Ljava/util/concurrent/ConcurrentHashMap;

.field private final f:Ljava/util/List;

.field private final g:Ljava/util/Map;


# direct methods
.method public constructor <init>(Lcom/google/android/youtube/app/b/b;Lcom/google/android/youtube/core/async/GDataRequest;Lcom/google/android/youtube/core/async/n;ILjava/util/List;Ljava/util/Map;)V
    .locals 1

    iput-object p1, p0, Lcom/google/android/youtube/app/b/d;->a:Lcom/google/android/youtube/app/b/b;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p2, p0, Lcom/google/android/youtube/app/b/d;->b:Lcom/google/android/youtube/core/async/GDataRequest;

    iput-object p3, p0, Lcom/google/android/youtube/app/b/d;->c:Lcom/google/android/youtube/core/async/n;

    iput-object p5, p0, Lcom/google/android/youtube/app/b/d;->f:Ljava/util/List;

    iput-object p6, p0, Lcom/google/android/youtube/app/b/d;->g:Ljava/util/Map;

    new-instance v0, Ljava/util/concurrent/atomic/AtomicInteger;

    invoke-direct {v0, p4}, Ljava/util/concurrent/atomic/AtomicInteger;-><init>(I)V

    iput-object v0, p0, Lcom/google/android/youtube/app/b/d;->d:Ljava/util/concurrent/atomic/AtomicInteger;

    new-instance v0, Ljava/util/concurrent/ConcurrentHashMap;

    invoke-direct {v0, p4}, Ljava/util/concurrent/ConcurrentHashMap;-><init>(I)V

    iput-object v0, p0, Lcom/google/android/youtube/app/b/d;->e:Ljava/util/concurrent/ConcurrentHashMap;

    return-void
.end method

.method private a()V
    .locals 6

    iget-object v0, p0, Lcom/google/android/youtube/app/b/d;->d:Ljava/util/concurrent/atomic/AtomicInteger;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicInteger;->decrementAndGet()I

    move-result v0

    if-nez v0, :cond_3

    invoke-static {}, Lcom/google/android/youtube/core/L;->d()V

    new-instance v3, Ljava/util/ArrayList;

    iget-object v0, p0, Lcom/google/android/youtube/app/b/d;->f:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    invoke-direct {v3, v0}, Ljava/util/ArrayList;-><init>(I)V

    iget-object v0, p0, Lcom/google/android/youtube/app/b/d;->f:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :cond_0
    :goto_0
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/youtube/core/model/Video;

    iget-object v1, p0, Lcom/google/android/youtube/app/b/d;->a:Lcom/google/android/youtube/app/b/b;

    invoke-static {v1}, Lcom/google/android/youtube/app/b/b;->a(Lcom/google/android/youtube/app/b/b;)Z

    move-result v1

    if-eqz v1, :cond_1

    iget-object v1, v0, Lcom/google/android/youtube/core/model/Video;->hqThumbnailUri:Landroid/net/Uri;

    :goto_1
    iget-object v2, p0, Lcom/google/android/youtube/app/b/d;->e:Ljava/util/concurrent/ConcurrentHashMap;

    invoke-virtual {v2, v1}, Ljava/util/concurrent/ConcurrentHashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/graphics/Bitmap;

    iget-object v2, p0, Lcom/google/android/youtube/app/b/d;->g:Ljava/util/Map;

    iget-object v5, v0, Lcom/google/android/youtube/core/model/Video;->id:Ljava/lang/String;

    invoke-interface {v2, v5}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/android/youtube/core/model/aj;

    if-eqz v1, :cond_0

    if-eqz v2, :cond_0

    iget-object v5, v0, Lcom/google/android/youtube/core/model/Video;->id:Ljava/lang/String;

    invoke-static {v5}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v5

    if-nez v5, :cond_0

    new-instance v5, Lcom/google/android/youtube/app/b/a;

    iget v2, v2, Lcom/google/android/youtube/core/model/aj;->a:I

    invoke-direct {v5, v0, v1, v2}, Lcom/google/android/youtube/app/b/a;-><init>(Lcom/google/android/youtube/core/model/Video;Landroid/graphics/Bitmap;I)V

    invoke-virtual {v3, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_1
    iget-object v1, v0, Lcom/google/android/youtube/core/model/Video;->defaultThumbnailUri:Landroid/net/Uri;

    goto :goto_1

    :cond_2
    iget-object v0, p0, Lcom/google/android/youtube/app/b/d;->c:Lcom/google/android/youtube/core/async/n;

    iget-object v1, p0, Lcom/google/android/youtube/app/b/d;->b:Lcom/google/android/youtube/core/async/GDataRequest;

    invoke-interface {v0, v1, v3}, Lcom/google/android/youtube/core/async/n;->a(Ljava/lang/Object;Ljava/lang/Object;)V

    :cond_3
    return-void
.end method


# virtual methods
.method public final synthetic a(Ljava/lang/Object;Ljava/lang/Exception;)V
    .locals 1

    const-string v0, "widget thumbnail error"

    invoke-static {v0, p2}, Lcom/google/android/youtube/core/L;->b(Ljava/lang/String;Ljava/lang/Throwable;)V

    invoke-direct {p0}, Lcom/google/android/youtube/app/b/d;->a()V

    return-void
.end method

.method public final synthetic a(Ljava/lang/Object;Ljava/lang/Object;)V
    .locals 4

    check-cast p1, Landroid/net/Uri;

    check-cast p2, Landroid/graphics/Bitmap;

    invoke-static {}, Lcom/google/android/youtube/core/L;->e()V

    invoke-virtual {p2}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v0

    int-to-float v1, v0

    const/high16 v2, 0x3f400000

    mul-float/2addr v1, v2

    float-to-int v1, v1

    const/4 v2, 0x0

    sub-int/2addr v0, v1

    div-int/lit8 v0, v0, 0x2

    invoke-virtual {p2}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v3

    invoke-static {p2, v2, v0, v3, v1}, Landroid/graphics/Bitmap;->createBitmap(Landroid/graphics/Bitmap;IIII)Landroid/graphics/Bitmap;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/youtube/app/b/d;->e:Ljava/util/concurrent/ConcurrentHashMap;

    invoke-virtual {v1, p1, v0}, Ljava/util/concurrent/ConcurrentHashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    invoke-direct {p0}, Lcom/google/android/youtube/app/b/d;->a()V

    return-void
.end method
