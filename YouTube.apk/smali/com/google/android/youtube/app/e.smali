.class public final Lcom/google/android/youtube/app/e;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/youtube/core/async/au;


# static fields
.field private static final a:Lcom/google/android/youtube/core/model/aj;

.field private static final b:Lcom/google/android/youtube/core/model/aj;

.field private static final c:Lcom/google/android/youtube/core/model/aj;

.field private static final d:Lcom/google/android/youtube/core/model/aj;

.field private static final e:Lcom/google/android/youtube/core/model/aj;


# instance fields
.field private final f:Lcom/google/android/youtube/core/async/GDataRequestFactory;

.field private final g:Lcom/google/android/youtube/core/async/au;

.field private final h:Lcom/google/android/youtube/core/async/au;

.field private final i:Lcom/google/android/youtube/core/async/au;

.field private final j:Ljava/lang/String;

.field private final k:Ljava/util/concurrent/ConcurrentHashMap;

.field private final l:Ljava/util/regex/Pattern;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    new-instance v0, Lcom/google/android/youtube/core/model/aj;

    const v1, 0x7f0b0123

    invoke-direct {v0, v1}, Lcom/google/android/youtube/core/model/aj;-><init>(I)V

    sput-object v0, Lcom/google/android/youtube/app/e;->a:Lcom/google/android/youtube/core/model/aj;

    new-instance v0, Lcom/google/android/youtube/core/model/aj;

    const v1, 0x7f0b0126

    invoke-direct {v0, v1}, Lcom/google/android/youtube/core/model/aj;-><init>(I)V

    sput-object v0, Lcom/google/android/youtube/app/e;->b:Lcom/google/android/youtube/core/model/aj;

    new-instance v0, Lcom/google/android/youtube/core/model/aj;

    const v1, 0x7f0b0127

    invoke-direct {v0, v1}, Lcom/google/android/youtube/core/model/aj;-><init>(I)V

    sput-object v0, Lcom/google/android/youtube/app/e;->c:Lcom/google/android/youtube/core/model/aj;

    new-instance v0, Lcom/google/android/youtube/core/model/aj;

    const v1, 0x7f0b0124

    invoke-direct {v0, v1}, Lcom/google/android/youtube/core/model/aj;-><init>(I)V

    sput-object v0, Lcom/google/android/youtube/app/e;->d:Lcom/google/android/youtube/core/model/aj;

    new-instance v0, Lcom/google/android/youtube/core/model/aj;

    const v1, 0x7f0b0125

    invoke-direct {v0, v1}, Lcom/google/android/youtube/core/model/aj;-><init>(I)V

    sput-object v0, Lcom/google/android/youtube/app/e;->e:Lcom/google/android/youtube/core/model/aj;

    return-void
.end method

.method public constructor <init>(Lcom/google/android/youtube/core/client/bc;Ljava/lang/String;)V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    invoke-static {p1}, Lcom/google/android/youtube/core/utils/s;->a(Ljava/lang/Object;)Ljava/lang/Object;

    invoke-interface {p1}, Lcom/google/android/youtube/core/client/bc;->a()Lcom/google/android/youtube/core/async/GDataRequestFactory;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/youtube/core/utils/s;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/youtube/core/async/GDataRequestFactory;

    iput-object v0, p0, Lcom/google/android/youtube/app/e;->f:Lcom/google/android/youtube/core/async/GDataRequestFactory;

    invoke-interface {p1}, Lcom/google/android/youtube/core/client/bc;->k()Lcom/google/android/youtube/core/async/au;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/youtube/core/utils/s;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/youtube/core/async/au;

    iput-object v0, p0, Lcom/google/android/youtube/app/e;->g:Lcom/google/android/youtube/core/async/au;

    invoke-interface {p1}, Lcom/google/android/youtube/core/client/bc;->t()Lcom/google/android/youtube/core/async/au;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/youtube/core/utils/s;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/youtube/core/async/au;

    iput-object v0, p0, Lcom/google/android/youtube/app/e;->h:Lcom/google/android/youtube/core/async/au;

    invoke-interface {p1}, Lcom/google/android/youtube/core/client/bc;->u()Lcom/google/android/youtube/core/async/au;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/youtube/core/utils/s;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/youtube/core/async/au;

    iput-object v0, p0, Lcom/google/android/youtube/app/e;->i:Lcom/google/android/youtube/core/async/au;

    iput-object p2, p0, Lcom/google/android/youtube/app/e;->j:Ljava/lang/String;

    new-instance v0, Ljava/util/concurrent/ConcurrentHashMap;

    invoke-direct {v0}, Ljava/util/concurrent/ConcurrentHashMap;-><init>()V

    iput-object v0, p0, Lcom/google/android/youtube/app/e;->k:Ljava/util/concurrent/ConcurrentHashMap;

    const-string v0, "users/([^/]*)"

    invoke-static {v0}, Ljava/util/regex/Pattern;->compile(Ljava/lang/String;)Ljava/util/regex/Pattern;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/youtube/app/e;->l:Ljava/util/regex/Pattern;

    return-void
.end method

.method static synthetic a(Lcom/google/android/youtube/app/e;Lcom/google/android/youtube/core/async/GDataRequest;)Ljava/lang/String;
    .locals 2

    iget-object v0, p0, Lcom/google/android/youtube/app/e;->l:Ljava/util/regex/Pattern;

    iget-object v1, p1, Lcom/google/android/youtube/core/async/GDataRequest;->c:Landroid/net/Uri;

    invoke-virtual {v1}, Landroid/net/Uri;->getPath()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/regex/Pattern;->matcher(Ljava/lang/CharSequence;)Ljava/util/regex/Matcher;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/regex/Matcher;->find()Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Ljava/util/regex/Matcher;->group(I)Ljava/lang/String;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method static synthetic a(Lcom/google/android/youtube/app/e;)Ljava/util/concurrent/ConcurrentHashMap;
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/app/e;->k:Ljava/util/concurrent/ConcurrentHashMap;

    return-object v0
.end method

.method static synthetic a(Lcom/google/android/youtube/app/e;Lcom/google/android/youtube/core/async/GDataRequest;Lcom/google/android/youtube/core/async/n;Lcom/google/android/youtube/core/model/UserAuth;)V
    .locals 0

    invoke-direct {p0, p1, p2, p3}, Lcom/google/android/youtube/app/e;->a(Lcom/google/android/youtube/core/async/GDataRequest;Lcom/google/android/youtube/core/async/n;Lcom/google/android/youtube/core/model/UserAuth;)V

    return-void
.end method

.method private a(Lcom/google/android/youtube/core/async/GDataRequest;Lcom/google/android/youtube/core/async/n;Lcom/google/android/youtube/core/model/UserAuth;)V
    .locals 9

    const/4 v5, 0x0

    new-instance v4, Ljava/util/HashMap;

    invoke-direct {v4}, Ljava/util/HashMap;-><init>()V

    iget-object v0, p0, Lcom/google/android/youtube/app/e;->f:Lcom/google/android/youtube/core/async/GDataRequestFactory;

    invoke-virtual {v0, p3}, Lcom/google/android/youtube/core/async/GDataRequestFactory;->l(Lcom/google/android/youtube/core/model/UserAuth;)Lcom/google/android/youtube/core/async/GDataRequest;

    move-result-object v6

    sget-object v0, Lcom/google/android/youtube/app/e;->a:Lcom/google/android/youtube/core/model/aj;

    invoke-virtual {v4, v6, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v0, p0, Lcom/google/android/youtube/app/e;->f:Lcom/google/android/youtube/core/async/GDataRequestFactory;

    sget-object v1, Lcom/google/android/youtube/core/async/GDataRequestFactory$StandardFeed;->MOST_POPULAR:Lcom/google/android/youtube/core/async/GDataRequestFactory$StandardFeed;

    iget-object v2, p0, Lcom/google/android/youtube/app/e;->j:Ljava/lang/String;

    sget-object v3, Lcom/google/android/youtube/core/async/GDataRequestFactory$TimeFilter;->THIS_WEEK:Lcom/google/android/youtube/core/async/GDataRequestFactory$TimeFilter;

    invoke-virtual {v0, v1, v5, v2, v3}, Lcom/google/android/youtube/core/async/GDataRequestFactory;->a(Lcom/google/android/youtube/core/async/GDataRequestFactory$StandardFeed;Ljava/lang/String;Ljava/lang/String;Lcom/google/android/youtube/core/async/GDataRequestFactory$TimeFilter;)Lcom/google/android/youtube/core/async/GDataRequest;

    move-result-object v7

    sget-object v0, Lcom/google/android/youtube/app/e;->b:Lcom/google/android/youtube/core/model/aj;

    invoke-virtual {v4, v7, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v0, p0, Lcom/google/android/youtube/app/e;->f:Lcom/google/android/youtube/core/async/GDataRequestFactory;

    sget-object v1, Lcom/google/android/youtube/core/async/GDataRequestFactory$StandardFeed;->MOST_DISCUSSED:Lcom/google/android/youtube/core/async/GDataRequestFactory$StandardFeed;

    iget-object v2, p0, Lcom/google/android/youtube/app/e;->j:Ljava/lang/String;

    sget-object v3, Lcom/google/android/youtube/core/async/GDataRequestFactory$TimeFilter;->THIS_WEEK:Lcom/google/android/youtube/core/async/GDataRequestFactory$TimeFilter;

    invoke-virtual {v0, v1, v5, v2, v3}, Lcom/google/android/youtube/core/async/GDataRequestFactory;->a(Lcom/google/android/youtube/core/async/GDataRequestFactory$StandardFeed;Ljava/lang/String;Ljava/lang/String;Lcom/google/android/youtube/core/async/GDataRequestFactory$TimeFilter;)Lcom/google/android/youtube/core/async/GDataRequest;

    move-result-object v8

    sget-object v0, Lcom/google/android/youtube/app/e;->c:Lcom/google/android/youtube/core/model/aj;

    invoke-virtual {v4, v8, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    new-instance v0, Lcom/google/android/youtube/app/f;

    const v5, 0x7fffffff

    move-object v1, p0

    move-object v2, p2

    move-object v3, p1

    invoke-direct/range {v0 .. v5}, Lcom/google/android/youtube/app/f;-><init>(Lcom/google/android/youtube/app/e;Lcom/google/android/youtube/core/async/n;Lcom/google/android/youtube/core/async/GDataRequest;Ljava/util/HashMap;I)V

    iget-object v1, p0, Lcom/google/android/youtube/app/e;->i:Lcom/google/android/youtube/core/async/au;

    invoke-interface {v1, v6, v0}, Lcom/google/android/youtube/core/async/au;->a(Ljava/lang/Object;Lcom/google/android/youtube/core/async/n;)V

    iget-object v1, p0, Lcom/google/android/youtube/app/e;->g:Lcom/google/android/youtube/core/async/au;

    invoke-interface {v1, v7, v0}, Lcom/google/android/youtube/core/async/au;->a(Ljava/lang/Object;Lcom/google/android/youtube/core/async/n;)V

    iget-object v1, p0, Lcom/google/android/youtube/app/e;->g:Lcom/google/android/youtube/core/async/au;

    invoke-interface {v1, v8, v0}, Lcom/google/android/youtube/core/async/au;->a(Ljava/lang/Object;Lcom/google/android/youtube/core/async/n;)V

    return-void
.end method

.method static synthetic b()Lcom/google/android/youtube/core/model/aj;
    .locals 1

    sget-object v0, Lcom/google/android/youtube/app/e;->d:Lcom/google/android/youtube/core/model/aj;

    return-object v0
.end method


# virtual methods
.method public final a()Ljava/util/concurrent/ConcurrentHashMap;
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/app/e;->k:Ljava/util/concurrent/ConcurrentHashMap;

    return-object v0
.end method

.method public final synthetic a(Ljava/lang/Object;Lcom/google/android/youtube/core/async/n;)V
    .locals 9

    const/4 v8, 0x0

    move-object v3, p1

    check-cast v3, Lcom/google/android/youtube/core/async/GDataRequest;

    iget-object v0, v3, Lcom/google/android/youtube/core/async/GDataRequest;->d:Lcom/google/android/youtube/core/model/UserAuth;

    if-nez v0, :cond_0

    new-instance v4, Ljava/util/HashMap;

    invoke-direct {v4}, Ljava/util/HashMap;-><init>()V

    iget-object v0, p0, Lcom/google/android/youtube/app/e;->f:Lcom/google/android/youtube/core/async/GDataRequestFactory;

    sget-object v1, Lcom/google/android/youtube/core/async/GDataRequestFactory$StandardFeed;->RECENTLY_FEATURED:Lcom/google/android/youtube/core/async/GDataRequestFactory$StandardFeed;

    iget-object v2, p0, Lcom/google/android/youtube/app/e;->j:Ljava/lang/String;

    invoke-virtual {v0, v1, v8, v2, v8}, Lcom/google/android/youtube/core/async/GDataRequestFactory;->a(Lcom/google/android/youtube/core/async/GDataRequestFactory$StandardFeed;Ljava/lang/String;Ljava/lang/String;Lcom/google/android/youtube/core/async/GDataRequestFactory$TimeFilter;)Lcom/google/android/youtube/core/async/GDataRequest;

    move-result-object v6

    sget-object v0, Lcom/google/android/youtube/app/e;->e:Lcom/google/android/youtube/core/model/aj;

    invoke-virtual {v4, v6, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v0, p0, Lcom/google/android/youtube/app/e;->f:Lcom/google/android/youtube/core/async/GDataRequestFactory;

    sget-object v1, Lcom/google/android/youtube/core/async/GDataRequestFactory$StandardFeed;->MOST_POPULAR:Lcom/google/android/youtube/core/async/GDataRequestFactory$StandardFeed;

    iget-object v2, p0, Lcom/google/android/youtube/app/e;->j:Ljava/lang/String;

    sget-object v5, Lcom/google/android/youtube/core/async/GDataRequestFactory$TimeFilter;->THIS_WEEK:Lcom/google/android/youtube/core/async/GDataRequestFactory$TimeFilter;

    invoke-virtual {v0, v1, v8, v2, v5}, Lcom/google/android/youtube/core/async/GDataRequestFactory;->a(Lcom/google/android/youtube/core/async/GDataRequestFactory$StandardFeed;Ljava/lang/String;Ljava/lang/String;Lcom/google/android/youtube/core/async/GDataRequestFactory$TimeFilter;)Lcom/google/android/youtube/core/async/GDataRequest;

    move-result-object v7

    sget-object v0, Lcom/google/android/youtube/app/e;->b:Lcom/google/android/youtube/core/model/aj;

    invoke-virtual {v4, v7, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v0, p0, Lcom/google/android/youtube/app/e;->f:Lcom/google/android/youtube/core/async/GDataRequestFactory;

    sget-object v1, Lcom/google/android/youtube/core/async/GDataRequestFactory$StandardFeed;->MOST_DISCUSSED:Lcom/google/android/youtube/core/async/GDataRequestFactory$StandardFeed;

    iget-object v2, p0, Lcom/google/android/youtube/app/e;->j:Ljava/lang/String;

    sget-object v5, Lcom/google/android/youtube/core/async/GDataRequestFactory$TimeFilter;->THIS_WEEK:Lcom/google/android/youtube/core/async/GDataRequestFactory$TimeFilter;

    invoke-virtual {v0, v1, v8, v2, v5}, Lcom/google/android/youtube/core/async/GDataRequestFactory;->a(Lcom/google/android/youtube/core/async/GDataRequestFactory$StandardFeed;Ljava/lang/String;Ljava/lang/String;Lcom/google/android/youtube/core/async/GDataRequestFactory$TimeFilter;)Lcom/google/android/youtube/core/async/GDataRequest;

    move-result-object v8

    sget-object v0, Lcom/google/android/youtube/app/e;->c:Lcom/google/android/youtube/core/model/aj;

    invoke-virtual {v4, v8, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    new-instance v0, Lcom/google/android/youtube/app/f;

    const/4 v5, 0x1

    move-object v1, p0

    move-object v2, p2

    invoke-direct/range {v0 .. v5}, Lcom/google/android/youtube/app/f;-><init>(Lcom/google/android/youtube/app/e;Lcom/google/android/youtube/core/async/n;Lcom/google/android/youtube/core/async/GDataRequest;Ljava/util/HashMap;I)V

    iget-object v1, p0, Lcom/google/android/youtube/app/e;->g:Lcom/google/android/youtube/core/async/au;

    invoke-interface {v1, v6, v0}, Lcom/google/android/youtube/core/async/au;->a(Ljava/lang/Object;Lcom/google/android/youtube/core/async/n;)V

    iget-object v1, p0, Lcom/google/android/youtube/app/e;->g:Lcom/google/android/youtube/core/async/au;

    invoke-interface {v1, v7, v0}, Lcom/google/android/youtube/core/async/au;->a(Ljava/lang/Object;Lcom/google/android/youtube/core/async/n;)V

    iget-object v1, p0, Lcom/google/android/youtube/app/e;->g:Lcom/google/android/youtube/core/async/au;

    invoke-interface {v1, v8, v0}, Lcom/google/android/youtube/core/async/au;->a(Ljava/lang/Object;Lcom/google/android/youtube/core/async/n;)V

    :goto_0
    return-void

    :cond_0
    iget-object v0, v3, Lcom/google/android/youtube/core/async/GDataRequest;->c:Landroid/net/Uri;

    invoke-virtual {v0}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v1, "/myfeed/users/"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/google/android/youtube/app/e;->h:Lcom/google/android/youtube/core/async/au;

    new-instance v1, Lcom/google/android/youtube/app/g;

    invoke-direct {v1, p0, p2}, Lcom/google/android/youtube/app/g;-><init>(Lcom/google/android/youtube/app/e;Lcom/google/android/youtube/core/async/n;)V

    invoke-interface {v0, v3, v1}, Lcom/google/android/youtube/core/async/au;->a(Ljava/lang/Object;Lcom/google/android/youtube/core/async/n;)V

    goto :goto_0

    :cond_1
    iget-object v0, v3, Lcom/google/android/youtube/core/async/GDataRequest;->d:Lcom/google/android/youtube/core/model/UserAuth;

    invoke-direct {p0, v3, p2, v0}, Lcom/google/android/youtube/app/e;->a(Lcom/google/android/youtube/core/async/GDataRequest;Lcom/google/android/youtube/core/async/n;Lcom/google/android/youtube/core/model/UserAuth;)V

    goto :goto_0
.end method
