.class final Lcom/google/android/youtube/app/remote/bl;
.super Landroid/os/Handler;
.source "SourceFile"


# instance fields
.field final synthetic a:Lcom/google/android/youtube/app/remote/bf;


# direct methods
.method private constructor <init>(Lcom/google/android/youtube/app/remote/bf;Landroid/os/Looper;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/youtube/app/remote/bl;->a:Lcom/google/android/youtube/app/remote/bf;

    invoke-direct {p0, p2}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    return-void
.end method

.method synthetic constructor <init>(Lcom/google/android/youtube/app/remote/bf;Landroid/os/Looper;B)V
    .locals 0

    invoke-direct {p0, p1, p2}, Lcom/google/android/youtube/app/remote/bl;-><init>(Lcom/google/android/youtube/app/remote/bf;Landroid/os/Looper;)V

    return-void
.end method

.method private a(Landroid/content/Context;Z)V
    .locals 2

    iget-object v0, p0, Lcom/google/android/youtube/app/remote/bl;->a:Lcom/google/android/youtube/app/remote/bf;

    invoke-static {v0}, Lcom/google/android/youtube/app/remote/bf;->b(Lcom/google/android/youtube/app/remote/bf;)Lcom/google/android/ytremote/backend/browserchannel/k;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/ytremote/backend/browserchannel/k;->a()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/youtube/app/remote/bl;->a:Lcom/google/android/youtube/app/remote/bf;

    invoke-static {v0}, Lcom/google/android/youtube/app/remote/bf;->b(Lcom/google/android/youtube/app/remote/bf;)Lcom/google/android/ytremote/backend/browserchannel/k;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/ytremote/backend/browserchannel/k;->b()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    iget-object v0, p0, Lcom/google/android/youtube/app/remote/bl;->a:Lcom/google/android/youtube/app/remote/bf;

    invoke-static {v0}, Lcom/google/android/youtube/app/remote/bf;->b(Lcom/google/android/youtube/app/remote/bf;)Lcom/google/android/ytremote/backend/browserchannel/k;

    move-result-object v0

    invoke-virtual {v0, p2}, Lcom/google/android/ytremote/backend/browserchannel/k;->a(Z)V

    :cond_1
    iget-object v0, p0, Lcom/google/android/youtube/app/remote/bl;->a:Lcom/google/android/youtube/app/remote/bf;

    invoke-static {v0}, Lcom/google/android/youtube/app/remote/bf;->k(Lcom/google/android/youtube/app/remote/bf;)Z

    move-result v0

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/google/android/youtube/app/remote/bl;->a:Lcom/google/android/youtube/app/remote/bf;

    invoke-static {v0}, Lcom/google/android/youtube/app/remote/bf;->l(Lcom/google/android/youtube/app/remote/bf;)Lcom/google/android/youtube/app/remote/bh;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/content/Context;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    iget-object v0, p0, Lcom/google/android/youtube/app/remote/bl;->a:Lcom/google/android/youtube/app/remote/bf;

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lcom/google/android/youtube/app/remote/bf;->b(Lcom/google/android/youtube/app/remote/bf;Z)Z

    :cond_2
    return-void
.end method

.method static synthetic a(Lcom/google/android/youtube/app/remote/bl;Lcom/google/android/ytremote/model/CloudScreen;Lcom/google/android/youtube/app/remote/bi;)V
    .locals 0

    invoke-direct {p0, p1, p2}, Lcom/google/android/youtube/app/remote/bl;->a(Lcom/google/android/ytremote/model/CloudScreen;Lcom/google/android/youtube/app/remote/bi;)V

    return-void
.end method

.method static synthetic a(Lcom/google/android/youtube/app/remote/bl;Lcom/google/android/ytremote/model/d;Lcom/google/android/ytremote/model/CloudScreen;)V
    .locals 3

    iget-object v0, p0, Lcom/google/android/youtube/app/remote/bl;->a:Lcom/google/android/youtube/app/remote/bf;

    invoke-static {v0}, Lcom/google/android/youtube/app/remote/bf;->o(Lcom/google/android/youtube/app/remote/bf;)Ljava/util/Map;

    move-result-object v0

    invoke-interface {v0, p1, p2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v0, p0, Lcom/google/android/youtube/app/remote/bl;->a:Lcom/google/android/youtube/app/remote/bf;

    invoke-static {v0}, Lcom/google/android/youtube/app/remote/bf;->p(Lcom/google/android/youtube/app/remote/bf;)Landroid/content/SharedPreferences;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/youtube/app/compat/ac;->a(Landroid/content/SharedPreferences;)Lcom/google/android/youtube/app/compat/ad;

    move-result-object v0

    invoke-virtual {p1}, Lcom/google/android/ytremote/model/d;->e()Lcom/google/android/ytremote/model/SsdpId;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/ytremote/model/SsdpId;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p2}, Lcom/google/android/ytremote/model/CloudScreen;->getScreenId()Lcom/google/android/ytremote/model/ScreenId;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/android/ytremote/model/ScreenId;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/google/android/youtube/app/compat/ad;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/youtube/app/compat/ad;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/youtube/app/compat/ad;->a()V

    return-void
.end method

.method private a(Lcom/google/android/ytremote/model/CloudScreen;Lcom/google/android/youtube/app/remote/bi;)V
    .locals 6

    iget-object v0, p0, Lcom/google/android/youtube/app/remote/bl;->a:Lcom/google/android/youtube/app/remote/bf;

    invoke-static {v0}, Lcom/google/android/youtube/app/remote/bf;->m(Lcom/google/android/youtube/app/remote/bf;)Lcom/google/android/ytremote/model/CloudScreen;

    move-result-object v0

    invoke-virtual {p1, v0}, Lcom/google/android/ytremote/model/CloudScreen;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/youtube/app/remote/bl;->a:Lcom/google/android/youtube/app/remote/bf;

    invoke-static {v0}, Lcom/google/android/youtube/app/remote/bf;->n(Lcom/google/android/youtube/app/remote/bf;)Ljava/util/Map;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Map;->clear()V

    :cond_0
    iget-object v0, p0, Lcom/google/android/youtube/app/remote/bl;->a:Lcom/google/android/youtube/app/remote/bf;

    invoke-static {v0, p1}, Lcom/google/android/youtube/app/remote/bf;->a(Lcom/google/android/youtube/app/remote/bf;Lcom/google/android/ytremote/model/CloudScreen;)Lcom/google/android/ytremote/model/CloudScreen;

    iget-object v0, p0, Lcom/google/android/youtube/app/remote/bl;->a:Lcom/google/android/youtube/app/remote/bf;

    invoke-static {v0}, Lcom/google/android/youtube/app/remote/bf;->k(Lcom/google/android/youtube/app/remote/bf;)Z

    move-result v0

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/google/android/youtube/app/remote/bl;->a:Lcom/google/android/youtube/app/remote/bf;

    invoke-static {v0}, Lcom/google/android/youtube/app/remote/bf;->a(Lcom/google/android/youtube/app/remote/bf;)Landroid/content/Context;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/youtube/app/remote/bl;->a:Lcom/google/android/youtube/app/remote/bf;

    invoke-static {v1}, Lcom/google/android/youtube/app/remote/bf;->l(Lcom/google/android/youtube/app/remote/bf;)Lcom/google/android/youtube/app/remote/bh;

    move-result-object v1

    invoke-static {}, Lcom/google/android/youtube/app/remote/bf;->y()Landroid/content/IntentFilter;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    :cond_1
    new-instance v0, Lcom/google/android/ytremote/backend/model/b;

    invoke-direct {v0}, Lcom/google/android/ytremote/backend/model/b;-><init>()V

    invoke-virtual {p1}, Lcom/google/android/ytremote/model/CloudScreen;->getLoungeToken()Lcom/google/android/ytremote/model/LoungeToken;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/ytremote/backend/model/b;->a(Lcom/google/android/ytremote/model/LoungeToken;)Lcom/google/android/ytremote/backend/model/b;

    move-result-object v0

    invoke-static {p2}, Lcom/google/android/youtube/app/remote/bi;->b(Lcom/google/android/youtube/app/remote/bi;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_2

    sget-object v1, Lcom/google/android/ytremote/backend/model/Method;->SET_PLAYLIST:Lcom/google/android/ytremote/backend/model/Method;

    invoke-virtual {v0, v1}, Lcom/google/android/ytremote/backend/model/b;->a(Lcom/google/android/ytremote/backend/model/Method;)Lcom/google/android/ytremote/backend/model/b;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/youtube/app/remote/bl;->a:Lcom/google/android/youtube/app/remote/bf;

    invoke-static {p2}, Lcom/google/android/youtube/app/remote/bi;->b(Lcom/google/android/youtube/app/remote/bi;)Ljava/lang/String;

    move-result-object v3

    invoke-static {p2}, Lcom/google/android/youtube/app/remote/bi;->c(Lcom/google/android/youtube/app/remote/bi;)J

    move-result-wide v4

    invoke-static {v2, v3, v4, v5}, Lcom/google/android/youtube/app/remote/bf;->a(Lcom/google/android/youtube/app/remote/bf;Ljava/lang/String;J)Lcom/google/android/ytremote/backend/model/Params;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/google/android/ytremote/backend/model/b;->a(Lcom/google/android/ytremote/backend/model/Params;)Lcom/google/android/ytremote/backend/model/b;

    :cond_2
    invoke-static {p2}, Lcom/google/android/youtube/app/remote/bi;->d(Lcom/google/android/youtube/app/remote/bi;)Z

    move-result v1

    if-eqz v1, :cond_3

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/google/android/ytremote/backend/model/b;->a(Z)V

    :cond_3
    invoke-virtual {v0}, Lcom/google/android/ytremote/backend/model/b;->a()Lcom/google/android/ytremote/backend/model/a;

    move-result-object v1

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v2, "Connecting to "

    invoke-direct {v0, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p1}, Lcom/google/android/ytremote/model/CloudScreen;->getScreenId()Lcom/google/android/ytremote/model/ScreenId;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, " with "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1}, Lcom/google/android/ytremote/backend/model/a;->f()Z

    move-result v2

    if-eqz v2, :cond_5

    iget-object v2, p0, Lcom/google/android/youtube/app/remote/bl;->a:Lcom/google/android/youtube/app/remote/bf;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v1}, Lcom/google/android/ytremote/backend/model/a;->b()Lcom/google/android/ytremote/backend/model/Method;

    move-result-object v3

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v3, " : "

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v1}, Lcom/google/android/ytremote/backend/model/a;->g()Z

    move-result v0

    if-eqz v0, :cond_4

    invoke-virtual {v1}, Lcom/google/android/ytremote/backend/model/a;->c()Lcom/google/android/ytremote/backend/model/Params;

    move-result-object v0

    :goto_0
    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v2, v0}, Lcom/google/android/youtube/app/remote/bf;->a(Lcom/google/android/youtube/app/remote/bf;Ljava/lang/String;)V

    :goto_1
    iget-object v0, p0, Lcom/google/android/youtube/app/remote/bl;->a:Lcom/google/android/youtube/app/remote/bf;

    invoke-static {v0}, Lcom/google/android/youtube/app/remote/bf;->b(Lcom/google/android/youtube/app/remote/bf;)Lcom/google/android/ytremote/backend/browserchannel/k;

    move-result-object v0

    invoke-virtual {v0, v1}, Lcom/google/android/ytremote/backend/browserchannel/k;->a(Lcom/google/android/ytremote/backend/model/a;)Ljava/util/concurrent/CountDownLatch;

    return-void

    :cond_4
    const-string v0, "{}"

    goto :goto_0

    :cond_5
    iget-object v2, p0, Lcom/google/android/youtube/app/remote/bl;->a:Lcom/google/android/youtube/app/remote/bf;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v3, "no message."

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v2, v0}, Lcom/google/android/youtube/app/remote/bf;->a(Lcom/google/android/youtube/app/remote/bf;Ljava/lang/String;)V

    goto :goto_1
.end method


# virtual methods
.method public final handleMessage(Landroid/os/Message;)V
    .locals 9

    const/4 v3, 0x0

    const/4 v5, 0x0

    const/4 v4, 0x1

    iget v0, p1, Landroid/os/Message;->what:I

    packed-switch v0, :pswitch_data_0

    :cond_0
    :goto_0
    return-void

    :pswitch_0
    iget-object v0, p0, Lcom/google/android/youtube/app/remote/bl;->a:Lcom/google/android/youtube/app/remote/bf;

    invoke-static {v0}, Lcom/google/android/youtube/app/remote/bf;->a(Lcom/google/android/youtube/app/remote/bf;)Landroid/content/Context;

    move-result-object v0

    invoke-direct {p0, v0, v5}, Lcom/google/android/youtube/app/remote/bl;->a(Landroid/content/Context;Z)V

    iget-object v0, p0, Lcom/google/android/youtube/app/remote/bl;->a:Lcom/google/android/youtube/app/remote/bf;

    invoke-virtual {v0}, Lcom/google/android/youtube/app/remote/bf;->v()Lcom/google/android/youtube/app/remote/RemoteControl$State;

    move-result-object v0

    sget-object v1, Lcom/google/android/youtube/app/remote/RemoteControl$State;->OFFLINE:Lcom/google/android/youtube/app/remote/RemoteControl$State;

    if-eq v0, v1, :cond_0

    iget-object v0, p0, Lcom/google/android/youtube/app/remote/bl;->a:Lcom/google/android/youtube/app/remote/bf;

    sget-object v1, Lcom/google/android/youtube/app/remote/RemoteControl$State;->SLEEP:Lcom/google/android/youtube/app/remote/RemoteControl$State;

    invoke-virtual {v0, v1}, Lcom/google/android/youtube/app/remote/bf;->a(Lcom/google/android/youtube/app/remote/RemoteControl$State;)V

    goto :goto_0

    :pswitch_1
    iget-object v0, p0, Lcom/google/android/youtube/app/remote/bl;->a:Lcom/google/android/youtube/app/remote/bf;

    invoke-static {v0}, Lcom/google/android/youtube/app/remote/bf;->b(Lcom/google/android/youtube/app/remote/bf;)Lcom/google/android/ytremote/backend/browserchannel/k;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/ytremote/backend/browserchannel/k;->a()Z

    move-result v0

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/google/android/youtube/app/remote/bl;->a:Lcom/google/android/youtube/app/remote/bf;

    invoke-static {v0}, Lcom/google/android/youtube/app/remote/bf;->b(Lcom/google/android/youtube/app/remote/bf;)Lcom/google/android/ytremote/backend/browserchannel/k;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/ytremote/backend/browserchannel/k;->b()Z

    move-result v0

    if-eqz v0, :cond_2

    :cond_1
    iget-object v0, p0, Lcom/google/android/youtube/app/remote/bl;->a:Lcom/google/android/youtube/app/remote/bf;

    const-string v1, "Connecting to a new screen. Will disconnect the previous browser channel connection."

    invoke-static {v0, v1}, Lcom/google/android/youtube/app/remote/bf;->a(Lcom/google/android/youtube/app/remote/bf;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/google/android/youtube/app/remote/bl;->a:Lcom/google/android/youtube/app/remote/bf;

    invoke-static {v0}, Lcom/google/android/youtube/app/remote/bf;->b(Lcom/google/android/youtube/app/remote/bf;)Lcom/google/android/ytremote/backend/browserchannel/k;

    move-result-object v0

    invoke-virtual {v0, v4}, Lcom/google/android/ytremote/backend/browserchannel/k;->a(Z)V

    :cond_2
    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v0, Lcom/google/android/youtube/app/remote/bi;

    invoke-static {v0}, Lcom/google/android/youtube/app/remote/bi;->a(Lcom/google/android/youtube/app/remote/bi;)Lcom/google/android/youtube/app/remote/bp;

    move-result-object v2

    iget-object v6, p0, Lcom/google/android/youtube/app/remote/bl;->a:Lcom/google/android/youtube/app/remote/bf;

    new-instance v7, Ljava/lang/StringBuilder;

    const-string v1, "Connecting to "

    invoke-direct {v7, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v1, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v1, Lcom/google/android/youtube/app/remote/bi;

    invoke-static {v1}, Lcom/google/android/youtube/app/remote/bi;->a(Lcom/google/android/youtube/app/remote/bi;)Lcom/google/android/youtube/app/remote/bp;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/youtube/app/remote/bp;->a()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v7, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v6, v1}, Lcom/google/android/youtube/app/remote/bf;->a(Lcom/google/android/youtube/app/remote/bf;Ljava/lang/String;)V

    invoke-virtual {v2}, Lcom/google/android/youtube/app/remote/bp;->d()Z

    move-result v1

    if-eqz v1, :cond_9

    invoke-virtual {v2}, Lcom/google/android/youtube/app/remote/bp;->b()Lcom/google/android/ytremote/model/d;

    move-result-object v6

    iget-object v1, p0, Lcom/google/android/youtube/app/remote/bl;->a:Lcom/google/android/youtube/app/remote/bf;

    invoke-static {v1}, Lcom/google/android/youtube/app/remote/bf;->o(Lcom/google/android/youtube/app/remote/bf;)Ljava/util/Map;

    move-result-object v1

    invoke-interface {v1, v6}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_4

    iget-object v1, p0, Lcom/google/android/youtube/app/remote/bl;->a:Lcom/google/android/youtube/app/remote/bf;

    invoke-static {v1}, Lcom/google/android/youtube/app/remote/bf;->o(Lcom/google/android/youtube/app/remote/bf;)Ljava/util/Map;

    move-result-object v1

    invoke-interface {v1, v6}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/ytremote/model/CloudScreen;

    move-object v3, v1

    :cond_3
    :goto_1
    if-eqz v3, :cond_8

    iget-object v1, p0, Lcom/google/android/youtube/app/remote/bl;->a:Lcom/google/android/youtube/app/remote/bf;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v7, "Found associated cloud screen "

    invoke-direct {v2, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v7, " for device "

    invoke-virtual {v2, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/google/android/youtube/app/remote/bf;->a(Lcom/google/android/youtube/app/remote/bf;Ljava/lang/String;)V

    iget-object v1, p0, Lcom/google/android/youtube/app/remote/bl;->a:Lcom/google/android/youtube/app/remote/bf;

    invoke-static {v1}, Lcom/google/android/youtube/app/remote/bf;->j(Lcom/google/android/youtube/app/remote/bf;)Lcom/google/android/ytremote/logic/a;

    move-result-object v1

    invoke-virtual {v6}, Lcom/google/android/ytremote/model/d;->b()Landroid/net/Uri;

    move-result-object v2

    invoke-interface {v1, v2}, Lcom/google/android/ytremote/logic/a;->a(Landroid/net/Uri;)Lcom/google/android/ytremote/model/a;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/youtube/app/remote/bl;->a:Lcom/google/android/youtube/app/remote/bf;

    new-instance v7, Ljava/lang/StringBuilder;

    const-string v8, "Application status for device is "

    invoke-direct {v7, v8}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1}, Lcom/google/android/ytremote/model/a;->a()I

    move-result v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v2, v7}, Lcom/google/android/youtube/app/remote/bf;->a(Lcom/google/android/youtube/app/remote/bf;Ljava/lang/String;)V

    invoke-virtual {v1}, Lcom/google/android/ytremote/model/a;->a()I

    move-result v1

    if-ne v1, v4, :cond_8

    iget-object v1, p0, Lcom/google/android/youtube/app/remote/bl;->a:Lcom/google/android/youtube/app/remote/bf;

    invoke-static {v1}, Lcom/google/android/youtube/app/remote/bf;->r(Lcom/google/android/youtube/app/remote/bf;)Lcom/google/android/ytremote/backend/logic/b;

    move-result-object v1

    new-array v2, v4, [Lcom/google/android/ytremote/model/CloudScreen;

    aput-object v3, v2, v5

    invoke-static {v2}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v2

    invoke-interface {v1, v2}, Lcom/google/android/ytremote/backend/logic/b;->a(Ljava/util/Collection;)Ljava/util/Map;

    move-result-object v1

    if-eqz v1, :cond_7

    invoke-interface {v1, v3}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_7

    invoke-interface {v1, v3}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    if-eqz v1, :cond_7

    move v1, v4

    :goto_2
    if-eqz v1, :cond_8

    iget-object v1, p0, Lcom/google/android/youtube/app/remote/bl;->a:Lcom/google/android/youtube/app/remote/bf;

    const-string v2, "Screen appears to be online. Will not send a launch request."

    invoke-static {v1, v2}, Lcom/google/android/youtube/app/remote/bf;->a(Lcom/google/android/youtube/app/remote/bf;Ljava/lang/String;)V

    invoke-direct {p0, v3, v0}, Lcom/google/android/youtube/app/remote/bl;->a(Lcom/google/android/ytremote/model/CloudScreen;Lcom/google/android/youtube/app/remote/bi;)V

    goto/16 :goto_0

    :cond_4
    invoke-virtual {v6}, Lcom/google/android/ytremote/model/d;->f()Z

    move-result v1

    if-eqz v1, :cond_5

    invoke-virtual {v6}, Lcom/google/android/ytremote/model/d;->a()Lcom/google/android/ytremote/model/a;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/ytremote/model/a;->b()Lcom/google/android/ytremote/model/ScreenId;

    move-result-object v1

    :goto_3
    if-nez v1, :cond_e

    invoke-virtual {v6}, Lcom/google/android/ytremote/model/d;->e()Lcom/google/android/ytremote/model/SsdpId;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/ytremote/model/SsdpId;->toString()Ljava/lang/String;

    move-result-object v2

    iget-object v1, p0, Lcom/google/android/youtube/app/remote/bl;->a:Lcom/google/android/youtube/app/remote/bf;

    invoke-static {v1}, Lcom/google/android/youtube/app/remote/bf;->p(Lcom/google/android/youtube/app/remote/bf;)Landroid/content/SharedPreferences;

    move-result-object v1

    invoke-interface {v1, v2}, Landroid/content/SharedPreferences;->contains(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_3

    new-instance v1, Lcom/google/android/ytremote/model/ScreenId;

    iget-object v7, p0, Lcom/google/android/youtube/app/remote/bl;->a:Lcom/google/android/youtube/app/remote/bf;

    invoke-static {v7}, Lcom/google/android/youtube/app/remote/bf;->p(Lcom/google/android/youtube/app/remote/bf;)Landroid/content/SharedPreferences;

    move-result-object v7

    const-string v8, ""

    invoke-interface {v7, v2, v8}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Lcom/google/android/ytremote/model/ScreenId;-><init>(Ljava/lang/String;)V

    move-object v2, v1

    :goto_4
    iget-object v1, p0, Lcom/google/android/youtube/app/remote/bl;->a:Lcom/google/android/youtube/app/remote/bf;

    invoke-static {v1}, Lcom/google/android/youtube/app/remote/bf;->q(Lcom/google/android/youtube/app/remote/bf;)Lcom/google/android/ytremote/backend/a/a;

    move-result-object v1

    new-array v7, v4, [Lcom/google/android/ytremote/model/ScreenId;

    aput-object v2, v7, v5

    invoke-static {v7}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v7

    invoke-virtual {v1, v7}, Lcom/google/android/ytremote/backend/a/a;->a(Ljava/util/Collection;)Ljava/util/Map;

    move-result-object v1

    invoke-interface {v1, v2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/ytremote/model/LoungeToken;

    if-nez v1, :cond_6

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v7, "Unable to retrieve lounge token for screenId "

    invoke-direct {v1, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/google/android/youtube/core/L;->b(Ljava/lang/String;)V

    goto/16 :goto_1

    :cond_5
    move-object v1, v3

    goto :goto_3

    :cond_6
    new-instance v3, Lcom/google/android/ytremote/model/CloudScreen;

    invoke-virtual {v6}, Lcom/google/android/ytremote/model/d;->c()Ljava/lang/String;

    move-result-object v7

    invoke-direct {v3, v2, v7, v1}, Lcom/google/android/ytremote/model/CloudScreen;-><init>(Lcom/google/android/ytremote/model/ScreenId;Ljava/lang/String;Lcom/google/android/ytremote/model/LoungeToken;)V

    iget-object v1, p0, Lcom/google/android/youtube/app/remote/bl;->a:Lcom/google/android/youtube/app/remote/bf;

    invoke-static {v1}, Lcom/google/android/youtube/app/remote/bf;->o(Lcom/google/android/youtube/app/remote/bf;)Ljava/util/Map;

    move-result-object v1

    invoke-interface {v1, v6, v3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto/16 :goto_1

    :cond_7
    move v1, v5

    goto/16 :goto_2

    :cond_8
    iget-object v1, p0, Lcom/google/android/youtube/app/remote/bl;->a:Lcom/google/android/youtube/app/remote/bf;

    invoke-static {v1, v4}, Lcom/google/android/youtube/app/remote/bf;->c(Lcom/google/android/youtube/app/remote/bf;Z)Z

    iget-object v1, p0, Lcom/google/android/youtube/app/remote/bl;->a:Lcom/google/android/youtube/app/remote/bf;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v4, "Sending launch request for "

    invoke-direct {v2, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v4, " to "

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v6}, Lcom/google/android/ytremote/model/d;->b()Landroid/net/Uri;

    move-result-object v4

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/google/android/youtube/app/remote/bf;->a(Lcom/google/android/youtube/app/remote/bf;Ljava/lang/String;)V

    iget-object v1, p0, Lcom/google/android/youtube/app/remote/bl;->a:Lcom/google/android/youtube/app/remote/bf;

    invoke-static {v1}, Lcom/google/android/youtube/app/remote/bf;->i(Lcom/google/android/youtube/app/remote/bf;)Lcom/google/android/ytremote/logic/d;

    move-result-object v1

    invoke-virtual {v6}, Lcom/google/android/ytremote/model/d;->b()Landroid/net/Uri;

    move-result-object v2

    new-instance v4, Lcom/google/android/youtube/app/remote/bm;

    invoke-direct {v4, p0, v6, v0}, Lcom/google/android/youtube/app/remote/bm;-><init>(Lcom/google/android/youtube/app/remote/bl;Lcom/google/android/ytremote/model/d;Lcom/google/android/youtube/app/remote/bi;)V

    invoke-interface {v1, v2, v4}, Lcom/google/android/ytremote/logic/d;->a(Landroid/net/Uri;Lcom/google/android/ytremote/logic/e;)V

    if-eqz v3, :cond_0

    invoke-direct {p0, v3, v0}, Lcom/google/android/youtube/app/remote/bl;->a(Lcom/google/android/ytremote/model/CloudScreen;Lcom/google/android/youtube/app/remote/bi;)V

    goto/16 :goto_0

    :cond_9
    invoke-virtual {v2}, Lcom/google/android/youtube/app/remote/bp;->c()Lcom/google/android/ytremote/model/CloudScreen;

    move-result-object v1

    invoke-direct {p0, v1, v0}, Lcom/google/android/youtube/app/remote/bl;->a(Lcom/google/android/ytremote/model/CloudScreen;Lcom/google/android/youtube/app/remote/bi;)V

    goto/16 :goto_0

    :pswitch_2
    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v0, Lcom/google/android/youtube/app/remote/bj;

    iget-object v1, v0, Lcom/google/android/youtube/app/remote/bj;->a:Lcom/google/android/youtube/app/remote/bp;

    iget-object v2, p0, Lcom/google/android/youtube/app/remote/bl;->a:Lcom/google/android/youtube/app/remote/bf;

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "Disconnecting from "

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1}, Lcom/google/android/youtube/app/remote/bp;->a()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v2, v5}, Lcom/google/android/youtube/app/remote/bf;->a(Lcom/google/android/youtube/app/remote/bf;Ljava/lang/String;)V

    iget-boolean v2, v0, Lcom/google/android/youtube/app/remote/bj;->b:Z

    if-eqz v2, :cond_a

    if-eqz v1, :cond_a

    invoke-virtual {v1}, Lcom/google/android/youtube/app/remote/bp;->d()Z

    move-result v0

    if-eqz v0, :cond_a

    iget-object v0, p0, Lcom/google/android/youtube/app/remote/bl;->a:Lcom/google/android/youtube/app/remote/bf;

    invoke-static {v0}, Lcom/google/android/youtube/app/remote/bf;->h(Lcom/google/android/youtube/app/remote/bf;)Z

    move-result v0

    if-eqz v0, :cond_a

    iget-object v0, p0, Lcom/google/android/youtube/app/remote/bl;->a:Lcom/google/android/youtube/app/remote/bf;

    invoke-static {v0}, Lcom/google/android/youtube/app/remote/bf;->c(Lcom/google/android/youtube/app/remote/bf;)Ljava/util/Map;

    move-result-object v0

    invoke-virtual {v1}, Lcom/google/android/youtube/app/remote/bp;->b()Lcom/google/android/ytremote/model/d;

    move-result-object v5

    invoke-virtual {v5}, Lcom/google/android/ytremote/model/d;->e()Lcom/google/android/ytremote/model/SsdpId;

    move-result-object v5

    invoke-interface {v0, v5}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_c

    iget-object v0, p0, Lcom/google/android/youtube/app/remote/bl;->a:Lcom/google/android/youtube/app/remote/bf;

    invoke-static {v0}, Lcom/google/android/youtube/app/remote/bf;->c(Lcom/google/android/youtube/app/remote/bf;)Ljava/util/Map;

    move-result-object v0

    invoke-virtual {v1}, Lcom/google/android/youtube/app/remote/bp;->b()Lcom/google/android/ytremote/model/d;

    move-result-object v3

    invoke-virtual {v3}, Lcom/google/android/ytremote/model/d;->e()Lcom/google/android/ytremote/model/SsdpId;

    move-result-object v3

    invoke-interface {v0, v3}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/net/Uri;

    :goto_5
    iget-object v3, p0, Lcom/google/android/youtube/app/remote/bl;->a:Lcom/google/android/youtube/app/remote/bf;

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "Sending stop request to "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/google/android/youtube/app/remote/bf;->a(Lcom/google/android/youtube/app/remote/bf;Ljava/lang/String;)V

    if-eqz v0, :cond_a

    iget-object v3, p0, Lcom/google/android/youtube/app/remote/bl;->a:Lcom/google/android/youtube/app/remote/bf;

    invoke-static {v3}, Lcom/google/android/youtube/app/remote/bf;->i(Lcom/google/android/youtube/app/remote/bf;)Lcom/google/android/ytremote/logic/d;

    move-result-object v3

    invoke-interface {v3, v0}, Lcom/google/android/ytremote/logic/d;->a(Landroid/net/Uri;)V

    :cond_a
    invoke-virtual {v1}, Lcom/google/android/youtube/app/remote/bp;->d()Z

    move-result v0

    if-eqz v0, :cond_b

    iget-object v0, p0, Lcom/google/android/youtube/app/remote/bl;->a:Lcom/google/android/youtube/app/remote/bf;

    invoke-static {v0}, Lcom/google/android/youtube/app/remote/bf;->c(Lcom/google/android/youtube/app/remote/bf;)Ljava/util/Map;

    move-result-object v0

    invoke-virtual {v1}, Lcom/google/android/youtube/app/remote/bp;->b()Lcom/google/android/ytremote/model/d;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/ytremote/model/d;->e()Lcom/google/android/ytremote/model/SsdpId;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    :cond_b
    iget-object v0, p0, Lcom/google/android/youtube/app/remote/bl;->a:Lcom/google/android/youtube/app/remote/bf;

    invoke-static {v0}, Lcom/google/android/youtube/app/remote/bf;->a(Lcom/google/android/youtube/app/remote/bf;)Landroid/content/Context;

    move-result-object v0

    invoke-direct {p0, v0, v2}, Lcom/google/android/youtube/app/remote/bl;->a(Landroid/content/Context;Z)V

    goto/16 :goto_0

    :cond_c
    invoke-virtual {v1}, Lcom/google/android/youtube/app/remote/bp;->b()Lcom/google/android/ytremote/model/d;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/ytremote/model/d;->b()Landroid/net/Uri;

    move-result-object v0

    iget-object v5, p0, Lcom/google/android/youtube/app/remote/bl;->a:Lcom/google/android/youtube/app/remote/bf;

    invoke-static {v5}, Lcom/google/android/youtube/app/remote/bf;->j(Lcom/google/android/youtube/app/remote/bf;)Lcom/google/android/ytremote/logic/a;

    move-result-object v5

    invoke-interface {v5, v0}, Lcom/google/android/ytremote/logic/a;->a(Landroid/net/Uri;)Lcom/google/android/ytremote/model/a;

    move-result-object v5

    if-eqz v5, :cond_d

    invoke-virtual {v5}, Lcom/google/android/ytremote/model/a;->a()I

    move-result v6

    if-ne v6, v4, :cond_d

    invoke-virtual {v5}, Lcom/google/android/ytremote/model/a;->c()Ljava/lang/String;

    move-result-object v4

    if-eqz v4, :cond_d

    invoke-virtual {v0}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v0

    invoke-virtual {v0, v4}, Landroid/net/Uri$Builder;->appendPath(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v0

    goto :goto_5

    :cond_d
    move-object v0, v3

    goto :goto_5

    :pswitch_3
    iget-object v0, p0, Lcom/google/android/youtube/app/remote/bl;->a:Lcom/google/android/youtube/app/remote/bf;

    invoke-virtual {v0}, Lcom/google/android/youtube/app/remote/bf;->v()Lcom/google/android/youtube/app/remote/RemoteControl$State;

    move-result-object v0

    sget-object v1, Lcom/google/android/youtube/app/remote/RemoteControl$State;->CONNECTED:Lcom/google/android/youtube/app/remote/RemoteControl$State;

    if-ne v0, v1, :cond_0

    iget-object v0, p0, Lcom/google/android/youtube/app/remote/bl;->a:Lcom/google/android/youtube/app/remote/bf;

    invoke-static {v0}, Lcom/google/android/youtube/app/remote/bf;->d(Lcom/google/android/youtube/app/remote/bf;)Lcom/google/android/youtube/app/remote/RemoteControl$RemotePlayerState;

    move-result-object v0

    sget-object v1, Lcom/google/android/youtube/app/remote/RemoteControl$RemotePlayerState;->UNCONFIRMED:Lcom/google/android/youtube/app/remote/RemoteControl$RemotePlayerState;

    if-ne v0, v1, :cond_0

    iget-object v0, p0, Lcom/google/android/youtube/app/remote/bl;->a:Lcom/google/android/youtube/app/remote/bf;

    invoke-static {v0}, Lcom/google/android/youtube/app/remote/bf;->e(Lcom/google/android/youtube/app/remote/bf;)V

    goto/16 :goto_0

    :pswitch_4
    iget-object v0, p0, Lcom/google/android/youtube/app/remote/bl;->a:Lcom/google/android/youtube/app/remote/bf;

    invoke-static {v0, v5}, Lcom/google/android/youtube/app/remote/bf;->a(Lcom/google/android/youtube/app/remote/bf;Z)Z

    goto/16 :goto_0

    :pswitch_5
    iget-object v1, p0, Lcom/google/android/youtube/app/remote/bl;->a:Lcom/google/android/youtube/app/remote/bf;

    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-static {v1, v0}, Lcom/google/android/youtube/app/remote/bf;->a(Lcom/google/android/youtube/app/remote/bf;I)I

    iget-object v0, p0, Lcom/google/android/youtube/app/remote/bl;->a:Lcom/google/android/youtube/app/remote/bf;

    iget-object v1, p0, Lcom/google/android/youtube/app/remote/bl;->a:Lcom/google/android/youtube/app/remote/bf;

    invoke-static {v1}, Lcom/google/android/youtube/app/remote/bf;->f(Lcom/google/android/youtube/app/remote/bf;)I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/google/android/youtube/app/remote/bf;->c(I)V

    iget-object v0, p0, Lcom/google/android/youtube/app/remote/bl;->a:Lcom/google/android/youtube/app/remote/bf;

    invoke-static {v0}, Lcom/google/android/youtube/app/remote/bf;->g(Lcom/google/android/youtube/app/remote/bf;)Ljava/util/Map;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/youtube/app/remote/bl;->a:Lcom/google/android/youtube/app/remote/bf;

    invoke-static {v1}, Lcom/google/android/youtube/app/remote/bf;->f(Lcom/google/android/youtube/app/remote/bf;)I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    goto/16 :goto_0

    :cond_e
    move-object v2, v1

    goto/16 :goto_4

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_3
        :pswitch_1
        :pswitch_2
        :pswitch_4
        :pswitch_5
    .end packed-switch
.end method
