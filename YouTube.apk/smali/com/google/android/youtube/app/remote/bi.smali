.class final Lcom/google/android/youtube/app/remote/bi;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field private final a:Lcom/google/android/youtube/app/remote/bp;

.field private final b:Ljava/lang/String;

.field private final c:J

.field private final d:Z


# direct methods
.method private constructor <init>(Lcom/google/android/youtube/app/remote/bp;Ljava/lang/String;JZ)V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const-string v0, "screen can not be null"

    invoke-static {p1, v0}, Lcom/google/android/youtube/core/utils/s;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/youtube/app/remote/bp;

    iput-object v0, p0, Lcom/google/android/youtube/app/remote/bi;->a:Lcom/google/android/youtube/app/remote/bp;

    iput-object p2, p0, Lcom/google/android/youtube/app/remote/bi;->b:Ljava/lang/String;

    iput-wide p3, p0, Lcom/google/android/youtube/app/remote/bi;->c:J

    iput-boolean p5, p0, Lcom/google/android/youtube/app/remote/bi;->d:Z

    return-void
.end method

.method synthetic constructor <init>(Lcom/google/android/youtube/app/remote/bp;Ljava/lang/String;JZB)V
    .locals 6

    const/4 v5, 0x1

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-wide v3, p3

    invoke-direct/range {v0 .. v5}, Lcom/google/android/youtube/app/remote/bi;-><init>(Lcom/google/android/youtube/app/remote/bp;Ljava/lang/String;JZ)V

    return-void
.end method

.method static synthetic a(Lcom/google/android/youtube/app/remote/bi;)Lcom/google/android/youtube/app/remote/bp;
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/app/remote/bi;->a:Lcom/google/android/youtube/app/remote/bp;

    return-object v0
.end method

.method static synthetic b(Lcom/google/android/youtube/app/remote/bi;)Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/app/remote/bi;->b:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic c(Lcom/google/android/youtube/app/remote/bi;)J
    .locals 2

    iget-wide v0, p0, Lcom/google/android/youtube/app/remote/bi;->c:J

    return-wide v0
.end method

.method static synthetic d(Lcom/google/android/youtube/app/remote/bi;)Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/youtube/app/remote/bi;->d:Z

    return v0
.end method
