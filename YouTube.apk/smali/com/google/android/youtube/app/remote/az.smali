.class final Lcom/google/android/youtube/app/remote/az;
.super Landroid/os/Handler;
.source "SourceFile"


# instance fields
.field private final a:Lcom/google/android/youtube/app/remote/ad;

.field private final b:Lcom/google/android/youtube/app/remote/av;

.field private final c:Lcom/google/android/youtube/app/remote/aq;


# direct methods
.method public constructor <init>(Lcom/google/android/youtube/app/remote/ad;Lcom/google/android/youtube/app/remote/av;Lcom/google/android/youtube/app/remote/aq;)V
    .locals 0

    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    iput-object p1, p0, Lcom/google/android/youtube/app/remote/az;->a:Lcom/google/android/youtube/app/remote/ad;

    iput-object p2, p0, Lcom/google/android/youtube/app/remote/az;->b:Lcom/google/android/youtube/app/remote/av;

    iput-object p3, p0, Lcom/google/android/youtube/app/remote/az;->c:Lcom/google/android/youtube/app/remote/aq;

    return-void
.end method


# virtual methods
.method public final handleMessage(Landroid/os/Message;)V
    .locals 3

    iget v0, p1, Landroid/os/Message;->what:I

    packed-switch v0, :pswitch_data_0

    :cond_0
    :goto_0
    return-void

    :pswitch_0
    iget-object v0, p0, Lcom/google/android/youtube/app/remote/az;->a:Lcom/google/android/youtube/app/remote/ad;

    invoke-virtual {v0}, Lcom/google/android/youtube/app/remote/ad;->b()Lcom/google/android/youtube/app/remote/RemoteControl;

    move-result-object v1

    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v0, Landroid/util/Pair;

    if-eqz v1, :cond_1

    invoke-interface {v1}, Lcom/google/android/youtube/app/remote/RemoteControl;->f()Lcom/google/android/youtube/app/remote/RemoteControl$RemotePlayerState;

    move-result-object v1

    sget-object v2, Lcom/google/android/youtube/app/remote/RemoteControl$RemotePlayerState;->ENDED:Lcom/google/android/youtube/app/remote/RemoteControl$RemotePlayerState;

    if-ne v1, v2, :cond_0

    :cond_1
    iget-object v1, p0, Lcom/google/android/youtube/app/remote/az;->b:Lcom/google/android/youtube/app/remote/av;

    invoke-virtual {v1}, Lcom/google/android/youtube/app/remote/av;->a()V

    iget-object v1, p0, Lcom/google/android/youtube/app/remote/az;->c:Lcom/google/android/youtube/app/remote/aq;

    iget-object v1, v0, Landroid/util/Pair;->first:Ljava/lang/Object;

    if-eqz v1, :cond_2

    iget-object v1, v0, Landroid/util/Pair;->first:Ljava/lang/Object;

    check-cast v1, Lcom/google/android/youtube/core/async/o;

    invoke-virtual {v1}, Lcom/google/android/youtube/core/async/o;->c()Z

    move-result v1

    if-nez v1, :cond_2

    iget-object v1, v0, Landroid/util/Pair;->first:Ljava/lang/Object;

    check-cast v1, Lcom/google/android/youtube/core/async/o;

    invoke-virtual {v1}, Lcom/google/android/youtube/core/async/o;->a()V

    :cond_2
    iget-object v1, v0, Landroid/util/Pair;->second:Ljava/lang/Object;

    if-eqz v1, :cond_0

    iget-object v1, v0, Landroid/util/Pair;->second:Ljava/lang/Object;

    check-cast v1, Lcom/google/android/youtube/core/async/p;

    invoke-virtual {v1}, Lcom/google/android/youtube/core/async/p;->b()Z

    move-result v1

    if-nez v1, :cond_0

    iget-object v0, v0, Landroid/util/Pair;->second:Ljava/lang/Object;

    check-cast v0, Lcom/google/android/youtube/core/async/p;

    invoke-virtual {v0}, Lcom/google/android/youtube/core/async/p;->a()V

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
    .end packed-switch
.end method
