.class final Lcom/google/android/youtube/app/remote/aw;
.super Landroid/content/BroadcastReceiver;
.source "SourceFile"


# instance fields
.field final synthetic a:Lcom/google/android/youtube/app/remote/as;


# direct methods
.method private constructor <init>(Lcom/google/android/youtube/app/remote/as;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/youtube/app/remote/aw;->a:Lcom/google/android/youtube/app/remote/as;

    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/google/android/youtube/app/remote/as;B)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/google/android/youtube/app/remote/aw;-><init>(Lcom/google/android/youtube/app/remote/as;)V

    return-void
.end method


# virtual methods
.method public final onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 2

    iget-object v0, p0, Lcom/google/android/youtube/app/remote/aw;->a:Lcom/google/android/youtube/app/remote/as;

    invoke-static {v0}, Lcom/google/android/youtube/app/remote/as;->a(Lcom/google/android/youtube/app/remote/as;)Lcom/google/android/youtube/app/remote/RemoteControl;

    move-result-object v0

    if-nez v0, :cond_1

    :cond_0
    :goto_0
    :pswitch_0
    return-void

    :cond_1
    const-string v0, "com.google.android.youtube.action.remote_next"

    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/google/android/youtube/app/remote/aw;->a:Lcom/google/android/youtube/app/remote/as;

    invoke-static {v0}, Lcom/google/android/youtube/app/remote/as;->a(Lcom/google/android/youtube/app/remote/as;)Lcom/google/android/youtube/app/remote/RemoteControl;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/youtube/app/remote/RemoteControl;->p()V

    goto :goto_0

    :cond_2
    const-string v0, "com.google.android.youtube.action.remote_prev"

    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/google/android/youtube/app/remote/aw;->a:Lcom/google/android/youtube/app/remote/as;

    invoke-static {v0}, Lcom/google/android/youtube/app/remote/as;->a(Lcom/google/android/youtube/app/remote/as;)Lcom/google/android/youtube/app/remote/RemoteControl;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/youtube/app/remote/RemoteControl;->r()V

    goto :goto_0

    :cond_3
    const-string v0, "com.google.android.youtube.action.remote_playpause"

    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/youtube/app/remote/aw;->a:Lcom/google/android/youtube/app/remote/as;

    invoke-static {v0}, Lcom/google/android/youtube/app/remote/as;->a(Lcom/google/android/youtube/app/remote/as;)Lcom/google/android/youtube/app/remote/RemoteControl;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/youtube/app/remote/RemoteControl;->f()Lcom/google/android/youtube/app/remote/RemoteControl$RemotePlayerState;

    move-result-object v0

    sget-object v1, Lcom/google/android/youtube/app/remote/at;->a:[I

    invoke-virtual {v0}, Lcom/google/android/youtube/app/remote/RemoteControl$RemotePlayerState;->ordinal()I

    move-result v0

    aget v0, v1, v0

    packed-switch v0, :pswitch_data_0

    iget-object v0, p0, Lcom/google/android/youtube/app/remote/aw;->a:Lcom/google/android/youtube/app/remote/as;

    invoke-static {v0}, Lcom/google/android/youtube/app/remote/as;->a(Lcom/google/android/youtube/app/remote/as;)Lcom/google/android/youtube/app/remote/RemoteControl;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/youtube/app/remote/RemoteControl;->c()V

    goto :goto_0

    :pswitch_1
    iget-object v0, p0, Lcom/google/android/youtube/app/remote/aw;->a:Lcom/google/android/youtube/app/remote/as;

    invoke-static {v0}, Lcom/google/android/youtube/app/remote/as;->a(Lcom/google/android/youtube/app/remote/as;)Lcom/google/android/youtube/app/remote/RemoteControl;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/youtube/app/remote/RemoteControl;->d()V

    goto :goto_0

    :pswitch_2
    iget-object v0, p0, Lcom/google/android/youtube/app/remote/aw;->a:Lcom/google/android/youtube/app/remote/as;

    invoke-static {v0}, Lcom/google/android/youtube/app/remote/as;->a(Lcom/google/android/youtube/app/remote/as;)Lcom/google/android/youtube/app/remote/RemoteControl;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/youtube/app/remote/RemoteControl;->c()V

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_2
        :pswitch_0
    .end packed-switch
.end method
