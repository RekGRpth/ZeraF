.class public final Lcom/google/android/youtube/app/adapter/p;
.super Lcom/google/android/youtube/app/adapter/j;
.source "SourceFile"


# instance fields
.field private final a:Landroid/content/Context;

.field private final b:Lcom/google/android/youtube/core/client/be;

.field private final c:Lcom/google/android/youtube/core/client/bc;

.field private final d:Landroid/os/Handler;


# direct methods
.method constructor <init>(Landroid/content/Context;Lcom/google/android/youtube/core/client/bc;Lcom/google/android/youtube/core/client/be;)V
    .locals 2

    invoke-direct {p0}, Lcom/google/android/youtube/app/adapter/j;-><init>()V

    const-string v0, "context can\'t be null"

    invoke-static {p1, v0}, Lcom/google/android/youtube/core/utils/s;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/Context;

    iput-object v0, p0, Lcom/google/android/youtube/app/adapter/p;->a:Landroid/content/Context;

    const-string v0, "gdataClient can\'t be null"

    invoke-static {p2, v0}, Lcom/google/android/youtube/core/utils/s;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/youtube/core/client/bc;

    iput-object v0, p0, Lcom/google/android/youtube/app/adapter/p;->c:Lcom/google/android/youtube/core/client/bc;

    const-string v0, "imageClient can\'t be null"

    invoke-static {p3, v0}, Lcom/google/android/youtube/core/utils/s;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/youtube/core/client/be;

    iput-object v0, p0, Lcom/google/android/youtube/app/adapter/p;->b:Lcom/google/android/youtube/core/client/be;

    new-instance v0, Landroid/os/Handler;

    invoke-virtual {p1}, Landroid/content/Context;->getMainLooper()Landroid/os/Looper;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    iput-object v0, p0, Lcom/google/android/youtube/app/adapter/p;->d:Landroid/os/Handler;

    return-void
.end method

.method static synthetic a(Lcom/google/android/youtube/app/adapter/p;)Landroid/content/Context;
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/app/adapter/p;->a:Landroid/content/Context;

    return-object v0
.end method

.method static synthetic b(Lcom/google/android/youtube/app/adapter/p;)Lcom/google/android/youtube/core/client/be;
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/app/adapter/p;->b:Lcom/google/android/youtube/core/client/be;

    return-object v0
.end method

.method static synthetic c(Lcom/google/android/youtube/app/adapter/p;)Landroid/os/Handler;
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/app/adapter/p;->d:Landroid/os/Handler;

    return-object v0
.end method

.method static synthetic d(Lcom/google/android/youtube/app/adapter/p;)Lcom/google/android/youtube/core/client/bc;
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/app/adapter/p;->c:Lcom/google/android/youtube/core/client/bc;

    return-object v0
.end method


# virtual methods
.method public final a(Landroid/view/View;Landroid/view/ViewGroup;)Lcom/google/android/youtube/app/adapter/bl;
    .locals 1

    new-instance v0, Lcom/google/android/youtube/app/adapter/q;

    invoke-direct {v0, p0, p1, p2}, Lcom/google/android/youtube/app/adapter/q;-><init>(Lcom/google/android/youtube/app/adapter/p;Landroid/view/View;Landroid/view/ViewGroup;)V

    return-object v0
.end method
