.class public final Lcom/google/android/youtube/app/adapter/aq;
.super Lcom/google/android/youtube/app/adapter/be;
.source "SourceFile"


# instance fields
.field private b:I


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/google/android/youtube/app/ui/ToolbarHelper;IZ)V
    .locals 2

    const/4 v0, 0x0

    const v1, 0x7f040068

    invoke-direct {p0, p1, v0, v1}, Lcom/google/android/youtube/app/adapter/be;-><init>(Landroid/content/Context;Lcom/google/android/youtube/app/ui/ToolbarHelper;I)V

    const/4 v0, 0x3

    iput v0, p0, Lcom/google/android/youtube/app/adapter/aq;->b:I

    return-void
.end method


# virtual methods
.method public final getCount()I
    .locals 2

    invoke-super {p0}, Lcom/google/android/youtube/app/adapter/be;->getCount()I

    move-result v0

    iget v1, p0, Lcom/google/android/youtube/app/adapter/aq;->b:I

    add-int/2addr v0, v1

    return v0
.end method

.method public final synthetic getItem(I)Ljava/lang/Object;
    .locals 1

    iget v0, p0, Lcom/google/android/youtube/app/adapter/aq;->b:I

    if-lt p1, v0, :cond_0

    invoke-virtual {p0}, Lcom/google/android/youtube/app/adapter/aq;->getCount()I

    move-result v0

    if-lt p1, v0, :cond_1

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_1
    iget v0, p0, Lcom/google/android/youtube/app/adapter/aq;->b:I

    sub-int v0, p1, v0

    invoke-super {p0, v0}, Lcom/google/android/youtube/app/adapter/be;->getItem(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/youtube/core/model/Playlist;

    goto :goto_0
.end method

.method public final getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 5

    iget v0, p0, Lcom/google/android/youtube/app/adapter/aq;->b:I

    if-lt p1, v0, :cond_0

    iget v0, p0, Lcom/google/android/youtube/app/adapter/aq;->b:I

    sub-int v0, p1, v0

    invoke-super {p0, v0}, Lcom/google/android/youtube/app/adapter/be;->getItem(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/youtube/core/model/Playlist;

    invoke-virtual {p0, v0, p2, p3}, Lcom/google/android/youtube/app/adapter/aq;->a(Lcom/google/android/youtube/core/model/Playlist;Landroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    invoke-virtual {p0, p2, p3}, Lcom/google/android/youtube/app/adapter/aq;->a(Landroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v1

    invoke-virtual {v1}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/youtube/app/adapter/bf;

    if-nez p1, :cond_2

    iget-object v2, v0, Lcom/google/android/youtube/app/adapter/bf;->a:Landroid/widget/TextView;

    iget-object v3, p0, Lcom/google/android/youtube/app/adapter/aq;->a:Landroid/content/res/Resources;

    const v4, 0x7f0b0191

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v2, v0, Lcom/google/android/youtube/app/adapter/bf;->e:Landroid/widget/ImageView;

    const v3, 0x7f02012d

    invoke-virtual {v2, v3}, Landroid/widget/ImageView;->setImageResource(I)V

    :cond_1
    :goto_1
    iget-object v2, v0, Lcom/google/android/youtube/app/adapter/bf;->d:Landroid/widget/ImageView;

    const/16 v3, 0x8

    invoke-virtual {v2, v3}, Landroid/widget/ImageView;->setVisibility(I)V

    iget-object v0, v0, Lcom/google/android/youtube/app/adapter/bf;->e:Landroid/widget/ImageView;

    const/4 v2, 0x0

    invoke-virtual {v0, v2}, Landroid/widget/ImageView;->setVisibility(I)V

    move-object v0, v1

    goto :goto_0

    :cond_2
    const/4 v2, 0x1

    if-ne p1, v2, :cond_3

    iget-object v2, v0, Lcom/google/android/youtube/app/adapter/bf;->a:Landroid/widget/TextView;

    iget-object v3, p0, Lcom/google/android/youtube/app/adapter/aq;->a:Landroid/content/res/Resources;

    const v4, 0x7f0b0190

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v2, v0, Lcom/google/android/youtube/app/adapter/bf;->e:Landroid/widget/ImageView;

    const v3, 0x7f02012c

    invoke-virtual {v2, v3}, Landroid/widget/ImageView;->setImageResource(I)V

    goto :goto_1

    :cond_3
    const/4 v2, 0x2

    if-ne p1, v2, :cond_1

    iget-object v2, v0, Lcom/google/android/youtube/app/adapter/bf;->a:Landroid/widget/TextView;

    iget-object v3, p0, Lcom/google/android/youtube/app/adapter/aq;->a:Landroid/content/res/Resources;

    const v4, 0x7f0b020c

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v2, v0, Lcom/google/android/youtube/app/adapter/bf;->e:Landroid/widget/ImageView;

    const v3, 0x7f02012b

    invoke-virtual {v2, v3}, Landroid/widget/ImageView;->setImageResource(I)V

    goto :goto_1
.end method
