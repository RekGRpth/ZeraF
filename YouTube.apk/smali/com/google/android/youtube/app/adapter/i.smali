.class final Lcom/google/android/youtube/app/adapter/i;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/youtube/app/adapter/bl;


# instance fields
.field final synthetic a:Lcom/google/android/youtube/app/adapter/h;

.field private b:Landroid/view/View;

.field private c:Landroid/view/View;

.field private d:Landroid/widget/ImageView;

.field private e:Landroid/widget/ImageView;

.field private f:Landroid/widget/ImageView;

.field private g:Lcom/google/android/youtube/core/model/Video;


# direct methods
.method public constructor <init>(Lcom/google/android/youtube/app/adapter/h;Landroid/view/View;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/youtube/app/adapter/i;->a:Lcom/google/android/youtube/app/adapter/h;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p2, p0, Lcom/google/android/youtube/app/adapter/i;->b:Landroid/view/View;

    return-void
.end method

.method private a(Landroid/widget/ImageView;I)Landroid/widget/ImageView;
    .locals 1

    if-eqz p1, :cond_0

    :goto_0
    return-object p1

    :cond_0
    iget-object v0, p0, Lcom/google/android/youtube/app/adapter/i;->b:Landroid/view/View;

    invoke-virtual {v0, p2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewStub;

    if-nez v0, :cond_1

    const/4 p1, 0x0

    goto :goto_0

    :cond_1
    invoke-virtual {v0}, Landroid/view/ViewStub;->inflate()Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    move-object p1, v0

    goto :goto_0
.end method


# virtual methods
.method public final synthetic a(ILjava/lang/Object;)Landroid/view/View;
    .locals 7

    const v6, 0x7f070094

    const v5, 0x7f070093

    const/4 v1, 0x1

    const/16 v3, 0x8

    const/4 v2, 0x0

    check-cast p2, Lcom/google/android/youtube/core/model/Video;

    iput-object p2, p0, Lcom/google/android/youtube/app/adapter/i;->g:Lcom/google/android/youtube/core/model/Video;

    iget-object v0, p0, Lcom/google/android/youtube/app/adapter/i;->a:Lcom/google/android/youtube/app/adapter/h;

    invoke-static {v0}, Lcom/google/android/youtube/app/adapter/h;->a(Lcom/google/android/youtube/app/adapter/h;)Z

    move-result v0

    if-eqz v0, :cond_9

    iget-boolean v0, p2, Lcom/google/android/youtube/core/model/Video;->is3d:Z

    if-eqz v0, :cond_9

    iget-object v0, p0, Lcom/google/android/youtube/app/adapter/i;->d:Landroid/widget/ImageView;

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/youtube/app/adapter/i;->b:Landroid/view/View;

    invoke-virtual {v0, v5}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    if-eqz v0, :cond_9

    :cond_0
    move v0, v1

    :goto_0
    iget-object v4, p0, Lcom/google/android/youtube/app/adapter/i;->a:Lcom/google/android/youtube/app/adapter/h;

    invoke-static {v4}, Lcom/google/android/youtube/app/adapter/h;->a(Lcom/google/android/youtube/app/adapter/h;)Z

    move-result v4

    if-eqz v4, :cond_1

    if-eqz v0, :cond_a

    iget-object v4, p0, Lcom/google/android/youtube/app/adapter/i;->d:Landroid/widget/ImageView;

    invoke-direct {p0, v4, v5}, Lcom/google/android/youtube/app/adapter/i;->a(Landroid/widget/ImageView;I)Landroid/widget/ImageView;

    move-result-object v4

    iput-object v4, p0, Lcom/google/android/youtube/app/adapter/i;->d:Landroid/widget/ImageView;

    iget-object v4, p0, Lcom/google/android/youtube/app/adapter/i;->d:Landroid/widget/ImageView;

    if-eqz v4, :cond_1

    iget-object v4, p0, Lcom/google/android/youtube/app/adapter/i;->d:Landroid/widget/ImageView;

    invoke-virtual {v4, v2}, Landroid/widget/ImageView;->setVisibility(I)V

    :cond_1
    :goto_1
    iget-object v4, p0, Lcom/google/android/youtube/app/adapter/i;->a:Lcom/google/android/youtube/app/adapter/h;

    invoke-static {v4}, Lcom/google/android/youtube/app/adapter/h;->b(Lcom/google/android/youtube/app/adapter/h;)Z

    move-result v4

    if-eqz v4, :cond_3

    iget-object v4, p0, Lcom/google/android/youtube/app/adapter/i;->c:Landroid/view/View;

    if-nez v4, :cond_2

    iget-object v4, p0, Lcom/google/android/youtube/app/adapter/i;->b:Landroid/view/View;

    const v5, 0x7f07009a

    invoke-virtual {v4, v5}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    iput-object v4, p0, Lcom/google/android/youtube/app/adapter/i;->c:Landroid/view/View;

    :cond_2
    iget-object v4, p0, Lcom/google/android/youtube/app/adapter/i;->c:Landroid/view/View;

    if-eqz v4, :cond_3

    iget-object v4, p0, Lcom/google/android/youtube/app/adapter/i;->g:Lcom/google/android/youtube/core/model/Video;

    iget-boolean v4, v4, Lcom/google/android/youtube/core/model/Video;->isHd:Z

    if-eqz v4, :cond_b

    iget-object v4, p0, Lcom/google/android/youtube/app/adapter/i;->a:Lcom/google/android/youtube/app/adapter/h;

    invoke-static {v4}, Lcom/google/android/youtube/app/adapter/h;->e(Lcom/google/android/youtube/app/adapter/h;)Z

    move-result v4

    if-eqz v4, :cond_b

    if-nez v0, :cond_b

    iget-object v0, p0, Lcom/google/android/youtube/app/adapter/i;->c:Landroid/view/View;

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    :cond_3
    :goto_2
    iget-object v0, p0, Lcom/google/android/youtube/app/adapter/i;->a:Lcom/google/android/youtube/app/adapter/h;

    invoke-static {v0}, Lcom/google/android/youtube/app/adapter/h;->c(Lcom/google/android/youtube/app/adapter/h;)Z

    move-result v0

    if-eqz v0, :cond_6

    iget-object v0, p0, Lcom/google/android/youtube/app/adapter/i;->e:Landroid/widget/ImageView;

    if-eqz v0, :cond_4

    iget-object v0, p0, Lcom/google/android/youtube/app/adapter/i;->e:Landroid/widget/ImageView;

    invoke-virtual {v0, v3}, Landroid/widget/ImageView;->setVisibility(I)V

    :cond_4
    iget-object v0, p0, Lcom/google/android/youtube/app/adapter/i;->e:Landroid/widget/ImageView;

    if-nez v0, :cond_5

    iget-object v0, p0, Lcom/google/android/youtube/app/adapter/i;->b:Landroid/view/View;

    invoke-virtual {v0, v6}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    if-eqz v0, :cond_6

    :cond_5
    iget-object v0, p0, Lcom/google/android/youtube/app/adapter/i;->a:Lcom/google/android/youtube/app/adapter/h;

    invoke-static {v0}, Lcom/google/android/youtube/app/adapter/h;->f(Lcom/google/android/youtube/app/adapter/h;)Ljava/util/Set;

    move-result-object v0

    iget-object v4, p0, Lcom/google/android/youtube/app/adapter/i;->g:Lcom/google/android/youtube/core/model/Video;

    iget-object v4, v4, Lcom/google/android/youtube/core/model/Video;->id:Ljava/lang/String;

    invoke-interface {v0, v4}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_6

    iget-object v0, p0, Lcom/google/android/youtube/app/adapter/i;->e:Landroid/widget/ImageView;

    invoke-direct {p0, v0, v6}, Lcom/google/android/youtube/app/adapter/i;->a(Landroid/widget/ImageView;I)Landroid/widget/ImageView;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/youtube/app/adapter/i;->e:Landroid/widget/ImageView;

    iget-object v0, p0, Lcom/google/android/youtube/app/adapter/i;->e:Landroid/widget/ImageView;

    invoke-virtual {v0, v2}, Landroid/widget/ImageView;->setVisibility(I)V

    :cond_6
    iget-object v0, p0, Lcom/google/android/youtube/app/adapter/i;->a:Lcom/google/android/youtube/app/adapter/h;

    invoke-static {v0}, Lcom/google/android/youtube/app/adapter/h;->d(Lcom/google/android/youtube/app/adapter/h;)Z

    move-result v0

    if-eqz v0, :cond_8

    iget-object v0, p0, Lcom/google/android/youtube/app/adapter/i;->a:Lcom/google/android/youtube/app/adapter/h;

    invoke-static {v0}, Lcom/google/android/youtube/app/adapter/h;->g(Lcom/google/android/youtube/app/adapter/h;)Lcom/google/android/youtube/app/prefetch/d;

    move-result-object v0

    if-eqz v0, :cond_10

    iget-object v0, p0, Lcom/google/android/youtube/app/adapter/i;->a:Lcom/google/android/youtube/app/adapter/h;

    invoke-static {v0}, Lcom/google/android/youtube/app/adapter/h;->g(Lcom/google/android/youtube/app/adapter/h;)Lcom/google/android/youtube/app/prefetch/d;

    move-result-object v0

    iget-object v4, p0, Lcom/google/android/youtube/app/adapter/i;->g:Lcom/google/android/youtube/core/model/Video;

    iget-object v4, v4, Lcom/google/android/youtube/core/model/Video;->id:Ljava/lang/String;

    invoke-virtual {v0, v4}, Lcom/google/android/youtube/app/prefetch/d;->b(Ljava/lang/String;)Lcom/google/android/youtube/core/transfer/Transfer$Status;

    move-result-object v0

    if-eqz v0, :cond_10

    sget-object v4, Lcom/google/android/youtube/core/transfer/Transfer$Status;->FAILED:Lcom/google/android/youtube/core/transfer/Transfer$Status;

    if-eq v0, v4, :cond_10

    sget-object v4, Lcom/google/android/youtube/core/transfer/Transfer$Status;->COMPLETED:Lcom/google/android/youtube/core/transfer/Transfer$Status;

    if-eq v0, v4, :cond_7

    iget-object v4, p0, Lcom/google/android/youtube/app/adapter/i;->a:Lcom/google/android/youtube/app/adapter/h;

    invoke-static {v4}, Lcom/google/android/youtube/app/adapter/h;->g(Lcom/google/android/youtube/app/adapter/h;)Lcom/google/android/youtube/app/prefetch/d;

    move-result-object v4

    invoke-virtual {v4}, Lcom/google/android/youtube/app/prefetch/d;->c()Z

    move-result v4

    if-eqz v4, :cond_10

    :cond_7
    iget-object v4, p0, Lcom/google/android/youtube/app/adapter/i;->f:Landroid/widget/ImageView;

    const v5, 0x7f070095

    invoke-direct {p0, v4, v5}, Lcom/google/android/youtube/app/adapter/i;->a(Landroid/widget/ImageView;I)Landroid/widget/ImageView;

    move-result-object v4

    iput-object v4, p0, Lcom/google/android/youtube/app/adapter/i;->f:Landroid/widget/ImageView;

    iget-object v4, p0, Lcom/google/android/youtube/app/adapter/i;->f:Landroid/widget/ImageView;

    if-eqz v4, :cond_10

    iget-object v4, p0, Lcom/google/android/youtube/app/adapter/i;->f:Landroid/widget/ImageView;

    sget-object v5, Lcom/google/android/youtube/core/transfer/Transfer$Status;->COMPLETED:Lcom/google/android/youtube/core/transfer/Transfer$Status;

    if-ne v0, v5, :cond_d

    iget-object v0, p0, Lcom/google/android/youtube/app/adapter/i;->a:Lcom/google/android/youtube/app/adapter/h;

    invoke-static {v0}, Lcom/google/android/youtube/app/adapter/h;->h(Lcom/google/android/youtube/app/adapter/h;)Z

    move-result v0

    if-eqz v0, :cond_c

    const v0, 0x7f020170

    :goto_3
    invoke-virtual {v4, v0}, Landroid/widget/ImageView;->setImageResource(I)V

    :goto_4
    iget-object v0, p0, Lcom/google/android/youtube/app/adapter/i;->f:Landroid/widget/ImageView;

    if-eqz v0, :cond_8

    iget-object v0, p0, Lcom/google/android/youtube/app/adapter/i;->f:Landroid/widget/ImageView;

    if-eqz v1, :cond_f

    :goto_5
    invoke-virtual {v0, v2}, Landroid/widget/ImageView;->setVisibility(I)V

    :cond_8
    iget-object v0, p0, Lcom/google/android/youtube/app/adapter/i;->b:Landroid/view/View;

    return-object v0

    :cond_9
    move v0, v2

    goto/16 :goto_0

    :cond_a
    iget-object v4, p0, Lcom/google/android/youtube/app/adapter/i;->d:Landroid/widget/ImageView;

    if-eqz v4, :cond_1

    iget-object v4, p0, Lcom/google/android/youtube/app/adapter/i;->d:Landroid/widget/ImageView;

    invoke-virtual {v4, v3}, Landroid/widget/ImageView;->setVisibility(I)V

    goto/16 :goto_1

    :cond_b
    iget-object v0, p0, Lcom/google/android/youtube/app/adapter/i;->c:Landroid/view/View;

    invoke-virtual {v0, v3}, Landroid/view/View;->setVisibility(I)V

    goto/16 :goto_2

    :cond_c
    const v0, 0x7f020171

    goto :goto_3

    :cond_d
    iget-object v0, p0, Lcom/google/android/youtube/app/adapter/i;->a:Lcom/google/android/youtube/app/adapter/h;

    invoke-static {v0}, Lcom/google/android/youtube/app/adapter/h;->h(Lcom/google/android/youtube/app/adapter/h;)Z

    move-result v0

    if-eqz v0, :cond_e

    const v0, 0x7f020172

    goto :goto_3

    :cond_e
    const v0, 0x7f020173

    goto :goto_3

    :cond_f
    move v2, v3

    goto :goto_5

    :cond_10
    move v1, v2

    goto :goto_4
.end method
