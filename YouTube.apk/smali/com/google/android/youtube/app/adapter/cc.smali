.class final Lcom/google/android/youtube/app/adapter/cc;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/youtube/app/adapter/bl;


# instance fields
.field final synthetic a:Lcom/google/android/youtube/app/adapter/cb;

.field private final b:Landroid/view/View;

.field private final c:Landroid/widget/TextView;

.field private final d:Landroid/widget/ImageView;

.field private final e:Landroid/widget/TextView;

.field private final f:Landroid/widget/ProgressBar;

.field private final g:Landroid/widget/TextView;

.field private final h:Landroid/widget/TextView;

.field private final i:Landroid/widget/ImageView;


# direct methods
.method private constructor <init>(Lcom/google/android/youtube/app/adapter/cb;Landroid/view/View;)V
    .locals 3

    iput-object p1, p0, Lcom/google/android/youtube/app/adapter/cc;->a:Lcom/google/android/youtube/app/adapter/cb;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p2, p0, Lcom/google/android/youtube/app/adapter/cc;->b:Landroid/view/View;

    const v0, 0x7f07002c

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/google/android/youtube/app/adapter/cc;->c:Landroid/widget/TextView;

    const v0, 0x7f070035

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/google/android/youtube/app/adapter/cc;->d:Landroid/widget/ImageView;

    const v0, 0x7f070059

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/google/android/youtube/app/adapter/cc;->e:Landroid/widget/TextView;

    const v0, 0x7f07003e

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ProgressBar;

    iput-object v0, p0, Lcom/google/android/youtube/app/adapter/cc;->f:Landroid/widget/ProgressBar;

    const v0, 0x7f070163

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/google/android/youtube/app/adapter/cc;->g:Landroid/widget/TextView;

    iget-object v0, p0, Lcom/google/android/youtube/app/adapter/cc;->g:Landroid/widget/TextView;

    new-instance v1, Lcom/google/android/youtube/app/ui/dg;

    invoke-virtual {p2}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-direct {v1, v2}, Lcom/google/android/youtube/app/ui/dg;-><init>(Landroid/content/Context;)V

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTransformationMethod(Landroid/text/method/TransformationMethod;)V

    const v0, 0x7f070164

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/google/android/youtube/app/adapter/cc;->h:Landroid/widget/TextView;

    iget-object v0, p0, Lcom/google/android/youtube/app/adapter/cc;->h:Landroid/widget/TextView;

    new-instance v1, Lcom/google/android/youtube/app/ui/dg;

    invoke-virtual {p2}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-direct {v1, v2}, Lcom/google/android/youtube/app/ui/dg;-><init>(Landroid/content/Context;)V

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTransformationMethod(Landroid/text/method/TransformationMethod;)V

    const v0, 0x7f070165

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    if-nez v0, :cond_0

    const v0, 0x7f070038

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewStub;

    invoke-virtual {v0}, Landroid/view/ViewStub;->inflate()Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    :cond_0
    iput-object v0, p0, Lcom/google/android/youtube/app/adapter/cc;->i:Landroid/widget/ImageView;

    return-void
.end method

.method synthetic constructor <init>(Lcom/google/android/youtube/app/adapter/cb;Landroid/view/View;Landroid/view/ViewGroup;)V
    .locals 0

    invoke-direct {p0, p1, p2}, Lcom/google/android/youtube/app/adapter/cc;-><init>(Lcom/google/android/youtube/app/adapter/cb;Landroid/view/View;)V

    return-void
.end method


# virtual methods
.method public final synthetic a(ILjava/lang/Object;)Landroid/view/View;
    .locals 4

    check-cast p2, Lcom/google/android/youtube/core/transfer/Transfer;

    iget-object v0, p0, Lcom/google/android/youtube/app/adapter/cc;->a:Lcom/google/android/youtube/app/adapter/cb;

    invoke-static {v0}, Lcom/google/android/youtube/app/adapter/cb;->a(Lcom/google/android/youtube/app/adapter/cb;)Ljava/util/WeakHashMap;

    move-result-object v0

    invoke-virtual {v0, p2, p0}, Ljava/util/WeakHashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v0, p0, Lcom/google/android/youtube/app/adapter/cc;->a:Lcom/google/android/youtube/app/adapter/cb;

    invoke-static {v0}, Lcom/google/android/youtube/app/adapter/cb;->b(Lcom/google/android/youtube/app/adapter/cb;)Lcom/google/android/youtube/app/ui/v;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/youtube/app/adapter/cc;->i:Landroid/widget/ImageView;

    invoke-virtual {v0, v1, p2}, Lcom/google/android/youtube/app/ui/v;->a(Landroid/view/View;Ljava/lang/Object;)V

    iget-object v0, p0, Lcom/google/android/youtube/app/adapter/cc;->e:Landroid/widget/TextView;

    if-eqz v0, :cond_0

    iget-object v0, p2, Lcom/google/android/youtube/core/transfer/Transfer;->g:Lcom/google/android/youtube/core/transfer/d;

    const-string v1, "username"

    invoke-virtual {v0, v1}, Lcom/google/android/youtube/core/transfer/d;->a(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/youtube/app/adapter/cc;->e:Landroid/widget/TextView;

    iget-object v1, p2, Lcom/google/android/youtube/core/transfer/Transfer;->g:Lcom/google/android/youtube/core/transfer/d;

    const-string v2, "username"

    invoke-virtual {v1, v2}, Lcom/google/android/youtube/core/transfer/d;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    :cond_0
    iget-object v0, p0, Lcom/google/android/youtube/app/adapter/cc;->c:Landroid/widget/TextView;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/youtube/app/adapter/cc;->c:Landroid/widget/TextView;

    iget-object v1, p2, Lcom/google/android/youtube/core/transfer/Transfer;->g:Lcom/google/android/youtube/core/transfer/d;

    const-string v2, "upload_title"

    invoke-virtual {v1, v2}, Lcom/google/android/youtube/core/transfer/d;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    :cond_1
    iget-object v0, p0, Lcom/google/android/youtube/app/adapter/cc;->d:Landroid/widget/ImageView;

    if-eqz v0, :cond_2

    iget-object v0, p2, Lcom/google/android/youtube/core/transfer/Transfer;->g:Lcom/google/android/youtube/core/transfer/d;

    const-string v1, "upload_file_thumbnail"

    invoke-virtual {v0, v1}, Lcom/google/android/youtube/core/transfer/d;->d(Ljava/lang/String;)[B

    move-result-object v0

    if-eqz v0, :cond_2

    iget-object v1, p0, Lcom/google/android/youtube/app/adapter/cc;->d:Landroid/widget/ImageView;

    const/4 v2, 0x0

    array-length v3, v0

    invoke-static {v0, v2, v3}, Landroid/graphics/BitmapFactory;->decodeByteArray([BII)Landroid/graphics/Bitmap;

    move-result-object v0

    invoke-virtual {v1, v0}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    :cond_2
    iget-object v0, p0, Lcom/google/android/youtube/app/adapter/cc;->g:Landroid/widget/TextView;

    if-eqz v0, :cond_3

    iget-object v0, p2, Lcom/google/android/youtube/core/transfer/Transfer;->c:Lcom/google/android/youtube/core/transfer/Transfer$Status;

    sget-object v1, Lcom/google/android/youtube/core/transfer/Transfer$Status;->RUNNING:Lcom/google/android/youtube/core/transfer/Transfer$Status;

    if-ne v0, v1, :cond_4

    iget-object v0, p0, Lcom/google/android/youtube/app/adapter/cc;->g:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/google/android/youtube/app/adapter/cc;->a:Lcom/google/android/youtube/app/adapter/cb;

    invoke-static {v1}, Lcom/google/android/youtube/app/adapter/cb;->c(Lcom/google/android/youtube/app/adapter/cb;)Landroid/content/Context;

    move-result-object v1

    iget-wide v2, p2, Lcom/google/android/youtube/core/transfer/Transfer;->f:J

    invoke-static {v1, v2, v3}, Landroid/text/format/Formatter;->formatShortFileSize(Landroid/content/Context;J)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    :cond_3
    :goto_0
    invoke-virtual {p0, p2}, Lcom/google/android/youtube/app/adapter/cc;->a(Lcom/google/android/youtube/core/transfer/Transfer;)V

    iget-object v0, p0, Lcom/google/android/youtube/app/adapter/cc;->b:Landroid/view/View;

    return-object v0

    :cond_4
    iget-object v0, p0, Lcom/google/android/youtube/app/adapter/cc;->g:Landroid/widget/TextView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0
.end method

.method final a(Lcom/google/android/youtube/core/transfer/Transfer;)V
    .locals 7

    const/4 v1, 0x1

    const/4 v0, 0x0

    const/high16 v2, 0x42c80000

    iget-wide v3, p1, Lcom/google/android/youtube/core/transfer/Transfer;->e:J

    long-to-float v3, v3

    mul-float/2addr v2, v3

    iget-wide v3, p1, Lcom/google/android/youtube/core/transfer/Transfer;->f:J

    long-to-float v3, v3

    div-float/2addr v2, v3

    invoke-static {v2}, Landroid/util/FloatMath;->ceil(F)F

    move-result v2

    float-to-int v2, v2

    iget-object v3, p0, Lcom/google/android/youtube/app/adapter/cc;->f:Landroid/widget/ProgressBar;

    if-eqz v3, :cond_0

    iget-object v3, p1, Lcom/google/android/youtube/core/transfer/Transfer;->c:Lcom/google/android/youtube/core/transfer/Transfer$Status;

    sget-object v4, Lcom/google/android/youtube/core/transfer/Transfer$Status;->FAILED:Lcom/google/android/youtube/core/transfer/Transfer$Status;

    if-ne v3, v4, :cond_2

    iget-object v3, p0, Lcom/google/android/youtube/app/adapter/cc;->f:Landroid/widget/ProgressBar;

    invoke-virtual {v3, v0}, Landroid/widget/ProgressBar;->setProgress(I)V

    :cond_0
    :goto_0
    iget-object v3, p0, Lcom/google/android/youtube/app/adapter/cc;->h:Landroid/widget/TextView;

    if-eqz v3, :cond_6

    iget-object v3, p1, Lcom/google/android/youtube/core/transfer/Transfer;->c:Lcom/google/android/youtube/core/transfer/Transfer$Status;

    sget-object v4, Lcom/google/android/youtube/core/transfer/Transfer$Status;->RUNNING:Lcom/google/android/youtube/core/transfer/Transfer$Status;

    if-ne v3, v4, :cond_4

    iget-wide v3, p1, Lcom/google/android/youtube/core/transfer/Transfer;->f:J

    iget-wide v5, p1, Lcom/google/android/youtube/core/transfer/Transfer;->e:J

    cmp-long v3, v3, v5

    if-nez v3, :cond_3

    iget-object v1, p0, Lcom/google/android/youtube/app/adapter/cc;->h:Landroid/widget/TextView;

    const v2, 0x7f0b0198

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(I)V

    :goto_1
    iget-object v1, p0, Lcom/google/android/youtube/app/adapter/cc;->i:Landroid/widget/ImageView;

    invoke-virtual {v1, v0}, Landroid/widget/ImageView;->setClickable(Z)V

    iget-object v1, p0, Lcom/google/android/youtube/app/adapter/cc;->i:Landroid/widget/ImageView;

    invoke-virtual {v1, v0}, Landroid/widget/ImageView;->setEnabled(Z)V

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/google/android/youtube/app/adapter/cc;->a:Lcom/google/android/youtube/app/adapter/cb;

    invoke-static {v0}, Lcom/google/android/youtube/app/adapter/cb;->b(Lcom/google/android/youtube/app/adapter/cb;)Lcom/google/android/youtube/app/ui/v;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/youtube/app/ui/v;->a()V

    :cond_1
    return-void

    :cond_2
    iget-object v3, p0, Lcom/google/android/youtube/app/adapter/cc;->f:Landroid/widget/ProgressBar;

    invoke-virtual {v3, v2}, Landroid/widget/ProgressBar;->setProgress(I)V

    goto :goto_0

    :cond_3
    iget-object v3, p0, Lcom/google/android/youtube/app/adapter/cc;->h:Landroid/widget/TextView;

    iget-object v4, p0, Lcom/google/android/youtube/app/adapter/cc;->a:Lcom/google/android/youtube/app/adapter/cb;

    invoke-static {v4}, Lcom/google/android/youtube/app/adapter/cb;->c(Lcom/google/android/youtube/app/adapter/cb;)Landroid/content/Context;

    move-result-object v4

    const v5, 0x7f0b01ad

    new-array v6, v1, [Ljava/lang/Object;

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v6, v0

    invoke-virtual {v4, v5, v6}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v3, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    move v0, v1

    goto :goto_1

    :cond_4
    iget-object v0, p1, Lcom/google/android/youtube/core/transfer/Transfer;->c:Lcom/google/android/youtube/core/transfer/Transfer$Status;

    sget-object v2, Lcom/google/android/youtube/core/transfer/Transfer$Status;->FAILED:Lcom/google/android/youtube/core/transfer/Transfer$Status;

    if-ne v0, v2, :cond_5

    iget-object v0, p0, Lcom/google/android/youtube/app/adapter/cc;->h:Landroid/widget/TextView;

    const v2, 0x7f0b019a

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setText(I)V

    move v0, v1

    goto :goto_1

    :cond_5
    iget-object v0, p0, Lcom/google/android/youtube/app/adapter/cc;->h:Landroid/widget/TextView;

    const v2, 0x7f0b0199

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setText(I)V

    :cond_6
    move v0, v1

    goto :goto_1
.end method
