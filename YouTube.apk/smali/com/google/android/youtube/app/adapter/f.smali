.class public abstract Lcom/google/android/youtube/app/adapter/f;
.super Lcom/google/android/youtube/app/adapter/n;
.source "SourceFile"


# instance fields
.field private final a:Lcom/google/android/youtube/core/client/bc;

.field private final b:Lcom/google/android/youtube/core/client/be;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/google/android/youtube/core/client/bc;Lcom/google/android/youtube/core/client/be;)V
    .locals 1

    const v0, 0x7f070070

    invoke-direct {p0, p1, v0}, Lcom/google/android/youtube/app/adapter/n;-><init>(Landroid/content/Context;I)V

    invoke-static {p2}, Lcom/google/android/youtube/core/utils/s;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/youtube/core/client/bc;

    iput-object v0, p0, Lcom/google/android/youtube/app/adapter/f;->a:Lcom/google/android/youtube/core/client/bc;

    invoke-static {p3}, Lcom/google/android/youtube/core/utils/s;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/youtube/core/client/be;

    iput-object v0, p0, Lcom/google/android/youtube/app/adapter/f;->b:Lcom/google/android/youtube/core/client/be;

    return-void
.end method

.method static synthetic a(Lcom/google/android/youtube/app/adapter/f;)Lcom/google/android/youtube/core/client/be;
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/app/adapter/f;->b:Lcom/google/android/youtube/core/client/be;

    return-object v0
.end method


# virtual methods
.method protected abstract a(Ljava/lang/Object;)Landroid/net/Uri;
.end method

.method protected a(Ljava/lang/Object;Landroid/view/View;Lcom/google/android/youtube/core/async/n;)V
    .locals 3

    iget-object v0, p0, Lcom/google/android/youtube/app/adapter/f;->a:Lcom/google/android/youtube/core/client/bc;

    invoke-virtual {p0, p1}, Lcom/google/android/youtube/app/adapter/f;->a(Ljava/lang/Object;)Landroid/net/Uri;

    move-result-object v1

    new-instance v2, Lcom/google/android/youtube/app/adapter/g;

    invoke-direct {v2, p0, p3}, Lcom/google/android/youtube/app/adapter/g;-><init>(Lcom/google/android/youtube/app/adapter/f;Lcom/google/android/youtube/core/async/n;)V

    invoke-interface {v0, v1, v2}, Lcom/google/android/youtube/core/client/bc;->c(Landroid/net/Uri;Lcom/google/android/youtube/core/async/n;)V

    return-void
.end method
