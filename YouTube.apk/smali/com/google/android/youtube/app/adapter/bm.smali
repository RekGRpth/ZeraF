.class public Lcom/google/android/youtube/app/adapter/bm;
.super Lcom/google/android/youtube/core/a/a;
.source "SourceFile"


# instance fields
.field private final a:I

.field private final b:Lcom/google/android/youtube/app/adapter/bv;

.field private final d:Landroid/view/LayoutInflater;

.field private final e:Ljava/util/List;

.field private final f:Ljava/util/List;

.field private g:I


# direct methods
.method public constructor <init>(Landroid/content/Context;ILcom/google/android/youtube/app/adapter/bv;)V
    .locals 2

    const/4 v1, 0x2

    invoke-direct {p0}, Lcom/google/android/youtube/core/a/a;-><init>()V

    iput p2, p0, Lcom/google/android/youtube/app/adapter/bm;->a:I

    const-string v0, "renderer can\'t be null"

    invoke-static {p3, v0}, Lcom/google/android/youtube/core/utils/s;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/youtube/app/adapter/bv;

    iput-object v0, p0, Lcom/google/android/youtube/app/adapter/bm;->b:Lcom/google/android/youtube/app/adapter/bv;

    invoke-static {p1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/youtube/app/adapter/bm;->d:Landroid/view/LayoutInflater;

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(I)V

    iput-object v0, p0, Lcom/google/android/youtube/app/adapter/bm;->e:Ljava/util/List;

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(I)V

    iput-object v0, p0, Lcom/google/android/youtube/app/adapter/bm;->f:Ljava/util/List;

    return-void
.end method


# virtual methods
.method public final a(ILcom/google/android/youtube/app/adapter/by;)I
    .locals 2

    iget-object v0, p0, Lcom/google/android/youtube/app/adapter/bm;->e:Ljava/util/List;

    invoke-interface {v0, p1, p2}, Ljava/util/List;->add(ILjava/lang/Object;)V

    iget v1, p0, Lcom/google/android/youtube/app/adapter/bm;->g:I

    invoke-interface {p2}, Lcom/google/android/youtube/app/adapter/by;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    add-int/2addr v0, v1

    iput v0, p0, Lcom/google/android/youtube/app/adapter/bm;->g:I

    invoke-virtual {p0}, Lcom/google/android/youtube/app/adapter/bm;->notifyDataSetChanged()V

    return p1

    :cond_0
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public final a(Lcom/google/android/youtube/app/adapter/by;)I
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/app/adapter/bm;->e:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    invoke-virtual {p0, v0, p1}, Lcom/google/android/youtube/app/adapter/bm;->a(ILcom/google/android/youtube/app/adapter/by;)I

    move-result v0

    return v0
.end method

.method public final a(I)Lcom/google/android/youtube/app/adapter/by;
    .locals 3

    iget-object v0, p0, Lcom/google/android/youtube/app/adapter/bm;->e:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge p1, v0, :cond_1

    iget-object v0, p0, Lcom/google/android/youtube/app/adapter/bm;->e:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->remove(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/youtube/app/adapter/by;

    iget v2, p0, Lcom/google/android/youtube/app/adapter/bm;->g:I

    invoke-interface {v0}, Lcom/google/android/youtube/app/adapter/by;->a()Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v1, 0x0

    :goto_0
    sub-int v1, v2, v1

    iput v1, p0, Lcom/google/android/youtube/app/adapter/bm;->g:I

    invoke-virtual {p0}, Lcom/google/android/youtube/app/adapter/bm;->notifyDataSetChanged()V

    return-object v0

    :cond_0
    const/4 v1, 0x1

    goto :goto_0

    :cond_1
    new-instance v0, Ljava/lang/IndexOutOfBoundsException;

    invoke-direct {v0}, Ljava/lang/IndexOutOfBoundsException;-><init>()V

    throw v0
.end method

.method protected final a(Ljava/lang/Iterable;)V
    .locals 1

    invoke-super {p0, p1}, Lcom/google/android/youtube/core/a/a;->a(Ljava/lang/Iterable;)V

    iget-object v0, p0, Lcom/google/android/youtube/app/adapter/bm;->b:Lcom/google/android/youtube/app/adapter/bv;

    invoke-interface {v0, p1}, Lcom/google/android/youtube/app/adapter/bv;->a(Ljava/lang/Iterable;)V

    return-void
.end method

.method public areAllItemsEnabled()Z
    .locals 1

    iget v0, p0, Lcom/google/android/youtube/app/adapter/bm;->g:I

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final b(Lcom/google/android/youtube/app/adapter/by;)I
    .locals 3

    invoke-virtual {p0}, Lcom/google/android/youtube/app/adapter/bm;->getCount()I

    move-result v1

    iget-object v0, p0, Lcom/google/android/youtube/app/adapter/bm;->f:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    iget v2, p0, Lcom/google/android/youtube/app/adapter/bm;->g:I

    invoke-interface {p1}, Lcom/google/android/youtube/app/adapter/by;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    add-int/2addr v0, v2

    iput v0, p0, Lcom/google/android/youtube/app/adapter/bm;->g:I

    invoke-virtual {p0}, Lcom/google/android/youtube/app/adapter/bm;->notifyDataSetChanged()V

    return v1

    :cond_0
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public final b()V
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/app/adapter/bm;->f:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    invoke-virtual {p0}, Lcom/google/android/youtube/app/adapter/bm;->notifyDataSetChanged()V

    return-void
.end method

.method public getCount()I
    .locals 2

    invoke-super {p0}, Lcom/google/android/youtube/core/a/a;->getCount()I

    move-result v0

    iget-object v1, p0, Lcom/google/android/youtube/app/adapter/bm;->e:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    add-int/2addr v0, v1

    iget-object v1, p0, Lcom/google/android/youtube/app/adapter/bm;->f:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    add-int/2addr v0, v1

    return v0
.end method

.method public getItem(I)Ljava/lang/Object;
    .locals 3

    const/4 v0, 0x0

    iget-object v1, p0, Lcom/google/android/youtube/app/adapter/bm;->e:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    if-ge p1, v1, :cond_1

    :cond_0
    :goto_0
    return-object v0

    :cond_1
    iget-object v1, p0, Lcom/google/android/youtube/app/adapter/bm;->e:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    sub-int v1, p1, v1

    invoke-super {p0}, Lcom/google/android/youtube/core/a/a;->getCount()I

    move-result v2

    sub-int/2addr v1, v2

    if-gez v1, :cond_0

    iget-object v0, p0, Lcom/google/android/youtube/app/adapter/bm;->e:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    sub-int v0, p1, v0

    invoke-super {p0, v0}, Lcom/google/android/youtube/core/a/a;->getItem(I)Ljava/lang/Object;

    move-result-object v0

    goto :goto_0
.end method

.method public getItemViewType(I)I
    .locals 3

    const/4 v0, -0x1

    iget-object v1, p0, Lcom/google/android/youtube/app/adapter/bm;->e:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    if-ge p1, v1, :cond_1

    :cond_0
    :goto_0
    return v0

    :cond_1
    iget-object v1, p0, Lcom/google/android/youtube/app/adapter/bm;->e:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    sub-int v1, p1, v1

    invoke-super {p0}, Lcom/google/android/youtube/core/a/a;->getCount()I

    move-result v2

    sub-int/2addr v1, v2

    if-gez v1, :cond_0

    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 3

    iget-object v0, p0, Lcom/google/android/youtube/app/adapter/bm;->e:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge p1, v0, :cond_0

    iget-object v0, p0, Lcom/google/android/youtube/app/adapter/bm;->e:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/youtube/app/adapter/by;

    invoke-interface {v0, p1}, Lcom/google/android/youtube/app/adapter/by;->a(I)Landroid/view/View;

    move-result-object p2

    :goto_0
    return-object p2

    :cond_0
    iget-object v0, p0, Lcom/google/android/youtube/app/adapter/bm;->e:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    sub-int v0, p1, v0

    invoke-super {p0}, Lcom/google/android/youtube/core/a/a;->getCount()I

    move-result v1

    sub-int/2addr v0, v1

    if-ltz v0, :cond_1

    iget-object v1, p0, Lcom/google/android/youtube/app/adapter/bm;->f:Ljava/util/List;

    invoke-interface {v1, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/youtube/app/adapter/by;

    invoke-interface {v0, p1}, Lcom/google/android/youtube/app/adapter/by;->a(I)Landroid/view/View;

    move-result-object p2

    goto :goto_0

    :cond_1
    if-nez p2, :cond_2

    iget-object v0, p0, Lcom/google/android/youtube/app/adapter/bm;->d:Landroid/view/LayoutInflater;

    iget v1, p0, Lcom/google/android/youtube/app/adapter/bm;->a:I

    const/4 v2, 0x0

    invoke-virtual {v0, v1, p3, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object p2

    iget-object v0, p0, Lcom/google/android/youtube/app/adapter/bm;->b:Lcom/google/android/youtube/app/adapter/bv;

    invoke-interface {v0, p2, p3}, Lcom/google/android/youtube/app/adapter/bv;->a(Landroid/view/View;Landroid/view/ViewGroup;)Lcom/google/android/youtube/app/adapter/bl;

    move-result-object v0

    invoke-virtual {p2, v0}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    :goto_1
    invoke-virtual {p0, p1}, Lcom/google/android/youtube/app/adapter/bm;->getItem(I)Ljava/lang/Object;

    move-result-object v1

    invoke-interface {v0, p1, v1}, Lcom/google/android/youtube/app/adapter/bl;->a(ILjava/lang/Object;)Landroid/view/View;

    goto :goto_0

    :cond_2
    invoke-virtual {p2}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/youtube/app/adapter/bl;

    goto :goto_1
.end method

.method public getViewTypeCount()I
    .locals 1

    const/4 v0, 0x2

    return v0
.end method

.method public isEnabled(I)Z
    .locals 2

    iget-object v0, p0, Lcom/google/android/youtube/app/adapter/bm;->e:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge p1, v0, :cond_0

    iget-object v0, p0, Lcom/google/android/youtube/app/adapter/bm;->e:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/youtube/app/adapter/by;

    invoke-interface {v0}, Lcom/google/android/youtube/app/adapter/by;->a()Z

    move-result v0

    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lcom/google/android/youtube/app/adapter/bm;->e:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    sub-int v0, p1, v0

    invoke-super {p0}, Lcom/google/android/youtube/core/a/a;->getCount()I

    move-result v1

    sub-int/2addr v0, v1

    if-ltz v0, :cond_1

    iget-object v1, p0, Lcom/google/android/youtube/app/adapter/bm;->f:Ljava/util/List;

    invoke-interface {v1, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/youtube/app/adapter/by;

    invoke-interface {v0}, Lcom/google/android/youtube/app/adapter/by;->a()Z

    move-result v0

    goto :goto_0

    :cond_1
    const/4 v0, 0x1

    goto :goto_0
.end method
