.class final Lcom/google/android/youtube/app/adapter/ae;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/youtube/app/adapter/bl;


# instance fields
.field final synthetic a:Lcom/google/android/youtube/app/adapter/ad;

.field private final b:Landroid/view/View;

.field private final c:Landroid/widget/TextView;

.field private final d:Landroid/widget/TextView;

.field private final e:Landroid/widget/TextView;

.field private final f:Landroid/view/View;


# direct methods
.method public constructor <init>(Lcom/google/android/youtube/app/adapter/ad;Landroid/view/View;)V
    .locals 1

    iput-object p1, p0, Lcom/google/android/youtube/app/adapter/ae;->a:Lcom/google/android/youtube/app/adapter/ad;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p2, p0, Lcom/google/android/youtube/app/adapter/ae;->b:Landroid/view/View;

    const v0, 0x7f070059

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/google/android/youtube/app/adapter/ae;->c:Landroid/widget/TextView;

    const v0, 0x7f070081

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/google/android/youtube/app/adapter/ae;->d:Landroid/widget/TextView;

    const v0, 0x7f070080

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/google/android/youtube/app/adapter/ae;->e:Landroid/widget/TextView;

    const v0, 0x7f07007f

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/youtube/app/adapter/ae;->f:Landroid/view/View;

    return-void
.end method


# virtual methods
.method public final synthetic a(ILjava/lang/Object;)Landroid/view/View;
    .locals 2

    check-cast p2, Lcom/google/android/youtube/core/model/Comment;

    iget-object v0, p0, Lcom/google/android/youtube/app/adapter/ae;->c:Landroid/widget/TextView;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/youtube/app/adapter/ae;->c:Landroid/widget/TextView;

    iget-object v1, p2, Lcom/google/android/youtube/core/model/Comment;->author:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    :cond_0
    iget-object v0, p0, Lcom/google/android/youtube/app/adapter/ae;->d:Landroid/widget/TextView;

    if-eqz v0, :cond_1

    iget-object v0, p2, Lcom/google/android/youtube/core/model/Comment;->publishedDate:Ljava/util/Date;

    iget-object v1, p0, Lcom/google/android/youtube/app/adapter/ae;->a:Lcom/google/android/youtube/app/adapter/ad;

    invoke-static {v1}, Lcom/google/android/youtube/app/adapter/ad;->a(Lcom/google/android/youtube/app/adapter/ad;)Landroid/content/res/Resources;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/youtube/core/utils/ah;->a(Ljava/util/Date;Landroid/content/res/Resources;)Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/youtube/app/adapter/ae;->d:Landroid/widget/TextView;

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    :cond_1
    iget-object v0, p0, Lcom/google/android/youtube/app/adapter/ae;->e:Landroid/widget/TextView;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/google/android/youtube/app/adapter/ae;->e:Landroid/widget/TextView;

    iget-object v1, p2, Lcom/google/android/youtube/core/model/Comment;->content:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    :cond_2
    iget-object v0, p0, Lcom/google/android/youtube/app/adapter/ae;->f:Landroid/view/View;

    if-eqz v0, :cond_3

    iget-object v1, p0, Lcom/google/android/youtube/app/adapter/ae;->f:Landroid/view/View;

    if-nez p1, :cond_4

    const/16 v0, 0x8

    :goto_0
    invoke-virtual {v1, v0}, Landroid/view/View;->setVisibility(I)V

    :cond_3
    iget-object v0, p0, Lcom/google/android/youtube/app/adapter/ae;->b:Landroid/view/View;

    return-object v0

    :cond_4
    const/4 v0, 0x0

    goto :goto_0
.end method
