.class public final Lcom/google/android/youtube/app/adapter/ce;
.super Lcom/google/android/youtube/app/adapter/j;
.source "SourceFile"


# instance fields
.field private final a:Lcom/google/android/youtube/app/adapter/bv;

.field private final b:Lcom/google/android/youtube/app/adapter/cb;


# direct methods
.method public constructor <init>(Lcom/google/android/youtube/app/adapter/bv;Lcom/google/android/youtube/app/adapter/cb;)V
    .locals 1

    invoke-direct {p0}, Lcom/google/android/youtube/app/adapter/j;-><init>()V

    const-string v0, "videoRendererFactory cannot be null"

    invoke-static {p1, v0}, Lcom/google/android/youtube/core/utils/s;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/youtube/app/adapter/bv;

    iput-object v0, p0, Lcom/google/android/youtube/app/adapter/ce;->a:Lcom/google/android/youtube/app/adapter/bv;

    const-string v0, "transferRendererFactory cannot be null"

    invoke-static {p2, v0}, Lcom/google/android/youtube/core/utils/s;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/youtube/app/adapter/cb;

    iput-object v0, p0, Lcom/google/android/youtube/app/adapter/ce;->b:Lcom/google/android/youtube/app/adapter/cb;

    return-void
.end method


# virtual methods
.method public final a(Landroid/view/View;Landroid/view/ViewGroup;)Lcom/google/android/youtube/app/adapter/bl;
    .locals 6

    new-instance v0, Lcom/google/android/youtube/app/adapter/cf;

    iget-object v1, p0, Lcom/google/android/youtube/app/adapter/ce;->a:Lcom/google/android/youtube/app/adapter/bv;

    invoke-interface {v1, p1, p2}, Lcom/google/android/youtube/app/adapter/bv;->a(Landroid/view/View;Landroid/view/ViewGroup;)Lcom/google/android/youtube/app/adapter/bl;

    move-result-object v4

    iget-object v1, p0, Lcom/google/android/youtube/app/adapter/ce;->b:Lcom/google/android/youtube/app/adapter/cb;

    invoke-virtual {v1, p1, p2}, Lcom/google/android/youtube/app/adapter/cb;->a(Landroid/view/View;Landroid/view/ViewGroup;)Lcom/google/android/youtube/app/adapter/bl;

    move-result-object v5

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    invoke-direct/range {v0 .. v5}, Lcom/google/android/youtube/app/adapter/cf;-><init>(Lcom/google/android/youtube/app/adapter/ce;Landroid/view/View;Landroid/view/ViewGroup;Lcom/google/android/youtube/app/adapter/bl;Lcom/google/android/youtube/app/adapter/bl;)V

    return-object v0
.end method

.method public final a(Lcom/google/android/youtube/core/transfer/Transfer;)Z
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/app/adapter/ce;->b:Lcom/google/android/youtube/app/adapter/cb;

    invoke-virtual {v0, p1}, Lcom/google/android/youtube/app/adapter/cb;->a(Lcom/google/android/youtube/core/transfer/Transfer;)Z

    move-result v0

    return v0
.end method
