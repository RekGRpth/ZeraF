.class public final Lcom/google/android/youtube/app/adapter/cd;
.super Lcom/google/android/youtube/app/adapter/bm;
.source "SourceFile"


# instance fields
.field private final a:Lcom/google/android/youtube/app/adapter/ce;


# direct methods
.method public constructor <init>(Landroid/content/Context;ILcom/google/android/youtube/app/adapter/ce;)V
    .locals 1

    const v0, 0x7f0400a7

    invoke-direct {p0, p1, v0, p3}, Lcom/google/android/youtube/app/adapter/bm;-><init>(Landroid/content/Context;ILcom/google/android/youtube/app/adapter/bv;)V

    iput-object p3, p0, Lcom/google/android/youtube/app/adapter/cd;->a:Lcom/google/android/youtube/app/adapter/ce;

    return-void
.end method


# virtual methods
.method public final a(Lcom/google/android/youtube/core/transfer/Transfer;)Z
    .locals 4

    iget-object v0, p0, Lcom/google/android/youtube/app/adapter/cd;->a:Lcom/google/android/youtube/app/adapter/ce;

    invoke-virtual {v0, p1}, Lcom/google/android/youtube/app/adapter/ce;->a(Lcom/google/android/youtube/core/transfer/Transfer;)Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-static {p1}, Lcom/google/android/youtube/app/ui/cz;->a(Lcom/google/android/youtube/core/transfer/Transfer;)Lcom/google/android/youtube/app/ui/cz;

    move-result-object v3

    const/4 v0, 0x0

    move v1, v0

    :goto_0
    invoke-virtual {p0}, Lcom/google/android/youtube/app/adapter/cd;->getCount()I

    move-result v0

    if-ge v1, v0, :cond_1

    invoke-virtual {p0, v1}, Lcom/google/android/youtube/app/adapter/cd;->getItem(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/youtube/app/ui/cz;

    invoke-virtual {v0, v3}, Lcom/google/android/youtube/app/ui/cz;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0, v1, v3}, Lcom/google/android/youtube/app/adapter/cd;->b(ILjava/lang/Object;)V

    :cond_0
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    :cond_1
    return v2
.end method
