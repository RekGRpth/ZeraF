.class final Lcom/google/android/youtube/app/adapter/av;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/youtube/core/async/n;


# instance fields
.field final synthetic a:Lcom/google/android/youtube/app/adapter/at;

.field private final b:Lcom/google/android/youtube/core/model/Video;

.field private final c:Lcom/google/android/youtube/core/async/n;


# direct methods
.method public constructor <init>(Lcom/google/android/youtube/app/adapter/at;Lcom/google/android/youtube/core/model/Video;Lcom/google/android/youtube/core/async/n;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/youtube/app/adapter/av;->a:Lcom/google/android/youtube/app/adapter/at;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p2, p0, Lcom/google/android/youtube/app/adapter/av;->b:Lcom/google/android/youtube/core/model/Video;

    iput-object p3, p0, Lcom/google/android/youtube/app/adapter/av;->c:Lcom/google/android/youtube/core/async/n;

    return-void
.end method


# virtual methods
.method public final bridge synthetic a(Ljava/lang/Object;Ljava/lang/Exception;)V
    .locals 0

    return-void
.end method

.method public final synthetic a(Ljava/lang/Object;Ljava/lang/Object;)V
    .locals 3

    check-cast p2, Lcom/google/android/youtube/core/model/UserProfile;

    iget-object v0, p0, Lcom/google/android/youtube/app/adapter/av;->a:Lcom/google/android/youtube/app/adapter/at;

    invoke-static {v0}, Lcom/google/android/youtube/app/adapter/at;->a(Lcom/google/android/youtube/app/adapter/at;)Ljava/util/Map;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/youtube/app/adapter/av;->b:Lcom/google/android/youtube/core/model/Video;

    iget-object v1, v1, Lcom/google/android/youtube/core/model/Video;->id:Ljava/lang/String;

    iget-object v2, p2, Lcom/google/android/youtube/core/model/UserProfile;->thumbnailUri:Landroid/net/Uri;

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v0, p0, Lcom/google/android/youtube/app/adapter/av;->a:Lcom/google/android/youtube/app/adapter/at;

    invoke-static {v0}, Lcom/google/android/youtube/app/adapter/at;->c(Lcom/google/android/youtube/app/adapter/at;)Lcom/google/android/youtube/core/client/be;

    move-result-object v0

    iget-object v1, p2, Lcom/google/android/youtube/core/model/UserProfile;->thumbnailUri:Landroid/net/Uri;

    iget-object v2, p0, Lcom/google/android/youtube/app/adapter/av;->c:Lcom/google/android/youtube/core/async/n;

    invoke-interface {v0, v1, v2}, Lcom/google/android/youtube/core/client/be;->a(Landroid/net/Uri;Lcom/google/android/youtube/core/async/n;)V

    return-void
.end method
