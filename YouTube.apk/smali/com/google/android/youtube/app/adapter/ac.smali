.class public final Lcom/google/android/youtube/app/adapter/ac;
.super Lcom/google/android/youtube/core/a/m;
.source "SourceFile"

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field protected final a:Lcom/google/android/youtube/core/a/e;

.field private final c:Landroid/app/Activity;

.field private final d:I

.field private e:Ljava/lang/String;


# direct methods
.method public constructor <init>(Landroid/app/Activity;Lcom/google/android/youtube/core/a/g;ILcom/google/android/youtube/core/a/e;)V
    .locals 6

    const/4 v3, 0x0

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move v4, p3

    move-object v5, p4

    invoke-direct/range {v0 .. v5}, Lcom/google/android/youtube/app/adapter/ac;-><init>(Landroid/app/Activity;Lcom/google/android/youtube/core/a/g;Ljava/lang/String;ILcom/google/android/youtube/core/a/e;)V

    return-void
.end method

.method public constructor <init>(Landroid/app/Activity;Lcom/google/android/youtube/core/a/g;Ljava/lang/String;ILcom/google/android/youtube/core/a/e;)V
    .locals 1

    invoke-direct {p0, p2}, Lcom/google/android/youtube/core/a/m;-><init>(Lcom/google/android/youtube/core/a/g;)V

    const-string v0, "activity cannot be null"

    invoke-static {p1, v0}, Lcom/google/android/youtube/core/utils/s;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/Activity;

    iput-object v0, p0, Lcom/google/android/youtube/app/adapter/ac;->c:Landroid/app/Activity;

    const-string v0, "bodyOutline cannot be null"

    invoke-static {p5, v0}, Lcom/google/android/youtube/core/utils/s;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/youtube/core/a/e;

    iput-object v0, p0, Lcom/google/android/youtube/app/adapter/ac;->a:Lcom/google/android/youtube/core/a/e;

    iput-object p3, p0, Lcom/google/android/youtube/app/adapter/ac;->e:Ljava/lang/String;

    iput p4, p0, Lcom/google/android/youtube/app/adapter/ac;->d:I

    return-void
.end method


# virtual methods
.method public final a(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 4

    const/4 v3, 0x0

    if-nez p2, :cond_0

    iget-object v0, p0, Lcom/google/android/youtube/app/adapter/ac;->c:Landroid/app/Activity;

    invoke-virtual {v0}, Landroid/app/Activity;->getLayoutInflater()Landroid/view/LayoutInflater;

    move-result-object v0

    iget v1, p0, Lcom/google/android/youtube/app/adapter/ac;->d:I

    invoke-virtual {v0, v1, p3, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    :goto_0
    iget-object v1, p0, Lcom/google/android/youtube/app/adapter/ac;->e:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    new-instance v1, Lcom/google/android/youtube/app/ui/dg;

    iget-object v2, p0, Lcom/google/android/youtube/app/adapter/ac;->c:Landroid/app/Activity;

    invoke-direct {v1, v2}, Lcom/google/android/youtube/app/ui/dg;-><init>(Landroid/content/Context;)V

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTransformationMethod(Landroid/text/method/TransformationMethod;)V

    iget-object v1, p0, Lcom/google/android/youtube/app/adapter/ac;->a:Lcom/google/android/youtube/core/a/e;

    invoke-virtual {v1}, Lcom/google/android/youtube/core/a/e;->l()Z

    move-result v1

    if-eqz v1, :cond_1

    const v1, 0x7f0200cd

    :goto_1
    invoke-virtual {v0, v3, v3, v1, v3}, Landroid/widget/TextView;->setCompoundDrawablesWithIntrinsicBounds(IIII)V

    invoke-virtual {v0, p0}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    return-object v0

    :cond_0
    check-cast p2, Landroid/widget/TextView;

    move-object v0, p2

    goto :goto_0

    :cond_1
    const v1, 0x7f0200cb

    goto :goto_1
.end method

.method public final a(Ljava/lang/String;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/youtube/app/adapter/ac;->e:Ljava/lang/String;

    invoke-virtual {p0}, Lcom/google/android/youtube/app/adapter/ac;->k()V

    return-void
.end method

.method public final onClick(Landroid/view/View;)V
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/app/adapter/ac;->a:Lcom/google/android/youtube/core/a/e;

    invoke-virtual {v0}, Lcom/google/android/youtube/core/a/e;->m()V

    invoke-virtual {p0}, Lcom/google/android/youtube/app/adapter/ac;->k()V

    return-void
.end method
