.class final Lcom/google/android/youtube/app/adapter/c;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/youtube/app/adapter/bl;


# instance fields
.field final synthetic a:Lcom/google/android/youtube/app/adapter/a;

.field private final b:Landroid/view/View;

.field private final c:Lcom/google/android/youtube/app/adapter/bl;

.field private final d:Lcom/google/android/youtube/app/adapter/bl;


# direct methods
.method public constructor <init>(Lcom/google/android/youtube/app/adapter/a;Landroid/view/View;Landroid/view/ViewGroup;)V
    .locals 1

    iput-object p1, p0, Lcom/google/android/youtube/app/adapter/c;->a:Lcom/google/android/youtube/app/adapter/a;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p2, p0, Lcom/google/android/youtube/app/adapter/c;->b:Landroid/view/View;

    new-instance v0, Lcom/google/android/youtube/app/adapter/d;

    invoke-direct {v0, p1, p2}, Lcom/google/android/youtube/app/adapter/d;-><init>(Lcom/google/android/youtube/app/adapter/a;Landroid/view/View;)V

    iput-object v0, p0, Lcom/google/android/youtube/app/adapter/c;->c:Lcom/google/android/youtube/app/adapter/bl;

    invoke-static {p1}, Lcom/google/android/youtube/app/adapter/a;->b(Lcom/google/android/youtube/app/adapter/a;)Lcom/google/android/youtube/app/adapter/bv;

    move-result-object v0

    invoke-interface {v0, p2, p3}, Lcom/google/android/youtube/app/adapter/bv;->a(Landroid/view/View;Landroid/view/ViewGroup;)Lcom/google/android/youtube/app/adapter/bl;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/youtube/app/adapter/c;->d:Lcom/google/android/youtube/app/adapter/bl;

    return-void
.end method


# virtual methods
.method public final bridge synthetic a(ILjava/lang/Object;)Landroid/view/View;
    .locals 3

    check-cast p2, Landroid/util/Pair;

    iget-object v0, p2, Landroid/util/Pair;->first:Ljava/lang/Object;

    const-string v1, "music video cannot be null"

    invoke-static {v0, v1}, Lcom/google/android/youtube/core/utils/s;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/youtube/core/model/MusicVideo;

    iget-object v1, p2, Landroid/util/Pair;->second:Ljava/lang/Object;

    const-string v2, "video cannot be null"

    invoke-static {v1, v2}, Lcom/google/android/youtube/core/utils/s;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/youtube/core/model/Video;

    iget-object v2, p0, Lcom/google/android/youtube/app/adapter/c;->c:Lcom/google/android/youtube/app/adapter/bl;

    invoke-interface {v2, p1, v0}, Lcom/google/android/youtube/app/adapter/bl;->a(ILjava/lang/Object;)Landroid/view/View;

    iget-object v0, p0, Lcom/google/android/youtube/app/adapter/c;->d:Lcom/google/android/youtube/app/adapter/bl;

    invoke-interface {v0, p1, v1}, Lcom/google/android/youtube/app/adapter/bl;->a(ILjava/lang/Object;)Landroid/view/View;

    iget-object v0, p0, Lcom/google/android/youtube/app/adapter/c;->b:Landroid/view/View;

    return-object v0
.end method
