.class public final Lcom/google/android/youtube/app/player/mp4/g;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Ljava/lang/Iterable;


# instance fields
.field private final a:Ljava/lang/Iterable;

.field private final b:Ljava/lang/Iterable;

.field private final c:Ljava/lang/Iterable;

.field private final d:Ljava/lang/Iterable;

.field private final e:Ljava/lang/Iterable;

.field private final f:I


# direct methods
.method public constructor <init>(Ljava/lang/Iterable;Ljava/lang/Iterable;Ljava/lang/Iterable;Ljava/lang/Iterable;Ljava/lang/Iterable;I)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/google/android/youtube/app/player/mp4/g;->a:Ljava/lang/Iterable;

    iput-object p2, p0, Lcom/google/android/youtube/app/player/mp4/g;->b:Ljava/lang/Iterable;

    iput-object p3, p0, Lcom/google/android/youtube/app/player/mp4/g;->c:Ljava/lang/Iterable;

    iput-object p4, p0, Lcom/google/android/youtube/app/player/mp4/g;->d:Ljava/lang/Iterable;

    iput-object p5, p0, Lcom/google/android/youtube/app/player/mp4/g;->e:Ljava/lang/Iterable;

    iput p6, p0, Lcom/google/android/youtube/app/player/mp4/g;->f:I

    return-void
.end method


# virtual methods
.method public final iterator()Ljava/util/Iterator;
    .locals 7

    new-instance v0, Lcom/google/android/youtube/app/player/mp4/h;

    iget-object v1, p0, Lcom/google/android/youtube/app/player/mp4/g;->a:Ljava/lang/Iterable;

    invoke-interface {v1}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/youtube/app/player/mp4/g;->b:Ljava/lang/Iterable;

    invoke-interface {v2}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v2

    iget-object v3, p0, Lcom/google/android/youtube/app/player/mp4/g;->c:Ljava/lang/Iterable;

    invoke-interface {v3}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v3

    iget-object v4, p0, Lcom/google/android/youtube/app/player/mp4/g;->d:Ljava/lang/Iterable;

    invoke-interface {v4}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v4

    iget-object v5, p0, Lcom/google/android/youtube/app/player/mp4/g;->e:Ljava/lang/Iterable;

    if-nez v5, :cond_0

    const/4 v5, 0x0

    :goto_0
    iget v6, p0, Lcom/google/android/youtube/app/player/mp4/g;->f:I

    invoke-direct/range {v0 .. v6}, Lcom/google/android/youtube/app/player/mp4/h;-><init>(Ljava/util/Iterator;Ljava/util/Iterator;Ljava/util/Iterator;Ljava/util/Iterator;Ljava/util/Iterator;I)V

    return-object v0

    :cond_0
    iget-object v5, p0, Lcom/google/android/youtube/app/player/mp4/g;->e:Ljava/lang/Iterable;

    invoke-interface {v5}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v5

    goto :goto_0
.end method
