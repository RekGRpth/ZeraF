.class public final Lcom/google/android/youtube/app/player/mp4/HdlrAtom;
.super Lcom/google/android/youtube/app/player/mp4/a;
.source "SourceFile"


# static fields
.field private static final b:Lcom/google/android/youtube/app/player/mp4/s;

.field private static final c:Lcom/google/android/youtube/app/player/mp4/s;


# instance fields
.field private a:Lcom/google/android/youtube/app/player/mp4/HdlrAtom$TrackType;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    new-instance v0, Lcom/google/android/youtube/app/player/mp4/s;

    const-string v1, "vide"

    invoke-direct {v0, v1}, Lcom/google/android/youtube/app/player/mp4/s;-><init>(Ljava/lang/String;)V

    sput-object v0, Lcom/google/android/youtube/app/player/mp4/HdlrAtom;->b:Lcom/google/android/youtube/app/player/mp4/s;

    new-instance v0, Lcom/google/android/youtube/app/player/mp4/s;

    const-string v1, "soun"

    invoke-direct {v0, v1}, Lcom/google/android/youtube/app/player/mp4/s;-><init>(Ljava/lang/String;)V

    sput-object v0, Lcom/google/android/youtube/app/player/mp4/HdlrAtom;->c:Lcom/google/android/youtube/app/player/mp4/s;

    return-void
.end method

.method public constructor <init>(ILcom/google/android/youtube/app/player/mp4/s;)V
    .locals 1

    invoke-direct {p0, p1, p2}, Lcom/google/android/youtube/app/player/mp4/a;-><init>(ILcom/google/android/youtube/app/player/mp4/s;)V

    sget-object v0, Lcom/google/android/youtube/app/player/mp4/HdlrAtom$TrackType;->TRACK_UNKNOWN:Lcom/google/android/youtube/app/player/mp4/HdlrAtom$TrackType;

    iput-object v0, p0, Lcom/google/android/youtube/app/player/mp4/HdlrAtom;->a:Lcom/google/android/youtube/app/player/mp4/HdlrAtom$TrackType;

    return-void
.end method


# virtual methods
.method public final a(Ljava/io/DataInputStream;)V
    .locals 2

    const/16 v0, 0x8

    invoke-virtual {p1, v0}, Ljava/io/DataInputStream;->skipBytes(I)I

    new-instance v0, Lcom/google/android/youtube/app/player/mp4/s;

    invoke-virtual {p1}, Ljava/io/DataInputStream;->readInt()I

    move-result v1

    invoke-direct {v0, v1}, Lcom/google/android/youtube/app/player/mp4/s;-><init>(I)V

    sget-object v1, Lcom/google/android/youtube/app/player/mp4/HdlrAtom;->b:Lcom/google/android/youtube/app/player/mp4/s;

    invoke-virtual {v0, v1}, Lcom/google/android/youtube/app/player/mp4/s;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    sget-object v0, Lcom/google/android/youtube/app/player/mp4/HdlrAtom$TrackType;->TRACK_VIDEO:Lcom/google/android/youtube/app/player/mp4/HdlrAtom$TrackType;

    iput-object v0, p0, Lcom/google/android/youtube/app/player/mp4/HdlrAtom;->a:Lcom/google/android/youtube/app/player/mp4/HdlrAtom$TrackType;

    :cond_0
    :goto_0
    invoke-virtual {p0}, Lcom/google/android/youtube/app/player/mp4/HdlrAtom;->b()I

    move-result v0

    add-int/lit8 v0, v0, -0x4

    add-int/lit8 v0, v0, -0x8

    invoke-virtual {p1, v0}, Ljava/io/DataInputStream;->skipBytes(I)I

    return-void

    :cond_1
    sget-object v1, Lcom/google/android/youtube/app/player/mp4/HdlrAtom;->c:Lcom/google/android/youtube/app/player/mp4/s;

    invoke-virtual {v0, v1}, Lcom/google/android/youtube/app/player/mp4/s;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    sget-object v0, Lcom/google/android/youtube/app/player/mp4/HdlrAtom$TrackType;->TRACK_AUDIO:Lcom/google/android/youtube/app/player/mp4/HdlrAtom$TrackType;

    iput-object v0, p0, Lcom/google/android/youtube/app/player/mp4/HdlrAtom;->a:Lcom/google/android/youtube/app/player/mp4/HdlrAtom$TrackType;

    goto :goto_0
.end method

.method public final e()Lcom/google/android/youtube/app/player/mp4/HdlrAtom$TrackType;
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/app/player/mp4/HdlrAtom;->a:Lcom/google/android/youtube/app/player/mp4/HdlrAtom$TrackType;

    return-object v0
.end method
