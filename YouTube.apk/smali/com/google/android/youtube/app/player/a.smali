.class public final Lcom/google/android/youtube/app/player/a;
.super Lcom/google/android/youtube/core/player/l;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/youtube/core/transfer/n;


# static fields
.field public static final a:Ljava/util/Set;

.field private static final b:Ljava/lang/String;


# instance fields
.field private final c:Lcom/google/android/youtube/core/utils/p;

.field private d:Landroid/content/Context;

.field private e:Landroid/net/Uri;

.field private f:Ljava/util/concurrent/atomic/AtomicReference;

.field private g:I

.field private h:J

.field private i:Lcom/google/android/youtube/core/transfer/b;

.field private j:Z

.field private k:Z

.field private l:Z

.field private m:I

.field private final n:Ljava/util/concurrent/atomic/AtomicLong;

.field private final o:Ljava/util/concurrent/atomic/AtomicBoolean;

.field private final p:Ljava/util/concurrent/atomic/AtomicReference;

.field private final q:Lcom/google/android/youtube/core/utils/z;

.field private r:J

.field private final s:Lcom/google/android/youtube/app/player/h;

.field private t:Landroid/os/Handler;

.field private final u:I

.field private final v:I

.field private final w:Lcom/google/android/youtube/app/player/i;

.field private final x:Ljava/lang/Runnable;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "awf-"

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v1

    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "-"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/google/android/youtube/app/player/a;->b:Ljava/lang/String;

    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    const-string v1, "video/mp4"

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    const-string v1, "video/3gpp"

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    invoke-static {v0}, Ljava/util/Collections;->unmodifiableSet(Ljava/util/Set;)Ljava/util/Set;

    move-result-object v0

    sput-object v0, Lcom/google/android/youtube/app/player/a;->a:Ljava/util/Set;

    new-instance v0, Lcom/google/android/youtube/app/player/b;

    invoke-direct {v0}, Lcom/google/android/youtube/app/player/b;-><init>()V

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Ljava/lang/Thread;->setPriority(I)V

    invoke-virtual {v0}, Ljava/lang/Thread;->start()V

    return-void
.end method

.method public constructor <init>(Lcom/google/android/youtube/core/player/ad;IILcom/google/android/youtube/core/utils/p;Lcom/google/android/youtube/app/player/i;)V
    .locals 5

    const/4 v1, 0x1

    const/4 v2, 0x0

    invoke-direct {p0, p1}, Lcom/google/android/youtube/core/player/l;-><init>(Lcom/google/android/youtube/core/player/ad;)V

    new-instance v0, Lcom/google/android/youtube/app/player/g;

    invoke-direct {v0, p0}, Lcom/google/android/youtube/app/player/g;-><init>(Lcom/google/android/youtube/app/player/a;)V

    iput-object v0, p0, Lcom/google/android/youtube/app/player/a;->x:Ljava/lang/Runnable;

    if-lez p2, :cond_0

    move v0, v1

    :goto_0
    const-string v3, "bufferLowMillis must be > 0"

    invoke-static {v0, v3}, Lcom/google/android/youtube/core/utils/s;->a(ZLjava/lang/Object;)V

    if-lez p3, :cond_1

    :goto_1
    const-string v0, "bufferFullMillis must be > 0"

    invoke-static {v1, v0}, Lcom/google/android/youtube/core/utils/s;->a(ZLjava/lang/Object;)V

    const-string v0, "errorListener cannot be null"

    invoke-static {p5, v0}, Lcom/google/android/youtube/core/utils/s;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/youtube/app/player/i;

    iput-object v0, p0, Lcom/google/android/youtube/app/player/a;->w:Lcom/google/android/youtube/app/player/i;

    const-string v0, "networkStatus cannot be null"

    invoke-static {p4, v0}, Lcom/google/android/youtube/core/utils/s;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/youtube/core/utils/p;

    iput-object v0, p0, Lcom/google/android/youtube/app/player/a;->c:Lcom/google/android/youtube/core/utils/p;

    iput p2, p0, Lcom/google/android/youtube/app/player/a;->u:I

    iput p3, p0, Lcom/google/android/youtube/app/player/a;->v:I

    new-instance v0, Ljava/util/concurrent/atomic/AtomicReference;

    invoke-direct {v0}, Ljava/util/concurrent/atomic/AtomicReference;-><init>()V

    iput-object v0, p0, Lcom/google/android/youtube/app/player/a;->f:Ljava/util/concurrent/atomic/AtomicReference;

    new-instance v0, Ljava/util/concurrent/atomic/AtomicLong;

    const-wide/32 v3, 0x40000

    invoke-direct {v0, v3, v4}, Ljava/util/concurrent/atomic/AtomicLong;-><init>(J)V

    iput-object v0, p0, Lcom/google/android/youtube/app/player/a;->n:Ljava/util/concurrent/atomic/AtomicLong;

    new-instance v0, Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-direct {v0, v2}, Ljava/util/concurrent/atomic/AtomicBoolean;-><init>(Z)V

    iput-object v0, p0, Lcom/google/android/youtube/app/player/a;->o:Ljava/util/concurrent/atomic/AtomicBoolean;

    new-instance v0, Ljava/util/concurrent/atomic/AtomicReference;

    invoke-direct {v0}, Ljava/util/concurrent/atomic/AtomicReference;-><init>()V

    iput-object v0, p0, Lcom/google/android/youtube/app/player/a;->p:Ljava/util/concurrent/atomic/AtomicReference;

    new-instance v0, Lcom/google/android/youtube/core/utils/z;

    invoke-direct {v0}, Lcom/google/android/youtube/core/utils/z;-><init>()V

    iput-object v0, p0, Lcom/google/android/youtube/app/player/a;->q:Lcom/google/android/youtube/core/utils/z;

    new-instance v0, Lcom/google/android/youtube/app/player/h;

    invoke-direct {v0, p0, v2}, Lcom/google/android/youtube/app/player/h;-><init>(Lcom/google/android/youtube/app/player/a;B)V

    iput-object v0, p0, Lcom/google/android/youtube/app/player/a;->s:Lcom/google/android/youtube/app/player/h;

    const-string v0, ".*Awful.*"

    invoke-static {v0}, Lcom/google/android/youtube/core/L;->d(Ljava/lang/String;)V

    return-void

    :cond_0
    move v0, v2

    goto :goto_0

    :cond_1
    move v1, v2

    goto :goto_1
.end method

.method static synthetic a(Lcom/google/android/youtube/app/player/a;I)J
    .locals 2

    invoke-direct {p0, p1}, Lcom/google/android/youtube/app/player/a;->f(I)J

    move-result-wide v0

    return-wide v0
.end method

.method private a(J)V
    .locals 12

    const/4 v9, 0x1

    invoke-direct {p0}, Lcom/google/android/youtube/app/player/a;->r()V

    iput-wide p1, p0, Lcom/google/android/youtube/app/player/a;->r:J

    iget-wide v0, p0, Lcom/google/android/youtube/app/player/a;->h:J

    const-wide/16 v2, -0x1

    cmp-long v0, v0, v2

    if-eqz v0, :cond_0

    iget-wide v0, p0, Lcom/google/android/youtube/app/player/a;->h:J

    const-wide/16 v2, 0x1

    sub-long/2addr v0, v2

    cmp-long v0, p1, v0

    if-gez v0, :cond_1

    :cond_0
    new-instance v0, Lcom/google/android/youtube/core/transfer/b;

    iget-object v1, p0, Lcom/google/android/youtube/app/player/a;->e:Landroid/net/Uri;

    invoke-virtual {v1}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/youtube/app/player/a;->f:Ljava/util/concurrent/atomic/AtomicReference;

    invoke-virtual {v2}, Ljava/util/concurrent/atomic/AtomicReference;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    const-wide/16 v5, 0x0

    const/high16 v8, 0x20000

    const/4 v11, 0x0

    move-wide v3, p1

    move-object v7, p0

    move v10, v9

    invoke-direct/range {v0 .. v11}, Lcom/google/android/youtube/core/transfer/b;-><init>(Ljava/lang/String;Ljava/lang/String;JJLcom/google/android/youtube/core/transfer/n;IZZLcom/google/android/youtube/core/transfer/e;)V

    iput-object v0, p0, Lcom/google/android/youtube/app/player/a;->i:Lcom/google/android/youtube/core/transfer/b;

    new-instance v0, Ljava/lang/Thread;

    iget-object v1, p0, Lcom/google/android/youtube/app/player/a;->i:Lcom/google/android/youtube/core/transfer/b;

    invoke-direct {v0, v1}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;)V

    invoke-virtual {v0}, Ljava/lang/Thread;->start()V

    :cond_1
    return-void
.end method

.method static synthetic a(Lcom/google/android/youtube/app/player/a;)V
    .locals 0

    invoke-direct {p0}, Lcom/google/android/youtube/app/player/a;->v()V

    return-void
.end method

.method static synthetic a(Lcom/google/android/youtube/app/player/a;J)V
    .locals 9

    const/4 v8, 0x1

    const-wide/16 v6, 0x1

    iget-object v0, p0, Lcom/google/android/youtube/app/player/a;->q:Lcom/google/android/youtube/core/utils/z;

    iget-wide v1, p0, Lcom/google/android/youtube/app/player/a;->r:J

    invoke-virtual {v0, v1, v2, p1, p2}, Lcom/google/android/youtube/core/utils/z;->a(JJ)J

    move-result-wide v0

    iput-wide v0, p0, Lcom/google/android/youtube/app/player/a;->r:J

    cmp-long v2, v0, p1

    if-lez v2, :cond_0

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "filled gap, downloading from "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    add-long v3, v0, v6

    invoke-virtual {v2, v3, v4}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    invoke-static {}, Lcom/google/android/youtube/core/L;->d()V

    add-long v2, v0, v6

    invoke-direct {p0, v2, v3}, Lcom/google/android/youtube/app/player/a;->a(J)V

    :cond_0
    const-wide/16 v2, 0x64

    mul-long/2addr v2, v0

    iget-wide v4, p0, Lcom/google/android/youtube/app/player/a;->h:J

    sub-long/2addr v4, v6

    div-long/2addr v2, v4

    long-to-int v2, v2

    invoke-virtual {p0, v2}, Lcom/google/android/youtube/app/player/a;->e(I)V

    iget-boolean v2, p0, Lcom/google/android/youtube/app/player/a;->j:Z

    if-eqz v2, :cond_4

    iget-object v2, p0, Lcom/google/android/youtube/app/player/a;->o:Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-virtual {v2}, Ljava/util/concurrent/atomic/AtomicBoolean;->get()Z

    move-result v2

    if-nez v2, :cond_2

    iget-object v2, p0, Lcom/google/android/youtube/app/player/a;->n:Ljava/util/concurrent/atomic/AtomicLong;

    invoke-virtual {v2}, Ljava/util/concurrent/atomic/AtomicLong;->get()J

    move-result-wide v2

    cmp-long v2, v0, v2

    if-gtz v2, :cond_1

    iget-wide v2, p0, Lcom/google/android/youtube/app/player/a;->h:J

    sub-long/2addr v2, v6

    cmp-long v2, v0, v2

    if-ltz v2, :cond_2

    :cond_1
    iget-object v2, p0, Lcom/google/android/youtube/app/player/a;->p:Ljava/util/concurrent/atomic/AtomicReference;

    invoke-virtual {v2}, Ljava/util/concurrent/atomic/AtomicReference;->get()Ljava/lang/Object;

    move-result-object v2

    if-nez v2, :cond_3

    iget-object v0, p0, Lcom/google/android/youtube/app/player/a;->o:Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-virtual {v0, v8}, Ljava/util/concurrent/atomic/AtomicBoolean;->set(Z)V

    new-instance v0, Lcom/google/android/youtube/app/player/f;

    invoke-direct {v0, p0}, Lcom/google/android/youtube/app/player/f;-><init>(Lcom/google/android/youtube/app/player/a;)V

    invoke-virtual {v0}, Lcom/google/android/youtube/app/player/f;->start()V

    :cond_2
    :goto_0
    return-void

    :cond_3
    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "start buffer full "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    invoke-static {}, Lcom/google/android/youtube/core/L;->b()V

    iget-object v0, p0, Lcom/google/android/youtube/app/player/a;->t:Landroid/os/Handler;

    invoke-virtual {v0, v8}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    goto :goto_0

    :cond_4
    iget-boolean v2, p0, Lcom/google/android/youtube/app/player/a;->k:Z

    if-eqz v2, :cond_7

    invoke-virtual {p0}, Lcom/google/android/youtube/app/player/a;->g()I

    move-result v2

    iget v3, p0, Lcom/google/android/youtube/app/player/a;->v:I

    add-int/2addr v2, v3

    invoke-direct {p0, v2}, Lcom/google/android/youtube/app/player/a;->f(I)J

    move-result-wide v2

    cmp-long v2, v0, v2

    if-gtz v2, :cond_5

    iget-wide v2, p0, Lcom/google/android/youtube/app/player/a;->h:J

    sub-long/2addr v2, v6

    cmp-long v2, v0, v2

    if-ltz v2, :cond_2

    :cond_5
    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "full buffer at "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    invoke-static {}, Lcom/google/android/youtube/core/L;->b()V

    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/google/android/youtube/app/player/a;->b(Z)V

    iget v0, p0, Lcom/google/android/youtube/app/player/a;->m:I

    if-ltz v0, :cond_6

    iget v0, p0, Lcom/google/android/youtube/app/player/a;->m:I

    invoke-super {p0, v0}, Lcom/google/android/youtube/core/player/l;->a(I)V

    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/youtube/app/player/a;->m:I

    :cond_6
    iget-boolean v0, p0, Lcom/google/android/youtube/app/player/a;->l:Z

    if-nez v0, :cond_2

    invoke-direct {p0}, Lcom/google/android/youtube/app/player/a;->t()V

    goto :goto_0

    :cond_7
    iget-object v2, p0, Lcom/google/android/youtube/app/player/a;->s:Lcom/google/android/youtube/app/player/h;

    invoke-virtual {v2}, Lcom/google/android/youtube/app/player/h;->d()Z

    move-result v2

    if-eqz v2, :cond_2

    invoke-virtual {p0}, Lcom/google/android/youtube/app/player/a;->g()I

    move-result v2

    const v3, 0x15f90

    add-int/2addr v2, v3

    invoke-direct {p0, v2}, Lcom/google/android/youtube/app/player/a;->f(I)J

    move-result-wide v2

    cmp-long v0, v0, v2

    if-lez v0, :cond_2

    invoke-static {}, Lcom/google/android/youtube/core/L;->b()V

    invoke-direct {p0}, Lcom/google/android/youtube/app/player/a;->r()V

    goto :goto_0
.end method

.method static synthetic b(Lcom/google/android/youtube/app/player/a;)V
    .locals 7

    const/4 v6, 0x1

    invoke-virtual {p0}, Lcom/google/android/youtube/app/player/a;->g()I

    move-result v0

    iget-object v1, p0, Lcom/google/android/youtube/app/player/a;->i:Lcom/google/android/youtube/core/transfer/b;

    if-nez v1, :cond_1

    add-int/lit16 v0, v0, 0x7530

    invoke-direct {p0, v0}, Lcom/google/android/youtube/app/player/a;->f(I)J

    move-result-wide v0

    iget-wide v2, p0, Lcom/google/android/youtube/app/player/a;->r:J

    cmp-long v0, v2, v0

    if-gez v0, :cond_0

    invoke-static {}, Lcom/google/android/youtube/core/L;->b()V

    iget-wide v0, p0, Lcom/google/android/youtube/app/player/a;->r:J

    invoke-direct {p0, v0, v1}, Lcom/google/android/youtube/app/player/a;->a(J)V

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget v1, p0, Lcom/google/android/youtube/app/player/a;->u:I

    add-int/2addr v0, v1

    invoke-direct {p0, v0}, Lcom/google/android/youtube/app/player/a;->f(I)J

    move-result-wide v0

    iget-wide v2, p0, Lcom/google/android/youtube/app/player/a;->r:J

    cmp-long v0, v2, v0

    if-gez v0, :cond_0

    iget-wide v0, p0, Lcom/google/android/youtube/app/player/a;->r:J

    iget-wide v2, p0, Lcom/google/android/youtube/app/player/a;->h:J

    const-wide/16 v4, 0x1

    sub-long/2addr v2, v4

    cmp-long v0, v0, v2

    if-gez v0, :cond_0

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "low buffer at "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-wide v1, p0, Lcom/google/android/youtube/app/player/a;->r:J

    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    invoke-static {}, Lcom/google/android/youtube/core/L;->b()V

    invoke-direct {p0, v6}, Lcom/google/android/youtube/app/player/a;->b(Z)V

    invoke-super {p0}, Lcom/google/android/youtube/core/player/l;->f()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/youtube/app/player/a;->s:Lcom/google/android/youtube/app/player/h;

    invoke-virtual {v0}, Lcom/google/android/youtube/app/player/h;->c()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-direct {p0}, Lcom/google/android/youtube/app/player/a;->u()V

    goto :goto_0

    :cond_2
    iget-object v0, p0, Lcom/google/android/youtube/app/player/a;->t:Landroid/os/Handler;

    const/4 v1, 0x2

    const/16 v2, -0xfa0

    invoke-static {v0, v1, v6, v2}, Landroid/os/Message;->obtain(Landroid/os/Handler;III)Landroid/os/Message;

    move-result-object v0

    invoke-virtual {v0}, Landroid/os/Message;->sendToTarget()V

    goto :goto_0
.end method

.method static synthetic b(Lcom/google/android/youtube/app/player/a;J)V
    .locals 0

    invoke-direct {p0, p1, p2}, Lcom/google/android/youtube/app/player/a;->a(J)V

    return-void
.end method

.method private b(Z)V
    .locals 2

    iput-boolean p1, p0, Lcom/google/android/youtube/app/player/a;->k:Z

    if-eqz p1, :cond_0

    const/16 v0, 0x2bd

    :goto_0
    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, Lcom/google/android/youtube/app/player/a;->a(II)Z

    return-void

    :cond_0
    const/16 v0, 0x2be

    goto :goto_0
.end method

.method static synthetic c(Lcom/google/android/youtube/app/player/a;)Ljava/util/concurrent/atomic/AtomicReference;
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/app/player/a;->f:Ljava/util/concurrent/atomic/AtomicReference;

    return-object v0
.end method

.method static synthetic d(Lcom/google/android/youtube/app/player/a;)Ljava/util/concurrent/atomic/AtomicReference;
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/app/player/a;->p:Ljava/util/concurrent/atomic/AtomicReference;

    return-object v0
.end method

.method static synthetic e(Lcom/google/android/youtube/app/player/a;)I
    .locals 1

    iget v0, p0, Lcom/google/android/youtube/app/player/a;->v:I

    return v0
.end method

.method private f(I)J
    .locals 3

    iget-object v0, p0, Lcom/google/android/youtube/app/player/a;->p:Ljava/util/concurrent/atomic/AtomicReference;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/SortedMap;

    if-eqz v0, :cond_0

    div-int/lit16 v1, p1, 0x3e8

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/SortedMap;->headMap(Ljava/lang/Object;)Ljava/util/SortedMap;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/SortedMap;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_0

    invoke-interface {v1}, Ljava/util/SortedMap;->lastKey()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Integer;

    invoke-interface {v0, v1}, Ljava/util/SortedMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    int-to-long v0, v0

    :goto_0
    return-wide v0

    :cond_0
    const-wide/16 v0, 0x0

    goto :goto_0
.end method

.method static synthetic f(Lcom/google/android/youtube/app/player/a;)Ljava/util/concurrent/atomic/AtomicLong;
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/app/player/a;->n:Ljava/util/concurrent/atomic/AtomicLong;

    return-object v0
.end method

.method static synthetic g(Lcom/google/android/youtube/app/player/a;)J
    .locals 2

    iget-wide v0, p0, Lcom/google/android/youtube/app/player/a;->r:J

    return-wide v0
.end method

.method static synthetic h(Lcom/google/android/youtube/app/player/a;)Landroid/os/Handler;
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/app/player/a;->t:Landroid/os/Handler;

    return-object v0
.end method

.method static synthetic i(Lcom/google/android/youtube/app/player/a;)Ljava/util/concurrent/atomic/AtomicBoolean;
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/app/player/a;->o:Ljava/util/concurrent/atomic/AtomicBoolean;

    return-object v0
.end method

.method static synthetic j(Lcom/google/android/youtube/app/player/a;)Landroid/content/Context;
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/app/player/a;->d:Landroid/content/Context;

    return-object v0
.end method

.method static synthetic k(Lcom/google/android/youtube/app/player/a;)Lcom/google/android/youtube/core/utils/p;
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/app/player/a;->c:Lcom/google/android/youtube/core/utils/p;

    return-object v0
.end method

.method static synthetic k()Ljava/lang/String;
    .locals 1

    sget-object v0, Lcom/google/android/youtube/app/player/a;->b:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic l(Lcom/google/android/youtube/app/player/a;)Lcom/google/android/youtube/core/transfer/b;
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/app/player/a;->i:Lcom/google/android/youtube/core/transfer/b;

    return-object v0
.end method

.method private p()V
    .locals 5

    const/4 v4, -0x1

    const/4 v3, 0x0

    const/4 v2, 0x0

    iget-object v0, p0, Lcom/google/android/youtube/app/player/a;->s:Lcom/google/android/youtube/app/player/h;

    invoke-virtual {v0}, Lcom/google/android/youtube/app/player/h;->b()V

    iput-object v3, p0, Lcom/google/android/youtube/app/player/a;->e:Landroid/net/Uri;

    iput v4, p0, Lcom/google/android/youtube/app/player/a;->g:I

    const-wide/16 v0, -0x1

    iput-wide v0, p0, Lcom/google/android/youtube/app/player/a;->h:J

    invoke-direct {p0}, Lcom/google/android/youtube/app/player/a;->r()V

    iget-object v0, p0, Lcom/google/android/youtube/app/player/a;->t:Landroid/os/Handler;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/youtube/app/player/a;->t:Landroid/os/Handler;

    invoke-virtual {v0, v3}, Landroid/os/Handler;->removeCallbacksAndMessages(Ljava/lang/Object;)V

    :cond_0
    iput-boolean v2, p0, Lcom/google/android/youtube/app/player/a;->j:Z

    iput-boolean v2, p0, Lcom/google/android/youtube/app/player/a;->k:Z

    iput v4, p0, Lcom/google/android/youtube/app/player/a;->m:I

    iget-object v0, p0, Lcom/google/android/youtube/app/player/a;->q:Lcom/google/android/youtube/core/utils/z;

    invoke-virtual {v0}, Lcom/google/android/youtube/core/utils/z;->a()V

    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/google/android/youtube/app/player/a;->r:J

    iget-object v0, p0, Lcom/google/android/youtube/app/player/a;->o:Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-virtual {v0, v2}, Ljava/util/concurrent/atomic/AtomicBoolean;->set(Z)V

    iget-object v0, p0, Lcom/google/android/youtube/app/player/a;->n:Ljava/util/concurrent/atomic/AtomicLong;

    const-wide/32 v1, 0x40000

    invoke-virtual {v0, v1, v2}, Ljava/util/concurrent/atomic/AtomicLong;->set(J)V

    iput-object v3, p0, Lcom/google/android/youtube/app/player/a;->d:Landroid/content/Context;

    return-void
.end method

.method private q()V
    .locals 3

    iget-object v0, p0, Lcom/google/android/youtube/app/player/a;->f:Ljava/util/concurrent/atomic/AtomicReference;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_0

    invoke-static {}, Lcom/google/android/youtube/core/L;->d()V

    iget-object v1, p0, Lcom/google/android/youtube/app/player/a;->f:Ljava/util/concurrent/atomic/AtomicReference;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Ljava/util/concurrent/atomic/AtomicReference;->set(Ljava/lang/Object;)V

    new-instance v1, Lcom/google/android/youtube/app/player/e;

    invoke-direct {v1, p0, v0}, Lcom/google/android/youtube/app/player/e;-><init>(Lcom/google/android/youtube/app/player/a;Ljava/lang/String;)V

    const/4 v0, 0x1

    invoke-virtual {v1, v0}, Ljava/lang/Thread;->setPriority(I)V

    invoke-virtual {v1}, Ljava/lang/Thread;->start()V

    :cond_0
    return-void
.end method

.method private r()V
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/app/player/a;->i:Lcom/google/android/youtube/core/transfer/b;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/youtube/app/player/a;->i:Lcom/google/android/youtube/core/transfer/b;

    invoke-virtual {v0}, Lcom/google/android/youtube/core/transfer/b;->a()V

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/youtube/app/player/a;->i:Lcom/google/android/youtube/core/transfer/b;

    :cond_0
    return-void
.end method

.method private s()V
    .locals 5

    iget v0, p0, Lcom/google/android/youtube/app/player/a;->m:I

    invoke-direct {p0, v0}, Lcom/google/android/youtube/app/player/a;->f(I)J

    move-result-wide v0

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "seek offset "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    invoke-static {}, Lcom/google/android/youtube/core/L;->d()V

    iget-object v2, p0, Lcom/google/android/youtube/app/player/a;->q:Lcom/google/android/youtube/core/utils/z;

    invoke-virtual {v2, v0, v1}, Lcom/google/android/youtube/core/utils/z;->a(J)J

    move-result-wide v1

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v3, "next gap at "

    invoke-direct {v0, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    invoke-static {}, Lcom/google/android/youtube/core/L;->d()V

    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/google/android/youtube/app/player/a;->b(Z)V

    const-wide/16 v3, 0x1

    sub-long v3, v1, v3

    iput-wide v3, p0, Lcom/google/android/youtube/app/player/a;->r:J

    invoke-direct {p0}, Lcom/google/android/youtube/app/player/a;->r()V

    iget-object v0, p0, Lcom/google/android/youtube/app/player/a;->f:Ljava/util/concurrent/atomic/AtomicReference;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iget-wide v3, p0, Lcom/google/android/youtube/app/player/a;->r:J

    invoke-virtual {p0, v0, v3, v4}, Lcom/google/android/youtube/app/player/a;->b(Ljava/lang/String;J)V

    invoke-direct {p0, v1, v2}, Lcom/google/android/youtube/app/player/a;->a(J)V

    return-void
.end method

.method private t()V
    .locals 0

    invoke-super {p0}, Lcom/google/android/youtube/core/player/l;->e()V

    invoke-direct {p0}, Lcom/google/android/youtube/app/player/a;->v()V

    return-void
.end method

.method private u()V
    .locals 2

    iget-object v0, p0, Lcom/google/android/youtube/app/player/a;->t:Landroid/os/Handler;

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeMessages(I)V

    invoke-super {p0}, Lcom/google/android/youtube/core/player/l;->d()V

    return-void
.end method

.method private v()V
    .locals 4

    iget-object v0, p0, Lcom/google/android/youtube/app/player/a;->t:Landroid/os/Handler;

    iget-object v1, p0, Lcom/google/android/youtube/app/player/a;->t:Landroid/os/Handler;

    const/4 v2, 0x4

    invoke-static {v1, v2}, Landroid/os/Message;->obtain(Landroid/os/Handler;I)Landroid/os/Message;

    move-result-object v1

    const-wide/16 v2, 0x3e8

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->sendMessageDelayed(Landroid/os/Message;J)Z

    return-void
.end method


# virtual methods
.method public final a()V
    .locals 0

    invoke-direct {p0}, Lcom/google/android/youtube/app/player/a;->p()V

    invoke-super {p0}, Lcom/google/android/youtube/core/player/l;->a()V

    invoke-direct {p0}, Lcom/google/android/youtube/app/player/a;->q()V

    return-void
.end method

.method public final a(I)V
    .locals 2

    iget v0, p0, Lcom/google/android/youtube/app/player/a;->m:I

    if-eq p1, v0, :cond_0

    iput p1, p0, Lcom/google/android/youtube/app/player/a;->m:I

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "seek position "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    invoke-static {}, Lcom/google/android/youtube/core/L;->b()V

    iget-boolean v0, p0, Lcom/google/android/youtube/app/player/a;->j:Z

    if-nez v0, :cond_0

    iget v0, p0, Lcom/google/android/youtube/app/player/a;->g:I

    if-lez v0, :cond_0

    invoke-direct {p0}, Lcom/google/android/youtube/app/player/a;->u()V

    invoke-direct {p0}, Lcom/google/android/youtube/app/player/a;->s()V

    :cond_0
    return-void
.end method

.method public final a(Landroid/content/Context;Landroid/net/Uri;)V
    .locals 2

    invoke-direct {p0}, Lcom/google/android/youtube/app/player/a;->p()V

    invoke-direct {p0}, Lcom/google/android/youtube/app/player/a;->q()V

    iput-object p1, p0, Lcom/google/android/youtube/app/player/a;->d:Landroid/content/Context;

    iput-object p2, p0, Lcom/google/android/youtube/app/player/a;->e:Landroid/net/Uri;

    new-instance v0, Lcom/google/android/youtube/app/player/d;

    invoke-virtual {p1}, Landroid/content/Context;->getMainLooper()Landroid/os/Looper;

    move-result-object v1

    invoke-direct {v0, p0, v1}, Lcom/google/android/youtube/app/player/d;-><init>(Lcom/google/android/youtube/app/player/a;Landroid/os/Looper;)V

    iput-object v0, p0, Lcom/google/android/youtube/app/player/a;->t:Landroid/os/Handler;

    return-void
.end method

.method public final a(Landroid/content/Context;Landroid/net/Uri;Ljava/util/Map;)V
    .locals 0

    invoke-virtual {p0, p1, p2}, Lcom/google/android/youtube/app/player/a;->a(Landroid/content/Context;Landroid/net/Uri;)V

    return-void
.end method

.method public final a(Lcom/google/android/youtube/core/player/ad;)V
    .locals 0

    return-void
.end method

.method public final a(Ljava/lang/String;J)V
    .locals 2

    iput-wide p2, p0, Lcom/google/android/youtube/app/player/a;->h:J

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "size is "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p2, p3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    invoke-static {}, Lcom/google/android/youtube/core/L;->d()V

    const-wide/32 v0, 0x6000000

    cmp-long v0, p2, v0

    if-lez v0, :cond_0

    const/4 v0, 0x1

    const/16 v1, -0xfa3

    invoke-virtual {p0, p0, v0, v1}, Lcom/google/android/youtube/app/player/a;->b(Lcom/google/android/youtube/core/player/ad;II)Z

    :cond_0
    return-void
.end method

.method public final a(Ljava/lang/String;Lcom/google/android/youtube/core/transfer/TransferException;)V
    .locals 8

    const/4 v7, 0x2

    const/16 v6, -0xfa0

    const/4 v1, 0x1

    const-string v0, "download error"

    invoke-static {v0, p2}, Lcom/google/android/youtube/core/L;->b(Ljava/lang/String;Ljava/lang/Throwable;)V

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/youtube/app/player/a;->i:Lcom/google/android/youtube/core/transfer/b;

    iget-object v0, p0, Lcom/google/android/youtube/app/player/a;->s:Lcom/google/android/youtube/app/player/h;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/google/android/youtube/app/player/a;->s:Lcom/google/android/youtube/app/player/h;

    invoke-virtual {v0}, Lcom/google/android/youtube/app/player/h;->c()Z

    move-result v0

    if-eqz v0, :cond_2

    move v0, v1

    :goto_0
    iget-boolean v2, p2, Lcom/google/android/youtube/core/transfer/TransferException;->fatal:Z

    if-nez v2, :cond_0

    if-eqz v0, :cond_4

    :cond_0
    invoke-virtual {p2}, Lcom/google/android/youtube/core/transfer/TransferException;->getCause()Ljava/lang/Throwable;

    move-result-object v0

    instance-of v0, v0, Ljava/io/IOException;

    if-eqz v0, :cond_3

    new-instance v0, Landroid/os/StatFs;

    iget-object v2, p0, Lcom/google/android/youtube/app/player/a;->d:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getFilesDir()Ljava/io/File;

    move-result-object v2

    invoke-virtual {v2}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v0, v2}, Landroid/os/StatFs;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Landroid/os/StatFs;->getAvailableBlocks()I

    move-result v2

    int-to-long v2, v2

    invoke-virtual {v0}, Landroid/os/StatFs;->getBlockSize()I

    move-result v0

    int-to-long v4, v0

    mul-long/2addr v2, v4

    const-wide/32 v4, 0x20000

    cmp-long v0, v2, v4

    if-gez v0, :cond_3

    const/16 v0, -0xfa2

    invoke-virtual {p0, p0, v1, v0}, Lcom/google/android/youtube/app/player/a;->b(Lcom/google/android/youtube/core/player/ad;II)Z

    :cond_1
    :goto_1
    return-void

    :cond_2
    const/4 v0, 0x0

    goto :goto_0

    :cond_3
    iget-object v0, p0, Lcom/google/android/youtube/app/player/a;->t:Landroid/os/Handler;

    invoke-static {v0, v7, v1, v6}, Landroid/os/Message;->obtain(Landroid/os/Handler;III)Landroid/os/Message;

    move-result-object v0

    invoke-virtual {v0}, Landroid/os/Message;->sendToTarget()V

    goto :goto_1

    :cond_4
    iget-boolean v0, p0, Lcom/google/android/youtube/app/player/a;->j:Z

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/youtube/app/player/a;->t:Landroid/os/Handler;

    invoke-static {v0, v7, v1, v6}, Landroid/os/Message;->obtain(Landroid/os/Handler;III)Landroid/os/Message;

    move-result-object v0

    invoke-virtual {v0}, Landroid/os/Message;->sendToTarget()V

    goto :goto_1
.end method

.method public final a(Ljava/lang/String;Lcom/google/android/youtube/core/transfer/d;)V
    .locals 5

    iget-object v0, p0, Lcom/google/android/youtube/app/player/a;->t:Landroid/os/Handler;

    iget-object v1, p0, Lcom/google/android/youtube/app/player/a;->t:Landroid/os/Handler;

    const/4 v2, 0x3

    iget-wide v3, p0, Lcom/google/android/youtube/app/player/a;->h:J

    invoke-static {v3, v4}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    invoke-static {v1, v2, v3}, Landroid/os/Message;->obtain(Landroid/os/Handler;ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    return-void
.end method

.method public final a(Lcom/google/android/youtube/core/player/ad;II)Z
    .locals 5

    const/4 v4, 0x0

    const/4 v0, 0x1

    if-ne p2, v0, :cond_0

    const/16 v0, 0x10

    if-ne p3, v0, :cond_0

    iget-object v0, p0, Lcom/google/android/youtube/app/player/a;->t:Landroid/os/Handler;

    iget-object v1, p0, Lcom/google/android/youtube/app/player/a;->x:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    iget-object v0, p0, Lcom/google/android/youtube/app/player/a;->t:Landroid/os/Handler;

    iget-object v1, p0, Lcom/google/android/youtube/app/player/a;->x:Ljava/lang/Runnable;

    const-wide/16 v2, 0xfa

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    :goto_0
    return v4

    :cond_0
    invoke-super {p0, p1, p2, p3}, Lcom/google/android/youtube/core/player/l;->a(Lcom/google/android/youtube/core/player/ad;II)Z

    goto :goto_0
.end method

.method public final b()V
    .locals 1

    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public final b(Ljava/lang/String;J)V
    .locals 4

    iget-object v0, p0, Lcom/google/android/youtube/app/player/a;->t:Landroid/os/Handler;

    iget-object v1, p0, Lcom/google/android/youtube/app/player/a;->t:Landroid/os/Handler;

    const/4 v2, 0x3

    invoke-static {p2, p3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    invoke-static {v1, v2, v3}, Landroid/os/Message;->obtain(Landroid/os/Handler;ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    return-void
.end method

.method public final b(Lcom/google/android/youtube/core/player/ad;II)Z
    .locals 1

    invoke-direct {p0}, Lcom/google/android/youtube/app/player/a;->p()V

    invoke-direct {p0}, Lcom/google/android/youtube/app/player/a;->q()V

    iget-object v0, p0, Lcom/google/android/youtube/app/player/a;->w:Lcom/google/android/youtube/app/player/i;

    invoke-interface {v0}, Lcom/google/android/youtube/app/player/i;->a()V

    invoke-super {p0, p1, p2, p3}, Lcom/google/android/youtube/core/player/l;->b(Lcom/google/android/youtube/core/player/ad;II)Z

    const/4 v0, 0x0

    return v0
.end method

.method public final c()V
    .locals 5

    const/4 v4, 0x2

    const/4 v3, 0x1

    iget-object v0, p0, Lcom/google/android/youtube/app/player/a;->e:Landroid/net/Uri;

    invoke-static {v0}, Lcom/google/android/youtube/core/utils/s;->a(Ljava/lang/Object;)Ljava/lang/Object;

    :try_start_0
    sget-object v0, Lcom/google/android/youtube/app/player/a;->b:Ljava/lang/String;

    const-string v1, ".tmp"

    const/4 v2, 0x0

    invoke-static {v0, v1, v2}, Ljava/io/File;->createTempFile(Ljava/lang/String;Ljava/lang/String;Ljava/io/File;)Ljava/io/File;

    move-result-object v0

    invoke-virtual {v0}, Ljava/io/File;->deleteOnExit()V

    iget-object v1, p0, Lcom/google/android/youtube/app/player/a;->f:Ljava/util/concurrent/atomic/AtomicReference;

    invoke-virtual {v0}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/util/concurrent/atomic/AtomicReference;->set(Ljava/lang/Object;)V

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "created file buffer "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/google/android/youtube/app/player/a;->f:Ljava/util/concurrent/atomic/AtomicReference;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    invoke-static {}, Lcom/google/android/youtube/core/L;->d()V

    iget-object v0, p0, Lcom/google/android/youtube/app/player/a;->f:Ljava/util/concurrent/atomic/AtomicReference;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-super {p0, v0}, Lcom/google/android/youtube/core/player/l;->a(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1

    iput-boolean v3, p0, Lcom/google/android/youtube/app/player/a;->j:Z

    iget-object v0, p0, Lcom/google/android/youtube/app/player/a;->s:Lcom/google/android/youtube/app/player/h;

    invoke-virtual {v0}, Lcom/google/android/youtube/app/player/h;->a()V

    :goto_0
    return-void

    :catch_0
    move-exception v0

    iget-object v0, p0, Lcom/google/android/youtube/app/player/a;->t:Landroid/os/Handler;

    const/16 v1, -0xfa2

    invoke-static {v0, v4, v3, v1}, Landroid/os/Message;->obtain(Landroid/os/Handler;III)Landroid/os/Message;

    move-result-object v0

    invoke-virtual {v0}, Landroid/os/Message;->sendToTarget()V

    goto :goto_0

    :catch_1
    move-exception v0

    iget-object v0, p0, Lcom/google/android/youtube/app/player/a;->t:Landroid/os/Handler;

    const/16 v1, -0xfa1

    invoke-static {v0, v4, v3, v1}, Landroid/os/Message;->obtain(Landroid/os/Handler;III)Landroid/os/Message;

    move-result-object v0

    invoke-virtual {v0}, Landroid/os/Message;->sendToTarget()V

    goto :goto_0
.end method

.method public final d()V
    .locals 1

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/youtube/app/player/a;->l:Z

    iget-boolean v0, p0, Lcom/google/android/youtube/app/player/a;->j:Z

    if-nez v0, :cond_0

    iget-boolean v0, p0, Lcom/google/android/youtube/app/player/a;->k:Z

    if-nez v0, :cond_0

    invoke-direct {p0}, Lcom/google/android/youtube/app/player/a;->u()V

    :cond_0
    return-void
.end method

.method public final e()V
    .locals 1

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/youtube/app/player/a;->l:Z

    iget-boolean v0, p0, Lcom/google/android/youtube/app/player/a;->j:Z

    if-nez v0, :cond_0

    iget-boolean v0, p0, Lcom/google/android/youtube/app/player/a;->k:Z

    if-nez v0, :cond_0

    invoke-direct {p0}, Lcom/google/android/youtube/app/player/a;->t()V

    :cond_0
    return-void
.end method

.method public final f()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/youtube/app/player/a;->l:Z

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final g()I
    .locals 1

    iget v0, p0, Lcom/google/android/youtube/app/player/a;->m:I

    if-ltz v0, :cond_0

    iget v0, p0, Lcom/google/android/youtube/app/player/a;->m:I

    :goto_0
    return v0

    :cond_0
    :try_start_0
    invoke-super {p0}, Lcom/google/android/youtube/core/player/l;->g()I
    :try_end_0
    .catch Ljava/lang/IllegalStateException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v0

    goto :goto_0

    :catch_0
    move-exception v0

    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final h()V
    .locals 4

    const/4 v3, 0x1

    const/4 v2, 0x0

    const/16 v1, -0xfa1

    :try_start_0
    invoke-super {p0}, Lcom/google/android/youtube/core/player/l;->b()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    :try_start_1
    invoke-virtual {p0}, Lcom/google/android/youtube/app/player/a;->l()I

    move-result v0

    iput v0, p0, Lcom/google/android/youtube/app/player/a;->g:I
    :try_end_1
    .catch Ljava/lang/IllegalStateException; {:try_start_1 .. :try_end_1} :catch_1

    iget-object v0, p0, Lcom/google/android/youtube/app/player/a;->o:Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-virtual {v0, v2}, Ljava/util/concurrent/atomic/AtomicBoolean;->set(Z)V

    iput-boolean v2, p0, Lcom/google/android/youtube/app/player/a;->j:Z

    invoke-virtual {p0}, Lcom/google/android/youtube/app/player/a;->o()V

    iget v0, p0, Lcom/google/android/youtube/app/player/a;->m:I

    if-ltz v0, :cond_0

    invoke-direct {p0}, Lcom/google/android/youtube/app/player/a;->s()V

    :goto_0
    return-void

    :catch_0
    move-exception v0

    invoke-virtual {p0, p0, v3, v1}, Lcom/google/android/youtube/app/player/a;->b(Lcom/google/android/youtube/core/player/ad;II)Z

    goto :goto_0

    :catch_1
    move-exception v0

    invoke-virtual {p0}, Lcom/google/android/youtube/app/player/a;->a()V

    invoke-virtual {p0, p0, v3, v1}, Lcom/google/android/youtube/app/player/a;->b(Lcom/google/android/youtube/core/player/ad;II)Z

    goto :goto_0

    :cond_0
    iget-boolean v0, p0, Lcom/google/android/youtube/app/player/a;->l:Z

    if-eqz v0, :cond_1

    invoke-direct {p0}, Lcom/google/android/youtube/app/player/a;->u()V

    goto :goto_0

    :cond_1
    invoke-direct {p0}, Lcom/google/android/youtube/app/player/a;->t()V

    goto :goto_0
.end method

.method public final i()Ljava/util/Set;
    .locals 1

    sget-object v0, Lcom/google/android/youtube/app/player/a;->a:Ljava/util/Set;

    return-object v0
.end method

.method public final j()Z
    .locals 1

    const/4 v0, 0x0

    return v0
.end method
