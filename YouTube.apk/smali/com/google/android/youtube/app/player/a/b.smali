.class final Lcom/google/android/youtube/app/player/a/b;
.super Lorg/apache/http/entity/AbstractHttpEntity;
.source "SourceFile"


# instance fields
.field private final a:Ljava/io/File;

.field private final b:J

.field private final c:J

.field private final d:Ljava/security/Key;


# direct methods
.method public constructor <init>(Ljava/io/File;Ljava/lang/String;JJLjava/security/Key;)V
    .locals 2

    invoke-direct {p0}, Lorg/apache/http/entity/AbstractHttpEntity;-><init>()V

    const-string v0, "file cannot be null"

    invoke-static {p1, v0}, Lcom/google/android/youtube/core/utils/s;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/io/File;

    iput-object v0, p0, Lcom/google/android/youtube/app/player/a/b;->a:Ljava/io/File;

    cmp-long v0, p3, p5

    if-gtz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    const-string v1, "begin must be less than or equal to end"

    invoke-static {v0, v1}, Lcom/google/android/youtube/core/utils/s;->a(ZLjava/lang/Object;)V

    iput-wide p3, p0, Lcom/google/android/youtube/app/player/a/b;->b:J

    iput-wide p5, p0, Lcom/google/android/youtube/app/player/a/b;->c:J

    iput-object p7, p0, Lcom/google/android/youtube/app/player/a/b;->d:Ljava/security/Key;

    const-string v0, "contentType cannot be empty"

    invoke-static {p2, v0}, Lcom/google/android/youtube/core/utils/s;->a(Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/android/youtube/app/player/a/b;->setContentType(Ljava/lang/String;)V

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public final getContent()Ljava/io/InputStream;
    .locals 7

    iget-object v0, p0, Lcom/google/android/youtube/app/player/a/b;->d:Ljava/security/Key;

    if-eqz v0, :cond_0

    new-instance v0, Lcom/google/android/youtube/app/player/a/a;

    iget-object v1, p0, Lcom/google/android/youtube/app/player/a/b;->a:Ljava/io/File;

    iget-wide v2, p0, Lcom/google/android/youtube/app/player/a/b;->b:J

    iget-wide v4, p0, Lcom/google/android/youtube/app/player/a/b;->c:J

    iget-object v6, p0, Lcom/google/android/youtube/app/player/a/b;->d:Ljava/security/Key;

    invoke-direct/range {v0 .. v6}, Lcom/google/android/youtube/app/player/a/a;-><init>(Ljava/io/File;JJLjava/security/Key;)V

    :goto_0
    return-object v0

    :cond_0
    new-instance v0, Lcom/google/android/youtube/app/player/a/c;

    iget-object v1, p0, Lcom/google/android/youtube/app/player/a/b;->a:Ljava/io/File;

    iget-wide v2, p0, Lcom/google/android/youtube/app/player/a/b;->b:J

    iget-wide v4, p0, Lcom/google/android/youtube/app/player/a/b;->c:J

    invoke-direct/range {v0 .. v5}, Lcom/google/android/youtube/app/player/a/c;-><init>(Ljava/io/File;JJ)V

    goto :goto_0
.end method

.method public final getContentLength()J
    .locals 4

    iget-wide v0, p0, Lcom/google/android/youtube/app/player/a/b;->c:J

    iget-wide v2, p0, Lcom/google/android/youtube/app/player/a/b;->b:J

    sub-long/2addr v0, v2

    const-wide/16 v2, 0x1

    add-long/2addr v0, v2

    return-wide v0
.end method

.method public final isRepeatable()Z
    .locals 1

    const/4 v0, 0x1

    return v0
.end method

.method public final isStreaming()Z
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public final writeTo(Ljava/io/OutputStream;)V
    .locals 2

    invoke-virtual {p0}, Lcom/google/android/youtube/app/player/a/b;->getContent()Ljava/io/InputStream;

    move-result-object v0

    :try_start_0
    invoke-static {v0, p1}, Lcom/google/android/youtube/core/utils/Util;->a(Ljava/io/InputStream;Ljava/io/OutputStream;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    invoke-virtual {v0}, Ljava/io/InputStream;->close()V

    return-void

    :catchall_0
    move-exception v1

    invoke-virtual {v0}, Ljava/io/InputStream;->close()V

    throw v1
.end method
