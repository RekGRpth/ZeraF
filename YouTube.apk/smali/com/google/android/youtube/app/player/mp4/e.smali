.class public final Lcom/google/android/youtube/app/player/mp4/e;
.super Ljava/lang/Object;
.source "SourceFile"


# direct methods
.method private static a(Lcom/google/android/youtube/app/player/mp4/c;Lcom/google/android/youtube/app/player/mp4/HdlrAtom$TrackType;)Lcom/google/android/youtube/app/player/mp4/c;
    .locals 5

    sget-object v0, Lcom/google/android/youtube/app/player/mp4/b;->b:Lcom/google/android/youtube/app/player/mp4/s;

    invoke-virtual {p0, v0}, Lcom/google/android/youtube/app/player/mp4/c;->c(Lcom/google/android/youtube/app/player/mp4/s;)I

    move-result v3

    const/4 v0, 0x0

    move v2, v0

    :goto_0
    if-ge v2, v3, :cond_1

    sget-object v0, Lcom/google/android/youtube/app/player/mp4/b;->b:Lcom/google/android/youtube/app/player/mp4/s;

    invoke-virtual {p0, v0, v2}, Lcom/google/android/youtube/app/player/mp4/c;->a(Lcom/google/android/youtube/app/player/mp4/s;I)Lcom/google/android/youtube/app/player/mp4/c;

    move-result-object v1

    sget-object v0, Lcom/google/android/youtube/app/player/mp4/b;->c:Lcom/google/android/youtube/app/player/mp4/s;

    invoke-virtual {v1, v0}, Lcom/google/android/youtube/app/player/mp4/c;->b(Lcom/google/android/youtube/app/player/mp4/s;)Lcom/google/android/youtube/app/player/mp4/c;

    move-result-object v0

    sget-object v4, Lcom/google/android/youtube/app/player/mp4/b;->d:Lcom/google/android/youtube/app/player/mp4/s;

    invoke-virtual {v0, v4}, Lcom/google/android/youtube/app/player/mp4/c;->a(Lcom/google/android/youtube/app/player/mp4/s;)Lcom/google/android/youtube/app/player/mp4/a;

    move-result-object v0

    check-cast v0, Lcom/google/android/youtube/app/player/mp4/HdlrAtom;

    invoke-virtual {v0}, Lcom/google/android/youtube/app/player/mp4/HdlrAtom;->e()Lcom/google/android/youtube/app/player/mp4/HdlrAtom$TrackType;

    move-result-object v0

    if-ne v0, p1, :cond_0

    move-object v0, v1

    :goto_1
    return-object v0

    :cond_0
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_0

    :cond_1
    const/4 v0, 0x0

    goto :goto_1
.end method

.method public static a(Ljava/io/InputStream;)Ljava/util/SortedMap;
    .locals 5

    new-instance v2, Ljava/io/DataInputStream;

    invoke-direct {v2, p0}, Ljava/io/DataInputStream;-><init>(Ljava/io/InputStream;)V

    const/4 v1, 0x0

    :goto_0
    if-nez v1, :cond_0

    invoke-static {v2}, Lcom/google/android/youtube/app/player/mp4/b;->a(Ljava/io/DataInputStream;)Lcom/google/android/youtube/app/player/mp4/a;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/youtube/app/player/mp4/a;->c()Lcom/google/android/youtube/app/player/mp4/s;

    move-result-object v3

    sget-object v4, Lcom/google/android/youtube/app/player/mp4/b;->a:Lcom/google/android/youtube/app/player/mp4/s;

    invoke-virtual {v3, v4}, Lcom/google/android/youtube/app/player/mp4/s;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    check-cast v0, Lcom/google/android/youtube/app/player/mp4/c;

    :goto_1
    move-object v1, v0

    goto :goto_0

    :cond_0
    invoke-virtual {v2}, Ljava/io/DataInputStream;->close()V

    sget-object v0, Lcom/google/android/youtube/app/player/mp4/HdlrAtom$TrackType;->TRACK_AUDIO:Lcom/google/android/youtube/app/player/mp4/HdlrAtom$TrackType;

    invoke-static {v1, v0}, Lcom/google/android/youtube/app/player/mp4/e;->a(Lcom/google/android/youtube/app/player/mp4/c;Lcom/google/android/youtube/app/player/mp4/HdlrAtom$TrackType;)Lcom/google/android/youtube/app/player/mp4/c;

    move-result-object v0

    sget-object v2, Lcom/google/android/youtube/app/player/mp4/HdlrAtom$TrackType;->TRACK_VIDEO:Lcom/google/android/youtube/app/player/mp4/HdlrAtom$TrackType;

    invoke-static {v1, v2}, Lcom/google/android/youtube/app/player/mp4/e;->a(Lcom/google/android/youtube/app/player/mp4/c;Lcom/google/android/youtube/app/player/mp4/HdlrAtom$TrackType;)Lcom/google/android/youtube/app/player/mp4/c;

    move-result-object v1

    if-eqz v1, :cond_1

    if-nez v0, :cond_2

    :cond_1
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "The input must contain an audio and a video track"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_2
    new-instance v2, Ljava/util/HashMap;

    invoke-direct {v2}, Ljava/util/HashMap;-><init>()V

    invoke-static {v1, v2}, Lcom/google/android/youtube/app/player/mp4/e;->a(Lcom/google/android/youtube/app/player/mp4/c;Ljava/util/Map;)V

    invoke-static {v0, v2}, Lcom/google/android/youtube/app/player/mp4/e;->a(Lcom/google/android/youtube/app/player/mp4/c;Ljava/util/Map;)V

    new-instance v0, Ljava/util/TreeMap;

    invoke-direct {v0, v2}, Ljava/util/TreeMap;-><init>(Ljava/util/Map;)V

    return-object v0

    :cond_3
    move-object v0, v1

    goto :goto_1
.end method

.method private static a(Lcom/google/android/youtube/app/player/mp4/c;Ljava/util/Map;)V
    .locals 8

    sget-object v0, Lcom/google/android/youtube/app/player/mp4/b;->c:Lcom/google/android/youtube/app/player/mp4/s;

    invoke-virtual {p0, v0}, Lcom/google/android/youtube/app/player/mp4/c;->b(Lcom/google/android/youtube/app/player/mp4/s;)Lcom/google/android/youtube/app/player/mp4/c;

    move-result-object v0

    sget-object v1, Lcom/google/android/youtube/app/player/mp4/b;->l:Lcom/google/android/youtube/app/player/mp4/s;

    invoke-virtual {v0, v1}, Lcom/google/android/youtube/app/player/mp4/c;->a(Lcom/google/android/youtube/app/player/mp4/s;)Lcom/google/android/youtube/app/player/mp4/a;

    move-result-object v0

    check-cast v0, Lcom/google/android/youtube/app/player/mp4/d;

    invoke-virtual {v0}, Lcom/google/android/youtube/app/player/mp4/d;->e()I

    move-result v7

    sget-object v0, Lcom/google/android/youtube/app/player/mp4/b;->c:Lcom/google/android/youtube/app/player/mp4/s;

    invoke-virtual {p0, v0}, Lcom/google/android/youtube/app/player/mp4/c;->b(Lcom/google/android/youtube/app/player/mp4/s;)Lcom/google/android/youtube/app/player/mp4/c;

    move-result-object v0

    sget-object v1, Lcom/google/android/youtube/app/player/mp4/b;->e:Lcom/google/android/youtube/app/player/mp4/s;

    invoke-virtual {v0, v1}, Lcom/google/android/youtube/app/player/mp4/c;->b(Lcom/google/android/youtube/app/player/mp4/s;)Lcom/google/android/youtube/app/player/mp4/c;

    move-result-object v0

    sget-object v1, Lcom/google/android/youtube/app/player/mp4/b;->f:Lcom/google/android/youtube/app/player/mp4/s;

    invoke-virtual {v0, v1}, Lcom/google/android/youtube/app/player/mp4/c;->b(Lcom/google/android/youtube/app/player/mp4/s;)Lcom/google/android/youtube/app/player/mp4/c;

    move-result-object v0

    sget-object v1, Lcom/google/android/youtube/app/player/mp4/b;->h:Lcom/google/android/youtube/app/player/mp4/s;

    invoke-virtual {v0, v1}, Lcom/google/android/youtube/app/player/mp4/c;->a(Lcom/google/android/youtube/app/player/mp4/s;)Lcom/google/android/youtube/app/player/mp4/a;

    move-result-object v4

    check-cast v4, Lcom/google/android/youtube/app/player/mp4/i;

    sget-object v1, Lcom/google/android/youtube/app/player/mp4/b;->j:Lcom/google/android/youtube/app/player/mp4/s;

    invoke-virtual {v0, v1}, Lcom/google/android/youtube/app/player/mp4/c;->a(Lcom/google/android/youtube/app/player/mp4/s;)Lcom/google/android/youtube/app/player/mp4/a;

    move-result-object v3

    check-cast v3, Lcom/google/android/youtube/app/player/mp4/j;

    sget-object v1, Lcom/google/android/youtube/app/player/mp4/b;->i:Lcom/google/android/youtube/app/player/mp4/s;

    invoke-virtual {v0, v1}, Lcom/google/android/youtube/app/player/mp4/c;->a(Lcom/google/android/youtube/app/player/mp4/s;)Lcom/google/android/youtube/app/player/mp4/a;

    move-result-object v2

    check-cast v2, Lcom/google/android/youtube/app/player/mp4/n;

    sget-object v1, Lcom/google/android/youtube/app/player/mp4/b;->g:Lcom/google/android/youtube/app/player/mp4/s;

    invoke-virtual {v0, v1}, Lcom/google/android/youtube/app/player/mp4/c;->a(Lcom/google/android/youtube/app/player/mp4/s;)Lcom/google/android/youtube/app/player/mp4/a;

    move-result-object v1

    check-cast v1, Lcom/google/android/youtube/app/player/mp4/p;

    sget-object v5, Lcom/google/android/youtube/app/player/mp4/b;->k:Lcom/google/android/youtube/app/player/mp4/s;

    invoke-virtual {v0, v5}, Lcom/google/android/youtube/app/player/mp4/c;->a(Lcom/google/android/youtube/app/player/mp4/s;)Lcom/google/android/youtube/app/player/mp4/a;

    move-result-object v5

    check-cast v5, Lcom/google/android/youtube/app/player/mp4/m;

    new-instance v0, Lcom/google/android/youtube/app/player/mp4/g;

    invoke-virtual {v2}, Lcom/google/android/youtube/app/player/mp4/n;->e()I

    move-result v6

    invoke-direct/range {v0 .. v6}, Lcom/google/android/youtube/app/player/mp4/g;-><init>(Ljava/lang/Iterable;Ljava/lang/Iterable;Ljava/lang/Iterable;Ljava/lang/Iterable;Ljava/lang/Iterable;I)V

    invoke-virtual {v0}, Lcom/google/android/youtube/app/player/mp4/g;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_0
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/youtube/app/player/mp4/f;

    iget-boolean v1, v0, Lcom/google/android/youtube/app/player/mp4/f;->j:Z

    if-eqz v1, :cond_0

    iget v1, v0, Lcom/google/android/youtube/app/player/mp4/f;->b:I

    div-int v3, v1, v7

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {p1, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Integer;

    if-eqz v1, :cond_1

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    iget v4, v0, Lcom/google/android/youtube/app/player/mp4/f;->g:I

    if-le v1, v4, :cond_0

    :cond_1
    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    iget v0, v0, Lcom/google/android/youtube/app/player/mp4/f;->g:I

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-interface {p1, v1, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    :cond_2
    return-void
.end method
