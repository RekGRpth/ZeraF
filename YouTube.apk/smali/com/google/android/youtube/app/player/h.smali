.class final Lcom/google/android/youtube/app/player/h;
.super Landroid/content/BroadcastReceiver;
.source "SourceFile"


# instance fields
.field final synthetic a:Lcom/google/android/youtube/app/player/a;

.field private b:Z

.field private c:Z

.field private d:Z


# direct methods
.method private constructor <init>(Lcom/google/android/youtube/app/player/a;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/youtube/app/player/h;->a:Lcom/google/android/youtube/app/player/a;

    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/google/android/youtube/app/player/a;B)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/google/android/youtube/app/player/h;-><init>(Lcom/google/android/youtube/app/player/a;)V

    return-void
.end method


# virtual methods
.method public final declared-synchronized a()V
    .locals 2

    monitor-enter p0

    :try_start_0
    iget-boolean v0, p0, Lcom/google/android/youtube/app/player/h;->b:Z

    if-nez v0, :cond_0

    new-instance v0, Landroid/content/IntentFilter;

    const-string v1, "android.net.conn.CONNECTIVITY_CHANGE"

    invoke-direct {v0, v1}, Landroid/content/IntentFilter;-><init>(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/google/android/youtube/app/player/h;->a:Lcom/google/android/youtube/app/player/a;

    invoke-static {v1}, Lcom/google/android/youtube/app/player/a;->j(Lcom/google/android/youtube/app/player/a;)Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1, p0, v0}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/youtube/app/player/h;->b:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :cond_0
    monitor-exit p0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized b()V
    .locals 1

    monitor-enter p0

    :try_start_0
    iget-boolean v0, p0, Lcom/google/android/youtube/app/player/h;->b:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/youtube/app/player/h;->a:Lcom/google/android/youtube/app/player/a;

    invoke-static {v0}, Lcom/google/android/youtube/app/player/a;->j(Lcom/google/android/youtube/app/player/a;)Landroid/content/Context;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/youtube/app/player/h;->a:Lcom/google/android/youtube/app/player/a;

    invoke-static {v0}, Lcom/google/android/youtube/app/player/a;->j(Lcom/google/android/youtube/app/player/a;)Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0, p0}, Landroid/content/Context;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/youtube/app/player/h;->b:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :cond_0
    monitor-exit p0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized c()Z
    .locals 1

    monitor-enter p0

    :try_start_0
    iget-boolean v0, p0, Lcom/google/android/youtube/app/player/h;->c:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized d()Z
    .locals 1

    monitor-enter p0

    :try_start_0
    iget-boolean v0, p0, Lcom/google/android/youtube/app/player/h;->d:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 4

    const/4 v0, 0x1

    monitor-enter p0

    :try_start_0
    iget-boolean v1, p0, Lcom/google/android/youtube/app/player/h;->b:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-nez v1, :cond_1

    :cond_0
    :goto_0
    monitor-exit p0

    return-void

    :cond_1
    :try_start_1
    iget-object v1, p0, Lcom/google/android/youtube/app/player/h;->a:Lcom/google/android/youtube/app/player/a;

    invoke-static {v1}, Lcom/google/android/youtube/app/player/a;->k(Lcom/google/android/youtube/app/player/a;)Lcom/google/android/youtube/core/utils/p;

    move-result-object v1

    invoke-interface {v1}, Lcom/google/android/youtube/core/utils/p;->a()Z

    move-result v1

    iput-boolean v1, p0, Lcom/google/android/youtube/app/player/h;->c:Z

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "connection "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-boolean v2, p0, Lcom/google/android/youtube/app/player/h;->c:Z

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    invoke-static {}, Lcom/google/android/youtube/core/L;->b()V

    iget-boolean v1, p0, Lcom/google/android/youtube/app/player/h;->c:Z

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/google/android/youtube/app/player/h;->a:Lcom/google/android/youtube/app/player/a;

    invoke-static {v1}, Lcom/google/android/youtube/app/player/a;->k(Lcom/google/android/youtube/app/player/a;)Lcom/google/android/youtube/core/utils/p;

    move-result-object v1

    invoke-interface {v1}, Lcom/google/android/youtube/core/utils/p;->c()Z

    move-result v1

    if-nez v1, :cond_3

    :goto_1
    iput-boolean v0, p0, Lcom/google/android/youtube/app/player/h;->d:Z

    :cond_2
    iget-object v0, p0, Lcom/google/android/youtube/app/player/h;->a:Lcom/google/android/youtube/app/player/a;

    invoke-static {v0}, Lcom/google/android/youtube/app/player/a;->l(Lcom/google/android/youtube/app/player/a;)Lcom/google/android/youtube/core/transfer/b;

    move-result-object v0

    if-nez v0, :cond_0

    iget-boolean v0, p0, Lcom/google/android/youtube/app/player/h;->c:Z

    if-eqz v0, :cond_4

    iget-object v0, p0, Lcom/google/android/youtube/app/player/h;->a:Lcom/google/android/youtube/app/player/a;

    iget-object v1, p0, Lcom/google/android/youtube/app/player/h;->a:Lcom/google/android/youtube/app/player/a;

    invoke-static {v1}, Lcom/google/android/youtube/app/player/a;->g(Lcom/google/android/youtube/app/player/a;)J

    move-result-wide v1

    invoke-static {v0, v1, v2}, Lcom/google/android/youtube/app/player/a;->b(Lcom/google/android/youtube/app/player/a;J)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    :cond_3
    const/4 v0, 0x0

    goto :goto_1

    :cond_4
    :try_start_2
    iget-object v0, p0, Lcom/google/android/youtube/app/player/h;->a:Lcom/google/android/youtube/app/player/a;

    iget-object v1, p0, Lcom/google/android/youtube/app/player/h;->a:Lcom/google/android/youtube/app/player/a;

    const/4 v2, 0x1

    const/16 v3, -0xfa0

    invoke-virtual {v0, v1, v2, v3}, Lcom/google/android/youtube/app/player/a;->b(Lcom/google/android/youtube/core/player/ad;II)Z
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0
.end method
