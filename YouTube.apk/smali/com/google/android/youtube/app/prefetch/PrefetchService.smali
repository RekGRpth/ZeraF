.class public Lcom/google/android/youtube/app/prefetch/PrefetchService;
.super Landroid/app/Service;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/youtube/core/async/bk;
.implements Lcom/google/android/youtube/core/transfer/i;


# instance fields
.field private a:Lcom/google/android/youtube/core/utils/e;

.field private b:Landroid/app/AlarmManager;

.field private c:Lcom/google/android/youtube/core/player/ak;

.field private d:Ljava/util/Set;

.field private e:Landroid/content/SharedPreferences;

.field private f:Lcom/google/android/youtube/app/prefetch/d;

.field private g:Lcom/google/android/youtube/core/utils/p;

.field private h:I

.field private i:Landroid/os/Handler;

.field private j:Lcom/google/android/youtube/core/client/bc;

.field private k:Lcom/google/android/youtube/core/async/UserAuthorizer;

.field private l:Lcom/google/android/youtube/core/async/au;

.field private m:Lcom/google/android/youtube/core/utils/aa;

.field private n:Lcom/google/android/youtube/core/transfer/l;

.field private o:Ljava/util/Map;

.field private p:Ljava/util/Set;

.field private q:Lcom/google/android/youtube/app/prefetch/h;

.field private r:Lcom/google/android/youtube/core/Analytics;

.field private s:Z

.field private t:Z

.field private u:Z

.field private v:Z


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Landroid/app/Service;-><init>()V

    return-void
.end method

.method static synthetic a(Lcom/google/android/youtube/app/prefetch/PrefetchService;)Lcom/google/android/youtube/app/prefetch/d;
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/app/prefetch/PrefetchService;->f:Lcom/google/android/youtube/app/prefetch/d;

    return-object v0
.end method

.method private a(Lcom/google/android/youtube/app/prefetch/Prefetch$Source;)V
    .locals 3

    iget-object v0, p0, Lcom/google/android/youtube/app/prefetch/PrefetchService;->p:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/youtube/app/prefetch/Prefetch;

    iget-object v2, v0, Lcom/google/android/youtube/app/prefetch/Prefetch;->d:Lcom/google/android/youtube/app/prefetch/Prefetch$Source;

    if-ne v2, p1, :cond_0

    iget-object v2, p0, Lcom/google/android/youtube/app/prefetch/PrefetchService;->n:Lcom/google/android/youtube/core/transfer/l;

    iget-object v0, v0, Lcom/google/android/youtube/app/prefetch/Prefetch;->a:Ljava/lang/String;

    invoke-virtual {v2, v0}, Lcom/google/android/youtube/core/transfer/l;->a(Ljava/lang/String;)V

    invoke-interface {v1}, Ljava/util/Iterator;->remove()V

    goto :goto_0

    :cond_1
    return-void
.end method

.method static synthetic a(Lcom/google/android/youtube/app/prefetch/PrefetchService;Lcom/google/android/youtube/core/model/Video;Lcom/google/android/youtube/app/prefetch/Prefetch$Source;ILandroid/net/Uri;J)V
    .locals 6

    iget-object v0, p0, Lcom/google/android/youtube/app/prefetch/PrefetchService;->f:Lcom/google/android/youtube/app/prefetch/d;

    iget-object v1, p1, Lcom/google/android/youtube/core/model/Video;->id:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/google/android/youtube/app/prefetch/d;->c(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p4}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v1

    const-string v2, "preload"

    const-string v3, "1"

    invoke-virtual {v1, v2, v3}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v1

    invoke-virtual {v1}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v1

    invoke-virtual {v1}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v1

    move-wide v2, p5

    move-object v4, p2

    move v5, p3

    invoke-static/range {v0 .. v5}, Lcom/google/android/youtube/app/prefetch/Prefetch;->a(Ljava/lang/String;Ljava/lang/String;JLcom/google/android/youtube/app/prefetch/Prefetch$Source;I)Lcom/google/android/youtube/app/prefetch/Prefetch;

    move-result-object v1

    iget-object v0, p0, Lcom/google/android/youtube/app/prefetch/PrefetchService;->o:Ljava/util/Map;

    invoke-interface {v0, p2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    return-void
.end method

.method static synthetic a(Lcom/google/android/youtube/app/prefetch/PrefetchService;Ljava/lang/Iterable;Lcom/google/android/youtube/app/prefetch/Prefetch$Source;)V
    .locals 7

    const/4 v0, 0x1

    invoke-interface {p1}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v3

    move v1, v0

    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/youtube/core/model/Video;

    add-int/lit8 v2, v1, 0x1

    iget-object v4, p0, Lcom/google/android/youtube/app/prefetch/PrefetchService;->c:Lcom/google/android/youtube/core/player/ak;

    iget-object v5, p0, Lcom/google/android/youtube/app/prefetch/PrefetchService;->d:Ljava/util/Set;

    new-instance v6, Lcom/google/android/youtube/app/prefetch/a;

    invoke-direct {v6, p0, v0, p2, v1}, Lcom/google/android/youtube/app/prefetch/a;-><init>(Lcom/google/android/youtube/app/prefetch/PrefetchService;Lcom/google/android/youtube/core/model/Video;Lcom/google/android/youtube/app/prefetch/Prefetch$Source;I)V

    invoke-interface {v4, v0, v5, v6}, Lcom/google/android/youtube/core/player/ak;->a(Lcom/google/android/youtube/core/model/Video;Ljava/util/Set;Lcom/google/android/youtube/core/player/al;)V

    move v1, v2

    goto :goto_0

    :cond_0
    return-void
.end method

.method public static a(Landroid/content/Context;)Z
    .locals 3

    const/4 v1, 0x0

    invoke-virtual {p0}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    check-cast v0, Lcom/google/android/youtube/app/YouTubeApplication;

    invoke-virtual {v0}, Lcom/google/android/youtube/app/YouTubeApplication;->S()Landroid/content/SharedPreferences;

    move-result-object v0

    const-string v2, "prefetch_subscriptions"

    invoke-interface {v0, v2, v1}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v2

    if-nez v2, :cond_0

    const-string v2, "prefetch_watch_later"

    invoke-interface {v0, v2, v1}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    move v0, v1

    goto :goto_0
.end method

.method static synthetic b(Lcom/google/android/youtube/app/prefetch/PrefetchService;)I
    .locals 2

    iget v0, p0, Lcom/google/android/youtube/app/prefetch/PrefetchService;->h:I

    add-int/lit8 v1, v0, 0x1

    iput v1, p0, Lcom/google/android/youtube/app/prefetch/PrefetchService;->h:I

    return v0
.end method

.method public static b(Landroid/content/Context;)Ljava/lang/String;
    .locals 3

    const/4 v0, 0x0

    invoke-static {}, Landroid/os/Environment;->getExternalStorageState()Ljava/lang/String;

    move-result-object v1

    const-string v2, "mounted"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_1

    :cond_0
    :goto_0
    return-object v0

    :cond_1
    invoke-virtual {p0, v0}, Landroid/content/Context;->getExternalFilesDir(Ljava/lang/String;)Ljava/io/File;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-virtual {v1}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method static synthetic c(Lcom/google/android/youtube/app/prefetch/PrefetchService;)Landroid/os/Handler;
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/app/prefetch/PrefetchService;->i:Landroid/os/Handler;

    return-object v0
.end method

.method private c()V
    .locals 7

    const/4 v2, 0x0

    iget v0, p0, Lcom/google/android/youtube/app/prefetch/PrefetchService;->h:I

    if-lez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, Lcom/google/android/youtube/core/utils/s;->a(Z)V

    iget v0, p0, Lcom/google/android/youtube/app/prefetch/PrefetchService;->h:I

    add-int/lit8 v0, v0, -0x1

    iput v0, p0, Lcom/google/android/youtube/app/prefetch/PrefetchService;->h:I

    if-lez v0, :cond_1

    :goto_1
    return-void

    :cond_0
    move v0, v2

    goto :goto_0

    :cond_1
    iget-boolean v0, p0, Lcom/google/android/youtube/app/prefetch/PrefetchService;->v:Z

    if-eqz v0, :cond_2

    invoke-virtual {p0}, Lcom/google/android/youtube/app/prefetch/PrefetchService;->stopSelf()V

    goto :goto_1

    :cond_2
    new-instance v3, Ljava/util/LinkedHashSet;

    invoke-direct {v3}, Ljava/util/LinkedHashSet;-><init>()V

    iget-object v0, p0, Lcom/google/android/youtube/app/prefetch/PrefetchService;->o:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->keySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :goto_2
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/youtube/app/prefetch/Prefetch$Source;

    iget-object v5, p0, Lcom/google/android/youtube/app/prefetch/PrefetchService;->q:Lcom/google/android/youtube/app/prefetch/h;

    iget-object v1, p0, Lcom/google/android/youtube/app/prefetch/PrefetchService;->o:Ljava/util/Map;

    invoke-interface {v1, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/List;

    invoke-virtual {v5, v0, v1}, Lcom/google/android/youtube/app/prefetch/h;->a(Lcom/google/android/youtube/app/prefetch/Prefetch$Source;Ljava/util/List;)Ljava/util/Set;

    move-result-object v0

    invoke-interface {v3, v0}, Ljava/util/Set;->addAll(Ljava/util/Collection;)Z

    goto :goto_2

    :cond_3
    new-instance v0, Lcom/google/android/youtube/app/prefetch/k;

    iget-object v1, p0, Lcom/google/android/youtube/app/prefetch/PrefetchService;->r:Lcom/google/android/youtube/core/Analytics;

    iget-object v4, p0, Lcom/google/android/youtube/app/prefetch/PrefetchService;->n:Lcom/google/android/youtube/core/transfer/l;

    invoke-direct {v0, v1, v4}, Lcom/google/android/youtube/app/prefetch/k;-><init>(Lcom/google/android/youtube/core/Analytics;Lcom/google/android/youtube/core/transfer/l;)V

    iget-object v1, p0, Lcom/google/android/youtube/app/prefetch/PrefetchService;->p:Ljava/util/Set;

    invoke-virtual {v0, v3, v1}, Lcom/google/android/youtube/app/prefetch/k;->a(Ljava/util/Set;Ljava/util/Set;)V

    invoke-static {}, Lcom/google/android/youtube/core/L;->b()V

    invoke-static {p0}, Lcom/google/android/youtube/app/prefetch/PrefetchService;->d(Landroid/content/Context;)Landroid/content/Intent;

    move-result-object v0

    invoke-static {p0, v2, v0, v2}, Landroid/app/PendingIntent;->getService(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/youtube/app/prefetch/PrefetchService;->b:Landroid/app/AlarmManager;

    const/4 v2, 0x2

    iget-object v3, p0, Lcom/google/android/youtube/app/prefetch/PrefetchService;->a:Lcom/google/android/youtube/core/utils/e;

    invoke-interface {v3}, Lcom/google/android/youtube/core/utils/e;->b()J

    move-result-wide v3

    const-wide/32 v5, 0x1499700

    add-long/2addr v3, v5

    invoke-virtual {v1, v2, v3, v4, v0}, Landroid/app/AlarmManager;->set(IJLandroid/app/PendingIntent;)V

    invoke-virtual {p0}, Lcom/google/android/youtube/app/prefetch/PrefetchService;->stopSelf()V

    goto :goto_1
.end method

.method public static c(Landroid/content/Context;)V
    .locals 1

    invoke-virtual {p0}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    check-cast v0, Lcom/google/android/youtube/app/YouTubeApplication;

    invoke-virtual {v0}, Lcom/google/android/youtube/app/YouTubeApplication;->n()Lcom/google/android/youtube/app/k;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/youtube/app/k;->m()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-static {p0}, Lcom/google/android/youtube/app/prefetch/PrefetchService;->b(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-static {p0}, Lcom/google/android/youtube/app/prefetch/PrefetchService;->d(Landroid/content/Context;)Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {p0, v0}, Landroid/content/Context;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;

    :cond_0
    return-void
.end method

.method private static d(Landroid/content/Context;)Landroid/content/Intent;
    .locals 2

    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/google/android/youtube/app/prefetch/PrefetchService;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    return-object v0
.end method

.method static synthetic d(Lcom/google/android/youtube/app/prefetch/PrefetchService;)Lcom/google/android/youtube/core/async/au;
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/app/prefetch/PrefetchService;->l:Lcom/google/android/youtube/core/async/au;

    return-object v0
.end method

.method private d()V
    .locals 3

    iget-object v0, p0, Lcom/google/android/youtube/app/prefetch/PrefetchService;->p:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/youtube/app/prefetch/Prefetch;

    iget-object v2, p0, Lcom/google/android/youtube/app/prefetch/PrefetchService;->n:Lcom/google/android/youtube/core/transfer/l;

    iget-object v0, v0, Lcom/google/android/youtube/app/prefetch/Prefetch;->a:Ljava/lang/String;

    invoke-virtual {v2, v0}, Lcom/google/android/youtube/core/transfer/l;->a(Ljava/lang/String;)V

    goto :goto_0

    :cond_0
    iget-object v0, p0, Lcom/google/android/youtube/app/prefetch/PrefetchService;->p:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->clear()V

    return-void
.end method

.method static synthetic e(Lcom/google/android/youtube/app/prefetch/PrefetchService;)V
    .locals 0

    invoke-direct {p0}, Lcom/google/android/youtube/app/prefetch/PrefetchService;->c()V

    return-void
.end method


# virtual methods
.method public final a(Lcom/google/android/youtube/core/model/UserAuth;)V
    .locals 4

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "User "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v1, p1, Lcom/google/android/youtube/core/model/UserAuth;->account:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " is signed in, requesting videos"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    invoke-static {}, Lcom/google/android/youtube/core/L;->b()V

    const/4 v0, 0x2

    iput v0, p0, Lcom/google/android/youtube/app/prefetch/PrefetchService;->h:I

    iget-boolean v0, p0, Lcom/google/android/youtube/app/prefetch/PrefetchService;->s:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/youtube/app/prefetch/PrefetchService;->j:Lcom/google/android/youtube/core/client/bc;

    iget-object v1, p0, Lcom/google/android/youtube/app/prefetch/PrefetchService;->i:Landroid/os/Handler;

    new-instance v2, Lcom/google/android/youtube/app/prefetch/c;

    sget-object v3, Lcom/google/android/youtube/app/prefetch/Prefetch$Source;->SUBSCRIPTION:Lcom/google/android/youtube/app/prefetch/Prefetch$Source;

    invoke-direct {v2, p0, v3}, Lcom/google/android/youtube/app/prefetch/c;-><init>(Lcom/google/android/youtube/app/prefetch/PrefetchService;Lcom/google/android/youtube/app/prefetch/Prefetch$Source;)V

    invoke-static {v1, v2}, Lcom/google/android/youtube/core/async/am;->a(Landroid/os/Handler;Lcom/google/android/youtube/core/async/n;)Lcom/google/android/youtube/core/async/am;

    move-result-object v1

    invoke-interface {v0, p1, v1}, Lcom/google/android/youtube/core/client/bc;->d(Lcom/google/android/youtube/core/model/UserAuth;Lcom/google/android/youtube/core/async/n;)V

    :goto_0
    iget-boolean v0, p0, Lcom/google/android/youtube/app/prefetch/PrefetchService;->t:Z

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/youtube/app/prefetch/PrefetchService;->j:Lcom/google/android/youtube/core/client/bc;

    iget-object v1, p0, Lcom/google/android/youtube/app/prefetch/PrefetchService;->i:Landroid/os/Handler;

    new-instance v2, Lcom/google/android/youtube/app/prefetch/c;

    sget-object v3, Lcom/google/android/youtube/app/prefetch/Prefetch$Source;->WATCH_LATER:Lcom/google/android/youtube/app/prefetch/Prefetch$Source;

    invoke-direct {v2, p0, v3}, Lcom/google/android/youtube/app/prefetch/c;-><init>(Lcom/google/android/youtube/app/prefetch/PrefetchService;Lcom/google/android/youtube/app/prefetch/Prefetch$Source;)V

    invoke-static {v1, v2}, Lcom/google/android/youtube/core/async/am;->a(Landroid/os/Handler;Lcom/google/android/youtube/core/async/n;)Lcom/google/android/youtube/core/async/am;

    move-result-object v1

    invoke-interface {v0, p1, v1}, Lcom/google/android/youtube/core/client/bc;->c(Lcom/google/android/youtube/core/model/UserAuth;Lcom/google/android/youtube/core/async/n;)V

    :goto_1
    return-void

    :cond_0
    invoke-direct {p0}, Lcom/google/android/youtube/app/prefetch/PrefetchService;->c()V

    goto :goto_0

    :cond_1
    invoke-direct {p0}, Lcom/google/android/youtube/app/prefetch/PrefetchService;->c()V

    goto :goto_1
.end method

.method public final a(Lcom/google/android/youtube/core/transfer/Transfer;)V
    .locals 0

    return-void
.end method

.method public final a(Ljava/lang/String;Ljava/lang/Exception;)V
    .locals 0

    invoke-static {}, Lcom/google/android/youtube/core/L;->b()V

    invoke-virtual {p0}, Lcom/google/android/youtube/app/prefetch/PrefetchService;->stopSelf()V

    return-void
.end method

.method public final b(Lcom/google/android/youtube/core/transfer/Transfer;)V
    .locals 0

    return-void
.end method

.method public final c(Lcom/google/android/youtube/core/transfer/Transfer;)V
    .locals 0

    return-void
.end method

.method public final d(Lcom/google/android/youtube/core/transfer/Transfer;)V
    .locals 0

    return-void
.end method

.method public final e(Lcom/google/android/youtube/core/transfer/Transfer;)V
    .locals 0

    return-void
.end method

.method public final g_()V
    .locals 0

    invoke-static {}, Lcom/google/android/youtube/core/L;->b()V

    invoke-direct {p0}, Lcom/google/android/youtube/app/prefetch/PrefetchService;->d()V

    invoke-virtual {p0}, Lcom/google/android/youtube/app/prefetch/PrefetchService;->stopSelf()V

    return-void
.end method

.method public onBind(Landroid/content/Intent;)Landroid/os/IBinder;
    .locals 1

    const/4 v0, 0x0

    return-object v0
.end method

.method public onCreate()V
    .locals 1

    invoke-super {p0}, Landroid/app/Service;->onCreate()V

    invoke-virtual {p0}, Lcom/google/android/youtube/app/prefetch/PrefetchService;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    check-cast v0, Lcom/google/android/youtube/app/YouTubeApplication;

    invoke-virtual {v0}, Lcom/google/android/youtube/app/YouTubeApplication;->n()Lcom/google/android/youtube/app/k;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/youtube/app/k;->m()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-static {p0}, Lcom/google/android/youtube/app/prefetch/PrefetchService;->b(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_1

    :cond_0
    invoke-virtual {p0}, Lcom/google/android/youtube/app/prefetch/PrefetchService;->stopSelf()V

    :cond_1
    return-void
.end method

.method public onDestroy()V
    .locals 2

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "PrefetchService destroyed at "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    new-instance v1, Ljava/util/Date;

    invoke-direct {v1}, Ljava/util/Date;-><init>()V

    invoke-virtual {v1}, Ljava/util/Date;->toGMTString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    invoke-static {}, Lcom/google/android/youtube/core/L;->b()V

    iget-object v0, p0, Lcom/google/android/youtube/app/prefetch/PrefetchService;->m:Lcom/google/android/youtube/core/utils/aa;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/youtube/app/prefetch/PrefetchService;->m:Lcom/google/android/youtube/core/utils/aa;

    invoke-virtual {v0, p0}, Lcom/google/android/youtube/core/utils/aa;->b(Landroid/content/Context;)V

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/youtube/app/prefetch/PrefetchService;->m:Lcom/google/android/youtube/core/utils/aa;

    :cond_0
    invoke-super {p0}, Landroid/app/Service;->onDestroy()V

    iget-boolean v0, p0, Lcom/google/android/youtube/app/prefetch/PrefetchService;->v:Z

    if-eqz v0, :cond_1

    invoke-virtual {p0}, Lcom/google/android/youtube/app/prefetch/PrefetchService;->getApplication()Landroid/app/Application;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/youtube/app/prefetch/PrefetchService;->c(Landroid/content/Context;)V

    :cond_1
    return-void
.end method

.method public onStartCommand(Landroid/content/Intent;II)I
    .locals 8

    const/4 v7, 0x1

    const/4 v5, 0x0

    iget-boolean v0, p0, Lcom/google/android/youtube/app/prefetch/PrefetchService;->u:Z

    if-eqz v0, :cond_0

    iput-boolean v7, p0, Lcom/google/android/youtube/app/prefetch/PrefetchService;->v:Z

    :goto_0
    return v7

    :cond_0
    iput-boolean v7, p0, Lcom/google/android/youtube/app/prefetch/PrefetchService;->u:Z

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "PrefetchService started at "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    new-instance v1, Ljava/util/Date;

    invoke-direct {v1}, Ljava/util/Date;-><init>()V

    invoke-virtual {v1}, Ljava/util/Date;->toGMTString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    invoke-static {}, Lcom/google/android/youtube/core/L;->b()V

    invoke-virtual {p0}, Lcom/google/android/youtube/app/prefetch/PrefetchService;->getApplication()Landroid/app/Application;

    move-result-object v0

    move-object v6, v0

    check-cast v6, Lcom/google/android/youtube/app/YouTubeApplication;

    invoke-virtual {v6}, Lcom/google/android/youtube/app/YouTubeApplication;->S()Landroid/content/SharedPreferences;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/youtube/app/prefetch/PrefetchService;->e:Landroid/content/SharedPreferences;

    invoke-virtual {v6}, Lcom/google/android/youtube/app/YouTubeApplication;->E()Lcom/google/android/youtube/app/prefetch/d;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/youtube/app/prefetch/PrefetchService;->f:Lcom/google/android/youtube/app/prefetch/d;

    invoke-virtual {v6}, Lcom/google/android/youtube/app/YouTubeApplication;->U()Lcom/google/android/youtube/core/async/UserAuthorizer;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/youtube/app/prefetch/PrefetchService;->k:Lcom/google/android/youtube/core/async/UserAuthorizer;

    invoke-virtual {v6}, Lcom/google/android/youtube/app/YouTubeApplication;->l()Lcom/google/android/youtube/core/utils/p;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/youtube/app/prefetch/PrefetchService;->g:Lcom/google/android/youtube/core/utils/p;

    invoke-virtual {v6}, Lcom/google/android/youtube/app/YouTubeApplication;->a()Lcom/google/android/youtube/core/client/bc;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/youtube/app/prefetch/PrefetchService;->j:Lcom/google/android/youtube/core/client/bc;

    invoke-virtual {v6}, Lcom/google/android/youtube/app/YouTubeApplication;->i()Lcom/google/android/youtube/core/Analytics;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/youtube/app/prefetch/PrefetchService;->r:Lcom/google/android/youtube/core/Analytics;

    new-instance v0, Lcom/google/android/youtube/core/player/k;

    iget-object v1, p0, Lcom/google/android/youtube/app/prefetch/PrefetchService;->g:Lcom/google/android/youtube/core/utils/p;

    invoke-static {p0}, Lcom/google/android/youtube/core/utils/j;->c(Landroid/content/Context;)Z

    move-result v2

    invoke-static {p0}, Lcom/google/android/youtube/core/utils/n;->a(Landroid/content/Context;)Z

    move-result v3

    invoke-virtual {v6}, Lcom/google/android/youtube/app/YouTubeApplication;->n()Lcom/google/android/youtube/app/k;

    move-result-object v4

    invoke-virtual {v4}, Lcom/google/android/youtube/app/k;->A()Z

    move-result v4

    invoke-direct/range {v0 .. v5}, Lcom/google/android/youtube/core/player/k;-><init>(Lcom/google/android/youtube/core/utils/p;ZZZZ)V

    iput-object v0, p0, Lcom/google/android/youtube/app/prefetch/PrefetchService;->c:Lcom/google/android/youtube/core/player/ak;

    invoke-virtual {v6}, Lcom/google/android/youtube/app/YouTubeApplication;->n()Lcom/google/android/youtube/app/k;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/youtube/app/k;->j()Z

    move-result v0

    if-eqz v0, :cond_1

    sget-object v0, Lcom/google/android/youtube/app/player/a;->a:Ljava/util/Set;

    :goto_1
    iput-object v0, p0, Lcom/google/android/youtube/app/prefetch/PrefetchService;->d:Ljava/util/Set;

    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    iput-object v0, p0, Lcom/google/android/youtube/app/prefetch/PrefetchService;->i:Landroid/os/Handler;

    new-instance v0, Lcom/google/android/youtube/core/utils/ag;

    invoke-direct {v0}, Lcom/google/android/youtube/core/utils/ag;-><init>()V

    iput-object v0, p0, Lcom/google/android/youtube/app/prefetch/PrefetchService;->a:Lcom/google/android/youtube/core/utils/e;

    const-string v0, "alarm"

    invoke-virtual {p0, v0}, Lcom/google/android/youtube/app/prefetch/PrefetchService;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/AlarmManager;

    iput-object v0, p0, Lcom/google/android/youtube/app/prefetch/PrefetchService;->b:Landroid/app/AlarmManager;

    invoke-virtual {v6}, Lcom/google/android/youtube/app/YouTubeApplication;->P()Lorg/apache/http/client/HttpClient;

    move-result-object v0

    invoke-virtual {v6}, Lcom/google/android/youtube/app/YouTubeApplication;->N()Ljava/util/concurrent/Executor;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/youtube/app/prefetch/PrefetchService;->a:Lcom/google/android/youtube/core/utils/e;

    invoke-static {v0, v1, v2}, Lcom/google/android/youtube/core/async/az;->a(Lorg/apache/http/client/HttpClient;Ljava/util/concurrent/Executor;Lcom/google/android/youtube/core/utils/e;)Lcom/google/android/youtube/core/async/au;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/youtube/app/prefetch/PrefetchService;->l:Lcom/google/android/youtube/core/async/au;

    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, Lcom/google/android/youtube/app/prefetch/PrefetchService;->p:Ljava/util/Set;

    new-instance v0, Ljava/util/EnumMap;

    const-class v1, Lcom/google/android/youtube/app/prefetch/Prefetch$Source;

    invoke-direct {v0, v1}, Ljava/util/EnumMap;-><init>(Ljava/lang/Class;)V

    iput-object v0, p0, Lcom/google/android/youtube/app/prefetch/PrefetchService;->o:Ljava/util/Map;

    invoke-static {}, Lcom/google/android/youtube/app/prefetch/Prefetch$Source;->values()[Lcom/google/android/youtube/app/prefetch/Prefetch$Source;

    move-result-object v1

    array-length v2, v1

    move v0, v5

    :goto_2
    if-ge v0, v2, :cond_2

    aget-object v3, v1, v0

    iget-object v4, p0, Lcom/google/android/youtube/app/prefetch/PrefetchService;->o:Ljava/util/Map;

    new-instance v6, Ljava/util/LinkedList;

    invoke-direct {v6}, Ljava/util/LinkedList;-><init>()V

    invoke-interface {v4, v3, v6}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    :cond_1
    sget-object v0, Lcom/google/android/youtube/core/player/aa;->a:Ljava/util/Set;

    goto :goto_1

    :cond_2
    iget-object v0, p0, Lcom/google/android/youtube/app/prefetch/PrefetchService;->e:Landroid/content/SharedPreferences;

    const-string v1, "prefetch_subscriptions"

    invoke-interface {v0, v1, v5}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    iput-boolean v0, p0, Lcom/google/android/youtube/app/prefetch/PrefetchService;->s:Z

    iget-object v0, p0, Lcom/google/android/youtube/app/prefetch/PrefetchService;->e:Landroid/content/SharedPreferences;

    const-string v1, "prefetch_watch_later"

    invoke-interface {v0, v1, v5}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    iput-boolean v0, p0, Lcom/google/android/youtube/app/prefetch/PrefetchService;->t:Z

    invoke-static {p0, p0}, Lcom/google/android/youtube/core/transfer/DownloadService;->a(Landroid/content/Context;Lcom/google/android/youtube/core/transfer/i;)Lcom/google/android/youtube/core/utils/aa;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/youtube/app/prefetch/PrefetchService;->m:Lcom/google/android/youtube/core/utils/aa;

    goto/16 :goto_0
.end method

.method public final t_()V
    .locals 11

    const-wide/16 v9, 0x1

    const/4 v3, 0x1

    const/4 v4, 0x0

    invoke-static {}, Lcom/google/android/youtube/core/L;->b()V

    iget-object v0, p0, Lcom/google/android/youtube/app/prefetch/PrefetchService;->m:Lcom/google/android/youtube/core/utils/aa;

    invoke-virtual {v0}, Lcom/google/android/youtube/core/utils/aa;->a()Landroid/os/Binder;

    move-result-object v0

    check-cast v0, Lcom/google/android/youtube/core/transfer/l;

    iput-object v0, p0, Lcom/google/android/youtube/app/prefetch/PrefetchService;->n:Lcom/google/android/youtube/core/transfer/l;

    iget-object v0, p0, Lcom/google/android/youtube/app/prefetch/PrefetchService;->n:Lcom/google/android/youtube/core/transfer/l;

    invoke-virtual {v0}, Lcom/google/android/youtube/core/transfer/l;->a()Ljava/util/Map;

    move-result-object v2

    iget-object v0, p0, Lcom/google/android/youtube/app/prefetch/PrefetchService;->n:Lcom/google/android/youtube/core/transfer/l;

    invoke-virtual {v0, p0}, Lcom/google/android/youtube/core/transfer/l;->b(Lcom/google/android/youtube/core/transfer/i;)Z

    invoke-static {p0}, Lcom/google/android/youtube/app/prefetch/PrefetchService;->b(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_0

    :goto_0
    return-void

    :cond_0
    new-instance v1, Landroid/os/StatFs;

    invoke-direct {v1, v0}, Landroid/os/StatFs;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1}, Landroid/os/StatFs;->getBlockCount()I

    move-result v0

    int-to-long v5, v0

    mul-long/2addr v5, v9

    invoke-virtual {v1}, Landroid/os/StatFs;->getBlockSize()I

    move-result v0

    int-to-long v7, v0

    mul-long/2addr v5, v7

    invoke-virtual {v1}, Landroid/os/StatFs;->getAvailableBlocks()I

    move-result v0

    int-to-long v7, v0

    mul-long/2addr v7, v9

    invoke-virtual {v1}, Landroid/os/StatFs;->getBlockSize()I

    move-result v0

    int-to-long v0, v0

    mul-long/2addr v0, v7

    invoke-interface {v2}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v7

    move-wide v1, v0

    :goto_1
    invoke-interface {v7}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v7}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/youtube/core/transfer/Transfer;

    new-instance v8, Ljava/io/File;

    iget-object v9, v0, Lcom/google/android/youtube/core/transfer/Transfer;->a:Ljava/lang/String;

    invoke-direct {v8, v9}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v8}, Ljava/io/File;->length()J

    move-result-wide v8

    add-long/2addr v1, v8

    iget-object v8, v0, Lcom/google/android/youtube/core/transfer/Transfer;->a:Ljava/lang/String;

    invoke-static {v8}, Lcom/google/android/youtube/app/prefetch/Prefetch;->a(Ljava/lang/String;)Z

    move-result v8

    if-eqz v8, :cond_1

    iget-object v8, p0, Lcom/google/android/youtube/app/prefetch/PrefetchService;->n:Lcom/google/android/youtube/core/transfer/l;

    iget-object v0, v0, Lcom/google/android/youtube/core/transfer/Transfer;->a:Ljava/lang/String;

    invoke-virtual {v8, v0}, Lcom/google/android/youtube/core/transfer/l;->a(Ljava/lang/String;)V

    goto :goto_1

    :cond_1
    invoke-static {v0}, Lcom/google/android/youtube/app/prefetch/Prefetch;->a(Lcom/google/android/youtube/core/transfer/Transfer;)Lcom/google/android/youtube/app/prefetch/Prefetch;

    move-result-object v0

    iget-object v8, p0, Lcom/google/android/youtube/app/prefetch/PrefetchService;->p:Ljava/util/Set;

    invoke-interface {v8, v0}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    goto :goto_1

    :cond_2
    const-wide/32 v7, 0x40000000

    const-wide/16 v9, 0x2

    div-long/2addr v5, v9

    invoke-static {v7, v8, v5, v6}, Ljava/lang/Math;->min(JJ)J

    move-result-wide v5

    const-wide/32 v7, 0x8000000

    sub-long v0, v1, v7

    invoke-static {v5, v6, v0, v1}, Ljava/lang/Math;->min(JJ)J

    move-result-wide v1

    iget-boolean v0, p0, Lcom/google/android/youtube/app/prefetch/PrefetchService;->s:Z

    if-eqz v0, :cond_3

    move v0, v3

    :goto_2
    iget-boolean v5, p0, Lcom/google/android/youtube/app/prefetch/PrefetchService;->t:Z

    if-eqz v5, :cond_4

    :goto_3
    add-int/2addr v0, v3

    if-nez v0, :cond_5

    invoke-direct {p0}, Lcom/google/android/youtube/app/prefetch/PrefetchService;->d()V

    invoke-virtual {p0}, Lcom/google/android/youtube/app/prefetch/PrefetchService;->stopSelf()V

    goto :goto_0

    :cond_3
    move v0, v4

    goto :goto_2

    :cond_4
    move v3, v4

    goto :goto_3

    :cond_5
    iget-boolean v3, p0, Lcom/google/android/youtube/app/prefetch/PrefetchService;->s:Z

    if-nez v3, :cond_6

    sget-object v3, Lcom/google/android/youtube/app/prefetch/Prefetch$Source;->SUBSCRIPTION:Lcom/google/android/youtube/app/prefetch/Prefetch$Source;

    invoke-direct {p0, v3}, Lcom/google/android/youtube/app/prefetch/PrefetchService;->a(Lcom/google/android/youtube/app/prefetch/Prefetch$Source;)V

    :cond_6
    iget-boolean v3, p0, Lcom/google/android/youtube/app/prefetch/PrefetchService;->t:Z

    if-nez v3, :cond_7

    sget-object v3, Lcom/google/android/youtube/app/prefetch/Prefetch$Source;->WATCH_LATER:Lcom/google/android/youtube/app/prefetch/Prefetch$Source;

    invoke-direct {p0, v3}, Lcom/google/android/youtube/app/prefetch/PrefetchService;->a(Lcom/google/android/youtube/app/prefetch/Prefetch$Source;)V

    :cond_7
    const-wide/32 v3, 0x20000000

    int-to-long v5, v0

    div-long v0, v1, v5

    invoke-static {v3, v4, v0, v1}, Ljava/lang/Math;->min(JJ)J

    move-result-wide v0

    new-instance v2, Lcom/google/android/youtube/app/prefetch/h;

    const-wide/16 v3, 0x0

    invoke-static {v3, v4, v0, v1}, Ljava/lang/Math;->max(JJ)J

    move-result-wide v0

    invoke-direct {v2, v0, v1}, Lcom/google/android/youtube/app/prefetch/h;-><init>(J)V

    iput-object v2, p0, Lcom/google/android/youtube/app/prefetch/PrefetchService;->q:Lcom/google/android/youtube/app/prefetch/h;

    iget-object v0, p0, Lcom/google/android/youtube/app/prefetch/PrefetchService;->g:Lcom/google/android/youtube/core/utils/p;

    invoke-interface {v0}, Lcom/google/android/youtube/core/utils/p;->c()Z

    move-result v0

    if-eqz v0, :cond_8

    invoke-static {p0}, Lcom/google/android/youtube/core/utils/Util;->d(Landroid/content/Context;)Z

    move-result v0

    if-nez v0, :cond_9

    :cond_8
    invoke-virtual {p0}, Lcom/google/android/youtube/app/prefetch/PrefetchService;->stopSelf()V

    goto/16 :goto_0

    :cond_9
    iget-object v0, p0, Lcom/google/android/youtube/app/prefetch/PrefetchService;->k:Lcom/google/android/youtube/core/async/UserAuthorizer;

    invoke-virtual {v0, p0}, Lcom/google/android/youtube/core/async/UserAuthorizer;->a(Lcom/google/android/youtube/core/async/bk;)V

    goto/16 :goto_0
.end method
