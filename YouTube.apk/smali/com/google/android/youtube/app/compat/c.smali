.class public final Lcom/google/android/youtube/app/compat/c;
.super Lcom/google/android/youtube/app/compat/SupportActionBar;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/youtube/app/compat/ab;


# instance fields
.field private final a:Landroid/app/Activity;

.field private final b:Landroid/view/ViewGroup;

.field private final c:Landroid/view/ViewGroup;

.field private final d:Landroid/widget/ImageView;

.field private final e:Landroid/widget/TextView;

.field private final f:Landroid/widget/TextView;

.field private final g:Landroid/view/View;

.field private final h:Landroid/widget/ImageView;

.field private final i:Landroid/widget/FrameLayout;

.field private j:Landroid/view/View;

.field private k:Ljava/util/List;

.field private final l:I

.field private final m:I

.field private final n:I

.field private final o:I

.field private final p:I

.field private final q:I

.field private final r:I

.field private s:I


# direct methods
.method public constructor <init>(Landroid/app/Activity;)V
    .locals 10

    const/4 v9, 0x3

    const/4 v1, 0x1

    const/4 v8, -0x1

    const/4 v7, 0x0

    const/4 v2, 0x0

    invoke-direct {p0}, Lcom/google/android/youtube/app/compat/SupportActionBar;-><init>()V

    const-string v0, "activity cannot be null"

    invoke-static {p1, v0}, Lcom/google/android/youtube/core/utils/s;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/Activity;

    iput-object v0, p0, Lcom/google/android/youtube/app/compat/c;->a:Landroid/app/Activity;

    instance-of v0, p1, Lcom/google/android/youtube/app/compat/i;

    const-string v3, "activity must implement SupportActionBarActivity interface"

    invoke-static {v0, v3}, Lcom/google/android/youtube/core/utils/s;->a(ZLjava/lang/Object;)V

    invoke-virtual {p1}, Landroid/app/Activity;->getWindow()Landroid/view/Window;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/Window;->getDecorView()Landroid/view/View;

    move-result-object v0

    const v3, 0x7f070136

    invoke-virtual {v0, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    iput-object v0, p0, Lcom/google/android/youtube/app/compat/c;->b:Landroid/view/ViewGroup;

    iget-object v0, p0, Lcom/google/android/youtube/app/compat/c;->b:Landroid/view/ViewGroup;

    if-eqz v0, :cond_0

    move v0, v1

    :goto_0
    const-string v3, "cannot find action_bar view"

    invoke-static {v0, v3}, Lcom/google/android/youtube/core/utils/s;->b(ZLjava/lang/Object;)V

    iget-object v0, p0, Lcom/google/android/youtube/app/compat/c;->b:Landroid/view/ViewGroup;

    const v3, 0x7f07013a

    invoke-virtual {v0, v3}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    iput-object v0, p0, Lcom/google/android/youtube/app/compat/c;->c:Landroid/view/ViewGroup;

    iget-object v0, p0, Lcom/google/android/youtube/app/compat/c;->b:Landroid/view/ViewGroup;

    const v3, 0x7f070137

    invoke-virtual {v0, v3}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/youtube/app/compat/c;->g:Landroid/view/View;

    iget-object v0, p0, Lcom/google/android/youtube/app/compat/c;->b:Landroid/view/ViewGroup;

    invoke-virtual {v0, v2}, Landroid/view/ViewGroup;->setVisibility(I)V

    iget-object v0, p0, Lcom/google/android/youtube/app/compat/c;->g:Landroid/view/View;

    new-instance v3, Lcom/google/android/youtube/app/compat/d;

    invoke-direct {v3, p0}, Lcom/google/android/youtube/app/compat/d;-><init>(Lcom/google/android/youtube/app/compat/c;)V

    invoke-virtual {v0, v3}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v0, p0, Lcom/google/android/youtube/app/compat/c;->b:Landroid/view/ViewGroup;

    const v3, 0x7f070138

    invoke-virtual {v0, v3}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/google/android/youtube/app/compat/c;->h:Landroid/widget/ImageView;

    iget-object v0, p0, Lcom/google/android/youtube/app/compat/c;->b:Landroid/view/ViewGroup;

    const v3, 0x7f070139

    invoke-virtual {v0, v3}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/google/android/youtube/app/compat/c;->d:Landroid/widget/ImageView;

    iget-object v0, p0, Lcom/google/android/youtube/app/compat/c;->b:Landroid/view/ViewGroup;

    const v3, 0x7f07002c

    invoke-virtual {v0, v3}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/google/android/youtube/app/compat/c;->e:Landroid/widget/TextView;

    iget-object v0, p0, Lcom/google/android/youtube/app/compat/c;->b:Landroid/view/ViewGroup;

    const v3, 0x7f0700a5

    invoke-virtual {v0, v3}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/google/android/youtube/app/compat/c;->f:Landroid/widget/TextView;

    const v0, 0x7f0d0024

    sget-object v3, Lcom/google/android/youtube/c;->i:[I

    invoke-virtual {p1, v0, v3}, Landroid/app/Activity;->obtainStyledAttributes(I[I)Landroid/content/res/TypedArray;

    move-result-object v0

    invoke-virtual {v0, v2, v7}, Landroid/content/res/TypedArray;->getDimension(IF)F

    move-result v3

    float-to-int v3, v3

    iget-object v4, p0, Lcom/google/android/youtube/app/compat/c;->b:Landroid/view/ViewGroup;

    new-instance v5, Landroid/widget/RelativeLayout$LayoutParams;

    invoke-direct {v5, v8, v3}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    invoke-virtual {v4, v5}, Landroid/view/ViewGroup;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    iget-object v3, p0, Lcom/google/android/youtube/app/compat/c;->d:Landroid/widget/ImageView;

    const v4, 0x7f020130

    invoke-virtual {v0, v1, v4}, Landroid/content/res/TypedArray;->getResourceId(II)I

    move-result v4

    invoke-virtual {v3, v4}, Landroid/widget/ImageView;->setImageResource(I)V

    const/4 v3, 0x2

    const v4, 0x7f090001

    invoke-virtual {v0, v3, v4}, Landroid/content/res/TypedArray;->getResourceId(II)I

    move-result v3

    iget-object v4, p0, Lcom/google/android/youtube/app/compat/c;->b:Landroid/view/ViewGroup;

    invoke-virtual {v4, v3}, Landroid/view/ViewGroup;->setBackgroundResource(I)V

    invoke-virtual {p1}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f080001

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v3

    iget-object v4, p0, Lcom/google/android/youtube/app/compat/c;->e:Landroid/widget/TextView;

    invoke-virtual {v0, v9, v3}, Landroid/content/res/TypedArray;->getDimensionPixelSize(II)I

    move-result v5

    int-to-float v5, v5

    invoke-virtual {v4, v2, v5}, Landroid/widget/TextView;->setTextSize(IF)V

    iget-object v4, p0, Lcom/google/android/youtube/app/compat/c;->e:Landroid/widget/TextView;

    const/4 v5, 0x4

    const/high16 v6, 0x7f090000

    invoke-virtual {v0, v5, v6}, Landroid/content/res/TypedArray;->getColor(II)I

    move-result v5

    invoke-virtual {v4, v5}, Landroid/widget/TextView;->setTextColor(I)V

    iget-object v4, p0, Lcom/google/android/youtube/app/compat/c;->e:Landroid/widget/TextView;

    iget-object v5, p0, Lcom/google/android/youtube/app/compat/c;->e:Landroid/widget/TextView;

    invoke-virtual {v5}, Landroid/widget/TextView;->getTypeface()Landroid/graphics/Typeface;

    move-result-object v5

    const/4 v6, 0x5

    invoke-virtual {v0, v6, v2}, Landroid/content/res/TypedArray;->getInteger(II)I

    move-result v6

    invoke-virtual {v4, v5, v6}, Landroid/widget/TextView;->setTypeface(Landroid/graphics/Typeface;I)V

    iget-object v4, p0, Lcom/google/android/youtube/app/compat/c;->f:Landroid/widget/TextView;

    const/4 v5, 0x6

    invoke-virtual {v0, v5, v3}, Landroid/content/res/TypedArray;->getDimensionPixelSize(II)I

    move-result v3

    int-to-float v3, v3

    invoke-virtual {v4, v2, v3}, Landroid/widget/TextView;->setTextSize(IF)V

    iget-object v3, p0, Lcom/google/android/youtube/app/compat/c;->f:Landroid/widget/TextView;

    const/4 v4, 0x7

    const v5, 0x7f09002d

    invoke-virtual {v0, v4, v5}, Landroid/content/res/TypedArray;->getColor(II)I

    move-result v4

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setTextColor(I)V

    iget-object v3, p0, Lcom/google/android/youtube/app/compat/c;->f:Landroid/widget/TextView;

    iget-object v4, p0, Lcom/google/android/youtube/app/compat/c;->e:Landroid/widget/TextView;

    invoke-virtual {v4}, Landroid/widget/TextView;->getTypeface()Landroid/graphics/Typeface;

    move-result-object v4

    const/16 v5, 0x8

    invoke-virtual {v0, v5, v2}, Landroid/content/res/TypedArray;->getInteger(II)I

    move-result v5

    invoke-virtual {v3, v4, v5}, Landroid/widget/TextView;->setTypeface(Landroid/graphics/Typeface;I)V

    invoke-virtual {v0}, Landroid/content/res/TypedArray;->recycle()V

    const v3, 0x7f0d0025

    sget-object v4, Lcom/google/android/youtube/c;->j:[I

    invoke-virtual {p1, v3, v4}, Landroid/app/Activity;->obtainStyledAttributes(I[I)Landroid/content/res/TypedArray;

    move-result-object v3

    invoke-virtual {v0, v9, v7}, Landroid/content/res/TypedArray;->getDimension(IF)F

    move-result v4

    float-to-int v4, v4

    iput v4, p0, Lcom/google/android/youtube/app/compat/c;->l:I

    const/4 v4, 0x5

    invoke-virtual {v0, v4, v7}, Landroid/content/res/TypedArray;->getDimension(IF)F

    move-result v4

    float-to-int v4, v4

    iput v4, p0, Lcom/google/android/youtube/app/compat/c;->m:I

    const/4 v4, 0x4

    invoke-virtual {v0, v4, v7}, Landroid/content/res/TypedArray;->getDimension(IF)F

    move-result v4

    float-to-int v4, v4

    iput v4, p0, Lcom/google/android/youtube/app/compat/c;->n:I

    const/4 v4, 0x6

    invoke-virtual {v0, v4, v7}, Landroid/content/res/TypedArray;->getDimension(IF)F

    move-result v4

    float-to-int v4, v4

    iput v4, p0, Lcom/google/android/youtube/app/compat/c;->o:I

    const/4 v4, 0x2

    invoke-virtual {v0, v4, v8}, Landroid/content/res/TypedArray;->getResourceId(II)I

    move-result v4

    iput v4, p0, Lcom/google/android/youtube/app/compat/c;->p:I

    invoke-virtual {v0, v2, v2}, Landroid/content/res/TypedArray;->getDimensionPixelSize(II)I

    move-result v2

    iput v2, p0, Lcom/google/android/youtube/app/compat/c;->q:I

    invoke-virtual {v0, v1, v8}, Landroid/content/res/TypedArray;->getColor(II)I

    move-result v0

    iput v0, p0, Lcom/google/android/youtube/app/compat/c;->r:I

    invoke-virtual {v3}, Landroid/content/res/TypedArray;->recycle()V

    new-instance v0, Ljava/util/LinkedList;

    invoke-direct {v0}, Ljava/util/LinkedList;-><init>()V

    iput-object v0, p0, Lcom/google/android/youtube/app/compat/c;->k:Ljava/util/List;

    move-object v0, p1

    check-cast v0, Lcom/google/android/youtube/app/compat/i;

    invoke-interface {v0}, Lcom/google/android/youtube/app/compat/i;->k()I

    move-result v0

    invoke-virtual {p1, v0}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v2

    invoke-virtual {v2}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/widget/RelativeLayout$LayoutParams;

    move-object v1, p1

    check-cast v1, Lcom/google/android/youtube/app/compat/i;

    invoke-interface {v1}, Lcom/google/android/youtube/app/compat/i;->j()Z

    move-result v1

    if-eqz v1, :cond_1

    const/16 v1, 0xa

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(I)V

    :goto_1
    invoke-virtual {v2, v0}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    const v0, 0x7f07013b

    invoke-virtual {p1, v0}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/FrameLayout;

    iput-object v0, p0, Lcom/google/android/youtube/app/compat/c;->i:Landroid/widget/FrameLayout;

    return-void

    :cond_0
    move v0, v2

    goto/16 :goto_0

    :cond_1
    const v1, 0x7f070136

    invoke-virtual {v0, v9, v1}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(II)V

    goto :goto_1
.end method

.method private static a(Landroid/content/Context;)I
    .locals 2

    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v0

    iget v0, v0, Landroid/content/res/Configuration;->orientation:I

    const/4 v1, 0x2

    if-ne v0, v1, :cond_0

    const/4 v0, 0x5

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x3

    goto :goto_0
.end method

.method static synthetic a(Lcom/google/android/youtube/app/compat/c;)Landroid/app/Activity;
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/app/compat/c;->a:Landroid/app/Activity;

    return-object v0
.end method

.method protected static a(Landroid/content/Context;Lcom/google/android/youtube/app/compat/m;)Ljava/util/List;
    .locals 7

    const/4 v1, 0x0

    new-instance v3, Ljava/util/LinkedList;

    invoke-direct {v3}, Ljava/util/LinkedList;-><init>()V

    invoke-static {p0}, Lcom/google/android/youtube/app/compat/c;->a(Landroid/content/Context;)I

    move-result v4

    move v0, v1

    move v2, v1

    :goto_0
    invoke-virtual {p1}, Lcom/google/android/youtube/app/compat/m;->d()I

    move-result v5

    if-ge v0, v5, :cond_1

    invoke-virtual {p1, v0}, Lcom/google/android/youtube/app/compat/m;->d(I)Lcom/google/android/youtube/app/compat/t;

    move-result-object v5

    invoke-interface {v5}, Lcom/google/android/youtube/app/compat/t;->h()Z

    move-result v6

    if-eqz v6, :cond_0

    invoke-interface {v5}, Lcom/google/android/youtube/app/compat/t;->i()I

    move-result v5

    and-int/lit8 v5, v5, 0x2

    if-eqz v5, :cond_0

    add-int/lit8 v2, v2, 0x1

    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_1
    :goto_1
    invoke-virtual {p1}, Lcom/google/android/youtube/app/compat/m;->d()I

    move-result v0

    if-ge v1, v0, :cond_5

    invoke-virtual {p1, v1}, Lcom/google/android/youtube/app/compat/m;->d(I)Lcom/google/android/youtube/app/compat/t;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/youtube/app/compat/t;->h()Z

    move-result v5

    if-eqz v5, :cond_3

    invoke-interface {v0}, Lcom/google/android/youtube/app/compat/t;->i()I

    move-result v5

    and-int/lit8 v5, v5, 0x2

    if-nez v5, :cond_2

    invoke-interface {v0}, Lcom/google/android/youtube/app/compat/t;->i()I

    move-result v5

    and-int/lit8 v5, v5, 0x1

    if-eqz v5, :cond_4

    if-ge v2, v4, :cond_4

    :cond_2
    add-int/lit8 v2, v2, 0x1

    :cond_3
    :goto_2
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    :cond_4
    invoke-interface {v3, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_2

    :cond_5
    return-object v3
.end method

.method private e()V
    .locals 10

    const/4 v9, -0x1

    const/4 v8, -0x2

    iget-object v0, p0, Lcom/google/android/youtube/app/compat/c;->c:Landroid/view/ViewGroup;

    invoke-virtual {v0}, Landroid/view/ViewGroup;->removeAllViews()V

    iget-object v0, p0, Lcom/google/android/youtube/app/compat/c;->k:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/youtube/app/compat/t;

    invoke-interface {v0}, Lcom/google/android/youtube/app/compat/t;->d()Landroid/view/View;

    move-result-object v1

    if-nez v1, :cond_2

    invoke-interface {v0}, Lcom/google/android/youtube/app/compat/t;->i()I

    move-result v1

    and-int/lit8 v1, v1, 0x4

    if-eqz v1, :cond_1

    new-instance v1, Landroid/widget/TextView;

    iget-object v2, p0, Lcom/google/android/youtube/app/compat/c;->a:Landroid/app/Activity;

    invoke-direct {v1, v2}, Landroid/widget/TextView;-><init>(Landroid/content/Context;)V

    invoke-interface {v0}, Lcom/google/android/youtube/app/compat/t;->g()Ljava/lang/CharSequence;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    const/16 v2, 0x10

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setGravity(I)V

    const/4 v2, 0x0

    iget v4, p0, Lcom/google/android/youtube/app/compat/c;->q:I

    int-to-float v4, v4

    invoke-virtual {v1, v2, v4}, Landroid/widget/TextView;->setTextSize(IF)V

    iget v2, p0, Lcom/google/android/youtube/app/compat/c;->r:I

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setTextColor(I)V

    new-instance v2, Lcom/google/android/youtube/app/ui/dg;

    iget-object v4, p0, Lcom/google/android/youtube/app/compat/c;->a:Landroid/app/Activity;

    invoke-direct {v2, v4}, Lcom/google/android/youtube/app/ui/dg;-><init>(Landroid/content/Context;)V

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setTransformationMethod(Landroid/text/method/TransformationMethod;)V

    :goto_1
    new-instance v2, Landroid/widget/LinearLayout$LayoutParams;

    invoke-direct {v2, v8, v9}, Landroid/widget/LinearLayout$LayoutParams;-><init>(II)V

    iget v4, p0, Lcom/google/android/youtube/app/compat/c;->p:I

    invoke-virtual {v1, v4}, Landroid/view/View;->setBackgroundResource(I)V

    iget v4, p0, Lcom/google/android/youtube/app/compat/c;->l:I

    iget v5, p0, Lcom/google/android/youtube/app/compat/c;->n:I

    iget v6, p0, Lcom/google/android/youtube/app/compat/c;->m:I

    iget v7, p0, Lcom/google/android/youtube/app/compat/c;->o:I

    invoke-virtual {v1, v4, v5, v6, v7}, Landroid/view/View;->setPadding(IIII)V

    new-instance v4, Lcom/google/android/youtube/app/compat/e;

    iget-object v5, p0, Lcom/google/android/youtube/app/compat/c;->a:Landroid/app/Activity;

    invoke-direct {v4, v5, v0}, Lcom/google/android/youtube/app/compat/e;-><init>(Landroid/app/Activity;Lcom/google/android/youtube/app/compat/t;)V

    invoke-virtual {v1, v4}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    move-object v0, v2

    :cond_0
    :goto_2
    iget-object v2, p0, Lcom/google/android/youtube/app/compat/c;->c:Landroid/view/ViewGroup;

    invoke-virtual {v2, v1, v0}, Landroid/view/ViewGroup;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    goto :goto_0

    :cond_1
    new-instance v1, Landroid/widget/ImageButton;

    iget-object v2, p0, Lcom/google/android/youtube/app/compat/c;->a:Landroid/app/Activity;

    invoke-direct {v1, v2}, Landroid/widget/ImageButton;-><init>(Landroid/content/Context;)V

    invoke-interface {v0}, Lcom/google/android/youtube/app/compat/t;->e()Landroid/graphics/drawable/Drawable;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/ImageButton;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    goto :goto_1

    :cond_2
    invoke-interface {v0}, Lcom/google/android/youtube/app/compat/t;->d()Landroid/view/View;

    move-result-object v1

    invoke-virtual {v1}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    if-nez v0, :cond_0

    new-instance v0, Landroid/widget/LinearLayout$LayoutParams;

    invoke-direct {v0, v8, v9}, Landroid/widget/LinearLayout$LayoutParams;-><init>(II)V

    goto :goto_2

    :cond_3
    return-void
.end method


# virtual methods
.method public final a()V
    .locals 2

    iget-object v0, p0, Lcom/google/android/youtube/app/compat/c;->b:Landroid/view/ViewGroup;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->setVisibility(I)V

    return-void
.end method

.method public final a(I)V
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/app/compat/c;->e:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(I)V

    return-void
.end method

.method public final a(II)V
    .locals 5

    const/16 v4, 0x8

    const/4 v3, 0x0

    and-int v0, p1, p2

    iget v1, p0, Lcom/google/android/youtube/app/compat/c;->s:I

    xor-int/lit8 v2, p2, -0x1

    and-int/2addr v1, v2

    or-int/2addr v0, v1

    iput v0, p0, Lcom/google/android/youtube/app/compat/c;->s:I

    iget v0, p0, Lcom/google/android/youtube/app/compat/c;->s:I

    and-int/lit8 v0, v0, 0x4

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/youtube/app/compat/c;->h:Landroid/widget/ImageView;

    invoke-virtual {v0, v3}, Landroid/widget/ImageView;->setVisibility(I)V

    :goto_0
    iget v0, p0, Lcom/google/android/youtube/app/compat/c;->s:I

    and-int/lit8 v0, v0, 0x8

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/youtube/app/compat/c;->e:Landroid/widget/TextView;

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setVisibility(I)V

    :goto_1
    iget v0, p0, Lcom/google/android/youtube/app/compat/c;->s:I

    and-int/lit8 v0, v0, 0x10

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/google/android/youtube/app/compat/c;->i:Landroid/widget/FrameLayout;

    invoke-virtual {v0, v3}, Landroid/widget/FrameLayout;->setVisibility(I)V

    :goto_2
    return-void

    :cond_0
    iget-object v0, p0, Lcom/google/android/youtube/app/compat/c;->h:Landroid/widget/ImageView;

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    goto :goto_0

    :cond_1
    iget-object v0, p0, Lcom/google/android/youtube/app/compat/c;->e:Landroid/widget/TextView;

    invoke-virtual {v0, v4}, Landroid/widget/TextView;->setVisibility(I)V

    goto :goto_1

    :cond_2
    iget-object v0, p0, Lcom/google/android/youtube/app/compat/c;->i:Landroid/widget/FrameLayout;

    invoke-virtual {v0, v4}, Landroid/widget/FrameLayout;->setVisibility(I)V

    goto :goto_2
.end method

.method public final a(Landroid/graphics/drawable/Drawable;)V
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/app/compat/c;->b:Landroid/view/ViewGroup;

    invoke-virtual {v0, p1}, Landroid/view/ViewGroup;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    invoke-static {p1}, Lcom/google/android/youtube/core/utils/Util;->a(Landroid/graphics/drawable/Drawable;)V

    return-void
.end method

.method public final a(Landroid/view/View;)V
    .locals 3

    invoke-virtual {p1}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    if-eqz v0, :cond_1

    new-instance v0, Lcom/google/android/youtube/app/compat/SupportActionBar$LayoutParams;

    invoke-virtual {p1}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/google/android/youtube/app/compat/SupportActionBar$LayoutParams;-><init>(Landroid/view/ViewGroup$LayoutParams;)V

    :goto_0
    iget-object v1, p0, Lcom/google/android/youtube/app/compat/c;->i:Landroid/widget/FrameLayout;

    invoke-virtual {v1}, Landroid/widget/FrameLayout;->removeAllViews()V

    if-eqz p1, :cond_0

    iget-object v1, p0, Lcom/google/android/youtube/app/compat/c;->i:Landroid/widget/FrameLayout;

    invoke-virtual {v1, p1, v0}, Landroid/widget/FrameLayout;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    :cond_0
    iput-object p1, p0, Lcom/google/android/youtube/app/compat/c;->j:Landroid/view/View;

    return-void

    :cond_1
    new-instance v0, Lcom/google/android/youtube/app/compat/SupportActionBar$LayoutParams;

    const/4 v1, -0x2

    const/4 v2, -0x1

    invoke-direct {v0, v1, v2}, Lcom/google/android/youtube/app/compat/SupportActionBar$LayoutParams;-><init>(II)V

    goto :goto_0
.end method

.method public final a(Lcom/google/android/youtube/app/compat/h;)V
    .locals 0

    return-void
.end method

.method public final a(Ljava/lang/CharSequence;)V
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/app/compat/c;->e:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    return-void
.end method

.method public final a(Z)V
    .locals 2

    const/16 v1, 0x10

    if-eqz p1, :cond_0

    move v0, v1

    :goto_0
    invoke-virtual {p0, v0, v1}, Lcom/google/android/youtube/app/compat/c;->a(II)V

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final a(Lcom/google/android/youtube/app/compat/m;)Z
    .locals 2

    const/4 v0, 0x0

    move v1, v0

    :goto_0
    invoke-virtual {p1}, Lcom/google/android/youtube/app/compat/m;->d()I

    move-result v0

    if-ge v1, v0, :cond_0

    invoke-virtual {p1, v1}, Lcom/google/android/youtube/app/compat/m;->d(I)Lcom/google/android/youtube/app/compat/t;

    move-result-object v0

    check-cast v0, Lcom/google/android/youtube/app/compat/z;

    invoke-virtual {v0, p0}, Lcom/google/android/youtube/app/compat/z;->a(Lcom/google/android/youtube/app/compat/ab;)V

    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    :cond_0
    invoke-super {p0, p1}, Lcom/google/android/youtube/app/compat/SupportActionBar;->a(Lcom/google/android/youtube/app/compat/m;)Z

    move-result v0

    return v0
.end method

.method public final b()V
    .locals 2

    iget-object v0, p0, Lcom/google/android/youtube/app/compat/c;->b:Landroid/view/ViewGroup;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->setVisibility(I)V

    return-void
.end method

.method public final b(I)V
    .locals 0

    return-void
.end method

.method public final b(Z)V
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/app/compat/c;->g:Landroid/view/View;

    invoke-virtual {v0, p1}, Landroid/view/View;->setClickable(Z)V

    return-void
.end method

.method public final b(Lcom/google/android/youtube/app/compat/m;)Z
    .locals 7

    const/4 v1, 0x0

    iget-object v0, p0, Lcom/google/android/youtube/app/compat/c;->a:Landroid/app/Activity;

    new-instance v3, Ljava/util/LinkedList;

    invoke-direct {v3}, Ljava/util/LinkedList;-><init>()V

    invoke-static {v0}, Lcom/google/android/youtube/app/compat/c;->a(Landroid/content/Context;)I

    move-result v4

    move v0, v1

    move v2, v1

    :goto_0
    invoke-virtual {p1}, Lcom/google/android/youtube/app/compat/m;->d()I

    move-result v5

    if-ge v0, v5, :cond_1

    invoke-virtual {p1, v0}, Lcom/google/android/youtube/app/compat/m;->d(I)Lcom/google/android/youtube/app/compat/t;

    move-result-object v5

    invoke-interface {v5}, Lcom/google/android/youtube/app/compat/t;->h()Z

    move-result v6

    if-eqz v6, :cond_0

    invoke-interface {v5}, Lcom/google/android/youtube/app/compat/t;->i()I

    move-result v5

    and-int/lit8 v5, v5, 0x2

    if-eqz v5, :cond_0

    add-int/lit8 v2, v2, 0x1

    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_1
    :goto_1
    invoke-virtual {p1}, Lcom/google/android/youtube/app/compat/m;->d()I

    move-result v0

    if-ge v1, v0, :cond_4

    invoke-virtual {p1, v1}, Lcom/google/android/youtube/app/compat/m;->d(I)Lcom/google/android/youtube/app/compat/t;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/youtube/app/compat/t;->h()Z

    move-result v5

    if-eqz v5, :cond_2

    invoke-interface {v0}, Lcom/google/android/youtube/app/compat/t;->i()I

    move-result v5

    and-int/lit8 v5, v5, 0x2

    if-eqz v5, :cond_3

    invoke-interface {v3, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :cond_2
    :goto_2
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    :cond_3
    invoke-interface {v0}, Lcom/google/android/youtube/app/compat/t;->i()I

    move-result v5

    and-int/lit8 v5, v5, 0x1

    if-eqz v5, :cond_2

    if-ge v2, v4, :cond_2

    invoke-interface {v3, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    add-int/lit8 v2, v2, 0x1

    goto :goto_2

    :cond_4
    iput-object v3, p0, Lcom/google/android/youtube/app/compat/c;->k:Ljava/util/List;

    invoke-direct {p0}, Lcom/google/android/youtube/app/compat/c;->e()V

    const/4 v0, 0x1

    return v0
.end method

.method public final c()Z
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/app/compat/c;->b:Landroid/view/ViewGroup;

    invoke-virtual {v0}, Landroid/view/ViewGroup;->getVisibility()I

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final d()V
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/app/compat/c;->a:Landroid/app/Activity;

    check-cast v0, Lcom/google/android/youtube/app/compat/i;

    invoke-interface {v0}, Lcom/google/android/youtube/app/compat/i;->l_()V

    return-void
.end method
