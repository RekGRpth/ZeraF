.class final Lcom/google/android/youtube/app/compat/l;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/youtube/core/v11/ui/b;


# instance fields
.field private final a:Landroid/app/Activity;

.field private final b:Lcom/google/android/youtube/core/player/overlay/PlayerOverlaysLayout;

.field private final c:Lcom/google/android/youtube/app/compat/SupportActionBar;

.field private final d:I

.field private e:Z


# direct methods
.method public constructor <init>(Lcom/google/android/youtube/app/honeycomb/phone/YouTubeActivity;Lcom/google/android/youtube/core/player/overlay/PlayerOverlaysLayout;)V
    .locals 3

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const-string v0, "activity cannot be null"

    invoke-static {p1, v0}, Lcom/google/android/youtube/core/utils/s;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/Activity;

    iput-object v0, p0, Lcom/google/android/youtube/app/compat/l;->a:Landroid/app/Activity;

    invoke-virtual {p1}, Lcom/google/android/youtube/app/honeycomb/phone/YouTubeActivity;->l()Lcom/google/android/youtube/app/compat/SupportActionBar;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/youtube/app/compat/l;->c:Lcom/google/android/youtube/app/compat/SupportActionBar;

    iput-object p2, p0, Lcom/google/android/youtube/app/compat/l;->b:Lcom/google/android/youtube/core/player/overlay/PlayerOverlaysLayout;

    const v0, 0x7f0d0024

    sget-object v1, Lcom/google/android/youtube/c;->i:[I

    invoke-virtual {p1, v0, v1}, Lcom/google/android/youtube/app/honeycomb/phone/YouTubeActivity;->obtainStyledAttributes(I[I)Landroid/content/res/TypedArray;

    move-result-object v0

    const/4 v1, 0x0

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/content/res/TypedArray;->getDimension(IF)F

    move-result v1

    float-to-int v1, v1

    iput v1, p0, Lcom/google/android/youtube/app/compat/l;->d:I

    invoke-virtual {v0}, Landroid/content/res/TypedArray;->recycle()V

    return-void
.end method


# virtual methods
.method public final a()V
    .locals 0

    return-void
.end method

.method public final a(Z)V
    .locals 5

    const/16 v1, 0x400

    const/4 v2, 0x0

    iget-boolean v0, p0, Lcom/google/android/youtube/app/compat/l;->e:Z

    if-eq v0, p1, :cond_3

    iput-boolean p1, p0, Lcom/google/android/youtube/app/compat/l;->e:Z

    iget-object v0, p0, Lcom/google/android/youtube/app/compat/l;->a:Landroid/app/Activity;

    invoke-virtual {v0}, Landroid/app/Activity;->getWindow()Landroid/view/Window;

    move-result-object v3

    if-eqz p1, :cond_1

    move v0, v1

    :goto_0
    invoke-virtual {v3, v0, v1}, Landroid/view/Window;->setFlags(II)V

    iget-object v0, p0, Lcom/google/android/youtube/app/compat/l;->b:Lcom/google/android/youtube/core/player/overlay/PlayerOverlaysLayout;

    invoke-virtual {v0}, Lcom/google/android/youtube/core/player/overlay/PlayerOverlaysLayout;->getChildCount()I

    move-result v3

    move v1, v2

    :goto_1
    if-ge v1, v3, :cond_3

    iget-object v0, p0, Lcom/google/android/youtube/app/compat/l;->b:Lcom/google/android/youtube/core/player/overlay/PlayerOverlaysLayout;

    invoke-virtual {v0, v1}, Lcom/google/android/youtube/core/player/overlay/PlayerOverlaysLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v4

    invoke-virtual {v4}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Lcom/google/android/youtube/core/player/overlay/PlayerOverlaysLayout$LayoutParams;

    iget-boolean v0, v0, Lcom/google/android/youtube/core/player/overlay/PlayerOverlaysLayout$LayoutParams;->a:Z

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/google/android/youtube/app/compat/l;->e:Z

    if-eqz v0, :cond_2

    iget v0, p0, Lcom/google/android/youtube/app/compat/l;->d:I

    :goto_2
    invoke-virtual {v4, v2, v0, v2, v2}, Landroid/view/View;->setPadding(IIII)V

    :cond_0
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_1

    :cond_1
    move v0, v2

    goto :goto_0

    :cond_2
    move v0, v2

    goto :goto_2

    :cond_3
    return-void
.end method

.method public final b(Z)V
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/youtube/app/compat/l;->e:Z

    if-eqz v0, :cond_0

    if-eqz p1, :cond_0

    iget-object v0, p0, Lcom/google/android/youtube/app/compat/l;->c:Lcom/google/android/youtube/app/compat/SupportActionBar;

    invoke-virtual {v0}, Lcom/google/android/youtube/app/compat/SupportActionBar;->a()V

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/google/android/youtube/app/compat/l;->c:Lcom/google/android/youtube/app/compat/SupportActionBar;

    invoke-virtual {v0}, Lcom/google/android/youtube/app/compat/SupportActionBar;->b()V

    goto :goto_0
.end method
