.class public final Lcom/google/android/youtube/app/ui/da;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field private final a:Landroid/content/Context;

.field private final b:Landroid/content/DialogInterface$OnClickListener;

.field private final c:Landroid/content/SharedPreferences;

.field private final d:Landroid/view/LayoutInflater;

.field private final e:Landroid/view/View;

.field private final f:Landroid/app/AlertDialog;

.field private g:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/google/android/youtube/app/ui/da;-><init>(Landroid/content/Context;Landroid/content/DialogInterface$OnClickListener;)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/content/DialogInterface$OnClickListener;)V
    .locals 4

    const/4 v3, 0x0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const-string v0, "context may not be null"

    invoke-static {p1, v0}, Lcom/google/android/youtube/core/utils/s;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/Context;

    iput-object v0, p0, Lcom/google/android/youtube/app/ui/da;->a:Landroid/content/Context;

    iput-object p2, p0, Lcom/google/android/youtube/app/ui/da;->b:Landroid/content/DialogInterface$OnClickListener;

    const-string v0, "youtube"

    invoke-virtual {p1, v0, v3}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/youtube/app/ui/da;->c:Landroid/content/SharedPreferences;

    iget-object v0, p0, Lcom/google/android/youtube/app/ui/da;->c:Landroid/content/SharedPreferences;

    const-string v1, "upload_policy"

    invoke-interface {v0, v1}, Landroid/content/SharedPreferences;->contains(Ljava/lang/String;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/google/android/youtube/app/ui/da;->g:Z

    invoke-static {p1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/youtube/app/ui/da;->d:Landroid/view/LayoutInflater;

    iget-object v0, p0, Lcom/google/android/youtube/app/ui/da;->d:Landroid/view/LayoutInflater;

    const v1, 0x7f0400a8

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/youtube/app/ui/da;->e:Landroid/view/View;

    iget-boolean v0, p0, Lcom/google/android/youtube/app/ui/da;->g:Z

    if-nez v0, :cond_0

    const-string v0, "connectivity"

    invoke-virtual {p1, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/net/ConnectivityManager;

    invoke-virtual {v0, v3}, Landroid/net/ConnectivityManager;->getNetworkInfo(I)Landroid/net/NetworkInfo;

    move-result-object v0

    if-nez v0, :cond_0

    const v0, 0x7f0b0028

    invoke-direct {p0, v0}, Lcom/google/android/youtube/app/ui/da;->a(I)V

    :cond_0
    iget-object v0, p0, Lcom/google/android/youtube/app/ui/da;->e:Landroid/view/View;

    const v1, 0x7f070166

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/RadioButton;

    new-instance v1, Lcom/google/android/youtube/app/ui/db;

    invoke-direct {v1, p0, v0}, Lcom/google/android/youtube/app/ui/db;-><init>(Lcom/google/android/youtube/app/ui/da;Landroid/widget/RadioButton;)V

    new-instance v0, Lcom/google/android/youtube/core/ui/w;

    invoke-direct {v0, p1}, Lcom/google/android/youtube/core/ui/w;-><init>(Landroid/content/Context;)V

    const v2, 0x7f0b00c7

    invoke-virtual {v0, v2}, Lcom/google/android/youtube/core/ui/w;->setTitle(I)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    iget-object v2, p0, Lcom/google/android/youtube/app/ui/da;->e:Landroid/view/View;

    invoke-virtual {v0, v2}, Landroid/app/AlertDialog$Builder;->setView(Landroid/view/View;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    const v2, 0x104000a

    invoke-virtual {v0, v2, v1}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    invoke-virtual {v0, v3}, Landroid/app/AlertDialog$Builder;->setCancelable(Z)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/youtube/app/ui/da;->f:Landroid/app/AlertDialog;

    return-void
.end method

.method static synthetic a(Lcom/google/android/youtube/app/ui/da;)Landroid/content/DialogInterface$OnClickListener;
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/app/ui/da;->b:Landroid/content/DialogInterface$OnClickListener;

    return-object v0
.end method

.method private a(I)V
    .locals 3

    iget-object v0, p0, Lcom/google/android/youtube/app/ui/da;->c:Landroid/content/SharedPreferences;

    invoke-static {v0}, Lcom/google/android/youtube/app/compat/ac;->a(Landroid/content/SharedPreferences;)Lcom/google/android/youtube/app/compat/ad;

    move-result-object v0

    const-string v1, "upload_policy"

    iget-object v2, p0, Lcom/google/android/youtube/app/ui/da;->a:Landroid/content/Context;

    invoke-virtual {v2, p1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/google/android/youtube/app/compat/ad;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/youtube/app/compat/ad;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/youtube/app/compat/ad;->a()V

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/youtube/app/ui/da;->g:Z

    return-void
.end method

.method static synthetic a(Lcom/google/android/youtube/app/ui/da;I)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/google/android/youtube/app/ui/da;->a(I)V

    return-void
.end method


# virtual methods
.method public final a()Landroid/app/Dialog;
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/app/ui/da;->f:Landroid/app/AlertDialog;

    return-object v0
.end method

.method public final b()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/youtube/app/ui/da;->g:Z

    return v0
.end method
