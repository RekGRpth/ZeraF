.class final Lcom/google/android/youtube/app/ui/ae;
.super Lcom/google/android/youtube/core/ui/e;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/youtube/core/async/n;


# instance fields
.field final synthetic a:Lcom/google/android/youtube/app/ui/ac;

.field private final b:Ljava/lang/String;

.field private final d:Z

.field private final e:Lcom/google/android/youtube/app/ui/af;


# direct methods
.method public constructor <init>(Lcom/google/android/youtube/app/ui/ac;Ljava/lang/String;ZLcom/google/android/youtube/app/ui/af;)V
    .locals 1

    iput-object p1, p0, Lcom/google/android/youtube/app/ui/ae;->a:Lcom/google/android/youtube/app/ui/ac;

    invoke-static {p1}, Lcom/google/android/youtube/app/ui/ac;->c(Lcom/google/android/youtube/app/ui/ac;)Lcom/google/android/youtube/core/e;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/google/android/youtube/core/ui/e;-><init>(Lcom/google/android/youtube/core/e;)V

    iput-object p2, p0, Lcom/google/android/youtube/app/ui/ae;->b:Ljava/lang/String;

    iput-boolean p3, p0, Lcom/google/android/youtube/app/ui/ae;->d:Z

    iput-object p4, p0, Lcom/google/android/youtube/app/ui/ae;->e:Lcom/google/android/youtube/app/ui/af;

    return-void
.end method


# virtual methods
.method public final a(Lcom/google/android/youtube/core/model/UserAuth;)V
    .locals 4

    iget-object v0, p0, Lcom/google/android/youtube/app/ui/ae;->a:Lcom/google/android/youtube/app/ui/ac;

    invoke-static {v0}, Lcom/google/android/youtube/app/ui/ac;->d(Lcom/google/android/youtube/app/ui/ac;)Lcom/google/android/youtube/core/client/bc;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/youtube/app/ui/ae;->b:Ljava/lang/String;

    iget-boolean v2, p0, Lcom/google/android/youtube/app/ui/ae;->d:Z

    iget-object v3, p0, Lcom/google/android/youtube/app/ui/ae;->a:Lcom/google/android/youtube/app/ui/ac;

    invoke-static {v3}, Lcom/google/android/youtube/app/ui/ac;->a(Lcom/google/android/youtube/app/ui/ac;)Landroid/app/Activity;

    move-result-object v3

    invoke-static {v3, p0}, Lcom/google/android/youtube/core/async/h;->a(Landroid/app/Activity;Lcom/google/android/youtube/core/async/n;)Lcom/google/android/youtube/core/async/h;

    move-result-object v3

    invoke-interface {v0, v1, v2, p1, v3}, Lcom/google/android/youtube/core/client/bc;->a(Ljava/lang/String;ZLcom/google/android/youtube/core/model/UserAuth;Lcom/google/android/youtube/core/async/n;)V

    return-void
.end method

.method public final synthetic a(Ljava/lang/Object;Ljava/lang/Exception;)V
    .locals 1

    const-string v0, "Error creating playlist"

    invoke-static {v0, p2}, Lcom/google/android/youtube/core/L;->a(Ljava/lang/String;Ljava/lang/Throwable;)V

    iget-object v0, p0, Lcom/google/android/youtube/app/ui/ae;->c:Lcom/google/android/youtube/core/e;

    invoke-virtual {v0, p2}, Lcom/google/android/youtube/core/e;->b(Ljava/lang/Throwable;)V

    return-void
.end method

.method public final bridge synthetic a(Ljava/lang/Object;Ljava/lang/Object;)V
    .locals 3

    check-cast p2, Lcom/google/android/youtube/core/model/Playlist;

    iget-object v0, p0, Lcom/google/android/youtube/app/ui/ae;->e:Lcom/google/android/youtube/app/ui/af;

    invoke-interface {v0, p2}, Lcom/google/android/youtube/app/ui/af;->a(Lcom/google/android/youtube/core/model/Playlist;)V

    iget-object v0, p0, Lcom/google/android/youtube/app/ui/ae;->a:Lcom/google/android/youtube/app/ui/ac;

    invoke-static {v0}, Lcom/google/android/youtube/app/ui/ac;->a(Lcom/google/android/youtube/app/ui/ac;)Landroid/app/Activity;

    move-result-object v0

    const v1, 0x7f0b0210

    const/4 v2, 0x1

    invoke-static {v0, v1, v2}, Lcom/google/android/youtube/core/utils/Util;->a(Landroid/content/Context;II)V

    return-void
.end method
