.class final Lcom/google/android/youtube/app/ui/du;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/youtube/core/async/bk;
.implements Lcom/google/android/youtube/core/async/n;


# instance fields
.field final synthetic a:Lcom/google/android/youtube/app/ui/dh;

.field private b:Lcom/google/android/youtube/core/model/UserAuth;


# direct methods
.method private constructor <init>(Lcom/google/android/youtube/app/ui/dh;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/youtube/app/ui/du;->a:Lcom/google/android/youtube/app/ui/dh;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/google/android/youtube/app/ui/dh;B)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/google/android/youtube/app/ui/du;-><init>(Lcom/google/android/youtube/app/ui/dh;)V

    return-void
.end method

.method static synthetic a(Lcom/google/android/youtube/app/ui/du;Landroid/widget/EditText;)V
    .locals 5

    invoke-virtual {p1}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v1

    const/4 v2, 0x2

    if-lt v1, v2, :cond_1

    iget-object v1, p0, Lcom/google/android/youtube/app/ui/du;->a:Lcom/google/android/youtube/app/ui/dh;

    invoke-static {v1}, Lcom/google/android/youtube/app/ui/dh;->h(Lcom/google/android/youtube/app/ui/dh;)Lcom/google/android/youtube/core/Analytics;

    move-result-object v1

    const-string v2, "Comment"

    invoke-interface {v1, v2}, Lcom/google/android/youtube/core/Analytics;->b(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/google/android/youtube/app/ui/du;->a:Lcom/google/android/youtube/app/ui/dh;

    invoke-static {v1}, Lcom/google/android/youtube/app/ui/dh;->e(Lcom/google/android/youtube/app/ui/dh;)Lcom/google/android/youtube/core/client/bc;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/youtube/app/ui/du;->a:Lcom/google/android/youtube/app/ui/dh;

    invoke-static {v2}, Lcom/google/android/youtube/app/ui/dh;->g(Lcom/google/android/youtube/app/ui/dh;)Lcom/google/android/youtube/core/model/Video;

    move-result-object v2

    iget-object v2, v2, Lcom/google/android/youtube/core/model/Video;->commentsUri:Landroid/net/Uri;

    iget-object v3, p0, Lcom/google/android/youtube/app/ui/du;->b:Lcom/google/android/youtube/core/model/UserAuth;

    iget-object v4, p0, Lcom/google/android/youtube/app/ui/du;->a:Lcom/google/android/youtube/app/ui/dh;

    iget-object v4, v4, Lcom/google/android/youtube/app/ui/dh;->a:Landroid/app/Activity;

    invoke-static {v4, p0}, Lcom/google/android/youtube/core/async/h;->a(Landroid/app/Activity;Lcom/google/android/youtube/core/async/n;)Lcom/google/android/youtube/core/async/h;

    move-result-object v4

    invoke-interface {v1, v2, v3, v0, v4}, Lcom/google/android/youtube/core/client/bc;->a(Landroid/net/Uri;Lcom/google/android/youtube/core/model/UserAuth;Ljava/lang/String;Lcom/google/android/youtube/core/async/n;)V

    const/4 v0, 0x0

    invoke-virtual {p1, v0}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    invoke-virtual {p1}, Landroid/widget/EditText;->clearFocus()V

    iget-object v0, p0, Lcom/google/android/youtube/app/ui/du;->a:Lcom/google/android/youtube/app/ui/dh;

    iget-object v0, v0, Lcom/google/android/youtube/app/ui/dh;->a:Landroid/app/Activity;

    const-string v1, "input_method"

    invoke-virtual {v0, v1}, Landroid/app/Activity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/inputmethod/InputMethodManager;

    invoke-virtual {p1}, Landroid/widget/EditText;->getWindowToken()Landroid/os/IBinder;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/view/inputmethod/InputMethodManager;->hideSoftInputFromWindow(Landroid/os/IBinder;I)Z

    :cond_0
    return-void

    :cond_1
    iget-object v1, p0, Lcom/google/android/youtube/app/ui/du;->a:Lcom/google/android/youtube/app/ui/dh;

    invoke-static {v1}, Lcom/google/android/youtube/app/ui/dh;->b(Lcom/google/android/youtube/app/ui/dh;)Lcom/google/android/youtube/core/e;

    move-result-object v1

    const v2, 0x7f0b01f8

    invoke-virtual {v1, v2}, Lcom/google/android/youtube/core/e;->a(I)V

    invoke-virtual {p1, v0}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lcom/google/android/youtube/app/ui/du;->a:Lcom/google/android/youtube/app/ui/dh;

    invoke-static {v0}, Lcom/google/android/youtube/app/ui/dh;->i(Lcom/google/android/youtube/app/ui/dh;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/youtube/app/ui/dn;

    invoke-interface {v0}, Lcom/google/android/youtube/app/ui/dn;->i()V

    goto :goto_0
.end method


# virtual methods
.method public final a()Landroid/app/Dialog;
    .locals 6

    const/4 v2, 0x5

    iget-object v0, p0, Lcom/google/android/youtube/app/ui/du;->a:Lcom/google/android/youtube/app/ui/dh;

    iget-object v0, v0, Lcom/google/android/youtube/app/ui/dh;->a:Landroid/app/Activity;

    invoke-virtual {v0}, Landroid/app/Activity;->getLayoutInflater()Landroid/view/LayoutInflater;

    move-result-object v0

    const v1, 0x7f040008

    const/4 v3, 0x0

    const/4 v4, 0x0

    invoke-virtual {v0, v1, v3, v4}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/EditText;

    new-instance v0, Lcom/google/android/youtube/core/ui/w;

    iget-object v3, p0, Lcom/google/android/youtube/app/ui/du;->a:Lcom/google/android/youtube/app/ui/dh;

    iget-object v3, v3, Lcom/google/android/youtube/app/ui/dh;->a:Landroid/app/Activity;

    invoke-direct {v0, v3}, Lcom/google/android/youtube/core/ui/w;-><init>(Landroid/content/Context;)V

    const v3, 0x7f0b01f4

    invoke-virtual {v0, v3}, Lcom/google/android/youtube/core/ui/w;->setTitle(I)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    iget-object v3, p0, Lcom/google/android/youtube/app/ui/du;->a:Lcom/google/android/youtube/app/ui/dh;

    iget-object v3, v3, Lcom/google/android/youtube/app/ui/dh;->a:Landroid/app/Activity;

    const v4, 0x7f0b01f3

    invoke-virtual {v3, v4}, Landroid/app/Activity;->getString(I)Ljava/lang/String;

    move-result-object v3

    new-instance v4, Lcom/google/android/youtube/app/ui/dw;

    invoke-direct {v4, p0, v1}, Lcom/google/android/youtube/app/ui/dw;-><init>(Lcom/google/android/youtube/app/ui/du;Landroid/widget/EditText;)V

    invoke-virtual {v0, v3, v4}, Landroid/app/AlertDialog$Builder;->setPositiveButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    iget-object v3, p0, Lcom/google/android/youtube/app/ui/du;->a:Lcom/google/android/youtube/app/ui/dh;

    iget-object v3, v3, Lcom/google/android/youtube/app/ui/dh;->a:Landroid/app/Activity;

    const/high16 v4, 0x1040000

    invoke-virtual {v3, v4}, Landroid/app/Activity;->getString(I)Ljava/lang/String;

    move-result-object v3

    new-instance v4, Lcom/google/android/youtube/app/ui/dv;

    invoke-direct {v4, p0, v1}, Lcom/google/android/youtube/app/ui/dv;-><init>(Lcom/google/android/youtube/app/ui/du;Landroid/widget/EditText;)V

    invoke-virtual {v0, v3, v4}, Landroid/app/AlertDialog$Builder;->setNegativeButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v0

    move v3, v2

    move v4, v2

    move v5, v2

    invoke-virtual/range {v0 .. v5}, Landroid/app/AlertDialog;->setView(Landroid/view/View;IIII)V

    new-instance v2, Lcom/google/android/youtube/app/ui/dx;

    invoke-direct {v2, p0, v1}, Lcom/google/android/youtube/app/ui/dx;-><init>(Lcom/google/android/youtube/app/ui/du;Landroid/widget/EditText;)V

    invoke-virtual {v0, v2}, Landroid/app/AlertDialog;->setOnShowListener(Landroid/content/DialogInterface$OnShowListener;)V

    return-object v0
.end method

.method public final a(Lcom/google/android/youtube/core/model/UserAuth;)V
    .locals 2

    iput-object p1, p0, Lcom/google/android/youtube/app/ui/du;->b:Lcom/google/android/youtube/core/model/UserAuth;

    iget-object v0, p0, Lcom/google/android/youtube/app/ui/du;->a:Lcom/google/android/youtube/app/ui/dh;

    iget-object v0, v0, Lcom/google/android/youtube/app/ui/dh;->a:Landroid/app/Activity;

    const/16 v1, 0x3e8

    invoke-virtual {v0, v1}, Landroid/app/Activity;->showDialog(I)V

    return-void
.end method

.method public final synthetic a(Ljava/lang/Object;Ljava/lang/Exception;)V
    .locals 2

    if-eqz p2, :cond_0

    const/16 v0, 0x193

    invoke-static {p2, v0}, Lcom/google/android/youtube/core/e;->a(Ljava/lang/Throwable;I)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/youtube/app/ui/du;->a:Lcom/google/android/youtube/app/ui/dh;

    invoke-static {v0}, Lcom/google/android/youtube/app/ui/dh;->b(Lcom/google/android/youtube/app/ui/dh;)Lcom/google/android/youtube/core/e;

    move-result-object v0

    const v1, 0x7f0b00d2

    invoke-virtual {v0, v1}, Lcom/google/android/youtube/core/e;->a(I)V

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/google/android/youtube/app/ui/du;->a:Lcom/google/android/youtube/app/ui/dh;

    invoke-static {v0}, Lcom/google/android/youtube/app/ui/dh;->b(Lcom/google/android/youtube/app/ui/dh;)Lcom/google/android/youtube/core/e;

    move-result-object v0

    const v1, 0x7f0b00d1

    invoke-virtual {v0, v1}, Lcom/google/android/youtube/core/e;->a(I)V

    goto :goto_0
.end method

.method public final synthetic a(Ljava/lang/Object;Ljava/lang/Object;)V
    .locals 3

    check-cast p2, Lcom/google/android/youtube/core/model/Comment;

    iget-object v0, p0, Lcom/google/android/youtube/app/ui/du;->a:Lcom/google/android/youtube/app/ui/dh;

    iget-object v0, v0, Lcom/google/android/youtube/app/ui/dh;->a:Landroid/app/Activity;

    const v1, 0x7f0b01f7

    const/4 v2, 0x0

    invoke-static {v0, v1, v2}, Lcom/google/android/youtube/core/utils/Util;->a(Landroid/content/Context;II)V

    iget-object v0, p0, Lcom/google/android/youtube/app/ui/du;->a:Lcom/google/android/youtube/app/ui/dh;

    invoke-static {v0}, Lcom/google/android/youtube/app/ui/dh;->i(Lcom/google/android/youtube/app/ui/dh;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/youtube/app/ui/dn;

    invoke-interface {v0, p2}, Lcom/google/android/youtube/app/ui/dn;->a(Lcom/google/android/youtube/core/model/Comment;)V

    goto :goto_0

    :cond_0
    return-void
.end method

.method public final a(Ljava/lang/String;Ljava/lang/Exception;)V
    .locals 2

    iget-object v0, p0, Lcom/google/android/youtube/app/ui/du;->a:Lcom/google/android/youtube/app/ui/dh;

    invoke-static {v0}, Lcom/google/android/youtube/app/ui/dh;->b(Lcom/google/android/youtube/app/ui/dh;)Lcom/google/android/youtube/core/e;

    move-result-object v0

    invoke-virtual {v0, p2}, Lcom/google/android/youtube/core/e;->b(Ljava/lang/Throwable;)V

    iget-object v0, p0, Lcom/google/android/youtube/app/ui/du;->a:Lcom/google/android/youtube/app/ui/dh;

    invoke-static {v0}, Lcom/google/android/youtube/app/ui/dh;->i(Lcom/google/android/youtube/app/ui/dh;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/youtube/app/ui/dn;

    invoke-interface {v0}, Lcom/google/android/youtube/app/ui/dn;->i()V

    goto :goto_0

    :cond_0
    return-void
.end method

.method public final c()V
    .locals 4

    iget-object v0, p0, Lcom/google/android/youtube/app/ui/du;->a:Lcom/google/android/youtube/app/ui/dh;

    invoke-static {v0}, Lcom/google/android/youtube/app/ui/dh;->i(Lcom/google/android/youtube/app/ui/dh;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/youtube/app/ui/dn;

    invoke-interface {v0}, Lcom/google/android/youtube/app/ui/dn;->b()V

    goto :goto_0

    :cond_0
    iget-object v0, p0, Lcom/google/android/youtube/app/ui/du;->a:Lcom/google/android/youtube/app/ui/dh;

    invoke-static {v0}, Lcom/google/android/youtube/app/ui/dh;->k(Lcom/google/android/youtube/app/ui/dh;)Lcom/google/android/youtube/core/async/by;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/youtube/app/ui/du;->a:Lcom/google/android/youtube/app/ui/dh;

    iget-object v1, v1, Lcom/google/android/youtube/app/ui/dh;->a:Landroid/app/Activity;

    const v2, 0x7f0b01bd

    iget-object v3, p0, Lcom/google/android/youtube/app/ui/du;->a:Lcom/google/android/youtube/app/ui/dh;

    invoke-static {v3}, Lcom/google/android/youtube/app/ui/dh;->j(Lcom/google/android/youtube/app/ui/dh;)Lcom/google/android/youtube/app/k;

    move-result-object v3

    invoke-virtual {v3}, Lcom/google/android/youtube/app/k;->f()Z

    move-result v3

    invoke-virtual {v0, v1, p0, v2, v3}, Lcom/google/android/youtube/core/async/by;->a(Landroid/app/Activity;Lcom/google/android/youtube/core/async/bk;IZ)V

    return-void
.end method

.method public final g_()V
    .locals 2

    iget-object v0, p0, Lcom/google/android/youtube/app/ui/du;->a:Lcom/google/android/youtube/app/ui/dh;

    invoke-static {v0}, Lcom/google/android/youtube/app/ui/dh;->i(Lcom/google/android/youtube/app/ui/dh;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/youtube/app/ui/dn;

    invoke-interface {v0}, Lcom/google/android/youtube/app/ui/dn;->i()V

    goto :goto_0

    :cond_0
    return-void
.end method
